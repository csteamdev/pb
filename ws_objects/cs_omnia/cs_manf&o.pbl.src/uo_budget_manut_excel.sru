﻿$PBExportHeader$uo_budget_manut_excel.sru
$PBExportComments$presenta in excel il bilancio delle manutenzioni
forward
global type uo_budget_manut_excel from nonvisualobject
end type
end forward

global type uo_budget_manut_excel from nonvisualobject
end type
global uo_budget_manut_excel uo_budget_manut_excel

type variables
datastore ids_budget
long il_max_livello, il_x, il_y, il_xbudget, il_periodo, il_num_righe, il_cont, &
		il_riga_iniziale, il_riga_finale, il_colonna, il_controllo = 0, il_tot_parziali[], &
		il_periodo_scelto 
string is_livello[], is_liv_1, is_liv_2,is_liv_3, is_liv_4, is_liv_5, is_controllo, is_totale 
OLEObject myoleobject
end variables

forward prototypes
public function string uof_nome_cella (integer fi_riga, integer fi_colonna)
public function string uof_sql_where (string fs_sql, string fs_cod_livello)
public subroutine uof_stile_riga (long fl_riga, long fl_col, long fl_foglio)
public subroutine uof_tree ()
public subroutine uof_crea_foglio (long fl_progressivo)
public function integer uof_totali (integer fl_progressivo, integer fl_num_periodi)
public subroutine uof_creafoglio_periodo (long fl_progressivo, long fl_periodo, string fs_totale)
public subroutine uof_tree_periodi ()
public function long uof_ins_dati (long fl_rig, long fl_col, long fl_progressivo)
public function long uof_ins_dati_periodi (long fl_rig, long fl_col, long fl_progressivo)
public function integer uof_totali_periodi (integer fl_progressivo, integer fl_num_periodi)
public function long uof_ins_dati_totale (long fl_rig, long fl_col, long fl_progressivo)
public subroutine uof_tree_totale ()
public subroutine uof_stile_riga_periodi (long fl_riga, long fl_col, long fl_foglio)
end prototypes

public function string uof_nome_cella (integer fi_riga, integer fi_colonna);string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if fi_colonna > 26 then
	ll_resto = mod(fi_colonna, 26)
	ll_i = long(fi_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	fi_colonna = ll_resto 
end if

if fi_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[fi_colonna] + string(fi_riga)
end if	

return ls_char
end function

public function string uof_sql_where (string fs_sql, string fs_cod_livello);
choose case fs_cod_livello
	case "R"
		// caricamento reparti
		fs_sql = " det_budget_manut_periodi.cod_reparto ASC "
	case "C"
		fs_sql = " det_budget_manut_periodi.cod_cat_attrezzature ASC "
		// caricamento categorie
	case "E" 
		fs_sql = " det_budget_manut_periodi.cod_area_aziendale ASC "
		// caricamento aree
	case "A"
		fs_sql = " det_budget_manut_periodi.cod_attrezzatura ASC "
//		 caricamento attrezzature
	case "D" 
		fs_sql = " det_budget_manut_periodi.cod_divisione ASC "
	case else
		//errore
	return ""
end choose


return fs_sql
end function

public subroutine uof_stile_riga (long fl_riga, long fl_col, long fl_foglio);
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col - 1).borders(1).linestyle = "1"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col - 1).borders(1).Weight = "4"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col).borders(1).linestyle = "1"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col).borders(1).Weight = "4"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col+3).borders(1).linestyle = "1"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col+6).borders(1).linestyle = "9"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col+9).borders(1).linestyle = "1"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col+11).borders(2).linestyle = "1"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col+11).borders(2).Weight = "4"
myoleobject.worksheets(fl_foglio).rows(fl_riga).borders(4).linestyle = "2"
myoleobject.worksheets(fl_foglio).rows(fl_riga).font.size = "8"
//myoleobject.sheets(fl_foglio).cells(fl_riga, fl_col+11).numberformat="#.##0,00;[Rosso]-#.##0,00"

myoleobject.worksheets(fl_foglio).range(string(uof_nome_cella(fl_riga,fl_col))+":"+string(uof_nome_cella(fl_riga, fl_col+11))).numberformat="#.##0,00;[Rosso]-#.##0,00"

//myoleobject.worksheets(fl_foglio).cells(fl_riga,1).font.size = "8"
end subroutine

public subroutine uof_tree ();string ls_cod_divisione, ls_des_divisione, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_cat_attrezzature, ls_cod_attrezzatura, &
ls_des_area_aziendale, ls_des_reparto, ls_des_cat_attrezzature, ls_des_attrezzatura

long ll_controllo, ll_appo


//elimino le righe che sono il  dettaglio 

if il_max_livello < 5 then
      if  not isnull(ids_budget.getitemstring(il_xbudget, "cod_divisione")) and not isnull(ids_budget.getitemstring(il_xbudget, "cod_area_aziendale")) and &
          not isnull(ids_budget.getitemstring(il_xbudget, "cod_reparto")) and not isnull(ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature")) and & 
			 not isnull(ids_budget.getitemstring(il_xbudget, "cod_attrezzaturA")) then
             if il_num_righe = il_xbudget and il_controllo = 0 then
						il_riga_finale = il_y 
				 end if
				 il_xbudget++
              return
            end if    
end if

//ll_controllo = 0

if il_cont > il_max_livello then 
            if il_periodo <> ids_budget.getitemnumber(il_xbudget, "num_periodo") then 
                        il_riga_finale = il_y
                        il_y = il_riga_iniziale
                        il_cont = 1
                        il_x = 1 
                        il_controllo = 1
                        return
            end if
            if il_cont > 5 then 
                        il_cont = 5 
                     return
           end if
            do while il_cont >0
                        if il_x <= 0 then il_x = 1
                        if il_cont <= 0 then
                                   il_cont = 1
                                   il_x = 1
                                   return
                        end if
                        CHOOSE CASE is_livello[il_cont]
                                   CASE "D"
                                               if ids_budget.getitemstring(il_xbudget, "cod_divisione") <> ids_budget.getitemstring(il_xbudget - 1, "cod_divisione") or isnull(ids_budget.getitemstring(il_xbudget, "cod_divisione")) then                             
                                                           il_cont --
                                                           il_x --
                                                           if il_cont <= 0 then
                                                                       il_cont = 1
                                                                       il_x = 1
                                                                       return
                                                           end if
                                                           continue
                                               else                                                                 
                                                           il_cont ++
                                                           il_x ++
                                                           if il_cont>5 then
                                                                       il_cont =5
                                                                       il_x =5
                                                           end if
                                                           return
                                               end if
                                   CASE "E"
                                               if ids_budget.getitemstring(il_xbudget, "cod_area_aziendale") <> ids_budget.getitemstring(il_xbudget - 1, "cod_area_aziendale") or isnull(ids_budget.getitemstring(il_xbudget, "cod_area_aziendale"))  then
                                                           il_cont --
                                                           il_x --
                                                           if il_cont <= 0 then
                                                                       il_cont = 1
                                                                       il_x = 1
                                                                       return
                                                           end if
                                                           continue
                                               else
                                                           il_cont ++
                                                           il_x ++
                                                           if il_cont>5 then
                                                                       il_cont =5
                                                                       il_x =5
                                                           end if
                                                           return
                                               end if
                                   CASE "R"
                                               if ids_budget.getitemstring(il_xbudget, "cod_reparto") <> ids_budget.getitemstring(il_xbudget - 1, "cod_reparto") or isnull(ids_budget.getitemstring(il_xbudget, "cod_reparto"))         then
                                                           il_cont --
                                                           il_x --
                                                           if il_cont <= 0 then
                                                                       il_cont = 1
                                                                       il_x = 1
                                                                       return
                                                           end if
                                                           continue
                                               else
                                                           il_cont ++
                                                           il_x ++
                                                           if il_cont>5 then
                                                                       il_cont =5
                                                                       il_x =5
                                                           end if
                                                           return
                                               end if
                                   CASE "C"
                                               if ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature") <> ids_budget.getitemstring(il_xbudget - 1, "cod_cat_attrezzature") or isnull(ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature"))            then
                                                           il_cont --
                                                           il_x --
                                                           if il_cont <= 0 then
                                                                       il_cont = 1
                                                                       il_x = 1
                                                                       return
                                                           end if
                                                           continue
                                               else
                                                           il_cont ++
                                                           il_x ++
                                                           if il_cont>5 then
                                                                       il_cont =5
                                                                       il_x =5
                                                           end if
                                                           return
                                               end if
                                   CASE "A"
                                               if ids_budget.getitemstring(il_xbudget, "cod_attrezzatura") <> ids_budget.getitemstring(il_xbudget - 1, "cod_attrezzatura") or isnull(ids_budget.getitemstring(il_xbudget, "cod_attrezzatura"))            then
                                                           il_cont --
                                                           il_x --
                                                           if il_cont <= 0 then
                                                                       il_cont = 1
                                                                       il_x = 1
                                                                       return
                                                           end if
                                                           continue
                                               else
                                                           il_cont ++
                                                           il_x ++
                                                           if il_cont>5 then
                                                                       il_cont =5
                                                                       il_x =5
                                                           end if
                                                           return
                                               end if
                                               CASE "N"
                                                           il_cont --
                                                           il_x --
                                                           if il_cont <= 0 then
                                                                       il_cont = 1
                                                                       il_x = 1
                                                                       return
                                                           end if
                                                           continue
                        end choose                   
            loop
            return
end if

//rimposto i parametri per il nuovo foglio

if il_periodo <> ids_budget.getitemnumber(il_xbudget, "num_periodo") and il_controllo = 0   then 
            il_riga_finale = il_y
            il_y = il_riga_iniziale
            il_cont = 1
            il_x = 1 
				il_controllo = 1
end if
if il_num_righe = il_xbudget and il_controllo = 0 then
	il_riga_finale = il_y + 1
end if
il_controllo = 0
il_periodo = ids_budget.getitemnumber(il_xbudget, "num_periodo")
//formatto la riga su cui scrivero'
uof_stile_riga(il_y, il_colonna, il_periodo+1)
if il_x = 1  and il_periodo  = 1 then
il_tot_parziali[upperbound(il_tot_parziali[])+1] = il_y
	//ll_i++
end if
//scrivo il codice e la descrizione nel foglio xl
CHOOSE CASE is_livello[il_cont]

            CASE "D"

                        ls_cod_divisione = ids_budget.getitemstring(il_xbudget, "cod_divisione")

                        select des_divisione

                        into :ls_des_divisione

                        from anag_divisioni

                        where cod_azienda = :s_cs_xx.cod_azienda and cod_divisione = :ls_cod_divisione;

                        if sqlca.sqlcode <> 0 then

                                   g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca delle divisioni!"+ sqlca.sqlerrtext)

                                   return

                        end if

                                               

                        myoleobject.worksheets(il_periodo+1).cells(il_y, il_x).value = ls_cod_divisione

                        myoleobject.worksheets(il_periodo+1).cells(il_y, il_x+1).value = ls_des_divisione

 

            CASE "E"

                        ls_cod_area_aziendale = ids_budget.getitemstring(il_xbudget, "cod_area_aziendale")

 

                        select des_area

                        into :ls_des_area_aziendale

                        from tab_aree_aziendali

                        where cod_azienda = :s_cs_xx.cod_azienda and cod_area_aziendale = :ls_cod_area_aziendale;

                        if sqlca.sqlcode <> 0 then

                                   g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca delle aree aziendali!"+ sqlca.sqlerrtext)

                                   return

                        end if

                                               

                        myoleobject.worksheets(il_periodo+1).cells(il_y, il_x).value = ls_cod_area_aziendale

                        myoleobject.worksheets(il_periodo+1).cells(il_y, il_x+1).value = ls_des_area_aziendale

            CASE "R"

                        ls_cod_reparto = ids_budget.getitemstring(il_xbudget, "cod_reparto")

 

                        select des_reparto

                        into :ls_des_reparto

                        from anag_reparti

                        where cod_azienda = :s_cs_xx.cod_azienda and cod_reparto = :ls_cod_reparto;

                        if sqlca.sqlcode <> 0 then

                                   g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei reparti!"+ sqlca.sqlerrtext)

                                   return

                        end if

                                               

                        myoleobject.worksheets(il_periodo+1).cells(il_y, il_x).value = ls_cod_reparto

                        myoleobject.worksheets(il_periodo+1).cells(il_y, il_x+1).value = ls_des_reparto

            CASE "C"

                        ls_cod_cat_attrezzature = ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature")

 

                        select des_cat_attrezzature

                        into :ls_des_cat_attrezzature

                        from tab_cat_attrezzature

                        where cod_azienda = :s_cs_xx.cod_azienda and cod_cat_attrezzature = :ls_cod_cat_attrezzature;

                        if sqlca.sqlcode <> 0 then

                                   g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca delle categorie attrezzature!"+ sqlca.sqlerrtext)

                                   return

                        end if

                                               

                        myoleobject.worksheets(il_periodo+1).cells(il_y, il_x).value = ls_cod_cat_attrezzature

                        myoleobject.worksheets(il_periodo+1).cells(il_y, il_x+1).value = ls_des_cat_attrezzature

            CASE "A"

                                   ls_cod_attrezzatura = ids_budget.getitemstring(il_xbudget, "cod_attrezzatura")

 

                        select descrizione

                        into :ls_des_attrezzatura

                        from anag_attrezzature

                        where cod_azienda = :s_cs_xx.cod_azienda and 
										cod_attrezzatura = :ls_cod_attrezzatura;

                        if sqlca.sqlcode <> 0 then

                                   g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca del codice attrezzatura!"+ sqlca.sqlerrtext)

                                   return

                        end if

                                               

                        myoleobject.worksheets(il_periodo+1).cells(il_y, il_x).value = ls_cod_attrezzatura

                        myoleobject.worksheets(il_periodo+1).cells(il_y, il_x+1).value = ls_des_attrezzatura

            CASE ELSE

                        g_mb.messagebox("OMNIA","Si è verificato un errore tipo non trovato!")

                        return 

END CHOOSE

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna -1).value = ids_budget.getitemnumber(il_xbudget, "num_copie_attrezzatura")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_int")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna+1).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_est")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna+2).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_ricambi")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna+3).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_int_str")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna+4).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_est_str")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna+5).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_ricambi_str")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna+6).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_risorse_int")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna+7).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_risorse_est")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna+8).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_ricambi")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna+9).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_risorse_int_str")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna+10).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_risorse_est_str")

myoleobject.worksheets(il_periodo+1).cells(il_y, il_colonna+11).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_ricambi_str")

il_y++

il_x++

il_xbudget++

il_cont++

 

return


end subroutine

public subroutine uof_crea_foglio (long fl_progressivo);//file iniziale di generazione fogli excel
long ll_col, ll_rig, ll_errore, ll_i, ll_num_periodi, ll_b, ll_riga_iniziale, ll_riga_finale, &
	  ll_foglio, ll_x, ll_y, ll_a
string ls_des_date, ls_rag_soc_azienda, ls_somma, ls_liv_1, ls_descrizione_1, ls_valore_1,&
	ls_descrizione_2, ls_valore_2, ls_descrizione_3, ls_valore_3, ls_somma_2, ls_des_budget
int li_num_fogli, li_i
datetime ldt_da_data, ldt_a_data
dec {2} ld_fattore_k

//OLEObject myoleobject

// controllo che ci sia almeno un livello 
// michela 22/11/2005: prelevo anche il fattore k
select 	flag_tipo_livello_1,
			fattore_k
into		:ls_liv_1,
			:ld_fattore_k
from 		tes_budget_manutenzioni
where		cod_azienda = :s_cs_xx.cod_azienda and
			progressivo = :fl_progressivo;
			
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nel controllo dei livelli!")
	return 
end if

if ls_liv_1 = "N" then
	g_mb.messagebox("OMNIA","Nessun livello selezionato!")
	return 
end if

// creazione oggetto EXCEL 
myoleobject = CREATE OLEObject
ll_col = 1
ll_rig = 1

g_mb.messagebox("APICE", "Non cliccare sul foglio di lavoro di excel.")

select rag_soc_1 
into :ls_rag_soc_azienda
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

ll_errore = myoleobject.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return
end if

// caratteristiche del foglio in generale
myoleobject.Visible = True
myoleobject.Workbooks.Add

select num_periodi 
into 	:ll_num_periodi
from 	tes_budget_manutenzioni
where cod_azienda =:s_cs_xx.cod_azienda and
		progressivo = :fl_progressivo;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei periodi!")
	return
end if
		
if ll_num_periodi <=0 then
	g_mb.messagebox("OMNIA","il Budget selezionato non ha periodi!")
	return
end if

li_num_fogli = myoleobject.sheets.count

for li_i = li_num_fogli + 1 to ll_num_periodi +1
	myoleobject.sheets.add
next
	
select max(data_fine), 
		 min(data_inizio)
into   :ldt_a_data, 
		 :ldt_da_data
from   det_budget_manut_periodi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 progressivo = :fl_progressivo;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei dati!")
	return
end if	

ls_des_date = "dal "+string(date(ldt_da_data))+" al "+string(date(ldt_a_data))

select descrizione_1, 
		 valore_1, 
		 descrizione_2, 
		 valore_2, 
		 descrizione_3, 
		 valore_3, 
		 des_budget
into   :ls_descrizione_1, 
       :ls_valore_1, 
		 :ls_descrizione_2, 
		 :ls_valore_2, 
		 :ls_descrizione_3, 
		 :ls_valore_3, 
		 :ls_des_budget
from   tes_budget_manutenzioni 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 progressivo = :fl_progressivo;
			
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei dati!")
	return
end if	

for ll_i = 1 to ll_num_periodi +1
	
	myoleobject.worksheets(ll_i).PageSetup.orientation = 2
	myoleobject.worksheets(ll_i).PageSetup.leftmargin = 40
	myoleobject.worksheets(ll_i).PageSetup.rightmargin = 40
	myoleobject.worksheets(ll_i).PageSetup.topmargin = 40
	myoleobject.worksheets(ll_i).PageSetup.bottommargin = 40
	myoleobject.worksheets(ll_i).range("A1:M1").font.name = "ARIAL"
	//unione celle
	myoleobject.worksheets(ll_i).range("A1:M1").merge(true)
	myoleobject.worksheets(ll_i).range("A1:M1").font.size = "14"
	myoleobject.worksheets(ll_i).range("A1:P2").Font.Bold = True
	myoleobject.worksheets(ll_i).range("A1:M1").Font.Italic = True
	myoleobject.worksheets(ll_i).range("A1:M1").font.ColorIndex = "1"
	myoleobject.worksheets(ll_i).range("A1:M1").HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).cells(1,1).value = ls_rag_soc_azienda
	myoleobject.worksheets(ll_i).range("A1:M1").borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range("A1:M1").Interior.ColorIndex = "3"
	myoleobject.worksheets(ll_i).range("O1:O1").HorizontalAlignment = "1"
	myoleobject.worksheets(ll_i).range("O2:O2").HorizontalAlignment = "1"
	myoleobject.worksheets(ll_i).range("N1:N1").value = "FATTORE K:" 
	myoleobject.worksheets(ll_i).range("P1:P1").value = ld_fattore_k// michela 22/11/2005: sostituisco con il valore"+1"
	myoleobject.worksheets(ll_i).range("N2:N2").value = "TOTALE VENDITA:" 

	
	myoleobject.worksheets(ll_i).range("A2:M2").font.name = "ARIAL"
	myoleobject.worksheets(ll_i).range("A2:M2").merge(true)
	myoleobject.worksheets(ll_i).range("A2:M2").value = "- BUDGET -  "+ ls_des_date + " PERIODO N."+string(ll_i - 1)
	myoleobject.worksheets(ll_i).range("A2:M2").font.size = "12"
	myoleobject.worksheets(ll_i).range("A2:M2").Font.Bold = True
	myoleobject.worksheets(ll_i).range("A2:M2").HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).range("A2:M2").Interior.ColorIndex = "36"
	myoleobject.worksheets(ll_i).range("A2:M2").borders.linestyle = "1"

	myoleobject.worksheets(ll_i).range("A3:M6").font.name = "ARIAL"
	myoleobject.worksheets(ll_i).range("A3:M6").Font.Bold = True
	myoleobject.worksheets(ll_i).range("A4:M4").merge(true)
	myoleobject.worksheets(ll_i).range("A5:M5").merge(true)
	myoleobject.worksheets(ll_i).range("A3:M3").merge(true)
	myoleobject.worksheets(ll_i).range("A6:M6").merge(true)
	myoleobject.worksheets(ll_i).range("A3:M5").font.size = "10"
	myoleobject.worksheets(ll_i).range("A3:M3").value = ls_des_budget
	myoleobject.worksheets(ll_i).range("A4:M4").value = ls_descrizione_1+" : "+ls_valore_1
	myoleobject.worksheets(ll_i).range("A5:M5").value = ls_descrizione_2+" : "+ls_valore_2
	myoleobject.worksheets(ll_i).range("A6:M6").value = ls_descrizione_3+" : "+ls_valore_3
	myoleobject.worksheets(ll_i).range("A3:M6").HorizontalAlignment = "3"

	myoleobject.worksheets(ll_i).rows(5).HorizontalAlignment = "3"

	for ll_b=1 to 6
		myoleobject.worksheets(ll_i).columns(ll_b).columnwidth = "8"
		myoleobject.worksheets(ll_i).columns(ll_b).font.bold = true
	next
	
	for ll_b=7 to 13
		myoleobject.worksheets(ll_i).columns(ll_b).columnwidth = "4"
		myoleobject.worksheets(ll_i).columns(ll_b).font.bold = true
	next
	
	myoleobject.worksheets(ll_i).name = string(ll_i - 1)
	ll_col =13
	ll_rig = 5
	
	if ll_i = 1 then	
		myoleobject.worksheets(1).range("A2:M2").value = "- BUDGET -  "+ ls_des_date + "  TOTALE"
		myoleobject.worksheets(1).name = "totale"
	
	else
		
		select min(data_inizio), 
				 max(data_fine)
		into 	 :ldt_da_data, 
		       :ldt_a_data
		from 	 det_budget_manut_periodi
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 progressivo = :fl_progressivo and
				 num_periodo = :ll_i - 1 ;
				
		if sqlca.sqlcode = 100 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nel conteggio dei periodi ricalcolare il budget!")
			return
		end if
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca delle date dei periodi:" + sqlca.sqlerrtext)
			return
		end if
		
	end if
	ll_col++
	ll_col++
	myoleobject.worksheets(ll_i).rows(ll_rig).Font.Bold = True
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(1).linestyle = "2"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(1).Weight = "4"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(2).linestyle = "2"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(2).Weight = "4"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(3).linestyle = "2"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(3).Weight = "4"
	
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col).value = "dal "+string(date(ldt_da_data))+"al "+string(date(ldt_a_data))
	ll_rig++
	
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(1).linestyle = "2"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(1).Weight = "4"
	myoleobject.worksheets(ll_i).rows(ll_rig).HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).rows(ll_rig).font.size = "8"
	myoleobject.worksheets(ll_i).rows(ll_rig).Font.Bold = True
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(3).linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(4).linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(2).linestyle = "9"
	myoleobject.worksheets(ll_i).cells(ll_rig, ll_col).value = "BUDGET"
	
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +6))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +6))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(3).linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +6))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(4).linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +6))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(2).linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +6))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(2).Weight = "4"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+6).value = "CONSUNTIVO"
	ll_rig++
	
	myoleobject.worksheets(ll_i).rows(ll_rig).HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).rows(ll_rig).font.size = "8"
	//myoleobject.worksheets(ll_i).rows(ll_rig).Font.Bold = True
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+2))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+2))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+2))).borders(1).Weight = "4"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col).value = "ORDINARIE"
	
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +3))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +3))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+3).value = "STRAORD."
	
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+6))+":"+string(uof_nome_cella(ll_rig, ll_col+8))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+6))+":"+string(uof_nome_cella(ll_rig, ll_col+8))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+6))+":"+string(uof_nome_cella(ll_rig, ll_col+8))).borders(1).linestyle = "9"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+6).value = "ORDINARIE"
	
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +9))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +9))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +9))+":"+string(uof_nome_cella(ll_rig, ll_col+11))).borders(2).Weight = "4"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+9).value = "STRAORD."
	
	ll_rig++
	myoleobject.worksheets(ll_i).rows(ll_rig).rowheight = 40
	myoleobject.worksheets(ll_i).rows(ll_rig).HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).rows(ll_rig).font.size = "8"
	myoleobject.worksheets(ll_i).rows(ll_rig).orientation = "90"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+1))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+1))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+1))).borders(1).Weight = "4"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col).value = "RISORSE"
	
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+2).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+2).value = "RICAMBI"
	
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+3))+":"+string(uof_nome_cella(ll_rig, ll_col+4))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+3))+":"+string(uof_nome_cella(ll_rig, ll_col+4))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+3).value = "RISORSE"
	
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+5).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+5).value = "RICAMBI"
	
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+6))+":"+string(uof_nome_cella(ll_rig, ll_col+7))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+6))+":"+string(uof_nome_cella(ll_rig, ll_col+7))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+6))+":"+string(uof_nome_cella(ll_rig, ll_col+7))).borders(1).linestyle = "9"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+6).value = "RISORSE"
	
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+8).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+8).value = "RICAMBI"
	
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+9))+":"+string(uof_nome_cella(ll_rig, ll_col+10))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+9))+":"+string(uof_nome_cella(ll_rig, ll_col+10))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+9).value = "RISORSE"
	
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+11).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+11).borders(2).Weight = "4"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+11).value = "RICAMBI"
	ll_rig++
	myoleobject.worksheets(ll_i).rows(ll_rig).HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).rows(ll_rig).font.size = "8"
	//myoleobject.worksheets(ll_i).rows(ll_rig).Font.Bold = True
	myoleobject.worksheets(ll_i).rows(ll_rig).orientation = "90"
	uof_stile_riga(ll_rig, ll_col, ll_i)
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col - 1).borders.Weight = "4"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col - 1).value = "Num. attrezzature"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col).value = "Operaio"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+1).value = "Fornitore"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+3).value = "Operaio"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+4).value = "Fornitore"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+6).value = "Operaio"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+7).value = "Fornitore"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+9).value = "Operaio"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+10).value = "Fornitore"
	ll_rig++
	il_colonna = ll_col
	il_riga_iniziale = ll_rig 

next


//myoleobject.worksheets(1).cells(ll_rig,ll_col).value = ls_des_date
//fun che carica i dati 

ll_rig = uof_ins_dati(ll_rig, ll_col,  fl_progressivo)
if ll_rig = -1 then
	return
end if

//fl_progressivo ++
// parametro da mettere quando ho finito di inserire le attrezzature
il_riga_finale = ll_rig + 1

//totali
ll_rig = uof_totali(fl_progressivo, ll_num_periodi )
if ll_rig = -1 then
	return
end if

//for ll_i = 1 to ll_num_periodi
	//myoleobject.worksheets(ll_i).PageSetup.PrintArea = "$A$1:$S$"+string(il_riga_finale+5)
//next

g_mb.messagebox("OMNIA","Budget terminato!")

DESTROY(myoleobject)

end subroutine

public function integer uof_totali (integer fl_progressivo, integer fl_num_periodi);long ll_num_periodi, ll_x, ll_col, ll_y, ll_i, ll_a, ll_prova, ll_prova_2, ll_b
string ls_somma, ls_cod_utente, ls_cod_destinatario, fs_errore, ls_nome_dest, ls_stringa_somma
datetime ldt_data_invio
time lt_ora_invio
int il_anno_reg_intervento, il_num_reg_intervento, il_prog_budget, il_anno_non_conf, il_num_non_conf

ll_num_periodi = long(fl_num_periodi)
ll_col = 15

//compilo la cartella totale con i dati particolari



for ll_x = ll_col to ll_col+11
	for ll_y = il_riga_iniziale to il_riga_finale - 1
		uof_stile_riga(ll_y, ll_col, 1)
		ls_somma = "=SOMMA('"+myoleobject.worksheets(2).name+":"+myoleobject.worksheets(ll_num_periodi +1).name+"'!"+uof_nome_cella(ll_y, ll_x)+")"
	 	myoleobject.worksheets(1).cells(ll_y,ll_x).value = ls_somma
	next
next
	
//somme parziali e totali per ogni foglio 
ll_y =	il_riga_finale + 1
ll_col = 15
//totali colonne
for ll_i = 1 to ll_num_periodi + 1
		uof_stile_riga(ll_y, ll_col, ll_i)
		myoleobject.worksheets(ll_i).rows(il_riga_finale).Interior.ColorIndex = "36"
		myoleobject.worksheets(ll_i).cells(ll_y, 1).value = "TOTALI COLONNE"
		for ll_a =  15 to 26
			
			ls_somma = "=" 
			//=+'1'!N10+'1'!N11+'1'!N12
			for ll_b = 1 to upperbound(il_tot_parziali[])
				ls_somma += "+'"+myoleobject.worksheets(ll_i).name+"'!"+ uof_nome_cella(il_tot_parziali[ll_b], ll_a)
			next

			 myoleobject.worksheets(ll_i).cells(ll_y, ll_a).value = ls_somma			 
		next
next
ll_y ++
for ll_i = 1 to ll_num_periodi + 1
		uof_stile_riga(ll_y, ll_col, ll_i)
		myoleobject.worksheets(ll_i).cells(ll_y, 1).value = "TOTALI PARZIALI"
		ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col)+":"+uof_nome_cella(ll_y - 1, ll_col + 2 )+")"	
	 	myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 2).value = ls_somma		
		ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col + 3)+":"+uof_nome_cella(ll_y - 1, ll_col + 5)+")"	
		myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 5).value = ls_somma			 
		ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col + 6)+":"+uof_nome_cella(ll_y - 1, ll_col + 8)+")"	
	 	myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 8).value = ls_somma			 
		ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col + 9)+":"+uof_nome_cella(ll_y - 1, ll_col + 11)+")"	
	 	myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 11).value = ls_somma			 		
next
ll_y ++

for ll_i = 1 to ll_num_periodi +1
		uof_stile_riga(ll_y, ll_col, ll_i)
		myoleobject.worksheets(ll_i).cells(ll_y, 1).value = "TOTALI"
		ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col)+":"+uof_nome_cella(ll_y - 1, ll_col + 3 + 2)+")"	
	 	myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 3 + 2).value = ls_somma	
		//ll_prova = myoleobject.worksheets(ll_i).range("P1:P1").value
		//ll_prova_2 = myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 3 + 2).value
		//ls_somma = "+"+myoleobject.worksheets(ll_i).range("P1:P1")+"*"+myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 3 + 2)
		myoleobject.worksheets(ll_i).range("P2:P2").value = "= P1 * "+uof_nome_cella(ll_y, ll_col + 3 + 2)
		ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col + 4 + 2 )+":"+uof_nome_cella(ll_y - 1, ll_col + 3 + 3 + 3 + 2)+")"	
	 	myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 3 + 3 + 3 + 2).value = ls_somma			 		
next


//
//firme

//
ll_y ++
declare cu_firme cursor for
	SELECT cod_utente,  cod_destinatario, data_invio,  ora_invio   
 	//into :ls_cod_utente, :ls_cod_destinatario, :ldt_data_invio, :lt_ora_invio
    FROM det_liste_dist_fasi_com  
   WHERE  det_liste_dist_fasi_com.cod_azienda = :s_cs_xx.cod_azienda AND  
          det_liste_dist_fasi_com.anno_reg_intervento is null AND  
         det_liste_dist_fasi_com.num_reg_intervento is null AND  
         det_liste_dist_fasi_com.anno_non_conf is null AND  
         det_liste_dist_fasi_com.num_non_conf is null AND  
         det_liste_dist_fasi_com.progressivo = :fl_progressivo   ; 
			
			
	open cu_firme;
	if sqlca.sqlcode = -1 then
		fs_errore = "Errore in OPEN cu_firme~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	do while true
		fetch cu_firme into :ls_cod_utente, 
								  :ls_cod_destinatario, 
									:ldt_data_invio, 
									:lt_ora_invio ;
									
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in FETCH cu_firme~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
SELECT  des_destinatario
into :ls_nome_dest
FROM tab_ind_dest
WHERE (cod_azienda = :s_cs_xx.cod_azienda )AND 
		cod_destinatario = :ls_cod_destinatario;
              
myoleobject.worksheets(1).rows(ll_y).font.size = "10"
//myoleobject.worksheets(1).rows(ll_y, 5).font.size = "10"

myoleobject.worksheets(1).cells(ll_y, 2).value = ls_nome_dest			
myoleobject.worksheets(1).cells(ll_y, 5).value = string(ldt_data_invio, 'dd/mm/yyyy')			
		
		
		
ll_y++	
	loop
	
	close cu_firme;
	


return 1

end function

public subroutine uof_creafoglio_periodo (long fl_progressivo, long fl_periodo, string fs_totale);//file iniziale di generazione fogli excel
long 		ll_col, ll_rig, ll_errore, ll_i, ll_num_periodi, ll_b, ll_riga_iniziale, ll_riga_finale, &
	  		ll_foglio, ll_x, ll_y, ll_a, ll_num_fogli
			  
string 	ls_des_date, ls_rag_soc_azienda, ls_somma, ls_liv_1, ls_descrizione_1, ls_valore_1,&
			ls_descrizione_2, ls_valore_2, ls_descrizione_3, ls_valore_3, ls_somma_2, ls_des_budget
			
int 		li_num_fogli, li_i

datetime ldt_da_data, ldt_a_data

dec {2}  ld_fattore_k

//if isnull(fl_periodo) then fl_periodo = 0

il_periodo_scelto = fl_periodo
is_totale = fs_totale

if is_totale = 'S' and il_periodo_scelto > 0 then
	ll_num_fogli = 2         
else 
	ll_num_fogli = 1			
end if

// controllo che ci sia almeno un livello 
// michela 22/11/2005: prelevo anche il fattore k

select 	flag_tipo_livello_1,
			fattore_k
into		:ls_liv_1,
			:ld_fattore_k
from 		tes_budget_manutenzioni
where		cod_azienda = :s_cs_xx.cod_azienda and
			progressivo = :fl_progressivo;			
			
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nel controllo dei livelli!")
	return 
end if

if ls_liv_1 = "N" then
	g_mb.messagebox("OMNIA","Nessun livello selezionato!")
	return 
end if

// creazione oggetto EXCEL 
myoleobject = CREATE OLEObject
ll_col = 1
ll_rig = 1

g_mb.messagebox("APICE", "Non cliccare sul foglio di lavoro di excel.")

select rag_soc_1 
into   :ls_rag_soc_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

ll_errore = myoleobject.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return
end if

// caratteristiche del foglio in generale
myoleobject.Visible = True
myoleobject.Workbooks.Add

select num_periodi 
into 	 :ll_num_periodi
from 	 tes_budget_manutenzioni
where  cod_azienda =:s_cs_xx.cod_azienda and
		 progressivo = :fl_progressivo;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei periodi!")
	return
end if
		
if ll_num_periodi <=0 then
	g_mb.messagebox("OMNIA","il Budget selezionato non ha periodi!")
	return
end if

li_num_fogli = myoleobject.sheets.count

for li_i = li_num_fogli + 1 to ll_num_fogli
	myoleobject.sheets.add
next
	
select max(data_fine), 
		 min(data_inizio)
into   :ldt_a_data, 
       :ldt_da_data
from   det_budget_manut_periodi
where  cod_azienda =:s_cs_xx.cod_azienda and
		 progressivo = :fl_progressivo;
			
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei dati!")
	return
end if	

ls_des_date = "dal " + string(date(ldt_da_data)) + " al "+string(date(ldt_a_data))

select descrizione_1, 
		 valore_1, 
		 descrizione_2, 
		 valore_2, 
		 descrizione_3, 
		 valore_3,
		 des_budget
into   :ls_descrizione_1, 
       :ls_valore_1, 
		 :ls_descrizione_2, 
		 :ls_valore_2, 
		 :ls_descrizione_3, 
		 :ls_valore_3,
		 :ls_des_budget
from   tes_budget_manutenzioni 
where  cod_azienda =:s_cs_xx.cod_azienda and
		 progressivo = :fl_progressivo;
			
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei dati!")
	return
end if	

for ll_i = 1 to ll_num_fogli
	myoleobject.worksheets(ll_i).PageSetup.orientation = 2
	myoleobject.worksheets(ll_i).PageSetup.leftmargin = 20
	myoleobject.worksheets(ll_i).PageSetup.rightmargin = 20
	myoleobject.worksheets(ll_i).PageSetup.topmargin = 20
	myoleobject.worksheets(ll_i).PageSetup.bottommargin = 20
	
	myoleobject.worksheets(ll_i).range("A1:M1").font.name = "ARIAL"
	//unione celle
	myoleobject.worksheets(ll_i).range("A1:M1").merge(true)
	myoleobject.worksheets(ll_i).range("A1:M1").font.size = "14"
	myoleobject.worksheets(ll_i).range("A1:P2").Font.Bold = True
	myoleobject.worksheets(ll_i).range("A1:M1").Font.Italic = True
	myoleobject.worksheets(ll_i).range("A1:M1").font.ColorIndex = "1"
	myoleobject.worksheets(ll_i).range("A1:M1").HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).cells(1,1).value = ls_rag_soc_azienda
	myoleobject.worksheets(ll_i).range("A1:M1").borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range("A1:M1").Interior.ColorIndex = "3"
	myoleobject.worksheets(ll_i).range("O1:O1").HorizontalAlignment = "1"
	myoleobject.worksheets(ll_i).range("O2:O2").HorizontalAlignment = "1"
	myoleobject.worksheets(ll_i).range("N1:N1").value = "FATTORE K:" 
	myoleobject.worksheets(ll_i).range("P1:P1").value = ld_fattore_k// sostituisco con il valore (Michela)"+1"
	myoleobject.worksheets(ll_i).range("N2:N2").value = "TOTALE VENDITA:" 

	
	myoleobject.worksheets(ll_i).range("A2:M2").font.name = "ARIAL"
	myoleobject.worksheets(ll_i).range("A2:M2").merge(true)
	myoleobject.worksheets(ll_i).range("A2:M2").value = "- BUDGET -  "+ ls_des_date + " PERIODO N."+string(il_periodo_scelto)
	myoleobject.worksheets(ll_i).range("A2:M2").font.size = "12"
	myoleobject.worksheets(ll_i).range("A2:M2").Font.Bold = True
	myoleobject.worksheets(ll_i).range("A2:M2").HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).range("A2:M2").Interior.ColorIndex = "36"
	myoleobject.worksheets(ll_i).range("A2:M2").borders.linestyle = "1"

	myoleobject.worksheets(ll_i).range("A3:M6").font.name = "ARIAL"
	myoleobject.worksheets(ll_i).range("A3:M6").Font.Bold = True
	myoleobject.worksheets(ll_i).range("A4:M4").merge(true)
	myoleobject.worksheets(ll_i).range("A5:M5").merge(true)
	myoleobject.worksheets(ll_i).range("A3:M3").merge(true)
	myoleobject.worksheets(ll_i).range("A6:M6").merge(true)
	myoleobject.worksheets(ll_i).range("A3:M6").font.size = "10"
	myoleobject.worksheets(ll_i).range("A3:M3").value  = ls_des_budget
	myoleobject.worksheets(ll_i).range("A4:M4").value = ls_descrizione_1+" : "+ls_valore_1
	myoleobject.worksheets(ll_i).range("A5:M5").value = ls_descrizione_2+" : "+ls_valore_2
	myoleobject.worksheets(ll_i).range("A6:M6").value = ls_descrizione_3+" : "+ls_valore_3
	myoleobject.worksheets(ll_i).range("A3:M6").HorizontalAlignment = "3"

	myoleobject.worksheets(ll_i).rows(5).HorizontalAlignment = "3"

	for ll_b=1 to 6
		myoleobject.worksheets(ll_i).columns(ll_b).columnwidth = "8"
		myoleobject.worksheets(ll_i).columns(ll_b).font.bold = true
	next
	for ll_b=7 to 13
		myoleobject.worksheets(ll_i).columns(ll_b).columnwidth = "4"
		myoleobject.worksheets(ll_i).columns(ll_b).font.bold = true
	next
	myoleobject.worksheets(ll_i).name = string(il_periodo_scelto)
	
	ll_col =13
	ll_rig = 5
	
	if ll_i = 1  and is_totale = 'S' then	
		
		myoleobject.worksheets(1).range("A2:M2").value = " - BUDGET -  " + ls_des_date + "  TOTALE"
		myoleobject.worksheets(1).name = "TOTALE"
	
	else
		
		select min(data_inizio), 
				 max(data_fine)
		into 	 :ldt_da_data, 
				 :ldt_a_data
		from 	 det_budget_manut_periodi
		where  cod_azienda =:s_cs_xx.cod_azienda and
				 progressivo = :fl_progressivo and
				 num_periodo = :il_periodo_scelto ;
				
		if sqlca.sqlcode = 100 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nel conteggio dei periodi ricalcolare il budget!")
			return
		end if
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca delle date dei periodi!")
			return
		end if
		
	end if
	
	ll_col++
	ll_col++
	myoleobject.worksheets(ll_i).rows(ll_rig).Font.Bold = True
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(1).linestyle = "2"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(1).Weight = "4"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(2).linestyle = "2"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(2).Weight = "4"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(3).linestyle = "2"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(3).Weight = "4"
	
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col).value = "dal "+string(date(ldt_da_data))+"al "+string(date(ldt_a_data))
	ll_rig++
	
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(1).linestyle = "2"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(1).Weight = "4"
	myoleobject.worksheets(ll_i).rows(ll_rig).HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).rows(ll_rig).font.size = "8"
	myoleobject.worksheets(ll_i).rows(ll_rig).Font.Bold = True
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(3).linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(4).linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(2).linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(2).Weight = "4"
	
	myoleobject.worksheets(ll_i).cells(ll_rig, ll_col).value = "BUDGET"
	
	ll_rig++
	
	myoleobject.worksheets(ll_i).rows(ll_rig).HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).rows(ll_rig).font.size = "8"
	//myoleobject.worksheets(ll_i).rows(ll_rig).Font.Bold = True
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+2))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+2))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+2))).borders(1).Weight = "4"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col).value = "ORDINARIE"
	
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +3))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +3))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col +3))+":"+string(uof_nome_cella(ll_rig, ll_col+5))).borders(2).Weight = "4"
	
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+3).value = "STRAORD."
	
	ll_rig++
	myoleobject.worksheets(ll_i).rows(ll_rig).rowheight = 40
	myoleobject.worksheets(ll_i).rows(ll_rig).HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).rows(ll_rig).font.size = "8"
	myoleobject.worksheets(ll_i).rows(ll_rig).orientation = "90"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+1))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+1))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col))+":"+string(uof_nome_cella(ll_rig, ll_col+1))).borders(1).Weight = "4"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col).value = "RISORSE"
	
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+2).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+2).value = "RICAMBI"
	
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+3))+":"+string(uof_nome_cella(ll_rig, ll_col+4))).merge(true)
	myoleobject.worksheets(ll_i).range(string(uof_nome_cella(ll_rig,ll_col+3))+":"+string(uof_nome_cella(ll_rig, ll_col+4))).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+3).value = "RISORSE"
	
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+5).borders.linestyle = "1"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+5).borders(2).Weight = "4"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+5).value = "RICAMBI"
	
	ll_rig++
	
	myoleobject.worksheets(ll_i).rows(ll_rig).HorizontalAlignment = "3"
	myoleobject.worksheets(ll_i).rows(ll_rig).font.size = "8"
	myoleobject.worksheets(ll_i).rows(ll_rig).orientation = "90"
	uof_stile_riga_periodi(ll_rig, ll_col, ll_i)
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col - 1).borders.Weight = "4"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col - 1).value = "Num. attrezzature"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col).value = "Operaio"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+1).value = "Fornitore"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+3).value = "Operaio"
	myoleobject.worksheets(ll_i).cells(ll_rig,ll_col+4).value = "Fornitore"
	ll_rig++
	il_colonna = ll_col
	il_riga_iniziale = ll_rig 

next

//fun che carica i dati 
if il_periodo_scelto > 0 then
	ll_rig = uof_ins_dati_periodi(ll_rig, ll_col,  fl_progressivo)
	if ll_rig = -1 then
		return
	end if

	il_riga_finale = ll_rig +1
end if

if is_totale = 'S' then
	ll_rig = uof_ins_dati_totale(ll_rig, ll_col,  fl_progressivo)
	if ll_rig = -1 then
		return
	end if

	il_riga_finale = ll_rig
end if

// parametro da mettere quando ho finito di inserire le attrezzature
//totali
ll_rig = uof_totali_periodi(fl_progressivo, ll_num_fogli  - 1)
if ll_rig = -1 then
	return
end if

for ll_i = 1 to ll_num_fogli
	myoleobject.worksheets(ll_i).PageSetup.PrintArea = "$A$1:$T$"+string(il_riga_finale+7)
next


g_mb.messagebox("OMNIA","Budget terminato!")

DESTROY(myoleobject)

end subroutine

public subroutine uof_tree_periodi ();string ls_cod_divisione, ls_des_divisione, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_cat_attrezzature, ls_cod_attrezzatura, &
		 ls_des_area_aziendale, ls_des_reparto, ls_des_cat_attrezzature, ls_des_attrezzatura
		 
long   ll_controllo, ll_i, ll_foglio

if ids_budget.getitemnumber(il_xbudget, "num_periodo") <> il_periodo_scelto then
	
	if il_num_righe = il_xbudget and il_controllo = 0 then
		il_riga_finale = il_y + 1
	end if
	il_xbudget++
	return
	
end if

if is_totale = 'S' then
	ll_foglio = 2
else
	ll_foglio = 1
end if

//elimino le righe che sono il  dettaglio 
if il_max_livello < 5 then
	if  not isnull(ids_budget.getitemstring(il_xbudget, "cod_divisione")) and not isnull(ids_budget.getitemstring(il_xbudget, "cod_area_aziendale")) and &
		not isnull(ids_budget.getitemstring(il_xbudget, "cod_reparto")) and not isnull(ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature")) and &
		not isnull(ids_budget.getitemstring(il_xbudget, "cod_attrezzaturA")) then
		 if il_num_righe = il_xbudget and il_controllo = 0 then
			il_riga_finale = il_y 
		 end if
		il_xbudget++
		return
	end if	
end if
	ll_i = 1
//ll_controllo = 0

if il_cont > il_max_livello then 
	if il_periodo <> ids_budget.getitemnumber(il_xbudget, "num_periodo") then 
		il_riga_finale = il_y
		il_y = il_riga_iniziale
		il_cont = 1
		il_x = 1	
		il_controllo = 1
		return
	end if
	
	if il_cont > 5 then 
		il_cont = 5 
		return
	end if
	do while il_cont >0
		if il_x <= 0 then il_x = 1
		if il_cont <= 0 then
			il_cont = 1
			il_x = 1
			return
		end if
		CHOOSE CASE is_livello[il_cont]
			CASE "D"
				if ids_budget.getitemstring(il_xbudget, "cod_divisione") <> ids_budget.getitemstring(il_xbudget - 1, "cod_divisione") or isnull(ids_budget.getitemstring(il_xbudget, "cod_divisione"))	then			
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
				else						
					il_cont ++
					il_x ++
					if il_cont>5 then
						il_cont =5
						il_x =5
					end if
					return
				end if
			CASE "E"
				if ids_budget.getitemstring(il_xbudget, "cod_area_aziendale") <> ids_budget.getitemstring(il_xbudget - 1, "cod_area_aziendale") or isnull(ids_budget.getitemstring(il_xbudget, "cod_area_aziendale"))	then
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
				else
					il_cont ++
					il_x ++
					if il_cont>5 then
						il_cont =5
						il_x =5
					end if
					return
				end if
			CASE "R"
				if ids_budget.getitemstring(il_xbudget, "cod_reparto") <> ids_budget.getitemstring(il_xbudget - 1, "cod_reparto") or isnull(ids_budget.getitemstring(il_xbudget, "cod_reparto"))	then
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
				else
					il_cont ++
					il_x ++
					if il_cont>5 then
						il_cont =5
						il_x =5
					end if
					return
				end if
			CASE "C"
				if ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature") <> ids_budget.getitemstring(il_xbudget - 1, "cod_cat_attrezzature") or isnull(ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature"))	then
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
				else
					il_cont ++
					il_x ++
					if il_cont>5 then
						il_cont =5
						il_x =5
					end if
					return
				end if
			CASE "A"
				if ids_budget.getitemstring(il_xbudget, "cod_attrezzatura") <> ids_budget.getitemstring(il_xbudget - 1, "cod_attrezzatura") or isnull(ids_budget.getitemstring(il_xbudget, "cod_attrezzatura"))	then
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
				else
					il_cont ++
					il_x ++
					if il_cont>5 then
						il_cont =5
						il_x =5
					end if
					return
				end if
				CASE "N"
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
		end choose		
	loop
	return
end if
//rimposto i parametri per il nuovo foglio
if il_periodo <> ids_budget.getitemnumber(il_xbudget, "num_periodo") and il_controllo = 0 then 
	il_riga_finale = il_y
	il_y = il_riga_iniziale
	il_cont = 1
	il_x = 1	
	if ids_budget.getitemnumber(il_xbudget, "num_periodo") <> il_periodo_scelto then
		il_xbudget++
		return
	end if
		il_controllo = 1
end if
if il_num_righe = il_xbudget and il_controllo = 0 then
	il_riga_finale = il_y + 1
end if
il_controllo = 0
il_periodo = ids_budget.getitemnumber(il_xbudget, "num_periodo")
//formatto la riga su cui scrivero'
uof_stile_riga_periodi(il_y, il_colonna, ll_foglio)

if il_x = 1  and il_periodo  = il_periodo_scelto then
	il_tot_parziali[upperbound(il_tot_parziali[])+1] = il_y
end if

//scrivo il codice e la descrizione nel foglio xl
CHOOSE CASE is_livello[il_cont]
	CASE "D"
		ls_cod_divisione = ids_budget.getitemstring(il_xbudget, "cod_divisione")
		
		select des_divisione
		into   :ls_des_divisione
		from   anag_divisioni
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_divisione = :ls_cod_divisione;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca delle divisioni!"+ sqlca.sqlerrtext) 
			return
		end if
				
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x).value = ls_cod_divisione
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x+1).value = ls_des_divisione

	CASE "E"
		ls_cod_area_aziendale = ids_budget.getitemstring(il_xbudget, "cod_area_aziendale")

		select des_area
		into :ls_des_area_aziendale
		from tab_aree_aziendali
		where cod_azienda = :s_cs_xx.cod_azienda and cod_area_aziendale = :ls_cod_area_aziendale;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca delle aree aziendali!"+ sqlca.sqlerrtext)
			return
		end if
				
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x).value = ls_cod_area_aziendale
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x+1).value = ls_des_area_aziendale
	CASE "R"
		ls_cod_reparto = ids_budget.getitemstring(il_xbudget, "cod_reparto")

		select des_reparto
		into :ls_des_reparto
		from anag_reparti
		where cod_azienda = :s_cs_xx.cod_azienda and cod_reparto = :ls_cod_reparto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei reparti!"+ sqlca.sqlerrtext)
			return
		end if
				
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x).value = ls_cod_reparto
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x+1).value = ls_des_reparto
	CASE "C"
		ls_cod_cat_attrezzature = ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature")

		select des_cat_attrezzature
		into :ls_des_cat_attrezzature
		from tab_cat_attrezzature
		where cod_azienda = :s_cs_xx.cod_azienda and cod_cat_attrezzature = :ls_cod_cat_attrezzature;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca delle categorie attrezzature!" + sqlca.sqlerrtext)
			return
		end if
				
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x).value = ls_cod_cat_attrezzature
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x+1).value = ls_des_cat_attrezzature
	CASE "A"
			ls_cod_attrezzatura = ids_budget.getitemstring(il_xbudget, "cod_attrezzatura")

		select descrizione
		into :ls_des_attrezzatura
		from anag_attrezzature
		where cod_azienda = :s_cs_xx.cod_azienda and cod_attrezzatura = :ls_cod_attrezzatura;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca del codice attrezzatura!"+ sqlca.sqlerrtext)
			return
		end if
				
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x).value = ls_cod_attrezzatura
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x+1).value = ls_des_attrezzatura
	CASE ELSE
		g_mb.messagebox("OMNIA","Si è verificato un errore tipo non trovato!")
		return 
END CHOOSE
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna -1).value = ids_budget.getitemnumber(il_xbudget, "num_copie_attrezzatura")

myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_int")
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+1).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_est")
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+2).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_ricambi")
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+3).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_int_str")
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+4).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_est_str")
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+5).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_ricambi_str")
//myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+6).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_risorse_int")
//myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+7).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_risorse_est")
//myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+8).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_ricambi")
//myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+9).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_risorse_int_str")
//myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+10).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_risorse_est_str")
//myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+11).value = ids_budget.getitemnumber(il_xbudget, "costo_cons_ricambi_str")
il_y++
il_x++
il_xbudget++
il_cont++

return
end subroutine

public function long uof_ins_dati (long fl_rig, long fl_col, long fl_progressivo);string 	 ls_sql, ls_cod_divisione, ls_cod_area_aziendale, ls_prova, ls_ordine, ls_controllo[]
			
long	 ll_prog_periodo, ll_i, ll_a, ll_b, ll_c, ll_errore
boolean	lb_dati
//datastore lds_budget

//declare 	cu_budget dynamic cursor for sqlsa;

il_y = fl_rig
il_periodo = 1

select 	flag_tipo_livello_1, 
			flag_tipo_livello_2,	
			flag_tipo_livello_3, 
			flag_tipo_livello_4,
			flag_tipo_livello_5
into		:is_liv_1, 
			:is_liv_2, 
			:is_liv_3, 
			:is_liv_4, 
			:is_liv_5
from 		tes_budget_manutenzioni
where		cod_azienda = :s_cs_xx.cod_azienda and
			progressivo = :fl_progressivo;
			
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei livelli!")
	return -1
end if
ls_sql = "  SELECT det_budget_manut_periodi.cod_azienda, " & 
         +"det_budget_manut_periodi.progressivo,  "&
         +"det_budget_manut_periodi.num_periodo, " & 
         +"det_budget_manut_periodi.prog_periodo,  " &
         +"det_budget_manut_periodi.data_inizio,   "&
         +"det_budget_manut_periodi.data_fine,   "&
        +" det_budget_manut_periodi.cod_divisione, "&  
        +" det_budget_manut_periodi.cod_area_aziendale, "&   
         +"det_budget_manut_periodi.cod_reparto,   "&
        +" det_budget_manut_periodi.cod_cat_attrezzature,  "&
        +" det_budget_manut_periodi.cod_attrezzatura,   "&
         +"det_budget_manut_periodi.costo_prev_ricambi,  "&
        +" det_budget_manut_periodi.costo_prev_ricambi_str,  "&
       +" det_budget_manut_periodi.costo_prev_risorse_int,   "&
       +" det_budget_manut_periodi.costo_prev_risorse_int_str, "&
        +" det_budget_manut_periodi.costo_prev_risorse_est,   "&
        +" det_budget_manut_periodi.costo_prev_risorse_est_str, "& 
        +" det_budget_manut_periodi.costo_cons_ricambi,   "&
       +"  det_budget_manut_periodi.costo_cons_ricambi_str,   "&
       +"  det_budget_manut_periodi.costo_cons_risorse_int,   "&
       +"  det_budget_manut_periodi.costo_cons_risorse_int_str,  "&
       +"  det_budget_manut_periodi.costo_cons_risorse_est,   "&
       +"  det_budget_manut_periodi.costo_cons_risorse_est_str,  "&
       +"  det_budget_manut_periodi.costo_trend_ricambi,   "&
        +" det_budget_manut_periodi.costo_trend_risorse_int,   "&
        +" det_budget_manut_periodi.costo_trend_risorse_est,  "&
        +" det_budget_manut_periodi.flag_consolidato,   "&
        +" det_budget_manut_periodi.flag_manuale,   "&
        +" det_budget_manut_periodi.flag_record_dati,  "&
		  + " det_budget_manut_periodi.num_copie_attrezzatura " &
    +"FROM det_budget_manut_periodi    "
//ls_sql += "into :ll_prog_periodo"
ls_sql += "where det_budget_manut_periodi.cod_azienda = '"+s_cs_xx.cod_azienda +"' and det_budget_manut_periodi.progressivo = "+string(fl_progressivo)
//ls_sql += " and det_budget_manut_periodi.num_periodo = "+ string(il_periodo_scelto)+" order by num_periodo ASC "
ls_sql += " order by num_periodo ASC"//, prog_periodo DESC "



il_max_livello = 0

if not isnull(is_liv_1) and is_liv_1 <> "N" then
	ls_sql+= "," +uof_sql_where(ls_sql, is_liv_1)
	il_max_livello = 1
	is_livello[1] = is_liv_1
else
	is_livello[1] = "N"
end if

if not isnull(is_liv_2) and is_liv_2 <> "N" then
	ls_sql+="," + uof_sql_where(ls_sql, is_liv_2)
	il_max_livello = 2
	is_livello[2] = is_liv_2
	else
	is_livello[2] = "N"
end if

if not isnull(is_liv_3) and is_liv_3 <> "N" then
	ls_sql+= "," +uof_sql_where(ls_sql, is_liv_3)
	il_max_livello = 3
	is_livello[3] = is_liv_3
	else
	is_livello[3] = "N"
end if

if not isnull(is_liv_4) and is_liv_4 <> "N" then
	ls_sql+= "," +uof_sql_where(ls_sql, is_liv_4)
	il_max_livello = 4
	is_livello[4] = is_liv_4
	else
	is_livello[4] = "N"
end if

if not isnull(is_liv_5) and is_liv_5 <> "N" then
	il_max_livello = 5
	ls_sql+= "," +uof_sql_where(ls_sql, is_liv_5)
	is_livello[5] = is_liv_5
	else
	is_livello[5] = "N"
end if
for ll_b = 1 to 5
	ls_controllo[ll_b] = "N"
next

is_controllo = "N"

for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'D' then
		ls_controllo[1]  = "D"
	end if		
next
for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'E' then
		ls_controllo[2]  = "E"
	end if		
next
for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'R' then
		ls_controllo[3]= "R"
	end if		
next
for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'C' then
		ls_controllo[4] = "C"
	end if		
next

for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'A' then
		ls_controllo[5] = "A"
	end if		
next

for ll_b = 1 to 5
	if ls_controllo[ll_b] = "N" then
		if ll_b = 1 then is_controllo = "D"
		if ll_b = 2 then is_controllo = "E"
		if ll_b = 3 then is_controllo = "R"
		if ll_b = 4 then is_controllo = "C"
		if ll_b = 5 then is_controllo = "A"
		
	end if
next
ls_sql+= "," +uof_sql_where(ls_sql, is_controllo)


ids_budget = create datastore
ids_budget.Dataobject = 'd_ds_budget_manut_excel'
ids_budget.SetTransObject(sqlca)

ids_budget.SetSQLSelect(ls_sql)

il_num_righe = ids_budget.retrieve()
if il_num_righe <0 then
	g_mb.messagebox("APICE", "Errore nella retrieve del datastore ids_budget", StopSign!)
	return -1
end if
il_x = 1
il_xbudget = 1
il_cont = 1

ll_c = 1
do while il_xbudget <= il_num_righe
//	ll_c ++
//	if mod(ll_c, 2) = 0 then
//		myoleobject.workbooks.SaveAs("C:\dati.xls")
//		destroy(myoleobject)
//		myoleobject = CREATE OLEObject
//		ll_errore = myoleobject.ConnectToObject("C:\dati.xls")
//
//		if ll_errore < 0 then
//			messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
//			return -1
//		end if
//		myoleobject.workbooks.open("C:\dati.xls")
//// caratteristiche del foglio in generale
		//myoleobject.Visible = True

		
//	end if

	uof_tree()
loop
if il_riga_finale < il_riga_iniziale then il_riga_finale = il_riga_iniziale + 1
myoleobject.worksheets(2).range(string(uof_nome_cella(il_riga_iniziale, 1))+":"+string(uof_nome_cella(il_riga_finale, 14))).copy(myoleobject.worksheets(1).range(string(uof_nome_cella(il_riga_iniziale, 1))+":"+string(uof_nome_cella(il_riga_finale, 14))))

return il_riga_finale  
end function

public function long uof_ins_dati_periodi (long fl_rig, long fl_col, long fl_progressivo);//carica i dati  

string 	 ls_sql, ls_cod_divisione, ls_cod_area_aziendale, ls_prova, ls_ordine, ls_controllo[]
			
long	 ll_prog_periodo, ll_i, ll_a, ll_b, ll_c, ll_errore
boolean	lb_dati
//datastore lds_budget

//declare 	cu_budget dynamic cursor for sqlsa;

il_y = fl_rig
il_periodo = 1

select 	flag_tipo_livello_1, flag_tipo_livello_2,	flag_tipo_livello_3, flag_tipo_livello_4,
			flag_tipo_livello_5
into		:is_liv_1, :is_liv_2, :is_liv_3, :is_liv_4, :is_liv_5
from 		tes_budget_manutenzioni
where		cod_azienda = :s_cs_xx.cod_azienda and
			progressivo = :fl_progressivo;
			
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei livelli!")
	return -1
end if

ls_sql = "  SELECT det_budget_manut_periodi.cod_azienda, " & 
         +"det_budget_manut_periodi.progressivo,  "&
         +"det_budget_manut_periodi.num_periodo, " & 
         +"det_budget_manut_periodi.prog_periodo,  " &
         +"det_budget_manut_periodi.data_inizio,   "&
         +"det_budget_manut_periodi.data_fine,   "&
        +" det_budget_manut_periodi.cod_divisione, "&  
        +" det_budget_manut_periodi.cod_area_aziendale, "&   
         +"det_budget_manut_periodi.cod_reparto,   "&
        +" det_budget_manut_periodi.cod_cat_attrezzature,  "&
        +" det_budget_manut_periodi.cod_attrezzatura,   "&
         +"det_budget_manut_periodi.costo_prev_ricambi,  "&
        +" det_budget_manut_periodi.costo_prev_ricambi_str,  "&
       +" det_budget_manut_periodi.costo_prev_risorse_int,   "&
       +" det_budget_manut_periodi.costo_prev_risorse_int_str, "&
        +" det_budget_manut_periodi.costo_prev_risorse_est,   "&
        +" det_budget_manut_periodi.costo_prev_risorse_est_str, "& 
        +" det_budget_manut_periodi.costo_cons_ricambi,   "&
       +"  det_budget_manut_periodi.costo_cons_ricambi_str,   "&
       +"  det_budget_manut_periodi.costo_cons_risorse_int,   "&
       +"  det_budget_manut_periodi.costo_cons_risorse_int_str,  "&
       +"  det_budget_manut_periodi.costo_cons_risorse_est,   "&
       +"  det_budget_manut_periodi.costo_cons_risorse_est_str,  "&
       +"  det_budget_manut_periodi.costo_trend_ricambi,   "&
        +" det_budget_manut_periodi.costo_trend_risorse_int,   "&
        +" det_budget_manut_periodi.costo_trend_risorse_est,  "&
        +" det_budget_manut_periodi.flag_consolidato,   "&
        +" det_budget_manut_periodi.flag_manuale,   "&
        +" det_budget_manut_periodi.flag_record_dati,  "&
		  +" det_budget_manut_periodi.num_copie_attrezzatura  "&
    +"FROM det_budget_manut_periodi    "
//ls_sql += "into :ll_prog_periodo"
ls_sql += "where det_budget_manut_periodi.cod_azienda = '"+s_cs_xx.cod_azienda +"' and det_budget_manut_periodi.progressivo = "+string(fl_progressivo)
//ls_sql += " and det_budget_manut_periodi.num_periodo = "+ string(il_periodo_scelto)+" order by num_periodo ASC "
ls_sql += " order by num_periodo ASC"//, prog_periodo DESC "

il_max_livello = 0

if not isnull(is_liv_1) and is_liv_1 <> "N" then
	ls_sql+= "," +uof_sql_where(ls_sql, is_liv_1)
	il_max_livello = 1
	is_livello[1] = is_liv_1
else
	is_livello[1] = "N"
end if

if not isnull(is_liv_2) and is_liv_2 <> "N" then
	ls_sql+="," + uof_sql_where(ls_sql, is_liv_2)
	il_max_livello = 2
	is_livello[2] = is_liv_2
	else
	is_livello[2] = "N"
end if

if not isnull(is_liv_3) and is_liv_3 <> "N" then
	ls_sql+= "," +uof_sql_where(ls_sql, is_liv_3)
	il_max_livello = 3
	is_livello[3] = is_liv_3
	else
	is_livello[3] = "N"
end if

if not isnull(is_liv_4) and is_liv_4 <> "N" then
	ls_sql+= "," +uof_sql_where(ls_sql, is_liv_4)
	il_max_livello = 4
	is_livello[4] = is_liv_4
	else
	is_livello[4] = "N"
end if

if not isnull(is_liv_5) and is_liv_5 <> "N" then
	il_max_livello = 5
	ls_sql+= "," +uof_sql_where(ls_sql, is_liv_5)
	is_livello[5] = is_liv_5
	else
	is_livello[5] = "N"
end if
for ll_b = 1 to 5
	ls_controllo[ll_b] = "N"
next

is_controllo = "N"

for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'D' then
		ls_controllo[1]  = "D"
	end if		
next
for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'E' then
		ls_controllo[2]  = "E"
	end if		
next
for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'R' then
		ls_controllo[3]= "R"
	end if		
next
for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'C' then
		ls_controllo[4] = "C"
	end if		
next

for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'A' then
		ls_controllo[5] = "A"
	end if		
next

for ll_b = 1 to 5
	if ls_controllo[ll_b] = "N" then
		if ll_b = 1 then is_controllo = "D"
		if ll_b = 2 then is_controllo = "E"
		if ll_b = 3 then is_controllo = "R"
		if ll_b = 4 then is_controllo = "C"
		if ll_b = 5 then is_controllo = "A"
		
	end if
next
ls_sql+= "," +uof_sql_where(ls_sql, is_controllo)


ids_budget = create datastore
ids_budget.Dataobject = 'd_ds_budget_manut_excel'
ids_budget.SetTransObject(sqlca)

ids_budget.SetSQLSelect(ls_sql)

il_num_righe = ids_budget.retrieve()
if il_num_righe <0 then
	g_mb.messagebox("APICE", "Errore nella retrieve del datastore ids_budget", StopSign!)
	return -1
end if
il_x = 1
il_xbudget = 1
il_cont = 1

ll_c = 1
do while il_xbudget <= il_num_righe
//	ll_c ++
//	if mod(ll_c, 2) = 0 then
//		myoleobject.workbooks.SaveAs("C:\dati.xls")
//		destroy(myoleobject)
//		myoleobject = CREATE OLEObject
//		ll_errore = myoleobject.ConnectToObject("C:\dati.xls")
//
//		if ll_errore < 0 then
//			messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
//			return -1
//		end if
//		myoleobject.workbooks.open("C:\dati.xls")
//// caratteristiche del foglio in generale
		//myoleobject.Visible = True

		
//	end if
	uof_tree_periodi()
loop
//COPIA NEL PRIMO FOGLIO CONTROLLARE
if il_riga_finale < il_riga_iniziale then il_riga_finale = il_riga_iniziale + 1
if is_totale = 'S' then
	myoleobject.worksheets(2).range(string(uof_nome_cella(il_riga_iniziale, 1))+":"+string(uof_nome_cella(il_riga_finale, 14))).copy(myoleobject.worksheets(1).range(string(uof_nome_cella(il_riga_iniziale, 1))+":"+string(uof_nome_cella(il_riga_finale, 14))))
end if
return il_riga_finale 
end function

public function integer uof_totali_periodi (integer fl_progressivo, integer fl_num_periodi);long 		ll_num_periodi, ll_x, ll_col, ll_y, ll_i, ll_a, ll_prova, ll_prova_2, ll_b
string 	ls_somma, ls_cod_utente, ls_cod_destinatario, fs_errore, ls_nome_dest, ls_stringa_somma
datetime ldt_data_invio
time 		lt_ora_invio
int 		il_anno_reg_intervento, il_num_reg_intervento, il_prog_budget, il_anno_non_conf, il_num_non_conf

ll_num_periodi = long(fl_num_periodi)

//compilo la cartella totale con i dati particolari

//somme parziali e totali per ogni foglio 
ll_y =	il_riga_finale + 1
ll_col = 14 - 6
//totali colonne
for ll_i = 1 to ll_num_periodi + 1
	
	uof_stile_riga_periodi(ll_y, ll_col +7, ll_i)
	myoleobject.worksheets(ll_i).rows(il_riga_finale).Interior.ColorIndex = "36"
	myoleobject.worksheets(ll_i).cells(ll_y, 1).value = "TOTALI COLONNE"
	
	for ll_a =  15 to 26 - 6
			
		ls_somma = "=" 
		//=+'1'!N10+'1'!N11+'1'!N12
		for ll_b = 1 to upperbound(il_tot_parziali[])
			ls_somma += "+'"+myoleobject.worksheets(ll_i).name+"'!"+ uof_nome_cella(il_tot_parziali[ll_b], ll_a)
		next
		myoleobject.worksheets(ll_i).cells(ll_y, ll_a).value = ls_somma			 
		
	next
next

ll_y ++
ll_col = 15
for ll_i = 1 to ll_num_periodi + 1
	uof_stile_riga_periodi(ll_y, ll_col, ll_i)
	myoleobject.worksheets(ll_i).cells(ll_y, 1).value = "TOTALI PARZIALI"
	ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col)+":"+uof_nome_cella(ll_y - 1, ll_col + 2 )+")"	
	myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 2).value = ls_somma		
	ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col + 3)+":"+uof_nome_cella(ll_y - 1, ll_col + 5)+")"	
	myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 5).value = ls_somma			 
	//ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col + 6)+":"+uof_nome_cella(ll_y - 1, ll_col + 8)+")"	
	//myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 8).value = ls_somma			 
//		ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col + 9)+":"+uof_nome_cella(ll_y - 1, ll_col + 11)+")"	
//	 	myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 11).value = ls_somma			 		
next

ll_y ++

for ll_i = 1 to ll_num_periodi + 1
	uof_stile_riga_periodi(ll_y, ll_col, ll_i)
	myoleobject.worksheets(ll_i).cells(ll_y, 1).value = "TOTALI"
	ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col)+":"+uof_nome_cella(ll_y - 1, ll_col + 3 + 2)+")"	
	myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 3 + 2).value = ls_somma	
	myoleobject.worksheets(ll_i).range("P2:P2").value = "= P1 * " + uof_nome_cella(ll_y, ll_col + 3 + 2)
	//ll_prova = myoleobject.worksheets(ll_i).range("P1:P1").value
	//ll_prova_2 = myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 3 + 2).value
	//ls_somma = "+"+myoleobject.worksheets(ll_i).range("P1:P1")+"*"+myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 3 + 2)
	
	//ls_somma = "=SOMMA('"+myoleobject.worksheets(ll_i).name+"'!"+uof_nome_cella(ll_y - 1, ll_col + 4 + 2 )+":"+uof_nome_cella(ll_y - 1, ll_col + 3 + 3 + 3 + 2)+")"	
	//myoleobject.worksheets(ll_i).cells(ll_y, ll_col + 3 + 3 + 3 + 2).value = ls_somma			 		
next


//
//firme

//
ll_y ++
declare cu_firme cursor for
SELECT cod_utente,  
		 cod_destinatario, 
		 data_invio,  
		 ora_invio   
FROM   det_liste_dist_fasi_com  
WHERE  det_liste_dist_fasi_com.cod_azienda = :s_cs_xx.cod_azienda AND  
       det_liste_dist_fasi_com.anno_reg_intervento is null AND  
       det_liste_dist_fasi_com.num_reg_intervento is null AND  
       det_liste_dist_fasi_com.anno_non_conf is null AND  
       det_liste_dist_fasi_com.num_non_conf is null AND  
       det_liste_dist_fasi_com.progressivo = :fl_progressivo   ; 
			
			
open cu_firme;
if sqlca.sqlcode = -1 then
	fs_errore = "Errore in OPEN cu_firme~r~n" + sqlca.sqlerrtext
	return -1
end if
	
do while true
	fetch cu_firme into :ls_cod_utente, 
							  :ls_cod_destinatario, 
							  :ldt_data_invio, 
							  :lt_ora_invio ;
									
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		fs_errore = "Errore in FETCH cu_firme~r~n" + sqlca.sqlerrtext
		return -1
	end if
		
	SELECT  des_destinatario
	into    :ls_nome_dest
	FROM    tab_ind_dest
	WHERE   cod_azienda = :s_cs_xx.cod_azienda and 
			  cod_destinatario = :ls_cod_destinatario;
					  
	myoleobject.worksheets(1).rows(ll_y).font.size = "8"
	//myoleobject.worksheets(1).cells(ll_y, 5).font.size = "10"
	
	myoleobject.worksheets(1).cells(ll_y, 2).value = ls_nome_dest			
	myoleobject.worksheets(1).cells(ll_y, 5).value = string(ldt_data_invio, 'dd/mm/yyyy')			
			
	ll_y++		
loop
close cu_firme;

return 1

end function

public function long uof_ins_dati_totale (long fl_rig, long fl_col, long fl_progressivo);string 	 ls_sql, ls_cod_divisione, ls_cod_area_aziendale, ls_prova, ls_ordine, ls_controllo[]
			
long	    ll_prog_periodo, ll_i, ll_a, ll_b, ll_c, ll_errore, ll_ret
boolean	 lb_dati

il_y = fl_rig
il_periodo = 1

select 	flag_tipo_livello_1, 
			flag_tipo_livello_2,	
			flag_tipo_livello_3, 
			flag_tipo_livello_4,
			flag_tipo_livello_5
into		:is_liv_1, 
		   :is_liv_2, 
			:is_liv_3, 
			:is_liv_4, 
			:is_liv_5
from 		tes_budget_manutenzioni
where		cod_azienda = :s_cs_xx.cod_azienda and
			progressivo = :fl_progressivo;
			
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei livelli!")
	return -1
end if

ls_sql = " select  det_budget_manut_periodi.cod_divisione,  						  " + &
			" 			 det_budget_manut_periodi.cod_area_aziendale, 					  " + &
			" 			 det_budget_manut_periodi.cod_reparto,    						  " + &
			" 			 det_budget_manut_periodi.cod_cat_attrezzature, 				  " + &
			"         det_budget_manut_periodi.cod_attrezzatura,   					  " + &
			"         sum(det_budget_manut_periodi.costo_prev_ricambi),  			  " + &
			"         sum(det_budget_manut_periodi.costo_prev_ricambi_str),        " + &
			"         sum(det_budget_manut_periodi.costo_prev_risorse_int),        " + &
			"         sum(det_budget_manut_periodi.costo_prev_risorse_int_str),    " + &
			"         sum(det_budget_manut_periodi.costo_prev_risorse_est),        " + &
			"         sum(det_budget_manut_periodi.costo_prev_risorse_est_str),    " + &
			"         sum(det_budget_manut_periodi.costo_cons_ricambi),            " + &
			"         sum(det_budget_manut_periodi.costo_cons_ricambi_str),        " + & 
			"         sum(det_budget_manut_periodi.costo_cons_risorse_int),        " + &
			"         sum(det_budget_manut_periodi.costo_cons_risorse_int_str),    " + &
			"         sum(det_budget_manut_periodi.costo_cons_risorse_est),        " + &
			"         sum(det_budget_manut_periodi.costo_cons_risorse_est_str),    " + &
			"         sum(det_budget_manut_periodi.costo_trend_ricambi),           " + &
			"         sum(det_budget_manut_periodi.costo_trend_risorse_int),       " + &
			"         sum(det_budget_manut_periodi.costo_trend_risorse_est),        " + &
			" 			 det_budget_manut_periodi.num_copie_attrezzatura  " + &
         " FROM    det_budget_manut_periodi                                     " + &
         " where   det_budget_manut_periodi.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
         "         det_budget_manut_periodi.flag_consolidato = 'N' and " + &
			"         det_budget_manut_periodi.progressivo = " + string(fl_progressivo) + &
         " group by  det_budget_manut_periodi.cod_divisione,  " + &
			"           det_budget_manut_periodi.cod_area_aziendale, " + &
         "           det_budget_manut_periodi.cod_reparto,    " + &
			"           det_budget_manut_periodi.cod_cat_attrezzature, " + &
         "           det_budget_manut_periodi.cod_attrezzatura," + &
			"				det_budget_manut_periodi.num_copie_attrezzatura "
//**AGGIUNTO VEDERE SE CON S O MENO....^(VEDI SOPRA)
il_max_livello = 0

if not isnull(is_liv_1) and is_liv_1 <> "N" then
	ls_sql+= "order by "+uof_sql_where(ls_sql, is_liv_1) 
	il_max_livello = 1
	is_livello[1] = is_liv_1
else
	is_livello[1] = "N"
end if

if not isnull(is_liv_2) and is_liv_2 <> "N" then
	ls_sql+= ","+ uof_sql_where(ls_sql, is_liv_2)
	il_max_livello = 2
	is_livello[2] = is_liv_2
	else
	is_livello[2] = "N"
end if

if not isnull(is_liv_3) and is_liv_3 <> "N" then
	ls_sql+= ","+uof_sql_where(ls_sql, is_liv_3)
	il_max_livello = 3
	is_livello[3] = is_liv_3
	else
	is_livello[3] = "N"
end if

if not isnull(is_liv_4) and is_liv_4 <> "N" then
	ls_sql+= ","+ uof_sql_where(ls_sql, is_liv_4)
	il_max_livello = 4
	is_livello[4] = is_liv_4
	else
	is_livello[4] = "N"
end if

if not isnull(is_liv_5) and is_liv_5 <> "N" then
	il_max_livello = 5
	ls_sql+= ","+ uof_sql_where(ls_sql, is_liv_5)
	is_livello[5] = is_liv_5
	else
	is_livello[5] = "N"
end if
for ll_b = 1 to 5
	ls_controllo[ll_b] = "N"
next

is_controllo = "N"

for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'D' then
		ls_controllo[1]  = "D"
	end if		
next
for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'E' then
		ls_controllo[2]  = "E"
	end if		
next
for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'R' then
		ls_controllo[3]= "R"
	end if		
next
for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'C' then
		ls_controllo[4] = "C"
	end if		
next

for ll_b = 1 to il_max_livello 
	if is_livello[ll_b] = 'A' then
		ls_controllo[5] = "A"
	end if		
next

for ll_b = 1 to 5
	if ls_controllo[ll_b] = "N" then
		if ll_b = 1 then is_controllo = "D"
		if ll_b = 2 then is_controllo = "E"
		if ll_b = 3 then is_controllo = "R"
		if ll_b = 4 then is_controllo = "C"
		if ll_b = 5 then is_controllo = "A"
		
	end if
next
ls_sql+= "," + uof_sql_where(ls_sql, is_controllo)

destroy ids_budget
ids_budget = create datastore
ids_budget.Dataobject = 'd_ds_budget_manut_totale_excel'
ids_budget.SetTransObject(sqlca)

ll_ret = ids_budget.SetSQLSelect(ls_sql)
if ll_ret < 0 then
	g_mb.messagebox("APICE", "Errore nella retrieve del datastore ids_budget", StopSign!)
	return -1
end if

il_num_righe = ids_budget.retrieve()
if il_num_righe <0 then
	g_mb.messagebox("APICE", "Errore nella retrieve del datastore ids_budget", StopSign!)
	return -1
end if
il_x = 1
il_xbudget = 1
il_cont = 1

ll_c = 1
il_y = il_riga_iniziale

do while il_xbudget <= il_num_righe
	uof_tree_totale()
loop

if il_riga_finale < il_riga_iniziale then il_riga_finale = il_riga_iniziale + 1

return il_riga_finale  
end function

public subroutine uof_tree_totale ();string ls_cod_divisione, ls_des_divisione, ls_cod_area_aziendale, ls_cod_reparto, ls_cod_cat_attrezzature, ls_cod_attrezzatura, &
		 ls_des_area_aziendale, ls_des_reparto, ls_des_cat_attrezzature, ls_des_attrezzatura

long   ll_controllo, ll_i, ll_foglio

ll_foglio = 1
il_periodo = 0

//elimino le righe che sono il  dettaglio 
if il_max_livello < 5 then
	if  not isnull(ids_budget.getitemstring(il_xbudget, "cod_divisione")) and not isnull(ids_budget.getitemstring(il_xbudget, "cod_area_aziendale")) and &
		not isnull(ids_budget.getitemstring(il_xbudget, "cod_reparto")) and not isnull(ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature")) and &
		not isnull(ids_budget.getitemstring(il_xbudget, "cod_attrezzaturA")) then
		 //if il_num_righe = il_xbudget and il_controllo = 0 then
		//		il_riga_finale = il_y 
		//end if
		il_xbudget++
		return
	end if	
end if

ll_i = 1

if il_cont > il_max_livello then 
	
	if il_cont > 5 then 
		il_cont = 5 
		return
	end if
	do while il_cont >0
		if il_x <= 0 then il_x = 1
		if il_cont <= 0 then
			il_cont = 1
			il_x = 1
			return
		end if
		CHOOSE CASE is_livello[il_cont]
			CASE "D"
				if ids_budget.getitemstring(il_xbudget, "cod_divisione") <> ids_budget.getitemstring(il_xbudget - 1, "cod_divisione") or isnull(ids_budget.getitemstring(il_xbudget, "cod_divisione"))	then			
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
				else						
					il_cont ++
					il_x ++
					if il_cont>5 then
						il_cont =5
						il_x =5
					end if
					return
				end if
			CASE "E"
				if ids_budget.getitemstring(il_xbudget, "cod_area_aziendale") <> ids_budget.getitemstring(il_xbudget - 1, "cod_area_aziendale") or isnull(ids_budget.getitemstring(il_xbudget, "cod_area_aziendale"))	then
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
				else
					il_cont ++
					il_x ++
					if il_cont>5 then
						il_cont =5
						il_x =5
					end if
					return
				end if
			CASE "R"
				if ids_budget.getitemstring(il_xbudget, "cod_reparto") <> ids_budget.getitemstring(il_xbudget - 1, "cod_reparto") or isnull(ids_budget.getitemstring(il_xbudget, "cod_reparto"))	then
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
				else
					il_cont ++
					il_x ++
					if il_cont>5 then
						il_cont =5
						il_x =5
					end if
					return
				end if
			CASE "C"
				if ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature") <> ids_budget.getitemstring(il_xbudget - 1, "cod_cat_attrezzature") or isnull(ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature"))	then
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
				else
					il_cont ++
					il_x ++
					if il_cont>5 then
						il_cont =5
						il_x =5
					end if
					return
				end if
			CASE "A"
				if ids_budget.getitemstring(il_xbudget, "cod_attrezzatura") <> ids_budget.getitemstring(il_xbudget - 1, "cod_attrezzatura") or isnull(ids_budget.getitemstring(il_xbudget, "cod_attrezzatura"))	then
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
				else
					il_cont ++
					il_x ++
					if il_cont>5 then
						il_cont =5
						il_x =5
					end if
					return
				end if
				CASE "N"
					il_cont --
					il_x --
					if il_cont <= 0 then
						il_cont = 1
						il_x = 1
						return
					end if
					continue
		end choose		
	loop
	return
end if

//if il_num_righe = il_xbudget  and il_periodo_scelto = 0 then
if  il_periodo_scelto = 0 then	
	il_riga_finale = il_y + 1
end if

if il_x = 1  and il_periodo  = il_periodo_scelto  and il_periodo_scelto = 0 then
	il_tot_parziali[upperbound(il_tot_parziali[])+1] = il_y
end if
il_controllo = 0
uof_stile_riga_periodi(il_y, il_colonna, ll_foglio)

CHOOSE CASE is_livello[il_cont]
	CASE "D"
		ls_cod_divisione = ids_budget.getitemstring(il_xbudget, "cod_divisione")
		select des_divisione
		into   :ls_des_divisione
		from   anag_divisioni
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_divisione = :ls_cod_divisione;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca delle divisioni!"+ sqlca.sqlerrtext)
			return
		end if
				
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x).value = ls_cod_divisione
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x+1).value = ls_des_divisione

	CASE "E"
		ls_cod_area_aziendale = ids_budget.getitemstring(il_xbudget, "cod_area_aziendale")

		select des_area
		into :ls_des_area_aziendale
		from tab_aree_aziendali
		where cod_azienda = :s_cs_xx.cod_azienda and cod_area_aziendale = :ls_cod_area_aziendale;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca delle aree aziendali!"+ sqlca.sqlerrtext)
			return
		end if
				
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x).value = ls_cod_area_aziendale
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x+1).value = ls_des_area_aziendale
	CASE "R"
		ls_cod_reparto = ids_budget.getitemstring(il_xbudget, "cod_reparto")

		select des_reparto
		into :ls_des_reparto
		from anag_reparti
		where cod_azienda = :s_cs_xx.cod_azienda and cod_reparto = :ls_cod_reparto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca dei reparti!"+ sqlca.sqlerrtext)
			return
		end if
				
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x).value = ls_cod_reparto
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x+1).value = ls_des_reparto
	CASE "C"
		ls_cod_cat_attrezzature = ids_budget.getitemstring(il_xbudget, "cod_cat_attrezzature")

		select des_cat_attrezzature
		into :ls_des_cat_attrezzature
		from tab_cat_attrezzature
		where cod_azienda = :s_cs_xx.cod_azienda and cod_cat_attrezzature = :ls_cod_cat_attrezzature;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca delle categorie attrezzature!"+ sqlca.sqlerrtext)
			return
		end if
				
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x).value = ls_cod_cat_attrezzature
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x+1).value = ls_des_cat_attrezzature
	CASE "A"
			ls_cod_attrezzatura = ids_budget.getitemstring(il_xbudget, "cod_attrezzatura")

		select descrizione
		into :ls_des_attrezzatura
		from anag_attrezzature
		where cod_azienda = :s_cs_xx.cod_azienda and cod_attrezzatura = :ls_cod_attrezzatura;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore nella ricerca del codice attrezzatura!"+ sqlca.sqlerrtext)
			return
		end if
				
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x).value = ls_cod_attrezzatura
		myoleobject.worksheets(ll_foglio).cells(il_y, il_x+1).value = ls_des_attrezzatura
	CASE ELSE
		g_mb.messagebox("OMNIA","Si è verificato un errore tipo non trovato!")
		return 
END CHOOSE

myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna -1).value = ids_budget.getitemnumber(il_xbudget, "num_copie_attrezzatura")
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_int")
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+1).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_est")
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+2).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_ricambi")
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+3).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_int_str")
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+4).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_risorse_est_str")
myoleobject.worksheets(ll_foglio).cells(il_y, il_colonna+5).value = ids_budget.getitemnumber(il_xbudget, "costo_prev_ricambi_str")

il_y++
il_x++
il_xbudget++
il_cont++

return
end subroutine

public subroutine uof_stile_riga_periodi (long fl_riga, long fl_col, long fl_foglio);myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col - 1).borders(1).linestyle = "1"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col - 1).borders(1).Weight = "4"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col).borders(1).linestyle = "1"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col).borders(1).Weight = "4"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col+3).borders(1).linestyle = "1"
//myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col+6).borders(1).linestyle = "9"
//myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col+9).borders(1).linestyle = "1"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col+5).borders(2).linestyle = "1"
myoleobject.worksheets(fl_foglio).cells(fl_riga,fl_col+5).borders(2).Weight = "4"
myoleobject.worksheets(fl_foglio).rows(fl_riga).borders(4).linestyle = "2"
myoleobject.worksheets(fl_foglio).rows(fl_riga).font.size = "8"
//myoleobject.sheets(fl_foglio).cells(fl_riga, fl_col+11).numberformat="#.##0,00;[Rosso]-#.##0,00"

myoleobject.worksheets(fl_foglio).range(string(uof_nome_cella(fl_riga,fl_col))+":"+string(uof_nome_cella(fl_riga, fl_col+11))).numberformat="#.##0,00;[Rosso]-#.##0,00"

//myoleobject.worksheets(fl_foglio).cells(fl_riga,1).font.size = "8"
end subroutine

on uo_budget_manut_excel.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_budget_manut_excel.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

