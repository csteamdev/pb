﻿$PBExportHeader$uo_budget_manutenzioni.sru
$PBExportComments$UO Elaborazioni e calcolo Budget
forward
global type uo_budget_manutenzioni from nonvisualobject
end type
end forward

global type uo_budget_manutenzioni from nonvisualobject
end type
global uo_budget_manutenzioni uo_budget_manutenzioni

forward prototypes
public function datetime uof_calcolo_p_s (datetime fdt_data_registrazione, string fs_periodicita, integer fd_frequenza)
public function datetime uof_prossima_scadenza (datetime fdt_data_registrazione, date fd_da, date fd_a, string fs_periodicita, integer fd_frequenza)
end prototypes

public function datetime uof_calcolo_p_s (datetime fdt_data_registrazione, string fs_periodicita, integer fd_frequenza);long li_aggiunta, ll_comodo_tempo
integer li_gg
time lt_prossima
datetime fdt_risposta_data

if fs_periodicita = 'M' then           //minuti
	li_aggiunta = fd_frequenza * 60
	if li_aggiunta > 86400 then
		li_gg = int(li_aggiunta / 86400)
		li_aggiunta = li_aggiunta - (li_gg * 86400) 
	else
		li_gg = 0
	end if		
	
	ll_comodo_tempo = secondsafter(time(fdt_data_registrazione),time("23:59:59"))
	if ll_comodo_tempo < li_aggiunta then
		li_gg = li_gg + 1
		li_aggiunta = li_aggiunta - ll_comodo_tempo
		lt_prossima = RelativeTime(time("00:00:00"), li_aggiunta)
		fdt_risposta_data = datetime(relativedate(date(fdt_data_registrazione), li_gg), lt_prossima)
	else
		lt_prossima = RelativeTime(time(fdt_data_registrazione), li_aggiunta)
		fdt_risposta_data = datetime(relativedate(date(fdt_data_registrazione), li_gg), lt_prossima)
	end if
end if

if fs_periodicita = 'O' then           //ore
	li_aggiunta = fd_frequenza * 3600
	if li_aggiunta > 86400 then
		li_gg = int(li_aggiunta / 86400)
		li_aggiunta = li_aggiunta - (li_gg * 86400) 
	else
		li_gg = 0
	end if			
	
	ll_comodo_tempo = secondsafter(time(fdt_data_registrazione),time("23:59:59"))
	if ll_comodo_tempo < li_aggiunta then
		li_gg = li_gg + 1
		li_aggiunta = li_aggiunta - ll_comodo_tempo
		lt_prossima = RelativeTime(time("00:00:00"), li_aggiunta)
		fdt_risposta_data = datetime(relativedate(date(fdt_data_registrazione), li_gg), lt_prossima)
	else
		lt_prossima = RelativeTime(time(fdt_data_registrazione), li_aggiunta)
		fdt_risposta_data = datetime(relativedate(date(fdt_data_registrazione), li_gg), lt_prossima)
	end if
end if

if fs_periodicita = 'S' then
	li_gg = fd_frequenza * 7
	fdt_risposta_data = datetime(relativedate(date(fdt_data_registrazione), li_gg), time(fdt_data_registrazione))
end if	
	
if fs_periodicita = 'E' then
	li_gg = fd_frequenza * 30
	fdt_risposta_data = datetime(relativedate(date(fdt_data_registrazione), li_gg), time(fdt_data_registrazione))
end if	

if fs_periodicita = 'G' then
	fdt_risposta_data = datetime(relativedate(date(fdt_data_registrazione), fd_frequenza), time(fdt_data_registrazione))	
end if

return fdt_risposta_data
end function

public function datetime uof_prossima_scadenza (datetime fdt_data_registrazione, date fd_da, date fd_a, string fs_periodicita, integer fd_frequenza);datetime ldt_data_att, ldt_risposta_data
long ll_i, ldt_data_att_n, fd_da_n, fd_a_n
string l_g, l_m, l_a

ldt_data_att=fdt_data_registrazione

l_g = mid(string(ldt_data_att), 1, 2)
l_m = mid(string(ldt_data_att), 4, 2)
if mid(string(ldt_data_att), 7, 2) > "50" then
	l_a = string(19) + mid(string(ldt_data_att), 7, 2)
else
	l_a = string(20) + mid(string(ldt_data_att), 7, 2)
end if
ldt_data_att_n = long(l_a + l_m + l_g)
//ldt_data_att_n
l_g = mid(string(fd_da), 1, 2)
l_m = mid(string(fd_da), 4, 2)
if mid(string(fd_da), 7, 2) > "50" then
	l_a = string(19) + mid(string(fd_da), 7, 2)
else
	l_a = string(20) + mid(string(fd_da), 7, 2)
end if
fd_da_n = long(l_a + l_m + l_g)
//fd_da_n
do while ldt_data_att_n < fd_da_n
	ldt_data_att=this.uof_calcolo_p_s(ldt_data_att,fs_periodicita,fd_frequenza)
	
	l_g = mid(string(ldt_data_att), 1, 2)
	l_m = mid(string(ldt_data_att), 4, 2)
	if mid(string(ldt_data_att), 7, 2) > "50" then
		l_a = string(19) + mid(string(ldt_data_att), 7, 2)
	else
		l_a = string(20) + mid(string(ldt_data_att), 7, 2)
	end if
	ldt_data_att_n = long(l_a + l_m + l_g)
	//ldt_data_att_n
loop

ldt_risposta_data = ldt_data_att

return ldt_risposta_data




//ll_i=1
//l_g = mid(string(ldt_data_att), 1, 2)
//l_m = mid(string(ldt_data_att), 4, 2)
//if mid(string(ldt_data_att), 7, 2) > "50" then
//	l_a = "19" + mid(string(ldt_data_att), 7, 2)
//else
//	l_a = "20" + mid(string(ldt_data_att), 7, 2)
//end if
//ldt_data_att_n = long(l_a + l_m + l_g)
////ldt_data_att_n
//l_g = mid(string(fd_a), 1, 2)
//l_m = mid(string(fd_a), 4, 2)
//if mid(string(fd_a), 7, 2) > "50" then
//	l_a = "19" + mid(string(fd_a), 7, 2)
//else
//	l_a = "20" + mid(string(fd_a), 7, 2)
//end if
//fd_a_n = long(l_a + l_m + l_g)
//fd_a_n
//do while ldt_data_att_n < fd_a_n
//	fdt_scadenze[ll_i]=ldt_data_att
//	ldt_data_att=this.uof_prossima_scadenza(ldt_data_att,fs_periodicita,fd_frequenza)
//	ll_i=ll_i+1
//	
//	l_g = mid(string(ldt_data_att), 1, 2)
//	l_m = mid(string(ldt_data_att), 4, 2)
//	if mid(string(ldt_data_att), 7, 2) > "50" then
//		l_a = "19" + mid(string(ldt_data_att), 7, 2)
//	else
//		l_a = "20" + mid(string(ldt_data_att), 7, 2)
//	end if
//	ldt_data_att_n = long(l_a + l_m + l_g)	
//	//ldt_data_att_n
//loop
end function

on uo_budget_manutenzioni.create
TriggerEvent( this, "constructor" )
end on

on uo_budget_manutenzioni.destroy
TriggerEvent( this, "destructor" )
end on

