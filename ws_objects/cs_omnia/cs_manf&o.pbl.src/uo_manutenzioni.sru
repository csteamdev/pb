﻿$PBExportHeader$uo_manutenzioni.sru
$PBExportComments$OserObject Manutenzioni
forward
global type uo_manutenzioni from nonvisualobject
end type
end forward

global type uo_manutenzioni from nonvisualobject
end type
global uo_manutenzioni uo_manutenzioni

type variables
boolean ib_global_service=false

// questa stabilisce se far apparire o no una messagebox di conferma;
// in alcuni punti non serve chiedere conferma;
// di default la lascio a TRUE come da funzionamneto tradizionale
boolean ib_messagebox=true
end variables

forward prototypes
public function integer uof_mod_codice_attr (string as_cod_attrezzatura, string as_cod_nuovo, ref string as_messaggio)
public function integer uof_scarica_magazzino (long fn_anno_registrazione, long fn_num_registrazione, ref string fs_messaggio)
public function integer uof_mod_tipo_manut (string as_cod_attrezzatura, string as_cod_tipo_manut, string as_cod_nuovo, string as_des_nuovo, boolean ab_blocco, ref string as_messaggio)
public function integer uof_crea_prossimo_intervento (long fn_anno_registrazione, long fn_num_registrazione, ref string fs_messaggio)
public subroutine uof_scadenze_periodo (datetime fdt_data_registrazione, date fd_da, date fd_a, string fs_periodicita, double fd_frequenza, ref datetime fdt_scadenze[])
public function integer uof_crea_nuova_scadenza_quiet (long fn_anno_registrazione, long fn_num_registrazione, ref string fs_messaggio)
public function integer uof_crea_nuova_scadenza (long fn_anno_registrazione, long fn_num_registrazione, ref string fs_messaggio)
public function datetime uof_prossima_scadenza (datetime fdt_data_registrazione, string fs_periodicita, double fd_frequenza)
public function integer uof_crea_manutenzione_programmata (long fn_anno_programma, long fn_num_programma, ref string fs_messaggio)
public function integer uof_crea_manutenzione_programmata_periodo (long fn_anno_programma, long fn_num_programma, datetime fdt_data_registrazione_manut, ref string fs_messaggio)
public function integer uof_crea_operatore_risorse (string fs_cod_tipo_manutenzione, string fs_cod_attrezzatura, long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio)
public function integer uof_crea_operatore_risorse_programma (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio)
public function integer uof_calcola_manutenzione (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_errore)
public function integer uof_calcola_manutenzione_fasi (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo_fase, ref string fs_errore)
public function integer uof_aggiorna_richieste (long fl_anno_richiesta, long fl_num_richiesta, ref string fs_errore)
public subroutine uof_scadenze_periodo (datetime fdt_data_registrazione, date fd_da, date fd_a, long fl_inizio_1, long fl_fine_1, long fl_inizio_2, long fl_fine_2, string fs_periodicita, double fd_frequenza, ref datetime fdt_scadenze[])
public function boolean uof_esiste_scadenza (datetime fdt_scadenze[], datetime fdt_data)
public function datetime uof_scadenza_semplice_con_intervalli (datetime fdt_data_registrazione, long fl_inizio_1, long fl_fine_1, long fl_inizio_2, long fl_fine_2)
public function integer uof_crea_manut_progr_semplice (long fn_anno_programma, long fn_num_programma, datetime fdt_data_registrazione, ref string fs_messaggio)
public function integer uof_crea_manut_progr_semplice2 (long fn_anno_programma, long fn_num_programma, datetime fdt_data_registrazione, ref string fs_messaggio)
public function integer uof_crea_manutenzione_programmata_sintex (long fn_anno_programma, long fn_num_programma, datetime fdt_data_registrazione, ref string fs_messaggio)
private function integer uof_calcola_manutenzioni_global (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_errore)
public function integer uof_calcola_ore_minuti (datetime adt_data_da, datetime adt_data_a, datetime adt_ora_da, datetime adt_ora_a, ref long al_ore, ref long al_minuti)
end prototypes

public function integer uof_mod_codice_attr (string as_cod_attrezzatura, string as_cod_nuovo, ref string as_messaggio);// Controllo validità ed esistenza del codice attrezzatura specificato nei parametri

if isnull(as_cod_attrezzatura) or len(trim(as_cod_attrezzatura)) < 1 then
	as_messaggio = "Specificare un codice attrezzatura"
	return -100
end if

select cod_attrezzatura
into   :as_messaggio
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in controllo esistenza attrezzatura: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(as_messaggio) then
	as_messaggio = "L'attrezzatura specificata non esiste in anagrafica attrezzature"
	return -100
end if

// Controllo validità ed esistenza del nuovo codice specificato nei parametri

if isnull(as_cod_nuovo) or len(trim(as_cod_nuovo)) < 1 then
	as_messaggio = "Specificare un nuovo codice"
	return -100
end if

select cod_attrezzatura
into   :as_messaggio
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_nuovo;
		 
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in controllo esistenza nuovo codice: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode <> 100 and not isnull(as_messaggio) then
	as_messaggio = "Il nuovo codice specificato è già utilizzato in anagrafica attrezzature"
	return -100
end if

// Inserimento attrezzatura con nuovo codice

insert
into   anag_attrezzature  
		 (cod_azienda,   
		 cod_attrezzatura,   
		 cod_cat_attrezzature,   
		 descrizione,   
		 fabbricante,   
		 modello,   
		 num_matricola,   
       data_acquisto,   
       periodicita_revisione,   
       unita_tempo,   
       utilizzo_scadenza,   
       cod_reparto,   
       certificata_fabbricante,   
       certificata_ce,   
       cod_inventario,   
       costo_orario_medio,   
       cod_prodotto,   
       cod_area_aziendale,   
       cod_utente,   
       reperibilita,   
       grado_precisione,   
       grado_precisione_um,   
       num_certificato_ente,   
       data_certificato_ente,   
       denominazione_ente,   
       qualita_attrezzatura,   
       note_esterne,   
       cod_centro_costo,   
       flag_primario,   
       flag_blocco,   
       data_blocco,   
       cod_versione,   
       note,   
       data_prima_attivazione,   
       data_installazione,   
       flag_reg_man,   
       flag_manuale,   
       flag_duplicazione,   
       flag_conta_ore,   
       costo_acquisto,   
       freq_guasti,   
       accessibilita,   
       disp_ricamnbi,   
       stato_attuale )  
select cod_azienda,   
       :as_cod_nuovo,   
       cod_cat_attrezzature,   
       descrizione,   
       fabbricante,   
       modello,   
       num_matricola,   
       data_acquisto,   
       periodicita_revisione,   
       unita_tempo,   
       utilizzo_scadenza,   
       cod_reparto,   
       certificata_fabbricante,   
       certificata_ce,   
       cod_inventario,   
       costo_orario_medio,   
       cod_prodotto,   
       cod_area_aziendale,   
       cod_utente,   
       reperibilita,   
       grado_precisione,   
       grado_precisione_um,   
       num_certificato_ente,   
       data_certificato_ente,   
       denominazione_ente,   
       qualita_attrezzatura,
		 null,
       cod_centro_costo,   
       flag_primario,   
       flag_blocco,   
       data_blocco,   
       cod_versione,   
       note,   
       data_prima_attivazione,   
       data_installazione,   
       flag_reg_man,   
       flag_manuale,   
       flag_duplicazione,   
       flag_conta_ore,   
       costo_acquisto,   
       freq_guasti,   
       accessibilita,   
       disp_ricamnbi,   
       stato_attuale  
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in inserimento nuovo codice in anag_attrezzature: " + sqlca.sqlerrtext
	return -1
end if

// Duplicazione tipologie di manutenzione

insert
into   tab_tipi_manutenzione  
       (cod_azienda,   
       cod_attrezzatura,   
       cod_tipo_manutenzione,   
       des_tipo_manutenzione,   
       tempo_previsto,   
       flag_manutenzione,   
       flag_ricambio_codificato,   
       cod_ricambio,   
       cod_ricambio_alternativo,   
       des_ricambio_non_codificato,   
       num_reg_lista,   
       num_versione,   
       num_edizione,   
       modalita_esecuzione,   
       cod_tipo_ord_acq,   
       cod_tipo_off_acq,   
       cod_tipo_det_acq,   
       cod_tipo_movimento,   
       periodicita,   
       frequenza,   
       quan_ricambio,   
       costo_unitario_ricambio,   
       cod_primario,   
       cod_misura,   
       val_min_taratura,   
       val_max_taratura,   
       valore_atteso,   
       cod_versione)  
select cod_azienda,   
       :as_cod_nuovo,   
       cod_tipo_manutenzione,   
       des_tipo_manutenzione,   
       tempo_previsto,   
       flag_manutenzione,   
       flag_ricambio_codificato,   
       cod_ricambio,   
       cod_ricambio_alternativo,   
       des_ricambio_non_codificato,   
       num_reg_lista,   
       num_versione,   
       num_edizione,   
       modalita_esecuzione,   
       cod_tipo_ord_acq,   
       cod_tipo_off_acq,   
       cod_tipo_det_acq,   
       cod_tipo_movimento,   
       periodicita,   
       frequenza,   
       quan_ricambio,   
       costo_unitario_ricambio,   
       cod_primario,   
       cod_misura,   
       val_min_taratura,   
       val_max_taratura,   
       valore_atteso,   
       cod_versione  
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in duplicazione tipologie di manutenzione: " + sqlca.sqlerrtext
	return -1
end if

insert into tab_guasti
( cod_azienda,   
  cod_attrezzatura,   
  cod_guasto,   
  des_guasto,   
  cod_tipo_manutenzione,   
  punteggio_guasto,   
  note,   
  cod_divisione,   
  cod_area_aziendale  )
	SELECT 
		cod_azienda,   
		:as_cod_nuovo,   
		cod_guasto,   
		des_guasto,   
		cod_tipo_manutenzione,   
		punteggio_guasto,   
		note,   
		cod_divisione,   
		cod_area_aziendale
  	FROM 	tab_guasti  
	where  	cod_azienda = :s_cs_xx.cod_azienda and
				cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in duplicazione tipologie di manutenzione: " + sqlca.sqlerrtext
	return -1
end if


// Aggiornamento riferimenti al codice attrezzaura da cambiare nelle tabelle referenziate

update budget_manutenzioni
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in aggiornamento tabella budget_manutenzioni: " + sqlca.sqlerrtext
	return -1
end if

update contratti_manut_attr
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in aggiornamento tabella contratti_manut_attr: " + sqlca.sqlerrtext
	return -1
end if

update det_attrezzature
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in aggiornamento tabella det_attrezzature: " + sqlca.sqlerrtext
	return -1
end if

update det_cal_attivita
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in aggiornamento tabella det_cal_attivita: " + sqlca.sqlerrtext
	return -1
end if

update det_mod_risorse_progetto
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in aggiornamento tabella det_mod_risorse_progetto: " + sqlca.sqlerrtext
	return -1
end if

update det_risorse_progetto
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in aggiornamento tabella det_risorse_progetto: " + sqlca.sqlerrtext
	return -1
end if

update difetti_det_orari_in
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in aggiornamento tabella difetti_det_orari_in: " + sqlca.sqlerrtext
	return -1
end if

update difetti_det_orari_out
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in aggiornamento tabella difetti_det_orari_out: " + sqlca.sqlerrtext
	return -1
end if

update fermi_macchina
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in aggiornamento tabella fermi_macchina: " + sqlca.sqlerrtext
	return -1
end if

update manutenzioni
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella manutenzioni (cod_attrezzatura): " + sqlca.sqlerrtext
	return -1
end if

update manutenzioni
set    cod_primario = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_primario = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella manutenzioni (cod_primario): " + sqlca.sqlerrtext
	return -1
end if

update manutenzioni_simulate
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella manutenzioni_simulate: " + sqlca.sqlerrtext
	return -1
end if

update non_conformita
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in aggiornamento tabella non_conformita: " + sqlca.sqlerrtext
	return -1
end if

update programmi_manutenzione
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella programmi_manutenzione (cod_attrezzatura): " + sqlca.sqlerrtext
	return -1
end if

update programmi_manutenzione
set    cod_primario = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_primario = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella programmi_manutenzione (cod_primario): " + sqlca.sqlerrtext
	return -1
end if

update registro_uso_attrezzature
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella registro_uso_attrezzature: " + sqlca.sqlerrtext
	return -1
end if

update tab_cal_attivita
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella tab_cal_attivita: " + sqlca.sqlerrtext
	return -1
end if

/*
update tab_guasti
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella tab_guasti: " + sqlca.sqlerrtext
	return -1
end if
*/

update tab_test_prodotti
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella tab_test_prodotti: " + sqlca.sqlerrtext
	return -1
end if

update temp_mov_magazzino
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella temp_mov_magazzino: " + sqlca.sqlerrtext
	return -1
end if

update tes_analisi_stat
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella tes_analisi_stat: " + sqlca.sqlerrtext
	return -1
end if

update tes_procedure_lavoro
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella tes_procedure_lavoro: " + sqlca.sqlerrtext
	return -1
end if

update det_tipi_manutenzioni
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella det_tipi_manutenzioni: " + sqlca.sqlerrtext
	return -1
end if

update elenco_risorse
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella elenco_risorse: " + sqlca.sqlerrtext
	return -1
end if

update prog_man_ordinati
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella prog_man_ordinati: " + sqlca.sqlerrtext
	return -1
end if

update richieste
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella richieste: " + sqlca.sqlerrtext
	return -1
end if

update simulazione_manutenzioni
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella simulazione_manutenzioni: " + sqlca.sqlerrtext
	return -1
end if

update tipi_manutenzioni_ricambi
set    cod_attrezzatura = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella tipi_manutenzioni_ricambi: " + sqlca.sqlerrtext
	return -1
end if

delete
from   tab_guasti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella tab_tipi_manutenzione (cod_attrezzatura): " + sqlca.sqlerrtext
	return -1
end if


delete
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella tab_tipi_manutenzione (cod_attrezzatura): " + sqlca.sqlerrtext
	return -1
end if

update tab_tipi_manutenzione
set    cod_primario = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_primario = :as_cod_attrezzatura;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella tab_tipi_manutenzione (cod_primario): " + sqlca.sqlerrtext
	return -1
end if

// Cancellazione dell'attrezzatura specificata

delete
from  anag_attrezzature
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_attrezzatura = :as_cod_attrezzatura;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in cancellazione dell'attrezzatura " + as_cod_attrezzatura + ": " + sqlca.sqlerrtext
	return -1
end if

datetime ldt_today

string ls_messaggio

ls_messaggio = "Codice Attrezzatura " + as_cod_attrezzatura + " sostituito con " + as_cod_nuovo

ldt_today = datetime(today(),now())

insert
into   log_sistema
		 (data_registrazione,
		 ora_registrazione,
		 utente,
		 flag_tipo_log,
		 messaggio,
		 db_sqlcode,
		 db_sqlerrtext)
values (:ldt_today,
		 :ldt_today,
		 :s_cs_xx.cod_utente,
		 '1',
		 :ls_messaggio,
		 0,
		 null);

return 0
end function

public function integer uof_scarica_magazzino (long fn_anno_registrazione, long fn_num_registrazione, ref string fs_messaggio);string						ls_cod_tipo_movimento, ls_cod_deposito[],ls_cod_ubicazione[],ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[], &
								ls_referenza,ls_cod_attrezzatura,ls_cod_tipo_manutenzione,ls_flag_ricambio_codificato,ls_cod_ricambio, ls_sql
								
datastore					lds_data
								
long							ll_anno_reg_mov_mag, ll_num_reg_mov_mag,ll_prog_stock[],ll_anno_reg_dest_stock,ll_num_reg_dest_stock, ll_num_documento, &
								ll_anno_reg_mov[], ll_num_reg_mov[], ll_prog_ricambio, ll_index, ll_tot
								
dec{4}						ld_quan_ricambio,ld_costo_unitario_ricambio, ld_prezzo_ricambio

datetime					ldt_data_stock[],ldt_data_documento,ldt_data_registrazione

uo_magazzino			luo_mag

integer						li_ret



select		data_registrazione,   
				cod_attrezzatura,
				cod_tipo_manutenzione,   
				flag_ricambio_codificato,   
				cod_ricambio,   
				quan_ricambio,   
				costo_unitario_ricambio,   
				anno_reg_mov_mag,   
				num_reg_mov_mag  
 into		:ldt_data_registrazione,   
			:ls_cod_attrezzatura,
			:ls_cod_tipo_manutenzione,   
			:ls_flag_ricambio_codificato,   
			:ls_cod_ricambio,   
			:ld_quan_ricambio,   
			:ld_costo_unitario_ricambio,   
			:ll_anno_reg_mov_mag,   
			:ll_num_reg_mov_mag  
from manutenzioni  
where	cod_azienda = :s_cs_xx.cod_azienda AND  
			anno_registrazione = :fn_anno_registrazione AND  
			num_registrazione = :fn_num_registrazione ;
		
if sqlca.sqlcode = 100 then
	fs_messaggio = "Manutenzione cercata inesistente"
	return -1
	
elseif sqlca.sqlcode < 0 then
	fs_messaggio = "Si è verificato un errore in SELECT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
	return -1
	
end if

//if ll_anno_reg_mov_mag <> 0 or not isnull(ll_anno_reg_mov_mag) then
//	// esiste già un movimento di magazzino associato: lo elimino e faccio quello giusto
//end if


if ls_flag_ricambio_codificato = "S" then
	
	//declare ricambi cursor for
	//select   cod_prodotto,
	//			quan_utilizzo,
	//			prezzo_ricambio,
	//			prog_riga_ricambio
	//from     manutenzioni_ricambi
	//where    cod_azienda = :s_cs_xx.cod_azienda and
	//			anno_registrazione = :fn_anno_registrazione and
	//			num_registrazione = :fn_num_registrazione and
	//			flag_utilizzo = 'S'
	//order by prog_riga_ricambio;
					 
	

	select	cod_tipo_movimento
	into		:ls_cod_tipo_movimento  
	from   tab_tipi_manutenzione
	where	cod_azienda = :s_cs_xx.cod_azienda and  
				cod_attrezzatura = :ls_cod_attrezzatura and  
				cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			 
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Tipo manutenzione " + ls_cod_tipo_manutenzione + " inesistente per l'attrezzatura " + ls_cod_attrezzatura
		return -1
		
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Errore in lettura tipo movimento da tipi manutenzioni: " + sqlca.sqlerrtext
		return -1
		
	end if
	
	if not isnull(ls_cod_tipo_movimento) then
		
		ls_sql = "select   cod_prodotto,"+&
						"quan_utilizzo,"+&
						"prezzo_ricambio,"+&
						"prog_riga_ricambio "+&
			"from     manutenzioni_ricambi "+&
			"where    cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"anno_registrazione="+string(fn_anno_registrazione)+" and "+&
						"num_registrazione="+string(fn_num_registrazione)+" and "+&
						"flag_utilizzo='S' "+&
			"order by prog_riga_ricambio"
		
		ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, fs_messaggio)
		if ll_tot<0 then
			fs_messaggio = "Errore creazione datastore dei ricambi: " + fs_messaggio
			return -1
		end if
		
		//open ricambi;
//		if sqlca.sqlcode <> 0 then
//			g_mb.messagebox("OMNIA","Errore nella open del cursore ricambi: " + sqlca.sqlerrtext)
//			return -1
//		end if
		
		//do while true
		for ll_index=1 to ll_tot
			ls_cod_ricambio			= lds_data.getitemstring(ll_index, 1)
			ld_quan_ricambio		= lds_data.getitemnumber(ll_index, 2)
			ld_prezzo_ricambio		= lds_data.getitemnumber(ll_index, 3)
			ll_prog_ricambio			= lds_data.getitemnumber(ll_index, 4)
			
			if ls_cod_ricambio="" or isnull(ls_cod_ricambio) then continue
			if isnull(ld_quan_ricambio) then ld_quan_ricambio = 0
			if isnull(ld_prezzo_ricambio) then ld_prezzo_ricambio = 0
			
//			fetch ricambi
//			into  :ls_cod_ricambio,
//					:ld_quan_ricambio,
//					:ld_prezzo_ricambio,
//					:ll_prog_ricambio;
//					
//			if sqlca.sqlcode < 0 then
//				g_mb.messagebox("OMNIA","Errore nella fetch del cursore ricambi: " + sqlca.sqlerrtext)
//				close ricambi;
//				return -1
//			elseif sqlca.sqlcode = 100 then
//				close ricambi;
//				exit
//			end if
		
			//if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
			if f_crea_dest_mov_mag (ls_cod_tipo_movimento, &
													ls_cod_ricambio, &
													ls_cod_deposito[], &
													ls_cod_ubicazione[], &
													ls_cod_lotto[], &
													ldt_data_stock[], &
													ll_prog_stock[], &
													ls_cod_cliente[], &
													ls_cod_fornitore[], &
													ll_anno_reg_dest_stock, &
													ll_num_reg_dest_stock, &
													fs_messaggio) = -1 then
				//fs_messaggio = "Errore durante la creazione destinazione stock"
				destroy lds_data
				rollback;
				return -1
			end if
			
			if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
											 ll_num_reg_dest_stock, &
											 ls_cod_tipo_movimento, &
											 ls_cod_ricambio) = -1 then
				fs_messaggio = "Errore durante la verifica destinazione movimenti"
				rollback;
				destroy lds_data
				return -1
			end if
			
			luo_mag = create uo_magazzino
			li_ret =  luo_mag.uof_movimenti_mag ( ldt_data_registrazione, &
										ls_cod_tipo_movimento, &
										"S", &
										ls_cod_ricambio, &
										ld_quan_ricambio, &
										ld_costo_unitario_ricambio, &
										ll_num_documento, &
										ldt_data_documento, &
										ls_referenza, &
										ll_anno_reg_dest_stock, &
										ll_num_reg_dest_stock, &
										ls_cod_deposito[], &
										ls_cod_ubicazione[], &
										ls_cod_lotto[], &
										ldt_data_stock[], &
										ll_prog_stock[], &
										ls_cod_fornitore[], &
										ls_cod_cliente[], &
										ll_anno_reg_mov[], &
										ll_num_reg_mov[], &
										fs_messaggio)
			destroy luo_mag
			
			if li_ret< 0 then
				fs_messaggio = "Errore creazione movimento: " + fs_messaggio
				destroy lds_data
				rollback;
				return -1
			end if
					
			update manutenzioni_ricambi
			set    anno_reg_mov_mag = :ll_anno_reg_mov[1], 
					 num_reg_mov_mag = :ll_num_reg_mov[1]
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :fn_anno_registrazione AND  
					 num_registrazione  = :fn_num_registrazione and
					 prog_riga_ricambio = :ll_prog_ricambio;
			
			if sqlca.sqlcode < 0 then
				destroy lds_data
				fs_messaggio = "Errore aggiornamento numero movimento in tabella manutenzioni-ricambi: " + sqlca.sqlerrtext
				rollback;
				return -1
			end if
			
			f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, ll_num_reg_dest_stock)
			
		//loop
		next
			
	end if
	
end if

return 0
end function

public function integer uof_mod_tipo_manut (string as_cod_attrezzatura, string as_cod_tipo_manut, string as_cod_nuovo, string as_des_nuovo, boolean ab_blocco, ref string as_messaggio);// Controllo validità ed esistenza del codice attrezzatura specificato nei parametri

if isnull(as_cod_attrezzatura) or len(trim(as_cod_attrezzatura)) < 1 then
	as_messaggio = "Specificare un codice attrezzatura"
	return -100
end if

select cod_attrezzatura
into   :as_messaggio
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura;
		 
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in controllo esistenza attrezzatura: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(as_messaggio) then
	as_messaggio = "L'attrezzatura specificata non esiste in anagrafica attrezzature"
	return -100
end if

// Controllo validità ed esistenza del tipo manutenzione specificato nei parametri

if isnull(as_cod_tipo_manut) or len(trim(as_cod_tipo_manut)) < 1 then
	as_messaggio = "Specificare un tipo manutenzione"
	return -100
end if

select cod_tipo_manutenzione
into   :as_messaggio
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_tipo_manut;
		 
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in controllo esistenza tipo manutenzione: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(as_messaggio) then
	as_messaggio = "Il tipo manutenzione specificato non esiste in tipologie manutenzione"
	return -100
end if

// Controllo validità ed esistenza del nuovo codice specificato nei parametri

if isnull(as_cod_nuovo) or len(trim(as_cod_nuovo)) < 1 then
	as_messaggio = "Specificare un nuovo codice"
	return -100
end if

select cod_tipo_manutenzione
into   :as_messaggio
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_nuovo;
		 
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in controllo esistenza nuovo codice: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode <> 100 and not isnull(as_messaggio) then
	as_messaggio = "Il nuovo codice specificato è già utilizzato in tipologie manutenzione"
	return -100
end if

// Inserimento tipo manutenzione con nuovo codice

insert
into   tab_tipi_manutenzione  
       (cod_azienda,   
       cod_attrezzatura,   
       cod_tipo_manutenzione,   
       des_tipo_manutenzione,   
       tempo_previsto,   
       flag_manutenzione,   
       flag_ricambio_codificato,   
       cod_ricambio,   
       cod_ricambio_alternativo,   
       des_ricambio_non_codificato,   
       num_reg_lista,   
       num_versione,   
       num_edizione,   
       modalita_esecuzione,   
       cod_tipo_ord_acq,   
       cod_tipo_off_acq,   
       cod_tipo_det_acq,   
       cod_tipo_movimento,   
       periodicita,   
       frequenza,   
       quan_ricambio,   
       costo_unitario_ricambio,   
       cod_primario,   
       cod_misura,   
       val_min_taratura,   
       val_max_taratura,   
       valore_atteso,   
       cod_versione)  
select cod_azienda,   
       cod_attrezzatura,   
       :as_cod_nuovo,   
       :as_des_nuovo,   
       tempo_previsto,   
       flag_manutenzione,   
       flag_ricambio_codificato,   
       cod_ricambio,   
       cod_ricambio_alternativo,   
       des_ricambio_non_codificato,   
       num_reg_lista,   
       num_versione,   
       num_edizione,   
       modalita_esecuzione,   
       cod_tipo_ord_acq,   
       cod_tipo_off_acq,   
       cod_tipo_det_acq,   
       cod_tipo_movimento,   
       periodicita,   
       frequenza,   
       quan_ricambio,   
       costo_unitario_ricambio,   
       cod_primario,   
       cod_misura,   
       val_min_taratura,   
       val_max_taratura,   
       valore_atteso,   
       cod_versione  
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_tipo_manut;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in inserimento nuovo tipo manutenzione: " + sqlca.sqlerrtext
	return -1
end if

// Blocco vecchia tipologia

if ab_blocco then
	
	update tab_tipi_manutenzione
	set 	 flag_blocco = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :as_cod_attrezzatura and
			 cod_tipo_manutenzione = :as_cod_tipo_manut;
	
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore in blocco vecchia tipologia manutenzione: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

// Duplicazione ricambi tipo manutenzione

insert
into   tipi_manutenzioni_ricambi
		 (cod_azienda,
		 cod_attrezzatura,
		 cod_tipo_manutenzione,
		 prog_riga_ricambio,
		 cod_prodotto,
		 des_prodotto,
		 quan_utilizzo,
		 prezzo_ricambio)
select cod_azienda,
		 cod_attrezzatura,
		 :as_cod_nuovo,
		 prog_riga_ricambio,
		 cod_prodotto,
		 des_prodotto,
		 quan_utilizzo,
		 prezzo_ricambio
from   tipi_manutenzioni_ricambi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_tipo_manut;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in duplicazione ricambi tipo manutenzione: " + sqlca.sqlerrtext
	return -1
end if

// Aggiornamento riferimenti al codice tipo manutenzione da cambiare nelle tabelle referenziate

update elenco_risorse
set    cod_tipo_manutenzione = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_tipo_manut;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella elenco_risorse: " + sqlca.sqlerrtext
	return -1
end if

update manutenzioni_simulate
set    cod_tipo_manutenzione = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_tipo_manut;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella manutenzioni_simulate: " + sqlca.sqlerrtext
	return -1
end if

update prog_man_ordinati
set    cod_tipo_manutenzione = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_tipo_manut;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella prog_man_ordinati: " + sqlca.sqlerrtext
	return -1
end if

update programmi_manutenzione
set    cod_tipo_manutenzione = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_tipo_manut;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella programmi_manutenzione: " + sqlca.sqlerrtext
	return -1
end if

update simulazione_manutenzioni
set    cod_tipo_manutenzione = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_tipo_manut;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella simulazione_manutenzioni: " + sqlca.sqlerrtext
	return -1
end if

update tab_guasti
set    cod_tipo_manutenzione = :as_cod_nuovo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_tipo_manut;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento tabella tab_guasti: " + sqlca.sqlerrtext
	return -1
end if

datetime ldt_today

string 	ls_messaggio

ldt_today = datetime(today(),now())

ls_messaggio = "Attrezzatura " + as_cod_attrezzatura + ": tipo manutenzione " + as_cod_tipo_manut + &
					" sostituito con " + as_cod_nuovo

insert
into   log_sistema
		 (data_registrazione,
		 ora_registrazione,
		 utente,
		 flag_tipo_log,
		 messaggio,
		 db_sqlcode,
		 db_sqlerrtext)
values (:ldt_today,
		 :ldt_today,
		 :s_cs_xx.cod_utente,
		 '1',
		 :ls_messaggio,
		 0,
		 null);

return 0
end function

public function integer uof_crea_prossimo_intervento (long fn_anno_registrazione, long fn_num_registrazione, ref string fs_messaggio);string ls_null, ls_des_intervento, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_tipo_intervento, ls_note_idl, &
       ls_flag_ricambio_codificato, ls_cod_kit_ricambio, ls_ricambio_non_codificato, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
		 ls_cod_risorsa_esterna, ls_rif_contratto_assistenza, ls_rif_ordine_acq_risorsa, ls_cod_primario, ls_cod_misura, &
		 ls_cod_guasto, ls_cod_versione, ls_ricambi[], ls_des_prodotto, ls_cod_ricambio, ls_des_ricambio, ls_blocco
long ll_anno_registrazione, ll_num_registrazione, ll_null, ll_operaio_ore, ll_operaio_minuti, &
     ll_risorsa_ore, ll_risorsa_minuti, ll_anno_contratto_assistenza, ll_num_contratto_assistenza, &
	  ll_anno_ord_acq_risorsa, ll_num_ord_acq_risorsa, ll_prog_riga_ord_acq_risorsa, ll_num_reg_lista, ll_prog_liste_con_comp, &
	  ll_num_versione, ll_num_edizione, ll_cont, ll_anno_reg_programma, ll_num_reg_programma, ll_j, ll_prog_ricambio
dec{4} ld_quan_ricambio, ld_costo_unitario_ricambio, ld_costo_orario_operaio, ld_risorsa_costo_orario, &
       ld_risorsa_costi_aggiuntivi,  ld_valore_min_taratura, ld_valore_max_taratura, ld_valore_atteso, &
		 ld_tot_costo_intervento, ld_quantita[], ld_prezzo, ld_prezzo_ricambio
datetime ldt_data_registrazione, ldt_data_prossimo_intervento


if ib_messagebox then
	if g_mb.messagebox("Manutenzioni & Tarature","Creo il prossimo intervento ?",Question!,YesNo!,2) = 2 then
		fs_messaggio = ""
		return 0
	end if
end if

ll_anno_registrazione = f_anno_esercizio()
select max(num_registrazione)
into   :ll_num_registrazione
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione;
if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

setnull(ldt_data_registrazione)

select     data_prossimo_intervento,   
           cod_guasto,   
           anno_reg_programma,   
           num_reg_programma,
           des_intervento,   
           cod_attrezzatura,   
           cod_tipo_manutenzione,   
           flag_tipo_intervento,   
           note_idl,   
           flag_ricambio_codificato,   
           cod_ricambio,
			  cod_versione,
           ricambio_non_codificato,   
           quan_ricambio,   
           costo_unitario_ricambio,   
           cod_operaio,   
           costo_orario_operaio,   
           operaio_ore,   
           operaio_minuti,   
           cod_cat_risorse_esterne,   
           cod_risorsa_esterna,   
           risorsa_ore,   
           risorsa_minuti,   
           risorsa_costo_orario,   
           risorsa_costi_aggiuntivi,   
           anno_contratto_assistenza,   
           num_contratto_assistenza,   
           anno_ord_acq_risorsa,   
           num_ord_acq_risorsa,   
           prog_riga_ord_acq_risorsa,   
           rif_contratto_assistenza,   
           rif_ordine_acq_risorsa,   
           cod_primario,   
           cod_misura,   
           valore_min_taratura,   
           valore_max_taratura,   
           valore_atteso,   
           tot_costo_intervento,   
           num_reg_lista,   
           prog_liste_con_comp,   
			  num_versione,   
           num_edizione
into       :ldt_data_prossimo_intervento,   
			  :ls_cod_guasto,
           :ll_anno_reg_programma,   
           :ll_num_reg_programma,
           :ls_des_intervento,   
           :ls_cod_attrezzatura,   
           :ls_cod_tipo_manutenzione,   
           :ls_flag_tipo_intervento,   
           :ls_note_idl,   
           :ls_flag_ricambio_codificato,   
           :ls_cod_kit_ricambio,
			  :ls_cod_versione,
           :ls_ricambio_non_codificato,   
           :ld_quan_ricambio,   
           :ld_costo_unitario_ricambio,   
           :ls_cod_operaio,   
           :ld_costo_orario_operaio,   
           :ll_operaio_ore,   
           :ll_operaio_minuti,   
           :ls_cod_cat_risorse_esterne,   
           :ls_cod_risorsa_esterna,   
           :ll_risorsa_ore,   
           :ll_risorsa_minuti,   
           :ld_risorsa_costo_orario,   
           :ld_risorsa_costi_aggiuntivi,   
           :ll_anno_contratto_assistenza,   
           :ll_num_contratto_assistenza,   
           :ll_anno_ord_acq_risorsa,   
           :ll_num_ord_acq_risorsa,   
           :ll_prog_riga_ord_acq_risorsa,   
           :ls_rif_contratto_assistenza,   
           :ls_rif_ordine_acq_risorsa,   
           :ls_cod_primario,   
           :ls_cod_misura,   
           :ld_valore_min_taratura,   
           :ld_valore_max_taratura,   
           :ld_valore_atteso,   
           :ld_tot_costo_intervento,   
           :ll_num_reg_lista,   
           :ll_prog_liste_con_comp,   
			  :ll_num_versione,   
           :ll_num_edizione
from       manutenzioni
where      cod_azienda = :s_cs_xx.cod_azienda and
      	  anno_registrazione = :fn_anno_registrazione and
		     num_registrazione = :fn_num_registrazione;
			  
// Modifica Michele per gestione blocco tipologie di manutenzione 01/04/2003

select flag_blocco
into   :ls_blocco
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in lettura dati tipologia manutenzione: " + sqlca.sqlerrtext
	return -1
elseif ls_blocco = "S" then
	fs_messaggio = "Questa tipologia di manutenzione è stata bloccata: operazione interrotta"
	return -1
end if

// Fine Modifica

insert into manutenzioni  
          (cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           data_registrazione,   
           cod_guasto,   
           flag_ordinario,   
           flag_eseguito,   
           anno_non_conf,   
           num_non_conf,   
           data_prossimo_intervento,   
           flag_budget,   
           anno_reg_programma,   
           num_reg_programma,
           des_intervento,   
           cod_attrezzatura,   
           cod_tipo_manutenzione,   
           flag_tipo_intervento,   
           note_idl,   
           flag_ricambio_codificato,   
           cod_ricambio,
			  cod_versione,
           ricambio_non_codificato,   
           quan_ricambio,   
           costo_unitario_ricambio,   
           cod_operaio,   
           costo_orario_operaio,   
           operaio_ore,   
           operaio_minuti,   
           cod_cat_risorse_esterne,   
           cod_risorsa_esterna,   
           risorsa_ore,   
           risorsa_minuti,   
           risorsa_costo_orario,   
           risorsa_costi_aggiuntivi,   
           anno_contratto_assistenza,   
           num_contratto_assistenza,   
           anno_ord_acq_risorsa,   
           num_ord_acq_risorsa,   
           prog_riga_ord_acq_risorsa,   
           rif_contratto_assistenza,   
           rif_ordine_acq_risorsa,   
           cod_primario,   
           cod_misura,   
           valore_min_taratura,   
           valore_max_taratura,   
           valore_atteso,   
           tot_costo_intervento,   
           num_reg_lista,   
           prog_liste_con_comp,   
           num_versione,   
           num_edizione	  )  
values  (  :s_cs_xx.cod_azienda,
			  :ll_anno_registrazione,
			  :ll_num_registrazione,
			  :ldt_data_prossimo_intervento,
			  :ls_cod_guasto,
			  'S',
			  'N',
			  null,
			  null,
			  null,
			  'N',
			  :ll_anno_reg_programma,
			  :ll_num_reg_programma,
			  :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_flag_tipo_intervento,   
			  :ls_note_idl,   
			  :ls_flag_ricambio_codificato,   
			  :ls_cod_kit_ricambio,
			  :ls_cod_versione,
			  :ls_ricambio_non_codificato,   
			  :ld_quan_ricambio,   
			  :ld_costo_unitario_ricambio,   
			  :ls_cod_operaio,   
			  :ld_costo_orario_operaio,   
			  :ll_operaio_ore,   
			  :ll_operaio_minuti,   
			  :ls_cod_cat_risorse_esterne,   
			  :ls_cod_risorsa_esterna,   
			  :ll_risorsa_ore,   
			  :ll_risorsa_minuti,   
			  :ld_risorsa_costo_orario,   
			  :ld_risorsa_costi_aggiuntivi,   
			  :ll_anno_contratto_assistenza,   
			  :ll_num_contratto_assistenza,   
			  :ll_anno_ord_acq_risorsa,   
			  :ll_num_ord_acq_risorsa,   
			  :ll_prog_riga_ord_acq_risorsa,   
			  :ls_rif_contratto_assistenza,   
			  :ls_rif_ordine_acq_risorsa,   
			  :ls_cod_primario,   
			  :ls_cod_misura,   
			  :ld_valore_min_taratura,   
			  :ld_valore_max_taratura,   
			  :ld_valore_atteso,   
			  :ld_tot_costo_intervento,   
			  :ll_num_reg_lista,   
			  null,   
			  :ll_num_versione,   
			  :ll_num_edizione           );
if sqlca.sqlcode = 100 then
	fs_messaggio = "Errore in inserimento manutenzione programmata" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
	return -1
elseif sqlca.sqlcode < 0 then
	fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
	return -1
end if

// *** michela 02/11/2004: inserimento di operatore e risorse nella manutenzione in base a quelli del programma di 
//     manutenzione

long ll_ret 

ll_ret = uof_crea_operatore_risorse_programma(ll_anno_registrazione, ll_num_registrazione, fs_messaggio)

if ll_ret <> 0 then return ll_ret

/////////////////////////////////////////// inserimento ricambi ////////////////////////////////////////////////////////

string ls_messaggio

uo_ricambi luo_ricambi

luo_ricambi = create uo_ricambi

if luo_ricambi.uof_ricambi_manutenzione(ll_anno_registrazione,ll_num_registrazione,ls_messaggio) <> 0 then
	g_mb.messagebox("OMNIA",ls_messaggio)
	destroy luo_ricambi
	return -1
end if

destroy luo_ricambi

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return 0
end function

public subroutine uof_scadenze_periodo (datetime fdt_data_registrazione, date fd_da, date fd_a, string fs_periodicita, double fd_frequenza, ref datetime fdt_scadenze[]);datetime ldt_data_att
long ll_i

ldt_data_att=fdt_data_registrazione

do while ldt_data_att<datetime(fd_da)
	ldt_data_att=this.uof_prossima_scadenza(ldt_data_att,fs_periodicita,fd_frequenza)
loop
ll_i=1
do while ldt_data_att<=datetime(fd_a,time("23:59:59"))
	fdt_scadenze[ll_i]=ldt_data_att
	ldt_data_att=this.uof_prossima_scadenza(ldt_data_att,fs_periodicita,fd_frequenza)
	ll_i=ll_i+1
loop
end subroutine

public function integer uof_crea_nuova_scadenza_quiet (long fn_anno_registrazione, long fn_num_registrazione, ref string fs_messaggio);string ls_null, ls_des_intervento, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_tipo_intervento, ls_note_idl, &
       ls_flag_ricambio_codificato, ls_cod_kit_ricambio, ls_ricambio_non_codificato, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
		 ls_cod_risorsa_esterna, ls_rif_contratto_assistenza, ls_rif_ordine_acq_risorsa, ls_cod_primario, ls_cod_misura, &
		 ls_flag_gest_manutenzione, ls_cod_versione, ls_ricambi[], ls_des_prodotto, ls_cod_ricambio, ls_des_ricambio, ls_blocco
long ll_anno_registrazione, ll_num_registrazione, ll_null, ll_operaio_ore_previste, ll_operaio_minuti_previsti, &
     ll_risorsa_ore_previste, ll_risorsa_minuti_previsti, ll_anno_contratto_assistenza, ll_num_contratto_assistenza, &
	  ll_anno_ord_acq_risorsa, ll_num_ord_acq_risorsa, ll_prog_riga_ord_acq_risorsa, ll_num_reg_lista, ll_prog_liste_con_comp, &
	  ll_num_versione, ll_num_edizione, ll_cont, ll_anno_reg_programma, ll_num_reg_programma, ll_j, ll_prog_ricambio, ll_ret
dec{4} ld_quan_ricambio, ld_costo_unitario_ricambio, ld_costo_orario_operaio, ld_risorsa_costo_orario, &
       ld_risorsa_costi_aggiuntivi,  ld_valore_min_taratura, ld_valore_max_taratura, ld_valore_atteso, &
		 ld_tot_costo_intervento, ld_frequenza, ld_quantita[], ld_prezzo, ld_prezzo_ricambio
datetime ldt_data_registrazione, ldt_null
string ls_periodicita, ls_flag_fasi

//Donato 13/11/2008 variabili per gestione periodo
dec{0} ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2
string ls_flag_scadenze

setnull(ls_null)
setnull(ll_null)
setnull(ldt_null)

select flag_gest_manutenzione
  into :ls_flag_gest_manutenzione
  from parametri_manutenzioni
 where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Manutenzioni & Tarature", "Errore in lettura dati dalla tabella parametri_manutenzioni " + sqlca.sqlerrtext)
	return -1
end if
if ls_flag_gest_manutenzione = "S" then return 0
 
select anno_reg_programma, num_reg_programma, data_registrazione
into   :ll_anno_reg_programma, :ll_num_reg_programma, :ldt_data_registrazione
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fn_anno_registrazione and
       num_registrazione  = :fn_num_registrazione ;

if sqlca.sqlcode = 100 then
	fs_messaggio = "Manutenzione cercata inesistente"
	return -1
elseif sqlca.sqlcode < 0 then
	fs_messaggio = "Si è verificato un errore in SELECT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
	return -1
end if

select  des_intervento,   
		  cod_attrezzatura,   
		  cod_tipo_manutenzione,   
		  flag_tipo_intervento,   
		  note_idl,   
		  flag_ricambio_codificato,   
		  cod_ricambio,
		  cod_versione,
		  ricambio_non_codificato,   
		  quan_ricambio,   
		  costo_unitario_ricambio,   
		  cod_operaio,   
		  costo_orario_operaio,   
		  operaio_ore_previste,   
		  operaio_minuti_previsti,   
		  cod_cat_risorse_esterne,   
		  cod_risorsa_esterna,   
		  risorsa_ore_previste,   
		  risorsa_minuti_previsti,   
		  risorsa_costo_orario,   
		  risorsa_costi_aggiuntivi,   
		  anno_contratto_assistenza,   
		  num_contratto_assistenza,   
		  anno_ord_acq_risorsa,   
		  num_ord_acq_risorsa,   
		  prog_riga_ord_acq_risorsa,   
		  rif_contratto_assistenza,   
		  rif_ordine_acq_risorsa,   
		  cod_primario,   
		  cod_misura,   
		  valore_min_taratura,   
		  valore_max_taratura,   
		  valore_atteso,   
		  tot_costo_intervento,   
		  num_reg_lista,   
		  prog_liste_con_comp,   
		  num_versione,   
		  num_edizione,
		  periodicita,
		  frequenza
		  //Donato 13/11/2008
		  ,flag_scadenze
		  ,data_inizio_val_1
		  ,data_fine_val_1
		  ,data_inizio_val_2
		  ,data_fine_val_2
		  //--------------------------
into    :ls_des_intervento,   
		  :ls_cod_attrezzatura,   
		  :ls_cod_tipo_manutenzione,   
		  :ls_flag_tipo_intervento,   
		  :ls_note_idl,   
		  :ls_flag_ricambio_codificato,   
		  :ls_cod_kit_ricambio,
		  :ls_cod_versione,
		  :ls_ricambio_non_codificato,   
		  :ld_quan_ricambio,   
		  :ld_costo_unitario_ricambio,   
		  :ls_cod_operaio,   
		  :ld_costo_orario_operaio,   
		  :ll_operaio_ore_previste,   
		  :ll_operaio_minuti_previsti,   
		  :ls_cod_cat_risorse_esterne,   
		  :ls_cod_risorsa_esterna,   
		  :ll_risorsa_ore_previste,   
		  :ll_risorsa_minuti_previsti,   
		  :ld_risorsa_costo_orario,   
		  :ld_risorsa_costi_aggiuntivi,   
		  :ll_anno_contratto_assistenza,   
		  :ll_num_contratto_assistenza,   
		  :ll_anno_ord_acq_risorsa,   
		  :ll_num_ord_acq_risorsa,   
		  :ll_prog_riga_ord_acq_risorsa,   
		  :ls_rif_contratto_assistenza,   
		  :ls_rif_ordine_acq_risorsa,   
		  :ls_cod_primario,   
		  :ls_cod_misura,   
		  :ld_valore_min_taratura,   
		  :ld_valore_max_taratura,   
		  :ld_valore_atteso,   
		  :ld_tot_costo_intervento,   
		  :ll_num_reg_lista,   
		  :ll_prog_liste_con_comp,   
		  :ll_num_versione,   
		  :ll_num_edizione,
		  :ls_periodicita,
		  :ld_frequenza
		  //Donato 13/11/2008
		  ,:ls_flag_scadenze
		  ,:ld_data_inizio_val_1
		  ,:ld_data_fine_val_1
		  ,:ld_data_inizio_val_2
		  ,:ld_data_fine_val_2
		  //------------------------
from    programmi_manutenzione
where   cod_azienda = :s_cs_xx.cod_azienda and
		  anno_registrazione = :ll_anno_reg_programma and
		  num_registrazione  = :ll_num_reg_programma;
		  		
//----------------------------------- Modifica Nicola ----------------------------
if sqlca.sqlcode = 100 and &
	(ll_anno_reg_programma <> 0 and &
	ll_num_reg_programma <> 0) then
//-------------------------------------- Fine Modifica ---------------------------
				  
//if sqlca.sqlcode = 100 then
	fs_messaggio = "Errore in ricerca programma manutenzione " + string(ll_num_reg_programma) + "/" + string(ll_num_reg_programma)
	return -1
elseif sqlca.sqlcode < 0 then
	fs_messaggio = "Si è verificato un errore in SELECT tabella programmi manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
	return -1
end if

// Modifica Michele per gestione blocco tipologie di manutenzione 01/04/2003

select flag_blocco
into   :ls_blocco
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in lettura dati tipologia manutenzione: " + sqlca.sqlerrtext
	return -1
elseif ls_blocco = "S" then
	fs_messaggio = "Questa tipologia di manutenzione è stata bloccata: operazione interrotta"
	return -1
end if

// Fine Modifica

ll_anno_registrazione = f_anno_esercizio()
select max(num_registrazione)
into   :ll_num_registrazione
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione;
if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

ldt_data_registrazione = this.uof_prossima_scadenza ( ldt_data_registrazione, ls_periodicita, ld_frequenza )

//--------------------------------------

//----------------------------------- Modifica Nicola ----------------------------
if (ll_anno_reg_programma = 0 or isnull(ll_anno_reg_programma)) and &
   (ll_num_reg_programma = 0 or isnull(ll_num_reg_programma)) then

	select des_intervento,
			cod_attrezzatura,
			cod_tipo_manutenzione,
			flag_tipo_intervento,
			note_idl,
			flag_ricambio_codificato,
			cod_ricambio,
			cod_versione,
			ricambio_non_codificato,
			quan_ricambio,
			costo_unitario_ricambio,
			cod_operaio,
			costo_orario_operaio,
			operaio_ore,
			operaio_minuti,
			cod_cat_risorse_esterne, 
			cod_risorsa_esterna,
			risorsa_ore,
			risorsa_minuti,
			risorsa_costo_orario,
			risorsa_costi_aggiuntivi,
			anno_contratto_assistenza,
			num_contratto_assistenza,
			anno_ord_acq_risorsa,
			num_ord_acq_risorsa,
			prog_riga_ord_acq_risorsa,
			rif_contratto_assistenza,
			rif_ordine_acq_risorsa,
			cod_primario,
			cod_misura,
			valore_min_taratura,
			valore_max_taratura,
			valore_atteso,
			tot_costo_intervento,
			num_reg_lista,
			prog_liste_con_comp,
			num_versione,
			num_edizione
   into :ls_des_intervento,   
		  :ls_cod_attrezzatura,   
		  :ls_cod_tipo_manutenzione,   
		  :ls_flag_tipo_intervento,   
		  :ls_note_idl,   
		  :ls_flag_ricambio_codificato,   
		  :ls_cod_kit_ricambio,
		  :ls_cod_versione,
		  :ls_ricambio_non_codificato,   
		  :ld_quan_ricambio,   
		  :ld_costo_unitario_ricambio,   
		  :ls_cod_operaio,   
		  :ld_costo_orario_operaio,   
		  :ll_operaio_ore_previste,   
		  :ll_operaio_minuti_previsti,   
		  :ls_cod_cat_risorse_esterne,   
		  :ls_cod_risorsa_esterna,   
		  :ll_risorsa_ore_previste,   
		  :ll_risorsa_minuti_previsti,   
		  :ld_risorsa_costo_orario,   
		  :ld_risorsa_costi_aggiuntivi,   
		  :ll_anno_contratto_assistenza,   
		  :ll_num_contratto_assistenza,   
		  :ll_anno_ord_acq_risorsa,   
		  :ll_num_ord_acq_risorsa,   
		  :ll_prog_riga_ord_acq_risorsa,   
		  :ls_rif_contratto_assistenza,   
		  :ls_rif_ordine_acq_risorsa,   
		  :ls_cod_primario,   
		  :ls_cod_misura,   
		  :ld_valore_min_taratura,   
		  :ld_valore_max_taratura,   
		  :ld_valore_atteso,   
		  :ld_tot_costo_intervento,   
		  :ll_num_reg_lista,   
		  :ll_prog_liste_con_comp,   
		  :ll_num_versione,   
		  :ll_num_edizione
   from manutenzioni
  where cod_azienda = :s_cs_xx.cod_azienda and
		  anno_registrazione = :fn_anno_registrazione and
		  num_registrazione  = :fn_num_registrazione;
		  
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in ricerca manutenzione " + string(fn_num_registrazione) + "/" + string(fn_anno_registrazione)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in SELECT tabella manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
	
	//Donato 13/11/2008
	setnull(ld_data_inizio_val_1)
	setnull(ld_data_fine_val_1)
	setnull(ld_data_inizio_val_2)
	setnull(ld_data_fine_val_2)
	//-------------------------
end if	
//-------------------------------------- Fine Modifica ---------------------------

//Donato 13/11/2008 se specificati i due intervalli verificare se ricade in alcuni di essi (vedi specifica Integrazione_ORV)
int mese1_i, mese1_f, mese2_i, mese2_f
int giorno1_i, giorno1_f, giorno2_i, giorno2_f
int giorno_inizio, mese_inizio, giorno_fine, mese_fine
int mese_effettivo, anno_effettivo, giorno_effettivo
			 
date ldt_data_partenza, ldt_data_arrivo, ldt_appo
										
giorno1_i = ( ld_data_inizio_val_1 / 100)
mese1_i = mod( ld_data_inizio_val_1, 100)						
giorno1_f = ( ld_data_fine_val_1 / 100)
mese1_f = mod( ld_data_fine_val_1, 100)

giorno2_i = ( ld_data_inizio_val_2 / 100)
mese2_i = mod( ld_data_inizio_val_2, 100)										
giorno2_f = ( ld_data_fine_val_2 / 100)
mese2_f = mod( ld_data_fine_val_2, 100)

if not isnull(giorno1_i) and giorno1_i > 0 and not isnull(mese1_i) and mese1_i> 0 &
		and not isnull(giorno1_f) and giorno1_f > 0 and not isnull(mese1_f) and mese1_f> 0 &
		and not isnull(giorno2_i) and giorno2_i > 0 and not isnull(mese2_i) and mese2_i> 0  &
		and not isnull(giorno2_f) and giorno2_f > 0 and not isnull(mese2_f) and mese2_f> 0 &
		and ls_periodicita <> "O" and ls_periodicita <> "M" then
						
	//sono stati specificati tutti e due gli intervalli
	//e la periodicita non è "O" e neanche "M" (ore, minuti)
	
	//verificare che ldt_data_registrazione ricada negli intervalli (vedi specifica Integrazione_ORV)
	ldt_data_registrazione = uof_scadenza_semplice_con_intervalli(ldt_data_registrazione, &
																										ld_data_inizio_val_1,ld_data_fine_val_1, &
																										ld_data_inizio_val_2, ld_data_fine_val_2)
end if
//--------------------------------------------------------------------------------------

if ll_anno_reg_programma <> 0 and &
   ll_num_reg_programma <> 0 then

	insert into manutenzioni  
				  ( 
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_guasto,   
				  flag_ordinario,   
				  flag_eseguito,   
				  anno_non_conf,   
				  num_non_conf,   
				  data_prossimo_intervento,   
				  flag_budget,   
				  anno_reg_programma,   
				  num_reg_programma,
				  des_intervento,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  flag_tipo_intervento,   
				  note_idl,   
				  flag_ricambio_codificato,   
				  cod_ricambio,
				  cod_versione,
				  ricambio_non_codificato,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_operaio,   
				  costo_orario_operaio,   
				  operaio_ore,   
				  operaio_minuti,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  risorsa_ore,   
				  risorsa_minuti,   
				  risorsa_costo_orario,   
				  risorsa_costi_aggiuntivi,   
				  anno_contratto_assistenza,   
				  num_contratto_assistenza,   
				  anno_ord_acq_risorsa,   
				  num_ord_acq_risorsa,   
				  prog_riga_ord_acq_risorsa,   
				  rif_contratto_assistenza,   
				  rif_ordine_acq_risorsa,   
				  cod_primario,   
				  cod_misura,   
				  valore_min_taratura,   
				  valore_max_taratura,   
				  valore_atteso,   
				  tot_costo_intervento,   
				  num_reg_lista,   
				  prog_liste_con_comp,   
				  num_versione,   
				  num_edizione)  
				values
				  (
				  :s_cs_xx.cod_azienda,
				  :ll_anno_registrazione,
				  :ll_num_registrazione,
				  :ldt_data_registrazione,
				  :ls_null,
				  'S',
				  'N',
				  :ll_null,
				  :ll_null,
				  :ldt_null,
				  'N',
				  :ll_anno_reg_programma,
				  :ll_num_reg_programma,
				  :ls_des_intervento,   
				  :ls_cod_attrezzatura,   
				  :ls_cod_tipo_manutenzione,   
				  :ls_flag_tipo_intervento,   
				  :ls_note_idl,   
				  :ls_flag_ricambio_codificato,   
				  :ls_cod_kit_ricambio,
				  :ls_cod_versione,
				  :ls_ricambio_non_codificato,   
				  :ld_quan_ricambio,   
				  :ld_costo_unitario_ricambio,   
				  :ls_cod_operaio,   
				  :ld_costo_orario_operaio,   
				  :ll_operaio_ore_previste,   
				  :ll_operaio_minuti_previsti,   
				  :ls_cod_cat_risorse_esterne,   
				  :ls_cod_risorsa_esterna,   
				  :ll_risorsa_ore_previste,   
				  :ll_risorsa_minuti_previsti,   
				  :ld_risorsa_costo_orario,   
				  :ld_risorsa_costi_aggiuntivi,   
				  :ll_anno_contratto_assistenza,   
				  :ll_num_contratto_assistenza,   
				  :ll_anno_ord_acq_risorsa,   
				  :ll_num_ord_acq_risorsa,   
				  :ll_prog_riga_ord_acq_risorsa,   
				  :ls_rif_contratto_assistenza,   
				  :ls_rif_ordine_acq_risorsa,   
				  :ls_cod_primario,   
				  :ls_cod_misura,   
				  :ld_valore_min_taratura,   
				  :ld_valore_max_taratura,   
				  :ld_valore_atteso,   
				  :ld_tot_costo_intervento,   
				  :ll_num_reg_lista,   
				  :ll_prog_liste_con_comp,   
				  :ll_num_versione,   
				  :ll_num_edizione );
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in inserimento manutenzione programmata" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
		return -1
	
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
	
	s_cs_xx.parametri.parametro_ul_1 = ll_anno_registrazione
	s_cs_xx.parametri.parametro_ul_2 = ll_num_registrazione
	
	// *** inserimento di operatore e risorse esterne del programma della manutenzione. controllo il parametro CFL per vedere
	// *** se bisogna considerare le fasi di lavoro oppure no. se non esiste proprio il parametro allora vado avanti normalmente
	
	ll_ret = uof_crea_operatore_risorse_programma(ll_anno_registrazione, ll_num_registrazione, fs_messaggio)
		
	if ll_ret <> 0 then return ll_ret
	
	/////////////////////////////////////////// inserimento ricambi ////////////////////////////////////////////////////////

	string ls_messaggio
	
	uo_ricambi luo_ricambi
	
	luo_ricambi = create uo_ricambi
	
	if luo_ricambi.uof_ricambi_manutenzione(ll_anno_registrazione,ll_num_registrazione,ls_messaggio) <> 0 then
		g_mb.messagebox("OMNIA",ls_messaggio)
		destroy luo_ricambi
		return -1
	end if
	
	destroy luo_ricambi
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

end if
// --------------------------------- scarico del magazzino ricambi ---------------------------------------------- //
//if uof_scarica_magazzino(fn_anno_registrazione, fn_num_registrazione, fs_messaggio) <> 0 then
//	messagebox("OMNIA", fs_messaggio)
//	ROLLBACK;
//	return -1
//else
//	update manutenzioni
//	set    flag_eseguito = 'S'
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       anno_registrazione = :fn_anno_registrazione and
//			 num_registrazione = :fn_num_registrazione;
//	if sqlca.sqlcode <> 0 then
//		fs_messaggio = "Errore in aggiornamento flag_eseguito in tabella manutenzioni nella manutenzione" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
//		ROLLBACK;
//		return -1
//	end if
//	COMMIT;
//	return 0
//end if

return 0
end function

public function integer uof_crea_nuova_scadenza (long fn_anno_registrazione, long fn_num_registrazione, ref string fs_messaggio);string ls_controllo
//-----------------CLAUDIA 28/05/07 ----------------PER SINTEXCAL//
// AGGIUNTO CONTROLLO PER FARE IN MODO CHE SE NON SI VUOLE IL CONTROLLO SI IMPOSTA IL PARAMETRO OMNIA PRM A S
select flag
into :ls_controllo
from parametri_omnia 
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_parametro = 'PRM' and
		flag_parametro = 'F';
		
if sqlca.sqlcode < 0 then
		g_mb.messagebox("Manutenzioni & Tarature", "Errore durante il controllo di un paremtro PRM ." + sqlca.sqlerrtext)
		setpointer(arrow!)
		return -1
end if
if ls_controllo = 'S' then
else
	if ib_messagebox then
		if g_mb.messagebox("Manutenzioni & Tarature","Creo la nuova manutenzione programmata?",Question!,YesNo!,2) = 2 then
			fs_messaggio = ""
			return 0
		end if
	end if
end if

if uof_crea_nuova_scadenza_quiet(fn_anno_registrazione,fn_num_registrazione,fs_messaggio) <> 0 then
	return -1
end if

return 0
end function

public function datetime uof_prossima_scadenza (datetime fdt_data_registrazione, string fs_periodicita, double fd_frequenza);string 	ls_festivi

datetime ldt_prossima

time 		lt_reg, lt_prossima

long 		ll_gg, ll_freq, ll_giorno, ll_mese, ll_anno, ll_ultimo

boolean  lb_bisestile


choose case fs_periodicita
	case 'M'			// minuti
		ll_freq = fd_frequenza * 3600
	case 'O' 		// ore
		ll_freq = fd_frequenza * 60
	case 'S' 		// settimana
		ll_freq = fd_frequenza * 7
	case 'B' 		// bi-settimana
		ll_freq = fd_frequenza * 14
	case 'E' 		// mesi
		ll_freq = fd_frequenza
	case 'I'			// bimensile
		ll_freq = fd_frequenza * 2
	case 'T' 		// trimestrale
		ll_freq = fd_frequenza * 3
	case 'R' 		// semestre
		ll_freq = fd_frequenza * 6
	case 'A' 		// anno
		ll_freq = fd_frequenza * 12
	case else
		ll_freq = fd_frequenza
	end choose

if fs_periodicita='M' or fs_periodicita='O' then  // minuti/ore
	lt_prossima=time(fdt_data_registrazione)
	if secondsafter(time("23:59:59"),lt_prossima) < ll_freq then
		lt_prossima=relativetime(lt_prossima,ll_freq -secondsafter(time("23:59:59"),lt_prossima))
		ll_gg=1
	else
		ll_gg=0
		lt_prossima=relativetime(lt_prossima,ll_freq)
	end if
	ll_freq=ll_gg
end if

select flag_includi_festivi
into   :ls_festivi
from   parametri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 or isnull(ls_festivi) then
	ls_festivi = "N"
end if

ldt_prossima = fdt_data_registrazione

if (fs_periodicita = 'G' or fs_periodicita = 'S') or ll_gg = 1 then
	ldt_prossima=datetime(relativedate(date(ldt_prossima),ll_freq))
	choose case daynumber(date(ldt_prossima))
		case 1		// domenica
			if ls_festivi = "N" then
				ldt_prossima=datetime(relativedate(date(ldt_prossima),1))
			end if
		case 7		// sabato
			if ls_festivi = "N" then
				ldt_prossima=datetime(relativedate(date(ldt_prossima),2))
			end if
	end choose
	ldt_prossima=datetime(date(ldt_prossima),time(fdt_data_registrazione))
end if

if fs_periodicita = 'E' or fs_periodicita = "A" or fs_periodicita = "R" or fs_periodicita = "T" or fs_periodicita = "I" then
	
	ll_giorno = day(date(ldt_prossima))
	
	ll_mese = month(date(ldt_prossima)) + ll_freq
	
	ll_anno = year(date(ldt_prossima))
	
	if ll_mese > 12 and mod(ll_mese,12) > 0 then
		ll_anno = ll_anno + int(ll_mese/12)
		ll_mese = mod(ll_mese,12)
	end if
	
	if ll_mese > 12 and mod(ll_mese,12) = 0 then
		ll_anno = ll_anno + int(ll_mese/12) - 1
		ll_mese = 12
	end if

	choose case ll_mese
		case 4,6,9,11
			ll_ultimo = 30
		case 1,3,5,7,8,10,12
			ll_ultimo = 31
		case 2
			if mod(ll_anno,4) = 0 and mod(ll_anno,100) <> 0 or mod(ll_anno,400) = 0 then
				ll_ultimo = 29
			else
				ll_ultimo = 28
			end if
	end choose
	
	if ll_giorno > ll_ultimo then
		ll_giorno = ll_ultimo
	end if
	
	ldt_prossima = datetime(date(string(ll_giorno) + "/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
	
end if

//02/07/2010: aggiunto per sintexcal Periodicita ANNO ESATTO (valore X)
//in tal caso, a prescindere dai festivi la prossima scadenza è esattamente lo stesso giorno, lo stesso mese dell'anno successivo
//ESEMPI:
//fdt_data_registrazione=25/05/2010		fd_frequenza=2
//la nuova scadenze sarà 25/05/2012

//casi particolari:
// se la data fdt_data_registrazione 29/02/2008, la data deve ricadere di 28 febbraio

if fs_periodicita = "X" then
	ll_giorno = day(date(fdt_data_registrazione))
	ll_mese = month(date(fdt_data_registrazione))
	ll_anno = year(date(fdt_data_registrazione))
	
	//se sono in un bisestile tra 4 anni avro ancora un bisestile
	if ll_giorno=29 and ll_mese=2 and fd_frequenza<>4 then ll_giorno = 28
	
	//il mese non cambia
	
	//incrementa l'anno di un valore pari alla frequenza
	ll_anno += fd_frequenza
	
	ldt_prossima = datetime(date(ll_anno, ll_mese, ll_giorno), 00:00:00)
end if

//fine modifica -------------------------------------------------------------


return ldt_prossima
end function

public function integer uof_crea_manutenzione_programmata (long fn_anno_programma, long fn_num_programma, ref string fs_messaggio);string ls_null, ls_des_intervento, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_tipo_intervento, ls_note_idl, &
       ls_flag_ricambio_codificato, ls_cod_kit_ricambio, ls_ricambio_non_codificato, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
		 ls_cod_risorsa_esterna, ls_rif_contratto_assistenza, ls_rif_ordine_acq_risorsa, ls_cod_primario, ls_cod_misura, &
		 ls_cod_versione, ls_ricambi[], ls_des_prodotto, ls_cod_ricambio, ls_des_ricambio, ls_blocco, ls_flag_abr
long ll_anno_registrazione, ll_num_registrazione, ll_null, ll_operaio_ore_previste, ll_operaio_minuti_previsti, &
     ll_risorsa_ore_previste, ll_risorsa_minuti_previsti, ll_anno_contratto_assistenza, ll_num_contratto_assistenza, &
	  ll_anno_ord_acq_risorsa, ll_num_ord_acq_risorsa, ll_prog_riga_ord_acq_risorsa, ll_num_reg_lista, ll_prog_liste_con_comp, &
	  ll_num_versione, ll_num_edizione, LL_CONT, ll_j, ll_prog_ricambio
dec{4} ld_quan_ricambio, ld_costo_unitario_ricambio, ld_costo_orario_operaio, ld_risorsa_costo_orario, &
       ld_risorsa_costi_aggiuntivi,  ld_valore_min_taratura, ld_valore_max_taratura, ld_valore_atteso, ld_tot_costo_intervento, &
		 ld_quantita[], ld_prezzo, ld_prezzo_ricambio
datetime ldt_data_registrazione, ldt_null

dec{0}   ld_data_inizio_val_1, ld_data_inizio_val_2, ld_data_fine_val_1, ld_data_fine_val_2

setnull(ls_null)
setnull(ll_null)
setnull(ldt_null)

select count(*)
into   :ll_cont
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_reg_programma = :fn_anno_programma and
       num_reg_programma = :fn_num_programma and
		 flag_eseguito = 'N';
if ll_cont > 0 and not isnull(ll_cont) then
	if g_mb.messagebox("Manutenzioni & Tarature","Attenzione: con questo programma di manutenzione risultano esistere ancora manutenzioni non eseguite.~r~nProseguo lo stesso?",Question!,YesNo!,2) = 2 then
		fs_messaggio = ""
		return -1
	end if
end if		 
//*******claudia 27/11/06 se presente il parametro multiaziendale abr allora esiste il campo flag_visualizza intranet e lo imposta
//******modifica fatta per abor specifica stampi
select flag
into :ls_flag_abr
from parametri 
where cod_parametro = 'ABR' and
		flag_parametro = 'F';

//Donato 04-02-2009 modifica per evitare che dia errore quando non trova il parametro ABR
//che tra l'altro è una personalizzazione del cliente ABOR
if sqlca.sqlcode <> 0 then
	ls_flag_abr = "N"
end if
/*
if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore durante il controllo di un paremtro ABR ." + sqlca.sqlerrtext)
		setpointer(arrow!)
		return -1
end if
*/
//fine modifica ---------------------------------------------------------------------

if ls_flag_abr = 'S' then
	select  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  note_idl,   
			  flag_ricambio_codificato,   
			  cod_ricambio,
			  cod_versione,
			  ricambio_non_codificato,   
			  quan_ricambio,   
			  costo_unitario_ricambio,   
			  cod_operaio,   
			  costo_orario_operaio,   
			  operaio_ore_previste,   
			  operaio_minuti_previsti,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  risorsa_ore_previste,   
			  risorsa_minuti_previsti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi,   
			  anno_contratto_assistenza,   
			  num_contratto_assistenza,   
			  anno_ord_acq_risorsa,   
			  num_ord_acq_risorsa,   
			  prog_riga_ord_acq_risorsa,   
			  rif_contratto_assistenza,   
			  rif_ordine_acq_risorsa,   
			  cod_primario,   
			  cod_misura,   
			  valore_min_taratura,   
			  valore_max_taratura,   
			  valore_atteso,   
			  tot_costo_intervento,   
			  num_reg_lista,   
			  prog_liste_con_comp,   
			  num_versione,   
			  num_edizione,
			  data_inizio_val_1,
			  data_fine_val_1,
			  data_inizio_val_2,
			  data_fine_val_2, 
			  flag_pubblica_intranet
	into    :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_flag_tipo_intervento,   
			  :ls_note_idl,   
			  :ls_flag_ricambio_codificato,   
			  :ls_cod_kit_ricambio,
			  :ls_cod_versione,
			  :ls_ricambio_non_codificato,   
			  :ld_quan_ricambio,   
			  :ld_costo_unitario_ricambio,   
			  :ls_cod_operaio,   
			  :ld_costo_orario_operaio,   
			  :ll_operaio_ore_previste,   
			  :ll_operaio_minuti_previsti,   
			  :ls_cod_cat_risorse_esterne,   
			  :ls_cod_risorsa_esterna,   
			  :ll_risorsa_ore_previste,   
			  :ll_risorsa_minuti_previsti,   
			  :ld_risorsa_costo_orario,   
			  :ld_risorsa_costi_aggiuntivi,   
			  :ll_anno_contratto_assistenza,   
			  :ll_num_contratto_assistenza,   
			  :ll_anno_ord_acq_risorsa,   
			  :ll_num_ord_acq_risorsa,   
			  :ll_prog_riga_ord_acq_risorsa,   
			  :ls_rif_contratto_assistenza,   
			  :ls_rif_ordine_acq_risorsa,   
			  :ls_cod_primario,   
			  :ls_cod_misura,   
			  :ld_valore_min_taratura,   
			  :ld_valore_max_taratura,   
			  :ld_valore_atteso,   
			  :ld_tot_costo_intervento,   
			  :ll_num_reg_lista,   
			  :ll_prog_liste_con_comp,   
			  :ll_num_versione,   
			  :ll_num_edizione,
			  :ld_data_inizio_val_1,
			  :ld_data_fine_val_1,
			  :ld_data_inizio_val_2,
			  :ld_data_fine_val_2,
			  :ls_flag_abr
	from    programmi_manutenzione
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fn_anno_programma and
			  num_registrazione  = :fn_num_programma;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in ricerca programma manutenzione " + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in SELECT tabella programmi manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
else
		select  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  note_idl,   
			  flag_ricambio_codificato,   
			  cod_ricambio,
			  cod_versione,
			  ricambio_non_codificato,   
			  quan_ricambio,   
			  costo_unitario_ricambio,   
			  cod_operaio,   
			  costo_orario_operaio,   
			  operaio_ore_previste,   
			  operaio_minuti_previsti,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  risorsa_ore_previste,   
			  risorsa_minuti_previsti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi,   
			  anno_contratto_assistenza,   
			  num_contratto_assistenza,   
			  anno_ord_acq_risorsa,   
			  num_ord_acq_risorsa,   
			  prog_riga_ord_acq_risorsa,   
			  rif_contratto_assistenza,   
			  rif_ordine_acq_risorsa,   
			  cod_primario,   
			  cod_misura,   
			  valore_min_taratura,   
			  valore_max_taratura,   
			  valore_atteso,   
			  tot_costo_intervento,   
			  num_reg_lista,   
			  prog_liste_con_comp,   
			  num_versione,   
			  num_edizione,
			  data_inizio_val_1,
			  data_fine_val_1,
			  data_inizio_val_2,
			  data_fine_val_2
	into    :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_flag_tipo_intervento,   
			  :ls_note_idl,   
			  :ls_flag_ricambio_codificato,   
			  :ls_cod_kit_ricambio,
			  :ls_cod_versione,
			  :ls_ricambio_non_codificato,   
			  :ld_quan_ricambio,   
			  :ld_costo_unitario_ricambio,   
			  :ls_cod_operaio,   
			  :ld_costo_orario_operaio,   
			  :ll_operaio_ore_previste,   
			  :ll_operaio_minuti_previsti,   
			  :ls_cod_cat_risorse_esterne,   
			  :ls_cod_risorsa_esterna,   
			  :ll_risorsa_ore_previste,   
			  :ll_risorsa_minuti_previsti,   
			  :ld_risorsa_costo_orario,   
			  :ld_risorsa_costi_aggiuntivi,   
			  :ll_anno_contratto_assistenza,   
			  :ll_num_contratto_assistenza,   
			  :ll_anno_ord_acq_risorsa,   
			  :ll_num_ord_acq_risorsa,   
			  :ll_prog_riga_ord_acq_risorsa,   
			  :ls_rif_contratto_assistenza,   
			  :ls_rif_ordine_acq_risorsa,   
			  :ls_cod_primario,   
			  :ls_cod_misura,   
			  :ld_valore_min_taratura,   
			  :ld_valore_max_taratura,   
			  :ld_valore_atteso,   
			  :ld_tot_costo_intervento,   
			  :ll_num_reg_lista,   
			  :ll_prog_liste_con_comp,   
			  :ll_num_versione,   
			  :ll_num_edizione,
			  :ld_data_inizio_val_1,
			  :ld_data_fine_val_1,
			  :ld_data_inizio_val_2,
			  :ld_data_fine_val_2
	from    programmi_manutenzione
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fn_anno_programma and
			  num_registrazione  = :fn_num_programma;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in ricerca programma manutenzione " + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in SELECT tabella programmi manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

end if
// Modifica Michele per gestione blocco tipologie di manutenzione 01/04/2003

select flag_blocco
into   :ls_blocco
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in lettura dati tipologia manutenzione: " + sqlca.sqlerrtext
	return -1
elseif ls_blocco = "S" then
	fs_messaggio = g_str.format("La tipologia di manutenzione $1 è bloccata.", ls_cod_tipo_manutenzione)
	return -2
end if

// Fine Modifica

ll_anno_registrazione = f_anno_esercizio()
select max(num_registrazione)
into   :ll_num_registrazione
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione;
if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

ldt_data_registrazione = datetime(today(),00:00:00)
if ls_flag_abr = 'S' then
	insert into manutenzioni  
				  ( 
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_guasto,   
				  flag_ordinario,   
				  flag_eseguito,   
				  anno_non_conf,   
				  num_non_conf,   
				  data_prossimo_intervento,   
				  flag_budget,   
				  anno_reg_programma,   
				  num_reg_programma,
				  des_intervento,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  flag_tipo_intervento,   
				  note_idl,   
				  flag_ricambio_codificato,   
				  cod_ricambio,
				  cod_versione,
				  ricambio_non_codificato,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_operaio,   
				  costo_orario_operaio,   
				  operaio_ore,   
				  operaio_minuti,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  risorsa_ore,   
				  risorsa_minuti,   
				  risorsa_costo_orario,   
				  risorsa_costi_aggiuntivi,   
				  anno_contratto_assistenza,   
				  num_contratto_assistenza,   
				  anno_ord_acq_risorsa,   
				  num_ord_acq_risorsa,   
				  prog_riga_ord_acq_risorsa,   
				  rif_contratto_assistenza,   
				  rif_ordine_acq_risorsa,   
				  cod_primario,   
				  cod_misura,   
				  valore_min_taratura,   
				  valore_max_taratura,   
				  valore_atteso,   
				  tot_costo_intervento,   
				  num_reg_lista,   
				  prog_liste_con_comp,   
				  num_versione,   
				  num_edizione,
				  flag_pubblica_intranet
				  )  
				values
				  (
				  :s_cs_xx.cod_azienda,
				  :ll_anno_registrazione,
				  :ll_num_registrazione,
				  :ldt_data_registrazione,
				  :ls_null,
				  'S',
				  'N',
				  :ll_null,
				  :ll_null,
				  :ldt_null,
				  'N',
				  :fn_anno_programma,
				  :fn_num_programma,
				  :ls_des_intervento,   
				  :ls_cod_attrezzatura,   
				  :ls_cod_tipo_manutenzione,   
				  :ls_flag_tipo_intervento,   
				  :ls_note_idl,   
				  :ls_flag_ricambio_codificato,   
				  :ls_cod_kit_ricambio,
				  :ls_cod_versione,
				  :ls_ricambio_non_codificato,   
				  :ld_quan_ricambio,   
				  :ld_costo_unitario_ricambio,   
				  :ls_cod_operaio,   
				  :ld_costo_orario_operaio,   
				  :ll_operaio_ore_previste,   
				  :ll_operaio_minuti_previsti,   
				  :ls_cod_cat_risorse_esterne,   
				  :ls_cod_risorsa_esterna,   
				  :ll_risorsa_ore_previste,   
				  :ll_risorsa_minuti_previsti,   
				  :ld_risorsa_costo_orario,   
				  :ld_risorsa_costi_aggiuntivi,   
				  :ll_anno_contratto_assistenza,   
				  :ll_num_contratto_assistenza,   
				  :ll_anno_ord_acq_risorsa,   
				  :ll_num_ord_acq_risorsa,   
				  :ll_prog_riga_ord_acq_risorsa,   
				  :ls_rif_contratto_assistenza,   
				  :ls_rif_ordine_acq_risorsa,   
				  :ls_cod_primario,   
				  :ls_cod_misura,   
				  :ld_valore_min_taratura,   
				  :ld_valore_max_taratura,   
				  :ld_valore_atteso,   
				  :ld_tot_costo_intervento,   
				  :ll_num_reg_lista,   
				  :ll_prog_liste_con_comp,   
				  :ll_num_versione,   
				  :ll_num_edizione,
				  :ls_flag_abr
				  );
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in inserimento manutenzione programmata" + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
else
		insert into manutenzioni  
				  ( 
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_guasto,   
				  flag_ordinario,   
				  flag_eseguito,   
				  anno_non_conf,   
				  num_non_conf,   
				  data_prossimo_intervento,   
				  flag_budget,   
				  anno_reg_programma,   
				  num_reg_programma,
				  des_intervento,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  flag_tipo_intervento,   
				  note_idl,   
				  flag_ricambio_codificato,   
				  cod_ricambio,
				  cod_versione,
				  ricambio_non_codificato,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_operaio,   
				  costo_orario_operaio,   
				  operaio_ore,   
				  operaio_minuti,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  risorsa_ore,   
				  risorsa_minuti,   
				  risorsa_costo_orario,   
				  risorsa_costi_aggiuntivi,   
				  anno_contratto_assistenza,   
				  num_contratto_assistenza,   
				  anno_ord_acq_risorsa,   
				  num_ord_acq_risorsa,   
				  prog_riga_ord_acq_risorsa,   
				  rif_contratto_assistenza,   
				  rif_ordine_acq_risorsa,   
				  cod_primario,   
				  cod_misura,   
				  valore_min_taratura,   
				  valore_max_taratura,   
				  valore_atteso,   
				  tot_costo_intervento,   
				  num_reg_lista,   
				  prog_liste_con_comp,   
				  num_versione,   
				  num_edizione)  
				values
				  (
				  :s_cs_xx.cod_azienda,
				  :ll_anno_registrazione,
				  :ll_num_registrazione,
				  :ldt_data_registrazione,
				  :ls_null,
				  'S',
				  'N',
				  :ll_null,
				  :ll_null,
				  :ldt_null,
				  'N',
				  :fn_anno_programma,
				  :fn_num_programma,
				  :ls_des_intervento,   
				  :ls_cod_attrezzatura,   
				  :ls_cod_tipo_manutenzione,   
				  :ls_flag_tipo_intervento,   
				  :ls_note_idl,   
				  :ls_flag_ricambio_codificato,   
				  :ls_cod_kit_ricambio,
				  :ls_cod_versione,
				  :ls_ricambio_non_codificato,   
				  :ld_quan_ricambio,   
				  :ld_costo_unitario_ricambio,   
				  :ls_cod_operaio,   
				  :ld_costo_orario_operaio,   
				  :ll_operaio_ore_previste,   
				  :ll_operaio_minuti_previsti,   
				  :ls_cod_cat_risorse_esterne,   
				  :ls_cod_risorsa_esterna,   
				  :ll_risorsa_ore_previste,   
				  :ll_risorsa_minuti_previsti,   
				  :ld_risorsa_costo_orario,   
				  :ld_risorsa_costi_aggiuntivi,   
				  :ll_anno_contratto_assistenza,   
				  :ll_num_contratto_assistenza,   
				  :ll_anno_ord_acq_risorsa,   
				  :ll_num_ord_acq_risorsa,   
				  :ll_prog_riga_ord_acq_risorsa,   
				  :ls_rif_contratto_assistenza,   
				  :ls_rif_ordine_acq_risorsa,   
				  :ls_cod_primario,   
				  :ls_cod_misura,   
				  :ld_valore_min_taratura,   
				  :ld_valore_max_taratura,   
				  :ld_valore_atteso,   
				  :ld_tot_costo_intervento,   
				  :ll_num_reg_lista,   
				  :ll_prog_liste_con_comp,   
				  :ll_num_versione,   
				  :ll_num_edizione);
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in inserimento manutenzione programmata" + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

end if
// **** michela 02/11/2004: inserimento operatore e risorse esterne

long ll_ret

ll_ret = uof_crea_operatore_risorse( ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ll_anno_registrazione, ll_num_registrazione, fs_messaggio)

if ll_ret <> 0 then return ll_ret

/////////////////////////////////////////// inserimento ricambi ////////////////////////////////////////////////////////

string ls_messaggio

uo_ricambi luo_ricambi

luo_ricambi = create uo_ricambi

if luo_ricambi.uof_ricambi_manutenzione(ll_anno_registrazione,ll_num_registrazione,ls_messaggio) <> 0 then
	g_mb.messagebox("OMNIA",ls_messaggio)
	destroy luo_ricambi
	return -1
end if

destroy luo_ricambi

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

s_cs_xx.parametri.parametro_ul_1 = ll_anno_registrazione
s_cs_xx.parametri.parametro_ul_2 = ll_num_registrazione

return 0
end function

public function integer uof_crea_manutenzione_programmata_periodo (long fn_anno_programma, long fn_num_programma, datetime fdt_data_registrazione_manut, ref string fs_messaggio);// ** specifica Programmazione_periodo (Guerrato) funzione 1 *** //
// michela: 30/06/2004
// ** a questo metodo passo: la data di registrazione da assegnare alla manutenzione
//                      

string ls_null, ls_des_intervento, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_tipo_intervento, ls_note_idl, &
       ls_flag_ricambio_codificato, ls_cod_kit_ricambio, ls_ricambio_non_codificato, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
		 ls_cod_risorsa_esterna, ls_rif_contratto_assistenza, ls_rif_ordine_acq_risorsa, ls_cod_primario, ls_cod_misura, &
		 ls_cod_versione, ls_ricambi[], ls_des_prodotto, ls_cod_ricambio, ls_des_ricambio, ls_blocco, ls_flag_abr
		 
long   ll_anno_registrazione, ll_num_registrazione, ll_null, ll_operaio_ore_previste, ll_operaio_minuti_previsti, &
       ll_risorsa_ore_previste, ll_risorsa_minuti_previsti, ll_anno_contratto_assistenza, ll_num_contratto_assistenza, &
	    ll_anno_ord_acq_risorsa, ll_num_ord_acq_risorsa, ll_prog_riga_ord_acq_risorsa, ll_num_reg_lista, ll_prog_liste_con_comp, &
	    ll_num_versione, ll_num_edizione, LL_CONT, ll_j, ll_prog_ricambio
		 
dec{4} ld_quan_ricambio, ld_costo_unitario_ricambio, ld_costo_orario_operaio, ld_risorsa_costo_orario, &
       ld_risorsa_costi_aggiuntivi,  ld_valore_min_taratura, ld_valore_max_taratura, ld_valore_atteso, ld_tot_costo_intervento, &
		 ld_quantita[], ld_prezzo, ld_prezzo_ricambio
		 
datetime ldt_data_registrazione, ldt_null
//*******claudia 27/11/06 se presente il parametro multiaziendale abr allora esiste il campo flag_visualizza intranet e lo imposta
//******modifica fatta per abor specifica stampi
select flag
into :ls_flag_abr
from parametri 
where cod_parametro = 'ABR' and
		flag_parametro = 'F';
		
//Donato 04-02-2009 modifica per evitare che dia errore quando non trova il parametro ABR
//che tra l'altro è una personalizzazione del cliente ABOR
if sqlca.sqlcode <> 0 then
	ls_flag_abr = "N"
end if
/*
if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore durante il controllo di un paremtro ABR ." + sqlca.sqlerrtext)
		setpointer(arrow!)
		return -1
end if
*/
//fine modifica ---------------------------------------------------------------------

setnull(ls_null)
setnull(ll_null)
setnull(ldt_null)

select count(*)
into   :ll_cont
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_reg_programma = :fn_anno_programma and
       num_reg_programma = :fn_num_programma and
		 flag_eseguito = 'N';
if ls_flag_abr = 'S' then 
	select  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  note_idl,   
			  flag_ricambio_codificato,   
			  cod_ricambio,
			  cod_versione,
			  ricambio_non_codificato,   
			  quan_ricambio,   
			  costo_unitario_ricambio,   
			  cod_operaio,   
			  costo_orario_operaio,   
			  operaio_ore_previste,   
			  operaio_minuti_previsti,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  risorsa_ore_previste,   
			  risorsa_minuti_previsti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi,   
			  anno_contratto_assistenza,   
			  num_contratto_assistenza,   
			  anno_ord_acq_risorsa,   
			  num_ord_acq_risorsa,   
			  prog_riga_ord_acq_risorsa,   
			  rif_contratto_assistenza,   
			  rif_ordine_acq_risorsa,   
			  cod_primario,   
			  cod_misura,   
			  valore_min_taratura,   
			  valore_max_taratura,   
			  valore_atteso,   
			  tot_costo_intervento,   
			  num_reg_lista,   
			  prog_liste_con_comp,   
			  num_versione,   
			  num_edizione,
			  flag_pubblica_intranet
	into    :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_flag_tipo_intervento,   
			  :ls_note_idl,   
			  :ls_flag_ricambio_codificato,   
			  :ls_cod_kit_ricambio,
			  :ls_cod_versione,
			  :ls_ricambio_non_codificato,   
			  :ld_quan_ricambio,   
			  :ld_costo_unitario_ricambio,   
			  :ls_cod_operaio,   
			  :ld_costo_orario_operaio,   
			  :ll_operaio_ore_previste,   
			  :ll_operaio_minuti_previsti,   
			  :ls_cod_cat_risorse_esterne,   
			  :ls_cod_risorsa_esterna,   
			  :ll_risorsa_ore_previste,   
			  :ll_risorsa_minuti_previsti,   
			  :ld_risorsa_costo_orario,   
			  :ld_risorsa_costi_aggiuntivi,   
			  :ll_anno_contratto_assistenza,   
			  :ll_num_contratto_assistenza,   
			  :ll_anno_ord_acq_risorsa,   
			  :ll_num_ord_acq_risorsa,   
			  :ll_prog_riga_ord_acq_risorsa,   
			  :ls_rif_contratto_assistenza,   
			  :ls_rif_ordine_acq_risorsa,   
			  :ls_cod_primario,   
			  :ls_cod_misura,   
			  :ld_valore_min_taratura,   
			  :ld_valore_max_taratura,   
			  :ld_valore_atteso,   
			  :ld_tot_costo_intervento,   
			  :ll_num_reg_lista,   
			  :ll_prog_liste_con_comp,   
			  :ll_num_versione,   
			  :ll_num_edizione,
			  :ls_flag_abr
	from    programmi_manutenzione
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fn_anno_programma and
			  num_registrazione  = :fn_num_programma;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in ricerca programma manutenzione " + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in SELECT tabella programmi manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
else
		select  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  note_idl,   
			  flag_ricambio_codificato,   
			  cod_ricambio,
			  cod_versione,
			  ricambio_non_codificato,   
			  quan_ricambio,   
			  costo_unitario_ricambio,   
			  cod_operaio,   
			  costo_orario_operaio,   
			  operaio_ore_previste,   
			  operaio_minuti_previsti,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  risorsa_ore_previste,   
			  risorsa_minuti_previsti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi,   
			  anno_contratto_assistenza,   
			  num_contratto_assistenza,   
			  anno_ord_acq_risorsa,   
			  num_ord_acq_risorsa,   
			  prog_riga_ord_acq_risorsa,   
			  rif_contratto_assistenza,   
			  rif_ordine_acq_risorsa,   
			  cod_primario,   
			  cod_misura,   
			  valore_min_taratura,   
			  valore_max_taratura,   
			  valore_atteso,   
			  tot_costo_intervento,   
			  num_reg_lista,   
			  prog_liste_con_comp,   
			  num_versione,   
			  num_edizione
	into    :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_flag_tipo_intervento,   
			  :ls_note_idl,   
			  :ls_flag_ricambio_codificato,   
			  :ls_cod_kit_ricambio,
			  :ls_cod_versione,
			  :ls_ricambio_non_codificato,   
			  :ld_quan_ricambio,   
			  :ld_costo_unitario_ricambio,   
			  :ls_cod_operaio,   
			  :ld_costo_orario_operaio,   
			  :ll_operaio_ore_previste,   
			  :ll_operaio_minuti_previsti,   
			  :ls_cod_cat_risorse_esterne,   
			  :ls_cod_risorsa_esterna,   
			  :ll_risorsa_ore_previste,   
			  :ll_risorsa_minuti_previsti,   
			  :ld_risorsa_costo_orario,   
			  :ld_risorsa_costi_aggiuntivi,   
			  :ll_anno_contratto_assistenza,   
			  :ll_num_contratto_assistenza,   
			  :ll_anno_ord_acq_risorsa,   
			  :ll_num_ord_acq_risorsa,   
			  :ll_prog_riga_ord_acq_risorsa,   
			  :ls_rif_contratto_assistenza,   
			  :ls_rif_ordine_acq_risorsa,   
			  :ls_cod_primario,   
			  :ls_cod_misura,   
			  :ld_valore_min_taratura,   
			  :ld_valore_max_taratura,   
			  :ld_valore_atteso,   
			  :ld_tot_costo_intervento,   
			  :ll_num_reg_lista,   
			  :ll_prog_liste_con_comp,   
			  :ll_num_versione,   
			  :ll_num_edizione
	from    programmi_manutenzione
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fn_anno_programma and
			  num_registrazione  = :fn_num_programma;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in ricerca programma manutenzione " + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in SELECT tabella programmi manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

end if

// Modifica Michele per gestione blocco tipologie di manutenzione 01/04/2003

select flag_blocco
into   :ls_blocco
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in lettura dati tipologia manutenzione: " + sqlca.sqlerrtext
	return -1
elseif ls_blocco = "S" then
	fs_messaggio = "Questa tipologia di manutenzione è stata bloccata: operazione interrotta"
	return -1
end if

// Fine Modifica

ll_anno_registrazione = f_anno_esercizio()

select max(num_registrazione)
into   :ll_num_registrazione
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione;
if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if
if ls_flag_abr = 'S' then 
	insert into manutenzioni  
				  ( 
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_guasto,   
				  flag_ordinario,   
				  flag_eseguito,   
				  anno_non_conf,   
				  num_non_conf,   
				  data_prossimo_intervento,   
				  flag_budget,   
				  anno_reg_programma,   
				  num_reg_programma,
				  des_intervento,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  flag_tipo_intervento,   
				  note_idl,   
				  flag_ricambio_codificato,   
				  cod_ricambio,
				  cod_versione,
				  ricambio_non_codificato,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_operaio,   
				  costo_orario_operaio,   
				  operaio_ore,   
				  operaio_minuti,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  risorsa_ore,   
				  risorsa_minuti,   
				  risorsa_costo_orario,   
				  risorsa_costi_aggiuntivi,   
				  anno_contratto_assistenza,   
				  num_contratto_assistenza,   
				  anno_ord_acq_risorsa,   
				  num_ord_acq_risorsa,   
				  prog_riga_ord_acq_risorsa,   
				  rif_contratto_assistenza,   
				  rif_ordine_acq_risorsa,   
				  cod_primario,   
				  cod_misura,   
				  valore_min_taratura,   
				  valore_max_taratura,   
				  valore_atteso,   
				  tot_costo_intervento,   
				  num_reg_lista,   
				  prog_liste_con_comp,   
				  num_versione,   
				  num_edizione,				  
				  flag_pubblica_intranet
				  )  
				values
				  (
				  :s_cs_xx.cod_azienda,
				  :ll_anno_registrazione,
				  :ll_num_registrazione,
				  :fdt_data_registrazione_manut,
				  :ls_null,
				  'S',
				  'N',
				  :ll_null,
				  :ll_null,
				  :ldt_null,
				  'N',
				  :fn_anno_programma,
				  :fn_num_programma,
				  :ls_des_intervento,   
				  :ls_cod_attrezzatura,   
				  :ls_cod_tipo_manutenzione,   
				  :ls_flag_tipo_intervento,   
				  :ls_note_idl,   
				  :ls_flag_ricambio_codificato,   
				  :ls_cod_kit_ricambio,
				  :ls_cod_versione,
				  :ls_ricambio_non_codificato,   
				  :ld_quan_ricambio,   
				  :ld_costo_unitario_ricambio,   
				  :ls_cod_operaio,   
				  :ld_costo_orario_operaio,   
				  :ll_operaio_ore_previste,   
				  :ll_operaio_minuti_previsti,   
				  :ls_cod_cat_risorse_esterne,   
				  :ls_cod_risorsa_esterna,   
				  :ll_risorsa_ore_previste,   
				  :ll_risorsa_minuti_previsti,   
				  :ld_risorsa_costo_orario,   
				  :ld_risorsa_costi_aggiuntivi,   
				  :ll_anno_contratto_assistenza,   
				  :ll_num_contratto_assistenza,   
				  :ll_anno_ord_acq_risorsa,   
				  :ll_num_ord_acq_risorsa,   
				  :ll_prog_riga_ord_acq_risorsa,   
				  :ls_rif_contratto_assistenza,   
				  :ls_rif_ordine_acq_risorsa,   
				  :ls_cod_primario,   
				  :ls_cod_misura,   
				  :ld_valore_min_taratura,   
				  :ld_valore_max_taratura,   
				  :ld_valore_atteso,   
				  :ld_tot_costo_intervento,   
				  :ll_num_reg_lista,   
				  :ll_prog_liste_con_comp,   
				  :ll_num_versione,   
				  :ll_num_edizione,
				  :ls_flag_abr
				  );
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in inserimento manutenzione programmata" + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
else
		insert into manutenzioni  
				  ( 
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_guasto,   
				  flag_ordinario,   
				  flag_eseguito,   
				  anno_non_conf,   
				  num_non_conf,   
				  data_prossimo_intervento,   
				  flag_budget,   
				  anno_reg_programma,   
				  num_reg_programma,
				  des_intervento,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  flag_tipo_intervento,   
				  note_idl,   
				  flag_ricambio_codificato,   
				  cod_ricambio,
				  cod_versione,
				  ricambio_non_codificato,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_operaio,   
				  costo_orario_operaio,   
				  operaio_ore,   
				  operaio_minuti,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  risorsa_ore,   
				  risorsa_minuti,   
				  risorsa_costo_orario,   
				  risorsa_costi_aggiuntivi,   
				  anno_contratto_assistenza,   
				  num_contratto_assistenza,   
				  anno_ord_acq_risorsa,   
				  num_ord_acq_risorsa,   
				  prog_riga_ord_acq_risorsa,   
				  rif_contratto_assistenza,   
				  rif_ordine_acq_risorsa,   
				  cod_primario,   
				  cod_misura,   
				  valore_min_taratura,   
				  valore_max_taratura,   
				  valore_atteso,   
				  tot_costo_intervento,   
				  num_reg_lista,   
				  prog_liste_con_comp,   
				  num_versione,   
				  num_edizione)  
				values
				  (
				  :s_cs_xx.cod_azienda,
				  :ll_anno_registrazione,
				  :ll_num_registrazione,
				  :fdt_data_registrazione_manut,
				  :ls_null,
				  'S',
				  'N',
				  :ll_null,
				  :ll_null,
				  :ldt_null,
				  'N',
				  :fn_anno_programma,
				  :fn_num_programma,
				  :ls_des_intervento,   
				  :ls_cod_attrezzatura,   
				  :ls_cod_tipo_manutenzione,   
				  :ls_flag_tipo_intervento,   
				  :ls_note_idl,   
				  :ls_flag_ricambio_codificato,   
				  :ls_cod_kit_ricambio,
				  :ls_cod_versione,
				  :ls_ricambio_non_codificato,   
				  :ld_quan_ricambio,   
				  :ld_costo_unitario_ricambio,   
				  :ls_cod_operaio,   
				  :ld_costo_orario_operaio,   
				  :ll_operaio_ore_previste,   
				  :ll_operaio_minuti_previsti,   
				  :ls_cod_cat_risorse_esterne,   
				  :ls_cod_risorsa_esterna,   
				  :ll_risorsa_ore_previste,   
				  :ll_risorsa_minuti_previsti,   
				  :ld_risorsa_costo_orario,   
				  :ld_risorsa_costi_aggiuntivi,   
				  :ll_anno_contratto_assistenza,   
				  :ll_num_contratto_assistenza,   
				  :ll_anno_ord_acq_risorsa,   
				  :ll_num_ord_acq_risorsa,   
				  :ll_prog_riga_ord_acq_risorsa,   
				  :ls_rif_contratto_assistenza,   
				  :ls_rif_ordine_acq_risorsa,   
				  :ls_cod_primario,   
				  :ls_cod_misura,   
				  :ld_valore_min_taratura,   
				  :ld_valore_max_taratura,   
				  :ld_valore_atteso,   
				  :ld_tot_costo_intervento,   
				  :ll_num_reg_lista,   
				  :ll_prog_liste_con_comp,   
				  :ll_num_versione,   
				  :ll_num_edizione);
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in inserimento manutenzione programmata" + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

end if
// *** michela 02/11/2004: inserimento di operatore e risorse nella tipologia di manutenzione

long ll_ret 

ll_ret = uof_crea_operatore_risorse( ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ll_anno_registrazione, ll_num_registrazione, fs_messaggio)

if ll_ret <> 0 then return ll_ret

/////////////////////////////////////////// inserimento ricambi ////////////////////////////////////////////////////////

string ls_messaggio

uo_ricambi luo_ricambi

luo_ricambi = create uo_ricambi

if luo_ricambi.uof_ricambi_manutenzione(ll_anno_registrazione,ll_num_registrazione,ls_messaggio) <> 0 then
	g_mb.messagebox("OMNIA",ls_messaggio)
	destroy luo_ricambi
	return -1
end if

destroy luo_ricambi

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return 0
end function

public function integer uof_crea_operatore_risorse (string fs_cod_tipo_manutenzione, string fs_cod_attrezzatura, long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio);//*** 02/11/04 inserimento operatori e risorse esterne della tipologia di manutenzione ***

string ls_cod_operaio_tipologia, ls_cod_cat_risorse_esterne_tipologia, ls_cod_risorsa_esterna_tipologia, ls_flag_fasi

dec{4} ld_tariffa_risorsa

setnull(ls_cod_operaio_tipologia)

setnull(ls_cod_cat_risorse_esterne_tipologia)

setnull(ls_cod_risorsa_esterna_tipologia)

setnull(ls_flag_fasi)

select cod_operaio,
       cod_cat_risorse_esterne,
		 cod_risorsa_esterna
into   :ls_cod_operaio_tipologia,
       :ls_cod_cat_risorse_esterne_tipologia,
		 :ls_cod_risorsa_esterna_tipologia
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :fs_cod_attrezzatura and
		 cod_tipo_manutenzione = :fs_cod_tipo_manutenzione;
		 
if sqlca.sqlcode < 0 then
	
	fs_messaggio = "Errore durante la ricerca operai/risorse dalla tipologia di manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext

	return -1
	
elseif sqlca.sqlcode = 100 then
	
	fs_messaggio = "Errore durante la ricerca operai/risorse dalla tipologia di manutenzione.~r~nTipologia non trovata."
	
	return -1
	
end if

// *** inserimento di operatore e risorse esterne della tipologia di manutenzione. controllo il parametro CFL per vedere
// *** se bisogna considerare le fasi di lavoro oppure no. se non esiste proprio il parametro allora vado avanti normalmente
	
select flag
into   :ls_flag_fasi
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL' and
		 flag_parametro = 'F';
			 
if sqlca.sqlcode < 0 then
		
	fs_messaggio = "Si è verificato un errore in lettura dalla tabella parametri omnia di CFL.~r~nDettaglio errore: " + sqlca.sqlerrtext
		
	return -1
		
elseif sqlca.sqlcode = 0 and ls_flag_fasi = 'S' then
		
	// ***  FASI DI LAVORO ***
	
	if ( not isnull(ls_cod_operaio_tipologia) ) or ( not isnull(ls_cod_cat_risorse_esterne_tipologia) and not isnull(ls_cod_risorsa_esterna_tipologia) ) then
	
		insert into manutenzioni_fasi ( cod_azienda,
												  anno_registrazione,
												  num_registrazione,
												  prog_fase,
												  des_fase)
									values   ( :s_cs_xx.cod_azienda,
												  :fl_anno_registrazione,
												  :fl_num_registrazione,
												  1,
												  'Fase 1 di lavoro.');
												  
		if sqlca.sqlcode < 0 then
			
			fs_messaggio = "Errore durante la creazione della prima fase di lavoro.~r~nDettaglio errore: " + sqlca.sqlerrtext
			
			return -1
		
		end if
		
		if not isnull(ls_cod_operaio_tipologia) then
			
			insert into manutenzioni_fasi_operai ( cod_azienda,
																anno_registrazione,
																num_registrazione,
																prog_fase,
																cod_operaio)
												values    ( :s_cs_xx.cod_azienda,
																:fl_anno_registrazione,
																:fl_num_registrazione,
																1,
																:ls_cod_operaio_tipologia);
																
			if sqlca.sqlcode < 0 then
			
				fs_messaggio = "Errore durante la creazione dell'operatore della prima fase di lavoro.~r~nDettaglio errore: " + sqlca.sqlerrtext
				
				return -1
		
			end if
			
		end if
		
		if not isnull(ls_cod_cat_risorse_esterne_tipologia) and not isnull(ls_cod_risorsa_esterna_tipologia) then
			
			select tariffa_std
			into   :ld_tariffa_risorsa
			from   anag_risorse_esterne
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne_tipologia and
					 cod_risorsa_esterna = :ls_cod_risorsa_esterna_tipologia;
	       
			insert into manutenzioni_fasi_risorse ( cod_azienda,
																 anno_registrazione,
																 num_registrazione,
																 prog_fase,
																 cod_cat_risorse_esterne,
																 cod_risorsa_esterna,
																 costo_ora_risorsa)
												values    (  :s_cs_xx.cod_azienda,
																 :fl_anno_registrazione,
																 :fl_num_registrazione,
																 1,
																 :ls_cod_cat_risorse_esterne_tipologia,
																 :ls_cod_risorsa_esterna_tipologia,
																 :ld_tariffa_risorsa);
																
			if sqlca.sqlcode < 0 then
			
				fs_messaggio = "Errore durante la creazione delle risorse della prima fase di lavoro.~r~nDettaglio errore: " + sqlca.sqlerrtext
				
				return -1
		
			end if		
			
		end if
	
	end if
		
else

	if not isnull(ls_cod_operaio_tipologia) then
		
		update manutenzioni
		set    cod_operaio = :ls_cod_operaio_tipologia
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_registrazione and
				 num_registrazione = :fl_num_registrazione;
				 
		if sqlca.sqlcode < 0 then
			
			fs_messaggio = "Errore durante la modifica dell'operatore della manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
				
			return -1
		
		end if					 
	
	end if
	
	if not isnull(ls_cod_cat_risorse_esterne_tipologia) and not isnull(ls_cod_risorsa_esterna_tipologia) then
		
		select tariffa_std
		into   :ld_tariffa_risorsa
		from   anag_risorse_esterne
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne_tipologia and
				 cod_risorsa_esterna = :ls_cod_risorsa_esterna_tipologia;		
		
		update manutenzioni
		set    cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne_tipologia,
		       cod_risorsa_esterna = :ls_cod_risorsa_esterna_tipologia,
				 risorsa_costo_orario = :ld_tariffa_risorsa				 
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_registrazione and
				 num_registrazione = :fl_num_registrazione;		
		
		if sqlca.sqlcode < 0 then
			
			fs_messaggio = "Errore durante la modifica delle risorse della manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
				
			return -1
		
		end if					 		
		
	end if
	
end if

return 0
end function

public function integer uof_crea_operatore_risorse_programma (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio);//*** 02/11/04 inserimento operatori e risorse esterne del programma della manutenzione ***

string ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_flag_fasi

dec{4} ld_tariffa_risorsa

long   ll_anno_reg_programma, ll_num_reg_programma

setnull(ls_cod_operaio)

setnull(ls_cod_cat_risorse_esterne)

setnull(ls_cod_risorsa_esterna)

setnull(ls_flag_fasi)

select anno_reg_programma,
       num_reg_programma
into   :ll_anno_reg_programma,
       :ll_num_reg_programma
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
		 
if sqlca.sqlcode < 0 then
	
	fs_messaggio = "Errore durante la ricerca dei programmi di manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext

	return -1
	
elseif sqlca.sqlcode = 100 then
	
	fs_messaggio = "Errore durante la ricerca dei programmi di manutenzione.~r~nProgramma non trovato."
	
	return -1
	
end if

select cod_operaio,
       cod_cat_risorse_esterne,
		 cod_risorsa_esterna
into   :ls_cod_operaio,
       :ls_cod_cat_risorse_esterne,
		 :ls_cod_risorsa_esterna
from   programmi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_reg_programma and
		 num_registrazione = :ll_num_reg_programma;
		 
if sqlca.sqlcode < 0 then
	
	fs_messaggio = "Errore durante la ricerca operai/risorse dai programmi di manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext

	return -1
	
elseif sqlca.sqlcode = 100 then
	
	fs_messaggio = "Errore durante la ricerca operai/risorse dai programmi di manutenzione.~r~nTipologia non trovata."
	
	return -1
	
end if
// *** inserimento di operatore e risorse esterne della tipologia di manutenzione. controllo il parametro CFL per vedere
// *** se bisogna considerare le fasi di lavoro oppure no. se non esiste proprio il parametro allora vado avanti normalmente
	
select flag
into   :ls_flag_fasi
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL' and
		 flag_parametro = 'F';
			 
if sqlca.sqlcode < 0 then
		
	fs_messaggio = "Si è verificato un errore in lettura dalla tabella parametri omnia di CFL.~r~nDettaglio errore: " + sqlca.sqlerrtext
		
	return -1
		
elseif sqlca.sqlcode = 0 and ls_flag_fasi = 'S' then
		
	// ***  FASI DI LAVORO ***
	
	if ( not isnull(ls_cod_operaio) ) or ( not isnull(ls_cod_cat_risorse_esterne) and not isnull(ls_cod_risorsa_esterna) ) then
	
		insert into manutenzioni_fasi ( cod_azienda,
												  anno_registrazione,
												  num_registrazione,
												  prog_fase,
												  des_fase)
									values   ( :s_cs_xx.cod_azienda,
												  :fl_anno_registrazione,
												  :fl_num_registrazione,
												  1,
												  'Fase 1 di lavoro.');
												  
		if sqlca.sqlcode < 0 then
			
			fs_messaggio = "Errore durante la creazione della prima fase di lavoro.~r~nDettaglio errore: " + sqlca.sqlerrtext
			
			return -1
		
		end if
		
		if not isnull(ls_cod_operaio) then
			
			insert into manutenzioni_fasi_operai ( cod_azienda,
																anno_registrazione,
																num_registrazione,
																prog_fase,
																cod_operaio)
												values    ( :s_cs_xx.cod_azienda,
																:fl_anno_registrazione,
																:fl_num_registrazione,
																1,
																:ls_cod_operaio);
																
			if sqlca.sqlcode < 0 then
			
				fs_messaggio = "Errore durante la creazione dell'operatore della prima fase di lavoro.~r~nDettaglio errore: " + sqlca.sqlerrtext
				
				return -1
		
			end if
			
		end if
		
		if not isnull(ls_cod_cat_risorse_esterne) and not isnull(ls_cod_risorsa_esterna) then
			
			select tariffa_std
			into   :ld_tariffa_risorsa
			from   anag_risorse_esterne
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne and
					 cod_risorsa_esterna = :ls_cod_risorsa_esterna;			
			
			insert into manutenzioni_fasi_risorse ( cod_azienda,
																 anno_registrazione,
																 num_registrazione,
																 prog_fase,
																 cod_cat_risorse_esterne,
																 cod_risorsa_esterna,
																 costo_ora_risorsa)
												values    (  :s_cs_xx.cod_azienda,
																 :fl_anno_registrazione,
																 :fl_num_registrazione,
																 1,
																 :ls_cod_cat_risorse_esterne,
																 :ls_cod_risorsa_esterna,
																 :ld_tariffa_risorsa);
																
			if sqlca.sqlcode < 0 then
			
				fs_messaggio = "Errore durante la creazione delle risorse della prima fase di lavoro.~r~nDettaglio errore: " + sqlca.sqlerrtext
				
				return -1
		
			end if		
			
		end if
	
	end if
		
else

	if not isnull(ls_cod_operaio) then
		
		update manutenzioni
		set    cod_operaio = :ls_cod_operaio
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_registrazione and
				 num_registrazione = :fl_num_registrazione;
				 
		if sqlca.sqlcode < 0 then
			
			fs_messaggio = "Errore durante la modifica dell'operatore della manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
				
			return -1
		
		end if					 
	
	end if
	
	if not isnull(ls_cod_cat_risorse_esterne) and not isnull(ls_cod_risorsa_esterna) then
		
		select tariffa_std
		into   :ld_tariffa_risorsa
		from   anag_risorse_esterne
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne and
				 cod_risorsa_esterna = :ls_cod_risorsa_esterna;				
		
		update manutenzioni
		set    cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne,
		       cod_risorsa_esterna = :ls_cod_risorsa_esterna,
				 risorsa_costo_orario = :ld_tariffa_risorsa
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_registrazione and
				 num_registrazione = :fl_num_registrazione;		
		
		if sqlca.sqlcode < 0 then
			
			fs_messaggio = "Errore durante la modifica delle risorse della manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
				
			return -1
		
		end if					 		
		
	end if
	
end if

return 0
end function

public function integer uof_calcola_manutenzione (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_errore);dec{4} ld_tot_costo_ricambi, ld_tot_costo_intervento,ld_tot_costo_operai, &
       ld_quan_ricambio, ld_costo_ricambio, ld_costo_ora_operaio, ld_tot_ore_risorsa, &
		 ld_risorsa_costo_orario, ld_risorsa_costi_aggiuntivi, ld_tot_ore_operaio
		 
long   ll_operaio_ore, ll_operaio_minuti, ll_risorsa_ore, ll_risorsa_minuti

if ib_global_service then
	// se applicazione global service chiamo funzione specifica.
	
	uof_calcola_manutenzioni_global(fl_anno_registrazione, fl_num_registrazione, ref fs_errore)
	
else
	// gestione normale NON GLOBAL SERVICE
	
	select sum(imponibile_iva)
	into   :ld_tot_costo_ricambi
	from   manutenzioni_ricambi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 flag_utilizzo = 'S';
	
	if isnull(ld_tot_costo_ricambi) then 
		ld_tot_costo_ricambi = 0
	end if
		
	select sum(tot_costo_operaio)
	into   :ld_tot_costo_operai
	from   manutenzioni_operai
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione;
	
	if isnull(ld_tot_costo_operai) then 
		ld_tot_costo_operai = 0
	end if
	
	SELECT quan_ricambio,   
			costo_unitario_ricambio,   
			costo_orario_operaio,   
			operaio_ore,   
			operaio_minuti,   
			risorsa_ore,   
			risorsa_minuti,   
			risorsa_costo_orario,   
			risorsa_costi_aggiuntivi  
	 INTO :ld_quan_ricambio,   
			:ld_costo_ricambio,   
			:ld_costo_ora_operaio,   
			:ll_operaio_ore,   
			:ll_operaio_minuti,   
			:ll_risorsa_ore,   
			:ll_risorsa_minuti,   
			:ld_risorsa_costo_orario,   
			:ld_risorsa_costi_aggiuntivi  
	 FROM manutenzioni  
	WHERE cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fl_anno_registrazione and
			num_registrazione = :fl_num_registrazione   ;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca dati manutenzione " + string(fl_anno_registrazione) + "/" + &
							string(fl_num_registrazione) + ": " + sqlca.sqlerrtext
		return -1
	end if
		
	if isnull(ld_quan_ricambio) then  ld_quan_ricambio = 0
	if isnull(ld_costo_ricambio) then  ld_costo_ricambio = 0
	if isnull(ld_costo_ora_operaio) then  ld_costo_ora_operaio = 0
	if isnull(ll_operaio_ore) then  ll_operaio_ore = 0
	if isnull(ll_operaio_minuti) then ll_operaio_minuti  = 0
	if isnull(ld_risorsa_costo_orario) then  ld_risorsa_costo_orario = 0
	if isnull(ld_risorsa_costi_aggiuntivi) then ld_risorsa_costi_aggiuntivi   = 0
	if isnull(ll_risorsa_ore) then ll_risorsa_ore  = 0
	if isnull(ll_risorsa_minuti) then  ll_risorsa_minuti = 0
	
	ld_tot_ore_operaio = ll_operaio_minuti / 60
	ld_tot_ore_operaio = round(ld_tot_ore_operaio, 2)
	ld_tot_ore_operaio += ll_operaio_ore
	
	ld_tot_ore_risorsa = ll_risorsa_minuti / 60
	ld_tot_ore_risorsa = round(ld_tot_ore_risorsa,2)
	ld_tot_ore_risorsa += ll_risorsa_ore
		
	ld_tot_costo_intervento = ld_tot_costo_ricambi + ld_tot_costo_operai + &
									 (ld_quan_ricambio * ld_costo_ricambio) + &
									 (ld_costo_ora_operaio *  ld_tot_ore_operaio) + &
									 (ld_risorsa_costo_orario *  ld_tot_ore_risorsa) + ld_risorsa_costi_aggiuntivi
	
	update manutenzioni
	set	 tot_costo_intervento = :ld_tot_costo_intervento
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in aggiornamento dati manutenzione " + string(fl_anno_registrazione) + "/" + &
							string(fl_num_registrazione) + ": " + sqlca.sqlerrtext
		return -1
	end if
end if

return 0
end function

public function integer uof_calcola_manutenzione_fasi (long fl_anno_registrazione, long fl_num_registrazione, long fl_progressivo_fase, ref string fs_errore);string	ls_sql, ls_syntax, ls_error, ls_cod_operaio, ls_cod_cat_risorse_esterne,ls_cod_risorsa_esterna

long	ll_ret, ll_i, ll_fase_tot_ore, ll_fase_tot_minuti, ll_operaio_tot_ore, ll_operaio_tot_minuti, &
         ll_fase_risorsa_tot_minuti, ll_fase_risorsa_tot_ore, ll_risorsa_ore, ll_risorsa_minuti
			
dec{4}   ld_operaio_tot_costo, ld_fase_tot_costo_operaio, ld_risorsa_tot_costo,ld_fase_risorsa_tot_costo, &
         ld_costo_ora_operaio, ld_sconto_1, ld_costo_ora_risorsa, ld_costi_aggiuntivi

datetime ldt_fase_data_inizio, ldt_fase_data_fine, ldt_fase_ora_inizio, ldt_fase_ora_fine, &
			ldt_operaio_data_inizio, ldt_operaio_data_fine, ldt_operaio_ora_inizio, ldt_operaio_ora_fine, &
			ldt_risorsa_data_inizio, ldt_risorsa_data_fine, ldt_risorsa_ora_inizio, ldt_risorsa_ora_fine

datastore lds_operai, lds_risorse


// ************************************************************
// ****** operai **********************************************
// ************************************************************


ls_sql = &
"select cod_operaio, " + &
"       data_inizio, " + &
"		  ora_inizio, " + &
"		  data_fine, " + &
"		  ora_fine, " + &
"		  tot_ore, " + &
"		  tot_minuti, " + &
"		  costo_ora_operaio, " + &
"		  sconto_1, " + &
"		  tot_costo_operaio " + &
"from	  manutenzioni_fasi_operai " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"		  anno_registrazione = " + string(fl_anno_registrazione) + " and " + &
"		  num_registrazione = " + string(fl_num_registrazione) + " and " + &
"		  prog_fase = " + string(fl_progressivo_fase)

ls_sql += " order by cod_operaio ASC"

ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	fs_errore = "Errore in impostazione select operai - fase "  + string(fl_anno_registrazione) + "/" + &
						string(fl_num_registrazione) + " " + string(fl_progressivo_fase) + ": " + ls_error
	return -1
end if

lds_operai = create datastore

lds_operai.create(ls_syntax,ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	fs_errore = "Errore in creazione datastore operai - fase "  + string(fl_anno_registrazione) + "/" + &
						string(fl_num_registrazione) + " " + string(fl_progressivo_fase) + ": " + ls_error
	destroy lds_operai
	return -1
end if

lds_operai.settransobject(sqlca)

if lds_operai.retrieve() = -1 then
	fs_errore = "Errore in lettura dati operai - fase "  + string(fl_anno_registrazione) + "/" + &
						string(fl_num_registrazione) + " " + string(fl_progressivo_fase)
	destroy lds_operai
	return -1
end if

setnull(ldt_fase_data_inizio)

setnull(ldt_fase_data_fine)

setnull(ldt_fase_ora_inizio)

setnull(ldt_fase_ora_fine)

ll_fase_tot_ore = 0

ll_fase_tot_minuti = 0

for ll_i = 1 to lds_operai.rowcount()
	
	ls_cod_operaio = lds_operai.getitemstring(ll_i,"cod_operaio")
	
	ldt_operaio_data_inizio = lds_operai.getitemdatetime(ll_i,"data_inizio")
	
	ldt_operaio_data_fine = lds_operai.getitemdatetime(ll_i,"data_fine")
	
	ldt_operaio_ora_inizio = lds_operai.getitemdatetime(ll_i,"ora_inizio")
	
	ldt_operaio_ora_fine = lds_operai.getitemdatetime(ll_i,"ora_fine")
	
	if lds_operai.getitemnumber(ll_i,"tot_ore") = 0 and lds_operai.getitemnumber(ll_i,"tot_minuti")=0 then
		ll_ret = uof_calcola_ore_minuti(ldt_operaio_data_inizio, ldt_operaio_data_fine, ldt_operaio_ora_inizio, ldt_operaio_ora_fine, ref ll_operaio_tot_ore, ref ll_operaio_tot_minuti)
	else
		ll_operaio_tot_ore = lds_operai.getitemnumber(ll_i,"tot_ore")
		if isnull(ll_operaio_tot_ore) then ll_operaio_tot_ore = 0
		
		ll_operaio_tot_minuti = lds_operai.getitemnumber(ll_i,"tot_minuti")
		if isnull(ll_operaio_tot_minuti) then ll_operaio_tot_minuti = 0
	end if
	
	ld_costo_ora_operaio = lds_operai.getitemnumber(ll_i,"costo_ora_operaio")
	if isnull(ld_costo_ora_operaio ) then ld_costo_ora_operaio  = 0
	
	ld_sconto_1 = lds_operai.getitemnumber(ll_i,"sconto_1")
	if isnull(ld_sconto_1) then ld_sconto_1 = 0
	
	ld_operaio_tot_costo = ll_operaio_tot_ore + (ll_operaio_tot_minuti / 60)
	ld_operaio_tot_costo = round(ld_operaio_tot_costo,2)
	ld_operaio_tot_costo = ld_operaio_tot_costo * ld_costo_ora_operaio
	ld_operaio_tot_costo = ld_operaio_tot_costo - (ld_operaio_tot_costo * ld_sconto_1 / 100)
	ld_operaio_tot_costo = round(ld_operaio_tot_costo,2)
	
	
	update manutenzioni_fasi_operai
	set    tot_costo_operaio = :ld_operaio_tot_costo,
			tot_ore = :ll_operaio_tot_ore,
			tot_minuti = :ll_operaio_tot_minuti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and 
			 num_registrazione = :fl_num_registrazione and 
			 prog_fase = :fl_progressivo_fase and 
			 cod_operaio = :ls_cod_operaio;
	

	if isnull(ldt_fase_data_inizio) or ldt_operaio_data_inizio < ldt_fase_data_inizio then
			ldt_fase_data_inizio = ldt_operaio_data_inizio
			ldt_fase_ora_inizio = ldt_operaio_ora_inizio
	elseif ldt_operaio_data_inizio = ldt_fase_data_inizio then
		if isnull(ldt_fase_ora_inizio) then
			ldt_fase_ora_inizio = ldt_operaio_ora_inizio
		else
			if ldt_operaio_ora_inizio < ldt_fase_ora_inizio then
				ldt_fase_ora_inizio = ldt_operaio_ora_inizio
			end if
		end if
	end if
	
	if isnull(ldt_fase_data_fine) or ldt_operaio_data_fine > ldt_fase_data_fine then
			ldt_fase_data_fine = ldt_operaio_data_fine
			ldt_fase_ora_fine = ldt_operaio_ora_fine
	elseif ldt_operaio_data_fine = ldt_fase_data_fine then
		if isnull(ldt_fase_ora_fine) then
			ldt_fase_ora_fine = ldt_operaio_ora_fine
		else
			if ldt_operaio_ora_fine > ldt_fase_ora_fine then
				ldt_fase_ora_fine = ldt_operaio_ora_fine
			end if
		end if
	end if
	
	
	ll_fase_tot_ore += ll_operaio_tot_ore
	
	ll_fase_tot_minuti += ll_operaio_tot_minuti
	
	ld_fase_tot_costo_operaio += ld_operaio_tot_costo
	
next

destroy lds_operai

ll_fase_tot_ore += int(ll_fase_tot_minuti / 60)

ll_fase_tot_minuti = mod(ll_fase_tot_minuti,60)


// ************************************************************
// ****** risorse esterne *************************************
// ************************************************************

ls_sql = ""

ls_sql = &
"select cod_cat_risorse_esterne, " + &
"       cod_risorsa_esterna, " + &
"       data_inizio, " + &
"		  ora_inizio, " + &
"		  data_fine, " + &
"		  ora_fine, " + &
"		  tot_ore, " + &
"		  tot_minuti, " + &
"		  costo_ora_risorsa, " + &
"		  sconto_1, " + &
"		  costi_aggiuntivi, " + &
"		  tot_costo_risorsa " + &
"from	  manutenzioni_fasi_risorse " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"		  anno_registrazione = " + string(fl_anno_registrazione) + " and " + &
"		  num_registrazione = " + string(fl_num_registrazione) + " and " + &
"		  prog_fase = " + string(fl_progressivo_fase)

ls_sql += " order by cod_cat_risorse_esterne ASC"

ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	fs_errore = "Errore in impostazione select risorse - fase "  + string(fl_anno_registrazione) + "/" + &
						string(fl_num_registrazione) + " " + string(fl_progressivo_fase) + ": " + ls_error
	return -1
end if

lds_risorse = create datastore

lds_risorse.create(ls_syntax,ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	fs_errore = "Errore in creazione datastore risorse - fase "  + string(fl_anno_registrazione) + "/" + &
						string(fl_num_registrazione) + " " + string(fl_progressivo_fase) + ": " + ls_error
	destroy lds_risorse
	return -1
end if

lds_risorse.settransobject(sqlca)

if lds_risorse.retrieve() = -1 then
	fs_errore = "Errore in lettura dati risorse - fase "  + string(fl_anno_registrazione) + "/" + &
						string(fl_num_registrazione) + " " + string(fl_progressivo_fase)
	destroy lds_operai
	return -1
end if

ll_fase_risorsa_tot_ore = 0

ll_fase_risorsa_tot_minuti = 0

for ll_i = 1 to lds_risorse.rowcount()
	
	ls_cod_risorsa_esterna = lds_risorse.getitemstring(ll_i,"cod_risorsa_esterna")
	
	ls_cod_cat_risorse_esterne = lds_risorse.getitemstring(ll_i,"cod_cat_risorse_esterne")
	
	ldt_risorsa_data_inizio = lds_risorse.getitemdatetime(ll_i,"data_inizio")
	
	ldt_risorsa_data_fine = lds_risorse.getitemdatetime(ll_i,"data_fine")
	
	ldt_risorsa_ora_inizio = lds_risorse.getitemdatetime(ll_i,"ora_inizio")
	
	ldt_risorsa_ora_fine = lds_risorse.getitemdatetime(ll_i,"ora_fine")
	
	if lds_operai.getitemnumber(ll_i,"tot_ore") = 0 and lds_operai.getitemnumber(ll_i,"tot_minuti")=0 then
		ll_ret = uof_calcola_ore_minuti(ldt_risorsa_data_inizio, ldt_risorsa_data_fine, ldt_risorsa_ora_inizio, ldt_risorsa_ora_fine, ref ll_risorsa_ore, ref ll_risorsa_minuti)
	else
		ll_risorsa_ore = lds_risorse.getitemnumber(ll_i,"tot_ore")
		if isnull(ll_risorsa_ore) then ll_risorsa_ore = 0
		
		ll_risorsa_minuti = lds_risorse.getitemnumber(ll_i,"tot_minuti")
		if isnull(ll_risorsa_minuti) then ll_risorsa_minuti = 0
	end if
	ld_costo_ora_risorsa = lds_risorse.getitemnumber(ll_i,"costo_ora_risorsa")
	if isnull(ld_costo_ora_risorsa) then ld_costo_ora_risorsa = 0
	
	ld_sconto_1 = lds_risorse.getitemnumber(ll_i,"sconto_1")
	if isnull(ld_sconto_1) then ld_sconto_1 = 0
	
	ld_costi_aggiuntivi = lds_risorse.getitemnumber(ll_i,"costi_aggiuntivi")
	if isnull(ld_costi_aggiuntivi) then ld_costi_aggiuntivi = 0
	
	ld_risorsa_tot_costo = ll_risorsa_ore + (ll_risorsa_minuti / 60)
	ld_risorsa_tot_costo = round(ld_risorsa_tot_costo,2)
	ld_risorsa_tot_costo = ld_risorsa_tot_costo * ld_costo_ora_risorsa
	ld_risorsa_tot_costo = (ld_risorsa_tot_costo - (ld_risorsa_tot_costo * ld_sconto_1 / 100)) + ld_costi_aggiuntivi
	ld_risorsa_tot_costo = round(ld_risorsa_tot_costo,2)
	
	update manutenzioni_fasi_risorse
	set    tot_costo_risorsa= :ld_risorsa_tot_costo,
			tot_ore = :ll_risorsa_ore,
			tot_minuti = :ll_risorsa_minuti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and 
			 num_registrazione = :fl_num_registrazione and 
			 prog_fase = :fl_progressivo_fase and 
			 cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne and
			 cod_risorsa_esterna = :ls_cod_risorsa_esterna;

	if isnull(ldt_fase_data_inizio) or ldt_risorsa_data_inizio < ldt_fase_data_inizio then
			ldt_fase_data_inizio = ldt_risorsa_data_inizio
			ldt_fase_ora_inizio = ldt_risorsa_ora_inizio
	elseif ldt_risorsa_data_inizio = ldt_fase_data_inizio then
		if isnull(ldt_fase_ora_inizio) then
			ldt_fase_ora_inizio = ldt_risorsa_ora_inizio
		else
			if ldt_risorsa_ora_inizio < ldt_fase_ora_inizio then
				ldt_fase_ora_inizio = ldt_risorsa_ora_inizio
			end if
		end if
	end if
	
	if isnull(ldt_fase_data_fine) or ldt_risorsa_data_fine > ldt_fase_data_fine then
			ldt_fase_data_fine = ldt_risorsa_data_fine
			ldt_fase_ora_fine = ldt_risorsa_ora_fine
	elseif ldt_risorsa_data_fine = ldt_fase_data_fine then
		if isnull(ldt_fase_ora_fine) then
			ldt_fase_ora_fine = ldt_risorsa_ora_fine
		else
			if ldt_risorsa_ora_fine > ldt_fase_ora_fine then
				ldt_fase_ora_fine = ldt_risorsa_ora_fine
			end if
		end if
	end if
	
	
	ll_fase_risorsa_tot_ore += ll_risorsa_ore
	
	ll_fase_risorsa_tot_minuti += ll_risorsa_minuti
	
	ld_fase_risorsa_tot_costo += ld_risorsa_tot_costo
	
next

destroy lds_risorse

ll_fase_risorsa_tot_ore += int(ll_fase_risorsa_tot_minuti / 60)

ll_fase_risorsa_tot_minuti = mod(ll_fase_risorsa_tot_minuti,60)
// ************************************************************

if isnull(ll_fase_tot_ore) then
	ll_fase_tot_ore = 0
end if

if isnull(ll_fase_tot_minuti) then
	ll_fase_tot_minuti = 0
end if

if isnull(ll_fase_risorsa_tot_ore) then
	ll_fase_risorsa_tot_ore = 0
end if

if isnull(ll_fase_risorsa_tot_minuti) then
	ll_fase_risorsa_tot_minuti = 0
end if

update manutenzioni_fasi
set	 data_inizio = :ldt_fase_data_inizio,
		 ora_inizio = :ldt_fase_ora_inizio,
		 data_fine = :ldt_fase_data_fine,
		 ora_fine = :ldt_fase_ora_fine,
		 tot_ore = :ll_fase_tot_ore,
		 tot_minuti = :ll_fase_tot_minuti,
		 tot_ore_ris = :ll_fase_risorsa_tot_ore,
		 tot_minuti_ris = :ll_fase_risorsa_tot_minuti,
		 tot_costo_operai = :ld_fase_tot_costo_operaio,
		 tot_costo_risorse_esterne = :ld_fase_risorsa_tot_costo
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_fase = :fl_progressivo_fase;

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in aggiornamento dati fase " + string(fl_anno_registrazione) + "/" + &
						string(fl_num_registrazione) + " " +  + string(fl_progressivo_fase) + ": " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_aggiorna_richieste (long fl_anno_richiesta, long fl_num_richiesta, ref string fs_errore);string	ls_sql, ls_syntax, ls_error

long		ll_i, ll_ric_tot_ore, ll_ric_tot_minuti, ll_man_tot_ore, ll_man_tot_minuti, &
			ll_anno_man, ll_num_man, ll_risposta_ore, ll_risposta_minuti

datetime ldt_ric_data_inizio, ldt_ric_data_fine, ldt_ric_ora_inizio, ldt_ric_ora_fine, &
			ldt_man_data_inizio, ldt_man_data_fine, ldt_man_ora_inizio, ldt_man_ora_fine, &
			ldt_data_reg, ldt_ora_reg

datastore lds_manut


ls_sql = &
"select anno_registrazione, " + &
"		  num_registrazione, " + &
"		  data_inizio_intervento, " + &
"		  ora_inizio_intervento, " + &
"		  data_fine_intervento, " + &
"		  ora_fine_intervento, " + &
"		  operaio_ore, " + &
"		  operaio_minuti " + &
"from	  manutenzioni " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"		  anno_reg_richiesta = " + string(fl_anno_richiesta) + " and " + &
"		  num_reg_richiesta = " + string(fl_num_richiesta)

ls_sql += " order by anno_registrazione ASC, num_registrazione ASC"

ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	fs_errore = "Errore in impostazione select manutenzioni - richiesta " + string(fl_anno_richiesta) + "/" + &
						string(fl_num_richiesta) + ": " + ls_error
	return -1
end if

lds_manut = create datastore

lds_manut.create(ls_syntax,ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	fs_errore = "Errore in creazione datastore manutenzioni - richiesta " + string(fl_anno_richiesta) + "/" + &
						string(fl_num_richiesta) + ": " + ls_error
	destroy lds_manut
	return -1
end if

lds_manut.settransobject(sqlca)

if lds_manut.retrieve() = -1 then
	fs_errore = "Errore in lettura dati manutenzioni - richiesta " + string(fl_anno_richiesta) + "/" + string(fl_num_richiesta)
	destroy lds_manut
	return -1
end if

setnull(ldt_ric_data_inizio)

setnull(ldt_ric_data_fine)

setnull(ldt_ric_ora_inizio)

setnull(ldt_ric_ora_fine)

ll_ric_tot_ore = 0

ll_ric_tot_minuti = 0

for ll_i = 1 to lds_manut.rowcount()
	
	ldt_man_data_inizio = lds_manut.getitemdatetime(ll_i,"data_inizio_intervento")
	
	ldt_man_data_fine = lds_manut.getitemdatetime(ll_i,"data_fine_intervento")
	
	ldt_man_ora_inizio = lds_manut.getitemdatetime(ll_i,"ora_inizio_intervento")
	
	ldt_man_ora_fine = lds_manut.getitemdatetime(ll_i,"ora_fine_intervento")
	
	ll_man_tot_ore = lds_manut.getitemnumber(ll_i,"operaio_ore")
	
	ll_man_tot_minuti = lds_manut.getitemnumber(ll_i,"operaio_minuti")
	
	if isnull(ll_man_tot_ore) then
		ll_man_tot_ore = 0
	end if
	
	if isnull(ll_man_tot_minuti) then
		ll_man_tot_minuti = 0
	end if		
	
	if isnull(ldt_ric_data_inizio) or ldt_man_data_inizio < ldt_ric_data_inizio then
			ldt_ric_data_inizio = ldt_man_data_inizio
			ldt_ric_ora_inizio = ldt_man_ora_inizio
	elseif ldt_man_data_inizio = ldt_ric_data_inizio then
		if isnull(ldt_ric_ora_inizio) then
			ldt_ric_ora_inizio = ldt_man_ora_inizio
		else
			if ldt_man_ora_inizio < ldt_ric_ora_inizio then
				ldt_ric_ora_inizio = ldt_man_ora_inizio
			end if
		end if
	end if	
	
	
	if isnull(ldt_ric_data_fine) or ldt_man_data_fine > ldt_ric_data_fine then
			ldt_ric_data_fine = ldt_man_data_fine
			ldt_ric_ora_fine = ldt_man_ora_fine
	elseif ldt_man_data_fine = ldt_ric_data_fine then
		if isnull(ldt_ric_ora_fine) then
			ldt_ric_ora_fine = ldt_man_ora_fine
		else
			if ldt_man_ora_fine > ldt_ric_ora_fine then
				ldt_ric_ora_fine = ldt_man_ora_fine
			end if
		end if
	end if		
	
	ll_ric_tot_ore += ll_man_tot_ore
	
	ll_ric_tot_minuti += ll_man_tot_minuti
	
next

destroy lds_manut

ll_ric_tot_ore += int(ll_ric_tot_minuti / 60)

ll_ric_tot_minuti = mod(ll_ric_tot_minuti,60)

select data_passaggio_chiamata,
       ora_passaggio_chiamata
into   :ldt_data_reg,
       :ldt_ora_reg
from   tab_richieste
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_richiesta and
	    num_registrazione  = :fl_num_richiesta;

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in lettura data registrazione richiesta " + string(fl_anno_richiesta) + "/" + &
						string(fl_num_richiesta) + ": " + sqlca.sqlerrtext
	return -1
end if

f_date_h_m( ldt_data_reg, ldt_ora_reg, ldt_ric_data_inizio, ldt_ric_ora_inizio, ll_risposta_ore, ll_risposta_minuti)

if isnull(ll_ric_tot_ore) then
	ll_ric_tot_ore = 0
end if

if isnull(ll_ric_tot_minuti) then
	ll_ric_tot_minuti = 0
end if

if isnull(ll_risposta_ore) then
	ll_risposta_ore = 0
end if

if isnull(ll_risposta_minuti) then
	ll_risposta_minuti = 0
end if

update tab_richieste
set	 data_inizio_attivita = :ldt_ric_data_inizio,
		 ora_inizio_attivita  = :ldt_ric_ora_inizio,
		 data_fine_attivita   = :ldt_ric_data_fine,
		 ora_fine_attivita    = :ldt_ric_ora_fine, 
		 tot_ore_attivita     = :ll_ric_tot_ore,
		 tot_minuti_attivita = :ll_ric_tot_minuti,
		 tot_ore_risposta = :ll_risposta_ore,
		 tot_minuti_risposta = :ll_risposta_minuti
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_richiesta and
		 num_registrazione = :fl_num_richiesta;

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in aggiornamento dati richiesta " + string(fl_anno_richiesta) + "/" + &
						string(fl_num_richiesta) + ": " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public subroutine uof_scadenze_periodo (datetime fdt_data_registrazione, date fd_da, date fd_a, long fl_inizio_1, long fl_fine_1, long fl_inizio_2, long fl_fine_2, string fs_periodicita, double fd_frequenza, ref datetime fdt_scadenze[]);//Donato 10/11/2008
//Funzione creata per la specifica "Integrazione_ORV" da una duplicazione della omonifa funzione ma con 
//l'aggiunta di due nuovi parametri (fd_da_2, fd_a_2)
/*
Nella tipologie_manutenzioni può essere utilizzato un secondo intervallo di tempo:
Inizio Int. 1 - Fine Int. 1 e Inizio Int. 2 - Fine Int. 2 (nelle nuove variabili fd_da_2, fd_a_2)
Inoltre viene letto il parametro aziendale ASM, che rappresenta il n° giorni entro cui arrotondare le scadenze
(per i dettagli vedi la specifica Integrazione_ORV)
*/

datetime ldt_data_att
date ldt_data_arrot_1, ldt_data_arrot_2
long ll_i, ll_arrot_scad_man
datetime ldt_scadenze_tmp[]
datetime ldt_A[], ldt_B[], ldt_C[], ldt_data_corrente
long ll_anno_inizio, ll_mese_inizio, ll_index, ll_gg_toll_scad, ll_tot_ABC, ll_tot_scad_tmp, ll_index2, ll_index3
long ll_gg_inizio_1, ll_mm_inizio_1, ll_gg_fine_1, ll_mm_fine_1
long ll_gg_inizio_2, ll_mm_inizio_2, ll_gg_fine_2, ll_mm_fine_2
datetime ldt_scad_corrente, ldt_data_A, ldt_data_B, ldt_data_C
boolean ib_trovata = false

ldt_data_att=fdt_data_registrazione

do while ldt_data_att<datetime(fd_da)
	ldt_data_att=this.uof_prossima_scadenza(ldt_data_att,fs_periodicita,fd_frequenza)
loop
ll_i=1
do while ldt_data_att<=datetime(fd_a,time("23:59:59"))
	ldt_scadenze_tmp[ll_i]=ldt_data_att
	ldt_data_att=this.uof_prossima_scadenza(ldt_data_att,fs_periodicita,fd_frequenza)
	ll_i=ll_i+1
loop

ll_anno_inizio = year(fd_da)
ll_mese_inizio = month(fd_da)
ll_index = 1
ldt_data_att=fdt_data_registrazione

//--------------------------------------------------
ll_gg_inizio_1 = (fl_inizio_1 / 100)
ll_mm_inizio_1 = mod(fl_inizio_1, 100)
//------
ll_gg_fine_1 = (fl_fine_1 / 100)
ll_mm_fine_1 = mod(fl_fine_1, 100)
//--------------------------------------------------
ll_gg_inizio_2 = (fl_inizio_2 / 100)
ll_mm_inizio_2 = mod(fl_inizio_2, 100)
//------
ll_gg_fine_2 = (fl_fine_2 / 100)
ll_mm_fine_2 = mod(fl_fine_2, 100)
//--------------------------------------------------

if (ll_mm_inizio_1>ll_mese_inizio and ll_mm_fine_1<=ll_mese_inizio) then
	//il periodo inizio è a cavallo di un fine anno e la data inizio è già ad anno nuovo
	ll_anno_inizio = ll_anno_inizio - 1
end if

select numero
into :ll_gg_toll_scad
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'ASM' and flag_parametro = 'N';
if sqlca.sqlcode = 0 then
else
	ll_gg_toll_scad = 0
end if

do while ldt_data_corrente <= datetime(fd_a,time("23:59:59"))
	//riempire il vettore degli intervalli
	if ll_mm_inizio_1 < ll_mm_fine_2 and ll_index >1 then
		ll_anno_inizio += 1
	end if
	ldt_data_corrente = datetime(date(string(ll_anno_inizio) + "-" + string(ll_mm_inizio_1) + "-" + string(ll_gg_inizio_1)),time("00:00:00"))
	if ldt_data_corrente > datetime(fd_a,time("23:59:59")) then exit
	ldt_A[ll_index] = ldt_data_corrente
	
	if ll_mm_fine_1 < ll_mm_inizio_1 then
		ll_anno_inizio += 1
	end if
	ldt_data_corrente = datetime(date(string(ll_anno_inizio) + "-" + string(ll_mm_fine_1) + "-" + string(ll_gg_fine_1)),time("23:59:59"))
	ldt_B[ll_index] = ldt_data_corrente
	ldt_data_corrente = datetime(relativedate( &
														date(string(ll_anno_inizio) + "-" + string(ll_mm_fine_1) + "-" + string(ll_gg_fine_1)), &
																	ll_gg_toll_scad), &
														time("23:59:59"))
	ldt_C[ll_index] = ldt_data_corrente
	
	ldt_data_corrente = ldt_B[ll_index]
	if ldt_data_corrente > datetime(fd_a,time("23:59:59")) then exit
	ll_index += 1
	
	if ll_mm_inizio_2 < ll_mm_fine_1 then
		ll_anno_inizio += 1
	end if
	ldt_data_corrente = datetime(date(string(ll_anno_inizio) + "-" + string(ll_mm_inizio_2) + "-" + string(ll_gg_inizio_2)),time("00:00:00"))
	if ldt_data_corrente > datetime(fd_a,time("23:59:59")) then exit
	ldt_A[ll_index] = ldt_data_corrente
	
	if ll_mm_fine_2 < ll_mm_inizio_2 then
		ll_anno_inizio += 1
	end if
	ldt_data_corrente = datetime(date(string(ll_anno_inizio) + "-" + string(ll_mm_fine_2) + "-" + string(ll_gg_fine_2)),time("23:59:59"))
	ldt_B[ll_index] = ldt_data_corrente
	ldt_data_corrente = datetime(relativedate( &
														date(string(ll_anno_inizio) + "-" + string(ll_mm_fine_2) + "-" + string(ll_gg_fine_2)), &
																	ll_gg_toll_scad), &
														time("23:59:59"))
	ldt_C[ll_index] = ldt_data_corrente
	ldt_data_corrente = ldt_B[ll_index]								
	ll_index += 1	
loop

ll_tot_scad_tmp = upperbound(ldt_scadenze_tmp)
ll_tot_ABC = upperbound(ldt_A)
ll_index3 = 0

for ll_index = 1 to ll_tot_scad_tmp
	ldt_scad_corrente = ldt_scadenze_tmp[ll_index]
	
	for ll_index2 = 1 to ll_tot_ABC
		ldt_data_A = ldt_A[ll_index2]
		ldt_data_B = ldt_B[ll_index2]
		if ldt_data_A <= ldt_scad_corrente and ldt_scad_corrente <= ldt_data_B then
			//cade in un intervallo
			
			//esiste già nel vettore?
			if not uof_esiste_scadenza(fdt_scadenze, ldt_scad_corrente) then
				ll_index3 += 1
				fdt_scadenze[ll_index3] = ldt_scad_corrente
			end if
			ib_trovata = true
			exit
		end if
		
	next
	if not ib_trovata then
		//controlla se cade entro N gg (prm ASM) dopo un intervallo
		for ll_index2 = 1 to ll_tot_ABC
			ldt_data_B = ldt_B[ll_index2]
			ldt_data_C = ldt_C[ll_index2]			
			if ldt_data_B < ldt_scad_corrente and ldt_scad_corrente <= ldt_data_C then
				//cade N gg dopo un intervallo
				
				//impostare la data fine del periodo corrente se non esiste già
				if not uof_esiste_scadenza(fdt_scadenze, ldt_data_B) then
					ll_index3 += 1
					fdt_scadenze[ll_index3] = ldt_data_B
				end if
				ib_trovata = true
				exit
			end if
		next
		if not ib_trovata then
			//si dovrebbe impostare la data inizio del primo periodo futuro disponibile
			//ma prima bisogna beccare l'intervallo interessato e spostarsi sulla data inizio
			//del primo intervallo futuro disponibile
			for ll_index2 = 1 to ll_tot_ABC - 1
				ldt_data_C = ldt_C[ll_index2]
				ldt_data_A = ldt_A[ll_index2 + 1]
				
				if ldt_data_C < ldt_scad_corrente and ldt_scad_corrente < ldt_data_A then
					//trovato l'intervallo interessato
					//cerca di impostare con la data inizio dell'intervallo successivo
					if not uof_esiste_scadenza(fdt_scadenze, ldt_data_A) then
						ll_index3 += 1
						fdt_scadenze[ll_index3] = ldt_data_A
					end if
				end if
			next						
		else
			ib_trovata = false
		end if		
	else
		ib_trovata = false
	end if
next

end subroutine

public function boolean uof_esiste_scadenza (datetime fdt_scadenze[], datetime fdt_data);//Donato 11-11-2008
//La funzione cerca una data "fdt_data" all'interno di un vettore di date "fdt_scadenze"
//torna true se la data già esiste nel vettore altrimenti torna false

long ll_tot, ll_index
date ldt_date_1, ldt_date_2

ll_tot = upperbound(fdt_scadenze)
ldt_date_1=date(fdt_data)

for ll_index = 1 to ll_tot
	ldt_date_2=date(fdt_scadenze[ll_index])
	if ldt_date_1 = ldt_date_2 then
		return true
	end if
next

return false
end function

public function datetime uof_scadenza_semplice_con_intervalli (datetime fdt_data_registrazione, long fl_inizio_1, long fl_fine_1, long fl_inizio_2, long fl_fine_2);//Donato 11/11/2008
//Funzione creata per la specifica "Integrazione_ORV"
/*
Nella tipologie_manutenzioni può essere utilizzato un secondo intervallo di tempo:
Inizio Int. 1 - Fine Int. 1 e Inizio Int. 2 - Fine Int. 2 (nelle nuove variabili fd_da_2, fd_a_2)
Inoltre viene letto il parametro aziendale ASM, che rappresenta il n° giorni entro cui arrotondare le scadenze
(per i dettagli vedi la specifica Integrazione_ORV)

data una data ne ritorna un'altra:
	- la stessa se ricade in uno degli intervalli
	- la data fine di un intervallo se ricade entro la tolleranza
	- altrimenti la data inizio del primo periodo futuro disponibile
*/

datetime ldt_data_inizio, ldt_data_fine, ldt_return
datetime ldt_A[], ldt_B[], ldt_C[], ldt_data_corrente
long ll_anno_inizio, ll_mese_inizio, ll_index, ll_gg_toll_scad, ll_tot_ABC, ll_tot_scad_tmp, ll_index2, ll_index3
long ll_gg_inizio_1, ll_mm_inizio_1, ll_gg_fine_1, ll_mm_fine_1
long ll_gg_inizio_2, ll_mm_inizio_2, ll_gg_fine_2, ll_mm_fine_2
datetime ldt_scad_corrente, ldt_data_A, ldt_data_B, ldt_data_C

setnull(ldt_return)

//crea un range un anno prima e 600 gg dopo
ldt_data_inizio = datetime(relativedate(date(fdt_data_registrazione), - 365),time("00:00:00")) 
ldt_data_fine = datetime(relativedate(date(fdt_data_registrazione), 600),time("23:59:59")) 

ll_anno_inizio = year(date(ldt_data_inizio))
ll_mese_inizio = month(date(ldt_data_inizio))
ll_index = 1
//ldt_data_att=fdt_data_registrazione
//
//--------------------------------------------------
ll_gg_inizio_1 = (fl_inizio_1 / 100)
ll_mm_inizio_1 = mod(fl_inizio_1, 100)
//------
ll_gg_fine_1 = (fl_fine_1 / 100)
ll_mm_fine_1 = mod(fl_fine_1, 100)
//--------------------------------------------------
ll_gg_inizio_2 = (fl_inizio_2 / 100)
ll_mm_inizio_2 = mod(fl_inizio_2, 100)
//------
ll_gg_fine_2 = (fl_fine_2 / 100)
ll_mm_fine_2 = mod(fl_fine_2, 100)
//--------------------------------------------------

if (ll_mm_inizio_1>ll_mese_inizio and ll_mm_fine_1<=ll_mese_inizio) then
	//il periodo inizio è a cavallo di un fine anno e la data inizio è già ad anno nuovo
	ll_anno_inizio = ll_anno_inizio - 1
end if

select numero
into :ll_gg_toll_scad
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'ASM' and flag_parametro = 'N';
if sqlca.sqlcode = 0 then
else
	ll_gg_toll_scad = 0
end if

do while ldt_data_corrente <= ldt_data_fine
	//riempire il vettore degli intervalli
	if ll_mm_inizio_1 < ll_mm_fine_2 and ll_index >1 then
		ll_anno_inizio += 1
	end if
	ldt_data_corrente = datetime(date(string(ll_anno_inizio) + "-" + string(ll_mm_inizio_1) + "-" + string(ll_gg_inizio_1)),time("00:00:00"))
	if ldt_data_corrente > ldt_data_fine then exit
	ldt_A[ll_index] = ldt_data_corrente
	
	if ll_mm_fine_1 < ll_mm_inizio_1 then
		ll_anno_inizio += 1
	end if
	ldt_data_corrente = datetime(date(string(ll_anno_inizio) + "-" + string(ll_mm_fine_1) + "-" + string(ll_gg_fine_1)),time("23:59:59"))
	ldt_B[ll_index] = ldt_data_corrente
	ldt_data_corrente = datetime(relativedate( &
														date(string(ll_anno_inizio) + "-" + string(ll_mm_fine_1) + "-" + string(ll_gg_fine_1)), &
																	ll_gg_toll_scad), &
														time("23:59:59"))
	ldt_C[ll_index] = ldt_data_corrente
	
	ldt_data_corrente = ldt_B[ll_index]
	if ldt_data_corrente > ldt_data_fine then exit
	ll_index += 1
	
	if ll_mm_inizio_2 < ll_mm_fine_1 then
		ll_anno_inizio += 1
	end if
	ldt_data_corrente = datetime(date(string(ll_anno_inizio) + "-" + string(ll_mm_inizio_2) + "-" + string(ll_gg_inizio_2)),time("00:00:00"))
	if ldt_data_corrente > ldt_data_fine then exit
	ldt_A[ll_index] = ldt_data_corrente
	
	if ll_mm_fine_2 < ll_mm_inizio_2 then
		ll_anno_inizio += 1
	end if
	ldt_data_corrente = datetime(date(string(ll_anno_inizio) + "-" + string(ll_mm_fine_2) + "-" + string(ll_gg_fine_2)),time("23:59:59"))
	ldt_B[ll_index] = ldt_data_corrente
	ldt_data_corrente = datetime(relativedate( &
														date(string(ll_anno_inizio) + "-" + string(ll_mm_fine_2) + "-" + string(ll_gg_fine_2)), &
																	ll_gg_toll_scad), &
														time("23:59:59"))
	ldt_C[ll_index] = ldt_data_corrente
	ldt_data_corrente = ldt_B[ll_index]								
	ll_index += 1	
loop

ll_tot_ABC = upperbound(ldt_A)
ldt_scad_corrente = fdt_data_registrazione
	
for ll_index2 = 1 to ll_tot_ABC
	ldt_data_A = ldt_A[ll_index2]
	ldt_data_B = ldt_B[ll_index2]
	if ldt_data_A <= ldt_scad_corrente and ldt_scad_corrente <= ldt_data_B then
		//cade in un intervallo		
		return ldt_scad_corrente
	end if	
next
for ll_index2 = 1 to ll_tot_ABC
	ldt_data_B = ldt_B[ll_index2]
	ldt_data_C = ldt_C[ll_index2]			
	if ldt_data_B < ldt_scad_corrente and ldt_scad_corrente <= ldt_data_C then
		//cade N gg dopo un intervallo
		return ldt_data_B			
	end if
next
for ll_index2 = 1 to ll_tot_ABC - 1
	ldt_data_C = ldt_C[ll_index2]
	ldt_data_A = ldt_A[ll_index2 + 1]
	
	if ldt_data_C < ldt_scad_corrente and ldt_scad_corrente < ldt_data_A then
		//non cade neanche N gg dopo un intervallo
		return ldt_data_A
	end if
next

//se arrivi qui vuol dire che non hai beccato niente: problema segnalare!
//tornerà null
return ldt_return
end function

public function integer uof_crea_manut_progr_semplice (long fn_anno_programma, long fn_num_programma, datetime fdt_data_registrazione, ref string fs_messaggio);string ls_null, ls_des_intervento, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_tipo_intervento, ls_note_idl, &
       ls_flag_ricambio_codificato, ls_cod_kit_ricambio, ls_ricambio_non_codificato, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
		 ls_cod_risorsa_esterna, ls_rif_contratto_assistenza, ls_rif_ordine_acq_risorsa, ls_cod_primario, ls_cod_misura, &
		 ls_cod_versione, ls_ricambi[], ls_des_prodotto, ls_cod_ricambio, ls_des_ricambio, ls_blocco, ls_flag_abr
long ll_anno_registrazione, ll_num_registrazione, ll_null, ll_operaio_ore_previste, ll_operaio_minuti_previsti, &
     ll_risorsa_ore_previste, ll_risorsa_minuti_previsti, ll_anno_contratto_assistenza, ll_num_contratto_assistenza, &
	  ll_anno_ord_acq_risorsa, ll_num_ord_acq_risorsa, ll_prog_riga_ord_acq_risorsa, ll_num_reg_lista, ll_prog_liste_con_comp, &
	  ll_num_versione, ll_num_edizione, LL_CONT, ll_j, ll_prog_ricambio
dec{4} ld_quan_ricambio, ld_costo_unitario_ricambio, ld_costo_orario_operaio, ld_risorsa_costo_orario, &
       ld_risorsa_costi_aggiuntivi,  ld_valore_min_taratura, ld_valore_max_taratura, ld_valore_atteso, ld_tot_costo_intervento, &
		 ld_quantita[], ld_prezzo, ld_prezzo_ricambio
datetime ldt_data_registrazione, ldt_null

dec{0}   ld_data_inizio_val_1, ld_data_inizio_val_2, ld_data_fine_val_1, ld_data_fine_val_2

setnull(ls_null)
setnull(ll_null)
setnull(ldt_null)

select count(*)
into   :ll_cont
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_reg_programma = :fn_anno_programma and
       num_reg_programma = :fn_num_programma and
		 flag_eseguito = 'N';
if ll_cont > 0 and not isnull(ll_cont) then
	if g_mb.messagebox("Manutenzioni & Tarature","Attenzione: con questo programma di manutenzione risultano esistere ancora manutenzioni non eseguite.~r~nProseguo lo stesso?",Question!,YesNo!,2) = 2 then
		fs_messaggio = ""
		return -1
	end if
end if		 
//*******claudia 27/11/06 se presente il parametro multiaziendale abr allora esiste il campo flag_visualizza intranet e lo imposta
//******modifica fatta per abor specifica stampi
select flag
into :ls_flag_abr
from parametri 
where cod_parametro = 'ABR' and
		flag_parametro = 'F';

//Donato 04-02-2009 modifica per evitare che dia errore quando non trova il parametro ABR
//che tra l'altro è una personalizzazione del cliente ABOR
if sqlca.sqlcode <> 0 then
	ls_flag_abr = "N"
end if
/*
if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore durante il controllo di un paremtro ABR ." + sqlca.sqlerrtext)
		setpointer(arrow!)
		return -1
end if
*/
//fine modifica ---------------------------------------------------------------------

if ls_flag_abr = 'S' then
	select  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  note_idl,   
			  flag_ricambio_codificato,   
			  cod_ricambio,
			  cod_versione,
			  ricambio_non_codificato,   
			  quan_ricambio,   
			  costo_unitario_ricambio,   
			  cod_operaio,   
			  costo_orario_operaio,   
			  operaio_ore_previste,   
			  operaio_minuti_previsti,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  risorsa_ore_previste,   
			  risorsa_minuti_previsti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi,   
			  anno_contratto_assistenza,   
			  num_contratto_assistenza,   
			  anno_ord_acq_risorsa,   
			  num_ord_acq_risorsa,   
			  prog_riga_ord_acq_risorsa,   
			  rif_contratto_assistenza,   
			  rif_ordine_acq_risorsa,   
			  cod_primario,   
			  cod_misura,   
			  valore_min_taratura,   
			  valore_max_taratura,   
			  valore_atteso,   
			  tot_costo_intervento,   
			  num_reg_lista,   
			  prog_liste_con_comp,   
			  num_versione,   
			  num_edizione,
			  data_inizio_val_1,
			  data_fine_val_1,
			  data_inizio_val_2,
			  data_fine_val_2, 
			  flag_pubblica_intranet
	into    :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_flag_tipo_intervento,   
			  :ls_note_idl,   
			  :ls_flag_ricambio_codificato,   
			  :ls_cod_kit_ricambio,
			  :ls_cod_versione,
			  :ls_ricambio_non_codificato,   
			  :ld_quan_ricambio,   
			  :ld_costo_unitario_ricambio,   
			  :ls_cod_operaio,   
			  :ld_costo_orario_operaio,   
			  :ll_operaio_ore_previste,   
			  :ll_operaio_minuti_previsti,   
			  :ls_cod_cat_risorse_esterne,   
			  :ls_cod_risorsa_esterna,   
			  :ll_risorsa_ore_previste,   
			  :ll_risorsa_minuti_previsti,   
			  :ld_risorsa_costo_orario,   
			  :ld_risorsa_costi_aggiuntivi,   
			  :ll_anno_contratto_assistenza,   
			  :ll_num_contratto_assistenza,   
			  :ll_anno_ord_acq_risorsa,   
			  :ll_num_ord_acq_risorsa,   
			  :ll_prog_riga_ord_acq_risorsa,   
			  :ls_rif_contratto_assistenza,   
			  :ls_rif_ordine_acq_risorsa,   
			  :ls_cod_primario,   
			  :ls_cod_misura,   
			  :ld_valore_min_taratura,   
			  :ld_valore_max_taratura,   
			  :ld_valore_atteso,   
			  :ld_tot_costo_intervento,   
			  :ll_num_reg_lista,   
			  :ll_prog_liste_con_comp,   
			  :ll_num_versione,   
			  :ll_num_edizione,
			  :ld_data_inizio_val_1,
			  :ld_data_fine_val_1,
			  :ld_data_inizio_val_2,
			  :ld_data_fine_val_2,
			  :ls_flag_abr
	from    programmi_manutenzione
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fn_anno_programma and
			  num_registrazione  = :fn_num_programma;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in ricerca programma manutenzione " + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in SELECT tabella programmi manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
else
		select  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  note_idl,   
			  flag_ricambio_codificato,   
			  cod_ricambio,
			  cod_versione,
			  ricambio_non_codificato,   
			  quan_ricambio,   
			  costo_unitario_ricambio,   
			  cod_operaio,   
			  costo_orario_operaio,   
			  operaio_ore_previste,   
			  operaio_minuti_previsti,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  risorsa_ore_previste,   
			  risorsa_minuti_previsti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi,   
			  anno_contratto_assistenza,   
			  num_contratto_assistenza,   
			  anno_ord_acq_risorsa,   
			  num_ord_acq_risorsa,   
			  prog_riga_ord_acq_risorsa,   
			  rif_contratto_assistenza,   
			  rif_ordine_acq_risorsa,   
			  cod_primario,   
			  cod_misura,   
			  valore_min_taratura,   
			  valore_max_taratura,   
			  valore_atteso,   
			  tot_costo_intervento,   
			  num_reg_lista,   
			  prog_liste_con_comp,   
			  num_versione,   
			  num_edizione,
			  data_inizio_val_1,
			  data_fine_val_1,
			  data_inizio_val_2,
			  data_fine_val_2
	into    :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_flag_tipo_intervento,   
			  :ls_note_idl,   
			  :ls_flag_ricambio_codificato,   
			  :ls_cod_kit_ricambio,
			  :ls_cod_versione,
			  :ls_ricambio_non_codificato,   
			  :ld_quan_ricambio,   
			  :ld_costo_unitario_ricambio,   
			  :ls_cod_operaio,   
			  :ld_costo_orario_operaio,   
			  :ll_operaio_ore_previste,   
			  :ll_operaio_minuti_previsti,   
			  :ls_cod_cat_risorse_esterne,   
			  :ls_cod_risorsa_esterna,   
			  :ll_risorsa_ore_previste,   
			  :ll_risorsa_minuti_previsti,   
			  :ld_risorsa_costo_orario,   
			  :ld_risorsa_costi_aggiuntivi,   
			  :ll_anno_contratto_assistenza,   
			  :ll_num_contratto_assistenza,   
			  :ll_anno_ord_acq_risorsa,   
			  :ll_num_ord_acq_risorsa,   
			  :ll_prog_riga_ord_acq_risorsa,   
			  :ls_rif_contratto_assistenza,   
			  :ls_rif_ordine_acq_risorsa,   
			  :ls_cod_primario,   
			  :ls_cod_misura,   
			  :ld_valore_min_taratura,   
			  :ld_valore_max_taratura,   
			  :ld_valore_atteso,   
			  :ld_tot_costo_intervento,   
			  :ll_num_reg_lista,   
			  :ll_prog_liste_con_comp,   
			  :ll_num_versione,   
			  :ll_num_edizione,
			  :ld_data_inizio_val_1,
			  :ld_data_fine_val_1,
			  :ld_data_inizio_val_2,
			  :ld_data_fine_val_2
	from    programmi_manutenzione
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fn_anno_programma and
			  num_registrazione  = :fn_num_programma;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in ricerca programma manutenzione " + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in SELECT tabella programmi manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

end if
// Modifica Michele per gestione blocco tipologie di manutenzione 01/04/2003

select flag_blocco
into   :ls_blocco
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in lettura dati tipologia manutenzione: " + sqlca.sqlerrtext
	return -1
elseif ls_blocco = "S" then
	fs_messaggio = "Questa tipologia di manutenzione è stata bloccata: operazione interrotta"
	return -1
end if

// Fine Modifica

ll_anno_registrazione = f_anno_esercizio()
select max(num_registrazione)
into   :ll_num_registrazione
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione;
if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

ldt_data_registrazione = datetime(today(),00:00:00)
if ls_flag_abr = 'S' then
	insert into manutenzioni  
				  ( 
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_guasto,   
				  flag_ordinario,   
				  flag_eseguito,   
				  anno_non_conf,   
				  num_non_conf,   
				  data_prossimo_intervento,   
				  flag_budget,   
				  anno_reg_programma,   
				  num_reg_programma,
				  des_intervento,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  flag_tipo_intervento,   
				  note_idl,   
				  flag_ricambio_codificato,   
				  cod_ricambio,
				  cod_versione,
				  ricambio_non_codificato,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_operaio,   
				  costo_orario_operaio,   
				  operaio_ore,   
				  operaio_minuti,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  risorsa_ore,   
				  risorsa_minuti,   
				  risorsa_costo_orario,   
				  risorsa_costi_aggiuntivi,   
				  anno_contratto_assistenza,   
				  num_contratto_assistenza,   
				  anno_ord_acq_risorsa,   
				  num_ord_acq_risorsa,   
				  prog_riga_ord_acq_risorsa,   
				  rif_contratto_assistenza,   
				  rif_ordine_acq_risorsa,   
				  cod_primario,   
				  cod_misura,   
				  valore_min_taratura,   
				  valore_max_taratura,   
				  valore_atteso,   
				  tot_costo_intervento,   
				  num_reg_lista,   
				  prog_liste_con_comp,   
				  num_versione,   
				  num_edizione,
				  flag_pubblica_intranet
				  )  
				values
				  (
				  :s_cs_xx.cod_azienda,
				  :ll_anno_registrazione,
				  :ll_num_registrazione,
				  :ldt_data_registrazione,
				  :ls_null,
				  'S',
				  'N',
				  :ll_null,
				  :ll_null,
				  :ldt_null,
				  'N',
				  :fn_anno_programma,
				  :fn_num_programma,
				  :ls_des_intervento,   
				  :ls_cod_attrezzatura,   
				  :ls_cod_tipo_manutenzione,   
				  :ls_flag_tipo_intervento,   
				  :ls_note_idl,   
				  :ls_flag_ricambio_codificato,   
				  :ls_cod_kit_ricambio,
				  :ls_cod_versione,
				  :ls_ricambio_non_codificato,   
				  :ld_quan_ricambio,   
				  :ld_costo_unitario_ricambio,   
				  :ls_cod_operaio,   
				  :ld_costo_orario_operaio,   
				  :ll_operaio_ore_previste,   
				  :ll_operaio_minuti_previsti,   
				  :ls_cod_cat_risorse_esterne,   
				  :ls_cod_risorsa_esterna,   
				  :ll_risorsa_ore_previste,   
				  :ll_risorsa_minuti_previsti,   
				  :ld_risorsa_costo_orario,   
				  :ld_risorsa_costi_aggiuntivi,   
				  :ll_anno_contratto_assistenza,   
				  :ll_num_contratto_assistenza,   
				  :ll_anno_ord_acq_risorsa,   
				  :ll_num_ord_acq_risorsa,   
				  :ll_prog_riga_ord_acq_risorsa,   
				  :ls_rif_contratto_assistenza,   
				  :ls_rif_ordine_acq_risorsa,   
				  :ls_cod_primario,   
				  :ls_cod_misura,   
				  :ld_valore_min_taratura,   
				  :ld_valore_max_taratura,   
				  :ld_valore_atteso,   
				  :ld_tot_costo_intervento,   
				  :ll_num_reg_lista,   
				  :ll_prog_liste_con_comp,   
				  :ll_num_versione,   
				  :ll_num_edizione,
				  :ls_flag_abr
				  );
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in inserimento manutenzione programmata" + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
else
		insert into manutenzioni  
				  ( 
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_guasto,   
				  flag_ordinario,   
				  flag_eseguito,   
				  anno_non_conf,   
				  num_non_conf,   
				  data_prossimo_intervento,   
				  flag_budget,   
				  anno_reg_programma,   
				  num_reg_programma,
				  des_intervento,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  flag_tipo_intervento,   
				  note_idl,   
				  flag_ricambio_codificato,   
				  cod_ricambio,
				  cod_versione,
				  ricambio_non_codificato,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_operaio,   
				  costo_orario_operaio,   
				  operaio_ore,   
				  operaio_minuti,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  risorsa_ore,   
				  risorsa_minuti,   
				  risorsa_costo_orario,   
				  risorsa_costi_aggiuntivi,   
				  anno_contratto_assistenza,   
				  num_contratto_assistenza,   
				  anno_ord_acq_risorsa,   
				  num_ord_acq_risorsa,   
				  prog_riga_ord_acq_risorsa,   
				  rif_contratto_assistenza,   
				  rif_ordine_acq_risorsa,   
				  cod_primario,   
				  cod_misura,   
				  valore_min_taratura,   
				  valore_max_taratura,   
				  valore_atteso,   
				  tot_costo_intervento,   
				  num_reg_lista,   
				  prog_liste_con_comp,   
				  num_versione,   
				  num_edizione
				  )  
				values
				  (
				  :s_cs_xx.cod_azienda,
				  :ll_anno_registrazione,
				  :ll_num_registrazione,
				  :ldt_data_registrazione,
				  :ls_null,
				  'S',
				  'N',
				  :ll_null,
				  :ll_null,
				  :ldt_null,
				  'N',
				  :fn_anno_programma,
				  :fn_num_programma,
				  :ls_des_intervento,   
				  :ls_cod_attrezzatura,   
				  :ls_cod_tipo_manutenzione,   
				  :ls_flag_tipo_intervento,   
				  :ls_note_idl,   
				  :ls_flag_ricambio_codificato,   
				  :ls_cod_kit_ricambio,
				  :ls_cod_versione,
				  :ls_ricambio_non_codificato,   
				  :ld_quan_ricambio,   
				  :ld_costo_unitario_ricambio,   
				  :ls_cod_operaio,   
				  :ld_costo_orario_operaio,   
				  :ll_operaio_ore_previste,   
				  :ll_operaio_minuti_previsti,   
				  :ls_cod_cat_risorse_esterne,   
				  :ls_cod_risorsa_esterna,   
				  :ll_risorsa_ore_previste,   
				  :ll_risorsa_minuti_previsti,   
				  :ld_risorsa_costo_orario,   
				  :ld_risorsa_costi_aggiuntivi,   
				  :ll_anno_contratto_assistenza,   
				  :ll_num_contratto_assistenza,   
				  :ll_anno_ord_acq_risorsa,   
				  :ll_num_ord_acq_risorsa,   
				  :ll_prog_riga_ord_acq_risorsa,   
				  :ls_rif_contratto_assistenza,   
				  :ls_rif_ordine_acq_risorsa,   
				  :ls_cod_primario,   
				  :ls_cod_misura,   
				  :ld_valore_min_taratura,   
				  :ld_valore_max_taratura,   
				  :ld_valore_atteso,   
				  :ld_tot_costo_intervento,   
				  :ll_num_reg_lista,   
				  :ll_prog_liste_con_comp,   
				  :ll_num_versione,   
				  :ll_num_edizione
				  );
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in inserimento manutenzione programmata" + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

end if
// **** michela 02/11/2004: inserimento operatore e risorse esterne

long ll_ret

ll_ret = uof_crea_operatore_risorse( ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ll_anno_registrazione, ll_num_registrazione, fs_messaggio)

if ll_ret <> 0 then return ll_ret

/////////////////////////////////////////// inserimento ricambi ////////////////////////////////////////////////////////

string ls_messaggio

uo_ricambi luo_ricambi

luo_ricambi = create uo_ricambi

if luo_ricambi.uof_ricambi_manutenzione(ll_anno_registrazione,ll_num_registrazione,ls_messaggio) <> 0 then
	g_mb.messagebox("OMNIA",ls_messaggio)
	destroy luo_ricambi
	return -1
end if

destroy luo_ricambi

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return 0
end function

public function integer uof_crea_manut_progr_semplice2 (long fn_anno_programma, long fn_num_programma, datetime fdt_data_registrazione, ref string fs_messaggio);string ls_null, ls_des_intervento, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_tipo_intervento, ls_note_idl, &
       ls_flag_ricambio_codificato, ls_cod_kit_ricambio, ls_ricambio_non_codificato, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
		 ls_cod_risorsa_esterna, ls_rif_contratto_assistenza, ls_rif_ordine_acq_risorsa, ls_cod_primario, ls_cod_misura, &
		 ls_cod_versione, ls_ricambi[], ls_des_prodotto, ls_cod_ricambio, ls_des_ricambio, ls_blocco, ls_flag_abr
long ll_anno_registrazione, ll_num_registrazione, ll_null, ll_operaio_ore_previste, ll_operaio_minuti_previsti, &
     ll_risorsa_ore_previste, ll_risorsa_minuti_previsti, ll_anno_contratto_assistenza, ll_num_contratto_assistenza, &
	  ll_anno_ord_acq_risorsa, ll_num_ord_acq_risorsa, ll_prog_riga_ord_acq_risorsa, ll_num_reg_lista, ll_prog_liste_con_comp, &
	  ll_num_versione, ll_num_edizione, LL_CONT, ll_j, ll_prog_ricambio
dec{4} ld_quan_ricambio, ld_costo_unitario_ricambio, ld_costo_orario_operaio, ld_risorsa_costo_orario, &
       ld_risorsa_costi_aggiuntivi,  ld_valore_min_taratura, ld_valore_max_taratura, ld_valore_atteso, ld_tot_costo_intervento, &
		 ld_quantita[], ld_prezzo, ld_prezzo_ricambio
datetime ldt_data_registrazione, ldt_null

dec{0}   ld_data_inizio_val_1, ld_data_inizio_val_2, ld_data_fine_val_1, ld_data_fine_val_2

setnull(ls_null)
setnull(ll_null)
setnull(ldt_null)

select count(*)
into   :ll_cont
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_reg_programma = :fn_anno_programma and
       num_reg_programma = :fn_num_programma and
		 flag_eseguito = 'N';
if ll_cont > 0 and not isnull(ll_cont) then
	if g_mb.messagebox("Manutenzioni & Tarature","Attenzione: con questo programma di manutenzione risultano esistere ancora manutenzioni non eseguite.~r~nProseguo lo stesso?",Question!,YesNo!,2) = 2 then
		fs_messaggio = ""
		return -1
	end if
end if		 
//*******claudia 27/11/06 se presente il parametro multiaziendale abr allora esiste il campo flag_visualizza intranet e lo imposta
//******modifica fatta per abor specifica stampi
select flag
into :ls_flag_abr
from parametri 
where cod_parametro = 'ABR' and
		flag_parametro = 'F';

//Donato 04-02-2009 modifica per evitare che dia errore quando non trova il parametro ABR
//che tra l'altro è una personalizzazione del cliente ABOR
if sqlca.sqlcode <> 0 then
	ls_flag_abr = "N"
end if
/*
if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore durante il controllo di un paremtro ABR ." + sqlca.sqlerrtext)
		setpointer(arrow!)
		return -1
end if
*/
//fine modifica ---------------------------------------------------------------------

if ls_flag_abr = 'S' then
	select  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  note_idl,   
			  flag_ricambio_codificato,   
			  cod_ricambio,
			  cod_versione,
			  ricambio_non_codificato,   
			  quan_ricambio,   
			  costo_unitario_ricambio,   
			  cod_operaio,   
			  costo_orario_operaio,   
			  operaio_ore_previste,   
			  operaio_minuti_previsti,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  risorsa_ore_previste,   
			  risorsa_minuti_previsti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi,   
			  anno_contratto_assistenza,   
			  num_contratto_assistenza,   
			  anno_ord_acq_risorsa,   
			  num_ord_acq_risorsa,   
			  prog_riga_ord_acq_risorsa,   
			  rif_contratto_assistenza,   
			  rif_ordine_acq_risorsa,   
			  cod_primario,   
			  cod_misura,   
			  valore_min_taratura,   
			  valore_max_taratura,   
			  valore_atteso,   
			  tot_costo_intervento,   
			  num_reg_lista,   
			  prog_liste_con_comp,   
			  num_versione,   
			  num_edizione,
			  data_inizio_val_1,
			  data_fine_val_1,
			  data_inizio_val_2,
			  data_fine_val_2, 
			  flag_pubblica_intranet
	into    :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_flag_tipo_intervento,   
			  :ls_note_idl,   
			  :ls_flag_ricambio_codificato,   
			  :ls_cod_kit_ricambio,
			  :ls_cod_versione,
			  :ls_ricambio_non_codificato,   
			  :ld_quan_ricambio,   
			  :ld_costo_unitario_ricambio,   
			  :ls_cod_operaio,   
			  :ld_costo_orario_operaio,   
			  :ll_operaio_ore_previste,   
			  :ll_operaio_minuti_previsti,   
			  :ls_cod_cat_risorse_esterne,   
			  :ls_cod_risorsa_esterna,   
			  :ll_risorsa_ore_previste,   
			  :ll_risorsa_minuti_previsti,   
			  :ld_risorsa_costo_orario,   
			  :ld_risorsa_costi_aggiuntivi,   
			  :ll_anno_contratto_assistenza,   
			  :ll_num_contratto_assistenza,   
			  :ll_anno_ord_acq_risorsa,   
			  :ll_num_ord_acq_risorsa,   
			  :ll_prog_riga_ord_acq_risorsa,   
			  :ls_rif_contratto_assistenza,   
			  :ls_rif_ordine_acq_risorsa,   
			  :ls_cod_primario,   
			  :ls_cod_misura,   
			  :ld_valore_min_taratura,   
			  :ld_valore_max_taratura,   
			  :ld_valore_atteso,   
			  :ld_tot_costo_intervento,   
			  :ll_num_reg_lista,   
			  :ll_prog_liste_con_comp,   
			  :ll_num_versione,   
			  :ll_num_edizione,
			  :ld_data_inizio_val_1,
			  :ld_data_fine_val_1,
			  :ld_data_inizio_val_2,
			  :ld_data_fine_val_2,
			  :ls_flag_abr
	from    programmi_manutenzione
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fn_anno_programma and
			  num_registrazione  = :fn_num_programma;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in ricerca programma manutenzione " + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in SELECT tabella programmi manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
else
		select  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  note_idl,   
			  flag_ricambio_codificato,   
			  cod_ricambio,
			  cod_versione,
			  ricambio_non_codificato,   
			  quan_ricambio,   
			  costo_unitario_ricambio,   
			  cod_operaio,   
			  costo_orario_operaio,   
			  operaio_ore_previste,   
			  operaio_minuti_previsti,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  risorsa_ore_previste,   
			  risorsa_minuti_previsti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi,   
			  anno_contratto_assistenza,   
			  num_contratto_assistenza,   
			  anno_ord_acq_risorsa,   
			  num_ord_acq_risorsa,   
			  prog_riga_ord_acq_risorsa,   
			  rif_contratto_assistenza,   
			  rif_ordine_acq_risorsa,   
			  cod_primario,   
			  cod_misura,   
			  valore_min_taratura,   
			  valore_max_taratura,   
			  valore_atteso,   
			  tot_costo_intervento,   
			  num_reg_lista,   
			  prog_liste_con_comp,   
			  num_versione,   
			  num_edizione,
			  data_inizio_val_1,
			  data_fine_val_1,
			  data_inizio_val_2,
			  data_fine_val_2
	into    :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_flag_tipo_intervento,   
			  :ls_note_idl,   
			  :ls_flag_ricambio_codificato,   
			  :ls_cod_kit_ricambio,
			  :ls_cod_versione,
			  :ls_ricambio_non_codificato,   
			  :ld_quan_ricambio,   
			  :ld_costo_unitario_ricambio,   
			  :ls_cod_operaio,   
			  :ld_costo_orario_operaio,   
			  :ll_operaio_ore_previste,   
			  :ll_operaio_minuti_previsti,   
			  :ls_cod_cat_risorse_esterne,   
			  :ls_cod_risorsa_esterna,   
			  :ll_risorsa_ore_previste,   
			  :ll_risorsa_minuti_previsti,   
			  :ld_risorsa_costo_orario,   
			  :ld_risorsa_costi_aggiuntivi,   
			  :ll_anno_contratto_assistenza,   
			  :ll_num_contratto_assistenza,   
			  :ll_anno_ord_acq_risorsa,   
			  :ll_num_ord_acq_risorsa,   
			  :ll_prog_riga_ord_acq_risorsa,   
			  :ls_rif_contratto_assistenza,   
			  :ls_rif_ordine_acq_risorsa,   
			  :ls_cod_primario,   
			  :ls_cod_misura,   
			  :ld_valore_min_taratura,   
			  :ld_valore_max_taratura,   
			  :ld_valore_atteso,   
			  :ld_tot_costo_intervento,   
			  :ll_num_reg_lista,   
			  :ll_prog_liste_con_comp,   
			  :ll_num_versione,   
			  :ll_num_edizione,
			  :ld_data_inizio_val_1,
			  :ld_data_fine_val_1,
			  :ld_data_inizio_val_2,
			  :ld_data_fine_val_2
	from    programmi_manutenzione
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fn_anno_programma and
			  num_registrazione  = :fn_num_programma;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in ricerca programma manutenzione " + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in SELECT tabella programmi manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

end if
// Modifica Michele per gestione blocco tipologie di manutenzione 01/04/2003

select flag_blocco
into   :ls_blocco
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in lettura dati tipologia manutenzione: " + sqlca.sqlerrtext
	return -1
elseif ls_blocco = "S" then
	fs_messaggio = "Questa tipologia di manutenzione è stata bloccata: operazione interrotta"
	return -1
end if

// Fine Modifica

ll_anno_registrazione = f_anno_esercizio()
select max(num_registrazione)
into   :ll_num_registrazione
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione;
if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

ldt_data_registrazione = fdt_data_registrazione//datetime(today(),00:00:00)
if ls_flag_abr = 'S' then
	insert into manutenzioni  
				  ( 
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_guasto,   
				  flag_ordinario,   
				  flag_eseguito,   
				  anno_non_conf,   
				  num_non_conf,   
				  data_prossimo_intervento,   
				  flag_budget,   
				  anno_reg_programma,   
				  num_reg_programma,
				  des_intervento,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  flag_tipo_intervento,   
				  note_idl,   
				  flag_ricambio_codificato,   
				  cod_ricambio,
				  cod_versione,
				  ricambio_non_codificato,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_operaio,   
				  costo_orario_operaio,   
				  operaio_ore,   
				  operaio_minuti,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  risorsa_ore,   
				  risorsa_minuti,   
				  risorsa_costo_orario,   
				  risorsa_costi_aggiuntivi,   
				  anno_contratto_assistenza,   
				  num_contratto_assistenza,   
				  anno_ord_acq_risorsa,   
				  num_ord_acq_risorsa,   
				  prog_riga_ord_acq_risorsa,   
				  rif_contratto_assistenza,   
				  rif_ordine_acq_risorsa,   
				  cod_primario,   
				  cod_misura,   
				  valore_min_taratura,   
				  valore_max_taratura,   
				  valore_atteso,   
				  tot_costo_intervento,   
				  num_reg_lista,   
				  prog_liste_con_comp,   
				  num_versione,   
				  num_edizione,
				  flag_pubblica_intranet
				  )  
				values
				  (
				  :s_cs_xx.cod_azienda,
				  :ll_anno_registrazione,
				  :ll_num_registrazione,
				  :ldt_data_registrazione,
				  :ls_null,
				  'S',
				  'N',
				  :ll_null,
				  :ll_null,
				  :ldt_null,
				  'N',
				  :fn_anno_programma,
				  :fn_num_programma,
				  :ls_des_intervento,   
				  :ls_cod_attrezzatura,   
				  :ls_cod_tipo_manutenzione,   
				  :ls_flag_tipo_intervento,   
				  :ls_note_idl,   
				  :ls_flag_ricambio_codificato,   
				  :ls_cod_kit_ricambio,
				  :ls_cod_versione,
				  :ls_ricambio_non_codificato,   
				  :ld_quan_ricambio,   
				  :ld_costo_unitario_ricambio,   
				  :ls_cod_operaio,   
				  :ld_costo_orario_operaio,   
				  :ll_operaio_ore_previste,   
				  :ll_operaio_minuti_previsti,   
				  :ls_cod_cat_risorse_esterne,   
				  :ls_cod_risorsa_esterna,   
				  :ll_risorsa_ore_previste,   
				  :ll_risorsa_minuti_previsti,   
				  :ld_risorsa_costo_orario,   
				  :ld_risorsa_costi_aggiuntivi,   
				  :ll_anno_contratto_assistenza,   
				  :ll_num_contratto_assistenza,   
				  :ll_anno_ord_acq_risorsa,   
				  :ll_num_ord_acq_risorsa,   
				  :ll_prog_riga_ord_acq_risorsa,   
				  :ls_rif_contratto_assistenza,   
				  :ls_rif_ordine_acq_risorsa,   
				  :ls_cod_primario,   
				  :ls_cod_misura,   
				  :ld_valore_min_taratura,   
				  :ld_valore_max_taratura,   
				  :ld_valore_atteso,   
				  :ld_tot_costo_intervento,   
				  :ll_num_reg_lista,   
				  :ll_prog_liste_con_comp,   
				  :ll_num_versione,   
				  :ll_num_edizione,
				  :ls_flag_abr
				  );
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in inserimento manutenzione programmata" + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
else
		insert into manutenzioni  
				  ( 
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_guasto,   
				  flag_ordinario,   
				  flag_eseguito,   
				  anno_non_conf,   
				  num_non_conf,   
				  data_prossimo_intervento,   
				  flag_budget,   
				  anno_reg_programma,   
				  num_reg_programma,
				  des_intervento,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  flag_tipo_intervento,   
				  note_idl,   
				  flag_ricambio_codificato,   
				  cod_ricambio,
				  cod_versione,
				  ricambio_non_codificato,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_operaio,   
				  costo_orario_operaio,   
				  operaio_ore,   
				  operaio_minuti,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  risorsa_ore,   
				  risorsa_minuti,   
				  risorsa_costo_orario,   
				  risorsa_costi_aggiuntivi,   
				  anno_contratto_assistenza,   
				  num_contratto_assistenza,   
				  anno_ord_acq_risorsa,   
				  num_ord_acq_risorsa,   
				  prog_riga_ord_acq_risorsa,   
				  rif_contratto_assistenza,   
				  rif_ordine_acq_risorsa,   
				  cod_primario,   
				  cod_misura,   
				  valore_min_taratura,   
				  valore_max_taratura,   
				  valore_atteso,   
				  tot_costo_intervento,   
				  num_reg_lista,   
				  prog_liste_con_comp,   
				  num_versione,   
				  num_edizione
				  )  
				values
				  (
				  :s_cs_xx.cod_azienda,
				  :ll_anno_registrazione,
				  :ll_num_registrazione,
				  :ldt_data_registrazione,
				  :ls_null,
				  'S',
				  'N',
				  :ll_null,
				  :ll_null,
				  :ldt_null,
				  'N',
				  :fn_anno_programma,
				  :fn_num_programma,
				  :ls_des_intervento,   
				  :ls_cod_attrezzatura,   
				  :ls_cod_tipo_manutenzione,   
				  :ls_flag_tipo_intervento,   
				  :ls_note_idl,   
				  :ls_flag_ricambio_codificato,   
				  :ls_cod_kit_ricambio,
				  :ls_cod_versione,
				  :ls_ricambio_non_codificato,   
				  :ld_quan_ricambio,   
				  :ld_costo_unitario_ricambio,   
				  :ls_cod_operaio,   
				  :ld_costo_orario_operaio,   
				  :ll_operaio_ore_previste,   
				  :ll_operaio_minuti_previsti,   
				  :ls_cod_cat_risorse_esterne,   
				  :ls_cod_risorsa_esterna,   
				  :ll_risorsa_ore_previste,   
				  :ll_risorsa_minuti_previsti,   
				  :ld_risorsa_costo_orario,   
				  :ld_risorsa_costi_aggiuntivi,   
				  :ll_anno_contratto_assistenza,   
				  :ll_num_contratto_assistenza,   
				  :ll_anno_ord_acq_risorsa,   
				  :ll_num_ord_acq_risorsa,   
				  :ll_prog_riga_ord_acq_risorsa,   
				  :ls_rif_contratto_assistenza,   
				  :ls_rif_ordine_acq_risorsa,   
				  :ls_cod_primario,   
				  :ls_cod_misura,   
				  :ld_valore_min_taratura,   
				  :ld_valore_max_taratura,   
				  :ld_valore_atteso,   
				  :ld_tot_costo_intervento,   
				  :ll_num_reg_lista,   
				  :ll_prog_liste_con_comp,   
				  :ll_num_versione,   
				  :ll_num_edizione
				  );
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in inserimento manutenzione programmata" + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

end if
// **** michela 02/11/2004: inserimento operatore e risorse esterne

long ll_ret

ll_ret = uof_crea_operatore_risorse( ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ll_anno_registrazione, ll_num_registrazione, fs_messaggio)

if ll_ret <> 0 then return ll_ret

/////////////////////////////////////////// inserimento ricambi ////////////////////////////////////////////////////////

string ls_messaggio

uo_ricambi luo_ricambi

luo_ricambi = create uo_ricambi

if luo_ricambi.uof_ricambi_manutenzione(ll_anno_registrazione,ll_num_registrazione,ls_messaggio) <> 0 then
	g_mb.messagebox("OMNIA",ls_messaggio)
	destroy luo_ricambi
	return -1
end if

destroy luo_ricambi

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return 0
end function

public function integer uof_crea_manutenzione_programmata_sintex (long fn_anno_programma, long fn_num_programma, datetime fdt_data_registrazione, ref string fs_messaggio);string ls_null, ls_des_intervento, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_flag_tipo_intervento, ls_note_idl, &
       ls_flag_ricambio_codificato, ls_cod_kit_ricambio, ls_ricambio_non_codificato, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
		 ls_cod_risorsa_esterna, ls_rif_contratto_assistenza, ls_rif_ordine_acq_risorsa, ls_cod_primario, ls_cod_misura, &
		 ls_cod_versione, ls_ricambi[], ls_des_prodotto, ls_cod_ricambio, ls_des_ricambio, ls_blocco, ls_flag_abr
long ll_anno_registrazione, ll_num_registrazione, ll_null, ll_operaio_ore_previste, ll_operaio_minuti_previsti, &
     ll_risorsa_ore_previste, ll_risorsa_minuti_previsti, ll_anno_contratto_assistenza, ll_num_contratto_assistenza, &
	  ll_anno_ord_acq_risorsa, ll_num_ord_acq_risorsa, ll_prog_riga_ord_acq_risorsa, ll_num_reg_lista, ll_prog_liste_con_comp, &
	  ll_num_versione, ll_num_edizione, LL_CONT, ll_j, ll_prog_ricambio
dec{4} ld_quan_ricambio, ld_costo_unitario_ricambio, ld_costo_orario_operaio, ld_risorsa_costo_orario, &
       ld_risorsa_costi_aggiuntivi,  ld_valore_min_taratura, ld_valore_max_taratura, ld_valore_atteso, ld_tot_costo_intervento, &
		 ld_quantita[], ld_prezzo, ld_prezzo_ricambio
datetime ldt_data_registrazione, ldt_null

dec{0}   ld_data_inizio_val_1, ld_data_inizio_val_2, ld_data_fine_val_1, ld_data_fine_val_2

setnull(ls_null)
setnull(ll_null)
setnull(ldt_null)

select count(*)
into   :ll_cont
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_reg_programma = :fn_anno_programma and
       num_reg_programma = :fn_num_programma and
		 flag_eseguito = 'N';
if ll_cont > 0 and not isnull(ll_cont) then
	if g_mb.messagebox("Manutenzioni & Tarature","Attenzione: con questo programma di manutenzione risultano esistere ancora manutenzioni non eseguite.~r~nProseguo lo stesso?",Question!,YesNo!,2) = 2 then
		fs_messaggio = ""
		return -1
	end if
end if		 
//*******claudia 27/11/06 se presente il parametro multiaziendale abr allora esiste il campo flag_visualizza intranet e lo imposta
//******modifica fatta per abor specifica stampi
select flag
into :ls_flag_abr
from parametri 
where cod_parametro = 'ABR' and
		flag_parametro = 'F';

//Donato 04-02-2009 modifica per evitare che dia errore quando non trova il parametro ABR
//che tra l'altro è una personalizzazione del cliente ABOR
if sqlca.sqlcode <> 0 then
	ls_flag_abr = "N"
end if
/*
if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore durante il controllo di un paremtro ABR ." + sqlca.sqlerrtext)
		setpointer(arrow!)
		return -1
end if
*/
//fine modifica ---------------------------------------------------------------------

if ls_flag_abr = 'S' then
	select  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  note_idl,   
			  flag_ricambio_codificato,   
			  cod_ricambio,
			  cod_versione,
			  ricambio_non_codificato,   
			  quan_ricambio,   
			  costo_unitario_ricambio,   
			  cod_operaio,   
			  costo_orario_operaio,   
			  operaio_ore_previste,   
			  operaio_minuti_previsti,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  risorsa_ore_previste,   
			  risorsa_minuti_previsti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi,   
			  anno_contratto_assistenza,   
			  num_contratto_assistenza,   
			  anno_ord_acq_risorsa,   
			  num_ord_acq_risorsa,   
			  prog_riga_ord_acq_risorsa,   
			  rif_contratto_assistenza,   
			  rif_ordine_acq_risorsa,   
			  cod_primario,   
			  cod_misura,   
			  valore_min_taratura,   
			  valore_max_taratura,   
			  valore_atteso,   
			  tot_costo_intervento,   
			  num_reg_lista,   
			  prog_liste_con_comp,   
			  num_versione,   
			  num_edizione,
			  data_inizio_val_1,
			  data_fine_val_1,
			  data_inizio_val_2,
			  data_fine_val_2, 
			  flag_pubblica_intranet
	into    :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_flag_tipo_intervento,   
			  :ls_note_idl,   
			  :ls_flag_ricambio_codificato,   
			  :ls_cod_kit_ricambio,
			  :ls_cod_versione,
			  :ls_ricambio_non_codificato,   
			  :ld_quan_ricambio,   
			  :ld_costo_unitario_ricambio,   
			  :ls_cod_operaio,   
			  :ld_costo_orario_operaio,   
			  :ll_operaio_ore_previste,   
			  :ll_operaio_minuti_previsti,   
			  :ls_cod_cat_risorse_esterne,   
			  :ls_cod_risorsa_esterna,   
			  :ll_risorsa_ore_previste,   
			  :ll_risorsa_minuti_previsti,   
			  :ld_risorsa_costo_orario,   
			  :ld_risorsa_costi_aggiuntivi,   
			  :ll_anno_contratto_assistenza,   
			  :ll_num_contratto_assistenza,   
			  :ll_anno_ord_acq_risorsa,   
			  :ll_num_ord_acq_risorsa,   
			  :ll_prog_riga_ord_acq_risorsa,   
			  :ls_rif_contratto_assistenza,   
			  :ls_rif_ordine_acq_risorsa,   
			  :ls_cod_primario,   
			  :ls_cod_misura,   
			  :ld_valore_min_taratura,   
			  :ld_valore_max_taratura,   
			  :ld_valore_atteso,   
			  :ld_tot_costo_intervento,   
			  :ll_num_reg_lista,   
			  :ll_prog_liste_con_comp,   
			  :ll_num_versione,   
			  :ll_num_edizione,
			  :ld_data_inizio_val_1,
			  :ld_data_fine_val_1,
			  :ld_data_inizio_val_2,
			  :ld_data_fine_val_2,
			  :ls_flag_abr
	from    programmi_manutenzione
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fn_anno_programma and
			  num_registrazione  = :fn_num_programma;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in ricerca programma manutenzione " + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in SELECT tabella programmi manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
else
		select  des_intervento,   
			  cod_attrezzatura,   
			  cod_tipo_manutenzione,   
			  flag_tipo_intervento,   
			  note_idl,   
			  flag_ricambio_codificato,   
			  cod_ricambio,
			  cod_versione,
			  ricambio_non_codificato,   
			  quan_ricambio,   
			  costo_unitario_ricambio,   
			  cod_operaio,   
			  costo_orario_operaio,   
			  operaio_ore_previste,   
			  operaio_minuti_previsti,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  risorsa_ore_previste,   
			  risorsa_minuti_previsti,   
			  risorsa_costo_orario,   
			  risorsa_costi_aggiuntivi,   
			  anno_contratto_assistenza,   
			  num_contratto_assistenza,   
			  anno_ord_acq_risorsa,   
			  num_ord_acq_risorsa,   
			  prog_riga_ord_acq_risorsa,   
			  rif_contratto_assistenza,   
			  rif_ordine_acq_risorsa,   
			  cod_primario,   
			  cod_misura,   
			  valore_min_taratura,   
			  valore_max_taratura,   
			  valore_atteso,   
			  tot_costo_intervento,   
			  num_reg_lista,   
			  prog_liste_con_comp,   
			  num_versione,   
			  num_edizione,
			  data_inizio_val_1,
			  data_fine_val_1,
			  data_inizio_val_2,
			  data_fine_val_2
	into    :ls_des_intervento,   
			  :ls_cod_attrezzatura,   
			  :ls_cod_tipo_manutenzione,   
			  :ls_flag_tipo_intervento,   
			  :ls_note_idl,   
			  :ls_flag_ricambio_codificato,   
			  :ls_cod_kit_ricambio,
			  :ls_cod_versione,
			  :ls_ricambio_non_codificato,   
			  :ld_quan_ricambio,   
			  :ld_costo_unitario_ricambio,   
			  :ls_cod_operaio,   
			  :ld_costo_orario_operaio,   
			  :ll_operaio_ore_previste,   
			  :ll_operaio_minuti_previsti,   
			  :ls_cod_cat_risorse_esterne,   
			  :ls_cod_risorsa_esterna,   
			  :ll_risorsa_ore_previste,   
			  :ll_risorsa_minuti_previsti,   
			  :ld_risorsa_costo_orario,   
			  :ld_risorsa_costi_aggiuntivi,   
			  :ll_anno_contratto_assistenza,   
			  :ll_num_contratto_assistenza,   
			  :ll_anno_ord_acq_risorsa,   
			  :ll_num_ord_acq_risorsa,   
			  :ll_prog_riga_ord_acq_risorsa,   
			  :ls_rif_contratto_assistenza,   
			  :ls_rif_ordine_acq_risorsa,   
			  :ls_cod_primario,   
			  :ls_cod_misura,   
			  :ld_valore_min_taratura,   
			  :ld_valore_max_taratura,   
			  :ld_valore_atteso,   
			  :ld_tot_costo_intervento,   
			  :ll_num_reg_lista,   
			  :ll_prog_liste_con_comp,   
			  :ll_num_versione,   
			  :ll_num_edizione,
			  :ld_data_inizio_val_1,
			  :ld_data_fine_val_1,
			  :ld_data_inizio_val_2,
			  :ld_data_fine_val_2
	from    programmi_manutenzione
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fn_anno_programma and
			  num_registrazione  = :fn_num_programma;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in ricerca programma manutenzione " + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in SELECT tabella programmi manutenzione.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

end if
// Modifica Michele per gestione blocco tipologie di manutenzione 01/04/2003

select flag_blocco
into   :ls_blocco
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura and
		 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in lettura dati tipologia manutenzione: " + sqlca.sqlerrtext
	return -1
elseif ls_blocco = "S" then
	fs_messaggio = "Questa tipologia di manutenzione è stata bloccata: operazione interrotta"
	return -2
end if

// Fine Modifica

ll_anno_registrazione = f_anno_esercizio()
select max(num_registrazione)
into   :ll_num_registrazione
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione;
if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

//per sintex
//ldt_data_registrazione = datetime(today(),00:00:00)
ldt_data_registrazione = datetime(date(fdt_data_registrazione), 00:00:00)

if ls_flag_abr = 'S' then
	insert into manutenzioni  
				  ( 
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_guasto,   
				  flag_ordinario,   
				  flag_eseguito,   
				  anno_non_conf,   
				  num_non_conf,   
				  data_prossimo_intervento,   
				  flag_budget,   
				  anno_reg_programma,   
				  num_reg_programma,
				  des_intervento,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  flag_tipo_intervento,   
				  note_idl,   
				  flag_ricambio_codificato,   
				  cod_ricambio,
				  cod_versione,
				  ricambio_non_codificato,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_operaio,   
				  costo_orario_operaio,   
				  operaio_ore,   
				  operaio_minuti,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  risorsa_ore,   
				  risorsa_minuti,   
				  risorsa_costo_orario,   
				  risorsa_costi_aggiuntivi,   
				  anno_contratto_assistenza,   
				  num_contratto_assistenza,   
				  anno_ord_acq_risorsa,   
				  num_ord_acq_risorsa,   
				  prog_riga_ord_acq_risorsa,   
				  rif_contratto_assistenza,   
				  rif_ordine_acq_risorsa,   
				  cod_primario,   
				  cod_misura,   
				  valore_min_taratura,   
				  valore_max_taratura,   
				  valore_atteso,   
				  tot_costo_intervento,   
				  num_reg_lista,   
				  prog_liste_con_comp,   
				  num_versione,   
				  num_edizione,
				  flag_pubblica_intranet
				  )  
				values
				  (
				  :s_cs_xx.cod_azienda,
				  :ll_anno_registrazione,
				  :ll_num_registrazione,
				  :ldt_data_registrazione,
				  :ls_null,
				  'S',
				  'N',
				  :ll_null,
				  :ll_null,
				  :ldt_null,
				  'N',
				  :fn_anno_programma,
				  :fn_num_programma,
				  :ls_des_intervento,   
				  :ls_cod_attrezzatura,   
				  :ls_cod_tipo_manutenzione,   
				  :ls_flag_tipo_intervento,   
				  :ls_note_idl,   
				  :ls_flag_ricambio_codificato,   
				  :ls_cod_kit_ricambio,
				  :ls_cod_versione,
				  :ls_ricambio_non_codificato,   
				  :ld_quan_ricambio,   
				  :ld_costo_unitario_ricambio,   
				  :ls_cod_operaio,   
				  :ld_costo_orario_operaio,   
				  :ll_operaio_ore_previste,   
				  :ll_operaio_minuti_previsti,   
				  :ls_cod_cat_risorse_esterne,   
				  :ls_cod_risorsa_esterna,   
				  :ll_risorsa_ore_previste,   
				  :ll_risorsa_minuti_previsti,   
				  :ld_risorsa_costo_orario,   
				  :ld_risorsa_costi_aggiuntivi,   
				  :ll_anno_contratto_assistenza,   
				  :ll_num_contratto_assistenza,   
				  :ll_anno_ord_acq_risorsa,   
				  :ll_num_ord_acq_risorsa,   
				  :ll_prog_riga_ord_acq_risorsa,   
				  :ls_rif_contratto_assistenza,   
				  :ls_rif_ordine_acq_risorsa,   
				  :ls_cod_primario,   
				  :ls_cod_misura,   
				  :ld_valore_min_taratura,   
				  :ld_valore_max_taratura,   
				  :ld_valore_atteso,   
				  :ld_tot_costo_intervento,   
				  :ll_num_reg_lista,   
				  :ll_prog_liste_con_comp,   
				  :ll_num_versione,   
				  :ll_num_edizione,
				  :ls_flag_abr
				  );
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in inserimento manutenzione programmata" + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
else
		insert into manutenzioni  
				  ( 
				  cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_guasto,   
				  flag_ordinario,   
				  flag_eseguito,   
				  anno_non_conf,   
				  num_non_conf,   
				  data_prossimo_intervento,   
				  flag_budget,   
				  anno_reg_programma,   
				  num_reg_programma,
				  des_intervento,   
				  cod_attrezzatura,   
				  cod_tipo_manutenzione,   
				  flag_tipo_intervento,   
				  note_idl,   
				  flag_ricambio_codificato,   
				  cod_ricambio,
				  cod_versione,
				  ricambio_non_codificato,   
				  quan_ricambio,   
				  costo_unitario_ricambio,   
				  cod_operaio,   
				  costo_orario_operaio,   
				  operaio_ore,   
				  operaio_minuti,   
				  cod_cat_risorse_esterne,   
				  cod_risorsa_esterna,   
				  risorsa_ore,   
				  risorsa_minuti,   
				  risorsa_costo_orario,   
				  risorsa_costi_aggiuntivi,   
				  anno_contratto_assistenza,   
				  num_contratto_assistenza,   
				  anno_ord_acq_risorsa,   
				  num_ord_acq_risorsa,   
				  prog_riga_ord_acq_risorsa,   
				  rif_contratto_assistenza,   
				  rif_ordine_acq_risorsa,   
				  cod_primario,   
				  cod_misura,   
				  valore_min_taratura,   
				  valore_max_taratura,   
				  valore_atteso,   
				  tot_costo_intervento,   
				  num_reg_lista,   
				  prog_liste_con_comp,   
				  num_versione,   
				  num_edizione
				  )  
				values
				  (
				  :s_cs_xx.cod_azienda,
				  :ll_anno_registrazione,
				  :ll_num_registrazione,
				  :ldt_data_registrazione,
				  :ls_null,
				  'S',
				  'N',
				  :ll_null,
				  :ll_null,
				  :ldt_null,
				  'N',
				  :fn_anno_programma,
				  :fn_num_programma,
				  :ls_des_intervento,   
				  :ls_cod_attrezzatura,   
				  :ls_cod_tipo_manutenzione,   
				  :ls_flag_tipo_intervento,   
				  :ls_note_idl,   
				  :ls_flag_ricambio_codificato,   
				  :ls_cod_kit_ricambio,
				  :ls_cod_versione,
				  :ls_ricambio_non_codificato,   
				  :ld_quan_ricambio,   
				  :ld_costo_unitario_ricambio,   
				  :ls_cod_operaio,   
				  :ld_costo_orario_operaio,   
				  :ll_operaio_ore_previste,   
				  :ll_operaio_minuti_previsti,   
				  :ls_cod_cat_risorse_esterne,   
				  :ls_cod_risorsa_esterna,   
				  :ll_risorsa_ore_previste,   
				  :ll_risorsa_minuti_previsti,   
				  :ld_risorsa_costo_orario,   
				  :ld_risorsa_costi_aggiuntivi,   
				  :ll_anno_contratto_assistenza,   
				  :ll_num_contratto_assistenza,   
				  :ll_anno_ord_acq_risorsa,   
				  :ll_num_ord_acq_risorsa,   
				  :ll_prog_riga_ord_acq_risorsa,   
				  :ls_rif_contratto_assistenza,   
				  :ls_rif_ordine_acq_risorsa,   
				  :ls_cod_primario,   
				  :ls_cod_misura,   
				  :ld_valore_min_taratura,   
				  :ld_valore_max_taratura,   
				  :ld_valore_atteso,   
				  :ld_tot_costo_intervento,   
				  :ll_num_reg_lista,   
				  :ll_prog_liste_con_comp,   
				  :ll_num_versione,   
				  :ll_num_edizione
				  );
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Errore in inserimento manutenzione programmata" + string(fn_num_programma) + "/" + string(fn_num_programma)
		return -1
	elseif sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore in INSERT tabella manutenzioni.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

end if
// **** michela 02/11/2004: inserimento operatore e risorse esterne

long ll_ret

ll_ret = uof_crea_operatore_risorse( ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ll_anno_registrazione, ll_num_registrazione, fs_messaggio)

if ll_ret <> 0 then return ll_ret

/////////////////////////////////////////// inserimento ricambi ////////////////////////////////////////////////////////

string ls_messaggio

uo_ricambi luo_ricambi

luo_ricambi = create uo_ricambi

if luo_ricambi.uof_ricambi_manutenzione(ll_anno_registrazione,ll_num_registrazione,ls_messaggio) <> 0 then
	g_mb.messagebox("OMNIA",ls_messaggio)
	destroy luo_ricambi
	return -1
end if

destroy luo_ricambi

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

s_cs_xx.parametri.parametro_ul_1 = ll_anno_registrazione
s_cs_xx.parametri.parametro_ul_2 = ll_num_registrazione

return 0
end function

private function integer uof_calcola_manutenzioni_global (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_errore);string	ls_sql, ls_syntax, ls_error

long		ll_i, ll_man_tot_ore, ll_man_tot_minuti, ll_prog_fase, ll_fase_tot_ore, ll_fase_tot_minuti, &
         ll_fase_tot_ore_ris, ll_fase_tot_minuti_ris, ll_man_tot_ore_ris, ll_man_tot_minuti_ris
			
dec{4}   ld_fase_tot_costo_operai,ld_fase_tot_costo_risorse_esterne,ld_man_tot_costo_operai,&
         ld_man_tot_costo_risorse_esterne,ld_tot_costo_ricambi,ld_tot_costo_intervento

datetime ldt_man_data_inizio, ldt_man_data_fine, ldt_man_ora_inizio, ldt_man_ora_fine, &
			ldt_fase_data_inizio, ldt_fase_data_fine, ldt_fase_ora_inizio, ldt_fase_ora_fine

datastore lds_fasi


ls_sql = &
"select prog_fase, " + &
"		  data_inizio, " + &
"		  ora_inizio, " + &
"		  data_fine, " + &
"		  ora_fine, " + &
"		  tot_ore, " + &
"		  tot_minuti, " + &
"		  tot_ore_ris, " + &
"		  tot_minuti_ris, " + &
"		  tot_costo_operai, " + &
"		  tot_costo_risorse_esterne " + &
"from	  manutenzioni_fasi " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"		  anno_registrazione = " + string(fl_anno_registrazione) + " and " + &
"		  num_registrazione = " + string(fl_num_registrazione)

ls_sql += " order by prog_fase ASC"

ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	fs_errore = "Errore in impostazione select fasi - manutenzione " + string(fl_anno_registrazione) + "/" + &
						string(fl_num_registrazione) + ": " + ls_error
	return -1
end if

lds_fasi = create datastore

lds_fasi.create(ls_syntax,ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	fs_errore = "Errore in creazione datastore fasi - manutenzione " + string(fl_anno_registrazione) + "/" + &
						string(fl_num_registrazione) + ": " + ls_error
	destroy lds_fasi
	return -1
end if

lds_fasi.settransobject(sqlca)

if lds_fasi.retrieve() = -1 then
	fs_errore = "Errore in lettura dati fasi - manutenzione " + string(fl_anno_registrazione) + "/" + string(fl_num_registrazione)
	destroy lds_fasi
	return -1
end if

for ll_i = 1 to lds_fasi.rowcount()
	
	ll_prog_fase = lds_fasi.getitemnumber(ll_i,"prog_fase")
	
	if uof_calcola_manutenzione_fasi(fl_anno_registrazione, fl_num_registrazione, ll_prog_fase, ref fs_errore) <> 0 then
		fs_errore = "Errore in aggiornamento tempi fase~n" + fs_errore
		destroy lds_fasi
		return -1
	end if
		
next

// ricarico datastore con nuovi dati calcolati.

if lds_fasi.retrieve() = -1 then
	fs_errore = "Errore in lettura dati fasi - manutenzione " + string(fl_anno_registrazione) + "/" + string(fl_num_registrazione)
	destroy lds_fasi
	return -1
end if

setnull(ldt_man_data_inizio)

setnull(ldt_man_data_fine)

setnull(ldt_man_ora_inizio)

setnull(ldt_man_ora_fine)

ll_man_tot_ore = 0

ll_man_tot_minuti = 0

for ll_i = 1 to lds_fasi.rowcount()
	
	ldt_fase_data_inizio = lds_fasi.getitemdatetime(ll_i,"data_inizio")
	
	ldt_fase_data_fine = lds_fasi.getitemdatetime(ll_i,"data_fine")
	
	ldt_fase_ora_inizio = lds_fasi.getitemdatetime(ll_i,"ora_inizio")
	
	ldt_fase_ora_fine = lds_fasi.getitemdatetime(ll_i,"ora_fine")
	
	ll_fase_tot_ore = lds_fasi.getitemnumber(ll_i,"tot_ore")
	
	ll_fase_tot_minuti = lds_fasi.getitemnumber(ll_i,"tot_minuti")

	ll_fase_tot_ore_ris = lds_fasi.getitemnumber(ll_i,"tot_ore_ris")
	
	ll_fase_tot_minuti_ris = lds_fasi.getitemnumber(ll_i,"tot_minuti_ris")
	
	ld_fase_tot_costo_operai = lds_fasi.getitemnumber(ll_i,"tot_costo_operai")
	
	ld_fase_tot_costo_risorse_esterne = lds_fasi.getitemnumber(ll_i,"tot_costo_risorse_esterne")
	
	if isnull(ll_fase_tot_ore) then
		ll_fase_tot_ore = 0
	end if
	
	if isnull(ll_fase_tot_minuti) then
		ll_fase_tot_minuti = 0
	end if
	
	if isnull(ll_fase_tot_ore_ris) then
		ll_fase_tot_ore_ris = 0
	end if
	
	if isnull(ll_fase_tot_minuti_ris) then
		ll_fase_tot_minuti_ris = 0
	end if

	if isnull(ldt_man_data_inizio) or ldt_fase_data_inizio < ldt_man_data_inizio then
			ldt_man_data_inizio = ldt_fase_data_inizio
			ldt_man_ora_inizio = ldt_fase_ora_inizio
	elseif ldt_fase_data_inizio = ldt_man_data_inizio then
		if isnull(ldt_man_ora_inizio) then
			ldt_man_ora_inizio = ldt_fase_ora_inizio
		else
			if ldt_fase_ora_inizio < ldt_man_ora_inizio then
				ldt_man_ora_inizio = ldt_fase_ora_inizio
			end if
		end if
	end if

	if isnull(ldt_man_data_fine) or ldt_fase_data_fine > ldt_man_data_fine then
			ldt_man_data_fine = ldt_fase_data_fine
			ldt_man_ora_fine = ldt_fase_ora_fine
	elseif ldt_fase_data_fine = ldt_man_data_fine then
		if isnull(ldt_man_ora_fine) then
			ldt_man_ora_fine = ldt_fase_ora_fine
		else
			if ldt_fase_ora_fine > ldt_man_ora_fine then
				ldt_man_ora_fine = ldt_fase_ora_fine
			end if
		end if
	end if

	ll_man_tot_ore += ll_fase_tot_ore
	
	ll_man_tot_minuti += ll_fase_tot_minuti
	
	ll_man_tot_ore_ris += ll_fase_tot_ore_ris
	
	ll_man_tot_minuti_ris += ll_fase_tot_minuti_ris
	
	ld_man_tot_costo_operai += ld_fase_tot_costo_operai
	
	ld_man_tot_costo_risorse_esterne += ld_fase_tot_costo_risorse_esterne
	
next

destroy lds_fasi

ll_man_tot_ore += int(ll_man_tot_minuti / 60)

ll_man_tot_minuti = mod(ll_man_tot_minuti,60)

ll_man_tot_ore_ris += int(ll_man_tot_minuti_ris / 60)

ll_man_tot_minuti_ris = mod(ll_man_tot_minuti_ris,60)

if isnull(ll_man_tot_ore) then
	ll_man_tot_ore = 0
end if

if isnull(ll_man_tot_minuti) then
	ll_man_tot_minuti = 0
end if

if isnull(ll_man_tot_ore_ris) then
	ll_man_tot_ore_ris = 0
end if

if isnull(ll_man_tot_minuti_ris) then
	ll_man_tot_minuti_ris = 0
end if

select sum(imponibile_iva)
into   :ld_tot_costo_ricambi
from   manutenzioni_ricambi
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 flag_utilizzo = 'S';

if isnull(ld_tot_costo_ricambi) then 
	ld_tot_costo_ricambi = 0
end if
	
ld_tot_costo_intervento = ld_tot_costo_ricambi + ld_man_tot_costo_operai + ld_man_tot_costo_risorse_esterne



update manutenzioni
set	 data_inizio_intervento = :ldt_man_data_inizio,
		 ora_inizio_intervento = :ldt_man_ora_inizio,
		 data_fine_intervento = :ldt_man_data_fine,
		 ora_fine_intervento = :ldt_man_ora_fine, 
		 operaio_ore = :ll_man_tot_ore,
		 operaio_minuti = :ll_man_tot_minuti,
		 risorsa_ore = :ll_man_tot_ore_ris,
		 risorsa_minuti = :ll_man_tot_minuti_ris,
		 tot_costo_intervento = :ld_tot_costo_intervento
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in aggiornamento dati manutenzione " + string(fl_anno_registrazione) + "/" + &
						string(fl_num_registrazione) + ": " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_calcola_ore_minuti (datetime adt_data_da, datetime adt_data_a, datetime adt_ora_da, datetime adt_ora_a, ref long al_ore, ref long al_minuti);long ll_secondi


if isnull(adt_data_da) or isnull(adt_data_a) or isnull(adt_ora_da) or isnull(adt_ora_a) or &
	adt_data_da > adt_data_a or (adt_data_da = adt_data_a and time(adt_ora_da) > time(adt_ora_a)) then
	setnull(al_ore)
	setnull(al_minuti)
	return -1
end if

ll_secondi = secondsafter(time(adt_ora_da),time(adt_ora_a))

al_minuti = int(ll_secondi / 60)

al_ore = int(al_minuti / 60)

al_minuti -= al_ore * 60

al_ore += daysafter(date(adt_data_da),date(adt_data_a)) * 24

return 0
end function

on uo_manutenzioni.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_manutenzioni.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;string ls_flag

select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';

if sqlca.sqlcode = 0 and ls_flag = "S" then 
	ib_global_service = true
end if
	
end event

