﻿$PBExportHeader$uo_progen_documenti.sru
forward
global type uo_progen_documenti from nonvisualobject
end type
end forward

global type uo_progen_documenti from nonvisualobject
end type
global uo_progen_documenti uo_progen_documenti

type variables
string is_chiave_root = "HKEY_LOCAL_MACHINE\Software\Consulting&Software\db_esterni\sintexcal\"
end variables

forward prototypes
public function long f_chiave_progen (string fs_keyname, string fs_dipartimento, string fs_azienda, ref string fs_errore, transaction ft_tran_import)
public function integer f_testata_ddi_progen (string fs_cod_azienda, string fs_cod_fornitore, string fs_cod_area_aziendale, string fs_num_documento, datetime fdt_data_documento, string fs_cod_porto, transaction tran_import, ref long fl_chiave_testata, ref string fs_errore)
public function integer f_registro (boolean fb_connetti, ref n_tran fnt_tran_import)
public function string uof_cdiva_progen ()
end prototypes

public function long f_chiave_progen (string fs_keyname, string fs_dipartimento, string fs_azienda, ref string fs_errore, transaction ft_tran_import);Long			rc
DateTime		ldt_oggi
Decimal		ll_key1, ll_key2, ll_inizio1, ll_fine1, ll_inizio2, ll_fine2, ll_count, ll_key
string      as_dipartimento, as_keyname, as_azienda

ldt_oggi = DateTime(Today(),Now()) 

as_dipartimento = fs_dipartimento
as_azienda = fs_azienda
as_keyname = fs_keyname

SELECT COUNT(*)
INTO   :ll_count
FROM   pgmr.keypools
WHERE  keyname = :as_keyname AND
 		 cddipa  = :as_dipartimento and
		 cdazie = :as_azienda
using  ft_tran_import;

if ft_tran_import.sqlcode < 0 then
	fs_errore = "Errore durante la ricerca della chiave: " + ft_tran_import.sqlerrtext
	return -1
end if
		  
// Se non esiste il Record per la chiave richiesta ritorno un codice errore
if ll_count = 0 then
	if as_keyname = "tkgene" then
		fs_errore = "Attenzione: NON TROVATO IL TOKEN TKGENE!"
		return -1
	end if
	fs_errore = "Attenzione: NON TROVATO IL TOKEN " + as_keyname + "!"
	return -1
end if


UPDATE PGMR.KEYPOOLS
SET    init1   = init1
WHERE  keyname = :as_keyname AND
 		 cddipa  = :as_dipartimento and
		 cdazie = :as_azienda
using  ft_tran_import;

if ft_tran_import.SqlCode <> 0 then
	fs_errore = "Errore durante update init1 = init1 : " + ft_tran_import.sqlerrtext
	return -6
end if

SELECT init1,
		 fine1,
		 init2,
		 fine2
INTO   :ll_inizio1,
  		 :ll_fine1,
		 :ll_inizio2,
		 :ll_fine2
FROM   pgmr.keypools
WHERE  keyname = :as_keyname AND
 		 cddipa  = :as_dipartimento and
		 cdazie = :as_azienda
using  ft_tran_import;

// Se non sono valorizzati gli estremi per la chiave ritorno un errore
if IsNull(ll_inizio1) or IsNull(ll_fine1) or IsNull(ll_inizio2) or IsNull(ll_fine2) then 
	fs_errore = "Non sono valorizzati gli estremi per la chiave!"	
	return -2
end if

ll_key1 = ll_inizio1 + 1
ll_key2 = ll_inizio2 + 1

if ll_key1 > ll_fine1 then ll_key1 = 0
if ll_key2 > ll_fine2 then ll_key2 = 0

// Per nessuna chiave disponibile ritorno un errore
if ll_key1 = 0 AND ll_key2 = 0 then
	fs_errore = "Nessuna chiave disponibile!"
	return -3
end if

// Per tutte e due i valori validi cerco il valore, minore, disponibile per la chiave 
if ll_key1 > 0 AND ll_key2 > 0 then
	if ll_key1 < ll_key2 then
		ll_key = ll_key1
	else
		ll_key = ll_key2
	end if
end if

// Ritorno la chiave valida se l'altra è esaurita
if ll_key1 > 0 AND ll_key2 = 0 then
	ll_key = ll_key1
end if
if ll_key2 > 0 AND ll_key1 = 0 then
	ll_key = ll_key2
end if


// aggiorno la key-pool
CHOOSE CASE ll_key
	CASE ll_key1
		
		UPDATE PGMR.KEYPOOLS
		SET    init1   = :ll_key1,
			    dtulag  = :ldt_oggi
		WHERE  keyname = :as_keyname AND
		 		 cddipa  = :as_dipartimento and
				 cdazie = :as_azienda
		using  ft_tran_import;
		
	CASE ll_key2
		
		UPDATE PGMR.KEYPOOLS
		SET    init2   = :ll_key2,
				 dtulag  = :ldt_oggi
		WHERE  keyname = :as_keyname AND
		 		 cddipa  = :as_dipartimento and
				 cdazie = :as_azienda
		using  ft_tran_import;
		
END CHOOSE

if ft_tran_import.SqlCode = 0 then
//	commit using tran_import;
	RETURN ll_key
else
	fs_errore = "Errore durante l'aggiornamento della key pool: " + ft_tran_import.sqlerrtext
	rollback using ft_tran_import;
	return -4
end if

return	ll_key
end function

public function integer f_testata_ddi_progen (string fs_cod_azienda, string fs_cod_fornitore, string fs_cod_area_aziendale, string fs_num_documento, datetime fdt_data_documento, string fs_cod_porto, transaction tran_import, ref long fl_chiave_testata, ref string fs_errore);long 		ll_riga, ll_anno, ll_numero, ll_prog_riga_ricambio, ll_prog_riga_new, ll_i, ll_prog_bolla_progen, ll_chiave_testata, ll_anno_documento, ll_chiave_pos, ll_chiave, ll_chiave_2, ll_max

string		ls_cod_attrezzatura, ls_flag_blocco, ls_flag_amministratore, ls_flag_rda, ls_cod_prodotto, ls_cod_fornitore, ls_cod_misura, ls_cod_porto, ls_des_prodotto, ls_des_estesa, &
			ls_flag_sconto_valore, ls_cod_stato, ls_cod_fornitore_old, ls_cod_tipo_programma, ls_controllo_reparto, ls_controllo_attrezzatura, ls_cod_azienda, ls_cod_centro_costo_mezzi, ls_prefisso_mg, &
			ls_cod_area_aziendale, ls_cod_centro_costo, ls_cdceco, ls_cdrmpo_ord, ls_adspds_1_ord, ls_adspds_2_ord, ls_cdarti, ls_cdpian, ls_cod_misura_2, ls_tkforn, ls_pgcodi, ls_dpcodi, ls_cddpag, &
			ls_ffinme, ls_cdbanc, ls_cdente, ls_cdunil, ls_cdenul, ls_cdusul, ls_cdunil_m, ls_dsunil, ls_cddipa, ls_cdtrme, ls_cdtdim_m, ls_cdstab_appo, ls_cdtdim, ls_cdstab_m, ls_cdstab, &
			ls_indiri, ls_cdtdoc, ls_cdcatm, ls_cdtpmg, ls_1, ls_2, ls_errore, ls_cod_iva_progen
			
boolean	lb_rda

datetime	ldt_data_consegna, ldt_oggi

dec{4}	ld_sconto_1_eff, ld_sconto_2_eff, ld_sconto_3_eff, ld_prezzo_netto_eff, ld_prezzo_ricambio_eff, ld_quan_eff, 	ld_adspim_1_ord, ld_adspim_2_ord, ld_scocas_ord, ld_scrap1_ord, ld_scrap2_ord, &
			ld_cstrkg_ord, ld_oneacc_ord

ll_anno_documento = year( date(fdt_data_documento))
ldt_oggi = datetime( date(today()), time(now()))

//	*** LEGGO I PARAMETRI DA OMNIA

select cdstab 
into   :ls_cdstab_m
from   tab_aree_aziendali 
where  cod_azienda = :s_cs_xx.cod_azienda and
       	 cod_area_aziendale = :fs_cod_area_aziendale;
		 
if sqlca.sqlcode <> 0 then
	fs_errore =  "Errore durante la ricerca di cdstab in anagrafica aree_aziendali per l'area " + fs_cod_area_aziendale + " : " + sqlca.sqlerrtext
	return -1
end if

select stringa
into   :ls_cdtdim_m
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'DIM';

if sqlca.sqlcode <> 0 then
	fs_errore = "Attenzione: impostare il parametro DIM nella tabella parametri_azienda!"
	return -1
end if

select cdstab
into   :ls_cdstab_appo
from   tab_aree_aziendali
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_area_aziendale = :fs_cod_area_aziendale;
		 
if sqlca.sqlcode <> 0 then
	fs_errore =  "1.Errore durante la ricerca cdstab area aziendale: " + sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode = 0 and ( isnull(ls_cdstab_appo) or ls_cdstab_appo = "" ) then
	fs_errore = "1.Errore durante la ricerca cdstab area aziendale: il campo risulta non valorizzato!"
	return -1	
end if

select stringa
into   :ls_cdtrme
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CDM';

if sqlca.sqlcode <> 0 then
	fs_errore = "Attenzione: impostare il parametro CDM come tipo ordine acquisto per progen nella tabella parametri_azienda!"
	return -1
end if

select stringa
into   :ls_cod_stato
from   parametri
where  cod_parametro = 'MSE';

if sqlca.sqlcode <> 0 then
	fs_errore = "Attenzione: impostare il parametro MSP con codice stato attività parzialmente eseguita:" + sqlca.sqlerrtext
	return -1
end if

select stringa
into   :ls_cod_azienda
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'AZP';
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in lettura parametro aziendale AZP (azienda PROGEN)"
	return -1
end if

// *** costante in omnia con il codice centro di costo per i mezzi che non ho trovato

select stringa
into    :ls_cod_centro_costo_mezzi
from   parametri
where cod_parametro = 'CCM';

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca del codice centro costo mezzi generico (per i mezzi non inseriti)(parametro CCM multiaziendale): " + sqlca.sqlerrtext
	return -1
end if

// *** costante in omnia con il prefisso per le attrezzature che sono mezzi generici (quali cassetta degli attrezzi).
//      dovrebbe essere scritto MME nel valore stringa

select stringa
into     :ls_prefisso_mg
from    parametri
where  cod_parametro = 'CCP';

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca del prefisso per i mezzi generici (parametro CCP multiaziendale): " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_prefisso_mg) or ls_prefisso_mg = "" then
	fs_errore = "Attenzione: il prefisso per i mezzi generici (parametro CCP multiaziendale) risulta nullo o vuoto.Controllare nell'apposita tabella! "
	return -1	
end if

select pgmr.costanti.costvalue
into   :ls_cdceco
from   pgmr.costanti
where  pgmr.costanti.costname = 'cdcncs_def' and
       	 pgmr.costanti.cdazie = :fs_cod_azienda and
		 pgmr.costanti.cddipa = :fs_cod_area_aziendale
using  tran_import;
		 
if tran_import.sqlcode <> 0 then
	fs_errore = "1.Errore durante la ricerca dati cdcncs_def: " + tran_import.sqlerrtext
	return -1
end if	

ls_cdtdim_m = ls_cdtdim_m + ls_cdstab_appo

select pgmr.tipo_docingrmer.cdtdim
into   :ls_cdtdim
from   pgmr.tipo_docingrmer
where  pgmr.tipo_docingrmer.cdtdim_m = :ls_cdtdim_m
using  tran_import;

if tran_import.sqlcode <> 0 then
	fs_errore = "1a.Errore durante la ricerca dati tipo documento ingresso: " + tran_import.sqlerrtext
	fs_errore += " 		valore cdtdim_m:" + ls_cdtdim_m
	return -1
end if

SELECT  pgmr.stabilimento.cdstab,
        pgmr.stabilimento.via
into    :ls_cdstab,
        :ls_indiri
FROM    pgmr.stabilimento
WHERE   pgmr.stabilimento.cdazie = :fs_cod_azienda and
		  pgmr.stabilimento.cdstab_m = :ls_cdstab_m
using   tran_import;

if tran_import.sqlcode <> 0 then
	fs_errore = "1.Errore durante la ricerca dati stabilimento: " + tran_import.sqlerrtext
	fs_errore += "	CDSTAB M = " + ls_cdstab_m + " - AZIENDA = " + ls_cod_azienda
	return -1
end if

select pgmr.tipo_docingrmer.cdtdoc,
       pgmr.tipo_docingrmer.cdcatm
into   :ls_cdtdoc,
       :ls_cdcatm
from   pgmr.tipo_docingrmer
where  pgmr.tipo_docingrmer.cdtdim = :ls_cdtdim
using  tran_import;

if tran_import.sqlcode <> 0 then
	fs_errore = "1.Errore durante la ricerca dati (parametro cdtdoc): " + tran_import.sqlerrtext
	fs_errore +=  "	-	2.Valore cdtdim: " + ls_cdtdim
	return -1
end if

select pgmr.costanti.costvalue
into   :ls_cdtpmg
from   pgmr.costanti
where  pgmr.costanti.costname = 'cdtpmg_ing_m' and
       pgmr.costanti.cdazie = :fs_cod_azienda and
		 pgmr.costanti.cddipa = :fs_cod_area_aziendale
using  tran_import;
		 
if tran_import.sqlcode < 0 then
	fs_errore = "1.Errore durante la ricerca dati cdtpmg_ing_m: " + tran_import.sqlerrtext
	fs_errore += "	- AZIENDA: " + fs_cod_azienda + " - CDDIPA: " + fs_cod_area_aziendale
	return -1
elseif tran_import.sqlcode = 100 then
	fs_errore = "Costante cdtpmg_ing_m non trovata. "
	fs_errore += "	- AZIENDA: " + fs_cod_azienda + " - CDDIPA: " + fs_cod_area_aziendale
	return -1	
end if		 

select tkforn
into   :ls_tkforn
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_fornitore = :fs_cod_fornitore;
		 
if sqlca.sqlcode <> 0 or isnull(ls_tkforn) or ls_tkforn = "" then
	fs_errore = "Attenzione: impostare il codice tkforn per il fornitore selezionato!"
	return -1	
end if	

select cod_porto_progen
into   :ls_cdrmpo_ord
from   tab_porti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_porto = :fs_cod_porto;	

if sqlca.sqlcode <> 0 or isnull(ls_cdrmpo_ord) or ls_cdrmpo_ord = "" then
	fs_errore = "Attenzione: impostare il codice progen per il porto:" + fs_cod_porto
	return -1	
end if	
		
// **** 	dati del fornitore

SELECT  pgmr.archforn.pgcodi ,
		  pgmr.archforn.dpcodi ,
		  pgmr.archforn.cddpag ,
		  pgmr.archforn.ffinme ,
		  pgmr.archforn.cdbanc
into    :ls_pgcodi,
		  :ls_dpcodi,
		  :ls_cddpag,
		  :ls_ffinme,
		  :ls_cdbanc
FROM    pgmr.archforn 
WHERE   pgmr.archforn.cdazie = :fs_cod_azienda and
		  pgmr.archforn.tkforn = :ls_tkforn
using   tran_import;

if tran_import.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca dati fornitore: " + tran_import.sqlerrtext
	return -1
end if		

SELECT pgmr.archenti.cdente ,
		 pgmr.enteuniloc.cdunil ,
		 pgmr.enteuniloc.cdenul ,
		 pgmr.enteuniloc.cdusul ,
		 pgmr.unitalocali.cdunil ,
		 pgmr.unitalocali.cdunil_m ,
		 pgmr.unitalocali.dsunil ,
		 pgmr.unitalocali.cddipa
into   :ls_cdente ,
		 :ls_cdunil,
		 :ls_cdenul ,
		 :ls_cdusul ,
		 :ls_cdunil ,
		 :ls_cdunil_m ,
		 :ls_dsunil ,
		 :ls_cddipa
FROM   pgmr.archforn LEFT OUTER JOIN pgmr.archenti ON pgmr.archforn.cdente = pgmr.archenti.cdente LEFT OUTER JOIN pgmr.enteuniloc ON pgmr.archenti.cdente = pgmr.enteuniloc.cdente LEFT OUTER JOIN pgmr.unitalocali ON pgmr.enteuniloc.cdunil = pgmr.unitalocali.cdunil LEFT OUTER JOIN pgmr.province ON pgmr.unitalocali.cdprov = pgmr.province.cdprov     
WHERE ( pgmr.archforn.cdazie = :fs_cod_azienda ) and          
		( pgmr.enteuniloc.cdusul = '0000000015' )  and
		( pgmr.archforn.tkforn = :ls_tkforn )
using   tran_import;	

if tran_import.sqlcode <> 0 then
	fs_errore = "2.Errore durante la ricerca dati fornitore: " + tran_import.sqlerrtext
	return -1
end if	

//	***	TOKEN DI POSIZIONE TESTATA PER IL NUOVO FORNITORE

ll_chiave_testata = f_chiave_progen("TKGENE", "01", "01", fs_errore, tran_import)

if ll_chiave_testata < 0 then	
	return -1
end if

//############################################################
//12/06/2012 Donato leggo il cod iva da passare a progen dalla tabella parametri (CIP)
ls_cod_iva_progen = uof_cdiva_progen()
//############################################################

// ***			INSERIMENTO TESTATA
INSERT INTO pgmr.bollacq_test( tkboll,
										 cdboll,
										 dtboll,
										 anno,
										 dtcomp,
										 tkordi,
										 cdordi, 
										 tkforn,
										 cdente,
										 cdrmpo,
										 cdtrme,
										 vacodi,
										 nrcamb,
										 pgcodi,
										 dpcodi,
										 cddpag,
										 ffinme, 
										 adspds_1,
										 adspim_1,
										 adspds_2,
										 adspim_2,
										 scocas,
										 scrap1,
										 scrap2,
										 cstrkg,
										 oneacc, 
										 cdfisc,
										 cdiva,
										 fstato,
										 cdazie,
										 cddipa,
										 profil,
										 dtinse,
										 dtulag,
										 cdtdim,
										 tkforn_a, 
										 tkclie_a,
										 cdvett,
										 cdautome,
										 cdentv,
										 cdstab,
										 dtcontr,
										 cdtdoc,
										 nrbolf,
										 dtbolf,
										 nrifft,
										 dtrift,
										 fmovim,
										 freplic,
										 cdcatm,
										 cdtpmg,
										 cdulio,
										 cdtpmg_da)
							vALUES (  :ll_chiave_testata,
										 :fs_num_documento,
										 :fdt_data_documento,
										 :ll_anno_documento,
										 :fdt_data_documento,
										 null,
										 null,
										 :ls_tkforn,
										 :ls_cdente,
										 :ls_cdrmpo_ord,
										 :ls_cdtrme,
										 '0000000003',
										 1,
										 :ls_pgcodi,
										 :ls_dpcodi,
										 :ls_cddpag,
										 :ls_ffinme,
										 null,
										 0,
										 null,
										 0,
										 0,
										 0,
										 0,
										 0,
										 0,
										 '0000000002',
										:ls_cod_iva_progen,
										 'N',
										 :fs_cod_azienda,
										 :fs_cod_area_aziendale,
										 :s_cs_xx.cod_utente,
										 :ldt_oggi,
										 :ldt_oggi,
										 :ls_cdtdim,
										 NULL,
										 NULL,
										 NULL,
										 NULL,
										 NULL,
										 :ls_cdstab,
										 NULL,
										 :ls_cdtdoc,
										 NULL,
										 NULL,
										 NULL,
										 NULL,
										 NULL,
										 'N',
										 :ls_cdcatm,
										 :ls_cdtpmg,
										 :ls_cdunil,
										 NULL)
using tran_import;
				
if tran_import.sqlcode < 0 then
	fs_errore = "Errore durante l'inserimento della testata:" + tran_import.sqlerrtext
	return -1
end if	

fl_chiave_testata = ll_chiave_testata

return 0
end function

public function integer f_registro (boolean fb_connetti, ref n_tran fnt_tran_import);string ls_default,ls_dbparm
integer li_risposta

if fb_connetti then
	// -----------------  leggo le informazioni della transazione dal registro ----------------------
	
	fnt_tran_import = CREATE n_tran
	li_risposta = registryget(is_chiave_root+ "database", "servername", fnt_tran_import.ServerName)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database", "logid", fnt_tran_import.LogId)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database", "logpass", fnt_tran_import.LogPass)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database" , "dbms", fnt_tran_import.DBMS)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database" , "database", fnt_tran_import.Database)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database" , "userid", fnt_tran_import.UserId)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database", "dbpass", fnt_tran_import.DBPass)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	li_risposta = registryget(is_chiave_root+ "database" , "dbparm", ls_dbparm)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	
	fnt_tran_import.DBParm = ls_dbparm
	
	li_risposta = registryget(is_chiave_root+ "database" , "lock", fnt_tran_import.Lock)
	if li_risposta = -1 then
		g_mb.messagebox("OMNIA","Mancano le impostazione del database sul registro.",stopsign!)
		return -1
	end if
	// ----------------------------------------------------------------------------------------------
	if f_po_connect(fnt_tran_import, true) <> 0 then
		return -1
	end if

else
	
	rollback using fnt_tran_import;
	disconnect using fnt_tran_import;
	destroy fnt_tran_import;

end if
return 0
end function

public function string uof_cdiva_progen ();
//restituisce il codice iva da applicare nei documenti tipo bolla, ordini ed rda da pa da OMNIA a Progen
//legge il parametro multiaziendale CIP, se non lo trova o è impostato vuoto mette quello di default '0003002001' che rappresenta l'iva al 21%   colpa di questi tecnici del caxxo al governo !!!!!

string ls_cdiva

select stringa
into :ls_cdiva
from parametri
where cod_parametro='CIP' and flag_parametro='S';

if ls_cdiva="" or isnull(ls_cdiva) then ls_cdiva = '0003002001'


return ls_cdiva
end function

on uo_progen_documenti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_progen_documenti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

