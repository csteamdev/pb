﻿$PBExportHeader$uo_registra_manutenzione.sru
forward
global type uo_registra_manutenzione from nonvisualobject
end type
end forward

global type uo_registra_manutenzione from nonvisualobject
end type
global uo_registra_manutenzione uo_registra_manutenzione

forward prototypes
public subroutine wf_registra_man (string fs_stato_manutenzione, datetime fdt_data_giorno, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione)
end prototypes

public subroutine wf_registra_man (string fs_stato_manutenzione, datetime fdt_data_giorno, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione);string ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_des_intervento, ls_flag_tipo_intervento, ls_periodicita, &
		 ls_flag_scadenze, ls_note_idl, ls_flag_ricambio_codificato, ls_cod_ricambio, ls_cod_ricambio_alternativo, ls_ricambio_non_codificato, &
		 ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_rif_contratto_assistenza, ls_rif_ordine_acq_risorsa, &
		 ls_flag_budget, ls_cod_primario, ls_cod_misura, ls_flag_blocco, ls_flag_priorita, ls_flag_stato, ls_stato_manutenzione, ls_null
datetime ldt_data_da, ldt_data_a, ldt_data_blocco, ldt_data_forzata, ldt_data_giorno, ldt_data_registrazione, ldt_null
long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_anno_reg_manutenzione, ll_num_reg_manutenzione, ll_null, ll_count
double ld_frequenza, ld_quan_ricambio, ld_costo_unitario_ricambio, ld_costo_orario_operaio, ld_operaio_ore_previste, &
		 ld_operaio_minuti_previsti, ld_risorsa_ore_previste, ld_risorsa_minuti_previsti, ld_risorsa_costo_orario, &
		 ld_risorsa_costi_aggiuntivi, ld_anno_contratto_assistenza, ld_num_contratto_assistenza, ld_anno_ord_acq_risorsa, &
		 ld_num_ord_acq_risorsa, ld_prog_riga_ord_acq_risorsa, ld_valore_min_taratura, ld_valore_max_taratura, ld_valore_atteso, &
		 ld_tot_costo_intervento, ld_num_reg_lista, ld_prog_liste_con_comp, ld_num_versione, ld_num_edizione
integer li_risposta
boolean lb_chiudi_maschera

setnull(ls_null)
setnull(ldt_null)
setnull(ll_null)

ls_stato_manutenzione = fs_stato_manutenzione
ldt_data_giorno = fdt_data_giorno
ls_cod_attrezzatura = fs_cod_attrezzatura
ls_cod_tipo_manutenzione = fs_cod_tipo_manutenzione

select anno_registrazione,
		 num_registrazione,
		 des_intervento,
		 flag_tipo_intervento,
		 note_idl,
		 flag_ricambio_codificato,
		 cod_ricambio,
		 cod_ricambio_alternativo,
		 ricambio_non_codificato,
		 quan_ricambio,
		 costo_unitario_ricambio,
		 cod_operaio,
		 costo_orario_operaio,
		 operaio_ore_previste,
		 operaio_minuti_previsti,
		 cod_cat_risorse_esterne,
		 cod_risorsa_esterna,
		 risorsa_ore_previste,
	 	 risorsa_minuti_previsti,
	 	 risorsa_costo_orario,
		 risorsa_costi_aggiuntivi,
		 anno_contratto_assistenza,
	 	 num_contratto_assistenza,
	 	 anno_ord_acq_risorsa,
		 num_ord_acq_risorsa,
		 prog_riga_ord_acq_risorsa,
		 rif_contratto_assistenza,
		 rif_ordine_acq_risorsa,
		 flag_budget,
		 cod_primario,
		 cod_misura,
		 valore_min_taratura,
		 valore_max_taratura,
		 valore_atteso,
		 tot_costo_intervento,
		 num_reg_lista,
		 prog_liste_con_comp,
		 num_versione,
		 num_edizione
  into :ll_anno_registrazione,
		 :ll_num_registrazione,
		 :ls_des_intervento,
		 :ls_flag_tipo_intervento,
		 :ls_note_idl,
		 :ls_flag_ricambio_codificato,
		 :ls_cod_ricambio,
		 :ls_cod_ricambio_alternativo,
		 :ls_ricambio_non_codificato,
		 :ld_quan_ricambio,
		 :ld_costo_unitario_ricambio,
		 :ls_cod_operaio,
		 :ld_costo_orario_operaio,
		 :ld_operaio_ore_previste,
		 :ld_operaio_minuti_previsti,
		 :ls_cod_cat_risorse_esterne,
		 :ls_cod_risorsa_esterna,
		 :ld_risorsa_ore_previste,
		 :ld_risorsa_minuti_previsti,
		 :ld_risorsa_costo_orario,
		 :ld_risorsa_costi_aggiuntivi,
		 :ld_anno_contratto_assistenza,
	 	 :ld_num_contratto_assistenza,
	 	 :ld_anno_ord_acq_risorsa,
		 :ld_num_ord_acq_risorsa,
		 :ld_prog_riga_ord_acq_risorsa,
		 :ls_rif_contratto_assistenza,
	 	 :ls_rif_ordine_acq_risorsa,
	 	 :ls_flag_budget,
		 :ls_cod_primario,
		 :ls_cod_misura,
		 :ld_valore_min_taratura,
		 :ld_valore_max_taratura,
		 :ld_valore_atteso,
		 :ld_tot_costo_intervento,
		 :ld_num_reg_lista,
		 :ld_prog_liste_con_comp,
		 :ld_num_versione,
		 :ld_num_edizione
  from programmi_manutenzione
 where cod_azienda = :s_cs_xx.cod_azienda
   and cod_attrezzatura = :ls_cod_attrezzatura
	and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella programmi_manutenzione " + sqlca.sqlerrtext)
	return
end if

ll_anno_reg_manutenzione = year(today())
select max(num_registrazione) + 1 
  into :ll_num_reg_manutenzione
  from manutenzioni
 where cod_azienda = :s_cs_xx.cod_azienda
   and anno_registrazione = :ll_anno_reg_manutenzione;
	
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella manutenzioni " + sqlca.sqlerrtext)
	return
end if	

if isnull(ll_num_reg_manutenzione) or ll_num_reg_manutenzione < 1 then
	ll_num_reg_manutenzione = 1
end if

ldt_data_registrazione = datetime(today())

if ls_stato_manutenzione = "Effettuata" or ls_stato_manutenzione = "Obsoleta" or &
	ls_stato_manutenzione = "Da Confermare" then
	g_mb.messagebox("Omnia", "Non è possibile eseguire la manutenzione.~n~rIl riga selezionata corrisponde a un programma manutenzione già Effettuato, Obsoleto o Da Confermare.")
	return
else	
	
	select count(cod_azienda)
	  into :ll_count
	  from det_cal_progr_manut
	 where cod_azienda = :s_cs_xx.cod_azienda 
	   and anno_registrazione = :ll_anno_registrazione
		and num_registrazione = :ll_num_registrazione
		and (flag_stato_manutenzione = 'A' or flag_stato_manutenzione = 'S')
		and data_giorno < :ldt_data_giorno;
		
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella det_cal_progr_manut " + sqlca.sqlerrtext)
		return
	end if			
	
	if not isnull(ll_count) and ll_count > 0 then
		li_risposta = g_mb.messagebox("Omnia", "Attenzione esistono delle manutenzioni ancora aperte o scadute con data precedente a quella del record selezionato.~n~rSi desidera procedere ?", &
		Exclamation!, YesNo!, 2)	
		if li_risposta = 2 then return	
		if li_risposta = 1 then
			update det_cal_progr_manut
			   set flag_stato_manutenzione = 'O'
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and anno_registrazione = :ll_anno_registrazione
				and num_registrazione = :ll_num_registrazione
				and (flag_stato_manutenzione = 'A' or flag_stato_manutenzione = 'S')
				and data_giorno < :ldt_data_giorno;
			if sqlca.sqlcode <> 0 then 
				g_mb.messagebox("Omnia", "Errore in fase di aggiornamento dati nella tabella det_cal_progr_manut " + sqlca.sqlerrtext)
				return
			end if					
		end if	
	end if	
	
	if ls_stato_manutenzione = "Aperta" then
		ls_flag_stato = 'N'
	elseif ls_stato_manutenzione = "Scaduta" then	
		ls_flag_stato = 'S'		
	end if	
	
	insert into manutenzioni (cod_azienda,
									  anno_registrazione, 
									  num_registrazione,
									  des_intervento,
									  data_registrazione,
									  cod_attrezzatura, 
									  cod_tipo_manutenzione,
									  cod_guasto,
									  flag_ordinario,
									  flag_tipo_intervento, 
									  flag_eseguito,
									  data_prossimo_intervento,
									  anno_non_conf,
									  num_non_conf,
									  note_idl,
									  flag_ricambio_codificato,
									  cod_ricambio,
									  ricambio_non_codificato,
									  quan_ricambio,
									  costo_unitario_ricambio,
									  cod_operaio,
									  costo_orario_operaio,
									  operaio_ore,
									  operaio_minuti,
									  cod_cat_risorse_esterne,
									  cod_risorsa_esterna,
									  risorsa_ore,
									  risorsa_minuti,
									  risorsa_costo_orario,
									  risorsa_costi_aggiuntivi,
									  anno_contratto_assistenza,
									  num_contratto_assistenza,
									  anno_ord_acq_risorsa,
									  num_ord_acq_risorsa,
									  prog_riga_ord_acq_risorsa,
									  rif_contratto_assistenza,
									  rif_ordine_acq_risorsa,
									  flag_budget,
									  cod_primario,
									  cod_misura,
									  valore_max_taratura,
									  valore_min_taratura,
									  valore_atteso,
									  tot_costo_intervento,
									  num_reg_lista,
									  prog_liste_con_comp,
									  num_versione,
									  num_edizione,
									  anno_reg_programma,
									  num_reg_programma,
									  anno_reg_mov_mag,
									  num_reg_mov_mag,
									  flag_scaduta)
							 values (:s_cs_xx.cod_azienda,
										:ll_anno_reg_manutenzione, 
										:ll_num_reg_manutenzione,
										:ls_des_intervento,
										:ldt_data_registrazione,
										:ls_cod_attrezzatura, 
										:ls_cod_tipo_manutenzione,
										:ls_null,
										'S',
										:ls_flag_tipo_intervento, 
										'N',
										:ldt_null,
										:ll_null,
										:ll_null,
										:ls_note_idl,
										:ls_flag_ricambio_codificato,
										:ls_cod_ricambio,
										:ls_ricambio_non_codificato,
										:ld_quan_ricambio,
										:ld_costo_unitario_ricambio,
										:ls_cod_operaio,
										:ld_costo_orario_operaio,
										:ld_operaio_ore_previste,
										:ld_operaio_minuti_previsti,
										:ls_cod_cat_risorse_esterne,
										:ls_cod_risorsa_esterna,
										:ld_risorsa_ore_previste,
										:ld_risorsa_minuti_previsti,
										:ld_risorsa_costo_orario,
										:ld_risorsa_costi_aggiuntivi,
										:ld_anno_contratto_assistenza,
										:ld_num_contratto_assistenza,
										:ld_anno_ord_acq_risorsa,
										:ld_num_ord_acq_risorsa,
										:ld_prog_riga_ord_acq_risorsa,
										:ls_rif_contratto_assistenza,
										:ls_rif_ordine_acq_risorsa,
										:ls_flag_budget,
										:ls_cod_primario,
										:ls_cod_misura,
										:ld_valore_max_taratura,
										:ld_valore_min_taratura,
										:ld_valore_atteso,
										:ld_tot_costo_intervento,
										:ld_num_reg_lista,
										:ld_prog_liste_con_comp,
										:ld_num_versione,
										:ld_num_edizione,
										:ll_anno_registrazione,
										:ll_num_registrazione,
										:ll_null,
										:ll_null,
										:ls_flag_stato);
										
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in inserimento dati nella tabella manutenzioni " + sqlca.sqlerrtext)
		return
	end if	
	
	update det_cal_progr_manut
		set flag_stato_manutenzione = 'D', 
			 anno_reg_manu = :ll_anno_reg_manutenzione,
			 num_reg_manu = :ll_num_reg_manutenzione
	 where cod_azienda = :s_cs_xx.cod_azienda
		and anno_registrazione = :ll_anno_registrazione
		and num_registrazione = :ll_num_registrazione
		and (flag_stato_manutenzione = 'A' or flag_stato_manutenzione = 'S')
		and data_giorno = :ldt_data_giorno;	

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in aggiornamento dati dalla tabella det_cal_progr_manut " + sqlca.sqlerrtext)
		return
	end if	
		
	g_mb.messagebox("Omnia", "Registrazione Effettuata!")	
	s_cs_xx.parametri.parametro_b_1 = true
	s_cs_xx.parametri.parametro_s_11 = ls_cod_tipo_manutenzione
	s_cs_xx.parametri.parametro_d_10 = ll_anno_reg_manutenzione
	s_cs_xx.parametri.parametro_d_11 = ll_num_reg_manutenzione
	close(w_registra_man)
end if		
end subroutine

on uo_registra_manutenzione.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_registra_manutenzione.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

