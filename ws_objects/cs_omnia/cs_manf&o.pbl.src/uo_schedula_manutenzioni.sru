﻿$PBExportHeader$uo_schedula_manutenzioni.sru
$PBExportComments$User object schedula manutezioni furbo
forward
global type uo_schedula_manutenzioni from nonvisualobject
end type
end forward

global type uo_schedula_manutenzioni from nonvisualobject
end type
global uo_schedula_manutenzioni uo_schedula_manutenzioni

type variables
string is_log,is_msg,is_cod_azienda,is_formato_data
double idd_tolleranza_tempo
end variables

forward prototypes
public function integer uof_elimina (ref string fs_errore)
public function integer uof_trova_giorno_libero (datetime fdt_da_data, datetime fdt_a_data, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, boolean fb_tolleranza_tempo, ref datetime fdt_giorno_trovato, ref string fs_errore)
public function integer uof_inserisci_giorno (datetime fdt_data_giorno, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, integer fi_anno_registrazione, long fl_num_registrazione, boolean fb_forza_inserimento, boolean fb_tolleranza_tempo, string fs_flag_stato, ref string fs_errore)
public function integer uof_ordina_normali (ref string fs_errore)
public function integer uof_aggiorna_scadute (datetime fdt_giorno_corrente, datetime fdt_primo_giorno, ref string fs_errore)
public function integer uof_schedula (datetime fdt_da_data, datetime fdt_a_data, datetime fdt_primo_giorno_nuove, datetime fdt_primo_giorno_scadute, boolean fb_mantieni_precedente, boolean fb_tolleranza_tempo, boolean fb_registrazioni_precedenti, ref string fs_errore)
public function integer uof_schedula_priorita (datetime fdt_a_data, boolean fb_tolleranza_tempo, boolean fb_registrazioni_precedenti, ref string fs_errore)
end prototypes

public function integer uof_elimina (ref string fs_errore);// Funzione che elimina la schedulazione presente in tabella det_cal_progr_manut
//
// Autore: Diego Ferrari
// data creazione 18/01/2001

string  ls_cod_attivita,ls_cod_cat_attrezzature,ls_cod_attrezzatura
long 	  ll_num_registrazione  
integer li_anno_registrazione
double  ldd_ore_utilizzate,ldd_num_ore_impegnate
datetime ldt_data_giorno
			
declare r_det_cal_progr_manut cursor for
select  cod_attivita,
		  data_giorno,
		  cod_cat_attrezzature,
		  cod_attrezzatura,
		  anno_registrazione,
		  num_registrazione,
		  ore_utilizzate
from    det_cal_progr_manut
where   cod_azienda=:is_cod_azienda;

open r_det_cal_progr_manut;

do while 1=1
	fetch r_det_cal_progr_manut
	into  :ls_cod_attivita,
		   :ldt_data_giorno,
		   :ls_cod_cat_attrezzature,
		   :ls_cod_attrezzatura,
		   :li_anno_registrazione,
		   :ll_num_registrazione,
		   :ldd_ore_utilizzate;
			
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close r_det_cal_progr_manut;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	select num_ore_impegnate
	into   :ldd_num_ore_impegnate
	from   tab_cal_attivita
	where  cod_azienda=:is_cod_azienda
	and    cod_attivita=:ls_cod_attivita
	and    data_giorno=:ldt_data_giorno
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature
	and    cod_attrezzatura=:ls_cod_attrezzatura;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close r_det_cal_progr_manut;
		return -1
	end if

	ldd_num_ore_impegnate = ldd_num_ore_impegnate - ldd_ore_utilizzate
	
	if (abs(ldd_num_ore_impegnate) - abs(int(ldd_num_ore_impegnate)) < 0.1) or ldd_num_ore_impegnate < 0 then
		ldd_num_ore_impegnate=0
	end if
	
	update tab_cal_attivita  
   set    num_ore_impegnate = :ldd_num_ore_impegnate  
   where  cod_azienda = :is_cod_azienda 
	and    cod_cat_attrezzature = :ls_cod_cat_attrezzature
	and    cod_attrezzatura = :ls_cod_attrezzatura 
	and    cod_attivita = :ls_cod_attivita 
	and    data_giorno = :ldt_data_giorno;

	if sqlca.sqlcode<>0 then
		fs_errore = "Errore nel DB "+SQLCA.SQLErrText
		close r_det_cal_progr_manut;
		return -1
	end if
loop

close r_det_cal_progr_manut;

delete det_cal_progr_manut
where  cod_azienda=:is_cod_azienda;

if sqlca.sqlcode<0 then
	fs_errore = "Errore nel DB "+SQLCA.SQLErrText
	return -1
end if

return 0 
end function

public function integer uof_trova_giorno_libero (datetime fdt_da_data, datetime fdt_a_data, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, boolean fb_tolleranza_tempo, ref datetime fdt_giorno_trovato, ref string fs_errore);// Funzione che trova il primo giorno utile alla schedulazione
// in un periodo compreso da data a data

// Autore: Diego Ferrari
// Creata il 16/01/2001

string ls_cod_cat_attrezzature,ls_cod_attrezzatura_server
double ldd_tempo_esecuzione,ldd_tolleranza_tempo
datetime ldt_giorno_corrente,ldt_giorno_minimo,ldt_giorno_minimo_prec
boolean lb_flag_test,lb_flag_non_trovato
long   ll_num_disponibili
integer li_r


ldt_giorno_corrente = fdt_da_data
	
do while ldt_giorno_corrente <= fdt_a_data

	lb_flag_test = false
	lb_flag_non_trovato = false
	ldt_giorno_minimo_prec = ldt_giorno_corrente
	
	declare r_elenco_risorse cursor for
	select  cod_cat_attrezzature,
			  tempo_esecuzione
	from    elenco_risorse
	where   cod_azienda=:is_cod_azienda
	and     cod_attrezzatura=:fs_cod_attrezzatura
	and     cod_tipo_manutenzione=:fs_cod_tipo_manutenzione;
	
	// Attenzione! Il codice attrezzatura della select soprastante è riferito alla attrezzatura da manutenere
	// e non all'attrezzatura che serve per fare la manutenzione.
	
	open r_elenco_risorse;
	
	do while 1=1
		fetch r_elenco_risorse
		into  :ls_cod_cat_attrezzature,
				:ldd_tempo_esecuzione;
				
		if sqlca.sqlcode<0 then
			fs_errore = "Errore sul DB:" + sqlca.sqlerrtext
			close r_elenco_risorse;
			return -1
		end if
			
		if sqlca.sqlcode = 100 then exit
		
		ldd_tempo_esecuzione = ldd_tempo_esecuzione/60 //trasformo il tempo esecuzione da minuti in ore
		
		if fb_tolleranza_tempo = true then
						
			ldd_tempo_esecuzione = ldd_tempo_esecuzione - idd_tolleranza_tempo
			
		end if
		
		select  min(data_giorno)
		into    :ldt_giorno_minimo
		from    tab_cal_attivita
		where   cod_azienda=:is_cod_azienda
		and     data_giorno between :ldt_giorno_corrente and :fdt_a_data
		and     cod_cat_attrezzature=:ls_cod_cat_attrezzature
		and     num_ore - num_ore_impegnate >= :ldd_tempo_esecuzione;

		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			close r_elenco_risorse;
			return -1
		end if
			
		if isnull(ldt_giorno_minimo) then 
			lb_flag_test = true
			exit
		end if		
		
		if ldt_giorno_minimo_prec <> ldt_giorno_minimo then
			ldt_giorno_corrente = ldt_giorno_minimo
			lb_flag_non_trovato = true
		end if
		
		ldt_giorno_minimo_prec = ldt_giorno_minimo
		
	loop
	
	close r_elenco_risorse;
		
	if lb_flag_test = true then exit
	
	if lb_flag_non_trovato = false then exit
	
loop

if lb_flag_test = true then return 100 // il 100 significa nessun record trovato

if lb_flag_non_trovato = false then
	fdt_giorno_trovato = ldt_giorno_minimo
	return 0
end if

return 0

end function

public function integer uof_inserisci_giorno (datetime fdt_data_giorno, string fs_cod_attrezzatura, string fs_cod_tipo_manutenzione, integer fi_anno_registrazione, long fl_num_registrazione, boolean fb_forza_inserimento, boolean fb_tolleranza_tempo, string fs_flag_stato, ref string fs_errore);// Funzione che alloca le risorse di una manutenzione programmata in un ben determinato giorno 
// del calendario attivita nella specifica tabella delle manutenzioni det_cal_prog_man
//
// Autore: Diego Ferrari
// Creata il 17/01/2001

string   ls_cod_cat_attrezzature,ls_cod_attrezzatura_server,ls_cod_attivita,ls_sql,ls_tempo
double   ldd_tempo_esecuzione,ldd_num_ore_impegnate,ldd_tolleranza_tempo
long     ll_trovato

declare r_elenco_risorse cursor for
select  cod_cat_attrezzature,
		  tempo_esecuzione
from    elenco_risorse
where   cod_azienda=:is_cod_azienda
and     cod_attrezzatura=:fs_cod_attrezzatura
and     cod_tipo_manutenzione=:fs_cod_tipo_manutenzione;

// Attenzione! Il codice attrezzatura della select soprastante è riferito alla attrezzatura da manutenere
// e non all'attrezzatura che serve per fare la manutenzione.

open r_elenco_risorse;

do while 1=1
	fetch r_elenco_risorse
	into  :ls_cod_cat_attrezzature,
			:ldd_tempo_esecuzione;
			
	if sqlca.sqlcode<0 then
		fs_errore = "Errore sul DB:" + sqlca.sqlerrtext
		close r_elenco_risorse;
		return -1
	end if
		
	if sqlca.sqlcode = 100 then exit
	
	ldd_tempo_esecuzione = ldd_tempo_esecuzione/60 //trasformo il tempo esecuzione da minuti in ore

	if fb_tolleranza_tempo = true then
				
		ls_tempo = string(ldd_tempo_esecuzione - idd_tolleranza_tempo)		
		
	else
		ls_tempo = string(ldd_tempo_esecuzione)		
	end if
	
	//***********ATTENZIONE I COMANDI CHE SEGUONO SERVONO AD OVVIARE L'INCOMPATIBILITA'
	// TRA IMPOSTAZIONI INTERNAZIONALI CHE HANNO COME DECIMAL SEPARATOR LA VIRGOLA
	// INVECE CHE IL PUNTO. DA NON CREDERE SIAMO NEL 2001 E ANCORA NON C'E' UNO STANDARD
	// SU QUESTO KAZZO DI SEPARATORE DECIMALE E IO DEVO STARE QUI A PERDERE TEMPO PER QUESTE
	// COSE DEL KAZZO. SPERIAMO CHE FRA 10 O 15 ANNI SI METTANO D'ACCORDO QUESTI KAZZO DI
	// ZUCCONI DEL KAZZO.
	
	ll_trovato = pos(ls_tempo,",")
	
	if ll_trovato>0 then
		ls_tempo = Replace( ls_tempo, ll_trovato, 1, "." )
	end if
	
	//********** FINE dei comandi che se il mondo funzionasse bene non servirebbero
	
	ls_sql = "select  cod_attivita,"+ &
			    			"cod_attrezzatura," + &
			    			"num_ore_impegnate" + &
				 " from    tab_cal_attivita" + &
				 " where   cod_azienda='" + is_cod_azienda + "'" + &
				 " and     data_giorno ='" +  string(date(fdt_data_giorno),is_formato_data) + "'" + &
				 " and     cod_cat_attrezzature='" + ls_cod_cat_attrezzature + "'"
				 
	if fb_forza_inserimento = false then
		ls_sql = ls_sql + "	and  num_ore - num_ore_impegnate >= " + ls_tempo 	
	end if

	ls_sql = ls_sql + "	order by num_ore_impegnate desc"

	declare r_tab_cal_attivita dynamic cursor for sqlsa;

	prepare sqlsa from :ls_sql;

	open dynamic r_tab_cal_attivita;
	
	fetch r_tab_cal_attivita 
	into :ls_cod_attivita,
		  :ls_cod_attrezzatura_server,
		  :ldd_num_ore_impegnate;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close r_tab_cal_attivita;
		close r_elenco_risorse;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then
		fs_errore = "Non è stato trovato alcun giorno disponibile in tab_cal_attivita che abbia ore disponibili. Codice categoria attrezzature " + ls_cod_cat_attrezzature + " data giorno " + string(date(fdt_data_giorno))
		return -1
	end if
	
	close r_tab_cal_attivita;
	
	ldd_num_ore_impegnate = ldd_num_ore_impegnate + ldd_tempo_esecuzione
	
	insert into det_cal_progr_manut
	(cod_azienda,
	 cod_attivita,
	 data_giorno,
	 cod_cat_attrezzature,
	 cod_attrezzatura,
	 anno_registrazione,
	 num_registrazione,
	 ore_utilizzate,
	 flag_stato_manutenzione)
	values
	(:is_cod_azienda,
	 :ls_cod_attivita,
	 :fdt_data_giorno,
	 :ls_cod_cat_attrezzature,
	 :ls_cod_attrezzatura_server,
	 :fi_anno_registrazione,
	 :fl_num_registrazione,
	 :ldd_tempo_esecuzione,
	 :fs_flag_stato);

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close r_elenco_risorse;
		return -1
	end if

	update tab_cal_attivita
	set    num_ore_impegnate=:ldd_num_ore_impegnate
	where  cod_azienda=:is_cod_azienda
	and    cod_attivita=:ls_cod_attivita
	and    data_giorno=:fdt_data_giorno
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature
	and    cod_attrezzatura=:ls_cod_attrezzatura_server;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close r_elenco_risorse;
		return -1
	end if


loop

close r_elenco_risorse;
	
return 0
end function

public function integer uof_ordina_normali (ref string fs_errore);// Funzione che ordina i programmi manutenzione (tabella programmi_manutenzione)
// solo quelli con flag_priorità = 'N'

// Autore: Diego Ferrari 
// Creata il 16/01/2001

integer li_anno_registrazione,li_frequenza,li_r
long    ll_num_registrazione
string  ls_cod_attrezzatura,ls_cod_tipo_manutenzione,ls_periodicita
double  ldd_periodo

delete prog_man_ordinati;

if sqlca.sqlcode < 0 then 
	fs_errore ="Errore sul DB:" + sqlca.sqlerrtext
	return -1
end if

declare r_prog_man cursor for
select anno_registrazione,
		 num_registrazione,
		 cod_attrezzatura,
		 cod_tipo_manutenzione,
		 periodicita,
		 frequenza
from   programmi_manutenzione
where  cod_azienda=:is_cod_azienda
and    flag_priorita='N';

open r_prog_man;

do while 1=1
	fetch r_prog_man 
	into :li_anno_registrazione,
		  :ll_num_registrazione,
		  :ls_cod_attrezzatura,
		  :ls_cod_tipo_manutenzione,
		  :ls_periodicita,
		  :li_frequenza;
	
	if sqlca.sqlcode < 0 then 
		fs_errore ="Errore sul DB:" + sqlca.sqlerrtext
		close r_prog_man;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	if isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura="" or isnull(ls_cod_tipo_manutenzione) or ls_cod_tipo_manutenzione="" or &
		isnull(ls_periodicita) or ls_periodicita="" or isnull(li_frequenza) or li_frequenza=0 then
	
		li_r = f_scrivi_log(is_msg + "La manutenzione programmata anno " + string(li_anno_registrazione) + & 
		        				  " numero " + string(ll_num_registrazione) + " è priva di alcuni dati e pertanto non può essere schedulata." + &
								  " Controllare che sia indicata l'attrezzatura, il tipo manutenzione, la periodicità e il periodo.")
		if li_r = -1 then
			fs_errore="Errore durante la crezione del file log. Verificare configurazione parametro log."
			return -1
		end if
	
		continue
		
	end if

	// *******************************
	// ATTENZIONE!
	// Si ricorda che il campo frequenza è usato impropriamente
	// in realtà indica il periodo
	// **********************************
	
	//Valori del flag_periodicita
	//M=minuti
	//O=ore
	//G=giorni
	//S=settimane
	//E=mesi
	
	choose case ls_periodicita
			
		case 'M'
			ldd_periodo = li_frequenza/60	
			
			
		case 'O'
			ldd_periodo = li_frequenza
			
		case 'G'
			ldd_periodo = li_frequenza*24
			
		case 'S'
			ldd_periodo = li_frequenza*24*7
			
		case 'E'
			ldd_periodo = li_frequenza*24*30
			
	end choose
	
	insert into prog_man_ordinati
	(cod_azienda,
	 anno_registrazione,
	 num_registrazione,
	 cod_attrezzatura,
	 cod_tipo_manutenzione,
	 periodo)
	values
	(:is_cod_azienda,
	 :li_anno_registrazione,
	 :ll_num_registrazione,
	 :ls_cod_attrezzatura,
	 :ls_cod_tipo_manutenzione,
	 :ldd_periodo);
	 
	if sqlca.sqlcode < 0 then 
		fs_errore ="Errore sul DB:" + sqlca.sqlerrtext
		close r_prog_man;
		return -1
	end if
	 
loop

close r_prog_man;

return 0
end function

public function integer uof_aggiorna_scadute (datetime fdt_giorno_corrente, datetime fdt_primo_giorno, ref string fs_errore);// Funzione di aggiornamento manutenzioni schedulate scadute
// Per manutenzione programmata a calendario (schedulata) scaduta si intende una manutenzione programmata 
// in una data precedente alla data corrente e che risulta ancora aperta.
// lo stato manutenzione viene impostato tramite il flag_stato_manutenzione.
// possibili valori del flag_stato_manutenzione:
//																A = Aperta
//																E = Effettuata
//																D = Da confermare
//																S = Scaduta
//																O = Obsoleta
// 

// Autore: Diego Ferrari
// Creata il 01/02/2001

string   ls_cod_attivita,ls_cod_cat_attrezzature,ls_cod_attrezzatura_server,ls_cod_tipo_manutenzione, &
		   ls_cod_attrezzatura,ls_errore,ls_test
datetime ldt_data_giorno,ldt_giorno_massimo
integer  li_anno_registrazione[],li_risposta
long     ll_num_registrazione[],ll_i,ll_t
double   ldd_tempo_esecuzione

if fdt_giorno_corrente > fdt_primo_giorno then
	fs_errore = "Attenzione! La data di primo giorno scadute risulta inferiore alla data corrente. Non è possibile! Reimpostare correttamente le date e riprovare"
	return -1
end if

ll_i = 0

declare  r_det_cal_progr_manut cursor for
select   anno_registrazione,
		   num_registrazione
from     det_cal_progr_manut
where    cod_azienda=:is_cod_azienda
and      data_giorno < :fdt_giorno_corrente
and      (flag_stato_manutenzione = 'A' or flag_stato_manutenzione = 'S')
group by anno_registrazione,num_registrazione
order by anno_registrazione,num_registrazione;

open r_det_cal_progr_manut;

do while 1=1
	ll_i++
	
	fetch r_det_cal_progr_manut
	into  :li_anno_registrazione[ll_i],
			:ll_num_registrazione[ll_i];
			
	if sqlca.sqlcode<0 then
		fs_errore = "Errore sul DB:" + sqlca.sqlerrtext
		close r_det_cal_progr_manut;
		return -1
	end if
		
	if sqlca.sqlcode = 100 then exit
	
loop

close r_det_cal_progr_manut;

for ll_t = 1 to upperbound(li_anno_registrazione[])-1
	
	select cod_tipo_manutenzione,
			 cod_attrezzatura
	into   :ls_cod_tipo_manutenzione,
			 :ls_cod_attrezzatura
	from   programmi_manutenzione
	where  cod_azienda=:is_cod_azienda
	and    anno_registrazione=:li_anno_registrazione[ll_t]
	and    num_registrazione=:ll_num_registrazione[ll_t];

	if sqlca.sqlcode<0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	//** prova a vedere se esiste la stessa programmazione nel giorno fdt_primo_giorno
	
	declare r_test cursor for 
	select cod_azienda
	from   det_cal_progr_manut
	where  cod_azienda=:is_cod_azienda
	and    anno_registrazione=:li_anno_registrazione[ll_t]
	and    num_registrazione=:ll_num_registrazione[ll_t]
	and    data_giorno=:fdt_primo_giorno;

	open r_test;

	fetch r_test into:ls_test;	

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close r_test;
		return -1
	end if

	if isnull(ls_test) or sqlca.sqlcode = 100 then
		
		close r_test;
		
		select max(data_giorno)
		into   :ldt_giorno_massimo
		from   det_cal_progr_manut
		where  cod_azienda=:is_cod_azienda
		and    anno_registrazione=:li_anno_registrazione[ll_t]
		and    num_registrazione=:ll_num_registrazione[ll_t]
		and    data_giorno < :fdt_giorno_corrente
		and    flag_stato_manutenzione = 'A';		
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if

		if not isnull(ldt_giorno_massimo) then
			update det_cal_progr_manut
			set    flag_stato_manutenzione = 'O'
			where  cod_azienda=:is_cod_azienda
			and    anno_registrazione=:li_anno_registrazione[ll_t]
			and    num_registrazione=:ll_num_registrazione[ll_t]
			and    data_giorno = :ldt_giorno_massimo
			and    flag_stato_manutenzione = 'A';
	
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
	
			update det_cal_progr_manut
			set    flag_stato_manutenzione = 'O'
			where  cod_azienda=:is_cod_azienda
			and    anno_registrazione=:li_anno_registrazione[ll_t]
			and    num_registrazione=:ll_num_registrazione[ll_t]
			and    data_giorno < :ldt_giorno_massimo
			and    (flag_stato_manutenzione = 'A' or flag_stato_manutenzione = 'S');
	
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
		end if
		
		li_risposta = uof_inserisci_giorno(fdt_primo_giorno,&
													  ls_cod_attrezzatura,&
													  ls_cod_tipo_manutenzione,&
													  li_anno_registrazione[ll_t],&
													  ll_num_registrazione[ll_t],&
													  true,&
													  false,&
													  'S',&
													  ls_errore)
	
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
		
	else	
		
		close r_test;
		
		update det_cal_progr_manut
		set    flag_stato_manutenzione = 'O'
		where  cod_azienda=:is_cod_azienda
		and    anno_registrazione=:li_anno_registrazione[ll_t]
		and    num_registrazione=:ll_num_registrazione[ll_t]
		and    data_giorno < :fdt_giorno_corrente
		and    (flag_stato_manutenzione = 'A' or flag_stato_manutenzione = 'S');
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if

	end if
	
next

return 0
end function

public function integer uof_schedula (datetime fdt_da_data, datetime fdt_a_data, datetime fdt_primo_giorno_nuove, datetime fdt_primo_giorno_scadute, boolean fb_mantieni_precedente, boolean fb_tolleranza_tempo, boolean fb_registrazioni_precedenti, ref string fs_errore);string   ls_cod_attrezzatura,ls_cod_tipo_manutenzione,ls_errore
integer  li_risposta,li_anno_registrazione,li_periodo_giorni,li_r
long     ll_num_registrazione,ll_controllo
double   ldd_periodo
datetime ldt_data_inizio_test,ldt_primo_giorno,ldt_giorno_corrente, ldt_giorno_trovato,ldt_data_ultima_reg

if fdt_a_data < fdt_primo_giorno_nuove then
	fs_errore = "Attenzione! La data di fine schedulazione deve essere superiore alla data di primo giorno schedulazione nuove o giorno corrente! Reinserire correttamente le date e rilanciare la schedulazione."
	return -1
end if

if fb_tolleranza_tempo = true then
	select tolleranza_tempo
	into   :idd_tolleranza_tempo 
	from   parametri_manutenzioni
	where  cod_azienda=:is_cod_azienda;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	idd_tolleranza_tempo = idd_tolleranza_tempo/60
	
end if

if fb_mantieni_precedente = false then
	li_risposta = uof_elimina(ls_errore)
	if li_risposta = -1 then
		fs_errore=ls_errore
		return -1
	end if
end if

li_risposta = uof_schedula_priorita(fdt_a_data,fb_tolleranza_tempo,fb_registrazioni_precedenti,ls_errore)

if li_risposta = -1 then
	fs_errore=ls_errore
	return -1
end if

li_risposta = uof_ordina_normali(ls_errore)

if li_risposta = -1 then
	fs_errore=ls_errore
	return -1
end if

declare r_prog_man_ordinati cursor for
select anno_registrazione,
		 num_registrazione,
		 cod_attrezzatura,
		 cod_tipo_manutenzione,
		 periodo
from   prog_man_ordinati
where  cod_azienda=:is_cod_azienda
order  by periodo;

open r_prog_man_ordinati;

do while 1=1
	fetch r_prog_man_ordinati
	into :li_anno_registrazione,
		  :ll_num_registrazione,
		  :ls_cod_attrezzatura,
		  :ls_cod_tipo_manutenzione,
		  :ldd_periodo;

	if sqlca.sqlcode < 0 then 
		fs_errore ="Errore sul DB:" + sqlca.sqlerrtext
		close r_prog_man_ordinati;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	li_periodo_giorni = int(ldd_periodo/24)
	
	if li_periodo_giorni = 0 then li_periodo_giorni = 1
	
	if fb_registrazioni_precedenti then								// aggiunto il 19/02/2002 specifica di guerrato "implementazione_man_1" funzione 1
		select max(data_registrazione)
		into   :ldt_data_ultima_reg
		from   manutenzioni
		where  cod_azienda=:is_cod_azienda
		and    anno_reg_programma=:li_anno_registrazione
		and    num_reg_programma=:ll_num_registrazione
		and    flag_eseguito='S';
	
		if sqlca.sqlcode < 0 then 
			fs_errore ="Errore sul DB:" + sqlca.sqlerrtext
			close r_prog_man_ordinati;
			return -1
		end if
	end if
		
	select max(data_giorno)
	into   :ldt_data_inizio_test
	from   det_cal_progr_manut
	where  cod_azienda=:is_cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	
	if sqlca.sqlcode < 0 then 
		fs_errore ="Errore sul DB:" + sqlca.sqlerrtext
		close r_prog_man_ordinati;
		return -1
	end if
		
	if fb_registrazioni_precedenti then 
		if not isnull(ldt_data_ultima_reg) then ldt_data_inizio_test = ldt_data_ultima_reg    
	end if
	
	if not isnull(ldt_data_inizio_test) then          
		ldt_primo_giorno=datetime(relativedate(date(ldt_data_inizio_test),li_periodo_giorni),00:00:00)
		// se esiste già una schedulazione il prossimo giorno sarà la data di quella registrazione + il periodo
	else
		ldt_primo_giorno=fdt_primo_giorno_nuove
	end if

	ldt_giorno_corrente = ldt_primo_giorno
	ll_controllo = 0
   do while ldt_giorno_corrente <= fdt_a_data 	
		
		li_risposta = uof_trova_giorno_libero ( ldt_giorno_corrente, &
														    fdt_a_data, & 
										                ls_cod_attrezzatura, &
										                ls_cod_tipo_manutenzione, &
															 fb_tolleranza_tempo,&
										                ldt_giorno_trovato, &
										                ls_errore )

		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if

		if li_risposta = 100 and ll_controllo = 0 then    //significa che non ha trovato alcun giorno pertanto deve passare alla schedulazione successiva
			li_r = f_scrivi_log(is_msg + "Il programma manutenzione anno " +	string(li_anno_registrazione) + " numero " + string(ll_num_registrazione) + & 
									" non è stato schedulato poichè non vi è tempo disponibile nella fascia di tempo considerata. Verificare che " + &
									"il calendario attività sia stato creato per le risorse legate al programma manutenzione indicato, oppure schedulare fino ad una data superiore rispetto al " + string(date(fdt_a_data)))
			if li_r = -1 then
				fs_errore="Errore durante la crezione del file log. Verificare configurazione parametro log."
				return -1
			end if
			exit
		end if								

		if li_risposta = 100 then 	exit			
		
		li_risposta = uof_inserisci_giorno(ldt_giorno_trovato,&
													  ls_cod_attrezzatura,&
													  ls_cod_tipo_manutenzione,&
													  li_anno_registrazione,&
													  ll_num_registrazione,&
													  false,&
													  fb_tolleranza_tempo,&
													  'A',&
													  ls_errore)

		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
		
		ldt_giorno_corrente=ldt_giorno_trovato
		
		ldt_giorno_corrente = datetime(relativedate(date(ldt_giorno_corrente),li_periodo_giorni),00:00:00)
		ll_controllo++
	loop	
	
loop

close r_prog_man_ordinati; 

return 0
end function

public function integer uof_schedula_priorita (datetime fdt_a_data, boolean fb_tolleranza_tempo, boolean fb_registrazioni_precedenti, ref string fs_errore);// Funzione che permette di schedulare le manutenzioni con flag_priorita impostato a 'S'
// La funzione consente di forzare l'inserimento nella data indicata dal campo data_forzata
// anche se nel calendario il numero di ore impegnate supera il numero di ore disponibili
// 
// Data creazione 19/01/2001

integer li_anno_registrazione,li_frequenza,li_periodo_giorni,li_risposta,li_r
long    ll_num_registrazione,ll_i
string  ls_cod_attrezzatura,ls_cod_tipo_manutenzione,ls_periodicita,ls_errore
double  ldd_periodo
datetime ldt_data_forzata,ldt_data_inizio_test,ldt_giorno_trovato,ldt_data_ultima_reg
boolean lb_forza

declare r_prog_man cursor for
select anno_registrazione,
		 num_registrazione,
		 cod_attrezzatura,
		 cod_tipo_manutenzione,
		 periodicita,
		 frequenza,
		 data_forzata
from   programmi_manutenzione
where  cod_azienda=:is_cod_azienda
and    flag_priorita='S';

open r_prog_man;

do while 1=1
	fetch r_prog_man 
	into :li_anno_registrazione,
		  :ll_num_registrazione,
		  :ls_cod_attrezzatura,
		  :ls_cod_tipo_manutenzione,
		  :ls_periodicita,
		  :li_frequenza,
		  :ldt_data_forzata;
	
	if sqlca.sqlcode < 0 then 
		fs_errore ="Errore sul DB:" + sqlca.sqlerrtext
		close r_prog_man;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	if isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura="" or isnull(ls_cod_tipo_manutenzione) or ls_cod_tipo_manutenzione="" or &
		isnull(ls_periodicita) or ls_periodicita="" or isnull(li_frequenza) or li_frequenza=0 or isnull(ldt_data_forzata) then
		
		li_r = f_scrivi_log(is_msg + "La manutenzione programmata prioritaria anno " + string(li_anno_registrazione) + & 
		         " numero " + string(ll_num_registrazione) + " è priva di alcuni dati e pertanto non può essere schedulata." + &
					" Controllare che sia indicata l'attrezzatura, il tipo manutenzione, la periodicità, il periodo " + &
					" nonchè la data forzata essendo questa una manutenzioni prioritaria.")
		if li_r = -1 then
			fs_errore="Errore durante la crezione del file log. Verificare configurazione parametro log."
			return -1
		end if
		
		continue
		
	end if

	// *******************************
	// ATTENZIONE!
	// Si ricorda che il campo frequenza è usato impropriamente
	// in realtà indica il periodo
	// **********************************
	
	//Valori del flag_periodicita
	//M=minuti
	//O=ore
	//G=giorni
	//S=settimane
	//E=mesi
	
	choose case ls_periodicita
			
		case 'M'
			ldd_periodo = li_frequenza/60	
			
			
		case 'O'
			ldd_periodo = li_frequenza
			
		case 'G'
			ldd_periodo = li_frequenza*24
			
		case 'S'
			ldd_periodo = li_frequenza*24*7
			
		case 'E'
			ldd_periodo = li_frequenza*24*7*30
			
	end choose
	
	li_periodo_giorni = int(ldd_periodo/24)
	
	if li_periodo_giorni = 0 then li_periodo_giorni = 1
	
	if fb_registrazioni_precedenti then								// aggiunto il 19/02/2002 specifica di guerrato "implementazione_man_1" funzione 1
		select max(data_registrazione)
		into   :ldt_data_ultima_reg
		from   manutenzioni
		where  cod_azienda=:is_cod_azienda
		and    anno_reg_programma=:li_anno_registrazione
		and    num_reg_programma=:ll_num_registrazione
		and    flag_eseguito='S';
		
		if sqlca.sqlcode < 0 then 
			fs_errore ="Errore sul DB:" + sqlca.sqlerrtext
			close r_prog_man;
			return -1
		end if
	end if
	
	select max(data_giorno)
	into   :ldt_data_inizio_test
	from   det_cal_progr_manut
	where  cod_azienda=:is_cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	
	if sqlca.sqlcode < 0 then 
		fs_errore ="Errore sul DB:" + sqlca.sqlerrtext
		close r_prog_man;
		return -1
	end if

	lb_forza = true

	if fb_registrazioni_precedenti then 
		if not isnull(ldt_data_ultima_reg) then ldt_data_inizio_test = ldt_data_ultima_reg  
	end if
	
	if not isnull(ldt_data_inizio_test) then
		            
		if ldt_data_inizio_test >= ldt_data_forzata then
			ldt_data_forzata=datetime(relativedate(date(ldt_data_inizio_test),li_periodo_giorni),00:00:00)
			// se esiste già una schedulazione il prossimo giorno sarà la data di quella registrazione + il periodo
			// in questo caso l'inserimento non sarà forzato
			ll_i = 1 //mettendo = 1 l'inserimento non viene forzato
		end if
			// se data inizio test è minore allora forza inserimento
	end if
	

	
	do while ldt_data_forzata <= fdt_a_data 	
		
		if ll_i > 0 then
			li_risposta = uof_trova_giorno_libero ( ldt_data_forzata, &
																 fdt_a_data, & 
																 ls_cod_attrezzatura, &
																 ls_cod_tipo_manutenzione, &
																 fb_tolleranza_tempo,&
																 ldt_giorno_trovato, &
																 ls_errore )
	
			if li_risposta = -1 then
				fs_errore=ls_errore
				return -1
			end if
	
			if li_risposta = 100 and ll_i=0 then    //significa che non ha trovato alcun giorno pertanto deve passare alla schedulazione successiva
				li_r = f_scrivi_log(is_msg + "Il programma manutenzione prioritario anno " +	string(li_anno_registrazione) + " numero " + string(ll_num_registrazione) + & 
									" non è stato schedulato poichè non vi è tempo disponibile nella fascia di tempo considerata. Verificare che " + &
									"il calendario attività sia stato creato per le risorse legate al programma manutenzione indicato, oppure schedulare fino ad una data superiore rispetto al " + string(date(fdt_a_data)))
		   	if li_r = -1 then
				   fs_errore="Errore durante la crezione del file log. Verificare configurazione parametro log."
			   	return -1
		   	end if
				
				exit							  
			end if
			
			if li_risposta = 100 then 	exit	
			
			lb_forza = false
			ldt_data_forzata=ldt_giorno_trovato
			
		end if		
		
		
		
		li_risposta = uof_inserisci_giorno(ldt_data_forzata,&
													  ls_cod_attrezzatura,&
													  ls_cod_tipo_manutenzione,&
													  li_anno_registrazione,&
													  ll_num_registrazione,&
													  lb_forza,&
													  fb_tolleranza_tempo,&
													  'A',&
												     ls_errore)

		if li_risposta = -1 then
			fs_errore=ls_errore
			close r_prog_man;
			return -1
		end if

		
		ldt_data_forzata = datetime(relativedate(date(ldt_data_forzata),li_periodo_giorni),00:00:00)
		
		ll_i++
		
	loop	
	
loop

close r_prog_man;

return 0
end function

on uo_schedula_manutenzioni.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_schedula_manutenzioni.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;is_msg = "Schedulazione Manutenzioni  >>> "
end event

