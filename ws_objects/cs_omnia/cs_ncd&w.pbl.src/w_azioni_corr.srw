﻿$PBExportHeader$w_azioni_corr.srw
$PBExportComments$Window Azioni Correttive
forward
global type w_azioni_corr from w_cs_xx_principale
end type
type cb_1 from cb_apri_manuale within w_azioni_corr
end type
type cb_dettagli from commandbutton within w_azioni_corr
end type
type dw_non_conformita_lista from uo_cs_xx_dw within w_azioni_corr
end type
type dw_non_conformita_std_2 from uo_cs_xx_dw within w_azioni_corr
end type
end forward

global type w_azioni_corr from w_cs_xx_principale
integer width = 3346
integer height = 1504
string title = "Azioni Correttive"
cb_1 cb_1
cb_dettagli cb_dettagli
dw_non_conformita_lista dw_non_conformita_lista
dw_non_conformita_std_2 dw_non_conformita_std_2
end type
global w_azioni_corr w_azioni_corr

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW(dw_non_conformita_std_2,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome", &
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW( dw_non_conformita_std_2, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW( dw_non_conformita_std_2, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")




end event

event pc_setwindow;call super::pc_setwindow;dw_non_conformita_lista.set_dw_options(sqlca,pcca.null_object,c_default , c_default)
dw_non_conformita_std_2.set_dw_options(sqlca,dw_non_conformita_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_non_conformita_lista
end event

on w_azioni_corr.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_dettagli=create cb_dettagli
this.dw_non_conformita_lista=create dw_non_conformita_lista
this.dw_non_conformita_std_2=create dw_non_conformita_std_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_dettagli
this.Control[iCurrent+3]=this.dw_non_conformita_lista
this.Control[iCurrent+4]=this.dw_non_conformita_std_2
end on

on w_azioni_corr.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_dettagli)
destroy(this.dw_non_conformita_lista)
destroy(this.dw_non_conformita_std_2)
end on

type cb_1 from cb_apri_manuale within w_azioni_corr
integer x = 2537
integer y = 1300
integer height = 80
integer taborder = 30
end type

on clicked;call cb_apri_manuale::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SL8")
end on

type cb_dettagli from commandbutton within w_azioni_corr
integer x = 2926
integer y = 1300
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dettaglio"
end type

on clicked;//if not isvalid(w_non_conformita_interna) then
//   s_cs_xx.parametri.parametro_d_1 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "anno_non_conf")
//   s_cs_xx.parametri.parametro_d_2 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "num_non_conf")
//   parent.hide()
//   window_open(w_non_conformita_interna, -1)
//end if
//
//
end on

type dw_non_conformita_lista from uo_cs_xx_dw within w_azioni_corr
integer x = 2469
integer y = 20
integer width = 823
integer height = 1260
integer taborder = 20
string dataobject = "d_azioni_corr_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i,ll_anno,ll_num_registrazione
integer li_anno

li_anno = f_anno_esercizio()

select max(num_non_conf)
into :ll_num_registrazione
from non_conformita
where cod_azienda = :s_cs_xx.cod_azienda
and   anno_non_conf = :li_anno;


if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
	ll_num_registrazione = 1
else
	ll_num_registrazione = ll_num_registrazione + 1
end if


for ll_i = 1 to rowcount()
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
		setitem(ll_i, "anno_non_conf", li_anno)
		setitem(ll_i, "num_non_conf", ll_num_registrazione)	
		
		
   end if
next

end event

type dw_non_conformita_std_2 from uo_cs_xx_dw within w_azioni_corr
integer x = 23
integer y = 20
integer width = 2423
integer height = 1260
integer taborder = 10
string dataobject = "d_azioni_corr_det"
borderstyle borderstyle = styleraised!
end type

