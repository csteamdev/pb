﻿$PBExportHeader$w_azioni_preventive.srw
$PBExportComments$Finestra Dati Standard Azioni Preventive
forward
global type w_azioni_preventive from w_cs_xx_principale
end type
type cb_documento from commandbutton within w_azioni_preventive
end type
type dw_registro_azioni_prev from uo_cs_xx_dw within w_azioni_preventive
end type
type cb_registro from commandbutton within w_azioni_preventive
end type
type cb_approvazione from commandbutton within w_azioni_preventive
end type
type cb_annulla_1 from commandbutton within w_azioni_preventive
end type
type cb_ricerca_1 from commandbutton within w_azioni_preventive
end type
type cb_chiusura from commandbutton within w_azioni_preventive
end type
type cb_report from commandbutton within w_azioni_preventive
end type
type dw_ricerca from u_dw_search within w_azioni_preventive
end type
type dw_folder_search from u_folder within w_azioni_preventive
end type
type dw_azioni_prev_lista from uo_cs_xx_dw within w_azioni_preventive
end type
type dw_azioni_prev_3 from uo_cs_xx_dw within w_azioni_preventive
end type
type dw_azioni_prev_2 from uo_cs_xx_dw within w_azioni_preventive
end type
type dw_folder from u_folder within w_azioni_preventive
end type
type dw_azioni_prev_1 from uo_cs_xx_dw within w_azioni_preventive
end type
end forward

global type w_azioni_preventive from w_cs_xx_principale
integer width = 2834
integer height = 1992
string title = "Azioni Preventive"
cb_documento cb_documento
dw_registro_azioni_prev dw_registro_azioni_prev
cb_registro cb_registro
cb_approvazione cb_approvazione
cb_annulla_1 cb_annulla_1
cb_ricerca_1 cb_ricerca_1
cb_chiusura cb_chiusura
cb_report cb_report
dw_ricerca dw_ricerca
dw_folder_search dw_folder_search
dw_azioni_prev_lista dw_azioni_prev_lista
dw_azioni_prev_3 dw_azioni_prev_3
dw_azioni_prev_2 dw_azioni_prev_2
dw_folder dw_folder
dw_azioni_prev_1 dw_azioni_prev_1
end type
global w_azioni_preventive w_azioni_preventive

type variables
boolean ib_delete=true, ib_in_new=false
string is_prodotto, is_tipo_requisito
string is_flag_apertura_nc, is_flag_chiusura_nc
string is_flag_conferma
end variables

forward prototypes
public function integer wf_cerca_azioni ()
end prototypes

public function integer wf_cerca_azioni ();string   ls_sql, ls_emessa, ls_aperte, ls_approvate, ls_chiuse, ls_verificate, &
			ls_sql1, ls_sql2, ls_sql3, ls_sql4, ls_area_filtro
long     ll_anno, ll_num, l_Error
datetime da_data_azione, a_data_azione, da_prev_chiusura, a_prev_chiusura, da_chiusura, &
         a_chiusura
			
dw_ricerca.accepttext()			
ll_anno = dw_ricerca.getitemnumber(1,"anno_registrazione")
ll_num = dw_ricerca.getitemnumber(1,"num_registrazione")
ls_emessa = dw_ricerca.getitemstring(1,"emessa_da")
da_data_azione = dw_ricerca.getitemdatetime(1,"da_azione_prev")
a_data_azione = dw_ricerca.getitemdatetime(1,"a_azione_prev")
da_prev_chiusura = dw_ricerca.getitemdatetime(1,"da_prevista_chiusura")
a_prev_chiusura = dw_ricerca.getitemdatetime(1,"a_prevista_chiusura")
da_chiusura = dw_ricerca.getitemdatetime(1,"da_chiusura")
a_chiusura = dw_ricerca.getitemdatetime(1,"a_chiusura")
ls_aperte = dw_ricerca.getitemstring(1,"aperta")
ls_approvate = dw_ricerca.getitemstring(1,"approvata")
ls_chiuse = dw_ricerca.getitemstring(1,"chiusa")
ls_verificate = dw_ricerca.getitemstring(1,"verificata")

ls_area_filtro = dw_ricerca.getitemstring(1,"cod_area_aziendale")

ls_sql = "  SELECT azioni_preventive.anno_registrazione,  " &
       + "  		 azioni_preventive.num_registrazione,   " &
       + "  		 azioni_preventive.cod_azienda,     " &
       + "  		 azioni_preventive.cod_divisione,     " & 
       + "  		 azioni_preventive.cod_area_aziendale,    "  &
       + "  		 azioni_preventive.verificata_da,     " &
       + "  		 azioni_preventive.chiusa_da,     " &
       + "  		 azioni_preventive.approvata_da,     " &
       + "  		 azioni_preventive.emessa_da,     " &
       + "  		 azioni_preventive.data_azione_preventiva,     " &
       + "  		 azioni_preventive.descrizione,     " &
       + "  		 azioni_preventive.note_su_responsabile,     " &
       + "  		 azioni_preventive.riferimenti,     " &
       + "  		 azioni_preventive.analisi_cause,     " &
       + "  		 azioni_preventive.azione_preventiva,     " &
       + "  		 azioni_preventive.data_prevista_chiusura,    "  &
       + "  		 azioni_preventive.costo_stimato,     " &
       + "  		 azioni_preventive.note_su_approvazione,     " &
       + "  		 azioni_preventive.data_approvazione,     " &
       + "  		 azioni_preventive.note_su_chiusura,     " &
       + "  		 azioni_preventive.data_chiusura,     " &
       + "  		 azioni_preventive.note_su_ver_efficacia,     " &
       + "  		 azioni_preventive.data_verifica_efficacia    " &
       + "  		 FROM azioni_preventive	 " &
		 + "         WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if ll_anno <> 0 and not isnull(ll_anno) then
	ls_sql = ls_sql + " AND anno_registrazione = " + string(ll_anno)
end if
if ll_num <> 0 and not isnull(ll_num) then
	ls_sql = ls_sql + " AND num_registrazione = " + string(ll_num)
end if
if ls_emessa <> "" and not isnull(ls_emessa) then
	ls_sql = ls_sql + " AND emessa_da = '" + ls_emessa + "' "
end if
if not isnull(da_data_azione) and not isnull(a_data_azione) then
	ls_sql = ls_sql + " AND data_azione_preventiva >= " + string(da_data_azione)
	ls_sql = ls_sql + " AND data_azione_preventiva <= " + string(a_data_azione)
elseif not isnull(da_data_azione) or not isnull(a_data_azione) then
	g_mb.messagebox("OMNIA","Attenzione, completare l'intervallo della data dell'azione preventiva!")
	return -1
end if
if not isnull(da_prev_chiusura) and not isnull(a_prev_chiusura) then
	ls_sql = ls_sql + " AND data_prevista_chiusura >= " + string(da_prev_chiusura)
	ls_sql = ls_sql + " AND data_prevista_chiusura <= " + string(a_prev_chiusura)
elseif not isnull(da_prev_chiusura) or not isnull(a_prev_chiusura) then
	g_mb.messagebox("OMNIA","Attenzione, completare l'intervallo della data di presunta chiusura dell'azione preventiva!")
	return -1	
end if
if not isnull(da_chiusura) and not isnull(a_chiusura) then
	ls_sql = ls_sql + " AND data_chiusura >= " + string(da_chiusura)
	ls_sql = ls_sql + " AND data_chiusura <= " + string(a_chiusura)
elseif not isnull(da_chiusura) or not isnull(a_chiusura) then
	g_mb.messagebox("OMNIA","Attenzione, completare l'intervallo della data di chiusura dell'azione preventiva!")
	return -1	
end if

if not isnull(ls_area_filtro) then
	ls_sql += " and cod_area_aziendale = '" + ls_area_filtro + "' "
end if

ls_sql1 = ""

if ls_aperte = "S" then
	ls_sql1 = " ( approvata_da IS NULL AND chiusa_da IS NULL AND verificata_da IS NULL ) "
end if

if ls_approvate = "S" and ls_sql1 = "" then
	ls_sql1 = " ( approvata_da IS NOT NULL AND chiusa_da IS NULL AND verificata_da IS NULL ) "	
elseif ls_approvate = "S" and ls_sql1 <> "" then
	ls_sql1 += " OR ( approvata_da IS NOT NULL AND chiusa_da IS NULL AND verificata_da IS NULL ) "
end if	

if ls_chiuse = "S" and ls_sql1 = "" then
	ls_sql1 = " ( chiusa_da IS NOT NULL AND approvata_da IS NOT NULL AND verificata_da IS NULL ) "	
elseif ls_chiuse = "S" and ls_sql1 <> "" then
	ls_sql1 += " OR ( chiusa_da IS NOT NULL AND approvata_da IS NOT NULL AND verificata_da IS NULL ) "
end if	

if ls_verificate = "S" and ls_sql1 = "" then
	ls_sql1 = " ( chiusa_da IS NOT NULL AND approvata_da IS NOT NULL AND verificata_da IS NOT NULL ) "	
elseif ls_verificate = "S" and ls_sql1 <> "" then
	ls_sql1 += " OR ( chiusa_da IS NOT NULL AND approvata_da IS NOT NULL AND verificata_da IS NOT NULL ) "
end if	

if (ls_sql1 <> "") then
	ls_sql = ls_sql + " AND ( " + ls_sql1 + " ) "
else 
	g_mb.messagebox("OMNIA","Attenzione: Selezionare almeno un tipo di Azione Preventiva!",stopsign!)
	return -1
end if

ls_sql = ls_sql + "  ORDER BY azioni_preventive.anno_registrazione ASC, azioni_preventive.num_registrazione ASC "


l_Error = dw_azioni_prev_lista.SetTransObject(sqlca)
if l_Error < 0 then
	g_mb.messagebox("OMNIA","Errore nell'impostazione della transazione!")
	return -1
end if

l_Error = dw_azioni_prev_lista.setsqlselect(ls_sql)
if l_Error < 0 then
	g_mb.messagebox("OMNIA","Errore nella retrieve delle Azioni Preventive")
	return -1
end if


return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ], lw_oggetti[ ]
string       l_criteriacolumn[ ], l_searchtable[ ], l_searchcolumn[]



l_objects[1] = dw_azioni_prev_1
dw_folder.fu_AssignTab(1, "Dati Generali", l_Objects[])
l_objects[1] = dw_azioni_prev_2
dw_folder.fu_AssignTab(2, "Analisi/Approv.", l_Objects[])
l_objects[1] = dw_azioni_prev_3
dw_folder.fu_AssignTab(3, "Chius./Verif.", l_Objects[])

dw_folder.fu_FolderCreate(3,3)
dw_folder.fu_SelectTab(1)

dw_azioni_prev_lista.set_dw_key("cod_azienda")
dw_azioni_prev_lista.set_dw_key("anno_registrazione")
dw_azioni_prev_lista.set_dw_key("num_registrazione")
dw_azioni_prev_lista.set_dw_options(sqlca, pcca.null_object, c_noretrieveonopen , c_default)
dw_azioni_prev_1.set_dw_options(sqlca, dw_azioni_prev_lista, c_sharedata + c_scrollparent, c_default)
dw_azioni_prev_2.set_dw_options(sqlca, dw_azioni_prev_lista, c_sharedata + c_scrollparent, c_default)
dw_azioni_prev_3.set_dw_options(sqlca, dw_azioni_prev_lista, c_sharedata + c_scrollparent, c_default)


iuo_dw_main = dw_azioni_prev_lista



// --------------------- roba nova ----------------------

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

lw_oggetti[1] = dw_azioni_prev_lista
dw_folder_search.fu_assigntab(2, "L", lw_oggetti[])
lw_oggetti[1] = dw_ricerca
lw_oggetti[2] = cb_ricerca_1
lw_oggetti[3] = cb_annulla_1
dw_folder_search.fu_assigntab(1, "R", lw_oggetti[])
dw_folder_search.fu_foldercreate(2, 2)
dw_folder_search.fu_selecttab(1)

dw_ricerca.setitem(1, "anno_registrazione", f_anno_esercizio())
dw_ricerca.setitem(1, "aperta", "S")
dw_ricerca.setitem(1, "approvata", "S")
dw_ricerca.setitem(1, "chiusa", "S")
dw_ricerca.setitem(1, "verificata", "S")


end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_azioni_prev_1,"emessa_da",sqlca,&
                 "mansionari","cod_resp_divisione","cognome + ' ' + nome", &
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW(dw_azioni_prev_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_azioni_prev_1,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

				  
f_PO_LoadDDDW_DW(dw_azioni_prev_3,"verificata_da",sqlca,&
                 "mansionari","cod_resp_divisione","cognome + ' ' + nome", &
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")		
					  
					  
//dw_ricerca.fu_loadcode("emessa_da", &
//                 "mansionari", &
//					  "cod_resp_divisione", &
//					  "cognome + ' ' + nome ", &
//					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' order by cognome asc", "" )

f_PO_LoadDDDW_DW(dw_ricerca,"emessa_da",sqlca,&
                 "mansionari","cod_resp_divisione","cognome + ' ' + nome", &
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_ricerca,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_azioni_preventive.create
int iCurrent
call super::create
this.cb_documento=create cb_documento
this.dw_registro_azioni_prev=create dw_registro_azioni_prev
this.cb_registro=create cb_registro
this.cb_approvazione=create cb_approvazione
this.cb_annulla_1=create cb_annulla_1
this.cb_ricerca_1=create cb_ricerca_1
this.cb_chiusura=create cb_chiusura
this.cb_report=create cb_report
this.dw_ricerca=create dw_ricerca
this.dw_folder_search=create dw_folder_search
this.dw_azioni_prev_lista=create dw_azioni_prev_lista
this.dw_azioni_prev_3=create dw_azioni_prev_3
this.dw_azioni_prev_2=create dw_azioni_prev_2
this.dw_folder=create dw_folder
this.dw_azioni_prev_1=create dw_azioni_prev_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_documento
this.Control[iCurrent+2]=this.dw_registro_azioni_prev
this.Control[iCurrent+3]=this.cb_registro
this.Control[iCurrent+4]=this.cb_approvazione
this.Control[iCurrent+5]=this.cb_annulla_1
this.Control[iCurrent+6]=this.cb_ricerca_1
this.Control[iCurrent+7]=this.cb_chiusura
this.Control[iCurrent+8]=this.cb_report
this.Control[iCurrent+9]=this.dw_ricerca
this.Control[iCurrent+10]=this.dw_folder_search
this.Control[iCurrent+11]=this.dw_azioni_prev_lista
this.Control[iCurrent+12]=this.dw_azioni_prev_3
this.Control[iCurrent+13]=this.dw_azioni_prev_2
this.Control[iCurrent+14]=this.dw_folder
this.Control[iCurrent+15]=this.dw_azioni_prev_1
end on

on w_azioni_preventive.destroy
call super::destroy
destroy(this.cb_documento)
destroy(this.dw_registro_azioni_prev)
destroy(this.cb_registro)
destroy(this.cb_approvazione)
destroy(this.cb_annulla_1)
destroy(this.cb_ricerca_1)
destroy(this.cb_chiusura)
destroy(this.cb_report)
destroy(this.dw_ricerca)
destroy(this.dw_folder_search)
destroy(this.dw_azioni_prev_lista)
destroy(this.dw_azioni_prev_3)
destroy(this.dw_azioni_prev_2)
destroy(this.dw_folder)
destroy(this.dw_azioni_prev_1)
end on

event pc_new;call super::pc_new;dw_azioni_prev_lista.setitem(dw_azioni_prev_lista.getrow(), "data_azione_preventiva", datetime(today()))


end event

type cb_documento from commandbutton within w_azioni_preventive
integer x = 2400
integer y = 1780
integer width = 366
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_azioni_prev_lista.accepttext()

if (isnull(dw_azioni_prev_lista.getrow()) or dw_azioni_prev_lista.getrow() < 1) then
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata nessuna azione correttiva!")
	return
end if	

if isnull(dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(), "anno_registrazione")) or &
	dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(), "anno_registrazione") < 2 then
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata nessuna azione correttiva!")
	return
end if	

//s_cs_xx.parametri.parametro_d_10 = dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(), "anno_registrazione")
//s_cs_xx.parametri.parametro_d_11 = dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(), "num_registrazione")
//
//window_open_parm(w_azioni_preventive_blob, -1, dw_azioni_prev_lista)

s_cs_xx.parametri.parametro_ul_1 = dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(), "anno_registrazione")
s_cs_xx.parametri.parametro_ul_2 = dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(), "num_registrazione")

open(w_azioni_preventive_ole)
end event

type dw_registro_azioni_prev from uo_cs_xx_dw within w_azioni_preventive
integer x = 3543
integer y = 640
integer width = 1646
integer height = 620
integer taborder = 170
string dataobject = "d_report_registro_az_prev"
end type

type cb_registro from commandbutton within w_azioni_preventive
integer x = 2400
integer y = 1680
integer width = 366
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Registro"
end type

event clicked;if dw_azioni_prev_lista.rowcount() > 0 then

	s_cs_xx.parametri.parametro_dw_1 = dw_azioni_prev_lista
	if not isvalid(w_registro_azioni_prev) then		
		window_open(w_registro_azioni_prev,-1)
	end if
else
	g_mb.messagebox("OMNIA","Attenzione! La lista di ricerca risulta essere vuota!")
	return -1
end if	



end event

type cb_approvazione from commandbutton within w_azioni_preventive
integer x = 2400
integer y = 1480
integer width = 366
integer height = 80
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Approva"
end type

event clicked;string   ls_chiusa_da, ls_difetto, ls_tipo_lista, ls_cod_lista

long     ll_anno, ll_num, ll_row

datetime ldt_chiusura


ls_chiusa_da = dw_azioni_prev_lista.getitemstring(dw_azioni_prev_lista.getrow(),"approvata_da")
ldt_chiusura = dw_azioni_prev_lista.getitemdatetime(dw_azioni_prev_lista.getrow(),"data_approvazione")

if not isnull(ls_chiusa_da) and not isnull(ldt_chiusura) then	
	g_mb.messagebox("OMNIA","Questa azione preventiva è già stata approvata in data " + string(ldt_chiusura) + " dal mansionario " + ls_chiusa_da)
	return -1
end if

if g_mb.messagebox("OMNIA","Approvare l'azione preventiva?",question!,yesno!,2) = 2 then
	return -1
end if

setnull(ls_chiusa_da)

// stefanop 06/07/2012: TULIOO
//select cod_resp_divisione
//into   :ls_chiusa_da
//from   mansionari
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_utente = :s_cs_xx.cod_utente and
//		 flag_approva_az_prev = 'S';
//		 
//if sqlca.sqlcode < 0 then
//	g_mb.messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
//	return -1
//elseif sqlca.sqlcode = 100 or isnull(ls_chiusa_da) then
//	g_mb.messagebox("OMNIA","Il mansionario corrente non è abilitato all'approvazione delle azioni preventive")
//	return -1
//end if

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

//if luo_mansionario.uof_get_privilege(luo_mansionario.chiusura_nc) then ls_flag_chiusura_nc = 'S'
if not luo_mansionario.uof_get_privilege(luo_mansionario.APPROVA_AZ_PREV) then
	g_mb.error("OMNIA","Il mansionario corrente non è abilitato all'approvazione delle azioni preventive")
	return 
end if

ls_chiusa_da = luo_mansionario.uof_get_cod_mansionario()

destroy luo_mansionario
// -----------------

ll_anno = dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(),"anno_registrazione")
ll_num = dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(),"num_registrazione")

if isnull(ll_anno) or isnull(ll_num) or ll_anno = 0 or ll_num = 0 then
	g_mb.messagebox("OMNIA","Attenzione! Necessario selezionare un'azione preventiva!")
	return -1
end if

ldt_chiusura = datetime(today(),00:00:00)

update azioni_preventive
set    approvata_da = :ls_chiusa_da,
       data_approvazione = :ldt_chiusura
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno and
		 num_registrazione = :ll_num;
		 
if sqlca.sqlcode <> 0 then
	rollback;
	g_mb.messagebox("OMNIA","Errore in Approvazione azione preventiva.~nErrore nella update di azione preventive: " + sqlca.sqlerrtext)
	return -1
end if

commit;

ll_row = dw_azioni_prev_lista.getrow()

dw_azioni_prev_lista.triggerevent("pcd_retrieve")

dw_azioni_prev_lista.setrow(ll_row)

dw_azioni_prev_lista.scrolltorow(ll_row)

if s_cs_xx.num_livello_mail > 0 then
	
	s_cs_xx.parametri.parametro_s_1 = "M"
	s_cs_xx.parametri.parametro_s_2 = ""
	s_cs_xx.parametri.parametro_s_3 = ""
	s_cs_xx.parametri.parametro_s_4 = " flag_approva_az_prev = 'S' "
	s_cs_xx.parametri.parametro_s_5 = "Approvazione Azione Preventiva " + string(ll_anno) + "/" + string(ll_num)
	s_cs_xx.parametri.parametro_s_6 = "E' stata Approvata l'Azione Preventiva " + string(ll_anno) + "/" + string(ll_num)
	s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista dei mansionari con privilegio di approvazione?"
	s_cs_xx.parametri.parametro_s_8 = ""
	
	openwithparm(w_invio_messaggi,0)

end if
end event

type cb_annulla_1 from commandbutton within w_azioni_preventive
integer x = 2377
integer y = 160
integer width = 361
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_ricerca.fu_Reset()

end event

type cb_ricerca_1 from commandbutton within w_azioni_preventive
integer x = 2377
integer y = 60
integer width = 361
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;//dw_ricerca.fu_BuildSearch(TRUE)
//dw_folder_search.fu_SelectTab(2)
//dw_azioni_prev_lista.change_dw_current()
//parent.triggerevent("pc_retrieve")
//

long ll_err
ll_err = wf_cerca_azioni()
if ll_err <> -1 then
	dw_folder_search.fu_SelectTab(2)
	dw_azioni_prev_lista.change_dw_current()	
	parent.triggerevent("pc_retrieve")	
end if

end event

type cb_chiusura from commandbutton within w_azioni_preventive
integer x = 2400
integer y = 1580
integer width = 366
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;string   ls_chiusa_da, ls_difetto, ls_tipo_lista, ls_cod_lista

long     ll_anno, ll_num, ll_row

datetime ldt_chiusura

ls_chiusa_da = dw_azioni_prev_lista.getitemstring(dw_azioni_prev_lista.getrow(),"approvata_da")

if isnull(ls_chiusa_da) or ls_chiusa_da = "" then
	g_mb.messagebox("OMNIA","Questa azione preventiva deve essere prima approvata! ")
	return -1	
end if

setnull(ls_chiusa_da)
ls_chiusa_da = dw_azioni_prev_lista.getitemstring(dw_azioni_prev_lista.getrow(),"chiusa_da")
ldt_chiusura = dw_azioni_prev_lista.getitemdatetime(dw_azioni_prev_lista.getrow(),"data_chiusura")

if not isnull(ls_chiusa_da) and not isnull(ldt_chiusura) then	
	g_mb.messagebox("OMNIA","Questa azione preventiva è già stata chiusa in data " + string(ldt_chiusura) + " dal mansionario " + ls_chiusa_da)
	return -1
end if

if g_mb.messagebox("OMNIA","Chiudere l'azione preventiva?",question!,yesno!,2) = 2 then
	return -1
end if

setnull(ls_chiusa_da)

//select cod_resp_divisione
//into   :ls_chiusa_da
//from   mansionari
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_utente = :s_cs_xx.cod_utente and
//		 flag_chiudi_az_prev = 'S';
//		 
//if sqlca.sqlcode < 0 then
//	g_mb.messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
//	return -1
//elseif sqlca.sqlcode = 100 or isnull(ls_chiusa_da) then
//	g_mb.messagebox("OMNIA","Il mansionario corrente non è abilitato alla chiusura delle azioni preventive")
//	return -1
//end if

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

//if luo_mansionario.uof_get_privilege(luo_mansionario.chiusura_nc) then ls_flag_chiusura_nc = 'S'
if not luo_mansionario.uof_get_privilege(luo_mansionario.CHIUSURA_AZ_PREV) then
	g_mb.error("OMNIA","Il mansionario corrente non è abilitato alla chiusura delle azioni preventive")
	return 
end if

ls_chiusa_da = luo_mansionario.uof_get_cod_mansionario()

destroy luo_mansionario
// -----------------

ll_anno = dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(),"anno_registrazione")
ll_num = dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(),"num_registrazione")
ldt_chiusura = datetime(today(),now())

update azioni_preventive
set    chiusa_da = :ls_chiusa_da,
       data_chiusura = :ldt_chiusura
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno and
		 num_registrazione = :ll_num;
		 
if sqlca.sqlcode <> 0 then
	rollback;
	g_mb.messagebox("OMNIA","Errore in chiusura azione preventiva.~nErrore nella update di azione preventive: " + sqlca.sqlerrtext)
	return -1
end if

commit;
ll_row = dw_azioni_prev_lista.getrow()

dw_azioni_prev_lista.triggerevent("pcd_retrieve")

dw_azioni_prev_lista.setrow(ll_row)

dw_azioni_prev_lista.scrolltorow(ll_row)

if s_cs_xx.num_livello_mail > 0 then

	
	s_cs_xx.parametri.parametro_s_1 = "M"
	s_cs_xx.parametri.parametro_s_2 = ""
	s_cs_xx.parametri.parametro_s_3 = ""
	s_cs_xx.parametri.parametro_s_4 = " flag_chiudi_az_prev = 'S' "
	s_cs_xx.parametri.parametro_s_5 = "Chiusura Azione Preventiva " + string(ll_anno) + "/" + string(ll_num)
	s_cs_xx.parametri.parametro_s_6 = "E' stata chiusa l'Azione Preventiva " + string(ll_anno) + "/" + string(ll_num)
	s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista dei mansionari con privilegio di chiusura?"
	s_cs_xx.parametri.parametro_s_8 = ""
	
	openwithparm(w_invio_messaggi,0)

end if
end event

type cb_report from commandbutton within w_azioni_preventive
integer x = 2400
integer y = 1380
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;if not isvalid(w_report_azioni_prev) then
	
	if dw_azioni_prev_lista.getrow() < 1 then
		g_mb.messagebox("OMNIA","Selezionare un'azione preventiva dalla lista",stopsign!)
		return -1
	end if

	setnull(s_cs_xx.parametri.parametro_d_1)
	setnull(s_cs_xx.parametri.parametro_d_2)
	s_cs_xx.parametri.parametro_d_1 = dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(),"anno_registrazione")
	s_cs_xx.parametri.parametro_d_2 = dw_azioni_prev_lista.getitemnumber(dw_azioni_prev_lista.getrow(),"num_registrazione")
	
	if isnull(s_cs_xx.parametri.parametro_d_2) or s_cs_xx.parametri.parametro_d_2 = 0 or isnull(s_cs_xx.parametri.parametro_d_1) or s_cs_xx.parametri.parametro_d_1 = 0 then
		g_mb.messagebox("OMNIA","Selezionare un'azione preventiva dalla lista",stopsign!)
		return -1
	end if
	
	window_open(w_report_azioni_prev,-1)

end if
end event

type dw_ricerca from u_dw_search within w_azioni_preventive
integer x = 229
integer y = 40
integer width = 2194
integer height = 560
integer taborder = 90
string dataobject = "d_azioni_preventive_ricerca"
boolean border = false
end type

type dw_folder_search from u_folder within w_azioni_preventive
integer x = 23
integer y = 20
integer width = 2743
integer height = 600
integer taborder = 110
end type

type dw_azioni_prev_lista from uo_cs_xx_dw within w_azioni_preventive
integer x = 206
integer y = 60
integer width = 2057
integer height = 456
integer taborder = 40
string dataobject = "d_azioni_preventive_lista"
boolean vscrollbar = true
end type

event pcd_new;call super::pcd_new;long ll_anno,ll_num_registrazione

dw_azioni_prev_lista.SetItem (dw_azioni_prev_lista.GetRow ( ),"anno_registrazione", f_anno_esercizio() )
dw_azioni_prev_lista.SetItem (dw_azioni_prev_lista.GetRow ( ),"cod_azienda", s_cs_xx.cod_azienda )
dw_azioni_prev_lista.setitem (dw_azioni_prev_lista.getrow(), "data_azione_preventiva", datetime(today()))
ll_anno = dw_azioni_prev_lista.GetItemNumber(dw_azioni_prev_lista.GetRow ( ),"anno_registrazione" )
	
select max(azioni_preventive.num_registrazione)
into :ll_num_registrazione
from azioni_preventive
where (azioni_preventive.cod_azienda = :s_cs_xx.cod_azienda) and (:ll_anno = azioni_preventive.anno_registrazione);

if (sqlca.sqlcode <0 ) then
	g_mb.messagebox("Azioni Preventive", "Errore nel numero di registrazione: " + sqlca.sqlerrtext)
end if
	
if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
	ll_num_registrazione = 1
else
   ll_num_registrazione = ll_num_registrazione + 1
end if
dw_azioni_prev_lista.SetItem (dw_azioni_prev_lista.GetRow ( ),"num_registrazione", ll_num_registrazione)


end event

event updatestart;call super::updatestart;if s_cs_xx.num_livello_mail > 0 then
	if getitemstatus(this.getrow(),0,primary!) = datamodified!	then			
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		setnull(s_cs_xx.parametri.parametro_s_4)
		s_cs_xx.parametri.parametro_s_5 = "Modifica dell'azione preventiva " + string(this.getitemnumber(this.getrow(),"anno_registrazione")) + "/" + string(this.getitemnumber(this.getrow(),"num_registrazione"))
		s_cs_xx.parametri.parametro_s_6 = "Modifica dell'azione preventiva " + string(this.getitemnumber(this.getrow(),"anno_registrazione")) + "/" + string(this.getitemnumber(this.getrow(),"num_registrazione"))
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari?"
		s_cs_xx.parametri.parametro_s_8 = ""
	
		openwithparm(w_invio_messaggi,0)
	elseif getitemstatus(this.getrow(),0,primary!) = newmodified! then
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		setnull(s_cs_xx.parametri.parametro_s_4)
		s_cs_xx.parametri.parametro_s_5 = "Creazione dell'azione preventiva " + string(this.getitemnumber(this.getrow(),"anno_registrazione")) + "/" + string(this.getitemnumber(this.getrow(),"num_registrazione"))
		s_cs_xx.parametri.parametro_s_6 = "Creazione dell'azione preventiva " + string(this.getitemnumber(this.getrow(),"anno_registrazione")) + "/" + string(this.getitemnumber(this.getrow(),"num_registrazione"))
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari?"
		s_cs_xx.parametri.parametro_s_8 = ""
	
		openwithparm(w_invio_messaggi,0)
		
	end if
end if



end event

event pcd_validaterow;call super::pcd_validaterow;long ll_oggi, ll_scad
datetime ldt_data

if isnull(this.getitemstring(this.getrow(), "emessa_da")) then
   g_mb.messagebox("Azioni Preventive","Codice responsabile azione preventiva obbligatorio!",Exclamation!)
   pcca.error = c_fatal
   return
end if

ldt_data = this.getitemdatetime(this.getrow(),"data_azione_preventiva")

if isnull(ldt_data) then
   g_mb.messagebox("Azioni Preventive","Data dell'azione preventiva obbligatoria!",Exclamation!)
   pcca.error = c_fatal
   return
end if
end event

event pcd_retrieve;call super::pcd_retrieve;long l_Error

l_Error = Retrieve()
if l_Error < 0 then
	PCCA.Error = c_Fatal
end if
end event

type dw_azioni_prev_3 from uo_cs_xx_dw within w_azioni_preventive
integer x = 114
integer y = 780
integer width = 2149
integer height = 920
integer taborder = 140
string dataobject = "d_azioni_prev_3"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_resp

choose case i_colname
	case "chiusa_da"
		ls_resp = this.getitemstring(this.getrow(),"approvata_da")
		if ls_resp = "" or isnull(ls_resp) then
			g_mb.messagebox("OMNIA","Attenzione! L'azione preventiva deve essere prima Approvata!")
			setnull(ls_resp)
			this.setitem(this.getrow(), "chiusa_da", ls_resp)
			return 0
		end if

	case "verificata_da"
		ls_resp = this.getitemstring(this.getrow(),"chiusa_da")
		if ls_resp = "" or isnull(ls_resp) then
			g_mb.messagebox("OMNIA","Attenzione! L'azione preventiva deve essere prima chiusa!")
			setnull(ls_resp)
			this.setitem(this.getrow(), "verificata_da", ls_resp)
			return 0
		end if
		
end choose
end event

type dw_azioni_prev_2 from uo_cs_xx_dw within w_azioni_preventive
integer x = 69
integer y = 780
integer width = 2240
integer height = 1040
integer taborder = 150
string dataobject = "d_azioni_prev_2"
boolean border = false
end type

type dw_folder from u_folder within w_azioni_preventive
integer x = 23
integer y = 640
integer width = 2354
integer height = 1220
integer taborder = 160
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Generici"
      SetFocus(dw_azioni_prev_1)
   CASE "Approvazione"
      SetFocus(dw_azioni_prev_2)
   CASE "Chius./Verif."
      SetFocus(dw_azioni_prev_3)
END CHOOSE



end event

type dw_azioni_prev_1 from uo_cs_xx_dw within w_azioni_preventive
integer x = 46
integer y = 760
integer width = 2286
integer height = 1080
integer taborder = 130
string dataobject = "d_azioni_prev_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_nulla, ls_cod_tipo_causa, ls_cod_divisione
	setnull(ls_nulla)
   choose case i_colname			 
		case "cod_divisione"
			setitem( i_rownbr, "cod_area_aziendale", ls_nulla)
			f_PO_LoadDDDW_DW( dw_azioni_prev_1, &
							"cod_area_aziendale", &
							sqlca, &
							"tab_aree_aziendali", &
							"cod_area_aziendale", &
                     "des_area", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + &
								"' and cod_divisione = '" + i_coltext + "' ")
		case "cod_area_aziendale"
			select cod_divisione
			into :ls_cod_divisione
			from tab_aree_aziendali
			where cod_azienda = :s_cs_xx.cod_azienda 
			  and cod_area_aziendale =:i_coltext;
			if sqlca.sqlcode = 0 then
				setitem(i_rownbr, "cod_divisione", ls_cod_divisione)
			end if
			 
	end choose
end if
end event

