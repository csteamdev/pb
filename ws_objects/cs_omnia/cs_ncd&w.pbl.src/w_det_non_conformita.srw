﻿$PBExportHeader$w_det_non_conformita.srw
$PBExportComments$Finestra Dettaglio Non Conformità per gestione documenti
forward
global type w_det_non_conformita from w_cs_xx_principale
end type
type dw_det_non_conformita from uo_cs_xx_dw within w_det_non_conformita
end type
type cb_chiavi from commandbutton within w_det_non_conformita
end type
type cb_note_esterne from commandbutton within w_det_non_conformita
end type
end forward

global type w_det_non_conformita from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2875
integer height = 1048
string title = "Documenti non Conformità"
dw_det_non_conformita dw_det_non_conformita
cb_chiavi cb_chiavi
cb_note_esterne cb_note_esterne
end type
global w_det_non_conformita w_det_non_conformita

type variables
long il_anno, il_num
end variables

event pc_setwindow;call super::pc_setwindow;dw_det_non_conformita.set_dw_key("cod_azienda")
dw_det_non_conformita.set_dw_options(sqlca, &
											i_openparm, &
											c_scrollparent, &
											c_default)

iuo_dw_main = dw_det_non_conformita
end event

on w_det_non_conformita.create
int iCurrent
call super::create
this.dw_det_non_conformita=create dw_det_non_conformita
this.cb_chiavi=create cb_chiavi
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_non_conformita
this.Control[iCurrent+2]=this.cb_chiavi
this.Control[iCurrent+3]=this.cb_note_esterne
end on

on w_det_non_conformita.destroy
call super::destroy
destroy(this.dw_det_non_conformita)
destroy(this.cb_chiavi)
destroy(this.cb_note_esterne)
end on

type dw_det_non_conformita from uo_cs_xx_dw within w_det_non_conformita
integer x = 23
integer y = 20
integer width = 2789
integer height = 800
integer taborder = 10
string dataobject = "d_det_non_conformita"
end type

event pcd_modify;call super::pcd_modify;cb_chiavi.enabled = false
cb_note_esterne.enabled = false	
end event

event pcd_new;call super::pcd_new;setitem(getrow(),"flag_blocco","N")

cb_chiavi.enabled = false
cb_note_esterne.enabled = false
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error


il_anno = i_parentdw.getitemnumber(i_parentdw.getrow(),"anno_non_conf")
il_num = i_parentdw.getitemnumber(i_parentdw.getrow(),"num_non_conf")

parent.title = "Documenti - Non Conformità " + string(il_anno) + "/" + string(il_num)

l_Error = Retrieve(s_cs_xx.cod_azienda,il_anno,il_num)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_save;call super::pcd_save;cb_chiavi.enabled = true
cb_note_esterne.enabled = true
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_progressivo, ll_num_protocollo


for ll_i = 1 to this.rowcount()
	
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemnumber(ll_i,"anno_non_conf")) then
		setitem(ll_i,"anno_non_conf",il_anno)
	end if
	
	if isnull(getitemnumber(ll_i,"num_non_conf")) then
		setitem(ll_i,"num_non_conf",il_num)
	end if
	
	if isnull(getitemnumber(ll_i,"prog_non_conformita")) then
		
		select max(prog_non_conformita)
		into	 :ll_progressivo
		from 	 det_non_conformita
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 anno_non_conf = :il_anno and
				 num_non_conf = :il_num;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura massimo progressivo documento: " + sqlca.sqlerrtext,stopsign!)
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) then
			ll_progressivo = 0
		end if
		
		ll_progressivo ++
		
		setitem(ll_i,"prog_non_conformita",ll_progressivo)
		
	end if
	
	if isnull(getitemnumber(ll_i,"num_protocollo")) then
		
		setnull(ll_num_protocollo)
		
		select max(num_protocollo)
		into 	 :ll_num_protocollo
		from 	 tab_protocolli
		where  cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura massimo progressivo protocollo: " + sqlca.sqlerrtext,stopsign!)
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ll_num_protocollo) then
			ll_num_protocollo = 0
		end if
		
		ll_num_protocollo ++	
		
		insert
		into   tab_protocolli
				 (cod_azienda,
				 num_protocollo)
		values (:s_cs_xx.cod_azienda,
				 :ll_num_protocollo);
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in aggiornamento tabella protocolli: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return -1
		end if
		
		commit;
		
		setitem(ll_i,"num_protocollo",ll_num_protocollo)
		
	end if
	
next
end event

event pcd_view;call super::pcd_view;cb_chiavi.enabled = true
cb_note_esterne.enabled = true
end event

event updateend;call super::updateend;long ll_i, ll_num_protocollo


for ll_i = 1 to deletedcount()
	
	ll_num_protocollo = getitemnumber(ll_i,"num_protocollo",delete!,true)

	delete
	from  tab_chiavi_protocollo
	where cod_azienda = :s_cs_xx.cod_azienda and
			num_protocollo = :ll_num_protocollo;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in cancellazione chiavi procollo: " + sqlca.sqlerrtext,stopsign!)
		return 1
	end if
	
	delete
	from  tab_protocolli
	where cod_azienda = :s_cs_xx.cod_azienda and
			num_protocollo = :ll_num_protocollo;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in cancellazione procollo: " + sqlca.sqlerrtext,stopsign!)
		return 1
	end if
	
next

cb_chiavi.enabled = true
cb_note_esterne.enabled = true
end event

type cb_chiavi from commandbutton within w_det_non_conformita
integer x = 2057
integer y = 840
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiavi"
end type

event clicked;if dw_det_non_conformita.getrow() < 1 or &
	isnull(dw_det_non_conformita.getrow()) then
	g_mb.messagebox("Omnia", "Inserire almeno un dettaglio")
	return
end if	

window_open_parm(w_tab_chiavi_protocollo, -1, dw_det_non_conformita)
end event

type cb_note_esterne from commandbutton within w_det_non_conformita
integer x = 2446
integer y = 840
integer width = 366
integer height = 80
integer taborder = 3
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;long        ll_i, ll_progressivo, ll_prog_mimetype, ll_anno_nc, ll_num_nc
string      ls_db, ls_gestione_doc
integer     li_risposta
transaction sqlcb
blob        lbl_null
setnull(lbl_null)

ll_i = dw_det_non_conformita.getrow()

if ll_i <= 0 then return

setnull(lbl_null)

ll_anno_nc = dw_det_non_conformita.i_parentdw.getitemnumber(dw_det_non_conformita.i_parentdw.i_selectedrows[1], "anno_non_conf")
ll_num_nc = dw_det_non_conformita.i_parentdw.getitemnumber(dw_det_non_conformita.i_parentdw.i_selectedrows[1], "num_non_conf")
ll_progressivo = dw_det_non_conformita.getitemnumber(ll_i, "prog_non_conformita")

ls_db = f_db()

if ls_db = "MSSQL" then

	li_risposta = f_crea_sqlcb(sqlcb)

	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       det_non_conformita
	where      cod_azienda = :s_cs_xx.cod_azienda and
				  anno_non_conf = :ll_anno_nc and 
				  num_non_conf = :ll_num_nc and 
				  prog_non_conformita = :ll_progressivo
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
		s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
		
	destroy sqlcb;	
	
else
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       det_non_conformita
	where      cod_azienda = :s_cs_xx.cod_azienda and
				  anno_non_conf = :ll_anno_nc and 
				  num_non_conf = :ll_num_nc and 
				  prog_non_conformita = :ll_progressivo;
	
	if sqlca.sqlcode <> 0 then
		s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if		

end if	
	
// *** Michela 16/03/2006: leggo il progressivo del mimetype
ll_prog_mimetype = dw_det_non_conformita.getitemnumber( ll_i, "prog_mimetype")

// *** se il blob non è nullo ed esiste il prog mimetype: sono documenti intranet
// *** se il blob non è nullo ma il progressivo si: sono documenti client/server
// *** se il blob è nullo: procedo con l'inserimento intranet

if isnull(s_cs_xx.parametri.parametro_bl_1) or ( not isnull(s_cs_xx.parametri.parametro_bl_1) and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 ) then

	s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype
	
	window_open(w_ole_documenti, 0)
	
	ll_prog_mimetype = s_cs_xx.parametri.parametro_d_1
	
	if not isnull(s_cs_xx.parametri.parametro_bl_1) and len(s_cs_xx.parametri.parametro_bl_1) > 0  and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			updateblob det_non_conformita
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  anno_non_conf = :ll_anno_nc and 
						  num_non_conf = :ll_num_nc and 
						  prog_non_conformita = :ll_progressivo
			using      sqlcb;		
			
			if sqlcb.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", sqlcb.sqlerrtext)
			end if				
			
			destroy sqlcb;
		
		else
		
			updateblob det_non_conformita
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  anno_non_conf = :ll_anno_nc and 
						  num_non_conf = :ll_num_nc and 
						  prog_non_conformita = :ll_progressivo;
					
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", sqlca.sqlerrtext)
			end if	
			
		end if
					
		update     det_non_conformita
		set        prog_mimetype = :ll_prog_mimetype
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_non_conf = :ll_anno_nc and 
					  num_non_conf = :ll_num_nc and 
					  prog_non_conformita = :ll_progressivo;
					  
		commit;
		
		parent.triggerevent("pc_view")
		
	end if
else
	
	window_open(w_ole, 0)
	
	if not isnull(s_cs_xx.parametri.parametro_bl_1) then
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			updateblob det_non_conformita
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  anno_non_conf = :ll_anno_nc and 
						  num_non_conf = :ll_num_nc and 
						  prog_non_conformita = :ll_progressivo
			using      sqlcb;		
			
			destroy sqlcb;
			
		else
			
			updateblob det_non_conformita
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  anno_non_conf = :ll_anno_nc and 
						  num_non_conf = :ll_num_nc and 
						  prog_non_conformita = :ll_progressivo;
	
		end if
		
		commit;
		
	end if

end if		
end event

