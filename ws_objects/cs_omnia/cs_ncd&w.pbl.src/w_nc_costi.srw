﻿$PBExportHeader$w_nc_costi.srw
forward
global type w_nc_costi from w_cs_xx_principale
end type
type dw_nc_costi from uo_cs_xx_dw within w_nc_costi
end type
end forward

global type w_nc_costi from w_cs_xx_principale
integer width = 2281
integer height = 1528
string title = "Costi - Non Conformità"
boolean maxbox = false
boolean resizable = false
dw_nc_costi dw_nc_costi
end type
global w_nc_costi w_nc_costi

type variables
long il_anno, il_num
end variables

on w_nc_costi.create
int iCurrent
call super::create
this.dw_nc_costi=create dw_nc_costi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_nc_costi
end on

on w_nc_costi.destroy
call super::destroy
destroy(this.dw_nc_costi)
end on

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_nc_costi

dw_nc_costi.change_dw_current()

dw_nc_costi.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_nc_costi,"cod_tipo_costo",sqlca,"tab_tipi_costi","cod_tipo_costo","des_tipo_costo","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_nc_costi from uo_cs_xx_dw within w_nc_costi
integer x = 23
integer y = 20
integer width = 2217
integer height = 1400
integer taborder = 10
string dataobject = "d_nc_costi"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;il_anno = i_parentdw.getitemnumber(i_parentdw.getrow(),"anno_non_conf")

il_num = i_parentdw.getitemnumber(i_parentdw.getrow(),"num_non_conf")

parent.title = "Costi - Non Conformità " + string(il_anno) + "/" + string(il_num)

retrieve(s_cs_xx.cod_azienda,il_anno,il_num)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
		setitem(ll_i,"anno_non_conf",il_anno)
		setitem(ll_i,"num_non_conf",il_num)
	end if
next
end event

event updatestart;call super::updatestart;dec{4}	ld_tot_costi_nc


if rowcount() > 0 then
	ld_tot_costi_nc = getitemnumber(rowcount(),"cf_totale")
else
	ld_tot_costi_nc = 0
end if

update
	non_conformita
set
	costo_nc = :ld_tot_costi_nc
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_non_conf = :il_anno and
	num_non_conf = :il_num;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in aggiornamento costo totale NC: " + sqlca.sqlerrtext,stopsign!)
	return 1
end if
end event

