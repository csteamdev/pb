﻿$PBExportHeader$w_non_conformita.srw
forward
global type w_non_conformita from w_cs_xx_treeview
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_2 dw_2
end type
type det_3 from userobject within tab_dettaglio
end type
type dw_3 from uo_cs_xx_dw within det_3
end type
type det_3 from userobject within tab_dettaglio
dw_3 dw_3
end type
type det_4 from userobject within tab_dettaglio
end type
type dw_4 from uo_cs_xx_dw within det_4
end type
type det_4 from userobject within tab_dettaglio
dw_4 dw_4
end type
end forward

global type w_non_conformita from w_cs_xx_treeview
integer width = 4087
integer height = 2520
string title = "Non Conformita"
event pc_menu_comunicazioni ( )
event pc_menu_respingi ( )
event pc_menu_invia ( )
event pc_menu_chiudi ( )
event pc_menu_stock ( )
event pc_menu_costi ( )
event pc_menu_report ( )
event pc_menu_dettagli ( )
event pc_menu_documenti ( )
event ue_carica_area_aziendale ( )
event pc_menu_gen_richiesta ( )
end type
global w_non_conformita w_non_conformita

type variables
public:
	// Abilita i pulsanti di ricerca
	boolean ib_pulsanti_padre = true
	
	// numero di elementi di ricerca massimi nella treeview
	int ii_max_treeview_result = 500
	
protected:
	uo_mansionario iuo_mansionario
	boolean ib_visualizza_campi

private:
	datastore ids_store
	
	long il_livello = 1
	
	string is_cod_tipi_nc[] = {"A", "I","S", "V", "P"}
	string is_des_tipi_nc[] = {"Approvvigionamenti", "Produzione","Sistema", "Vendita", "Progettazione"}
	
	string is_flag_BNC, is_flag_conferma, is_flag_nc_abcd
	boolean ib_wizard, ib_open_costi, ib_new

	// ICONE
	int ICONA_NC, ICONA_DATA_SCADENZA, ICONA_DATA_REGISTRAZIONE, ICONA_PRODOTTO, ICONA_TIPO_NC
	int ICONA_TIPO_CAUSA, ICONA_CAUSA, ICONA_NC_SELEZIONATA
end variables

forward prototypes
public subroutine wf_imposta_ricerca ()
public function integer wf_inserisci_tipo_nc (long al_handle)
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function integer wf_inserisci_nc (long al_handle)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public subroutine wf_proteggi_documento (string fs_cod_tipo_causa, long fl_num_sequenza_corrente, boolean fb_chiusura)
public function integer wf_loaddddw_dw (datawindow dddw_dw, string dddw_column, transaction trans_object, string table_name, string column_code, string column_desc, string join_clause, string where_clause)
public function integer wf_responsabile (string fs_cod_tipo_causa, long fl_num_sequenza_corrente, boolean fb_chiusura)
public function datetime wf_seleziona_data_chiusura ()
public function string wf_nc_o_reclamo (string as_tipo_causa)
public subroutine wf_treeview_icons ()
public function integer wf_inserisci_prodotti (long al_handle)
public function integer wf_inserisci_data_creazione (long al_handle)
public function integer wf_inserisci_data_scadenza (long al_handle)
public function integer wf_inserisci_tipi_cause (long al_handle)
public function integer wf_inserisci_cause (long al_handle)
public function long wf_inserisci_nc (long al_handle, long al_anno_non_conf, long al_num_non_conf)
public subroutine wf_valori_livelli ()
public function integer wf_nome_cognome ()
public subroutine wf_ddlb_tipo_nc ()
public function string wf_stato_documento (integer fi_anno_non_conf, long fl_num_non_conf)
public function string wf_label_aggiuntiva (integer fi_anno_non_conf, long fl_num_non_conf)
end prototypes

event pc_menu_comunicazioni();long   ll_numero, ll_anno, ll_row

ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row < 1 then return

ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_non_conf")
ll_numero = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_non_conf")

setnull(s_cs_xx.parametri.parametro_ul_1)
setnull(s_cs_xx.parametri.parametro_ul_2)
setnull(s_cs_xx.parametri.parametro_ul_3)
setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)

s_cs_xx.parametri.parametro_d_1 = ll_anno
s_cs_xx.parametri.parametro_d_2 = ll_numero

window_open(w_schede_intervento_comunicazioni, 0)

end event

event pc_menu_respingi();string ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi, ls_cod_area_aziendale, ls_cod_prodotto, ls_cod_cat_mer
string ls_firma_chiusura
long   ll_riga, ll_anno, ll_num, ll_num_sequenza_corrente, li_ret

ll_riga = tab_dettaglio.det_1.dw_1.getrow()

if ll_riga < 1 then return

ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "anno_non_conf")
ll_num = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "num_non_conf")
ls_cod_tipo_causa = tab_dettaglio.det_1.dw_1.getitemstring( ll_riga, "cod_tipo_causa")
			
//Donato 26-01-2009 verifica se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	//questi dati li prendiamo direttamente dalla tabella
	ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "anno_non_conf")
	ll_num = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "num_non_conf")
	
	select num_sequenza_corrente,
			cod_tipo_causa,
			firma_chiusura
	into
		:ll_num_sequenza_corrente,
		:ls_cod_tipo_causa,
		:ls_firma_chiusura
	from non_conformita
	where cod_azienda = :s_cs_xx.cod_azienda
		and anno_non_conf = :ll_anno and num_non_conf = :ll_num;
	//---------------------
	
	if sqlca.sqlcode <> 0 then
		return
	end if
	
	if not isnull(ls_firma_chiusura) and ls_firma_chiusura <> "" then
		//Conclusa: impossibile respingere
		return
	else
		if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 &
							and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)		
		else
			//inizio: non esistono fasi precedenti
			li_ret = 2
		end if
	end if
	
	if li_ret = 1 then
		//responsabile OK			
	elseif li_ret = 0 then
		//non responsabile
		g_mb.messagebox( "OMNIA", "Non Autorizzato!",Exclamation!)	
		return
	elseif  li_ret = 2 then
		//non esistono fasi precedenti
		return
	else
		//qualcosa non quadra e c'è stato un errore (msg già dato)
		return
	end if
	//fine modifica -------------------------------------------------------------	
end if
			
if not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
				
	select cod_tipo_lista_dist,
	       cod_lista_dist
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_cause
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_causa = :ls_cod_tipo_causa;
						 
	if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
					
		select flag_gestione_fasi
		into   :ls_flag_gestione_fasi
		from   tab_tipi_liste_dist
		where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
					
		if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
		
		if ls_flag_gestione_fasi <> "S" then return
		
		select cod_area_aziendale,
		       cod_prodotto,
				 num_sequenza_corrente
		into   :ls_cod_area_aziendale,
		       :ls_cod_prodotto,
				 :ll_num_sequenza_corrente
		from   non_conformita 
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_non_conf = :ll_anno and
				 num_non_conf = :ll_num;
		
		select cod_cat_mer
		into   :ls_cod_cat_mer
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
				 
		if isnull(ls_cod_cat_mer) then ls_cod_cat_mer = ""
		
		if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""
					
		s_cs_xx.parametri.parametro_s_1 = "L"
		s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
		s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
		s_cs_xx.parametri.parametro_s_4 = ""
		s_cs_xx.parametri.parametro_s_5 = "E' stata modificata la Non Conformità " + string(ll_anno) + "/" + string(ll_num)
		s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno) + "/" + string(ll_num)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari autorizzati?"
		s_cs_xx.parametri.parametro_s_8 = ""
		s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
		s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer
		s_cs_xx.parametri.parametro_s_11 = ""
		s_cs_xx.parametri.parametro_s_12 = ""
		s_cs_xx.parametri.parametro_s_13 = string(ll_anno)
		s_cs_xx.parametri.parametro_s_14 = string(ll_num)
		s_cs_xx.parametri.parametro_s_15 = "N"
		s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
		s_cs_xx.parametri.parametro_b_1 = false
							
		openwithparm( w_invio_messaggi, 0)
		
		if not isnull(s_cs_xx.parametri.parametro_ul_1) then
			
			if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
			
			update non_conformita
			set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_non_conf = :ll_anno and
					 num_non_conf = :ll_num;
					 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			commit;
			
			triggerevent("pc_retrieve")
			
			tab_dettaglio.det_1.dw_1.setrow( ll_riga)					 
			
		end if		
		
	elseif sqlca.sqlcode < 0 then				
		g_mb.messagebox( "OMNIA", "Errore durante la ricerca della lista di distribuzione: " + sqlca.sqlerrtext)						
		return	
	else		
		g_mb.messagebox( "OMNIA", "Attenzione: controllare l'associazione tipo causa / tipo lista!")
		return
	end if					
end if				

end event

event pc_menu_invia();string ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi, ls_cod_area_aziendale, ls_cod_prodotto, &
       ls_cod_cat_mer, ls_firma_chiusura
integer li_ret
long   ll_riga, ll_anno, ll_num, ll_num_sequenza_corrente

ll_riga = tab_dettaglio.det_1.dw_1.getrow()

if ll_riga < 1 then return

//Donato 25-11-2008 se l'utente non è un resp. di fase esci
ll_num_sequenza_corrente = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_sequenza_corrente")
ls_cod_tipo_causa = tab_dettaglio.det_1.dw_1.getitemstring( ll_riga, "cod_tipo_causa")
ls_firma_chiusura = tab_dettaglio.det_1.dw_1.getitemstring( ll_riga, "firma_chiusura")

//questi dati li prendiamo direttamente dalla tabella
ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "anno_non_conf")
ll_num = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "num_non_conf")

//Donato 26-01-2009 controlla se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	select num_sequenza_corrente,
			cod_tipo_causa,
			firma_chiusura
	into
		:ll_num_sequenza_corrente,
		:ls_cod_tipo_causa,
		:ls_firma_chiusura
	from non_conformita
	where cod_azienda = :s_cs_xx.cod_azienda
		and anno_non_conf = :ll_anno and num_non_conf = :ll_num;
	//---------------------
	
	if sqlca.sqlcode <> 0 then
		return
	end if
	
	if not isnull(ls_firma_chiusura) and ls_firma_chiusura <> "" then
		//Conclusa
		return
	else
		if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 &
							and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)		
		else
			//inizio: abilitare
			//li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)
			li_ret = 1
		end if
	end if
	
	if li_ret = 1 then
		//responsabile OK			
	elseif li_ret = 0 then
		//non responsabile
		g_mb.messagebox( "OMNIA", "Non Autorizzato!",Exclamation!)	
		return
	else
		//qualcosa non quadra e c'è stato un errore (msg già dato)
		return
	end if
	//fine modifica -------------------------------------------------------------
end if

ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "anno_non_conf")
ll_num = tab_dettaglio.det_1.dw_1.getitemnumber( ll_riga, "num_non_conf")
ls_cod_tipo_causa = tab_dettaglio.det_1.dw_1.getitemstring( ll_riga, "cod_tipo_causa")
			
if not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
				
	select cod_tipo_lista_dist,
	       cod_lista_dist
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_cause
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_causa = :ls_cod_tipo_causa;
						 
	if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
					
		select flag_gestione_fasi
		into   :ls_flag_gestione_fasi
		from   tab_tipi_liste_dist
		where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
					
		if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
		
		if ls_flag_gestione_fasi <> "S" then return
					
		select cod_area_aziendale,
		       cod_prodotto,
				 num_sequenza_corrente
		into   :ls_cod_area_aziendale,
		       :ls_cod_prodotto,
				 :ll_num_sequenza_corrente
		from   non_conformita 
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_non_conf = :ll_anno and
				 num_non_conf = :ll_num;
				 
		select cod_cat_mer
		into   :ls_cod_cat_mer
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
				 
		if isnull(ls_cod_cat_mer) then ls_cod_cat_mer = ""
		
		if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""		
						
		s_cs_xx.parametri.parametro_s_1 = "L"
		s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
		s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
		s_cs_xx.parametri.parametro_s_4 = ""
		s_cs_xx.parametri.parametro_s_5 = "E' stata modificata la Non conformità " + string(ll_anno) + "/" + string(ll_num)
		s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno) + "/" + string(ll_num)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari della fase successiva?"
		s_cs_xx.parametri.parametro_s_8 = ""
		s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
		s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer
		s_cs_xx.parametri.parametro_s_11 = ""
		s_cs_xx.parametri.parametro_s_12 = ""
		s_cs_xx.parametri.parametro_s_13 = string(ll_anno)
		s_cs_xx.parametri.parametro_s_14 = string(ll_num)
		s_cs_xx.parametri.parametro_s_15 = "N"
		s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
		s_cs_xx.parametri.parametro_b_1 = true
							
		openwithparm( w_invio_messaggi, 0)	
		
		if not isnull(s_cs_xx.parametri.parametro_ul_1) then
			
			if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
			
			update non_conformita
			set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_non_conf = :ll_anno and
					 num_non_conf = :ll_num;
					 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			commit;
			
			triggerevent("pc_retrieve")
			
			tab_dettaglio.det_1.dw_1.setrow( ll_riga)					 
			
		end if				
					
	elseif sqlca.sqlcode < 0 then
				
		g_mb.messagebox( "OMNIA", "Errore durante la ricerca della lista di distribuzione: " + sqlca.sqlerrtext)						
		return
		
	else
		
		g_mb.messagebox( "OMNIA", "Attenzione: controllare l'associazione tipo causa / tipo lista!")
		return
		
	end if
					
end if				
end event

event pc_menu_chiudi();string   ls_cod_resp, ls_difetto, ls_tipo_lista, ls_cod_lista, ls_cod_tipo_causa, ls_flag_chiusura_nc, ls_nome_cognome

long     ll_anno, ll_num, ll_row, ll_num_sequenza_corrente, ll_sequenza_destinatari

datetime ldt_chiusura

//Abilitare la chiusura dal pulsante solo se siamo nell'ultima fase

//questi dati li prendiamo direttamente dalla tabella
ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_non_conf")
ll_num = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_non_conf")

select num_sequenza_corrente,
		cod_tipo_causa,
		cod_errore,
		firma_chiusura,
		data_chiusura
into
	:ll_num_sequenza_corrente,
	:ls_cod_tipo_causa,
	:ls_difetto,
	:ls_cod_resp,
	:ldt_chiusura
from non_conformita
where cod_azienda = :s_cs_xx.cod_azienda
	and anno_non_conf = :ll_anno and num_non_conf = :ll_num;
	
if not isnull(ls_cod_resp) and not isnull(ldt_chiusura) then	
	g_mb.messagebox("OMNIA","Questa non conformità è già stata chiusa in data " + string(ldt_chiusura) + " dal mansionario " + ls_cod_resp)
	return
end if

//Donato 12-01-2009 prima procedi per tipo causa, altrimenti vai per difetto
if not isnull(ls_cod_tipo_causa) or ls_cod_tipo_causa <> "" then
	select cod_tipo_lista_dist,
			 cod_lista_dist
	into   :ls_tipo_lista,
			 :ls_cod_lista
	from   tab_tipi_cause
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_causa = :ls_cod_tipo_causa;
else
	select cod_tipo_lista_dist,
			 cod_lista_dist
	into   :ls_tipo_lista,
			 :ls_cod_lista
	from   tab_difformita
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_errore = :ls_difetto;
end if
//fine modifica ------------------------------------------------------------------------------

select num_sequenza
into   :ll_sequenza_destinatari
from   det_liste_dist
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_lista_dist = :ls_tipo_lista and
		 cod_lista_dist = :ls_cod_lista and
		 num_sequenza_prec = :ll_num_sequenza_corrente;

ll_num_sequenza_corrente = ll_sequenza_destinatari
setnull(ll_sequenza_destinatari)

//Donato 26-01-2009 verifica se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	//vedo se ci sono fasi successive
	select num_sequenza
	into   :ll_sequenza_destinatari
	from   det_liste_dist
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_lista_dist = :ls_tipo_lista and
			 cod_lista_dist = :ls_cod_lista and
			 num_sequenza_prec = :ll_num_sequenza_corrente;
			 
	if isnull(ll_sequenza_destinatari) or ll_sequenza_destinatari = 0 then
		// in questo caso sono nell'ultima fase
	else
		messagebox( "OMNIA", "Impossibile procedere alla chiusura del documento: ci sono altre fasi nel ciclo di vita!")
		return
	end if
	//---------------------
end if

if g_mb.messagebox("OMNIA","Chiudere la non conformità?",question!,yesno!,2) = 2 then
	return
end if

setnull(ls_cod_resp)

//15/12/2011 inizio modifica Giulio:

//select cod_resp_divisione
//into   :ls_cod_resp
//from   mansionari
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_utente = :s_cs_xx.cod_utente and
//		 flag_chiusura_nc = 'S';

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_chiusura_nc = 'N'

//if luo_mansionario.uof_get_privilege(luo_mansionario.chiusura_nc) then ls_flag_chiusura_nc = 'S'
if not luo_mansionario.uof_get_privilege(luo_mansionario.chiusura_nc) then
	g_mb.error("OMNIA","Utente non autorizzato alla chiusura NC!")
	return 
end if

//leggi cod mansionario dell'utente e sua firma (nome e cognome dell'utente)
ls_cod_resp = luo_mansionario.uof_get_cod_mansionario()
ls_nome_cognome = luo_mansionario.uof_get_firma()

if isnull(ls_cod_resp) or ls_cod_resp="" then
	g_mb.messagebox("OMNIA","Nessun Mansionario non associato all'utente!")
	return 
end if

//if isnull (luo_mansionario.uof_get_cod_mansionario())then
//	g_mb.messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
//	return 
//end if

//fine modifica Giulio

ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_non_conf")
ll_num = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_non_conf")

//sintexcal ha chiesto di poter scegliere la data chiusura della NC
ldt_chiusura = wf_seleziona_data_chiusura()


update non_conformita
set    firma_chiusura = :ls_cod_resp,
        data_chiusura = :ldt_chiusura,
	   nome_cognome = :ls_nome_cognome
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_non_conf = :ll_anno and
		 num_non_conf = :ll_num;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in chiusura non_conformità.~nErrore nella update di non_conformita: " + sqlca.sqlerrtext)
	return
end if

ll_row = tab_dettaglio.det_1.dw_1.getrow()

tab_dettaglio.det_1.dw_1.triggerevent("pcd_retrieve")

tab_dettaglio.det_1.dw_1.setrow(ll_row)

tab_dettaglio.det_1.dw_1.scrolltorow(ll_row)

if s_cs_xx.num_livello_mail > 0 then

	ls_difetto = tab_dettaglio.det_1.dw_1.getitemstring(ll_row,"cod_errore")
		
	select cod_tipo_lista_dist,
			 cod_lista_dist
	into   :ls_tipo_lista,
			 :ls_cod_lista
	from   tab_difformita
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_errore = :ls_difetto;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in lettura lista distribuzione da tab_difformita: " + sqlca.sqlerrtext)
		return
	elseif sqlca.sqlcode = 100 then
		return
	end if
	
	s_cs_xx.parametri.parametro_s_1 = "L"
	s_cs_xx.parametri.parametro_s_2 = ls_tipo_lista
	s_cs_xx.parametri.parametro_s_3 = ls_cod_lista
	s_cs_xx.parametri.parametro_s_4 = ""
	s_cs_xx.parametri.parametro_s_5 = "Chiusura Non Conformità " + string(ll_anno) + "/" + string(ll_num)
	s_cs_xx.parametri.parametro_s_6 = "E' stata chiusa la Non Conformità " + string(ll_anno) + "/" + string(ll_num)
	s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
	s_cs_xx.parametri.parametro_s_8 = ""
	
	openwithparm(w_invio_messaggi,0)

end if
end event

event pc_menu_stock();//se l'utente non è un resp. di fase esci
long ll_num_sequenza_corrente, ll_riga, ll_anno_nc, ll_num_nc
integer li_ret
string ls_cod_tipo_causa, ls_firma_chiusura, ls_cod_prodotto

ll_riga = tab_dettaglio.det_1.dw_1.getrow()
if ll_riga <=0 then return

ll_num_sequenza_corrente = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_sequenza_corrente")
ls_cod_tipo_causa = tab_dettaglio.det_1.dw_1.getitemstring( ll_riga, "cod_tipo_causa")
ls_firma_chiusura = tab_dettaglio.det_1.dw_1.getitemstring( ll_riga, "firma_chiusura")

//Donato 29/01/2009 verifica se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	if not isnull(ls_firma_chiusura) and ls_firma_chiusura <> "" then
		//Conclusa
		if s_cs_xx.cod_utente = "CS_SYSTEM" then
			li_ret = 1
		else
			return
		end if
	else
		
		//controlla se sei all'inizio --------
		//NUOVO CODICE
		if ll_num_sequenza_corrente= 0 or isnull(ll_num_sequenza_corrente) then
			//si tratta della prima fase: fai continuare
			li_ret = 1
		else
			//fai quello che faceva prima:
			//serve che si tratti del responsabile
			if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 &
								and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
				li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)		
			else
				//inizio
				li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)
			end if
		end if
		
		//vecchio codice
		/*
		if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 &
							and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)		
		else
			//inizio
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)
		end if
		*/
		//fine modifica -------------------------------------------------
	end if
	
	if li_ret = 1 then
		//responsabile OK			
	elseif li_ret = 0 then
		//non responsabile
		g_mb.messagebox( "OMNIA", "Non Autorizzato!",Exclamation!)	
		return
	else
		//qualcosa non quadra e c'è stato un errore (msg già dato)
		return
	end if
	//fine modifica -------------------------------------------------------------
end if

//controlla se esiste il prodotto
ll_anno_nc = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "anno_non_conf")
ll_num_nc = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_non_conf")

select cod_prodotto
into :ls_cod_prodotto
from non_conformita
where cod_azienda = :s_cs_xx.cod_azienda and
	anno_non_conf = :ll_anno_nc and num_non_conf = :ll_num_nc;
	
if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
	g_mb.messagebox( "OMNIA", "E' necessario selezionare il prodotto a cui è associata la N.C.!",Exclamation!)	
	return
end if

//apri la maschera di dettaglio
if not isvalid(w_non_conformita_lotti) then
   s_cs_xx.parametri.parametro_d_1 = ll_anno_nc
   s_cs_xx.parametri.parametro_d_2 = ll_num_nc
   if s_cs_xx.parametri.parametro_d_1 > 0 and s_cs_xx.parametri.parametro_d_2 > 0 then
      window_open_parm(w_non_conformita_lotti, -1, tab_dettaglio.det_1.dw_1)
   end if
end if


end event

event pc_menu_costi();long ll_row, ll_anno, ll_num


ll_row = tab_dettaglio.det_1.dw_1.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("OMNIA","Selezionare una non conformità!",stopsign!)
	return
end if

ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row,"anno_non_conf")

ll_num = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row,"num_non_conf")

if isnull(ll_anno) or ll_anno = 0 or isnull(ll_num) or ll_num = 0 then
	g_mb.messagebox("OMNIA","Selezionare una non conformità!",stopsign!)
	return
end if

window_open_parm(w_nc_costi,-1,tab_dettaglio.det_1.dw_1)
end event

event pc_menu_report();string ls_tipo_report, ls_tipo_causa

if not isvalid(w_report_nc) then
	
	if tab_dettaglio.det_1.dw_1.getrow() < 1 then
		g_mb.messagebox("OMNIA","Selezionare una non conformità dalla lista",stopsign!)
		return
	end if

	s_cs_xx.parametri.parametro_d_1 = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_non_conf")
	s_cs_xx.parametri.parametro_d_2 = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_non_conf")
	
	//Donato 26-01-2009 controlla se è abilitato il blocco dei campi
	if is_flag_BNC = "S" then
		//prima di aprire il report verifica se si tratta di una NC o di un reclamo
		ls_tipo_causa = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"cod_tipo_causa")
		if isnull(ls_tipo_causa) or ls_tipo_causa = "" then
			g_mb.messagebox("OMNIA", "Impossibile stabilire il tipo di report: campo Tipo Causa non specificato!")			
			return
		end if
		
		ls_tipo_report = wf_nc_o_reclamo(ls_tipo_causa)
		choose case ls_tipo_report
			case "N"
				window_open(w_report_nc2,-1)
				
			case "R"
				window_open(w_report_nc_reclami,-1)
				
		end choose
	else
		//report classico
		window_open(w_report_nc,-1)
	end if
end if
end event

event pc_menu_dettagli();//Donato 25-11-2008 se l'utente non è un resp. di fase esci
long ll_num_sequenza_corrente, ll_riga
integer li_ret
string ls_cod_tipo_causa, ls_firma_chiusura, ls_flag_tipo_nc

ll_riga = tab_dettaglio.det_1.dw_1.getrow()
if ll_riga <=0 then return

ll_num_sequenza_corrente = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_sequenza_corrente")
ls_cod_tipo_causa = tab_dettaglio.det_1.dw_1.getitemstring( ll_riga, "cod_tipo_causa")
ls_firma_chiusura = tab_dettaglio.det_1.dw_1.getitemstring( ll_riga, "firma_chiusura")

//Donato 29/01/2009 verifica se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	if not isnull(ls_firma_chiusura) and ls_firma_chiusura <> "" then
		//Conclusa
		if s_cs_xx.cod_utente = "CS_SYSTEM" then
			li_ret = 1
		else
			return
		end if
	else
		if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 &
							and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)		
		else
			//inizio
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)
		end if
	end if
	
	if li_ret = 1 then
		//responsabile OK			
	elseif li_ret = 0 then
		//non responsabile
		g_mb.messagebox( "OMNIA", "Non Autorizzato!",Exclamation!)	
		return
	else
		//qualcosa non quadra e c'è stato un errore (msg già dato)
		return
	end if
	//fine modifica -------------------------------------------------------------
end if

 s_cs_xx.parametri.parametro_d_1 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "anno_non_conf")
 s_cs_xx.parametri.parametro_d_2 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_non_conf")

if s_cs_xx.parametri.parametro_d_1 > 0 and s_cs_xx.parametri.parametro_d_2 > 0 then
else
	return
end if

ls_flag_tipo_nc =  tab_dettaglio.det_1.dw_1.getitemstring( ll_riga, "flag_tipo_nc")

choose case ls_flag_tipo_nc
	case "P"	//progetto
		if not isvalid(w_non_conformita_progetti) then
			window_open(w_non_conformita_progetti, -1)
		end if
		
	case "I"	//interna
		if not isvalid(w_non_conformita_interna) then
			window_open(w_non_conformita_interna, -1)
		end if
		
	case "V"	//vendita
		if not isvalid(w_non_conformita_vendite) then
			window_open(w_non_conformita_vendite, -1)
		end if
		
	case "S"	//sistema
		if not isvalid(w_non_conformita_sistema) then
			window_open(w_non_conformita_sistema, -1)
		end if
		
	case "A"	//approvigionamento
		if not isvalid(w_non_conformita_approvv) then
			window_open(w_non_conformita_approvv, -1)
		end if
		
end choose





end event

event pc_menu_documenti();long			ll_riga

ll_riga = tab_dettaglio.det_1.dw_1.getrow()
if ll_riga <=0 then return

s_cs_xx.parametri.parametro_ul_1 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "anno_non_conf")
s_cs_xx.parametri.parametro_ul_2 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_non_conf")

//s_cs_xx.parametri.parametro_d_1 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "anno_non_conf")
//s_cs_xx.parametri.parametro_d_2 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_non_conf")

//if s_cs_xx.parametri.parametro_d_1 > 0 and s_cs_xx.parametri.parametro_d_2 > 0 then
if s_cs_xx.parametri.parametro_ul_1 > 0 and s_cs_xx.parametri.parametro_ul_1 > 0 then
else
	return
end if

open(w_non_conformita_ole)
end event

event ue_carica_area_aziendale();string ls_cod_area_aziendale, ls_cod_resp, ls_flag_supervisore

//caricare/proporre nel filtro l'area aziendale di appartenenza del mansionario collegato all'utente
//se l'utente è supervisore allora non proporre alcuna area aziendale nel filtro

if is_flag_nc_abcd = "1" then
	//caso sintexcal: filtrare le NC per area aziendale e bloccare il campo
	
	if s_cs_xx.cod_utente="CS_SYSTEM" then
		//non proporre l'area aziendale
		return
		
	else
		select flag_supervisore
		into :ls_flag_supervisore
		from utenti
		where cod_azienda=:s_cs_xx.cod_azienda and
				cod_utente=:s_cs_xx.cod_utente;
	end if
	
	if ls_flag_supervisore="S" then return   //non proporre l'area aziendale
	
	
	//carica area aziendale dell'utente (solo proposta)
	//---------------------------------------------------------------------------------------
	ls_cod_resp = iuo_mansionario.uof_get_cod_mansionario()
	
	select cod_area_aziendale
	into :ls_cod_area_aziendale
	from mansionari
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_resp_divisione=:ls_cod_resp;
			
	if sqlca.sqlcode<0 then
		g_mb.error("Errore in lettura area aziendale del mansionario: "+sqlca.sqlerrtext)
		return
	end if
	
	if ls_cod_area_aziendale<>"" and not isnull(ls_cod_area_aziendale) then
		//carica filtro per area aziendale
		tab_ricerca.ricerca.dw_ricerca.setitem(tab_ricerca.ricerca.dw_ricerca.getrow(), "cod_area_aziendale", ls_cod_area_aziendale)
		
		//blocca filtro per area aziendale
		//tab_ricerca.ricerca.dw_ricerca.settaborder("cod_area_aziendale", 0)
		//tab_ricerca.ricerca.dw_ricerca.object.b_ricerca_area_aziendale.enabled = false
	end if
	//---------------------------------------------------------------------------------------
	
else
	return
end if
end event

event pc_menu_gen_richiesta();long			ll_riga, ll_num_nc
integer		li_anno_nc, li_count

ll_riga = tab_dettaglio.det_1.dw_1.getrow()
if ll_riga <=0 then return

li_anno_nc = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "anno_non_conf")
ll_num_nc = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_non_conf")

select		count(*)
into		:li_count
from   non_conformita  
where			cod_azienda   = :s_cs_xx.cod_azienda and
				anno_non_conf = :li_anno_nc and
				num_non_conf  = :ll_num_nc ;

if sqlca.sqlcode = 0 and li_count>0 then
	if g_mb.confirm("Omnia","Predisporre una nuova richiesta a partire dalla Non Conformità "+string(li_anno_nc) + "/" + string(ll_num_nc) +" ?") then
	
		s_cs_xx.parametri.parametro_s_1 = "GEN_RICHIESTA"
		s_cs_xx.parametri.parametro_ul_1 = li_anno_nc
		s_cs_xx.parametri.parametro_ul_2 = ll_num_nc
		
		window_open(w_call_center_tv, -1)
	end if
end if
end event

public subroutine wf_imposta_ricerca ();long ll_anno_non_conf, ll_num_non_conf

is_sql_filtro = ""
ll_anno_non_conf = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "anno_registrazione")
ll_num_non_conf = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "num_registrazione")

if not isnull(ll_anno_non_conf) and ll_anno_non_conf > 1900 then
	is_sql_filtro += " AND non_conformita.anno_non_conf=" + string(ll_anno_non_conf)
end if
if not isnull(ll_num_non_conf) and ll_num_non_conf>0 then
	is_sql_filtro += " AND non_conformita.num_non_conf=" + string(ll_num_non_conf)
end if
if not isnull(ll_anno_non_conf) and not isnull(ll_num_non_conf) then
	// mi fermo non serve altro, filtro solo la singola NC
	return
end if


if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")) then
		is_sql_filtro += " AND non_conformita.cod_prodotto='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_cliente")) then
		is_sql_filtro += " AND non_conformita.cod_cliente='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_cliente") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"tipo_causa")) then
		is_sql_filtro += " AND non_conformita.cod_tipo_causa='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"tipo_causa") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"causa")) then
		is_sql_filtro += " AND non_conformita.cod_causa='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"causa") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_errore")) then
		is_sql_filtro += " AND non_conformita.cod_errore='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_errore") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"trattamento")) then
		is_sql_filtro += " AND non_conformita.cod_trattamento='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"trattamento") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_area_aziendale")) then
		is_sql_filtro += " AND non_conformita.cod_area_aziendale='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_area_aziendale") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_tipo_nc")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_tipo_nc") <> "" then
		is_sql_filtro += " AND non_conformita.flag_tipo_nc='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_tipo_nc") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_nc")) then
		is_sql_filtro += " AND non_conformita.flag_nc='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_nc") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_provenienza_nc")) then
		is_sql_filtro += " AND non_conformita.flag_provenienza_nc='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_provenienza_nc") +"'"
end if

end subroutine

public function integer wf_inserisci_tipo_nc (long al_handle);string ls_tipi_nc[]
integer ll_count, ll_i
treeviewitem ltvi_item

for ll_i = 1 to upperbound(is_cod_tipi_nc)
	
	str_treeview lstr_data
	
	lstr_data.tipo_livello = "T"
	lstr_data.livello = il_livello
	lstr_data.codice = is_cod_tipi_nc[ll_i]

	ltvi_item = wf_new_item(true, ICONA_TIPO_NC)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = is_des_tipi_nc[ll_i]
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
	
next

setnull(ids_store)
return  upperbound(is_cod_tipi_nc)
end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "T"
		return wf_inserisci_tipo_nc(al_handle)
		
	case "P"
		return wf_inserisci_prodotti(al_handle)
		
	case "A"
		return wf_inserisci_data_creazione(al_handle)
		
	case "S"
		return wf_inserisci_data_scadenza(al_handle)
		
	case "Z"
		return wf_inserisci_tipi_cause(al_handle)
		
	case "B"
		return wf_inserisci_cause(al_handle)
		
	case else
		return wf_inserisci_nc(al_handle)
		
end choose
end function

public function integer wf_inserisci_nc (long al_handle);string ls_sql, ls_error
integer ll_count, ll_i
treeviewitem ltvi_item

ls_sql = "SELECT anno_non_conf, num_non_conf FROM non_conformita WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' "
ls_sql += is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

//order by anno e numero registrazione
ls_sql += " order by anno_non_conf desc, num_non_conf desc "

ll_count = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_count < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_count = 0 then
	return 0
end if

for ll_i = 1 to ll_count
	
	if ll_i = ii_max_treeview_result then 
		g_mb.warning("Attenzione superato il numero masismo di elementi. Inserire una ricerca più restrittiva.")
		exit
	end if
	
	wf_inserisci_nc(al_handle, ids_store.getitemnumber(ll_i, 1), ids_store.getitemnumber(ll_i, 2))

next

setnull(ids_store)
return ll_count
end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
str_treeview lstr_data


if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello		
			
		case "T"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND non_conformita.flag_tipo_nc is null "
			else
				as_sql += " AND non_conformita.flag_tipo_nc = '" + lstr_data.codice + "' "
			end if
			
		case "P"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND non_conformita.cod_prodotto is null "
			else
				as_sql += " AND non_conformita.cod_prodotto = '" + lstr_data.codice + "' "
			end if
			
		case "A"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND non_conformita.data_creazione is null "
			else
				as_sql += " AND non_conformita.data_creazione = '" + lstr_data.codice + "' "
			end if
			
		case "S"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND non_conformita.data_scadenza is null "
			else
				as_sql += " AND non_conformita.data_scadenza = '" + lstr_data.codice + "' "
			end if
			
		case "Z"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND non_conformita.cod_tipo_causa is null "
			else
				as_sql += " AND non_conformita.cod_tipo_causa = '" + lstr_data.codice + "' "
			end if
			
		case "B"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND non_conformita.cod_causa is null "
			else
				as_sql += " AND non_conformita.cod_causa = '" + lstr_data.codice + "' "
			end if
			
			
						
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public subroutine wf_proteggi_documento (string fs_cod_tipo_causa, long fl_num_sequenza_corrente, boolean fb_chiusura);string ls_cod_tipo_lista_dist, ls_flag_gestione_fasi, ls_cod_lista_dist, ls_des_fase, ls_cod_destinatario
string ls_sql, ls_sintassi, ls_errore, ls_count, ls_column_name, ls_col_index, ls_protect, ls_ret
datastore lds_campi
long ll_righe, ll_sequenza_destinatari, ll_num_sequenza_corrente
integer li_index
string ls_colonna, ls_flag_modificabile, ls_flag_responsabile

ll_num_sequenza_corrente = fl_num_sequenza_corrente
if isnull(ll_num_sequenza_corrente) then ll_num_sequenza_corrente = 0

select cod_tipo_lista_dist,
		 cod_lista_dist
into   :ls_cod_tipo_lista_dist,
		 :ls_cod_lista_dist
from   tab_tipi_cause 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_causa = :fs_cod_tipo_causa;
	 
select flag_gestione_fasi
into   :ls_flag_gestione_fasi
from   tab_tipi_liste_dist
where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
		 
//numero di colonne nella dw di dettaglio
ls_count = tab_dettaglio.det_1.dw_1.Describe("DataWindow.Column.Count")
		 
select stringa
into   :ls_cod_destinatario
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'PCU';

//l'utente collegato è un responsabile?
//se no allora tutti i campi devono essere protetti
//se CS_SYSTEM allora tutti i campi sprotetti
//se responsabile allora dipende dal flag_modificabile

select num_sequenza
into   :ll_sequenza_destinatari
from   det_liste_dist
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
		 cod_lista_dist = :ls_cod_lista_dist and
		 (num_sequenza_prec = :ll_num_sequenza_corrente
		 	or num_sequenza_prec is null);

//ciclo su tutte le colonne delle dw per eventualmente proteggere
for li_index = 1 to integer(ls_count)
	ls_col_index = "#"+string(li_index)
	ls_column_name =tab_dettaglio.det_1.dw_1.Describe(ls_col_index+".name")
	
	if s_cs_xx.cod_utente = "CS_SYSTEM" or ls_flag_gestione_fasi <> "S" then
		ls_protect = "0"
	elseif fb_chiusura then
		ls_protect = "1"
	else
		//se si tratta della prima fase (ll_num_sequenza_corrente zero o null)
		//devi controllare solo se i campi sono modificabili
		//altrimenti serve pure che l'utente sia responsabile della fase
		if ll_num_sequenza_corrente= 0 or isnull(ll_num_sequenza_corrente) then
			//si tratta della prima fase
			select distinct det_liste_dist_campi_nc.colonna
					,det_liste_dist_campi_nc.flag_modificabile					
			into    :ls_colonna
					,:ls_flag_modificabile					
			from det_liste_dist_campi_nc
			join det_liste_dist_destinatari on det_liste_dist_destinatari.cod_azienda=det_liste_dist_campi_nc.cod_azienda
					and det_liste_dist_destinatari.cod_tipo_lista_dist=det_liste_dist_campi_nc.cod_tipo_lista_dist
					and det_liste_dist_destinatari.cod_lista_dist=det_liste_dist_campi_nc.cod_lista_dist
					and det_liste_dist_destinatari.cod_destinatario=det_liste_dist_campi_nc.cod_destinatario
					and det_liste_dist_destinatari.num_sequenza=det_liste_dist_campi_nc.num_sequenza
			join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
					and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
			where det_liste_dist_campi_nc.cod_azienda=:s_cs_xx.cod_azienda
					and det_liste_dist_campi_nc.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
					and det_liste_dist_campi_nc.cod_lista_dist=:ls_cod_lista_dist
					and det_liste_dist_campi_nc.cod_destinatario=:ls_cod_destinatario
					and det_liste_dist_campi_nc.num_sequenza=:ll_sequenza_destinatari//:fl_num_sequenza_corrente
					and det_liste_dist_campi_nc.colonna=:ls_column_name
					//and tab_ind_dest.cod_utente = :s_cs_xx.cod_utente
			;
			if sqlca.sqlcode < 0 then
				g_mb.messagebox( "OMNIA", "Errore durante la lettura delle informazioni "+&
									"per la colonna '"+ls_column_name+"'")
				destroy lds_campi;
				return 
			end if
			
			if sqlca.sqlcode = 0 then
				//la protezione del campo dipende dalla variabile ls_flag_modificabile
				if ls_flag_modificabile = "S" then
					ls_protect = "0"
				else
					ls_protect = "1"
				end if
			else
				//valore non letto
				ls_protect = "1"
			end if	
		//-------------------------------------
		else
			//ciò che discrimina è il responsabile di fase
			//leggi info della colonna dalla tabella det_liste_dist_campi_nc
			select det_liste_dist_campi_nc.colonna
					,det_liste_dist_campi_nc.flag_modificabile
					,det_liste_dist_destinatari.flag_responsabile
			into    :ls_colonna
					,:ls_flag_modificabile
					,:ls_flag_responsabile
			from det_liste_dist_campi_nc
			join det_liste_dist_destinatari on det_liste_dist_destinatari.cod_azienda=det_liste_dist_campi_nc.cod_azienda
					and det_liste_dist_destinatari.cod_tipo_lista_dist=det_liste_dist_campi_nc.cod_tipo_lista_dist
					and det_liste_dist_destinatari.cod_lista_dist=det_liste_dist_campi_nc.cod_lista_dist
					and det_liste_dist_destinatari.cod_destinatario=det_liste_dist_campi_nc.cod_destinatario
					and det_liste_dist_destinatari.num_sequenza=det_liste_dist_campi_nc.num_sequenza
			join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
					and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
			where det_liste_dist_campi_nc.cod_azienda=:s_cs_xx.cod_azienda
					and det_liste_dist_campi_nc.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
					and det_liste_dist_campi_nc.cod_lista_dist=:ls_cod_lista_dist
					and det_liste_dist_campi_nc.cod_destinatario=:ls_cod_destinatario
					and det_liste_dist_campi_nc.num_sequenza=:ll_sequenza_destinatari//:fl_num_sequenza_corrente
					and det_liste_dist_campi_nc.colonna=:ls_column_name
					and tab_ind_dest.cod_utente = :s_cs_xx.cod_utente
			;
			if sqlca.sqlcode < 0 then
				g_mb.messagebox( "OMNIA", "Errore durante la lettura delle informazioni "+&
									"per la colonna '"+ls_column_name+"'")
				destroy lds_campi;
				return 
			end if
			
			if sqlca.sqlcode = 0 and ls_flag_responsabile="S" then
				//la protezione del campo dipende dalla variabile ls_flag_modificabile
				if ls_flag_modificabile = "S" then
					ls_protect = "0"
				else
					ls_protect = "1"
				end if
			else
				//non si tratta di responsabile
				ls_protect = "1"
			end if	
		end if			
	end if
	
	//QUESTA MODIFICA SE LA DEVONO SUDARE!!!!!!!
	//vedi anche evento pcd_new di dw_non_conformita_lista
	
	//il campo responsabile Iter NC deve essere protetto a prescindere
	if ls_column_name = "cod_operaio" then ls_protect = "1"
	//-------------
	
	ls_ret = tab_dettaglio.det_1.dw_1.modify(ls_column_name+'.protect='+ls_protect)
	ls_ret = tab_dettaglio.det_2.dw_2.modify(ls_column_name+'.protect='+ls_protect)
	ls_ret = tab_dettaglio.det_3.dw_3.modify(ls_column_name+'.protect='+ls_protect)
	ls_ret = tab_dettaglio.det_4.dw_4.modify(ls_column_name+'.protect='+ls_protect)

next

end subroutine

public function integer wf_loaddddw_dw (datawindow dddw_dw, string dddw_column, transaction trans_object, string table_name, string column_code, string column_desc, string join_clause, string where_clause);INTEGER          l_ColNbr,     l_Idx,      l_Jdx
INTEGER          l_ColPos,     l_Start,    l_Count
LONG             l_Error
STRING           l_SQLString,  l_ErrStr
STRING           l_dwDescribe, l_dwModify, l_Attrib, l_ColStr
DATAWINDOWCHILD  l_DWC

STRING           c_ColAttrib[]  =            &
                    { ".BackGround.Color",   & 
                      ".Background.Mode",    &
                      ".Border",             &
                      ".Color",              &
                      ".Font.CharSet",       &
                      ".Font.Face",          &
                      ".Font.Family",        &
                      ".Font.Height",        &
                      ".Font.Italic",        &
                      ".Font.Pitch",         &
                      ".Font.Strikethrough", &
                      ".Font.Underline",     &
                      ".Font.Weight",        &
                      ".Font.Width",         &
                      ".Height",             &
                      ".Width" }

//------------------------------------------------------------------
//  Get the number and name of the DDDW column.
//------------------------------------------------------------------

l_dwDescribe = DDDW_Column + ".ID"
l_ColNbr     = Integer(DDDW_DW.Describe(l_dwDescribe))
l_dwDescribe = "#" + String(l_ColNbr) + ".Name"
l_ColStr     = DDDW_DW.Describe(l_dwDescribe)

//****** Modifica Michele per far funzionare la select di cognome e nome anche su Oracle - 21/09/2001 *******

long   ll_return, ll_pos
string ls_db

if ll_return = -1 then
	g_mb.messagebox("f_po_loaddddw_dw","Errore nella lettura del profilo corrente dal registro")
	return -1
end if

ll_return = Registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "enginetype", ls_db)

if ll_return = -1 then
	g_mb.messagebox("f_po_loaddddw_dw","Errore nella lettura del motore database dal registro")
	return -1
end if

if ls_db = "ORACLE" then
	
	do
		
		ll_pos = pos(Column_Desc,"+",1)
		
		if ll_pos > 0 then
			Column_Desc = replace(Column_Desc,ll_pos,1,"||")
		end if	
	
	loop while ll_pos > 0
	
end if

//******************************************** Fine modifica ************************************************

	
//------------------------------------------------------------------
//  Build the SQL SELECT statement.
//------------------------------------------------------------------

l_SQLString = "SELECT DISTINCT " + Column_Code + " , " + &
				  Column_Desc + " FROM " + Table_Name
				  
if Trim(join_clause) <> "" then
	l_SQLString = l_SQLString + " " + join_clause
end if

IF Trim(Where_Clause) <> "" THEN
	l_SQLString = l_SQLString + " WHERE " + Where_Clause
END IF

// ------------------------------------------------------------------
//   Aggiunto da EnMe per fare Sort su colonna codice
// ------------------------------------------------------------------

l_SQLString = l_SQLString + " ORDER BY " + Column_Code
	
//------------------------------------------------------------------
//  Get the child DataWindow.  If column is not a DDDW column,
//  then return with error.  Otherwise, we need to set the
//  Select statement in the drop down DataWindow.
//------------------------------------------------------------------

l_Error = DDDW_DW.GetChild(l_ColStr, l_DWC)

IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWChildFindError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Set the transaction object.
//------------------------------------------------------------------

l_Error = l_DWC.SetTransObject(trans_object)

IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWTransactionError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Assign the Select statement to the drop down DataWindow.
//------------------------------------------------------------------

l_dwModify = "DataWindow.Table.Select='" + &
             w_POManager_STD.MB.fu_QuoteChar(l_SQLString, "'")  + "'"
l_ErrStr   = l_DWC.Modify(l_dwModify)

IF l_ErrStr <> "" THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWModifySQLError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Retrieve the data for the child DataWindow.
//------------------------------------------------------------------

l_Error = l_DWC.Retrieve()
IF l_Error < 0 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWRetrieveError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//---------- Modifica Michele per togliere spazi dalla lista 04/03/2003 ------------
long ll_i

for ll_i = 1 to l_DWC.rowcount()
	l_DWC.setitem(ll_i,"display_column",trim(l_DWC.getitemstring(ll_i,"display_column")))
next
//------------- Fine Modifica Michele per togliere spazi dalla lista ---------------

//------------------------------------------------------------------
//  Set the attributes of the column to be the same as the
//  parent column.
//------------------------------------------------------------------

l_Jdx = UpperBound(c_ColAttrib[])
FOR l_Idx = 1 TO l_Jdx

   l_dwDescribe = l_ColStr + c_ColAttrib[l_Idx]
   l_Attrib     = DDDW_DW.Describe(l_dwDescribe)

   IF c_ColAttrib[l_Idx] = ".Font.Face" THEN
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "Arial"
      END IF
      l_Attrib = "'" + l_Attrib + "'"
   ELSE
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "0"
      END IF
   END IF

   l_dwModify = "#1" + c_ColAttrib[l_Idx] + "=" + l_Attrib
   l_ErrStr   = l_DWC.Modify(l_dwModify)

NEXT

//------------------------------------------------------------------
//  Make sure the border for the child column is off.
//------------------------------------------------------------------

l_dwModify = "#1" + ".Border=0"
l_ErrStr   = l_DWC.Modify(l_dwModify)

//------------------------------------------------------------------
//  Indicate that there not any errors.
//------------------------------------------------------------------

string ls_Required

ls_Required = dddw_dw.Describe(dddw_column + ".DDDW.Required")

IF ls_Required = "no" THEN
   l_DWC.InsertRow(1)
END IF

RETURN 0
end function

public function integer wf_responsabile (string fs_cod_tipo_causa, long fl_num_sequenza_corrente, boolean fb_chiusura);string ls_cod_tipo_lista_dist, ls_flag_gestione_fasi, ls_cod_lista_dist, ls_des_fase, ls_cod_destinatario
string ls_sql, ls_sintassi, ls_errore, ls_count, ls_column_name, ls_col_index, ls_protect, ls_ret
//datastore lds_campi
long ll_righe, ll_sequenza_destinatari, ll_num_sequenza_corrente
integer li_index
string ls_colonna, ls_flag_modificabile, ls_flag_responsabile, ls_cod_area_aziendale
long ll_anno, ll_num, ll_riga

if s_cs_xx.cod_utente = "CS_SYSTEM" then return 1

ll_num_sequenza_corrente = fl_num_sequenza_corrente
if isnull(ll_num_sequenza_corrente) then ll_num_sequenza_corrente = 0

select cod_tipo_lista_dist,
		 cod_lista_dist
into   :ls_cod_tipo_lista_dist,
		 :ls_cod_lista_dist
from   tab_tipi_cause 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_causa = :fs_cod_tipo_causa;
	 
select flag_gestione_fasi
into   :ls_flag_gestione_fasi
from   tab_tipi_liste_dist
where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
		 
if ls_flag_gestione_fasi <> "S" then return 0

select stringa
into   :ls_cod_destinatario
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'PCU';

select num_sequenza
into   :ll_sequenza_destinatari
from   det_liste_dist
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
		 cod_lista_dist = :ls_cod_lista_dist and
		 num_sequenza_prec = :ll_num_sequenza_corrente;

//---------------
ll_riga = tab_dettaglio.det_1.dw_1.getrow()
ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "anno_non_conf")
ll_num = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_non_conf")

select cod_area_aziendale
into :ls_cod_area_aziendale
from non_conformita
where cod_azienda=:s_cs_xx.cod_azienda
	and anno_non_conf=:ll_anno and num_non_conf=:ll_num;
	
if ls_cod_area_aziendale = "" then setnull(ls_cod_area_aziendale)
//------------------

if isnull(ls_cod_area_aziendale) then
	select det_liste_dist_destinatari.flag_responsabile
	into :ls_flag_responsabile
	from det_liste_dist_destinatari
	join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
			and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
	where det_liste_dist_destinatari.cod_azienda=:s_cs_xx.cod_azienda
			and det_liste_dist_destinatari.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
			and det_liste_dist_destinatari.cod_lista_dist=:ls_cod_lista_dist
			and det_liste_dist_destinatari.cod_destinatario=:ls_cod_destinatario
			and det_liste_dist_destinatari.num_sequenza=:ll_sequenza_destinatari
			and tab_ind_dest.cod_utente = :s_cs_xx.cod_utente;
else
	select det_liste_dist_destinatari.flag_responsabile
	into :ls_flag_responsabile
	from det_liste_dist_destinatari
	join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
		and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail	
	where det_liste_dist_destinatari.cod_azienda=:s_cs_xx.cod_azienda
		and det_liste_dist_destinatari.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
		and det_liste_dist_destinatari.cod_lista_dist=:ls_cod_lista_dist
		and det_liste_dist_destinatari.cod_destinatario=:ls_cod_destinatario
		and det_liste_dist_destinatari.num_sequenza=:ll_sequenza_destinatari
		and det_liste_dist_destinatari.flag_responsabile = 'S'
		and det_liste_dist_destinatari.cod_area_aziendale=:ls_cod_area_aziendale
		and tab_ind_dest.cod_utente = :s_cs_xx.cod_utente;
		
		//Donato 07/01/2009 se non hai trovato niente per area aziendale, vuol dire
		//che non è stata specificata. Allora prova a vedere se esiste un responsabile senza area
		//(N.B. deve essere unico altrimenti sarà generato un errore dal programma)
		if sqlca.sqlcode = 100 then
			select det_liste_dist_destinatari.flag_responsabile
			into :ls_flag_responsabile
			from det_liste_dist_destinatari
			join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
					and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
			where det_liste_dist_destinatari.cod_azienda=:s_cs_xx.cod_azienda
					and det_liste_dist_destinatari.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
					and det_liste_dist_destinatari.cod_lista_dist=:ls_cod_lista_dist
					and det_liste_dist_destinatari.cod_destinatario=:ls_cod_destinatario
					and det_liste_dist_destinatari.num_sequenza=:ll_sequenza_destinatari
					and tab_ind_dest.cod_utente = :s_cs_xx.cod_utente;
		end if
end if

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la lettura delle informazioni del responsabile")
	return -1
end if

if ls_flag_responsabile = "S" then
	return 1
else
	return 0
end if

end function

public function datetime wf_seleziona_data_chiusura ();//sintexcal ha chiesto di poter scegliere la data di chiusura della NC

string ls_prova
datetime ldt_chiusura

ldt_chiusura = datetime(today(),now())

//select stringa
//into   :ls_prova
//from   parametri_azienda
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_parametro = 'AZP';
//		 
//if sqlca.sqlcode = 0 and not isnull(ls_prova) then
//	//sintexcal
//	open(w_seleziona_data_chiusura)
//	
//	ldt_chiusura = s_cs_xx.parametri.parametro_data_4
//	
//	if isnull(ldt_chiusura) or year(date(ldt_chiusura))<1950 then ldt_chiusura = datetime(today(),now())
//end if

open(w_seleziona_data_chiusura)

ldt_chiusura = s_cs_xx.parametri.parametro_data_4
if isnull(ldt_chiusura) or year(date(ldt_chiusura))<1950 then ldt_chiusura = datetime(today(),now())

return ldt_chiusura
end function

public function string wf_nc_o_reclamo (string as_tipo_causa);//DONATO 30/01/2009
/*
Questa funzione elabora l'argomento "as_tipo_causa", che contiene il codice del tipo causa della NC corrente
Controlla nella parametri_azienda l'esistenza del parametro TCR (Tipo Causa Reclamo)
Il parametro contiene l'elenco dei codici dei tipi causa che servono per la gestione dei reclami, separati da una virgola
es. CD1,CD2,CD3

Se il parametro "as_tipo_causa" contiene uno di questi valori allora la funzione torna "R"
																								 altrimenti torna "N"
																								 
argomenti
	string		as_tipo_causa	(codice del tipo causa della NC corrente)

val. ritorno
	string	(R o N)
*/

string ls_prm_TCR, ls_array[], ls_separetor
integer li_index

select stringa
into :ls_prm_TCR
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'TCR';			
if sqlca.sqlcode = 0 then
else
	//il parametro non è previsto: quindi Non Conformità
	return "N"
end if	

if isnull(ls_prm_TCR) or ls_prm_TCR = "" then return "N"

ls_separetor = ","

//if guo_functions.uof_explode(ls_prm_TCR, ls_separetor, ls_array) = 0 then
guo_functions.uof_explode(ls_prm_TCR, ls_separetor, ls_array)

if  upperbound(ls_array) > 0 then
	//sfoglio l'array per vedere se c'è il codice passato
	for li_index = 1 to upperbound(ls_array)
		if ls_array[li_index] = as_tipo_causa then
			return "R"
		end if
	next
	
	//se arrivi qui vuol dire che il codice tipo causa non è stato trovato tra quelli
	//specificati dal parametro aziendale TCR per la gestione dei reclami
	return "N"
else
	//errore durante l'elaborazione della stringa
	g_mb.messagebox("OMNIA","Errore durante l'interpretazione del del valore parametro TCR", Exclamation!)
	return ""
end if
end function

public subroutine wf_treeview_icons ();
ICONA_NC = wf_treeview_add_icon("treeview\tipo_documento.png")
ICONA_DATA_SCADENZA = wf_treeview_add_icon("treeview\data_2.png")
ICONA_DATA_REGISTRAZIONE = wf_treeview_add_icon("treeview\data_1.png")
ICONA_PRODOTTO = wf_treeview_add_icon("treeview\prodotto.png")
ICONA_TIPO_NC = wf_treeview_add_icon("treeview\tag_red.png")
ICONA_TIPO_CAUSA = wf_treeview_add_icon("treeview\documento_grigio.png")
ICONA_CAUSA = wf_treeview_add_icon("treeview\operatore.png")

//nodo NC selezionato
ICONA_NC_SELEZIONATA = wf_treeview_add_icon("treeview\cartella.png")

end subroutine

public function integer wf_inserisci_prodotti (long al_handle);string ls_sql, ls_error, ls_cod_prodotto, ls_des_prodotto
integer ll_count, ll_i
treeviewitem ltvi_item

ls_sql = "SELECT DISTINCT non_conformita.cod_prodotto, anag_prodotti.des_prodotto &
FROM non_conformita LEFT JOIN anag_prodotti ON anag_prodotti.cod_azienda = non_conformita.cod_azienda AND &
anag_prodotti.cod_prodotto = non_conformita.cod_prodotto WHERE non_conformita.cod_azienda='" + s_cs_xx.cod_azienda + "' "
ls_sql += is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " order by non_conformita.cod_prodotto "

ll_count = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_count < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_count = 0 then
	return 0
end if

for ll_i = 1 to ll_count
	str_treeview lstr_data
	
	ls_cod_prodotto =  ids_store.getitemstring(ll_i, 1)
	ls_des_prodotto =  ids_store.getitemstring(ll_i, 2)
	
	lstr_data.tipo_livello = "P"
	lstr_data.livello = il_livello
	lstr_data.codice = ls_cod_prodotto
	
	ltvi_item = wf_new_item(true, ICONA_PRODOTTO)
	
	ltvi_item.data = lstr_data
	if isnull(ls_cod_prodotto) then
		ltvi_item.label = "prodotto mancante"
	else
		ltvi_item.label = ls_cod_prodotto + " - " + ls_des_prodotto
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
	
next

setnull(ids_store)
return ll_count
end function

public function integer wf_inserisci_data_creazione (long al_handle);string ls_sql, ls_error
integer ll_count, ll_i
datetime ldt_data_creazione
treeviewitem ltvi_item

ls_sql = "SELECT DISTINCT data_creazione FROM non_conformita WHERE non_conformita.cod_azienda='" + s_cs_xx.cod_azienda + "' "
ls_sql += is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " order by non_conformita.data_creazione desc "

ll_count = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_count < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_count = 0 then
	return 0
end if

for ll_i = 1 to ll_count
	str_treeview lstr_data
	
	ldt_data_creazione =  ids_store.getitemdatetime(ll_i, 1)
	
	lstr_data.tipo_livello = "A"
	lstr_data.livello = il_livello
	lstr_data.codice = string(ldt_data_creazione, s_cs_xx.db_funzioni.formato_data)
	
	ltvi_item = wf_new_item(true, ICONA_DATA_REGISTRAZIONE)
	
	ltvi_item.data = lstr_data
	if isnull(ldt_data_creazione) then
		ltvi_item.label = "data creazione mancante"
	else
		ltvi_item.label = string(ldt_data_creazione, "dd/mm/yyyy")
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
	
next

setnull(ids_store)
return ll_count
end function

public function integer wf_inserisci_data_scadenza (long al_handle);string ls_sql, ls_error
integer ll_count, ll_i
datetime ldt_data_scadenza
treeviewitem ltvi_item

ls_sql = "SELECT DISTINCT data_scadenza FROM non_conformita WHERE non_conformita.cod_azienda='" + s_cs_xx.cod_azienda + "' "
ls_sql += is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " order by non_conformita.data_scadenza desc "

ll_count = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_count < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_count = 0 then
	return 0
end if

for ll_i = 1 to ll_count
	str_treeview lstr_data
	
	ldt_data_scadenza =  ids_store.getitemdatetime(ll_i, 1)
	
	lstr_data.tipo_livello = "S"
	lstr_data.livello = il_livello
	lstr_data.codice = string(ldt_data_scadenza, s_cs_xx.db_funzioni.formato_data)
	
	ltvi_item = wf_new_item(true, ICONA_DATA_SCADENZA)
	
	ltvi_item.data = lstr_data
	if isnull(ldt_data_scadenza) then
		ltvi_item.label = "data creazione mancante"
	else
		ltvi_item.label = string(ldt_data_scadenza, "dd/mm/yyyy")
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
	
next

setnull(ids_store)
return ll_count
end function

public function integer wf_inserisci_tipi_cause (long al_handle);string ls_sql, ls_error, ls_cod_tipo_causa, ls_des_tipo_causa
integer ll_count, ll_i
treeviewitem ltvi_item

ls_sql = "SELECT DISTINCT non_conformita.cod_tipo_causa, tab_tipi_cause.des_tipo_causa &
FROM non_conformita LEFT JOIN tab_tipi_cause ON tab_tipi_cause.cod_azienda = tab_tipi_cause.cod_azienda AND &
non_conformita.cod_tipo_causa = tab_tipi_cause.cod_tipo_causa WHERE non_conformita.cod_azienda='" + s_cs_xx.cod_azienda + "' "
ls_sql += is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " order by non_conformita.cod_tipo_causa "

ll_count = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_count < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_count = 0 then
	return 0
end if

for ll_i = 1 to ll_count
	str_treeview lstr_data
	
	ls_cod_tipo_causa =  ids_store.getitemstring(ll_i, 1)
	ls_des_tipo_causa =  ids_store.getitemstring(ll_i, 2)
	
	lstr_data.tipo_livello = "Z"
	lstr_data.livello = il_livello
	lstr_data.codice = ls_cod_tipo_causa
	
	ltvi_item = wf_new_item(true, ICONA_TIPO_CAUSA)
	
	ltvi_item.data = lstr_data
	if isnull(ls_cod_tipo_causa) then
		ltvi_item.label = "Tipo causa mancante"
	else
		ltvi_item.label = ls_cod_tipo_causa + " - " + ls_des_tipo_causa
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
	
next

setnull(ids_store)
return ll_count
end function

public function integer wf_inserisci_cause (long al_handle);string ls_sql, ls_error, ls_cod_causa, ls_des_causa
integer ll_count, ll_i
treeviewitem ltvi_item

ls_sql = "SELECT DISTINCT non_conformita.cod_causa, cause.des_causa &
FROM non_conformita LEFT JOIN cause ON cause.cod_azienda = non_conformita.cod_azienda AND &
non_conformita.cod_causa = cause.cod_causa WHERE non_conformita.cod_azienda='" + s_cs_xx.cod_azienda + "' "
ls_sql += is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " order by non_conformita.cod_causa "

ll_count = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_count < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_count = 0 then
	return 0
end if

for ll_i = 1 to ll_count
	str_treeview lstr_data
	
	ls_cod_causa =  ids_store.getitemstring(ll_i, 1)
	ls_des_causa =  ids_store.getitemstring(ll_i, 2)
	
	lstr_data.tipo_livello = "B"
	lstr_data.livello = il_livello
	lstr_data.codice = ls_cod_causa
	
	ltvi_item = wf_new_item(true, ICONA_CAUSA)
	
	ltvi_item.data = lstr_data
	if isnull(ls_cod_causa) then
		ltvi_item.label = "Causa mancante"
	else
		ltvi_item.label = ls_cod_causa + " - " + ls_des_causa
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
	
next

setnull(ids_store)
return ll_count
end function

public function long wf_inserisci_nc (long al_handle, long al_anno_non_conf, long al_num_non_conf);treeviewitem ltvi_item
datetime ldt_data_scadenza

str_treeview lstr_data

lstr_data.tipo_livello = "N"
lstr_data.livello = il_livello
lstr_data.decimale[1] = al_anno_non_conf
lstr_data.decimale[2] = al_num_non_conf

ltvi_item = wf_new_item(false, ICONA_NC)

//cambio l'icona della NC selezionata
ltvi_item.selectedpictureindex = ICONA_NC_SELEZIONATA

ltvi_item.data = lstr_data
ltvi_item.label = string(long(lstr_data.decimale[1])) + "/" + string(long(lstr_data.decimale[2])) + &
															wf_label_aggiuntiva(al_anno_non_conf, al_num_non_conf)

return tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
end function

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato","N")
wf_add_valore_livello("Prodotto","P")
wf_add_valore_livello("Data Creazione","A")
wf_add_valore_livello("Data Scadenza","S")
wf_add_valore_livello("Tipo Causa","Z")
wf_add_valore_livello("Causa","B")
wf_add_valore_livello("Tipologia NC","T")
end subroutine

public function integer wf_nome_cognome ();//string ls_nome, ls_cognome, ls_nome_cognome, ls_cod_mansionario
//
//ls_cod_mansionario = uo_mansionario.uof_get_cod_mansionario()
//select nome,cognome
//into :ls_nome, :ls_cognome
//from mansionari
//where
//	cod_azienda = :s_cs_xx.cod_azienda and 
//	cod_resp_divisione =:ls_cod_mansionario ;
//
//if sqlca.sqlcode = 100 or sqlca.sqlcode = -1 then
//	return -1
//else
//	ls_nome_cognome = ls_nome + " " + ls_cognome
//	return ls_nome_cognome
//end if
//
return 0
end function

public subroutine wf_ddlb_tipo_nc ();//Gestione personalòizzazione etichette riferimento Tipo NC
/*
A	Approvigionamenti
V  Vendite
S	Sistema
P	Progettazione
I	Produzione
*/

string						ls_test, ls_origine, ls_vecchio_valore, ls_nuovo_valore, ls_origine_filtro

ls_origine = tab_dettaglio.det_1.dw_1.describe("flag_tipo_nc.Values")
ls_origine_filtro =tab_ricerca.ricerca.dw_ricerca.describe("flag_tipo_nc.Values")

if ls_origine = "!" or ls_origine="?" or ls_origine_filtro = "!" or ls_origine_filtro="?" then
	return
end if

//Sintexcal  --------------------------------------------------------------------------------

//sostituire Produzione con Interne
select stringa
into   :ls_test
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
		 
if sqlca.sqlcode = 0 and not isnull(ls_test) and ls_test<>"" then
	//sintexcal
	ls_vecchio_valore = "Produzione"
	ls_nuovo_valore = "Interna"
	
	//sostuituisci
	guo_functions.uof_replace_string(ls_origine, ls_vecchio_valore, ls_nuovo_valore)
	guo_functions.uof_replace_string(ls_origine_filtro, ls_vecchio_valore, ls_nuovo_valore)
	
	tab_dettaglio.det_1.dw_1.modify("flag_tipo_nc.Values='" + ls_origine + "'")
	tab_ricerca.ricerca.dw_ricerca.modify("flag_tipo_nc.Values='" + ls_origine_filtro + "'")
	
	is_des_tipi_nc[] = {"Approvvigionamenti", ls_nuovo_valore,"Sistema", "Vendita", "Progettazione"}
	
end if
//--------------------------------------------------------------------------------------------


return
end subroutine

public function string wf_stato_documento (integer fi_anno_non_conf, long fl_num_non_conf);string ls_firma_chiusura, ls_cod_tipo_causa, ls_ret
long ll_num_sequenza_corrente

ls_ret = ""
if fi_anno_non_conf>0 and fl_num_non_conf>0 then
else
	return ls_ret
end if

select firma_chiusura, num_sequenza_corrente, cod_tipo_causa
into :ls_firma_chiusura, :ll_num_sequenza_corrente, :ls_cod_tipo_causa
from non_conformita
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_non_conf=:fi_anno_non_conf and
			num_non_conf=:fl_num_non_conf;

if not isnull(ls_firma_chiusura) and ls_firma_chiusura<>"" then return "Conclusa"

if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 and &
		not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
	
	ls_ret = f_trova_stato_documento( ls_cod_tipo_causa, ll_num_sequenza_corrente)
	if isnull(ls_ret) then ls_ret=""

end if

return ls_ret


end function

public function string wf_label_aggiuntiva (integer fi_anno_non_conf, long fl_num_non_conf);datetime	ldt_data_scadenza
string ls_ret, ls_stato_documento

ls_ret = ""
if fi_anno_non_conf>0 and fl_num_non_conf>0 then
else
	return ls_ret
end if

//stato documento -----------------------------------------------------
ls_stato_documento = wf_stato_documento(fi_anno_non_conf, fl_num_non_conf)
if ls_stato_documento<>"" then ls_ret = " - " + ls_stato_documento


//data scadenza -----------------------------------------------------
select data_scadenza
into :ldt_data_scadenza
from non_conformita
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_non_conf=:fi_anno_non_conf and
			num_non_conf=:fl_num_non_conf;

if not isnull(ldt_data_scadenza) and year(date(ldt_data_scadenza))>1950 then
	ls_ret += " - Scad." + string(ldt_data_scadenza, "dd-mm-yyyy")
end if

return ls_ret


end function

on w_non_conformita.create
int iCurrent
call super::create
end on

on w_non_conformita.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;string ls_wizard, ls_ret

is_codice_filtro = "NCF"

tab_dettaglio.det_1.dw_1.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_1.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)
tab_dettaglio.det_2.dw_2.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)
tab_dettaglio.det_3.dw_3.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)
tab_dettaglio.det_4.dw_4.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)

tab_dettaglio.det_1.dw_1.ib_dw_detail = true
tab_dettaglio.det_2.dw_2.ib_dw_detail = true
tab_dettaglio.det_3.dw_3.ib_dw_detail = true
tab_dettaglio.det_4.dw_4.ib_dw_detail = true

iuo_dw_main = tab_dettaglio.det_1.dw_1

iuo_mansionario = create uo_mansionario

//Donato 26-01-2009 parametro per abilitare o meno la gestione protezione dei campi della NC
//in base a quelli specificati nelle fasi del ciclo di vita
is_flag_BNC = "N"

select flag
into :is_flag_BNC
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'BNC' and flag_parametro = 'F';			
if sqlca.sqlcode = 0 then
else
	is_flag_BNC = "N"
end if				
//-----------------------------------------------------------------------------------------------------------------------

// stefanop 27/07/2011: parametro aziendale NWD per abilitare il wizard nella creazione di una nuova non conformita
select flag
into :ls_wizard
from parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_parametro = 'NWD' and 
	flag_parametro = 'F';
	
if sqlca.sqlcode = 0 and ls_wizard = "S" then
	ib_wizard = true
end if				
//-----------------------------------------------------------------------------------------------------------------------

// stefanop 25/010/2011: parametro omnia NCC per abilitare l'apertura finestra dei corsi al salvataggio
select flag
into :ls_wizard
from parametri_omnia
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_parametro = 'NCC' and 
	flag_parametro = 'F';
	
ib_open_costi = (ls_wizard = 'S')	
//-----------------------------------------------------------------------------------------------------------------------

//parametro per abilitare i 4 flag A,B,C,D e renderne almeno uno obbligatorio ad "S"
select stringa
into   :is_flag_nc_abcd
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
		 
if sqlca.sqlcode = 0 and not isnull( is_flag_nc_abcd ) and is_flag_nc_abcd<>"" then
	//sintexcal
	is_flag_nc_abcd = "1"
else
	//no sintexcal
	is_flag_nc_abcd = "0"
end if

ls_ret = tab_dettaglio.det_1.dw_1.modify("flag_nc_tipo_a.visible=~"1~tif('"+is_flag_nc_abcd+"'='1' and flag_tipo_nc='A',1,0)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("flag_nc_tipo_b.visible=~"1~tif('"+is_flag_nc_abcd+"'='1' and flag_tipo_nc='A',1,0)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("flag_nc_tipo_c.visible=~"1~tif('"+is_flag_nc_abcd+"'='1' and flag_tipo_nc='A',1,0)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("flag_nc_tipo_d.visible=~"1~tif('"+is_flag_nc_abcd+"'='1' and flag_tipo_nc='A',1,0)~"")

//SINTEXCAL: nascondi segnalatore NC e Responsabile NC
ls_ret = tab_dettaglio.det_1.dw_1.modify("gb_segnalatore.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("gb_referente_causa.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")

ls_ret = tab_dettaglio.det_1.dw_1.modify("segnalatore_nc_nome_t.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("segnalatore_nc_tel_t.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("segnalatore_nc_email_t.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("segnalatore_nc_nome.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("segnalatore_nc_tel.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("segnalatore_nc_email.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")

ls_ret = tab_dettaglio.det_1.dw_1.modify("referente_causa_nome_t.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("referente_causa_tel_t.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("referente_causa_email_t.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("referente_causa_nome.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("referente_causa_tel.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
ls_ret = tab_dettaglio.det_1.dw_1.modify("referente_causa_email.visible=~"1~tif('"+is_flag_nc_abcd+"'='1',0,1)~"")
//-----------------------------------------------------------------------------------------------------------------------


//caricamento personalizzazioni descrizioni tipo non conformità
wf_ddlb_tipo_nc()

//blocca la ricerca con l'area aziendale del mansionario dell'utente collegato
postevent("ue_carica_area_aziendale")

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(tab_ricerca.ricerca.dw_ricerca, &
							"tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
							"des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'" )

f_PO_LoadDDDW_DW(tab_ricerca.ricerca.dw_ricerca, &
							"causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
							"des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'" )
								  
f_PO_LoadDDDW_DW(tab_ricerca.ricerca.dw_ricerca, &
							"cod_errore", &
							sqlca, &
							"tab_difformita", &
							"cod_errore", &
							"des_difformita", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
								  
f_PO_LoadDDDW_DW(tab_ricerca.ricerca.dw_ricerca, &
							"trattamento", &
							sqlca, &
							"tab_trattamenti", &
							"cod_trattamento", &
							"des_trattamento", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
							
// DW_1							
f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
							"des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'" )

f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
							"des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'" )
								  
f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1, &
							"cod_errore", &
							sqlca, &
							"tab_difformita", &
							"cod_errore", &
							"des_difformita", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
								  
f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1, &
							"cod_trattamento", &
							sqlca, &
							"tab_trattamenti", &
							"cod_trattamento", &
							"des_trattamento", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
							
f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1,"tipo_prodotto",sqlca,&
                 "tab_tipi_prodotto","tipo_prodotto","desc_prodotto", &
                 "tab_tipi_prodotto.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
//f_PO_LoadDDDW_DW(tab_dettaglio.det_4.dw_4,"cod_resp_ver_efficacia",sqlca,&
//                 "mansionari","cod_resp_divisione","nome", &
//                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//					  
//f_PO_LoadDDDW_DW(tab_dettaglio.det_4.dw_4,"firma_chiusura",sqlca,&
//                 "mansionari","cod_resp_divisione","nome", &
//                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
end event

event close;call super::close;destroy iuo_mansionario
end event

event pc_new;// !!! tolto ancestor script !!!

if iuo_mansionario.uof_get_privilege(iuo_mansionario.apertura_nc) = false then
	g_mb.error("Utente non abilitato all'apertura di una Non Conformità")
	return
end if	

call Super::pc_new

tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "data_creazione", datetime(today()))
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_non_conformita
integer x = 1531
integer width = 2491
integer height = 2368
det_2 det_2
det_3 det_3
det_4 det_4
end type

on tab_dettaglio.create
this.det_2=create det_2
this.det_3=create det_3
this.det_4=create det_4
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_4}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
destroy(this.det_3)
destroy(this.det_4)
end on

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 2455
integer height = 2244
string text = "Dati"
dw_1 dw_1
end type

on det_1.create
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_non_conformita
integer width = 1481
integer height = 2364
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer width = 1445
integer height = 2240
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer width = 1211
integer height = 2200
string dataobject = "d_non_conformita_ricerca_tv"
boolean vscrollbar = false
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto")
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca, "cod_cliente")
		
	case "b_ricerca_area_aziendale"
		guo_ricerca.uof_ricerca_area_aziendale(dw_ricerca, "cod_area_aziendale")
		
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer width = 1445
integer height = 2240
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
integer width = 1422
integer height = 2200
end type

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().postevent("pc_retrieve")

end event

event tv_selezione::rightclicked;call super::rightclicked;treeviewitem ltvi_item
str_treeview lstr_data
m_non_conformita lm_menu

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "N" then

	lm_menu = create m_non_conformita
	
	lm_menu.m_chiudi.enabled = iuo_mansionario.uof_get_privilege(iuo_mansionario.chiusura_nc)
	
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
	destroy lm_menu
	
end if
end event

type dw_1 from uo_cs_xx_dw within det_1
integer x = 5
integer y = 12
integer width = 2423
integer height = 2216
integer taborder = 20
string dataobject = "d_non_conformita_1"
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_stato

if not isvalid(istr_data) or isnull(istr_data) or upperbound(istr_data.decimale) < 2 then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2])

if ll_errore < 0 then
   pcca.error = c_fatal
end if

ls_stato = wf_stato_documento(istr_data.decimale[1],istr_data.decimale[2])

tab_dettaglio.det_1.dw_1.Modify("t_stato_nc.text='"+ls_stato+"'")
tab_dettaglio.det_2.dw_2.Modify("t_stato_nc.text='"+ls_stato+"'")
tab_dettaglio.det_3.dw_3.Modify("t_stato_nc.text='"+ls_stato+"'")
tab_dettaglio.det_4.dw_4.Modify("t_stato_nc.text='"+ls_stato+"'")

change_dw_current()
end event

event pcd_disable;call super::pcd_disable;ib_new = false
end event

event pcd_modify;call super::pcd_modify;if ib_pulsanti_padre then
	object.b_ricerca_fornitore.enabled=true
	object.b_ricerca_cliente.enabled=true
	object.b_ricerca_operaio.enabled = true
	object.b_ricerca_reparto.enabled = true
end if

ib_new = false
end event

event pcd_new;call super::pcd_new;string ls_cod_operaio

ib_new = true
if ib_pulsanti_padre then
	object.b_ricerca_fornitore.enabled=true
	object.b_ricerca_cliente.enabled=true
	object.b_ricerca_operaio.enabled = true
	object.b_ricerca_reparto.enabled = true
end if


//Donato 10/06/2009 aggiunto dettaglio stock
//cb_lotti.enabled = false

setitem(getrow(),"flag_stato_nc","O")

//Donato 26-01-2009 verifica se è abilitata la gestione blocco campi
if is_flag_BNC = "S" then
	//Donato 26-11-2008 recupero responsabile a partire dall'utente collegato
	select cod_operaio
	into :ls_cod_operaio
	from anag_operai
	where cod_azienda=:s_cs_xx.cod_azienda
		and cod_utente=:s_cs_xx.cod_utente;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore in lettura dati da tabella anag_operai", sqlca)
		return
	end if
	
	if not isnull(ls_cod_operaio) and ls_cod_operaio <> "" then
		//set del campo responsabile iter NC
		setitem(getrow(),"cod_operaio",ls_cod_operaio)
		
		//QUESTA MODIFICA SE LA DEVONO SUDARE!!!!!!!
		//vedi anche evento funzione wf_proteggi_documento
		
		//il campo responsabile Iter NC deve essere protetto a prescindere
		object.cod_operaio.protect = 1
		//----------
	end if
end if

if ib_wizard then
	s_cs_xx.parametri.parametro_uo_dw_1 = dw_1
	s_cs_xx.parametri.parametro_s_1 = "V"
	window_open(w_non_conformita_std_wizard, -1)
end if
end event

event pcd_validaterow;call super::pcd_validaterow;datetime ldt_creaz, ldt_scad
string ls_flag, ls_flag_tipo_nc

if isnull(this.getitemstring(this.getrow(), "flag_tipo_nc")) or this.getitemstring(this.getrow(), "flag_tipo_nc")="" then
   g_mb.messagebox("Non Conformità","Tipologia NC obbligatoria",Exclamation!)
   pcca.error = c_fatal
   return
end if

if isnull(this.getitemstring(this.getrow(), "cod_operaio")) then
   g_mb.messagebox("Non Conformità","Codice responsabile non conformità obbligatorio",Exclamation!)
   pcca.error = c_fatal
   return
end if

//Donato 26-01-2009 verifica se è abilitata la gestione blocco campi
if is_flag_BNC <> "S" then
	//vecchia gestione: obbligatorietà del campo
	if isnull(this.getitemstring(this.getrow(), "cod_errore")) then
		g_mb.messagebox("Non Conformità","Codice difetto obbligatorio",Exclamation!)
		pcca.error = c_fatal
		return
	end if
	
	if isnull(this.getitemstring(this.getrow(), "cod_trattamento")) then
		g_mb.messagebox("Non Conformità","Codice trattamento obbligatorio",Exclamation!)
		pcca.error = c_fatal
		return
	end if
	
end if

if isnull(this.getitemstring(this.getrow(), "cod_misura")) and (this.getitemnumber(this.getrow(), "quan_non_conformita") > 0) then
   g_mb.messagebox("Non Conformità","Codice misura obbligatorio in presenza di quantità",Exclamation!)
   pcca.error = c_fatal
   return
end if

ldt_creaz = getitemdatetime(this.getrow(),"data_creazione")
ldt_scad = getitemdatetime(this.getrow(),"data_scadenza")


//Donato 26-01-2009 verifica se è abilitata la gestione blocco campi
if is_flag_BNC <> "S" then
	//vecchia gestione: obbligatorietà del campo
	if isnull(ldt_scad) then
		g_mb.messagebox("Non Conformità","ATTENZIONE! la data scadenza risulta mancante",Exclamation!)
			pcca.error = c_fatal
		return
	end if
end if

if ldt_scad < ldt_creaz and not isnull(ldt_scad) then
   g_mb.messagebox("Non Conformità","ATTENZIONE! data scadenza minore di data creazione",Exclamation!)
   pcca.error = c_fatal
   return
end if

if is_flag_nc_abcd="1" then
	ls_flag_tipo_nc = tab_dettaglio.det_1.dw_1.getitemstring(this.getrow(), "flag_tipo_nc" )
	
	if ls_flag_tipo_nc="A" then
	
		ls_flag = tab_dettaglio.det_1.dw_1.getitemstring(this.getrow(), "flag_nc_tipo_a" )
		ls_flag += tab_dettaglio.det_1.dw_1.getitemstring(this.getrow(), "flag_nc_tipo_b" )
		ls_flag += tab_dettaglio.det_1.dw_1.getitemstring(this.getrow(), "flag_nc_tipo_c" )
		ls_flag += tab_dettaglio.det_1.dw_1.getitemstring(this.getrow(), "flag_nc_tipo_d" )
	
		if pos(ls_flag,"S") > 0 then
			//OK c'è almeno uno a S
		else
			 g_mb.messagebox("Non Conformità","Indicare almeno una tipologia tra A,B,C,D", Exclamation!)
			pcca.error = c_fatal
			return
		end if
	
	end if
	
end if
end event

event pcd_view;call super::pcd_view;//cb_costi.enabled = true

//cb_doc_aut_deroga.enabled = false
if ib_pulsanti_padre then
object.b_ricerca_fornitore.enabled=false
object.b_ricerca_cliente.enabled=false
object.b_ricerca_operaio.enabled = false
object.b_ricerca_reparto.enabled = false
end if
//cb_dettagli.enabled = true
//cb_doc_compilato.enabled = false

//Donato 10/06/2009 aggiunto dettaglio stock
//cb_lotti.enabled = true
end event

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_tipo_causa, ls_firma_chiusura

long   ll_riga, ll_num_sequenza_corrente

ll_riga = this.getrow()

if ll_riga < 1 then return

is_flag_conferma =getitemstring(ll_riga, "flag_eseguito_ac")

ll_num_sequenza_corrente = this.getitemnumber( ll_riga, "num_sequenza_corrente")

ls_cod_tipo_causa = this.getitemstring( ll_riga, "cod_tipo_causa")

ls_firma_chiusura = getitemstring( ll_riga, "firma_chiusura")

if not isnull(ls_firma_chiusura) and ls_firma_chiusura <> "" then
//	mle_stato.text = "Conclusa"
	//Donato 26-01-2009 questo lo fai solo se è abilitata la gestione del blocco campi
	if is_flag_BNC = "S" then
		wf_proteggi_documento(ls_cod_tipo_causa, ll_num_sequenza_corrente, true)
//		dw_non_conformita_std_1.setrow(ll_riga)
//		dw_non_conformita_std_2.setrow(ll_riga)
//		dw_non_conformita_std_3.setrow(ll_riga)
//		dw_non_conformita_std_4.setrow(ll_riga)
	end if	
else

	if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
		//mle_stato.text = f_trova_stato_documento( ls_cod_tipo_causa, ll_num_sequenza_corrente)
		//Donato 26-01-2009 questo lo fai solo se è abilitata la gestione del blocco campi
		if is_flag_BNC = "S" then
			wf_proteggi_documento(ls_cod_tipo_causa, ll_num_sequenza_corrente, false)
//			dw_non_conformita_std_1.setrow(ll_riga)
//			dw_non_conformita_std_2.setrow(ll_riga)
//			dw_non_conformita_std_3.setrow(ll_riga)
//			dw_non_conformita_std_4.setrow(ll_rig//a)
		end if		
	else
		//mle_stato.text = ""
		//Donato 26-01-2009 questo lo fai solo se è abilitata la gestione del blocco campi
		if is_flag_BNC = "S" then
			wf_proteggi_documento(ls_cod_tipo_causa, ll_num_sequenza_corrente, false)
//			dw_non_conformita_std_1.setrow(ll_riga)
//			dw_non_conformita_std_2.setrow(ll_riga)
//			dw_non_conformita_std_3.setrow(ll_riga)
//			dw_non_conformita_std_4.setrow(ll_riga)
		end if	
	end if
end if

string ls_responsabile_iter_nc, ls_cod_utente

//Donato 26-01-2009 questo lo fai solo se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	
	ls_responsabile_iter_nc = getitemstring( ll_riga, "cod_operaio")
	select cod_utente
	into :ls_cod_utente
	from anag_operai
	where cod_azienda = :s_cs_xx.cod_azienda
		and cod_operaio = :ls_responsabile_iter_nc;
		
	if not isnull(ls_cod_utente) and ls_cod_utente <> "" then
		wf_loaddddw_dw( dw_1, &
								"cod_tipo_causa", &
								sqlca, &
								"tab_tipi_cause", &
								"b.cod_tipo_causa", &
											"b.des_tipo_causa", &
								"as b "+&
									"join (select 	x.cod_azienda,"+&
												"x.cod_tipo_lista_dist,"+&
												"x.cod_lista_dist,"+&
												"x.cod_destinatario "+&
											"from det_liste_dist_destinatari as x "+&
											"join det_liste_dist as y on y.cod_azienda=x.cod_azienda "+&
												"and y.cod_tipo_lista_dist=x.cod_tipo_lista_dist "+&
												"and y.cod_lista_dist=x.cod_lista_dist "+&
												"and y.cod_destinatario=x.cod_destinatario "+&
												"and y.num_sequenza=x.num_sequenza "+&
											"join tab_ind_dest as z on z.cod_azienda=x.cod_azienda "+&
												"and z.cod_destinatario= x.cod_destinatario_mail " +&							
											"where z.cod_utente = '"+ls_cod_utente+"' "+&
												"and (y.num_sequenza_prec=0 or y.num_sequenza_prec is null) "+&
											"group by x.cod_azienda,x.cod_tipo_lista_dist,x.cod_lista_dist,x.cod_destinatario "+&
											  ") as a on a.cod_azienda=b.cod_azienda "+&
												"and a.cod_tipo_lista_dist=b.cod_tipo_lista_dist "+&
												"and b.cod_lista_dist=a.cod_lista_dist ",&
								"b.cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((b.flag_blocco <> 'S') or (b.flag_blocco = 'S' and b.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	end if
end if
end event

event updatestart;call super::updatestart;//if ib_new then
//
//	long ll_anno,ll_num_registrazione
//	
//	ll_anno = f_anno_esercizio()
//	SetItem (getrow(), "anno_non_conf", ll_anno)
//	
//	select max(non_conformita.num_non_conf)
//	into :ll_num_registrazione
//	from non_conformita
//	where 
//		non_conformita.cod_azienda = :s_cs_xx.cod_azienda and
//		 non_conformita.anno_non_conf = :ll_anno;
//	
//	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
//		ll_num_registrazione = 1
//	else
//		ll_num_registrazione ++
//	end if
//	
//	SetItem (getrow(), "num_non_conf", ll_num_registrazione)
//	
//	
//	wf_inserisci_nc(0, ll_anno, ll_num_registrazione)
//end if

integer li_anno_nc
long ll_num_nc, ll_row

if DeletedCount() > 0 then
	
	li_anno_nc = getitemnumber(1, "anno_non_conf", delete!, false)
	ll_num_nc = getitemnumber(1, "num_non_conf", delete!, false)
	
	delete from det_liste_dist_fasi_com
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_non_conf=:li_anno_nc and
				num_non_conf=:ll_num_nc;
	
 	wf_set_deleted_item_status()
end if



end event

event updateend;call super::updateend;//cb_costi.enabled = true

long 	 ll_i, ll_anno_nc, ll_num_nc, ll_num_sequenza_corrente, ll_d, ll_anno_d, ll_numero_d

string ls_difetto, ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_Flag_gestione_fasi, &
       ls_cod_area_aziendale, ls_cod_prodotto, ls_cod_cat_mer


//inserimento elemento sull'albero se nuovo ######################################
ll_i = this.getrow()

choose case this.getitemstatus(ll_i, 0, Primary!)
	case NewModified!	
		
		ll_anno_nc = this.getitemnumber(ll_i,"anno_non_conf")
		ll_num_nc = this.getitemnumber(ll_i,"num_non_conf")
		
		wf_inserisci_nc(0, ll_anno_nc, ll_num_nc)
		
	case New!
		
end choose
//###################################################################


if s_cs_xx.num_livello_mail < 1 or isnull(s_cs_xx.num_livello_mail) then return


for ll_i = 1 to rowcount()
		
	ll_anno_nc = this.getitemnumber(ll_i,"anno_non_conf")
	ll_num_nc = this.getitemnumber(ll_i,"num_non_conf")
	ls_difetto = this.getitemstring(ll_i,"cod_errore")
	ls_cod_tipo_causa = this.getitemstring( ll_i, "cod_tipo_causa")
			
	if not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then       // *** controllo le fasi
				
		select cod_tipo_lista_dist,
		       cod_lista_dist
		into   :ls_cod_tipo_lista_dist,
		       :ls_cod_lista_dist
		from   tab_tipi_cause
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_causa = :ls_cod_tipo_causa;
					 
		if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
					
			select flag_gestione_fasi
			into   :ls_flag_gestione_fasi
			from   tab_tipi_liste_dist
			where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
					
			if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
					
			if ls_flag_gestione_fasi = "S" then
					
				ll_num_sequenza_corrente = this.getitemnumber( ll_i, "num_sequenza_corrente")
						
				select cod_area_aziendale
				into   :ls_cod_area_aziendale
				from   non_conformita 
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_non_conf = :ll_anno_nc and
						 num_non_conf = :ll_num_nc;
					
				ls_cod_prodotto = this.getitemstring( ll_i, "cod_prodotto")			
						
				select cod_cat_mer
				into   :ls_cod_cat_mer
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if isnull(ls_cod_cat_mer) then ls_cod_cat_mer = ""
					
				if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""							
					
				if getitemstatus(ll_i,0,primary!) = datamodified!	then
						
					s_cs_xx.parametri.parametro_s_1 = "L"	
					s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
					s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
					s_cs_xx.parametri.parametro_s_4 = ""											
					s_cs_xx.parametri.parametro_s_5 = "Modifica Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari autorizzati?"
					s_cs_xx.parametri.parametro_s_8 = ""
					s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
					s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer						
					s_cs_xx.parametri.parametro_s_11 = ""
					s_cs_xx.parametri.parametro_s_12 = ""
					s_cs_xx.parametri.parametro_s_13 = string(ll_anno_nc)
					s_cs_xx.parametri.parametro_s_14 = string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_15 = "N"
					s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
					s_cs_xx.parametri.parametro_b_1 = true
							
					openwithparm( w_invio_messaggi, 0)					
							
					if not isnull(s_cs_xx.parametri.parametro_ul_1) then
						
						if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
							
						update non_conformita
						set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_non_conf = :ll_anno_nc and
								 num_non_conf = :ll_num_nc;
								 
						if sqlca.sqlcode < 0 then
							g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
							rollback;
							return -1
						end if
						
						commit;							
						//parent.triggerevent("pc_retrieve")	
							
					end if
					//dw_non_conformita_lista.setrow( ll_i)
						
					continue												
						
				elseif getitemstatus(ll_i,0,primary!) = newmodified! then
						
					s_cs_xx.parametri.parametro_s_1 = "L"
					s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
					s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
					s_cs_xx.parametri.parametro_s_4 = ""																	
					s_cs_xx.parametri.parametro_s_5 = "Creazione Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_6 = "E' stata creata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari autorizzati?"
					s_cs_xx.parametri.parametro_s_8 = ""
					s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
					s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer						
					s_cs_xx.parametri.parametro_s_11 = ""
					s_cs_xx.parametri.parametro_s_12 = ""
					s_cs_xx.parametri.parametro_s_13 = string(ll_anno_nc)
					s_cs_xx.parametri.parametro_s_14 = string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_15 = "N"
					s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
					s_cs_xx.parametri.parametro_b_1 = true
							
					openwithparm( w_invio_messaggi, 0)					
							
					if not isnull(s_cs_xx.parametri.parametro_ul_1) then
							
						if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
							
						update non_conformita
						set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_non_conf = :ll_anno_nc and
								 num_non_conf = :ll_num_nc;
								 
						if sqlca.sqlcode < 0 then
							g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
							rollback;
							return -1
						end if
							
						commit;							
							
						//parent.triggerevent("pc_retrieve")
	
					end if						
							
					//dw_non_conformita_lista.setrow( ll_i)
							
					continue
						
				else								
					continue						
				end if	
				
			else  //************** da mettere gestione normale
				
				select cod_tipo_lista_dist,
						 cod_lista_dist
				into   :ls_cod_tipo_lista_dist,
						 :ls_cod_lista_dist
				from   tab_difformita
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_errore = :ls_difetto;
								 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("OMNIA","Errore in lettura lista distribuzione da tab_difformita: " + sqlca.sqlerrtext)
					continue
				elseif sqlca.sqlcode = 100 then
					continue
				end if
						
				if getitemstatus(ll_i,0,primary!) = datamodified!	then
					s_cs_xx.parametri.parametro_s_1 = "L"
					s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
					s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
					s_cs_xx.parametri.parametro_s_4 = ""
					s_cs_xx.parametri.parametro_s_5 = "Modifica Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
					s_cs_xx.parametri.parametro_s_8 = ""
					openwithparm(w_invio_messaggi,0)
				elseif getitemstatus(ll_i,0,primary!) = newmodified! then
					s_cs_xx.parametri.parametro_s_1 = "L"
					s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
					s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
					s_cs_xx.parametri.parametro_s_4 = ""
					s_cs_xx.parametri.parametro_s_5 = "Creazione Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_6 = "E' stata creata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
					s_cs_xx.parametri.parametro_s_8 = ""
					openwithparm(w_invio_messaggi,0)
				else		
					continue		
				end if								
										
			end if
					
		elseif sqlca.sqlcode < 0 then
					
			g_mb.messagebox( "OMNIA", "Errore durante la ricerca della lista di distribuzione: " + sqlca.sqlerrtext)				
			return 1
			
		else

			select cod_tipo_lista_dist,
					 cod_lista_dist
			into   :ls_cod_tipo_lista_dist,
					 :ls_cod_lista_dist
			from   tab_difformita
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_errore = :ls_difetto;
							 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA","Errore in lettura lista distribuzione da tab_difformita: " + sqlca.sqlerrtext)
				continue
			elseif sqlca.sqlcode = 100 then
				continue
			end if
						
			if getitemstatus(ll_i,0,primary!) = datamodified!	then
				s_cs_xx.parametri.parametro_s_1 = "L"
				s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
				s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
				s_cs_xx.parametri.parametro_s_4 = ""
				s_cs_xx.parametri.parametro_s_5 = "Modifica Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
				s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
				s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
				s_cs_xx.parametri.parametro_s_8 = ""
				openwithparm(w_invio_messaggi,0)
			elseif getitemstatus(ll_i,0,primary!) = newmodified! then
				s_cs_xx.parametri.parametro_s_1 = "L"
				s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
				s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
				s_cs_xx.parametri.parametro_s_4 = ""
				s_cs_xx.parametri.parametro_s_5 = "Creazione Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
				s_cs_xx.parametri.parametro_s_6 = "E' stata creata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
				s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
				s_cs_xx.parametri.parametro_s_8 = ""
				openwithparm(w_invio_messaggi,0)
			else		
				continue		
			end if				
				
		end if
			
	else
			
		select cod_tipo_lista_dist,
				 cod_lista_dist
		into   :ls_cod_tipo_lista_dist,
				 :ls_cod_lista_dist
		from   tab_difformita
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_errore = :ls_difetto;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura lista distribuzione da tab_difformita: " + sqlca.sqlerrtext)
			continue
		elseif sqlca.sqlcode = 100 then
			continue
		end if
			
		if getitemstatus(ll_i,0,primary!) = datamodified!	then
			s_cs_xx.parametri.parametro_s_1 = "L"
			s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
			s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
			s_cs_xx.parametri.parametro_s_4 = ""
			s_cs_xx.parametri.parametro_s_5 = "Modifica Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
			s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
			s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
			s_cs_xx.parametri.parametro_s_8 = ""
			openwithparm(w_invio_messaggi,0)
		elseif getitemstatus(ll_i,0,primary!) = newmodified! then
			s_cs_xx.parametri.parametro_s_1 = "L"
			s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
			s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
			s_cs_xx.parametri.parametro_s_4 = ""
			s_cs_xx.parametri.parametro_s_5 = "Creazione Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
			s_cs_xx.parametri.parametro_s_6 = "E' stata creata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
			s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
			s_cs_xx.parametri.parametro_s_8 = ""
			openwithparm(w_invio_messaggi,0)
		else		
			continue		
		end if
										
	end if
		
next	

if rowsinserted > 0 or rowsupdated > 0 then
	// stefanop : apro finestra costi se necessario
	if ib_open_costi then
		//cb_costi.triggerevent("clicked")
	end if
	// ---
end if

// ***  michela 16/05/2005: per ogni riga cancellata, se esiste l'accettazione materiali, la annullo (per sintexcal)

for ll_d = 1 to deletedcount()
	ll_anno_d = this.getitemnumber( ll_d, "anno_non_conf", delete!, true)
	ll_numero_d = this.getitemnumber( ll_d, "num_non_conf", delete!, true)
	
	update acc_materiali
	set    anno_non_conf = null,
	       num_non_conf = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_non_conf = :ll_anno_d and
			 num_non_conf = :ll_numero_d;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante l'annullamento NC in accettazione: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
next

commit;
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_1, "cod_cliente")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_1, "cod_fornitore")
		
	case "b_ricerca_operaio"
		guo_ricerca.uof_set_where("anag_operai.flag_blocco<>'S'")
		guo_ricerca.uof_ricerca_operaio(dw_1, "cod_operaio")
		
	case "b_ricerca_reparto"
		guo_ricerca.uof_ricerca_reparto(dw_1, "cod_reparto")
		
end choose
end event

event itemchanged;call super::itemchanged;string ls_nulla, ls_cod_tipo_causa, ls_cod_divisione, ls_flag_criticita, ls_flag_categoria_regis
long ll_num_sequenza_corrente, ll_riga
setnull(ls_nulla)

if i_extendmode then
	
   choose case i_colname
			
		case "cod_tipo_causa"
			
			setitem( i_rownbr, "cod_causa", ls_nulla)
			
			f_PO_LoadDDDW_DW(dw_1, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + &
								"' and ( cod_tipo_causa = '" + i_coltext + "' or cod_tipo_causa is null ) and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			
			//Donato 26-01-2009 controlla se è abilitata la obbligatorietà dei campi
			if is_flag_BNC = "S" then
				ll_num_sequenza_corrente = getitemnumber(row, "num_sequenza_corrente")			
				wf_proteggi_documento(data, ll_num_sequenza_corrente, false)
				
				setcolumn("cod_causa")
			end if
			//vecchio codice
			/*
			//Donato 26-11-2008						
			ll_riga = dw_non_conformita_lista.getrow()
			ll_num_sequenza_corrente = dw_non_conformita_lista.getitemnumber(ll_riga, "num_sequenza_corrente")			
			wf_proteggi_documento(data, ll_num_sequenza_corrente, false)
			
			setcolumn("cod_causa")
			//fine modifica
			*/
			//fine modifica ---------------------------------------------------
						
		case "cod_causa"
			
			select cod_tipo_causa
			into   :ls_cod_tipo_causa
			from   cause
			where  cod_azienda = :s_cs_xx.cod_azienda and 
			       cod_causa = :i_coltext;
					 
//			if sqlca.sqlcode = 0 then
//				setitem(i_rownbr, "cod_tipo_causa", ls_cod_tipo_causa)
//			end if
			
			if ib_visualizza_campi then
			
				select flag_criticita,
						 flag_categoria_regis
				into   :ls_flag_criticita,
						 :ls_flag_categoria_regis
				from   cause
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_causa = :i_coltext;
					 
				if sqlca.sqlcode < 0 then				
					g_mb.messagebox( "OMNIA", "Errore durante la ricerca criticita e categoria registrazione: " + sqlca.sqlerrtext)				
					return -1				
				end if
			
				if ls_flag_criticita = "" then setnull(ls_flag_criticita)			
				if ls_flag_categoria_regis = "" then setnull(ls_flag_categoria_regis)	
			
				choose case ls_flag_criticita
					case 'G'
						ls_flag_criticita = "A"
					case 'M'
						ls_flag_criticita = "B"
					case else
						ls_flag_criticita = "B"
				end choose
				
				this.setitem( row, "livello_impatto", ls_flag_criticita)				
				this.setitem( row, "flag_categoria_regis", ls_flag_categoria_regis)			
				
			end if
			
		case "cod_divisione"
			setitem( i_rownbr, "cod_area_aziendale", ls_nulla)
			f_PO_LoadDDDW_DW( dw_1, &
							"cod_area_aziendale", &
							sqlca, &
							"tab_aree_aziendali", &
							"cod_area_aziendale", &
                     "des_area", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + &
								"' and cod_divisione = '" + i_coltext + "' ")
		case "cod_area_aziendale"
			select cod_divisione
			into :ls_cod_divisione
			from tab_aree_aziendali
			where cod_azienda = :s_cs_xx.cod_azienda 
			  and cod_area_aziendale =:i_coltext;
			if sqlca.sqlcode = 0 then
				setitem(i_rownbr, "cod_divisione", ls_cod_divisione)
			end if
			 
	end choose
end if
end event

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

	string ls_cod_trattamento
	
	choose case i_colname
	case "cod_errore"
		SELECT tab_difformita.cod_trattamento  
		INTO   :ls_cod_trattamento  
		FROM   tab_difformita  
		WHERE  ( tab_difformita.cod_azienda = :s_cs_xx.cod_azienda ) AND  
				 ( tab_difformita.cod_errore = :i_coltext )   ;
		if isnull(ls_cod_trattamento) or len(ls_cod_trattamento) < 1 then
			g_mb.messagebox("Non Conformità","Attenzione non è possibile determinare il trattamento proposto per questo errore: selezionarlo dall'apposita lista", Information!)
		else
			setitem(getrow(),"cod_trattamento",ls_cod_trattamento)
		end if
	end choose

end if ////  del controllo su i_extendmode

end event

event pcd_setkey;call super::pcd_setkey;integer li_i, li_anno
long ll_num_registrazione

for li_i = 1 to rowcount()
	
	if isnull(getitemstring(li_i, "cod_azienda")) or getitemstring(li_i, "cod_azienda")="" then
		setitem(li_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemnumber(li_i, "anno_non_conf")) or getitemnumber(li_i, "anno_non_conf")<=0 then
		li_anno =  f_anno_esercizio()
		setitem(li_i, "anno_non_conf", li_anno)
	else
		li_anno =  getitemnumber(li_i, "anno_non_conf")
	end if
	
	if isnull(getitemnumber(li_i, "num_non_conf")) or getitemnumber(li_i, "num_non_conf")<=0 then
		select max(num_non_conf)
		into :ll_num_registrazione
		from non_conformita
		where 	cod_azienda = :s_cs_xx.cod_azienda and
			 		anno_non_conf = :li_anno;
					 
		if isnull(ll_num_registrazione) then ll_num_registrazione = 0
		ll_num_registrazione += 1
		
		setitem(li_i, "num_non_conf", ll_num_registrazione)
	end if
	
next

end event

event dberror;long ll_null
string ls_errore, ls_db, ls_errore_PK

//pulisco num_registrazione
if row>0 then
	setnull(ll_null)
	setitem(row, "num_non_conf", ll_null)
end if

//visualizzo anche i primi 300 caratteri dell'errore
ls_errore = "sqldbcode="+string(sqldbcode) + "~r~n" + left(sqlerrtext, 300)
ls_errore_PK = "Problemi nel calcolo del numero NC da assegnare. Riprova subito a salvare con il menu del tasto destro per riassegnare un nuovo numero alla NC!"

//codice temporaneo da integrare a breve nella classe
//se si tratta di violazione di chiave duplicata restituisco un messaggio di errore mirato

ls_db = f_db()
choose case ls_db
	case "SYBASE_ASA"
		if sqldbcode = -193 then
			ls_errore = ls_errore_PK
		end if
		
	case "SYBASE_ASE"
		if sqldbcode = 2601 then
			ls_errore = ls_errore_PK
		end if

	case "MSSQL"
		if sqldbcode = 2627 then
			ls_errore = ls_errore_PK
		end if

	case "ORACLE"
		//per ora non conosciamo il formato di codice che restituisce oracle (sarebbe in realtà ORA-00001)
	
end choose

g_mb.warning("Errore in salvataggio", ls_errore)

pcca.error = c_fatal

return 3
end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2455
integer height = 2244
long backcolor = 12632256
string text = "Decisioni/Cause"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from uo_cs_xx_dw within det_2
integer x = 32
integer y = 16
integer width = 2395
integer height = 1392
integer taborder = 11
string dataobject = "d_non_conformita_std_2"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;double ld_quantita

ld_quantita = this.getitemnumber(this.getrow(), "quan_non_conformita")

choose case i_colname
case "quan_derogata"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_derogata") + long(i_coltext)
case "quan_recuperata"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_recuperata") + long(i_coltext)
case "quan_accettabile"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_accettabile") + long(i_coltext)
case "quan_resa"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_resa") + long(i_coltext)
case "quan_rottamata"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_rottamata") + long(i_coltext)
case "quan_carico"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_carico") + long(i_coltext)
end choose

this.setitem(this.getrow(),"quan_non_conformita", ld_quantita)


end event

type det_3 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2455
integer height = 2244
long backcolor = 12632256
string text = "Q.tà/Correzioni"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_3 dw_3
end type

on det_3.create
this.dw_3=create dw_3
this.Control[]={this.dw_3}
end on

on det_3.destroy
destroy(this.dw_3)
end on

type dw_3 from uo_cs_xx_dw within det_3
integer x = 41
integer y = 20
integer width = 2391
integer height = 1140
integer taborder = 11
string dataobject = "d_non_conformita_std_3"
boolean border = false
end type

type det_4 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2455
integer height = 2244
long backcolor = 12632256
string text = "Prevenz./Note"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_4 dw_4
end type

on det_4.create
this.dw_4=create dw_4
this.Control[]={this.dw_4}
end on

on det_4.destroy
destroy(this.dw_4)
end on

type dw_4 from uo_cs_xx_dw within det_4
event ue_imposta_flag_ac ( )
integer x = -18
integer y = 12
integer width = 2354
integer height = 1444
integer taborder = 11
string dataobject = "d_non_conformita_std_4"
boolean border = false
end type

event ue_imposta_flag_ac();tab_dettaglio.det_1.dw_1.setitem(getrow(), "flag_eseguito_ac", is_flag_conferma)
end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "flag_eseguito_ac"
		if not iuo_mansionario.uof_get_privilege(iuo_mansionario.chiusura_nc) then
			g_mb.messagebox("Omnia", "Utente non abilitato alla modifica di questo Flag.")
			postevent("ue_imposta_flag_ac")
		end if			
			
end choose
	
end event

