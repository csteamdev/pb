﻿$PBExportHeader$w_non_conformita_approvv.srw
$PBExportComments$Finestra Dati Specifici Non Conformità Approvvigionamenti
forward
global type w_non_conformita_approvv from w_cs_xx_principale
end type
type cb_gen_richiesta from commandbutton within w_non_conformita_approvv
end type
type dw_non_conformita_approvvigionamenti from uo_cs_xx_dw within w_non_conformita_approvv
end type
type dw_folder from u_folder within w_non_conformita_approvv
end type
end forward

global type w_non_conformita_approvv from w_cs_xx_principale
integer width = 2519
integer height = 1704
string title = "Dati Specifici Non Conformità Approvvigionamenti"
cb_gen_richiesta cb_gen_richiesta
dw_non_conformita_approvvigionamenti dw_non_conformita_approvvigionamenti
dw_folder dw_folder
end type
global w_non_conformita_approvv w_non_conformita_approvv

type variables
string is_cod_fornitore
end variables

forward prototypes
public subroutine wf_ric_bol_acq ()
public subroutine wf_collaudo_ric ()
end prototypes

public subroutine wf_ric_bol_acq ();string LS_NUM

// ------------------------------------------------------------------------------------------
//                              RICERCA BOLLE DI ACQUISTO
// Significato dei parametri
//
//
//   s_cs_xx.parametri.parametro_s_1 .... s_9   --->> nomi colonne su cui incidere i valori cercati
//   s_cs_xx.parametri.parametro_s_10.... s_15  --->> valori di filtro
//
//   s_cs_xx.parametri.parametro_d_1  = anno della bolla
//   s_cs_xx.parametri.parametro_s_10 = codice del prodotto
//   s_cs_xx.parametri.parametro_d_11 = codice del numero della bolla originale
//
// ------------------------------------------------------------------------------------------

dw_non_conformita_approvvigionamenti.accepttext()

s_cs_xx.parametri.parametro_d_1 = dw_non_conformita_approvvigionamenti.getitemnumber(dw_non_conformita_approvvigionamenti.getrow(),"anno_bolla_acq")
if isnull(s_cs_xx.parametri.parametro_d_1) then
   g_mb.messagebox("Ricerca Bolla di Vendita","Selezionare l'anno su cui effetuare la ricerca",StopSign!)
   return
end if

s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_approvvigionamenti.getitemstring(dw_non_conformita_approvvigionamenti.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = string(dw_non_conformita_approvvigionamenti.getitemnumber(dw_non_conformita_approvvigionamenti.getrow(),"num_bolla_acq"))
s_cs_xx.parametri.parametro_d_5 = 1

dw_non_conformita_approvvigionamenti.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "anno_bolla_acq"
s_cs_xx.parametri.parametro_s_2 = "num_bolla_acq"
s_cs_xx.parametri.parametro_s_3 = "prog_riga_bolla_acq"

window_open(w_ricerca_bol_acq, 0)
end subroutine

public subroutine wf_collaudo_ric ();s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_approvvigionamenti.getitemstring(dw_non_conformita_approvvigionamenti.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_non_conformita_approvvigionamenti.getitemstring(dw_non_conformita_approvvigionamenti.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_non_conformita_approvvigionamenti.getitemstring(dw_non_conformita_approvvigionamenti.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_non_conformita_approvvigionamenti.getitemstring(dw_non_conformita_approvvigionamenti.getrow(),"cod_lotto")
s_cs_xx.parametri.parametro_data_1 = dw_non_conformita_approvvigionamenti.getitemdatetime(dw_non_conformita_approvvigionamenti.getrow(),"data_stock")
s_cs_xx.parametri.parametro_d_1 = dw_non_conformita_approvvigionamenti.getitemnumber(dw_non_conformita_approvvigionamenti.getrow(),"prog_stock")

dw_non_conformita_approvvigionamenti.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "anno_reg_collaudi"
s_cs_xx.parametri.parametro_s_2 = "num_reg_collaudi"
s_cs_xx.parametri.parametro_s_3 = "cod_deposito"
s_cs_xx.parametri.parametro_s_4 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_5 = "cod_lotto"
s_cs_xx.parametri.parametro_s_6 = "data_stock"
s_cs_xx.parametri.parametro_s_7 = "prog_stock"
s_cs_xx.parametri.parametro_s_8 = "cod_prodotto"

window_open(w_ricerca_collaudo, 0)

end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

dw_non_conformita_approvvigionamenti.set_dw_key("cod_azienda")
dw_non_conformita_approvvigionamenti.set_dw_key("anno_non_conf")
dw_non_conformita_approvvigionamenti.set_dw_key("num_non_conf")
dw_non_conformita_approvvigionamenti.set_dw_options(sqlca,pcca.null_object,c_modifyonopen+c_nonew+c_nodelete,c_default)
iuo_dw_main = dw_non_conformita_approvvigionamenti

l_objects[1] = dw_non_conformita_approvvigionamenti
dw_folder.fu_AssignTab(1, "Approvvigionamenti", l_Objects[])
dw_folder.fu_FolderCreate(1,4)
                          // il primo parametri indica quanti fogli dentro al folder
                          // il secondo parametro indica quanti fogli per riga
dw_folder.fu_SelectTab(1)


end event

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW(dw_non_conformita_approvvigionamenti,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_approvvigionamenti,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome + ' ' + nome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDLB_DW(dw_non_conformita_approvvigionamenti,"anno_bolla_acq",sqlca,&
//                 "tes_bol_acq","anno_bolla_acq","anno_bolla_acq",&
//                 "(tes_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//                 "(tes_bol_acq.cod_fornitore = '"+ s_cs_xx.parametri.parametro_s_1 + "')")
//
//f_PO_LoadDDLB_DW(dw_non_conformita_approvvigionamenti,"anno_registrazione_ord_acq",sqlca,&
//                 "tes_ord_acq","anno_registrazione","anno_registrazione",&
//                 "(tes_ord_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//                 "(tes_ord_acq.cod_fornitore = '"+ s_cs_xx.parametri.parametro_s_1 + "')")

f_PO_LoadDDLB_DW(dw_non_conformita_approvvigionamenti,"anno_reg_collaudi",sqlca,&
                 "collaudi","anno_registrazione","anno_registrazione",&
                 "collaudi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_non_conformita_approvv.create
int iCurrent
call super::create
this.cb_gen_richiesta=create cb_gen_richiesta
this.dw_non_conformita_approvvigionamenti=create dw_non_conformita_approvvigionamenti
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_gen_richiesta
this.Control[iCurrent+2]=this.dw_non_conformita_approvvigionamenti
this.Control[iCurrent+3]=this.dw_folder
end on

on w_non_conformita_approvv.destroy
call super::destroy
destroy(this.cb_gen_richiesta)
destroy(this.dw_non_conformita_approvvigionamenti)
destroy(this.dw_folder)
end on

type cb_gen_richiesta from commandbutton within w_non_conformita_approvv
integer x = 2057
integer y = 1400
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Richiesta"
end type

event clicked;long ll_anno_nc, ll_num_nc, ll_risposta

if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")) then
   s_cs_xx.parametri.parametro_d_1 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")
else
   return
end if

if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"num_non_conf")) then
   s_cs_xx.parametri.parametro_d_2 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"num_non_conf")
else
   return
end if

select anno_non_conf, 
       num_non_conf  
into   :ll_anno_nc, :ll_num_nc
from   non_conformita  
where  cod_azienda   = :s_cs_xx.cod_azienda and
       anno_non_conf = :s_cs_xx.parametri.parametro_d_1 and
       num_non_conf  = :s_cs_xx.parametri.parametro_d_2 ;

if sqlca.sqlcode = 0 then
   ll_risposta = g_mb.messagebox("Nuova Richiesta","Apro un nuova richiesta dalla Non Conformità "+string(ll_anno_nc) + " / " + string(ll_num_nc) +"  ?",Question!, YesNo!, 2)
   if ll_risposta = 1 then
      s_cs_xx.parametri.parametro_s_1 = "GEN_RICHIESTA"
      window_open(w_call_center, -1)
   end if
end if
end event

type dw_non_conformita_approvvigionamenti from uo_cs_xx_dw within w_non_conformita_approvv
integer x = 46
integer y = 140
integer width = 2400
integer height = 1200
integer taborder = 80
string dataobject = "d_non_conformita_approvv"
boolean border = false
end type

event pcd_view;call super::pcd_view;dw_non_conformita_approvvigionamenti.object.b_ricerca_collaudo.enabled = false
dw_non_conformita_approvvigionamenti.object.b_ricerca_prodotto.enabled = false
dw_non_conformita_approvvigionamenti.object.b_ricerca_bolla.enabled = false
dw_non_conformita_approvvigionamenti.object.b_ricerca_deposito.enabled = false
end event

event pcd_new;call super::pcd_new;dw_non_conformita_approvvigionamenti.object.b_ricerca_collaudo.enabled = true
dw_non_conformita_approvvigionamenti.object.b_ricerca_prodotto.enabled = true
dw_non_conformita_approvvigionamenti.object.b_ricerca_bolla.enabled = true
dw_non_conformita_approvvigionamenti.object.b_ricerca_deposito.enabled = true
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_deposito, ls_cod_prodotto, ls_cod_ubicazione, ls_cod_lotto
datetime   ld_data_stock
long   ll_prog_stock, ll_qta_stock, ll_qta_nc

ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
ls_cod_deposito = getitemstring(getrow(),"cod_deposito")
ls_cod_ubicazione = getitemstring(getrow(),"cod_ubicazione")
ls_cod_lotto = getitemstring(getrow(),"cod_lotto")
ld_data_stock = getitemdatetime(getrow(),"data_stock")
ll_prog_stock = getitemnumber(getrow(),"prog_stock")
ll_qta_nc = getitemnumber(getrow(),"quan_non_conformita")


SELECT stock.quan_assegnata  
INTO   :ll_qta_stock  
FROM   stock  
WHERE  (stock.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       (stock.cod_prodotto = :ls_cod_prodotto ) AND  
       (stock.cod_deposito = :ls_cod_deposito ) AND  
       (stock.cod_ubicazione = :ls_cod_ubicazione ) AND  
       (stock.cod_lotto = :ls_cod_lotto ) AND  
       (stock.data_stock = :ld_data_stock ) AND  
       (stock.prog_stock = :ll_prog_stock )   ;

if (sqlca.sqlcode = 0) and (ll_qta_stock > 0) and (ll_qta_nc <> ll_qta_stock) then
   g_mb.messagebox("Non Conformità","Attenzione: la quantità stock non corrisponde con la quantità non conformtà!", Information!)
end if

end event

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

string ls_cod_prod_fornitore, ls_prodotto, ls_fornitore, ls_null
string ls_str1, ls_str2, ls_str3
long   ll_null
datetime   ld_null
double ld_progressivo

setnull(ls_null)
setnull(ll_null)
setnull(ld_null)


choose case i_colname

case "cod_prodotto"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   if i_coltext <> ls_prodotto  then
      g_mb.messagebox("Non Conformità Interna","Prodotto variato: devi reimpostare stock !",Exclamation!)
      this.setitem(this.getrow(), "cod_deposito", ls_null)
      this.setitem(this.getrow(), "cod_ubicazione", ls_null)
      this.setitem(this.getrow(), "cod_lotto", ls_null)
      this.setitem(this.getrow(), "data_stock", ld_null)
      this.setitem(this.getrow(), "prog_stock", ll_null)
      this.setitem(this.getrow(), "anno_reg_collaudi", ll_null)
      this.setitem(this.getrow(), "num_reg_collaudi", ll_null)
   end if
   ls_prodotto = i_coltext
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")

case "cod_deposito"
   if not isnull(i_coltext) then
      f_PO_LoadDDLB_DW(dw_non_conformita_approvvigionamenti, &
                       "cod_ubicazione", &
                       SQLCA, &
                       "stock", &
                       "cod_ubicazione" , &
                       "cod_ubicazione" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (stock.cod_deposito = '" + i_coltext + "')")
   end if

case "cod_ubicazione"
   if not isnull(i_coltext) then
      f_PO_LoadDDLB_DW(dw_non_conformita_approvvigionamenti, &
                       "cod_lotto", &
                       SQLCA, &
                       "stock", &
                       "cod_lotto" , &
                       "cod_lotto" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
                       "(stock.cod_deposito = '" + this.getitemstring(this.getrow(),"cod_ubicazione") + "') and " + &
                       "(stock.cod_ubicazione = '" + i_coltext + "')")
   end if

case "anno_bolla_acq"
//   ls_str1 = this.getitemstring(this.getrow(), "cod_fornitore")
//   if isnull(ls_str1) then ls_str1 = "%"
//   this.setitem(this.getrow(), "num_bolla_acq", ll_null)
//   this.setitem(this.getrow(), "prog_riga_bolla_acq", ll_null)
//   f_PO_LoadDDDW_DW(dw_non_conformita_approvvigionamenti,"num_bolla_acq",sqlca,&
//              "tes_bol_acq","num_bolla_acq","num_bolla_acq",&
//              "(tes_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//              "(tes_bol_acq.anno_bolla_acq = "+i_coltext+") and " + &
//              "(tes_bol_acq.cod_fornitore like '"+ls_str1+"')" )

case "num_bolla_acq"
//   SELECT tes_bol_acq.progressivo
//   INTO   :ld_progressivo
//   FROM   tes_bol_acq
//   WHERE  (tes_bol_acq.cod_azienda = :s_cs_xx.cod_azienda) and
//               (tes_bol_acq.anno_bolla_acq = :ls_str2) and
//               (tes_bol_acq.cod_fornitore like :ls_str1) and
//               (tes_bol_acq.num_bolla_acq = :i_coltext) ;
//
////   ls_str3 = string(int(ld_progressivo))
//   ls_str1 = this.getitemstring(this.getrow(), "cod_fornitore")
//   if isnull(ls_str1) then ls_str1 = "%"
//   ls_str2 = string(int(this.getitemnumber(this.getrow(), "anno_bolla_acq")))
////   this.setitem(this.getrow(), "num_bolla_acq", ll_null)
////   this.setitem(this.getrow(), "prog_riga_bolla_acq", ll_null)
//   f_PO_LoadDDLB_DW(dw_non_conformita_approvvigionamenti,"prog_riga_bolla_acq",sqlca,&
//              "det_bol_acq","prog_riga_bolla_acq","prog_riga_bolla_acq",&
//              "(det_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//              "(det_bol_acq.anno_bolla_acq = "+ls_str2+") and " + &
//              "(det_bol_acq.num_bolla_acq = "+i_coltext+") " )
//// (det_bol_acq.progressivo = "+ls_str3+")  and &					

//------------------------------- Inserimento Nicola ---------------------------------------
case "anno_registrazione_ord_acq"
   ls_str1 = this.getitemstring(this.getrow(), "cod_fornitore")
   if isnull(ls_str1) then ls_str1 = "%"
   this.setitem(this.getrow(), "num_registrazione_ord_acq", ll_null)
   this.setitem(this.getrow(), "prog_riga_ordine_acq", ll_null)
   f_PO_LoadDDDW_DW(dw_non_conformita_approvvigionamenti,"num_registrazione_ord_acq",sqlca,&
              "tes_ord_acq","num_registrazione","num_registrazione",&
              "(tes_ord_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
              "(tes_ord_acq.anno_registrazione = "+i_coltext + ") and " + &
              "(tes_ord_acq.cod_fornitore like '" + ls_str1 + "')" )

case "num_registrazione_ord_acq"

   ls_str1 = this.getitemstring(this.getrow(), "cod_fornitore")
   if isnull(ls_str1) then ls_str1 = "%"
   ls_str2 = string(int(this.getitemnumber(this.getrow(), "anno_registrazione_ord_acq")))
   f_PO_LoadDDLB_DW(dw_non_conformita_approvvigionamenti,"prog_riga_ordine_acq",sqlca,&
              "det_ord_acq","prog_riga_ordine_acq","prog_riga_ordine_acq",&
              "(det_ord_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
              "(det_ord_acq.anno_registrazione = "+ls_str2+") and " + &
              "(det_ord_acq.num_registrazione = "+i_coltext+") " )
					
case "anno_reg_collaudi"
   this.setitem(this.getrow(), "num_reg_collaudi", ll_null)
   f_PO_LoadDDDW_DW(dw_non_conformita_approvvigionamenti,"num_reg_collaudi",sqlca,&
              "collaudi","num_registrazione","num_registrazione",&
              "(collaudi.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
              "(collaudi.anno_registrazione = " + i_coltext + ")" )					

//---------------------------------- Fine Mod. -------------------------------------------
case "cod_fornitore"
   this.setitem(this.getrow(), "anno_bolla_acq", ll_null)
   this.setitem(this.getrow(), "num_bolla_acq", ll_null)
   this.setitem(this.getrow(), "prog_riga_bolla_acq", ll_null)
   ls_str1 = i_coltext
   if isnull(ls_str1) then ls_str1 = "%"
   f_PO_LoadDDLB_DW(dw_non_conformita_approvvigionamenti,"anno_registrazione_bol_acq",sqlca,&
                 "tes_bol_acq","anno_bolla_acq","anno_bolla_acq",&
                 "(tes_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
                 "(tes_bol_acq.cod_fornitore like '"+ ls_str1 + "')")
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   ls_fornitore =i_coltext

case "cod_prod_fornitore"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")


end choose


if ( (i_colname = "cod_prod_fornitore") or (i_colname = "cod_prodotto") or (i_colname = "cod_fornitore") ) and &
   (not(isnull(ls_fornitore))) and &
   (not(isnull(ls_prodotto)))  then

   select tab_prod_fornitori.cod_prod_fornitore
   into   :ls_cod_prod_fornitore
   from   tab_prod_fornitori
   where  (tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and
          (tab_prod_fornitori.cod_prodotto = :ls_prodotto) and
          (tab_prod_fornitori.cod_fornitore = :ls_fornitore) ;

 	if sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_cod_prod_fornitore)
   else
      this.setitem(this.getrow(), "cod_prod_fornitore", "Non Indicato")
   end if
end if

end if   // si riferisce alla condizione iniziale di test della variabile i_extendmode
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_d_1, s_cs_xx.parametri.parametro_d_2)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

event pcd_modify;call super::pcd_modify;dw_non_conformita_approvvigionamenti.object.b_ricerca_collaudo.enabled = true
dw_non_conformita_approvvigionamenti.object.b_ricerca_prodotto.enabled = true
dw_non_conformita_approvvigionamenti.object.b_ricerca_bolla.enabled = true
dw_non_conformita_approvvigionamenti.object.b_ricerca_deposito.enabled = true
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_non_conformita_approvvigionamenti,"cod_prodotto")
	case "b_ricerca_deposito"
		guo_ricerca.uof_ricerca_deposito(dw_non_conformita_approvvigionamenti,"cod_deposito")
		
	case "b_ricerca_collaudo"
		wf_collaudo_ric()
		
	case "b_ricerca_bolla"
		wf_ric_bol_acq()
		
end choose
end event

type dw_folder from u_folder within w_non_conformita_approvv
integer x = 23
integer y = 20
integer width = 2446
integer height = 1360
integer taborder = 90
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Approvvigionamenti"
      SetFocus(dw_non_conformita_approvvigionamenti)
END CHOOSE

end on

