﻿$PBExportHeader$w_non_conformita_interna.srw
$PBExportComments$Finestra Input Dati Specifici per Non Conformita Tipo Interna
forward
global type w_non_conformita_interna from w_cs_xx_principale
end type
type cb_gen_richiesta from commandbutton within w_non_conformita_interna
end type
type cb_collaudi_ric from commandbutton within w_non_conformita_interna
end type
type cb_ricerca_lavorazioni from commandbutton within w_non_conformita_interna
end type
type cb_manutenzione_ric from commandbutton within w_non_conformita_interna
end type
type cb_ricerca_stock from cb_stock_ricerca within w_non_conformita_interna
end type
type dw_non_conformita_interna_lista from uo_cs_xx_dw within w_non_conformita_interna
end type
type dw_non_conformita_interna_2 from uo_cs_xx_dw within w_non_conformita_interna
end type
type dw_non_conformita_interna_1 from uo_cs_xx_dw within w_non_conformita_interna
end type
type dw_folder from u_folder within w_non_conformita_interna
end type
end forward

global type w_non_conformita_interna from w_cs_xx_principale
integer width = 2537
integer height = 1372
string title = "Dati Specifici Non Conformita Interna"
cb_gen_richiesta cb_gen_richiesta
cb_collaudi_ric cb_collaudi_ric
cb_ricerca_lavorazioni cb_ricerca_lavorazioni
cb_manutenzione_ric cb_manutenzione_ric
cb_ricerca_stock cb_ricerca_stock
dw_non_conformita_interna_lista dw_non_conformita_interna_lista
dw_non_conformita_interna_2 dw_non_conformita_interna_2
dw_non_conformita_interna_1 dw_non_conformita_interna_1
dw_folder dw_folder
end type
global w_non_conformita_interna w_non_conformita_interna

type variables
boolean ib_delete=true
string is_prodotto, is_tipo_requisito
end variables

event pc_setwindow;call super::pc_setwindow;dw_non_conformita_interna_lista.set_dw_options(sqlca,pcca.null_object, c_modifyonopen + c_nodelete + c_nonew, c_default)
dw_non_conformita_interna_1.set_dw_options(sqlca,dw_non_conformita_interna_lista,c_modifyonopen + c_nodelete + c_nonew + c_sharedata + c_scrollparent,c_default)
dw_non_conformita_interna_2.set_dw_options(sqlca,dw_non_conformita_interna_lista,c_modifyonopen + c_nodelete + c_nonew + c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_non_conformita_interna_lista

windowobject l_objects[ ]

l_objects[1] = dw_non_conformita_interna_2
l_objects[2] = cb_collaudi_ric
l_objects[3] = cb_manutenzione_ric
dw_folder.fu_AssignTab(2, "Dati Interni", l_Objects[])
l_objects[1] = dw_non_conformita_interna_1
l_objects[2] = cb_ricerca_stock
l_objects[3] = cb_ricerca_lavorazioni
dw_folder.fu_AssignTab(1, "Anagrafici", l_Objects[])

dw_folder.fu_FolderCreate(2,4)
dw_folder.fu_SelectTab(1)     
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_non_conformita_interna_2,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_interna_2,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_interna_1,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_interna_1,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on w_non_conformita_interna.create
int iCurrent
call super::create
this.cb_gen_richiesta=create cb_gen_richiesta
this.cb_collaudi_ric=create cb_collaudi_ric
this.cb_ricerca_lavorazioni=create cb_ricerca_lavorazioni
this.cb_manutenzione_ric=create cb_manutenzione_ric
this.cb_ricerca_stock=create cb_ricerca_stock
this.dw_non_conformita_interna_lista=create dw_non_conformita_interna_lista
this.dw_non_conformita_interna_2=create dw_non_conformita_interna_2
this.dw_non_conformita_interna_1=create dw_non_conformita_interna_1
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_gen_richiesta
this.Control[iCurrent+2]=this.cb_collaudi_ric
this.Control[iCurrent+3]=this.cb_ricerca_lavorazioni
this.Control[iCurrent+4]=this.cb_manutenzione_ric
this.Control[iCurrent+5]=this.cb_ricerca_stock
this.Control[iCurrent+6]=this.dw_non_conformita_interna_lista
this.Control[iCurrent+7]=this.dw_non_conformita_interna_2
this.Control[iCurrent+8]=this.dw_non_conformita_interna_1
this.Control[iCurrent+9]=this.dw_folder
end on

on w_non_conformita_interna.destroy
call super::destroy
destroy(this.cb_gen_richiesta)
destroy(this.cb_collaudi_ric)
destroy(this.cb_ricerca_lavorazioni)
destroy(this.cb_manutenzione_ric)
destroy(this.cb_ricerca_stock)
destroy(this.dw_non_conformita_interna_lista)
destroy(this.dw_non_conformita_interna_2)
destroy(this.dw_non_conformita_interna_1)
destroy(this.dw_folder)
end on

type cb_gen_richiesta from commandbutton within w_non_conformita_interna
integer x = 2121
integer y = 1176
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Richiesta"
end type

event clicked;long ll_anno_nc, ll_num_nc, ll_risposta

if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")) then
   s_cs_xx.parametri.parametro_d_1 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")
else
   return
end if

if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"num_non_conf")) then
   s_cs_xx.parametri.parametro_d_2 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"num_non_conf")
else
   return
end if

select anno_non_conf, 
       num_non_conf  
into   :ll_anno_nc, :ll_num_nc
from   non_conformita  
where  cod_azienda   = :s_cs_xx.cod_azienda and
       anno_non_conf = :s_cs_xx.parametri.parametro_d_1 and
       num_non_conf  = :s_cs_xx.parametri.parametro_d_2 ;

if sqlca.sqlcode = 0 then
   ll_risposta = g_mb.messagebox("Nuova Richiesta","Apro un nuova richiesta dalla Non Conformità "+string(ll_anno_nc) + " / " + string(ll_num_nc) +"  ?",Question!, YesNo!, 2)
   if ll_risposta = 1 then
      s_cs_xx.parametri.parametro_s_1 = "GEN_RICHIESTA"
      window_open(w_call_center, -1)
   end if
end if
end event

type cb_collaudi_ric from commandbutton within w_non_conformita_interna
integer x = 1989
integer y = 540
integer width = 69
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_lotto")
s_cs_xx.parametri.parametro_data_1 = dw_non_conformita_interna_1.getitemdatetime(dw_non_conformita_interna_1.getrow(),"data_stock")
s_cs_xx.parametri.parametro_d_1 = dw_non_conformita_interna_1.getitemnumber(dw_non_conformita_interna_1.getrow(),"prog_stock")

dw_non_conformita_interna_2.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "anno_reg_collaudi"
s_cs_xx.parametri.parametro_s_2 = "num_reg_collaudi"
s_cs_xx.parametri.parametro_s_3 = "cod_deposito"
s_cs_xx.parametri.parametro_s_4 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_5 = "cod_lotto"
s_cs_xx.parametri.parametro_s_6 = "data_stock"
s_cs_xx.parametri.parametro_s_7 = "prog_stock"
s_cs_xx.parametri.parametro_s_8 = "cod_prodotto"

window_open(w_ricerca_collaudo, 0)

end event

type cb_ricerca_lavorazioni from commandbutton within w_non_conformita_interna
integer x = 2240
integer y = 880
integer width = 73
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;dw_non_conformita_interna_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_lavorazione"
s_cs_xx.parametri.parametro_s_2 = "cod_reparto"
s_cs_xx.parametri.parametro_s_3 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_reparto")
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Lavorazioni","Prodotto Mancante: la ricerca verrà effettuata su un numero elevato di elementi",Information!)
end if
window_open(w_ricerca_lavorazione, 0)
end on

type cb_manutenzione_ric from commandbutton within w_non_conformita_interna
integer x = 1189
integer y = 260
integer width = 69
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;dw_non_conformita_interna_2.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "num_reg_manutenzione"
s_cs_xx.parametri.parametro_s_2 = "cod_utente"
s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_interna_2.getitemstring(dw_non_conformita_interna_2.getrow(),"cod_attrezzatura")
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Manutenzione","Codice attrezzatura mancante",Stopsign!)
   return
end if

window_open(w_ricerca_manutenzione, 0)
end event

type cb_ricerca_stock from cb_stock_ricerca within w_non_conformita_interna
integer x = 2217
integer y = 160
integer width = 73
integer height = 80
integer taborder = 30
end type

on clicked;call cb_stock_ricerca::clicked;s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_non_conformita_interna_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)
end on

type dw_non_conformita_interna_lista from uo_cs_xx_dw within w_non_conformita_interna
integer x = 55
integer y = 1428
integer width = 709
integer height = 160
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_non_conformita_interna_lista"
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;cb_collaudi_ric.enabled = true
dw_non_conformita_interna_2.object.b_ricerca_commessa.enabled = true
dw_non_conformita_interna_1.object.b_ricerca_cliente.enabled = true
dw_non_conformita_interna_1.object.b_ricerca_fornitore.enabled = true
dw_non_conformita_interna_1.object.b_ricerca_prodotto.enabled = true
cb_ricerca_lavorazioni.enabled = true
cb_ricerca_stock.enabled = true
cb_manutenzione_ric.enabled = true



end event

event pcd_view;call super::pcd_view;cb_collaudi_ric.enabled = false
dw_non_conformita_interna_2.object.b_ricerca_commessa.enabled = false
dw_non_conformita_interna_1.object.b_ricerca_cliente.enabled = false
dw_non_conformita_interna_1.object.b_ricerca_fornitore.enabled = false
dw_non_conformita_interna_1.object.b_ricerca_prodotto.enabled = false
cb_ricerca_lavorazioni.enabled = false
cb_ricerca_stock.enabled = false
cb_manutenzione_ric.enabled = false



end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_deposito, ls_cod_prodotto, ls_cod_ubicazione, ls_cod_lotto
datetime   ld_data_stock
long   ll_prog_stock, ll_qta_stock, ll_qta_nc

ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
ls_cod_deposito = getitemstring(getrow(),"cod_deposito")
ls_cod_ubicazione = getitemstring(getrow(),"cod_ubicazione")
ls_cod_lotto = getitemstring(getrow(),"cod_lotto")
ld_data_stock = getitemdatetime(getrow(),"data_stock")
ll_prog_stock = getitemnumber(getrow(),"prog_stock")
ll_qta_nc = getitemnumber(getrow(),"quan_non_conformita")


SELECT stock.quan_assegnata  
INTO   :ll_qta_stock  
FROM   stock  
WHERE  (stock.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       (stock.cod_prodotto = :ls_cod_prodotto ) AND  
       (stock.cod_deposito = :ls_cod_deposito ) AND  
       (stock.cod_ubicazione = :ls_cod_ubicazione ) AND  
       (stock.cod_lotto = :ls_cod_lotto ) AND  
       (stock.data_stock = :ld_data_stock ) AND  
       (stock.prog_stock = :ll_prog_stock )   ;

if (sqlca.sqlcode = 0) and (ll_qta_stock > 0) and (ll_qta_nc <> ll_qta_stock) then
   g_mb.messagebox("Non Conformità","Attenzione: la quantità stock non corrisponde con la quantità non conformtà!", Information!)
end if

end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_d_1, s_cs_xx.parametri.parametro_d_2)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;//LONG  l_Idx
//
//FOR l_Idx = 1 TO RowCount()
//   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
//      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
//   END IF
//   IF IsNull(GetItemstring(l_Idx, "anno_non_conf")) THEN
//      SetItem(l_Idx, "anno_non_conf", s_cs_xx.parametri.parametro_d_1)
//   END IF
//   IF IsNull(GetItemstring(l_Idx, "num_non_conf")) THEN
//      SetItem(l_Idx, "num_non_conf", s_cs_xx.parametri.parametro_d_2)
//   END IF
//NEXT
//
end on

event pcd_modify;call super::pcd_modify;cb_collaudi_ric.enabled = true
dw_non_conformita_interna_2.object.b_ricerca_commessa.enabled = true
dw_non_conformita_interna_1.object.b_ricerca_cliente.enabled = true
dw_non_conformita_interna_1.object.b_ricerca_fornitore.enabled = true
dw_non_conformita_interna_1.object.b_ricerca_prodotto.enabled = true
cb_ricerca_lavorazioni.enabled = true
cb_ricerca_stock.enabled = true
cb_manutenzione_ric.enabled = true







end event

event getfocus;call super::getfocus;dw_non_conformita_interna_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "anno_commessa"
s_cs_xx.parametri.parametro_s_2 = "num_commessa"
s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_prodotto")

end event

type dw_non_conformita_interna_2 from uo_cs_xx_dw within w_non_conformita_interna
integer x = 69
integer y = 120
integer width = 2354
integer height = 1000
integer taborder = 130
string dataobject = "d_non_conformita_interna_2"
boolean border = false
end type

on itemchanged;call uo_cs_xx_dw::itemchanged;if not(i_extendmode) then return

string ls_attrezzatura
if i_colname = "cod_attrezzatura" then
   ls_attrezzatura = this.getitemstring(this.getrow(), "cod_attrezzatura")
   if i_coltext <> ls_attrezzatura  then
      g_mb.messagebox("Non Conformità Interna","Attrezzatura variata: azzero il numero registraz. manutenzione",Exclamation!)
      this.setitem(this.getrow(), "num_reg_manutenzione", 0)
   end if
end if
end on

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_commessa"
		guo_ricerca.uof_ricerca_commessa(dw_non_conformita_interna_2,"anno_commessa","num_commessa")
end choose
end event

type dw_non_conformita_interna_1 from uo_cs_xx_dw within w_non_conformita_interna
integer x = 46
integer y = 140
integer width = 2377
integer height = 920
integer taborder = 110
string dataobject = "d_non_conformita_interna_1"
boolean border = false
end type

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then

string ls_cod_prod_fornitore, ls_prodotto, ls_fornitore, ls_cod_reparto, ls_cod_prodotto
string ls_null
datetime   ld_null
long   ll_null

setnull(ls_null)
setnull(ld_null)
setnull(ll_null)

if i_colname = "cod_prodotto" then
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   if i_coltext <> ls_prodotto  then
      g_mb.messagebox("Non Conformità Interna","Prodotto variato: devi reimpostare STOCK, LAVORAZIONI, COLLAUDI !",Exclamation!)
      this.setitem(this.getrow(), "cod_reparto", ls_null)
      this.setitem(this.getrow(), "cod_lavorazione", ls_null)
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
      dw_non_conformita_interna_2.setitem(dw_non_conformita_interna_2.getrow(), "anno_reg_collaudi", 0)
      dw_non_conformita_interna_2.setitem(dw_non_conformita_interna_2.getrow(), "num_reg_collaudi", 0)
   end if
end if

if i_colname = "cod_fornitore" then
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")
   if i_coltext <> ls_fornitore  then
      g_mb.messagebox("Non Conformità Interna","Fornitore variato: devi reimpostare prodotto fornitore",Exclamation!)
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
   end if
end if

choose case i_colname
case "cod_prodotto"
   ls_prodotto = i_coltext
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")
case "cod_fornitore"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   ls_fornitore =i_coltext
case "cod_prod_fornitore"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")
end choose


if ( (i_colname = "cod_prod_fornitore") or (i_colname = "cod_prodotto") or (i_colname = "cod_fornitore") ) and &
   (not(isnull(ls_fornitore))) and &
   (not(isnull(ls_prodotto)))  then

   select tab_prod_fornitori.cod_prod_fornitore
   into   :ls_cod_prod_fornitore
   from   tab_prod_fornitori
   where  (tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and
          (tab_prod_fornitori.cod_prodotto = :ls_prodotto) and
          (tab_prod_fornitori.cod_fornitore = :ls_fornitore) ;

 	if sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_cod_prod_fornitore)
   else
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
   end if
end if


if i_colname = "cod_deposito" then
   this.setitem(this.getrow(), "cod_ubicazione", ls_null)
   this.setitem(this.getrow(), "cod_lotto", ls_null)
   this.setitem(this.getrow(), "data_stock", ld_null)
   this.setitem(this.getrow(), "prog_stock", ll_null)
   if not isnull(i_coltext) then
      f_PO_LoadDDLB_DW(dw_non_conformita_interna_1, &
                       "cod_ubicazione", &
                       SQLCA, &
                       "stock", &
                       "cod_ubicazione" , &
                       "cod_ubicazione" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (stock.cod_deposito = '" + i_coltext + "')")
   end if
end if

if i_colname = "cod_ubicazione" then
   this.setitem(this.getrow(), "cod_lotto", ls_null)
   this.setitem(this.getrow(), "data_stock", ld_null)
   this.setitem(this.getrow(), "prog_stock", ll_null)

   if not isnull(i_coltext) then
      f_PO_LoadDDLB_DW(dw_non_conformita_interna_1, &
                       "cod_lotto", &
                       SQLCA, &
                       "stock", &
                       "cod_lotto" , &
                       "cod_lotto" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (stock.cod_deposito = '" + this.getitemstring(this.getrow(),"cod_ubicazione") + "') and &
                        (stock.cod_ubicazione = '" + i_coltext + "')")
   end if
end if

if i_colname = "cod_lotto" then
   this.setitem(this.getrow(), "data_stock", ld_null)
   this.setitem(this.getrow(), "prog_stock", ll_null)
end if
//////

if i_colname = "cod_reparto" then
   ls_cod_reparto = this.getitemstring(this.getrow(), "cod_reparto")
   if isnull(ls_cod_prodotto) then ls_cod_prodotto = " "
   if (i_coltext <> ls_cod_reparto) and (not isnull(i_coltext)) then
      g_mb.messagebox("Non Conformita Sistema Qualità", "Reparto variato: devi reimpostare la lavorazione !", Information!)
      this.setitem(this.getrow(), "cod_lavorazione", ls_null)
   else
      if isnull(i_coltext) then
         g_mb.messagebox("Non Conformita Sistema Qualità", "Reparto azzerato: devi reimpostare anche la lavorazione !", Information!)
         this.setitem(this.getrow(), "cod_lavorazione", ls_null)
      end if
   end if
end if

// ------------------------  caricamento dinamico ListBox + DDDW -----------------------------

if i_colname = "cod_prodotto" then
   if not isnull(i_coltext) then
      f_PO_LoadDDDW_DW(dw_non_conformita_interna_1, &
                       "cod_reparto", &
                       SQLCA, &
                       "anag_reparti", &
                       "cod_reparto" , &
                       "des_reparto" , &
                       "(anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (anag_reparti.cod_reparto in (SELECT tes_fasi_lavorazione.cod_reparto FROM   tes_fasi_lavorazione WHERE  (tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_fasi_lavorazione.cod_prodotto = '" + i_coltext + "')))")
                           
   end if
end if


if i_colname = "cod_reparto" then
   if (not isnull(i_coltext)) and (not isnull( this.getitemstring(this.getrow(), "cod_prodotto") )) then
      f_PO_LoadDDLB_DW(dw_non_conformita_interna_1, &
                       "cod_lavorazione", &
                       SQLCA, &
                       "tes_fasi_lavorazione", &
                       "cod_lavorazione" , &
                       "cod_lavorazione" , &
                       "(tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (tes_fasi_lavorazione.cod_prodotto = '" + this.getitemstring(this.getrow(), "cod_prodotto") + "') and &
                        (tes_fasi_lavorazione.cod_reparto = '" + i_coltext + "')" )
   end if
end if

end if   // si riferisce alla condizione iniziale di test della variabile i_extendmode

end event

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

string ls_cod_prod_fornitore, ls_prodotto, ls_fornitore, ls_cod_reparto, ls_cod_prodotto
string ls_null
datetime   ld_null
long   ll_null

setnull(ls_null)
setnull(ld_null)
setnull(ll_null)

if i_colname = "cod_prodotto" then
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   if i_coltext <> ls_prodotto  then
      g_mb.messagebox("Non Conformità Interna","Prodotto variato: devi reimpostare STOCK, LAVORAZIONI, COLLAUDI !",Exclamation!)
      this.setitem(this.getrow(), "cod_reparto", ls_null)
      this.setitem(this.getrow(), "cod_lavorazione", ls_null)
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
      dw_non_conformita_interna_2.setitem(dw_non_conformita_interna_2.getrow(), "anno_reg_collaudi", 0)
      dw_non_conformita_interna_2.setitem(dw_non_conformita_interna_2.getrow(), "num_reg_collaudi", 0)
   end if
end if

if i_colname = "cod_fornitore" then
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")
   if i_coltext <> ls_fornitore  then
      g_mb.messagebox("Non Conformità Interna","Fornitore variato: devi reimpostare prodotto fornitore",Exclamation!)
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
   end if
end if

choose case i_colname
case "cod_prodotto"
   ls_prodotto = i_coltext
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")
case "cod_fornitore"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   ls_fornitore =i_coltext
case "cod_prod_fornitore"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")
end choose


if ( (i_colname = "cod_prod_fornitore") or (i_colname = "cod_prodotto") or (i_colname = "cod_fornitore") ) and &
   (not(isnull(ls_fornitore))) and &
   (not(isnull(ls_prodotto)))  then

   select tab_prod_fornitori.cod_prod_fornitore
   into   :ls_cod_prod_fornitore
   from   tab_prod_fornitori
   where  (tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and
          (tab_prod_fornitori.cod_prodotto = :ls_prodotto) and
          (tab_prod_fornitori.cod_fornitore = :ls_fornitore) ;

 	if sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_cod_prod_fornitore)
   else
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
   end if
end if


if i_colname = "cod_deposito" then
   this.setitem(this.getrow(), "cod_ubicazione", ls_null)
   this.setitem(this.getrow(), "cod_lotto", ls_null)
//   this.setitem(this.getrow(), "data_stock", ld_null)
//   this.setitem(this.getrow(), "prog_stock", ll_null)
   if not isnull(i_coltext) then
      f_PO_LoadDDLB_DW(dw_non_conformita_interna_1, &
                       "cod_ubicazione", &
                       SQLCA, &
                       "stock", &
                       "cod_ubicazione" , &
                       "cod_ubicazione" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (stock.cod_deposito = '" + i_coltext + "')")
   end if
end if

if i_colname = "cod_ubicazione" then
   this.setitem(this.getrow(), "cod_lotto", ls_null)
//   this.setitem(this.getrow(), "data_stock", ld_null)
//   this.setitem(this.getrow(), "prog_stock", ll_null)
   if not isnull(i_coltext) then
      f_PO_LoadDDLB_DW(dw_non_conformita_interna_1, &
                       "cod_lotto", &
                       SQLCA, &
                       "stock", &
                       "cod_lotto" , &
                       "cod_lotto" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (stock.cod_deposito = '" + this.getitemstring(this.getrow(),"cod_ubicazione") + "') and &
                        (stock.cod_ubicazione = '" + i_coltext + "')")
   end if
end if

//if i_colname = "cod_lotto" then
//   this.setitem(this.getrow(), "data_stock", ld_null)
//   this.setitem(this.getrow(), "prog_stock", ll_null)
//end if
//////

if i_colname = "cod_reparto" then
   ls_cod_reparto = this.getitemstring(this.getrow(), "cod_reparto")
   if isnull(ls_cod_prodotto) then ls_cod_prodotto = " "
   if (i_coltext <> ls_cod_reparto) and (not isnull(i_coltext)) then
      g_mb.messagebox("Non Conformita Sistema Qualità", "Reparto variato: devi reimpostare la lavorazione !", Information!)
      this.setitem(this.getrow(), "cod_lavorazione", ls_null)
   else
      if isnull(i_coltext) then
         g_mb.messagebox("Non Conformita Sistema Qualità", "Reparto azzerato: devi reimpostare anche la lavorazione !", Information!)
         this.setitem(this.getrow(), "cod_lavorazione", ls_null)
      end if
   end if
end if

// ------------------------  caricamento dinamico ListBox + DDDW -----------------------------

if i_colname = "cod_prodotto" then
   if not isnull(i_coltext) then
      f_PO_LoadDDDW_DW(dw_non_conformita_interna_1, &
                       "cod_reparto", &
                       SQLCA, &
                       "anag_reparti", &
                       "cod_reparto" , &
                       "des_reparto" , &
                       "(anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (anag_reparti.cod_reparto in (SELECT tes_fasi_lavorazione.cod_reparto FROM   tes_fasi_lavorazione WHERE  (tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_fasi_lavorazione.cod_prodotto = '" + i_coltext + "')))")
                           
   end if
end if


if i_colname = "cod_reparto" then
   if (not isnull(i_coltext)) and (not isnull( this.getitemstring(this.getrow(), "cod_prodotto") )) then
      f_PO_LoadDDLB_DW(dw_non_conformita_interna_1, &
                       "cod_lavorazione", &
                       SQLCA, &
                       "tes_fasi_lavorazione", &
                       "cod_lavorazione" , &
                       "cod_lavorazione" , &
                       "(tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (tes_fasi_lavorazione.cod_prodotto = '" + this.getitemstring(this.getrow(), "cod_prodotto") + "') and &
                        (tes_fasi_lavorazione.cod_reparto = '" + i_coltext + "')" )
   end if
end if

end if   // si riferisce alla condizione iniziale di test della variabile i_extendmode

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_non_conformita_interna_1,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_non_conformita_interna_1,"cod_prodotto")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_non_conformita_interna_1,"cod_fornitore")
end choose
end event

type dw_folder from u_folder within w_non_conformita_interna
integer x = 23
integer y = 20
integer width = 2469
integer height = 1140
integer taborder = 140
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Anagrafici"
      SetFocus(dw_non_conformita_interna_1)
   CASE "Dati Interni"
      SetFocus(dw_non_conformita_interna_2)
END CHOOSE



end on

