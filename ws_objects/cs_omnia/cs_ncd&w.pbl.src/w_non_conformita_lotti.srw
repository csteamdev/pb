﻿$PBExportHeader$w_non_conformita_lotti.srw
forward
global type w_non_conformita_lotti from w_cs_xx_principale
end type
type cb_sel_stock from commandbutton within w_non_conformita_lotti
end type
type dw_lista from uo_cs_xx_dw within w_non_conformita_lotti
end type
end forward

global type w_non_conformita_lotti from w_cs_xx_principale
integer width = 2807
integer height = 1620
string title = "Lotti Non Conformi"
cb_sel_stock cb_sel_stock
dw_lista dw_lista
end type
global w_non_conformita_lotti w_non_conformita_lotti

type variables
//parametro che consente la gestione di Stock multipli
string is_flag_QSM
long il_anno_nc, il_num_nc
end variables

on w_non_conformita_lotti.create
int iCurrent
call super::create
this.cb_sel_stock=create cb_sel_stock
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_sel_stock
this.Control[iCurrent+2]=this.dw_lista
end on

on w_non_conformita_lotti.destroy
call super::destroy
destroy(this.cb_sel_stock)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("anno_non_conf")
dw_lista.set_dw_key("num_non_conf")

dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + c_nohighlightselected+ c_ViewModeBorderUnchanged + c_CursorRowPointer)

iuo_dw_main = dw_lista

//controlla se puoi inserire più di un lotto
select flag
into :is_flag_QSM
from parametri_omnia
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'QSM' and flag_parametro = 'F';
	
if sqlca.sqlcode <> 0 or isnull(is_flag_QSM) or is_flag_QSM = "" then
	is_flag_QSM = "N"
end if

//anno e numero NC
il_anno_nc = s_cs_xx.parametri.parametro_d_1
il_num_nc = s_cs_xx.parametri.parametro_d_2
end event

event pc_new;long ll_row
string ls_suggerimento

ll_row = dw_lista.rowcount()
if ll_row > 0 and is_flag_QSM <> "S" then
	ls_suggerimento = "Per inserire stock multipli, verifica che il Parametro Omnia " + &
								"'QSM' esista e sia impostato a tipologia 'flag' con valore 'S'!"
	g_mb.messagebox("OMNIA", "Non puoi inserire più di un lotto non conforme!" + char(13) + &
												ls_suggerimento, Exclamation!)
	PCCA.Error = c_Fatal
	return
end if

if isvalid(iuo_dw_main) then
   iuo_dw_main.change_dw_current()
end if

call super::pc_new
end event

type cb_sel_stock from commandbutton within w_non_conformita_lotti
integer x = 32
integer y = 24
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ric.Stock"
end type

event clicked;long ll_row//, ll_anno_nc, ll_num_nc
string ls_cod_prodotto, ls_flag_tipo_nc, ls_cod_fornitore

dw_lista.accepttext()
ll_row = dw_lista.getrow()

if ll_row > 0 then	
else
	g_mb.messagebox("OMNIA", "E' necessario selezionare una riga!", Exclamation!)
	return
end if

//ll_anno_nc = dw_lista.getitemnumber(ll_row, "anno_non_conf")
//ll_num_nc = dw_lista.getitemnumber(ll_row, "num_non_conf")

ls_cod_prodotto = dw_lista.getitemstring(ll_row, "cod_prodotto")
if ls_cod_prodotto = "" or isnull(ls_cod_prodotto) then	
	
	select cod_prodotto, flag_tipo_nc, cod_fornitore
	into :ls_cod_prodotto, :ls_flag_tipo_nc, :ls_cod_fornitore
	from non_conformita
	where cod_azienda = :s_cs_xx.cod_azienda and
		anno_non_conf = :il_anno_nc and num_non_conf = :il_num_nc;
	
	if ls_cod_prodotto = "" or isnull(ls_cod_prodotto) then		
		g_mb.messagebox("OMNIA", "La N.C " + string(il_anno_nc) + "/" + string(il_num_nc) + " non ha nessun prodotto associato!", Exclamation!)
		return
	end if
	
	dw_lista.setitem(ll_row, "cod_prodotto", ls_cod_prodotto)
end if

s_cs_xx.parametri.parametro_s_10 = dw_lista.getitemstring(ll_row, "cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_lista.getitemstring(ll_row, "cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_lista.getitemstring(ll_row, "cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_lista.getitemstring(ll_row, "cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0


dw_lista.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
setnull(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"
setnull(s_cs_xx.parametri.parametro_s_7)
setnull(s_cs_xx.parametri.parametro_s_8)
setnull(s_cs_xx.parametri.parametro_s_9)

select flag_tipo_nc, cod_fornitore
into :ls_flag_tipo_nc, :ls_cod_fornitore
from non_conformita
where cod_azienda = :s_cs_xx.cod_azienda and
	anno_non_conf = :il_anno_nc and num_non_conf = :il_num_nc;

if ls_flag_tipo_nc="A" then
			
	if ls_cod_fornitore="" then setnull(ls_cod_fornitore)
	s_cs_xx.parametri.parametro_s_9 = ls_cod_fornitore
	s_cs_xx.parametri.parametro_data_4 = dw_lista.getitemdatetime(ll_row, "data_stock")
	window_open(w_ricerca_stock_acc, 0)
else
	window_open(w_ricerca_stock, 0)
end if
end event

type dw_lista from uo_cs_xx_dw within w_non_conformita_lotti
integer x = 32
integer y = 124
integer width = 2702
integer height = 1356
integer taborder = 10
string dataobject = "d_non_conf_stock"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_anno_registrazione, ll_num_registrazione, ll_errore

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_non_conf")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_non_conf")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long  l_Idx, ll_anno_registrazione, ll_num_registrazione, ll_prog, ll_rows

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_non_conf")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_non_conf")

ll_rows = RowCount()

for l_Idx = 1 TO ll_rows
	if IsNull(GetItemstring(l_Idx, "cod_azienda")) then
		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	if IsNull(GetItemnumber(l_Idx, "anno_non_conf")) then
		SetItem(l_Idx, "anno_non_conf", ll_anno_registrazione)
	end if
	if IsNull(GetItemnumber(l_Idx, "num_non_conf")) then
		SetItem(l_Idx, "num_non_conf", ll_num_registrazione)
	end if
	
	ll_prog = getitemnumber(l_Idx, "progressivo")
	
	if isnull(ll_prog) or ll_prog <= 0 then
		select max(progressivo)
		into :ll_prog
		from non_conformita_stock
		where cod_azienda = :s_cs_xx.cod_azienda and
			anno_non_conf = :ll_anno_registrazione and num_non_conf = :ll_num_registrazione;
			
		if isnull(ll_prog) then ll_prog = 0
		
		ll_prog += 1
		SetItem(l_Idx, "progressivo", ll_prog)
	end if
next

end event

event pcd_new;call super::pcd_new;string ls_cod_prodotto
long ll_row, ll_anno_nc, ll_num_nc

ll_anno_nc = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_non_conf")
ll_num_nc = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_non_conf")

select cod_prodotto
into :ls_cod_prodotto
from non_conformita
where cod_azienda = :s_cs_xx.cod_azienda and
	anno_non_conf = :ll_anno_nc and num_non_conf = :ll_num_nc;

if ls_cod_prodotto = "" or isnull(ls_cod_prodotto) then
	setnull(ls_cod_prodotto)
	g_mb.messagebox("OMNIA", "La N.C " + string(ll_anno_nc) + "/" + string(ll_num_nc) + " non ha nessun prodotto associato!", Exclamation!)
	ll_row = getrow()
	setitem(ll_row, "cod_prodotto", ls_cod_prodotto)
else
	ll_row = getrow()
	setitem(ll_row, "cod_prodotto", ls_cod_prodotto)
	//cb_sel_stock.postevent(clicked!)
end if


end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto
decimal ld_qta_nc
long ll_index

for ll_index = 1 to rowcount()
	
	ls_cod_deposito = getitemstring(ll_index, "cod_deposito")
	if ls_cod_deposito = "" or isnull(ls_cod_deposito) then
		g_mb.messagebox("OMNIA", "Il Deposito è obbligatorio!", Exclamation!)
		PCCA.Error = c_Fatal
		return
	end if
	
	ls_cod_ubicazione = getitemstring(ll_index, "cod_ubicazione")
	if ls_cod_ubicazione = "" or isnull(ls_cod_ubicazione) then
		g_mb.messagebox("OMNIA", "La Ubicazione è obbligatoria!", Exclamation!)
		PCCA.Error = c_Fatal
		return
	end if
	
	ls_cod_lotto = getitemstring(ll_index, "cod_lotto")
	if ls_cod_lotto = "" or isnull(ls_cod_lotto) then
		g_mb.messagebox("OMNIA", "Il Lotto è obbligatorio!", Exclamation!)
		PCCA.Error = c_Fatal
		return
	end if
	
	ld_qta_nc = getitemdecimal(ll_index, "qta_lotto_nc")
	if ld_qta_nc <= 0 or isnull(ld_qta_nc) then
		g_mb.messagebox("OMNIA", "Indicare la Quantità Non Conforme!", Exclamation!)
		PCCA.Error = c_Fatal
		return
	end if
next
end event

