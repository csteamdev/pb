﻿$PBExportHeader$w_non_conformita_progetti.srw
$PBExportComments$Finestra Dettaglio Non Conformità Progettazione
forward
global type w_non_conformita_progetti from w_cs_xx_principale
end type
type cb_gen_richiesta from commandbutton within w_non_conformita_progetti
end type
type cb_cancella_progetto from commandbutton within w_non_conformita_progetti
end type
type dw_non_conformita_progetti from uo_cs_xx_dw within w_non_conformita_progetti
end type
type dw_folder from u_folder within w_non_conformita_progetti
end type
type cb_documento from commandbutton within w_non_conformita_progetti
end type
end forward

global type w_non_conformita_progetti from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2249
integer height = 1600
string title = "Dati Specifici Non Conformità Progettazione"
event ue_imposta_anno ( )
cb_gen_richiesta cb_gen_richiesta
cb_cancella_progetto cb_cancella_progetto
dw_non_conformita_progetti dw_non_conformita_progetti
dw_folder dw_folder
cb_documento cb_documento
end type
global w_non_conformita_progetti w_non_conformita_progetti

event ue_imposta_anno();if isnull(dw_non_conformita_progetti.getitemnumber(dw_non_conformita_progetti.getrow(),"anno_registrazione")) or &
	dw_non_conformita_progetti.getitemnumber(dw_non_conformita_progetti.getrow(),"anno_registrazione") = 0 &
then
	
//	dw_non_conformita_progetti.setitem(dw_non_conformita_progetti.getrow(),"anno_registrazione",year(today()))
//	
//	f_PO_LoadDDDW_DW(dw_non_conformita_progetti,"num_registrazione",sqlca,&
//                 "tes_documenti","num_registrazione","des_elemento",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + &
//					  "' and anno_registrazione = " + string(year(today())) + &
//					  " and num_registrazione in (select num_registrazione from det_documenti where cod_azienda = '" + &
//					  s_cs_xx.cod_azienda + "' and autorizzato_da is not null and approvato_da is not null)")
					  
	
end if
end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

dw_non_conformita_progetti.set_dw_key("cod_azienda")
dw_non_conformita_progetti.set_dw_key("anno_non_conf")
dw_non_conformita_progetti.set_dw_key("num_non_conf")
dw_non_conformita_progetti.set_dw_options(sqlca,pcca.null_object,c_modifyonopen+c_nonew+c_nodelete,c_default)
iuo_dw_main = dw_non_conformita_progetti

l_objects[1] = dw_non_conformita_progetti
l_objects[2] = cb_documento
l_objects[3] = cb_cancella_progetto
dw_folder.fu_AssignTab(1, "Progettazione", l_Objects[])
dw_folder.fu_FolderCreate(1,4)
                          // il primo parametri indica quanti fogli dentro al folder
                          // il secondo parametro indica quanti fogli per riga
dw_folder.fu_SelectTab(1)


end event

event pc_setddlb;call super::pc_setddlb;long ll_anno_progetto

f_PO_LoadDDDW_DW(dw_non_conformita_progetti,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDLB_DW(dw_non_conformita_progetti, &
                 "anno_progetto", &
                 SQLCA, &
                 "det_fasi_progetto", &
                 "anno_progetto" , &
                 "anno_progetto" , &
                 "det_fasi_progetto.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_progetti,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
					  "GROUP BY num_reg_lista, des_lista")
					  
//f_PO_LoadDDDW_DW(dw_non_conformita_progetti,"num_registrazione",sqlca,&
//                 "tes_documenti","num_registrazione","des_elemento",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + &
//					  "' and num_registrazione in (select num_registrazione from det_documenti where cod_azienda = '" + &
//					  s_cs_xx.cod_azienda + "' and autorizzato_da is not null and approvato_da is not null)")
end event

on w_non_conformita_progetti.create
int iCurrent
call super::create
this.cb_gen_richiesta=create cb_gen_richiesta
this.cb_cancella_progetto=create cb_cancella_progetto
this.dw_non_conformita_progetti=create dw_non_conformita_progetti
this.dw_folder=create dw_folder
this.cb_documento=create cb_documento
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_gen_richiesta
this.Control[iCurrent+2]=this.cb_cancella_progetto
this.Control[iCurrent+3]=this.dw_non_conformita_progetti
this.Control[iCurrent+4]=this.dw_folder
this.Control[iCurrent+5]=this.cb_documento
end on

on w_non_conformita_progetti.destroy
call super::destroy
destroy(this.cb_gen_richiesta)
destroy(this.cb_cancella_progetto)
destroy(this.dw_non_conformita_progetti)
destroy(this.dw_folder)
destroy(this.cb_documento)
end on

event open;call super::open;postevent("ue_imposta_anno")
end event

type cb_gen_richiesta from commandbutton within w_non_conformita_progetti
integer x = 1824
integer y = 1400
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Richiesta"
end type

event clicked;long ll_anno_nc, ll_num_nc, ll_risposta

if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")) then
   s_cs_xx.parametri.parametro_d_1 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")
else
   return
end if

if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"num_non_conf")) then
   s_cs_xx.parametri.parametro_d_2 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"num_non_conf")
else
   return
end if

select anno_non_conf, 
       num_non_conf  
into   :ll_anno_nc, :ll_num_nc
from   non_conformita  
where  cod_azienda   = :s_cs_xx.cod_azienda and
       anno_non_conf = :s_cs_xx.parametri.parametro_d_1 and
       num_non_conf  = :s_cs_xx.parametri.parametro_d_2 ;

if sqlca.sqlcode = 0 then
   ll_risposta = g_mb.messagebox("Nuova Richiesta","Apro un nuova richiesta dalla Non Conformità "+string(ll_anno_nc) + " / " + string(ll_num_nc) +"  ?",Question!, YesNo!, 2)
   if ll_risposta = 1 then
      s_cs_xx.parametri.parametro_s_1 = "GEN_RICHIESTA"
      window_open(w_call_center, -1)
   end if
end if
end event

type cb_cancella_progetto from commandbutton within w_non_conformita_progetti
integer x = 1943
integer y = 400
integer width = 137
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C"
end type

event clicked;long		ll_null

string	ls_null


setnull(ll_null)
setnull(ls_null)

dw_non_conformita_progetti.setitem(dw_non_conformita_progetti.getrow(),"anno_progetto", ll_null)
dw_non_conformita_progetti.setitem(dw_non_conformita_progetti.getrow(),"cod_progetto", ls_null)
dw_non_conformita_progetti.setitem(dw_non_conformita_progetti.getrow(),"num_ver_progetto", ll_null)
dw_non_conformita_progetti.setitem(dw_non_conformita_progetti.getrow(),"num_ediz_progetto", ll_null)
dw_non_conformita_progetti.setitem(dw_non_conformita_progetti.getrow(),"prog_riga_det_fasi", ll_null)
dw_non_conformita_progetti.setitem(dw_non_conformita_progetti.getrow(),"cod_prodotto", ls_null)
end event

type dw_non_conformita_progetti from uo_cs_xx_dw within w_non_conformita_progetti
integer x = 46
integer y = 120
integer width = 2126
integer height = 1240
integer taborder = 40
string dataobject = "d_non_conformita_progetti"
boolean border = false
end type

event pcd_modify;call super::pcd_modify;string ls_cod_progetto
long ll_anno_progetto, ll_num_versione, ll_num_edizione


ll_anno_progetto = dw_non_conformita_progetti.getitemnumber(dw_non_conformita_progetti.getrow(),"anno_progetto")
ls_cod_progetto = getitemstring(getrow(),"cod_progetto")
if not isnull(ll_anno_progetto) and (ll_anno_progetto > 0) then
	f_PO_LoadDDDW_DW(dw_non_conformita_progetti,"cod_progetto",sqlca,&
  	              		"tes_progetti","cod_progetto","des_progetto",&
                 		"(tes_progetti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
	    			  		"(tes_progetti.anno_progetto = " + string(ll_anno_progetto) + ") " + &
					  		"GROUP BY cod_progetto, des_progetto")
end if

if not isnull(ll_anno_progetto) and (ll_anno_progetto > 0) and &
   not isnull(ls_cod_progetto) then
	   ll_num_versione = getitemnumber(getrow(),"num_ver_progetto") 
	   ll_num_edizione = getitemnumber(getrow(),"num_ediz_progetto") 
	   f_PO_LoadDDDW_DW(dw_non_conformita_progetti,"prog_riga_det_fasi",sqlca,&
                   "det_fasi_progetto","prog_riga_det_fasi","des_fase",&
                   "(det_fasi_progetto.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
		   			 "(det_fasi_progetto.anno_progetto = " + string(ll_anno_progetto) + ") and " +& 
					    "(det_fasi_progetto.cod_progetto = '" + ls_cod_progetto + "') and " + & 
					    "(det_fasi_progetto.num_versione = " + string(ll_num_versione) + ") and " +& 
					    "(det_fasi_progetto.num_edizione = " + string(ll_num_edizione) + ")"  )
end if

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_d_1, s_cs_xx.parametri.parametro_d_2)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
end if
end event

event pcd_new;call super::pcd_new;long ll_null

setnull(ll_null)

setitem(getrow(),"anno_registrazione", ll_null)
setitem(getrow(),"num_registrazione", ll_null)
setitem(getrow(),"progressivo", ll_null)
end event

event itemchanged;call super::itemchanged;long ll_anno_progetto, ll_num_edizione, ll_num_versione, ll_null, ll_lista, ll_anno, ll_numero, ll_progressivo
string ls_null, ls_cod_prodotto

if i_extendmode then

setnull(ll_null)
setnull(ls_null)
choose case i_colname
	case "anno_progetto"
		
      if not isnull(i_coltext) and (long(i_coltext) > 0) then
         f_PO_LoadDDDW_DW(dw_non_conformita_progetti,"cod_progetto",sqlca,&
                 "tes_progetti","cod_progetto","des_progetto",&
                 "(tes_progetti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "(tes_progetti.anno_progetto = " + i_coltext + ") and exists (select cod_progetto from det_fasi_progetto where det_fasi_progetto.cod_progetto = tes_progetti.cod_progetto)" + &
					  "GROUP BY cod_progetto, des_progetto")
	end if
	
	
	
   case "cod_progetto"
      if not isnull(i_coltext) then
         ll_anno_progetto = getitemnumber(getrow(),"anno_progetto")
			
         SELECT max(tes_progetti.num_versione),   
                max(tes_progetti.num_edizione)
         INTO   :ll_num_versione,   
                :ll_num_edizione
         FROM   tes_progetti  
         WHERE  ( tes_progetti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
                ( tes_progetti.anno_progetto = :ll_anno_progetto ) AND  
                ( tes_progetti.cod_progetto = :i_coltext )   ;
         SELECT tes_progetti.cod_prodotto
         INTO   :ls_cod_prodotto
         FROM   tes_progetti  
         WHERE  ( tes_progetti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
                ( tes_progetti.anno_progetto = :ll_anno_progetto ) AND  
                ( tes_progetti.cod_progetto = :i_coltext )   ;
					 
         setitem(getrow(),"num_ver_progetto",ll_num_versione) 
         setitem(getrow(),"num_ediz_progetto",ll_num_edizione) 
         setitem(getrow(),"cod_prodotto",ls_cod_prodotto) 
         f_PO_LoadDDDW_DW(dw_non_conformita_progetti,"prog_riga_det_fasi",sqlca,&
                 "det_fasi_progetto","prog_riga_det_fasi","des_fase",&
                 " cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  " anno_progetto = " + string(ll_anno_progetto) + " and " +& 
					  " cod_progetto = '" + i_coltext + "' and " + & 
					  " num_versione = " + string(ll_num_versione) + " and " +& 
					  " num_edizione = " + string(ll_num_edizione) )
		else
			setitem(getrow(),"anno_progetto", ll_null)
			setitem(getrow(),"cod_progetto", ls_null)
			setitem(getrow(),"num_ver_progetto", ll_null)
			setitem(getrow(),"num_ediz_progetto", ll_null)
			setitem(getrow(),"prog_riga_det_fasi", ll_null)
			setitem(getrow(),"cod_prodotto", ls_null)
		end if

	case "num_reg_lista"
      if not isnull(i_coltext) and i_coltext <> "" and i_coltext <> "0" then			
			
         ll_lista = long(i_coltext)

			select max(num_edizione)
			into   :ll_num_edizione
			from   tes_liste_controllo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 num_reg_lista = :ll_lista and
					 approvato_da is not null;
			 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
				return -1
			end if
			
			select max(num_versione)
			into   :ll_num_versione
			from   tes_liste_controllo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 num_reg_lista = :ll_lista and
					 num_edizione = :ll_num_edizione and
					 approvato_da is not null;
		 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
				return -1
			end if

         setitem(getrow(),"num_ver_lista",ll_num_versione) 
         setitem(getrow(),"num_ediz_lista",ll_num_edizione) 
			//setitem(getrow(),"codice_documento", ll_null)
		else

         setitem( getrow(), "num_ver_lista", 0) 
         setitem( getrow(), "num_ediz_lista", 0) 			
		end if

	case "anno_registrazione"
		
//		if trim(i_coltext) = "" or long(i_coltext) = 0 then
//			setnull(i_coltext)
//		end if
//		
//		if not isnull(i_coltext) then
//			
//			setitem(getrow(),"num_reg_lista", ll_null)
//			setitem(getrow(),"num_ver_lista", ll_null)
//			setitem(getrow(),"num_ediz_lista", ll_null)
			
//			f_PO_LoadDDDW_DW(dw_non_conformita_progetti,"num_registrazione",sqlca,&
//                 "tes_documenti","num_registrazione","des_elemento",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + &
//					  "' and anno_registrazione = " + i_coltext + &
//					  " and num_registrazione in (select num_registrazione from det_documenti where cod_azienda = '" + &
//					  s_cs_xx.cod_azienda + "' and autorizzato_da is not null and approvato_da is not null)" + &
//					  " and tipo_elemento = 'D'")
		
//		else
			
//			f_PO_LoadDDDW_DW(dw_non_conformita_progetti,"num_registrazione",sqlca,&
//                 "tes_documenti","num_registrazione","des_elemento",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + &
//					  "' and num_registrazione in (select num_registrazione from det_documenti where cod_azienda = '" + &
//					  s_cs_xx.cod_azienda + "' and autorizzato_da is not null and approvato_da is not null)" + &
//					  " and tipo_elemento = 'D'")
		
//		end if
//		
//		setitem(getrow(),"num_registrazione", ll_null)		
//		setitem(getrow(),"progressivo", ll_null)
		
	case "num_registrazione"
		
//		if trim(i_coltext) = "" then
//			setnull(i_coltext)
//		end if
//		
//		ll_anno = getitemnumber(getrow(),"anno_registrazione")
//		ll_numero = long(i_coltext)
//		
//		if not isnull(i_coltext) then
//			
//			setitem(getrow(),"num_reg_lista", ll_null)
//			setitem(getrow(),"num_ver_lista", ll_null)
//			setitem(getrow(),"num_ediz_lista", ll_null)			
//			
//			select max(progressivo)
//			into   :ll_progressivo
//			from   det_documenti
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 anno_registrazione = :ll_anno and
//					 num_registrazione = :ll_numero and
//					 autorizzato_da is not null and
//					 approvato_da is not null;
//												 
//			if sqlca.sqlcode <> 0 then
//				messagebox("OMNIA","Errore nella select di det_documenti: " + sqlca.sqlerrtext)
//				return -1
//			end if
//			
//			setitem(getrow(),"progressivo", ll_progressivo)
//						
//		else
//			
//			setitem(getrow(),"progressivo", ll_null)
//					
//		end if
		
end choose

end if			//  fine di i_extendmode

end event

event updatestart;call super::updatestart;// modifica Michela: come richiesto da marco in data 14/06/2004

//if not (isnull(getitemnumber(getrow(),"anno_registrazione")) or getitemnumber(getrow(),"anno_registrazione") = 0) and &
//   (isnull(getitemnumber(getrow(),"num_registrazione")) or &
//	isnull(getitemnumber(getrow(),"progressivo"))) then
//	messagebox("OMNIA","Impostare anno e numero del documento")
//	return 1
//end if
end event

type dw_folder from u_folder within w_non_conformita_progetti
integer x = 23
integer y = 20
integer width = 2171
integer height = 1360
integer taborder = 50
end type

type cb_documento from commandbutton within w_non_conformita_progetti
integer x = 2057
integer y = 624
integer width = 69
integer height = 72
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;string  ls_doc, ls_appo
integer li_pos
long    ll_anno, ll_numero, ll_progressivo, ll_num_versione, ll_num_revisione

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
window_open(w_det_visite_ispettive_collegamenti,0)

if isnull(s_cs_xx.parametri.parametro_s_1) or isnull(s_cs_xx.parametri.parametro_s_2) then return 

ls_appo = s_cs_xx.parametri.parametro_s_1

li_pos = pos(ls_appo, "/")
ll_anno = long(left(ls_appo, li_pos -1))
ls_appo = mid(ls_appo, li_pos + 1)

li_pos = pos(ls_appo, "/")
ll_numero = long(left(ls_appo, li_pos -1))

ll_progressivo = long(mid(ls_appo, li_pos + 1))

dw_non_conformita_progetti.setitem(dw_non_conformita_progetti.getrow(), "anno_registrazione", ll_anno)
dw_non_conformita_progetti.setitem(dw_non_conformita_progetti.getrow(), "num_registrazione", ll_numero)
dw_non_conformita_progetti.setitem(dw_non_conformita_progetti.getrow(), "progressivo", ll_progressivo)





end event

