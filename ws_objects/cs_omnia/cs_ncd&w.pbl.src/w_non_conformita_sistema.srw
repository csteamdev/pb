﻿$PBExportHeader$w_non_conformita_sistema.srw
$PBExportComments$Finestra Gestione Dettaglio Non Conformita Sistema
forward
global type w_non_conformita_sistema from w_cs_xx_principale
end type
type cb_gen_richiesta from commandbutton within w_non_conformita_sistema
end type
type cb_prod_ric from cb_prod_ricerca within w_non_conformita_sistema
end type
type cb_ric_lavorazione from cb_lavorazione_ricerca within w_non_conformita_sistema
end type
type dw_non_conformita_sistema from uo_cs_xx_dw within w_non_conformita_sistema
end type
type dw_folder from u_folder within w_non_conformita_sistema
end type
type cb_documento from commandbutton within w_non_conformita_sistema
end type
end forward

global type w_non_conformita_sistema from w_cs_xx_principale
integer width = 2354
integer height = 1336
string title = "Dati Specifici Non Conformità Sistema Qualità"
event ue_imposta_anno ( )
cb_gen_richiesta cb_gen_richiesta
cb_prod_ric cb_prod_ric
cb_ric_lavorazione cb_ric_lavorazione
dw_non_conformita_sistema dw_non_conformita_sistema
dw_folder dw_folder
cb_documento cb_documento
end type
global w_non_conformita_sistema w_non_conformita_sistema

event ue_imposta_anno();if isnull(dw_non_conformita_sistema.getitemnumber(dw_non_conformita_sistema.getrow(),"anno_registrazione")) or &
	dw_non_conformita_sistema.getitemnumber(dw_non_conformita_sistema.getrow(),"anno_registrazione") = 0 &
then
	
//	dw_non_conformita_sistema.setitem(dw_non_conformita_sistema.getrow(),"anno_registrazione",year(today()))
//	
//	f_PO_LoadDDDW_DW(dw_non_conformita_sistema,"num_registrazione",sqlca,&
//                 "tes_documenti","num_registrazione","des_elemento",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + &
//					  "' and anno_registrazione = " + string(year(today())) + &
//					  " and num_registrazione in (select num_registrazione from det_documenti where cod_azienda = '" + &
//					  s_cs_xx.cod_azienda + "' and autorizzato_da is not null and approvato_da is not null)")
					  
	
end if
end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

dw_non_conformita_sistema.set_dw_key("cod_azienda")
dw_non_conformita_sistema.set_dw_key("anno_non_conf")
dw_non_conformita_sistema.set_dw_key("num_non_conf")
dw_non_conformita_sistema.set_dw_options(sqlca,pcca.null_object,c_modifyonopen + c_nonew + c_nodelete,c_default)
iuo_dw_main = dw_non_conformita_sistema

l_objects[1] = dw_non_conformita_sistema
l_objects[2] = cb_prod_ric
l_objects[3] = cb_ric_lavorazione
l_objects[3] = cb_documento
dw_folder.fu_AssignTab(1, "Sistema Qualità", l_Objects[])
dw_folder.fu_FolderCreate(1,4)
                          // il primo parametro indica quanti fogli dentro al folder
                          // il secondo parametro indica quanti fogli per riga
dw_folder.fu_SelectTab(1)


//windowobject l_objects[ ]


end event

event pc_setddlb;call super::pc_setddlb;F_PO_LoadDDDW_DW(dw_non_conformita_sistema,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

					  
f_PO_LoadDDDW_DW(dw_non_conformita_sistema,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
					  "GROUP BY num_reg_lista, des_lista")
					  
//f_PO_LoadDDDW_DW(dw_non_conformita_sistema,"num_registrazione",sqlca,&
//                 "tes_documenti","num_registrazione","des_elemento",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + &
//					  "' and num_registrazione in (select num_registrazione from det_documenti where cod_azienda = '" + &
//					  s_cs_xx.cod_azienda + "' and autorizzato_da is not null and approvato_da is not null)")
end event

on w_non_conformita_sistema.create
int iCurrent
call super::create
this.cb_gen_richiesta=create cb_gen_richiesta
this.cb_prod_ric=create cb_prod_ric
this.cb_ric_lavorazione=create cb_ric_lavorazione
this.dw_non_conformita_sistema=create dw_non_conformita_sistema
this.dw_folder=create dw_folder
this.cb_documento=create cb_documento
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_gen_richiesta
this.Control[iCurrent+2]=this.cb_prod_ric
this.Control[iCurrent+3]=this.cb_ric_lavorazione
this.Control[iCurrent+4]=this.dw_non_conformita_sistema
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.cb_documento
end on

on w_non_conformita_sistema.destroy
call super::destroy
destroy(this.cb_gen_richiesta)
destroy(this.cb_prod_ric)
destroy(this.cb_ric_lavorazione)
destroy(this.dw_non_conformita_sistema)
destroy(this.dw_folder)
destroy(this.cb_documento)
end on

type cb_gen_richiesta from commandbutton within w_non_conformita_sistema
integer x = 1920
integer y = 1140
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Richiesta"
end type

event clicked;long ll_anno_nc, ll_num_nc, ll_risposta

if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")) then
   s_cs_xx.parametri.parametro_d_1 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")
else
   return
end if

if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"num_non_conf")) then
   s_cs_xx.parametri.parametro_d_2 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"num_non_conf")
else
   return
end if

select anno_non_conf, 
       num_non_conf  
into   :ll_anno_nc, :ll_num_nc
from   non_conformita  
where  cod_azienda   = :s_cs_xx.cod_azienda and
       anno_non_conf = :s_cs_xx.parametri.parametro_d_1 and
       num_non_conf  = :s_cs_xx.parametri.parametro_d_2 ;

if sqlca.sqlcode = 0 then
   ll_risposta = g_mb.messagebox("Nuova Richiesta","Apro un nuova richiesta dalla Non Conformità "+string(ll_anno_nc) + " / " + string(ll_num_nc) +"  ?",Question!, YesNo!, 2)
   if ll_risposta = 1 then
      s_cs_xx.parametri.parametro_s_1 = "GEN_RICHIESTA"
      window_open(w_call_center, -1)
   end if
end if
end event

type cb_prod_ric from cb_prod_ricerca within w_non_conformita_sistema
integer x = 2149
integer y = 160
integer width = 73
integer height = 80
integer taborder = 50
boolean bringtotop = true
end type

on getfocus;call cb_prod_ricerca::getfocus;dw_non_conformita_sistema.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"

end on

type cb_ric_lavorazione from cb_lavorazione_ricerca within w_non_conformita_sistema
integer x = 2149
integer y = 240
integer width = 73
integer height = 80
integer taborder = 40
boolean bringtotop = true
end type

event clicked;call cb_lavorazione_ricerca::clicked;dw_non_conformita_sistema.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_lavorazione"
s_cs_xx.parametri.parametro_s_2 = "cod_reparto"
s_cs_xx.parametri.parametro_s_3 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_sistema.getitemstring(dw_non_conformita_sistema.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_non_conformita_sistema.getitemstring(dw_non_conformita_sistema.getrow(),"cod_reparto")
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Lavorazioni","Prodotto Mancante: la ricerca verrà effettuata su un numero elevato di elementi",Information!)
end if
window_open(w_ricerca_lavorazione, 0)
end event

type dw_non_conformita_sistema from uo_cs_xx_dw within w_non_conformita_sistema
integer x = 46
integer y = 140
integer width = 2217
integer height = 960
integer taborder = 60
string dataobject = "d_non_conformita_sistema"
boolean border = false
end type

event pcd_modify;call super::pcd_modify;cb_documento.enabled = true
end event

event pcd_validatecol;call super::pcd_validatecol;//if i_extendmode then
//	string ls_null
//	
//	choose case i_colname
//	case "cod_reparto"		
//		if isnull(s_cs_xx.parametri.parametro_s_3) then
//			setnull(ls_null)
//			dw_non_conformita_sistema.setitem(dw_non_conformita_sistema.getrow(), "cod_prodotto", ls_null)
//		end if
//	end choose

//string ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_null, ls_str
//
//setnull(ls_null)
//
//choose case i_colname
//case "cod_prodotto"
//   ls_cod_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
//   if isnull(ls_cod_prodotto) then ls_cod_prodotto = " "
//   if (i_coltext <> ls_cod_prodotto) then     ////   and (not isnull(i_coltext))
//      messagebox("Non Conformita Sistema Qualità", "Prodotto variato: devi reimpostare reparto e lavorazione !", Information!)
//      this.setitem(this.getrow(), "cod_reparto", ls_null)
//      this.setitem(this.getrow(), "cod_lavorazione", ls_null)
//      f_PO_LoadDDDW_DW(dw_non_conformita_sistema, &
//                       "cod_reparto", &
//                       SQLCA, &
//                       "anag_reparti", &
//                       "cod_reparto" , &
//                       "des_reparto" , &
//                       "(anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
//                        (anag_reparti.cod_reparto in (SELECT tes_fasi_lavorazione.cod_reparto FROM   tes_fasi_lavorazione WHERE  (tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_fasi_lavorazione.cod_prodotto = '" + i_coltext + "')))")
//   end if
//
//case "cod_reparto"
//   ls_cod_reparto = this.getitemstring(this.getrow(), "cod_reparto")
//   if isnull(ls_cod_reparto) then ls_cod_reparto = " "
//   if (i_coltext <> ls_cod_reparto) or isnull(i_coltext) then
//      messagebox("Non Conformita Sistema Qualità", "Reparto variato: devi reimpostare la lavorazione !", Information!)
//      this.setitem(this.getrow(), "cod_lavorazione", ls_null)
//      f_PO_LoadDDLB_DW(dw_non_conformita_sistema, &
//                       "cod_lavorazione", &
//                       SQLCA, &
//                       "tes_fasi_lavorazione", &
//                       "cod_lavorazione" , &
//                       "cod_lavorazione" , &
//                       "(tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
//                        (tes_fasi_lavorazione.cod_prodotto = '" + this.getitemstring(this.getrow(), "cod_prodotto") + "') and &
//                        (tes_fasi_lavorazione.cod_reparto = '" + i_coltext + "')" )
//   end if
//end choose
//
//end if  // non toccare : si riferisce al test della variabile i_extendmode
//
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_d_1, s_cs_xx.parametri.parametro_d_2)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
//else
//
//if len(this.getitemstring(this.getrow(), "cod_reparto")) > 0 then
//   f_PO_LoadDDLB_DW(dw_non_conformita_sistema, &
//                    "cod_lavorazione", &
//                    SQLCA, &
//                    "tes_fasi_lavorazione", &
//                    "cod_lavorazione" , &
//                    "cod_lavorazione" , &
//                    "(tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
//                     (tes_fasi_lavorazione.cod_prodotto = '" + dw_non_conformita_sistema.getitemstring(dw_non_conformita_sistema.getrow(), "cod_prodotto") + "') and &
//                     (tes_fasi_lavorazione.cod_reparto = '" + this.getitemstring(this.getrow(), "cod_reparto") + "')" )
//end if
//
//if len(this.getitemstring(this.getrow(), "cod_prodotto")) > 0 then
//   f_PO_LoadDDDW_DW(dw_non_conformita_sistema, &
//                    "cod_reparto", &
//                    SQLCA, &
//                    "anag_reparti", &
//                    "cod_reparto" , &
//                    "des_reparto" , &
//                    "(anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
//                     (anag_reparti.cod_reparto in (SELECT tes_fasi_lavorazione.cod_reparto FROM   tes_fasi_lavorazione WHERE  (tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_fasi_lavorazione.cod_prodotto = '" + this.getitemstring(this.getrow(), "cod_prodotto") + "')))")
//end if
end if
end event

event pcd_new;call super::pcd_new;cb_documento.enabled = true
end event

event pcd_view;call super::pcd_view;cb_documento.enabled = false
end event

event itemchanged;call super::itemchanged;long   ll_anno_progetto, ll_num_edizione, ll_num_versione, ll_null, ll_lista, ll_anno, ll_numero, ll_progressivo

string ls_null


if i_extendmode then

setnull(ll_null)
setnull(ls_null)
choose case i_colname
		
	case "num_reg_lista"
		
      if not isnull(i_coltext) then
			
         ll_lista = long(i_coltext)
		
			select max(num_edizione)
			into   :ll_num_edizione
			from   tes_liste_controllo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 num_reg_lista = :ll_lista and
					 approvato_da is not null;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
				return -1
			end if

			select max(num_versione)
			into   :ll_num_versione
			from   tes_liste_controllo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 num_reg_lista = :ll_lista and
					 num_edizione = :ll_num_edizione and
					 approvato_da is not null;
		 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
				return -1
			end if

			setitem(getrow(),"num_ediz_lista",ll_num_edizione)
			setitem(getrow(),"num_ver_lista",ll_num_versione) 
//			setitem(getrow(),"anno_registrazione", ll_null)
//			setitem(getrow(),"num_registrazione", ll_null)
//			setitem(getrow(),"progressivo", ll_null)
						
		end if
		
end choose

end if			//  fine di i_extendmode

end event

event updatestart;call super::updatestart;//if not (isnull(getitemnumber(getrow(),"anno_registrazione")) or getitemnumber(getrow(),"anno_registrazione") = 0) and &
//   (isnull(getitemnumber(getrow(),"num_registrazione")) or &
//	isnull(getitemnumber(getrow(),"progressivo"))) then
//	messagebox("OMNIA","Impostare anno e numero del documento")
//	return 1
//end if	
end event

type dw_folder from u_folder within w_non_conformita_sistema
integer x = 23
integer y = 20
integer width = 2263
integer height = 1100
integer taborder = 70
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Sistema Qualità"
      SetFocus(dw_non_conformita_sistema)
END CHOOSE

end on

type cb_documento from commandbutton within w_non_conformita_sistema
integer x = 2126
integer y = 444
integer width = 69
integer height = 72
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;string  ls_doc, ls_appo
integer li_pos
long    ll_anno, ll_numero, ll_progressivo, ll_num_versione, ll_num_revisione

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
window_open(w_det_visite_ispettive_collegamenti,0)

if isnull(s_cs_xx.parametri.parametro_s_1) or isnull(s_cs_xx.parametri.parametro_s_2) then return 

ls_appo = s_cs_xx.parametri.parametro_s_1

li_pos = pos(ls_appo, "/")
ll_anno = long(left(ls_appo, li_pos -1))
ls_appo = mid(ls_appo, li_pos + 1)

li_pos = pos(ls_appo, "/")
ll_numero = long(left(ls_appo, li_pos -1))

ll_progressivo = long(mid(ls_appo, li_pos + 1))

dw_non_conformita_sistema.setitem(dw_non_conformita_sistema.getrow(), "anno_registrazione", ll_anno)
dw_non_conformita_sistema.setitem(dw_non_conformita_sistema.getrow(), "num_registrazione", ll_numero)
dw_non_conformita_sistema.setitem(dw_non_conformita_sistema.getrow(), "progressivo", ll_progressivo)





end event

