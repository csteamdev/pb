﻿$PBExportHeader$w_non_conformita_std.srw
$PBExportComments$Finestra Dati Standard Non Conformita
forward
global type w_non_conformita_std from w_cs_xx_principale
end type
type cb_lotti from commandbutton within w_non_conformita_std
end type
type sle_filtro_data_scadenza from u_sle_search within w_non_conformita_std
end type
type st_nota_4 from statictext within w_non_conformita_std
end type
type st_nota_3 from statictext within w_non_conformita_std
end type
type st_nota_1 from statictext within w_non_conformita_std
end type
type st_filtro_data from statictext within w_non_conformita_std
end type
type sle_filtro_data_registrazione from u_sle_search within w_non_conformita_std
end type
type st_filtro_data_scad from statictext within w_non_conformita_std
end type
type cb_annulla_1 from commandbutton within w_non_conformita_std
end type
type cb_chiusura from commandbutton within w_non_conformita_std
end type
type cb_comunicazioni from commandbutton within w_non_conformita_std
end type
type cb_costi from commandbutton within w_non_conformita_std
end type
type cb_dettagli from commandbutton within w_non_conformita_std
end type
type cb_doc_aut_deroga from cb_documenti_compilati within w_non_conformita_std
end type
type cb_doc_compilato from cb_documenti_compilati within w_non_conformita_std
end type
type cb_invia from commandbutton within w_non_conformita_std
end type
type cb_report from commandbutton within w_non_conformita_std
end type
type cb_respingi from commandbutton within w_non_conformita_std
end type
type cb_ricerca_1 from commandbutton within w_non_conformita_std
end type
type dw_non_conformita_lista from uo_cs_xx_dw within w_non_conformita_std
end type
type dw_folder_search from u_folder within w_non_conformita_std
end type
type dw_non_conformita_std_1 from uo_cs_xx_dw within w_non_conformita_std
end type
type dw_non_conformita_std_3 from uo_cs_xx_dw within w_non_conformita_std
end type
type dw_non_conformita_std_4 from uo_cs_xx_dw within w_non_conformita_std
end type
type dw_non_conformita_std_2 from uo_cs_xx_dw within w_non_conformita_std
end type
type mle_stato from multilineedit within w_non_conformita_std
end type
type dw_folder from u_folder within w_non_conformita_std
end type
type dw_nc_ricerca from u_dw_search within w_non_conformita_std
end type
type dw_non_conformita_std_5 from uo_cs_xx_dw within w_non_conformita_std
end type
end forward

global type w_non_conformita_std from w_cs_xx_principale
integer width = 3351
integer height = 2496
string title = "Dati Generici Non Conformità"
cb_lotti cb_lotti
sle_filtro_data_scadenza sle_filtro_data_scadenza
st_nota_4 st_nota_4
st_nota_3 st_nota_3
st_nota_1 st_nota_1
st_filtro_data st_filtro_data
sle_filtro_data_registrazione sle_filtro_data_registrazione
st_filtro_data_scad st_filtro_data_scad
cb_annulla_1 cb_annulla_1
cb_chiusura cb_chiusura
cb_comunicazioni cb_comunicazioni
cb_costi cb_costi
cb_dettagli cb_dettagli
cb_doc_aut_deroga cb_doc_aut_deroga
cb_doc_compilato cb_doc_compilato
cb_invia cb_invia
cb_report cb_report
cb_respingi cb_respingi
cb_ricerca_1 cb_ricerca_1
dw_non_conformita_lista dw_non_conformita_lista
dw_folder_search dw_folder_search
dw_non_conformita_std_1 dw_non_conformita_std_1
dw_non_conformita_std_3 dw_non_conformita_std_3
dw_non_conformita_std_4 dw_non_conformita_std_4
dw_non_conformita_std_2 dw_non_conformita_std_2
mle_stato mle_stato
dw_folder dw_folder
dw_nc_ricerca dw_nc_ricerca
dw_non_conformita_std_5 dw_non_conformita_std_5
end type
global w_non_conformita_std w_non_conformita_std

type variables
boolean ib_delete=true, ib_in_new=false, ib_visualizza_campi
string is_prodotto, is_tipo_requisito
string is_flag_apertura_nc, is_flag_chiusura_nc
string is_flag_conferma

long il_orig_val
string is_flag_BNC = "N"

// stefanop: 27/07/2011: paramentro aziendale NWD per abilitare il wizard nella creazione
boolean ib_wizard = false

// stefanop: 25/10/2011: parametro NCC per abilitare l'apertura della finestra dei corsi dopo il savaltaggio
boolean ib_open_costi = false
end variables

forward prototypes
public subroutine wf_proteggi_documento (string fs_cod_tipo_causa, long fl_num_sequenza_corrente, boolean fb_chiusura)
public function integer wf_responsabile (string fs_cod_tipo_causa, long fl_num_sequenza_corrente, boolean fb_chiusura)
public function integer wf_loaddddw_dw (datawindow dddw_dw, string dddw_column, transaction trans_object, string table_name, string column_code, string column_desc, string join_clause, string where_clause)
public function string wf_nc_o_reclamo (string as_tipo_causa)
public function integer wf_string_to_array (string as_input, string as_sep, ref string as_array[])
public function datetime wf_seleziona_data_chiusura ()
end prototypes

public subroutine wf_proteggi_documento (string fs_cod_tipo_causa, long fl_num_sequenza_corrente, boolean fb_chiusura);string ls_cod_tipo_lista_dist, ls_flag_gestione_fasi, ls_cod_lista_dist, ls_des_fase, ls_cod_destinatario
string ls_sql, ls_sintassi, ls_errore, ls_count, ls_column_name, ls_col_index, ls_protect, ls_ret
datastore lds_campi
long ll_righe, ll_sequenza_destinatari, ll_num_sequenza_corrente
integer li_index
string ls_colonna, ls_flag_modificabile, ls_flag_responsabile

ll_num_sequenza_corrente = fl_num_sequenza_corrente
if isnull(ll_num_sequenza_corrente) then ll_num_sequenza_corrente = 0

select cod_tipo_lista_dist,
		 cod_lista_dist
into   :ls_cod_tipo_lista_dist,
		 :ls_cod_lista_dist
from   tab_tipi_cause 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_causa = :fs_cod_tipo_causa;
	 
select flag_gestione_fasi
into   :ls_flag_gestione_fasi
from   tab_tipi_liste_dist
where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
		 
//numero di colonne nella dw di dettaglio
ls_count = dw_non_conformita_std_1.Describe("DataWindow.Column.Count")
		 
select stringa
into   :ls_cod_destinatario
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'PCU';

//l'utente collegato è un responsabile?
//se no allora tutti i campi devono essere protetti
//se CS_SYSTEM allora tutti i campi sprotetti
//se responsabile allora dipende dal flag_modificabile

select num_sequenza
into   :ll_sequenza_destinatari
from   det_liste_dist
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
		 cod_lista_dist = :ls_cod_lista_dist and
		 (num_sequenza_prec = :ll_num_sequenza_corrente
		 	or num_sequenza_prec is null);

//ciclo su tutte le colonne delle dw per eventualmente proteggere
for li_index = 1 to integer(ls_count)
	ls_col_index = "#"+string(li_index)
	ls_column_name =dw_non_conformita_std_1.Describe(ls_col_index+".name")
	
	if s_cs_xx.cod_utente = "CS_SYSTEM" or ls_flag_gestione_fasi <> "S" then
		ls_protect = "0"
	elseif fb_chiusura then
		ls_protect = "1"
	else
		//se si tratta della prima fase (ll_num_sequenza_corrente zero o null)
		//devi controllare solo se i campi sono modificabili
		//altrimenti serve pure che l'utente sia responsabile della fase
		if ll_num_sequenza_corrente= 0 or isnull(ll_num_sequenza_corrente) then
			//si tratta della prima fase
			select distinct det_liste_dist_campi_nc.colonna
					,det_liste_dist_campi_nc.flag_modificabile					
			into    :ls_colonna
					,:ls_flag_modificabile					
			from det_liste_dist_campi_nc
			join det_liste_dist_destinatari on det_liste_dist_destinatari.cod_azienda=det_liste_dist_campi_nc.cod_azienda
					and det_liste_dist_destinatari.cod_tipo_lista_dist=det_liste_dist_campi_nc.cod_tipo_lista_dist
					and det_liste_dist_destinatari.cod_lista_dist=det_liste_dist_campi_nc.cod_lista_dist
					and det_liste_dist_destinatari.cod_destinatario=det_liste_dist_campi_nc.cod_destinatario
					and det_liste_dist_destinatari.num_sequenza=det_liste_dist_campi_nc.num_sequenza
			join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
					and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
			where det_liste_dist_campi_nc.cod_azienda=:s_cs_xx.cod_azienda
					and det_liste_dist_campi_nc.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
					and det_liste_dist_campi_nc.cod_lista_dist=:ls_cod_lista_dist
					and det_liste_dist_campi_nc.cod_destinatario=:ls_cod_destinatario
					and det_liste_dist_campi_nc.num_sequenza=:ll_sequenza_destinatari//:fl_num_sequenza_corrente
					and det_liste_dist_campi_nc.colonna=:ls_column_name
					//and tab_ind_dest.cod_utente = :s_cs_xx.cod_utente
			;
			if sqlca.sqlcode < 0 then
				g_mb.messagebox( "OMNIA", "Errore durante la lettura delle informazioni "+&
									"per la colonna '"+ls_column_name+"'")
				destroy lds_campi;
				return 
			end if
			
			if sqlca.sqlcode = 0 then
				//la protezione del campo dipende dalla variabile ls_flag_modificabile
				if ls_flag_modificabile = "S" then
					ls_protect = "0"
				else
					ls_protect = "1"
				end if
			else
				//valore non letto
				ls_protect = "1"
			end if	
		//-------------------------------------
		else
			//ciò che discrimina è il responsabile di fase
			//leggi info della colonna dalla tabella det_liste_dist_campi_nc
			select det_liste_dist_campi_nc.colonna
					,det_liste_dist_campi_nc.flag_modificabile
					,det_liste_dist_destinatari.flag_responsabile
			into    :ls_colonna
					,:ls_flag_modificabile
					,:ls_flag_responsabile
			from det_liste_dist_campi_nc
			join det_liste_dist_destinatari on det_liste_dist_destinatari.cod_azienda=det_liste_dist_campi_nc.cod_azienda
					and det_liste_dist_destinatari.cod_tipo_lista_dist=det_liste_dist_campi_nc.cod_tipo_lista_dist
					and det_liste_dist_destinatari.cod_lista_dist=det_liste_dist_campi_nc.cod_lista_dist
					and det_liste_dist_destinatari.cod_destinatario=det_liste_dist_campi_nc.cod_destinatario
					and det_liste_dist_destinatari.num_sequenza=det_liste_dist_campi_nc.num_sequenza
			join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
					and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
			where det_liste_dist_campi_nc.cod_azienda=:s_cs_xx.cod_azienda
					and det_liste_dist_campi_nc.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
					and det_liste_dist_campi_nc.cod_lista_dist=:ls_cod_lista_dist
					and det_liste_dist_campi_nc.cod_destinatario=:ls_cod_destinatario
					and det_liste_dist_campi_nc.num_sequenza=:ll_sequenza_destinatari//:fl_num_sequenza_corrente
					and det_liste_dist_campi_nc.colonna=:ls_column_name
					and tab_ind_dest.cod_utente = :s_cs_xx.cod_utente
			;
			if sqlca.sqlcode < 0 then
				g_mb.messagebox( "OMNIA", "Errore durante la lettura delle informazioni "+&
									"per la colonna '"+ls_column_name+"'")
				destroy lds_campi;
				return 
			end if
			
			if sqlca.sqlcode = 0 and ls_flag_responsabile="S" then
				//la protezione del campo dipende dalla variabile ls_flag_modificabile
				if ls_flag_modificabile = "S" then
					ls_protect = "0"
				else
					ls_protect = "1"
				end if
			else
				//non si tratta di responsabile
				ls_protect = "1"
			end if	
		end if			
	end if
	
	//QUESTA MODIFICA SE LA DEVONO SUDARE!!!!!!!
	//vedi anche evento pcd_new di dw_non_conformita_lista
	
	//il campo responsabile Iter NC deve essere protetto a prescindere
	if ls_column_name = "cod_operaio" then ls_protect = "1"
	//-------------
	
	ls_ret = dw_non_conformita_std_1.modify(ls_column_name+'.protect='+ls_protect)
	ls_ret = dw_non_conformita_std_2.modify(ls_column_name+'.protect='+ls_protect)
	ls_ret = dw_non_conformita_std_3.modify(ls_column_name+'.protect='+ls_protect)
	ls_ret = dw_non_conformita_std_4.modify(ls_column_name+'.protect='+ls_protect)

next

end subroutine

public function integer wf_responsabile (string fs_cod_tipo_causa, long fl_num_sequenza_corrente, boolean fb_chiusura);string ls_cod_tipo_lista_dist, ls_flag_gestione_fasi, ls_cod_lista_dist, ls_des_fase, ls_cod_destinatario
string ls_sql, ls_sintassi, ls_errore, ls_count, ls_column_name, ls_col_index, ls_protect, ls_ret
//datastore lds_campi
long ll_righe, ll_sequenza_destinatari, ll_num_sequenza_corrente
integer li_index
string ls_colonna, ls_flag_modificabile, ls_flag_responsabile, ls_cod_area_aziendale
long ll_anno, ll_num, ll_riga

if s_cs_xx.cod_utente = "CS_SYSTEM" then return 1

ll_num_sequenza_corrente = fl_num_sequenza_corrente
if isnull(ll_num_sequenza_corrente) then ll_num_sequenza_corrente = 0

select cod_tipo_lista_dist,
		 cod_lista_dist
into   :ls_cod_tipo_lista_dist,
		 :ls_cod_lista_dist
from   tab_tipi_cause 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_causa = :fs_cod_tipo_causa;
	 
select flag_gestione_fasi
into   :ls_flag_gestione_fasi
from   tab_tipi_liste_dist
where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
		 
if ls_flag_gestione_fasi <> "S" then return 0

select stringa
into   :ls_cod_destinatario
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'PCU';

select num_sequenza
into   :ll_sequenza_destinatari
from   det_liste_dist
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
		 cod_lista_dist = :ls_cod_lista_dist and
		 num_sequenza_prec = :ll_num_sequenza_corrente;

//---------------
ll_riga = dw_non_conformita_lista.getrow()
ll_anno = dw_non_conformita_lista.getitemnumber(ll_riga, "anno_non_conf")
ll_num = dw_non_conformita_lista.getitemnumber(ll_riga, "num_non_conf")

select cod_area_aziendale
into :ls_cod_area_aziendale
from non_conformita
where cod_azienda=:s_cs_xx.cod_azienda
	and anno_non_conf=:ll_anno and num_non_conf=:ll_num;
	
if ls_cod_area_aziendale = "" then setnull(ls_cod_area_aziendale)
//------------------

if isnull(ls_cod_area_aziendale) then
	select det_liste_dist_destinatari.flag_responsabile
	into :ls_flag_responsabile
	from det_liste_dist_destinatari
	join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
			and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
	where det_liste_dist_destinatari.cod_azienda=:s_cs_xx.cod_azienda
			and det_liste_dist_destinatari.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
			and det_liste_dist_destinatari.cod_lista_dist=:ls_cod_lista_dist
			and det_liste_dist_destinatari.cod_destinatario=:ls_cod_destinatario
			and det_liste_dist_destinatari.num_sequenza=:ll_sequenza_destinatari
			and tab_ind_dest.cod_utente = :s_cs_xx.cod_utente;
else
	select det_liste_dist_destinatari.flag_responsabile
	into :ls_flag_responsabile
	from det_liste_dist_destinatari
	join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
		and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail	
	where det_liste_dist_destinatari.cod_azienda=:s_cs_xx.cod_azienda
		and det_liste_dist_destinatari.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
		and det_liste_dist_destinatari.cod_lista_dist=:ls_cod_lista_dist
		and det_liste_dist_destinatari.cod_destinatario=:ls_cod_destinatario
		and det_liste_dist_destinatari.num_sequenza=:ll_sequenza_destinatari
		and det_liste_dist_destinatari.flag_responsabile = 'S'
		and det_liste_dist_destinatari.cod_area_aziendale=:ls_cod_area_aziendale
		and tab_ind_dest.cod_utente = :s_cs_xx.cod_utente;
		
		//Donato 07/01/2009 se non hai trovato niente per area aziendale, vuol dire
		//che non è stata specificata. Allora prova a vedere se esiste un responsabile senza area
		//(N.B. deve essere unico altrimenti sarà generato un errore dal programma)
		if sqlca.sqlcode = 100 then
			select det_liste_dist_destinatari.flag_responsabile
			into :ls_flag_responsabile
			from det_liste_dist_destinatari
			join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
					and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
			where det_liste_dist_destinatari.cod_azienda=:s_cs_xx.cod_azienda
					and det_liste_dist_destinatari.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
					and det_liste_dist_destinatari.cod_lista_dist=:ls_cod_lista_dist
					and det_liste_dist_destinatari.cod_destinatario=:ls_cod_destinatario
					and det_liste_dist_destinatari.num_sequenza=:ll_sequenza_destinatari
					and tab_ind_dest.cod_utente = :s_cs_xx.cod_utente;
		end if
end if

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la lettura delle informazioni del responsabile")
	return -1
end if

if ls_flag_responsabile = "S" then
	return 1
else
	return 0
end if

end function

public function integer wf_loaddddw_dw (datawindow dddw_dw, string dddw_column, transaction trans_object, string table_name, string column_code, string column_desc, string join_clause, string where_clause);INTEGER          l_ColNbr,     l_Idx,      l_Jdx
INTEGER          l_ColPos,     l_Start,    l_Count
LONG             l_Error
STRING           l_SQLString,  l_ErrStr
STRING           l_dwDescribe, l_dwModify, l_Attrib, l_ColStr
DATAWINDOWCHILD  l_DWC

STRING           c_ColAttrib[]  =            &
                    { ".BackGround.Color",   & 
                      ".Background.Mode",    &
                      ".Border",             &
                      ".Color",              &
                      ".Font.CharSet",       &
                      ".Font.Face",          &
                      ".Font.Family",        &
                      ".Font.Height",        &
                      ".Font.Italic",        &
                      ".Font.Pitch",         &
                      ".Font.Strikethrough", &
                      ".Font.Underline",     &
                      ".Font.Weight",        &
                      ".Font.Width",         &
                      ".Height",             &
                      ".Width" }

//------------------------------------------------------------------
//  Get the number and name of the DDDW column.
//------------------------------------------------------------------

l_dwDescribe = DDDW_Column + ".ID"
l_ColNbr     = Integer(DDDW_DW.Describe(l_dwDescribe))
l_dwDescribe = "#" + String(l_ColNbr) + ".Name"
l_ColStr     = DDDW_DW.Describe(l_dwDescribe)

//****** Modifica Michele per far funzionare la select di cognome e nome anche su Oracle - 21/09/2001 *******

long   ll_return, ll_pos
string ls_db

if ll_return = -1 then
	g_mb.messagebox("f_po_loaddddw_dw","Errore nella lettura del profilo corrente dal registro")
	return -1
end if

ll_return = Registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "enginetype", ls_db)

if ll_return = -1 then
	g_mb.messagebox("f_po_loaddddw_dw","Errore nella lettura del motore database dal registro")
	return -1
end if

if ls_db = "ORACLE" then
	
	do
		
		ll_pos = pos(Column_Desc,"+",1)
		
		if ll_pos > 0 then
			Column_Desc = replace(Column_Desc,ll_pos,1,"||")
		end if	
	
	loop while ll_pos > 0
	
end if

//******************************************** Fine modifica ************************************************

	
//------------------------------------------------------------------
//  Build the SQL SELECT statement.
//------------------------------------------------------------------

l_SQLString = "SELECT DISTINCT " + Column_Code + " , " + &
				  Column_Desc + " FROM " + Table_Name
				  
if Trim(join_clause) <> "" then
	l_SQLString = l_SQLString + " " + join_clause
end if

IF Trim(Where_Clause) <> "" THEN
	l_SQLString = l_SQLString + " WHERE " + Where_Clause
END IF

// ------------------------------------------------------------------
//   Aggiunto da EnMe per fare Sort su colonna codice
// ------------------------------------------------------------------

l_SQLString = l_SQLString + " ORDER BY " + Column_Code
	
//------------------------------------------------------------------
//  Get the child DataWindow.  If column is not a DDDW column,
//  then return with error.  Otherwise, we need to set the
//  Select statement in the drop down DataWindow.
//------------------------------------------------------------------

l_Error = DDDW_DW.GetChild(l_ColStr, l_DWC)

IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWChildFindError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Set the transaction object.
//------------------------------------------------------------------

l_Error = l_DWC.SetTransObject(trans_object)

IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWTransactionError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Assign the Select statement to the drop down DataWindow.
//------------------------------------------------------------------

l_dwModify = "DataWindow.Table.Select='" + &
             w_POManager_STD.MB.fu_QuoteChar(l_SQLString, "'")  + "'"
l_ErrStr   = l_DWC.Modify(l_dwModify)

IF l_ErrStr <> "" THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWModifySQLError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Retrieve the data for the child DataWindow.
//------------------------------------------------------------------

l_Error = l_DWC.Retrieve()
IF l_Error < 0 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWRetrieveError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//---------- Modifica Michele per togliere spazi dalla lista 04/03/2003 ------------
long ll_i

for ll_i = 1 to l_DWC.rowcount()
	l_DWC.setitem(ll_i,"display_column",trim(l_DWC.getitemstring(ll_i,"display_column")))
next
//------------- Fine Modifica Michele per togliere spazi dalla lista ---------------

//------------------------------------------------------------------
//  Set the attributes of the column to be the same as the
//  parent column.
//------------------------------------------------------------------

l_Jdx = UpperBound(c_ColAttrib[])
FOR l_Idx = 1 TO l_Jdx

   l_dwDescribe = l_ColStr + c_ColAttrib[l_Idx]
   l_Attrib     = DDDW_DW.Describe(l_dwDescribe)

   IF c_ColAttrib[l_Idx] = ".Font.Face" THEN
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "Arial"
      END IF
      l_Attrib = "'" + l_Attrib + "'"
   ELSE
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "0"
      END IF
   END IF

   l_dwModify = "#1" + c_ColAttrib[l_Idx] + "=" + l_Attrib
   l_ErrStr   = l_DWC.Modify(l_dwModify)

NEXT

//------------------------------------------------------------------
//  Make sure the border for the child column is off.
//------------------------------------------------------------------

l_dwModify = "#1" + ".Border=0"
l_ErrStr   = l_DWC.Modify(l_dwModify)

//------------------------------------------------------------------
//  Indicate that there not any errors.
//------------------------------------------------------------------

string ls_Required

ls_Required = dddw_dw.Describe(dddw_column + ".DDDW.Required")

IF ls_Required = "no" THEN
   l_DWC.InsertRow(1)
END IF

RETURN 0
end function

public function string wf_nc_o_reclamo (string as_tipo_causa);//DONATO 30/01/2009
/*
Questa funzione elabora l'argomento "as_tipo_causa", che contiene il codice del tipo causa della NC corrente
Controlla nella parametri_azienda l'esistenza del parametro TCR (Tipo Causa Reclamo)
Il parametro contiene l'elenco dei codici dei tipi causa che servono per la gestione dei reclami, separati da una virgola
es. CD1,CD2,CD3

Se il parametro "as_tipo_causa" contiene uno di questi valori allora la funzione torna "R"
																								 altrimenti torna "N"
																								 
argomenti
	string		as_tipo_causa	(codice del tipo causa della NC corrente)

val. ritorno
	string	(R o N)
*/

string ls_prm_TCR, ls_array[], ls_separetor
integer li_index

select stringa
into :ls_prm_TCR
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'TCR';			
if sqlca.sqlcode = 0 then
else
	//il parametro non è previsto: quindi Non Conformità
	return "N"
end if	

if isnull(ls_prm_TCR) or ls_prm_TCR = "" then return "N"

ls_separetor = ","

if wf_string_to_array(ls_prm_TCR, ls_separetor, ls_array) = 0 then
	//sfoglio l'array per vedere se c'è il codice passato
	for li_index = 1 to upperbound(ls_array)
		if ls_array[li_index] = as_tipo_causa then
			return "R"
		end if
	next
	
	//se arrivi qui vuol dire che il codice tipo causa non è stato trovato tra quelli
	//specificati dal parametro aziendale TCR per la gestione dei reclami
	return "N"
else
	//errore durante l'elaborazione della stringa
	g_mb.messagebox("OMNIA","Errore durante l'interpretazione del del valore parametro TCR", Exclamation!)
	return ""
end if
end function

public function integer wf_string_to_array (string as_input, string as_sep, ref string as_array[]);/*
Function fof_string_to_array
Created by: Michele
Creation Date: 18/12/2007
Comment: Given an input string and a separator string, returns an array of all elements in input string

Revisions
___________________________________________
Name                                   Date                                     Comments
------------------------------------------------------------
Parameters
___________________________________________
Name                                                                                  Comments
as_input                                                                            The input string to process
as_sep                                                                The string to use as separator
as_array[]                                                          Reference to an array of strings, in which we will store the results
------------------------------------------------------------
Return Values
___________________________________________
Value                                                                   Comments
 0                                                                                           Execution completed succesfully
-1                                                                                          Error during execution
------------------------------------------------------------
*/


long                                                      ll_pos, ll_element
string                                    ls_array[], ls_element


as_array = ls_array
ll_element = 0

do 
                ll_pos = pos(as_input,as_sep,1)
                
                if ll_pos > 0 then
                               ls_element        = left(as_input,(ll_pos - 1))
                               as_input                             = right(as_input,(len(as_input) - ll_pos))
                else
                               ls_element        = as_input
                               as_input                             = ""
                end if
                
                ll_element++    
                ls_array[ll_element] = ls_element
loop while ll_pos > 0

as_array = ls_array

return 0

end function

public function datetime wf_seleziona_data_chiusura ();//sintexcal ha chiesto di poter scegliere la data di chiusura della NC

string ls_prova
datetime ldt_chiusura

ldt_chiusura = datetime(today(),now())

select stringa
into   :ls_prova
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
		 
if sqlca.sqlcode = 0 and not isnull(ls_prova) then
	//sintexcal
	open(w_seleziona_data_chiusura)
	
	ldt_chiusura = s_cs_xx.parametri.parametro_data_4
	
	if isnull(ldt_chiusura) or year(date(ldt_chiusura))<1950 then ldt_chiusura = datetime(today(),now())
end if

return ldt_chiusura
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ], lw_oggetti[ ], lw_vuoto[]
string       l_criteriacolumn[ ], l_searchtable[ ], l_searchcolumn[], ls_wizard

//Donato 26-01-2009 parametro per abilitare o meno la gestione protezione dei campi della NC
//in base a quelli specificati nelle fasi del ciclo di vita
is_flag_BNC = "N"

select flag
into :is_flag_BNC
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'BNC' and flag_parametro = 'F';			
if sqlca.sqlcode = 0 then
else
	is_flag_BNC = "N"
end if				
//-----------------------------------------------------------------------------------------------------------------------

// stefanop 27/07/2011: parametro aziendale NWD per abilitare il wizard nella creazione di una nuova non conformita
select flag
into :ls_wizard
from parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_parametro = 'NWD' and 
	flag_parametro = 'F';
	
if sqlca.sqlcode = 0 and ls_wizard = "S" then
	ib_wizard = true
end if				
//-----------------------------------------------------------------------------------------------------------------------

// stefanop 25/010/2011: parametro omnia NCC per abilitare l'apertura finestra dei corsi al salvataggio
select flag
into :ls_wizard
from parametri_omnia
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_parametro = 'NCC' and 
	flag_parametro = 'F';
	
ib_open_costi = (ls_wizard = 'S')	
//-----------------------------------------------------------------------------------------------------------------------

l_objects[1] = dw_non_conformita_std_4
dw_folder.fu_AssignTab(5, "Prevenz./Note", l_Objects[])

l_objects = lw_vuoto
l_objects[1] = dw_non_conformita_std_3
dw_folder.fu_AssignTab(4, "Q.tà/Correzioni", l_Objects[])

l_objects = lw_vuoto
l_objects[1] = dw_non_conformita_std_2
l_objects[2] = mle_stato
dw_folder.fu_AssignTab(3, "Decisioni/Cause", l_Objects[])

l_objects = lw_vuoto
l_objects[1] = dw_non_conformita_std_5
dw_folder.fu_AssignTab(2, "Riferimenti", l_Objects[])

l_objects = lw_vuoto
l_objects[1] = dw_non_conformita_std_1
dw_folder.fu_AssignTab(1, "Generici", l_Objects[])


dw_folder.fu_FolderCreate(5,5)
dw_folder.fu_SelectTab(1)

dw_non_conformita_lista.set_dw_key("cod_azienda")

dw_non_conformita_lista.set_dw_options( sqlca, pcca.null_object, c_noretrieveonopen, c_default)

dw_non_conformita_std_1.set_dw_options( sqlca, dw_non_conformita_lista, c_sharedata + c_scrollparent, c_default)

dw_non_conformita_std_2.set_dw_options( sqlca, dw_non_conformita_lista, c_sharedata + c_scrollparent, c_default)

dw_non_conformita_std_3.set_dw_options( sqlca, dw_non_conformita_lista, c_sharedata + c_scrollparent, c_default)

dw_non_conformita_std_4.set_dw_options( sqlca, dw_non_conformita_lista, c_sharedata + c_scrollparent, c_default)

dw_non_conformita_std_5.set_dw_options( sqlca, dw_non_conformita_lista, c_sharedata + c_scrollparent, c_default)

iuo_dw_main = dw_non_conformita_lista

// --------------------- roba nova ----------------------

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

lw_oggetti[1] = dw_non_conformita_lista
dw_folder_search.fu_assigntab(2, "L", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_nc_ricerca
lw_oggetti[2] = cb_ricerca_1
lw_oggetti[3] = cb_annulla_1
lw_oggetti[4] = sle_filtro_data_registrazione
lw_oggetti[5] = sle_filtro_data_scadenza
lw_oggetti[6] = st_filtro_data
lw_oggetti[7] = st_filtro_data_scad
lw_oggetti[8] = st_nota_1
lw_oggetti[9] = st_nota_3
lw_oggetti[10] = st_nota_4
dw_folder_search.fu_assigntab(1, "R", lw_oggetti[])
dw_folder_search.fu_foldercreate(2, 2)
dw_folder_search.fu_selecttab(1)

l_criteriacolumn[1] = "num_registrazione"
l_criteriacolumn[2] = "anno_registrazione"
l_criteriacolumn[3] = "cod_prodotto"
l_criteriacolumn[4] = "cod_cliente"
l_criteriacolumn[5] = "tipo_causa"
l_criteriacolumn[6] = "causa"
l_criteriacolumn[7] = "difetto"
l_criteriacolumn[8] = "trattamento"
l_criteriacolumn[9] = "flag_tipo_nc"
l_criteriacolumn[10] = "flag_stato_nc"
l_criteriacolumn[11] = "cod_area_aziendale"
//Donato 21-11-2008 Specifica Non_Conformita cmtz
l_criteriacolumn[12] = "flag_nc"
l_criteriacolumn[13] = "flag_provenienza_nc"
//----------------------------------------------------------------

l_searchtable[1] = "non_conformita"
l_searchtable[2] = "non_conformita"
l_searchtable[3] = "non_conformita"
l_searchtable[4] = "non_conformita"
l_searchtable[5] = "non_conformita"
l_searchtable[6] = "non_conformita"
l_searchtable[7] = "non_conformita"
l_searchtable[8] = "non_conformita"
l_searchtable[9] = "non_conformita"
l_searchtable[10] = "non_conformita"
l_searchtable[11] = "non_conformita"
//Donato 21-11-2008 Specifica Non_Conformita cmtz
l_searchtable[12] = "non_conformita"
l_searchtable[13] = "non_conformita"
//----------------------------------------------------------------

l_searchcolumn[1] = "num_non_conf"
l_searchcolumn[2] = "anno_non_conf"
l_searchcolumn[3] = "cod_prodotto"
l_searchcolumn[4] = "cod_cliente"
l_searchcolumn[5] = "cod_tipo_causa"
l_searchcolumn[6] = "cod_causa"
l_searchcolumn[7] = "cod_errore"
l_searchcolumn[8] = "cod_trattamento"
l_searchcolumn[9] = "flag_tipo_nc"
l_searchcolumn[10] = "flag_stato_nc"
l_searchcolumn[11] = "cod_area_aziendale"
//Donato 21-11-2008 Specifica Non_Conformita cmtz
l_searchcolumn[12] = "flag_nc"
l_searchcolumn[13] = "flag_provenienza_nc"
//----------------------------------------------------------------

dw_nc_ricerca.fu_wiredw(l_criteriacolumn[], &
                        dw_non_conformita_lista, &
							   l_searchtable[], &
							   l_searchcolumn[], &
							   SQLCA)

dw_nc_ricerca.setitem(1, "anno_registrazione", f_anno_esercizio())

sle_filtro_data_registrazione.fu_WireDW(dw_non_conformita_lista, &
												    "non_conformita", &
												    "data_creazione", &
												    SQLCA)
													 
sle_filtro_data_scadenza.fu_WireDW(dw_non_conformita_lista, &
												    "non_conformita", &
												    "data_scadenza", &
												    SQLCA)													 

il_orig_val = dw_nc_ricerca.getitemnumber(1, "num_registrazione")
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_trattamento",sqlca,&
                 "tab_trattamenti","cod_trattamento","des_trattamento", &
                 "tab_trattamenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_fornitore",sqlca,&
//                 "anag_fornitori","cod_fornitore","rag_soc_1", &
//                 "anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//					  
//Donato 30/01/2009 aggiunto campo cliente
//f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_cliente",sqlca,&
//                 "anag_clienti","cod_cliente","rag_soc_1", &
//                 "anag_clienti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"tipo_prodotto",sqlca,&
                 "tab_tipi_prodotto","tipo_prodotto","desc_prodotto", &
                 "tab_tipi_prodotto.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_errore",sqlca,&
                 "tab_difformita","cod_errore","des_difformita", &
                 "tab_difformita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

/* f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome", &
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'") */

//Donato 26-11-2008
if s_cs_xx.cod_utente <> "CS_SYSTEM" and is_flag_BNC = "S" then
	
	wf_loaddddw_dw( dw_non_conformita_std_1, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"b.cod_tipo_causa", &
                     			"b.des_tipo_causa", &
							"as b "+&
								"join (select 	x.cod_azienda,"+&
											"x.cod_tipo_lista_dist,"+&
											"x.cod_lista_dist,"+&
											"x.cod_destinatario "+&
										"from det_liste_dist_destinatari as x "+&
										"join det_liste_dist as y on y.cod_azienda=x.cod_azienda "+&
											"and y.cod_tipo_lista_dist=x.cod_tipo_lista_dist "+&
											"and y.cod_lista_dist=x.cod_lista_dist "+&
											"and y.cod_destinatario=x.cod_destinatario "+&
											"and y.num_sequenza=x.num_sequenza "+&
										"join tab_ind_dest as z on z.cod_azienda=x.cod_azienda "+&
											"and z.cod_destinatario= x.cod_destinatario_mail " +&							
										"where z.cod_utente = '"+s_cs_xx.cod_utente+"' "+&
											"and (y.num_sequenza_prec=0 or y.num_sequenza_prec is null) "+&
										"group by x.cod_azienda,x.cod_tipo_lista_dist,x.cod_lista_dist,x.cod_destinatario "+&
										  ") as a on a.cod_azienda=b.cod_azienda "+&
											"and a.cod_tipo_lista_dist=b.cod_tipo_lista_dist "+&
											"and b.cod_lista_dist=a.cod_lista_dist ",&
							"b.cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((b.flag_blocco <> 'S') or (b.flag_blocco = 'S' and b.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	/*
	wf_loaddddw_dw( dw_non_conformita_std_1, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     			"des_tipo_causa", &
							"as b "+&
								"join (select cod_azienda,cod_tipo_lista_dist,cod_lista_dist,"+&
												"cod_destinatario,min(num_sequenza) as num_sequenza "+&
											"from det_liste_dist_destinatari "+&
											"where cod_destinatario_mail = '"+s_cs_xx.cod_utente+"' "+&
											"group by cod_azienda,cod_tipo_lista_dist,cod_lista_dist,cod_destinatario "+&
										") as a on a.cod_tipo_lista_dist=b.cod_tipo_lista_dist "+&
															"and b.cod_lista_dist=a.cod_lista_dist ",&
							"b.cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((b.flag_blocco <> 'S') or (b.flag_blocco = 'S' and b.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	*/
else
	//f_PO_LoadDDDW_DW( dw_non_conformita_std_1, &
	wf_loaddddw_dw( dw_non_conformita_std_1, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", "",&
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end if
/*
f_PO_LoadDDDW_DW( dw_non_conformita_std_1, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
*/
//fine modifica --------------------------

f_PO_LoadDDDW_DW( dw_non_conformita_std_1, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_std_1,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_std_4,"cod_resp_ver_efficacia",sqlca,&
                 "mansionari","cod_resp_divisione","nome", &
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_non_conformita_std_4,"firma_chiusura",sqlca,&
                 "mansionari","cod_resp_divisione","nome", &
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
dw_nc_ricerca.fu_loadcode("tipo_causa", &
      			           "tab_tipi_cause", &
								  "cod_tipo_causa", &
								  "des_tipo_causa", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", "" )

dw_nc_ricerca.fu_loadcode("causa", &
      			           "cause", &
								  "cod_causa", &
								  "des_causa", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", "" )
								  
dw_nc_ricerca.fu_loadcode("difetto", &
      			           "tab_difformita", &
								  "cod_errore", &
								  "des_difformita", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", "" )
								  
dw_nc_ricerca.fu_loadcode("trattamento", &
      			           "tab_trattamenti", &
								  "cod_trattamento", &
								  "des_trattamento", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", "" )								  
								  
dw_nc_ricerca.fu_loadcode("cod_area_aziendale", &
      			           "tab_aree_aziendali", &
								  "cod_area_aziendale", &
								  "des_area", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", "" )

f_PO_LoadDDDW_DW(dw_non_conformita_std_3,"cod_centro_costo",sqlca,&
                 "tab_centri_costo","cod_centro_costo","des_centro_costo", &
                 "tab_centri_costo.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
// *** michela 31/03/2005: se questo parametro è a si allora riempio il campo responsabile
string ls_flag

select flag
into   :ls_flag
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CCR';
		 
ib_visualizza_campi = false		 
		 
if sqlca.sqlcode = 0 and ls_flag = 'S' and not isnull(ls_flag) then
	
	ib_visualizza_campi = true
	
	f_PO_LoadDDDW_DW(dw_non_conformita_std_3,"cod_resp_reso",sqlca,&
						  "tab_resp_resi","cod_resp_reso","des_responsabile", &
						  "tab_resp_resi.cod_azienda = '" + s_cs_xx.cod_azienda + "' and (flag_reso_commerciale = 'N' or flag_reso_commerciale is null) ")	
	
end if


end event

on w_non_conformita_std.create
int iCurrent
call super::create
this.cb_lotti=create cb_lotti
this.sle_filtro_data_scadenza=create sle_filtro_data_scadenza
this.st_nota_4=create st_nota_4
this.st_nota_3=create st_nota_3
this.st_nota_1=create st_nota_1
this.st_filtro_data=create st_filtro_data
this.sle_filtro_data_registrazione=create sle_filtro_data_registrazione
this.st_filtro_data_scad=create st_filtro_data_scad
this.cb_annulla_1=create cb_annulla_1
this.cb_chiusura=create cb_chiusura
this.cb_comunicazioni=create cb_comunicazioni
this.cb_costi=create cb_costi
this.cb_dettagli=create cb_dettagli
this.cb_doc_aut_deroga=create cb_doc_aut_deroga
this.cb_doc_compilato=create cb_doc_compilato
this.cb_invia=create cb_invia
this.cb_report=create cb_report
this.cb_respingi=create cb_respingi
this.cb_ricerca_1=create cb_ricerca_1
this.dw_non_conformita_lista=create dw_non_conformita_lista
this.dw_folder_search=create dw_folder_search
this.dw_non_conformita_std_1=create dw_non_conformita_std_1
this.dw_non_conformita_std_3=create dw_non_conformita_std_3
this.dw_non_conformita_std_4=create dw_non_conformita_std_4
this.dw_non_conformita_std_2=create dw_non_conformita_std_2
this.mle_stato=create mle_stato
this.dw_folder=create dw_folder
this.dw_nc_ricerca=create dw_nc_ricerca
this.dw_non_conformita_std_5=create dw_non_conformita_std_5
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_lotti
this.Control[iCurrent+2]=this.sle_filtro_data_scadenza
this.Control[iCurrent+3]=this.st_nota_4
this.Control[iCurrent+4]=this.st_nota_3
this.Control[iCurrent+5]=this.st_nota_1
this.Control[iCurrent+6]=this.st_filtro_data
this.Control[iCurrent+7]=this.sle_filtro_data_registrazione
this.Control[iCurrent+8]=this.st_filtro_data_scad
this.Control[iCurrent+9]=this.cb_annulla_1
this.Control[iCurrent+10]=this.cb_chiusura
this.Control[iCurrent+11]=this.cb_comunicazioni
this.Control[iCurrent+12]=this.cb_costi
this.Control[iCurrent+13]=this.cb_dettagli
this.Control[iCurrent+14]=this.cb_doc_aut_deroga
this.Control[iCurrent+15]=this.cb_doc_compilato
this.Control[iCurrent+16]=this.cb_invia
this.Control[iCurrent+17]=this.cb_report
this.Control[iCurrent+18]=this.cb_respingi
this.Control[iCurrent+19]=this.cb_ricerca_1
this.Control[iCurrent+20]=this.dw_non_conformita_lista
this.Control[iCurrent+21]=this.dw_folder_search
this.Control[iCurrent+22]=this.dw_non_conformita_std_1
this.Control[iCurrent+23]=this.dw_non_conformita_std_3
this.Control[iCurrent+24]=this.dw_non_conformita_std_4
this.Control[iCurrent+25]=this.dw_non_conformita_std_2
this.Control[iCurrent+26]=this.mle_stato
this.Control[iCurrent+27]=this.dw_folder
this.Control[iCurrent+28]=this.dw_nc_ricerca
this.Control[iCurrent+29]=this.dw_non_conformita_std_5
end on

on w_non_conformita_std.destroy
call super::destroy
destroy(this.cb_lotti)
destroy(this.sle_filtro_data_scadenza)
destroy(this.st_nota_4)
destroy(this.st_nota_3)
destroy(this.st_nota_1)
destroy(this.st_filtro_data)
destroy(this.sle_filtro_data_registrazione)
destroy(this.st_filtro_data_scad)
destroy(this.cb_annulla_1)
destroy(this.cb_chiusura)
destroy(this.cb_comunicazioni)
destroy(this.cb_costi)
destroy(this.cb_dettagli)
destroy(this.cb_doc_aut_deroga)
destroy(this.cb_doc_compilato)
destroy(this.cb_invia)
destroy(this.cb_report)
destroy(this.cb_respingi)
destroy(this.cb_ricerca_1)
destroy(this.dw_non_conformita_lista)
destroy(this.dw_folder_search)
destroy(this.dw_non_conformita_std_1)
destroy(this.dw_non_conformita_std_3)
destroy(this.dw_non_conformita_std_4)
destroy(this.dw_non_conformita_std_2)
destroy(this.mle_stato)
destroy(this.dw_folder)
destroy(this.dw_nc_ricerca)
destroy(this.dw_non_conformita_std_5)
end on

event pc_new;if is_flag_apertura_nc = 'N' then
	g_mb.messagebox("Omnia", "Utente non abilitato all'apertura di una Non Conformità")
	return
end if	

CALL Super::pc_new


dw_non_conformita_std_1.setitem(dw_non_conformita_std_1.getrow(), "data_creazione", datetime(today()))


end event

event open;call super::open;//Giulio: 10/11/2011 cambio gestione privilegi mansionario
//select flag_apertura_nc,
//		 flag_chiusura_nc
//  into :is_flag_apertura_nc,
//	    :is_flag_chiusura_nc 
//  from mansionari
// where (mansionari.cod_azienda=:s_cs_xx.cod_azienda) and 
//	    (mansionari.cod_utente =:s_cs_xx.cod_utente);

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

is_flag_apertura_nc = 'N'
is_flag_chiusura_nc = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.apertura_nc) then is_flag_apertura_nc = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.chiusura_nc) then is_flag_chiusura_nc = 'S'

//if sqlca.sqlcode < 0 then	
if isnull (luo_mansionario.uof_get_cod_mansionario())then
	g_mb.messagebox("Omnia", "Errore in lettura dati da tabella mansionari. Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if	
//--- Fine modifica Giulio
end event

type cb_lotti from commandbutton within w_non_conformita_std
integer x = 2537
integer y = 2040
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stock"
end type

event clicked;//se l'utente non è un resp. di fase esci
long ll_num_sequenza_corrente, ll_riga, ll_anno_nc, ll_num_nc
integer li_ret
string ls_cod_tipo_causa, ls_firma_chiusura, ls_cod_prodotto

ll_riga = dw_non_conformita_lista.getrow()
if ll_riga <=0 then return

ll_num_sequenza_corrente = dw_non_conformita_lista.getitemnumber(ll_riga, "num_sequenza_corrente")
ls_cod_tipo_causa = dw_non_conformita_lista.getitemstring( ll_riga, "cod_tipo_causa")
ls_firma_chiusura = dw_non_conformita_lista.getitemstring( ll_riga, "firma_chiusura")

//Donato 29/01/2009 verifica se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	if not isnull(ls_firma_chiusura) and ls_firma_chiusura <> "" then
		//Conclusa
		if s_cs_xx.cod_utente = "CS_SYSTEM" then
			li_ret = 1
		else
			return
		end if
	else
		
		//controlla se sei all'inizio --------
		//NUOVO CODICE
		if ll_num_sequenza_corrente= 0 or isnull(ll_num_sequenza_corrente) then
			//si tratta della prima fase: fai continuare
			li_ret = 1
		else
			//fai quello che faceva prima:
			//serve che si tratti del responsabile
			if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 &
								and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
				li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)		
			else
				//inizio
				li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)
			end if
		end if
		
		//vecchio codice
		/*
		if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 &
							and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)		
		else
			//inizio
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)
		end if
		*/
		//fine modifica -------------------------------------------------
	end if
	
	if li_ret = 1 then
		//responsabile OK			
	elseif li_ret = 0 then
		//non responsabile
		g_mb.messagebox( "OMNIA", "Non Autorizzato!",Exclamation!)	
		return
	else
		//qualcosa non quadra e c'è stato un errore (msg già dato)
		return
	end if
	//fine modifica -------------------------------------------------------------
end if

//controlla se esiste il prodotto
ll_anno_nc = dw_non_conformita_lista.getitemnumber(ll_riga, "anno_non_conf")
ll_num_nc = dw_non_conformita_lista.getitemnumber(ll_riga, "num_non_conf")

select cod_prodotto
into :ls_cod_prodotto
from non_conformita
where cod_azienda = :s_cs_xx.cod_azienda and
	anno_non_conf = :ll_anno_nc and num_non_conf = :ll_num_nc;
	
if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
	g_mb.messagebox( "OMNIA", "E' necessario selezionare il prodotto a cui è associata la N.C.!",Exclamation!)	
	return
end if

//apri la maschera di dettaglio
if not isvalid(w_non_conformita_lotti) then
   s_cs_xx.parametri.parametro_d_1 = ll_anno_nc
   s_cs_xx.parametri.parametro_d_2 = ll_num_nc
   if s_cs_xx.parametri.parametro_d_1 > 0 and s_cs_xx.parametri.parametro_d_2 > 0 then
      window_open_parm(w_non_conformita_lotti, -1, dw_non_conformita_lista)
   end if
end if


end event

type sle_filtro_data_scadenza from u_sle_search within w_non_conformita_std
integer x = 2034
integer y = 660
integer width = 823
integer height = 80
integer taborder = 10
end type

type st_nota_4 from statictext within w_non_conformita_std
integer x = 2514
integer y = 1080
integer width = 768
integer height = 120
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "Per filtrare dal 15/03/2002 in poi scrivere ~" > 15/03/2002~""
boolean focusrectangle = false
end type

type st_nota_3 from statictext within w_non_conformita_std
integer x = 2514
integer y = 840
integer width = 768
integer height = 260
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "Per filtrare, per esempio,  dal 15/03/2002 al 30/03/2002 scrivere: ~"15/03/2002 to 30/03/2002~""
boolean focusrectangle = false
end type

type st_nota_1 from statictext within w_non_conformita_std
integer x = 2514
integer y = 780
integer width = 777
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Esempi di filtro per data."
boolean focusrectangle = false
end type

type st_filtro_data from statictext within w_non_conformita_std
integer x = 183
integer y = 660
integer width = 297
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Data Reg.:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_filtro_data_registrazione from u_sle_search within w_non_conformita_std
integer x = 503
integer y = 660
integer width = 823
integer height = 80
integer taborder = 110
boolean bringtotop = true
end type

type st_filtro_data_scad from statictext within w_non_conformita_std
integer x = 1714
integer y = 660
integer width = 320
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Data Scad.:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_annulla_1 from commandbutton within w_non_conformita_std
integer x = 2880
integer y = 140
integer width = 361
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_nc_ricerca.fu_Reset()

end event

type cb_chiusura from commandbutton within w_non_conformita_std
integer x = 2926
integer y = 2040
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiusura"
end type

event clicked;string   ls_cod_resp, ls_difetto, ls_tipo_lista, ls_cod_lista, ls_cod_tipo_causa

long     ll_anno, ll_num, ll_row, ll_num_sequenza_corrente, ll_sequenza_destinatari

datetime ldt_chiusura

/*
ls_cod_resp = dw_non_conformita_lista.getitemstring(dw_non_conformita_lista.getrow(),"firma_chiusura")
ldt_chiusura = dw_non_conformita_lista.getitemdatetime(dw_non_conformita_lista.getrow(),"data_chiusura")

if not isnull(ls_cod_resp) and not isnull(ldt_chiusura) then	
	g_mb.messagebox("OMNIA","Questa non conformità è già stata chiusa in data " + string(ldt_chiusura) + " dal mansionario " + ls_cod_resp)
	return -1
end if
*/

//Abilitare la chiusura dal pulsante solo se siamo nell'ultima fase

//questi dati li prendiamo direttamente dalla tabella
ll_anno = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "anno_non_conf")
ll_num = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "num_non_conf")

select num_sequenza_corrente,
		cod_tipo_causa,
		cod_errore,
		firma_chiusura,
		data_chiusura
into
	:ll_num_sequenza_corrente,
	:ls_cod_tipo_causa,
	:ls_difetto,
	:ls_cod_resp,
	:ldt_chiusura
from non_conformita
where cod_azienda = :s_cs_xx.cod_azienda
	and anno_non_conf = :ll_anno and num_non_conf = :ll_num;
	
if not isnull(ls_cod_resp) and not isnull(ldt_chiusura) then	
	g_mb.messagebox("OMNIA","Questa non conformità è già stata chiusa in data " + string(ldt_chiusura) + " dal mansionario " + ls_cod_resp)
	return -1
end if

//Donato 12-01-2009 prima procedi per tipo causa, altrimenti vai per difetto
if not isnull(ls_cod_tipo_causa) or ls_cod_tipo_causa <> "" then
	select cod_tipo_lista_dist,
			 cod_lista_dist
	into   :ls_tipo_lista,
			 :ls_cod_lista
	from   tab_tipi_cause
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_causa = :ls_cod_tipo_causa;
else
	select cod_tipo_lista_dist,
			 cod_lista_dist
	into   :ls_tipo_lista,
			 :ls_cod_lista
	from   tab_difformita
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_errore = :ls_difetto;
end if
/*
select cod_tipo_lista_dist,
			 cod_lista_dist
	into   :ls_tipo_lista,
			 :ls_cod_lista
	from   tab_difformita
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_errore = :ls_difetto;
*/
//fine modifica ------------------------------------------------------------------------------

select num_sequenza
into   :ll_sequenza_destinatari
from   det_liste_dist
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_lista_dist = :ls_tipo_lista and
		 cod_lista_dist = :ls_cod_lista and
		 num_sequenza_prec = :ll_num_sequenza_corrente;

ll_num_sequenza_corrente = ll_sequenza_destinatari
setnull(ll_sequenza_destinatari)

//Donato 26-01-2009 verifica se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	//vedo se ci sono fasi successive
	select num_sequenza
	into   :ll_sequenza_destinatari
	from   det_liste_dist
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_lista_dist = :ls_tipo_lista and
			 cod_lista_dist = :ls_cod_lista and
			 num_sequenza_prec = :ll_num_sequenza_corrente;
			 
	if isnull(ll_sequenza_destinatari) or ll_sequenza_destinatari = 0 then
		// in questo caso sono nell'ultima fase
	else
		messagebox( "OMNIA", "Impossibile procedere alla chiusura del documento: ci sono altre fasi nel ciclo di vita!")
		return -1
	end if
	//---------------------
end if

if g_mb.messagebox("OMNIA","Chiudere la non conformità?",question!,yesno!,2) = 2 then
	return -1
end if

setnull(ls_cod_resp)

select cod_resp_divisione
into   :ls_cod_resp
from   mansionari
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_chiusura_nc = 'S';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_cod_resp) then
	g_mb.messagebox("OMNIA","Il mansionario corrente non è abilitato alla chiusura delle non conformità")
	return -1
end if

ll_anno = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(),"anno_non_conf")
ll_num = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(),"num_non_conf")

//sintexcal ha chiesto di poter scegliere la data chiusura della NC
ldt_chiusura = wf_seleziona_data_chiusura()
//ldt_chiusura = datetime(today(),now())

update non_conformita
set    firma_chiusura = :ls_cod_resp,
       data_chiusura = :ldt_chiusura
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_non_conf = :ll_anno and
		 num_non_conf = :ll_num;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in chiusura non_conformità.~nErrore nella update di non_conformita: " + sqlca.sqlerrtext)
	return -1
end if

ll_row = dw_non_conformita_lista.getrow()

dw_non_conformita_lista.triggerevent("pcd_retrieve")

dw_non_conformita_lista.setrow(ll_row)

dw_non_conformita_lista.scrolltorow(ll_row)

if s_cs_xx.num_livello_mail > 0 then

	ls_difetto = dw_non_conformita_lista.getitemstring(ll_row,"cod_errore")
		
	select cod_tipo_lista_dist,
			 cod_lista_dist
	into   :ls_tipo_lista,
			 :ls_cod_lista
	from   tab_difformita
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_errore = :ls_difetto;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in lettura lista distribuzione da tab_difformita: " + sqlca.sqlerrtext)
		return
	elseif sqlca.sqlcode = 100 then
		return
	end if
	
	s_cs_xx.parametri.parametro_s_1 = "L"
	s_cs_xx.parametri.parametro_s_2 = ls_tipo_lista
	s_cs_xx.parametri.parametro_s_3 = ls_cod_lista
	s_cs_xx.parametri.parametro_s_4 = ""
	s_cs_xx.parametri.parametro_s_5 = "Chiusura Non Conformità " + string(ll_anno) + "/" + string(ll_num)
	s_cs_xx.parametri.parametro_s_6 = "E' stata chiusa la Non Conformità " + string(ll_anno) + "/" + string(ll_num)
	s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
	s_cs_xx.parametri.parametro_s_8 = ""
	
	openwithparm(w_invio_messaggi,0)

end if
end event

type cb_comunicazioni from commandbutton within w_non_conformita_std
integer x = 2926
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Co&munic."
end type

event clicked;long   ll_numero, ll_anno, ll_row

ll_row = dw_non_conformita_lista.getrow()

if ll_row < 1 then return

ll_anno = dw_non_conformita_lista.getitemnumber(ll_row, "anno_non_conf")
ll_numero = dw_non_conformita_lista.getitemnumber(ll_row, "num_non_conf")

setnull(s_cs_xx.parametri.parametro_ul_1)
setnull(s_cs_xx.parametri.parametro_ul_2)
setnull(s_cs_xx.parametri.parametro_ul_3)
setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)

s_cs_xx.parametri.parametro_d_1 = ll_anno
s_cs_xx.parametri.parametro_d_2 = ll_numero

window_open(w_schede_intervento_comunicazioni,-1)


end event

type cb_costi from commandbutton within w_non_conformita_std
integer x = 2537
integer y = 2140
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Costi"
end type

event clicked;long ll_row, ll_anno, ll_num


ll_row = dw_non_conformita_lista.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("OMNIA","Selezionare una non conformità!",stopsign!)
	return -1
end if

ll_anno = dw_non_conformita_lista.getitemnumber(ll_row,"anno_non_conf")

ll_num = dw_non_conformita_lista.getitemnumber(ll_row,"num_non_conf")

if isnull(ll_anno) or ll_anno = 0 or isnull(ll_num) or ll_num = 0 then
	g_mb.messagebox("OMNIA","Selezionare una non conformità!",stopsign!)
	return -1
end if

window_open_parm(w_nc_costi,-1,dw_non_conformita_lista)
end event

type cb_dettagli from commandbutton within w_non_conformita_std
integer x = 2926
integer y = 1940
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dettaglio"
end type

event clicked;//if not isvalid(w_non_conformita_interna) then
//   s_cs_xx.parametri.parametro_d_1 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "anno_non_conf")
//   s_cs_xx.parametri.parametro_d_2 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "num_non_conf")
//   parent.hide()
//   window_open(w_non_conformita_interna, -1)
//end if
//
//

end event

type cb_doc_aut_deroga from cb_documenti_compilati within w_non_conformita_std
boolean visible = false
integer x = 1440
integer y = 1680
integer width = 69
integer height = 80
integer taborder = 10
boolean bringtotop = true
string text = "..."
end type

event clicked;call super::clicked;string ls_ritorno, ls_setting

ls_setting = dw_non_conformita_std_1.Describe("nome_doc_aut_deroga.Protect")

if ls_setting = "1" then return

s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "nome_doc_aut_deroga"
s_cs_xx.parametri.parametro_s_2 = "DQ4"
s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_std_2.getitemstring(dw_non_conformita_std_2.getrow(), "nome_doc_aut_deroga")

if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)

   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
      s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
      s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_ritorno)
   end if      
else
   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
end if


end event

type cb_doc_compilato from cb_documenti_compilati within w_non_conformita_std
boolean visible = false
integer x = 2309
integer y = 1800
integer width = 69
integer height = 80
integer taborder = 20
boolean bringtotop = true
string text = "..."
end type

on clicked;call cb_documenti_compilati::clicked;string ls_ritorno

s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "nome_doc_compilato"
s_cs_xx.parametri.parametro_s_2 = "DQ3"
s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_std_3.getitemstring(dw_non_conformita_std_3.getrow(), "nome_doc_compilato")

if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)

   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
      s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
      s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_ritorno)
   end if      
else
   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
end if


end on

type cb_invia from commandbutton within w_non_conformita_std
integer x = 2926
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "I&nvia"
end type

event clicked;string ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi, ls_cod_area_aziendale, ls_cod_prodotto, &
       ls_cod_cat_mer, ls_firma_chiusura
integer li_ret
long   ll_riga, ll_anno, ll_num, ll_num_sequenza_corrente

ll_riga = dw_non_conformita_lista.getrow()

if ll_riga < 1 then return

//Donato 25-11-2008 se l'utente non è un resp. di fase esci
ll_num_sequenza_corrente = dw_non_conformita_lista.getitemnumber(ll_riga, "num_sequenza_corrente")
ls_cod_tipo_causa = dw_non_conformita_lista.getitemstring( ll_riga, "cod_tipo_causa")
ls_firma_chiusura = dw_non_conformita_lista.getitemstring( ll_riga, "firma_chiusura")

//questi dati li prendiamo direttamente dalla tabella
ll_anno = dw_non_conformita_lista.getitemnumber( ll_riga, "anno_non_conf")
ll_num = dw_non_conformita_lista.getitemnumber( ll_riga, "num_non_conf")

//Donato 26-01-2009 controlla se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	select num_sequenza_corrente,
			cod_tipo_causa,
			firma_chiusura
	into
		:ll_num_sequenza_corrente,
		:ls_cod_tipo_causa,
		:ls_firma_chiusura
	from non_conformita
	where cod_azienda = :s_cs_xx.cod_azienda
		and anno_non_conf = :ll_anno and num_non_conf = :ll_num;
	//---------------------
	
	if sqlca.sqlcode <> 0 then
		return
	end if
	
	if not isnull(ls_firma_chiusura) and ls_firma_chiusura <> "" then
		//Conclusa
		return
	else
		if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 &
							and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)		
		else
			//inizio: abilitare
			//li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)
			li_ret = 1
		end if
	end if
	
	if li_ret = 1 then
		//responsabile OK			
	elseif li_ret = 0 then
		//non responsabile
		g_mb.messagebox( "OMNIA", "Non Autorizzato!",Exclamation!)	
		return
	else
		//qualcosa non quadra e c'è stato un errore (msg già dato)
		return
	end if
	//fine modifica -------------------------------------------------------------
end if

ll_anno = dw_non_conformita_lista.getitemnumber( ll_riga, "anno_non_conf")
ll_num = dw_non_conformita_lista.getitemnumber( ll_riga, "num_non_conf")
ls_cod_tipo_causa = dw_non_conformita_lista.getitemstring( ll_riga, "cod_tipo_causa")
			
if not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
				
	select cod_tipo_lista_dist,
	       cod_lista_dist
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_cause
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_causa = :ls_cod_tipo_causa;
						 
	if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
					
		select flag_gestione_fasi
		into   :ls_flag_gestione_fasi
		from   tab_tipi_liste_dist
		where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
					
		if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
		
		if ls_flag_gestione_fasi <> "S" then return
					
		select cod_area_aziendale,
		       cod_prodotto,
				 num_sequenza_corrente
		into   :ls_cod_area_aziendale,
		       :ls_cod_prodotto,
				 :ll_num_sequenza_corrente
		from   non_conformita 
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_non_conf = :ll_anno and
				 num_non_conf = :ll_num;
				 
		select cod_cat_mer
		into   :ls_cod_cat_mer
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
				 
		if isnull(ls_cod_cat_mer) then ls_cod_cat_mer = ""
		
		if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""		
						
		s_cs_xx.parametri.parametro_s_1 = "L"
		s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
		s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
		s_cs_xx.parametri.parametro_s_4 = ""
		s_cs_xx.parametri.parametro_s_5 = "E' stata modificata la Non conformità " + string(ll_anno) + "/" + string(ll_num)
		s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno) + "/" + string(ll_num)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari della fase successiva?"
		s_cs_xx.parametri.parametro_s_8 = ""
		s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
		s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer
		s_cs_xx.parametri.parametro_s_11 = ""
		s_cs_xx.parametri.parametro_s_12 = ""
		s_cs_xx.parametri.parametro_s_13 = string(ll_anno)
		s_cs_xx.parametri.parametro_s_14 = string(ll_num)
		s_cs_xx.parametri.parametro_s_15 = "N"
		s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
		s_cs_xx.parametri.parametro_b_1 = true
							
		openwithparm( w_invio_messaggi, 0)	
		
		if not isnull(s_cs_xx.parametri.parametro_ul_1) then
			
			if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
			
			update non_conformita
			set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_non_conf = :ll_anno and
					 num_non_conf = :ll_num;
					 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
				rollback;
				return -1
			end if
			
			commit;
			
			parent.triggerevent("pc_retrieve")
			
			dw_non_conformita_lista.setrow( ll_riga)					 
			
		end if				
					
	elseif sqlca.sqlcode < 0 then
				
		g_mb.messagebox( "OMNIA", "Errore durante la ricerca della lista di distribuzione: " + sqlca.sqlerrtext)						
		return -1
		
	else
		
		g_mb.messagebox( "OMNIA", "Attenzione: controllare l'associazione tipo causa / tipo lista!")
		return -1
		
	end if
					
end if				
end event

type cb_report from commandbutton within w_non_conformita_std
integer x = 2926
integer y = 2140
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_tipo_report, ls_tipo_causa

if not isvalid(w_report_nc) then
	
	if dw_non_conformita_lista.getrow() < 1 then
		g_mb.messagebox("OMNIA","Selezionare una non conformità dalla lista",stopsign!)
		return -1
	end if

	s_cs_xx.parametri.parametro_d_1 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(),"anno_non_conf")
	s_cs_xx.parametri.parametro_d_2 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(),"num_non_conf")
	
	//Donato 26-01-2009 controlla se è abilitato il blocco dei campi
	if is_flag_BNC = "S" then
		//prima di aprire il report verifica se si tratta di una NC o di un reclamo
		ls_tipo_causa = dw_non_conformita_lista.getitemstring(dw_non_conformita_lista.getrow(),"cod_tipo_causa")
		if isnull(ls_tipo_causa) or ls_tipo_causa = "" then
			g_mb.messagebox("OMNIA", "Impossibile stabilire il tipo di report: campo Tipo Causa non specificato!")			
			return
		end if
		
		ls_tipo_report = wf_nc_o_reclamo(ls_tipo_causa)
		choose case ls_tipo_report
			case "N"
				window_open(w_report_nc2,-1)
				
			case "R"
				window_open(w_report_nc_reclami,-1)
				
		end choose
		
		//window_open(w_report_nc2,-1)
		
	else
		//report classico
		window_open(w_report_nc,-1)
	end if
	//vecchio codice
	/*
	//Donato 26-11-2008
	window_open(w_report_nc2,-1)
	//window_open(w_report_nc,-1)
	*/
	//fine modifica
end if
end event

type cb_respingi from commandbutton within w_non_conformita_std
integer x = 2926
integer y = 1320
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Re&spingi"
end type

event clicked;string ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi, ls_cod_area_aziendale, ls_cod_prodotto, ls_cod_cat_mer
string ls_firma_chiusura
long   ll_riga, ll_anno, ll_num, ll_num_sequenza_corrente, li_ret

ll_riga = dw_non_conformita_lista.getrow()

if ll_riga < 1 then return

ll_anno = dw_non_conformita_lista.getitemnumber( ll_riga, "anno_non_conf")
ll_num = dw_non_conformita_lista.getitemnumber( ll_riga, "num_non_conf")
ls_cod_tipo_causa = dw_non_conformita_lista.getitemstring( ll_riga, "cod_tipo_causa")
			
//Donato 26-01-2009 verifica se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	//questi dati li prendiamo direttamente dalla tabella
	ll_anno = dw_non_conformita_lista.getitemnumber( ll_riga, "anno_non_conf")
	ll_num = dw_non_conformita_lista.getitemnumber( ll_riga, "num_non_conf")
	
	select num_sequenza_corrente,
			cod_tipo_causa,
			firma_chiusura
	into
		:ll_num_sequenza_corrente,
		:ls_cod_tipo_causa,
		:ls_firma_chiusura
	from non_conformita
	where cod_azienda = :s_cs_xx.cod_azienda
		and anno_non_conf = :ll_anno and num_non_conf = :ll_num;
	//---------------------
	
	if sqlca.sqlcode <> 0 then
		return
	end if
	
	if not isnull(ls_firma_chiusura) and ls_firma_chiusura <> "" then
		//Conclusa: impossibile respingere
		return
	else
		if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 &
							and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)		
		else
			//inizio: non esistono fasi precedenti
			li_ret = 2
		end if
	end if
	
	if li_ret = 1 then
		//responsabile OK			
	elseif li_ret = 0 then
		//non responsabile
		g_mb.messagebox( "OMNIA", "Non Autorizzato!",Exclamation!)	
		return
	elseif  li_ret = 2 then
		//non esistono fasi precedenti
		return
	else
		//qualcosa non quadra e c'è stato un errore (msg già dato)
		return
	end if
	//fine modifica -------------------------------------------------------------	
end if
			
if not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
				
	select cod_tipo_lista_dist,
	       cod_lista_dist
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_cause
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_causa = :ls_cod_tipo_causa;
						 
	if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
					
		select flag_gestione_fasi
		into   :ls_flag_gestione_fasi
		from   tab_tipi_liste_dist
		where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
					
		if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
		
		if ls_flag_gestione_fasi <> "S" then return
		
		select cod_area_aziendale,
		       cod_prodotto,
				 num_sequenza_corrente
		into   :ls_cod_area_aziendale,
		       :ls_cod_prodotto,
				 :ll_num_sequenza_corrente
		from   non_conformita 
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_non_conf = :ll_anno and
				 num_non_conf = :ll_num;
		
		select cod_cat_mer
		into   :ls_cod_cat_mer
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
				 
		if isnull(ls_cod_cat_mer) then ls_cod_cat_mer = ""
		
		if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""
					
		s_cs_xx.parametri.parametro_s_1 = "L"
		s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
		s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
		s_cs_xx.parametri.parametro_s_4 = ""
		s_cs_xx.parametri.parametro_s_5 = "E' stata modificata la Non Conformità " + string(ll_anno) + "/" + string(ll_num)
		s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno) + "/" + string(ll_num)
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari autorizzati?"
		s_cs_xx.parametri.parametro_s_8 = ""
		s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
		s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer
		s_cs_xx.parametri.parametro_s_11 = ""
		s_cs_xx.parametri.parametro_s_12 = ""
		s_cs_xx.parametri.parametro_s_13 = string(ll_anno)
		s_cs_xx.parametri.parametro_s_14 = string(ll_num)
		s_cs_xx.parametri.parametro_s_15 = "N"
		s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
		s_cs_xx.parametri.parametro_b_1 = false
							
		openwithparm( w_invio_messaggi, 0)
		
		if not isnull(s_cs_xx.parametri.parametro_ul_1) then
			
			if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
			
			update non_conformita
			set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_non_conf = :ll_anno and
					 num_non_conf = :ll_num;
					 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
				rollback;
				return -1
			end if
			
			commit;
			
			parent.triggerevent("pc_retrieve")
			
			dw_non_conformita_lista.setrow( ll_riga)					 
			
		end if		
		
	elseif sqlca.sqlcode < 0 then				
		g_mb.messagebox( "OMNIA", "Errore durante la ricerca della lista di distribuzione: " + sqlca.sqlerrtext)						
		return -1		
	else		
		g_mb.messagebox( "OMNIA", "Attenzione: controllare l'associazione tipo causa / tipo lista!")
		return -1		
	end if					
end if				

end event

type cb_ricerca_1 from commandbutton within w_non_conformita_std
integer x = 2880
integer y = 40
integer width = 361
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;dw_nc_ricerca.fu_BuildSearch(TRUE)
sle_filtro_data_registrazione.fu_BuildSearch(false)
sle_filtro_data_scadenza.fu_BuildSearch(false)
dw_folder_search.fu_SelectTab(2)
dw_non_conformita_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type dw_non_conformita_lista from uo_cs_xx_dw within w_non_conformita_std
integer x = 183
integer y = 60
integer width = 2149
integer height = 680
integer taborder = 100
string dataobject = "d_non_conformita_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_new;call super::pcd_new;string ls_cod_operaio

cb_costi.enabled = false

dw_non_conformita_std_1.object.b_ricerca_fornitore.enabled=true
dw_non_conformita_std_1.object.b_ricerca_cliente.enabled=true
dw_non_conformita_std_1.object.b_ricerca_operaio.enabled = true
dw_non_conformita_std_1.object.b_ricerca_reparto.enabled = true
cb_dettagli.enabled=false
ib_in_new = true
cb_doc_compilato.enabled = true

//Donato 10/06/2009 aggiunto dettaglio stock
cb_lotti.enabled = false

setitem(getrow(),"flag_stato_nc","O")

//Donato 26-01-2009 verifica se è abilitata la gestione blocco campi
if is_flag_BNC = "S" then
	//Donato 26-11-2008 recupero responsabile a partire dall'utente collegato
	select cod_operaio
	into :ls_cod_operaio
	from anag_operai
	where cod_azienda=:s_cs_xx.cod_azienda
		and cod_utente=:s_cs_xx.cod_utente;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati da tabella anag_operai. Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	if not isnull(ls_cod_operaio) and ls_cod_operaio <> "" then
		//set del campo responsabile iter NC
		setitem(getrow(),"cod_operaio",ls_cod_operaio)
		
		//QUESTA MODIFICA SE LA DEVONO SUDARE!!!!!!!
		//vedi anche evento funzione wf_proteggi_documento
		
		//il campo responsabile Iter NC deve essere protetto a prescindere
		dw_non_conformita_std_1.object.cod_operaio.protect = 1
		//----------
	end if
end if
end event

event pcd_view;call super::pcd_view;cb_costi.enabled = true

dw_non_conformita_std_1.object.b_ricerca_fornitore.enabled=false
dw_non_conformita_std_1.object.b_ricerca_cliente.enabled=false
dw_non_conformita_std_1.object.b_ricerca_operaio.enabled = false
dw_non_conformita_std_1.object.b_ricerca_reparto.enabled = false
cb_dettagli.enabled = true
cb_doc_compilato.enabled = false

//Donato 10/06/2009 aggiunto dettaglio stock
cb_lotti.enabled = true
end event

event updatestart;call super::updatestart;if ib_in_new then

   long ll_anno,ll_num_registrazione

   dw_non_conformita_lista.SetItem (dw_non_conformita_lista.GetRow ( ),&
                                 "anno_non_conf", f_anno_esercizio() )
   ll_anno = dw_non_conformita_lista.GetItemNumber(dw_non_conformita_lista.GetRow ( ),&
                                 "anno_non_conf" )
   select max(non_conformita.num_non_conf)
     into :ll_num_registrazione
     from non_conformita
     where (non_conformita.cod_azienda = :s_cs_xx.cod_azienda) and (:ll_anno = non_conformita.anno_non_conf);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   dw_non_conformita_lista.SetItem (dw_non_conformita_lista.GetRow ( ),&
                                    "num_non_conf", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      dw_non_conformita_lista.SetItem (dw_non_conformita_lista.GetRow ( ),&
                                   "num_non_conf", 1)
   end if
end if



end event

event pcd_validaterow;call super::pcd_validaterow;//long ll_oggi, ll_scad
datetime ldt_creaz, ldt_scad

if isnull(this.getitemstring(this.getrow(), "cod_operaio")) then
   g_mb.messagebox("Non Conformità","Codice responsabile non conformità obbligatorio",Exclamation!)
   pcca.error = c_fatal
   return
end if

//Donato 26-01-2009 verifica se è abilitata la gestione blocco campi
if is_flag_BNC <> "S" then
	//vecchia gestione: obbligatorietà del campo
	if isnull(this.getitemstring(this.getrow(), "cod_errore")) then
		g_mb.messagebox("Non Conformità","Codice difetto obbligatorio",Exclamation!)
		pcca.error = c_fatal
		return
	end if
	
	if isnull(this.getitemstring(this.getrow(), "cod_trattamento")) then
		g_mb.messagebox("Non Conformità","Codice trattamento obbligatorio",Exclamation!)
		pcca.error = c_fatal
		return
	end if
	
end if
//Donato 28-11-2008 non deve essere più obbligatorio
/*
if isnull(this.getitemstring(this.getrow(), "cod_errore")) then
   g_mb.messagebox("Non Conformità","Codice difetto obbligatorio",Exclamation!)
   pcca.error = c_fatal
   return
end if

if isnull(this.getitemstring(this.getrow(), "cod_trattamento")) then
   g_mb.messagebox("Non Conformità","Codice trattamento obbligatorio",Exclamation!)
   pcca.error = c_fatal
   return
end if
*/
//fine modifica ------------------------------------------------------------------

if isnull(this.getitemstring(this.getrow(), "cod_misura")) and (this.getitemnumber(this.getrow(), "quan_non_conformita") > 0) then
   g_mb.messagebox("Non Conformità","Codice misura obbligatorio in presenza di quantità",Exclamation!)
   pcca.error = c_fatal
   return
end if

//ll_oggi = long(string(date(this.getitemdatetime(this.getrow(),"data_creazione")), "YYYYMMDD"))
//ll_scad = long(string(date(this.getitemdatetime(this.getrow(),"data_scadenza")), "YYYYMMDD"))
ldt_creaz = getitemdatetime(this.getrow(),"data_creazione")
ldt_scad = getitemdatetime(this.getrow(),"data_scadenza")


//Donato 26-01-2009 verifica se è abilitata la gestione blocco campi
if is_flag_BNC <> "S" then
	//vecchia gestione: obbligatorietà del campo
	if isnull(ldt_scad) then
		g_mb.messagebox("Non Conformità","ATTENZIONE! la data scadenza risulta mancante",Exclamation!)
			pcca.error = c_fatal
		return
	end if
end if
//Donato 28-11-2008 non deve essere più obbligatorio
/*
if ll_scad = 0 then
   g_mb.messagebox("Non Conformità","ATTENZIONE! la data scadenza risulta mancante",Exclamation!)
   pcca.error = c_fatal
   return
end if
*/
//fine modifica ------------------------------------------------------------------

//if ll_scad < ll_oggi then
if ldt_scad < ldt_creaz and not isnull(ldt_scad) then
   g_mb.messagebox("Non Conformità","ATTENZIONE! data scadenza minore di data creazione",Exclamation!)
   pcca.error = c_fatal
   return
end if

//Donato 03-02-2009 gestione vetusta disabilitata
/*
if (isnull(this.getitemstring(this.getrow(), "nome_doc_aut_deroga"))) and (this.getitemnumber(this.getrow(), "quan_derogata") > 0) then
   g_mb.messagebox("Non Conformità","ATTENZIONE! non esiste un documento che autorizzi la deroga",Exclamation!)
   pcca.error = c_fatal
   return
end if
*/
end event

event pcd_modify;call super::pcd_modify;cb_costi.enabled = false

dw_non_conformita_std_1.object.b_ricerca_fornitore.enabled=true
dw_non_conformita_std_1.object.b_ricerca_cliente.enabled=true
dw_non_conformita_std_1.object.b_ricerca_operaio.enabled = true
dw_non_conformita_std_1.object.b_ricerca_reparto.enabled = true

cb_dettagli.enabled=false
ib_in_new = false
cb_doc_compilato.enabled = true

//Donato 10/06/2009 aggiunto dettaglio stock
cb_lotti.enabled = false
end event

on pcd_disable;call uo_cs_xx_dw::pcd_disable;ib_in_new = false
end on

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_tipo_causa, ls_firma_chiusura

long   ll_riga, ll_num_sequenza_corrente

ll_riga = this.getrow()

if ll_riga < 1 then return

if not isnull(dw_non_conformita_lista.getrow()) and dw_non_conformita_lista.getrow() > 0 then
	is_flag_conferma = dw_non_conformita_lista.getitemstring(dw_non_conformita_lista.getrow(), "flag_eseguito_ac")
end if	

ll_num_sequenza_corrente = this.getitemnumber( ll_riga, "num_sequenza_corrente")

ls_cod_tipo_causa = this.getitemstring( ll_riga, "cod_tipo_causa")

ls_firma_chiusura = getitemstring( ll_riga, "firma_chiusura")

if not isnull(ls_firma_chiusura) and ls_firma_chiusura <> "" then
	mle_stato.text = "Conclusa"
	//Donato 26-01-2009 questo lo fai solo se è abilitata la gestione del blocco campi
	if is_flag_BNC = "S" then
		wf_proteggi_documento(ls_cod_tipo_causa, ll_num_sequenza_corrente, true)
		dw_non_conformita_std_1.setrow(ll_riga)
		dw_non_conformita_std_2.setrow(ll_riga)
		dw_non_conformita_std_3.setrow(ll_riga)
		dw_non_conformita_std_4.setrow(ll_riga)
	end if	
else

	if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
		mle_stato.text = f_trova_stato_documento( ls_cod_tipo_causa, ll_num_sequenza_corrente)
		//Donato 26-01-2009 questo lo fai solo se è abilitata la gestione del blocco campi
		if is_flag_BNC = "S" then
			wf_proteggi_documento(ls_cod_tipo_causa, ll_num_sequenza_corrente, false)
			dw_non_conformita_std_1.setrow(ll_riga)
			dw_non_conformita_std_2.setrow(ll_riga)
			dw_non_conformita_std_3.setrow(ll_riga)
			dw_non_conformita_std_4.setrow(ll_riga)
		end if		
	else
		mle_stato.text = ""
		//Donato 26-01-2009 questo lo fai solo se è abilitata la gestione del blocco campi
		if is_flag_BNC = "S" then
			wf_proteggi_documento(ls_cod_tipo_causa, ll_num_sequenza_corrente, false)
			dw_non_conformita_std_1.setrow(ll_riga)
			dw_non_conformita_std_2.setrow(ll_riga)
			dw_non_conformita_std_3.setrow(ll_riga)
			dw_non_conformita_std_4.setrow(ll_riga)
		end if	
	end if
end if

string ls_responsabile_iter_nc, ls_cod_utente

//Donato 26-01-2009 questo lo fai solo se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	
	ls_responsabile_iter_nc = getitemstring( ll_riga, "cod_operaio")
	select cod_utente
	into :ls_cod_utente
	from anag_operai
	where cod_azienda = :s_cs_xx.cod_azienda
		and cod_operaio = :ls_responsabile_iter_nc;
		
	if not isnull(ls_cod_utente) and ls_cod_utente <> "" then
		wf_loaddddw_dw( dw_non_conformita_std_1, &
								"cod_tipo_causa", &
								sqlca, &
								"tab_tipi_cause", &
								"b.cod_tipo_causa", &
											"b.des_tipo_causa", &
								"as b "+&
									"join (select 	x.cod_azienda,"+&
												"x.cod_tipo_lista_dist,"+&
												"x.cod_lista_dist,"+&
												"x.cod_destinatario "+&
											"from det_liste_dist_destinatari as x "+&
											"join det_liste_dist as y on y.cod_azienda=x.cod_azienda "+&
												"and y.cod_tipo_lista_dist=x.cod_tipo_lista_dist "+&
												"and y.cod_lista_dist=x.cod_lista_dist "+&
												"and y.cod_destinatario=x.cod_destinatario "+&
												"and y.num_sequenza=x.num_sequenza "+&
											"join tab_ind_dest as z on z.cod_azienda=x.cod_azienda "+&
												"and z.cod_destinatario= x.cod_destinatario_mail " +&							
											"where z.cod_utente = '"+ls_cod_utente+"' "+&
												"and (y.num_sequenza_prec=0 or y.num_sequenza_prec is null) "+&
											"group by x.cod_azienda,x.cod_tipo_lista_dist,x.cod_lista_dist,x.cod_destinatario "+&
											  ") as a on a.cod_azienda=b.cod_azienda "+&
												"and a.cod_tipo_lista_dist=b.cod_tipo_lista_dist "+&
												"and b.cod_lista_dist=a.cod_lista_dist ",&
								"b.cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((b.flag_blocco <> 'S') or (b.flag_blocco = 'S' and b.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	end if
end if
end event

event updateend;call super::updateend;cb_costi.enabled = true

long 	 ll_i, ll_anno_nc, ll_num_nc, ll_num_sequenza_corrente, ll_d, ll_anno_d, ll_numero_d

string ls_difetto, ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_Flag_gestione_fasi, &
       ls_cod_area_aziendale, ls_cod_prodotto, ls_cod_cat_mer

if s_cs_xx.num_livello_mail < 1 or isnull(s_cs_xx.num_livello_mail) then return

for ll_i = 1 to rowcount()
		
	ll_anno_nc = this.getitemnumber(ll_i,"anno_non_conf")
	ll_num_nc = this.getitemnumber(ll_i,"num_non_conf")
	ls_difetto = this.getitemstring(ll_i,"cod_errore")
	ls_cod_tipo_causa = this.getitemstring( ll_i, "cod_tipo_causa")
			
	if not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then       // *** controllo le fasi
				
		select cod_tipo_lista_dist,
		       cod_lista_dist
		into   :ls_cod_tipo_lista_dist,
		       :ls_cod_lista_dist
		from   tab_tipi_cause
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_causa = :ls_cod_tipo_causa;
					 
		if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
					
			select flag_gestione_fasi
			into   :ls_flag_gestione_fasi
			from   tab_tipi_liste_dist
			where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
					
			if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
					
			if ls_flag_gestione_fasi = "S" then
					
				ll_num_sequenza_corrente = this.getitemnumber( ll_i, "num_sequenza_corrente")
						
				select cod_area_aziendale
				into   :ls_cod_area_aziendale
				from   non_conformita 
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_non_conf = :ll_anno_nc and
						 num_non_conf = :ll_num_nc;
					
				ls_cod_prodotto = this.getitemstring( ll_i, "cod_prodotto")			
						
				select cod_cat_mer
				into   :ls_cod_cat_mer
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if isnull(ls_cod_cat_mer) then ls_cod_cat_mer = ""
					
				if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""							
					
				if getitemstatus(ll_i,0,primary!) = datamodified!	then
						
					s_cs_xx.parametri.parametro_s_1 = "L"	
					s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
					s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
					s_cs_xx.parametri.parametro_s_4 = ""											
					s_cs_xx.parametri.parametro_s_5 = "Modifica Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari autorizzati?"
					s_cs_xx.parametri.parametro_s_8 = ""
					s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
					s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer						
					s_cs_xx.parametri.parametro_s_11 = ""
					s_cs_xx.parametri.parametro_s_12 = ""
					s_cs_xx.parametri.parametro_s_13 = string(ll_anno_nc)
					s_cs_xx.parametri.parametro_s_14 = string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_15 = "N"
					s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
					s_cs_xx.parametri.parametro_b_1 = true
							
					openwithparm( w_invio_messaggi, 0)					
							
					if not isnull(s_cs_xx.parametri.parametro_ul_1) then
						
						if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
							
						update non_conformita
						set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_non_conf = :ll_anno_nc and
								 num_non_conf = :ll_num_nc;
								 
						if sqlca.sqlcode < 0 then
							g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
							rollback;
							return -1
						end if
						
						commit;							
						//parent.triggerevent("pc_retrieve")	
							
					end if
					//dw_non_conformita_lista.setrow( ll_i)
						
					continue												
						
				elseif getitemstatus(ll_i,0,primary!) = newmodified! then
						
					s_cs_xx.parametri.parametro_s_1 = "L"
					s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
					s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
					s_cs_xx.parametri.parametro_s_4 = ""																	
					s_cs_xx.parametri.parametro_s_5 = "Creazione Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_6 = "E' stata creata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari autorizzati?"
					s_cs_xx.parametri.parametro_s_8 = ""
					s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
					s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer						
					s_cs_xx.parametri.parametro_s_11 = ""
					s_cs_xx.parametri.parametro_s_12 = ""
					s_cs_xx.parametri.parametro_s_13 = string(ll_anno_nc)
					s_cs_xx.parametri.parametro_s_14 = string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_15 = "N"
					s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
					s_cs_xx.parametri.parametro_b_1 = true
							
					openwithparm( w_invio_messaggi, 0)					
							
					if not isnull(s_cs_xx.parametri.parametro_ul_1) then
							
						if s_cs_xx.parametri.parametro_ul_1 = -1 then setnull(s_cs_xx.parametri.parametro_ul_1)
							
						update non_conformita
						set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_non_conf = :ll_anno_nc and
								 num_non_conf = :ll_num_nc;
								 
						if sqlca.sqlcode < 0 then
							g_mb.messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
							rollback;
							return -1
						end if
							
						commit;							
							
						//parent.triggerevent("pc_retrieve")
	
					end if						
							
					//dw_non_conformita_lista.setrow( ll_i)
							
					continue
						
				else								
					continue						
				end if	
				
			else  //************** da mettere gestione normale
				
				select cod_tipo_lista_dist,
						 cod_lista_dist
				into   :ls_cod_tipo_lista_dist,
						 :ls_cod_lista_dist
				from   tab_difformita
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_errore = :ls_difetto;
								 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("OMNIA","Errore in lettura lista distribuzione da tab_difformita: " + sqlca.sqlerrtext)
					continue
				elseif sqlca.sqlcode = 100 then
					continue
				end if
						
				if getitemstatus(ll_i,0,primary!) = datamodified!	then
					s_cs_xx.parametri.parametro_s_1 = "L"
					s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
					s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
					s_cs_xx.parametri.parametro_s_4 = ""
					s_cs_xx.parametri.parametro_s_5 = "Modifica Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
					s_cs_xx.parametri.parametro_s_8 = ""
					openwithparm(w_invio_messaggi,0)
				elseif getitemstatus(ll_i,0,primary!) = newmodified! then
					s_cs_xx.parametri.parametro_s_1 = "L"
					s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
					s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
					s_cs_xx.parametri.parametro_s_4 = ""
					s_cs_xx.parametri.parametro_s_5 = "Creazione Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_6 = "E' stata creata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
					s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
					s_cs_xx.parametri.parametro_s_8 = ""
					openwithparm(w_invio_messaggi,0)
				else		
					continue		
				end if								
										
			end if
					
		elseif sqlca.sqlcode < 0 then
					
			g_mb.messagebox( "OMNIA", "Errore durante la ricerca della lista di distribuzione: " + sqlca.sqlerrtext)				
			return 1
			
		else

			select cod_tipo_lista_dist,
					 cod_lista_dist
			into   :ls_cod_tipo_lista_dist,
					 :ls_cod_lista_dist
			from   tab_difformita
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_errore = :ls_difetto;
							 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA","Errore in lettura lista distribuzione da tab_difformita: " + sqlca.sqlerrtext)
				continue
			elseif sqlca.sqlcode = 100 then
				continue
			end if
						
			if getitemstatus(ll_i,0,primary!) = datamodified!	then
				s_cs_xx.parametri.parametro_s_1 = "L"
				s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
				s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
				s_cs_xx.parametri.parametro_s_4 = ""
				s_cs_xx.parametri.parametro_s_5 = "Modifica Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
				s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
				s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
				s_cs_xx.parametri.parametro_s_8 = ""
				openwithparm(w_invio_messaggi,0)
			elseif getitemstatus(ll_i,0,primary!) = newmodified! then
				s_cs_xx.parametri.parametro_s_1 = "L"
				s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
				s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
				s_cs_xx.parametri.parametro_s_4 = ""
				s_cs_xx.parametri.parametro_s_5 = "Creazione Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
				s_cs_xx.parametri.parametro_s_6 = "E' stata creata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
				s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
				s_cs_xx.parametri.parametro_s_8 = ""
				openwithparm(w_invio_messaggi,0)
			else		
				continue		
			end if				
				
		end if
			
	else
			
		select cod_tipo_lista_dist,
				 cod_lista_dist
		into   :ls_cod_tipo_lista_dist,
				 :ls_cod_lista_dist
		from   tab_difformita
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_errore = :ls_difetto;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura lista distribuzione da tab_difformita: " + sqlca.sqlerrtext)
			continue
		elseif sqlca.sqlcode = 100 then
			continue
		end if
			
		if getitemstatus(ll_i,0,primary!) = datamodified!	then
			s_cs_xx.parametri.parametro_s_1 = "L"
			s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
			s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
			s_cs_xx.parametri.parametro_s_4 = ""
			s_cs_xx.parametri.parametro_s_5 = "Modifica Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
			s_cs_xx.parametri.parametro_s_6 = "E' stata modificata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
			s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
			s_cs_xx.parametri.parametro_s_8 = ""
			openwithparm(w_invio_messaggi,0)
		elseif getitemstatus(ll_i,0,primary!) = newmodified! then
			s_cs_xx.parametri.parametro_s_1 = "L"
			s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
			s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
			s_cs_xx.parametri.parametro_s_4 = ""
			s_cs_xx.parametri.parametro_s_5 = "Creazione Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
			s_cs_xx.parametri.parametro_s_6 = "E' stata creata la Non Conformità " + string(ll_anno_nc) + "/" + string(ll_num_nc)
			s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
			s_cs_xx.parametri.parametro_s_8 = ""
			openwithparm(w_invio_messaggi,0)
		else		
			continue		
		end if
										
	end if
		
next	

if rowsinserted > 0 or rowsupdated > 0 then
	// stefanop : apro finestra costi se necessario
	if ib_open_costi then
		cb_costi.triggerevent("clicked")
	end if
	// ---
end if

// ***  michela 16/05/2005: per ogni riga cancellata, se esiste l'accettazione materiali, la annullo (per sintexcal)

for ll_d = 1 to deletedcount()
	ll_anno_d = this.getitemnumber( ll_d, "anno_non_conf", delete!, true)
	ll_numero_d = this.getitemnumber( ll_d, "num_non_conf", delete!, true)
	
	update acc_materiali
	set    anno_non_conf = null,
	       num_non_conf = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_non_conf = :ll_anno_d and
			 num_non_conf = :ll_numero_d;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante l'annullamento NC in accettazione: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
next

commit;
end event

type dw_folder_search from u_folder within w_non_conformita_std
integer x = 23
integer y = 20
integer width = 3269
integer height = 740
integer taborder = 110
end type

type dw_non_conformita_std_1 from uo_cs_xx_dw within w_non_conformita_std
integer x = 69
integer y = 880
integer width = 2377
integer height = 1440
integer taborder = 60
string dataobject = "d_non_conformita_std_1"
boolean border = false
end type

event pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol

if i_extendmode then

	string ls_cod_trattamento
	
	choose case i_colname
	case "cod_errore"
		SELECT tab_difformita.cod_trattamento  
		INTO   :ls_cod_trattamento  
		FROM   tab_difformita  
		WHERE  ( tab_difformita.cod_azienda = :s_cs_xx.cod_azienda ) AND  
				 ( tab_difformita.cod_errore = :i_coltext )   ;
		if isnull(ls_cod_trattamento) or len(ls_cod_trattamento) < 1 then
			g_mb.messagebox("Non Conformità","Attenzione non è possibile determinare il trattamento proposto per questo errore: selezionarlo dall'apposita lista", Information!)
		else
			setitem(getrow(),"cod_trattamento",ls_cod_trattamento)
		end if
	end choose

end if ////  del controllo su i_extendmode

end event

event itemchanged;call super::itemchanged;string ls_nulla, ls_cod_tipo_causa, ls_cod_divisione, ls_flag_criticita, ls_flag_categoria_regis
long ll_num_sequenza_corrente, ll_riga
setnull(ls_nulla)

if i_extendmode then
	
   choose case i_colname
			
		case "cod_tipo_causa"
			
			setitem( i_rownbr, "cod_causa", ls_nulla)
			
			f_PO_LoadDDDW_DW(dw_non_conformita_std_1, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + &
								"' and ( cod_tipo_causa = '" + i_coltext + "' or cod_tipo_causa is null ) and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			
			//Donato 26-01-2009 controlla se è abilitata la obbligatorietà dei campi
			if is_flag_BNC = "S" then
				ll_riga = dw_non_conformita_lista.getrow()
				ll_num_sequenza_corrente = dw_non_conformita_lista.getitemnumber(ll_riga, "num_sequenza_corrente")			
				wf_proteggi_documento(data, ll_num_sequenza_corrente, false)
				
				setcolumn("cod_causa")
			end if
			//vecchio codice
			/*
			//Donato 26-11-2008						
			ll_riga = dw_non_conformita_lista.getrow()
			ll_num_sequenza_corrente = dw_non_conformita_lista.getitemnumber(ll_riga, "num_sequenza_corrente")			
			wf_proteggi_documento(data, ll_num_sequenza_corrente, false)
			
			setcolumn("cod_causa")
			//fine modifica
			*/
			//fine modifica ---------------------------------------------------
						
		case "cod_causa"
			
			select cod_tipo_causa
			into   :ls_cod_tipo_causa
			from   cause
			where  cod_azienda = :s_cs_xx.cod_azienda and 
			       cod_causa = :i_coltext;
					 
//			if sqlca.sqlcode = 0 then
//				setitem(i_rownbr, "cod_tipo_causa", ls_cod_tipo_causa)
//			end if
			
			if ib_visualizza_campi then
			
				select flag_criticita,
						 flag_categoria_regis
				into   :ls_flag_criticita,
						 :ls_flag_categoria_regis
				from   cause
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_causa = :i_coltext;
					 
				if sqlca.sqlcode < 0 then				
					g_mb.messagebox( "OMNIA", "Errore durante la ricerca criticita e categoria registrazione: " + sqlca.sqlerrtext)				
					return -1				
				end if
			
				if ls_flag_criticita = "" then setnull(ls_flag_criticita)			
				if ls_flag_categoria_regis = "" then setnull(ls_flag_categoria_regis)	
			
				choose case ls_flag_criticita
					case 'G'
						ls_flag_criticita = "A"
					case 'M'
						ls_flag_criticita = "B"
					case else
						ls_flag_criticita = "B"
				end choose
				
				this.setitem( row, "livello_impatto", ls_flag_criticita)				
				this.setitem( row, "flag_categoria_regis", ls_flag_categoria_regis)			
				
			end if
			
		case "cod_divisione"
			setitem( i_rownbr, "cod_area_aziendale", ls_nulla)
			f_PO_LoadDDDW_DW( dw_non_conformita_std_1, &
							"cod_area_aziendale", &
							sqlca, &
							"tab_aree_aziendali", &
							"cod_area_aziendale", &
                     "des_area", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + &
								"' and cod_divisione = '" + i_coltext + "' ")
		case "cod_area_aziendale"
			select cod_divisione
			into :ls_cod_divisione
			from tab_aree_aziendali
			where cod_azienda = :s_cs_xx.cod_azienda 
			  and cod_area_aziendale =:i_coltext;
			if sqlca.sqlcode = 0 then
				setitem(i_rownbr, "cod_divisione", ls_cod_divisione)
			end if
			 
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_non_conformita_std_1, "cod_cliente")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_non_conformita_std_1, "cod_fornitore")
		
	case "b_ricerca_operaio"
		guo_ricerca.uof_ricerca_operaio(dw_non_conformita_std_1, "cod_operaio")
		
	case "b_ricerca_reparto"
		guo_ricerca.uof_ricerca_reparto(dw_non_conformita_std_1, "cod_reparto")
		
end choose
end event

type dw_non_conformita_std_3 from uo_cs_xx_dw within w_non_conformita_std
integer x = 69
integer y = 880
integer width = 2377
integer height = 1020
integer taborder = 80
string dataobject = "d_non_conformita_std_3"
boolean border = false
end type

type dw_non_conformita_std_4 from uo_cs_xx_dw within w_non_conformita_std
event ue_imposta_flag_ac ( )
integer x = 69
integer y = 880
integer width = 2331
integer height = 1200
integer taborder = 70
string dataobject = "d_non_conformita_std_4"
boolean border = false
end type

event ue_imposta_flag_ac;call super::ue_imposta_flag_ac;//messagebox("Omnia", is_flag_conferma)			
dw_non_conformita_lista.setitem(dw_non_conformita_lista.getrow(), "flag_eseguito_ac", is_flag_conferma)
end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "flag_eseguito_ac"
		if is_flag_chiusura_nc = "N" then
			g_mb.messagebox("Omnia", "Utente non abilitato alla modifica di questo Flag.")
			dw_non_conformita_std_4.postevent("ue_imposta_flag_ac")
		end if			
			
end choose
	
end event

type dw_non_conformita_std_2 from uo_cs_xx_dw within w_non_conformita_std
integer x = 69
integer y = 880
integer width = 2377
integer height = 1160
integer taborder = 90
string dataobject = "d_non_conformita_std_2"
boolean border = false
end type

on pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol;double ld_quantita

ld_quantita = this.getitemnumber(this.getrow(), "quan_non_conformita")

choose case i_colname
case "quan_derogata"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_derogata") + long(i_coltext)
case "quan_recuperata"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_recuperata") + long(i_coltext)
case "quan_accettabile"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_accettabile") + long(i_coltext)
case "quan_resa"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_resa") + long(i_coltext)
case "quan_rottamata"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_rottamata") + long(i_coltext)
case "quan_carico"
   ld_quantita = ld_quantita - this.getitemnumber(this.getrow(), "quan_carico") + long(i_coltext)
end choose

this.setitem(this.getrow(),"quan_non_conformita", ld_quantita)


end on

type mle_stato from multilineedit within w_non_conformita_std
integer x = 297
integer y = 1940
integer width = 2126
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 134217732
boolean enabled = false
borderstyle borderstyle = stylelowered!
end type

type dw_folder from u_folder within w_non_conformita_std
integer x = 23
integer y = 780
integer width = 2469
integer height = 1580
integer taborder = 110
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Generici"
      SetFocus(dw_non_conformita_std_1)
   CASE "Qta/Decisioni"
      SetFocus(dw_non_conformita_std_2)
   CASE "Correz./Cause"
      SetFocus(dw_non_conformita_std_3)
   CASE "Prevenz./Note"
      SetFocus(dw_non_conformita_std_4)
END CHOOSE



end on

type dw_nc_ricerca from u_dw_search within w_non_conformita_std
integer x = 160
integer y = 40
integer width = 3109
integer height = 600
integer taborder = 10
string dataobject = "d_non_conformita_ricerca"
boolean border = false
end type

event itemchanged;call super::itemchanged;long ll_nulla, l_Idx
string ls_cod_prodotto, ls_cod_cliente, ls_tipo_causa, ls_causa, ls_difetto, ls_trattamento
string ls_flag_tipo_nc, ls_flag_stato_nc, ls_cod_area_aziendale, ls_flag_nc, ls_flag_provenienza_nc
datetime ldt_da_data_registrazione, ldt_a_data_registrazione, ldt_da_data_scadenza, ldt_a_data_scadenza
 
 setnull(ll_nulla)
 
 if not isnull(dwo) then
	choose case dwo.name
		case "num_registrazione"
			l_Idx = 1
			
			if data = "0" then 
				ls_cod_prodotto = getitemstring(1, "cod_prodotto")
				ls_cod_cliente = getitemstring(1, "cod_cliente")
				ls_tipo_causa = getitemstring(1, "tipo_causa")
				ls_causa = getitemstring(1, "causa")
				ls_difetto = getitemstring(1, "difetto")
				ls_trattamento = getitemstring(1, "trattamento")
				ls_flag_tipo_nc = getitemstring(1, "flag_tipo_nc")
				ls_flag_stato_nc = getitemstring(1, "flag_stato_nc")
				ls_cod_area_aziendale = getitemstring(1, "cod_area_aziendale")
				ls_flag_nc = getitemstring(1, "flag_nc")
				ls_flag_provenienza_nc = getitemstring(1, "flag_provenienza_nc")
				
				ldt_da_data_registrazione = getitemdatetime(1, "da_data_registrazione")
				ldt_a_data_registrazione = getitemdatetime(1, "a_data_registrazione")
				ldt_da_data_scadenza = getitemdatetime(1, "da_data_scadenza")
				ldt_a_data_scadenza = getitemdatetime(1, "a_data_scadenza")
				
				dw_nc_ricerca.fu_Reset()
				dw_nc_ricerca.setitem(1, "anno_registrazione", f_anno_esercizio())
				
				//ripristino altri valori
				if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then setitem(1,"cod_prodotto",ls_cod_prodotto)
				if not isnull(ls_cod_cliente) and ls_cod_cliente<>"" then setitem(1,"cod_cliente",ls_cod_cliente)
				if not isnull(ls_tipo_causa) and ls_tipo_causa<>"" then setitem(1,"tipo_causa",ls_tipo_causa)
				if not isnull(ls_causa) and ls_causa<>"" then setitem(1,"causa",ls_causa)
				if not isnull(ls_difetto) and ls_difetto<>"" then setitem(1,"difetto",ls_difetto)
				if not isnull(ls_trattamento) and ls_trattamento<>"" then setitem(1,"trattamento",ls_trattamento)
				if not isnull(ls_flag_tipo_nc) and ls_flag_tipo_nc<>"" then setitem(1,"flag_tipo_nc",ls_flag_tipo_nc)
				if not isnull(ls_flag_stato_nc) and ls_flag_stato_nc<>"" then setitem(1,"flag_stato_nc",ls_flag_stato_nc)
				if not isnull(ls_cod_area_aziendale) and ls_cod_area_aziendale<>"" then setitem(1,"cod_area_aziendale",ls_cod_area_aziendale)
				if not isnull(ls_flag_nc) and ls_flag_nc<>"" then setitem(1,"cod_prodotto",ls_flag_nc)
				if not isnull(ls_flag_provenienza_nc) and ls_flag_provenienza_nc<>"" then setitem(1,"flag_provenienza_nc",ls_flag_provenienza_nc)
				
				if not isnull(ldt_da_data_registrazione) then setitem(1,"da_data_registrazione",ldt_da_data_registrazione)
				if not isnull(ldt_a_data_registrazione) then setitem(1,"a_data_registrazione",ldt_a_data_registrazione)
				if not isnull(ldt_da_data_scadenza) then setitem(1,"da_data_scadenza",ldt_da_data_scadenza)
				if not isnull(ldt_a_data_scadenza) then setitem(1,"a_data_scadenza",ldt_a_data_scadenza)
				
			end if
			//setitem(1,"num_registrazione", "%0")//il_orig_val)
			//fu_SetCode(i_ColumnName[l_Idx], i_ColumnDefault[l_Idx])
		
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_nc_ricerca,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_nc_ricerca,"cod_prodotto")
end choose
end event

type dw_non_conformita_std_5 from uo_cs_xx_dw within w_non_conformita_std
integer x = 69
integer y = 880
integer width = 2377
integer height = 1440
integer taborder = 70
string dataobject = "d_non_conformita_std_5"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_non_conformita_std_1, "cod_cliente")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_non_conformita_std_1, "cod_fornitore")
		
	case "b_ricerca_operaio"
		guo_ricerca.uof_ricerca_operaio(dw_non_conformita_std_1, "cod_operaio")
		
	case "b_ricerca_reparto"
		guo_ricerca.uof_ricerca_reparto(dw_non_conformita_std_1, "cod_reparto")
		
end choose
end event

event itemchanged;call super::itemchanged;string ls_nulla, ls_cod_tipo_causa, ls_cod_divisione, ls_flag_criticita, ls_flag_categoria_regis
long ll_num_sequenza_corrente, ll_riga
setnull(ls_nulla)

if i_extendmode then
	
   choose case i_colname
			
		case "cod_tipo_causa"
			
			setitem( i_rownbr, "cod_causa", ls_nulla)
			
			f_PO_LoadDDDW_DW(dw_non_conformita_std_1, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + &
								"' and ( cod_tipo_causa = '" + i_coltext + "' or cod_tipo_causa is null ) and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			
			//Donato 26-01-2009 controlla se è abilitata la obbligatorietà dei campi
			if is_flag_BNC = "S" then
				ll_riga = dw_non_conformita_lista.getrow()
				ll_num_sequenza_corrente = dw_non_conformita_lista.getitemnumber(ll_riga, "num_sequenza_corrente")			
				wf_proteggi_documento(data, ll_num_sequenza_corrente, false)
				
				setcolumn("cod_causa")
			end if
			//vecchio codice
			/*
			//Donato 26-11-2008						
			ll_riga = dw_non_conformita_lista.getrow()
			ll_num_sequenza_corrente = dw_non_conformita_lista.getitemnumber(ll_riga, "num_sequenza_corrente")			
			wf_proteggi_documento(data, ll_num_sequenza_corrente, false)
			
			setcolumn("cod_causa")
			//fine modifica
			*/
			//fine modifica ---------------------------------------------------
						
		case "cod_causa"
			
			select cod_tipo_causa
			into   :ls_cod_tipo_causa
			from   cause
			where  cod_azienda = :s_cs_xx.cod_azienda and 
			       cod_causa = :i_coltext;
					 
//			if sqlca.sqlcode = 0 then
//				setitem(i_rownbr, "cod_tipo_causa", ls_cod_tipo_causa)
//			end if
			
			if ib_visualizza_campi then
			
				select flag_criticita,
						 flag_categoria_regis
				into   :ls_flag_criticita,
						 :ls_flag_categoria_regis
				from   cause
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_causa = :i_coltext;
					 
				if sqlca.sqlcode < 0 then				
					g_mb.messagebox( "OMNIA", "Errore durante la ricerca criticita e categoria registrazione: " + sqlca.sqlerrtext)				
					return -1				
				end if
			
				if ls_flag_criticita = "" then setnull(ls_flag_criticita)			
				if ls_flag_categoria_regis = "" then setnull(ls_flag_categoria_regis)	
			
				choose case ls_flag_criticita
					case 'G'
						ls_flag_criticita = "A"
					case 'M'
						ls_flag_criticita = "B"
					case else
						ls_flag_criticita = "B"
				end choose
				
				this.setitem( row, "livello_impatto", ls_flag_criticita)				
				this.setitem( row, "flag_categoria_regis", ls_flag_categoria_regis)			
				
			end if
			
		case "cod_divisione"
			setitem( i_rownbr, "cod_area_aziendale", ls_nulla)
			f_PO_LoadDDDW_DW( dw_non_conformita_std_1, &
							"cod_area_aziendale", &
							sqlca, &
							"tab_aree_aziendali", &
							"cod_area_aziendale", &
                     "des_area", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + &
								"' and cod_divisione = '" + i_coltext + "' ")
		case "cod_area_aziendale"
			select cod_divisione
			into :ls_cod_divisione
			from tab_aree_aziendali
			where cod_azienda = :s_cs_xx.cod_azienda 
			  and cod_area_aziendale =:i_coltext;
			if sqlca.sqlcode = 0 then
				setitem(i_rownbr, "cod_divisione", ls_cod_divisione)
			end if
			 
	end choose
end if
end event

event pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol

if i_extendmode then

	string ls_cod_trattamento
	
	choose case i_colname
	case "cod_errore"
		SELECT tab_difformita.cod_trattamento  
		INTO   :ls_cod_trattamento  
		FROM   tab_difformita  
		WHERE  ( tab_difformita.cod_azienda = :s_cs_xx.cod_azienda ) AND  
				 ( tab_difformita.cod_errore = :i_coltext )   ;
		if isnull(ls_cod_trattamento) or len(ls_cod_trattamento) < 1 then
			g_mb.messagebox("Non Conformità","Attenzione non è possibile determinare il trattamento proposto per questo errore: selezionarlo dall'apposita lista", Information!)
		else
			setitem(getrow(),"cod_trattamento",ls_cod_trattamento)
		end if
	end choose

end if ////  del controllo su i_extendmode

end event

