﻿$PBExportHeader$w_non_conformita_std_app.srw
$PBExportComments$Finestra Dati Standard Non Conformita - NC Approvvigionamenti
forward
global type w_non_conformita_std_app from w_non_conformita_std
end type
type cb_pareto from commandbutton within w_non_conformita_std_app
end type
type cb_eti_acc from commandbutton within w_non_conformita_std_app
end type
type cb_documento from commandbutton within w_non_conformita_std_app
end type
end forward

global type w_non_conformita_std_app from w_non_conformita_std
string title = "Dati Generici Non Conformità "
event ue_cerca_riga ( )
cb_pareto cb_pareto
cb_eti_acc cb_eti_acc
cb_documento cb_documento
end type
global w_non_conformita_std_app w_non_conformita_std_app

type variables
long il_anno, il_num

boolean ib_modifica = false
end variables

forward prototypes
public subroutine wf_carica_da_report ()
end prototypes

event ue_cerca_riga();long    ll_row

boolean lb_trovato


if isnull(il_anno) or il_anno = 0 or isnull(il_num) or il_num = 0 then
	return
end if

dw_nc_ricerca.setitem( 1, "anno_registrazione", il_anno)
dw_nc_ricerca.setitem( 1, "num_registrazione", il_num)

cb_ricerca_1.triggerevent("clicked")

for ll_row = 1 to dw_non_conformita_lista.rowcount()
	if dw_non_conformita_lista.getitemnumber(ll_row,"anno_non_conf") = il_anno and &
		dw_non_conformita_lista.getitemnumber(ll_row,"num_non_conf") = il_num then
		lb_trovato = true
		exit
	end if
next

if lb_trovato then
	dw_non_conformita_lista.setrow(ll_row)
	dw_non_conformita_lista.scrolltorow(ll_row)
	if ib_modifica then
		triggerevent("pc_modify")
	end if
else	
	if dw_non_conformita_lista.rowcount() > 0 then
		dw_non_conformita_lista.setrow(1)
		dw_non_conformita_lista.scrolltorow(1)
	end if
end if
end event

public subroutine wf_carica_da_report ();dw_non_conformita_lista.postevent("ue_ultimo")
end subroutine

on w_non_conformita_std_app.create
int iCurrent
call super::create
this.cb_pareto=create cb_pareto
this.cb_eti_acc=create cb_eti_acc
this.cb_documento=create cb_documento
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_pareto
this.Control[iCurrent+2]=this.cb_eti_acc
this.Control[iCurrent+3]=this.cb_documento
end on

on w_non_conformita_std_app.destroy
call super::destroy
destroy(this.cb_pareto)
destroy(this.cb_eti_acc)
destroy(this.cb_documento)
end on

event pc_delete;long ll_anno_registrazione, ll_num_registrazione, ll_count
double ld_num_protocollo
integer ll_risposta


if is_flag_apertura_nc = 'N' then
	g_mb.messagebox("Omnia", "Utente non abilitato alla cancellazione di una Non Conformità")
	return
end if	

ll_anno_registrazione = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "anno_non_conf")
ll_num_registrazione = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "num_non_conf")

select count(*)
  into :ll_count
  from det_non_conformita
 where cod_azienda = :s_cs_xx.cod_azienda
   and anno_non_conf = :ll_anno_registrazione
	and num_non_conf = :ll_num_registrazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Dettaglio Non Conformita")
	return
end if	

if ll_count > 0 then

	ll_risposta = g_mb.messagebox("Omnia", "Attenzione ci sono dei documenti collegati alla Non Conformità che si sta tentando di cancellare. ~n~rProseguendo con l'operazione verranno cancellati", Exclamation!, OKCancel!, 2)
	
	if ll_risposta = 1 then
		
		declare cu_det_non_conformita cursor for
			select num_protocollo
			  from det_non_conformita
			 where cod_azienda = :s_cs_xx.cod_azienda
				and anno_non_conf = :ll_anno_registrazione
				and num_non_conf = :ll_num_registrazione;
		
		open cu_det_non_conformita;
		
		do while 1 = 1
			fetch cu_det_non_conformita into :ld_num_protocollo;
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Dettaglio Non Conformita")
				return
			end if	
			
			if sqlca.sqlcode = 0 then
				delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
				where cod_azienda = :s_cs_xx.cod_azienda
				  and num_protocollo = :ld_num_protocollo;
				  
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
					return
				end if		  
		
				delete from det_non_conformita  //cancellazione dettaglio manutenzioni
					where cod_azienda = :s_cs_xx.cod_azienda
					  and anno_non_conf = :ll_anno_registrazione
					  and num_non_conf = :ll_num_registrazione;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Errore cancellazione dalla tabella DET_NON_CONFORMITA:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
					return
				end if	  		  
				  
				delete from tab_protocolli  //cancellazione protocollo
				where cod_azienda = :s_cs_xx.cod_azienda
				  and num_protocollo = :ld_num_protocollo;
				  
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
					return
				end if
			end if
		loop	
		close cu_det_non_conformita;
		
		
		CALL Super::pc_delete
	end if	
else
	CALL Super::pc_delete
end if	


end event

event pc_setwindow;call super::pc_setwindow;il_anno = s_cs_xx.parametri.parametro_d_1

il_num = s_cs_xx.parametri.parametro_d_2

// *** se ho appena inserito nell'accettazione la nc allora la metto in modifica. michela 16/05/2005 per sintexcal
if s_cs_xx.parametri.parametro_s_1 = "S" and not isnull(s_cs_xx.parametri.parametro_s_1) then
	ib_modifica = true
else
	ib_modifica = false
end if

setnull(s_cs_xx.parametri.parametro_d_1)

setnull(s_cs_xx.parametri.parametro_d_2)

setnull(s_cs_xx.parametri.parametro_s_1)
end event

event open;call super::open;// modifiche Michela

postevent("ue_cerca_riga")
dw_nc_ricerca.setitem(0,"flag_tipo_nc","A")
end event

type cb_lotti from w_non_conformita_std`cb_lotti within w_non_conformita_std_app
integer x = 2926
integer y = 2100
end type

type sle_filtro_data_scadenza from w_non_conformita_std`sle_filtro_data_scadenza within w_non_conformita_std_app
integer taborder = 40
end type

type st_nota_4 from w_non_conformita_std`st_nota_4 within w_non_conformita_std_app
end type

type st_nota_3 from w_non_conformita_std`st_nota_3 within w_non_conformita_std_app
end type

type st_nota_1 from w_non_conformita_std`st_nota_1 within w_non_conformita_std_app
end type

type st_filtro_data from w_non_conformita_std`st_filtro_data within w_non_conformita_std_app
end type

type sle_filtro_data_registrazione from w_non_conformita_std`sle_filtro_data_registrazione within w_non_conformita_std_app
integer taborder = 20
end type

type st_filtro_data_scad from w_non_conformita_std`st_filtro_data_scad within w_non_conformita_std_app
end type

type cb_annulla_1 from w_non_conformita_std`cb_annulla_1 within w_non_conformita_std_app
integer taborder = 80
end type

type cb_chiusura from w_non_conformita_std`cb_chiusura within w_non_conformita_std_app
integer y = 1812
integer taborder = 130
end type

type cb_comunicazioni from w_non_conformita_std`cb_comunicazioni within w_non_conformita_std_app
end type

type cb_costi from w_non_conformita_std`cb_costi within w_non_conformita_std_app
integer x = 2926
integer y = 2004
end type

type cb_dettagli from w_non_conformita_std`cb_dettagli within w_non_conformita_std_app
integer y = 1908
integer taborder = 180
string text = "&Dettaglio"
end type

event cb_dettagli::clicked;call super::clicked;//Donato 25-11-2008 se l'utente non è un resp. di fase esci
long ll_num_sequenza_corrente, ll_riga
integer li_ret
string ls_cod_tipo_causa, ls_firma_chiusura

ll_riga = dw_non_conformita_lista.getrow()
if ll_riga <=0 then return

ll_num_sequenza_corrente = dw_non_conformita_lista.getitemnumber(ll_riga, "num_sequenza_corrente")
ls_cod_tipo_causa = dw_non_conformita_lista.getitemstring( ll_riga, "cod_tipo_causa")
ls_firma_chiusura = dw_non_conformita_lista.getitemstring( ll_riga, "firma_chiusura")

//Donato 29/01/2009 verifica se è abilitata la gestione del blocco campi
if is_flag_BNC = "S" then
	if not isnull(ls_firma_chiusura) and ls_firma_chiusura <> "" then
		//Conclusa
		if s_cs_xx.cod_utente = "CS_SYSTEM" then
			li_ret = 1
		else
			return
		end if
	else
		if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 &
							and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)		
		else
			//inizio
			li_ret = wf_responsabile(ls_cod_tipo_causa,ll_num_sequenza_corrente,false)
		end if
	end if
	
	if li_ret = 1 then
		//responsabile OK			
	elseif li_ret = 0 then
		//non responsabile
		g_mb.messagebox( "OMNIA", "Non Autorizzato!",Exclamation!)	
		return
	else
		//qualcosa non quadra e c'è stato un errore (msg già dato)
		return
	end if
	//fine modifica -------------------------------------------------------------	
end if

if not isvalid(w_non_conformita_interna) then
   s_cs_xx.parametri.parametro_d_1 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "anno_non_conf")
   s_cs_xx.parametri.parametro_d_2 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "num_non_conf")
   s_cs_xx.parametri.parametro_s_1 = dw_non_conformita_lista.getitemstring(dw_non_conformita_lista.getrow(), "cod_fornitore")
   if s_cs_xx.parametri.parametro_d_1 > 0 and s_cs_xx.parametri.parametro_d_2 > 0 then
      window_open(w_non_conformita_approvv, -1)
   end if
end if


end event

type cb_doc_aut_deroga from w_non_conformita_std`cb_doc_aut_deroga within w_non_conformita_std_app
integer taborder = 30
end type

type cb_doc_compilato from w_non_conformita_std`cb_doc_compilato within w_non_conformita_std_app
integer taborder = 50
end type

type cb_invia from w_non_conformita_std`cb_invia within w_non_conformita_std_app
end type

type cb_report from w_non_conformita_std`cb_report within w_non_conformita_std_app
integer y = 1708
integer taborder = 70
end type

type cb_respingi from w_non_conformita_std`cb_respingi within w_non_conformita_std_app
end type

type cb_ricerca_1 from w_non_conformita_std`cb_ricerca_1 within w_non_conformita_std_app
integer taborder = 60
end type

type dw_non_conformita_lista from w_non_conformita_std`dw_non_conformita_lista within w_non_conformita_std_app
event ue_ultimo ( )
integer taborder = 230
end type

event dw_non_conformita_lista::ue_ultimo;call super::ue_ultimo;//----------------------------------- Gestione dati derivanti da report protocolli --------------------------------
//w_non_conformita_std_sis.triggerevent("pc_retrieve")
dw_non_conformita_lista.SetRow ( s_cs_xx.parametri.parametro_d_11)

cb_documento.postevent("clicked")

end event

on dw_non_conformita_lista::pcd_view;call w_non_conformita_std`dw_non_conformita_lista::pcd_view;cb_pareto.enabled = true
end on

on dw_non_conformita_lista::pcd_setkey;call w_non_conformita_std`dw_non_conformita_lista::pcd_setkey;call uo_cs_xx_dw::pcd_setkey;
LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   IF IsNull(GetItemstring(l_Idx, "flag_tipo_nc")) THEN
      SetItem(l_Idx, "flag_tipo_nc", "A")
   END IF
NEXT

end on

event dw_non_conformita_lista::pcd_retrieve;call super::pcd_retrieve;
LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, "A")
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

//parent.postevent("ue_cerca_riga")
end event

event dw_non_conformita_lista::pcd_modify;call super::pcd_modify;cb_pareto.enabled = false
end event

on dw_non_conformita_lista::pcd_new;call w_non_conformita_std`dw_non_conformita_lista::pcd_new;cb_pareto.enabled = false
end on

event dw_non_conformita_lista::pcd_delete;call super::pcd_delete;//long ll_anno_registrazione, ll_num_registrazione, ll_count
//double ld_num_protocollo
//integer ll_risposta
//
//
//if is_flag_apertura_nc = 'N' then
//	messagebox("Omnia", "Utente non abilitato alla cancellazione di una Non Conformità")
//	return
//end if	
//
//ll_anno_registrazione = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "anno_non_conf")
//ll_num_registrazione = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "num_non_conf")
//
//select count(*)
//  into :ll_count
//  from det_non_conformita
// where cod_azienda = :s_cs_xx.cod_azienda
//   and anno_non_conf = :ll_anno_registrazione
//	and num_non_conf = :ll_num_registrazione;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Dettaglio Non Conformita")
//	return
//end if	
//
//if ll_count > 0 then
//
//	ll_risposta = messagebox("Omnia", "Attenzione ci sono dei documenti collegati alla Non Conformità che si sta tentando di cancellare. ~n~rProseguendo con l'operazione verranno cancellati", Exclamation!, OKCancel!, 2)
//	
//	if ll_risposta = 1 then
//		
//		declare cu_det_non_conformita cursor for
//			select num_protocollo
//			  from det_non_conformita
//			 where cod_azienda = :s_cs_xx.cod_azienda
//				and anno_non_conf = :ll_anno_registrazione
//				and num_non_conf = :ll_num_registrazione;
//		
//		open cu_det_non_conformita;
//		
//		do while 1 = 1
//			fetch cu_det_non_conformita into :ld_num_protocollo;
//			if sqlca.sqlcode = 100 then exit
//			
//			if sqlca.sqlcode <> 0 then
//				messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Dettaglio Non Conformita")
//				return
//			end if	
//			
//			if sqlca.sqlcode = 0 then
//				delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
//				where cod_azienda = :s_cs_xx.cod_azienda
//				  and num_protocollo = :ld_num_protocollo;
//				  
//				if sqlca.sqlcode < 0 then
//					messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
//					return
//				end if		  
//		
//				delete from det_non_conformita  //cancellazione dettaglio manutenzioni
//					where cod_azienda = :s_cs_xx.cod_azienda
//					  and anno_non_conf = :ll_anno_registrazione
//					  and num_non_conf = :ll_num_registrazione;
//				
//				if sqlca.sqlcode < 0 then
//					messagebox("Errore cancellazione dalla tabella DET_NON_CONFORMITA:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
//					return
//				end if	  		  
//				  
//				delete from tab_protocolli  //cancellazione protocollo
//				where cod_azienda = :s_cs_xx.cod_azienda
//				  and num_protocollo = :ld_num_protocollo;
//				  
//				if sqlca.sqlcode < 0 then
//					messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
//					return
//				end if
//			end if
//		loop	
//		close cu_det_non_conformita;
//		
//		
//		CALL Super::pcd_delete
//	end if	
//else
//	CALL Super::pcd_delete
//end if	
//
//
end event

type dw_folder_search from w_non_conformita_std`dw_folder_search within w_non_conformita_std_app
integer taborder = 220
end type

type dw_non_conformita_std_1 from w_non_conformita_std`dw_non_conformita_std_1 within w_non_conformita_std_app
integer taborder = 150
end type

type dw_non_conformita_std_3 from w_non_conformita_std`dw_non_conformita_std_3 within w_non_conformita_std_app
integer taborder = 200
end type

type dw_non_conformita_std_4 from w_non_conformita_std`dw_non_conformita_std_4 within w_non_conformita_std_app
integer taborder = 190
end type

type dw_non_conformita_std_2 from w_non_conformita_std`dw_non_conformita_std_2 within w_non_conformita_std_app
integer taborder = 210
end type

type mle_stato from w_non_conformita_std`mle_stato within w_non_conformita_std_app
end type

type dw_folder from w_non_conformita_std`dw_folder within w_non_conformita_std_app
integer taborder = 240
end type

type dw_nc_ricerca from w_non_conformita_std`dw_nc_ricerca within w_non_conformita_std_app
end type

event dw_nc_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_nc_ricerca,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_nc_ricerca,"cod_prodotto")
end choose
end event

type cb_pareto from commandbutton within w_non_conformita_std_app
integer x = 2926
integer y = 1608
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Grafico"
end type

on clicked;if not isvalid(w_graf_par_nc) then
   s_cs_xx.parametri.parametro_s_10 = "A"
   window_open(w_graf_par_nc, -1)
end if
end on

type cb_eti_acc from commandbutton within w_non_conformita_std_app
event clicked pbm_bnclicked
integer x = 2926
integer y = 1408
integer width = 366
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Etichette"
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "anno_non_conf") 
s_cs_xx.parametri.parametro_d_2 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "num_non_conf") 

if not(isnull(s_cs_xx.parametri.parametro_d_1)) and not(isnull(s_cs_xx.parametri.parametro_d_2)) then
   window_open(w_etichette_non_conformita, 0)
end if
end event

type cb_documento from commandbutton within w_non_conformita_std_app
integer x = 2926
integer y = 1508
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_non_conformita_lista.accepttext()

if (isnull(dw_non_conformita_lista.getrow()) or dw_non_conformita_lista.getrow() < 1) then
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata nessuna non conformità")
	return
end if	

if isnull(dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "anno_non_conf")) or &
	dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "anno_non_conf") < 2 then
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata nessuna non conformità")
	return
end if	

//window_open_parm(w_det_non_conformita, -1, dw_non_conformita_lista)

s_cs_xx.parametri.parametro_ul_1 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "anno_non_conf")
s_cs_xx.parametri.parametro_ul_2 = dw_non_conformita_lista.getitemnumber(dw_non_conformita_lista.getrow(), "num_non_conf")

open(w_non_conformita_ole)
end event

