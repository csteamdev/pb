﻿$PBExportHeader$w_non_conformita_std_wizard.srw
$PBExportComments$Finestra Dati Standard Non Conformita
forward
global type w_non_conformita_std_wizard from w_cs_xx_principale
end type
type cb_finish from commandbutton within w_non_conformita_std_wizard
end type
type cb_prev from commandbutton within w_non_conformita_std_wizard
end type
type cb_next from commandbutton within w_non_conformita_std_wizard
end type
type dw_wizard from datawindow within w_non_conformita_std_wizard
end type
type st_info from statictext within w_non_conformita_std_wizard
end type
end forward

global type w_non_conformita_std_wizard from w_cs_xx_principale
integer width = 2491
integer height = 1416
string title = "Non Conformita Wizard"
boolean center = true
event ue_close ( )
cb_finish cb_finish
cb_prev cb_prev
cb_next cb_next
dw_wizard dw_wizard
st_info st_info
end type
global w_non_conformita_std_wizard w_non_conformita_std_wizard

type variables
private:
	string is_w_title = "Non conformita wizard"
	string is_dw_step[]
	int ii_current_step = 0

	uo_cs_xx_dw iuo_parent_dw
	string is_cod_tipo_nc // V = vendita, 

	// indica se è Libero, Bolla, Fattura o Ordine
	string is_cod_tipo_documento, is_cod_cliente, is_cod_prodotto
	long il_anno_registrazione, il_num_registrazione, il_prog_riga
end variables

forward prototypes
public subroutine wf_init_wizard ()
public function boolean wf_next_step ()
public function boolean wf_set_dw_step ()
public function boolean wf_prev_step ()
public function boolean wf_end_step (string as_dataobject)
public function boolean wf_start_step (string as_dataobject)
public subroutine wf_cerca_ordine ()
public subroutine wf_cerca_fattura (boolean ab_fiscale)
public subroutine wf_cerca_bolla (boolean ab_fiscale)
public subroutine wf_cerca_bol_ven_fast ()
end prototypes

event ue_close();close(this)
end event

public subroutine wf_init_wizard ();/**
 * Preparo l'array delle datawindow da usare per gli steps
 **/
 
 
is_dw_step[1] = "d_non_conf_wizard_1"
is_dw_step[2] = "d_non_conf_wizard_2"

cb_prev.visible = false
cb_finish.visible = false
cb_next.visible = true
end subroutine

public function boolean wf_next_step ();/**
 * Procedo allo step successivo
 **/
 
if ii_current_step > 0 then
	dw_wizard.accepttext()
	
	// processo dati
	if wf_end_step(is_dw_step[ii_current_step]) = false then return false
	
	// siamo alla fine?
	if ii_current_step = upperbound(is_dw_step) then return true
end if


ii_current_step++
title = is_w_title + " - passo " + string(ii_current_step)

if ii_current_step = upperbound(is_dw_step) then
	cb_finish.visible = true
	cb_next.visible = false
end if

cb_prev.visible = true

wf_set_dw_step()
wf_start_step(is_dw_step[ii_current_step])

return true
end function

public function boolean wf_set_dw_step ();string ls_cod_tipo_documento

if ii_current_step = 2 then
	ls_cod_tipo_documento = dw_wizard.getitemstring(1, "cod_tipo_documento")
end if

dw_wizard.reset()
dw_wizard.dataobject = is_dw_step[ii_current_step]
dw_wizard.insertrow(0)

if ii_current_step = 2 then
	dw_wizard.setitem(1, "cod_tipo_documento", ls_cod_tipo_documento)
end if

return true
end function

public function boolean wf_prev_step ();/**
 * Procedo allo step successivo
 **/
 
if ii_current_step > 1 then
	ii_current_step --

	cb_finish.visible = false
	cb_next.visible = true
	cb_prev.visible = not (ii_current_step = 1)
	
	title = is_w_title + " - passo " + string(ii_current_step)
	
	wf_set_dw_step()
	wf_start_step(is_dw_step[ii_current_step])
end if

return true
end function

public function boolean wf_end_step (string as_dataobject);choose case ii_current_step
	case 1
		is_cod_tipo_documento = dw_wizard.getitemstring(1,1)
		
		if is_cod_tipo_documento = "L" then // Libera
			postevent("ue_close")
		end if
		
	case 2
		il_anno_registrazione  = dw_wizard.getitemnumber(1,"anno_registrazione")
		il_num_registrazione = dw_wizard.getitemnumber(1,"num_registrazione")
		il_prog_riga = dw_wizard.getitemnumber(1,"prog_riga")
		is_cod_cliente = dw_wizard.getitemstring(1, "cod_cliente")
		is_cod_prodotto = dw_wizard.getitemstring(1,"cod_prodotto")
		
		iuo_parent_dw.setitem(iuo_parent_dw.getrow(), "cod_prodotto", is_cod_prodotto)
		iuo_parent_dw.setitem(iuo_parent_dw.getrow(), "cod_cliente", is_cod_cliente)
		
		if is_cod_tipo_nc = "V" then // vendita
			if is_cod_tipo_documento = "B" then // bolla 
				iuo_parent_dw.setitem(iuo_parent_dw.getrow(), "anno_registrazione_bol_ven", il_anno_registrazione)
				iuo_parent_dw.setitem(iuo_parent_dw.getrow(), "num_registrazione_bol_ven", il_num_registrazione)
				iuo_parent_dw.setitem(iuo_parent_dw.getrow(), "prog_riga_bol_ven", il_prog_riga)
			elseif is_cod_tipo_documento = "F" then // fattura 
				iuo_parent_dw.setitem(iuo_parent_dw.getrow(), "anno_reg_fat_ven", il_anno_registrazione)
				iuo_parent_dw.setitem(iuo_parent_dw.getrow(), "num_reg_fat_ven", il_num_registrazione)
				iuo_parent_dw.setitem(iuo_parent_dw.getrow(), "prog_riga_fat_ven", il_prog_riga)
			elseif is_cod_tipo_documento = "O" then // ordine 
				iuo_parent_dw.setitem(iuo_parent_dw.getrow(), "anno_registrazione_ord_ven", il_anno_registrazione)
				iuo_parent_dw.setitem(iuo_parent_dw.getrow(), "num_registrazione_ord_ven", il_num_registrazione)
				iuo_parent_dw.setitem(iuo_parent_dw.getrow(), "prog_riga_ord_ven", il_prog_riga)
			end if
		end if
		
end choose

return true
end function

public function boolean wf_start_step (string as_dataobject);string ls_null

setnull(ls_null)

choose case ii_current_step
	case 1
		if not isnull(is_cod_tipo_documento) and is_cod_tipo_documento <> "" then
			dw_wizard.setitem(1, 1, is_cod_tipo_documento)
		end if
		
	case 2
		if not isnull(il_anno_registrazione) and il_anno_registrazione > 0 then dw_wizard.setitem(1, "anno_registrazione", il_anno_registrazione)
		if not isnull(il_num_registrazione) and il_num_registrazione > 0 then dw_wizard.setitem(1, "num_registrazione", il_num_registrazione)
		if not isnull(il_prog_riga) and il_prog_riga > 0 then dw_wizard.setitem(1, "prog_riga", il_prog_riga)
		if not isnull(is_cod_cliente) and is_cod_cliente <> "" then dw_wizard.setitem(1, "cod_cliente", is_cod_cliente)
		if not isnull(is_cod_prodotto) and is_cod_prodotto <> "" then dw_wizard.setitem(1, "cod_prodotto", is_cod_prodotto)
				
end choose

return true
end function

public subroutine wf_cerca_ordine ();/**
 * Cerco bolla
 **/
 
long ll_anno
str_ricerca_documenti lstr_ricerca_documenti

dw_wizard.accepttext()

ll_anno = dw_wizard.getitemnumber(1, "anno_registrazione")
if isnull(ll_anno) then
	g_mb.messagebox("Ricerca Ordine", "Selezionare l'anno su ciu effetuare la ricerca", StopSign!)
	dw_wizard.setColumn("anno_registrazione")
	return
end if

lstr_ricerca_documenti.cod_cliente = dw_wizard.getitemstring(1, "cod_cliente")
lstr_ricerca_documenti.cod_prodotto = dw_wizard.getitemstring(1, "cod_prodotto")
lstr_ricerca_documenti.anno_registrazione = dw_wizard.getitemnumber(1, "anno_registrazione")
lstr_ricerca_documenti.num_registrazione = dw_wizard.getitemnumber(1, "num_registrazione")
lstr_ricerca_documenti.cod_documento = dw_wizard.getitemstring(1, "cod_documento")
lstr_ricerca_documenti.anno_documento = dw_wizard.getitemnumber(1, "anno_documento")
lstr_ricerca_documenti.numeratore_documento = dw_wizard.getitemstring(1, "numeratore_documento")
lstr_ricerca_documenti.num_documento = dw_wizard.getitemnumber(1, "num_documento")
lstr_ricerca_documenti.tipo_documento = "OV"

window_open_parm(w_ricerca_documenti, 0, lstr_ricerca_documenti)

if isvalid(message.powerobjectparm) and not isnull(message.powerobjectparm) then
	lstr_ricerca_documenti = message.powerobjectparm
	
	dw_wizard.setitem(1, "cod_cliente", lstr_ricerca_documenti.cod_cliente)
	dw_wizard.setitem(1, "cod_prodotto", lstr_ricerca_documenti.cod_prodotto)
	dw_wizard.setitem(1, "anno_registrazione", lstr_ricerca_documenti.anno_registrazione)
	dw_wizard.setitem(1, "num_registrazione", lstr_ricerca_documenti.num_registrazione)
	dw_wizard.setitem(1, "prog_riga", lstr_ricerca_documenti.prog_riga)
	dw_wizard.setitem(1, "cod_documento", lstr_ricerca_documenti.cod_documento)
	dw_wizard.setitem(1, "anno_documento", lstr_ricerca_documenti.anno_documento)
	dw_wizard.setitem(1, "numeratore_documento", lstr_ricerca_documenti.numeratore_documento)
	dw_wizard.setitem(1, "num_documento", lstr_ricerca_documenti.num_documento)
end if
end subroutine

public subroutine wf_cerca_fattura (boolean ab_fiscale);/**
 * Cerco Fattura
 **/
 
long ll_anno
str_ricerca_documenti lstr_ricerca_documenti

dw_wizard.accepttext()

if ab_fiscale then
	ll_anno = dw_wizard.getitemnumber(1, "anno_documento")
	if isnull(ll_anno) then
		g_mb.messagebox("Ricerca Fattura", "Selezionare l'anno fiscale su ciu effetuare la ricerca", StopSign!)
		dw_wizard.setColumn("anno_documento")
		return
	end if
else
	ll_anno = dw_wizard.getitemnumber(1, "anno_registrazione")
	if isnull(ll_anno) then
		g_mb.messagebox("Ricerca Fattura", "Selezionare l'anno su ciu effetuare la ricerca", StopSign!)
		dw_wizard.setColumn("anno_registrazione")
		return
	end if
end if

lstr_ricerca_documenti.cod_cliente = dw_wizard.getitemstring(1, "cod_cliente")
lstr_ricerca_documenti.cod_prodotto = dw_wizard.getitemstring(1, "cod_prodotto")
lstr_ricerca_documenti.anno_registrazione = dw_wizard.getitemnumber(1, "anno_registrazione")
lstr_ricerca_documenti.num_registrazione = dw_wizard.getitemnumber(1, "num_registrazione")
lstr_ricerca_documenti.cod_documento = dw_wizard.getitemstring(1, "cod_documento")
lstr_ricerca_documenti.anno_documento = dw_wizard.getitemnumber(1, "anno_documento")
lstr_ricerca_documenti.numeratore_documento = dw_wizard.getitemstring(1, "numeratore_documento")
lstr_ricerca_documenti.num_documento = dw_wizard.getitemnumber(1, "num_documento")
lstr_ricerca_documenti.tipo_documento = "FV"

window_open_parm(w_ricerca_documenti, 0, lstr_ricerca_documenti)

if isvalid(message.powerobjectparm) and not isnull(message.powerobjectparm) then
	lstr_ricerca_documenti = message.powerobjectparm
	
	dw_wizard.setitem(1, "cod_cliente", lstr_ricerca_documenti.cod_cliente)
	dw_wizard.setitem(1, "cod_prodotto", lstr_ricerca_documenti.cod_prodotto)
	dw_wizard.setitem(1, "anno_registrazione", lstr_ricerca_documenti.anno_registrazione)
	dw_wizard.setitem(1, "num_registrazione", lstr_ricerca_documenti.num_registrazione)
	dw_wizard.setitem(1, "prog_riga", lstr_ricerca_documenti.prog_riga)
	dw_wizard.setitem(1, "cod_documento", lstr_ricerca_documenti.cod_documento)
	dw_wizard.setitem(1, "anno_documento", lstr_ricerca_documenti.anno_documento)
	dw_wizard.setitem(1, "numeratore_documento", lstr_ricerca_documenti.numeratore_documento)
	dw_wizard.setitem(1, "num_documento", lstr_ricerca_documenti.num_documento)
end if
end subroutine

public subroutine wf_cerca_bolla (boolean ab_fiscale);/**
 * Cerco bolla
 **/
 
long ll_anno
str_ricerca_documenti lstr_ricerca_documenti

dw_wizard.accepttext()

if ab_fiscale then
	ll_anno = dw_wizard.getitemnumber(1, "anno_documento")
	if isnull(ll_anno) then
		g_mb.messagebox("Ricerca Bolla", "Selezionare l'anno fiscale su ciu effetuare la ricerca", StopSign!)
		dw_wizard.setColumn("anno_documento")
		return
	end if
else
	ll_anno = dw_wizard.getitemnumber(1, "anno_registrazione")
	if isnull(ll_anno) then
		g_mb.messagebox("Ricerca Bolla", "Selezionare l'anno su ciu effetuare la ricerca", StopSign!)
		dw_wizard.setColumn("anno_registrazione")
		return
	end if
end if

lstr_ricerca_documenti.cod_cliente = dw_wizard.getitemstring(1, "cod_cliente")
lstr_ricerca_documenti.cod_prodotto = dw_wizard.getitemstring(1, "cod_prodotto")
lstr_ricerca_documenti.anno_registrazione = dw_wizard.getitemnumber(1, "anno_registrazione")
lstr_ricerca_documenti.num_registrazione = dw_wizard.getitemnumber(1, "num_registrazione")
lstr_ricerca_documenti.cod_documento = dw_wizard.getitemstring(1, "cod_documento")
lstr_ricerca_documenti.anno_documento = dw_wizard.getitemnumber(1, "anno_documento")
lstr_ricerca_documenti.numeratore_documento = dw_wizard.getitemstring(1, "numeratore_documento")
lstr_ricerca_documenti.num_documento = dw_wizard.getitemnumber(1, "num_documento")
lstr_ricerca_documenti.tipo_documento = "BV"

window_open_parm(w_ricerca_documenti, 0, lstr_ricerca_documenti)

if isvalid(message.powerobjectparm) and not isnull(message.powerobjectparm) then
	lstr_ricerca_documenti = message.powerobjectparm
	
	dw_wizard.setitem(1, "cod_cliente", lstr_ricerca_documenti.cod_cliente)
	dw_wizard.setitem(1, "cod_prodotto", lstr_ricerca_documenti.cod_prodotto)
	dw_wizard.setitem(1, "anno_registrazione", lstr_ricerca_documenti.anno_registrazione)
	dw_wizard.setitem(1, "num_registrazione", lstr_ricerca_documenti.num_registrazione)
	dw_wizard.setitem(1, "prog_riga", lstr_ricerca_documenti.prog_riga)
	dw_wizard.setitem(1, "cod_documento", lstr_ricerca_documenti.cod_documento)
	dw_wizard.setitem(1, "anno_documento", lstr_ricerca_documenti.anno_documento)
	dw_wizard.setitem(1, "numeratore_documento", lstr_ricerca_documenti.numeratore_documento)
	dw_wizard.setitem(1, "num_documento", lstr_ricerca_documenti.num_documento)
end if
end subroutine

public subroutine wf_cerca_bol_ven_fast ();/**
 * Funzione usata nell'itemchaged
 **/
 

end subroutine

on w_non_conformita_std_wizard.create
int iCurrent
call super::create
this.cb_finish=create cb_finish
this.cb_prev=create cb_prev
this.cb_next=create cb_next
this.dw_wizard=create dw_wizard
this.st_info=create st_info
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_finish
this.Control[iCurrent+2]=this.cb_prev
this.Control[iCurrent+3]=this.cb_next
this.Control[iCurrent+4]=this.dw_wizard
this.Control[iCurrent+5]=this.st_info
end on

on w_non_conformita_std_wizard.destroy
call super::destroy
destroy(this.cb_finish)
destroy(this.cb_prev)
destroy(this.cb_next)
destroy(this.dw_wizard)
destroy(this.st_info)
end on

event resize;st_info.move(20,20)
st_info.width = newwidth - 20

cb_next.move(newwidth - cb_next.width - 20, newheight - cb_next.height - 20)
cb_finish.move(cb_next.x, cb_next.y)
cb_prev.move(cb_next.x - cb_prev.width - 20, cb_next.y)

dw_wizard.move(20, st_info.y + st_info.height + 20)
dw_wizard.resize(cb_next.x + cb_next.width, cb_next.y - dw_wizard.y - 20)
end event

event pc_setwindow;call super::pc_setwindow;iuo_parent_dw = s_cs_xx.parametri.parametro_uo_dw_1
is_cod_tipo_nc = s_cs_xx.parametri.parametro_s_1

setnull(s_cs_xx.parametri.parametro_uo_dw_1)
setnull(s_cs_xx.parametri.parametro_s_1)

wf_init_wizard()
wf_next_step()
end event

type cb_finish from commandbutton within w_non_conformita_std_wizard
integer x = 457
integer y = 1180
integer width = 503
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Fine"
end type

event clicked;wf_next_step()

parent.postevent("ue_close")
end event

type cb_prev from commandbutton within w_non_conformita_std_wizard
boolean visible = false
integer x = 983
integer y = 1180
integer width = 503
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Indietro"
end type

event clicked;wf_prev_step()
end event

type cb_next from commandbutton within w_non_conformita_std_wizard
integer x = 1509
integer y = 1180
integer width = 503
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Avanti"
end type

event clicked;wf_next_step()
end event

type dw_wizard from datawindow within w_non_conformita_std_wizard
integer x = 23
integer y = 120
integer width = 2400
integer height = 1020
integer taborder = 10
string title = "none"
string dataobject = "d_non_conf_wizard_2"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event clicked;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
setnull(s_cs_xx.parametri.parametro_uo_dw_search)
		
choose case dwo.name
		
	case "b_cerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_wizard,"cod_cliente")
		
	case "b_cerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_wizard,"cod_prodotto")

	case "b_cerca_documento_fisc"
		choose case is_cod_tipo_documento
				
			case "B" // bolla
				wf_cerca_bolla(true)
				
			case "F" // fattura
				wf_cerca_fattura(true)
				
			case "O" // ordine
				wf_cerca_ordine()
				
		end choose		
		
	case "b_cerca_documento_int"
		choose case is_cod_tipo_documento
				
			case "B" // bolla
				wf_cerca_bolla(false)
				
			case "F" // fattura
				wf_cerca_fattura(false)
				
			case "O" // ordine
				wf_cerca_ordine()
				
				
		end choose		
		
end choose
end event

type st_info from statictext within w_non_conformita_std_wizard
integer x = 23
integer y = 40
integer width = 1527
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Selezionare il tipo di documento per creare la non conformita"
boolean focusrectangle = false
end type

