﻿$PBExportHeader$w_non_conformita_vendite.srw
$PBExportComments$Finestra Input Dati Specifici per Non Conformita Vendite / Post Vendita
forward
global type w_non_conformita_vendite from w_cs_xx_principale
end type
type cb_manutenzione_ric from commandbutton within w_non_conformita_vendite
end type
type cb_ricerca_stock from commandbutton within w_non_conformita_vendite
end type
type cb_manutenz from uo_richiesta_manutenz within w_non_conformita_vendite
end type
type cb_intervento from uo_richiesta_intervento within w_non_conformita_vendite
end type
type cb_visita from uo_richiesta_visita within w_non_conformita_vendite
end type
type dw_non_conformita_interna_lista from uo_cs_xx_dw within w_non_conformita_vendite
end type
type dw_folder from u_folder within w_non_conformita_vendite
end type
type dw_non_conformita_vendite_det_2 from uo_cs_xx_dw within w_non_conformita_vendite
end type
type dw_non_conformita_vendite_det_1 from uo_cs_xx_dw within w_non_conformita_vendite
end type
end forward

global type w_non_conformita_vendite from w_cs_xx_principale
integer width = 2551
integer height = 1300
string title = "Dati Specifici Non Conformita Vendite / Post Vendite"
boolean resizable = false
cb_manutenzione_ric cb_manutenzione_ric
cb_ricerca_stock cb_ricerca_stock
cb_manutenz cb_manutenz
cb_intervento cb_intervento
cb_visita cb_visita
dw_non_conformita_interna_lista dw_non_conformita_interna_lista
dw_folder dw_folder
dw_non_conformita_vendite_det_2 dw_non_conformita_vendite_det_2
dw_non_conformita_vendite_det_1 dw_non_conformita_vendite_det_1
end type
global w_non_conformita_vendite w_non_conformita_vendite

type variables
boolean ib_delete=true
string is_prodotto, is_tipo_requisito
end variables

forward prototypes
public subroutine wf_cerca_bolla_ven ()
public subroutine wf_cerca_ord_ven ()
public subroutine wf_cerca_fat_ven ()
end prototypes

public subroutine wf_cerca_bolla_ven ();long ll_anno
str_ricerca_documenti lstr_ricerca_documenti

dw_non_conformita_vendite_det_1.accepttext()

ll_anno = dw_non_conformita_vendite_det_1.getitemnumber(1, "anno_registrazione_bol_ven")
if isnull(ll_anno) then
	g_mb.messagebox("Ricerca Bolla", "Selezionare l'anno fiscale su ciu effetuare la ricerca", StopSign!)
	dw_non_conformita_vendite_det_1.setColumn("anno_registrazione_bol_ven")
	return
end if

lstr_ricerca_documenti.cod_cliente = dw_non_conformita_vendite_det_1.getitemstring(1, "cod_cliente")
lstr_ricerca_documenti.cod_prodotto = dw_non_conformita_vendite_det_1.getitemstring(1, "cod_prodotto")
lstr_ricerca_documenti.anno_registrazione = dw_non_conformita_vendite_det_1.getitemnumber(1, "anno_registrazione_bol_ven")
lstr_ricerca_documenti.num_registrazione = dw_non_conformita_vendite_det_1.getitemnumber(1, "num_registrazione_bol_ven")
setnull(lstr_ricerca_documenti.prog_riga)
setnull(lstr_ricerca_documenti.cod_documento)
setnull(lstr_ricerca_documenti.anno_documento)
setnull(lstr_ricerca_documenti.numeratore_documento)
setnull(lstr_ricerca_documenti.num_documento)
lstr_ricerca_documenti.tipo_documento = "BV"

window_open_parm(w_ricerca_documenti, 0, lstr_ricerca_documenti)

if isvalid(message.powerobjectparm) and not isnull(message.powerobjectparm) then
	lstr_ricerca_documenti = message.powerobjectparm
	
	dw_non_conformita_vendite_det_1.setitem(1, "cod_cliente", lstr_ricerca_documenti.cod_cliente)
	dw_non_conformita_vendite_det_1.setitem(1, "cod_prodotto", lstr_ricerca_documenti.cod_prodotto)
	dw_non_conformita_vendite_det_1.setitem(1, "anno_registrazione_bol_ven", lstr_ricerca_documenti.anno_registrazione)
	dw_non_conformita_vendite_det_1.setitem(1, "num_registrazione_bol_ven", lstr_ricerca_documenti.num_registrazione)
	dw_non_conformita_vendite_det_1.setitem(1, "prog_riga_bol_ven", lstr_ricerca_documenti.prog_riga)
end if
end subroutine

public subroutine wf_cerca_ord_ven ();long ll_anno
str_ricerca_documenti lstr_ricerca_documenti

dw_non_conformita_vendite_det_1.accepttext()

ll_anno = dw_non_conformita_vendite_det_1.getitemnumber(1, "anno_registrazione_ord_ven")
if isnull(ll_anno) then
	g_mb.messagebox("Ricerca Ordine", "Selezionare l'anno fiscale su ciu effetuare la ricerca", StopSign!)
	dw_non_conformita_vendite_det_1.setColumn("anno_registrazione_ord_ven")
	return
end if

lstr_ricerca_documenti.cod_cliente = dw_non_conformita_vendite_det_1.getitemstring(1, "cod_cliente")
lstr_ricerca_documenti.cod_prodotto = dw_non_conformita_vendite_det_1.getitemstring(1, "cod_prodotto")
lstr_ricerca_documenti.anno_registrazione = dw_non_conformita_vendite_det_1.getitemnumber(1, "anno_registrazione_ord_ven")
lstr_ricerca_documenti.num_registrazione = dw_non_conformita_vendite_det_1.getitemnumber(1, "num_registrazione_ord_ven")
setnull(lstr_ricerca_documenti.prog_riga)
setnull(lstr_ricerca_documenti.cod_documento)
setnull(lstr_ricerca_documenti.anno_documento)
setnull(lstr_ricerca_documenti.numeratore_documento)
setnull(lstr_ricerca_documenti.num_documento)
lstr_ricerca_documenti.tipo_documento = "OV"

window_open_parm(w_ricerca_documenti, 0, lstr_ricerca_documenti)

if isvalid(message.powerobjectparm) and not isnull(message.powerobjectparm) then
	lstr_ricerca_documenti = message.powerobjectparm
	
	dw_non_conformita_vendite_det_1.setitem(1, "cod_cliente", lstr_ricerca_documenti.cod_cliente)
	dw_non_conformita_vendite_det_1.setitem(1, "cod_prodotto", lstr_ricerca_documenti.cod_prodotto)
	dw_non_conformita_vendite_det_1.setitem(1, "anno_registrazione_ord_ven", lstr_ricerca_documenti.anno_registrazione)
	dw_non_conformita_vendite_det_1.setitem(1, "num_registrazione_ord_ven", lstr_ricerca_documenti.num_registrazione)
	dw_non_conformita_vendite_det_1.setitem(1, "prog_riga_ord_ven", lstr_ricerca_documenti.prog_riga)
end if
end subroutine

public subroutine wf_cerca_fat_ven ();long ll_anno
str_ricerca_documenti lstr_ricerca_documenti

dw_non_conformita_vendite_det_1.accepttext()

ll_anno = dw_non_conformita_vendite_det_1.getitemnumber(1, "anno_reg_fat_ven")
if isnull(ll_anno) or ll_anno < 1900 then
	g_mb.messagebox("Ricerca Fattura", "Selezionare l'anno fiscale su ciu effetuare la ricerca", StopSign!)
	dw_non_conformita_vendite_det_1.setColumn("anno_reg_fat_ven")
	return
end if

lstr_ricerca_documenti.cod_cliente = dw_non_conformita_vendite_det_1.getitemstring(1, "cod_cliente")
lstr_ricerca_documenti.cod_prodotto = dw_non_conformita_vendite_det_1.getitemstring(1, "cod_prodotto")
lstr_ricerca_documenti.anno_registrazione = dw_non_conformita_vendite_det_1.getitemnumber(1, "anno_reg_fat_ven")
lstr_ricerca_documenti.num_registrazione = dw_non_conformita_vendite_det_1.getitemnumber(1, "num_reg_fat_ven")
setnull(lstr_ricerca_documenti.prog_riga)
setnull(lstr_ricerca_documenti.cod_documento)
setnull(lstr_ricerca_documenti.anno_documento)
setnull(lstr_ricerca_documenti.numeratore_documento)
setnull(lstr_ricerca_documenti.num_documento)
lstr_ricerca_documenti.tipo_documento = "FV"

window_open_parm(w_ricerca_documenti, 0, lstr_ricerca_documenti)

if isvalid(message.powerobjectparm) and not isnull(message.powerobjectparm) then
	lstr_ricerca_documenti = message.powerobjectparm
	
	dw_non_conformita_vendite_det_1.setitem(1, "cod_cliente", lstr_ricerca_documenti.cod_cliente)
	dw_non_conformita_vendite_det_1.setitem(1, "cod_prodotto", lstr_ricerca_documenti.cod_prodotto)
	dw_non_conformita_vendite_det_1.setitem(1, "anno_reg_fat_ven", lstr_ricerca_documenti.anno_registrazione)
	dw_non_conformita_vendite_det_1.setitem(1, "num_reg_fat_ven", lstr_ricerca_documenti.num_registrazione)
	dw_non_conformita_vendite_det_1.setitem(1, "prog_riga_fat_ven", lstr_ricerca_documenti.prog_riga)
end if
end subroutine

event pc_setwindow;call super::pc_setwindow;dw_non_conformita_interna_lista.set_dw_options(sqlca,pcca.null_object, c_modifyonopen + c_nodelete + c_nonew, c_default)
dw_non_conformita_vendite_det_1.set_dw_options(sqlca,dw_non_conformita_interna_lista,c_sharedata+c_scrollparent,c_default)
dw_non_conformita_vendite_det_2.set_dw_options(sqlca,dw_non_conformita_interna_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_non_conformita_interna_lista

windowobject l_objects[ ]

l_objects[1] = dw_non_conformita_vendite_det_2
l_objects[2] = cb_manutenzione_ric
dw_folder.fu_AssignTab(2, "Dati Interni", l_Objects[])
l_objects[1] = dw_non_conformita_vendite_det_1
l_objects[2] = cb_ricerca_stock
dw_folder.fu_AssignTab(1, "Riferimenti", l_Objects[])

dw_folder.fu_FolderCreate(2,4)
dw_folder.fu_SelectTab(1)     
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_non_conformita_vendite_det_1,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_vendite_det_2,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_non_conformita_vendite_det_2,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_non_conformita_vendite.create
int iCurrent
call super::create
this.cb_manutenzione_ric=create cb_manutenzione_ric
this.cb_ricerca_stock=create cb_ricerca_stock
this.cb_manutenz=create cb_manutenz
this.cb_intervento=create cb_intervento
this.cb_visita=create cb_visita
this.dw_non_conformita_interna_lista=create dw_non_conformita_interna_lista
this.dw_folder=create dw_folder
this.dw_non_conformita_vendite_det_2=create dw_non_conformita_vendite_det_2
this.dw_non_conformita_vendite_det_1=create dw_non_conformita_vendite_det_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_manutenzione_ric
this.Control[iCurrent+2]=this.cb_ricerca_stock
this.Control[iCurrent+3]=this.cb_manutenz
this.Control[iCurrent+4]=this.cb_intervento
this.Control[iCurrent+5]=this.cb_visita
this.Control[iCurrent+6]=this.dw_non_conformita_interna_lista
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.dw_non_conformita_vendite_det_2
this.Control[iCurrent+9]=this.dw_non_conformita_vendite_det_1
end on

on w_non_conformita_vendite.destroy
call super::destroy
destroy(this.cb_manutenzione_ric)
destroy(this.cb_ricerca_stock)
destroy(this.cb_manutenz)
destroy(this.cb_intervento)
destroy(this.cb_visita)
destroy(this.dw_non_conformita_interna_lista)
destroy(this.dw_folder)
destroy(this.dw_non_conformita_vendite_det_2)
destroy(this.dw_non_conformita_vendite_det_1)
end on

event getfocus;call super::getfocus;dw_non_conformita_vendite_det_2.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "anno_commessa"
s_cs_xx.parametri.parametro_s_2 = "num_commessa"
s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_vendite_det_2.getitemstring(dw_non_conformita_vendite_det_2.getrow(),"cod_prodotto")

end event

type cb_manutenzione_ric from commandbutton within w_non_conformita_vendite
integer x = 1189
integer y = 560
integer width = 69
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;dw_non_conformita_vendite_det_2.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "num_reg_manutenzione"
s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_vendite_det_2.getitemstring(dw_non_conformita_vendite_det_2.getrow(),"cod_attrezzatura")
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Manutenzione","Codice attrezzatura mancante",Stopsign!)
   return
end if

window_open(w_ricerca_manutenzione, 0)
end event

type cb_ricerca_stock from commandbutton within w_non_conformita_vendite
integer x = 2171
integer y = 460
integer width = 69
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_vendite_det_1.getitemstring(dw_non_conformita_vendite_det_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_non_conformita_vendite_det_1.getitemstring(dw_non_conformita_vendite_det_1.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_non_conformita_vendite_det_1.getitemstring(dw_non_conformita_vendite_det_1.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_non_conformita_vendite_det_1.getitemstring(dw_non_conformita_vendite_det_1.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_non_conformita_vendite_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)
end event

type cb_manutenz from uo_richiesta_manutenz within w_non_conformita_vendite
boolean visible = false
integer x = 1371
integer y = 1220
integer taborder = 30
end type

type cb_intervento from uo_richiesta_intervento within w_non_conformita_vendite
boolean visible = false
integer x = 1760
integer y = 1220
integer height = 80
integer taborder = 20
end type

type cb_visita from uo_richiesta_visita within w_non_conformita_vendite
boolean visible = false
integer x = 2149
integer y = 1220
integer height = 80
integer taborder = 10
end type

type dw_non_conformita_interna_lista from uo_cs_xx_dw within w_non_conformita_vendite
integer x = 2560
integer y = 60
integer width = 709
integer height = 160
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_non_conformita_interna_lista"
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;
dw_non_conformita_vendite_det_2.object.b_ricerca_commessa.enabled = true
dw_non_conformita_vendite_det_1.object.b_ricerca_cliente.enabled = true
dw_non_conformita_vendite_det_1.object.b_ricerca_prodotto.enabled = true
dw_non_conformita_vendite_det_1.object.b_ricerca_fat_ven.enabled = true
dw_non_conformita_vendite_det_1.object.b_ricerca_ord_ven.enabled = true
dw_non_conformita_vendite_det_1.object.b_ricerca_bol_ven.enabled = true
cb_ricerca_stock.enabled = true
cb_manutenzione_ric.enabled = true

cb_manutenz.enabled = false
cb_intervento.enabled = false
cb_visita.enabled = false

end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_deposito, ls_cod_prodotto, ls_cod_ubicazione, ls_cod_lotto
datetime ld_data_stock
long   ll_prog_stock, ll_qta_stock, ll_qta_nc

ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
ls_cod_deposito = getitemstring(getrow(),"cod_deposito")
ls_cod_ubicazione = getitemstring(getrow(),"cod_ubicazione")
ls_cod_lotto = getitemstring(getrow(),"cod_lotto")
ld_data_stock = getitemdatetime(getrow(),"data_stock")
ll_prog_stock = getitemnumber(getrow(),"prog_stock")
ll_qta_nc = getitemnumber(getrow(),"quan_non_conformita")


SELECT stock.quan_assegnata  
INTO   :ll_qta_stock  
FROM   stock  
WHERE  (stock.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       (stock.cod_prodotto = :ls_cod_prodotto ) AND  
       (stock.cod_deposito = :ls_cod_deposito ) AND  
       (stock.cod_ubicazione = :ls_cod_ubicazione ) AND  
       (stock.cod_lotto = :ls_cod_lotto ) AND  
       (stock.data_stock = :ld_data_stock ) AND  
       (stock.prog_stock = :ll_prog_stock )   ;

if (sqlca.sqlcode = 0) and (ll_qta_stock > 0) and (ll_qta_nc <> ll_qta_stock) then
   g_mb.messagebox("Non Conformità","Attenzione: la quantità stock non corrisponde con la quantità non conformtà!", Information!)
end if

end event

event pcd_view;call super::pcd_view;
dw_non_conformita_vendite_det_2.object.b_ricerca_commessa.enabled = false
dw_non_conformita_vendite_det_1.object.b_ricerca_cliente.enabled = false
dw_non_conformita_vendite_det_1.object.b_ricerca_prodotto.enabled = false
dw_non_conformita_vendite_det_1.object.b_ricerca_fat_ven.enabled = false
dw_non_conformita_vendite_det_1.object.b_ricerca_ord_ven.enabled = false
dw_non_conformita_vendite_det_1.object.b_ricerca_bol_ven.enabled = false
cb_ricerca_stock.enabled = false
cb_manutenzione_ric.enabled = false

cb_manutenz.enabled = true
cb_intervento.enabled = true
cb_visita.enabled = true


end event

event pcd_modify;call super::pcd_modify;
dw_non_conformita_vendite_det_2.object.b_ricerca_commessa.enabled = true
dw_non_conformita_vendite_det_1.object.b_ricerca_cliente.enabled = true
dw_non_conformita_vendite_det_1.object.b_ricerca_prodotto.enabled = true
dw_non_conformita_vendite_det_1.object.b_ricerca_fat_ven.enabled = true
dw_non_conformita_vendite_det_1.object.b_ricerca_ord_ven.enabled = true
dw_non_conformita_vendite_det_1.object.b_ricerca_bol_ven.enabled = true
cb_ricerca_stock.enabled = true
cb_manutenzione_ric.enabled = true

cb_manutenz.enabled = false
cb_intervento.enabled = false
cb_visita.enabled = false

// modifiche Michela 07/02/2003 : la table schede_manutenzioni non esiste, deve essere sostituita con manutenzioni
if not isnull(this.getitemstring(this.getrow(),"cod_attrezzatura")) then
	f_PO_LoadDDLB_DW(dw_non_conformita_vendite_det_2, &
                    "num_reg_manutenzione", &
                    SQLCA, &
                    "manutenzioni", &
                    "num_registrazione" , &
                    "num_registrazione" , &
                    "(manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
                    "(manutenzioni.cod_attrezzatura = '" + this.getitemstring(this.getrow(),"cod_attrezzatura") + "')" )
end if
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_d_1, s_cs_xx.parametri.parametro_d_2)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_folder from u_folder within w_non_conformita_vendite
integer x = 23
integer y = 20
integer width = 2491
integer height = 1180
integer taborder = 130
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Riferimenti"
      SetFocus(dw_non_conformita_vendite_det_1)
   CASE "Dati Interni"
      SetFocus(dw_non_conformita_vendite_det_2)
END CHOOSE



end on

type dw_non_conformita_vendite_det_2 from uo_cs_xx_dw within w_non_conformita_vendite
integer x = 69
integer y = 120
integer width = 2400
integer height = 1000
integer taborder = 120
string dataobject = "d_non_conformita_vendite_det_2"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;// modifiche Michela 07/02/2003 : la table schede_manutenzioni non esiste, deve essere sostituita con manutenzioni

if i_extendmode then

long ll_null
string ls_str

setnull(ll_null)

choose case i_colname

case "cod_attrezzatura" 
   ls_str = this.getitemstring(this.getrow(), "cod_attrezzatura")
   if isnull(ls_str) then ls_str = " "
   if i_coltext <> ls_str  then
      g_mb.messagebox("Non Conformità Interna","Attrezzatura variata: azzero il numero registraz. manutenzione",Exclamation!)
      this.setitem(this.getrow(), "num_reg_manutenzione", ll_null)
      f_PO_LoadDDLB_DW(dw_non_conformita_vendite_det_2, &
                       "num_reg_manutenzione", &
                       SQLCA, &
                       "manutenzioni", &
                       "num_registrazione" , &
                       "num_registrazione" , &
                       "(manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (manutenzioni.cod_attrezzatura = '" + i_coltext + "')" )
   end if

case "anno_commessa" 
   ls_str = string(int(this.getitemnumber(this.getrow(), "anno_commessa")))
   if isnull(ls_str) then ls_str = " "
   if i_coltext <> ls_str  then
      g_mb.messagebox("Non Conformità Interna","Anno Commessa variato: azzero il numero commessa !",Exclamation!)
      this.setitem(this.getrow(), "num_commessa", ll_null)
      f_PO_LoadDDLB_DW(dw_non_conformita_vendite_det_2, &
                       "num_commessa", &
                       SQLCA, &
                       "anag_commesse", &
                       "num_commessa" , &
                       "num_commessa" , &
                       "(anag_commesse.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (anag_commesse.anno_commessa = '" + i_coltext + "') and &
                        ( (anag_commesse.cod_cliente = '" + dw_non_conformita_vendite_det_1.getitemstring(dw_non_conformita_vendite_det_1.getrow(), "cod_cliente") + "') or &
                          (anag_commesse.cod_cliente is null))" )
   end if


end choose
end if   // non toccare: si riferisce al controllo dellavariabile i_extendmode
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_commessa"
		guo_ricerca.uof_ricerca_commessa(dw_non_conformita_vendite_det_2,"anno_commessa","num_commessa")
end choose
end event

type dw_non_conformita_vendite_det_1 from uo_cs_xx_dw within w_non_conformita_vendite
integer x = 69
integer y = 120
integer width = 2354
integer height = 1040
integer taborder = 100
boolean bringtotop = true
string dataobject = "d_non_conformita_vendite_det_1"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

string ls_cod_prod_fornitore, ls_prodotto, ls_fornitore, ls_null, ls_cliente
long   ll_null
datetime   ld_null

setnull(ls_null)
setnull(ld_null)
setnull(ll_null)

choose case i_colname

case "cod_prodotto"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   if i_coltext <> ls_prodotto  then
      g_mb.messagebox("Non Conformità Interna","Prodotto variato: devi reimpostare stock, lavoraz., rif.for.!",Exclamation!)
      this.setitem(this.getrow(), "cod_reparto", ls_null)
      this.setitem(this.getrow(), "cod_lavorazione", ls_null)
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
      this.setitem(this.getrow(), "cod_deposito", ls_null)
      this.setitem(this.getrow(), "cod_ubicazione", ls_null)
      this.setitem(this.getrow(), "cod_lotto", ls_null)
      this.setitem(this.getrow(), "data_stock", ld_null)
      this.setitem(this.getrow(), "prog_stock", ll_null)
   end if

case "cod_fornitore"
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")
   if i_coltext <> ls_fornitore  then
      g_mb.messagebox("Non Conformità Interna","Fornitore variato: devi reimpostare prodotto fornitore",Exclamation!)
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
   end if

case "cod_deposito"
   this.setitem(this.getrow(), "cod_ubicazione", ls_null)
   this.setitem(this.getrow(), "cod_lotto", ls_null)
   this.setitem(this.getrow(), "data_stock", ld_null)
   this.setitem(this.getrow(), "prog_stock", ll_null)
   if not isnull(i_coltext) then
      f_PO_LoadDDLB_DW(dw_non_conformita_vendite_det_1, &
                       "cod_ubicazione", &
                       SQLCA, &
                       "stock", &
                       "cod_ubicazione" , &
                       "cod_ubicazione" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (stock.cod_deposito = '" + i_coltext + "')")
   end if

case "cod_ubicazione"
   this.setitem(this.getrow(), "cod_lotto", ls_null)
   this.setitem(this.getrow(), "data_stock", ld_null)
   this.setitem(this.getrow(), "prog_stock", ll_null)
   if not isnull(i_coltext) then
      f_PO_LoadDDLB_DW(dw_non_conformita_vendite_det_1, &
                       "cod_lotto", &
                       SQLCA, &
                       "stock", &
                       "cod_lotto" , &
                       "cod_lotto" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
                        (stock.cod_deposito = '" + this.getitemstring(this.getrow(),"cod_ubicazione") + "') and &
                        (stock.cod_ubicazione = '" + i_coltext + "')")
   end if



//case "cod_lotto"
//	if not isnull(this.getitemdatetime(this.getrow(), "data_stock")) then 
//		this.setitem(this.getrow(), "data_stock", ld_null)
//   if not isnull(this.getitemnumber(this.getrow(), "prog_stock")) then this.setitem(this.getrow(), "prog_stock", ll_null)


case "cod_cliente"
   ls_cliente = this.getitemstring(this.getrow(), "cod_cliente")
   if i_coltext <> ls_cliente  then
      g_mb.messagebox("Non Conformità Interna","Cliente variato: devi reimpostare Ordine e Bolla !",Exclamation!)
      this.setitem(this.getrow(), "anno_registrazione_bol_ven", ll_null)
      this.setitem(this.getrow(), "num_registrazione_bol_ven", ll_null)
      this.setitem(this.getrow(), "prog_riga_bol_ven", ll_null)
      this.setitem(this.getrow(), "anno_registrazione_ord_ven", ll_null)
      this.setitem(this.getrow(), "num_registrazione_ord_ven", ll_null)
      this.setitem(this.getrow(), "prog_riga_ord_ven", ll_null)
   end if

case "anno_registrazione_bol_ven"
   ls_cliente = string(this.getitemnumber(this.getrow(), "anno_registrazione_bol_ven"))
   if isnull(ls_cliente) then ls_cliente = " "
   g_mb.messagebox("Non Conformità Interna","Cambiato Anno Bolla: devi reimpostare numero e riga bolla !",Exclamation!)
   this.setitem(this.getrow(), "num_registrazione_bol_ven", ll_null)
   this.setitem(this.getrow(), "prog_riga_bol_ven", ll_null)
   f_PO_LoadDDLB_DW(dw_non_conformita_vendite_det_1,"num_registrazione_bol_ven",sqlca,&
              "tes_bol_ven","num_registrazione","num_registrazione",&
              "(tes_bol_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
               (tes_bol_ven.anno_registrazione = "+i_coltext+")" )


case "num_registrazione_bol_ven"
   ls_prodotto = string(this.getitemnumber(this.getrow(), "anno_registrazione_bol_ven"))
   ls_cliente = string(this.getitemnumber(this.getrow(), "num_registrazione_bol_ven"))
   if isnull(ls_cliente) then ls_cliente = " "
   if isnull(ls_prodotto) then ls_prodotto = " "
   g_mb.messagebox("Non Conformità Interna","Cambiato Numero Bolla: devi reimpostare riga bolla !",Exclamation!)
   this.setitem(this.getrow(), "prog_riga_bol_ven", ll_null)
   f_PO_LoadDDLB_DW(dw_non_conformita_vendite_det_1,"prog_riga_bol_ven",sqlca,&
              "det_bol_ven","prog_riga_bol_ven","prog_riga_bol_ven",&
              "(det_bol_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
               (det_bol_ven.anno_registrazione = "+ls_prodotto+") and &
               (det_bol_ven.num_registrazione = "+i_coltext+")" )

end choose



// ------------------------  ricerca automatica ricerca prodotto fornitore --------------------

choose case i_colname
case "cod_prodotto"
   ls_prodotto = i_coltext
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")
case "cod_fornitore"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   ls_fornitore =i_coltext
case "cod_prod_fornitore"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   ls_fornitore =this.getitemstring(this.getrow(), "cod_fornitore")
end choose

if ( (i_colname = "cod_prod_fornitore") or (i_colname = "cod_prodotto") or (i_colname = "cod_fornitore") ) and &
   (not(isnull(ls_fornitore))) and &
   (not(isnull(ls_prodotto)))  then

   select tab_prod_fornitori.cod_prod_fornitore
   into   :ls_cod_prod_fornitore
   from   tab_prod_fornitori
   where  (tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and
          (tab_prod_fornitori.cod_prodotto = :ls_prodotto) and
          (tab_prod_fornitori.cod_fornitore = :ls_fornitore) ;

 	if sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_prod_fornitore", ls_cod_prod_fornitore)
   else
      this.setitem(this.getrow(), "cod_prod_fornitore", "Non Disponibile!")
   end if
end if

end if
end event

event clicked;call super::clicked;choose case dwo.name
	case "b_ricerca_bol_ven"
		wf_cerca_bolla_ven()
		
	case "b_ricerca_ord_ven"
		wf_cerca_ord_ven()
		
	case "b_ricerca_fat_ven"
		wf_cerca_fat_ven()
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_non_conformita_vendite_det_1,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_non_conformita_vendite_det_1,"cod_prodotto")
end choose
end event

