﻿$PBExportHeader$w_registro_azioni_prev.srw
forward
global type w_registro_azioni_prev from w_cs_xx_principale
end type
type dw_registro_azioni_prev from uo_cs_xx_dw within w_registro_azioni_prev
end type
end forward

global type w_registro_azioni_prev from w_cs_xx_principale
integer width = 3474
integer height = 2392
string title = "Registro Azioni Preventive"
dw_registro_azioni_prev dw_registro_azioni_prev
end type
global w_registro_azioni_prev w_registro_azioni_prev

type variables

end variables

on w_registro_azioni_prev.create
int iCurrent
call super::create
this.dw_registro_azioni_prev=create dw_registro_azioni_prev
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_registro_azioni_prev
end on

on w_registro_azioni_prev.destroy
call super::destroy
destroy(this.dw_registro_azioni_prev)
end on

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

set_w_options(c_noresizewin)

save_on_close(c_socnosave)


dw_registro_azioni_prev.set_dw_options(sqlca, &
								pcca.null_object, &
								c_nonew + &
								c_nomodify + &
								c_nodelete + &
								c_noenablenewonopen + &
								c_noenablemodifyonopen + &
								c_scrollparent + &
								c_disablecc, &
								c_noresizedw + &
								c_nohighlightselected + &
								c_nocursorrowfocusrect + &
								c_nocursorrowpointer)	




									





									 


end event

event open;call super::open;integer  li_i, li_anno
long     ll_righe, ll_num
date     ldt_data
string   ls_descrizione, ls_stato, emessa_da, approvata_da, chiusa_da, verificata_da, &
         ls_path_logo, ls_modify, ls_cod_area, ls_des_area

if s_cs_xx.parametri.parametro_dw_1.rowcount() > 0 then	
	for li_i = 1 to s_cs_xx.parametri.parametro_dw_1.rowcount()
		li_anno = s_cs_xx.parametri.parametro_dw_1.getitemnumber(li_i,"anno_registrazione")
		ll_num = s_cs_xx.parametri.parametro_dw_1.getitemnumber(li_i,"num_registrazione")
		ldt_data = date(s_cs_xx.parametri.parametro_dw_1.getitemdatetime(li_i,"data_azione_preventiva"))
		ls_descrizione = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_i,"descrizione")		
		emessa_da = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_i,"emessa_da")	
		approvata_da = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_i,"approvata_da")	
		chiusa_da = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_i,"chiusa_da")	
		verificata_da = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_i,"verificata_da")
		if not isnull(emessa_da) and isnull(approvata_da) and isnull(chiusa_da) and isnull(verificata_da) then
			ls_stato = "aperta"
		elseif not isnull(emessa_da) and not isnull(approvata_da) and isnull(chiusa_da) and isnull(verificata_da) then
			ls_stato = "approvata"
		elseif not isnull(emessa_da) and not isnull(approvata_da) and not isnull(chiusa_da) and isnull(verificata_da) then
			ls_stato = "chiusa"
		elseif not isnull(emessa_da) and not isnull(approvata_da) and not isnull(chiusa_da) and not isnull(verificata_da) then
			ls_stato = "verificata"
		end if
		
		ls_cod_area = s_cs_xx.parametri.parametro_dw_1.getitemstring(li_i,"cod_area_aziendale")
		
		if not isnull(ls_cod_area) then
			ls_des_area = f_des_tabella("tab_aree_aziendali","cod_area_aziendale = '" + ls_cod_area + "'","des_area")
		else
			setnull(ls_des_area)
		end if
		
		ll_righe = dw_registro_azioni_prev.insertrow(li_i)
		dw_registro_azioni_prev.SetItem(ll_righe,"anno_registrazione",li_anno)
		dw_registro_azioni_prev.SetItem(ll_righe,"num_registrazione",ll_num)
		dw_registro_azioni_prev.SetItem(ll_righe,"data_registrazione",ldt_data)
		dw_registro_azioni_prev.SetItem(ll_righe,"descrizione",ls_descrizione)
		dw_registro_azioni_prev.SetItem(ll_righe,"stato",ls_stato)
		
		dw_registro_azioni_prev.SetItem(ll_righe,"cod_area",ls_cod_area)
		
		dw_registro_azioni_prev.SetItem(ll_righe,"des_area",ls_des_area)
		
		setnull(li_anno)
		setnull(ll_num)
		setnull(ldt_data)
		setnull(ls_descrizione)
		setnull(ls_stato)
	next
	
	select parametri_azienda.stringa
	into   :ls_path_logo
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
	       parametri_azienda.flag_parametro = 'S' and &
	       parametri_azienda.cod_parametro = 'LOA';

	if sqlca.sqlcode < 0 then 
		messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	if sqlca.sqlcode = 100 then 
		messagebox("OMNIA","manca il parametro LOA in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)
	end if	

	ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo + "'"
	
	dw_registro_azioni_prev.modify(ls_modify)	
//	dw_registro_azioni_prev.Print()
//	dw_registro_azioni_prev.reset()




	return 0
else
	messagebox("OMNIA","Attenzione! La lista di ricerca risulta essere vuota!")
	return -1
end if
end event

type dw_registro_azioni_prev from uo_cs_xx_dw within w_registro_azioni_prev
integer x = 23
integer y = 20
integer width = 3383
integer height = 2240
integer taborder = 10
string dataobject = "d_report_registro_az_prev"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

