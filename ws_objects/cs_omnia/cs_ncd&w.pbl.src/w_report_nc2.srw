﻿$PBExportHeader$w_report_nc2.srw
forward
global type w_report_nc2 from w_cs_xx_principale
end type
type dw_report_nc from uo_cs_xx_dw within w_report_nc2
end type
end forward

global type w_report_nc2 from w_cs_xx_principale
integer width = 3840
integer height = 2380
string title = "Report Non Conformità"
dw_report_nc dw_report_nc
end type
global w_report_nc2 w_report_nc2

type variables
long il_anno, il_num
end variables

forward prototypes
public function integer wf_ds_fasi (ref datastore fds_fasi)
public function integer wf_ds_nc (ref datastore fds_nc)
public function integer wf_ds_campi (ref datastore fds_campi, string fs_cod_tipo_causa, long fl_num_sequenza_corrente)
public function integer wf_responsabile (string fs_cod_tipo_causa, long fl_num_sequenza_corrente, long fl_anno_non_conf, long fl_num_non_conf, ref string fs_cod_responsabile, ref string fs_firma_responsabile, ref datetime fdt_data_firma)
public function string wf_etichetta (string fs_colonna)
public function integer wf_report ()
public function string wf_des_tabella_campi_flag (string fs_colonna, string fs_valore)
end prototypes

public function integer wf_ds_fasi (ref datastore fds_fasi);string ls_sql, ls_sintassi, ls_errore, ls_cod_area_aziendale
long ll_righe, ll_max_num_sequenza

string ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_tipo_causa, ls_cod_destinatario

select cod_tipo_causa
into   :ls_cod_tipo_causa
from   non_conformita 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_non_conf = :il_anno and num_non_conf= :il_num;

select cod_tipo_lista_dist,
		 cod_lista_dist
into   :ls_cod_tipo_lista_dist,
		 :ls_cod_lista_dist
from   tab_tipi_cause 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_causa = :ls_cod_tipo_causa;
		 
select stringa
into   :ls_cod_destinatario
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'PCU';

select max(num_sequenza)
into :ll_max_num_sequenza
from det_liste_dist
where cod_azienda=:s_cs_xx.cod_azienda
	and cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and cod_lista_dist = :ls_cod_lista_dist
	and cod_destinatario = :ls_cod_destinatario;

select cod_area_aziendale
into :ls_cod_area_aziendale
from non_conformita
where cod_azienda=:s_cs_xx.cod_azienda
	and anno_non_conf=:il_anno and num_non_conf=:il_num
;
if ls_cod_area_aziendale = "" then setnull(ls_cod_area_aziendale)

ls_sql = &
"select "+&
	"det_liste_dist.des_fase,"+&
	"det_liste_dist_destinatari.cod_destinatario_mail,"+&
	"det_liste_dist.num_sequenza,"+&
	"non_conformita.num_sequenza_corrente,"+&
	"non_conformita.firma_chiusura,"+&
	"non_conformita.data_chiusura,"+&
	"det_liste_dist_destinatari.cod_destinatario_mail,"+&
	"mansionari.nome"+&
	",det_liste_dist.num_sequenza_prec "+&
	" "+&
"from det_liste_dist "+&
"join det_liste_dist_destinatari on det_liste_dist_destinatari.cod_azienda=det_liste_dist.cod_azienda "+&
	"and det_liste_dist_destinatari.cod_tipo_lista_dist=det_liste_dist.cod_tipo_lista_dist "+&
	"and det_liste_dist_destinatari.cod_lista_dist=det_liste_dist.cod_lista_dist "+&
	"and det_liste_dist_destinatari.cod_destinatario=det_liste_dist.cod_destinatario "+&
	"and det_liste_dist_destinatari.num_sequenza=det_liste_dist.num_sequenza "+&
"join tab_tipi_cause on tab_tipi_cause.cod_azienda=det_liste_dist.cod_azienda "+&
	"and tab_tipi_cause.cod_tipo_lista_dist=det_liste_dist.cod_tipo_lista_dist "+&
	"and tab_tipi_cause.cod_lista_dist=det_liste_dist.cod_lista_dist "+&
"join non_conformita on non_conformita.cod_azienda=tab_tipi_cause.cod_azienda "+&
	"and non_conformita.cod_tipo_causa=tab_tipi_cause.cod_tipo_causa "+&
"left outer join mansionari on mansionari.cod_azienda=non_conformita.cod_azienda "+&
	"and mansionari.cod_utente=det_liste_dist_destinatari.cod_destinatario_mail "+&
"where non_conformita.cod_azienda='"+s_cs_xx.cod_azienda+"' "+&
	"and non_conformita.anno_non_conf="+string(il_anno)+" "+&
	"and non_conformita.num_non_conf="+string(il_num)+" "+&	
	"and det_liste_dist_destinatari.flag_responsabile='S' "

if not isnull(ls_cod_area_aziendale) then
	ls_sql +="and det_liste_dist_destinatari.cod_area_aziendale='"+ls_cod_area_aziendale+"' "
else
	//ls_sql +="and det_liste_dist_destinatari.cod_area_aziendale is null "
end if


ls_sql += &
" " + &
"union all " + &
"select "+&
	"det_liste_dist.des_fase,"+&
	"det_liste_dist_destinatari.cod_destinatario_mail,"+&
	"det_liste_dist.num_sequenza,"+&
	"non_conformita.num_sequenza_corrente,"+&
	"non_conformita.firma_chiusura,"+&
	"non_conformita.data_chiusura,"+&
	"det_liste_dist_destinatari.cod_destinatario_mail,"+&
	"mansionari.nome"+&
	",det_liste_dist.num_sequenza_prec "+&
	" "+&
"from det_liste_dist "+&
"join det_liste_dist_destinatari on det_liste_dist_destinatari.cod_azienda=det_liste_dist.cod_azienda "+&
	"and det_liste_dist_destinatari.cod_tipo_lista_dist=det_liste_dist.cod_tipo_lista_dist "+&
	"and det_liste_dist_destinatari.cod_lista_dist=det_liste_dist.cod_lista_dist "+&
	"and det_liste_dist_destinatari.cod_destinatario=det_liste_dist.cod_destinatario "+&
	"and det_liste_dist_destinatari.num_sequenza=det_liste_dist.num_sequenza "+&
"join tab_tipi_cause on tab_tipi_cause.cod_azienda=det_liste_dist.cod_azienda "+&
	"and tab_tipi_cause.cod_tipo_lista_dist=det_liste_dist.cod_tipo_lista_dist "+&
	"and tab_tipi_cause.cod_lista_dist=det_liste_dist.cod_lista_dist "+&
"join non_conformita on non_conformita.cod_azienda=tab_tipi_cause.cod_azienda "+&
	"and non_conformita.cod_tipo_causa=tab_tipi_cause.cod_tipo_causa "+&
"left outer join mansionari on mansionari.cod_azienda=non_conformita.cod_azienda "+&
	"and mansionari.cod_utente=det_liste_dist_destinatari.cod_destinatario_mail "+&
"where non_conformita.cod_azienda='"+s_cs_xx.cod_azienda+"' "+&
	"and non_conformita.anno_non_conf="+string(il_anno)+" "+&
	"and non_conformita.num_non_conf="+string(il_num)+" "+&	
	"and det_liste_dist_destinatari.flag_responsabile='S' "
ls_sql +="and det_liste_dist_destinatari.cod_area_aziendale is null"

//fase iniziale: nessun responsabile
ls_sql += &
" " + &
"union all " + &
"select "+&
	"det_liste_dist.des_fase,"+&
	"'' as det_liste_dist_destinatari_cod_destinatario_mail,"+&
	"det_liste_dist.num_sequenza,"+&
	"non_conformita.num_sequenza_corrente,"+&
	"non_conformita.firma_chiusura,"+&
	"non_conformita.data_creazione as non_conformita_data_chiusura,"+&
	"'' as det_liste_dist_destinatari_cod_destinatario_mail,"+&
	"anag_operai.nome + ' ' + anag_operai.cognome as mansionari_nome"+&
	",det_liste_dist.num_sequenza_prec "+&
	" "+&
"from det_liste_dist "+&
"join det_liste_dist_destinatari on det_liste_dist_destinatari.cod_azienda=det_liste_dist.cod_azienda "+&
	"and det_liste_dist_destinatari.cod_tipo_lista_dist=det_liste_dist.cod_tipo_lista_dist "+&
	"and det_liste_dist_destinatari.cod_lista_dist=det_liste_dist.cod_lista_dist "+&
	"and det_liste_dist_destinatari.cod_destinatario=det_liste_dist.cod_destinatario "+&
	"and det_liste_dist_destinatari.num_sequenza=det_liste_dist.num_sequenza "+&
"join tab_tipi_cause on tab_tipi_cause.cod_azienda=det_liste_dist.cod_azienda "+&
	"and tab_tipi_cause.cod_tipo_lista_dist=det_liste_dist.cod_tipo_lista_dist "+&
	"and tab_tipi_cause.cod_lista_dist=det_liste_dist.cod_lista_dist "+&
"join non_conformita on non_conformita.cod_azienda=tab_tipi_cause.cod_azienda "+&
	"and non_conformita.cod_tipo_causa=tab_tipi_cause.cod_tipo_causa "+&
"left outer join anag_operai on anag_operai.cod_azienda=non_conformita.cod_azienda "+&
	"and anag_operai.cod_operaio=non_conformita.cod_operaio "+&
"where non_conformita.cod_azienda='"+s_cs_xx.cod_azienda+"' "+&
	"and non_conformita.anno_non_conf="+string(il_anno)+" "+&
	"and non_conformita.num_non_conf="+string(il_num)+" "+&	
	"and (det_liste_dist.num_sequenza_prec=0 or det_liste_dist.num_sequenza_prec is null) "+&
" "

//fase finale: nessun responsabile
ls_sql += &
" " + &
"union all " + &
"select "+&
	"det_liste_dist.des_fase,"+&
	"'' as det_liste_dist_destinatari_cod_destinatario_mail,"+&
	"det_liste_dist.num_sequenza,"+&
	"non_conformita.num_sequenza_corrente,"+&
	"non_conformita.firma_chiusura,"+&
	"non_conformita.data_creazione as non_conformita_data_chiusura,"+&
	"'' as det_liste_dist_destinatari_cod_destinatario_mail,"+&
	"'' as mansionari_nome"+&
	",det_liste_dist.num_sequenza_prec "+&
	" "+&
"from det_liste_dist "+&
"join det_liste_dist_destinatari on det_liste_dist_destinatari.cod_azienda=det_liste_dist.cod_azienda "+&
	"and det_liste_dist_destinatari.cod_tipo_lista_dist=det_liste_dist.cod_tipo_lista_dist "+&
	"and det_liste_dist_destinatari.cod_lista_dist=det_liste_dist.cod_lista_dist "+&
	"and det_liste_dist_destinatari.cod_destinatario=det_liste_dist.cod_destinatario "+&
	"and det_liste_dist_destinatari.num_sequenza=det_liste_dist.num_sequenza "+&
"join tab_tipi_cause on tab_tipi_cause.cod_azienda=det_liste_dist.cod_azienda "+&
	"and tab_tipi_cause.cod_tipo_lista_dist=det_liste_dist.cod_tipo_lista_dist "+&
	"and tab_tipi_cause.cod_lista_dist=det_liste_dist.cod_lista_dist "+&
"join non_conformita on non_conformita.cod_azienda=tab_tipi_cause.cod_azienda "+&
	"and non_conformita.cod_tipo_causa=tab_tipi_cause.cod_tipo_causa "+&
"left outer join anag_operai on anag_operai.cod_azienda=non_conformita.cod_azienda "+&
	"and anag_operai.cod_operaio=non_conformita.cod_operaio "+&
"where non_conformita.cod_azienda='"+s_cs_xx.cod_azienda+"' "+&
	"and non_conformita.anno_non_conf="+string(il_anno)+" "+&
	"and non_conformita.num_non_conf="+string(il_num)+" "+&	
	"and  det_liste_dist.num_sequenza="+ string(ll_max_num_sequenza)+" "+&
"and det_liste_dist_destinatari.cod_area_aziendale is null  "+&
" " + &
"order by det_liste_dist.num_sequenza "
				
ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)
IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore delle fasi:" + ls_errore)
   RETURN -1
END IF

fds_fasi = create datastore
fds_fasi.Create( ls_sintassi, ls_errore)
IF Len(ls_errore) > 0 THEN
	g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore delle fasi:" + ls_errore)
	destroy fds_fasi
	RETURN -1
END IF

fds_fasi.settransobject(sqlca)
ll_righe = fds_fasi.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve del datastore delle fasi NC:" + sqlca.sqlerrtext)
	destroy fds_fasi;
	return -1
end if

return ll_righe
end function

public function integer wf_ds_nc (ref datastore fds_nc);string ls_sql, ls_sintassi, ls_errore
long ll_righe

//recupero i campi già eventualmente salvati -------------------------------
ls_sql = &
"select "+&
	"cod_azienda,"+&
	"anno_non_conf,"+&
	"num_non_conf,"+&
	"flag_tipo_nc,"+&
	"cod_operaio,"+&
	"num_reg_manutenzione,"+&
	"data_creazione,"+&
	"data_scadenza,"+&
	"flag_prod_codificato,"+&
	"des_manuale_prod,"+&
	"tipo_prodotto,"+&
	"cod_prodotto,"+&
	"cod_deposito,"+&
	"cod_ubicazione,"+&
	"cod_lotto,"+&
	"data_stock,"+&
	"prog_stock,"+&
	"cod_misura,"+&
	"cod_errore,"+&
	"cod_trattamento,"+&
	"progr,"+&
	"quan_non_conformita,"+&
	"quan_derogata,"+&
	"quan_recuperata,"+&
	"quan_accettabile,"+&
	"quan_resa,"+&
	"quan_rottamata,"+&
	"quan_carico,"+&
	"flag_azione_correttiva,"+&
	"azione_intrapresa,"+&
	"cod_fornitore,"+&
	"cod_prod_fornitore,"+&
	"anno_registrazione_ord_acq,"+&
	"num_registrazione_ord_acq,"+&
	"prog_riga_ordine_acq,"+&
	"anno_bolla_acq,"+&
	"num_bolla_acq,"+&
	"cod_fornitore_bolla_acq,"+&
	"prog_riga_bolla_acq,"+&
	"anno_commessa,"+&
	"num_commessa,"+&
	"cod_reparto,"+&
	"cod_lavorazione,"+&
	"cod_attrezzatura,"+&
	"anno_reg_collaudi,"+&
	"num_reg_collaudi,"+&
	"cod_cliente,"+&
	"anno_registrazione_ord_ven,"+&
	"num_registrazione_ord_ven,"+&
	"prog_riga_ord_ven,"+&
	"anno_registrazione_bol_ven,"+&
	"num_registrazione_bol_ven,"+&
	"prog_riga_bol_ven,"+&
	"flag_comunicare_cli,"+&
	"flag_comunicare_for,"+&
	"flag_comunicare_int,"+&
	"cod_resp_divisione,"+&
	"flag_comunicato_cli,"+&
	"flag_comunicato_for,"+&
	"flag_comunicato_int,"+&
	"nome_doc_compilato,"+&
	"nome_doc_aut_deroga,"+&
	"azione_immediata,"+&
	"nota_causa_nc,"+&
	"nota_ac_preventive,"+&
	"nota_reclamo,"+&
	"flag_eseguito_ac,"+&
	"nota_verifiche_su_ac,"+&
	"cod_doc_origine,"+&
	"numeratore_doc_origine,"+&
	"anno_doc_origine,"+&
	"num_doc_origine,"+&
	"prog_riga_fat_acq,"+&
	"anno_reg_fat_ven,"+&
	"num_reg_fat_ven,"+&
	"prog_riga_fat_ven,"+&
	"anno_reg_off_ven,"+&
	"num_reg_off_ven,"+&
	"prog_riga_off_ven,"+&
	"anno_progetto,"+&
	"cod_progetto,"+&
	"num_ver_progetto,"+&
	"num_ediz_progetto,"+&
	"prog_riga_det_fasi,"+&
	"num_reg_lista,"+&
	"num_ver_lista,"+&
	"num_ediz_lista,"+&
	"livello_gravita,"+&
	"livello_impatto,"+&
	"cod_tipo_causa,"+&
	"cod_causa,"+&
	"cod_area_aziendale,"+&
	"cod_divisione,"+&
	"anno_registrazione,"+&
	"num_registrazione,"+&
	"progressivo,"+&
	"firma_chiusura,"+&
	"data_chiusura,"+&
	"costo_nc,"+&
	"verifica_efficacia,"+&
	"data_verifica_efficacia,"+&
	"cod_resp_ver_efficacia,"+&
	"anno_reg_az_corr,"+&
	"num_reg_az_corr,"+&
	"flag_stato_nc,"+&
	"num_sequenza_corrente,"+&
	"flag_categoria_regis,"+&
	"cod_centro_costo,"+&
	"flag_nc,"+&
	"flag_provenienza_nc"+&
	" "+&
"from non_conformita "+&
"where non_conformita.cod_azienda='"+s_cs_xx.cod_azienda+"' "+&
	"and non_conformita.anno_non_conf="+string(il_anno)+" "+&
	"and non_conformita.num_non_conf="+string(il_num)
				
ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)
IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore dei dati della NC:" + ls_errore)
   RETURN -1
END IF

fds_nc = create datastore
fds_nc.Create( ls_sintassi, ls_errore)
IF Len(ls_errore) > 0 THEN
	g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore dei dati della NC:" + ls_errore)
	destroy fds_nc
	RETURN -1
END IF

fds_nc.settransobject(sqlca)
ll_righe = fds_nc.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve dei dati della NC:" + sqlca.sqlerrtext)
	destroy fds_nc;
	return -1
end if

return ll_righe
end function

public function integer wf_ds_campi (ref datastore fds_campi, string fs_cod_tipo_causa, long fl_num_sequenza_corrente);string ls_cod_tipo_lista_dist, ls_flag_gestione_fasi, ls_cod_lista_dist, ls_des_fase, ls_cod_destinatario
string ls_sql, ls_sintassi, ls_errore
datastore lds_campi
long ll_righe, ll_sequenza_destinatari, ll_num_sequenza_corrente
integer li_index
string ls_colonna, ls_flag_modificabile, ls_flag_responsabile

ll_num_sequenza_corrente = fl_num_sequenza_corrente
if isnull(ll_num_sequenza_corrente) then ll_num_sequenza_corrente = 0

select cod_tipo_lista_dist,
		 cod_lista_dist
into   :ls_cod_tipo_lista_dist,
		 :ls_cod_lista_dist
from   tab_tipi_cause 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_causa = :fs_cod_tipo_causa;
	 
select flag_gestione_fasi
into   :ls_flag_gestione_fasi
from   tab_tipi_liste_dist
where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
		 		 
select stringa
into   :ls_cod_destinatario
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'PCU';

//l'utente collegato è un responsabile?
//se no allora tutti i campi devono essere protetti
//se CS_SYSTEM allora tutti i campi sprotetti
//se responsabile allora dipende dal flag_modificabile

//select num_sequenza
//into   :ll_sequenza_destinatari
//from   det_liste_dist
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
//		 cod_lista_dist = :ls_cod_lista_dist and
//		 num_sequenza_prec = :ll_num_sequenza_corrente;
ll_sequenza_destinatari = ll_num_sequenza_corrente

ls_sql = &
"select colonna,etichetta,ordine "+&
"from det_liste_dist_campi_nc "+&
"where cod_azienda='"+s_cs_xx.cod_azienda+"' "+&
	"and cod_tipo_lista_dist='"+ls_cod_tipo_lista_dist+"' "+&
	"and cod_lista_dist='"+ls_cod_lista_dist+"' "+&
	"and cod_destinatario='"+ls_cod_destinatario+"' "+&
	"and flag_modificabile = 'S' "+&
	"and num_sequenza="+string(ll_sequenza_destinatari) + " "+&
"order by ordine, colonna"

ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)
IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore dei campi:" + ls_errore)
   RETURN -1
END IF

fds_campi = create datastore
fds_campi.Create( ls_sintassi, ls_errore)
IF Len(ls_errore) > 0 THEN
	g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore dei campi:" + ls_errore)
	destroy fds_campi
	RETURN -1
END IF

fds_campi.settransobject(sqlca)
ll_righe = fds_campi.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve dei campi della NC:" + sqlca.sqlerrtext)
	destroy fds_campi;
	return -1
end if

return ll_righe
end function

public function integer wf_responsabile (string fs_cod_tipo_causa, long fl_num_sequenza_corrente, long fl_anno_non_conf, long fl_num_non_conf, ref string fs_cod_responsabile, ref string fs_firma_responsabile, ref datetime fdt_data_firma);string ls_cod_tipo_lista_dist, ls_flag_gestione_fasi, ls_cod_lista_dist, ls_des_fase, ls_cod_destinatario
string ls_sql, ls_sintassi, ls_errore, ls_count, ls_column_name, ls_col_index, ls_protect, ls_ret
long ll_count, ll_num_sequenza_corrente
string ls_colonna, ls_flag_modificabile, ls_flag_responsabile, ls_cod_utente, ls_firma_resp
datetime ldt_data_firma, ldt_ora_firma
string ls_cod_area_aziendale

ll_num_sequenza_corrente = fl_num_sequenza_corrente
if isnull(ll_num_sequenza_corrente) then ll_num_sequenza_corrente = 0

select cod_tipo_lista_dist,
		 cod_lista_dist
into   :ls_cod_tipo_lista_dist,
		 :ls_cod_lista_dist
from   tab_tipi_cause 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_causa = :fs_cod_tipo_causa;
	 
select flag_gestione_fasi
into   :ls_flag_gestione_fasi
from   tab_tipi_liste_dist
where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
		 
if ls_flag_gestione_fasi <> "S" then return 0

select stringa
into   :ls_cod_destinatario
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'PCU';

select cod_area_aziendale
into :ls_cod_area_aziendale
from non_conformita
where cod_azienda=:s_cs_xx.cod_azienda
	and anno_non_conf=:il_anno and num_non_conf=:il_num
;
if ls_cod_area_aziendale = "" then setnull(ls_cod_area_aziendale)

if isnull(ls_cod_area_aziendale) then
	
	select tab_ind_dest.cod_utente,
			mansionari.nome
	into :fs_cod_responsabile,
		:fs_firma_responsabile
	from det_liste_dist_destinatari
	join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
		and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
	join mansionari on mansionari.cod_azienda=det_liste_dist_destinatari.cod_azienda
		and mansionari.cod_utente=tab_ind_dest.cod_utente
	where det_liste_dist_destinatari.cod_azienda=:s_cs_xx.cod_azienda
		and det_liste_dist_destinatari.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
		and det_liste_dist_destinatari.cod_lista_dist=:ls_cod_lista_dist
		and det_liste_dist_destinatari.cod_destinatario=:ls_cod_destinatario
		and det_liste_dist_destinatari.num_sequenza=:ll_num_sequenza_corrente
		and det_liste_dist_destinatari.flag_responsabile = 'S'
		and det_liste_dist_destinatari.cod_area_aziendale is null;
	
	/*
	declare cu_no_area cursor for
	select tab_ind_dest.cod_utente,
			mansionari.nome
//	into :fs_cod_responsabile,
//		:fs_firma_responsabile
	from det_liste_dist_destinatari
	join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
		and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
	join mansionari on mansionari.cod_azienda=det_liste_dist_destinatari.cod_azienda
		and mansionari.cod_utente=tab_ind_dest.cod_utente
	where det_liste_dist_destinatari.cod_azienda=:s_cs_xx.cod_azienda
		and det_liste_dist_destinatari.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
		and det_liste_dist_destinatari.cod_lista_dist=:ls_cod_lista_dist
		and det_liste_dist_destinatari.cod_destinatario=:ls_cod_destinatario
		and det_liste_dist_destinatari.num_sequenza=:ll_num_sequenza_corrente
		and det_liste_dist_destinatari.flag_responsabile = 'S'
		and det_liste_dist_destinatari.cod_area_aziendale is null;
	
	open cu_no_area;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in OPEN del cursore cu_no_area.~r~n"+sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	do while true	
	
		fetch cu_no_area into :fs_cod_responsabile, 
									 :fs_firma_responsabile;
		
		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in FETCH del cursore cu_no_area.~r~n"+sqlca.sqlerrtext)
			close cu_no_area;
			rollback;
			return -1
		end if
		
		//ha trovato il primo
		
	loop
	close cu_no_area;
	*/
else
	select tab_ind_dest.cod_utente,
			mansionari.nome
	into :fs_cod_responsabile,
		:fs_firma_responsabile
	from det_liste_dist_destinatari
	join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
		and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
	join mansionari on mansionari.cod_azienda=det_liste_dist_destinatari.cod_azienda
		and mansionari.cod_utente=tab_ind_dest.cod_utente
	where det_liste_dist_destinatari.cod_azienda=:s_cs_xx.cod_azienda
		and det_liste_dist_destinatari.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
		and det_liste_dist_destinatari.cod_lista_dist=:ls_cod_lista_dist
		and det_liste_dist_destinatari.cod_destinatario=:ls_cod_destinatario
		and det_liste_dist_destinatari.num_sequenza=:ll_num_sequenza_corrente
		and det_liste_dist_destinatari.flag_responsabile = 'S'
		and det_liste_dist_destinatari.cod_area_aziendale=:ls_cod_area_aziendale;
	
	//Donato 07/01/2009 se non hai trovato niente per area aziendale, vuol dire
	//che non è stata specificata. Allora prova a vedere se esiste qualcosa senza area
	//(N.B. deve essere unico altrimenti sarà generato un errore dal programma)
	if sqlca.sqlcode = 100 then
		select tab_ind_dest.cod_utente,
				mansionari.nome
		into :fs_cod_responsabile,
			:fs_firma_responsabile
		from det_liste_dist_destinatari
		join tab_ind_dest on tab_ind_dest.cod_azienda=det_liste_dist_destinatari.cod_azienda
			and tab_ind_dest.cod_destinatario=det_liste_dist_destinatari.cod_destinatario_mail
		join mansionari on mansionari.cod_azienda=det_liste_dist_destinatari.cod_azienda
			and mansionari.cod_utente=tab_ind_dest.cod_utente
		where det_liste_dist_destinatari.cod_azienda=:s_cs_xx.cod_azienda
			and det_liste_dist_destinatari.cod_tipo_lista_dist=:ls_cod_tipo_lista_dist
			and det_liste_dist_destinatari.cod_lista_dist=:ls_cod_lista_dist
			and det_liste_dist_destinatari.cod_destinatario=:ls_cod_destinatario
			and det_liste_dist_destinatari.num_sequenza=:ll_num_sequenza_corrente
			and det_liste_dist_destinatari.flag_responsabile = 'S'
			and det_liste_dist_destinatari.cod_area_aziendale is null;
	end if
	
end if

if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore durante la lettura del responsabile")
	return -1
end if

select distinct cod_utente,
		data_invio//,
		//ora_invio
into :ls_cod_utente,
		:ldt_data_firma//,
		//:ldt_ora_firma
from det_liste_dist_fasi_com
where cod_azienda=:s_cs_xx.cod_azienda
	and anno_reg_intervento is null and num_reg_intervento is null
	and anno_non_conf=:fl_anno_non_conf and num_non_conf=:fl_num_non_conf
	and anno_non_conf is not null and num_non_conf is not null
	and cod_tipo_lista_dist=:ls_cod_tipo_lista_dist and cod_lista_dist=:ls_cod_lista_dist
	and num_sequenza=:ll_num_sequenza_corrente
	and progressivo is null and cod_utente=:fs_cod_responsabile
;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore durante la lettura della presenza del responsabile")
	return -1
end if

if isnull(ls_cod_utente) then ls_cod_utente = ""

if sqlca.sqlcode = 100 and ls_cod_utente = "" then return 0

if sqlca.sqlcode = 0 and ls_cod_utente <> "" then
	fdt_data_firma = datetime(date(ldt_data_firma),time(00:00:00))//time(ldt_ora_firma))
	return 1
else
	return 0
end if


end function

public function string wf_etichetta (string fs_colonna);string ls_return
datastore lds_dw

//cerca nella d_non_conformita_std_1
lds_dw = create datastore
lds_dw.dataobject = "d_non_conformita_std_1"
lds_dw.settransobject(sqlca)
ls_return = lds_dw.describe(fs_colonna+"_t.text")
if isnull(ls_return) then ls_return = ""
if ls_return = "!" or ls_return = "?" then ls_return = ""

if ls_return <> "" then	
	return ls_return
end if
destroy lds_dw
//------------------------------------------------

//cerca nella d_non_conformita_std_2
lds_dw = create datastore
lds_dw.dataobject = "d_non_conformita_std_2"
lds_dw.settransobject(sqlca)
ls_return = lds_dw.describe(fs_colonna+"_t.text")
if isnull(ls_return) then ls_return = ""
if ls_return = "!" or ls_return = "?" then ls_return = ""

if ls_return <> "" then	
	return ls_return
end if
destroy lds_dw
//------------------------------------------------

//cerca nella d_non_conformita_std_3
lds_dw = create datastore
lds_dw.dataobject = "d_non_conformita_std_3"
lds_dw.settransobject(sqlca)
ls_return = lds_dw.describe(fs_colonna+"_t.text")
if isnull(ls_return) then ls_return = ""
if ls_return = "!" or ls_return = "?" then ls_return = ""

if ls_return <> "" then	
	return ls_return
end if
destroy lds_dw
//------------------------------------------------

//cerca nella d_non_conformita_std_4
lds_dw = create datastore
lds_dw.dataobject = "d_non_conformita_std_4"
lds_dw.settransobject(sqlca)
ls_return = lds_dw.describe(fs_colonna+"_t.text")
if isnull(ls_return) then ls_return = ""
if ls_return = "!" or ls_return = "?" then ls_return = ""

if ls_return <> "" then	
	return ls_return
end if
destroy lds_dw
//------------------------------------------------

//cerca nella d_non_conformita_std_1
lds_dw = create datastore
lds_dw.dataobject = "dw_non_conformita_lista"
lds_dw.settransobject(sqlca)
ls_return = lds_dw.describe(fs_colonna+"_t.text")
if isnull(ls_return) then ls_return = ""
if ls_return = "!" or ls_return = "?" then ls_return = ""

if ls_return <> "" then	
	return ls_return
end if
destroy lds_dw

return ls_return
end function

public function integer wf_report ();string ls_sql, ls_sintassi, ls_errore, ls_count, ls_column_name, ls_col_index, ls_column_type
datastore lds_fasi, lds_nc, lds_campi
long ll_row_fasi, ll_row_nc, ll_row_campi
long ll_i_fasi, ll_i_nc, ll_i_campi, ll_riga, ll_i_fasi_old
string ls_cod_tipo_causa, ls_colonna, ls_firma_resp, ls_cod_responsabile, ls_etichetta, ls_val_stringa
long ll_num_sequenza_corrente
datetime ldt_data_firma
long ll_num_sequenza_prec
string ls_fase_old, ls_fase_new

setpointer(HourGlass!)
dw_report_nc.setredraw(false)
dw_report_nc.reset()

/*
    det_liste_dist.des_fase
	det_liste_dist_destinatari.cod_destinatario_mail
	det_liste_dist.num_sequenza
	non_conformita.num_sequenza_corrente
	non_conformita.firma_chiusura
	non_conformita.data_chiusura
	det_liste_dist_destinatari.cod_destinatario_mail
	mansionari.nome
*/
ll_row_fasi = wf_ds_fasi(lds_fasi)
if ll_row_fasi <= 0 then return -1
//------------------------------------------------------------------

//tutti i valori della NC
ll_row_nc = wf_ds_nc(lds_nc)
if ll_row_nc <= 0 then return -1

//numero di colonne nel ds delle NC
ls_count = lds_nc.Describe("DataWindow.Column.Count")
//------------------------------------------------------------------

ll_i_fasi_old = 0
ls_fase_old = ""
ls_fase_new = ""
for ll_i_fasi = 1 to ll_row_fasi
	//ciclo tutte le fasi interessate dalla NC
	//ls_fase_new = lds_fasi.getitemstring(ll_i_fasi,"det_liste_dist_des_fase")
	ls_fase_new = lds_fasi.getitemstring(ll_i_fasi, 1)
	
	if ls_fase_new = ls_fase_old then continue
	
	//leggo la sequenza della fase e il tipo causa della NC
	//ll_num_sequenza_corrente = lds_fasi.getitemnumber(ll_i_fasi, "det_liste_dist_num_sequenza")
	ll_num_sequenza_corrente = lds_fasi.getitemnumber(ll_i_fasi, 3)
	
	ls_cod_tipo_causa = lds_nc.getitemstring(1,"cod_tipo_causa")
	
	//ll_num_sequenza_prec = lds_fasi.getitemnumber(ll_i_fasi, "det_liste_dist_num_sequenza_prec")
	ll_num_sequenza_prec = lds_fasi.getitemnumber(ll_i_fasi, 9)
	
	if isnull(ll_num_sequenza_prec) then ll_num_sequenza_prec = 0
	
	//retrieve delle colonne abilitate per questa fase
	ll_row_campi = wf_ds_campi(lds_campi,ls_cod_tipo_causa, ll_num_sequenza_corrente)
	
	integer li_ret
	string ls_filter
	
	ls_filter = "colonna<>'anno_non_conf' and colonna<>'num_non_conf' and "+&
									"colonna<>'data_creazione' and colonna<>'data_scadenza' and " +&
									"colonna<>'flag_nc' and colonna<>'flag_provenienza_nc'"
	li_ret = lds_campi.setfilter(ls_filter)
	li_ret = lds_campi.filter()
	ll_row_campi = lds_campi.rowcount()
	
	//scrivo la fase corrente
	ll_riga = dw_report_nc.insertrow(0)
	dw_report_nc.setitem(ll_riga, "etichetta_fase", "Fase:")
	
	//dw_report_nc.setitem(ll_riga, "des_fase", lds_fasi.getitemstring(ll_i_fasi,"det_liste_dist_des_fase"))
	dw_report_nc.setitem(ll_riga, "des_fase", lds_fasi.getitemstring(ll_i_fasi, 1))
	
	//MEMORIZZO LA FASE CORRENTE
	//ls_fase_old = lds_fasi.getitemstring(ll_i_fasi,"det_liste_dist_des_fase")
	ls_fase_old = lds_fasi.getitemstring(ll_i_fasi, 1)
	
	//inserisci testata ----------------
	dw_report_nc.setitem(ll_riga, "anno_non_conf",lds_nc.getitemnumber(1,"anno_non_conf"))
	dw_report_nc.setitem(ll_riga, "num_non_conf",lds_nc.getitemnumber(1,"num_non_conf"))
	dw_report_nc.setitem(ll_riga, "data_creazione",lds_nc.getitemdatetime(1,"data_creazione"))
	dw_report_nc.setitem(ll_riga, "data_scadenza",lds_nc.getitemdatetime(1,"data_scadenza"))
	dw_report_nc.setitem(ll_riga, "flag_nc",lds_nc.getitemstring(1,"flag_nc"))
	dw_report_nc.setitem(ll_riga, "flag_provenienza_nc",lds_nc.getitemstring(1,"flag_provenienza_nc"))
	//--------------------------------------
	
	for ll_i_campi=1 to ll_row_campi
		//ciclo tutte le colonne abilitate per la fase, già ordinate
		ls_colonna = lds_campi.getitemstring(ll_i_campi, "colonna")		
		
		//leggi la etichetta da una delle 4 d_non_conformita_std_1,2,3,4
		ls_etichetta = ls_colonna + ":"
		
		//leggo il tipo della colonna (rif. è la dw_non_conformita_lista)
		ls_column_type= lds_nc.Describe(ls_colonna+".colType")
		
		//inserisci la riga
		ll_riga = dw_report_nc.insertrow(0)
		
		//inserisci testata ----------------
		dw_report_nc.setitem(ll_riga, "anno_non_conf",lds_nc.getitemnumber(1,"anno_non_conf"))
		dw_report_nc.setitem(ll_riga, "num_non_conf",lds_nc.getitemnumber(1,"num_non_conf"))
		dw_report_nc.setitem(ll_riga, "data_creazione",lds_nc.getitemdatetime(1,"data_creazione"))
		dw_report_nc.setitem(ll_riga, "data_scadenza",lds_nc.getitemdatetime(1,"data_scadenza"))
		dw_report_nc.setitem(ll_riga, "flag_nc",lds_nc.getitemstring(1,"flag_nc"))
		dw_report_nc.setitem(ll_riga, "flag_provenienza_nc",lds_nc.getitemstring(1,"flag_provenienza_nc"))
		//--------------------------------------		
		
		//inserire etichetta
		ls_etichetta = wf_etichetta(ls_colonna)
		if ls_etichetta = "" then
			dw_report_nc.setitem(ll_riga, "etichetta", ls_colonna + ":")
		else
			dw_report_nc.setitem(ll_riga, "etichetta", ls_etichetta)
		end if		
		//-------------------------------------------------
		
		//inserire campo
		choose case lower(left(ls_column_type,5))
			case "char("
				ls_val_stringa = lds_nc.getitemstring(1, ls_colonna)
				//verifica se deve accodare una descrizione oppure deve visualizzare il valore dei flag
				ls_val_stringa = wf_des_tabella_campi_flag(ls_colonna, ls_val_stringa)
				dw_report_nc.setitem(ll_riga, "campo_string", ls_val_stringa)
				
			case "numbe", "long"
				dw_report_nc.setitem(ll_riga, "campo_integer", lds_nc.getitemnumber(1, ls_colonna))
				
			case "decim"
				if  lower(left(ls_column_type,9)) = "decimal(0" then
					dw_report_nc.setitem(ll_riga, "campo_integer", lds_nc.getitemdecimal(1, ls_colonna))
				else
					dw_report_nc.setitem(ll_riga, "campo_decimal", lds_nc.getitemdecimal(1, ls_colonna))
				end if			
				
			case "date"
				//dw_report_nc.setitem(ll_riga, "campo_string", lds_nc.getitemdate(1, ls_colonna))
				
			case "datet"
				dw_report_nc.setitem(ll_riga, "campo_datetime", lds_nc.getitemdateTime(1, ls_colonna))
				
			case "time", "times"
				//dw_report_nc.setitem(ll_riga, "campo_string", lds_nc.getitemtime(1, ls_colonna))
				
			case else
				
		end choose	
	next
	
	//verifica se devi inserire la firma e l'ora o se la NC è chiusa
	if ll_num_sequenza_prec=0 then
		//siamo nella prima fase: firma e data sono il resp. iter NC e la data creazione
		//inserisci la riga
		ll_riga = dw_report_nc.insertrow(0)
		//inserisci testata ----------------
		dw_report_nc.setitem(ll_riga, "anno_non_conf",lds_nc.getitemnumber(1,"anno_non_conf"))
		dw_report_nc.setitem(ll_riga, "num_non_conf",lds_nc.getitemnumber(1,"num_non_conf"))
		dw_report_nc.setitem(ll_riga, "data_creazione",lds_nc.getitemdatetime(1,"data_creazione"))
		dw_report_nc.setitem(ll_riga, "data_scadenza",lds_nc.getitemdatetime(1,"data_scadenza"))
		dw_report_nc.setitem(ll_riga, "flag_nc",lds_nc.getitemstring(1,"flag_nc"))
		dw_report_nc.setitem(ll_riga, "flag_provenienza_nc",lds_nc.getitemstring(1,"flag_provenienza_nc"))
		//--------------------------------------
		//inserisci la firma
		dw_report_nc.setitem(ll_riga, "etichetta_resp", "Responsabile Iter NC:")
		//.....
		
		//ls_firma_resp = lds_fasi.getitemstring(ll_i_fasi, "mansionari_nome")
		ls_firma_resp = lds_fasi.getitemstring(ll_i_fasi, 8)
		
		dw_report_nc.setitem(ll_riga, "campo_firma", ls_firma_resp)
		
		ll_riga = dw_report_nc.insertrow(0)
		//inserisci testata ----------------
		dw_report_nc.setitem(ll_riga, "anno_non_conf",lds_nc.getitemnumber(1,"anno_non_conf"))
		dw_report_nc.setitem(ll_riga, "num_non_conf",lds_nc.getitemnumber(1,"num_non_conf"))
		dw_report_nc.setitem(ll_riga, "data_creazione",lds_nc.getitemdatetime(1,"data_creazione"))
		dw_report_nc.setitem(ll_riga, "data_scadenza",lds_nc.getitemdatetime(1,"data_scadenza"))
		dw_report_nc.setitem(ll_riga, "flag_nc",lds_nc.getitemstring(1,"flag_nc"))
		dw_report_nc.setitem(ll_riga, "flag_provenienza_nc",lds_nc.getitemstring(1,"flag_provenienza_nc"))
		//--------------------------------------
		//inserisci la data della firma
		dw_report_nc.setitem(ll_riga, "etichetta_resp", "Data NC:")
		
		//ldt_data_firma = lds_fasi.getitemdatetime(ll_i_fasi, "non_conformita_data_chiusura")
		ldt_data_firma = lds_fasi.getitemdatetime(ll_i_fasi, 6)
		
		dw_report_nc.setitem(ll_riga, "data_firma", ldt_data_firma)
		
	elseif wf_responsabile(ls_cod_tipo_causa, ll_num_sequenza_corrente, il_anno, il_num, ls_firma_resp, ls_cod_responsabile, ldt_data_firma) = 1 then
		//inserisci la riga
		ll_riga = dw_report_nc.insertrow(0)
		//inserisci testata ----------------
		dw_report_nc.setitem(ll_riga, "anno_non_conf",lds_nc.getitemnumber(1,"anno_non_conf"))
		dw_report_nc.setitem(ll_riga, "num_non_conf",lds_nc.getitemnumber(1,"num_non_conf"))
		dw_report_nc.setitem(ll_riga, "data_creazione",lds_nc.getitemdatetime(1,"data_creazione"))
		dw_report_nc.setitem(ll_riga, "data_scadenza",lds_nc.getitemdatetime(1,"data_scadenza"))
		dw_report_nc.setitem(ll_riga, "flag_nc",lds_nc.getitemstring(1,"flag_nc"))
		dw_report_nc.setitem(ll_riga, "flag_provenienza_nc",lds_nc.getitemstring(1,"flag_provenienza_nc"))
		//--------------------------------------
		//inserisci la firma
		dw_report_nc.setitem(ll_riga, "etichetta_resp", "Firma Responsabile:")
		//.....
		ls_val_stringa = wf_des_tabella_campi_flag("altra_firma", ls_firma_resp)
		dw_report_nc.setitem(ll_riga, "campo_firma", ls_val_stringa)
		//dw_report_nc.setitem(ll_riga, "campo_firma", ls_firma_resp)
		
		ll_riga = dw_report_nc.insertrow(0)
		//inserisci testata ----------------
		dw_report_nc.setitem(ll_riga, "anno_non_conf",lds_nc.getitemnumber(1,"anno_non_conf"))
		dw_report_nc.setitem(ll_riga, "num_non_conf",lds_nc.getitemnumber(1,"num_non_conf"))
		dw_report_nc.setitem(ll_riga, "data_creazione",lds_nc.getitemdatetime(1,"data_creazione"))
		dw_report_nc.setitem(ll_riga, "data_scadenza",lds_nc.getitemdatetime(1,"data_scadenza"))
		dw_report_nc.setitem(ll_riga, "flag_nc",lds_nc.getitemstring(1,"flag_nc"))
		dw_report_nc.setitem(ll_riga, "flag_provenienza_nc",lds_nc.getitemstring(1,"flag_provenienza_nc"))
		//--------------------------------------
		//inserisci la data della firma
		dw_report_nc.setitem(ll_riga, "etichetta_resp", "Data Fine Fase:")
		dw_report_nc.setitem(ll_riga, "data_firma", ldt_data_firma)
	else		
		//esiste la firma di chiusura?
		ls_firma_resp = lds_nc.getitemstring(1,"firma_chiusura")
		ldt_data_firma = lds_nc.getitemdatetime(1,"data_chiusura")
		
		if not isnull(ls_firma_resp) and ls_firma_resp <> "" &
				and not isnull(ldt_data_firma) then
			//firma chiusura
			ll_riga = dw_report_nc.insertrow(0)
			//inserisci testata ----------------
			dw_report_nc.setitem(ll_riga, "anno_non_conf",lds_nc.getitemnumber(1,"anno_non_conf"))
			dw_report_nc.setitem(ll_riga, "num_non_conf",lds_nc.getitemnumber(1,"num_non_conf"))
			dw_report_nc.setitem(ll_riga, "data_creazione",lds_nc.getitemdatetime(1,"data_creazione"))
			dw_report_nc.setitem(ll_riga, "data_scadenza",lds_nc.getitemdatetime(1,"data_scadenza"))
			dw_report_nc.setitem(ll_riga, "flag_nc",lds_nc.getitemstring(1,"flag_nc"))
			dw_report_nc.setitem(ll_riga, "flag_provenienza_nc",lds_nc.getitemstring(1,"flag_provenienza_nc"))
			//--------------------------------------
			//inserisci la firma
			dw_report_nc.setitem(ll_riga, "etichetta_resp", "Firma Responsabile:")
			//.....
			ls_val_stringa = wf_des_tabella_campi_flag("firma_chiusura", ls_firma_resp)
			dw_report_nc.setitem(ll_riga, "campo_firma", ls_val_stringa)
			//dw_report_nc.setitem(ll_riga, "campo_firma", ls_firma_resp)
			
			ll_riga = dw_report_nc.insertrow(0)
			//inserisci testata ----------------
			dw_report_nc.setitem(ll_riga, "anno_non_conf",lds_nc.getitemnumber(1,"anno_non_conf"))
			dw_report_nc.setitem(ll_riga, "num_non_conf",lds_nc.getitemnumber(1,"num_non_conf"))
			dw_report_nc.setitem(ll_riga, "data_creazione",lds_nc.getitemdatetime(1,"data_creazione"))
			dw_report_nc.setitem(ll_riga, "data_scadenza",lds_nc.getitemdatetime(1,"data_scadenza"))
			dw_report_nc.setitem(ll_riga, "flag_nc",lds_nc.getitemstring(1,"flag_nc"))
			dw_report_nc.setitem(ll_riga, "flag_provenienza_nc",lds_nc.getitemstring(1,"flag_provenienza_nc"))
			//--------------------------------------
			//inserisci la data della firma
			dw_report_nc.setitem(ll_riga, "etichetta_resp", "Data Chiusura:")
			dw_report_nc.setitem(ll_riga, "data_firma", ldt_data_firma)
		else
			//niente
			//firma chiusura
			ll_riga = dw_report_nc.insertrow(0)
			//inserisci testata ----------------
			dw_report_nc.setitem(ll_riga, "anno_non_conf",lds_nc.getitemnumber(1,"anno_non_conf"))
			dw_report_nc.setitem(ll_riga, "num_non_conf",lds_nc.getitemnumber(1,"num_non_conf"))
			dw_report_nc.setitem(ll_riga, "data_creazione",lds_nc.getitemdatetime(1,"data_creazione"))
			dw_report_nc.setitem(ll_riga, "data_scadenza",lds_nc.getitemdatetime(1,"data_scadenza"))
			dw_report_nc.setitem(ll_riga, "flag_nc",lds_nc.getitemstring(1,"flag_nc"))
			dw_report_nc.setitem(ll_riga, "flag_provenienza_nc",lds_nc.getitemstring(1,"flag_provenienza_nc"))
			//--------------------------------------
			//inserisci la firma
			dw_report_nc.setitem(ll_riga, "etichetta_resp", "Firma Responsabile:")
			dw_report_nc.setitem(ll_riga, "campo_firma", "")//......
			
			ll_riga = dw_report_nc.insertrow(0)
			//inserisci testata ----------------
			dw_report_nc.setitem(ll_riga, "anno_non_conf",lds_nc.getitemnumber(1,"anno_non_conf"))
			dw_report_nc.setitem(ll_riga, "num_non_conf",lds_nc.getitemnumber(1,"num_non_conf"))
			dw_report_nc.setitem(ll_riga, "data_creazione",lds_nc.getitemdatetime(1,"data_creazione"))
			dw_report_nc.setitem(ll_riga, "data_scadenza",lds_nc.getitemdatetime(1,"data_scadenza"))
			dw_report_nc.setitem(ll_riga, "flag_nc",lds_nc.getitemstring(1,"flag_nc"))
			dw_report_nc.setitem(ll_riga, "flag_provenienza_nc",lds_nc.getitemstring(1,"flag_provenienza_nc"))
			//--------------------------------------
			//inserisci la data della firma
			dw_report_nc.setitem(ll_riga, "etichetta_resp", "Data Fine Fase:")
			//dw_report_nc.setitem(ll_riga, "data_firma", ldt_data_firma)
		end if
	end if
	
	//a fine fase lascia una riga prima di iniziare la nuova fase: ma solo se non sei all'ultima
	if ll_i_fasi = ll_row_fasi then
	else
		ll_riga = dw_report_nc.insertrow(0)
	end if	
next

dw_report_nc.groupcalc()
dw_report_nc.sort()
dw_report_nc.setredraw(true)
dw_report_nc.Object.DataWindow.Print.Preview = 'Yes'
setpointer(Arrow!)

return 1
end function

public function string wf_des_tabella_campi_flag (string fs_colonna, string fs_valore);string ls_descrizione

if isnull(fs_valore) or fs_valore = "" then return ""

choose case fs_colonna
	//campi descrizione----------------------
	case "cod_operaio"
		ls_descrizione = fs_valore + " - " + f_des_tabella("anag_operai", "cod_operaio='"+fs_valore+"'","cognome + ' ' + nome")
		
	case "cod_tipo_causa"
		ls_descrizione = fs_valore + " - " + f_des_tabella("tab_tipi_cause", "cod_tipo_causa='"+fs_valore+"'","des_tipo_causa")
		
	case "cod_causa"
		ls_descrizione = fs_valore + " - " + f_des_tabella("cause", "cod_causa='"+fs_valore+"'","des_causa")
		
	case "cod_errore"
		ls_descrizione = fs_valore + " - " + f_des_tabella("tab_difformita", "cod_errore='"+fs_valore+"'","des_difformita")
		
	case "cod_trattamento"
		ls_descrizione = fs_valore + " - " + f_des_tabella("tab_trattamenti", "cod_trattamento='"+fs_valore+"'","des_trattamento")
		
	case "tipo_prodotto"
		ls_descrizione = fs_valore + " - " + f_des_tabella("tab_tipi_prodotto", "tipo_prodotto='"+fs_valore+"'","desc_prodotto")
	
	case "cod_misura"
		ls_descrizione = fs_valore + " - " + f_des_tabella("tab_misure", "cod_misura='"+fs_valore+"'","des_misura")
		
	case "cod_fornitore"
		ls_descrizione = fs_valore + " - " + f_des_tabella("anag_fornitori", "cod_fornitore='"+fs_valore+"'","rag_soc_1")
		
	case "cod_divisione"
		ls_descrizione = fs_valore + " - " + f_des_tabella("anag_divisioni", "cod_divisione='"+fs_valore+"'","des_divisione")
		
	case "cod_area_aziendale"
		ls_descrizione = fs_valore + " - " + f_des_tabella("tab_aree_aziendali", "cod_area_aziendale='"+fs_valore+"'","des_area")
	
	case "cod_centro_costo"
		ls_descrizione = fs_valore + " - " + f_des_tabella("tab_centri_costo", "cod_centro_costo='"+fs_valore+"'","des_centro_costo")
	
	//campi flag---------------------------
	case "flag_tipo_nc"
		choose case fs_valore
			case "A"
				ls_descrizione = "Approvigionamento" 
			case "V"
				ls_descrizione = "Vendita" 
			case "I"
				ls_descrizione = "Produzione" 
			case "S"
				ls_descrizione = "Sistema" 
			case "P"
				ls_descrizione = "Progettazione" 
		end choose
	
	case "flag_stato_nc"
		choose case fs_valore
			case "C"
				ls_descrizione = "Controllata" 
			case "I"
				ls_descrizione = "OmniaNet" 
			case "O"
				ls_descrizione = "Omnia" 
			case else
				ls_descrizione = "non specificato" 
		end choose
	
	case "flag_prod_codificato", "flag_eseguito_ac", "flag_azione_correttiva"
		if fs_valore = "S" then ls_descrizione = "Si" else ls_descrizione = "No"
		
	case "flag_nc"
		choose case fs_valore
			case "N"
				ls_descrizione = "Non Conformità" 
			case "O"
				ls_descrizione = "Osservazione" 
			case "F"
				ls_descrizione = "Fuori Specifica" 
			case else
				ls_descrizione = "non specificato" 
		end choose
		
	case "flag_provenienza_nc"
		choose case fs_valore
			case "Q"
				ls_descrizione = "Qualità" 
			case "A"
				ls_descrizione = "Ambiente" 
			case "S"
				ls_descrizione = "Sicurezza" 
			case else
				ls_descrizione = "non specificato" 
		end choose
		
	case "livello_impatto"
		choose case fs_valore
			case "B"
				ls_descrizione = "Bassa" 
			case "M"
				ls_descrizione = "Media" 
			case "A"
				ls_descrizione = "Alta"
			case "T"
				ls_descrizione = "Altissima" 				
			case else
				ls_descrizione = "non specificato" 
		end choose
		
	case "livello_gravita"
		choose case fs_valore
			case "B"
				ls_descrizione = "Bassa" 
			case "M"
				ls_descrizione = "Media" 
			case "A"
				ls_descrizione = "Alta" 
			case "T"
				ls_descrizione = "Altissima" 
		end choose
	
	//per le firme
	case "firma_chiusura"
			ls_descrizione = f_des_tabella("mansionari", "cod_resp_divisione='"+fs_valore+"'","nome")
			
	case "altra_firma"
			ls_descrizione = f_des_tabella("mansionari", "cod_utente='"+fs_valore+"'","nome")
	
	case else
		ls_descrizione = fs_valore
		
end choose

return ls_descrizione
end function

on w_report_nc2.create
int iCurrent
call super::create
this.dw_report_nc=create dw_report_nc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_nc
end on

on w_report_nc2.destroy
call super::destroy
destroy(this.dw_report_nc)
end on

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

dw_report_nc.ib_dw_report = true

set_w_options(c_noresizewin)

save_on_close(c_socnosave)

dw_report_nc.set_dw_options(sqlca, &
									pcca.null_object, &
									c_nonew + &
									c_nomodify + &
									c_nodelete + &
									c_noenablenewonopen + &
									c_noenablemodifyonopen + &
									c_scrollparent + &
									c_disablecc, &
									c_noresizedw + &
									c_nohighlightselected + &
									c_nocursorrowfocusrect + &
									c_nocursorrowpointer)
									
il_anno = s_cs_xx.parametri.parametro_d_1

il_num = s_cs_xx.parametri.parametro_d_2


select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOA in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report_nc.modify(ls_modify)
end event

type dw_report_nc from uo_cs_xx_dw within w_report_nc2
integer x = 23
integer y = 20
integer width = 3749
integer height = 2240
integer taborder = 10
string dataobject = "d_report_nc3"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

