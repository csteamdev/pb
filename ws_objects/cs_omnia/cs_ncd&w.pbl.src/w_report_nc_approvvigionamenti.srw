﻿$PBExportHeader$w_report_nc_approvvigionamenti.srw
$PBExportComments$Finestra Gestione Report Non Conformità Gestione Approvvigionamenti
forward
global type w_report_nc_approvvigionamenti from w_report_non_conformita
end type
end forward

global type w_report_nc_approvvigionamenti from w_report_non_conformita
string title = "Report NC Approvvigionamenti"
end type
global w_report_nc_approvvigionamenti w_report_nc_approvvigionamenti

event open;call super::open;wf_init('A')

end event

on w_report_nc_approvvigionamenti.create
call super::create
end on

on w_report_nc_approvvigionamenti.destroy
call super::destroy
end on

type cb_annulla from w_report_non_conformita`cb_annulla within w_report_nc_approvvigionamenti
end type

type cb_report from w_report_non_conformita`cb_report within w_report_nc_approvvigionamenti
end type

type cb_selezione from w_report_non_conformita`cb_selezione within w_report_nc_approvvigionamenti
end type

type dw_sel_report_nc from w_report_non_conformita`dw_sel_report_nc within w_report_nc_approvvigionamenti
end type

type dw_report_nc from w_report_non_conformita`dw_report_nc within w_report_nc_approvvigionamenti
string dataobject = "d_report_nc_approvv"
end type

