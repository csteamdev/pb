﻿$PBExportHeader$w_report_nc_reclami.srw
forward
global type w_report_nc_reclami from w_cs_xx_principale
end type
type dw_report_nc from uo_cs_xx_dw within w_report_nc_reclami
end type
end forward

global type w_report_nc_reclami from w_cs_xx_principale
integer width = 3840
integer height = 2380
string title = "Report Gestione Reclami"
dw_report_nc dw_report_nc
end type
global w_report_nc_reclami w_report_nc_reclami

type variables
long il_anno, il_num
end variables

on w_report_nc_reclami.create
int iCurrent
call super::create
this.dw_report_nc=create dw_report_nc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_nc
end on

on w_report_nc_reclami.destroy
call super::destroy
destroy(this.dw_report_nc)
end on

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

dw_report_nc.ib_dw_report = true

set_w_options(c_noresizewin)

save_on_close(c_socnosave)

dw_report_nc.set_dw_options(sqlca, &
									pcca.null_object, &
									c_nonew + &
									c_nomodify + &
									c_nodelete + &
									c_noenablenewonopen + &
									c_noenablemodifyonopen + &
									c_scrollparent + &
									c_disablecc, &
									c_noresizedw + &
									c_nohighlightselected + &
									c_nocursorrowfocusrect + &
									c_nocursorrowpointer)
									
il_anno = s_cs_xx.parametri.parametro_d_1

il_num = s_cs_xx.parametri.parametro_d_2


select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOA in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report_nc.modify(ls_modify)
end event

type dw_report_nc from uo_cs_xx_dw within w_report_nc_reclami
integer x = 23
integer y = 20
integer width = 3749
integer height = 2240
integer taborder = 10
string dataobject = "d_report_nc_reclami"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_return

ll_return = retrieve(s_cs_xx.cod_azienda,il_anno,il_num)

if ll_return < 0 then
	g_mb.messagebox("OMNIA","Errore nella retrieve di dw_report_nc")
	return -1
end if

dw_report_nc.Object.DataWindow.Print.Preview = 'Yes'
end event

