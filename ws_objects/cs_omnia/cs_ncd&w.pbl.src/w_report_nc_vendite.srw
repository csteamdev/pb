﻿$PBExportHeader$w_report_nc_vendite.srw
$PBExportComments$Finestra Gestione Report Non Conformità Gestione Vendite
forward
global type w_report_nc_vendite from w_report_non_conformita
end type
end forward

global type w_report_nc_vendite from w_report_non_conformita
string title = "Report NC Vendite"
end type
global w_report_nc_vendite w_report_nc_vendite

event open;call super::open;wf_init('V')

end event

on w_report_nc_vendite.create
call super::create
end on

on w_report_nc_vendite.destroy
call super::destroy
end on

type cb_annulla from w_report_non_conformita`cb_annulla within w_report_nc_vendite
end type

type cb_report from w_report_non_conformita`cb_report within w_report_nc_vendite
end type

type cb_selezione from w_report_non_conformita`cb_selezione within w_report_nc_vendite
end type

type dw_sel_report_nc from w_report_non_conformita`dw_sel_report_nc within w_report_nc_vendite
end type

type dw_report_nc from w_report_non_conformita`dw_report_nc within w_report_nc_vendite
string dataobject = "d_report_nc_vendite"
end type

