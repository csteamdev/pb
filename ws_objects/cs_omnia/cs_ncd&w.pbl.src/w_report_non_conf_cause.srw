﻿$PBExportHeader$w_report_non_conf_cause.srw
$PBExportComments$Finestra Report non conformità per cause
forward
global type w_report_non_conf_cause from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_non_conf_cause
end type
type cb_report from commandbutton within w_report_non_conf_cause
end type
type cb_selezione from commandbutton within w_report_non_conf_cause
end type
type dw_selezione from uo_cs_xx_dw within w_report_non_conf_cause
end type
type dw_report_nc_cause from uo_cs_xx_dw within w_report_non_conf_cause
end type
type dw_report_reclami_cause from uo_cs_xx_dw within w_report_non_conf_cause
end type
end forward

global type w_report_non_conf_cause from w_cs_xx_principale
integer width = 3630
integer height = 1660
string title = "Stampa Non Conformità / Reclami per cause"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report_nc_cause dw_report_nc_cause
dw_report_reclami_cause dw_report_reclami_cause
end type
global w_report_non_conf_cause w_report_non_conf_cause

event pc_setwindow;call super::pc_setwindow;dw_report_nc_cause.ib_dw_report = true
dw_report_reclami_cause.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_nc_cause.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_report_reclami_cause.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
iuo_dw_main = dw_report_reclami_cause

this.x = 741
this.y = 885
this.width = 2509
this.height = 928

end event

on w_report_non_conf_cause.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report_nc_cause=create dw_report_nc_cause
this.dw_report_reclami_cause=create dw_report_reclami_cause
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report_nc_cause
this.Control[iCurrent+6]=this.dw_report_reclami_cause
end on

on w_report_non_conf_cause.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report_nc_cause)
destroy(this.dw_report_reclami_cause)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event resize;call super::resize;if dw_report_nc_cause.visible or  dw_report_reclami_cause.visible then
	cb_selezione.x = newwidth - 400
	cb_selezione.y = newheight - 100
	dw_report_nc_cause.width = newwidth - 50
	dw_report_nc_cause.height = newheight - 130
	
	dw_report_reclami_cause.width = newwidth - 50
	dw_report_reclami_cause.height = newheight - 130
end if

//if dw_report_reclami_cause.visible then
//	cb_selezione.x = newwidth - 400
//	cb_selezione.y = newheight - 100
//	dw_report_reclami_cause.width = newwidth - 50
//	dw_report_reclami_cause.height = newheight - 130
//end if

end event

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type cb_annulla from commandbutton within w_report_non_conf_cause
integer x = 1527
integer y = 688
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_non_conf_cause
integer x = 1915
integer y = 688
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_flag_tipo_rilevazione, ls_cod_cliente, ls_cod_tipo_causa, ls_cod_causa, &
		 ls_sql, ls_flag_tipo_nc, ls_cod_operaio, ls_cod_tipo_causa_cur, ls_cod_causa_cur, &
		 ls_cod_cliente_cur, ls_des_operatore, ls_cod_area_aziendale, ls_cod_divisione, &
		 ls_des_tipo_causa, ls_des_causa, ls_titolo, ls_des_area, ls_des_divisione, &
		 ls_flag_multiaziendale, ls_cod_azienda, ls_des_azienda, ls_selezione, ls_cod_fornitore, &
		 ls_cod_fornitore_cur, ls_des_cli_for
datetime ldt_da_data, ldt_a_data, ldt_data_creazione_cur, ldt_data_chiamata
long ll_anno_non_conf, ll_num_non_conf, ll_i, ll_anno_reg_intervento,  ll_num_reg_intervento
decimal {4}	 ld_costo_nc


dw_selezione.accepttext()
dw_report_nc_cause.reset()
dw_report_reclami_cause.reset()

ls_flag_tipo_rilevazione = dw_selezione.getitemstring(1,"flag_tipo_rilevazione")
ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
ls_cod_fornitore = dw_selezione.getitemstring(1,"cod_fornitore")
ls_cod_tipo_causa = dw_selezione.getitemstring(1,"cod_tipo_causa")
ls_cod_causa = dw_selezione.getitemstring(1,"cod_causa")
ldt_da_data  = dw_selezione.getitemdatetime(1,"data_registrazione_da")
ldt_a_data= dw_selezione.getitemdatetime(1,"data_registrazione_a")
ls_flag_multiaziendale = dw_selezione.getitemstring(1,"flag_multiaziendale")
ls_flag_tipo_nc = dw_selezione.getitemstring(1,"flag_tipo_nc")
ls_cod_divisione = dw_selezione.getitemstring(1,"cod_divisione")

if isnull(ls_flag_tipo_rilevazione) then ls_flag_tipo_rilevazione = "N"

if ls_flag_tipo_rilevazione = "N" then
	ls_selezione = "Rilevazione delle non conformità "
else
	ls_selezione = "Rilevazione dei reclami "
end if	

if isnull(ldt_da_data) then
	ls_selezione += "  Dalla prima data "
else
	ls_selezione += "  Dalla data:" + string(ldt_da_data,"dd/mm/yyyy")
end if

if isnull(ldt_a_data) then
	ls_selezione += "  All'ultima data "
else
	ls_selezione += "  Alla data:" + string(ldt_a_data,"dd/mm/yyyy")
end if

ls_selezione += "~r~n"

if not isnull(ls_cod_cliente) then
	select rag_soc_1
	into :ls_des_cli_for
	from  anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :ls_cod_cliente;

	ls_selezione += "  Cliente:" + ls_des_cli_for
end if

if not isnull(ls_cod_tipo_causa) then
	select des_tipo_causa
	into   :ls_des_tipo_causa
	from   tab_tipi_cause
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_causa = :ls_cod_tipo_causa;
	ls_selezione += "  Tipo Causa:" + ls_des_tipo_causa
end if

if not isnull(ls_cod_causa) then
	select des_causa
	into :ls_des_causa
	from  cause
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_causa = :ls_cod_causa;
	ls_selezione += "  Causa:" + ls_des_causa
end if



choose case ls_flag_tipo_rilevazione 
	case "N"						// Non Conformità

		if isnull(ldt_da_data) then  ldt_da_data  = datetime(date("01/01/1900"))
		if isnull(ldt_a_data)  then  ldt_a_data= datetime(date("31/12/2999"))

		ls_sql = "SELECT cod_azienda, " + &
		"     anno_non_conf, " + &
		"		num_non_conf, " + &
		"		flag_tipo_nc, " + &  
		"		cod_operaio, " + &
		"		data_creazione, " + &
		"		cod_tipo_causa, " + &
		"		cod_causa, " + &
		"		cod_cliente, " + &
		"		cod_fornitore, " + &
		"		cod_area_aziendale, " + &
		"		cod_divisione, " + &
		"		costo_nc       " + &	
		" FROM non_conformita  " + &
		"WHERE " 
		
		if ls_flag_multiaziendale = "N" then
			ls_sql += " cod_azienda = '" + s_cs_xx.cod_azienda + "' and "
		end if
		
		ls_sql += " data_creazione >= '" + string(ldt_da_data, s_cs_xx.db_funzioni.formato_data) + "' " + &
		          " and data_creazione <= '" + string(ldt_a_data, s_cs_xx.db_funzioni.formato_data) + "' "
	
		if (not isnull(ls_cod_tipo_causa)) and ls_cod_tipo_causa<>"" then
			ls_sql = ls_sql + "and cod_tipo_causa ='" + ls_cod_tipo_causa + "' "
		end if
		
		if (not isnull(ls_cod_causa)) and ls_cod_causa<>"" then
			ls_sql = ls_sql + "and cod_causa ='" + ls_cod_causa + "' "
		end if
		
		if (not isnull(ls_cod_cliente)) and ls_cod_cliente<>"" then
			ls_sql = ls_sql + "and cod_cliente ='" + ls_cod_cliente + "' "
		end if
		
		if (not isnull(ls_cod_fornitore)) and ls_cod_fornitore<>"" then
			ls_sql = ls_sql + "and cod_fornitore ='" + ls_cod_fornitore + "' "
		end if
		
		if not isnull(ls_flag_tipo_nc) and ls_flag_tipo_nc <> "X" then
			ls_sql = ls_sql + "and flag_tipo_nc ='" + ls_flag_tipo_nc + "' "
		end if
		
		if (not isnull(ls_cod_divisione)) and ls_cod_divisione<>"" then
			ls_sql = ls_sql + "and cod_divisione ='" + ls_cod_divisione + "' "
		end if
		
		declare cur_nc dynamic cursor for SQLSA ;
		prepare SQLSA from :ls_sql ;
		open dynamic cur_nc ;
		// -- stefanop 16/03/210: ticket 2010/71
		if sqlca.sqlcode < 0 then
			g_mb.error("APICE", "Errore durante l'apertura del cursore.~r~n" + sqlca.sqlerrtext)
			return -1
		end if
		// ----
		
		do while 0=0
			fetch cur_nc into :ls_cod_azienda,
			                  :ll_anno_non_conf,
									:ll_num_non_conf,
									:ls_flag_tipo_nc,
									:ls_cod_operaio,
									:ldt_data_creazione_cur,
									:ls_cod_tipo_causa_cur,
									:ls_cod_causa_cur,
									:ls_cod_cliente_cur,
									:ls_cod_fornitore_cur,
									:ls_cod_area_aziendale,
									:ls_cod_divisione,
									:ld_costo_nc; 
			if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then
				close cur_nc;
				exit
			end if
			
			if sqlca.sqlcode<>0 then
				g_mb.messagebox("Errore lettura dati nel DB ", SQLCA.SQLErrText)
				close cur_nc;
				return
			end if

			ls_des_operatore = ""
			select cognome + ' ' + nome
			into :ls_des_operatore
			from  anag_operai
			where cod_azienda = :ls_cod_azienda and
			      cod_operaio = :ls_cod_operaio;
			
			ls_des_tipo_causa = ""
			select des_tipo_causa
			into :ls_des_tipo_causa
			from  tab_tipi_cause
			where cod_azienda = :ls_cod_azienda and
			      cod_tipo_causa = :ls_cod_tipo_causa_cur;
			
			ls_des_causa = ""
			select des_causa
			into :ls_des_causa
			from  cause
			where cod_azienda = :ls_cod_azienda and
			      cod_causa = :ls_cod_causa_cur;
			
			if not isnull(ls_cod_cliente_cur) and isnull(ls_cod_fornitore_cur) then
				
				ls_des_cli_for = ""
				select rag_soc_1
				into :ls_des_cli_for
				from  anag_clienti
				where cod_azienda = :ls_cod_azienda and
						cod_cliente = :ls_cod_cliente_cur;
						
			elseif isnull(ls_cod_cliente_cur) and not isnull(ls_cod_fornitore_cur) then
				
				ls_des_cli_for = ""
				select rag_soc_1
				into :ls_des_cli_for
				from  anag_fornitori
				where cod_azienda = :ls_cod_azienda and
						cod_fornitore = :ls_cod_fornitore_cur;
						
			else
				
				setnull(ls_des_cli_for)
						
			end if
			
			ls_des_area = ""
			select des_area
			into :ls_des_area
			from  tab_aree_aziendali
			where cod_azienda = :ls_cod_azienda and
			      cod_area_aziendale = :ls_cod_area_aziendale;
			
			ls_des_divisione = ""
			select des_divisione
			into :ls_des_divisione
			from  anag_divisioni
			where cod_azienda = :ls_cod_azienda and
			      cod_divisione = :ls_cod_divisione;
			
			ls_des_azienda = ""
			select rag_soc_1
			into :ls_des_azienda
			from  aziende
			where cod_azienda = :ls_cod_azienda;

			ll_i = dw_report_nc_cause.insertrow(0)
			dw_report_nc_cause.setitem(ll_i, "cod_azienda", ls_cod_azienda)
			dw_report_nc_cause.setitem(ll_i, "des_azienda", ls_des_azienda)
			dw_report_nc_cause.setitem(ll_i, "anno_registrazione", ll_anno_non_conf)
			dw_report_nc_cause.setitem(ll_i, "num_registrazione", ll_num_non_conf)
			dw_report_nc_cause.setitem(ll_i, "data_registrazione", ldt_data_creazione_cur)
			dw_report_nc_cause.setitem(ll_i, "des_operatore", ls_des_operatore)
			dw_report_nc_cause.setitem(ll_i, "des_tipo_causa", ls_des_tipo_causa)
			dw_report_nc_cause.setitem(ll_i, "des_causa", ls_des_causa)
			dw_report_nc_cause.setitem(ll_i, "des_cli_for", ls_des_cli_for)
			dw_report_nc_cause.setitem(ll_i, "des_area", ls_des_area)
			dw_report_nc_cause.setitem(ll_i, "des_divisione", ls_des_divisione)
			dw_report_nc_cause.setitem(ll_i, "costo_nc", ld_costo_nc)
			dw_report_nc_cause.setitem(ll_i, "flag_tipo_nc", ls_flag_tipo_nc)
			
		loop
		close cur_nc;
		rollback;
		
		iuo_dw_main = dw_report_nc_cause
		
		dw_report_nc_cause.object.selezione.text = ls_selezione
		dw_report_nc_cause.setsort("cod_azienda A, anno_registrazione A, num_registrazione A")
		dw_report_nc_cause.sort()
		dw_report_nc_cause.groupcalc()
		dw_report_nc_cause.show()
		dw_report_nc_cause.change_dw_current()
		dw_report_nc_cause.object.datawindow.print.preview = 'Yes'
		
	case "R"	//		Reclami

		ls_sql = "SELECT cod_azienda, " + &
		   "     anno_reg_intervento, " + &
			"		num_reg_intervento, " + &
			"		data_chiamata, " + &
			"		cod_cliente, " + &
			"		cod_operaio, " + &
			"		cod_tipo_causa, " + &
			"		cod_causa   " + &
			" FROM schede_intervento " + &
			" WHERE "

		if ls_flag_multiaziendale = "N" then
			ls_sql += " cod_azienda = '" + s_cs_xx.cod_azienda + "' " 
		else
			ls_sql += " cod_azienda like '%' " 
		end if 	
		
		if not isnull(ldt_da_data) then	
			ls_sql = ls_sql + " and data_chiamata >= '" + string(ldt_da_data, s_cs_xx.db_funzioni.formato_data) + "' "
		end if
		if not isnull(ldt_a_data) then
			ls_sql = ls_sql + " and data_chiamata <= '" + string(ldt_a_data, s_cs_xx.db_funzioni.formato_data) + "' "
		end if
	
		if (not isnull(ls_cod_tipo_causa)) and ls_cod_tipo_causa<>"" then
			ls_sql = ls_sql + "and cod_tipo_causa ='" + ls_cod_tipo_causa + "' "
		end if
		
		if (not isnull(ls_cod_causa)) and ls_cod_causa<>"" then
			ls_sql = ls_sql + "and cod_causa ='" + ls_cod_causa + "' "
		end if
		if (not isnull(ls_cod_cliente)) and ls_cod_cliente<>"" then
			ls_sql = ls_sql + "and cod_cliente ='" + ls_cod_cliente + "' "
		end if
		
		if not isnull(ls_flag_tipo_nc) and ls_flag_tipo_nc <> "X" then
			ls_sql = ls_sql + "and flag_tipo_nc ='" + ls_flag_tipo_nc + "' "
		end if
		
		declare cur_recl dynamic cursor for SQLSA ;
		prepare SQLSA from :ls_sql ;
		open dynamic cur_recl ;
		// -- stefanop 16/03/210: ticket 2010/71
		if sqlca.sqlcode < 0 then
			g_mb.error("APICE", "Errore durante l'apertura del cursore.~r~n" + sqlca.sqlerrtext)
			return -1
		end if
		// ----
		
		do while 0=0
			fetch cur_recl into :ls_cod_azienda, 
									  :ll_anno_reg_intervento,  
									  :ll_num_reg_intervento,
									  :ldt_data_chiamata,
									  :ls_cod_cliente_cur, 
									  :ls_cod_operaio,
									  :ls_cod_tipo_causa_cur, 
									  :ls_cod_causa_cur ;
			 
			if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then
				close cur_recl;
				exit
			end if
			
			if sqlca.sqlcode<>0 then
				g_mb.messagebox("Errore lettura dati nel DB ", SQLCA.SQLErrText)
				close cur_recl;
				return
			end if
			
			select cognome + ' ' + nome
			into :ls_des_operatore
			from  anag_operai
			where cod_azienda = :ls_cod_azienda and
			      cod_operaio = :ls_cod_operaio;
			
			select des_tipo_causa
			into :ls_des_tipo_causa
			from  tab_tipi_cause
			where cod_azienda = :ls_cod_azienda and
			      cod_tipo_causa = :ls_cod_tipo_causa_cur;

			select des_causa
			into :ls_des_causa
			from  cause
			where cod_azienda = :ls_cod_azienda and
			      cod_causa = :ls_cod_causa_cur;
			
			select rag_soc_1
			into :ls_des_cli_for
			from  anag_clienti
			where cod_azienda = :ls_cod_azienda and
			      cod_cliente = :ls_cod_cliente_cur;
			
			select rag_soc_1
			into :ls_des_azienda
			from  aziende
			where cod_azienda = :ls_cod_azienda;
			
			ll_i = dw_report_reclami_cause.insertrow(0)
			dw_report_reclami_cause.setitem(ll_i, "cod_azienda", ls_cod_azienda)
			dw_report_reclami_cause.setitem(ll_i, "des_azienda", ls_des_azienda)
			dw_report_reclami_cause.setitem(ll_i, "anno_registrazione", ll_anno_reg_intervento)
			dw_report_reclami_cause.setitem(ll_i, "num_registrazione", ll_num_reg_intervento)
			dw_report_reclami_cause.setitem(ll_i, "data_registrazione", ldt_data_chiamata)
			dw_report_reclami_cause.setitem(ll_i, "des_operatore", ls_des_operatore)
			dw_report_reclami_cause.setitem(ll_i, "des_tipo_causa", ls_des_tipo_causa)
			dw_report_reclami_cause.setitem(ll_i, "des_causa", ls_des_causa)
			dw_report_reclami_cause.setitem(ll_i, "des_cliente", ls_des_cli_for)
			
		loop
		close cur_recl;
		rollback;
		
		iuo_dw_main = dw_report_reclami_cause

		dw_report_reclami_cause.object.selezione.text = ls_selezione
		dw_report_reclami_cause.setsort("cod_azienda A, anno_registrazione A, num_registrazione A")
		dw_report_reclami_cause.sort()
		dw_report_reclami_cause.groupcalc()
		dw_report_reclami_cause.show()
		dw_report_reclami_cause.change_dw_current()
		dw_report_reclami_cause.object.datawindow.print.preview = 'Yes'
		
end choose

dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3630
parent.height = 1665

cb_selezione.show()
end event

type cb_selezione from commandbutton within w_report_non_conf_cause
integer x = 3214
integer y = 1464
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

parent.x = 741
parent.y = 885
parent.width = 2409
parent.height = 828

dw_report_nc_cause.hide()
dw_report_reclami_cause.hide()
cb_selezione.hide()

dw_selezione.show()
dw_selezione.object.b_ricerca_cliente.visible=true
dw_selezione.object.b_ricerca_fornitore.visible=true

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_non_conf_cause
integer x = 23
integer y = 20
integer width = 2377
integer height = 660
integer taborder = 20
string dataobject = "d_selezione_non_conf_cause"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_nulla, ls_cod_tipo_causa, ls_cod_divisione
	setnull(ls_nulla)
   choose case i_colname
		case "cod_tipo_causa"
			setitem( i_rownbr, "cod_causa", ls_nulla)
			f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + &
								"' and cod_tipo_causa = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		case "cod_causa"
			select cod_tipo_causa
			into :ls_cod_tipo_causa
			from cause
			where cod_azienda = :s_cs_xx.cod_azienda 
			  and cod_causa =:i_coltext;
			if sqlca.sqlcode = 0 then
				setitem(i_rownbr, "cod_tipo_causa", ls_cod_tipo_causa)
			end if
			 
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
		
	case "b_ricerca_divisione"
		guo_ricerca.uof_ricerca_divisione(dw_selezione,"cod_divisione")
		
end choose
end event

event pcd_new;call super::pcd_new;// stefnaop 26/10/11 : beatrice : imposto in automatico le date
long ll_anno
date ldt_date

ll_anno = f_anno_esercizio();

setitem(1, "data_registrazione_da", date("01/01/" + string(ll_anno)))
setitem(1, "data_registrazione_a", date("31/12/" + string(ll_anno)))

end event

type dw_report_nc_cause from uo_cs_xx_dw within w_report_non_conf_cause
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_nc_cause"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_report_reclami_cause from uo_cs_xx_dw within w_report_non_conf_cause
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 12
string dataobject = "d_report_nc_cause"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

