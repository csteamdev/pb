﻿$PBExportHeader$w_report_non_conformita.srw
$PBExportComments$Finestra Gestione Report Non Conformità (da ereditare)
forward
global type w_report_non_conformita from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_non_conformita
end type
type cb_report from commandbutton within w_report_non_conformita
end type
type cb_selezione from commandbutton within w_report_non_conformita
end type
type dw_sel_report_nc from uo_cs_xx_dw within w_report_non_conformita
end type
type dw_report_nc from uo_cs_xx_dw within w_report_non_conformita
end type
end forward

global type w_report_non_conformita from w_cs_xx_principale
integer width = 3739
integer height = 1884
string title = "Report Non Conformità"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_sel_report_nc dw_sel_report_nc
dw_report_nc dw_report_nc
end type
global w_report_non_conformita w_report_non_conformita

type variables
string is_flag_tipo_nc
end variables

forward prototypes
public subroutine wf_init (string flag_tipo_nc)
public subroutine wf_report ()
end prototypes

public subroutine wf_init (string flag_tipo_nc);is_flag_tipo_nc = flag_tipo_nc
choose case is_flag_tipo_nc
	case "A"	// Approvvigionamenti
		dw_sel_report_nc.Modify("rs_cod_fornitore_t.Visible=True")
		dw_sel_report_nc.Modify("cs_cod_fornitore.Visible=True")
		dw_sel_report_nc.Modify("rs_cod_fornitore.Visible=True")
						
	case "P" // Progettazione
		dw_sel_report_nc.Modify("rn_anno_progetto_t.Visible=True")
		dw_sel_report_nc.Modify("rn_anno_progetto.Visible=True")				
		dw_sel_report_nc.Modify("rs_cod_progetto_t.Visible=True")
		dw_sel_report_nc.Modify("cs_cod_progetto.Visible=True")
		dw_sel_report_nc.Modify("rs_cod_progetto.Visible=True")
		dw_sel_report_nc.Modify("r1.Visible=True")
					
	case "V" // Vendite
		dw_sel_report_nc.Modify("rs_cod_cliente_t.Visible=True")
		dw_sel_report_nc.Modify("cs_cod_cliente.Visible=True")
		dw_sel_report_nc.Modify("rs_cod_cliente.Visible=True")

	case else // Sistema e Interne
		dw_sel_report_nc.Modify("rs_cod_reparto_t.Visible=True")
		dw_sel_report_nc.Modify("cs_cod_reparto.Visible=True")
		dw_sel_report_nc.Modify("rs_cod_reparto.Visible=True")

end choose
end subroutine

public subroutine wf_report ();string   ls_cod_operaio, ls_cod_progetto, ls_cod_prodotto, ls_livello_gravita, ls_livello_impatto, ls_flag_eseguito_ac, &
			ls_cod_cliente, ls_cod_fornitore, ls_cod_reparto, ls_sql, ls_flag_prod_codificato, ls_des_manuale_prod, ls_tipo_prodotto, &
			ls_cod_ubicazione, ls_cod_lotto,ls_flag_comunicare_cli, ls_flag_comunicare_for,ls_flag_comunicare_int,ls_flag_comunicato_cli, &
			ls_flag_comunicato_for, ls_flag_comunicato_int, ls_cod_resp_divisione, ls_cod_lavorazione, &
			ls_cliente_rag_soc_1, ls_cliente_rag_soc_2, ls_operaio_cognome, ls_operaio_nome, ls_des_misura, ls_des_prodotto,&
			ls_des_deposito, ls_fornitore_rag_soc_1, ls_fornitore_rag_soc_2, ls_des_reparto, ls_des_progetto, ls_cognome, &
			ls_nome, ls_cod_misura, ls_cod_deposito
			
datetime ldt_data_creazione_da, ldt_data_creazione_a, ldt_data_scadenza_da, ldt_data_scadenza_a, ldt_data_creazione, &
			ldt_data_scadenza, ldt_data_stock
			
long     ll_anno_progetto, ll_anno_non_conf, ll_num_non_conf, ll_prog_stock, ll_anno_commessa, ll_num_commessa, ll_num_reg_lista, &
         ll_num_ver_lista, ll_num_ediz_lista, ll_num_versione, ll_num_edizione, ll_anno_progetto_tes_progetti, ll_righe
			
decimal  ld_quan_non_conformita, ld_quan_accettabile, ld_quan_recuperata, ld_quan_resa, ld_quan_rottamata, ld_quan_carico, &
			ld_quan_derogata, ld_costo_nc

dw_sel_report_nc.accepttext()

setnull(ls_cod_cliente)
setnull(ldt_data_creazione_da)
setnull(ldt_data_creazione_a)
setnull(ldt_data_scadenza_da)
setnull(ldt_data_scadenza_a)
setnull(ls_cod_prodotto)
setnull(ls_livello_gravita)
setnull(ls_livello_impatto)
setnull(ls_flag_eseguito_ac)
setnull(ls_cod_fornitore)
setnull(ll_anno_progetto)
setnull(ls_cod_progetto)
setnull(ls_cod_cliente)
setnull(ls_cod_reparto)

ls_cod_operaio = dw_sel_report_nc.getitemstring(1,"rs_cod_operaio")
ldt_data_creazione_da = dw_sel_report_nc.getitemdatetime(1,"rd_data_creazione_da")
ldt_data_creazione_a = dw_sel_report_nc.getitemdatetime(1,"rd_data_creazione_a")
ldt_data_scadenza_da = dw_sel_report_nc.getitemdatetime(1,"rd_data_scadenza_da")
ldt_data_scadenza_a = dw_sel_report_nc.getitemdatetime(1,"rd_data_scadenza_a")
ls_cod_prodotto = dw_sel_report_nc.getitemstring(1,"rs_cod_prodotto")
ls_livello_gravita = dw_sel_report_nc.getitemstring(1,"rs_livello_gravita")
ls_livello_impatto = dw_sel_report_nc.getitemstring(1,"rs_livello_impatto")
ls_flag_eseguito_ac = dw_sel_report_nc.getitemstring(1,"rs_flag_eseguito_ac")

if is_flag_tipo_nc = "A" then
	ls_cod_fornitore = dw_sel_report_nc.getitemstring(1,"rs_cod_fornitore")
elseif is_flag_tipo_nc = "P" then
	ll_anno_progetto = dw_sel_report_nc.getitemnumber(1,"rn_anno_progetto")
	ls_cod_progetto = dw_sel_report_nc.getitemstring(1,"rs_cod_progetto")
elseif is_flag_tipo_nc = "V" then
	ls_cod_cliente = dw_sel_report_nc.getitemstring(1,"rs_cod_cliente")
else
	ls_cod_reparto = dw_sel_report_nc.getitemstring(1,"rs_cod_reparto")
end if

ls_sql = " SELECT anno_non_conf, " &
       + "        num_non_conf, " &
		 + "        data_creazione, " &
		 + "        data_scadenza, " &
		 + "        flag_prod_codificato, " &		 
		 + "        des_manuale_prod, " &		 
		 + "        tipo_prodotto, " &		 
		 + "        cod_ubicazione, " &		 
		 + "        cod_lotto, " &		 
 		 + "        data_stock, " &
		 + "        prog_stock, " &		  
		 + "        quan_non_conformita, " &		 
 		 + "        quan_derogata, " &
		 + "        quan_recuperata, " &		  
		 + "        quan_accettabile, " &		 
		 + "        quan_resa, " &		 
		 + "        quan_rottamata, " &		 
		 + "        quan_carico, " &		 
		 + "        flag_comunicare_cli, " &		 
		 + "        flag_comunicare_for, " &
		 + "        flag_comunicare_int, " &
		 + "        flag_comunicato_cli, " &
		 + "        flag_comunicato_for, " &
		 + "        flag_comunicato_int, " &		 
		 + "        livello_gravita, " &
		 + "        livello_impatto, " &
		 + "        costo_nc, " &
		 + "        anno_commessa,    " &
		 + "        num_commessa,   " &
		 + "        cod_resp_divisione,    " &
		 + "        cod_lavorazione,    " &
		 + "        num_reg_lista,    " &
		 + "        num_ver_lista,    " &
		 + "        num_ediz_lista, " &
		 + "        cod_misura, " &
		 + "        cod_progetto, " &
		 + "        cod_deposito, " &		 
		 + "        cod_cliente, " &		 
		 + "        cod_fornitore, " &		 		 
		 + "        cod_operaio, " &
		 + "        cod_reparto, " &
		 + "        cod_prodotto " &		 
		 + " FROM   non_conformita " &
		 + " WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' " &
		 + " AND    flag_tipo_nc = '" + is_flag_tipo_nc + "' " 
		 
if not isnull(ls_cod_operaio) and ls_cod_operaio <> "" then
	ls_sql = ls_sql + "   AND cod_operaio = '" + ls_cod_operaio + "' "
end if
		 
if not isnull(ldt_data_creazione_da) and string(ldt_data_creazione_da) <> "" then
	ls_sql = ls_sql + "   AND data_creazione >= '" + string(ldt_data_creazione_da,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_creazione_a) and string(ldt_data_creazione_a) <> "" then
	ls_sql = ls_sql + "   AND data_creazione <= '" + string(ldt_data_creazione_a,s_cs_xx.db_funzioni.formato_data)+ "' "
end if

if not isnull(ldt_data_scadenza_da) and string(ldt_data_scadenza_da) <> "" then
	ls_sql = ls_sql + "   AND data_scadenza >= '" + string(ldt_data_scadenza_da,s_cs_xx.db_funzioni.formato_data)+ "' "
end if

if not isnull(ldt_data_scadenza_a) and string(ldt_data_scadenza_a) <> "" then
	ls_sql = ls_sql + "   AND data_scadenza <= '" + string(ldt_data_scadenza_a,s_cs_xx.db_funzioni.formato_data)+ "' "
end if

if not isnull(ls_cod_prodotto) and ls_cod_prodotto <> "" then
	ls_sql = ls_sql + "   AND cod_prodotto = '" + ls_cod_prodotto + "' " 
end if

if not isnull(ls_livello_gravita) and ls_livello_gravita <> "" then
	ls_sql = ls_sql + "   AND livello_gravita = '" + ls_livello_gravita + "' "
end if

if not isnull(ls_livello_impatto) and ls_livello_impatto <> "" then
	ls_sql = ls_sql + "   AND livello_impatto = '" + ls_livello_impatto + "' "
end if

if not isnull(ls_flag_eseguito_ac) and ls_flag_eseguito_ac <> "" then
	ls_sql = ls_sql + "   AND flag_eseguito_ac = '" + ls_flag_eseguito_ac + "' "
end if

if not isnull(ls_cod_fornitore) and ls_cod_fornitore <> "" then
	ls_sql = ls_sql + "   AND cod_fornitore = '" + ls_cod_fornitore + "' "
end if

if not isnull(ll_anno_progetto) and string(ll_anno_progetto) <> "" and ll_anno_progetto > 0 then
	ls_sql = ls_sql + "   AND anno_progetto = " + string(ll_anno_progetto)	
end if

if not isnull(ls_cod_progetto) and ls_cod_progetto <> "" then
	ls_sql = ls_sql + "   AND cod_progetto = '" + ls_cod_progetto + "' "
end if

if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
	ls_sql = ls_sql + "   AND cod_cliente = '" + ls_cod_cliente + "' "
end if

if not isnull(ls_cod_reparto) and ls_cod_reparto <> "" then
	ls_sql = ls_sql + "   AND cod_reparto = '" + ls_cod_reparto + "' "
end if

ls_sql = ls_sql + " ORDER BY anno_non_conf ASC, num_non_conf ASC "

DECLARE cu_conformita DYNAMIC CURSOR FOR SQLSA ;

PREPARE SQLSA FROM :ls_sql;

OPEN DYNAMIC cu_conformita;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("OMNIA","Errore nell'apertura del cursore delle non conformità! Dettaglio: " +sqlca.sqlerrtext)
	return 
end if

dw_report_nc.reset()

ll_righe = 0
do while 1 = 1
	fetch cu_conformita into :ll_anno_non_conf,
	                         :ll_num_non_conf,
									 :ldt_data_creazione,
									 :ldt_data_scadenza, 
		        					 :ls_flag_prod_codificato,
		                      :ls_des_manuale_prod,
		 							 :ls_tipo_prodotto, 
		                      :ls_cod_ubicazione, 		 
		                      :ls_cod_lotto, 	 
 		                      :ldt_data_stock, 
		                      :ll_prog_stock, 		  
		                      :ld_quan_non_conformita, 		 
 		                      :ld_quan_derogata, 
		                      :ld_quan_recuperata, 		  
		                      :ld_quan_accettabile, 		 
		                      :ld_quan_resa, 	 
		                      :ld_quan_rottamata, 		 
		                      :ld_quan_carico, 		 
		                      :ls_flag_comunicare_cli, 		 
		                      :ls_flag_comunicare_for, 
		                      :ls_flag_comunicare_int, 
		                      :ls_flag_comunicato_cli, 
		                      :ls_flag_comunicato_for, 
		                      :ls_flag_comunicato_int, 		 
		                      :ls_livello_gravita, 
		                      :ls_livello_impatto, 
		                      :ld_costo_nc, 
		                      :ll_anno_commessa,    
		                      :ll_num_commessa,  
		                      :ls_cod_resp_divisione,    
		                      :ls_cod_lavorazione,    
		                      :ll_num_reg_lista,    
		                      :ll_num_ver_lista,    
		                      :ll_num_ediz_lista,
		                      :ls_cod_misura, 
									 :ls_cod_progetto,
									 :ls_cod_deposito,
									 :ls_cod_cliente, 
		                      :ls_cod_fornitore,
		                      :ls_cod_operaio,
		                      :ls_cod_reparto,
		                      :ls_cod_prodotto; 
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore nell fetch del cursore delle non conformità! Dettaglio: " + sqlca.sqlerrtext)
		close cu_conformita;
		return
	end if
	
	ll_righe = dw_report_nc.insertrow(0)
	
//	if isnull(ll_righe) then 
//		ll_righe = 0
//	end if
	
	dw_report_nc.setitem(ll_righe,"rn_anno_non_conf",ll_anno_non_conf)
	dw_report_nc.setitem(ll_righe,"rn_num_non_conf",ll_num_non_conf)	
	dw_report_nc.setitem(ll_righe,"rd_data_creazione",ldt_data_creazione)
	dw_report_nc.setitem(ll_righe,"rd_data_scadenza",ldt_data_scadenza)
	dw_report_nc.setitem(ll_righe,"rs_flag_prod_codificato",ls_flag_prod_codificato)
	dw_report_nc.setitem(ll_righe,"rs_des_manuale_prod",ls_des_manuale_prod)
	dw_report_nc.setitem(ll_righe,"rs_tipo_prodotto",ls_tipo_prodotto)
	dw_report_nc.setitem(ll_righe,"rs_cod_ubicazione",ls_cod_ubicazione)
	dw_report_nc.setitem(ll_righe,"rs_cod_lotto",ls_cod_lotto)
	dw_report_nc.setitem(ll_righe,"rd_data_stock",ldt_data_stock)
	dw_report_nc.setitem(ll_righe,"rn_prog_stock",ll_prog_stock)
	dw_report_nc.setitem(ll_righe,"rn_quan_non_conformita",ld_quan_non_conformita)
	dw_report_nc.setitem(ll_righe,"rn_quan_derogata",ld_quan_derogata)
	dw_report_nc.setitem(ll_righe,"rn_quan_recuperata",ld_quan_recuperata)
	dw_report_nc.setitem(ll_righe,"rn_quan_accettabile",ld_quan_accettabile)
	dw_report_nc.setitem(ll_righe,"rn_quan_resa",ld_quan_resa)
	dw_report_nc.setitem(ll_righe,"rn_quan_rottamata",ld_quan_rottamata)
	dw_report_nc.setitem(ll_righe,"rn_quan_carico",ld_quan_carico)
	dw_report_nc.setitem(ll_righe,"rs_flag_comunicare_cli",ls_flag_comunicare_cli)
	dw_report_nc.setitem(ll_righe,"rs_flag_comunicare_for",ls_flag_comunicare_for)
	dw_report_nc.setitem(ll_righe,"rs_flag_comunicare_int",ls_flag_comunicare_int)
	dw_report_nc.setitem(ll_righe,"rs_flag_comunicato_cli",ls_flag_comunicato_cli)
	dw_report_nc.setitem(ll_righe,"rs_flag_comunicato_for",ls_flag_comunicato_for)
	dw_report_nc.setitem(ll_righe,"rs_flag_comunicato_int",ls_flag_comunicato_int)
	dw_report_nc.setitem(ll_righe,"rs_livello_gravita",ls_livello_gravita)
	dw_report_nc.setitem(ll_righe,"rs_livello_impatto",ls_livello_impatto)
	dw_report_nc.setitem(ll_righe,"rn_costo_nc",ld_costo_nc)
	dw_report_nc.setitem(ll_righe,"rn_anno_commessa",ll_anno_commessa)
	dw_report_nc.setitem(ll_righe,"rn_num_commessa",ll_num_commessa)
	dw_report_nc.setitem(ll_righe,"rs_cod_resp_divisione",ls_cod_resp_divisione)
	dw_report_nc.setitem(ll_righe,"rs_cod_lavorazione",ls_cod_lavorazione)
	dw_report_nc.setitem(ll_righe,"rn_num_reg_lista",ll_num_reg_lista)
	dw_report_nc.setitem(ll_righe,"rn_num_ver_lista",ll_num_ver_lista)	
	dw_report_nc.setitem(ll_righe,"rn_num_ediz_lista",ll_num_ediz_lista)		
	
	select cognome, nome
	into   :ls_cognome, :ls_nome
	from   anag_operai
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    cod_operaio = :ls_cod_operaio;
	
	dw_report_nc.setitem(ll_righe,"rs_des_cognome",ls_cognome)
	dw_report_nc.setitem(ll_righe,"rs_des_nome",ls_nome)
	
	setnull(ls_cognome)
	setnull(ls_nome)
	
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    cod_prodotto = :ls_cod_prodotto;	
	
	select des_deposito
	into   :ls_des_deposito
	from   anag_depositi
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    cod_deposito = :ls_cod_deposito;	

	select des_misura
	into   :ls_des_misura
	from   tab_misure
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    cod_misura = :ls_cod_misura;	
	
	dw_report_nc.setitem(ll_righe,"rs_des_misura",ls_des_misura)
	dw_report_nc.setitem(ll_righe,"rs_des_prodotto",ls_des_prodotto)
	dw_report_nc.setitem(ll_righe,"rs_des_deposito",ls_des_deposito)	
	
	setnull(ls_des_misura)
	setnull(ls_des_prodotto)
	setnull(ls_des_deposito)	

	if is_flag_tipo_nc = "A" then
		select rag_soc_1, rag_soc_2
		into   :ls_fornitore_rag_soc_1, :ls_fornitore_rag_soc_2
		from   anag_fornitori 
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_fornitore = :ls_cod_fornitore;
		
		if isnull(ls_fornitore_rag_soc_1) then ls_fornitore_rag_soc_1 = ""
		if isnull(ls_fornitore_rag_soc_2) then ls_fornitore_rag_soc_2 = ""
		
		dw_report_nc.setitem(ll_righe,"rs_des_fornitore_1",ls_fornitore_rag_soc_1)
		dw_report_nc.setitem(ll_righe,"rs_des_fornitore_2",ls_fornitore_rag_soc_2)
		
		setnull(ls_fornitore_rag_soc_1)
		setnull(ls_fornitore_rag_soc_2)
		
		
	elseif is_flag_tipo_nc = "P" then
		
		select anno_progetto, num_versione, num_edizione, des_progetto
		into   :ll_anno_progetto_tes_progetti,:ll_num_versione,:ll_num_edizione,:ls_des_progetto
		from   tes_progetti
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_progetto = :ls_cod_progetto
		and    anno_progetto = :ll_anno_progetto;
		
	   dw_report_nc.setitem(ll_righe,"rn_anno_progetto", ll_anno_progetto_tes_progetti)
	   dw_report_nc.setitem(ll_righe,"rs_des_progetto", ls_des_progetto)		
	   dw_report_nc.setitem(ll_righe,"rn_num_versione", ll_num_versione)
	   dw_report_nc.setitem(ll_righe,"rn_num_edizione", ll_num_edizione)		

		setnull(ll_anno_progetto_tes_progetti)
		setnull(ls_cod_progetto)
		setnull(ll_num_versione)
		setnull(ll_num_edizione)

	elseif is_flag_tipo_nc = "V" then
		
		select rag_soc_1, rag_soc_2
		into   :ls_cliente_rag_soc_1, :ls_cliente_rag_soc_2
		from   anag_clienti 
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_cliente =:ls_cod_cliente;		
		
		if isnull(ls_cliente_rag_soc_1) then ls_cliente_rag_soc_1 = ""
		if isnull(ls_cliente_rag_soc_2) then ls_cliente_rag_soc_2 = ""

		dw_report_nc.setitem(ll_righe,"rs_rag_soc_1",ls_cliente_rag_soc_1)
		dw_report_nc.setitem(ll_righe,"rs_rag_soc_2",ls_cliente_rag_soc_2)

		setnull(ls_cliente_rag_soc_1)
		setnull(ls_cliente_rag_soc_2)

	else
		
		select des_reparto
		into   :ls_des_reparto
		from   anag_reparti
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_reparto =:ls_cod_reparto;				
		
		dw_report_nc.setitem(ll_righe,"rs_des_reparto",ls_des_reparto)
		
		setnull(ls_des_reparto)
		
	end if	
	
	setnull(ll_anno_non_conf)
	setnull(ll_num_non_conf)
	setnull(ldt_data_creazione)
	setnull(ldt_data_scadenza) 
	setnull(ls_flag_prod_codificato)
	setnull(ls_des_manuale_prod)
	setnull(ls_tipo_prodotto) 
	setnull(ls_cod_ubicazione) 		 
	setnull(ls_cod_lotto) 	 
	setnull(ldt_data_stock) 
	setnull(ll_prog_stock) 		  
	setnull(ld_quan_non_conformita) 		 
	setnull(ld_quan_derogata) 
	setnull(ld_quan_recuperata) 		  
	setnull(ld_quan_accettabile) 		 
	setnull(ld_quan_resa) 	 
	setnull(ld_quan_rottamata) 		 
	setnull(ld_quan_carico) 		 
	setnull(ls_flag_comunicare_cli) 		 
	setnull(ls_flag_comunicare_for) 
	setnull(ls_flag_comunicare_int) 
	setnull(ls_flag_comunicato_cli) 
	setnull(ls_flag_comunicato_for) 
	setnull(ls_flag_comunicato_int) 		 
	setnull(ls_livello_gravita) 
	setnull(ls_livello_impatto) 
	setnull(ld_costo_nc) 
	setnull(ll_anno_commessa)    
	setnull(ll_num_commessa)  
	setnull(ls_cod_resp_divisione)    
	setnull(ls_cod_lavorazione)    
	setnull(ll_num_reg_lista)    
	setnull(ll_num_ver_lista)    
	setnull(ll_num_ediz_lista)
	setnull(ls_cod_misura) 
	setnull(ls_cod_progetto)
	setnull(ls_cod_deposito)
	setnull(ls_cod_cliente) 
	setnull(ls_cod_fornitore)
	setnull(ls_cod_operaio)
	setnull(ls_cod_reparto)
	setnull(ls_cod_prodotto) 	
		
loop

CLOSE cu_conformita;
rollback;

dw_report_nc.object.datawindow.print.preview = 'Yes'


end subroutine

event pc_setwindow;call super::pc_setwindow;dw_report_nc.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup )

//dw_report_nc.set_dw_options(sqlca, &
//										pcca.null_object, &
//										c_noretrieveonopen + &
//										c_nomodify + &
//										c_nodelete + &
//										c_disableCC, &
//										c_noresizedw + &
//										c_nohighlightselected + &
//										c_cursorrowpointer + &
//										c_InactiveDWColorUnchanged)

dw_report_nc.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer + &
                         c_nocursorrowfocusrect + &
								 c_InactiveDWColorUnchanged )


dw_sel_report_nc.set_dw_options(sqlca, &
										pcca.null_object, &
										c_nomodify + &
										c_nodelete + &
										c_newonopen + &
										c_disableCC, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_cursorrowpointer)
iuo_dw_main = dw_report_nc

this.x = 741
this.y = 400
this.width = 2300
this.height = 1200

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_sel_report_nc,"rs_cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_sel_report_nc,"rs_cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_sel_report_nc,"rs_cod_progetto",sqlca,&
                 "tes_progetti","cod_progetto","des_progetto", &
                 "tes_progetti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_sel_report_nc,"rs_cod_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1", &
                 "anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_sel_report_nc,"rs_cod_cliente",sqlca,&
                 "anag_clienti","cod_cliente","rag_soc_1",&
                 "anag_clienti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

on w_report_non_conformita.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_sel_report_nc=create dw_sel_report_nc
this.dw_report_nc=create dw_report_nc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_sel_report_nc
this.Control[iCurrent+5]=this.dw_report_nc
end on

on w_report_non_conformita.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_sel_report_nc)
destroy(this.dw_report_nc)
end on

type cb_annulla from commandbutton within w_report_non_conformita
integer x = 1486
integer y = 1000
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_non_conformita
integer x = 1874
integer y = 1000
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;dw_sel_report_nc.hide()

wf_report()

parent.x = 100
parent.y = 50
parent.width = 3739
parent.height = 1884

dw_report_nc.show()
						
cb_selezione.show()
iuo_dw_main = dw_report_nc
dw_report_nc.change_dw_current()
dw_report_nc.event getfocus( )



end event

type cb_selezione from commandbutton within w_report_non_conformita
integer x = 3328
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string   ls_prodotto, ls_cat_mer
datetime ld_da_data, ld_a_data

dw_sel_report_nc.show()

parent.x = 741
parent.y = 400
parent.width = 2300
parent.height = 1200

dw_report_nc.hide()
cb_selezione.hide()

dw_sel_report_nc.change_dw_current()
end event

type dw_sel_report_nc from uo_cs_xx_dw within w_report_non_conformita
integer x = 23
integer y = 20
integer width = 2217
integer height = 960
integer taborder = 20
string dataobject = "d_sel_report_nc"
borderstyle borderstyle = stylelowered!
end type

type dw_report_nc from uo_cs_xx_dw within w_report_non_conformita
boolean visible = false
integer x = 23
integer width = 3680
integer height = 1628
integer taborder = 10
string dataobject = ""
boolean vscrollbar = true
end type

