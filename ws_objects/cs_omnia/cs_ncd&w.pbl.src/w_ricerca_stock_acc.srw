﻿$PBExportHeader$w_ricerca_stock_acc.srw
$PBExportComments$Finestra Ricerca Dati Stock di un Prodotto
forward
global type w_ricerca_stock_acc from w_cs_xx_risposta
end type
type dw_search from uo_cs_xx_dw within w_ricerca_stock_acc
end type
type cb_ok from uo_cb_accept within w_ricerca_stock_acc
end type
type cb_chiudi from uo_cb_close within w_ricerca_stock_acc
end type
type dw_ricerca_stock from uo_dw_main within w_ricerca_stock_acc
end type
type dw_folder from u_folder within w_ricerca_stock_acc
end type
end forward

global type w_ricerca_stock_acc from w_cs_xx_risposta
integer width = 2816
integer height = 1952
string title = "Ricerca Stock"
event ue_imposta_filtro ( )
dw_search dw_search
cb_ok cb_ok
cb_chiudi cb_chiudi
dw_ricerca_stock dw_ricerca_stock
dw_folder dw_folder
end type
global w_ricerca_stock_acc w_ricerca_stock_acc

type variables

end variables

forward prototypes
public function integer wf_retrieve ()
end prototypes

event ue_imposta_filtro();//impostazione filtri, se passati
//cod_prodotto
if not isnull(s_cs_xx.parametri.parametro_s_10) and s_cs_xx.parametri.parametro_s_10 <> ""  then
	dw_search.setitem(1, "cod_prodotto", s_cs_xx.parametri.parametro_s_10)
end if

//cod_deposito
if not isnull(s_cs_xx.parametri.parametro_s_11) and s_cs_xx.parametri.parametro_s_11 <> "" then
	dw_search.setitem(1, "cod_deposito", s_cs_xx.parametri.parametro_s_11)
end if

//cod_ubicazione
if not isnull(s_cs_xx.parametri.parametro_s_12) and s_cs_xx.parametri.parametro_s_12 <> "" then
	dw_search.setitem(1, "cod_ubicazione", s_cs_xx.parametri.parametro_s_12)
end if

//cod_lotto
if not isnull(s_cs_xx.parametri.parametro_s_13) and s_cs_xx.parametri.parametro_s_13 <> "" then
	dw_search.setitem(1, "cod_lotto", s_cs_xx.parametri.parametro_s_13)
end if

//cod_fornitore
if not isnull(s_cs_xx.parametri.parametro_s_9) and s_cs_xx.parametri.parametro_s_9 <> "" then
	dw_search.setitem(1, "cod_fornitore", s_cs_xx.parametri.parametro_s_9)
end if

//data_stock
if year(date(s_cs_xx.parametri.parametro_data_4)) >= 1950 then
	dw_search.setitem(1, "data_stock", s_cs_xx.parametri.parametro_data_4)
end if

end event

public function integer wf_retrieve ();LONG  l_Error
string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_fornitore
datetime ldt_data_stock
long ll_anno_acc_materiali, ll_num_acc_materiali


dw_search.accepttext()

ls_cod_prodotto = dw_search.getitemstring(1, "cod_prodotto")
ls_cod_deposito = dw_search.getitemstring(1, "cod_deposito")
ls_cod_ubicazione = dw_search.getitemstring(1, "cod_ubicazione")
ls_cod_lotto = dw_search.getitemstring(1, "cod_lotto")
ls_cod_fornitore = dw_search.getitemstring(1, "cod_fornitore")
ldt_data_stock = dw_search.getitemdatetime(1, "data_stock")
ll_anno_acc_materiali = dw_search.getitemnumber(1, "anno_acc_materiali")
ll_num_acc_materiali = dw_search.getitemnumber(1, "num_acc_materiali")


//cod_prodotto
if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) < 1 then setnull(ls_cod_prodotto)

//cod_deposito
if isnull(ls_cod_deposito) or len(ls_cod_deposito) < 1 then setnull(ls_cod_deposito)

//cod_ubicazione
if isnull(ls_cod_ubicazione) or len(ls_cod_ubicazione) < 1 then setnull(ls_cod_ubicazione)

//cod_lotto
if isnull(ls_cod_lotto) or len(ls_cod_lotto) < 1 then setnull(ls_cod_lotto)

//cod_fornitore
if isnull(ls_cod_fornitore) or len(ls_cod_fornitore) < 1 then setnull(ls_cod_fornitore)

//data_stock
if year(date(ldt_data_stock)) < 1950 then setnull(ldt_data_stock)

if ll_anno_acc_materiali > 0 then
else
	setnull(ll_anno_acc_materiali)
end if

if ll_num_acc_materiali > 0 then
else
	setnull(ll_num_acc_materiali)
end if

l_Error = dw_ricerca_stock.retrieve(	s_cs_xx.cod_azienda, &
                   	ls_cod_prodotto , &
                   	ls_cod_deposito , &
                   	ls_cod_ubicazione , &
                   	ls_cod_lotto, &
			    			ls_cod_fornitore, &
			    			ldt_data_stock, &
							ll_anno_acc_materiali, &
							ll_num_acc_materiali)

return l_Error

end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

set_w_options(c_closenosave)	// + c_autoposition + c_noresizewin)

dw_search.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_ricerca_stock.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)

l_objects[1] = dw_ricerca_stock
dw_folder.fu_AssignTab(1, "&Ricerca Stock", l_Objects[])

dw_folder.fu_FolderCreate(1,2)
                          // il primo parametri indica quanti fogli dentro al folder
                          // il secondo parametro indica quanti fogli per riga
dw_folder.fu_SelectTab(1)
dw_folder.fu_HideTab(2)


postevent("ue_imposta_filtro")





end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ricerca_stock,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW(dw_search,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on w_ricerca_stock_acc.create
int iCurrent
call super::create
this.dw_search=create dw_search
this.cb_ok=create cb_ok
this.cb_chiudi=create cb_chiudi
this.dw_ricerca_stock=create dw_ricerca_stock
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_search
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.cb_chiudi
this.Control[iCurrent+4]=this.dw_ricerca_stock
this.Control[iCurrent+5]=this.dw_folder
end on

on w_ricerca_stock_acc.destroy
call super::destroy
destroy(this.dw_search)
destroy(this.cb_ok)
destroy(this.cb_chiudi)
destroy(this.dw_ricerca_stock)
destroy(this.dw_folder)
end on

type dw_search from uo_cs_xx_dw within w_ricerca_stock_acc
integer x = 32
integer y = 24
integer width = 2661
integer height = 652
integer taborder = 10
string dataobject = "d_sel_ricerca_stock"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;if row >0 then
	
	choose case dwo.name
			
		case "b_prodotto"
			
			
			
		case "b_fornitore"
			
			
			
		case "b_ricerca"
			wf_retrieve()
			
	end choose
	
end if
end event

type cb_ok from uo_cb_accept within w_ricerca_stock_acc
integer x = 2213
integer y = 1748
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;call super::clicked;datetime ldt_data_stock
long ll_prog_stock

if not isnull(s_cs_xx.parametri.parametro_s_1) then
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_ricerca_stock.getitemstring(dw_ricerca_stock.getrow(),"cod_prodotto"))
end if
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, dw_ricerca_stock.getitemstring(dw_ricerca_stock.getrow(),"cod_deposito"))
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_3)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, dw_ricerca_stock.getitemstring(dw_ricerca_stock.getrow(),"cod_ubicazione"))
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_4)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_4, dw_ricerca_stock.getitemstring(dw_ricerca_stock.getrow(),"cod_lotto"))

ldt_data_stock = dw_ricerca_stock.getitemdatetime(dw_ricerca_stock.getrow(),"data_stock")
//s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_5)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_5, ldt_data_stock)

ll_prog_stock = dw_ricerca_stock.getitemnumber(dw_ricerca_stock.getrow(),"prog_stock")
//s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_6)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_6, ll_prog_stock)

ll_prog_stock = dw_ricerca_stock.getitemnumber(dw_ricerca_stock.getrow(),"quan_assegnata")
if (ll_prog_stock > 0) and (not isnull(s_cs_xx.parametri.parametro_s_7)) and (len(s_cs_xx.parametri.parametro_s_7) > 0) then
   s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_7, ll_prog_stock)
end if

ll_prog_stock = dw_ricerca_stock.getitemnumber(dw_ricerca_stock.getrow(),"costo_medio")
if (ll_prog_stock > 0) and (not isnull(s_cs_xx.parametri.parametro_s_8)) and (len(s_cs_xx.parametri.parametro_s_8) > 0) then
   s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_8, ll_prog_stock)
end if

s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
parent.triggerevent("pc_close")
end event

type cb_chiudi from uo_cb_close within w_ricerca_stock_acc
integer x = 1824
integer y = 1748
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type dw_ricerca_stock from uo_dw_main within w_ricerca_stock_acc
integer x = 41
integer y = 816
integer width = 2656
integer height = 876
integer taborder = 40
string dataobject = "d_ricerca_stock_acc"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;string ls_objecttype, ls_objectname

if i_extendmode then
	if this.getrow() > 0 then		
		
		ls_objectname = dwo.name
		ls_objecttype = this.Describe(ls_objectname+".Type")
		
		if ls_objecttype = "text" then
			//ordinamento...da fare
		else
			//come prima
			cb_ok.triggerevent("Clicked")
		end if
		
		
	end if
end if

end event

type dw_folder from u_folder within w_ricerca_stock_acc
integer x = 23
integer y = 692
integer width = 2725
integer height = 1024
integer taborder = 10
boolean border = false
end type

