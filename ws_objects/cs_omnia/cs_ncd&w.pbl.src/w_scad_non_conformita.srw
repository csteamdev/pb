﻿$PBExportHeader$w_scad_non_conformita.srw
$PBExportComments$Finestra Scadenzario Non Conformità
forward
global type w_scad_non_conformita from w_cs_xx_principale
end type
type dw_filtro_scad_nc from datawindow within w_scad_non_conformita
end type
type dw_scad_non_conformita from uo_cs_xx_dw within w_scad_non_conformita
end type
type cb_filtro from commandbutton within w_scad_non_conformita
end type
type cb_reset from commandbutton within w_scad_non_conformita
end type
end forward

global type w_scad_non_conformita from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3387
integer height = 1700
string title = "Scadenzario Non Conformità"
boolean maxbox = false
boolean resizable = false
dw_filtro_scad_nc dw_filtro_scad_nc
dw_scad_non_conformita dw_scad_non_conformita
cb_filtro cb_filtro
cb_reset cb_reset
end type
global w_scad_non_conformita w_scad_non_conformita

on w_scad_non_conformita.create
int iCurrent
call super::create
this.dw_filtro_scad_nc=create dw_filtro_scad_nc
this.dw_scad_non_conformita=create dw_scad_non_conformita
this.cb_filtro=create cb_filtro
this.cb_reset=create cb_reset
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_filtro_scad_nc
this.Control[iCurrent+2]=this.dw_scad_non_conformita
this.Control[iCurrent+3]=this.cb_filtro
this.Control[iCurrent+4]=this.cb_reset
end on

on w_scad_non_conformita.destroy
call super::destroy
destroy(this.dw_filtro_scad_nc)
destroy(this.dw_scad_non_conformita)
destroy(this.cb_filtro)
destroy(this.cb_reset)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup)

dw_scad_non_conformita.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)

iuo_dw_main = dw_scad_non_conformita

dw_filtro_scad_nc.insertrow(0)

cb_reset.triggerevent("clicked")

cb_filtro.postevent("clicked")
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_scad_non_conformita,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_scad_non_conformita,"cod_errore",sqlca,&
                 "tab_difformita","cod_errore","des_difformita", &
                 "tab_difformita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_scad_non_conformita,"cod_trattamento",sqlca,&
                 "tab_trattamenti","cod_trattamento","des_trattamento", &
                 "tab_trattamenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_filtro_scad_nc,"rs_cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome", &
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_filtro_scad_nc from datawindow within w_scad_non_conformita
integer y = 20
integer width = 2949
integer height = 452
integer taborder = 20
string dataobject = "d_filtro_scad_nc"
boolean border = false
boolean livescroll = true
end type

type dw_scad_non_conformita from uo_cs_xx_dw within w_scad_non_conformita
integer x = 23
integer y = 480
integer width = 3314
integer height = 1100
integer taborder = 20
string dataobject = "d_scad_non_conformita"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_filtro from commandbutton within w_scad_non_conformita
event clicked pbm_bnclicked
integer x = 2971
integer y = 360
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_sql, ls_tipo, ls_chiusura, ls_operaio

long   ll_anno


dw_filtro_scad_nc.accepttext()

ll_anno = dw_filtro_scad_nc.getitemnumber(1,"anno")

ls_tipo = dw_filtro_scad_nc.getitemstring(1,"tipo")

ls_operaio = dw_filtro_scad_nc.getitemstring(1,"operaio")

ls_chiusura = dw_filtro_scad_nc.getitemstring(1,"chiusura")

ls_sql = &
"select cod_azienda, " + &   
"       anno_non_conf, " + &   
"       num_non_conf, " + &   
"       flag_tipo_nc, " + &   
"       cod_operaio, " + &   
"       num_reg_manutenzione, " + &   
"       data_creazione, " + &   
"       data_scadenza, " + &   
"       flag_prod_codificato, " + &   
"       des_manuale_prod, " + &   
"       tipo_prodotto, " + &   
"       cod_prodotto, " + &   
"       cod_deposito, " + &   
"       cod_ubicazione, " + &   
"       cod_lotto, " + &   
"       data_stock, " + &   
"       prog_stock, " + &   
"       cod_misura, " + &   
"       cod_errore, " + &   
"       cod_trattamento, " + &   
"       progr, " + &   
"       quan_non_conformita, " + &   
"       quan_derogata, " + &   
"       quan_recuperata, " + &   
"       quan_accettabile, " + &   
"       quan_resa, " + &   
"       quan_rottamata, " + &   
"       quan_carico, " + &   
"       flag_azione_correttiva, " + &   
"       azione_intrapresa, " + &   
"       cod_fornitore, " + &   
"       cod_prod_fornitore, " + &   
"       anno_registrazione_ord_acq, " + &   
"       num_registrazione_ord_acq, " + &   
"       prog_riga_ordine_acq, " + &   
"       anno_bolla_acq, " + &   
"       num_bolla_acq, " + &   
"       cod_fornitore_bolla_acq, " + &   
"       prog_riga_bolla_acq, " + &   
"       anno_commessa, " + &   
"       num_commessa, " + &   
"       cod_reparto, " + &   
"       cod_lavorazione, " + &   
"       cod_attrezzatura, " + &   
"       anno_reg_collaudi, " + &   
"       num_reg_collaudi, " + &   
"       cod_cliente, " + &   
"       anno_registrazione_ord_ven, " + &   
"       num_registrazione_ord_ven, " + &   
"       prog_riga_ord_ven, " + &   
"       anno_registrazione_bol_ven, " + &   
"       num_registrazione_bol_ven, " + &   
"       prog_riga_bol_ven, " + &   
"       flag_comunicare_cli, " + &   
"       flag_comunicare_for, " + &   
"       flag_comunicare_int, " + &   
"       cod_resp_divisione, " + &   
"       flag_comunicato_cli, " + &   
"       flag_comunicato_for, " + &   
"       flag_comunicato_int, " + &   
"       nome_doc_compilato, " + &   
"       nome_doc_aut_deroga, " + &   
"       azione_immediata, " + &   
"       nota_causa_nc, " + &   
"       nota_ac_preventive, " + &   
"       nota_reclamo, " + &   
"       flag_eseguito_ac, " + &   
"       nota_verifiche_su_ac, " + &   
"       cod_doc_origine, " + &   
"       numeratore_doc_origine, " + &   
"       anno_doc_origine, " + &   
"       num_doc_origine, " + &   
"       prog_riga_fat_acq, " + &   
"       anno_reg_fat_ven, " + &   
"       num_reg_fat_ven, " + &   
"       prog_riga_fat_ven, " + &   
"       anno_reg_off_ven, " + &   
"       num_reg_off_ven, " + &   
"       prog_riga_off_ven, " + &   
"       num_reg_lista, " + &   
"       num_ver_lista, " + &   
"       num_ediz_lista, " + &   
"       anno_progetto, " + &   
"       cod_progetto, " + &   
"       num_ver_progetto, " + &   
"       num_ediz_progetto, " + &   
"       prog_riga_det_fasi, " + &   
"       livello_gravita, " + &   
"       livello_impatto, " + &   
"       cod_tipo_causa, " + &   
"       cod_causa, " + &   
"       cod_area_aziendale, " + &   
"       cod_divisione, " + &   
"       anno_registrazione, " + &   
"       num_registrazione, " + &   
"       progressivo, " + &   
"       firma_chiusura, " + &   
"       data_chiusura " + &
"from   non_conformita " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ll_anno) and ll_anno > 0 then
	ls_sql = ls_sql + " and anno_non_conf = " + string(ll_anno)
end if

if not isnull(ls_tipo) and ls_tipo <> "" then
	ls_sql = ls_sql + " and flag_tipo_nc = '" + string(ls_tipo) + "'"
end if

if not isnull(ls_operaio) and ls_operaio <> "" then
	ls_sql = ls_sql + " and cod_operaio = '" + string(ls_operaio) + "'"
end if

if not isnull(ls_chiusura) and ls_chiusura <> "" then
	choose case ls_chiusura
		case "S"
			ls_sql = ls_sql + " and firma_chiusura is not null"
		case "N"
			ls_sql = ls_sql + " and firma_chiusura is null"
	end choose
end if

ls_sql = ls_sql + " order by anno_non_conf ASC, num_non_conf ASC, flag_tipo_nc ASC"

if dw_scad_non_conformita.setsqlselect(ls_sql) = -1 then
	g_mb.messagebox("OMNIA","Errore nell'impostazione della select di dw_scad_non_conformita")
	return -1
end if

parent.triggerevent("pc_retrieve")
end event

type cb_reset from commandbutton within w_scad_non_conformita
event clicked pbm_bnclicked
integer x = 2971
integer y = 260
integer width = 366
integer height = 80
integer taborder = 22
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;string ls_null


setnull(ls_null)

dw_filtro_scad_nc.setitem(1,"tipo",ls_null)

dw_filtro_scad_nc.setitem(1,"anno",f_anno_esercizio())

dw_filtro_scad_nc.setitem(1,"operaio",ls_null)

dw_filtro_scad_nc.setitem(1,"chiusura","I")
end event

