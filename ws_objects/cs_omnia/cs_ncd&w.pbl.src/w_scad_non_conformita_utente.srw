﻿$PBExportHeader$w_scad_non_conformita_utente.srw
$PBExportComments$Finestra Scadenzario Non Conformità
forward
global type w_scad_non_conformita_utente from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_scad_non_conformita_utente
end type
type dw_filtro_scad_nc from datawindow within w_scad_non_conformita_utente
end type
type cb_filtro from commandbutton within w_scad_non_conformita_utente
end type
type cb_reset from commandbutton within w_scad_non_conformita_utente
end type
end forward

global type w_scad_non_conformita_utente from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3387
integer height = 2172
string title = "Scadenzario Non Conformità"
boolean maxbox = false
boolean resizable = false
dw_report dw_report
dw_filtro_scad_nc dw_filtro_scad_nc
cb_filtro cb_filtro
cb_reset cb_reset
end type
global w_scad_non_conformita_utente w_scad_non_conformita_utente

forward prototypes
public function integer wf_report_stato (long fl_anno, long fl_numero, ref string fs_stato)
public subroutine wf_report ()
end prototypes

public function integer wf_report_stato (long fl_anno, long fl_numero, ref string fs_stato);long      ll_righe, ll_anno, ll_num, ll_stato, ll_count, ll_riga, ll_stato_prec, ll_comunicazioni

string    ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_cod_tipo_causa,ls_flag_gestione_fasi, ls_cod_destinatario, ls_str, ls_cod_area_aziendale, ls_cod_prodotto, &
          ls_flag_tipo_dist_multipla, ls_cod_cat_mer, ls_firma_chiusura
			 
datetime  ldt_data_chiusura			 

ll_anno = fl_anno

ll_num = fl_numero

select num_sequenza_corrente,
		 cod_tipo_causa,
		 cod_area_aziendale,
		 cod_prodotto,
		 firma_chiusura,
		 data_chiusura
into   :ll_stato,
       :ls_cod_tipo_causa,
		 :ls_cod_area_aziendale,
		 :ls_cod_prodotto,
		 :ls_firma_chiusura,
		 :ldt_data_chiusura
from   non_conformita
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_non_conf = :ll_anno and
		 num_non_conf = :ll_num;
		 
if ( not isnull(ls_firma_chiusura) and ls_firma_chiusura <> "" ) or not isnull(ldt_data_chiusura)  then
	fs_stato = "Conclusa"
	return -1
end if	

select cod_tipo_lista_dist,
       cod_lista_dist
into   :ls_cod_tipo_lista_dist,
       :ls_cod_lista_dist
from   tab_tipi_cause
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_tipo_causa = :ls_cod_tipo_causa;
			 
select flag_gestione_fasi
into   :ls_flag_gestione_fasi
from   tab_tipi_liste_dist
where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
	
if ls_flag_gestione_fasi = "S" and not isnull(ls_flag_gestione_fasi) then
	
	ll_stato_prec = ll_stato
	
	select stringa
	into   :ls_cod_destinatario
	from   parametri_omnia 
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'PCU';	
	
	// *** controllo se esiste la fase successiva
	
	if not isnull(ll_stato) and ll_stato > 0 then
	
		select count(*)
		into   :ll_count
		from   det_liste_dist
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
				 cod_lista_dist = :ls_cod_lista_dist and
				 cod_destinatario = :ls_cod_destinatario and
				 num_sequenza_prec = :ll_stato;
			 
		if sqlca.sqlcode <> 0 then
			
			fs_stato = ""
			return -1
			
		end if
	
		if isnull(ll_count) or ll_count < 1 or ll_count > 1 then
			
			fs_stato = ""
			return -1
					
		end if
	
		select num_sequenza
		into   :ll_stato
		from   det_liste_dist
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
				 cod_lista_dist = :ls_cod_lista_dist and
				 cod_destinatario = :ls_cod_destinatario and
				 num_sequenza_prec = :ll_stato;	
				 
	elseif isnull(ll_stato) or ll_stato = 0 then
		
		// se la non conformita non ha stato, allora devo prendere i destinatari della prima fase
		
		select num_sequenza 
		into   :ll_stato 
		from   det_liste_dist
		where  cod_azienda = :s_cs_xx.cod_azienda and
		 	    cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
			    cod_lista_dist = :ls_cod_lista_dist and
			    cod_destinatario = :ls_cod_destinatario and
	          ( num_sequenza_prec is null or num_sequenza_prec = 0 );	
				 
		if sqlca.sqlcode <> 0 then
			fs_stato = ""
			return -1
		end if
		
	end if

	setnull(ll_count)		
			 
//	select count(*)
//	into   :ll_comunicazioni
//	from   det_liste_dist_fasi_com
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 anno_non_conf = :ll_anno and
//			 num_non_conf = :ll_num;	
//			 
//	if isnull(ll_comunicazioni) or ll_comunicazioni = 0 then   //** la fase corrente da cui devo prendere i destinatari è la prima
//	
//		select num_sequenza
//		into   :ll_stato
//		from   det_liste_dist
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//		       cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
//			 	 cod_lista_dist = :ls_cod_lista_dist and
//			 	 cod_destinatario = :ls_cod_destinatario and
//		       ( num_sequenza_prec is null or num_sequenza_prec = 0 );
//
//	end if
			 
	select flag_tipo_dist_multipla
	into   :ls_flag_tipo_dist_multipla
	from   det_liste_dist
	where  cod_azienda = :s_cs_xx.cod_azienda and
		 	 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
			 cod_lista_dist = :ls_cod_lista_dist and
			 cod_destinatario = :ls_cod_destinatario and
			 num_sequenza = :ll_stato;
			 
	if not isnull(ls_flag_tipo_dist_multipla) and ls_flag_tipo_dist_multipla <> "" and ls_flag_tipo_dist_multipla = "A" then
	
		if ls_cod_area_aziendale <> "" and not isnull(ls_cod_area_aziendale) then
			
			select count(*)
			into   :ll_count
			from   det_liste_dist_destinatari
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
					 cod_lista_dist = :ls_cod_lista_dist and
					 cod_destinatario = :ls_cod_destinatario and
					 num_sequenza = :ll_stato and
					 ( cod_area_aziendale = :ls_cod_area_aziendale or cod_area_aziendale is null ) and
					 cod_destinatario_mail in (select cod_destinatario
														from   tab_ind_dest
														where  cod_azienda = :s_cs_xx.cod_azienda and
																 cod_utente = :s_cs_xx.cod_utente);
		else
			
			select count(*)
			into   :ll_count
			from   det_liste_dist_destinatari
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
					 cod_lista_dist = :ls_cod_lista_dist and
					 cod_destinatario = :ls_cod_destinatario and
					 num_sequenza = :ll_stato and
					 cod_destinatario_mail in (select cod_destinatario
														from   tab_ind_dest
														where  cod_azienda = :s_cs_xx.cod_azienda and
																 cod_utente = :s_cs_xx.cod_utente);			
		end if
															 
	elseif not isnull(ls_flag_tipo_dist_multipla) and ls_flag_tipo_dist_multipla <> "" and ls_flag_tipo_dist_multipla = "C" then

		select cod_cat_mer
		into   :ls_cod_cat_mer
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_cat_mer) and ls_cod_cat_mer <> "" then
			
			select count(*)
			into   :ll_count
			from   det_liste_dist_destinatari
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
					 cod_lista_dist = :ls_cod_lista_dist and
					 cod_destinatario = :ls_cod_destinatario and
					 num_sequenza = :ll_stato and
					 ( cod_cat_mer = :ls_cod_cat_mer or cod_cat_mer is null ) and
					 ( cod_area_aziendale = :ls_cod_area_aziendale or cod_area_aziendale is null ) and
					 cod_destinatario_mail in (select cod_destinatario
														from   tab_ind_dest
														where  cod_azienda = :s_cs_xx.cod_azienda and
																 cod_utente = :s_cs_xx.cod_utente);
		else
			
			select count(*)
			into   :ll_count
			from   det_liste_dist_destinatari
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
					 cod_lista_dist = :ls_cod_lista_dist and
					 cod_destinatario = :ls_cod_destinatario and
					 num_sequenza = :ll_stato and
					 cod_destinatario_mail in (select cod_destinatario
														from   tab_ind_dest
														where  cod_azienda = :s_cs_xx.cod_azienda and
																 cod_utente = :s_cs_xx.cod_utente);			
			
		end if
		
	else
		
		select count(*)
		into   :ll_count
		from   det_liste_dist_destinatari
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
				 cod_lista_dist = :ls_cod_lista_dist and
				 cod_destinatario = :ls_cod_destinatario and
				 num_sequenza = :ll_stato and
				 cod_destinatario_mail in (select cod_destinatario
													from   tab_ind_dest
													where  cod_azienda = :s_cs_xx.cod_azienda and
															 cod_utente = :s_cs_xx.cod_utente);				
		
	end if
			 
	if isnull(ll_count) or ll_count < 1 then 
		
		select des_fase
		into   :fs_stato
		from   det_liste_dist
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
				 cod_lista_dist = :ls_cod_lista_dist and
				 cod_destinatario = :ls_cod_destinatario and
				 num_sequenza = :ll_stato_prec;
				 
		return -1
		
	else
		
		select des_fase
		into   :fs_stato
		from   det_liste_dist
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_lista_dist = :ls_cod_tipo_lista_dist and
				 cod_lista_dist = :ls_cod_lista_dist and
				 cod_destinatario = :ls_cod_destinatario and
				 num_sequenza = :ll_stato_prec;
		
		return 0
		
	end if
else
	fs_stato = ""
	return -1
end if
	

end function

public subroutine wf_report ();string ls_chiusura, ls_operaio

datetime ldt_data_da, ldt_data_a, ldt_data_registrazione, ldt_data_chiusura, ldt_data_scadenza

string   ls_cod_cliente, ls_filtro_utente, ls_cod_area_aziendale, ls_tipo, ls_tipo_reso, ls_classe_reso, ls_sql, ls_cod_azienda, &
         ls_flag_reso_cu, ls_flag_tipo_reso_cu, ls_flag_classe_reso_cu, ls_cod_cliente_cu, ls_cod_tipo_causa_cu, ls_cod_causa_cu, ls_cod_causa_reso_cu, &
			ls_cod_resp_reso_cu, ls_flag_criticita_cu, ls_flag_categoria_regis_cu, ls_stato, ls_des_cliente, ls_des_tipo_causa, ls_des_causa, &
			ls_des_causa_reso, ls_des_responsabile, ls_tipo_nc_cu, ls_cod_prodotto_cu, ls_cod_fornitore_cu, ls_firma_chiusura_cu, ls_des_fornitore

long     ll_anno, ll_num, ll_ret, ll_riga

dw_filtro_scad_nc.accepttext()

dw_report.reset()

dw_report.setredraw(false)

setpointer(hourglass!)

ll_anno = dw_filtro_scad_nc.getitemnumber(1,"anno")

ls_tipo = dw_filtro_scad_nc.getitemstring(1,"tipo")

ls_operaio = dw_filtro_scad_nc.getitemstring(1,"operaio")

ls_chiusura = dw_filtro_scad_nc.getitemstring(1,"chiusura")

ls_filtro_utente = dw_filtro_scad_nc.getitemstring( 1, "flag_utente")

ls_sql = &
"select anno_non_conf, " + &   
"       num_non_conf, " + &   
"       flag_tipo_nc, " + &   
"       data_creazione, " + &   
"       data_scadenza, " + &   
"       cod_prodotto, " + &   
"       cod_fornitore, " + &   
"       cod_cliente, " + &   
"       cod_tipo_causa, " + &   
"       cod_causa, " + &   
"       cod_area_aziendale, " + &   
"       livello_impatto, " + &   
"       flag_categoria_regis, " + &   
"       firma_chiusura, " + &   
"       data_chiusura " + &
"from   non_conformita " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ll_anno) and ll_anno > 0 then
	ls_sql = ls_sql + " and anno_non_conf = " + string(ll_anno)
end if

if not isnull(ls_tipo) and ls_tipo <> "" then
	ls_sql = ls_sql + " and flag_tipo_nc = '" + string(ls_tipo) + "'"
end if

if not isnull(ls_operaio) and ls_operaio <> "" then
	ls_sql = ls_sql + " and cod_operaio = '" + string(ls_operaio) + "'"
end if

if not isnull(ls_chiusura) and ls_chiusura <> "" then
	choose case ls_chiusura
		case "S"
			ls_sql = ls_sql + " and firma_chiusura is not null"
		case "N"
			ls_sql = ls_sql + " and firma_chiusura is null"
	end choose
end if

ls_sql = ls_sql + " order by anno_non_conf ASC, num_non_conf ASC, flag_tipo_nc ASC"

DECLARE cu_nc DYNAMIC CURSOR FOR SQLSA ;

PREPARE SQLSA FROM :ls_sql;

OPEN DYNAMIC cu_nc ;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante apertura cursore : " + sqlca.sqlerrtext)
	dw_report.setredraw(true)
	setpointer(arrow!)		
	return 
end if

do while 2 = 2
	
	FETCH cu_nc INTO :ll_anno,   
                    :ll_num,
						  :ls_tipo_nc_cu,
                    :ldt_data_registrazione,
						  :ldt_data_scadenza,
						  :ls_cod_prodotto_cu,
						  :ls_cod_fornitore_cu,
						  :ls_cod_cliente_cu,
                    :ls_cod_tipo_causa_cu,   
                    :ls_cod_causa_cu,   
						  :ls_cod_area_aziendale,						  
                    :ls_flag_criticita_cu,   
                    :ls_flag_categoria_regis_cu,
						  :ls_firma_chiusura_cu,
						  :ldt_data_chiusura;				
	
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante il caricamento del cursore: " + sqlca.sqlerrtext)
		close cu_nc;
		dw_report.setredraw(true)
		setpointer(arrow!)		
		return 
	end if
	
	ls_stato = ""
	
	ll_ret = wf_report_stato( ll_anno, ll_num, ls_stato)
	
	if ll_ret <> 0 and ls_filtro_utente = "S" and not isnull(ls_filtro_utente) then continue
	
	ll_riga = dw_report.insertrow(0)
	
	select rag_soc_1
	into   :ls_des_cliente
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :ls_cod_cliente_cu;
			 
	select rag_soc_1
	into   :ls_des_fornitore
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_fornitore = :ls_cod_fornitore_cu;			 
			 
	select des_tipo_causa
	into   :ls_des_tipo_causa
	from   tab_tipi_cause
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_causa = :ls_cod_tipo_causa_cu;
	
	select des_causa
	into   :ls_des_causa
	from   cause
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_causa = :ls_cod_causa_cu;
			 

	dw_report.setitem( ll_riga, "anno_registrazione", ll_anno)
	dw_report.setitem( ll_riga, "num_registrazione", ll_num)
	dw_report.setitem( ll_riga, "data_registrazione", ldt_data_registrazione)
	dw_report.setitem( ll_riga, "cliente", ls_des_cliente)
	dw_report.setitem( ll_riga, "fornitore", ls_des_fornitore)
	dw_report.setitem( ll_riga, "tipo_causa", ls_des_tipo_causa)
	dw_report.setitem( ll_riga, "causa", ls_des_causa)
	dw_report.setitem( ll_riga, "stato", ls_stato)
	dw_report.setitem( ll_riga, "criticita", ls_flag_criticita_cu)
	dw_report.setitem( ll_riga, "categoria_regis", ls_flag_categoria_regis_cu)		
	dw_report.setitem( ll_riga, "tipo_nc", ls_tipo_nc_cu)
	dw_report.setitem( ll_riga, "data_scadenza", ldt_data_scadenza)
	
loop

CLOSE cu_nc ;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la chiusura del cursore: " + sqlca.sqlerrtext)
end if

dw_report.setredraw(true)

setpointer(arrow!)

end subroutine

on w_scad_non_conformita_utente.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.dw_filtro_scad_nc=create dw_filtro_scad_nc
this.cb_filtro=create cb_filtro
this.cb_reset=create cb_reset
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.dw_filtro_scad_nc
this.Control[iCurrent+3]=this.cb_filtro
this.Control[iCurrent+4]=this.cb_reset
end on

on w_scad_non_conformita_utente.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.dw_filtro_scad_nc)
destroy(this.cb_filtro)
destroy(this.cb_reset)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &						 
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
iuo_dw_main = dw_report

dw_filtro_scad_nc.insertrow(0)

cb_reset.triggerevent("clicked")

cb_filtro.postevent("clicked")



end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_filtro_scad_nc,"rs_cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome", &
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_report from uo_cs_xx_dw within w_scad_non_conformita_utente
integer x = 23
integer y = 480
integer width = 3314
integer height = 1600
integer taborder = 30
string dataobject = "d_scad_nc_utente"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_filtro_scad_nc from datawindow within w_scad_non_conformita_utente
integer y = 20
integer width = 2949
integer height = 452
integer taborder = 20
string dataobject = "d_filtro_scad_nc_utente"
boolean border = false
boolean livescroll = true
end type

type cb_filtro from commandbutton within w_scad_non_conformita_utente
event clicked pbm_bnclicked
integer x = 2971
integer y = 360
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;wf_report()
end event

type cb_reset from commandbutton within w_scad_non_conformita_utente
event clicked pbm_bnclicked
integer x = 2971
integer y = 260
integer width = 366
integer height = 80
integer taborder = 22
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;string ls_null


setnull(ls_null)

dw_filtro_scad_nc.setitem(1,"tipo",ls_null)

dw_filtro_scad_nc.setitem(1,"anno",f_anno_esercizio())

dw_filtro_scad_nc.setitem(1,"operaio",ls_null)

dw_filtro_scad_nc.setitem(1,"chiusura","I")
end event

