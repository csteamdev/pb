﻿$PBExportHeader$w_seleziona_data_chiusura.srw
forward
global type w_seleziona_data_chiusura from window
end type
type cb_non_impostare from commandbutton within w_seleziona_data_chiusura
end type
type cb_esc from commandbutton within w_seleziona_data_chiusura
end type
type st_1 from statictext within w_seleziona_data_chiusura
end type
type cb_conferma from commandbutton within w_seleziona_data_chiusura
end type
type dp_data_chiusura from datepicker within w_seleziona_data_chiusura
end type
end forward

global type w_seleziona_data_chiusura from window
integer width = 1294
integer height = 424
boolean titlebar = true
string title = "Seleziona Data Chiusura"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_non_impostare cb_non_impostare
cb_esc cb_esc
st_1 st_1
cb_conferma cb_conferma
dp_data_chiusura dp_data_chiusura
end type
global w_seleziona_data_chiusura w_seleziona_data_chiusura

type variables
datetime idt_data_selezionata
end variables

on w_seleziona_data_chiusura.create
this.cb_non_impostare=create cb_non_impostare
this.cb_esc=create cb_esc
this.st_1=create st_1
this.cb_conferma=create cb_conferma
this.dp_data_chiusura=create dp_data_chiusura
this.Control[]={this.cb_non_impostare,&
this.cb_esc,&
this.st_1,&
this.cb_conferma,&
this.dp_data_chiusura}
end on

on w_seleziona_data_chiusura.destroy
destroy(this.cb_non_impostare)
destroy(this.cb_esc)
destroy(this.st_1)
destroy(this.cb_conferma)
destroy(this.dp_data_chiusura)
end on

event open;string			ls_label

try
	ls_label = message.stringparm
	if g_str.isnotempty(ls_label) then
		st_1.text = ls_label
		cb_esc.visible = true
		cb_non_impostare.visible = true
	end if
		
catch (runtimeerror e)
end try



//inizializzo con data sistema
dp_data_chiusura.setvalue(today(), now())

dp_data_chiusura.getvalue( idt_data_selezionata)
end event

type cb_non_impostare from commandbutton within w_seleziona_data_chiusura
boolean visible = false
integer x = 837
integer y = 224
integer width = 430
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "non impostare"
end type

event clicked;


if g_mb.confirm("Hai deciso non impostare nessuna data. Confermi?") then
	s_cs_xx.parametri.parametro_b_1 = true
	setnull(s_cs_xx.parametri.parametro_data_4)
	close(parent)

end if
end event

type cb_esc from commandbutton within w_seleziona_data_chiusura
boolean visible = false
integer x = 9
integer y = 220
integer width = 306
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;

if g_mb.confirm("Hai deciso di annullare il processo. Confermi?") then
	s_cs_xx.parametri.parametro_b_1 = false
	close(parent)

end if
end event

type st_1 from statictext within w_seleziona_data_chiusura
integer x = 18
integer y = 28
integer width = 1253
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Seleziona Data Chiusura NC"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within w_seleziona_data_chiusura
integer x = 421
integer y = 220
integer width = 338
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;datetime ldt_return

s_cs_xx.parametri.parametro_b_1 = true
s_cs_xx.parametri.parametro_data_4 = idt_data_selezionata

close(parent)
end event

type dp_data_chiusura from datepicker within w_seleziona_data_chiusura
integer x = 311
integer y = 104
integer width = 544
integer height = 100
integer taborder = 10
boolean border = true
borderstyle borderstyle = stylelowered!
boolean allowedit = true
date maxdate = Date("2999-12-31")
date mindate = Date("1800-01-01")
datetime value = DateTime(Date("2015-03-19"), Time("14:10:52.000000"))
integer textsize = -10
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
integer calendarfontweight = 400
boolean todaysection = true
boolean todaycircle = true
end type

event valuechanged;

idt_data_selezionata = dtm
end event

