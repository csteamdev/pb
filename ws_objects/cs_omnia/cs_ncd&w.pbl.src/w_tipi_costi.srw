﻿$PBExportHeader$w_tipi_costi.srw
forward
global type w_tipi_costi from w_cs_xx_principale
end type
type dw_tipi_costi from uo_cs_xx_dw within w_tipi_costi
end type
end forward

global type w_tipi_costi from w_cs_xx_principale
integer width = 2098
integer height = 1568
string title = "Tipi Costi"
boolean maxbox = false
boolean resizable = false
dw_tipi_costi dw_tipi_costi
end type
global w_tipi_costi w_tipi_costi

on w_tipi_costi.create
int iCurrent
call super::create
this.dw_tipi_costi=create dw_tipi_costi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_costi
end on

on w_tipi_costi.destroy
call super::destroy
destroy(this.dw_tipi_costi)
end on

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_tipi_costi

dw_tipi_costi.change_dw_current()

dw_tipi_costi.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
end event

type dw_tipi_costi from uo_cs_xx_dw within w_tipi_costi
integer x = 23
integer y = 20
integer width = 2034
integer height = 1440
integer taborder = 10
string dataobject = "d_tipi_costi"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
next
end event

