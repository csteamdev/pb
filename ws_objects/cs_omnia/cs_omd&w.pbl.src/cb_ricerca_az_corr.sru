﻿$PBExportHeader$cb_ricerca_az_corr.sru
$PBExportComments$Command Button Clienti Ricerca
forward
global type cb_ricerca_az_corr from commandbutton
end type
end forward

global type cb_ricerca_az_corr from commandbutton
integer width = 73
integer height = 80
integer taborder = 1
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type
global cb_ricerca_az_corr cb_ricerca_az_corr

event clicked;if not isvalid(w_ricerca_az_corr) then
   window_open(w_ricerca_az_corr,0)
end if

w_ricerca_az_corr.show()
end event

on cb_ricerca_az_corr.create
end on

on cb_ricerca_az_corr.destroy
end on

