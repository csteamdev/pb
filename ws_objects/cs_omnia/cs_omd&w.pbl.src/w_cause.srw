﻿$PBExportHeader$w_cause.srw
$PBExportComments$Finestra Cause
forward
global type w_cause from w_cs_xx_principale
end type
type dw_cause_lista from uo_cs_xx_dw within w_cause
end type
type dw_cause_det from uo_cs_xx_dw within w_cause
end type
end forward

global type w_cause from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2345
integer height = 1476
string title = "Tabella Cause"
dw_cause_lista dw_cause_lista
dw_cause_det dw_cause_det
end type
global w_cause w_cause

on w_cause.create
int iCurrent
call super::create
this.dw_cause_lista=create dw_cause_lista
this.dw_cause_det=create dw_cause_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_cause_lista
this.Control[iCurrent+2]=this.dw_cause_det
end on

on w_cause.destroy
call super::destroy
destroy(this.dw_cause_lista)
destroy(this.dw_cause_det)
end on

event pc_setwindow;call super::pc_setwindow;string ls_parametro

dw_cause_lista.set_dw_key("cod_azienda")
dw_cause_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_cause_det.set_dw_options(sqlca, &
                                  dw_cause_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
iuo_dw_main = dw_cause_lista

// **** michela 03/05/2005: per non personalizzare le cose, visualizzo i campi solo se quel parametro esiste ed è a si

select flag
into   :ls_parametro
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CCR';
		 
if sqlca.sqlcode = 0 and not isnull(ls_parametro) and ls_parametro <> "" and ls_parametro = "S" then
	dw_cause_det.modify("flag_criticita.visible = 1")
	dw_cause_det.modify("t_flag_criticita.visible = 1")
   dw_cause_det.modify("flag_categoria_regis.visible = 1") 	
	dw_cause_det.modify("t_flag_categoria_regis.visible = 1")
else
	dw_cause_det.modify("flag_criticita.visible = 0")
	dw_cause_det.modify("t_flag_criticita.visible = 0")
   dw_cause_det.modify("flag_categoria_regis.visible = 0") 
	dw_cause_det.modify("t_flag_categoria_regis.visible = 0")
end if

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_cause_lista, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
							
							
f_PO_LoadDDDW_DW( dw_cause_det, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type dw_cause_lista from uo_cs_xx_dw within w_cause
integer x = 23
integer y = 20
integer width = 2263
integer height = 540
string dataobject = "d_cause_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type dw_cause_det from uo_cs_xx_dw within w_cause
integer x = 23
integer y = 580
integer width = 2263
integer height = 780
integer taborder = 2
string dataobject = "d_cause_det"
end type

