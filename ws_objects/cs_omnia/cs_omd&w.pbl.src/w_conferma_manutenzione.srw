﻿$PBExportHeader$w_conferma_manutenzione.srw
$PBExportComments$Finestra modale per conferma Manutenzioni
forward
global type w_conferma_manutenzione from w_cs_xx_risposta
end type
type dw_selezione from uo_dw_main within w_conferma_manutenzione
end type
type cb_conferma from commandbutton within w_conferma_manutenzione
end type
end forward

global type w_conferma_manutenzione from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 3058
integer height = 660
string title = "Conferma Manutenzioni"
dw_selezione dw_selezione
cb_conferma cb_conferma
end type
global w_conferma_manutenzione w_conferma_manutenzione

type variables
long   il_controllo_conferma = 0, il_anno_ric, il_num_ric, il_anno_nc, il_num_nc

string is_note
end variables

on w_conferma_manutenzione.create
int iCurrent
call super::create
this.dw_selezione=create dw_selezione
this.cb_conferma=create cb_conferma
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione
this.Control[iCurrent+2]=this.cb_conferma
end on

on w_conferma_manutenzione.destroy
call super::destroy
destroy(this.dw_selezione)
destroy(this.cb_conferma)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
il_anno_ric = s_cs_xx.parametri.parametro_d_1

il_num_ric = s_cs_xx.parametri.parametro_d_2

il_anno_nc = s_cs_xx.parametri.parametro_d_3

il_num_nc = s_cs_xx.parametri.parametro_d_4

is_note = s_cs_xx.parametri.parametro_s_4
end event

event pc_setddlb;call super::pc_setddlb;string ls_cod_attrezzatura

f_PO_LoadDDDW_DW(dw_selezione,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'"+" and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco <= " + s_cs_xx.db_funzioni.oggi + "))")
					  
if isnull(s_cs_xx.parametri.parametro_s_2) then

	f_PO_LoadDDDW_DW(dw_selezione,"cod_tipo_manutenzione",sqlca,&
						  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
						  "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "'")		
else		
	ls_cod_attrezzatura = s_cs_xx.parametri.parametro_s_2
	f_PO_LoadDDDW_DW(dw_selezione,"cod_tipo_manutenzione",sqlca,& 
					  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
					  "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + ls_cod_attrezzatura + "'")					  
end if					  
end event

type dw_selezione from uo_dw_main within w_conferma_manutenzione
integer y = 20
integer width = 3017
integer height = 440
string dataobject = "d_conferma_manutenzioni"
boolean border = false
end type

event pcd_new;call super::pcd_new;long ll_max_num

select max(num_registrazione) + 1
into   :ll_max_num
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	
	g_mb.messagebox("Omnia", "Errore in lettura dati in tabella MANUTENZIONI")
	
	return
	
end if	

dw_selezione.setitem(1, "anno_registrazione", year(today()))

dw_selezione.setitem(1, "num_registrazione", ll_max_num)

dw_selezione.setitem(1, "cod_tipo_manutenzione", s_cs_xx.parametri.parametro_s_1)

dw_selezione.setitem(1, "cod_attrezzatura", s_cs_xx.parametri.parametro_s_2)

dw_selezione.setitem(1, "cod_operaio", s_cs_xx.parametri.parametro_s_3)

dw_selezione.setitem(1, "data_registrazione", today())
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_null
	
	setnull(ls_null)
	
   choose case i_colname
      case "cod_attrezzatura"
			dw_selezione.setitem(1, "cod_tipo_manutenzione", ls_null)
			f_PO_LoadDDDW_DW(dw_selezione,"cod_tipo_manutenzione",sqlca,& 
							  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
							  "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + i_coltext + "'")
	end choose
end if




					  
end event

type cb_conferma from commandbutton within w_conferma_manutenzione
integer x = 2629
integer y = 460
integer width = 366
integer height = 80
integer taborder = 2
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;string   ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_cod_operaio, ls_descrizione, ls_modalita_esecuzione

long     ll_anno_registrazione, ll_num_registrazione

datetime ldt_data_registrazione

dw_selezione.accepttext()

setpointer(hourglass!)

if il_controllo_conferma = 0 then
	
	if isnull(dw_selezione.getitemnumber(1, "anno_registrazione")) or &
		isnull(dw_selezione.getitemnumber(1, "num_registrazione")) or &
		isnull(dw_selezione.getitemstring(1, "descrizione")) or &
		isnull(dw_selezione.getitemstring(1, "cod_tipo_manutenzione")) or &
		isnull(dw_selezione.getitemstring(1, "cod_attrezzatura")) or &
		isnull(dw_selezione.getitemstring(1, "cod_operaio")) or &
		isnull(dw_selezione.getitemdate(1, "data_registrazione")) then
		
		g_mb.messagebox("Omnia", "Attenzione dati incompleti!")
		
		setpointer(arrow!)
		
		return -1
		
	end if	
	
	ll_anno_registrazione = dw_selezione.getitemnumber(1, "anno_registrazione")
	
	ll_num_registrazione = dw_selezione.getitemnumber(1, "num_registrazione")
	
	ls_descrizione = dw_selezione.getitemstring(1, "descrizione")
	
	ls_cod_tipo_manutenzione = dw_selezione.getitemstring(1, "cod_tipo_manutenzione")
	
	ls_cod_attrezzatura = dw_selezione.getitemstring(1, "cod_attrezzatura")
	
	ls_cod_operaio = dw_selezione.getitemstring(1, "cod_operaio")
	
	ldt_data_registrazione = datetime(today())
	
	select modalita_esecuzione
	into   :ls_modalita_esecuzione
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			 
	if isnull(ls_modalita_esecuzione) then ls_modalita_esecuzione = ""
	
	if isnull(is_note) then is_note = ""
	
	if ls_modalita_esecuzione <> "" then ls_modalita_esecuzione = ls_modalita_esecuzione + "~n"
	
	ls_modalita_esecuzione = ls_modalita_esecuzione + is_note
	
	if ls_modalita_esecuzione = "" then setnull(ls_modalita_esecuzione)
	
	insert
	into   manutenzioni
	       (cod_azienda,
			 anno_registrazione,
			 num_registrazione,
			 des_intervento,
			 data_registrazione,
			 cod_attrezzatura,
			 cod_tipo_manutenzione,
			 flag_ordinario,
			 cod_operaio,
			 anno_non_conf,
			 num_non_conf,
			 anno_reg_richiesta,
			 num_reg_richiesta,
			 note_idl)
	values (:s_cs_xx.cod_azienda,
	       :ll_anno_registrazione,
			 :ll_num_registrazione,
			 :ls_descrizione,
			 :ldt_data_registrazione,
			 :ls_cod_attrezzatura,
			 :ls_cod_tipo_manutenzione,
			 'N',
			 :ls_cod_operaio,
			 :il_anno_nc,
			 :il_num_nc,
			 :il_anno_ric,
			 :il_num_ric,
			 :ls_modalita_esecuzione);
				  
	if sqlca.sqlcode <> 0 then 
		
		g_mb.messagebox("Omnia","Errore nella insert di manutenzioni: " + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext + &
		           "~nAnno Registrazione " + string(ll_anno_registrazione) + " Numero Registrazione " + string(ll_num_registrazione))
		rollback;
		
		setpointer(arrow!)
		
		return -1
		
	end if
	
	commit;
	
	g_mb.messagebox("Omnia","Manutenzione generata con successo")
	
	close(parent)
	
else
	
	g_mb.messagebox("Omnia","Attenzione si sta tentando di creare una manutenzione già esistente!")
	
end if

setpointer(arrow!)
end event

