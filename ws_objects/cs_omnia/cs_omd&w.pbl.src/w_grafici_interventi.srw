﻿$PBExportHeader$w_grafici_interventi.srw
forward
global type w_grafici_interventi from w_cs_xx_principale
end type
type dw_graph_schede_intervento from uo_cs_graph within w_grafici_interventi
end type
type cb_annulla from commandbutton within w_grafici_interventi
end type
type hpb_1 from hprogressbar within w_grafici_interventi
end type
type cb_elabora from commandbutton within w_grafici_interventi
end type
type gb_1 from groupbox within w_grafici_interventi
end type
type gb_2 from groupbox within w_grafici_interventi
end type
type dw_sel_grafico from u_dw_search within w_grafici_interventi
end type
type gb_3 from groupbox within w_grafici_interventi
end type
end forward

global type w_grafici_interventi from w_cs_xx_principale
integer width = 3241
integer height = 2160
string title = "Statistiche Interventi"
boolean maxbox = false
boolean resizable = false
dw_graph_schede_intervento dw_graph_schede_intervento
cb_annulla cb_annulla
hpb_1 hpb_1
cb_elabora cb_elabora
gb_1 gb_1
gb_2 gb_2
dw_sel_grafico dw_sel_grafico
gb_3 gb_3
end type
global w_grafici_interventi w_grafici_interventi

on w_grafici_interventi.create
int iCurrent
call super::create
this.dw_graph_schede_intervento=create dw_graph_schede_intervento
this.cb_annulla=create cb_annulla
this.hpb_1=create hpb_1
this.cb_elabora=create cb_elabora
this.gb_1=create gb_1
this.gb_2=create gb_2
this.dw_sel_grafico=create dw_sel_grafico
this.gb_3=create gb_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_graph_schede_intervento
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.hpb_1
this.Control[iCurrent+4]=this.cb_elabora
this.Control[iCurrent+5]=this.gb_1
this.Control[iCurrent+6]=this.gb_2
this.Control[iCurrent+7]=this.dw_sel_grafico
this.Control[iCurrent+8]=this.gb_3
end on

on w_grafici_interventi.destroy
call super::destroy
destroy(this.dw_graph_schede_intervento)
destroy(this.cb_annulla)
destroy(this.hpb_1)
destroy(this.cb_elabora)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.dw_sel_grafico)
destroy(this.gb_3)
end on

event pc_setwindow;call super::pc_setwindow;cb_annulla.triggerevent("clicked")

dw_sel_grafico.setitem(1,"tipo_report","C")

dw_sel_grafico.event trigger itemchanged(1,dw_sel_grafico.object.tipo_report,"C")
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_grafico,"cod_area",sqlca,"tab_aree_aziendali","cod_area_aziendale",&
							"des_area","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_graph_schede_intervento from uo_cs_graph within w_grafici_interventi
integer x = 50
integer y = 772
integer width = 3122
integer height = 1268
integer taborder = 30
end type

type cb_annulla from commandbutton within w_grafici_interventi
integer x = 2423
integer y = 380
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string ls_null


setnull(ls_null)

dw_sel_grafico.setitem(1,"data_a",today())

dw_sel_grafico.setitem(1,"data_da",relativedate(today(),-30))

dw_sel_grafico.setitem(1,"flag_autorizzato","%")

dw_sel_grafico.setitem(1,"cod_area",ls_null)

dw_sel_grafico.setitem(1,"cod_cliente",ls_null)

dw_sel_grafico.setitem(1,"cod_operaio",ls_null)

dw_sel_grafico.setitem(1,"cod_prodotto",ls_null)
end event

type hpb_1 from hprogressbar within w_grafici_interventi
integer x = 69
integer y = 580
integer width = 3077
integer height = 64
unsignedinteger maxposition = 100
integer setstep = 1
end type

type cb_elabora from commandbutton within w_grafici_interventi
integer x = 2811
integer y = 380
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora"
end type

event clicked;datetime ldt_da, ldt_a
long     ll_riga, ll_count
string   ls_prodotto, ls_cliente, ls_difetto, ls_filtro_area, ls_filtro_cliente, ls_filtro_prodotto, ls_filtro_operaio, ls_tipo_reso
string ls_tipo_report
datastore lds_dati

dw_sel_grafico.accepttext()

ldt_da = dw_sel_grafico.getitemdatetime(1,"data_da")

ldt_a = dw_sel_grafico.getitemdatetime(1,"data_a")

if isnull(ldt_da) or isnull(ldt_a) then
	g_mb.messagebox("OMNIA","Impostare correttamente entrambe le date",stopsign!)
	return -1
end if

if ldt_da > ldt_a then
	g_mb.messagebox("OMNIA","La data inizio è maggiore della data fine",stopsign!)
	return -1
end if

lds_dati = create datastore
lds_dati.dataobject = "d_grafici_interventi"
lds_dati.settransobject(sqlca)

ls_tipo_reso = dw_sel_grafico.getitemstring(1,"flag_autorizzato")

ls_filtro_area = dw_sel_grafico.getitemstring(1,"cod_area")

ls_filtro_cliente = dw_sel_grafico.getitemstring(1,"cod_cliente")

ls_filtro_operaio = dw_sel_grafico.getitemstring(1,"cod_operaio")

ls_filtro_prodotto = dw_sel_grafico.getitemstring(1,"cod_prodotto")

enabled = false

setpointer(hourglass!)

hpb_1.position = 0

//dw_grafico.reset()

//dw_grafico.setredraw(false)

select count(*)
into   :ll_count
from   schede_intervento
where  cod_azienda = :s_cs_xx.cod_azienda and
		 data_inizio_intervento >= :ldt_da and
		 data_inizio_intervento <= :ldt_a and
		 flag_tipo_reso like :ls_tipo_reso and
		 (cod_area_aziendale = :ls_filtro_area or :ls_filtro_area is null) and
		 (cod_cliente = :ls_filtro_cliente or :ls_filtro_cliente is null) and
		 (cod_operaio = :ls_filtro_operaio or :ls_filtro_operaio is null) and
		 (cod_prodotto = :ls_filtro_prodotto or :ls_filtro_prodotto is null);
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore nella select di schede_intervento: " + sqlca.sqlerrtext)
	enabled = true
	setpointer(arrow!)
	hpb_1.position = 0
	destroy lds_dati
	//dw_grafico.setredraw(true)
	return -1
elseif sqlca.sqlcode = 100 or ll_count < 1 then
	enabled = true
	setpointer(arrow!)
	hpb_1.position = 0
	destroy lds_dati
	//dw_grafico.setredraw(true)
	return 0
end if

declare interventi cursor for

select cod_cliente,
		 cod_prodotto,
		 cod_errore
from   schede_intervento
where  cod_azienda = :s_cs_xx.cod_azienda and
		 data_inizio_intervento >= :ldt_da and
		 data_inizio_intervento <= :ldt_a and
		 flag_tipo_reso like :ls_tipo_reso and
		 (cod_area_aziendale = :ls_filtro_area or :ls_filtro_area is null) and
		 (cod_cliente = :ls_filtro_cliente or :ls_filtro_cliente is null) and
		 (cod_operaio = :ls_filtro_operaio or :ls_filtro_operaio is null) and
		 (cod_prodotto = :ls_filtro_prodotto or :ls_filtro_prodotto is null);
		 
open interventi;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore interventi: " + sqlca.sqlerrtext)
	enabled = true
	setpointer(arrow!)
	hpb_1.position = 0
	destroy lds_dati
	//dw_grafico.setredraw(true)
	return -1
end if

do while true
	
	fetch interventi
	into  :ls_cliente,
			:ls_prodotto,
			:ls_difetto;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore interventi: " + sqlca.sqlerrtext)
		close interventi;
		enabled = true
		setpointer(arrow!)
		hpb_1.position = 0
		destroy lds_dati
		//dw_grafico.setredraw(true)
		return -1		
	elseif sqlca.sqlcode = 100 then
		close interventi;
		exit
	end if
	/*
	ll_riga = dw_grafico.insertrow(0)
	dw_grafico.setitem(ll_riga,"cliente",ls_cliente)
	dw_grafico.setitem(ll_riga,"prodotto",ls_prodotto)
	dw_grafico.setitem(ll_riga,"difetto",ls_difetto)
	*/
	ll_riga = lds_dati.insertrow(0)
	lds_dati.setitem(ll_riga,"cliente",ls_cliente)
	lds_dati.setitem(ll_riga,"prodotto",ls_prodotto)
	lds_dati.setitem(ll_riga,"difetto",ls_difetto)
	
	hpb_1.position = round( (ll_riga * 100 / ll_count) , 0 )
	
loop

enabled = true
setpointer(arrow!)
hpb_1.position = 0

//costruzione grafico -----------------------------------------------------------------------------
dw_graph_schede_intervento.ib_datastore = true
dw_graph_schede_intervento.ids_data = lds_dati

dw_graph_schede_intervento.is_str[1] = "cliente"
dw_graph_schede_intervento.is_str[2] = "prodotto"
dw_graph_schede_intervento.is_str[3] = "difetto"

ls_tipo_report = dw_sel_grafico.getitemstring(dw_sel_grafico.getrow(), "tipo_report")
choose case ls_tipo_report
	case "C"
		dw_graph_schede_intervento.is_source_categoria_col = "cliente"
		dw_graph_schede_intervento.is_categoria_col = "str_1"
		dw_graph_schede_intervento.is_exp_valore = "count(str_1 for graph)"
		dw_graph_schede_intervento.is_serie_col = ""
		dw_graph_schede_intervento.is_label_categoria= "CLIENTE"
		
	case "P"
		dw_graph_schede_intervento.is_source_categoria_col = "prodotto"
		dw_graph_schede_intervento.is_categoria_col = "str_2"
		dw_graph_schede_intervento.is_exp_valore = "count(str_2 for graph)"
		dw_graph_schede_intervento.is_serie_col = ""
		dw_graph_schede_intervento.is_label_categoria= "PRODOTTO"
		
	case "D"
		dw_graph_schede_intervento.is_source_categoria_col = "difetto"
		dw_graph_schede_intervento.is_categoria_col = "str_3"
		dw_graph_schede_intervento.is_exp_valore = "count(str_3 for graph)"
		dw_graph_schede_intervento.is_serie_col = ""
		dw_graph_schede_intervento.is_label_categoria= "DIFETTO"
		
end choose

dw_graph_schede_intervento.is_titolo = ""
dw_graph_schede_intervento.is_label_valori = "Reclami"

//tipo scala: 1 se lineare, 2 se logaritmica in base 10
dw_graph_schede_intervento.ii_scala = 1

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_graph_schede_intervento.ii_tipo_grafico = 9  //Colonne 3D Obj

//colore di sfondo del grafico
//per il bianco (DEFAULT)	: 16777215
//per il silver			: 12632256
dw_graph_schede_intervento.il_backcolor = 12632256 //silver

//metodo che visualizza i dati nel grafico
dw_graph_schede_intervento.uof_retrieve( )

destroy lds_dati
//--------------------------------------------------------------------------------------------------------

destroy lds_dati

//dw_grafico.setredraw(true)

end event

type gb_1 from groupbox within w_grafici_interventi
integer x = 23
integer y = 500
integer width = 3177
integer height = 180
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stato Elaborazione:"
end type

type gb_2 from groupbox within w_grafici_interventi
integer x = 23
integer y = 700
integer width = 3177
integer height = 1360
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Risultati Elaborazione:"
end type

type dw_sel_grafico from u_dw_search within w_grafici_interventi
integer x = 46
integer y = 40
integer width = 3131
integer height = 420
integer taborder = 20
string dataobject = "d_sel_grafici_interventi"
boolean border = false
end type

event itemchanged;call super::itemchanged;if isnull(dwo) then
	return 0
end if

if dwo.name = "tipo_report" then
		
		choose case data
				//str_1 -> cliente
				//str_2 -> prodotto
				//str_3 -> difetto
			case "C"
				/*
				dw_grafico.object.gr_1.category = 'cliente'
				dw_grafico.object.gr_1.values = 'count(cliente for graph)'
				dw_grafico.object.gr_1.category.label = 'CLIENTE'
				*/
				cb_elabora.postevent(clicked!)
				
			case "P"
				/*
				dw_grafico.object.gr_1.category = 'prodotto'
				dw_grafico.object.gr_1.values = 'count(prodotto for graph)'
				dw_grafico.object.gr_1.category.label = 'PRODOTTO'
				*/
				cb_elabora.postevent(clicked!)
				
			case "D"
				/*
				dw_grafico.object.gr_1.category = 'difetto'
				dw_grafico.object.gr_1.values = 'count(difetto for graph)'
				dw_grafico.object.gr_1.category.label = 'DIFETTO'
				*/
				cb_elabora.postevent(clicked!)
				
		end choose
		
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_sel_grafico,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_sel_grafico,"cod_prodotto")
	case "b_ricerca_operaio"
		guo_ricerca.uof_ricerca_operaio(dw_sel_grafico,"cod_operaio")
end choose
end event

type gb_3 from groupbox within w_grafici_interventi
integer x = 23
integer width = 3177
integer height = 480
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
end type

