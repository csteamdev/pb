﻿$PBExportHeader$w_grafici_richieste.srw
forward
global type w_grafici_richieste from w_cs_xx_principale
end type
type hpb_1 from hprogressbar within w_grafici_richieste
end type
type dw_grafico from datawindow within w_grafici_richieste
end type
type cb_elabora from commandbutton within w_grafici_richieste
end type
type dw_sel_grafico from datawindow within w_grafici_richieste
end type
type gb_1 from groupbox within w_grafici_richieste
end type
type gb_2 from groupbox within w_grafici_richieste
end type
type gb_3 from groupbox within w_grafici_richieste
end type
end forward

global type w_grafici_richieste from w_cs_xx_principale
integer width = 3232
integer height = 2160
string title = "Statistiche Richieste"
boolean maxbox = false
boolean resizable = false
hpb_1 hpb_1
dw_grafico dw_grafico
cb_elabora cb_elabora
dw_sel_grafico dw_sel_grafico
gb_1 gb_1
gb_2 gb_2
gb_3 gb_3
end type
global w_grafici_richieste w_grafici_richieste

on w_grafici_richieste.create
int iCurrent
call super::create
this.hpb_1=create hpb_1
this.dw_grafico=create dw_grafico
this.cb_elabora=create cb_elabora
this.dw_sel_grafico=create dw_sel_grafico
this.gb_1=create gb_1
this.gb_2=create gb_2
this.gb_3=create gb_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.hpb_1
this.Control[iCurrent+2]=this.dw_grafico
this.Control[iCurrent+3]=this.cb_elabora
this.Control[iCurrent+4]=this.dw_sel_grafico
this.Control[iCurrent+5]=this.gb_1
this.Control[iCurrent+6]=this.gb_2
this.Control[iCurrent+7]=this.gb_3
end on

on w_grafici_richieste.destroy
call super::destroy
destroy(this.hpb_1)
destroy(this.dw_grafico)
destroy(this.cb_elabora)
destroy(this.dw_sel_grafico)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.gb_3)
end on

event pc_setwindow;call super::pc_setwindow;dw_sel_grafico.insertrow(0)

dw_sel_grafico.setitem(1,"data_a",today())

dw_sel_grafico.setitem(1,"data_da",relativedate(today(),-30))

dw_sel_grafico.setitem(1,"tipo_report","C")

dw_sel_grafico.event trigger itemchanged(1,dw_sel_grafico.object.tipo_report,"C")
end event

type hpb_1 from hprogressbar within w_grafici_richieste
integer x = 69
integer y = 260
integer width = 3077
integer height = 64
unsignedinteger maxposition = 100
integer setstep = 1
end type

type dw_grafico from datawindow within w_grafici_richieste
integer x = 46
integer y = 460
integer width = 3131
integer height = 1580
integer taborder = 30
string dataobject = "d_grafici_richieste"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type cb_elabora from commandbutton within w_grafici_richieste
integer x = 2789
integer y = 60
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elabora"
end type

event clicked;datetime ldt_da, ldt_a

long     ll_riga, ll_count

string   ls_tipo, ls_prodotto, ls_cliente, ls_difetto


dw_sel_grafico.accepttext()

ldt_da = dw_sel_grafico.getitemdatetime(1,"data_da")

ldt_a = dw_sel_grafico.getitemdatetime(1,"data_a")

if isnull(ldt_da) or isnull(ldt_a) then
	g_mb.messagebox("OMNIA","Impostare correttamente entrambe le date",stopsign!)
	return -1
end if

if ldt_da > ldt_a then
	g_mb.messagebox("OMNIA","La data inizio è maggiore della data fine",stopsign!)
	return -1
end if

enabled = false

setpointer(hourglass!)

hpb_1.position = 0

dw_grafico.reset()

dw_grafico.setredraw(false)

select count(*)
into   :ll_count
from   richieste
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_tipo_richiesta = 'I' and
		 data_richiesta >= :ldt_da and
		 data_richiesta <= :ldt_a;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore nella select di richieste: " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
elseif sqlca.sqlcode = 100 or ll_count < 1 then
	enabled = true
	setpointer(arrow!)
	hpb_1.position = 0
	dw_grafico.setredraw(true)
	return 0
end if

declare richieste cursor for

select cod_cliente,
		 cod_prodotto,
		 cod_errore
from   richieste
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_tipo_richiesta = 'I' and
		 data_richiesta >= :ldt_da and
		 data_richiesta <= :ldt_a;
		 
open richieste;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella open del cursore richieste: " + sqlca.sqlerrtext)
	enabled = true
	setpointer(arrow!)
	hpb_1.position = 0
	dw_grafico.setredraw(true)
	return -1
end if

do while true
	
	fetch richieste
	into  :ls_cliente,
			:ls_prodotto,
			:ls_difetto;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore richieste: " + sqlca.sqlerrtext)
		close richieste;
		enabled = true
		setpointer(arrow!)
		hpb_1.position = 0
		dw_grafico.setredraw(true)
		return -1		
	elseif sqlca.sqlcode = 100 then
		close richieste;
		exit
	end if
	
	ll_riga = dw_grafico.insertrow(0)
	dw_grafico.setitem(ll_riga,"cliente",ls_cliente)
	dw_grafico.setitem(ll_riga,"prodotto",ls_prodotto)
	dw_grafico.setitem(ll_riga,"difetto",ls_difetto)
	
	hpb_1.position = round( (ll_riga * 100 / ll_count) , 0 )
	
loop

enabled = true

setpointer(arrow!)

hpb_1.position = 0

dw_grafico.setredraw(true)
end event

type dw_sel_grafico from datawindow within w_grafici_richieste
integer x = 46
integer y = 40
integer width = 2697
integer height = 100
integer taborder = 10
string dataobject = "d_sel_grafici_richieste"
boolean border = false
boolean livescroll = true
end type

event itemchanged;if dwo.name = "tipo_report" then
		
		choose case data
				
			case "C"
				
				dw_grafico.object.gr_1.category = 'cliente'
				dw_grafico.object.gr_1.values = 'count(cliente for graph)'
				dw_grafico.object.gr_1.category.label = 'CLIENTE'
				
			case "P"
				
				dw_grafico.object.gr_1.category = 'prodotto'
				dw_grafico.object.gr_1.values = 'count(prodotto for graph)'
				dw_grafico.object.gr_1.category.label = 'PRODOTTO'
								
			case "D"
				
				dw_grafico.object.gr_1.category = 'difetto'
				dw_grafico.object.gr_1.values = 'count(difetto for graph)'
				dw_grafico.object.gr_1.category.label = 'DIFETTO'
								
		end choose
		
end if
end event

type gb_1 from groupbox within w_grafici_richieste
integer x = 23
integer y = 180
integer width = 3177
integer height = 180
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Stato Elaborazione:"
borderstyle borderstyle = stylelowered!
end type

type gb_2 from groupbox within w_grafici_richieste
integer x = 23
integer y = 380
integer width = 3177
integer height = 1680
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Risultati Elaborazione:"
borderstyle borderstyle = stylelowered!
end type

type gb_3 from groupbox within w_grafici_richieste
integer x = 23
integer width = 3177
integer height = 160
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
borderstyle borderstyle = stylelowered!
end type

