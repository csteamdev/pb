﻿$PBExportHeader$w_input_azione_correttiva.srw
$PBExportComments$Inserimento Azione Correttiva da Vista Attesa Azioni Correttive OK
forward
global type w_input_azione_correttiva from w_cs_xx_risposta
end type
type dw_azione_correttiva from uo_cs_xx_dw within w_input_azione_correttiva
end type
type cb_1 from uo_cb_close within w_input_azione_correttiva
end type
end forward

global type w_input_azione_correttiva from w_cs_xx_risposta
int Width=1802
int Height=1293
boolean TitleBar=true
string Title="Input Azione Correttiva"
dw_azione_correttiva dw_azione_correttiva
cb_1 cb_1
end type
global w_input_azione_correttiva w_input_azione_correttiva

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_azione_correttiva.set_dw_key("cod_azienda")
dw_azione_correttiva.set_dw_options(sqlca,pcca.null_object,c_nonew+c_nodelete+c_modifyonopen,c_default)

end on

on w_input_azione_correttiva.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_azione_correttiva=create dw_azione_correttiva
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_azione_correttiva
this.Control[iCurrent+2]=cb_1
end on

on w_input_azione_correttiva.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_azione_correttiva)
destroy(this.cb_1)
end on

type dw_azione_correttiva from uo_cs_xx_dw within w_input_azione_correttiva
int X=1
int Y=21
int Width=1783
int Height=1041
int TabOrder=10
string DataObject="d_azione_correttiva"
boolean Border=false
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_d_1,s_cs_xx.parametri.parametro_i_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type cb_1 from uo_cb_close within w_input_azione_correttiva
int X=1441
int Y=1061
int TabOrder=20
string Text="&Chiudi"
end type

