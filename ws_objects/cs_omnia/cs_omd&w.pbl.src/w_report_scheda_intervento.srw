﻿$PBExportHeader$w_report_scheda_intervento.srw
$PBExportComments$Finestra Report Scheda Intervento
forward
global type w_report_scheda_intervento from w_cs_xx_principale
end type
type dw_report_scheda_intervento from uo_cs_xx_dw within w_report_scheda_intervento
end type
end forward

global type w_report_scheda_intervento from w_cs_xx_principale
integer width = 3872
integer height = 4772
string title = "Report Scheda Intervento"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
dw_report_scheda_intervento dw_report_scheda_intervento
end type
global w_report_scheda_intervento w_report_scheda_intervento

type variables
boolean ib_modifica=false, ib_nuovo=false
end variables

event pc_setwindow;call super::pc_setwindow;dw_report_scheda_intervento.ib_dw_report = true

set_w_options(c_noresizewin)

dw_report_scheda_intervento.set_dw_options(sqlca, &
                                            pcca.null_object, &
                                            c_nonew + &
                                            c_nomodify + &
                                            c_nodelete + &
                                            c_noenablenewonopen + &
                                            c_noenablemodifyonopen + &
                                            c_scrollparent + &
                                            c_disableCC + &
                                            c_disableCCinsert, &
                                            c_nohighlightselected + &
                                            c_nocursorrowfocusrect + &
                                            c_nocursorrowpointer)

end event

on w_report_scheda_intervento.create
int iCurrent
call super::create
this.dw_report_scheda_intervento=create dw_report_scheda_intervento
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_scheda_intervento
end on

on w_report_scheda_intervento.destroy
call super::destroy
destroy(this.dw_report_scheda_intervento)
end on

type dw_report_scheda_intervento from uo_cs_xx_dw within w_report_scheda_intervento
integer x = 23
integer y = 20
integer width = 3657
integer height = 4520
integer taborder = 20
string dataobject = "d_report_scheda_intervento"
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


//ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
//ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

ll_errore = retrieve(s_cs_xx.cod_azienda, &
                     s_cs_xx.parametri.parametro_d_1, &
                     s_cs_xx.parametri.parametro_d_2  )
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

