﻿$PBExportHeader$w_report_tempi_chiusura.srw
forward
global type w_report_tempi_chiusura from w_cs_xx_principale
end type
type cb_operaio_ricerca from cb_operai_ricerca within w_report_tempi_chiusura
end type
type cb_report from commandbutton within w_report_tempi_chiusura
end type
type dw_sel_tempi_chiusura from uo_cs_xx_dw within w_report_tempi_chiusura
end type
type cb_selezione from commandbutton within w_report_tempi_chiusura
end type
type dw_report_tempi_chiusura from uo_cs_xx_dw within w_report_tempi_chiusura
end type
end forward

global type w_report_tempi_chiusura from w_cs_xx_principale
integer width = 2807
integer height = 608
string title = "Report Tempi Chiusura"
boolean maxbox = false
boolean resizable = false
cb_operaio_ricerca cb_operaio_ricerca
cb_report cb_report
dw_sel_tempi_chiusura dw_sel_tempi_chiusura
cb_selezione cb_selezione
dw_report_tempi_chiusura dw_report_tempi_chiusura
end type
global w_report_tempi_chiusura w_report_tempi_chiusura

on w_report_tempi_chiusura.create
int iCurrent
call super::create
this.cb_operaio_ricerca=create cb_operaio_ricerca
this.cb_report=create cb_report
this.dw_sel_tempi_chiusura=create dw_sel_tempi_chiusura
this.cb_selezione=create cb_selezione
this.dw_report_tempi_chiusura=create dw_report_tempi_chiusura
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_operaio_ricerca
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_sel_tempi_chiusura
this.Control[iCurrent+4]=this.cb_selezione
this.Control[iCurrent+5]=this.dw_report_tempi_chiusura
end on

on w_report_tempi_chiusura.destroy
call super::destroy
destroy(this.cb_operaio_ricerca)
destroy(this.cb_report)
destroy(this.dw_sel_tempi_chiusura)
destroy(this.cb_selezione)
destroy(this.dw_report_tempi_chiusura)
end on

event pc_setwindow;call super::pc_setwindow;dw_report_tempi_chiusura.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_sel_tempi_chiusura.set_dw_options(sqlca, &
                     			       pcca.null_object, &
			                            c_nomodify + &
         			                   c_nodelete + &
                  			          c_newonopen + &
                           			 c_disableCC, &
			                            c_noresizedw + &
         			                   c_nohighlightselected + &
                  			          c_cursorrowpointer)
												 
dw_report_tempi_chiusura.set_dw_options(sqlca, &
				            	             pcca.null_object, &
            				   	          c_nomodify + &
				                 		       c_nodelete + &
				                 	    	    c_newonopen + &
			                         		 c_disableCC, &
					                         c_noresizedw + &
               					          c_nohighlightselected + &
					                         c_nocursorrowpointer +&
               					          c_nocursorrowfocusrect )

dw_sel_tempi_chiusura.change_dw_current()
dw_sel_tempi_chiusura.setfocus()
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_tempi_chiusura,"cod_intervento",sqlca,"tab_interventi",&
					"cod_intervento","des_intervento","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event getfocus;call super::getfocus;dw_sel_tempi_chiusura.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type cb_operaio_ricerca from cb_operai_ricerca within w_report_tempi_chiusura
integer x = 2629
integer y = 128
integer width = 73
integer height = 72
integer taborder = 40
boolean bringtotop = true
end type

event getfocus;call super::getfocus;dw_sel_tempi_chiusura.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_operaio"

end event

type cb_report from commandbutton within w_report_tempi_chiusura
integer x = 2377
integer y = 400
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;long     ll_x, ll_y, ll_return

datetime ldt_da, ldt_a

string   ls_cliente, ls_operaio, ls_intervento, ls_filter


dw_report_tempi_chiusura.change_dw_current()

dw_report_tempi_chiusura.setfocus()

dw_report_tempi_chiusura.object.datawindow.print.preview = 'No'

dw_report_tempi_chiusura.object.datawindow.print.preview.rulers = 'No'

ls_filter = ""

dw_report_tempi_chiusura.setfilter(ls_filter)

dw_report_tempi_chiusura.filter()

dw_sel_tempi_chiusura.accepttext()

ldt_da = dw_sel_tempi_chiusura.getitemdatetime(1,"da_data")

ldt_a = dw_sel_tempi_chiusura.getitemdatetime(1,"a_data")

if isnull(ldt_da) or isnull(ldt_a) then
	g_mb.messagebox("OMNIA","Impostare correttamente entrambe le date di selezione",stopsign!)
	return -1
end if

if ldt_da > ldt_a then
	g_mb.messagebox("OMNIA","Data inizio maggiore di data fine",stopsign!)
	return -1
end if

setpointer(hourglass!)

ls_cliente = dw_sel_tempi_chiusura.getitemstring(1,"cod_cliente")

if len(trim(ls_cliente)) < 1 then
	setnull(ls_cliente)
end if

ls_operaio = dw_sel_tempi_chiusura.getitemstring(1,"cod_operaio")

if len(trim(ls_operaio)) < 1 then
	setnull(ls_operaio)
end if

ls_intervento = dw_sel_tempi_chiusura.getitemstring(1,"cod_intervento")

if len(trim(ls_intervento)) < 1 then
	setnull(ls_intervento)
end if

ll_return = dw_report_tempi_chiusura.retrieve(s_cs_xx.cod_azienda,ldt_da,ldt_a)

if ll_return < 0 then
	g_mb.messagebox("OMNIA","Errore nella retrieve di dw_report_tempi_chiusura",stopsign!)
	setpointer(arrow!)
	return -1
end if	

dw_report_tempi_chiusura.object.t_periodo.text = "dal " + string(date(ldt_da)) + " al " + string(date(ldt_a))

if not isnull(ls_cliente) then
	
	if len(ls_filter) > 0 then
		ls_filter = ls_filter + " and "
	end if
	
	ls_filter = ls_filter + "cod_cliente = '" + ls_cliente + "'"
	
end if

if not isnull(ls_operaio) then
	
	if len(ls_filter) > 0 then
		ls_filter = ls_filter + " and "
	end if
	
	ls_filter = ls_filter + "cod_operaio = '" + ls_operaio + "'"
	
end if

if not isnull(ls_intervento) then
	
	if len(ls_filter) > 0 then
		ls_filter = ls_filter + " and "
	end if
	
	ls_filter = ls_filter + "cod_intervento = '" + ls_intervento + "'"
	
end if

dw_report_tempi_chiusura.setfilter(ls_filter)

dw_report_tempi_chiusura.filter()

dw_report_tempi_chiusura.resetupdate()

setpointer(arrow!)

parent.width = 3700

parent.height = 2370

ll_x = (w_cs_xx_mdi.WorkSpaceWidth() - parent.width)/2
ll_y = (w_cs_xx_mdi.WorkSpaceheight() - parent.height)/2

if ll_x < 0 then ll_x = 0
if ll_y < 0 then ll_y = 0

parent.move(ll_x,ll_y)

hide()

dw_sel_tempi_chiusura.object.b_ricerca_cliente.visible=false

cb_operaio_ricerca.hide()

dw_sel_tempi_chiusura.hide()

dw_report_tempi_chiusura.show()

cb_selezione.show()

dw_report_tempi_chiusura.object.datawindow.print.preview = 'Yes'

dw_report_tempi_chiusura.object.datawindow.print.preview.rulers = 'Yes'
end event

type dw_sel_tempi_chiusura from uo_cs_xx_dw within w_report_tempi_chiusura
integer x = 23
integer y = 20
integer width = 2720
integer height = 360
integer taborder = 20
string dataobject = "d_sel_tempi_chiusura"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_sel_tempi_chiusura,"cod_cliente")
end choose
end event

type cb_selezione from commandbutton within w_report_tempi_chiusura
integer x = 3291
integer y = 2180
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Selezione"
end type

event clicked;long ll_x, ll_y


hide()

dw_report_tempi_chiusura.hide()

dw_sel_tempi_chiusura.show()

cb_report.show()

dw_sel_tempi_chiusura.object.b_ricerca_cliente.visible=true

cb_operaio_ricerca.show()

parent.width = 2807

parent.height = 608

ll_x = (w_cs_xx_mdi.WorkSpaceWidth() - parent.width)/2
ll_y = (w_cs_xx_mdi.WorkSpaceheight() - parent.height)/2

if ll_x < 0 then ll_x = 0
if ll_y < 0 then ll_y = 0

parent.move(ll_x,ll_y)
end event

type dw_report_tempi_chiusura from uo_cs_xx_dw within w_report_tempi_chiusura
boolean visible = false
integer x = 23
integer y = 20
integer width = 3634
integer height = 2140
integer taborder = 10
string dataobject = "d_report_tempi_chiusura"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

