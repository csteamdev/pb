﻿$PBExportHeader$w_ricerca_az_corr.srw
$PBExportComments$Finestra Clienti Ricerca
forward
global type w_ricerca_az_corr from w_cs_xx_principale
end type
type cb_numero from uo_cb_ok within w_ricerca_az_corr
end type
type cb_annulla from uo_cb_close within w_ricerca_az_corr
end type
type cb_ok from commandbutton within w_ricerca_az_corr
end type
type cb_descrizione from uo_cb_ok within w_ricerca_az_corr
end type
type cb_anno from uo_cb_ok within w_ricerca_az_corr
end type
type st_ricerca from statictext within w_ricerca_az_corr
end type
type dw_ricerca from datawindow within w_ricerca_az_corr
end type
type st_totale from statictext within w_ricerca_az_corr
end type
type dw_lista from datawindow within w_ricerca_az_corr
end type
end forward

global type w_ricerca_az_corr from w_cs_xx_principale
integer width = 2501
integer height = 1528
boolean titlebar = false
string title = ""
boolean controlmenu = false
boolean minbox = false
boolean maxbox = false
boolean resizable = false
boolean border = false
cb_numero cb_numero
cb_annulla cb_annulla
cb_ok cb_ok
cb_descrizione cb_descrizione
cb_anno cb_anno
st_ricerca st_ricerca
dw_ricerca dw_ricerca
st_totale st_totale
dw_lista dw_lista
end type
global w_ricerca_az_corr w_ricerca_az_corr

type variables
long il_row
string is_tipo_ricerca='A'
end variables

forward prototypes
public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca)
end prototypes

public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca);long		ll_pos

string	ls_sql


ls_sql = "select anno_registrazione, num_registrazione, descrizione from tab_azioni_correttive where cod_azienda = '" + s_cs_xx.cod_azienda + "'"

if not isnull(fs_stringa_ricerca) and len(trim(fs_stringa_ricerca)) > 0 then

	if right(trim(fs_stringa_ricerca), 1) <> "*" then
		fs_stringa_ricerca += "*"
	end if
	
	for ll_pos = 1 to len(fs_stringa_ricerca)
		if mid(fs_stringa_ricerca, ll_pos, 1) = "*" then
			fs_stringa_ricerca = replace(fs_stringa_ricerca, ll_pos, 1, "%")
		end if
	next
	
	ll_pos = 1
	
	do
	
		ll_pos = pos(fs_stringa_ricerca,"'",ll_pos)
		
		if ll_pos <> 0 then
			fs_stringa_ricerca = replace(fs_stringa_ricerca,ll_pos,1,"''")
			ll_pos = ll_pos + 2
		end if	
	
	loop while ll_pos <> 0
	
	choose case fs_tipo_ricerca
		case "A"
			ls_sql += " and anno_registrazione = " + fs_stringa_ricerca
			ls_sql += " order by anno_registrazione ASC, num_registrazione ASC"
		case "N"
			ls_sql += " and num_registrazione = " + fs_stringa_ricerca
			ls_sql += " order by num_registrazione ASC, anno_registrazione ASC"
		case "D"
			if pos(fs_stringa_ricerca,"%",1) > 0 then
				ls_sql += " and descrizione like '" + fs_stringa_ricerca + "'"
			else
				ls_sql += " and descrizione = " + fs_stringa_ricerca + "'"
			end if
			ls_sql += " order by descrizione ASC, anno_registrazione ASC, num_registrazione ASC"
	end choose
	
end if

if dw_lista.setsqlselect(ls_sql) < 1 then
	g_mb.messagebox("APICE", "Errore nella assegnazione sintassi SQL di ricerca con setsqlselect: comunicare l'errore all'amministratore del sistema")
	return
end if

if dw_lista.retrieve() > 0 then
	st_totale.text = string(dw_lista.rowcount()) + " RECORD TROVATI"
else
	st_totale.text = "NESSUN RECORD TROVATO"
	dw_ricerca.setfocus()
end if
end subroutine

on w_ricerca_az_corr.create
int iCurrent
call super::create
this.cb_numero=create cb_numero
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.cb_descrizione=create cb_descrizione
this.cb_anno=create cb_anno
this.st_ricerca=create st_ricerca
this.dw_ricerca=create dw_ricerca
this.st_totale=create st_totale
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_numero
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_ok
this.Control[iCurrent+4]=this.cb_descrizione
this.Control[iCurrent+5]=this.cb_anno
this.Control[iCurrent+6]=this.st_ricerca
this.Control[iCurrent+7]=this.dw_ricerca
this.Control[iCurrent+8]=this.st_totale
this.Control[iCurrent+9]=this.dw_lista
end on

on w_ricerca_az_corr.destroy
call super::destroy
destroy(this.cb_numero)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.cb_descrizione)
destroy(this.cb_anno)
destroy(this.st_ricerca)
destroy(this.dw_ricerca)
destroy(this.st_totale)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_lista.settransobject(sqlca)

dw_lista.setrowfocusindicator(hand!)

dw_ricerca.insertrow(0)

choose case s_cs_xx.parametri.parametro_tipo_ricerca
	case 1
		cb_anno.triggerevent("clicked")
	case 2
		cb_numero.triggerevent("clicked")
	case 3
		cb_descrizione.triggerevent("clicked")
	case else
		cb_anno.triggerevent("clicked")
end choose

if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) and len(trim(s_cs_xx.parametri.parametro_pos_ricerca)) > 0 then
	dw_ricerca.setitem(1,"stringa_ricerca",s_cs_xx.parametri.parametro_pos_ricerca)
	dw_ricerca.event trigger ue_key(keyenter!,0)
else
	dw_ricerca.setfocus()
end if
end event

event activate;call super::activate;dw_ricerca.setfocus()
end event

type cb_numero from uo_cb_ok within w_ricerca_az_corr
integer x = 389
integer y = 120
integer width = 343
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Numero"
end type

event clicked;call super::clicked;dw_lista.reset()
is_tipo_ricerca = "N"
st_ricerca.text = "Ricerca per Numero:"
dw_ricerca.setfocus()

end event

type cb_annulla from uo_cb_close within w_ricerca_az_corr
integer x = 1714
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type cb_ok from commandbutton within w_ricerca_az_corr
integer x = 2103
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;if dw_lista.getrow() > 0 then
	
	if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_1.settext (string(dw_lista.getitemnumber(dw_lista.getrow(),"anno_registrazione")))
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
		s_cs_xx.parametri.parametro_uo_dw_1.settext (string(dw_lista.getitemnumber(dw_lista.getrow(),"num_registrazione")))
		s_cs_xx.parametri.parametro_uo_dw_1.accepttext()
		s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
		s_cs_xx.parametri.parametro_uo_dw_1.setfocus()
	elseif not isnull(s_cs_xx.parametri.parametro_uo_dw_search) then
		s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_search.settext(string(dw_lista.getitemnumber(dw_lista.getrow(),"anno_registrazione")))
		s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_2)
		s_cs_xx.parametri.parametro_uo_dw_search.settext(string(dw_lista.getitemnumber(dw_lista.getrow(),"num_registrazione")))
		s_cs_xx.parametri.parametro_uo_dw_search.accepttext()
		s_cs_xx.parametri.parametro_uo_dw_search.setfocus()
	elseif not isnull(s_cs_xx.parametri.parametro_dw_1) then
		s_cs_xx.parametri.parametro_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_dw_1.settext(string(dw_lista.getitemnumber(dw_lista.getrow(),"anno_registrazione")))
		s_cs_xx.parametri.parametro_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
		s_cs_xx.parametri.parametro_dw_1.settext(string(dw_lista.getitemnumber(dw_lista.getrow(),"num_registrazione")))
		s_cs_xx.parametri.parametro_dw_1.accepttext()
		s_cs_xx.parametri.parametro_dw_1.setfocus()
	end if
	
	if il_row > 0 and not isnull(il_row) then
		dw_lista.setrow(il_row - 1)
		il_row = 0
	end if
	
	parent.hide()
	
end if
end event

type cb_descrizione from uo_cb_ok within w_ricerca_az_corr
integer x = 731
integer y = 120
integer width = 1737
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Descrizione"
end type

event clicked;call super::clicked;dw_lista.reset()
is_tipo_ricerca = "D"
st_ricerca.text = "Ricerca per Descrizione:"
dw_ricerca.setfocus()

end event

type cb_anno from uo_cb_ok within w_ricerca_az_corr
integer x = 137
integer y = 120
integer width = 251
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Anno"
end type

event clicked;call super::clicked;dw_lista.reset()
is_tipo_ricerca = "A"
st_ricerca.text = "Ricerca per Anno:"
dw_ricerca.setfocus()

end event

type st_ricerca from statictext within w_ricerca_az_corr
integer x = 23
integer y = 32
integer width = 686
integer height = 60
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ricerca per Anno:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_ricerca from datawindow within w_ricerca_az_corr
event ue_key pbm_dwnkey
integer x = 731
integer y = 20
integer width = 1737
integer height = 80
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_ricerca_az_corr_sel"
boolean border = false
end type

event ue_key;if key = keyenter! then
	wf_dw_select(is_tipo_ricerca,gettext())
	dw_lista.setfocus()
end if
end event

event getfocus;selecttext(1,len(gettext()))
end event

type st_totale from statictext within w_ricerca_az_corr
integer x = 23
integer y = 1420
integer width = 1669
integer height = 80
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
boolean border = true
boolean focusrectangle = false
end type

type dw_lista from datawindow within w_ricerca_az_corr
event ue_key pbm_dwnkey
integer x = 23
integer y = 200
integer width = 2446
integer height = 1200
integer taborder = 30
string dataobject = "d_ricerca_az_corr_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;CHOOSE CASE key
	CASE KeyEnter!
		il_row = this.getrow()
		cb_ok.triggerevent("clicked")
	Case keydownarrow! 
		if il_row < this.rowcount() then
			il_row = this.getrow() + 1
		end if
	Case keyuparrow!
		if il_row > 1 then
			il_row = this.getrow() - 1
		end if
END CHOOSE

end event

event doubleclicked;cb_ok.postevent(clicked!)

end event

