﻿$PBExportHeader$w_richieste.srw
$PBExportComments$Finestra Richieste Manutenzione/Interventi/Visite Ispettive
forward
global type w_richieste from w_cs_xx_principale
end type
type dw_richieste_lista from uo_cs_xx_dw within w_richieste
end type
type dw_richieste_det from uo_cs_xx_dw within w_richieste
end type
end forward

global type w_richieste from w_cs_xx_principale
int Width=2209
int Height=1925
dw_richieste_lista dw_richieste_lista
dw_richieste_det dw_richieste_det
end type
global w_richieste w_richieste

forward prototypes
public function integer wf_tipo_richiesta (string ws_tipo_richiesta)
end prototypes

public function integer wf_tipo_richiesta (string ws_tipo_richiesta);   dw_richieste_det.Modify("cod_tipo_manutenzione_t.Visible=0")
   dw_richieste_det.Modify("cf_cod_tipo_manutenzione.Visible=0")
   dw_richieste_det.Modify("cod_tipo_manutenzione.Visible=0")
   dw_richieste_det.Modify("cod_attrezzatura_t.Visible=0")
   dw_richieste_det.Modify("cf_attrezzatura.Visible=0")
   dw_richieste_det.Modify("cod_attrezzatura.Visible=0")
   dw_richieste_det.Modify("num_registrazione_t.Visible=0")
   dw_richieste_det.Modify("num_registrazione.Visible=0")
   dw_richieste_det.Modify("ret_1.Visible=0")

   dw_richieste_det.Modify("cod_intervento_t.Visible=0")
   dw_richieste_det.Modify("cf_cod_intervento.Visible=0")
   dw_richieste_det.Modify("cod_intervento.Visible=0")
   dw_richieste_det.Modify("num_reg_intervento_t.Visible=0")
   dw_richieste_det.Modify("num_reg_intervento.Visible=0")
   dw_richieste_det.Modify("anno_reg_intervento_t.Visible=0")
   dw_richieste_det.Modify("anno_reg_intervento.Visible=0")
   dw_richieste_det.Modify("ret_3.Visible=0")

   dw_richieste_det.Modify("cod_intervento.Visible=0")
   dw_richieste_det.Modify("num_reg_visita_t.Visible=0")
   dw_richieste_det.Modify("num_reg_visita.Visible=0")
   dw_richieste_det.Modify("anno_reg_visita_t.Visible=0")
   dw_richieste_det.Modify("anno_reg_visita.Visible=0")
   dw_richieste_det.Modify("ret_2.Visible=0")

choose case ws_tipo_richiesta
case "M"
   dw_richieste_det.Modify("cod_tipo_manutenzione_t.Visible=1")
   dw_richieste_det.Modify("cf_cod_tipo_manutenzione.Visible=1")
   dw_richieste_det.Modify("cod_tipo_manutenzione.Visible=1")
   dw_richieste_det.Modify("cod_attrezzatura_t.Visible=1")
   dw_richieste_det.Modify("cf_attrezzatura.Visible=1")
   dw_richieste_det.Modify("cod_attrezzatura.Visible=1")
   dw_richieste_det.Modify("num_registrazione_t.Visible=1")
   dw_richieste_det.Modify("num_registrazione.Visible=1")
   dw_richieste_det.Modify("ret_1.Visible=1")
case "I"
   dw_richieste_det.Modify("cod_intervento_t.Visible=1")
   dw_richieste_det.Modify("cf_cod_intervento.Visible=1")
   dw_richieste_det.Modify("cod_intervento.Visible=1")
   dw_richieste_det.Modify("num_reg_intervento_t.Visible=1")
   dw_richieste_det.Modify("num_reg_intervento.Visible=1")
   dw_richieste_det.Modify("anno_reg_intervento_t.Visible=1")
   dw_richieste_det.Modify("anno_reg_intervento.Visible=1")
   dw_richieste_det.Modify("ret_3.Visible=1")
case "V"
   dw_richieste_det.Modify("cod_intervento.Visible=1")
   dw_richieste_det.Modify("num_reg_visita_t.Visible=1")
   dw_richieste_det.Modify("num_reg_visita.Visible=1")
   dw_richieste_det.Modify("anno_reg_visita_t.Visible=1")
   dw_richieste_det.Modify("anno_reg_visita.Visible=1")
   dw_richieste_det.Modify("ret_2.Visible=1")
end choose
return 0

end function

on w_richieste.create
int iCurrent
call w_cs_xx_principale::create
this.dw_richieste_lista=create dw_richieste_lista
this.dw_richieste_det=create dw_richieste_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_richieste_lista
this.Control[iCurrent+2]=dw_richieste_det
end on

on w_richieste.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_richieste_lista)
destroy(this.dw_richieste_det)
end on

event pc_new;call super::pc_new;dw_richieste_det.setitem(dw_richieste_det.getrow(), "data_richiesta", datetime(today()))
end event

type dw_richieste_lista from uo_cs_xx_dw within w_richieste
int X=23
int Y=21
int Width=2126
int Height=501
int TabOrder=10
string DataObject="d_richieste_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

type dw_richieste_det from uo_cs_xx_dw within w_richieste
int X=23
int Y=541
int Width=2126
int Height=1261
int TabOrder=20
string DataObject="d_richieste_det"
BorderStyle BorderStyle=StyleRaised!
end type

