﻿$PBExportHeader$w_richieste_elenco_manut.srw
$PBExportComments$Finestra Gestione Richieste Intervento
forward
global type w_richieste_elenco_manut from w_cs_xx_risposta
end type
type dw_richieste_elenco_manut from datawindow within w_richieste_elenco_manut
end type
type cb_genera from commandbutton within w_richieste_elenco_manut
end type
type cb_chiudi from commandbutton within w_richieste_elenco_manut
end type
end forward

global type w_richieste_elenco_manut from w_cs_xx_risposta
integer width = 2784
integer height = 1068
string title = "Elenco Manutenzioni - Richiesta di Intervento"
dw_richieste_elenco_manut dw_richieste_elenco_manut
cb_genera cb_genera
cb_chiudi cb_chiudi
end type
global w_richieste_elenco_manut w_richieste_elenco_manut

type variables
long il_anno, il_num, il_row

datawindow idw_openparm
end variables

on w_richieste_elenco_manut.create
int iCurrent
call super::create
this.dw_richieste_elenco_manut=create dw_richieste_elenco_manut
this.cb_genera=create cb_genera
this.cb_chiudi=create cb_chiudi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_richieste_elenco_manut
this.Control[iCurrent+2]=this.cb_genera
this.Control[iCurrent+3]=this.cb_chiudi
end on

on w_richieste_elenco_manut.destroy
call super::destroy
destroy(this.dw_richieste_elenco_manut)
destroy(this.cb_genera)
destroy(this.cb_chiudi)
end on

event pc_setwindow;call super::pc_setwindow;idw_openparm = i_openparm

il_row = idw_openparm.getrow()

il_anno = idw_openparm.getitemnumber(il_row,"anno_reg_richiesta")

il_num = idw_openparm.getitemnumber(il_row,"num_reg_richiesta")

title = "Elenco Manutenzioni - Richiesta di Intervento " + string(il_anno) + " / " + string(il_num)

dw_richieste_elenco_manut.settransobject(sqlca)

dw_richieste_elenco_manut.retrieve(s_cs_xx.cod_azienda,il_anno,il_num)
end event

type dw_richieste_elenco_manut from datawindow within w_richieste_elenco_manut
integer x = 23
integer y = 20
integer width = 2697
integer height = 820
integer taborder = 20
string dataobject = "d_richieste_elenco_manut"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_genera from commandbutton within w_richieste_elenco_manut
integer x = 1966
integer y = 860
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = idw_openparm.getitemstring(il_row,"cod_tipo_manutenzione")
s_cs_xx.parametri.parametro_s_2 = idw_openparm.getitemstring(il_row,"cod_attrezzatura")
s_cs_xx.parametri.parametro_s_3 = idw_openparm.getitemstring(il_row,"cod_operaio")
s_cs_xx.parametri.parametro_d_1 = il_anno
s_cs_xx.parametri.parametro_d_2 = il_num
s_cs_xx.parametri.parametro_d_3 = idw_openparm.getitemnumber(il_row,"anno_non_conf")
s_cs_xx.parametri.parametro_d_4 = idw_openparm.getitemnumber(il_row,"num_non_conf")

window_open(w_conferma_manutenzione, 0)

dw_richieste_elenco_manut.retrieve(s_cs_xx.cod_azienda,il_anno,il_num)
end event

type cb_chiudi from commandbutton within w_richieste_elenco_manut
integer x = 2354
integer y = 860
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

