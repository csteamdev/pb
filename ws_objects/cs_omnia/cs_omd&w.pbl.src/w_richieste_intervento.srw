﻿$PBExportHeader$w_richieste_intervento.srw
$PBExportComments$Finestra Gestione Richieste Intervento
forward
global type w_richieste_intervento from w_richieste
end type
type cb_visite_isp from commandbutton within w_richieste_intervento
end type
type cb_stock_ric from cb_stock_ricerca within w_richieste_intervento
end type
type cb_nc from commandbutton within w_richieste_intervento
end type
type cb_ano from commandbutton within w_richieste_intervento
end type
type cb_int from commandbutton within w_richieste_intervento
end type
type cb_chiusura from commandbutton within w_richieste_intervento
end type
end forward

global type w_richieste_intervento from w_richieste
integer width = 2231
integer height = 2344
string title = "Richieste Intervento"
cb_visite_isp cb_visite_isp
cb_stock_ric cb_stock_ric
cb_nc cb_nc
cb_ano cb_ano
cb_int cb_int
cb_chiusura cb_chiusura
end type
global w_richieste_intervento w_richieste_intervento

type variables
boolean ib_in_new = FALSE
end variables

forward prototypes
public function integer wf_tipo_richiesta (string ws_tipo_richiesta)
end prototypes

public function integer wf_tipo_richiesta (string ws_tipo_richiesta);dw_richieste_det.Modify("cod_tipo_manutenzione_t.Visible=1")
dw_richieste_det.Modify("cf_cod_tipo_manutenzione.Visible=1")
dw_richieste_det.Modify("cod_tipo_manutenzione.Visible=1")
dw_richieste_det.Modify("cod_attrezzatura_t.Visible=1")
dw_richieste_det.Modify("cf_attrezzatura.Visible=1")
dw_richieste_det.Modify("cod_attrezzatura.Visible=1")
//dw_richieste_det.Modify("num_registrazione_t.Visible=1")
//dw_richieste_det.Modify("num_registrazione.Visible=1")
dw_richieste_det.Modify("ret_1.Visible=1")

dw_richieste_det.Modify("cod_intervento_t.Visible=1")
dw_richieste_det.Modify("cf_cod_intervento.Visible=1")
dw_richieste_det.Modify("cod_intervento.Visible=1")
dw_richieste_det.Modify("num_reg_intervento_t.Visible=1")
dw_richieste_det.Modify("num_reg_intervento.Visible=1")
dw_richieste_det.Modify("anno_reg_intervento_t.Visible=1")
dw_richieste_det.Modify("anno_reg_intervento.Visible=1")
dw_richieste_det.Modify("ret_3.Visible=1")

dw_richieste_det.Modify("cod_intervento.Visible=1")
dw_richieste_det.Modify("num_reg_visita_t.Visible=1")
dw_richieste_det.Modify("num_reg_visita.Visible=1")
dw_richieste_det.Modify("anno_reg_visita_t.Visible=1")
dw_richieste_det.Modify("anno_reg_visita.Visible=1")
dw_richieste_det.Modify("ret_2.Visible=1")

return 0

end function

event pc_setwindow;call super::pc_setwindow;dw_richieste_lista.set_dw_key("cod_azienda")
dw_richieste_lista.set_dw_key("flag_tipo_richiesta")
if (s_cs_xx.parametri.parametro_s_1 = "INTERVENTO") and &
   (s_cs_xx.parametri.parametro_d_1 > 0) and &
   (s_cs_xx.parametri.parametro_d_2 > 0)  then
      dw_richieste_lista.set_dw_options(sqlca,pcca.null_object,c_newonopen,c_default)
else
   dw_richieste_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
end if
dw_richieste_det.set_dw_options(sqlca,dw_richieste_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_richieste_lista

wf_tipo_richiesta("I")
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_richieste_det,"cod_intervento",sqlca,&
                 "tab_interventi","cod_intervento","des_intervento",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_richieste_det,"cod_errore",sqlca,&
                 "tab_difformita","cod_errore","des_difformita",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_richieste_intervento.create
int iCurrent
call super::create
this.cb_visite_isp=create cb_visite_isp
this.cb_stock_ric=create cb_stock_ric
this.cb_nc=create cb_nc
this.cb_ano=create cb_ano
this.cb_int=create cb_int
this.cb_chiusura=create cb_chiusura
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_visite_isp
this.Control[iCurrent+2]=this.cb_stock_ric
this.Control[iCurrent+3]=this.cb_nc
this.Control[iCurrent+4]=this.cb_ano
this.Control[iCurrent+5]=this.cb_int
this.Control[iCurrent+6]=this.cb_chiusura
end on

on w_richieste_intervento.destroy
call super::destroy
destroy(this.cb_visite_isp)
destroy(this.cb_stock_ric)
destroy(this.cb_nc)
destroy(this.cb_ano)
destroy(this.cb_int)
destroy(this.cb_chiusura)
end on

type dw_richieste_lista from w_richieste`dw_richieste_lista within w_richieste_intervento
integer width = 2149
string dataobject = "d_richieste_intervento_lista"
end type

event dw_richieste_lista::pcd_new;call super::pcd_new;if (s_cs_xx.parametri.parametro_s_1 = "INTERVENTO") and &
   (s_cs_xx.parametri.parametro_d_1 > 0) and &
   (s_cs_xx.parametri.parametro_d_2 > 0)  then
       this.setitem(this.getrow(),"anno_non_conf", s_cs_xx.parametri.parametro_d_1)
       this.setitem(this.getrow(),"num_non_conf", s_cs_xx.parametri.parametro_d_2)
end if
ib_in_new = true
this.SetItem(this.getrow(), "flag_tipo_richiesta", "I")
this.SetItem(this.getrow(), "data_richiesta", today())
this.SetItem(this.getrow(), "data_scadenza", relativedate(today(), 7))
this.SetItem(this.getrow(), "flag_ric_reso", "N")

cb_nc.enabled = false
cb_ano.enabled = false
cb_int.enabled = false
cb_chiusura.enabled = false
cb_visite_isp.enabled = false

cb_stock_ric.enabled = true
dw_richieste_det.object.b_ricerca_operaio.enabled = true
dw_richieste_det.object.b_ricerca_cliente.enabled = true
dw_richieste_det.object.b_ricerca_commessa.enabled = true
dw_richieste_det.object.b_ricerca_prodotto.enabled = true
end event

on dw_richieste_lista::pcd_setkey;call w_richieste`dw_richieste_lista::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "flag_tipo_richiesta")) THEN
      SetItem(l_Idx, "flag_tipo_richiesta", "I")
   END IF
NEXT

end on

event dw_richieste_lista::pcd_view;call super::pcd_view;ib_in_new = false

cb_nc.enabled = true
cb_ano.enabled = true
cb_int.enabled = true
cb_chiusura.enabled = true
cb_visite_isp.enabled = true

cb_stock_ric.enabled = false
dw_richieste_det.object.b_ricerca_operaio.enabled = false
dw_richieste_det.object.b_ricerca_cliente.enabled = false
dw_richieste_det.object.b_ricerca_commessa.enabled = false
dw_richieste_det.object.b_ricerca_prodotto.enabled = false
end event

on dw_richieste_lista::pcd_retrieve;call w_richieste`dw_richieste_lista::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, "I")

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on dw_richieste_lista::pcd_savebefore;call w_richieste`dw_richieste_lista::pcd_savebefore;setnull(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_d_1 = 0
s_cs_xx.parametri.parametro_d_2 = 0

end on

event dw_richieste_lista::updatestart;call super::updatestart;if ib_in_new then

   long ll_anno_registrazione, ll_num_registrazione

   dw_richieste_det.SetItem (dw_richieste_det.GetRow ( ),"anno_reg_richiesta", year(today()) )
   ll_anno_registrazione = dw_richieste_det.GetItemNumber(dw_richieste_det.GetRow ( ), "anno_reg_richiesta" )
   select max(richieste.num_reg_richiesta)
     into :ll_num_registrazione
     from richieste
     where (richieste.cod_azienda = :s_cs_xx.cod_azienda) and (:ll_anno_registrazione = richieste.anno_reg_richiesta);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   dw_richieste_det.SetItem (dw_richieste_det.GetRow ( ),"num_reg_richiesta", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      dw_richieste_det.SetItem (dw_richieste_det.GetRow ( ), "num_reg_richiesta", 1)
   end if

   ib_in_new = false
end if


long 	 ll_i, ll_anno, ll_num

string ls_difetto, ls_tipo_lista, ls_cod_lista


if s_cs_xx.num_livello_mail > 0 then

	for ll_i = 1 to rowcount()
		
		ll_anno = getitemnumber(ll_i,"anno_reg_richiesta")
		
		ll_num = getitemnumber(ll_i,"num_reg_richiesta")
		
		ls_difetto = getitemstring(ll_i,"cod_errore")
		
		select cod_tipo_lista_dist,
				 cod_lista_dist
		into   :ls_tipo_lista,
				 :ls_cod_lista
		from   tab_difformita
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_errore = :ls_difetto;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura lista distribuzione da tab_difformita: " + sqlca.sqlerrtext)
			continue
		elseif sqlca.sqlcode = 100 then
			continue
		end if
		
		if getitemstatus(ll_i,"cod_errore",primary!) = datamodified! then
			s_cs_xx.parametri.parametro_s_1 = "L"
			s_cs_xx.parametri.parametro_s_2 = ls_tipo_lista
			s_cs_xx.parametri.parametro_s_3 = ls_cod_lista
			s_cs_xx.parametri.parametro_s_4 = ""
			s_cs_xx.parametri.parametro_s_5 = "Registrazione Richiesta di Intervento " + string(ll_anno) + "/" + string(ll_num)
			s_cs_xx.parametri.parametro_s_6 = "E' stata registrata la Richiesta di Intervento " + string(ll_anno) + "/" + string(ll_num)
			s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione alla lista di distribuzione collegata al difetto?"
			s_cs_xx.parametri.parametro_s_8 = ""
			openwithparm(w_invio_messaggi,0)
		else		
			continue		
		end if
		
	next
	
end if
end event

event dw_richieste_lista::pcd_modify;call super::pcd_modify;ib_in_new = false

cb_nc.enabled = false
cb_ano.enabled = false
cb_int.enabled = false
cb_chiusura.enabled = false
cb_visite_isp.enabled = false

cb_stock_ric.enabled = true
dw_richieste_det.object.b_ricerca_operaio.enabled = true
dw_richieste_det.object.b_ricerca_cliente.enabled = true
dw_richieste_det.object.b_ricerca_commessa.enabled = true
dw_richieste_det.object.b_ricerca_prodotto.enabled = true


end event

event dw_richieste_lista::pcd_save;call super::pcd_save;cb_nc.enabled = true
cb_ano.enabled = true
cb_int.enabled = true
cb_chiusura.enabled = true
cb_visite_isp.enabled = true

cb_stock_ric.enabled = false
dw_richieste_det.object.b_ricerca_operaio.enabled = false
dw_richieste_det.object.b_ricerca_cliente.enabled = false
dw_richieste_det.object.b_ricerca_commessa.enabled = false
dw_richieste_det.object.b_ricerca_prodotto.enabled = false
end event

type dw_richieste_det from w_richieste`dw_richieste_det within w_richieste_intervento
integer width = 2149
integer height = 1580
string dataobject = "d_richieste_intervento_det"
end type

event dw_richieste_det::itemchanged;call super::itemchanged;string ls_null


setnull(ls_null)

choose case i_colname
		
	case "cod_operaio","cod_cliente","cod_prodotto","cod_deposito"
		
		if i_coltext = "" then
			
			setitem(getrow(),i_colname,ls_null)
			
		end if
		
end choose
end event

event dw_richieste_det::buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_operaio"
		guo_ricerca.uof_ricerca_operaio(dw_richieste_det,"cod_operaio")
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_richieste_det,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_richieste_det,"cod_prodotto")
	case "b_ricerca_commessa"
		guo_ricerca.uof_ricerca_commessa(dw_richieste_det,"anno_commessa","num_commessa")
end choose
end event

type cb_visite_isp from commandbutton within w_richieste_intervento
event clicked pbm_bnclicked
integer x = 1829
integer y = 2140
integer width = 343
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Manutenz."
end type

event clicked;window_open_parm(w_richieste_elenco_manut,0,dw_richieste_lista)
end event

type cb_stock_ric from cb_stock_ricerca within w_richieste_intervento
integer x = 2039
integer y = 1172
integer width = 73
integer height = 72
integer taborder = 60
boolean bringtotop = true
boolean enabled = false
end type

event clicked;call super::clicked;s_cs_xx.parametri.parametro_s_10 = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)

s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_richieste_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)
end event

type cb_nc from commandbutton within w_richieste_intervento
integer x = 366
integer y = 2140
integer width = 343
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Non Conf."
end type

event clicked;string   ls_cod_resp

long     ll_row, ll_anno, ll_num, ll_anno_nc, ll_num_nc


select cod_resp_divisione
into   :ls_cod_resp
from   mansionari
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_apertura_nc = 'S';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_cod_resp) then
	g_mb.messagebox("OMNIA","Il mansionario corrente non è abilitato all'apertura delle non conformità")
	return -1
end if

ll_anno_nc = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"anno_non_conf")

ll_num_nc = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"num_non_conf")

if isnull(ll_anno_nc) and isnull(ll_num_nc) then
	
	ll_anno_nc = year(today())
	
	select max(num_non_conf)
	into   :ll_num_nc
	from   non_conformita
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_non_conf = :ll_anno_nc;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella select di non_conformita: " + sqlca.sqlerrtext)
		return -1
	elseif sqlca.sqlcode = 100 or isnull(ll_num_nc) then
		ll_num_nc = 0
	end if
	
	ll_num_nc++
	
	insert
	into   non_conformita
			 (cod_azienda,
			 anno_non_conf,
			 num_non_conf,
			 flag_tipo_nc)
	values (:s_cs_xx.cod_azienda,
			 :ll_anno_nc,
			 :ll_num_nc,
			 'V');
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella insert di non_conformita: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	ll_anno = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"anno_reg_richiesta")
	
	ll_num = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"num_reg_richiesta")
	
	update richieste
	set    anno_non_conf = :ll_anno_nc,
			 num_non_conf = :ll_num_nc
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_richiesta = :ll_anno and
			 num_reg_richiesta = :ll_num;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella update di richieste: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	commit;
			 
	ll_row = dw_richieste_lista.getrow()
	
	dw_richieste_lista.triggerevent("pcd_retrieve")
	
	dw_richieste_lista.setrow(ll_row)
	
	dw_richieste_lista.scrolltorow(ll_row)
	
end if

s_cs_xx.parametri.parametro_d_1 = ll_anno_nc

s_cs_xx.parametri.parametro_d_2 = ll_num_nc

window_open(w_non_conformita_std_ven,-1)
end event

type cb_ano from commandbutton within w_richieste_intervento
integer x = 731
integer y = 2140
integer width = 343
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Anomalia"
end type

event clicked;long     ll_row, ll_anno, ll_num, ll_anno_ano, ll_num_ano


ll_anno_ano = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"anno_reg_anomalia")

ll_num_ano = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"num_reg_anomalia")

if isnull(ll_anno_ano) and isnull(ll_num_ano) then
	
	ll_anno_ano = year(today())
	
	select max(num_registrazione)
	into   :ll_num_ano
	from   tab_anomalie
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_ano;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella select di tab_anomalie: " + sqlca.sqlerrtext)
		return -1
	elseif sqlca.sqlcode = 100 or isnull(ll_num_ano) then
		ll_num_ano = 0
	end if
	
	ll_num_ano++
	
	insert
	into   tab_anomalie
			 (cod_azienda,
			 anno_registrazione,
			 num_registrazione)
	values (:s_cs_xx.cod_azienda,
			 :ll_anno_ano,
			 :ll_num_ano);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella insert di tab_anomalie: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	ll_anno = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"anno_reg_richiesta")
	
	ll_num = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"num_reg_richiesta")
	
	update richieste
	set    anno_reg_anomalia = :ll_anno_ano,
			 num_reg_anomalia = :ll_num_ano
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_richiesta = :ll_anno and
			 num_reg_richiesta = :ll_num;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella update di richieste: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	commit;
			 
	ll_row = dw_richieste_lista.getrow()
	
	dw_richieste_lista.triggerevent("pcd_retrieve")
	
	dw_richieste_lista.setrow(ll_row)
	
	dw_richieste_lista.scrolltorow(ll_row)
	
end if

s_cs_xx.parametri.parametro_d_1 = ll_anno_ano

s_cs_xx.parametri.parametro_d_2 = ll_num_ano

window_open(w_anomalie,-1)
end event

type cb_int from commandbutton within w_richieste_intervento
integer x = 1097
integer y = 2140
integer width = 343
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Intervento"
end type

event clicked;dec{4}   ld_quan_stock

datetime ldt_data_stock, ldt_reso

long     ll_row, ll_anno, ll_num, ll_anno_int, ll_num_int, ll_prog_stock, ll_anno_fat, ll_num_fat,&
			ll_riga_fat, ll_anno_nc, ll_num_nc

string   ls_cod_cliente, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, &
			ls_cod_difetto, ls_flag, ls_resp_reso


ll_anno_int = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"anno_reg_intervento")

ll_num_int = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"num_reg_intervento")

if isnull(ll_anno_int) and isnull(ll_num_int) then
	
	ll_anno = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"anno_reg_richiesta")
	
	ll_num = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"num_reg_richiesta")
	
	ls_cod_cliente = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_cliente")
	
	ls_cod_prodotto = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_prodotto")
	
	ls_cod_deposito = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_deposito")
	
	ls_cod_ubicazione = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_ubicazione")
	
	ls_cod_lotto = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_lotto")
	
	ls_cod_difetto = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_errore")
	
	ldt_data_stock = dw_richieste_det.getitemdatetime(dw_richieste_det.getrow(),"data_stock")
	
	ll_prog_stock = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"prog_stock")
	
	ll_anno_fat = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"anno_reg_fat_ven")
	
	ll_num_fat = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"num_reg_fat_ven")
	
	ll_riga_fat = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"prog_riga_fat_ven")
	
	ll_anno_nc = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"anno_non_conf")
	
	ll_num_nc = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"num_non_conf")
	
	ls_flag = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"flag_ric_reso")
	
	select cod_resp_divisione
	into   :ls_resp_reso
	from   mansionari
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_utente = :s_cs_xx.cod_utente and
			 flag_autorizza_reso = 'S';
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
		setnull(ls_resp_reso)
		setnull(ldt_reso)
	elseif sqlca.sqlcode = 100 or isnull(ls_resp_reso) then
		g_mb.messagebox("OMNIA","Il mansionario corrente non è abilitato ad autorizzare un reso")
		setnull(ls_resp_reso)
		setnull(ldt_reso)
	else
	
		ldt_reso = datetime(today(),now())
		
		select quan_assegnata
		into   :ld_quan_stock
		from   stock
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto and
				 cod_deposito = :ls_cod_deposito and
				 cod_ubicazione = :ls_cod_ubicazione and
				 cod_lotto = :ls_cod_lotto and
				 data_stock = :ldt_data_stock and
				 prog_stock = :ll_prog_stock;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore in lettura quantità stock da tabella stock: " + sqlca.sqlerrtext)
			setnull(ld_quan_stock)
		elseif sqlca.sqlcode = 100 then
			setnull(ld_quan_stock)
		end if
		
	end if
	
	ll_anno_int = year(today())
	
	select max(num_reg_intervento)
	into   :ll_num_int
	from   schede_intervento
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_intervento = :ll_anno_int;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella select di schede_intervento: " + sqlca.sqlerrtext)
		return -1
	elseif sqlca.sqlcode = 100 or isnull(ll_num_int) then
		ll_num_int = 0
	end if
	
	ll_num_int++
	
	insert
	into   schede_intervento
			 (cod_azienda,
			 anno_reg_intervento,
			 num_reg_intervento,
			 anno_reg_richiesta,
			 num_reg_richiesta,
			 cod_cliente,
			 cod_prodotto,
			 cod_deposito,
			 cod_ubicazione,
			 cod_lotto,
			 cod_errore,
			 data_stock,
			 prog_stock,
			 anno_registrazione,
			 num_registrazione,
			 prog_riga_fat_ven,
			 anno_non_conf,
			 num_non_conf,
			 flag_reso,
			 anno_reg_campionamenti,
			 num_reg_campionamenti,
			 cod_resp_divisione,
			 data_reso,
			 quan_stock)
	values (:s_cs_xx.cod_azienda,
			 :ll_anno_int,
			 :ll_num_int,
			 :ll_anno,
			 :ll_num,
			 :ls_cod_cliente,
			 :ls_cod_prodotto,
			 :ls_cod_deposito,
			 :ls_cod_ubicazione,
			 :ls_cod_lotto,
			 :ls_cod_difetto,
			 :ldt_data_stock,
			 :ll_prog_stock,
			 :ll_anno_fat,
			 :ll_num_fat,
			 :ll_riga_fat,
			 :ll_anno_nc,
			 :ll_num_nc,
			 :ls_flag,
			 null,
			 null,
			 :ls_resp_reso,
			 :ldt_reso,
			 :ld_quan_stock);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella insert di schede_intervento: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	update richieste
	set    anno_reg_intervento = :ll_anno_int,
			 num_reg_intervento = :ll_num_int
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_richiesta = :ll_anno and
			 num_reg_richiesta = :ll_num;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore nella update di richieste: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	commit;
			 
	ll_row = dw_richieste_lista.getrow()
	
	dw_richieste_lista.triggerevent("pcd_retrieve")
	
	dw_richieste_lista.setrow(ll_row)
	
	dw_richieste_lista.scrolltorow(ll_row)
	
	if isnull(dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_resp_divisione")) and &
		isnull(dw_richieste_det.getitemdatetime(dw_richieste_det.getrow(),"data_chiusura")) then		
		cb_chiusura.triggerevent("clicked")
	end if	
	
end if	

s_cs_xx.parametri.parametro_d_1 = ll_anno_int

s_cs_xx.parametri.parametro_d_2 = ll_num_int

window_open(w_schede_intervento,-1)
end event

type cb_chiusura from commandbutton within w_richieste_intervento
integer x = 1463
integer y = 2140
integer width = 343
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiusura"
end type

event clicked;string   ls_cod_resp

long     ll_anno, ll_num, ll_row

datetime ldt_chiusura


ls_cod_resp = dw_richieste_det.getitemstring(dw_richieste_det.getrow(),"cod_resp_divisione")
ldt_chiusura = dw_richieste_det.getitemdatetime(dw_richieste_det.getrow(),"data_chiusura")

if not isnull(ls_cod_resp) and not isnull(ldt_chiusura) then	
	g_mb.messagebox("OMNIA","Questa richiesta di intervento è già stata chiusa in data " + string(ldt_chiusura) + " dal mansionario " + ls_cod_resp)
	return -1
end if

if g_mb.messagebox("OMNIA","Chiudere la richiesta di intervento?",question!,yesno!,2) = 2 then
	return -1
end if

setnull(ls_cod_resp)

select cod_resp_divisione
into   :ls_cod_resp
from   mansionari
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_chiusura_nc = 'S';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_cod_resp) then
	g_mb.messagebox("OMNIA","Il mansionario corrente non è abilitato alla chiusura delle richieste di intervento")
	return -1
end if

ll_anno = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"anno_reg_richiesta")
ll_num = dw_richieste_det.getitemnumber(dw_richieste_det.getrow(),"num_reg_richiesta")
ldt_chiusura = datetime(today(),now())

update richieste
set    cod_resp_divisione = :ls_cod_resp,
       data_chiusura = :ldt_chiusura
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_reg_richiesta = :ll_anno and
		 num_reg_richiesta = :ll_num;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in chiusura richiesta di intervento.~nErrore nella update di richieste: " + sqlca.sqlerrtext)
	return -1
end if

ll_row = dw_richieste_lista.getrow()

dw_richieste_lista.triggerevent("pcd_retrieve")

dw_richieste_lista.setrow(ll_row)

dw_richieste_lista.scrolltorow(ll_row)
end event

