﻿$PBExportHeader$w_richieste_manutenzione.srw
$PBExportComments$Finestra Gestione Richieste Manutenzione
forward
global type w_richieste_manutenzione from w_richieste
end type
type cb_visite_isp from commandbutton within w_richieste_manutenzione
end type
type cb_ricerca_operaio from cb_operai_ricerca within w_richieste_manutenzione
end type
end forward

global type w_richieste_manutenzione from w_richieste
integer width = 2290
integer height = 1856
string title = "Richieste Manutenzione"
cb_visite_isp cb_visite_isp
cb_ricerca_operaio cb_ricerca_operaio
end type
global w_richieste_manutenzione w_richieste_manutenzione

type variables
boolean ib_in_new = FALSE, ib_open = true
end variables

forward prototypes
public function integer wf_tipo_richiesta (string ws_tipo_richiesta)
end prototypes

public function integer wf_tipo_richiesta (string ws_tipo_richiesta);dw_richieste_det.Modify("cod_tipo_manutenzione_t.Visible=0")
dw_richieste_det.Modify("cf_cod_tipo_manutenzione.Visible=0")
dw_richieste_det.Modify("cod_tipo_manutenzione.Visible=0")
dw_richieste_det.Modify("cod_attrezzatura_t.Visible=0")
dw_richieste_det.Modify("cf_attrezzatura.Visible=0")
dw_richieste_det.Modify("cod_attrezzatura.Visible=0")
dw_richieste_det.Modify("num_registrazione_t.Visible=0")
dw_richieste_det.Modify("num_registrazione.Visible=0")
dw_richieste_det.Modify("ret_1.Visible=0")

dw_richieste_det.Modify("cod_intervento_t.Visible=0")
dw_richieste_det.Modify("cf_cod_intervento.Visible=0")
dw_richieste_det.Modify("cod_intervento.Visible=0")
dw_richieste_det.Modify("num_reg_intervento_t.Visible=0")
dw_richieste_det.Modify("num_reg_intervento.Visible=0")
dw_richieste_det.Modify("anno_reg_intervento_t.Visible=0")
dw_richieste_det.Modify("anno_reg_intervento.Visible=0")
dw_richieste_det.Modify("ret_3.Visible=0")

dw_richieste_det.Modify("cod_intervento.Visible=0")
dw_richieste_det.Modify("num_reg_visita_t.Visible=0")
dw_richieste_det.Modify("num_reg_visita.Visible=0")
dw_richieste_det.Modify("anno_reg_visita_t.Visible=0")
dw_richieste_det.Modify("anno_reg_visita.Visible=0")
dw_richieste_det.Modify("ret_2.Visible=0")
choose case ws_tipo_richiesta
case "M"
   dw_richieste_det.Modify("cod_tipo_manutenzione_t.Visible=1")
   dw_richieste_det.Modify("cf_cod_tipo_manutenzione.Visible=1")
   dw_richieste_det.Modify("cod_tipo_manutenzione.Visible=1")
   dw_richieste_det.Modify("cod_attrezzatura_t.Visible=1")
   dw_richieste_det.Modify("cf_attrezzatura.Visible=1")
   dw_richieste_det.Modify("cod_attrezzatura.Visible=1")
   dw_richieste_det.Modify("num_registrazione_t.Visible=1")
   dw_richieste_det.Modify("num_registrazione.Visible=1")
   dw_richieste_det.Modify("ret_1.Visible=1")
case "I"
   dw_richieste_det.Modify("cod_intervento_t.Visible=1")
   dw_richieste_det.Modify("cf_cod_intervento.Visible=1")
   dw_richieste_det.Modify("cod_intervento.Visible=1")
   dw_richieste_det.Modify("num_reg_intervento_t.Visible=1")
   dw_richieste_det.Modify("num_reg_intervento.Visible=1")
   dw_richieste_det.Modify("anno_reg_intervento_t.Visible=1")
   dw_richieste_det.Modify("anno_reg_intervento.Visible=1")
   dw_richieste_det.Modify("ret_3.Visible=1")
case "V"
   dw_richieste_det.Modify("cod_intervento.Visible=1")
   dw_richieste_det.Modify("num_reg_visita_t.Visible=1")
   dw_richieste_det.Modify("num_reg_visita.Visible=1")
   dw_richieste_det.Modify("anno_reg_visita_t.Visible=1")
   dw_richieste_det.Modify("anno_reg_visita.Visible=1")
   dw_richieste_det.Modify("ret_2.Visible=1")
end choose
return 0

end function

event pc_setwindow;call super::pc_setwindow;dw_richieste_lista.set_dw_key("cod_azienda")

dw_richieste_lista.set_dw_key("flag_tipo_richiesta")

if (s_cs_xx.parametri.parametro_s_1 = "MANUTENZIONE") and &
   (s_cs_xx.parametri.parametro_d_1 > 0) and &
   (s_cs_xx.parametri.parametro_d_2 > 0)  then
	
	dw_richieste_lista.set_dw_options( sqlca, &
	                                   pcca.null_object, &
												  c_newonopen, &
												  c_default)

else

   dw_richieste_lista.set_dw_options( sqlca, &
	                                   pcca.null_object, &
												  c_default, &
												  c_default)

end if

dw_richieste_det.set_dw_options( sqlca, &
                                 dw_richieste_lista, &
											c_sharedata + c_scrollparent, &
											c_default)

iuo_dw_main = dw_richieste_lista

wf_tipo_richiesta("M")
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_richieste_det,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_richieste_det,"cod_intervento",sqlca,&
                 "tab_interventi","cod_intervento","des_intervento",&
                 "tab_interventi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on w_richieste_manutenzione.create
int iCurrent
call super::create
this.cb_visite_isp=create cb_visite_isp
this.cb_ricerca_operaio=create cb_ricerca_operaio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_visite_isp
this.Control[iCurrent+2]=this.cb_ricerca_operaio
end on

on w_richieste_manutenzione.destroy
call super::destroy
destroy(this.cb_visite_isp)
destroy(this.cb_ricerca_operaio)
end on

type dw_richieste_lista from w_richieste`dw_richieste_lista within w_richieste_manutenzione
event ue_last ( )
integer width = 2208
end type

event dw_richieste_lista::ue_last();setrow(rowcount())

scrolltorow(rowcount())
end event

event dw_richieste_lista::pcd_new;call super::pcd_new;if (s_cs_xx.parametri.parametro_s_1 = "MANUTENZIONE") and &
   (s_cs_xx.parametri.parametro_d_1 > 0) and &
   (s_cs_xx.parametri.parametro_d_2 > 0)  then
       this.setitem(this.getrow(),"anno_non_conf", s_cs_xx.parametri.parametro_d_1)
       this.setitem(this.getrow(),"num_non_conf", s_cs_xx.parametri.parametro_d_2)
end if

ib_in_new = true

this.SetItem(this.getrow(), "flag_tipo_richiesta", "M")

this.SetItem(this.getrow(), "data_richiesta", today())

this.SetItem(this.getrow(), "data_scadenza", relativedate(today(), 7))

cb_visite_isp.enabled = false

cb_ricerca_operaio.enabled = true

dw_richieste_det.object.b_ricerca_att.enabled = true

end event

event dw_richieste_lista::pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, "M")

IF l_Error < 0 THEN
	
   PCCA.Error = c_Fatal
	
END IF

if ib_open then
	
	postevent("ue_last")
	
	ib_open = false
	
end if
end event

event dw_richieste_lista::pcd_savebefore;call super::pcd_savebefore;setnull(s_cs_xx.parametri.parametro_s_1)

s_cs_xx.parametri.parametro_d_1 = 0

s_cs_xx.parametri.parametro_d_2 = 0

end event

event dw_richieste_lista::pcd_setkey;call super::pcd_setkey;long  l_idx, ll_anno_registrazione, ll_num_registrazione

for l_idx = 1 to rowcount()
	
   if isnull(getitemstring(l_idx, "cod_azienda")) then
		
      setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
		
   end if
	
   if isnull(getitemstring(l_idx, "flag_tipo_richiesta")) then
		
      setitem(l_idx, "flag_tipo_richiesta", "m")
		
   end if
	
	ll_anno_registrazione = f_anno_esercizio()
	
   if isnull(getitemnumber(l_idx, "anno_reg_richiesta")) or getitemnumber(l_idx, "anno_reg_richiesta") < 1 then
	   
		setitem(l_idx, "anno_reg_richiesta", ll_anno_registrazione)

	end if
	
   if isnull(getitemnumber(l_idx, "num_reg_richiesta")) or getitemnumber(l_idx, "num_reg_richiesta") < 1 then
	
		ll_num_registrazione = 0
		
	   select max(num_reg_richiesta)
   	into   :ll_num_registrazione
	   from   richieste
   	where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_reg_richiesta = :ll_anno_registrazione;
				 
		if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
			
			ll_num_registrazione = 1
			
		else
			
			ll_num_registrazione ++
			
		end if
		
		setitem(getrow(), "num_reg_richiesta", ll_num_registrazione)
		
	end if
	
next

end event

event dw_richieste_lista::pcd_modify;call super::pcd_modify;ib_in_new = false

cb_visite_isp.enabled = false
cb_ricerca_operaio.enabled = true
dw_richieste_det.object.b_ricerca_att.enabled = true

end event

event dw_richieste_lista::pcd_view;call super::pcd_view;ib_in_new = false

cb_visite_isp.enabled = true

cb_ricerca_operaio.enabled = false

dw_richieste_det.object.b_ricerca_att.enabled = false
end event

event dw_richieste_lista::pcd_save;call super::pcd_save;cb_visite_isp.enabled = true
dw_richieste_det.object.b_ricerca_att.enabled = false
end event

type dw_richieste_det from w_richieste`dw_richieste_det within w_richieste_manutenzione
integer width = 2213
integer height = 1100
integer taborder = 30
end type

event dw_richieste_det::itemchanged;call super::itemchanged;if i_extendmode then

	string ls_null
	if i_colname = "cod_attrezzatura" then
		setnull(ls_null)
		this.setitem(this.getrow(),"cod_tipo_manutenzione", ls_null)
		f_PO_LoadDDDW_DW(dw_richieste_det,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "(tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and "  + &
                 "(tab_tipi_manutenzione.cod_attrezzatura = '" + i_coltext + "')")
	end if
end if
end event

event dw_richieste_det::buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_richieste_det,"cod_attrezzatura")
end choose
end event

type cb_visite_isp from commandbutton within w_richieste_manutenzione
integer x = 1865
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Manutenz."
end type

event clicked;long ll_row


ll_row = dw_richieste_lista.getrow()

s_cs_xx.parametri.parametro_s_1 = dw_richieste_det.getitemstring(ll_row,"cod_tipo_manutenzione")
s_cs_xx.parametri.parametro_s_2 = dw_richieste_det.getitemstring(ll_row,"cod_attrezzatura")
s_cs_xx.parametri.parametro_s_3 = dw_richieste_det.getitemstring(ll_row,"cod_operaio")
s_cs_xx.parametri.parametro_s_4 = dw_richieste_det.getitemstring(ll_row,"note")
s_cs_xx.parametri.parametro_d_1 = dw_richieste_det.getitemnumber(ll_row,"anno_reg_richiesta")
s_cs_xx.parametri.parametro_d_2 = dw_richieste_det.getitemnumber(ll_row,"num_reg_richiesta")
s_cs_xx.parametri.parametro_d_3 = dw_richieste_det.getitemnumber(ll_row,"anno_non_conf")
s_cs_xx.parametri.parametro_d_4 = dw_richieste_det.getitemnumber(ll_row,"num_non_conf")

window_open(w_conferma_manutenzione, 0)

dw_richieste_lista.triggerevent("pcd_retrieve")

dw_richieste_lista.setrow(ll_row)
dw_richieste_lista.scrolltorow(ll_row)
end event

type cb_ricerca_operaio from cb_operai_ricerca within w_richieste_manutenzione
integer x = 2144
integer y = 812
integer width = 78
integer height = 72
integer taborder = 12
boolean bringtotop = true
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_1 = dw_richieste_det
s_cs_xx.parametri.parametro_s_1 = "cod_operaio"

end event

