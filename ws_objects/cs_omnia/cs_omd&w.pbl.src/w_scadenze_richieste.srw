﻿$PBExportHeader$w_scadenze_richieste.srw
$PBExportComments$Finestra Standard Scadenzario Richieste
forward
global type w_scadenze_richieste from w_cs_xx_principale
end type
type dw_scadenze_richieste from uo_cs_xx_dw within w_scadenze_richieste
end type
type st_1 from statictext within w_scadenze_richieste
end type
type em_1 from editmask within w_scadenze_richieste
end type
type st_2 from statictext within w_scadenze_richieste
end type
type em_2 from editmask within w_scadenze_richieste
end type
end forward

global type w_scadenze_richieste from w_cs_xx_principale
int Width=2890
int Height=1257
boolean TitleBar=true
string Title="Scadenzario Richieste"
dw_scadenze_richieste dw_scadenze_richieste
st_1 st_1
em_1 em_1
st_2 st_2
em_2 em_2
end type
global w_scadenze_richieste w_scadenze_richieste

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_scadenze_richieste.set_dw_options(sqlca,pcca.null_object,c_nonew+c_nodelete+c_nomodify,c_default)

iuo_dw_main = dw_scadenze_richieste

em_1.text = string(today(), "dd/mm/yyyy")
em_2.text   = string(relativedate(today(), 30), "dd/mm/yyyy")


end on

on w_scadenze_richieste.create
int iCurrent
call w_cs_xx_principale::create
this.dw_scadenze_richieste=create dw_scadenze_richieste
this.st_1=create st_1
this.em_1=create em_1
this.st_2=create st_2
this.em_2=create em_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_scadenze_richieste
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=em_1
this.Control[iCurrent+4]=st_2
this.Control[iCurrent+5]=em_2
end on

on w_scadenze_richieste.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_scadenze_richieste)
destroy(this.st_1)
destroy(this.em_1)
destroy(this.st_2)
destroy(this.em_2)
end on

type dw_scadenze_richieste from uo_cs_xx_dw within w_scadenze_richieste
int X=23
int Y=21
int Width=2812
int Height=1001
int TabOrder=10
string DataObject="d_scadenze_richieste"
BorderStyle BorderStyle=StyleLowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
datetime ld_data_inizio, ld_data_fine

ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))
l_Error = Retrieve(s_cs_xx.cod_azienda, "M", ld_data_inizio, ld_data_fine)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type st_1 from statictext within w_scadenze_richieste
int X=23
int Y=1041
int Width=293
int Height=73
boolean Enabled=false
string Text="Data Inizio:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_1 from editmask within w_scadenze_richieste
int X=321
int Y=1041
int Width=435
int Height=81
int TabOrder=20
Alignment Alignment=Center!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
double Increment=1
string MinMax="01/01/1900~~31/12/2999"
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_scadenze_richieste
int X=801
int Y=1041
int Width=270
int Height=73
boolean Enabled=false
string Text="Data Fine:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_2 from editmask within w_scadenze_richieste
int X=1075
int Y=1041
int Width=435
int Height=81
int TabOrder=30
Alignment Alignment=Center!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
double Increment=1
string MinMax="01/01/1900~~31/12/2999"
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

