﻿$PBExportHeader$w_schede_intervento.srw
$PBExportComments$Finestra Schede Intervento
forward
global type w_schede_intervento from w_cs_xx_principale
end type
type cb_documento from commandbutton within w_schede_intervento
end type
type cb_invia from commandbutton within w_schede_intervento
end type
type mle_stato from multilineedit within w_schede_intervento
end type
type cb_cerca from commandbutton within w_schede_intervento
end type
type cb_annulla from commandbutton within w_schede_intervento
end type
type cb_ricerca_operaio from cb_operai_ricerca within w_schede_intervento
end type
type cb_tes_campionamenti from commandbutton within w_schede_intervento
end type
type cb_1 from commandbutton within w_schede_intervento
end type
type cb_doc_comp from cb_documenti_compilati within w_schede_intervento
end type
type cb_ric_prod from cb_prod_ricerca within w_schede_intervento
end type
type cb_ric_stock from cb_stock_ricerca within w_schede_intervento
end type
type cb_cerca_az_corr from cb_ricerca_az_corr within w_schede_intervento
end type
type cb_chiusura from commandbutton within w_schede_intervento
end type
type cb_controllo from commandbutton within w_schede_intervento
end type
type cb_comunicazioni from commandbutton within w_schede_intervento
end type
type cb_respingi from commandbutton within w_schede_intervento
end type
type tab_folder from tab within w_schede_intervento
end type
type tabpage_anagrafici from userobject within tab_folder
end type
type tabpage_anagrafici from userobject within tab_folder
end type
type tabpage_generale from userobject within tab_folder
end type
type tabpage_generale from userobject within tab_folder
end type
type tabpage_costi from userobject within tab_folder
end type
type tabpage_costi from userobject within tab_folder
end type
type tabpage_date from userobject within tab_folder
end type
type tabpage_date from userobject within tab_folder
end type
type tabpage_reso from userobject within tab_folder
end type
type tabpage_reso from userobject within tab_folder
end type
type tab_folder from tab within w_schede_intervento
tabpage_anagrafici tabpage_anagrafici
tabpage_generale tabpage_generale
tabpage_costi tabpage_costi
tabpage_date tabpage_date
tabpage_reso tabpage_reso
end type
type dw_schede_intervento_det_1 from uo_cs_xx_dw within w_schede_intervento
end type
type dw_schede_intervento_sel from u_dw_search within w_schede_intervento
end type
type dw_folder from u_folder within w_schede_intervento
end type
type dw_schede_intervento_lista from uo_cs_xx_dw within w_schede_intervento
end type
type dw_schede_intervento_det_2 from uo_cs_xx_dw within w_schede_intervento
end type
type dw_schede_intervento_det_3 from uo_cs_xx_dw within w_schede_intervento
end type
type dw_schede_intervento_det_4 from uo_cs_xx_dw within w_schede_intervento
end type
type dw_schede_intervento_det_5 from uo_cs_xx_dw within w_schede_intervento
end type
end forward

global type w_schede_intervento from w_cs_xx_principale
integer width = 2907
integer height = 2448
string title = "Interventi"
event ue_cerca_riga ( )
cb_documento cb_documento
cb_invia cb_invia
mle_stato mle_stato
cb_cerca cb_cerca
cb_annulla cb_annulla
cb_ricerca_operaio cb_ricerca_operaio
cb_tes_campionamenti cb_tes_campionamenti
cb_1 cb_1
cb_doc_comp cb_doc_comp
cb_ric_prod cb_ric_prod
cb_ric_stock cb_ric_stock
cb_cerca_az_corr cb_cerca_az_corr
cb_chiusura cb_chiusura
cb_controllo cb_controllo
cb_comunicazioni cb_comunicazioni
cb_respingi cb_respingi
tab_folder tab_folder
dw_schede_intervento_det_1 dw_schede_intervento_det_1
dw_schede_intervento_sel dw_schede_intervento_sel
dw_folder dw_folder
dw_schede_intervento_lista dw_schede_intervento_lista
dw_schede_intervento_det_2 dw_schede_intervento_det_2
dw_schede_intervento_det_3 dw_schede_intervento_det_3
dw_schede_intervento_det_4 dw_schede_intervento_det_4
dw_schede_intervento_det_5 dw_schede_intervento_det_5
end type
global w_schede_intervento w_schede_intervento

type variables
boolean ib_in_new=false, ib_visualizza_campi = false
boolean stato=false, ib_aggiorna_richiesta=false
boolean ib_bol_acq=false, ib_fat_ven=false
long    il_anno, il_num, il_row = 0

string is_flag_BNC = "N"

end variables

forward prototypes
public function integer wf_genera_campionamento (ref long fl_anno_reg_campionamento, ref long fl_num_reg_campionamento)
public function integer wf_loaddddw_dw (datawindow dddw_dw, string dddw_column, transaction trans_object, string table_name, string column_code, string column_desc, string join_clause, string where_clause)
end prototypes

event ue_cerca_riga();long    ll_row

boolean lb_trovato


for ll_row = 1 to dw_schede_intervento_lista.rowcount()
	if dw_schede_intervento_lista.getitemnumber(ll_row,"anno_reg_intervento") = il_anno and &
		dw_schede_intervento_lista.getitemnumber(ll_row,"num_reg_intervento") = il_num then
		lb_trovato = true
		exit
	end if
next

if lb_trovato then
	dw_schede_intervento_lista.setrow(ll_row)
	dw_schede_intervento_lista.scrolltorow(ll_row)
end if
end event

public function integer wf_genera_campionamento (ref long fl_anno_reg_campionamento, ref long fl_num_reg_campionamento);integer li_row
string ls_cod_prodotto, ls_cod_piano_campionamento, ls_cod_deposito, ls_cod_ubicazione, &
		 ls_cod_lotto, ls_note
datetime ldt_data_stock, ldt_data_validita
long ll_prog_stock, ll_num_reg_campionamenti, ll_anno_reg_campionamenti, ll_anno_schede_intervento, &
	  ll_num_schede_intervento

li_row = dw_schede_intervento_lista.getrow()
ls_cod_prodotto = dw_schede_intervento_lista.getitemstring(li_row, "cod_prodotto")

declare cur_piani_camp cursor for
  select tes_piani_campionamento.cod_piano_campionamento, 
		   tes_piani_campionamento.data_validita
    from tes_piani_campionamento,
         det_piani_campionamento
   where ( det_piani_campionamento.cod_azienda = tes_piani_campionamento.cod_azienda ) and
         ( det_piani_campionamento.cod_piano_campionamento = tes_piani_campionamento.cod_piano_campionamento ) and
         ( det_piani_campionamento.data_validita = tes_piani_campionamento.data_validita ) and
         ( ( tes_piani_campionamento.cod_azienda = :s_cs_xx.cod_azienda ) and
         ( det_piani_campionamento.cod_prodotto = :ls_cod_prodotto ) )   ;

open cur_piani_camp;														
fetch cur_piani_camp into :ls_cod_piano_campionamento, :ldt_data_validita;
if sqlca.sqlcode = 100 then
	messagebox("Creazione Campionamento da Schede Intervento", "Nessun Piano di Campionamento legato al Prodotto Corrente")
	close cur_piani_camp;
	return -1
elseif sqlca.sqlcode = -1 then
	messagebox("Creazione Campionamento da Schede Intervento", "Errore durante l'estrazione del Piano di Campionamento")
	close cur_piani_camp;
	return -1
end if

close cur_piani_camp;

ls_cod_deposito = dw_schede_intervento_lista.getitemstring(li_row, "cod_deposito")
ls_cod_ubicazione = dw_schede_intervento_lista.getitemstring(li_row, "cod_ubicazione")
ls_cod_lotto = dw_schede_intervento_lista.getitemstring(li_row, "cod_lotto")
ldt_data_stock = dw_schede_intervento_lista.getitemdatetime(li_row, "data_stock")
ll_prog_stock = dw_schede_intervento_lista.getitemnumber(li_row, "prog_stock")

if isnull(ls_cod_deposito) or isnull(ls_cod_ubicazione) or isnull(ls_cod_lotto) or &
	isnull(ldt_data_stock) or isnull(ll_prog_stock) then
	messagebox("Creazione Campionamento da Schede Intervento", "E' necessario definire lo STOCK!")
	return -1
end if

ll_anno_reg_campionamenti = year(today())

select max(num_reg_campionamenti) 
into:ll_num_reg_campionamenti 
from tes_campionamenti 
where cod_azienda=:s_cs_xx.cod_azienda 
and anno_reg_campionamenti=:ll_anno_reg_campionamenti; 

if isnull(ll_num_reg_campionamenti) then
	ll_num_reg_campionamenti = 1
else
	ll_num_reg_campionamenti++
end if

ll_anno_schede_intervento = dw_schede_intervento_lista.getitemnumber(li_row, "anno_reg_intervento")
ll_num_schede_intervento = dw_schede_intervento_lista.getitemnumber(li_row, "num_reg_intervento")
ls_note = "Rif.Acc.Mat.: " + string(ll_anno_schede_intervento) + "/" + string(ll_num_schede_intervento)

window_open(w_ext_dati_campionamento, 0)

//if isnull(s_cs_xx.parametri.parametro_s_1) or &
//	isnull(s_cs_xx.parametri.parametro_s_2) or &
if	isnull(s_cs_xx.parametri.parametro_data_1) then
	messagebox("Creazione Campionamento da Schede Intervento", "Operazione annullata")
	return -1
end if

insert into tes_campionamenti
		( cod_azienda,
		  anno_reg_campionamenti,
		  num_reg_campionamenti,
		  des_campionamento,
		  data_registrazione,
		  cod_piano_campionamento,
		  data_validita,
		  cod_prodotto,
		  cod_deposito,
		  cod_ubicazione,
		  cod_lotto,
		  data_stock,
		  prog_stock,
		  cod_operaio,
		  livello_significativita,
		  note )  
values (:s_cs_xx.cod_azienda,   
		  :ll_anno_reg_campionamenti,   
		  :ll_num_reg_campionamenti,   
		  :s_cs_xx.parametri.parametro_s_1,   
		  :s_cs_xx.parametri.parametro_data_1,   
		  :ls_cod_piano_campionamento,   
		  :ldt_data_validita,   
		  :ls_cod_prodotto,   
		  :ls_cod_deposito,   
		  :ls_cod_ubicazione,   
		  :ls_cod_lotto,   
		  :ldt_data_stock,   
		  :ll_prog_stock,   
		  :s_cs_xx.parametri.parametro_s_2,   
		  0,   
		  :ls_note )  ;

if sqlca.sqlcode <> 0 then
	messagebox("Creazione Campionamento da Schede Intervento", "Errore Durante l'Inserimento")
	rollback;
	return -1
end if

update schede_intervento
  set anno_reg_campionamenti = :ll_anno_reg_campionamenti,   
		num_reg_campionamenti = :ll_num_reg_campionamenti
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_reg_intervento = :ll_anno_schede_intervento and
		num_reg_intervento = :ll_num_schede_intervento;

if sqlca.sqlcode <> 0 then
	messagebox("Creazione Campionamento da Schede Intervento", "Errore Durante l'Aggiornamento Accettazione")
	rollback;
	return -1
end if

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
setnull(s_cs_xx.parametri.parametro_data_1)
setnull(s_cs_xx.parametri.parametro_data_2)

messagebox("Creazione Campionamento da Schede Intervento", "Creato Campionamento: " + &
				string(ll_anno_reg_campionamenti) + "/" + string(ll_num_reg_campionamenti))

fl_anno_reg_campionamento = ll_anno_reg_campionamenti
fl_num_reg_campionamento = ll_num_reg_campionamenti

return 0
end function

public function integer wf_loaddddw_dw (datawindow dddw_dw, string dddw_column, transaction trans_object, string table_name, string column_code, string column_desc, string join_clause, string where_clause);INTEGER          l_ColNbr,     l_Idx,      l_Jdx
INTEGER          l_ColPos,     l_Start,    l_Count
LONG             l_Error
STRING           l_SQLString,  l_ErrStr
STRING           l_dwDescribe, l_dwModify, l_Attrib, l_ColStr
DATAWINDOWCHILD  l_DWC

STRING           c_ColAttrib[]  =            &
                    { ".BackGround.Color",   & 
                      ".Background.Mode",    &
                      ".Border",             &
                      ".Color",              &
                      ".Font.CharSet",       &
                      ".Font.Face",          &
                      ".Font.Family",        &
                      ".Font.Height",        &
                      ".Font.Italic",        &
                      ".Font.Pitch",         &
                      ".Font.Strikethrough", &
                      ".Font.Underline",     &
                      ".Font.Weight",        &
                      ".Font.Width",         &
                      ".Height",             &
                      ".Width" }

//------------------------------------------------------------------
//  Get the number and name of the DDDW column.
//------------------------------------------------------------------

l_dwDescribe = DDDW_Column + ".ID"
l_ColNbr     = Integer(DDDW_DW.Describe(l_dwDescribe))
l_dwDescribe = "#" + String(l_ColNbr) + ".Name"
l_ColStr     = DDDW_DW.Describe(l_dwDescribe)

//****** Modifica Michele per far funzionare la select di cognome e nome anche su Oracle - 21/09/2001 *******

long   ll_return, ll_pos
string ls_db

if ll_return = -1 then
	g_mb.messagebox("f_po_loaddddw_dw","Errore nella lettura del profilo corrente dal registro")
	return -1
end if

ll_return = Registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "enginetype", ls_db)

if ll_return = -1 then
	g_mb.messagebox("f_po_loaddddw_dw","Errore nella lettura del motore database dal registro")
	return -1
end if

if ls_db = "ORACLE" then
	
	do
		
		ll_pos = pos(Column_Desc,"+",1)
		
		if ll_pos > 0 then
			Column_Desc = replace(Column_Desc,ll_pos,1,"||")
		end if	
	
	loop while ll_pos > 0
	
end if

//******************************************** Fine modifica ************************************************

	
//------------------------------------------------------------------
//  Build the SQL SELECT statement.
//------------------------------------------------------------------

l_SQLString = "SELECT DISTINCT " + Column_Code + " , " + &
				  Column_Desc + " FROM " + Table_Name
				  
if Trim(join_clause) <> "" then
	l_SQLString = l_SQLString + " " + join_clause
end if

IF Trim(Where_Clause) <> "" THEN
	l_SQLString = l_SQLString + " WHERE " + Where_Clause
END IF

// ------------------------------------------------------------------
//   Aggiunto da EnMe per fare Sort su colonna codice
// ------------------------------------------------------------------

l_SQLString = l_SQLString + " ORDER BY " + Column_Code
	
//------------------------------------------------------------------
//  Get the child DataWindow.  If column is not a DDDW column,
//  then return with error.  Otherwise, we need to set the
//  Select statement in the drop down DataWindow.
//------------------------------------------------------------------

l_Error = DDDW_DW.GetChild(l_ColStr, l_DWC)

IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWChildFindError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Set the transaction object.
//------------------------------------------------------------------

l_Error = l_DWC.SetTransObject(trans_object)

IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWTransactionError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Assign the Select statement to the drop down DataWindow.
//------------------------------------------------------------------

l_dwModify = "DataWindow.Table.Select='" + &
             w_POManager_STD.MB.fu_QuoteChar(l_SQLString, "'")  + "'"
l_ErrStr   = l_DWC.Modify(l_dwModify)

IF l_ErrStr <> "" THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWModifySQLError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Retrieve the data for the child DataWindow.
//------------------------------------------------------------------

l_Error = l_DWC.Retrieve()
IF l_Error < 0 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWRetrieveError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//---------- Modifica Michele per togliere spazi dalla lista 04/03/2003 ------------
long ll_i

for ll_i = 1 to l_DWC.rowcount()
	l_DWC.setitem(ll_i,"display_column",trim(l_DWC.getitemstring(ll_i,"display_column")))
next
//------------- Fine Modifica Michele per togliere spazi dalla lista ---------------

//------------------------------------------------------------------
//  Set the attributes of the column to be the same as the
//  parent column.
//------------------------------------------------------------------

l_Jdx = UpperBound(c_ColAttrib[])
FOR l_Idx = 1 TO l_Jdx

   l_dwDescribe = l_ColStr + c_ColAttrib[l_Idx]
   l_Attrib     = DDDW_DW.Describe(l_dwDescribe)

   IF c_ColAttrib[l_Idx] = ".Font.Face" THEN
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "Arial"
      END IF
      l_Attrib = "'" + l_Attrib + "'"
   ELSE
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "0"
      END IF
   END IF

   l_dwModify = "#1" + c_ColAttrib[l_Idx] + "=" + l_Attrib
   l_ErrStr   = l_DWC.Modify(l_dwModify)

NEXT

//------------------------------------------------------------------
//  Make sure the border for the child column is off.
//------------------------------------------------------------------

l_dwModify = "#1" + ".Border=0"
l_ErrStr   = l_DWC.Modify(l_dwModify)

//------------------------------------------------------------------
//  Indicate that there not any errors.
//------------------------------------------------------------------

string ls_Required

ls_Required = dddw_dw.Describe(dddw_column + ".DDDW.Required")

IF ls_Required = "no" THEN
   l_DWC.InsertRow(1)
END IF

RETURN 0
end function

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_schede_intervento_det_1,"cod_errore",sqlca,&
                 "tab_difformita","cod_errore","des_difformita",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_schede_intervento_det_1,"cod_trattamento",sqlca,&
                 "tab_trattamenti","cod_trattamento","des_trattamento",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_schede_intervento_det_1,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_schede_intervento_det_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_schede_intervento_det_1,"cod_intervento",sqlca,&
                 "tab_interventi","cod_intervento","des_intervento",&
                 "tab_interventi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_schede_intervento_det_1,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_schede_intervento_det_2,"cod_valuta",sqlca,&
                 "tab_valute","cod_valuta","des_valuta", &
                 "tab_valute.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_schede_intervento_det_2,"cod_centro_costo",sqlca,&
                 "tab_centri_costo","cod_centro_costo","des_centro_costo", &
                 "tab_centri_costo.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//Donato 26-11-2008 -------------------------------------------
if s_cs_xx.cod_utente <> "CS_SYSTEM" and is_flag_BNC = "S" then
	wf_loaddddw_dw( dw_schede_intervento_det_1, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     			"des_tipo_causa", &
							"as b "+&
								"join (select cod_azienda,cod_tipo_lista_dist,cod_lista_dist,"+&
												"cod_destinatario,min(num_sequenza) as num_sequenza "+&
											"from det_liste_dist_destinatari "+&
											"where cod_destinatario_mail = '"+s_cs_xx.cod_utente+"' "+&
											"group by cod_azienda,cod_tipo_lista_dist,cod_lista_dist,cod_destinatario "+&
										") as a on a.cod_tipo_lista_dist=b.cod_tipo_lista_dist "+&
															"and b.cod_lista_dist=a.cod_lista_dist ",&
							"b.cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((b.flag_blocco <> 'S') or (b.flag_blocco = 'S' and b.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
else
	f_PO_LoadDDDW_DW( dw_schede_intervento_det_1, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end if
/*
f_PO_LoadDDDW_DW( dw_schede_intervento_det_1, &
							"cod_tipo_causa", &
							sqlca, &
							"tab_tipi_cause", &
							"cod_tipo_causa", &
                     "des_tipo_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
*/
//fine modifica -------------------------------------
f_PO_LoadDDDW_DW( dw_schede_intervento_det_1, &
							"cod_causa", &
							sqlca, &
							"cause", &
							"cod_causa", &
                     "des_causa", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


if ib_bol_acq then
   f_PO_LoadDDLB_DW(dw_schede_intervento_det_3,"anno_bolla_acq",sqlca,&
                    "tes_bol_acq","anno_bolla_acq","anno_bolla_acq",&
                    "(tes_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "')" )
end if

f_PO_LoadDDDW_DW(dw_schede_intervento_det_4,"cod_errore",sqlca,&
                 "tab_difformita","cod_errore","des_difformita",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_schede_intervento_det_5,"cod_agente",sqlca,&
                 "anag_agenti","cod_agente","rag_soc_1",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
					  
f_PO_LoadDDDW_DW(dw_schede_intervento_sel,"cod_area",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
					  					  
end event

event pc_setwindow;call super::pc_setwindow;string ls_flag_rif_ord, ls_flag_rif_bol, ls_flag_rif_fat, ls_proj, ls_parametro

windowobject l_objects[]


il_anno = s_cs_xx.parametri.parametro_d_1

il_num = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_1)

setnull(s_cs_xx.parametri.parametro_d_2)

dw_schede_intervento_lista.set_dw_key("cod_azienda")
dw_schede_intervento_lista.set_dw_options(sqlca, pcca.null_object, c_noretrieveonopen, c_default)
dw_schede_intervento_det_1.set_dw_options(sqlca, dw_schede_intervento_lista, c_sharedata+c_scrollparent,c_default)
dw_schede_intervento_det_2.set_dw_options(sqlca, dw_schede_intervento_lista, c_sharedata+c_scrollparent,c_default)
dw_schede_intervento_det_3.set_dw_options(sqlca, dw_schede_intervento_lista, c_sharedata+c_scrollparent,c_default)
dw_schede_intervento_det_4.set_dw_options(sqlca, dw_schede_intervento_lista, c_sharedata+c_scrollparent,c_default)
dw_schede_intervento_det_5.set_dw_options(sqlca, dw_schede_intervento_lista, c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_schede_intervento_lista

SELECT parametri_omnia.flag  
INTO   :ls_flag_rif_bol
FROM   parametri_omnia  
WHERE  parametri_omnia.cod_azienda = :s_cs_xx.cod_azienda AND  
       parametri_omnia.flag_parametro = 'F' AND  
       parametri_omnia.cod_parametro = 'GBA';
		 
choose case sqlca.sqlcode
   case 100
	   messagebox("ERRORE", "Attenzione!! Non è stato caricato il parametro GBA in parametri azienda", StopSign!)   
	case -1
	   messagebox("ERRORE", sqlca.sqlerrtext + "DURANTE RICERCA PARAMETRO GBA IN PARAMETRI AZIENDA", StopSign!)   
end choose

if ls_flag_rif_bol = "S" then
   ib_bol_acq = true
else
   ib_bol_acq = false
end if

SELECT parametri_omnia.flag  
INTO   :ls_flag_rif_fat
FROM   parametri_omnia  
WHERE  parametri_omnia.cod_azienda = :s_cs_xx.cod_azienda AND  
       parametri_omnia.flag_parametro = 'F' AND  
       parametri_omnia.cod_parametro = 'GFV';
		 
choose case sqlca.sqlcode
   case 100
	   messagebox("ERRORE", "Attenzione!! Non è stato caricato il parametro GFV in parametri azienda", StopSign!)   
	case -1
	   messagebox("ERRORE", sqlca.sqlerrtext + "DURANTE RICERCA PARAMETRO GFA IN PARAMETRI AZIENDA", StopSign!)   
end choose

if ls_flag_rif_fat = "S" then
   ib_fat_ven = true
else
   ib_fat_ven = false
end if

if ib_bol_acq  then
   dw_schede_intervento_det_3.modify("anno_bolla_acq.visible = 1")
   dw_schede_intervento_det_3.modify("anno_bolla_acq_t.visible = 1")
   dw_schede_intervento_det_3.modify("num_bolla_acq.visible = 1")
   dw_schede_intervento_det_3.modify("num_bolla_acq_t.visible = 1")
   dw_schede_intervento_det_3.modify("prog_riga_bolla_acq.visible = 1")
   dw_schede_intervento_det_3.modify("prog_riga_bolla_acq_t.visible = 1")
   dw_schede_intervento_det_3.modify("rif_bol_acq.visible = 0")
   dw_schede_intervento_det_3.modify("rif_bol_acq_t.visible = 0")
else  
   dw_schede_intervento_det_3.modify("anno_bolla_acq.visible = 0")
   dw_schede_intervento_det_3.modify("anno_bolla_acq_t.visible = 0")
   dw_schede_intervento_det_3.modify("num_bolla_acq.visible = 0")
   dw_schede_intervento_det_3.modify("num_bolla_acq_t.visible = 0")
   dw_schede_intervento_det_3.modify("prog_riga_bolla_acq.visible = 0")
   dw_schede_intervento_det_3.modify("prog_riga_bolla_acq_t.visible = 0")
   dw_schede_intervento_det_3.modify("rif_bol_acq.visible = 1")
   dw_schede_intervento_det_3.modify("rif_bol_acq_t.visible = 1")
end if

if ib_fat_ven  then
   dw_schede_intervento_det_3.modify("anno_registrazione.visible = 1")
   dw_schede_intervento_det_3.modify("anno_registrazione_t.visible = 1")
   dw_schede_intervento_det_3.modify("num_registrazione.visible = 1")
   dw_schede_intervento_det_3.modify("num_registrazione_t.visible = 1")
   dw_schede_intervento_det_3.modify("prog_riga_fat_ven.visible = 1")
   dw_schede_intervento_det_3.modify("prog_riga_fat_ven_t.visible = 1")
   dw_schede_intervento_det_3.modify("rif_fat_ven.visible = 0")
   dw_schede_intervento_det_3.modify("rif_fat_ven_t.visible = 0")
else  
   dw_schede_intervento_det_3.modify("anno_registrazione.visible = 0")
   dw_schede_intervento_det_3.modify("anno_registrazione_t.visible = 0")
   dw_schede_intervento_det_3.modify("num_registrazione.visible = 0")
   dw_schede_intervento_det_3.modify("num_registrazione_t.visible = 0")
   dw_schede_intervento_det_3.modify("prog_riga_fat_ven.visible = 0")
   dw_schede_intervento_det_3.modify("prog_riga_fat_ven_t.visible = 0")
   dw_schede_intervento_det_3.modify("rif_fat_ven.visible = 1")
   dw_schede_intervento_det_3.modify("rif_fat_ven_t.visible = 1")
end if

select flag
into   :ls_parametro
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CCR';
	
ib_visualizza_campi = false	
if sqlca.sqlcode = 0 and not isnull(ls_parametro) and ls_parametro <> "" and ls_parametro = "S" then	
	ib_visualizza_campi = true
end if

dw_folder.fu_folderoptions(dw_folder.c_defaultheight,dw_folder.c_foldertableft)

l_objects[1] = dw_schede_intervento_lista
dw_folder.fu_assigntab(1, "L.",l_objects)

l_objects[1] = dw_schede_intervento_sel
l_objects[2] = cb_ricerca_operaio
dw_folder.fu_assigntab(2,"R.",l_objects)

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(2)

cb_annulla.postevent("clicked")

end event

on w_schede_intervento.create
int iCurrent
call super::create
this.cb_documento=create cb_documento
this.cb_invia=create cb_invia
this.mle_stato=create mle_stato
this.cb_cerca=create cb_cerca
this.cb_annulla=create cb_annulla
this.cb_ricerca_operaio=create cb_ricerca_operaio
this.cb_tes_campionamenti=create cb_tes_campionamenti
this.cb_1=create cb_1
this.cb_doc_comp=create cb_doc_comp
this.cb_ric_prod=create cb_ric_prod
this.cb_ric_stock=create cb_ric_stock
this.cb_cerca_az_corr=create cb_cerca_az_corr
this.cb_chiusura=create cb_chiusura
this.cb_controllo=create cb_controllo
this.cb_comunicazioni=create cb_comunicazioni
this.cb_respingi=create cb_respingi
this.tab_folder=create tab_folder
this.dw_schede_intervento_det_1=create dw_schede_intervento_det_1
this.dw_schede_intervento_sel=create dw_schede_intervento_sel
this.dw_folder=create dw_folder
this.dw_schede_intervento_lista=create dw_schede_intervento_lista
this.dw_schede_intervento_det_2=create dw_schede_intervento_det_2
this.dw_schede_intervento_det_3=create dw_schede_intervento_det_3
this.dw_schede_intervento_det_4=create dw_schede_intervento_det_4
this.dw_schede_intervento_det_5=create dw_schede_intervento_det_5
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_documento
this.Control[iCurrent+2]=this.cb_invia
this.Control[iCurrent+3]=this.mle_stato
this.Control[iCurrent+4]=this.cb_cerca
this.Control[iCurrent+5]=this.cb_annulla
this.Control[iCurrent+6]=this.cb_ricerca_operaio
this.Control[iCurrent+7]=this.cb_tes_campionamenti
this.Control[iCurrent+8]=this.cb_1
this.Control[iCurrent+9]=this.cb_doc_comp
this.Control[iCurrent+10]=this.cb_ric_prod
this.Control[iCurrent+11]=this.cb_ric_stock
this.Control[iCurrent+12]=this.cb_cerca_az_corr
this.Control[iCurrent+13]=this.cb_chiusura
this.Control[iCurrent+14]=this.cb_controllo
this.Control[iCurrent+15]=this.cb_comunicazioni
this.Control[iCurrent+16]=this.cb_respingi
this.Control[iCurrent+17]=this.tab_folder
this.Control[iCurrent+18]=this.dw_schede_intervento_det_1
this.Control[iCurrent+19]=this.dw_schede_intervento_sel
this.Control[iCurrent+20]=this.dw_folder
this.Control[iCurrent+21]=this.dw_schede_intervento_lista
this.Control[iCurrent+22]=this.dw_schede_intervento_det_2
this.Control[iCurrent+23]=this.dw_schede_intervento_det_3
this.Control[iCurrent+24]=this.dw_schede_intervento_det_4
this.Control[iCurrent+25]=this.dw_schede_intervento_det_5
end on

on w_schede_intervento.destroy
call super::destroy
destroy(this.cb_documento)
destroy(this.cb_invia)
destroy(this.mle_stato)
destroy(this.cb_cerca)
destroy(this.cb_annulla)
destroy(this.cb_ricerca_operaio)
destroy(this.cb_tes_campionamenti)
destroy(this.cb_1)
destroy(this.cb_doc_comp)
destroy(this.cb_ric_prod)
destroy(this.cb_ric_stock)
destroy(this.cb_cerca_az_corr)
destroy(this.cb_chiusura)
destroy(this.cb_controllo)
destroy(this.cb_comunicazioni)
destroy(this.cb_respingi)
destroy(this.tab_folder)
destroy(this.dw_schede_intervento_det_1)
destroy(this.dw_schede_intervento_sel)
destroy(this.dw_folder)
destroy(this.dw_schede_intervento_lista)
destroy(this.dw_schede_intervento_det_2)
destroy(this.dw_schede_intervento_det_3)
destroy(this.dw_schede_intervento_det_4)
destroy(this.dw_schede_intervento_det_5)
end on

event pc_close;call super::pc_close;setnull(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_i_1 = 0
s_cs_xx.parametri.parametro_i_2 = 0
s_cs_xx.parametri.parametro_d_1 = 0
s_cs_xx.parametri.parametro_d_2 = 0


end event

event pc_new;call super::pc_new;dw_schede_intervento_det_1.setitem(dw_schede_intervento_det_1.getrow(), "data_inizio_intervento", datetime(today()))
dw_schede_intervento_det_1.setitem(dw_schede_intervento_det_1.getrow(), "data_fine_intervento", datetime(today()))

end event

event open;call super::open;postevent("ue_cerca_riga")

end event

type cb_documento from commandbutton within w_schede_intervento
integer x = 846
integer y = 1780
integer width = 366
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_schede_intervento_lista.accepttext()

if (isnull(dw_schede_intervento_lista.getrow()) or dw_schede_intervento_lista.getrow() < 1) then
	messagebox("Omnia", "Attenzione riga non valida per la scheda di intervento!")
	return
end if	

if isnull(dw_schede_intervento_lista.getitemnumber(dw_schede_intervento_lista.getrow(), "anno_reg_intervento")) or &
	dw_schede_intervento_lista.getitemnumber(dw_schede_intervento_lista.getrow(), "num_reg_intervento") < 1 then
	messagebox("Omnia", "Attenzione non è stata selezionata nessuna scheda di intervento!")
	return
end if	

window_open_parm(w_schede_intervento_blob, -1, dw_schede_intervento_lista)
end event

type cb_invia from commandbutton within w_schede_intervento
integer x = 69
integer y = 1780
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "I&nvia"
end type

event clicked;call super::clicked;setnull(s_cs_xx.parametri.parametro_uo_dw_search)
dw_schede_intervento_det_5.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = dw_schede_intervento_det_5
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"

end event

type mle_stato from multilineedit within w_schede_intervento
integer x = 562
integer y = 1680
integer width = 1714
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 10789024
boolean enabled = false
boolean autohscroll = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_cerca from commandbutton within w_schede_intervento
integer x = 2491
integer y = 420
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;iuo_dw_main = dw_schede_intervento_lista

dw_schede_intervento_lista.change_dw_current()

parent.postevent("pc_retrieve")

dw_folder.fu_selecttab(1)
end event

type cb_annulla from commandbutton within w_schede_intervento
integer x = 2491
integer y = 320
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string		ls_null

datetime	ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_schede_intervento_sel.setitem(1,"data_a",ldt_null)

dw_schede_intervento_sel.setitem(1,"data_da",ldt_null)

dw_schede_intervento_sel.setitem(1,"flag_autorizzato","%")

dw_schede_intervento_sel.setitem(1,"cod_area",ls_null)

dw_schede_intervento_sel.setitem(1,"cod_cliente",ls_null)

dw_schede_intervento_sel.setitem(1,"cod_operaio",ls_null)

dw_schede_intervento_sel.setitem(1,"cod_prodotto",ls_null)

dw_folder.fu_selecttab(2)
end event

type cb_ricerca_operaio from cb_operai_ricerca within w_schede_intervento
integer x = 2350
integer y = 224
integer width = 82
integer height = 72
integer taborder = 90
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_schede_intervento_sel
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type cb_tes_campionamenti from commandbutton within w_schede_intervento
integer x = 2491
integer y = 220
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ca&mpionam."
end type

event clicked;long   ll_num_reg_campionamenti, ll_anno_reg_campionamenti, ll_row


ll_row = dw_schede_intervento_lista.getrow()
ll_anno_reg_campionamenti = dw_schede_intervento_lista.getitemnumber(ll_row, "anno_reg_campionamenti")
ll_num_reg_campionamenti = dw_schede_intervento_lista.getitemnumber(ll_row, "num_reg_campionamenti")

if isnull(ll_anno_reg_campionamenti) or ll_anno_reg_campionamenti = 0 or &
	isnull(ll_num_reg_campionamenti) or ll_num_reg_campionamenti = 0 then
	if wf_genera_campionamento(ll_anno_reg_campionamenti,ll_num_reg_campionamenti) <> 0 then
		return
	end if
	dw_schede_intervento_lista.triggerevent("pcd_retrieve")
end if

window_open(w_det_campionamenti,-1)

w_det_campionamenti.ib_accettazioni = true
w_det_campionamenti.ii_anno_reg_campionamenti = ll_anno_reg_campionamenti
w_det_campionamenti.il_num_reg_campionamenti = ll_num_reg_campionamenti

dw_schede_intervento_lista.setrow(ll_row)

end event

type cb_1 from commandbutton within w_schede_intervento
integer x = 2491
integer y = 20
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;if not isvalid(w_report_scheda_intervento) then
	s_cs_xx.parametri.parametro_d_1 = dw_schede_intervento_lista.getitemnumber(dw_schede_intervento_lista.getrow(), "anno_reg_intervento")
	s_cs_xx.parametri.parametro_d_2 = dw_schede_intervento_lista.getitemnumber(dw_schede_intervento_lista.getrow(), "num_reg_intervento")
	window_open(w_report_scheda_intervento, -1)
end if

end event

type cb_doc_comp from cb_documenti_compilati within w_schede_intervento
integer x = 1541
integer y = 1484
integer width = 69
integer height = 72
integer taborder = 10
boolean bringtotop = true
boolean enabled = false
string text = "..."
end type

event clicked;call super::clicked;string ls_ritorno

s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "doc_compilato"
s_cs_xx.parametri.parametro_s_2 = "DQ1"
s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1)

if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)

   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
      s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
      s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_ritorno)
   end if      
else
   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
end if


end event

event getfocus;call super::getfocus;dw_schede_intervento_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type cb_ric_prod from cb_prod_ricerca within w_schede_intervento
integer x = 2647
integer y = 1424
integer width = 73
integer height = 72
integer taborder = 20
boolean bringtotop = true
boolean enabled = false
end type

event getfocus;call super::getfocus;dw_schede_intervento_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type cb_ric_stock from cb_stock_ricerca within w_schede_intervento
integer x = 1797
integer y = 1864
integer width = 73
integer height = 72
integer taborder = 30
boolean bringtotop = true
boolean enabled = false
end type

event clicked;call super::clicked;// ------------------------------------------------------------------------------------------
//                              RICERCA STOCK PRODOTTO
// Significato dei parametri
//
//
//   s_cs_xx.parametri.parametro_s_1 .... s_9   --->> nomi colonne su cui incidere i valori cercati
//   s_cs_xx.parametri.parametro_s_10.... s_15  --->> valori di filtro
//
//   s_cs_xx.parametri.parametro_s_10 = codice prodotto
//   s_cs_xx.parametri.parametro_s_11 = codice deposito
//   s_cs_xx.parametri.parametro_s_12 = codice ubicazione
//   s_cs_xx.parametri.parametro_s_13 = codice lotto
//   s_cs_xx.parametri.parametro_data_1 = data stock
//   s_cs_xx.parametri.parametro_d_1 = progressivo stock
//
// ------------------------------------------------------------------------------------------

s_cs_xx.parametri.parametro_s_10 = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_schede_intervento_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"
setnull(s_cs_xx.parametri.parametro_s_7)
setnull(s_cs_xx.parametri.parametro_s_8)

window_open(w_ricerca_stock, 0)

decimal  ld_quan_stock
string   ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto
datetime ldt_data_stock
long     ll_prog_stock

ls_cod_prodotto = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(),"cod_prodotto")
ls_cod_deposito = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(),"cod_deposito")
ls_cod_ubicazione = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(),"cod_ubicazione")
ls_cod_lotto = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(),"cod_lotto")
ldt_data_stock = dw_schede_intervento_det_1.getitemdatetime(dw_schede_intervento_det_1.getrow(),"data_stock")
ll_prog_stock = dw_schede_intervento_det_1.getitemnumber(dw_schede_intervento_det_1.getrow(),"prog_stock")

select quan_assegnata
into   :ld_quan_stock
from   stock
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_prodotto and
		 cod_deposito = :ls_cod_deposito and
		 cod_ubicazione = :ls_cod_ubicazione and
		 cod_lotto = :ls_cod_lotto and
		 data_stock = :ldt_data_stock and
		 prog_stock = :ll_prog_stock;
		 
if sqlca.sqlcode < 0 then
	messagebox("OMNIA","Errore in lettura quantità stock da tabella stock: " + sqlca.sqlerrtext)
	return -1
end if
			
dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"quan_stock",ld_quan_stock)
end event

type cb_cerca_az_corr from cb_ricerca_az_corr within w_schede_intervento
integer x = 1298
integer y = 2184
integer height = 72
integer taborder = 90
boolean bringtotop = true
boolean enabled = false
end type

event clicked;call super::clicked;dw_schede_intervento_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "anno_azione_corr"
s_cs_xx.parametri.parametro_s_2 = "num_azione_corr"
end event

type cb_chiusura from commandbutton within w_schede_intervento
integer x = 2322
integer y = 2044
integer width = 434
integer height = 80
integer taborder = 170
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiusura"
end type

event clicked;string   ls_firma_chiusura, ls_difetto, ls_tipo_lista, ls_cod_lista, ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, &
         ls_flag_gestione_fasi, ls_cod_area_aziendale, ls_cod_prodotto, ls_cod_cat_mer

long     ll_anno, ll_num, ll_row, ll_num_sequenza_corrente

datetime ldt_chiusura


ls_firma_chiusura = dw_schede_intervento_lista.getitemstring(dw_schede_intervento_lista.getrow(),"firma_chiusura")
ldt_chiusura = dw_schede_intervento_lista.getitemdatetime(dw_schede_intervento_lista.getrow(),"data_chiusura")

if not isnull(ls_firma_chiusura) and not isnull(ldt_chiusura) then	
	messagebox("OMNIA","Questo Reso/Reclamo è già stato chiusa in data " + string(ldt_chiusura) + " dal mansionario " + ls_firma_chiusura)
	return -1
end if

if messagebox("OMNIA","Chiudere il Reso/Reclamo selezionato?",question!,yesno!,2) = 2 then
	return -1
end if

setnull(ls_firma_chiusura)

select cod_resp_divisione
into   :ls_firma_chiusura
from   mansionari
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_chiudi_reclami = 'S';
		 
if sqlca.sqlcode < 0 then
	messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_firma_chiusura) then
	messagebox("OMNIA","Il mansionario corrente non è abilitato alla chiusura dei resi/reclami")
	return -1
end if

ll_anno = dw_schede_intervento_lista.getitemnumber(dw_schede_intervento_lista.getrow(),"anno_reg_intervento")
ll_num = dw_schede_intervento_lista.getitemnumber(dw_schede_intervento_lista.getrow(),"num_reg_intervento")
ldt_chiusura = datetime(today(),now())

update schede_intervento
set    firma_chiusura = :ls_firma_chiusura,
       data_chiusura = :ldt_chiusura
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_reg_intervento = :ll_anno and
		 num_reg_intervento = :ll_num;
		 
if sqlca.sqlcode <> 0 then
	messagebox("OMNIA","Errore in chiusura Reso/Reclamo.~nErrore nella update di schede_intervento: " + sqlca.sqlerrtext)
	return -1
end if

commit;

dw_schede_intervento_lista.triggerevent("pcd_retrieve")

dw_schede_intervento_lista.setrow(ll_row)

dw_schede_intervento_lista.scrolltorow(ll_row)

ll_row = dw_schede_intervento_lista.getrow()

ls_cod_tipo_causa = dw_schede_intervento_lista.getitemstring( ll_row, "cod_tipo_causa")
			
if not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
				
	select cod_tipo_lista_dist,
	       cod_lista_dist
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_cause
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_causa = :ls_cod_tipo_causa;
						 
	if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
					
		select flag_gestione_fasi
		into   :ls_flag_gestione_fasi
		from   tab_tipi_liste_dist
		where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
					
		if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
		
		if ls_flag_gestione_fasi <> "S" then return
					
		ll_num_sequenza_corrente = dw_schede_intervento_lista.getitemnumber( ll_row, "num_sequenza_corrente")
		
		ls_cod_area_aziendale = dw_schede_intervento_lista.getitemstring( ll_row, "cod_area_aziendale")
		
		ls_cod_prodotto = dw_schede_intervento_lista.getitemstring( ll_row, "cod_prodotto")
		
		select cod_cat_mer
		into   :ls_cod_cat_mer
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
				 
		if isnull(ls_cod_cat_mer) then ls_cod_cat_mer = ""
		
		if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""		
						
		s_cs_xx.parametri.parametro_s_1 = "L"
						
		s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
						
		s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
							
		s_cs_xx.parametri.parametro_s_4 = " flag_autorizza_reso = 'S' "
							
		s_cs_xx.parametri.parametro_s_5 = "Registrazione Reso " + string(ll_anno) + "/" + string(ll_num)
							
		s_cs_xx.parametri.parametro_s_6 = "E' stato registrato il Reso " + string(ll_anno) + "/" + string(ll_num)
							
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari autorizzati al reso?"
		
		s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
							
		s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer
							
		s_cs_xx.parametri.parametro_s_11 = string(ll_anno)
							
		s_cs_xx.parametri.parametro_s_12 = string(ll_num)
						
		s_cs_xx.parametri.parametro_s_13 = ""
							
		s_cs_xx.parametri.parametro_s_14 = ""
							
		s_cs_xx.parametri.parametro_s_15 = "R"
							
		s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
							
		s_cs_xx.parametri.parametro_b_1 = true
							
		openwithparm( w_invio_messaggi, 0)	
		
		if not isnull(s_cs_xx.parametri.parametro_ul_1) then
			
			update schede_intervento
			set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_reg_intervento = :ll_anno and
					 num_reg_intervento = :ll_num;
					 
			if sqlca.sqlcode < 0 then
				messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
				rollback;
				return -1
			end if
			
			commit;
			
			parent.triggerevent("pc_retrieve")
			
			dw_schede_intervento_lista.setrow( ll_row)
					 
			
		end if
			
	elseif sqlca.sqlcode < 0 then
				
		messagebox( "OMNIA", "Errore durante la ricerca della lista di distribuzione: " + sqlca.sqlerrtext)						
		return -1
		
	elseif sqlca.sqlcode = 100 then
		
		//messagebox( "OMNIA", "Attenzione: controllare l'associazione tipo causa / tipo lista!")
		return -1
		
	end if
					
end if		

dw_schede_intervento_lista.triggerevent("pcd_retrieve")

dw_schede_intervento_lista.setrow(ll_row)

dw_schede_intervento_lista.scrolltorow(ll_row)


end event

type cb_controllo from commandbutton within w_schede_intervento
integer x = 2491
integer y = 120
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Controllo"
end type

event clicked;long   ll_i[], ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista, ll_anno_reg_intervento, ll_num_reg_intervento
   
ll_i[1] = dw_schede_intervento_lista.getrow()
if isnull(dw_schede_intervento_det_1.getitemnumber(ll_i[1], "num_reg_lista")) or &
   dw_schede_intervento_det_1.getitemnumber(ll_i[1], "num_reg_lista") = 0 then
      messagebox("Schede Intervento","ATTENZIONE: non è stata impostata la lista da utilizzare nella gestione interventi", StopSign!)
      return
end if

ll_num_reg_lista = dw_schede_intervento_det_1.getitemnumber(ll_i[1],"num_reg_lista") 
ll_prog_liste_con_comp = dw_schede_intervento_det_1.getitemnumber(ll_i[1],"prog_liste_con_comp") 

if isnull(ll_prog_liste_con_comp) or ll_prog_liste_con_comp = 0 then
	
	if messagebox("OMNIA","Attenzione!~nUna volta eseguita la compilazione non sara più possibile modificare la lista associata. Continuare?",&
                 question!,yesno!,2) = 2 then
		return -1
	end if
	
   f_crea_liste_con_comp(ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

   ll_anno_reg_intervento = dw_schede_intervento_lista.getitemnumber(ll_i[1],"anno_reg_intervento")
   ll_num_reg_intervento = dw_schede_intervento_lista.getitemnumber(ll_i[1],"num_reg_intervento")
   
   update schede_intervento
   set    schede_intervento.num_versione = :ll_num_versione,
          schede_intervento.num_edizione = :ll_num_edizione,
          schede_intervento.num_reg_lista = :ll_num_reg_lista,
          schede_intervento.prog_liste_con_comp = :ll_prog_liste_con_comp
   where  schede_intervento.cod_azienda = :s_cs_xx.cod_azienda and
          schede_intervento.anno_reg_intervento = :ll_anno_reg_intervento and 
          schede_intervento.num_reg_intervento = :ll_num_reg_intervento;
   if sqlca.sqlcode = 0 then
      commit;
   else
      messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   dw_schede_intervento_lista.triggerevent("pcd_retrieve")
   dw_schede_intervento_lista.set_selected_rows(1, &
                                                ll_i[], &
                                                c_ignorechanges, &
                                                c_refreshchildren, &
                                                c_refreshsame)
end if

window_open_parm(w_det_liste_con_comp, 0, dw_schede_intervento_lista)

end event

type cb_comunicazioni from commandbutton within w_schede_intervento
integer x = 1870
integer y = 1780
integer width = 434
integer height = 80
integer taborder = 150
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Co&municazioni"
end type

event clicked;long   ll_num_reg_intervento, ll_anno_reg_intervento, ll_row

ll_row = dw_schede_intervento_lista.getrow()

if ll_row < 1 then return

ll_anno_reg_intervento = dw_schede_intervento_lista.getitemnumber(ll_row, "anno_reg_intervento")

ll_num_reg_intervento = dw_schede_intervento_lista.getitemnumber(ll_row, "num_reg_intervento")

setnull(s_cs_xx.parametri.parametro_ul_1)

setnull(s_cs_xx.parametri.parametro_ul_2)

setnull(s_cs_xx.parametri.parametro_d_1)

setnull(s_cs_xx.parametri.parametro_d_2)

s_cs_xx.parametri.parametro_ul_1 = ll_anno_reg_intervento

s_cs_xx.parametri.parametro_ul_2 = ll_num_reg_intervento

window_open(w_schede_intervento_comunicazioni,-1)


end event

type cb_respingi from commandbutton within w_schede_intervento
integer x = 457
integer y = 1780
integer width = 366
integer height = 80
integer taborder = 160
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Re&spingi"
end type

event clicked;string ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi, ls_cod_area_aziendale, ls_cod_prodotto, ls_cod_cat_mer

long   ll_riga, ll_anno, ll_num, ll_num_sequenza_corrente

ll_riga = dw_schede_intervento_lista.getrow()

if ll_riga < 1 then return

ll_anno = dw_schede_intervento_lista.getitemnumber( ll_riga, "anno_reg_intervento")
		
ll_num = dw_schede_intervento_lista.getitemnumber( ll_riga, "num_reg_intervento")
		
ls_cod_tipo_causa = dw_schede_intervento_lista.getitemstring( ll_riga, "cod_tipo_causa")
			
if not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
				
	select cod_tipo_lista_dist,
	       cod_lista_dist
	into   :ls_cod_tipo_lista_dist,
	       :ls_cod_lista_dist
	from   tab_tipi_cause
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_causa = :ls_cod_tipo_causa;
						 
	if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
					
		select flag_gestione_fasi
		into   :ls_flag_gestione_fasi
		from   tab_tipi_liste_dist
		where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
					
		if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
		
		if ls_flag_gestione_fasi <> "S" then return
		
		ls_cod_area_aziendale = dw_schede_intervento_lista.getitemstring( ll_riga, "cod_area_aziendale")
		
		ls_cod_prodotto = dw_schede_intervento_lista.getitemstring( ll_riga, "cod_prodotto")
		
		select cod_cat_mer
		into   :ls_cod_cat_mer
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
				 
		if isnull(ls_cod_cat_mer) then ls_cod_cat_mer = ""
		
		if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""
					
		ll_num_sequenza_corrente = dw_schede_intervento_lista.getitemnumber( ll_riga, "num_sequenza_corrente")
		
		s_cs_xx.parametri.parametro_s_1 = "L"
						
		s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
						
		s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
							
		s_cs_xx.parametri.parametro_s_4 = " flag_autorizza_reso = 'S' "
							
		s_cs_xx.parametri.parametro_s_5 = "Registrazione Reso " + string(ll_anno) + "/" + string(ll_num)
							
		s_cs_xx.parametri.parametro_s_6 = "E' stato registrato il Reso " + string(ll_anno) + "/" + string(ll_num)
							
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari autorizzati al reso?"
		
		s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
		
		s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer
							
		s_cs_xx.parametri.parametro_s_11 = string(ll_anno)
							
		s_cs_xx.parametri.parametro_s_12 = string(ll_num)
						
		s_cs_xx.parametri.parametro_s_13 = ""
							
		s_cs_xx.parametri.parametro_s_14 = ""
							
		s_cs_xx.parametri.parametro_s_15 = "R"
							
		s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
							
		s_cs_xx.parametri.parametro_b_1 = false
							
		openwithparm( w_invio_messaggi, 0)	
		
		if not isnull(s_cs_xx.parametri.parametro_ul_1) then
			
			update schede_intervento
			set    num_sequenza_corrente = :s_cs_xx.parametri.parametro_ul_1
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_reg_intervento = :ll_anno and
					 num_reg_intervento = :ll_num;
					 
			if sqlca.sqlcode < 0 then
				messagebox( "OMNIA", "Errore durante aggiornamento stato fase: " + sqlca.sqlerrtext)
				rollback;
				return -1
			end if
			
			commit;
			
			parent.triggerevent("pc_retrieve")
			
			dw_schede_intervento_lista.setrow( ll_riga)					 
			
		end if
			
	elseif sqlca.sqlcode < 0 then				
		messagebox( "OMNIA", "Errore durante la ricerca della lista di distribuzione: " + sqlca.sqlerrtext)						
		return -1		
	else		
		messagebox( "OMNIA", "Attenzione: controllare l'associazione tipo causa / tipo lista!")
		return -1		
	end if					
end if				

end event

type tab_folder from tab within w_schede_intervento
event create ( )
event destroy ( )
integer x = 23
integer y = 540
integer width = 2834
integer height = 1760
integer taborder = 100
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean fixedwidth = true
boolean boldselectedtext = true
alignment alignment = center!
integer selectedtab = 1
tabpage_anagrafici tabpage_anagrafici
tabpage_generale tabpage_generale
tabpage_costi tabpage_costi
tabpage_date tabpage_date
tabpage_reso tabpage_reso
end type

on tab_folder.create
this.tabpage_anagrafici=create tabpage_anagrafici
this.tabpage_generale=create tabpage_generale
this.tabpage_costi=create tabpage_costi
this.tabpage_date=create tabpage_date
this.tabpage_reso=create tabpage_reso
this.Control[]={this.tabpage_anagrafici,&
this.tabpage_generale,&
this.tabpage_costi,&
this.tabpage_date,&
this.tabpage_reso}
end on

on tab_folder.destroy
destroy(this.tabpage_anagrafici)
destroy(this.tabpage_generale)
destroy(this.tabpage_costi)
destroy(this.tabpage_date)
destroy(this.tabpage_reso)
end on

event selectionchanged;choose case newindex
	case 1
		dw_schede_intervento_det_1.show()
		dw_schede_intervento_det_2.hide()
		dw_schede_intervento_det_3.hide()
		dw_schede_intervento_det_4.hide()
		dw_schede_intervento_det_5.hide()	
		mle_stato.hide()
		cb_doc_comp.hide()
		cb_ric_prod.show()		
		cb_ric_stock.show()
		dw_schede_intervento_det_1.object.b_ricerca_cliente.visible=true
		cb_cerca_az_corr.show()
		dw_schede_intervento_det_5.object.b_ricerca_fornitore.visible=false
		cb_comunicazioni.hide()
		cb_invia.hide()		
		cb_documento.hide()
		cb_respingi.hide()
		cb_chiusura.hide()
	case 2
		dw_schede_intervento_det_1.hide()
		dw_schede_intervento_det_2.hide()
		dw_schede_intervento_det_3.hide()
		dw_schede_intervento_det_4.hide()
		dw_schede_intervento_det_5.show()		
		mle_stato.show()		
		cb_doc_comp.hide()
		cb_ric_prod.hide()		
		cb_ric_stock.hide()
dw_schede_intervento_det_1.object.b_ricerca_cliente.visible=false
		cb_cerca_az_corr.hide()
dw_schede_intervento_det_5.object.b_ricerca_fornitore.visible=true
		cb_comunicazioni.show()
		cb_invia.show()
		cb_respingi.show()
		cb_documento.show()
		cb_chiusura.hide()
	case 3
		dw_schede_intervento_det_1.hide()
		dw_schede_intervento_det_2.show()
		dw_schede_intervento_det_3.hide()
		dw_schede_intervento_det_4.hide()
		dw_schede_intervento_det_5.hide()	
		mle_stato.hide()
		cb_doc_comp.hide()
		cb_ric_prod.hide()		
		cb_ric_stock.hide()
dw_schede_intervento_det_1.object.b_ricerca_cliente.visible=false
		cb_cerca_az_corr.hide()
dw_schede_intervento_det_5.object.b_ricerca_fornitore.visible=false
		cb_comunicazioni.hide()
		cb_invia.hide()
		cb_respingi.hide()	
		cb_documento.hide()
		cb_chiusura.hide()
	case 4
		dw_schede_intervento_det_1.hide()
		dw_schede_intervento_det_2.hide()
		dw_schede_intervento_det_3.show()
		dw_schede_intervento_det_4.hide()
		dw_schede_intervento_det_5.hide()		
		mle_stato.hide()
		cb_doc_comp.show()
		cb_ric_prod.hide()		
		cb_ric_stock.hide()
dw_schede_intervento_det_1.object.b_ricerca_cliente.visible=false
		cb_cerca_az_corr.hide()
dw_schede_intervento_det_5.object.b_ricerca_fornitore.visible=false
		cb_comunicazioni.hide()
		cb_invia.hide()
		cb_respingi.hide()
		cb_documento.hide()
		cb_chiusura.show()
	case 5
		dw_schede_intervento_det_1.hide()
		dw_schede_intervento_det_2.hide()
		dw_schede_intervento_det_3.hide()
		dw_schede_intervento_det_4.show()
		dw_schede_intervento_det_5.hide()		
		mle_stato.hide()		
		cb_doc_comp.hide()
		cb_ric_prod.hide()		
		cb_ric_stock.hide()
dw_schede_intervento_det_1.object.b_ricerca_cliente.visible=false
		cb_cerca_az_corr.hide()
dw_schede_intervento_det_5.object.b_ricerca_fornitore.visible=false
		cb_comunicazioni.hide()
		cb_invia.hide()
		cb_respingi.hide()	
		cb_documento.hide()
		cb_chiusura.hide()
end choose
end event

type tabpage_anagrafici from userobject within tab_folder
event create ( )
event destroy ( )
integer x = 18
integer y = 104
integer width = 2798
integer height = 1640
long backcolor = 79741120
string text = "Dati Anagrafici"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 553648127
end type

type tabpage_generale from userobject within tab_folder
integer x = 18
integer y = 104
integer width = 2798
integer height = 1640
long backcolor = 79741120
string text = "Note Generali"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
end type

type tabpage_costi from userobject within tab_folder
integer x = 18
integer y = 104
integer width = 2798
integer height = 1640
long backcolor = 79741120
string text = "Costi / Tempi"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
end type

type tabpage_date from userobject within tab_folder
event create ( )
event destroy ( )
integer x = 18
integer y = 104
integer width = 2798
integer height = 1640
long backcolor = 79741120
string text = "Date / Documenti"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
end type

type tabpage_reso from userobject within tab_folder
integer x = 18
integer y = 104
integer width = 2798
integer height = 1640
long backcolor = 79741120
string text = "Reso"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
end type

type dw_schede_intervento_det_1 from uo_cs_xx_dw within w_schede_intervento
integer x = 46
integer y = 640
integer width = 2789
integer height = 1640
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_schede_intervento_det_1"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

string ls_prodotto, ls_cliente, ls_null
string ls_str1, ls_str2, ls_str3, ls_cod_misura
long   ll_null, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_costo_ora

datetime   ld_null
double ld_progressivo

setnull(ls_null)
setnull(ll_null)
setnull(ld_null)


choose case i_colname

case "cod_intervento"
   if not isnull(i_coltext) then
      SELECT tab_interventi.num_reg_lista,   
             tab_interventi.num_versione,   
             tab_interventi.num_edizione  
      INTO   :ll_num_reg_lista,   
             :ll_num_versione,   
             :ll_num_edizione  
      FROM   tab_interventi  
      WHERE  ( tab_interventi.cod_azienda = :s_cs_xx.cod_azienda ) AND  
             ( tab_interventi.cod_intervento = :i_coltext )   ;
      if sqlca.sqlcode <> 0 then
         messagebox("Scheda Intervento",sqlca.sqlerrtext)
         return
      end if
      this.setitem(this.getrow(),"num_reg_lista", ll_num_reg_lista)
      this.setitem(this.getrow(),"num_versione", ll_num_versione)
      this.setitem(this.getrow(),"num_edizione", ll_num_edizione)
   end if

case "cod_prodotto"
   ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
   if isnull(ls_prodotto) then ls_prodotto = " "
   if (i_coltext <> ls_prodotto) and (not isnull(this.getitemstring(this.getrow(),"cod_deposito")))  then
      messagebox("Accettazione Materiali","Prodotto variato: devi reimpostare stock !",Exclamation!)
      dw_schede_intervento_det_1.setitem(dw_schede_intervento_det_1.getrow(), "cod_deposito", ls_null)
      dw_schede_intervento_det_1.setitem(dw_schede_intervento_det_1.getrow(), "cod_ubicazione", ls_null)
      dw_schede_intervento_det_1.setitem(dw_schede_intervento_det_1.getrow(), "cod_lotto", ls_null)
      dw_schede_intervento_det_1.setitem(dw_schede_intervento_det_1.getrow(), "data_stock", ld_null)
      dw_schede_intervento_det_1.setitem(dw_schede_intervento_det_1.getrow(), "prog_stock", ll_null)
   end if
   ls_prodotto = i_coltext
   ls_cliente =this.getitemstring(this.getrow(), "cod_cliente")

case "cod_deposito"
   if (not isnull(i_coltext)) and &
      (not isnull(this.getitemstring(this.getrow(),"cod_prodotto"))) and &
      (not isnull(this.getitemstring(this.getrow(),"cod_deposito"))) then
      f_PO_LoadDDLB_DW(dw_schede_intervento_det_1, &
                       "cod_ubicazione", &
                       SQLCA, &
                       "stock", &
                       "cod_ubicazione" , &
                       "cod_ubicazione" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
                       "(stock.cod_deposito = '" + i_coltext + "') and " + &
                       "(stock.cod_prodotto = '" + this.getitemstring(this.getrow(),"cod_prodotto") + "')" )
   end if

case "cod_ubicazione"
   if (not isnull(i_coltext)) and &
      (not isnull(this.getitemstring(this.getrow(),"cod_prodotto"))) and &
      (not isnull(this.getitemstring(this.getrow(),"cod_deposito"))) and &
      (not isnull(this.getitemstring(this.getrow(),"cod_ubicazione"))) then
      f_PO_LoadDDLB_DW(dw_schede_intervento_det_1, &
                       "cod_lotto", &
                       SQLCA, &
                       "stock", &
                       "cod_lotto" , &
                       "cod_lotto" , &
                       "(stock.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
                       "(stock.cod_deposito = '" + this.getitemstring(this.getrow(),"cod_ubicazione") + "') and " + &
                       "(stock.cod_ubicazione = '" + i_coltext + "') and " + &
                       "(stock.cod_prodotto = '" + this.getitemstring(this.getrow(),"cod_prodotto") + "')" )

   end if

case "cod_operaio"
      SELECT tab_cat_attrezzature.costo_medio_orario  
      INTO   :ll_costo_ora  
      FROM   tab_cat_attrezzature  
      WHERE  ( tab_cat_attrezzature.cod_azienda = :s_cs_xx.cod_azienda ) AND  
             ( tab_cat_attrezzature.cod_cat_attrezzature in
                                   (  SELECT anag_operai.cod_cat_attrezzature  
                                      FROM anag_operai  
                                      WHERE ( anag_operai.cod_azienda = :s_cs_xx.cod_azienda ) AND  
                                            ( anag_operai.cod_operaio = :i_coltext )  ))   ;
      choose case sqlca.sqlcode
         case 100
            messagebox("Schede Intervento","Manca l'indicazione della risorsa umana: verificare il campo categoria attrezzatura in anagrafica dipendenti e il campo costo medio in risorse umane", StopSign!)
            this.setitem(this.getrow(),"costo_ora_operaio", 0)
            return
         case 0
            this.setitem(this.getrow(),"costo_ora_operaio", ll_costo_ora)
         case else
            messagebox("Schede Intervento","Errore durante la ricerca costo medio in risorse umane  SQL--> "+sqlca.sqlerrtext, StopSign!)
            this.setitem(this.getrow(),"costo_ora_operaio", 0)
            return
      end choose
end choose

end if   // si riferisce alla condizione iniziale di test della variabile i_extendmode

end event

on pcd_save;call uo_cs_xx_dw::pcd_save;//ib_x = false
end on

event itemchanged;call super::itemchanged;datetime ldt_data_stock, ldt_null

dec{4} 	ld_quan_stock, ld_null

string 	ls_cod_resp, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_null, ls_cod_causa, ls_flag_criticita, &
         ls_flag_categoria_regis

long   	ll_lista, ll_prog_lista, ll_edizione, ll_versione, ll_prog_stock, ll_null


choose case i_colname
		
	case "num_reg_lista"
	
		ll_prog_lista = getitemnumber(getrow(),"prog_liste_con_comp")
	
		if not isnull(ll_prog_lista) then
			messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata alla manutenzione dopo aver già eseguito la compilazione")
			return 1
		end if	
		
		ll_lista = long(i_coltext)
		
		select max(num_edizione)
		into   :ll_edizione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 approvato_da is not null;
				 
		if sqlca.sqlcode <> 0 then
			messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		select max(num_versione)
		into   :ll_versione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 num_edizione = :ll_edizione and
				 approvato_da is not null;
				 
		if sqlca.sqlcode <> 0 then
			messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if
		
		setnull(ll_prog_lista)
		
		setitem(getrow(),"num_edizione",ll_edizione)
		setitem(getrow(),"num_versione",ll_versione)
		setitem(getrow(),"prog_liste_con_comp",ll_prog_lista)
		
	case "flag_reso"
		
		if i_coltext = "S" then
			
			setnull(ls_cod_resp)
			
			select cod_resp_divisione
			into   :ls_cod_resp
			from   mansionari
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_utente = :s_cs_xx.cod_utente and
					 flag_autorizza_reso = 'S';
					 
			if sqlca.sqlcode < 0 then
				messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
				return 1
			elseif sqlca.sqlcode = 100 or isnull(ls_cod_resp) then
				messagebox("OMNIA","Il mansionario corrente non è abilitato ad autorizzare un reso")
				return 1
			end if
			
			tab_folder.tabpage_reso.enabled = true
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"cod_resp_divisione",ls_cod_resp)
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"data_reso",datetime(today(),now()))
			
			ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
			
			ls_cod_deposito = getitemstring(getrow(),"cod_deposito")
			
			ls_cod_ubicazione = getitemstring(getrow(),"cod_ubicazione")
			
			ls_cod_lotto = getitemstring(getrow(),"cod_lotto")
			
			ldt_data_stock = getitemdatetime(getrow(),"data_stock")
			
			ll_prog_stock = getitemnumber(getrow(),"prog_stock")
			
			select quan_assegnata
			into   :ld_quan_stock
			from   stock
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and
					 cod_deposito = :ls_cod_deposito and
					 cod_ubicazione = :ls_cod_ubicazione and
					 cod_lotto = :ls_cod_lotto and
					 data_stock = :ldt_data_stock and
					 prog_stock = :ll_prog_stock;
					 
			if sqlca.sqlcode < 0 then
				messagebox("OMNIA","Errore in lettura quantità stock da tabella stock: " + sqlca.sqlerrtext)
				setnull(ld_quan_stock)
			end if
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"quan_stock",ld_quan_stock)
			
		else
			
			if messagebox("OMNIA","I dati del reso verranno cancellati. Continuare?",question!,yesno!,2) = 2 then
				return 1
			end if
			
			tab_folder.tabpage_reso.enabled = false
			
			setnull(ls_null)
			
			setnull(ld_null)
			
			setnull(ll_null)
			
			setnull(ldt_null)
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"cod_resp_divisione",ls_null)
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"data_reso",ldt_null)
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"quan_stock",ld_null)
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"quan_reso",ld_null)
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"val_reso",ld_null)
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"motivo_reso",ls_null)
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"anno_bol_reso",ll_null)
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"num_bol_reso",ll_null)
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"anno_reg_campionamenti",ll_null)
			
			dw_schede_intervento_det_4.setitem(dw_schede_intervento_det_4.getrow(),"num_reg_campionamenti",ll_null)
			
		end if
		
	case "cod_cliente"
		
		setnull(ls_null)
		
      f_po_loaddddw_dw( this, &
                        "cod_des_cliente", &
                        sqlca, &
                        "anag_des_clienti", &
                        "cod_des_cliente", &
                        "rag_soc_1", &
                        "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
      this.setitem( row, "cod_des_cliente", ls_null)	
		
	case "cod_causa"
		
		if ib_visualizza_campi = true then
		
			ls_cod_causa = i_coltext
			
			select flag_criticita,
					 flag_categoria_regis
			into   :ls_flag_criticita,
					 :ls_flag_categoria_regis
			from   cause
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_causa = :ls_cod_causa;
					 
			if sqlca.sqlcode <> 0 then
				
				messagebox( "OMNIA", "Errore durante la ricerca criticita e categoria registrazione: " + sqlca.sqlerrtext)
				
				return -1
				
			end if
			
			if ls_flag_criticita = "" then setnull(ls_flag_criticita)			
			if ls_flag_categoria_regis = "" then setnull(ls_flag_categoria_regis)					
			this.setitem( row, "flag_criticita", ls_flag_criticita)			
			this.setitem( row, "flag_categoria_regis", ls_flag_categoria_regis)
			
		end if
		
end choose

end event

on itemfocuschanged;call uo_cs_xx_dw::itemfocuschanged;//if not(ib_x) then return
//
//string ls_prodotto, ls_fornitore, ls_null, ls_colonna, ls_cod_prod_fornitore
//
//if this.getrow() < 1 then return
//
//setnull(ls_null)
//ls_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
//ls_fornitore = this.getitemstring(this.getrow(), "cod_fornitore")
//ls_colonna = this.getcolumnname()
//
//if (this.getcolumnname() = "cod_prod_fornitore") or (this.getcolumnname() = "cod_prodotto") or (this.getcolumnname() = "cod_fornitore") then
//
//   if (isnull(ls_fornitore)) or (isnull(ls_prodotto))  then
//         f_PO_LoadDDDW_DW(dw_acc_materiali_1,"cod_prod_fornitore",sqlca,&
//                          "tab_prod_fornitori","cod_prod_fornitore","des_prod_fornitore", &
//                          "(tab_prod_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
//                          (tab_prod_fornitori.cod_prodotto = '" + ls_prodotto + "') and&
//                          (tab_prod_fornitori.cod_fornitore = '" + ls_fornitore + "')" )
//         this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
//   else
//        select tab_prod_fornitori.cod_prod_fornitore
//        into   :ls_cod_prod_fornitore
//        from   tab_prod_fornitori
//        where  (tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and
//               (tab_prod_fornitori.cod_prodotto = :ls_prodotto) and
//               (tab_prod_fornitori.cod_fornitore = :ls_fornitore) ;
//        if sqlca.sqlcode = 0 then
//           f_PO_LoadDDDW_DW(dw_acc_materiali_1,"cod_prod_fornitore",sqlca,&
//                             "tab_prod_fornitori","cod_prod_fornitore","des_prod_fornitore", &
//                             "(tab_prod_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
//                             (tab_prod_fornitori.cod_prodotto = '" + ls_prodotto + "') and&
//                             (tab_prod_fornitori.cod_fornitore = '" + ls_fornitore + "')" )
//           this.setitem(this.getrow(), "cod_prod_fornitore", ls_cod_prod_fornitore)
//        else
//           this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
//        end if
//   end if
//end if
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;//ib_x = false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;//ib_x = true
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;//ib_x = true
end on

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_cliente

if currentrow > 0 then
	
	ls_cod_cliente = this.getitemstring( currentrow, "cod_cliente")
	
	if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
	
   	f_po_loaddddw_dw( this, &
                        "cod_des_cliente", &
                        sqlca, &
                        "anag_des_clienti", &
                        "cod_des_cliente", &
                        "rag_soc_1", &
                        "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
	end if
	
end if
		

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_schede_intervento_det_1,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_schede_intervento_det_1,"cod_prodotto")
	case "b_ricerca_deposito"
		guo_ricerca.uof_ricerca_deposito(dw_schede_intervento_det_1,"cod_deposito")
end choose
end event

type dw_schede_intervento_sel from u_dw_search within w_schede_intervento
integer x = 137
integer y = 40
integer width = 2309
integer height = 420
integer taborder = 130
string dataobject = "d_schede_intervento_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_schede_intervento_sel,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_schede_intervento_sel,"cod_prodotto")
end choose
end event

type dw_folder from u_folder within w_schede_intervento
integer x = 23
integer y = 20
integer width = 2446
integer height = 500
integer taborder = 10
end type

type dw_schede_intervento_lista from uo_cs_xx_dw within w_schede_intervento
integer x = 137
integer y = 40
integer width = 2309
integer height = 460
integer taborder = 120
string dataobject = "d_schede_intervento_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_view;call super::pcd_view;stato = false

cb_cerca_az_corr.enabled = false

dw_schede_intervento_det_1.object.b_ricerca_cliente.enabled = false

cb_ric_prod.enabled = false

cb_doc_comp.enabled = false

cb_ric_stock.enabled = false

ib_in_new = false

cb_1.enabled = true

cb_controllo.enabled = true

cb_tes_campionamenti.enabled = true

dw_schede_intervento_det_5.object.b_ricerca_fornitore.enabled = false
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_inizio, ll_fine


ll_inizio = long(string(date(this.getitemdatetime(this.getrow(),"data_inizio_intervento")), "YYYYMMDD"))
ll_fine   = long(string(date(this.getitemdatetime(this.getrow(),"data_fine_intervento")), "YYYYMMDD"))

if ll_inizio = 0 then
   messagebox("Non Conformità","ATTENZIONE! la data inizio intervento risulta mancante",Exclamation!)
   pcca.error = c_fatal
   return
end if

if ll_fine = 0 then
   messagebox("Non Conformità","ATTENZIONE! data fine intervento non impostata",Exclamation!)
   pcca.error = c_fatal
   return
end if

if isnull(this.getitemstring(this.getrow(),"cod_operaio") )then
   messagebox("Non Conformità","ATTENZIONE! manca indicazione del responsabile dell'intervento",Exclamation!)
   pcca.error = c_fatal
   return
end if

if isnull(this.getitemstring(this.getrow(),"cod_intervento") )then
   messagebox("Non Conformità","ATTENZIONE! manca indicazione del tipo di intervento",Exclamation!)
   pcca.error = c_fatal
   return
end if

   
end event

event pcd_new;call super::pcd_new;string ls_cod_operaio

ib_in_new = true

stato = true

cb_cerca_az_corr.enabled = true

dw_schede_intervento_det_1.object.b_ricerca_cliente.enabled = true

cb_ric_prod.enabled = true

cb_doc_comp.enabled = true

cb_ric_stock.enabled = true

cb_1.enabled = false

cb_controllo.enabled = false

cb_tes_campionamenti.enabled = false

dw_schede_intervento_det_5.object.b_ricerca_fornitore.enabled = true

setitem(getrow(),"flag_reso","N")

if ib_visualizza_campi then
	setitem(getrow(),"flag_tipo_reso","C")
end if

if tab_folder.selectedtab = 4 then
	tab_folder.selecttab(1)
end if

tab_folder.tabpage_reso.enabled = false

//Donato 29-01-2009 verifica se è abilitata la gestione blocco campi
if is_flag_BNC = "S" then
	//Donato 26-11-2008 recupero operaio a partire dall'utente collegato
	select cod_operaio
	into :ls_cod_operaio
	from anag_operai
	where cod_azienda=:s_cs_xx.cod_azienda
		and cod_utente=:s_cs_xx.cod_utente;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati da tabella anag_operai. Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	if not isnull(ls_cod_operaio) and ls_cod_operaio <> "" then
		//set del campo operaio
		setitem(getrow(),"cod_operaio",ls_cod_operaio)
	end if
end if

end event

event updatestart;call super::updatestart;long   ll_anno_registrazione, ll_num_registrazione, ll_anno_ric, ll_num_ric, ll_i, ll_anno, ll_num, ll_num_sequenza_corrente

string ls_flag_reso, ls_null, ls_cod_lista_dist, ls_cod_tipo_lista_dist, ls_cod_tipo_causa, ls_flag_gestione_fasi, ls_cod_area_aziendale, &
       ls_cod_prodotto, ls_cod_cat_mer

if ib_in_new then

   this.SetItem (this.GetRow ( ),"anno_reg_intervento", f_anno_esercizio() )
	
   ll_anno_registrazione = this.GetItemNumber(this.GetRow ( ), "anno_reg_intervento" )
	
   select max(num_reg_intervento)
   into   :ll_num_registrazione
   from   schede_intervento
   where  cod_azienda = :s_cs_xx.cod_azienda and 
	       anno_reg_intervento = :ll_anno_registrazione;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
	
   this.SetItem (this.GetRow ( ),"num_reg_intervento", ll_num_registrazione)

//   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
//      this.SetItem (this.GetRow ( ), "num_reg_intervento", 10)
//   end if
	
   ib_aggiorna_richiesta = true
	
   ib_in_new = false
	
end if

for ll_i = 1 to deletedcount()
	
	ll_anno_ric = getitemnumber( ll_i, "anno_reg_richiesta", delete!, false)
	
	ll_num_ric = getitemnumber( ll_i, "num_reg_richiesta", delete!, false)
	
	update richieste
	set    anno_reg_intervento = null,
			 num_reg_intervento = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_richiesta = :ll_anno_ric and
			 num_reg_richiesta = :ll_num_ric;
			 
	if sqlca.sqlcode <> 0 then
		messagebox("OMNIA","Errore nella update di richieste: " + sqlca.sqlerrtext)
		return 1
	end if
	
next

//// *** invio email: in base alla specifica "mod_nc_resi_reclami 03 del 10/02/2005 di marr, se ho la gestione delle fasi
////                  l'invio e-mail funziona diversamente
//
//if s_cs_xx.num_livello_mail > 0 then
//
//	for ll_i = 1 to rowcount()
//		
//		ll_anno = getitemnumber( ll_i, "anno_reg_intervento")
//		
//		ll_num = getitemnumber( ll_i, "num_reg_intervento")
//		
//		if getitemstatus( ll_i, "flag_reso", primary!) = datamodified! and getitemstring( ll_i, "flag_reso") = "S" then
//			
//			// *** controllo tipologia causa
//			
//			ls_cod_tipo_causa = this.getitemstring( ll_i, "cod_tipo_causa")
//			
//			if not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
//				
//				select cod_tipo_lista_dist,
//				       cod_lista_dist
//				into   :ls_cod_tipo_lista_dist,
//				       :ls_cod_lista_dist
//				from   tab_tipi_cause
//				where  cod_azienda = :s_cs_xx.cod_azienda and
//				       cod_tipo_causa = :ls_cod_tipo_causa;
//						 
//				if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
//					
//					select flag_gestione_fasi
//					into   :ls_flag_gestione_fasi
//					from   tab_tipi_liste_dist
//					where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
//					
//					if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
//					
//					if ls_flag_gestione_fasi = "S" then
//					
//						ll_num_sequenza_corrente = this.getitemnumber( ll_i, "num_sequenza_corrente")
//						
//						ls_cod_area_aziendale = this.getitemstring( ll_i, "cod_area_aziendale")
//						
//						ls_cod_prodotto = this.getitemstring( ll_i, "cod_prodotto")			
//						
//						select cod_cat_mer
//						into   :ls_cod_cat_mer
//						from   anag_prodotti
//						where  cod_azienda = :s_cs_xx.cod_azienda and
//								 cod_prodotto = :ls_cod_prodotto;
//								 
//						if isnull(ls_cod_cat_mer) then ls_cod_cat_mer = ""
//						
//						if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""								
//						
//						s_cs_xx.parametri.parametro_s_1 = "L"
//						
//						s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
//						
//						s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
//						
//						s_cs_xx.parametri.parametro_s_4 = " flag_autorizza_reso = 'S' "
//						
//						s_cs_xx.parametri.parametro_s_5 = "Registrazione Reso " + string(ll_anno) + "/" + string(ll_num)
//						
//						s_cs_xx.parametri.parametro_s_6 = "E' stato registrato il Reso " + string(ll_anno) + "/" + string(ll_num)
//						
//						s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari autorizzati al reso?"
//						
//						s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
//						
//						s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer						
//						
//						s_cs_xx.parametri.parametro_s_11 = string(ll_anno)
//						
//						s_cs_xx.parametri.parametro_s_12 = string(ll_num)
//						
//						s_cs_xx.parametri.parametro_s_13 = ""
//						
//						s_cs_xx.parametri.parametro_s_14 = ""
//						
//						s_cs_xx.parametri.parametro_s_15 = "R"
//						
//						s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
//						
//						s_cs_xx.parametri.parametro_b_1 = true
//						
//						openwithparm( w_invio_messaggi, 0)					
//						
//						if not isnull(s_cs_xx.parametri.parametro_ul_1) then
//							this.setitem( ll_i, "num_sequenza_corrente", s_cs_xx.parametri.parametro_ul_1)
//						end if
//						
//						continue
//						
//					end if
//					
//				elseif sqlca.sqlcode < 0 then
//					
//					messagebox( "OMNIA", "Errore durante la ricerca della lista di distribuzione: " + sqlca.sqlerrtext)
//					
//					return 1
//					
//				end if
//				
//			else
//				
//				s_cs_xx.parametri.parametro_s_1 = "M"
//				
//				s_cs_xx.parametri.parametro_s_2 = ""
//				
//				s_cs_xx.parametri.parametro_s_3 = ""
//				
//				s_cs_xx.parametri.parametro_s_4 = " flag_autorizza_reso = 'S' "
//				
//				s_cs_xx.parametri.parametro_s_5 = "Registrazione Reso " + string(ll_anno) + "/" + string(ll_num)
//				
//				s_cs_xx.parametri.parametro_s_6 = "E' stato registrato il Reso " + string(ll_anno) + "/" + string(ll_num)
//				
//				s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari autorizzati al reso?"
//				
//				openwithparm(w_invio_messaggi,0)
//											
//			end if
//			
//		else
//			
//			continue
//			
//		end if
//		
//	next
//	
//end if
end event

event pcd_retrieve;call super::pcd_retrieve;long			ll_error, ll_ret, ll_index, ll_errore

datetime	   ldt_da, ldt_a

string		ls_cliente, ls_prodotto, ls_operaio, ls_area, ls_tipo_reso, ls_where, ls_old_select, new_select


dw_schede_intervento_sel.accepttext()

ldt_a = dw_schede_intervento_sel.getitemdatetime(1,"data_a")

ldt_da = dw_schede_intervento_sel.getitemdatetime(1,"data_da")

ls_area = dw_schede_intervento_sel.getitemstring(1,"cod_area")

ls_cliente = dw_schede_intervento_sel.getitemstring(1,"cod_cliente")

ls_operaio = dw_schede_intervento_sel.getitemstring(1,"cod_operaio")

ls_prodotto = dw_schede_intervento_sel.getitemstring(1,"cod_prodotto")

if ls_area = "" then setnull(ls_area)

if ls_cliente = "" then setnull(ls_cliente)

if ls_operaio = "" then setnull(ls_operaio)

if ls_prodotto = "" then setnull(ls_operaio)

ll_error = retrieve( s_cs_xx.cod_azienda, ldt_da, ldt_a, ls_cliente, ls_prodotto, ls_operaio, ls_area)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_modify;call super::pcd_modify;stato = true

cb_cerca_az_corr.enabled = true

dw_schede_intervento_det_1.object.b_ricerca_cliente.enabled = true

dw_schede_intervento_det_5.object.b_ricerca_fornitore.enabled = true

cb_ric_prod.enabled = true

cb_doc_comp.enabled = true

cb_ric_stock.enabled = true

cb_1.enabled = false

cb_controllo.enabled = false

cb_tes_campionamenti.enabled = false
end event

event updateend;call super::updateend;long ll_anno,ll_registrazione

ll_anno = this.getitemnumber(this.getrow(),"anno_reg_intervento")
ll_registrazione = this.getitemnumber(this.getrow(),"num_reg_intervento")

if s_cs_xx.parametri.parametro_s_1 = "INTERVENTO" and ib_aggiorna_richiesta then
   UPDATE richieste  
   SET    anno_reg_intervento = :ll_anno,   
          num_reg_intervento =  :ll_registrazione
   WHERE (richieste.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         (richieste.anno_reg_richiesta = :s_cs_xx.parametri.parametro_i_1 ) AND  
         (richieste.num_reg_richiesta  = :s_cs_xx.parametri.parametro_d_1 )   ;
   if sqlca.sqlcode <> 0 then
      messagebox("Schede Interventi","Impossibile aggiornare il numero di Non Conformità in richieste visite ispettive", Information!)
   end if
   ib_aggiorna_richiesta = false
end if




long   ll_anno_registrazione, ll_num_registrazione, ll_anno_ric, ll_num_ric, ll_i, ll_num, ll_num_sequenza_corrente

string ls_flag_reso, ls_null, ls_cod_lista_dist, ls_cod_tipo_lista_dist, ls_cod_tipo_causa, ls_flag_gestione_fasi, ls_cod_area_aziendale, &
       ls_cod_prodotto, ls_cod_cat_mer


// *** invio email: in base alla specifica "mod_nc_resi_reclami 03 del 10/02/2005 di marr, se ho la gestione delle fasi
//                  l'invio e-mail funziona diversamente

if s_cs_xx.num_livello_mail > 0 then

	for ll_i = 1 to rowcount()
		
		ll_anno = getitemnumber( ll_i, "anno_reg_intervento")
		
		ll_num = getitemnumber( ll_i, "num_reg_intervento")
		
		if getitemstatus( ll_i, "flag_reso", primary!) = datamodified! and getitemstring( ll_i, "flag_reso") = "S" then
			
			// *** controllo tipologia causa
			
			ls_cod_tipo_causa = this.getitemstring( ll_i, "cod_tipo_causa")
			
			if not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then
				
				select cod_tipo_lista_dist,
				       cod_lista_dist
				into   :ls_cod_tipo_lista_dist,
				       :ls_cod_lista_dist
				from   tab_tipi_cause
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_tipo_causa = :ls_cod_tipo_causa;
						 
				if sqlca.sqlcode = 0 and not isnull(ls_cod_tipo_lista_dist) and not isnull(ls_cod_lista_dist) and ls_cod_tipo_lista_dist <> "" and ls_cod_lista_dist <> "" then
					
					select flag_gestione_fasi
					into   :ls_flag_gestione_fasi
					from   tab_tipi_liste_dist
					where  cod_tipo_lista_dist = :ls_cod_tipo_lista_dist;
					
					if isnull(ls_flag_gestione_fasi) then ls_flag_gestione_fasi = "N"
					
					if ls_flag_gestione_fasi = "S" then
					
						ll_num_sequenza_corrente = this.getitemnumber( ll_i, "num_sequenza_corrente")
						
						ls_cod_area_aziendale = this.getitemstring( ll_i, "cod_area_aziendale")
						
						ls_cod_prodotto = this.getitemstring( ll_i, "cod_prodotto")			
						
						select cod_cat_mer
						into   :ls_cod_cat_mer
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto;
								 
						if isnull(ls_cod_cat_mer) then ls_cod_cat_mer = ""
						
						if isnull(ls_cod_area_aziendale) then ls_cod_area_aziendale = ""								
						
						s_cs_xx.parametri.parametro_s_1 = "L"
						
						s_cs_xx.parametri.parametro_s_2 = ls_cod_tipo_lista_dist
						
						s_cs_xx.parametri.parametro_s_3 = ls_cod_lista_dist
						
						s_cs_xx.parametri.parametro_s_4 = " flag_autorizza_reso = 'S' "
						
						s_cs_xx.parametri.parametro_s_5 = "Registrazione Reso " + string(ll_anno) + "/" + string(ll_num)
						
						s_cs_xx.parametri.parametro_s_6 = "E' stato registrato il Reso " + string(ll_anno) + "/" + string(ll_num)
						
						s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari autorizzati al reso?"
						
						s_cs_xx.parametri.parametro_s_9 = ls_cod_area_aziendale
						
						s_cs_xx.parametri.parametro_s_10 = ls_cod_cat_mer						
						
						s_cs_xx.parametri.parametro_s_11 = string(ll_anno)
						
						s_cs_xx.parametri.parametro_s_12 = string(ll_num)
						
						s_cs_xx.parametri.parametro_s_13 = ""
						
						s_cs_xx.parametri.parametro_s_14 = ""
						
						s_cs_xx.parametri.parametro_s_15 = "R"
						
						s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
						
						s_cs_xx.parametri.parametro_b_1 = true
						
						openwithparm( w_invio_messaggi, 0)					
						
						if not isnull(s_cs_xx.parametri.parametro_ul_1) then
							this.setitem( ll_i, "num_sequenza_corrente", s_cs_xx.parametri.parametro_ul_1)
						end if
						
						continue
						
					end if
					
				elseif sqlca.sqlcode < 0 then
					
					messagebox( "OMNIA", "Errore durante la ricerca della lista di distribuzione: " + sqlca.sqlerrtext)
					
					return 1
					
				end if
				
			else
				
				s_cs_xx.parametri.parametro_s_1 = "M"
				
				s_cs_xx.parametri.parametro_s_2 = ""
				
				s_cs_xx.parametri.parametro_s_3 = ""
				
				s_cs_xx.parametri.parametro_s_4 = " flag_autorizza_reso = 'S' "
				
				s_cs_xx.parametri.parametro_s_5 = "Registrazione Reso " + string(ll_anno) + "/" + string(ll_num)
				
				s_cs_xx.parametri.parametro_s_6 = "E' stato registrato il Reso " + string(ll_anno) + "/" + string(ll_num)
				
				s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari autorizzati al reso?"
				
				openwithparm(w_invio_messaggi,0)
											
			end if
			
		else
			
			continue
			
		end if
		
	next
	
end if

end event

event pcd_save;call super::pcd_save;cb_cerca_az_corr.enabled = false

cb_doc_comp.enabled = false

cb_ric_prod.enabled = false

cb_ric_stock.enabled = false

dw_schede_intervento_det_1.object.b_ricerca_cliente.enabled = false

cb_1.enabled = true

cb_controllo.enabled = true

cb_tes_campionamenti.enabled = true

dw_schede_intervento_det_5.object.b_ricerca_fornitore.enabled = false
end event

event rowfocuschanged;call super::rowfocuschanged;string ls_flag_classe_reso, ls_cod_tipo_causa, ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_gestione_fasi, ls_des_fase

long   ll_num_sequenza_corrente

if getrow() < 1 then
	return
end if

if getitemstring(getrow(),"flag_reso") = "S" then
	
	tab_folder.tabpage_reso.enabled = true
	
else
	if tab_folder.selectedtab = 4 then
		tab_folder.selecttab(1)
	end if
	tab_folder.tabpage_reso.enabled = false
end if

ll_num_sequenza_corrente = this.getitemnumber( this.getrow(), "num_sequenza_corrente")

ls_cod_tipo_causa = this.getitemstring( this.getrow(), "cod_tipo_causa")

if not isnull(ll_num_sequenza_corrente) and ll_num_sequenza_corrente > 0 and not isnull(ls_cod_tipo_causa) and ls_cod_tipo_causa <> "" then

	mle_stato.text = f_trova_stato_documento( ls_cod_tipo_causa, ll_num_sequenza_corrente)
	
else
	
	mle_stato.text = ""

end if
end event

event pcd_delete;call super::pcd_delete;if dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(),"flag_reso") = "N" then

	if tab_folder.selectedtab = 4 then
		tab_folder.selecttab(1)
	end if
	
	tab_folder.tabpage_reso.enabled = false
	
end if
end event

type dw_schede_intervento_det_2 from uo_cs_xx_dw within w_schede_intervento
integer x = 69
integer y = 640
integer width = 2743
integer height = 1116
integer taborder = 90
boolean bringtotop = true
string dataobject = "d_schede_intervento_det_2"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_operaio, ls_str
double ll_totale_ore, ll_costo_ora, ll_costo_totale,  ll_costo_risorsa
double ll_num_1, ll_num_2, ll_num_3
double ll_ore_viaggio
datetime ld_data_inizio, ld_data_fine
datetime lt_ora_inizio, lt_ora_fine

if i_extendmode then
   ld_data_inizio = this.getitemdatetime(this.getrow(), "data_inizio_intervento")
   ld_data_fine   = this.getitemdatetime(this.getrow(), "data_fine_intervento")
   lt_ora_inizio  = this.getitemdatetime(this.getrow(), "ora_inizio_intervento")
   lt_ora_fine    = this.getitemdatetime(this.getrow(), "ora_fine_intervento")
   ls_cod_operaio = dw_schede_intervento_lista.getitemstring(dw_schede_intervento_lista.getrow(),"cod_operaio")
   ll_ore_viaggio = this.getitemnumber(this.getrow(), "ore_viaggio")
   choose case i_colname
      case "data_inizio_intervento"
         ld_data_inizio = datetime(date(i_coltext))
      case "data_fine_intervento"
         ld_data_fine = datetime(date(i_coltext))
      case "ora_inizio_intervento"
         lt_ora_inizio = datetime(date(s_cs_xx.db_funzioni.data_neutra), time(i_coltext))
      case "ora_fine_intervento"
         lt_ora_fine = datetime(date(s_cs_xx.db_funzioni.data_neutra), time(i_coltext))
      case "ore_viaggio"
         ll_ore_viaggio = f_string_double(i_coltext)
	end choose

   if not isnull(ld_data_inizio) and &
      not isnull(ld_data_fine) and &
      not isnull(lt_ora_inizio) and &
      not isnull(ls_cod_operaio) and &
      not isnull(lt_ora_fine) then
 
		ll_num_1 = daysafter(date(ld_data_inizio), date(ld_data_fine)) * 24
      ll_num_2 = secondsafter(time(lt_ora_inizio), time(lt_ora_fine)) / 3600
      ll_num_3 = ll_ore_viaggio
		
		ll_totale_ore = ll_num_1 + ll_num_2 + ll_num_3
      this.setitem(this.getrow(),"totale_ore", ll_totale_ore)
   end if
end if
end event

type dw_schede_intervento_det_3 from uo_cs_xx_dw within w_schede_intervento
integer x = 91
integer y = 640
integer width = 2743
integer height = 1400
integer taborder = 110
boolean bringtotop = true
string dataobject = "d_schede_intervento_det_3"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

string ls_cod_prod_fornitore, ls_prodotto, ls_fornitore, ls_null
string ls_str1, ls_str2, ls_str3, ls_str4, ls_str5
long   ll_null, ll_riga
datetime   ld_null
double ld_progressivo

setnull(ls_null)
setnull(ll_null)
setnull(ld_null)


choose case i_colname


case "anno_bolla_acq"
   ls_str1 = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(), "cod_prodotto")
   
	if isnull(ls_str1) then ls_str1 = "%"
	
   this.setitem(this.getrow(), "num_bolla_acq", ll_null)
	
   this.setitem(this.getrow(), "prog_riga_bolla_acq", ll_null)
	
   f_PO_LoadDDLB_DW(dw_schede_intervento_det_3,"num_bolla_acq",sqlca,&
              "det_bol_acq","num_bolla_acq","num_bolla_acq",&
              "(det_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
               (det_bol_acq.anno_bolla_acq = "+i_coltext+")  and &
               (det_bol_acq.cod_prodotto like '"+ls_str1+"')" )


case "num_bolla_acq"
   ls_str1 = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(), "cod_prodotto")
	
   ls_str2 = string(int(this.getitemnumber(this.getrow(), "anno_bolla_acq")))
	
   this.setitem(this.getrow(), "prog_riga_bolla_acq", ll_null)
	
   f_PO_LoadDDLB_DW(dw_schede_intervento_det_3,"prog_riga_bolla_acq",sqlca,&
              "det_bol_acq","prog_riga_bolla_acq","prog_riga_bolla_acq",&
              "(det_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
               (det_bol_acq.anno_bolla_acq = "+ls_str2+")  and &
               (det_bol_acq.prodotto like "+ls_str1+")  and &
               (det_bol_acq.num_bolla_acq = "+i_coltext+") " )	
	
case "anno_registrazione"
   ls_str1 = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(), "cod_prodotto")
   if isnull(ls_str1) then ls_str1 = "%"
   this.setitem(this.getrow(), "num_registrazione", ll_null)
   this.setitem(this.getrow(), "prog_riga_fat_ven", ll_null)
   f_PO_LoadDDLB_DW(dw_schede_intervento_det_3,"num_registrazione",sqlca,&
              "det_fat_ven","num_registrazione","num_registrazione",&
              "(det_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
              "(det_fat_ven.cod_prodotto = '" + ls_str1 + "') and " + &
              "(det_fat_ven.anno_registrazione = "+i_coltext+")" )


case "num_registrazione"
   ls_str1 = dw_schede_intervento_det_1.getitemstring(dw_schede_intervento_det_1.getrow(), "cod_prodotto")
   if isnull(ls_str1) then ls_str1 = "%"
   ls_str2 = string(int(this.getitemnumber(this.getrow(), "anno_registrazione")))
   this.setitem(this.getrow(), "prog_riga_ordine_acq", ll_null)
   f_PO_LoadDDLB_DW(dw_schede_intervento_det_3,"prog_riga_fat_ven",sqlca,&
              "det_fat_ven","prog_riga_fat_ven","prog_riga_fat_ven",&
              "(det_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "') and &
               (det_fat_ven.anno_registrazione = "+ls_str2+")  and &
               (det_fat_ven.prodotto like "+ls_str1+")  and &
               (det_fat_ven.num_registrazione = "+i_coltext+") " )

end choose


end if   // si riferisce alla condizione iniziale di test della variabile i_extendmode
end event

type dw_schede_intervento_det_4 from uo_cs_xx_dw within w_schede_intervento
integer x = 46
integer y = 640
integer width = 2789
integer height = 920
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_schede_intervento_det_5"
boolean border = false
end type

type dw_schede_intervento_det_5 from uo_cs_xx_dw within w_schede_intervento
integer x = 46
integer y = 660
integer width = 2789
integer height = 1620
integer taborder = 120
boolean bringtotop = true
string dataobject = "d_schede_intervento_det_4"
boolean border = false
end type

