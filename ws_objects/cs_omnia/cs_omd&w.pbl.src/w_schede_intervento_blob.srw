﻿$PBExportHeader$w_schede_intervento_blob.srw
$PBExportComments$gestione documenti delle schede di intervento
forward
global type w_schede_intervento_blob from w_cs_xx_principale
end type
type dw_schede_intervento_blob from uo_cs_xx_dw within w_schede_intervento_blob
end type
type cb_note_esterne from commandbutton within w_schede_intervento_blob
end type
end forward

global type w_schede_intervento_blob from w_cs_xx_principale
integer width = 2875
integer height = 1040
string title = "Documenti non Conformità"
dw_schede_intervento_blob dw_schede_intervento_blob
cb_note_esterne cb_note_esterne
end type
global w_schede_intervento_blob w_schede_intervento_blob

type variables
long il_anno, il_num
end variables

event pc_setwindow;call super::pc_setwindow;dw_schede_intervento_blob.set_dw_key("cod_azienda")
dw_schede_intervento_blob.set_dw_options(sqlca, &
											i_openparm, &
											c_scrollparent, &
											c_default)

iuo_dw_main = dw_schede_intervento_blob
end event

on w_schede_intervento_blob.create
int iCurrent
call super::create
this.dw_schede_intervento_blob=create dw_schede_intervento_blob
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_schede_intervento_blob
this.Control[iCurrent+2]=this.cb_note_esterne
end on

on w_schede_intervento_blob.destroy
call super::destroy
destroy(this.dw_schede_intervento_blob)
destroy(this.cb_note_esterne)
end on

type dw_schede_intervento_blob from uo_cs_xx_dw within w_schede_intervento_blob
integer x = 23
integer y = 20
integer width = 2789
integer height = 800
integer taborder = 10
string dataobject = "d_schede_intervento_blob"
boolean vscrollbar = true
end type

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false	
end event

event pcd_new;call super::pcd_new;long ll_null

setnull(ll_null)

setitem( getrow(), "flag_blocco", "N")
setitem( getrow(), "prog_mimetype", ll_null)

cb_note_esterne.enabled = false
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error


il_anno = i_parentdw.getitemnumber(i_parentdw.getrow(),"anno_reg_intervento")
il_num = i_parentdw.getitemnumber(i_parentdw.getrow(),"num_reg_intervento")

parent.title = "Documenti - Schede Intervento" + string(il_anno) + "/" + string(il_num)

l_Error = Retrieve(s_cs_xx.cod_azienda,il_anno,il_num)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_save;call super::pcd_save;cb_note_esterne.enabled = true
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_progressivo, ll_num_protocollo


for ll_i = 1 to this.rowcount()
	
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemnumber(ll_i,"anno_reg_intervento")) then
		setitem(ll_i,"anno_reg_intervento",il_anno)
	end if
	
	if isnull(getitemnumber(ll_i,"num_reg_intervento")) then
		setitem(ll_i,"num_reg_intervento",il_num)
	end if
	
	if isnull(getitemnumber(ll_i,"progressivo")) then
		
		select max(progressivo)
		into	 :ll_progressivo
		from 	 schede_intervento_blob
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 anno_reg_intervento = :il_anno and
				 num_reg_intervento = :il_num;
		
		if sqlca.sqlcode < 0 then
			messagebox("OMNIA","Errore in lettura massimo progressivo documento: " + sqlca.sqlerrtext,stopsign!)
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) then
			ll_progressivo = 0
		end if
		
		ll_progressivo ++
		
		setitem(ll_i,"progressivo",ll_progressivo)
		
	end if		
next
end event

event pcd_view;call super::pcd_view;cb_note_esterne.enabled = true
end event

event updatestart;call super::updatestart;long ll_i, ll_null

setnull(ll_null)
	
for ll_i = 1 to rowcount()
	
	if getitemstatus(ll_i,0,primary!) = newmodified! then
		
		setitem( ll_i, "prog_mimetype", 1)
		
	end if
next
end event

type cb_note_esterne from commandbutton within w_schede_intervento_blob
integer x = 2446
integer y = 840
integer width = 366
integer height = 80
integer taborder = 3
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;long    ll_i, ll_progressivo, ll_prog_mimetype

string  ls_db

integer li_risposta

transaction sqlcb

blob    lbl_null

setnull(lbl_null)

ll_i = dw_schede_intervento_blob.getrow()

if ll_i > 0 then

	ll_progressivo = dw_schede_intervento_blob.getitemnumber(ll_i, "progressivo")
	ll_prog_mimetype = dw_schede_intervento_blob.getitemnumber( ll_i, "prog_mimetype")
	
	// 15-07-2002 modifiche Michela: controllo l'enginetype
	ls_db = f_db()
	
	if ls_db = "MSSQL"  then
	
		li_risposta = f_crea_sqlcb(sqlcb)
	
		selectblob blob
		into       :s_cs_xx.parametri.parametro_bl_1
		from       schede_intervento_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_reg_intervento = :il_anno and 
					  num_reg_intervento = :il_num and 
					  progressivo = :ll_progressivo
		using      sqlcb;
		
		if sqlcb.sqlcode <> 0 then
			s_cs_xx.parametri.parametro_bl_1 = lbl_null
		end if
			
		destroy sqlcb;	
			
	else
	
		selectblob blob
		into       :s_cs_xx.parametri.parametro_bl_1
		from       schede_intervento_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_reg_intervento = :il_anno and 
					  num_reg_intervento = :il_num and 
					  progressivo = :ll_progressivo;
		
		if sqlca.sqlcode <> 0 then
			s_cs_xx.parametri.parametro_bl_1 = lbl_null
		end if
	
	end if
	
	
	//ll_prog_mimetype = dw_schede_intervento_blob.getitemnumber( ll_i, "prog_mimetype")

// *** se il blob non è nullo ed esiste il prog mimetype: sono documenti intranet
// *** se il blob non è nullo ma il progressivo si: sono documenti client/server
// *** se il blob è nullo: procedo con l'inserimento intranet

if isnull(s_cs_xx.parametri.parametro_bl_1) or ( not isnull(s_cs_xx.parametri.parametro_bl_1) and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 ) then


	
	s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype

	window_open(w_ole_documenti, 0)

	ll_prog_mimetype = s_cs_xx.parametri.parametro_d_1

	if not isnull(s_cs_xx.parametri.parametro_bl_1) and len(s_cs_xx.parametri.parametro_bl_1) > 0  and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
		
			if not isnull(s_cs_xx.parametri.parametro_bl_1) then
			
				if ls_db = "MSSQL"  then
					
					li_risposta = f_crea_sqlcb(sqlcb)
					
					updateblob schede_intervento_blob
					set        blob = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_reg_intervento = :il_anno and 
								  num_reg_intervento = :il_num and 
								  progressivo = :ll_progressivo
					using      sqlcb;			
					
					if sqlcb.sqlcode <> 0 then
						messagebox("OMNIA","Errore nell'inserimento del file: " + sqlcb.sqlerrtext,stopsign!)
						return -1
					end if
					
					destroy sqlcb;
					
				else
					
					updateblob schede_intervento_blob
					set        blob = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_reg_intervento = :il_anno and 
								  num_reg_intervento = :il_num and 
								  progressivo = :ll_progressivo;
								 
					if sqlca.sqlcode <> 0 then
						messagebox("OMNIA","Errore nell'inserimento del file: " + sqlcb.sqlerrtext,stopsign!)
						return -1
					end if
			
				end if
				// CLAUDIA 19/09/06 ELIMINATO PERCHè IN ORACLE CONTROLLA E VEDE CHE C'è STATA UNA MODIFICA E QUINDI MANCA INTEGRITà DATI
	//			update     schede_intervento_blob
	//			set        prog_mimetype = :ll_prog_mimetype
	//			where      cod_azienda = :s_cs_xx.cod_azienda and
	//						  anno_reg_intervento = :il_anno and 
	//						  num_reg_intervento = :il_num and 
	//						  progressivo = :ll_progressivo;
	//						  
				commit;
				
				dw_schede_intervento_blob.setitem( ll_i, "prog_mimetype", ll_prog_mimetype)
				
				parent.triggerevent("pc_save")
				
				parent.triggerevent("pc_view")			
				
				commit;
			end if
		end if
	end if
end if
end event

