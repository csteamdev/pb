﻿$PBExportHeader$w_schede_intervento_comunicazioni.srw
$PBExportComments$comunicazioni schede intervento
forward
global type w_schede_intervento_comunicazioni from w_cs_xx_risposta
end type
type dw_schede_intervento_comunicazioni from uo_cs_xx_dw within w_schede_intervento_comunicazioni
end type
end forward

global type w_schede_intervento_comunicazioni from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 2985
integer height = 980
string title = "Comunicazioni Cicli di Vita"
dw_schede_intervento_comunicazioni dw_schede_intervento_comunicazioni
end type
global w_schede_intervento_comunicazioni w_schede_intervento_comunicazioni

type prototypes
function long GetTempPathA  (Long nBufferLength, ref String lpBuffer) Library  "kernel32.dll" alias for "GetTempPathA;Ansi"


end prototypes

type variables
long il_anno_reg_intervento, il_num_reg_intervento, il_anno_non_conf, il_num_non_conf, il_prog_budget
end variables

on w_schede_intervento_comunicazioni.create
int iCurrent
call super::create
this.dw_schede_intervento_comunicazioni=create dw_schede_intervento_comunicazioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_schede_intervento_comunicazioni
end on

on w_schede_intervento_comunicazioni.destroy
call super::destroy
destroy(this.dw_schede_intervento_comunicazioni)
end on

event pc_setwindow;call super::pc_setwindow;dw_schede_intervento_comunicazioni.set_dw_options( sqlca, &
                                            			pcca.null_object, &
														  			c_nonew + c_default, &
														  			c_default)
																	  
il_anno_reg_intervento = s_cs_xx.parametri.parametro_ul_1																	  

il_num_reg_intervento = s_cs_xx.parametri.parametro_ul_2

il_prog_budget = s_cs_xx.parametri.parametro_ul_3

il_anno_non_conf = s_cs_xx.parametri.parametro_d_1

il_num_non_conf = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_ul_1)

setnull(s_cs_xx.parametri.parametro_ul_2)

setnull(s_cs_xx.parametri.parametro_d_1)

setnull(s_cs_xx.parametri.parametro_d_2)
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_schede_intervento_comunicazioni, &
                  "cod_destinatario", &
						sqlca, &
                  "tab_ind_dest", &
						"cod_destinatario", &
						"des_destinatario", &
                  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
						
f_PO_LoadDDDW_DW( dw_schede_intervento_comunicazioni, &
                  "cod_utente", &
						sqlca, &
                  "utenti", &
						"cod_utente", &
						"nome_cognome", &
                  "")						

end event

type dw_schede_intervento_comunicazioni from uo_cs_xx_dw within w_schede_intervento_comunicazioni
integer x = 23
integer y = 20
integer width = 2903
integer height = 840
integer taborder = 0
string dataobject = "d_det_liste_dist_fasi_com_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve( s_cs_xx.cod_azienda, il_anno_reg_intervento, il_num_reg_intervento, il_anno_non_conf, il_num_non_conf, il_prog_budget)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

