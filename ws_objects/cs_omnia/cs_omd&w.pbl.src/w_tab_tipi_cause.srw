﻿$PBExportHeader$w_tab_tipi_cause.srw
$PBExportComments$Finestra tabella cause
forward
global type w_tab_tipi_cause from w_cs_xx_principale
end type
type dw_tab_tipi_causa_lista from uo_cs_xx_dw within w_tab_tipi_cause
end type
type dw_tab_tipi_causa_det from uo_cs_xx_dw within w_tab_tipi_cause
end type
end forward

global type w_tab_tipi_cause from w_cs_xx_principale
integer width = 2743
integer height = 1376
string title = "Tabella Tipi Cause"
dw_tab_tipi_causa_lista dw_tab_tipi_causa_lista
dw_tab_tipi_causa_det dw_tab_tipi_causa_det
end type
global w_tab_tipi_cause w_tab_tipi_cause

on w_tab_tipi_cause.create
int iCurrent
call super::create
this.dw_tab_tipi_causa_lista=create dw_tab_tipi_causa_lista
this.dw_tab_tipi_causa_det=create dw_tab_tipi_causa_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_tipi_causa_lista
this.Control[iCurrent+2]=this.dw_tab_tipi_causa_det
end on

on w_tab_tipi_cause.destroy
call super::destroy
destroy(this.dw_tab_tipi_causa_lista)
destroy(this.dw_tab_tipi_causa_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_tipi_causa_lista.set_dw_key("cod_azienda")
dw_tab_tipi_causa_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_tab_tipi_causa_det.set_dw_options(sqlca, &
                                  dw_tab_tipi_causa_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
iuo_dw_main = dw_tab_tipi_causa_lista

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_tipi_causa_det, &
                 "cod_tipo_lista_dist", &
					  sqlca, &
                 "tab_tipi_liste_dist", &
					  "cod_tipo_lista_dist", &
					  "descrizione", &
					  "")
					  
f_po_loaddddw_dw( dw_tab_tipi_causa_det, "cod_lista_dist", sqlca, &
						"tes_liste_dist", &
						"cod_lista_dist", &
						"des_lista_dist", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
end event

type dw_tab_tipi_causa_lista from uo_cs_xx_dw within w_tab_tipi_cause
integer x = 23
integer y = 20
integer width = 2651
integer height = 500
string dataobject = "d_tab_tipi_causa_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

type dw_tab_tipi_causa_det from uo_cs_xx_dw within w_tab_tipi_cause
integer x = 23
integer y = 540
integer width = 2651
integer height = 720
integer taborder = 2
string dataobject = "d_tab_tipi_causa_det"
end type

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

choose case i_colname
		
	case "cod_tipo_lista_dist"
			
			setitem( getrow(), "cod_lista_dist", ls_null)
			
			f_po_loaddddw_dw( dw_tab_tipi_causa_det, "cod_lista_dist", sqlca, &
									"tes_liste_dist", &
									"cod_lista_dist", &
									"des_lista_dist", &
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + i_coltext + "' ")
										 
end choose	

end event

