﻿$PBExportHeader$w_vista_azioni_correttive.srw
$PBExportComments$Windows Vista Non Conformità in Attesa di  Azioni Correttive  OK
forward
global type w_vista_azioni_correttive from w_cs_xx_principale
end type
type dw_vista_azioni_correttive from uo_cs_xx_dw within w_vista_azioni_correttive
end type
end forward

global type w_vista_azioni_correttive from w_cs_xx_principale
int Width=3141
int Height=1149
boolean TitleBar=true
string Title="Vista Non Conformità in Attesa di Azione Correttiva"
dw_vista_azioni_correttive dw_vista_azioni_correttive
end type
global w_vista_azioni_correttive w_vista_azioni_correttive

on activate;call w_cs_xx_principale::activate;dw_vista_azioni_correttive.triggerevent("pcd_retrieve")
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_vista_azioni_correttive.set_dw_options(sqlca,pcca.null_object,c_nonew+c_nodelete+c_nomodify,c_default)

end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_vista_azioni_correttive,"cod_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1","anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_vista_azioni_correttive,"cod_errore",sqlca,&
                 "tab_difformita","cod_errore","des_difformita","")

f_PO_LoadDDDW_DW(dw_vista_azioni_correttive,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto","anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_vista_azioni_correttive,"cod_trattamento",sqlca,&
                 "tab_trattamenti","cod_trattamento","des_trattamento","")

f_PO_LoadDDDW_DW(dw_vista_azioni_correttive,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura","tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")





end on

on w_vista_azioni_correttive.create
int iCurrent
call w_cs_xx_principale::create
this.dw_vista_azioni_correttive=create dw_vista_azioni_correttive
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_vista_azioni_correttive
end on

on w_vista_azioni_correttive.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_vista_azioni_correttive)
end on

type dw_vista_azioni_correttive from uo_cs_xx_dw within w_vista_azioni_correttive
event evento_1 pbm_custom01
int X=23
int Y=21
int Width=3063
int Height=1001
string DataObject="d_vista_azioni_correttive"
boolean HScrollBar=true
boolean VScrollBar=true
boolean HSplitScroll=true
boolean LiveScroll=true
end type

on evento_1;call uo_cs_xx_dw::evento_1;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_pickedrow;call uo_cs_xx_dw::pcd_pickedrow;s_cs_xx.parametri.parametro_i_1 = getitemnumber(dw_vista_azioni_correttive.getrow(),"num_non_conf")
s_cs_xx.parametri.parametro_d_1 = getitemnumber(dw_vista_azioni_correttive.getrow(),"anno_non_conf")
if s_cs_xx.parametri.parametro_i_1 > 0 then
   window_open(w_input_azione_correttiva,0)
end if

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

