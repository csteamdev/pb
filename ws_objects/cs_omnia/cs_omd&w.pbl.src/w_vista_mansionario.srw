﻿$PBExportHeader$w_vista_mansionario.srw
$PBExportComments$Window Vista Dettaglio Mansionario
forward
global type w_vista_mansionario from w_cs_xx_risposta
end type
type cb_1 from uo_cb_close within w_vista_mansionario
end type
type dw_vista_mansionario from uo_cs_xx_dw within w_vista_mansionario
end type
end forward

global type w_vista_mansionario from w_cs_xx_risposta
int Width=1879
int Height=1005
boolean TitleBar=true
string Title="Mansionario"
cb_1 cb_1
dw_vista_mansionario dw_vista_mansionario
end type
global w_vista_mansionario w_vista_mansionario

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_vista_mansionario.set_dw_options(sqlca,pcca.null_object, c_nonew +c_nomodify + &
                                    c_nodelete+ c_disableCC + c_disableCCinsert ,c_default)

end on

on w_vista_mansionario.create
int iCurrent
call w_cs_xx_risposta::create
this.cb_1=create cb_1
this.dw_vista_mansionario=create dw_vista_mansionario
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_1
this.Control[iCurrent+2]=dw_vista_mansionario
end on

on w_vista_mansionario.destroy
call w_cs_xx_risposta::destroy
destroy(this.cb_1)
destroy(this.dw_vista_mansionario)
end on

on pc_setddlb;call w_cs_xx_risposta::pc_setddlb;f_PO_LoadDDDW_DW(dw_vista_mansionario,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione", &
                 "anag_divisioni.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end on

type cb_1 from uo_cb_close within w_vista_mansionario
int X=1486
int Y=761
int TabOrder=20
string Text="&Chiudi"
end type

type dw_vista_mansionario from uo_cs_xx_dw within w_vista_mansionario
int X=1
int Y=1
int Width=1852
int Height=901
int TabOrder=10
string DataObject="d_vista_mansionario"
boolean Border=false
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error


l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_s_1)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

