﻿$PBExportHeader$f_nuovo_doc.srf
$PBExportComments$Funzione di creazione nuovo record e nuovo documento
global type f_nuovo_doc from function_object
end type

forward prototypes
global function string f_nuovo_doc (decimal progressivo)
end prototypes

global function string f_nuovo_doc (decimal progressivo);//	Funzione di creazione nuovo documento e nuovo record in tabella documenti
// usata per creare nuove revisioni di documenti
//
// nome: f_nuovo_doc
// tipo: string 
//  
//	Variabili passate: 		nome					 tipo			passaggio per
//							
//							 progressivo 				long				valore
//							 finestra					window			valore
//
//		Creata il 14-01-96 
//		Autore Diego Ferrari
//

string l_password,l_percorso_word,l_nome_doc,l_nome_modello,ls_resp_divisione,l_approvato_da,l_risposta, &
		 l_autorizzato_da,l_validato_da,l_area,l_flag_uso,l_path_documento,l_dati,l_versione_edizione,l_nuovo_nome_doc, &
       l_num_contratto,l_cod_fornitore,l_desc_progetto,l_prodotto,ls_pdoc,ls_pmod,ls_pcom,ls_nome_divisione, &
       ls_flag_recuperato, ls_validato_da, ls_autorizzato_da, ls_modifiche, ls_livello_documento, &
       ls_des_documento, ls_path_documento_esterno, ls_null, ls_cod_resp_conserv_doc , &
		 ls_periodo_conserv_unita_tempo, ls_flag_doc_registraz_qualita, ls_flag_catalogazione, &
		 ls_flag_doc_automatico, ls_flag_cap_manuale, ls_nome_modello_cfr,&
		 ls_nome_documento,ls_approvato_da,ls_verificato_da,ls_cod_approvato_da,ls_cod_verificato_da, & 
 	    ls_nome_modello,ls_redatto_da, ls_cod_redatto_da,ls_valore,ls_db
datetime l_emesso_il,l_autorizzato_il,l_oggi,l_data_contratto, ld_null, ldt_data_verifica, &
			ldt_approvato_il,ldt_verificato_il,ldt_redatto_il
integer l_num_max_edizione,li_nr_edizione,li_nr_versione
decimal l_num_versione,l_num_edizione,l_valido_per
long l_progr, ll_progr_lista_dist, ll_prog_ordinamento_livello, ll_periodo_conserv
blob lbl_documento_esterno
integer li_versione_word,li_risposta, li_ris

transaction sqlcb

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "vew", ls_valore)

li_versione_word = integer(ls_valore)

l_risposta=f_parametri(l_password,l_percorso_word,ls_pdoc,ls_pmod,l_num_max_edizione, ls_pcom)

l_oggi=datetime(today())
setnull(ld_null)
setnull(ls_null)

SELECT mansionari.cod_resp_divisione, 
		 mansionari.cod_divisione 
INTO:ls_resp_divisione,
    :ls_nome_divisione & 
FROM mansionari
WHERE mansionari.cod_azienda=:s_cs_xx.cod_azienda AND 
	   mansionari.cod_utente =:s_cs_xx.cod_utente;

choose case sqlca.sqlcode
	case 100
      g_mb.messagebox("OMNIA","Attenzione: l'utente con cui ci si è connessi non ha associato alcun mansionario: verificare nella maschera mansionari!", StopSign!)
		return "ok"
	case is > 0
		g_mb.messagebox("OMNIA","Errore durante verifica mansionari: " + SQLCA.SQLErrText,StopSign!)
      return "ok"
end choose

SELECT documenti.progr,
		 documenti.nome_documento, 
		 documenti.num_versione,
		 documenti.num_edizione,
  		 documenti.nome_modello,
		 documenti.emesso_il,
		 documenti.cod_resp_divisione,
		 documenti.approvato_da,
		 documenti.autorizzato_il,
		 documenti.valido_per,
		 documenti.cod_area_aziendale,
		 documenti.flag_uso,
		 documenti.flag_recuperato,
		 documenti.validato_da,
		 documenti.autorizzato_da,
		 documenti.modifiche,
		 documenti.livello_documento,
		 documenti.progr_lista_dist,
		 documenti.des_documento,
		 documenti.path_documento_esterno,
		 prog_ordinamento_livello,
		 cod_resp_conserv_doc,
		 periodo_conserv_unita_tempo,
		 periodo_conserv,
		 flag_doc_registraz_qualita,
		 flag_catalogazione,
		 data_verifica,
		 flag_doc_automatico,
		 flag_cap_manuale
INTO	 :l_progr, 
		 :l_nome_doc,
    	 :l_num_versione,
    	 :l_num_edizione,
	    :l_nome_modello,
		 :l_emesso_il,
		 :ls_resp_divisione,
		 :l_approvato_da,
		 :l_autorizzato_il,
		 :l_valido_per,
		 :l_area,
		 :l_flag_uso,
		 :ls_flag_recuperato,
		 :ls_validato_da,
		 :ls_autorizzato_da,
		 :ls_modifiche,
		 :ls_livello_documento,
		 :ll_progr_lista_dist,
		 :ls_des_documento,
		 :ls_path_documento_esterno,
		 :ll_prog_ordinamento_livello,
		 :ls_cod_resp_conserv_doc,
		 :ls_periodo_conserv_unita_tempo,
		 :ll_periodo_conserv,
		 :ls_flag_doc_registraz_qualita,
		 :ls_flag_catalogazione,
		 :ldt_data_verifica,
		 :ls_flag_doc_automatico,
		 :ls_flag_cap_manuale
FROM 	 documenti & 
WHERE  documenti.cod_azienda=:s_cs_xx.cod_azienda AND 
	    documenti.progr =:progressivo;

choose case sqlca.sqlcode
	case 100
      g_mb.messagebox("OMNIA","Attenzione: il documento codice:" + string(progressivo) + "cercato non esiste.", StopSign!)
		return "ok"
	case is > 0
		g_mb.messagebox("OMNIA","Errore durante ricerca documento: " + string(progressivo) + SQLCA.SQLErrText,StopSign!)
      return "ok"
end choose


if ls_db = "MSSQL" then
	
	li_ris = f_crea_sqlcb(sqlcb)
		
	selectblob documenti.blob
	      into :lbl_documento_esterno
	      from documenti
	     where documenti.cod_azienda=:s_cs_xx.cod_azienda AND 
			     documenti.progr =:progressivo
	using      sqlcb;
				  
	if sqlcb.sqlcode<>0 then
		g_mb.messagebox("OMNIA","Errore in fase di ricerca documento: " + sqlcb.SQLErrText,Information!)
		destroy sqlcb;
		return "ok"
	end if
	
	destroy sqlcb;
	
else
	
	selectblob documenti.blob
	      into :lbl_documento_esterno
	      from documenti
	     where documenti.cod_azienda=:s_cs_xx.cod_azienda AND 
			     documenti.progr =:progressivo;
				  
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("OMNIA","Errore in fase di ricerca documento: " + SQLCA.SQLErrText,Information!)
		return "ok"
	end if

end if

CHOOSE CASE l_flag_uso	
	case "C"
		select cod_fornitore,
				 num_contratto,
				 data_contratto,
			    path_documento
		into  :l_cod_fornitore,
				:l_num_contratto,
				:l_data_contratto,
				:l_path_documento &
		from contratti &
		where contratti.cod_azienda=:s_cs_xx.cod_azienda and
				contratti.progr=:progressivo;
	case "G"
		select doc_generici.path_documento
		into :l_path_documento &
		from doc_generici &
		where doc_generici.cod_azienda=:s_cs_xx.cod_azienda and
				doc_generici.progr=:progressivo;
	case "P"
		select desc_progetto,
				 cod_prodotto,
				 path_documento
		into  :l_desc_progetto,
				:l_prodotto,
				:l_path_documento &
		from progetti &
		where progetti.cod_azienda=:s_cs_xx.cod_azienda and
				progetti.progr=:progressivo;
END CHOOSE

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Errore nel DB",SQLCA.SQLErrText,Information!)
end if

if l_num_edizione=l_num_max_edizione then
	l_num_edizione=0
	l_num_versione++
else
	l_num_edizione++		  
end if

if li_versione_word = 6 then
	l_risposta=f_word("fileapri " + char(34) + l_path_documento + char(34) +",.passworddocumento="+char(34)+l_password+char(34),l_percorso_word)
else
	l_risposta=f_word("fileopen " + char(34) + l_path_documento + char(34) +",.passworddoc="+char(34)+l_password+char(34),l_percorso_word)
end if

if l_risposta = "no" then	
	rollback;
	return "no"
end if 

if len(string(l_num_edizione))<2 then
	l_versione_edizione=string(l_num_versione) + "0" + string(l_num_edizione)
else	
	l_versione_edizione=string(l_num_versione) + string(l_num_edizione) 
end if

l_path_documento=left(l_path_documento,len(l_path_documento)-3) + l_versione_edizione

if li_versione_word = 6 then
	l_risposta=f_word("filesalvaconnome.nome=" + char(34) + l_path_documento + char(34) +",.password=" +char(34)+ l_password +char(34),l_percorso_word) 
else
	l_risposta=f_word("filesaveas.name=" + char(34) + l_path_documento + char(34) +",.password=" +char(34)+ l_password +char(34),l_percorso_word) 
end if

if l_risposta = "no" then	
	rollback;
	return "no"
end if

if li_versione_word = 6 then
	l_risposta=f_word("filechiuditutto",l_percorso_word)
else
	l_risposta=f_word("filecloseall",l_percorso_word)
end if

if l_risposta = "no" then	
	rollback;
	return "no"
end if
l_progr=f_maxprogr()

INSERT INTO documenti
		 (cod_azienda,
		 progr,
		 nome_documento, 
		 num_versione,
		 num_edizione,
  		 nome_modello,
		 emesso_il,
		 cod_resp_divisione,
		 approvato_da,
		 autorizzato_il,
		 valido_per,
		 cod_area_aziendale,
		 flag_uso,
		 flag_recuperato,
		 validato_da,
		 autorizzato_da,
		 modifiche,
		 livello_documento,
		 progr_lista_dist,
		 des_documento,
		 path_documento_esterno,
		 prog_ordinamento_livello,
		 cod_resp_conserv_doc,
		 periodo_conserv_unita_tempo,
		 periodo_conserv,
		 flag_doc_registraz_qualita,
		 flag_catalogazione,
		 data_verifica,
		 flag_doc_automatico,
		 flag_cap_manuale)
VALUES(:s_cs_xx.cod_azienda,
		 :l_progr, 
		 :l_nome_doc,
    	 :l_num_versione,
    	 :l_num_edizione,
	    :l_nome_modello,
		 :l_oggi,
		 :ls_resp_divisione,
		 :ls_null,
		 :ld_null,
		 0,
		 :l_area,
		 :l_flag_uso,
		 'N',
		 :ls_null,
		 :ls_null,
		 :ls_null,
		 :ls_livello_documento,
		 :ll_progr_lista_dist,
		 :ls_des_documento,
		 :ls_path_documento_esterno,
		 :ll_prog_ordinamento_livello,
		 :ls_cod_resp_conserv_doc,
		 :ls_periodo_conserv_unita_tempo,
		 :ll_periodo_conserv,
		 :ls_flag_doc_registraz_qualita,
		 :ls_flag_catalogazione,
		 :ld_null,
		 :ls_flag_doc_automatico,
		 :ls_flag_cap_manuale);

if sqlca.sqlcode<>0 then
	g_mb.messagebox("OMNIA","Errore durante creazione nuova revisione: " + SQLCA.SQLErrText,Information!)
   rollback;
	return "ok"
end if

INSERT INTO paragrafi_iso_documenti  
 		    ( cod_azienda,   
		      cod_paragrafo,   
		      progr,   
		      flag_rif_primario )  
    SELECT  paragrafi_iso_documenti.cod_azienda,   
			   paragrafi_iso_documenti.cod_paragrafo,   
			   paragrafi_iso_documenti.progr - paragrafi_iso_documenti.progr + :l_progr,   
			   paragrafi_iso_documenti.flag_rif_primario  
	  FROM   paragrafi_iso_documenti
	 WHERE ( paragrafi_iso_documenti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
			 ( paragrafi_iso_documenti.progr = :progressivo )   ;
if sqlca.sqlcode<>0 then
	g_mb.messagebox("OMNIA","Errore durante creazione nuova revisione: " + SQLCA.SQLErrText,Information!)
   rollback;
	return "ok"
end if

if not isnull(lbl_documento_esterno) then
	
	if ls_db = "MSSQL" then
		
		li_ris = f_crea_sqlcb(sqlcb)
		
		updateblob documenti
		set        documenti.blob = :lbl_documento_esterno
		where      documenti.cod_azienda = :s_cs_xx.cod_azienda and
					  documenti.progr = :l_progr
		using      sqlcb;
	
		if sqlcb.sqlcode<>0 then
			g_mb.messagebox("Errore nel DB","Errore durante creazione nuova revisione: " + sqlcb.SQLErrText,Information!)
			rollback;
			destroy sqlcb;
			return "ok"
		end if
		
		destroy sqlcb;
		
	else
		
		updateblob documenti
		set        documenti.blob = :lbl_documento_esterno
		where      documenti.cod_azienda = :s_cs_xx.cod_azienda and
					  documenti.progr = :l_progr;
	
		if sqlca.sqlcode<>0 then
			g_mb.messagebox("Errore nel DB","Errore durante creazione nuova revisione: " + SQLCA.SQLErrText,Information!)
			rollback;
			return "ok"
		end if
		
	end if
	
end if

CHOOSE CASE l_flag_uso	
	case "C"
		INSERT INTO contratti
				 (cod_azienda,
				 progr,
				 cod_fornitore,
				 num_contratto,
				 data_contratto,
				 path_documento)
		VALUES (:s_cs_xx.cod_azienda,
				  :l_progr,
				  :l_cod_fornitore,
				  :l_num_contratto,
				  :l_data_contratto,
				  :l_path_documento);
	case "G"
			INSERT INTO doc_generici
				 (cod_azienda,
				 progr,
				 path_documento)
			VALUES (:s_cs_xx.cod_azienda,
				  :l_progr,
				  :l_path_documento);
	case "P"
			INSERT INTO progetti
				 (cod_azienda,
				 progr,
				 desc_progetto,
				 cod_prodotto,
				 path_documento)
			VALUES (:s_cs_xx.cod_azienda,
				  :l_progr,
				  :l_desc_progetto,
				  :l_prodotto,
				  :l_path_documento);
END CHOOSE

if sqlca.sqlcode<>0 then
	g_mb.messagebox("OMNIA","Errore durante creazione nuova revisione: " + SQLCA.SQLErrText,Information!)
   rollback;
	return "ok"
end if


INSERT INTO doc_divisioni  
          ( cod_azienda,   
            progr,   
            cod_divisione )  
VALUES   ( :s_cs_xx.cod_azienda,   
           :l_progr,   
           :ls_nome_divisione )  ;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("OMNIA","Errore durante creazione nuova revisione: " + SQLCA.SQLErrText,Information!)
   rollback;
	return "ok"
end if

SELECT nome_documento,
	    num_versione,
		 num_edizione,
		 flag_doc_automatico,
		 autorizzato_il,
		 autorizzato_da,
		 approvato_da,
		 data_verifica,
		 nome_modello,
		 cod_resp_divisione,
		 emesso_il
 INTO  :ls_nome_documento,
		 :li_nr_versione,
		 :li_nr_edizione,
		 :ls_flag_doc_automatico,
		 :ldt_approvato_il,
		 :ls_approvato_da,
		 :ls_verificato_da,	
		 :ldt_verificato_il,
		 :ls_nome_modello,
		 :ls_redatto_da,
		 :ldt_redatto_il
FROM   documenti
WHERE  cod_azienda = :s_cs_xx.cod_azienda
AND    progr = :l_progr;


if li_versione_word = 6 then		 
	l_risposta=f_word("fileapri " + char(34) + l_path_documento + char(34) +",.passworddocumento="+char(34)+l_password+char(34),l_percorso_word)
else
	l_risposta=f_word("fileopen " + char(34) + l_path_documento + char(34) +",.passworddoc="+char(34)+l_password+char(34),l_percorso_word)
end if

if l_risposta = "no" then	
	rollback;
	return "no"
end if 

if ls_flag_doc_automatico="S" then
	select stringa
	into   :ls_nome_modello_cfr
	from   parametri_omnia
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_parametro='MLC';
	
   select cognome
	into   :ls_approvato_da
	from   mansionari
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_utente=:ls_cod_approvato_da;

	select cognome
	into   :ls_redatto_da
	from   mansionari
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_resp_divisione=:ls_cod_redatto_da;

	select cognome
	into   :ls_verificato_da
	from   mansionari
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_utente=:ls_cod_verificato_da;

   if ls_nome_modello_cfr=ls_nome_modello	then				 
	   f_aggiorna_doc_word_c( l_percorso_word, string(li_nr_versione), string(li_nr_edizione), ls_nome_documento)
	else
	   f_aggiorna_doc_word ( l_percorso_word, string(li_nr_versione), string(li_nr_edizione), ls_nome_documento, ls_cod_redatto_da + "-" + ls_redatto_da, string(date(ldt_redatto_il)), ls_cod_approvato_da + "-" + ls_approvato_da, string(date(ldt_approvato_il)), ls_cod_verificato_da + "-" + ls_verificato_da, string(date(ldt_verificato_il)))
	end if
end if

window_open(w_risposta_salva,0)

return "ok"
end function

