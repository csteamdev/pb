﻿$PBExportHeader$uo_richiesta_manutenz.sru
$PBExportComments$Oggetto CommandButton Per Richiesta Manutenzione da NC
forward
global type uo_richiesta_manutenz from commandbutton
end type
end forward

global type uo_richiesta_manutenz from commandbutton
integer width = 366
integer height = 80
integer taborder = 1
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Manutenz."
end type
global uo_richiesta_manutenz uo_richiesta_manutenz

event clicked;long ll_anno_nc, ll_num_nc, ll_risposta

if not isnull(pcca.window_currentdw) then

	if not (pcca.window_currentdw.getrow() > 0) then return
	
	s_cs_xx.parametri.parametro_d_1 = pcca.window_currentdw.getitemnumber( pcca.window_currentdw.getrow(), "anno_non_conf")
	s_cs_xx.parametri.parametro_d_2 = pcca.window_currentdw.getitemnumber( pcca.window_currentdw.getrow(), "num_non_conf")
	
	if isnull(s_cs_xx.parametri.parametro_d_1) then return
	if isnull(s_cs_xx.parametri.parametro_d_2) then return
	
	//if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")) then
	//   s_cs_xx.parametri.parametro_d_1 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")
	//else
	//   return
	//end if
	//
	//if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"")) then
	//   s_cs_xx.parametri.parametro_d_2 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"num_non_conf")
	//else
	//   return
	//end if
	
	SELECT non_conformita.anno_non_conf, 
			 non_conformita.num_non_conf  
	INTO   :ll_anno_nc, 
			 :ll_num_nc
	FROM   non_conformita  
	WHERE  cod_azienda   = :s_cs_xx.cod_azienda AND  
			 anno_non_conf = :s_cs_xx.parametri.parametro_d_1 AND  
			 num_non_conf  = :s_cs_xx.parametri.parametro_d_2 ;
	
	if sqlca.sqlcode = 0 then
		ll_risposta = g_mb.messagebox("Nuova Richiesta","Apro un nuova richiesta di manutenzione  dalla Non Conformità "+string(ll_anno_nc) + " / " + string(ll_num_nc) +"  ?",Question!, YesNo!, 2)
		if ll_risposta = 1 then
			s_cs_xx.parametri.parametro_s_1 = "MANUTENZIONE"
			window_open(w_richieste_manutenzione, -1)
		end if
	end if

else
	return
end if
end event

on uo_richiesta_manutenz.create
end on

on uo_richiesta_manutenz.destroy
end on

