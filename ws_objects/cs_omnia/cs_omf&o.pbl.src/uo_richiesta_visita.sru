﻿$PBExportHeader$uo_richiesta_visita.sru
$PBExportComments$Oggetto CommandButton Per Richiesta Visita Ispettiva da NC
forward
global type uo_richiesta_visita from commandbutton
end type
end forward

global type uo_richiesta_visita from commandbutton
int Width=366
int Height=81
int TabOrder=1
string Text="Visita Isp."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global uo_richiesta_visita uo_richiesta_visita

on clicked;long ll_anno_nc, ll_num_nc, ll_risposta

if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")) then
   s_cs_xx.parametri.parametro_d_1 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"anno_non_conf")
else
   return
end if

if not isnull(pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"num_non_conf")) then
   s_cs_xx.parametri.parametro_d_2 =  pcca.window_currentdw.getitemnumber(pcca.window_currentdw.i_selectedrows[1],"num_non_conf")
else
   return
end if

SELECT non_conformita.anno_non_conf, non_conformita.num_non_conf  
INTO   :ll_anno_nc, :ll_num_nc
FROM   non_conformita  
WHERE  (non_conformita.cod_azienda   = :s_cs_xx.cod_azienda ) AND  
       (non_conformita.anno_non_conf = :s_cs_xx.parametri.parametro_d_1 ) AND  
       (non_conformita.num_non_conf  = :s_cs_xx.parametri.parametro_d_2 )   ;

if sqlca.sqlcode = 0 then
   ll_risposta = g_mb.messagebox("Nuova Richiesta","Apro un nuova richiesta di Visita Ispettiva dalla Non Conformità "+string(ll_anno_nc) + " / " + string(ll_num_nc) +"  ?",Question!, YesNo!, 2)
   if ll_risposta = 1 then
      s_cs_xx.parametri.parametro_s_1 = "VISITA"
      window_open(w_richieste_visite, -1)
   end if
end if
end on

