﻿$PBExportHeader$uo_valutazione_lotto.sru
$PBExportComments$UO Contiene funzioni per valutazione lotto richiamabili da accettazione materiali.
forward
global type uo_valutazione_lotto from nonvisualobject
end type
end forward

global type uo_valutazione_lotto from nonvisualobject
end type
global uo_valutazione_lotto uo_valutazione_lotto

forward prototypes
public function integer fuo_valutazione_lotto (string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, string fs_messaggio)
end prototypes

public function integer fuo_valutazione_lotto (string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, string fs_messaggio);g_mb.messagebox("Valutazione Lotto","Procedura non disponibile in questa installazione")
return 0
end function

on uo_valutazione_lotto.create
TriggerEvent( this, "constructor" )
end on

on uo_valutazione_lotto.destroy
TriggerEvent( this, "destructor" )
end on

