﻿$PBExportHeader$w_caratt_qualita.srw
$PBExportComments$Window caratt_qualita
forward
global type w_caratt_qualita from w_cs_xx_principale
end type
type dw_caratt_qualita from uo_cs_xx_dw within w_caratt_qualita
end type
end forward

global type w_caratt_qualita from w_cs_xx_principale
int Width=1911
int Height=837
boolean TitleBar=true
string Title="Caratteristiche Qualità"
dw_caratt_qualita dw_caratt_qualita
end type
global w_caratt_qualita w_caratt_qualita

event pc_setwindow;call super::pc_setwindow;dw_caratt_qualita.set_dw_key("cod_azienda")
dw_caratt_qualita.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_caratt_qualita
end event

on w_caratt_qualita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_caratt_qualita=create dw_caratt_qualita
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_caratt_qualita
end on

on w_caratt_qualita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_caratt_qualita)
end on

type dw_caratt_qualita from uo_cs_xx_dw within w_caratt_qualita
int X=23
int Y=21
int Width=1829
int Height=701
int TabOrder=10
string DataObject="d_caratt_qualita"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

