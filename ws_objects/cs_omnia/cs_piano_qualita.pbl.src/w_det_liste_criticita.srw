﻿$PBExportHeader$w_det_liste_criticita.srw
$PBExportComments$Window det_liste_criticita
forward
global type w_det_liste_criticita from w_cs_xx_principale
end type
type dw_det_liste_criticita_lista from uo_cs_xx_dw within w_det_liste_criticita
end type
type dw_det_liste_criticita_det from uo_cs_xx_dw within w_det_liste_criticita
end type
end forward

global type w_det_liste_criticita from w_cs_xx_principale
int Width=2460
int Height=1409
boolean TitleBar=true
string Title="Liste Criticità (dettaglio)"
dw_det_liste_criticita_lista dw_det_liste_criticita_lista
dw_det_liste_criticita_det dw_det_liste_criticita_det
end type
global w_det_liste_criticita w_det_liste_criticita

on w_det_liste_criticita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_liste_criticita_lista=create dw_det_liste_criticita_lista
this.dw_det_liste_criticita_det=create dw_det_liste_criticita_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_liste_criticita_lista
this.Control[iCurrent+2]=dw_det_liste_criticita_det
end on

on w_det_liste_criticita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_liste_criticita_lista)
destroy(this.dw_det_liste_criticita_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_det_liste_criticita_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_default, &
                                    c_default)

dw_det_liste_criticita_det.set_dw_options(sqlca, &
                                    dw_det_liste_criticita_lista, &
                                    c_sharedata+c_scrollparent, &
                                    c_default)

iuo_dw_main = dw_det_liste_criticita_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_liste_criticita_det,"cod_caratteristica",sqlca,&
                 "caratt_qualita","cod_caratteristica","des_caratteristica",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_det_liste_criticita_det,"cod_fattore",sqlca,&
                 "tab_fattori_analisi","cod_fattore","des_fattore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_det_liste_criticita_det,"cod_livello",sqlca,&
                 "liv_criticita","cod_livello","des_livello",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_det_liste_criticita_det,"cod_valore",sqlca,&
                 "tab_valori_fatt_analisi","cod_valore","des_valore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
		
end event

type dw_det_liste_criticita_lista from uo_cs_xx_dw within w_det_liste_criticita
int X=23
int Y=21
int Width=2378
int Height=721
int TabOrder=20
string DataObject="d_det_liste_criticita_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG    l_Error,ll_progr
integer li_anno

li_anno = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_reg_lista")
ll_progr = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")

l_Error = Retrieve(s_cs_xx.cod_azienda,li_anno,ll_progr)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_new;call super::pcd_new;LONG    ll_num_reg_lista,ll_prog_riga_lista,ll_i
integer li_anno_reg_lista

li_anno_reg_lista = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_reg_lista")
ll_num_reg_lista = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")


select max(prog_riga_lista)
into   :ll_prog_riga_lista
from   det_liste_criticita
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_reg_lista =:li_anno_reg_lista
and    num_reg_lista=:ll_num_reg_lista;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

if isnull(ll_prog_riga_lista) then
	ll_prog_riga_lista = 1
else
	ll_prog_riga_lista++
end if

FOR ll_i = 1 TO RowCount()
	IF IsNull(GetItemstring(ll_i, "cod_azienda")) THEN
		SetItem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(ll_i, "anno_reg_lista", li_anno_reg_lista)
		SetItem(ll_i, "num_reg_lista", ll_num_reg_lista)
		SetItem(ll_i, "prog_riga_lista", ll_prog_riga_lista)
	end if
NEXT
end event

type dw_det_liste_criticita_det from uo_cs_xx_dw within w_det_liste_criticita
int X=23
int Y=761
int Width=2378
int Height=521
string DataObject="d_det_liste_criticita_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;string ls_null,ls_cod_caratteristica,ls_cod_fattore,ls_cod_livello,ls_des_valore,ls_cod_valore

choose case dwo.name
	case "cod_caratteristica" 
		f_PO_LoadDDDW_DW(dw_det_liste_criticita_det,"cod_fattore",sqlca,&
                 "tab_fattori_analisi","cod_fattore","des_fattore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_caratteristica='" + data + "'")
					  
		setnull(ls_null)
		setitem(getrow(),"cod_fattore",ls_null)
		setitem(getrow(),"cod_livello",ls_null)
		setitem(getrow(),"cod_valore",ls_null)
		
	case "cod_fattore" 
		f_PO_LoadDDDW_DW(dw_det_liste_criticita_det,"cod_livello",sqlca,&
                 "liv_criticita","cod_livello","des_livello",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
		setnull(ls_null)
		setitem(getrow(),"cod_livello",ls_null)
		setitem(getrow(),"cod_valore",ls_null)	
		
	case "cod_livello" 
		ls_cod_caratteristica = getitemstring(getrow(),"cod_caratteristica")
		ls_cod_fattore = getitemstring(getrow(),"cod_fattore")
		ls_cod_livello = data
		
		f_PO_LoadDDDW_DW(dw_det_liste_criticita_det,"cod_valore",sqlca,&
                 "tab_valori_fatt_analisi","cod_valore","des_valore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_caratteristica='" + ls_cod_caratteristica + "'" + &
					  " and cod_fattore ='" + ls_cod_fattore + "' and cod_livello ='" + ls_cod_livello + "'")
		
		setnull(ls_null)
		setitem(getrow(),"cod_valore",ls_null)	
	
	case "cod_valore" 
		ls_cod_caratteristica = getitemstring(getrow(),"cod_caratteristica")
		ls_cod_fattore = getitemstring(getrow(),"cod_fattore")
		ls_cod_livello = getitemstring(getrow(),"cod_livello")
		ls_cod_valore = data
		
		select des_valore
		into   :ls_des_valore
		from   tab_valori_fatt_analisi
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_caratteristica=:ls_cod_caratteristica
		and    cod_fattore=:ls_cod_fattore
		and    cod_livello=:ls_cod_livello
		and    cod_valore=:ls_cod_valore;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
			return
		end if
		
end choose

	



end event

