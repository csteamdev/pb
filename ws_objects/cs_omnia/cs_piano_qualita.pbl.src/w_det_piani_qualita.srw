﻿$PBExportHeader$w_det_piani_qualita.srw
$PBExportComments$Window det_piani_qualita
forward
global type w_det_piani_qualita from w_cs_xx_principale
end type
type dw_det_piani_qualita_lista from uo_cs_xx_dw within w_det_piani_qualita
end type
type dw_det_piani_qualita_det from uo_cs_xx_dw within w_det_piani_qualita
end type
end forward

global type w_det_piani_qualita from w_cs_xx_principale
int Width=2163
int Height=1521
boolean TitleBar=true
string Title="Piani di Qualità (dettaglio)"
dw_det_piani_qualita_lista dw_det_piani_qualita_lista
dw_det_piani_qualita_det dw_det_piani_qualita_det
end type
global w_det_piani_qualita w_det_piani_qualita

event pc_setwindow;call super::pc_setwindow;
dw_det_piani_qualita_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)
dw_det_piani_qualita_det.set_dw_options(sqlca,dw_det_piani_qualita_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_det_piani_qualita_lista
end event

on w_det_piani_qualita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_piani_qualita_lista=create dw_det_piani_qualita_lista
this.dw_det_piani_qualita_det=create dw_det_piani_qualita_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_piani_qualita_lista
this.Control[iCurrent+2]=dw_det_piani_qualita_det
end on

on w_det_piani_qualita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_piani_qualita_lista)
destroy(this.dw_det_piani_qualita_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_piani_qualita_det,"cod_caratteristica",sqlca,&
                 "caratt_qualita","cod_caratteristica","des_caratteristica",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					
f_PO_LoadDDDW_DW(dw_det_piani_qualita_det,"cod_fattore",sqlca,&
                 "tab_fattori_analisi","cod_fattore","des_fattore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_det_piani_qualita_det,"cod_livello",sqlca,&
                 "liv_criticita","cod_livello","des_livello",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_det_piani_qualita_det,"cod_valore",sqlca,&
                 "tab_valori_fatt_analisi","cod_valore","des_valore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
		
end event

type dw_det_piani_qualita_lista from uo_cs_xx_dw within w_det_piani_qualita
int X=23
int Y=21
int Width=2081
int Height=641
int TabOrder=10
string DataObject="d_det_piani_qualita_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_piano

ls_cod_piano = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_piano")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_piano)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_new;call super::pcd_new;LONG  l_Idx
string ls_cod_piano

ls_cod_piano = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_piano")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "cod_piano", ls_cod_piano)
   END IF
NEXT
end event

type dw_det_piani_qualita_det from uo_cs_xx_dw within w_det_piani_qualita
int X=23
int Y=681
int Width=2081
int Height=721
int TabOrder=20
string DataObject="d_det_piani_qualita_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;string ls_null,ls_cod_caratteristica,ls_cod_fattore,ls_cod_livello,ls_des_valore,ls_cod_valore

choose case dwo.name
	case "cod_caratteristica" 
		f_PO_LoadDDDW_DW(dw_det_piani_qualita_det,"cod_fattore",sqlca,&
                 "tab_fattori_analisi","cod_fattore","des_fattore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_caratteristica='" + data + "'")
					  
		setnull(ls_null)
		setitem(getrow(),"cod_fattore",ls_null)
		setitem(getrow(),"cod_livello",ls_null)
		setitem(getrow(),"cod_valore",ls_null)
		
	case "cod_fattore" 
		f_PO_LoadDDDW_DW(dw_det_piani_qualita_det,"cod_livello",sqlca,&
                 "liv_criticita","cod_livello","des_livello",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
		setnull(ls_null)
		setitem(getrow(),"cod_livello",ls_null)
		setitem(getrow(),"cod_valore",ls_null)	
		
	case "cod_livello" 
		ls_cod_caratteristica = getitemstring(getrow(),"cod_caratteristica")
		ls_cod_fattore = getitemstring(getrow(),"cod_fattore")
		ls_cod_livello = data
		
		f_PO_LoadDDDW_DW(dw_det_piani_qualita_det,"cod_valore",sqlca,&
                 "tab_valori_fatt_analisi","cod_valore","des_valore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_caratteristica='" + ls_cod_caratteristica + "'" + &
					  " and cod_fattore ='" + ls_cod_fattore + "' and cod_livello ='" + ls_cod_livello + "'")
		
		setnull(ls_null)
		setitem(getrow(),"cod_valore",ls_null)	
	
	case "cod_valore" 
		ls_cod_caratteristica = getitemstring(getrow(),"cod_caratteristica")
		ls_cod_fattore = getitemstring(getrow(),"cod_fattore")
		ls_cod_livello = getitemstring(getrow(),"cod_livello")
		ls_cod_valore = data
		
		select des_valore
		into   :ls_des_valore
		from   tab_valori_fatt_analisi
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_caratteristica=:ls_cod_caratteristica
		and    cod_fattore=:ls_cod_fattore
		and    cod_livello=:ls_cod_livello
		and    cod_valore=:ls_cod_valore;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
			return
		end if
		
		setitem(getrow(),"des_valore",ls_des_valore)	
								
		
	
end choose

	



end event

