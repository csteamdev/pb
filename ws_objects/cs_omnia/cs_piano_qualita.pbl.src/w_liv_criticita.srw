﻿$PBExportHeader$w_liv_criticita.srw
$PBExportComments$Window liv_criticita
forward
global type w_liv_criticita from w_cs_xx_principale
end type
type dw_liv_criticita from uo_cs_xx_dw within w_liv_criticita
end type
end forward

global type w_liv_criticita from w_cs_xx_principale
int Width=2277
int Height=1141
boolean TitleBar=true
string Title="Livelli Criticità"
dw_liv_criticita dw_liv_criticita
end type
global w_liv_criticita w_liv_criticita

event pc_setwindow;call super::pc_setwindow;dw_liv_criticita.set_dw_key("cod_azienda")
dw_liv_criticita.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_liv_criticita
end event

on w_liv_criticita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_liv_criticita=create dw_liv_criticita
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_liv_criticita
end on

on w_liv_criticita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_liv_criticita)
end on

type dw_liv_criticita from uo_cs_xx_dw within w_liv_criticita
int X=23
int Y=21
int Width=2195
int Height=1001
int TabOrder=10
string DataObject="d_liv_criticita"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

