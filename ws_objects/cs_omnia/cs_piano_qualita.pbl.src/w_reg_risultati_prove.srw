﻿$PBExportHeader$w_reg_risultati_prove.srw
$PBExportComments$Window reg_risultati_prove
forward
global type w_reg_risultati_prove from w_cs_xx_principale
end type
type dw_reg_risultati_prove_lista from uo_cs_xx_dw within w_reg_risultati_prove
end type
type dw_reg_risultati_prove_det from uo_cs_xx_dw within w_reg_risultati_prove
end type
type cb_nuova_prova from commandbutton within w_reg_risultati_prove
end type
end forward

global type w_reg_risultati_prove from w_cs_xx_principale
int Width=2894
int Height=1865
boolean TitleBar=true
string Title="Prove/Controlli"
dw_reg_risultati_prove_lista dw_reg_risultati_prove_lista
dw_reg_risultati_prove_det dw_reg_risultati_prove_det
cb_nuova_prova cb_nuova_prova
end type
global w_reg_risultati_prove w_reg_risultati_prove

event pc_setwindow;call super::pc_setwindow;
dw_reg_risultati_prove_lista.set_dw_options(sqlca,i_openparm,c_default + c_nonew,c_default)
dw_reg_risultati_prove_det.set_dw_options(sqlca,dw_reg_risultati_prove_lista,c_sharedata+c_scrollparent + c_nonew,c_default)

iuo_dw_main = dw_reg_risultati_prove_lista



end event

on w_reg_risultati_prove.create
int iCurrent
call w_cs_xx_principale::create
this.dw_reg_risultati_prove_lista=create dw_reg_risultati_prove_lista
this.dw_reg_risultati_prove_det=create dw_reg_risultati_prove_det
this.cb_nuova_prova=create cb_nuova_prova
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_reg_risultati_prove_lista
this.Control[iCurrent+2]=dw_reg_risultati_prove_det
this.Control[iCurrent+3]=cb_nuova_prova
end on

on w_reg_risultati_prove.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_reg_risultati_prove_lista)
destroy(this.dw_reg_risultati_prove_det)
destroy(this.cb_nuova_prova)
end on

type dw_reg_risultati_prove_lista from uo_cs_xx_dw within w_reg_risultati_prove
int X=23
int Y=21
int Width=2812
int Height=841
int TabOrder=10
string DataObject="d_reg_risultati_prove_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_num_reg_lista
integer li_anno_reg_lista

ll_num_reg_lista = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
li_anno_reg_lista = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_reg_lista")

l_Error = Retrieve(s_cs_xx.cod_azienda,li_anno_reg_lista,ll_num_reg_lista)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_new;call super::pcd_new;//if i_extendmode then
//	
//	s_cs_xx.parametri.parametro_ul_1 = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
//	s_cs_xx.parametri.parametro_i_1 = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_reg_lista")
//
//	window_open(w_scelta_prove,0)
//	if s_cs_xx.parametri.parametro_i_1 = -1 then
//		triggerevent("pcd_view")
//	else
//		triggerevent("pcd_save")
//	end if
//end if
end event

type dw_reg_risultati_prove_det from uo_cs_xx_dw within w_reg_risultati_prove
int X=23
int Y=881
int Width=2812
int Height=761
int TabOrder=20
string DataObject="d_reg_risultati_prove_det"
BorderStyle BorderStyle=StyleRaised!
end type

type cb_nuova_prova from commandbutton within w_reg_risultati_prove
int X=2469
int Y=1661
int Width=366
int Height=81
int TabOrder=21
boolean BringToTop=true
string Text="&Nuova Prova"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_ul_1 = dw_reg_risultati_prove_lista.i_parentdw.getitemnumber(dw_reg_risultati_prove_lista.i_parentdw.i_selectedrows[1], "num_reg_lista")
s_cs_xx.parametri.parametro_i_1 = dw_reg_risultati_prove_lista.i_parentdw.getitemnumber(dw_reg_risultati_prove_lista.i_parentdw.i_selectedrows[1], "anno_reg_lista")

window_open(w_scelta_prove,0)
if s_cs_xx.parametri.parametro_i_1 = -1 then
	
else
	dw_reg_risultati_prove_lista.change_dw_current()
	parent.triggerevent("pc_retrieve")
end if
end event

