﻿$PBExportHeader$w_report_vettore_qualita.srw
$PBExportComments$Window w_report_vettore_qualita
forward
global type w_report_vettore_qualita from w_cs_xx_principale
end type
type dw_vettore_qualita from uo_cs_xx_dw within w_report_vettore_qualita
end type
end forward

global type w_report_vettore_qualita from w_cs_xx_principale
integer width = 3369
integer height = 1944
string title = "Report Vettore di Qualità Necessaria"
dw_vettore_qualita dw_vettore_qualita
end type
global w_report_vettore_qualita w_report_vettore_qualita

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();string  ls_cod_caratteristica[],ls_cod_livello[],ls_errore,ls_cod_cliente,ls_cod_piano,ls_des_piano,ls_rag_soc, & 
		  ls_des_caratteristica,ls_des_livello
long    ll_num_registrazione
integer li_anno_registrazione,li_risposta,ll_righe

ll_num_registrazione = s_cs_xx.parametri.parametro_ul_1
li_anno_registrazione = s_cs_xx.parametri.parametro_i_1

li_risposta = f_vettore_qualita(li_anno_registrazione,ll_num_registrazione,ls_cod_caratteristica[],ls_cod_livello[],ls_errore)

if li_risposta < 0 then
	g_mb.messagebox("Omnia",ls_errore,stopsign!)
	return -1
end if

select cod_cliente,
		 cod_piano
into   :ls_cod_cliente,
		 :ls_cod_piano
from   tes_liste_criticita
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_reg_lista = :li_anno_registrazione
and    num_reg_lista =: ll_num_registrazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return -1
end if

select des_piano
into   :ls_des_piano
from   tes_piani_qualita
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_piano=:ls_cod_piano;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return -1
end if


select rag_soc_1
into   :ls_rag_soc
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return -1
end if

for ll_righe = 1 to upperbound(ls_cod_caratteristica[])
	
	select des_caratteristica 
	into   :ls_des_caratteristica
	from   caratt_qualita
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_caratteristica = :ls_cod_caratteristica[ll_righe];

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return -1
	end if

	select des_livello
	into   :ls_des_livello
	from   liv_criticita
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_livello = :ls_cod_livello[ll_righe];

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return -1
	end if

	dw_vettore_qualita.insertrow(ll_righe)
	dw_vettore_qualita.setitem(ll_righe,"cod_caratteristica",ls_cod_caratteristica[ll_righe])
	dw_vettore_qualita.setitem(ll_righe,"des_caratteristica",ls_des_caratteristica)
	dw_vettore_qualita.setitem(ll_righe,"cod_livello",ls_cod_livello[ll_righe])
	dw_vettore_qualita.setitem(ll_righe,"des_livello",ls_des_livello)

next 

dw_vettore_qualita.Modify("anno.text='" + string(li_anno_registrazione) + "'")
dw_vettore_qualita.Modify("numero.text='" + string(ll_num_registrazione) + "'")
dw_vettore_qualita.Modify("piano_qualita.text='" + ls_cod_piano + "  " + ls_des_piano + "'")
dw_vettore_qualita.Modify("cliente.text='" + ls_cod_cliente + "  " + ls_rag_soc + "'")


return 0
end function

on w_report_vettore_qualita.create
int iCurrent
call super::create
this.dw_vettore_qualita=create dw_vettore_qualita
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_vettore_qualita
end on

on w_report_vettore_qualita.destroy
call super::destroy
destroy(this.dw_vettore_qualita)
end on

event pc_setwindow;call super::pc_setwindow;dw_vettore_qualita.ib_dw_report = true

set_w_options(c_closenosave)
dw_vettore_qualita.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_nomodify + c_nonew +c_nodelete, &
                                    c_noresizedw + &
			                           c_nohighlightselected + &
           				               c_nocursorrowpointer +&
                      				   c_nocursorrowfocusrect)



save_on_close(c_socnosave)

end event

type dw_vettore_qualita from uo_cs_xx_dw within w_report_vettore_qualita
integer x = 23
integer y = 20
integer width = 3291
integer height = 1800
integer taborder = 20
string dataobject = "d_vettore_qualita"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;integer li_risposta

li_risposta = wf_report()

resetupdate()
end event

