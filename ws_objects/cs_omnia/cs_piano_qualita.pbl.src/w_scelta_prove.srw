﻿$PBExportHeader$w_scelta_prove.srw
$PBExportComments$Window scelta prove
forward
global type w_scelta_prove from w_cs_xx_risposta
end type
type dw_lista_caratteristiche from uo_cs_xx_dw within w_scelta_prove
end type
type cb_ok from commandbutton within w_scelta_prove
end type
type cb_annulla from commandbutton within w_scelta_prove
end type
type dw_lista_prove from datawindow within w_scelta_prove
end type
end forward

global type w_scelta_prove from w_cs_xx_risposta
int Width=3351
int Height=2105
boolean TitleBar=true
string Title="Scelta Prove"
boolean ControlMenu=false
dw_lista_caratteristiche dw_lista_caratteristiche
cb_ok cb_ok
cb_annulla cb_annulla
dw_lista_prove dw_lista_prove
end type
global w_scelta_prove w_scelta_prove

forward prototypes
public function integer wf_caratteristiche ()
public function integer wf_imposta_prove ()
end prototypes

public function integer wf_caratteristiche ();string  ls_cod_caratteristica[],ls_cod_livello[],ls_errore,ls_cod_cliente,ls_cod_piano,ls_des_piano,ls_rag_soc, & 
		  ls_des_caratteristica,ls_des_livello
long    ll_num_registrazione
integer li_anno_registrazione,li_risposta,ll_righe

ll_num_registrazione = s_cs_xx.parametri.parametro_ul_1
li_anno_registrazione = s_cs_xx.parametri.parametro_i_1

li_risposta = f_vettore_qualita(li_anno_registrazione,ll_num_registrazione,ls_cod_caratteristica[],ls_cod_livello[],ls_errore)

if li_risposta < 0 then
	g_mb.messagebox("Omnia",ls_errore,stopsign!)
	return -1
end if

select cod_cliente,
		 cod_piano
into   :ls_cod_cliente,
		 :ls_cod_piano
from   tes_liste_criticita
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_reg_lista = :li_anno_registrazione
and    num_reg_lista =: ll_num_registrazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return -1
end if

select des_piano
into   :ls_des_piano
from   tes_piani_qualita
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_piano=:ls_cod_piano;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return -1
end if


select rag_soc_1
into   :ls_rag_soc
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return -1
end if

for ll_righe = 1 to upperbound(ls_cod_caratteristica[])
	
	select des_caratteristica 
	into   :ls_des_caratteristica
	from   caratt_qualita
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_caratteristica = :ls_cod_caratteristica[ll_righe];

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return -1
	end if

	select des_livello
	into   :ls_des_livello
	from   liv_criticita
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_livello = :ls_cod_livello[ll_righe];

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return -1
	end if

	dw_lista_caratteristiche.insertrow(ll_righe)
	dw_lista_caratteristiche.setitem(ll_righe,"cod_caratteristica",ls_cod_caratteristica[ll_righe])
	dw_lista_caratteristiche.setitem(ll_righe,"cod_livello",ls_cod_livello[ll_righe])
	

next 
return 0
end function

public function integer wf_imposta_prove ();string ls_cod_caratteristica,ls_cod_livello,ls_cod_fattore,ls_cod_valore,ls_cod_prova,ls_des_prova
long ll_righe

ls_cod_caratteristica=dw_lista_caratteristiche.getitemstring(dw_lista_caratteristiche.getrow(),"cod_caratteristica")
ls_cod_livello=dw_lista_caratteristiche.getitemstring(dw_lista_caratteristiche.getrow(),"cod_livello")

dw_lista_prove.reset()

declare righe_prove cursor for
select  cod_fattore,
		  cod_valore,
		  cod_prova,
		  des_prova
from    tab_prove
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_caratteristica=:ls_cod_caratteristica
and     cod_livello=:ls_cod_livello;

open righe_prove;

do while 1 = 1
	fetch righe_prove
	into  :ls_cod_fattore,
			:ls_cod_valore,
			:ls_cod_prova,
			:ls_des_prova;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close righe_prove;
		return -1
	end if
	
	ll_righe++
	dw_lista_prove.insertrow(ll_righe)
	dw_lista_prove.setitem(ll_righe,"cod_fattore",ls_cod_fattore)
	dw_lista_prove.setitem(ll_righe,"cod_valore",ls_cod_valore)
	dw_lista_prove.setitem(ll_righe,"cod_prova",ls_cod_prova)
	dw_lista_prove.setitem(ll_righe,"des_prova",ls_des_prova)
			
loop


close righe_prove;

return 0
end function

on w_scelta_prove.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_lista_caratteristiche=create dw_lista_caratteristiche
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.dw_lista_prove=create dw_lista_prove
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_caratteristiche
this.Control[iCurrent+2]=cb_ok
this.Control[iCurrent+3]=cb_annulla
this.Control[iCurrent+4]=dw_lista_prove
end on

on w_scelta_prove.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_lista_caratteristiche)
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.dw_lista_prove)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_lista_caratteristiche.set_dw_options(sqlca, pcca.null_object, c_nonew + c_nomodify + c_nodelete + c_disableCC, &
										 c_noresizedw + c_cursorrowpointer)

end event

type dw_lista_caratteristiche from uo_cs_xx_dw within w_scelta_prove
int X=23
int Y=21
int Width=3269
int Height=921
int TabOrder=30
boolean BringToTop=true
string DataObject="d_lista_caratteristiche"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;integer li_risposta

li_risposta = wf_caratteristiche()

resetupdate()
end event

event rowfocuschanged;call super::rowfocuschanged;if currentrow > 0 then
	wf_imposta_prove()
end if
end event

type cb_ok from commandbutton within w_scelta_prove
int X=2538
int Y=1901
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string  ls_cod_caratteristica,ls_cod_livello,ls_cod_fattore,ls_cod_valore,ls_cod_prova,ls_des_prova,ls_flag_esito
long    ll_i,ll_num_reg_lista
integer li_anno_reg_lista
datetime ldt_data_ora_reg

ldt_data_ora_reg = datetime(today(),now())

ll_num_reg_lista = s_cs_xx.parametri.parametro_ul_1
li_anno_reg_lista = s_cs_xx.parametri.parametro_i_1
ls_cod_caratteristica = dw_lista_caratteristiche.getitemstring(dw_lista_caratteristiche.getrow(),"cod_caratteristica")
ls_cod_livello = dw_lista_caratteristiche.getitemstring(dw_lista_caratteristiche.getrow(),"cod_livello")

for ll_i = 1 to dw_lista_prove.rowcount()
	if dw_lista_prove.getitemstring(ll_i,"flag_esegui") = 'S' then
		ls_cod_fattore = dw_lista_prove.getitemstring(ll_i,"cod_fattore")
		ls_cod_valore = dw_lista_prove.getitemstring(ll_i,"cod_valore")
		ls_cod_prova = dw_lista_prove.getitemstring(ll_i,"cod_prova")
		ls_flag_esito = dw_lista_prove.getitemstring(ll_i,"flag_esito")
		
		insert into reg_risultati_prove
		(cod_azienda,
		 anno_reg_lista,
		 num_reg_lista,
		 cod_caratteristica,
		 cod_fattore,
		 cod_livello,
		 cod_valore,
		 cod_prova,
		 data_ora_reg,
		 flag_esito)
		 values
		 (:s_cs_xx.cod_azienda,
		  :li_anno_reg_lista,
		  :ll_num_reg_lista,
		  :ls_cod_caratteristica,
		  :ls_cod_fattore,
		  :ls_cod_livello,
		  :ls_cod_valore,
		  :ls_cod_prova,
		  :ldt_data_ora_reg,
		  :ls_flag_esito);
		  
		if sqlca.sqlcode < 0 then
		 	g_mb.messagebox("Omnia","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			exit
		end if
		
	end if	
	
next

close(parent)
end event

type cb_annulla from commandbutton within w_scelta_prove
int X=2926
int Y=1901
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_i_1= -1

close(parent)
end event

type dw_lista_prove from datawindow within w_scelta_prove
int X=23
int Y=961
int Width=3269
int Height=921
int TabOrder=31
boolean BringToTop=true
string DataObject="d_lista_prove"
BorderStyle BorderStyle=StyleLowered!
boolean LiveScroll=true
end type

