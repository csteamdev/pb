﻿$PBExportHeader$w_seleziona_piano.srw
$PBExportComments$Window seleziona_piano
forward
global type w_seleziona_piano from w_cs_xx_risposta
end type
type st_1 from statictext within w_seleziona_piano
end type
type dw_tes_piani_qualita from uo_cs_xx_dw within w_seleziona_piano
end type
type cb_ok from commandbutton within w_seleziona_piano
end type
end forward

global type w_seleziona_piano from w_cs_xx_risposta
int Width=1751
int Height=1485
boolean TitleBar=true
string Title="Selezione piano di qualità"
st_1 st_1
dw_tes_piani_qualita dw_tes_piani_qualita
cb_ok cb_ok
end type
global w_seleziona_piano w_seleziona_piano

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave)
set_w_options(c_noenablepopup)

dw_tes_piani_qualita.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify +c_nodelete,c_default)

end event

on w_seleziona_piano.create
int iCurrent
call w_cs_xx_risposta::create
this.st_1=create st_1
this.dw_tes_piani_qualita=create dw_tes_piani_qualita
this.cb_ok=create cb_ok
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=dw_tes_piani_qualita
this.Control[iCurrent+3]=cb_ok
end on

on w_seleziona_piano.destroy
call w_cs_xx_risposta::destroy
destroy(this.st_1)
destroy(this.dw_tes_piani_qualita)
destroy(this.cb_ok)
end on

type st_1 from statictext within w_seleziona_piano
int X=23
int Y=21
int Width=1669
int Height=101
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Selezionare un piano di Qualità"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=255
long BackColor=79741120
int TextSize=-12
int Weight=700
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_tes_piani_qualita from uo_cs_xx_dw within w_seleziona_piano
int X=23
int Y=141
int Width=1669
int Height=1121
string DataObject="d_tes_piani_qualita"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event doubleclicked;call super::doubleclicked;s_cs_xx.parametri.parametro_s_15 = dw_tes_piani_qualita.getitemstring(dw_tes_piani_qualita.getrow(),"cod_piano")
close(parent)
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type cb_ok from commandbutton within w_seleziona_piano
int X=1326
int Y=1281
int Width=366
int Height=81
int TabOrder=2
boolean BringToTop=true
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;
s_cs_xx.parametri.parametro_s_15 = dw_tes_piani_qualita.getitemstring(dw_tes_piani_qualita.getrow(),"cod_piano")
close(parent)
end event

