﻿$PBExportHeader$w_tab_fattori_analisi.srw
$PBExportComments$Window tab_fattori_analisi
forward
global type w_tab_fattori_analisi from w_cs_xx_principale
end type
type dw_tab_fattori_analisi_lista from uo_cs_xx_dw within w_tab_fattori_analisi
end type
type dw_tab_fattori_analisi_det from uo_cs_xx_dw within w_tab_fattori_analisi
end type
end forward

global type w_tab_fattori_analisi from w_cs_xx_principale
int Width=2391
int Height=1345
boolean TitleBar=true
string Title="Fattori di Analisi"
dw_tab_fattori_analisi_lista dw_tab_fattori_analisi_lista
dw_tab_fattori_analisi_det dw_tab_fattori_analisi_det
end type
global w_tab_fattori_analisi w_tab_fattori_analisi

event pc_setwindow;call super::pc_setwindow;dw_tab_fattori_analisi_lista.set_dw_key("cod_azienda")
dw_tab_fattori_analisi_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_fattori_analisi_det.set_dw_options(sqlca,dw_tab_fattori_analisi_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_fattori_analisi_lista
end event

on w_tab_fattori_analisi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_fattori_analisi_lista=create dw_tab_fattori_analisi_lista
this.dw_tab_fattori_analisi_det=create dw_tab_fattori_analisi_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_fattori_analisi_lista
this.Control[iCurrent+2]=dw_tab_fattori_analisi_det
end on

on w_tab_fattori_analisi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_fattori_analisi_lista)
destroy(this.dw_tab_fattori_analisi_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_fattori_analisi_det,"cod_caratteristica",sqlca,&
                 "caratt_qualita","cod_caratteristica","des_caratteristica",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type dw_tab_fattori_analisi_lista from uo_cs_xx_dw within w_tab_fattori_analisi
int X=23
int Y=21
int Width=2309
int Height=501
int TabOrder=10
string DataObject="d_tab_fattori_analisi_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_tab_fattori_analisi_det from uo_cs_xx_dw within w_tab_fattori_analisi
int X=23
int Y=541
int Width=2309
int Height=681
int TabOrder=20
string DataObject="d_tab_fattori_analisi_det"
BorderStyle BorderStyle=StyleRaised!
end type

