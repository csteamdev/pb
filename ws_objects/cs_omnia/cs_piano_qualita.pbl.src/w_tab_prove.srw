﻿$PBExportHeader$w_tab_prove.srw
$PBExportComments$w_tab_prove
forward
global type w_tab_prove from w_cs_xx_principale
end type
type dw_tab_prove_lista from uo_cs_xx_dw within w_tab_prove
end type
type dw_tab_prove_det from uo_cs_xx_dw within w_tab_prove
end type
end forward

global type w_tab_prove from w_cs_xx_principale
int Width=2460
int Height=1625
boolean TitleBar=true
string Title="Prove/Controlli"
dw_tab_prove_lista dw_tab_prove_lista
dw_tab_prove_det dw_tab_prove_det
end type
global w_tab_prove w_tab_prove

event pc_setwindow;call super::pc_setwindow;dw_tab_prove_lista.set_dw_key("cod_azienda")
dw_tab_prove_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)
dw_tab_prove_det.set_dw_options(sqlca,dw_tab_prove_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_prove_lista



end event

on w_tab_prove.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_prove_lista=create dw_tab_prove_lista
this.dw_tab_prove_det=create dw_tab_prove_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_prove_lista
this.Control[iCurrent+2]=dw_tab_prove_det
end on

on w_tab_prove.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_prove_lista)
destroy(this.dw_tab_prove_det)
end on

type dw_tab_prove_lista from uo_cs_xx_dw within w_tab_prove
int X=23
int Y=21
int Width=2378
int Height=501
int TabOrder=10
string DataObject="d_tab_prove_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_caratteristica,ls_cod_fattore,ls_cod_livello,ls_cod_valore
LONG  l_Error

ls_cod_caratteristica = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_caratteristica")
ls_cod_fattore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fattore")
ls_cod_livello = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello")
ls_cod_valore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valore")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_caratteristica,ls_cod_fattore,ls_cod_livello,ls_cod_valore)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_new;call super::pcd_new;string ls_cod_caratteristica,ls_cod_fattore,ls_cod_livello,ls_cod_valore
LONG  l_Idx

ls_cod_caratteristica = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_caratteristica")
ls_cod_fattore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fattore")
ls_cod_livello = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello")
ls_cod_valore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valore")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "cod_caratteristica", ls_cod_caratteristica)
		SetItem(l_Idx, "cod_fattore", ls_cod_fattore)
		SetItem(l_Idx, "cod_livello", ls_cod_livello)
		SetItem(l_Idx, "cod_valore", ls_cod_valore)
   END IF
NEXT

end event

type dw_tab_prove_det from uo_cs_xx_dw within w_tab_prove
int X=23
int Y=541
int Width=2378
int Height=961
int TabOrder=20
string DataObject="d_tab_prove_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;string ls_null,ls_cod_caratteristica,ls_cod_fattore,ls_cod_livello
choose case dwo.name
	case "cod_caratteristica" 
		f_PO_LoadDDDW_DW(dw_tab_prove_det,"cod_fattore",sqlca,&
                 "tab_fattori_analisi","cod_fattore","des_fattore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_caratteristica='" + data + "'")
					  
		setnull(ls_null)
		setitem(getrow(),"cod_fattore",ls_null)
		setitem(getrow(),"cod_livello",ls_null)
		setitem(getrow(),"cod_valore",ls_null)
		
	case "cod_fattore" 
		f_PO_LoadDDDW_DW(dw_tab_prove_det,"cod_livello",sqlca,&
                 "liv_criticita","cod_livello","des_livello",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
		setnull(ls_null)
		setitem(getrow(),"cod_livello",ls_null)
		setitem(getrow(),"cod_valore",ls_null)	
		
	case "cod_livello" 
		ls_cod_caratteristica = getitemstring(getrow(),"cod_caratteristica")
		ls_cod_fattore = getitemstring(getrow(),"cod_fattore")
		ls_cod_livello = data
		
		f_PO_LoadDDDW_DW(dw_tab_prove_det,"cod_valore",sqlca,&
                 "tab_valori_fatt_analisi","cod_valore","des_valore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_caratteristica='" + ls_cod_caratteristica + "'" + &
					  " and cod_fattore ='" + ls_cod_fattore + "' and cod_livello ='" + ls_cod_livello + "'")
		
		setnull(ls_null)
		setitem(getrow(),"cod_valore",ls_null)	
		
end choose

	
end event

