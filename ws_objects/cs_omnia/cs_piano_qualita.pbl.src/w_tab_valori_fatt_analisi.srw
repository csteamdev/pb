﻿$PBExportHeader$w_tab_valori_fatt_analisi.srw
$PBExportComments$Window tab_valori_fatt_analisi
forward
global type w_tab_valori_fatt_analisi from w_cs_xx_principale
end type
type dw_tab_valori_fatt_analisi_lista from uo_cs_xx_dw within w_tab_valori_fatt_analisi
end type
type dw_tab_valori_fatt_analisi_det from uo_cs_xx_dw within w_tab_valori_fatt_analisi
end type
type cb_prove from commandbutton within w_tab_valori_fatt_analisi
end type
end forward

global type w_tab_valori_fatt_analisi from w_cs_xx_principale
int Width=2323
int Height=1645
boolean TitleBar=true
string Title="Valori Fattori di Analisi"
dw_tab_valori_fatt_analisi_lista dw_tab_valori_fatt_analisi_lista
dw_tab_valori_fatt_analisi_det dw_tab_valori_fatt_analisi_det
cb_prove cb_prove
end type
global w_tab_valori_fatt_analisi w_tab_valori_fatt_analisi

event pc_setwindow;call super::pc_setwindow;dw_tab_valori_fatt_analisi_lista.set_dw_key("cod_azienda")
dw_tab_valori_fatt_analisi_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_valori_fatt_analisi_det.set_dw_options(sqlca,dw_tab_valori_fatt_analisi_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_valori_fatt_analisi_lista
end event

on w_tab_valori_fatt_analisi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_valori_fatt_analisi_lista=create dw_tab_valori_fatt_analisi_lista
this.dw_tab_valori_fatt_analisi_det=create dw_tab_valori_fatt_analisi_det
this.cb_prove=create cb_prove
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_valori_fatt_analisi_lista
this.Control[iCurrent+2]=dw_tab_valori_fatt_analisi_det
this.Control[iCurrent+3]=cb_prove
end on

on w_tab_valori_fatt_analisi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_valori_fatt_analisi_lista)
destroy(this.dw_tab_valori_fatt_analisi_det)
destroy(this.cb_prove)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_valori_fatt_analisi_det,"cod_caratteristica",sqlca,&
                 "caratt_qualita","cod_caratteristica","des_caratteristica",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_valori_fatt_analisi_det,"cod_livello",sqlca,&
                 "liv_criticita","cod_livello","des_livello",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type dw_tab_valori_fatt_analisi_lista from uo_cs_xx_dw within w_tab_valori_fatt_analisi
int X=23
int Y=21
int Width=2241
int Height=501
int TabOrder=10
string DataObject="d_tab_valori_fatt_analisi_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_new;call super::pcd_new;if i_extendmode then
	cb_prove.enabled=false
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_prove.enabled=false
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	cb_prove.enabled=true
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	cb_prove.enabled=true
end if
end event

type dw_tab_valori_fatt_analisi_det from uo_cs_xx_dw within w_tab_valori_fatt_analisi
int X=23
int Y=541
int Width=2241
int Height=881
int TabOrder=20
string DataObject="d_tab_valori_fatt_analisi_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;string ls_null
choose case dwo.name
	case "cod_caratteristica" 
		f_PO_LoadDDDW_DW(dw_tab_valori_fatt_analisi_det,"cod_fattore",sqlca,&
                 "tab_fattori_analisi","cod_fattore","des_fattore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_caratteristica='" + data + "'")
		setnull(ls_null)
		setitem(getrow(),"cod_fattore",ls_null)
end choose

	
end event

type cb_prove from commandbutton within w_tab_valori_fatt_analisi
int X=1898
int Y=1441
int Width=366
int Height=81
int TabOrder=21
boolean BringToTop=true
string Text="&Prove/Ctrl."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if dw_tab_valori_fatt_analisi_lista.rowcount() > 0 then
	if isnull(dw_tab_valori_fatt_analisi_lista.getitemstring(dw_tab_valori_fatt_analisi_lista.getrow(),"cod_caratteristica")) then return
	window_open_parm(w_tab_prove,-1,dw_tab_valori_fatt_analisi_lista)
end if
end event

