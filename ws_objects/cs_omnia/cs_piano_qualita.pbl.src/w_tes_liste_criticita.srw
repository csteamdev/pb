﻿$PBExportHeader$w_tes_liste_criticita.srw
$PBExportComments$Window tes_liste_criticita
forward
global type w_tes_liste_criticita from w_cs_xx_principale
end type
type cb_dettaglio from commandbutton within w_tes_liste_criticita
end type
type cb_ricerca_operaio from cb_operai_ricerca within w_tes_liste_criticita
end type
type dw_tes_liste_criticita_lista from uo_cs_xx_dw within w_tes_liste_criticita
end type
type dw_tes_liste_criticita_det from uo_cs_xx_dw within w_tes_liste_criticita
end type
type cb_vettore from commandbutton within w_tes_liste_criticita
end type
type cb_prove from commandbutton within w_tes_liste_criticita
end type
end forward

global type w_tes_liste_criticita from w_cs_xx_principale
integer width = 2601
integer height = 1948
string title = "Liste Criticità"
cb_dettaglio cb_dettaglio
cb_ricerca_operaio cb_ricerca_operaio
dw_tes_liste_criticita_lista dw_tes_liste_criticita_lista
dw_tes_liste_criticita_det dw_tes_liste_criticita_det
cb_vettore cb_vettore
cb_prove cb_prove
end type
global w_tes_liste_criticita w_tes_liste_criticita

forward prototypes
public function integer wf_genera_det ()
end prototypes

public function integer wf_genera_det ();integer li_anno_reg_lista
long    ll_num_reg_lista,ll_prog_riga_lista
string  ls_cod_caratteristica,ls_cod_valore,ls_cod_fattore,ls_cod_livello,ls_cod_piano

ls_cod_piano = s_cs_xx.parametri.parametro_s_15
li_anno_reg_lista = dw_tes_liste_criticita_lista.getitemnumber(dw_tes_liste_criticita_lista.getrow(),"anno_reg_lista")
ll_num_reg_lista = dw_tes_liste_criticita_lista.getitemnumber(dw_tes_liste_criticita_lista.getrow(),"num_reg_lista")

declare righe_det_piani cursor for
select  cod_caratteristica,
		  cod_fattore,
		  cod_livello,
		  cod_valore
from    det_piani_qualita
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_piano=:ls_cod_piano;

open righe_det_piani;

do while 1=1
	fetch righe_det_piani
	into  :ls_cod_caratteristica,
			:ls_cod_fattore,
			:ls_cod_livello,
			:ls_cod_valore;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		close righe_det_piani;
		return -1
	end if

	if sqlca.sqlcode = 100 then exit
	
	ll_prog_riga_lista++
	
	insert into det_liste_criticita
	(cod_azienda,
	 anno_reg_lista,
	 num_reg_lista,
	 prog_riga_lista,
	 cod_caratteristica,
	 cod_fattore,
	 cod_livello,
	 cod_valore)
	 values
	 (:s_cs_xx.cod_azienda,
	  :li_anno_reg_lista,
	  :ll_num_reg_lista,
	  :ll_prog_riga_lista,
	  :ls_cod_caratteristica,
	  :ls_cod_fattore,
	  :ls_cod_livello,
	  :ls_cod_valore);


	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		close righe_det_piani;
		return -1
	end if


loop

return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_flag_autorizzazione

dw_tes_liste_criticita_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tes_liste_criticita_det.set_dw_options(sqlca,dw_tes_liste_criticita_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main=dw_tes_liste_criticita_lista


end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_liste_criticita_det,"cod_centro_costo",sqlca,&
                 "tab_centri_costo","cod_centro_costo","des_centro_costo",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  
//f_PO_LoadDDDW_DW(dw_tes_liste_criticita_det,"cod_piano",sqlca,&
//                 "tes_piani_qualita","cod_piano","des_piano",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//

end event

on w_tes_liste_criticita.create
int iCurrent
call super::create
this.cb_dettaglio=create cb_dettaglio
this.cb_ricerca_operaio=create cb_ricerca_operaio
this.dw_tes_liste_criticita_lista=create dw_tes_liste_criticita_lista
this.dw_tes_liste_criticita_det=create dw_tes_liste_criticita_det
this.cb_vettore=create cb_vettore
this.cb_prove=create cb_prove
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_dettaglio
this.Control[iCurrent+2]=this.cb_ricerca_operaio
this.Control[iCurrent+3]=this.dw_tes_liste_criticita_lista
this.Control[iCurrent+4]=this.dw_tes_liste_criticita_det
this.Control[iCurrent+5]=this.cb_vettore
this.Control[iCurrent+6]=this.cb_prove
end on

on w_tes_liste_criticita.destroy
call super::destroy
destroy(this.cb_dettaglio)
destroy(this.cb_ricerca_operaio)
destroy(this.dw_tes_liste_criticita_lista)
destroy(this.dw_tes_liste_criticita_det)
destroy(this.cb_vettore)
destroy(this.cb_prove)
end on

type cb_dettaglio from commandbutton within w_tes_liste_criticita
integer x = 2171
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettaglio"
end type

event clicked;if dw_tes_liste_criticita_lista.rowcount() > 0 then
	if isnull(dw_tes_liste_criticita_lista.getitemnumber(dw_tes_liste_criticita_lista.getrow(),"anno_reg_lista")) then return
	window_open_parm(w_det_liste_criticita,-1,dw_tes_liste_criticita_lista)
end if
end event

type cb_ricerca_operaio from cb_operai_ricerca within w_tes_liste_criticita
integer x = 2437
integer y = 1184
integer width = 73
integer height = 80
integer taborder = 30
boolean enabled = false
end type

event getfocus;call super::getfocus;dw_tes_liste_criticita_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "compilato_da"
end event

type dw_tes_liste_criticita_lista from uo_cs_xx_dw within w_tes_liste_criticita
integer x = 23
integer y = 20
integer width = 2537
integer height = 740
integer taborder = 40
string dataobject = "d_tes_liste_criticita_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long     ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if


end event

event updatestart;call super::updatestart;if i_extendmode then
   integer li_anno
   long ll_progr,ll_i
		
   for ll_i = 1 to deletedcount()
      li_anno = getitemnumber(ll_i, "anno_reg_lista", delete!, true)
      ll_progr = getitemnumber(ll_i, "num_reg_lista", delete!, true)
		
		delete det_liste_criticita
		where cod_azienda=:s_cs_xx.cod_azienda
		and   anno_reg_lista=:li_anno
		and   num_reg_lista=:ll_progr;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return
		end if
		
	next
	
	commit;
end if
end event

event pcd_new;call super::pcd_new;LONG  l_Idx,ll_progressivo
integer li_anno
string  ls_cod_piano

if i_extendmode then
	cb_dettaglio.enabled = false
	cb_ricerca_operaio.enabled = true
	dw_tes_liste_criticita_det.object.b_ricerca_cliente.enabled = true
	
	window_open(w_seleziona_piano,0)
	ls_cod_piano = s_cs_xx.parametri.parametro_s_15	

	if isnull(ls_cod_piano) or ls_cod_piano = "" then
		g_mb.messagebox("Omnia","Non è stato selezionato alcun piano di qualità. Ripetere l'operazione", stopsign!)
//		pcca.c_valfailed
		return
	end if
	
	li_anno=f_anno_esercizio()
	
	select max(num_reg_lista)
	into   :ll_progressivo
	from   tes_liste_criticita
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_reg_lista=:li_anno;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return
	end if
	
	if isnull(ll_progressivo) or ll_progressivo = 0 then
		ll_progressivo = 1
	else
		ll_progressivo++
	end if
			
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
			SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
			SetItem(l_Idx, "anno_reg_lista", li_anno)
			SetItem(l_Idx, "num_reg_lista", ll_progressivo)
			SetItem(l_Idx, "cod_piano", ls_cod_piano)
		END IF
	NEXT

end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_dettaglio.enabled = false
	cb_ricerca_operaio.enabled = true
	dw_tes_liste_criticita_det.object.b_ricerca_cliente.enabled = true
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	cb_dettaglio.enabled = true
	cb_ricerca_operaio.enabled = false
	dw_tes_liste_criticita_det.object.b_ricerca_cliente.enabled = false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	cb_dettaglio.enabled = true
	cb_ricerca_operaio.enabled = false
	dw_tes_liste_criticita_det.object.b_ricerca_cliente.enabled = false
end if
end event

event pcd_saveafter;call super::pcd_saveafter;if i_extendmode then
	integer li_risposta
	
	li_risposta = wf_genera_det()
	
	if li_risposta = 0 then 
		return
	end if
end if
end event

type dw_tes_liste_criticita_det from uo_cs_xx_dw within w_tes_liste_criticita
integer x = 23
integer y = 780
integer width = 2514
integer height = 940
integer taborder = 50
string dataobject = "d_tes_liste_criticita_det"
borderstyle borderstyle = styleraised!
end type

event getfocus;call super::getfocus;dw_tes_liste_criticita_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_liste_criticita_det,"cod_cliente")
end choose
end event

type cb_vettore from commandbutton within w_tes_liste_criticita
integer x = 1783
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Vettore Q."
end type

event clicked;integer li_anno_registrazione,li_risposta
long    ll_num_registrazione

li_anno_registrazione = dw_tes_liste_criticita_lista.getitemnumber(dw_tes_liste_criticita_lista.getrow(),"anno_reg_lista")
ll_num_registrazione = dw_tes_liste_criticita_lista.getitemnumber(dw_tes_liste_criticita_lista.getrow(),"num_reg_lista")

s_cs_xx.parametri.parametro_i_1 = li_anno_registrazione
s_cs_xx.parametri.parametro_ul_1 = ll_num_registrazione

window_open(w_report_vettore_qualita,-1)


end event

type cb_prove from commandbutton within w_tes_liste_criticita
integer x = 1394
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Prove"
end type

event clicked;if dw_tes_liste_criticita_lista.rowcount() > 0 then
	if isnull(dw_tes_liste_criticita_lista.getitemnumber(dw_tes_liste_criticita_lista.getrow(),"anno_reg_lista")) then return
	window_open_parm(w_reg_risultati_prove,-1,dw_tes_liste_criticita_lista)
end if
end event

