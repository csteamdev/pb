﻿$PBExportHeader$w_tes_piani_qualita.srw
$PBExportComments$Window tes_piani_qualita
forward
global type w_tes_piani_qualita from w_cs_xx_principale
end type
type dw_tes_piani_qualita from uo_cs_xx_dw within w_tes_piani_qualita
end type
type cb_dettaglio from commandbutton within w_tes_piani_qualita
end type
end forward

global type w_tes_piani_qualita from w_cs_xx_principale
int Width=2277
int Height=1385
boolean TitleBar=true
string Title="Piani Qualità"
dw_tes_piani_qualita dw_tes_piani_qualita
cb_dettaglio cb_dettaglio
end type
global w_tes_piani_qualita w_tes_piani_qualita

event pc_setwindow;call super::pc_setwindow;
dw_tes_piani_qualita.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_tes_piani_qualita
end event

on w_tes_piani_qualita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tes_piani_qualita=create dw_tes_piani_qualita
this.cb_dettaglio=create cb_dettaglio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tes_piani_qualita
this.Control[iCurrent+2]=cb_dettaglio
end on

on w_tes_piani_qualita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tes_piani_qualita)
destroy(this.cb_dettaglio)
end on

type dw_tes_piani_qualita from uo_cs_xx_dw within w_tes_piani_qualita
int X=23
int Y=21
int Width=2195
int Height=1141
int TabOrder=10
string DataObject="d_tes_piani_qualita"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_new;call super::pcd_new;if i_extendmode then
	cb_dettaglio.enabled=false
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_dettaglio.enabled=false
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	cb_dettaglio.enabled=true
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	cb_dettaglio.enabled=true
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
   string ls_cod_piano
	long   ll_i
	
   for ll_i = 1 to deletedcount()
      ls_cod_piano = getitemstring(ll_i, "cod_piano", delete!, true)
	
		delete det_piani_qualita
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_piano=:ls_cod_piano;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return
		end if
		
	next
	
	commit;
end if
end event

type cb_dettaglio from commandbutton within w_tes_piani_qualita
int X=1852
int Y=1181
int Width=366
int Height=81
int TabOrder=11
boolean BringToTop=true
string Text="&Dettaglio"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if dw_tes_piani_qualita.rowcount() > 0 then
	if isnull(dw_tes_piani_qualita.getitemstring(dw_tes_piani_qualita.getrow(),"cod_piano")) then return
	window_open_parm(w_det_piani_qualita,-1,dw_tes_piani_qualita)
end if
end event

