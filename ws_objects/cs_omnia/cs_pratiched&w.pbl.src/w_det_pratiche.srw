﻿$PBExportHeader$w_det_pratiche.srw
$PBExportComments$finestra dettagli pratiche
forward
global type w_det_pratiche from w_cs_xx_principale
end type
type dw_det_pratiche_lista from uo_cs_xx_dw within w_det_pratiche
end type
type dw_det_pratiche_det from uo_cs_xx_dw within w_det_pratiche
end type
type gb_1 from groupbox within w_det_pratiche
end type
type rb_ultima_rev from radiobutton within w_det_pratiche
end type
type rb_approvati from radiobutton within w_det_pratiche
end type
type rb_tutti from radiobutton within w_det_pratiche
end type
type cb_note_esterne from commandbutton within w_det_pratiche
end type
type cb_operazione from commandbutton within w_det_pratiche
end type
type cb_chiavi from commandbutton within w_det_pratiche
end type
end forward

global type w_det_pratiche from w_cs_xx_principale
integer width = 2994
integer height = 1680
string title = "Dettagli Pratica"
dw_det_pratiche_lista dw_det_pratiche_lista
dw_det_pratiche_det dw_det_pratiche_det
gb_1 gb_1
rb_ultima_rev rb_ultima_rev
rb_approvati rb_approvati
rb_tutti rb_tutti
cb_note_esterne cb_note_esterne
cb_operazione cb_operazione
cb_chiavi cb_chiavi
end type
global w_det_pratiche w_det_pratiche

type variables
string is_vis_stato_doc, is_vis_ultima_rev, is_num_pratica
string  is_vis_stato_doc2
long il_anno_pratica, il_num_versione, il_num_edizione, il_progressivo
double id_num_protocollo
end variables

forward prototypes
public function integer wf_nuova_vers_det (long fl_num_versione_old, long fl_num_edizione_old, long fl_num_versione_new, long fl_num_edizione_new, long fl_anno_pratica, string fs_num_pratica, long fl_num_versione_tes, long fl_num_edizione_tes, long fl_progressivo, ref string fs_messaggio)
end prototypes

public function integer wf_nuova_vers_det (long fl_num_versione_old, long fl_num_edizione_old, long fl_num_versione_new, long fl_num_edizione_new, long fl_anno_pratica, string fs_num_pratica, long fl_num_versione_tes, long fl_num_edizione_tes, long fl_progressivo, ref string fs_messaggio);// wf_nuova_vers_det(fl_num_versione_old, fl_num_edizione_old, fl_num_versione_new, fl_num_edizione_new, fl_anno_pratica, fs_num_pratica, fs_messaggio)
// funzione che copia i documenti (dettagli) dalla vecchia versione a quella nuova

long ll_num_documenti, ll_i, ll_anno_pratica, ll_num_versione, ll_num_edizione, ll_num_versione_det, &
		ll_num_edizione_det, ll_progressivo
datetime ldt_data_blocco, ldt_oggi
double ld_num_protocollo, ld_num_protocollo_new
string ls_num_pratica, ls_des_blob, ls_note, ls_flag_stato_doc, ls_flag_ultima_revisione, ls_flag_blocco, &
		 ls_cod_utente, ls_db
integer li_risposta
blob lbl_documento
transaction sqlcb

select progressivo
into :ll_num_documenti
from det_pratiche
where cod_azienda = :s_cs_xx.cod_azienda
  and anno_pratica = :fl_anno_pratica
  and num_pratica = :fs_num_pratica
  and num_versione = :fl_num_versione_tes
  and num_edizione = :fl_num_edizione_tes
  and num_versione_det = :fl_num_versione_old
  and num_edizione_det = :fl_num_edizione_old
  and progressivo = :fl_progressivo;
  
if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore lettura dettagli " + sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode = 100 then return 0

ls_cod_utente = s_cs_xx.cod_utente
ldt_oggi = datetime(today())

SELECT det_pratiche.anno_pratica,   
		det_pratiche.num_pratica,   
		det_pratiche.num_versione,   
		det_pratiche.num_edizione,   
		det_pratiche.num_versione_det,   
		det_pratiche.num_edizione_det,   
		det_pratiche.progressivo,   
		det_pratiche.des_blob,   
		det_pratiche.num_protocollo,   
		det_pratiche.note,   
		det_pratiche.flag_stato_doc,   
		det_pratiche.flag_ultima_revisione,   
		det_pratiche.flag_blocco,   
		det_pratiche.data_blocco   
into :ll_anno_pratica,   
		:ls_num_pratica,   
		:ll_num_versione,   
		:ll_num_edizione,   
		:ll_num_versione_det,   
		:ll_num_edizione_det,   
		:ll_progressivo,   
		:ls_des_blob,   
		:ld_num_protocollo,   
		:ls_note,   
		:ls_flag_stato_doc,   
		:ls_flag_ultima_revisione,   
		:ls_flag_blocco,   
		:ldt_data_blocco
from det_pratiche  
where cod_azienda = :s_cs_xx.cod_azienda 
  and anno_pratica = :fl_anno_pratica   
  and num_pratica = :fs_num_pratica 
  and num_versione = :fl_num_versione_tes  
  and num_edizione = :fl_num_edizione_tes     
  and num_versione_det = :fl_num_versione_old
  and num_edizione_det = :fl_num_edizione_old
  and progressivo = :fl_progressivo;


				
if (sqlca.sqlcode = 100) then return 0
if sqlca.sqlcode<>0 then
	fs_messaggio = "Errore lettura dettagli  " + sqlca.sqlerrtext
	return -1
end if

// nuovo numero protocollo: 
setnull(ld_num_protocollo_new)

select max(num_protocollo)
into :ld_num_protocollo_new
from tab_protocolli
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore lettura Num. protocollo: " + sqlca.sqlerrtext
	return -1
end if
if (sqlca.sqlcode <> 0 ) or isnull(ld_num_protocollo_new) then
	ld_num_protocollo_new = 0
end if

ld_num_protocollo_new = ld_num_protocollo_new + 1	

insert into tab_protocolli
(cod_azienda, num_protocollo)
values (:s_cs_xx.cod_azienda, :ld_num_protocollo_new);
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Impossibile Aggiornare la tabella protocolli" + sqlca.sqlerrtext
	return -1
end if

// 11-07-2002 modifiche Michela: controllo enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob det_pratiche.blob
	into :lbl_documento
	from  det_pratiche
	where cod_azienda = :s_cs_xx.cod_azienda
	  and anno_pratica = :fl_anno_pratica
	  and num_pratica = :fs_num_pratica
	  and num_versione = :fl_num_versione_tes
	  and num_edizione = :fl_num_edizione_tes
	  and num_versione_det = :fl_num_versione_old
	  and num_edizione_det = :fl_num_edizione_old
	  and progressivo = :fl_progressivo
	using  sqlcb;

	if sqlcb.sqlcode<>0 then
		fs_messaggio = "Errore lettura blob in det_pratiche: " + sqlcb.sqlerrtext
		destroy sqlcb;
		return -1
	end if
	
	destroy sqlcb;
	
else
	
	selectblob det_pratiche.blob
	into :lbl_documento
	from  det_pratiche
	where cod_azienda = :s_cs_xx.cod_azienda
	  and anno_pratica = :fl_anno_pratica
	  and num_pratica = :fs_num_pratica
	  and num_versione = :fl_num_versione_tes
	  and num_edizione = :fl_num_edizione_tes
	  and num_versione_det = :fl_num_versione_old
	  and num_edizione_det = :fl_num_edizione_old
	  and progressivo = :fl_progressivo;

	if sqlca.sqlcode<>0 then
		fs_messaggio = "Errore lettura blob in det_pratiche: " + sqlca.sqlerrtext
		return -1
	end if

end if
// fine modifica


INSERT INTO det_pratiche  
		( cod_azienda,   
		  anno_pratica,   
		  num_pratica,   
		  num_versione,   
		  num_edizione,   
		  num_versione_det,   
		  num_edizione_det,   
		  progressivo,   
		  des_blob,   
		  num_protocollo,   
		  note,   
		  flag_stato_doc,   
		  flag_ultima_revisione,   
		  flag_blocco,   
		  data_blocco )  
values (:s_cs_xx.cod_azienda,
		 :ll_anno_pratica,   
			:ls_num_pratica,   
			:ll_num_versione,   
			:ll_num_edizione,   
			:fl_num_versione_new,   
			:fl_num_edizione_new,   
			:ll_progressivo,   
			:ls_des_blob,   
			:ld_num_protocollo_new,   
			:ls_note,   
			'N',   
			'S',   
			:ls_flag_blocco,   
			:ldt_data_blocco) ;
if sqlca.sqlcode<>0 then
	fs_messaggio = "Errore inserimento det_pratiche: " + sqlca.sqlerrtext
//	messagebox("debug", "Errore inserimento det_pratiche: " + sqlca.sqlerrtext)
	return -1
end if

select progressivo
into :ll_num_documenti
from det_pratiche
where cod_azienda = :s_cs_xx.cod_azienda
  and anno_pratica = :fl_anno_pratica
  and num_pratica = :fs_num_pratica
  and num_versione = :fl_num_versione_tes
  and num_edizione = :fl_num_edizione_tes
  and num_versione_det = :fl_num_versione_old
  and num_edizione_det = :fl_num_edizione_old
  and progressivo = :fl_progressivo;
  
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore lettura 2 in det_pratiche: " + sqlca.sqlerrtext
	return -1
end if
	
//messagebox("debug "+ s_cs_xx.cod_azienda + " - " + string(fl_anno_pratica) + " " + ls_num_pratica ,  &
//				string(fl_num_versione_tes) + "/" + string(fl_num_edizione_tes)+ "det:" + string(fl_num_versione_old) + "/" + string(fl_num_edizione_old) + " - " + string(fl_progressivo))

if not isnull(lbl_documento) then
	
	
	// idem,controllo l'enginetype
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
		updateblob det_pratiche
		set blob = :lbl_documento
		where cod_azienda = :s_cs_xx.cod_azienda 
		  and anno_pratica = :fl_anno_pratica   
		  and num_pratica = :fs_num_pratica 
		  and num_versione = :ll_num_versione  
		  and num_edizione = :ll_num_edizione  
		  and num_versione_det = :fl_num_versione_new
		  and num_edizione_det = :fl_num_edizione_new
		  and progressivo = :ll_progressivo
		  using  sqlcb;
		  
		if sqlcb.sqlcode<>0 then
			fs_messaggio = "Errore update blob in det_pratiche: " + sqlcb.sqlerrtext
			destroy sqlcb;
			return -1
		end if
		
		destroy sqlcb;
		
	else
		
		updateblob det_pratiche
		set blob = :lbl_documento
		where cod_azienda = :s_cs_xx.cod_azienda 
		  and anno_pratica = :fl_anno_pratica   
		  and num_pratica = :fs_num_pratica 
		  and num_versione = :ll_num_versione  
		  and num_edizione = :ll_num_edizione  
		  and num_versione_det = :fl_num_versione_new
		  and num_edizione_det = :fl_num_edizione_new
		  and progressivo = :ll_progressivo;
		  
		if sqlca.sqlcode<>0 then
			fs_messaggio = "Errore update blob in det_pratiche: " + sqlca.sqlerrtext
			return -1
		end if
		
	end if
end if
	

return 0

end function

on w_det_pratiche.create
int iCurrent
call super::create
this.dw_det_pratiche_lista=create dw_det_pratiche_lista
this.dw_det_pratiche_det=create dw_det_pratiche_det
this.gb_1=create gb_1
this.rb_ultima_rev=create rb_ultima_rev
this.rb_approvati=create rb_approvati
this.rb_tutti=create rb_tutti
this.cb_note_esterne=create cb_note_esterne
this.cb_operazione=create cb_operazione
this.cb_chiavi=create cb_chiavi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_pratiche_lista
this.Control[iCurrent+2]=this.dw_det_pratiche_det
this.Control[iCurrent+3]=this.gb_1
this.Control[iCurrent+4]=this.rb_ultima_rev
this.Control[iCurrent+5]=this.rb_approvati
this.Control[iCurrent+6]=this.rb_tutti
this.Control[iCurrent+7]=this.cb_note_esterne
this.Control[iCurrent+8]=this.cb_operazione
this.Control[iCurrent+9]=this.cb_chiavi
end on

on w_det_pratiche.destroy
call super::destroy
destroy(this.dw_det_pratiche_lista)
destroy(this.dw_det_pratiche_det)
destroy(this.gb_1)
destroy(this.rb_ultima_rev)
destroy(this.rb_approvati)
destroy(this.rb_tutti)
destroy(this.cb_note_esterne)
destroy(this.cb_operazione)
destroy(this.cb_chiavi)
end on

event pc_setwindow;call super::pc_setwindow;dw_det_pratiche_lista.set_dw_key("cod_azienda")
dw_det_pratiche_lista.set_dw_key("anno_pratica")
dw_det_pratiche_lista.set_dw_key("num_pratica")
dw_det_pratiche_lista.set_dw_key("num_versione")
dw_det_pratiche_lista.set_dw_key("num_edizione")

dw_det_pratiche_lista.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)
dw_det_pratiche_det.set_dw_options(sqlca, &
                                    dw_det_pratiche_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

iuo_dw_main = dw_det_pratiche_lista

is_vis_stato_doc = "%"
is_vis_stato_doc2 = "%"
is_vis_ultima_rev = "S"



end event

event open;call super::open;is_num_pratica = dw_det_pratiche_lista.i_parentdw.getitemstring(dw_det_pratiche_lista.i_parentdw.i_selectedrows[1], "num_pratica")
il_anno_pratica = dw_det_pratiche_lista.i_parentdw.getitemnumber(dw_det_pratiche_lista.i_parentdw.i_selectedrows[1], "anno_pratica")
il_num_versione = dw_det_pratiche_lista.i_parentdw.getitemnumber(dw_det_pratiche_lista.i_parentdw.i_selectedrows[1], "num_versione")
il_num_edizione = dw_det_pratiche_lista.i_parentdw.getitemnumber(dw_det_pratiche_lista.i_parentdw.i_selectedrows[1], "num_edizione")

w_det_pratiche.title = "Dettagli Pratica: " + string(il_anno_pratica) + " - " + &
		is_num_pratica + "; ver. " + string(il_num_versione) + "; ed. " + string(il_num_edizione)


end event

type dw_det_pratiche_lista from uo_cs_xx_dw within w_det_pratiche
integer x = 23
integer y = 20
integer width = 2903
integer height = 480
integer taborder = 40
string dataobject = "d_det_pratiche_lista"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_num_righe

ll_errore = retrieve(s_cs_xx.cod_azienda, il_anno_pratica, is_num_pratica, il_num_versione, il_num_edizione, is_vis_stato_doc, is_vis_ultima_rev, is_vis_stato_doc2 )

ll_num_righe = rowcount()
// messagebox("debug", string(ll_num_righe))
if ll_num_righe = 0 then
	cb_note_esterne.enabled = false
	cb_operazione.enabled = false
else
	cb_note_esterne.enabled = true
	cb_operazione.enabled = true
end if


if ll_errore < 0 then
   pcca.error = c_fatal
end if





end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   this.setitem(ll_i, "anno_pratica", il_anno_pratica)
	this.setitem(ll_i, "num_pratica", is_num_pratica)
	this.setitem(ll_i, "num_versione", il_num_versione)
	this.setitem(ll_i, "num_edizione", il_num_edizione)

next


end event

event pcd_new;call super::pcd_new;if i_extendmode then
	string ls_flag_modifica, ls_flag_ultima_revisione 
	double ld_num_protocollo
	
	// verifico autorizzazioni necessarie
	
//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//	SELECT flag_modifica  
//	 INTO :ls_flag_modifica  
//	 FROM mansionari  
//	 where cod_azienda = :s_cs_xx.cod_azienda
//		and cod_utente = :s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_modifica = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then ls_flag_modifica = 'S'
//--- Fine modifica Giulio
	
	if (sqlca.sqlcode <> 0) then
		g_mb.messagebox("Errore", "Errore lettura autorizzazione nei mansionari")
		postevent("updatestart")
	end if
	
	if ls_flag_modifica = "N" then
//		messagebox("Errore", "Manca l'autorizzazione alla modifica")
		postevent("updatestart")
	end if
	
	// Assegnare un nuovo numero di protocollo
	
	setnull(ld_num_protocollo)
	
	select max(num_protocollo)
	into :ld_num_protocollo
	from tab_protocolli
	where cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore", "Impossibile Leggere la tabella protocolli " + sqlca.sqlerrtext )
		dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
		dw_det_pratiche_det.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	if (sqlca.sqlcode <> 0 ) or isnull(ld_num_protocollo) then
		ld_num_protocollo = 0
	end if
	
	ld_num_protocollo = ld_num_protocollo + 1	
	
	insert into tab_protocolli
	(cod_azienda, num_protocollo)
	values (:s_cs_xx.cod_azienda, :ld_num_protocollo);
	// messagebox("debug", string(ld_num_protocollo))
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Errore", "Impossibile Aggiornare la tabella protocolli" + sqlca.sqlerrtext)
		dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
		dw_det_pratiche_det.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	this.setitem(this.getrow(), "num_protocollo", ld_num_protocollo)
	this.setitem(this.getrow(), "flag_stato_doc", "N")
	this.setitem(this.getrow(), "flag_ultima_revisione", "S")
	
	cb_chiavi.enabled = false
	cb_operazione.enabled = false
	cb_note_esterne.enabled = false

end if




end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	long ll_i
	string ls_flag_ultima_rev
	ll_i = getrow()
	if ll_i > 0 and (not isnull(this.getitemstring(ll_i, "num_pratica"))) then
		ls_flag_ultima_rev = getitemstring(ll_i, "flag_ultima_revisione")
		
		il_progressivo = ll_i
		if ls_flag_ultima_rev = "N" then
			cb_operazione.enabled = false
			cb_chiavi.enabled = false
		else
			cb_operazione.enabled = true
			cb_chiavi.enabled = true
		end if

	end if


end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	string ls_flag_modifica, ls_flag_ultima_revisione 
	
	
//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//	SELECT flag_modifica  
//	 INTO :ls_flag_modifica  
//	 FROM mansionari  
//	 where cod_azienda = :s_cs_xx.cod_azienda
//		and cod_utente = :s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_modifica = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then ls_flag_modifica = 'S'
//--- Fine modifica Giulio
	
	if (sqlca.sqlcode <> 0) then
		g_mb.messagebox("Errore", "Errore lettura autorizzazione nei mansionari")
		postevent("updatestart")
	end if
	
	if ls_flag_modifica = "N" then
//		messagebox("Errore", "Manca l'autorizzazione alla modifica")
		postevent("updatestart")
	end if
	
	ls_flag_ultima_revisione = getitemstring(getrow(), "flag_ultima_revisione")
	if ls_flag_ultima_revisione = "N" then
		g_mb.messagebox("Attenzione", "Questo documento non è l'ultima revisione")
		// permetto di modificare qualcosa, es, flag_ultima_versione, a rischio dell'operatore
	end if
	cb_chiavi.enabled = false
	cb_operazione.enabled = false
	cb_note_esterne.enabled = false
end if


end event

event updatestart;call super::updatestart;
string ls_flag_elimina, ls_flag_ultima_revisione, ls_num_pratica, ls_cod_famiglia_chiavi, ls_cod_chiave,  &
		 ls_verifica, ls_flag_modifica
long ll_anno_pratica, ll_num_edizione, ll_num_versione, ll_num_ed_prec, ll_num_ver_prec, ll_prova, ll_i, ll_j, &
		ll_num_edizione_det, ll_num_versione_det, ll_progressivo
double ld_num_protocollo

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT flag_elimina, flag_modifica
// INTO :ls_flag_elimina  , :ls_flag_modifica
// FROM mansionari  
// where cod_azienda = :s_cs_xx.cod_azienda
//	and cod_utente = :s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_modifica = 'N'
ls_flag_elimina = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then ls_flag_modifica = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.elimina) then ls_flag_elimina = 'S'
//--- Fine modifica Giulio

if (sqlca.sqlcode <> 0) then
	g_mb.messagebox("Errore", "Errore lettura autorizzazione nei mansionari")
	dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
	pcca.error = c_fatal
	return 1
end if

if ls_flag_modifica = "N" then
	g_mb.messagebox("Errore", "Manca l'autorizzazione alle modifiche")
	dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
	pcca.error = c_fatal
	return 1
end if


ll_j = this.deletedcount()
if ll_j > 0 then
	
	if ls_flag_elimina = "N" then
		g_mb.messagebox("Errore", "Manca l'autorizzazione alla cancellazione")
		dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return 1
	end if
	
	cb_chiavi.enabled = false
	cb_operazione.enabled = false
	cb_note_esterne.enabled = false
	
	for ll_i = 1 to ll_j
		ls_flag_ultima_revisione = getitemstring(ll_i, "flag_ultima_revisione", delete!, true)
		if ls_flag_ultima_revisione = "N" then
			g_mb.messagebox("Errore", "Cancellazione ammessa solo sull'ultima revisione")
			dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if
		
		ld_num_protocollo = getitemnumber(ll_i, "num_protocollo", delete!, true)
		if isnull(ld_num_protocollo) then 
			g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore lettura numero protocollo " )
			dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if
		
		delete from tab_chiavi_protocollo
		where cod_azienda = :s_cs_xx.cod_azienda
		  and num_protocollo = :ld_num_protocollo;
		  
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
			dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if
		
				// cerco l'ultima versione/edizione
		ll_num_edizione_det =  getitemnumber(ll_i, "num_edizione_det", delete!, true) 
		ll_num_versione_det =  getitemnumber(ll_i, "num_versione_det", delete!, true)
		ll_progressivo = getitemnumber(ll_i, "progressivo", delete!, true)
		
		ll_num_ver_prec = ll_num_versione_det - 1
		ll_num_ed_prec = ll_num_edizione_det - 1
		
		setnull(ll_prova)
		select max(num_edizione_det)
		into :ll_prova
		from det_pratiche
		where cod_azienda = :s_cs_xx.cod_azienda
		  and anno_pratica =:il_anno_pratica
		  and num_pratica = :is_num_pratica
		  and num_versione = :il_num_versione
		  and num_edizione = :il_num_edizione 
		  and num_versione_det = :ll_num_versione_det
		  and num_edizione_det <= :ll_num_ed_prec;
		
		if sqlca.sqlcode = 0 and (not isnull(ll_prova)) then  // ultima release: stessa versione, edizione -1
			select num_edizione_det
			into :ll_prova
			from det_pratiche
			where cod_azienda = :s_cs_xx.cod_azienda
			  and anno_pratica =:il_anno_pratica
			  and num_pratica = :is_num_pratica
			  and num_versione = :il_num_versione
			  and num_edizione = :il_num_edizione 
			  and num_versione_det = :ll_num_versione_det
			  and num_edizione_det = :ll_prova
			  and progressivo = : ll_progressivo;
		
			if sqlca.sqlcode = 0 then
				update det_pratiche
				set flag_ultima_revisione = 'S'
				where cod_azienda = :s_cs_xx.cod_azienda
				  and anno_pratica =:il_anno_pratica
				  and num_pratica = :is_num_pratica
				  and num_versione = :il_num_versione
				  and num_edizione = :il_num_edizione 
				  and num_versione_det = :ll_num_versione_det
				  and num_edizione_det = :ll_prova
				  and progressivo = : ll_progressivo;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Errore 1", "Errore ripristino ultima edizione " + sqlca.sqlerrtext)
//					dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
//					pcca.error = c_fatal
//					return 1
				end if
			else
				g_mb.messagebox("Errore 2", "Errore ripristino ultima edizione " + sqlca.sqlerrtext)
//				dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
//				pcca.error = c_fatal
//				return 1
			end if	
		else	// ultima release: versione -1, edizione  da trovare
			setnull(ll_prova)
			select max(num_versione_det)
			into :ll_prova
			from det_pratiche
				where cod_azienda = :s_cs_xx.cod_azienda
				  and anno_pratica =:il_anno_pratica
				  and num_pratica = :is_num_pratica
				  and num_versione = :il_num_versione
				  and num_edizione = :il_num_edizione 
				  and num_versione_det <= :ll_num_ver_prec;
			
			if sqlca.sqlcode = 0 and (not isnull(ll_prova)) then  // c' e' una versione precedente
				ll_num_ver_prec = ll_prova
				select max(num_edizione_det)	// determino edizione
				into :ll_num_ed_prec
				from det_pratiche
				where cod_azienda = :s_cs_xx.cod_azienda
				  and anno_pratica =:il_anno_pratica
				  and num_pratica = :is_num_pratica
				  and num_versione = :il_num_versione
				  and num_edizione = :il_num_edizione 
				  and num_versione_det = :ll_num_ver_prec;
				
				if sqlca.sqlcode = 0 then  				// verifico
					select num_edizione_det
					into :ll_prova
					from det_pratiche
					where cod_azienda = :s_cs_xx.cod_azienda
					  and anno_pratica =:il_anno_pratica
					  and num_pratica = :is_num_pratica
					  and num_versione = :il_num_versione
					  and num_edizione = :il_num_edizione 
					  and num_versione_det = :ll_num_ver_prec
					  and num_edizione_det = :ll_num_ed_prec
					  and progressivo = : ll_progressivo;
					if sqlca.sqlcode = 0 then	// update
						update det_pratiche
						set flag_ultima_revisione = 'S'
						where cod_azienda = :s_cs_xx.cod_azienda
						  and anno_pratica =:il_anno_pratica
						  and num_pratica = :is_num_pratica
						  and num_versione = :il_num_versione
						  and num_edizione = :il_num_edizione 
						  and num_versione_det = :ll_num_ver_prec
						  and num_edizione_det = :ll_num_ed_prec
						  and progressivo = : ll_progressivo;
						if sqlca.sqlcode <> 0 then
							g_mb.messagebox("Errore 3", "Errore ripristino ultima revisione" + sqlca.sqlerrtext)
//							dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
//							pcca.error = c_fatal
//							return 1
						end if
					end if	// fine update
				else
					g_mb.messagebox("Errore 4", "Errore ripristino ultima edizione " + sqlca.sqlerrtext)
//					dw_det_pratiche_lista.set_dw_view(c_ignorechanges)
//					pcca.error = c_fatal
//					return 1
				end if // fine verifico
			end if	// fine c' e' una versione precedente
		end if	// fine	else
		
		
		
		
		
	next
end if	// ll_j > 0



	

end event

event updateend;call super::updateend;long ll_i
double ld_num_protocollo
string ls_cod_famiglia_chiavi, ls_cod_chiave

for ll_i = 1 to this.deletedcount()
	ld_num_protocollo = getitemnumber(ll_i, "num_protocollo", delete!, true)

	delete from tab_protocolli
	where cod_azienda = :s_cs_xx.cod_azienda
	  and num_protocollo = :ld_num_protocollo;
	  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
		return 1
	end if
	
	cb_chiavi.enabled = true
	cb_operazione.enabled = true
	cb_note_esterne.enabled = true
	
next
end event

event pcd_delete;call super::pcd_delete;if i_extendmode then
	string ls_flag_elimina, ls_flag_ultima_revisione 
	
	//Giulio: 10/11/2011 cambio gestione privilegi mansionario
//	SELECT flag_elimina  
//	 INTO :ls_flag_elimina  
//	 FROM mansionari  
//	 where cod_azienda = :s_cs_xx.cod_azienda
//		and cod_utente = :s_cs_xx.cod_utente;
	
	uo_mansionario luo_mansionario
	luo_mansionario = create uo_mansionario
	
	ls_flag_elimina = 'N'
	
	if luo_mansionario.uof_get_privilege(luo_mansionario.elimina) then ls_flag_elimina = 'S'
	

//	if (sqlca.sqlcode <> 0) then
	if isnull(luo_mansionario.uof_get_cod_mansionario()) then
		g_mb.messagebox("Errore", "Errore lettura autorizzazione nei mansionari")
		postevent("updatestart")
	end if
	//--- Fine modifica Giulio
	
	if ls_flag_elimina = "N" then
//		messagebox("Errore", "Manca l'autorizzazione alla cancellazione")
		postevent("updatestart")
	end if

end if



end event

type dw_det_pratiche_det from uo_cs_xx_dw within w_det_pratiche
integer x = 23
integer y = 520
integer width = 2903
integer height = 840
integer taborder = 50
string dataobject = "d_det_pratiche_det"
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_db
	integer li_risposta
	
   choose case i_colname
      case "note"
			li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
			// ----- controllo lunghezza note per ASE
			if ls_db = "SYBASE_ASE" and len(i_coltext) > 255 then
				g_mb.messagebox("Attenzione", "Nota limitata a 255 caratteri")
				i_coltext = left(i_coltext, 255)
				this.setitem(this.getrow(), "note", i_coltext)
				this.settext(i_coltext)
				return 2
				pcca.error = c_fatal
			end if
			// -----  fine controllo lunghezza note per ASE
	end choose
end if


end event

type gb_1 from groupbox within w_det_pratiche
integer x = 23
integer y = 1380
integer width = 1509
integer height = 160
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza Pratiche"
end type

type rb_ultima_rev from radiobutton within w_det_pratiche
integer x = 91
integer y = 1440
integer width = 512
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Ultima Revisione"
boolean checked = true
end type

event clicked;if this.checked = true then
	is_vis_stato_doc = "%"
	is_vis_stato_doc2 = "%"
	is_vis_ultima_rev = "S"
else
	is_vis_stato_doc = "%"
	is_vis_stato_doc2 = "%"
	is_vis_ultima_rev = "N"
end if

w_det_pratiche.postevent("pc_view")
w_det_pratiche.postevent("pc_retrieve")

end event

type rb_approvati from radiobutton within w_det_pratiche
integer x = 663
integer y = 1440
integer width = 352
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Approvate"
end type

event clicked;if this.checked = true then
	is_vis_stato_doc = "A"
	is_vis_stato_doc2 = "L"
	is_vis_ultima_rev = "S"
else
	is_vis_stato_doc = "%"
	is_vis_stato_doc2 = "%"
	is_vis_ultima_rev = "S"
end if

w_det_pratiche.postevent("pc_view")
w_det_pratiche.postevent("pc_retrieve")

end event

type rb_tutti from radiobutton within w_det_pratiche
integer x = 1166
integer y = 1440
integer width = 274
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tutte"
end type

event clicked;if this.checked = true then
	is_vis_stato_doc = "%"
	is_vis_stato_doc2 = "%"
	is_vis_ultima_rev = "%"
else
	is_vis_stato_doc = "%"
	is_vis_stato_doc2 = "%"
	is_vis_ultima_rev = "%"
end if

w_det_pratiche.postevent("pc_view")
w_det_pratiche.postevent("pc_retrieve")

end event

type cb_note_esterne from commandbutton within w_det_pratiche
integer x = 2560
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string      ls_db, ls_doc

long        ll_i, ll_num_versione_det, ll_num_edizione_det, ll_progressivo, ll_prog_mimetype

integer     li_risposta

transaction sqlcb

blob        lbl_null, lbl_blob

setnull(lbl_null)

ll_i = dw_det_pratiche_lista.getrow()

ll_num_versione_det = dw_det_pratiche_lista.getitemnumber(ll_i, "num_versione_det")
ll_num_edizione_det = dw_det_pratiche_lista.getitemnumber(ll_i, "num_edizione_det")
ll_progressivo = dw_det_pratiche_lista.getitemnumber(ll_i, "progressivo")
ll_prog_mimetype = dw_det_pratiche_lista.getitemnumber( ll_i, "prog_mimetype")


select prog_mimetype
into :ll_prog_mimetype
from det_pratiche
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_pratica = :il_anno_pratica and 
	num_pratica = :is_num_pratica and
	num_versione = :il_num_versione and
	num_edizione = :il_num_edizione and
	num_versione_det = :ll_num_versione_det and
	num_edizione_det = :ll_num_edizione_det and
	progressivo = :ll_progressivo;
	
selectblob blob
into :lbl_blob
from det_pratiche
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_pratica = :il_anno_pratica and 
	num_pratica = :is_num_pratica and
	num_versione = :il_num_versione and
	num_edizione = :il_num_edizione and
	num_versione_det = :ll_num_versione_det and
	num_edizione_det = :ll_num_edizione_det and
	progressivo = :ll_progressivo;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update det_pratiche
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_pratica = :il_anno_pratica and 
			num_pratica = :is_num_pratica and
			num_versione = :il_num_versione and
			num_edizione = :il_num_edizione and
			num_versione_det = :ll_num_versione_det and
			num_edizione_det = :ll_num_edizione_det and
			progressivo = :ll_progressivo;
	else
		updateblob det_pratiche
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_pratica = :il_anno_pratica and 
			num_pratica = :is_num_pratica and
			num_versione = :il_num_versione and
			num_edizione = :il_num_edizione and
			num_versione_det = :ll_num_versione_det and
			num_edizione_det = :ll_num_edizione_det and
			progressivo = :ll_progressivo;
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
		
	update det_pratiche
	set prog_mimetype = :ll_prog_mimetype
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_pratica = :il_anno_pratica and 
		num_pratica = :is_num_pratica and
		num_versione = :il_num_versione and
		num_edizione = :il_num_edizione and
		num_versione_det = :ll_num_versione_det and
		num_edizione_det = :ll_num_edizione_det and
		progressivo = :ll_progressivo;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if


// 11-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//
//if ls_db = "MSSQL" then
//	
//	li_risposta = f_crea_sqlcb(sqlcb)
//	
//	selectblob det_pratiche.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       det_pratiche
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           anno_pratica = :il_anno_pratica and 
//	           num_pratica = :is_num_pratica and
//				  num_versione = :il_num_versione and
//				  num_edizione = :il_num_edizione and
//				  num_versione_det = :ll_num_versione_det and
//				  num_edizione_det = :ll_num_edizione_det and
//				  progressivo = :ll_progressivo
//	using      sqlcb;
//
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//	
//else
//	
//	selectblob det_pratiche.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       det_pratiche
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           anno_pratica = :il_anno_pratica and 
//	           num_pratica = :is_num_pratica and
//				  num_versione = :il_num_versione and
//				  num_edizione = :il_num_edizione and
//				  num_versione_det = :ll_num_versione_det and
//				  num_edizione_det = :ll_num_edizione_det and
//				  progressivo = :ll_progressivo;
//
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//end if
//
//s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype
//
//window_open(w_ole_documenti, 0)
//
//ll_prog_mimetype = s_cs_xx.parametri.parametro_d_1
//
//if not isnull(s_cs_xx.parametri.parametro_bl_1) and len(s_cs_xx.parametri.parametro_bl_1) > 0 and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob det_pratiche
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_pratica = :il_anno_pratica and 
//					  num_pratica = :is_num_pratica and
//					  num_versione = :il_num_versione and
//					  num_edizione = :il_num_edizione and
//					  num_versione_det = :ll_num_versione_det and
//					  num_edizione_det = :ll_num_edizione_det and
//					  progressivo = :ll_progressivo
//		using      sqlcb;
//		
//		destroy sqlcb;
//					  
//	else
//		
//	   updateblob det_pratiche
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_pratica = :il_anno_pratica and 
//					  num_pratica = :is_num_pratica and
//					  num_versione = :il_num_versione and
//					  num_edizione = :il_num_edizione and
//					  num_versione_det = :ll_num_versione_det and
//					  num_edizione_det = :ll_num_edizione_det and
//					  progressivo = :ll_progressivo;
//	
//	end if
//
////   update     det_pratiche
////   set        prog_mimetype = :ll_prog_mimetype
////	where      cod_azienda = :s_cs_xx.cod_azienda and
////				  anno_pratica = :il_anno_pratica and 
////				  num_pratica = :is_num_pratica and
////				  num_versione = :il_num_versione and
////				  num_edizione = :il_num_edizione and
////				  num_versione_det = :ll_num_versione_det and
////				  num_edizione_det = :ll_num_edizione_det and
////				  progressivo = :ll_progressivo;
//
//   commit;
//	
//	dw_det_pratiche_det.setitem( ll_i, "prog_mimetype", ll_prog_mimetype)
//	
//	parent.triggerevent("pc_save")
//	
//	parent.triggerevent("pc_view")	
//end if
end event

type cb_operazione from commandbutton within w_det_pratiche
integer x = 2126
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Operazione"
end type

event clicked;long ll_num_edizione_new, ll_num_versione_new, ll_num_edizione_old, ll_num_versione_old, ll_return, &
	  ll_anno_pratica, ll_num_versione_tes, ll_num_edizione_tes, ll_progressivo
boolean ib_modificato
string ls_num_pratica, ls_messaggio

ib_modificato = false
s_cs_xx.parametri.parametro_s_1 = dw_det_pratiche_det.getitemstring(dw_det_pratiche_det.getrow(),"flag_stato_doc")
s_cs_xx.parametri.parametro_s_2 = dw_det_pratiche_det.getitemstring(dw_det_pratiche_det.getrow(),"flag_ultima_revisione")

ll_anno_pratica = dw_det_pratiche_det.getitemnumber(dw_det_pratiche_det.getrow(),"anno_pratica")
ls_num_pratica  = dw_det_pratiche_det.getitemstring(dw_det_pratiche_det.getrow(),"num_pratica")
s_cs_xx.parametri.parametro_ul_3 = ll_anno_pratica
s_cs_xx.parametri.parametro_s_3 = ls_num_pratica 

ll_num_versione_tes = dw_det_pratiche_det.getitemnumber(dw_det_pratiche_det.getrow(),"num_versione")
ll_num_edizione_tes = dw_det_pratiche_det.getitemnumber(dw_det_pratiche_det.getrow(),"num_edizione")
s_cs_xx.parametri.parametro_d_1 = ll_num_versione_tes
s_cs_xx.parametri.parametro_d_2  = ll_num_edizione_tes

ll_num_versione_old = dw_det_pratiche_det.getitemnumber(dw_det_pratiche_det.getrow(),"num_versione_det")
ll_num_edizione_old = dw_det_pratiche_det.getitemnumber(dw_det_pratiche_det.getrow(),"num_edizione_det")
s_cs_xx.parametri.parametro_ul_1 = ll_num_edizione_old
s_cs_xx.parametri.parametro_ul_2 = ll_num_versione_old 
ll_progressivo = dw_det_pratiche_det.getitemnumber(dw_det_pratiche_det.getrow(),"progressivo")

window_open(w_operazioni_pratica_det, 0)

if not isnull(s_cs_xx.parametri.parametro_s_1) then
	if s_cs_xx.parametri.parametro_s_1 = "V" then	//Verifico
		dw_det_pratiche_det.setitem(dw_det_pratiche_det.getrow(), "flag_stato_doc", "V")
		ib_modificato = true
	end if
	if s_cs_xx.parametri.parametro_s_1 = "A" then	//Approvo
		dw_det_pratiche_det.setitem(dw_det_pratiche_det.getrow(), "flag_stato_doc", "A")		
		ib_modificato = true
	end if
	if s_cs_xx.parametri.parametro_s_1 = "L" then	//Valido
		dw_det_pratiche_det.setitem(dw_det_pratiche_det.getrow(), "flag_stato_doc", "L")		
		ib_modificato = true
	end if
	if s_cs_xx.parametri.parametro_s_1 = "N" then	//Nuova revisione
		ll_num_edizione_new = s_cs_xx.parametri.parametro_ul_1
		ll_num_versione_new = s_cs_xx.parametri.parametro_ul_2 
		// copio tutti i documenti da versione/edizione corrente a quella nuova: 
		commit;
		ll_return = wf_nuova_vers_det(ll_num_versione_old, ll_num_edizione_old, ll_num_versione_new, ll_num_edizione_new, ll_anno_pratica, ls_num_pratica, ll_num_versione_tes, ll_num_edizione_tes, ll_progressivo, ls_messaggio)
		if ll_return = 0 then
			dw_det_pratiche_det.setitem(dw_det_pratiche_det.getrow(), "flag_ultima_revisione", "N")		
			ib_modificato = true
		else
			g_mb.messagebox("Errore nuova versione", ls_messaggio)
			ib_modificato = false
			rollback;
		end if
	end if
end if

if ib_modificato = true then
	parent.postevent("pc_save")
end if

end event

type cb_chiavi from commandbutton within w_det_pratiche
integer x = 1714
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiavi"
end type

event clicked;window_open_parm(w_tab_chiavi_protocollo, -1, dw_det_pratiche_lista)
end event

