﻿$PBExportHeader$w_det_prog_manut.srw
$PBExportComments$Finestra di dettaglio documenti per programmi manutenzioni
forward
global type w_det_prog_manut from w_cs_xx_principale
end type
type dw_det_prog_manutenzioni from uo_cs_xx_dw within w_det_prog_manut
end type
type cb_chiavi from commandbutton within w_det_prog_manut
end type
type cb_note_esterne from commandbutton within w_det_prog_manut
end type
end forward

global type w_det_prog_manut from w_cs_xx_principale
integer width = 2994
integer height = 1104
string title = "Documenti Programmi Manutenzione"
dw_det_prog_manutenzioni dw_det_prog_manutenzioni
cb_chiavi cb_chiavi
cb_note_esterne cb_note_esterne
end type
global w_det_prog_manut w_det_prog_manut

event pc_setwindow;call super::pc_setwindow;dw_det_prog_manutenzioni.set_dw_key("cod_azienda")
dw_det_prog_manutenzioni.set_dw_options(sqlca, &
											i_openparm, &
											c_scrollparent, &
											c_default)

iuo_dw_main = dw_det_prog_manutenzioni
end event

on w_det_prog_manut.create
int iCurrent
call super::create
this.dw_det_prog_manutenzioni=create dw_det_prog_manutenzioni
this.cb_chiavi=create cb_chiavi
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_prog_manutenzioni
this.Control[iCurrent+2]=this.cb_chiavi
this.Control[iCurrent+3]=this.cb_note_esterne
end on

on w_det_prog_manut.destroy
call super::destroy
destroy(this.dw_det_prog_manutenzioni)
destroy(this.cb_chiavi)
destroy(this.cb_note_esterne)
end on

type dw_det_prog_manutenzioni from uo_cs_xx_dw within w_det_prog_manut
integer x = 23
integer y = 20
integer width = 2903
integer height = 860
integer taborder = 10
string dataobject = "d_det_prog_manutenzioni"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_d_10, s_cs_xx.parametri.parametro_d_11)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   this.setitem(ll_i, "anno_registrazione", s_cs_xx.parametri.parametro_d_10)
   this.setitem(ll_i, "num_registrazione", s_cs_xx.parametri.parametro_d_11)	
next


end event

event pcd_new;call super::pcd_new;if i_extendmode then

	long ll_progressivo
	double ld_num_protocollo
	
	select max(progressivo) + 1
	  into :ll_progressivo
	  from det_prog_manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda
		and anno_registrazione = :s_cs_xx.parametri.parametro_d_10
		and num_registrazione = :s_cs_xx.parametri.parametro_d_11;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati da tabella DET_PROG_MANUTENZIONI")
		pcca.error = c_Fatal
		return
	end if
	
	if ll_progressivo = 0 or isnull(ll_progressivo) then
		ll_progressivo = 1
	end if
	


	// Assegnare un nuovo numero di protocollo
	
	setnull(ld_num_protocollo)
	
	select max(num_protocollo)
	into :ld_num_protocollo
	from tab_protocolli
	where cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore", "Impossibile Leggere la tabella protocolli " + sqlca.sqlerrtext )
		dw_det_prog_manutenzioni.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	if (sqlca.sqlcode <> 0 ) or isnull(ld_num_protocollo) then
		ld_num_protocollo = 0
	end if
	
	ld_num_protocollo = ld_num_protocollo + 1	
	
	insert into tab_protocolli
	(cod_azienda, num_protocollo)
	values (:s_cs_xx.cod_azienda, :ld_num_protocollo);

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Errore", "Impossibile Aggiornare la tabella protocolli" + sqlca.sqlerrtext)
		dw_det_prog_manutenzioni.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	this.setitem(this.getrow(), "num_protocollo", ld_num_protocollo)
	this.setitem(this.getrow(), "progressivo", ll_progressivo)
	this.setitem(this.getrow(), "flag_blocco", "N")
	
	cb_chiavi.enabled = false
	cb_note_esterne.enabled = false	
	
end if	
end event

event updateend;call super::updateend;long ll_i
double ld_num_protocollo
string ls_cod_famiglia_chiavi, ls_cod_chiave

for ll_i = 1 to this.deletedcount()
	ld_num_protocollo = getitemnumber(ll_i, "num_protocollo", delete!, true)

	delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
	where cod_azienda = :s_cs_xx.cod_azienda
	  and num_protocollo = :ld_num_protocollo;
	  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
		return
	end if	

	delete from tab_protocolli  //cancellazione tabella protocollo
	where cod_azienda = :s_cs_xx.cod_azienda
	  and num_protocollo = :ld_num_protocollo;
	  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
		return 1
	end if
	
	cb_chiavi.enabled = true
	cb_note_esterne.enabled = true
	
next

end event

event pcd_modify;call super::pcd_modify;cb_chiavi.enabled = false
cb_note_esterne.enabled = false	
end event

event pcd_save;call super::pcd_save;cb_chiavi.enabled = true
cb_note_esterne.enabled = true	
end event

event pcd_view;call super::pcd_view;cb_chiavi.enabled = true
cb_note_esterne.enabled = true	
end event

type cb_chiavi from commandbutton within w_det_prog_manut
integer x = 2171
integer y = 900
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiavi"
end type

event clicked;if dw_det_prog_manutenzioni.getrow() < 1 or &
	isnull(dw_det_prog_manutenzioni.getrow()) then
	g_mb.messagebox("Omnia", "Inserire almeno un dettaglio")
	return
end if	

window_open_parm(w_tab_chiavi_protocollo, -1, dw_det_prog_manutenzioni)
end event

type cb_note_esterne from commandbutton within w_det_prog_manut
integer x = 2560
integer y = 900
integer width = 366
integer height = 80
integer taborder = 3
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;long ll_i, ll_progressivo, ll_prog_mimetype
integer li_risposta
string ls_db, ls_doc
transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)
ll_i = dw_det_prog_manutenzioni.getrow()

ll_progressivo = dw_det_prog_manutenzioni.getitemnumber(ll_i, "progressivo")

select prog_mimetype
into :ll_prog_mimetype
from det_prog_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :s_cs_xx.parametri.parametro_d_10 and 
	num_registrazione = :s_cs_xx.parametri.parametro_d_11 and 
	progressivo = :ll_progressivo;
	
selectblob blob
into :lbl_blob
from det_prog_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :s_cs_xx.parametri.parametro_d_10 and 
	num_registrazione = :s_cs_xx.parametri.parametro_d_11 and 
	progressivo = :ll_progressivo;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update det_prog_manutenzioni
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :s_cs_xx.parametri.parametro_d_10 and 
			num_registrazione = :s_cs_xx.parametri.parametro_d_11 and 
			progressivo = :ll_progressivo;
	else
		updateblob det_prog_manutenzioni
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :s_cs_xx.parametri.parametro_d_10 and 
			num_registrazione = :s_cs_xx.parametri.parametro_d_11 and 
			progressivo = :ll_progressivo;
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
		
	update det_prog_manutenzioni
	set prog_mimetype = :ll_prog_mimetype
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :s_cs_xx.parametri.parametro_d_10 and 
		num_registrazione = :s_cs_xx.parametri.parametro_d_11 and 
		progressivo = :ll_progressivo;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if

// 11-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//if ls_db = "MSSQL" then
//	
//	li_risposta = f_crea_sqlcb(sqlcb)
//	
//	selectblob blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       det_prog_manutenzioni
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           anno_registrazione = :s_cs_xx.parametri.parametro_d_10 and 
//	           num_registrazione = :s_cs_xx.parametri.parametro_d_11 and 
//				  progressivo = :ll_progressivo
//	using      sqlcb;
//	
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if	
//	
//	destroy sqlcb;
//	
//else
//
//	selectblob blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       det_prog_manutenzioni
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           anno_registrazione = :s_cs_xx.parametri.parametro_d_10 and 
//	           num_registrazione = :s_cs_xx.parametri.parametro_d_11 and 
//				  progressivo = :ll_progressivo;
//	
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//
//end if
//
//window_open(w_ole, 0)
//
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob det_prog_manutenzioni
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_registrazione = :s_cs_xx.parametri.parametro_d_10 and 
//					  num_registrazione = :s_cs_xx.parametri.parametro_d_11 and 
//					  progressivo = :ll_progressivo
//		using      sqlcb;		
//		
//		destroy sqlcb;
//		
//	else
//		
//	   updateblob det_prog_manutenzioni
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  anno_registrazione = :s_cs_xx.parametri.parametro_d_10 and 
//					  num_registrazione = :s_cs_xx.parametri.parametro_d_11 and 
//					  progressivo = :ll_progressivo;
//
//	end if
//	
//// fine modifiche	
//	
//   commit;
//end if
end event

