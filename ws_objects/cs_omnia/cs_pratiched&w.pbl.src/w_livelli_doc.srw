﻿$PBExportHeader$w_livelli_doc.srw
$PBExportComments$Finestra Livelli Documenti
forward
global type w_livelli_doc from w_cs_xx_principale
end type
type tv_1 from treeview within w_livelli_doc
end type
type dw_tab_livelli_doc from uo_cs_xx_dw within w_livelli_doc
end type
type dw_folder from u_folder within w_livelli_doc
end type
type ws_record from structure within w_livelli_doc
end type
end forward

type ws_record from structure
	string		cod_livello
	string		des_livello
	long		handle_padre
	string		tipo_livello
	integer		livello
end type

global type w_livelli_doc from w_cs_xx_principale
integer width = 3264
integer height = 1088
string title = "Gestione Livelli Documenti"
event ue_close ( )
event ue_elimina ( )
event ue_modifica ( )
event ue_ricaricamenu ( )
event ue_inserisci ( )
event ue_inserisci_solo ( )
tv_1 tv_1
dw_tab_livelli_doc dw_tab_livelli_doc
dw_folder dw_folder
end type
global w_livelli_doc w_livelli_doc

type variables
boolean      ib_carica_tutto=FALSE, ib_retrieve=true, ib_fam = false, ib_new = false, ib_delete

long         il_handle, il_anno_registrazione, il_num_registrazione, il_livello_corrente,il_handle_modificato,il_handle_cancellato

string       is_tipo_manut, is_tipo_ordinamento, is_flag_eseguito, is_sql_filtro, is_area

string       is_cod_tipo_lista_dist, is_cod_lista_dist, is_cod_destinatario, is_tipo_retrieve, is_tipo_livello, is_cod_destinatario_mail

long         il_num_sequenza

treeviewitem tvi_campo

string       is_codice_rubrica


string       is_cod_livello_padre
end variables

forward prototypes
public subroutine wf_cancella_treeview ()
public subroutine wf_imposta_tv ()
public function integer wf_inserisci_tipi (long fl_handle)
public function integer wf_inserisci ()
public function integer wf_inserisci_testate (long fl_handle, string fs_cod_livello_padre)
public function integer wf_modifica ()
public function integer wf_elimina ()
public function integer wf_elimina_dettagli (string fs_cod_livello)
end prototypes

event ue_close();
close(this)
end event

event ue_elimina();wf_elimina()
end event

event ue_modifica();wf_modifica()
end event

event ue_ricaricamenu();wf_cancella_treeview()

wf_imposta_tv()
end event

event ue_inserisci();wf_inserisci()
end event

event ue_inserisci_solo();long         ll_progressivo, ll_handle, ll_ordinamento

il_handle = 0

iuo_dw_main = dw_tab_livelli_doc

dw_tab_livelli_doc.triggerevent("pcd_new")


end event

public subroutine wf_cancella_treeview ();long tvi_hdl = 0

do 
	tvi_hdl = tv_1.finditem(roottreeitem!,0)
	if tvi_hdl <> -1 then
		tv_1.deleteitem(tvi_hdl)
	end if
loop while tvi_hdl <> -1

return
end subroutine

public subroutine wf_imposta_tv ();string    ls_null, ls_cod_attrezzatura, ls_des_attrezzatura

long      ll_risposta, ll_1, ll_2, ll_3, ll_i, ll_tvi, ll_root, ll_anno_registrazione, ll_num_registrazione

ws_record lstr_record

setnull(ls_null)

tvi_campo.expanded = false

tvi_campo.selected = false

tvi_campo.children = false	

ll_i = 0

setpointer(HourGlass!)

tv_1.setredraw(false)

il_livello_corrente = 0

wf_inserisci_tipi(0)

tv_1.setredraw(true)

setpointer(arrow!)

return
end subroutine

public function integer wf_inserisci_tipi (long fl_handle);boolean   lb_dati = false

string    ls_cod_livello, ls_des_livello, ls_null

long      ll_risposta, ll_i, ll_livello, ll_count

ws_record lstr_record

setnull(ls_null)

ll_livello = il_livello_corrente + 1

DECLARE  cu_lista CURSOR FOR  
SELECT   cod_livello,   
         des_livello
FROM     tab_livelli_doc 
WHERE    cod_azienda = :s_cs_xx.cod_azienda AND  
		   cod_livello_padre is NULL
order by cod_livello;

open cu_lista;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_lista (wf_inserisci_tipi)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while 1 = 1
	
   fetch cu_lista into :ls_cod_livello,   
                       :ls_des_livello;
										  
   if (sqlca.sqlcode = 100) then exit
	
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_lista (wf_inserisci_tipi)~r~n" + sqlca.sqlerrtext)
		close cu_lista;
		return -1
	end if
	
	lstr_record.cod_livello = ls_cod_livello 
	lstr_record.des_livello = ls_des_livello
	lstr_record.tipo_livello = "L"
	lstr_record.livello = ll_livello
	lstr_record.handle_padre = fl_handle
	
	tvi_campo.data = lstr_record
	tvi_campo.label = ls_des_livello
	tvi_campo.pictureindex = 1
	tvi_campo.selectedpictureindex = 1
	tvi_campo.overlaypictureindex = 1
	
	select count(*)
	into   :ll_count
	from   tab_livelli_doc
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_livello_padre = :ls_cod_livello;
			 
	if ll_count > 0 and not isnull(ll_count) then
	
		tvi_campo.children = true
		
	else
	
		tvi_campo.children = false
				
	end if
	
	tvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento nel TREEVIEW")
		close cu_lista;
		return 0
	end if
	
	wf_inserisci_testate( ll_risposta, ls_cod_livello)			
	
loop

close cu_lista;
return 0
end function

public function integer wf_inserisci ();long         ll_progressivo, ll_handle, ll_ordinamento

iuo_dw_main = dw_tab_livelli_doc

dw_tab_livelli_doc.triggerevent("pcd_new")

return 0
end function

public function integer wf_inserisci_testate (long fl_handle, string fs_cod_livello_padre);boolean   lb_dati = false

string    ls_cod_livello, ls_des_livello, ls_null, ls_sql, ls_sintassi, ls_errore, ls_cod_livello_a[], ls_des_livello_a[]

long      ll_risposta, ll_i, ll_livello, ll_count, ll_ret

ws_record lstr_record

datastore lds_dati

setnull(ls_null)

ll_livello = il_livello_corrente + 1

ls_sql = " SELECT cod_livello, des_livello FROM tab_livelli_doc WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_livello_padre = '" + fs_cod_livello_padre + "' order by des_livello "

ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)

IF Len(ls_errore) > 0 THEN
   g_mb.messagebox("OMNIA", "1.Errore durante la costruzione del datastore: " + ls_errore)
   RETURN -1
END IF

lds_dati = create datastore

lds_dati.Create( ls_sintassi, ls_errore)

IF Len(ls_errore) > 0 THEN
   g_mb.messagebox("OMNIA", "2.Errore durante la costruzione del datastore: " + ls_errore)
   RETURN -1
END IF

lds_dati.settransobject( sqlca)
ll_ret = lds_dati.retrieve()

if ll_ret < 0 then
	g_mb.messagebox("OMNIA","Errore nella retrieve:~r~n" + sqlca.sqlerrtext)
	g_mb.messagebox("OMNIA","Sintassi:~r~n" + ls_sql)
	destroy lds_dati;
	return -1
end if

for ll_i = 1 to ll_ret
	ls_cod_livello_a[ll_i] = lds_dati.getitemstring( ll_i, "cod_livello")
	ls_des_livello_a[ll_i] = lds_dati.getitemstring( ll_i, "des_livello")
next

destroy lds_dati;

for ll_i = 1 to upperbound(ls_cod_livello_a)
	
	ls_cod_livello = ls_cod_livello_a[ll_i]
    ls_des_livello = ls_des_livello_a[ll_i]
										  
	if isnull(ls_des_livello) then ls_des_livello = ""

	lstr_record.cod_livello = ls_cod_livello 
	lstr_record.des_livello = ls_des_livello
	lstr_record.livello = ll_livello
	lstr_record.handle_padre = fl_handle
	
	tvi_campo.data = lstr_record
	tvi_campo.label = ls_des_livello
	
	tvi_campo.pictureindex = 2
	
	tvi_campo.selectedpictureindex = 2
	
	tvi_campo.overlaypictureindex = 2
	
	select count(*)
	into   :ll_count
	from   tab_livelli_doc
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_livello_padre = :ls_cod_livello;
			 
	if ll_count > 0 and not isnull(ll_count) then
	
		tvi_campo.children = true
		
	else
	
		tvi_campo.children = false
				
	end if
	
	tvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento livelli nel TREEVIEW")
		return 0
	end if
	
	wf_inserisci_testate( ll_risposta, ls_cod_livello)
	
next

return 0
end function

public function integer wf_modifica ();iuo_dw_main = dw_tab_livelli_doc

dw_tab_livelli_doc.triggerevent("pcd_modify")

return 0
end function

public function integer wf_elimina ();string       ls_cod_livello

treeviewitem ltv_item
		
ws_record    lstr_voce

long         ll_count, ll_ret

if isnull(il_handle) or il_handle < 1 then return 0

ll_ret = g_mb.messagebox( "OMNIA", "Continuare con la cancellazione del livello selezionato?", Exclamation!, YesNo!, 2)

if ll_ret <> 1 then return 0

tv_1.getitem(il_handle,ltv_item)
		
lstr_voce = ltv_item.data

ls_cod_livello = lstr_voce.cod_livello

select count(*)
into   :ll_count
from   tab_livelli_doc
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_livello_padre = :ls_cod_livello;

if sqlca.sqlcode = 0 and not isnull(ll_count) and ll_count > 0 then	

	ll_ret = g_mb.messagebox( "OMNIA", "Il livello selezionato ha dei sottolivelli.~r~nContinuare con la cancellazione dei sottolivelli?", Exclamation!, YesNo!, 2)

	if ll_ret <> 1 then
		
		g_mb.messagebox( "OMNIA", "Operazione annullata!", stopsign!)
		return 0
		
	end if
	
	ll_ret = wf_elimina_dettagli(ls_cod_livello)
	
	if ll_ret <> 1 then
		
		rollback;
		return -1
		
	end if	
	
end if

delete from tab_livelli_doc
where       cod_azienda = :s_cs_xx.cod_azienda and
            cod_livello = :ls_cod_livello;
				
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la cancellazione del livello:" + sqlca.sqlerrtext, stopsign!)
	rollback;
	g_mb.messagebox( "OMNIA", "Operazione Annullata!", stopsign!)
	return 0
end if

commit;

triggerevent("ue_ricaricamenu")

return 0
end function

public function integer wf_elimina_dettagli (string fs_cod_livello);string       ls_cod_livello

treeviewitem ltv_item
		
ws_record    lstr_voce

long         ll_count, ll_ret

select count(*)
into   :ll_count
from   tab_livelli_doc
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_livello_padre = :fs_cod_livello;

if sqlca.sqlcode = 0 and not isnull(ll_count) and ll_count > 0 then	

	ll_ret = wf_elimina_dettagli(fs_cod_livello)	
	
	if ll_ret < 0 then return -1
	
end if

delete from tab_livelli_doc
where       cod_azienda = :s_cs_xx.cod_azienda and
            cod_livello = :fs_cod_livello;
				
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la cancellazione del livello:" + sqlca.sqlerrtext, stopsign!)
	rollback;
	g_mb.messagebox( "OMNIA", "Operazione Annullata!", stopsign!)
	return -1
end if

return 0
end function

on w_livelli_doc.create
int iCurrent
call super::create
this.tv_1=create tv_1
this.dw_tab_livelli_doc=create dw_tab_livelli_doc
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tv_1
this.Control[iCurrent+2]=this.dw_tab_livelli_doc
this.Control[iCurrent+3]=this.dw_folder
end on

on w_livelli_doc.destroy
call super::destroy
destroy(this.tv_1)
destroy(this.dw_tab_livelli_doc)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

dw_tab_livelli_doc.set_dw_key("cod_azienda")

dw_tab_livelli_doc.set_dw_options( sqlca, pcca.null_object, c_noretrieveonopen , c_default)

iuo_dw_main = dw_tab_livelli_doc

lw_oggetti[1] = dw_tab_livelli_doc

dw_folder.fu_assigntab(1, "Livelli Documento", lw_oggetti[])

dw_folder.fu_foldercreate(1, 1)

dw_folder.fu_selecttab(1)

tv_1.deletepictures()

tv_1.PictureHeight = 16

tv_1.PictureWidth = 16

tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_categorie.bmp")

tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_reparti.bmp")

tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_area.bmp")

tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_attrezzature.bmp")

wf_cancella_treeview()

wf_imposta_tv()

tv_1.setfocus()

ib_retrieve = true


	

end event

type tv_1 from treeview within w_livelli_doc
integer x = 18
integer y = 20
integer width = 1157
integer height = 940
integer taborder = 10
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
boolean linesatroot = true
boolean hideselection = false
string picturename[] = {"Custom093!","C:\CS_70\framework\RISORSE\Foglio2_Dim_Orig.bmp","Start!","Picture!","Custom096!","Custom024!"}
long picturemaskcolor = 553648127
string statepicturename[] = {"SetVariable!","DeclareVariable!"}
long statepicturemaskcolor = 553648127
end type

event selectionchanged;treeviewitem ltv_item

ws_record lws_record

if isnull(newhandle) or newhandle <= 0 then return 0

il_handle = newhandle

getitem( newhandle, ltv_item)

lws_record = ltv_item.data

dw_folder.fu_selecttab(1)

is_cod_livello_padre = lws_record.cod_livello
iuo_dw_main = dw_tab_livelli_doc
dw_tab_livelli_doc.Change_DW_Current( )
parent.triggerevent("pc_retrieve")			

end event

event rightclicked;selectitem(handle)
il_handle = handle

m_menu_livelli_doc menu
menu = create m_menu_livelli_doc

s_cs_xx.parametri.parametro_w_cs_xx_1 = parent
	
if not isnull(il_handle) and il_handle > 0 then
		
	menu.m_inserisci.visible = true
	menu.m_modifica.visible = true	
	menu.m_elimina.visible = true
	menu.m_inserisci_solo.visible = false
	menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())	
	
else
	
	menu.m_inserisci.visible = false
	menu.m_modifica.visible = false
	menu.m_elimina.visible = false
	menu.m_inserisci_solo.visible = true
	menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())		

end if
end event

type dw_tab_livelli_doc from uo_cs_xx_dw within w_livelli_doc
event ue_lista ( )
integer x = 1211
integer y = 140
integer width = 1966
integer height = 716
integer taborder = 60
string dataobject = "d_tab_livelli_doc_padri"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

ib_new = false

ib_delete = false

l_Error = Retrieve( s_cs_xx.cod_azienda, is_cod_livello_padre)

if l_Error < 0 then
		PCCA.Error = c_Fatal
end if	
	

end event

event pcd_new;call super::pcd_new;string ls_cod_livello_padre

ib_new = true
ib_delete = false

if not isnull(il_handle) and il_handle > 0 then
	
	treeviewitem ltv_item
	
	ws_record lws_record
	tv_1.getitem( il_handle, ltv_item)
	lws_record = ltv_item.data
	
	ls_cod_livello_padre = lws_record.cod_livello
	setitem( getrow(), "cod_livello_padre", ls_cod_livello_padre)
	
end if

//setitem( getrow(), "flag_blocco", "N")
end event

event pcd_modify;call super::pcd_modify;ib_new = false

ib_delete = false
end event

event pcd_view;call super::pcd_view;treeviewitem   ltv_item

ws_record      lstr_voce

long           ll_handle

if ib_new then

	if this.getrow() < 1 then return -1
	
	is_cod_livello_padre = getitemstring( getrow(), "cod_livello")
	
	wf_cancella_treeview()

	wf_imposta_tv()

	tv_1.setfocus()

	ib_retrieve = true
	
	ib_new = false
	
end if

if ib_delete then
	
	tv_1.deleteitem(il_handle)
	
	ib_delete = false
	
end if



return 0

end event

event pcd_delete;call super::pcd_delete;ib_delete = true
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

type dw_folder from u_folder within w_livelli_doc
integer x = 1189
integer y = 20
integer width = 2011
integer height = 940
integer taborder = 30
end type

