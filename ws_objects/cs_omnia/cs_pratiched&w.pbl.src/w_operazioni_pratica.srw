﻿$PBExportHeader$w_operazioni_pratica.srw
$PBExportComments$finestra response per verifica operazioni testata pratiche
forward
global type w_operazioni_pratica from w_cs_xx_risposta
end type
type gb_1 from groupbox within w_operazioni_pratica
end type
type rb_verifica from radiobutton within w_operazioni_pratica
end type
type rb_approva from radiobutton within w_operazioni_pratica
end type
type rb_valida from radiobutton within w_operazioni_pratica
end type
type rb_nuova_rev from radiobutton within w_operazioni_pratica
end type
type dw_nuova_rev from uo_cs_xx_dw within w_operazioni_pratica
end type
type cb_ok from commandbutton within w_operazioni_pratica
end type
type cb_esci from commandbutton within w_operazioni_pratica
end type
end forward

global type w_operazioni_pratica from w_cs_xx_risposta
integer width = 1499
integer height = 576
string title = "Operazioni su Pratica"
gb_1 gb_1
rb_verifica rb_verifica
rb_approva rb_approva
rb_valida rb_valida
rb_nuova_rev rb_nuova_rev
dw_nuova_rev dw_nuova_rev
cb_ok cb_ok
cb_esci cb_esci
end type
global w_operazioni_pratica w_operazioni_pratica

type variables
string is_flag_stato_doc, is_flag_ultima_revisione, &
         is_num_pratica, is_tes_det
long il_num_edizione_old, il_num_versione_old, &
        il_anno_pratica, il_num_edizione_tes, &
        il_num_versione_tes

end variables

on w_operazioni_pratica.create
int iCurrent
call super::create
this.gb_1=create gb_1
this.rb_verifica=create rb_verifica
this.rb_approva=create rb_approva
this.rb_valida=create rb_valida
this.rb_nuova_rev=create rb_nuova_rev
this.dw_nuova_rev=create dw_nuova_rev
this.cb_ok=create cb_ok
this.cb_esci=create cb_esci
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.gb_1
this.Control[iCurrent+2]=this.rb_verifica
this.Control[iCurrent+3]=this.rb_approva
this.Control[iCurrent+4]=this.rb_valida
this.Control[iCurrent+5]=this.rb_nuova_rev
this.Control[iCurrent+6]=this.dw_nuova_rev
this.Control[iCurrent+7]=this.cb_ok
this.Control[iCurrent+8]=this.cb_esci
end on

on w_operazioni_pratica.destroy
call super::destroy
destroy(this.gb_1)
destroy(this.rb_verifica)
destroy(this.rb_approva)
destroy(this.rb_valida)
destroy(this.rb_nuova_rev)
destroy(this.dw_nuova_rev)
destroy(this.cb_ok)
destroy(this.cb_esci)
end on

event open;call super::open;
dw_nuova_rev.hide()
if is_flag_stato_doc = "V" then	//Verificato: posso approvare
	rb_approva.checked = true
	rb_approva.enabled = true
	rb_valida.checked = false
	rb_valida.enabled = false
	rb_nuova_rev.checked = false
	rb_nuova_rev.enabled = false
	rb_verifica.checked = false
	rb_verifica.enabled = false
end if

if is_flag_stato_doc = "A" then	//Approvato: posso validare oppure fare nuova revisione
	rb_approva.checked = false
	rb_approva.enabled = false
	rb_valida.checked = true
	rb_valida.enabled = true
	rb_nuova_rev.checked = false
	rb_nuova_rev.enabled = true
	rb_verifica.checked = false
	rb_verifica.enabled = false
end if

if is_flag_stato_doc = "L" then	//Validato: posso fare nuova revisione
	rb_approva.checked = false
	rb_approva.enabled = false
	rb_valida.checked = false
	rb_valida.enabled = false
	rb_nuova_rev.checked = true
	rb_nuova_rev.enabled = true
	rb_verifica.checked = false
	rb_verifica.enabled = false
	dw_nuova_rev.show()
end if

if is_flag_stato_doc = "N" then	//Nuova versione: posso solo verificare
	rb_approva.checked = false
	rb_approva.enabled = false
	rb_valida.checked = false
	rb_valida.enabled = false
	rb_nuova_rev.checked = false
	rb_nuova_rev.enabled = false
	rb_verifica.checked = true
	rb_verifica.enabled = true
end if


end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_nuova_rev.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

is_flag_stato_doc = s_cs_xx.parametri.parametro_s_1
is_flag_ultima_revisione = s_cs_xx.parametri.parametro_s_2
is_num_pratica = s_cs_xx.parametri.parametro_s_3

il_num_edizione_old = s_cs_xx.parametri.parametro_ul_1 
il_num_versione_old = s_cs_xx.parametri.parametro_ul_2 

il_anno_pratica = s_cs_xx.parametri.parametro_ul_3 

// is_tes_det = s_cs_xx.parametri.parametro_s_4

end event

type gb_1 from groupbox within w_operazioni_pratica
integer x = 23
integer y = 20
integer width = 594
integer height = 420
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Operazioni"
end type

type rb_verifica from radiobutton within w_operazioni_pratica
integer x = 69
integer y = 100
integer width = 343
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Verifica"
boolean checked = true
end type

event clicked;dw_nuova_rev.hide()


end event

type rb_approva from radiobutton within w_operazioni_pratica
integer x = 69
integer y = 180
integer width = 297
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Approva"
end type

event clicked;dw_nuova_rev.hide()

end event

type rb_valida from radiobutton within w_operazioni_pratica
integer x = 69
integer y = 260
integer width = 274
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Valida"
end type

event clicked;dw_nuova_rev.hide()

end event

type rb_nuova_rev from radiobutton within w_operazioni_pratica
integer x = 69
integer y = 340
integer width = 507
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Nuova Revisione"
end type

event clicked;dw_nuova_rev.show()
end event

type dw_nuova_rev from uo_cs_xx_dw within w_operazioni_pratica
integer x = 663
integer y = 20
integer width = 777
integer height = 180
integer taborder = 30
string dataobject = "d_nuova_rev"
end type

event pcd_new;call super::pcd_new;// messagebox("debug", string(il_num_edizione_old) + " - vers. " + string(il_num_versione_old))
this.setitem(1, "num_edizione", il_num_edizione_old)
this.setitem(1, "num_versione", il_num_versione_old)

end event

type cb_ok from commandbutton within w_operazioni_pratica
integer x = 1074
integer y = 360
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;string ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione, ls_flag_modifica  
long ll_num_edizione, ll_num_versione, ll_controllo
// leggo i privilegi in mansionari

dw_nuova_rev.accepttext()

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT flag_approvazione,   
//		flag_autorizzazione,   
//		flag_validazione,   
//		flag_modifica  
// INTO :ls_flag_approvazione,   
//		:ls_flag_autorizzazione,   
//		:ls_flag_validazione,   
//		:ls_flag_modifica  
// FROM mansionari  
// where cod_azienda = :s_cs_xx.cod_azienda
//   and cod_utente = :s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'
ls_flag_modifica = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then ls_flag_modifica = 'S'

setnull(s_cs_xx.parametri.parametro_s_1)		// se ritornato null, non esegue operazione
setnull(s_cs_xx.parametri.parametro_ul_1)
setnull(s_cs_xx.parametri.parametro_ul_2)

if rb_verifica.checked then	//Verifico: 
	if ls_flag_autorizzazione = "S" then
		// controllo che non ci siano dettagli (ultime rev.) da Verificare
		select count(num_edizione)
		into :ll_controllo
		from det_pratiche
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_pratica = :il_anno_pratica and 
					  num_pratica = :is_num_pratica and
					  num_versione = : il_num_versione_old and
					  num_edizione = :il_num_edizione_old and 
					  flag_stato_doc = 'N' and
					  flag_ultima_revisione = 'S';
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore", "Errore 1 lettura det_pratiche " + sqlca.sqlerrtext)
			return
		end if
		if (not isnull(ll_controllo)) and ll_controllo > 0 then
			g_mb.messagebox("Errore", "Alcuni documenti dettaglio sono ancora da Verificare !")
			return
		end if
		
		s_cs_xx.parametri.parametro_s_1 = "V"
	end if
end if

if rb_approva.checked then	//Approvo
	if ls_flag_approvazione = "S" then
		// controllo che non ci siano dettagli (ultime rev.) da Approvare
		select count(num_edizione)
		into :ll_controllo
		from det_pratiche
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_pratica = :il_anno_pratica and 
					  num_pratica = :is_num_pratica and
					  num_versione = : il_num_versione_old and
					  num_edizione = :il_num_edizione_old and 
					  (flag_stato_doc = 'N' or
					  flag_stato_doc = 'V') and
					  flag_ultima_revisione = 'S';
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore", "Errore 2 lettura det_pratiche " + sqlca.sqlerrtext)
			return
		end if
		if (not isnull(ll_controllo)) and ll_controllo > 0 then
			g_mb.messagebox("Errore", "Alcuni documenti dettaglio sono ancora da Approvare !")
			return
		end if
		s_cs_xx.parametri.parametro_s_1 = "A"
	end if
end if

if rb_valida.checked then	//valido
	if ls_flag_validazione = "S" then
		// controllo che non ci siano dettagli (ultime rev.) da vaLidare
		select count(num_edizione)
		into :ll_controllo
		from det_pratiche
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_pratica = :il_anno_pratica and 
					  num_pratica = :is_num_pratica and
					  num_versione = : il_num_versione_old and
					  num_edizione = :il_num_edizione_old and 
					  (flag_stato_doc = 'N' or
					  flag_stato_doc = 'V' or
					  flag_stato_doc = 'A') and
					  flag_ultima_revisione = 'S';
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore", "Errore 3 lettura det_pratiche " + sqlca.sqlerrtext)
			return
		end if
		if (not isnull(ll_controllo)) and ll_controllo > 0 then
			g_mb.messagebox("Errore", "Alcuni documenti dettaglio sono ancora da Validare !")
			return
		end if
		s_cs_xx.parametri.parametro_s_1 = "L"
	end if
end if

if rb_nuova_rev.checked then	//Nuova revisione
	if ls_flag_modifica = "S" then
		ll_num_edizione = dw_nuova_rev.getitemnumber(1, "num_edizione")
		ll_num_versione = dw_nuova_rev.getitemnumber(1, "num_versione")
		if isnull(ll_num_edizione) or isnull(ll_num_versione) then
			g_mb.messagebox("Attenzione", "Impostare il numero versione ed edizione")
			return
		end if
		// controllo che non ci siano dettagli (ultime rev.) da Approvare
		select count(num_edizione)
		into :ll_controllo
		from det_pratiche
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_pratica = :il_anno_pratica and 
					  num_pratica = :is_num_pratica and
					  num_versione = : il_num_versione_old and
					  num_edizione = :il_num_edizione_old and 
					  (flag_stato_doc = 'N' or
					  flag_stato_doc = 'V') and
					  flag_ultima_revisione = 'S';
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore", "Errore 4 lettura det_pratiche " + sqlca.sqlerrtext)
			return
		end if
		if (not isnull(ll_controllo)) and ll_controllo > 0 then
			g_mb.messagebox("Errore", "Alcuni documenti dettaglio sono ancora da Approvare !")
			return
		end if
		
		select count(num_edizione)
		into :ll_controllo
		from tes_pratiche
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_pratica = :il_anno_pratica and 
					  num_pratica = :is_num_pratica and
					  num_versione = :ll_num_versione and
					  num_edizione = :ll_num_edizione ;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore", "Errore lettura tes_pratiche " + sqlca.sqlerrtext)
			return
		end if
		if (not isnull(ll_controllo)) and ll_controllo > 0 then
			g_mb.messagebox("Errore", "Versione / Edizione già presente !")
			return
		end if
		s_cs_xx.parametri.parametro_s_1 = "N"
		s_cs_xx.parametri.parametro_ul_1 = ll_num_edizione
		s_cs_xx.parametri.parametro_ul_2 = ll_num_versione
	end if
end if

close(parent)


end event

type cb_esci from commandbutton within w_operazioni_pratica
integer x = 663
integer y = 360
integer width = 366
integer height = 80
integer taborder = 4
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;setnull(s_cs_xx.parametri.parametro_s_1)

close(parent)


end event

