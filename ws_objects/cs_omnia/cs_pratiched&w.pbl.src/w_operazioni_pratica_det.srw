﻿$PBExportHeader$w_operazioni_pratica_det.srw
$PBExportComments$finestra response per verifica operazioni dettagli pratiche
forward
global type w_operazioni_pratica_det from w_cs_xx_risposta
end type
type gb_1 from groupbox within w_operazioni_pratica_det
end type
type rb_verifica from radiobutton within w_operazioni_pratica_det
end type
type rb_approva from radiobutton within w_operazioni_pratica_det
end type
type rb_valida from radiobutton within w_operazioni_pratica_det
end type
type rb_nuova_rev from radiobutton within w_operazioni_pratica_det
end type
type dw_nuova_rev from uo_cs_xx_dw within w_operazioni_pratica_det
end type
type cb_ok from commandbutton within w_operazioni_pratica_det
end type
type cb_esci from commandbutton within w_operazioni_pratica_det
end type
end forward

global type w_operazioni_pratica_det from w_cs_xx_risposta
int Width=1559
int Height=577
boolean TitleBar=true
string Title="Operazioni su Dettaglio Pratica"
gb_1 gb_1
rb_verifica rb_verifica
rb_approva rb_approva
rb_valida rb_valida
rb_nuova_rev rb_nuova_rev
dw_nuova_rev dw_nuova_rev
cb_ok cb_ok
cb_esci cb_esci
end type
global w_operazioni_pratica_det w_operazioni_pratica_det

type variables
string is_flag_stato_doc, is_flag_ultima_revisione, &
         is_num_pratica, is_tes_det
long il_num_edizione_old, il_num_versione_old, &
        il_anno_pratica, il_num_edizione_tes, &
        il_num_versione_tes

end variables

on w_operazioni_pratica_det.create
int iCurrent
call w_cs_xx_risposta::create
this.gb_1=create gb_1
this.rb_verifica=create rb_verifica
this.rb_approva=create rb_approva
this.rb_valida=create rb_valida
this.rb_nuova_rev=create rb_nuova_rev
this.dw_nuova_rev=create dw_nuova_rev
this.cb_ok=create cb_ok
this.cb_esci=create cb_esci
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=gb_1
this.Control[iCurrent+2]=rb_verifica
this.Control[iCurrent+3]=rb_approva
this.Control[iCurrent+4]=rb_valida
this.Control[iCurrent+5]=rb_nuova_rev
this.Control[iCurrent+6]=dw_nuova_rev
this.Control[iCurrent+7]=cb_ok
this.Control[iCurrent+8]=cb_esci
end on

on w_operazioni_pratica_det.destroy
call w_cs_xx_risposta::destroy
destroy(this.gb_1)
destroy(this.rb_verifica)
destroy(this.rb_approva)
destroy(this.rb_valida)
destroy(this.rb_nuova_rev)
destroy(this.dw_nuova_rev)
destroy(this.cb_ok)
destroy(this.cb_esci)
end on

event open;call super::open;
dw_nuova_rev.hide()
if is_flag_stato_doc = "V" then	//Verificato: posso approvare
	rb_approva.checked = true
	rb_approva.enabled = true
	rb_valida.checked = false
	rb_valida.enabled = false
	rb_nuova_rev.checked = false
	rb_nuova_rev.enabled = false
	rb_verifica.checked = false
	rb_verifica.enabled = false
end if

if is_flag_stato_doc = "A" then	//Approvato: posso validare oppure fare nuova revisione
	rb_approva.checked = false
	rb_approva.enabled = false
	rb_valida.checked = true
	rb_valida.enabled = true
	rb_nuova_rev.checked = false
	rb_nuova_rev.enabled = true
	rb_verifica.checked = false
	rb_verifica.enabled = false
end if

if is_flag_stato_doc = "L" then	//Validato: posso fare nuova revisione
	rb_approva.checked = false
	rb_approva.enabled = false
	rb_valida.checked = false
	rb_valida.enabled = false
	rb_nuova_rev.checked = true
	rb_nuova_rev.enabled = true
	rb_verifica.checked = false
	rb_verifica.enabled = false
	dw_nuova_rev.show()
end if

if is_flag_stato_doc = "N" then	//Nuova versione: posso solo verificare
	rb_approva.checked = false
	rb_approva.enabled = false
	rb_valida.checked = false
	rb_valida.enabled = false
	rb_nuova_rev.checked = false
	rb_nuova_rev.enabled = false
	rb_verifica.checked = true
	rb_verifica.enabled = true
end if


end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_nuova_rev.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

is_flag_stato_doc = s_cs_xx.parametri.parametro_s_1
is_flag_ultima_revisione = s_cs_xx.parametri.parametro_s_2

il_num_edizione_old = s_cs_xx.parametri.parametro_ul_1 
il_num_versione_old = s_cs_xx.parametri.parametro_ul_2 

il_anno_pratica = s_cs_xx.parametri.parametro_ul_3 
is_num_pratica = s_cs_xx.parametri.parametro_s_3
il_num_versione_tes = s_cs_xx.parametri.parametro_d_1
il_num_edizione_tes = s_cs_xx.parametri.parametro_d_2


end event

type gb_1 from groupbox within w_operazioni_pratica_det
int X=23
int Y=21
int Width=595
int Height=421
int TabOrder=20
string Text="Operazioni"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_verifica from radiobutton within w_operazioni_pratica_det
int X=69
int Y=101
int Width=343
int Height=81
boolean BringToTop=true
string Text="Verifica"
boolean Checked=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;dw_nuova_rev.hide()


end event

type rb_approva from radiobutton within w_operazioni_pratica_det
int X=69
int Y=181
int Width=298
int Height=81
boolean BringToTop=true
string Text="Approva"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;dw_nuova_rev.hide()

end event

type rb_valida from radiobutton within w_operazioni_pratica_det
int X=69
int Y=261
int Width=275
int Height=81
boolean BringToTop=true
string Text="Valida"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;dw_nuova_rev.hide()

end event

type rb_nuova_rev from radiobutton within w_operazioni_pratica_det
int X=69
int Y=341
int Width=508
int Height=81
boolean BringToTop=true
string Text="Nuova Revisione"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;dw_nuova_rev.show()
end event

type dw_nuova_rev from uo_cs_xx_dw within w_operazioni_pratica_det
int X=663
int Y=21
int Width=846
int Height=181
int TabOrder=30
string DataObject="d_nuova_rev_det"
end type

event pcd_new;call super::pcd_new;// messagebox("debug", string(il_num_edizione_old) + " - vers. " + string(il_num_versione_old))
this.setitem(1, "num_edizione", il_num_edizione_old)
this.setitem(1, "num_versione", il_num_versione_old)

end event

type cb_ok from commandbutton within w_operazioni_pratica_det
int X=1143
int Y=361
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="OK"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione, ls_flag_modifica  
long ll_num_edizione, ll_num_versione, ll_controllo
// leggo i privilegi in mansionari

dw_nuova_rev.accepttext()

SELECT flag_approvazione,   
		flag_autorizzazione,   
		flag_validazione,   
		flag_modifica  
 INTO :ls_flag_approvazione,   
		:ls_flag_autorizzazione,   
		:ls_flag_validazione,   
		:ls_flag_modifica  
 FROM mansionari  
 where cod_azienda = :s_cs_xx.cod_azienda
   and cod_utente = :s_cs_xx.cod_utente;

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_ul_1)
setnull(s_cs_xx.parametri.parametro_ul_2)

if rb_verifica.checked then	//Verifico: 
	if ls_flag_autorizzazione = "S" then
		s_cs_xx.parametri.parametro_s_1 = "V"
	end if
end if

if rb_approva.checked then	//Approvo
	if ls_flag_approvazione = "S" then
		s_cs_xx.parametri.parametro_s_1 = "A"
	end if
end if

if rb_valida.checked then	//valido
	if ls_flag_validazione = "S" then
		s_cs_xx.parametri.parametro_s_1 = "L"
	end if
end if

if rb_nuova_rev.checked then	//Nuova revisione
	if ls_flag_modifica = "S" then
		ll_num_edizione = dw_nuova_rev.getitemnumber(1, "num_edizione")
		ll_num_versione = dw_nuova_rev.getitemnumber(1, "num_versione")
		if isnull(ll_num_edizione) or isnull(ll_num_versione) then
			g_mb.messagebox("Attenzione", "Impostare il numero versione ed edizione")
			return
		end if
		
		setnull(ll_controllo)
		select count(num_edizione)
		into :ll_controllo
		from det_pratiche
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_pratica = :il_anno_pratica and 
					  num_pratica = :is_num_pratica and
					  num_versione = :ll_num_versione and
					  num_edizione = :ll_num_edizione and
					  num_versione_det = :ll_num_versione and
					  num_edizione_det = :ll_num_edizione ;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore", "Errore lettura tes_pratiche " + sqlca.sqlerrtext)
			return
		end if
		if (not isnull(ll_controllo)) and ll_controllo > 0 then
			g_mb.messagebox("Errore", "Versione_det / Edizione_det già presente !")
			return
		end if
		s_cs_xx.parametri.parametro_s_1 = "N"
		s_cs_xx.parametri.parametro_ul_1 = ll_num_edizione
		s_cs_xx.parametri.parametro_ul_2 = ll_num_versione
	end if
end if

close(parent)


end event

type cb_esci from commandbutton within w_operazioni_pratica_det
int X=732
int Y=361
int Width=366
int Height=81
int TabOrder=4
boolean BringToTop=true
string Text="Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;setnull(s_cs_xx.parametri.parametro_s_1)

close(parent)


end event

