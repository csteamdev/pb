﻿$PBExportHeader$w_report_pratiche.srw
$PBExportComments$finestra report Pratiche
forward
global type w_report_pratiche from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_pratiche
end type
type cb_report from commandbutton within w_report_pratiche
end type
type cb_selezione from commandbutton within w_report_pratiche
end type
type dw_selezione from uo_cs_xx_dw within w_report_pratiche
end type
type dw_report from uo_cs_xx_dw within w_report_pratiche
end type
end forward

global type w_report_pratiche from w_cs_xx_principale
integer width = 3552
integer height = 1840
string title = "Report Pratiche"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_pratiche w_report_pratiche

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2030
this.height = 820

end event

on w_report_pratiche.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_pratiche.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "cod_tipo_pratica", &
                 sqlca, &
                 "tab_tipi_pratiche", &
                 "cod_tipo_pratica", &
                 "des_tipo_pratica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_po_loaddddw_dw(dw_report, &
                 "cod_tipo_pratica", &
                 sqlca, &
                 "tab_tipi_pratiche", &
                 "cod_tipo_pratica", &
                 "des_tipo_pratica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type cb_annulla from commandbutton within w_report_pratiche
integer x = 1211
integer y = 620
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_pratiche
integer x = 1600
integer y = 620
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_tipo_pratica, ls_num_pratica_da, ls_num_pratica_a, ls_des_pratica, &
		 ls_flag_stato_doc, ls_flag_ultima_revisione, ls_sql, ls_det_num_pratica, &
		 ls_det_des_blob, ls_det_flag_stato_doc, ls_det_flag_ultima_revisione, ls_tes_des_pratica, &
		 ls_tes_cod_tipo_pratica, ls_tes_flag_stato_doc, ls_tes_flag_ultima_revisione , ls_selezione
long ll_anno_pratica_da, ll_anno_pratica_a, ll_num_versione_da, ll_num_versione_a, &
		ll_num_edizione_da, ll_num_edizione_a, ll_det_num_versione_det, ll_det_num_edizione_det, ll_det_progressivo, &
		ll_det_anno_pratica, ll_det_num_versione, ll_det_num_edizione, ll_i
		
double ld_det_num_protocollo
		
		
		
dw_report.reset()
dw_selezione.accepttext()

ls_cod_tipo_pratica = dw_selezione.getitemstring(1, "cod_tipo_pratica")
ls_num_pratica_da = dw_selezione.getitemstring(1, "num_pratica_da")
ls_num_pratica_a = dw_selezione.getitemstring(1, "num_pratica_a")
ls_des_pratica = dw_selezione.getitemstring(1, "des_pratica")
ls_flag_stato_doc = dw_selezione.getitemstring(1, "flag_stato_doc")
ls_flag_ultima_revisione = dw_selezione.getitemstring(1, "flag_ultima_revisione")

ll_anno_pratica_da = dw_selezione.getitemnumber(1, "anno_pratica_da")
ll_anno_pratica_a = dw_selezione.getitemnumber(1, "anno_pratica_a")
ll_num_versione_da = dw_selezione.getitemnumber(1, "num_versione_da")
ll_num_versione_a = dw_selezione.getitemnumber(1, "num_versione_a")
ll_num_edizione_da = dw_selezione.getitemnumber(1, "num_edizione_da")
ll_num_edizione_a = dw_selezione.getitemnumber(1, "num_edizione_a")

if isnull(ls_des_pratica) then ls_des_pratica = ""
if ls_des_pratica <> "" then
	ls_des_pratica = "%" + ls_des_pratica + "%"
end if

ls_sql = "SELECT det_pratiche.anno_pratica, " + &
"		det_pratiche.num_pratica, " + &
"		det_pratiche.num_versione, " + &
"		det_pratiche.num_edizione, " + &
"		det_pratiche.num_versione_det, " + &
"		det_pratiche.num_edizione_det, " + &
"		det_pratiche.progressivo, " + &
"		det_pratiche.des_blob, " + &
"		det_pratiche.num_protocollo, " + &
"		det_pratiche.flag_stato_doc, " + &
"		det_pratiche.flag_ultima_revisione, " + &
"		tes_pratiche.des_pratica, " + &
"		tes_pratiche.cod_tipo_pratica, " + &
"		tes_pratiche.flag_stato_doc, " + &
"		tes_pratiche.flag_ultima_revisione  " + &
" FROM det_pratiche, " + &
"		tes_pratiche  " + &
"WHERE ( tes_pratiche.cod_azienda = det_pratiche.cod_azienda ) and  " + &
"		( tes_pratiche.anno_pratica = det_pratiche.anno_pratica ) and  " + &
"		( tes_pratiche.num_pratica = det_pratiche.num_pratica ) and  " + &
"		( tes_pratiche.num_versione = det_pratiche.num_versione ) and  " + &
"		( tes_pratiche.num_edizione = det_pratiche.num_edizione ) and   " + &
"	   tes_pratiche.cod_azienda = '" + s_cs_xx.cod_azienda + "'  and " + &
"		tes_pratiche.anno_pratica >= " + string(ll_anno_pratica_da)  + "  and  " + &
"		tes_pratiche.anno_pratica <= " + string(ll_anno_pratica_a) + "   and " + &
"		det_pratiche.flag_ultima_revisione = 'S'  " 

if not isnull(ls_des_pratica) and ls_des_pratica <> "" then
	ls_sql = ls_sql + " and ( tes_pratiche.des_pratica like '" + ls_des_pratica + "' ) "
	
end if

ls_selezione = ""

if not isnull(ls_cod_tipo_pratica) and ls_cod_tipo_pratica <> "" then
	ls_sql = ls_sql + " and tes_pratiche.cod_tipo_pratica = '" + ls_cod_tipo_pratica + "' "
	ls_selezione = ls_selezione + "Tipo pratica: " + ls_cod_tipo_pratica + ";"
end if
if not isnull(ls_flag_stato_doc) and ls_flag_stato_doc <> "" then
	ls_sql = ls_sql + " and tes_pratiche.flag_stato_doc = '" + ls_flag_stato_doc + "' "
	ls_selezione = ls_selezione + " Stato pratica: " + ls_flag_stato_doc + ";"
end if
if not isnull(ls_flag_ultima_revisione) and ls_flag_ultima_revisione <> "" then	// third state = %
	ls_sql = ls_sql + " and tes_pratiche.flag_ultima_revisione like '" + ls_flag_ultima_revisione + "' "
	ls_selezione = ls_selezione + " Ult. Rev.: " + ls_flag_ultima_revisione + ";"
end if
if not isnull(ls_num_pratica_da) and ls_num_pratica_da <> "" then
	ls_sql = ls_sql + " and tes_pratiche.num_pratica >= '" + ls_num_pratica_da + "' "
	ls_selezione = ls_selezione + " Num. Prat. da: " + ls_num_pratica_da + ";"
end if
if not isnull(ls_num_pratica_a) and ls_num_pratica_a <> "" then
	ls_sql = ls_sql + " and tes_pratiche.num_pratica <= '" + ls_num_pratica_a + "' "
	ls_selezione = ls_selezione + " Num. Prat. a: " + ls_num_pratica_a + ";"
end if
if not isnull(ll_num_versione_da) then
	ls_sql = ls_sql + " and tes_pratiche.num_versione >= " + string(ll_num_versione_da) + " "
	ls_selezione = ls_selezione + " Vers. Prat. da: " + string(ll_num_versione_da) + ";"
end if
if not isnull(ll_num_versione_a) then
	ls_sql = ls_sql + " and tes_pratiche.num_versione <= " + string(ll_num_versione_a) + " "
	ls_selezione = ls_selezione + " Vers. Prat. a: " + string(ll_num_versione_a) + ";"
end if
if not isnull(ll_num_edizione_da) then
	ls_sql = ls_sql + " and tes_pratiche.num_edizione >= " + string(ll_num_edizione_da) + " "
	ls_selezione = ls_selezione + " Ed. Prat. da: " + string(ll_num_edizione_da) + ";"
end if
if not isnull(ll_num_edizione_a) then
	ls_sql = ls_sql + " and tes_pratiche.num_edizione <= " + string(ll_num_edizione_a) + " "
	ls_selezione = ls_selezione + " Ed. Prat. a: " + string(ll_num_edizione_a) + ";"
end if

// messagebox("debug", ls_sql)


DECLARE cur_pratiche DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql ;
OPEN DYNAMIC cur_pratiche ;
DO while 0=0
	FETCH cur_pratiche INTO :ll_det_anno_pratica, 
		:ls_det_num_pratica, 
		:ll_det_num_versione, 
		:ll_det_num_edizione, 
		:ll_det_num_versione_det, 
		:ll_det_num_edizione_det, 
		:ll_det_progressivo, 
		:ls_det_des_blob, 
		:ld_det_num_protocollo, 
		:ls_det_flag_stato_doc, 
		:ls_det_flag_ultima_revisione, 
		:ls_tes_des_pratica, 
		:ls_tes_cod_tipo_pratica, 
		:ls_tes_flag_stato_doc, 
		:ls_tes_flag_ultima_revisione ;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore lettura pratiche nel DB ", SQLCA.SQLErrText)
		CLOSE cur_pratiche;
		return
	end if
	ll_i = dw_report.insertrow(0)
	dw_report.setitem(ll_i, "anno_pratica", ll_det_anno_pratica)
	dw_report.setitem(ll_i, "num_pratica", ls_det_num_pratica)
	dw_report.setitem(ll_i, "num_versione", ll_det_num_versione)
	dw_report.setitem(ll_i, "num_edizione", ll_det_num_edizione)
	dw_report.setitem(ll_i, "cod_tipo_pratica", ls_tes_cod_tipo_pratica)
	dw_report.setitem(ll_i, "flag_stato_pratica", ls_tes_flag_stato_doc)
	dw_report.setitem(ll_i, "flag_ultima_revisione", ls_tes_flag_ultima_revisione)
	dw_report.setitem(ll_i, "flag_stato_doc_det", ls_det_flag_stato_doc)
	dw_report.setitem(ll_i, "flag_ultima_revisione_det", ls_det_flag_ultima_revisione)
	dw_report.setitem(ll_i, "des_pratica", ls_tes_des_pratica)
	dw_report.setitem(ll_i, "num_versione_det", ll_det_num_versione_det)
	dw_report.setitem(ll_i, "num_edizione_det", ll_det_num_edizione_det)
	dw_report.setitem(ll_i, "progressivo", ll_det_progressivo)
	dw_report.setitem(ll_i, "num_protocollo", ld_det_num_protocollo)
	dw_report.setitem(ll_i, "des_blob", ls_det_des_blob)
	
loop

CLOSE cur_pratiche;

dw_selezione.hide()

parent.x = 673
parent.y = 275
parent.width = 3553
parent.height = 1841

dw_report.object.selezione.text = ls_selezione
dw_report.setsort("anno_pratica A, num_pratica A, num_versione A, num_edizione A ")
dw_report.sort()
dw_report.groupcalc()

dw_report.show()
cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_pratiche
integer x = 3131
integer y = 1620
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2030
parent.height = 820

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_pratiche
integer x = 23
integer y = 20
integer width = 1943
integer height = 580
integer taborder = 20
string dataobject = "d_selezione_report_pratiche"
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;this.setitem(1, "anno_pratica_da", year(today()))
this.setitem(1, "anno_pratica_a", year(today()))
this.setitem(1, "flag_ultima_revisione", "S")



end event

type dw_report from uo_cs_xx_dw within w_report_pratiche
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1580
integer taborder = 10
string dataobject = "d_report_pratiche"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

