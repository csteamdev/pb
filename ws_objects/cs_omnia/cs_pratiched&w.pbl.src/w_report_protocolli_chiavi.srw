﻿$PBExportHeader$w_report_protocolli_chiavi.srw
$PBExportComments$Finstra che mostra i protocolli associali ad una o più chiavi
forward
global type w_report_protocolli_chiavi from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_protocolli_chiavi
end type
type cb_report from commandbutton within w_report_protocolli_chiavi
end type
type cb_selezione from commandbutton within w_report_protocolli_chiavi
end type
type cb_chiavi_ricerca from uo_cb_chiavi_doc_ricerca within w_report_protocolli_chiavi
end type
type cb_add from commandbutton within w_report_protocolli_chiavi
end type
type dw_selezione from uo_cs_xx_dw within w_report_protocolli_chiavi
end type
type cb_less from commandbutton within w_report_protocolli_chiavi
end type
type dw_chiavi_selezionate from datawindow within w_report_protocolli_chiavi
end type
type st_1 from statictext within w_report_protocolli_chiavi
end type
type st_2 from statictext within w_report_protocolli_chiavi
end type
type cb_apri_prat from commandbutton within w_report_protocolli_chiavi
end type
type dw_report from uo_cs_xx_dw within w_report_protocolli_chiavi
end type
end forward

global type w_report_protocolli_chiavi from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Report Protocolli Chiavi"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
cb_chiavi_ricerca cb_chiavi_ricerca
cb_add cb_add
dw_selezione dw_selezione
cb_less cb_less
dw_chiavi_selezionate dw_chiavi_selezionate
st_1 st_1
st_2 st_2
cb_apri_prat cb_apri_prat
dw_report dw_report
end type
global w_report_protocolli_chiavi w_report_protocolli_chiavi

type variables

end variables

forward prototypes
public subroutine wf_non_conformita ()
public subroutine wf_pratiche ()
public subroutine wf_infortuni ()
public subroutine wf_cons_dispositivi ()
public subroutine wf_manutenzioni ()
public subroutine wf_attrezzature ()
public subroutine wf_prog_manutenzioni ()
end prototypes

public subroutine wf_non_conformita ();string ls_null, ls_cerca, ls_flag_tipo_nc
double ld_num_protocollo
long ll_i, ll_found, ll_num_righe_det , ll_anno_registrazione, ll_num_registrazione, ll_num_riga_find, ll_i_2, ll_max_righe, &
	  ll_anno_ds, ll_num_ds
datastore ds_non_conformita_lista

ds_non_conformita_lista = create datastore
ds_non_conformita_lista.DataObject = 'd_non_conformita_lista'
ds_non_conformita_lista.SetTransObject (sqlca)
ds_non_conformita_lista.reset()

ll_i = dw_report.getselectedrow(0)
if ll_i <= 0 then
	g_mb.messagebox("Attenzione", "Selezionare una chiave")
	return
end if
ld_num_protocollo = dw_report.getitemnumber(ll_i, "num_protocollo")

select anno_non_conf,
		num_non_conf
 into :ll_anno_registrazione,
		:ll_num_registrazione
 from det_non_conformita  
where ( cod_azienda = :s_cs_xx.cod_azienda ) and  
		( num_protocollo = :ld_num_protocollo )   ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore in lettura dettaglio non conformità " + sqlca.sqlerrtext)
	return
end if

select flag_tipo_nc  
  into :ls_flag_tipo_nc 
  from non_conformita  
 where ( cod_azienda = :s_cs_xx.cod_azienda ) and  
	 	 ( anno_non_conf = :ll_anno_registrazione) and 
		 ( num_non_conf = :ll_num_registrazione) ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore in lettura non conformità " + sqlca.sqlerrtext)
	return
end if

declare cu_non_conformita cursor for
 select anno_non_conf,
		  num_non_conf
   from non_conformita
  where cod_azienda = :s_cs_xx.cod_azienda
    and flag_tipo_nc = :ls_flag_tipo_nc;
	
open cu_non_conformita;

ll_i_2 = 1

do while 1 = 1
	fetch cu_non_conformita into :ll_anno_ds, :ll_num_ds;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Attenzione", "Errore in lettura non conformità " + sqlca.sqlerrtext)
		return
	end if
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = 0 then
		ds_non_conformita_lista.insertrow(0)
		ds_non_conformita_lista.setitem(ll_i_2, "anno_non_conf", ll_anno_ds)
		ds_non_conformita_lista.setitem(ll_i_2, "num_non_conf", ll_num_ds)		
		ll_i_2 ++
	end if
loop

close cu_non_conformita;

if ls_flag_tipo_nc = "S" then
	if not isvalid(w_non_conformita_std_sis) then
		ll_num_riga_find = ds_non_conformita_lista.find("anno_non_conf = " + string(ll_anno_registrazione) + " and num_non_conf = " + string(ll_num_registrazione), &
								 0, ds_non_conformita_lista.rowcount())
		window_open(w_non_conformita_std_sis, -1)
		if ll_num_riga_find > 0 then
			w_non_conformita_std_sis.postevent("pcd_retieve")
			s_cs_xx.parametri.parametro_d_11 = ll_num_riga_find			
			w_non_conformita_std_sis.wf_carica_da_report()
		end if			
	end if	
elseif ls_flag_tipo_nc = "A" then
	if not isvalid(w_non_conformita_std_app) then
		ll_num_riga_find = ds_non_conformita_lista.find("anno_non_conf = " + string(ll_anno_registrazione) + " and num_non_conf = " + string(ll_num_registrazione), &
								 0, ds_non_conformita_lista.rowcount())
		window_open(w_non_conformita_std_app, -1)
		if ll_num_riga_find > 0 then
			w_non_conformita_std_app.postevent("pcd_retieve")
			s_cs_xx.parametri.parametro_d_11 = ll_num_riga_find			
			w_non_conformita_std_app.wf_carica_da_report()
		end if					
	end if		
elseif ls_flag_tipo_nc = "V" then
	if not isvalid(w_non_conformita_std_ven) then
		ll_num_riga_find = ds_non_conformita_lista.find("anno_non_conf = " + string(ll_anno_registrazione) + " and num_non_conf = " + string(ll_num_registrazione), &
								 0, ds_non_conformita_lista.rowcount())
		window_open(w_non_conformita_std_ven, -1)
		if ll_num_riga_find > 0 then
			w_non_conformita_std_ven.postevent("pcd_retieve")
			s_cs_xx.parametri.parametro_d_11 = ll_num_riga_find			
			w_non_conformita_std_ven.wf_carica_da_report()
		end if					
	end if		
elseif ls_flag_tipo_nc = "I" then
	if not isvalid(w_non_conformita_std_int) then
		ll_num_riga_find = ds_non_conformita_lista.find("anno_non_conf = " + string(ll_anno_registrazione) + " and num_non_conf = " + string(ll_num_registrazione), &
								 0, ds_non_conformita_lista.rowcount())
		window_open(w_non_conformita_std_int, -1)
		if ll_num_riga_find > 0 then
			w_non_conformita_std_int.postevent("pcd_retieve")
			s_cs_xx.parametri.parametro_d_11 = ll_num_riga_find			
			w_non_conformita_std_int.wf_carica_da_report()
		end if					
	end if		
elseif ls_flag_tipo_nc = "P" then
	if not isvalid(w_non_conformita_std_prog) then
		ll_num_riga_find = ds_non_conformita_lista.find("anno_non_conf = " + string(ll_anno_registrazione) + " and num_non_conf = " + string(ll_num_registrazione), &
								 0, ds_non_conformita_lista.rowcount())
		window_open(w_non_conformita_std_prog, -1)
		if ll_num_riga_find > 0 then
			w_non_conformita_std_prog.postevent("pcd_retieve")
			s_cs_xx.parametri.parametro_d_11 = ll_num_riga_find			
			w_non_conformita_std_prog.wf_carica_da_report()
		end if					
	end if		
end if	


//if not isvalid(w_attrezzature) then
//	window_open(w_attrezzature, -1)
//end if
//
//w_attrezzature.dw_ricerca.insertrow(0)
//w_attrezzature.dw_ricerca.setitem(1,"cod_attrezzatura", ls_cod_attrezzatura)
//
//w_attrezzature.cb_ricerca.triggerevent("clicked")
//w_attrezzature.cb_documento.postevent("clicked")
//











end subroutine

public subroutine wf_pratiche ();string ls_num_pratica, ls_null, ls_cerca
double ld_num_protocollo
long ll_i, ll_anno_pratica, ll_num_versione, ll_num_edizione,  ll_num_versione_det, ll_num_edizione_det, &
		ll_progressivo, ll_found, ll_num_righe_det 



ll_i = dw_report.getselectedrow(0)
if ll_i <= 0 then
	g_mb.messagebox("Attenzione", "Selezionare la pratica da aprire")
	return
end if
ld_num_protocollo = dw_report.getitemnumber(ll_i, "num_protocollo")

// messagebox("Debug", "Num. Prot. " + string(ld_num_protocollo))


  SELECT det_pratiche.anno_pratica,   
         det_pratiche.num_pratica,   
         det_pratiche.num_versione,   
         det_pratiche.num_edizione,   
         det_pratiche.num_versione_det,   
         det_pratiche.num_edizione_det,   
         det_pratiche.progressivo  
    INTO :ll_anno_pratica,   
         :ls_num_pratica,   
         :ll_num_versione,   
         :ll_num_edizione,   
         :ll_num_versione_det,   
         :ll_num_edizione_det,   
         :ll_progressivo  
    FROM det_pratiche  
   WHERE ( det_pratiche.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( det_pratiche.num_protocollo = :ld_num_protocollo )   ;



if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore in lettura dettaglio pratiche " + sqlca.sqlerrtext)
	return
end if

if not isvalid(w_tes_pratiche) then
	window_open(w_tes_pratiche, -1)
end if

w_tes_pratiche.dw_ricerca.setitem(1,"anno_pratica", ll_anno_pratica)
w_tes_pratiche.dw_ricerca.setitem(1,"num_pratica", ls_num_pratica)
w_tes_pratiche.dw_ricerca.setitem(1,"num_versione", ll_num_versione)
w_tes_pratiche.dw_ricerca.setitem(1,"num_edizione", ll_num_edizione)
w_tes_pratiche.dw_ricerca.setitem(1,"flag_ultima_revisione", ls_null)


w_tes_pratiche.cb_reset.triggerevent("clicked")
w_tes_pratiche.cb_dettagli.triggerevent("clicked")

if isvalid(w_det_pratiche) then
//	w_det_pratiche.rb_tutti.triggerevent("clicked")
	w_det_pratiche.rb_ultima_rev.checked = false
	w_det_pratiche.rb_tutti.checked = true
	w_det_pratiche.is_vis_stato_doc = "%"
	w_det_pratiche.is_vis_ultima_rev = "%"
	w_det_pratiche.triggerevent("pc_view")
	w_det_pratiche.triggerevent("pc_retrieve")

	
//	ls_cerca = "num_versione_det = " + string(ll_num_versione_det) + " and  num_edizione_det = " + &
//				 string(ll_num_edizione_det)
// 		
//	ll_num_righe_det = w_det_pratiche.dw_det_pratiche_lista.RowCount()
//	ll_found = w_det_pratiche.dw_det_pratiche_lista.Find(ls_cerca, 1, ll_num_righe_det )
//	w_det_pratiche.dw_det_pratiche_lista.scrolltorow(ll_found) 
//	alla fine fa un retrieve e si posiziona sulla prima riga
end if










end subroutine

public subroutine wf_infortuni ();string ls_null, ls_cerca, ls_cod_attrezzatura
double ld_num_protocollo
long ll_i, ll_prog_attrezzatura, ll_found, ll_num_righe_det, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga 



ll_i = dw_report.getselectedrow(0)
if ll_i <= 0 then
	g_mb.messagebox("Attenzione", "Selezionare una chiave")
	return
end if
ld_num_protocollo = dw_report.getitemnumber(ll_i, "num_protocollo")

select anno_registrazione,
  		 num_registrazione,
		 prog_riga
into   :ll_anno_registrazione,
       :ll_num_registrazione,
		 :ll_prog_riga
from   det_reg_infortuni_blob  
where ( cod_azienda = :s_cs_xx.cod_azienda ) and  
      ( num_protocollo = :ld_num_protocollo );

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore in lettura dettaglio registro infortuni: " + sqlca.sqlerrtext)
	return
end if

s_cs_xx.parametri.parametro_d_10 = ll_anno_registrazione
s_cs_xx.parametri.parametro_d_11 = ll_num_registrazione

if not isvalid(w_det_reg_infortuni) then
	window_open(w_det_reg_infortuni, -1)
end if

w_det_reg_infortuni.triggerevent("pc_view")
w_det_reg_infortuni.triggerevent("pc_retrieve")

w_det_reg_infortuni.wf_seleziona(ll_anno_registrazione,ll_num_registrazione,ll_prog_riga)


end subroutine

public subroutine wf_cons_dispositivi ();string ls_null, ls_cerca, ls_cod_attrezzatura
double ld_num_protocollo
long ll_i, ll_prog_attrezzatura, ll_found, ll_num_righe_det, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga 



ll_i = dw_report.getselectedrow(0)
if ll_i <= 0 then
	g_mb.messagebox("Attenzione", "Selezionare una chiave")
	return
end if
ld_num_protocollo = dw_report.getitemnumber(ll_i, "num_protocollo")

select anno_registrazione,
  		 num_registrazione,
		 prog_riga
into   :ll_anno_registrazione,
       :ll_num_registrazione,
		 :ll_prog_riga
from   det_reg_consegna_disp_blob 
where ( cod_azienda = :s_cs_xx.cod_azienda ) and  
      ( num_protocollo = :ld_num_protocollo );

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore in lettura dettaglio consegna dispositivi: " + sqlca.sqlerrtext)
	return
end if

s_cs_xx.parametri.parametro_d_10 = ll_anno_registrazione
s_cs_xx.parametri.parametro_d_11 = ll_num_registrazione

if not isvalid(w_det_reg_consegna_disp) then
	window_open(w_det_reg_consegna_disp, -1)
end if

w_det_reg_consegna_disp.triggerevent("pc_view")
w_det_reg_consegna_disp.triggerevent("pc_retrieve")



end subroutine

public subroutine wf_manutenzioni ();string ls_null, ls_cerca, ls_des_intervento
double ld_num_protocollo
long ll_i, ll_found, ll_num_righe_det, ll_anno_registrazione, ll_num_registrazione



ll_i = dw_report.getselectedrow(0)
if ll_i <= 0 then
	g_mb.messagebox("Attenzione", "Selezionare una chiave")
	return
end if
ld_num_protocollo = dw_report.getitemnumber(ll_i, "num_protocollo")

  select anno_registrazione,
  			num_registrazione
    into :ll_anno_registrazione,
	 		:ll_num_registrazione 
    from det_manutenzioni
   where ( cod_azienda = :s_cs_xx.cod_azienda ) and  
         ( num_protocollo = :ld_num_protocollo )   ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore in lettura dettaglio manutenzioni " + sqlca.sqlerrtext)
	return
end if

  select des_intervento
    into :ls_des_intervento 
    from manutenzioni
   where ( cod_azienda = :s_cs_xx.cod_azienda ) and  
         ( anno_registrazione = :ll_anno_registrazione) and
         ( num_registrazione = :ll_num_registrazione);

if not isvalid(w_manutenzioni) then
	window_open(w_manutenzioni, -1)
end if

w_manutenzioni.wf_carica_singola_registrazione(ll_anno_registrazione, ll_num_registrazione)









end subroutine

public subroutine wf_attrezzature ();string ls_null, ls_cerca, ls_cod_attrezzatura
double ld_num_protocollo
long ll_i, ll_prog_attrezzatura, ll_found, ll_num_righe_det 



ll_i = dw_report.getselectedrow(0)
if ll_i <= 0 then
	g_mb.messagebox("Attenzione", "Selezionare una chiave")
	return
end if
ld_num_protocollo = dw_report.getitemnumber(ll_i, "num_protocollo")

  select cod_attrezzatura
    into :ls_cod_attrezzatura 
    from det_attrezzature  
   where ( cod_azienda = :s_cs_xx.cod_azienda ) and  
         ( num_protocollo = :ld_num_protocollo )   ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore in lettura dettaglio attrezzature " + sqlca.sqlerrtext)
	return
end if

s_cs_xx.parametri.parametro_s_10 = ls_cod_attrezzatura

if not isvalid(w_attrezzature) then
	window_open(w_attrezzature, -1)
end if

// stefanop 01/04/2009
/*w_attrezzature.dw_ricerca.insertrow(0)
w_attrezzature.dw_ricerca.setitem(1,"cod_attrezzatura", ls_cod_attrezzatura)

w_attrezzature.cb_ricerca.triggerevent("clicked")
w_attrezzature.cb_documento.postevent("clicked")
*/
w_attrezzature.dw_filtro.triggerevent("ue_clear")
w_attrezzature.dw_filtro.setitem(1, "cod_attrezzatura_da", ls_cod_attrezzatura)
w_attrezzature.cb_cerca.triggerevent("clicked")

//messagebox("Attenzione", "1")

//if isvalid(w_det_attrezzature) then
//	s_cs_xx.parametri.parametro_s_10 = ls_cod_attrezzatura
//	w_det_attrezzature.triggerevent("pc_view")
//	w_det_attrezzature.triggerevent("pc_retrieve")
//end if










end subroutine

public subroutine wf_prog_manutenzioni ();string ls_null, ls_cerca, ls_des_intervento
double ld_num_protocollo
long ll_i, ll_found, ll_num_righe_det, ll_anno_registrazione, ll_num_registrazione



ll_i = dw_report.getselectedrow(0)
if ll_i <= 0 then
	g_mb.messagebox("Attenzione", "Selezionare una chiave")
	return
end if
ld_num_protocollo = dw_report.getitemnumber(ll_i, "num_protocollo")

  select anno_registrazione,
  			num_registrazione
    into :ll_anno_registrazione,
	 		:ll_num_registrazione 
    from det_prog_manutenzioni
   where ( cod_azienda = :s_cs_xx.cod_azienda ) and  
         ( num_protocollo = :ld_num_protocollo )   ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore in lettura dettaglio programmi manutenzioni " + sqlca.sqlerrtext)
	return
end if

  select des_intervento
    into :ls_des_intervento 
    from programmi_manutenzione
   where ( cod_azienda = :s_cs_xx.cod_azienda ) and  
         ( anno_registrazione = :ll_anno_registrazione) and
         ( num_registrazione = :ll_num_registrazione);

if not isvalid(w_programmi_manutenzione) then
	window_open(w_programmi_manutenzione, -1)
end if

//w_programmi_manutenzione.wf_imposta_tv()
//w_programmi_manutenzione.wf_ricerca_item(ll_anno_registrazione, ll_num_registrazione, ls_des_intervento)
//









end subroutine

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

//dw_report.set_dw_options(sqlca, &
//                         pcca.null_object, &
//                         c_nomodify + &
//                         c_nodelete + &
//                         c_newonopen + &
//                         c_disableCC, &
//                         c_noresizedw + &
//                         c_nohighlightselected + &
//                         c_nocursorrowpointer +&
//                         c_nocursorrowfocusrect )
dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_default )


dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)


iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2200
this.height = 1300

end event

on w_report_protocolli_chiavi.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.cb_chiavi_ricerca=create cb_chiavi_ricerca
this.cb_add=create cb_add
this.dw_selezione=create dw_selezione
this.cb_less=create cb_less
this.dw_chiavi_selezionate=create dw_chiavi_selezionate
this.st_1=create st_1
this.st_2=create st_2
this.cb_apri_prat=create cb_apri_prat
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.cb_chiavi_ricerca
this.Control[iCurrent+5]=this.cb_add
this.Control[iCurrent+6]=this.dw_selezione
this.Control[iCurrent+7]=this.cb_less
this.Control[iCurrent+8]=this.dw_chiavi_selezionate
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.st_2
this.Control[iCurrent+11]=this.cb_apri_prat
this.Control[iCurrent+12]=this.dw_report
end on

on w_report_protocolli_chiavi.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.cb_chiavi_ricerca)
destroy(this.cb_add)
destroy(this.dw_selezione)
destroy(this.cb_less)
destroy(this.dw_chiavi_selezionate)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.cb_apri_prat)
destroy(this.dw_report)
end on

type cb_annulla from commandbutton within w_report_protocolli_chiavi
integer x = 1371
integer y = 1060
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_protocolli_chiavi
integer x = 1760
integer y = 1060
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;double ld_num_protocollo_da, ld_num_protocollo_a, ld_num_protocollo
string ls_flag_pratiche, ls_sql, ls_where, ls_cod_famiglia_chiavi, ls_cod_chiave, &
		 ls_cod_famiglia_chiavi_sel, ls_cod_chiave_sel, ls_num_pratica, ls_blob, ls_flag_stato_doc_det, &
		 ls_flag_ultima_revisione_det, ls_des_oggetto, ls_flag_sel_pratiche, ls_flag_sel_attrezzature, &
		 ls_flag_sel_manutenzioni, ls_flag_prog_manut, ls_flag_non_conformita, ls_flag_sel_infortuni, ls_flag_cons_disp
long ll_num_chiavi, ll_i, ll_anno_pratica, ll_num_versione, ll_num_edizione, ll_num_versione_det, &
		ll_num_edizione_det, ll_progressivo

dw_selezione.accepttext()
dw_report.reset()

ll_num_chiavi = dw_chiavi_selezionate.rowcount()

ls_flag_sel_pratiche = dw_selezione.getitemstring(1, "flag_sel_pratiche")
ls_flag_sel_attrezzature = dw_selezione.getitemstring(1, "flag_sel_attrezzature")
ls_flag_sel_manutenzioni = dw_selezione.getitemstring(1, "flag_sel_manutenzioni")
ls_flag_prog_manut = dw_selezione.getitemstring(1, "flag_sel_prog_manut")
ls_flag_non_conformita = dw_selezione.getitemstring(1, "flag_non_conformita")
ls_flag_sel_infortuni = dw_selezione.getitemstring(1, "flag_sel_infortuni")
ls_flag_cons_disp = dw_selezione.getitemstring(1, "flag_sel_cons_disp")

ls_flag_pratiche = dw_selezione.getitemstring(1, "flag_pratiche")
ld_num_protocollo_da = dw_selezione.getitemnumber(1, "num_protocollo_da")
ld_num_protocollo_a = dw_selezione.getitemnumber(1, "num_protocollo_a")
ls_cod_famiglia_chiavi_sel = dw_selezione.getitemstring(1, "cod_famiglia_chiave")
ls_cod_chiave_sel = dw_selezione.getitemstring(1, "cod_chiave")
ls_where = ""
if ll_num_chiavi > 0 then
	for ll_i = 1 to ll_num_chiavi
		ls_cod_famiglia_chiavi = dw_chiavi_selezionate.getitemstring(ll_i, "l_cod_famiglia_chiavi")
		ls_cod_chiave = dw_chiavi_selezionate.getitemstring(ll_i, "l_cod_chiave")
		if ll_i > 1 then
			ls_where = ls_where + " or num_protocollo in (SELECT num_protocollo " + &
									"FROM tab_chiavi_protocollo  " + &
									"where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
									"and  (tab_chiavi_protocollo.cod_famiglia_chiavi = '" + ls_cod_famiglia_chiavi + &
									"' and tab_chiavi_protocollo.cod_chiave = '" + ls_cod_chiave + "') ) "	
		else	
			ls_where = ls_where + " and (num_protocollo in (SELECT num_protocollo " + &
									"FROM tab_chiavi_protocollo  " + &
									"where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
									"and  (tab_chiavi_protocollo.cod_famiglia_chiavi = '" + ls_cod_famiglia_chiavi + &
									"' and tab_chiavi_protocollo.cod_chiave = '" + ls_cod_chiave + "') ) "
		end if							
	next
	ls_where = ls_where + ") "
end if

if isnull(ls_cod_famiglia_chiavi_sel) and (ll_num_chiavi = 0) then	// no chiavi selezionate, solo cod_chiave
	if not isnull(ls_cod_chiave_sel) then
		ls_where = " and tab_chiavi_protocollo.cod_chiave = '" + ls_cod_chiave_sel + "' "
	end if
end if
if isnull(ls_cod_chiave_sel) then	// no chiavi selezionate, solo cod_famiglia_chiavi
	if not isnull(ls_cod_famiglia_chiavi_sel) then
		ls_where = " and tab_chiavi_protocollo.cod_famiglia_chiavi = '" + ls_cod_famiglia_chiavi_sel + "' "
	end if
end if

if not isnull(ld_num_protocollo_da) then
	ls_where = " and tab_chiavi_protocollo.num_protocollo >= " + string(ld_num_protocollo_da) + " "
end if
if not isnull(ld_num_protocollo_a) then
	ls_where = " and tab_chiavi_protocollo.num_protocollo <= " + string(ld_num_protocollo_a) + " "
end if

//modifica Nicola

if ls_flag_sel_pratiche = 'S' then
	ls_where = ls_where + " and tab_chiavi_protocollo.num_protocollo in (select num_protocollo from det_pratiche) "
end if
if ls_flag_sel_attrezzature = 'S' then
	ls_where = ls_where + " and tab_chiavi_protocollo.num_protocollo in (select num_protocollo from det_attrezzature) "
end if
if ls_flag_sel_manutenzioni = 'S' then
	ls_where = ls_where + " and tab_chiavi_protocollo.num_protocollo in (select num_protocollo from det_manutenzioni) "
end if
if ls_flag_prog_manut = 'S' then
	ls_where = ls_where + " and tab_chiavi_protocollo.num_protocollo in (select num_protocollo from det_prog_manutenzioni) "
end if
if ls_flag_non_conformita = 'S' then
	ls_where = ls_where + " and tab_chiavi_protocollo.num_protocollo in (select num_protocollo from det_non_conformita) "
end if

if ls_flag_sel_infortuni = 'S' then
	ls_where = ls_where + " and tab_chiavi_protocollo.num_protocollo in (select num_protocollo from det_reg_infortuni_blob) "
end if
if ls_flag_cons_disp = 'S' then
	ls_where = ls_where + " and tab_chiavi_protocollo.num_protocollo in (select num_protocollo from det_reg_consegna_disp_blob) "
end if
//fine modifica
ls_sql = "SELECT tab_chiavi_protocollo.num_protocollo, " + &
			"		tab_chiavi_protocollo.cod_famiglia_chiavi, " + &
			"		tab_chiavi_protocollo.cod_chiave " + &
			" FROM tab_chiavi_protocollo   " + &
			" where cod_azienda = '" + s_cs_xx.cod_azienda + "' " 
ls_sql = ls_sql + ls_where 

// messagebox("debug", ls_sql)

DECLARE cur_protocolli DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql ;
OPEN DYNAMIC cur_protocolli ;
DO while 0=0
	FETCH cur_protocolli INTO :ld_num_protocollo, 
		:ls_cod_famiglia_chiavi, 
		:ls_cod_chiave; 
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore lettura protocolli - chiavi nel DB ", SQLCA.SQLErrText)
		CLOSE cur_protocolli;
		return
	end if
	
	ll_i = dw_report.insertrow(0)
	dw_report.setitem(ll_i, "cod_famiglia_chiavi", ls_cod_famiglia_chiavi)
	dw_report.setitem(ll_i, "cod_chiave", ls_cod_chiave)
	dw_report.setitem(ll_i, "num_protocollo", ld_num_protocollo)
	if ls_flag_pratiche = "S" then	// recuperare i dati delle pratiche associati ai protocolli
		SELECT det_pratiche.anno_pratica,   
			det_pratiche.num_pratica,   
			det_pratiche.num_versione,   
			det_pratiche.num_edizione,   
			det_pratiche.num_versione_det,   
			det_pratiche.num_edizione_det,   
			det_pratiche.progressivo,   
			det_pratiche.des_blob,   
			det_pratiche.flag_stato_doc,   
			det_pratiche.flag_ultima_revisione  
		INTO :ll_anno_pratica,   
			:ls_num_pratica,   
			:ll_num_versione,   
			:ll_num_edizione,   
			:ll_num_versione_det,   
			:ll_num_edizione_det,   
			:ll_progressivo,   
			:ls_blob,   
			:ls_flag_stato_doc_det,   
			:ls_flag_ultima_revisione_det  
		FROM det_pratiche
		 where cod_azienda = :s_cs_xx.cod_azienda
		 and num_protocollo = :ld_num_protocollo;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Errore lettura pratica prot." + string(ld_num_protocollo), sqlca.sqlerrtext)
			return
		end if
		ls_des_oggetto = "Pratica: " + string(ll_anno_pratica) + "-" + ls_num_pratica + "  " + &
				string(ll_num_versione) + "/" + string(ll_num_edizione) + "  Dettaglio: " + & 
				string(ll_num_versione_det) + "/" + string(ll_num_edizione_det) + "  " + &
				string(ll_progressivo)
//		messagebox("debug", ls_des_oggetto)
	dw_report.setitem(ll_i, "des_oggetto", ls_des_oggetto)
				
	end if

loop

CLOSE cur_protocolli;

dw_selezione.hide()

dw_report.setsort("num_protocollo A")
dw_report.sort()
dw_report.groupcalc()


dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.show()
cb_selezione.show()
cb_apri_prat.show()


dw_report.change_dw_current()

dw_report.SelectRow(1, TRUE)
end event

type cb_selezione from commandbutton within w_report_protocolli_chiavi
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

dw_selezione.show()
cb_chiavi_ricerca.show()
parent.x = 741
parent.y = 885
parent.width = 2200
parent.height = 1300

dw_report.hide()
cb_selezione.hide()
cb_apri_prat.hide()

dw_selezione.change_dw_current()
end event

type cb_chiavi_ricerca from uo_cb_chiavi_doc_ricerca within w_report_protocolli_chiavi
integer x = 2016
integer y = 444
integer width = 73
integer height = 80
integer taborder = 10
boolean bringtotop = true
end type

event clicked;if not isvalid(w_ricerca_chiavi_doc) then
   window_open(w_ricerca_chiavi_doc, 0)
end if

end event

event getfocus;call super::getfocus;s_cs_xx.parametri.parametro_uo_dw_1 = dw_selezione
s_cs_xx.parametri.parametro_s_1 = "cod_famiglia_chiave"
s_cs_xx.parametri.parametro_s_2 = "cod_chiave"


end event

type cb_add from commandbutton within w_report_protocolli_chiavi
integer x = 2034
integer y = 560
integer width = 91
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = symbol!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "Symbol"
string text = "¯"
end type

event clicked;string ls_cod_famiglia_chiave, ls_cod_chiave
long ll_i, ll_found1, ll_found2, ll_num_righe

ls_cod_famiglia_chiave = dw_selezione.getitemstring(1, "cod_famiglia_chiave")
ls_cod_chiave = dw_selezione.getitemstring(1, "cod_chiave")

if isnull(ls_cod_famiglia_chiave) or ls_cod_famiglia_chiave="" or isnull(ls_cod_chiave) or ls_cod_chiave= "" then
	g_mb.messagebox("Errore", "Inserire i campi Famiglia chiavi e Chiave")
	return
end if

ll_num_righe = dw_chiavi_selezionate.rowcount()
ll_found1 = dw_chiavi_selezionate.Find("l_cod_famiglia_chiavi = '"+ ls_cod_famiglia_chiave + "'",  1, ll_num_righe)
if ll_found1 > 0 then
	ll_found2 = dw_chiavi_selezionate.Find("l_cod_chiave = '"+ ls_cod_chiave + "'",  ll_found1, ll_num_righe)
	if  ll_found2 > 0 then
		g_mb.messagebox("Attenzione", "Chiave già selezionata")
		return
	end if
end if


ll_i = dw_chiavi_selezionate.insertrow(0)
dw_chiavi_selezionate.setitem(ll_i, "l_cod_famiglia_chiavi", ls_cod_famiglia_chiave)
dw_chiavi_selezionate.setitem(ll_i, "l_cod_chiave", ls_cod_chiave)
dw_chiavi_selezionate.ScrollToRow (ll_i) 
dw_chiavi_selezionate.SelectRow( 0, false)
dw_chiavi_selezionate.SelectRow( ll_i, true)






end event

type dw_selezione from uo_cs_xx_dw within w_report_protocolli_chiavi
integer x = 23
integer y = 20
integer width = 2103
integer height = 520
integer taborder = 20
string dataobject = "d_selezione_protocolli_chiavi"
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;setitem(1, "flag_pratiche", "S")
setitem(1, "flag_sel_pratiche", "S")
setitem(1, "flag_sel_attrezzature", "N")
setitem(1, "flag_sel_manutenzioni", "N")
setitem(1, "flag_sel_prog_manut", "N")
setitem(1, "flag_non_conformita", "N")
setitem(1, "flag_sel_infortuni", "N")
setitem(1, "flag_sel_cons_disp", "N")
end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "flag_sel_pratiche"
		dw_selezione.object.flag_pratiche.TabSequence = 30
		setitem(1, "flag_sel_attrezzature", "N")
		setitem(1, "flag_sel_manutenzioni", "N")
		setitem(1, "flag_sel_prog_manut", "N")
		setitem(1, "flag_non_conformita", "N")	
		setitem(1, "flag_sel_infortuni", "N")
		setitem(1, "flag_sel_cons_disp", "N")
	case "flag_sel_attrezzature"
		setitem(1, "flag_sel_pratiche", "N")
		setitem(1, "flag_sel_manutenzioni", "N")
		setitem(1, "flag_sel_prog_manut", "N")		
		setitem(1, "flag_pratiche", "N")
		setitem(1, "flag_non_conformita", "N")				
		setitem(1, "flag_sel_infortuni", "N")
		setitem(1, "flag_sel_cons_disp", "N")		
		dw_selezione.object.flag_pratiche.TabSequence = 0
	case "flag_sel_manutenzioni"		
		setitem(1, "flag_sel_attrezzature", "N")
		setitem(1, "flag_sel_pratiche", "N")
		setitem(1, "flag_sel_prog_manut", "N")		
		setitem(1, "flag_pratiche", "N")		
		setitem(1, "flag_non_conformita", "N")				
		setitem(1, "flag_sel_infortuni", "N")
		setitem(1, "flag_sel_cons_disp", "N")		
		dw_selezione.object.flag_pratiche.TabSequence = 0
	case "flag_sel_prog_manut"
		setitem(1, "flag_sel_attrezzature", "N")
		setitem(1, "flag_sel_manutenzioni", "N")
		setitem(1, "flag_sel_pratiche", "N")
		setitem(1, "flag_pratiche", "N")		
		setitem(1, "flag_non_conformita", "N")				
		setitem(1, "flag_sel_infortuni", "N")
		setitem(1, "flag_sel_cons_disp", "N")		
		dw_selezione.object.flag_pratiche.TabSequence = 0
	case "flag_non_conformita"
		setitem(1, "flag_sel_attrezzature", "N")
		setitem(1, "flag_sel_manutenzioni", "N")
		setitem(1, "flag_sel_pratiche", "N")
		setitem(1, "flag_pratiche", "N")		
		setitem(1, "flag_sel_prog_manut", "N")				
		setitem(1, "flag_sel_infortuni", "N")
		setitem(1, "flag_sel_cons_disp", "N")		
		dw_selezione.object.flag_pratiche.TabSequence = 0		
	case "flag_sel_infortuni"
		setitem(1, "flag_sel_attrezzature", "N")
		setitem(1, "flag_sel_manutenzioni", "N")
		setitem(1, "flag_sel_pratiche", "N")
		setitem(1, "flag_pratiche", "N")		
		setitem(1, "flag_sel_prog_manut", "N")				
		setitem(1, "flag_non_conformita", "N")
		setitem(1, "flag_sel_cons_disp", "N")	
		dw_selezione.object.flag_pratiche.TabSequence = 0
	case "flag_sel_cons_disp"
		setitem(1, "flag_sel_attrezzature", "N")
		setitem(1, "flag_sel_manutenzioni", "N")
		setitem(1, "flag_sel_pratiche", "N")
		setitem(1, "flag_pratiche", "N")		
		setitem(1, "flag_sel_prog_manut", "N")				
		setitem(1, "flag_non_conformita", "N")
		setitem(1, "flag_sel_infortuni", "N")	
		dw_selezione.object.flag_pratiche.TabSequence = 0		
end choose	
end event

type cb_less from commandbutton within w_report_protocolli_chiavi
integer x = 663
integer y = 560
integer width = 96
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = symbol!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "Symbol"
string text = "­"
end type

event clicked;long ll_i

ll_i = dw_chiavi_selezionate.GetSelectedRow(0)

// messagebox("debug", string(ll_i))


if ll_i > 0 then
	dw_chiavi_selezionate.deleterow(ll_i)
end if
end event

type dw_chiavi_selezionate from datawindow within w_report_protocolli_chiavi
integer x = 23
integer y = 640
integer width = 2103
integer height = 400
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_chiavi_selezionate"
boolean vscrollbar = true
boolean livescroll = true
end type

event clicked;this.SelectRow( 0, false)
this.selectrow(row, true)




end event

type st_1 from statictext within w_report_protocolli_chiavi
integer x = 1349
integer y = 560
integer width = 672
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Aggiungi Chiave alla Lista"
boolean focusrectangle = false
end type

type st_2 from statictext within w_report_protocolli_chiavi
integer x = 23
integer y = 560
integer width = 626
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Rimuovi Chiave da Lista"
boolean focusrectangle = false
end type

type cb_apri_prat from commandbutton within w_report_protocolli_chiavi
integer x = 2720
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 81
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Apri Doc."
end type

event clicked;dw_report.accepttext()

if dw_selezione.getitemstring(1, "flag_sel_pratiche") = "S" then
	wf_pratiche()
end if	

if dw_selezione.getitemstring(1, "flag_sel_attrezzature") = "S" then
	wf_attrezzature()
end if	

if dw_selezione.getitemstring(1, "flag_sel_manutenzioni") = "S" then
	wf_manutenzioni()
end if	

if dw_selezione.getitemstring(1, "flag_sel_prog_manut") = "S" then
	wf_prog_manutenzioni()
end if	

if dw_selezione.getitemstring(1, "flag_non_conformita") = "S" then
	wf_non_conformita()
end if	

if dw_selezione.getitemstring(1, "flag_sel_infortuni") = "S" then
	wf_infortuni()
end if	

if dw_selezione.getitemstring(1, "flag_sel_cons_disp") = "S" then
	wf_cons_dispositivi()
end if



end event

type dw_report from uo_cs_xx_dw within w_report_protocolli_chiavi
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 40
string dataobject = "d_report_protocolli_chiavi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

