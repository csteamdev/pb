﻿$PBExportHeader$w_tab_fam_chiavi_doc.srw
$PBExportComments$Finstra Definizione Famiglie chiavi
forward
global type w_tab_fam_chiavi_doc from w_cs_xx_principale
end type
type dw_tab_fam_chiavi_doc from uo_cs_xx_dw within w_tab_fam_chiavi_doc
end type
type rb_clienti from radiobutton within w_tab_fam_chiavi_doc
end type
type rb_fornitori from radiobutton within w_tab_fam_chiavi_doc
end type
type rb_prodotti from radiobutton within w_tab_fam_chiavi_doc
end type
type rb_dipendenti from radiobutton within w_tab_fam_chiavi_doc
end type
type r_1 from rectangle within w_tab_fam_chiavi_doc
end type
type cb_aggiungi from commandbutton within w_tab_fam_chiavi_doc
end type
type st_progress from statictext within w_tab_fam_chiavi_doc
end type
type rb_attrezzature from radiobutton within w_tab_fam_chiavi_doc
end type
end forward

global type w_tab_fam_chiavi_doc from w_cs_xx_principale
int Width=2926
int Height=1681
boolean TitleBar=true
string Title="Tabella Famiglia Chiavi"
dw_tab_fam_chiavi_doc dw_tab_fam_chiavi_doc
rb_clienti rb_clienti
rb_fornitori rb_fornitori
rb_prodotti rb_prodotti
rb_dipendenti rb_dipendenti
r_1 r_1
cb_aggiungi cb_aggiungi
st_progress st_progress
rb_attrezzature rb_attrezzature
end type
global w_tab_fam_chiavi_doc w_tab_fam_chiavi_doc

forward prototypes
public function integer wf_clienti (string fs_cod_fam_chiave, string fs_des_fam_chiave)
public function integer wf_fornitori (string fs_cod_fam_chiave, string fs_des_fam_chiave)
public function integer wf_prodotti (string fs_cod_fam_chiave, string fs_des_fam_chiave)
public function integer wf_dipendenti (string fs_cod_fam_chiave, string fs_des_fam_chiave)
public function integer wf_attrezzature (string fs_cod_fam_chiave, string fs_des_fam_chiave)
end prototypes

public function integer wf_clienti (string fs_cod_fam_chiave, string fs_des_fam_chiave);string ls_cod_cliente, ls_des_chiave

declare cu_clienti cursor for
 select cod_cliente
   from anag_clienti
  where cod_azienda = :s_cs_xx.cod_azienda
    and flag_blocco = 'N';

open cu_clienti;

do while 1 = 1
	fetch cu_clienti into :ls_cod_cliente;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati in tabella ANAG_CLIENTI")
		return -1
	end if	
	if sqlca.sqlcode = 0 then
		ls_des_chiave = fs_des_fam_chiave + " " + ls_cod_cliente

		st_progress.text = ls_des_chiave

		insert into tab_chiavi_doc (cod_azienda, cod_famiglia_chiavi, cod_chiave, des_chiave)
		     values (:s_cs_xx.cod_azienda, :fs_cod_fam_chiave, :ls_cod_cliente, :ls_des_chiave) ;	
			  
//		if sqlca.sqlcode <> 0 then 		
//			messagebox("Apice", sqlca.sqlerrtext + " " + string(sqlca.sqlcode))
//			messagebox("Apice", "Cod_chiave " + ls_cod_cliente + " Des_chiave " + ls_des_chiave)				
//		end if	
	end if
loop

close cu_clienti;
return 1
end function

public function integer wf_fornitori (string fs_cod_fam_chiave, string fs_des_fam_chiave);string ls_cod_fornitore, ls_des_chiave

declare cu_fornitori cursor for
 select cod_fornitore
   from anag_fornitori
  where cod_azienda = :s_cs_xx.cod_azienda
    and flag_blocco = 'N';

open cu_fornitori;

do while 1 = 1
	fetch cu_fornitori into :ls_cod_fornitore;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati in tabella ANAG_FORNITORI")
		return -1
	end if	
	if sqlca.sqlcode = 0 then
		ls_des_chiave = fs_des_fam_chiave + " " + ls_cod_fornitore

		st_progress.text = ls_des_chiave

		insert into tab_chiavi_doc (cod_azienda, cod_famiglia_chiavi, cod_chiave, des_chiave)
		     values (:s_cs_xx.cod_azienda, :fs_cod_fam_chiave, :ls_cod_fornitore, :ls_des_chiave) ;	
			  
//		if sqlca.sqlcode <> 0 then 		
//			messagebox("Apice", sqlca.sqlerrtext + " " + string(sqlca.sqlcode))
//			messagebox("Apice", "Cod_chiave " + ls_cod_fornitore + " Des_chiave " + ls_des_chiave)				
//		end if	
	end if
loop

close cu_fornitori;
return 1
end function

public function integer wf_prodotti (string fs_cod_fam_chiave, string fs_des_fam_chiave);string ls_cod_prodotto, ls_des_chiave

declare cu_prodotti cursor for
 select cod_prodotto
   from anag_prodotti
  where cod_azienda = :s_cs_xx.cod_azienda
    and flag_blocco = 'N';

open cu_prodotti;

do while 1 = 1
	fetch cu_prodotti into :ls_cod_prodotto;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati in tabella ANAG_CLIENTI")
		return -1
	end if	
	if sqlca.sqlcode = 0 then
		ls_des_chiave = fs_des_fam_chiave + " " + ls_cod_prodotto

		st_progress.text = ls_des_chiave

		insert into tab_chiavi_doc (cod_azienda, cod_famiglia_chiavi, cod_chiave, des_chiave)
		     values (:s_cs_xx.cod_azienda, :fs_cod_fam_chiave, :ls_cod_prodotto, :ls_des_chiave) ;	

	end if
loop

close cu_prodotti;
return 1
end function

public function integer wf_dipendenti (string fs_cod_fam_chiave, string fs_des_fam_chiave);string ls_cod_dipendente, ls_des_chiave

declare cu_dipendenti cursor for
 select cod_operaio
   from anag_operai
  where cod_azienda = :s_cs_xx.cod_azienda;

open cu_dipendenti;

do while 1 = 1
	fetch cu_dipendenti into :ls_cod_dipendente;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati in tabella ANAG_OPERAI")
		return -1
	end if	
	if sqlca.sqlcode = 0 then
		ls_des_chiave = fs_des_fam_chiave + " " + ls_cod_dipendente

		st_progress.text = ls_des_chiave

		insert into tab_chiavi_doc (cod_azienda, cod_famiglia_chiavi, cod_chiave, des_chiave)
		     values (:s_cs_xx.cod_azienda, :fs_cod_fam_chiave, :ls_cod_dipendente, :ls_des_chiave) ;	
			  
	end if
loop

close cu_dipendenti;
return 1
end function

public function integer wf_attrezzature (string fs_cod_fam_chiave, string fs_des_fam_chiave);string ls_cod_attrezzatura, ls_des_chiave

declare cu_attrezzature cursor for
 select cod_attrezzatura
   from anag_attrezzature
  where cod_azienda = :s_cs_xx.cod_azienda
    and flag_blocco = 'N';

open cu_attrezzature;

do while 1 = 1
	fetch cu_attrezzature into :ls_cod_attrezzatura;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati in tabella ANAG_ATTREZZATURE")
		return -1
	end if	
	if sqlca.sqlcode = 0 then
		ls_des_chiave = fs_des_fam_chiave + " " + ls_cod_attrezzatura

		st_progress.text = ls_des_chiave

		insert into tab_chiavi_doc (cod_azienda, cod_famiglia_chiavi, cod_chiave, des_chiave)
		     values (:s_cs_xx.cod_azienda, :fs_cod_fam_chiave, :ls_cod_attrezzatura, :ls_des_chiave) ;	

	end if
loop

close cu_attrezzature;
return 1
end function

on w_tab_fam_chiavi_doc.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_fam_chiavi_doc=create dw_tab_fam_chiavi_doc
this.rb_clienti=create rb_clienti
this.rb_fornitori=create rb_fornitori
this.rb_prodotti=create rb_prodotti
this.rb_dipendenti=create rb_dipendenti
this.r_1=create r_1
this.cb_aggiungi=create cb_aggiungi
this.st_progress=create st_progress
this.rb_attrezzature=create rb_attrezzature
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_fam_chiavi_doc
this.Control[iCurrent+2]=rb_clienti
this.Control[iCurrent+3]=rb_fornitori
this.Control[iCurrent+4]=rb_prodotti
this.Control[iCurrent+5]=rb_dipendenti
this.Control[iCurrent+6]=r_1
this.Control[iCurrent+7]=cb_aggiungi
this.Control[iCurrent+8]=st_progress
this.Control[iCurrent+9]=rb_attrezzature
end on

on w_tab_fam_chiavi_doc.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_fam_chiavi_doc)
destroy(this.rb_clienti)
destroy(this.rb_fornitori)
destroy(this.rb_prodotti)
destroy(this.rb_dipendenti)
destroy(this.r_1)
destroy(this.cb_aggiungi)
destroy(this.st_progress)
destroy(this.rb_attrezzature)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_fam_chiavi_doc.set_dw_key("cod_azienda")
dw_tab_fam_chiavi_doc.set_dw_options(sqlca, &
											pcca.null_object, &
											c_default, &
											c_default)

iuo_dw_main = dw_tab_fam_chiavi_doc
end event

type dw_tab_fam_chiavi_doc from uo_cs_xx_dw within w_tab_fam_chiavi_doc
int X=23
int Y=21
int Width=2423
int Height=1441
int TabOrder=10
string DataObject="d_tab_fam_chiavi_doc"
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type rb_clienti from radiobutton within w_tab_fam_chiavi_doc
int X=2492
int Y=41
int Width=343
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="Clienti"
BorderStyle BorderStyle=StyleLowered!
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_fornitori from radiobutton within w_tab_fam_chiavi_doc
int X=2492
int Y=121
int Width=343
int Height=77
int TabOrder=30
boolean BringToTop=true
string Text="Fornitori"
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_prodotti from radiobutton within w_tab_fam_chiavi_doc
int X=2492
int Y=201
int Width=343
int Height=77
int TabOrder=40
boolean BringToTop=true
string Text="Prodotti"
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_dipendenti from radiobutton within w_tab_fam_chiavi_doc
int X=2492
int Y=281
int Width=343
int Height=77
int TabOrder=50
boolean BringToTop=true
string Text="Dipendenti"
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type r_1 from rectangle within w_tab_fam_chiavi_doc
int X=2469
int Y=21
int Width=394
int Height=425
boolean Enabled=false
int LineThickness=5
long FillColor=12632256
end type

type cb_aggiungi from commandbutton within w_tab_fam_chiavi_doc
int X=2492
int Y=461
int Width=366
int Height=81
int TabOrder=60
boolean BringToTop=true
string Text="&Aggiungi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_fam_chiave, ls_des_fam_chiave

SetPointer(HourGlass!)

ls_cod_fam_chiave = dw_tab_fam_chiavi_doc.getitemstring(dw_tab_fam_chiavi_doc.getrow(), "cod_famiglia_chiavi")
ls_des_fam_chiave = dw_tab_fam_chiavi_doc.getitemstring(dw_tab_fam_chiavi_doc.getrow(), "des_famiglia_chiavi")

if rb_clienti.checked = true then
	
	if wf_clienti(ls_cod_fam_chiave, ls_des_fam_chiave) = 1 then
		st_progress.text = "OK !"
		g_mb.messagebox("Apice", "Inserimento Chiavi Clienti avvenuto con successo!")	
	end if
	
elseif rb_fornitori.checked = true then	

	if wf_fornitori(ls_cod_fam_chiave, ls_des_fam_chiave) = 1 then
		st_progress.text = "OK !"
		g_mb.messagebox("Apice", "Inserimento Chiavi Fornitori avvenuto con successo!")	
	end if

elseif rb_prodotti.checked = true then	

	if wf_prodotti(ls_cod_fam_chiave, ls_des_fam_chiave) = 1 then
		st_progress.text = "OK !"
		g_mb.messagebox("Apice", "Inserimento Chiavi Prodotti avvenuto con successo!")	
	end if

elseif rb_dipendenti.checked = true then		

	if wf_dipendenti(ls_cod_fam_chiave, ls_des_fam_chiave) = 1 then
		st_progress.text = "OK !"
		g_mb.messagebox("Apice", "Inserimento Chiavi Dipendenti avvenuto con successo!")	
	end if

elseif rb_attrezzature.checked = true then		

	if wf_attrezzature(ls_cod_fam_chiave, ls_des_fam_chiave) = 1 then
		st_progress.text = "OK !"
		g_mb.messagebox("Apice", "Inserimento Chiavi Dipendenti avvenuto con successo!")	
	end if

end if	

st_progress.text = " "

SetPointer(Arrow!)
end event

type st_progress from statictext within w_tab_fam_chiavi_doc
int X=23
int Y=1481
int Width=2378
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_attrezzature from radiobutton within w_tab_fam_chiavi_doc
int X=2492
int Y=361
int Width=343
int Height=77
boolean BringToTop=true
string Text="Attrez."
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

