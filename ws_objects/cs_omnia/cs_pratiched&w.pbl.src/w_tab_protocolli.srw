﻿$PBExportHeader$w_tab_protocolli.srw
$PBExportComments$Tabella Protocolli
forward
global type w_tab_protocolli from w_cs_xx_principale
end type
type dw_tab_protocolli from uo_cs_xx_dw within w_tab_protocolli
end type
end forward

global type w_tab_protocolli from w_cs_xx_principale
int Width=1418
int Height=1389
boolean TitleBar=true
string Title="Tabella Protocolli"
dw_tab_protocolli dw_tab_protocolli
end type
global w_tab_protocolli w_tab_protocolli

on w_tab_protocolli.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_protocolli=create dw_tab_protocolli
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_protocolli
end on

on w_tab_protocolli.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_protocolli)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_protocolli.set_dw_key("cod_azienda")
dw_tab_protocolli.set_dw_options(sqlca, &
											pcca.null_object, &
											c_NoNew + c_NoDelete, &
											c_default)

iuo_dw_main = dw_tab_protocolli
end event

type dw_tab_protocolli from uo_cs_xx_dw within w_tab_protocolli
int X=23
int Y=21
int Width=1326
int Height=1241
int TabOrder=1
string DataObject="d_tab_protocolli"
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

