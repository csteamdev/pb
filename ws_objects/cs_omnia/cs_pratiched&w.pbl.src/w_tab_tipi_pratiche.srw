﻿$PBExportHeader$w_tab_tipi_pratiche.srw
$PBExportComments$finestra tab_tipi_pratiche
forward
global type w_tab_tipi_pratiche from w_cs_xx_principale
end type
type dw_tab_tipi_pratiche_lista from uo_cs_xx_dw within w_tab_tipi_pratiche
end type
type dw_tab_tipi_pratiche_det from uo_cs_xx_dw within w_tab_tipi_pratiche
end type
end forward

global type w_tab_tipi_pratiche from w_cs_xx_principale
int Width=2876
int Height=1297
boolean TitleBar=true
string Title="Tabella Tipi Pratiche"
dw_tab_tipi_pratiche_lista dw_tab_tipi_pratiche_lista
dw_tab_tipi_pratiche_det dw_tab_tipi_pratiche_det
end type
global w_tab_tipi_pratiche w_tab_tipi_pratiche

on w_tab_tipi_pratiche.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_tipi_pratiche_lista=create dw_tab_tipi_pratiche_lista
this.dw_tab_tipi_pratiche_det=create dw_tab_tipi_pratiche_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_tipi_pratiche_lista
this.Control[iCurrent+2]=dw_tab_tipi_pratiche_det
end on

on w_tab_tipi_pratiche.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_tipi_pratiche_lista)
destroy(this.dw_tab_tipi_pratiche_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_tipi_pratiche_lista.set_dw_key("cod_azienda")
dw_tab_tipi_pratiche_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_tipi_pratiche_det.set_dw_options(sqlca,dw_tab_tipi_pratiche_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_tipi_pratiche_lista
end event

type dw_tab_tipi_pratiche_lista from uo_cs_xx_dw within w_tab_tipi_pratiche
int X=23
int Y=21
int Width=2789
int Height=461
string DataObject="d_tab_tipi_pratiche_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

type dw_tab_tipi_pratiche_det from uo_cs_xx_dw within w_tab_tipi_pratiche
int X=23
int Y=501
int Width=2789
int Height=661
int TabOrder=2
string DataObject="d_tab_tipi_pratiche_det"
end type

