﻿$PBExportHeader$w_tes_pratiche.srw
$PBExportComments$finestra testate pratiche
forward
global type w_tes_pratiche from w_cs_xx_principale
end type
type dw_tes_pratiche_det from uo_cs_xx_dw within w_tes_pratiche
end type
type cb_reset from commandbutton within w_tes_pratiche
end type
type cb_ricerca from commandbutton within w_tes_pratiche
end type
type dw_tes_pratiche_lista from uo_cs_xx_dw within w_tes_pratiche
end type
type dw_folder_search from u_folder within w_tes_pratiche
end type
type dw_ricerca from u_dw_search within w_tes_pratiche
end type
type cb_operazione from commandbutton within w_tes_pratiche
end type
type cb_dettagli from commandbutton within w_tes_pratiche
end type
end forward

global type w_tes_pratiche from w_cs_xx_principale
integer x = 59
integer y = 320
integer width = 3168
integer height = 2076
string title = "Gestione Pratiche"
dw_tes_pratiche_det dw_tes_pratiche_det
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_tes_pratiche_lista dw_tes_pratiche_lista
dw_folder_search dw_folder_search
dw_ricerca dw_ricerca
cb_operazione cb_operazione
cb_dettagli cb_dettagli
end type
global w_tes_pratiche w_tes_pratiche

type variables
boolean ib_delete
end variables

forward prototypes
public function integer wf_nuova_vers (long fl_num_versione_old, long fl_num_edizione_old, long fl_num_versione_new, long fl_num_edizione_new, long fl_anno_pratica, string fs_num_pratica, ref string fs_messaggio)
end prototypes

public function integer wf_nuova_vers (long fl_num_versione_old, long fl_num_edizione_old, long fl_num_versione_new, long fl_num_edizione_new, long fl_anno_pratica, string fs_num_pratica, ref string fs_messaggio);// wf_nuova_vers(fl_num_versione_old, fl_num_edizione_old, fl_num_versione_new, fl_num_edizione_new, fl_anno_pratica, fs_num_pratica, fs_messaggio)
// funzione che copia i documenti (dettagli) dalla vecchia versione a quella nuova

long ll_num_documenti, ll_i, ll_anno_pratica, ll_num_versione, ll_num_edizione, ll_num_versione_det, &
		ll_num_edizione_det, ll_progressivo
datetime ldt_data_blocco, ldt_oggi
double ld_num_protocollo
string ls_num_pratica, ls_des_blob, ls_note, ls_flag_stato_doc, ls_flag_ultima_revisione, ls_flag_blocco, &
		 ls_cod_utente, ls_db
integer li_risposta

transaction sqlcb
blob lbl_documento

select count(anno_pratica)
into :ll_num_documenti
from det_pratiche
where cod_azienda = :s_cs_xx.cod_azienda
  and anno_pratica = :fl_anno_pratica
  and num_pratica = :fs_num_pratica
  and num_versione = :fl_num_versione_old
  and num_edizione = :fl_num_edizione_old;
  
if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore lettura dettagli " + sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode = 100 then return 0

ls_cod_utente = s_cs_xx.cod_utente
ldt_oggi = datetime(today())

  INSERT INTO tes_pratiche  
         ( cod_azienda,   
           anno_pratica,   
           num_pratica,   
           num_versione,   
           num_edizione,   
           des_pratica,   
           cod_tipo_pratica,   
           creato_il,   
           creato_da,   
           note,   
           flag_stato_doc,   
           flag_ultima_revisione,   
           flag_blocco,   
           data_blocco )  
  select  :s_cs_xx.cod_azienda,   
           :fl_anno_pratica,   
           :fs_num_pratica,   
           :fl_num_versione_new,   
           :fl_num_edizione_new,   
           des_pratica,   
           cod_tipo_pratica,   
           :ldt_oggi,   
           :s_cs_xx.cod_utente,   
           note,   
           'N',   
           'S',   
           flag_blocco,   
           data_blocco
	from tes_pratiche
	where cod_azienda = :s_cs_xx.cod_azienda 
	  and anno_pratica = :fl_anno_pratica   
     and num_pratica = :fs_num_pratica 
     and num_versione = :fl_num_versione_old  
     and num_edizione = :fl_num_edizione_old   ;

	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore inserimento tes_pratiche" + SQLCA.SQLErrText
		rollback;
		return -1
	end if

 DECLARE cu_det_prat CURSOR FOR  
  SELECT det_pratiche.anno_pratica,   
         det_pratiche.num_pratica,   
         det_pratiche.num_versione,   
         det_pratiche.num_edizione,   
         det_pratiche.num_versione_det,   
         det_pratiche.num_edizione_det,   
         det_pratiche.progressivo,   
         det_pratiche.des_blob,   
         det_pratiche.num_protocollo,   
         det_pratiche.note,   
         det_pratiche.flag_stato_doc,   
         det_pratiche.flag_ultima_revisione,   
         det_pratiche.flag_blocco,   
         det_pratiche.data_blocco   
    FROM det_pratiche  
	where cod_azienda = :s_cs_xx.cod_azienda 
	  and anno_pratica = :fl_anno_pratica   
     and num_pratica = :fs_num_pratica 
     and num_versione = :fl_num_versione_old  
     and num_edizione = :fl_num_edizione_old   ;


open cu_det_prat;

do while 0 = 0
	fetch cu_det_prat into :ll_anno_pratica,   
				:ls_num_pratica,   
				:ll_num_versione,   
				:ll_num_edizione,   
				:ll_num_versione_det,   
				:ll_num_edizione_det,   
				:ll_progressivo,   
				:ls_des_blob,   
				:ld_num_protocollo,   
				:ls_note,   
				:ls_flag_stato_doc,   
				:ls_flag_ultima_revisione,   
				:ls_flag_blocco,   
				:ldt_data_blocco; 
				
   if (sqlca.sqlcode = 100) then exit
	if sqlca.sqlcode<>0 then
		fs_messaggio = "Errore lettura dettagli  " + sqlca.sqlerrtext
		close cu_det_prat;
		return -1
	end if
	
	// Assegnare un nuovo numero di protocollo
	
	setnull(ld_num_protocollo)
	
	select max(num_protocollo)
	into :ld_num_protocollo
	from tab_protocolli
	where cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore", "Impossibile Leggere la tabella protocolli " + sqlca.sqlerrtext )
		dw_tes_pratiche_lista.set_dw_view(c_ignorechanges)
		dw_tes_pratiche_det.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return -1
	end if
	
	if (sqlca.sqlcode <> 0 ) or isnull(ld_num_protocollo) then
		ld_num_protocollo = 0
	end if
	
	ld_num_protocollo = ld_num_protocollo + 1	
	
	insert into tab_protocolli
	(cod_azienda, num_protocollo)
	values (:s_cs_xx.cod_azienda, :ld_num_protocollo);
	// messagebox("debug", string(ld_num_protocollo))
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Errore", "Impossibile Aggiornare la tabella protocolli" + sqlca.sqlerrtext)
		dw_tes_pratiche_lista.set_dw_view(c_ignorechanges)
		dw_tes_pratiche_det.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return - 1
	end if
	
// 11-07-2002 modifiche Michela: controllo l'enginetype

	ls_db = f_db()
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
	
		selectblob det_pratiche.blob
				into :lbl_documento
				from det_pratiche
			where cod_azienda = :s_cs_xx.cod_azienda 
			  and anno_pratica = :fl_anno_pratica   
			  and num_pratica = :fs_num_pratica 
			  and num_versione = :fl_num_versione_old  
			  and num_edizione = :fl_num_edizione_old   
			  and num_versione_det = :ll_num_versione_det
			  and num_edizione_det = :ll_num_edizione_det
			  and progressivo = :ll_progressivo
		using sqlcb;
		
		if sqlcb.sqlcode<>0 then
			fs_messaggio = "Errore lettura blob in det_pratiche: " + sqlcb.sqlerrtext
			close cu_det_prat;
			destroy sqlcb;
			return -1
		end if
				
		destroy sqlcb;
		
	else
	
		selectblob det_pratiche.blob
				into :lbl_documento
				from det_pratiche
			where cod_azienda = :s_cs_xx.cod_azienda 
			  and anno_pratica = :fl_anno_pratica   
			  and num_pratica = :fs_num_pratica 
			  and num_versione = :fl_num_versione_old  
			  and num_edizione = :fl_num_edizione_old   
			  and num_versione_det = :ll_num_versione_det
			  and num_edizione_det = :ll_num_edizione_det
			  and progressivo = :ll_progressivo;
		
		if sqlca.sqlcode<>0 then
			fs_messaggio = "Errore lettura blob in det_pratiche: " + sqlca.sqlerrtext
			close cu_det_prat;
			return -1
		end if
		
	end if

	
  INSERT INTO det_pratiche  
         ( cod_azienda,   
           anno_pratica,   
           num_pratica,   
           num_versione,   
           num_edizione,   
           num_versione_det,   
           num_edizione_det,   
           progressivo,   
           des_blob,   
           num_protocollo,   
           note,   
           flag_stato_doc,   
           flag_ultima_revisione,   
           flag_blocco,   
           data_blocco )  
	values (:s_cs_xx.cod_azienda,
			 :ll_anno_pratica,   
				:ls_num_pratica,   
				:fl_num_versione_new,   
				:fl_num_edizione_new,   
				:ll_num_versione_det,   
				:ll_num_edizione_det,   
				:ll_progressivo,   
				:ls_des_blob,   
				:ld_num_protocollo,   
				:ls_note,   
				:ls_flag_stato_doc,   
				:ls_flag_ultima_revisione,   
				:ls_flag_blocco,   
				:ldt_data_blocco) ;
	if sqlca.sqlcode<>0 then
		fs_messaggio = "Errore inserimento det_pratiche: " + sqlca.sqlerrtext
		close cu_det_prat;
		return -1
	end if
				
	if not isnull(lbl_documento) then
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			updateblob det_pratiche
			set blob = :lbl_documento
				where cod_azienda = :s_cs_xx.cod_azienda 
				  and anno_pratica = :fl_anno_pratica   
				  and num_pratica = :fs_num_pratica 
				  and num_versione = :fl_num_versione_new  
				  and num_edizione = :fl_num_edizione_new   
				  and num_versione_det = :ll_num_versione_det
				  and num_edizione_det = :ll_num_edizione_det
				  and progressivo = :ll_progressivo
			using sqlcb;
				  
			if sqlcb.sqlcode<>0 then
				fs_messaggio = "Errore update blob in det_pratiche: " + sqlcb.sqlerrtext
				close cu_det_prat;
				destroy sqlcb;
				return -1
			end if
						
			destroy sqlcb;
			
		else
			
			updateblob det_pratiche
			set blob = :lbl_documento
				where cod_azienda = :s_cs_xx.cod_azienda 
				  and anno_pratica = :fl_anno_pratica   
				  and num_pratica = :fs_num_pratica 
				  and num_versione = :fl_num_versione_new  
				  and num_edizione = :fl_num_edizione_new   
				  and num_versione_det = :ll_num_versione_det
				  and num_edizione_det = :ll_num_edizione_det
				  and progressivo = :ll_progressivo;
				  
			if sqlca.sqlcode<>0 then
				fs_messaggio = "Errore update blob in det_pratiche: " + sqlca.sqlerrtext
				close cu_det_prat;
				return -1
			end if
			
		end if
		
//   fine modifiche

	end if
	
loop


return 0


end function

on w_tes_pratiche.create
int iCurrent
call super::create
this.dw_tes_pratiche_det=create dw_tes_pratiche_det
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_tes_pratiche_lista=create dw_tes_pratiche_lista
this.dw_folder_search=create dw_folder_search
this.dw_ricerca=create dw_ricerca
this.cb_operazione=create cb_operazione
this.cb_dettagli=create cb_dettagli
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_pratiche_det
this.Control[iCurrent+2]=this.cb_reset
this.Control[iCurrent+3]=this.cb_ricerca
this.Control[iCurrent+4]=this.dw_tes_pratiche_lista
this.Control[iCurrent+5]=this.dw_folder_search
this.Control[iCurrent+6]=this.dw_ricerca
this.Control[iCurrent+7]=this.cb_operazione
this.Control[iCurrent+8]=this.cb_dettagli
end on

on w_tes_pratiche.destroy
call super::destroy
destroy(this.dw_tes_pratiche_det)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_tes_pratiche_lista)
destroy(this.dw_folder_search)
destroy(this.dw_ricerca)
destroy(this.cb_operazione)
destroy(this.cb_dettagli)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tes_pratiche_det, &
                 "cod_tipo_pratica", &
                 sqlca, &
                 "tab_tipi_pratiche", &
                 "cod_tipo_pratica", &
                 "des_tipo_pratica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

dw_ricerca.fu_loadcode("cod_tipo_pratica", &
                 "tab_tipi_pratiche", &
					  "cod_tipo_pratica", &
					  "des_tipo_pratica", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) ", "(Tutti)" )



end event

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[],l_objects[]

dw_tes_pratiche_lista.set_dw_key("cod_azienda")
dw_tes_pratiche_lista.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_noretrieveonopen, &
                                 c_default)
											
dw_tes_pratiche_det.set_dw_options(sqlca, &
                                dw_tes_pratiche_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)

l_criteriacolumn[1] = "anno_pratica"
l_criteriacolumn[2] = "num_pratica"
l_criteriacolumn[3] = "num_versione"
l_criteriacolumn[4] = "num_edizione"
l_criteriacolumn[5] = "des_pratica"
l_criteriacolumn[6] = "cod_tipo_pratica"
l_criteriacolumn[7] = "flag_stato_doc"
l_criteriacolumn[8] = "flag_ultima_revisione"
l_criteriacolumn[9] = "flag_blocco"
l_criteriacolumn[10] = "data_blocco"

l_searchtable[1] = "tes_pratiche"
l_searchtable[2] = "tes_pratiche"
l_searchtable[3] = "tes_pratiche"
l_searchtable[4] = "tes_pratiche"
l_searchtable[5] = "tes_pratiche"
l_searchtable[6] = "tes_pratiche"
l_searchtable[7] = "tes_pratiche"
l_searchtable[8] = "tes_pratiche"
l_searchtable[9] = "tes_pratiche"
l_searchtable[10] = "tes_pratiche"

l_searchcolumn[1] = "anno_pratica"
l_searchcolumn[2] = "num_pratica"
l_searchcolumn[3] = "num_versione"
l_searchcolumn[4] = "num_edizione"
l_searchcolumn[5] = "des_pratica"
l_searchcolumn[6] = "cod_tipo_pratica"
l_searchcolumn[7] = "flag_stato_doc"
l_searchcolumn[8] = "flag_ultima_revisione"
l_searchcolumn[9] = "flag_blocco"
l_searchcolumn[10] = "data_blocco"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_tes_pratiche_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

lw_oggetti[1] = dw_tes_pratiche_lista
dw_folder_search.fu_assigntab(2, "L. ", lw_oggetti[])
lw_oggetti[1] = dw_ricerca
lw_oggetti[2] = cb_ricerca
lw_oggetti[3] = cb_reset
dw_folder_search.fu_assigntab(1, "R. ", lw_oggetti[])
dw_folder_search.fu_foldercreate(2, 2)
dw_folder_search.fu_selecttab(1)

iuo_dw_main = dw_tes_pratiche_lista
dw_tes_pratiche_lista.change_dw_current()

dw_ricerca.setitem(1, "anno_pratica", f_anno_esercizio())
dw_ricerca.setitem(1, "flag_blocco", "N")
dw_ricerca.setitem(1, "flag_ultima_revisione", "S")

cb_operazione.enabled = false
cb_dettagli.enabled = false

ib_delete = false


end event

type dw_tes_pratiche_det from uo_cs_xx_dw within w_tes_pratiche
integer x = 23
integer y = 500
integer width = 3063
integer height = 1340
integer taborder = 60
string dataobject = "d_tes_pratiche_det"
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_db
	integer li_risposta
	
   choose case i_colname
      case "note"
			li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
			// ----- controllo lunghezza note per ASE
			if ls_db = "SYBASE_ASE" and len(i_coltext) > 255 then
				g_mb.messagebox("Attenzione", "Nota limitata a 255 caratteri")
				i_coltext = left(i_coltext, 255)
				this.setitem(this.getrow(), "note", i_coltext)
				this.settext(i_coltext)
				return 2
				pcca.error = c_fatal
			end if
			// -----  fine controllo lunghezza note per ASE
	end choose
end if


end event

type cb_reset from commandbutton within w_tes_pratiche
integer x = 2674
integer y = 360
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_buildsearch(TRUE)
dw_folder_search.fu_selecttab(2)
dw_tes_pratiche_lista.change_dw_current()
parent.triggerevent("pc_retrieve")

// cb_chiavi.enabled=true

end event

type cb_ricerca from commandbutton within w_tes_pratiche
integer x = 2286
integer y = 360
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_reset()



end event

type dw_tes_pratiche_lista from uo_cs_xx_dw within w_tes_pratiche
integer x = 229
integer y = 20
integer width = 2834
integer height = 440
integer taborder = 50
string dataobject = "d_tes_pratiche_lista"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

if this.rowcount() > 0 then
	cb_operazione.enabled = true
	cb_dettagli.enabled = true
else
	cb_operazione.enabled = false
	cb_dettagli.enabled = false
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	long ll_i
	string ls_flag_ultima_rev
	
	ll_i = getrow()
	if ll_i > 0  and (not isnull(this.getitemstring(ll_i, "num_pratica"))) then
		ls_flag_ultima_rev = getitemstring(ll_i, "flag_ultima_revisione")
		
		if ls_flag_ultima_rev = "N" then
			cb_operazione.enabled = false
		else
			cb_operazione.enabled = true
		end if
	else
		cb_operazione.enabled = false
	end if

end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then

	string ls_flag_ultima_revisione, ls_flag_modifica
	datetime ldt_oggi

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//	SELECT flag_modifica  
//	 INTO :ls_flag_modifica  
//	 FROM mansionari  
//	 where cod_azienda = :s_cs_xx.cod_azienda
//		and cod_utente = :s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_modifica = 'N'
	
if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then ls_flag_modifica = 'S'
//--- Fine modifica Giulio

	if (sqlca.sqlcode <> 0) then
		g_mb.messagebox("Errore", "Errore lettura autorizzazione nei mansionari")
		postevent("updatestart")
	end if
	
	if ls_flag_modifica = "N" then
//		messagebox("Errore", "Manca l'autorizzazione alla modifica")
		postevent("updatestart")
	end if
	

	ldt_oggi = datetime(today())
	this.setitem(this.getrow(), "creato_il", ldt_oggi)
	this.setitem(this.getrow(), "creato_da", s_cs_xx.cod_utente)
	this.setitem(this.getrow(), "anno_pratica", year(today()))
	

end if


end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	string ls_flag_modifica, ls_flag_ultima_revisione 
	
//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//	SELECT flag_modifica  
//	 INTO :ls_flag_modifica  
//	 FROM mansionari  
//	 where cod_azienda = :s_cs_xx.cod_azienda
//		and cod_utente = :s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_modifica = 'N'
	
if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then ls_flag_modifica = 'S'
//--- Fine modifica Giulio
	
	if (sqlca.sqlcode <> 0) then
		g_mb.messagebox("Errore", "Errore lettura autorizzazione nei mansionari")
		postevent("updatestart")
	end if
	
	if ls_flag_modifica = "N" then
//		messagebox("Errore", "Manca l'autorizzazione alla modifica")
		postevent("updatestart")
	end if
	
	ls_flag_ultima_revisione = getitemstring(getrow(), "flag_ultima_revisione")
	if ls_flag_ultima_revisione = "N" then
		g_mb.messagebox("Attenzione", "Questa pratica non è l'ultima revisione")
//		pcca.error = c_fatal
//		return
	end if

end if


end event

event pcd_delete;call super::pcd_delete;if i_extendmode then
	string ls_flag_elimina, ls_flag_ultima_revisione 

	//Giulio: 10/11/2011 cambio gestione privilegi mansionario
//	SELECT flag_elimina  
//	 INTO :ls_flag_elimina  
//	 FROM mansionari  
//	 where cod_azienda = :s_cs_xx.cod_azienda
//		and cod_utente = :s_cs_xx.cod_utente;

	uo_mansionario luo_mansionario
	luo_mansionario = create uo_mansionario

	ls_flag_elimina = 'N'
	
	if luo_mansionario.uof_get_privilege(luo_mansionario.elimina) then ls_flag_elimina = 'S'
	
//	if (sqlca.sqlcode <> 0) then
	if isnull(luo_mansionario.uof_get_cod_mansionario()) then
		g_mb.messagebox("Errore", "Errore lettura autorizzazione nei mansionari")
		postevent("updatestart")
	end if
	//--- Fine modifica Giulio
	
	if ls_flag_elimina = "N" then
//		messagebox("Errore", "Manca l'autorizzazione alla cancellazione")
		postevent("updatestart")
	end if

	ib_delete = true

end if



end event

event updatestart;call super::updatestart;// ripristino versione/ edizione precedente: non funziona sempre, per cui si è lasciato il campo flag_ultima_versione aggiornabile

string ls_flag_elimina, ls_flag_ultima_revisione, ls_num_pratica, ls_flag_modifica
long ll_anno_pratica, ll_num_edizione, ll_num_versione, ll_num_ed_prec, ll_num_ver_prec, ll_prova, ll_i, ll_j

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//	SELECT flag_elimina, flag_modifica
//	 INTO :ls_flag_elimina, :ls_flag_modifica  
//	 FROM mansionari  
//	 where cod_azienda = :s_cs_xx.cod_azienda
//		and cod_utente = :s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_modifica = 'N'
ls_flag_elimina = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then ls_flag_modifica = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.elimina) then ls_flag_elimina = 'S'
//--- Fine modifica Giulio

	if (sqlca.sqlcode <> 0) then
		g_mb.messagebox("Errore", "Errore lettura autorizzazione nei mansionari")
		dw_tes_pratiche_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return 1
	end if

	if ls_flag_modifica = "N" then
		g_mb.messagebox("Errore", "Manca l'autorizzazione alle modifiche")
		dw_tes_pratiche_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return 1
	end if

ll_j = this.deletedcount()
if ll_j > 0 then	// controlli autorizzazioni cancellazione
	if ls_flag_elimina = "N" then
		g_mb.messagebox("Errore", "Manca l'autorizzazione alla cancellazione")
		dw_tes_pratiche_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return 1
	end if
end if	

if ll_j > 0 then	// controlli preventivi cancellazione
	for ll_i = 1 to ll_j
			
		ls_flag_ultima_revisione = getitemstring(ll_i, "flag_ultima_revisione", delete!, true)
		if ls_flag_ultima_revisione = "N" then
			g_mb.messagebox("Errore", "Cancellazione ammessa solo sull'ultima revisione")
			dw_tes_pratiche_lista.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if
		
	//	// cerco l'ultima versione/edizione
		ll_anno_pratica =  getitemnumber(ll_i, "anno_pratica", delete!, true)
		ls_num_pratica =  getitemstring(ll_i, "num_pratica", delete!, true)
		ll_num_edizione =  getitemnumber(ll_i, "num_edizione", delete!, true) 
		ll_num_versione =  getitemnumber(ll_i, "num_versione", delete!, true)
		
		ll_num_ver_prec = ll_num_versione - 1
		ll_num_ed_prec = ll_num_edizione - 1
		
		setnull(ll_prova)
		select max(num_edizione)
		into :ll_prova
		from tes_pratiche
		where cod_azienda = :s_cs_xx.cod_azienda
		  and anno_pratica =:ll_anno_pratica
		  and num_pratica = :ls_num_pratica
		  and num_versione = :ll_num_versione
		  and num_edizione <= :ll_num_ed_prec;
		
		
		if sqlca.sqlcode = 0 and (not isnull(ll_prova)) then  // ultima release: stessa versione, edizione -1
			
//			messagebox("debug 1", "ediz. prec. stessa vers. " + string(ll_num_versione) + " / " + string(ll_prova))
			select num_edizione
			into :ll_prova
			from tes_pratiche
			where cod_azienda = :s_cs_xx.cod_azienda
			  and anno_pratica =:ll_anno_pratica
			  and num_pratica = :ls_num_pratica
			  and num_versione = :ll_num_versione
			  and num_edizione = :ll_prova;
		
			if sqlca.sqlcode = 0 then
				update tes_pratiche
				set flag_ultima_revisione = 'S'
				where cod_azienda = :s_cs_xx.cod_azienda
				  and anno_pratica =:ll_anno_pratica
				  and num_pratica = :ls_num_pratica
				  and num_versione = :ll_num_versione
				  and num_edizione = :ll_prova;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Errore", "Errore ripristino ultima edizione " + sqlca.sqlerrtext)
					dw_tes_pratiche_lista.set_dw_view(c_ignorechanges)
					pcca.error = c_fatal
					return 1
				end if
				
//				messagebox("debug 2", "vers. prec " + string(ll_num_versione) + " / " + string(ll_prova))
			end if	
		else	// ultima release: versione -1, edizione  da trovare
			setnull(ll_prova)
			
			select max(num_versione)
			into :ll_prova
			from tes_pratiche
			where cod_azienda = :s_cs_xx.cod_azienda
		  	  and anno_pratica =:ll_anno_pratica
			  and num_pratica = :ls_num_pratica
			  and num_versione <= :ll_num_ver_prec;
			
			if sqlca.sqlcode = 0 and (not isnull(ll_prova)) then  // c' e' una versione precedente
//				messagebox("debug 3", " vers. prec" + string(ll_prova) )
				ll_num_ver_prec = ll_prova
				select max(num_edizione)	// determino edizione
				into :ll_num_ed_prec
				from tes_pratiche
				where  cod_azienda = :s_cs_xx.cod_azienda
		  		  and anno_pratica =:ll_anno_pratica
				  and num_pratica = :ls_num_pratica
				  and num_versione = :ll_num_ver_prec;
				
				if sqlca.sqlcode = 0 then  				// verifico
					select num_edizione
					into :ll_prova
					from tes_pratiche
					where  cod_azienda = :s_cs_xx.cod_azienda
		  		     and anno_pratica =:ll_anno_pratica
					  and num_pratica = :ls_num_pratica
					  and num_versione = :ll_num_ver_prec
					  and num_edizione = :ll_num_ed_prec;
					if sqlca.sqlcode = 0 then	// update
//						messagebox("debug 4", "vers. prec " + string(ll_num_ver_prec) + " / " + string(ll_num_ed_prec))
						update tes_pratiche
						set flag_ultima_revisione = 'S'
						where  cod_azienda = :s_cs_xx.cod_azienda
						  and anno_pratica =:ll_anno_pratica
						  and num_pratica = :ls_num_pratica
						  and num_versione = :ll_num_ver_prec
						  and num_edizione = :ll_num_ed_prec;
						if sqlca.sqlcode <> 0 then
							g_mb.messagebox("Errore", "Errore ripristino ultima revisione" + sqlca.sqlerrtext)
							dw_tes_pratiche_lista.set_dw_view(c_ignorechanges)
							pcca.error = c_fatal
							return 1
						end if
					end if	// fine update
				end if // fine verifico
			end if	// fine c' e' una versione precedente
		end if	// fine	else
	
	next
	
end if	// if ll_j > 0




end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_db
	integer li_risposta
	
   choose case i_colname
      case "note"
			li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
			// ----- controllo lunghezza note per ASE
			if ls_db = "SYBASE_ASE" and len(i_coltext) > 255 then
				g_mb.messagebox("Attenzione", "Nota limitata a 255 caratteri")
				i_coltext = left(i_coltext, 255)
				this.setitem(this.getrow(), "note", i_coltext)
				this.settext(i_coltext)
				return 2
				pcca.error = c_fatal
			end if
			// -----  fine controllo lunghezza note per ASE
	end choose
end if


end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if pcca.error = c_success then
      cb_operazione.enabled = true
      cb_dettagli.enabled = true
	else
		cb_operazione.enabled = false
      cb_operazione.enabled = true
	end if
	if ib_delete = true then
		cb_reset.postevent("clicked")
		ib_delete = false
	end if
end if
end event

type dw_folder_search from u_folder within w_tes_pratiche
integer x = 23
integer y = 20
integer width = 3063
integer height = 460
integer taborder = 40
end type

type dw_ricerca from u_dw_search within w_tes_pratiche
integer x = 229
integer y = 40
integer width = 2834
integer height = 420
integer taborder = 30
string dataobject = "d_tes_pratiche_search"
end type

type cb_operazione from commandbutton within w_tes_pratiche
integer x = 2309
integer y = 1860
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Operazione"
end type

event clicked;long ll_num_edizione_new, ll_num_versione_new, ll_num_edizione_old, ll_num_versione_old, ll_return, &
	  ll_anno_pratica
boolean ib_modificato
string ls_num_pratica, ls_messaggio
datetime ldt_oggi

ldt_oggi = datetime(today())

ib_modificato = false
s_cs_xx.parametri.parametro_s_1 = dw_tes_pratiche_det.getitemstring(dw_tes_pratiche_det.getrow(),"flag_stato_doc")
s_cs_xx.parametri.parametro_s_2 = dw_tes_pratiche_det.getitemstring(dw_tes_pratiche_det.getrow(),"flag_ultima_revisione")
ll_num_versione_old = dw_tes_pratiche_det.getitemnumber(dw_tes_pratiche_det.getrow(),"num_versione")
ll_num_edizione_old = dw_tes_pratiche_det.getitemnumber(dw_tes_pratiche_det.getrow(),"num_edizione")
s_cs_xx.parametri.parametro_ul_1 = ll_num_edizione_old
s_cs_xx.parametri.parametro_ul_2 = ll_num_versione_old 
ll_anno_pratica = dw_tes_pratiche_det.getitemnumber(dw_tes_pratiche_det.getrow(),"anno_pratica")
ls_num_pratica  = dw_tes_pratiche_det.getitemstring(dw_tes_pratiche_det.getrow(),"num_pratica")

s_cs_xx.parametri.parametro_ul_3 = ll_anno_pratica
s_cs_xx.parametri.parametro_s_3 = ls_num_pratica 

window_open(w_operazioni_pratica, 0)

if not isnull(s_cs_xx.parametri.parametro_s_1) then
	if s_cs_xx.parametri.parametro_s_1 = "V" then	//Verifico
		dw_tes_pratiche_det.setitem(dw_tes_pratiche_det.getrow(), "flag_stato_doc", "V")
		dw_tes_pratiche_det.setitem(dw_tes_pratiche_det.getrow(), "verificato_da", s_cs_xx.cod_utente)
		dw_tes_pratiche_det.setitem(dw_tes_pratiche_det.getrow(), "verificato_il", ldt_oggi)
		ib_modificato = true
	end if
	if s_cs_xx.parametri.parametro_s_1 = "A" then	//Approvo
		dw_tes_pratiche_det.setitem(dw_tes_pratiche_det.getrow(), "flag_stato_doc", "A")		
		dw_tes_pratiche_det.setitem(dw_tes_pratiche_det.getrow(), "approvato_da", s_cs_xx.cod_utente)
		dw_tes_pratiche_det.setitem(dw_tes_pratiche_det.getrow(), "approvato_il", ldt_oggi)
		ib_modificato = true
	end if
	if s_cs_xx.parametri.parametro_s_1 = "L" then	//Valido
		dw_tes_pratiche_det.setitem(dw_tes_pratiche_det.getrow(), "flag_stato_doc", "L")		
		dw_tes_pratiche_det.setitem(dw_tes_pratiche_det.getrow(), "validato_da", s_cs_xx.cod_utente)
		dw_tes_pratiche_det.setitem(dw_tes_pratiche_det.getrow(), "validato_il", ldt_oggi)
		ib_modificato = true
	end if
	if s_cs_xx.parametri.parametro_s_1 = "N" then	//Nuova revisione
		ll_num_edizione_new = s_cs_xx.parametri.parametro_ul_1
		ll_num_versione_new = s_cs_xx.parametri.parametro_ul_2 
		// copio tutti i documenti da versione/edizione corrente a quella nuova: 
		// wf_nuova_vers(fl_num_versione_old, fl_num_edizione_old, fl_num_versione_new, fl_num_edizione_new, fl_anno_pratica, fs_num_pratica, fs_messaggio)

		ll_return = wf_nuova_vers(ll_num_versione_old, ll_num_edizione_old, ll_num_versione_new, ll_num_edizione_new, ll_anno_pratica, ls_num_pratica, ls_messaggio)
		if ll_return = 0 then
			dw_tes_pratiche_det.setitem(dw_tes_pratiche_det.getrow(), "flag_ultima_revisione", "N")		
			ib_modificato = true
			commit;
		else
			rollback;
			g_mb.messagebox("Errore nuova versione", ls_messaggio)
			ib_modificato = false
		end if
	end if
end if

if ib_modificato = true then
	parent.postevent("pc_save")
end if

end event

type cb_dettagli from commandbutton within w_tes_pratiche
integer x = 2720
integer y = 1860
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dettagli"
end type

event clicked; window_open_parm(w_det_pratiche, -1, dw_tes_pratiche_lista)
end event

