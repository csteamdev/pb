﻿$PBExportHeader$w_det_fasi_mod_prog.srw
$PBExportComments$Modelli progetto: Attività progetti
forward
global type w_det_fasi_mod_prog from w_cs_xx_principale
end type
type dw_det_fasi_mod_prog_lista from uo_cs_xx_dw within w_det_fasi_mod_prog
end type
type dw_det_fasi_mod_prog_det from uo_cs_xx_dw within w_det_fasi_mod_prog
end type
type cb_1 from commandbutton within w_det_fasi_mod_prog
end type
end forward

global type w_det_fasi_mod_prog from w_cs_xx_principale
int Width=2044
int Height=1197
boolean TitleBar=true
string Title="Fasi Modello di Progetto"
dw_det_fasi_mod_prog_lista dw_det_fasi_mod_prog_lista
dw_det_fasi_mod_prog_det dw_det_fasi_mod_prog_det
cb_1 cb_1
end type
global w_det_fasi_mod_prog w_det_fasi_mod_prog

on w_det_fasi_mod_prog.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_fasi_mod_prog_lista=create dw_det_fasi_mod_prog_lista
this.dw_det_fasi_mod_prog_det=create dw_det_fasi_mod_prog_det
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_fasi_mod_prog_lista
this.Control[iCurrent+2]=dw_det_fasi_mod_prog_det
this.Control[iCurrent+3]=cb_1
end on

on w_det_fasi_mod_prog.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_fasi_mod_prog_lista)
destroy(this.dw_det_fasi_mod_prog_det)
destroy(this.cb_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_det_fasi_mod_prog_lista.set_dw_key("cod_azienda")
dw_det_fasi_mod_prog_lista.set_dw_key("anno_progetto")
dw_det_fasi_mod_prog_lista.set_dw_key("cod_progetto")
dw_det_fasi_mod_prog_lista.set_dw_key("num_versione")
dw_det_fasi_mod_prog_lista.set_dw_key("num_edizione")
dw_det_fasi_mod_prog_lista.set_dw_key("prog_riga_det_fasi")

dw_det_fasi_mod_prog_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_norefreshparent, &
                                    c_default)

dw_det_fasi_mod_prog_det.set_dw_options(sqlca,dw_det_fasi_mod_prog_lista,c_sharedata + c_scrollparent,c_default)

iuo_dw_main = dw_det_fasi_mod_prog_lista

end event

event pc_delete;call super::pc_delete;if dw_det_fasi_mod_prog_lista.rowcount()>0 and not(isnull(dw_det_fasi_mod_prog_lista.getitemstring(dw_det_fasi_mod_prog_lista.getrow(),"cod_mod_progetto"))) then
	cb_1.enabled=true
else
	cb_1.enabled=false
end if
end event

event pc_modify;call super::pc_modify;cb_1.enabled=false
end event

event pc_new;call super::pc_new;cb_1.enabled=false
end event

event pc_save;call super::pc_save;if dw_det_fasi_mod_prog_lista.rowcount()>0 and not(isnull(dw_det_fasi_mod_prog_lista.getitemstring(dw_det_fasi_mod_prog_lista.getrow(),"cod_mod_progetto"))) then
	cb_1.enabled=true
else
	cb_1.enabled=false
end if
end event

event pc_view;call super::pc_view;if dw_det_fasi_mod_prog_lista.rowcount()>0 and not(isnull(dw_det_fasi_mod_prog_lista.getitemstring(dw_det_fasi_mod_prog_lista.getrow(),"cod_mod_progetto"))) then
	cb_1.enabled=true
else
	cb_1.enabled=false
end if
end event

type dw_det_fasi_mod_prog_lista from uo_cs_xx_dw within w_det_fasi_mod_prog
int X=23
int Y=21
int Width=1966
int Height=481
int TabOrder=10
string DataObject="d_det_fasi_mod_prog_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_progetto,ll_num_versione,ll_num_edizione
string ls_cod_progetto

ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_mod_progetto")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_progetto)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

if l_error>0 then
	cb_1.enabled=true
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	long l_Idx,ll_anno_progetto,ll_prog_riga_det_fasi,ll_prog_riga_det_fasi_prec
	integer li_num_versione,li_num_edizione
	string ls_cod_progetto
	
	ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_mod_progetto")
  
   select max(prog_riga_det_fasi) into :ll_prog_riga_det_fasi_prec
	from det_mod_fasi_progetto
	where cod_azienda = :s_cs_xx.cod_azienda
	and cod_mod_progetto = :ls_cod_progetto;

	ll_prog_riga_det_fasi = ll_prog_riga_det_fasi_prec
	
	if isnull(ll_prog_riga_det_fasi) then
     ll_prog_riga_det_fasi = 10
   else
	  ll_prog_riga_det_fasi +=10
   end if

	//Richiesto da Diego
	ll_prog_riga_det_fasi_prec=0

	FOR l_Idx = 1 TO RowCount()
  		IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
     		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
 	 		SetItem(l_Idx, "cod_mod_progetto", ls_cod_progetto)
	 		SetItem(l_Idx, "prog_riga_det_fasi",ll_prog_riga_det_fasi)
			setitem(l_Idx,"prog_riga_det_fasi_prec",ll_prog_riga_det_fasi_prec)
  		end if
  	NEXT
end if
end event

type dw_det_fasi_mod_prog_det from uo_cs_xx_dw within w_det_fasi_mod_prog
int X=23
int Y=521
int Width=1966
int Height=461
int TabOrder=20
string DataObject="d_det_fasi_mod_prog_det"
BorderStyle BorderStyle=StyleRaised!
end type

type cb_1 from commandbutton within w_det_fasi_mod_prog
int X=1623
int Y=1001
int Width=366
int Height=81
int TabOrder=3
boolean Enabled=false
string Text="&Attività"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_det_risorse_mod_prog,-1,dw_det_fasi_mod_prog_lista)
end event

