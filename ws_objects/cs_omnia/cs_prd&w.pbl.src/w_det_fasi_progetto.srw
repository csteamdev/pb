﻿$PBExportHeader$w_det_fasi_progetto.srw
$PBExportComments$Window det_fasi_progetto
forward
global type w_det_fasi_progetto from w_cs_xx_principale
end type
type dw_det_fasi_progetto_lista from uo_cs_xx_dw within w_det_fasi_progetto
end type
type cb_1 from commandbutton within w_det_fasi_progetto
end type
type cb_documenti from commandbutton within w_det_fasi_progetto
end type
type dw_det_fasi_progetto_dett from uo_cs_xx_dw within w_det_fasi_progetto
end type
type cb_controllo_1 from commandbutton within w_det_fasi_progetto
end type
end forward

global type w_det_fasi_progetto from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2089
integer height = 1744
string title = "Fasi Progetto"
dw_det_fasi_progetto_lista dw_det_fasi_progetto_lista
cb_1 cb_1
cb_documenti cb_documenti
dw_det_fasi_progetto_dett dw_det_fasi_progetto_dett
cb_controllo_1 cb_controllo_1
end type
global w_det_fasi_progetto w_det_fasi_progetto

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_det_fasi_progetto_lista.set_dw_key("cod_azienda")
dw_det_fasi_progetto_lista.set_dw_key("anno_progetto")
dw_det_fasi_progetto_lista.set_dw_key("cod_progetto")
dw_det_fasi_progetto_lista.set_dw_key("num_versione")
dw_det_fasi_progetto_lista.set_dw_key("num_edizione")
dw_det_fasi_progetto_lista.set_dw_key("prog_riga_det_fasi")

dw_det_fasi_progetto_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_norefreshparent, &
                                    c_default)

dw_det_fasi_progetto_dett.set_dw_options(sqlca,dw_det_fasi_progetto_lista,c_sharedata + c_scrollparent,c_default)

iuo_dw_main = dw_det_fasi_progetto_lista

cb_1.enabled = false
end on

on w_det_fasi_progetto.create
int iCurrent
call super::create
this.dw_det_fasi_progetto_lista=create dw_det_fasi_progetto_lista
this.cb_1=create cb_1
this.cb_documenti=create cb_documenti
this.dw_det_fasi_progetto_dett=create dw_det_fasi_progetto_dett
this.cb_controllo_1=create cb_controllo_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_fasi_progetto_lista
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_documenti
this.Control[iCurrent+4]=this.dw_det_fasi_progetto_dett
this.Control[iCurrent+5]=this.cb_controllo_1
end on

on w_det_fasi_progetto.destroy
call super::destroy
destroy(this.dw_det_fasi_progetto_lista)
destroy(this.cb_1)
destroy(this.cb_documenti)
destroy(this.dw_det_fasi_progetto_dett)
destroy(this.cb_controllo_1)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_fasi_progetto_dett,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')"  )

end event

type dw_det_fasi_progetto_lista from uo_cs_xx_dw within w_det_fasi_progetto
integer x = 23
integer y = 20
integer width = 2011
integer height = 500
integer taborder = 10
string dataobject = "d_det_fasi_progetto_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;if i_extendmode then
	long l_Idx,ll_anno_progetto,ll_prog_riga_det_fasi,ll_prog_riga_det_fasi_prec
	integer li_num_versione,li_num_edizione
	string ls_cod_progetto
	
	cb_1.enabled = false
	cb_documenti.enabled = false

	ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_progetto")
	ll_anno_progetto = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_progetto") 
 	li_num_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione") 
	li_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione") 
  
   select max(prog_riga_det_fasi) into:ll_prog_riga_det_fasi_prec
	from det_fasi_progetto
	where cod_azienda = :s_cs_xx.cod_azienda
	and anno_progetto = :ll_anno_progetto
	and cod_progetto = :ls_cod_progetto
	and num_versione = : li_num_versione
	and num_edizione = :li_num_edizione;

	ll_prog_riga_det_fasi = ll_prog_riga_det_fasi_prec
	
	if isnull(ll_prog_riga_det_fasi) then
     ll_prog_riga_det_fasi = 10
   else
	  ll_prog_riga_det_fasi +=10
   end if


	FOR l_Idx = 1 TO RowCount()
  		IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
     		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
	      SetItem(l_Idx, "anno_progetto", ll_anno_progetto)
 	 		SetItem(l_Idx, "cod_progetto", ls_cod_progetto)
			SetItem(l_Idx, "num_versione", li_num_versione)
			SetItem(l_Idx, "num_edizione", li_num_edizione)
	 		SetItem(l_Idx, "prog_riga_det_fasi",ll_prog_riga_det_fasi)
			setitem(l_Idx,"prog_riga_det_fasi_prec",ll_prog_riga_det_fasi_prec)
  		end if
  	NEXT

end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then	

	string ls_flag_valido,ls_flag_in_prova

	ls_flag_valido = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"flag_valido")
	ls_flag_in_prova = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"flag_in_prova")

	if ls_flag_valido = "S" and ls_flag_in_prova = "N" then
		g_mb.messagebox("Omnia","Attenzione non è possibile apportare alcuna modifica al progetto in quanto è già stato autorizzato. Per fare delle modifiche creare una nuova Versione/Revisione.",exclamation!)
		dw_det_fasi_progetto_dett.triggerevent("pc_view")
		return
		
	end if

	if ls_flag_valido = "N" and ls_flag_in_prova = "N" then
		g_mb.messagebox("Omnia","Attenzione non è possibile apportare alcuna modifica al progetto in quanto è scaduto.",exclamation!)
		dw_det_fasi_progetto_dett.triggerevent("pc_view")
		return

	end if
	
	cb_1.enabled = false
	cb_documenti.enabled = false
	
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then

	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_progetto"))) then
		cb_1.enabled = true
		cb_documenti.enabled = true

	else
		cb_1.enabled = false	
		cb_documenti.enabled = false

	end if

end if
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error,ll_anno_progetto,ll_num_versione,ll_num_edizione
string ls_cod_progetto

ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_progetto")
ll_anno_progetto = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_progetto")
ll_num_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_progetto,ls_cod_progetto,ll_num_versione,ll_num_edizione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
	
	long ll_i, ll_anno_progetto,ll_prog_riga_det_fasi
	integer li_num_versione, li_num_edizione
	string ls_cod_progetto

	for ll_i = 1 to this.deletedcount()

	   ll_anno_progetto = this.getitemnumber(ll_i, "anno_progetto", delete!, true)
   	ls_cod_progetto = this.getitemstring(ll_i, "cod_progetto", delete!, true)
		ll_prog_riga_det_fasi = this.getitemnumber(ll_i, "prog_riga_det_fasi", delete!, true)
		li_num_versione = this.getitemnumber(ll_i, "num_versione", delete!, true)
	   li_num_edizione = this.getitemnumber(ll_i, "num_edizione", delete!, true)

		delete from det_risorse_progetto
		where  cod_azienda = :s_cs_xx.cod_azienda
		and 	 anno_progetto = :ll_anno_progetto
		and 	 cod_progetto = :ls_cod_progetto
		and    prog_riga_det_fasi = : ll_prog_riga_det_fasi
		and    num_versione = : li_num_versione
		and    num_edizione = : li_num_edizione;

	next

end if 
end on

event pcd_delete;call super::pcd_delete;if i_extendmode then

	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_azienda"))) then
		cb_1.enabled = true
		cb_documenti.enabled = true
	else
		cb_1.enabled = false
		cb_documenti.enabled = false
	end if

end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then

	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_progetto"))) then
		cb_1.enabled = true
		cb_documenti.enabled = true
		
	else
		cb_1.enabled = false	
		cb_documenti.enabled = false	

	end if

end if
end event

type cb_1 from commandbutton within w_det_fasi_progetto
integer x = 1669
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Attività"
end type

on clicked;window_open_parm(w_det_risorse_progetto,-1,dw_det_fasi_progetto_lista)
end on

type cb_documenti from commandbutton within w_det_fasi_progetto
integer x = 1280
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;window_open_parm(w_det_fasi_progetto_blob, -1, dw_det_fasi_progetto_lista)
end event

type dw_det_fasi_progetto_dett from uo_cs_xx_dw within w_det_fasi_progetto
integer x = 23
integer y = 540
integer width = 2011
integer height = 980
integer taborder = 30
string dataobject = "d_det_fasi_progetto_dett"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;long ll_edizione, ll_versione, ll_lista, ll_prog_lista


if i_colname <> "num_reg_lista" then
	return
end if

ll_prog_lista = getitemnumber(getrow(),"prog_liste_con_comp")

if not isnull(ll_prog_lista) then
	g_mb.messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata alla manutenzione dopo aver già eseguito la compilazione")
	return 1
end if	

ll_lista = long(i_coltext)

select max(num_edizione)
into   :ll_edizione
from   tes_liste_controllo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 num_reg_lista = :ll_lista and
		 approvato_da is not null;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
	return -1
end if

select max(num_versione)
into   :ll_versione
from   tes_liste_controllo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 num_reg_lista = :ll_lista and
		 num_edizione = :ll_edizione and
		 approvato_da is not null;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
	return -1
end if

setnull(ll_prog_lista)

setitem(getrow(),"num_edizione_lista",ll_edizione)
setitem(getrow(),"num_versione_lista",ll_versione)
setitem(getrow(),"prog_liste_con_comp",ll_prog_lista)
end event

type cb_controllo_1 from commandbutton within w_det_fasi_progetto
integer x = 891
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Controllo"
end type

event clicked;long    ll_num_edizione_lista, ll_num_versione_lista, ll_prog_liste_con_comp, ll_num_registrazione, ll_num_reg_lista_comp, &
        ll_anno_registrazione, ll_rows[],ll_anno_progetto,ll_prog_riga_det_fasi, ll_i[], ll_num_versione,ll_num_edizione
string  ls_cod_progetto

   
ll_i[1] = dw_det_fasi_progetto_lista.getrow()
ll_num_reg_lista_comp = dw_det_fasi_progetto_lista.getitemnumber(ll_i[1],"prog_liste_con_comp") 

if isnull(ll_num_reg_lista_comp) or ll_num_reg_lista_comp = 0 then
   f_crea_liste_con_comp(ll_num_versione_lista, ll_num_edizione_lista, ll_prog_liste_con_comp)
	ll_anno_progetto = dw_det_fasi_progetto_lista.getitemnumber(ll_i[1],"anno_progetto")
	ls_cod_progetto = dw_det_fasi_progetto_lista.getitemstring(ll_i[1],"cod_progetto")
	ll_prog_riga_det_fasi = dw_det_fasi_progetto_lista.getitemnumber(ll_i[1],"prog_riga_det_fasi")
	ll_num_versione = dw_det_fasi_progetto_lista.getitemnumber(ll_i[1],"num_versione")
	ll_num_edizione = dw_det_fasi_progetto_lista.getitemnumber(ll_i[1],"num_edizione")

   update det_fasi_progetto
   set    num_versione_lista = :ll_num_versione_lista,
          num_edizione_lista = :ll_num_edizione_lista,
          prog_liste_con_comp = :ll_prog_liste_con_comp
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_progetto=:ll_anno_progetto
	and    cod_progetto=:ls_cod_progetto
	and    num_versione=:ll_num_versione
	and    num_edizione=:ll_num_edizione
	and    prog_riga_det_fasi=:ll_prog_riga_det_fasi;

   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   dw_det_fasi_progetto_lista.triggerevent("pcd_retrieve")
   dw_det_fasi_progetto_lista.set_selected_rows(1, &
                                                  ll_i[], &
                                                  c_ignorechanges, &
                                                  c_refreshchildren, &
                                                  c_refreshsame)
end if

window_open_parm(w_det_liste_con_comp, 0, dw_det_fasi_progetto_lista)

end event

