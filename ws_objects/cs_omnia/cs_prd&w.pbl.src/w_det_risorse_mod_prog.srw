﻿$PBExportHeader$w_det_risorse_mod_prog.srw
$PBExportComments$Modelli progetti: risorse per attività
forward
global type w_det_risorse_mod_prog from w_cs_xx_principale
end type
type dw_det_risorse_mod_prog_lista from uo_cs_xx_dw within w_det_risorse_mod_prog
end type
type dw_det_risorse_mod_prog_det from uo_cs_xx_dw within w_det_risorse_mod_prog
end type
end forward

global type w_det_risorse_mod_prog from w_cs_xx_principale
int Width=2277
int Height=1385
boolean TitleBar=true
string Title="Attività Fasi Modelli di Progetto"
dw_det_risorse_mod_prog_lista dw_det_risorse_mod_prog_lista
dw_det_risorse_mod_prog_det dw_det_risorse_mod_prog_det
end type
global w_det_risorse_mod_prog w_det_risorse_mod_prog

event pc_setwindow;call super::pc_setwindow;dw_det_risorse_mod_prog_lista.set_dw_key("cod_azienda")
dw_det_risorse_mod_prog_lista.set_dw_key("cod_mod_progetto")
dw_det_risorse_mod_prog_lista.set_dw_key("prog_riga_det_fasi")
dw_det_risorse_mod_prog_lista.set_dw_key("prog_riga_det_risorse")

dw_det_risorse_mod_prog_lista.set_dw_options(sqlca, &
                              	    		   i_openparm, &
		                                 	   c_scrollparent, &
      		                              	c_default)

dw_det_risorse_mod_prog_det.set_dw_options(sqlca, &
														  dw_det_risorse_mod_prog_lista, &
                                            c_sharedata + c_scrollparent, c_default)

iuo_dw_main = dw_det_risorse_mod_prog_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_risorse_mod_prog_det,"cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne",&
						"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_risorse_mod_prog_det,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_risorse_mod_prog_det,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_det_risorse_mod_prog_det,"cod_risorsa_esterna",sqlca,&
                 "anag_risorse_esterne","cod_risorsa_esterna","descrizione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_det_risorse_mod_prog.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_risorse_mod_prog_lista=create dw_det_risorse_mod_prog_lista
this.dw_det_risorse_mod_prog_det=create dw_det_risorse_mod_prog_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_risorse_mod_prog_lista
this.Control[iCurrent+2]=dw_det_risorse_mod_prog_det
end on

on w_det_risorse_mod_prog.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_risorse_mod_prog_lista)
destroy(this.dw_det_risorse_mod_prog_det)
end on

type dw_det_risorse_mod_prog_lista from uo_cs_xx_dw within w_det_risorse_mod_prog
int X=23
int Y=21
int Width=2195
int Height=341
string DataObject="d_det_risorse_mod_prog_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_modify;call super::pcd_modify;if i_extendmode then	

	string ls_flag_valido,ls_flag_in_prova,ls_temp
	ls_temp = getitemstring(getrow(),"des_attivita")
	setitem(getrow(),"des_attivita"," ")
	setitem(getrow(),"des_attivita",ls_temp)

end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	string ls_flag_valido,ls_flag_in_prova
	long l_Idx,ll_anno_progetto,ll_prog_riga_det_fasi,ll_prog_riga_det_risorse
	integer li_num_versione,li_num_edizione
	string ls_cod_progetto
	
	ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_mod_progetto")
	ll_prog_riga_det_fasi = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_det_fasi")   

   select max(prog_riga_det_risorse) into :ll_prog_riga_det_risorse
	from   det_mod_risorse_progetto
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    cod_mod_progetto = :ls_cod_progetto
	and    prog_riga_det_fasi =: ll_prog_riga_det_fasi;

	if isnull(ll_prog_riga_det_risorse) then
     ll_prog_riga_det_risorse = 1
   else
	  ll_prog_riga_det_risorse++
   end if


	FOR l_Idx = 1 TO RowCount()

  		IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
     		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
 	 		SetItem(l_Idx, "cod_mod_progetto", ls_cod_progetto)
	 		SetItem(l_Idx, "prog_riga_det_fasi",ll_prog_riga_det_fasi)
			SetItem(l_Idx, "prog_riga_det_risorse",ll_prog_riga_det_risorse)
  		end if
  	
	NEXT

end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_progetto,ll_prog_riga_det_fasi,ll_num_versione,ll_num_edizione
string ls_cod_progetto

ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_mod_progetto")
ll_prog_riga_det_fasi = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_det_fasi")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_progetto,ll_prog_riga_det_fasi)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_validaterow;call super::pcd_validaterow;
	setpointer(hourglass!)
	string ls_cod_cat_risorse_esterne,ls_cod_cat_attrezzature,ls_cod_risorsa_esterna,ls_cod_attrezzatura
	long ll_anno_progetto,ll_prog_riga_det_fasi,ll_prog_riga_det_risorse,ll_costo,ll_costo_medio_orario, ll_diritto_chiamata,ll_costo_totale,ll_somma_costo
	integer li_num_versione,li_num_edizione
	double ldd_durata_teorica,ldd_durata_effettiva,ldd_ore,ldd_somma_ore,ldd_somma_durata_teorica
	string ls_cod_progetto
	datetime ld_data_inizio,ld_data_fine
	
	ldd_ore = getitemnumber(getrow(),"ore")
	ll_prog_riga_det_risorse = getitemnumber(getrow(), "prog_riga_det_risorse")   
	
	if (getitemstring(getrow(),"flag_attrezzatura_int"))='N' then
		ls_cod_cat_risorse_esterne = getitemstring(getrow(),"cod_cat_risorse_esterne")
		ls_cod_risorsa_esterna = getitemstring(getrow(),"cod_risorsa_esterna")						
		if ls_cod_cat_risorse_esterne="" or isnull(ls_cod_cat_risorse_esterne) or ls_cod_risorsa_esterna="" or isnull(ls_cod_risorsa_esterna) then
			g_mb.messagebox("Omnia","Attenzione! Non è stata selezionata la risorsa esterna", exclamation!)
			pcca.error = c_valfailed
			return
		end if
	else
		ls_cod_cat_attrezzature = getitemstring(getrow(),"cod_cat_attrezzature")
		ls_cod_attrezzatura = getitemstring(getrow(),"cod_attrezzatura")
		if ls_cod_cat_attrezzature="" or isnull(ls_cod_cat_attrezzature) or ls_cod_attrezzatura="" or isnull(ls_cod_attrezzatura)then
			g_mb.messagebox("Omnia","Attenzione! Non è stata selezionata la risorsa interna.", exclamation!)
			pcca.error = c_valfailed
			return
		end if
	end if

	setpointer(arrow!)
end event

type dw_det_risorse_mod_prog_det from uo_cs_xx_dw within w_det_risorse_mod_prog
int X=23
int Y=381
int Width=2195
int Height=881
int TabOrder=2
string DataObject="d_det_risorse_mod_prog_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;if i_extendMode then

	string ls_nl,ls_cod_cat_risorse_esterne,ls_cod_cat_attrezzature
	string ls_cod_risorsa_esterna,ls_cod_attrezzatura
	long	ll_costo_medio_orario, ll_diritto_chiamata

	choose case i_colname
		case  "cod_cat_risorse_esterne" 
			setnull(ls_nl)
			ls_cod_cat_risorse_esterne = i_coltext		
			f_PO_LoadDDDW_DW(dw_det_risorse_mod_prog_det,"cod_risorsa_esterna",sqlca,&
	      	           "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
							  "cod_azienda = '"+ s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne ='" + ls_cod_cat_risorse_esterne + "'")
			f_PO_LoadDDDW_DW(dw_det_risorse_mod_prog_det,"cod_attrezzatura",sqlca,&
	      	           "anag_attrezzature","cod_attrezzatura","descrizione", &
							  "cod_azienda = null")

			setitem(getrow(),"cod_risorsa_esterna",ls_nl)
			setitem(getrow(),"cod_cat_attrezzature",ls_nl)
			setitem(getrow(),"cod_attrezzatura",ls_nl)
			setitem(getrow(),"flag_attrezzatura_int","N")
			setitem(getrow(),"ore",0)

		case "cod_cat_attrezzature" 
			setnull(ls_nl)
			ls_cod_cat_attrezzature = i_coltext		
			f_PO_LoadDDDW_DW(dw_det_risorse_mod_prog_det,"cod_risorsa_esterna",sqlca,&
	      	           "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
							  "cod_azienda = null")
			f_PO_LoadDDDW_DW(dw_det_risorse_mod_prog_det,"cod_attrezzatura",sqlca,&
	      	           "anag_attrezzature","cod_attrezzatura","descrizione", &
							  "cod_azienda = '"+ s_cs_xx.cod_azienda + "' and cod_cat_attrezzature ='" + ls_cod_cat_attrezzature + "'")

			setitem(getrow(),"cod_attrezzatura",ls_nl)
			setitem(getrow(),"cod_cat_risorse_esterne",ls_nl)
			setitem(getrow(),"cod_risorsa_esterna",ls_nl)
			setitem(getrow(),"flag_attrezzatura_int","S")

	end choose

end if
end event

