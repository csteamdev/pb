﻿$PBExportHeader$w_det_risorse_progetto.srw
$PBExportComments$Window det_risorse_progetto
forward
global type w_det_risorse_progetto from w_cs_xx_principale
end type
type dw_det_risorse_progetto_lista from uo_cs_xx_dw within w_det_risorse_progetto
end type
type cb_2 from commandbutton within w_det_risorse_progetto
end type
type rb_straordinario from radiobutton within w_det_risorse_progetto
end type
type rb_standard from radiobutton within w_det_risorse_progetto
end type
type cbx_chiamata from checkbox within w_det_risorse_progetto
end type
type gb_1 from groupbox within w_det_risorse_progetto
end type
type dw_det_risorse_progetto_dett from uo_cs_xx_dw within w_det_risorse_progetto
end type
type cb_controllo_1 from commandbutton within w_det_risorse_progetto
end type
end forward

global type w_det_risorse_progetto from w_cs_xx_principale
integer width = 2437
integer height = 2020
string title = "Attivita Fasi Progetto"
dw_det_risorse_progetto_lista dw_det_risorse_progetto_lista
cb_2 cb_2
rb_straordinario rb_straordinario
rb_standard rb_standard
cbx_chiamata cbx_chiamata
gb_1 gb_1
dw_det_risorse_progetto_dett dw_det_risorse_progetto_dett
cb_controllo_1 cb_controllo_1
end type
global w_det_risorse_progetto w_det_risorse_progetto

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_risorse_progetto_dett,"cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne",&
						"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_risorse_progetto_dett,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_risorse_progetto_dett,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_det_risorse_progetto_dett,"cod_risorsa_esterna",sqlca,&
                 "anag_risorse_esterne","cod_risorsa_esterna","descrizione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_det_risorse_progetto_dett,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')"  )

end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_det_risorse_progetto_lista.set_dw_key("cod_azienda")
dw_det_risorse_progetto_lista.set_dw_key("anno_progetto")
dw_det_risorse_progetto_lista.set_dw_key("cod_progetto")
dw_det_risorse_progetto_lista.set_dw_key("prog_riga_det_fasi")
dw_det_risorse_progetto_lista.set_dw_key("prog_riga_det_risorse")

dw_det_risorse_progetto_lista.set_dw_options(sqlca, &
                              	    		   i_openparm, &
		                                 	   c_scrollparent, &
      		                              	c_default)

dw_det_risorse_progetto_dett.set_dw_options(sqlca, &
														  dw_det_risorse_progetto_lista, &
                                            c_sharedata + c_scrollparent, c_default)

iuo_dw_main = dw_det_risorse_progetto_lista

rb_standard.checked = true
end on

on w_det_risorse_progetto.create
int iCurrent
call super::create
this.dw_det_risorse_progetto_lista=create dw_det_risorse_progetto_lista
this.cb_2=create cb_2
this.rb_straordinario=create rb_straordinario
this.rb_standard=create rb_standard
this.cbx_chiamata=create cbx_chiamata
this.gb_1=create gb_1
this.dw_det_risorse_progetto_dett=create dw_det_risorse_progetto_dett
this.cb_controllo_1=create cb_controllo_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_risorse_progetto_lista
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.rb_straordinario
this.Control[iCurrent+4]=this.rb_standard
this.Control[iCurrent+5]=this.cbx_chiamata
this.Control[iCurrent+6]=this.gb_1
this.Control[iCurrent+7]=this.dw_det_risorse_progetto_dett
this.Control[iCurrent+8]=this.cb_controllo_1
end on

on w_det_risorse_progetto.destroy
call super::destroy
destroy(this.dw_det_risorse_progetto_lista)
destroy(this.cb_2)
destroy(this.rb_straordinario)
destroy(this.rb_standard)
destroy(this.cbx_chiamata)
destroy(this.gb_1)
destroy(this.dw_det_risorse_progetto_dett)
destroy(this.cb_controllo_1)
end on

type dw_det_risorse_progetto_lista from uo_cs_xx_dw within w_det_risorse_progetto
integer x = 23
integer y = 20
integer width = 2354
integer height = 340
integer taborder = 10
string dataobject = "d_det_risorse_progetto_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_modify;call super::pcd_modify;if i_extendmode then	

	string ls_flag_valido,ls_flag_in_prova,ls_temp
	ls_temp = getitemstring(getrow(),"des_attivita")
	setitem(getrow(),"des_attivita"," ")
	setitem(getrow(),"des_attivita",ls_temp)
	ls_flag_valido = i_parentdw.i_parentdw.getitemstring(i_parentdw.i_parentdw.i_selectedrows[1],"flag_valido")
	ls_flag_in_prova = i_parentdw.i_parentdw.getitemstring(i_parentdw.i_parentdw.i_selectedrows[1],"flag_in_prova")

	if ls_flag_valido = "S" and ls_flag_in_prova = "N" then
		g_mb.messagebox("Omnia","Attenzione non è possibile apportare alcuna modifica al progetto in quanto è già stato autorizzato. Per fare delle modifiche creare una nuova Versione/Revisione.",exclamation!)
		w_det_risorse_progetto.triggerevent("pc_view")
		return

	end if

	if ls_flag_valido = "N" and ls_flag_in_prova = "N" then
		g_mb.messagebox("Omnia","Attenzione non è possibile apportare alcuna modifica al progetto in quanto è scaduto.",exclamation!)
		w_det_risorse_progetto.triggerevent("pc_view")
		return

	end if

	cb_2.enabled = false
	rb_standard.enabled = true
	rb_straordinario.enabled = true
	cbx_chiamata.enabled = true

end if
end event

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then

	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_progetto"))) then
		cb_2.enabled = true
		rb_standard.enabled = false
		rb_straordinario.enabled = false
		cbx_chiamata.enabled = false
	
	else
		cb_2.enabled = false	
		rb_standard.enabled = false
		rb_straordinario.enabled = false
		cbx_chiamata.enabled = false
	
	end if

end if
end on

event pcd_save;call super::pcd_save;if i_extendmode then

	cb_2.enabled = true
	rb_standard.enabled = false
	rb_straordinario.enabled = false
	cbx_chiamata.enabled = false

end if
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error,ll_anno_progetto,ll_prog_riga_det_fasi,ll_num_versione,ll_num_edizione
string ls_cod_progetto

ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_progetto")
ll_anno_progetto = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_progetto")
ll_prog_riga_det_fasi = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_det_fasi")
ll_num_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
ll_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_progetto,ls_cod_progetto,ll_prog_riga_det_fasi,ll_num_versione,ll_num_edizione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

event pcd_new;call super::pcd_new;if i_extendmode then
	string ls_flag_valido,ls_flag_in_prova
	long l_Idx,ll_anno_progetto,ll_prog_riga_det_fasi,ll_prog_riga_det_risorse
	integer li_num_versione,li_num_edizione
	string ls_cod_progetto
	
	ls_flag_valido = i_parentdw.i_parentdw.getitemstring(i_parentdw.i_parentdw.i_selectedrows[1],"flag_valido")
	ls_flag_in_prova = i_parentdw.i_parentdw.getitemstring(i_parentdw.i_parentdw.i_selectedrows[1],"flag_in_prova")

	if ls_flag_valido = "S" and ls_flag_in_prova = "N" then
		g_mb.messagebox("Omnia","Attenzione non è possibile apportare alcuna modifica al progetto in quanto è già stato autorizzato. Per fare delle modifiche creare una nuova Versione/Revisione.",exclamation!)
		w_det_risorse_progetto.triggerevent("pc_view")
		return
	end if

	if ls_flag_valido = "N" and ls_flag_in_prova = "N" then
		g_mb.messagebox("Omnia","Attenzione non è possibile apportare alcuna modifica al progetto in quanto è scaduto.",exclamation!)
		w_det_risorse_progetto.triggerevent("pc_view")
		return

	end if

	ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_progetto")
	ll_anno_progetto = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_progetto") 
	ll_prog_riga_det_fasi = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_det_fasi")   
	li_num_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")   
	li_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")   
	
	cb_2.enabled = false
	rb_standard.enabled = true
	rb_straordinario.enabled = true
	cbx_chiamata.enabled = true

   select max(prog_riga_det_risorse) into:ll_prog_riga_det_risorse
	from   det_risorse_progetto
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    anno_progetto = :ll_anno_progetto
	and    cod_progetto = :ls_cod_progetto
	and	 num_versione = :li_num_versione
	and    num_edizione = :li_num_edizione
	and    prog_riga_det_fasi =: ll_prog_riga_det_fasi;

	if isnull(ll_prog_riga_det_risorse) then
     ll_prog_riga_det_risorse = 1
   else
	  ll_prog_riga_det_risorse++
   end if


	FOR l_Idx = 1 TO RowCount()

  		IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
     		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
	      SetItem(l_Idx, "anno_progetto", ll_anno_progetto)
 	 		SetItem(l_Idx, "cod_progetto", ls_cod_progetto)
			SetItem(l_Idx, "num_versione", li_num_versione)
			SetItem(l_Idx, "num_edizione", li_num_edizione)
	 		SetItem(l_Idx, "prog_riga_det_fasi",ll_prog_riga_det_fasi)
			SetItem(l_Idx, "prog_riga_det_risorse",ll_prog_riga_det_risorse)
  		end if
  	
	NEXT

end if
end event

event pcd_validaterow;call super::pcd_validaterow;
	setpointer(hourglass!)
	string ls_cod_cat_risorse_esterne,ls_cod_cat_attrezzature,ls_cod_risorsa_esterna,ls_cod_attrezzatura
	long ll_anno_progetto,ll_prog_riga_det_fasi,ll_prog_riga_det_risorse,ll_costo,ll_costo_medio_orario, ll_diritto_chiamata,ll_costo_totale,ll_somma_costo
	integer li_num_versione,li_num_edizione
	double ldd_durata_teorica,ldd_durata_effettiva,ldd_ore,ldd_somma_ore,ldd_somma_durata_teorica
	string ls_cod_progetto
	datetime ld_data_inizio,ld_data_fine
	
	ldd_ore = getitemnumber(getrow(),"ore")
	ll_prog_riga_det_risorse = getitemnumber(getrow(), "prog_riga_det_risorse")   
	
	if (getitemstring(getrow(),"flag_attrezzatura_int"))='N' then
		ls_cod_cat_risorse_esterne = getitemstring(getrow(),"cod_cat_risorse_esterne")
		ls_cod_risorsa_esterna = getitemstring(getrow(),"cod_risorsa_esterna")						
		if ls_cod_cat_risorse_esterne="" or isnull(ls_cod_cat_risorse_esterne) or ls_cod_risorsa_esterna="" or isnull(ls_cod_risorsa_esterna) then
			g_mb.messagebox("Omnia","Attenzione! Non è stata selezionata la risorsa esterna", exclamation!)
			pcca.error = c_valfailed
			return
		end if
		if rb_standard.checked = true then
	
			select tariffa_std,
					 diritto_chiamata
			into	 :ll_costo_medio_orario,	
				    :ll_diritto_chiamata
			from	 anag_risorse_esterne
			where  cod_azienda = :s_cs_xx.cod_azienda
			and	 cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne
			and    cod_risorsa_esterna = :ls_cod_risorsa_esterna;
		
			ll_costo_totale = ll_costo_medio_orario * ldd_ore

		end if

		if rb_straordinario.checked = true then

			select tariffa_str,
					 diritto_chiamata
			into	 :ll_costo_medio_orario,	
				    :ll_diritto_chiamata
			from	 anag_risorse_esterne
			where  cod_azienda = :s_cs_xx.cod_azienda
			and	 cod_cat_risorse_esterne = :ls_cod_cat_risorse_esterne
			and    cod_risorsa_esterna = :ls_cod_risorsa_esterna;
		
			ll_costo_totale = ll_costo_medio_orario * ldd_ore

		end if

		if cbx_chiamata.checked = true then
			ll_costo_totale = ll_costo_totale + ll_diritto_chiamata
		end if
	
	else
		ls_cod_cat_attrezzature = getitemstring(getrow(),"cod_cat_attrezzature")
		ls_cod_attrezzatura = getitemstring(getrow(),"cod_attrezzatura")
		if ls_cod_cat_attrezzature="" or isnull(ls_cod_cat_attrezzature) or ls_cod_attrezzatura="" or isnull(ls_cod_attrezzatura)then
			g_mb.messagebox("Omnia","Attenzione! Non è stata selezionata la risorsa interna.", exclamation!)
			pcca.error = c_valfailed
			return
		end if
		select costo_medio_orario
		into	 :ll_costo_medio_orario
		from	 tab_cat_attrezzature
		where  cod_azienda = :s_cs_xx.cod_azienda
		and	 cod_cat_attrezzature = :ls_cod_cat_attrezzature;
		
		ll_costo_totale = ll_costo_medio_orario * ldd_ore
	
	end if

	setitem(getrow(),"costo",ll_costo_totale)	

//	ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_progetto")
//	ll_anno_progetto = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_progetto") 
//	ll_prog_riga_det_fasi = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_det_fasi")   
//	li_num_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")   
//	li_num_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")   
//
//   select sum(durata_teorica)
//	into   :ldd_somma_durata_teorica
//	from   det_risorse_progetto
//	where  cod_azienda = :s_cs_xx.cod_azienda
//	and    anno_progetto = :ll_anno_progetto
//	and    cod_progetto = :ls_cod_progetto
//	and	 num_versione = :li_num_versione
//	and    num_edizione = :li_num_edizione
//	and    prog_riga_det_fasi = :ll_prog_riga_det_fasi
//	and 	 prog_riga_det_risorse <> :ll_prog_riga_det_risorse;
//	
//	ldd_somma_durata_teorica =	ldd_durata_teorica + (ldd_ore)/24
//
//	i_parentdw.setitem(i_parentdw.getrow(),"durata_totale_teorica",ldd_somma_durata_teorica)
//	i_parentdw.resetupdate()
//
//   UPDATE det_fasi_progetto  
//   SET    durata_totale_teorica = :ldd_somma_durata_teorica
//   WHERE  cod_azienda = :s_cs_xx.cod_azienda 
//	AND    anno_progetto = :ll_anno_progetto 
//   AND    cod_progetto = :ls_cod_progetto 
//	AND    num_versione = :li_num_versione
//	AND    num_edizione = :li_num_edizione
//	AND    prog_riga_det_fasi = :ll_prog_riga_det_fasi;
//
//   select sum(durata_teorica),
//			 sum(ore),
//			 sum(costo),
//			 min(data_inizio),
//			 max(data_fine)
//   into   :ldd_somma_durata_teorica,
//			 :ldd_somma_ore,
//			 :ll_somma_costo,	
//			 :ld_data_inizio,
//			 :ld_data_fine
//	from   det_risorse_progetto
//	where  cod_azienda = :s_cs_xx.cod_azienda
//	and    anno_progetto = :ll_anno_progetto
//	and    cod_progetto = :ls_cod_progetto
//	and	 num_versione = :li_num_versione
//	and    num_edizione = :li_num_edizione
//	and 	 prog_riga_det_fasi <> :ll_prog_riga_det_fasi
//	and 	 prog_riga_det_risorse <> :ll_prog_riga_det_risorse;
//	
//	ldd_somma_durata_teorica =	ldd_somma_durata_teorica + (ldd_ore)/24
//	ll_somma_costo = ll_somma_costo + ll_costo_totale
//	ldd_somma_ore = ldd_somma_ore + ldd_ore
//	
//   UPDATE tes_progetti
//   SET    durata_teorica = :ldd_somma_durata_teorica,   
//			 ore = :ldd_somma_ore,	
//			 costo = :ll_somma_costo
//   WHERE  cod_azienda = :s_cs_xx.cod_azienda 
//	AND    anno_progetto = :ll_anno_progetto 
//   AND    cod_progetto = :ls_cod_progetto 
//	AND    num_versione = :li_num_versione
//	AND    num_edizione = :li_num_edizione;
//
//	i_parentdw.i_parentdw.setitem(i_parentdw.i_parentdw.getrow(),"durata_teorica",ldd_somma_durata_teorica)	
//	i_parentdw.i_parentdw.setitem(i_parentdw.i_parentdw.getrow(),"ore",ldd_somma_ore)
//	i_parentdw.i_parentdw.setitem(i_parentdw.i_parentdw.getrow(),"costo",ll_somma_costo)	
//	i_parentdw.i_parentdw.resetupdate()
	setpointer(arrow!)
end event

type cb_2 from commandbutton within w_det_risorse_progetto
integer x = 2011
integer y = 1820
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;window_open_parm(w_det_risorse_progetto_blob,-1,dw_det_risorse_progetto_lista)
end event

type rb_straordinario from radiobutton within w_det_risorse_progetto
integer x = 389
integer y = 1800
integer width = 425
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Straordinario"
end type

type rb_standard from radiobutton within w_det_risorse_progetto
integer x = 46
integer y = 1800
integer width = 320
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Standard"
end type

type cbx_chiamata from checkbox within w_det_risorse_progetto
integer x = 846
integer y = 1800
integer width = 498
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Diritto Chiamata"
end type

type gb_1 from groupbox within w_det_risorse_progetto
integer x = 23
integer y = 1720
integer width = 1349
integer height = 180
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Costo Attrezzatura Esterna"
end type

type dw_det_risorse_progetto_dett from uo_cs_xx_dw within w_det_risorse_progetto
integer x = 23
integer y = 380
integer width = 2354
integer height = 1320
integer taborder = 20
string dataobject = "d_det_risorse_progetto_dett"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendMode then

	string ls_nl,ls_cod_cat_risorse_esterne,ls_cod_cat_attrezzature
	string ls_cod_risorsa_esterna,ls_cod_attrezzatura
	long	ll_costo_medio_orario, ll_diritto_chiamata, ll_edizione, ll_versione, ll_lista, ll_prog_lista
	datetime ldt_nl

	choose case i_colname
		case  "cod_cat_risorse_esterne" 
			setnull(ls_nl)
			ls_cod_cat_risorse_esterne = i_coltext		
			f_PO_LoadDDDW_DW(dw_det_risorse_progetto_dett,"cod_risorsa_esterna",sqlca,&
	      	           "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
							  "cod_azienda = '"+ s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne ='" + ls_cod_cat_risorse_esterne + "'")

			setitem(getrow(),"cod_risorsa_esterna",ls_nl)
			setitem(getrow(),"cod_cat_attrezzature",ls_nl)
			setitem(getrow(),"cod_attrezzatura",ls_nl)
			setitem(getrow(),"flag_attrezzatura_int","N")
			setitem(getrow(),"ore",0)
			setitem(getrow(),"costo",0)

		case "cod_cat_attrezzature" 
			setnull(ls_nl)
			ls_cod_cat_attrezzature = i_coltext		
			f_PO_LoadDDDW_DW(dw_det_risorse_progetto_dett,"cod_attrezzatura",sqlca,&
	      	           "anag_attrezzature","cod_attrezzatura","descrizione", &
							  "cod_azienda = '"+ s_cs_xx.cod_azienda + "' and cod_cat_attrezzature ='" + ls_cod_cat_attrezzature + "'")

			setitem(getrow(),"cod_attrezzatura",ls_nl)
			setitem(getrow(),"cod_cat_risorse_esterne",ls_nl)
			setitem(getrow(),"cod_risorsa_esterna",ls_nl)
			setitem(getrow(),"flag_attrezzatura_int","S")
			setitem(getrow(),"ore",0)
			setitem(getrow(),"costo",0)

		case "flag_iniziata" 
			setnull(ldt_nl)
			if i_coltext='S' then
				setitem(getrow(),"iniziata_il",datetime(today(),now()))
			else
				setitem(getrow(),"iniziata_il",ldt_nl)
			end if				

		case "flag_finita"
			setnull(ldt_nl)
			if i_coltext='S' then
				setitem(getrow(),"finita_il",datetime(today(),now()))
			else
				setitem(getrow(),"finita_il",ldt_nl)
			end if
			
		case "num_reg_lista"		

		ll_prog_lista = getitemnumber(getrow(),"prog_liste_con_comp")

		if not isnull(ll_prog_lista) then
			g_mb.messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata alla manutenzione dopo aver già eseguito la compilazione")
			return 1
		end if	
	
		ll_lista = long(i_coltext)

		select max(num_edizione)
		into   :ll_edizione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 approvato_da is not null;
		 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if
	
		select max(num_versione)
		into   :ll_versione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_lista and
				 num_edizione = :ll_edizione and
				 approvato_da is not null;
		 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if

		setnull(ll_prog_lista)

		setitem(getrow(),"num_edizione_lista",ll_edizione)
		setitem(getrow(),"num_versione_lista",ll_versione)
		setitem(getrow(),"prog_liste_con_comp",ll_prog_lista)
		
	end choose

end if
end event

type cb_controllo_1 from commandbutton within w_det_risorse_progetto
integer x = 1623
integer y = 1820
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Controllo"
end type

event clicked;long    ll_num_edizione_lista, ll_num_versione_lista, ll_prog_liste_con_comp, ll_num_registrazione, ll_num_reg_lista_comp, &
        ll_anno_registrazione, ll_rows[],ll_anno_progetto,ll_prog_riga_det_fasi, ll_i[], & 
		  ll_num_versione,ll_num_edizione,ll_prog_riga_det_risorse
string  ls_cod_progetto

   
ll_i[1] = dw_det_risorse_progetto_lista.getrow()
ll_num_reg_lista_comp = dw_det_risorse_progetto_lista.getitemnumber(ll_i[1],"prog_liste_con_comp") 

if isnull(ll_num_reg_lista_comp) or ll_num_reg_lista_comp = 0 then
   f_crea_liste_con_comp(ll_num_versione_lista, ll_num_edizione_lista, ll_prog_liste_con_comp)
	ll_anno_progetto = dw_det_risorse_progetto_lista.getitemnumber(ll_i[1],"anno_progetto")
	ls_cod_progetto = dw_det_risorse_progetto_lista.getitemstring(ll_i[1],"cod_progetto")
	ll_prog_riga_det_fasi = dw_det_risorse_progetto_lista.getitemnumber(ll_i[1],"prog_riga_det_fasi")
	ll_prog_riga_det_risorse = dw_det_risorse_progetto_lista.getitemnumber(ll_i[1],"prog_riga_det_risorse")
	ll_num_versione = dw_det_risorse_progetto_lista.getitemnumber(ll_i[1],"num_versione")
	ll_num_edizione = dw_det_risorse_progetto_lista.getitemnumber(ll_i[1],"num_edizione")

   update det_risorse_progetto
   set    num_versione_lista = :ll_num_versione_lista,
          num_edizione_lista = :ll_num_edizione_lista,
          prog_liste_con_comp = :ll_prog_liste_con_comp
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_progetto=:ll_anno_progetto
	and    cod_progetto=:ls_cod_progetto
	and    num_versione=:ll_num_versione
	and    num_edizione=:ll_num_edizione
	and    prog_riga_det_fasi=:ll_prog_riga_det_fasi
	and    prog_riga_det_risorse=:ll_prog_riga_det_risorse;

   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione!", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   dw_det_risorse_progetto_lista.triggerevent("pcd_retrieve")
   dw_det_risorse_progetto_lista.set_selected_rows(1, &
                                                  ll_i[], &
                                                  c_ignorechanges, &
                                                  c_refreshchildren, &
                                                  c_refreshsame)
end if

window_open_parm(w_det_liste_con_comp, 0, dw_det_risorse_progetto_lista)

end event

