﻿$PBExportHeader$w_det_risorse_progetto_blob.srw
$PBExportComments$Window w_det_risorse_progetto_blob
forward
global type w_det_risorse_progetto_blob from w_cs_xx_principale
end type
type dw_det_risorse_progetto_blob_lista from uo_cs_xx_dw within w_det_risorse_progetto_blob
end type
type dw_det_fasi_progetto_blob_det from uo_cs_xx_dw within w_det_risorse_progetto_blob
end type
type cb_note_esterne from commandbutton within w_det_risorse_progetto_blob
end type
end forward

global type w_det_risorse_progetto_blob from w_cs_xx_principale
integer width = 2254
integer height = 1260
string title = "Documenti per risorse progetto"
dw_det_risorse_progetto_blob_lista dw_det_risorse_progetto_blob_lista
dw_det_fasi_progetto_blob_det dw_det_fasi_progetto_blob_det
cb_note_esterne cb_note_esterne
end type
global w_det_risorse_progetto_blob w_det_risorse_progetto_blob

event pc_setwindow;call super::pc_setwindow;
dw_det_risorse_progetto_blob_lista.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)
dw_det_fasi_progetto_blob_det.set_dw_options(sqlca, &
                                    dw_det_risorse_progetto_blob_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

iuo_dw_main=dw_det_risorse_progetto_blob_lista

cb_note_esterne.enabled = false

end event

on pc_delete;call w_cs_xx_principale::pc_delete;cb_note_esterne.enabled = false

end on

on w_det_risorse_progetto_blob.create
int iCurrent
call super::create
this.dw_det_risorse_progetto_blob_lista=create dw_det_risorse_progetto_blob_lista
this.dw_det_fasi_progetto_blob_det=create dw_det_fasi_progetto_blob_det
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_risorse_progetto_blob_lista
this.Control[iCurrent+2]=this.dw_det_fasi_progetto_blob_det
this.Control[iCurrent+3]=this.cb_note_esterne
end on

on w_det_risorse_progetto_blob.destroy
call super::destroy
destroy(this.dw_det_risorse_progetto_blob_lista)
destroy(this.dw_det_fasi_progetto_blob_det)
destroy(this.cb_note_esterne)
end on

type dw_det_risorse_progetto_blob_lista from uo_cs_xx_dw within w_det_risorse_progetto_blob
integer x = 23
integer y = 20
integer width = 2171
integer height = 640
integer taborder = 10
string dataobject = "d_det_risorse_progetto_blob_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_progetto")) then
      cb_note_esterne.enabled = true
   else
      cb_note_esterne.enabled = false
   end if
end if
end event

on pcd_new;call uo_cs_xx_dw::pcd_new;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end on

event pcd_setkey;call super::pcd_setkey;integer li_versione,li_edizione,li_anno_progetto
string ls_cod_progetto
long ll_progressivo,ll_i,ll_prog_riga_det_fasi,ll_prog_riga_det_risorse

ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_progetto")
li_anno_progetto = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_progetto")
li_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
li_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
ll_prog_riga_det_fasi = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_det_fasi")
ll_prog_riga_det_risorse = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_det_risorse")

select max(progr)
into   :ll_progressivo
from   det_risorse_progetto_blob
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_progetto=:li_anno_progetto
and    cod_progetto=:ls_cod_progetto
and    num_versione=:li_versione
and    num_edizione=:li_edizione
and    prog_riga_det_fasi=:ll_prog_riga_det_fasi
and    prog_riga_det_risorse=:ll_prog_riga_det_risorse;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul database:" + sqlca.sqlerrtext)
	return 
end if

if isnull(ll_progressivo) then
	ll_progressivo=1
else
	ll_progressivo++
end if

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
      this.setitem(ll_i, "cod_progetto", ls_cod_progetto)
		this.setitem(ll_i, "anno_progetto", li_anno_progetto)
		this.setitem(ll_i, "num_versione", li_versione)
		this.setitem(ll_i, "num_edizione", li_edizione)
		this.setitem(ll_i, "prog_riga_det_fasi", ll_prog_riga_det_fasi)
		this.setitem(ll_i, "prog_riga_det_risorse", ll_prog_riga_det_risorse)
		this.setitem(ll_i, "progr", ll_progressivo)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore,ll_prog_riga_det_fasi
integer li_versione,li_edizione,li_anno_progetto,ll_prog_riga_det_risorse
string ls_cod_progetto

ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_progetto")
li_anno_progetto = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_progetto")
li_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
li_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
ll_prog_riga_det_fasi = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_det_fasi")
ll_prog_riga_det_risorse = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_det_risorse")

ll_errore = retrieve(s_cs_xx.cod_azienda, li_anno_progetto,ls_cod_progetto,li_versione,li_edizione,ll_prog_riga_det_fasi,ll_prog_riga_det_risorse)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_progetto")) then
      cb_note_esterne.enabled = true
   else
      cb_note_esterne.enabled = false
   end if
end if
end event

type dw_det_fasi_progetto_blob_det from uo_cs_xx_dw within w_det_risorse_progetto_blob
integer x = 23
integer y = 680
integer width = 2171
integer height = 360
integer taborder = 20
string dataobject = "d_det_risorse_progetto_blob_det"
borderstyle borderstyle = styleraised!
end type

type cb_note_esterne from commandbutton within w_det_risorse_progetto_blob
integer x = 1829
integer y = 1060
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_progetto, ls_db
long ll_progressivo,ll_prog_riga_det_fasi
integer li_i,li_anno_progetto,li_versione,li_edizione,ll_prog_riga_det_risorse, li_risposta

transaction sqlcb;
blob lbl_null

setnull(lbl_null)

li_i = dw_det_risorse_progetto_blob_lista.getrow()

li_anno_progetto = dw_det_risorse_progetto_blob_lista.getitemnumber(li_i, "anno_progetto")
ls_cod_progetto = dw_det_risorse_progetto_blob_lista.getitemstring(li_i, "cod_progetto")
ll_progressivo = dw_det_risorse_progetto_blob_lista.getitemnumber(li_i, "progr")
li_versione = dw_det_risorse_progetto_blob_lista.getitemnumber(li_i, "num_versione")
li_edizione = dw_det_risorse_progetto_blob_lista.getitemnumber(li_i, "num_edizione")
ll_prog_riga_det_fasi = dw_det_risorse_progetto_blob_lista.getitemnumber(li_i, "prog_riga_det_fasi")
ll_prog_riga_det_risorse = dw_det_risorse_progetto_blob_lista.getitemnumber(li_i, "prog_riga_det_risorse")


ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)	
	
	selectblob blob
	into   :s_cs_xx.parametri.parametro_bl_1
	from   det_risorse_progetto_blob
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_progetto=:li_anno_progetto
	and    cod_progetto=:ls_cod_progetto
	and    num_versione=:li_versione
	and    num_edizione=:li_edizione
	and    prog_riga_det_fasi=:ll_prog_riga_det_fasi
	and    prog_riga_det_risorse=:ll_prog_riga_det_risorse
	and    progr=:ll_progressivo
	using  sqlcb;

	if sqlcb.sqlcode > 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

	if sqlcb.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlcb.sqlerrtext)
		destroy sqlcb;
		return
	end if	
	
	destroy sqlcb;
	
else

	selectblob blob
	into   :s_cs_xx.parametri.parametro_bl_1
	from   det_risorse_progetto_blob
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_progetto=:li_anno_progetto
	and    cod_progetto=:ls_cod_progetto
	and    num_versione=:li_versione
	and    num_edizione=:li_edizione
	and    prog_riga_det_fasi=:ll_prog_riga_det_fasi
	and    prog_riga_det_risorse=:ll_prog_riga_det_risorse
	and    progr=:ll_progressivo;

	if sqlca.sqlcode > 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
		return
	end if

end if
window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then

	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
		updateblob det_risorse_progetto_blob
		set    blob = :s_cs_xx.parametri.parametro_bl_1
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_progetto=:li_anno_progetto
		and    cod_progetto=:ls_cod_progetto
		and    num_versione=:li_versione
		and    num_edizione=:li_edizione
		and    prog_riga_det_fasi=:ll_prog_riga_det_fasi
		and    prog_riga_det_risorse=:ll_prog_riga_det_risorse
		and    progr=:ll_progressivo
		using  sqlcb;
	
	
		if sqlcb.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlcb.sqlerrtext)
			destroy sqlcb;
			return
		end if
		
		destroy sqlcb;
				
	else

		updateblob det_risorse_progetto_blob
		set    blob = :s_cs_xx.parametri.parametro_bl_1
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_progetto=:li_anno_progetto
		and    cod_progetto=:ls_cod_progetto
		and    num_versione=:li_versione
		and    num_edizione=:li_edizione
		and    prog_riga_det_fasi=:ll_prog_riga_det_fasi
		and    prog_riga_det_risorse=:ll_prog_riga_det_risorse
		and    progr=:ll_progressivo;
	
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
			return
		end if
		
	end if
	
   commit;
end if
end event

