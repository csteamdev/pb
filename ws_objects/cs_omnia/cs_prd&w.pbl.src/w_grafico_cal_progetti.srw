﻿$PBExportHeader$w_grafico_cal_progetti.srw
$PBExportComments$Window grafico calendario progetti
forward
global type w_grafico_cal_progetti from w_cs_xx_risposta
end type
type dw_grafico_det_cal_progetti from uo_cs_graph within w_grafico_cal_progetti
end type
type em_data_inizio from editmask within w_grafico_cal_progetti
end type
type em_data_fine from editmask within w_grafico_cal_progetti
end type
type st_1 from statictext within w_grafico_cal_progetti
end type
type st_2 from statictext within w_grafico_cal_progetti
end type
type cb_aggiorna from commandbutton within w_grafico_cal_progetti
end type
type cb_2 from uo_cb_close within w_grafico_cal_progetti
end type
end forward

global type w_grafico_cal_progetti from w_cs_xx_risposta
integer width = 3488
integer height = 1980
string title = "Giorni e Risorse per Progetto"
dw_grafico_det_cal_progetti dw_grafico_det_cal_progetti
em_data_inizio em_data_inizio
em_data_fine em_data_fine
st_1 st_1
st_2 st_2
cb_aggiorna cb_aggiorna
cb_2 cb_2
end type
global w_grafico_cal_progetti w_grafico_cal_progetti

event pc_setwindow;call super::pc_setwindow;//dw_grafico_det_cal_progetti.set_dw_options(sqlca,pcca.null_object, c_noretrieveonopen + &
//														                         c_nodelete + & 
//																						 c_nomodify + &
//																						 c_nonew + &
//																						 c_disablecc,c_default)
																						 
																						 
em_data_inizio.text="01/01/" + string(year(today()))
em_data_fine.text="31/12/" + string(year(today()))
end event

on w_grafico_cal_progetti.create
int iCurrent
call super::create
this.dw_grafico_det_cal_progetti=create dw_grafico_det_cal_progetti
this.em_data_inizio=create em_data_inizio
this.em_data_fine=create em_data_fine
this.st_1=create st_1
this.st_2=create st_2
this.cb_aggiorna=create cb_aggiorna
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_grafico_det_cal_progetti
this.Control[iCurrent+2]=this.em_data_inizio
this.Control[iCurrent+3]=this.em_data_fine
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.cb_aggiorna
this.Control[iCurrent+7]=this.cb_2
end on

on w_grafico_cal_progetti.destroy
call super::destroy
destroy(this.dw_grafico_det_cal_progetti)
destroy(this.em_data_inizio)
destroy(this.em_data_fine)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.cb_aggiorna)
destroy(this.cb_2)
end on

type dw_grafico_det_cal_progetti from uo_cs_graph within w_grafico_cal_progetti
integer x = 37
integer y = 32
integer width = 3378
integer height = 1724
integer taborder = 10
end type

type em_data_inizio from editmask within w_grafico_cal_progetti
integer x = 411
integer y = 1780
integer width = 389
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

type em_data_fine from editmask within w_grafico_cal_progetti
integer x = 1234
integer y = 1780
integer width = 389
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

type st_1 from statictext within w_grafico_cal_progetti
integer x = 69
integer y = 1800
integer width = 343
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
string text = "Data Inizio:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_grafico_cal_progetti
integer x = 914
integer y = 1800
integer width = 320
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
string text = "Data Fine:"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_aggiorna from commandbutton within w_grafico_cal_progetti
integer x = 1691
integer y = 1780
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;//dw_grafico_det_cal_progetti.triggerevent("pcd_retrieve")

datetime ld_data_inizio,ld_data_fine

ld_data_inizio = datetime(date(em_data_inizio.text),00:00:00)
ld_data_fine = datetime(date(em_data_fine.text),00:00:00)

//SEZIONE 1 ###############################################################################
//creazione e popolamento del datastore di appoggio
//N.B. potrebbe essere usato anche una datawindow al posto di un datastore, purchè
//     questo venga specificato nell'oggetto grafico (vedi più avanti SEZIONE 2)
//----------------------------------------------------------------------------------------
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_grafico_det_cal_progetti" //immettere qui il nome del datastore
lds_dati.settransobject(sqlca)
lds_dati.Retrieve(	s_cs_xx.cod_azienda,ld_data_inizio,&
						 	ld_data_fine,s_cs_xx.parametri.parametro_s_1,&
						 	s_cs_xx.parametri.parametro_d_1,&
						 	s_cs_xx.parametri.parametro_d_2,&
						 	s_cs_xx.parametri.parametro_d_3) //gli argomenti di retrieve dipendono dal datastore

//########################################################################################

//SEZIONE 2 ###############################################################################
//impostazione dell'oggetto grafico
//----------------------------------------------------------------------------------------

//si vuole usare un datastore (valore di default è TRUE, mettere FALSE se si tratta di datawindow)
//dw_grafico_det_cal_progetti è il nome dato al controllo grafico sulla finestra
dw_grafico_det_cal_progetti.ib_datastore = true

//impostare l'oggetto datastore (o datawindow: dw_grafico.idw_data )
dw_grafico_det_cal_progetti.ids_data = lds_dati

//impostazione corrispondenza campi datastore (o datawindow) e grafico
/*ESEMPIO 1
	se il datastore ha 3 campi stringa 2 datetime e 1 numerico (esempio long, decimal):

	dw_grafico.is_str[1] = "nome prima colonna del ds di tipo stringa"
	dw_grafico.is_str[2] = "nome seconda colonna del ds di tipo stringa"
	dw_grafico.is_str[3] = "nome terza colonna del ds di tipo stringa"
	dw_grafico.is_dtm[1] = "nome prima colonna del ds di tipo datetime"
	dw_grafico.is_dtm[2] = "nome seconda colonna del ds di tipo datetime"
	dw_grafico.is_num[1] = "nome prima colonna del ds di tipo long, decimal, ecc..."

N.B. non è importante l'ordine di specifica dei vari campi
*/
dw_grafico_det_cal_progetti.is_dtm[1] = "ora_inizio"
dw_grafico_det_cal_progetti.is_dtm[2] = "ora_fine"
dw_grafico_det_cal_progetti.is_dtm[3] = "data_giorno"
dw_grafico_det_cal_progetti.is_str[1] = "cod_progetto"
dw_grafico_det_cal_progetti.is_str[2] = "anag_attrezzature_descrizione"

//Questo serve per impostare l'ordinamento corretto della categoria prima di popolare il grafico
dw_grafico_det_cal_progetti.is_source_categoria_col = "data_giorno" //nome colonna del datastore che corrisponde alla categoria

//impostazione della categoria
/*ESEMPI
	se la categoria è una colonna di tipo string del datastore indicare "str_i"
	dove i è la posizione nell'array dw_grafico.is_istr[]

	se la categoria è una colonna di tipo datetime del datastore indicare "dtm_j"
	dove j è la posizione nell'array dw_grafico.is_dtm[]

	se la categoria è una colonna di tipo numerico del datastore indicare "num_k"
	dove k è la posizione nell'array dw_grafico.is_num[]
*/
dw_grafico_det_cal_progetti.is_categoria_col = "dtm_3"

//impostazione della categoria (analogo a quanto fatto per la categoria)
/*
	essa può essere una colonna numerica o una espressione numerica
	ESEMPI
		"num_2"
		"num_1 - num_2"
		"(hour(dtm_3)*60 + minute(dtm_3) - hour(dtm_2)*60 - minute(dtm_2))/60"
*/
dw_grafico_det_cal_progetti.is_exp_valore = "hour(dtm_2) - hour(dtm_1) + (minute(dtm_2) - minute(dtm_1))/60" //default è "num_1"

//impostazione della categoria (analogo a quanto fatto per la categoria)
//lasciare stringa vuota se non si vuole gestire il grafico per serie
dw_grafico_det_cal_progetti.is_serie_col = "str_2"

//altre impostazioni del grafico
dw_grafico_det_cal_progetti.is_titolo = "Giorni e Risorse per Progetto" //Titolo del grafico
dw_grafico_det_cal_progetti.is_label_categoria= "Data" //Etichetta sull'asse della categoria (Default ascissa)
dw_grafico_det_cal_progetti.is_label_valori = "Ore" //Etichetta sull'asse dei valori (Default valori)

//tipo scala: 1 se lineare, 2 se logaritmica in base 10
dw_grafico_det_cal_progetti.ii_scala = 1

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_grafico_det_cal_progetti.ii_tipo_grafico = 15 //Barre a Stack

//colore di sfondo del grafico
//per il bianco (DEFAULT)	: 16777215
//per il silver			: 12632256
dw_grafico_det_cal_progetti.il_backcolor = 16777215 //bianco

//metodo che visualizza i dati nel grafico
dw_grafico_det_cal_progetti.uof_retrieve( )

destroy lds_dati
//########################################################################################


end event

type cb_2 from uo_cb_close within w_grafico_cal_progetti
integer x = 3063
integer y = 1780
integer width = 366
integer height = 80
integer taborder = 5
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

