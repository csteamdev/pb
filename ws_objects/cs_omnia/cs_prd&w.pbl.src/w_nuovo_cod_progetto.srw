﻿$PBExportHeader$w_nuovo_cod_progetto.srw
$PBExportComments$Window nuovo codice progetto
forward
global type w_nuovo_cod_progetto from w_cs_xx_risposta
end type
type em_codice from editmask within w_nuovo_cod_progetto
end type
type st_1 from statictext within w_nuovo_cod_progetto
end type
type cb_ok from commandbutton within w_nuovo_cod_progetto
end type
type cb_annulla from commandbutton within w_nuovo_cod_progetto
end type
type em_anno from editmask within w_nuovo_cod_progetto
end type
type st_2 from statictext within w_nuovo_cod_progetto
end type
end forward

global type w_nuovo_cod_progetto from w_cs_xx_risposta
int Width=1111
int Height=465
boolean TitleBar=true
string Title="Anno e codice Nuovo Progetto"
em_codice em_codice
st_1 st_1
cb_ok cb_ok
cb_annulla cb_annulla
em_anno em_anno
st_2 st_2
end type
global w_nuovo_cod_progetto w_nuovo_cod_progetto

type variables
integer ii_ok
end variables

on w_nuovo_cod_progetto.create
int iCurrent
call w_cs_xx_risposta::create
this.em_codice=create em_codice
this.st_1=create st_1
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.em_anno=create em_anno
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=em_codice
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=cb_ok
this.Control[iCurrent+4]=cb_annulla
this.Control[iCurrent+5]=em_anno
this.Control[iCurrent+6]=st_2
end on

on w_nuovo_cod_progetto.destroy
call w_cs_xx_risposta::destroy
destroy(this.em_codice)
destroy(this.st_1)
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.em_anno)
destroy(this.st_2)
end on

event open;call super::open;ii_ok=-1

em_anno.setfocus()
end event

event closequery;call super::closequery;if ii_ok=-1 then return 1
end event

type em_codice from editmask within w_nuovo_cod_progetto
int X=481
int Y=141
int Width=572
int Height=81
int TabOrder=20
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
MaskDataType MaskDataType=StringMask!
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_nuovo_cod_progetto
int X=23
int Y=141
int Width=444
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Codice Progetto:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_ok from commandbutton within w_nuovo_cod_progetto
int X=686
int Y=261
int Width=366
int Height=81
int TabOrder=40
boolean BringToTop=true
string Text="&Ok"
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = upper(em_codice.text)
s_cs_xx.parametri.parametro_i_2 = integer(em_anno.text)
s_cs_xx.parametri.parametro_i_1 = 1

ii_ok=0

close (parent)

end event

type cb_annulla from commandbutton within w_nuovo_cod_progetto
int X=298
int Y=261
int Width=366
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 0
ii_ok = 0
close (parent)
end event

type em_anno from editmask within w_nuovo_cod_progetto
int X=481
int Y=41
int Width=572
int Height=81
int TabOrder=10
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="####"
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_nuovo_cod_progetto
int X=23
int Y=41
int Width=444
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Anno Progetto:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

