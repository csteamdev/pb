﻿$PBExportHeader$w_report_progetto.srw
$PBExportComments$Finestra report di progetto
forward
global type w_report_progetto from w_cs_xx_principale
end type
type dw_1 from uo_cs_xx_dw within w_report_progetto
end type
type cb_stampa from commandbutton within w_report_progetto
end type
end forward

global type w_report_progetto from w_cs_xx_principale
integer width = 3721
integer height = 4440
string title = "Report di Progetto"
boolean hscrollbar = true
boolean vscrollbar = true
dw_1 dw_1
cb_stampa cb_stampa
end type
global w_report_progetto w_report_progetto

on w_report_progetto.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.cb_stampa=create cb_stampa
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.cb_stampa
end on

on w_report_progetto.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.cb_stampa)
end on

event pc_setwindow;call super::pc_setwindow;dw_1.ib_dw_report = true

set_w_options(c_closenosave+c_autoposition)
dw_1.Object.DataWindow.Zoom =100
dw_1.settransobject(sqlca)
dw_1.retrieve(s_cs_xx.cod_azienda, &
                     s_cs_xx.parametri.parametro_i_1, &
							s_cs_xx.parametri.parametro_s_1, &
							s_cs_xx.parametri.parametro_ul_3, &
							s_cs_xx.parametri.parametro_ul_2)
end event

type dw_1 from uo_cs_xx_dw within w_report_progetto
integer x = 23
integer y = 120
integer width = 3566
integer height = 4120
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_report_progetto"
boolean livescroll = true
end type

type cb_stampa from commandbutton within w_report_progetto
integer x = 23
integer y = 20
integer width = 366
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long Job

Job = PrintOpen( )
PrintDataWindow(job, dw_1) 
PrintClose(job)
end event

