﻿$PBExportHeader$w_scelta_mod_progetto.srw
$PBExportComments$Scelta del modello da utilizzare da per nuovo progetto in w_tes_progetti
forward
global type w_scelta_mod_progetto from w_cs_xx_risposta
end type
type dw_mod_prog_lista from uo_cs_xx_dw within w_scelta_mod_progetto
end type
type cb_conferma from commandbutton within w_scelta_mod_progetto
end type
type cb_annulla from commandbutton within w_scelta_mod_progetto
end type
type st_1 from statictext within w_scelta_mod_progetto
end type
end forward

global type w_scelta_mod_progetto from w_cs_xx_risposta
int Width=2460
int Height=1285
boolean TitleBar=true
string Title="Scelta Modello di Progetto"
dw_mod_prog_lista dw_mod_prog_lista
cb_conferma cb_conferma
cb_annulla cb_annulla
st_1 st_1
end type
global w_scelta_mod_progetto w_scelta_mod_progetto

on w_scelta_mod_progetto.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_mod_prog_lista=create dw_mod_prog_lista
this.cb_conferma=create cb_conferma
this.cb_annulla=create cb_annulla
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_mod_prog_lista
this.Control[iCurrent+2]=cb_conferma
this.Control[iCurrent+3]=cb_annulla
this.Control[iCurrent+4]=st_1
end on

on w_scelta_mod_progetto.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_mod_prog_lista)
destroy(this.cb_conferma)
destroy(this.cb_annulla)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_mod_prog_lista.set_dw_key("cod_azienda")
dw_mod_prog_lista.set_dw_key("cod_mod_progetto")
dw_mod_prog_lista.set_dw_options(sqlca,pcca.null_object, c_retrieveonopen+c_nonew+c_nomodify+c_nodelete,c_default)

//iuo_dw_main = dw_mod_prog_lista
end event

type dw_mod_prog_lista from uo_cs_xx_dw within w_scelta_mod_progetto
int X=23
int Y=141
int Width=2378
int Height=921
int TabOrder=20
string DataObject="d_tes_mod_prog_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_conferma from commandbutton within w_scelta_mod_progetto
int X=2035
int Y=1081
int Width=366
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="&Conferma"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;integer li_riga

li_riga=dw_mod_prog_lista.getselectedrow(0)
s_cs_xx.parametri.parametro_s_1=dw_mod_prog_lista.getitemstring(li_riga,"cod_mod_progetto")
s_cs_xx.parametri.parametro_i_1 = 1
close(parent)

end event

type cb_annulla from commandbutton within w_scelta_mod_progetto
int X=1651
int Y=1081
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 0

close (parent)
end event

type st_1 from statictext within w_scelta_mod_progetto
int X=23
int Y=21
int Width=2378
int Height=101
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Selezionare un modello di progetto"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-12
int Weight=700
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

