﻿$PBExportHeader$w_tes_mod_prog.srw
$PBExportComments$Modelli progetto:progetti
forward
global type w_tes_mod_prog from w_cs_xx_principale
end type
type dw_tes_mod_prog_lista from uo_cs_xx_dw within w_tes_mod_prog
end type
type st_2 from statictext within w_tes_mod_prog
end type
type cb_2 from cb_prod_ricerca within w_tes_mod_prog
end type
type dw_tes_mod_prog_det from uo_cs_xx_dw within w_tes_mod_prog
end type
type cb_fasi from commandbutton within w_tes_mod_prog
end type
end forward

global type w_tes_mod_prog from w_cs_xx_principale
integer width = 2638
integer height = 1144
string title = "Modelli di Progetto"
dw_tes_mod_prog_lista dw_tes_mod_prog_lista
st_2 st_2
cb_2 cb_2
dw_tes_mod_prog_det dw_tes_mod_prog_det
cb_fasi cb_fasi
end type
global w_tes_mod_prog w_tes_mod_prog

on w_tes_mod_prog.create
int iCurrent
call super::create
this.dw_tes_mod_prog_lista=create dw_tes_mod_prog_lista
this.st_2=create st_2
this.cb_2=create cb_2
this.dw_tes_mod_prog_det=create dw_tes_mod_prog_det
this.cb_fasi=create cb_fasi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_mod_prog_lista
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.dw_tes_mod_prog_det
this.Control[iCurrent+5]=this.cb_fasi
end on

on w_tes_mod_prog.destroy
call super::destroy
destroy(this.dw_tes_mod_prog_lista)
destroy(this.st_2)
destroy(this.cb_2)
destroy(this.dw_tes_mod_prog_det)
destroy(this.cb_fasi)
end on

event pc_setwindow;call super::pc_setwindow;dw_tes_mod_prog_lista.set_dw_key("cod_azienda")
dw_tes_mod_prog_lista.set_dw_key("cod_mod_progetto")
dw_tes_mod_prog_lista.set_dw_options(sqlca,pcca.null_object, c_retrieveonopen,c_default)
dw_tes_mod_prog_det.set_dw_options(sqlca,dw_tes_mod_prog_lista,c_sharedata + c_scrollparent,c_default)

iuo_dw_main = dw_tes_mod_prog_lista
end event

event pc_view;call super::pc_view;cb_2.enabled=false
if dw_tes_mod_prog_lista.rowcount()>0 and not(isnull(dw_tes_mod_prog_lista.getitemstring(dw_tes_mod_prog_lista.getrow(),"cod_mod_progetto"))) then
	cb_fasi.enabled=true
end if
end event

event pc_modify;call super::pc_modify;cb_2.enabled=true
cb_fasi.enabled=false
end event

event pc_new;call super::pc_new;cb_2.enabled=true
cb_fasi.enabled=false
end event

event pc_save;call super::pc_save;cb_2.enabled=false
if dw_tes_mod_prog_lista.rowcount()>0 and not(isnull(dw_tes_mod_prog_lista.getitemstring(dw_tes_mod_prog_lista.getrow(),"cod_mod_progetto"))) then
	cb_fasi.enabled=true
end if
end event

event pc_delete;call super::pc_delete;if dw_tes_mod_prog_lista.rowcount()>0 and not(isnull(dw_tes_mod_prog_lista.getitemstring(dw_tes_mod_prog_lista.getrow(),"cod_mod_progetto"))) then
	cb_fasi.enabled=true
else
	cb_fasi.enabled=false
end if
end event

type dw_tes_mod_prog_lista from uo_cs_xx_dw within w_tes_mod_prog
integer x = 23
integer y = 120
integer width = 2560
integer height = 420
integer taborder = 20
string dataobject = "d_tes_mod_prog_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,li_valido_per


FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
		
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

if l_error>0 then
	cb_fasi.enabled=true
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	if this.getrow() > 0 then
		f_PO_LoadDDDW_DW(dw_tes_mod_prog_det,"cod_versione",sqlca,&
									  "distinta_padri","cod_versione","des_versione",&
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + getitemstring(getrow(),"cod_prodotto") + "'")
	
	end if
end if

end event

type st_2 from statictext within w_tes_mod_prog
integer x = 23
integer y = 20
integer width = 2560
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long backcolor = 12632256
boolean enabled = false
string text = "ELENCO MODELLI PROGETTO"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_2 from cb_prod_ricerca within w_tes_mod_prog
integer x = 2149
integer y = 736
integer width = 73
integer height = 80
integer taborder = 10
boolean enabled = false
end type

event getfocus;call super::getfocus;dw_tes_mod_prog_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type dw_tes_mod_prog_det from uo_cs_xx_dw within w_tes_mod_prog
integer x = 23
integer y = 560
integer width = 2560
integer height = 360
integer taborder = 30
string dataobject = "d_tes_mod_prog_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_colname ="cod_prodotto" then
	string ls_cod_versione
	
	f_PO_LoadDDDW_DW(dw_tes_mod_prog_det,"cod_versione",sqlca,&
							  "distinta_padri","cod_versione","des_versione",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext + "'")
			
				select cod_versione
				into   :ls_cod_versione
				from   distinta_padri
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_prodotto = :i_coltext
				and    flag_predefinita = 'S';
				if sqlca.sqlcode = 0 then setitem(row,"cod_versione",ls_cod_versione)

end if
end event

event ue_key;call super::ue_key;if this.getcolumnname()="cod_prodotto" then
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_tes_mod_prog_det,"cod_prodotto")
		end if
	end if
end event

type cb_fasi from commandbutton within w_tes_mod_prog
integer x = 2217
integer y = 940
integer width = 366
integer height = 80
integer taborder = 3
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Fasi "
end type

event clicked;window_open_parm(w_det_fasi_mod_prog,-1,dw_tes_mod_prog_lista)
end event

