﻿$PBExportHeader$w_tes_progetti.srw
$PBExportComments$Window tes_progetti
forward
global type w_tes_progetti from w_cs_xx_principale
end type
type cb_fasi from commandbutton within w_tes_progetti
end type
type rb_approva from radiobutton within w_tes_progetti
end type
type rb_valida from radiobutton within w_tes_progetti
end type
type rb_autorizza from radiobutton within w_tes_progetti
end type
type rb_nuova_edizione from radiobutton within w_tes_progetti
end type
type rb_approvati from radiobutton within w_tes_progetti
end type
type rb_autorizzati from radiobutton within w_tes_progetti
end type
type rb_validi from radiobutton within w_tes_progetti
end type
type rb_scaduti from radiobutton within w_tes_progetti
end type
type rb_elimina from radiobutton within w_tes_progetti
end type
type rb_tutti from radiobutton within w_tes_progetti
end type
type rb_da_approvare from radiobutton within w_tes_progetti
end type
type cbx_approvazione from checkbox within w_tes_progetti
end type
type cbx_autorizzazione from checkbox within w_tes_progetti
end type
type cbx_validazione from checkbox within w_tes_progetti
end type
type cbx_lettura from checkbox within w_tes_progetti
end type
type cbx_modifica from checkbox within w_tes_progetti
end type
type cbx_elimina from checkbox within w_tes_progetti
end type
type gb_1 from groupbox within w_tes_progetti
end type
type gb_2 from groupbox within w_tes_progetti
end type
type cb_controllo from commandbutton within w_tes_progetti
end type
type dw_tes_progetti_lista from uo_cs_xx_dw within w_tes_progetti
end type
type st_1 from statictext within w_tes_progetti
end type
type em_1 from editmask within w_tes_progetti
end type
type st_2 from statictext within w_tes_progetti
end type
type st_3 from statictext within w_tes_progetti
end type
type st_4 from statictext within w_tes_progetti
end type
type st_5 from statictext within w_tes_progetti
end type
type st_6 from statictext within w_tes_progetti
end type
type cb_schedula_tutti from commandbutton within w_tes_progetti
end type
type cb_grafici from commandbutton within w_tes_progetti
end type
type cb_esegui from commandbutton within w_tes_progetti
end type
type gb_3 from groupbox within w_tes_progetti
end type
type cb_schedula_nuovi from commandbutton within w_tes_progetti
end type
type cb_elimina_sc from commandbutton within w_tes_progetti
end type
type cb_documenti from commandbutton within w_tes_progetti
end type
type cb_duplica from commandbutton within w_tes_progetti
end type
type cb_report from commandbutton within w_tes_progetti
end type
type dw_tes_progetti_dett_1 from uo_cs_xx_dw within w_tes_progetti
end type
type dw_folder from u_folder within w_tes_progetti
end type
type dw_tes_progetti_dett_3 from uo_cs_xx_dw within w_tes_progetti
end type
type dw_tes_progetti_dett_2 from uo_cs_xx_dw within w_tes_progetti
end type
type cb_calcola from commandbutton within w_tes_progetti
end type
type cb_nuovo_da_mod from commandbutton within w_tes_progetti
end type
end forward

global type w_tes_progetti from w_cs_xx_principale
integer width = 3282
integer height = 1824
string title = "Progetti"
cb_fasi cb_fasi
rb_approva rb_approva
rb_valida rb_valida
rb_autorizza rb_autorizza
rb_nuova_edizione rb_nuova_edizione
rb_approvati rb_approvati
rb_autorizzati rb_autorizzati
rb_validi rb_validi
rb_scaduti rb_scaduti
rb_elimina rb_elimina
rb_tutti rb_tutti
rb_da_approvare rb_da_approvare
cbx_approvazione cbx_approvazione
cbx_autorizzazione cbx_autorizzazione
cbx_validazione cbx_validazione
cbx_lettura cbx_lettura
cbx_modifica cbx_modifica
cbx_elimina cbx_elimina
gb_1 gb_1
gb_2 gb_2
cb_controllo cb_controllo
dw_tes_progetti_lista dw_tes_progetti_lista
st_1 st_1
em_1 em_1
st_2 st_2
st_3 st_3
st_4 st_4
st_5 st_5
st_6 st_6
cb_schedula_tutti cb_schedula_tutti
cb_grafici cb_grafici
cb_esegui cb_esegui
gb_3 gb_3
cb_schedula_nuovi cb_schedula_nuovi
cb_elimina_sc cb_elimina_sc
cb_documenti cb_documenti
cb_duplica cb_duplica
cb_report cb_report
dw_tes_progetti_dett_1 dw_tes_progetti_dett_1
dw_folder dw_folder
dw_tes_progetti_dett_3 dw_tes_progetti_dett_3
dw_tes_progetti_dett_2 dw_tes_progetti_dett_2
cb_calcola cb_calcola
cb_nuovo_da_mod cb_nuovo_da_mod
end type
global w_tes_progetti w_tes_progetti

type variables
string is_tipo_operazione
//      NP = Nuovo Progetto
//      AP = Approva
//      AU = Autorizza  
//      VA = Valida
//      NU = Nuova Revisione
//      MO = Modifica
//      EL = Elimina

string is_tipo_visualizzazione
//      AP = Approvati
//      AU = Autorizzati
//      VA = Validi
//      SC = Scaduti
//      TU = Tutti

string is_flag_valido
string is_flag_in_prova

string is_flag_approvazione
string is_flag_autorizzazione
string is_flag_validazione
string is_flag_lettura
string is_flag_modifica
string is_flag_elimina
string is_flag_nuovo
string is_cod_progetto 
long il_anno_progetto
long il_num_versione 
long il_num_edizione
string is_nuova_revisione // S o N
integer ii_nuovo
end variables

forward prototypes
public function integer wf_autorizza ()
public function integer wf_approva ()
public function integer wf_valida ()
public function integer wf_imposta_rb ()
public function integer wf_lettura ()
public function integer wf_elimina (ref string fs_errore)
public function integer wf_nuovo_prog_da_mod (string fs_cod_mod, integer fi_anno, string fs_cod_prog, integer fi_n_vers, integer fi_n_ed, ref string fs_errore)
public function integer wf_aggiorna_progetti ()
public function integer wf_duplica_progetti (integer fi_anno_progetto, string fs_cod_progetto, integer fi_anno_progetto_nuovo, string fs_cod_progetto_nuovo, integer fi_num_versione, integer fi_num_edizione, ref string fs_errore)
public function integer wf_nuova_revisione (integer anno_progetto, string cod_progetto, ref string fs_errore)
end prototypes

public function integer wf_autorizza ();//	Window Function di autorizzazione progetto
//
// nome: wf_autorizza
// tipo: integer
//	ritorno: 0 execution passed
//  		  -1 execution failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//				
//
//
//		Creata il 06-12-96 
//		Autore Diego Ferrari
//

string ls_cod_progetto,ls_flag_valido,ls_flag_in_prova
long ll_anno_progetto,ll_num_versione,ll_num_edizione,ll_num_versione_precedente,ll_num_edizione_precedente
datetime ld_oggi

if dw_tes_progetti_lista.rowcount() = 0 then return -1

ls_cod_progetto = dw_tes_progetti_lista.getitemstring(dw_tes_progetti_lista.getrow(),"cod_progetto")
ll_anno_progetto = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"anno_progetto")
ll_num_versione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_versione")
ll_num_edizione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_edizione")

ld_oggi = datetime(today())

select num_versione,
		 num_edizione
into   :ll_num_versione_precedente,
		 :ll_num_edizione_precedente
from	 tes_progetti
WHERE  cod_azienda   = :s_cs_xx.cod_azienda 
AND  	 anno_progetto = :ll_anno_progetto
and    cod_progetto  = :ls_cod_progetto
AND    flag_valido ='S';

if sqlca.sqlcode = 100 then
	UPDATE tes_progetti
	SET    flag_valido = 'S',
   	    flag_in_prova = 'N',
			 autorizzato_da = :s_cs_xx.cod_utente,
	       autorizzato_il = :ld_oggi,
			 valido_per = 1
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND  	 anno_progetto = :ll_anno_progetto
	and    cod_progetto  = :ls_cod_progetto
	AND    num_versione  = :ll_num_versione 
	AND    num_edizione  = :ll_num_edizione;

	if sqlca.sqlcode <> 0 then
		rollback;
   	return -1
	end if

	if sqlca.sqlcode <> 0 then
		rollback;
   	return -1
	end if

	commit;

	dw_tes_progetti_lista.setitem(dw_tes_progetti_lista.getrow(),"autorizzato_da",s_cs_xx.cod_utente)
	dw_tes_progetti_lista.setitem(dw_tes_progetti_lista.getrow(),"autorizzato_il",ld_oggi)
	dw_tes_progetti_lista.setitem(dw_tes_progetti_lista.getrow(),"valido_per",1)
	dw_tes_progetti_lista.resetupdate()

else
	UPDATE tes_progetti
	SET    flag_valido = 'N',
	       flag_in_prova = 'N'
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND  	 anno_progetto = :ll_anno_progetto
	and    cod_progetto  = :ls_cod_progetto
	AND    num_versione  = :ll_num_versione_precedente 
	AND    num_edizione  = :ll_num_edizione_precedente;
	
	if sqlca.sqlcode <> 0 then
   	return -1
	end if

	UPDATE tes_progetti
	SET    flag_valido = 'S' ,
   	    flag_in_prova = 'N',
			 autorizzato_da = :s_cs_xx.cod_utente,
	       autorizzato_il = :ld_oggi
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND  	 anno_progetto = :ll_anno_progetto
	and    cod_progetto  = :ls_cod_progetto
	AND    num_versione  = :ll_num_versione 
	AND    num_edizione  = :ll_num_edizione;

	if sqlca.sqlcode <> 0 then
   	return -1
	end if

	dw_tes_progetti_lista.setitem(dw_tes_progetti_lista.getrow(),"autorizzato_da",s_cs_xx.cod_utente)
	dw_tes_progetti_lista.setitem(dw_tes_progetti_lista.getrow(),"autorizzato_il",ld_oggi)
	dw_tes_progetti_lista.resetupdate()

end if

return 0
end function

public function integer wf_approva ();//	Window Function di approvazione progetto
//
// nome: wf_approva
// tipo: integer
//	ritorno: 0 execution passed
//  		  -1 execution failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//				
//
//
//		Creata il 06-12-96 
//		Autore Diego Ferrari
//
string ls_cod_progetto
long ll_anno_progetto,ll_num_versione,ll_num_edizione
datetime ld_oggi

if dw_tes_progetti_lista.rowcount() = 0 then return -1

ls_cod_progetto = dw_tes_progetti_lista.getitemstring(dw_tes_progetti_lista.getrow(),"cod_progetto")
ll_anno_progetto = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"anno_progetto")
ll_num_versione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_versione")
ll_num_edizione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_edizione")

ld_oggi = datetime(today())

UPDATE tes_progetti
SET    approvato_da = :s_cs_xx.cod_utente,
       approvato_il = :ld_oggi,
		 flag_valido = 'N',
		 flag_in_prova = 'S'
WHERE  cod_azienda = :s_cs_xx.cod_azienda 
AND  	 anno_progetto = :ll_anno_progetto
and    cod_progetto  = :ls_cod_progetto
AND    num_versione  = :ll_num_versione 
AND    num_edizione  = :ll_num_edizione;

if sqlca.sqlcode <> 0 then
   return -1
end if

dw_tes_progetti_lista.setitem(dw_tes_progetti_lista.getrow(),"approvato_da",s_cs_xx.cod_utente)
dw_tes_progetti_lista.setitem(dw_tes_progetti_lista.getrow(),"approvato_il",ld_oggi)
dw_tes_progetti_lista.resetupdate()

return 0
end function

public function integer wf_valida ();//	Window Function di validazione progetto
//
// nome: wf_valida
// tipo: integer
//	ritorno: 0 executed passed
//  		  -1 executed failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//				
//
//
//		Creata il 06-12-96 
//		Autore Diego Ferrari
//

string ls_cod_progetto
long ll_anno_progetto,ll_num_versione,ll_num_edizione
datetime ld_oggi

if dw_tes_progetti_lista.rowcount() = 0 then return -1

ls_cod_progetto = dw_tes_progetti_lista.getitemstring(dw_tes_progetti_lista.getrow(),"cod_progetto")
ll_anno_progetto = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"anno_progetto")
ll_num_versione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_versione")
ll_num_edizione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_edizione")

ld_oggi = datetime(today())

UPDATE tes_progetti
SET    validato_da = :s_cs_xx.cod_utente,
       validato_il = :ld_oggi
WHERE  cod_azienda = :s_cs_xx.cod_azienda 
AND  	 anno_progetto = :ll_anno_progetto
and    cod_progetto  = :ls_cod_progetto
AND    num_versione  = :ll_num_versione 
AND    num_edizione  = :ll_num_edizione;

if sqlca.sqlcode <> 0 then
   return -1
end if

dw_tes_progetti_lista.setitem(dw_tes_progetti_lista.getrow(),"validato_da",s_cs_xx.cod_utente)
dw_tes_progetti_lista.setitem(dw_tes_progetti_lista.getrow(),"validato_il",ld_oggi)
dw_tes_progetti_lista.resetupdate()

return 0
end function

public function integer wf_imposta_rb ();//	Window Function che imposta gli option button della window
//
// nome: wf_imposta_rb
// tipo: integer
//	ritorno: 0 execution passed
//  		  -1 execution failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//				
//
//
//		Creata il 07-12-96 
//		Autore Diego Ferrari
//

boolean lb_approva,lb_autorizza,lb_elimina,lb_modifica,lb_nuova_edizione,lb_valida,lb_lettura,lb_nuovo_progetto

rb_approva.enabled = false
rb_autorizza.enabled = false
rb_elimina.enabled = false
rb_nuova_edizione.enabled = false
rb_valida.enabled = false


lb_approva = rb_approva.checked
rb_approva.checked = false

lb_autorizza = rb_autorizza.checked
rb_autorizza.checked = false

lb_elimina = rb_elimina.checked
rb_elimina.checked = false

lb_nuova_edizione = rb_nuova_edizione.checked
rb_nuova_edizione.checked = false

lb_valida = rb_valida.checked
rb_valida.checked = false

choose case is_tipo_visualizzazione
	case "AP"
		if is_flag_autorizzazione = 'S' then
			rb_autorizza.enabled = true
			if lb_autorizza = true then rb_autorizza.checked = true
		end if
		
		if	is_flag_elimina = 'S' then
			rb_elimina.enabled = true
			if lb_elimina = true then rb_elimina.checked = true
		end if
	
 	case "AU" 
		if is_flag_validazione = 'S' then
			rb_valida.enabled = true
			if lb_valida = true then rb_valida.checked = true
		end if
	
		if	is_flag_elimina = 'S' then
			rb_elimina.enabled = true
			if lb_elimina = true then rb_elimina.checked = true
		end if	

		if is_flag_modifica = 'S' then
			rb_nuova_edizione.enabled = true
		end if

   case "VA" 	
		if	is_flag_elimina = 'S' then
			rb_elimina.enabled = true
			if lb_elimina = true then rb_elimina.checked = true
		end if	
		if is_flag_modifica = 'S' then
			rb_nuova_edizione.enabled = true
		end if

   case "SC" 
		if	is_flag_elimina = 'S' then
			rb_elimina.enabled = true
			if lb_elimina = true then rb_elimina.checked = true
		end if	

   case "TU" 
					
   case "DP" 
		if	is_flag_elimina = 'S' then
			rb_elimina.enabled = true
			if lb_elimina = true then rb_elimina.checked = true
		end if	
				
		if	is_flag_approvazione = 'S' then
			rb_approva.enabled = true
			if lb_approva = true then rb_approva.checked = true
		end if				

end choose

return 0
end function

public function integer wf_lettura ();//	Window Function di lettura
//
// nome: wf_lettura
// tipo: integer
//	ritorno: 0 execution passed
//  		  -1 execution failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//				
//
//
//		Creata il 06-12-96 
//		Autore Diego Ferrari
//
string ls_str,ls_cod_progetto
long ll_anno_progetto,ll_num_versione,ll_num_edizione,ll_num_riga

ls_cod_progetto = is_cod_progetto
ll_anno_progetto = il_anno_progetto
ll_num_versione = il_num_versione
ll_num_edizione = il_num_edizione

dw_tes_progetti_lista.setfilter("")
dw_tes_progetti_lista.filter()

choose case is_tipo_visualizzazione
	case 'AP'
		ls_str = "((not isnull(approvato_da)) and (isnull(autorizzato_da)) and (isnull(validato_da)) and (flag_valido='N') and (flag_in_prova='S'))"
					
  	case 'AU'
     	ls_str = "((not isnull(autorizzato_da)) and (isnull(validato_da)) and (flag_valido = 'S') and (flag_in_prova = 'N'))"
			
	case 'VA'
		ls_str = "((not isnull(validato_da)) and (flag_valido = 'S') and (flag_in_prova = 'N'))"

	case 'SC'
		ls_str = "((flag_valido = 'N') and (flag_in_prova = 'N'))"

	case 'TU'
		ls_str = ""

	case 'DP'
		ls_str = "((isnull(approvato_da)) and (flag_valido = 'N') and (flag_in_prova = 'S'))"
	
end choose

if ls_str = "" then
	ls_str = "(anno_progetto = " + string(em_1.text) + ")"

else
	ls_str = ls_str + " and (anno_progetto = " + string(em_1.text) + ")"

end if

dw_tes_progetti_lista.change_dw_current()
dw_tes_progetti_lista.setfilter(ls_str)
dw_tes_progetti_lista.filter()
dw_tes_progetti_lista.SetSort("anno_progetto, cod_progetto, num_versione, num_edizione") 
dw_tes_progetti_lista.sort()

if dw_tes_progetti_lista.rowcount() = 0 then
	cb_fasi.enabled = false
	cb_controllo.enabled = false

else
	cb_fasi.enabled = true
	cb_controllo.enabled = true

end if

ll_num_riga = dw_tes_progetti_lista.Find( "(cod_progetto = '" + ls_cod_progetto + "')  and (anno_progetto = " + string(ll_anno_progetto) + ") and (num_versione = " + string(ll_num_versione) + ") and (num_edizione = " + string(ll_num_edizione) + ")", 0,  dw_tes_progetti_lista.RowCount( ))
if ll_num_riga > 0 then
	dw_tes_progetti_lista.ScrollToRow(ll_num_riga)
	
else
	ll_num_riga = dw_tes_progetti_lista.find( "(cod_progetto = '" + ls_cod_progetto + "')  and (anno_progetto = " + string(ll_anno_progetto) + ")", 0,  dw_tes_progetti_lista.RowCount( ))
	if ll_num_riga > 0 then
		dw_tes_progetti_lista.ScrollToRow(ll_num_riga)
	
	else
		if dw_tes_progetti_lista.rowcount() > 0 then
			dw_tes_progetti_lista.ScrollToRow(1)

		else
			dw_tes_progetti_lista.ScrollToRow(0)
			dw_tes_progetti_lista.selectrow(0,false)

		end if
	end if
end if


return 0
end function

public function integer wf_elimina (ref string fs_errore);//	Window Function di eliminazione progetto
//
// nome: wf_elimina
// tipo: integer
//	ritorno: 0 execution passed
//  		  -1 execution failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//									fs_errore				string			riferimento
//
//
//		Creata il 06-12-96 
//		Autore Diego Ferrari
//

string ls_cod_progetto,ls_errore
long ll_num_versione,ll_num_edizione
integer li_risposta,li_anno_progetto
datetime ld_data_creazione

if dw_tes_progetti_lista.rowcount() = 0 then 
	fs_errore="Attenzione! Non vi sono progetti da eliminare"
	return -1
end if

ls_cod_progetto = dw_tes_progetti_lista.getitemstring(dw_tes_progetti_lista.getrow(),"cod_progetto")
li_anno_progetto = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"anno_progetto")
ll_num_versione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_versione")
ll_num_edizione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_edizione")


select data_creazione 
into   :ld_data_creazione
from   non_conformita
where  cod_azienda = :s_cs_xx.cod_azienda
and 	 anno_progetto = :li_anno_progetto
and 	 cod_progetto = :ls_cod_progetto
and 	 num_ver_progetto = :ll_num_versione
and 	 num_ediz_progetto = :ll_num_edizione;

if sqlca.sqlcode = 100 then

	li_risposta = f_ripristina_cal_progetti(li_anno_progetto,ls_cod_progetto,ll_num_versione,ll_num_edizione,ls_errore)

	if li_risposta = -1 then
		fs_errore=ls_errore
		return -1
	end if

	delete from det_risorse_progetto_blob
	where cod_azienda = :s_cs_xx.cod_azienda
	and 	anno_progetto = :li_anno_progetto
	and 	cod_progetto = :ls_cod_progetto
	and 	num_versione = :ll_num_versione
	and 	num_edizione = :ll_num_edizione;

	if sqlca.sqlcode <> 0 then
		fs_errore ="Attenzione! Errore durante delete det_risorse progetto, errore DB " + sqlca.sqlerrtext
   	return -1
	end if
	
	
	delete from det_risorse_progetto
	where cod_azienda = :s_cs_xx.cod_azienda
	and 	anno_progetto = :li_anno_progetto
	and 	cod_progetto = :ls_cod_progetto
	and 	num_versione = :ll_num_versione
	and 	num_edizione = :ll_num_edizione;

	if sqlca.sqlcode <> 0 then
		fs_errore ="Attenzione! Errore durante delete det_risorse progetto, errore DB " + sqlca.sqlerrtext
   	return -1
	end if

	delete from det_fasi_progetto_blob
	where cod_azienda = :s_cs_xx.cod_azienda
	and 	anno_progetto = :li_anno_progetto
	and 	cod_progetto = :ls_cod_progetto
	and 	num_versione = :ll_num_versione
	and 	num_edizione = :ll_num_edizione;

	if sqlca.sqlcode <> 0 then
		fs_errore ="Attenzione! Errore durante delete det_fasi progetto, errore DB " + sqlca.sqlerrtext
	   return -1
	end if

	delete from det_fasi_progetto
	where cod_azienda = :s_cs_xx.cod_azienda
	and 	anno_progetto = :li_anno_progetto
	and 	cod_progetto = :ls_cod_progetto
	and 	num_versione = :ll_num_versione
	and 	num_edizione = :ll_num_edizione;

	if sqlca.sqlcode <> 0 then
		fs_errore ="Attenzione! Errore durante delete det_fasi progetto, errore DB " + sqlca.sqlerrtext
	   return -1
	end if

	DELETE from tes_progetti
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND  	 anno_progetto = :li_anno_progetto
	and    cod_progetto  = :ls_cod_progetto
	AND    num_versione  = :ll_num_versione 
	AND    num_edizione  = :ll_num_edizione;

	if sqlca.sqlcode <> 0 then
		fs_errore ="Attenzione! Errore durante delete tes_progetti, errore DB " + sqlca.sqlerrtext
   	return -1
	end if

	commit;

else
	fs_errore="Attenzione! Il progetto "+ string(li_anno_progetto) + " " + ls_cod_progetto + " Versione " + string(ll_num_versione) + " revisione " + string(ll_num_edizione) + ", ha una o più non conformità, pertanto non può essere eliminato."
	return -1
	end if

return 0
end function

public function integer wf_nuovo_prog_da_mod (string fs_cod_mod, integer fi_anno, string fs_cod_prog, integer fi_n_vers, integer fi_n_ed, ref string fs_errore);//controllo le informazioni nei mansionari
string ls_cod_resp_divisione,ls_cod_area_aziendale
double ldd_costo_medio_orario,ldd_costo_totale

SELECT cod_resp_divisione,cod_area_aziendale
INTO 	 :ls_cod_resp_divisione,  
		 :ls_cod_area_aziendale
FROM 	 mansionari  
WHERE  cod_azienda = :s_cs_xx.cod_azienda 
AND    cod_utente = :s_cs_xx.cod_utente;


if sqlca.sqlcode = 100 then
	fs_errore="Attenzione! L'utente connesso non ha un mansionario abilitato, pertanto non sarà possibile creare nuovi progetti."
	return -1
end if


//mi servono i dati per completare la testata e li pesco dal modello
string ls_des_progetto,ls_cod_prod, ls_cod_ver
SELECT des_mod_progetto,   
		cod_prodotto,   
		cod_versione  
 INTO :ls_des_progetto,   
		:ls_cod_prod,   
		:ls_cod_ver  
 FROM tes_mod_progetti  
where cod_azienda=:s_cs_xx.cod_azienda 
  and cod_mod_progetto=:fs_cod_mod;
	 
if sqlca.sqlcode<>0 then
	fs_errore='Errore nella lettura dei dati del progetto.~r~nErrore SQL:'+sqlca.sqlerrtext
	return -1
end if

datetime ldt_data_oggi
//inserisco una nuova testata
//i dati a completamento mimati da pcd_new di dw_tes_progetti_lista:
//	setitem(getrow(),"anno_progetto",year(today())) questo va cambiato
//	setitem(getrow(),"cod_resp_divisione",ls_cod_resp_divisione)
//	setitem(getrow(),"cod_area_aziendale",ls_cod_area_aziendale)
//	setitem(getrow(),"emesso_da",s_cs_xx.cod_utente)
//	setitem(getrow(),"emesso_il",today())
//	dw_tes_progetti_dett_2.setitem(dw_tes_progetti_dett_2.getrow(),"data_inizio",today())
//	setitem(getrow(),"num_versione",1)
//	setitem(getrow(),"num_edizione",0)
// setitem(getrow(),"flag_valido", "N")
// setitem(getrow(),"flag_in_prova", "S")		
//	setitem(getrow(),"flag_schedulato", "N")
//i restanti sono come da valori iniziali dello schema

ldt_data_oggi=datetime(today())
  INSERT INTO tes_progetti  
         ( cod_azienda,   
           anno_progetto,   
           cod_progetto,   
           num_versione,   
           num_edizione,   
           des_progetto,   
           cod_prodotto,   
           data_inizio,   
           data_fine,   
           ore,   
           costo,   
           durata_teorica,   
           durata_effettiva,   
           cod_area_aziendale,   
           cod_resp_divisione,   
           emesso_da,   
           emesso_il,   
           approvato_da,   
           approvato_il,   
           autorizzato_da,   
           autorizzato_il,   
           valido_per,   
           validato_da,   
           validato_il,   
           flag_uso,   
           flag_recuperato,   
           flag_valido,   
           flag_in_prova,   
           scadenza_validazione,   
           modifiche_apportate,   
           note,   
           flag_schedulato,   
           data_inizio_effettiva,   
           cod_versione )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :fi_anno,   
           :fs_cod_prog,   
           :fi_n_vers,   
           :fi_n_ed,   
           :ls_des_progetto,   
           :ls_cod_prod,   
           :ldt_data_oggi,   
           null,   
           0.0,   
           0.0,   
           0.0,   
           0.0,   
           :ls_cod_area_aziendale,   
           :ls_cod_resp_divisione,   
           :s_cs_xx.cod_utente,   
           :ldt_data_oggi,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           'G',   
           'N',   
           'N',   
           'S',   
           null,   
           null,   
           null,   
           'N',   
           null,   
           :ls_cod_ver )  ;
if sqlca.sqlcode<0 then
	fs_errore="Errore nell'inserzione del nuovo progetto.~r~nErrore SQL:"+sqlca.sqlerrtext
	return -1
end if

//inserisco le fasi della testata
long ll_riga,ll_riga_prec,ll_riga_ris
string ls_des,ls_cod_cat_att,ls_cod_att,ls_cod_cat_ris_est,ls_cod_ris_est,ls_des_ris,ls_flag_int
boolean lb_finiti
double ld_ore

 DECLARE cur_det_fasi_mod CURSOR FOR  
  SELECT prog_riga_det_fasi,   
         des_fase,   
         prog_riga_det_fasi_prec  
    FROM det_mod_fasi_progetto  
   WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( cod_mod_progetto = :fs_cod_mod )   
ORDER BY prog_riga_det_fasi ASC  ;

open cur_det_fasi_mod;

DO
	fetch cur_det_fasi_mod into :ll_riga,:ls_des,:ll_riga_prec;
	if sqlca.sqlcode<0 then	
		fs_errore="Errore nell'inserzione di una fase del progetto.~r~nErrore SQL:"+sqlca.sqlerrtext
		return -1
	end if
	if sqlca.sqlcode=100 then exit
	
  INSERT INTO det_fasi_progetto  
         ( cod_azienda,   
           anno_progetto,   
           cod_progetto,   
           num_versione,   
           num_edizione,   
           prog_riga_det_fasi,   
           des_fase,   
           prog_riga_det_fasi_prec,   
           durata_totale_teorica,   
           durata_totale_effettiva,   
           num_reg_lista,   
           prog_liste_con_comp,   
           data_inizio,   
           data_fine,   
           num_versione_lista,   
           num_edizione_lista )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :fi_anno,   
           :fs_cod_prog,   
           :fi_n_vers,   
           :fi_n_ed,   
           :ll_riga,   
           :ls_des,   
           :ll_riga_prec,   
           0.0,   
           0.0,   
           null,   
           null,   
           null,   
           null,   
           0.0,   
           0.0 )  ;
	if sqlca.sqlcode<0 then	
		fs_errore="Errore nell'inserzione di una fase del progetto.~r~nErrore SQL:"+sqlca.sqlerrtext
		return -1
	end if
 //inserisco le risorse

 DECLARE cur_risorse_mod CURSOR FOR  
  SELECT prog_riga_det_risorse,   
         cod_cat_attrezzature,   
         cod_attrezzatura,   
         cod_cat_risorse_esterne,   
         cod_risorsa_esterna,   
         des_attivita,   
         flag_attrezzatura_int,   
         ore  
    FROM det_mod_risorse_progetto  
   WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( cod_mod_progetto = :fs_cod_mod ) AND  
         ( prog_riga_det_fasi = :ll_riga )   ;

	open cur_risorse_mod;
	do
		fetch cur_risorse_mod into :ll_riga_ris,:ls_cod_cat_att,:ls_cod_att, &
											:ls_cod_cat_ris_est,:ls_cod_ris_est,&
											:ls_des_ris,:ls_flag_int,:ld_ore;
		if sqlca.sqlcode<0 then
			fs_errore="Errore nell'inserzione di una risorsa del progetto.~r~nErrore SQL:"+sqlca.sqlerrtext
			return -1
		end if
		
		if sqlca.sqlcode=100 then exit
		
		if ls_flag_int = 'S' then
			select costo_medio_orario
			into	 :ldd_costo_medio_orario
			from	 tab_cat_attrezzature
			where  cod_azienda = :s_cs_xx.cod_azienda
			and	 cod_cat_attrezzature = :ls_cod_cat_att;
			
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
			
			ldd_costo_totale = ldd_costo_medio_orario * ld_ore
			
		else
		
			select tariffa_str

			into	 :ldd_costo_medio_orario
			from	 anag_risorse_esterne
			where  cod_azienda = :s_cs_xx.cod_azienda
			and	 cod_cat_risorse_esterne = :ls_cod_cat_ris_est
			and    cod_risorsa_esterna = :ls_cod_ris_est;
			
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
			
			ldd_costo_totale = ldd_costo_medio_orario * ld_ore
			
		end if			
			
	  INSERT INTO det_risorse_progetto  
			( cod_azienda,   
			  anno_progetto,   
			  cod_progetto,   
			  num_versione,   
			  num_edizione,   
			  prog_riga_det_fasi,   
			  prog_riga_det_risorse,   
			  flag_attrezzatura_int,   
			  cod_cat_risorse_esterne,   
			  cod_risorsa_esterna,   
			  cod_cat_attrezzature,   
			  cod_attrezzatura,   
			  data_inizio,   
			  data_fine,   
			  ore,   
			  costo,   
			  durata_teorica,   
			  durata_effettiva,   
			  flag_fine_progetto,   
			  num_reg_lista,   
			  prog_liste_con_comp,   
			  des_attivita,   
			  num_versione_lista,   
			  num_edizione_lista,   
			  flag_iniziata,   
			  flag_finita,   
			  iniziata_il,   
			  finita_il )  
	VALUES (:s_cs_xx.cod_azienda,   
			  :fi_anno,   
			  :fs_cod_prog,   
			  :fi_n_vers,   
			  :fi_n_ed,   
			  :ll_riga,   
			  :ll_riga_ris,   
			  :ls_flag_int,   
			  :ls_cod_cat_ris_est,
			  :ls_cod_ris_est,
			  :ls_cod_cat_att,
			  :ls_cod_att,
			  null,   
			  null,   
			  :ld_ore,   
			  :ldd_costo_totale,   
			  0.0,   
			  0.0,   
			  'N',   
			  null,   
			  null,   
			  :ls_des_ris,   
			  0.0,   
			  0.0,   
			  'N',   
			  'N',   
			  null,   
			  null)  ;

		if sqlca.sqlcode<0 then
			fs_errore="Errore nell'inserzione di una risorsa del progetto.~r~nErrore SQL:"+sqlca.sqlerrtext
			return -1
		end if
		
	loop until false
	close cur_risorse_mod;
	
	if sqlca.sqlcode<0 then exit

LOOP until false
 
close cur_det_fasi_mod;

if sqlca.sqlcode<0 then
	return -1
end if

return 0
end function

public function integer wf_aggiorna_progetti ();//	Window Function di aggiornamenti progetti
//
// nome: wf_aggiorna_progetti
// tipo: integer
//	ritorno: 0 execution passed
//  		  -1 execution failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//				
//
//
//		Creata il 11-12-96 
//		Autore Diego Ferrari
//

string ls_cod_progetto,ls_flag_valido,ls_flag_in_prova,ls_validato_da,ls_errore
long ll_anno_progetto,ll_num_versione,ll_num_edizione,ll_num_versione_precedente,ll_num_edizione_precedente
long ll_valido_per
integer li_risposta
datetime ld_oggi,ld_autorizzato_il,ld_validato_il,ld_scadenza_validazione,ld_scadenza

ld_oggi = datetime(today())

declare righe_tes_progetti cursor for 
select  anno_progetto,
		  cod_progetto,
		  num_versione,
    	  num_edizione,
		  valido_per,
		  autorizzato_il,
		  validato_il,
        scadenza_validazione
from	  tes_progetti
where	  cod_azienda = :s_cs_xx.cod_azienda
and     flag_valido ='S';

open righe_tes_progetti;

do while 1 = 1
  	fetch righe_tes_progetti into :ll_anno_progetto, :ls_cod_progetto, :ll_num_versione, :ll_num_edizione, :ll_valido_per, :ld_autorizzato_il, :ld_validato_il, :ld_scadenza_validazione;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode =-1) or (sqlca.sqlcode <> 0) then exit

	if isnull(ls_validato_da) or ls_validato_da = "" then
		if relativedate(date(ld_autorizzato_il),ll_valido_per) < date(ld_oggi) then
			is_nuova_revisione = "N"
			li_risposta = wf_nuova_revisione(ll_anno_progetto,ls_cod_progetto,ls_errore)

			UPDATE tes_progetti
			SET    flag_valido = 'N' ,
			       flag_in_prova = 'N'
			WHERE  cod_azienda = :s_cs_xx.cod_azienda 
			AND  	 anno_progetto = :ll_anno_progetto
			AND    cod_progetto  = :ls_cod_progetto
			AND    num_versione  = :ll_num_versione 
			AND    num_edizione  = :ll_num_edizione;			
		end if

	end if

loop	

close righe_tes_progetti;

return 0
end function

public function integer wf_duplica_progetti (integer fi_anno_progetto, string fs_cod_progetto, integer fi_anno_progetto_nuovo, string fs_cod_progetto_nuovo, integer fi_num_versione, integer fi_num_edizione, ref string fs_errore);//	Window Function di duplicazione dei progetti
//
// nome: wf_duplica_progetti
// tipo: integer
//	ritorno: 0 execution passed
//  		  -1 execution failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//				
//
//
//		Creata il 25-06-99 
//		Autore Diego Ferrari
//

string ls_cod_progetto,ls_des_progetto,ls_cod_prodotto,ls_cod_area_aziendale,ls_cod_resp_divisione
string ls_emesso_da,ls_approvato_da,ls_autorizzato_da,ls_validato_da,ls_flag_uso,ls_flag_recuperato
string ls_flag_valido,ls_flag_in_prova,ls_modifiche_apportate,ls_note,ls_des_fase
string ls_flag_attrezzatura_int,ls_cod_cat_risorse_esterne,ls_cod_risorsa_esterna,ls_cod_cat_attrezzature
string ls_cod_attrezzatura,ls_flag_fine_progetto,ls_des_blob,ls_flag_default
string ls_db
integer li_nmax_revisioni, li_risposta  
long ll_anno_progetto,ll_num_versione,ll_num_edizione,ll_costo,ll_valido_per,ll_prog_riga_det_fasi,ll_progr_blob
long ll_prog_riga_det_fasi_prec,ll_num_reg_lista_f,ll_prog_liste_con_comp_f,ll_num_reg_lista_r,ll_prog_liste_con_comp_r
long ll_costo_r,ll_num_nuova_edizione,ll_prog_riga_det_risorse,ll_num_nuova_versione
double ldd_ore,ldd_durata_teorica,ldd_durata_effettiva,ldd_durata_totale_teorica,ldd_durata_totale_effettiva
double ldd_ore_r,ldd_durata_teorica_r,ldd_durata_effettiva_r
datetime ld_oggi,ld_data_inizio,ld_data_fine,ld_emesso_il,ld_approvato_il,ld_autorizzato_il,ld_validato_il
datetime ld_scadenza_validazione,ld_data_inizio_r,ld_data_fine_r

transaction sqlcb;
blob lbo_blob

SELECT des_progetto,   
       cod_prodotto,   
       data_inizio,   
       data_fine,   
       ore,   
       costo,   
       durata_teorica,   
       durata_effettiva,   
       cod_area_aziendale,   
       cod_resp_divisione,   
       emesso_da,   
       emesso_il,   
       approvato_da,   
       approvato_il,   
       autorizzato_da,   
       autorizzato_il,   
       valido_per,   
       validato_da,   
       validato_il,   
       flag_uso,   
       flag_recuperato,   
       flag_valido,   
       flag_in_prova,   
       scadenza_validazione,   
       modifiche_apportate,   
       note  
INTO 	 :ls_des_progetto,   
       :ls_cod_prodotto,   
       :ld_data_inizio,   
       :ld_data_fine,   
       :ldd_ore,   
       :ll_costo,   
       :ldd_durata_teorica,   
       :ldd_durata_effettiva,   
       :ls_cod_area_aziendale,   
       :ls_cod_resp_divisione,   
       :ls_emesso_da,   
       :ld_emesso_il,   
       :ls_approvato_da,   
       :ld_approvato_il,   
       :ls_autorizzato_da,   
       :ld_autorizzato_il,   
       :ll_valido_per,   
       :ls_validato_da,   
       :ld_validato_il,   
       :ls_flag_uso,   
       :ls_flag_recuperato,   
       :ls_flag_valido,   
       :ls_flag_in_prova,   
       :ld_scadenza_validazione,   
       :ls_modifiche_apportate,   
       :ls_note  
FROM 	 tes_progetti  
WHERE  cod_azienda=:s_cs_xx.cod_azienda	
AND    anno_progetto =:fi_anno_progetto
AND	 cod_progetto =: fs_cod_progetto
AND	 num_versione =: fi_num_versione
AND 	 num_edizione =: fi_num_edizione;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
   return -1
end if

ld_oggi = datetime(today())

INSERT INTO tes_progetti  
         ( cod_azienda,   
           anno_progetto,   
           cod_progetto,   
           num_versione,   
           num_edizione,   
           des_progetto,   
           cod_prodotto,   
           data_inizio,   
           data_fine,   
           ore,   
           costo,   
           durata_teorica,   
           durata_effettiva,   
           cod_area_aziendale,   
           cod_resp_divisione,   
           emesso_da,   
           emesso_il,   
           approvato_da,   
           approvato_il,   
           autorizzato_da,   
           autorizzato_il,   
           valido_per,   
           validato_da,   
           validato_il,   
           flag_uso,   
           flag_recuperato,   
           flag_valido,   
           flag_in_prova,   
           scadenza_validazione,   
           modifiche_apportate,   
           note,   
           flag_schedulato,   
           data_inizio_effettiva)  
VALUES 	(  :s_cs_xx.cod_azienda,   
            :fi_anno_progetto_nuovo,   
            :fs_cod_progetto_nuovo,   
            1,   
            0,   
            :ls_des_progetto,   
            :ls_cod_prodotto,   
            :ld_data_inizio,   
            :ld_data_fine,   
            :ldd_ore,   
            :ll_costo,   
            null,   
            null,   
            :ls_cod_area_aziendale,   
            :ls_cod_resp_divisione,   
            :s_cs_xx.cod_utente,   
            :ld_oggi,   
            null,   
            null,   
            null,   
            null,   
            null,   
            null,   
            null,   
            null,   
            null,   
            'N',   
            'S',   
            null,   
            null,   
            :ls_note,
				'N',
				null);

if sqlca.sqlcode < 0 then
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
   return -1
end if

//****** copia tes_progetti_blob

declare righe_tes_progetti_blob cursor for 
select  progr,
		  des_blob,
		  flag_default
from	  tes_progetti_blob
where	  cod_azienda = :s_cs_xx.cod_azienda
and	  anno_progetto = :fi_anno_progetto
and     cod_progetto =:fs_cod_progetto
and 	  num_versione = :fi_num_versione
and 	  num_edizione = :fi_num_edizione;

open righe_tes_progetti_blob;

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()


do while 1 = 1
  	fetch righe_tes_progetti_blob 
	into :ll_progr_blob, 
		  :ls_des_blob, 
		  :ls_flag_default;
		  
   if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
		rollback;
		close righe_tes_progetti_blob;
		return -1
	end if
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
		selectblob blob
		into	:lbo_blob
		from  tes_progetti_blob
		where	cod_azienda = :s_cs_xx.cod_azienda
		and	anno_progetto = :fi_anno_progetto
		and   cod_progetto =:fs_cod_progetto
		and 	num_versione = :fi_num_versione
		and 	num_edizione = :fi_num_edizione
		and   progr=:ll_progr_blob
		using sqlcb;
	
		if sqlcb.sqlcode < 0 then
			fs_errore ="Errore sul DB: " + sqlcb.sqlerrtext
			rollback;
			destroy sqlcb;
			close righe_tes_progetti_blob;
			return -1
		end if		
		
		destroy sqlcb;
		
	else
	
		selectblob blob
		into	:lbo_blob
		from  tes_progetti_blob
		where	cod_azienda = :s_cs_xx.cod_azienda
		and	anno_progetto = :fi_anno_progetto
		and   cod_progetto =:fs_cod_progetto
		and 	num_versione = :fi_num_versione
		and 	num_edizione = :fi_num_edizione
		and   progr=:ll_progr_blob;
	
		if sqlca.sqlcode < 0 then
			fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
			rollback;
			close righe_tes_progetti_blob;
			return -1
		end if
		
	end if
	
	insert into tes_progetti_blob
  	       ( cod_azienda,   
				anno_progetto,   
				cod_progetto,   
				num_versione,   
				num_edizione,   
				progr,
				des_blob,
				flag_default)			
   VALUES ( :s_cs_xx.cod_azienda,   
	         :fi_anno_progetto_nuovo,   
    	      :fs_cod_progetto_nuovo,   
       	   1,   
          	0,   
	         :ll_progr_blob,   
  	         :ls_des_blob,   
        		:ls_flag_default);

	if sqlca.sqlcode < 0 then
		fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
		rollback;
		close righe_tes_progetti_blob;
		return -1
	end if

	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
		updateblob tes_progetti_blob
		set blob =:lbo_blob
		where	cod_azienda = :s_cs_xx.cod_azienda
		and	anno_progetto = :fi_anno_progetto_nuovo
		and   cod_progetto =:fs_cod_progetto_nuovo
		and 	num_versione = 1
		and 	num_edizione = 0
		and   progr=:ll_progr_blob
		using sqlcb;

		if sqlcb.sqlcode < 0 then
			fs_errore ="Errore sul DB: " + sqlcb.sqlerrtext
			rollback;
			destroy sqlcb;
			close righe_tes_progetti_blob;
			return -1
		end if
						
		destroy sqlcb;
						
	else
		
		updateblob tes_progetti_blob
		set blob =:lbo_blob
		where	cod_azienda = :s_cs_xx.cod_azienda
		and	anno_progetto = :fi_anno_progetto_nuovo
		and   cod_progetto =:fs_cod_progetto_nuovo
		and 	num_versione = 1
		and 	num_edizione = 0
		and   progr=:ll_progr_blob;

		if sqlca.sqlcode < 0 then
			fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
			rollback;
			close righe_tes_progetti_blob;
			return -1
		end if
		
	end if
loop

close righe_tes_progetti_blob;
		
//***** fine copia tes progetti blob


declare righe_det_fasi_progetto cursor for 
select  prog_riga_det_fasi,
		  des_fase,
		  prog_riga_det_fasi_prec,
		  durata_totale_teorica,
		  durata_totale_effettiva,
		  num_reg_lista,
		  prog_liste_con_comp
from	  det_fasi_progetto
where	  cod_azienda = :s_cs_xx.cod_azienda
and	  anno_progetto = :fi_anno_progetto
and     cod_progetto =:fs_cod_progetto
and 	  num_versione = :fi_num_versione
and 	  num_edizione = :fi_num_edizione;

open righe_det_fasi_progetto;

do while 1 = 1
  	fetch righe_det_fasi_progetto 
	into :ll_prog_riga_det_fasi, 
		  :ls_des_fase, 
		  :ll_prog_riga_det_fasi_prec, 
		  :ldd_durata_totale_teorica, 
		  :ldd_durata_totale_effettiva, 
		  :ll_num_reg_lista_f, 
		  :ll_prog_liste_con_comp_f;
		  
   if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
		rollback;
		return -1
	end if

	INSERT INTO det_fasi_progetto  
    	       ( cod_azienda,   
        		   anno_progetto,   
           		cod_progetto,   
           		num_versione,   
           		num_edizione,   
           		prog_riga_det_fasi,   
           		des_fase,   
           		prog_riga_det_fasi_prec,   
           		durata_totale_teorica,   
           		durata_totale_effettiva,   
           		documento_allegato,   
           		num_reg_lista,   
           		prog_liste_con_comp )  
   VALUES    ( :s_cs_xx.cod_azienda,   
	            :fi_anno_progetto_nuovo,   
    	         :fs_cod_progetto_nuovo,   
       	      1,   
           		0,   
	            :ll_prog_riga_det_fasi,   
    	         :ls_des_fase,   
        		   :ll_prog_riga_det_fasi_prec,   
           		null,   
	            null,   
    	         null,   
        		   :ll_num_reg_lista_f,   
               :ll_prog_liste_con_comp_f )  ;

	if sqlca.sqlcode < 0 then
		fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
		rollback;
		return -1
	end if

//****** copia det_fasi_progetto_blob

	declare righe_det_fasi_progetto_blob cursor for 
	select  progr,
			  des_blob
	from	  det_fasi_progetto_blob
	where	  cod_azienda = :s_cs_xx.cod_azienda
	and	  anno_progetto = :fi_anno_progetto
	and     cod_progetto = :fs_cod_progetto
	and 	  num_versione = :fi_num_versione
	and 	  num_edizione = :fi_num_edizione
	and     prog_riga_det_fasi = :ll_prog_riga_det_fasi;
	
	open righe_det_fasi_progetto_blob;
	
	do while 1 = 1
		fetch righe_det_fasi_progetto_blob 
		into :ll_progr_blob, 
			  :ls_des_blob;
			  
		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode < 0 then
			fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
			rollback;
			close righe_det_fasi_progetto_blob;
			return -1
		end if
	
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb);
			
			selectblob blob
			into	:lbo_blob
			from  det_fasi_progetto_blob
			where	cod_azienda = :s_cs_xx.cod_azienda
			and	anno_progetto = :fi_anno_progetto
			and   cod_progetto =:fs_cod_progetto
			and 	num_versione = :fi_num_versione
			and 	num_edizione = :fi_num_edizione
			and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
			and   progr=:ll_progr_blob
			using sqlcb;
		
			if sqlcb.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlcb.sqlerrtext
				rollback;
				destroy sqlcb;
				close righe_det_fasi_progetto_blob;
				return -1
			end if
			
			destroy sqlcb;
						
		else
			
			selectblob blob
			into	:lbo_blob
			from  det_fasi_progetto_blob
			where	cod_azienda = :s_cs_xx.cod_azienda
			and	anno_progetto = :fi_anno_progetto
			and   cod_progetto =:fs_cod_progetto
			and 	num_versione = :fi_num_versione
			and 	num_edizione = :fi_num_edizione
			and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
			and   progr=:ll_progr_blob;
		
			if sqlca.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
				rollback;
				close righe_det_fasi_progetto_blob;
				return -1
			end if
			
		end if
	
		insert into det_fasi_progetto_blob
				 ( cod_azienda,   
					anno_progetto,   
					cod_progetto,   
					num_versione,   
					num_edizione,
					prog_riga_det_fasi,
					progr,
					des_blob)			
		VALUES ( :s_cs_xx.cod_azienda,   
					:fi_anno_progetto_nuovo,   
					:fs_cod_progetto_nuovo,   
					1,   
					0,   
					:ll_prog_riga_det_fasi,
					:ll_progr_blob,   
					:ls_des_blob);
	
		if sqlca.sqlcode < 0 then
			fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
			rollback;
			close righe_det_fasi_progetto_blob;
			return -1
		end if
	
		if ls_db = "MSSQL" then

			li_risposta = f_crea_sqlcb(sqlcb)

			updateblob det_fasi_progetto_blob
			set blob =:lbo_blob
			where	cod_azienda = :s_cs_xx.cod_azienda
			and	anno_progetto = :fi_anno_progetto_nuovo
			and   cod_progetto =:fs_cod_progetto_nuovo
			and 	num_versione = 1
			and 	num_edizione = 0
			and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
			and   progr=:ll_progr_blob
			using sqlcb;
	
			if sqlcb.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlcb.sqlerrtext
				rollback;
				destroy sqlcb;
				close righe_det_fasi_progetto_blob;
				return -1
			end if
			
			destroy sqlcb;
			
		else
			
			updateblob det_fasi_progetto_blob
			set blob =:lbo_blob
			where	cod_azienda = :s_cs_xx.cod_azienda
			and	anno_progetto = :fi_anno_progetto_nuovo
			and   cod_progetto =:fs_cod_progetto_nuovo
			and 	num_versione = 1
			and 	num_edizione = 0
			and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
			and   progr=:ll_progr_blob;
	
			if sqlca.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
				rollback;
				close righe_det_fasi_progetto_blob;
				return -1
			end if
			
		end if
	loop
	
	close righe_det_fasi_progetto_blob;
			
	//***** fine copia det_fasi_progetto_blob

	declare righe_det_risorse_progetto cursor for 
	select  prog_riga_det_risorse,
			  flag_attrezzatura_int,
			  cod_cat_risorse_esterne,
			  cod_risorsa_esterna,
			  cod_cat_attrezzature,
			  cod_attrezzatura,
			  data_inizio,
			  data_fine,
			  ore,
			  costo,
			  durata_teorica,	
			  durata_effettiva,
			  flag_fine_progetto,	
			  num_reg_lista,
			  prog_liste_con_comp
	from	  det_risorse_progetto
	where	  cod_azienda = :s_cs_xx.cod_azienda
	and	  anno_progetto = :fi_anno_progetto
	and     cod_progetto = :fs_cod_progetto
	and 	  num_versione = :fi_num_versione
	and 	  num_edizione = :fi_num_edizione
	and 	  prog_riga_det_fasi = :ll_prog_riga_det_fasi;
	
	if sqlca.sqlcode < 0 then
		fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
	open righe_det_risorse_progetto;

	do while 1 = 1
		fetch righe_det_risorse_progetto 
		into  :ll_prog_riga_det_risorse, 
				:ls_flag_attrezzatura_int, 
				:ls_cod_cat_risorse_esterne, 
				:ls_cod_risorsa_esterna, 
				:ls_cod_cat_attrezzature, 
				:ls_cod_attrezzatura, 
				:ld_data_inizio_r, 
				:ld_data_fine_r, 
				:ldd_ore_r, 
				:ll_costo_r, 
				:ldd_durata_teorica_r, 
				:ldd_durata_effettiva_r, 
				:ls_flag_fine_progetto, 
				:ll_num_reg_lista_r, 
				:ll_prog_liste_con_comp_r;

	   if sqlca.sqlcode = 100 then exit

		if sqlca.sqlcode < 0 then
			fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
			rollback;
			return -1
		end if

	   INSERT INTO det_risorse_progetto  
		          ( cod_azienda,   
      		      anno_progetto,   
		            cod_progetto,   
      		      num_versione,   
		            num_edizione,   
      		      prog_riga_det_fasi,   
           			prog_riga_det_risorse,   
		            flag_attrezzatura_int,   
        		      cod_cat_risorse_esterne,   
		            cod_risorsa_esterna,   
		            cod_cat_attrezzature,   
       		      cod_attrezzatura,   
		            data_inizio,   
       		      data_fine,   
		            ore,   
       		      costo,   
		            durata_teorica,   
           			durata_effettiva,   
		            documento_allegato,   
           			flag_fine_progetto,   
		            num_reg_lista,   
           			prog_liste_con_comp )  
	  VALUES     ( :s_cs_xx.cod_azienda,   
		            :fi_anno_progetto_nuovo,   
       		      :fs_cod_progetto_nuovo,   
		            1,   
       		      0,   
		            :ll_prog_riga_det_fasi,   
       		      :ll_prog_riga_det_risorse,   
		            :ls_flag_attrezzatura_int,   
       		      :ls_cod_cat_risorse_esterne,   
		            :ls_cod_risorsa_esterna,   
		            :ls_cod_cat_attrezzature,   
       		      :ls_cod_attrezzatura,   
		            null,   
       		      null,   
		            :ldd_ore_r,   
       		      :ll_costo_r,   
		            null,   
       		      null,   
		            null,   
       		      :ls_flag_fine_progetto,   
		            :ll_num_reg_lista_r,   
       		      :ll_prog_liste_con_comp_r )  ;
	
		if sqlca.sqlcode < 0 then
			fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
			rollback;
			return -1
		end if
			
			
		//****** copia det_risorse_progetto_blob
				
		declare ri_det_risorse_progetto_blob cursor for
		select  progr,
				  des_blob
		from	  det_risorse_progetto_blob
		where	  cod_azienda = :s_cs_xx.cod_azienda
		and	  anno_progetto = :fi_anno_progetto
		and     cod_progetto = :fs_cod_progetto
		and 	  num_versione = :fi_num_versione
		and 	  num_edizione = :fi_num_edizione
		and     prog_riga_det_fasi = :ll_prog_riga_det_fasi
		and     prog_riga_det_risorse =:ll_prog_riga_det_risorse;
		
		open ri_det_risorse_progetto_blob;
		
		do while 1 = 1
			fetch ri_det_risorse_progetto_blob 
			into :ll_progr_blob, 
				  :ls_des_blob;
				  
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
				rollback;
				close ri_det_risorse_progetto_blob;
				return -1
			end if
		
			if ls_db = "MSSQL" then

				li_risposta = f_crea_sqlcb(sqlcb)
				
				selectblob blob
				into	:lbo_blob
				from  det_risorse_progetto_blob
				where	cod_azienda = :s_cs_xx.cod_azienda
				and	anno_progetto = :fi_anno_progetto
				and   cod_progetto =:fs_cod_progetto
				and 	num_versione = :fi_num_versione
				and 	num_edizione = :fi_num_edizione
				and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
				and   prog_riga_det_risorse=:ll_prog_riga_det_risorse
				and   progr=:ll_progr_blob
				using sqlcb;
		
				if sqlcb.sqlcode < 0 then
					fs_errore ="Errore sul DB: " + sqlcb.sqlerrtext
					rollback;
					destroy sqlcb;
					close ri_det_risorse_progetto_blob;
					return -1
				end if
				
				destroy sqlcb;
				
			else
				
				selectblob blob
				into	:lbo_blob
				from  det_risorse_progetto_blob
				where	cod_azienda = :s_cs_xx.cod_azienda
				and	anno_progetto = :fi_anno_progetto
				and   cod_progetto =:fs_cod_progetto
				and 	num_versione = :fi_num_versione
				and 	num_edizione = :fi_num_edizione
				and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
				and   prog_riga_det_risorse=:ll_prog_riga_det_risorse
				and   progr=:ll_progr_blob;
		
				if sqlca.sqlcode < 0 then
					fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
					rollback;
					close ri_det_risorse_progetto_blob;
					return -1
				end if
				
			end if
		
			insert into det_risorse_progetto_blob
					 ( cod_azienda,   
						anno_progetto,   
						cod_progetto,   
						num_versione,   
						num_edizione,
						prog_riga_det_fasi,
						prog_riga_det_risorse,
						progr,
						des_blob)			
			VALUES ( :s_cs_xx.cod_azienda,   
						:fi_anno_progetto_nuovo,   
						:fs_cod_progetto_nuovo,   
						1,   
						0,   
						:ll_prog_riga_det_fasi,
						:ll_prog_riga_det_risorse,
						:ll_progr_blob,   
						:ls_des_blob);
		
			if sqlca.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
				rollback;
				close ri_det_risorse_progetto_blob;
				return -1
			end if
		
			if ls_db = "MSSQL" then

				li_risposta = f_crea_sqlcb(sqlcb)
	
				updateblob det_risorse_progetto_blob
				set blob =:lbo_blob
				where	cod_azienda = :s_cs_xx.cod_azienda
				and	anno_progetto = :fi_anno_progetto_nuovo
				and   cod_progetto =:fs_cod_progetto_nuovo
				and 	num_versione = 1
				and 	num_edizione = 0
				and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
				and   prog_riga_det_risorse=:ll_prog_riga_det_risorse
				and   progr=:ll_progr_blob
				using sqlcb;
			
				if sqlcb.sqlcode < 0 then
					fs_errore ="Errore sul DB: " + sqlcb.sqlerrtext
					rollback;
					destroy sqlcb;
					close ri_det_risorse_progetto_blob;
					return -1
				end if
				
				destroy sqlcb;
				
			else	
	
				updateblob det_risorse_progetto_blob
				set blob =:lbo_blob
				where	cod_azienda = :s_cs_xx.cod_azienda
				and	anno_progetto = :fi_anno_progetto_nuovo
				and   cod_progetto =:fs_cod_progetto_nuovo
				and 	num_versione = 1
				and 	num_edizione = 0
				and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
				and   prog_riga_det_risorse=:ll_prog_riga_det_risorse
				and   progr=:ll_progr_blob;
			
				if sqlca.sqlcode < 0 then
					fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
					rollback;
					close ri_det_risorse_progetto_blob;
					return -1
				end if
				
			end if		
		loop
		
		close ri_det_risorse_progetto_blob;
				
		//***** fine copia det_risorse_progetto_blob

	loop

	close righe_det_risorse_progetto;

	if sqlca.sqlcode < 0 then
		fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
		rollback;
		return -1
	end if

loop

close righe_det_fasi_progetto;

commit;

return 0
end function

public function integer wf_nuova_revisione (integer anno_progetto, string cod_progetto, ref string fs_errore);//	Window Function di creazione nuova revisione progetto
//
// nome: wf_nuova_revisione
// tipo: integer
//	ritorno: 0 execution passed
//  		  -1 execution failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//				
//
//
//		Creata il 06-12-96 
//		Autore Diego Ferrari
//

string ls_cod_progetto,ls_des_progetto,ls_cod_prodotto,ls_cod_area_aziendale,ls_cod_resp_divisione
string ls_emesso_da,ls_approvato_da,ls_autorizzato_da,ls_validato_da,ls_flag_uso,ls_flag_recuperato
string ls_flag_valido,ls_flag_in_prova,ls_modifiche_apportate,ls_note,ls_des_fase
string ls_flag_attrezzatura_int,ls_cod_cat_risorse_esterne,ls_cod_risorsa_esterna,ls_cod_cat_attrezzature
string ls_cod_attrezzatura,ls_flag_fine_progetto,ls_des_blob,ls_flag_default
string ls_db
integer li_nmax_revisioni, li_risposta  
long ll_anno_progetto,ll_num_versione,ll_num_edizione,ll_costo,ll_valido_per,ll_prog_riga_det_fasi,ll_progr_blob
long ll_prog_riga_det_fasi_prec,ll_num_reg_lista_f,ll_prog_liste_con_comp_f,ll_num_reg_lista_r,ll_prog_liste_con_comp_r
long ll_costo_r,ll_num_nuova_edizione,ll_prog_riga_det_risorse,ll_num_nuova_versione
double ldd_ore,ldd_durata_teorica,ldd_durata_effettiva,ldd_durata_totale_teorica,ldd_durata_totale_effettiva
double ldd_ore_r,ldd_durata_teorica_r,ldd_durata_effettiva_r
datetime ld_oggi,ld_data_inizio,ld_data_fine,ld_emesso_il,ld_approvato_il,ld_autorizzato_il,ld_validato_il
datetime ld_scadenza_validazione,ld_data_inizio_r,ld_data_fine_r

transaction sqlcb
blob lbo_blob

if isnull(anno_progetto) or isnull(cod_progetto) or cod_progetto = "" then 
	fs_errore = "Anno e codice progetto sono nulli."
	return -1
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

select  max(num_versione)
into	  :ll_num_versione
from	  tes_progetti
where	  cod_azienda = :s_cs_xx.cod_azienda
and	  anno_progetto = :anno_progetto
and     cod_progetto = :cod_progetto;
	
select  max(num_edizione)
into	  :ll_num_edizione
from	  tes_progetti
where	  cod_azienda = :s_cs_xx.cod_azienda
and	  anno_progetto = :anno_progetto
and     cod_progetto = :cod_progetto
and	  num_versione = :ll_num_versione;

SELECT des_progetto,   
       cod_prodotto,   
       data_inizio,   
       data_fine,   
       ore,   
       costo,   
       durata_teorica,   
       durata_effettiva,   
       cod_area_aziendale,   
       cod_resp_divisione,   
       emesso_da,   
       emesso_il,   
       approvato_da,   
       approvato_il,   
       autorizzato_da,   
       autorizzato_il,   
       valido_per,   
       validato_da,   
       validato_il,   
       flag_uso,   
       flag_recuperato,   
       flag_valido,   
       flag_in_prova,   
       scadenza_validazione,   
       modifiche_apportate,   
       note  
INTO 	 :ls_des_progetto,   
       :ls_cod_prodotto,   
       :ld_data_inizio,   
       :ld_data_fine,   
       :ldd_ore,   
       :ll_costo,   
       :ldd_durata_teorica,   
       :ldd_durata_effettiva,   
       :ls_cod_area_aziendale,   
       :ls_cod_resp_divisione,   
       :ls_emesso_da,   
       :ld_emesso_il,   
       :ls_approvato_da,   
       :ld_approvato_il,   
       :ls_autorizzato_da,   
       :ld_autorizzato_il,   
       :ll_valido_per,   
       :ls_validato_da,   
       :ld_validato_il,   
       :ls_flag_uso,   
       :ls_flag_recuperato,   
       :ls_flag_valido,   
       :ls_flag_in_prova,   
       :ld_scadenza_validazione,   
       :ls_modifiche_apportate,   
       :ls_note  
FROM 	 tes_progetti  
WHERE  cod_azienda=:s_cs_xx.cod_azienda	
AND    anno_progetto =:anno_progetto
AND	 cod_progetto =: cod_progetto
AND	 num_versione =: ll_num_versione
AND 	 num_edizione =: ll_num_edizione;

if ls_flag_valido <> "S" and is_nuova_revisione = "S" then
	fs_errore = "Attenzione! L'ultima Versione/Revisione del progetto non è ancora valida, pertanto non è possibile creare una nuova revisione del progetto."
	return -1
end if

if is_nuova_revisione = "N" then
	select  max(num_versione)
	into	  :ll_num_versione
	from	  tes_progetti
	where	  cod_azienda = :s_cs_xx.cod_azienda
	and	  anno_progetto = :anno_progetto
	and     cod_progetto = :cod_progetto
	and     autorizzato_da is not null;
	
	select  max(num_edizione)
	into	  :ll_num_edizione
	from	  tes_progetti
	where	  cod_azienda = :s_cs_xx.cod_azienda
	and	  anno_progetto = :anno_progetto
	and     cod_progetto = :cod_progetto
	and	  num_versione = :ll_num_versione
	and     autorizzato_da is not null;

	SELECT num_versione,   
	       num_edizione,   
   	    des_progetto,   
      	 cod_prodotto,   
	       data_inizio,   
   	    data_fine,   
      	 ore,   
	       costo,   
   	    durata_teorica,   
      	 durata_effettiva,   
	       cod_area_aziendale,   
   	    cod_resp_divisione,   
      	 emesso_da,   
	       emesso_il,   
   	    approvato_da,   
      	 approvato_il,   
	       autorizzato_da,   
   	    autorizzato_il,   
      	 valido_per,   
   	    validato_da,   
	       validato_il,   
      	 flag_uso,   
	       flag_recuperato,   
   	    flag_valido,   
      	 flag_in_prova,   
	       scadenza_validazione,   
   	    modifiche_apportate,   
      	 note  
	INTO 	 :ll_num_versione,   
   	    :ll_num_edizione,   
      	 :ls_des_progetto,   
	       :ls_cod_prodotto,   
   	    :ld_data_inizio,   
      	 :ld_data_fine,   
	       :ldd_ore,   
   	    :ll_costo,   
      	 :ldd_durata_teorica,   
	       :ldd_durata_effettiva,   
   	    :ls_cod_area_aziendale,   
      	 :ls_cod_resp_divisione,   
	       :ls_emesso_da,   
   	    :ld_emesso_il,   
      	 :ls_approvato_da,   
	       :ld_approvato_il,   
   	    :ls_autorizzato_da,   
      	 :ld_autorizzato_il,   
	       :ll_valido_per,   
   	    :ls_validato_da,   
      	 :ld_validato_il,   
	       :ls_flag_uso,   
   	    :ls_flag_recuperato,   
      	 :ls_flag_valido,   
	       :ls_flag_in_prova,   
   	    :ld_scadenza_validazione,   
      	 :ls_modifiche_apportate,   
	       :ls_note  
	FROM 	 tes_progetti  
	WHERE  cod_azienda=:s_cs_xx.cod_azienda	
	AND	 anno_progetto =:anno_progetto
	AND	 cod_progetto =: cod_progetto
	AND	 num_versione =: ll_num_versione
	AND 	 num_edizione =: ll_num_edizione;

end if

ld_oggi = datetime(today())

ll_num_nuova_edizione = ll_num_edizione + 1
ll_num_nuova_versione = ll_num_versione

SELECT numero  
INTO   :li_nmax_revisioni  
FROM   parametri  
WHERE  parametri.cod_parametro = 'NEP' ;

if ll_num_nuova_edizione > li_nmax_revisioni then
	ll_num_nuova_edizione = 0
	ll_num_nuova_versione = ll_num_versione  + 1
end if
	
INSERT INTO tes_progetti  
         ( cod_azienda,   
           anno_progetto,   
           cod_progetto,   
           num_versione,   
           num_edizione,   
           des_progetto,   
           cod_prodotto,   
           data_inizio,   
           data_fine,   
           ore,   
           costo,   
           durata_teorica,   
           durata_effettiva,   
           cod_area_aziendale,   
           cod_resp_divisione,   
           emesso_da,   
           emesso_il,   
           approvato_da,   
           approvato_il,   
           autorizzato_da,   
           autorizzato_il,   
           valido_per,   
           validato_da,   
           validato_il,   
           flag_uso,   
           flag_recuperato,   
           flag_valido,   
           flag_in_prova,   
           scadenza_validazione,   
           modifiche_apportate,   
           note,   
           flag_schedulato,   
           data_inizio_effettiva)  
VALUES 	(  :s_cs_xx.cod_azienda,   
            :anno_progetto,   
            :cod_progetto,   
            :ll_num_nuova_versione,   
            :ll_num_nuova_edizione,   
            :ls_des_progetto,   
            :ls_cod_prodotto,   
            :ld_data_inizio,   
            :ld_data_fine,   
            :ldd_ore,   
            :ll_costo,   
            null,   
            null,   
            :ls_cod_area_aziendale,   
            :ls_cod_resp_divisione,   
            :s_cs_xx.cod_utente,   
            :ld_oggi,   
            null,   
            null,   
            null,   
            null,   
            null,   
            null,   
            null,   
            :ls_flag_uso,   
            :ls_flag_recuperato,   
            'N',   
            'S',   
            null,   
            :ls_modifiche_apportate,   
            :ls_note,
				'N',
				null);

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	rollback;
   return -1
end if

//****** copia tes_progetti_blob

declare righe_tes_progetti_blob cursor for 
select  progr,
		  des_blob,
		  flag_default
from	  tes_progetti_blob
where	  cod_azienda = :s_cs_xx.cod_azienda
AND     anno_progetto =:anno_progetto
AND	  cod_progetto =: cod_progetto
AND	  num_versione =: ll_num_versione
AND 	  num_edizione =: ll_num_edizione;

open righe_tes_progetti_blob;

do while 1 = 1
  	fetch righe_tes_progetti_blob 
	into :ll_progr_blob, 
		  :ls_des_blob, 
		  :ls_flag_default;
		  
   if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		rollback;
		return -1
	end if

	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
		selectblob blob
		into	:lbo_blob
		from  tes_progetti_blob
		where	cod_azienda = :s_cs_xx.cod_azienda
		AND   anno_progetto =:anno_progetto
		AND	cod_progetto =: cod_progetto
		AND	num_versione =: ll_num_versione
		AND 	num_edizione =: ll_num_edizione
		and   progr=:ll_progr_blob
		using sqlcb;
	
		if sqlcb.sqlcode < 0 then
			fs_errore = "Errore sul DB: " + sqlcb.sqlerrtext
			rollback;
			destroy sqlcb;
			close righe_tes_progetti_blob;
			return -1
		end if
		
		destroy sqlcb;
		
	else
		
		selectblob blob
		into	:lbo_blob
		from  tes_progetti_blob
		where	cod_azienda = :s_cs_xx.cod_azienda
		AND   anno_progetto =:anno_progetto
		AND	cod_progetto =: cod_progetto
		AND	num_versione =: ll_num_versione
		AND 	num_edizione =: ll_num_edizione
		and   progr=:ll_progr_blob;
	
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			rollback;
			close righe_tes_progetti_blob;
			return -1
		end if
	
	end if

	insert into tes_progetti_blob
  	       ( cod_azienda,   
				anno_progetto,   
				cod_progetto,   
				num_versione,   
				num_edizione,   
				progr,
				des_blob,
				flag_default)			
   VALUES ( :s_cs_xx.cod_azienda,   
	         :anno_progetto,   
            :cod_progetto,   
            :ll_num_nuova_versione,   
            :ll_num_nuova_edizione,     
	         :ll_progr_blob,   
  	         :ls_des_blob,   
        		:ls_flag_default);

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		rollback;
		close righe_tes_progetti_blob;
		return -1
	end if

	if ls_db = "MSSQL" then

		li_risposta = f_crea_sqlcb(sqlcb)
		
		updateblob tes_progetti_blob
		set blob =:lbo_blob
		where	cod_azienda = :s_cs_xx.cod_azienda
		and	anno_progetto = :anno_progetto
		and   cod_progetto =:cod_progetto
		and 	num_versione = :ll_num_nuova_versione
		and 	num_edizione = :ll_num_nuova_edizione
		and   progr=:ll_progr_blob
		using sqlcb;
	
		if sqlcb.sqlcode < 0 then
			fs_errore = "Errore sul DB: " + sqlcb.sqlerrtext
			rollback;
			destroy sqlcb;
			close righe_tes_progetti_blob;
			return -1
		end if
		
		destroy sqlcb;
		
	else
		
		updateblob tes_progetti_blob
		set blob =:lbo_blob
		where	cod_azienda = :s_cs_xx.cod_azienda
		and	anno_progetto = :anno_progetto
		and   cod_progetto =:cod_progetto
		and 	num_versione = :ll_num_nuova_versione
		and 	num_edizione = :ll_num_nuova_edizione
		and   progr=:ll_progr_blob;
	
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			rollback;
			close righe_tes_progetti_blob;
			return -1
		end if
		
	end if
loop

close righe_tes_progetti_blob;
		
//***** fine copia tes progetti blob


il_num_versione = ll_num_versione
il_num_edizione = ll_num_edizione

declare righe_det_fasi_progetto cursor for 
select  prog_riga_det_fasi,
		  des_fase,
		  prog_riga_det_fasi_prec,
		  durata_totale_teorica,
		  durata_totale_effettiva,
		  num_reg_lista,
		  prog_liste_con_comp
from	  det_fasi_progetto
where	  cod_azienda = :s_cs_xx.cod_azienda
and	  anno_progetto = :anno_progetto
and     cod_progetto =:cod_progetto
and 	  num_versione = :ll_num_versione
and 	  num_edizione = :ll_num_edizione;

open righe_det_fasi_progetto;

do while 1 = 1
  	fetch righe_det_fasi_progetto 
	into :ll_prog_riga_det_fasi, 
		  :ls_des_fase, 
		  :ll_prog_riga_det_fasi_prec, 
		  :ldd_durata_totale_teorica, 
		  :ldd_durata_totale_effettiva, 
		  :ll_num_reg_lista_f, 
		  :ll_prog_liste_con_comp_f;
		  
   if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		rollback;
		return -1
	end if

	INSERT INTO det_fasi_progetto  
    	       ( cod_azienda,   
        		   anno_progetto,   
           		cod_progetto,   
           		num_versione,   
           		num_edizione,   
           		prog_riga_det_fasi,   
           		des_fase,   
           		prog_riga_det_fasi_prec,   
           		durata_totale_teorica,   
           		durata_totale_effettiva,   
           		documento_allegato,   
           		num_reg_lista,   
           		prog_liste_con_comp )  
   VALUES    ( :s_cs_xx.cod_azienda,   
	            :anno_progetto,   
    	         :cod_progetto,   
       	      :ll_num_nuova_versione,   
           		:ll_num_nuova_edizione,   
	            :ll_prog_riga_det_fasi,   
    	         :ls_des_fase,   
        		   :ll_prog_riga_det_fasi_prec,   
           		null,   
	            null,   
    	         null,   
        		   :ll_num_reg_lista_f,   
               :ll_prog_liste_con_comp_f )  ;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		rollback;
		return -1
	end if

//****** copia det_fasi_progetto_blob

	declare righe_det_fasi_progetto_blob cursor for 
	select  progr,
			  des_blob
	from	  det_fasi_progetto_blob
	where	  cod_azienda = :s_cs_xx.cod_azienda
	and	  anno_progetto = :anno_progetto
	and     cod_progetto = :cod_progetto
	and 	  num_versione = :ll_num_versione
	and 	  num_edizione = :ll_num_edizione
	and     prog_riga_det_fasi = :ll_prog_riga_det_fasi;
	
	open righe_det_fasi_progetto_blob;
	
	do while 1 = 1
		fetch righe_det_fasi_progetto_blob 
		into :ll_progr_blob, 
			  :ls_des_blob;
			  
		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode < 0 then
			fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
			rollback;
			close righe_det_fasi_progetto_blob;
			return -1
		end if
	
		if ls_db = "MSSQL" then

			li_risposta = f_crea_sqlcb(sqlcb)
			
			selectblob blob
			into	:lbo_blob
			from  det_fasi_progetto_blob
			where	cod_azienda = :s_cs_xx.cod_azienda
			and	anno_progetto = :anno_progetto
			and   cod_progetto =:cod_progetto
			and 	num_versione = :ll_num_versione
			and 	num_edizione = :ll_num_edizione
			and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
			and   progr=:ll_progr_blob
			using sqlcb;
		
			if sqlcb.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlcb.sqlerrtext
				rollback;
				destroy sqlcb;
				close righe_det_fasi_progetto_blob;
				return -1
			end if
			
			destroy sqlcb;
			
		else
			
			selectblob blob
			into	:lbo_blob
			from  det_fasi_progetto_blob
			where	cod_azienda = :s_cs_xx.cod_azienda
			and	anno_progetto = :anno_progetto
			and   cod_progetto =:cod_progetto
			and 	num_versione = :ll_num_versione
			and 	num_edizione = :ll_num_edizione
			and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
			and   progr=:ll_progr_blob;
		
			if sqlca.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
				rollback;
				close righe_det_fasi_progetto_blob;
				return -1
			end if
	
		end if
	
		insert into det_fasi_progetto_blob
				 ( cod_azienda,   
					anno_progetto,   
					cod_progetto,   
					num_versione,   
					num_edizione,
					prog_riga_det_fasi,
					progr,
					des_blob)			
		VALUES ( :s_cs_xx.cod_azienda,   
					:anno_progetto,   
					:cod_progetto,   
					:ll_num_nuova_versione,   
					:ll_num_nuova_edizione,   
					:ll_prog_riga_det_fasi,
					:ll_progr_blob,   
					:ls_des_blob);
	
		if sqlca.sqlcode < 0 then
			fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
			rollback;
			close righe_det_fasi_progetto_blob;
			return -1
		end if
	
		if ls_db = "MSSQL" then

			li_risposta = f_crea_sqlcb(sqlcb)
			
			updateblob det_fasi_progetto_blob
			set blob =:lbo_blob
			where	cod_azienda = :s_cs_xx.cod_azienda
			and	anno_progetto = :anno_progetto
			and   cod_progetto =:cod_progetto
			and 	num_versione = :ll_num_nuova_versione
			and 	num_edizione = :ll_num_nuova_edizione
			and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
			and   progr=:ll_progr_blob
			using sqlcb;
		
			if sqlcb.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlcb.sqlerrtext
				rollback;
				destroy sqlcb;
				close righe_det_fasi_progetto_blob;
				return -1
			end if
			
			destroy sqlcb;
			
		else
			
			updateblob det_fasi_progetto_blob
			set blob =:lbo_blob
			where	cod_azienda = :s_cs_xx.cod_azienda
			and	anno_progetto = :anno_progetto
			and   cod_progetto =:cod_progetto
			and 	num_versione = :ll_num_nuova_versione
			and 	num_edizione = :ll_num_nuova_edizione
			and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
			and   progr=:ll_progr_blob;
		
			if sqlca.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
				rollback;
				close righe_det_fasi_progetto_blob;
				return -1
			end if
			
		end if
	
	loop
	
	close righe_det_fasi_progetto_blob;
			
//***** fine copia det_fasi_progetto_blob

	declare righe_det_risorse_progetto cursor for 
	select  prog_riga_det_risorse,
			  flag_attrezzatura_int,
			  cod_cat_risorse_esterne,
			  cod_risorsa_esterna,
			  cod_cat_attrezzature,
			  cod_attrezzatura,
			  data_inizio,
			  data_fine,
			  ore,
			  costo,
			  durata_teorica,	
			  durata_effettiva,
			  flag_fine_progetto,	
			  num_reg_lista,
			  prog_liste_con_comp
	from	  det_risorse_progetto
	where	  cod_azienda = :s_cs_xx.cod_azienda
	and	  anno_progetto = :anno_progetto
	and     cod_progetto = :cod_progetto
	and 	  num_versione = :ll_num_versione
	and 	  num_edizione = :ll_num_edizione
	and 	  prog_riga_det_fasi = :ll_prog_riga_det_fasi;
	
	if sqlca.sqlcode < 0 then exit
	
	open righe_det_risorse_progetto;

	do while 1 = 1
		fetch righe_det_risorse_progetto 
		into  :ll_prog_riga_det_risorse, 
				:ls_flag_attrezzatura_int, 
				:ls_cod_cat_risorse_esterne, 
				:ls_cod_risorsa_esterna, 
				:ls_cod_cat_attrezzature, 
				:ls_cod_attrezzatura, 
				:ld_data_inizio_r, 
				:ld_data_fine_r, 
				:ldd_ore_r, 
				:ll_costo_r, 
				:ldd_durata_teorica_r, 
				:ldd_durata_effettiva_r, 
				:ls_flag_fine_progetto, 
				:ll_num_reg_lista_r, 
				:ll_prog_liste_con_comp_r;

	   if sqlca.sqlcode = 100 then exit

		if sqlca.sqlcode < 0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			rollback;
			return -1
		end if

	   INSERT INTO det_risorse_progetto  
		          ( cod_azienda,   
      		      anno_progetto,   
		            cod_progetto,   
      		      num_versione,   
		            num_edizione,   
      		      prog_riga_det_fasi,   
           			prog_riga_det_risorse,   
		            flag_attrezzatura_int,   
        		      cod_cat_risorse_esterne,   
		            cod_risorsa_esterna,   
		            cod_cat_attrezzature,   
       		      cod_attrezzatura,   
		            data_inizio,   
       		      data_fine,   
		            ore,   
       		      costo,   
		            durata_teorica,   
           			durata_effettiva,   
		            documento_allegato,   
           			flag_fine_progetto,   
		            num_reg_lista,   
           			prog_liste_con_comp )  
	  VALUES     ( :s_cs_xx.cod_azienda,   
		            :anno_progetto,   
       		      :cod_progetto,   
		            :ll_num_nuova_versione,   
       		      :ll_num_nuova_edizione,   
		            :ll_prog_riga_det_fasi,   
       		      :ll_prog_riga_det_risorse,   
		            :ls_flag_attrezzatura_int,   
       		      :ls_cod_cat_risorse_esterne,   
		            :ls_cod_risorsa_esterna,   
		            :ls_cod_cat_attrezzature,   
       		      :ls_cod_attrezzatura,   
		            null,   
       		      null,   
		            :ldd_ore_r,   
       		      :ll_costo_r,   
		            null,   
       		      null,   
		            null,   
       		      :ls_flag_fine_progetto,   
		            :ll_num_reg_lista_r,   
       		      :ll_prog_liste_con_comp_r )  ;
	
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				rollback;
				return -1
			end if
			
		//****** copia det_risorse_progetto_blob
				
		declare ri_det_risorse_progetto_blob cursor for
		select  progr,
				  des_blob
		from	  det_risorse_progetto_blob
		where   cod_azienda=:s_cs_xx.cod_azienda
		and	  anno_progetto = :anno_progetto
		and     cod_progetto = :cod_progetto
		and 	  num_versione = :ll_num_versione
		and 	  num_edizione = :ll_num_edizione
		and 	  prog_riga_det_fasi = :ll_prog_riga_det_fasi
		and     prog_riga_det_risorse =:ll_prog_riga_det_risorse;
		
		open ri_det_risorse_progetto_blob;
		
		do while 1 = 1
			fetch ri_det_risorse_progetto_blob 
			into :ll_progr_blob, 
				  :ls_des_blob;
				  
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
				rollback;
				close ri_det_risorse_progetto_blob;
				return -1
			end if
		
			if ls_db = "MSSQL" then

				li_risposta = f_crea_sqlcb(sqlcb)
				
				selectblob blob
				into	:lbo_blob
				from  det_risorse_progetto_blob
				where cod_azienda=:s_cs_xx.cod_azienda
				and	anno_progetto = :anno_progetto
				and   cod_progetto = :cod_progetto
				and 	num_versione = :ll_num_versione
				and 	num_edizione = :ll_num_edizione
				and 	prog_riga_det_fasi = :ll_prog_riga_det_fasi
				and   prog_riga_det_risorse=:ll_prog_riga_det_risorse
				and   progr=:ll_progr_blob
				using sqlcb;
		
				if sqlcb.sqlcode < 0 then
					fs_errore ="Errore sul DB: " + sqlcb.sqlerrtext
					rollback;
					destroy sqlcb;
					close ri_det_risorse_progetto_blob;
					return -1
				end if
				
				destroy sqlcb;
				
			else
		
				selectblob blob
				into	:lbo_blob
				from  det_risorse_progetto_blob
				where cod_azienda=:s_cs_xx.cod_azienda
				and	anno_progetto = :anno_progetto
				and   cod_progetto = :cod_progetto
				and 	num_versione = :ll_num_versione
				and 	num_edizione = :ll_num_edizione
				and 	prog_riga_det_fasi = :ll_prog_riga_det_fasi
				and   prog_riga_det_risorse=:ll_prog_riga_det_risorse
				and   progr=:ll_progr_blob;
		
				if sqlca.sqlcode < 0 then
					fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
					rollback;
					close ri_det_risorse_progetto_blob;
					return -1
				end if
			
			end if
			
			insert into det_risorse_progetto_blob
					 ( cod_azienda,   
						anno_progetto,   
						cod_progetto,   
						num_versione,   
						num_edizione,
						prog_riga_det_fasi,
						prog_riga_det_risorse,
						progr,
						des_blob)			
			VALUES ( :s_cs_xx.cod_azienda,   
						:anno_progetto,   
						:cod_progetto,   
						:ll_num_nuova_versione,   
						:ll_num_nuova_edizione,   
						:ll_prog_riga_det_fasi,
						:ll_prog_riga_det_risorse,
						:ll_progr_blob,   
						:ls_des_blob);
		
			if sqlca.sqlcode < 0 then
				fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
				rollback;
				close ri_det_risorse_progetto_blob;
				return -1
			end if
		
			if ls_db = "MSSQL" then
				
				li_risposta = f_crea_sqlcb(sqlcb)
				
				updateblob det_risorse_progetto_blob
				set blob =:lbo_blob
				where	cod_azienda = :s_cs_xx.cod_azienda
				and	anno_progetto = :anno_progetto
				and   cod_progetto =:cod_progetto
				and 	num_versione = :ll_num_nuova_versione
				and 	num_edizione = :ll_num_nuova_edizione
				and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
				and   prog_riga_det_risorse=:ll_prog_riga_det_risorse
				and   progr=:ll_progr_blob
				using sqlcb;
		     
				if sqlcb.sqlcode < 0 then
					fs_errore ="Errore sul DB: " + sqlcb.sqlerrtext
					rollback;
					destroy sqlcb;
					close ri_det_risorse_progetto_blob;
					return -1
				end if              
				
				destroy sqlcb;
				
			else
				
				updateblob det_risorse_progetto_blob
				set blob =:lbo_blob
				where	cod_azienda = :s_cs_xx.cod_azienda
				and	anno_progetto = :anno_progetto
				and   cod_progetto =:cod_progetto
				and 	num_versione = :ll_num_nuova_versione
				and 	num_edizione = :ll_num_nuova_edizione
				and   prog_riga_det_fasi=:ll_prog_riga_det_fasi
				and   prog_riga_det_risorse=:ll_prog_riga_det_risorse
				and   progr=:ll_progr_blob;
		     
				if sqlca.sqlcode < 0 then
					fs_errore ="Errore sul DB: " + sqlca.sqlerrtext
					rollback;
					close ri_det_risorse_progetto_blob;
					return -1
				end if
              
		   end if
		
		loop
		
		close ri_det_risorse_progetto_blob;
				
		//***** fine copia det_risorse_progetto_blob

	loop

	close righe_det_risorse_progetto;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
loop

close righe_det_fasi_progetto;

commit;

return 0
end function

event pc_setwindow;call super::pc_setwindow;integer li_risposta
string ls_cod_resp_divisione

windowobject l_objects[ ]

l_objects[1] = dw_tes_progetti_dett_2
dw_folder.fu_AssignTab(2, "&Tempi/Costi", l_Objects[])
l_objects[1] = dw_tes_progetti_dett_3
dw_folder.fu_AssignTab(3, "&Responsabilità", l_Objects[])
l_objects[1] = dw_tes_progetti_dett_1
//l_objects[2] = cb_2
dw_folder.fu_AssignTab(1, "&Generalità", l_Objects[])

dw_folder.fu_FolderCreate(3,4)
dw_tes_progetti_lista.set_dw_key("cod_azienda")
dw_folder.fu_SelectTab(1)

dw_tes_progetti_dett_1.object.b_ricerca_prodotto.enabled = false

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT flag_approvazione,
//		 flag_autorizzazione,
//		 flag_validazione,
//		 flag_lettura,
//		 flag_modifica,
//		 flag_elimina,
//		 cod_resp_divisione
//INTO 	 :is_flag_approvazione,
//		 :is_flag_autorizzazione,
//		 :is_flag_validazione,
//		 :is_flag_lettura,
//		 :is_flag_modifica,
//		 :is_flag_elimina,
//		 :ls_cod_resp_divisione
//FROM 	 mansionari  
//WHERE  cod_azienda = :s_cs_xx.cod_azienda 
//AND    cod_utente = :s_cs_xx.cod_utente;

SELECT cod_resp_divisione
INTO :ls_cod_resp_divisione
FROM 	 mansionari  
WHERE  cod_azienda = :s_cs_xx.cod_azienda 
AND    cod_utente = :s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

is_flag_approvazione = 'N'
is_flag_autorizzazione = 'N'
is_flag_validazione = 'N'
is_flag_lettura = 'N'
is_flag_modifica = 'N'
is_flag_elimina = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then is_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then is_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then is_flag_validazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.lettura) then is_flag_lettura = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.modifica) then is_flag_modifica = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.elimina) then is_flag_elimina = 'S'
//--- Fine modifica Giulio


if is_flag_approvazione = 'S' then
	cbx_approvazione.checked = true
end if

if is_flag_autorizzazione = 'S' then
	cbx_autorizzazione.checked = true
end if

if is_flag_validazione = 'S' then
	cbx_validazione.checked = true
end if

if (is_flag_lettura = 'S') then
	if is_flag_modifica = 'S' then
		dw_tes_progetti_lista.set_dw_options(sqlca,pcca.null_object,c_nodelete,c_default)
		dw_tes_progetti_dett_1.set_dw_options(sqlca,dw_tes_progetti_lista,c_sharedata + c_scrollparent + c_nodelete,c_default)
		dw_tes_progetti_dett_2.set_dw_options(sqlca,dw_tes_progetti_lista,c_sharedata + c_scrollparent + c_nodelete,c_default)
		dw_tes_progetti_dett_3.set_dw_options(sqlca,dw_tes_progetti_lista,c_sharedata + c_scrollparent + c_nodelete,c_default)

	end if

	if is_flag_modifica = 'N' then
		dw_tes_progetti_lista.set_dw_options(sqlca,pcca.null_object,c_nodelete + c_nomodify + c_nonew,c_default)
		dw_tes_progetti_dett_1.set_dw_options(sqlca,dw_tes_progetti_lista,c_sharedata + c_scrollparent + c_nodelete + c_nomodify + c_nonew,c_default)
		dw_tes_progetti_dett_2.set_dw_options(sqlca,dw_tes_progetti_lista,c_sharedata + c_scrollparent + c_nodelete + c_nomodify + c_nonew,c_default)
		dw_tes_progetti_dett_3.set_dw_options(sqlca,dw_tes_progetti_lista,c_sharedata + c_scrollparent + c_nodelete + c_nomodify + c_nonew,c_default)

	end if

	cbx_lettura.checked = true

else
	dw_tes_progetti_lista.set_dw_options(sqlca,pcca.null_object, c_noretrieveonopen + c_nodelete + c_nomodify + c_nonew,c_default)
	dw_tes_progetti_dett_1.set_dw_options(sqlca,dw_tes_progetti_lista,c_sharedata + c_scrollparent + c_nodelete,c_default)
	dw_tes_progetti_dett_2.set_dw_options(sqlca,dw_tes_progetti_lista,c_sharedata + c_scrollparent + c_nodelete,c_default)
	dw_tes_progetti_dett_3.set_dw_options(sqlca,dw_tes_progetti_lista,c_sharedata + c_scrollparent + c_nodelete,c_default)

end if

iuo_dw_main = dw_tes_progetti_lista

if is_flag_modifica = 'S' then
	cbx_modifica.checked = true
end if

if is_flag_elimina = 'S' then
	cbx_elimina.checked = true
end if

if sqlca.sqlcode = 100 then
	g_mb.messagebox("Omnia","Attenzione! L'utente connesso non ha un mansionario abilitato, pertanto non sarà possibile creare nuovi progetti.",exclamation!)
end if

li_risposta = wf_imposta_rb()

if is_flag_lettura = "N" then
	rb_approvati.enabled = false
	rb_autorizzati.enabled = false  
	rb_da_approvare.enabled = false
	rb_scaduti.enabled = false
	rb_tutti.enabled = false
	rb_validi.enabled = false
end if

st_5.text = s_cs_xx.cod_utente
st_6.text = ls_cod_resp_divisione
em_1.text = string(year(today()))

rb_tutti.checked = true
is_tipo_visualizzazione = "TU"
st_2.text = "ELENCO COMPLESSIVO PROGETTI"
is_flag_valido = "%"
is_flag_in_prova = "%"
li_risposta = wf_imposta_rb()
rb_tutti.triggerevent("clicked")
end event

on pc_new;call w_cs_xx_principale::pc_new;is_flag_nuovo = "S"
end on

on w_tes_progetti.create
int iCurrent
call super::create
this.cb_fasi=create cb_fasi
this.rb_approva=create rb_approva
this.rb_valida=create rb_valida
this.rb_autorizza=create rb_autorizza
this.rb_nuova_edizione=create rb_nuova_edizione
this.rb_approvati=create rb_approvati
this.rb_autorizzati=create rb_autorizzati
this.rb_validi=create rb_validi
this.rb_scaduti=create rb_scaduti
this.rb_elimina=create rb_elimina
this.rb_tutti=create rb_tutti
this.rb_da_approvare=create rb_da_approvare
this.cbx_approvazione=create cbx_approvazione
this.cbx_autorizzazione=create cbx_autorizzazione
this.cbx_validazione=create cbx_validazione
this.cbx_lettura=create cbx_lettura
this.cbx_modifica=create cbx_modifica
this.cbx_elimina=create cbx_elimina
this.gb_1=create gb_1
this.gb_2=create gb_2
this.cb_controllo=create cb_controllo
this.dw_tes_progetti_lista=create dw_tes_progetti_lista
this.st_1=create st_1
this.em_1=create em_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.st_5=create st_5
this.st_6=create st_6
this.cb_schedula_tutti=create cb_schedula_tutti
this.cb_grafici=create cb_grafici
this.cb_esegui=create cb_esegui
this.gb_3=create gb_3
this.cb_schedula_nuovi=create cb_schedula_nuovi
this.cb_elimina_sc=create cb_elimina_sc
this.cb_documenti=create cb_documenti
this.cb_duplica=create cb_duplica
this.cb_report=create cb_report
this.dw_tes_progetti_dett_1=create dw_tes_progetti_dett_1
this.dw_folder=create dw_folder
this.dw_tes_progetti_dett_3=create dw_tes_progetti_dett_3
this.dw_tes_progetti_dett_2=create dw_tes_progetti_dett_2
this.cb_calcola=create cb_calcola
this.cb_nuovo_da_mod=create cb_nuovo_da_mod
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_fasi
this.Control[iCurrent+2]=this.rb_approva
this.Control[iCurrent+3]=this.rb_valida
this.Control[iCurrent+4]=this.rb_autorizza
this.Control[iCurrent+5]=this.rb_nuova_edizione
this.Control[iCurrent+6]=this.rb_approvati
this.Control[iCurrent+7]=this.rb_autorizzati
this.Control[iCurrent+8]=this.rb_validi
this.Control[iCurrent+9]=this.rb_scaduti
this.Control[iCurrent+10]=this.rb_elimina
this.Control[iCurrent+11]=this.rb_tutti
this.Control[iCurrent+12]=this.rb_da_approvare
this.Control[iCurrent+13]=this.cbx_approvazione
this.Control[iCurrent+14]=this.cbx_autorizzazione
this.Control[iCurrent+15]=this.cbx_validazione
this.Control[iCurrent+16]=this.cbx_lettura
this.Control[iCurrent+17]=this.cbx_modifica
this.Control[iCurrent+18]=this.cbx_elimina
this.Control[iCurrent+19]=this.gb_1
this.Control[iCurrent+20]=this.gb_2
this.Control[iCurrent+21]=this.cb_controllo
this.Control[iCurrent+22]=this.dw_tes_progetti_lista
this.Control[iCurrent+23]=this.st_1
this.Control[iCurrent+24]=this.em_1
this.Control[iCurrent+25]=this.st_2
this.Control[iCurrent+26]=this.st_3
this.Control[iCurrent+27]=this.st_4
this.Control[iCurrent+28]=this.st_5
this.Control[iCurrent+29]=this.st_6
this.Control[iCurrent+30]=this.cb_schedula_tutti
this.Control[iCurrent+31]=this.cb_grafici
this.Control[iCurrent+32]=this.cb_esegui
this.Control[iCurrent+33]=this.gb_3
this.Control[iCurrent+34]=this.cb_schedula_nuovi
this.Control[iCurrent+35]=this.cb_elimina_sc
this.Control[iCurrent+36]=this.cb_documenti
this.Control[iCurrent+37]=this.cb_duplica
this.Control[iCurrent+38]=this.cb_report
this.Control[iCurrent+39]=this.dw_tes_progetti_dett_1
this.Control[iCurrent+40]=this.dw_folder
this.Control[iCurrent+41]=this.dw_tes_progetti_dett_3
this.Control[iCurrent+42]=this.dw_tes_progetti_dett_2
this.Control[iCurrent+43]=this.cb_calcola
this.Control[iCurrent+44]=this.cb_nuovo_da_mod
end on

on w_tes_progetti.destroy
call super::destroy
destroy(this.cb_fasi)
destroy(this.rb_approva)
destroy(this.rb_valida)
destroy(this.rb_autorizza)
destroy(this.rb_nuova_edizione)
destroy(this.rb_approvati)
destroy(this.rb_autorizzati)
destroy(this.rb_validi)
destroy(this.rb_scaduti)
destroy(this.rb_elimina)
destroy(this.rb_tutti)
destroy(this.rb_da_approvare)
destroy(this.cbx_approvazione)
destroy(this.cbx_autorizzazione)
destroy(this.cbx_validazione)
destroy(this.cbx_lettura)
destroy(this.cbx_modifica)
destroy(this.cbx_elimina)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.cb_controllo)
destroy(this.dw_tes_progetti_lista)
destroy(this.st_1)
destroy(this.em_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_6)
destroy(this.cb_schedula_tutti)
destroy(this.cb_grafici)
destroy(this.cb_esegui)
destroy(this.gb_3)
destroy(this.cb_schedula_nuovi)
destroy(this.cb_elimina_sc)
destroy(this.cb_documenti)
destroy(this.cb_duplica)
destroy(this.cb_report)
destroy(this.dw_tes_progetti_dett_1)
destroy(this.dw_folder)
destroy(this.dw_tes_progetti_dett_3)
destroy(this.dw_tes_progetti_dett_2)
destroy(this.cb_calcola)
destroy(this.cb_nuovo_da_mod)
end on

type cb_fasi from commandbutton within w_tes_progetti
integer x = 2857
integer y = 1120
integer width = 366
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Fasi "
end type

on clicked;window_open_parm(w_det_fasi_progetto,-1,dw_tes_progetti_lista)
end on

type rb_approva from radiobutton within w_tes_progetti
integer x = 2491
integer y = 100
integer width = 434
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Approva"
end type

on clicked;is_tipo_operazione = "AP"

end on

type rb_valida from radiobutton within w_tes_progetti
integer x = 2491
integer y = 244
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Valida"
end type

on clicked;is_tipo_operazione = "VA"

end on

type rb_autorizza from radiobutton within w_tes_progetti
integer x = 2491
integer y = 172
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Autorizza"
end type

on clicked;is_tipo_operazione = "AU"

end on

type rb_nuova_edizione from radiobutton within w_tes_progetti
integer x = 2491
integer y = 316
integer width = 434
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Nuova Rev."
end type

on clicked;is_tipo_operazione = "NU"

end on

type rb_approvati from radiobutton within w_tes_progetti
integer x = 46
integer y = 1620
integer width = 343
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Approvati"
end type

event clicked;integer li_risposta

is_flag_valido = "N"
is_flag_in_prova = "S"
is_tipo_visualizzazione = "AP"
st_2.text = "ELENCO PROGETTI APPROVATI"
li_risposta = wf_imposta_rb()
li_risposta = wf_lettura()

dw_tes_progetti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")

end event

type rb_autorizzati from radiobutton within w_tes_progetti
integer x = 411
integer y = 1620
integer width = 357
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Autorizzati"
end type

event clicked;integer li_risposta

is_tipo_visualizzazione = "AU"
is_flag_valido = "S"
is_flag_in_prova = "N"
st_2.text = "ELENCO PROGETTI AUTORIZZATI"
li_risposta = wf_imposta_rb()
li_risposta = wf_lettura()				

dw_tes_progetti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type rb_validi from radiobutton within w_tes_progetti
integer x = 791
integer y = 1620
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Validati"
end type

event clicked;integer li_risposta

is_tipo_visualizzazione = "VA"
is_flag_valido = "S"
is_flag_in_prova = "N"
st_2.text = "ELENCO PROGETTI VALIDATI"
li_risposta = wf_imposta_rb()

li_risposta = wf_lettura()


dw_tes_progetti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type rb_scaduti from radiobutton within w_tes_progetti
integer x = 1111
integer y = 1620
integer width = 274
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Storico"
end type

event clicked;integer li_risposta

is_tipo_visualizzazione = "SC"
is_flag_valido = "N"
is_flag_in_prova = "N"
st_2.text = "ELENCO PROGETTI STORICIZZATI"
li_risposta = wf_imposta_rb()
li_risposta = wf_lettura()		

dw_tes_progetti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type rb_elimina from radiobutton within w_tes_progetti
integer x = 2491
integer y = 388
integer width = 270
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Elimina"
end type

on clicked;is_tipo_operazione = "EL"
end on

type rb_tutti from radiobutton within w_tes_progetti
integer x = 1408
integer y = 1620
integer width = 206
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Tutti"
end type

event clicked;integer li_risposta

is_tipo_visualizzazione = "TU"
is_flag_valido = "%"
is_flag_in_prova = "%"
st_2.text = "ELENCO COMPLESSIVO PROGETTI"
li_risposta = wf_imposta_rb()
li_risposta = wf_lettura()

dw_tes_progetti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type rb_da_approvare from radiobutton within w_tes_progetti
integer x = 1637
integer y = 1620
integer width = 434
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Da Approvare"
end type

event clicked;integer li_risposta

is_flag_valido = "N"
is_flag_in_prova = "S"
is_tipo_visualizzazione = "DP"
st_2.text = "ELENCO PROGETTI DA APPROVARE"
li_risposta = wf_imposta_rb()
li_risposta = wf_lettura()

dw_tes_progetti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cbx_approvazione from checkbox within w_tes_progetti
integer x = 2491
integer y = 740
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Appr."
boolean automatic = false
boolean threestate = true
end type

type cbx_autorizzazione from checkbox within w_tes_progetti
integer x = 2743
integer y = 740
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Autor."
boolean automatic = false
boolean threestate = true
end type

type cbx_validazione from checkbox within w_tes_progetti
integer x = 2491
integer y = 820
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Valid."
boolean automatic = false
boolean threestate = true
end type

type cbx_lettura from checkbox within w_tes_progetti
integer x = 2743
integer y = 820
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Lett."
boolean automatic = false
boolean threestate = true
end type

type cbx_modifica from checkbox within w_tes_progetti
integer x = 2491
integer y = 900
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Mod."
boolean automatic = false
boolean threestate = true
end type

type cbx_elimina from checkbox within w_tes_progetti
integer x = 2743
integer y = 900
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Elim."
boolean automatic = false
boolean threestate = true
end type

type gb_1 from groupbox within w_tes_progetti
integer x = 2469
integer y = 20
integer width = 754
integer height = 500
integer taborder = 190
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Operazioni"
end type

type gb_2 from groupbox within w_tes_progetti
integer x = 23
integer y = 1540
integer width = 2080
integer height = 160
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza"
end type

type cb_controllo from commandbutton within w_tes_progetti
integer x = 2857
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 170
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

on clicked;integer li_risposta

li_risposta = wf_aggiorna_progetti()

dw_tes_progetti_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

rb_tutti.checked = true
rb_tutti.triggerevent("clicked")
end on

type dw_tes_progetti_lista from uo_cs_xx_dw within w_tes_progetti
integer x = 23
integer y = 120
integer width = 2423
integer height = 400
integer taborder = 180
string dataobject = "d_tes_progetti_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_modify;call super::pcd_modify;if i_extendmode and (ii_nuovo = 0) then	
	string ls_flag_valido, ls_flag_in_prova
	
	ls_flag_valido = getitemstring(getrow(),"flag_valido")
	ls_flag_in_prova = getitemstring(getrow(),"flag_in_prova")

	if ls_flag_valido = "S" and ls_flag_in_prova = "N" then
		g_mb.messagebox("Omnia","Attenzione non è possibile apportare alcuna modifica al progetto in quanto è già stato autorizzato. Per fare delle modifiche creare una nuova Versione/Revisione.",exclamation!)
		triggerevent("pcd_save")
		return
	end if

	if ls_flag_valido = "N" and ls_flag_in_prova = "N" then
		g_mb.messagebox("Omnia","Attenzione non è possibile apportare alcuna modifica al progetto in quanto è scaduto.",exclamation!)
		triggerevent("pcd_save")
		return
	end if

	cb_fasi.enabled = false
	cb_esegui.enabled = false
	cb_controllo.enabled = false
	dw_tes_progetti_dett_1.object.b_ricerca_prodotto.enabled = true

	rb_approvati.enabled = false
	rb_autorizzati.enabled = false
	rb_da_approvare.enabled = false
	rb_scaduti.enabled = false
	rb_tutti.enabled = false
	rb_validi.enabled = false

end if

ii_nuovo = 0
end event

event pcd_view;call super::pcd_view;if i_extendmode then

	ii_nuovo = 0

	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_progetto"))) then
		cb_fasi.enabled = true
		cb_esegui.enabled = true
		cb_controllo.enabled = true
		dw_tes_progetti_dett_1.object.b_ricerca_prodotto.enabled = false
		cb_esegui.enabled = true
	else
		cb_controllo.enabled = false
		cb_fasi.enabled = false	
		dw_tes_progetti_dett_1.object.b_ricerca_prodotto.enabled = true
		cb_esegui.enabled = false
	end if
	rb_approvati.enabled = true
	rb_autorizzati.enabled = true
	rb_da_approvare.enabled = true
	rb_scaduti.enabled = true
	rb_tutti.enabled = true
	rb_validi.enabled = true

end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	
	string ls_cod_resp_divisione,ls_cod_area_aziendale

	SELECT cod_resp_divisione,cod_area_aziendale
	INTO 	 :ls_cod_resp_divisione,  
			 :ls_cod_area_aziendale
	FROM 	 mansionari  
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_utente = :s_cs_xx.cod_utente;


	if sqlca.sqlcode = 100 then
		g_mb.messagebox("Omnia","Attenzione! L'utente connesso non ha un mansionario abilitato, pertanto non sarà possibile creare nuovi progetti.",exclamation!)//		dw_tes_progetti_lista.SetItemStatus ( dw_tes_progetti_lista.getrow(), 0, primary!, DataModified!)
		triggerevent("pcd_delete")
		triggerevent("pcd_save")
		return
	end if

	setitem(getrow(),"anno_progetto",year(today()))
	setitem(getrow(),"cod_resp_divisione",ls_cod_resp_divisione)
	setitem(getrow(),"cod_area_aziendale",ls_cod_area_aziendale)
	setitem(getrow(),"emesso_da",s_cs_xx.cod_utente)
	setitem(getrow(),"emesso_il",today())
	dw_tes_progetti_dett_2.setitem(dw_tes_progetti_dett_2.getrow(),"data_inizio",today())
	setitem(getrow(),"num_versione",1)
	setitem(getrow(),"num_edizione",0)
   setitem(getrow(),"flag_valido", "N")
   setitem(getrow(),"flag_in_prova", "S")		
	setitem(getrow(),"flag_schedulato", "N")
	
	dw_tes_progetti_dett_1.object.b_ricerca_prodotto.enabled = true
	cb_fasi.enabled = false
	cb_esegui.enabled = false
	cb_controllo.enabled = false
	is_flag_nuovo = "S"
	rb_approvati.enabled = false
	rb_autorizzati.enabled = false
	rb_da_approvare.enabled = false
	rb_scaduti.enabled = false
	rb_tutti.enabled = false
	rb_validi.enabled = false

end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
		
	ii_nuovo = 1
	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_progetto"))) then
		cb_fasi.enabled = true
		cb_esegui.enabled = true
		cb_controllo.enabled = true
		dw_tes_progetti_dett_1.object.b_ricerca_prodotto.enabled = false

	else
		cb_controllo.enabled = false
		cb_fasi.enabled = false	
		dw_tes_progetti_dett_1.object.b_ricerca_prodotto.enabled = true

	end if

	rb_approvati.enabled = true
	rb_autorizzati.enabled = true
	rb_da_approvare.enabled = true
	rb_scaduti.enabled = true
	rb_tutti.enabled = true
	rb_validi.enabled = true

end if
end event

event pcd_savebefore;call super::pcd_savebefore;if i_extendmode then	
	
	integer li_valido_per
	datetime ld_scadenza_validazione,ld_autorizzato_il
	
	ld_autorizzato_il = getitemdatetime(getrow(),"autorizzato_il")
	li_valido_per = getitemnumber(getrow(),"valido_per")
	ld_scadenza_validazione = datetime(relativedate(date(ld_autorizzato_il),li_valido_per))
   setitem(getrow(),"scadenza_validazione",ld_scadenza_validazione)
	is_cod_progetto = getitemstring(getrow(),"cod_progetto")
	il_anno_progetto = getitemnumber(getrow(),"anno_progetto")
	il_num_versione = getitemnumber(getrow(),"num_versione")
	il_num_edizione = getitemnumber(getrow(),"num_edizione")

end if
end event

event pcd_saveafter;call super::pcd_saveafter;if i_extendmode then
	if is_flag_nuovo = "S" then	
		is_flag_nuovo = "N"
//		dw_tes_progetti_lista.Change_DW_Current( )
//		parent.triggerevent("pc_retrieve")
		rb_da_approvare.checked = true
//		rb_da_approvare.triggerevent("clicked")
	end if
	is_flag_nuovo = "N"
end if

end event

event pcd_delete;call super::pcd_delete;if i_extendmode then

	if rowcount() > 0 and not(isnull(getitemstring(getrow(),"cod_progetto"))) then
		cb_fasi.enabled = true
		cb_esegui.enabled = true
		cb_controllo.enabled = true
		dw_tes_progetti_dett_1.object.b_ricerca_prodotto.enabled = false
	else
		cb_fasi.enabled = false		
		cb_controllo.enabled = false
		dw_tes_progetti_dett_1.object.b_ricerca_prodotto.enabled = true
	end if

end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,li_valido_per


FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
		
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
	
	long ll_i, ll_anno_progetto
	integer li_num_versione,li_num_edizione
	string ls_cod_progetto

	for ll_i = 1 to this.deletedcount()

	   ll_anno_progetto = this.getitemnumber(ll_i, "anno_progetto", delete!, true)
   	ls_cod_progetto = this.getitemstring(ll_i, "cod_progetto", delete!, true)
		li_num_versione = this.getitemnumber(ll_i, "num_versione", delete!, true)
		li_num_edizione = this.getitemnumber(ll_i, "num_edizione", delete!, true)
		
		delete from det_risorse_progetto
		where cod_azienda = :s_cs_xx.cod_azienda
		and 	anno_progetto = :ll_anno_progetto
		and 	cod_progetto = :ls_cod_progetto
		and 	num_versione = :ls_cod_progetto
		and 	num_edizione = :ls_cod_progetto;

	   delete from det_fasi_progetto
		where cod_azienda = :s_cs_xx.cod_azienda
		and 	anno_progetto = :ll_anno_progetto
		and 	cod_progetto = :ls_cod_progetto
		and 	num_versione = :ls_cod_progetto
		and 	num_edizione = :ls_cod_progetto;

	next

end if 
end on

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	if this.getrow() > 0 then
		f_PO_LoadDDDW_DW(dw_tes_progetti_dett_1,"cod_versione",sqlca,&
									  "distinta_padri","cod_versione","des_versione",&
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + getitemstring(getrow(),"cod_prodotto") + "'")
	
	end if
end if

end event

type st_1 from statictext within w_tes_progetti
integer x = 2126
integer y = 1640
integer width = 379
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Anno Progetti:"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_1 from editmask within w_tes_progetti
integer x = 2514
integer y = 1620
integer width = 297
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type st_2 from statictext within w_tes_progetti
integer x = 23
integer y = 20
integer width = 2423
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long backcolor = 12632256
boolean enabled = false
string text = "ELENCO PROGETTI"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_3 from statictext within w_tes_progetti
integer x = 2491
integer y = 620
integer width = 274
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Cod. Utente:"
boolean focusrectangle = false
end type

type st_4 from statictext within w_tes_progetti
integer x = 2491
integer y = 680
integer width = 274
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Cod. Mans.:"
boolean focusrectangle = false
end type

type st_5 from statictext within w_tes_progetti
integer x = 2766
integer y = 620
integer width = 320
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Cod. Utente"
boolean focusrectangle = false
end type

type st_6 from statictext within w_tes_progetti
integer x = 2766
integer y = 680
integer width = 320
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Cod. Utente"
boolean focusrectangle = false
end type

type cb_schedula_tutti from commandbutton within w_tes_progetti
integer x = 2857
integer y = 1320
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "S.&Tutti P."
end type

event clicked;integer li_risposta,li_num_versione,li_num_edizione,li_anno
string ls_cod_progetto,ls_errore

setpointer (hourglass!)

li_anno = integer(em_1.text)

declare righe_tes_progetti cursor for
select cod_progetto,
       num_versione,
		 num_edizione
from   tes_progetti
where  cod_azienda = :s_cs_xx.cod_azienda
and	 anno_progetto = :li_anno
and    flag_valido = 'S'
and    flag_schedulato = 'S';

open righe_tes_progetti;

do while 1 = 1
  	fetch righe_tes_progetti into :ls_cod_progetto, :li_num_versione, :li_num_edizione;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or (sqlca.sqlcode <> 0) then exit

	li_risposta = f_ripristina_cal_progetti(li_anno,ls_cod_progetto,li_num_versione,li_num_edizione,ls_errore)
	
	if li_risposta = -1 then
		g_mb.messagebox("Omnia",ls_errore,exclamation!)
		rollback;
		return
	end if
	
	update tes_progetti
	set flag_schedulato = 'N'
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 anno_progetto = :li_anno
	and 	 num_versione = :li_num_versione
	and	 num_edizione = :li_num_edizione;
		
loop

close righe_tes_progetti;

li_risposta = f_schedula_progetti(ls_errore)

if li_risposta= -1 then
	g_mb.messagebox("Omnia",ls_errore,exclamation!)
	setpointer (arrow!)
	rollback;
	return
end if

commit;

dw_tes_progetti_lista.Change_DW_Current()
parent.triggerevent("pc_retrieve")

rb_tutti.checked = true
rb_tutti.triggerevent("clicked")

setpointer (arrow!)

g_mb.messagebox("Omnia","Schedulazione completata",information!)
end event

type cb_grafici from commandbutton within w_tes_progetti
integer x = 2857
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Grafici"
end type

event clicked;string ls_cod_progetto
integer li_anno_progetto,li_versione,li_edizione

ls_cod_progetto = dw_tes_progetti_lista.getitemstring(dw_tes_progetti_lista.getrow(),"cod_progetto")
li_anno_progetto = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"anno_progetto")
li_versione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_versione")
li_edizione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_edizione")

s_cs_xx.parametri.parametro_s_1 = ls_cod_progetto
s_cs_xx.parametri.parametro_d_1 = li_anno_progetto
s_cs_xx.parametri.parametro_d_2 = li_versione
s_cs_xx.parametri.parametro_d_3 = li_edizione

window_open(w_grafico_cal_progetti,0)
end event

type cb_esegui from commandbutton within w_tes_progetti
integer x = 2834
integer y = 420
integer width = 366
integer height = 80
integer taborder = 220
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui "
end type

event clicked;Integer li_risposta
string ls_str,ls_cod_progetto,ls_errore
long ll_anno_progetto

setpointer(hourglass!)

choose case is_tipo_operazione
	case 'AP' 
		li_risposta = wf_approva()	
		if li_risposta < 0 then	
			g_mb.messagebox("Omnia","Attenzione! Si è verificato un errore",exclamation!)
			return
		end if	
		rb_approvati.checked = true	
		rb_approvati.triggerevent("clicked")

	case 'AU'
		li_risposta = wf_autorizza()	
		if li_risposta < 0 then	
			g_mb.messagebox("Omnia","Attenzione! Si è verificato un errore",exclamation!)
			return
		end if		

		dw_tes_progetti_lista.Change_DW_Current()
		triggerevent(pcca.window_current, "pc_retrieve")
		rb_autorizzati.checked = true
		rb_autorizzati.triggerevent("clicked")

	case 'VA' 
		li_risposta = wf_valida()	
		if li_risposta < 0 then	
			g_mb.messagebox("Omnia","Attenzione! Si è verificato un errore",exclamation!)
			return
		end if		
		rb_validi.checked = true
		rb_validi.triggerevent("clicked")

	case 'NU' 	
		ls_cod_progetto = dw_tes_progetti_lista.getitemstring(dw_tes_progetti_lista.getrow(),"cod_progetto")
		ll_anno_progetto = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"anno_progetto")
		is_nuova_revisione = "S"
		li_risposta = wf_nuova_revisione(ll_anno_progetto,ls_cod_progetto,ls_errore)	
		is_nuova_revisione = "N"
		if li_risposta < 0 then	
			g_mb.messagebox("Omnia",ls_errore,exclamation!)
			return
		end if		

		dw_tes_progetti_lista.Change_DW_Current()
		parent.triggerevent("pc_retrieve")
		rb_da_approvare.checked = true
		rb_da_approvare.triggerevent("clicked")

	case 'EL' 
		if (g_mb.messagebox("Omnia","Sei sicuro di volere cancellare il progetto corrente?",question!,yesno!)) = 1 then 
			li_risposta = wf_elimina(ls_errore)	
			if li_risposta < 0 then	
				g_mb.messagebox("Omnia",ls_errore,exclamation!)
				return
			end if		
		end if
		dw_tes_progetti_lista.Change_DW_Current()
		triggerevent(pcca.window_current, "pc_retrieve")
		rb_tutti.checked = true
		rb_tutti.triggerevent("clicked")

end choose

setpointer(arrow!)
end event

type gb_3 from groupbox within w_tes_progetti
integer x = 2469
integer y = 520
integer width = 754
integer height = 460
integer taborder = 210
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Riepilogo Utente"
end type

type cb_schedula_nuovi from commandbutton within w_tes_progetti
event clicked pbm_bnclicked
integer x = 2857
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&S.&Nuovi P."
end type

event clicked;integer li_risposta
string ls_errore

setpointer (hourglass!)

li_risposta = f_schedula_progetti(ls_errore)

if li_risposta= -1 then
	g_mb.messagebox("Omnia",ls_errore,exclamation!)
	setpointer (arrow!)
	rollback;
	return
end if

commit;

dw_tes_progetti_lista.Change_DW_Current()
parent.triggerevent("pc_retrieve")

rb_tutti.checked = true
rb_tutti.triggerevent("clicked")

setpointer (arrow!)

g_mb.messagebox("Omnia","Schedulazione completata",information!)
end event

type cb_elimina_sc from commandbutton within w_tes_progetti
integer x = 2469
integer y = 1120
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina sch."
end type

event clicked;integer li_risposta,li_num_versione,li_num_edizione,li_anno
string ls_cod_progetto,ls_errore

setpointer (hourglass!)

li_anno = integer(em_1.text)

declare righe_tes_progetti cursor for
select cod_progetto,
       num_versione,
		 num_edizione
from   tes_progetti
where  cod_azienda = :s_cs_xx.cod_azienda
and	 anno_progetto = :li_anno
and    flag_valido = 'S'
and    flag_schedulato = 'S';

open righe_tes_progetti;

do while 1 = 1
  	fetch righe_tes_progetti into :ls_cod_progetto, :li_num_versione, :li_num_edizione;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or (sqlca.sqlcode <> 0) then exit

	li_risposta = f_ripristina_cal_progetti(li_anno,ls_cod_progetto,li_num_versione,li_num_edizione,ls_errore)
	
	if li_risposta = -1 then
		g_mb.messagebox("Omnia",ls_errore,exclamation!)
		setpointer (hourglass!)
		rollback;
		return
	end if
	
	update tes_progetti
	set flag_schedulato = 'N'
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 anno_progetto = :li_anno
	and 	 num_versione = :li_num_versione
	and	 num_edizione = :li_num_edizione;
		
loop

close righe_tes_progetti;

setpointer (hourglass!)
commit;
end event

type cb_documenti from commandbutton within w_tes_progetti
integer x = 2469
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;window_open_parm(w_tes_progetti_blob, -1, dw_tes_progetti_lista)
end event

type cb_duplica from commandbutton within w_tes_progetti
integer x = 2469
integer y = 1320
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "D&uplica P."
end type

event clicked;integer li_risposta,li_anno_progetto,li_anno_progetto_nuovo,li_num_versione,li_num_edizione
string ls_cod_progetto,ls_cod_progetto_nuovo,ls_errore

window_open(w_nuovo_cod_progetto,0)

if s_cs_xx.parametri.parametro_i_1 = 0 then 
	g_mb.messagebox("Omnia","Operazione annullata",information!)
	return
end if

setpointer(hourglass!)

ls_cod_progetto_nuovo = s_cs_xx.parametri.parametro_s_1
li_anno_progetto_nuovo = s_cs_xx.parametri.parametro_i_2

ls_cod_progetto = dw_tes_progetti_lista.getitemstring(dw_tes_progetti_lista.getrow(),"cod_progetto")
li_anno_progetto = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"anno_progetto")
li_num_versione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_versione")
li_num_edizione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_edizione")



li_risposta = wf_duplica_progetti ( li_anno_progetto, &
												ls_cod_progetto, &
												li_anno_progetto_nuovo, &
												ls_cod_progetto_nuovo, &
												li_num_versione, &
												li_num_edizione, &
												ls_errore )
												

if li_risposta = -1 then
	g_mb.messagebox("Omnia",ls_errore,stopsign!)
	setpointer(arrow!)
	return
else
	g_mb.messagebox("Omnia","Duplicazione del progetto avvenuta con successo.",information!)
end if

dw_tes_progetti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
setpointer(arrow!)
end event

type cb_report from commandbutton within w_tes_progetti
integer x = 2469
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;

s_cs_xx.parametri.parametro_i_1 = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"anno_progetto")
s_cs_xx.parametri.parametro_s_1= dw_tes_progetti_lista.getitemstring(dw_tes_progetti_lista.getrow(),"cod_progetto")
s_cs_xx.parametri.parametro_ul_3= dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_versione")
s_cs_xx.parametri.parametro_ul_2= dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_edizione")

window_open(w_report_progetto,-1)
end event

type dw_tes_progetti_dett_1 from uo_cs_xx_dw within w_tes_progetti
integer x = 91
integer y = 680
integer width = 2309
integer height = 760
integer taborder = 140
string dataobject = "d_tes_progetti_dett_1"
end type

event itemchanged;call super::itemchanged;if i_colname ="cod_prodotto" then
	string ls_cod_versione
	
	f_PO_LoadDDDW_DW(dw_tes_progetti_dett_1,"cod_versione",sqlca,&
							  "distinta_padri","cod_versione","des_versione",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext + "'")
			
				select cod_versione
				into   :ls_cod_versione
				from   distinta_padri
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_prodotto = :i_coltext
				and    flag_predefinita = 'S';
				if sqlca.sqlcode = 0 then setitem(row,"cod_versione",ls_cod_versione)

end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tes_progetti_dett_1, "cod_prodotto")
end choose
end event

type dw_folder from u_folder within w_tes_progetti
integer x = 23
integer y = 540
integer width = 2423
integer height = 980
integer taborder = 200
end type

event itemchanged;call super::itemchanged;CHOOSE CASE i_SelectedTabName
   CASE "&Generalità"
      SetFocus(dw_tes_progetti_dett_1)
		dw_tes_progetti_dett_1.object.b_ricerca_prodotto.visible = true
//		SetFocus(cb_2)
   CASE "&Tempi/Costi"
		dw_tes_progetti_dett_1.object.b_ricerca_prodotto.visible = false
      SetFocus(dw_tes_progetti_dett_2)
   CASE "&Responsabilità"
		dw_tes_progetti_dett_1.object.b_ricerca_prodotto.visible = false
  		SetFocus(dw_tes_progetti_dett_3)
END CHOOSE

end event

type dw_tes_progetti_dett_3 from uo_cs_xx_dw within w_tes_progetti
integer x = 91
integer y = 680
integer width = 2309
integer height = 760
integer taborder = 10
string dataobject = "d_tes_progetti_dett_3"
end type

type dw_tes_progetti_dett_2 from uo_cs_xx_dw within w_tes_progetti
integer x = 91
integer y = 680
integer width = 2309
integer height = 760
integer taborder = 130
string dataobject = "d_tes_progetti_dett_2"
end type

type cb_calcola from commandbutton within w_tes_progetti
integer x = 2469
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ca&lcola"
end type

event clicked;string  ls_cod_progetto
integer li_anno_progetto,li_versione,li_edizione
double  ldd_somma_ore,ldd_durata_teorica,ldd_costo

ls_cod_progetto = dw_tes_progetti_lista.getitemstring(dw_tes_progetti_lista.getrow(),"cod_progetto")
li_anno_progetto = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"anno_progetto")
li_versione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_versione")
li_edizione = dw_tes_progetti_lista.getitemnumber(dw_tes_progetti_lista.getrow(),"num_edizione")

select sum(ore)
into   :ldd_somma_ore
from	 det_risorse_progetto
where  cod_azienda =:s_cs_xx.cod_azienda
and	 anno_progetto =:li_anno_progetto
and    cod_progetto =:ls_cod_progetto
and    num_versione =:li_versione
and    num_edizione =:li_edizione;

ldd_durata_teorica = truncate(ldd_somma_ore/24,4)

select sum(costo)
into   :ldd_costo
from   det_risorse_progetto
where  cod_azienda =:s_cs_xx.cod_azienda
and	 anno_progetto =:li_anno_progetto
and  	 cod_progetto =:ls_cod_progetto
and 	 num_versione =:li_versione
and    num_edizione =:li_edizione;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Omnia","Errore sul DB = "+ sqlca.sqlerrtext,stopsign!)
	return
end if

update tes_progetti
set    durata_teorica =:ldd_durata_teorica,
		 costo = :ldd_costo
where  cod_azienda =:s_cs_xx.cod_azienda
and	 anno_progetto =:li_anno_progetto
and  	 cod_progetto =:ls_cod_progetto
and 	 num_versione =:li_versione
and    num_edizione =:li_edizione;	

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Omnia","Errore sul DB = "+ sqlca.sqlerrtext,stopsign!)
	return
end if

end event

type cb_nuovo_da_mod from commandbutton within w_tes_progetti
integer x = 2469
integer y = 1020
integer width = 754
integer height = 80
integer taborder = 51
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Crea da Modello"
end type

event clicked;integer li_risposta,li_anno_progetto,li_anno_progetto_nuovo,li_num_versione,li_num_edizione
string ls_cod_progetto,ls_cod_progetto_nuovo,ls_errore,ls_cod_mod_prog_nuovo

//Scelgo un modello da utilizzare
window_open(w_scelta_mod_progetto,0)
if s_cs_xx.parametri.parametro_i_1 = 0 then 
	g_mb.messagebox("Omnia","Operazione annullata",information!)
	return
end if
ls_cod_mod_prog_nuovo = s_cs_xx.parametri.parametro_s_1

//scelgo il codice e l'anno del progetto nuovo
window_open(w_nuovo_cod_progetto,0)

if s_cs_xx.parametri.parametro_i_1 = 0 then 
	g_mb.messagebox("Omnia","Operazione annullata",information!)
	return
end if

ls_cod_progetto_nuovo = s_cs_xx.parametri.parametro_s_1
li_anno_progetto_nuovo = s_cs_xx.parametri.parametro_i_2

//la versione e l'edizione del progetto sono come quelli di uno nuovo:
//pcd_new di dw_tes_progetti_lista
li_num_versione = 1
li_num_edizione = 0

setpointer(hourglass!)

li_risposta = wf_nuovo_prog_da_mod (ls_cod_mod_prog_nuovo, &
												li_anno_progetto_nuovo, &
												ls_cod_progetto_nuovo, &
												li_num_versione, &
												li_num_edizione, &
												ls_errore )
												

if li_risposta = -1 then
	g_mb.messagebox("Omnia",ls_errore,stopsign!)
	setpointer(arrow!)
	rollback;
	return
else
	g_mb.messagebox("Omnia","Generazione del progetto da modello avvenuta con successo.",information!)
end if

dw_tes_progetti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
setpointer(arrow!)
end event

