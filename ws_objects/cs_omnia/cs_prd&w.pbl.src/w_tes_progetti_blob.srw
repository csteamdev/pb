﻿$PBExportHeader$w_tes_progetti_blob.srw
$PBExportComments$Window tes_progetti_blob
forward
global type w_tes_progetti_blob from w_cs_xx_principale
end type
type dw_tes_progetti_blob_det from uo_cs_xx_dw within w_tes_progetti_blob
end type
type cb_note_esterne from commandbutton within w_tes_progetti_blob
end type
type dw_tes_progetti_blob_lista from uo_cs_xx_dw within w_tes_progetti_blob
end type
end forward

global type w_tes_progetti_blob from w_cs_xx_principale
integer width = 2459
integer height = 1184
string title = "Documenti per progetto"
dw_tes_progetti_blob_det dw_tes_progetti_blob_det
cb_note_esterne cb_note_esterne
dw_tes_progetti_blob_lista dw_tes_progetti_blob_lista
end type
global w_tes_progetti_blob w_tes_progetti_blob

event pc_setwindow;call super::pc_setwindow;
dw_tes_progetti_blob_lista.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)
dw_tes_progetti_blob_det.set_dw_options(sqlca, &
                                    dw_tes_progetti_blob_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

iuo_dw_main=dw_tes_progetti_blob_lista

cb_note_esterne.enabled = false

end event

on pc_delete;call w_cs_xx_principale::pc_delete;cb_note_esterne.enabled = false

end on

on w_tes_progetti_blob.create
int iCurrent
call super::create
this.dw_tes_progetti_blob_det=create dw_tes_progetti_blob_det
this.cb_note_esterne=create cb_note_esterne
this.dw_tes_progetti_blob_lista=create dw_tes_progetti_blob_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_progetti_blob_det
this.Control[iCurrent+2]=this.cb_note_esterne
this.Control[iCurrent+3]=this.dw_tes_progetti_blob_lista
end on

on w_tes_progetti_blob.destroy
call super::destroy
destroy(this.dw_tes_progetti_blob_det)
destroy(this.cb_note_esterne)
destroy(this.dw_tes_progetti_blob_lista)
end on

type dw_tes_progetti_blob_det from uo_cs_xx_dw within w_tes_progetti_blob
integer x = 23
integer y = 680
integer width = 2377
integer height = 280
integer taborder = 20
string dataobject = "d_tes_progetti_blob_det"
borderstyle borderstyle = styleraised!
end type

type cb_note_esterne from commandbutton within w_tes_progetti_blob
integer x = 2034
integer y = 980
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_progetto, ls_db, ls_doc
long ll_progressivo, ll_prog_mimetype
integer li_i,li_anno_progetto,li_versione,li_edizione, li_risposta

transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)

li_i = dw_tes_progetti_blob_lista.getrow()

li_anno_progetto = dw_tes_progetti_blob_lista.getitemnumber(li_i, "anno_progetto")
ls_cod_progetto = dw_tes_progetti_blob_lista.getitemstring(li_i, "cod_progetto")
ll_progressivo = dw_tes_progetti_blob_lista.getitemnumber(li_i, "progr")
li_versione = dw_tes_progetti_blob_lista.getitemnumber(li_i, "num_versione")
li_edizione = dw_tes_progetti_blob_lista.getitemnumber(li_i, "num_edizione")

select prog_mimetype
into :ll_prog_mimetype
from tes_progetti_blob
where
	cod_azienda=:s_cs_xx.cod_azienda and
	anno_progetto=:li_anno_progetto and
	cod_progetto=:ls_cod_progetto and
	num_versione=:li_versione and
	num_edizione=:li_edizione and
	progr=:ll_progressivo;
	
selectblob blob
into :lbl_blob
from tes_progetti_blob
where
	cod_azienda=:s_cs_xx.cod_azienda and
	anno_progetto=:li_anno_progetto and
	cod_progetto=:ls_cod_progetto and
	num_versione=:li_versione and
	num_edizione=:li_edizione and
	progr=:ll_progressivo;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tes_progetti_blob
		set blob = :lbl_blob
		where
			cod_azienda=:s_cs_xx.cod_azienda and
			anno_progetto=:li_anno_progetto and
			cod_progetto=:ls_cod_progetto and
			num_versione=:li_versione and
			num_edizione=:li_edizione and
			progr=:ll_progressivo;
			
	else
		updateblob tes_progetti_blob
		set blob = :lbl_blob
		where
			cod_azienda=:s_cs_xx.cod_azienda and
			anno_progetto=:li_anno_progetto and
			cod_progetto=:ls_cod_progetto and
			num_versione=:li_versione and
			num_edizione=:li_edizione and
			progr=:ll_progressivo;
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
		
	update tes_progetti_blob
	set prog_mimetype = :ll_prog_mimetype
	where
		cod_azienda=:s_cs_xx.cod_azienda and
		anno_progetto=:li_anno_progetto and
		cod_progetto=:ls_cod_progetto and
		num_versione=:li_versione and
		num_edizione=:li_edizione and
		progr=:ll_progressivo;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//
//if ls_db = "MSSQL" then
//
//	li_risposta = f_crea_sqlcb(sqlcb)
//	
//	selectblob tes_progetti_blob.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tes_progetti_blob
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    anno_progetto=:li_anno_progetto
//	and    cod_progetto=:ls_cod_progetto
//	and    num_versione=:li_versione
//	and    num_edizione=:li_edizione
//	and    progr=:ll_progressivo
//	using sqlcb;
//
//	if sqlcb.sqlcode > 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//
//	if sqlcb.sqlcode < 0 then
//		g_mb.messagebox("Omnia","Errore sul DB: " + sqlcb.sqlerrtext)
//		destroy sqlcb;
//		return
//	end if
//
//	destroy sqlcb;
//
//else
//
//	selectblob tes_progetti_blob.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tes_progetti_blob
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    anno_progetto=:li_anno_progetto
//	and    cod_progetto=:ls_cod_progetto
//	and    num_versione=:li_versione
//	and    num_edizione=:li_edizione
//	and  progr=:ll_progressivo;
//
//	if sqlca.sqlcode > 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//
//	if sqlca.sqlcode < 0 then
//		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
//		return
//	end if
//
//end if
//
//window_open(w_ole, 0)
//
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob tes_progetti_blob
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda 
//		and    anno_progetto=:li_anno_progetto
//		and    cod_progetto=:ls_cod_progetto
//		and    num_versione=:li_versione
//		and    num_edizione=:li_edizione
//		and    progr=:ll_progressivo
//		using  sqlcb;
//
//		if sqlcb.sqlcode < 0 then
//			g_mb.messagebox("Omnia","Errore sul DB: " + sqlcb.sqlerrtext)
//			destroy sqlcb;
//			return
//		end if
//		
//		destroy sqlcb;
//		
//	else
//		
//	   updateblob tes_progetti_blob
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda 
//		and    anno_progetto=:li_anno_progetto
//		and    cod_progetto=:ls_cod_progetto
//		and    num_versione=:li_versione
//		and    num_edizione=:li_edizione
//		and    progr=:ll_progressivo;
//
//		if sqlca.sqlcode < 0 then
//			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
//			return
//		end if
//
//	end if
//	
//   commit;
//end if
end event

type dw_tes_progetti_blob_lista from uo_cs_xx_dw within w_tes_progetti_blob
integer x = 23
integer y = 20
integer width = 2377
integer height = 640
integer taborder = 10
string dataobject = "d_tes_progetti_blob_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_progetto")) then
      cb_note_esterne.enabled = true
   else
      cb_note_esterne.enabled = false
   end if
end if
end event

on pcd_new;call uo_cs_xx_dw::pcd_new;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end on

event pcd_setkey;call super::pcd_setkey;integer li_versione,li_edizione,li_anno_progetto
string ls_cod_progetto
long ll_progressivo,ll_i

ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_progetto")
li_anno_progetto = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_progetto")
li_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
li_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")

select max(progr)
into   :ll_progressivo
from   tes_progetti_blob
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_progetto=:li_anno_progetto
and    cod_progetto=:ls_cod_progetto
and    num_versione=:li_versione
and    num_edizione=:li_edizione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul database:" + sqlca.sqlerrtext)
	return 
end if

if isnull(ll_progressivo) then
	ll_progressivo=1
else
	ll_progressivo++
end if

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
      this.setitem(ll_i, "cod_progetto", ls_cod_progetto)
		this.setitem(ll_i, "anno_progetto", li_anno_progetto)
		this.setitem(ll_i, "num_versione", li_versione)
		this.setitem(ll_i, "num_edizione", li_edizione)
		this.setitem(ll_i, "progr", ll_progressivo)
//		this.setitem(ll_i, "flag_default", "N")
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
integer li_versione,li_edizione,li_anno_progetto
string ls_cod_progetto

ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_progetto")
li_anno_progetto = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_progetto")
li_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
li_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")

ll_errore = retrieve(s_cs_xx.cod_azienda, li_anno_progetto,ls_cod_progetto,li_versione,li_edizione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_progetto")) then
      cb_note_esterne.enabled = true
   else
      cb_note_esterne.enabled = false
   end if
end if
end event

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then
	string ls_cod_progetto,ls_test,ls_flag_default
	long ll_progressivo,ll_i
	integer li_versione,li_edizione,li_anno_progetto
	
	ls_cod_progetto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_progetto")
	li_anno_progetto = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_progetto")
	li_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
	li_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
	ls_flag_default = getitemstring(getrow(), "flag_default")
	
	if ls_flag_default = 'S' then
		select cod_azienda
		into   :ls_test
		from   tes_progetti_blob
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_progetto=:li_anno_progetto
		and    cod_progetto=:ls_cod_progetto
		and    num_versione=:li_versione
		and    num_edizione=:li_edizione
		and    flag_default = 'S';
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul database:" + sqlca.sqlerrtext)
			pcca.error = c_fatal
			return
		end if
		
		if sqlca.sqlcode = 0 then
			g_mb.messagebox("Omnia","Attenzione! Esiste già un documento di default, non è possibile impostarne un altro." + sqlca.sqlerrtext)
			pcca.error = c_fatal
			return
		end if
		
	end if
	
end if
end event

