﻿$PBExportHeader$w_tes_progetti_semplice.srw
$PBExportComments$Window tes_progetti
forward
global type w_tes_progetti_semplice from w_cs_xx_principale
end type
type dw_blob from uo_dw_drag within w_tes_progetti_semplice
end type
type dw_folder from u_folder within w_tes_progetti_semplice
end type
type dw_1 from uo_cs_xx_dw within w_tes_progetti_semplice
end type
type dw_folder_ricerca from u_folder within w_tes_progetti_semplice
end type
type dw_lista from uo_cs_xx_dw within w_tes_progetti_semplice
end type
type dw_ricerca from u_dw_search within w_tes_progetti_semplice
end type
end forward

global type w_tes_progetti_semplice from w_cs_xx_principale
integer width = 2857
integer height = 2060
string title = "Gestione Progetti"
dw_blob dw_blob
dw_folder dw_folder
dw_1 dw_1
dw_folder_ricerca dw_folder_ricerca
dw_lista dw_lista
dw_ricerca dw_ricerca
end type
global w_tes_progetti_semplice w_tes_progetti_semplice

type variables
string			is_sql_base
transaction	itran_note
end variables

forward prototypes
public function integer wf_ricerca (ref string as_errore)
public subroutine wf_retrieve_blob (long al_row)
end prototypes

public function integer wf_ricerca (ref string as_errore);string		ls_sql, ls_codice, ls_descrizione, ls_ret, ls_flag_visualizza
long		ll_count

dw_ricerca.accepttext()

ls_sql = is_sql_base
ls_sql += " where cod_azienda='"+s_cs_xx.cod_azienda+"'"

ls_codice = dw_ricerca.getitemstring(1, "cod_progetto")
if ls_codice<>"" and not isnull(ls_codice) then ls_sql+= " and cod_progetto like '%"+ls_codice+"%'"

ls_descrizione = dw_ricerca.getitemstring(1, "des_progetto")
if ls_descrizione<>"" and not isnull(ls_descrizione) then ls_sql+= " and des_progetto like '%"+ls_descrizione+"%'"

ls_flag_visualizza = dw_ricerca.getitemstring(1, "flag_visualizza")
if ls_flag_visualizza<>"" and not isnull(ls_flag_visualizza) and upper(ls_flag_visualizza)<>"TU" then
	choose case upper(ls_flag_visualizza)
		case 'AP'
			ls_sql += " and approvato_da is not null and autorizzato_da is null and validato_da is null and flag_valido='N' and flag_in_prova='S' "
		case 'AU'
			ls_sql += " and autorizzato_da is not null and validato_da is null and flag_valido='S' and flag_in_prova='N' "
		case 'VA'
			ls_sql += " and validato_da is not null and flag_valido='S' and flag_in_prova='N' "
		case 'SC'
			ls_sql += " and flag_valido='N' and flag_in_prova='N' "
		case 'DP'
			ls_sql += " and approvato_da is null and flag_valido='N' and flag_in_prova='S' "
	end choose
end if


ll_count = dw_lista.setsqlselect(ls_sql)

ll_count = dw_lista.retrieve()

if ll_count>0 then
	wf_retrieve_blob(1)
else
	wf_retrieve_blob(-1)
end if

return 0
end function

public subroutine wf_retrieve_blob (long al_row);integer			li_anno_progetto
string				ls_cod_progetto
long				ll_num_versione, ll_num_edizione

dw_blob.reset()

if al_row > 0 then
	li_anno_progetto = dw_lista.getitemnumber(al_row, "anno_progetto")
	ls_cod_progetto = dw_lista.getitemstring(al_row, "cod_progetto")
	ll_num_versione = dw_lista.getitemnumber(al_row, "num_versione")
	ll_num_edizione = dw_lista.getitemnumber(al_row, "num_edizione")
	
	if not isnull(li_anno_progetto) and li_anno_progetto > 0 and ls_cod_progetto<>"" and not isnull(ls_cod_progetto) and &
				not isnull(ll_num_versione) and ll_num_versione >= 0 and not isnull(ll_num_edizione) and ll_num_edizione >= 0 then
		
		dw_blob.retrieve(s_cs_xx.cod_azienda, li_anno_progetto, ls_cod_progetto, ll_num_versione, ll_num_edizione)
	
	end if
end if
end subroutine

on w_tes_progetti_semplice.create
int iCurrent
call super::create
this.dw_blob=create dw_blob
this.dw_folder=create dw_folder
this.dw_1=create dw_1
this.dw_folder_ricerca=create dw_folder_ricerca
this.dw_lista=create dw_lista
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_blob
this.Control[iCurrent+2]=this.dw_folder
this.Control[iCurrent+3]=this.dw_1
this.Control[iCurrent+4]=this.dw_folder_ricerca
this.Control[iCurrent+5]=this.dw_lista
this.Control[iCurrent+6]=this.dw_ricerca
end on

on w_tes_progetti_semplice.destroy
call super::destroy
destroy(this.dw_blob)
destroy(this.dw_folder)
destroy(this.dw_1)
destroy(this.dw_folder_ricerca)
destroy(this.dw_lista)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],  lw_vuoto[]
long			ll_pos


dw_lista.set_dw_key("cod_azienda")

dw_lista.set_dw_options(	sqlca, &
									pcca.null_object, &
									c_noretrieveonopen, &
									c_default)

dw_1.set_dw_options(	sqlca, &
                                    dw_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_1
dw_folder.fu_assigntab(1, "Dettaglio", lw_oggetti[])

dw_blob.settransobject(sqlca)

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_blob
dw_folder.fu_assigntab(2, "Documenti", lw_oggetti[])

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

iuo_dw_main = dw_lista

dw_folder_ricerca.fu_folderoptions(dw_folder_ricerca.c_defaultheight, dw_folder_ricerca.c_foldertableft)

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_ricerca
dw_folder_ricerca.fu_assigntab(1, "R.", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_lista
dw_folder_ricerca.fu_assigntab(2, "L.", lw_oggetti[])

dw_folder_ricerca.fu_foldercreate(2, 2)
dw_folder_ricerca.fu_selecttab(1)

is_sql_base = dw_lista.getsqlselect()

ll_pos = pos(upper(is_sql_base), "WHERE")

if ll_pos > 0 then
	is_sql_base = left(is_sql_base, ll_pos - 1)
end if

//nascondo oggetti prodotto e versione -------------------
dw_1.object.cod_prodotto_t.visible = false
dw_1.object.cod_prodotto.visible = false
dw_1.object.cf_des_prodotto.visible = false
dw_1.object.b_ricerca_prodotto.visible = false
dw_1.object.cod_versione_t.visible = false
dw_1.object.cod_versione.visible = false



end event

type dw_blob from uo_dw_drag within w_tes_progetti_semplice
integer x = 73
integer y = 1140
integer width = 2450
integer height = 768
integer taborder = 40
string dataobject = "d_tes_progetti_blob_semplice"
end type

event buttonclicked;call super::buttonclicked;if isvalid(dwo) then
	if dwo.name = "b_elimina" and row > 0 then
		
		integer			li_anno_progetto
		long				ll_num_versione, ll_num_edizione, ll_progressivo
		string				ls_cod_progetto, ls_des_blob, ls_errore
		transaction		l_tran_blob
	
		li_anno_progetto = getitemnumber(row, "anno_progetto")
		ls_cod_progetto = getitemstring(row, "cod_progetto")
		ll_num_versione = getitemnumber(row, "num_versione")
		ll_num_edizione = getitemnumber(row, "num_versione")
		ll_progressivo = getitemnumber(row, "progr")
		ls_des_blob = getitemstring(row, "des_blob")
		
		if isnull(ls_des_blob) or ls_des_blob="" then ls_des_blob="<senza nome>"
		
		if g_mb.messagebox("Apice", "Procedo con la cancellazione del documento " + string(ll_progressivo) + " - " + ls_des_blob + "?",Question!, Yesno!, 2) = 2 then return 
		
		setpointer(Hourglass!)
		
		// creo transazione separata
		if not guo_functions.uof_create_transaction_from(sqlca, l_tran_blob, ls_errore)  then
			setpointer(Arrow!)
			g_mb.error("Errore in creazione transazione: " + ls_errore)
			return
		end if
		
		delete tes_progetti_blob
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_progetto = :li_anno_progetto and
				cod_progetto = :ls_cod_progetto and
				num_versione = :ll_num_versione and
				num_edizione = :ll_num_edizione and
				progr = :ll_progressivo
		using l_tran_blob;
		
		if l_tran_blob.sqlcode <> 0 then
			setpointer(Arrow!)
			g_mb.error("Errore in cancellazione documento: " + l_tran_blob.sqlerrtext)
			rollback using l_tran_blob;
			disconnect using l_tran_blob;
			destroy l_tran_blob
		else
			commit using l_tran_blob;
			disconnect using l_tran_blob;
			destroy l_tran_blob
		end if		
		
		retrieve(s_cs_xx.cod_azienda, li_anno_progetto, ls_cod_progetto, ll_num_versione, ll_num_edizione)
		
		setpointer(Arrow!)
	end if
end if
end event

event doubleclicked;call super::doubleclicked;string			ls_temp_dir, ls_rnd, ls_estensione, ls_file_name, ls_cod_progetto
long			ll_num_versione, ll_num_edizione, ll_cont,ll_prog_mimetype, ll_len, ll_progressivo
integer		li_anno_progetto
blob lb_blob
uo_shellexecute luo_run

if row > 0 then
	
	setpointer(Hourglass!)
	
	li_anno_progetto = getitemnumber(row, "anno_progetto")
	ls_cod_progetto = getitemstring(row, "cod_progetto")
	ll_num_versione = getitemnumber(row, "num_versione")
	ll_num_edizione = getitemnumber(row, "num_edizione")
	ll_progressivo = getitemnumber(row, "progr")
	ll_prog_mimetype = getitemnumber(row, "prog_mimetype")
	
	selectblob blob
	into :lb_blob
	from tes_progetti_blob
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_progetto = :li_anno_progetto and
			cod_progetto = :ls_cod_progetto and
			num_versione = :ll_num_versione and
			num_edizione = :ll_num_edizione and
			progr = :ll_progressivo;

	if sqlca.sqlcode = 0  then 
		
		ll_len = lenA(lb_blob)
		
		select estensione
		into :ls_estensione
		from tab_mimetype
		where cod_azienda = :s_cs_xx.cod_azienda and
				prog_mimetype = :ll_prog_mimetype;
		
		ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
		
		ls_rnd = string( hour(now()),"00") +"_" + string( minute(now()),"00") +"_" + string( second(now()),"00") 
		ls_file_name =  ls_temp_dir + ls_rnd + "." + ls_estensione
		guo_functions.uof_blob_to_file( lb_blob, ls_file_name)
		
		
		luo_run = create uo_shellexecute
		luo_run.uof_run( handle(parent), ls_file_name )
		destroy(luo_run)
		
	end if
	
	setpointer(Arrow!)
	
end if
end event

event ue_start_drop;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

integer		li_anno_progetto, li_cont
long			ll_row, ll_num_versione, ll_num_edizione
string			ls_errore, ls_cod_progetto


ll_row = dw_1.getrow()

if isnull(ll_row) or ll_row < 1 then
	as_message = "Non è stata inserito alcun progetto oppure è necessario selezionare un progetto dalla lista!"
	ab_return = false
	return
end if

li_anno_progetto = dw_1.getitemnumber(ll_row, "anno_progetto")
ls_cod_progetto = dw_1.getitemstring(ll_row, "cod_progetto")
ll_num_versione = dw_1.getitemnumber(ll_row, "num_versione")
ll_num_edizione = dw_1.getitemnumber(ll_row, "num_edizione")

select 	count(*) 
into 		:li_cont
from 		tes_progetti
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_progetto = :li_anno_progetto and
			cod_progetto = :ls_cod_progetto and
			num_versione = :ll_num_versione and
			num_edizione = :ll_num_edizione;

if li_cont>0 then
else
	as_message = "Prima di associare documenti è necessario salvare il progetto!"
	ab_return = false
	return
end if


//creo la transazione (di istanza) che sarà usata per il salvataggio dei documenti
if not guo_functions.uof_create_transaction_from( sqlca, itran_note, ls_errore)  then
	as_message = "Errore in creazione transazione: " + ls_errore
	ab_return = false
	return
end if

ab_return = true

return
end event

event ue_drop_file;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

integer			li_anno_progetto
long				ll_num_versione, ll_num_edizione, ll_row, ll_progressivo, ll_len, ll_prog_mimetype
string				ls_cod_nota, ls_dir, ls_file, ls_ext, ls_errore, ls_cod_progetto
blob				lb_note_esterne


ll_row = dw_1.getrow()

//di sicuro una riga selezionata c'è (controllato da ue_start_drop)
li_anno_progetto = dw_1.getitemnumber(ll_row, "anno_progetto")
ls_cod_progetto = dw_1.getitemstring(ll_row, "cod_progetto")
ll_num_versione = dw_1.getitemnumber(ll_row, "num_versione")
ll_num_edizione = dw_1.getitemnumber(ll_row, "num_edizione")

select 	max(progr)
into 		:ll_progressivo
from 		tes_progetti_blob
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_progetto = :li_anno_progetto and
			cod_progetto = :ls_cod_progetto and
			num_versione = :ll_num_versione and
			num_edizione = :ll_num_edizione;
			
if ll_progressivo = 0 or isnull(ll_progressivo) then
	ll_progressivo = 1
else
	ll_progressivo += 1
end if

guo_functions.uof_file_to_blob(as_filename, lb_note_esterne)

ll_len = lenA(lb_note_esterne)

guo_functions.uof_get_file_info( as_filename, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;
if sqlca.sqlcode <> 0 then
	as_message = "Attenzione: estensione file " + ls_ext + " non prevista da sistema."
	return false
end if

ls_file = left(ls_file, 100)

insert into tes_progetti_blob  
		( cod_azienda,   
		  anno_progetto,
		  cod_progetto,
		  num_versione,
		  num_edizione,
		  progr,   
		  des_blob, 
		  blob,
		  flag_default,
		  prog_mimetype)  
values ( :s_cs_xx.cod_azienda,   
		  :li_anno_progetto,
		  :ls_cod_progetto,
		  :ll_num_versione,
		  :ll_num_edizione,
		  :ll_progressivo,
		  :ls_file,   
		  null,
		  'N',
		 :ll_prog_mimetype) 
using itran_note;

if itran_note.sqlcode = 0 then
	
	updateblob tes_progetti_blob
	set blob = :lb_note_esterne
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_progetto = :li_anno_progetto and
				cod_progetto = :ls_cod_progetto and
				num_versione = :ll_num_versione and
				num_edizione = :ll_num_edizione and
				progr = :ll_progressivo
	using itran_note;
			
	if itran_note.sqlcode <> 0 then
		as_message = "Errore nell'inserimento del documento digitale: " + itran_note.sqlerrtext
		return false
	end if
	
else
	
	if itran_note.sqlcode <> 0 then
		as_message = "Errore nell'inserimento del documento digitale: " + itran_note.sqlerrtext
		return false
	end if
end if

return true


end event

event ue_end_drop;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

long					ll_row

if ab_status then
	//commit
	commit using itran_note;
	disconnect using itran_note;
	destroy itran_note;
else
	//rollback
	if isvalid(itran_note) then
		rollback using itran_note;
		disconnect using itran_note;
		destroy itran_note;
	end if
	
	if as_message<>"" and not isnull(as_message) then g_mb.error(as_message)
	
end if

ll_row = dw_lista.getrow()

wf_retrieve_blob(ll_row)

return true
end event

type dw_folder from u_folder within w_tes_progetti_semplice
integer x = 46
integer y = 1008
integer width = 2747
integer height = 940
integer taborder = 20
boolean border = false
end type

type dw_1 from uo_cs_xx_dw within w_tes_progetti_semplice
integer x = 73
integer y = 1140
integer width = 2693
integer height = 768
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_tes_progetti_dett_1"
boolean minbox = true
boolean border = false
end type

type dw_folder_ricerca from u_folder within w_tes_progetti_semplice
integer x = 46
integer y = 16
integer width = 2747
integer height = 968
integer taborder = 10
boolean bringtotop = true
boolean border = false
end type

type dw_lista from uo_cs_xx_dw within w_tes_progetti_semplice
integer x = 215
integer y = 40
integer width = 2565
integer height = 928
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_tes_progetti_lista_semplice"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event pcd_setkey;call super::pcd_setkey;long		ll_index


for ll_index = 1 to rowcount()
	if isnull(getitemstring(ll_index, "cod_azienda")) or getitemstring(ll_index, "cod_azienda")="" then
		setitem(ll_index, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemnumber(ll_index, "anno_progetto")) or getitemnumber(ll_index, "anno_progetto")<=1900 then
		setitem(ll_index, "anno_progetto", f_anno_esercizio())
	end if
	
next

end event

event pcd_retrieve;call super::pcd_retrieve;integer			li_ret
string				ls_errore


setpointer(hourglass!)
		
li_ret = wf_ricerca(ls_errore)
if li_ret < 0 then
	dw_blob.retrieve("", -1, "", -1, -1)
	setpointer(arrow!)
	g_mb.error(ls_errore)
	 pcca.error = c_fatal
	return
end if

dw_folder_ricerca.fu_selecttab(2)

setpointer(arrow!)
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	long		ll_row
	
	ll_row = getrow()
	
	wf_retrieve_blob(ll_row)

end if
end event

event pcd_new;call super::pcd_new;integer			li_anno_progetto
long				ll_row
string				ls_cod_resp_divisione, ls_cod_area_aziendale

if i_extendmode then

	select cod_resp_divisione, cod_area_aziendale
	into 	 :ls_cod_resp_divisione, :ls_cod_area_aziendale
	FROM 	 mansionari  
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_utente = :s_cs_xx.cod_utente;


	if sqlca.sqlcode = 100 or ls_cod_resp_divisione="" or isnull(ls_cod_resp_divisione) then
		g_mb.warning("L'utente collegato non ha un mansionario abilitato, pertanto non è possibile creare nuovi progetti!")
		triggerevent("pcd_delete")
		triggerevent("pcd_save")
		return
	end if
	
	ll_row = dw_lista.getrow()

	if ll_row>0 then
		li_anno_progetto = f_anno_esercizio()
		dw_lista.setitem(ll_row, "anno_progetto", li_anno_progetto)
	end if

	dw_blob.reset()
	
	setitem(ll_row, "cod_resp_divisione",ls_cod_resp_divisione)
	
	if ls_cod_area_aziendale="" then setnull(ls_cod_area_aziendale)
	setitem(ll_row, "cod_area_aziendale",ls_cod_area_aziendale)
	
	setitem(ll_row, "emesso_da", s_cs_xx.cod_utente)
	setitem(ll_row, "emesso_il", today())
	
	setitem(ll_row, "data_inizio", today())
	
	setitem(ll_row, "num_versione", 1)
	setitem(ll_row, "num_edizione", 0)
	setitem(ll_row, "flag_valido", "N")
	setitem(ll_row, "flag_in_prova", "S")		
	setitem(ll_row, "flag_schedulato", "N")
	
	//dw_1.object.b_ricerca_prodotto.enabled = true
	//cb_fasi.enabled = false
	//cb_esegui.enabled = false
	//cb_controllo.enabled = false
	//is_flag_nuovo = "S"
	//rb_approvati.enabled = false
	//rb_autorizzati.enabled = false
	//rb_da_approvare.enabled = false
	//rb_scaduti.enabled = false
	//rb_tutti.enabled = false
	//rb_validi.enabled = false

end if
end event

event updatestart;call super::updatestart;long			ll_index, ll_num_versione, ll_num_edizione
integer		li_anno_progetto
string			ls_cod_progetto


for ll_index = 1 to deletedcount()
	li_anno_progetto = getitemnumber(ll_index, "anno_progetto", delete!, true)
	ls_cod_progetto = getitemstring(ll_index, "cod_progetto", delete!, true)
	ll_num_versione = getitemnumber(ll_index, "num_versione", delete!, true)
	ll_num_edizione = getitemnumber(ll_index, "num_edizione", delete!, true)
	
	delete tes_progetti_blob
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_progetto = :li_anno_progetto and
				cod_progetto = :ls_cod_progetto and
				num_versione = :ll_num_versione and
				num_edizione =: ll_num_edizione;

	if sqlca.sqlcode < 0 then
		g_mb.error("Errore in cancellazione documenti associati al progetto: "+sqlca.sqlerrtext)
		return 1
	end if
	

next

end event

type dw_ricerca from u_dw_search within w_tes_progetti_semplice
integer x = 215
integer y = 44
integer width = 2555
integer height = 888
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_tes_progetti_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;integer			li_ret
string				ls_errore


choose case dwo.name
	case "b_ricerca"
		dw_lista.change_dw_current()
		parent.postevent("pc_retrieve")
		
end choose
end event

