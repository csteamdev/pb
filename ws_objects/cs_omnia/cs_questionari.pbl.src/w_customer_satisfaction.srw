﻿$PBExportHeader$w_customer_satisfaction.srw
$PBExportComments$Finestra di Valutazione della soddisfazione del cliente
forward
global type w_customer_satisfaction from window
end type
type tab_1 from tab within w_customer_satisfaction
end type
type tabpage_1 from userobject within tab_1
end type
type cb_calcola from commandbutton within tabpage_1
end type
type em_data_fine from editmask within tabpage_1
end type
type st_2 from statictext within tabpage_1
end type
type st_1 from statictext within tabpage_1
end type
type em_data_inizio from editmask within tabpage_1
end type
type dw_lista_questionari from datawindow within tabpage_1
end type
type tabpage_1 from userobject within tab_1
cb_calcola cb_calcola
em_data_fine em_data_fine
st_2 st_2
st_1 st_1
em_data_inizio em_data_inizio
dw_lista_questionari dw_lista_questionari
end type
type tabpage_2 from userobject within tab_1
end type
type dw_graph_media_risposte from uo_cs_graph within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_graph_media_risposte dw_graph_media_risposte
end type
type tabpage_3 from userobject within tab_1
end type
type dw_hit_risposte from datawindow within tabpage_3
end type
type tabpage_3 from userobject within tab_1
dw_hit_risposte dw_hit_risposte
end type
type tabpage_4 from userobject within tab_1
end type
type dw_graph_rilevanza_cum from uo_cs_graph within tabpage_4
end type
type tabpage_4 from userobject within tab_1
dw_graph_rilevanza_cum dw_graph_rilevanza_cum
end type
type tab_1 from tab within w_customer_satisfaction
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
end type
end forward

global type w_customer_satisfaction from window
integer x = 1189
integer y = 556
integer width = 3282
integer height = 1912
boolean titlebar = true
string title = "Customer Satisfaction"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
tab_1 tab_1
end type
global w_customer_satisfaction w_customer_satisfaction

forward prototypes
public subroutine wf_ordinamento (long fl_dati[6], long fl_riga, datawindow ldw_datawindow)
public subroutine wf_rilevanza_cumulata ()
public subroutine wf_frequenza_risposte ()
public subroutine wf_media_risposte ()
end prototypes

public subroutine wf_ordinamento (long fl_dati[6], long fl_riga, datawindow ldw_datawindow);

// --------------- ALGORITMO DI ORDINAMENTO



ldw_datawindow.setitem(fl_riga, "risposta_1", "Risposta 1 ~r~n(" + string(fl_dati[1]) + ")" )
ldw_datawindow.setitem(fl_riga, "risposta_2", "Risposta 2 ~r~n(" + string(fl_dati[2]) + ")" )
ldw_datawindow.setitem(fl_riga, "risposta_3", "Risposta 3 ~r~n(" + string(fl_dati[3]) + ")" )
ldw_datawindow.setitem(fl_riga, "risposta_4", "Risposta 4 ~r~n(" + string(fl_dati[4]) + ")" )
ldw_datawindow.setitem(fl_riga, "risposta_5", "Risposta 5 ~r~n(" + string(fl_dati[5]) + ")" )
ldw_datawindow.setitem(fl_riga, "titolo", "FREQUENZA RISPOSTE" )
end subroutine

public subroutine wf_rilevanza_cumulata ();string   ls_flag_tipo_risposta,ls_check_button, ls_option_button, ls_des_option_libero
long     ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp, ll_peso_check_1, &
         ll_peso_check_2, ll_peso_option_1, ll_peso_option_2, ll_peso_option_3, ll_peso_option_4, ll_peso_option_5, &
	      ll_peso_option_libero, ll_importanza_domanda, ll_prog_riga_liste_contr, ll_prog_riga_prec, ll_num_domande, &
	      ll_riga, ll_numero_domanda, ll_range_importanza
dec{4}   ld_punteggio
datetime ldt_data_inizio, ldt_data_fine
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_questionari_grafico_1"
lds_dati.settransobject(sqlca)

declare  cu_questionario cursor for
select   det_liste_con_comp.flag_tipo_risposta,   
         tes_liste_con_comp.num_reg_lista,   
         tes_liste_con_comp.prog_liste_con_comp,   
         tes_liste_con_comp.num_versione,   
         tes_liste_con_comp.num_edizione,   
         det_liste_con_comp.check_button,   
         det_liste_con_comp.option_button,   
         det_liste_con_comp.peso_check_1,   
         det_liste_con_comp.peso_check_2,   
         det_liste_con_comp.peso_option_1,   
         det_liste_con_comp.peso_option_2,   
         det_liste_con_comp.peso_option_3,   
         det_liste_con_comp.peso_option_4,   
         det_liste_con_comp.peso_option_5,   
         det_liste_con_comp.peso_option_libero,   
         det_liste_con_comp.des_option_libero,   
         det_liste_con_comp.importanza_domanda,   
         det_liste_con_comp.prog_riga_liste_contr  
from     tes_liste_con_comp,   
         det_liste_con_comp  
where    ( det_liste_con_comp.cod_azienda = tes_liste_con_comp.cod_azienda ) and  
         ( det_liste_con_comp.num_reg_lista = tes_liste_con_comp.num_reg_lista ) and  
         ( det_liste_con_comp.prog_liste_con_comp = tes_liste_con_comp.prog_liste_con_comp ) and  
         ( ( tes_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda ) and  
         ( tes_liste_con_comp.num_reg_lista = :ll_num_reg_lista ) and  
         ( tes_liste_con_comp.num_versione = :ll_num_versione ) and  
         ( tes_liste_con_comp.num_edizione = :ll_num_edizione ) and
			( tes_liste_con_comp.data_registrazione between :ldt_data_inizio and :ldt_data_fine) )   
order by tes_liste_con_comp.num_reg_lista asc,   
         tes_liste_con_comp.num_versione asc,   
         tes_liste_con_comp.num_edizione asc,
			det_liste_con_comp.prog_riga_liste_contr asc,			
         tes_liste_con_comp.prog_liste_con_comp asc;

ll_num_reg_lista = tab_1.tabpage_1.dw_lista_questionari.getitemnumber(tab_1.tabpage_1.dw_lista_questionari.getrow(),"num_reg_lista")
ll_num_versione = tab_1.tabpage_1.dw_lista_questionari.getitemnumber(tab_1.tabpage_1.dw_lista_questionari.getrow(),"num_versione")
ll_num_edizione = tab_1.tabpage_1.dw_lista_questionari.getitemnumber(tab_1.tabpage_1.dw_lista_questionari.getrow(),"num_edizione")
ldt_data_inizio = datetime(date(tab_1.tabpage_1.em_data_inizio.text), 00:00:00)
ldt_data_fine   = datetime(date(tab_1.tabpage_1.em_data_fine.text), 00:00:00)

if isnull(ldt_data_inizio) or ldt_data_inizio < datetime(date("01/01/1900"), 00:00:00) then
	g_mb.messagebox("OMNIA","Impostare dei limiti di data inizio e data fine validi")
	destroy lds_dati
	return
end if

if isnull(ldt_data_fine) or ldt_data_fine < datetime(date("01/01/1900"), 00:00:00) then
	g_mb.messagebox("OMNIA","Impostare dei limiti di data inizio e data fine validi")
	destroy lds_dati
	return
end if

if ldt_data_inizio > ldt_data_fine then
	g_mb.messagebox("OMNIA","Limiti di date incoerenti!")
	destroy lds_dati
	return
end if

//tab_1.tabpage_4.dw_grafico_2.reset()
ll_prog_riga_prec = 0
ll_numero_domanda = 0
ll_num_domande = 0
ld_punteggio = 0

open cu_questionario;

do while true
	
	fetch cu_questionario
	into :ls_flag_tipo_risposta,
	     :ll_num_reg_lista,
		  :ll_prog_liste_con_comp,
		  :ll_num_versione,
		  :ll_num_edizione,
		  :ls_check_button,
		  :ls_option_button,
		  :ll_peso_check_1,
		  :ll_peso_check_2,
		  :ll_peso_option_1,
		  :ll_peso_option_2,
		  :ll_peso_option_3,
		  :ll_peso_option_4,
		  :ll_peso_option_5,
		  :ll_peso_option_libero,
		  :ls_des_option_libero,
		  :ll_importanza_domanda,
		  :ll_prog_riga_liste_contr;
	
	if sqlca.sqlcode = 100 then
		
		if ll_num_domande > 0 and ld_punteggio > 0 then
			/*
			ll_riga = tab_1.tabpage_4.dw_grafico_2.insertrow(0)
			tab_1.tabpage_4.dw_grafico_2.setitem(ll_riga, "asse_x", ll_numero_domanda)
			tab_1.tabpage_4.dw_grafico_2.setitem(ll_riga, "asse_y", ld_punteggio)
			*/
			ll_riga = lds_dati.insertrow(0)
			lds_dati.setitem(ll_riga, "asse_x", ll_numero_domanda)
			lds_dati.setitem(ll_riga, "asse_y", ld_punteggio)
		end if
		
		exit
		
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in fetch cursore cu_questionario. Dettaglio errore " + sqlca.sqlerrtext)
		exit
	end if
	
	select range_importanza
	into   :ll_range_importanza
	from   tes_liste_controllo
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       num_reg_lista = :ll_num_reg_lista  and
			 num_versione  = :ll_num_versione and
			 num_edizione  = :ll_num_edizione;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in ricerca range importanza in testata lista controllo")
		destroy lds_dati
		return
	end if
	
	if ll_prog_riga_liste_contr <> ll_prog_riga_prec then
		
		if ll_num_domande > 0 and ld_punteggio > 0 then
			/*
			ll_riga = tab_1.tabpage_4.dw_grafico_2.insertrow(0)
			tab_1.tabpage_4.dw_grafico_2.setitem(ll_riga, "asse_x", ll_numero_domanda)
			tab_1.tabpage_4.dw_grafico_2.setitem(ll_riga, "asse_y", ld_punteggio)
			*/
			ll_riga = lds_dati.insertrow(0)
			lds_dati.setitem(ll_riga, "asse_x", ll_numero_domanda)
			lds_dati.setitem(ll_riga, "asse_y", ld_punteggio)
		end if
		
		ll_numero_domanda ++
		ll_num_domande = 0
		ld_punteggio   = 0
		ll_prog_riga_prec = ll_prog_riga_liste_contr
		
	end if		
	
	ll_num_domande ++
	
	if ls_flag_tipo_risposta = "C" or ls_flag_tipo_risposta = "O" then
		ld_punteggio = ld_punteggio +  (ll_importanza_domanda / ll_range_importanza)
	end if		
	
loop

commit;

//costruzione del grafico -------------------------------------------------------------
tab_1.tabpage_4.dw_graph_rilevanza_cum.ib_datastore = true
tab_1.tabpage_4.dw_graph_rilevanza_cum.ids_data = lds_dati

tab_1.tabpage_4.dw_graph_rilevanza_cum.is_str[1] = "titolo"
tab_1.tabpage_4.dw_graph_rilevanza_cum.is_str[2] = "label_x"
tab_1.tabpage_4.dw_graph_rilevanza_cum.is_str[3] = "label_y"
tab_1.tabpage_4.dw_graph_rilevanza_cum.is_num[1] = "asse_x"
tab_1.tabpage_4.dw_graph_rilevanza_cum.is_num[2] = "asse_y"

tab_1.tabpage_4.dw_graph_rilevanza_cum.is_source_categoria_col = "asse_x"

tab_1.tabpage_4.dw_graph_rilevanza_cum.is_categoria_col = "num_1"
tab_1.tabpage_4.dw_graph_rilevanza_cum.is_exp_valore = "num_2"
tab_1.tabpage_4.dw_graph_rilevanza_cum.is_serie_col = ""

//altre impostazioni del grafico
tab_1.tabpage_4.dw_graph_rilevanza_cum.is_titolo = "RILEVANZA CUMULATA RISPOSTE"
tab_1.tabpage_4.dw_graph_rilevanza_cum.is_label_categoria= "Numero domanda"
tab_1.tabpage_4.dw_graph_rilevanza_cum.is_label_valori = "Punteggio"

//tipo scala: 1 se lineare, 2 se logaritmica in base 10
tab_1.tabpage_4.dw_graph_rilevanza_cum.ii_scala = 1

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
tab_1.tabpage_4.dw_graph_rilevanza_cum.ii_tipo_grafico = 7 //Colonne

//colore di sfondo del grafico
//per il bianco (DEFAULT)	: 16777215
//per il silver			: 12632256
tab_1.tabpage_4.dw_graph_rilevanza_cum.il_backcolor = 16777215 //white

//metodo che visualizza i dati nel grafico
tab_1.tabpage_4.dw_graph_rilevanza_cum.uof_retrieve( )
//--------------------------------------------------------------------------------------------

close cu_questionario;
destroy lds_dati

tab_1.tabpage_4.enabled = true

/*
tab_1.tabpage_4.dw_grafico_2.object.grafico.title = "RILEVANZA CUMULATA RISPOSTE"
tab_1.tabpage_4.dw_grafico_2.object.grafico.values.label = "Punteggio"
tab_1.tabpage_4.dw_grafico_2.object.grafico.category.label = "Numero domanda"
tab_1.tabpage_4.dw_grafico_2.setredraw(true)
*/
end subroutine

public subroutine wf_frequenza_risposte ();string   ls_flag_tipo_risposta,ls_check_button, ls_option_button, ls_des_option_libero, ls_des_domanda, ls_des_domanda_precedente
long     ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp, ll_peso_check_1, &
         ll_peso_check_2, ll_peso_option_1, ll_peso_option_2, ll_peso_option_3, ll_peso_option_4, ll_peso_option_5, &
	      ll_peso_option_libero, ll_importanza_domanda, ll_prog_riga_liste_contr, ll_prog_riga_prec, ll_num_domande, ll_media_domanda, &
	      ll_riga, ll_numero_domanda, ll_range_importanza, ll_risposte[6]
datetime ldt_data_inizio, ldt_data_fine


declare  cu_questionario cursor for
select   det_liste_con_comp.flag_tipo_risposta,   
         tes_liste_con_comp.num_reg_lista,   
         tes_liste_con_comp.prog_liste_con_comp,   
         tes_liste_con_comp.num_versione,   
         tes_liste_con_comp.num_edizione,   
         det_liste_con_comp.des_domanda,   
         det_liste_con_comp.check_button,   
         det_liste_con_comp.option_button,   
         det_liste_con_comp.peso_check_1,   
         det_liste_con_comp.peso_check_2,   
         det_liste_con_comp.peso_option_1,   
         det_liste_con_comp.peso_option_2,   
         det_liste_con_comp.peso_option_3,   
         det_liste_con_comp.peso_option_4,   
         det_liste_con_comp.peso_option_5,   
         det_liste_con_comp.peso_option_libero,   
         det_liste_con_comp.des_option_libero,   
         det_liste_con_comp.importanza_domanda,   
         det_liste_con_comp.prog_riga_liste_contr  
from     tes_liste_con_comp,   
         det_liste_con_comp  
where    ( det_liste_con_comp.cod_azienda = tes_liste_con_comp.cod_azienda ) and  
         ( det_liste_con_comp.num_reg_lista = tes_liste_con_comp.num_reg_lista ) and  
         ( det_liste_con_comp.prog_liste_con_comp = tes_liste_con_comp.prog_liste_con_comp ) and  
         ( ( tes_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda ) and  
         ( tes_liste_con_comp.num_reg_lista = :ll_num_reg_lista ) and  
         ( tes_liste_con_comp.num_versione = :ll_num_versione ) and  
         ( tes_liste_con_comp.num_edizione = :ll_num_edizione ) and
			( tes_liste_con_comp.data_registrazione between :ldt_data_inizio and :ldt_data_fine) )   
order by tes_liste_con_comp.num_reg_lista asc,   
         tes_liste_con_comp.num_versione asc,   
         tes_liste_con_comp.num_edizione asc,
         det_liste_con_comp.prog_riga_liste_contr asc,
			tes_liste_con_comp.prog_liste_con_comp asc;

ll_num_reg_lista = tab_1.tabpage_1.dw_lista_questionari.getitemnumber(tab_1.tabpage_1.dw_lista_questionari.getrow(),"num_reg_lista")
ll_num_versione = tab_1.tabpage_1.dw_lista_questionari.getitemnumber(tab_1.tabpage_1.dw_lista_questionari.getrow(),"num_versione")
ll_num_edizione = tab_1.tabpage_1.dw_lista_questionari.getitemnumber(tab_1.tabpage_1.dw_lista_questionari.getrow(),"num_edizione")
ldt_data_inizio = datetime(date(tab_1.tabpage_1.em_data_inizio.text), 00:00:00)
ldt_data_fine   = datetime(date(tab_1.tabpage_1.em_data_fine.text), 00:00:00)

if isnull(ldt_data_inizio) or ldt_data_inizio < datetime(date("01/01/1900"), 00:00:00) then
	g_mb.messagebox("OMNIA","Impostare dei limiti di data inizio e data fine validi")
	return
end if

if isnull(ldt_data_fine) or ldt_data_fine < datetime(date("01/01/1900"), 00:00:00) then
	g_mb.messagebox("OMNIA","Impostare dei limiti di data inizio e data fine validi")
	return
end if

if ldt_data_inizio > ldt_data_fine then
	g_mb.messagebox("OMNIA","Limiti di date incoerenti!")
	return
end if

tab_1.tabpage_3.dw_hit_risposte.reset()
ll_prog_riga_prec = 0
ll_numero_domanda = 0
ll_num_domande = 0
ll_risposte[1] = 0
ll_risposte[2] = 0
ll_risposte[3] = 0
ll_risposte[4] = 0
ll_risposte[5] = 0
ll_risposte[6] = 0

open cu_questionario;

do while true
	
	fetch cu_questionario
	into :ls_flag_tipo_risposta,
	     :ll_num_reg_lista,
		  :ll_prog_liste_con_comp,
		  :ll_num_versione,
		  :ll_num_edizione,
		  :ls_des_domanda,
		  :ls_check_button,
		  :ls_option_button,
		  :ll_peso_check_1,
		  :ll_peso_check_2,
		  :ll_peso_option_1,
		  :ll_peso_option_2,
		  :ll_peso_option_3,
		  :ll_peso_option_4,
		  :ll_peso_option_5,
		  :ll_peso_option_libero,
		  :ls_des_option_libero,
		  :ll_importanza_domanda,
		  :ll_prog_riga_liste_contr;
	
	if sqlca.sqlcode = 100 then
		
		if ll_risposte[1] > 0 or ll_risposte[2] > 0 or ll_risposte[3] > 0 or ll_risposte[4] > 0 or ll_risposte[5] > 0 or ll_risposte[6] > 0 then
			wf_ordinamento(ll_risposte[], ll_riga, tab_1.tabpage_3.dw_hit_risposte)
		end if
		
		exit
		
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in fetch cursore cu_questionario. Dettaglio errore " + sqlca.sqlerrtext)
		exit
	end if
	
	select range_importanza
	into   :ll_range_importanza
	from   tes_liste_controllo
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       num_reg_lista = :ll_num_reg_lista  and
			 num_versione  = :ll_num_versione and
			 num_edizione  = :ll_num_edizione;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in ricerca range importanza in testata lista controllo")
		return
	end if
	
	if ll_prog_riga_liste_contr <> ll_prog_riga_prec then
		
		if ll_risposte[1] > 0 or ll_risposte[2] > 0 or ll_risposte[3] > 0 or ll_risposte[4] > 0 or ll_risposte[5] > 0 then			
			wf_ordinamento(ll_risposte[], ll_riga, tab_1.tabpage_3.dw_hit_risposte)
		end if
		
		ll_numero_domanda ++
		ll_num_domande = 0
		ll_prog_riga_prec = ll_prog_riga_liste_contr
		ll_risposte[1] = 0
		ll_risposte[2] = 0
		ll_risposte[3] = 0
		ll_risposte[4] = 0
		ll_risposte[5] = 0
		ll_risposte[6] = 0
		
		ll_riga = tab_1.tabpage_3.dw_hit_risposte.insertrow(0)
		tab_1.tabpage_3.dw_hit_risposte.setitem(ll_riga, "des_domanda", "(" + string(ll_numero_domanda) + ") " + ls_des_domanda)
		
	end if
		
	ll_num_domande ++		
	
	choose case ls_flag_tipo_risposta
			
		case "C"
			
			if ls_check_button = "S" then
				ll_risposte[1] ++
			else
				ll_risposte[2] ++
			end if
			
		case "O"
			
			choose case ls_option_button
				case "1"
					ll_risposte[1] ++
				case "2"
					ll_risposte[2] ++
				case "3"
					ll_risposte[3] ++
				case "4"
					ll_risposte[4] ++
				case "5"
					ll_risposte[5] ++
				case "L"
					ll_risposte[6] ++
			end choose
			
	end choose
	
loop

commit;

close cu_questionario;

tab_1.tabpage_3.enabled = true
tab_1.tabpage_3.dw_hit_risposte.setredraw(true)
end subroutine

public subroutine wf_media_risposte ();string   ls_flag_tipo_risposta,ls_check_button, ls_option_button, ls_des_option_libero
long     ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp, ll_peso_check_1, &
         ll_peso_check_2, ll_peso_option_1, ll_peso_option_2, ll_peso_option_3, ll_peso_option_4, ll_peso_option_5, &
	      ll_peso_option_libero, ll_importanza_domanda, ll_prog_riga_liste_contr, ll_prog_riga_prec, ll_num_domande, &
	      ll_punteggio, ll_riga, ll_numero_domanda
dec{4}   ld_media_domanda		 
datetime ldt_data_inizio, ldt_data_fine
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_questionari_grafico_1"
lds_dati.settransobject(sqlca)

declare  cu_questionario cursor for
select   det_liste_con_comp.flag_tipo_risposta,   
         tes_liste_con_comp.num_reg_lista,   
         tes_liste_con_comp.prog_liste_con_comp,   
         tes_liste_con_comp.num_versione,   
         tes_liste_con_comp.num_edizione,   
         det_liste_con_comp.check_button,   
         det_liste_con_comp.option_button,   
         det_liste_con_comp.peso_check_1,   
         det_liste_con_comp.peso_check_2,   
         det_liste_con_comp.peso_option_1,   
         det_liste_con_comp.peso_option_2,   
         det_liste_con_comp.peso_option_3,   
         det_liste_con_comp.peso_option_4,   
         det_liste_con_comp.peso_option_5,   
         det_liste_con_comp.peso_option_libero,   
         det_liste_con_comp.des_option_libero,   
         det_liste_con_comp.importanza_domanda,   
         det_liste_con_comp.prog_riga_liste_contr  
from     tes_liste_con_comp,   
         det_liste_con_comp  
where    ( det_liste_con_comp.cod_azienda = tes_liste_con_comp.cod_azienda ) and  
         ( det_liste_con_comp.num_reg_lista = tes_liste_con_comp.num_reg_lista ) and  
         ( det_liste_con_comp.prog_liste_con_comp = tes_liste_con_comp.prog_liste_con_comp ) and  
         ( ( tes_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda ) and  
         ( tes_liste_con_comp.num_reg_lista = :ll_num_reg_lista ) and  
         ( tes_liste_con_comp.num_versione = :ll_num_versione ) and  
         ( tes_liste_con_comp.num_edizione = :ll_num_edizione ) and
			( tes_liste_con_comp.data_registrazione between :ldt_data_inizio and :ldt_data_fine) )   
order by tes_liste_con_comp.num_reg_lista asc,   
         tes_liste_con_comp.num_versione asc,   
         tes_liste_con_comp.num_edizione asc,
			det_liste_con_comp.prog_riga_liste_contr asc,
			tes_liste_con_comp.prog_liste_con_comp asc;

ll_num_reg_lista = tab_1.tabpage_1.dw_lista_questionari.getitemnumber(tab_1.tabpage_1.dw_lista_questionari.getrow(),"num_reg_lista")
ll_num_versione = tab_1.tabpage_1.dw_lista_questionari.getitemnumber(tab_1.tabpage_1.dw_lista_questionari.getrow(),"num_versione")
ll_num_edizione = tab_1.tabpage_1.dw_lista_questionari.getitemnumber(tab_1.tabpage_1.dw_lista_questionari.getrow(),"num_edizione")
ldt_data_inizio = datetime(date(tab_1.tabpage_1.em_data_inizio.text), 00:00:00)
ldt_data_fine   = datetime(date(tab_1.tabpage_1.em_data_fine.text), 00:00:00)

if isnull(ldt_data_inizio) or ldt_data_inizio < datetime(date("01/01/1900"), 00:00:00) then
	g_mb.messagebox("OMNIA","Impostare dei limiti di data inizio e data fine validi")
	destroy lds_dati
	return
end if

if isnull(ldt_data_fine) or ldt_data_fine < datetime(date("01/01/1900"), 00:00:00) then
	g_mb.messagebox("OMNIA","Impostare dei limiti di data inizio e data fine validi")
	destroy lds_dati
	return
end if

if ldt_data_inizio > ldt_data_fine then
	g_mb.messagebox("OMNIA","Limiti di date incoerenti!")
	destroy lds_dati
	return
end if

//tab_1.tabpage_2.dw_grafico_1.reset()
ll_prog_riga_prec = 0
ll_numero_domanda = 0
ll_num_domande = 0
ll_punteggio = 0

open cu_questionario;

do while true
	
	fetch cu_questionario
	into :ls_flag_tipo_risposta,
	     :ll_num_reg_lista,
		  :ll_prog_liste_con_comp,
		  :ll_num_versione,
		  :ll_num_edizione,
		  :ls_check_button,
		  :ls_option_button,
		  :ll_peso_check_1,
		  :ll_peso_check_2,
		  :ll_peso_option_1,
		  :ll_peso_option_2,
		  :ll_peso_option_3,
		  :ll_peso_option_4,
		  :ll_peso_option_5,
		  :ll_peso_option_libero,
		  :ls_des_option_libero,
		  :ll_importanza_domanda,
		  :ll_prog_riga_liste_contr;	
	
	if sqlca.sqlcode = 100 then
		
		if ll_num_domande > 0 then			
			ld_media_domanda = ll_punteggio / ll_num_domande
			/*
			ll_riga = tab_1.tabpage_2.dw_grafico_1.insertrow(0)
			tab_1.tabpage_2.dw_grafico_1.setitem(ll_riga, "asse_x", ll_numero_domanda)
			tab_1.tabpage_2.dw_grafico_1.setitem(ll_riga, "asse_y", ld_media_domanda)		
			*/
			ll_riga = lds_dati.insertrow(0)
			lds_dati.setitem(ll_riga, "asse_x", ll_numero_domanda)
			lds_dati.setitem(ll_riga, "asse_y", ld_media_domanda)
		end if
		
		exit
		
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in fetch cursore cu_questionario. Dettaglio errore " + sqlca.sqlerrtext)
		exit
	end if
	
	if ll_prog_riga_liste_contr <> ll_prog_riga_prec then
		
		if ll_num_domande > 0 then			
			ld_media_domanda = ll_punteggio / ll_num_domande
			/*
			ll_riga = tab_1.tabpage_2.dw_grafico_1.insertrow(0)
			tab_1.tabpage_2.dw_grafico_1.setitem(ll_riga, "asse_x", ll_numero_domanda)
			tab_1.tabpage_2.dw_grafico_1.setitem(ll_riga, "asse_y", ld_media_domanda)
			*/
			ll_riga = lds_dati.insertrow(0)
			lds_dati.setitem(ll_riga, "asse_x", ll_numero_domanda)
			lds_dati.setitem(ll_riga, "asse_y", ld_media_domanda)
		end if
		
		ll_numero_domanda ++
		ll_num_domande = 0
		ll_punteggio   = 0
		ll_prog_riga_prec = ll_prog_riga_liste_contr
		
	end if
		
	ll_num_domande ++

	choose case ls_flag_tipo_risposta
			
		case "C"
			
			if ls_check_button = "N" then
				ll_punteggio = ll_punteggio + ll_peso_check_1
			else
				ll_punteggio = ll_punteggio + ll_peso_check_2
			end if
			
		case "O"
			
			choose case ls_option_button
				case "1"
					ll_punteggio = ll_punteggio + ll_peso_option_1
				case "2"
					ll_punteggio = ll_punteggio + ll_peso_option_2
				case "3"
					ll_punteggio = ll_punteggio + ll_peso_option_3
				case "4"
					ll_punteggio = ll_punteggio + ll_peso_option_4
				case "5"
					ll_punteggio = ll_punteggio + ll_peso_option_5
				case "L"
					ll_punteggio = ll_punteggio + ll_peso_option_libero
			end choose				
			
	end choose
	
loop

commit;

//costruzione del grafico -------------------------------------------------------------
tab_1.tabpage_2.dw_graph_media_risposte.ib_datastore = true
tab_1.tabpage_2.dw_graph_media_risposte.ids_data = lds_dati

tab_1.tabpage_2.dw_graph_media_risposte.is_str[1] = "titolo"
tab_1.tabpage_2.dw_graph_media_risposte.is_str[2] = "label_x"
tab_1.tabpage_2.dw_graph_media_risposte.is_str[3] = "label_y"
tab_1.tabpage_2.dw_graph_media_risposte.is_num[1] = "asse_x"
tab_1.tabpage_2.dw_graph_media_risposte.is_num[2] = "asse_y"

tab_1.tabpage_2.dw_graph_media_risposte.is_source_categoria_col = "asse_x"

tab_1.tabpage_2.dw_graph_media_risposte.is_categoria_col = "num_1"
tab_1.tabpage_2.dw_graph_media_risposte.is_exp_valore = "num_2"
tab_1.tabpage_2.dw_graph_media_risposte.is_serie_col = ""

//altre impostazioni del grafico
tab_1.tabpage_2.dw_graph_media_risposte.is_titolo = "MEDIA RISPOSTE"
tab_1.tabpage_2.dw_graph_media_risposte.is_label_categoria= "Numero domanda"
tab_1.tabpage_2.dw_graph_media_risposte.is_label_valori = "Media"

//tipo scala: 1 se lineare, 2 se logaritmica in base 10
tab_1.tabpage_2.dw_graph_media_risposte.ii_scala = 1

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
tab_1.tabpage_2.dw_graph_media_risposte.ii_tipo_grafico = 7 //Colonne

//colore di sfondo del grafico
//per il bianco (DEFAULT)	: 16777215
//per il silver			: 12632256
tab_1.tabpage_2.dw_graph_media_risposte.il_backcolor = 16777215 //white

//metodo che visualizza i dati nel grafico
tab_1.tabpage_2.dw_graph_media_risposte.uof_retrieve( )
//--------------------------------------------------------------------------------------------
close cu_questionario;
destroy lds_dati

tab_1.tabpage_2.enabled = true

/*
tab_1.tabpage_2.enabled = true
tab_1.tabpage_2.dw_grafico_1.object.grafico.title = "MEDIA RISPOSTE"
tab_1.tabpage_2.dw_grafico_1.object.grafico.values.label = "Media"
tab_1.tabpage_2.dw_grafico_1.object.grafico.category.label = "Numero domanda"
tab_1.tabpage_2.dw_grafico_1.setredraw(true)
*/
end subroutine

event open;tab_1.tabpage_1.dw_lista_questionari.settransobject(sqlca)
tab_1.tabpage_1.dw_lista_questionari.setrowfocusindicator(Hand!)
tab_1.tabpage_1.dw_lista_questionari.retrieve(s_cs_xx.cod_azienda)
tab_1.tabpage_1.em_data_inizio.text = string("01/01/1900")
tab_1.tabpage_1.em_data_fine.text   = string("31/12/2999")
tab_1.tabpage_2.enabled = false
tab_1.tabpage_3.enabled = false
tab_1.tabpage_4.enabled = false

end event

on w_customer_satisfaction.create
this.tab_1=create tab_1
this.Control[]={this.tab_1}
end on

on w_customer_satisfaction.destroy
destroy(this.tab_1)
end on

type tab_1 from tab within w_customer_satisfaction
integer x = 23
integer y = 20
integer width = 3200
integer height = 1760
integer taborder = 1
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 80269524
boolean fixedwidth = true
boolean raggedright = true
alignment alignment = center!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3163
integer height = 1636
long backcolor = 80269524
string text = "Selezione"
long tabtextcolor = 33554432
long tabbackcolor = 80269524
long picturemaskcolor = 536870912
cb_calcola cb_calcola
em_data_fine em_data_fine
st_2 st_2
st_1 st_1
em_data_inizio em_data_inizio
dw_lista_questionari dw_lista_questionari
end type

on tabpage_1.create
this.cb_calcola=create cb_calcola
this.em_data_fine=create em_data_fine
this.st_2=create st_2
this.st_1=create st_1
this.em_data_inizio=create em_data_inizio
this.dw_lista_questionari=create dw_lista_questionari
this.Control[]={this.cb_calcola,&
this.em_data_fine,&
this.st_2,&
this.st_1,&
this.em_data_inizio,&
this.dw_lista_questionari}
end on

on tabpage_1.destroy
destroy(this.cb_calcola)
destroy(this.em_data_fine)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_data_inizio)
destroy(this.dw_lista_questionari)
end on

type cb_calcola from commandbutton within tabpage_1
integer x = 2747
integer y = 1532
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcola"
end type

event clicked;long ll_row


ll_row = tab_1.tabpage_1.dw_lista_questionari.getrow()

if isnull(ll_row) or ll_row < 1 then
	return -1
end if

wf_media_risposte()

wf_rilevanza_cumulata()

wf_frequenza_risposte()
end event

type em_data_fine from editmask within tabpage_1
integer x = 1193
integer y = 1532
integer width = 434
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "$"
string minmax = "01/01/1900~~31/12/2999"
end type

type st_2 from statictext within tabpage_1
integer x = 827
integer y = 1532
integer width = 343
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Data Fine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within tabpage_1
integer x = 5
integer y = 1532
integer width = 320
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "Data Inizio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_data_inizio from editmask within tabpage_1
integer x = 347
integer y = 1532
integer width = 434
integer height = 80
integer taborder = 15
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "À"
string minmax = "01/01/1900~~31/12/2999"
end type

type dw_lista_questionari from datawindow within tabpage_1
integer x = 5
integer y = 12
integer width = 3145
integer height = 1480
integer taborder = 10
string dataobject = "d_lista_questionari"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3163
integer height = 1636
long backcolor = 80269524
string text = "Media Risposte"
long tabtextcolor = 33554432
long tabbackcolor = 80269524
long picturemaskcolor = 536870912
dw_graph_media_risposte dw_graph_media_risposte
end type

on tabpage_2.create
this.dw_graph_media_risposte=create dw_graph_media_risposte
this.Control[]={this.dw_graph_media_risposte}
end on

on tabpage_2.destroy
destroy(this.dw_graph_media_risposte)
end on

type dw_graph_media_risposte from uo_cs_graph within tabpage_2
integer x = 9
integer y = 20
integer width = 3141
integer height = 1608
integer taborder = 11
end type

type tabpage_3 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3163
integer height = 1636
long backcolor = 80269524
string text = "Frequenza Risposte"
long tabtextcolor = 33554432
long tabbackcolor = 80269524
long picturemaskcolor = 536870912
dw_hit_risposte dw_hit_risposte
end type

on tabpage_3.create
this.dw_hit_risposte=create dw_hit_risposte
this.Control[]={this.dw_hit_risposte}
end on

on tabpage_3.destroy
destroy(this.dw_hit_risposte)
end on

type dw_hit_risposte from datawindow within tabpage_3
integer x = 5
integer y = 12
integer width = 3154
integer height = 1620
integer taborder = 2
string dataobject = "d_hit_risposte"
boolean border = false
boolean livescroll = true
end type

type tabpage_4 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3163
integer height = 1636
long backcolor = 80269524
string text = "Rilevanza Cumulata"
long tabtextcolor = 33554432
long tabbackcolor = 80269524
long picturemaskcolor = 536870912
dw_graph_rilevanza_cum dw_graph_rilevanza_cum
end type

on tabpage_4.create
this.dw_graph_rilevanza_cum=create dw_graph_rilevanza_cum
this.Control[]={this.dw_graph_rilevanza_cum}
end on

on tabpage_4.destroy
destroy(this.dw_graph_rilevanza_cum)
end on

type dw_graph_rilevanza_cum from uo_cs_graph within tabpage_4
integer x = 9
integer y = 24
integer width = 3145
integer height = 1604
integer taborder = 11
end type

