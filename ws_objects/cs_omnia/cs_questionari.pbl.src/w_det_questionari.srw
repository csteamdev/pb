﻿$PBExportHeader$w_det_questionari.srw
$PBExportComments$Dettaglio Questionari
forward
global type w_det_questionari from w_cs_xx_principale
end type
type dw_det_questionari_lista from uo_cs_xx_dw within w_det_questionari
end type
type dw_det_questionari_det_2 from uo_cs_xx_dw within w_det_questionari
end type
type dw_det_questionari_det_1 from uo_cs_xx_dw within w_det_questionari
end type
type dw_det_questionari_det_3 from uo_cs_xx_dw within w_det_questionari
end type
type dw_folder from u_folder within w_det_questionari
end type
end forward

global type w_det_questionari from w_cs_xx_principale
int Width=2369
int Height=1641
boolean TitleBar=true
string Title="Dettaglio Questionari"
dw_det_questionari_lista dw_det_questionari_lista
dw_det_questionari_det_2 dw_det_questionari_det_2
dw_det_questionari_det_1 dw_det_questionari_det_1
dw_det_questionari_det_3 dw_det_questionari_det_3
dw_folder dw_folder
end type
global w_det_questionari w_det_questionari

type variables

end variables

forward prototypes
public function integer wf_proteggi_campi (string ws_tipo_campo)
public function integer wf_calcola_progressivo ()
end prototypes

public function integer wf_proteggi_campi (string ws_tipo_campo);choose case ws_tipo_campo
case "T"
   dw_det_questionari_det_2.modify("check_button.protect=0")
   dw_det_questionari_det_2.modify("des_check_1.protect=0")
   dw_det_questionari_det_2.modify("peso_check_1.protect=0")
   dw_det_questionari_det_2.modify("des_check_2.protect=0")
   dw_det_questionari_det_2.modify("peso_check_2.protect=0")

   dw_det_questionari_det_3.modify("option_button.protect=0")
   dw_det_questionari_det_3.modify("des_option_1.protect=0")
   dw_det_questionari_det_3.modify("peso_option_1.protect=0")
   dw_det_questionari_det_3.modify("des_option_2.protect=0")
   dw_det_questionari_det_3.modify("peso_option_2.protect=0")
   dw_det_questionari_det_3.modify("des_option_3.protect=0")
   dw_det_questionari_det_3.modify("peso_option_3.protect=0")
   dw_det_questionari_det_3.modify("des_option_4.protect=0")
   dw_det_questionari_det_3.modify("peso_option_4.protect=0")
   dw_det_questionari_det_3.modify("des_option_5.protect=0")
   dw_det_questionari_det_3.modify("peso_option_5.protect=0")
   dw_det_questionari_det_3.modify("des_option_libero.protect=0")
   dw_det_questionari_det_3.modify("peso_option_libero.protect=0")
   dw_det_questionari_det_3.modify("num_opzioni.protect=0")

   dw_det_questionari_det_1.modify("campo_libero.protect=0")
   dw_det_questionari_det_1.modify("des_subtotale.protect=0")

case "L"
   dw_det_questionari_det_2.modify("check_button.protect=1")
   dw_det_questionari_det_2.modify("des_check_1.protect=1")
   dw_det_questionari_det_2.modify("peso_check_1.protect=1")
   dw_det_questionari_det_2.modify("des_check_2.protect=1")
   dw_det_questionari_det_2.modify("peso_check_2.protect=1")

   dw_det_questionari_det_3.modify("option_button.protect=1")
   dw_det_questionari_det_3.modify("des_option_1.protect=1")
   dw_det_questionari_det_3.modify("peso_option_1.protect=1")
   dw_det_questionari_det_3.modify("des_option_2.protect=1")
   dw_det_questionari_det_3.modify("peso_option_2.protect=1")
   dw_det_questionari_det_3.modify("des_option_3.protect=1")
   dw_det_questionari_det_3.modify("peso_option_3.protect=1")
   dw_det_questionari_det_3.modify("des_option_4.protect=1")
   dw_det_questionari_det_3.modify("peso_option_4.protect=1")
   dw_det_questionari_det_3.modify("des_option_5.protect=1")
   dw_det_questionari_det_3.modify("peso_option_5.protect=1")
   dw_det_questionari_det_3.modify("des_option_libero.protect=1")
   dw_det_questionari_det_3.modify("peso_option_libero.protect=1")
   dw_det_questionari_det_3.modify("num_opzioni.protect=1")

   dw_det_questionari_det_1.modify("campo_libero.protect=0")

   dw_det_questionari_det_1.modify("des_subtotale.protect=1")

case "C"
   dw_det_questionari_det_2.modify("check_button.protect=0")
   dw_det_questionari_det_2.modify("des_check_1.protect=0")
   dw_det_questionari_det_2.modify("peso_check_1.protect=0")
   dw_det_questionari_det_2.modify("des_check_2.protect=0")
   dw_det_questionari_det_2.modify("peso_check_2.protect=0")

   dw_det_questionari_det_3.modify("option_button.protect=1")
   dw_det_questionari_det_3.modify("des_option_1.protect=1")
   dw_det_questionari_det_3.modify("peso_option_1.protect=1")
   dw_det_questionari_det_3.modify("des_option_2.protect=1")
   dw_det_questionari_det_3.modify("peso_option_2.protect=1")
   dw_det_questionari_det_3.modify("des_option_3.protect=1")
   dw_det_questionari_det_3.modify("peso_option_3.protect=1")
   dw_det_questionari_det_3.modify("des_option_4.protect=1")
   dw_det_questionari_det_3.modify("peso_option_4.protect=1")
   dw_det_questionari_det_3.modify("des_option_5.protect=1")
   dw_det_questionari_det_3.modify("peso_option_5.protect=1")
   dw_det_questionari_det_3.modify("des_option_libero.protect=1")
   dw_det_questionari_det_3.modify("peso_option_libero.protect=1")
   dw_det_questionari_det_3.modify("num_opzioni.protect=1")

   dw_det_questionari_det_1.modify("campo_libero.protect=1")

   dw_det_questionari_det_1.modify("des_subtotale.protect=1")

case "O"
   dw_det_questionari_det_2.modify("check_button.protect=1")
   dw_det_questionari_det_2.modify("des_check_1.protect=1")
   dw_det_questionari_det_2.modify("peso_check_1.protect=1")
   dw_det_questionari_det_2.modify("des_check_2.protect=1")
   dw_det_questionari_det_2.modify("peso_check_2.protect=1")

   dw_det_questionari_det_3.modify("option_button.protect=0")
   dw_det_questionari_det_3.modify("des_option_1.protect=0")
   dw_det_questionari_det_3.modify("peso_option_1.protect=0")
   dw_det_questionari_det_3.modify("des_option_2.protect=0")
   dw_det_questionari_det_3.modify("peso_option_2.protect=0")
   dw_det_questionari_det_3.modify("des_option_3.protect=0")
   dw_det_questionari_det_3.modify("peso_option_3.protect=0")
   dw_det_questionari_det_3.modify("des_option_4.protect=0")
   dw_det_questionari_det_3.modify("peso_option_4.protect=0")
   dw_det_questionari_det_3.modify("des_option_5.protect=0")
   dw_det_questionari_det_3.modify("peso_option_5.protect=0")
   dw_det_questionari_det_3.modify("des_option_libero.protect=0")
   dw_det_questionari_det_3.modify("peso_option_libero.protect=0")
   dw_det_questionari_det_3.modify("num_opzioni.protect=0")

   dw_det_questionari_det_1.modify("campo_libero.protect=1")

   dw_det_questionari_det_1.modify("des_subtotale.protect=1")

case "S"
   dw_det_questionari_det_2.modify("check_button.protect=1")
   dw_det_questionari_det_2.modify("des_check_1.protect=1")
   dw_det_questionari_det_2.modify("peso_check_1.protect=1")
   dw_det_questionari_det_2.modify("des_check_2.protect=1")
   dw_det_questionari_det_2.modify("peso_check_2.protect=1")

   dw_det_questionari_det_3.modify("option_button.protect=1")
   dw_det_questionari_det_3.modify("des_option_1.protect=1")
   dw_det_questionari_det_3.modify("peso_option_1.protect=1")
   dw_det_questionari_det_3.modify("des_option_2.protect=1")
   dw_det_questionari_det_3.modify("peso_option_2.protect=1")
   dw_det_questionari_det_3.modify("des_option_3.protect=1")
   dw_det_questionari_det_3.modify("peso_option_3.protect=1")
   dw_det_questionari_det_3.modify("des_option_4.protect=1")
   dw_det_questionari_det_3.modify("peso_option_4.protect=1")
   dw_det_questionari_det_3.modify("des_option_5.protect=1")
   dw_det_questionari_det_3.modify("peso_option_5.protect=1")
   dw_det_questionari_det_3.modify("des_option_libero.protect=1")
   dw_det_questionari_det_3.modify("peso_option_libero.protect=1")
   dw_det_questionari_det_3.modify("num_opzioni.protect=1")

   dw_det_questionari_det_1.modify("campo_libero.protect=1")

   dw_det_questionari_det_1.modify("des_subtotale.protect=0")

end choose
return 0

end function

public function integer wf_calcola_progressivo ();long ll_num_registrazione, ll_num_reg, ll_versione, ll_edizione

ll_num_reg = dw_det_questionari_lista.i_parentdw.getitemnumber(dw_det_questionari_lista.i_parentdw.i_selectedrows[1], "num_reg_lista")
ll_versione = dw_det_questionari_lista.i_parentdw.getitemnumber(dw_det_questionari_lista.i_parentdw.i_selectedrows[1], "num_versione")
ll_edizione = dw_det_questionari_lista.i_parentdw.getitemnumber(dw_det_questionari_lista.i_parentdw.i_selectedrows[1], "num_edizione")

select max(det_liste_controllo.prog_riga_liste_contr)
  into :ll_num_registrazione
  from det_liste_controllo
  where (det_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda) and (det_liste_controllo.num_reg_lista = :ll_num_reg) and (det_liste_controllo.num_versione = :ll_versione) and (det_liste_controllo.num_edizione = :ll_edizione)  ;

if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
    dw_det_questionari_lista.SetItem(dw_det_questionari_lista.GetRow(),"prog_riga_liste_contr", 10 )
else
   if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
      ll_num_registrazione = 10
   else
      ll_num_registrazione = ll_num_registrazione + 10
   end if
   dw_det_questionari_lista.SetItem(dw_det_questionari_lista.GetRow(),"prog_riga_liste_contr", ll_num_registrazione)
end if
return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_det_questionari_det_1
dw_folder.fu_AssignTab(1, "&Dati Generici", l_Objects[])
l_objects[1] = dw_det_questionari_det_2
dw_folder.fu_AssignTab(2, "&Stile Si No", l_Objects[])
l_objects[1] = dw_det_questionari_det_3
dw_folder.fu_AssignTab(3, "Stile &Opzione", l_Objects[])

dw_folder.fu_FolderCreate(3,4)

dw_det_questionari_lista.set_dw_key("cod_azienda")
dw_det_questionari_lista.set_dw_key("num_reg_lista")
dw_det_questionari_lista.set_dw_key("num_versione")
dw_det_questionari_lista.set_dw_key("num_edizione")

dw_det_questionari_lista.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)
dw_det_questionari_det_1.set_dw_options(sqlca,dw_det_questionari_lista,c_sharedata+c_scrollparent,c_default)
dw_det_questionari_det_2.set_dw_options(sqlca,dw_det_questionari_lista,c_sharedata+c_scrollparent,c_default)
dw_det_questionari_det_3.set_dw_options(sqlca,dw_det_questionari_lista,c_sharedata+c_scrollparent,c_default)

//dw_det_questionari_det_1.ib_proteggi_chiavi = false
uo_dw_main = dw_det_questionari_lista
dw_folder.fu_SelectTab(1)













end event

on pc_new;call w_cs_xx_principale::pc_new;//ib_in_new = true
end on

on w_det_questionari.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_questionari_lista=create dw_det_questionari_lista
this.dw_det_questionari_det_2=create dw_det_questionari_det_2
this.dw_det_questionari_det_1=create dw_det_questionari_det_1
this.dw_det_questionari_det_3=create dw_det_questionari_det_3
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_questionari_lista
this.Control[iCurrent+2]=dw_det_questionari_det_2
this.Control[iCurrent+3]=dw_det_questionari_det_1
this.Control[iCurrent+4]=dw_det_questionari_det_3
this.Control[iCurrent+5]=dw_folder
end on

on w_det_questionari.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_questionari_lista)
destroy(this.dw_det_questionari_det_2)
destroy(this.dw_det_questionari_det_1)
destroy(this.dw_det_questionari_det_3)
destroy(this.dw_folder)
end on

type dw_det_questionari_lista from uo_cs_xx_dw within w_det_questionari
int X=23
int Y=21
int Width=2286
int Height=501
int TabOrder=20
string DataObject="d_det_questionari_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;if isnull(this.getitemstring(this.getrow(), "des_domanda")) or len(this.getitemstring(this.getrow(), "des_domanda")) < 1 then
   g_mb.messagebox("Dettaglio Lista di Controllo","L'indicazione della domanda è obbligatorio", StopSign!)
   pcca.error = c_valfailed
   return
end if

end event

event pcd_new;call super::pcd_new;//long ll_num_registrazione, ll_num_reg, ll_versione, ll_edizione
wf_calcola_progressivo()
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx, ll_num_reg, ll_versione, ll_edizione

ll_num_reg = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
ll_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
ll_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      dw_det_questionari_lista.SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   IF IsNull(GetItemnumber(l_Idx, "num_reg_lista"))  or GetItemnumber(l_Idx, "num_reg_lista") < 1 THEN
      dw_det_questionari_lista.SetItem(l_Idx, "num_reg_lista", ll_num_reg)
   END IF
   IF IsNull(GetItemnumber(l_Idx, "num_versione")) or GetItemnumber(l_Idx, "num_versione") < 1 THEN
      dw_det_questionari_lista.SetItem(l_Idx, "num_versione", ll_versione)
   END IF
   IF IsNull(GetItemnumber(l_Idx, "num_edizione")) or GetItemnumber(l_Idx, "num_edizione") < 1 THEN
      dw_det_questionari_lista.SetItem(l_Idx, "num_edizione", ll_edizione)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore, ll_num_reg, ll_edizione, ll_versione


ll_num_reg  = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
ll_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
ll_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_num_reg, ll_versione, ll_edizione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end on

on updateend;call uo_cs_xx_dw::updateend;wf_proteggi_campi("T")
end on

type dw_det_questionari_det_2 from uo_cs_xx_dw within w_det_questionari
int X=115
int Y=701
int Width=2035
int Height=797
int TabOrder=50
string DataObject="d_det_questionari_det_2"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event pcd_new;call super::pcd_new;wf_calcola_progressivo()
end event

type dw_det_questionari_det_1 from uo_cs_xx_dw within w_det_questionari
int X=92
int Y=661
int Width=2012
int Height=841
int TabOrder=30
string DataObject="d_det_questionari_det_1"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

on pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol;if i_extendmode then 
   wf_proteggi_campi(i_coltext)
end if

end on

on pcd_view;call uo_cs_xx_dw::pcd_view;//if getrow() > 0 then wf_nascondi_campi(getitemstring(getrow(),"flag_tipo_risposta"), dw_det_questionari_det)

end on

event pcd_new;call super::pcd_new;wf_calcola_progressivo()
end event

type dw_det_questionari_det_3 from uo_cs_xx_dw within w_det_questionari
int X=69
int Y=701
int Width=2195
int Height=797
int TabOrder=40
string DataObject="d_det_questionari_det_3"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event pcd_new;call super::pcd_new;wf_calcola_progressivo()
end event

type dw_folder from u_folder within w_det_questionari
int X=23
int Y=541
int Width=2286
int Height=981
int TabOrder=10
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "&Dati Generici"
      SetFocus(dw_det_questionari_det_1)
   CASE "&Stile Si No"
      SetFocus(dw_det_questionari_det_2)
   CASE "Stile &Opzione"
      SetFocus(dw_det_questionari_det_3)
END CHOOSE

end on

