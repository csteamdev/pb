﻿$PBExportHeader$w_det_questionari_comp.srw
$PBExportComments$Compilazione Questionario
forward
global type w_det_questionari_comp from w_cs_xx_risposta
end type
type cb_1 from uo_cb_close within w_det_questionari_comp
end type
type st_azione from statictext within w_det_questionari_comp
end type
type dw_det_questionari_comp from uo_cs_xx_dw within w_det_questionari_comp
end type
end forward

global type w_det_questionari_comp from w_cs_xx_risposta
int Width=3489
int Height=1741
boolean TitleBar=true
string Title="Compilazione Questionari"
cb_1 cb_1
st_azione st_azione
dw_det_questionari_comp dw_det_questionari_comp
end type
global w_det_questionari_comp w_det_questionari_comp

type variables
long il_totale_punteggio, il_punteggio_riferimento
end variables

forward prototypes
public function integer wf_calcola_totali ()
public function integer wf_totalizza ()
end prototypes

public function integer wf_calcola_totali ();long  ll_i, ll_subtot, ll_num, ll_riferimento, ll_totale
long  ll_num_reg_lista, ll_prog_liste_con_comp, ll_valore_riferimento
string ls_str


if dw_det_questionari_comp.rowcount() > 0 then 

   il_totale_punteggio = 0
   ll_subtot = 0
   ll_totale = 0
   for ll_i = 1 to dw_det_questionari_comp.rowcount()
      
      choose case dw_det_questionari_comp.getitemstring(ll_i, "flag_tipo_risposta")
         case "C" 
            if ll_i = dw_det_questionari_comp.getrow() then
               ls_str = dw_det_questionari_comp.i_coltext
            else
               ls_str = dw_det_questionari_comp.getitemstring(ll_i, "check_button")
            end if

            if ls_str = "N" then
               ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_check_1")
               ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_check_1")
            else
               ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_check_2")
               ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_check_2")
            end if
            dw_det_questionari_comp.setitem(ll_i, "val_subtotale", 0)
         case "O" 
            if ll_i = dw_det_questionari_comp.getrow() then
               ls_str = dw_det_questionari_comp.i_coltext
            else
               ls_str = dw_det_questionari_comp.getitemstring(ll_i, "option_button")
            end if
            choose case ls_str
               case "1"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_1")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_1")
               case "2"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_2")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_2")
               case "3"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_3")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_3")
               case "4"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_4")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_4")
               case "5"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_5")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_5")
               case "L"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_libero")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_libero")
            end choose
            dw_det_questionari_comp.setitem(ll_i, "val_subtotale", 0)

         case "L"
            dw_det_questionari_comp.setitem(ll_i, "val_subtotale", 0)

         case "S"
            dw_det_questionari_comp.setitem(ll_i, "val_subtotale", ll_subtot)
            ll_subtot = 0
      end choose  
   next
ls_str = "cf_tot.expression='" + string(ll_totale) + "'"
dw_det_questionari_comp.Modify(ls_str)
il_totale_punteggio = ll_totale

choose case il_punteggio_riferimento
	case is <= il_totale_punteggio
	st_azione.text = "Punteggio superiore a quello di riferimento: verificare azione da intraprendere"
	st_azione.backcolor = 65280
	case is > il_totale_punteggio
	st_azione.text = "Punteggio inferiore a quello di riferimento: verificare azione da intraprendere"
	st_azione.backcolor = 255
end choose

end if

return 0

end function

public function integer wf_totalizza ();long ll_i, ll_subtot, ll_num, ll_riferimento, ll_totale
string ls_str


if dw_det_questionari_comp.rowcount() > 0 then 

   il_totale_punteggio = 0
   ll_subtot = 0
   ll_totale = 0
   for ll_i = 1 to dw_det_questionari_comp.rowcount()
      
      choose case dw_det_questionari_comp.getitemstring(ll_i, "flag_tipo_risposta")
         case "C" 
            ls_str = dw_det_questionari_comp.getitemstring(ll_i, "check_button")
            if ls_str = "N" then
               ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_check_1")
               ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_check_1")
            else
               ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_check_2")
               ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_check_2")
            end if
            dw_det_questionari_comp.setitem(ll_i, "val_subtotale", 0)
         case "O" 
            ls_str = dw_det_questionari_comp.getitemstring(ll_i, "option_button")
            choose case ls_str
               case "1"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_1")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_1")
               case "2"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_2")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_2")
               case "3"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_3")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_3")
               case "4"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_4")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_4")
               case "5"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_5")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_5")
               case "L"
                  ll_subtot = ll_subtot + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_libero")
                  ll_totale = ll_totale + dw_det_questionari_comp.getitemnumber(ll_i, "peso_option_libero")
            end choose
            dw_det_questionari_comp.setitem(ll_i, "val_subtotale", 0)

         case "L"
            dw_det_questionari_comp.setitem(ll_i, "val_subtotale", 0)

         case "S"
            dw_det_questionari_comp.setitem(ll_i, "val_subtotale", ll_subtot)
            ll_subtot = 0
      end choose  
   next
ls_str = "cf_tot.expression='" + string(ll_totale) + "'"
dw_det_questionari_comp.Modify(ls_str)
il_totale_punteggio = ll_totale
end if

return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_det_questionari_comp.set_dw_options(sqlca, i_openparm, c_modifyonopen, &
                                       c_nohighlightselected +&
                                       c_cursorrowpointer)

dw_det_questionari_comp.Set_Row_Indicator(c_SetRowIndicatorNormal, &
                         Hand!, c_NullPicture, -15, 0)

Save_On_Close(c_SOCSave)

// ------------  visualizza risultato confronto con punteggio di riferimento ----------------

long ll_num_reg_lista, ll_prog_liste_con_comp, ll_punteggio_totale

ll_num_reg_lista       = dw_det_questionari_comp.i_parentdw.getitemnumber(dw_det_questionari_comp.i_parentdw.i_selectedrows[1], "num_reg_lista")
ll_prog_liste_con_comp = dw_det_questionari_comp.i_parentdw.getitemnumber(dw_det_questionari_comp.i_parentdw.i_selectedrows[1], "prog_liste_con_comp")

SELECT tes_liste_con_comp.valore_riferimento, tes_liste_con_comp.valore_ottenuto
INTO   :il_punteggio_riferimento, :ll_punteggio_totale
FROM   tes_liste_con_comp  
WHERE  ( tes_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_con_comp.num_reg_lista = :ll_num_reg_lista ) AND  
       ( tes_liste_con_comp.prog_liste_con_comp = :ll_prog_liste_con_comp )   ;

choose case il_punteggio_riferimento
	case is <= ll_punteggio_totale
	st_azione.text = "Punteggio superiore a quello di riferimento: verificare azione da intraprendere"
	st_azione.backcolor = 65280
	case is > ll_punteggio_totale
	st_azione.text = "Punteggio inferiore a quello di riferimento: verificare azione da intraprendere"
	st_azione.backcolor = 255
end choose

end event

on w_det_questionari_comp.create
int iCurrent
call w_cs_xx_risposta::create
this.cb_1=create cb_1
this.st_azione=create st_azione
this.dw_det_questionari_comp=create dw_det_questionari_comp
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_1
this.Control[iCurrent+2]=st_azione
this.Control[iCurrent+3]=dw_det_questionari_comp
end on

on w_det_questionari_comp.destroy
call w_cs_xx_risposta::destroy
destroy(this.cb_1)
destroy(this.st_azione)
destroy(this.dw_det_questionari_comp)
end on

type cb_1 from uo_cb_close within w_det_questionari_comp
int X=3063
int Y=1541
int Width=366
int Height=81
int TabOrder=10
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_azione from statictext within w_det_questionari_comp
int X=23
int Y=1541
int Width=2995
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_det_questionari_comp from uo_cs_xx_dw within w_det_questionari_comp
int X=23
int Y=1
int Width=3406
int Height=1501
int TabOrder=20
string DataObject="d_det_questionari_comp"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_update;call super::pcd_update;long   ll_num_reg_lista, ll_prog_liste_con_comp, ll_num_versione, ll_num_edizione
long	 ll_valore_riferimento
string ls_azione_se_minore, ls_azione_se_maggiore_uguale, ls_azione


ll_num_reg_lista       = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
ll_prog_liste_con_comp = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_liste_con_comp")

SELECT tes_liste_con_comp.num_versione,   
       tes_liste_con_comp.num_edizione , 
       tes_liste_con_comp.valore_riferimento  
  INTO :ll_num_versione,   
       :ll_num_edizione,
		 :ll_valore_riferimento
  FROM tes_liste_con_comp  
 WHERE ( tes_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_con_comp.num_reg_lista = :ll_num_reg_lista ) AND  
       ( tes_liste_con_comp.prog_liste_con_comp = :ll_prog_liste_con_comp )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Liste di Controllo","Errore SQL nr" + string(sqlca.sqlcode) + "durante SELECT su TES_LISTE_CONT_COMP", StopSign!)
else
   commit;
end if


SELECT tes_liste_controllo.azione_2,   
       tes_liste_controllo.azione_1  
  INTO :ls_azione_se_minore,   
       :ls_azione_se_maggiore_uguale  
  FROM tes_liste_controllo  
 WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_controllo.num_reg_lista = :ll_num_reg_lista ) AND  
       ( tes_liste_controllo.num_versione = :ll_num_versione ) AND  
       ( tes_liste_controllo.num_edizione = :ll_num_edizione )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Liste di Controllo","Errore SQL nr" + string(sqlca.sqlcode) + "durante SELECT su TES_LISTE_CONTROLLO", StopSign!)
else
   commit;
end if


if il_totale_punteggio > ll_valore_riferimento then
	ls_azione = ls_azione_se_maggiore_uguale
else
	ls_azione = ls_azione_se_minore
end if

UPDATE tes_liste_con_comp  
   SET valore_ottenuto = :il_totale_punteggio,
		 azione_intrapresa = :ls_azione
 WHERE ( tes_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_con_comp.num_reg_lista = :ll_num_reg_lista ) AND  
       ( tes_liste_con_comp.prog_liste_con_comp = :ll_prog_liste_con_comp )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Liste di Controllo","Attenzione: si è verificato un errore SQL durante l'aggiornamento del VALORE OTTENUTO; tale valore non risulta aggiornato", Information!)
else
   commit;
end if

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error, ll_num_reg_lista, ll_prog_liste_con_comp

ll_num_reg_lista       = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
ll_prog_liste_con_comp = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_liste_con_comp")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_num_reg_lista, ll_prog_liste_con_comp)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

wf_totalizza()
end event

on pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol;wf_calcola_totali()
end on

event getfocus;call super::getfocus;LONG  ll_num_reg_lista, ll_prog_liste_con_comp, ll_valore_riferimento

ll_num_reg_lista       = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
ll_prog_liste_con_comp = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_liste_con_comp")

SELECT tes_liste_con_comp.valore_riferimento  
INTO   :ll_valore_riferimento  
FROM   tes_liste_con_comp  
WHERE  ( tes_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_con_comp.num_reg_lista = :ll_num_reg_lista ) AND  
       ( tes_liste_con_comp.prog_liste_con_comp = :ll_prog_liste_con_comp )   ;


end event

event itemchanged;call super::itemchanged;if i_extendmode then

	LONG  ll_num_reg_lista, ll_prog_liste_con_comp, ll_valore_riferimento, ll_num_versione, ll_num_edizione, ll_dato, ll_range_importanza
	
	if i_colname = "importanza_domanda" then
		ll_num_reg_lista       = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
		ll_num_versione        = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
		ll_num_edizione        = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
		
		select range_importanza
		into   :ll_range_importanza
		from   tes_liste_controllo
		where  cod_azienda   = :s_cs_xx.cod_azienda and  
				 num_reg_lista = :ll_num_reg_lista and  
				 num_versione  = :ll_num_versione and
				 num_edizione  = :ll_num_edizione ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in ricerca range dalla lista")
			return
		end if
		
		ll_dato = long(data)
		if ll_dato < 1 or ll_dato > ll_range_importanza then
			g_mb.messagebox("OMNIA","Attenzione: non è possibile inserire un valore al di fuori del range previsto dal questionario", Information!)
			return 1
		end if
	end if
end if
end event

