﻿$PBExportHeader$w_report_questionario_comp.srw
$PBExportComments$Finestra Report Questionario Compilato
forward
global type w_report_questionario_comp from w_cs_xx_principale
end type
type dw_det_liste_contr_comp from uo_cs_xx_dw within w_report_questionario_comp
end type
end forward

global type w_report_questionario_comp from w_cs_xx_principale
integer width = 4014
integer height = 2828
string title = "Report Lista Controllo Compilata"
boolean hscrollbar = true
boolean vscrollbar = true
dw_det_liste_contr_comp dw_det_liste_contr_comp
end type
global w_report_questionario_comp w_report_questionario_comp

type variables
long il_totale_punteggio
end variables

forward prototypes
public function integer wf_totalizza ()
public function integer wf_calcola_totali ()
end prototypes

public function integer wf_totalizza ();long ll_i, ll_subtot, ll_num, ll_riferimento, ll_totale
string ls_str


if dw_det_liste_contr_comp.rowcount() > 0 then 

   il_totale_punteggio = 0
   ll_subtot = 0
   ll_totale = 0
   for ll_i = 1 to dw_det_liste_contr_comp.rowcount()
      
      choose case dw_det_liste_contr_comp.getitemstring(ll_i, "flag_tipo_risposta")
         case "C" 
            ls_str = dw_det_liste_contr_comp.getitemstring(ll_i, "check_button")
            if ls_str = "N" then
               ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_check_1")
               ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_check_1")
            else
               ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_check_2")
               ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_check_2")
            end if
            dw_det_liste_contr_comp.setitem(ll_i, "val_subtotale", 0)
         case "O" 
            ls_str = dw_det_liste_contr_comp.getitemstring(ll_i, "option_button")
            choose case ls_str
               case "1"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_1")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_1")
               case "2"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_2")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_2")
               case "3"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_3")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_3")
               case "4"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_4")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_4")
               case "5"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_5")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_5")
               case "L"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_libero")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_libero")
            end choose
            dw_det_liste_contr_comp.setitem(ll_i, "val_subtotale", 0)

         case "L"
            dw_det_liste_contr_comp.setitem(ll_i, "val_subtotale", 0)

         case "S"
            dw_det_liste_contr_comp.setitem(ll_i, "val_subtotale", ll_subtot)
            ll_subtot = 0
      end choose  
   next
ls_str = "cf_tot.expression='" + string(ll_totale) + "'"
dw_det_liste_contr_comp.Modify(ls_str)
il_totale_punteggio = ll_totale
end if

return 0
end function

public function integer wf_calcola_totali ();long ll_i, ll_subtot, ll_num, ll_riferimento, ll_totale
string ls_str


if dw_det_liste_contr_comp.rowcount() > 0 then 

   il_totale_punteggio = 0
   ll_subtot = 0
   ll_totale = 0
   for ll_i = 1 to dw_det_liste_contr_comp.rowcount()
      
      choose case dw_det_liste_contr_comp.getitemstring(ll_i, "flag_tipo_risposta")
         case "C" 
            if ll_i = dw_det_liste_contr_comp.getrow() then
               ls_str = dw_det_liste_contr_comp.i_coltext
            else
               ls_str = dw_det_liste_contr_comp.getitemstring(ll_i, "check_button")
            end if

            if ls_str = "N" then
               ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_check_1")
               ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_check_1")
            else
               ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_check_2")
               ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_check_2")
            end if
            dw_det_liste_contr_comp.setitem(ll_i, "val_subtotale", 0)
         case "O" 
            if ll_i = dw_det_liste_contr_comp.getrow() then
               ls_str = dw_det_liste_contr_comp.i_coltext
            else
               ls_str = dw_det_liste_contr_comp.getitemstring(ll_i, "option_button")
            end if
            choose case ls_str
               case "1"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_1")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_1")
               case "2"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_2")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_2")
               case "3"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_3")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_3")
               case "4"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_4")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_4")
               case "5"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_5")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_5")
               case "L"
                  ll_subtot = ll_subtot + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_libero")
                  ll_totale = ll_totale + dw_det_liste_contr_comp.getitemnumber(ll_i, "peso_option_libero")
            end choose
            dw_det_liste_contr_comp.setitem(ll_i, "val_subtotale", 0)

         case "L"
            dw_det_liste_contr_comp.setitem(ll_i, "val_subtotale", 0)

         case "S"
            dw_det_liste_contr_comp.setitem(ll_i, "val_subtotale", ll_subtot)
            ll_subtot = 0
      end choose  
   next
ls_str = "cf_tot.expression='" + string(ll_totale) + "'"
dw_det_liste_contr_comp.Modify(ls_str)
il_totale_punteggio = ll_totale
end if

return 0

end function

event pc_setwindow;call super::pc_setwindow;dw_det_liste_contr_comp.ib_dw_report = true

Save_On_Close(c_SOCnoSave)
dw_det_liste_contr_comp.set_dw_options(sqlca, &
                                       i_openparm, &
													c_nonew + &
                                       c_nodelete + &
													c_nomodify + &
													c_disableCC, &
                                       c_nohighlightselected)

iuo_dw_main = dw_det_liste_contr_comp


dw_det_liste_contr_comp.Object.DataWindow.Print.Preview='Yes'
dw_det_liste_contr_comp.Object.DataWindow.Print.Preview.rulers='Yes'

end event

on w_report_questionario_comp.create
int iCurrent
call super::create
this.dw_det_liste_contr_comp=create dw_det_liste_contr_comp
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_liste_contr_comp
end on

on w_report_questionario_comp.destroy
call super::destroy
destroy(this.dw_det_liste_contr_comp)
end on

event pc_view;call super::pc_view;Save_On_Close(c_SOCNoSave)
end event

type dw_det_liste_contr_comp from uo_cs_xx_dw within w_report_questionario_comp
integer x = 23
integer y = 20
integer width = 3840
integer height = 5100
integer taborder = 0
string dataobject = "d_report_questionario_comp"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;//wf_calcola_totali()
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error, ll_num_reg_lista, ll_prog_liste_con_comp, ll_valore_riferimento

ll_num_reg_lista       = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
ll_prog_liste_con_comp = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_liste_con_comp")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_num_reg_lista, ll_prog_liste_con_comp)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

wf_totalizza()

//ll_num_reg_lista       = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
//ll_prog_liste_con_comp = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_liste_con_comp")

SELECT tes_liste_con_comp.valore_riferimento  
INTO   :ll_valore_riferimento  
FROM   tes_liste_con_comp  
WHERE  ( tes_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_con_comp.num_reg_lista = :ll_num_reg_lista ) AND  
       ( tes_liste_con_comp.prog_liste_con_comp = :ll_prog_liste_con_comp )   ;

if not isnull(ll_valore_riferimento) and sqlca.sqlcode = 0 then
   dw_det_liste_contr_comp.Modify("cf_tot.Color='0~tIf( sum(val_subtotale  for all)>="+string(ll_valore_riferimento)+",65280,255)'")
end if

end event

event pcd_view;call super::pcd_view;Save_On_Close(c_SOCNoSave)
end event

