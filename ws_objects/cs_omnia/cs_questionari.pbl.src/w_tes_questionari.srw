﻿$PBExportHeader$w_tes_questionari.srw
$PBExportComments$Questionari
forward
global type w_tes_questionari from w_cs_xx_principale
end type
type dw_tes_questionari_lista from uo_cs_xx_dw within w_tes_questionari
end type
type cb_dettagli from commandbutton within w_tes_questionari
end type
type cb_verifica from commandbutton within w_tes_questionari
end type
type rb_ultime_edizioni from radiobutton within w_tes_questionari
end type
type rb_in_creazione from radiobutton within w_tes_questionari
end type
type rb_scadenza from radiobutton within w_tes_questionari
end type
type rb_storico from radiobutton within w_tes_questionari
end type
type gb_2 from groupbox within w_tes_questionari
end type
type rb_autorizza from radiobutton within w_tes_questionari
end type
type rb_valida from radiobutton within w_tes_questionari
end type
type rb_nuova_edizione from radiobutton within w_tes_questionari
end type
type cb_esegui from commandbutton within w_tes_questionari
end type
type rb_approva from radiobutton within w_tes_questionari
end type
type gb_1 from groupbox within w_tes_questionari
end type
type dw_tes_questionari_det_4 from uo_cs_xx_dw within w_tes_questionari
end type
type dw_tes_questionari_det_1 from uo_cs_xx_dw within w_tes_questionari
end type
type dw_tes_questionari_det_3 from uo_cs_xx_dw within w_tes_questionari
end type
type dw_folder from u_folder within w_tes_questionari
end type
end forward

global type w_tes_questionari from w_cs_xx_principale
integer width = 2885
integer height = 1892
string title = "Questionari"
dw_tes_questionari_lista dw_tes_questionari_lista
cb_dettagli cb_dettagli
cb_verifica cb_verifica
rb_ultime_edizioni rb_ultime_edizioni
rb_in_creazione rb_in_creazione
rb_scadenza rb_scadenza
rb_storico rb_storico
gb_2 gb_2
rb_autorizza rb_autorizza
rb_valida rb_valida
rb_nuova_edizione rb_nuova_edizione
cb_esegui cb_esegui
rb_approva rb_approva
gb_1 gb_1
dw_tes_questionari_det_4 dw_tes_questionari_det_4
dw_tes_questionari_det_1 dw_tes_questionari_det_1
dw_tes_questionari_det_3 dw_tes_questionari_det_3
dw_folder dw_folder
end type
global w_tes_questionari w_tes_questionari

type variables
boolean ib_delete=true, ib_new=false, ib_new_rec=false
long il_num_lista, il_versione, il_edizione, ll_new_lista
long il_tipo_operazione
string is_flag_valido, is_flag_in_prova
string is_tipo_records
date idt_da_data, idt_a_data
end variables

forward prototypes
public function integer wf_valida ()
public function integer wf_autorizza ()
public function integer wf_approva ()
public function integer wf_nuova_edizione ()
end prototypes

public function integer wf_valida ();string ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione
long ll_num_reg, ll_versione, ll_edizione, ll_riga_corrente


if is_tipo_records <> "E" then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Si possono validare solo le liste nelle ultime edizioni", StopSign!)
   return -1
end if

if isnull(dw_tes_questionari_lista.getitemstring(dw_tes_questionari_lista.getrow(),"approvato_da")) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo non ancora APPROVATA", StopSign!)
   return -1
end if

if isnull(dw_tes_questionari_lista.getitemstring(dw_tes_questionari_lista.getrow(),"autorizzato_da")) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo non ancora AUTORIZZATA", StopSign!)
   return -1
end if

if NOT(isnull(dw_tes_questionari_lista.getitemstring(dw_tes_questionari_lista.getrow(),"validato_da"))) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo già VALIDATA", StopSign!)
   return -1
end if

setpointer(hourglass!)
ll_riga_corrente = dw_tes_questionari_det_1.getrow()
ll_num_reg = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_reg_lista")
ll_versione = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_versione")
ll_edizione = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_edizione")

COMMIT;

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.cod_resp_divisione,
//       mansionari.cod_area_aziendale,   
//       mansionari.flag_approvazione,   
//       mansionari.flag_autorizzazione,   
//       mansionari.flag_validazione  
//INTO   :ls_cod_resp_divisione,
//       :ls_area_aziendale,   
//       :ls_flag_approvazione,   
//       :ls_flag_autorizzazione,   
//       :ls_flag_validazione  
//FROM   mansionari  
//WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

SELECT mansionari.cod_resp_divisione,
       mansionari.cod_area_aziendale
INTO   :ls_cod_resp_divisione,
       :ls_area_aziendale
FROM   mansionari  
WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
		 
uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario		 

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
//--- Fine modifica Giulio

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Validazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

if ls_flag_Validazione <> "S" then
   g_mb.messagebox("Validazione Lista di Controllo","L'utente non possiede il PRIVILEGIO di AUTORIZZAZIONE: verificare mansionario", StopSign!)
   return -1
end if 

//UPDATE tes_liste_controllo  
//SET    flag_valido = 'N'
//WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( tes_liste_controllo.num_reg_lista = :ll_num_reg );
//
//UPDATE tes_liste_controllo  
//SET    flag_valido = 'S'
//WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
//       ( tes_liste_controllo.num_versione  = :ll_versione ) AND  
//       ( tes_liste_controllo.num_edizione  = :ll_edizione )   ;

UPDATE tes_liste_controllo  
SET    validato_da = :s_cs_xx.cod_utente
WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
       ( tes_liste_controllo.num_versione  = :ll_versione ) AND  
       ( tes_liste_controllo.num_edizione  = :ll_edizione )   ;

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Validazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
   ROLLBACK;
else
   COMMIT;
end if
return 0
end function

public function integer wf_autorizza ();string ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione
string ls_approvato_da, ls_autorizzato_da
long ll_num_reg, ll_versione, ll_edizione, ll_riga_corrente
datetime ldt_oggi

if is_tipo_records <> "C" then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Si possono autorizzare solo le liste in fase di CREAZIONE", StopSign!)
   return -1
end if

if isnull(dw_tes_questionari_lista.getitemstring(dw_tes_questionari_lista.getrow(),"approvato_da")) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo non ancora APPROVATA", StopSign!)
   return -1
end if

if not(isnull(dw_tes_questionari_lista.getitemstring(dw_tes_questionari_lista.getrow(),"autorizzato_da"))) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo già AUTORIZZATA", StopSign!)
   return -1
end if


setpointer(HourGlass!)

ll_riga_corrente = dw_tes_questionari_det_1.getrow()
ll_num_reg = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_reg_lista")
ll_versione = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_versione")
ll_edizione = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_edizione")

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.cod_resp_divisione,
//       mansionari.cod_area_aziendale,   
//       mansionari.flag_approvazione,   
//       mansionari.flag_autorizzazione,   
//       mansionari.flag_validazione  
//INTO   :ls_cod_resp_divisione,
//       :ls_area_aziendale,   
//       :ls_flag_approvazione,   
//       :ls_flag_autorizzazione,   
//       :ls_flag_validazione  
//FROM   mansionari  
//WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

SELECT mansionari.cod_resp_divisione,
       mansionari.cod_area_aziendale
INTO   :ls_cod_resp_divisione,
       :ls_area_aziendale
FROM   mansionari  
WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
//--- Fine modifica Giulio

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Autorizzazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

if ls_flag_autorizzazione <> "S" then
   g_mb.messagebox("Autorizzazione Lista di Controllo","L'utente non possiede il PRIVILEGIO di AUTORIZZAZIONE: verificare mansionario", StopSign!)
   return -1
end if 

UPDATE tes_liste_controllo  
SET    flag_valido = 'N'
WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_controllo.num_reg_lista = :ll_num_reg );
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Autorizzazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

UPDATE tes_liste_controllo  
SET    flag_valido = 'S' ,
       flag_in_prova = 'N'
WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
       ( tes_liste_controllo.num_versione  = :ll_versione ) AND  
       ( tes_liste_controllo.num_edizione  = :ll_edizione )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Autorizzazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

ldt_oggi = datetime(today())    //  data autorizzazione da sistema

UPDATE tes_liste_controllo  
SET    autorizzato_da = :s_cs_xx.cod_utente,
       autorizzato_il = :ldt_oggi
WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
       ( tes_liste_controllo.num_versione  = :ll_versione ) AND  
       ( tes_liste_controllo.num_edizione  = :ll_edizione )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Autorizzazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

return 0

end function

public function integer wf_approva ();string ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione
long ll_num_reg, ll_versione, ll_edizione, ll_riga_corrente
if is_tipo_records <> "C" then
   g_mb.messagebox("Approvazione Lista di Controllo","Si possono approvare solo le liste in fase di CREAZIONE", StopSign!)
   return -1
end if
if not(isnull(dw_tes_questionari_lista.getitemstring(dw_tes_questionari_lista.getrow(),"approvato_da"))) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo già APPROVATA", StopSign!)
   return -1
end if

setpointer(hourglass!)

ll_riga_corrente = dw_tes_questionari_det_1.getrow()
ll_num_reg = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_reg_lista")
ll_versione = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_versione")
ll_edizione = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_edizione")

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.cod_resp_divisione,
//       mansionari.cod_area_aziendale,   
//       mansionari.flag_approvazione,   
//       mansionari.flag_autorizzazione,   
//       mansionari.flag_validazione  
//INTO   :ls_cod_resp_divisione,
//       :ls_area_aziendale,   
//       :ls_flag_approvazione,   
//       :ls_flag_autorizzazione,   
//       :ls_flag_validazione  
//FROM   mansionari  
//WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

SELECT mansionari.cod_resp_divisione,
       mansionari.cod_area_aziendale
INTO   :ls_cod_resp_divisione,
       :ls_area_aziendale
FROM   mansionari  
WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
//--- Fine modifica Giulio

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Approvazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

if ls_flag_approvazione <> "S" then
   g_mb.messagebox("Approvazione Lista di Controllo","L'utente non possiede il PRIVILEGIO di APPROVAZIONE: verificare mansionario", StopSign!)
   return -1
end if 


UPDATE tes_liste_controllo  
SET    approvato_da = :s_cs_xx.cod_utente
WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
       ( tes_liste_controllo.num_versione  = :ll_versione ) AND  
       ( tes_liste_controllo.num_edizione  = :ll_edizione )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Approvazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

return 0

end function

public function integer wf_nuova_edizione ();string ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione
string ls_null, ls_str
long ll_num_reg, ll_versione, ll_edizione, ll_riga_corrente, ll_new_edizione, ll_new_versione, ll_max_edizioni, ll_nuova_edizione
datetime ldt_scadenza_validazione, ldt_oggi, ldt_null

setnull(ls_null)
setnull(ldt_null)
if is_tipo_records <> "E" then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Si possono creare nuove revisioni solo da liste approvate/autorizzate", StopSign!)
   return -1
end if

if isnull(dw_tes_questionari_lista.getitemstring(dw_tes_questionari_lista.getrow(),"approvato_da")) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo non ancora APPROVATA", StopSign!)
   return -1
end if

if isnull(dw_tes_questionari_lista.getitemstring(dw_tes_questionari_lista.getrow(),"autorizzato_da")) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo non ancora AUTORIZZATA", StopSign!)
   return -1
end if

setpointer(hourglass!)

ll_riga_corrente = dw_tes_questionari_det_1.getrow()
ll_num_reg = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_reg_lista")

ll_versione = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_versione")
ll_edizione = dw_tes_questionari_det_1.getitemnumber(ll_riga_corrente,"num_edizione")
ll_nuova_edizione = ll_edizione + 1 

COMMIT;

SELECT tes_liste_controllo.cod_resp_divisione
 INTO :ls_str
 FROM tes_liste_controllo  
WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
		( tes_liste_controllo.num_versione = :ll_versione ) AND  
		( tes_liste_controllo.num_edizione = :ll_nuova_edizione )   ;
if sqlca.sqlcode = 0 then
	g_mb.messagebox("Nuova Edizione Lista di Controllo","INTERROMPO ELABORAZIONE: è già esistente una revisione di questa lista")
	ROLLBACK;
	return -1
end if

INSERT INTO tes_liste_controllo  
		( cod_azienda,   
		  num_reg_lista,   
		  num_versione,   
		  num_edizione,   
		  des_lista,   
		  cod_area_aziendale,   
		  cod_resp_divisione,   
		  approvato_da,   
		  emesso_il,   
		  autorizzato_da,   
		  autorizzato_il,   
		  valido_per,   
		  validato_da,   
		  flag_uso,   
		  flag_recuperato,   
		  note,   
		  flag_in_prova,   
		  flag_valido,   
		  scadenza_validazione,
		  valore_riferimento,
		  azione_1,
		  azione_2,
		  des_etichetta_1,
		  des_etichetta_2,
		  des_etichetta_3,
		  des_etichetta_4,
		  range_importanza,
		  flag_questionario)  
  SELECT tes_liste_controllo.cod_azienda,   
			tes_liste_controllo.num_reg_lista,   
			tes_liste_controllo.num_versione,   
			tes_liste_controllo.num_edizione + 1,   
			tes_liste_controllo.des_lista,   
			tes_liste_controllo.cod_area_aziendale,   
			tes_liste_controllo.cod_resp_divisione,   
			tes_liste_controllo.approvato_da,   
			tes_liste_controllo.emesso_il,   
			tes_liste_controllo.autorizzato_da,   
			tes_liste_controllo.autorizzato_il,   
			tes_liste_controllo.valido_per,   
			tes_liste_controllo.validato_da,   
			tes_liste_controllo.flag_uso,   
			tes_liste_controllo.flag_recuperato,   
			tes_liste_controllo.note,   
			tes_liste_controllo.flag_in_prova,   
			tes_liste_controllo.flag_valido,   
			tes_liste_controllo.scadenza_validazione,
			tes_liste_controllo.valore_riferimento,
			tes_liste_controllo.azione_1,
			tes_liste_controllo.azione_2,
			tes_liste_controllo.des_etichetta_1,
			tes_liste_controllo.des_etichetta_2,
			tes_liste_controllo.des_etichetta_3,
			tes_liste_controllo.des_etichetta_4,
			tes_liste_controllo.range_importanza,
			tes_liste_controllo.flag_questionario
	 FROM tes_liste_controllo  
	WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
			( tes_liste_controllo.num_versione = :ll_versione ) AND  
			( tes_liste_controllo.num_edizione = :ll_edizione )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in creazione nuova Revisione.~r~n" + sqlca.sqlerrtext)
	ROLLBACK;
	return -1
end if

SELECT mansionari.cod_resp_divisione  
INTO   :ls_cod_resp_divisione  
FROM   mansionari  
WHERE  (mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		 (mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in creazione nuova Revisione.~r~n" + sqlca.sqlerrtext)
	ROLLBACK;
	return -1
end if

setnull(ldt_scadenza_validazione)
ldt_oggi = datetime(today())
UPDATE tes_liste_controllo  
  SET flag_valido = 'N' ,
		flag_in_prova = 'S',
		cod_resp_divisione = :ls_cod_resp_divisione,
		valido_per = 0,
		scadenza_validazione = :ldt_scadenza_validazione,
		emesso_il = :ldt_oggi,
		autorizzato_il = :ldt_null
WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda )  AND  
		( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
		( tes_liste_controllo.num_versione = :ll_versione ) AND
		( tes_liste_controllo.num_edizione = :ll_nuova_edizione);
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in creazione nuova Revisione.~r~n" + sqlca.sqlerrtext)
	ROLLBACK;
	return -1
end if

UPDATE tes_liste_controllo  
  SET approvato_da  = null,
		autorizzato_da = null,
		validato_da = null
WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda )  AND  
		( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
		( tes_liste_controllo.num_versione = :ll_versione ) AND
		( tes_liste_controllo.num_edizione = :ll_nuova_edizione);
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nella creazione nuova revisione/edizione del questionario.~r~n"+sqlca.sqlerrtext, Stopsign!)
	ROLLBACK;
	return -1
end if


INSERT INTO det_liste_controllo  
		( cod_azienda,   
		  num_reg_lista,   
		  num_versione,   
		  num_edizione,   
		  prog_riga_liste_contr,   
		  des_domanda,   
		  flag_tipo_risposta,   
		  des_check_1,   
		  des_check_2,   
		  check_button,   
		  des_option_1,   
		  des_option_2,   
		  des_option_3,   
		  des_option_4,   
		  des_option_5,   
		  num_opzioni,   
		  option_button,
		  peso_check_1,
		  peso_check_2,
		  peso_option_1,
		  peso_option_2,
		  peso_option_3,
		  peso_option_4,
		  peso_option_5,
		  peso_option_libero,
		  des_option_libero,
		  campo_libero,
		  des_libero,
		  des_subtotale,
		  val_subtotale)  
  SELECT det_liste_controllo.cod_azienda,   
			det_liste_controllo.num_reg_lista,   
			det_liste_controllo.num_versione,   
			det_liste_controllo.num_edizione + 1,   
			det_liste_controllo.prog_riga_liste_contr,   
			det_liste_controllo.des_domanda,   
			det_liste_controllo.flag_tipo_risposta,   
			det_liste_controllo.des_check_1,   
			det_liste_controllo.des_check_2,   
			det_liste_controllo.check_button,   
			det_liste_controllo.des_option_1,   
			det_liste_controllo.des_option_2,   
			det_liste_controllo.des_option_3,   
			det_liste_controllo.des_option_4,   
			det_liste_controllo.des_option_5,   
			det_liste_controllo.num_opzioni,   
			det_liste_controllo.option_button,  
			det_liste_controllo.peso_check_1,
			det_liste_controllo.peso_check_2,
			det_liste_controllo.peso_option_1,
			det_liste_controllo.peso_option_2,
			det_liste_controllo.peso_option_3,
			det_liste_controllo.peso_option_4,
			det_liste_controllo.peso_option_5,
			det_liste_controllo.peso_option_libero,
			det_liste_controllo.des_option_libero,
			det_liste_controllo.campo_libero,
			det_liste_controllo.des_libero,
			det_liste_controllo.des_subtotale,
			det_liste_controllo.val_subtotale  
	 FROM det_liste_controllo  
	WHERE ( det_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( det_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
			( det_liste_controllo.num_versione = :ll_versione ) AND  
			( det_liste_controllo.num_edizione = :ll_edizione )   ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore nell'inserimento delle domande nella nuova revisione/edizione del questionario.~r~n"+sqlca.sqlerrtext, Stopsign!)
	ROLLBACK;
	return -1
end if

ll_max_edizioni = f_num_max_edi()

if (ll_edizione > ll_max_edizioni ) and (ll_max_edizioni <> 0) then
	ll_new_edizione = 0 
	ll_new_versione = ll_versione + 1
	
	SELECT tes_liste_controllo.cod_resp_divisione
	 INTO :ls_str
	 FROM tes_liste_controllo  
	WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
			( tes_liste_controllo.num_versione = :ll_new_versione ) AND  
			( tes_liste_controllo.num_edizione = :ll_new_edizione )   ;
	if sqlca.sqlcode = 0 then
		g_mb.messagebox("OMNIA","Un questionario con questo indice di edizione/ revisione è già esistente:verificare!")
		ROLLBACK;
		return -1
	end if
	
	UPDATE tes_liste_controllo  
	  SET num_versione = :ll_new_versione,  
			num_edizione = :ll_new_edizione
	WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda )  AND  
			( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
			( tes_liste_controllo.num_versione = :ll_versione ) AND  
			( tes_liste_controllo.num_edizione = :ll_edizione );
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in impostazione del numero della nuova revisione~r~n"+sqlca.sqlerrtext)
		ROLLBACK;
		return -1
	end if
	
	UPDATE det_liste_controllo  
	  SET num_versione = :ll_new_versione,  
			num_edizione = :ll_new_edizione
	WHERE ( det_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda )  AND  
			( det_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
			( det_liste_controllo.num_versione = :ll_versione ) AND  
			( det_liste_controllo.num_edizione = :ll_edizione );
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in impostazione del numero della nuova revisione~r~n"+sqlca.sqlerrtext)
		ROLLBACK;
		return -1
	end if
end if

COMMIT;
return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

is_flag_valido = "S"
is_flag_in_prova = "%"


l_objects[1] = dw_tes_questionari_det_1
dw_folder.fu_AssignTab(1, "&Dati Lista", l_Objects[])
l_objects[1] = dw_tes_questionari_det_3
dw_folder.fu_AssignTab(2, "&Note e Modifiche", l_Objects[])
l_objects[1] = dw_tes_questionari_det_4
dw_folder.fu_AssignTab(3, "&Punteggi", l_Objects[])

dw_folder.fu_FolderCreate(3,4)
dw_folder.fu_SelectTab(1)

dw_tes_questionari_lista.set_dw_key("cod_azienda")
dw_tes_questionari_lista.set_dw_options(sqlca,pcca.null_object, c_nodelete, c_default)
dw_tes_questionari_det_1.set_dw_options(sqlca,dw_tes_questionari_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_questionari_det_4.set_dw_options(sqlca,dw_tes_questionari_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_questionari_det_3.set_dw_options(sqlca,dw_tes_questionari_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_questionari_lista

rb_ultime_edizioni.checked = true
rb_approva.enabled = false
rb_autorizza.enabled = false
rb_valida.enabled = true
rb_nuova_edizione.enabled = true
cb_esegui.enabled = false
rb_ultime_edizioni.postevent("clicked")
end event

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_questionari_det_1,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_questionari_det_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end on

on w_tes_questionari.create
int iCurrent
call super::create
this.dw_tes_questionari_lista=create dw_tes_questionari_lista
this.cb_dettagli=create cb_dettagli
this.cb_verifica=create cb_verifica
this.rb_ultime_edizioni=create rb_ultime_edizioni
this.rb_in_creazione=create rb_in_creazione
this.rb_scadenza=create rb_scadenza
this.rb_storico=create rb_storico
this.gb_2=create gb_2
this.rb_autorizza=create rb_autorizza
this.rb_valida=create rb_valida
this.rb_nuova_edizione=create rb_nuova_edizione
this.cb_esegui=create cb_esegui
this.rb_approva=create rb_approva
this.gb_1=create gb_1
this.dw_tes_questionari_det_4=create dw_tes_questionari_det_4
this.dw_tes_questionari_det_1=create dw_tes_questionari_det_1
this.dw_tes_questionari_det_3=create dw_tes_questionari_det_3
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_questionari_lista
this.Control[iCurrent+2]=this.cb_dettagli
this.Control[iCurrent+3]=this.cb_verifica
this.Control[iCurrent+4]=this.rb_ultime_edizioni
this.Control[iCurrent+5]=this.rb_in_creazione
this.Control[iCurrent+6]=this.rb_scadenza
this.Control[iCurrent+7]=this.rb_storico
this.Control[iCurrent+8]=this.gb_2
this.Control[iCurrent+9]=this.rb_autorizza
this.Control[iCurrent+10]=this.rb_valida
this.Control[iCurrent+11]=this.rb_nuova_edizione
this.Control[iCurrent+12]=this.cb_esegui
this.Control[iCurrent+13]=this.rb_approva
this.Control[iCurrent+14]=this.gb_1
this.Control[iCurrent+15]=this.dw_tes_questionari_det_4
this.Control[iCurrent+16]=this.dw_tes_questionari_det_1
this.Control[iCurrent+17]=this.dw_tes_questionari_det_3
this.Control[iCurrent+18]=this.dw_folder
end on

on w_tes_questionari.destroy
call super::destroy
destroy(this.dw_tes_questionari_lista)
destroy(this.cb_dettagli)
destroy(this.cb_verifica)
destroy(this.rb_ultime_edizioni)
destroy(this.rb_in_creazione)
destroy(this.rb_scadenza)
destroy(this.rb_storico)
destroy(this.gb_2)
destroy(this.rb_autorizza)
destroy(this.rb_valida)
destroy(this.rb_nuova_edizione)
destroy(this.cb_esegui)
destroy(this.rb_approva)
destroy(this.gb_1)
destroy(this.dw_tes_questionari_det_4)
destroy(this.dw_tes_questionari_det_1)
destroy(this.dw_tes_questionari_det_3)
destroy(this.dw_folder)
end on

type dw_tes_questionari_lista from uo_cs_xx_dw within w_tes_questionari
integer x = 23
integer y = 20
integer width = 1646
integer height = 500
integer taborder = 70
string dataobject = "d_tes_questionari_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   is_flag_valido, &
                   is_flag_in_prova)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end on

event updatestart;call super::updatestart;string ls_cod_resp_divisione
long ll_i, ll_num_reg_lista,ll_num_versione,ll_num_edizione

for ll_i = 1 to this.deletedcount()
	ll_num_reg_lista = this.getitemnumber(ll_i, "num_reg_lista", delete!, true)		
	ll_num_versione = this.getitemnumber(ll_i, "num_versione", delete!, true)		
	ll_num_edizione = this.getitemnumber(ll_i, "num_edizione", delete!, true)		
   delete from  det_liste_controllo
          where cod_azienda = :s_cs_xx.cod_azienda and
                num_reg_lista = :ll_num_reg_lista and
                num_versione = :ll_num_versione and
                num_edizione = :ll_num_edizione;
	if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Questionari","Errore in cancellazione domande del questionario.~r~n"+sqlca.sqlerrtext, StopSign!)
		return 1
	end if
next
//if ib_delete = true then
//   DELETE FROM det_liste_controllo
//          WHERE (det_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda) and
//                (det_liste_controllo.num_reg_lista = :il_num_lista) and
//                (det_liste_controllo.num_versione = :il_versione) and
//                (det_liste_controllo.num_edizione = :il_edizione);
//end if


end event

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then

string ls_data
datetime ldt_scad, ldt_oggi
long   ll_valido_per

   if isnull(getitemstring(getrow(), "cod_area_aziendale")) or len(getitemstring(getrow(), "cod_area_aziendale")) < 1  then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione area di riferimento lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemdatetime(getrow(), "emesso_il")) or (getitemdatetime(getrow(), "emesso_il") < datetime(date("01/01/1901")))then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione data di creazione lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "cod_resp_divisione")) or len(getitemstring(getrow(), "cod_resp_divisione")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "des_lista")) or len(getitemstring(getrow(), "des_lista")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   ls_data = string(getitemdatetime(getrow(),"autorizzato_il"), s_cs_xx.db_funzioni.formato_data)

   if (getitemnumber(getrow(), "valido_per") > 0) and ( isnull(ls_data) or ls_data = "")  then
      g_mb.messagebox("Lista di controllo","Sono stati indicati giorni di validità senza la data di autorizzazione: in questo modo non posso calcolare la scadenza entro la quale bisogna validare la lista",StopSign!)
      pcca.error = c_fatal
      return
   end if

	ll_valido_per = getitemnumber(getrow(),"valido_per") 
	if (ll_valido_per > 0) and not(isnull(ll_valido_per)) then
		ldt_scad = datetime(RelativeDate(date(getitemdatetime(getrow(),"autorizzato_il")),  ll_valido_per))
	   ldt_oggi = datetime(today())

	   if ldt_scad <= ldt_oggi  then
	      g_mb.messagebox("Lista di controllo","La data di scadenza della validazione è già inferiore o uguale a oggi: aumentare giorni di validità",StopSign!)
	      pcca.error = c_valfailed
	      return
	   end if
      setitem(getrow(),"scadenza_validazione", ldt_scad)
	end if

end if			//  di extendmode
end event

event pcd_new;call super::pcd_new;string ls_cod_resp_divisione

cb_dettagli.enabled = false
SELECT mansionari.cod_resp_divisione  
INTO   :ls_cod_resp_divisione  
FROM   mansionari  
WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		 ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Nuova Lista","Attenzione!! all'utente corrispondono più mansionari, oppure non esiste il mansionario dell'utente", StopSign!)
	pcca.error = c_fatal
end if
setitem(getrow(), "cod_resp_divisione", ls_cod_resp_divisione)
setitem(getrow(),"emesso_il", datetime(today()))

setitem(getrow(),"num_versione", 1)
setitem(getrow(),"num_edizione", 0)
setitem(getrow(),"flag_valido", "N")
setitem(getrow(),"flag_in_prova", "S")
ib_new_rec = true

end event

event pcd_view;call super::pcd_view;if dw_tes_questionari_lista.getrow() > 0 then
   il_num_lista = dw_tes_questionari_lista.getitemnumber(dw_tes_questionari_lista.getrow(), "num_reg_lista")
   il_versione = dw_tes_questionari_lista.getitemnumber(dw_tes_questionari_lista.getrow(), "num_versione")
   il_edizione = dw_tes_questionari_lista.getitemnumber(dw_tes_questionari_lista.getrow(), "num_edizione")
end if

cb_dettagli.enabled = true

if ib_new_rec then
   rb_in_creazione.checked = true
   rb_in_creazione.postevent("Clicked")
   ib_new_rec = false
end if
end event

event pcd_setkey;call super::pcd_setkey;long  l_idx, ll_num_reg_lista


for l_idx = 1 to rowcount()
	
   if isnull(getitemstring(l_idx, "cod_azienda")) then
      setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(getitemnumber(l_idx, "num_reg_lista")) or getitemnumber(l_idx, "num_reg_lista") = 0 then
		
		select max(num_reg_lista)
		into   :ll_num_reg_lista
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 or isnull(ll_num_reg_lista) then
			ll_num_reg_lista = 0
		end if
		
		ll_num_reg_lista ++
		
		setitem(l_idx,"num_reg_lista", ll_num_reg_lista)
		
	end if
	
next

end event

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_dettagli.enabled = false
ib_delete = true
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_dettagli.enabled = false

end on

type cb_dettagli from commandbutton within w_tes_questionari
integer x = 2446
integer y = 640
integer width = 370
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Dettaglio"
end type

event clicked;if dw_tes_questionari_lista.i_numselected < 1 then
   g_mb.messagebox("Dettaglio Liste di Controllo","Selezionare prima la lista sulla quale entrare nel dettaglio",StopSign!)
   return
end if
if not isvalid(w_det_liste_controllo) then
	window_open_parm(w_det_questionari, -1, dw_tes_questionari_lista)
end if
end event

type cb_verifica from commandbutton within w_tes_questionari
integer x = 2446
integer y = 540
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Controllo"
end type

on clicked;setnull(s_cs_xx.parametri.parametro_s_1)
f_lista_controllo_verifica_validazioni()
if isnull(s_cs_xx.parametri.parametro_s_1) then
   s_cs_xx.parametri.parametro_s_1 = "Nessuna azione intrapresa"
end if
window_open(w_rapporto_verifiche, 0)
setnull(s_cs_xx.parametri.parametro_s_1)

end on

type rb_ultime_edizioni from radiobutton within w_tes_questionari
integer x = 2286
integer y = 100
integer width = 494
integer height = 68
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Ultime Revisioni"
end type

event clicked;rb_approva.enabled = false
rb_autorizza.enabled = false
rb_valida.enabled = true
rb_nuova_edizione.enabled = true
cb_esegui.enabled = false

is_flag_valido = "S"
is_flag_in_prova = "%"

is_tipo_records = "E"

dw_tes_questionari_lista.setfilter("")
dw_tes_questionari_lista.filter()

dw_tes_questionari_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
end event

type rb_in_creazione from radiobutton within w_tes_questionari
integer x = 2286
integer y = 180
integer width = 411
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "In Creazione"
end type

event clicked;rb_approva.enabled = true
rb_autorizza.enabled = true
rb_valida.enabled = false
rb_nuova_edizione.enabled = false
cb_esegui.enabled = false

is_flag_valido = "N"
is_flag_in_prova = "S"
is_tipo_records = "C"

dw_tes_questionari_lista.setfilter("")
dw_tes_questionari_lista.filter()

dw_tes_questionari_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
end event

type rb_scadenza from radiobutton within w_tes_questionari
integer x = 2286
integer y = 260
integer width = 411
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "In Scadenza"
end type

event clicked;string ls_str

rb_approva.enabled = false
rb_autorizza.enabled = false
rb_valida.enabled = true
rb_nuova_edizione.enabled = false
cb_esegui.enabled = false

is_flag_valido = "S"
is_flag_in_prova = "%"

is_tipo_records = "S"
dw_tes_questionari_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

ls_str = "(scadenza_validazione > date(" + char(34) + "01/01/1900" + char(34) + ")  and isnull(validato_da))"
dw_tes_questionari_lista.setfilter(ls_str)
dw_tes_questionari_lista.filter()

end event

type rb_storico from radiobutton within w_tes_questionari
integer x = 2286
integer y = 340
integer width = 274
integer height = 68
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Storico"
end type

event clicked;rb_approva.enabled = false
rb_autorizza.enabled = false
rb_valida.enabled = false
rb_nuova_edizione.enabled = false
cb_esegui.enabled = false

is_flag_valido = "N"
is_flag_in_prova = "N"

is_tipo_records = "T"

dw_tes_questionari_lista.setfilter("")
dw_tes_questionari_lista.filter()

dw_tes_questionari_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
end event

type gb_2 from groupbox within w_tes_questionari
integer x = 2263
integer y = 20
integer width = 549
integer height = 420
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza"
end type

type rb_autorizza from radiobutton within w_tes_questionari
integer x = 1714
integer y = 180
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Approva"
end type

on clicked;il_tipo_operazione = 2
cb_esegui.enabled = true
end on

type rb_valida from radiobutton within w_tes_questionari
integer x = 1714
integer y = 260
integer width = 462
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Valida"
end type

on clicked;il_tipo_operazione = 3
cb_esegui.enabled = true
end on

type rb_nuova_edizione from radiobutton within w_tes_questionari
integer x = 1714
integer y = 340
integer width = 507
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Nuova Revisione"
end type

on clicked;il_tipo_operazione = 4
cb_esegui.enabled = true
end on

type cb_esegui from commandbutton within w_tes_questionari
integer x = 1691
integer y = 440
integer width = 549
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui Operazione"
end type

event clicked;long ll_riga_corrente


ll_riga_corrente = dw_tes_questionari_lista.getrow()
choose case il_tipo_operazione
case 1
   if wf_approva() = 0 then parent.triggerevent("pc_retrieve")
   dw_tes_questionari_lista.setrow(ll_riga_corrente)
   rb_in_creazione.triggerevent("clicked")
   setpointer(arrow!)
case 2
   if wf_autorizza() = 0 then parent.triggerevent("pc_retrieve")
   dw_tes_questionari_lista.setrow(ll_riga_corrente)
   rb_in_creazione.triggerevent("clicked")
   setpointer(arrow!) 
case 3
   if wf_valida() = 0 then parent.triggerevent("pc_retrieve")
   dw_tes_questionari_lista.setrow(ll_riga_corrente)
   rb_ultime_edizioni.triggerevent("clicked")
   setpointer(arrow!)
case 4
   if wf_nuova_edizione() = 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo","La nuova edizione si trova fra le liste IN CREAZIONE",Information!)
   end if   
   setpointer(arrow!)
end choose
end event

type rb_approva from radiobutton within w_tes_questionari
integer x = 1714
integer y = 100
integer width = 434
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Verifica"
end type

on clicked;il_tipo_operazione = 1
cb_esegui.enabled = true
end on

type gb_1 from groupbox within w_tes_questionari
integer x = 1691
integer y = 20
integer width = 549
integer height = 420
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Operazioni"
end type

type dw_tes_questionari_det_4 from uo_cs_xx_dw within w_tes_questionari
integer x = 69
integer y = 680
integer width = 2286
integer height = 996
integer taborder = 30
string dataobject = "d_tes_questionari_det_4"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

datetime ldt_data
choose case i_colname
   case "valido_per"
   if (long(i_coltext) > 0) and (not isnull(i_coltext)) then
      setitem(getrow(),"scadenza_validazione", datetime(RelativeDate(date(getitemdatetime(getrow(),"autorizzato_il")), long(i_coltext))))
      if getitemdatetime(getrow(),"scadenza_validazione") < datetime(today()) then
         g_mb.messagebox("Lista Controllo","Attenzione la data di scadenza validazione è inferiore alla data odierna: aumentare il numero di giorni di validità", Information!)
         pcca.error = c_valfailed
      end if
   else
      setnull(ldt_data)
      setitem(getrow(),"scadenza_validazione", ldt_data)
   end if
end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then

string ls_data
datetime ldt_scad, ldt_oggi

   if isnull(getitemstring(getrow(), "cod_area_aziendale")) or len(getitemstring(getrow(), "cod_area_aziendale")) < 1  then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione area di riferimento lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemdatetime(getrow(), "emesso_il")) or (getitemdatetime(getrow(), "emesso_il") < datetime(date("01/01/1901")))then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione data di creazione lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "cod_resp_divisione")) or len(getitemstring(getrow(), "cod_resp_divisione")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "des_lista")) or len(getitemstring(getrow(), "des_lista")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   ls_data = string(getitemdatetime(getrow(),"autorizzato_il"), s_cs_xx.db_funzioni.formato_data)

   if (getitemnumber(getrow(), "valido_per") > 0) and ( isnull(ls_data) or ls_data = "")  then
      g_mb.messagebox("Lista di controllo","Sono stati indicati giorni di validità senza la data di autorizzazione: in questo modo non posso calcolare la scadenza entro la quale bisogna validare la lista",StopSign!)
      pcca.error = c_fatal
      return
   end if

   ldt_scad = datetime(RelativeDate(date(getitemdatetime(getrow(),"autorizzato_il")),getitemnumber(getrow(),"valido_per")))
   ldt_oggi = datetime(today())

   if ldt_scad <= ldt_oggi  then
      g_mb.messagebox("Lista di controllo","La data di scadenza della validazione è già inferiore o uguale a oggi: aumentare giorni di validità",StopSign!)
      pcca.error = c_valfailed
      return
   end if

   if getitemnumber(getrow(), "valido_per") > 0 then
      setitem(getrow(),"scadenza_validazione", ldt_scad)
   else
      setnull(ldt_oggi)
      setitem(getrow(),"scadenza_validazione", ldt_oggi)
   end if
end if
end event

type dw_tes_questionari_det_1 from uo_cs_xx_dw within w_tes_questionari
integer x = 91
integer y = 680
integer width = 2286
integer height = 1020
integer taborder = 20
string dataobject = "d_tes_questionari_det_1"
boolean border = false
end type

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then

string ls_data
datetime ldt_scad, ldt_oggi

   if isnull(getitemstring(getrow(), "cod_area_aziendale")) or len(getitemstring(getrow(), "cod_area_aziendale")) < 1  then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione area di riferimento lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemdatetime(getrow(), "emesso_il")) or (getitemdatetime(getrow(), "emesso_il") < datetime(date("01/01/1901")))then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione data di creazione lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "cod_resp_divisione")) or len(getitemstring(getrow(), "cod_resp_divisione")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "des_lista")) or len(getitemstring(getrow(), "des_lista")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   ls_data = string(getitemdatetime(getrow(),"autorizzato_il"), s_cs_xx.db_funzioni.formato_data)

   if (getitemnumber(getrow(), "valido_per") > 0) and ( isnull(ls_data) or ls_data = "")  then
      g_mb.messagebox("Lista di controllo","Sono stati indicati giorni di validità senza la data di autorizzazione: in questo modo non posso calcolare la scadenza entro la quale bisogna validare la lista",StopSign!)
      pcca.error = c_fatal
      return
   end if

   ldt_scad = datetime(RelativeDate(date(getitemdatetime(getrow(),"autorizzato_il")), getitemnumber(getrow(),"valido_per")))
   ldt_oggi = datetime(today())

   if ldt_scad <= ldt_oggi  then
      g_mb.messagebox("Lista di controllo","La data di scadenza della validazione è già inferiore o uguale a oggi: aumentare giorni di validità",StopSign!)
      pcca.error = c_valfailed
      return
   end if

   if getitemnumber(getrow(), "valido_per") > 0 then
      setitem(getrow(),"scadenza_validazione", ldt_scad)
   else
      setnull(ldt_oggi)
      setitem(getrow(),"scadenza_validazione", ldt_oggi)
   end if
end if
end event

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

datetime ldt_data
choose case i_colname
   case "valido_per"
   if (long(i_coltext) > 0) and (not isnull(i_coltext)) then
      setitem(getrow(),"scadenza_validazione", datetime(RelativeDate(date(getitemdatetime(getrow(),"autorizzato_il")), long(i_coltext))))
      if getitemdatetime(getrow(),"scadenza_validazione") < datetime(today()) then
         g_mb.messagebox("Lista Controllo","Attenzione la data di scadenza validazione è inferiore alla data odierna: aumentare il numero di giorni di validità", Information!)
         pcca.error = c_valfailed
      end if
   else
      setnull(ldt_data)
      setitem(getrow(),"scadenza_validazione", ldt_data)
   end if
end choose
end if
end event

type dw_tes_questionari_det_3 from uo_cs_xx_dw within w_tes_questionari
integer x = 69
integer y = 680
integer width = 2263
integer height = 996
integer taborder = 60
string dataobject = "d_tes_questionari_det_3"
boolean border = false
end type

type dw_folder from u_folder within w_tes_questionari
integer x = 23
integer y = 540
integer width = 2400
integer height = 1180
integer taborder = 110
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "&Dati Liste"
      SetFocus(dw_tes_questionari_det_1)
   CASE "&Note"
      SetFocus(dw_tes_questionari_det_4)
   CASE "&Modifiche Apportate"
      SetFocus(dw_tes_questionari_det_3)
END CHOOSE



end on

