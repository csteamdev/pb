﻿$PBExportHeader$w_tes_questionari_comp.srw
$PBExportComments$Compilazione Questionari
forward
global type w_tes_questionari_comp from w_cs_xx_principale
end type
type dw_tes_questionari_comp_lista from uo_cs_xx_dw within w_tes_questionari_comp
end type
type cb_compila from commandbutton within w_tes_questionari_comp
end type
type cb_report from commandbutton within w_tes_questionari_comp
end type
type dw_tes_questionari_comp_det_1 from uo_cs_xx_dw within w_tes_questionari_comp
end type
type dw_tes_questionari_comp_det_2 from uo_cs_xx_dw within w_tes_questionari_comp
end type
type dw_folder from u_folder within w_tes_questionari_comp
end type
end forward

global type w_tes_questionari_comp from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3072
integer height = 1988
string title = "Dettaglio Questionari"
dw_tes_questionari_comp_lista dw_tes_questionari_comp_lista
cb_compila cb_compila
cb_report cb_report
dw_tes_questionari_comp_det_1 dw_tes_questionari_comp_det_1
dw_tes_questionari_comp_det_2 dw_tes_questionari_comp_det_2
dw_folder dw_folder
end type
global w_tes_questionari_comp w_tes_questionari_comp

type variables
long ll_new_rows[], ll_null[]
end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_tes_questionari_comp_det_1
dw_folder.fu_AssignTab(1, "&Attributi", l_Objects[])
l_objects[1] = dw_tes_questionari_comp_det_2
dw_folder.fu_AssignTab(2, "&Note e Azioni", l_Objects[])

dw_folder.fu_FolderCreate(2,4)

dw_tes_questionari_comp_lista.set_dw_key("cod_azienda")
dw_tes_questionari_comp_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tes_questionari_comp_det_1.set_dw_options(sqlca,dw_tes_questionari_comp_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_questionari_comp_det_2.set_dw_options(sqlca,dw_tes_questionari_comp_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_questionari_comp_lista
dw_folder.fu_SelectTab(1)


end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_questionari_comp_det_1,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S') and (tes_liste_controllo.flag_questionario = 'S')" )

f_PO_LoadDDDW_DW(dw_tes_questionari_comp_det_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_questionari_comp_det_1,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_tes_questionari_comp.create
int iCurrent
call super::create
this.dw_tes_questionari_comp_lista=create dw_tes_questionari_comp_lista
this.cb_compila=create cb_compila
this.cb_report=create cb_report
this.dw_tes_questionari_comp_det_1=create dw_tes_questionari_comp_det_1
this.dw_tes_questionari_comp_det_2=create dw_tes_questionari_comp_det_2
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_questionari_comp_lista
this.Control[iCurrent+2]=this.cb_compila
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.dw_tes_questionari_comp_det_1
this.Control[iCurrent+5]=this.dw_tes_questionari_comp_det_2
this.Control[iCurrent+6]=this.dw_folder
end on

on w_tes_questionari_comp.destroy
call super::destroy
destroy(this.dw_tes_questionari_comp_lista)
destroy(this.cb_compila)
destroy(this.cb_report)
destroy(this.dw_tes_questionari_comp_det_1)
destroy(this.dw_tes_questionari_comp_det_2)
destroy(this.dw_folder)
end on

type dw_tes_questionari_comp_lista from uo_cs_xx_dw within w_tes_questionari_comp
integer x = 23
integer y = 20
integer width = 2994
integer height = 500
integer taborder = 30
string dataobject = "d_tes_questionari_comp_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;cb_compila.enabled = false
end event

event updatestart;call super::updatestart;if i_extendmode then
	long   ll_num_reg_lista,ll_prog_liste_con_comp, ll_i

   for ll_i = 1 to this.deletedcount()
      ll_num_reg_lista = getitemnumber(ll_i,"num_reg_lista", delete!, true) 
      ll_prog_liste_con_comp = getitemnumber(ll_i,"prog_liste_con_comp", delete!, true) 
      delete from det_liste_con_comp
      where (det_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda) and
            (det_liste_con_comp.num_reg_lista = :ll_num_reg_lista) and
            (det_liste_con_comp.prog_liste_con_comp = :ll_prog_liste_con_comp) ;
      if sqlca.sqlcode <> 0 then
         g_mb.messagebox("Liste Compilate",sqlca.sqlerrtext)
      end if
   next
end if

end event

on pcd_view;call uo_cs_xx_dw::pcd_view;cb_compila.enabled = true
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

event pcd_setkey;call super::pcd_setkey;long ll_max, l_idx, ll_progressivo, ll_num_reg_lista

for l_idx = 1 to rowcount()
   if isnull(getitemstring(l_idx, "cod_azienda")) then
      setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(getitemnumber(l_idx, "prog_liste_con_comp")) or getitemnumber(l_idx, "prog_liste_con_comp") < 1 then
		ll_num_reg_lista = getitemnumber(l_idx,"num_reg_lista")
		SELECT max(prog_liste_con_comp)
		INTO   :ll_progressivo  
		FROM   tes_liste_con_comp  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda and
			    num_reg_lista = :ll_num_reg_lista;
		if sqlca.sqlcode <> 0 then
			ll_progressivo = 1
		else
			if isnull(ll_progressivo) or (ll_progressivo < 1) then
				ll_progressivo = 1
			else
				ll_progressivo++
			end if
		end if
		
		setitem(l_idx, "prog_liste_con_comp",ll_progressivo)
		
		ll_max = upperbound(ll_new_rows)
		if ll_max < 1 or isnull(ll_max) then ll_max = 1
		ll_new_rows[ll_max] = l_idx
		// vedi evento updateend per inserimento automatico delle domande (dettagli)
	end if
next

end event

event pcd_modify;call super::pcd_modify;cb_compila.enabled = false
end event

event updateend;call super::updateend;string ls_sql, ls_des_domanda, ls_flag_tipo_risposta, ls_des_check_1, ls_des_check_2, ls_check_button
string ls_des_option[], ls_option_button, ls_des_option_libero, ls_campo_libero, ls_des_libero, ls_des_subtotale, ls_tabella, &
		 ls_codice, ls_cod_area
long   ll_num_opzioni, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp, &
		 ll_prog_riga_liste_contr,ll_peso_check[2], ll_peso_option[6], ll_i
	
if upperbound(ll_new_rows) > 0 and not isnull(upperbound(ll_new_rows)) then
	for ll_i = 1 to upperbound(ll_new_rows)
		ll_num_reg_lista = getitemnumber(ll_new_rows[ll_i],"num_reg_lista") 
		ll_num_versione  = getitemnumber(ll_new_rows[ll_i],"num_versione") 
		ll_num_edizione  = getitemnumber(ll_new_rows[ll_i],"num_edizione") 
		ll_prog_liste_con_comp = getitemnumber(ll_new_rows[ll_i],"prog_liste_con_comp") 
	
		declare cu_dett dynamic cursor for sqlsa;
	
		ls_sql = "SELECT prog_riga_liste_contr, "+&
						 "des_domanda, "+&
						 "flag_tipo_risposta, "+&   
						 "des_check_1, "+&   
						 "des_check_2, "+&   
						 "check_button, "+&   
						 "des_option_1, "+&   
						 "des_option_2, "+&   
						 "des_option_3, "+&   
						 "des_option_4, "+&   
						 "des_option_5, "+&   
						 "num_opzioni, "+&   
						 "option_button, "+&   
						 "peso_check_1, "+&
						 "peso_check_2, "+&
						 "peso_option_1, "+&
						 "peso_option_2, "+&
						 "peso_option_3, "+&
						 "peso_option_4, "+&
						 "peso_option_5, "+&
						 "peso_option_libero, "+&
						 "des_option_libero, "+&
						 "campo_libero, "+&
						 "des_libero, "+&
						 "des_subtotale "+&
						 "FROM det_liste_controllo "+& 
						 "WHERE cod_azienda = '"+s_cs_xx.cod_azienda+"' and  "+&
								 "num_reg_lista = "+string(ll_num_reg_lista)+" and "+& 
								 "num_versione = "+string(ll_num_versione)+" and "+& 
								 "num_edizione = "+string(ll_num_edizione)
	
		prepare sqlsa from :ls_sql ;
		open dynamic cu_dett ;
		do while 1=1
			fetch cu_dett into :ll_prog_riga_liste_contr,
										:ls_des_domanda,
										:ls_flag_tipo_risposta, 
										:ls_des_check_1,
										:ls_des_check_2,
										:ls_check_button,
										:ls_des_option[1],
										:ls_des_option[2],
										:ls_des_option[3],
										:ls_des_option[4],
										:ls_des_option[5],
										:ll_num_opzioni,
										:ls_option_button,
										:ll_peso_check[1],
										:ll_peso_check[2],
										:ll_peso_option[1],
										:ll_peso_option[2],
										:ll_peso_option[3],
										:ll_peso_option[4],
										:ll_peso_option[5],
										:ll_peso_option[6],
										:ls_des_option_libero,
										:ls_campo_libero,
										:ls_des_libero,
										:ls_des_subtotale;
			if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
		
			INSERT INTO det_liste_con_comp  
					( cod_azienda,   
					  num_reg_lista,   
					  prog_liste_con_comp,   
					  prog_riga_liste_contr,   
					  des_domanda,   
					  flag_tipo_risposta,   
					  des_check_1,   
					  des_check_2,   
					  check_button,   
					  des_option_1,   
					  des_option_2,   
					  des_option_3,   
					  des_option_4,   
					  des_option_5,   
					  num_opzioni,   
					  option_button,
					  peso_check_1, 
					  peso_check_2, 
					  peso_option_1,           
					  peso_option_2,           
					  peso_option_3,           
					  peso_option_4,           
					  peso_option_5,           
					  peso_option_libero,           
					  des_option_libero,
					  campo_libero,
					  des_libero,
					  des_subtotale,
					  val_subtotale )  
			VALUES ( :s_cs_xx.cod_azienda,   
					  :ll_num_reg_lista,   
					  :ll_prog_liste_con_comp,   
					  :ll_prog_riga_liste_contr,   
					  :ls_des_domanda,   
					  :ls_flag_tipo_risposta,   
					  :ls_des_check_1,   
					  :ls_des_check_2,   
					  :ls_check_button,   
					  :ls_des_option[1],   
					  :ls_des_option[2],   
					  :ls_des_option[3],   
					  :ls_des_option[4],   
					  :ls_des_option[5],   
					  :ll_num_opzioni,   
					  :ls_option_button,
					  :ll_peso_check[1],
					  :ll_peso_check[2],
					  :ll_peso_option[1],
					  :ll_peso_option[2],
					  :ll_peso_option[3],
					  :ll_peso_option[4],
					  :ll_peso_option[5],
					  :ll_peso_option[6],
					  :ls_des_option_libero,
					  :ls_campo_libero,
					  :ls_des_libero,
					  :ls_des_subtotale,
					  0  )  ;
		loop
		close cu_dett;
	next
end if
ll_new_rows= ll_null
end event

type cb_compila from commandbutton within w_tes_questionari_comp
integer x = 2651
integer y = 1780
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Compila"
end type

event clicked;long ll_current_row[], l_error

if not isvalid(w_det_liste_con_comp) then
   if dw_tes_questionari_comp_lista.i_numselected > 0 then
      window_open_parm(w_det_questionari_comp, 0, dw_tes_questionari_comp_lista)

      ll_current_row[1] = dw_tes_questionari_comp_lista.i_selectedrows[1]
      parent.triggerevent("pc_retrieve")
      dw_tes_questionari_comp_lista.Set_Selected_Rows(1, ll_current_row[], c_IgnoreChanges, &	
           c_RefreshChildren, c_RefreshSame)
   end if
end if
end event

type cb_report from commandbutton within w_tes_questionari_comp
integer x = 2263
integer y = 1780
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;if not isvalid(w_report_lista_comp) then
   if dw_tes_questionari_comp_lista.i_numselected > 0 then
      window_open_parm(w_report_questionario_comp, -1, dw_tes_questionari_comp_lista)
   end if
end if
end event

type dw_tes_questionari_comp_det_1 from uo_cs_xx_dw within w_tes_questionari_comp
integer x = 46
integer y = 640
integer width = 2949
integer height = 1060
integer taborder = 40
string dataobject = "d_tes_questionari_comp_det"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;//if i_extendmode then

end event

event itemchanged;call super::itemchanged;decimal ld_num_reg_lista

if i_extendmode then

string ls_des_lista, ls_cod_area_aziendale, ls_cod_resp_divisione, ls_approvato_da, ls_autorizzato_da, &
       ls_etichetta_1, ls_etichetta_2, ls_etichetta_3, ls_etichetta_4, ls_validato_da, ls_flag_uso, ls_flag_recuperato, ls_note
long ll_num_versione,ll_num_edizione, ll_valido_per, ll_progressivo, ll_valore_riferimento
datetime ldt_emesso_il, ldt_autorizzato_il

ld_num_reg_lista = dec(data)

   if i_colname="num_reg_lista" then
        SELECT tes_liste_controllo.num_versione,   
         tes_liste_controllo.num_edizione,   
         tes_liste_controllo.des_lista,   
         tes_liste_controllo.cod_area_aziendale,   
         tes_liste_controllo.cod_resp_divisione,   
         tes_liste_controllo.approvato_da,   
         tes_liste_controllo.emesso_il,   
         tes_liste_controllo.autorizzato_da,   
         tes_liste_controllo.autorizzato_il,   
         tes_liste_controllo.valido_per,   
         tes_liste_controllo.validato_da,   
         tes_liste_controllo.flag_uso,   
         tes_liste_controllo.flag_recuperato,
         tes_liste_controllo.note,
         tes_liste_controllo.valore_riferimento,
			tes_liste_controllo.des_etichetta_1,
			tes_liste_controllo.des_etichetta_2,
			tes_liste_controllo.des_etichetta_3,
			tes_liste_controllo.des_etichetta_4
    INTO :ll_num_versione,   
         :ll_num_edizione,   
         :ls_des_lista,   
         :ls_cod_area_aziendale,   
         :ls_cod_resp_divisione,   
         :ls_approvato_da,   
         :ldt_emesso_il,   
         :ls_autorizzato_da,   
         :ldt_autorizzato_il,   
         :ll_valido_per,   
         :ls_validato_da,   
         :ls_flag_uso,   
         :ls_flag_recuperato,
         :ls_note,
         :ll_valore_riferimento,
			:ls_etichetta_1,
			:ls_etichetta_2,
			:ls_etichetta_3,
			:ls_etichetta_4
    FROM tes_liste_controllo  
   WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( tes_liste_controllo.num_reg_lista = :ld_num_reg_lista ) AND  
         ( tes_liste_controllo.flag_valido = 'S' )   ;

	setredraw(FALSE)

   setitem(getrow(),"prog_liste_con_comp", ll_progressivo)
   setitem(getrow(),"num_versione", ll_num_versione)
   setitem(getrow(),"num_edizione", ll_num_edizione)
   setitem(getrow(),"des_lista", ls_des_lista)
   setitem(getrow(),"cod_area_aziendale", ls_cod_area_aziendale)
   setitem(getrow(),"cod_resp_divisione", ls_cod_resp_divisione)
   setitem(getrow(),"approvato_da", ls_approvato_da)
   setitem(getrow(),"emesso_il", ldt_emesso_il)
   setitem(getrow(),"autorizzato_da", ls_autorizzato_da)
   setitem(getrow(),"autorizzato_il", ldt_autorizzato_il)
   setitem(getrow(),"valido_per", ll_valido_per)
   setitem(getrow(),"validato_da", ls_validato_da)
   setitem(getrow(),"flag_uso", ls_flag_uso)
   setitem(getrow(),"flag_recuperato", ls_flag_recuperato)
   setitem(getrow(),"note", ls_note)
   setitem(getrow(),"valore_riferimento", ll_valore_riferimento)
	setitem(getrow(),"des_etichetta_1", ls_etichetta_1)
	setitem(getrow(),"des_etichetta_2", ls_etichetta_2)
	setitem(getrow(),"des_etichetta_3", ls_etichetta_3)
	setitem(getrow(),"des_etichetta_4", ls_etichetta_4)
	
	setredraw(TRUE)
   end if
end if
end event

type dw_tes_questionari_comp_det_2 from uo_cs_xx_dw within w_tes_questionari_comp
integer x = 69
integer y = 660
integer width = 2903
integer height = 900
integer taborder = 10
string dataobject = "d_tes_questionari_comp_det_2"
boolean border = false
end type

type dw_folder from u_folder within w_tes_questionari_comp
integer x = 23
integer y = 540
integer width = 2994
integer height = 1220
integer taborder = 20
end type

on itemchanged;call u_folder::itemchanged;CHOOSE CASE i_SelectedTabName
   CASE "&Attrubi"
      SetFocus(dw_tes_questionari_comp_det_1)
   CASE "&Note e Azioni"
      SetFocus(dw_tes_questionari_comp_det_2)
END CHOOSE

end on

