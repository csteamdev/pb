﻿$PBExportHeader$uo_richieste_stat.sru
forward
global type uo_richieste_stat from nonvisualobject
end type
end forward

global type uo_richieste_stat from nonvisualobject
end type
global uo_richieste_stat uo_richieste_stat

forward prototypes
public function integer uof_mi5 (string as_cod_reparto, datetime adt_data_dal, datetime adt_data_al, ref decimal ad_mi5, ref string as_errore)
public function integer uof_mi6 (string as_cod_reparto, datetime adt_data_dal, datetime adt_data_al, ref decimal ad_mi6, ref string as_errore)
public function integer uof_mi7 (string as_cod_reparto, datetime adt_data_dal, datetime adt_data_al, ref decimal ad_mi7, ref string as_errore)
public function long uof_datediff_in_minutes (datetime fdt_data_inizio, datetime fdt_data_fine)
public function long uof_datediff_in_minutes (datetime fdt_data_inizio, datetime fdt_ora_inizio, datetime fdt_data_fine, datetime fdt_ora_fine)
end prototypes

public function integer uof_mi5 (string as_cod_reparto, datetime adt_data_dal, datetime adt_data_al, ref decimal ad_mi5, ref string as_errore);/*
Per ogni attività manutentiva basata su richiesta il sistema calcola il parametro ZI5 che rappresenta il numero dei quarti d’ora di ritardo.
Poi viene calcolato il parametro RIT_I5 = ZI5 * 25
I5 = 100 - RIT_I5
Se I5 < 0 allora I5 viene posto a zero.
Alla fine viene effettuata la somma di tutti questi I5 (per tutte le richieste analizzate)
*/

dec{4}		ld_sopralluogo_r, ld_attivita_r, ld_risoluzione_minuti, ld_gg_assoluti, ld_rita_attivita, ld_sopralluogo_previsto_r
datetime		ldt_data_registrazione_r, ldt_data_fine_attivita_r, ldt_ora_fine_attivita_r, ldt_ora_assoluta
datetime		ldt_data_inizio_intervento_r, ldt_ora_inizio_intervento_r, ldt_data_fine_intervento_r, ldt_ora_fine_intervento_r, ldt_ora_registrazione_r
string			ls_flag_risoluzione_assoluta, ls_date_1, ls_date_2
integer		li_anno_richiesta_r
long			ll_num_richiesta_r, ll_rita_sopralluogo, ll_rita_soluzione, ll_ZI5, ll_RIT_I5, ll_I5, ll_penalita_ogni, ll_moltiplicatore_zi5, ll_sottrazione_i5, ll_gg, ll_giorno_settimana, &
				ll_count, ll_numero_ritardi
date			ldt_data, ldt_data_fine
time			ldt_ora, ldt_ora_fine

ll_penalita_ogni			= 15
ll_moltiplicatore_zi5	= 25
ll_sottrazione_i5		= 100

adt_data_dal = datetime(date(adt_data_dal), 00:00:00)
adt_data_al = datetime(date(adt_data_al), 00:00:00)
ll_numero_ritardi = 0
ad_MI5 = 0

declare cu_mi5 cursor for 
	select		tab_richieste.anno_registrazione, tab_richieste.num_registrazione, tab_richieste.data_registrazione, tab_richieste.ora_registrazione,
				manutenzioni.data_inizio_intervento, manutenzioni.ora_inizio_intervento,
				manutenzioni.data_fine_intervento, manutenzioni.ora_fine_intervento,
				tab_reparti_categorie_tempi.sopralluogo_minuti as sopralluogo_previsto,
				tab_richieste.data_fine_attivita,
				tab_richieste.ora_fine_attivita,
				tab_reparti_categorie_tempi.flag_risoluzione_assoluta,
				tab_reparti_categorie_tempi.risoluzione_minuti,
				tab_reparti_categorie_tempi.gg_assoluti,
				tab_reparti_categorie_tempi.ora_assoluta
	from tab_richieste
	join manutenzioni on 	manutenzioni.cod_azienda=tab_richieste.cod_azienda and
								manutenzioni.anno_reg_richiesta=tab_richieste.anno_registrazione and
								manutenzioni.num_reg_richiesta=tab_richieste.num_registrazione
	join anag_attrezzature on   anag_attrezzature.cod_azienda=tab_richieste.cod_azienda and
										 anag_attrezzature.cod_attrezzatura=tab_richieste.cod_attrezzatura
	join tab_reparti_categorie_tempi on tab_reparti_categorie_tempi.cod_azienda=anag_attrezzature.cod_azienda and
													tab_reparti_categorie_tempi.cod_reparto=anag_attrezzature.cod_reparto and
													tab_reparti_categorie_tempi.cod_cat_attrezzature=anag_attrezzature.cod_cat_attrezzature
	where tab_richieste.cod_azienda=:s_cs_xx.cod_azienda and
			tab_richieste.flag_richiesta_chiusa='S' and anag_attrezzature.cod_reparto=:as_cod_reparto and
			tab_richieste.data_registrazione >= :adt_data_dal and tab_richieste.data_registrazione <= :adt_data_al
	order by tab_richieste.data_registrazione asc;

open cu_mi5;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore 'cu_mi5'~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_mi5 into 	:li_anno_richiesta_r,
							:ll_num_richiesta_r,
							:ldt_data_registrazione_r, :ldt_ora_registrazione_r,
							:ldt_data_inizio_intervento_r, :ldt_ora_inizio_intervento_r, :ldt_data_fine_intervento_r, :ldt_ora_fine_intervento_r,
							:ld_sopralluogo_previsto_r,
							:ldt_data_fine_attivita_r,
							:ldt_ora_fine_attivita_r,
							:ls_flag_risoluzione_assoluta,
							:ld_risoluzione_minuti,
							:ld_gg_assoluti,
							:ldt_ora_assoluta;

	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in FETCH cursore 'cu_mi5'~r~n" + sqlca.sqlerrtext
		close cu_mi5;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	if isvalid(pcca.mdi_frame) then
		pcca.mdi_frame.setmicrohelp("Analisi MI5 --> Data Reg." + string(ldt_data_registrazione_r, "dd/mm/yyyy") + &
																" - Richiesta n. " + string(li_anno_richiesta_r) +"/" + string(ll_num_richiesta_r) + " in corso ...")
	end if

	if isnull(ld_risoluzione_minuti) then ld_risoluzione_minuti = 0
	if isnull(ld_gg_assoluti) or ld_gg_assoluti<0 then ld_gg_assoluti = 0

	//[   (differenza in minuti tra data inizio intervento e data registrazione richiesta)  meno i minuti inizio sopralluogo previsti
	ld_sopralluogo_r = uof_datediff_in_minutes(ldt_data_registrazione_r, ldt_ora_registrazione_r, ldt_data_inizio_intervento_r, ldt_ora_inizio_intervento_r) - ld_sopralluogo_previsto_r

	//esprimo l'eventuale ritardo come conteggio di quarti d'ora di ritardo, successivamente troncherò a valori interi
	ld_sopralluogo_r = ld_sopralluogo_r / ll_penalita_ogni
	
	if ld_sopralluogo_r>=0 then ll_numero_ritardi += 1
	
	//calcola lo scostamento dal tempo inizio sopralluogo atteso-------------------------------------------------------------------------------------------------
	//NOTA: la variabile ld_sopralluogo_r contiene già la differenza in quarti d'ora tra sopralluogo e sopralluogo atteso (se negativo o 0 virgola qualcosa poni ZERO)
	if ld_sopralluogo_r < 0 then
		ld_sopralluogo_r = 0
	else
		//altrimenti prendo il valore intero
		ld_sopralluogo_r = truncate(ld_sopralluogo_r, 0)
	end if
	ll_rita_sopralluogo = ld_sopralluogo_r		//sarà qui sempre un numero intero
	
	//metto in ZI5
	ll_ZI5 = ll_rita_sopralluogo
	//valutaRIT_I5
	ll_RIT_I5 = ll_ZI5 * ll_moltiplicatore_ZI5
	//valuta I5
	ll_I5 = ll_sottrazione_I5 - ll_RIT_I5
	//incrementa nella variabile di ritorno
	if ll_I5> 0 then 
		ad_MI5 += ll_I5
	else
		//non aggiungo nulla
	end if
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	//calcola lo scostamento dal tempo durata attivita atteso ---------------------------------------------------------------------------------------------------
	if ls_flag_risoluzione_assoluta="N" then
		//***********************************************************************************************************
		//calcolo in base al tempo relativo
		
		//quanti minuti è durato il guasto
		ld_attivita_r = uof_datediff_in_minutes(ldt_data_registrazione_r, ldt_ora_registrazione_r, ldt_data_fine_intervento_r, ldt_ora_fine_intervento_r)
		
		//esprimo lo scostamento dai minuti di durata guasto previsti, come conteggio di quarti d'ora
		ld_rita_attivita = (ld_attivita_r - ld_risoluzione_minuti) / ll_penalita_ogni
		//***********************************************************************************************************
	else
		//***********************************************************************************************************
		//calcolo in base al tempo assoluto
		ldt_data = date(ldt_data_registrazione_r)
		ll_gg = 0
		
		//CON QUESTO CICLO VALUTA LA DATA LIMITE OLTRE LA QUALE LA CHIUSURA NON DEVE ANDARE
		//se ld_gg_assoluti è zero neanche entrare nel ciclo 
		//alla fine in ldt_data ci sarà la data oltre la quale la chiusura della richiesta non deve andare
		do while ld_gg_assoluti>0
			
			ldt_data = relativedate(ldt_data, 1)		//vado al giorno successivo
			
			//verifica se il giorno in esame è presente come festività
			setnull(ll_count)
			
			select count(*)
			into :ll_count
			from tab_cal_ferie
			where cod_azienda=:s_cs_xx.cod_azienda and
					data_giorno=:ldt_data and
					flag_tipo_giorno = 'F'
			using sqlca;
			
			if sqlca.sqlcode<0 then
				as_errore = "Errore in lettura da tabella festività: "+sqlca.sqlerrtext
				close cu_mi5;
				return -1
			end if
			
			if ll_count>0 then
				//presente nelle festività: salta come conteggio (cioè non incrementare la variabile contatore ll_gg)
			
			else
				//non è una festività pre-impostata, verifica se sabato o domenica (Domenica è 1, Sabato è 7)
				ll_giorno_settimana = daynumber(ldt_data)
				
				if ll_giorno_settimana=1 or ll_giorno_settimana=7 then
					//è un sabato o una domenica: salta come conteggio (cioè non incrementare la variabile contatore ll_gg)
				else
					//incrementa il contatore
					ll_gg += 1
				end if
			end if
	
			//verifica se hai raggiunto il numero di giorni lavorativi dopo il quale la richiesta deve essere chiusa
			if ll_gg = ld_gg_assoluti then
				//data fine ideale trovata (in ldt_data), esci dal loop
				exit
			end if
		loop
																				//in ldt_data la data limite
		ldt_ora = time(ldt_ora_assoluta)								//questa è ora limite
		ldt_data_fine = date(ldt_data_fine_intervento_r)			//questa è data fine intervento
		ldt_ora_fine = time(ldt_ora_fine_intervento_r)				//questa è ora fine intervento
		
		
		ld_rita_attivita = uof_datediff_in_minutes(datetime(date(ldt_data), ldt_ora), datetime(date(ldt_data_fine_intervento_r), ldt_ora_fine ))
		if ld_rita_attivita<0 then
			ld_rita_attivita = 0
		else
			ld_rita_attivita = ld_rita_attivita / ll_penalita_ogni
		end if
		
	
		//***********************************************************************************************************
	end if
	
	if ld_rita_attivita>=1 then ll_numero_ritardi += 1
	
	//NOTA la variabile ld_rita_attivita contiene già la differenza in quarti d'ora tra data fine attivita e data fine attivita attesa (se negativo o 0 virgola qualcosa poni ZERO)
	//sia nel caso in cui ho calcolato come tempo relativo che come assoluto ...
	if ld_rita_attivita <= 0 then
		ld_rita_attivita = 0
	else
		//altrimenti prendo il valore intero
		ld_rita_attivita = truncate(ld_rita_attivita, 0)
	end if
	
	//metto in ZI5 (ho già espresso come scostamento in quarti d'ora)
	ll_ZI5 = ld_rita_attivita
	
	//valuta i parametri finali
	ll_RIT_I5 = ll_ZI5 * ll_moltiplicatore_ZI5
	ll_I5 = ll_sottrazione_I5 - ll_RIT_I5
	
	if ll_I5> 0 then
		ad_MI5 += ll_I5
	else
		//non aggiungo nulla
	end if
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------
loop

close cu_mi5;

////faccio la media sui ritardi
//if ll_numero_ritardi>0 then ad_MI5 = ad_MI5 / ll_numero_ritardi

if isvalid(pcca.mdi_frame) then
	pcca.mdi_frame.setmicrohelp("Fine!")
end if


return 0

end function

public function integer uof_mi6 (string as_cod_reparto, datetime adt_data_dal, datetime adt_data_al, ref decimal ad_mi6, ref string as_errore);/*
Per ogni attività manutentiva di tipo ordinario il sistema calcola il parametro ZI6 che rappresenta il numero dei giorni  di ritardo.
Poi viene calcolato il parametro RIT_I6 = ZI6 * 15
I6 = 100 - RIT_I6
Se I6 < 0 allora I6 viene posto a zero.
Alla fine viene effettuata la somma di tutti questi I6 (per tutte le manutenzioni ordinarie analizzate)
*/
//ogni giorno di ritardo, il valore 100 verrà decurtato di un valore pari a 15


//08/03/2013 Donato Integrazione
//il numero giorni di ritardo va calcolato dopo una tolleranza espressa in giorni in tabella 
//		tab_reparti_tempi     colonna -> gg_sopralluogo_man   (solo per Impianto attrezzatura)
//se non esiste oppure è ZERO allora non c'è nessuna tolleranza


long			ll_moltiplicatore_zi6, ll_sottrazione_i6, ll_num_registrazione, ll_gg_rita, ll_ZI6, ll_RIT_I6, ll_I6, ll_numero_ritardi, ll_gg_tolleranza_man
integer		li_anno_registrazione
datetime		ldt_data_registrazione, ldt_inizio_intervento


ll_moltiplicatore_zi6	= 15
ll_sottrazione_i6		= 100

adt_data_dal = datetime(date(adt_data_dal), 00:00:00)
adt_data_al = datetime(date(adt_data_al), 00:00:00)
ll_numero_ritardi = 0
ad_MI6 = 0

//recupero tolleranza in giorni per l'impianto --------------------
setnull(ll_gg_tolleranza_man)
		
select gg_sopralluogo_man
into :ll_gg_tolleranza_man
from tab_reparti_tempi
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_reparto=:as_cod_reparto;
			
if isnull(ll_gg_tolleranza_man) or ll_gg_tolleranza_man<0 then ll_gg_tolleranza_man=0
//---------------------------------------------------------------------

declare cu_mi6 cursor for  
	select		manutenzioni.anno_registrazione, manutenzioni.num_registrazione,
				manutenzioni.data_registrazione, manutenzioni.data_inizio_intervento
	from manutenzioni
	join anag_attrezzature on	anag_attrezzature.cod_azienda=manutenzioni.cod_azienda and
										anag_attrezzature.cod_attrezzatura=manutenzioni.cod_attrezzatura
	where 	manutenzioni.cod_azienda=:s_cs_xx.cod_azienda and
				manutenzioni.flag_eseguito='S' and manutenzioni.flag_ordinario='S' and 
				anag_attrezzature.cod_reparto=:as_cod_reparto and
				manutenzioni.data_registrazione >= :adt_data_dal and manutenzioni.data_registrazione <= :adt_data_al
	order by manutenzioni.data_registrazione asc;

open cu_mi6;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore 'cu_mi6'~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_mi6 into 	:li_anno_registrazione,
							:ll_num_registrazione,
							:ldt_data_registrazione,
							:ldt_inizio_intervento;

	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in FETCH cursore 'cu_mi6'~r~n" + sqlca.sqlerrtext
		close cu_mi6;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	if isvalid(pcca.mdi_frame) then
		pcca.mdi_frame.setmicrohelp("Analisi MI6 --> Data Reg." + string(ldt_data_registrazione, "dd/mm/yyyy") + &
																" - Man.Ordinaria n. " + string(li_anno_registrazione) +"/" + string(ll_num_registrazione) + " in corso ...")
	end if
	
	if isnull(ldt_data_registrazione) or year(date(ldt_data_registrazione)) < 1950 then continue
	if isnull(ldt_inizio_intervento) or year(date(ldt_inizio_intervento)) < 1950 then continue
	
	
	ll_gg_rita = daysafter(date(ldt_data_registrazione), date(ldt_inizio_intervento))
	if isnull(ll_gg_rita) then ll_gg_rita=0
	
	//08/03/2013 Donato tolleranza in giorni per l'impianto
	//tengo conto di una eventuale tolleranza ----------------------
	
	ll_gg_rita = ll_gg_rita - ll_gg_tolleranza_man
	if ll_gg_rita<0 then ll_gg_rita = 0
	//ll_gg_rita = abs(ll_gg_rita - ll_gg_tolleranza_man)
	//-------------------------------------------------------------------
	//if ll_gg_rita>0 then ll_numero_ritardi += 1
	
	
	//metto in ZI6
	ll_ZI6 = ll_gg_rita
	//valutaRIT_I6
	ll_RIT_I6 = ll_ZI6 * ll_moltiplicatore_ZI6
	//valuta I6
	ll_I6 = ll_sottrazione_I6 - ll_RIT_I6
	//incrementa nella variabile di ritorno
	if ll_I6> 0 then 
		ad_MI6 += ll_I6
	else
		//non aggiungo nulla
	end if
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
loop

close cu_MI6;

//if ll_numero_ritardi>0 then ad_MI6 = ad_MI6 / ll_numero_ritardi

if isvalid(pcca.mdi_frame) then
	pcca.mdi_frame.setmicrohelp("Fine!")
end if


return 0

end function

public function integer uof_mi7 (string as_cod_reparto, datetime adt_data_dal, datetime adt_data_al, ref decimal ad_mi7, ref string as_errore);/*
Si ponga R1=5, R2=5, R3=5, R4=10, R5=15
Per ogni attività manutentiva starordinaria programmata il sistema calcola il parametro ZI7 che rappresenta il numero dei giorni  di ritardo.
Poi viene calcolato il parametro 
RIT_I7 = R1+R2+R3+R4+R5 e così via 
In base ai giorno di ritardo i valori Rn vengono valorizzati e vanno ad incrementare il valore di RIT_I7
I7 = 100 - RIT_I7
Se I7 < 0 allora I7 viene posto a zero.

Alla fine viene effettuata la somma di tutti questi I6 (per tutte le manutenzioni straoordinarie programmate analizzate)
*/

//08/03/2013 Donato Integrazione
//il numero giorni di ritardo va calcolato dopo una tolleranza espressa in giorni in tabella 
//		tab_reparti_tempi     colonna -> gg_sopralluogo_man   (solo per Impianto attrezzatura)
//se non esiste oppure è ZERO allora non c'è nessuna tolleranza

long			ll_sottrazione_i7, ll_num_registrazione, ll_gg_rita, ll_RIT_I7, ll_I7, ll_num_ritardi, ll_gg_tolleranza_man
integer		li_anno_registrazione
datetime		ldt_data_registrazione, ldt_inizio_intervento


ll_sottrazione_i7		= 100

adt_data_dal = datetime(date(adt_data_dal), 00:00:00)
adt_data_al = datetime(date(adt_data_al), 00:00:00)
ll_num_ritardi = 0
ad_MI7 = 0
ll_RIT_I7 = 0

//recupero tolleranza in giorni per l'impianto --------------------
setnull(ll_gg_tolleranza_man)
		
select gg_sopralluogo_man
into :ll_gg_tolleranza_man
from tab_reparti_tempi
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_reparto=:as_cod_reparto;
			
if isnull(ll_gg_tolleranza_man) or ll_gg_tolleranza_man<0 then ll_gg_tolleranza_man=0
//---------------------------------------------------------------------

declare cu_mi7 cursor for  
	select		manutenzioni.anno_registrazione, manutenzioni.num_registrazione,
				manutenzioni.data_registrazione, manutenzioni.data_inizio_intervento
	from manutenzioni
	join anag_attrezzature on	anag_attrezzature.cod_azienda=manutenzioni.cod_azienda and
										anag_attrezzature.cod_attrezzatura=manutenzioni.cod_attrezzatura
	where 	manutenzioni.cod_azienda=:s_cs_xx.cod_azienda and
				manutenzioni.flag_eseguito='S' and manutenzioni.flag_straordinario='S' and 
				anag_attrezzature.cod_reparto=:as_cod_reparto and
				manutenzioni.data_registrazione >= :adt_data_dal and manutenzioni.data_registrazione <= :adt_data_al
	order by manutenzioni.data_registrazione asc;

open cu_mi7;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore 'cu_mi7'~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_mi7 into 	:li_anno_registrazione,
							:ll_num_registrazione,
							:ldt_data_registrazione,
							:ldt_inizio_intervento;

	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in FETCH cursore 'cu_mi7'~r~n" + sqlca.sqlerrtext
		close cu_mi7;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	if isvalid(pcca.mdi_frame) then
		pcca.mdi_frame.setmicrohelp("Analisi MI7 --> Data Reg." + string(ldt_data_registrazione, "dd/mm/yyyy") + &
																" - Man.Straordinaria Programmata n. " + string(li_anno_registrazione) +"/" + string(ll_num_registrazione) + " in corso ...")
	end if
	
	if isnull(ldt_data_registrazione) or year(date(ldt_data_registrazione)) < 1950 then continue
	if isnull(ldt_inizio_intervento) or year(date(ldt_inizio_intervento)) < 1950 then continue
	
	
	ll_gg_rita = daysafter(date(ldt_data_registrazione), date(ldt_inizio_intervento))
	if isnull(ll_gg_rita) then ll_gg_rita=0
	
	
	//08/03/2013 Donato tolleranza in giorni per l'impianto
	//tengo conto di una eventuale tolleranza ------------------------
	ll_gg_rita = ll_gg_rita - ll_gg_tolleranza_man
	if ll_gg_rita < 0 then ll_gg_rita = 0
	
	//ll_gg_rita = abs(ll_gg_rita - ll_gg_tolleranza_man)
	//-------------------------------------------------------------------
	//if ll_gg_rita>=1 then ll_num_ritardi += 1
	
	
	//incremento RIT_I7
	ll_RIT_I7 += ll_gg_rita
	
loop

close cu_MI7;


//valuta I7
ll_I7 = ll_sottrazione_I7 - ll_RIT_I7

if ll_I7> 0 then 
	ad_MI7 += ll_I7
end if

////faccio la media sui ritardi
//if ll_num_ritardi>0 then ad_MI7 = ad_MI7 / ll_num_ritardi


if isvalid(pcca.mdi_frame) then
	pcca.mdi_frame.setmicrohelp("Fine!")
end if


return 0

end function

public function long uof_datediff_in_minutes (datetime fdt_data_inizio, datetime fdt_data_fine);long ll_datediff
string ls_data_1, ls_data_2

ls_data_1 = string(fdt_data_inizio, "yyyymmdd HH:MM:SS")
ls_data_2 = string(fdt_data_fine, "yyyymmdd HH:MM:SS") 


select DATEDIFF ( minute , :ls_data_1 , :ls_data_2 )
into :ll_datediff
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

if isnull(ll_datediff) then ll_datediff=0

return ll_datediff
end function

public function long uof_datediff_in_minutes (datetime fdt_data_inizio, datetime fdt_ora_inizio, datetime fdt_data_fine, datetime fdt_ora_fine);

fdt_data_inizio = datetime(date(fdt_data_inizio), time(fdt_ora_inizio))
fdt_data_fine = datetime(date(fdt_data_fine), time(fdt_ora_fine))

return uof_datediff_in_minutes(fdt_data_inizio, fdt_data_fine)
end function

on uo_richieste_stat.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_richieste_stat.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

