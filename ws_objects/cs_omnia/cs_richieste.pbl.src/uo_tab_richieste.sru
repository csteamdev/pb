﻿$PBExportHeader$uo_tab_richieste.sru
forward
global type uo_tab_richieste from nonvisualobject
end type
end forward

global type uo_tab_richieste from nonvisualobject
end type
global uo_tab_richieste uo_tab_richieste

type variables
boolean ib_global_service = false
end variables

forward prototypes
public function integer uof_setddlb (ref uo_cs_xx_dw fdw_datawindow)
public function integer uof_doubleclicked (ref uo_cs_xx_dw fdw_datawindow, long row, dwobject dwo)
public function integer uof_rowfocuschanged (ref uo_cs_xx_dw fdw_datawindow, long newrow)
public function integer uof_itemchanged (ref uo_cs_xx_dw fdw_datawindow, long row, string fs_colname, string fs_coltext)
public function integer uof_clicked (ref uo_cs_xx_dw fdw_datawindow, long row, dwobject dwo)
public function integer uof_crea_offerta (long fl_anno_richiesta, long fl_num_richiesta, ref string fs_errore)
public function integer uof_crea_det_off_ven (long fl_anno_off_ven, long fl_num_off_ven, long fl_anno_richiesta, long fl_num_richiesta, ref string fs_errore)
public function integer uof_new_modify (ref datawindow fdw_datawindow, boolean fb_newmodify)
public function integer uof_crea_nc_reclami (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_errore)
public function integer uof_crea_intervento (ref uo_cs_xx_dw fdw_datawindow, ref string fs_errore)
public function integer uof_visualizza_mappa (double fd_latitudine, double fd_longitudine)
public subroutine uf_rimpiazza (ref string as_mystring)
public function integer uof_invia_mail (long fl_row, datawindow fd_dw)
public function integer uof_genera_int (uo_cs_xx_dw fdw_datawindow)
private function integer uof_note_strumentali (ref uo_cs_xx_dw fdw_datawindow)
public function boolean uof_sospensioni (integer ai_anno_reg, long al_num_reg, ref datetime adt_data_sospensione)
end prototypes

public function integer uof_setddlb (ref uo_cs_xx_dw fdw_datawindow);// caricamento DROP DOWN iniziali
boolean lb_exit=false
string ls_str, ls_oggetto, ls_type, ls_visible, ls_dw
long   ll_pos

ls_str = fdw_datawindow.Object.DataWindow.Objects
ls_dw = fdw_datawindow.dataobject
do while true
	ll_pos = pos(ls_str, "~t", 1)
	if ll_pos < 1 then 
		ls_oggetto = ls_str
		lb_exit=true
	else
		ls_oggetto = left(ls_str, ll_pos - 1)
	end if
			
	ls_visible = fdw_datawindow.Describe(ls_oggetto + ".visible")
	ls_type = fdw_datawindow.Describe(ls_oggetto + ".type")
	if ls_type = "column" and ls_visible <> '0' then

		choose case ls_oggetto
			case "cod_tipo_richiesta"
				f_PO_LoadDDDW_sort(fdw_datawindow,ls_oggetto,sqlca,&
									  "tab_tipi_richieste","cod_tipo_richiesta","des_tipo_richiesta",&
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " des_tipo_richiesta ASC ")

			case "cod_richiedente"
				f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_richiedente", &
									  sqlca, &
									  "tab_richiedenti", &
									  "cod_richiedente", &
									  "nominativo", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
									  " nominativo ASC ")
									  
			case "cod_divisione"
					//in d_call_center_1 non c'è +la drop claudia 14/03/07
					// stefanop 02/03/2009: vedi commento claudia.		
					// stefano 05/05/2010: aggiunta d_call_center_8
					// EnMe 19/5/2011 aggiunta anche d_call_center_10
				if ls_dw <> "d_call_center_1" and ls_dw <> "d_call_center_3" and &
				   ls_dw <> "d_call_center_4" and ls_dw <> "d_call_center_5" and &
				   ls_dw <> "d_call_center_6" and ls_dw <> "d_call_center_8" and &
				   ls_dw <> "d_call_center_10"  and ls_dw <> "d_call_center_12" then 
					f_PO_LoadDDDW_sort(fdw_datawindow,ls_oggetto,sqlca,&
										  "anag_divisioni","cod_divisione","des_divisione",&
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " des_divisione ASC ")
				end if
			
			case "cod_difformita"
				f_PO_LoadDDDW_sort(fdw_datawindow,ls_oggetto,sqlca,&
									  "tab_difformita","cod_errore","des_difformita",&
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " des_difformita ASC ")
			
		
			case "cod_area_chiamata"
				f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_area_chiamata", &
									  sqlca, &
									  "tab_aree_chiamate", &
									  "cod_area_chiamata", &
									  "des_area_chiamata", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " des_area_chiamata ASC ")

		
			case "cod_guasto"
					// non caricare; si carica da itemchanged dell'attrezzatura
					
					// stefano 07/05/2010: carico guasti generici solo se d_call_center_8
					if fdw_datawindow.dataobject = "d_call_center_8" then
						f_po_loaddddw_sort(fdw_datawindow, &
										  "cod_guasto", &
										  sqlca, &
										  "tab_guasti_generici", &
										  "cod_guasto", &
										  "des_guasto", &
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " des_guasto ASC ")
										  
						f_po_loaddddw_sort(fdw_datawindow, &
										  "cod_tipo_richiesta", &
										  sqlca, &
										  "tab_tipi_richieste", &
										  "cod_tipo_richiesta", &
										  "des_tipo_richiesta", &
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " des_tipo_richiesta ASC ")
					end if
					
					
			case "cod_deposito"
				f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_deposito", &
									  sqlca, &
									  "anag_depositi", &
									  "cod_deposito", &
									  "des_deposito", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " des_deposito ASC ")
					
			case "cod_area_aziendale"
					// non caricare; si carica da itemchanged della commessa
			case "cod_reparto"
					// non caricare; si carica da itemchanged della commessa
					//*****************claudia 30/11/06 aggiunto controllo del flag blocco a no anomalia segnalata da baggio
			case "cod_operaio_risolve"
				f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_operaio_risolve", &
									  sqlca, &
									  "anag_operai", &
									  "cod_operaio", &
									  "cognome + ' ' + nome", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N'", &
									  " cognome ASC, nome ASC ")

			case "cod_operaio_incaricato"
				f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_operaio_incaricato", &
									  sqlca, &
									  "anag_operai", &
									  "cod_operaio", &
									  "cognome + ' ' + nome", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N'", &
									  " cognome ASC, nome ASC ")
									  
			case "cod_operaio_registrazione"
				f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_operaio_registrazione", &
									  sqlca, &
									  "anag_operai", &
									  "cod_operaio", &
									  "cognome + ' ' + nome", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N'", &
									  " cognome ASC, nome ASC ")

			case "cod_tipologia_richiesta"
				f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_tipologia_richiesta", &
									  sqlca, &
									  "tab_richieste_tipologie", &
									  "cod_tipologia_richiesta", &
									  "des_tipologia_richiesta", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
									  " des_tipologia_richiesta ")

			
			case "cod_classe_richiesta"
				
				f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_classe_richiesta", &
									  sqlca, &
									  "tab_richieste_classificazioni", &
									  "cod_classe_richiesta", &
									  "des_classe_richiesta + ' ' + des_ore_intervento", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
									  " des_classe_richiesta ")

			case "anno_reg_richiesta_obsoleta"
				fdw_datawindow.modify("anno_reg_richiesta_obsoleta.visible=~"0~tif(flag_richiesta_obsoleta='S',1,0)~"")

			case "num_reg_richiesta_obsoleta"
				fdw_datawindow.modify("num_reg_richiesta_obsoleta.visible=~"0~tif(flag_richiesta_obsoleta='S',1,0)~"")
	
			case "cod_documento"
				f_po_loaddddw_dw(fdw_datawindow, &
									  "cod_documento", &
									  sqlca, &
									  "tab_documenti", &
									  "cod_documento", &
									  "des_documento", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					
			case "cod_cat_risorse_esterne"
				// stefano 05/05/2010: in d_call_center_8 non c'è
				if ls_dw <> "d_call_center_8" then
					f_po_loaddddw_sort(fdw_datawindow, &
										  "cod_cat_risorse_esterne", &
										  sqlca, &
										  "tab_cat_risorse_esterne", &
										  "cod_cat_risorse_esterne", &
										  "des_cat_risorse_esterne", &
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
										  " des_cat_risorse_esterne ASC ")	
				end if
									  
			case "cod_risorsa_esterna"
				// stefano 05/05/2010: in d_call_center_8 non c'è
				if ls_dw <> "d_call_center_8" then
					f_po_loaddddw_sort(fdw_datawindow, &
										  "cod_risorsa_esterna", &
										  sqlca, &
										  "anag_risorse_esterne", &
										  "cod_risorsa_esterna", &
										  "descrizione", &
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
										  " descrizione ASC ")
				end if
			
			case "cod_resp_ver_efficacia"
				f_po_loaddddw_dw(fdw_datawindow, &
						  "cod_resp_ver_efficacia", &
						  sqlca, &
						  "mansionari", &
						  "cod_resp_divisione", &
						  "cognome + ' ' + nome", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  

	
		end choose

	end if
	
	if lb_exit then exit
	ls_str = mid(ls_str, ll_pos + 1)

loop

return 0
end function

public function integer uof_doubleclicked (ref uo_cs_xx_dw fdw_datawindow, long row, dwobject dwo);choose case fdw_datawindow.dataobject
	case 'd_call_center_elenco_man'
		
		if dwo.name = "cf_manutenzione" then
			if not isnull(row) and row > 0 then
				if not isvalid(w_manutenzioni) then
					window_open(w_manutenzioni,-1)
				else
					w_manutenzioni.show()
				end if
				w_manutenzioni.wf_carica_singola_registrazione(fdw_datawindow.getitemnumber(row,"manutenzioni_anno_registrazione"),fdw_datawindow.getitemnumber(row,"manutenzioni_num_registrazione"))
			end if
		end if

end choose

return 0
end function

public function integer uof_rowfocuschanged (ref uo_cs_xx_dw fdw_datawindow, long newrow);string			ls_describe, ls_risposta, ls_flag_richiedente_codificato, ls_cod_attrezzatura,ls_cod_divisione

long				ll_anno, ll_numero

datetime		ldt_data_sospensione


//campo testo sospensione --------------------------------------------------------------------------------
choose case fdw_datawindow.dataobject
		
	case "d_call_center_1", "d_call_center_3", "d_call_center_4","d_call_center_5","d_call_center_6",&
			"d_call_center_8","d_call_center_10", "d_call_center_12","d_call_center_manutenz_semplice"
		if newrow>0 then
			ll_anno = fdw_datawindow.getitemnumber(newrow, "anno_registrazione")
			ll_numero = fdw_datawindow.getitemnumber(newrow, "num_registrazione")
			
			if ll_anno > 0 and ll_numero > 0 then
				if uof_sospensioni(ll_anno, ll_numero, ldt_data_sospensione) then
					ls_describe = "Sospesa dal "+string(ldt_data_sospensione, "dd/mm/yyyy")
				else
					ls_describe = ""
				end if
				fdw_datawindow.modify("flag_sospesa_t.text='"+ls_describe+"'")
			end if
		end if
		
end choose
//------------------------------------------------------------------------------------------------------------------


//campo testo durata del guasto---------------------------------------------------------------------------
choose case fdw_datawindow.dataobject
		
	case "d_call_center_2","d_call_center_7","d_call_center_9"
		if newrow>0 then
			
			ll_anno = fdw_datawindow.getitemnumber(newrow, "anno_registrazione")
			ll_numero = fdw_datawindow.getitemnumber(newrow, "num_registrazione")
			
			if ll_anno > 0 and ll_numero > 0 then
				ls_describe = fdw_datawindow.Describe("durata_guasto_t.Visible")
				
				if ls_describe = "!" or ls_describe="?" then
					//campo testo non contemplato sulla datawindow
				else
					ls_describe = f_durata_guasto_richiesta_str( ll_anno ,  ll_numero )
					
					if ls_describe <> "" then
						ls_describe = "Durata del guasto:~r~n" + ls_describe
					else
						ls_describe = ""
					end if
				
					try
						fdw_datawindow.modify("durata_guasto_t.text='"+ls_describe+"'")
						//fdw_datawindow.object.durata_guasto_t.text = ls_describe
					catch (runtimeerror err2)
					end try
				end if
			end if
			
		end if
end choose
//------------------------------------------------------------------------------------------------------------------



choose case fdw_datawindow.dataobject
	case 'd_call_center_lista'

	case 'd_call_center_1'
		
	case 'd_call_center_2', 'd_call_center_manutenz_semplice_chiusura', 'd_call_center_7', 'd_call_center_9'
		
		if fdw_datawindow.getitemstatus(newrow,0,primary!) = New! or fdw_datawindow.getitemstatus(newrow,0,primary!) = NewModified! then
			return 0
		end if				
		
		ll_anno = fdw_datawindow.getitemnumber(newrow, "anno_registrazione")
		ll_numero = fdw_datawindow.getitemnumber(newrow, "num_registrazione")
		
		ls_describe = fdw_datawindow.Describe("risposta_richiesta.Visible")
		
		choose case ls_describe
				
			case "!","?"
				return 0
				
			case "1"
				// continue
				
			case else
				return 0
				
		end choose
				
		
		if not (ll_anno > 0 and ll_numero > 0) then
			return 0
		end if
		
		ls_risposta = f_descrizione_risposta_richiesta(ll_anno, ll_numero,"S")
		
		if not isnull(ls_risposta) then
			
			fdw_datawindow.setitem(newrow,"risposta_richiesta",ls_risposta)
		
			fdw_datawindow.Modify("risposta_richiesta.tabsequence=1")
			fdw_datawindow.Modify("risposta_richiesta.Edit.DisplayOnly=Yes")
			fdw_datawindow.Modify("risposta_richiesta.Edit.HScrollBar=Yes")
			fdw_datawindow.Modify("risposta_richiesta.Edit.VScrollBar=Yes")
			fdw_datawindow.Modify("risposta_richiesta.Edit.AutoVScroll=Yes")
			fdw_datawindow.Modify("risposta_richiesta.Edit.AutoVScroll=Yes")
			
			fdw_datawindow.setitemstatus(newrow, "risposta_richiesta", primary!,NotModified! )
			
		end if
	

	
	case 'd_call_center_5', "d_call_center_12"
		ls_cod_divisione = fdw_datawindow.getitemstring(newrow, "cod_divisione")
		
		if not isnull(ls_cod_divisione) and len(ls_cod_divisione) > 0 then
			f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_richiedente", &
									  sqlca, &
									  "tab_richiedenti", &
									  "cod_richiedente", &
									  "nominativo", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and (cod_divisione = '"+ ls_cod_divisione +"' or cod_divisione is null)", " nominativo ASC ")
		else
			// carico la lista con un codice divisione assurdo tanto per resettarla
			f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_richiedente", &
									  sqlca, &
									  "tab_richiedenti", &
									  "cod_richiedente", &
									  "nominativo", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = 'CODICEINESISTENTE'", " nominativo ASC ")
		end if
	
	case 'd_call_center_11'
		
			ls_cod_attrezzatura = fdw_datawindow.getitemstring(newrow, "cod_attrezzatura")
			
			if not isnull(ls_cod_attrezzatura) then
				f_PO_LoadDDDW_sort(fdw_datawindow, &
					 "cod_guasto", &
					 sqlca, &
					 "tab_guasti", &
					 "cod_guasto", &
					 "des_guasto + ' ' + cod_attrezzatura", &
					 "cod_azienda = '" + s_cs_xx.cod_azienda + &
					 "' and cod_attrezzatura = '" + ls_cod_attrezzatura + "' union all select cod_guasto, des_guasto from tab_guasti_generici where cod_azienda = '" + &
					  s_cs_xx.cod_azienda + "' and cod_guasto not in ( select cod_guasto from tab_guasti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '"+ls_cod_attrezzatura+"' )" , &
					  "1")								  
			else
				// carico la lista con un codice attrezzatura assurdo tanto per resettarla
				f_PO_LoadDDDW_sort(fdw_datawindow, &
					 "cod_guasto", &
					 sqlca, &
					 "tab_guasti", &
					 "cod_guasto", &
					 "des_guasto + ' ' + cod_attrezzatura", &
					 "cod_azienda = '" + s_cs_xx.cod_azienda + &
					 "' and cod_attrezzatura = 'aaazxy' " , &
					  "1")								  
			end if				
			
end choose

return 0
end function

public function integer uof_itemchanged (ref uo_cs_xx_dw fdw_datawindow, long row, string fs_colname, string fs_coltext);string ls_str, ls_null, ls_cod_area_aziendale, ls_cod_divisione, ls_cod_tipo_richiesta
long   ll_null

choose case fdw_datawindow.dataobject
	case "d_call_center_1", "d_call_center_3", "d_call_center_4",  "d_call_center_3","d_call_center_6", "d_call_center_11"
		
		choose case fs_colname
				
			case "cod_divisione"
				
				if isnull(fs_coltext) or len(fs_coltext) < 1 then
					fs_coltext = "%"
				end if
				
				f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_richiedente", &
									  sqlca, &
									  "tab_richiedenti", &
									  "cod_richiedente", &
									  "nominativo", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and (cod_divisione like '"+ fs_coltext +"' or cod_divisione is null)", " nominativo ASC ")

				
				f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_area_chiamata", &
									  sqlca, &
									  "tab_aree_chiamate", &
									  "cod_area_chiamata", &
									  "des_area_chiamata", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and (cod_divisione like '"+ fs_coltext +"' or cod_divisione is null)", " des_area_chiamata ASC ")

			case "cod_tipo_richiesta"
				setnull(ls_null)			
				setnull(ll_null)
				if not isnull(fdw_datawindow.getitemstring(row, "cod_documento")) then
					messagebox("OMNIA", "Numeratore azzerato impostarlo manualmente.~r~nATTENZIONE potrebbero restare dei buchi!")
				end if
				fdw_datawindow.setitem(row, "cod_documento", ls_null)
				fdw_datawindow.setitem(row, "numeratore_documento", ls_null)
				fdw_datawindow.setitem(row, "anno_documento", ll_null)
				fdw_datawindow.setitem(row, "num_documento", ll_null)
				
				
			case "cod_cat_risorse_esterne"
				setnull(ls_null)
				fdw_datawindow.setitem(row,"cod_risorsa_esterna",ls_null)
				
				
			case "cod_attrezzatura"
				
				if not isnull(fs_coltext) and len(fs_coltext) > 0 then
				
					select cod_area_aziendale
					into	 :ls_cod_area_aziendale
					from   anag_attrezzature
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_attrezzatura = :fs_coltext;
				
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("OMNIA", "Errore in ricerca attrezzatura")
						return 1
					elseif ls_cod_area_aziendale <> "" and not isnull(ls_cod_area_aziendale) then
						
						select cod_divisione
						into   :ls_cod_divisione
						from   tab_aree_aziendali
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_area_aziendale = :ls_cod_area_aziendale;
						
						if sqlca.sqlcode <> 0 then
							g_mb.messagebox("OMNIA", "Errore in ricerca attrezzatura")
							return 1
						else
							fdw_datawindow.setitem(fdw_datawindow.getrow(), "cod_divisione",      ls_cod_divisione)
							fdw_datawindow.setitem(fdw_datawindow.getrow(), "cod_area_aziendale", ls_cod_area_aziendale)
						end if
						
					end if
					
				else
					setnull(ls_null)
					fdw_datawindow.setitem(fdw_datawindow.getrow(), "cod_divisione",      ls_null)
					fdw_datawindow.setitem(fdw_datawindow.getrow(), "cod_area_aziendale", ls_null)
					
				end if		
				
				if  fdw_datawindow.dataobject = "d_call_center_11"  then

					f_PO_LoadDDDW_sort(fdw_datawindow, &
						 "cod_guasto", &
						 sqlca, &
						 "tab_guasti", &
						 "cod_guasto", &
						 "des_guasto + ' ' + cod_attrezzatura", &
						 "cod_azienda = '" + s_cs_xx.cod_azienda + &
						 "' and cod_attrezzatura = '" + fs_coltext + "' union all select cod_guasto, des_guasto from tab_guasti_generici where cod_azienda = '" + &
						  s_cs_xx.cod_azienda + "' and cod_guasto not in ( select cod_guasto from tab_guasti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '"+fs_coltext+"' )" , &
						  "1")								  
					
					fdw_datawindow.setitem(fdw_datawindow.getrow(),"cod_guasto",ls_null)
					
				end if
				
		end choose
		
		
	case "d_call_center_5", "d_call_center_12"
		
		choose case fs_colname
				
			case "cod_divisione"
				setnull(ll_null)
				setnull(ls_null)
				
				if not isnull(fs_coltext) and len(fs_coltext)>0 then
					f_po_loaddddw_sort(fdw_datawindow, &
										  "cod_richiedente", &
										  sqlca, &
										  "tab_richiedenti", &
										  "cod_richiedente", &
										  "nominativo", &
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and (cod_divisione = '"+ fs_coltext +"' or cod_divisione is null)", " nominativo ASC ")
					
				else
					//reset della dropdown richiedenti e del campo ubicazione (che dipende da cod divisione)
					f_po_loaddddw_sort(fdw_datawindow, &
										  "cod_richiedente", &
										  sqlca, &
										  "tab_richiedenti", &
										  "cod_richiedente", &
										  "nominativo", &
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = 'CODICEINESISTENTE'", " nominativo ASC ")
				end if
				//pulisco il campo richiedente e l'ubicazione
				fdw_datawindow.setitem(fdw_datawindow.getrow(), "cod_richiedente", ll_null)
				fdw_datawindow.setitem(fdw_datawindow.getrow(), "cod_area_aziendale", ls_null)
				
				
			case "flag_richiedente_codificato"
				setnull(ls_null)
				if fs_coltext = "S" then
					//vuoi selezionare dalla tab_richiedenti
					fdw_datawindow.setitem(fdw_datawindow.getrow(), "nome_richiedente", ls_null)
					//fdw_datawindow.setitem(fdw_datawindow.getrow(), "nome_particolare", ls_null)
					//fdw_datawindow.setitem(fdw_datawindow.getrow(), "telefono_richiedente", ls_null)
					
//					fdw_datawindow.object.nome_richiedente.visible = "0"
//					fdw_datawindow.object.nome_particolare.visible = "0"
//					fdw_datawindow.object.email_richiedente.visible = "0"
//										
//					fdw_datawindow.object.cod_richiedente.visible = "1"
					fdw_datawindow.setcolumn("cod_richiedente")
				else
					//richiedente manuale
					
					setnull(ll_null)
					fdw_datawindow.setitem(fdw_datawindow.getrow(), "cod_richiedente", ll_null)

//					fdw_datawindow.object.nome_richiedente.visible = "1"
//					fdw_datawindow.object.nome_particolare.visible = "1"
//					fdw_datawindow.object.email_richiedente.visible = "1"
//										
//					fdw_datawindow.object.cod_richiedente.visible = "0"
					fdw_datawindow.setcolumn("nome_richiedente")
					
				end if
				
		end choose
		
		// Stefano 11/07/2010: aggiunta d_call_center_8
	case "d_call_center_8"
		choose case fs_colname
			case "cod_divisione"
				if not isnull(fs_coltext) and fs_coltext <> "" then
					select cod_tipo_richiesta
					into :ls_cod_tipo_richiesta
					from anag_divisioni
					where
						cod_azienda = :s_cs_xx.cod_azienda and
						cod_divisione = :fs_coltext;
						
					if sqlca.sqlcode < 0 or (sqlca.sqlcode = 0 and isnull(ls_cod_tipo_richiesta)) then
						fdw_datawindow.setitem(row, "cod_tipo_richiesta", ls_null)
					else
						fdw_datawindow.setitem(row, "cod_tipo_richiesta", ls_cod_tipo_richiesta)
					end if
				else
					fdw_datawindow.setitem(row, "cod_tipo_richiesta", ls_null)
				end if
				
			case "cod_area_aziendale"
				if not isnull(fs_coltext) and fs_coltext <> "" then
					select cod_divisione
					into :ls_cod_divisione
					from tab_aree_aziendali
					where
						cod_azienda = :s_cs_xx.cod_azienda and
						cod_area_aziendale = :fs_coltext;
						
					if sqlca.sqlcode = 0 and not isnull(ls_cod_divisione) then
						
						select cod_tipo_richiesta
						into :ls_cod_tipo_richiesta
						from anag_divisioni
						where
							cod_azienda = :s_cs_xx.cod_azienda and
							cod_divisione = :ls_cod_divisione;
							
						if sqlca.sqlcode < 0 or (sqlca.sqlcode = 0 and isnull(ls_cod_tipo_richiesta)) then
							fdw_datawindow.setitem(row, "cod_tipo_richiesta", ls_null)
						else
							fdw_datawindow.setitem(row, "cod_tipo_richiesta", ls_cod_tipo_richiesta)
						end if
						
					end if
				end if
		end choose

end choose

return -1
end function

public function integer uof_clicked (ref uo_cs_xx_dw fdw_datawindow, long row, dwobject dwo);integer						li_risposta

long							ll_anno_reg_richiesta,ll_num_reg_richiesta, ll_cont, ll_row, ll_anno_nc, ll_num_nc

string						ls_null, ls_errore, ls_cod_operaio_risolve, ls_cod_resp, ls_cod_divisione

datetime					ldt_data_effettiva_chiusura, ldt_data_sospensione

double						ld_lat, ld_long

window						lw_open




choose case fdw_datawindow.dataobject
	// -- stefanop 02/03/2009: Aggiunta d_call_center_3
	// -- stefanop 22/05/2009: Aggiunta d_call_center_4 (aggiunta colonna cod_attrezzatura)
	case 'd_call_center_1'
		if isvalid(dwo) then
			choose case dwo.name
				case "b_ric_divisioni"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
					s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
					if not isvalid(w_divisioni_ricerca) then
						window_open(w_divisioni_ricerca, 0)
					end if
					w_divisioni_ricerca.show()
				case "cod_risorsa_esterna"
					f_po_loaddddw_sort(fdw_datawindow, &
							  "cod_risorsa_esterna", &
							  sqlca, &
							  "anag_risorse_esterne", &
							  "cod_risorsa_esterna", &
							  "descrizione", &
							  " cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_cat_risorse_esterne") + "' ", &
							  " descrizione ASC ")										  
			end choose	
		end if			

	// -- stefanop 02/03/2009: Codice click d_call_center_3 (SieSolari)
	case 'd_call_center_3'
		if isvalid(dwo) then
			choose case dwo.name
				case "b_ric_aree"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
					s_cs_xx.parametri.parametro_s_1 = "cod_area_aziendale"
					if not isvalid(w_divisioni_ricerca) then
						window_open(w_divisioni_ricerca, 0)
					end if
					w_divisioni_ricerca.show()
				case "b_ric_divisioni"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
					s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
					if not isvalid(w_divisioni_ricerca) then
						window_open(w_divisioni_ricerca, 0)
					end if
					w_divisioni_ricerca.show()
				case "cod_risorsa_esterna"
					f_po_loaddddw_sort(fdw_datawindow, &
							  "cod_risorsa_esterna", &
							  sqlca, &
							  "anag_risorse_esterne", &
							  "cod_risorsa_esterna", &
							  "descrizione", &
							  " cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_cat_risorse_esterne") + "' ", &
							  " descrizione ASC ")		
			end choose
		end if
		
	// -- Donato 22/05/2009: Codice click d_call_center_4
	case 'd_call_center_4'
		if isvalid(dwo) then
			choose case dwo.name
				case "b_ric_aree"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
					s_cs_xx.parametri.parametro_s_1 = "cod_area_aziendale"
					if not isvalid(w_divisioni_ricerca) then
						window_open(w_divisioni_ricerca, 0)
					end if
					w_divisioni_ricerca.show()
					
				case "b_ric_divisioni"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
					s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
					if not isvalid(w_divisioni_ricerca) then
						window_open(w_divisioni_ricerca, 0)
					end if
					w_divisioni_ricerca.show()
					
				case "cod_risorsa_esterna"
					f_po_loaddddw_sort(fdw_datawindow, &
							  "cod_risorsa_esterna", &
							  sqlca, &
							  "anag_risorse_esterne", &
							  "cod_risorsa_esterna", &
							  "descrizione", &
							  " cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_cat_risorse_esterne") + "' ", &
							  " descrizione ASC ")		
							  
				case "b_ric_attrezzature"
					if dwo.enabled = "0" then return 0
					guo_ricerca.uof_ricerca_attrezzatura(fdw_datawindow,"cod_attrezzatura")
							  
			end choose
		end if
		
	// -- Donato 06/10/2009: gestione GPS richiesta e richiedente codificato o non codificato
	case 'd_call_center_5', 'd_call_center_12'
		if isvalid(dwo) then
			choose case dwo.name
				case "b_ric_aree"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
					s_cs_xx.parametri.parametro_s_1 = "cod_area_aziendale"
					if not isvalid(w_divisioni_ricerca) then
						window_open(w_divisioni_ricerca, 0)
					end if
					w_divisioni_ricerca.show()
				case "b_ric_divisioni"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
					s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
					if not isvalid(w_divisioni_ricerca) then
						window_open(w_divisioni_ricerca, 0)
					end if
					w_divisioni_ricerca.show()
				case "cod_risorsa_esterna"
					f_po_loaddddw_sort(fdw_datawindow, &
							  "cod_risorsa_esterna", &
							  sqlca, &
							  "anag_risorse_esterne", &
							  "cod_risorsa_esterna", &
							  "descrizione", &
							  " cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_cat_risorse_esterne") + "' ", &
							  " descrizione ASC ")
							  
				case "b_mappa"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					ld_lat = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"lat_richiesta")
					ld_long = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"long_richiesta")
						
					if isnull(ld_lat) then ld_lat = 0
					if isnull(ld_long) then ld_long = 0
						
					uof_visualizza_mappa(ld_lat, ld_long)
					
			end choose
		end if
		
	// -- Donato 27/01/2010: gestione segnalazioni anomalie (specif.interna migrazione)
	case 'd_call_center_6',  'd_call_center_11',  'd_call_center_10'
		if isvalid(dwo) then
			choose case dwo.name
				case "b_mail"
					uof_invia_mail(row, fdw_datawindow)
					
				case "b_ric_attrezzature"
					if dwo.enabled = "0" then return 0
					guo_ricerca.uof_ricerca_attrezzatura(fdw_datawindow,"cod_attrezzatura")
					
				case "b_ric_cliente"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					guo_ricerca.uof_ricerca_cliente(fdw_datawindow,"cod_cliente")
					
				case "b_ric_prodotto"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					guo_ricerca.uof_ricerca_prodotto(fdw_datawindow,"cod_prodotto")
									
				case "b_ric_divisioni"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
					s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
					if not isvalid(w_divisioni_ricerca) then
						window_open(w_divisioni_ricerca, 0)
					end if
					w_divisioni_ricerca.show()
					
				case "b_add_richiedente"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					
					ls_cod_divisione = fdw_datawindow.getitemstring(fdw_datawindow.getrow(), "cod_divisione")
					if ls_cod_divisione="" then setnull(ls_cod_divisione)
					
					s_cs_xx.parametri.parametro_s_1 = ls_cod_divisione
					
					window_open(w_richiedenti_response, 0)
					
					
					
					if ls_cod_divisione="" or isnull(ls_cod_divisione) then
						f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_richiedente", &
									  sqlca, &
									  "tab_richiedenti", &
									  "cod_richiedente", &
									  "nominativo", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
									  " nominativo ASC ")
					else
						f_po_loaddddw_sort(fdw_datawindow, &
									  "cod_richiedente", &
									  sqlca, &
									  "tab_richiedenti", &
									  "cod_richiedente", &
									  "nominativo", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione='"+ls_cod_divisione+"'", &
									  " nominativo ASC ")
					end if					
					
				case "b_ric_aree"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
					s_cs_xx.parametri.parametro_s_1 = "cod_area_aziendale"
					if not isvalid(w_divisioni_ricerca) then
						window_open(w_divisioni_ricerca, 0)
					end if
					w_divisioni_ricerca.show()
					
				// stefanop 10/04/2012: spec. "ordini_lavoro_richieste" alpiq
				case "b_note_strumentali"
					if dwo.enabled = "0" then return 0
					uof_note_strumentali(ref fdw_datawindow)
				// ----
				
			end choose
		end if			
			
	case 'd_call_center_manutenz_semplice'
		if isvalid(dwo) then

			choose case dwo.name
				case "b_ric_attrezzature"
					if dwo.enabled = "0" then return 0
					guo_ricerca.uof_ricerca_attrezzatura(fdw_datawindow,"cod_attrezzatura")				
					
				case "b_genera_int"
					if dwo.enabled = "0" then return 0
					setnull(ls_null)
					
					if fdw_datawindow.getitemstring(fdw_datawindow.getrow(), "flag_richiesta_chiusa") = "S" then
						messagebox("OMNIA", "Attenzione: richiesta risolta, impossibile generare interventi!")
						return 0
					end if
					
					if fdw_datawindow.getitemstring(fdw_datawindow.getrow(), "flag_richiesta_obsoleta") = "S" then
						messagebox("OMNIA", "Attenzione: richiesta obsoleta, impossibile generare interventi!")
						return 0
					end if
					
					ll_anno_reg_richiesta = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")
					ll_num_reg_richiesta  = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")
					
					//se ci sono sospensioni attive, avvisa ma prosegui lo stesso, se l'utente desidera
					setnull(ldt_data_sospensione)
					if uof_sospensioni(	ll_anno_reg_richiesta, ll_num_reg_richiesta, ldt_data_sospensione) then	
						if not g_mb.confirm("Attenzione: per la richiesta risulta una sospensione attiva dal "+string(ldt_data_sospensione, "dd/mm/yyyy")+"! Continuare lo stesso?") then
							return 0
						end if
					end if
					//--------------------------------
					
					select count(*)
					into   :ll_cont
					from   manutenzioni
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 anno_reg_richiesta = :ll_anno_reg_richiesta and
							 num_reg_richiesta = :ll_num_reg_richiesta;
							 
					if not isnull(ll_cont) and ll_cont > 0 then
						li_risposta = MessageBox("OMNIA", "Esistono già attività di manutenzione su questa richiesta.~r~nCreare una nuova attività di manutenzione?",Exclamation!, YesNo!, 2)
					else
						li_risposta = 1
					end if
					
					if li_risposta = 1 THEN
						s_cs_xx.parametri.parametro_s_1 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_gen_manutenzione")
						s_cs_xx.parametri.parametro_s_2 = ls_null
						s_cs_xx.parametri.parametro_s_3 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_operaio_incaricato")
						if isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"des_richiesta")) then
							s_cs_xx.parametri.parametro_s_4 = ""
						else
							s_cs_xx.parametri.parametro_s_4 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"des_richiesta")
						end if
						if isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"risposta")) then
							s_cs_xx.parametri.parametro_s_5 = ""
						else
							s_cs_xx.parametri.parametro_s_5 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"risposta")
						end if
						if isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"messaggio_errore")) then
							s_cs_xx.parametri.parametro_s_6 = ""
						else
							s_cs_xx.parametri.parametro_s_6 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"messaggio_errore")
						end if
						s_cs_xx.parametri.parametro_s_7 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_richiesta_straordinaria")
						s_cs_xx.parametri.parametro_d_1 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")
						s_cs_xx.parametri.parametro_d_2 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")
						setnull(s_cs_xx.parametri.parametro_d_3)
						setnull(s_cs_xx.parametri.parametro_d_4)
						s_cs_xx.parametri.parametro_data_1 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"data_inizio_attivita")
						s_cs_xx.parametri.parametro_data_2 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"ora_inizio_attivita")
						s_cs_xx.parametri.parametro_data_3 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"data_fine_attivita")
						s_cs_xx.parametri.parametro_data_4 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"ora_fine_attivita")
						
						s_cs_xx.parametri.parametro_s_10 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_cat_risorse_esterne")
						s_cs_xx.parametri.parametro_s_11 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_risorsa_esterna")
						
						
						ll_row = fdw_datawindow.getrow()
						
						window_open(w_call_center_conferma_manut, 0)	
						
						fdw_datawindow.change_dw_current()
						fdw_datawindow.postevent("ue_esegui_retrieve")
						
					end if
			end choose	
		end if		
					
	case 'd_call_center_visite_ispettive'
		if isvalid(dwo) then
			
			choose case dwo.name
				case "b_genera_visita_isp"
					if dwo.enabled = "0" then return 0
					if messagebox("OMNIA","Procedo con la creazione di una Visita Ispettiva?",Question!,YesNo!,2) = 1 then
						s_cs_xx.parametri.parametro_s_1 = "RICHIESTA"
						s_cs_xx.parametri.parametro_i_1 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")
						s_cs_xx.parametri.parametro_i_2 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_non_conf")
						s_cs_xx.parametri.parametro_d_1 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")
						s_cs_xx.parametri.parametro_d_2 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_non_conf")
						window_open(w_tes_visite_ispettive, -1)					
					end if					
			end choose
		end if

	// stefano 07/05/2010: aggiunta d_call_center_9
	case 'd_call_center_2', 'd_call_center_manutenz_semplice_chiusura','d_call_center_7', 'd_call_center_9'
		if isvalid(dwo) then
			
			choose case dwo.name
				case "b_crea_off_ven"
					if dwo.enabled = "0" then return 0
					if row > 0 then
						// creazione offerte ammessa solo per straordinarie
						if fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_richiesta_straordinaria") = "N" then
						  messagebox("OMNIA","Generazione offerte ammessa solo per richieste straordinarie",stopsign!)
						  rollback;
						else
						
							if uof_crea_offerta(fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione"), &
													  fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione"), ref ls_errore) <> 0 then
								  rollback;
								  messagebox("OMNIA",ls_errore)
							end if
							commit;
						end if
					end if				
			
				case "b_off_ven"
					if dwo.enabled = "0" then return 0
					if row > 0 then
						// creazione offerte ammessa solo per straordinarie
						if fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_richiesta_straordinaria") = "N" then
						  messagebox("OMNIA","Gestione offerte ammessa solo per richieste straordinarie",stopsign!)
						  rollback;
						else
							s_cs_xx.parametri.parametro_d_1 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")
							s_cs_xx.parametri.parametro_d_2 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")
							s_cs_xx.parametri.parametro_s_1 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_divisione")
							window_open(w_call_center_off_ven, -1)
						end if
					end if				
			
				case "b_manut_eseguite"
					if dwo.enabled = "0" then return 0
					if row > 0 then
						s_cs_xx.parametri.parametro_d_1 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")
						s_cs_xx.parametri.parametro_d_2  = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")
						s_cs_xx.parametri.parametro_s_1 = "T"
						window_open(w_call_center_elenco_man, -1)
					end if	
					
				// stefano 06/05/2010: offerte clienti !!! window sul global service !!!
				case "b_off_fornitori"
					s_cs_xx.parametri.parametro_d_1 = fdw_datawindow.getitemnumber(row, "anno_registrazione")
					s_cs_xx.parametri.parametro_d_2 = fdw_datawindow.getitemnumber(row, "num_registrazione")
					s_cs_xx.parametri.parametro_s_1 = fdw_datawindow.getitemstring(row, "cod_divisione")
					
					if s_cs_xx.parametri.parametro_d_1 > 1900 and s_cs_xx.parametri.parametro_d_2 > 0 then
						if not isvalid(w_offerte_fornitori) then
							window_open_parm(w_offerte_fornitori, -1, fdw_datawindow)
						end if
						w_offerte_fornitori.show()
					end if
					
				//donato 11/06/2010 specifica importazione_sri per eng2k: fatture vene acq delle richieste (funz 2 e 3)
				case "b_tes_fat_acq"
					if dwo.enabled = "0" then return 0
					if row > 0 then
						// gestione fatture ammessa solo per straordinarie
						if fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_richiesta_straordinaria") = "N" then
						  messagebox("OMNIA","Gestione Fatture di Acquisto ammessa solo per richieste straordinarie",stopsign!)
						  rollback;
						else
							s_cs_xx.parametri.parametro_d_1 = fdw_datawindow.getitemnumber(row, "anno_registrazione")
							s_cs_xx.parametri.parametro_d_2 = fdw_datawindow.getitemnumber(row, "num_registrazione")
							
							if s_cs_xx.parametri.parametro_d_1 > 1900 and s_cs_xx.parametri.parametro_d_2 > 0 then
								window_open_parm(w_call_center_fat_acq, -1, fdw_datawindow)
							end if
						end if
					end if
					
				case "b_tes_fat_ven"
					if dwo.enabled = "0" then return 0
					if row > 0 then
						// gestione fatture ammessa solo per straordinarie
						if fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_richiesta_straordinaria") = "N" then
						  messagebox("OMNIA","Gestione Fatture di Vendita ammessa solo per richieste straordinarie",stopsign!)
						  rollback;
						else
							s_cs_xx.parametri.parametro_d_1 = fdw_datawindow.getitemnumber(row, "anno_registrazione")
							s_cs_xx.parametri.parametro_d_2 = fdw_datawindow.getitemnumber(row, "num_registrazione")
							
							if s_cs_xx.parametri.parametro_d_1 > 1900 and s_cs_xx.parametri.parametro_d_2 > 0 then
								window_open_parm(w_call_center_fat_ven, -1, fdw_datawindow)
							end if
						end if
					end if
					//fine modifica ---------------------------------------------------------------------------------------------------------------------
				
			end choose
			
		end if
		
		
		if isvalid(dwo) then
			if dwo.name = "b_genera_int" then
				if dwo.enabled = "0" then return 0
			
				setnull(ls_null)
				
				if fdw_datawindow.getitemstring(fdw_datawindow.getrow(), "flag_richiesta_chiusa") = "S" then
					messagebox("OMNIA", "Attenzione: richiesta risolta, impossibile generare interventi!")
					return 0
				end if
				
				if fdw_datawindow.getitemstring(fdw_datawindow.getrow(), "flag_richiesta_obsoleta") = "S" then
					messagebox("OMNIA", "Attenzione: richiesta obsoleta, impossibile generare interventi!")
					return 0
				end if
				
				ll_anno_reg_richiesta = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")
				ll_num_reg_richiesta  = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")
				
				//se ci sono sospensioni attive, avvisa ma prosegui lo stesso, se l'utente desidera
				setnull(ldt_data_sospensione)
				if uof_sospensioni(	ll_anno_reg_richiesta, ll_num_reg_richiesta, ldt_data_sospensione) then	
					if not g_mb.confirm("Attenzione: per la richiesta risulta una sospensione attiva dal "+string(ldt_data_sospensione, "dd/mm/yyyy")+"! Continuare lo stesso?") then
						return 0
					end if
				end if
				//--------------------------------
				
				
				select count(*)
				into   :ll_cont
				from   manutenzioni
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_reg_richiesta = :ll_anno_reg_richiesta and
				       num_reg_richiesta = :ll_num_reg_richiesta;
						 
				if not isnull(ll_cont) and ll_cont > 0 then
					li_risposta = MessageBox("OMNIA", "Esistono già attività di manutenzione su questa richiesta.~r~nCreare una nuova attività di manutenzione?",Exclamation!, YesNo!, 2)
				else
					li_risposta = 1
				end if
				
				if li_risposta = 1 THEN
					s_cs_xx.parametri.parametro_s_1 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_gen_manutenzione")
					s_cs_xx.parametri.parametro_s_2 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_attrezzatura") // imposto l'apparecchiatura selezionata
					s_cs_xx.parametri.parametro_s_3 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_operaio_incaricato")
					if isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"des_richiesta")) then
						s_cs_xx.parametri.parametro_s_4 = ""
					else
						s_cs_xx.parametri.parametro_s_4 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"des_richiesta")
					end if
					if isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"risposta")) then
						s_cs_xx.parametri.parametro_s_5 = ""
					else
						s_cs_xx.parametri.parametro_s_5 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"risposta")
					end if
					if isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"messaggio_errore")) then
						s_cs_xx.parametri.parametro_s_6 = ""
					else
						s_cs_xx.parametri.parametro_s_6 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"messaggio_errore")
					end if
					s_cs_xx.parametri.parametro_s_7 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_richiesta_straordinaria")
					s_cs_xx.parametri.parametro_s_8 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_attrezzatura")
					s_cs_xx.parametri.parametro_d_1 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")
					s_cs_xx.parametri.parametro_d_2 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")
					setnull(s_cs_xx.parametri.parametro_d_3)
					setnull(s_cs_xx.parametri.parametro_d_4)
					s_cs_xx.parametri.parametro_data_1 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"data_inizio_attivita")
					s_cs_xx.parametri.parametro_data_2 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"ora_inizio_attivita")
					s_cs_xx.parametri.parametro_data_3 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"data_fine_attivita")
					s_cs_xx.parametri.parametro_data_4 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"ora_fine_attivita")
					
					s_cs_xx.parametri.parametro_s_10 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_cat_risorse_esterne")
					s_cs_xx.parametri.parametro_s_11 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_risorsa_esterna")
					
					ll_row = fdw_datawindow.getrow()
					
					window_open(w_call_center_conferma_manut, 0)	
					
					//##################################################
					//##################################################
					if fdw_datawindow.dataobject = "d_call_center_7" then
						//chiedere se si vuole inviare una mail
						if g_mb.confirm("OMNIA", "Inviare la segnalazione di chiusura richiesta al cliente?") then &
								uof_invia_mail(row, fdw_datawindow)
					end if
					//##################################################
					//##################################################
					
					fdw_datawindow.change_dw_current()
					fdw_datawindow.postevent("ue_esegui_retrieve")
					
				end if
				
			end if
			
		end if
		
	case 'd_call_center_reclami'
		if isvalid(dwo) then

			choose case dwo.name
				case "b_ric_cliente"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					guo_ricerca.uof_ricerca_cliente(fdw_datawindow,"cod_cliente")	
					
				case "b_ric_prodotto"
					if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					guo_ricerca.uof_ricerca_prodotto(fdw_datawindow,"cod_prodotto")		
					
				case "b_ric_stock"
					if dwo.enabled = "0" then return 0
					
					s_cs_xx.parametri.parametro_s_10 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_prodotto")
					s_cs_xx.parametri.parametro_s_11 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_deposito")
					s_cs_xx.parametri.parametro_s_12 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_ubicazione")
					s_cs_xx.parametri.parametro_s_13 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_lotto")
					setnull(s_cs_xx.parametri.parametro_data_1)
					s_cs_xx.parametri.parametro_d_1 = 0

					fdw_datawindow.change_dw_current()

					s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
					s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
					s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
					s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
					s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
					s_cs_xx.parametri.parametro_s_5 = "data_stock"
					s_cs_xx.parametri.parametro_s_6 = "prog_stock"
					setnull(s_cs_xx.parametri.parametro_s_7)
					setnull(s_cs_xx.parametri.parametro_s_8)

					window_open(w_ricerca_stock, 0)	
					
				case "b_nc" 
					if messagebox("OMNIA","Procedo con la creazione di una Non Conformità?",Question!,YesNo!,2) = 1 then
						
						ll_anno_nc = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_non_conf")
						ll_num_nc = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_non_conf")
						if not isnull(ll_anno_nc) or not isnull(ll_anno_nc) then
							messagebox("OMNIA","Esiste già una NC associata a questo reclamo!",stopsign!)
						else
							
							ll_anno_reg_richiesta = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")
							ll_num_reg_richiesta  = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")
							if uof_crea_nc_reclami(ll_anno_reg_richiesta, ll_num_reg_richiesta, ref ls_errore) < 0 then
								messagebox("OMNIA", ls_errore)
								rollback;
							else
								commit;
							end if
							fdw_datawindow.change_dw_current()
							fdw_datawindow.postevent("ue_esegui_retrieve")
						end if
					end if					
					
				case "b_gen_intervento"    
					if messagebox("OMNIA","Procedo con la creazione di un intervento?",Question!,YesNo!,2) = 1 then
						if uof_crea_intervento(ref fdw_datawindow, ref ls_errore) < 0 then
							messagebox("OMNIA", ls_errore)
							rollback;
							return -1
						end if
						
						commit;
						
					end if					
					
				case "b_chiusura"
					
					ldt_data_effettiva_chiusura = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"data_effettiva_chiusura")
					if not isnull(ldt_data_effettiva_chiusura)  then	
						messagebox("OMNIA","Questa richiesta di intervento è giò stata chiusa in data " + string(ldt_data_effettiva_chiusura, "dd/mm/yyyy"))
						return -1
					end if

					if messagebox("OMNIA","Procedo con l'evasione della richiesta?",Question!,YesNo!,2) = 1 then
						
						select cod_resp_divisione
						into   :ls_cod_resp
						from   mansionari
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_utente = :s_cs_xx.cod_utente and
								 flag_chiusura_nc = 'S';
			 
						if sqlca.sqlcode < 0 then
							messagebox("OMNIA","Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext)
							return -1
						elseif sqlca.sqlcode = 100 or isnull(ls_cod_resp) then
							messagebox("OMNIA","Il mansionario corrente non è abilitato alla chiusura delle richieste di intervento")
							return -1
						end if

						ll_anno_reg_richiesta = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")
						ll_num_reg_richiesta  = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")
						ldt_data_effettiva_chiusura = datetime(today(),00:00:00)
						
						select cod_operaio
						into   :ls_cod_operaio_risolve
						from   anag_operai
						where  cod_azienda = :s_cs_xx.cod_azienda and
						       cod_utente = :s_cs_xx.cod_utente;
								 
						if sqlca.sqlcode <> 0 then setnull(ls_cod_operaio_risolve)

						update tab_richieste
						set    cod_operaio_risolve = :ls_cod_operaio_risolve,
								 data_effettiva_chiusura = :ldt_data_effettiva_chiusura,
								 flag_richiesta_chiusa = 'S'
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :ll_anno_reg_richiesta and
								 num_registrazione = :ll_num_reg_richiesta;
								 
						if sqlca.sqlcode <> 0 then
							messagebox("OMNIA","Errore in chiusura richiesta di intervento.~nErrore nella update di richieste: " + sqlca.sqlerrtext)
							return -1
						end if
						
						commit;
						
						fdw_datawindow.change_dw_current()
						fdw_datawindow.postevent("ue_esegui_retrieve")
						
					end if					
					
			end choose
		end if
		
	// stefano 05/05/2010: fornitori multipli !!! GLOBAL SERVICE !!!
	case "d_call_center_8"
		choose case dwo.name
			case "b_ric_divisioni"
				if dwo.enabled = "0" then return 0
					fdw_datawindow.change_dw_current()
					s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
					s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
				if not isvalid(w_divisioni_ricerca) then
					window_open(w_divisioni_ricerca, 0)
				end if
				w_divisioni_ricerca.show()
				
			case "b_fornitori"
				s_cs_xx.parametri.parametro_s_1 = fdw_datawindow.getitemstring(row, "cod_operaio_incaricato")
				s_cs_xx.parametri.parametro_d_1 = fdw_datawindow.getitemnumber(row, "anno_registrazione")
				s_cs_xx.parametri.parametro_d_2 = fdw_datawindow.getitemnumber(row, "num_registrazione")
				
				if (not isnull(s_cs_xx.parametri.parametro_d_1) and s_cs_xx.parametri.parametro_d_1 > 1900) and &
				(not isnull(s_cs_xx.parametri.parametro_d_2) and s_cs_xx.parametri.parametro_d_2 > 0) then
					
					//window_type_open_parm(w_cs_xx_principale, "w_richieste_fornitori", -1, fdw_datawindow)
					// la finestra sta nella libreria cs_gb_richieste del Global Service
					window_type_open(w_cs_xx_risposta, "w_richieste_fornitori", 0)
					//open(lw_open, "w_richieste_fornitori")
					//destroy lw_open
					
				end if

			case "b_ric_aree"
				if dwo.enabled = "0" then return 0
				fdw_datawindow.change_dw_current()
				s_cs_xx.parametri.parametro_uo_dw_1 = fdw_datawindow
				s_cs_xx.parametri.parametro_s_1 = "cod_area_aziendale"
				if not isvalid(w_divisioni_ricerca) then
					window_open(w_divisioni_ricerca, 0)
				end if
				w_divisioni_ricerca.show()
		end choose
		
end choose

return 0
end function

public function integer uof_crea_offerta (long fl_anno_richiesta, long fl_num_richiesta, ref string fs_errore);// funzione di creazione offerta da richiesta
string ls_cod_divisione, ls_cod_cliente, ls_cod_tipo_richiesta, ls_cod_tipo_off_ven, ls_cod_iva, &
     ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_resa,ls_flag_fuori_fido,ls_flag_riep_boll,ls_flag_riep_fatt, &
     ls_cod_pagamento,ls_cod_agente_1,ls_cod_agente_2,ls_cod_banca_clien_for, ls_cod_operatore, &
	  ls_cod_banca,ls_cod_imballo,ls_cod_vettore,ls_cod_inoltro,ls_cod_mezzo,ls_cod_porto, &
	  ls_cod_deposito,ls_cod_tipo_det_ven, ls_des_tipo_det_ven, ls_des_tipo_richiesta, ls_cod_documento, &
	  ls_flag_doc_suc, ls_flag_st_note, ls_des_prodotto
	  
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven, ll_anno_documento, ll_num_documento

dec{4} ld_sconto, ld_quan_default

datetime ldt_data_registrazione, ldt_data_scadenza, ldt_data_reg_richiesta


if messagebox("OMNIA","Procedo con la creazione dell'offerta?", question!,YesNo!,2) = 1 then
	select cod_divisione,
			 cod_tipo_richiesta,
			 data_registrazione,
			 cod_documento,
			 anno_documento,
			 num_documento
	into   :ls_cod_divisione,
			 :ls_cod_tipo_richiesta,
			 :ldt_data_reg_richiesta,
			 :ls_cod_documento,
			 :ll_anno_documento,
			 :ll_num_documento
	from   tab_richieste
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_richiesta and
			 num_registrazione = :fl_num_richiesta;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca commessa/divisione nelle richieste~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ls_cod_divisione) then
		fs_errore = "Nella richiesta non è stata specificata alcuna commessa/divisione!~r~nImpossibile proseguire."
		return -1
	end if
	
	if isnull(ls_cod_tipo_richiesta) then
		fs_errore = "Nella richiesta non è stata specificata il tipo richiesta!~r~nImpossibile proseguire."
		return -1
	end if

	select cod_cliente
	into   :ls_cod_cliente
	from   anag_divisioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_divisione = :ls_cod_divisione;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca cliente in tabella commessa/divisione~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ls_cod_cliente) then
		fs_errore = "Nessun cliente associato alla commessa/divisione codice:" + ls_cod_divisione
		return -1
	end if

	select cod_tipo_off_ven,
			 cod_tipo_det_ven,
			 des_tipo_richiesta
	into   :ls_cod_tipo_off_ven,
			 :ls_cod_tipo_det_ven,
			 :ls_des_tipo_richiesta
	from   tab_tipi_richieste
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_richiesta = :ls_cod_tipo_richiesta;
	if sqlca.sqlcode = 100 then
		fs_errore = "Nel tipo richiesta non è stato impostato il tipo offerta!~r~nImpossibile proseguire."
		return -1
	end if
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca tipo richiesta in tabella tipi_richieste~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	setnull(ls_cod_operatore)
	select cod_operatore
	  into :ls_cod_operatore
	  from tab_operatori_utenti
	 where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_utente = :s_cs_xx.cod_utente and
			 flag_default = 'S';
			 
	if sqlca.sqlcode = -1 then
		fs_errore = "Errore durante l'Estrazione dell'Operatore di Default"
		return -1
	end if
	
	
	ldt_data_registrazione = datetime(today(),00:00:00)
	ldt_data_scadenza      = datetime(relativedate(today(), 30),00:00:00)

	
	select cod_deposito,
			 cod_valuta,
			 cod_tipo_listino_prodotto,
			 cod_pagamento,
			 sconto,
			 cod_agente_1,
			 cod_agente_2,
			 cod_banca_clien_for,
			 cod_banca,
			 cod_imballo,
			 cod_vettore,
			 cod_inoltro,
			 cod_mezzo,
			 cod_porto,
			 cod_resa,
			 flag_fuori_fido,
			 flag_riep_boll,
			 flag_riep_fatt
	into   :ls_cod_deposito,
			 :ls_cod_valuta,
			 :ls_cod_tipo_listino_prodotto,
			 :ls_cod_pagamento,
			 :ld_sconto,
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca_clien_for,
			 :ls_cod_banca,
			 :ls_cod_imballo,
			 :ls_cod_vettore,
			 :ls_cod_inoltro,
			 :ls_cod_mezzo,
			 :ls_cod_porto,
			 :ls_cod_resa,
			 :ls_flag_fuori_fido,
			 :ls_flag_riep_boll,
			 :ls_flag_riep_fatt
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_cliente = :ls_cod_cliente;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca dati anagrafici del cliente." + sqlca.sqlerrtext
		return -1
	end if
	

	ll_anno_registrazione = f_anno_esercizio()
	
	ll_num_registrazione = 0
	
	select max(num_registrazione)
	into   :ll_num_registrazione
	from   tes_off_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione;
	if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
		ll_num_registrazione = 1
	else
		ll_num_registrazione ++
	end if
	
	update con_off_ven
	set    num_registrazione = :ll_num_registrazione
	where  cod_azienda = :s_cs_xx.cod_azienda;
	
	
	INSERT INTO tes_off_ven  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  cod_cliente,   
			  data_registrazione,   
			  data_scadenza,   
			  cod_operatore,   
			  cod_tipo_off_ven,   
			  cod_deposito,   
			  cod_ubicazione,   
			  cod_valuta,   
			  cambio_ven,   
			  cod_tipo_listino_prodotto,   
			  cod_pagamento,   
			  sconto,   
			  cod_agente_1,   
			  cod_agente_2,   
			  cod_banca,   
			  num_ric_cliente,   
			  data_ric_cliente,   
			  cod_imballo,   
			  aspetto_beni,   
			  peso_netto,   
			  peso_lordo,   
			  num_colli,   
			  cod_vettore,   
			  cod_inoltro,   
			  cod_mezzo,   
			  causale_trasporto,   
			  cod_porto,   
			  cod_resa,   
			  nota_testata,   
			  nota_piede,   
			  flag_fuori_fido,   
			  flag_blocco,   
			  flag_evasione,   
			  flag_riep_bol,   
			  flag_riep_fat,   
			  tot_val_offerta,   
			  cod_contatto,   
			  data_consegna,   
			  cod_banca_clien_for,   
			  tot_val_offerta_valuta,   
			  tot_merci,   
			  tot_spese_trasporto,   
			  tot_spese_imballo,   
			  tot_spese_bolli,   
			  tot_spese_varie,   
			  tot_sconto_cassa,   
			  tot_sconti_commerciali,   
			  imponibile_provvigioni_1,   
			  imponibile_provvigioni_2,   
			  tot_provvigioni_1,   
			  tot_provvigioni_2,   
			  importo_iva,   
			  imponibile_iva,   
			  importo_iva_valuta,   
			  imponibile_iva_valuta,   
			  flag_doc_suc_tes,   
			  flag_doc_suc_pie,   
			  flag_st_note_tes,   
			  flag_st_note_pie,   
			  num_documento,   
			  num_revisione,   
			  spese_sicurezza )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  :ls_cod_cliente,   
			  :ldt_data_registrazione,   
			  :ldt_data_scadenza,   
			  :ls_cod_operatore,   
			  :ls_cod_tipo_off_ven,   
			  :ls_cod_deposito,   
			  null,   
			  :ls_cod_valuta,   
			  1,   
			  :ls_cod_tipo_listino_prodotto,   
			  :ls_cod_pagamento,   
			  :ld_sconto,   
			  :ls_cod_agente_1,   
			  :ls_cod_agente_2,   
			  :ls_cod_banca,   
			  null,   
			  null,   
			  :ls_cod_imballo,   
			  null,   
			  0,   
			  0,   
			  0,   
			  :ls_cod_vettore,   
			  :ls_cod_inoltro,   
			  :ls_cod_mezzo,   
			  null,   
			  :ls_cod_porto,   
			  :ls_cod_resa,   
			  null,   
			  null,   
			  'S',   
			  'N',   
			  'A',   
			  :ls_flag_riep_boll,   
			  :ls_flag_riep_fatt,   
			  0,   
			  null,   
			  :ldt_data_scadenza,   
			  :ls_cod_banca_clien_for,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  'I',   
			  'I',   
			  'I',   
			  'I',   
			  null,   
			  0,   
			  0 )  ;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la creazione dell'offerta." + sqlca.sqlerrtext
		return -1
	end if
	
	// creo la riga di dettaglio di riferimento
	//
	// il fatto che la colonna tab_tipi_richieste.cod_tipo_det_ven <> null significa che
	// voglio creare la riga automaticamente.
	
			
	if uof_crea_det_off_ven(ll_anno_registrazione, ll_num_registrazione,fl_anno_richiesta,fl_num_richiesta, ref fs_errore) < 0 then
		return -1
	end if
	
	commit;
	
//	s_cs_xx.parametri.parametro_s_10 = "richiesteM"		
//	s_cs_xx.parametri.parametro_d_1  = fl_anno_richiesta
//	s_cs_xx.parametri.parametro_d_2  = fl_num_richiesta
//	s_cs_xx.parametri.parametro_d_3  = ll_anno_registrazione
//	s_cs_xx.parametri.parametro_d_4  = ll_num_registrazione
//	window_open(w_tes_off_ven_det, -1)

	// stefanop_ 01/02/2013; cambio finestra con quella nuova, la vecchia è DEPRECATA
	s_cs_xx.parametri.parametro_d_1  = ll_anno_registrazione
	s_cs_xx.parametri.parametro_d_2  = ll_num_registrazione
	s_cs_xx.parametri.parametro_s_10 = "richiesteM"
	
	window_open(w_tes_off_ven, -1)
	//
	
end if

return 0
end function

public function integer uof_crea_det_off_ven (long fl_anno_off_ven, long fl_num_off_ven, long fl_anno_richiesta, long fl_num_richiesta, ref string fs_errore);// funzione di creazione riga di dettaglio offerta da richiesta
//	
//
//
string ls_cod_tipo_richiesta, ls_cod_tipo_off_ven, ls_cod_iva, &
     ls_cod_tipo_det_ven, ls_des_tipo_det_ven, ls_des_tipo_richiesta, ls_cod_documento, &
	  ls_flag_doc_suc, ls_flag_st_note, ls_des_prodotto
	  
long ll_prog_riga_off_ven, ll_anno_documento, ll_num_documento

dec{4} ld_sconto, ld_quan_default

datetime ldt_data_registrazione, ldt_data_scadenza, ldt_data_reg_richiesta


select cod_tipo_richiesta,
		 data_registrazione,
		 cod_documento,
		 anno_documento,
		 num_documento
into   :ls_cod_tipo_richiesta,
		 :ldt_data_reg_richiesta,
		 :ls_cod_documento,
		 :ll_anno_documento,
		 :ll_num_documento
from   tab_richieste
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_richiesta and
		 num_registrazione = :fl_num_richiesta;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca commessa/divisione nelle richieste~r~n" + sqlca.sqlerrtext
	return -1
end if
		
if isnull(ls_cod_tipo_richiesta) then
	fs_errore = "Nella richiesta non è stata specificata il tipo richiesta!~r~nImpossibile proseguire."
	return -1
end if

select cod_tipo_det_ven,
		 des_tipo_richiesta
into   :ls_cod_tipo_det_ven,
		 :ls_des_tipo_richiesta
from   tab_tipi_richieste
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_richiesta = :ls_cod_tipo_richiesta;
if sqlca.sqlcode = 100 then
	fs_errore = "Nel tipo richiesta non è stata impostato il tipo offerta!~r~nImpossibile proseguire."
	return -1
end if
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca tipo richiesta in tabella tipi_richieste~r~n" + sqlca.sqlerrtext
	return -1
end if
		
ldt_data_registrazione = datetime(today(),00:00:00)
ldt_data_scadenza      = datetime(relativedate(today(), 30),00:00:00)
	
// creo la riga di dettaglio di riferimento
//
// il fatto che la colonna tab_tipi_richieste.cod_tipo_det_ven <> null significa che
// voglio creare la riga automaticamente.

if not isnull(ls_cod_tipo_det_ven) then

	ll_prog_riga_off_ven = 0
	
	select max(prog_riga_off_ven)
	into   :ll_prog_riga_off_ven
	from   det_off_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_off_ven and
			 num_registrazione = :fl_num_off_ven;
	if isnull(ll_prog_riga_off_ven) or ll_prog_riga_off_ven < 1 then
		ll_prog_riga_off_ven = 10
	else
		ll_prog_riga_off_ven = ll_prog_riga_off_ven + 10
	end if

	
	select cod_iva,   
			 flag_doc_suc,   
			 flag_st_note,   
			 quan_default  
	into  :ls_cod_iva,   
			:ls_flag_doc_suc,   
			:ls_flag_st_note,   
			:ld_quan_default  
	from  tab_tipi_det_ven
	where cod_azienda = :s_cs_xx.cod_azienda and 
			cod_tipo_det_ven = :ls_cod_tipo_det_ven; 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in select tab_tipi_det_ven.~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	ls_des_prodotto = "Richiesta. "  + string(ll_anno_documento) + "/" + string(ll_num_documento) + " del " + string(ldt_data_reg_richiesta, "dd/mm/yyyy")
					
	INSERT INTO det_off_ven  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_off_ven,   
			  cod_prodotto,   
			  cod_tipo_det_ven,   
			  cod_misura,   
			  des_prodotto,   
			  quan_offerta,   
			  prezzo_vendita,   
			  fat_conversione_ven,   
			  sconto_1,   
			  sconto_2,   
			  provvigione_1,   
			  provvigione_2,   
			  val_riga,   
			  cod_iva,   
			  data_consegna,   
			  nota_dettaglio,   
			  sconto_3,   
			  sconto_4,   
			  sconto_5,   
			  sconto_6,   
			  sconto_7,   
			  sconto_8,   
			  sconto_9,   
			  sconto_10,   
			  cod_centro_costo,   
			  cod_versione,   
			  num_confezioni,   
			  num_pezzi_confezione,   
			  num_riga_appartenenza,   
			  flag_gen_commessa,   
			  flag_doc_suc_det,   
			  flag_st_note_det,   
			  quantita_um,   
			  prezzo_um,   
			  imponibile_iva,   
			  imponibile_iva_valuta,   
			  settimana_consegna,   
			  flag_stampa_settimana,   
			  anno_reg_richiesta,   
			  num_reg_richiesta )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :fl_anno_off_ven,   
			  :fl_num_off_ven,   
			  :ll_prog_riga_off_ven,   
			  null,   
			  :ls_cod_tipo_det_ven,   
			  null,   
			  :ls_des_prodotto,   
			  :ld_quan_default,   
			  0,   
			  1,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  :ls_cod_iva,   
			  :ldt_data_scadenza,   
			  null,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  null,
			  null,
			  0,   
			  0,   
			  0,   
			  'N',   
			  :ls_flag_doc_suc,   
			  :ls_flag_st_note,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  'N',   
			  :fl_anno_richiesta,   
			  :fl_num_richiesta )  ;
		
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la creazione riga rif in offerta." + sqlca.sqlerrtext
		return -1
	end if
	
	//Donato 19-09-2008
	//modifica per specifica interna: caricamento nel dettaglio dell'offerta anche dei componenti del kit di ricambio ---------
	string ls_return, ls_cod_prodotto_padre, ls_cod_versione_padre, ls_stringa_sep
	string ls_cod_prodotto_figlio, ls_des_prodotto_figlio,ls_cod_misura_figlio, ls_cod_iva_figlio
	decimal ld_quan_utilizzo_figlio
	long ll_pos, ll_len2, ll_index, ll_num_righe
	datastore lds_componenti_kit
	
	open(w_seleziona_kit)
	ls_return = message.stringparm
	ls_stringa_sep = "-stringa-separatrice-"
	
	if ls_return <> "" and not isnull(ls_return) then
		ll_pos = pos(ls_return, ls_stringa_sep)
		ls_cod_prodotto_padre = left(ls_return, ll_pos - 1)
		ll_len2 = len(ls_return) - (len(ls_stringa_sep) + len(ls_cod_prodotto_padre))
		ls_cod_versione_padre = right(ls_return, ll_len2)
		
		lds_componenti_kit = create datastore
		lds_componenti_kit.dataobject = "d_ds_componenti_kit"
		lds_componenti_kit.settransobject(sqlca)
		
		ll_num_righe = lds_componenti_kit.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto_padre, ls_cod_versione_padre)
		for ll_index = 1 to ll_num_righe
			ls_cod_prodotto_figlio = lds_componenti_kit.getitemstring(ll_index, "distinta_cod_prodotto_figlio")
			ls_cod_misura_figlio = lds_componenti_kit.getitemstring(ll_index, "distinta_cod_misura")
			ld_quan_utilizzo_figlio = lds_componenti_kit.getitemdecimal(ll_index, "distinta_quan_utilizzo")
			ls_des_prodotto_figlio = lds_componenti_kit.getitemstring(ll_index, "anag_prodotti_des_prodotto")
			ls_cod_iva_figlio = lds_componenti_kit.getitemstring(ll_index, "anag_prodotti_cod_iva")
			
			if ls_cod_misura_figlio = "" then setnull(ls_cod_misura_figlio)
			if ls_cod_iva_figlio = "" then setnull(ls_cod_iva_figlio)
			
			ll_prog_riga_off_ven = 0
	
			select max(prog_riga_off_ven)
			into   :ll_prog_riga_off_ven
			from   det_off_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :fl_anno_off_ven and
					 num_registrazione = :fl_num_off_ven;
			if isnull(ll_prog_riga_off_ven) or ll_prog_riga_off_ven < 1 then
				ll_prog_riga_off_ven = 10
			else
				ll_prog_riga_off_ven = ll_prog_riga_off_ven + 10
			end if
			
			select cod_tipo_det_ven_ricambi
			into :ls_cod_tipo_det_ven
			from parametri_manutenzioni
			where cod_azienda = :s_cs_xx.cod_azienda
			;
			
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la ricerca del parametro 'cod_tipo_det_ven_ricambi' nella tabella parametri_manutenzioni."+char(13) &								
								+ sqlca.sqlerrtext
				return -1
			end if
			
			if isnull(ls_cod_tipo_det_ven) or ls_cod_tipo_det_ven = "" then
				fs_errore = "Non è stato impostato il parametro 'Tipo Dettaglio Fatturazione Ricambi' nella tabella parametri_manutenzioni."+char(13) &								
								+ sqlca.sqlerrtext
				return -1
			end if
			
			INSERT INTO det_off_ven  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_off_ven,   
				  cod_prodotto,   
				  cod_tipo_det_ven,   
				  cod_misura,   
				  des_prodotto,   
				  quan_offerta,   
				  prezzo_vendita,   
				  fat_conversione_ven,   
				  sconto_1,   
				  sconto_2,   
				  provvigione_1,   
				  provvigione_2,   
				  val_riga,   
				  cod_iva,   
				  data_consegna,   
				  nota_dettaglio,   
				  sconto_3,   
				  sconto_4,   
				  sconto_5,   
				  sconto_6,   
				  sconto_7,   
				  sconto_8,   
				  sconto_9,   
				  sconto_10,   
				  cod_centro_costo,   
				  cod_versione,   
				  num_confezioni,   
				  num_pezzi_confezione,   
				  num_riga_appartenenza,   
				  flag_gen_commessa,   
				  flag_doc_suc_det,   
				  flag_st_note_det,   
				  quantita_um,   
				  prezzo_um,   
				  imponibile_iva,   
				  imponibile_iva_valuta,   
				  settimana_consegna,   
				  flag_stampa_settimana,   
				  anno_reg_richiesta,   
				  num_reg_richiesta )  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :fl_anno_off_ven,   
				  :fl_num_off_ven,   
				  :ll_prog_riga_off_ven,   
				  :ls_cod_prodotto_figlio,   //cod_prodotto_figlio
				  :ls_cod_tipo_det_ven,   
				  :ls_cod_misura_figlio,   //cod_misura
				  :ls_des_prodotto_figlio,  //des_prodotto_figlio 
				  :ld_quan_utilizzo_figlio,   //quan_utilizzo
				  0,   
				  1,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  :ls_cod_iva_figlio,   //cod_iva_flglio
				  :ldt_data_scadenza,   
				  null,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  null,
				  null,
				  0,   
				  0,   
				  0,   
				  'N',   
				  :ls_flag_doc_suc,   
				  :ls_flag_st_note,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  'N',   
				  :fl_anno_richiesta,   
				  :fl_num_richiesta )  ;
		
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la creazione riga ricambio in offerta."+char(13) + &
								"Ricambio '"+ ls_cod_prodotto_figlio + "'"+ char(13) &
								+ sqlca.sqlerrtext
				return -1
			end if				
		next
		destroy lds_componenti_kit
		
		//*************************************************************
		string ls_flag_decimali, ls_cod_misura_mag_figlio, ls_cod_cliente, ls_cod_tipo_listino_prodotto, ls_cod_valuta
		string ls_cod_agente_1, ls_cod_agente_2, ls_cod_valuta_testata, ls_cod_contatto, ls_messaggio
		datastore lds_det_off_ven_lista
		double ld_fat_conversione, ld_quantita, ld_ultimo_prezzo
		datetime ldt_data_reg_testata, ldt_data_esenzione_iva
		uo_condizioni_cliente luo_condizioni_cliente
		
		//recupero info sulla testata
		select
			 cod_cliente
			,cod_contatto
			,cod_tipo_listino_prodotto
			,cod_agente_1
			,cod_agente_2
			,cod_valuta
			,data_registrazione
			,cod_valuta
		into
			 :ls_cod_cliente
			,:ls_cod_contatto
			,:ls_cod_tipo_listino_prodotto
			,:ls_cod_agente_1
			,:ls_cod_agente_2
			,:ls_cod_valuta_testata
			,:ldt_data_reg_testata
			,:ls_cod_valuta
		from tes_off_ven
		where cod_azienda = :s_cs_xx.cod_azienda
				and anno_registrazione = :fl_anno_off_ven  and num_registrazione = :fl_num_off_ven
		;
		
		luo_condizioni_cliente = create uo_condizioni_cliente
		
		lds_det_off_ven_lista = create datastore
		lds_det_off_ven_lista.dataobject = "d_det_off_ven_lista"
		lds_det_off_ven_lista.settransobject(sqlca)
		
		ll_num_righe = lds_det_off_ven_lista.retrieve(s_cs_xx.cod_azienda, fl_anno_off_ven, fl_num_off_ven)
		lds_det_off_ven_lista.setfilter("cod_prodotto <> ''")
		lds_det_off_ven_lista.filter()
		
		ll_num_righe = lds_det_off_ven_lista.rowcount()
		for ll_index = 1 to ll_num_righe
			ls_cod_prodotto_figlio = lds_det_off_ven_lista.getitemstring(ll_index, "cod_prodotto")
			
			select 
				cod_misura_mag,
				cod_misura_ven,
				 fat_conversione_ven,
				 flag_decimali,
				 cod_iva,
				des_prodotto
			into
				:ls_cod_misura_mag_figlio,
				:ls_cod_misura_figlio,
				:ld_fat_conversione,
				:ls_flag_decimali,
				:ls_cod_iva_figlio,
				:ls_des_prodotto_figlio
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_figlio;
						 
			 if ld_fat_conversione <> 0 then
				lds_det_off_ven_lista.setitem(ll_index, "fat_conversione_ven", ld_fat_conversione)
			else
				ld_fat_conversione = 1
				lds_det_off_ven_lista.setitem(ll_index, "fat_conversione_ven", 1)
			end if
			
			if not isnull(ls_cod_iva_figlio) then lds_det_off_ven_lista.setitem(ll_index, "cod_iva", ls_cod_iva_figlio)
			
			//f_situazione_prodotto(i_coltext, uo_1)
			
			f_cerca_ultimo_prezzo_vendita_fatture (ls_cod_cliente, ls_cod_prodotto_figlio, ld_ultimo_prezzo, ls_messaggio )
			
			if not isnull(ls_cod_cliente) then
				select anag_clienti.cod_iva,
						 anag_clienti.data_esenzione_iva
				into   :ls_cod_iva,
						 :ldt_data_esenzione_iva
				from   anag_clienti
				where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_clienti.cod_cliente = :ls_cod_cliente;
			else
				select anag_contatti.cod_iva,
						 anag_contatti.data_esenzione_iva
				into   :ls_cod_iva,
						 :ldt_data_esenzione_iva
				from   anag_contatti
				where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_contatti.cod_contatto = :ls_cod_contatto;
			end if
			
			if sqlca.sqlcode = 0 then
				if ls_cod_iva <> "" and (ldt_data_esenzione_iva <= ldt_data_reg_testata or isnull(ldt_data_esenzione_iva)) then
					lds_det_off_ven_lista.setitem(ll_index, "cod_iva", ls_cod_iva)
				end if
			end if
			
			ld_quantita = lds_det_off_ven_lista.getitemnumber(ll_index, "quan_offerta")
			if ls_flag_decimali = "N" and &
				ld_quantita - int(ld_quantita) > 0 then
				ld_quantita = ceiling(ld_quantita)
				lds_det_off_ven_lista.setitem(ll_index, "quan_offerta", ld_quantita)
			end if
			if mid(f_flag_controllo(),1,1) = "S" then
				
				lds_det_off_ven_lista.setrow(ll_index)
				//luo_condizioni_cliente.str_parametri.ldw_oggetto = lds_det_off_ven_lista
				luo_condizioni_cliente.str_parametri.lds_oggetto = lds_det_off_ven_lista
				luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				if mid(f_flag_controllo(),7,1) = "S" then
					luo_condizioni_cliente.str_parametri.data_riferimento = lds_det_off_ven_lista.getitemdatetime(ll_index,"data_consegna")
				else
					luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto_figlio
				luo_condizioni_cliente.str_parametri.dim_1 = 0
				luo_condizioni_cliente.str_parametri.dim_2 = 0
				luo_condizioni_cliente.str_parametri.quantita = ld_quantita
				luo_condizioni_cliente.str_parametri.valore = 0
				luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				luo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				luo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				luo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				luo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				luo_condizioni_cliente.wf_condizioni_cliente_2()				
			end if
		next
		
		ll_index = lds_det_off_ven_lista.update()
		
		//*************************************************************
		destroy luo_condizioni_cliente
	end if
	//fine modifica ---------------------------------------------------------------------------------------------------------

end if	

commit;
		
return 0
end function

public function integer uof_new_modify (ref datawindow fdw_datawindow, boolean fb_newmodify);string ls_enabled

if fb_newmodify then
	ls_enabled = "No"
else
	ls_enabled = "Yes"
end if

choose case fdw_datawindow.dataobject
		//-CLAUDIA 14/03/07 GESTITO IL PULSANTE b_ric_divisioni
	case 'd_call_center_1'
		fdw_datawindow.object.b_ric_divisioni.enabled = fb_newmodify
		
	// -- stefanop 02/03/2009
	case 'd_call_center_3'
		fdw_datawindow.object.b_ric_divisioni.enabled = fb_newmodify
		fdw_datawindow.object.b_ric_aree.enabled = fb_newmodify
	// -----------------
	
	// -- Donato 22/05/2009
	case 'd_call_center_4'
		fdw_datawindow.object.b_ric_divisioni.enabled = fb_newmodify
		fdw_datawindow.object.b_ric_aree.enabled = fb_newmodify
		fdw_datawindow.object.b_ric_attrezzature.enabled = fb_newmodify
	// -----------------
	
	// -- Donato 06/10/2009
	case 'd_call_center_5', "d_call_center_12"
		fdw_datawindow.object.b_ric_divisioni.enabled = fb_newmodify
		fdw_datawindow.object.b_ric_aree.enabled = fb_newmodify
		//fdw_datawindow.object.b_mappa.enabled = fb_newmodify
	// -----------------
	
	// -- Donato 27/01/2010
	case 'd_call_center_6'
		fdw_datawindow.object.b_ric_attrezzature.enabled = fb_newmodify
		fdw_datawindow.object.b_ric_cliente.enabled = fb_newmodify
		fdw_datawindow.object.b_ric_prodotto.enabled = fb_newmodify
		fdw_datawindow.object.b_ric_divisioni.enabled = fb_newmodify
		
		fdw_datawindow.object.b_mail.enabled = not fb_newmodify
		fdw_datawindow.object.b_add_richiedente.enabled = fb_newmodify
	// -----------------
	
	case 'd_call_center_manutenz_semplice'
		fdw_datawindow.object.b_ric_attrezzature.enabled = fb_newmodify
		
	case 'd_call_center_2','d_call_center_manutenz_semplice_chiusura'
		fdw_datawindow.object.b_genera_int.enabled = not(fb_newmodify)
		fdw_datawindow.object.b_crea_off_ven.enabled = not(fb_newmodify)
		fdw_datawindow.object.b_off_ven.enabled = not(fb_newmodify)
		fdw_datawindow.object.b_manut_eseguite.enabled = not(fb_newmodify)
		
	case 'd_call_center_visite_ispettive'
		fdw_datawindow.object.b_genera_visita_isp.enabled = not(fb_newmodify)
		
	case 'd_call_center_reclami'
		fdw_datawindow.object.b_ric_cliente.enabled = fb_newmodify
		fdw_datawindow.object.b_ric_prodotto.enabled = fb_newmodify
		fdw_datawindow.object.b_ric_stock.enabled = fb_newmodify
		
	// stefano 05/05/2010
	case "d_call_center_8"
		fdw_datawindow.object.b_fornitori.enabled = not fb_newmodify
	// ----
	
	// Giulio: 15/02/2012
	case "d_call_center_11"
		fdw_datawindow.object.b_ric_attrezzature.enabled = fb_newmodify
		fdw_datawindow.object.b_add_richiedente.enabled = fb_newmodify
		fdw_datawindow.object.b_note_strumentali.enabled = not fb_newmodify
	
		if guo_functions.uof_is_supervisore(s_cs_xx.cod_utente) then
			fdw_datawindow.SetTabOrder("risposta", 110)
		else
			fdw_datawindow.SetTabOrder("risposta", 0)
		end if
		
end choose

return 0
end function

public function integer uof_crea_nc_reclami (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_errore);string   ls_cod_resp

long     ll_row, ll_anno_nc, ll_num_nc


select cod_resp_divisione
into   :ls_cod_resp
from   mansionari
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_apertura_nc = 'S';
		 
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_cod_resp) then
	fs_errore = "Il mansionario corrente non è abilitato all'apertura delle non conformità"
	return -1
end if

ll_anno_nc = year(today())

select max(num_non_conf)
into   :ll_num_nc
from   non_conformita
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_non_conf = :ll_anno_nc;
		 
if sqlca.sqlcode < 0 then
	messagebox("OMNIA","Errore nella select di non_conformita: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_num_nc) then
	ll_num_nc = 0
end if

ll_num_nc++

insert
into   non_conformita
		 (cod_azienda,
		 anno_non_conf,
		 num_non_conf,
		 flag_tipo_nc)
values (:s_cs_xx.cod_azienda,
		 :ll_anno_nc,
		 :ll_num_nc,
		 'V');
		 
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore nella insert di non_conformita: " + sqlca.sqlerrtext
	return -1
end if

update tab_richieste
set    anno_non_conf = :ll_anno_nc,
		 num_non_conf = :ll_num_nc
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_reg_richiesta = :fl_anno_registrazione and
		 num_reg_richiesta = :fl_num_registrazione;
		 
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore nella update di richieste: " + sqlca.sqlerrtext
	rollback;
	return -1
end if

//commit;
//		 
//ll_row = dw_richieste_lista.getrow()
//
//dw_richieste_lista.triggerevent("pcd_retrieve")
//
//dw_richieste_lista.setrow(ll_row)
//
//dw_richieste_lista.scrolltorow(ll_row)
//	
//s_cs_xx.parametri.parametro_d_1 = ll_anno_nc
//
//s_cs_xx.parametri.parametro_d_2 = ll_num_nc
//
//window_open(w_non_conformita_std_ven,-1)

return 0
end function

public function integer uof_crea_intervento (ref uo_cs_xx_dw fdw_datawindow, ref string fs_errore);dec{4}   ld_quan_stock

datetime ldt_data_stock, ldt_reso

long     ll_row, ll_anno_int, ll_num_int, ll_prog_stock, ll_anno_nc, ll_num_nc, ll_anno_registrazione, ll_num_registrazione

string   ls_cod_cliente, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, &
			ls_cod_difetto, ls_flag, ls_resp_reso, ls_risposta


if fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_gen_manutenzione") <> "N" then
	fs_errore = "La richiesta è già stata evasa con un intervento."
	return -1
end if

ll_anno_registrazione = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")

ll_num_registrazione = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")

ls_cod_cliente = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_cliente")
	
ls_cod_prodotto = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_prodotto")
	
ls_cod_deposito = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_deposito")
	
ls_cod_ubicazione = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_ubicazione")
	
ls_cod_lotto = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_lotto")
	
ls_cod_difetto = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_difformita")
	
ldt_data_stock = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"data_stock")
	
ll_prog_stock = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"prog_stock")
	
ll_anno_nc = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_non_conf")
	
ll_num_nc = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_non_conf")
	
ls_flag = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_richiesta_reso")
	
select cod_resp_divisione
into   :ls_resp_reso
from   mansionari
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_autorizza_reso = 'S';
			 
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in ricerca mansionario dalla tabella mansionari: " + sqlca.sqlerrtext
	setnull(ls_resp_reso)
	setnull(ldt_reso)
elseif sqlca.sqlcode = 100 or isnull(ls_resp_reso) then
	fs_errore = "Il mansionario corrente non è abilitato ad autorizzare un reso"
	setnull(ls_resp_reso)
	setnull(ldt_reso)
else

	ldt_reso = datetime(today(),00:00:00)
	
	select quan_assegnata
	into   :ld_quan_stock
	from   stock
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto and
			 cod_deposito = :ls_cod_deposito and
			 cod_ubicazione = :ls_cod_ubicazione and
			 cod_lotto = :ls_cod_lotto and
			 data_stock = :ldt_data_stock and
			 prog_stock = :ll_prog_stock;
			 
	if sqlca.sqlcode < 0 then
		messagebox("OMNIA","Errore in lettura quantità stock da tabella stock: " + sqlca.sqlerrtext)
		setnull(ld_quan_stock)
	elseif sqlca.sqlcode = 100 then
		setnull(ld_quan_stock)
	end if
	
end if
	
ll_anno_int = f_anno_esercizio()
	
select max(num_reg_intervento)
into   :ll_num_int
from   schede_intervento
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_reg_intervento = :ll_anno_int;
			 
if sqlca.sqlcode < 0 then
	fs_errore = "Errore nella select di schede_intervento: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_num_int) then
	ll_num_int = 0
end if
	
ll_num_int++
	
insert
into   schede_intervento
		 (cod_azienda,
		 anno_reg_intervento,
		 num_reg_intervento,
		 anno_reg_richiesta,
		 num_reg_richiesta,
		 cod_cliente,
		 cod_prodotto,
		 cod_deposito,
		 cod_ubicazione,
		 cod_lotto,
		 cod_errore,
		 data_stock,
		 prog_stock,
		 anno_registrazione,
		 num_registrazione,
		 prog_riga_fat_ven,
		 anno_non_conf,
		 num_non_conf,
		 flag_reso,
		 anno_reg_campionamenti,
		 num_reg_campionamenti,
		 cod_resp_divisione,
		 data_reso,
		 quan_stock)
values (:s_cs_xx.cod_azienda,
		 :ll_anno_int,
		 :ll_num_int,
		 :ll_anno_registrazione,
		 :ll_num_registrazione,
		 :ls_cod_cliente,
		 :ls_cod_prodotto,
		 :ls_cod_deposito,
		 :ls_cod_ubicazione,
		 :ls_cod_lotto,
		 :ls_cod_difetto,
		 :ldt_data_stock,
		 :ll_prog_stock,
		 null,
		 null,
		 null,
		 :ll_anno_nc,
		 :ll_num_nc,
		 :ls_flag,
		 null,
		 null,
		 :ls_resp_reso,
		 :ldt_reso,
		 :ld_quan_stock);
			 
if sqlca.sqlcode <> 0 then
	messagebox("OMNIA","Errore nella insert di schede_intervento: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

ls_risposta = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"risposta")

if isnull(ls_risposta) or len(ls_risposta) < 1 then
	ls_risposta = "Generato Intervento " + string(ll_anno_int) + "/" + string(ll_num_int)
else
	ls_risposta = "~r~nGenerato Intervento " + string(ll_anno_int) + "/" + string(ll_num_int)
end if

update tab_richieste
set    flag_gen_manutenzione = 'S',
       risposta = :ls_risposta
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione = :ll_num_registrazione;
		 
if sqlca.sqlcode <> 0 then
	messagebox("OMNIA","Errore nella update di richieste: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if
	

return 0
end function

public function integer uof_visualizza_mappa (double fd_latitudine, double fd_longitudine);string ls_url, ls_lat, ls_long
inet iinet_base

ls_url = "http://maps.google.it/maps?f=q&source=s_q&hl=it&q="


ls_lat = string(fd_latitudine)
uf_rimpiazza(ls_lat)

ls_long = string(fd_longitudine)
uf_rimpiazza(ls_long)

ls_url += ls_lat + "," + ls_long

/*
//1° metodo run internet explorer
ls_run = "C:\Programmi\Internet Explorer\iexplore.exe "+ ls_url
run(ls_run)
//---------------------------------------------------
*/


//2° metodo Get Context Service "Internet"
GetContextService("Internet", iinet_base)
iinet_base.HyperlinkToUrl(ls_url)
//---------------------------------------------------

/*
//3° metodo run firefox
ls_run = "C:\Programmi\Mozilla Firefox\firefox.exe "+ ls_url
run(ls_run)
//---------------------------------------------------
*/

/*
//4° metodo apri window con oggetto webbrowser

////N.B. va creata la w_gps_web_browser con un oggetto 
////fare insert->control-> OLE
////poi clic sul tab Insert Control e selezionare Microsoft Web Browser
//
////codice: 
////ls_url = "http://maps.google.it/maps?f=q&source=s_q&hl=it&q="+ls_lat+","+ls_long
////ole_webcontrol.object.Navigate( sle_url.text )
////ole_webcontrol.object.Navigate( ls_url )

//s_cs_xx.parametri.parametro_s_1 = ls_url
//open(w_gps_webbrowser)
//---------------------------------------------------------------
*/

return 1
end function

public subroutine uf_rimpiazza (ref string as_mystring);long start_pos=1

//rimpiazza la virgola con il punto --------PREZZO------
if not isnull(as_mystring) and as_mystring<>"" then
	// Find the first occurrence of old_str.
	start_pos = Pos(as_mystring, ",", start_pos)
	
	// Only enter the loop if you find old_str.
	DO WHILE start_pos > 0
		 // Replace old_str with new_str.
		 as_mystring = Replace(as_mystring, start_pos, Len(","), ".")
		 // Find the next occurrence of old_str.
		 start_pos = Pos(as_mystring, ",", start_pos+Len("."))
	LOOP	
else
	as_mystring="0"
end if
end subroutine

public function integer uof_invia_mail (long fl_row, datawindow fd_dw);uo_outlook luo_outlook
string ls_destinatari[], ls_allegati[], ls_msg, ls_tecnico, ls_email, ls_des_richiesta, ls_sql, ls_des_fase, ls_note_fase
string ls_oggetto, ls_messaggio, ls_nome_tecnico, ls_cod_classe_richiesta, ls_des_classe_richiesta, ls_cod_divisione
string ls_ptenda_emails[], ls_email_richiedente, ls_flag_richiesta_chiusa
integer li_ret, li_i, li_j
long ll_row, ll_cod_richiedente, ll_anno_reg, ll_num_reg, ll_i_mail, ll_totfasi, ll_index
datastore lds_data

string ls_br = "<br />"
//###################################
//gestione nostra interna segnalazioni e richieste clienti
luo_outlook = create uo_outlook

ll_i_mail = 0
ll_row = fl_row
if ll_row > 0 then
else
	return 1
end if

ll_anno_reg = fd_dw.getitemnumber(ll_row,"anno_registrazione")
ll_num_reg = fd_dw.getitemnumber(ll_row,"num_registrazione")

//Donato 16/09/2010
//Se esistono risposte inserirle nella mail e cambiare l'oggetto in Ticket n°.....

ls_sql = "select a.prog_fase, a.des_fase, a.note "+&
			"from manutenzioni_fasi as a "+&
			"join manutenzioni as b on b.cod_azienda=a.cod_azienda and "+&
									"b.anno_registrazione=a.anno_registrazione and b.num_registrazione=a.num_registrazione "+&
			"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
				"b.anno_reg_richiesta="+string(ll_anno_reg)+" and "+&
				"b.num_reg_richiesta="+string(ll_num_reg)+" "+&
			"order by a.anno_registrazione desc, a.num_registrazione desc, a.prog_fase desc"
			
if not f_crea_datastore(lds_data, ls_sql) then
	g_mb.messagebox("","Errore in creazione datastore delle fasi/risposte. Non sarà inserita la risposta nel testo mail.",StopSign!)
	ll_totfasi = 0
else
	ll_totfasi = lds_data.retrieve()
end if

//Donato 09/11/2010
select flag_richiesta_chiusa
into :ls_flag_richiesta_chiusa
from tab_richieste
where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_reg and
			 num_registrazione = :ll_num_reg;

if isnull(ls_flag_richiesta_chiusa) then ls_flag_richiesta_chiusa="N"

if ll_totfasi > 0 then
	
	if ls_flag_richiesta_chiusa="S" then
		ls_oggetto = "Chiusura Ticket n°"+&
					string(ll_anno_reg) + "/" + string(ll_num_reg)
		ls_messaggio = "Gentile Cliente,"+ls_br + &
								"Le comunichiamo che il ticket relativo alla Sua segnalazione è stato gestito e chiuso dai nostri tecnici."+ls_br+&
								"Di seguito la/le risposte relativa/e"
	else
		ls_oggetto = "Gestione Ticket n°"+&
					string(ll_anno_reg) + "/" + string(ll_num_reg)
		ls_messaggio = "Gentile Cliente,"+ls_br + &
								"Le comunichiamo il ticket relativo alla Sua segnalazione è in gestione da parte nostri tecnici."+ls_br+&
								"Di seguito la/le risposte relativa/e disponibile/i al momento "
	end if
else
	ls_oggetto = "Apertura Ticket n°"+&
				string(ll_anno_reg) + "/" + string(ll_num_reg)
	ls_messaggio = "Gentile Cliente,"+ls_br+ &
							"Le comunichiamo che é stato aperto un ticket relativo alla Sua segnalazione."
end if

for ll_index=1 to ll_totfasi
	ls_des_fase = lds_data.getitemstring(ll_index, 2)
	ls_note_fase = lds_data.getitemstring(ll_index, 3)
	
	if not isnull(ls_des_fase) and ls_des_fase<>"" and not isnull(ls_note_fase) and ls_note_fase<>"" then
		ls_messaggio += ls_br+ls_br+upper(ls_des_fase)+ls_br
		ls_messaggio += ls_note_fase
	end if
next
//---------------------------


ls_des_richiesta = fd_dw.getitemstring(ll_row, "des_richiesta")	
ll_cod_richiedente = fd_dw.getitemnumber(ll_row, "cod_richiedente")
ls_cod_classe_richiesta = fd_dw.getitemstring(ll_row,"cod_classe_richiesta")

//ls_oggetto = "Apertura Ticket n°"+&
//				string(ll_anno_reg) + "/" + string(ll_num_reg)
//ls_messaggio = "Gentile Cliente,"+char(13) + &
//							"Le comunichiamo che è stato aperto un ticket relativo alla Sua segnalazione."

if ls_cod_classe_richiesta<> "" and not isnull(ls_cod_classe_richiesta) then
	select des_classe_richiesta
	into :ls_des_classe_richiesta
	from tab_richieste_classificazioni
	where cod_azienda = :s_cs_xx.cod_azienda and
		cod_classe_richiesta = :ls_cod_classe_richiesta;
		
	if isnull(ls_des_classe_richiesta) or ls_des_classe_richiesta = "" then		
		ls_des_classe_richiesta = ls_cod_classe_richiesta
	end if
	
	ls_messaggio +=ls_br+ls_br+"Classificazione Segnalazione: "+ls_des_classe_richiesta
end if

if not isnull(ls_des_richiesta) and ls_des_richiesta<> "" then ls_messaggio += ls_br+ls_br+"Testo segnalazione:"+ls_br + "'<em>" + ls_des_richiesta+"</em>'"

if isnull(ll_cod_richiedente) then
	//ls_destinatari[1] = ""
else
	select email_richiedente
	into :ls_email
	from tab_richiedenti
	where cod_azienda = :s_cs_xx.cod_azienda and cod_richiedente=:ll_cod_richiedente;
	
	ll_i_mail += 1
	ls_destinatari[ll_i_mail] = ls_email
	ls_email_richiedente = ls_email
	ls_email = ""
end if

ls_tecnico = fd_dw.getitemstring(ll_row,"cod_operaio_incaricato")

if ls_tecnico <> "" and not isnull(ls_tecnico) then
	select utenti.e_mail, 
			anag_operai.nome + ' ' + anag_operai.cognome
	into :ls_email, :ls_nome_tecnico
	from utenti
	join anag_operai on anag_operai.cod_utente=utenti.cod_utente
	where anag_operai.cod_azienda = :s_cs_xx.cod_azienda and anag_operai.cod_operaio =:ls_tecnico;
	
	if isnull(ls_email) or ls_email = "" then
		//ls_destinatari[2] = ls_email
	else
		ll_i_mail += 1
		ls_destinatari[ll_i_mail] = ls_email
		
		if not isnull(ls_nome_tecnico) and ls_nome_tecnico<> "" then ls_messaggio +=ls_br+ls_br+"Il tecnico di riferimento è "+ ls_nome_tecnico
	end if
else
	//ls_destinatari[2] = ""	
end if	

// stefanop 11/10/2010: controllo divisione per richiesta di enrico in accordo con beatrice: ticket 2010/282
ls_cod_divisione = fd_dw.getitemstring(ll_row,"cod_divisione")
if ls_cod_divisione = "010033" or ls_cod_divisione = "019004" or ls_cod_divisione= "187190" then
	
	// destinatari da avvisare
	ls_ptenda_emails[] =   {"bortolotto.beatrice@progettotenda.com", "riello.daniele@progettotenda.com", "maero@viropasrl.it", "valerio.sandona@centrogibus.com"}
	
	for li_i = 1 to upperbound(ls_ptenda_emails)
		// destinatario già inserito
		if ls_ptenda_emails[li_i] = ls_email_richiedente then continue
		
		ll_i_mail += 1
		ls_destinatari[ll_i_mail] = ls_ptenda_emails[li_i]
	next 
end if
// ----

//invio solo al richiedente
//li_ret = luo_outlook.uof_invio_outlook_redemption(0,"M", ls_oggetto, ls_messaggio, ls_destinatari[], ls_allegati[], true, ls_msg)
//li_ret = luo_outlook.uof_invio_outlook(0,"M", ls_oggetto, ls_messaggio, ls_destinatari[], ls_allegati[], true, ls_msg)
li_ret = luo_outlook.uof_outlook(0,"M", ls_oggetto, ls_messaggio, ls_destinatari[], ls_allegati[], true, ls_msg)

//luo_outlook.uof_invio_sendmail(ls_destinatari, ls_oggetto, ls_messaggio, ls_allegati, ref ls_msg)
																	
destroy luo_outlook

return 1
end function

public function integer uof_genera_int (uo_cs_xx_dw fdw_datawindow);string			ls_null
integer			li_risposta
long				ll_anno_reg_richiesta, ll_num_reg_richiesta, ll_cont, ll_row
datetime		ldt_data_sospensione

setnull(ls_null)

if fdw_datawindow.getitemstring(fdw_datawindow.getrow(), "flag_richiesta_chiusa") = "S" then
	messagebox("OMNIA", "Attenzione: richiesta risolta, impossibile generare interventi!")
	return 0
end if

if fdw_datawindow.getitemstring(fdw_datawindow.getrow(), "flag_richiesta_obsoleta") = "S" then
	messagebox("OMNIA", "Attenzione: richiesta obsoleta, impossibile generare interventi!")
	return 0
end if

ll_anno_reg_richiesta = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")
ll_num_reg_richiesta  = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")


//se ci sono sospensioni attive, avvisa ma prosegui lo stesso, se l'utente desidera
if uof_sospensioni(	ll_anno_reg_richiesta, ll_num_reg_richiesta, ldt_data_sospensione) then	
	if not g_mb.confirm("Attenzione: per la richiesta risulta una sospensione attiva dal "+string(ldt_data_sospensione, "dd/mm/yyyy")+"! Continuare lo stesso?") then
		return 0
	end if
end if
//--------------------------------


select count(*)
into   :ll_cont
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_reg_richiesta = :ll_anno_reg_richiesta and
		 num_reg_richiesta = :ll_num_reg_richiesta;
		 
if not isnull(ll_cont) and ll_cont > 0 then
	li_risposta = MessageBox("OMNIA", "Esistono già attività di manutenzione su questa richiesta.~r~nCreare una nuova attività di manutenzione?",Exclamation!, YesNo!, 2)
else
	li_risposta = 1
end if
		
if li_risposta = 1 THEN
	s_cs_xx.parametri.parametro_s_1 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_gen_manutenzione")
	s_cs_xx.parametri.parametro_s_2 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_attrezzatura") // imposto l'apparecchiatura selezionata
	s_cs_xx.parametri.parametro_s_3 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_operaio_incaricato")
	if isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"des_richiesta")) then
		s_cs_xx.parametri.parametro_s_4 = ""
	else
		s_cs_xx.parametri.parametro_s_4 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"des_richiesta")
	end if
	if isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"risposta")) then
		s_cs_xx.parametri.parametro_s_5 = ""
	else
		s_cs_xx.parametri.parametro_s_5 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"risposta")
	end if
	if isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"messaggio_errore")) then
		s_cs_xx.parametri.parametro_s_6 = ""
	else
		s_cs_xx.parametri.parametro_s_6 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"messaggio_errore")
	end if
	s_cs_xx.parametri.parametro_s_7 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"flag_richiesta_straordinaria")
	s_cs_xx.parametri.parametro_s_8 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_attrezzatura")
	s_cs_xx.parametri.parametro_d_1 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"anno_registrazione")
	s_cs_xx.parametri.parametro_d_2 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"num_registrazione")
	setnull(s_cs_xx.parametri.parametro_d_3)
	setnull(s_cs_xx.parametri.parametro_d_4)
	s_cs_xx.parametri.parametro_data_1 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"data_inizio_attivita")
	s_cs_xx.parametri.parametro_data_2 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"ora_inizio_attivita")
	s_cs_xx.parametri.parametro_data_3 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"data_fine_attivita")
	s_cs_xx.parametri.parametro_data_4 = fdw_datawindow.getitemdatetime(fdw_datawindow.getrow(),"ora_fine_attivita")
	
	s_cs_xx.parametri.parametro_s_10 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_cat_risorse_esterne")
	s_cs_xx.parametri.parametro_s_11 = fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_risorsa_esterna")
	
	ll_row = fdw_datawindow.getrow()
	
	window_open(w_call_center_conferma_manut, 0)	
	
	//##################################################
	//##################################################
	if fdw_datawindow.dataobject = "d_call_center_7" then
		//chiedere se si vuole inviare una mail
		if g_mb.confirm("OMNIA", "Inviare la segnalazione di chiusura richiesta al cliente?") then
				uof_invia_mail(ll_row, fdw_datawindow)
		end if
	end if
	//##################################################
	//##################################################
	
	fdw_datawindow.change_dw_current()
	fdw_datawindow.postevent("ue_esegui_retrieve")
	
end if

return 1
end function

private function integer uof_note_strumentali (ref uo_cs_xx_dw fdw_datawindow);/* *
 * stefanop
 * 10/04/2012
 *
 * Specifica: Ordini_Lavoro_Richieste rev.01 - Alpiq
 *
 * La funzione chiede all'utente di inserire un testo che verrà inserito nelle note strumentali
 * Nella nota verrà aggiunto l'operatore che ha scritto, la data e l'ora dell'operazione.
 *
 * Solo l'operatore amministratore potrà modificare le note strumentali
 */


string ls_testo, ls_cod_operaio, ls_des_operaio, ls_risposta
long ll_anno, ll_num
datetime ldt_today

ll_anno = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(), "anno_registrazione")
if isnull(ll_anno) or ll_anno < 1901 then return 0

if s_cs_xx.cod_utente = "CS_SYSTEM" then
	ls_cod_operaio = "CS_SYSTEM"
else
	ls_cod_operaio = guo_functions.uof_get_operaio_utente()
	
	if isnull(ls_cod_operaio) or ls_cod_operaio = "" then
		g_mb.warning("Attenzione: L'utente collegato non è associato a nessun operaio!")
		return -1
	end if
	
	ls_des_operaio = f_des_tabella("anag_operai", "cod_operaio='" + ls_cod_operaio + "'", "nome + ' ' + cognome")
	if isnull(ls_des_operaio) then  ls_des_operaio = ""
end if

ldt_today = datetime(today(), now())

g_mb.prompt(ls_testo, "Inserimento Note Strumentali", 255)
if isnull(ls_testo) or ls_testo = "" then return 0

ls_testo = string(ldt_today, "dd/mm/yyyy hh:mm:ss") + " - " + ls_des_operaio + " - " + ls_testo

ls_risposta = fdw_datawindow.getitemstring(fdw_datawindow.getrow(), "risposta")

if isnull(ls_risposta) or ls_risposta = "" then
	ls_risposta = ""
else
	ls_risposta += "~r~n"
end if

ls_risposta += ls_testo

fdw_datawindow.setitem(fdw_datawindow.getrow(), "risposta", ls_risposta)
//fdw_datawindow.getparent().triggerevent("pc_save")
fdw_datawindow.triggerevent("pcd_save")

return  1
end function

public function boolean uof_sospensioni (integer ai_anno_reg, long al_num_reg, ref datetime adt_data_sospensione);//controlla se per la richiesta esistono sospensioni attive
//se si restituisce la data della sospensione attiva + recente
//per sospensione attiva si intende una sospensione con data riattivazione nulla


select max(data_sospensione)
into :adt_data_sospensione
from tab_richieste_sospensioni
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_reg and
			num_registrazione=:al_num_reg and
			data_riattivazione is null;

if sqlca.sqlcode=0 and not isnull(adt_data_sospensione) and year(date(adt_data_sospensione))>1950 then
	return true
end if


return false
end function

on uo_tab_richieste.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_tab_richieste.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

