﻿$PBExportHeader$w_aree_chiamate.srw
forward
global type w_aree_chiamate from w_cs_xx_principale
end type
type dw_aree_chiamate from uo_cs_xx_dw within w_aree_chiamate
end type
end forward

global type w_aree_chiamate from w_cs_xx_principale
integer width = 2939
integer height = 1428
string title = "Aree Chiamate"
dw_aree_chiamate dw_aree_chiamate
end type
global w_aree_chiamate w_aree_chiamate

on w_aree_chiamate.create
int iCurrent
call super::create
this.dw_aree_chiamate=create dw_aree_chiamate
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_aree_chiamate
end on

on w_aree_chiamate.destroy
call super::destroy
destroy(this.dw_aree_chiamate)
end on

event pc_setwindow;call super::pc_setwindow;dw_aree_chiamate.set_dw_key("cod_azienda")
dw_aree_chiamate.set_dw_options(sqlca, &
										 pcca.null_object, &
										 c_default, &
										 c_default)

iuo_dw_main = dw_aree_chiamate
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_aree_chiamate, &
				  "cod_divisione", &
					sqlca, &
				  "anag_divisioni", &
				  "cod_divisione", &
				  "des_divisione", &
				  "cod_azienda = '"+s_cs_xx.cod_azienda+"'")
end event

type dw_aree_chiamate from uo_cs_xx_dw within w_aree_chiamate
integer x = 9
integer y = 8
integer width = 2880
integer height = 1300
integer taborder = 10
string dataobject = "d_aree_chiamate"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
next
end event

event doubleclicked;call super::doubleclicked;if row < 1 then return

if s_cs_xx.admin then

	str_des_multilingua lstr_des_multilingua
	
	lstr_des_multilingua.nome_tabella = "tab_aree_chiamate"
	lstr_des_multilingua.descrizione_origine = getitemstring(row,"des_area_chiamata")
	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_area_chiamata")
	setnull(lstr_des_multilingua.chiave_str_2)
	
	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
	
	openwithparm(w_des_multilingua, lstr_des_multilingua)

end if
end event

