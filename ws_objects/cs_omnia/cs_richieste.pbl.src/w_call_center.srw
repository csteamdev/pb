﻿$PBExportHeader$w_call_center.srw
$PBExportComments$Finestra Gestione Richieste
forward
global type w_call_center from w_cs_xx_principale
end type
type dw_richieste_blob from uo_dw_drag within w_call_center
end type
type cb_supplementi from commandbutton within w_call_center
end type
type dw_4 from uo_cs_xx_dw within w_call_center
end type
type dw_3 from uo_cs_xx_dw within w_call_center
end type
type dw_2 from uo_cs_xx_dw within w_call_center
end type
type dw_5 from uo_cs_xx_dw within w_call_center
end type
type dw_1 from uo_cs_xx_dw within w_call_center
end type
type dw_folder from u_folder within w_call_center
end type
type cb_blob from commandbutton within w_call_center
end type
type cb_genera_nc from commandbutton within w_call_center
end type
type cb_reset from commandbutton within w_call_center
end type
type cb_ricerca from commandbutton within w_call_center
end type
type dw_call_center_lista from uo_cs_xx_dw within w_call_center
end type
type dw_folder_search from u_folder within w_call_center
end type
type dw_ricerca from u_dw_search within w_call_center
end type
type cb_ol_lav_richiesta from commandbutton within w_call_center
end type
end forward

global type w_call_center from w_cs_xx_principale
integer width = 3694
integer height = 2508
string title = "Richieste"
event ue_barcode_focus ( )
event ue_chiudi ( )
dw_richieste_blob dw_richieste_blob
cb_supplementi cb_supplementi
dw_4 dw_4
dw_3 dw_3
dw_2 dw_2
dw_5 dw_5
dw_1 dw_1
dw_folder dw_folder
cb_blob cb_blob
cb_genera_nc cb_genera_nc
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_call_center_lista dw_call_center_lista
dw_folder_search dw_folder_search
dw_ricerca dw_ricerca
cb_ol_lav_richiesta cb_ol_lav_richiesta
end type
global w_call_center w_call_center

type variables
//boolean			ib_ricerca=true
boolean				ib_global_service=false, ib_proviene_da_nc=false, ib_send_mail=false, ib_dw_blob_visible = false
long    				il_row = 0, il_anno_nc=0, il_num_nc=0
uo_tab_richieste 	iuo_richieste
string 				is_NMS = "N", is_sql_base
string					is_order_by = "order by anno_documento desc, cod_documento desc, numeratore_documento asc, num_documento desc"
transaction			itran_note
end variables

forward prototypes
public function integer wf_imposta_folder ()
public function integer wf_carica_elenco_manut (ref uo_cs_xx_dw fdw_datawindow, long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_blocca_folder (string fs_cod_tipo_richiesta)
public function integer wf_invia_mail (string as_oggetto, string as_messaggio)
public function boolean wf_get_barcode (string as_barcode, ref integer ai_anno_registrazione, ref long al_num_registrazione, ref string as_errore)
public subroutine wf_retrieve_blob (long al_row)
end prototypes

event ue_barcode_focus();// imposto fuoco sul campo barcode
dw_ricerca.setfocus()
dw_ricerca.setcolumn("barcode")
end event

event ue_chiudi();// *** ENRICO: bisogna usare la w_call_center_TV quella con il Tree
g_mb.error("Attenzione, questa finestra è obsoleta e nella versione corrente è stata disattivata: rivolgersi all'assistenza.")
close(this)
return

end event

public function integer wf_imposta_folder ();string ls_nome_dw, ls_des_folder, ls_sharing
long   ll_i, ll_cont=0
unsignedlong lu_sharing
windowobject l_objects[]
uo_tab_richieste luo_richieste 

declare cu_folder cursor for  
  select nome_dw,   
         des_folder,
			sharing_type
    from tab_tipi_richieste_dw  
   where cod_azienda = :s_cs_xx.cod_azienda   
order by cod_azienda asc,   
         progressivo asc  ;

open cu_folder;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore nell'impostazione dei dati della finestra (open cursor)." + sqlca.sqlerrtext)
	return -1
end if

luo_richieste = CREATE uo_tab_richieste

do while true
	fetch cu_folder into :ls_nome_dw, :ls_des_folder, :ls_sharing;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore nell'impostazione dei dati della finestra (fetch cursor)." + sqlca.sqlerrtext)
		return -1
	end if
	
	ll_cont ++
	
	lu_sharing = c_default
	
	if isnull(ls_sharing) or len(ls_sharing) < 1 then 
		lu_sharing = c_sharedata + c_scrollparent
	else
		if pos(lower(ls_sharing), "c_sharedata") > 0        then lu_sharing = lu_sharing + c_sharedata
		if pos(lower(ls_sharing), "c_scrollparent") > 0     then lu_sharing = lu_sharing + c_scrollparent
		if pos(lower(ls_sharing), "c_retrieveonopen") > 0   then lu_sharing = lu_sharing + c_retrieveonopen
		if pos(lower(ls_sharing), "c_noretrieveonopen") > 0 then lu_sharing = lu_sharing + c_noretrieveonopen
		if pos(lower(ls_sharing), "c_nonew") > 0            then lu_sharing = lu_sharing + c_nonew
		if pos(lower(ls_sharing), "c_nomodify") > 0         then lu_sharing = lu_sharing + c_nomodify
		if pos(lower(ls_sharing), "c_nodelete") > 0         then lu_sharing = lu_sharing + c_nodelete
		if pos(lower(ls_sharing), "c_default") > 0          then lu_sharing = lu_sharing + c_default
	end if	
	
	choose case ll_cont
		case 1
			dw_1.dataobject = ls_nome_dw
			
			dw_1.set_dw_options(sqlca, dw_call_center_lista, /*c_sharedata + c_scrollparent*/ lu_sharing, c_default)
			l_objects[1] = dw_1
			dw_folder.fu_assigntab(ll_cont, ls_des_folder, l_Objects[])
			luo_richieste.uof_setddlb(dw_1)
			
		case 2
			dw_2.dataobject = ls_nome_dw
			dw_2.set_dw_options(sqlca, dw_call_center_lista,lu_sharing, c_default)
			l_objects[1] = dw_2
			dw_folder.fu_assigntab(ll_cont, ls_des_folder, l_Objects[])
			luo_richieste.uof_setddlb(dw_2)
			
		case 3
			dw_3.dataobject = ls_nome_dw
			dw_3.set_dw_options(sqlca, dw_call_center_lista,lu_sharing,c_default)
			l_objects[1] = dw_3
			dw_folder.fu_assigntab(ll_cont, ls_des_folder, l_Objects[])
			luo_richieste.uof_setddlb(dw_3)
			
		case 4
			dw_4.dataobject = ls_nome_dw
			dw_4.set_dw_options(sqlca, dw_call_center_lista,lu_sharing,c_default)
			l_objects[1] = dw_4
			dw_folder.fu_assigntab(ll_cont, ls_des_folder, l_Objects[])
			luo_richieste.uof_setddlb(dw_4)
			
		case 5
			dw_5.dataobject = ls_nome_dw
			dw_5.set_dw_options(sqlca, dw_call_center_lista,lu_sharing,c_default)
			l_objects[1] = dw_5
			dw_folder.fu_assigntab(ll_cont, ls_des_folder, l_Objects[])
			luo_richieste.uof_setddlb(dw_5)
			
	end choose
loop

//se ho folder disponibili ne aggiungo uno per il drag-drop dei documenti
ib_dw_blob_visible = false

if ll_cont < 5 then
	ll_cont ++
	l_objects[1] = dw_richieste_blob
	dw_folder.fu_assigntab(ll_cont, "Documenti", l_Objects[])
	
//	lu_sharing += c_nonew + c_nomodify + c_nodelete + c_noretrieveonopen
//	choose case ll_cont
//		case 1
//			dw_1.set_dw_options(sqlca, dw_call_center_lista, lu_sharing, c_default)
//		case 2
//			dw_2.set_dw_options(sqlca, dw_call_center_lista, lu_sharing, c_default)
//		case 3
//			dw_3.set_dw_options(sqlca, dw_call_center_lista, lu_sharing, c_default)
//		case 4
//			dw_4.set_dw_options(sqlca, dw_call_center_lista, lu_sharing, c_default)
//		case 5
//			dw_5.set_dw_options(sqlca, dw_call_center_lista, lu_sharing, c_default)
//	end choose
	
	dw_richieste_blob.settransobject(sqlca)
	
	ib_dw_blob_visible = true
end if


dw_folder.fu_foldercreate(ll_cont,ll_cont)
dw_folder.fu_selectTab(1)

close cu_folder;
return 0 
end function

public function integer wf_carica_elenco_manut (ref uo_cs_xx_dw fdw_datawindow, long fl_anno_registrazione, long fl_num_registrazione);long	 	ll_i, ll_j, ll_k, ll_riga, ll_anno_man, ll_num_man, ll_prog_fase, &
			ll_man_tot_ore, ll_man_tot_minuti, ll_fase_tot_ore, ll_fase_tot_minuti, &
			ll_operaio_tot_ore, ll_operaio_tot_minuti, ll_risorsa_tot_ore, ll_risorsa_tot_minuti, &
			ll_man_ris_tot_ore, ll_man_ris_tot_minuti, ll_fase_tot_ore_ris, ll_fase_tot_minuti_ris

string 	ls_sql, ls_syntax, ls_error, ls_cod_attr, ls_des_attr, ls_des_fase, ls_note, &
			ls_cod_operaio, ls_cognome, ls_nome, ls_cod_risorsa_esterna, ls_descrizione, ls_cod_cat_risorse_esterne

datetime ldt_fase_data_inizio, ldt_fase_ora_inizio, ldt_fase_data_fine, ldt_fase_ora_fine, &
			ldt_operaio_data_inizio, ldt_operaio_ora_inizio, ldt_operaio_data_fine, ldt_operaio_ora_fine, &
			ldt_risorsa_data_inizio, ldt_risorsa_ora_inizio, ldt_risorsa_data_fine, ldt_risorsa_ora_fine

datastore lds_manut, lds_fasi, lds_operai, lds_risorse


ls_sql = &
"select anno_registrazione, " + &
"		  num_registrazione, " + &
"		  cod_attrezzatura, " + &
"		  operaio_ore, " + &
"		  operaio_minuti, " + &
"		  risorsa_ore, " + &
"		  risorsa_minuti " + &
"from	  manutenzioni " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

ls_sql += " order by anno_registrazione ASC, num_registrazione ASC"

ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	g_mb.messagebox("OMNIA","Errore in impostazione select manutenzioni:~n" + ls_error,stopsign!)
	return -1
end if

lds_manut = create datastore

lds_manut.create(ls_syntax,ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	g_mb.messagebox("OMNIA","Errore in creazione datastore manutenzioni:~n" + ls_error,stopsign!)
	destroy lds_manut
	return -1
end if

lds_manut.settransobject(sqlca)

if lds_manut.retrieve() = -1 then
	g_mb.messagebox("OMNIA","Errore in lettura dati manutenzioni",stopsign!)
	destroy lds_manut
	return -1
end if

for ll_i = 1 to lds_manut.rowcount()
	
	ll_anno_man = lds_manut.getitemnumber(ll_i,"anno_registrazione")
	
	ll_num_man = lds_manut.getitemnumber(ll_i,"num_registrazione")
	
	ls_cod_attr = lds_manut.getitemstring(ll_i,"cod_attrezzatura")
	
	ll_man_tot_ore = lds_manut.getitemnumber(ll_i,"operaio_ore")
	
	ll_man_tot_minuti = lds_manut.getitemnumber(ll_i,"operaio_minuti")
	
	ll_man_ris_tot_ore = lds_manut.getitemnumber(ll_i, "risorsa_ore")
	
	ll_man_ris_tot_minuti = lds_manut.getitemnumber(ll_i, "risorsa_minuti")
	
	ls_des_attr = f_des_tabella("anag_attrezzature","cod_attrezzatura = '" + ls_cod_attr + "'","descrizione")
	
	if isnull(ls_des_attr) then
		ls_des_attr = ""
	end if
	
	ll_riga = fdw_datawindow.insertrow(0)
	
	fdw_datawindow.setitem(ll_riga,"tipo_riga","M")
	
	fdw_datawindow.setitem(ll_riga,"manutenzioni_anno_registrazione",ll_anno_man)
	
	fdw_datawindow.setitem(ll_riga,"manutenzioni_num_registrazione",ll_num_man)
	
	fdw_datawindow.setitem(ll_riga,"manutenzioni_cod_attrezzatura",ls_cod_attr)
	
	fdw_datawindow.setitem(ll_riga,"anag_attrezzature_descrizione",ls_des_attr)
	
	fdw_datawindow.setitem(ll_riga,"manutenzioni_tot_ore",ll_man_tot_ore + ll_man_ris_tot_ore)
	
	fdw_datawindow.setitem(ll_riga,"manutenzioni_tot_minuti",ll_man_tot_minuti + ll_man_ris_tot_minuti)
	
	ls_sql = &
	"select prog_fase, " + &
	"		  des_fase, " + &
	"		  data_inizio, " + &
	"		  ora_inizio, " + &
	"		  data_fine, " + &
	"		  ora_fine, " + &
	"		  note, " + &
	"		  tot_ore, " + &
	"		  tot_minuti, " + &
	"		  tot_ore_ris, " + &
	"		  tot_minuti_ris " + &	
	"from	  manutenzioni_fasi " + &
	"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
	"		  anno_registrazione = " + string(ll_anno_man) + " and " + &
	"		  num_registrazione = " + string(ll_num_man) + " "
	
	ls_sql += " order by data_inizio ASC, ora_inizio ASC, data_fine ASC, ora_fine ASC, prog_fase ASC"
	
	ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)
	
	if not isnull(ls_error) and len(trim(ls_error)) > 0 then
		g_mb.messagebox("OMNIA","Errore in impostazione select fasi:~n" + ls_error,stopsign!)
		destroy lds_manut
		return -1
	end if
	
	lds_fasi = create datastore
	
	lds_fasi.create(ls_syntax,ls_error)
	
	if not isnull(ls_error) and len(trim(ls_error)) > 0 then
		g_mb.messagebox("OMNIA","Errore in creazione datastore fasi:~n" + ls_error,stopsign!)
		destroy lds_manut
		destroy lds_fasi
		return -1
	end if
	
	lds_fasi.settransobject(sqlca)
	
	if lds_fasi.retrieve() = -1 then
		g_mb.messagebox("OMNIA","Errore in lettura dati fasi",stopsign!)
		destroy lds_manut
		destroy lds_fasi
		return -1
	end if
	
	for ll_j = 1 to lds_fasi.rowcount()
		
		ll_prog_fase = lds_fasi.getitemnumber(ll_j,"prog_fase")
		
		ls_des_fase = lds_fasi.getitemstring(ll_j,"des_fase")
		
		ldt_fase_data_inizio = lds_fasi.getitemdatetime(ll_j,"data_inizio")
		
		ldt_fase_ora_inizio = lds_fasi.getitemdatetime(ll_j,"ora_inizio")
		
		ldt_fase_data_fine = lds_fasi.getitemdatetime(ll_j,"data_fine")
		
		ldt_fase_ora_fine = lds_fasi.getitemdatetime(ll_j,"ora_fine")
		
		ls_note = lds_fasi.getitemstring(ll_j,"note")
		
		ll_fase_tot_ore = lds_fasi.getitemnumber(ll_j,"tot_ore")
		
		ll_fase_tot_minuti = lds_fasi.getitemnumber(ll_j,"tot_minuti")
		
		ll_fase_tot_ore_ris = lds_fasi.getitemnumber(ll_j,"tot_ore_ris")
		
		ll_fase_tot_minuti_ris = lds_fasi.getitemnumber(ll_j,"tot_minuti_ris")
		
		ll_riga = fdw_datawindow.insertrow(0)
	
		fdw_datawindow.setitem(ll_riga,"tipo_riga","F")
		
		fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_prog_fase",ll_prog_fase)
		
		fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_des_fase",ls_des_fase)
		
		fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_data_inizio",ldt_fase_data_inizio)
		
		fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_ora_inizio",ldt_fase_ora_inizio)
		
		fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_data_fine",ldt_fase_data_fine)
		
		fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_ora_fine",ldt_fase_ora_fine)
		
		fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_note",ls_note)
		
		fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_tot_ore",ll_fase_tot_ore + ll_fase_tot_ore_ris)
		
		fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_tot_minuti",ll_fase_tot_minuti + ll_fase_tot_minuti_ris)
		
		
		ls_sql = &
		"select cod_operaio, " + &
		"		  data_inizio, " + &
		"		  ora_inizio, " + &
		"		  data_fine, " + &
		"		  ora_fine, " + &
		"		  tot_ore, " + &
		"		  tot_minuti " + &
		"from	  manutenzioni_fasi_operai " + &
		"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		"		  anno_registrazione = " + string(ll_anno_man) + " and " + &
		"		  num_registrazione = " + string(ll_num_man) + " and " + &
		"		  prog_fase = " + string(ll_prog_fase)
		
		ls_sql += " order by data_inizio ASC, ora_inizio ASC, data_fine ASC, ora_fine ASC, cod_operaio ASC"
		
		ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)
		
		if not isnull(ls_error) and len(trim(ls_error)) > 0 then
			g_mb.messagebox("OMNIA","Errore in impostazione select operai:~n" + ls_error,stopsign!)
			destroy lds_manut
			return -1
		end if
		
		lds_operai = create datastore
		
		lds_operai.create(ls_syntax,ls_error)
		
		if not isnull(ls_error) and len(trim(ls_error)) > 0 then
			g_mb.messagebox("OMNIA","Errore in creazione datastore operai:~n" + ls_error,stopsign!)
			destroy lds_manut
			destroy lds_fasi
			destroy lds_operai
			return -1
		end if
			
		
		lds_operai.settransobject(sqlca)
		
		if lds_operai.retrieve() = -1 then
			g_mb.messagebox("OMNIA","Errore in lettura dati operai",stopsign!)
			destroy lds_manut
			destroy lds_fasi
			destroy lds_operai
			return -1
		end if
		
		for ll_k = 1 to lds_operai.rowcount()
			
			ls_cod_operaio = lds_operai.getitemstring(ll_k,"cod_operaio")
		
			ldt_operaio_data_inizio = lds_operai.getitemdatetime(ll_k,"data_inizio")
			
			ldt_operaio_ora_inizio = lds_operai.getitemdatetime(ll_k,"ora_inizio")
			
			ldt_operaio_data_fine = lds_operai.getitemdatetime(ll_k,"data_fine")
			
			ldt_operaio_ora_fine = lds_operai.getitemdatetime(ll_k,"ora_fine")
			
			ll_operaio_tot_ore = lds_operai.getitemnumber(ll_k,"tot_ore")
			
			ll_operaio_tot_minuti = lds_operai.getitemnumber(ll_k,"tot_minuti")
			
			ls_cognome = f_des_tabella("anag_operai","cod_operaio = '" + ls_cod_operaio + "'","cognome")
			
			if isnull(ls_cognome) then
				ls_cognome = ""
			end if
			
			ls_nome = f_des_tabella("anag_operai","cod_operaio = '" + ls_cod_operaio + "'","nome")
			
			if isnull(ls_nome) then
				ls_nome = ""
			end if
			
			if ll_k = 1 then
			
				ll_riga = fdw_datawindow.insertrow(0)
			
				fdw_datawindow.setitem(ll_riga,"tipo_riga","E")
				
			end if
			
			ll_riga = fdw_datawindow.insertrow(0)
		
			fdw_datawindow.setitem(ll_riga,"tipo_riga","O")
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_cod_operaio",ls_cod_operaio)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_data_inizio",ldt_operaio_data_inizio)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_ora_inizio",ldt_operaio_ora_inizio)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_data_fine",ldt_operaio_data_fine)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_ora_fine",ldt_operaio_ora_fine)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_tot_ore",ll_operaio_tot_ore)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_tot_minuti",ll_operaio_tot_minuti)
			
			fdw_datawindow.setitem(ll_riga,"anag_operai_cognome",ls_cognome)
			
			fdw_datawindow.setitem(ll_riga,"anag_operai_nome",ls_nome)
			
		next
		
		destroy lds_operai
		
		
		// michela 20/02/2004: risorse
		ls_sql = ""
		ls_sql = &
		"select cod_cat_risorse_esterne, " + &
		"		  cod_risorsa_esterna, " + &		
		"		  data_inizio, " + &
		"		  ora_inizio, " + &
		"		  data_fine, " + &
		"		  ora_fine, " + &
		"		  tot_ore, " + &
		"		  tot_minuti " + &
		"from	  manutenzioni_fasi_risorse " + &
		"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		"		  anno_registrazione = " + string(ll_anno_man) + " and " + &
		"		  num_registrazione = " + string(ll_num_man) + " and " + &
		"		  prog_fase = " + string(ll_prog_fase)
		
		ls_sql += " order by data_inizio ASC, ora_inizio ASC, data_fine ASC, ora_fine ASC, cod_risorsa_esterna ASC, cod_cat_risorse_esterne ASC "
		
		ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)
		
		if not isnull(ls_error) and len(trim(ls_error)) > 0 then
			g_mb.messagebox("OMNIA","Errore in impostazione select risorse:~n" + ls_error,stopsign!)
			destroy lds_manut
			return -1
		end if
		
		lds_risorse = create datastore
		
		lds_risorse.create(ls_syntax,ls_error)
		
		if not isnull(ls_error) and len(trim(ls_error)) > 0 then
			g_mb.messagebox("OMNIA","Errore in creazione datastore operai:~n" + ls_error,stopsign!)
			destroy lds_manut
			destroy lds_fasi
			destroy lds_risorse
			return -1
		end if
			
		
		lds_risorse.settransobject(sqlca)
		
		if lds_risorse.retrieve() = -1 then
			g_mb.messagebox("OMNIA","Errore in lettura dati risorse",stopsign!)
			destroy lds_manut
			destroy lds_fasi
			destroy lds_risorse
			return -1
		end if
		
		for ll_k = 1 to lds_risorse.rowcount()
			
			ls_cod_cat_risorse_esterne = lds_risorse.getitemstring(ll_k, "cod_cat_risorse_esterne")
			
			ls_cod_risorsa_esterna = lds_risorse.getitemstring(ll_k, "cod_risorsa_esterna")			
		
			ldt_risorsa_data_inizio = lds_risorse.getitemdatetime(ll_k,"data_inizio")
			
			ldt_risorsa_ora_inizio = lds_risorse.getitemdatetime(ll_k,"ora_inizio")
			
			ldt_risorsa_data_fine = lds_risorse.getitemdatetime(ll_k,"data_fine")
			
			ldt_risorsa_ora_fine = lds_risorse.getitemdatetime(ll_k,"ora_fine")
			
			ll_risorsa_tot_ore = lds_risorse.getitemnumber(ll_k,"tot_ore")
			
			ll_risorsa_tot_minuti = lds_risorse.getitemnumber(ll_k,"tot_minuti")
			
			ls_descrizione = f_des_tabella("anag_risorse_esterne","cod_risorsa_esterna = '" + ls_cod_risorsa_esterna + "'  AND cod_cat_risorse_esterne = '" + ls_cod_cat_risorse_esterne + "'" ,"descrizione")
			
			if isnull(ls_descrizione) then
				ls_descrizione = ""
			end if
			
			if ll_k = 1 then
			
				ll_riga = fdw_datawindow.insertrow(0)
			
				fdw_datawindow.setitem(ll_riga,"tipo_riga","E")
				
			end if
			
			ll_riga = fdw_datawindow.insertrow(0)
		
			fdw_datawindow.setitem(ll_riga,"tipo_riga","O")
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_cod_operaio",ls_cod_risorsa_esterna)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_data_inizio",ldt_risorsa_data_inizio)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_ora_inizio",ldt_risorsa_ora_inizio)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_data_fine",ldt_risorsa_data_fine)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_ora_fine",ldt_risorsa_ora_fine)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_tot_ore",ll_risorsa_tot_ore)
			
			fdw_datawindow.setitem(ll_riga,"manutenzioni_fasi_operai_tot_minuti",ll_risorsa_tot_minuti)
			
			fdw_datawindow.setitem(ll_riga,"anag_operai_cognome",ls_descrizione)
			
			fdw_datawindow.setitem(ll_riga,"anag_operai_nome"," ")
			
		next
		
		destroy lds_risorse		
		
	next
	
	destroy lds_fasi
	
next

destroy lds_manut

return 0
end function

public function integer wf_blocca_folder (string fs_cod_tipo_richiesta);string ls_flag_blocco
long   ll_i, ll_cont=0
uo_tab_richieste luo_richieste 

declare cu_folder cursor for  
  select flag_blocco         
    from tab_tipi_richieste_folder  
   where cod_azienda = :s_cs_xx.cod_azienda   and
	      cod_tipo_richiesta = :fs_cod_tipo_richiesta
order by cod_azienda asc,   
         progressivo asc  ;

open cu_folder;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore nell'impostazione dei dati della finestra (open cursor)." + sqlca.sqlerrtext)
	return -1
end if

do while true
	fetch cu_folder into :ls_flag_blocco;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore nell'impostazione dei dati della finestra (fetch cursor)." + sqlca.sqlerrtext)
		return -1
	end if
	
	ll_cont ++
	
	if ls_flag_blocco = 'S' then
		dw_folder.fu_hidetab(ll_cont)
	else
		dw_folder.fu_showtab(ll_cont)
	end if
	
loop

close cu_folder;

dw_folder.fu_FolderResize()
return 0 
end function

public function integer wf_invia_mail (string as_oggetto, string as_messaggio);s_cs_xx.parametri.parametro_s_1 = "L"
s_cs_xx.parametri.parametro_s_2 = ""
s_cs_xx.parametri.parametro_s_3 = ""
s_cs_xx.parametri.parametro_s_4 = ""
s_cs_xx.parametri.parametro_s_5 = as_oggetto
s_cs_xx.parametri.parametro_s_6 = as_messaggio
s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari della fase successiva?"
s_cs_xx.parametri.parametro_s_8 = ""
s_cs_xx.parametri.parametro_s_11 = ""
s_cs_xx.parametri.parametro_s_12 = ""
//s_cs_xx.parametri.parametro_s_13 = string(ll_anno)
//s_cs_xx.parametri.parametro_s_14 = string(ll_num)
s_cs_xx.parametri.parametro_s_15 = "N"
//s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
s_cs_xx.parametri.parametro_b_1 = true
					
openwithparm( w_invio_messaggi, 0)	

return 0
end function

public function boolean wf_get_barcode (string as_barcode, ref integer ai_anno_registrazione, ref long al_num_registrazione, ref string as_errore);/**
 * stefanop
 * 10/04/2012
 *
 * Legge il barcode dell'ordine di di lavoro e chiude la richiesta.
 **/
 
long ll_anno_registrazione, ll_num_registrazione

setnull(ai_anno_registrazione)
setnull(al_num_registrazione)
setnull(as_errore)
 
// Validazioni
if isnull(as_barcode) or as_barcode = "" then return true

as_barcode = trim(as_barcode)

if len(as_barcode) < 11 then
	as_errore = "Formato barcode non valido"
	return false
end if

ai_anno_registrazione = integer(mid(as_barcode, 2,4))
al_num_registrazione = long(mid(as_barcode, 6))

return true



end function

public subroutine wf_retrieve_blob (long al_row);integer			li_anno_registrazione
long				ll_num_registrazione

dw_richieste_blob.reset()

if al_row > 0 then
	li_anno_registrazione = dw_call_center_lista.getitemnumber(al_row, "anno_registrazione")
	ll_num_registrazione = dw_call_center_lista.getitemnumber(al_row, "num_registrazione")
	
	if not isnull(li_anno_registrazione) and ll_num_registrazione > 0 and ib_dw_blob_visible then
		dw_richieste_blob.retrieve(s_cs_xx.cod_azienda, li_anno_registrazione, ll_num_registrazione )
	end if
end if
end subroutine

on w_call_center.create
int iCurrent
call super::create
this.dw_richieste_blob=create dw_richieste_blob
this.cb_supplementi=create cb_supplementi
this.dw_4=create dw_4
this.dw_3=create dw_3
this.dw_2=create dw_2
this.dw_5=create dw_5
this.dw_1=create dw_1
this.dw_folder=create dw_folder
this.cb_blob=create cb_blob
this.cb_genera_nc=create cb_genera_nc
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_call_center_lista=create dw_call_center_lista
this.dw_folder_search=create dw_folder_search
this.dw_ricerca=create dw_ricerca
this.cb_ol_lav_richiesta=create cb_ol_lav_richiesta
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_richieste_blob
this.Control[iCurrent+2]=this.cb_supplementi
this.Control[iCurrent+3]=this.dw_4
this.Control[iCurrent+4]=this.dw_3
this.Control[iCurrent+5]=this.dw_2
this.Control[iCurrent+6]=this.dw_5
this.Control[iCurrent+7]=this.dw_1
this.Control[iCurrent+8]=this.dw_folder
this.Control[iCurrent+9]=this.cb_blob
this.Control[iCurrent+10]=this.cb_genera_nc
this.Control[iCurrent+11]=this.cb_reset
this.Control[iCurrent+12]=this.cb_ricerca
this.Control[iCurrent+13]=this.dw_call_center_lista
this.Control[iCurrent+14]=this.dw_folder_search
this.Control[iCurrent+15]=this.dw_ricerca
this.Control[iCurrent+16]=this.cb_ol_lav_richiesta
end on

on w_call_center.destroy
call super::destroy
destroy(this.dw_richieste_blob)
destroy(this.cb_supplementi)
destroy(this.dw_4)
destroy(this.dw_3)
destroy(this.dw_2)
destroy(this.dw_5)
destroy(this.dw_1)
destroy(this.dw_folder)
destroy(this.cb_blob)
destroy(this.cb_genera_nc)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_call_center_lista)
destroy(this.dw_folder_search)
destroy(this.dw_ricerca)
destroy(this.cb_ol_lav_richiesta)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag, ls_ASS
windowobject l_objects[ ], l_vuoto[]
integer li_index


// *** se non esiste il parametro GCB allora faccio vedere 
//		 la lista con la commessa

select flag
into   :ls_flag
from   parametri
where  cod_parametro = 'GCB';

if sqlca.sqlcode <> 0 or (sqlca.sqlcode = 0 and not isnull(ls_flag) and ls_flag = "N") then
	dw_call_center_lista.dataobject = "d_call_center_lista_divisione"
end if

setnull(ls_flag)

dw_call_center_lista.set_dw_options(sqlca,pcca.null_object, c_noretrieveonopen,c_default)

iuo_dw_main = dw_call_center_lista

if wf_imposta_folder() = -1 then return

select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
if sqlca.sqlcode = 0 and ls_flag ="S" then ib_global_service = true


dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[] = l_vuoto[]
l_objects[1] = dw_call_center_lista
l_objects[2] = cb_blob
l_objects[3] = cb_genera_nc

li_index = 3
//questo sempre per ultimo ---------------------------------------------
if ib_global_service then
	li_index += 1
	l_objects[li_index] = cb_ol_lav_richiesta
	
	select flag
	into   :ls_ASS
	from   parametri_azienda
	where  cod_parametro = 'ASS' and flag_parametro='F';
	
	if ls_ASS="S" then
		cb_supplementi.visible = true
		li_index += 1
		l_objects[li_index] = cb_supplementi
	else
		cb_supplementi.visible = false
	end if
else
	cb_supplementi.visible = false
end if
//---------------------------------------------------------------------------

//cb_ol_lav_richiesta.enabled = ib_global_service
//cb_ol_lav_richiesta.visible = ib_global_service
//--------------------------------------------------------------------

dw_folder_search.fu_assigntab(1, "L.", l_Objects[])


l_objects[] = l_vuoto[]
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset

dw_folder_search.fu_assigntab(2, "R.", l_Objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)

dw_ricerca.setitem(1, "anno_registrazione", f_anno_esercizio())

is_sql_base = dw_call_center_lista.getsqlselect()

event post ue_barcode_focus()

event post ue_chiudi()


end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_sort(dw_ricerca, &
				  "cod_tipo_richiesta", &
				  sqlca, &
				  "tab_tipi_richieste", &
				  "cod_tipo_richiesta", &
				  "des_tipo_richiesta", &
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ", &
				  " cod_tipo_richiesta ASC ")

f_po_loaddddw_sort(dw_ricerca, &
					  "cod_richiedente", &
					  sqlca, &
					  "tab_richiedenti", &
					  "cod_richiedente", &
					  "nominativo", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
					  " nominativo ASC ")
					  
f_po_loaddddw_sort(dw_ricerca, &
				  "cod_area_chiamata", &
				  sqlca, &
				  "tab_aree_chiamate", &
				  "cod_area_chiamata", &
				  "des_area_chiamata", &
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ", &
				  " des_area_chiamata ASC ")					  

f_po_loaddddw_sort(dw_ricerca, &
                 "cod_operaio_registrazione", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
					  " cognome ASC, nome ASC ")
					  
					  
f_po_loaddddw_sort(dw_ricerca, &
                 "cod_operaio_incaricato", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
					  " cognome ASC, nome ASC ")

f_po_loaddddw_sort(dw_ricerca, &
                 "cod_tipologia_richiesta", &
                 sqlca, &
                 "tab_richieste_tipologie", &
                 "cod_tipologia_richiesta", &
                 "des_tipologia_richiesta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
					  " des_tipologia_richiesta ASC ")

f_po_loaddddw_sort(dw_ricerca, &
                 "cod_classe_richiesta", &
                 sqlca, &
                 "tab_richieste_classificazioni", &
                 "cod_classe_richiesta", &
                 "des_classe_richiesta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
					  " des_classe_richiesta ASC ")

f_po_loaddddw_sort(dw_ricerca, &
					  "cod_documento", &
					  sqlca, &
					  "tab_documenti", &
					  "cod_documento", &
					  "des_documento", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
					  " des_documento ASC ")

// stefano 05/05/2010
if dw_1.dataobject <> "d_call_center_8" then
	f_po_loaddddw_sort(dw_ricerca, &
						  "cod_cat_risorsa_esterna", &
						  sqlca, &
						  "tab_cat_risorse_esterne", &
						  "cod_cat_risorse_esterne", &
						  "des_cat_risorse_esterne", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
						  " des_cat_risorse_esterne ASC ")
end if
end event

event open;call super::open;string ls_valore
long 	ll_return

iuo_richieste = create uo_tab_richieste
iuo_richieste.ib_global_service = ib_global_service

if s_cs_xx.parametri.parametro_s_1 = "GEN_RICHIESTA" then
	ib_proviene_da_nc=true
	il_anno_nc = s_cs_xx.parametri.parametro_d_1
	il_num_nc  = s_cs_xx.parametri.parametro_d_2
end if

// -- stefanop 03/07/2009: Controllo parametro MAI nel registro per inviare la mail
ll_return = registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "mai", ls_valore) 
if ll_return = -1 or long(ls_valore) <= 0 then
	ib_send_mail = false
else
	ib_send_mail = true
end if
// ----------------------

//12/07/2010 verifica se deve apparire il messaggio di conferma invio e-mail se aggiorni la richiesta a staordinaria
select flag
into   :is_NMS
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'F' and &
       parametri_azienda.cod_parametro = 'NMS';

//se non esiste: funzionamento normale
if isnull(is_NMS) or is_NMS="" then is_NMS="N"
//------------------------------------------------------------------------------
end event

event close;call super::close;destroy iuo_richieste
end event

event pc_modify;call super::pc_modify;iuo_richieste.uof_new_modify(dw_1,true)
iuo_richieste.uof_new_modify(dw_2,true)
iuo_richieste.uof_new_modify(dw_3,true)
iuo_richieste.uof_new_modify(dw_4,true)
iuo_richieste.uof_new_modify(dw_5,true)

end event

event pc_new;call super::pc_new;iuo_richieste.uof_new_modify(dw_1,true)
iuo_richieste.uof_new_modify(dw_2,true)
iuo_richieste.uof_new_modify(dw_3,true)
iuo_richieste.uof_new_modify(dw_4,true)
iuo_richieste.uof_new_modify(dw_5,true)

end event

event pc_view;call super::pc_view;iuo_richieste.uof_new_modify(dw_1,false)
iuo_richieste.uof_new_modify(dw_2,false)
iuo_richieste.uof_new_modify(dw_3,false)
iuo_richieste.uof_new_modify(dw_4,false)
iuo_richieste.uof_new_modify(dw_5,false)

end event

type dw_richieste_blob from uo_dw_drag within w_call_center
integer x = 14
integer y = 1168
integer width = 3584
integer height = 1212
integer taborder = 20
string dataobject = "d_call_center_blob_drag"
boolean border = true
end type

event doubleclicked;call super::doubleclicked;string ls_temp_dir, ls_rnd, ls_estensione, ls_file_name
long ll_anno_registrazione, ll_num_registrazione, ll_cont,ll_prog_mimetype, ll_len, ll_progressivo
blob lb_blob
uo_shellexecute luo_run

if row > 0 then
	
	ll_anno_registrazione = getitemnumber(row, "anno_registrazione")
	ll_num_registrazione = getitemnumber(row, "num_registrazione")
	
	ll_progressivo = getitemnumber(row, "progressivo")
	ll_prog_mimetype = getitemnumber(row, "prog_mimetype")
	
	selectblob blob
	into :lb_blob
	from tab_richieste_blob
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			progressivo = :ll_progressivo;
			
	if sqlca.sqlcode = 0  then 
		
		ll_len = lenA(lb_blob)
		
		select estensione
		into :ls_estensione
		from tab_mimetype
		where cod_azienda = :s_cs_xx.cod_azienda and
				prog_mimetype = :ll_prog_mimetype;
		
		ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
		
		ls_rnd = string( hour(now()),"00") +"_" + string( minute(now()),"00") +"_" + string( second(now()),"00") 
		ls_file_name =  ls_temp_dir + ls_rnd + "." + ls_estensione
		guo_functions.uof_blob_to_file( lb_blob, ls_file_name)
		
		
		luo_run = create uo_shellexecute
		luo_run.uof_run( handle(parent), ls_file_name )
		destroy(luo_run)
		
	end if
end if
end event

event buttonclicked;call super::buttonclicked;if isvalid(dwo) then
	if dwo.name = "b_elimina" and row > 0 then
		
		integer li_anno_registrazione
		long ll_num_registrazione, ll_progressivo
		string ls_des_blob, ls_errore
		transaction sqlc_blob
	
		
		li_anno_registrazione = getitemnumber(row, "anno_registrazione")
		ll_num_registrazione = getitemnumber(row, "num_registrazione")
		ll_progressivo = getitemnumber(row, "progressivo")
		ls_des_blob = getitemstring(row, "des_blob")
		
		if isnull(ls_des_blob) or ls_des_blob="" then ls_des_blob="<senza nome>"
		
		if g_mb.messagebox("Apice", "Procedo con la cancellazione del documento " + string(ll_progressivo) + " - " + ls_des_blob + "?",Question!, Yesno!, 2) = 2 then return 
			
		// creo transazione separata
		if not guo_functions.uof_create_transaction_from( sqlca, sqlc_blob, ls_errore)  then
			g_mb.error("Errore in creazione transazione~r~n" + ls_errore)
			return
		end if
		
		delete tab_richieste_blob
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_registrazione and
				num_registrazione = :ll_num_registrazione and
				progressivo = :ll_progressivo
		using sqlc_blob;
		
		if sqlc_blob.sqlcode <> 0 then
			g_mb.error("Errore in cancellazione documento~r~n " + sqlc_blob.sqlerrtext)
			rollback using sqlc_blob;
			disconnect using sqlc_blob;
			destroy sqlc_blob
		else
			commit using sqlc_blob;
			disconnect using sqlc_blob;
			destroy sqlc_blob
		end if		
		
		retrieve(s_cs_xx.cod_azienda, li_anno_registrazione, ll_num_registrazione)
				
	end if
end if
end event

event ue_start_drop;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

integer		li_anno_registrazione, li_cont
long			ll_row, ll_num_registrazione
string			ls_errore


ll_row = dw_call_center_lista.getrow()

if isnull(ll_row) or ll_row < 1 then
	as_message = "Non è stata inserita alcuna richiesta oppure è necessario selezionare una richiesta dalla lista!"
	ab_return = false
	return
end if

li_anno_registrazione = dw_call_center_lista.getitemnumber(ll_row, "anno_registrazione")
ll_num_registrazione = dw_call_center_lista.getitemnumber(ll_row, "num_registrazione")

select 	count(*) 
into 		:li_cont
from 		tab_richieste
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :li_anno_registrazione and
			num_registrazione = :ll_num_registrazione;

if li_cont>0 then
else
	as_message = "Prima di associare documenti è necessario salvare la richiesta!"
	ab_return = false
	return 
end if


//creo la transazione (di istanza) che sarà usata per il salvataggio dei documenti
if not guo_functions.uof_create_transaction_from( sqlca, itran_note, ls_errore)  then
	as_message = "Errore in creazione transazione~r~n" + ls_errore
	ab_return = false
	return
end if

ab_return = true
return
end event

event ue_drop_file;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

integer			li_anno_registrazione
long				ll_num_registrazione, ll_row, ll_progressivo, ll_len, ll_prog_mimetype
string				ls_cod_nota, ls_des_blob, ls_dir, ls_file, ls_ext,ls_errore
blob				lb_note_esterne


ll_row = dw_call_center_lista.getrow()

//di sicuro una riga selezionata c'è (controllato da ue_start_drop)
li_anno_registrazione = dw_call_center_lista.getitemnumber(ll_row, "anno_registrazione")
ll_num_registrazione = dw_call_center_lista.getitemnumber(ll_row, "num_registrazione")

select 	count(*) 
into 		:ll_progressivo
from 		tab_richieste_blob
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :li_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
			
if ll_progressivo = 0 or isnull(ll_progressivo) then
	ll_progressivo = 1
else
	ll_progressivo += 1
end if

ls_des_blob = "Documento"

guo_functions.uof_file_to_blob(as_filename, lb_note_esterne)

ll_len = lenA(lb_note_esterne)

guo_functions.uof_get_file_info( as_filename, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;
if sqlca.sqlcode <> 0 then
	as_message = "Attenzione: estensione file " + ls_ext + " non prevista da sistema."
	return false
end if

insert into tab_richieste_blob  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  progressivo,   
		  des_blob, 
		  blob,
		  prog_mimetype,
		  descrizione)  
values ( :s_cs_xx.cod_azienda,   
		  :li_anno_registrazione,   
		  :ll_num_registrazione,
		  :ll_progressivo,
		  :ls_file,   
		  null,
		 :ll_prog_mimetype,
		 :ls_file) 
using itran_note;

if itran_note.sqlcode = 0 then
	
	updateblob tab_richieste_blob
	set blob = :lb_note_esterne
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :li_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			progressivo = :ll_progressivo
	using itran_note;
			
	if itran_note.sqlcode <> 0 then
		as_message = "Errore nell'inserimento del documento digitale.~r~n" + itran_note.sqlerrtext
		return false
	end if
	
else
	
	if itran_note.sqlcode <> 0 then
		as_message = "Errore nell'inserimento del documento digitale.~r~n" + itran_note.sqlerrtext
		return false
	end if
end if

return true


end event

event ue_end_drop;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

long					ll_row

if ab_status then
	//commit
	commit using itran_note;
	disconnect using itran_note;
	destroy itran_note;
else
	//rollback
	if isvalid(itran_note) then
		rollback using itran_note;
		disconnect using itran_note;
		destroy itran_note;
	end if
	
	if as_message<>"" and not isnull(as_message) then g_mb.error(as_message)
	
end if

ll_row = dw_call_center_lista.getrow()

wf_retrieve_blob(ll_row)

return true
end event

type cb_supplementi from commandbutton within w_call_center
boolean visible = false
integer x = 1920
integer y = 944
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Supplementi"
end type

event clicked;integer			li_anno_reg_richiesta
long				ll_num_reg_richiesta, ll_row

if not ib_global_service then return

ll_row = dw_call_center_lista.getrow()

if ll_row>0 then
	li_anno_reg_richiesta = dw_call_center_lista.getitemnumber(ll_row, "anno_registrazione")
	ll_num_reg_richiesta = dw_call_center_lista.getitemnumber(ll_row, "num_registrazione")
	
	if li_anno_reg_richiesta>0 and ll_num_reg_richiesta>0 then
	else
		return
	end if
	
	window_open_parm(w_call_center_supplementi, -1, dw_call_center_lista)
	
end if









end event

type dw_4 from uo_cs_xx_dw within w_call_center
integer x = 14
integer y = 1164
integer width = 3589
integer height = 1216
integer taborder = 40
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	
	if i_colname = "cod_tipo_richiesta" then
		if not isnull(i_coltext) then
			wf_blocca_folder(i_coltext)
		end if
	end if	
	iuo_richieste.uof_itemchanged(dw_4, row, i_colname, i_coltext)
	
end if
end event

event doubleclicked;call super::doubleclicked;if i_extendmode then
	
	iuo_richieste.uof_doubleclicked(dw_4, row, dwo)
	
end if
end event

event clicked;call super::clicked;if i_extendmode then
	
	iuo_richieste.uof_clicked(dw_4, row, dwo)
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	if isvalid(iuo_richieste) then
	
		iuo_richieste.uof_rowfocuschanged(REF dw_4, getrow())
		
	end if
	
end if
end event

event pcd_delete;call super::pcd_delete;//long ll_anno_reg_richiesta, ll_num_reg_richiesta, ll_cont
//
//ll_anno_reg_richiesta = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"anno_registrazione")
//ll_num_reg_richiesta  = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"num_registrazione")
//				
//select count(*)
//into   :ll_cont
//from   manutenzioni
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 anno_reg_richiesta = :ll_anno_reg_richiesta and
//		 num_reg_richiesta = :ll_num_reg_richiesta;
//		 
//if not isnull(ll_cont) and ll_cont > 0 then
//	g_mb.messagebox("OMNIA", "Esistono delle manutenzioni associate a questa richiesta.~r~nE' necessario cancellare prima quelle!", Exclamation!)
//	return -1
//end if
end event

type dw_3 from uo_cs_xx_dw within w_call_center
integer x = 14
integer y = 1164
integer width = 3589
integer height = 1216
integer taborder = 40
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	
	if i_colname = "cod_tipo_richiesta" then
		if not isnull(i_coltext) then
			wf_blocca_folder(i_coltext)
		end if
	end if	
	iuo_richieste.uof_itemchanged(dw_3, row, i_colname, i_coltext)
	
end if
end event

event doubleclicked;call super::doubleclicked;if i_extendmode then
	
	iuo_richieste.uof_doubleclicked(dw_3, row, dwo)
	
end if
end event

event clicked;call super::clicked;if i_extendmode then
	
	iuo_richieste.uof_clicked(dw_3, row, dwo)
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	if isvalid(iuo_richieste) then
	
		iuo_richieste.uof_rowfocuschanged(REF dw_3, getrow())
		
	end if
	
end if
end event

event pcd_delete;call super::pcd_delete;//long ll_anno_reg_richiesta, ll_num_reg_richiesta, ll_cont
//
//ll_anno_reg_richiesta = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"anno_registrazione")
//ll_num_reg_richiesta  = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"num_registrazione")
//				
//select count(*)
//into   :ll_cont
//from   manutenzioni
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 anno_reg_richiesta = :ll_anno_reg_richiesta and
//		 num_reg_richiesta = :ll_num_reg_richiesta;
//		 
//if not isnull(ll_cont) and ll_cont > 0 then
//	g_mb.messagebox("OMNIA", "Esistono delle manutenzioni associate a questa richiesta.~r~nE' necessario cancellare prima quelle!", Exclamation!)
//	return -1
//end if
end event

type dw_2 from uo_cs_xx_dw within w_call_center
event ue_esegui_retrieve ( )
event ue_noscroll pbm_vscroll
integer x = 14
integer y = 1164
integer width = 3589
integer height = 1216
integer taborder = 30
end type

event ue_esegui_retrieve();//ue_esegui_retrieve
il_row = getrow()
dw_call_center_lista.triggerevent("pcd_retrieve")
dw_call_center_lista.postevent("ue_riposiziona_riga")
end event

event ue_noscroll;return 1
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	
	if i_colname = "cod_tipo_richiesta" then
		if not isnull(i_coltext) then
			wf_blocca_folder(i_coltext)
		end if
	end if	
	iuo_richieste.uof_itemchanged(dw_2, row, i_colname, i_coltext)
	
end if
end event

event doubleclicked;call super::doubleclicked;if i_extendmode then
	
	iuo_richieste.uof_doubleclicked(dw_2, row, dwo)
	
end if
end event

event clicked;call super::clicked;if i_extendmode then
	
	iuo_richieste.uof_clicked(dw_2, row, dwo)
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	if isvalid(iuo_richieste) then
	
		iuo_richieste.uof_rowfocuschanged(REF dw_2, getrow())
		
	end if
	
end if
end event

event pcd_delete;call super::pcd_delete;//long ll_anno_reg_richiesta, ll_num_reg_richiesta, ll_cont
//
//ll_anno_reg_richiesta = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"anno_registrazione")
//ll_num_reg_richiesta  = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"num_registrazione")
//				
//select count(*)
//into   :ll_cont
//from   manutenzioni
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 anno_reg_richiesta = :ll_anno_reg_richiesta and
//		 num_reg_richiesta = :ll_num_reg_richiesta;
//		 
//if not isnull(ll_cont) and ll_cont > 0 then
//	g_mb.messagebox("OMNIA", "Esistono delle manutenzioni associate a questa richiesta.~r~nE' necessario cancellare prima quelle!", Exclamation!)
//	return -1
//end if
end event

type dw_5 from uo_cs_xx_dw within w_call_center
integer x = 14
integer y = 1164
integer width = 3589
integer height = 1216
integer taborder = 40
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	
	if i_colname = "cod_tipo_richiesta" then
		if not isnull(i_coltext) then
			wf_blocca_folder(i_coltext)
		change_dw_current(this)
		end if
	end if	
	iuo_richieste.uof_itemchanged(dw_5, row, i_colname, i_coltext)
	
end if
end event

event doubleclicked;call super::doubleclicked;if i_extendmode then
	
	iuo_richieste.uof_doubleclicked(dw_5, row, dwo)
	
end if
end event

event clicked;call super::clicked;if i_extendmode then
	
	iuo_richieste.uof_clicked(dw_5, row, dwo)
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	if isvalid(iuo_richieste) then
	
		iuo_richieste.uof_rowfocuschanged(REF dw_5, getrow())
		
	end if
	
end if
end event

event pcd_delete;call super::pcd_delete;//long ll_anno_reg_richiesta, ll_num_reg_richiesta, ll_cont
//
//ll_anno_reg_richiesta = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"anno_registrazione")
//ll_num_reg_richiesta  = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"num_registrazione")
//				
//select count(*)
//into   :ll_cont
//from   manutenzioni
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 anno_reg_richiesta = :ll_anno_reg_richiesta and
//		 num_reg_richiesta = :ll_num_reg_richiesta;
//		 
//if not isnull(ll_cont) and ll_cont > 0 then
//	g_mb.messagebox("OMNIA", "Esistono delle manutenzioni associate a questa richiesta.~r~nE' necessario cancellare prima quelle!", Exclamation!)
//	return -1
//end if
end event

type dw_1 from uo_cs_xx_dw within w_call_center
event ue_esegui_retrieve ( )
event ue_noscroll pbm_vscroll
integer x = 14
integer y = 1164
integer width = 3589
integer height = 1216
integer taborder = 20
end type

event ue_esegui_retrieve();//ue_esegui_retrieve
il_row = getrow()
parent.triggerevent("pc_retrieve")
dw_call_center_lista.postevent("ue_riposiziona_riga")
end event

event ue_noscroll;return 1
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	
	if i_colname = "cod_tipo_richiesta" then
		if not isnull(i_coltext) then
			wf_blocca_folder(i_coltext)
		end if
	end if	
	
	iuo_richieste.uof_itemchanged(dw_1, row, i_colname, i_coltext)

end if
end event

event doubleclicked;call super::doubleclicked;if i_extendmode then
	
	iuo_richieste.uof_doubleclicked(dw_1, row, dwo)
	
end if
end event

event clicked;call super::clicked;if i_extendmode then

	iuo_richieste.uof_clicked(dw_1, row, dwo)
		
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	if isvalid(iuo_richieste) then
	
		iuo_richieste.uof_rowfocuschanged(REF dw_1, getrow())
		
	end if
	
end if
end event

event pcd_modify;call super::pcd_modify;string ls_flag

select flag
into :ls_flag
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
cod_parametro = 'RIC';

if sqlca.sqlcode <> 0 then
	return
else
	dw_1.settaborder("num_documento",140)
	dw_1.object.num_documento.border = 5
	dw_1.object.num_documento.background.mode = 0
	dw_1.object.num_documento.background.color = RGB(255,255,255) 
end if
end event

event pcd_delete;call super::pcd_delete;//long ll_anno_reg_richiesta, ll_num_reg_richiesta, ll_cont
//
//ll_anno_reg_richiesta = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"anno_registrazione")
//ll_num_reg_richiesta  = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"num_registrazione")
//				
//select count(*)
//into   :ll_cont
//from   manutenzioni
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 anno_reg_richiesta = :ll_anno_reg_richiesta and
//		 num_reg_richiesta = :ll_num_reg_richiesta;
//		 
//if not isnull(ll_cont) and ll_cont > 0 then
//	g_mb.messagebox("OMNIA", "Esistono delle manutenzioni associate a questa richiesta.~r~nE' necessario cancellare prima quelle!", Exclamation!)
//	return -1
//end if
end event

event pcd_view;call super::pcd_view;//if not ib_stato_modifica then
//	dw_1.resetupdate()
//end if
end event

event rbuttondown;//Donato 30/03/2009
/*
sovrascritto l'evento dell'ancestor perchè quando apre la maschera di ricerca
delle commesse e seleziono una commessa perde la window current e poi non salva più

N.B. QUESTA E' UNA "PEZZA" PROVVISORIA FINO A QUANDO NON CAPISCO
PERCHE' NON FUNZIONA!!!!! PORCA ZOZZA!!!!!

quindi di seguito metto prima:	pcca.window_current = parent
e poi incollo il codice dell'ancestor, togliendo il segno di spunta
in Edit -> Exstend Ancestor Script....

*/

//CODICE DA ESEGUIRE PER PRIMO
pcca.window_current = parent
//------------------------------------------

//INCOLLO QUI POI IL CODICE DELL'ANCESTOR
//uo_dw_main #################################################
//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : RButtonDown
//  Description   : Processes the Right-button-down event.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

IF NOT i_InUse THEN
   i_Window.TriggerEvent("RButtonDown")
   GOTO Finished
END IF

//----------
//  Make sure that this window is active before we process
//  the right mouse button event.
//----------

IF PCCA.Window_Current <> i_Window THEN
   i_Window.SetFocus()
   Change_DW_Focus(THIS)
END IF

//----------
//  Make sure that the popup menu has been initialized.
//----------

IF NOT i_PopupMenuInit THEN
   Set_DW_PMenu(i_PopupMenu)
END IF

//----------
//  Make sure that we have a right-button Popup Menu.
//----------

IF i_PopupMenuIsValid THEN

   //----------
   //  Make sure the POPUP menus are loaded correctly.
   //----------

   is_EventControl.Control_Mode = c_ControlPopupMenu
   TriggerEvent("pcd_SetControl")

   //----------
   //  We need to specify the position of the right-button Popup
   //  Menu.  If the window that contains this DataWindow is
   //  within an MDI frame, we need to specify the Popup Menu
   //  postion relative to the MDI Frame.  If the window is not
   //  inside the MDI frame, then we specify the Popup Menu's
   //  position relative to the window.  Note that a Response!-
   //  style window is not inside the MDI frame, even for a MDI
   //  application.
   //----------

   IF PCCA.MDI_Frame_Valid AND &
      i_Window.WindowType <> Response! THEN
      i_PopupMenu.PopMenu(PCCA.MDI_Frame.PointerX(), &
                          PCCA.MDI_Frame.PointerY())
   ELSE
      i_PopupMenu.PopMenu(i_Window.PointerX(), &
                          i_Window.PointerY())
   END IF
END IF

Finished:

i_ExtendMode = i_InUse

//##########################################################
//FINE CODICE ANCESTOR------------------------------------------------------------------------------------
end event

type dw_folder from u_folder within w_call_center
integer y = 1072
integer width = 3621
integer height = 1324
integer taborder = 10
end type

type cb_blob from commandbutton within w_call_center
integer x = 2729
integer y = 944
integer width = 389
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Documenti"
end type

event clicked;if dw_call_center_lista.rowcount()>0 then
//	if isnull(dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"anno_registrazione")) then return
//	window_open_parm(w_call_center_blob, -1, dw_call_center_lista)
	//window_open_parm(w_call_center_ole, -1, dw_call_center_lista)
	
	s_cs_xx.parametri.parametro_ul_1 = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(), "anno_registrazione")
	s_cs_xx.parametri.parametro_ul_2 = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(), "num_registrazione")
	//window_open(w_call_center_ole, 1)
	
	open(w_call_center_ole)
end if
end event

type cb_genera_nc from commandbutton within w_call_center
integer x = 3136
integer y = 944
integer width = 389
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen. N.C."
end type

event clicked;string ls_cod_prodotto, ls_cod_operaio,ls_cod_cliente, ls_des_anomalia_1, ls_flag_prod_codificato
long ll_anno_registrazione, ll_num_registrazione,ll_anno_richiesta, ll_num_richiesta
datetime ldt_data_registrazione

if dw_call_center_lista.getrow() < 1 then return
if dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"anno_non_conformita") > 0 or not isnull(dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"anno_non_conformita")) then
	g_mb.messagebox("OMNIA","E' già stata generata una NC per questa anomalia")
	return
end if

ll_anno_richiesta = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"anno_registrazione")
ll_num_richiesta  = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"num_registrazione")

setnull(ll_num_registrazione)

ll_anno_registrazione = f_anno_esercizio()

select max(num_non_conf)
into   :ll_num_registrazione
from   non_conformita
where  cod_azienda =:s_cs_xx.cod_azienda and
       anno_non_conf = :ll_anno_registrazione;
if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

ls_cod_prodotto = dw_call_center_lista.getitemstring(dw_call_center_lista.getrow(),"cod_prodotto")
ls_cod_operaio = dw_call_center_lista.getitemstring(dw_call_center_lista.getrow(),"cod_operaio_registra")
ldt_data_registrazione = datetime(today(), 00:00:00)
ls_cod_cliente = dw_call_center_lista.getitemstring(dw_call_center_lista.getrow(),"cod_cliente")
ls_des_anomalia_1 = dw_call_center_lista.getitemstring(dw_call_center_lista.getrow(),"des_anomalia_1")
if isnull(ls_cod_prodotto) then
	ls_flag_prod_codificato = "N"
else
	ls_flag_prod_codificato = "S"
end if

insert into non_conformita
		(cod_azienda,
		anno_non_conf,
		num_non_conf,
		flag_tipo_nc,
		cod_operaio,
		data_creazione,
		data_scadenza,
		cod_cliente,
		flag_prod_codificato,
		cod_prodotto)
values
		(:s_cs_xx.cod_azienda,
		:ll_anno_registrazione,
		:ll_num_registrazione,
		'V',
		:ls_cod_operaio,
		:ldt_data_registrazione,
		null,
		:ls_cod_cliente,
		:ls_flag_prod_codificato,
		:ls_cod_prodotto);
if sqlca.sqlcode <> 0  then
	g_mb.messagebox("OMNIA","Errore in fase di creazione N.C. Dettaglio errore: " + sqlca.sqlerrtext)
	return
	rollback;
end if

update tab_richieste
set    anno_non_conf = :ll_anno_registrazione, 
       num_non_conf = :ll_num_registrazione
where cod_azienda   = :s_cs_xx.cod_azienda and
      anno_registrazione = :ll_anno_richiesta and
		num_registrazione  = :ll_num_richiesta;
if sqlca.sqlcode <> 0  then
	g_mb.messagebox("OMNIA","Errore in fase di aggiornamento richiesta selezionata con il numero della N.C. generata. Dettaglio errore: " + sqlca.sqlerrtext)
	return
	rollback;
end if

commit;

g_mb.messagebox("OMNIA","E' stata generata con successo la Non Conformità " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))

end event

type cb_reset from commandbutton within w_call_center
integer x = 2729
integer y = 944
integer width = 389
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;//gestire il reset del filtro
long			ll_null
integer		li_null
datetime		ldt_null
string			ls_null

setnull(ll_null)
setnull(li_null)
setnull(ldt_null)
setnull(ls_null)

dw_ricerca.setitem(1,"anno_registrazione", li_null)
dw_ricerca.setitem(1,"num_registrazione", ll_null)

dw_ricerca.setitem(1,"flag_chiusa", "N")
dw_ricerca.setitem(1,"flag_obsoleto", "T")
dw_ricerca.setitem(1,"flag_straordinario", "T")

dw_ricerca.setitem(1,"cod_operaio_registrazione", ls_null)
dw_ricerca.setitem(1,"cod_richiedente", ls_null)
//dw_ricerca.setitem(1,"cod_operaio_risolve", ls_null)
dw_ricerca.setitem(1,"cod_area_chiamata", ls_null)
dw_ricerca.setitem(1,"cod_classe_richiesta", ls_null)
dw_ricerca.setitem(1,"cod_tipologia_richiesta", ls_null)

dw_ricerca.setitem(1,"flag_competenza", "T")
dw_ricerca.setitem(1,"flag_gen_manutenzioni", "T")

dw_ricerca.setitem(1,"cod_documento", ls_null)
dw_ricerca.setitem(1,"numeratore_documento", ls_null)
dw_ricerca.setitem(1,"anno_documento", li_null)
dw_ricerca.setitem(1,"num_documento", ll_null)
dw_ricerca.setitem(1,"cod_operaio_incaricato", ls_null)
dw_ricerca.setitem(1,"cod_divisione", ls_null)
dw_ricerca.setitem(1,"chiamata_cliente", ls_null)
dw_ricerca.setitem(1,"cod_cat_risorsa_esterna", ls_null)
dw_ricerca.setitem(1,"cod_risorsa_esterna", ls_null)
dw_ricerca.setitem(1,"cod_area_aziendale", ls_null)
dw_ricerca.setitem(1,"nome_richiedente", ls_null)
dw_ricerca.setitem(1,"cod_tipo_richiesta", ls_null)
dw_ricerca.setitem(1,"cod_attrezzatura", ls_null)
dw_ricerca.setitem(1,"data_reg_inizio", ldt_null)
dw_ricerca.setitem(1,"data_reg_fine", ldt_null)


end event

type cb_ricerca from commandbutton within w_call_center
integer x = 2322
integer y = 944
integer width = 389
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;
dw_call_center_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type dw_call_center_lista from uo_cs_xx_dw within w_call_center
event ue_riposiziona_riga ( )
event ue_noscroll pbm_vscroll
event ue_invio_mail ( long al_row )
integer x = 146
integer y = 44
integer width = 3433
integer height = 884
integer taborder = 10
string dataobject = "d_call_center_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_riposiziona_riga();if il_row > 0 then
	change_dw_current()
	scrolltorow(il_row)
	il_row = 0
end if
end event

event ue_noscroll;return 1
end event

event ue_invio_mail(long al_row);uo_outlook luo_outlook
string ls_destinatari[], ls_allegati[], ls_msg, ls_tecnico, ls_email, ls_des_richiesta
string ls_oggetto, ls_messaggio, ls_nome_tecnico
integer li_ret
long ll_row, ll_cod_richiedente

if dw_1.dataobject = "d_call_center_6" then
	//###################################
	//gestione nostra interna segnalazioni e richieste clienti
	luo_outlook = create uo_outlook
	
	ll_row = al_row
	
	ls_oggetto = "Apertura Ticket n°"+&
					string(getitemnumber(ll_row,"anno_registrazione")) + "/" + string(getitemnumber(ll_row,"num_registrazione"))
	ls_messaggio = "Gentile Cliente,"+char(13) + &
								"Le comunichiamo che è stato aperto un ticket relativo alla Sua segnalazione."
	
	ls_des_richiesta = getitemstring(ll_row, "des_richiesta")		
	if not isnull(ls_des_richiesta) and ls_des_richiesta<> "" then ls_messaggio += char(13)+char(13)+"Testo segnalazione:"+char(13) + "'" + ls_des_richiesta+"'"
	
	ll_cod_richiedente = getitemnumber(ll_row, "cod_richiedente")
	if isnull(ll_cod_richiedente) then
		g_mb.messagebox("CS_TEAM", "Non è stato possibile inviare un e-mail al richiedente in quanto lo stesso non è stato specificato!", Exclamation!)
	else
		select email_richiedente
		into :ls_email
		from tab_richiedenti
		where cod_azienda = :s_cs_xx.cod_azienda and cod_richiedente=:ll_cod_richiedente;
		
		if isnull(ls_email) or ls_email = "" then
			g_mb.messagebox("CS_TEAM", "Non è stato possibile inviare un e-mail al richiedente in quanto l'indirizzo non è stato specificato in anagrafica!", Exclamation!)
		else
			ls_destinatari[1] = ls_email
			
			ls_email = ""
			
			ls_tecnico = getitemstring(ll_row,"cod_operaio_incaricato")
			
			if ls_tecnico <> "" and not isnull(ls_tecnico) then
				select utenti.e_mail, 
						anag_operai.nome + ' ' + anag_operai.cognome
				into :ls_email, :ls_nome_tecnico
				from utenti
				join anag_operai on anag_operai.cod_utente=utenti.cod_utente
				where anag_operai.cod_azienda = :s_cs_xx.cod_azienda and anag_operai.cod_operaio =:ls_tecnico;
				
				if isnull(ls_email) or ls_email = "" then
					g_mb.messagebox("CS_TEAM", "Non sarà inviata un e-mail al tecnico in quanto l'indirizzo non è stato specificato in anagrafica!", Exclamation!)
					
					//invio solo al richiedente
					// TODO
					/*li_ret = luo_outlook.uof_invio_outlook_redemption(0,"M", ls_oggetto, ls_messaggio, ls_destinatari[], ls_allegati[], true, ls_msg)*/
				else
					ls_destinatari[2] = ls_email
					
					if not isnull(ls_nome_tecnico) and ls_nome_tecnico<> "" then ls_messaggio +=char(13)+char(13)+"Il tecnico di riferimento è "+ ls_nome_tecnico
					
					//invio al richiedente e  al tecnico
					// TODO
					/*li_ret = luo_outlook.uof_invio_outlook_redemption(0,"M", ls_oggetto, ls_messaggio, ls_destinatari[], ls_allegati[], true, ls_msg)*/
				end if
			else
				//invio solo al richiedente
				// TODO
				/*li_ret = luo_outlook.uof_invio_outlook_redemption(0,"M", ls_oggetto, ls_messaggio, ls_destinatari[], ls_allegati[], true, ls_msg)*/
			end if								
		end if	
		
		destroy luo_outlook
	end if
	//###################################
end if
end event

event updatestart;call super::updatestart;string ls_flag_straordinario, ls_des_richiesta, ls_cod_tipo_richiesta, ls_des_tipo_richiesta, ls_note_idl,ls_des_tipo_manutenzione,ls_cod_guasto,ls_cod_attrezzatura
long ll_i,ll_anno_registrazione,ll_num_registrazione, ll_cont
datetime ldt_data_neutra
dwItemStatus l_statusRow

ldt_data_neutra = datetime(date("01/01/1900"),00:00:00)

for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		continue
	end if
	if getitemstatus(ll_i,0,primary!) = NewModified! or getitemstatus(ll_i,0,primary!) = New! or getitemstatus(ll_i,0,primary!) = DataModified! then
		if isnull(GetItemdatetime(ll_i, "data_registrazione")) or GetItemdatetime(ll_i, "data_registrazione") <= ldt_data_neutra then
			if g_mb.messagebox("Omnia","Data registrazione obbligatoria: la imposto con quella attuale?",question!,YesNo!,1) = 1 then
				// se non è stata impostata data e ora, li imposto automaticamente.
				setitem(ll_i, "data_registrazione",datetime(today(),00:00:00))
				setitem(getrow(), "ora_registrazione",datetime(date(1900, 1, 1),Now()))				
				//setitem(ll_i, "ora_registrazione",Now())
			else
				return 1
			end if
		end if
		
		setitem(ll_i, "cod_utente_modifica", s_cs_xx.cod_utente)
		setitem(ll_i, "data_ultima_modifica", datetime(today(), now()))
	end if
next

if ib_global_service then
	for ll_i = 1 to rowcount()
		if getitemstatus(ll_i,"cod_guasto",primary!) = DataModified! then
		
			ls_cod_guasto = getitemstring(ll_i, "cod_guasto")
			ls_cod_attrezzatura = getitemstring(ll_i, "cod_attrezzatura")
			
			select count(*)
			into :ll_cont
			from tab_guasti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_attrezzatura = :ls_cod_attrezzatura and
					cod_guasto = :ls_cod_guasto;
					
			if (isnull(ll_cont) or ll_cont = 0) and sqlca.sqlcode = 0 then
				
				insert into tab_guasti
					(cod_azienda,
					 cod_guasto,
					 cod_attrezzatura,
					 des_guasto)
						select 	cod_azienda,
								cod_guasto,
								:ls_cod_attrezzatura,
								des_guasto
						from 	tab_guasti_generici
						where 	cod_azienda = :s_cs_xx.cod_azienda and
								cod_guasto = :ls_cod_guasto;
			end if
		end if
		
		if getitemstatus(ll_i,"flag_richiesta_straordinaria",primary!) = DataModified! then
			
			ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione")
			ll_num_registrazione = getitemnumber(ll_i, "num_registrazione")
			ls_flag_straordinario = getitemstring(ll_i, "flag_richiesta_straordinaria")
			
			update manutenzioni
			set    flag_straordinario = :ls_flag_straordinario
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_reg_richiesta = :ll_anno_registrazione and
					 num_reg_richiesta  = :ll_num_registrazione ;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Omnia", "Errore aggiornamento flag_straordinario nella manutenzione collegata.~r~n"+sqlca.sqlerrtext)
				return 1
			end if
			
			// -- stefanop 06/05/2009: se il flag = S allora chiedo ed invio mail (sie solari, modifiche intranet)
			//if getitemstring(ll_i, "flag_richiesta_straordinaria") = "S" then
			
			//12/07/2010 se il parametro aziendale flag  (NMS) vale a S non chiedere nè inviare email (chiesto da davide Baggio)
			if getitemstring(ll_i, "flag_richiesta_straordinaria") = "S" and is_NMS<>"S" then
				if g_mb.messagebox("Omnia", "Si vuole inviare una mail di avviso?", Question!, YesNo!) = 1 then
					s_cs_xx.parametri.parametro_s_1 = "L"
					s_cs_xx.parametri.parametro_s_4 = ""
					s_cs_xx.parametri.parametro_s_5 = "Richiesta straordinaria " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
					s_cs_xx.parametri.parametro_s_6 = "Richiesta straordinaria " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
					s_cs_xx.parametri.parametro_s_13 = string(ll_anno_registrazione)
					s_cs_xx.parametri.parametro_s_14 = string(ll_num_registrazione)
					s_cs_xx.parametri.parametro_s_15 = "N"
					s_cs_xx.parametri.parametro_b_1 = true
										
					openwithparm( w_invio_messaggi, 0)	
				end if
			end if
			// ----------------------
			
		end if		
		
		if getitemstatus(ll_i,"des_richiesta",primary!) = DataModified! then
			
			ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione")
			ll_num_registrazione = getitemnumber(ll_i, "num_registrazione")
			ls_cod_tipo_richiesta = getitemstring(ll_i, "cod_tipo_richiesta")
			ls_des_richiesta = getitemstring(ll_i, "des_richiesta")
			
			select des_tipo_manutenzione
			into   :ls_des_tipo_manutenzione
			from   tab_tipi_richieste
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_richiesta = :ls_cod_tipo_richiesta;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Omnia", "Errore in ricerca tipo manutenzione nella tab_tipi_richieste."+sqlca.sqlerrtext)
				return 1
			end if
			
			if isnull(ls_des_tipo_manutenzione) then
				ls_note_idl = "RICHIESTA:~r~n"
			else
				ls_note_idl = ls_des_tipo_manutenzione + ":~r~n"
			end if
			
			if not isnull(ls_des_richiesta) and len(ls_des_richiesta) > 0 then
				ls_note_idl += ls_des_richiesta
			end if
			
			
			update manutenzioni
			set    note_idl = :ls_note_idl
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_reg_richiesta = :ll_anno_registrazione and
					 num_reg_richiesta  = :ll_num_registrazione ;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Omnia", "Errore aggiornamento istruzioni operative nella manutenzione collegata.~r~n"+sqlca.sqlerrtext)
				return 1
			end if
				
		end if
		
//		//inserire qui il codice evento post invio mail
//		if dw_1.dataobject = "d_call_center_6" then
//			l_statusRow = GetItemStatus( ll_i, 0, Primary!)
//			if l_statusRow = NewModified! then
//				this.event post ue_invio_mail(ll_i)
//			end if
//		end if				
	next
	
end if

for ll_i = 1 to this.deletedcount()
	
	ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione", delete!, true)
	ll_num_registrazione = getitemnumber(ll_i, "num_registrazione", delete!, true)
	
	//messagebox("DEBUG","DELETED " + string(ll_anno_registrazione) + "\" + string(ll_num_registrazione))
	
	select count(*)
	into   :ll_cont
	from   manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_richiesta = :ll_anno_registrazione and
			 num_reg_richiesta = :ll_num_registrazione;
			 
	if not isnull(ll_cont) and ll_cont > 0 then
		g_mb.messagebox("OMNIA","Richiesta " + string(ll_anno_registrazione) + "\" + string(ll_num_registrazione) + "~r~nEsistono delle manutenzioni associate a questa richiesta.~r~nE' necessario cancellare prima quelle!", Exclamation!)
		return -1
	end if

	delete tab_richieste_blob
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
          num_registrazione = :ll_num_registrazione;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Richiesta " + string(ll_anno_registrazione) + "\" + string(ll_num_registrazione) + "~r~nErrore sulla cancellazione documenti-anomalie. Errore DB:" + sqlca.sqlerrtext,stopsign!)
		return 1
		rollback;
	end if
	
	// stefano 10/05/2010: elimino fornitori associati alla richiesta
	if dw_1.dataobject="d_call_center_8" then
		delete from tab_richieste_fornitori
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
	end if
	// ----
next


end event

event pcd_modify;call super::pcd_modify;cb_blob.enabled = false
cb_genera_nc.enabled = false

if ib_global_service then
	cb_ol_lav_richiesta.enabled = false
else
	cb_ol_lav_richiesta.visible = false
end if


iuo_richieste.uof_new_modify(dw_1,true)
iuo_richieste.uof_new_modify(dw_2,true)
iuo_richieste.uof_new_modify(dw_3,true)
iuo_richieste.uof_new_modify(dw_4,true)
iuo_richieste.uof_new_modify(dw_5,true)




end event

event pcd_new;call super::pcd_new;string ls_cod_operaio

if dw_1.dataobject = "d_call_center_6" or dw_1.dataobject = "d_call_center_10" then
	
	select cod_operaio
	into :ls_cod_operaio
	from anag_operai
	where cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode = 0 then
		if ls_cod_operaio <> "" and not isnull(ls_cod_operaio) then
			setitem(getrow(),"cod_operaio_registrazione", ls_cod_operaio)
		end if		
	end if
	
end if


cb_blob.enabled = false
cb_genera_nc.enabled = false

if ib_global_service then
	cb_ol_lav_richiesta.enabled = false
else
	cb_ol_lav_richiesta.visible = false
end if


setitem(getrow(), "data_registrazione",datetime(today(),00:00:00))
setitem(getrow(), "ora_registrazione",datetime(date(1900, 1, 1),Now()))
// stefano 11/05/2010
if dw_1.dataobject = "d_call_center_8" then
	setitem(getrow(), "data_passaggio_chiamata",datetime(today(),00:00:00))
	setitem(getrow(), "ora_passaggio_chiamata",datetime(date(1900, 1, 1),Now()))
end if

//setitem(getrow(), "ora_registrazione",Now())

if ib_proviene_da_nc then
	setitem(getrow(), "anno_non_conf", il_anno_nc)
	setitem(getrow(), "num_non_conf" , il_num_nc)
	setitem(getrow(), "des_richiesta" ,"Richiesta da non conformità n. " + string(il_anno_nc) + "/" + string(il_num_nc))
end if

iuo_richieste.uof_new_modify(dw_1,true)
iuo_richieste.uof_new_modify(dw_2,true)
iuo_richieste.uof_new_modify(dw_3,true)
iuo_richieste.uof_new_modify(dw_4,true)
iuo_richieste.uof_new_modify(dw_5,true)

// impostazione default campi (tolti da initial value della DW per evitare problemi in chiusura window con riga vuota)
setitem(getrow(),"flag_tipo_richiesta","M")
setitem(getrow(),"flag_richiedente_codificato","S")
setitem(getrow(),"flag_comunicato_risposta","N")
setitem(getrow(),"flag_richiesta_chiusa","N")
setitem(getrow(),"flag_richiesta_obsoleta","N")
setitem(getrow(),"flag_richiesta_straordinaria","N")
setitem(getrow(),"flag_competenza","S")
setitem(getrow(),"flag_gen_manutenzione","N")
setitem(getrow(),"flag_richiesta_reso","N")
setitem(getrow(),"cod_utente_creazione", s_cs_xx.cod_utente)

// Impostazione della commessa di default
// fatto per ALPIQ  20.05.2011
if dw_1.dataobject = "d_call_center_10" then
	 setitem(getrow(),"cod_divisione","001")
	 setitem(getrow(),"cod_tipo_richiesta","001")
	 setitem(getrow(),"flag_richiedente_codificato","N")
end if

end event

event pcd_retrieve;call super::pcd_retrieve;integer		li_anno_registrazione, li_anno_documento
long			l_error, ll_num_registrazione, ll_num_documento
string			ls_flag_chiusa, ls_flag_obsoleto, ls_flag_straordinario, ls_cod_operaio_registrazione, ls_cod_richiedente, &
				ls_cod_operaio_risolve, ls_cod_area_chiamata, ls_cod_classe_richiesta, ls_cod_tipologia_richiesta, ls_flag_competenza, &
				ls_flag_gen_manutenzioni, ls_cod_documento, ls_numeratore_documento, ls_cod_operaio_incaricato, ls_cod_divisione, ls_chiamata_cliente,&
				ls_cod_cat_risorsa_esterna, ls_cod_risorsa_esterna, ls_cod_area_aziendale, ls_nome_richiedente, ls_cod_tipo_richiesta, &
				ls_cod_attrezzatura, ls_sql_new, ls_barcode, ls_errore
boolean		lb_barcode
datetime		ldt_data_reg_inizio, ldt_data_reg_fine

dw_ricerca.accepttext()
lb_barcode = false

li_anno_registrazione = dw_ricerca.getitemnumber(1,"anno_registrazione")
ll_num_registrazione = dw_ricerca.getitemnumber(1,"num_registrazione")
ls_flag_chiusa = dw_ricerca.getitemstring(1,"flag_chiusa")
ls_flag_obsoleto = dw_ricerca.getitemstring(1,"flag_obsoleto")
ls_flag_straordinario = dw_ricerca.getitemstring(1,"flag_straordinario")
ls_cod_operaio_registrazione = dw_ricerca.getitemstring(1,"cod_operaio_registrazione")
ls_cod_richiedente = dw_ricerca.getitemstring(1,"cod_richiedente")
ls_cod_area_chiamata = dw_ricerca.getitemstring(1,"cod_area_chiamata")
ls_cod_classe_richiesta = dw_ricerca.getitemstring(1,"cod_classe_richiesta")
ls_cod_tipologia_richiesta = dw_ricerca.getitemstring(1,"cod_tipologia_richiesta")
ls_flag_competenza = dw_ricerca.getitemstring(1,"flag_competenza")
ls_flag_gen_manutenzioni = dw_ricerca.getitemstring(1,"flag_gen_manutenzioni")
ls_cod_documento = dw_ricerca.getitemstring(1,"cod_documento")
ls_numeratore_documento = dw_ricerca.getitemstring(1,"numeratore_documento")
li_anno_documento = dw_ricerca.getitemnumber(1,"anno_documento")
ll_num_documento = dw_ricerca.getitemnumber(1,"num_documento")
ls_cod_operaio_incaricato = dw_ricerca.getitemstring(1,"cod_operaio_incaricato")
ls_cod_divisione = dw_ricerca.getitemstring(1,"cod_divisione")
ls_chiamata_cliente = dw_ricerca.getitemstring(1,"chiamata_cliente")
ls_cod_cat_risorsa_esterna = dw_ricerca.getitemstring(1,"cod_cat_risorsa_esterna")
ls_cod_risorsa_esterna = dw_ricerca.getitemstring(1,"cod_risorsa_esterna")
ls_cod_area_aziendale = dw_ricerca.getitemstring(1,"cod_area_aziendale")
ls_nome_richiedente = dw_ricerca.getitemstring(1,"nome_richiedente")
ls_cod_tipo_richiesta = dw_ricerca.getitemstring(1,"cod_tipo_richiesta")
ls_cod_attrezzatura = dw_ricerca.getitemstring(1,"cod_attrezzatura")
ls_barcode = dw_ricerca.getitemstring(1,"barcode")

ldt_data_reg_inizio = dw_ricerca.getitemdatetime(1,"data_reg_inizio")
ldt_data_reg_inizio = datetime(date(ldt_data_reg_inizio), 00:00:00)

ldt_data_reg_fine = dw_ricerca.getitemdatetime(1,"data_reg_fine")
ldt_data_reg_fine = datetime(date(ldt_data_reg_fine), 23:59:59)

ls_sql_new = is_sql_base
ls_sql_new += " where cod_azienda='"+s_cs_xx.cod_azienda+"' "

// stefanop 10/04/2012: spec. "ordini_lavoro_richieste" alpiq
if not isnull(ls_barcode) and ls_barcode <> "" then
	wf_get_barcode(ls_barcode, li_anno_registrazione, ll_num_registrazione, ls_errore)
	lb_barcode = true
	
	if not isnull(ls_errore) or ls_errore <> "" then
		g_mb.error(ls_errore)
		return -1
	end if
end if
// ----

if li_anno_registrazione > 0 then
	ls_sql_new += " and anno_registrazione="+string(li_anno_registrazione)
end if
if ll_num_registrazione > 0 then
	ls_sql_new += " and num_registrazione="+string(ll_num_registrazione)
end if
if ls_flag_chiusa<>"T" then
	ls_sql_new += " and flag_richiesta_chiusa='"+ls_flag_chiusa+"'"
end if
if ls_flag_obsoleto<>"T" then
	ls_sql_new += " and flag_richiesta_obsoleta='"+ls_flag_obsoleto+"'"
end if
if ls_flag_straordinario<>"T" then
	ls_sql_new += " and flag_richiesta_straordinaria='"+ls_flag_straordinario+"'"
end if
if ls_flag_competenza<>"T" then
	ls_sql_new += " and flag_competenza='"+ls_flag_competenza+"'"
end if
if ls_flag_gen_manutenzioni<>"T" then
	ls_sql_new += " and flag_gen_manutenzione='"+ls_flag_gen_manutenzioni+"'"
end if
if ls_cod_operaio_registrazione<>"" and not isnull(ls_cod_operaio_registrazione) then
	ls_sql_new += " and cod_operaio_registrazione='"+ls_cod_operaio_registrazione+"'"
end if
if ls_cod_richiedente<>"" and not isnull(ls_cod_richiedente) then
	ls_sql_new += " and cod_richiedente='"+ls_cod_richiedente+"'"
end if
if ls_cod_area_chiamata<>"" and not isnull(ls_cod_area_chiamata) then
	ls_sql_new += " and cod_area_chiamata='"+ls_cod_area_chiamata+"'"
end if
if ls_cod_classe_richiesta<>"" and not isnull(ls_cod_classe_richiesta) then
	ls_sql_new += " and cod_classe_richiesta='"+ls_cod_classe_richiesta+"'"
end if
if ls_cod_tipologia_richiesta<>"" and not isnull(ls_cod_tipologia_richiesta) then
	ls_sql_new += " and cod_tipologia_richiesta='"+ls_cod_tipologia_richiesta+"'"
end if
if ls_cod_documento<>"" and not isnull(ls_cod_documento) then
	ls_sql_new += " and cod_documento='"+ls_cod_documento+"'"
end if
if ls_numeratore_documento<>"" and not isnull(ls_numeratore_documento) then
	ls_sql_new += " and numeratore_documento='"+ls_numeratore_documento+"'"
end if
if li_anno_documento>0 then
	ls_sql_new += " and anno_documento="+string(li_anno_documento)
end if
if ll_num_documento>0 then
	ls_sql_new += " and num_documento="+string(ll_num_documento)
end if
if ls_cod_operaio_incaricato<>"" and not isnull(ls_cod_operaio_incaricato) then
	ls_sql_new += " and cod_operaio_incaricato='"+ls_cod_operaio_incaricato+"'"
end if
if ls_cod_divisione<>"" and not isnull(ls_cod_divisione) then
	ls_sql_new += " and cod_divisione='"+ls_cod_divisione+"'"
end if
if ls_chiamata_cliente<>"" and not isnull(ls_chiamata_cliente) then
	ls_sql_new += " and nr_chiamata_cliente='"+ls_chiamata_cliente+"'"
end if
if ls_cod_cat_risorsa_esterna<>"" and not isnull(ls_cod_cat_risorsa_esterna) then
	ls_sql_new += " and cod_cat_risorse_esterne='"+ls_cod_cat_risorsa_esterna+"'"
end if
if ls_cod_risorsa_esterna<>"" and not isnull(ls_cod_risorsa_esterna) then
	ls_sql_new += " and cod_risorsa_esterna='"+ls_cod_risorsa_esterna+"'"
end if
if ls_cod_area_aziendale<>"" and not isnull(ls_cod_area_aziendale) then
	ls_sql_new += " and cod_area_aziendale='"+ls_cod_area_aziendale+"'"
end if
if ls_nome_richiedente<>"" and not isnull(ls_nome_richiedente) then
	ls_sql_new += " and nome_richiedente='"+ls_nome_richiedente+"'"
end if
if ls_cod_tipo_richiesta<>"" and not isnull(ls_cod_tipo_richiesta) then
	ls_sql_new += " and cod_tipo_richiesta='"+ls_cod_tipo_richiesta+"'"
end if
if ls_cod_attrezzatura<>"" and not isnull(ls_cod_attrezzatura) then
	ls_sql_new += " and cod_attrezzatura='"+ls_cod_attrezzatura+"'"
end if
if not isnull(ldt_data_reg_inizio) and year(date(ldt_data_reg_inizio)) > 1950 then
	ls_sql_new += " and data_registrazione>='"+string(ldt_data_reg_inizio, s_cs_xx.db_funzioni.formato_data)+"'"
end if
if not isnull(ldt_data_reg_fine) and year(date(ldt_data_reg_fine)) > 1950 then
	ls_sql_new += " and data_registrazione<='"+string(ldt_data_reg_fine, s_cs_xx.db_funzioni.formato_data)+"'"
end if

ls_sql_new = ls_sql_new + " " + is_order_by

dw_call_center_lista.setsqlselect(ls_sql_new)
l_Error = Retrieve(s_cs_xx.cod_azienda)

if l_error < 0 then
   PCCA.Error = c_Fatal
end if

if not lb_barcode then
	// funzionamento classico, non è stato impostato il barcode
	dw_folder_search.fu_selecttab(1)
	dw_call_center_lista.change_dw_current()
else
	// è stato impostato il barcode, controllo se la retrieve mi ha data almeno una riga ed eseguo la chiusura
	if l_Error = 1 then
		iuo_richieste.uof_genera_int(dw_call_center_lista)
	else
		g_mb.warning("Non è stata trovata nessuna richiesta per il barcode inserito o Richiesta già chiusa.")
	end if
	
	parent.postevent("ue_barcode_focus")
end if


end event

event pcd_save;call super::pcd_save;cb_blob.enabled = true
//cb_gen_manut.enabled = true
cb_genera_nc.enabled = true
//cb_manut_eseguite.enabled = true

if ib_global_service then
	cb_ol_lav_richiesta.enabled = true
else
	cb_ol_lav_richiesta.visible = false
end if
end event

event pcd_setkey;call super::pcd_setkey;integer li_anno_esercizio, li_giorno_anno
string ls_cod_tipo_richiesta, ls_flag_contatore_giornaliero, ls_cod_documento,ls_numeratore_documento
long  l_idx,li_valido_per,ll_num_registrazione, ll_min, ll_max, ll_num_documento
date ld_inizio
datetime ldt_data_registrazione

for l_idx = 1 to rowcount()
	
   if isnull(getitemstring(l_idx, "cod_azienda")) then
	   setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(getitemnumber(l_idx, "anno_registrazione")) or getitemnumber(l_idx, "anno_registrazione") = 0 then
		ldt_data_registrazione = getitemdatetime(l_idx, "data_registrazione")
		li_anno_esercizio = f_anno_esercizio()
		setitem(l_idx, "anno_registrazione", li_anno_esercizio)
	end if		
	
   if isnull(getitemnumber(l_idx, "num_registrazione")) or getitemnumber(l_idx, "num_registrazione") = 0 then
		
	   li_anno_esercizio = f_anno_esercizio()

		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tab_richieste
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 anno_registrazione = :li_anno_esercizio;
				 
		if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
			ll_num_registrazione = 1
		else
			ll_num_registrazione ++
		end if
		
		setitem(l_idx, "num_registrazione", ll_num_registrazione)
		
		// procedo con l'impostazione del numero di registro

		ls_cod_tipo_richiesta = getitemstring(l_idx, "cod_tipo_richiesta")
		
		if isnull(ls_cod_tipo_richiesta) or len(ls_cod_tipo_richiesta) < 1 then
			g_mb.messagebox("OMNIA","Impostare il tipo richiesta !~r~nIL NUMERO DI REGISTRO NON VERRA' IMPOSTATO.")
		end if
		
		select cod_documento, numeratore_documento, flag_contatore_giornaliero
		into   :ls_cod_documento, :ls_numeratore_documento, :ls_flag_contatore_giornaliero
		from   tab_tipi_richieste
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_richiesta = :ls_cod_tipo_richiesta;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in ricerca tipo richiesta")
			return -1
		end if
		
		setitem(l_idx, "cod_documento", ls_cod_documento)
		setitem(l_idx, "numeratore_documento", ls_numeratore_documento)		
		setitem(l_idx, "anno_documento", li_anno_esercizio)		
		ldt_data_registrazione = getitemdatetime(l_idx, "data_registrazione")
		
		choose case ls_flag_contatore_giornaliero
			case "S"
		
				ld_inizio = date( "01/01/" + string(li_anno_esercizio,"0000"))
				li_giorno_anno = daysafter(ld_inizio, date(ldt_data_registrazione) ) + 1
				ll_min = (li_giorno_anno * 1000) + 1
				ll_max = (li_giorno_anno + 1) * 1000
				ll_num_documento = 0
				
				select max(num_documento)
				into   :ll_num_documento
				from   tab_richieste
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_documento = :ls_cod_documento and
						 numeratore_documento = :ls_numeratore_documento and
						 anno_documento = :li_anno_esercizio and
						 num_documento >= :ll_min and
						 num_documento <= :ll_max;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("omnia","errore sul db: " + sqlca.sqlerrtext,stopsign!)
					return 
				end if
		
				if isnull(ll_num_documento) or ll_num_documento = 0 then 
					ll_num_documento = ll_min
				else 
					ll_num_documento++
				end if
				
			case else
				
			   li_anno_esercizio = getitemnumber(l_idx, "anno_registrazione")
	
				select max(num_documento)
				into   :ll_num_documento
				from   tab_richieste
				where  cod_azienda = :s_cs_xx.cod_azienda and 
				       cod_documento = :ls_cod_documento and
						 numeratore_documento = :ls_numeratore_documento and
				       anno_documento = :li_anno_esercizio;
						 
		   	if isnull(ll_num_documento) or ll_num_documento < 1 then
		      	ll_num_documento = 1
		   	else
		      	ll_num_documento ++
		   	end if
		
		end choose
		
		setitem(l_idx, "num_documento", ll_num_documento)
		
		update numeratori
		set    num_documento = :ll_num_documento
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_documento = :ls_cod_documento and
				 numeratore_documento = :ls_numeratore_documento and
				 anno_documento = :li_anno_esercizio;

		
		
	end if
next

end event

event pcd_view;call super::pcd_view;cb_blob.enabled = true
cb_genera_nc.enabled = true

if ib_global_service then
	cb_ol_lav_richiesta.enabled = true
else
	cb_ol_lav_richiesta.visible = false
end if

iuo_richieste.uof_new_modify(dw_1,false)
iuo_richieste.uof_new_modify(dw_2,false)
iuo_richieste.uof_new_modify(dw_3,false)
iuo_richieste.uof_new_modify(dw_4,false)
iuo_richieste.uof_new_modify(dw_5,false)


end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	long ll_row
	
	ll_row = getrow()

	if not isnull(dw_call_center_lista.getitemstring(ll_row,"cod_tipo_richiesta" ) ) then
		wf_blocca_folder(dw_call_center_lista.getitemstring(ll_row,"cod_tipo_richiesta" ))
	end if
	
	if isvalid(iuo_richieste) then
	
		iuo_richieste.uof_rowfocuschanged(dw_call_center_lista, ll_row )
		
	end if
	
	wf_retrieve_blob(ll_row)
	
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	
	iuo_richieste.uof_itemchanged(dw_call_center_lista, row, i_colname, i_coltext)
	
end if
end event

event ue_key;call super::ue_key;//messagebox("keycode",string(key) + " - " + string(keyflags))
end event

event pcd_delete;call super::pcd_delete;//long ll_anno_reg_richiesta, ll_num_reg_richiesta, ll_cont
//
//ll_anno_reg_richiesta = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"anno_registrazione")
//ll_num_reg_richiesta  = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(),"num_registrazione")
//				
//select count(*)
//into   :ll_cont
//from   manutenzioni
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 anno_reg_richiesta = :ll_anno_reg_richiesta and
//		 num_reg_richiesta = :ll_num_reg_richiesta;
//		 
//if not isnull(ll_cont) and ll_cont > 0 then
//	g_mb.messagebox("OMNIA", "Esistono delle manutenzioni associate a questa richiesta.~r~nE' necessario cancellare prima quelle!", Exclamation!)
//	return -1
//end if
end event

event retrieveend;call super::retrieveend;// --- FILTRO X DES COMMESSA --- {
	string ls_des_commessa
	
	ls_des_commessa = dw_ricerca.getitemstring(dw_ricerca.getrow(),"des_commessa")
	
	if not isnull(ls_des_commessa) then
		modify("cf_filtro_commessa.expression = ~"f_des_tabella(~~~"anag_clienti~~~",~~~"cod_azienda = '~~~" + cod_azienda + ~~~"' and cod_cliente = (select cod_cliente from anag_divisioni where cod_azienda = '~~~" + cod_azienda + ~~~"' and cod_divisione = '~~~" + cod_divisione +~~~"')~~~",~~~"rag_soc_1~~~")~"")
		dw_call_center_lista.setfilter("cf_filtro_commessa like '" + ls_des_commessa + "'")
	else
		dw_call_center_lista.object.cf_filtro_commessa.expression = "''"
		dw_call_center_lista.setfilter("")
	end if
	
	dw_call_center_lista.filter()
// --- FILTRO X DES COMMESSA --- }


end event

event sqlpreview;call super::sqlpreview;//messagebox("SQL PREVIEW",sqlsyntax)
end event

event updateend;call super::updateend;string ls_oggetto, ls_messaggio, ls_numeratore_documento, ls_richiesta, ls_des_richiesta
long	ll_numero, ll_anno
datetime ldt_data_registrazione

// -- stefanop 03/07/2009: se è possibile invio la mail (ticket 2009/158)
if ib_send_mail then

	ll_anno = getitemnumber(getrow(), "anno_documento")
	ls_numeratore_documento = getitemstring(getrow(), "numeratore_documento")
	ll_numero = getitemnumber(getrow(), "num_documento")
	ldt_data_registrazione = getitemdatetime(getrow(), "data_registrazione")
	ls_des_richiesta = getitemstring(getrow(),"des_richiesta")
	
	ls_richiesta = string(ll_anno) + "/" + ls_numeratore_documento + "/" + string(ll_numero)
	
	if rowsinserted > 0 then
		ls_oggetto = " (OMNIA) NUOVA RICHIESTA NUM.: "
	end if
	
	if rowsupdated > 0 then
		ls_oggetto = " (OMNIA) MODIFICA RICHIESTA NUM.: "
	end if
	
	ls_oggetto += ls_richiesta + " DEL " + string(ldt_data_registrazione, "dd/mm/yyyy")
	ls_messaggio += ls_oggetto + "~r~n" + ls_des_richiesta
	
	wf_invia_mail(ls_oggetto, ls_messaggio)
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_tipo_richiesta

if getrow() >0 then
	ls_cod_tipo_richiesta = getitemstring(getrow(), "cod_tipo_richiesta")
	
	if isnull(ls_cod_tipo_richiesta) or ls_cod_tipo_richiesta="" then
		g_mb.messagebox("OMNIA","Tipo richiesta obbligatoria!")
		dw_1.setcolumn("cod_tipo_richiesta")
		pcca.error = c_fatal
		return
	end if
end if
end event

type dw_folder_search from u_folder within w_call_center
integer y = 20
integer width = 3621
integer height = 1036
integer taborder = 10
end type

type dw_ricerca from u_dw_search within w_call_center
event ue_key pbm_dwnkey
integer x = 137
integer y = 40
integer width = 3451
integer height = 980
integer taborder = 20
string dataobject = "d_call_center_ricerca"
boolean border = false
end type

event ue_key;if key = keyenter! then
	cb_ricerca.triggerevent("clicked")
end if
end event

event itemchanged;call super::itemchanged;string	ls_null
date		ld_data_inizio, ld_data_fine, ld_data_neutra


if getcolumnname() = "" then return

choose case getcolumnname()
	case "cod_divisione"
		
		f_po_loaddddw_dw(dw_ricerca, &
						  "cod_area_chiamata", &
						  sqlca, &
						  "tab_aree_chiamate", &
						  "cod_area_chiamata", &
						  "des_area_chiamata", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + data + "'")
						  
		f_po_loaddddw_sort(dw_ricerca, &
							  "cod_richiedente", &
							  sqlca, &
							  "tab_richiedenti", &
							  "cod_richiedente", &
							  "nominativo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + data + "'", &
							  " nominativo ASC ")
		
	case "cod_cat_risorsa_esterna"
		
			setnull(ls_null)
			setitem(row,"cod_risorsa_esterna",ls_null)
		
	// stefanop 10/04/2012: spec. "ordini_lavoro_richieste" alpiq
	case "barcode"
		parent.postevent("pc_retrieve")
		
end choose
end event

event itemfocuschanged;call super::itemfocuschanged;if isvalid(dwo) then
	
	if dwo.name = "cod_risorsa_esterna" then
		
		f_po_loaddddw_sort(dw_ricerca, &
                 "cod_risorsa_esterna", &
                 sqlca, &
                 "anag_risorse_esterne", &
                 "cod_risorsa_esterna", &
                 "descrizione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + getitemstring(getrow(),"cod_cat_risorsa_esterna") + "'", &
					  " descrizione ASC ")
					  
	end if

end if
end event

event buttonclicked;call super::buttonclicked;////pesonalizzazione per uso nostro interno
//
////la d_call_center_ricerca è stata personalizzata per inserire il filtro per cliente
////quindi questo pulsante starà solo sulla w_call_center quando si lancia il nostro gestionale interno
//
//if row > 0 then
//	choose case dwo.name
//		case "b_cliente_ric"
//			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
//			s_cs_xx.parametri.parametro_uo_dw_search = this
//			s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
//			
//			if not isvalid(w_clienti_ricerca) then
//				window_open(w_clienti_ricerca, 0)
//			end if			
//			w_clienti_ricerca.show()
//			
//	end choose
//end if
//
////-------------------------------------------------------
//

choose case dwo.name
	case "b_ricerca_divisione"
		guo_ricerca.uof_ricerca_divisione(dw_ricerca, "cod_divisione")
		
	case "b_ricerca_area_aziendale"
		guo_ricerca.uof_ricerca_area_aziendale(dw_ricerca, "cod_area_aziendale")
		
	case "b_ricerca_attrezz"
		guo_ricerca.uof_ricerca_attrezzatura(dw_ricerca, "cod_attrezzatura")
		
end choose
end event

type cb_ol_lav_richiesta from commandbutton within w_call_center
integer x = 2322
integer y = 944
integer width = 389
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "O.Lav."
end type

event clicked;decimal ld_null


if not ib_global_service then return

if dw_call_center_lista.rowcount()>0 then
	setnull( ld_null )
	
	setnull(s_cs_xx.parametri.parametro_s_10)
	s_cs_xx.parametri.parametro_d_1_a[1] = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(), "anno_registrazione")
	s_cs_xx.parametri.parametro_d_1_a[2] = dw_call_center_lista.getitemnumber(dw_call_center_lista.getrow(), "num_registrazione")
	
	if s_cs_xx.parametri.parametro_d_1_a[1]>0 and s_cs_xx.parametri.parametro_d_1_a[2] > 0 then
		
		//apri una window (è nel global service) che consente all'utente di scegliere il tipo report
		s_cs_xx.parametri.parametro_s_1_a[1] = "G"
		s_cs_xx.parametri.parametro_s_2_a[1] = "Generico"
		s_cs_xx.parametri.parametro_s_1_a[2] = "T"
		s_cs_xx.parametri.parametro_s_2_a[2] = "Modello per Richieste Telefoniche"
		s_cs_xx.parametri.parametro_s_1_a[3] = "S"
		s_cs_xx.parametri.parametro_s_2_a[3] = "Modello per Richieste Scritte"
		s_cs_xx.parametri.parametro_s_1_a[4] = "R"
		s_cs_xx.parametri.parametro_s_2_a[4] = "Preparazione Impianti"
		s_cs_xx.parametri.parametro_s_1_a[5] = "U"
		s_cs_xx.parametri.parametro_s_2_a[5] = "Strumentale"
		s_cs_xx.parametri.parametro_s_1_a[6] = "Q"
		s_cs_xx.parametri.parametro_s_2_a[6] = "Preparaz. Impianti con QRcode (necessaria connessione internet!)"
		s_cs_xx.parametri.parametro_s_1_a[7] = "D"
		s_cs_xx.parametri.parametro_s_2_a[7] = "Strumentale DUVRI"
		
		window_type_open(w_cs_xx_risposta, "w_sel_tipo_report_ol_ric", 0 )
		
		s_cs_xx.parametri.parametro_s_10 = message.stringparm
		
		if isnull(s_cs_xx.parametri.parametro_s_10) or s_cs_xx.parametri.parametro_s_10="" then
			s_cs_xx.parametri.parametro_s_10 = "G"
		end if
		
		//layout report attualmente gestiti
		//	1	report generico 														(G)
		//	2	rich. telefoniche														(T)
		//	3	rich. scritte																(S)
		//	4	preparazione impianti (VERONA FIERA)							(R)
		//	5	strumentale (attrezzatura)											(U)
		//	6	preparazione impianti con qrcode (VERONA FIERA)			(Q)
		//	7	strumentale DUVRI (VERONA FIERA)								(D)
		
		//questo report si trova nel global service
		window_type_open(w_cs_xx_risposta, "w_report_ordine_lavoro_richieste", -1 )

	else
		setnull(s_cs_xx.parametri.parametro_s_10)
		s_cs_xx.parametri.parametro_d_1_a[1] = ld_null
		s_cs_xx.parametri.parametro_d_1_a[2] = ld_null
	end if

end if







end event

