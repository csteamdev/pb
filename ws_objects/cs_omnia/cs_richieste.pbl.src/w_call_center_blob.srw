﻿$PBExportHeader$w_call_center_blob.srw
forward
global type w_call_center_blob from w_cs_xx_principale
end type
type cb_documento from commandbutton within w_call_center_blob
end type
type dw_call_center_blob from uo_cs_xx_dw within w_call_center_blob
end type
end forward

global type w_call_center_blob from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2267
integer height = 856
cb_documento cb_documento
dw_call_center_blob dw_call_center_blob
end type
global w_call_center_blob w_call_center_blob

on w_call_center_blob.create
int iCurrent
call super::create
this.cb_documento=create cb_documento
this.dw_call_center_blob=create dw_call_center_blob
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_documento
this.Control[iCurrent+2]=this.dw_call_center_blob
end on

on w_call_center_blob.destroy
call super::destroy
destroy(this.cb_documento)
destroy(this.dw_call_center_blob)
end on

event pc_setwindow;call super::pc_setwindow;dw_call_center_blob.set_dw_key("cod_azienda")
dw_call_center_blob.set_dw_key("anno_registrazione")
dw_call_center_blob.set_dw_key("num_registrazione")

dw_call_center_blob.set_dw_options(sqlca, &
											i_openparm, &
											c_scrollparent, &
											c_default)

iuo_dw_main = dw_call_center_blob
end event

type cb_documento from commandbutton within w_call_center_blob
integer x = 1815
integer y = 656
integer width = 402
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Documento"
end type

event clicked;long    ll_i, ll_progressivo, ll_prog_mimetype, ll_anno_registrazione, ll_num_registrazione
integer li_risposta
string  ls_db
transaction sqlcb
blob    lbl_null

setnull(lbl_null)

ll_i = dw_call_center_blob.getrow()

if ll_i < 1 then return

ll_progressivo = dw_call_center_blob.getitemnumber(ll_i, "progressivo")

ll_anno_registrazione = dw_call_center_blob.i_parentdw.getitemnumber(dw_call_center_blob.i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = dw_call_center_blob.i_parentdw.getitemnumber(dw_call_center_blob.i_parentdw.i_selectedrows[1], "num_registrazione")
ll_prog_mimetype = dw_call_center_blob.getitemnumber( ll_i, "prog_mimetype")

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       tab_richieste_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_registrazione = :ll_anno_registrazione and 
				  num_registrazione = :ll_num_registrazione and
				  progressivo = :ll_progressivo
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

	
	destroy sqlcb;
	
else
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       tab_richieste_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_registrazione = :ll_anno_registrazione and 
				  num_registrazione = :ll_num_registrazione and
				  progressivo = :ll_progressivo;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if


// *** se il blob non è nullo ed esiste il prog mimetype: sono documenti intranet
// *** se il blob non è nullo ma il progressivo si: sono documenti client/server
// *** se il blob è nullo: procedo con l'inserimento intranet

if isnull(s_cs_xx.parametri.parametro_bl_1) or ( not isnull(s_cs_xx.parametri.parametro_bl_1) and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 ) then

	s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype
	
	window_open(w_ole_documenti, 0)
	
	ll_prog_mimetype = s_cs_xx.parametri.parametro_d_1
	
	if not isnull(s_cs_xx.parametri.parametro_bl_1) and len(s_cs_xx.parametri.parametro_bl_1) > 0  and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			updateblob tab_richieste_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  anno_registrazione = :ll_anno_registrazione and 
						  num_registrazione = :ll_num_registrazione and
						  progressivo = :ll_progressivo
			using      sqlcb;
				
			if sqlcb.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", sqlcb.sqlerrtext)						
			end if	
	
			destroy sqlcb;
		
		else
		
			updateblob tab_richieste_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  anno_registrazione = :ll_anno_registrazione and 
						  num_registrazione = :ll_num_registrazione and
						  progressivo = :ll_progressivo;
					
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Prova", sqlca.sqlerrtext)
			end if	
			
		end if
					
		update     tab_richieste_blob
		set        prog_mimetype = :ll_prog_mimetype
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_registrazione = :ll_anno_registrazione and 
					  num_registrazione = :ll_num_registrazione and
					  progressivo = :ll_progressivo;
					  
		commit;
		
		parent.triggerevent("pc_retrieve")

		
//		dw_call_center_blob.setitem( ll_i, "prog_mimetype", ll_prog_mimetype)
//		
//		parent.triggerevent("pc_save")
//		
//		parent.triggerevent("pc_view")
		
	end if
else
	
	window_open(w_ole, 0)
	
	if not isnull(s_cs_xx.parametri.parametro_bl_1) then
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			updateblob tab_richieste_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  anno_registrazione = :ll_anno_registrazione and 
						  num_registrazione = :ll_num_registrazione and
						  progressivo = :ll_progressivo
			using      sqlcb;
			
			destroy sqlcb;
			
		else
			
			updateblob tab_richieste_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  anno_registrazione = :ll_anno_registrazione and 
						  num_registrazione = :ll_num_registrazione and
						  progressivo = :ll_progressivo;
	
		end if
		
		commit;
		
	end if
	
end if

return

end event

type dw_call_center_blob from uo_cs_xx_dw within w_call_center_blob
integer width = 2217
integer height = 640
integer taborder = 10
string dataobject = "d_call_center_blob"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_progressivo

ll_anno_registrazione = dw_call_center_blob.i_parentdw.getitemnumber(dw_call_center_blob.i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = dw_call_center_blob.i_parentdw.getitemnumber(dw_call_center_blob.i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
	if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or this.getitemnumber(ll_i, "anno_registrazione") < 1 then	
		setitem(getrow(), "anno_registrazione", ll_anno_registrazione)
	end if
	
	if isnull(this.getitemnumber(ll_i, "num_registrazione")) or this.getitemnumber(ll_i, "num_registrazione") < 1 then	
		setitem(getrow(), "num_registrazione", ll_num_registrazione)
	end if
	
	if isnull(this.getitemnumber(ll_i, "progressivo")) or this.getitemnumber(ll_i, "progressivo") < 1 then	
	
		select max(progressivo)
		into   :ll_progressivo
		from   tab_richieste_blob
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione  and
				 num_registrazione = :ll_num_registrazione ;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return 
		end if

		if isnull(ll_progressivo) or ll_progressivo = 0 then 
			ll_progressivo = 1
		else 
			ll_progressivo++
		end if
		setitem(getrow(), "progressivo", ll_progressivo)
	end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;long l_Error, ll_anno_registrazione, ll_num_registrazione, ll_progressivo

ll_anno_registrazione = dw_call_center_blob.i_parentdw.getitemnumber(dw_call_center_blob.i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = dw_call_center_blob.i_parentdw.getitemnumber(dw_call_center_blob.i_parentdw.i_selectedrows[1], "num_registrazione")


l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_new;call super::pcd_new;cb_documento.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_documento.enabled = false
end event

event pcd_view;call super::pcd_view;cb_documento.enabled = true
end event

