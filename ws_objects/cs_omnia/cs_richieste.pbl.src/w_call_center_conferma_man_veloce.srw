﻿$PBExportHeader$w_call_center_conferma_man_veloce.srw
$PBExportComments$Finestra modale per conferma Manutenzioni
forward
global type w_call_center_conferma_man_veloce from w_cs_xx_risposta
end type
type cb_ricerca from commandbutton within w_call_center_conferma_man_veloce
end type
type sle_operaio from singlelineedit within w_call_center_conferma_man_veloce
end type
type sle_divisione from singlelineedit within w_call_center_conferma_man_veloce
end type
type dw_1 from datawindow within w_call_center_conferma_man_veloce
end type
type cb_4 from commandbutton within w_call_center_conferma_man_veloce
end type
type cb_2 from commandbutton within w_call_center_conferma_man_veloce
end type
type cb_3 from commandbutton within w_call_center_conferma_man_veloce
end type
type cb_conferma from commandbutton within w_call_center_conferma_man_veloce
end type
type dw_call_center_operai_lista from datawindow within w_call_center_conferma_man_veloce
end type
type cb_11 from commandbutton within w_call_center_conferma_man_veloce
end type
type dw_call_center_operai from datawindow within w_call_center_conferma_man_veloce
end type
type dw_folder from u_folder within w_call_center_conferma_man_veloce
end type
end forward

global type w_call_center_conferma_man_veloce from w_cs_xx_risposta
integer width = 4882
integer height = 3052
string title = "Conferma Manutenzioni"
cb_ricerca cb_ricerca
sle_operaio sle_operaio
sle_divisione sle_divisione
dw_1 dw_1
cb_4 cb_4
cb_2 cb_2
cb_3 cb_3
cb_conferma cb_conferma
dw_call_center_operai_lista dw_call_center_operai_lista
cb_11 cb_11
dw_call_center_operai dw_call_center_operai
dw_folder dw_folder
end type
global w_call_center_conferma_man_veloce w_call_center_conferma_man_veloce

type variables
boolean ib_global_service=false

end variables

forward prototypes
public function integer wf_trova_inizio_fine (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_fase, ref datetime fdt_inizio_attivita, ref datetime fdt_fine_attivita, ref string fs_errore)
public subroutine wf_calcola_totale_operai (long fl_row)
public function integer wf_operatori_semplice (long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_conferma (integer fi_anno_richiesta, long fl_num_richiesta, string fs_risposta, ref string fs_errore)
end prototypes

public function integer wf_trova_inizio_fine (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_fase, ref datetime fdt_inizio_attivita, ref datetime fdt_fine_attivita, ref string fs_errore);datetime ldt_data_min, ldt_data_max
datetime lt_ora_min, lt_ora_max
long     ll_prog_fase

select count(prog_fase)
into   :ll_prog_fase
from   manutenzioni_fasi_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_fase = :fl_prog_fase;

if sqlca.sqlcode  < 0 then
	fs_errore = "Errore nella ricerca della fase: " + sqlca.sqlerrtext
	return -1
elseif ll_prog_fase = 0 or isnull(ll_prog_fase) then
	fs_errore = ""
	return 0
end if

select MIN(data_inizio)
into   :ldt_data_min
from   manutenzioni_fasi_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_fase = :fl_prog_fase;
		 
if sqlca.sqlcode < 0 then
	setnull(ldt_data_min)
end if

select MIN(ora_inizio)
into   :lt_ora_min
from   manutenzioni_fasi_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_fase = :fl_prog_fase and
		 data_inizio = :ldt_data_min;
		 
if sqlca.sqlcode < 0 then
	setnull(lt_ora_min)
end if

select MAX(data_fine)
into   :ldt_data_max
from   manutenzioni_fasi_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_fase = :fl_prog_fase;
		 
if sqlca.sqlcode < 0 then
	setnull(ldt_data_max)
end if		 

select MAX(ora_fine)
into   :lt_ora_max
from   manutenzioni_fasi_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_fase = :fl_prog_fase and
		 data_fine = :ldt_data_max;
		 
if sqlca.sqlcode < 0 then
	setnull(lt_ora_max)
end if

if not isnull(lt_ora_min) then
	fdt_inizio_attivita = datetime(date(ldt_data_min), time(lt_ora_min))
else
	fdt_inizio_attivita = datetime(date(ldt_data_min), 00:00:00)
end if
if not isnull(lt_ora_max) then
	fdt_fine_attivita = datetime(date(ldt_data_max), time(lt_ora_max))
else
	fdt_fine_attivita = datetime(date(ldt_data_max), 00:00:00)
end if
return 0
end function

public subroutine wf_calcola_totale_operai (long fl_row);dec{4} ld_costo_ora, ld_tempo, ld_sconto_1, ld_totale, ld_ore, ld_minuti

ld_costo_ora = dw_call_center_operai.getitemnumber(fl_row, "costo_orario")
ld_ore = dw_call_center_operai.getitemnumber(fl_row, "tot_ore")
ld_minuti = dw_call_center_operai.getitemnumber(fl_row, "tot_minuti")

if isnull(ld_ore) then ld_ore = 0
if isnull(ld_minuti) then ld_minuti = 0

ld_tempo = ld_ore + ( round(ld_minuti / 60,2) )
ld_sconto_1 = dw_call_center_operai.getitemnumber(fl_row, "sconto_1")

if isnull(ld_costo_ora) then ld_costo_ora = 0
if isnull(ld_tempo) then ld_tempo = 0
if isnull(ld_sconto_1) then ld_sconto_1 = 0

ld_totale = (ld_costo_ora * ld_tempo)

if ld_totale = 0 then 
	dw_call_center_operai.setitem(fl_row, "imponibile_iva", 0)
	return
else
	if ld_sconto_1 > 0 and not isnull(ld_sconto_1) then
		ld_totale = ld_totale - (ld_totale * ld_sconto_1 / 100)
	end if
	
	dw_call_center_operai.setitem(fl_row, "imponibile_iva", round(ld_totale, 2))
end if
dw_call_center_operai.accepttext()
return
end subroutine

public function integer wf_operatori_semplice (long fl_anno_registrazione, long fl_num_registrazione);long   ll_i, ll_tot_ore, ll_tot_minuti
string ls_cod_operaio
dec{4} ld_costo_orario, ld_imponibile

for ll_i = 1 to dw_call_center_operai.rowcount()
	
	if ll_i = 1 then continue
	
	ls_cod_operaio = dw_call_center_operai.getitemstring(ll_i, "cod_operaio")
	ll_tot_ore = dw_call_center_operai.getitemnumber(ll_i, "tot_ore")
	ll_tot_minuti = dw_call_center_operai.getitemnumber( ll_i, "tot_minuti")
	ld_costo_orario = dw_call_center_operai.getitemnumber( ll_i, "costo_orario")
	ld_imponibile = dw_call_center_operai.getitemnumber( ll_i, "imponibile_iva")
	
	if isnull(ll_tot_ore) then
		ll_tot_ore = 0
	end if
	
	if isnull(ll_tot_minuti) then
		ll_tot_minuti = 0
	end if
	
	insert into manutenzioni_operai (cod_azienda,
												 anno_registrazione,
												 num_registrazione,
												 progressivo,
												 cod_operaio,
												 costo_operatore,
												 ore,
												 minuti,
												 tot_costo_operaio)
	values                           (:s_cs_xx.cod_azienda,
												 :fl_anno_registrazione,
												 :fl_num_registrazione,
												 :ll_i,
												 :ls_cod_operaio,
												 :ld_costo_orario,
												 :ll_tot_ore,
												 :ll_tot_minuti,
												 :ld_imponibile);
														 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante la registrazione degli operatori della manutenzione: " + sqlca.sqlerrtext)
		return -1
	end if
	
next	

return 0
end function

public function integer wf_conferma (integer fi_anno_richiesta, long fl_num_richiesta, string fs_risposta, ref string fs_errore);string ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_cod_operaio,ls_des_ricambio_non_codificato,ls_modalita_esecuzione, &
		 ls_cod_ricambio,ls_flag_ricambio_codificato, ls_note_idl, ls_flag_blocco,ls_cod_operaio_cur, &
		 ls_cod_risorsa, ls_cod_cat_risorsa, ls_flag_gen_manutenzioni, ls_errore, ls_note, ls_appo, ls_descrizione_fase, &
		 ls_descrizione_manut, ls_titolo_fase, ls_descrizione_manut_1, ls_note_idl_1, ls_des_ricambio,&
		 ls_cod_tipo_richiesta, ls_des_tipo_manutenzione, ls_str, ls_nome, ls_cognome, ls_des_risorsa, &
		 ls_cod_guasto, ls_controllo,ls_flag_eseguito,ls_flag_spostamento
		 
long   ll_anno_registrazione, ll_num_registrazione, ll_tot_ore, ll_tot_minuti, ll_n, li_risposta, ll_ore_op, ll_min_op, &
       ll_num_manutenzioni_esistenti, ll_prog_fase_effettiva, ll_i, ll_aggiorna_data, ll_ret,  &
		 ll_somma_ore_operai, ll_somma_minuti_operai, ll_somma_ore_risorse, ll_somma_minuti_risorse

//donato 19-06-2008 per calcolare il max in tabella manutenzioni_ricambi prima degli INSERT
long ll_max_manut_ric

dec{4}   ld_quan_ricambio,ld_costo_unitario_ricambio,ld_operaio_minuti,ld_operaio_ore, &
         ld_n_ore,ld_n_ore_2,ld_minuti_fin,ld_ore_fin,ld_costo_orario,ld_imponibile_iva, ld_sconto_1, &
			ld_sconto_2, ld_costi_aggiuntivi, ld_prezzo_ricambio

date     ldt_null

datetime ldt_data_registrazione, lt_tempo_previsto, ldt_inizio_intervento, ldt_fine_intervento, ldt_data_blocco, &
         ldt_data_min_fase, ldt_data_max_fase, ldt_data_max, ldt_ora_max, ldt_data_min_a, ldt_data_max_a, &
         ldt_data_inizio, ldt_data_fine, ldt_inizio, ldt_fine, &
			ldt_operai_data_inizio, ldt_operai_ora_inizio, ldt_operai_data_fine, ldt_operai_ora_fine


dw_1.accepttext()
dw_call_center_operai.accepttext()

select cod_attrezzatura, data_registrazione
into :ls_cod_attrezzatura, :ldt_data_registrazione
from tab_richieste
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:fi_anno_richiesta and
		num_registrazione=:fl_num_richiesta;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in lettura attrezzatura e data della richiesta: "+sqlca.sqlerrtext
	return -1
end if

if ls_cod_attrezzatura="" then setnull(ls_cod_attrezzatura)

setnull(ls_cod_guasto)


ls_descrizione_fase = fs_risposta
ls_descrizione_manut = "" //s_cs_xx.parametri.parametro_s_5

// ----- conto le manutenzioni associate alla richiesta con l'attrezzatura ---
if not isnull(ls_cod_attrezzatura) then
	select count(*)
	into   :ll_num_manutenzioni_esistenti
	from   manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_richiesta = :fi_anno_richiesta and
			 num_reg_richiesta = :fl_num_richiesta and
			 cod_attrezzatura = :ls_cod_attrezzatura;
else
	select count(*)
	into   :ll_num_manutenzioni_esistenti
	from   manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_richiesta = :fi_anno_richiesta and
			 num_reg_richiesta = :fl_num_richiesta and
			 cod_attrezzatura is null;
end if

if sqlca.sqlcode < 0 then
	fs_errore = "Errore nella ricerca delle manutenzioni esistenti: " + sqlca.sqlerrtext
	return -1
end if

// ----- ricerco il codice tipo manutenzione da utilizzare per questa richiesta
ls_flag_eseguito = "S"

if not isnull(ls_cod_attrezzatura) then

	select cod_tipo_richiesta 
	into   :ls_cod_tipo_richiesta
	from   tab_richieste
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :fi_anno_richiesta and
	       num_registrazione = :fl_num_richiesta;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca codice tipo richiesta nella tab_richieste."+sqlca.sqlerrtext
		return -1
	end if
	
	select cod_tipo_manutenzione, des_tipo_manutenzione, flag_spostamento
	into   :ls_cod_tipo_manutenzione, :ls_des_tipo_manutenzione, :ls_flag_spostamento
	from   tab_tipi_richieste
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_richiesta = :ls_cod_tipo_richiesta;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca tipo manutenzione nella tab_tipi_richieste."+sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ls_cod_tipo_manutenzione) then
		fs_errore = "Codice tipo manutenzione non impostato in tabella tipi richieste: impossibile procedere!"
		return -1
	end if
	
	if isnull(ls_des_tipo_manutenzione) then
		fs_errore = "Descrizione tipo manutenzione non impostato in tabella tipi richieste: impossibile procedere!"
		return -1
	end if
	
	// tento sempre la creazione della tipologia di manutenzione
	insert into tab_tipi_manutenzione
		(cod_azienda, 
		 cod_attrezzatura, 
		 cod_tipo_manutenzione, 
		 des_tipo_manutenzione)
	values
		(:s_cs_xx.cod_azienda, 
		 :ls_cod_attrezzatura, 
		 :ls_cod_tipo_manutenzione, 
		 :ls_des_tipo_manutenzione);
		 
	if sqlca.sqlcode <> 0 then
		// nessun controllo errore perchè in effetti potrebbe già esistere
	end if
	
	select modalita_esecuzione, 
			 quan_ricambio, 
			 costo_unitario_ricambio,
			 flag_ricambio_codificato,
			 cod_ricambio,
			 des_ricambio_non_codificato,
			 tempo_previsto
	into   :ls_modalita_esecuzione,
			 :ld_quan_ricambio,
			 :ld_costo_unitario_ricambio,
			 :ls_flag_ricambio_codificato,
			 :ls_cod_ricambio,
			 :ls_des_ricambio_non_codificato,
			 :lt_tempo_previsto
	from   tab_tipi_manutenzione
	where  cod_azienda =:s_cs_xx.cod_azienda and
			 cod_tipo_manutenzione =: ls_cod_tipo_manutenzione and 
			 cod_attrezzatura =:ls_cod_attrezzatura;
	
	if sqlca.sqlcode <> 0 then 		
		fs_errore = "Errore nel ricerca del tipo manutenzione (tab_tipi_manutenznioni)~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_flag_spostamento = "S" then
		ls_flag_eseguito = "N"
	end if
	
else
	
	setnull(ls_cod_tipo_manutenzione)
	setnull(ls_des_tipo_manutenzione)
	setnull(ls_modalita_esecuzione)
	setnull(ld_quan_ricambio)
	setnull(ld_costo_unitario_ricambio)
	ls_flag_ricambio_codificato = "S"
	setnull(ls_cod_ricambio)
	setnull(ls_des_ricambio_non_codificato)
	setnull(lt_tempo_previsto)
	
end if

// -----------------------    note idl e descrizione

if isnull(ls_descrizione_fase) then ls_descrizione_fase = ""
if isnull(ls_modalita_esecuzione) then ls_modalita_esecuzione = ""

if isnull(ls_des_tipo_manutenzione) then
	ls_descrizione_manut_1 = "MANUTENZIONE SU RICHIESTA"
else
	ls_descrizione_manut_1 = ls_des_tipo_manutenzione
end if

ls_flag_blocco = "N"
setnull(ldt_data_blocco)


ll_aggiorna_data = 0

date ldd_appo
datetime ldt_ora_inizio, ldt_ora_fine
ldd_appo = date(ldt_data_registrazione)

ldd_appo = relativedate(ldd_appo, 2)
if daynumber(ldd_appo) = 1 then
	//domenica, aggiungi + 1
	ldd_appo = relativedate(ldd_appo, 1)
	
elseif daynumber(ldd_appo) = 7 then
	//sabato, aggiungi +2
	ldd_appo = relativedate(ldd_appo, 1)
end if

ldt_fine_intervento = datetime(ldd_appo, 00:00:00)
ldt_ora_inizio = datetime(date(1900, 1, 1), 10:00:00)
ldt_ora_fine = datetime(date(1900, 1, 1), 12:00:00)


if ll_num_manutenzioni_esistenti = 0 then
	
	setnull(ls_cod_operaio)
	
	ll_prog_fase_effettiva = 1
	ll_anno_registrazione = f_anno_esercizio()
	
	select max(num_registrazione)
	into :ll_num_registrazione
	from manutenzioni
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione;
	if ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
		ll_num_registrazione = 1 
	else
		ll_num_registrazione ++
	end if
	
	//************************************** Aggiungo la descrizione della richiesta ***************
	if isnull(ls_des_tipo_manutenzione) then
		ls_note_idl_1 = "RICHIESTA:~r~n"
	else
		ls_note_idl_1 = ls_des_tipo_manutenzione + ":~r~n"
	end if
	//*****************************************************
	
	ls_flag_eseguito = "S"

	insert into   manutenzioni
			 (cod_azienda,
			 anno_registrazione,
			 num_registrazione,
			 des_intervento,
			 data_registrazione,
			 cod_attrezzatura,
			 cod_tipo_manutenzione,
			 flag_ordinario,
			 cod_operaio,
			 anno_non_conf,
			 num_non_conf,
			 note_idl,
			 quan_ricambio,
			 costo_unitario_ricambio,
			 cod_ricambio,
			 flag_ricambio_codificato,
			 ricambio_non_codificato,
			 operaio_minuti,
			 operaio_ore,
			 flag_richieste_telefoniche,
			 flag_richieste_cartacee,
			 flag_risc_giro_isp,
			 data_inizio_intervento,
			 ora_inizio_intervento,
			 data_fine_intervento,
			 ora_fine_intervento,
			 flag_eseguito,
			 anno_reg_richiesta,
			 num_reg_richiesta,
			 flag_straordinario,
			 cod_guasto)
	values (:s_cs_xx.cod_azienda,
			 :ll_anno_registrazione,
			 :ll_num_registrazione,
			 :ls_descrizione_manut_1,
			 :ldt_fine_intervento,
			 :ls_cod_attrezzatura,
			 :ls_cod_tipo_manutenzione,
			 'N',
			 :ls_cod_operaio,
			 null,
			 null,
			 :ls_note_idl_1,
			 :ld_quan_ricambio,
			 :ld_costo_unitario_ricambio,
			 :ls_cod_ricambio,
			 :ls_flag_ricambio_codificato,
			 :ls_des_ricambio_non_codificato,
			 :ld_operaio_minuti,
			 :ld_operaio_ore,
			 'S',
			 'N',
			 'N',
			 :ldt_fine_intervento,
			 :ldt_ora_inizio,
			 :ldt_fine_intervento,
			 :ldt_ora_fine,
			 :ls_flag_eseguito,
			 :fi_anno_richiesta,
			 :fl_num_richiesta,
			 'N',
			 :ls_cod_guasto);
			  
	if sqlca.sqlcode <> 0 then 		
		fs_errore = "Errore nella registrazione della manutenzione.~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext + &
					  "~nAnno Registrazione " + string(ll_anno_registrazione) + " Numero Registrazione " + string(ll_num_registrazione)
		return -1
	end if
else
// ------------ esiste già una manutenzione, quindi vado a creare una nuova fase della manutenzione -----
// ------------ dopo che so anno e numero naturalmente ----
	
	if not isnull(ls_cod_attrezzatura) then
		select anno_registrazione, num_registrazione
		into   :ll_anno_registrazione, :ll_num_registrazione
		from   manutenzioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_reg_richiesta = :fi_anno_richiesta and
				 num_reg_richiesta = :fl_num_richiesta and
				 cod_attrezzatura = :ls_cod_attrezzatura;
	else
		select anno_registrazione, num_registrazione
		into   :ll_anno_registrazione, :ll_num_registrazione
		from   manutenzioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_reg_richiesta = :fi_anno_richiesta and
				 num_reg_richiesta = :fl_num_richiesta and
				 cod_attrezzatura is null;
	end if
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore errore in selezione anno/numero manutenzione esistente: " + sqlca.sqlerrtext
		return -1
	end if

	select max(prog_fase)
	into   :ll_prog_fase_effettiva
	from   manutenzioni_fasi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
			 
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in ricerca fasi manutenzione.~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext
		return -1		
	end if
	
	if isnull(ll_prog_fase_effettiva) or ll_prog_fase_effettiva = 0 then
		ll_prog_fase_effettiva = 1
	else
		ll_prog_fase_effettiva = ll_prog_fase_effettiva + 1
	end if
	
	ls_flag_eseguito = "S"

	update   manutenzioni
	set flag_eseguito = :ls_flag_eseguito
	where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione = :ll_num_registrazione;
		 
	if sqlca.sqlcode <> 0 then 		
		fs_errore = "Errore nell'aggiornamento della mautenzione (eseguita=SI).~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext + &
					  "~nAnno Registrazione " + string(ll_anno_registrazione) + " Numero Registrazione " + string(ll_num_registrazione)
		return -1
	end if
	
	//fine modifica -------------------------------------------------------------------------------------------------------
	
end if

ls_titolo_fase = "Fase" + string(ll_prog_fase_effettiva)
// ------------ nuova fase --- 
insert into manutenzioni_fasi(cod_azienda, 
										anno_registrazione, 
										num_registrazione, 
										prog_fase,
										des_fase,
										note)
							values  (:s_cs_xx.cod_azienda,
										:ll_anno_registrazione,
										:ll_num_registrazione,
										:ll_prog_fase_effettiva,
										:ls_titolo_fase,
										:ls_descrizione_fase);
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in creazione fase manutenzione.~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext + " prog. fase = " + string(ll_prog_fase_effettiva)
	return -1
end if
//
// -------------------------------- inserisco tutti gli operatori -----------------------------------------






for ll_i = 1 to dw_call_center_operai.rowcount()
	
	ls_cod_operaio = dw_call_center_operai.getitemstring(ll_i, "cod_operaio")
	ldt_data_inizio = ldt_fine_intervento
	ldt_ora_inizio = ldt_ora_inizio
	ldt_data_fine = ldt_fine_intervento
	ldt_ora_fine = ldt_ora_fine
	ll_tot_ore = 2
	ll_tot_minuti = 0
	ld_costo_orario = 0
	ld_sconto_1 = 0
	ld_imponibile_iva = 0
	
	if isnull(ll_tot_ore) then
		ll_tot_ore = 0
	end if
	
	if isnull(ll_tot_minuti) then
		ll_tot_minuti = 0
	end if
	
	INSERT INTO manutenzioni_fasi_operai  
			( cod_azienda,
			  anno_registrazione,   
			  num_registrazione,   
			  prog_fase,
			  cod_operaio,
			  data_inizio,   
			  ora_inizio,   
			  data_fine,
			  ora_fine,
			  tot_ore,
			  tot_minuti,
			  costo_ora_operaio,
			  sconto_1,
			  tot_costo_operaio)  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  :ll_prog_fase_effettiva,   
			  :ls_cod_operaio,   
			  :ldt_data_inizio,
			  :ldt_ora_inizio,
			  :ldt_data_fine,
			  :ldt_ora_fine,
			  :ll_tot_ore,
			  :ll_tot_minuti,
			  :ld_costo_orario,
			  :ld_sconto_1,
			  :ld_imponibile_iva);

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in inserimento operai in fase delle manutenzioni.~r~n"+ sqlca.sqlerrtext
		return -1
	end if
		
next	


// 	aggiunto da enrico su richiesta di Baggio 9/4/2004
// 	Quando si generano le manutenzioni dalle richieste senza codice attrezzatura, 
// 	allora non va messo il flag_gen_manutenzioni a si.

ls_flag_gen_manutenzioni = "S"

if isnull(ls_cod_attrezzatura) then
	select flag_gen_manutenzione
	into   :ls_flag_gen_manutenzioni
	from   tab_richieste
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fi_anno_richiesta and
			 num_registrazione = :fl_num_richiesta;
	if sqlca.sqlcode = 0  and ls_flag_gen_manutenzioni = "N" then
		ls_flag_gen_manutenzioni = "N"
	end if
end if
//    fine aggiunta di Enrico.

update tab_richieste
set    flag_gen_manutenzione = :ls_flag_gen_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fi_anno_richiesta and
		 num_registrazione = :fl_num_richiesta;

if sqlca.sqlcode <> 0 then 		
	fs_errore = "Errore in UPDATE tab_richieste per flag_gen_manutenzioni ~r~nERRRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext
	return -1
end if

	
ls_errore = ""
li_risposta = f_tempi_richiesta(fi_anno_richiesta, fl_num_richiesta, ls_errore)
if li_risposta <> 0 then
	fs_errore = "Errore nell'aggiornamento delle date!" + ls_errore
	return -1
end if


update tab_richieste
set    flag_richiesta_chiusa = 'S'
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fi_anno_richiesta and
		 num_registrazione = :fl_num_richiesta;

if sqlca.sqlcode <> 0 then 		
	fs_errore ="Errore in UPDATE richiesta chiusa.~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext
	return -1
end if	

return 0

end function

on w_call_center_conferma_man_veloce.create
int iCurrent
call super::create
this.cb_ricerca=create cb_ricerca
this.sle_operaio=create sle_operaio
this.sle_divisione=create sle_divisione
this.dw_1=create dw_1
this.cb_4=create cb_4
this.cb_2=create cb_2
this.cb_3=create cb_3
this.cb_conferma=create cb_conferma
this.dw_call_center_operai_lista=create dw_call_center_operai_lista
this.cb_11=create cb_11
this.dw_call_center_operai=create dw_call_center_operai
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ricerca
this.Control[iCurrent+2]=this.sle_operaio
this.Control[iCurrent+3]=this.sle_divisione
this.Control[iCurrent+4]=this.dw_1
this.Control[iCurrent+5]=this.cb_4
this.Control[iCurrent+6]=this.cb_2
this.Control[iCurrent+7]=this.cb_3
this.Control[iCurrent+8]=this.cb_conferma
this.Control[iCurrent+9]=this.dw_call_center_operai_lista
this.Control[iCurrent+10]=this.cb_11
this.Control[iCurrent+11]=this.dw_call_center_operai
this.Control[iCurrent+12]=this.dw_folder
end on

on w_call_center_conferma_man_veloce.destroy
call super::destroy
destroy(this.cb_ricerca)
destroy(this.sle_operaio)
destroy(this.sle_divisione)
destroy(this.dw_1)
destroy(this.cb_4)
destroy(this.cb_2)
destroy(this.cb_3)
destroy(this.cb_conferma)
destroy(this.dw_call_center_operai_lista)
destroy(this.cb_11)
destroy(this.dw_call_center_operai)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag
windowobject lw_oggetti[], lw_oggetti_2[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)
save_on_close(c_socnosave)

dw_1.settransobject(sqlca)

dw_call_center_operai_lista.settransobject(sqlca)
dw_call_center_operai.settransobject(sqlca)

save_on_close(c_socnosave)

lw_oggetti[1] = dw_call_center_operai_lista
lw_oggetti[2] = dw_call_center_operai
lw_oggetti[3] = cb_11
lw_oggetti[4] = cb_2
lw_oggetti[5] = cb_3
lw_oggetti[6] = cb_4
dw_folder.fu_assigntab(1, "Operatori", lw_oggetti[])

dw_folder.fu_foldercreate(1,1)
dw_folder.fu_selecttab(1)

select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
if (sqlca.sqlcode = 0) and ls_flag = "S" then
	ib_global_service = true
end if
	

end event

event pc_setddlb;call super::pc_setddlb;string ls_cod_attrezzatura

if isnull(s_cs_xx.parametri.parametro_s_2) or len(s_cs_xx.parametri.parametro_s_2) < 1 then

else		
	ls_cod_attrezzatura = s_cs_xx.parametri.parametro_s_2
end if					  
end event

event open;call super::open;long			l_Error, ll_riga, ll_ore_totali, ll_minuti_totali, li_risposta
string		ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna
datetime		ldt_data_inizio, ldt_data_fine, ldt_oggi
time			lt_inizio, lt_fine
dec{4}		ld_costo_orario, ld_costi_aggiuntivi


ldt_oggi = datetime(today(), 00:00:00)

l_Error = dw_call_center_operai_lista.Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


if s_cs_xx.parametri.parametro_s_1 <> "S" and not isnull(s_cs_xx.parametri.parametro_s_3) and len(s_cs_xx.parametri.parametro_s_3) > 0 then
	ls_cod_operaio = s_cs_xx.parametri.parametro_s_3
	
	ll_riga = dw_call_center_operai.insertrow(0)
	
	dw_call_center_operai.setitem(ll_riga,"cod_azienda",s_cs_xx.cod_azienda)
	dw_call_center_operai.setitem(ll_riga,"cod_operaio",ls_cod_operaio)
	dw_call_center_operai.setitem(ll_riga,"data_inizio", s_cs_xx.parametri.parametro_data_1)
	dw_call_center_operai.setitem(ll_riga,"ora_inizio", s_cs_xx.parametri.parametro_data_2)
	dw_call_center_operai.setitem(ll_riga,"data_fine", s_cs_xx.parametri.parametro_data_3)
	dw_call_center_operai.setitem(ll_riga,"ora_fine", s_cs_xx.parametri.parametro_data_4)
	
	
	li_risposta = f_date_h_m(s_cs_xx.parametri.parametro_data_1,s_cs_xx.parametri.parametro_data_2,s_cs_xx.parametri.parametro_data_3,s_cs_xx.parametri.parametro_data_4, ll_ore_totali, ll_minuti_totali)
	dw_call_center_operai.setitem(ll_riga,"tot_ore",ll_ore_totali)
	dw_call_center_operai.setitem(ll_riga,"tot_minuti",ll_minuti_totali)	
	
	// *** Michela 20/07/2006: aggiungo i costi
	
	ld_costo_orario = 0
	dw_call_center_operai.setitem(ll_riga,"costo_orario",ld_costo_orario)	
	wf_calcola_totale_operai(ll_riga)
	dw_call_center_operai.postevent("ue_totale_riga")	
	
	
end if

end event

type cb_ricerca from commandbutton within w_call_center_conferma_man_veloce
integer x = 1815
integer y = 32
integer width = 571
integer height = 108
integer taborder = 20
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "cerca"
end type

event clicked;string ls_cod_divisione, ls_cod_operaio


ls_cod_divisione = sle_divisione.text
ls_cod_operaio = sle_operaio.text

if ls_cod_divisione="" then setnull(ls_cod_divisione)
if ls_cod_operaio="" then setnull(ls_cod_operaio)

dw_1.retrieve(s_cs_xx.cod_azienda, ls_cod_operaio, ls_cod_divisione)
end event

type sle_operaio from singlelineedit within w_call_center_conferma_man_veloce
integer x = 1120
integer y = 32
integer width = 571
integer height = 124
integer taborder = 10
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
end type

type sle_divisione from singlelineedit within w_call_center_conferma_man_veloce
integer x = 251
integer y = 32
integer width = 571
integer height = 124
integer taborder = 10
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
end type

type dw_1 from datawindow within w_call_center_conferma_man_veloce
integer x = 46
integer y = 216
integer width = 4704
integer height = 1492
integer taborder = 10
string title = "none"
string dataobject = "d_call_center_conferma_man_veloce"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type cb_4 from commandbutton within w_call_center_conferma_man_veloce
integer x = 1618
integer y = 2548
integer width = 123
integer height = 120
integer taborder = 62
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<<"
end type

event clicked;dw_call_center_operai.reset()
end event

type cb_2 from commandbutton within w_call_center_conferma_man_veloce
integer x = 1618
integer y = 2396
integer width = 123
integer height = 120
integer taborder = 52
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<"
end type

event clicked;long   ll_riga

if dw_call_center_operai.getrow() > 0 then
	
	ll_riga = dw_call_center_operai.getrow()
	dw_call_center_operai.deleterow(ll_riga)	

end if

end event

type cb_3 from commandbutton within w_call_center_conferma_man_veloce
integer x = 1618
integer y = 2052
integer width = 123
integer height = 120
integer taborder = 32
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">>"
end type

event clicked;long ll_i
datetime ldt_null
long     ll_riga
string   ls_cod_operaio
datetime ldt_data_inizio, ldt_data_fine
time     lt_inizio, lt_fine
long     ll_n_ore, ll_n_ore_2, ll_minuti_fin, ll_ore_fin
dec{4}   ld_costo_orario


for ll_i = 1 to dw_call_center_operai_lista.rowcount()
	dw_call_center_operai_lista.setrow(ll_i)

	if dw_call_center_operai_lista.getrow() > 0 then
		
		ls_cod_operaio = dw_call_center_operai_lista.getitemstring(dw_call_center_operai_lista.getrow(),"cod_operaio")
		if dw_call_center_operai.find("cod_operaio = '" + ls_cod_operaio + "'", 1, dw_call_center_operai.rowcount() ) > 0 then
			beep(10)
			return
		end if		
	
		setnull(ldt_null)
		
		ll_riga = dw_call_center_operai.insertrow(0)
		dw_call_center_operai.setitem(ll_riga,"cod_azienda",s_cs_xx.cod_azienda)
		dw_call_center_operai.setitem(ll_riga,"cod_operaio",ls_cod_operaio)
		ld_costo_orario = 0
		
		wf_calcola_totale_operai(ll_riga)
		
		if ll_riga > 1 then
			dw_call_center_operai.setitem(ll_riga,"data_inizio",dw_call_center_operai.getitemdatetime(ll_riga - 1,"data_inizio"))
			dw_call_center_operai.setitem(ll_riga,"ora_inizio",dw_call_center_operai.getitemdatetime(ll_riga - 1,"ora_inizio"))
			dw_call_center_operai.setitem(ll_riga,"data_fine",dw_call_center_operai.getitemdatetime(ll_riga - 1,"data_fine"))
			dw_call_center_operai.setitem(ll_riga,"ora_fine",dw_call_center_operai.getitemdatetime(ll_riga - 1,"ora_fine"))
			dw_call_center_operai.setitem(ll_riga,"tot_ore",dw_call_center_operai.getitemnumber(ll_riga - 1,"tot_ore"))
			dw_call_center_operai.setitem(ll_riga,"tot_minuti",dw_call_center_operai.getitemnumber(ll_riga - 1,"tot_minuti"))
		else
			dw_call_center_operai.setitem(ll_riga,"data_inizio", ldt_null)
			dw_call_center_operai.setitem(ll_riga,"ora_inizio", ldt_null)
			dw_call_center_operai.setitem(ll_riga,"data_fine", ldt_null)
			dw_call_center_operai.setitem(ll_riga,"ora_fine", ldt_null)
			dw_call_center_operai.setitem(ll_riga,"tot_ore",0)
			dw_call_center_operai.setitem(ll_riga,"tot_minuti",0)
		end if
	end if

next
end event

type cb_conferma from commandbutton within w_call_center_conferma_man_veloce
integer x = 4023
integer y = 2724
integer width = 366
integer height = 80
integer taborder = 2
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;long ll_index, ll_tot, ll_num_richiesta
integer li_anno_richiesta, li_ret
string ls_errore, ls_risposta, ls_selezionato, ls_cod_operaio

ll_tot = dw_1.rowcount()
dw_1.accepttext()

if dw_call_center_operai.rowcount() = 1 then
	ls_cod_operaio = dw_call_center_operai.getitemstring(1, "cod_operaio")
	if ls_cod_operaio="" or isnull(ls_cod_operaio) then
		g_mb.error("Selezionare un operatore!")
		return
	end if
else
	g_mb.error("Selezionare un operatore!")
	return
end if


for ll_index=1 to ll_tot
	
	ls_selezionato = dw_1.getitemstring(ll_index, "selezionato")
	if ls_selezionato<>'S' then continue
	
	li_anno_richiesta = dw_1.getitemnumber(ll_index, "anno_registrazione")
	ll_num_richiesta = dw_1.getitemnumber(ll_index, "num_registrazione")
	
	ls_risposta = dw_1.getitemstring(ll_index, "risposta_operatore")
	
	if ls_risposta="" or isnull(ls_risposta) then
		g_mb.warning("Risposta non digitata per la richiesta "+string(li_anno_richiesta)+"/"+string(ll_num_richiesta), " ! Verrà saltata!")
		continue
	end if
	
	li_ret = wf_conferma(li_anno_richiesta, ll_num_richiesta, ls_risposta, ls_errore)
	if li_ret < 0 then
		rollback;
		g_mb.error(ls_errore)
	else
		commit;
	end if
next


cb_ricerca.postevent(clicked!)
end event

type dw_call_center_operai_lista from datawindow within w_call_center_conferma_man_veloce
event ue_key pbm_dwnkey
integer x = 87
integer y = 2004
integer width = 1522
integer height = 696
integer taborder = 22
string title = "none"
string dataobject = "d_call_center_conferma_manut_operai"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanging;selectrow(currentrow,false)
selectrow(newrow,true)
end event

type cb_11 from commandbutton within w_call_center_conferma_man_veloce
integer x = 1618
integer y = 2240
integer width = 123
integer height = 120
integer taborder = 42
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">"
end type

event clicked;string   ls_cod_operaio
datetime ldt_null, ldt_data_inizio, ldt_data_fine
time     lt_inizio, lt_fine
long     ll_riga, ll_n_ore, ll_n_ore_2, ll_minuti_fin, ll_ore_fin
dec{4}   ld_costo_orario

if dw_call_center_operai_lista.getrow() > 0 then
	
	ls_cod_operaio = dw_call_center_operai_lista.getitemstring(dw_call_center_operai_lista.getrow(),"cod_operaio")
	if dw_call_center_operai.find("cod_operaio = '" + ls_cod_operaio + "'", 1, dw_call_center_operai.rowcount() ) > 0 then
		beep(10)
		return
	end if
	
	setnull(ldt_null)
	
	ll_riga = dw_call_center_operai.insertrow(0)
	
	dw_call_center_operai.setitem(ll_riga,"cod_azienda",s_cs_xx.cod_azienda)
	dw_call_center_operai.setitem(ll_riga,"cod_operaio",ls_cod_operaio)
	ld_costo_orario = 0
	dw_call_center_operai.setitem(ll_riga,"costo_orario",ld_costo_orario)	
	
	
	if ll_riga > 1 then
		dw_call_center_operai.setitem(ll_riga,"data_inizio",dw_call_center_operai.getitemdatetime(ll_riga - 1,"data_inizio"))
		dw_call_center_operai.setitem(ll_riga,"ora_inizio",dw_call_center_operai.getitemdatetime(ll_riga - 1,"ora_inizio"))
		dw_call_center_operai.setitem(ll_riga,"data_fine",dw_call_center_operai.getitemdatetime(ll_riga - 1,"data_fine"))
		dw_call_center_operai.setitem(ll_riga,"ora_fine",dw_call_center_operai.getitemdatetime(ll_riga - 1,"ora_fine"))
		dw_call_center_operai.setitem(ll_riga,"tot_ore",dw_call_center_operai.getitemnumber(ll_riga - 1,"tot_ore"))
		dw_call_center_operai.setitem(ll_riga,"tot_minuti",dw_call_center_operai.getitemnumber(ll_riga - 1,"tot_minuti"))
	else
		dw_call_center_operai.setitem(ll_riga,"data_inizio", ldt_null)
		dw_call_center_operai.setitem(ll_riga,"ora_inizio", ldt_null)
		dw_call_center_operai.setitem(ll_riga,"data_fine", ldt_null)
		dw_call_center_operai.setitem(ll_riga,"ora_fine", ldt_null)
		dw_call_center_operai.setitem(ll_riga,"tot_ore",0)
		dw_call_center_operai.setitem(ll_riga,"tot_minuti",0)
	end if
	wf_calcola_totale_operai(ll_riga)
//	dw_call_center_operai.postevent("ue_totale_riga_operaio")

end if
end event

type dw_call_center_operai from datawindow within w_call_center_conferma_man_veloce
event ue_imposta_fine ( )
event ue_totale_riga_operaio ( )
integer x = 1755
integer y = 2004
integer width = 2642
integer height = 696
integer taborder = 22
string title = "none"
string dataobject = "d_call_center_conferma_manut_operai_list"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_imposta_fine();setitem(getrow(),"data_fine",getitemdatetime(getrow(),"data_inizio"))
end event

event ue_totale_riga_operaio();wf_calcola_totale_operai(getrow())
end event

event itemchanged;datetime ldt_data_inizio, ldt_data_fine
time     lt_inizio, lt_fine
long     li_risposta, ll_ore_totali, ll_minuti_totali, ll_i
dec{4}   ld_costo_orario

ll_i = getrow()
if ll_i <= 0 then return -1
	
accepttext()

choose case dwo.name
	case "data_inizio", "ora_inizio", "data_fine", "ora_fine"


		if dwo.name = "data_inizio" then
			postevent("ue_imposta_fine")
		end if

		li_risposta = f_date_h_m(getitemdatetime(ll_i,"data_inizio"), &
		                         getitemdatetime(ll_i,"ora_inizio"), &
										 getitemdatetime(ll_i,"data_fine"),&
										 getitemdatetime(ll_i,"ora_fine"), &
										 ll_ore_totali, &
										 ll_minuti_totali)
		setitem(ll_i,"tot_ore",ll_ore_totali)
		setitem(ll_i,"tot_minuti",ll_minuti_totali)	
		
		postevent("ue_totale_riga_operaio")
		
	case "cod_operaio"
		ld_costo_orario = 0
		setitem(ll_i,"costo_orario",ll_minuti_totali)	
		postevent("ue_totale_riga_operaio")

	case "tot_ore", "tot_minuti", "sconto_1", "costo_orario"
		postevent("ue_totale_riga_operaio")

end choose


end event

event itemfocuschanged;selecttext(1, len(gettext()))
end event

type dw_folder from u_folder within w_call_center_conferma_man_veloce
integer x = 64
integer y = 1764
integer width = 4366
integer height = 1140
integer taborder = 11
end type

