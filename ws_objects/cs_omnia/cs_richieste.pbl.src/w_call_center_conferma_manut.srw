﻿$PBExportHeader$w_call_center_conferma_manut.srw
$PBExportComments$Finestra modale per conferma Manutenzioni
forward
global type w_call_center_conferma_manut from w_cs_xx_risposta
end type
type mle_idl from multilineedit within w_call_center_conferma_manut
end type
type dw_tipi_man from datawindow within w_call_center_conferma_manut
end type
type cb_4_ricambi from commandbutton within w_call_center_conferma_manut
end type
type cb_2_ricambi from commandbutton within w_call_center_conferma_manut
end type
type cb_4_ris from commandbutton within w_call_center_conferma_manut
end type
type cb_2_ris from commandbutton within w_call_center_conferma_manut
end type
type st_ric_op from statictext within w_call_center_conferma_manut
end type
type cb_4 from commandbutton within w_call_center_conferma_manut
end type
type cb_2 from commandbutton within w_call_center_conferma_manut
end type
type cb_3 from commandbutton within w_call_center_conferma_manut
end type
type cb_conferma from commandbutton within w_call_center_conferma_manut
end type
type dw_selezione from uo_dw_main within w_call_center_conferma_manut
end type
type st_ric_op_risorse from statictext within w_call_center_conferma_manut
end type
type dw_call_center_operai_lista from datawindow within w_call_center_conferma_manut
end type
type dw_call_center_risorse_lista from datawindow within w_call_center_conferma_manut
end type
type cb_3_ricambi from commandbutton within w_call_center_conferma_manut
end type
type cb_11_ris from commandbutton within w_call_center_conferma_manut
end type
type cb_3_ris from commandbutton within w_call_center_conferma_manut
end type
type cb_11 from commandbutton within w_call_center_conferma_manut
end type
type cb_11_ricambi from commandbutton within w_call_center_conferma_manut
end type
type dw_call_center_manut_ricambi from datawindow within w_call_center_conferma_manut
end type
type dw_call_center_operai from datawindow within w_call_center_conferma_manut
end type
type dw_call_center_risorse from datawindow within w_call_center_conferma_manut
end type
type dw_folder from u_folder within w_call_center_conferma_manut
end type
type dw_call_center_manut_ricambi_lista from datawindow within w_call_center_conferma_manut
end type
type dw_ricerca_ricambi from u_dw_search within w_call_center_conferma_manut
end type
type cb_retrieve_ricambi from commandbutton within w_call_center_conferma_manut
end type
end forward

global type w_call_center_conferma_manut from w_cs_xx_risposta
integer width = 4489
integer height = 2504
string title = "Conferma Manutenzioni"
event ue_limit ( )
mle_idl mle_idl
dw_tipi_man dw_tipi_man
cb_4_ricambi cb_4_ricambi
cb_2_ricambi cb_2_ricambi
cb_4_ris cb_4_ris
cb_2_ris cb_2_ris
st_ric_op st_ric_op
cb_4 cb_4
cb_2 cb_2
cb_3 cb_3
cb_conferma cb_conferma
dw_selezione dw_selezione
st_ric_op_risorse st_ric_op_risorse
dw_call_center_operai_lista dw_call_center_operai_lista
dw_call_center_risorse_lista dw_call_center_risorse_lista
cb_3_ricambi cb_3_ricambi
cb_11_ris cb_11_ris
cb_3_ris cb_3_ris
cb_11 cb_11
cb_11_ricambi cb_11_ricambi
dw_call_center_manut_ricambi dw_call_center_manut_ricambi
dw_call_center_operai dw_call_center_operai
dw_call_center_risorse dw_call_center_risorse
dw_folder dw_folder
dw_call_center_manut_ricambi_lista dw_call_center_manut_ricambi_lista
dw_ricerca_ricambi dw_ricerca_ricambi
cb_retrieve_ricambi cb_retrieve_ricambi
end type
global w_call_center_conferma_manut w_call_center_conferma_manut

type variables
boolean ib_global_service=false
long il_controllo_conferma = 0, il_anno_ric, il_num_ric, il_anno_nc, il_num_nc

string is_ricerca = ""
end variables

forward prototypes
public function integer wf_trova_inizio_fine (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_fase, ref datetime fdt_inizio_attivita, ref datetime fdt_fine_attivita, ref string fs_errore)
public function decimal wf_carica_prezzo (string fs_cod_prodotto, decimal fd_quantita)
public subroutine wf_calcola_totale (long fl_row)
public subroutine wf_calcola_totale_risorse (long fl_row)
public subroutine wf_calcola_totale_operai (long fl_row)
public function decimal wf_carica_costo_operaio (string fs_cod_operaio)
public function integer wf_operatori_semplice (long fl_anno_registrazione, long fl_num_registrazione)
public subroutine wf_carica_costo_risorse (string fs_cod_cat_risorsa, string fs_cod_risorsa, ref decimal fd_costo_orario, ref decimal fd_costi_aggiuntivi)
public subroutine wf_imposta_campi_note ()
public function integer wf_idl (ref string as_messaggio)
end prototypes

event ue_limit();
string				ls_modify, ls_colonna


ls_colonna = "descrizione"


ls_modify = ls_colonna + ".Edit.limit='32000'~t"

dw_selezione.modify(ls_modify)
end event

public function integer wf_trova_inizio_fine (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_fase, ref datetime fdt_inizio_attivita, ref datetime fdt_fine_attivita, ref string fs_errore);datetime ldt_data_min, ldt_data_max
datetime lt_ora_min, lt_ora_max
long     ll_prog_fase

select count(prog_fase)
into   :ll_prog_fase
from   manutenzioni_fasi_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_fase = :fl_prog_fase;

if sqlca.sqlcode  < 0 then
	fs_errore = "Errore nella ricerca della fase: " + sqlca.sqlerrtext
	return -1
elseif ll_prog_fase = 0 or isnull(ll_prog_fase) then
	fs_errore = ""
	return 0
end if

select MIN(data_inizio)
into   :ldt_data_min
from   manutenzioni_fasi_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_fase = :fl_prog_fase;
		 
if sqlca.sqlcode < 0 then
	setnull(ldt_data_min)
end if

select MIN(ora_inizio)
into   :lt_ora_min
from   manutenzioni_fasi_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_fase = :fl_prog_fase and
		 data_inizio = :ldt_data_min;
		 
if sqlca.sqlcode < 0 then
	setnull(lt_ora_min)
end if

select MAX(data_fine)
into   :ldt_data_max
from   manutenzioni_fasi_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_fase = :fl_prog_fase;
		 
if sqlca.sqlcode < 0 then
	setnull(ldt_data_max)
end if		 

select MAX(ora_fine)
into   :lt_ora_max
from   manutenzioni_fasi_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_fase = :fl_prog_fase and
		 data_fine = :ldt_data_max;
		 
if sqlca.sqlcode < 0 then
	setnull(lt_ora_max)
end if

if not isnull(lt_ora_min) then
	fdt_inizio_attivita = datetime(date(ldt_data_min), time(lt_ora_min))
else
	fdt_inizio_attivita = datetime(date(ldt_data_min), 00:00:00)
end if
if not isnull(lt_ora_max) then
	fdt_fine_attivita = datetime(date(ldt_data_max), time(lt_ora_max))
else
	fdt_fine_attivita = datetime(date(ldt_data_max), 00:00:00)
end if
return 0
end function

public function decimal wf_carica_prezzo (string fs_cod_prodotto, decimal fd_quantita);string ls_cod_prodotto, ls_cod_cliente,ls_cod_attrezzatura,ls_cod_tipo_listino_prodotto,ls_cod_valuta
datetime ldt_data_registrazione
dec{4} ld_quantita, ld_prezzo
uo_condizioni_cliente luo_condizioni_cliente

ldt_data_registrazione = datetime(today(),00:00:00)

ls_cod_prodotto = fs_cod_prodotto

ld_quantita = fd_quantita

ls_cod_attrezzatura = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_attrezzatura")

//if isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura="" then
//	select cod_cliente
//	into   :ls_cod_cliente
//	from   anag_divisioni
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_divisione in (select cod_divisione 
//									 from   tab_aree_aziendali
//									 where  cod_azienda = :s_cs_xx.cod_azienda and
//											  cod_area_aziendale in ( select cod_area_aziendale
//																			  from   anag_attrezzature
//																			  where  cod_azienda = :s_cs_xx.cod_azienda ));
//else
//	select cod_cliente
//	into   :ls_cod_cliente
//	from   anag_divisioni
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_divisione in (select cod_divisione 
//									 from   tab_aree_aziendali
//									 where  cod_azienda = :s_cs_xx.cod_azienda and
//											  cod_area_aziendale in ( select cod_area_aziendale
//																			  from   anag_attrezzature
//																			  where  cod_azienda = :s_cs_xx.cod_azienda and
//																						cod_attrezzatura = :ls_cod_attrezzatura));
//end if
select cod_cliente
into   :ls_cod_cliente
from   anag_divisioni
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_divisione in (select cod_divisione 
								 from   tab_aree_aziendali
								 where  cod_azienda = :s_cs_xx.cod_azienda and
										  cod_area_aziendale in ( select cod_area_aziendale
																		  from   anag_attrezzature
																		  where  cod_azienda = :s_cs_xx.cod_azienda and
																					cod_attrezzatura = :ls_cod_attrezzatura));

if not isnull(ls_cod_cliente) then

	select cod_tipo_listino_prodotto,
			 cod_valuta
	into   :ls_cod_tipo_listino_prodotto,
			 :ls_cod_valuta
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :ls_cod_cliente;
else
	setnull(ls_cod_tipo_listino_prodotto)
	setnull(ls_cod_valuta)
end if

luo_condizioni_cliente = create uo_condizioni_cliente
luo_condizioni_cliente.str_parametri.ldw_oggetto = dw_call_center_manut_ricambi
luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
luo_condizioni_cliente.str_parametri.dim_1 = 0
luo_condizioni_cliente.str_parametri.dim_2 = 0
luo_condizioni_cliente.str_parametri.quantita = ld_quantita
luo_condizioni_cliente.str_parametri.valore = 0
// luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
// luo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
luo_condizioni_cliente.str_parametri.fat_conversione_ven = 1
luo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ricambio"
luo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
luo_condizioni_cliente.ib_setitem = false
luo_condizioni_cliente.ib_provvigioni = false
luo_condizioni_cliente.ib_setitem_provvigioni = false
luo_condizioni_cliente.wf_condizioni_cliente()     

ld_prezzo = luo_condizioni_cliente.str_output.variazioni[1]
destroy luo_condizioni_cliente
	
return ld_prezzo
end function

public subroutine wf_calcola_totale (long fl_row);dec{4} ld_prezzo, ld_quantita, ld_sconto_1, ld_sconto_2, ld_totale

ld_prezzo = dw_call_center_manut_ricambi.getitemnumber(fl_row, "prezzo_ricambio")
ld_quantita = dw_call_center_manut_ricambi.getitemnumber(fl_row, "quan_ricambio")
ld_sconto_1 = dw_call_center_manut_ricambi.getitemnumber(fl_row, "sconto_1")
ld_sconto_2 = dw_call_center_manut_ricambi.getitemnumber(fl_row, "sconto_2")

if isnull(ld_prezzo) then ld_prezzo = 0
if isnull(ld_quantita) then ld_quantita = 0
if isnull(ld_sconto_1) then ld_sconto_1 = 0
if isnull(ld_sconto_2) then ld_sconto_2 = 0

ld_totale = ld_quantita * ld_prezzo

if ld_totale = 0 then 
	dw_call_center_manut_ricambi.setitem(fl_row, "imponibile_iva", 0)
	return
else
	if ld_sconto_1 > 0 and not isnull(ld_sconto_1) then
		ld_totale = ld_totale - (ld_totale * ld_sconto_1 / 100)
	end if
	
	if ld_sconto_2 > 0 and not isnull(ld_sconto_2) then
		ld_totale = ld_totale - (ld_totale * ld_sconto_2 / 100)
	end if
	
	dw_call_center_manut_ricambi.setitem(fl_row, "imponibile_iva", round(ld_totale, 2))
end if
dw_call_center_manut_ricambi.accepttext()
return
end subroutine

public subroutine wf_calcola_totale_risorse (long fl_row);dec{4} ld_costo_ora, ld_tempo, ld_sconto_1, ld_costi_aggiuntivi, ld_totale, ld_ore, ld_minuti

ld_costo_ora = dw_call_center_risorse.getitemnumber(fl_row, "costo_orario")

ld_ore = dw_call_center_risorse.getitemnumber(fl_row, "tot_ore")
ld_minuti = dw_call_center_risorse.getitemnumber(fl_row, "tot_minuti")

if isnull(ld_ore) then ld_ore = 0
if isnull(ld_minuti) then ld_minuti = 0

ld_tempo = ld_ore + ( round(ld_minuti / 60,2) )
ld_sconto_1 = dw_call_center_risorse.getitemnumber(fl_row, "sconto_1")
ld_costi_aggiuntivi = dw_call_center_risorse.getitemnumber(fl_row, "costi_aggiuntivi")

if isnull(ld_costo_ora) then ld_costo_ora = 0
if isnull(ld_tempo) then ld_tempo = 0
if isnull(ld_sconto_1) then ld_sconto_1 = 0
if isnull(ld_costi_aggiuntivi) then ld_costi_aggiuntivi = 0

ld_totale = (ld_costo_ora * ld_tempo)

if ld_totale = 0 then 
	dw_call_center_risorse.setitem(fl_row, "imponibile_iva", ld_costi_aggiuntivi)
	return
else
	if ld_sconto_1 > 0 and not isnull(ld_sconto_1) then
		ld_totale = (ld_totale - (ld_totale * ld_sconto_1 / 100)) + ld_costi_aggiuntivi
	end if
	
	dw_call_center_risorse.setitem(fl_row, "imponibile_iva", round(ld_totale, 2))
end if
dw_call_center_risorse.accepttext()
return
end subroutine

public subroutine wf_calcola_totale_operai (long fl_row);dec{4} ld_costo_ora, ld_tempo, ld_sconto_1, ld_totale, ld_ore, ld_minuti

ld_costo_ora = dw_call_center_operai.getitemnumber(fl_row, "costo_orario")
ld_ore = dw_call_center_operai.getitemnumber(fl_row, "tot_ore")
ld_minuti = dw_call_center_operai.getitemnumber(fl_row, "tot_minuti")

if isnull(ld_ore) then ld_ore = 0
if isnull(ld_minuti) then ld_minuti = 0

ld_tempo = ld_ore + ( round(ld_minuti / 60,2) )
ld_sconto_1 = dw_call_center_operai.getitemnumber(fl_row, "sconto_1")

if isnull(ld_costo_ora) then ld_costo_ora = 0
if isnull(ld_tempo) then ld_tempo = 0
if isnull(ld_sconto_1) then ld_sconto_1 = 0

ld_totale = (ld_costo_ora * ld_tempo)

if ld_totale = 0 then 
	dw_call_center_operai.setitem(fl_row, "imponibile_iva", 0)
	return
else
	if ld_sconto_1 > 0 and not isnull(ld_sconto_1) then
		ld_totale = ld_totale - (ld_totale * ld_sconto_1 / 100)
	end if
	
	dw_call_center_operai.setitem(fl_row, "imponibile_iva", round(ld_totale, 2))
end if
dw_call_center_operai.accepttext()
return
end subroutine

public function decimal wf_carica_costo_operaio (string fs_cod_operaio);string ls_cod_cat_attrezzatura
dec{4} ld_costo_orario

if isnull(fs_cod_operaio) then return 0

select cod_cat_attrezzature
into   :ls_cod_cat_attrezzatura
from   anag_operai
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_operaio = :fs_cod_operaio;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in select anag_operai~r~n"+sqlca.sqlerrtext)
	return 0
end if

if isnull(ls_cod_cat_attrezzatura) then
	g_mb.messagebox("OMNIA", "L'operatore non è associato ad alcuna categoria di costo.")
	return 0
end if

select costo_medio_orario
into   :ld_costo_orario
from   tab_cat_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cat_attrezzature = :ls_cod_cat_attrezzatura;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in select tab_cat_attrezzature~r~n"+sqlca.sqlerrtext)
	return 0
end if

return ld_costo_orario
end function

public function integer wf_operatori_semplice (long fl_anno_registrazione, long fl_num_registrazione);long   ll_i, ll_tot_ore, ll_tot_minuti
string ls_cod_operaio
dec{4} ld_costo_orario, ld_imponibile

for ll_i = 1 to dw_call_center_operai.rowcount()
	
	if ll_i = 1 then continue
	
	ls_cod_operaio = dw_call_center_operai.getitemstring(ll_i, "cod_operaio")
	ll_tot_ore = dw_call_center_operai.getitemnumber(ll_i, "tot_ore")
	ll_tot_minuti = dw_call_center_operai.getitemnumber( ll_i, "tot_minuti")
	ld_costo_orario = dw_call_center_operai.getitemnumber( ll_i, "costo_orario")
	ld_imponibile = dw_call_center_operai.getitemnumber( ll_i, "imponibile_iva")
	
	if isnull(ll_tot_ore) then
		ll_tot_ore = 0
	end if
	
	if isnull(ll_tot_minuti) then
		ll_tot_minuti = 0
	end if
	
	insert into manutenzioni_operai (cod_azienda,
												 anno_registrazione,
												 num_registrazione,
												 progressivo,
												 cod_operaio,
												 costo_operatore,
												 ore,
												 minuti,
												 tot_costo_operaio)
	values                           (:s_cs_xx.cod_azienda,
												 :fl_anno_registrazione,
												 :fl_num_registrazione,
												 :ll_i,
												 :ls_cod_operaio,
												 :ld_costo_orario,
												 :ll_tot_ore,
												 :ll_tot_minuti,
												 :ld_imponibile);
														 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante la registrazione degli operatori della manutenzione: " + sqlca.sqlerrtext)
		return -1
	end if
	
next	

return 0
end function

public subroutine wf_carica_costo_risorse (string fs_cod_cat_risorsa, string fs_cod_risorsa, ref decimal fd_costo_orario, ref decimal fd_costi_aggiuntivi);fd_costo_orario = 0
fd_costi_aggiuntivi = 0

if isnull(fs_cod_risorsa) or isnull(fs_cod_cat_risorsa) then return

select tariffa_std, diritto_chiamata
into   :fd_costo_orario, :fd_costi_aggiuntivi
from   anag_risorse_esterne
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cat_risorse_esterne = :fs_cod_cat_risorsa and
		 cod_risorsa_esterna = :fs_cod_risorsa;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in ricerca risorsa esterna~r~n"+sqlca.sqlerrtext)
	return
end if

return
end subroutine

public subroutine wf_imposta_campi_note ();/*	
QUESTA FUNZIONE PROVVEDE A BLOCCARE L'IMPUTAZIONE DI UN NUMERO DI CARATTERI NEI CAMPI NOTE
MAGGIORE DI QUELLO CHE ACCETTA IL TIPO DI DATO DEL DATABASE.
SE IN QUALCHE GESTIONE PERSONALIZZATA I CAMPI NOTE POSSO ACCETTARE PIU' CARATTERI
E' POSSIBILE IMPOSTARE LA VARIABILE DI ISTANZA IL_LIMITE_CAMPO_NOTE PRIMA DI CHIAMARE
LA SET_DW_OPTION.
*/

integer li_i, li_char
string ls_modify, ls_colonna, ls_str
long ll_limit_campo_note

ll_limit_campo_note = 0

for li_i = 1 to integer(dw_selezione.describe("datawindow.column.count"))
	
   ls_colonna = dw_selezione.describe("#" + string(li_i) + ".name")
	
   if dw_selezione.describe(ls_colonna + ".visible") = "1" and (dw_selezione.describe(ls_colonna + ".edit.style") = "edit" or dw_selezione.describe(ls_colonna + ".edit.style") = "editmask") then
		ls_str = dw_selezione.describe(ls_colonna + ".coltype")
		
      if lower( left(ls_str, 4) ) = "char" then
			li_char = integer(mid(ls_str, 6, len(ls_str) - 6))
			
			// se il tipo di dato prevede più di 100 caratteri.
			if li_char > 100 then
				
				if ll_limit_campo_note = 0 then
					
					choose case f_db()
						case "SYBASE_ASA"
							ls_modify = ls_modify + ls_colonna + ".Edit.limit='32000'~t"
						case "SYBASE_ASE"
							ls_modify = ls_modify + ls_colonna + ".Edit.limit='255'~t"
						case "ORACLE"
							ls_modify = ls_modify + ls_colonna + ".Edit.limit='1999'~t"
						case "MSSQL"
							ls_modify = ls_modify + ls_colonna + ".Edit.limit='255'~t"
					end choose
				else
					ls_modify = ls_modify + ls_colonna + ".Edit.limit='"+string(ll_limit_campo_note)+"'~t"
				end if
				
			end if
		end if
   end if
next

if not isnull(ls_modify) and ls_modify <> "" then
   dw_selezione.modify(ls_modify)
end if
end subroutine

public function integer wf_idl (ref string as_messaggio);string				ls_cod_attrezzatura


dw_selezione.accepttext()
ls_cod_attrezzatura = dw_selezione.getitemstring(1, "cod_attrezzatura")


if ls_cod_attrezzatura="" or isnull(ls_cod_attrezzatura) then
	as_messaggio = "E' necessario selezionare prima un'attrezzatura!"
	return 1
end if



return 0
end function

on w_call_center_conferma_manut.create
int iCurrent
call super::create
this.mle_idl=create mle_idl
this.dw_tipi_man=create dw_tipi_man
this.cb_4_ricambi=create cb_4_ricambi
this.cb_2_ricambi=create cb_2_ricambi
this.cb_4_ris=create cb_4_ris
this.cb_2_ris=create cb_2_ris
this.st_ric_op=create st_ric_op
this.cb_4=create cb_4
this.cb_2=create cb_2
this.cb_3=create cb_3
this.cb_conferma=create cb_conferma
this.dw_selezione=create dw_selezione
this.st_ric_op_risorse=create st_ric_op_risorse
this.dw_call_center_operai_lista=create dw_call_center_operai_lista
this.dw_call_center_risorse_lista=create dw_call_center_risorse_lista
this.cb_3_ricambi=create cb_3_ricambi
this.cb_11_ris=create cb_11_ris
this.cb_3_ris=create cb_3_ris
this.cb_11=create cb_11
this.cb_11_ricambi=create cb_11_ricambi
this.dw_call_center_manut_ricambi=create dw_call_center_manut_ricambi
this.dw_call_center_operai=create dw_call_center_operai
this.dw_call_center_risorse=create dw_call_center_risorse
this.dw_folder=create dw_folder
this.dw_call_center_manut_ricambi_lista=create dw_call_center_manut_ricambi_lista
this.dw_ricerca_ricambi=create dw_ricerca_ricambi
this.cb_retrieve_ricambi=create cb_retrieve_ricambi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.mle_idl
this.Control[iCurrent+2]=this.dw_tipi_man
this.Control[iCurrent+3]=this.cb_4_ricambi
this.Control[iCurrent+4]=this.cb_2_ricambi
this.Control[iCurrent+5]=this.cb_4_ris
this.Control[iCurrent+6]=this.cb_2_ris
this.Control[iCurrent+7]=this.st_ric_op
this.Control[iCurrent+8]=this.cb_4
this.Control[iCurrent+9]=this.cb_2
this.Control[iCurrent+10]=this.cb_3
this.Control[iCurrent+11]=this.cb_conferma
this.Control[iCurrent+12]=this.dw_selezione
this.Control[iCurrent+13]=this.st_ric_op_risorse
this.Control[iCurrent+14]=this.dw_call_center_operai_lista
this.Control[iCurrent+15]=this.dw_call_center_risorse_lista
this.Control[iCurrent+16]=this.cb_3_ricambi
this.Control[iCurrent+17]=this.cb_11_ris
this.Control[iCurrent+18]=this.cb_3_ris
this.Control[iCurrent+19]=this.cb_11
this.Control[iCurrent+20]=this.cb_11_ricambi
this.Control[iCurrent+21]=this.dw_call_center_manut_ricambi
this.Control[iCurrent+22]=this.dw_call_center_operai
this.Control[iCurrent+23]=this.dw_call_center_risorse
this.Control[iCurrent+24]=this.dw_folder
this.Control[iCurrent+25]=this.dw_call_center_manut_ricambi_lista
this.Control[iCurrent+26]=this.dw_ricerca_ricambi
this.Control[iCurrent+27]=this.cb_retrieve_ricambi
end on

on w_call_center_conferma_manut.destroy
call super::destroy
destroy(this.mle_idl)
destroy(this.dw_tipi_man)
destroy(this.cb_4_ricambi)
destroy(this.cb_2_ricambi)
destroy(this.cb_4_ris)
destroy(this.cb_2_ris)
destroy(this.st_ric_op)
destroy(this.cb_4)
destroy(this.cb_2)
destroy(this.cb_3)
destroy(this.cb_conferma)
destroy(this.dw_selezione)
destroy(this.st_ric_op_risorse)
destroy(this.dw_call_center_operai_lista)
destroy(this.dw_call_center_risorse_lista)
destroy(this.cb_3_ricambi)
destroy(this.cb_11_ris)
destroy(this.cb_3_ris)
destroy(this.cb_11)
destroy(this.cb_11_ricambi)
destroy(this.dw_call_center_manut_ricambi)
destroy(this.dw_call_center_operai)
destroy(this.dw_call_center_risorse)
destroy(this.dw_folder)
destroy(this.dw_call_center_manut_ricambi_lista)
destroy(this.dw_ricerca_ricambi)
destroy(this.cb_retrieve_ricambi)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag
windowobject lw_oggetti[], lw_oggetti_2[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)
save_on_close(c_socnosave)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
il_anno_ric = s_cs_xx.parametri.parametro_d_1
il_num_ric  = s_cs_xx.parametri.parametro_d_2
il_anno_nc  = s_cs_xx.parametri.parametro_d_3
il_num_nc   = s_cs_xx.parametri.parametro_d_4

dw_call_center_operai_lista.settransobject(sqlca)
dw_call_center_operai.settransobject(sqlca)

dw_call_center_risorse_lista.settransobject(sqlca)
dw_call_center_risorse.settransobject(sqlca)

dw_call_center_manut_ricambi_lista.settransobject(sqlca)
dw_call_center_manut_ricambi.settransobject(sqlca)

dw_tipi_man.settransobject(sqlca)
dw_tipi_man.setrowfocusindicator(Hand!)

save_on_close(c_socnosave)

lw_oggetti[1] = dw_call_center_risorse_lista
lw_oggetti[2] = dw_call_center_risorse
lw_oggetti[3] = cb_11_ris
lw_oggetti[4] = cb_2_ris
lw_oggetti[5] = cb_3_ris
lw_oggetti[6] = cb_4_ris
//lw_oggetti[7] = st_ric_op_risorse
dw_folder.fu_assigntab(2, "Risorse Esterne", lw_oggetti[])

lw_oggetti_2[1] = dw_call_center_manut_ricambi_lista
lw_oggetti_2[2] = dw_call_center_manut_ricambi
lw_oggetti_2[3] = cb_2_ricambi
lw_oggetti_2[4] = cb_11_ricambi
lw_oggetti_2[5] = cb_3_ricambi
lw_oggetti_2[6] = cb_4_ricambi
lw_oggetti_2[7] = dw_ricerca_ricambi
//lw_oggetti_2[8] = cb_ricerca_ricambi
lw_oggetti_2[8] = cb_retrieve_ricambi
dw_folder.fu_assigntab(3, "Ricambi", lw_oggetti_2[])

lw_oggetti[1] = dw_call_center_operai_lista
lw_oggetti[2] = dw_call_center_operai
lw_oggetti[3] = cb_11
lw_oggetti[4] = cb_2
lw_oggetti[5] = cb_3
lw_oggetti[6] = cb_4
lw_oggetti[7] = st_ric_op
dw_folder.fu_assigntab(1, "Operatori", lw_oggetti[])

dw_folder.fu_foldercreate(3,3)
dw_folder.fu_selecttab(1)

select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
if (sqlca.sqlcode = 0) and ls_flag = "S" then
	ib_global_service = true
end if


event post ue_limit()

	

end event

event open;call super::open;long			l_Error, ll_riga, ll_ore_totali, ll_minuti_totali, li_risposta
string		ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna
datetime		ldt_data_inizio, ldt_data_fine, ldt_oggi
time			lt_inizio, lt_fine
dec{4}		ld_costo_orario, ld_costi_aggiuntivi


ldt_oggi = datetime(today(), 00:00:00)

l_Error = dw_call_center_operai_lista.Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

l_Error = dw_call_center_risorse_lista.Retrieve(s_cs_xx.cod_azienda, ldt_oggi)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

l_Error = dw_call_center_manut_ricambi_lista.Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


if s_cs_xx.parametri.parametro_s_1 <> "S" and not isnull(s_cs_xx.parametri.parametro_s_3) and len(s_cs_xx.parametri.parametro_s_3) > 0 then
	ls_cod_operaio = s_cs_xx.parametri.parametro_s_3
	
	ll_riga = dw_call_center_operai.insertrow(0)
	
	dw_call_center_operai.setitem(ll_riga,"cod_azienda",s_cs_xx.cod_azienda)
	dw_call_center_operai.setitem(ll_riga,"cod_operaio",ls_cod_operaio)
	dw_call_center_operai.setitem(ll_riga,"data_inizio", s_cs_xx.parametri.parametro_data_1)
	dw_call_center_operai.setitem(ll_riga,"ora_inizio", s_cs_xx.parametri.parametro_data_2)
	dw_call_center_operai.setitem(ll_riga,"data_fine", s_cs_xx.parametri.parametro_data_3)
	dw_call_center_operai.setitem(ll_riga,"ora_fine", s_cs_xx.parametri.parametro_data_4)
	
	
	li_risposta = f_date_h_m(s_cs_xx.parametri.parametro_data_1,s_cs_xx.parametri.parametro_data_2,s_cs_xx.parametri.parametro_data_3,s_cs_xx.parametri.parametro_data_4, ll_ore_totali, ll_minuti_totali)
	dw_call_center_operai.setitem(ll_riga,"tot_ore",ll_ore_totali)
	dw_call_center_operai.setitem(ll_riga,"tot_minuti",ll_minuti_totali)	
	
	// *** Michela 20/07/2006: aggiungo i costi
	
	ld_costo_orario = wf_carica_costo_operaio(ls_cod_operaio)
	dw_call_center_operai.setitem(ll_riga,"costo_orario",ld_costo_orario)	
	wf_calcola_totale_operai(ll_riga)
	dw_call_center_operai.postevent("ue_totale_riga")	
	
	if ib_global_service = false then
		dw_selezione.setitem( 1, "cod_attrezzatura", s_cs_xx.parametri.parametro_s_8)
		
		dw_tipi_man.retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_8)
		
	end if
	
end if

//sulla dw_selezione
wf_imposta_campi_note()


// Michele 19/10/2007 - caricamento automatico anche della risorsa esterna (se specificata)
if s_cs_xx.parametri.parametro_s_1 <> "S" and ( not isnull(s_cs_xx.parametri.parametro_s_10) or not isnull(s_cs_xx.parametri.parametro_s_11)) then
	
	if s_cs_xx.parametri.parametro_s_10 = "" then setnull(s_cs_xx.parametri.parametro_s_10)
	if s_cs_xx.parametri.parametro_s_11 = "" then setnull(s_cs_xx.parametri.parametro_s_11)
	
	ls_cod_cat_risorse_esterne = s_cs_xx.parametri.parametro_s_10
	ls_cod_risorsa_esterna = s_cs_xx.parametri.parametro_s_11
	
	ll_riga = dw_call_center_risorse.insertrow(0)
	
	if isnull(ls_cod_cat_risorse_esterne) then
		
		select cod_cat_risorse_esterne
		into	 :ls_cod_cat_risorse_esterne
		from	 anag_risorse_esterne
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 cod_risorsa_esterna = : ls_cod_risorsa_esterna;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "OMNIA", "Errore durante il controllo della categoria risorsa:" + sqlca.sqlerrtext, stopsign!)
		end if
				 				 
	end if
	
	
	dw_call_center_risorse.setitem(ll_riga,"cod_azienda",s_cs_xx.cod_azienda)
	dw_call_center_risorse.setitem(ll_riga,"cod_cat_risorsa",ls_cod_cat_risorse_esterne)
	dw_call_center_risorse.setitem(ll_riga,"cod_risorsa",ls_cod_risorsa_esterna)
	dw_call_center_risorse.setitem(ll_riga,"data_inizio", s_cs_xx.parametri.parametro_data_1)
	dw_call_center_risorse.setitem(ll_riga,"ora_inizio", s_cs_xx.parametri.parametro_data_2)
	dw_call_center_risorse.setitem(ll_riga,"data_fine", s_cs_xx.parametri.parametro_data_3)
	dw_call_center_risorse.setitem(ll_riga,"ora_fine", s_cs_xx.parametri.parametro_data_4)
	
	
	li_risposta = f_date_h_m(s_cs_xx.parametri.parametro_data_1,s_cs_xx.parametri.parametro_data_2,s_cs_xx.parametri.parametro_data_3,s_cs_xx.parametri.parametro_data_4, ll_ore_totali, ll_minuti_totali)
	dw_call_center_risorse.setitem(ll_riga,"tot_ore",ll_ore_totali)
	dw_call_center_risorse.setitem(ll_riga,"tot_minuti",ll_minuti_totali)	
	
	// COSTI
	wf_carica_costo_risorse(ls_cod_cat_risorse_esterne,ls_cod_risorsa_esterna,ld_costo_orario,ld_costi_aggiuntivi)
	dw_call_center_risorse.setitem(ll_riga,"costo_orario",ld_costo_orario)	
	dw_call_center_risorse.setitem(ll_riga,"costi_aggiuntivi",ld_costi_aggiuntivi)
	
	wf_calcola_totale_risorse(ll_riga)
	dw_call_center_risorse.postevent("ue_totale_riga")
	
	if ib_global_service = false then
		dw_selezione.setitem( 1, "cod_attrezzatura", s_cs_xx.parametri.parametro_s_8)
		
		dw_tipi_man.retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_8)
	end if
	
end if
end event

event timer;call super::timer;long 	 ll_i

string ls_cognome, ls_nome, ls_op


setpointer(hourglass!)

timer(0)

st_ric_op.text = is_ricerca

for ll_i = 1 to dw_call_center_operai_lista.rowcount()
	
	ls_cognome = dw_call_center_operai_lista.getitemstring(ll_i,"cognome")
	
	if isnull(ls_cognome) then
		ls_cognome = ""
	end if
	
	ls_nome = dw_call_center_operai_lista.getitemstring(ll_i,"nome")
	
	if isnull(ls_nome) then
		ls_nome = ""
	end if
	
	ls_op = upper(trim(ls_cognome + ls_nome))
	
	if pos(ls_op,is_ricerca,1) = 1 then
		dw_call_center_operai_lista.setrow(ll_i)
		dw_call_center_operai_lista.scrolltorow(ll_i)
		exit
	end if
	
next

is_ricerca = ""

st_ric_op.text = is_ricerca

setpointer(arrow!)
end event

type mle_idl from multilineedit within w_call_center_conferma_manut
integer x = 2587
integer y = 636
integer width = 1829
integer height = 520
integer taborder = 21
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
boolean vscrollbar = true
boolean autovscroll = true
boolean displayonly = true
end type

type dw_tipi_man from datawindow within w_call_center_conferma_manut
integer x = 2587
integer y = 24
integer width = 1829
integer height = 600
integer taborder = 11
string title = "none"
string dataobject = "d_call_center_conferma_tipi_man"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;string			ls_idl

if currentrow>0 then
	selectrow(0, false)
	selectrow(currentrow, true)
	
	ls_idl = getitemstring(currentrow, "modalita_esecuzione")
	mle_idl.text = ls_idl
else
	mle_idl.text = ""
end if
end event

event buttonclicked;long 				ll_index
string				ls_testo, ls_sel, ls_temp


accepttext()


choose case dwo.name
	case "b_incolla"
		
		ls_testo = ""
		
		for ll_index=1 to rowcount()
			ls_sel = getitemstring(ll_index, "selezionato")
			
			if ls_sel="S" then
				//solo se ci sono IDL
				ls_temp = getitemstring(ll_index, "modalita_esecuzione")
				if ls_temp<>"" and not isnull(ls_temp) then
					
					ls_testo += getitemstring(ll_index, "cod_tipo_manutenzione")
					
					//se c'è una descrizione del tipo manutenzione metto anche quella ...
					if getitemstring(ll_index, "des_tipo_manutenzione")<>"" and not isnull(getitemstring(ll_index, "des_tipo_manutenzione")) then
						ls_testo += " - "+getitemstring(ll_index, "des_tipo_manutenzione")
					end if
					
					//infine vado accapo e incollo, le IDL
					ls_testo += "~r~n~r~n" + ls_temp + "~r~n~r~n~r~n"
				end if
			end if
		next
		
		if ls_testo<>"" and not isnull(ls_testo) then
			dw_selezione.setcolumn("descrizione")
			
			ll_index = dw_selezione.position()
			dw_selezione.ReplaceText(ls_testo )
		end if
		
		
end choose
end event

event clicked;string			ls_idl

if row>0 then
	selectrow(0, false)
	selectrow(row, true)
	
	ls_idl = getitemstring(row, "modalita_esecuzione")
	mle_idl.text = ls_idl
else
	mle_idl.text = ""
end if
end event

type cb_4_ricambi from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 2000
integer width = 123
integer height = 120
integer taborder = 82
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<<"
end type

event clicked;dw_call_center_manut_ricambi.reset()
end event

type cb_2_ricambi from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 1840
integer width = 123
integer height = 120
integer taborder = 72
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<"
end type

event clicked;long   ll_riga

if dw_call_center_manut_ricambi.getrow() > 0 then
	
	ll_riga = dw_call_center_manut_ricambi.getrow()
	dw_call_center_manut_ricambi.deleterow(ll_riga)	

end if

end event

type cb_4_ris from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 2000
integer width = 123
integer height = 120
integer taborder = 72
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<<"
end type

event clicked;dw_call_center_risorse.reset()
end event

type cb_2_ris from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 1840
integer width = 123
integer height = 120
integer taborder = 62
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<"
end type

event clicked;long   ll_riga

if dw_call_center_risorse.getrow() > 0 then
	
	ll_riga = dw_call_center_risorse.getrow()
	dw_call_center_risorse.deleterow(ll_riga)	

end if

end event

type st_ric_op from statictext within w_call_center_conferma_manut
integer x = 69
integer y = 2176
integer width = 1509
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
boolean focusrectangle = false
end type

type cb_4 from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 2000
integer width = 123
integer height = 120
integer taborder = 62
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<<"
end type

event clicked;dw_call_center_operai.reset()
end event

type cb_2 from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 1840
integer width = 123
integer height = 120
integer taborder = 52
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<"
end type

event clicked;long   ll_riga

if dw_call_center_operai.getrow() > 0 then
	
	ll_riga = dw_call_center_operai.getrow()
	dw_call_center_operai.deleterow(ll_riga)	

end if

end event

type cb_3 from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 1480
integer width = 123
integer height = 120
integer taborder = 32
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">>"
end type

event clicked;long ll_i
datetime ldt_null
long     ll_riga
string   ls_cod_operaio
datetime ldt_data_inizio, ldt_data_fine
time     lt_inizio, lt_fine
long     ll_n_ore, ll_n_ore_2, ll_minuti_fin, ll_ore_fin
dec{4}   ld_costo_orario


for ll_i = 1 to dw_call_center_operai_lista.rowcount()
	dw_call_center_operai_lista.setrow(ll_i)

	if dw_call_center_operai_lista.getrow() > 0 then
		
		ls_cod_operaio = dw_call_center_operai_lista.getitemstring(dw_call_center_operai_lista.getrow(),"cod_operaio")
		if dw_call_center_operai.find("cod_operaio = '" + ls_cod_operaio + "'", 1, dw_call_center_operai.rowcount() ) > 0 then
			beep(10)
			return
		end if		
	
		setnull(ldt_null)
		
		ll_riga = dw_call_center_operai.insertrow(0)
		dw_call_center_operai.setitem(ll_riga,"cod_azienda",s_cs_xx.cod_azienda)
		dw_call_center_operai.setitem(ll_riga,"cod_operaio",ls_cod_operaio)
		ld_costo_orario = wf_carica_costo_operaio(ls_cod_operaio)
		dw_call_center_risorse.setitem(ll_riga,"costo_orario",ld_costo_orario)	
		wf_calcola_totale_operai(ll_riga)
		
		if ll_riga > 1 then
			dw_call_center_operai.setitem(ll_riga,"data_inizio",dw_call_center_operai.getitemdatetime(ll_riga - 1,"data_inizio"))
			dw_call_center_operai.setitem(ll_riga,"ora_inizio",dw_call_center_operai.getitemdatetime(ll_riga - 1,"ora_inizio"))
			dw_call_center_operai.setitem(ll_riga,"data_fine",dw_call_center_operai.getitemdatetime(ll_riga - 1,"data_fine"))
			dw_call_center_operai.setitem(ll_riga,"ora_fine",dw_call_center_operai.getitemdatetime(ll_riga - 1,"ora_fine"))
			dw_call_center_operai.setitem(ll_riga,"tot_ore",dw_call_center_operai.getitemnumber(ll_riga - 1,"tot_ore"))
			dw_call_center_operai.setitem(ll_riga,"tot_minuti",dw_call_center_operai.getitemnumber(ll_riga - 1,"tot_minuti"))
		else
			dw_call_center_operai.setitem(ll_riga,"data_inizio", ldt_null)
			dw_call_center_operai.setitem(ll_riga,"ora_inizio", ldt_null)
			dw_call_center_operai.setitem(ll_riga,"data_fine", ldt_null)
			dw_call_center_operai.setitem(ll_riga,"ora_fine", ldt_null)
			dw_call_center_operai.setitem(ll_riga,"tot_ore",0)
			dw_call_center_operai.setitem(ll_riga,"tot_minuti",0)
		end if
	end if

next
end event

type cb_conferma from commandbutton within w_call_center_conferma_manut
integer x = 4005
integer y = 2176
integer width = 366
integer height = 80
integer taborder = 2
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;string		ls_cod_tipo_manutenzione, ls_cod_attrezzatura, ls_cod_operaio,ls_des_ricambio_non_codificato,ls_modalita_esecuzione, &
				ls_cod_ricambio,ls_flag_ricambio_codificato, ls_note_idl, ls_flag_blocco,ls_cod_operaio_cur, &
				ls_cod_risorsa, ls_cod_cat_risorsa, ls_flag_gen_manutenzioni, ls_errore, ls_note, ls_appo, ls_descrizione_fase, &
				ls_descrizione_manut, ls_titolo_fase, ls_descrizione_manut_1, ls_note_idl_1, ls_des_ricambio,&
				ls_cod_tipo_richiesta, ls_des_tipo_manutenzione, ls_str, ls_nome, ls_cognome, ls_des_risorsa, &
				ls_cod_guasto, ls_controllo,ls_flag_eseguito,ls_flag_spostamento
		 
long			ll_anno_registrazione, ll_num_registrazione, ll_tot_ore, ll_tot_minuti, ll_n, li_risposta, ll_ore_op, ll_min_op, &
				ll_num_manutenzioni_esistenti, ll_prog_fase_effettiva, ll_i, ll_aggiorna_data, ll_ret,  &
				ll_somma_ore_operai, ll_somma_minuti_operai, ll_somma_ore_risorse, ll_somma_minuti_risorse

//donato 19-06-2008 per calcolare il max in tabella manutenzioni_ricambi prima degli INSERT
long			ll_max_manut_ric

dec{4}		ld_quan_ricambio,ld_costo_unitario_ricambio,ld_operaio_minuti,ld_operaio_ore, &
				ld_n_ore,ld_n_ore_2,ld_minuti_fin,ld_ore_fin,ld_costo_orario,ld_imponibile_iva, ld_sconto_1, &
				ld_sconto_2, ld_costi_aggiuntivi, ld_prezzo_ricambio

date			ldt_null

datetime	ldt_data_registrazione, lt_tempo_previsto, ldt_inizio_intervento, ldt_fine_intervento, ldt_data_blocco, &
				ldt_data_min_fase, ldt_data_max_fase, ldt_data_max, ldt_ora_max, ldt_data_min_a, ldt_data_max_a, &
				ldt_data_inizio, ldt_ora_inizio, ldt_data_fine, ldt_ora_fine, ldt_inizio, ldt_fine, &
				ldt_operai_data_inizio, ldt_operai_ora_inizio, ldt_operai_data_fine, ldt_operai_ora_fine


dw_selezione.accepttext()
dw_call_center_operai.accepttext()
dw_call_center_risorse.accepttext()

ls_cod_attrezzatura = dw_selezione.getitemstring(1, "cod_attrezzatura")

setnull(ldt_data_registrazione)

if dw_call_center_operai.rowcount() < 1  and dw_call_center_risorse.rowcount() < 1 then
	g_mb.messagebox("Omnia", "Attenzione: indicare almento un operatore o una risorsa esterna !")
	return	
end if

ls_cod_guasto = dw_selezione.getitemstring(1, "cod_guasto")

// *** modifica michela 29/05/2006: su richiesta di davide baggio non è più obbligatorio il guasto

//if not isnull(ls_cod_attrezzatura) and len(ls_cod_attrezzatura) > 0 then
//	if isnull(ls_cod_guasto) or len(ls_cod_guasto) < 1 then
//		messagebox("Omnia", "E' obbligatorio indicare un guasto.")
//		return	
//	end if
//
//	select cod_guasto
//	into   :ls_controllo
//	from   tab_guasti_generici
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_guasto = :ls_cod_guasto;
//	if sqlca.sqlcode <> 0 then
//		messagebox("Omnia", "il guasto indicato è inesitente.")
//		return	
//	end if
//end if

ls_descrizione_fase = dw_selezione.getitemstring(1, "descrizione")
ls_descrizione_manut = s_cs_xx.parametri.parametro_s_5

// ----- conto le manutenzioni associate alla richiesta con l'attrezzatura ---
if not isnull(ls_cod_attrezzatura) then
	select count(*)
	into   :ll_num_manutenzioni_esistenti
	from   manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_richiesta = :s_cs_xx.parametri.parametro_d_1 and
			 num_reg_richiesta = :s_cs_xx.parametri.parametro_d_2 and
			 cod_attrezzatura = :ls_cod_attrezzatura;
else
	select count(*)
	into   :ll_num_manutenzioni_esistenti
	from   manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_richiesta = :s_cs_xx.parametri.parametro_d_1 and
			 num_reg_richiesta = :s_cs_xx.parametri.parametro_d_2 and
			 cod_attrezzatura is null;
end if

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore nella ricerca delle manutenzioni esistenti: " + sqlca.sqlerrtext)
	return -1
end if

// ----- ricerco il codice tipo manutenzione da utilizzare per questa richiesta
ls_flag_eseguito = "S"
if not isnull(ls_cod_attrezzatura) then

	select cod_tipo_richiesta 
	into   :ls_cod_tipo_richiesta
	from   tab_richieste
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :il_anno_ric and
	       num_registrazione = :il_num_ric;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in ricerca codice tipo richiesta nella tab_richieste."+sqlca.sqlerrtext)
		return -1
	end if
	
	select cod_tipo_manutenzione, des_tipo_manutenzione, flag_spostamento
	into   :ls_cod_tipo_manutenzione, :ls_des_tipo_manutenzione, :ls_flag_spostamento
	from   tab_tipi_richieste
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_richiesta = :ls_cod_tipo_richiesta;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in ricerca tipo manutenzione nella tab_tipi_richieste."+sqlca.sqlerrtext)
		return -1
	end if
	
	if isnull(ls_cod_tipo_manutenzione) then
		g_mb.messagebox("Omnia", "Codice tipo manutenzione non impostato in tabella tipi richieste: impossibile procedere!",stopsign!)
		return -1
	end if
	
	if isnull(ls_des_tipo_manutenzione) then
		g_mb.messagebox("Omnia", "Descrizione tipo manutenzione non impostato in tabella tipi richieste: impossibile procedere!",stopsign!)
		return -1
	end if
	
	// tento sempre la creazione della tipologia di manutenzione
	

	insert into tab_tipi_manutenzione
		(cod_azienda, 
		 cod_attrezzatura, 
		 cod_tipo_manutenzione, 
		 des_tipo_manutenzione)
	values
		(:s_cs_xx.cod_azienda, 
		 :ls_cod_attrezzatura, 
		 :ls_cod_tipo_manutenzione, 
		 :ls_des_tipo_manutenzione);
		 
	if sqlca.sqlcode <> 0 then
		// nessun controllo errore perchè in effetti potrebbe già esistere
	end if
	
	select modalita_esecuzione, 
			 quan_ricambio, 
			 costo_unitario_ricambio,
			 flag_ricambio_codificato,
			 cod_ricambio,
			 des_ricambio_non_codificato,
			 tempo_previsto
	into   :ls_modalita_esecuzione,
			 :ld_quan_ricambio,
			 :ld_costo_unitario_ricambio,
			 :ls_flag_ricambio_codificato,
			 :ls_cod_ricambio,
			 :ls_des_ricambio_non_codificato,
			 :lt_tempo_previsto
	from   tab_tipi_manutenzione
	where  cod_azienda =:s_cs_xx.cod_azienda and
			 cod_tipo_manutenzione =: ls_cod_tipo_manutenzione and 
			 cod_attrezzatura =:ls_cod_attrezzatura;
	
	if sqlca.sqlcode <> 0 then 		
		g_mb.messagebox("Omnia","Errore nel ricerca del tipo manutenzione (tab_tipi_manutenznioni)~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext )
		rollback;
		return -1
	end if
	
	if ls_flag_spostamento = "S" then
		ls_flag_eseguito = "N"
	end if
	
else
	
	setnull(ls_cod_tipo_manutenzione)
	setnull(ls_des_tipo_manutenzione)
	setnull(ls_modalita_esecuzione)
	setnull(ld_quan_ricambio)
	setnull(ld_costo_unitario_ricambio)
	ls_flag_ricambio_codificato = "S"
	setnull(ls_cod_ricambio)
	setnull(ls_des_ricambio_non_codificato)
	setnull(lt_tempo_previsto)
	
end if

// -----------------------    note idl e descrizione

if isnull(ls_descrizione_fase) then ls_descrizione_fase = ""
if isnull(ls_modalita_esecuzione) then ls_modalita_esecuzione = ""

if isnull(ls_des_tipo_manutenzione) then
	ls_descrizione_manut_1 = "MANUTENZIONE SU RICHIESTA"
else
	ls_descrizione_manut_1 = ls_des_tipo_manutenzione
end if

if not isnull(ls_cod_attrezzatura) then
	// -------------------- controllo se l'attrezzatura è stata bloccata o dismessa --------------- //
	select flag_blocco,
			 data_blocco
	into   :ls_flag_blocco,
			 :ldt_data_blocco
	from   anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :ls_cod_attrezzatura;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in ricerca apparecchiatura~nr" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
else
	ls_flag_blocco = "N"
	setnull(ldt_data_blocco)
end if

// ------------------ controllo che tutti gli operatori abbiano data-ora inizio/fine 
for ll_n = 1 to dw_call_center_operai.rowcount()
	ll_ore_op = dw_call_center_operai.getitemnumber(ll_n, "tot_ore")
	ll_min_op = dw_call_center_operai.getitemnumber(ll_n, "tot_minuti")
	if (ll_ore_op = 0 or ll_ore_op < 0) and (ll_min_op = 0 or ll_min_op < 0) then
		setnull(ls_appo)
		ls_appo = dw_call_center_operai.getitemstring(ll_n, "cod_operaio")
		g_mb.messagebox("Omnia","Attenzione: controllare i tempi dell'operatore numero " + ls_appo)
		rollback;
		return -1
	else
		ldt_inizio = dw_call_center_operai.getitemdatetime(ll_n, "data_inizio")
		ldt_fine = dw_call_center_operai.getitemdatetime(ll_n, "data_fine")
		if isnull(ldt_inizio) or isnull(ldt_fine) then
			setnull(ls_appo)
			ls_appo = dw_call_center_operai.getitemstring(ll_n, "cod_operaio")
			g_mb.messagebox("Omnia","Attenzione: controllare i tempi dell'operatore numero " + ls_appo)
			rollback;
			return -1		
		end if
	end if
next


// ------------------ controllo che tutte le risorse esterne abbiano data-ora inizio/fine 
for ll_n = 1 to dw_call_center_risorse.rowcount()
	ll_ore_op = dw_call_center_risorse.getitemnumber(ll_n, "tot_ore")
	ll_min_op = dw_call_center_risorse.getitemnumber(ll_n, "tot_minuti")
	if (ll_ore_op = 0 or ll_ore_op < 0) and (ll_min_op = 0 or ll_min_op < 0) then
		setnull(ls_appo)
		ls_appo = dw_call_center_risorse.getitemstring(ll_n, "cod_risorsa")
		if isnull(ls_appo) then ls_appo = "<Risorsa non codificata>"
		g_mb.messagebox("Omnia","Attenzione: controllare i tempi della risorsa " + ls_appo)
		rollback;
		return -1
	else
		ldt_inizio = dw_call_center_risorse.getitemdatetime(ll_n, "data_inizio")
		ldt_fine = dw_call_center_risorse.getitemdatetime(ll_n, "data_fine")
		if isnull(ldt_inizio) or isnull(ldt_fine) then
			setnull(ls_appo)
			ls_appo = dw_call_center_risorse.getitemstring(ll_n, "cod_risorsa")
			if isnull(ls_appo) then ls_appo = "<Risorsa non codificata>"
			g_mb.messagebox("Omnia","Attenzione: controllare i tempi della risorsa esterna: " + ls_appo)
			rollback;
			return -1		
		end if
	end if
next



// ------------------ la data di blocco se esiste, deve essere > della fine dell'intervento
if ls_flag_blocco = "S" then
	for ll_n = 1 to dw_call_center_operai.rowcount()
		ldt_fine_intervento = datetime(date(dw_call_center_operai.getitemdatetime(ll_n, "data_fine")), time(dw_call_center_operai.getitemdatetime(ll_n, "ora_fine")))
		if ldt_data_blocco <= ldt_fine_intervento then
			g_mb.messagebox("Omnia","Attenzione: apparecchiatura dismessa dal " + string(ldt_data_blocco,"dd/mm/yyyy"))
			rollback;
			return -1
		end if
	next
end if

// ------------------ la data di blocco se esiste, deve essere > della fine dell'intervento
if ls_flag_blocco = "S" then
	for ll_n = 1 to dw_call_center_risorse.rowcount()
		ldt_fine_intervento = datetime(date(dw_call_center_risorse.getitemdatetime(ll_n, "data_fine")), time(dw_call_center_risorse.getitemdatetime(ll_n, "ora_fine")))
		if ldt_data_blocco <= ldt_fine_intervento then
			g_mb.messagebox("Omnia","Attenzione: apparecchiatura dismessa dal " + string(ldt_data_blocco,"dd/mm/yyyy"))
			rollback;
			return -1
		end if
	next
end if

ll_aggiorna_data = 0


if ib_global_service then		
	
	// -------------------------------------------------------------------------------------------- //
	//			 modalità global service
	// ---    se ho zero manutenzioni ---- ne creo una nuova 
	// -------------------------------------------------------------------------------------------- //
	
	if ll_num_manutenzioni_esistenti = 0 then
		
		ll_aggiorna_data = 1
		
		ls_cod_operaio = s_cs_xx.parametri.parametro_s_3
		
		if ls_cod_operaio = "" then setnull(ls_cod_operaio)
		
		ll_prog_fase_effettiva = 1
		ll_anno_registrazione = f_anno_esercizio()
		
		select max(num_registrazione)
		into :ll_num_registrazione
		from manutenzioni
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_registrazione;
		if ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
			ll_num_registrazione = 1 
		else
			ll_num_registrazione ++
		end if
		
		//************************************** Aggiungo la descrizione della richiesta ***************
		if isnull(ls_des_tipo_manutenzione) then
			ls_note_idl_1 = "RICHIESTA:~r~n"
		else
			ls_note_idl_1 = ls_des_tipo_manutenzione + ":~r~n"
		end if
		
		if not isnull(s_cs_xx.parametri.parametro_s_4) and len(s_cs_xx.parametri.parametro_s_4) > 0 then
			ls_note_idl_1 = ls_note_idl_1 + s_cs_xx.parametri.parametro_s_4 
		end if
		//*****************************************************
		
		INSERT INTO tab_guasti  
				( cod_azienda,   
				  cod_attrezzatura,   
				  cod_guasto,   
				  des_guasto,   
				  cod_tipo_manutenzione,   
				  punteggio_guasto,   
				  note )  
		 select cod_azienda,   
				  :ls_cod_attrezzatura,   
				  cod_guasto,   
				  des_guasto,   
				  null,   
				  0,   
				  null
		from tab_guasti_generici
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_guasto = :ls_cod_guasto   ;
	
		//Donato 15/05/2009 chiedere se l'intervento deve essere creato già eseguito o lasciato aperto
		li_risposta = g_mb.messagebox("OMNIA", "Chiudere l'intervento da generare?", Exclamation!, YesNo!, 2)
		if li_risposta = 2 then
			ls_flag_eseguito = "N"
		else
			ls_flag_eseguito = "S"
		end if
		//fine modifica -------------------------------------------------------------------------------------------------------

		insert into   manutenzioni
				 (cod_azienda,
				 anno_registrazione,
				 num_registrazione,
				 des_intervento,
				 data_registrazione,
				 cod_attrezzatura,
				 cod_tipo_manutenzione,
				 flag_ordinario,
				 cod_operaio,
				 anno_non_conf,
				 num_non_conf,
				 note_idl,
				 quan_ricambio,
				 costo_unitario_ricambio,
				 cod_ricambio,
				 flag_ricambio_codificato,
				 ricambio_non_codificato,
				 operaio_minuti,
				 operaio_ore,
				 flag_richieste_telefoniche,
				 flag_richieste_cartacee,
				 flag_risc_giro_isp,
				 data_inizio_intervento,
				 ora_inizio_intervento,
				 data_fine_intervento,
				 ora_fine_intervento,
				 flag_eseguito,
				 anno_reg_richiesta,
				 num_reg_richiesta,
				 flag_straordinario,
				 cod_guasto)
		values (:s_cs_xx.cod_azienda,
				 :ll_anno_registrazione,
				 :ll_num_registrazione,
				 :ls_descrizione_manut_1,
				 :ldt_fine_intervento,
				 :ls_cod_attrezzatura,
				 :ls_cod_tipo_manutenzione,
				 'N',
				 :ls_cod_operaio,
				 :il_anno_nc,
				 :il_num_nc,
				 :ls_note_idl_1,
				 :ld_quan_ricambio,
				 :ld_costo_unitario_ricambio,
				 :ls_cod_ricambio,
				 :ls_flag_ricambio_codificato,
				 :ls_des_ricambio_non_codificato,
				 :ld_operaio_minuti,
				 :ld_operaio_ore,
				 'S',
				 'N',
				 'N',
				 :s_cs_xx.parametri.parametro_data_1,
				 :s_cs_xx.parametri.parametro_data_2,
				 :s_cs_xx.parametri.parametro_data_3,
				 :s_cs_xx.parametri.parametro_data_4,
				 :ls_flag_eseguito,
				 :s_cs_xx.parametri.parametro_d_1,
				 :s_cs_xx.parametri.parametro_d_2,
				 :s_cs_xx.parametri.parametro_s_7,
				 :ls_cod_guasto);
				  
		if sqlca.sqlcode <> 0 then 		
			g_mb.messagebox("Omnia","Errore nella registrazione della manutenzione.~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext + &
						  "~nAnno Registrazione " + string(ll_anno_registrazione) + " Numero Registrazione " + string(ll_num_registrazione))
			rollback;
			return -1
		end if
	else
	// ------------ esiste già una manutenzione, quindi vado a creare una nuova fase della manutenzione -----
	// ------------ dopo che so anno e numero naturalmente ----
		
		if not isnull(ls_cod_attrezzatura) then
			select anno_registrazione, num_registrazione
			into   :ll_anno_registrazione, :ll_num_registrazione
			from   manutenzioni
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_reg_richiesta = :il_anno_ric and
					 num_reg_richiesta = :il_num_ric and
					 cod_attrezzatura = :ls_cod_attrezzatura;
		else
			select anno_registrazione, num_registrazione
			into   :ll_anno_registrazione, :ll_num_registrazione
			from   manutenzioni
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_reg_richiesta = :il_anno_ric and
					 num_reg_richiesta = :il_num_ric and
					 cod_attrezzatura is null;
		end if
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore errore in selezione anno/numero manutenzione esistente: " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
	
		select max(prog_fase)
		into   :ll_prog_fase_effettiva
		from   manutenzioni_fasi
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore in ricerca fasi manutenzione.~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext)
			rollback;
			return -1		
		end if
		
		if isnull(ll_prog_fase_effettiva) or ll_prog_fase_effettiva = 0 then
			ll_prog_fase_effettiva = 1
		else
			ll_prog_fase_effettiva = ll_prog_fase_effettiva + 1
		end if
		
		//Donato 09/11/2011 chiedere se l'intervento deve essere creato già eseguito o lasciato aperto (caso in cui c'era già una manutenzione)
		li_risposta = g_mb.messagebox("OMNIA", "Chiudere l'intervento da generare?", Exclamation!, YesNo!, 2)
		if li_risposta = 2 then
			ls_flag_eseguito = "N"
		else
			ls_flag_eseguito = "S"
		end if
		
		update   manutenzioni
		set flag_eseguito = :ls_flag_eseguito
		where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
			 
		if sqlca.sqlcode <> 0 then 		
			g_mb.messagebox("Omnia","Errore nell'aggiornamento della mautenzione (eseguita=SI).~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext + &
						  "~nAnno Registrazione " + string(ll_anno_registrazione) + " Numero Registrazione " + string(ll_num_registrazione))
			rollback;
			return -1
		end if
		
		//fine modifica -------------------------------------------------------------------------------------------------------
		
	end if
	
	ls_titolo_fase = "Fase" + string(ll_prog_fase_effettiva)
	// ------------ nuova fase --- 
	insert into manutenzioni_fasi(cod_azienda, 
											anno_registrazione, 
											num_registrazione, 
											prog_fase,
											des_fase,
											note)
								values  (:s_cs_xx.cod_azienda,
											:ll_anno_registrazione,
											:ll_num_registrazione,
											:ll_prog_fase_effettiva,
											:ls_titolo_fase,
											:ls_descrizione_fase);
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia","Errore in creazione fase manutenzione.~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext + " prog. fase = " + string(ll_prog_fase_effettiva))
		rollback;
		return -1
	end if
	//
	// -------------------------------- inserisco tutti gli operatori -----------------------------------------
	//
	for ll_i = 1 to dw_call_center_operai.rowcount()
		
		ls_cod_operaio = dw_call_center_operai.getitemstring(ll_i, "cod_operaio")
		ldt_data_inizio = dw_call_center_operai.getitemdatetime(ll_i, "data_inizio")
		ldt_ora_inizio = dw_call_center_operai.getitemdatetime(ll_i, "ora_inizio")
		ldt_data_fine = dw_call_center_operai.getitemdatetime(ll_i, "data_fine")
		ldt_ora_fine = dw_call_center_operai.getitemdatetime(ll_i, "ora_fine")
		ll_tot_ore = dw_call_center_operai.getitemnumber(ll_i, "tot_ore")
		ll_tot_minuti = dw_call_center_operai.getitemnumber( ll_i, "tot_minuti")
		ld_costo_orario = dw_call_center_operai.getitemnumber( ll_i, "costo_orario")
		ld_sconto_1 = dw_call_center_operai.getitemnumber( ll_i, "sconto_1")
		ld_imponibile_iva = dw_call_center_operai.getitemnumber( ll_i, "imponibile_iva")
		
		if isnull(ll_tot_ore) then
			ll_tot_ore = 0
		end if
		
		if isnull(ll_tot_minuti) then
			ll_tot_minuti = 0
		end if
		
		INSERT INTO manutenzioni_fasi_operai  
				( cod_azienda,
				  anno_registrazione,   
				  num_registrazione,   
				  prog_fase,
				  cod_operaio,
				  data_inizio,   
				  ora_inizio,   
				  data_fine,
				  ora_fine,
				  tot_ore,
				  tot_minuti,
				  costo_ora_operaio,
				  sconto_1,
				  tot_costo_operaio)  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ll_anno_registrazione,   
				  :ll_num_registrazione,   
				  :ll_prog_fase_effettiva,   
				  :ls_cod_operaio,   
				  :ldt_data_inizio,
				  :ldt_ora_inizio,
				  :ldt_data_fine,
				  :ldt_ora_fine,
				  :ll_tot_ore,
				  :ll_tot_minuti,
				  :ld_costo_orario,
				  :ld_sconto_1,
				  :ld_imponibile_iva);
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in inserimento operai in fase delle manutenzioni.~r~n"+ sqlca.sqlerrtext)
			rollback;
			return -1
		end if
			
	next	
	
	// ------------ inserisco tutte le risorse ----------------------------
	
	for ll_i = 1 to dw_call_center_risorse.rowcount()
		
		ls_cod_risorsa = dw_call_center_risorse.getitemstring(ll_i, "cod_risorsa")
		ls_cod_cat_risorsa = dw_call_center_risorse.getitemstring(ll_i, "cod_cat_risorsa")
		ldt_data_inizio = dw_call_center_risorse.getitemdatetime(ll_i, "data_inizio")
		ldt_ora_inizio = dw_call_center_risorse.getitemdatetime(ll_i, "ora_inizio")
		ldt_data_fine = dw_call_center_risorse.getitemdatetime(ll_i, "data_fine")
		ldt_ora_fine = dw_call_center_risorse.getitemdatetime(ll_i, "ora_fine")
		ll_tot_ore = dw_call_center_risorse.getitemnumber(ll_i, "tot_ore")
		ll_tot_minuti = dw_call_center_risorse.getitemnumber( ll_i, "tot_minuti")
		ld_costo_orario = dw_call_center_risorse.getitemnumber( ll_i, "costo_orario")
		ld_sconto_1 = dw_call_center_risorse.getitemnumber( ll_i, "sconto_1")
		ld_costi_aggiuntivi = dw_call_center_risorse.getitemnumber( ll_i, "costi_aggiuntivi")
		ld_imponibile_iva = dw_call_center_risorse.getitemnumber( ll_i, "imponibile_iva")
		
		if isnull(ll_tot_ore) then
			ll_tot_ore = 0
		end if
		
		if isnull(ll_tot_minuti) then
			ll_tot_minuti = 0
		end if
		
		INSERT INTO manutenzioni_fasi_risorse  
				( cod_azienda,
				  anno_registrazione,   
				  num_registrazione,   
				  prog_fase,
				  cod_cat_risorse_esterne,
				  cod_risorsa_esterna,
				  data_inizio,   
				  ora_inizio,   
				  data_fine,
				  ora_fine,
				  tot_ore,
				  tot_minuti,
				  costo_ora_risorsa,
				  sconto_1,
				  costi_aggiuntivi,
				  tot_costo_risorsa)  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ll_anno_registrazione,   
				  :ll_num_registrazione,   
				  :ll_prog_fase_effettiva,   
				  :ls_cod_cat_risorsa,   
				  :ls_cod_risorsa,
				  :ldt_data_inizio,
				  :ldt_ora_inizio,
				  :ldt_data_fine,
				  :ldt_ora_fine,
				  :ll_tot_ore,
				  :ll_tot_minuti,
				  :ld_costo_orario,
				  :ld_sconto_1,
				  :ld_costi_aggiuntivi,
				  :ld_imponibile_iva);
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in inserimento risorse in fase delle manutenzioni.~r~n"+ sqlca.sqlerrtext)
			rollback;
			return -1
		end if
			
	next	
	
	// ------------ inserisco tutti i ricambi ----------------------------
	
	for ll_i = 1 to dw_call_center_manut_ricambi.rowcount()
		
		ls_cod_ricambio    = dw_call_center_manut_ricambi.getitemstring(ll_i, "cod_prodotto")
		ls_des_ricambio    = dw_call_center_manut_ricambi.getitemstring(ll_i, "des_prodotto")
		ld_quan_ricambio   = dw_call_center_manut_ricambi.getitemnumber(ll_i, "quan_ricambio")
		ld_prezzo_ricambio = dw_call_center_manut_ricambi.getitemnumber(ll_i, "prezzo_ricambio")
		ld_sconto_1        = dw_call_center_manut_ricambi.getitemnumber(ll_i, "sconto_1")
		ld_sconto_2        = dw_call_center_manut_ricambi.getitemnumber(ll_i, "sconto_2")
		ld_imponibile_iva  = dw_call_center_manut_ricambi.getitemnumber(ll_i, "imponibile_iva")
		
		//donato 19-06-2008 per calcolare il max in tabella manutenzioni_ricambi prima degli INSERT
		SELECT max(prog_riga_ricambio)
		INTO :ll_max_manut_ric
		FROM manutenzioni_ricambi
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND
				anno_registrazione = :ll_anno_registrazione AND
				num_registrazione = :ll_num_registrazione;
				
		if isnull(ll_max_manut_ric) then ll_max_manut_ric = 0
		ll_max_manut_ric +=1
		
		if isnull(ld_quan_ricambio) then ld_quan_ricambio = 0
		if isnull(ld_prezzo_ricambio) then ld_prezzo_ricambio = 0
		if isnull(ld_sconto_1) then ld_sconto_1 = 0
		if isnull(ld_sconto_2) then ld_sconto_2 = 0
		
		//fine modifica ------------------------------------------------------------------------------------------------------------
		INSERT INTO manutenzioni_ricambi  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_ricambio,   
			  cod_prodotto,   
			  des_prodotto,   
			  quan_utilizzo,   
			  prezzo_ricambio,   
			  flag_utilizzo,   
			  anno_reg_mov_mag,   
			  num_reg_mov_mag,
			  sconto_1,
			  sconto_2,
			  imponibile_iva)  
		VALUES ( :s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  :ll_max_manut_ric, //:ll_i,
			  :ls_cod_ricambio,   
			  :ls_des_ricambio,   
			  :ld_quan_ricambio,   
			  :ld_prezzo_ricambio,   
			  'S',   
			  null,   
			  null,
			  :ld_sconto_1,
			  :ld_sconto_2,
			  :ld_imponibile_iva)  ;
		
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in inserimento ricambi associati alla manutenzione.~r~n"+ sqlca.sqlerrtext)
			rollback;
			return -1
		end if
			
	next	
	// 	aggiunto da enrico su richiesta di Baggio 9/4/2004
	// 	Quando si generano le manutenzioni dalle richieste senza codice attrezzatura, 
	// 	allora non va messo il flag_gen_manutenzioni a si.
	
	ls_flag_gen_manutenzioni = "S"
	
	if isnull(ls_cod_attrezzatura) then
		select flag_gen_manutenzione
		into   :ls_flag_gen_manutenzioni
		from   tab_richieste
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_ric and
				 num_registrazione = :il_num_ric;
		if sqlca.sqlcode = 0  and ls_flag_gen_manutenzioni = "N" then
			ls_flag_gen_manutenzioni = "N"
		end if
	end if
	//    fine aggiunta di Enrico.
	
	update tab_richieste
	set    flag_gen_manutenzione = :ls_flag_gen_manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_ric and
			 num_registrazione = :il_num_ric;
	
	if sqlca.sqlcode <> 0 then 		
		g_mb.messagebox("Omnia","Errore in UPDATE tab_richieste per flag_gen_manutenzioni ~r~nERRRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext)	
		rollback;
		return -1
	end if
		
else
	
	// caso NON GLOBAL SERVICE (quindi niente fasi di manutenzione)
	// Se genero un uleriore intervento viene creata una nuova registrazione intervento.

	// --- Calcolo il tempo totale dell'intervento; non essendoci le fasi verrà 
	//                 assoggettato tutto al primo operaio
	
	ls_note_idl += "~r~nELENCO ALTRI OPERAI :"
	
	setnull(ldt_operai_data_inizio)
	setnull(ldt_operai_ora_inizio)
	setnull(ldt_operai_data_fine)
	setnull(ldt_operai_ora_fine)
	setnull(ls_cod_operaio)

	for ll_i = 1 to dw_call_center_operai.rowcount()
		
		if ll_i > 1 then exit
		
		ls_str = ""
		
		if isnull(ls_cod_operaio) then
			ls_cod_operaio = dw_call_center_operai.getitemstring(ll_i, "cod_operaio")
		end if
		
		select nome, cognome
		into   :ls_nome, :ls_cognome
		from   anag_operai
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_operaio = :ls_cod_operaio;
				 
		if not isnull(ls_nome) then ls_str += ls_nome + " "
		if not isnull(ls_cognome) then ls_str += ls_cognome + " "
		
		ldt_data_inizio = dw_call_center_operai.getitemdatetime(ll_i, "data_inizio")
		ldt_ora_inizio = dw_call_center_operai.getitemdatetime(ll_i, "ora_inizio")
		ldt_data_fine = dw_call_center_operai.getitemdatetime(ll_i, "data_fine")
		ldt_ora_fine = dw_call_center_operai.getitemdatetime(ll_i, "ora_fine")
		ll_tot_ore = dw_call_center_operai.getitemnumber(ll_i, "tot_ore")
		ll_tot_minuti = dw_call_center_operai.getitemnumber( ll_i, "tot_minuti")
		
		if isnull(ldt_data_registrazione) or ldt_data_registrazione > ldt_data_inizio then
			ldt_data_registrazione = ldt_data_inizio
		end if
		
		if isnull(ldt_operai_data_inizio) then
			ldt_operai_data_inizio = ldt_data_inizio
			ldt_operai_ora_inizio = ldt_ora_inizio
		else
			// ho trovato una data precedente di inizio
			if ldt_operai_data_inizio > ldt_data_inizio then
				ldt_operai_data_inizio = ldt_data_inizio
				ldt_operai_ora_inizio = ldt_ora_inizio
			else
				if ldt_operai_data_inizio = ldt_data_inizio then
					// se è la stessa data controllo l'orario
					if ldt_operai_ora_inizio > ldt_ora_inizio then ldt_operai_ora_inizio = ldt_ora_inizio
				end if
			end if
		end if
			
		
		if isnull(ldt_operai_data_fine) then
			ldt_operai_data_fine = ldt_data_fine
			ldt_operai_ora_fine = ldt_ora_fine
		else
			// ho trovato una data successiva di fine
			if ldt_operai_data_fine < ldt_data_fine then
				ldt_operai_data_fine = ldt_data_fine
				ldt_operai_ora_fine = ldt_ora_fine
			else
				if ldt_operai_data_fine = ldt_data_fine then
					// se è la stessa data controllo l'orario
					if ldt_operai_ora_fine > ldt_ora_fine then ldt_operai_ora_fine = ldt_ora_fine
				end if
			end if
		end if
		
		
		ls_str += string(ldt_data_inizio, "dd/mm/yyyy") + " " + string(ldt_ora_inizio, "hh:mm") + " - " + string(ldt_data_fine, "dd/mm/yyyy") + " " + string(ldt_ora_fine, "hh:mm") + " TOT." + string(ll_tot_ore) + ":" + string(ll_tot_minuti)
		
		ls_note_idl += "~r~n" + ls_str

		if isnull(ll_tot_ore) then
			ll_tot_ore = 0
		end if
		
		if isnull(ll_tot_minuti) then
			ll_tot_minuti = 0
		end if
		
		ll_somma_ore_operai += ll_tot_ore
		ll_somma_minuti_operai += ll_tot_minuti
			
	next	
	
	
	// ------------ inserisco tutte le risorse -----------------------------------
	
	setnull(ls_cod_risorsa)
	setnull(ls_cod_cat_risorsa)
	
	for ll_i = 1 to dw_call_center_risorse.rowcount()
		
		ls_str = ""
		
		if ll_i = 1 then		// per il non global service considero solo la prima risorsa
									// dato che non ci sono le fasi
			ls_cod_risorsa = dw_call_center_risorse.getitemstring(ll_i, "cod_risorsa")
			ls_cod_cat_risorsa = dw_call_center_risorse.getitemstring(ll_i, "cod_cat_risorsa")
		end if
		
		select anag_risorse_esterne.descrizione  
		into   :ls_des_risorsa  
	   from   anag_risorse_esterne  
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cat_risorse_esterne = :ls_cod_cat_risorsa and
				 cod_risorsa_esterna = :ls_cod_risorsa;
		
		if not isnull(ls_nome) then ls_str += ls_des_risorsa + " "
		
		ldt_data_inizio = dw_call_center_risorse.getitemdatetime(ll_i, "data_inizio")
		ldt_ora_inizio = dw_call_center_risorse.getitemdatetime(ll_i, "ora_inizio")
		ldt_data_fine = dw_call_center_risorse.getitemdatetime(ll_i, "data_fine")
		ldt_ora_fine = dw_call_center_risorse.getitemdatetime(ll_i, "ora_fine")
		ll_tot_ore = dw_call_center_risorse.getitemnumber(ll_i, "tot_ore")
		ll_tot_minuti = dw_call_center_risorse.getitemnumber( ll_i, "tot_minuti")
		
		if isnull(ldt_data_registrazione) or ldt_data_registrazione > ldt_data_inizio then
			ldt_data_registrazione = ldt_data_inizio
		end if

		ls_str += string(ldt_data_inizio, "dd/mm/yyyy") + " " + string(ldt_ora_inizio, "hh:mm") + " - " + string(ldt_data_fine, "dd/mm/yyyy") + " " + string(ldt_ora_fine, "hh:mm") + " TOT." + string(ll_tot_ore) + ":" + string(ll_tot_minuti)

		ls_note_idl += "~r~n" + ls_str
		
		if isnull(ll_tot_ore) then
			ll_tot_ore = 0
		end if
		
		if isnull(ll_tot_minuti) then
			ll_tot_minuti = 0
		end if
		
		ll_somma_ore_risorse += ll_tot_ore
		ll_somma_minuti_risorse += ll_tot_minuti
		
	next	
		
	// sistemo ore e minuti operai
	do while true
		if ll_somma_minuti_operai > 59 then
			ll_somma_ore_operai ++
			ll_somma_minuti_operai = ll_somma_minuti_operai - 60
		else
			exit
		end if
	loop

	// sistemo ore e minuti risorse
	do while true
		if ll_somma_minuti_risorse > 59 then
			ll_somma_ore_risorse ++
			ll_somma_minuti_risorse = ll_somma_minuti_risorse - 60
		else
			exit
		end if
	loop

		
//	ls_cod_operaio = s_cs_xx.parametri.parametro_s_3
//	
//	if ls_cod_operaio = "" then setnull(ls_cod_operaio)
	
	ll_anno_registrazione = f_anno_esercizio()
		
	select max(num_registrazione)
	into   :ll_num_registrazione
	from   manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione;
			 
	if ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
		ll_num_registrazione = 1 
	else
		ll_num_registrazione ++
	end if
		
	//************************************** Aggiungo la descrizione della richiesta ***************
	ls_note_idl_1 = ls_des_tipo_manutenzione + ":~r~n"
	if not isnull(s_cs_xx.parametri.parametro_s_4) and len(s_cs_xx.parametri.parametro_s_4) > 0 then
		ls_note_idl_1 = ls_note_idl_1 + s_cs_xx.parametri.parametro_s_4 
	end if
	
	if isnull(ls_note_idl) then ls_note_idl = ""
	
	ls_note_idl_1 += ls_note_idl
	
	//**************************************** Inserisco guasto specifico in tabella ***************
	INSERT INTO tab_guasti  
			( cod_azienda,   
			  cod_attrezzatura,   
			  cod_guasto,   
			  des_guasto,   
			  cod_tipo_manutenzione,   
			  punteggio_guasto,   
			  note )  
	 select cod_azienda,   
			  :ls_cod_attrezzatura,   
			  cod_guasto,   
			  des_guasto,   
			  null,   
			  0,   
			  null
	from tab_guasti_generici
	where cod_azienda = :s_cs_xx.cod_azienda and
	       cod_guasto = :ls_cod_guasto   ;
	

	if isnull(ldt_data_registrazione) then ldt_data_registrazione = datetime(today(), 00:00:00)
		
	insert into manutenzioni
			 (cod_azienda,
			 anno_registrazione,
			 num_registrazione,
			 des_intervento,
			 data_registrazione,
			 cod_attrezzatura,
			 cod_tipo_manutenzione,
			 flag_ordinario,
			 cod_operaio,
			 note_idl,
			 quan_ricambio,
			 costo_unitario_ricambio,
			 cod_ricambio,
			 flag_ricambio_codificato,
			 ricambio_non_codificato,
			 operaio_minuti,
			 operaio_ore,
			 data_inizio_intervento,
			 ora_inizio_intervento,
			 data_fine_intervento,
			 ora_fine_intervento,
			 flag_eseguito,
			 anno_reg_richiesta,
			 num_reg_richiesta,
			 cod_cat_risorse_esterne,
			 cod_risorsa_esterna,
			 risorsa_ore,
			 risorsa_minuti,
			 cod_guasto)
	values (:s_cs_xx.cod_azienda,
			 :ll_anno_registrazione,
			 :ll_num_registrazione,
			 :ls_descrizione_manut_1,
			 :ldt_data_registrazione,
			 :ls_cod_attrezzatura,
			 :ls_cod_tipo_manutenzione,
			 'N',
			 :ls_cod_operaio,
			 :ls_note_idl_1,
			 :ld_quan_ricambio,
			 :ld_costo_unitario_ricambio,
			 :ls_cod_ricambio,
			 :ls_flag_ricambio_codificato,
			 :ls_des_ricambio_non_codificato,
			 :ll_somma_minuti_operai,
			 :ll_somma_ore_operai,
			 :ldt_operai_data_inizio,
			 :ldt_operai_ora_inizio,
			 :ldt_operai_data_fine,
			 :ldt_operai_ora_fine,
			 'N',
			 :il_anno_ric,
			 :il_num_ric,
			 :ls_cod_cat_risorsa,
			 :ls_cod_risorsa,
			 :ll_somma_ore_risorse,
			 :ll_somma_minuti_risorse,
			 :ls_cod_guasto);
			 
	if sqlca.sqlcode <> 0 then 		
		g_mb.messagebox("Omnia","Errore nella registrazione della manutenzione.~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext + &
					  "~nAnno Registrazione " + string(ll_anno_registrazione) + " Numero Registrazione " + string(ll_num_registrazione))
		rollback;
		return -1
	end if
	
	// ------------ inserisco gli altri operatori
	
	ll_ret = wf_operatori_semplice( ll_anno_registrazione, ll_num_registrazione)
	if ll_ret < 0 then
		rollback;
		return -1
	end if
	
	// ------------ inserisco tutti i ricambi legati alla manutenzione --------------------
	
	for ll_i = 1 to dw_call_center_manut_ricambi.rowcount()
		
		ls_cod_ricambio    = dw_call_center_manut_ricambi.getitemstring(ll_i, "cod_prodotto")
		ls_des_ricambio    = dw_call_center_manut_ricambi.getitemstring(ll_i, "des_prodotto")
		ld_quan_ricambio   = dw_call_center_manut_ricambi.getitemnumber(ll_i, "quan_ricambio")
		ld_prezzo_ricambio = dw_call_center_manut_ricambi.getitemnumber(ll_i, "prezzo_ricambio")
		ld_sconto_1        = dw_call_center_manut_ricambi.getitemnumber(ll_i, "sconto_1")
		ld_sconto_2        = dw_call_center_manut_ricambi.getitemnumber(ll_i, "sconto_2")
		ld_imponibile_iva  = dw_call_center_manut_ricambi.getitemnumber(ll_i, "imponibile_iva")
		
		//donato 19-06-2008 per calcolare il max in tabella manutenzioni_ricambi prima degli INSERT
		SELECT max(prog_riga_ricambio)
		INTO :ll_max_manut_ric
		FROM manutenzioni_ricambi
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND
				anno_registrazione = :ll_anno_registrazione AND
				num_registrazione = :ll_num_registrazione;
				
		if isnull(ll_max_manut_ric) then ll_max_manut_ric = 0
		ll_max_manut_ric +=1
		
		//fine modifica ------------------------------------------------------------------------------------------------------------
		
		if isnull(ld_quan_ricambio) then ld_quan_ricambio = 0
		if isnull(ld_prezzo_ricambio) then ld_prezzo_ricambio = 0
		if isnull(ld_sconto_1) then ld_sconto_1 = 0
		if isnull(ld_sconto_2) then ld_sconto_2 = 0
		
		INSERT INTO manutenzioni_ricambi  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_ricambio,   
			  cod_prodotto,   
			  des_prodotto,   
			  quan_utilizzo,   
			  prezzo_ricambio,   
			  flag_utilizzo,   
			  anno_reg_mov_mag,   
			  num_reg_mov_mag,
			  sconto_1,
			  sconto_2,
			  imponibile_iva)  
		VALUES ( :s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  :ll_max_manut_ric, //:ll_i,   
			  :ls_cod_ricambio,   
			  :ls_des_ricambio,   
			  :ld_quan_ricambio,   
			  :ld_prezzo_ricambio,   
			  'S',   
			  null,   
			  null,
			  :ld_sconto_1,
			  :ld_sconto_2,
			  :ld_imponibile_iva)  ;
		
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in inserimento ricambi associati alla manutenzione.~r~n"+ sqlca.sqlerrtext)
			rollback;
			return -1
		end if
	next
end if
// **************  fine inserimento altri operatori alla fase creata  **********************************
// **************  chiudere la richiesta? **************************************************************


if ib_global_service then
	
	ls_errore = ""
	li_risposta = f_tempi_richiesta(il_anno_ric, il_num_ric, ls_errore)
	if li_risposta <> 0 then
		g_mb.messagebox("OMNIA", "Errore nell'aggiornamento delle date!" + ls_errore)
		rollback;
		return -1
	end if
	
	//-------------------------------------------------
	//Donato 13/07/2009 anomalia segnalata da soraluce: metteva sempre la data fine intervento in data registrazione
	//anche se l'utente sceglie di allineare con la data inizio intervento
	if s_cs_xx.parametri.parametro_s_10 = "S" then		
		//semaforo acceso: vuol dire che l'utente ha scelto di allineare la data_registrazione intervento
		//con la data inizio intervento, quindi non sovrascrivere con l'update
		
		//ora rimetto il semaforo a spento
		setnull(s_cs_xx.parametri.parametro_s_10)
	else
		//TUTTO COME PRIMA
		// se ho una nuova manutenzione, aggiorno la data di registrazione 
		if ll_aggiorna_data = 1 then
			
			update manutenzioni
			set    data_registrazione = data_fine_intervento
			where  cod_azienda = :s_cs_xx.cod_azienda   
			and    anno_registrazione = :ll_anno_registrazione
			and    num_registrazione =	:ll_num_registrazione;
		
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("OMNIA", "Errore nell'aggiornamento della data di registrazione delle manutenzioni:~r~n" + sqlca.sqlerrtext)	
				rollback;
				return -1	
			end if
		end if	
	end if
	//-------------------------------------------------
end if

li_risposta = g_mb.messagebox("OMNIA", "Considerare chiusa la richiesta?",Exclamation!, YesNo!, 2)
if li_risposta = 1 then

	update tab_richieste
	set    flag_richiesta_chiusa = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_ric and
			 num_registrazione = :il_num_ric;

	if sqlca.sqlcode <> 0 then 		
		g_mb.messagebox("Omnia","Errore in UPDATE richiesta chiusa.~r~nERRORE=" + string(sqlca.sqlcode) + "~r~nDETTAGLIO=" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if	
end if	
	
commit ;

g_mb.messagebox("Omnia", "Generazione attività manutentiva avvenuta con successo!")	
il_controllo_conferma = 1
close(parent)


end event

type dw_selezione from uo_dw_main within w_call_center_conferma_manut
integer y = 20
integer width = 2587
integer height = 1160
string dataobject = "d_call_center_conferma_manut"
boolean border = false
end type

event pcd_new;call super::pcd_new;string ls_str, ls_cod_attrezzatura

dw_selezione.setitem(dw_selezione.getrow(), "cod_tipo_manutenzione", s_cs_xx.parametri.parametro_s_1)

 ls_cod_attrezzatura = s_cs_xx.parametri.parametro_s_2
dw_selezione.setitem(dw_selezione.getrow(), "cod_attrezzatura", ls_cod_attrezzatura)

if not isnull(ls_cod_attrezzatura) and len(ls_cod_attrezzatura) > 0 then

	f_PO_LoadDDDW_sort(dw_selezione, &
							 "cod_guasto", &
							 sqlca, &
							 "tab_guasti", &
							 "cod_guasto", &
							 "des_guasto + ' ' + cod_attrezzatura", &
							 "cod_azienda = '" + s_cs_xx.cod_azienda + &
							 "' and cod_attrezzatura = '" + ls_cod_attrezzatura + "' union all select cod_guasto, des_guasto from tab_guasti_generici where cod_azienda = '" + &
							  s_cs_xx.cod_azienda + "' and cod_guasto not in ( select cod_guasto from tab_guasti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '"+ls_cod_attrezzatura+"' )" , &
							  "1")								  

	dw_tipi_man.retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura)

else
	dw_tipi_man.retrieve(s_cs_xx.cod_azienda, "NON-ESISTENTE")
end if

dw_selezione.setitem(dw_selezione.getrow(), "cod_operaio", s_cs_xx.parametri.parametro_s_3)
dw_selezione.setitem(dw_selezione.getrow(), "data_registrazione", s_cs_xx.parametri.parametro_data_3)
setnull(ls_str)

if s_cs_xx.parametri.parametro_s_1 <> "S" then
	dw_selezione.setitem(dw_selezione.getrow(), "descrizione", s_cs_xx.parametri.parametro_s_5)
else
	dw_selezione.setitem(dw_selezione.getrow(), "descrizione", ls_str)
end if

end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_null, ls_cod_tipo_richiesta, ls_flag_spostamento
	long ll_cont
	
	setnull(ls_null)
	
   choose case i_colname
      case "cod_attrezzatura"
			
			ll_cont = 0
			
			if not isnull(i_coltext) and len(i_coltext) > 0 then
				select count(*)
				into   :ll_cont
				from   anag_attrezzature
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_attrezzatura = :i_coltext;
				if ll_cont < 1  then
					g_mb.messagebox("OMNIA","Apparecchiatura inesistente !!!",Stopsign!)
					return 1
				end if
				
				dw_tipi_man.retrieve(s_cs_xx.cod_azienda, i_coltext)
				
				
				// ---------- enrico per gestione spostamenti -------------------------- //
				
				select cod_tipo_richiesta 
				into   :ls_cod_tipo_richiesta
				from   tab_richieste
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_ric and
						 num_registrazione = :il_num_ric;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Omnia", "Errore in ricerca codice tipo richiesta nella tab_richieste."+sqlca.sqlerrtext)
					return -1
				end if
				
				select flag_spostamento
				into   :ls_flag_spostamento
				from   tab_tipi_richieste
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_richiesta = :ls_cod_tipo_richiesta;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Omnia", "Errore in ricerca tipo manutenzione nella tab_tipi_richieste."+sqlca.sqlerrtext)
					return -1
				end if
				
				if ls_flag_spostamento = "S" then
				
					select count(*) 
					into   :ll_cont
					from   tab_tipi_richieste
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 flag_spostamento = 'S' and
							 cod_tipo_richiesta in
							  (select distinct cod_tipo_richiesta
								from   tab_richieste, manutenzioni
								where  tab_richieste.cod_azienda = :s_cs_xx.cod_azienda and
										 tab_richieste.cod_azienda = manutenzioni.cod_azienda and
										 tab_richieste.anno_registrazione = manutenzioni.anno_reg_richiesta and
										 tab_richieste.num_registrazione = manutenzioni.num_reg_richiesta and
										 manutenzioni.flag_eseguito = 'N' AND
										 manutenzioni.cod_attrezzatura = :i_coltext);
										 
					if ll_cont > 0 then
						g_mb.messagebox("OMNIA","L'apparecchiatura ha già qualche richiesta di spostamento in corso.",Stopsign!)
						return 1
					end if
					
				end if
				
				// ---------------------------------------------------------------------
				
				f_PO_LoadDDDW_sort(dw_selezione, &
										 "cod_guasto", &
										 sqlca, &
										 "tab_guasti", &
										 "cod_guasto", &
										 "des_guasto + ' ' + cod_attrezzatura", &
										 "cod_azienda = '" + s_cs_xx.cod_azienda + &
										 "' and cod_attrezzatura = '" + i_coltext + "' union all select cod_guasto, des_guasto from tab_guasti_generici where cod_azienda = '" + &
										  s_cs_xx.cod_azienda + "' and cod_guasto not in ( select cod_guasto from tab_guasti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '"+i_coltext+"' )" , &
										  "1")								  
		
				setitem(getrow(),"cod_guasto",ls_null)
		
			else
				// non deve caricare nulla; metto una where assurda e impossibile
				
				dw_tipi_man.retrieve(s_cs_xx.cod_azienda, "NONESISTENTE")
				
				f_PO_LoadDDDW_dw(dw_selezione, &
										 "cod_guasto", &
										 sqlca, &
										 "tab_guasti", &
										 "cod_guasto", &
										 "des_guasto + ' ' + cod_attrezzatura", &
										 "cod_azienda = '9999'")								  
		
				setitem(getrow(),"cod_guasto",ls_null)
		
			end if
	end choose
end if




					  
end event

event buttonclicked;call super::buttonclicked;string				ls_messaggio
integer			li_ret


choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_selezione,"cod_attrezzatura")
		
	case "ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
		
	case "b_idl"
		li_ret = wf_idl(ls_messaggio)
		if li_ret<0 then
			g_mb.error(ls_messaggio)
			return
			
		elseif li_ret>0 then
			g_mb.warning(ls_messaggio)
			return
			
		else
			//tutto OK
		end if
		
end choose
end event

type st_ric_op_risorse from statictext within w_call_center_conferma_manut
boolean visible = false
integer x = 69
integer y = 2176
integer width = 1509
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
boolean focusrectangle = false
end type

type dw_call_center_operai_lista from datawindow within w_call_center_conferma_manut
event ue_key pbm_dwnkey
integer x = 69
integer y = 1456
integer width = 1522
integer height = 696
integer taborder = 22
string title = "none"
string dataobject = "d_call_center_conferma_manut_operai"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_key;timer(0.5,parent)

choose case key
	case keya!
		is_ricerca += "A"
	case keyb!
		is_ricerca += "B"
	case keyc!
		is_ricerca += "C"
	case keyd!
		is_ricerca += "D"
	case keye!
		is_ricerca += "E"
	case keyf!
		is_ricerca += "F"
	case keyg!
		is_ricerca += "G"
	case keyh!
		is_ricerca += "H"
	case keyi!
		is_ricerca += "I"
	case keyl!
		is_ricerca += "L"
	case keym!
		is_ricerca += "M"
	case keyn!
		is_ricerca += "N"
	case keyo!
		is_ricerca += "O"
	case keyp!
		is_ricerca += "P"
	case keyq!
		is_ricerca += "Q"
	case keyr!
		is_ricerca += "R"
	case keys!
		is_ricerca += "S"
	case keyt!
		is_ricerca += "T"
	case keyu!
		is_ricerca += "U"
	case keyv!
		is_ricerca += "V"
	case keyz!
		is_ricerca += "Z"
	case keyx!
		is_ricerca += "X"
	case keyy!
		is_ricerca += "Y"
	case keyw!
		is_ricerca += "W"
	case keyj!
		is_ricerca += "J"
	case keyk!
		is_ricerca += "K"
end choose

st_ric_op.text = is_ricerca
end event

event rowfocuschanging;selectrow(currentrow,false)
selectrow(newrow,true)
end event

type dw_call_center_risorse_lista from datawindow within w_call_center_conferma_manut
event ue_key pbm_dwnkey
integer x = 69
integer y = 1456
integer width = 1522
integer height = 696
integer taborder = 21
boolean bringtotop = true
string title = "none"
string dataobject = "d_call_center_conferma_manut_ris_est"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanging;selectrow(currentrow,false)
selectrow(newrow,true)
end event

type cb_3_ricambi from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 1480
integer width = 123
integer height = 120
integer taborder = 42
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">>"
end type

event clicked;long     ll_i, ll_riga
string   ls_cod_prodotto, ls_des_prodotto
dec{4}   ld_prezzo
datetime ldt_null

if dw_call_center_manut_ricambi_lista.rowcount() > 25 then
	if g_mb.messagebox("OMNIA", "Sei sicuro di voler caricare TUTTI i ricambi in lista?",Question!,YesNo!,2) = 2 then return
end if

for ll_i = 1 to dw_call_center_manut_ricambi_lista.rowcount()
	dw_call_center_risorse_lista.setrow(ll_i)

	if dw_call_center_manut_ricambi_lista.getrow() > 0 then
		ls_cod_prodotto = dw_call_center_manut_ricambi_lista.getitemstring(dw_call_center_manut_ricambi_lista.getrow(),"cod_prodotto")
		ls_des_prodotto = dw_call_center_manut_ricambi_lista.getitemstring(dw_call_center_manut_ricambi_lista.getrow(),"des_prodotto")
		
		if  dw_call_center_manut_ricambi.find(" cod_prodotto = '" + ls_cod_prodotto + "'", 1, dw_call_center_manut_ricambi.rowcount() ) > 0 then return
	
		setnull(ldt_null)
		
		ll_riga = dw_call_center_manut_ricambi.insertrow(0)
		dw_call_center_manut_ricambi.setfocus()
		dw_call_center_manut_ricambi.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
		dw_call_center_manut_ricambi.setitem(ll_riga,"des_prodotto",ls_des_prodotto)	
		dw_call_center_manut_ricambi.setitem(ll_riga,"quan_ricambio",1)	
		dw_call_center_manut_ricambi.setitem(ll_riga,"sconto_1",0)	
		dw_call_center_manut_ricambi.setitem(ll_riga,"sconto_2",0)	
		
		ld_prezzo = wf_carica_prezzo(ls_cod_prodotto, 1)
		dw_call_center_manut_ricambi.setitem(ll_riga,"prezzo_ricambio",ld_prezzo)	
		wf_calcola_totale(ll_riga)
		
	end if

next
end event

type cb_11_ris from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 1640
integer width = 123
integer height = 120
integer taborder = 52
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">"
end type

event clicked;string   ls_cod_cat_risorsa, ls_cod_risorsa, ls_descrizione
long     ll_riga
dec{4}   ld_costo_orario, ld_costi_aggiuntivi
datetime ldt_null

if dw_call_center_risorse_lista.getrow() > 0 then
	
	ls_cod_cat_risorsa = dw_call_center_risorse_lista.getitemstring(dw_call_center_risorse_lista.getrow(),"cod_cat_risorse_esterne")
	ls_cod_risorsa = dw_call_center_risorse_lista.getitemstring(dw_call_center_risorse_lista.getrow(),"cod_risorsa_esterna")
	ls_descrizione = dw_call_center_risorse_lista.getitemstring(dw_call_center_risorse_lista.getrow(),"descrizione")
	
	if  dw_call_center_risorse.find(" cod_risorsa = '" + ls_cod_risorsa + "' and cod_cat_risorsa =  '" + ls_cod_cat_risorsa + "'", 1, dw_call_center_risorse.rowcount() ) > 0 then return

	setnull(ldt_null)
	
	ll_riga = dw_call_center_risorse.insertrow(0)
	dw_call_center_risorse.setitem(ll_riga,"cod_azienda",s_cs_xx.cod_azienda)
	dw_call_center_risorse.setitem(ll_riga,"cod_risorsa",ls_cod_risorsa)
	dw_call_center_risorse.setitem(ll_riga,"cod_cat_risorsa",ls_cod_cat_risorsa)	
	dw_call_center_risorse.setitem(ll_riga,"descrizione",ls_descrizione)		
	wf_carica_costo_risorse(ls_cod_cat_risorsa, ls_cod_risorsa, ref ld_costo_orario, ref ld_costi_aggiuntivi)
	dw_call_center_risorse.setitem(ll_riga,"costo_orario",ld_costo_orario)	
	dw_call_center_risorse.setitem(ll_riga,"costi_aggiuntivi",ld_costi_aggiuntivi)		
	
	if ll_riga > 1 then
		dw_call_center_risorse.setitem(ll_riga,"data_inizio",dw_call_center_risorse.getitemdatetime(ll_riga - 1,"data_inizio"))
		dw_call_center_risorse.setitem(ll_riga,"ora_inizio",dw_call_center_risorse.getitemdatetime(ll_riga - 1,"ora_inizio"))
		dw_call_center_risorse.setitem(ll_riga,"data_fine",dw_call_center_risorse.getitemdatetime(ll_riga - 1,"data_fine"))
		dw_call_center_risorse.setitem(ll_riga,"ora_fine",dw_call_center_risorse.getitemdatetime(ll_riga - 1,"ora_fine"))
		dw_call_center_risorse.setitem(ll_riga,"tot_ore",dw_call_center_risorse.getitemnumber(ll_riga - 1,"tot_ore"))
		dw_call_center_risorse.setitem(ll_riga,"tot_minuti",dw_call_center_risorse.getitemnumber(ll_riga - 1,"tot_minuti"))

		
	else
		dw_call_center_risorse.setitem(ll_riga,"data_inizio", ldt_null)
		dw_call_center_risorse.setitem(ll_riga,"ora_inizio", ldt_null)
		dw_call_center_risorse.setitem(ll_riga,"data_fine", ldt_null)
		dw_call_center_risorse.setitem(ll_riga,"ora_fine", ldt_null)
		dw_call_center_risorse.setitem(ll_riga,"tot_ore",0)
		dw_call_center_risorse.setitem(ll_riga,"tot_minuti",0)
	end if
	
	
	
	wf_calcola_totale_risorse(ll_riga)
//	dw_call_center_risorse.postevent("ue_totale_riga")
	
end if
end event

type cb_3_ris from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 1480
integer width = 123
integer height = 120
integer taborder = 42
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">>"
end type

event clicked;long     ll_i, ll_riga
string   ls_cod_cat_risorsa, ls_cod_risorsa, ls_descrizione
dec{4}   ld_costo_orario, ld_costi_aggiuntivi
datetime ldt_null

for ll_i = 1 to dw_call_center_risorse_lista.rowcount()
	dw_call_center_risorse_lista.setrow(ll_i)

	if dw_call_center_risorse_lista.getrow() > 0 then
		ls_cod_cat_risorsa = dw_call_center_risorse_lista.getitemstring(dw_call_center_risorse_lista.getrow(),"cod_cat_risorse_esterne")
		ls_cod_risorsa = dw_call_center_risorse_lista.getitemstring(dw_call_center_risorse_lista.getrow(),"cod_risorsa_esterna")
		ls_descrizione = dw_call_center_risorse_lista.getitemstring(dw_call_center_risorse_lista.getrow(),"descrizione")
		
		if dw_call_center_risorse.find("cod_risorsa = '" + ls_cod_risorsa + "' and cod_cat_risorsa = '" + ls_cod_cat_risorsa + "'", 1, dw_call_center_risorse.rowcount() ) > 0 then
			beep(10)
			return
		end if		
	
		setnull(ldt_null)
		
		ll_riga = dw_call_center_risorse.insertrow(0)
		dw_call_center_risorse.setitem(ll_riga,"cod_azienda",s_cs_xx.cod_azienda)
		dw_call_center_risorse.setitem(ll_riga,"cod_risorsa",ls_cod_risorsa)
		dw_call_center_risorse.setitem(ll_riga,"cod_cat_risorsa",ls_cod_cat_risorsa)	
		dw_call_center_risorse.setitem(ll_riga,"descrizione",ls_descrizione)			
		wf_carica_costo_risorse(ls_cod_cat_risorsa, ls_cod_risorsa, ref ld_costo_orario, ref ld_costi_aggiuntivi)
		dw_call_center_risorse.setitem(ll_riga,"costo_orario",ld_costo_orario)	
		dw_call_center_risorse.setitem(ll_riga,"costi_aggiuntivi",ld_costi_aggiuntivi)	
		wf_calcola_totale_risorse(ll_riga)
		
		if ll_riga > 1 then
			dw_call_center_risorse.setitem(ll_riga,"data_inizio",dw_call_center_risorse.getitemdatetime(ll_riga - 1,"data_inizio"))
			dw_call_center_risorse.setitem(ll_riga,"ora_inizio",dw_call_center_risorse.getitemdatetime(ll_riga - 1,"ora_inizio"))
			dw_call_center_risorse.setitem(ll_riga,"data_fine",dw_call_center_risorse.getitemdatetime(ll_riga - 1,"data_fine"))
			dw_call_center_risorse.setitem(ll_riga,"ora_fine",dw_call_center_risorse.getitemdatetime(ll_riga - 1,"ora_fine"))
			dw_call_center_risorse.setitem(ll_riga,"tot_ore",dw_call_center_risorse.getitemnumber(ll_riga - 1,"tot_ore"))
			dw_call_center_risorse.setitem(ll_riga,"tot_minuti",dw_call_center_risorse.getitemnumber(ll_riga - 1,"tot_minuti"))
		else
			dw_call_center_risorse.setitem(ll_riga,"data_inizio", ldt_null)
			dw_call_center_risorse.setitem(ll_riga,"ora_inizio", ldt_null)
			dw_call_center_risorse.setitem(ll_riga,"data_fine", ldt_null)
			dw_call_center_risorse.setitem(ll_riga,"ora_fine", ldt_null)
			dw_call_center_risorse.setitem(ll_riga,"tot_ore",0)
			dw_call_center_risorse.setitem(ll_riga,"tot_minuti",0)
		end if
	end if

next
end event

type cb_11 from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 1640
integer width = 123
integer height = 120
integer taborder = 42
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">"
end type

event clicked;string   ls_cod_operaio
datetime ldt_null, ldt_data_inizio, ldt_data_fine
time     lt_inizio, lt_fine
long     ll_riga, ll_n_ore, ll_n_ore_2, ll_minuti_fin, ll_ore_fin
dec{4}   ld_costo_orario

if dw_call_center_operai_lista.getrow() > 0 then
	
	ls_cod_operaio = dw_call_center_operai_lista.getitemstring(dw_call_center_operai_lista.getrow(),"cod_operaio")
	if dw_call_center_operai.find("cod_operaio = '" + ls_cod_operaio + "'", 1, dw_call_center_operai.rowcount() ) > 0 then
		beep(10)
		return
	end if
	
	setnull(ldt_null)
	
	ll_riga = dw_call_center_operai.insertrow(0)
	
	dw_call_center_operai.setitem(ll_riga,"cod_azienda",s_cs_xx.cod_azienda)
	dw_call_center_operai.setitem(ll_riga,"cod_operaio",ls_cod_operaio)
	ld_costo_orario = wf_carica_costo_operaio(ls_cod_operaio)
	dw_call_center_operai.setitem(ll_riga,"costo_orario",ld_costo_orario)	
	
	
	if ll_riga > 1 then
		dw_call_center_operai.setitem(ll_riga,"data_inizio",dw_call_center_operai.getitemdatetime(ll_riga - 1,"data_inizio"))
		dw_call_center_operai.setitem(ll_riga,"ora_inizio",dw_call_center_operai.getitemdatetime(ll_riga - 1,"ora_inizio"))
		dw_call_center_operai.setitem(ll_riga,"data_fine",dw_call_center_operai.getitemdatetime(ll_riga - 1,"data_fine"))
		dw_call_center_operai.setitem(ll_riga,"ora_fine",dw_call_center_operai.getitemdatetime(ll_riga - 1,"ora_fine"))
		dw_call_center_operai.setitem(ll_riga,"tot_ore",dw_call_center_operai.getitemnumber(ll_riga - 1,"tot_ore"))
		dw_call_center_operai.setitem(ll_riga,"tot_minuti",dw_call_center_operai.getitemnumber(ll_riga - 1,"tot_minuti"))
	else
		dw_call_center_operai.setitem(ll_riga,"data_inizio", ldt_null)
		dw_call_center_operai.setitem(ll_riga,"ora_inizio", ldt_null)
		dw_call_center_operai.setitem(ll_riga,"data_fine", ldt_null)
		dw_call_center_operai.setitem(ll_riga,"ora_fine", ldt_null)
		dw_call_center_operai.setitem(ll_riga,"tot_ore",0)
		dw_call_center_operai.setitem(ll_riga,"tot_minuti",0)
	end if
	wf_calcola_totale_operai(ll_riga)
//	dw_call_center_operai.postevent("ue_totale_riga_operaio")

end if
end event

type cb_11_ricambi from commandbutton within w_call_center_conferma_manut
integer x = 1600
integer y = 1640
integer width = 123
integer height = 120
integer taborder = 62
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">"
end type

event clicked;string   ls_cod_prodotto, ls_des_prodotto,ls_cod_attrezzatura
string ls_cod_cliente,ls_cod_tipo_listino_prodotto,ls_cod_valuta
long     ll_riga
dec{4}   ld_prezzo, ld_quantita
datetime ldt_null,ldt_data_registrazione
uo_condizioni_cliente iuo_condizioni_cliente

if dw_call_center_manut_ricambi_lista.getrow() > 0 then
	
	ls_cod_prodotto = dw_call_center_manut_ricambi_lista.getitemstring(dw_call_center_manut_ricambi_lista.getrow(),"cod_prodotto")
	ls_des_prodotto = dw_call_center_manut_ricambi_lista.getitemstring(dw_call_center_manut_ricambi_lista.getrow(),"des_prodotto")
	
	if  dw_call_center_manut_ricambi.find(" cod_prodotto = '" + ls_cod_prodotto + "'", 1, dw_call_center_manut_ricambi.rowcount() ) > 0 then return

	setnull(ldt_null)
	
	ll_riga = dw_call_center_manut_ricambi.insertrow(0)
	dw_call_center_manut_ricambi.setfocus()
	dw_call_center_manut_ricambi.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
	dw_call_center_manut_ricambi.accepttext()
	dw_call_center_manut_ricambi.setitem(ll_riga,"des_prodotto",ls_des_prodotto)	
	dw_call_center_manut_ricambi.setitem(ll_riga,"quan_ricambio",1)	
	dw_call_center_manut_ricambi.setitem(ll_riga,"sconto_1",0)	
	dw_call_center_manut_ricambi.setitem(ll_riga,"sconto_2",0)	
//	ld_prezzo = wf_carica_prezzo(ls_cod_prodotto, 1)
	dw_call_center_manut_ricambi.setitem(ll_riga,"prezzo_ricambio",ld_prezzo)	
				
	//--------------
		string ls_cod_misura_mag,ls_cod_misura,ls_flag_decimali,ls_cod_iva
		string ls_messaggio
		double ld_fat_conversione,ld_ultimo_prezzo
		datetime ldt_data_esenzione_iva
		
		ld_quantita = 1
		if isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura="" then
			select cod_cliente
			into   :ls_cod_cliente
			from   anag_divisioni
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_divisione in (select cod_divisione 
											 from   tab_aree_aziendali
											 where  cod_azienda = :s_cs_xx.cod_azienda and
													  cod_area_aziendale in ( select cod_area_aziendale
																					  from   anag_attrezzature
																					  where  cod_azienda = :s_cs_xx.cod_azienda ));
		else
			select cod_cliente
			into   :ls_cod_cliente
			from   anag_divisioni
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_divisione in (select cod_divisione 
											 from   tab_aree_aziendali
											 where  cod_azienda = :s_cs_xx.cod_azienda and
													  cod_area_aziendale in ( select cod_area_aziendale
																					  from   anag_attrezzature
																					  where  cod_azienda = :s_cs_xx.cod_azienda and
																								cod_attrezzatura = :ls_cod_attrezzatura));
		end if
		
		if not isnull(ls_cod_cliente) then
			select cod_tipo_listino_prodotto,
					 cod_valuta
			into   :ls_cod_tipo_listino_prodotto,
					 :ls_cod_valuta
			from   anag_clienti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_cliente = :ls_cod_cliente;
		else
			setnull(ls_cod_tipo_listino_prodotto)
			setnull(ls_cod_valuta)
		end if
		
		select cod_misura_mag,
					 cod_misura_ven,
                fat_conversione_ven,
                flag_decimali,
                cod_iva,
					 des_prodotto
         into   :ls_cod_misura_mag,
					 :ls_cod_misura,
                :ld_fat_conversione,
                :ls_flag_decimali,
                :ls_cod_iva,
					 :ls_des_prodotto
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then

            if ld_fat_conversione <> 0 then              
            else
					ld_fat_conversione = 1             
            end if

				//f_situazione_prodotto(data, uo_1)
				
				//le ultime due variabili sono byref
				f_cerca_ultimo_prezzo_vendita_fatture (ls_cod_cliente, ls_cod_prodotto, ld_ultimo_prezzo, ls_messaggio )
	end if

         if not isnull(ls_cod_cliente) then
			select anag_clienti.cod_iva,
					 anag_clienti.data_esenzione_iva
			into   :ls_cod_iva,
					 :ldt_data_esenzione_iva
			from   anag_clienti
			where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_clienti.cod_cliente = :ls_cod_cliente;
         end if

		if mid(f_flag_controllo(),1,1) = "S" then
			
			select fat_conversione_ven
			into   :ld_fat_conversione						
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			
			iuo_condizioni_cliente = CREATE uo_condizioni_cliente
			
			iuo_condizioni_cliente.str_parametri.ldw_oggetto = dw_call_center_manut_ricambi
			iuo_condizioni_cliente.str_parametri.ldw_oggetto.setrow(ll_riga)
			
			iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
			iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
			iuo_condizioni_cliente.str_parametri.data_riferimento = datetime(today(),now())

			iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
			iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
			iuo_condizioni_cliente.str_parametri.dim_1 = 0
			iuo_condizioni_cliente.str_parametri.dim_2 = 0
			iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
			iuo_condizioni_cliente.str_parametri.valore = 0
			iuo_condizioni_cliente.str_parametri.cod_agente_1 = ""
			iuo_condizioni_cliente.str_parametri.cod_agente_2 = ""
			iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
			iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ricambio"
			iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_ricambio"
			iuo_condizioni_cliente.wf_condizioni_cliente()
		end if		
	//---------------------------------------------------------------------------------------------------------------------------
		
		wf_calcola_totale(ll_riga)
		
end if
end event

type dw_call_center_manut_ricambi from datawindow within w_call_center_conferma_manut
event ue_totale_riga ( )
integer x = 1737
integer y = 1464
integer width = 2642
integer height = 696
integer taborder = 92
string title = "none"
string dataobject = "d_call_center_conferma_manut_ricambi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_totale_riga();wf_calcola_totale(getrow())

end event

event itemchanged;dec{4} ld_prezzo

choose case dwo.name
	case "cod_prodotto"

		ld_prezzo = wf_carica_prezzo(data, getitemnumber(row,"quan_ricambio") )
		
		setitem(row,"prezzo_ricambio", ld_prezzo)				
		
		postevent("ue_totale_riga")

	case "quan_ricambio"

		ld_prezzo = wf_carica_prezzo(getitemstring(row,"cod_prodotto"), dec(data) )
		
		setitem(row,"prezzo_ricambio", ld_prezzo)

		postevent("ue_totale_riga")
		
	case "sconto_1"
		
		postevent("ue_totale_riga")
		
	case "sconto_2"
		
		postevent("ue_totale_riga")

	case "prezzo_ricambio"
		
		postevent("ue_totale_riga")

end choose
end event

type dw_call_center_operai from datawindow within w_call_center_conferma_manut
event ue_imposta_fine ( )
event ue_totale_riga_operaio ( )
integer x = 1728
integer y = 1456
integer width = 2642
integer height = 696
integer taborder = 22
string title = "none"
string dataobject = "d_call_center_conferma_manut_operai_list"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_imposta_fine();setitem(getrow(),"data_fine",getitemdatetime(getrow(),"data_inizio"))
end event

event ue_totale_riga_operaio();wf_calcola_totale_operai(getrow())
end event

event itemchanged;datetime ldt_data_inizio, ldt_data_fine
time     lt_inizio, lt_fine
long     li_risposta, ll_ore_totali, ll_minuti_totali, ll_i
dec{4}   ld_costo_orario

ll_i = getrow()
if ll_i <= 0 then return -1
	
accepttext()

choose case dwo.name
	case "data_inizio", "ora_inizio", "data_fine", "ora_fine"


		if dwo.name = "data_inizio" then
			postevent("ue_imposta_fine")
		end if

		li_risposta = f_date_h_m(getitemdatetime(ll_i,"data_inizio"), &
		                         getitemdatetime(ll_i,"ora_inizio"), &
										 getitemdatetime(ll_i,"data_fine"),&
										 getitemdatetime(ll_i,"ora_fine"), &
										 ll_ore_totali, &
										 ll_minuti_totali)
		setitem(ll_i,"tot_ore",ll_ore_totali)
		setitem(ll_i,"tot_minuti",ll_minuti_totali)	
		
		postevent("ue_totale_riga_operaio")
		
	case "cod_operaio"
		ld_costo_orario = wf_carica_costo_operaio(data)
		setitem(ll_i,"costo_orario",ll_minuti_totali)	
		postevent("ue_totale_riga_operaio")

	case "tot_ore", "tot_minuti", "sconto_1", "costo_orario"
		postevent("ue_totale_riga_operaio")

end choose


end event

event itemfocuschanged;selecttext(1, len(gettext()))
end event

type dw_call_center_risorse from datawindow within w_call_center_conferma_manut
event ue_imposta_fine ( )
event ue_totale_riga ( )
integer x = 1728
integer y = 1456
integer width = 2642
integer height = 696
integer taborder = 31
string title = "none"
string dataobject = "d_call_center_conferma_manut_ris_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_imposta_fine();setitem(getrow(),"data_fine",getitemdatetime(getrow(),"data_inizio"))
end event

event ue_totale_riga();wf_calcola_totale_risorse(getrow())
end event

event itemchanged;datetime ldt_data_inizio, ldt_data_fine
time     lt_inizio, lt_fine
long     li_risposta, ll_ore_totali, ll_minuti_totali, ll_i
dec{4}   ld_costo_orario, ld_costi_aggiuntivi

ll_i = getrow()
if ll_i <= 0 then return -1
	
accepttext()

choose case dwo.name
	case "data_inizio", "ora_inizio", "data_fine", "ora_fine"


		if dwo.name = "data_inizio" then
			postevent("ue_imposta_fine")
		end if

		li_risposta = f_date_h_m(getitemdatetime(ll_i,"data_inizio"), &
		                         getitemdatetime(ll_i,"ora_inizio"), &
										 getitemdatetime(ll_i,"data_fine"),&
										 getitemdatetime(ll_i,"ora_fine"), &
										 ll_ore_totali, &
										 ll_minuti_totali)
		setitem(ll_i,"tot_ore",ll_ore_totali)
		setitem(ll_i,"tot_minuti",ll_minuti_totali)	
		postevent("ue_totale_riga")

	case "cod_risorsa"
		wf_carica_costo_risorse(data, getitemstring(row,"cod_cat_risorsa"), ref ld_costo_orario, ref ld_costi_aggiuntivi )		
		setitem(row,"costo_orario", ld_costo_orario)
		setitem(row,"costi_aggiuntivi", ld_costi_aggiuntivi)
		postevent("ue_totale_riga")
		
	case "tot_ore", "tot_minuti", "sconto_1", "costi_aggiuntivi", "costo_orario"
		postevent("ue_totale_riga")
		
end choose

end event

event itemfocuschanged;selecttext(1, len(gettext()))
end event

event rowfocuschanging;selectrow(currentrow,false)
selectrow(newrow,true)
end event

type dw_folder from u_folder within w_call_center_conferma_manut
integer x = 46
integer y = 1216
integer width = 4366
integer height = 1140
integer taborder = 11
end type

type dw_call_center_manut_ricambi_lista from datawindow within w_call_center_conferma_manut
event ue_key pbm_dwnkey
integer x = 69
integer y = 1456
integer width = 1509
integer height = 680
integer taborder = 32
boolean bringtotop = true
string title = "none"
string dataobject = "d_call_center_conferma_manut_ricambi_lis"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_key;timer(0.5,parent)

choose case key
	case keya!
		is_ricerca += "A"
	case keyb!
		is_ricerca += "B"
	case keyc!
		is_ricerca += "C"
	case keyd!
		is_ricerca += "D"
	case keye!
		is_ricerca += "E"
	case keyf!
		is_ricerca += "F"
	case keyg!
		is_ricerca += "G"
	case keyh!
		is_ricerca += "H"
	case keyi!
		is_ricerca += "I"
	case keyl!
		is_ricerca += "L"
	case keym!
		is_ricerca += "M"
	case keyn!
		is_ricerca += "N"
	case keyo!
		is_ricerca += "O"
	case keyp!
		is_ricerca += "P"
	case keyq!
		is_ricerca += "Q"
	case keyr!
		is_ricerca += "R"
	case keys!
		is_ricerca += "S"
	case keyt!
		is_ricerca += "T"
	case keyu!
		is_ricerca += "U"
	case keyv!
		is_ricerca += "V"
	case keyz!
		is_ricerca += "Z"
	case keyx!
		is_ricerca += "X"
	case keyy!
		is_ricerca += "Y"
	case keyw!
		is_ricerca += "W"
	case keyj!
		is_ricerca += "J"
	case keyk!
		is_ricerca += "K"
end choose

st_ric_op.text = is_ricerca
end event

event rowfocuschanging;selectrow(currentrow,false)
selectrow(newrow,true)
end event

type dw_ricerca_ricambi from u_dw_search within w_call_center_conferma_manut
integer x = 69
integer y = 1336
integer width = 2560
integer height = 120
integer taborder = 21
boolean bringtotop = true
string dataobject = "d_ext_ricerca_ricambio"
end type

type cb_retrieve_ricambi from commandbutton within w_call_center_conferma_manut
integer x = 2377
integer y = 1356
integer width = 233
integer height = 80
integer taborder = 102
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;integer ll_row
string ls_cod_prodotto

ll_row = dw_ricerca_ricambi.GetRow()
if ll_row > 0 then
	dw_ricerca_ricambi.Accepttext()
	ls_cod_prodotto = dw_ricerca_ricambi.GetItemString(ll_row, "rs_cod_prodotto")
	if ls_cod_prodotto <> "" and not isnull(ls_cod_prodotto) then
		dw_call_center_manut_ricambi_lista.SetFilter("cod_prodotto = '"+ls_cod_prodotto+"'")
		dw_call_center_manut_ricambi_lista.Filter()
	else
		dw_call_center_manut_ricambi_lista.SetFilter("")
		dw_call_center_manut_ricambi_lista.Filter()
	end if
end if

end event

