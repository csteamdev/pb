﻿$PBExportHeader$w_call_center_elenco_man.srw
forward
global type w_call_center_elenco_man from w_cs_xx_risposta
end type
type tv_man from treeview within w_call_center_elenco_man
end type
type dw_richieste_elenco_man from datawindow within w_call_center_elenco_man
end type
end forward

global type w_call_center_elenco_man from w_cs_xx_risposta
integer width = 3511
integer height = 2124
string title = "Manutenzioni Eseguite"
event ue_expand ( long ll_handle )
tv_man tv_man
dw_richieste_elenco_man dw_richieste_elenco_man
end type
global w_call_center_elenco_man w_call_center_elenco_man

type variables
string is_tipo

long	 il_anno, il_num, il_anno_man, il_num_man

boolean ib_global_service = false
end variables

forward prototypes
public function integer wf_report ()
public function integer wf_report_2 ()
end prototypes

public function integer wf_report ();long	 	ll_i, ll_j, ll_k, ll_riga, ll_anno_man, ll_num_man, ll_prog_fase, &
			ll_man_tot_ore, ll_man_tot_minuti, ll_fase_tot_ore, ll_fase_tot_minuti, &
			ll_operaio_tot_ore, ll_operaio_tot_minuti, ll_risorsa_tot_ore, ll_risorsa_tot_minuti, &
			ll_man_ris_tot_ore, ll_man_ris_tot_minuti, ll_fase_tot_ore_ris, ll_fase_tot_minuti_ris, ll_operaio_ore_pri, ll_operaio_minuti_pri, ll_ok

string 	ls_sql, ls_syntax, ls_error, ls_cod_attr, ls_des_attr, ls_des_fase, ls_note, &
			ls_cod_operaio, ls_cognome, ls_nome, ls_cod_risorsa_esterna, ls_descrizione, ls_cod_cat_risorse_esterne, ls_cod_operaio_pri

datetime ldt_fase_data_inizio, ldt_fase_ora_inizio, ldt_fase_data_fine, ldt_fase_ora_fine, &
			ldt_operaio_data_inizio, ldt_operaio_ora_inizio, ldt_operaio_data_fine, ldt_operaio_ora_fine, &
			ldt_risorsa_data_inizio, ldt_risorsa_ora_inizio, ldt_risorsa_data_fine, ldt_risorsa_ora_fine

datastore lds_manut, lds_fasi, lds_operai, lds_risorse


ls_sql = &
"select anno_registrazione, " + &
"		  num_registrazione, " + &
"		  cod_attrezzatura, " + &
"		  operaio_ore, " + &
"		  operaio_minuti, " + &
"		  risorsa_ore, " + &
"		  risorsa_minuti " + &
"from	  manutenzioni " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

ls_sql += " and anno_reg_richiesta = " + string(il_anno) + " and num_reg_richiesta = " + string(il_num)

ls_sql += " order by anno_registrazione ASC, num_registrazione ASC"

ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	g_mb.messagebox("OMNIA","Errore in impostazione select manutenzioni:~n" + ls_error,stopsign!)
	return -1
end if

lds_manut = create datastore

lds_manut.create(ls_syntax,ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	g_mb.messagebox("OMNIA","Errore in creazione datastore manutenzioni:~n" + ls_error,stopsign!)
	destroy lds_manut
	return -1
end if

lds_manut.settransobject(sqlca)

if lds_manut.retrieve() = -1 then
	g_mb.messagebox("OMNIA","Errore in lettura dati manutenzioni",stopsign!)
	destroy lds_manut
	return -1
end if

for ll_i = 1 to lds_manut.rowcount()
	
	ll_anno_man = lds_manut.getitemnumber(ll_i,"anno_registrazione")
	
	ll_num_man = lds_manut.getitemnumber(ll_i,"num_registrazione")
	
	ls_cod_attr = lds_manut.getitemstring(ll_i,"cod_attrezzatura")
	
	ll_man_tot_ore = lds_manut.getitemnumber(ll_i,"operaio_ore")
	
	ll_man_tot_minuti = lds_manut.getitemnumber(ll_i,"operaio_minuti")
	
	ll_man_ris_tot_ore = lds_manut.getitemnumber(ll_i, "risorsa_ore")
	
	ll_man_ris_tot_minuti = lds_manut.getitemnumber(ll_i, "risorsa_minuti")
	
	ls_des_attr = f_des_tabella("anag_attrezzature","cod_attrezzatura = '" + ls_cod_attr + "'","descrizione")
	
	if isnull(ls_des_attr) then
		ls_des_attr = ""
	end if
	
	ll_riga = dw_richieste_elenco_man.insertrow(0)
	
	dw_richieste_elenco_man.setitem(ll_riga,"tipo_riga","M")
	
	dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_anno_registrazione",ll_anno_man)
	
	dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_num_registrazione",ll_num_man)
	
	dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_cod_attrezzatura",ls_cod_attr)
	
	dw_richieste_elenco_man.setitem(ll_riga,"anag_attrezzature_descrizione",ls_des_attr)
	
	dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_tot_ore",ll_man_tot_ore + ll_man_ris_tot_ore)
	
	dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_tot_minuti",ll_man_tot_minuti + ll_man_ris_tot_minuti)
	
	// *** Michela 26/07/2006: se non è global service leggo direttamente gli operatori
	
	if ib_global_service = false then
		
		// *** prendo anche l'operatore nella manutenzione
		
		select cod_operaio,
		       operaio_ore,
				 operaio_minuti
		into   :ls_cod_operaio_pri,
		       :ll_operaio_ore_pri,
				 :ll_operaio_minuti_pri
		from   manutenzioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_man and
				 num_registrazione = :ll_num_man;
			
		ll_ok = 0 
		if not isnull(ls_cod_operaio_pri) and ls_cod_operaio_pri <> "" then
			
			ls_cognome = f_des_tabella("anag_operai","cod_operaio = '" + ls_cod_operaio_pri + "'","cognome")
			
			if isnull(ls_cognome) then
				ls_cognome = ""
			end if
			
			ls_nome = f_des_tabella("anag_operai","cod_operaio = '" + ls_cod_operaio_pri + "'","nome")
			
			if isnull(ls_nome) then
				ls_nome = ""
			end if			
				 
			ll_riga = dw_richieste_elenco_man.insertrow(0)			
			dw_richieste_elenco_man.setitem(ll_riga,"tipo_riga","E")		
				
			ll_riga = dw_richieste_elenco_man.insertrow(0)				
			dw_richieste_elenco_man.setitem(ll_riga,"tipo_riga","O")			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_cod_operaio",ls_cod_operaio_pri)	
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_tot_ore",ll_operaio_ore_pri)
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_tot_minuti",ll_operaio_minuti_pri)
			dw_richieste_elenco_man.setitem(ll_riga,"anag_operai_cognome",ls_cognome)
			dw_richieste_elenco_man.setitem(ll_riga,"anag_operai_nome",ls_nome)				
			ll_ok = 1
			
		end if
		
		ls_sql = &
		"select cod_operaio, " + &
		"		  costo_operatore, " + &
		"		  ore, " + &
		"		  minuti, " + &
		"		  tot_costo_operaio " + &
		"from	  manutenzioni_operai " + &
		"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		"		  anno_registrazione = " + string(ll_anno_man) + " and " + &
		"		  num_registrazione = " + string(ll_num_man) + " " 
		
		ls_sql += " order by cod_operaio ASC"
		
		ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)
		
		if not isnull(ls_error) and len(trim(ls_error)) > 0 then
			g_mb.messagebox("OMNIA","Errore in impostazione select operai:~n" + ls_error,stopsign!)
			destroy lds_manut
			return -1
		end if
		
		lds_operai = create datastore
		
		lds_operai.create(ls_syntax,ls_error)
		
		if not isnull(ls_error) and len(trim(ls_error)) > 0 then
			g_mb.messagebox("OMNIA","Errore in creazione datastore operai:~n" + ls_error,stopsign!)
			destroy lds_manut
			destroy lds_operai
			return -1
		end if
			
		
		lds_operai.settransobject(sqlca)
		
		if lds_operai.retrieve() = -1 then
			g_mb.messagebox("OMNIA","Errore in lettura dati operai",stopsign!)
			destroy lds_manut
			destroy lds_operai
			return -1
		end if
		
		for ll_k = 1 to lds_operai.rowcount()
			
			ls_cod_operaio = lds_operai.getitemstring(ll_k,"cod_operaio")	
			ll_operaio_tot_ore = lds_operai.getitemnumber(ll_k,"ore")			
			ll_operaio_tot_minuti = lds_operai.getitemnumber(ll_k,"minuti")
//			ldt_operaio_data_inizio = lds_operai.getitemdatetime(ll_k,"data_inizio")			
//			ldt_operaio_ora_inizio = lds_operai.getitemdatetime(ll_k,"ora_inizio")			
//			ldt_operaio_data_fine = lds_operai.getitemdatetime(ll_k,"data_fine")			
//			ldt_operaio_ora_fine = lds_operai.getitemdatetime(ll_k,"ora_fine")			
//			ll_operaio_tot_ore = lds_operai.getitemnumber(ll_k,"tot_ore")			
//			ll_operaio_tot_minuti = lds_operai.getitemnumber(ll_k,"tot_minuti")
			
			ls_cognome = f_des_tabella("anag_operai","cod_operaio = '" + ls_cod_operaio + "'","cognome")
			
			if isnull(ls_cognome) then
				ls_cognome = ""
			end if
			
			ls_nome = f_des_tabella("anag_operai","cod_operaio = '" + ls_cod_operaio + "'","nome")
			
			if isnull(ls_nome) then
				ls_nome = ""
			end if
					
			if ll_ok = 0 then
			
				ll_riga = dw_richieste_elenco_man.insertrow(0)			
				dw_richieste_elenco_man.setitem(ll_riga,"tipo_riga","E")		
				ll_ok = 1
				
			end if			
			
			ll_riga = dw_richieste_elenco_man.insertrow(0)
		
			dw_richieste_elenco_man.setitem(ll_riga,"tipo_riga","O")			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_cod_operaio",ls_cod_operaio)	
			
//			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_data_inizio",ldt_operaio_data_inizio)			
//			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_ora_inizio",ldt_operaio_ora_inizio)
//			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_data_fine",ldt_operaio_data_fine)
//			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_ora_fine",ldt_operaio_ora_fine)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_tot_ore",ll_operaio_tot_ore)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_tot_minuti",ll_operaio_tot_minuti)
			
			dw_richieste_elenco_man.setitem(ll_riga,"anag_operai_cognome",ls_cognome)
			
			dw_richieste_elenco_man.setitem(ll_riga,"anag_operai_nome",ls_nome)
			
		next
		
		destroy lds_operai		
		continue
		
	end if
	// ***
	
	ls_sql = &
	"select prog_fase, " + &
	"		  des_fase, " + &
	"		  data_inizio, " + &
	"		  ora_inizio, " + &
	"		  data_fine, " + &
	"		  ora_fine, " + &
	"		  note, " + &
	"		  tot_ore, " + &
	"		  tot_minuti, " + &
	"		  tot_ore_ris, " + &
	"		  tot_minuti_ris " + &	
	"from	  manutenzioni_fasi " + &
	"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
	"		  anno_registrazione = " + string(ll_anno_man) + " and " + &
	"		  num_registrazione = " + string(ll_num_man) + " "
	
	ls_sql += " order by data_inizio ASC, ora_inizio ASC, data_fine ASC, ora_fine ASC, prog_fase ASC"
	
	ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)
	
	if not isnull(ls_error) and len(trim(ls_error)) > 0 then
		g_mb.messagebox("OMNIA","Errore in impostazione select fasi:~n" + ls_error,stopsign!)
		destroy lds_manut
		return -1
	end if
	
	lds_fasi = create datastore
	
	lds_fasi.create(ls_syntax,ls_error)
	
	if not isnull(ls_error) and len(trim(ls_error)) > 0 then
		g_mb.messagebox("OMNIA","Errore in creazione datastore fasi:~n" + ls_error,stopsign!)
		destroy lds_manut
		destroy lds_fasi
		return -1
	end if
	
	lds_fasi.settransobject(sqlca)
	
	if lds_fasi.retrieve() = -1 then
		g_mb.messagebox("OMNIA","Errore in lettura dati fasi",stopsign!)
		destroy lds_manut
		destroy lds_fasi
		return -1
	end if
	
	for ll_j = 1 to lds_fasi.rowcount()
		
		ll_prog_fase = lds_fasi.getitemnumber(ll_j,"prog_fase")
		
		ls_des_fase = lds_fasi.getitemstring(ll_j,"des_fase")
		
		ldt_fase_data_inizio = lds_fasi.getitemdatetime(ll_j,"data_inizio")
		
		ldt_fase_ora_inizio = lds_fasi.getitemdatetime(ll_j,"ora_inizio")
		
		ldt_fase_data_fine = lds_fasi.getitemdatetime(ll_j,"data_fine")
		
		ldt_fase_ora_fine = lds_fasi.getitemdatetime(ll_j,"ora_fine")
		
		ls_note = lds_fasi.getitemstring(ll_j,"note")
		
		ll_fase_tot_ore = lds_fasi.getitemnumber(ll_j,"tot_ore")
		
		ll_fase_tot_minuti = lds_fasi.getitemnumber(ll_j,"tot_minuti")
		
		ll_fase_tot_ore_ris = lds_fasi.getitemnumber(ll_j,"tot_ore_ris")
		
		ll_fase_tot_minuti_ris = lds_fasi.getitemnumber(ll_j,"tot_minuti_ris")
		
		ll_riga = dw_richieste_elenco_man.insertrow(0)
	
		dw_richieste_elenco_man.setitem(ll_riga,"tipo_riga","F")
		
		dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_prog_fase",ll_prog_fase)
		
		dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_des_fase",ls_des_fase)
		
		dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_data_inizio",ldt_fase_data_inizio)
		
		dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_ora_inizio",ldt_fase_ora_inizio)
		
		dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_data_fine",ldt_fase_data_fine)
		
		dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_ora_fine",ldt_fase_ora_fine)
		
		dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_note",ls_note)
		
		dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_tot_ore",ll_fase_tot_ore + ll_fase_tot_ore_ris)
		
		dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_tot_minuti",ll_fase_tot_minuti + ll_fase_tot_minuti_ris)
		
		
		ls_sql = &
		"select cod_operaio, " + &
		"		  data_inizio, " + &
		"		  ora_inizio, " + &
		"		  data_fine, " + &
		"		  ora_fine, " + &
		"		  tot_ore, " + &
		"		  tot_minuti " + &
		"from	  manutenzioni_fasi_operai " + &
		"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		"		  anno_registrazione = " + string(ll_anno_man) + " and " + &
		"		  num_registrazione = " + string(ll_num_man) + " and " + &
		"		  prog_fase = " + string(ll_prog_fase)
		
		ls_sql += " order by data_inizio ASC, ora_inizio ASC, data_fine ASC, ora_fine ASC, cod_operaio ASC"
		
		ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)
		
		if not isnull(ls_error) and len(trim(ls_error)) > 0 then
			g_mb.messagebox("OMNIA","Errore in impostazione select operai:~n" + ls_error,stopsign!)
			destroy lds_manut
			return -1
		end if
		
		lds_operai = create datastore
		
		lds_operai.create(ls_syntax,ls_error)
		
		if not isnull(ls_error) and len(trim(ls_error)) > 0 then
			g_mb.messagebox("OMNIA","Errore in creazione datastore operai:~n" + ls_error,stopsign!)
			destroy lds_manut
			destroy lds_fasi
			destroy lds_operai
			return -1
		end if
			
		
		lds_operai.settransobject(sqlca)
		
		if lds_operai.retrieve() = -1 then
			g_mb.messagebox("OMNIA","Errore in lettura dati operai",stopsign!)
			destroy lds_manut
			destroy lds_fasi
			destroy lds_operai
			return -1
		end if
		
		for ll_k = 1 to lds_operai.rowcount()
			
			ls_cod_operaio = lds_operai.getitemstring(ll_k,"cod_operaio")
		
			ldt_operaio_data_inizio = lds_operai.getitemdatetime(ll_k,"data_inizio")
			
			ldt_operaio_ora_inizio = lds_operai.getitemdatetime(ll_k,"ora_inizio")
			
			ldt_operaio_data_fine = lds_operai.getitemdatetime(ll_k,"data_fine")
			
			ldt_operaio_ora_fine = lds_operai.getitemdatetime(ll_k,"ora_fine")
			
			ll_operaio_tot_ore = lds_operai.getitemnumber(ll_k,"tot_ore")
			
			ll_operaio_tot_minuti = lds_operai.getitemnumber(ll_k,"tot_minuti")
			
			ls_cognome = f_des_tabella("anag_operai","cod_operaio = '" + ls_cod_operaio + "'","cognome")
			
			if isnull(ls_cognome) then
				ls_cognome = ""
			end if
			
			ls_nome = f_des_tabella("anag_operai","cod_operaio = '" + ls_cod_operaio + "'","nome")
			
			if isnull(ls_nome) then
				ls_nome = ""
			end if
			
			if ll_k = 1 then
			
				ll_riga = dw_richieste_elenco_man.insertrow(0)
			
				dw_richieste_elenco_man.setitem(ll_riga,"tipo_riga","E")
				
			end if
			
			ll_riga = dw_richieste_elenco_man.insertrow(0)
		
			dw_richieste_elenco_man.setitem(ll_riga,"tipo_riga","O")
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_cod_operaio",ls_cod_operaio)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_data_inizio",ldt_operaio_data_inizio)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_ora_inizio",ldt_operaio_ora_inizio)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_data_fine",ldt_operaio_data_fine)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_ora_fine",ldt_operaio_ora_fine)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_tot_ore",ll_operaio_tot_ore)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_tot_minuti",ll_operaio_tot_minuti)
			
			dw_richieste_elenco_man.setitem(ll_riga,"anag_operai_cognome",ls_cognome)
			
			dw_richieste_elenco_man.setitem(ll_riga,"anag_operai_nome",ls_nome)
			
		next
		
		destroy lds_operai
		
		
		// michela 20/02/2004: risorse
		ls_sql = ""
		ls_sql = &
		"select cod_cat_risorse_esterne, " + &
		"		  cod_risorsa_esterna, " + &		
		"		  data_inizio, " + &
		"		  ora_inizio, " + &
		"		  data_fine, " + &
		"		  ora_fine, " + &
		"		  tot_ore, " + &
		"		  tot_minuti " + &
		"from	  manutenzioni_fasi_risorse " + &
		"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		"		  anno_registrazione = " + string(ll_anno_man) + " and " + &
		"		  num_registrazione = " + string(ll_num_man) + " and " + &
		"		  prog_fase = " + string(ll_prog_fase)
		
		ls_sql += " order by data_inizio ASC, ora_inizio ASC, data_fine ASC, ora_fine ASC, cod_risorsa_esterna ASC, cod_cat_risorse_esterne ASC "
		
		ls_syntax = sqlca.syntaxfromsql(ls_sql,'style(type=grid)',ls_error)
		
		if not isnull(ls_error) and len(trim(ls_error)) > 0 then
			g_mb.messagebox("OMNIA","Errore in impostazione select risorse:~n" + ls_error,stopsign!)
			destroy lds_manut
			return -1
		end if
		
		lds_risorse = create datastore
		
		lds_risorse.create(ls_syntax,ls_error)
		
		if not isnull(ls_error) and len(trim(ls_error)) > 0 then
			g_mb.messagebox("OMNIA","Errore in creazione datastore operai:~n" + ls_error,stopsign!)
			destroy lds_manut
			destroy lds_fasi
			destroy lds_risorse
			return -1
		end if
			
		
		lds_risorse.settransobject(sqlca)
		
		if lds_risorse.retrieve() = -1 then
			g_mb.messagebox("OMNIA","Errore in lettura dati risorse",stopsign!)
			destroy lds_manut
			destroy lds_fasi
			destroy lds_risorse
			return -1
		end if
		
		for ll_k = 1 to lds_risorse.rowcount()
			
			ls_cod_cat_risorse_esterne = lds_risorse.getitemstring(ll_k, "cod_cat_risorse_esterne")
			
			ls_cod_risorsa_esterna = lds_risorse.getitemstring(ll_k, "cod_risorsa_esterna")			
		
			ldt_risorsa_data_inizio = lds_risorse.getitemdatetime(ll_k,"data_inizio")
			
			ldt_risorsa_ora_inizio = lds_risorse.getitemdatetime(ll_k,"ora_inizio")
			
			ldt_risorsa_data_fine = lds_risorse.getitemdatetime(ll_k,"data_fine")
			
			ldt_risorsa_ora_fine = lds_risorse.getitemdatetime(ll_k,"ora_fine")
			
			ll_risorsa_tot_ore = lds_risorse.getitemnumber(ll_k,"tot_ore")
			
			ll_risorsa_tot_minuti = lds_risorse.getitemnumber(ll_k,"tot_minuti")
			
			ls_descrizione = f_des_tabella("anag_risorse_esterne","cod_risorsa_esterna = '" + ls_cod_risorsa_esterna + "'  AND cod_cat_risorse_esterne = '" + ls_cod_cat_risorse_esterne + "'" ,"descrizione")
			
			if isnull(ls_descrizione) then
				ls_descrizione = ""
			end if
			
			if ll_k = 1 then
			
				ll_riga = dw_richieste_elenco_man.insertrow(0)
			
				dw_richieste_elenco_man.setitem(ll_riga,"tipo_riga","E")
				
			end if
			
			ll_riga = dw_richieste_elenco_man.insertrow(0)
		
			dw_richieste_elenco_man.setitem(ll_riga,"tipo_riga","O")
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_cod_operaio",ls_cod_risorsa_esterna)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_data_inizio",ldt_risorsa_data_inizio)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_ora_inizio",ldt_risorsa_ora_inizio)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_data_fine",ldt_risorsa_data_fine)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_ora_fine",ldt_risorsa_ora_fine)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_tot_ore",ll_risorsa_tot_ore)
			
			dw_richieste_elenco_man.setitem(ll_riga,"manutenzioni_fasi_operai_tot_minuti",ll_risorsa_tot_minuti)
			
			dw_richieste_elenco_man.setitem(ll_riga,"anag_operai_cognome",ls_descrizione)
			
			dw_richieste_elenco_man.setitem(ll_riga,"anag_operai_nome"," ")
			
		next
		
		destroy lds_risorse		
		
	next
	
	destroy lds_fasi
	
next

destroy lds_manut

return 0
end function

public function integer wf_report_2 ();long ll_rows
string ls_cod_operatore

ll_rows = dw_richieste_elenco_man.retrieve(s_cs_xx.cod_azienda, il_anno, il_num)

setnull(il_anno_man)
setnull(il_num_man)

if ll_rows < 0 then
	g_mb.messagebox("OMNIA", "Errore durante la retrieve delle manutenzioni!", Exclamation!)
	
elseif ll_rows > 0 then
	//nel caso global service può esistere una sola manutenzione (con una o più fasi)
	
	il_anno_man = dw_richieste_elenco_man.getitemnumber(1, "manutenzioni_anno_reg_man")
	il_num_man = dw_richieste_elenco_man.getitemnumber(1, "manutenzioni_num_reg_man")
	dw_richieste_elenco_man.expandall( )
	
end if

return ll_rows
end function

on w_call_center_elenco_man.create
int iCurrent
call super::create
this.tv_man=create tv_man
this.dw_richieste_elenco_man=create dw_richieste_elenco_man
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tv_man
this.Control[iCurrent+2]=this.dw_richieste_elenco_man
end on

on w_call_center_elenco_man.destroy
call super::destroy
destroy(this.tv_man)
destroy(this.dw_richieste_elenco_man)
end on

event pc_setwindow;call super::pc_setwindow;string ls_cod_tipo_richiesta,ls_des_tipo_richiesta, ls_Flag, ls_path
long ll_anno_documento,ll_num_documento

setnull(s_cs_xx.parametri.parametro_s_1)

il_anno = s_cs_xx.parametri.parametro_d_1

setnull(s_cs_xx.parametri.parametro_d_1)

il_num = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_2)

select cod_tipo_richiesta,
       anno_documento,
		 num_documento
into   :ls_cod_tipo_richiesta,
       :ll_anno_documento,
		 :ll_num_documento
from   tab_richieste
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno and
		 num_registrazione = :il_num;

select des_tipo_richiesta
into   :ls_des_tipo_richiesta
from   tab_tipi_richieste
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_richiesta = :s_cs_xx.parametri.parametro_s_1;

title = "Manutenzioni Eseguite - Richiesta: "+ls_des_tipo_richiesta+" " + string(il_anno) + "/" + string(il_num)

select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
if sqlca.sqlcode = 0 and ls_flag ="S" then ib_global_service = true

//----------------------------------------------

//if ib_global_service then
//	//datawindow tree
//	
//	dw_richieste_elenco_man.dataobject = "d_tv_man_eseguite"
//	dw_richieste_elenco_man.settransobject(sqlca)
//	
//	//livello 1: manutenzione
//	ls_path = s_cs_xx.volume + s_cs_xx.risorse + "man_eseguita.png"
//	dw_richieste_elenco_man.object.p_man.FileName = ls_path
//	
//	//livello 2: fase
//	ls_path = s_cs_xx.volume + s_cs_xx.risorse + "man_fase.png"
//	dw_richieste_elenco_man.object.p_fase.FileName = ls_path
//	
//	//livello 3a: operatore
//	ls_path = s_cs_xx.volume + s_cs_xx.risorse + "man_operatori.png"
//	dw_richieste_elenco_man.object.p_ope.FileName = ls_path
//	
//	//livello 3b: risorsa esterna
//	ls_path = s_cs_xx.volume + s_cs_xx.risorse + "man_risorse.png"
//	dw_richieste_elenco_man.object.p_ris.FileName = ls_path
//	
//	wf_report_2()
//else
	dw_richieste_elenco_man.dataobject = "d_call_center_elenco_man"
	dw_richieste_elenco_man.settransobject(sqlca)
	
	wf_report()
//end if


//---------------------------------------------


end event

type tv_man from treeview within w_call_center_elenco_man
integer x = 23
integer y = 136
integer width = 3429
integer height = 1864
integer taborder = 20
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 8388608
long backcolor = 12632256
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event clicked;string ls_tag
long ll_anno_reg, ll_num_reg, ll_pos
treeviewitem ltvi_item

if not isnull(handle) and handle > 0 then
	
	tv_man.getItem(handle, ltvi_item)
	ls_tag = ltvi_item.data
	if ls_tag = "" or isnull(ls_tag) then return
	
	ll_pos = pos(ls_tag, "-")
	if ll_pos > 0 then
	else
		return
	end if
	
	ll_anno_reg = long(left(ls_tag, ll_pos - 1))
	ll_num_reg = long(right(ls_tag, len(ls_tag) - ll_pos))
	
	if ll_anno_reg > 0 and ll_num_reg > 0 then
	else
		return
	end if	
	
	if not isvalid(w_manutenzioni) then
		
		s_cs_xx_parametri lstr_parametri
		
		lstr_parametri.parametro_ul_1 = ll_anno_reg
		lstr_parametri.parametro_ul_2 = ll_num_reg
	
		window_open_Parm(w_manutenzioni, -1, lstr_parametri)			
	
	else
		w_manutenzioni.show()
		w_manutenzioni.wf_carica_singola_registrazione(ll_anno_reg, ll_num_reg)
	end if
end if
end event

type dw_richieste_elenco_man from datawindow within w_call_center_elenco_man
integer x = 23
integer y = 24
integer width = 3429
integer height = 1976
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_call_center_elenco_man"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;s_cs_xx_parametri lstr_parametri
long ll_row

if dwo.name = "cf_manutenzione" then
	if not isnull(row) and row > 0 then
		if not isvalid(w_manutenzioni) then
			
			lstr_parametri.parametro_ul_1 = getitemnumber(row,"manutenzioni_anno_registrazione")
			lstr_parametri.parametro_ul_2 = getitemnumber(row,"manutenzioni_num_registrazione")
		
		  	Window_Open_Parm(w_manutenzioni, -1, lstr_parametri)			
			
			//window_open(w_manutenzioni,-1)
		else
			w_manutenzioni.show()
			w_manutenzioni.wf_carica_singola_registrazione(getitemnumber(row,"manutenzioni_anno_registrazione"),getitemnumber(row,"manutenzioni_num_registrazione"))
		end if		
	end if
end if
end event

event clicked;//s_cs_xx_parametri lstr_parametri
//
//if il_anno_man > 0 and il_num_man > 0 and ib_global_service and (dwo.name = "p_man" or dwo.name = "cf_manut")  then
//	//nel caso global service può esistere una sola manutenzione (con una o più fasi)
//	
//	if not isvalid(w_manutenzioni) then
//		
//		lstr_parametri.parametro_ul_1 = il_anno_man
//		lstr_parametri.parametro_ul_2 = il_num_man
//	
//		Window_Open_Parm(w_manutenzioni, -1, lstr_parametri)			
//		
//		//window_open(w_manutenzioni,-1)
//	else
//		w_manutenzioni.show()
//		w_manutenzioni.wf_carica_singola_registrazione(il_anno_man, il_num_man)
//	end if
//end if
end event

