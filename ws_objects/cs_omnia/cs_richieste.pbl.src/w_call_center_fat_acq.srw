﻿$PBExportHeader$w_call_center_fat_acq.srw
forward
global type w_call_center_fat_acq from w_cs_xx_principale
end type
type cb_chiudi from commandbutton within w_call_center_fat_acq
end type
type cb_ok from commandbutton within w_call_center_fat_acq
end type
type dw_1 from uo_cs_xx_dw within w_call_center_fat_acq
end type
end forward

global type w_call_center_fat_acq from w_cs_xx_principale
integer width = 2610
integer height = 1416
string title = "Fattura Acquisto"
cb_chiudi cb_chiudi
cb_ok cb_ok
dw_1 dw_1
end type
global w_call_center_fat_acq w_call_center_fat_acq

type variables
long il_anno_ric, il_num_ric
boolean ib_nuova_fattura = false
end variables

on w_call_center_fat_acq.create
int iCurrent
call super::create
this.cb_chiudi=create cb_chiudi
this.cb_ok=create cb_ok
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_chiudi
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.dw_1
end on

on w_call_center_fat_acq.destroy
call super::destroy
destroy(this.cb_chiudi)
destroy(this.cb_ok)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;long ll_count

il_anno_ric = long(s_cs_xx.parametri.parametro_d_1 )
il_num_ric = long(s_cs_xx.parametri.parametro_d_2)

//dw_1.insertrow(0)
set_w_options(c_noenablepopup + c_closenosave)
dw_1.set_dw_options(sqlca, pcca.null_object, c_default, c_default)

//verifica se esiste la fattura di acquisto
select 	count(*)
into		:ll_count
from tes_fat_acq
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_reg_richiesta=:il_anno_ric and
			num_reg_richiesta=:il_num_ric;
			
if sqlca.sqlcode<0 then
	//errore
	rollback;
	g_mb.messagebox("OMNIA", "Errore durante la lettura fattura acquisto! "+sqlca.sqlerrtext)
	cb_chiudi.postevent(clicked!)
	
elseif ll_count=0 then
	//non esiste la fattura
	ib_nuova_fattura = true
	dw_1.postevent("pcd_new")
	
else
	//esiste già la fattura
	ib_nuova_fattura = false
	dw_1.postevent("pcd_new")
	
end if


end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_1, &
                 "cod_tipo_fat_acq", &
                 sqlca, &
                 "tab_tipi_fat_acq", &
                 "cod_tipo_fat_acq", &
                 "des_tipi_fat_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_1, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(dw_1, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type cb_chiudi from commandbutton within w_call_center_fat_acq
integer x = 1335
integer y = 1208
integer width = 402
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type cb_ok from commandbutton within w_call_center_fat_acq
integer x = 901
integer y = 1208
integer width = 402
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Crea"
end type

event clicked;long ll_num_registrazione,  ll_anno_registrazione, ll_anno_doc_origine, ll_num_doc_origine
datetime ldt_data_registrazione
string ls_cod_tipo_fat_acq, ls_cod_fornitore, ls_cod_tipo_fat_ven_det, &
		ls_cod_valuta, ls_des_prodotto, ls_cod_iva, ls_protocollo
//string ls_des_num_fat_acq
decimal ld_importo

if g_mb.messagebox("OMNIA","Creare la fattura?", Question!, YesNo!, 1) = 1 then
	
	dw_1.accepttext()
	
	ldt_data_registrazione = dw_1.getitemdatetime(1,"data_fattura")
	ls_cod_tipo_fat_acq = dw_1.getitemstring(1,"cod_tipo_fat_acq")
	ls_cod_fornitore = dw_1.getitemstring(1,"cod_fornitore")
	ls_cod_tipo_fat_ven_det = dw_1.getitemstring(1,"cod_tipo_det_acq")
	ld_importo = dw_1.getitemdecimal(1,"importo")
//	ls_des_num_fat_acq = dw_1.getitemstring(1,"num_ordine_acquisto")
	ls_cod_valuta = dw_1.getitemstring(1,"cod_valuta")
	ls_des_prodotto = dw_1.getitemstring(1,"des_prodotto")
	ls_cod_iva = dw_1.getitemstring(1,"cod_iva")
	ls_protocollo = dw_1.getitemstring(1,"protocollo")
	ll_anno_doc_origine = dw_1.getitemnumber(1,"anno_doc_origine")
	ll_num_doc_origine = dw_1.getitemnumber(1,"num_doc_origine")
	
	//controlla campi obbligatori
	if isnull(ldt_data_registrazione) then
		g_mb.messagebox("Errore","data documento obbligatoria!", Exclamation!)
		return
	end if
	if isnull(ls_cod_tipo_fat_acq) or ls_cod_tipo_fat_acq="" then
		g_mb.messagebox("Errore","Tipo fattura obbligatoria!", Exclamation!)
		return
	end if
	if isnull(ls_cod_tipo_fat_ven_det) or ls_cod_tipo_fat_ven_det="" then
		g_mb.messagebox("Errore","Tipo dettaglio fattura obbligatoria!", Exclamation!)
		return
	end if
	if isnull(ls_cod_fornitore) or ls_cod_fornitore="" then
		g_mb.messagebox("Errore","Fornitore obbligatorio!", Exclamation!)
		return
	end if
	if isnull(ld_importo) or ld_importo<=0 then
		g_mb.messagebox("Errore","Inserire un importo fattura superiore a zero!", Exclamation!)
		return
	end if
//	if isnull(ls_des_num_fat_acq) or ls_des_num_fat_acq="" then
//		g_mb.messagebox("Errore","N° ordine acquisto obbligatorio!", Exclamation!)
//		return
//	end if
	if isnull(ls_cod_valuta) or ls_cod_valuta="" then
		g_mb.messagebox("Errore","Codice Valuta obbligatoria!", Exclamation!)
		return
	end if
	if isnull(ls_des_prodotto) or ls_des_prodotto="" then
		g_mb.messagebox("Errore","Des. Prodotto obbligatorio!", Exclamation!)
		return
	end if
	if isnull(ls_cod_iva) or ls_cod_iva="" then
		g_mb.messagebox("Errore","Codice Iva obbligatoria!", Exclamation!)
		return
	end if
	if isnull(ls_protocollo) or ls_protocollo="" then
		g_mb.messagebox("Errore","N° fattura obbligatoria!", Exclamation!)
		return
	end if
	if isnull(ll_anno_doc_origine) or ll_anno_doc_origine<=0 then
		g_mb.messagebox("Errore","Anno N° Ordine obbligatorio!", Exclamation!)
		return
	end if
	if isnull(ll_num_doc_origine) or ll_num_doc_origine<=0 then
		g_mb.messagebox("Errore","N° Ordine obbligatorio!", Exclamation!)
		return
	end if
	//------------------------------------------
	
	ll_anno_registrazione = f_anno_esercizio()
	
	select max(num_registrazione)
	into :ll_num_registrazione
	from tes_fat_acq
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_registrazione;
				
	if isnull(ll_num_registrazione) then ll_num_registrazione=0
	ll_num_registrazione += 1
	
	insert into tes_fat_acq
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		data_registrazione,
		cod_tipo_fat_acq,
		cod_fornitore,
		//des_num_fat_acq,
		anno_reg_richiesta,
		num_reg_richiesta,
		cod_valuta,
		anno_doc_origine,
		num_doc_origine,
		protocollo)
	values
	(	:s_cs_xx.cod_azienda,
		:ll_anno_registrazione,
		:ll_num_registrazione,
		:ldt_data_registrazione,
		:ls_cod_tipo_fat_acq,
		:ls_cod_fornitore,
//		:ls_des_num_fat_acq,
		:il_anno_ric,
		:il_num_ric,
		:ls_cod_valuta,
		:ll_anno_doc_origine,
		:ll_num_doc_origine,
		:ls_protocollo);
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore","Errore in inserimento testata fattura acquisto: "+sqlca.sqlerrtext, StopSign!)
		return
	end if
	
	//inserisci dettaglio fattura acquisto
	insert into det_fat_acq
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		prog_riga_fat_acq,
		cod_tipo_det_acq,
		imponibile_iva,
		flag_imponibile_forzato,
		quan_fatturata,
		prezzo_acquisto,
		des_prodotto,
		cod_iva)
	values
	(	:s_cs_xx.cod_azienda,
		:ll_anno_registrazione,
		:ll_num_registrazione,
		1,
		:ls_cod_tipo_fat_ven_det,
		:ld_importo,
		'N',
		1,
		:ld_importo,
		:ls_des_prodotto,
		:ls_cod_iva);
	
	if sqlca.sqlcode < 0 then
		rollback;
		g_mb.messagebox("Errore","Errore in inserimento dettaglio fattura acquisto: "+sqlca.sqlerrtext, StopSign!)
		return
	end if
	
	commit;
	g_mb.messagebox("Errore","Fattura acquisto creata correttamente: "&
				+string(ll_anno_registrazione)+"/"+ string(ll_num_registrazione), StopSign!)
	cb_chiudi.postevent(clicked!)
	
end if
end event

type dw_1 from uo_cs_xx_dw within w_call_center_fat_acq
integer x = 32
integer y = 32
integer width = 2505
integer height = 1152
integer taborder = 10
string dataobject = "d_tes_fat_acq_richiesta"
boolean border = false
end type

event pcd_new;call super::pcd_new;string ls_cod_tipo_fat_acq, ls_cod_tipo_det_acq, ls_cod_fornitore, &
			ls_des_prodotto, ls_cod_iva, ls_cod_valuta, ls_protocollo
//string ls_des_num_fat_acq
long ll_anno_reg_fattura, ll_num_reg_fattura, ll_anno_doc_origine, ll_num_doc_origine
datetime ldt_data_registrazione
decimal ld_imponibile

if ib_nuova_fattura then

	cb_ok.enabled=true
	cb_chiudi.text = "Annulla"
//	cb_fornitori_ricerca.enabled = true
	this.title = "Creazione Fattura Acquisto fornitori per la richiesta " + string(il_anno_ric) + "/" + string(il_num_ric)
	
	select con_fat_acq.cod_tipo_fat_acq
	into   :ls_cod_tipo_fat_acq
	from   	con_fat_acq
	where  con_fat_acq.cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode = 0 and ls_cod_tipo_fat_acq<>"" and not isnull(ls_cod_tipo_fat_acq) then
		select tab_tipi_fat_acq.cod_tipo_det_acq
		into   :ls_cod_tipo_det_acq
		from   tab_tipi_fat_acq
		where  	tab_tipi_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and 
					tab_tipi_fat_acq.cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;
					
		dw_1.setitem(1, "cod_tipo_fat_acq", ls_cod_tipo_fat_acq)
		dw_1.setitem(1, "cod_tipo_det_acq", ls_cod_tipo_det_acq)
	end if
	
else
	//la fattura esiste già
	select 	a.anno_registrazione,
				a.num_registrazione,
				a.cod_fornitore,
				a.data_registrazione,
				b.imponibile_iva,
				//a.des_num_fat_acq,
				a.cod_tipo_fat_acq,
				b.cod_tipo_det_acq,
				a.cod_valuta,
				b.des_prodotto,
				b.cod_iva,
				a.anno_doc_origine,
				a.num_doc_origine,
				a.protocollo
	into		:ll_anno_reg_fattura,
				:ll_num_reg_fattura,
				:ls_cod_fornitore,
				:ldt_data_registrazione,
				:ld_imponibile,
				//:ls_des_num_fat_acq,
				:ls_cod_tipo_fat_acq,
				:ls_cod_tipo_det_acq,
				:ls_cod_valuta,
				:ls_des_prodotto,
				:ls_cod_iva,
				:ll_anno_doc_origine,
				:ll_num_doc_origine,
				:ls_protocollo
	from tes_fat_acq a
	join det_fat_acq b on 	b.cod_azienda=a.cod_azienda and
										b.anno_registrazione=a.anno_registrazione and
										b.num_registrazione=a.num_registrazione
	where 	a.cod_azienda=:s_cs_xx.cod_azienda and
				a.anno_reg_richiesta=:il_anno_ric and
				a.num_reg_richiesta=:il_num_ric;
				
	if sqlca.sqlcode<0 then
		g_mb.messagebox("OMNIA", "Errore in lettura dati fattura! "+sqlca.sqlerrtext, StopSign!)
		cb_chiudi.postevent(clicked!)
		return
	end if
	
	cb_ok.enabled=false
	cb_chiudi.text = "Chiudi"
	this.title = "Fattura Acquisto fornitori per la richiesta " + string(il_anno_ric) + "/" + string(il_num_ric)
	
	//proteggi i campi della fattura (solo visualizzazione)
	//dw_1.object.num_ordine_acquisto.protect = 1
	dw_1.object.cod_fornitore.protect = 1
	dw_1.object.data_fattura.protect = 1
	dw_1.object.importo.protect = 1
	dw_1.object.cod_tipo_fat_acq.protect = 1
	dw_1.object.cod_tipo_det_acq.protect = 1
	
	dw_1.object.cod_valuta.protect = 1
	dw_1.object.des_prodotto.protect = 1
	dw_1.object.cod_iva.protect = 1
	
	dw_1.object.anno_doc_origine.protect = 1
	dw_1.object.num_doc_origine.protect = 1
	dw_1.object.protocollo.protect = 1
	
	dw_1.object.b_ricerca_fornitore.enabled = false
	//riempi i campi della dw
	dw_1.setitem(1, "anno_reg_fattura", ll_anno_reg_fattura)
	dw_1.setitem(1, "num_reg_fattura", ll_num_reg_fattura)
//	dw_1.setitem(1, "num_ordine_acquisto", ls_des_num_fat_acq)
	dw_1.setitem(1, "cod_fornitore", ls_cod_fornitore)
	dw_1.setitem(1, "data_fattura", ldt_data_registrazione)
	dw_1.setitem(1, "importo", ld_imponibile)
	dw_1.setitem(1, "cod_tipo_fat_acq", ls_cod_tipo_fat_acq)
	dw_1.setitem(1, "cod_tipo_det_acq", ls_cod_tipo_det_acq)
	
	dw_1.setitem(1, "cod_valuta", ls_cod_valuta)
	dw_1.setitem(1, "des_prodotto", ls_des_prodotto)
	dw_1.setitem(1, "cod_iva", ls_cod_iva)
	
	dw_1.setitem(1, "anno_doc_origine", ll_anno_doc_origine)
	dw_1.setitem(1, "num_doc_origine", ll_num_doc_origine)
	dw_1.setitem(1, "protocollo", ls_protocollo)
end if
end event

event getfocus;call super::getfocus;dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_1,"cod_fornitore")
end choose
end event

