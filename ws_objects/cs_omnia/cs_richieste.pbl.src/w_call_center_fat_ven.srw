﻿$PBExportHeader$w_call_center_fat_ven.srw
forward
global type w_call_center_fat_ven from w_cs_xx_principale
end type
type dw_1 from uo_cs_xx_dw within w_call_center_fat_ven
end type
type cb_chiudi from commandbutton within w_call_center_fat_ven
end type
type cb_ok from commandbutton within w_call_center_fat_ven
end type
end forward

global type w_call_center_fat_ven from w_cs_xx_principale
integer width = 2610
integer height = 1468
string title = "Fattura Vendita"
dw_1 dw_1
cb_chiudi cb_chiudi
cb_ok cb_ok
end type
global w_call_center_fat_ven w_call_center_fat_ven

type variables
long il_anno_ric, il_num_ric
boolean ib_nuova_fattura = false
end variables

on w_call_center_fat_ven.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.cb_chiudi=create cb_chiudi
this.cb_ok=create cb_ok
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.cb_chiudi
this.Control[iCurrent+3]=this.cb_ok
end on

on w_call_center_fat_ven.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.cb_chiudi)
destroy(this.cb_ok)
end on

event pc_setwindow;call super::pc_setwindow;long ll_count

il_anno_ric = long(s_cs_xx.parametri.parametro_d_1 )
il_num_ric = long(s_cs_xx.parametri.parametro_d_2)

//dw_1.insertrow(0)
set_w_options(c_noenablepopup + c_closenosave)
dw_1.set_dw_options(sqlca, pcca.null_object, c_default, c_default)

//verifica se esiste la fattura di vendita
select 	count(*)
into		:ll_count
from tes_fat_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_reg_richiesta=:il_anno_ric and
			num_reg_richiesta=:il_num_ric;
			
if sqlca.sqlcode<0 then
	//errore
	rollback;
	g_mb.messagebox("OMNIA", "Errore durante la lettura fattura vendita! "+sqlca.sqlerrtext)
	cb_chiudi.postevent(clicked!)
	
elseif ll_count=0 then
	//non esiste la fattura
	ib_nuova_fattura = true
	dw_1.postevent("pcd_new")
	
else
	//esiste già la fattura
	ib_nuova_fattura = false
	dw_1.postevent("pcd_new")
	
end if


end event

event pc_setddlb;call super::pc_setddlb; f_po_loaddddw_dw(dw_1, &
                 "cod_tipo_fat_ven", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_1, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_1, &
                 "cod_conto", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_1,"cod_centro_costo",sqlca,&
                 "tab_centri_costo","cod_centro_costo","des_centro_costo", &
					  "tab_centri_costo.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "((tab_centri_costo.flag_blocco <> 'S') or (tab_centri_costo.flag_blocco = 'S' and tab_centri_costo.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(dw_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(dw_1, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event getfocus;call super::getfocus;dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type dw_1 from uo_cs_xx_dw within w_call_center_fat_ven
integer x = 27
integer y = 32
integer width = 2505
integer height = 1180
integer taborder = 10
string dataobject = "d_tes_fat_ven_richiesta"
boolean border = false
end type

event pcd_new;call super::pcd_new;string ls_cod_tipo_fat_ven, ls_cod_tipo_det_ven, ls_cod_cliente, ls_cod_conto, ls_cod_centro_costo,&
			ls_cod_deposito, ls_cod_valuta, ls_des_prodotto, ls_cod_iva
long ll_anno_reg_fattura, ll_num_reg_fattura
datetime ldt_data_registrazione
decimal ld_imponibile

if ib_nuova_fattura then

	cb_ok.enabled=true
	cb_chiudi.text = "Annulla"
	dw_1.object.b_ricerca_cliente.enabled = true
	this.title = "Creazione Fattura Vendita Clienti per la richiesta " + string(il_anno_ric) + "/" + string(il_num_ric)
	
	select cod_tipo_fat_ven
	into :ls_cod_tipo_fat_ven
	from con_fat_ven
	where cod_azienda=:s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode<0 then
		g_mb.messagebox("OMNIA","Errore in lettura tipo fattura vendita da parametri fatture (con_fat_ven)!"+&
									sqlca.sqlerrtext,StopSign!)
		setnull(ls_cod_tipo_fat_ven)
	end if
	
	if isnull(ls_cod_tipo_fat_ven) or ls_cod_tipo_fat_ven="" then
		g_mb.messagebox("OMNIA","Impossibile leggere il tipo dettaglio fattura da tipi fatture vendita (tab_tipi_fat_ven): "+&
										"manca il tipo fattura della testata di vendita!",StopSign!)
		setnull(ls_cod_tipo_fat_ven)
		setnull(ls_cod_tipo_det_ven)
	else
		select cod_tipo_det_ven
		into :ls_cod_tipo_det_ven
		from tab_tipi_fat_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
					
		if sqlca.sqlcode<0 then
			g_mb.messagebox("OMNIA","Errore in lettura dettaglio tipo fattura vendita da tipo fatture vendita (con_fat_ven)!"+&
											sqlca.sqlerrtext,StopSign!)
			setnull(ls_cod_tipo_fat_ven)
		end if
		
		If isnull(ls_cod_tipo_fat_ven) or ls_cod_tipo_fat_ven="" then
			g_mb.messagebox("OMNIA","Manca il tipo dettaglio fattura da tipi fatture vendita (tab_tipi_fat_ven)!",StopSign!)
			setnull(ls_cod_tipo_det_ven)
		end if
	end if

	dw_1.setitem(1, "cod_tipo_fat_ven", ls_cod_tipo_fat_ven)
	dw_1.setitem(1, "cod_tipo_det_ven", ls_cod_tipo_det_ven)
	
//	select con_fat_acq.cod_tipo_fat_acq
//	into   :ls_cod_tipo_fat_acq
//	from   	con_fat_acq
//	where  con_fat_acq.cod_azienda = :s_cs_xx.cod_azienda;
//	
//	if sqlca.sqlcode = 0 and ls_cod_tipo_fat_acq<>"" and not isnull(ls_cod_tipo_fat_acq) then
//		select tab_tipi_fat_acq.cod_tipo_det_acq
//		into   :ls_cod_tipo_det_acq
//		from   tab_tipi_fat_acq
//		where  	tab_tipi_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and 
//					tab_tipi_fat_acq.cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;
//					
//		dw_1.setitem(1, "cod_tipo_fat_acq", ls_cod_tipo_fat_acq)
//		dw_1.setitem(1, "cod_tipo_det_acq", ls_cod_tipo_det_acq)
//	end if
	
else
	//la fattura esiste già
	select 	a.anno_registrazione,
				a.num_registrazione,
				a.cod_cliente,
				a.data_registrazione,
				b.prezzo_vendita,
				a.cod_tipo_fat_ven,
				b.cod_tipo_det_ven,
				c.cod_conto,
				c.cod_centro_costo,
				a.cod_deposito,
				a.cod_valuta,
				b.des_prodotto,
				b.cod_iva
	into		:ll_anno_reg_fattura,
				:ll_num_reg_fattura,
				:ls_cod_cliente,
				:ldt_data_registrazione,
				:ld_imponibile,
				:ls_cod_tipo_fat_ven,
				:ls_cod_tipo_det_ven,
				:ls_cod_conto,
				:ls_cod_centro_costo,
				:ls_cod_deposito,
				:ls_cod_valuta,
				:ls_des_prodotto,
				:ls_cod_iva
	from tes_fat_ven a
	join det_fat_ven b on 	b.cod_azienda=a.cod_azienda and
										b.anno_registrazione=a.anno_registrazione and
										b.num_registrazione=a.num_registrazione
	join det_fat_ven_cc c on 	c.cod_azienda=b.cod_azienda and
										c.anno_registrazione=b.anno_registrazione and
										c.num_registrazione=b.num_registrazione and
										c.prog_riga_fat_ven=b.prog_riga_fat_ven
	where 	a.cod_azienda=:s_cs_xx.cod_azienda and
				a.anno_reg_richiesta=:il_anno_ric and
				a.num_reg_richiesta=:il_num_ric;
				
	if sqlca.sqlcode<0 then
		g_mb.messagebox("OMNIA", "Errore in lettura dati fattura! "+sqlca.sqlerrtext, StopSign!)
		cb_chiudi.postevent(clicked!)
		return
	end if
	
	cb_ok.enabled=false
	cb_chiudi.text = "Chiudi"
	this.title = "Fattura Vendita Clienti per la richiesta " + string(il_anno_ric) + "/" + string(il_num_ric)
	
	//proteggi i campi della fattura (solo visualizzazione)
	dw_1.object.cod_cliente.protect = 1
	dw_1.object.data_fattura.protect = 1
	dw_1.object.importo.protect = 1
	dw_1.object.cod_tipo_fat_ven.protect = 1
	dw_1.object.cod_tipo_det_ven.protect = 1
	
	dw_1.object.cod_deposito.protect = 1
	dw_1.object.cod_valuta.protect = 1
	dw_1.object.des_prodotto.protect = 1
	dw_1.object.cod_iva.protect = 1
	
	dw_1.object.cod_conto.protect = 1
	dw_1.object.cod_centro_costo.protect = 1
	
	dw_1.object.b_ricerca_cliente.enabled = false
	
	//riempi i campi della dw
	dw_1.setitem(1, "anno_reg_fattura", ll_anno_reg_fattura)
	dw_1.setitem(1, "num_reg_fattura", ll_num_reg_fattura)
	dw_1.setitem(1, "cod_cliente", ls_cod_cliente)
	dw_1.setitem(1, "data_fattura", ldt_data_registrazione)
	dw_1.setitem(1, "importo", ld_imponibile)
	dw_1.setitem(1, "cod_tipo_fat_ven", ls_cod_tipo_fat_ven)
	dw_1.setitem(1, "cod_tipo_det_ven", ls_cod_tipo_det_ven)
	dw_1.setitem(1, "cod_centro_costo", ls_cod_centro_costo)
	dw_1.setitem(1, "cod_conto", ls_cod_conto)
	
	dw_1.setitem(1, "cod_deposito", ls_cod_deposito)
	dw_1.setitem(1, "cod_valuta", ls_cod_valuta)
	dw_1.setitem(1, "des_prodotto", ls_des_prodotto)
	dw_1.setitem(1, "cod_iva", ls_cod_iva)
end if
end event

event itemchanged;call super::itemchanged;string ls_cod_deposito

if row > 0 then
else
	return
end if

choose case dwo.name
	case "cod_cliente"
		
		if data<>"" and not isnull(data) then
		
			select cod_deposito
			into :ls_cod_deposito
			from anag_clienti
			where cod_azienda=:s_cs_xx.cod_azienda and cod_cliente=:data;
			
			if sqlca.sqlcode<0 then
				g_mb.messagebox("OMNIA","Errore in selezione deposito default del cliente!",Stopsign!)
				return
			end if
			
			if ls_cod_deposito<>"" and not isnull(ls_cod_deposito) then
				setitem(1, "cod_deposito", ls_cod_deposito)
			end if
			
		end if
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_1,"cod_cliente")
end choose
end event

type cb_chiudi from commandbutton within w_call_center_fat_ven
integer x = 1335
integer y = 1232
integer width = 402
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type cb_ok from commandbutton within w_call_center_fat_ven
integer x = 901
integer y = 1232
integer width = 402
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Crea"
end type

event clicked;long ll_num_registrazione,  ll_anno_registrazione
datetime ldt_data_registrazione
string ls_cod_tipo_fat_acq, ls_cod_fornitore, ls_des_num_fat_acq, ls_cod_tipo_fat_ven_det,&
			ls_cod_deposito, ls_cod_valuta, ls_des_prodotto, ls_cod_iva
decimal ld_importo

if g_mb.messagebox("OMNIA","Creare la fattura?", Question!, YesNo!, 1) = 1 then
	
	dw_1.accepttext()
	
	ldt_data_registrazione = dw_1.getitemdatetime(1,"data_fattura")
	ls_cod_tipo_fat_acq = dw_1.getitemstring(1,"cod_tipo_fat_acq")
	ls_cod_fornitore = dw_1.getitemstring(1,"cod_fornitore")
	ls_cod_tipo_fat_ven_det = dw_1.getitemstring(1,"cod_tipo_det_acq")
	ld_importo = dw_1.getitemdecimal(1,"importo")
	ls_des_num_fat_acq = dw_1.getitemstring(1,"num_ordine_acquisto")
	
	ls_cod_deposito = dw_1.getitemstring(1,"cod_deposito")
	ls_cod_valuta = dw_1.getitemstring(1,"cod_valuta")
	ls_des_prodotto = dw_1.getitemstring(1,"des_prodotto")
	ls_cod_iva = dw_1.getitemstring(1,"cod_iva")
	
	//controlla campi obbligatori
	if isnull(ldt_data_registrazione) then
		g_mb.messagebox("Errore","data documento obbligatoria!", Exclamation!)
		return
	end if
	if isnull(ls_cod_tipo_fat_acq) or ls_cod_tipo_fat_acq="" then
		g_mb.messagebox("Errore","Tipo fattura obbligatoria!", Exclamation!)
		return
	end if
	if isnull(ls_cod_tipo_fat_ven_det) or ls_cod_tipo_fat_ven_det="" then
		g_mb.messagebox("Errore","Tipo dettaglio fattura obbligatoria!", Exclamation!)
		return
	end if
	if isnull(ls_cod_fornitore) or ls_cod_fornitore="" then
		g_mb.messagebox("Errore","Fornitore obbligatorio!", Exclamation!)
		return
	end if
	if isnull(ld_importo) or ld_importo<=0 then
		g_mb.messagebox("Errore","Inserire un importo fattura superiore a zero!", Exclamation!)
		return
	end if
	if isnull(ls_des_num_fat_acq) or ls_des_num_fat_acq="" then
		g_mb.messagebox("Errore","N° ordine acquisto obbligatorio!", Exclamation!)
		return
	end if
	
	if isnull(ls_cod_deposito) or ls_cod_deposito="" then
		g_mb.messagebox("Errore","Deposito obbligatorio!", Exclamation!)
		return
	end if
	if isnull(ls_cod_valuta) or ls_cod_valuta="" then
		g_mb.messagebox("Errore","Cod.Valuta obbligatorio!", Exclamation!)
		return
	end if
	if isnull(ls_des_prodotto) or ls_des_prodotto="" then
		g_mb.messagebox("Errore","Des.Prodotto obbligatorio!", Exclamation!)
		return
	end if
	if isnull(ls_cod_iva) or ls_cod_iva="" then
		g_mb.messagebox("Errore","Cod.Iva obbligatoria!", Exclamation!)
		return
	end if
	//------------------------------------------
	
	ll_anno_registrazione = f_anno_esercizio()
	
	select max(num_registrazione)
	into :ll_num_registrazione
	from tes_fat_acq
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_registrazione;
				
	if isnull(ll_num_registrazione) then ll_num_registrazione=0
	ll_num_registrazione += 1
	
	insert into tes_fat_acq
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		data_registrazione,
		cod_tipo_fat_acq,
		cod_fornitore,
		des_num_fat_acq,
		anno_reg_richiesta,
		num_reg_richiesta,
		cod_deposito,
		cod_valuta)
	values
	(	:s_cs_xx.cod_azienda,
		:ll_anno_registrazione,
		:ll_num_registrazione,
		:ldt_data_registrazione,
		:ls_cod_tipo_fat_acq,
		:ls_cod_fornitore,
		:ls_des_num_fat_acq,
		:il_anno_ric,
		:il_num_ric,
		:ls_cod_deposito,
		:ls_cod_valuta);
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore","Errore in inserimento testata fattura acquisto: "+sqlca.sqlerrtext, StopSign!)
		return
	end if
	
	//inserisci dettaglio fattura acquisto
	insert into det_fat_acq
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		prog_riga_fat_acq,
		cod_tipo_det_acq,
		imponibile_iva,
		flag_imponibile_forzato,
		quan_fatturata,
		prezzo_acquisto,
		des_prodotto,
		cod_iva)
	values
	(	:s_cs_xx.cod_azienda,
		:ll_anno_registrazione,
		:ll_num_registrazione,
		1,
		:ls_cod_tipo_fat_ven_det,
		:ld_importo,
		'N',
		1,
		:ld_importo,
		:ls_des_prodotto,
		:ls_cod_iva);
	
	if sqlca.sqlcode < 0 then
		rollback;
		g_mb.messagebox("Errore","Errore in inserimento dettaglio fattura acquisto: "+sqlca.sqlerrtext, StopSign!)
		return
	end if
	
	commit;
	g_mb.messagebox("Errore","Fattura acquisto creata correttamente: "&
				+string(ll_anno_registrazione)+"/"+ string(ll_num_registrazione), StopSign!)
	cb_chiudi.postevent(clicked!)
	
end if
end event

