﻿$PBExportHeader$w_call_center_off_ven.srw
forward
global type w_call_center_off_ven from w_cs_xx_principale
end type
type tab_1 from tab within w_call_center_off_ven
end type
type tabpage_1 from userobject within tab_1
end type
type cb_2 from commandbutton within tabpage_1
end type
type cb_1 from commandbutton within tabpage_1
end type
type dw_off_ven_lista from uo_std_dw within tabpage_1
end type
type tabpage_1 from userobject within tab_1
cb_2 cb_2
cb_1 cb_1
dw_off_ven_lista dw_off_ven_lista
end type
type tabpage_2 from userobject within tab_1
end type
type cb_apri from commandbutton within tabpage_2
end type
type dw_det_off_ven_lista from uo_std_dw within tabpage_2
end type
type tabpage_2 from userobject within tab_1
cb_apri cb_apri
dw_det_off_ven_lista dw_det_off_ven_lista
end type
type tab_1 from tab within w_call_center_off_ven
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
end forward

global type w_call_center_off_ven from w_cs_xx_principale
integer width = 4119
integer height = 1436
string title = "Offerte"
tab_1 tab_1
end type
global w_call_center_off_ven w_call_center_off_ven

type variables
string	is_cod_divisione
long il_anno_richiesta, il_num_richiesta
end variables

on w_call_center_off_ven.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_call_center_off_ven.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;tab_1.tabpage_1.dw_off_ven_lista.SetRowFocusIndicator(Hand!)
tab_1.tabpage_2.dw_det_off_ven_lista.SetRowFocusIndicator(Hand!)

il_anno_richiesta = s_cs_xx.parametri.parametro_d_1
il_num_richiesta = s_cs_xx.parametri.parametro_d_2
is_cod_divisione = s_cs_xx.parametri.parametro_s_1

this.title = g_str.format("Stampa/Gestione Preventivi per la richiesta $1/$2", il_anno_richiesta, il_num_richiesta)

s_cs_xx.parametri.parametro_d_1 = 0
s_cs_xx.parametri.parametro_d_2 = 0
setnull(s_cs_xx.parametri.parametro_s_1)
tab_1.tabpage_1.dw_off_ven_lista.settransobject(sqlca)
tab_1.tabpage_2.dw_det_off_ven_lista.settransobject(sqlca)
tab_1.tabpage_1.dw_off_ven_lista.event post ue_retrieve()

end event

event activate;call super::activate;if s_cs_xx.parametri.parametro_d_1 <> 0 then
	il_anno_richiesta = s_cs_xx.parametri.parametro_d_1
	il_num_richiesta = s_cs_xx.parametri.parametro_d_2
	is_cod_divisione = s_cs_xx.parametri.parametro_s_1
	tab_1.tabpage_1.dw_off_ven_lista.event ue_retrieve()	
	tab_1.tabpage_2.dw_det_off_ven_lista.event ue_retrieve()	
end if

end event

type tab_1 from tab within w_call_center_off_ven
integer width = 4069
integer height = 1320
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

event selectionchanged;choose case newindex
	case 1
		tab_1.tabpage_1.dw_off_ven_lista.event post ue_retrieve()
	case 2
		tab_1.tabpage_2.dw_det_off_ven_lista.event post ue_retrieve()
end choose
end event

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4032
integer height = 1196
long backcolor = 12632256
string text = "Elenco Preventivi del Cliente"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_2 cb_2
cb_1 cb_1
dw_off_ven_lista dw_off_ven_lista
end type

on tabpage_1.create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_off_ven_lista=create dw_off_ven_lista
this.Control[]={this.cb_2,&
this.cb_1,&
this.dw_off_ven_lista}
end on

on tabpage_1.destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_off_ven_lista)
end on

type cb_2 from commandbutton within tabpage_1
integer x = 3182
integer y = 1112
integer width = 402
integer height = 84
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi"
end type

event clicked;integer li_ret
string ls_errore

if tab_1.tabpage_1.dw_off_ven_lista.getrow() > 0 then
	
	if g_mb.messagebox("OMNIA", "Aggiungo la richiesta al preventivo selezionata?", Question!, YesNo!,2) = 1 then
	
		uo_tab_richieste luo_richieste
		luo_richieste = create uo_tab_richieste
		li_ret = luo_richieste.uof_crea_det_off_ven(	tab_1.tabpage_1.dw_off_ven_lista.getitemnumber(tab_1.tabpage_1.dw_off_ven_lista.getrow(),"anno_registrazione"), &
																tab_1.tabpage_1.dw_off_ven_lista.getitemnumber(tab_1.tabpage_1.dw_off_ven_lista.getrow(),"num_registrazione"), &
																il_anno_richiesta, &
																il_num_richiesta, &
																ref ls_errore)
		if li_ret < 0 then
			g_mb.messagebox("OMNIA", ls_errore)
			rollback;
		else
			commit;
		end if
		
		s_cs_xx.parametri.parametro_s_10 = "richiesteM"		
		s_cs_xx.parametri.parametro_d_1  = il_anno_richiesta
		s_cs_xx.parametri.parametro_d_2  = il_num_richiesta
		s_cs_xx.parametri.parametro_d_3  = tab_1.tabpage_1.dw_off_ven_lista.getitemnumber(tab_1.tabpage_1.dw_off_ven_lista.getrow(),"anno_registrazione")
		s_cs_xx.parametri.parametro_d_4  = tab_1.tabpage_1.dw_off_ven_lista.getitemnumber(tab_1.tabpage_1.dw_off_ven_lista.getrow(),"num_registrazione")
		
		if isnull(s_cs_xx.parametri.parametro_d_1) or s_cs_xx.parametri.parametro_d_1 = 0 then return
		if isnull(s_cs_xx.parametri.parametro_d_2) or s_cs_xx.parametri.parametro_d_2 = 0 then return
		if isnull(s_cs_xx.parametri.parametro_d_3) or s_cs_xx.parametri.parametro_d_3 = 0 then return
		if isnull(s_cs_xx.parametri.parametro_d_4) or s_cs_xx.parametri.parametro_d_4 = 0 then return
		
		window_open(w_tes_off_ven, -1)
		
	end if
	
end if
end event

type cb_1 from commandbutton within tabpage_1
integer x = 3593
integer y = 1112
integer width = 411
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long 	 ll_righe_dett


if tab_1.tabpage_1.dw_off_ven_lista.getrow() > 0 then
	
	s_cs_xx.parametri.parametro_d_1 = tab_1.tabpage_1.dw_off_ven_lista.getitemnumber(tab_1.tabpage_1.dw_off_ven_lista.getrow(),"anno_registrazione")
	s_cs_xx.parametri.parametro_d_2 = tab_1.tabpage_1.dw_off_ven_lista.getitemnumber(tab_1.tabpage_1.dw_off_ven_lista.getrow(),"num_registrazione")	
	
	select count(*)
	into   :ll_righe_dett
	from   det_off_ven
	where  cod_azienda        = :s_cs_xx.cod_azienda and
			 anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
			 num_registrazione  = :s_cs_xx.parametri.parametro_d_2;
	
	if ll_righe_dett = 0 or isnull(ll_righe_dett) then
		g_mb.messagebox("Stampa Offerta Vendita","Nessun dettaglio presente in questa offerta: impossibile stampare!",StopSign!)
		return
	end if
	
	window_open(w_report_off_ven_euro,-1)
	
end if
end event

type dw_off_ven_lista from uo_std_dw within tabpage_1
event ue_retrieve ( )
integer x = 27
integer y = 12
integer width = 3977
integer height = 1080
integer taborder = 60
string dataobject = "d_call_center_off_ven_lista"
borderstyle borderstyle = stylebox!
end type

event ue_retrieve();string ls_cod_cliente
long ll_errore


if isnull(is_cod_divisione) then
	g_mb.messagebox("OMNIA","Alla richiesta non è stata associata alcuna commessa/divisione! Impossibile caricare le relative offerte.")
   pcca.error = c_fatal
else
	select cod_cliente
	into   :ls_cod_cliente
	from   anag_divisioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
   	    cod_divisione = :is_cod_divisione;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Cercando il cliente associato alla commessa si è verificato il seguente errore:~r~n"+sqlca.sqlerrtext)
		pcca.error = c_fatal
	else
		ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_cliente)
		if ll_errore < 0 then
			pcca.error = c_fatal
		end if
	end if
end if
end event

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4032
integer height = 1196
long backcolor = 12632256
string text = "Preventivi della Richiesta"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_apri cb_apri
dw_det_off_ven_lista dw_det_off_ven_lista
end type

on tabpage_2.create
this.cb_apri=create cb_apri
this.dw_det_off_ven_lista=create dw_det_off_ven_lista
this.Control[]={this.cb_apri,&
this.dw_det_off_ven_lista}
end on

on tabpage_2.destroy
destroy(this.cb_apri)
destroy(this.dw_det_off_ven_lista)
end on

type cb_apri from commandbutton within tabpage_2
integer x = 3616
integer y = 1112
integer width = 389
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Apri"
end type

event clicked;integer li_ret
string ls_errore

if tab_1.tabpage_2.dw_det_off_ven_lista.getrow() > 0 then
	
	s_cs_xx.parametri.parametro_s_10 = "richiesteC"		
	s_cs_xx.parametri.parametro_d_1  = tab_1.tabpage_2.dw_det_off_ven_lista.getitemnumber(tab_1.tabpage_2.dw_det_off_ven_lista.getrow(),"det_off_ven_anno_registrazione")
	s_cs_xx.parametri.parametro_d_2  = tab_1.tabpage_2.dw_det_off_ven_lista.getitemnumber(tab_1.tabpage_2.dw_det_off_ven_lista.getrow(),"det_off_ven_num_registrazione")
	
	if isnull(s_cs_xx.parametri.parametro_d_1) or s_cs_xx.parametri.parametro_d_1 = 0 then return
	if isnull(s_cs_xx.parametri.parametro_d_2) or s_cs_xx.parametri.parametro_d_2 = 0 then return
	
	window_open(w_tes_off_ven, -1)
	
end if
end event

type dw_det_off_ven_lista from uo_std_dw within tabpage_2
event ue_retrieve ( )
integer x = -18
integer y = -8
integer width = 4023
integer height = 1100
integer taborder = 70
string dataobject = "d_call_center_det_off_ven_lista"
end type

event ue_retrieve();long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, il_anno_richiesta, il_num_richiesta)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

