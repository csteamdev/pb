﻿$PBExportHeader$w_call_center_ole.srw
forward
global type w_call_center_ole from w_ole_v2
end type
end forward

global type w_call_center_ole from w_ole_v2
end type
global w_call_center_ole w_call_center_ole

type variables

end variables

forward prototypes
public subroutine wf_load_documents ()
public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob)
public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob)
public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data)
public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name)
end prototypes

public subroutine wf_load_documents ();string ls_sql
long li_rows, li_i
datastore lds_documenti
str_ole lstr_data

ls_sql = "SELECT &
	tab_richieste_blob.progressivo, &
	tab_richieste_blob.descrizione, &
	tab_richieste_blob.prog_mimetype &
FROM tab_richieste_blob &
WHERE &
	tab_richieste_blob.cod_azienda ='" + s_cs_xx.cod_azienda +"' and &
	tab_richieste_blob.anno_registrazione =" + string(il_anno_registrazione) + " and &
	tab_richieste_blob.num_registrazione =" + string(il_num_registrazione) + " &
ORDER BY &
	tab_richieste_blob.cod_azienda ASC, &
	tab_richieste_blob.anno_registrazione ASC, &
	tab_richieste_blob.num_registrazione ASC, &
	tab_richieste_blob.progressivo ASC "
	
if not f_crea_datastore(ref lds_documenti, ls_sql) then
	return 
end if

li_rows = lds_documenti.retrieve()
for li_i = 1 to li_rows
	lstr_data.anno_registrazione = il_anno_registrazione
	lstr_data.num_registrazione = il_num_registrazione
	lstr_data.progressivo = lds_documenti.getitemnumber(li_i, "progressivo")
	lstr_data.prog_mimetype = lds_documenti.getitemnumber(li_i, "prog_mimetype")
	
	wf_add_document(lds_documenti.getitemstring(li_i, "descrizione"), lstr_data)
next


end subroutine

public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob);long ll_progressivo
str_ole lstr_data

select max(progressivo)
into :ll_progressivo
from tab_richieste_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione;
	
if sqlca.sqlcode < 0 then
	g_mb.error("OMNIA", "Errore durante il calcolo del progressivo del nuovo documento. " + sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) or ll_progressivo = 0 then
	ll_progressivo = 0
end if

ll_progressivo++

insert into tab_richieste_blob (
	cod_azienda,
	anno_registrazione,
	num_registrazione,
	progressivo,
	descrizione,
	prog_mimetype)
values (
	:s_cs_xx.cod_azienda, 
	:il_anno_registrazione,
	:il_num_registrazione,
	:ll_progressivo,
	:as_file_name,
	:al_prog_mimetype);
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Omnia", "Errore durante la creazione del record per il nuovo documento." + sqlca.sqlerrtext)
	return false
end if

updateblob tab_richieste_blob
set blob = :ab_file_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and 
	num_registrazione = :il_num_registrazione and
	progressivo = :ll_progressivo;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Omnia", "Errore durante l'inserimento del documento." + sqlca.sqlerrtext)
	return false
end if

lstr_data.anno_registrazione = il_anno_registrazione
lstr_data.num_registrazione = il_num_registrazione
lstr_data.progressivo = ll_progressivo
lstr_data.prog_mimetype = al_prog_mimetype

wf_add_document(as_file_name, lstr_data)

return true
			  
end function

public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob);selectblob blob
into :ab_file_blob
from tab_richieste_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	progressivo = :astr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante il prelievo del file." + sqlca.sqlerrtext)
	return false
end if
end function

public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data);delete from tab_richieste_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	progressivo = :astr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la cancellazione del documento " + alv_item.label +". " + sqlca.sqlerrtext)
	return false
else
	return true
end if
end function

public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name);update tab_richieste_blob
set descrizione = :as_new_name
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :astr_data.anno_registrazione and
	num_registrazione = :astr_data.num_registrazione and
	progressivo = :astr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	return false
else
	return true
end if
end function

on w_call_center_ole.create
call super::create
end on

on w_call_center_ole.destroy
call super::destroy
end on

event open;call super::open;il_anno_registrazione = s_cs_xx.parametri.parametro_ul_1
il_num_registrazione = s_cs_xx.parametri.parametro_ul_2
end event

type st_loading from w_ole_v2`st_loading within w_call_center_ole
end type

type cb_cancella from w_ole_v2`cb_cancella within w_call_center_ole
end type

type lv_documenti from w_ole_v2`lv_documenti within w_call_center_ole
end type

type ddlb_style from w_ole_v2`ddlb_style within w_call_center_ole
end type

type cb_sfoglia from w_ole_v2`cb_sfoglia within w_call_center_ole
end type

