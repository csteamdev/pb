﻿$PBExportHeader$w_call_center_sospensioni.srw
forward
global type w_call_center_sospensioni from w_cs_xx_principale
end type
type dw_dettaglio from uo_cs_xx_dw within w_call_center_sospensioni
end type
type dw_lista from uo_cs_xx_dw within w_call_center_sospensioni
end type
end forward

global type w_call_center_sospensioni from w_cs_xx_principale
integer width = 2939
integer height = 2352
string title = "Sospensioni Richiesta"
dw_dettaglio dw_dettaglio
dw_lista dw_lista
end type
global w_call_center_sospensioni w_call_center_sospensioni

type variables


integer			ii_anno_reg_richiesta

long				il_num_reg_richiesta

string			is_flag_richiesta_chiusa

datetime		idt_data_registrazione, idt_ora_registrazione
end variables

forward prototypes
public subroutine wf_proteggi_campi (boolean ab_proteggi)
end prototypes

public subroutine wf_proteggi_campi (boolean ab_proteggi);
string		ls_protect

ls_protect = "0"
if ab_proteggi then ls_protect = "1"


dw_dettaglio.modify("data_sospensione.protect='"+ls_protect+"'")
dw_dettaglio.modify("ora_sospensione.protect='"+ls_protect+"'")
dw_dettaglio.modify("note_sospensione.protect='"+ls_protect+"'")

dw_dettaglio.modify("data_riattivazione.protect='"+ls_protect+"'")
dw_dettaglio.modify("ora_riattivazione.protect='"+ls_protect+"'")
dw_dettaglio.modify("note_riattivazione.protect='"+ls_protect+"'")


return
end subroutine

on w_call_center_sospensioni.create
int iCurrent
call super::create
this.dw_dettaglio=create dw_dettaglio
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_dettaglio
this.Control[iCurrent+2]=this.dw_lista
end on

on w_call_center_sospensioni.destroy
call super::destroy
destroy(this.dw_dettaglio)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag
windowobject lw_oggetti[], lw_vuoto[]

dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("anno_registrazione")
dw_lista.set_dw_key("num_registrazione")

dw_lista.set_dw_options(	sqlca, &
										i_openparm, &
										c_scrollparent, &
										c_default + &
										c_nohighlightselected + c_ViewModeBorderUnchanged + c_CursorRowPointer)
										
dw_dettaglio.set_dw_options(		sqlca, &
												dw_lista, &
												c_sharedata + c_scrollparent, &
												c_default)

iuo_dw_main = dw_lista

end event

type dw_dettaglio from uo_cs_xx_dw within w_call_center_sospensioni
integer x = 32
integer y = 728
integer width = 2830
integer height = 1488
integer taborder = 10
string dataobject = "d_call_center_sospensioni_1"
borderstyle borderstyle = styleraised!
long il_limit_campo_note = 1999
end type

event buttonclicked;call super::buttonclicked;string		ls_null
datetime	ldt_null


if row>0 then
else
	return
end if

setnull(ldt_null)
setnull(ls_null)

choose case dwo.name
		
	case "b_ripulisci"
		if not g_mb.confirm("Ripulire i campi della riattivazione? (Dopo il salvataggio la richiesta tornerà in Sospensione!)") then
			return
		end if
		
		setitem(row, "utente_riattivazione", ls_null)
		setitem(row, "data_riattivazione", ldt_null)
		setitem(row, "ora_riattivazione", ldt_null)
		setitem(row, "note_riattivazione", ls_null)
		
end choose
end event

event itemchanged;call super::itemchanged;datetime			ldt_data
time					ldt_time_sospensione, ldt_time_registrazione

if row>0 then
else
	return
end if


choose case dwo.name
		
	case "data_sospensione"
		if data<>"" then
			if date(data) < date(idt_data_registrazione) then
				g_mb.warning("Attenzione: la data sospensione risulta antecedente alla data registrazione della richiesta!")
				return
				
			elseif date(data) = date(idt_data_registrazione) then
				
				//ignoro il campo secondi per il confronto
				ldt_time_sospensione = time(getitemdatetime(row, "ora_sospensione"))
				ldt_time_sospensione = time(hour(ldt_time_sospensione), minute(ldt_time_sospensione), 0)
				
				ldt_time_registrazione = time(idt_ora_registrazione)
				ldt_time_registrazione = time(hour(ldt_time_registrazione), minute(ldt_time_registrazione), 0)
				//-----------------------------------------------
				
				if not isnull(ldt_time_sospensione) and 	ldt_time_sospensione< ldt_time_registrazione then
					
					g_mb.warning("Attenzione: la data sospensione (parte ora:minuti) risulta antecedente alla data registrazione della richiesta!")
					return
					
				end if
				
			end if
		end if
	
	case "ora_sospensione"
		if data<>"" then
			
			ldt_data = getitemdatetime(row, "data_sospensione")
			
			//ignoro il campo secondi per il confronto
			ldt_time_sospensione = time(datetime(data))
			ldt_time_sospensione = time(hour(ldt_time_sospensione), minute(ldt_time_sospensione), 0)
			
			ldt_time_registrazione = time(idt_ora_registrazione)
			ldt_time_registrazione = time(hour(ldt_time_registrazione), minute(ldt_time_registrazione), 0)
			//-----------------------------------------------
			
			if date(ldt_data)=date(idt_data_registrazione) and ldt_time_sospensione<ldt_time_registrazione then
				
				g_mb.warning("Attenzione: la data sospensione (parte ora:minuti) risulta antecedente alla data registrazione della richiesta!")
				return
				
			end if
		end if
		
end choose
end event

type dw_lista from uo_cs_xx_dw within w_call_center_sospensioni
integer x = 37
integer y = 20
integer width = 2830
integer height = 684
integer taborder = 10
string dataobject = "d_call_center_sospensioni_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
long il_limit_campo_note = 1999
end type

event pcd_retrieve;call super::pcd_retrieve;
long			ll_errore
boolean		lb_proteggi = false


ii_anno_reg_richiesta = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
il_num_reg_richiesta = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
is_flag_richiesta_chiusa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "flag_richiesta_chiusa")

idt_data_registrazione = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_registrazione")
idt_ora_registrazione = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "ora_registrazione")

//if is_flag_richiesta_chiusa="S" then lb_proteggi = true

parent.title = "Sospensioni effettuate o in corso per la Richiesta n° " + string(ii_anno_reg_richiesta) + "/" + string(il_num_reg_richiesta)

ll_errore = dw_lista.retrieve(s_cs_xx.cod_azienda, ii_anno_reg_richiesta, il_num_reg_richiesta)

if ll_errore < 0 then
   pcca.error = c_fatal
	return
end if

wf_proteggi_campi(lb_proteggi)

end event

event pcd_setkey;call super::pcd_setkey;long			ll_index, ll_progressivo



for ll_index = 1 to rowcount()
	
	if isnull(getitemstring(ll_index, "cod_azienda")) or getitemstring(ll_index, "cod_azienda")="" then
		setitem(ll_index, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemnumber(ll_index, "anno_registrazione")) or getitemnumber(ll_index, "anno_registrazione") = 0 then
		setitem(ll_index, "anno_registrazione", ii_anno_reg_richiesta)
	end if		
	
	if isnull(getitemnumber(ll_index, "num_registrazione")) or getitemnumber(ll_index, "num_registrazione") = 0 then
		setitem(ll_index, "num_registrazione", il_num_reg_richiesta)
	end if
	
	if isnull(getitemnumber(ll_index, "progressivo")) or getitemnumber(ll_index, "progressivo") = 0 then
	
		select max(progressivo)
		into   :ll_progressivo
		from   tab_richieste_sospensioni
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 anno_registrazione = :ii_anno_reg_richiesta and
				 num_registrazione=:il_num_reg_richiesta;
				 
		if isnull(ll_progressivo) then ll_progressivo = 0

		ll_progressivo += 1
		
		setitem(ll_index, "progressivo", ll_progressivo)
		
	end if
	
	//se il campo utente sospensione è vuoto, qui lo valorizzo
	if isnull(getitemstring(ll_index, "utente_sospensione")) or getitemstring(ll_index, "utente_sospensione")="" then
		setitem(ll_index, "utente_sospensione", s_cs_xx.cod_utente)
	end if
	
	
next
end event

event updatestart;call super::updatestart;
datetime		ldt_data_riattivazione, ldt_ora_riattivazione, ldt_data_sospensione, ldt_ora_sospensione

time				ldt_time_sospensione, ldt_time_riattivazione

long				ll_index

string			ls_null



setnull(ls_null)

for ll_index = 1 to this.rowcount()

	if dw_lista.getitemnumber(ll_index, "progressivo")=0 or isnull(dw_lista.getitemnumber(ll_index, "progressivo")) then continue

	ldt_data_sospensione = dw_lista.getitemdatetime(ll_index, "data_sospensione")
	ldt_ora_sospensione = dw_lista.getitemdatetime(ll_index, "data_sospensione")
	
	if isnull(ldt_data_sospensione) or year(date(ldt_data_sospensione))<1950 or isnull(ldt_ora_sospensione) then
		g_mb.warning("La data/ora sospensione deve essere valida o non nulla! (riga "+string(ll_index)+")")
		return 1
	end if
	
	
	//se hai valorizzato la data riattivazione, controlla di aver inserito anche l'ora, e viceversa
	if 	not isnull(dw_lista.getitemdatetime(ll_index, "data_riattivazione")) and &
		year(date(dw_lista.getitemdatetime(ll_index, "data_riattivazione"))) >1950 and &
		isnull(dw_lista.getitemdatetime(ll_index, "ora_riattivazione"))			then
		
		g_mb.warning("Manca l'ora della riattivazione! (riga "+string(ll_index)+")")
		return 1
		
	end if
	if 	not isnull(dw_lista.getitemdatetime(ll_index, "ora_riattivazione")) and &
		(	year(date(dw_lista.getitemdatetime(ll_index, "data_riattivazione"))) <1950 or &
			isnull(dw_lista.getitemdatetime(ll_index, "data_riattivazione")))		then
			
			g_mb.warning("Manca la data della riattivazione! (riga "+string(ll_index)+")")
			return 1
		
	end if
	//--------------------------------------------------------------------------------------------------------------
	
	
	//se i campi data/ora riattivazione sono stati soggetti a modifica:
	//		se sono non nulli, registra l'utente riattivazione
	//		altrimenti ripulisci il campo utente riattivazione
	if 	getitemstatus(ll_index, "data_riattivazione", Primary!) = DataModified! or &
		getitemstatus(ll_index, "data_riattivazione", Primary!) = NewModified! or &
		getitemstatus(ll_index, "ora_riattivazione", Primary!) = DataModified! or &
		getitemstatus(ll_index, "ora_riattivazione", Primary!) = NewModified! then
		
		if (isnull(dw_lista.getitemdatetime(ll_index, "data_riattivazione")) or &
				year(date(dw_lista.getitemdatetime(ll_index, "data_riattivazione"))) <1950) and &
				isnull(dw_lista.getitemdatetime(ll_index, "ora_riattivazione")) then
				
			//hai ripulito i campi
			setitem(ll_index, "utente_riattivazione", ls_null)
			setitem(ll_index, "note_riattivazione", ls_null)
		else
			//campi riattivazione valorizzati
			
			//controlla che la data riattivazione sia superiore a quella di sospensione
			
			//ignoro il campo secondi per il confronto
			ldt_time_sospensione = time(getitemdatetime(ll_index, "ora_sospensione"))
			ldt_time_sospensione = time(hour(ldt_time_sospensione), minute(ldt_time_sospensione), 0)
			
			ldt_time_riattivazione = time(dw_lista.getitemdatetime(ll_index, "ora_riattivazione"))
			ldt_time_riattivazione = time(hour(ldt_time_riattivazione), minute(ldt_time_riattivazione), 0)
			//-----------------------------------------------
			
			if date(dw_lista.getitemdatetime(ll_index, "data_riattivazione"))<date(dw_lista.getitemdatetime(ll_index, "data_sospensione")) or &
			   		(date(dw_lista.getitemdatetime(ll_index, "data_riattivazione"))=date(dw_lista.getitemdatetime(ll_index, "data_sospensione")) and &
						ldt_time_riattivazione<ldt_time_sospensione) then
				
				g_mb.warning("La data della riattivazione NON può essere inferiore a quella di sospensione! (riga "+string(ll_index)+")")
				return 1
				
			end if
			
			//predisponi l'utente riattivazione (ovvero ultima modifica riattivazione)
			setitem(ll_index, "utente_riattivazione", s_cs_xx.cod_utente)
		end if
		
	end if
	
	

next
end event

event pcd_new;call super::pcd_new;

dw_dettaglio.object.b_ripulisci.enabled = true
end event

event pcd_modify;call super::pcd_modify;

dw_dettaglio.object.b_ripulisci.enabled = true
end event

event pcd_view;call super::pcd_view;

dw_dettaglio.object.b_ripulisci.enabled = false
end event

