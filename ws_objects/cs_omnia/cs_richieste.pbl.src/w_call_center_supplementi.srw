﻿$PBExportHeader$w_call_center_supplementi.srw
forward
global type w_call_center_supplementi from w_cs_xx_principale
end type
type em_qta_richiesta from editmask within w_call_center_supplementi
end type
type em_qta_servizio from editmask within w_call_center_supplementi
end type
type st_3 from statictext within w_call_center_supplementi
end type
type st_2 from statictext within w_call_center_supplementi
end type
type st_1 from statictext within w_call_center_supplementi
end type
type mle_des_richiesta from multilineedit within w_call_center_supplementi
end type
type dw_richieste_supplementi_lista from uo_cs_xx_dw within w_call_center_supplementi
end type
end forward

global type w_call_center_supplementi from w_cs_xx_principale
integer width = 3643
integer height = 1696
string title = "Servizi Aggiuntivi Richiesta n°"
em_qta_richiesta em_qta_richiesta
em_qta_servizio em_qta_servizio
st_3 st_3
st_2 st_2
st_1 st_1
mle_des_richiesta mle_des_richiesta
dw_richieste_supplementi_lista dw_richieste_supplementi_lista
end type
global w_call_center_supplementi w_call_center_supplementi

type variables
integer ii_anno_reg_richiesta
long il_num_reg_richiesta
string is_cod_tipo_richiesta
end variables

on w_call_center_supplementi.create
int iCurrent
call super::create
this.em_qta_richiesta=create em_qta_richiesta
this.em_qta_servizio=create em_qta_servizio
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.mle_des_richiesta=create mle_des_richiesta
this.dw_richieste_supplementi_lista=create dw_richieste_supplementi_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_qta_richiesta
this.Control[iCurrent+2]=this.em_qta_servizio
this.Control[iCurrent+3]=this.st_3
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.mle_des_richiesta
this.Control[iCurrent+7]=this.dw_richieste_supplementi_lista
end on

on w_call_center_supplementi.destroy
call super::destroy
destroy(this.em_qta_richiesta)
destroy(this.em_qta_servizio)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.mle_des_richiesta)
destroy(this.dw_richieste_supplementi_lista)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag
windowobject lw_oggetti[]

dw_richieste_supplementi_lista.set_dw_key("cod_azienda")
dw_richieste_supplementi_lista.set_dw_key("anno_registrazione")
dw_richieste_supplementi_lista.set_dw_key("num_registrazione")

dw_richieste_supplementi_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default)

iuo_dw_main = dw_richieste_supplementi_lista
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(	dw_richieste_supplementi_lista, &
                 				"cod_listino", &
                 				sqlca, &
                 				"tab_tipi_richieste_listini", &
                 				"cod_listino", &
                				 "des_listino", &
                 				"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
end event

type em_qta_richiesta from editmask within w_call_center_supplementi
integer x = 2729
integer y = 152
integer width = 489
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "0"
alignment alignment = center!
boolean displayonly = true
string mask = "###,##0.00"
end type

type em_qta_servizio from editmask within w_call_center_supplementi
integer x = 2729
integer y = 32
integer width = 489
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "0"
alignment alignment = center!
boolean displayonly = true
string mask = "###,##0.00"
end type

type st_3 from statictext within w_call_center_supplementi
integer x = 2258
integer y = 160
integer width = 457
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Q.tà Richiesta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_call_center_supplementi
integer x = 2299
integer y = 32
integer width = 416
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Q.tà Servizio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_call_center_supplementi
integer x = 27
integer y = 32
integer width = 425
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Des.Richiesta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type mle_des_richiesta from multilineedit within w_call_center_supplementi
integer x = 471
integer y = 32
integer width = 1760
integer height = 392
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
boolean vscrollbar = true
boolean displayonly = true
end type

type dw_richieste_supplementi_lista from uo_cs_xx_dw within w_call_center_supplementi
integer x = 27
integer y = 444
integer width = 3566
integer height = 1136
integer taborder = 10
string dataobject = "d_call_center_supplementi_lista"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;
long ll_errore
string ls_des_richiesta
decimal ld_qta_servizio, ld_qta_richiesta

ii_anno_reg_richiesta = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
il_num_reg_richiesta = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
is_cod_tipo_richiesta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_richiesta")
ls_des_richiesta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "des_richiesta")
ld_qta_servizio = i_parentdw.getitemdecimal(i_parentdw.i_selectedrows[1], "quota_servizio")
ld_qta_richiesta = i_parentdw.getitemdecimal(i_parentdw.i_selectedrows[1], "quota_richiesta")

if isnull(ld_qta_servizio) then ld_qta_servizio = 0
if isnull(ld_qta_richiesta) then ld_qta_richiesta = 0

parent.title = "Servizi Aggiuntivi Richiesta n° " + string(ii_anno_reg_richiesta) + "/" + string(il_num_reg_richiesta)
mle_des_richiesta.text = ls_des_richiesta
em_qta_servizio.text = string(ld_qta_servizio, "###,###,##0.00")
em_qta_richiesta.text = string(ld_qta_richiesta, "###,###,##0.00")


ll_errore = dw_richieste_supplementi_lista.retrieve(s_cs_xx.cod_azienda, ii_anno_reg_richiesta, il_num_reg_richiesta)
if ll_errore < 0 then
   pcca.error = c_fatal
	return
end if

f_po_loaddddw_dw(	dw_richieste_supplementi_lista, &
                 				"cod_listino", &
                 				sqlca, &
                 				"tab_tipi_richieste_listini", &
                 				"cod_listino", &
                				 "des_listino", &
                 				"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_richiesta='"+is_cod_tipo_richiesta+"'")
end event

event itemchanged;call super::itemchanged;decimal ld_prezzo_unitario

if row>0 then
else
	return
end if

choose case dwo.name
	case "cod_listino"
		if data="" then
			setitem(row, "prezzo_unitario", 0)
		else
			select prezzo_unitario
			into :ld_prezzo_unitario
			from tab_tipi_richieste_listini
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						cod_listino=:data;
			
			if sqlca.sqlcode<0 then
				g_mb.error("Errore in lettura prezzo unitario servizio aggiuntivo!")
				return
			end if
			
			setitem(row, "prezzo_unitario", ld_prezzo_unitario)
			
		end if
end choose
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(getitemnumber(ll_i, "anno_registrazione")) or getitemnumber(ll_i, "anno_registrazione") <= 0 then
      setitem(ll_i, "anno_registrazione", ii_anno_reg_richiesta)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or this.getitemnumber(ll_i, "num_registrazione") <= 0 then
      setitem(ll_i, "num_registrazione", il_num_reg_richiesta)
   end if
next

end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_tipo_richiesta, ls_cod_listino, ls_des_listino
decimal ld_prezzo_unitario
long ll_row

ll_row = getrow()

if ll_row>0 then
	//------------------------------------------------------------------------------------------------------------
	ls_cod_listino = getitemstring(ll_row, "cod_listino")
	if isnull(ls_cod_listino) or ls_cod_listino="" then
		g_mb.messagebox("OMNIA","Codice listino obbligatorio!")
		setcolumn("cod_listino")
		pcca.error = c_fatal
		return
	end if
	
	//------------------------------------------------------------------------------------------------------------
	ld_prezzo_unitario = getitemdecimal(ll_row, "prezzo_unitario")
	if isnull(ld_prezzo_unitario) or ld_prezzo_unitario<=0 then
		g_mb.messagebox("OMNIA","Prezzo Unitario obbligatorio!")
		setcolumn("prezzo_unitario")
		pcca.error = c_fatal
		return
	end if
	
end if
end event

