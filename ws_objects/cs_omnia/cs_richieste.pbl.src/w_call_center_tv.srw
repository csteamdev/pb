﻿$PBExportHeader$w_call_center_tv.srw
$PBExportComments$Finestra Gestione Richieste
forward
global type w_call_center_tv from w_cs_xx_treeview
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_2 dw_2
end type
type det_3 from userobject within tab_dettaglio
end type
type dw_3 from uo_cs_xx_dw within det_3
end type
type det_3 from userobject within tab_dettaglio
dw_3 dw_3
end type
type det_4 from userobject within tab_dettaglio
end type
type dw_4 from uo_cs_xx_dw within det_4
end type
type det_4 from userobject within tab_dettaglio
dw_4 dw_4
end type
type det_5 from userobject within tab_dettaglio
end type
type dw_5 from uo_cs_xx_dw within det_5
end type
type det_5 from userobject within tab_dettaglio
dw_5 dw_5
end type
type documenti from userobject within tab_ricerca
end type
type dw_documenti from uo_dw_drag within documenti
end type
type documenti from userobject within tab_ricerca
dw_documenti dw_documenti
end type
end forward

global type w_call_center_tv from w_cs_xx_treeview
integer width = 5221
integer height = 2776
string title = "Richieste"
event ue_barcode_focus ( )
event ue_documenti ( )
event ue_genera_nc ( )
event ue_ol_richiesta ( )
event ue_supplementi ( )
event ue_dati_manutenzione ( )
event ue_sospensioni ( )
event ue_calcola ( )
end type
global w_call_center_tv w_call_center_tv

type variables


private:
	uo_tab_richieste 	iuo_richieste
	long					il_livello, il_row = 0, il_anno_nc=0, il_num_nc=0, il_max_richieste = 150
	string					is_NMS = "N", is_sql_base
							
	transaction			itran_note
	boolean				ib_global_service=false, ib_proviene_da_nc=false, ib_send_mail=false, ib_ol_visibile= false, ib_supplementi_visibile = false, ib_paramRIC=false
	datastore ids_store
	
	string					is_etichetta_divisione="Divisione", is_etichetta_area_az="Area Aziendale", is_etichette_reparto="Reparto"

	
	// icone  ------------------------------------------------------------------------------
	int 		ICONA_RICHIESTA, ICONA_RICHIESTA_INIZIATA, ICONA_RICHIESTA_CHIUSA, &
				ICONA_RICHIESTA_SEL, ICONA_RICHIESTA_INIZIATA_SEL, ICONA_RICHIESTA_CHIUSA_SEL, &
				ICONA_OPERATORE, ICONA_ANNO, &
				ICONA_DIVISIONE, ICONA_UBICAZIONE, ICONA_ATTREZZATURA, ICONA_TIPO_RICHIESTA
	//--------------------------------------------------------------------------------------
	
	boolean			ib_select = true, ib_nuovo = false, ib_modifica=false, ib_visualizza=true
end variables

forward prototypes
public function integer wf_imposta_folder ()
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public subroutine wf_imposta_ricerca ()
public function boolean wf_get_barcode (string as_barcode, ref integer ai_anno_registrazione, ref long al_num_registrazione, ref string as_errore)
public function integer wf_inserisci_richieste (long al_handle)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public subroutine wf_treeview_icons ()
public subroutine wf_valori_livelli ()
public subroutine wf_retrieve_blob (long al_row)
public function integer wf_blocca_folder (string fs_cod_tipo_richiesta)
public function integer wf_invia_mail (string as_oggetto, string as_messaggio)
public subroutine wf_abilita_pulsanti (boolean ab_enabled)
public function integer wf_inserisci_anno (long al_handle)
public function integer wf_inserisci_ubicazioni (long al_handle)
public function integer wf_inserisci_commesse (long al_handle)
public function integer wf_inserisci_attrezzature (long al_handle)
public function integer wf_inserisci_tipi_richieste (long al_handle)
public function integer wf_inserisci_operatori_reg (long al_handle)
public function integer wf_inserisci_operatori_ris (long al_handle)
public function integer wf_inserisci_operatori_inc (long al_handle)
public function integer wf_inserisci_risorse_esterne (long al_handle)
public function long wf_refresh_richiesta (long al_handle, long al_anno_registrazione, long al_num_registrazione, datetime adt_data_registrazione, integer ai_anno_documento, string as_cod_documento, string as_numeratore_documento, long al_num_documento, string as_flag_richiesta_chiusa)
public subroutine wf_aggiorna_risposta (ref uo_cs_xx_dw fdw_datawindow)
public subroutine wf_imposta_tipo_richiesta_predefinita (long al_row, string as_dataobject)
public function long wf_inserisci_singola_richiesta (long al_handle, long al_anno_registrazione, long al_num_registrazione, datetime adt_data_registrazione, integer ai_anno_documento, string as_cod_documento, string as_numeratore_documento, long al_num_documento, string as_flag_richiesta_chiusa, string as_cod_divisione, string as_des_richiesta)
end prototypes

event ue_barcode_focus();// imposto fuoco sul campo barcode
tab_ricerca.ricerca.dw_ricerca.setfocus()
tab_ricerca.ricerca.dw_ricerca.setcolumn("barcode")
end event

event ue_documenti();long ll_row, ll_num_registrazione
integer li_anno_registrazione

ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row>0 then
	
	li_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
	ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")
	
	if ll_num_registrazione>0 then
		s_cs_xx.parametri.parametro_ul_1 = li_anno_registrazione
		s_cs_xx.parametri.parametro_ul_2 = ll_num_registrazione
	
		open(w_call_center_ole)
		
	end if
end if
end event

event ue_genera_nc();string			ls_cod_prodotto, ls_cod_operaio,ls_cod_cliente, ls_des_anomalia_1, ls_flag_prod_codificato
long			ll_anno_registrazione, ll_num_registrazione,ll_anno_richiesta, ll_num_richiesta, ll_row
datetime		ldt_data_registrazione

ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row < 1 then return
if tab_dettaglio.det_1.dw_1.getitemnumber(ll_row,"anno_non_conformita") > 0 or not isnull(tab_dettaglio.det_1.dw_1.getitemnumber(ll_row,"anno_non_conformita")) then
	g_mb.warning("OMNIA","E' già stata generata una NC per questa anomalia")
	return
end if

ll_anno_richiesta = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row,"anno_registrazione")
ll_num_richiesta  = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row,"num_registrazione")

setnull(ll_num_registrazione)

ll_anno_registrazione = f_anno_esercizio()

select max(num_non_conf)
into   :ll_num_registrazione
from   non_conformita
where  cod_azienda =:s_cs_xx.cod_azienda and
       anno_non_conf = :ll_anno_registrazione;
if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

ls_cod_prodotto = tab_dettaglio.det_1.dw_1.getitemstring(ll_row,"cod_prodotto")
ls_cod_operaio = tab_dettaglio.det_1.dw_1.getitemstring(ll_row,"cod_operaio_registra")
ldt_data_registrazione = datetime(today(), 00:00:00)
ls_cod_cliente = tab_dettaglio.det_1.dw_1.getitemstring(ll_row,"cod_cliente")
ls_des_anomalia_1 = tab_dettaglio.det_1.dw_1.getitemstring(ll_row,"des_anomalia_1")
if isnull(ls_cod_prodotto) then
	ls_flag_prod_codificato = "N"
else
	ls_flag_prod_codificato = "S"
end if

insert into non_conformita
		(cod_azienda,
		anno_non_conf,
		num_non_conf,
		flag_tipo_nc,
		cod_operaio,
		data_creazione,
		data_scadenza,
		cod_cliente,
		flag_prod_codificato,
		cod_prodotto)
values
		(:s_cs_xx.cod_azienda,
		:ll_anno_registrazione,
		:ll_num_registrazione,
		'V',
		:ls_cod_operaio,
		:ldt_data_registrazione,
		null,
		:ls_cod_cliente,
		:ls_flag_prod_codificato,
		:ls_cod_prodotto);
if sqlca.sqlcode <> 0  then
	g_mb.messagebox("OMNIA","Errore in fase di creazione N.C. Dettaglio errore: " + sqlca.sqlerrtext)
	return
	rollback;
end if

update tab_richieste
set    anno_non_conf = :ll_anno_registrazione, 
       num_non_conf = :ll_num_registrazione
where cod_azienda   = :s_cs_xx.cod_azienda and
      anno_registrazione = :ll_anno_richiesta and
		num_registrazione  = :ll_num_richiesta;
if sqlca.sqlcode <> 0  then
	g_mb.messagebox("OMNIA","Errore in fase di aggiornamento richiesta selezionata con il numero della N.C. generata. Dettaglio errore: " + sqlca.sqlerrtext)
	return
	rollback;
end if

commit;

g_mb.show("OMNIA","E' stata generata con successo la Non Conformità " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))

return

end event

event ue_ol_richiesta();string		ls_cod_tipo_richiesta, ls_flag_tipo_report
decimal 	ld_null


if not ib_global_service then return

if tab_dettaglio.det_1.dw_1.rowcount()>0 then
	setnull( ld_null )
	
	setnull(s_cs_xx.parametri.parametro_s_10)
	s_cs_xx.parametri.parametro_d_1_a[1] = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_registrazione")
	s_cs_xx.parametri.parametro_d_1_a[2] = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_registrazione")
	ls_cod_tipo_richiesta = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "cod_tipo_richiesta")
	
	if s_cs_xx.parametri.parametro_d_1_a[1]>0 and s_cs_xx.parametri.parametro_d_1_a[2] > 0 then
		
		setnull(ls_flag_tipo_report)
		if not isnull(ls_cod_tipo_richiesta) then
			select flag_tipo_report
			into	:ls_flag_tipo_report
			from	tab_tipi_richieste
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_richiesta = :ls_cod_tipo_richiesta;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_flag_tipo_report)
			end if
		end if	
		
		if isnull(ls_flag_tipo_report) or ls_flag_tipo_report = "N" then
		
			//apri una window (è nel global service) che consente all'utente di scegliere il tipo report
			s_cs_xx.parametri.parametro_s_1_a[1] = "G"
			s_cs_xx.parametri.parametro_s_2_a[1] = "Generico"
			s_cs_xx.parametri.parametro_s_1_a[2] = "T"
			s_cs_xx.parametri.parametro_s_2_a[2] = "Modello per Richieste Telefoniche"
			s_cs_xx.parametri.parametro_s_1_a[3] = "S"
			s_cs_xx.parametri.parametro_s_2_a[3] = "Modello per Richieste Scritte"
			s_cs_xx.parametri.parametro_s_1_a[4] = "R"
			s_cs_xx.parametri.parametro_s_2_a[4] = "Preparazione Impianti"
			s_cs_xx.parametri.parametro_s_1_a[5] = "U"
			s_cs_xx.parametri.parametro_s_2_a[5] = "Strumentale"
			s_cs_xx.parametri.parametro_s_1_a[6] = "Q"
			s_cs_xx.parametri.parametro_s_2_a[6] = "Preparaz. Impianti con QRcode (necessaria connessione internet!)"
			s_cs_xx.parametri.parametro_s_1_a[7] = "D"
			s_cs_xx.parametri.parametro_s_2_a[7] = "Strumentale DUVRI"
			
			window_type_open(w_cs_xx_risposta, "w_sel_tipo_report_ol_ric", 0 )
			
			s_cs_xx.parametri.parametro_s_10 = message.stringparm
		else
			s_cs_xx.parametri.parametro_s_10 = ls_flag_tipo_report
		end if
		
		if isnull(s_cs_xx.parametri.parametro_s_10) or s_cs_xx.parametri.parametro_s_10="" then
			s_cs_xx.parametri.parametro_s_10 = "G"
		end if
		
		//layout report attualmente gestiti
		//	1	report generico 														(G)
		//	2	rich. telefoniche														(T)
		//	3	rich. scritte																(S)
		//	4	preparazione impianti (VERONA FIERA)							(R)
		//	5	strumentale (attrezzatura)											(U)
		//	6	preparazione impianti con qrcode (VERONA FIERA)			(Q)
		//	6	strumentale DUVRI					 (VERONA FIERA)			(D)
		
		//questo report si trova nel global service
		window_type_open(w_cs_xx_risposta, "w_report_ordine_lavoro_richieste", -1 )

	else
		setnull(s_cs_xx.parametri.parametro_s_10)
		s_cs_xx.parametri.parametro_d_1_a[1] = ld_null
		s_cs_xx.parametri.parametro_d_1_a[2] = ld_null
	end if

end if







end event

event ue_supplementi();integer			li_anno_reg_richiesta
long				ll_num_reg_richiesta, ll_row

if not ib_global_service then return

ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row>0 then
	li_anno_reg_richiesta = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
	ll_num_reg_richiesta = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")
	
	if li_anno_reg_richiesta>0 and ll_num_reg_richiesta>0 then
	else
		return
	end if
	
	window_open_parm(w_call_center_supplementi, -1, tab_dettaglio.det_1.dw_1)
	
end if



end event

event ue_dati_manutenzione();long			ll_row, ll_num_reg_richiesta
integer		li_anno_reg_richiesta


ll_row = tab_dettaglio.det_1.dw_1.getrow()
if ll_row>0 then
	li_anno_reg_richiesta = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
	ll_num_reg_richiesta = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")
	
	if li_anno_reg_richiesta>0 and ll_num_reg_richiesta>0 then
	else
		return
	end if
	
	s_cs_xx.parametri.parametro_d_1 = li_anno_reg_richiesta
	s_cs_xx.parametri.parametro_d_2  = ll_num_reg_richiesta
	
	window_open(w_call_center_elenco_man, -1)
	
end if
end event

event ue_sospensioni();
integer			li_anno_reg_richiesta
long				ll_num_reg_richiesta, ll_row
string			ls_flag_richiesta_chiusa


if not ib_global_service then return

if ib_nuovo or ib_modifica then
	g_mb.warning("Per questa azione il dato NON deve essere in modalità NUOVO o MODIFICA!")
	return
end if

ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row>0 then
	li_anno_reg_richiesta = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
	ll_num_reg_richiesta = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")
	
	if li_anno_reg_richiesta>0 and ll_num_reg_richiesta>0 then
	else
		return
	end if
	
	//s_cs_xx.parametri.
	
	window_open_parm(w_call_center_sospensioni, -1, tab_dettaglio.det_1.dw_1)
	
end if


return
end event

event ue_calcola();integer		li_anno_richiesta

long			ll_num_richiesta, ll_row

string		ls_errore


if not ib_visualizza then return

ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row>0 then
	
	li_anno_richiesta = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
	ll_num_richiesta = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")
	
	if ll_num_richiesta>0 and li_anno_richiesta>0 then
		f_tempi_richiesta(li_anno_richiesta, ll_num_richiesta, ls_errore)
		
		commit;
		tab_dettaglio.det_1.dw_1.retrieve(s_cs_xx.cod_azienda, li_anno_richiesta, ll_num_richiesta)
		
	end if
end if



end event

public function integer wf_imposta_folder ();string ls_nome_dw, ls_des_folder, ls_sharing, ls_sql
long   ll_i, ll_count=0
unsignedlong lu_sharing
uo_tab_richieste luo_richieste
datastore lds_folder
userobject ltp_page
uo_cs_xx_dw ldw_dw

ls_sql = "select	nome_dw,des_folder,sharing_type from tab_tipi_richieste_dw where cod_azienda = '"+s_cs_xx.cod_azienda+"' "+&
			"order by cod_azienda asc,  progressivo asc"

ll_count = guo_functions.uof_crea_datastore(lds_folder, ls_sql)

tab_dettaglio.setredraw(true)

for ll_i = ll_count + 1 to 5
	ltp_page = tab_dettaglio.control[ll_i]
	ltp_page.visible = false
next

for ll_i=1 to ll_count
	
	lu_sharing = c_default
	ls_sharing = lds_folder.getitemstring(ll_i, "sharing_type")
	
	ltp_page = tab_dettaglio.control[ll_i]
	ltp_page.text =  lds_folder.getitemstring(ll_i, "des_folder")
	ltp_page.visible = true
	
	ldw_dw = ltp_page.control[1]
	ldw_dw.dataobject = lds_folder.getitemstring(ll_i, "nome_dw")
	
	if ll_i > 1 then
		if isnull(ls_sharing) or len(ls_sharing) < 1 then 
			lu_sharing = c_sharedata + c_scrollparent
		else
			if pos(lower(ls_sharing), "c_sharedata") > 0        then lu_sharing = lu_sharing + c_sharedata
			if pos(lower(ls_sharing), "c_scrollparent") > 0     then lu_sharing = lu_sharing + c_scrollparent
			if pos(lower(ls_sharing), "c_retrieveonopen") > 0   then lu_sharing = lu_sharing + c_retrieveonopen
			if pos(lower(ls_sharing), "c_noretrieveonopen") > 0 then lu_sharing = lu_sharing + c_noretrieveonopen
			if pos(lower(ls_sharing), "c_nonew") > 0            then lu_sharing = lu_sharing + c_nonew
			if pos(lower(ls_sharing), "c_nomodify") > 0         then lu_sharing = lu_sharing + c_nomodify
			if pos(lower(ls_sharing), "c_nodelete") > 0         then lu_sharing = lu_sharing + c_nodelete
			if pos(lower(ls_sharing), "c_default") > 0          then lu_sharing = lu_sharing + c_default
		end if
		
		ldw_dw.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, lu_sharing, c_default + c_nohighlightselected)
	else
		
		lu_sharing = c_noretrieveonopen
		ldw_dw.set_dw_options(sqlca, pcca.null_object, lu_sharing, c_default + c_nohighlightselected)
	end if
	
	iuo_richieste.uof_setddlb(ldw_dw)

next

tab_ricerca.documenti.dw_documenti.settransobject(sqlca)

return 0 
end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello


choose case wf_get_valore_livello(ai_livello)
	case "A"
		return wf_inserisci_anno(al_handle)
	
	case "D"
		return wf_inserisci_commesse(al_handle)
		
	case "U"
		return wf_inserisci_ubicazioni(al_handle)
	
	case "Z"
		return wf_inserisci_attrezzature(al_handle)
		
	case "T"
		return wf_inserisci_tipi_richieste(al_handle)
	
	case "O"
		return wf_inserisci_operatori_reg(al_handle)
		
	case "R"
		return wf_inserisci_operatori_ris(al_handle)
	
	case "I"
		return wf_inserisci_operatori_inc(al_handle)
		
	case "E"
		return wf_inserisci_risorse_esterne(al_handle)
			
	case else
		return wf_inserisci_richieste(al_handle)
		
end choose

return 0
end function

public subroutine wf_imposta_ricerca ();integer		li_anno_registrazione, li_anno_documento
long			l_error, ll_num_registrazione, ll_num_documento
string			ls_flag_chiusa, ls_flag_obsoleto, ls_flag_straordinario, ls_cod_operaio_registrazione, ls_cod_richiedente, &
				ls_cod_operaio_risolve, ls_cod_area_chiamata, ls_cod_classe_richiesta, ls_cod_tipologia_richiesta, ls_flag_competenza, &
				ls_flag_gen_manutenzioni, ls_cod_documento, ls_numeratore_documento, ls_cod_operaio_incaricato, ls_cod_divisione, ls_chiamata_cliente,&
				ls_cod_cat_risorsa_esterna, ls_cod_risorsa_esterna, ls_cod_area_aziendale, ls_nome_richiedente, ls_cod_tipo_richiesta, &
				ls_cod_attrezzatura, ls_sql_new, ls_barcode, ls_errore, ls_flag_stampata
boolean		lb_barcode
datetime		ldt_data_reg_inizio, ldt_data_reg_fine


is_sql_filtro = ""
tab_ricerca.ricerca.dw_ricerca.accepttext()

tab_dettaglio.det_1.dw_1.retrieve("", -1, -1)
//wf_retrieve_blob(-1)


li_anno_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "num_registrazione")

ls_flag_chiusa = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_chiusa")
ls_flag_obsoleto = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_obsoleto")
ls_flag_straordinario = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_straordinario")
ls_flag_stampata = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_stampata")

ls_cod_operaio_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_operaio_registrazione")
ls_cod_richiedente = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_richiedente")
ls_cod_area_chiamata = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_area_chiamata")
ls_cod_classe_richiesta = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_classe_richiesta")
ls_cod_tipologia_richiesta = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipologia_richiesta")
ls_flag_competenza = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_competenza")
ls_flag_gen_manutenzioni = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_gen_manutenzioni")
ls_cod_documento = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_documento")
ls_numeratore_documento = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"numeratore_documento")
li_anno_documento = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"anno_documento")
ll_num_documento = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"num_documento")
ls_cod_operaio_incaricato = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_operaio_incaricato")
ls_cod_divisione = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_divisione")
ls_chiamata_cliente = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"chiamata_cliente")
ls_cod_cat_risorsa_esterna = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_cat_risorsa_esterna")
ls_cod_risorsa_esterna = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_risorsa_esterna")
ls_cod_area_aziendale = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_area_aziendale")
ls_nome_richiedente = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"nome_richiedente")
ls_cod_tipo_richiesta = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_richiesta")
ls_cod_attrezzatura = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_attrezzatura")
ls_barcode = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"barcode")

ldt_data_reg_inizio = tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_reg_inizio")
ldt_data_reg_inizio = datetime(date(ldt_data_reg_inizio), 00:00:00)
ldt_data_reg_fine = tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_reg_fine")
ldt_data_reg_fine = datetime(date(ldt_data_reg_fine), 23:59:59)

//----------------------------------------------------------------------------------------------
// stefanop 10/04/2012: spec. "ordini_lavoro_richieste" alpiq
if not isnull(ls_barcode) and ls_barcode <> "" then
	wf_get_barcode(ls_barcode, li_anno_registrazione, ll_num_registrazione, ls_errore)
	lb_barcode = true
	
	if not isnull(ls_errore) or ls_errore <> "" then
		g_mb.error(ls_errore)
		return
	end if
end if
// ----

if not isnull(li_anno_registrazione) and li_anno_registrazione > 1900 then
	is_sql_filtro += " AND tab_richieste.anno_registrazione=" + string(li_anno_registrazione)
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione > 0 then
	is_sql_filtro += " AND tab_richieste.num_registrazione=" + string(ll_num_registrazione)
end if
if not isnull(li_anno_registrazione) and li_anno_registrazione>0 and not isnull(ll_num_registrazione) and ll_num_registrazione>0 then
	// mi fermo non serve altro, filtro solo la singola richiesta
	return
end if
//----------------------------------------------------------------------------------------------

if ls_flag_chiusa<>"T" then
	is_sql_filtro += " and tab_richieste.flag_richiesta_chiusa='"+ls_flag_chiusa+"'"
end if
if ls_flag_obsoleto<>"T" then
	is_sql_filtro += " and tab_richieste.flag_richiesta_obsoleta='"+ls_flag_obsoleto+"'"
end if
if ls_flag_straordinario<>"T" then
	is_sql_filtro += " and tab_richieste.flag_richiesta_straordinaria='"+ls_flag_straordinario+"'"
end if
if ls_flag_competenza<>"T" then
	is_sql_filtro += " and tab_richieste.flag_competenza='"+ls_flag_competenza+"'"
end if
if ls_flag_stampata<>"T" and ls_flag_stampata<>'' and not isnull(ls_flag_stampata) then
	is_sql_filtro += " and tab_richieste.flag_stampato='"+ls_flag_stampata+"'"
end if
if ls_flag_gen_manutenzioni<>"T" then
	is_sql_filtro += " and tab_richieste.flag_gen_manutenzione='"+ls_flag_gen_manutenzioni+"'"
end if
if ls_cod_operaio_registrazione<>"" and not isnull(ls_cod_operaio_registrazione) then
	is_sql_filtro += " and tab_richieste.cod_operaio_registrazione='"+ls_cod_operaio_registrazione+"'"
end if
if ls_cod_richiedente<>"" and not isnull(ls_cod_richiedente) then
	is_sql_filtro += " and tab_richieste.cod_richiedente='"+ls_cod_richiedente+"'"
end if
if ls_cod_area_chiamata<>"" and not isnull(ls_cod_area_chiamata) then
	is_sql_filtro += " and tab_richieste.cod_area_chiamata='"+ls_cod_area_chiamata+"'"
end if
if ls_cod_classe_richiesta<>"" and not isnull(ls_cod_classe_richiesta) then
	is_sql_filtro += " and tab_richieste.cod_classe_richiesta='"+ls_cod_classe_richiesta+"'"
end if
if ls_cod_tipologia_richiesta<>"" and not isnull(ls_cod_tipologia_richiesta) then
	is_sql_filtro += " and tab_richieste.cod_tipologia_richiesta='"+ls_cod_tipologia_richiesta+"'"
end if
if ls_cod_documento<>"" and not isnull(ls_cod_documento) then
	is_sql_filtro += " and tab_richieste.cod_documento='"+ls_cod_documento+"'"
end if
if ls_numeratore_documento<>"" and not isnull(ls_numeratore_documento) then
	is_sql_filtro += " and tab_richieste.numeratore_documento='"+ls_numeratore_documento+"'"
end if
if li_anno_documento>0 then
	is_sql_filtro += " and tab_richieste.anno_documento="+string(li_anno_documento)
end if
if ll_num_documento>0 then
	is_sql_filtro += " and tab_richieste.num_documento="+string(ll_num_documento)
end if
if ls_cod_operaio_incaricato<>"" and not isnull(ls_cod_operaio_incaricato) then
	is_sql_filtro += " and tab_richieste.cod_operaio_incaricato='"+ls_cod_operaio_incaricato+"'"
end if
if ls_cod_divisione<>"" and not isnull(ls_cod_divisione) then
	is_sql_filtro += " and tab_richieste.cod_divisione='"+ls_cod_divisione+"'"
end if
if ls_chiamata_cliente<>"" and not isnull(ls_chiamata_cliente) then
	is_sql_filtro += " and tab_richieste.nr_chiamata_cliente='"+ls_chiamata_cliente+"'"
end if
if ls_cod_cat_risorsa_esterna<>"" and not isnull(ls_cod_cat_risorsa_esterna) then
	is_sql_filtro += " and tab_richieste.cod_cat_risorse_esterne='"+ls_cod_cat_risorsa_esterna+"'"
end if
if ls_cod_risorsa_esterna<>"" and not isnull(ls_cod_risorsa_esterna) then
	is_sql_filtro += " and tab_richieste.cod_risorsa_esterna='"+ls_cod_risorsa_esterna+"'"
end if
if ls_cod_area_aziendale<>"" and not isnull(ls_cod_area_aziendale) then
	is_sql_filtro += " and tab_richieste.cod_area_aziendale='"+ls_cod_area_aziendale+"'"
end if
if ls_nome_richiedente<>"" and not isnull(ls_nome_richiedente) then
	is_sql_filtro += " and tab_richieste.nome_richiedente='"+ls_nome_richiedente+"'"
	
end if
if ls_cod_tipo_richiesta<>"" and not isnull(ls_cod_tipo_richiesta) then
	is_sql_filtro += " and tab_richieste.cod_tipo_richiesta='"+ls_cod_tipo_richiesta+"'"
end if
if ls_cod_attrezzatura<>"" and not isnull(ls_cod_attrezzatura) then
	is_sql_filtro += " and tab_richieste.cod_attrezzatura='"+ls_cod_attrezzatura+"'"
end if
if not isnull(ldt_data_reg_inizio) then
	is_sql_filtro += " AND tab_richieste.data_registrazione>='" + string(ldt_data_reg_inizio, s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(ldt_data_reg_fine) then
	is_sql_filtro += " AND tab_richieste.data_registrazione<='" + string(ldt_data_reg_fine, s_cs_xx.db_funzioni.formato_data) +"'"
end if

end subroutine

public function boolean wf_get_barcode (string as_barcode, ref integer ai_anno_registrazione, ref long al_num_registrazione, ref string as_errore);/**
 * stefanop
 * 10/04/2012
 *
 * Legge il barcode dell'ordine di di lavoro e chiude la richiesta.
 **/
 
long ll_anno_registrazione, ll_num_registrazione

setnull(ai_anno_registrazione)
setnull(al_num_registrazione)
setnull(as_errore)
 
// Validazioni
if isnull(as_barcode) or as_barcode = "" then return true

as_barcode = trim(as_barcode)

if len(as_barcode) < 11 then
	as_errore = "Formato barcode non valido"
	return false
end if

ai_anno_registrazione = integer(mid(as_barcode, 2,4))
al_num_registrazione = long(mid(as_barcode, 6))

return true

end function

public function integer wf_inserisci_richieste (long al_handle);string				ls_sql, ls_error
long				ll_rows, ll_i
treeviewitem	ltvi_item


ls_sql = "SELECT tab_richieste.anno_registrazione, tab_richieste.num_registrazione, tab_richieste.data_registrazione,"+&
					"tab_richieste.anno_documento, tab_richieste.cod_documento, tab_richieste.numeratore_documento, tab_richieste.num_documento,"+&
					"tab_richieste.flag_richiesta_chiusa,tab_richieste.cod_divisione ,tab_richieste.des_richiesta "+&
			"FROM tab_richieste WHERE tab_richieste.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#4 DESC,#5 DESC,#6 DESC,#7DESC,")
ids_store.sort()

if ll_rows > il_max_richieste then
	
	if not g_mb.confirm(g_str.format("Attenzione: sono state recuperate più di $1 richieste.~r~nContinuare a visualizzare ?~r~nPotrebbe richiedere diverso tempo", il_max_richieste)) then
		ll_rows = il_max_richieste
	end if
	
end if


for ll_i = 1 to ll_rows
	wf_inserisci_singola_richiesta(	al_handle, ids_store.getitemnumber(ll_i, 1), ids_store.getitemnumber(ll_i, 2), ids_store.getitemdatetime(ll_i, 3), &
											ids_store.getitemnumber(ll_i, 4), ids_store.getitemstring(ll_i, 5), ids_store.getitemstring(ll_i, 6), ids_store.getitemnumber(ll_i, 7), &
											ids_store.getitemstring(ll_i, 8), ids_store.getitemstring(ll_i, 9), ids_store.getitemstring(ll_i, 10))
next

if isnull(ll_rows) then ll_rows=0

return ll_rows
end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long					ll_item
treeviewitem		ltv_item
str_treeview			lstr_data
string					ls_array_cat_e_risorsa[]

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello		
			
		case "A"		//anno registrazione
			as_sql += " AND tab_richieste.anno_registrazione = " + lstr_data.codice + " "
			
		case "D"		//commessa/divisione
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tab_richieste.cod_divisione is null "
			else
				as_sql += " AND tab_richieste.cod_divisione = '" + lstr_data.codice + "' "
			end if
			
		case "U"		//Ubicazione/Area aziendale
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tab_richieste.cod_area_aziendale is null "
			else
				as_sql += " AND tab_richieste.cod_area_aziendale = '" + lstr_data.codice + "' "
			end if
	
		case "Z"		//Attrezzatura
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tab_richieste.cod_attrezzatura is null "
			else
				as_sql += " AND tab_richieste.cod_attrezzatura = '" + lstr_data.codice + "' "
			end if
			
		case "T"		//Tipo richiesta
			as_sql += " AND tab_richieste.cod_tipo_richiesta = '" + lstr_data.codice + "' "
			
			
		case "O"		//Operatore registra
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tab_richieste.cod_operaio_registrazione is null "
			else
				as_sql += " AND tab_richieste.cod_operaio_registrazione = '" + lstr_data.codice + "' "
			end if
			
		case "R"		//Operatore risolve
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tab_richieste.cod_operaio_risolve is null "
			else
				as_sql += " AND tab_richieste.cod_operaio_risolve = '" + lstr_data.codice + "' "
			end if
			
		case "I"		//Operatore incaricato
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tab_richieste.cod_operaio_incaricato is null "
			else
				as_sql += " AND tab_richieste.cod_operaio_incaricato = '" + lstr_data.codice + "' "
			end if
		
		case "E"		//Risorsa Esterna e Categoria Risorsa Esterna
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tab_richieste.cod_risorsa_esterna is null "
			else
				
				g_str.explode(lstr_data.codice, "&", ls_array_cat_e_risorsa[])
				as_sql += " AND tab_richieste.cod_cat_risorse_esterne = '" + ls_array_cat_e_risorsa[1] + "' and "+&
									  "tab_richieste.cod_risorsa_esterna = '" + ls_array_cat_e_risorsa[2] + "' "
			end if

		
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public subroutine wf_treeview_icons ();ICONA_RICHIESTA = wf_treeview_add_icon("treeview\tipo_documento.png")
ICONA_RICHIESTA_CHIUSA = wf_treeview_add_icon("treeview\tag_red.png")
ICONA_RICHIESTA_INIZIATA = wf_treeview_add_icon("treeview\tag_yellow.png")

ICONA_RICHIESTA_SEL = wf_treeview_add_icon("treeview\tipo_documento_selected.png")
ICONA_RICHIESTA_CHIUSA_SEL = wf_treeview_add_icon("treeview\tag_red_selected.png")
ICONA_RICHIESTA_INIZIATA_SEL = wf_treeview_add_icon("treeview\tag_yellow_selected.png")

ICONA_OPERATORE = wf_treeview_add_icon("treeview\operatore.png")
ICONA_ANNO = wf_treeview_add_icon("treeview\anno.png")
ICONA_DIVISIONE = wf_treeview_add_icon("treeview\deposito.png")
ICONA_UBICAZIONE = wf_treeview_add_icon("treeview\area_aziendale.png")
ICONA_ATTREZZATURA = wf_treeview_add_icon("treeview\prodotto.png")
ICONA_TIPO_RICHIESTA = wf_treeview_add_icon("treeview\cartella.png")
end subroutine

public subroutine wf_valori_livelli ();

wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Anno Registrazione", "A")
wf_add_valore_livello(is_etichetta_divisione, "D")
wf_add_valore_livello(is_etichetta_area_az, "U")
wf_add_valore_livello("Attrezzatura", "Z")
wf_add_valore_livello("Tipo Richiesta", "T")
wf_add_valore_livello("Operatore Registra", "O")
wf_add_valore_livello("Operatore Incaricato", "I")
wf_add_valore_livello("Operatore Risolve", "R")
wf_add_valore_livello("Risorsa Esterna", "E")

end subroutine

public subroutine wf_retrieve_blob (long al_row);integer			li_anno_registrazione
long				ll_num_registrazione, ll_count

tab_ricerca.documenti.dw_documenti.reset()

if al_row > 0 then
	li_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(al_row, "anno_registrazione")
	ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(al_row, "num_registrazione")
	
	if not isnull(li_anno_registrazione) and ll_num_registrazione > 0 then
		ll_count = tab_ricerca.documenti.dw_documenti.retrieve(s_cs_xx.cod_azienda, li_anno_registrazione, ll_num_registrazione )
	end if
end if

if isnull(ll_count) then ll_count = 0
	
tab_ricerca.documenti.text = "Documenti ("+string(ll_count)+")"

end subroutine

public function integer wf_blocca_folder (string fs_cod_tipo_richiesta);string						ls_flag_blocco
long						ll_i, ll_cont=0
uo_tab_richieste		luo_richieste
boolean					lb_visible[]

//per tipologia richiesta, è possibile impostare di bloccare qualche flder, tra quelli previsti

//reset di tutti e 5
lb_visible[1] = false
lb_visible[2] = false
lb_visible[3] = false
lb_visible[4] = false
lb_visible[5] = false


//prima conta quanti folder ci sono
//successivamente verifica se qualcuno (tra quelli presenti è da bloccare/rendere invisibile per tipo richiesta)
select count(*)
into :ll_cont
from tab_tipi_richieste_dw  
where 	cod_azienda = :s_cs_xx.cod_azienda;

for ll_i=1 to ll_cont
	lb_visible[ll_i] = true
next


declare cu_folder cursor for  
	select flag_blocco         
	from tab_tipi_richieste_folder  
	where 	cod_azienda = :s_cs_xx.cod_azienda   and
				cod_tipo_richiesta = :fs_cod_tipo_richiesta
	order by cod_azienda asc, progressivo asc  ;

open cu_folder;
if sqlca.sqlcode <> 0 then
	g_mb.error("OMNIA", "Errore nell'impostazione dei dati della finestra (open cursor)." + sqlca.sqlerrtext)
	return -1
end if

ll_cont = 0

do while true
	fetch cu_folder into :ls_flag_blocco;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.error("OMNIA", "Errore nell'impostazione dei dati della finestra (fetch cursor)." + sqlca.sqlerrtext)
		return -1
	end if
	
	ll_cont += 1		//progressivo del folder (da 1 a 5)
	
	if ls_flag_blocco = 'S' then		//se qualcuno è da bloccare lo rendo invisibile
		lb_visible[ll_cont] = false
	end if
	
loop

close cu_folder;

tab_dettaglio.det_1.visible = lb_visible[1]
tab_dettaglio.det_2.visible = lb_visible[2]
tab_dettaglio.det_3.visible = lb_visible[3]
tab_dettaglio.det_4.visible = lb_visible[4]
tab_dettaglio.det_5.visible = lb_visible[5]




return 0 
end function

public function integer wf_invia_mail (string as_oggetto, string as_messaggio);s_cs_xx.parametri.parametro_s_1 = "L"
s_cs_xx.parametri.parametro_s_2 = ""
s_cs_xx.parametri.parametro_s_3 = ""
s_cs_xx.parametri.parametro_s_4 = ""
s_cs_xx.parametri.parametro_s_5 = as_oggetto
s_cs_xx.parametri.parametro_s_6 = as_messaggio
s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai destinatari della fase successiva?"
s_cs_xx.parametri.parametro_s_8 = ""
s_cs_xx.parametri.parametro_s_11 = ""
s_cs_xx.parametri.parametro_s_12 = ""
//s_cs_xx.parametri.parametro_s_13 = string(ll_anno)
//s_cs_xx.parametri.parametro_s_14 = string(ll_num)
s_cs_xx.parametri.parametro_s_15 = "N"
//s_cs_xx.parametri.parametro_ul_3 = ll_num_sequenza_corrente
s_cs_xx.parametri.parametro_b_1 = true
					
openwithparm( w_invio_messaggi, 0)	

return 0
end function

public subroutine wf_abilita_pulsanti (boolean ab_enabled);iuo_richieste.uof_new_modify(tab_dettaglio.det_1.dw_1,ab_enabled)
iuo_richieste.uof_new_modify(tab_dettaglio.det_2.dw_2,ab_enabled)
iuo_richieste.uof_new_modify(tab_dettaglio.det_3.dw_3,ab_enabled)
iuo_richieste.uof_new_modify(tab_dettaglio.det_4.dw_4,ab_enabled)
iuo_richieste.uof_new_modify(tab_dettaglio.det_5.dw_5,ab_enabled)


ib_nuovo = ab_enabled
ib_modifica=ab_enabled
ib_visualizza=not ab_enabled


end subroutine

public function integer wf_inserisci_anno (long al_handle);string ls_sql, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anno_registrazione FROM tab_richieste WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 DESC")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "A"
	lstr_data.codice = string(ids_store.getitemnumber(ll_i, 1))
	
	ltvi_item = wf_new_item(true, ICONA_ANNO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = string(ids_store.getitemnumber(ll_i, 1))
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_ubicazioni (long al_handle);string 					ls_sql, ls_error, ls_des_item
long 						ll_rows, ll_i
treeviewitem 			ltvi_item


ls_sql = 	"SELECT DISTINCT tab_richieste.cod_area_aziendale, tab_aree_aziendali.des_area "+&
			"FROM tab_richieste "+&
			"LEFT OUTER JOIN tab_aree_aziendali ON tab_aree_aziendali.cod_azienda=tab_richieste.cod_azienda AND "+&
															"tab_aree_aziendali.cod_area_aziendale=tab_richieste.cod_area_aziendale "+&
			"WHERE tab_richieste.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#2 ASC")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "U"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	ltvi_item = wf_new_item(true, ICONA_UBICAZIONE)
	
	ltvi_item.data = lstr_data
	ls_des_item = ids_store.getitemstring(ll_i, 2)
	
	if ls_des_item="" or isnull(ls_des_item) then
		ls_des_item = "<"+is_etichetta_area_az+" mancante>"
	end if
	
	ltvi_item.label = ls_des_item
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_commesse (long al_handle);string				ls_sql, ls_error, ls_des_item
long				ll_rows, ll_i
treeviewitem 	ltvi_item


ls_sql = 	"SELECT DISTINCT tab_richieste.cod_divisione, anag_divisioni.des_divisione "+&
			"FROM tab_richieste "+&
			"LEFT OUTER JOIN anag_divisioni ON anag_divisioni.cod_azienda=tab_richieste.cod_azienda AND "+&
															"anag_divisioni.cod_divisione=tab_richieste.cod_divisione "+&
			"WHERE tab_richieste.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#2 ASC")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "D"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	ltvi_item = wf_new_item(true, ICONA_DIVISIONE)
	
	ltvi_item.data = lstr_data
	
	if lstr_data.codice="" or isnull(lstr_data.codice) then
		ls_des_item = "<"+is_etichetta_divisione+" mancante>"
	else
		ls_des_item = ids_store.getitemstring(ll_i, 2)
	end if
	
	ltvi_item.label = ls_des_item
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_attrezzature (long al_handle);string				ls_sql, ls_error, ls_des_item
long				ll_rows, ll_i
treeviewitem	ltvi_item


ls_sql = 	"SELECT DISTINCT tab_richieste.cod_attrezzatura, anag_attrezzature.descrizione "+&
			"FROM tab_richieste "+&
			"LEFT OUTER JOIN anag_attrezzature ON anag_attrezzature.cod_azienda=tab_richieste.cod_azienda AND "+&
															"anag_attrezzature.cod_attrezzatura=tab_richieste.cod_attrezzatura "+&
			"WHERE tab_richieste.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 ASC")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "Z"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	ltvi_item = wf_new_item(true, ICONA_ATTREZZATURA)
	
	ltvi_item.data = lstr_data
	
	if lstr_data.codice="" or isnull(lstr_data.codice) then
		ls_des_item = "<Attrezzatura mancante>"
	else
		ls_des_item = "(" + lstr_data.codice + ") " + ids_store.getitemstring(ll_i, 2)
	end if
	
	ltvi_item.label = ls_des_item
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_tipi_richieste (long al_handle);string ls_sql, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = 	"SELECT DISTINCT tab_richieste.cod_tipo_richiesta, isnull(tab_tipi_richieste.des_tipo_richiesta, '<Tipo Richiesta mancante>') "+&
			"FROM tab_richieste "+&
			"LEFT OUTER JOIN tab_tipi_richieste ON tab_tipi_richieste.cod_azienda=tab_richieste.cod_azienda AND "+&
															"tab_tipi_richieste.cod_tipo_richiesta=tab_richieste.cod_tipo_richiesta "+&
			"WHERE tab_richieste.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#2 ASC")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "T"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	ltvi_item = wf_new_item(true, ICONA_TIPO_RICHIESTA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ids_store.getitemstring(ll_i, 2)
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_operatori_reg (long al_handle);string				ls_sql, ls_error, ls_des_item
long				ll_rows, ll_i
treeviewitem 	ltvi_item


ls_sql = 	"SELECT DISTINCT tab_richieste.cod_operaio_registrazione, anag_operai.cognome + ' ' + anag_operai.nome "+&
			"FROM tab_richieste "+&
			"LEFT OUTER JOIN anag_operai ON anag_operai.cod_azienda=tab_richieste.cod_azienda AND "+&
															"anag_operai.cod_operaio=tab_richieste.cod_operaio_registrazione "+&
			"WHERE tab_richieste.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#2 ASC")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "O"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	ltvi_item = wf_new_item(true, ICONA_OPERATORE)
	
	ltvi_item.data = lstr_data
	
	if lstr_data.codice="" or isnull(lstr_data.codice) then
		ls_des_item = "Operatore Registra mancante>"
	else
		//ls_des_item = ids_store.getitemstring(ll_i, 2) + " ("+ids_store.getitemstring(ll_i, 1) + ")"
		ls_des_item = ids_store.getitemstring(ll_i, 2)
	end if
	
	ltvi_item.label = ls_des_item
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_operatori_ris (long al_handle);string				ls_sql, ls_error, ls_des_item
long				ll_rows, ll_i
treeviewitem 	ltvi_item


ls_sql = 	"SELECT DISTINCT tab_richieste.cod_operaio_risolve, anag_operai.cognome + ' ' + anag_operai.nome "+&
			"FROM tab_richieste "+&
			"LEFT OUTER JOIN anag_operai ON anag_operai.cod_azienda=tab_richieste.cod_azienda AND "+&
															"anag_operai.cod_operaio=tab_richieste.cod_operaio_risolve "+&
			"WHERE tab_richieste.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#2 ASC")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "R"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	ltvi_item = wf_new_item(true, ICONA_OPERATORE)
	
	ltvi_item.data = lstr_data
	
	if lstr_data.codice="" or isnull(lstr_data.codice) then
		ls_des_item = "<Operatore Risolve mancante>"
	else
		//ls_des_item = ids_store.getitemstring(ll_i, 2) + " ("+ids_store.getitemstring(ll_i, 1)+")"
		ls_des_item = ids_store.getitemstring(ll_i, 2)
	end if
	
	ltvi_item.label = ls_des_item
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_operatori_inc (long al_handle);string				ls_sql, ls_error, ls_des_item
long				ll_rows, ll_i
treeviewitem	ltvi_item


ls_sql = 	"SELECT DISTINCT tab_richieste.cod_operaio_incaricato, anag_operai.cognome + ' ' + anag_operai.nome "+&
			"FROM tab_richieste "+&
			"LEFT OUTER JOIN anag_operai ON anag_operai.cod_azienda=tab_richieste.cod_azienda AND "+&
															"anag_operai.cod_operaio=tab_richieste.cod_operaio_incaricato "+&
			"WHERE tab_richieste.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#2 ASC")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "I"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	ltvi_item = wf_new_item(true, ICONA_OPERATORE)
	
	ltvi_item.data = lstr_data
	
	if lstr_data.codice="" or isnull(lstr_data.codice) then
		ls_des_item = "<Operatore Incaricato mancante>"
	else
		//ls_des_item = ids_store.getitemstring(ll_i, 2) + " ("+ids_store.getitemstring(ll_i, 1) + ")"
		ls_des_item = ids_store.getitemstring(ll_i, 2)
	end if
	
	ltvi_item.label = ls_des_item
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_risorse_esterne (long al_handle);string					ls_sql, ls_error, ls_des_item
long 					ll_rows, ll_i
treeviewitem 		ltvi_item


ls_sql = 	"SELECT DISTINCT tab_richieste.cod_cat_risorse_esterne, tab_richieste.cod_risorsa_esterna, "+&
									"anag_risorse_esterne.descrizione "+&
			"FROM tab_richieste "+&
			"LEFT OUTER JOIN anag_risorse_esterne ON anag_risorse_esterne.cod_azienda=tab_richieste.cod_azienda AND "+&
															"anag_risorse_esterne.cod_cat_risorse_esterne=tab_richieste.cod_cat_risorse_esterne and "+&
															"anag_risorse_esterne.cod_risorsa_esterna=tab_richieste.cod_risorsa_esterna "+&
			"WHERE tab_richieste.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

ids_store.setsort("#3 ASC")
ids_store.sort()

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "E"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1) + "&" + ids_store.getitemstring(ll_i, 2)
	
	ltvi_item = wf_new_item(true, ICONA_OPERATORE)
	
	ltvi_item.data = lstr_data
	
	if ids_store.getitemstring(ll_i, 2)="" or isnull(ids_store.getitemstring(ll_i, 2)) then
		setnull(lstr_data.codice)
		ls_des_item = "<Risorsa Esterna Incaricata mancante>"
	else
		//nel formato PAPERINO PAOLINO (IDR - IDR01)
		ls_des_item = ids_store.getitemstring(ll_i, 3) + " ("+ids_store.getitemstring(ll_i, 1)+" - "+ids_store.getitemstring(ll_i, 2)+")"
	end if
	
	ltvi_item.label = ls_des_item
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_refresh_richiesta (long al_handle, long al_anno_registrazione, long al_num_registrazione, datetime adt_data_registrazione, integer ai_anno_documento, string as_cod_documento, string as_numeratore_documento, long al_num_documento, string as_flag_richiesta_chiusa);string					ls_sql, ls_error
treeviewitem 		ltvi_item
str_treeview 		lstr_data
long					ll_count
	
//recupero la struttura esistente e successivamente modifico quelle che voglio
tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltvi_item)
lstr_data = ltvi_item.data

//se non è un nodo di una richiesta esco subito per evitare problemi
if lstr_data.tipo_livello <> "N" or upperbound(lstr_data.decimale[]) < 2 then
	return 0
end if

if lstr_data.decimale[1]>0 and lstr_data.decimale[2]>0 then
else
	return 0
end if
//-----------------------------------------------------------------

if as_flag_richiesta_chiusa = "S" then
	//ltvi_item = wf_new_item(false, ICONA_RICHIESTA_CHIUSA, ICONA_RICHIESTA_CHIUSA_SEL)
	ltvi_item.pictureindex = ICONA_RICHIESTA_CHIUSA		
	ltvi_item.selectedpictureindex = ICONA_RICHIESTA_CHIUSA_SEL	
	lstr_data.stringa[1] = "S"
else
	
	select count(*)
	into :ll_count
	from manutenzioni
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_reg_richiesta=:al_anno_registrazione and
				num_reg_richiesta=:al_num_registrazione;
	
	if ll_count>0 then
		//esistono manutenzioni/fasi
		//ltvi_item = wf_new_item(false, ICONA_RICHIESTA_INIZIATA, ICONA_RICHIESTA_INIZIATA_SEL)
		ltvi_item.pictureindex = ICONA_RICHIESTA_INIZIATA
		ltvi_item.selectedpictureindex = ICONA_RICHIESTA_INIZIATA_SEL
		lstr_data.stringa[1] = "S"
	else
		//ltvi_item = wf_new_item(false, ICONA_RICHIESTA, ICONA_RICHIESTA_SEL)
		ltvi_item.pictureindex = ICONA_RICHIESTA
		ltvi_item.selectedpictureindex = ICONA_RICHIESTA_SEL
		lstr_data.stringa[1] = "N"
	end if
end if


ltvi_item.children = false
	

//mettiamo sul nodo le info della richiesta -----------------------------
if ai_anno_documento>0 and as_numeratore_documento<>"" and not isnull(as_numeratore_documento) and al_num_documento>0 then
	ltvi_item.label = string(ai_anno_documento) + "/" + as_numeratore_documento + "/" + string(al_num_documento) + " ("+string(al_anno_registrazione) + "/"+ string(al_num_registrazione)+")"
else
	ltvi_item.label = string(al_anno_registrazione) + "/"+ string(al_num_registrazione)
end if

if not isnull(adt_data_registrazione) and year(date(adt_data_registrazione)) > 1950 then
	ltvi_item.label += " del " + string(adt_data_registrazione, "dd/mm/yyyy")
end if

//-------------------------------------------------------------------------

//aggiorno la struttura e la riassegno al nodo
ltvi_item.data = lstr_data

//return tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
tab_ricerca.selezione.tv_selezione.setitem(al_handle, ltvi_item)

tab_dettaglio.det_1.dw_1.retrieve(s_cs_xx.cod_azienda, al_anno_registrazione, al_num_registrazione)


return 1
end function

public subroutine wf_aggiorna_risposta (ref uo_cs_xx_dw fdw_datawindow);string				ls_describe, ls_risposta
integer			li_anno
long				ll_numero, ll_row

ll_row = fdw_datawindow.getrow()
if not (ll_row>0) then
	return
end if

try
	//se presente effettuo il refresh della risposta alla richiesta
	ls_describe = fdw_datawindow.Describe("risposta_richiesta.Visible")

	choose case ls_describe
		case "!","?"
			return
			
		case "1"
			// continue
			
		case else
			return
			
	end choose
	
	li_anno = fdw_datawindow.getitemnumber(ll_row, "anno_registrazione")
	ll_numero = fdw_datawindow.getitemnumber(ll_row, "num_registrazione")
	
	if not (li_anno > 0 and ll_numero > 0) then
		return
	end if
	
	ls_risposta = f_descrizione_risposta_richiesta(li_anno, ll_numero,"S")
	
	if not isnull(ls_risposta) then
		
		fdw_datawindow.setitem(ll_row,"risposta_richiesta",ls_risposta)
	
		fdw_datawindow.Modify("risposta_richiesta.tabsequence=1")
		fdw_datawindow.Modify("risposta_richiesta.Edit.DisplayOnly=Yes")
		fdw_datawindow.Modify("risposta_richiesta.Edit.HScrollBar=Yes")
		fdw_datawindow.Modify("risposta_richiesta.Edit.VScrollBar=Yes")
		fdw_datawindow.Modify("risposta_richiesta.Edit.AutoVScroll=Yes")
		fdw_datawindow.Modify("risposta_richiesta.Edit.AutoVScroll=Yes")
		
		fdw_datawindow.setitemstatus(ll_row, "risposta_richiesta", primary!,NotModified! )
		
	end if
catch (RuntimeError err)
end try
end subroutine

public subroutine wf_imposta_tipo_richiesta_predefinita (long al_row, string as_dataobject);/**
 * stefanop
 * 07/03/2014
 *
 * Imposto il tipo pratica predefinito dell'utente se presente
 **/

string ls_cod_tipo_richiesta
 
select stringa
into :ls_cod_tipo_richiesta
from parametri_azienda_utente
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 cod_parametro = 'TRP';
		 
if not isnull(ls_cod_tipo_richiesta) and ls_cod_tipo_richiesta <> "" then
	try
		tab_dettaglio.det_1.dw_1.setitem(al_row, "cod_tipo_richiesta", ls_cod_tipo_richiesta)
	catch(RuntimeError e)
	end try
end if

end subroutine

public function long wf_inserisci_singola_richiesta (long al_handle, long al_anno_registrazione, long al_num_registrazione, datetime adt_data_registrazione, integer ai_anno_documento, string as_cod_documento, string as_numeratore_documento, long al_num_documento, string as_flag_richiesta_chiusa, string as_cod_divisione, string as_des_richiesta);string					ls_sql, ls_error, ls_flag_visualizza_commessa, ls_commessa
treeviewitem 		ltvi_item
str_treeview 		lstr_data
long					ll_count

ls_flag_visualizza_commessa = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_visualizza_commessa")

lstr_data.livello = il_livello
lstr_data.tipo_livello = "N"
lstr_data.decimale[1] = al_anno_registrazione
lstr_data.decimale[2] = al_num_registrazione

ls_commessa = ""

if as_cod_divisione<>"" and not isnull(as_cod_divisione) then
	select des_divisione
	into :ls_commessa
	from anag_divisioni
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_divisione=:as_cod_divisione;
	
	if sqlca.sqlcode=0 then
	else
		ls_commessa = as_cod_divisione
	end if
	
end if


if as_flag_richiesta_chiusa = "S" then
	ltvi_item = wf_new_item(false, ICONA_RICHIESTA_CHIUSA, ICONA_RICHIESTA_CHIUSA_SEL)
	lstr_data.stringa[1] = "S"
else
	
	select count(*)
	into :ll_count
	from manutenzioni
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_reg_richiesta=:al_anno_registrazione and
				num_reg_richiesta=:al_num_registrazione;
	
	if ll_count>0 then
		//esistono manutenzioni/fasi
		ltvi_item = wf_new_item(false, ICONA_RICHIESTA_INIZIATA, ICONA_RICHIESTA_INIZIATA_SEL)
		lstr_data.stringa[1] = "S"
	else
		ltvi_item = wf_new_item(false, ICONA_RICHIESTA, ICONA_RICHIESTA_SEL)
		lstr_data.stringa[1] = "N"
	end if
end if


//mettiamo sul nodo le info della richiesta -----------------------------
if ai_anno_documento>0 and as_numeratore_documento<>"" and not isnull(as_numeratore_documento) and al_num_documento>0 then
	ltvi_item.label +=   g_str.format("$1-$2 ($3-$4-$5) ", al_anno_registrazione, al_num_registrazione,ai_anno_documento, as_numeratore_documento,al_num_documento)
else
	ltvi_item.label +=   g_str.format("$1-$2 (--) ", al_anno_registrazione, al_num_registrazione)
end if

if not isnull(adt_data_registrazione) and year(date(adt_data_registrazione)) > 1950 then
	ltvi_item.label += g_str.format(" [$1]", string(adt_data_registrazione,"dd/mm/yyyy"))
end if

if not isnull(as_des_richiesta) then
	ltvi_item.label += g_str.format(" $1 ", left(as_des_richiesta,100))
end if

if ls_flag_visualizza_commessa="S" then
	if ls_commessa<>"" and not isnull(ls_commessa) then ltvi_item.label += " : " + ls_commessa
end if

//-------------------------------------------------------------------------

ltvi_item.data = lstr_data
istr_data = lstr_data

return tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
end function

on w_call_center_tv.create
int iCurrent
call super::create
end on

on w_call_center_tv.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag

is_codice_filtro = "RIC"

//LA dw di lista non c'è, questa è la finestra con il tree ##############################

//// *** se non esiste il parametro GCB allora faccio vedere 
////		 la lista con la commessa
//select flag
//into   :ls_flag
//from   parametri
//where  cod_parametro = 'GCB';
//
//if sqlca.sqlcode <> 0 or (sqlca.sqlcode = 0 and not isnull(ls_flag) and ls_flag = "N") then
//	dw_call_center_lista.dataobject = "d_call_center_lista_divisione"
//end if

//###############################################################


//L'evento ue_invio_mail in questa versione di finestra è stato rimosso, in quanto nella precedente (w_call_center)
//esiste l'evento ma la chiamata dell'evento no o era commentato



iuo_richieste = create uo_tab_richieste

tab_dettaglio.det_1.dw_1.ib_dw_detail = true
tab_dettaglio.det_2.dw_2.ib_dw_detail = true
tab_dettaglio.det_3.dw_3.ib_dw_detail = true
tab_dettaglio.det_4.dw_4.ib_dw_detail = true
tab_dettaglio.det_5.dw_5.ib_dw_detail = true

if wf_imposta_folder() = -1 then return

iuo_dw_main = tab_dettaglio.det_1.dw_1

il_livello = 0


//LETTURA PARAMETRI VARI ################################################
setnull(ls_flag)
select flag
into :ls_flag
from parametri_azienda
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_parametro = 'RIC';

if sqlca.sqlcode = 0 then
	ib_paramRIC = true
end if

//---------------------------------------------
setnull(ls_flag)
select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
		 
if sqlca.sqlcode = 0 and ls_flag ="S" then ib_global_service = true

if ib_global_service then
	
	is_etichetta_divisione="Commessa"
	is_etichetta_area_az="Ubicazione"
	is_etichette_reparto="Impianto"
	
	ib_ol_visibile= true
	
	setnull(ls_flag)
	select flag
	into   :ls_flag
	from   parametri_azienda
	where  cod_parametro = 'ASS' and flag_parametro='F';
	
	if ls_flag="S" then
		ib_supplementi_visibile = true
	else
		//NO FUNZIONE SUPPLEMENTI
	end if
else
	//NO FUNZIONE SUPPLEMENTI
end if

iuo_richieste.ib_global_service = ib_global_service
//--------------------------------------------------------

//12/07/2010 verifica se deve apparire il messaggio di conferma invio e-mail se aggiorni la richiesta a staordinaria
select flag
into   :is_NMS
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'F' and &
       parametri_azienda.cod_parametro = 'NMS';

//se non esiste: funzionamento normale
if isnull(is_NMS) or is_NMS="" then is_NMS="N"
//------------------------------------------------------------------------------

//###############################################################

tab_ricerca.ricerca.dw_ricerca.setitem(1, "anno_registrazione", f_anno_esercizio())

event post ue_barcode_focus()

string ls_valore
long 	ll_return


if s_cs_xx.parametri.parametro_s_1 = "GEN_RICHIESTA" then
	ib_proviene_da_nc=true
	il_anno_nc = s_cs_xx.parametri.parametro_ul_1
	il_num_nc  = s_cs_xx.parametri.parametro_ul_2
	
	s_cs_xx.parametri.parametro_s_1 = ""
	setnull(s_cs_xx.parametri.parametro_ul_1)
	setnull(s_cs_xx.parametri.parametro_ul_2)
	
	tab_dettaglio.det_1.dw_1.postevent("pcd_new")
end if

// -- stefanop 03/07/2009: Controllo parametro MAI nel registro per inviare la mail
ll_return = registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "mai", ls_valore) 
if ll_return = -1 or long(ls_valore) <= 0 then
	ib_send_mail = false
else
	ib_send_mail = true
end if
// ----------------------
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_sort(		tab_ricerca.ricerca.dw_ricerca, &
							  "cod_tipo_richiesta", &
							  sqlca, &
							  "tab_tipi_richieste", &
							  "cod_tipo_richiesta", &
							  "des_tipo_richiesta", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ", &
							  " cod_tipo_richiesta ASC ")

f_po_loaddddw_sort(	tab_ricerca.ricerca.dw_ricerca, &
							  "cod_richiedente", &
							  sqlca, &
							  "tab_richiedenti", &
							  "cod_richiedente", &
							  "nominativo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
							  " nominativo ASC ")
					  
f_po_loaddddw_sort(tab_ricerca.ricerca.dw_ricerca, &
						  "cod_area_chiamata", &
						  sqlca, &
						  "tab_aree_chiamate", &
						  "cod_area_chiamata", &
						  "des_area_chiamata", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ", &
						  " des_area_chiamata ASC ")					  

f_po_loaddddw_sort(	tab_ricerca.ricerca.dw_ricerca, &
							  "cod_operaio_registrazione", &
							  sqlca, &
							  "anag_operai", &
							  "cod_operaio", &
							  "cognome + ' ' + nome", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
							  " cognome ASC, nome ASC ")
					  
					  
f_po_loaddddw_sort(	tab_ricerca.ricerca.dw_ricerca, &
							  "cod_operaio_incaricato", &
							  sqlca, &
							  "anag_operai", &
							  "cod_operaio", &
							  "cognome + ' ' + nome", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
							  " cognome ASC, nome ASC ")

f_po_loaddddw_sort(	tab_ricerca.ricerca.dw_ricerca, &
							  "cod_tipologia_richiesta", &
							  sqlca, &
							  "tab_richieste_tipologie", &
							  "cod_tipologia_richiesta", &
							  "des_tipologia_richiesta", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
							  " des_tipologia_richiesta ASC ")

f_po_loaddddw_sort(	tab_ricerca.ricerca.dw_ricerca, &
							  "cod_classe_richiesta", &
							  sqlca, &
							  "tab_richieste_classificazioni", &
							  "cod_classe_richiesta", &
							  "des_classe_richiesta", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
							  " des_classe_richiesta ASC ")

f_po_loaddddw_sort(	tab_ricerca.ricerca.dw_ricerca, &
							  "cod_documento", &
							  sqlca, &
							  "tab_documenti", &
							  "cod_documento", &
							  "des_documento", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
							  " des_documento ASC ")

// stefano 05/05/2010
if tab_dettaglio.det_1.dw_1.dataobject <> "d_call_center_8" then
	f_po_loaddddw_sort(	tab_ricerca.ricerca.dw_ricerca, &
								  "cod_cat_risorsa_esterna", &
								  sqlca, &
								  "tab_cat_risorse_esterne", &
								  "cod_cat_risorse_esterne", &
								  "des_cat_risorse_esterne", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
								  " des_cat_risorse_esterne ASC ")
end if
end event

event close;call super::close;destroy iuo_richieste
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_call_center_tv
integer x = 1527
integer y = 16
integer width = 3621
integer height = 2624
boolean raggedright = false
boolean focusonbuttondown = false
boolean showpicture = false
det_2 det_2
det_3 det_3
det_4 det_4
det_5 det_5
end type

on tab_dettaglio.create
this.det_2=create det_2
this.det_3=create det_3
this.det_4=create det_4
this.det_5=create det_5
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_4,&
this.det_5}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
destroy(this.det_3)
destroy(this.det_4)
destroy(this.det_5)
end on

event tab_dettaglio::ue_resize;call super::ue_resize;int li_i, li_j
datawindow ldw_dw

// scorro i tab
for li_i = 1 to upperbound(control)

	// scorro gli elementi del tabpage
	for li_j = 1 to upperbound(control[li_i].control)
		if control[li_i].control[li_j].typeof() = DataWindow! then
			
			ldw_dw = control[li_i].control[li_j]
			ldw_dw.move(0,0)
			ldw_dw.width = control[li_i].width

		end if
	next 	
	
next

end event

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 3584
integer height = 2500
string text = "Folder 1"
dw_1 dw_1
end type

on det_1.create
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_call_center_tv
integer width = 1495
integer height = 2628
integer textsize = -8
boolean raggedright = false
documenti documenti
end type

on tab_ricerca.create
this.documenti=create documenti
call super::create
this.Control[]={this.ricerca,&
this.selezione,&
this.documenti}
end on

on tab_ricerca.destroy
call super::destroy
destroy(this.documenti)
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer y = 104
integer width = 1458
integer height = 2508
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer width = 1445
integer height = 2472
string dataobject = "d_call_center_ricerca_tv"
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_divisione"
		guo_ricerca.uof_ricerca_divisione(dw_ricerca, "cod_divisione")
		
	case "b_ricerca_area_aziendale"
		guo_ricerca.uof_ricerca_area_aziendale(dw_ricerca, "cod_area_aziendale")
		
	case "b_ricerca_attrezz"
		guo_ricerca.uof_ricerca_attrezzatura(dw_ricerca, "cod_attrezzatura")
		
end choose
end event

event dw_ricerca::itemchanged;call super::itemchanged;string	ls_null
date		ld_data_inizio, ld_data_fine, ld_data_neutra


if getcolumnname() = "" then return

choose case getcolumnname()
	case "cod_divisione"
		
		f_po_loaddddw_dw(dw_ricerca, &
						  "cod_area_chiamata", &
						  sqlca, &
						  "tab_aree_chiamate", &
						  "cod_area_chiamata", &
						  "des_area_chiamata", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + data + "'")
						  
		f_po_loaddddw_sort(dw_ricerca, &
							  "cod_richiedente", &
							  sqlca, &
							  "tab_richiedenti", &
							  "cod_richiedente", &
							  "nominativo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + data + "'", &
							  " nominativo ASC ")
		
	case "cod_cat_risorsa_esterna"
		
			setnull(ls_null)
			setitem(row,"cod_risorsa_esterna",ls_null)
		
	// stefanop 10/04/2012: spec. "ordini_lavoro_richieste" alpiq
	case "barcode"
		//w_call_center_tv.postevent("pc_retrieve")
		getwindow().postevent("pc_retrieve")
		
end choose
end event

event dw_ricerca::itemfocuschanged;call super::itemfocuschanged;if isvalid(dwo) then
	
	if dwo.name = "cod_risorsa_esterna" then
		
		f_po_loaddddw_sort(dw_ricerca, &
                 "cod_risorsa_esterna", &
                 sqlca, &
                 "anag_risorse_esterne", &
                 "cod_risorsa_esterna", &
                 "descrizione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + getitemstring(getrow(),"cod_cat_risorsa_esterna") + "'", &
					  " descrizione ASC ")
					  
	end if

end if
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer y = 104
integer width = 1458
integer height = 2508
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
boolean tooltips = true
end type

event tv_selezione::rightclicked;call super::rightclicked;long ll_row
treeviewitem ltvi_item
str_treeview lstr_data
m_call_center_tv lm_menu

if AncestorReturnValue < 0 then return

pcca.window_current = getwindow()
//ll_row = tab_dettaglio.det_1.dw_1.getrow()

//tab_ricerca.selezione.tv_selezione.selectitem(handle)
tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "N" then

	lm_menu = create m_call_center_tv
	
	lm_menu.m_olavoro.visible =  ib_ol_visibile
	lm_menu.m_supplementi.visible =ib_supplementi_visibile
	
	if lstr_data.stringa[1] <> "S" then
		lm_menu.m_datimanutenzione.visible = false
	end if
	
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
	destroy lm_menu
	
end if
end event

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data


if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

if not ib_select then
	ib_select = true
	return
end if

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data

tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")
end event

type dw_1 from uo_cs_xx_dw within det_1
event ue_esegui_retrieve ( )
event ue_selectitem ( long al_handle )
integer x = 23
integer y = 24
integer width = 3365
integer height = 1976
integer taborder = 40
boolean border = false
long il_limit_campo_note = 32000
end type

event ue_esegui_retrieve();long 					ll_handle, ll_row, ll_num_registrazione, ll_num_documento
//treeviewitem 		ltvi_item
//str_treeview 		lstr_data
integer				li_anno_registrazione, li_anno_documento
string					ls_cod_documento, ls_numeratore_documento, ls_flag_richiesta_chiusa
datetime				ldt_data_registrazione


ll_handle = wf_get_current_handle()
ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_handle>0 and ll_row>0 then
	li_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
	ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")
	//---------------------------------
	li_anno_documento = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_documento")
	ls_cod_documento =  tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_documento")
	ll_num_documento = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_documento")
	ls_numeratore_documento = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "numeratore_documento")
	ldt_data_registrazione = tab_dettaglio.det_1.dw_1.getitemdatetime(ll_row, "data_registrazione")
	
	//ls_flag_richiesta_chiusa = tab_dettaglio.det_1.dw_1.getitemstring(ll_row,"flag_richiesta_chiusa")
	select flag_richiesta_chiusa
	into :ls_flag_richiesta_chiusa
	from tab_richieste
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno_registrazione and
			num_registrazione=:ll_num_registrazione;
	
	if isvalid(istr_data) and not isnull(istr_data) and UpperBound(istr_data.decimale) >= 2 then
		//refresh del nodo della richiesta e retrieve della dw per aggiornare eventuali campi relativi a tempi e risposta alla richiesta
		wf_refresh_richiesta(	wf_get_current_handle(), li_anno_registrazione, ll_num_registrazione, ldt_data_registrazione, &
									li_anno_documento, ls_cod_documento, ls_numeratore_documento, ll_num_documento, &
									ls_flag_richiesta_chiusa)
		
	end if
	
end if

end event

event ue_selectitem(long al_handle);
ib_select = false
if al_handle>0 then tab_ricerca.selezione.tv_selezione.selectitem(al_handle)
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(istr_data) or isnull(istr_data) or UpperBound(istr_data.decimale) < 2 then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2])

if ll_errore < 0 then
   pcca.error = c_fatal
end if

if ll_errore>0 then
else
	tab_ricerca.documenti.dw_documenti.retrieve("", -1, -1)
end if
// forse questo non serve
// change_dw_current()
end event

event clicked;call super::clicked;if i_extendmode then

	iuo_richieste.uof_clicked(dw_1, row, dwo)
		
end if
end event

event doubleclicked;call super::doubleclicked;if i_extendmode then
	
	iuo_richieste.uof_doubleclicked(dw_1, row, dwo)
	
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	
	if i_colname = "cod_tipo_richiesta" then
		if not isnull(i_coltext) then
			wf_blocca_folder(i_coltext)
		end if
	end if	
	
	iuo_richieste.uof_itemchanged(dw_1, row, i_colname, i_coltext)

end if
end event

event pcd_modify;call super::pcd_modify;
if ib_paramRIC then
	dw_1.settaborder("num_documento",140)
	dw_1.object.num_documento.border = 5
	dw_1.object.num_documento.background.mode = 0
	dw_1.object.num_documento.background.color = RGB(255,255,255) 
end if


wf_abilita_pulsanti(true)
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	long ll_row
	
	ll_row = getrow()
	
	if isvalid(iuo_richieste) then
	
		iuo_richieste.uof_rowfocuschanged(REF dw_1, getrow())
		
	end if
	
	wf_retrieve_blob(ll_row)
	
end if
end event

event pcd_new;call super::pcd_new;
string ls_cod_operaio
long ll_row

ll_row = getrow()
if ll_row <= 0 then return

wf_imposta_tipo_richiesta_predefinita(ll_row, string(dw_1.dataobject))

if dw_1.dataobject = "d_call_center_6" or dw_1.dataobject = "d_call_center_10" then
	
	select cod_operaio
	into :ls_cod_operaio
	from anag_operai
	where cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode = 0 then
		if ls_cod_operaio <> "" and not isnull(ls_cod_operaio) then
			setitem(ll_row,"cod_operaio_registrazione", ls_cod_operaio)
		end if		
	end if
	
end if

setitem(ll_row, "data_registrazione",datetime(today(),00:00:00))
setitem(ll_row, "ora_registrazione",datetime(date(1900, 1, 1),Now()))
// stefano 11/05/2010
if dw_1.dataobject = "d_call_center_8" then
	setitem(ll_row, "data_passaggio_chiamata",datetime(today(),00:00:00))
	setitem(ll_row, "ora_passaggio_chiamata",datetime(date(1900, 1, 1),Now()))
end if

//setitem(getrow(), "ora_registrazione",Now())

if ib_proviene_da_nc then
	setitem(ll_row, "anno_non_conf", il_anno_nc)
	setitem(ll_row, "num_non_conf" , il_num_nc)
	setitem(ll_row, "des_richiesta" ,"Richiesta da non conformità n. " + string(il_anno_nc) + "/" + string(il_num_nc))
end if

wf_abilita_pulsanti(true)

// impostazione default campi (tolti da initial value della DW per evitare problemi in chiusura window con riga vuota)
setitem(ll_row,"flag_tipo_richiesta","M")
setitem(ll_row,"flag_richiedente_codificato","S")
setitem(ll_row,"flag_comunicato_risposta","N")
setitem(ll_row,"flag_richiesta_chiusa","N")
setitem(ll_row,"flag_richiesta_obsoleta","N")
setitem(ll_row,"flag_richiesta_straordinaria","N")
setitem(ll_row,"flag_competenza","S")
setitem(ll_row,"flag_gen_manutenzione","N")
setitem(ll_row,"flag_richiesta_reso","N")
setitem(ll_row,"cod_utente_creazione", s_cs_xx.cod_utente)

// Impostazione della commessa di default
// fatto per ALPIQ  20.05.2011
if dw_1.dataobject = "d_call_center_10" then
	 setitem(ll_row,"cod_divisione","001")
	 setitem(ll_row,"cod_tipo_richiesta","001")
	 setitem(ll_row,"flag_richiedente_codificato","N")
end if



end event

event pcd_view;call super::pcd_view;wf_abilita_pulsanti(false)
end event

event updateend;call super::updateend;string				ls_oggetto, ls_messaggio, ls_numeratore_documento, ls_richiesta, ls_des_richiesta, ls_cod_documento, ls_flag_richiesta_chiusa, ls_cod_divisione
long				ll_num_documento, ll_num_registrazione, ll_row, ll_handle
integer			li_anno_documento, li_anno_registrazione
datetime			ldt_data_registrazione
DWItemStatus	ldw_status

// -- stefanop 03/07/2009: se è possibile invio la mail (ticket 2009/158)
if rowsinserted + rowsupdated > 0 then
	
	ll_row = this.getrow()
	if ll_row>0 then
		
		li_anno_registrazione = getitemnumber(ll_row, "anno_registrazione")
		ll_num_registrazione = getitemnumber(ll_row, "num_registrazione")
		//---------------------------------
		li_anno_documento = getitemnumber(ll_row, "anno_documento")
		ls_cod_documento =  getitemstring(ll_row, "cod_documento")
		ll_num_documento = getitemnumber(ll_row, "num_documento")
		ls_numeratore_documento = getitemstring(ll_row, "numeratore_documento")
		ldt_data_registrazione = getitemdatetime(ll_row, "data_registrazione")
		ls_des_richiesta = getitemstring(ll_row,"des_richiesta")
		ls_flag_richiesta_chiusa = getitemstring(ll_row,"flag_richiesta_chiusa")
		ls_cod_divisione = getitemstring(ll_row,"cod_divisione")
		
		//INSERISCI NODO NELL TREE SE NUOVA RICHIESTA ----------------------------------------------------------
		ldw_status = dw_1.getitemstatus(ll_row, 0, Primary!)
		choose case ldw_status
			case NewModified!
				//la inserisco a partire dalla root (handle 0)
				ll_handle = wf_inserisci_singola_richiesta(	0, li_anno_registrazione, ll_num_registrazione, ldt_data_registrazione, &
																		li_anno_documento, ls_cod_documento, ls_numeratore_documento, ll_num_documento, &
																		ls_flag_richiesta_chiusa,ls_cod_divisione,ls_des_richiesta)
				
				//lasciare questo commenato: non so perchè ma in inserimento e seguente salvataggio chiede da errore di chiave duplicata!!!!!
				//********************************************************************************************************
				//********************************************************************************************************
				//tab_ricerca.selezione.tv_selezione.selectitem(ll_handle)
				//********************************************************************************************************
				//********************************************************************************************************
				
				event post ue_selectitem(ll_handle)
				
			case DataModified!
				if not isvalid(istr_data) or isnull(istr_data) or UpperBound(istr_data.decimale) < 2 then
				else
					//refresh del nodo della richiesta e retrieve della dw per aggiornare eventuali campi relativi a tempi e risposta alla richiesta
					wf_refresh_richiesta(wf_get_current_handle(), li_anno_registrazione, ll_num_registrazione, ldt_data_registrazione, &
																		li_anno_documento, ls_cod_documento, ls_numeratore_documento, ll_num_documento, &
																		ls_flag_richiesta_chiusa)
				end if

		end choose
		//-------------------------------------------------------------------------------------------------------------------------------
	
		//INVIO MAIL SE PREVISTO E SE SI TRATTA DI UNA MODIFICA O INSERIMENTO ------------------------------------
		if ib_send_mail then
			ls_richiesta = string(li_anno_documento) + "/" + ls_numeratore_documento + "/" + string(ll_num_documento)
			
			if rowsinserted > 0 then
				ls_oggetto = " (OMNIA) NUOVA RICHIESTA NUM.: "
			end if
			
			if rowsupdated > 0 then
				ls_oggetto = " (OMNIA) MODIFICA RICHIESTA NUM.: "
			end if
			
			ls_oggetto += ls_richiesta + " DEL " + string(ldt_data_registrazione, "dd/mm/yyyy")
			ls_messaggio += ls_oggetto + "~r~n" + ls_des_richiesta
			
			wf_invia_mail(ls_oggetto, ls_messaggio)
		end if
		//-------------------------------------------------------------------------------------------------------------------------------
		
	end if
	
end if

end event

event updatestart;call super::updatestart;string ls_flag_straordinario, ls_des_richiesta, ls_cod_tipo_richiesta, ls_des_tipo_richiesta, ls_note_idl,ls_des_tipo_manutenzione,ls_cod_guasto,ls_cod_attrezzatura
long ll_i,ll_anno_registrazione,ll_num_registrazione, ll_cont
datetime ldt_data_neutra
dwItemStatus l_statusRow

ldt_data_neutra = datetime(date("01/01/1900"),00:00:00)

for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		continue
	end if
	if getitemstatus(ll_i,0,primary!) = NewModified! or getitemstatus(ll_i,0,primary!) = New! or getitemstatus(ll_i,0,primary!) = DataModified! then
		if isnull(GetItemdatetime(ll_i, "data_registrazione")) or GetItemdatetime(ll_i, "data_registrazione") <= ldt_data_neutra then
			if g_mb.messagebox("Omnia","Data registrazione obbligatoria: la imposto con quella attuale?",question!,YesNo!,1) = 1 then
				// se non è stata impostata data e ora, li imposto automaticamente.
				setitem(ll_i, "data_registrazione",datetime(today(),00:00:00))
				setitem(getrow(), "ora_registrazione",datetime(date(1900, 1, 1),Now()))				
				//setitem(ll_i, "ora_registrazione",Now())
			else
				return 1
			end if
		end if
		
		setitem(ll_i, "cod_utente_modifica", s_cs_xx.cod_utente)
		setitem(ll_i, "data_ultima_modifica", datetime(today(), now()))
	end if
next

if ib_global_service then
	for ll_i = 1 to rowcount()
		if getitemstatus(ll_i,"cod_guasto",primary!) = DataModified! then
		
			ls_cod_guasto = getitemstring(ll_i, "cod_guasto")
			ls_cod_attrezzatura = getitemstring(ll_i, "cod_attrezzatura")
			
			select count(*)
			into :ll_cont
			from tab_guasti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_attrezzatura = :ls_cod_attrezzatura and
					cod_guasto = :ls_cod_guasto;
					
			if (isnull(ll_cont) or ll_cont = 0) and sqlca.sqlcode = 0 then
				
				insert into tab_guasti
					(cod_azienda,
					 cod_guasto,
					 cod_attrezzatura,
					 des_guasto)
						select 	cod_azienda,
								cod_guasto,
								:ls_cod_attrezzatura,
								des_guasto
						from 	tab_guasti_generici
						where 	cod_azienda = :s_cs_xx.cod_azienda and
								cod_guasto = :ls_cod_guasto;
			end if
		end if
		
		if getitemstatus(ll_i,"flag_richiesta_straordinaria",primary!) = DataModified! then
			
			ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione")
			ll_num_registrazione = getitemnumber(ll_i, "num_registrazione")
			ls_flag_straordinario = getitemstring(ll_i, "flag_richiesta_straordinaria")
			
			update manutenzioni
			set    flag_straordinario = :ls_flag_straordinario
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_reg_richiesta = :ll_anno_registrazione and
					 num_reg_richiesta  = :ll_num_registrazione ;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Omnia", "Errore aggiornamento flag_straordinario nella manutenzione collegata.~r~n"+sqlca.sqlerrtext)
				return 1
			end if
			
			// -- stefanop 06/05/2009: se il flag = S allora chiedo ed invio mail (sie solari, modifiche intranet)
			//if getitemstring(ll_i, "flag_richiesta_straordinaria") = "S" then
			
			//12/07/2010 se il parametro aziendale flag  (NMS) vale a S non chiedere nè inviare email (chiesto da davide Baggio)
			if getitemstring(ll_i, "flag_richiesta_straordinaria") = "S" and is_NMS<>"S" then
				if g_mb.messagebox("Omnia", "Si vuole inviare una mail di avviso?", Question!, YesNo!) = 1 then
					s_cs_xx.parametri.parametro_s_1 = "L"
					s_cs_xx.parametri.parametro_s_4 = ""
					s_cs_xx.parametri.parametro_s_5 = "Richiesta straordinaria " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
					s_cs_xx.parametri.parametro_s_6 = "Richiesta straordinaria " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
					s_cs_xx.parametri.parametro_s_13 = string(ll_anno_registrazione)
					s_cs_xx.parametri.parametro_s_14 = string(ll_num_registrazione)
					s_cs_xx.parametri.parametro_s_15 = "N"
					s_cs_xx.parametri.parametro_b_1 = true
										
					openwithparm( w_invio_messaggi, 0)	
				end if
			end if
			// ----------------------
			
		end if		
		
		if getitemstatus(ll_i,"des_richiesta",primary!) = DataModified! then
			
			ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione")
			ll_num_registrazione = getitemnumber(ll_i, "num_registrazione")
			ls_cod_tipo_richiesta = getitemstring(ll_i, "cod_tipo_richiesta")
			ls_des_richiesta = getitemstring(ll_i, "des_richiesta")
			
			select des_tipo_manutenzione
			into   :ls_des_tipo_manutenzione
			from   tab_tipi_richieste
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_richiesta = :ls_cod_tipo_richiesta;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Omnia", "Errore in ricerca tipo manutenzione nella tab_tipi_richieste."+sqlca.sqlerrtext)
				return 1
			end if
			
			if isnull(ls_des_tipo_manutenzione) then
				ls_note_idl = "RICHIESTA:~r~n"
			else
				ls_note_idl = ls_des_tipo_manutenzione + ":~r~n"
			end if
			
			if not isnull(ls_des_richiesta) and len(ls_des_richiesta) > 0 then
				ls_note_idl += ls_des_richiesta
			end if
			
			
			update manutenzioni
			set    note_idl = :ls_note_idl
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_reg_richiesta = :ll_anno_registrazione and
					 num_reg_richiesta  = :ll_num_registrazione ;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Omnia", "Errore aggiornamento istruzioni operative nella manutenzione collegata.~r~n"+sqlca.sqlerrtext)
				return 1
			end if
				
		end if
	next
	
end if

//for ll_i = 1 to this.deletedcount()
if DeletedCount() > 0 then
	
	//ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione", delete!, true)
	//ll_num_registrazione = getitemnumber(ll_i, "num_registrazione", delete!, true)
	ll_anno_registrazione = getitemnumber(1, "anno_registrazione", delete!, true)
	ll_num_registrazione = getitemnumber(1, "num_registrazione", delete!, true)
	
	//messagebox("DEBUG","DELETED " + string(ll_anno_registrazione) + "\" + string(ll_num_registrazione))
	
	select count(*)
	into   :ll_cont
	from   manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_richiesta = :ll_anno_registrazione and
			 num_reg_richiesta = :ll_num_registrazione;
			 
	if not isnull(ll_cont) and ll_cont > 0 then
		g_mb.messagebox("OMNIA","Richiesta " + string(ll_anno_registrazione) + "\" + string(ll_num_registrazione) + "~r~nEsistono delle manutenzioni associate a questa richiesta.~r~nE' necessario cancellare prima quelle!", Exclamation!)
		return -1
	end if

	delete tab_richieste_blob
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
          num_registrazione = :ll_num_registrazione;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Richiesta " + string(ll_anno_registrazione) + "\" + string(ll_num_registrazione) + "~r~nErrore sulla cancellazione documenti-anomalie. Errore DB:" + sqlca.sqlerrtext,stopsign!)
		return 1
		rollback;
	end if
	
	// stefano 10/05/2010: elimino fornitori associati alla richiesta
	if dw_1.dataobject="d_call_center_8" then
		delete from tab_richieste_fornitori
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
	end if
	// ----
	
	// Stefanop: 18/09/2015: elimino richieste supplementari (vr)
	delete from tab_richieste_supplementari
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
	
	//icona richiesta cancellata sull'albero
 	wf_set_deleted_item_status()

//next
end if


end event

event pcd_setkey;call super::pcd_setkey;

datetime		ldt_data_registrazione
long				ll_row, ll_num_registrazione, ll_min, ll_max, ll_num_documento
integer			li_anno_esercizio, li_giorno_anno, li_anno_documento
string			ls_cod_documento, ls_cod_tipo_richiesta, ls_numeratore_documento, ls_flag_contatore_giornaliero
date				ld_inizio

ll_row = getrow()
if ll_row > 0 then
else
	return
end if

li_anno_esercizio = f_anno_esercizio()

//azienda
if isnull(getitemstring(ll_row, "cod_azienda")) then
	setitem(ll_row, "cod_azienda", s_cs_xx.cod_azienda)
end if

//anno
if isnull(getitemnumber(ll_row, "anno_registrazione")) or getitemnumber(ll_row, "anno_registrazione") = 0 then
	setitem(ll_row, "anno_registrazione", li_anno_esercizio)
end if		

//numero
if isnull(getitemnumber(ll_row, "num_registrazione")) or getitemnumber(ll_row, "num_registrazione") = 0 then
		
	select max(num_registrazione)
	into   :ll_num_registrazione
	from   tab_richieste
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :li_anno_esercizio;
				 
	if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
		ll_num_registrazione = 1
	else
		ll_num_registrazione ++
	end if
		
	setitem(ll_row, "num_registrazione", ll_num_registrazione)

end if

//tipo richiesta -------------------------------------
ls_cod_tipo_richiesta = getitemstring(ll_row, "cod_tipo_richiesta")
		
if isnull(ls_cod_tipo_richiesta) or len(ls_cod_tipo_richiesta) < 1 then
	g_mb.error("OMNIA","Il tipo richiesta è obbligatorio!")
	PCCA.Error = c_Fatal
	return
end if


//se la numerazione per registro tipo richiesta è stata GIA' effettuata non rifare il calcolo della numerazione
ll_num_documento = getitemnumber(ll_row, "num_documento")
ls_cod_documento = getitemstring(ll_row, "cod_documento")
ls_numeratore_documento = getitemstring(ll_row, "numeratore_documento")
li_anno_documento = getitemnumber(ll_row, "anno_documento")


if ll_num_documento>0 and ls_cod_documento<>"" and not isnull(ls_cod_documento) and &
	ls_numeratore_documento<>"" and not isnull(ls_numeratore_documento) and li_anno_documento>0	then
else
	setnull(ll_num_documento)
	setnull(ls_cod_documento)
	setnull(ls_numeratore_documento)
	setnull(li_anno_documento)
	
	//leggo i parametri della richiesta
	select cod_documento, numeratore_documento, flag_contatore_giornaliero
	into   :ls_cod_documento, :ls_numeratore_documento, :ls_flag_contatore_giornaliero
	from   tab_tipi_richieste
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_richiesta = :ls_cod_tipo_richiesta;
			 
	if sqlca.sqlcode < 0 then
		g_mb.error("OMNIA","Errore in ricerca tipo richiesta in tabella tab_tipi_richieste: "+sqlca.sqlerrtext)
		PCCA.Error = c_Fatal
		return
		
	elseif sqlca.sqlcode = 100 then
		g_mb.error("OMNIA","Tipo richiesta "+ls_cod_tipo_richiesta+" non trovata in tabella tab_tipi_richieste")
		PCCA.Error = c_Fatal
		return
		
	end if

	//-------------------------------------------------------------------------
	//controlla che siano valorizzati i parametri del tipo richiesta
	if isnull(ls_cod_documento) or ls_cod_documento="" then
		g_mb.error("OMNIA","Il tipo richiesta selezionato ha il campo Cod. Documento non impostato. Controllare il tipo richiesta!")
		PCCA.Error = c_Fatal
		return
	end if
	
	if isnull(ls_numeratore_documento) or ls_numeratore_documento="" then
		g_mb.error("OMNIA","Il tipo richiesta selezionato ha il campo Numeratore Documento non impostato. Controllare il tipo richiesta!")
		PCCA.Error = c_Fatal
		return
	end if
	//-------------------------------------------------------------------------
	
	//predispongo cod documento, numeratore documento e anno documento
	li_anno_documento = li_anno_esercizio
	
	setitem(ll_row, "cod_documento", ls_cod_documento)
	setitem(ll_row, "numeratore_documento", ls_numeratore_documento)		
	setitem(ll_row, "anno_documento", li_anno_documento)
	
	choose case ls_flag_contatore_giornaliero
		//#########################################################################
		//GESTIONE FLAG CONTATORE GIORNALIERO ABILITATO
		case "S"
	
			ldt_data_registrazione = getitemdatetime(ll_row, "data_registrazione")
	
			ld_inizio = date( "01/01/" + string(li_anno_esercizio,"0000"))
			li_giorno_anno = daysafter(ld_inizio, date(ldt_data_registrazione) ) + 1
			ll_min = (li_giorno_anno * 1000) + 1
			ll_max = (li_giorno_anno + 1) * 1000
			ll_num_documento = 0
			
			select max(num_documento)
			into   :ll_num_documento
			from   tab_richieste
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_documento = :ls_cod_documento and
					 numeratore_documento = :ls_numeratore_documento and
					 anno_documento = :li_anno_documento and
					 num_documento >= :ll_min and
					 num_documento <= :ll_max;
			
			if sqlca.sqlcode < 0 then
				g_mb.error("OMNIA","Errore in lettura max num_documento in tabella  tab_richieste: " + sqlca.sqlerrtext)
				PCCA.Error = c_Fatal
				return 
			end if
	
			if isnull(ll_num_documento) or ll_num_documento = 0 then 
				ll_num_documento = ll_min
			else 
				ll_num_documento++
			end if
		
		//#########################################################################
		case else
			
			li_anno_esercizio = getitemnumber(ll_row, "anno_registrazione")

			select max(num_documento)
			into   :ll_num_documento
			from   tab_richieste
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_documento = :ls_cod_documento and
					 numeratore_documento = :ls_numeratore_documento and
					 anno_documento = :li_anno_documento;
					 
			if isnull(ll_num_documento) or ll_num_documento < 1 then
				ll_num_documento = 1
			else
				ll_num_documento ++
			end if
		//#########################################################################
	end choose
	
	
	//qui ho il numero documento
	setitem(ll_row, "num_documento", ll_num_documento)
	
	update numeratori
	set    num_documento = :ll_num_documento
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_documento = :ls_cod_documento and
			 numeratore_documento = :ls_numeratore_documento and
			 anno_documento = :li_anno_documento;
	//-------------------------------------------------------------------------------


end if

end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_tipo_richiesta

if getrow() >0 then
	ls_cod_tipo_richiesta = getitemstring(getrow(), "cod_tipo_richiesta")
	
	if isnull(ls_cod_tipo_richiesta) or ls_cod_tipo_richiesta="" then
		g_mb.messagebox("OMNIA","Tipo richiesta obbligatoria!")
		dw_1.setcolumn("cod_tipo_richiesta")
		pcca.error = c_fatal
		return
	end if
end if
end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3584
integer height = 2500
long backcolor = 12632256
string text = "Folder 2"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from uo_cs_xx_dw within det_2
event ue_esegui_retrieve ( )
integer x = 23
integer y = 24
integer width = 3365
integer height = 1664
integer taborder = 30
boolean border = false
long il_limit_campo_note = 32000
end type

event ue_esegui_retrieve();tab_dettaglio.det_1.dw_1.triggerevent("ue_esegui_retrieve")

wf_aggiorna_risposta(dw_2)
end event

event clicked;call super::clicked;if i_extendmode then
	
	iuo_richieste.uof_clicked(dw_2, row, dwo)
	
end if
end event

event doubleclicked;call super::doubleclicked;if i_extendmode then
	
	iuo_richieste.uof_doubleclicked(dw_2, row, dwo)
	
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	
	if i_colname = "cod_tipo_richiesta" then
		if not isnull(i_coltext) then
			wf_blocca_folder(i_coltext)
		end if
	end if	
	iuo_richieste.uof_itemchanged(dw_2, row, i_colname, i_coltext)
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	if isvalid(iuo_richieste) then
	
		iuo_richieste.uof_rowfocuschanged(REF dw_2, getrow())
		
	end if
	
end if
end event

type det_3 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3584
integer height = 2500
long backcolor = 12632256
string text = "Folder 3"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_3 dw_3
end type

on det_3.create
this.dw_3=create dw_3
this.Control[]={this.dw_3}
end on

on det_3.destroy
destroy(this.dw_3)
end on

type dw_3 from uo_cs_xx_dw within det_3
event ue_esegui_retrieve ( )
integer x = 23
integer y = 28
integer width = 3365
integer height = 1724
integer taborder = 30
boolean border = false
long il_limit_campo_note = 32000
end type

event ue_esegui_retrieve();tab_dettaglio.det_1.dw_1.triggerevent("ue_esegui_retrieve")

wf_aggiorna_risposta(dw_3)
end event

event clicked;call super::clicked;if i_extendmode then
	
	iuo_richieste.uof_clicked(dw_3, row, dwo)
	
end if
end event

event doubleclicked;call super::doubleclicked;if i_extendmode then
	
	iuo_richieste.uof_doubleclicked(dw_3, row, dwo)
	
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	
	if i_colname = "cod_tipo_richiesta" then
		if not isnull(i_coltext) then
			wf_blocca_folder(i_coltext)
		end if
	end if	
	iuo_richieste.uof_itemchanged(dw_3, row, i_colname, i_coltext)
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	if isvalid(iuo_richieste) then
	
		iuo_richieste.uof_rowfocuschanged(REF dw_3, getrow())
		
	end if
	
end if
end event

type det_4 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3584
integer height = 2500
long backcolor = 12632256
string text = "Folder 4"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_4 dw_4
end type

on det_4.create
this.dw_4=create dw_4
this.Control[]={this.dw_4}
end on

on det_4.destroy
destroy(this.dw_4)
end on

type dw_4 from uo_cs_xx_dw within det_4
event ue_esegui_retrieve ( )
integer x = 23
integer y = 24
integer width = 3365
integer height = 1976
integer taborder = 30
boolean border = false
long il_limit_campo_note = 32000
end type

event ue_esegui_retrieve();tab_dettaglio.det_1.dw_1.triggerevent("ue_esegui_retrieve")

wf_aggiorna_risposta(dw_4)
end event

event clicked;call super::clicked;if i_extendmode then
	
	iuo_richieste.uof_clicked(dw_4, row, dwo)
	
end if
end event

event doubleclicked;call super::doubleclicked;if i_extendmode then
	
	iuo_richieste.uof_doubleclicked(dw_4, row, dwo)
	
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	
	if i_colname = "cod_tipo_richiesta" then
		if not isnull(i_coltext) then
			wf_blocca_folder(i_coltext)
		end if
	end if	
	iuo_richieste.uof_itemchanged(dw_4, row, i_colname, i_coltext)
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	if isvalid(iuo_richieste) then
	
		iuo_richieste.uof_rowfocuschanged(REF dw_4, getrow())
		
	end if
	
end if
end event

type det_5 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3584
integer height = 2500
long backcolor = 12632256
string text = "Folder 5"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_5 dw_5
end type

on det_5.create
this.dw_5=create dw_5
this.Control[]={this.dw_5}
end on

on det_5.destroy
destroy(this.dw_5)
end on

type dw_5 from uo_cs_xx_dw within det_5
event ue_esegui_retrieve ( )
integer x = 23
integer y = 24
integer width = 3365
integer height = 1976
integer taborder = 30
boolean border = false
long il_limit_campo_note = 32000
end type

event ue_esegui_retrieve();tab_dettaglio.det_1.dw_1.triggerevent("ue_esegui_retrieve")

wf_aggiorna_risposta(dw_5)
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	
	if i_colname = "cod_tipo_richiesta" then
		if not isnull(i_coltext) then
			wf_blocca_folder(i_coltext)
		change_dw_current(this)
		end if
	end if	
	iuo_richieste.uof_itemchanged(dw_5, row, i_colname, i_coltext)
	
end if
end event

event clicked;call super::clicked;if i_extendmode then
	
	iuo_richieste.uof_clicked(dw_5, row, dwo)
	
end if
end event

event doubleclicked;call super::doubleclicked;if i_extendmode then
	
	iuo_richieste.uof_doubleclicked(dw_5, row, dwo)
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	if isvalid(iuo_richieste) then
	
		iuo_richieste.uof_rowfocuschanged(REF dw_5, getrow())
		
	end if
	
end if
end event

type documenti from userobject within tab_ricerca
integer x = 18
integer y = 104
integer width = 1458
integer height = 2508
long backcolor = 12632256
string text = "Documenti"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_documenti dw_documenti
end type

on documenti.create
this.dw_documenti=create dw_documenti
this.Control[]={this.dw_documenti}
end on

on documenti.destroy
destroy(this.dw_documenti)
end on

type dw_documenti from uo_dw_drag within documenti
integer x = 50
integer y = 36
integer width = 1381
integer height = 2360
integer taborder = 30
string dataobject = "d_call_center_blob_drag"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event buttonclicked;call super::buttonclicked;if isvalid(dwo) then
	if dwo.name = "b_elimina" and row > 0 then
		
		integer li_anno_registrazione
		long ll_num_registrazione, ll_progressivo
		string ls_des_blob, ls_errore
		transaction sqlc_blob
	
		
		li_anno_registrazione = getitemnumber(row, "anno_registrazione")
		ll_num_registrazione = getitemnumber(row, "num_registrazione")
		ll_progressivo = getitemnumber(row, "progressivo")
		ls_des_blob = getitemstring(row, "des_blob")
		
		if isnull(ls_des_blob) or ls_des_blob="" then ls_des_blob="<senza nome>"
		
		if g_mb.messagebox("Apice", "Procedo con la cancellazione del documento " + string(ll_progressivo) + " - " + ls_des_blob + "?",Question!, Yesno!, 2) = 2 then return 
		
		setpointer(Hourglass!)
		
		// creo transazione separata
		if not guo_functions.uof_create_transaction_from( sqlca, sqlc_blob, ls_errore)  then
			setpointer(Arrow!)
			g_mb.error("Errore in creazione transazione~r~n" + ls_errore)
			return
		end if
		
		delete tab_richieste_blob
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_registrazione and
				num_registrazione = :ll_num_registrazione and
				progressivo = :ll_progressivo
		using sqlc_blob;
		
		if sqlc_blob.sqlcode <> 0 then
			setpointer(Arrow!)
			g_mb.error("Errore in cancellazione documento~r~n " + sqlc_blob.sqlerrtext)
			rollback using sqlc_blob;
			disconnect using sqlc_blob;
			destroy sqlc_blob
		else
			commit using sqlc_blob;
			disconnect using sqlc_blob;
			destroy sqlc_blob
		end if		
		
		retrieve(s_cs_xx.cod_azienda, li_anno_registrazione, ll_num_registrazione)
		
		setpointer(Arrow!)
	end if
end if
end event

event doubleclicked;call super::doubleclicked;string ls_temp_dir, ls_rnd, ls_estensione, ls_file_name
long ll_anno_registrazione, ll_num_registrazione, ll_cont,ll_prog_mimetype, ll_len, ll_progressivo
blob lb_blob
uo_shellexecute luo_run

if row > 0 then
	
	setpointer(Hourglass!)
	
	ll_anno_registrazione = getitemnumber(row, "anno_registrazione")
	ll_num_registrazione = getitemnumber(row, "num_registrazione")
	
	ll_progressivo = getitemnumber(row, "progressivo")
	ll_prog_mimetype = getitemnumber(row, "prog_mimetype")
	
	selectblob blob
	into :lb_blob
	from tab_richieste_blob
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			progressivo = :ll_progressivo;
			
	if sqlca.sqlcode = 0  then 
		
		ll_len = lenA(lb_blob)
		
		select estensione
		into :ls_estensione
		from tab_mimetype
		where cod_azienda = :s_cs_xx.cod_azienda and
				prog_mimetype = :ll_prog_mimetype;
		
		ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
		
		ls_rnd = string( hour(now()),"00") +"_" + string( minute(now()),"00") +"_" + string( second(now()),"00") 
		ls_file_name =  ls_temp_dir + ls_rnd + "." + ls_estensione
		guo_functions.uof_blob_to_file( lb_blob, ls_file_name)
		
		
		luo_run = create uo_shellexecute
		luo_run.uof_run( handle(parent), ls_file_name )
		destroy(luo_run)
		
	end if
	
	setpointer(Arrow!)
	
end if
end event

event ue_drop_file;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

integer			li_anno_registrazione
long				ll_num_registrazione, ll_row, ll_progressivo, ll_len, ll_prog_mimetype
string				ls_cod_nota, ls_des_blob, ls_dir, ls_file, ls_ext,ls_errore
blob				lb_note_esterne


ll_row = tab_dettaglio.det_1.dw_1.getrow()

//di sicuro una riga selezionata c'è (controllato da ue_start_drop)
li_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")

select 	max(progressivo)
into 		:ll_progressivo
from 		tab_richieste_blob
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :li_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
			
if ll_progressivo = 0 or isnull(ll_progressivo) then
	ll_progressivo = 1
else
	ll_progressivo += 1
end if

ls_des_blob = "Documento"

guo_functions.uof_file_to_blob(as_filename, lb_note_esterne)

ll_len = lenA(lb_note_esterne)

guo_functions.uof_get_file_info( as_filename, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;
if sqlca.sqlcode <> 0 then
	as_message = "Attenzione: estensione file " + ls_ext + " non prevista da sistema."
	return false
end if

insert into tab_richieste_blob  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  progressivo,   
		  des_blob, 
		  blob,
		  prog_mimetype,
		  descrizione)  
values ( :s_cs_xx.cod_azienda,   
		  :li_anno_registrazione,   
		  :ll_num_registrazione,
		  :ll_progressivo,
		  :ls_file,   
		  null,
		 :ll_prog_mimetype,
		 :ls_file) 
using itran_note;

if itran_note.sqlcode = 0 then
	
	updateblob tab_richieste_blob
	set blob = :lb_note_esterne
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :li_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			progressivo = :ll_progressivo
	using itran_note;
			
	if itran_note.sqlcode <> 0 then
		as_message = "Errore nell'inserimento del documento digitale.~r~n" + itran_note.sqlerrtext
		return false
	end if
	
else
	
	if itran_note.sqlcode <> 0 then
		as_message = "Errore nell'inserimento del documento digitale.~r~n" + itran_note.sqlerrtext
		return false
	end if
end if

return true


end event

event ue_end_drop;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

long					ll_row

if ab_status then
	//commit
	commit using itran_note;
	disconnect using itran_note;
	destroy itran_note;
else
	//rollback
	if isvalid(itran_note) then
		rollback using itran_note;
		disconnect using itran_note;
		destroy itran_note;
	end if
	
	if as_message<>"" and not isnull(as_message) then g_mb.error(as_message)
	
end if

ll_row = tab_dettaglio.det_1.dw_1.getrow()

wf_retrieve_blob(ll_row)

return true
end event

event ue_start_drop;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

integer		li_anno_registrazione, li_cont
long			ll_row, ll_num_registrazione
string			ls_errore


ll_row = tab_dettaglio.det_1.dw_1.getrow()

if isnull(ll_row) or ll_row < 1 then
	as_message = "Non è stata inserita alcuna richiesta oppure è necessario selezionare una richiesta dalla lista!"
	ab_return = false
	return
end if

li_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")

select 	count(*) 
into 		:li_cont
from 		tab_richieste
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :li_anno_registrazione and
			num_registrazione = :ll_num_registrazione;

if li_cont>0 then
else
	as_message = "Prima di associare documenti è necessario salvare la richiesta!"
	ab_return = false
	return
end if


//creo la transazione (di istanza) che sarà usata per il salvataggio dei documenti
if not guo_functions.uof_create_transaction_from( sqlca, itran_note, ls_errore)  then
	as_message = "Errore in creazione transazione~r~n" + ls_errore
	ab_return = false
	return
end if

ab_return = true

return
end event

