﻿$PBExportHeader$w_importa_espositori.srw
forward
global type w_importa_espositori from w_cs_xx_principale
end type
type st_log from statictext within w_importa_espositori
end type
type dw_ricerca from u_dw_search within w_importa_espositori
end type
type dw_1 from datawindow within w_importa_espositori
end type
end forward

global type w_importa_espositori from w_cs_xx_principale
integer width = 3616
integer height = 2428
string title = "Importa Espositori"
st_log st_log
dw_ricerca dw_ricerca
dw_1 dw_1
end type
global w_importa_espositori w_importa_espositori

type variables
private:
	long COLOR_SUCCESS = 11599832
	long COLOR_CURRENT = 16769734
	long COLOR_WARNING = 10944511
	long COLOR_ERROR = 14540287
	
	int PROCESS = 1
	int SUCCESS = 2
	int WARNING = 3
	int CONFLICT = 4
	
	boolean ib_stop_import_file = false
	
	int ii_msg_confirmed = 0
	int ii_current_dw_row = -1
	
	boolean ib_chiedi_se_troncare = true
end variables

forward prototypes
public subroutine wf_importa_file_excel (ref string as_file)
public subroutine wf_abilita_pulsanti (boolean ab_seleziona_file)
public function boolean wf_importa ()
public function boolean wf_controlla_cliente (ref string as_cod_cliente, string as_rag_soc, string as_indirizzo, string as_citta, string as_cap, string as_provincia, string as_piva, string as_flag_tipo_cliente, string as_abbreviazione, string as_telefono)
public function boolean wf_controlla_lunghezza (ref string as_stringa, integer ai_length)
public subroutine wf_stato_riga (integer ai_status)
public function boolean wf_controllla_tipo_cliente (ref string as_flag_tipo_cliente, string as_des_tipo_cliente)
public function boolean wf_controlla_area_aziendale (ref string as_cod_area_aziendale, string as_des_area, string as_cod_divisione)
public function boolean wf_controlla_tipo_richiesta (string as_cod_tipo_richiesta, ref string as_des_tipo_richiesta)
public function boolean wf_calcola_registro (long al_anno_registrazione, long al_num_registrazione, string as_cod_tipo_richiesta, ref string as_cod_documento, ref string as_numeratore_documento, ref long al_num_documento)
public subroutine wf_imposta_importazione ()
public function boolean wf_controlla_richiedente (ref long al_cod_richiedente, string as_cod_cliente, string as_rag_soc, string as_cod_divisione, string as_cod_area_aziendale, string as_particella, decimal ad_metri_quadri)
public function boolean wf_controlla_richiesta (string as_num_pratica, string as_cod_tipo_richiesta, string as_cod_area_aziendale, string as_cod_divisione)
public function string wf_cazzillo (string as_stringa)
public function boolean wf_fix_string (ref string as_string)
end prototypes

public subroutine wf_importa_file_excel (ref string as_file);string 	ls_rag_soc, ls_indirizzo, ls_citta, ls_cap, ls_provincia, ls_piva, ls_cod_tipo_anagrafica, ls_abbreviazione, ls_telefono, &
			 ls_cod_area_aziendale, ls_particella, ls_cod_tipo_richiesta, ls_num_pratica, ls_des_area_aziendale, ls_cod_divisione
long ll_riga_excel, ll_riga_dw
decimal{4} ld_quantita_servizio, ld_quantita_richiesta, ld_limite_mq, ld_valore_aggiuntivo
uo_excel luo_excel

dw_ricerca.accepttext()
ls_cod_divisione = dw_ricerca.getitemstring(1, "cod_divisione")

// nascondo pulsanti
ib_stop_import_file = true
wf_abilita_pulsanti(false)
dw_1.reset()
dw_1.setredraw(false)

ib_stop_import_file = false // semaforo per abilitare il pulsante "Importa"

luo_excel = create uo_excel
if luo_excel.uof_open(as_file, false) then
	
	ll_riga_excel = 0
	do while true
		Yield()
		ll_riga_excel++
		
		st_log.text = "Lettura in corso riga n° "+string(ll_riga_excel)
		
		ls_rag_soc = string(luo_excel.uof_read(ll_riga_excel, "A"))
		if isnull(ls_rag_soc) or ls_rag_soc = "" then
			// ho finito le righe, processo terminato
			st_log.text = "Pronto!"
			exit
		end if
		
		// leggo le righe
		ls_indirizzo = string(luo_excel.uof_read(ll_riga_excel, "B"))
		ls_citta = string(luo_excel.uof_read(ll_riga_excel, "C"))
		ls_cap = string(luo_excel.uof_read(ll_riga_excel, "D"))
		ls_provincia = string(luo_excel.uof_read(ll_riga_excel, "E"))
		ls_piva = string(luo_excel.uof_read(ll_riga_excel, "F"))
		ls_cod_tipo_anagrafica = string(luo_excel.uof_read(ll_riga_excel, "G"))
		ls_abbreviazione = string(luo_excel.uof_read(ll_riga_excel, "H"))
		ls_telefono = string(luo_excel.uof_read(ll_riga_excel, "I"))
		ls_des_area_aziendale = string(luo_excel.uof_read(ll_riga_excel, "J"))
		ls_particella = string(luo_excel.uof_read(ll_riga_excel, "K"))
		ls_cod_tipo_richiesta = string(luo_excel.uof_read(ll_riga_excel, "L"))
		//ld_quantita_servizio = dec(luo_excel.uof_read(ll_riga_excel, "M"))
		ld_limite_mq = dec(luo_excel.uof_read(ll_riga_excel, "M"))
		ld_quantita_richiesta = dec(luo_excel.uof_read(ll_riga_excel, "N"))
		ls_num_pratica = string(luo_excel.uof_read(ll_riga_excel, "O"))
		ld_valore_aggiuntivo =  dec(luo_excel.uof_read(ll_riga_excel, "P"))
		
		// calcolo quota servizio in base ai MQ
		select min(quota_servizio)
		into :ld_quantita_servizio
		from tab_richieste_metrature
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_divisione = :ls_cod_divisione and
			cod_tipo_richiesta = :ls_cod_tipo_richiesta and
			limite_mq >= :ld_limite_mq;
			
		if sqlca.sqlcode < 0 then
			st_log.text = "Errore!"
			g_mb.error("Errore durante il controllo delle metrature.", sqlca)
			return
			
		elseif sqlca.sqlcode = 100 then
			st_log.text = "Errore!"
			g_mb.error("Attenzione la riga " + string(ll_riga_excel) + " del foglio excel non ha un limite mq valido.")
			return
			
		elseif isnull(ld_quantita_servizio) or ld_quantita_servizio < 0 then
			st_log.text = "Errore!"
			g_mb.error("Attenzione la riga "  + string(ll_riga_excel) + " del foglio excel ha un limite mq non impostato all'interno del software.")
			return
			
		end if
		// ---
		
		ll_riga_dw = dw_1.insertrow(0)
		dw_1.setitem(ll_riga_dw, "rag_soc", ls_rag_soc)
		dw_1.setitem(ll_riga_dw, "indirizzo", ls_indirizzo)
		dw_1.setitem(ll_riga_dw, "citta", ls_citta)
		dw_1.setitem(ll_riga_dw, "cap", ls_cap)
		dw_1.setitem(ll_riga_dw, "provincia", ls_provincia)
		dw_1.setitem(ll_riga_dw, "piva", ls_piva)
		dw_1.setitem(ll_riga_dw, "cod_tipo_anagrafica", ls_cod_tipo_anagrafica)
		dw_1.setitem(ll_riga_dw, "abbreviazione", ls_abbreviazione)
		dw_1.setitem(ll_riga_dw, "telefono", ls_telefono)
		dw_1.setitem(ll_riga_dw, "des_area_aziendale", ls_des_area_aziendale)
		dw_1.setitem(ll_riga_dw, "particella", ls_particella)
		dw_1.setitem(ll_riga_dw, "cod_tipo_richiesta", ls_cod_tipo_richiesta)
		dw_1.setitem(ll_riga_dw, "limite_mq", ld_limite_mq)
		dw_1.setitem(ll_riga_dw, "quantita_servizio", ld_quantita_servizio)
		dw_1.setitem(ll_riga_dw, "quantita_richiesta", ld_quantita_richiesta)
		dw_1.setitem(ll_riga_dw, "num_pratica", ls_num_pratica)
		dw_1.setitem(ll_riga_dw, "valore_aggiuntivo", ld_valore_aggiuntivo)
		
	loop
	
else
	st_log.text = "Errore!"
	g_mb.error("Errore durante l'apertura del file Excel. Controllare il percorso e riprovare")
	ib_stop_import_file = true
	
end if

// riabilito il pulsante
wf_abilita_pulsanti(true)
dw_1.setredraw(true)
end subroutine

public subroutine wf_abilita_pulsanti (boolean ab_seleziona_file);//cb_seleziona_file.visible = ab_seleziona_file
//cb_stop.visible = not ab_seleziona_file
//if ab_seleziona_file and not ib_stop_import_file then
//	cb_importa.enabled = true
//else
//	cb_importa.enabled = false
//end if
end subroutine

public function boolean wf_importa ();/**
 * Funzione che importa i dati presenti nella DW dentro il DB
 **/
 
string ls_indirizzo, ls_citta, ls_cap, ls_provincia, ls_piva, ls_cod_tipo_anagrafica, ls_abbreviazione, ls_telefono, ls_des_area_aziendale, ls_particella, &
		ls_cod_tipo_richiesta, ls_num_pratica, ls_rag_soc, ls_cod_cliente, ls_flag_tipo_cliente, ls_cod_area_aziendale, &
		ls_cod_divisione, ls_des_tipo_richiesta, ls_cod_documento, ls_numeratore_documento, ls_des_richiesta
long ll_i, ll_cod_richiedente, ll_anno, ll_num, ll_num_documento, ll_tot
decimal{4} ld_quantita_servizio, ld_quantita_richiesta, ld_limite_mq, ld_valore_aggiuntivo
datetime ldt_today, ldt_time

string ls_temp

ll_anno = f_anno_esercizio()
ldt_today = datetime(today(), 00:00:00)
ldt_time = datetime(date(1900,1,1), now())

ls_cod_divisione = dw_ricerca.getitemstring(1, "cod_divisione")

ll_tot = dw_1.rowcount()
for ll_i = 1 to ll_tot
	
	st_log.text = "Importazione riga "+string(ll_i) + " di "+ string(ll_tot)
	
	ii_current_dw_row = ll_i
	wf_stato_riga(PROCESS)
	
	ls_rag_soc = dw_1.getitemstring(ll_i, "rag_soc")
	ls_indirizzo =dw_1.getitemstring(ll_i, "indirizzo")
	ls_citta = dw_1.getitemstring(ll_i, "citta")
	ls_cap = dw_1.getitemstring(ll_i, "cap")
	ls_provincia = dw_1.getitemstring(ll_i, "provincia")
	ls_piva = dw_1.getitemstring(ll_i, "piva")
	ls_cod_tipo_anagrafica = dw_1.getitemstring(ll_i, "cod_tipo_anagrafica") // flag_tipo_cliente
	ls_abbreviazione =dw_1.getitemstring(ll_i, "abbreviazione")
	ls_telefono = dw_1.getitemstring(ll_i, "telefono")
	ls_des_area_aziendale = dw_1.getitemstring(ll_i, "des_area_aziendale")
	ls_particella =dw_1.getitemstring(ll_i, "particella")
	ls_cod_tipo_richiesta = dw_1.getitemstring(ll_i, "cod_tipo_richiesta")
	ld_limite_mq = dw_1.getitemdecimal(ll_i, "limite_mq")
	ld_quantita_servizio = dw_1.getitemdecimal(ll_i, "quantita_servizio")
	ld_quantita_richiesta =dw_1.getitemdecimal(ll_i, "quantita_richiesta")
	ls_num_pratica = dw_1.getitemstring(ll_i, "num_pratica")
	ld_valore_aggiuntivo = dw_1.getitemdecimal(ll_i, "valore_aggiuntivo")
	
	if ls_num_pratica="750" or ls_num_pratica="217" then
		ls_temp = ""
	end if
	
	// controlli lunghezza campi
	if not wf_controlla_lunghezza(ls_rag_soc, 40) then return false
	if not wf_controlla_lunghezza(ls_indirizzo, 40) then return false
	if not wf_controlla_lunghezza(ls_citta, 30) then return false
	if not wf_controlla_lunghezza(ls_cap, 6) then return false
	if not wf_controlla_lunghezza(ls_provincia, 2) then return false
	if not wf_controlla_lunghezza(ls_piva, 16) then return false
	if not wf_controlla_lunghezza(ls_abbreviazione, 40) then return false
	if not wf_controlla_lunghezza(ls_telefono, 20) then return false
	if not wf_controlla_lunghezza(ls_des_area_aziendale, 40) then return false
	if not wf_controlla_lunghezza(ls_particella, 100) then return false
	if not wf_controlla_lunghezza(ls_cod_tipo_richiesta, 15) then return false
	if not wf_controlla_lunghezza(ls_num_pratica, 100) then return false
	
	wf_fix_string(ls_rag_soc)
	wf_fix_string(ls_indirizzo)
	wf_fix_string(ls_citta)
	wf_fix_string(ls_cap)
	wf_fix_string(ls_provincia)
	wf_fix_string(ls_piva)
	wf_fix_string(ls_abbreviazione)
	wf_fix_string(ls_telefono)
	wf_fix_string(ls_des_area_aziendale)
	wf_fix_string(ls_particella)
	wf_fix_string(ls_cod_tipo_richiesta)
	wf_fix_string(ls_num_pratica)
	//ls_rag_soc = wf_cazzillo(ls_rag_soc)
//	ls_indirizzo = wf_cazzillo(ls_indirizzo)
//	ls_citta = wf_cazzillo(ls_citta)
//	ls_cap = wf_cazzillo(ls_cap)
//	ls_provincia = wf_cazzillo(ls_provincia)
//	ls_piva = wf_cazzillo(ls_piva)
//	ls_abbreviazione = wf_cazzillo(ls_abbreviazione)
//	ls_telefono = wf_cazzillo(ls_telefono)
//	ls_des_area_aziendale = wf_cazzillo(ls_des_area_aziendale)
//	ls_particella = wf_cazzillo(ls_particella)
//	ls_telefono = wf_cazzillo(ls_telefono)
	
	// controlli
	if not wf_controlla_tipo_richiesta(ls_cod_tipo_richiesta, ls_des_tipo_richiesta) then return false
	if not wf_controlla_area_aziendale(ls_cod_area_aziendale, ls_des_area_aziendale, ls_cod_divisione) then return false
	
	if not wf_controlla_richiesta(ls_num_pratica, ls_cod_tipo_richiesta, ls_cod_area_aziendale, ls_cod_divisione) then return false
	
	if not wf_controllla_tipo_cliente(ls_flag_tipo_cliente, ls_cod_tipo_anagrafica) then return false
	if not wf_controlla_cliente(ls_cod_cliente, ls_rag_soc, ls_indirizzo, ls_citta, ls_cap, ls_provincia, ls_piva, ls_flag_tipo_cliente, ls_abbreviazione, ls_telefono) then return false
	
	if not wf_controlla_richiedente(ll_cod_richiedente, ls_cod_cliente, ls_rag_soc, ls_cod_divisione, ls_cod_area_aziendale, ls_particella, ld_limite_mq) then return false
	
	// creo richiesta
	select max(num_registrazione)
	into :ll_num
	from tab_richieste
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il calcolo del progressivo della richiesta.", sqlca)
		return false
	elseif sqlca.sqlcode = 100 or isnull(ll_num) then
		ll_num = 1
	else
		ll_num++
	end if
	
	// registro
	if not wf_calcola_registro(ll_anno, ll_num, ls_cod_tipo_richiesta, ls_cod_documento, ls_numeratore_documento, ll_num_documento) then return false
	
	// descrizione richiesta
	ls_des_richiesta = ls_des_tipo_richiesta
	if not isnull(ld_quantita_servizio) and ld_quantita_servizio > 0 then ls_des_richiesta += "~r~nQuota Base: " + string(ld_quantita_servizio, "###,###,##0.00##")
	if not isnull(ld_quantita_richiesta) and ld_quantita_richiesta > 0 then ls_des_richiesta += "~r~nQuota Richiesta: " + string(ld_quantita_richiesta, "###,###,##0.00##")
	//if not isnull(ls_num_pratica) and ls_num_pratica <> "" then ls_des_richiesta += "~r~nNum. Pratica originale: " + ls_num_pratica
	
	insert into tab_richieste(
		cod_azienda, 
		anno_registrazione,
		num_registrazione,
		cod_tipo_richiesta,
		cod_divisione,
		cod_cliente,
		des_richiesta,
		cod_richiedente,
		cod_area_aziendale,
		quota_servizio,
		quota_richiesta,
		num_pratica,
		data_registrazione,
		ora_registrazione,
		cod_documento,
		anno_documento,
		numeratore_documento,
		num_documento,
		valore_aggiuntivo
	) values (
		:s_cs_xx.cod_azienda,
		:ll_anno,
		:ll_num,
		:ls_cod_tipo_richiesta,
		:ls_cod_divisione,
		:ls_cod_cliente,
		:ls_des_richiesta,
		:ll_cod_richiedente,
		:ls_cod_area_aziendale,
		:ld_quantita_servizio,
		:ld_quantita_richiesta,
		:ls_num_pratica,
		:ldt_today,
		:ldt_time,
		:ls_cod_documento,
		:ll_anno,
		:ls_numeratore_documento,
		:ll_num_documento,
		:ld_valore_aggiuntivo
		);
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante la creazione della richiesta.", sqlca)
		return false
	end if
		
	wf_stato_riga(SUCCESS)
	
next


 
return true
end function

public function boolean wf_controlla_cliente (ref string as_cod_cliente, string as_rag_soc, string as_indirizzo, string as_citta, string as_cap, string as_provincia, string as_piva, string as_flag_tipo_cliente, string as_abbreviazione, string as_telefono);/**
 * Controllo se esiste il cliente in tabella usando i valori di P.IVA
 * Nel caso il cliente esista la funzione ritorna il codice, altrimeni prima provvede all'inserimento
 * e poi ritorna il codice.
 **/
 
string ls_cod_fiscale, ls_sql
long ll_row
datastore lds_store

//if isnull(as_piva) or as_piva = "" then
	//g_mb.error("La partita iva o il codice fiscale sono campi obbligatori.~r~nControllare foglio excel e riprovare.")
	//return false
	
	// controllo ragione sociale
	ls_sql = "select cod_cliente from anag_clienti where cod_azienda='" + s_cs_xx.cod_azienda + "' and upper(rag_soc_1)='" + upper(as_rag_soc) + "'"
	
	if not f_crea_datastore(lds_store, ls_sql) then return false

	lds_store.retrieve()
	if lds_store.rowcount() > 0 then
		as_cod_cliente = lds_store.getitemstring(1, 1)
		return true
	end if
//else
//	// Controllo l'esistenza
//	select cod_cliente
//	into :as_cod_cliente
//	from anag_clienti
//	where
//		cod_azienda = :s_cs_xx.cod_azienda and
//		(cod_fiscale = :as_piva or partita_iva = :as_piva);
//		
//	if sqlca.sqlcode < 0 then
//		g_mb.error("Errore durante il controllo del cliente.")
//		return false
//	elseif sqlca.sqlcode = 0 and not isnull(as_cod_cliente) and as_cod_cliente <> "" then
//		return true
//	end if
//end if


	
// calcolo progressivo
select max(cod_cliente)
into :as_cod_cliente
from anag_clienti
where
	cod_azienda=:s_cs_xx.cod_azienda and
	IsNumeric(cod_cliente) = 1;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il calcolo del progressivo del nuovo cliente.")
	return false
elseif sqlca.sqlcode = 100 or isnull(as_cod_cliente) then
	as_cod_cliente = "000001"
else
	as_cod_cliente = right("000000" + string( long(as_cod_cliente) + 1 ), 6)
end if

ls_cod_fiscale = as_piva
as_piva = left(as_piva, 11)
	
// inserisco
insert into anag_clienti (
	cod_azienda,
	cod_cliente,
	rag_soc_1,
	indirizzo,
	localita,
	cap,
	provincia,
	partita_iva,
	cod_fiscale,
	telefono,
	rag_soc_abbreviata,
	flag_tipo_cliente
) values (
	:s_cs_xx.cod_azienda,
	:as_cod_cliente,
	:as_rag_soc,
	:as_indirizzo,
	:as_citta,
	:as_cap,
	:as_provincia,
	:as_piva,
	:ls_cod_fiscale,
	:as_telefono,
	:as_abbreviazione,
	:as_flag_tipo_cliente);
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante l'inserimento del nuovo cliente (" + as_cod_cliente + ")", sqlca)
	return false
end if

return true
end function

public function boolean wf_controlla_lunghezza (ref string as_stringa, integer ai_length);/**
 * Controlla se la lunghezza della stringa è maggiore di quella indicata.
 * In caso sia superiore avvisa l'utente si procedere con il processo o interrompere.
 * La richiesta viene mostrata 3 volte e poi diventa automatica di continuare il processo.
 **/
 
string ls_msg_text

as_stringa = trim(as_stringa, true)

if len(as_stringa) > ai_length then
	
	// TODO mettere il conrollo che se ii_msg_confirmed > 4 fa in automatico il left e non mostra più la msgbox
	
	wf_stato_riga(WARNING)
	
	ls_msg_text = "La descrizione " + as_stringa + " è superiore del limite,~r~n"
	ls_msg_text += "verrà troncata a " + left(as_stringa, ai_length) + ".~r~n"
	ls_msg_text += "Procedo con il processo?"
	
	if not ib_chiedi_se_troncare then
		//non chiedere più, tronca e basta
		as_stringa = left(as_stringa, ai_length)
		ii_msg_confirmed++
		wf_stato_riga(PROCESS)
		return true
	else
		//chiedi prima
		if g_mb.confirm(ls_msg_text) then
			as_stringa = left(as_stringa, ai_length)
			ii_msg_confirmed++
			wf_stato_riga(PROCESS)
			
			//chiedi se ignorare i prossimi alerts
			if  g_mb.confirm("Ignorare gli eventuali successivi messaggi di troncamento?") then
				ib_chiedi_se_troncare = false
			end if
			
			return true
		else
			return false
		end if
	end if
	
//	if g_mb.confirm(ls_msg_text) then
//		as_stringa = left(as_stringa, ai_length)
//		ii_msg_confirmed++
//		wf_stato_riga(PROCESS)
//		return true
//	else
//		return false
//	end if
	
end if

return true
end function

public subroutine wf_stato_riga (integer ai_status);/** 
* Imposta il colore della riga
**/

long ll_color

choose case ai_status
		
	case 1
		ll_color = COLOR_CURRENT
		
	case 2
		ll_color = COLOR_SUCCESS
		
	case 3
		ll_color = COLOR_WARNING
		
	case 4
		ll_color = COLOR_ERROR
		
	case else
		ll_color = 16777215
		
		
end choose

dw_1.setitem(ii_current_dw_row, "colore", ll_color)
dw_1.setredraw(true)
dw_1.scrolltorow(ii_current_dw_row)
end subroutine

public function boolean wf_controllla_tipo_cliente (ref string as_flag_tipo_cliente, string as_des_tipo_cliente);/**
 * Controlla il tipo di cleinte.
 * Da specifica il foglio excel può avere valori
 * Ita, Eu, Est
 **/
 
as_des_tipo_cliente = lower(trim(as_des_tipo_cliente, true))

choose case as_des_tipo_cliente
		
	case "ita"
		as_flag_tipo_cliente = "S"
		
	case "eu"
		as_flag_tipo_cliente = "C"
		
	case "est"
		as_flag_tipo_cliente = "E"
		
	case else
		setnull(as_flag_tipo_cliente)

end choose

return true
end function

public function boolean wf_controlla_area_aziendale (ref string as_cod_area_aziendale, string as_des_area, string as_cod_divisione);/**
 * Controlla se eiste l'area aziendale e se necessario inserisce
 **/
 

as_des_area = trim(as_des_area, true)

select cod_area_aziendale
into :as_cod_area_aziendale
from tab_aree_aziendali
where
	cod_azienda = :s_cs_xx.cod_azienda and
	des_area = :as_des_area and
	cod_divisione = :as_cod_divisione;
	
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il controllo dell'area aziendale.", sqlca)
	return false
	
	
elseif sqlca.sqlcode = 100 then
	
	// non esiste, da creare
	select max(right("000000" + cod_area_aziendale, 6))
	into :as_cod_area_aziendale
	from tab_aree_aziendali
	where
		cod_azienda=:s_cs_xx.cod_azienda and
		IsNumeric(right("000000" + cod_area_aziendale, 6)) = 1;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il calcolo del progressivo della nuova area aziendale.")
		return false
	elseif sqlca.sqlcode = 100 or isnull(as_cod_area_aziendale) then
		as_cod_area_aziendale = "000001"
	else
		as_cod_area_aziendale = right("000000" + string( long(as_cod_area_aziendale) + 1 ), 6)
	end if
	
	insert into tab_aree_aziendali (
		cod_azienda,
		cod_area_aziendale,
		des_area,
		cod_divisione
	) values (
		:s_cs_xx.cod_azienda,
		:as_cod_area_aziendale,
		:as_des_area,
		:as_cod_divisione);
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante l'inserimento dell'area aziendale.", sqlca)
		return false
	else
		return true
	end if
	
elseif sqlca.sqlcode = 0 and not isnull(as_cod_area_aziendale) and as_cod_area_aziendale <> "" then
	return true
	
else
	return false
	
end if
end function

public function boolean wf_controlla_tipo_richiesta (string as_cod_tipo_richiesta, ref string as_des_tipo_richiesta);/**
 * Controllo che esista il tipo richiesta all'interno del databse
 **/
 
if isnull(as_cod_tipo_richiesta) or as_cod_tipo_richiesta = "" then
	g_mb.error("Attenzione: Tipo Servizio Richiesto è obbligatorio.~r~nControllare il foglio excel.")
	return false
end if

select des_tipo_richiesta
into :as_des_tipo_richiesta
from tab_tipi_richieste
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_richiesta = :as_cod_tipo_richiesta;
	
if sqlca.sqlcode < 0 then 
	g_mb.error("Errore durante il controllo del tipo richiesta.", sqlca)
	return false
elseif sqlca.sqlcode = 100 then
	g_mb.show("Il tipo richiesta " + as_cod_tipo_richiesta + " non è codificato.")
	return false
end if

return true
end function

public function boolean wf_calcola_registro (long al_anno_registrazione, long al_num_registrazione, string as_cod_tipo_richiesta, ref string as_cod_documento, ref string as_numeratore_documento, ref long al_num_documento);/**
 * Funzione che calcola il numero di registro della richiesta
 * !!! COPIATA DA W_CALL_CENTER !!!
 **/
string ls_cod_documento,  ls_numeratore_documento, ls_flag_contatore_giornaliero
int li_giorno_anno
long ll_min, ll_max, ll_num_documento
date ld_inizio
datetime ldt_data_registrazione

ldt_data_registrazione = datetime(today(), now())

select cod_documento, numeratore_documento, flag_contatore_giornaliero
into :ls_cod_documento, :ls_numeratore_documento, :ls_flag_contatore_giornaliero
from tab_tipi_richieste
where 
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_richiesta = :as_cod_tipo_richiesta;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore in ricerca tipo richiesta")
	return false
end if

as_cod_documento = ls_cod_documento
as_numeratore_documento = ls_numeratore_documento

choose case ls_flag_contatore_giornaliero
	case "S"

		ld_inizio = date( "01/01/" + string(al_anno_registrazione,"0000"))
		li_giorno_anno = daysafter(ld_inizio, date(ldt_data_registrazione) ) + 1
		ll_min = (li_giorno_anno * 1000) + 1
		ll_max = (li_giorno_anno + 1) * 1000
		ll_num_documento = 0
		
		select max(num_documento)
		into   :ll_num_documento
		from   tab_richieste
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_documento = :ls_cod_documento and
				 numeratore_documento = :ls_numeratore_documento and
				 anno_documento = :al_anno_registrazione and
				 num_documento >= :ll_min and
				 num_documento <= :ll_max;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore sul db nel calcolo del registro.", sqlca)
			return false
		end if

		if isnull(ll_num_documento) or ll_num_documento = 0 then 
			ll_num_documento = ll_min
		else 
			ll_num_documento++
		end if
		
	case else
		
		select max(num_documento)
		into :ll_num_documento
		from tab_richieste
		where
			cod_azienda = :s_cs_xx.cod_azienda and 
			cod_documento = :ls_cod_documento and
			numeratore_documento = :ls_numeratore_documento and
			anno_documento = :al_anno_registrazione;
				 
		if isnull(ll_num_documento) or ll_num_documento < 1 then
			ll_num_documento = 1
		else
			ll_num_documento ++
		end if

end choose

al_num_documento = ll_num_documento

update numeratori
set num_documento = :ll_num_documento
where
	cod_azienda = :s_cs_xx.cod_azienda and 
	cod_documento = :ls_cod_documento and
	numeratore_documento = :ls_numeratore_documento and
	anno_documento = :al_anno_registrazione;

end function

public subroutine wf_imposta_importazione ();/**
 * Esegue i controlli di validazione prima di lanciare l'importazione vera e propria
 **/
 
string ls_cod_divisione, ls_test

dw_ricerca.accepttext()
ls_cod_divisione = dw_ricerca.getitemstring(1, "cod_divisione")
if isnull(ls_cod_divisione) or ls_cod_divisione = "" then
	g_mb.error("Selezionare una esposizione prima di procedere con l'importazione")
	return
else
	// controllo che il codice esista veramente
	select cod_divisione
	into :ls_test
	from anag_divisioni
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_divisione = :ls_cod_divisione;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il controllo del codice esposizione!", sqlca)
		return
	elseif sqlca.sqlcode = 100 then
		g_mb.error("Il codice esposizione inserito non esiste.")
		return
	end if
end if

if dw_1.rowcount() < 1 then
	g_mb.show("Nessun espositore da importare.")
	return
end if

// inizio importazione vera!
if wf_importa() then
	commit;
	st_log.text = "Importazione terminata con successo!"
	
	g_mb.show("Importazione completata con successo.")
else
	st_log.text = "Importazione terminata con errori!"
	wf_stato_riga(CONFLICT)
	rollback;
end if
end subroutine

public function boolean wf_controlla_richiedente (ref long al_cod_richiedente, string as_cod_cliente, string as_rag_soc, string as_cod_divisione, string as_cod_area_aziendale, string as_particella, decimal ad_metri_quadri);/**
 * Controlla l'esistenza del richiedente
 **/
 
long ll_cod_richiedente

select cod_richiedente 
into :ll_cod_richiedente
from tab_richiedenti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_divisione = :as_cod_divisione and
	cod_cliente = :as_cod_cliente;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il controllo del richiedente.", sqlca)
	return false
elseif sqlca.sqlcode = 100 then
	// inserisco
	select max(cod_richiedente)
	into :ll_cod_richiedente
	from tab_richiedenti
	where cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errure durante il calcolo del progressivo del richiedente.", sqlca)
		return false
	elseif sqlca.sqlcode = 100 or isnull(ll_cod_richiedente) or ll_cod_richiedente < 1 then
		ll_cod_richiedente = 1
	else
		ll_cod_richiedente++
	end if
		
	insert into tab_richiedenti (
		cod_azienda,
		cod_richiedente,
		nominativo,
		flag_blocco,
		cod_divisione,
		cod_area_aziendale,
		cod_cliente,
		particella,
		numero
	) values (
		:s_cs_xx.cod_azienda,
		:ll_cod_richiedente,
		:as_rag_soc,
		'N',
		:as_cod_divisione,
		:as_cod_area_aziendale,
		:as_cod_cliente,
		:as_particella,
		:ad_metri_quadri);
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante l'inserimento del richiedente.", sqlca)
		return false
	else
		al_cod_richiedente = ll_cod_richiedente
		return true
	end if
	
elseif sqlca.sqlcode = 0 and not isnull(ll_cod_richiedente) then
	al_cod_richiedente = ll_cod_richiedente
	return true
else
	return false
end if
end function

public function boolean wf_controlla_richiesta (string as_num_pratica, string as_cod_tipo_richiesta, string as_cod_area_aziendale, string as_cod_divisione);/**
 * Controllo la richiesta
 **/
 
 
long ll_count

if isnull(as_num_pratica) or as_num_pratica = "" then
	g_mb.error("Numero di pratica mancante!")
	return false
end if

select count(anno_registrazione)
into :ll_count
from tab_richieste
where
	cod_azienda = :s_cs_xx.cod_azienda and
	num_pratica = :as_num_pratica and
	cod_tipo_richiesta = :as_cod_tipo_richiesta and
	cod_area_aziendale = :as_cod_area_aziendale and
	cod_divisione = :as_cod_divisione;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il controllo della richiesta", sqlca)
	return false
elseif ll_count > 0 then
	//g_mb.error("Attenzione la richiesta risulta essere già presente all'interno del database (pratica:" + as_num_pratica +", tipo richiesta: " + as_cod_tipo_richiesta+ ", padiglione: "+ as_cod_area_aziendale)
	//return false
	return true
end if
 
 return true
end function

public function string wf_cazzillo (string as_stringa);string ls_old_char = "'", ls_new_char = "~~~'"
long start_pos=1
 
//rimpiazza la virgola con il punto --------PREZZO------
if not isnull(as_stringa) and as_stringa<>"" then
	// Find the first occurrence of old_str.
	start_pos = Pos(as_stringa, ls_old_char, start_pos)

	// Only enter the loop if you find old_str.
	DO WHILE start_pos > 0
		 // Replace old_str with new_str.
		 as_stringa = Replace(as_stringa, start_pos, Len(ls_old_char), ls_new_char)
		 // Find the next occurrence of old_str.
		 start_pos = Pos(as_stringa, ls_old_char, start_pos+Len(ls_new_char))
	LOOP   
end if

return as_stringa
end function

public function boolean wf_fix_string (ref string as_string);/**
 * pulisco la stringa da caratteri non ammessi
 **/
 
guo_functions.uof_replace_string(as_string, "'", "")
 
return true
end function

on w_importa_espositori.create
int iCurrent
call super::create
this.st_log=create st_log
this.dw_ricerca=create dw_ricerca
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_log
this.Control[iCurrent+2]=this.dw_ricerca
this.Control[iCurrent+3]=this.dw_1
end on

on w_importa_espositori.destroy
call super::destroy
destroy(this.st_log)
destroy(this.dw_ricerca)
destroy(this.dw_1)
end on

event resize;/** TOLTO ANCESTOR **/

dw_ricerca.move(20, 20)

dw_1.move(20, dw_ricerca.y + dw_ricerca.height + 30)
dw_1.resize(newwidth - 40, newheight - dw_1.y - 30)
end event

event pc_setwindow;call super::pc_setwindow;string ls_cod_divisione

// imposto automaticamente la fiera corrente
select cod_divisione
into :ls_cod_divisione
from parametri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode = 0 and not isnull(ls_cod_divisione) and ls_cod_divisione <> "" then
	dw_ricerca.setitem(1, "cod_divisione", ls_cod_divisione)
end if
// ----
end event

type st_log from statictext within w_importa_espositori
integer x = 2505
integer y = 156
integer width = 1056
integer height = 160
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 12632256
string text = "Pronto!"
boolean focusrectangle = false
end type

type dw_ricerca from u_dw_search within w_importa_espositori
integer x = 23
integer y = 20
integer width = 2446
integer height = 280
integer taborder = 30
string dataobject = "d_importa_espositori_ricerca"
boolean border = false
end type

event clicked;call super::clicked;choose case dwo.name

	case "b_file"
		string file_path, file_name[]
		int li_rtn
		
		// controllo la divisione che è necessaria per importare il file (per il calcolo dei mq)
		if isnull(getitemstring(1, "cod_divisione")) or getitemstring(1, "cod_divisione") = "" then
			g_mb.show("Selezionare un'esposizione prima di procedere con la selezione del file!")
			setcolumn("cod_divisione")
			return
		end if
		
		setpointer(Hourglass!)
		
		li_rtn = GetFileOpenName("Select File",  file_path, file_name[], "Excel",  "Excel (*.xls; *.xlsx), *.xls; *.xlsx")
		
		if li_rtn = 1 then
			dw_ricerca.setitem(1, "file_path", file_path)
			wf_importa_file_excel(file_path)
		elseif li_rtn = 0 then
			dw_ricerca.setitem(1, "file_path", "")
		end if
		
		dw_1.sort()
		
		setpointer(Arrow!)
		
		
	case "b_divisioni"
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
		s_cs_xx.parametri.parametro_uo_dw_search = dw_ricerca
		s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
		s_cs_xx.parametri.parametro_w_cs_xx_1 = parent
		if not isvalid(w_divisioni_ricerca) then
			window_open(w_divisioni_ricerca, 0)
		end if
		w_divisioni_ricerca.show()


	case "b_importa"
		setpointer(Hourglass!)
		ib_chiedi_se_troncare = true
		
		wf_imposta_importazione()
		
		setpointer(Arrow!)
end choose
end event

type dw_1 from datawindow within w_importa_espositori
integer x = 23
integer y = 360
integer width = 3543
integer height = 1920
integer taborder = 20
string title = "none"
string dataobject = "d_importa_espositori"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

