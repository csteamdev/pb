﻿$PBExportHeader$w_offerte_fornitori.srw
$PBExportComments$Finestra Gestione Parametri Azienda
forward
global type w_offerte_fornitori from w_cs_xx_principale
end type
type dw_list from uo_cs_xx_dw within w_offerte_fornitori
end type
type cb_genera from commandbutton within w_offerte_fornitori
end type
end forward

global type w_offerte_fornitori from w_cs_xx_principale
integer width = 3223
integer height = 1148
string title = ""
boolean center = true
dw_list dw_list
cb_genera cb_genera
end type
global w_offerte_fornitori w_offerte_fornitori

type variables
private:
	string is_cod_divisione, is_cod_cliente
	long il_anno_ric, il_num_ric, il_anno_offerta_gen, il_num_offerta_gen
end variables

forward prototypes
public function integer wf_crea_offerta (long al_anno_off, long al_num_off, ref string fs_errore)
public function integer wf_crea_dettaglio_offerta (long al_anno_off_acq, long al_num_off_acq, long al_anno_off_ven, long al_num_off_ven, ref string as_errore)
end prototypes

public function integer wf_crea_offerta (long al_anno_off, long al_num_off, ref string fs_errore);// funzione di creazione offerta da richiesta
string ls_cod_tipo_richiesta, ls_cod_tipo_off_ven, ls_cod_iva, &
     ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_resa,ls_flag_fuori_fido,ls_flag_riep_boll,ls_flag_riep_fatt, &
     ls_cod_pagamento,ls_cod_agente_1,ls_cod_agente_2,ls_cod_banca_clien_for, ls_cod_operatore, &
	  ls_cod_banca,ls_cod_imballo,ls_cod_vettore,ls_cod_inoltro,ls_cod_mezzo,ls_cod_porto, &
	  ls_cod_deposito,ls_cod_tipo_det_ven, ls_des_tipo_det_ven, ls_des_tipo_richiesta, ls_cod_documento, &
	  ls_flag_doc_suc, ls_flag_st_note, ls_des_prodotto
	  
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven, ll_anno_documento, ll_num_documento

dec{4} ld_sconto, ld_quan_default

datetime ldt_data_registrazione, ldt_data_scadenza, ldt_data_reg_richiesta

// Tipo Offerta Vendita
select  cod_tipo_off_ven
into :ls_cod_tipo_off_ven
from con_off_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	num_registrazione in (select max(num_registrazione) from con_off_ven where cod_azienda=:s_cs_xx.cod_azienda);

if sqlca.sqlcode = 100 then
	fs_errore = "Nel tipo richiesta non è stato impostato il tipo offerta!~r~nImpossibile proseguire."
	return -1
elseif sqlca.sqlcode < 0 then
	fs_errore = "Errore in ricerca tipo richiesta in tabella tipi_richieste~r~n" + sqlca.sqlerrtext
	return -1
end if
// ----
	
// Operatore
setnull(ls_cod_operatore)
select cod_operatore
into :ls_cod_operatore
from tab_operatori_utenti
where cod_azienda = :s_cs_xx.cod_azienda and
	 cod_utente = :s_cs_xx.cod_utente and
	 flag_default = 'S';
	 
if sqlca.sqlcode = -1 then
	fs_errore = "Errore durante l'Estrazione dell'Operatore di Default"
	return -1
end if
// ----
	
ldt_data_registrazione = datetime(today(),00:00:00)
ldt_data_scadenza      = datetime(relativedate(today(), 30),00:00:00)

// Carico campi cliente
select cod_deposito,
		 cod_valuta,
		 cod_tipo_listino_prodotto,
		 cod_pagamento,
		 sconto,
		 cod_agente_1,
		 cod_agente_2,
		 cod_banca_clien_for,
		 cod_banca,
		 cod_imballo,
		 cod_vettore,
		 cod_inoltro,
		 cod_mezzo,
		 cod_porto,
		 cod_resa,
		 flag_fuori_fido,
		 flag_riep_boll,
		 flag_riep_fatt
into   :ls_cod_deposito,
		 :ls_cod_valuta,
		 :ls_cod_tipo_listino_prodotto,
		 :ls_cod_pagamento,
		 :ld_sconto,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_banca_clien_for,
		 :ls_cod_banca,
		 :ls_cod_imballo,
		 :ls_cod_vettore,
		 :ls_cod_inoltro,
		 :ls_cod_mezzo,
		 :ls_cod_porto,
		 :ls_cod_resa,
		 :ls_flag_fuori_fido,
		 :ls_flag_riep_boll,
		 :ls_flag_riep_fatt
from anag_clienti
where
	cod_azienda = :s_cs_xx.cod_azienda and 
	cod_cliente = :is_cod_cliente;
	
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca dati anagrafici del cliente." + sqlca.sqlerrtext
	return -1
end if
// ----

// Prelevo PK
ll_anno_registrazione = f_anno_esercizio()
ll_num_registrazione = 0

select max(num_registrazione)
into   :ll_num_registrazione
from   tes_off_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione;
if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

update con_off_ven
set num_registrazione = :ll_num_registrazione
where  cod_azienda = :s_cs_xx.cod_azienda;
// ----

// salvo campo per messagebox finale
il_anno_offerta_gen = ll_anno_registrazione
il_num_offerta_gen = ll_num_registrazione
// ----

// Creo TESTATA
INSERT INTO tes_off_ven  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  cod_cliente,   
		  data_registrazione,   
		  data_scadenza,   
		  cod_operatore,   
		  cod_tipo_off_ven,   
		  cod_deposito,   
		  cod_ubicazione,   
		  cod_valuta,   
		  cambio_ven,   
		  cod_tipo_listino_prodotto,   
		  cod_pagamento,   
		  sconto,   
		  cod_agente_1,   
		  cod_agente_2,   
		  cod_banca,   
		  num_ric_cliente,   
		  data_ric_cliente,   
		  cod_imballo,   
		  aspetto_beni,   
		  peso_netto,   
		  peso_lordo,   
		  num_colli,   
		  cod_vettore,   
		  cod_inoltro,   
		  cod_mezzo,   
		  causale_trasporto,   
		  cod_porto,   
		  cod_resa,   
		  nota_testata,   
		  nota_piede,   
		  flag_fuori_fido,   
		  flag_blocco,   
		  flag_evasione,   
		  flag_riep_bol,   
		  flag_riep_fat,   
		  tot_val_offerta,   
		  cod_contatto,   
		  data_consegna,   
		  cod_banca_clien_for,   
		  tot_val_offerta_valuta,   
		  tot_merci,   
		  tot_spese_trasporto,   
		  tot_spese_imballo,   
		  tot_spese_bolli,   
		  tot_spese_varie,   
		  tot_sconto_cassa,   
		  tot_sconti_commerciali,   
		  imponibile_provvigioni_1,   
		  imponibile_provvigioni_2,   
		  tot_provvigioni_1,   
		  tot_provvigioni_2,   
		  importo_iva,   
		  imponibile_iva,   
		  importo_iva_valuta,   
		  imponibile_iva_valuta,   
		  flag_doc_suc_tes,   
		  flag_doc_suc_pie,   
		  flag_st_note_tes,   
		  flag_st_note_pie,   
		  num_documento,   
		  num_revisione,   
		  spese_sicurezza )  
VALUES ( :s_cs_xx.cod_azienda,   
		  :ll_anno_registrazione,   
		  :ll_num_registrazione,   
		  :is_cod_cliente,   
		  :ldt_data_registrazione,   
		  :ldt_data_scadenza,   
		  :ls_cod_operatore,   
		  :ls_cod_tipo_off_ven,   
		  :ls_cod_deposito,   
		  null,   
		  :ls_cod_valuta,   
		  1,   
		  :ls_cod_tipo_listino_prodotto,   
		  :ls_cod_pagamento,   
		  :ld_sconto,   
		  :ls_cod_agente_1,   
		  :ls_cod_agente_2,   
		  :ls_cod_banca,   
		  null,   
		  null,   
		  :ls_cod_imballo,   
		  null,   
		  0,   
		  0,   
		  0,   
		  :ls_cod_vettore,   
		  :ls_cod_inoltro,   
		  :ls_cod_mezzo,   
		  null,   
		  :ls_cod_porto,   
		  :ls_cod_resa,   
		  null,   
		  null,   
		  'S',   
		  'N',   
		  'A',   
		  :ls_flag_riep_boll,   
		  :ls_flag_riep_fatt,   
		  0,   
		  null,   
		  :ldt_data_scadenza,   
		  :ls_cod_banca_clien_for,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  'I',   
		  'I',   
		  'I',   
		  'I',   
		  null,   
		  0,   
		  0 )  ;

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la creazione dell'offerta." + sqlca.sqlerrtext
	return -1
end if
// ---

if wf_crea_dettaglio_offerta(al_anno_off, al_num_off, ll_anno_registrazione, ll_num_registrazione, fs_errore) < 0 then
	return -1
end if

// -- Calcolo totale
uo_calcola_documento_euro luo_calcola_documento_euro
luo_calcola_documento_euro = create uo_calcola_documento_euro

if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"off_ven",fs_errore) <> 0 then
	destroy luo_calcola_documento_euro
	return -1
end if

destroy luo_calcola_documento_euro

return 0
	
end function

public function integer wf_crea_dettaglio_offerta (long al_anno_off_acq, long al_num_off_acq, long al_anno_off_ven, long al_num_off_ven, ref string as_errore);string ls_sql, ls_cod_tipo_off_ven, ls_des_prodotto,ls_cod_iva,ls_flag_doc_suc, ls_flag_st_note, &
		ls_cod_tipo_det_ven, ls_cod_misura
decimal ld_quan_ordinata, ld_prezzo_acquisto, ld_sconto_1
long ll_rows, ll_i, ll_prog_riga_off_ven
datetime ldt_data_registrazione, ldt_data_scadenza
datastore lds_store

ls_sql = "SELECT * FROM det_off_acq WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' AND "
ls_sql += "anno_registrazione=" + string(al_anno_off_acq)
ls_sql += " AND num_registrazione=" + string(al_num_off_acq)

if not f_crea_datastore(lds_store, ls_sql) then
	as_errore = "Errore durante la creazione del datastore di dettaglio"
	return -1
end if

ll_rows = lds_store.retrieve()

if ll_rows = 0 then
	return 0
elseif ll_rows < 0 then
	as_errore = "Errore durante la retrieve del dettaglio offerte acquisto"
	return -1
end if

// Tipo Dettaglio Offerta Acquisto
select cod_tipo_off_ven
into :ls_cod_tipo_off_ven
from con_off_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	num_registrazione in (select max(num_registrazione) from con_off_ven where cod_azienda=:s_cs_xx.cod_azienda);
	

select cod_tipo_det_ven
into :ls_cod_tipo_det_ven
from tab_tipi_off_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_off_ven = :ls_cod_tipo_off_ven;

if sqlca.sqlcode = 100 then
	as_errore = "Nel tipo richiesta non è stata impostato il tipo offerta!~r~nImpossibile proseguire."
	return -1
elseif sqlca.sqlcode < 0 then
	as_errore = "Errore in ricerca tipo richiesta in tabella tipi_richieste~r~n" + sqlca.sqlerrtext
	return -1
end if

ll_prog_riga_off_ven = 0

select max(prog_riga_off_ven)
into :ll_prog_riga_off_ven
from det_off_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno_off_ven and
	num_registrazione = :al_num_off_ven;
	
if isnull(ll_prog_riga_off_ven) or ll_prog_riga_off_ven < 1 then
	ll_prog_riga_off_ven = 10
else
	ll_prog_riga_off_ven = ll_prog_riga_off_ven + 10
end if

ldt_data_registrazione = datetime(today(),00:00:00)
ldt_data_scadenza      = datetime(relativedate(today(), 30),00:00:00)
// ----

if isnull(ls_cod_tipo_det_ven) then
	return 0
end if

for ll_i = 1 to ll_rows
	
	ls_des_prodotto = lds_store.getitemstring(ll_i, "des_prodotto")
	ls_cod_misura = lds_store.getitemstring(ll_i, "cod_misura")
	ld_quan_ordinata = lds_store.getitemnumber(ll_i, "quan_ordinata")
	ld_prezzo_acquisto = lds_store.getitemnumber(ll_i, "prezzo_acquisto")
	ld_sconto_1 = lds_store.getitemnumber(ll_i, "sconto_1")
	ls_cod_iva = lds_store.getitemstring(ll_i, "cod_iva")
	//ls_flag_doc_suc = lds_store.getitemstring(ll_i, "flag_doc_suc")
	//ls_flag_st_note = lds_store.getitemstring(ll_i, "flag_st_note")
	
	INSERT INTO det_off_ven  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_off_ven,   
			  cod_prodotto,   
			  cod_tipo_det_ven,   
			  cod_misura,   
			  des_prodotto,   
			  quan_offerta,   
			  prezzo_vendita,   
			  fat_conversione_ven,   
			  sconto_1,   
			  sconto_2,   
			  provvigione_1,   
			  provvigione_2,   
			  val_riga,   
			  cod_iva,   
			  data_consegna,   
			  nota_dettaglio,   
			  sconto_3,   
			  sconto_4,   
			  sconto_5,   
			  sconto_6,   
			  sconto_7,   
			  sconto_8,   
			  sconto_9,   
			  sconto_10,   
			  cod_centro_costo,   
			  cod_versione,   
			  num_confezioni,   
			  num_pezzi_confezione,   
			  num_riga_appartenenza,   
			  flag_gen_commessa,   
			  flag_doc_suc_det,   
			  flag_st_note_det,   
			  quantita_um,   
			  prezzo_um,   
			  imponibile_iva,   
			  imponibile_iva_valuta,   
			  settimana_consegna,   
			  flag_stampa_settimana,   
			  anno_reg_richiesta,   
			  num_reg_richiesta )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :al_anno_off_ven,   
			  :al_num_off_ven,   
			  :ll_prog_riga_off_ven,   
			  null,   
			  :ls_cod_tipo_det_ven,   
			  :ls_cod_misura,   
			  :ls_des_prodotto,   
			  :ld_quan_ordinata,   
			  :ld_prezzo_acquisto,   
			  1,   
			  :ld_sconto_1,   
			  0,   
			  0,   
			  0,   
			  0,   
			  :ls_cod_iva,   
			  :ldt_data_scadenza,   
			  null,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  null,
			  null,
			  0,   
			  0,   
			  0,   
			  'N',   
			  'I',   
			  'I',   
			  0, // quantità um
			  0, //prezzo um   
			  0,   
			  0,   
			  0,   
			  'N',   
			  :il_anno_ric,   
			  :il_num_ric );
		
	if sqlca.sqlcode <> 0 then
		as_errore = "Errore durante la creazione riga rif in offerta." + sqlca.sqlerrtext
		return -1
	end if
	
	ll_prog_riga_off_ven += 10
next

return 1
end function

on w_offerte_fornitori.create
int iCurrent
call super::create
this.dw_list=create dw_list
this.cb_genera=create cb_genera
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_list
this.Control[iCurrent+2]=this.cb_genera
end on

on w_offerte_fornitori.destroy
call super::destroy
destroy(this.dw_list)
destroy(this.cb_genera)
end on

event pc_setwindow;call super::pc_setwindow;il_anno_ric = long(s_cs_xx.parametri.parametro_d_1 )
il_num_ric = long(s_cs_xx.parametri.parametro_d_2)
is_cod_divisione = s_cs_xx.parametri.parametro_s_1

if isnull(il_anno_ric) or il_anno_ric < 1900 or isnull(il_num_ric) or il_num_ric < 1 then
	close(this)
end if

// se non è impostata la commessa blocco il pulsante genera offerta cliente
if isnull(is_cod_divisione) or is_cod_divisione = "" then
	cb_genera.enabled = false
end if

this.title = "Offerte fornitori richiesta " + string(il_anno_ric) + "/" + string(il_num_ric)

set_w_options(c_noenablepopup)
dw_list.set_dw_key("cod_azienda")
dw_list.set_dw_options(sqlca, pcca.null_object, c_default, c_default)
dw_list.ib_proteggi_chiavi = false

dw_list.event pcd_retrieve(wparam, lparam)
end event

event resize;call super::resize;dw_list.move(20,20)

cb_genera.move(newwidth - cb_genera.width - 40, newheight - cb_genera.height - 40)

dw_list.resize(newwidth - 40, cb_genera.y - 40)
end event

type dw_list from uo_cs_xx_dw within w_offerte_fornitori
event ue_calcola_documenti ( )
integer x = 23
integer y = 20
integer width = 3136
integer height = 860
integer taborder = 20
string dataobject = "d_offerte_fornitori"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event ue_calcola_documenti();string ls_messaggio
long ll_rows, ll_i, ll_anno_reg, ll_num_reg
uo_calcola_documento_euro luo_doc

// calcolo i documenti
ll_rows = rowcount()
if ll_rows > 0 then
	luo_doc = create uo_calcola_documento_euro
	
	for ll_i=1 to ll_rows 
	
		ll_anno_reg = getitemnumber(ll_i, "anno_registrazione")
		ll_num_reg = getitemnumber(ll_i, "num_registrazione")
		
		if not isnull(ll_anno_reg) and ll_anno_reg <> 0 and not isnull(ll_num_reg) and ll_num_reg <> 0 then
	
			if luo_doc.uof_calcola_documento(ll_anno_reg,ll_num_reg,"off_acq",ls_messaggio) <> 0 then
				g_mb.messagebox("Apice",ls_messaggio)
				rollback;
				destroy luo_doc
				return 
			else
				commit;	
			end if
	
		end if	
	
	next
	
	destroy luo_doc	
end if
end event

event pcd_retrieve;call super::pcd_retrieve;if retrieve(s_cs_xx.cod_azienda, il_anno_ric, il_num_ric) < 0 then
   pcca.error = c_fatal
end if

postevent("ue_calcola_documenti")

end event

event doubleclicked;call super::doubleclicked;if row > 0 then
	
	if not isvalid(w_det_off_acq) then
		s_cs_xx.parametri.parametro_b_1 = true
		window_open_parm(w_det_off_acq, -1, this)
	end if
	
	w_det_off_acq.show()
	
end if
end event

type cb_genera from commandbutton within w_offerte_fornitori
integer x = 2345
integer y = 912
integer width = 823
integer height = 100
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Genera offerta cliente"
end type

event clicked;string ls_des_cliente, ls_cod_valuta, ls_cod_deposito, ls_cod_pagamento
string ls_error

// doppio controllo per sicurezza
if isnull(is_cod_divisione) or is_cod_divisione = "" then
	g_mb.error("Omnia", "Nessuna commessa assegnata alla richiesta")
	return -1
end if
// ----

// controllo codice cliente della commessa
select cod_cliente
into :is_cod_cliente
from anag_divisioni 
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_divisione = :is_cod_divisione;
	
if sqlca.sqlcode <> 0 or (sqlca.sqlcode = 0 and isnull(is_cod_cliente)) then
	g_mb.show("Omnia", "Associare un cliente alla commessa per poter generare l'offerta")
	return -1
end if
// ----

// prelevo campi cliente
select rag_soc_1, cod_valuta, cod_deposito, cod_pagamento
into :ls_des_cliente, :ls_cod_valuta, :ls_cod_deposito, :ls_cod_pagamento
from anag_clienti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :is_cod_cliente;

if sqlca.sqlcode <> 0 then
	g_mb.error("Omnia", "Errore durante il recuper dati cliente.~r~n" + sqlca.sqlerrtext)
	return -1
end if
// ----

if dw_list.getrow() < 1 then
	g_mb.error("Omnia", "Selezionare un offerta fornitore per generare l'offerta al cliente")
	return -1
end if

// chiedo conferma
if not g_mb.confirm("Omnia", "Si vuole confermare la generazione dell'offerta per il cliente " + ls_des_cliente + "?") then
	return -1
end if
// ----

// TESTATA
long ll_anno_off, ll_num_off

ll_anno_off = dw_list.getitemnumber(dw_list.getrow(), "anno_registrazione")
ll_num_off = dw_list.getitemnumber(dw_list.getrow(), "num_registrazione")


if wf_crea_offerta(ll_anno_off, ll_num_off, ls_error) < 0 then
	rollback;
	g_mb.error("Apice", ls_error)
else
	commit;
	g_mb.show("Apice", "Generazione offerta " + string(il_anno_offerta_gen) + "/" + string(il_num_offerta_gen) + " completata con successo!")
end if

end event

