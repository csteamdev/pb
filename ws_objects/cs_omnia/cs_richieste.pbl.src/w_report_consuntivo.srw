﻿$PBExportHeader$w_report_consuntivo.srw
forward
global type w_report_consuntivo from w_cs_xx_principale
end type
type dw_sel_tipi_richieste from uo_cs_xx_dw within w_report_consuntivo
end type
type dw_sel_manut_eseguite from uo_cs_xx_dw within w_report_consuntivo
end type
type cb_annulla from commandbutton within w_report_consuntivo
end type
type cb_report from commandbutton within w_report_consuntivo
end type
end forward

global type w_report_consuntivo from w_cs_xx_principale
integer width = 2560
integer height = 2476
string title = "Report Consuntivo"
dw_sel_tipi_richieste dw_sel_tipi_richieste
dw_sel_manut_eseguite dw_sel_manut_eseguite
cb_annulla cb_annulla
cb_report cb_report
end type
global w_report_consuntivo w_report_consuntivo

forward prototypes
public function integer wf_report ()
public function integer wf_foglio_excel (datastore fds_dati)
public function string uof_nome_cella (long fi_riga, long fi_colonna)
end prototypes

public function integer wf_report ();long 		ll_riga, ll_tempo_totale, ll_anno_registrazione, ll_num_registrazione, ll_index, ll_i, ll_anno_richiesta, ll_num_richiesta

datetime ldt_da_data, ldt_a_data, ldt_data_registrazione, ldt_data_fine_att_inizio, ldt_data_fine_att_fine, &
         ldt_data_fine_attivita
			
dec{4}	ld_operaio_costo_orario, ld_risorsa_costo_orario, ld_risorsa_costi_aggiuntivi, ld_costo_totale_intervento, ld_costo_ricambi, &
         ld_operaio_ore, ld_operaio_minuti, ld_risorsa_ore, ld_risorsa_minuti, ld_costo_tot_operaio, ld_tot_ore_operaio, ld_tot_minuti_operaio, &
			ld_costo_tot_risorsa, ld_tot_ore_risorsa, ld_tot_minuti_risorsa, ld_ore_risorse, ld_costo_risorse

string 	ls_selezione, ls_operatori, ls_cod_guasto, ls_attr_da, ls_attr_a, ls_tipo_manut, ls_categoria, &
		 	ls_cod_reparto, ls_area, ls_ordinaria, ls_giro_isp, ls_altre, ls_sql, ls_tipi_richiesta_sel, &
		 	ls_flag, ls_flag_scaduta, ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_cognome, ls_nome, ls_tipologia, &
		 	ls_des_attrezzatura, ls_cod_reparto_attrezzatura, ls_des_tipo_manutenzione, ls_cat_attrezzature, ls_des_area_chiamata, &
			ls_cod_operatore, ls_cod_risorsa_esterna, ls_cod_cat_risorse_esterne, ls_divisione, ls_global_service, ls_cod_area_chiamata

datastore lds_dati

dw_sel_manut_eseguite.accepttext()

ls_selezione = ""

// selezione da data a data registrazione

ldt_da_data = dw_sel_manut_eseguite.getitemdatetime(1,"data_da")

ldt_a_data = dw_sel_manut_eseguite.getitemdatetime(1,"data_a")

if not isnull(ldt_da_data) or not isnull(ldt_a_data) then
	
	ls_selezione += "Data registrazione:"
	
end if

if not isnull(ldt_da_data) then
	
	ls_selezione += " dal " + string(ldt_da_data,"dd/mm/yyyy") + " "
	
end if


if not isnull(ldt_a_data) then
	
	ls_selezione += " al " + string(ldt_a_data,"dd/mm/yyyy")
	
end if

if ldt_da_data > ldt_a_data then
	
	g_mb.messagebox("OMNIA","Date di selezione registrazione non coerenti!",exclamation!)
	
	return -1
	
end if

// selezione da data a data fine attività

ldt_data_fine_att_inizio = dw_sel_manut_eseguite.getitemdatetime(1,"data_fine_att_inizio")

ldt_data_fine_att_fine = dw_sel_manut_eseguite.getitemdatetime(1,"data_fine_att_fine")

if not isnull(ldt_data_fine_att_inizio) or not isnull(ldt_data_fine_att_fine) then
	
	if len(ls_selezione) > 0 then ls_selezione += "~r~n"
	
	ls_selezione += "Data fine attività:"
	
end if

if not isnull(ldt_data_fine_att_inizio) then
	
	ls_selezione += " dal " + string(ldt_data_fine_att_inizio,"dd/mm/yyyy") + " "
	
end if


if not isnull(ldt_data_fine_att_fine) then
	
	ls_selezione += " al " + string(ldt_data_fine_att_fine,"dd/mm/yyyy")
	
end if

if ldt_data_fine_att_inizio > ldt_data_fine_att_fine then
	
	g_mb.messagebox("OMNIA","Date di selezione fine attività non coerenti!",exclamation!)
	
	return -1
	
end if

//dw_report_manut_eseguite.object.st_selezione.text = ls_selezione

ls_cod_operatore = dw_sel_manut_eseguite.getitemstring(1,"cod_operaio")
ls_cod_cat_risorse_esterne = dw_sel_manut_eseguite.getitemstring(1,"cod_cat_risorse_esterne")
ls_cod_risorsa_esterna = dw_sel_manut_eseguite.getitemstring(1,"cod_risorsa_esterna")
ls_cod_guasto = dw_sel_manut_eseguite.getitemstring(1,"cod_guasto")
ls_attr_da = dw_sel_manut_eseguite.getitemstring(1,"attrezzatura_da")
ls_attr_a = dw_sel_manut_eseguite.getitemstring(1,"attrezzatura_a")
ls_tipo_manut = dw_sel_manut_eseguite.getitemstring(1,"cod_tipo_manutenzione")
ls_categoria = dw_sel_manut_eseguite.getitemstring(1,"cod_categoria")
ls_cod_reparto = dw_sel_manut_eseguite.getitemstring(1,"cod_reparto")
ls_area = dw_sel_manut_eseguite.getitemstring(1,"cod_area")
ls_divisione = dw_sel_manut_eseguite.getitemstring(1,"cod_divisione")
ls_ordinaria = dw_sel_manut_eseguite.getitemstring(1,"flag_ordinaria")
ls_giro_isp = dw_sel_manut_eseguite.getitemstring(1,"flag_giro_isp")
ls_altre = dw_sel_manut_eseguite.getitemstring(1,"flag_altre")
ls_tipologia = dw_sel_manut_eseguite.getitemstring( 1, "cod_tipo_richiesta")
ll_anno_richiesta = dw_sel_manut_eseguite.getitemnumber( 1, "anno_richiesta")
ll_num_richiesta = dw_sel_manut_eseguite.getitemnumber( 1, "numero_richiesta")

// stefanop 21/07/2010: imposto commessa come campo obbligatorio: ticket 2010/2010
//if isnull(ls_divisione) or ls_divisione = "" then
//	g_mb.show("APICE", "Attenzione: il campo commessa è obbligatorio")
//	return -1
//end if
// ----

//if not isnull(ls_divisione) and ls_divisione <> "" then
//	
//	ls_descrizione = ""
//	select des_divisione
//	into   :ls_descrizione
//	from   anag_divisioni
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_divisione = :ls_divisione;
//			 
//	if isnull(ls_descrizione) then ls_descrizione = ""
//	ls_filtri += "COMMESSA: " + ls_divisione + " " + ls_descrizione + "     "
//	
//end if

//if not isnull(ls_cod_reparto) and ls_cod_reparto <> "" then
//	
//	ls_descrizione = ""
//	select des_reparto
//	into   :ls_descrizione
//	from   anag_reparti
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_reparto = :ls_cod_reparto;
//			 
//	if isnull(ls_descrizione) then ls_descrizione = ""
//	ls_filtri += "UBICAZIONE: " + ls_cod_reparto + " " + ls_descrizione + "     "	
//	
//end if

//if not isnull(ls_area) and ls_area <> "" then
//	
//	ls_descrizione = ""
//	select des_area
//	into   :ls_descrizione
//	from   tab_aree_aziendali
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_area_aziendale = :ls_area;
//			 
//	if isnull(ls_descrizione) then ls_descrizione = ""
//	ls_filtri += "IMPIANTO: " + ls_area + " " + ls_descrizione + "     "		
//	
//end if

//lds_dati.Object.t_filtri.Text = ls_filtri

// *** fine modifica

select flag
into   :ls_global_service
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
		 
if isnull(ls_global_service) or ls_global_service = "" then ls_global_service = "N"		 
		 
declare manut_eseg dynamic cursor for sqlsa;

ls_sql = &
"select manutenzioni.anno_registrazione, " + &
		" manutenzioni.num_registrazione, " + &
		" manutenzioni.data_registrazione, " + &
		" tab_richieste.cod_area_chiamata, " + &
		" manutenzioni.operaio_ore, " + &
		" manutenzioni.operaio_minuti, " + &
		" manutenzioni.risorsa_ore, " + &
		" manutenzioni.risorsa_minuti, " + &		
		" manutenzioni.costo_orario_operaio, " + &
		" manutenzioni.risorsa_costo_orario, " + &		
		" manutenzioni.risorsa_costi_aggiuntivi, " + &
		" manutenzioni.tot_costo_intervento " + &		
"from   manutenzioni, " + &
		" tab_richieste "

if not isnull(ls_cod_operatore) then
	if ls_global_service = "S" then
		ls_sql += ",manutenzioni_fasi_operai "
	end if
end if

if not isnull(ls_cod_risorsa_esterna) then
	if ls_global_service = "S" then
		ls_sql += ",manutenzioni_fasi_risorse "
	end if
end if

// stefanop 21/07/2010: aggiunta tabella anag_attrezzature, COME FACEVA A FUNZIONARE PRIMA??? ticket 2010/210
if not isnull(ls_categoria) or not isnull(ls_cod_reparto) or not isnull(ls_area) or not isnull(ls_divisione) then
	ls_sql += ",anag_attrezzature "
end if
// ----

// aggiunto per nuova gestione richieste su tabella unica enme 17/6/2006
ll_index = 0
ls_tipi_richiesta_sel = ""
for ll_i = 1 to dw_sel_tipi_richieste.rowcount()
	if dw_sel_tipi_richieste.getitemstring(ll_i, "flag_selezione") = "S" then
		
		
		if ls_tipi_richiesta_sel = "" then
			ls_tipi_richiesta_sel += " and ( tab_richieste.cod_tipo_richiesta = '" + dw_sel_tipi_richieste.getitemstring(ll_i, "cod_tipo_richiesta") + "' "
		else
			ls_tipi_richiesta_sel += " or tab_richieste.cod_tipo_richiesta = '" + dw_sel_tipi_richieste.getitemstring(ll_i, "cod_tipo_richiesta") + "' "
		end if
	
	end if
next

if ls_ordinaria = "S" then
	
	if ls_tipi_richiesta_sel = "" then
		ls_tipi_richiesta_sel += " and ( ( manutenzioni.flag_ordinario = 'S' and manutenzioni.flag_risc_giro_isp = 'N' ) "
	else
		ls_tipi_richiesta_sel += " or ( manutenzioni.flag_ordinario = 'S' and manutenzioni.flag_risc_giro_isp = 'N' ) "
	end if
end if

if ls_giro_isp = "S" then
	if ls_tipi_richiesta_sel = "" then
		ls_tipi_richiesta_sel += " and ( manutenzioni.flag_risc_giro_isp = 'S' "
	else
		ls_tipi_richiesta_sel += " or manutenzioni.flag_risc_giro_isp = 'S' "
	end if	
end if

if ls_altre = "S" then
	
	if ls_tipi_richiesta_sel = "" then
		ls_tipi_richiesta_sel += " and ( ( manutenzioni.flag_ordinario = 'N' and manutenzioni.flag_risc_giro_isp = 'N' ) "
	else
		ls_tipi_richiesta_sel += " or  ( manutenzioni.flag_ordinario = 'N' and manutenzioni.flag_risc_giro_isp = 'N' ) "
	end if	
end if

if ls_tipi_richiesta_sel <> "" then
	ls_tipi_richiesta_sel += " ) "
end if

ls_sql += " where  manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			 "        manutenzioni.flag_eseguito = 'S' and " + &
			 "        manutenzioni.flag_straordinario = 'S' and " + &
          "        manutenzioni.anno_reg_richiesta IS NOT NULL and " + &
			 "        manutenzioni.num_reg_richiesta IS NOT NULL and " + &
			 "        manutenzioni.anno_reg_richiesta > 0 and " + &
			 "        manutenzioni.num_reg_richiesta > 0 and " + &
			 "        ( tab_richieste.cod_azienda = manutenzioni.cod_azienda and " + &
			 "          tab_richieste.anno_registrazione = manutenzioni.anno_reg_richiesta and " + &
	       "          tab_richieste.num_registrazione = manutenzioni.num_reg_richiesta and " + &
			 "          tab_richieste.flag_richiesta_straordinaria = 'S' "
			 
// stefanop 21/07/2010: aggiunta tabella anag_attrezzature, COME FACEVA A FUNZIONARE PRIMA??? ticket 2010/210
if not isnull(ls_categoria) or not isnull(ls_cod_reparto) or not isnull(ls_area) or not isnull(ls_divisione) then
	ls_sql += "and manutenzioni.cod_attrezzatura = anag_attrezzature.cod_attrezzatura "
	ls_sql += "and manutenzioni.cod_azienda = anag_attrezzature.cod_azienda "
end if
// ----
			 
if len(ls_tipi_richiesta_sel) > 0 then			 
	ls_sql += ls_tipi_richiesta_sel + " ) " 			 
else
	ls_sql += " ) "
end if

if not isnull(ls_tipologia) and ls_tipologia <> "" then
	ls_sql += " and tab_richieste.cod_tipologia_richiesta = '" + ls_tipologia + "' "
end if

if not isnull(ll_anno_richiesta) and ll_anno_richiesta > 0 then
	ls_sql += " and tab_richieste.anno_registrazione = " + string(ll_anno_richiesta) 
end if

if not isnull(ll_num_richiesta) and ll_num_richiesta > 0 then
	ls_sql += " and tab_richieste.num_registrazione = " + string(ll_num_richiesta)
end if

if not isnull(ls_cod_operatore) then
	if ls_global_service = "S" then
		ls_sql += " and manutenzioni_fasi_operai.cod_azienda = manutenzioni.cod_azienda and " + &
					 " manutenzioni_fasi_operai.anno_registrazione = manutenzioni.anno_registrazione and " + &
					 " manutenzioni_fasi_operai.num_registrazione = manutenzioni.num_registrazione and " + &
					 " manutenzioni_fasi_operi.cod_operaio = '" + ls_cod_operatore + "' "
	else
		ls_sql += " and manutenzioni.cod_operaio = '" + ls_cod_operatore + "' "
	end if
end if

if (not isnull(ls_cod_risorsa_esterna)) or (not isnull(ls_cod_cat_risorse_esterne)) then

	if ls_global_service = "S" then
		
		ls_sql += " and manutenzioni_fasi_risorse.cod_azienda = manutenzioni.cod_azienda and " + &
					 " manutenzioni_fasi_risorse.anno_registrazione = manutenzioni.anno_registrazione and " + &
					 " manutenzioni_fasi_risorse.num_registrazione = manutenzioni.num_registrazione "					 
		if not isnull(ls_cod_risorsa_esterna) then
			ls_sql += " and manutenzioni_fasi_risorse.cod_risorsa_esterna = '" + ls_cod_risorsa_esterna + "' "
		elseif not isnull(ls_cod_cat_risorse_esterne) then
			ls_sql += " and manutenzioni_fasi_risorse.cod_cat_risorse_esterne = '" + ls_cod_cat_risorse_esterne + "' "
		end if		
					 
	else
		if not isnull(ls_cod_risorsa_esterna) then
			ls_sql += " and manutenzioni.cod_risorsa_esterna = '" + ls_cod_risorsa_esterna + "' "
		elseif not isnull(ls_cod_cat_risorse_esterne) then
			ls_sql += " and manutenzioni.cod_cat_risorse_esterne = '" + ls_cod_cat_risorse_esterne + "' "
		end if
	end if
end if
	
if not isnull(ls_cod_guasto)	then
	ls_sql = ls_sql + " and manutenzioni.cod_guasto = '" + ls_cod_guasto + "' "
end if

if not isnull(ldt_da_data) then
	ls_sql += " and manutenzioni.data_registrazione >= '" + string(ldt_da_data,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_a_data) then
	ls_sql += " and manutenzioni.data_registrazione <= '" + string(ldt_a_data,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine_att_inizio) then
	ls_sql += " and manutenzioni.data_fine_intervento >= '" + string(ldt_data_fine_att_inizio,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine_att_fine) then
	ls_sql += " and manutenzioni.data_fine_intervento <= '" + string(ldt_data_fine_att_fine,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ls_attr_da) then
	ls_sql = ls_sql + " and manutenzioni.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_sql = ls_sql + " and manutenzioni.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_tipo_manut) then
	ls_sql = ls_sql + " and manutenzioni.cod_tipo_manutenzione = '" + ls_tipo_manut + "' "
end if

if not isnull(ls_categoria) then
	ls_sql = ls_sql + " and anag_attrezzature.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_cod_reparto) then
	ls_sql = ls_sql + " and anag_attrezzature.cod_reparto = '" + ls_cod_reparto + "' "
end if

if not isnull(ls_area) then
	ls_sql = ls_sql + " and anag_attrezzature.cod_area_aziendale = '" + ls_area + "' "
end if

if not isnull(ls_divisione) then
	ls_sql = ls_sql + " and anag_attrezzature.cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "') "
end if

ls_sql = ls_sql + " order by manutenzioni.data_registrazione"

PREPARE SQLSA FROM :ls_sql ;

open dynamic manut_eseg;

if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore manut_eseg. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

lds_dati = create datastore
lds_dati.dataobject = "d_dati_report_consuntivo"
lds_dati.settransobject( sqlca)

do while true
	
	fetch manut_eseg into  :ll_anno_registrazione,
	     						  :ll_num_registrazione,
	      					  :ldt_data_registrazione,
			                 :ls_cod_area_chiamata,
			                 :ld_operaio_ore,
			                 :ld_operaio_minuti,
								  :ld_risorsa_ore,
								  :ld_risorsa_minuti,
								  :ld_operaio_costo_orario,
								  :ld_risorsa_costo_orario,
								  :ld_risorsa_costi_aggiuntivi,
								  :ld_costo_totale_intervento;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore manut_eseg. Dettaglio = " + sqlca.sqlerrtext)
		close manut_eseg;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		exit		
	end if
	

	select des_area_chiamata
	into   :ls_des_area_chiamata
	from   tab_aree_chiamate
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_area_chiamata = :ls_cod_area_chiamata;
	
	select sum(prezzo_ricambio * quan_utilizzo)
	into   :ld_costo_ricambi
	from   manutenzioni_ricambi 
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 flag_utilizzo = 'S';
	
	if isnull(ld_costo_ricambi) then ld_costo_ricambi = 0
	
	select sum(tot_costo_operaio),
	       sum(tot_ore),
			 sum(tot_minuti)
	into   :ld_costo_tot_operaio,
	       :ld_tot_ore_operaio,
			 :ld_tot_minuti_operaio
	from   manutenzioni_fasi_operai
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
			 
	select sum(tot_costo_risorsa),
	       sum(tot_ore),
			 sum(tot_minuti)
	into   :ld_costo_tot_risorsa,
	       :ld_tot_ore_risorsa,
			 :ld_tot_minuti_risorsa
	from   manutenzioni_fasi_risorse
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;			 
	
	
	if isnull(ld_costo_tot_operaio) then ld_costo_tot_operaio = 0
	if isnull(ld_tot_ore_operaio) then ld_tot_ore_operaio = 0
	if isnull(ld_tot_minuti_operaio) then ld_tot_minuti_operaio = 0
	
	if isnull(ld_costo_tot_risorsa) then ld_costo_tot_risorsa = 0
	if isnull(ld_tot_ore_risorsa) then ld_tot_ore_risorsa = 0
	if isnull(ld_tot_minuti_risorsa) then ld_tot_minuti_risorsa = 0
	
	ld_ore_risorse = ld_tot_ore_operaio + ld_tot_ore_risorsa + (ld_tot_minuti_operaio/60) + (ld_tot_minuti_risorsa/60)
	ld_costo_risorse = ld_costo_tot_operaio + ld_costo_tot_risorsa
	
	ll_riga = lds_dati.insertrow(0)
	
	lds_dati.setitem( ll_riga, "scheda", string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
	lds_dati.setitem(ll_riga, "data_registrazione", ldt_data_registrazione)
	lds_dati.setitem(ll_riga, "reparto", ls_des_area_chiamata)
	lds_dati.setitem(ll_riga, "costo_materiale", ld_costo_ricambi)
	lds_dati.setitem(ll_riga, "ore_lavoro", ld_ore_risorse)
	lds_dati.setitem(ll_riga, "costo_risorse", ld_costo_risorse)
	lds_dati.setitem(ll_riga, "costo_totale", ld_costo_risorse + ld_costo_ricambi)

loop

close manut_eseg;

commit;

if lds_dati.rowcount() > 0 then
	wf_foglio_excel(lds_dati)
else
	g_mb.messagebox( "OMNIA", "Nessun risultato dalla ricerca!")
end if

destroy lds_dati;

return 0
end function

public function integer wf_foglio_excel (datastore fds_dati);
long ll_colonna, ll_riga, ll_errore, ll_controllo, ll_i
string ls_path, ls_rag_soc_azienda, ls_cod_divisione, ls_foglio, ls_title, ls_des_divisione

OLEObject myoleobject

g_mb.messagebox("APICE", "Non cliccare sul foglio di lavoro di excel. ~r~nAttendere il termine.")

// stefanop 21/07/2010: campi dimanici: ticket 2010/210
ls_cod_divisione = dw_sel_manut_eseguite.getitemstring(1, "cod_divisione")

select compiti, des_divisione
into :ls_title, :ls_des_divisione
from anag_divisioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_divisione = :ls_cod_divisione;
	
if sqlca.sqlcode <> 0 or isnull(ls_title) then ls_title = ""
if sqlca.sqlcode <> 0 or isnull(ls_des_divisione) then ls_des_divisione = ""


myoleobject = CREATE OLEObject
ll_colonna = 1
ll_riga = 1

select rag_soc_1 
into :ls_rag_soc_azienda
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 or isnull(ls_rag_soc_azienda) then ls_rag_soc_azienda = ""

select  stringa
into :ls_path
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
cod_parametro = 'LO1' and
flag_parametro = 'S'; 

ll_errore = myoleobject.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	g_mb.messagebox("APICE","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return 0
end if

ls_foglio = "foglio1"
myoleobject.Visible = True
myoleobject.Workbooks.Add
myoleobject.sheets(ls_foglio).PageSetup.PrintGridlines = True
myoleobject.sheets(ls_foglio).PageSetup.orientation = 2 
/// COSì è ORIZZONTALE 1 X VERTICALE
myoleobject.sheets(ls_foglio).PageSetup.leftmargin = 20
myoleobject.sheets(ls_foglio).PageSetup.rightmargin = 10
myoleobject.sheets(ls_foglio).PageSetup.topmargin = 20
myoleobject.sheets(ls_foglio).PageSetup.bottommargin = 20
myoleobject.sheets(ls_foglio).PageSetup.FooterMargin = 5
myoleobject.sheets(ls_foglio).PageSetup.CenterHorizontally = True


ls_path = s_cs_xx.volume + ls_path
ll_controllo = FileLength(ls_path)
if ll_controllo > 0 then
	myoleobject.worksheets(ls_foglio).Shapes.AddPicture(ls_path, True, True, 15, 20, 610, 54)
else
	g_mb.messagebox("APICE","Il logo non è presente, impostare correttamente il parametro 'LO1'!")
end if


ll_riga = 8
///INSERISCO IL LOGO E FORMATTO IL FOGLIO
myoleobject.worksheets(ls_foglio).PageSetup.CenterFooter = "&P di &N"
myoleobject.sheets(ls_foglio).cells(8, ll_colonna).value = "Oggetto:"
//myoleobject.sheets(ls_foglio).cells(8, ll_colonna +1).wraptext = true
myoleobject.worksheets(ls_foglio).rows(ll_riga).HorizontalAlignment = "3"
myoleobject.sheets(ls_foglio).rows(ll_riga).rowHeight = "50"
myoleobject.worksheets(ls_foglio).rows(ll_riga).VerticalAlignment = "1"
myoleobject.sheets(ls_foglio).range(uof_nome_cella(ll_riga, ll_colonna + 1)+":"+uof_nome_cella(ll_riga, ll_colonna + 6)).merge(true)
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna +1).wraptext = true
//myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna +1).value = "Fornitura di servizi a gestione integrata e di manutenzione ordinaria e straordinaria del sistema edificio-impianto costituenti l'ospedale di Bassano del Grappa con fornitura di energia, complementi edili ed impiantistici.~n Contratto Rep. N° 235 del 22/03/2002"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna +1).value = ls_title
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna +1).value ="LISTA N°LE             ME: MATERIALI E MANODOPERA IMPIANTI MECCANICI MESE DI "
ll_riga = 11

//// formatto le celle di intestazione e el relative colonne
//
//myoleobject.sheets(ls_foglio).range(uof_nome_cella(ll_riga, ll_colonna )+":"+uof_nome_cella(ll_riga, ll_colonna + 6)).columnwidth = "15"
myoleobject.sheets(ls_foglio).range(uof_nome_cella(ll_riga, ll_colonna )+":"+uof_nome_cella(ll_riga, ll_colonna + 6)).wraptext = true
myoleobject.sheets(ls_foglio).range(uof_nome_cella(ll_riga, ll_colonna )+":"+uof_nome_cella(ll_riga, ll_colonna + 6)).borders.Weight = "3"

myoleobject.worksheets(ls_foglio).rows(ll_riga).HorizontalAlignment = "3"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = "N° Richiesta"
myoleobject.sheets(ls_foglio).columns(ll_colonna).columnwidth = "15"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = "Data intervento "
myoleobject.sheets(ls_foglio).columns(ll_colonna + 1).columnwidth = "10"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 2).value = "Reparto"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 2).columnwidth = "35"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 3).value = "Costo materiale[€] "
myoleobject.sheets(ls_foglio).columns(ll_colonna + 3).numberformat="#.##0,00"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 3).columnwidth = "13"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 4).value = "Ore di lavoro"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 4).numberformat="#.##0,0000"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 4).columnwidth = "13"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 5).value = "Costo ore lavoro "
myoleobject.sheets(ls_foglio).columns(ll_colonna + 5).columnwidth = "15"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 5).numberformat="#.##0,00"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 6).value = "Costo intervento totale [€] "
myoleobject.sheets(ls_foglio).columns(ll_colonna + 6).columnwidth = "15"
myoleobject.sheets(ls_foglio).columns(ll_colonna + 6).numberformat="#.##0,00"

myoleobject.worksheets(ls_foglio).rows(ll_riga).HorizontalAlignment = "1"
myoleobject.worksheets(ls_foglio).rows(ll_riga).VerticalAlignment = "3"

//// inserisco il dettaglio delle righe 
ll_riga++
//ll_prima_riga = ll_riga
for ll_i = 1 to fds_dati.rowcount()
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna).value = fds_dati.getitemstring(ll_i, "scheda")
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).value = fds_dati.getitemdatetime(ll_i, "data_registrazione")
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 2).value = fds_dati.getitemstring(ll_i, "reparto")
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 3).value = fds_dati.getitemnumber(ll_i, "costo_materiale")
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 4).value = fds_dati.getitemnumber(ll_i, "ore_lavoro")
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 5).value = fds_dati.getitemnumber(ll_i, "costo_risorse")
	myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 6).value = fds_dati.getitemnumber(ll_i, "costo_totale")
	ll_riga++
next

ll_riga++
ll_riga++
//myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna ).value = "Responsabile Servizio Ati Guerrato "
//myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna +5 ).value = "Responsabile Servizio ASL "
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna ).value = "Responsabile Servizio " + ls_rag_soc_azienda
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna +5 ).value = "Responsabile Servizio " + ls_des_divisione
ll_riga++
ll_riga++
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna ).borders(3).Weight = "3"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 1).borders(3).Weight = "3"

myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 5).borders(3).Weight = "3"
myoleobject.sheets(ls_foglio).cells(ll_riga, ll_colonna + 6).borders(3).Weight = "3"

g_mb.messagebox("APICE","OK! report terminato.")

return 0
end function

public function string uof_nome_cella (long fi_riga, long fi_colonna);string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if fi_colonna > 26 then
	ll_resto = mod(fi_colonna, 26)
	ll_i = long(fi_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	fi_colonna = ll_resto 
end if

if fi_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[fi_colonna] + string(fi_riga)
end if	

return ls_char
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

dw_sel_manut_eseguite.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

windowobject l_objects[ ], l_vuoto[]


dw_sel_manut_eseguite.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"

dw_sel_tipi_richieste.settransobject(sqlca)
dw_sel_tipi_richieste.retrieve(s_cs_xx.cod_azienda)

end event

on w_report_consuntivo.create
int iCurrent
call super::create
this.dw_sel_tipi_richieste=create dw_sel_tipi_richieste
this.dw_sel_manut_eseguite=create dw_sel_manut_eseguite
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sel_tipi_richieste
this.Control[iCurrent+2]=this.dw_sel_manut_eseguite
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_report
end on

on w_report_consuntivo.destroy
call super::destroy
destroy(this.dw_sel_tipi_richieste)
destroy(this.dw_sel_manut_eseguite)
destroy(this.cb_annulla)
destroy(this.cb_report)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_manut_eseguite,"cod_guasto",sqlca,"tab_guasti","cod_guasto","des_guasto",&
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " cognome ASC, nome ASC ")
					  
f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_categoria",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N' ")
					  
f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_area",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")
					  
f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_sel_manut_eseguite,"cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")				
					  
f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_tipo_richiesta",sqlca,&
                 "tab_richieste_tipologie","cod_tipologia_richiesta","des_tipologia_richiesta",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  					  


end event

type dw_sel_tipi_richieste from uo_cs_xx_dw within w_report_consuntivo
integer x = 46
integer y = 1440
integer width = 1623
integer height = 684
integer taborder = 80
string title = "none"
string dataobject = "d_selezione_richieste_report"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;resetupdate()
end event

type dw_sel_manut_eseguite from uo_cs_xx_dw within w_report_consuntivo
event ue_cod_attr ( )
event ue_carica_tipologie ( )
event ue_aree_aziendali ( )
integer x = 46
integer y = 120
integer width = 2377
integer height = 1316
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_sel_report_consuntivo"
boolean border = false
end type

event ue_cod_attr();if not isnull(getitemstring(getrow(),"attrezzatura_da")) then
	setitem(getrow(),"attrezzatura_a",getitemstring(getrow(),"attrezzatura_da"))
end if
end event

event ue_carica_tipologie();string ls_where, ls_null, ls_reparto, ls_categoria, ls_area, ls_attr_da, ls_attr_a, ls_divisione


ls_attr_da = getitemstring(getrow(),"attrezzatura_da")

ls_attr_a = getitemstring(getrow(),"attrezzatura_a")

ls_reparto = getitemstring(getrow(),"cod_reparto")

ls_categoria = getitemstring(getrow(),"cod_categoria")

ls_area = getitemstring(getrow(),"cod_area")

ls_divisione = getitemstring(getrow(), "cod_divisione")

setnull(ls_null)

setitem(getrow(),"cod_tipo_manutenzione",ls_null)

ls_where = "a.cod_azienda = b.cod_azienda and a.cod_attrezzatura = b.cod_attrezzatura and " + &
			  "a.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_attr_da) then
	ls_where += " and a.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_where += " and a.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_reparto) then
	ls_where += " and b.cod_reparto = '" + ls_reparto + "' "
end if

if not isnull(ls_categoria) then
	ls_where += " and b.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_area) then
	ls_where += " and b.cod_area_aziendale = '" + ls_area + "' "
end if

if not isnull(ls_divisione) then
	ls_where += " and b.cod_area_aziendale IN ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ) "
end if

f_po_loaddddw_dw(this,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione a,anag_attrezzature b", &
					  "a.cod_tipo_manutenzione", "a.des_tipo_manutenzione",ls_where)
end event

event ue_aree_aziendali();string ls_divisione

ls_divisione = getitemstring(getrow(),"cod_divisione")

if not isnull(ls_divisione) then	
	f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_area",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ", " ordinamento ASC ")					  		
else
	f_PO_LoadDDDW_sort (dw_sel_manut_eseguite,"cod_area",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")
					  	
end if

triggerevent("ue_carica_tipologie")
end event

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

choose case i_colname
		
	case "attrezzatura_da"
		
		postevent("ue_cod_attr")
		
		postevent("ue_carica_tipologie")
		
	case "attrezzatura_a","cod_reparto","cod_categoria","cod_area"
		
		postevent("ue_carica_tipologie")
		
	case "cod_cat_risorse_esterne"
		
		this.setitem( row, "cod_risorsa_esterna", ls_null)
		
		f_PO_LoadDDDW_DW(dw_sel_manut_eseguite,"cod_risorsa_esterna",sqlca,&
							  "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + i_coltext + "' ")

	case "cod_divisione"
		
		postevent("ue_aree_aziendali")
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_manut_eseguite,"attrezzatura_da")
	case "b_ricerca_att_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_manut_eseguite,"attrezzatura_a")
end choose
end event

type cb_annulla from commandbutton within w_report_consuntivo
integer x = 2057
integer y = 2060
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string 	ls_null

datetime ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_sel_manut_eseguite.setitem(1,"data_da",ldt_null)

dw_sel_manut_eseguite.setitem(1,"data_a",ldt_null)

dw_sel_manut_eseguite.setitem(1,"cod_operaio",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_guasto",ls_null)

dw_sel_manut_eseguite.setitem(1,"attrezzatura_da",ls_null)

dw_sel_manut_eseguite.setitem(1,"attrezzatura_a",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_tipo_manutenzione",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_categoria",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_reparto",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_area",ls_null)

dw_sel_manut_eseguite.setitem(1,"cod_divisione",ls_null)

dw_sel_manut_eseguite.setitem(1,"flag_ordinaria","S")

dw_sel_manut_eseguite.setitem(1,"flag_ric_car","N")

dw_sel_manut_eseguite.setitem(1,"flag_ric_tel","N")

dw_sel_manut_eseguite.setitem(1,"flag_giro_isp","N")

dw_sel_manut_eseguite.setitem(1,"flag_altre","N")

f_PO_LoadDDDW_DW (dw_sel_manut_eseguite,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_report from commandbutton within w_report_consuntivo
integer x = 2057
integer y = 1960
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;wf_report()
end event

