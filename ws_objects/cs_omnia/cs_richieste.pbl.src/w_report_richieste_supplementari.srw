﻿$PBExportHeader$w_report_richieste_supplementari.srw
forward
global type w_report_richieste_supplementari from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_richieste_supplementari
end type
type dw_folder from u_folder within w_report_richieste_supplementari
end type
type dw_ricerca from uo_cs_xx_dw within w_report_richieste_supplementari
end type
end forward

global type w_report_richieste_supplementari from w_cs_xx_principale
integer width = 3936
integer height = 2424
string title = "Riepilogo Servizi Supplementari"
dw_report dw_report
dw_folder dw_folder
dw_ricerca dw_ricerca
end type
global w_report_richieste_supplementari w_report_richieste_supplementari

type variables
private:
	string is_sql_where
end variables

forward prototypes
public function integer wf_report (ref string fs_errore)
public subroutine wf_annulla ()
public function integer wf_leggi_servizi_suppl_richiedenti (string fs_cod_divisione, string fs_cod_tipo_richiesta, string fs_des_divisione, string fs_des_tipo_richiesta, decimal fd_qta_richiesta, ref string fs_errore)
public function integer wf_leggi_servizi_suppl (string fs_cod_divisione, string fs_cod_tipo_richiesta, string fs_des_divisione, string fs_des_tipo_richiesta, decimal fd_qta_richiesta, ref string fs_errore)
end prototypes

public function integer wf_report (ref string fs_errore);datetime		ldt_data_dal, ldt_data_al
string			ls_cod_divisione, ls_cod_tipo_richiesta, ls_sql, ls_groupby, ls_tipo_report
string			ls_cod_divisione_cu, ls_des_divisione_cu, ls_cod_tipo_richiesta_cu, ls_des_tipo_richiesta_cu
decimal		ld_quota_richiesta
long			ll_new
int				li_ret


ls_tipo_report =  dw_ricerca.getitemstring(1, "flag_tipo_report")
dw_ricerca.object.log_t.text = "Preparazione query in corso ..."

choose case ls_tipo_report
	case "C"
		dw_report.dataobject = "d_report_richieste_suppl"
		
	case "R"
		dw_report.dataobject = "d_report_richieste_suppl_richiedenti"
		
end choose

is_sql_where = ""
ls_sql = 	"select tab_richieste.cod_divisione, " + &
				"anag_divisioni.des_divisione," + &
				"tab_richieste.cod_tipo_richiesta," + &
				"tab_tipi_richieste.des_tipo_richiesta," + &
				"sum(isnull(tab_richieste.quota_richiesta, 0)) as quota_richiesta " + &
			"from tab_richieste " + &
			"join anag_divisioni on anag_divisioni.cod_azienda=tab_richieste.cod_azienda and " + &
										"anag_divisioni.cod_divisione = tab_richieste.cod_divisione " + &
			"join tab_tipi_richieste on tab_tipi_richieste.cod_azienda=tab_richieste.cod_azienda and " + &
			"tab_tipi_richieste.cod_tipo_richiesta=tab_richieste.cod_tipo_richiesta "
			
ls_sql += "where tab_richieste.cod_azienda = '"+s_cs_xx.cod_azienda+"' "

ldt_data_dal = dw_ricerca.getitemdatetime(1, "data_reg_da")
if not isnull(ldt_data_dal) and year(date(ldt_data_dal)) > 1950 then
	is_sql_where += " and tab_richieste.data_registrazione >= '"+string(ldt_data_dal, s_cs_xx.db_funzioni.formato_data)+"' "
end if

ldt_data_al = dw_ricerca.getitemdatetime(1, "data_reg_a")
if not isnull(ldt_data_al) and year(date(ldt_data_al)) > 1950 then
	is_sql_where += " and tab_richieste.data_registrazione <= '"+string(ldt_data_al, s_cs_xx.db_funzioni.formato_data)+"' "
end if

ls_cod_divisione = dw_ricerca.getitemstring(1, "cod_divisione")
if ls_cod_divisione<>"" and not isnull(ls_cod_divisione) then
	is_sql_where += " and tab_richieste.cod_divisione = '"+ls_cod_divisione+"' "
end if

ls_cod_tipo_richiesta = dw_ricerca.getitemstring(1, "cod_tipo_richiesta")
if ls_cod_tipo_richiesta<>"" and not isnull(ls_cod_tipo_richiesta) then
	is_sql_where += " and tab_richieste.cod_tipo_richiesta = '"+ls_cod_tipo_richiesta+"' "
end if

ls_groupby = " group by tab_richieste.cod_divisione,anag_divisioni.des_divisione,tab_richieste.cod_tipo_richiesta,tab_tipi_richieste.des_tipo_richiesta "

ls_sql += is_sql_where + ls_groupby


declare cu_rep_supp dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_rep_supp;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore nella open del cursore cu_rep_supp: " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_rep_supp into	:ls_cod_divisione_cu,
									:ls_des_divisione_cu,
									:ls_cod_tipo_richiesta_cu,
									:ls_des_tipo_richiesta_cu,
									:ld_quota_richiesta;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore nella fetch del cursore cu_rep_supp: " + sqlca.sqlerrtext
		close cu_rep_supp;
		return -1
		
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	dw_ricerca.object.log_t.text = "Elaborazione dati Commessa "+ls_des_divisione_cu+" - Tipo Richiesta "+ls_des_tipo_richiesta_cu+" ..."
	
	if ls_tipo_report = "C" then
		li_ret = wf_leggi_servizi_suppl(ls_cod_divisione_cu, ls_cod_tipo_richiesta_cu, ls_des_divisione_cu, ls_des_tipo_richiesta_cu, ld_quota_richiesta, fs_errore)
	else
		li_ret = wf_leggi_servizi_suppl_richiedenti(ls_cod_divisione_cu, ls_cod_tipo_richiesta_cu, ls_des_divisione_cu, ls_des_tipo_richiesta_cu, ld_quota_richiesta, fs_errore)
	end if
	
	if li_ret <0 then
		//in fs_errore il messaggio ...
		close cu_rep_supp;
		return -1
	end if
	
loop

close cu_rep_supp;

iuo_dw_main = dw_report
dw_report.sort()
dw_report.groupcalc()
dw_report.change_dw_current()
dw_report.set_document_name("Report-Richieste-Supplementari")
dw_folder.fu_selecttab(2)

return 1
end function

public subroutine wf_annulla ();datetime ldt_null
string ls_null

setnull(ldt_null)
setnull(ls_null)

dw_ricerca.setitem( 1, "data_reg_da", ldt_null)
dw_ricerca.setitem( 1, "data_reg_a", ldt_null)
dw_ricerca.setitem( 1, "cod_divisione", ls_null)
dw_ricerca.setitem( 1, "cod_tipo_richiesta", ls_null)

return
end subroutine

public function integer wf_leggi_servizi_suppl_richiedenti (string fs_cod_divisione, string fs_cod_tipo_richiesta, string fs_des_divisione, string fs_des_tipo_richiesta, decimal fd_qta_richiesta, ref string fs_errore);  
string			ls_sql, ls_cod_listino_cu, ls_temp,ls_temp1,ls_des_listino_cu, ls_des_richiedente
decimal		ldd_qta_servizi_aggiuntivi_cu
long			ll_new, ll_count_richieste, ll_count_richieste_evase, ll_rows, ll_i, ll_cod_richiedente
datastore 	lds_richiedenti


// stefanop: 17/03/2014: Integrazione_sate_2014
select count(*)
into :ll_count_richieste
from tab_richieste
where cod_azienda = :s_cs_xx.cod_azienda and	
		 cod_divisione = :fs_cod_divisione;
		 
if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
elseif isnull(ll_count_richieste) then
	ll_count_richieste = 0
end if

select count(*)
into :ll_count_richieste_evase
from tab_richieste
where cod_azienda = :s_cs_xx.cod_azienda and	
		 cod_divisione = :fs_cod_divisione and
		 flag_richiesta_chiusa = 'S';
		 
if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
elseif isnull(ll_count_richieste_evase) then
	ll_count_richieste_evase = 0
end if

ls_sql = "select distinct tab_richieste.cod_richiedente, tab_richiedenti.nominativo " + &
			"from tab_richieste " + &
			"join tab_richiedenti on " + &
				"tab_richiedenti.cod_azienda = tab_richieste.cod_azienda and " + &
				"tab_richiedenti.cod_richiedente = tab_richieste.cod_richiedente " + &
			"where tab_richieste.cod_azienda = '" + s_cs_xx.cod_azienda + "' and tab_richieste.cod_divisione = '" + fs_cod_divisione +"' " + is_sql_where
			
ll_rows = guo_functions.uof_crea_datastore(lds_richiedenti, ls_sql, ref fs_errore)

if ll_rows < 0 then
	return -1
end if

for ll_i = 1 to ll_rows
	 ll_cod_richiedente = lds_richiedenti.getitemnumber(ll_i, 1)
	 ls_des_richiedente = lds_richiedenti.getitemstring(ll_i, 2)
	 //"tab_richieste.cod_tipo_richiesta = '"+fs_cod_tipo_richiesta+"' and "+&
	 
	 ls_sql = "select distinct tab_richieste.cod_tipo_richiesta,"+&
							"tab_richieste_supplementari.cod_listino,"+&
							"sum(tab_richieste_supplementari.quantita) as qta_servizi_aggiuntivi "+&
				"from tab_richieste_supplementari "+&
				"join tab_richieste on tab_richieste.cod_azienda = tab_richieste_supplementari.cod_azienda and "+&
											"tab_richieste.anno_registrazione = tab_richieste_supplementari.anno_registrazione and "+&
											"tab_richieste.num_registrazione = tab_richieste_supplementari.num_registrazione "+&
				"where tab_richieste.cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
						"tab_richieste.cod_divisione = '"+fs_cod_divisione+"' and "+&						
						"tab_richieste.cod_richiedente='" + string(ll_cod_richiedente) + "' " + &
						is_sql_where + &
				"group by tab_richieste.cod_tipo_richiesta, tab_richieste_supplementari.cod_listino "

	declare cu_supp_det dynamic cursor for sqlsa;
	prepare sqlsa from :ls_sql;
	open dynamic cu_supp_det;
	
	if sqlca.sqlcode <> 0 then 
		fs_errore = "Errore nella open del cursore cu_supp_det: " + sqlca.sqlerrtext
		return -1
	end if
	
	do while true
		fetch cu_supp_det into 
			:ls_temp1,
			:ls_cod_listino_cu,
			:ldd_qta_servizi_aggiuntivi_cu;
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore nella fetch del cursore cu_supp_det: " + sqlca.sqlerrtext
			close cu_supp_det;
			return -1
			
		elseif sqlca.sqlcode = 100 then
			exit
			
		end if	
		
		dw_ricerca.object.log_t.text = "Elaborazione dati Commessa "+fs_cod_divisione+" - Tipo Richiesta "+fs_cod_tipo_richiesta+" - Richiedente " + string(ll_cod_richiedente) +" ..."
		yield()
		
		ll_new = dw_report.insertrow(0)
		
		dw_report.setitem(ll_new, "cod_divisione", fs_cod_divisione)
		dw_report.setitem(ll_new, "cod_tipo_richiesta", fs_cod_tipo_richiesta)
		dw_report.setitem(ll_new, "des_divisione", fs_des_divisione)
		dw_report.setitem(ll_new, "des_tipo_richiesta", fs_des_tipo_richiesta)
		dw_report.setitem(ll_new, "quota_richiesta", fd_qta_richiesta)
		dw_report.setitem(ll_new, "totale_richieste", ll_count_richieste)
		dw_report.setitem(ll_new, "totale_richieste_evase", ll_count_richieste_evase)
		dw_report.setitem(ll_new, "cod_richiedente", ll_cod_richiedente)
		dw_report.setitem(ll_new, "des_richiedente", ls_des_richiedente)
		
		select des_listino
		into :ls_des_listino_cu
		from tab_tipi_richieste_listini
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_listino=:ls_cod_listino_cu;
					
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in lettura descrizione listino: "+sqlca.sqlerrtext
			close cu_supp_det;
			return -1
		end if
		
		dw_report.setitem(ll_new, "des_listino", ls_des_listino_cu)
		dw_report.setitem(ll_new, "quantita_listino", ldd_qta_servizi_aggiuntivi_cu)
		
	loop

	close cu_supp_det;
	
next
  
return 1
end function

public function integer wf_leggi_servizi_suppl (string fs_cod_divisione, string fs_cod_tipo_richiesta, string fs_des_divisione, string fs_des_tipo_richiesta, decimal fd_qta_richiesta, ref string fs_errore);  
string			ls_sql, ls_cod_listino_cu, ls_temp,ls_temp1,ls_des_listino_cu
decimal		ldd_qta_servizi_aggiuntivi_cu
long			ll_new, ll_count_richieste, ll_count_richieste_evase


// stefanop: 17/03/2014: Integrazione_sate_2014
select count(*)
into :ll_count_richieste
from tab_richieste
where cod_azienda = :s_cs_xx.cod_azienda and	
		 cod_divisione = :fs_cod_divisione;
		 
if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
elseif isnull(ll_count_richieste) then
	ll_count_richieste = 0
end if

select count(*)
into :ll_count_richieste_evase
from tab_richieste
where cod_azienda = :s_cs_xx.cod_azienda and	
		 cod_divisione = :fs_cod_divisione and
		 flag_richiesta_chiusa = 'S';
		 
if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
elseif isnull(ll_count_richieste_evase) then
	ll_count_richieste_evase = 0
end if

ls_sql =		"select 	tab_richieste.cod_divisione,"+&
							"tab_richieste.cod_tipo_richiesta,"+&
							"tab_richieste_supplementari.cod_listino,"+&
							"sum(tab_richieste_supplementari.quantita) as qta_servizi_aggiuntivi "+&
				"from tab_richieste_supplementari "+&
				"join tab_richieste on tab_richieste.cod_azienda = tab_richieste_supplementari.cod_azienda and "+&
											"tab_richieste.anno_registrazione = tab_richieste_supplementari.anno_registrazione and "+&
											"tab_richieste.num_registrazione = tab_richieste_supplementari.num_registrazione "+&
				"where tab_richieste.cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
						"tab_richieste.cod_divisione = '"+fs_cod_divisione+"' and "+&
						"tab_richieste.cod_tipo_richiesta = '"+fs_cod_tipo_richiesta+"' "+&
				"group by tab_richieste.cod_divisione, tab_richieste.cod_tipo_richiesta, tab_richieste_supplementari.cod_listino "

declare cu_supp_det dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_supp_det;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore nella open del cursore cu_supp_det: " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_supp_det into 	:ls_temp,
									:ls_temp1,
									:ls_cod_listino_cu,
									:ldd_qta_servizi_aggiuntivi_cu;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore nella fetch del cursore cu_supp_det: " + sqlca.sqlerrtext
		close cu_supp_det;
		return -1
		
	elseif sqlca.sqlcode = 100 then
		exit
		
	end if	
	
	ll_new = dw_report.insertrow(0)
	
	dw_report.setitem(ll_new, "cod_divisione", fs_cod_divisione)
	dw_report.setitem(ll_new, "cod_tipo_richiesta", fs_cod_tipo_richiesta)
	dw_report.setitem(ll_new, "des_divisione", fs_des_divisione)
	dw_report.setitem(ll_new, "des_tipo_richiesta", fs_des_tipo_richiesta)
	dw_report.setitem(ll_new, "quota_richiesta", fd_qta_richiesta)
	dw_report.setitem(ll_new, "totale_richieste", ll_count_richieste)
	dw_report.setitem(ll_new, "totale_richieste_evase", ll_count_richieste_evase)
	
	
	select des_listino
	into :ls_des_listino_cu
	from tab_tipi_richieste_listini
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_listino=:ls_cod_listino_cu;
				
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in lettura descrizione listino: "+sqlca.sqlerrtext
		close cu_supp_det;
		return -1
	end if
	
	dw_report.setitem(ll_new, "des_listino", ls_des_listino_cu)
	dw_report.setitem(ll_new, "quantita_listino", ldd_qta_servizi_aggiuntivi_cu)
	
loop

close cu_supp_det;
  
  
return 1
end function

on w_report_richieste_supplementari.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.dw_folder
this.Control[iCurrent+3]=this.dw_ricerca
end on

on w_report_richieste_supplementari.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(	dw_ricerca, &
							"cod_tipo_richiesta", &
							sqlca, &
							"tab_tipi_richieste", &
							"cod_tipo_richiesta", &
							"des_tipo_richiesta", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;string ls_path_logo,ls_modify, ls_odl
windowobject lw_oggetti[]

dw_report.ib_dw_report = true
set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer + &
                         c_nocursorrowfocusrect)

dw_ricerca.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
 
iuo_dw_main = dw_report

lw_oggetti[1] = dw_report
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])

lw_oggetti[1] = dw_ricerca
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_selecttab(1)

dw_folder.fu_foldercreate(2,4)
dw_folder.fu_selecttab(1)

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode < 0 then g_mb.messagebox("Omnia","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
if sqlca.sqlcode = 100 then g_mb.messagebox("Omnia","manca il parametro LO5 in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
dw_report.modify(ls_modify)

dw_report.object.datawindow.print.preview = 'Yes'
end event

event pc_print;/**
 Tolto ancestor
 !!! IL FRAMEWORK E' DA BUTTARE !!!
 **/

dw_report.print()
end event

type dw_report from uo_cs_xx_dw within w_report_richieste_supplementari
integer x = 69
integer y = 148
integer width = 3776
integer height = 2120
integer taborder = 30
string dataobject = "d_report_richieste_suppl"
boolean vscrollbar = true
end type

type dw_folder from u_folder within w_report_richieste_supplementari
integer x = 9
integer y = 8
integer width = 3858
integer height = 2276
integer taborder = 10
boolean border = false
end type

type dw_ricerca from uo_cs_xx_dw within w_report_richieste_supplementari
integer x = 69
integer y = 148
integer width = 2523
integer height = 840
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_report_richieste_suppl_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string ls_errore

choose case dwo.name
	case "b_ricerca_divisione"
		guo_ricerca.uof_ricerca_divisione(dw_ricerca, "cod_divisione")
		
	case "b_report"
		if wf_report(ls_errore)<0 then
			g_mb.error( ls_errore )
			dw_ricerca.object.log_t.text = "Elaborazione terminata con errori!"
			return
		else
			dw_ricerca.object.log_t.text = "Pronto!"
		end if
		
	case "b_annulla"
		wf_annulla()
		
end choose
end event

