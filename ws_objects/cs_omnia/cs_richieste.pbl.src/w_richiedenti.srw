﻿$PBExportHeader$w_richiedenti.srw
forward
global type w_richiedenti from w_cs_xx_principale
end type
type dw_tab_richiedenti from uo_cs_xx_dw within w_richiedenti
end type
type dw_tab_richiedenti_lista from uo_cs_xx_dw within w_richiedenti
end type
type dw_folder_search from u_folder within w_richiedenti
end type
type dw_ricerca from u_dw_search within w_richiedenti
end type
end forward

global type w_richiedenti from w_cs_xx_principale
integer width = 2775
integer height = 2124
string title = "Richiedenti"
dw_tab_richiedenti dw_tab_richiedenti
dw_tab_richiedenti_lista dw_tab_richiedenti_lista
dw_folder_search dw_folder_search
dw_ricerca dw_ricerca
end type
global w_richiedenti w_richiedenti

forward prototypes
public subroutine wf_imposta_ricerca ()
end prototypes

public subroutine wf_imposta_ricerca ();string ls_nominativo
dw_ricerca.accepttext()

ls_nominativo = dw_ricerca.getitemstring(1, "nominativo")
if isnull(ls_nominativo) or ls_nominativo = "" then
	dw_ricerca.setitem(1, "nominativo_fix", ls_nominativo)
else
	// forzo l'uso del like
	dw_ricerca.setitem(1, "nominativo_fix", "%" + ls_nominativo + "%")
end if

dw_ricerca.fu_BuildSearch(TRUE)
dw_tab_richiedenti_lista.change_dw_current()
triggerevent("pc_retrieve")
dw_folder_search.fu_SelectTab(1)
end subroutine

on w_richiedenti.create
int iCurrent
call super::create
this.dw_tab_richiedenti=create dw_tab_richiedenti
this.dw_tab_richiedenti_lista=create dw_tab_richiedenti_lista
this.dw_folder_search=create dw_folder_search
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_richiedenti
this.Control[iCurrent+2]=this.dw_tab_richiedenti_lista
this.Control[iCurrent+3]=this.dw_folder_search
this.Control[iCurrent+4]=this.dw_ricerca
end on

on w_richiedenti.destroy
call super::destroy
destroy(this.dw_tab_richiedenti)
destroy(this.dw_tab_richiedenti_lista)
destroy(this.dw_folder_search)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject l_objects[]

dw_tab_richiedenti_lista.set_dw_key("cod_azienda")
dw_tab_richiedenti_lista.set_dw_options(sqlca, pcca.null_object, c_noretrieveonopen, c_default)

dw_tab_richiedenti.set_dw_options(sqlca, dw_tab_richiedenti_lista, c_sharedata + c_scrollparent, c_default)

iuo_dw_main = dw_tab_richiedenti_lista

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, dw_folder_search.c_foldertableft)								 

l_objects[1] = dw_tab_richiedenti_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_ricerca
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)

// Filtri
l_criteriacolumn[1] = "cod_divisione"
l_criteriacolumn[2] = "cod_cliente"
l_criteriacolumn[3] = "nominativo_fix"

l_searchtable[1] = "tab_richiedenti"
l_searchtable[2] = "tab_richiedenti"
l_searchtable[3] = "tab_richiedenti"

l_searchcolumn[1] = "cod_divisione"
l_searchcolumn[2] = "cod_cliente"
l_searchcolumn[3] = "nominativo"

dw_ricerca.fu_wiredw(l_criteriacolumn[], dw_tab_richiedenti_lista, l_searchtable[], l_searchcolumn[], SQLCA)
end event

type dw_tab_richiedenti from uo_cs_xx_dw within w_richiedenti
integer y = 640
integer width = 2720
integer height = 1352
integer taborder = 20
string dataobject = "d_richiedenti_det"
borderstyle borderstyle = styleraised!
end type

event clicked;/** TOLTO ANCESTOR SCRIPT **/

choose case dwo.name
	case "b_divisioni"
		dw_tab_richiedenti.change_dw_current()
		s_cs_xx.parametri.parametro_uo_dw_1 = dw_tab_richiedenti
		s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
		if not isvalid(w_divisioni_ricerca) then
			window_open(w_divisioni_ricerca, 0)
		end if
		w_divisioni_ricerca.show()

	case "b_area_aziendale"
		dw_tab_richiedenti.change_dw_current()
		s_cs_xx.parametri.parametro_uo_dw_1 = dw_tab_richiedenti
		s_cs_xx.parametri.parametro_s_1 = "cod_area_aziendale"
		if not isvalid(w_divisioni_ricerca) then
			window_open(w_divisioni_ricerca, 0)
		end if
		w_divisioni_ricerca.show()
		
	case "b_cliente"
		dw_tab_richiedenti.change_dw_current()
		guo_ricerca.uof_ricerca_cliente(dw_tab_richiedenti,"cod_cliente")
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tab_richiedenti,"cod_cliente")
			
	case "b_ricerca_utente"
		guo_ricerca.uof_ricerca_utente(dw_tab_richiedenti,"cod_utente")
end choose
end event

type dw_tab_richiedenti_lista from uo_cs_xx_dw within w_richiedenti
integer x = 137
integer y = 40
integer width = 2537
integer height = 556
integer taborder = 10
string dataobject = "d_richiedenti_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i,ll_num_richiedente


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	if isnull(this.getitemnumber(ll_i, "cod_richiedente")) or this.getitemnumber(ll_i, "cod_richiedente") < 1 then	
		select max(cod_richiedente)
		into   :ll_num_richiedente
		from   tab_richiedenti
		where  cod_azienda = :s_cs_xx.cod_azienda;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return 
		end if

		if isnull(ll_num_richiedente) or ll_num_richiedente = 0 then 
			ll_num_richiedente = 1
		else 
			ll_num_richiedente++
		end if
		setitem(getrow(), "cod_richiedente", ll_num_richiedente)
	end if
	
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_view;call super::pcd_view;dw_tab_richiedenti.object.b_ricerca_cliente.enabled=false
dw_tab_richiedenti.object.b_divisioni.enabled=false
dw_tab_richiedenti.object.b_area_aziendale.enabled=false
end event

event pcd_new;call super::pcd_new;dw_tab_richiedenti.object.b_ricerca_cliente.enabled=true
dw_tab_richiedenti.object.b_divisioni.enabled=true
dw_tab_richiedenti.object.b_area_aziendale.enabled=true
end event

event pcd_modify;call super::pcd_modify;dw_tab_richiedenti.object.b_ricerca_cliente.enabled=true
dw_tab_richiedenti.object.b_divisioni.enabled=true
dw_tab_richiedenti.object.b_area_aziendale.enabled=true
end event

event pcd_delete;call super::pcd_delete;dw_tab_richiedenti.object.b_ricerca_cliente.enabled=false
dw_tab_richiedenti.object.b_divisioni.enabled=false
dw_tab_richiedenti.object.b_area_aziendale.enabled=false
end event

type dw_folder_search from u_folder within w_richiedenti
integer x = 23
integer y = 20
integer width = 2697
integer height = 600
integer taborder = 10
end type

type dw_ricerca from u_dw_search within w_richiedenti
integer x = 137
integer y = 40
integer width = 2560
integer height = 560
integer taborder = 30
string dataobject = "d_richiedenti_ricerca"
boolean border = false
end type

event clicked;call super::clicked;choose case dwo.name
		
	case "b_divisione"
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
		s_cs_xx.parametri.parametro_uo_dw_search = dw_ricerca
		s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
		s_cs_xx.parametri.parametro_w_cs_xx_1 = parent
		if not isvalid(w_divisioni_ricerca) then
			window_open(w_divisioni_ricerca, 0)
		end if
		
		w_divisioni_ricerca.show()
		
	case "b_cliente"
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
	guo_ricerca.uof_ricerca_cliente(dw_ricerca,"cod_cliente")


	case "b_cerca"
		wf_imposta_ricerca()

	case "b_annulla"
		dw_ricerca.fu_Reset()
		
end choose
end event

