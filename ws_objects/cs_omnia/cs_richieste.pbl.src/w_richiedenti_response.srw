﻿$PBExportHeader$w_richiedenti_response.srw
forward
global type w_richiedenti_response from w_cs_xx_principale
end type
type dw_tab_richiedenti from uo_cs_xx_dw within w_richiedenti_response
end type
type dw_tab_richiedenti_lista from uo_cs_xx_dw within w_richiedenti_response
end type
end forward

global type w_richiedenti_response from w_cs_xx_principale
integer width = 2574
integer height = 1888
string title = "Richiedenti"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
dw_tab_richiedenti dw_tab_richiedenti
dw_tab_richiedenti_lista dw_tab_richiedenti_lista
end type
global w_richiedenti_response w_richiedenti_response

type variables
string is_cod_divisione
end variables

on w_richiedenti_response.create
int iCurrent
call super::create
this.dw_tab_richiedenti=create dw_tab_richiedenti
this.dw_tab_richiedenti_lista=create dw_tab_richiedenti_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_richiedenti
this.Control[iCurrent+2]=this.dw_tab_richiedenti_lista
end on

on w_richiedenti_response.destroy
call super::destroy
destroy(this.dw_tab_richiedenti)
destroy(this.dw_tab_richiedenti_lista)
end on

event pc_setwindow;call super::pc_setwindow;


dw_tab_richiedenti_lista.set_dw_key("cod_azienda")
dw_tab_richiedenti_lista.set_dw_options(sqlca, &
                        			       pcca.null_object, &
			                               c_noretrieveonopen + c_default, &
			                               c_default)
													 
dw_tab_richiedenti.set_dw_options(sqlca, &
                             dw_tab_richiedenti_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)

iuo_dw_main = dw_tab_richiedenti_lista

is_cod_divisione = s_cs_xx.parametri.parametro_s_1
setnull(s_cs_xx.parametri.parametro_s_1)

if is_cod_divisione="" then setnull(is_cod_divisione)

//if is_cod_divisione<>"" and not isnull(is_cod_divisione) then dw_tab_richiedenti_lista.postevent("pcd_new")

//dw_tab_richiedenti_lista.change_dw_current()
//this.postevent("pc_retrieve")

//this.postevent("pc_new")
dw_tab_richiedenti_lista.event post ue_scroll_last_row()



end event

type dw_tab_richiedenti from uo_cs_xx_dw within w_richiedenti_response
integer y = 540
integer width = 2560
integer height = 1260
integer taborder = 20
string dataobject = "d_richiedenti_det"
borderstyle borderstyle = styleraised!
end type

event pcd_setkey;call super::pcd_setkey;//long ll_i, ll_num_richiedente
//
//dw_tab_richiedenti.acceptText()
//
//for ll_i = 1 to this.rowcount()
//   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
//      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
//   end if
//	if isnull(this.getitemnumber(ll_i, "cod_richiedente")) or this.getitemnumber(ll_i, "cod_richiedente") < 1 then	
//		select max(cod_richiedente)
//		into   :ll_num_richiedente
//		from   tab_richiedenti
//		where  cod_azienda = :s_cs_xx.cod_azienda;
//
//		if sqlca.sqlcode < 0 then
//			messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//			return 
//		end if
//
//		if isnull(ll_num_richiedente) or ll_num_richiedente = 0 then 
//			ll_num_richiedente = 1
//		else 
//			ll_num_richiedente++
//		end if
//		setitem(getrow(), "cod_richiedente", ll_num_richiedente)
//	end if
//next
//
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_divisioni"
		guo_ricerca.uof_ricerca_divisione(dw_tab_richiedenti, "cod_divisione")
	case "b_area_aziendale"
		guo_ricerca.uof_ricerca_area_aziendale(dw_tab_richiedenti, "cod_area_aziendale")
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tab_richiedenti, "cod_cliente")
end choose
end event

event pcd_new;call super::pcd_new;dw_tab_richiedenti.object.b_divisioni.enabled = true
dw_tab_richiedenti.object.b_area_aziendale.enabled = true
dw_tab_richiedenti.object.b_ricerca_cliente.enabled = true
end event

event pcd_modify;call super::pcd_modify;dw_tab_richiedenti.object.b_divisioni.enabled = true
dw_tab_richiedenti.object.b_area_aziendale.enabled = true
dw_tab_richiedenti.object.b_ricerca_cliente.enabled = true
end event

event pcd_view;call super::pcd_view;dw_tab_richiedenti.object.b_divisioni.enabled = false
dw_tab_richiedenti.object.b_area_aziendale.enabled = false
dw_tab_richiedenti.object.b_ricerca_cliente.enabled = false
end event

type dw_tab_richiedenti_lista from uo_cs_xx_dw within w_richiedenti_response
event ue_scroll_last_row ( )
integer x = 18
integer y = 20
integer width = 2537
integer height = 516
integer taborder = 10
string dataobject = "d_richiedenti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_scroll_last_row();long  ll_last_row


this.change_dw_current()
parent.postevent("pc_retrieve")

parent.postevent("pc_new")



//ll_last_row = insertrow(0)

ll_last_row = rowcount()

if ll_last_row > 0 then
	scrolltorow(ll_last_row)
	
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i,ll_num_richiedente


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	if isnull(this.getitemnumber(ll_i, "cod_richiedente")) or this.getitemnumber(ll_i, "cod_richiedente") < 1 then	
		select max(cod_richiedente)
		into   :ll_num_richiedente
		from   tab_richiedenti
		where  cod_azienda = :s_cs_xx.cod_azienda;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return 
		end if

		if isnull(ll_num_richiedente) or ll_num_richiedente = 0 then 
			ll_num_richiedente = 1
		else 
			ll_num_richiedente++
		end if
		setitem(getrow(), "cod_richiedente", ll_num_richiedente)
	end if
	
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_new;call super::pcd_new;

dw_tab_richiedenti_lista.setitem(getrow(), "cod_divisione", is_cod_divisione)

end event

event itemfocuschanged;call super::itemfocuschanged;//dw_tab_richiedenti.acceptText()
end event

