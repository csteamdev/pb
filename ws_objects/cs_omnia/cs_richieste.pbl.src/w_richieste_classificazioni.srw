﻿$PBExportHeader$w_richieste_classificazioni.srw
forward
global type w_richieste_classificazioni from w_cs_xx_principale
end type
type dw_richieste_classificazioni from uo_cs_xx_dw within w_richieste_classificazioni
end type
end forward

global type w_richieste_classificazioni from w_cs_xx_principale
integer width = 3771
integer height = 1264
string title = "Classificazioni Richieste"
dw_richieste_classificazioni dw_richieste_classificazioni
end type
global w_richieste_classificazioni w_richieste_classificazioni

on w_richieste_classificazioni.create
int iCurrent
call super::create
this.dw_richieste_classificazioni=create dw_richieste_classificazioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_richieste_classificazioni
end on

on w_richieste_classificazioni.destroy
call super::destroy
destroy(this.dw_richieste_classificazioni)
end on

event pc_setwindow;call super::pc_setwindow;dw_richieste_classificazioni.set_dw_key("cod_azienda")
dw_richieste_classificazioni.set_dw_options(sqlca, &
											 pcca.null_object, &
											 c_default, &
											 c_default)

iuo_dw_main = dw_richieste_classificazioni
end event

type dw_richieste_classificazioni from uo_cs_xx_dw within w_richieste_classificazioni
integer x = 23
integer y = 20
integer width = 3703
integer height = 1120
integer taborder = 10
string dataobject = "d_richieste_classificazioni"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long   l_errore, ll_i

//ls_cod_tipo_richiesta= i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_richiesta")
	
for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
//	 if isnull(this.getitemstring(ll_i, "flag_blocco")) then
//      this.setitem(ll_i, "flag_blocco",'N')
//   end if
next

end event

event doubleclicked;call super::doubleclicked;//if row < 1 then return
//
//if s_cs_xx.admin then
//
//	str_des_multilingua lstr_des_multilingua
//	
//	lstr_des_multilingua.nome_tabella = "tab_richieste_classificazioni"
//	lstr_des_multilingua.descrizione_origine = getitemstring(row,"des_classe_richiesta")
//	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_classe_richiesta")
//	setnull(lstr_des_multilingua.chiave_str_2)
//	
//	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
//	
//	openwithparm(w_des_multilingua, lstr_des_multilingua)
//
//end if
end event

