﻿$PBExportHeader$w_richieste_tipologie.srw
forward
global type w_richieste_tipologie from w_cs_xx_principale
end type
type dw_richieste_tipologie from uo_cs_xx_dw within w_richieste_tipologie
end type
end forward

global type w_richieste_tipologie from w_cs_xx_principale
integer width = 4073
string title = "Tipologie Richieste"
dw_richieste_tipologie dw_richieste_tipologie
end type
global w_richieste_tipologie w_richieste_tipologie

on w_richieste_tipologie.create
int iCurrent
call super::create
this.dw_richieste_tipologie=create dw_richieste_tipologie
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_richieste_tipologie
end on

on w_richieste_tipologie.destroy
call super::destroy
destroy(this.dw_richieste_tipologie)
end on

event pc_setwindow;call super::pc_setwindow;dw_richieste_tipologie.set_dw_key("cod_azienda")
dw_richieste_tipologie.set_dw_options(sqlca, &
                        			       pcca.null_object, &
			                               c_default, &
			                               c_default)

iuo_dw_main = dw_richieste_tipologie
end event

type dw_richieste_tipologie from uo_cs_xx_dw within w_richieste_tipologie
integer width = 4023
integer height = 1320
integer taborder = 10
string dataobject = "d_richieste_tipologie"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event doubleclicked;call super::doubleclicked;//if row < 1 then return
//
//if s_cs_xx.admin then
//
//	str_des_multilingua lstr_des_multilingua
//	
//	lstr_des_multilingua.nome_tabella = "tab_richieste_tipologie"
//	lstr_des_multilingua.descrizione_origine = getitemstring(row,"des_tipologia_richiesta")
//	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_tipologia_richiesta")
//	setnull(lstr_des_multilingua.chiave_str_2)
//	
//	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
//	
//	openwithparm(w_des_multilingua, lstr_des_multilingua)
//
//end if
end event

