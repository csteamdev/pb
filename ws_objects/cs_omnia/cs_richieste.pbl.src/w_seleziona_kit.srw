﻿$PBExportHeader$w_seleziona_kit.srw
forward
global type w_seleziona_kit from w_cs_xx_risposta
end type
type st_2 from statictext within w_seleziona_kit
end type
type st_1 from statictext within w_seleziona_kit
end type
type dw_folder from u_folder within w_seleziona_kit
end type
type dw_kit from datawindow within w_seleziona_kit
end type
type dw_filtro from u_dw_search within w_seleziona_kit
end type
type cb_1 from commandbutton within w_seleziona_kit
end type
type cb_annulla from commandbutton within w_seleziona_kit
end type
type cb_annulla2 from commandbutton within w_seleziona_kit
end type
type cb_cerca from commandbutton within w_seleziona_kit
end type
type cb_reset from commandbutton within w_seleziona_kit
end type
type cb_naviga from cb_gps_maps within w_seleziona_kit
end type
end forward

global type w_seleziona_kit from w_cs_xx_risposta
integer width = 2798
integer height = 1532
string title = "Selezione Kit di Ricambio"
st_2 st_2
st_1 st_1
dw_folder dw_folder
dw_kit dw_kit
dw_filtro dw_filtro
cb_1 cb_1
cb_annulla cb_annulla
cb_annulla2 cb_annulla2
cb_cerca cb_cerca
cb_reset cb_reset
cb_naviga cb_naviga
end type
global w_seleziona_kit w_seleziona_kit

type variables

end variables

on w_seleziona_kit.create
int iCurrent
call super::create
this.st_2=create st_2
this.st_1=create st_1
this.dw_folder=create dw_folder
this.dw_kit=create dw_kit
this.dw_filtro=create dw_filtro
this.cb_1=create cb_1
this.cb_annulla=create cb_annulla
this.cb_annulla2=create cb_annulla2
this.cb_cerca=create cb_cerca
this.cb_reset=create cb_reset
this.cb_naviga=create cb_naviga
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_2
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_kit
this.Control[iCurrent+5]=this.dw_filtro
this.Control[iCurrent+6]=this.cb_1
this.Control[iCurrent+7]=this.cb_annulla
this.Control[iCurrent+8]=this.cb_annulla2
this.Control[iCurrent+9]=this.cb_cerca
this.Control[iCurrent+10]=this.cb_reset
this.Control[iCurrent+11]=this.cb_naviga
end on

on w_seleziona_kit.destroy
call super::destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_folder)
destroy(this.dw_kit)
destroy(this.dw_filtro)
destroy(this.cb_1)
destroy(this.cb_annulla)
destroy(this.cb_annulla2)
destroy(this.cb_cerca)
destroy(this.cb_reset)
destroy(this.cb_naviga)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW (dw_filtro,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

dw_filtro.insertrow(0)
dw_kit.settransobject(sqlca)

lw_oggetti[1] = dw_kit
lw_oggetti[2] = cb_annulla
lw_oggetti[3] = cb_1
lw_oggetti[4] = st_1
lw_oggetti[5] = st_2
dw_folder.fu_assigntab(2, "Kit di Ricambio", lw_oggetti[])

lw_oggetti[1] = dw_filtro
//lw_oggetti[2] = cb_attrezzature_ricerca
lw_oggetti[2] = cb_cerca
lw_oggetti[3] = cb_reset
lw_oggetti[4] = cb_annulla2
//lw_oggetti[5] = cb_ricerca_kit
lw_oggetti[5] = cb_naviga
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(1)
end event

type st_2 from statictext within w_seleziona_kit
integer x = 434
integer y = 160
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Clic sul pulsante"
boolean focusrectangle = false
end type

type st_1 from statictext within w_seleziona_kit
integer x = 1029
integer y = 160
integer width = 1317
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "per selezionare un Kit di ricambio"
boolean focusrectangle = false
end type

type dw_folder from u_folder within w_seleziona_kit
integer x = 23
integer y = 20
integer width = 2720
integer height = 1380
integer taborder = 10
end type

type dw_kit from datawindow within w_seleziona_kit
integer x = 46
integer y = 240
integer width = 2674
integer height = 1140
integer taborder = 50
boolean bringtotop = true
string title = "none"
string dataobject = "d_tv_kit"
boolean border = false
boolean livescroll = true
end type

event retrieveend;object.p_padre.FileName = s_cs_xx.volume + s_cs_xx.risorse + "pf.bmp"
object.p_figlio.FileName = s_cs_xx.volume + s_cs_xx.risorse + "mp.bmp"
end event

event buttonclicked;string ls_cod_prodotto_padre, ls_cod_versione_padre, ls_separetor, ls_return, ls_des_prodotto, ls_msg

if row > 0 then
	choose case dwo.name
		case "b_seleziona"
			ls_separetor="-stringa-separatrice-"
			ls_cod_prodotto_padre = getitemstring(row, "cod_prodotto_padre")
			ls_cod_versione_padre = getitemstring(row, "cod_versione_padre")
			ls_des_prodotto = getitemstring(row, "des_prodotto_padre")
			
			if isnull(ls_des_prodotto) then ls_des_prodotto = ""
			
			ls_msg = "hai scelto di associare all'offerta i componenti del kit di ricambio relativi al prodotto "+ls_cod_prodotto_padre + &
						" - " + ls_des_prodotto + " versione " + ls_cod_versione_padre+ ". Continuare?"
			
			if g_mb.messagebox("OMNIA",ls_msg,Question!,YesNo!,2) = 1 then
				ls_return = ls_cod_prodotto_padre+ ls_separetor + ls_cod_versione_padre
				if isnull(ls_return) or ls_return="-stringa-separatrice-" then ls_return = ""
				closewithreturn(parent,ls_return)
			end if
	end choose	
end if
end event

type dw_filtro from u_dw_search within w_seleziona_kit
integer x = 46
integer y = 140
integer width = 2674
integer height = 680
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_ext_filtro_x_kit"
end type

event itemchanged;call super::itemchanged;string ls_cod_versione
long ll_test

choose case dwo.name
	case "cod_attrezzatura"
	//ripopolare la dddw_delle tipologie manutenzioni
		if data = "" or isnull(data) then
			f_PO_LoadDDDW_DW (dw_filtro,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
		else
			f_PO_LoadDDDW_DW (dw_filtro,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura= '" + data + "' ")
		end if
		
		
	case "rs_cod_prodotto"
			
		if isnull(data) or data = "" then
			dw_filtro.object.cod_versione.protect = '1'
			dw_filtro.object.cod_versione.color = string(RGB(128,128,128))
		else
			dw_filtro.object.cod_versione.protect = '0'
			dw_filtro.object.cod_versione.color = string(RGB(255,255,255))

			f_PO_LoadDDDW_DW(this,"cod_versione",sqlca,&
							  "distinta_padri","cod_versione","'VERSIONE ' "+guo_functions.uof_concat_op()+" cod_versione",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + data + "'")
		end if
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_filtro,"cod_attrezzatura")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_filtro,"rs_cod_prodotto")
end choose
end event

type cb_1 from commandbutton within w_seleziona_kit
integer x = 891
integer y = 140
integer width = 105
integer height = 84
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "S"
end type

type cb_annulla from commandbutton within w_seleziona_kit
integer x = 46
integer y = 140
integer width = 357
integer height = 84
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;closewithreturn(parent,"")
end event

type cb_annulla2 from commandbutton within w_seleziona_kit
integer x = 2217
integer y = 680
integer width = 357
integer height = 84
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;if g_mb.messagebox("OMNIA","Hai deciso di non associare nessun kit di ricambio all'offerta. Porcedere?",Question!, YesNo!,2) = 1 then	
	closewithreturn(parent,"")
end if
end event

type cb_cerca from commandbutton within w_seleziona_kit
integer x = 1486
integer y = 680
integer width = 357
integer height = 84
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_cod_attrezzatura, ls_cod_tipo_manut, ls_cod_prodotto_padre, ls_cod_versione_padre
boolean lb_filtro_ok = false

ls_cod_attrezzatura = dw_filtro.getitemstring(1, "cod_attrezzatura")
if ls_cod_attrezzatura = "" then setnull(ls_cod_attrezzatura)

ls_cod_tipo_manut = dw_filtro.getitemstring(1, "cod_tipo_manutenzione")
if ls_cod_tipo_manut = "" then setnull(ls_cod_tipo_manut)

ls_cod_prodotto_padre = dw_filtro.getitemstring(1, "rs_cod_prodotto")
if ls_cod_prodotto_padre = "" then setnull(ls_cod_prodotto_padre)

ls_cod_versione_padre = dw_filtro.getitemstring(1, "cod_versione")
if ls_cod_versione_padre = "" then setnull(ls_cod_versione_padre)

if isnull(ls_cod_attrezzatura) and isnull(ls_cod_tipo_manut) and isnull(ls_cod_prodotto_padre) and isnull(ls_cod_versione_padre) then
	if g_mb.messagebox("OMNIA","Non hai impostato nessun filtro. Vuoi continuare con la ricerca?",Question!, YesNo!, 2) = 1 then
		dw_kit.retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura, ls_cod_tipo_manut, ls_cod_prodotto_padre, ls_cod_versione_padre)
		dw_folder.fu_selecttab(2)
	end if
else
	dw_kit.retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura, ls_cod_tipo_manut, ls_cod_prodotto_padre, ls_cod_versione_padre)
	dw_folder.fu_selecttab(2)
end if
end event

type cb_reset from commandbutton within w_seleziona_kit
integer x = 1851
integer y = 680
integer width = 357
integer height = 84
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string ls_null

setnull(ls_null)

dw_filtro.setitem(1,"cod_attrezzatura",ls_null)
dw_filtro.setitem(1,"cod_tipo_manutenzione",ls_null)

dw_filtro.setitem(1,"rs_cod_prodotto",ls_null)
dw_filtro.setitem(1,"cod_versione",ls_null)
end event

type cb_naviga from cb_gps_maps within w_seleziona_kit
integer x = 2400
integer y = 160
integer width = 297
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
string facename = "Arial"
end type

event getfocus;call super::getfocus;decimal ld_lat, ld_long
string ls_cod_attrezzatura, ls_cod_area_aziendale
long ll_row

ll_row = dw_filtro.getrow()

if ll_row > 0 then
	ls_cod_attrezzatura = dw_filtro.getitemstring(ll_row, "cod_attrezzatura")
	
	select latitudine, longitudine, cod_area_aziendale
	into :ld_lat, :ld_long, :ls_cod_area_aziendale
	from anag_attrezzature
	where cod_azienda = :s_cs_xx.cod_azienda and
		cod_attrezzatura = :ls_cod_attrezzatura;
		
	if sqlca.sqlcode = 0 and ld_lat > 0 and ld_long > 0 then
		s_cs_xx.parametri.parametro_d_1 = ld_lat
		s_cs_xx.parametri.parametro_d_2 = ld_long
	else
		//prova con l'ubicazione, forse sei più fortunato....
		select latitudine, longitudine
		into :ld_lat, :ld_long
		from tab_aree_aziendali
		where cod_azienda = :s_cs_xx.cod_azienda and
			cod_area_aziendale = :ls_cod_area_aziendale;
			
		if sqlca.sqlcode = 0 then
			s_cs_xx.parametri.parametro_d_1 = ld_lat
			s_cs_xx.parametri.parametro_d_2 = ld_long
		else
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_d_2 = 0
		end if
	end if	
else
	s_cs_xx.parametri.parametro_d_1 = 0
	s_cs_xx.parametri.parametro_d_2 = 0
end if
end event

