﻿$PBExportHeader$w_tab_richieste_metrature.srw
forward
global type w_tab_richieste_metrature from w_cs_xx_principale
end type
type dw_det from uo_cs_xx_dw within w_tab_richieste_metrature
end type
type dw_folder_search from u_folder within w_tab_richieste_metrature
end type
type dw_lista from uo_cs_xx_dw within w_tab_richieste_metrature
end type
type dw_ricerca from u_dw_search within w_tab_richieste_metrature
end type
end forward

global type w_tab_richieste_metrature from w_cs_xx_principale
integer width = 2857
integer height = 1216
string title = "Richiedenti"
event ue_imposta_esposizione_corrente ( )
dw_det dw_det
dw_folder_search dw_folder_search
dw_lista dw_lista
dw_ricerca dw_ricerca
end type
global w_tab_richieste_metrature w_tab_richieste_metrature

forward prototypes
public subroutine wf_imposta_ricerca ()
public function integer wf_duplica_per_divisione (ref string fs_errore)
end prototypes

event ue_imposta_esposizione_corrente();string ls_cod_divisione_corrente

select cod_divisione
into :ls_cod_divisione_corrente
from parametri_manutenzioni
where cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in lettura Esposizione corrente: "+sqlca.sqlerrtext)
	return
	
end if

if ls_cod_divisione_corrente="" then setnull(ls_cod_divisione_corrente)

dw_ricerca.setitem(dw_ricerca.getrow(), "cod_divisione_corrente", ls_cod_divisione_corrente)
end event

public subroutine wf_imposta_ricerca ();
dw_ricerca.accepttext()

dw_ricerca.fu_BuildSearch(TRUE)
dw_lista.change_dw_current()

triggerevent("pc_retrieve")
dw_folder_search.fu_SelectTab(1)
end subroutine

public function integer wf_duplica_per_divisione (ref string fs_errore);string ls_cod_divisione_origine, ls_cod_divisione_corrente
long ll_count

dw_ricerca.accepttext()

//leggo i dati impostati
ls_cod_divisione_origine = dw_ricerca.getitemstring(dw_ricerca.getrow(), "cod_divisione_duplica")
ls_cod_divisione_corrente = dw_ricerca.getitemstring(dw_ricerca.getrow(), "cod_divisione_corrente")

if ls_cod_divisione_origine="" or isnull(ls_cod_divisione_origine) then
	fs_errore = "Selezionare l'Esposizione da cui copiare le metrature!"
	return -1
end if

if ls_cod_divisione_corrente="" or isnull(ls_cod_divisione_corrente) then
	fs_errore = "Manca l'Esposizione corrente!"
	return -1
end if

//controllo che ci siano dati da duplicare
select count(*)
into :ll_count
from tab_richieste_metrature
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_divisione=:ls_cod_divisione_origine;
		
if sqlca.sqlcode<0 then
	fs_errore = "Errore in controllo presenza metrature per l'Esposizione da duplicare: "+sqlca.sqlerrtext
	return -1
end if

if ll_count>0 then
else
	fs_errore = "Nessuna metratura presente da duplicare!"
	return -1
end if

//controllo se ci sono dati per l'Esposizione corrente 
select count(*)
into :ll_count
from tab_richieste_metrature
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_divisione=:ls_cod_divisione_corrente;
		
if sqlca.sqlcode<0 then
	fs_errore = "Errore in controllo presenza metrature per l'Esposizione corrente: "+sqlca.sqlerrtext
	return -1
end if

if ll_count>0 then
	if g_mb.confirm("Esistono già metrature per l'Esposizione corrente. Cancello e re-inserisco") then
	else
		fs_errore = "Operazione annullata dall'utente!"
		return -1
	end if
end if

if g_mb.confirm("Eseguire la duplicazione da Esposizione '"+ls_cod_divisione_origine+"' a Esposizione '"+ls_cod_divisione_corrente+"' (corrente)?") then
else
	fs_errore = "Operazione annullata dall'utente!"
	return -1
end if

delete from tab_richieste_metrature
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_divisione=:ls_cod_divisione_corrente;
		
if sqlca.sqlcode<0 then
	fs_errore = "Errore in pulizia metrature per Esposizione corrente: "+sqlca.sqlerrtext
	return -1
end if

insert into tab_richieste_metrature
(cod_azienda, cod_divisione, cod_tipo_richiesta, limite_mq, quota_servizio)
select 	cod_azienda,
			:ls_cod_divisione_corrente,
			cod_tipo_richiesta,
			limite_mq,
			quota_servizio
from tab_richieste_metrature
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_divisione=:ls_cod_divisione_origine;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in duplicazione: "+sqlca.sqlerrtext
	return -1
end if

return 1
end function

on w_tab_richieste_metrature.create
int iCurrent
call super::create
this.dw_det=create dw_det
this.dw_folder_search=create dw_folder_search
this.dw_lista=create dw_lista
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det
this.Control[iCurrent+2]=this.dw_folder_search
this.Control[iCurrent+3]=this.dw_lista
this.Control[iCurrent+4]=this.dw_ricerca
end on

on w_tab_richieste_metrature.destroy
call super::destroy
destroy(this.dw_det)
destroy(this.dw_folder_search)
destroy(this.dw_lista)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject l_objects[]

dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_options(sqlca, pcca.null_object, c_noretrieveonopen, c_default)

dw_det.set_dw_options(sqlca, dw_lista, c_sharedata + c_scrollparent, c_default)

iuo_dw_main = dw_lista

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, dw_folder_search.c_foldertableft)								 

l_objects[1] = dw_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_ricerca
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)

// Filtri
l_criteriacolumn[1] = "cod_divisione"
l_criteriacolumn[2] = "cod_tipo_richiesta"

l_searchtable[1] = "tab_richieste_metrature"
l_searchtable[2] = "tab_richieste_metrature"

l_searchcolumn[1] = "cod_divisione"
l_searchcolumn[2] = "cod_tipo_richiesta"

dw_ricerca.fu_wiredw(l_criteriacolumn[], dw_lista, l_searchtable[], l_searchcolumn[], SQLCA)

postevent("ue_imposta_esposizione_corrente")
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ricerca, &
				  "cod_tipo_richiesta", &
					sqlca, &
				  "tab_tipi_richieste", &
				  "cod_tipo_richiesta", &
				  "des_tipo_richiesta", &
				  "cod_azienda = '"+s_cs_xx.cod_azienda+"'")
				  
f_po_loaddddw_dw(dw_det, &
				  "cod_tipo_richiesta", &
					sqlca, &
				  "tab_tipi_richieste", &
				  "cod_tipo_richiesta", &
				  "des_tipo_richiesta", &
				  "cod_azienda = '"+s_cs_xx.cod_azienda+"'")
end event

type dw_det from uo_cs_xx_dw within w_tab_richieste_metrature
integer x = 5
integer y = 640
integer width = 2789
integer height = 456
integer taborder = 20
string dataobject = "d_richieste_metrature_det"
end type

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_1"
		dw_det.change_dw_current()
		s_cs_xx.parametri.parametro_uo_dw_1 = dw_det
		s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
		if not isvalid(w_divisioni_ricerca) then
			window_open(w_divisioni_ricerca, 0)
		end if
		w_divisioni_ricerca.show()

		
end choose
end event

type dw_folder_search from u_folder within w_tab_richieste_metrature
integer x = 27
integer width = 2770
integer height = 600
integer taborder = 10
end type

type dw_lista from uo_cs_xx_dw within w_tab_richieste_metrature
integer x = 142
integer y = 40
integer width = 2624
integer height = 556
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_richieste_metrature_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i,ll_num_richiedente


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if

next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_delete;call super::pcd_delete;dw_det.object.b_1.enabled = false
dw_ricerca.object.b_duplica.enabled= true
end event

event pcd_modify;call super::pcd_modify;dw_det.object.b_1.enabled = true
dw_ricerca.object.b_duplica.enabled= false
end event

event pcd_new;call super::pcd_new;dw_det.object.b_1.enabled = true
dw_ricerca.object.b_duplica.enabled= false
end event

event pcd_view;call super::pcd_view;dw_det.object.b_1.enabled = false
dw_ricerca.object.b_duplica.enabled= true
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_i,ll_num_richiedente


for ll_i = 1 to this.rowcount()

	if isnull(this.getitemstring(ll_i, "cod_tipo_richiesta"))  then	
		g_mb.error("Il campo Tipo Richiesta è obbligatorio!")
		pcca.error = c_fatal
		return
	end if
	
	if isnull(this.getitemnumber(ll_i, "limite_mq"))  then	
		g_mb.error("Il campo Limite MQ è obbligatorio!")
		pcca.error = c_fatal
		return
	end if
	
next
end event

type dw_ricerca from u_dw_search within w_tab_richieste_metrature
integer x = 137
integer y = 40
integer width = 2578
integer height = 524
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_richieste_metrature_ricerca"
boolean border = false
end type

event clicked;string ls_errore

choose case dwo.name
		
	case "b_divisione"
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
		s_cs_xx.parametri.parametro_uo_dw_search = dw_ricerca
		s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
		s_cs_xx.parametri.parametro_w_cs_xx_1 = parent
		if not isvalid(w_divisioni_ricerca) then
			window_open(w_divisioni_ricerca, 0)
		end if
		w_divisioni_ricerca.show()


	case "b_cerca"
		wf_imposta_ricerca()


	case "b_annulla"
		dw_ricerca.fu_Reset()
		
		
	case "b_divisione_origine"
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
		s_cs_xx.parametri.parametro_uo_dw_search = dw_ricerca
		s_cs_xx.parametri.parametro_s_1 = "cod_divisione_duplica"
		s_cs_xx.parametri.parametro_w_cs_xx_1 = parent
		if not isvalid(w_divisioni_ricerca) then
			window_open(w_divisioni_ricerca, 0)
		end if
		w_divisioni_ricerca.show()
		
		
	case "b_duplica"
		if wf_duplica_per_divisione(ls_errore)<0 then
			rollback;
			g_mb.error(ls_errore)
		else
			commit;
			g_mb.show("Duplicazione effettuata con successo!")
			wf_imposta_ricerca()
		end if
		
end choose
end event

