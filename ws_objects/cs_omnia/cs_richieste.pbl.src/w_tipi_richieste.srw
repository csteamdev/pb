﻿$PBExportHeader$w_tipi_richieste.srw
forward
global type w_tipi_richieste from w_cs_xx_principale
end type
type cb_utenti from commandbutton within w_tipi_richieste
end type
type dw_det from uo_cs_xx_dw within w_tipi_richieste
end type
type cb_folder from commandbutton within w_tipi_richieste
end type
type dw_lista from uo_cs_xx_dw within w_tipi_richieste
end type
end forward

global type w_tipi_richieste from w_cs_xx_principale
integer width = 2446
integer height = 2496
string title = "Tipi Richieste"
cb_utenti cb_utenti
dw_det dw_det
cb_folder cb_folder
dw_lista dw_lista
end type
global w_tipi_richieste w_tipi_richieste

type variables
private:
	string is_cod_cat_risorse_esterne
end variables

on w_tipi_richieste.create
int iCurrent
call super::create
this.cb_utenti=create cb_utenti
this.dw_det=create dw_det
this.cb_folder=create cb_folder
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_utenti
this.Control[iCurrent+2]=this.dw_det
this.Control[iCurrent+3]=this.cb_folder
this.Control[iCurrent+4]=this.dw_lista
end on

on w_tipi_richieste.destroy
call super::destroy
destroy(this.cb_utenti)
destroy(this.dw_det)
destroy(this.cb_folder)
destroy(this.dw_lista)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det, &
	"cod_tipo_off_ven", &
	sqlca, &
	"tab_tipi_off_ven", &
	"cod_tipo_off_ven", &
	"des_tipo_off_ven", &
	"cod_azienda = '"+s_cs_xx.cod_azienda+"'")
				  
f_po_loaddddw_dw(dw_det, &
	"cod_tipo_lista_dist", &
	sqlca, &
	"tab_tipi_liste_dist", &
	"cod_tipo_lista_dist", &
	"descrizione",&
	"")
	
f_po_loaddddw_dw( dw_det, &
	"cod_lista_dist", &
	sqlca, &
	"tes_liste_dist", &
	"cod_lista_dist", &
	"des_lista_dist", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
	

f_po_loaddddw_dw(dw_det, &
	"cod_documento", &
	sqlca, &
	"tab_documenti", &
	"cod_documento", &
	"des_documento", &
	"cod_azienda = '"+s_cs_xx.cod_azienda+"'")
				  
f_po_loaddddw_dw(dw_det, &
	"cod_tipo_det_ven", &
	sqlca, &
	"tab_tipi_det_ven", &
	"cod_tipo_det_ven", &
	"des_tipo_det_ven", &
	"cod_azienda = '"+s_cs_xx.cod_azienda+"' and flag_tipo_det_ven not in ('M','C')")
				  
f_po_loaddddw_dw( dw_det, &
	"cod_utente_responsabile", &
	sqlca, &
	"utenti", &
	"cod_utente", &
	"nome_cognome", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
	
f_po_loaddddw_dw( dw_det, &
	"cod_cat_risorse_esterne", &
	sqlca, &
	"tab_cat_risorse_esterne", &
	"cod_cat_risorse_esterne", &
	"des_cat_risorse_esterne", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

end event

event pc_setwindow;call super::pc_setwindow;
dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_options(sqlca, pcca.null_object, c_default, c_default)

iuo_dw_main = dw_lista

dw_det.set_dw_options(sqlca, dw_lista, c_sharedata + c_scrollparent, c_default)
end event

type cb_utenti from commandbutton within w_tipi_richieste
integer x = 1920
integer y = 2304
integer width = 448
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Utenti Notifiche"
end type

event clicked;window_open_parm(w_tipi_richieste_utenti, -1, dw_lista)
end event

type dw_det from uo_cs_xx_dw within w_tipi_richieste
event ue_dropdown pbm_dwndropdown
integer x = 23
integer y = 620
integer width = 2331
integer height = 1680
integer taborder = 10
string dataobject = "d_tipi_richieste_det"
boolean border = false
end type

event ue_dropdown;string			ls_column_name, ls_cod_tipo_lista_dist


ls_column_name = getcolumnname()


choose case ls_column_name
	case "cod_lista_dist"
		
		ls_cod_tipo_lista_dist = getitemstring(getrow(), "cod_tipo_lista_dist")
		
		if not isnull(ls_cod_tipo_lista_dist) and ls_cod_tipo_lista_dist<>"" then
			//filtra per tipo lista distribuzione
			f_po_loaddddw_dw(	dw_det, &
											"cod_lista_dist", &
											sqlca, &
											"tes_liste_dist", &
											"cod_lista_dist", &
											"des_lista_dist", &
											"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + ls_cod_tipo_lista_dist + "' ")
				
		else
			//mostra tutte le liste di distribuzione
			f_po_loaddddw_dw( 	dw_det, &
											"cod_lista_dist", &
											sqlca, &
											"tes_liste_dist", &
											"cod_lista_dist", &
											"des_lista_dist", &
											"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
		end if
			
			
	//---------------------------------------------------------------------------------------------------
	case "cod_lista_dist_notifica"
		
		ls_cod_tipo_lista_dist = getitemstring(getrow(), "cod_tipo_lista_dist_notifica")
		
		if not isnull(ls_cod_tipo_lista_dist) and ls_cod_tipo_lista_dist<>"" then
			//filtra per tipo lista distribuzione
			f_po_loaddddw_dw(	dw_det, &
											"cod_lista_dist_notifica", &
											sqlca, &
											"tes_liste_dist", &
											"cod_lista_dist", &
											"des_lista_dist", &
											"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + ls_cod_tipo_lista_dist + "' ")
				
		else
			//mostra tutte le liste di distribuzione
			f_po_loaddddw_dw( 	dw_det, &
											"cod_lista_dist_notifica", &
											sqlca, &
											"tes_liste_dist", &
											"cod_lista_dist", &
											"des_lista_dist", &
											"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
		end if
		
		
end choose
	
end event

event itemchanged;call super::itemchanged;string 				ls_null, ls_cod_tipo_lista_dist
integer				li_count

setnull(ls_null)
setnull(li_count)

if i_extendmode then
	
	choose case i_colname
			
		//---------------------------------------------------------------------------------------------------
		case "cod_tipo_lista_dist"
				
			setitem( getrow(), "cod_lista_dist", ls_null)
				
			f_po_loaddddw_dw( dw_det, &
									"cod_lista_dist", &
									sqlca, &
									"tes_liste_dist", &
									"cod_lista_dist", &
									"des_lista_dist", &
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + i_coltext + "' ")
									
									
		//---------------------------------------------------------------------------------------------------
		case "cod_tipo_lista_dist_notifica"
				
			setitem( getrow(), "cod_lista_dist_notifica", ls_null)
				
			f_po_loaddddw_dw( dw_det, &
									"cod_lista_dist_notifica", &
									sqlca, &
									"tes_liste_dist", &
									"cod_lista_dist", &
									"des_lista_dist", &
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + i_coltext + "' ")
		
		//---------------------------------------------------------------------------------------------------
		case "cod_lista_dist"
			
			if data <>"" then
				ls_cod_tipo_lista_dist = getitemstring(getrow(), "cod_tipo_lista_dist")
				if isnull(ls_cod_tipo_lista_dist) or ls_cod_tipo_lista_dist="" then
					g_mb.warning("Selezionare prima il codice del tipo lista distribuzione!")
					return 1
				end if
				
				select count(*)
				into :li_count
				from tes_liste_dist
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							cod_tipo_lista_dist=:ls_cod_tipo_lista_dist and
							cod_lista_dist=:data;
							
				if li_count>0 then
				else
					g_mb.warning("Hai selezionato un codice lista distribuzione non previsto per la tipologia lista dist. selezionata!")
					return 1
				end if		
			end if
			
			
		//---------------------------------------------------------------------------------------------------
		case "cod_lista_dist_notifica"
			
			if data <>"" then
				ls_cod_tipo_lista_dist = getitemstring(getrow(), "cod_tipo_lista_dist_notifica")
				if isnull(ls_cod_tipo_lista_dist) or ls_cod_tipo_lista_dist="" then
					g_mb.warning("Selezionare prima il codice del tipo lista distribuzione di notifica!")
					return 1
				end if
				
				select count(*)
				into :li_count
				from tes_liste_dist
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							cod_tipo_lista_dist=:ls_cod_tipo_lista_dist and
							cod_lista_dist=:data;
							
				if li_count>0 then
				else
					g_mb.warning("Hai selezionato un codice lista distribuzione notifica non previsto per la tipologia lista dist. notifica selezionata!")
					return 1
				end if		
			end if
			
		case "cod_cat_risorse_esterne"
			f_po_loaddddw_dw( dw_det, &
				"cod_risorsa_esterna", &
				sqlca, &
				"anag_risorse_esterne", &
				"cod_risorsa_esterna", &
				"descrizione", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "' AND cod_cat_risorse_esterne='" + data + "' ")
			
			
	end choose
	
end if


end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;//string ls_cod_tipo_richiesta
long   l_errore, ll_i

//ls_cod_tipo_richiesta= i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_richiesta")
	
for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_cat_risorse_esterne, ls_where

if currentrow > 0 then
	ls_cod_cat_risorse_esterne = getitemstring(currentrow, "cod_cat_risorse_esterne")
	
	if isnull(ls_cod_cat_risorse_esterne) then
		
		f_po_loaddddw_dw( dw_det, &
					"cod_risorsa_esterna", &
					sqlca, &
					"anag_risorse_esterne", &
					"cod_risorsa_esterna", &
					"descrizione", &
					"cod_azienda = ' Z ' ")
		
	elseif ls_cod_cat_risorse_esterne <> is_cod_cat_risorse_esterne then
		
		is_cod_cat_risorse_esterne = ls_cod_cat_risorse_esterne
		f_po_loaddddw_dw( dw_det, &
					"cod_risorsa_esterna", &
					sqlca, &
					"anag_risorse_esterne", &
					"cod_risorsa_esterna", &
					"descrizione", &
					"cod_azienda = '" + s_cs_xx.cod_azienda + "' AND cod_cat_risorse_esterne='" + ls_cod_cat_risorse_esterne + "' ")
		
	end if
end if
end event

type cb_folder from commandbutton within w_tipi_richieste
integer x = 1531
integer y = 2304
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Folder Attivi"
end type

event clicked;long   ll_cont
string ls_cod_tipo_richiesta

ls_cod_tipo_richiesta = dw_lista.getitemstring(dw_lista.getrow(),"cod_tipo_richiesta")

select count(*)
into   :ll_cont
from   tab_tipi_richieste_folder
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_richiesta = :ls_cod_tipo_richiesta;

if ll_cont < 1 then
	
	insert into tab_tipi_richieste_folder
	(cod_azienda,
	 progressivo,
	 cod_tipo_richiesta,
	 flag_blocco)
	select cod_azienda,
	  progressivo,
	 :ls_cod_tipo_richiesta,
	 'N'
	 from tab_tipi_richieste_dw;
	 
	 commit;
	 
end if
	 
window_open_parm(w_tipi_richieste_folder, -1, dw_lista)
end event

type dw_lista from uo_cs_xx_dw within w_tipi_richieste
integer y = 20
integer width = 2377
integer height = 580
integer taborder = 10
string dataobject = "d_tipi_richieste"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;//string ls_cod_tipo_richiesta
long   l_errore, ll_i

//ls_cod_tipo_richiesta= i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_richiesta")
	
for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event updatestart;call super::updatestart;long			ll_index

string			ls_cod_tipo_richiesta


for ll_index=1 to deletedcount()
	ls_cod_tipo_richiesta = this.getitemstring(ll_index, "cod_tipo_richiesta", delete!, true)
	
	delete from tab_tipi_richieste_folder
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_richiesta=:ls_cod_tipo_richiesta;
	
next
end event

