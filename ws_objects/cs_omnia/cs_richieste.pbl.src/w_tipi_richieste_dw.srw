﻿$PBExportHeader$w_tipi_richieste_dw.srw
forward
global type w_tipi_richieste_dw from w_cs_xx_principale
end type
type dw_tipi_richieste_dw from uo_cs_xx_dw within w_tipi_richieste_dw
end type
end forward

global type w_tipi_richieste_dw from w_cs_xx_principale
integer width = 1897
integer height = 1392
string title = "Tipi Richieste Datawindow"
dw_tipi_richieste_dw dw_tipi_richieste_dw
end type
global w_tipi_richieste_dw w_tipi_richieste_dw

on w_tipi_richieste_dw.create
int iCurrent
call super::create
this.dw_tipi_richieste_dw=create dw_tipi_richieste_dw
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_richieste_dw
end on

on w_tipi_richieste_dw.destroy
call super::destroy
destroy(this.dw_tipi_richieste_dw)
end on

event pc_setwindow;call super::pc_setwindow;dw_tipi_richieste_dw.set_dw_key("cod_azienda")
dw_tipi_richieste_dw.set_dw_options(sqlca, &
                        			       pcca.null_object, &
			                               c_default, &
			                               c_default)
//dw_tab_richiedenti.set_dw_options(sqlca, &
////                             dw_tab_richiedenti_lista, &
////                             c_sharedata + c_scrollparent, &
////                             c_default)
////
iuo_dw_main = dw_tipi_richieste_dw
end event

type dw_tipi_richieste_dw from uo_cs_xx_dw within w_tipi_richieste_dw
integer x = 14
integer y = 12
integer width = 1829
integer height = 1260
integer taborder = 10
string dataobject = "d_tipi_richieste_dw"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;
long ll_i, ll_progressivo


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	if isnull(this.getitemnumber(ll_i, "progressivo")) or this.getitemnumber(ll_i, "progressivo") < 1 then	
		select max(progressivo)
		into   :ll_progressivo
		from   tab_tipi_richieste_dw
		where  cod_azienda = :s_cs_xx.cod_azienda;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return 
		end if

		if isnull(ll_progressivo) or ll_progressivo = 0 then 
			ll_progressivo = 1
		else 
			ll_progressivo++
		end if
		setitem(getrow(), "progressivo", ll_progressivo)
	end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end event

