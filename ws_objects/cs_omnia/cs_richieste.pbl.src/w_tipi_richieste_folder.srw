﻿$PBExportHeader$w_tipi_richieste_folder.srw
forward
global type w_tipi_richieste_folder from w_cs_xx_principale
end type
type dw_tipi_richieste_folder from uo_cs_xx_dw within w_tipi_richieste_folder
end type
end forward

global type w_tipi_richieste_folder from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 1906
integer height = 1500
string title = "Tipi Richieste"
dw_tipi_richieste_folder dw_tipi_richieste_folder
end type
global w_tipi_richieste_folder w_tipi_richieste_folder

on w_tipi_richieste_folder.create
int iCurrent
call super::create
this.dw_tipi_richieste_folder=create dw_tipi_richieste_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_richieste_folder
end on

on w_tipi_richieste_folder.destroy
call super::destroy
destroy(this.dw_tipi_richieste_folder)
end on

event pc_setwindow;call super::pc_setwindow;dw_tipi_richieste_folder.set_dw_key("cod_azienda")
dw_tipi_richieste_folder.set_dw_key("cod_tipo_richiesta")
dw_tipi_richieste_folder.set_dw_options(sqlca, &
											i_openparm, &
											 c_nonew + c_nodelete, &
											 c_default)

iuo_dw_main = dw_tipi_richieste_folder
end event

type dw_tipi_richieste_folder from uo_cs_xx_dw within w_tipi_richieste_folder
integer width = 1810
integer height = 1280
integer taborder = 10
string dataobject = "d_tipi_richieste_folder"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;string ls_cod_tipo_richiesta
long   l_errore, ll_i

ls_cod_tipo_richiesta= i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_richiesta")
	
for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_tipo_richiesta")) then
      this.setitem(ll_i, "cod_tipo_richiesta", s_cs_xx.cod_azienda)
   end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_tipo_richiesta

ls_cod_tipo_richiesta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_richiesta")
ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_richiesta)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
	
end event

