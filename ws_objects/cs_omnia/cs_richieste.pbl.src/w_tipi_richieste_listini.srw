﻿$PBExportHeader$w_tipi_richieste_listini.srw
forward
global type w_tipi_richieste_listini from w_cs_xx_principale
end type
type dw_tipi_richieste_listini from uo_cs_xx_dw within w_tipi_richieste_listini
end type
type dw_tipi_richieste_listini_lista from uo_cs_xx_dw within w_tipi_richieste_listini
end type
type dw_folder from u_folder within w_tipi_richieste_listini
end type
type dw_ricerca from u_dw_search within w_tipi_richieste_listini
end type
end forward

global type w_tipi_richieste_listini from w_cs_xx_principale
integer width = 3182
integer height = 1684
string title = "Listini Servizi Aggiuntivi"
dw_tipi_richieste_listini dw_tipi_richieste_listini
dw_tipi_richieste_listini_lista dw_tipi_richieste_listini_lista
dw_folder dw_folder
dw_ricerca dw_ricerca
end type
global w_tipi_richieste_listini w_tipi_richieste_listini

type variables
string is_sql_base = ""
end variables

on w_tipi_richieste_listini.create
int iCurrent
call super::create
this.dw_tipi_richieste_listini=create dw_tipi_richieste_listini
this.dw_tipi_richieste_listini_lista=create dw_tipi_richieste_listini_lista
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_richieste_listini
this.Control[iCurrent+2]=this.dw_tipi_richieste_listini_lista
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_ricerca
end on

on w_tipi_richieste_listini.destroy
call super::destroy
destroy(this.dw_tipi_richieste_listini)
destroy(this.dw_tipi_richieste_listini_lista)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[], lw_vuoto[]

//dw_tipi_richieste_listini_lista.set_dw_key("cod_azienda")
dw_tipi_richieste_listini_lista.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_default + c_noretrieveonopen, &
                                 c_default)
dw_tipi_richieste_listini.set_dw_options(sqlca, &
                               dw_tipi_richieste_listini_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)
										 								 
iuo_dw_main = dw_tipi_richieste_listini_lista


lw_oggetti[1] = dw_tipi_richieste_listini_lista
dw_folder.fu_assigntab(1, "L.", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_ricerca
dw_folder.fu_assigntab(2, "R.", lw_oggetti[])

dw_folder.fu_folderoptions(dw_folder.c_defaultheight, dw_folder.c_foldertableft)
dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(2)

is_sql_base = dw_tipi_richieste_listini_lista.getsqlselect()

end event

event pc_setddlb;call super::pc_setddlb;
f_po_loaddddw_dw(	dw_ricerca, &
                 				"cod_tipo_richiesta", &
                 				sqlca, &
                 				"tab_tipi_richieste", &
                 				"cod_tipo_richiesta", &
                				 "des_tipo_richiesta", &
                 				"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
									  
f_po_loaddddw_dw(dw_tipi_richieste_listini, "cod_tipo_richiesta",sqlca,&
									  "tab_tipi_richieste","cod_tipo_richiesta","des_tipo_richiesta",&
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_tipi_richieste_listini from uo_cs_xx_dw within w_tipi_richieste_listini
integer x = 27
integer y = 912
integer width = 3067
integer taborder = 30
string dataobject = "d_tipi_richieste_listini_1"
borderstyle borderstyle = styleraised!
end type

type dw_tipi_richieste_listini_lista from uo_cs_xx_dw within w_tipi_richieste_listini
integer x = 169
integer y = 52
integer width = 2926
integer height = 816
integer taborder = 20
string dataobject = "d_tipi_richieste_listini_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_tipo_richiesta, ls_cod_listino, ls_des_listino
decimal ld_prezzo_unitario
long ll_row

ll_row = getrow()

if ll_row>0 then
	//------------------------------------------------------------------------------------------------------------
	ls_cod_listino = getitemstring(ll_row, "cod_listino")
	if isnull(ls_cod_listino) or ls_cod_listino="" then
		g_mb.messagebox("OMNIA","Codice listino obbligatorio!")
		setcolumn("cod_listino")
		pcca.error = c_fatal
		return
	end if
	
	//------------------------------------------------------------------------------------------------------------
	ls_cod_tipo_richiesta = getitemstring(ll_row, "cod_tipo_richiesta")
	if isnull(ls_cod_tipo_richiesta) or ls_cod_tipo_richiesta="" then
		g_mb.messagebox("OMNIA","Tipo richiesta obbligatoria!")
		setcolumn("cod_tipo_richiesta")
		pcca.error = c_fatal
		return
	end if
	
	//------------------------------------------------------------------------------------------------------------
	ls_cod_listino = getitemstring(ll_row, "des_listino")
	if isnull(ls_cod_listino) or ls_cod_listino="" then
		g_mb.messagebox("OMNIA","Descrizione Listino obbligatorio!")
		setcolumn("des_listino")
		pcca.error = c_fatal
		return
	end if
	
	//------------------------------------------------------------------------------------------------------------
	ld_prezzo_unitario = getitemdecimal(ll_row, "prezzo_unitario")
	if isnull(ld_prezzo_unitario) or ld_prezzo_unitario<=0 then
		g_mb.messagebox("OMNIA","Prezzo Unitario obbligatorio!")
		setcolumn("prezzo_unitario")
		pcca.error = c_fatal
		return
	end if
	
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i,ll_num_richiedente


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tipo_richiesta, ls_sql, ls_error
long ll_errore

ls_sql = is_sql_base
ls_sql += " where tab_tipi_richieste_listini.cod_azienda='"+s_cs_xx.cod_azienda+"' "

dw_ricerca.accepttext()
ls_cod_tipo_richiesta = dw_ricerca.getitemstring(1, "cod_tipo_richiesta")
if not isnull(ls_cod_tipo_richiesta) and ls_cod_tipo_richiesta <> "" then
	ls_sql += "and tab_tipi_richieste_listini.cod_tipo_richiesta='" + ls_cod_tipo_richiesta + "' "
end if
// ----

dw_tipi_richieste_listini_lista.setsqlselect(ls_sql)
ll_errore = dw_tipi_richieste_listini_lista.retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type dw_folder from u_folder within w_tipi_richieste_listini
integer x = 27
integer y = 20
integer width = 3104
integer height = 876
integer taborder = 10
boolean border = false
end type

type dw_ricerca from u_dw_search within w_tipi_richieste_listini
integer x = 197
integer y = 104
integer width = 2245
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_tipi_richieste_listini_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string ls_null

choose case dwo.name
	case "b_cerca"
		dw_folder.fu_selecttab(1)
		dw_tipi_richieste_listini_lista.change_dw_current()
		parent.triggerevent("pc_retrieve")

	case "b_annulla"
		setnull(ls_null)
		dw_ricerca.setitem(1, "cod_tipo_richiesta", ls_null)
		
end choose
end event

