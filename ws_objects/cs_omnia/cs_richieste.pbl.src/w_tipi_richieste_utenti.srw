﻿$PBExportHeader$w_tipi_richieste_utenti.srw
forward
global type w_tipi_richieste_utenti from w_cs_xx_principale
end type
type dw_list from uo_cs_xx_dw within w_tipi_richieste_utenti
end type
end forward

global type w_tipi_richieste_utenti from w_cs_xx_principale
integer width = 2011
integer height = 1644
string title = "Parametri Aziendali"
dw_list dw_list
end type
global w_tipi_richieste_utenti w_tipi_richieste_utenti

type variables
private:
	string is_cod_tipo_richiesta
end variables

on w_tipi_richieste_utenti.create
int iCurrent
call super::create
this.dw_list=create dw_list
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_list
end on

on w_tipi_richieste_utenti.destroy
call super::destroy
destroy(this.dw_list)
end on

event pc_setwindow;call super::pc_setwindow;dw_list.set_dw_key("cod_azienda")
dw_list.set_dw_options(sqlca, i_openparm, c_default, c_default)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw( dw_list, &
	"cod_utente", &
	sqlca, &
	"utenti", &
	"cod_utente", &
	"nome_cognome", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
end event

type dw_list from uo_cs_xx_dw within w_tipi_richieste_utenti
integer x = 23
integer y = 20
integer width = 1920
integer height = 1500
integer taborder = 10
string dataobject = "d_tipi_richieste_utenti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_des_tipo_richiesta
long ll_errore

is_cod_tipo_richiesta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_richiesta")
ls_des_tipo_richiesta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "des_tipo_richiesta")

parent.title = "Utenti tipo richiesta " + is_cod_tipo_richiesta + " - " + ls_des_tipo_richiesta

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_tipo_richiesta)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
	
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemstring(ll_i, "cod_tipo_richiesta")) then
		setitem(ll_i, "cod_tipo_richiesta", is_cod_tipo_richiesta)
	end if
next
end event

