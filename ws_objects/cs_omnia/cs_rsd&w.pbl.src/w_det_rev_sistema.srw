﻿$PBExportHeader$w_det_rev_sistema.srw
$PBExportComments$Finestra Dettaglio Revisione Sistema Qualità
forward
global type w_det_rev_sistema from w_cs_xx_principale
end type
type dw_det_rev_sistema_lista from uo_cs_xx_dw within w_det_rev_sistema
end type
type cb_calcola from commandbutton within w_det_rev_sistema
end type
type dw_det_rev_sistema_det from uo_cs_xx_dw within w_det_rev_sistema
end type
end forward

global type w_det_rev_sistema from w_cs_xx_principale
integer width = 2583
integer height = 1672
string title = "Dettaglio Revisione Sistema Qualità"
dw_det_rev_sistema_lista dw_det_rev_sistema_lista
cb_calcola cb_calcola
dw_det_rev_sistema_det dw_det_rev_sistema_det
end type
global w_det_rev_sistema w_det_rev_sistema

type variables
boolean ib_in_new
end variables

forward prototypes
public subroutine wf_flag_consuntivo (string as_flag_consuntivo)
public function character wf_tipo_indicatore (string as_cod_indicatore)
end prototypes

public subroutine wf_flag_consuntivo (string as_flag_consuntivo);if as_flag_consuntivo = "S" then
	dw_det_rev_sistema_det.Object.anno_consuntivo.visible = 1
	dw_det_rev_sistema_det.Object.anno_consuntivo_t.visible = 1
else
	dw_det_rev_sistema_det.Object.anno_consuntivo.visible = 0
	dw_det_rev_sistema_det.Object.anno_consuntivo_t.visible = 0
end if

end subroutine

public function character wf_tipo_indicatore (string as_cod_indicatore);string ls_tipo_indicatore

  SELECT tab_indicatori.flag_tipo_indicatore
    INTO :ls_tipo_indicatore  
    FROM tab_indicatori
	 WHERE tab_indicatori.cod_indicatore = :as_cod_indicatore ;

choose case ls_tipo_indicatore
	case "D"  // Descrittivo
		dw_det_rev_sistema_det.Object.des_target.Visible = 1
		dw_det_rev_sistema_det.Object.des_effettivo.Visible = 1
		dw_det_rev_sistema_det.Object.des_target_t.Visible = 1
		dw_det_rev_sistema_det.Object.des_effettivo_t.Visible = 1
		dw_det_rev_sistema_det.Object.numero_target.Visible = 0
		dw_det_rev_sistema_det.Object.numero_effettivo.Visible = 0
		dw_det_rev_sistema_det.Object.numero_target_t.Visible = 0
		dw_det_rev_sistema_det.Object.numero_effettivo_t.Visible = 0
		dw_det_rev_sistema_det.Object.calc_target.Visible = 0
		dw_det_rev_sistema_det.Object.calc_effettivo.Visible = 0
		dw_det_rev_sistema_det.Object.calc_target_t.Visible = 0
		dw_det_rev_sistema_det.Object.calc_effettivo_t.Visible = 0
		cb_calcola.visible = false
		
	case "N"  // Numerico/Frazionario
		dw_det_rev_sistema_det.Object.des_target.Visible = 0
		dw_det_rev_sistema_det.Object.des_effettivo.Visible = 0
		dw_det_rev_sistema_det.Object.des_target_t.Visible = 0
		dw_det_rev_sistema_det.Object.des_effettivo_t.Visible = 0
		dw_det_rev_sistema_det.Object.numero_target.Visible = 1
		dw_det_rev_sistema_det.Object.numero_effettivo.Visible = 1
		dw_det_rev_sistema_det.Object.numero_target_t.Visible = 1
		dw_det_rev_sistema_det.Object.numero_effettivo_t.Visible = 1
		dw_det_rev_sistema_det.Object.calc_target.Visible = 0
		dw_det_rev_sistema_det.Object.calc_effettivo.Visible = 0
		dw_det_rev_sistema_det.Object.calc_target_t.Visible = 0
		dw_det_rev_sistema_det.Object.calc_effettivo_t.Visible = 0
		cb_calcola.visible = false

	case "C"  // Calcolato
		dw_det_rev_sistema_det.Object.des_target.Visible = 0
		dw_det_rev_sistema_det.Object.des_effettivo.Visible = 0
		dw_det_rev_sistema_det.Object.des_target_t.Visible = 0
		dw_det_rev_sistema_det.Object.des_effettivo_t.Visible = 0
		dw_det_rev_sistema_det.Object.numero_target.Visible = 0
		dw_det_rev_sistema_det.Object.numero_effettivo.Visible = 0
		dw_det_rev_sistema_det.Object.numero_target_t.Visible = 0
		dw_det_rev_sistema_det.Object.numero_effettivo_t.Visible = 0
		dw_det_rev_sistema_det.Object.calc_target.Visible = 1
		dw_det_rev_sistema_det.Object.calc_effettivo.Visible = 1
		dw_det_rev_sistema_det.Object.calc_target_t.Visible = 1
		dw_det_rev_sistema_det.Object.calc_effettivo_t.Visible = 1
		cb_calcola.visible = true

end choose

return ls_tipo_indicatore
end function

event pc_setwindow;call super::pc_setwindow;dw_det_rev_sistema_lista.set_dw_key("cod_azienda")
dw_det_rev_sistema_lista.set_dw_key("cod_obiettivo")
dw_det_rev_sistema_lista.set_dw_key("cod_indicatore")
dw_det_rev_sistema_lista.set_dw_key("prog")
dw_det_rev_sistema_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)

dw_det_rev_sistema_det.set_dw_options(sqlca,dw_det_rev_sistema_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_det_rev_sistema_lista
end event

on w_det_rev_sistema.create
int iCurrent
call super::create
this.dw_det_rev_sistema_lista=create dw_det_rev_sistema_lista
this.cb_calcola=create cb_calcola
this.dw_det_rev_sistema_det=create dw_det_rev_sistema_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_rev_sistema_lista
this.Control[iCurrent+2]=this.cb_calcola
this.Control[iCurrent+3]=this.dw_det_rev_sistema_det
end on

on w_det_rev_sistema.destroy
call super::destroy
destroy(this.dw_det_rev_sistema_lista)
destroy(this.cb_calcola)
destroy(this.dw_det_rev_sistema_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_rev_sistema_lista,"cod_obiettivo",sqlca,&
                 "tab_obiettivi","cod_obiettivo","des_obiettivo",&
                 "tab_obiettivi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_det_rev_sistema_det,"cod_obiettivo",sqlca,&
                 "tab_obiettivi","cod_obiettivo","des_obiettivo",&
                 "tab_obiettivi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_rev_sistema_lista,"cod_indicatore",sqlca,&
                 "tab_indicatori","cod_indicatore","des_indicatore",&
                 "tab_indicatori.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_det_rev_sistema_det,"cod_indicatore",sqlca,&
                 "tab_indicatori","cod_indicatore","des_indicatore",&
                 "tab_indicatori.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_rev_sistema_det,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

event activate;call super::activate;integer li_row
li_row = dw_det_rev_sistema_lista.getrow()
wf_tipo_indicatore (dw_det_rev_sistema_lista.getitemstring(li_row, "cod_indicatore"))
end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_det_rev_sistema_lista.getrow()

if not( li_row > 0 and dw_det_rev_sistema_det.getitemstring(li_row,"cod_indicatore") <> "" ) then
	cb_calcola.enabled = False
end if

end event

type dw_det_rev_sistema_lista from uo_cs_xx_dw within w_det_rev_sistema
integer x = 23
integer y = 20
integer width = 2487
integer height = 500
integer taborder = 10
string dataobject = "d_det_rev_sistema_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_obiettivo, ls_cod_indicatore

ls_cod_obiettivo = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_obiettivo")
ls_cod_indicatore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_indicatore")
l_Error = Retrieve(s_cs_xx.cod_azienda, ls_cod_obiettivo, ls_cod_indicatore)
wf_tipo_indicatore (ls_cod_indicatore)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx
string ls_cod_obiettivo, ls_cod_indicatore

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

ls_cod_obiettivo = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_obiettivo")
FOR l_Idx = 1 TO RowCount()
   IF IsNull(getitemstring(l_Idx, "cod_obiettivo")) or getitemstring(l_Idx, "cod_obiettivo") = '' THEN
      SetItem(l_Idx, "cod_obiettivo", ls_cod_obiettivo)
   END IF
NEXT

ls_cod_indicatore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_indicatore")
FOR l_Idx = 1 TO RowCount()
   IF IsNull(getitemstring(l_Idx, "cod_indicatore")) or getitemstring(l_Idx, "cod_indicatore") = '' THEN
      SetItem(l_Idx, "cod_indicatore", ls_cod_indicatore)
   END IF
NEXT


end event

event updatestart;call super::updatestart;integer li_row
li_row = this.getrow()
if li_row > 0 and ib_in_new then

   string ls_cod_obiettivo, ls_cod_indicatore
	long ll_prog_riga_det_rev

   ls_cod_obiettivo = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_obiettivo")
   ls_cod_indicatore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_indicatore")

   select max(det_rev_sistema.prog_riga_det_rev)
     into :ll_prog_riga_det_rev
     from det_rev_sistema
     where (det_rev_sistema.cod_azienda = :s_cs_xx.cod_azienda) and
           (det_rev_sistema.cod_obiettivo = :ls_cod_obiettivo ) and
           (det_rev_sistema.cod_indicatore = :ls_cod_indicatore );

  if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_prog_riga_det_rev) then
      ll_prog_riga_det_rev = 1
   else
      ll_prog_riga_det_rev = ll_prog_riga_det_rev + 1
   end if
   this.SetItem (li_row, "prog_riga_det_rev", ll_prog_riga_det_rev)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (li_row ,"prog_riga_det_rev", 10)
   end if

   ib_in_new = false
end if
end event

event pcd_delete;call super::pcd_delete;ib_in_new = false
end event

event pcd_modify;call super::pcd_modify;ib_in_new = false
cb_calcola.enabled = true

end event

event pcd_new;call super::pcd_new;string ls_cod_obiettivo, ls_cod_indicatore
datetime ldd_oggi

ls_cod_obiettivo = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_obiettivo")
ls_cod_indicatore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_indicatore")

dw_det_rev_sistema_lista.setitem(getrow(),"cod_obiettivo",ls_cod_obiettivo)
dw_det_rev_sistema_lista.setitem(getrow(),"cod_indicatore",ls_cod_indicatore)
dw_det_rev_sistema_lista.setitem(getrow(),"flag_consuntivo","N")
dw_det_rev_sistema_lista.setitem(getrow(),"anno_consuntivo",f_anno_esercizio())
wf_flag_consuntivo("N")

ib_in_new = true
ldd_oggi = datetime(today())
dw_det_rev_sistema_lista.setitem(getrow(),"data_comp",ldd_oggi)
cb_calcola.enabled = true

end event

event pcd_save;call super::pcd_save;ib_in_new = false
cb_calcola.enabled = False

end event

event pcd_view;call super::pcd_view;ib_in_new = false
cb_calcola.enabled = false

end event

event pcd_pickedrow;call super::pcd_pickedrow;integer li_row
li_row = this.getrow()
if li_row > 0 then
	wf_flag_consuntivo (this.getitemstring(li_row, "flag_consuntivo"))
end if
end event

event pcd_validaterow;call super::pcd_validaterow;integer li_row, li_count, li_anno_consuntivo
string ls_cod_obiettivo, ls_cod_indicatore

li_row = this.getrow()
if li_row > 0 then
	
	if ( this.getitemstring(li_row,"flag_consuntivo") = 'S' ) then // Consuntivo
		li_anno_consuntivo = this.getitemnumber(li_row, "anno_consuntivo")
		if isnull(li_anno_consuntivo) then
			g_mb.messagebox("Revisione Sistema Qualità", "L'Indicazione dell' Anno è obbligatoria", StopSign!)
			pcca.error = c_valfailed
			return
		else
			ls_cod_obiettivo = this.getitemstring(li_row, "cod_obiettivo")
			ls_cod_indicatore = this.getitemstring(li_row, "cod_indicatore")
			SELECT count(*)
				 INTO :li_count
				 FROM det_rev_sistema
				 WHERE det_rev_sistema.cod_obiettivo = :ls_cod_obiettivo AND
						 det_rev_sistema.cod_indicatore = :ls_cod_indicatore AND
				 		 det_rev_sistema.flag_consuntivo = 'S' AND
						 det_rev_sistema.anno_consuntivo = :li_anno_consuntivo ;
			if li_count > 0 then // Trovato
				g_mb.messagebox("Revisione Sistema Qualità", "Dato Consuntivo per Quest'Anno già Presente", StopSign!)
				pcca.error = c_valfailed
				return
			end if
		end if
	else
		if isnull(this.getitemdatetime(li_row, "data_comp")) then
			g_mb.messagebox("Revisione Sistema Qualità", "L'indicazione della Data di Compilazione è Obbligatoria", StopSign!)
			pcca.error = c_valfailed
			return
		end if
	end if
end if
end event

type cb_calcola from commandbutton within w_det_rev_sistema
integer x = 1179
integer y = 920
integer width = 69
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;integer li_row
double ldd_risultato
string ls_cod_indicatore, ls_comp_codici, ls_cod_formula

li_row = dw_det_rev_sistema_det.getrow()
if li_row > 0 then
	ls_cod_indicatore = dw_det_rev_sistema_lista.getitemstring(li_row,"cod_indicatore")
	if isnull(ls_cod_indicatore) or ls_cod_indicatore = "" then 
		g_mb.messagebox("Revisione Sistema","Scegliere un Indicatore Prima di calcolare")
		return
	else
		select cod_formula
		 into :ls_cod_formula  
		 from tab_indicatori
		where cod_azienda = :s_cs_xx.cod_azienda and
			   cod_indicatore = :ls_cod_indicatore;

		if isnull(ls_cod_formula) or ls_cod_formula = "" then 
			g_mb.messagebox("Revisione Sistema","L'indicatore Selezionato non ha Formule")
			return
		else		
			select comp_codici
			 into :ls_comp_codici  
			 from tes_formule  
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_formula = :ls_cod_formula;
			
			if isnull(ls_comp_codici) or ls_comp_codici = "" then 
				g_mb.messagebox("Revisione Sistema","La Formula Selezionata non è Stata Generata")
				return
			else
				ldd_risultato = f_calcola_formula(ls_comp_codici)
				update tes_formule
				  set ult_risultato = :ldd_risultato  
				where ( cod_azienda = :s_cs_xx.cod_azienda ) and
						( cod_formula = :ls_cod_formula );
				dw_det_rev_sistema_det.setitem(li_row, "calc_effettivo", ldd_risultato)
				commit;
			end if
		end if
	end if
end if
end event

type dw_det_rev_sistema_det from uo_cs_xx_dw within w_det_rev_sistema
integer x = 23
integer y = 540
integer width = 2487
integer height = 1000
integer taborder = 20
string dataobject = "d_det_rev_sistema_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_colname = "flag_consuntivo" then
	if i_coltext = "S" then
		this.setitem(this.getrow(),"anno_consuntivo",year(today()))
	end if
	wf_flag_consuntivo(i_coltext)
end if
end event

