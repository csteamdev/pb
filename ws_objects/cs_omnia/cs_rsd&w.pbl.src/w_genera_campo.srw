﻿$PBExportHeader$w_genera_campo.srw
$PBExportComments$Finestra di Generazione SQL per Campi Formule
forward
global type w_genera_campo from w_cs_xx_risposta
end type
type cb_chiudi from commandbutton within w_genera_campo
end type
type cb_elimina_riga from commandbutton within w_genera_campo
end type
type ddlb_tabelle from dropdownlistbox within w_genera_campo
end type
type ddlb_colonne from dropdownlistbox within w_genera_campo
end type
type ddlb_funzioni from dropdownlistbox within w_genera_campo
end type
type st_1 from statictext within w_genera_campo
end type
type st_2 from statictext within w_genera_campo
end type
type st_3 from statictext within w_genera_campo
end type
type st_4 from statictext within w_genera_campo
end type
type dw_imposta_report from datawindow within w_genera_campo
end type
type st_5 from statictext within w_genera_campo
end type
end forward

global type w_genera_campo from w_cs_xx_risposta
integer width = 3223
integer height = 1456
string title = "Generazione Campo"
cb_chiudi cb_chiudi
cb_elimina_riga cb_elimina_riga
ddlb_tabelle ddlb_tabelle
ddlb_colonne ddlb_colonne
ddlb_funzioni ddlb_funzioni
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
dw_imposta_report dw_imposta_report
st_5 st_5
end type
global w_genera_campo w_genera_campo

type variables
boolean ib_cod_azienda
string is_db
long   il_aperte = 0, il_chiuse = 0
end variables

forward prototypes
public function string wf_formatta (string fs_campo, string fs_confronto)
public subroutine wf_agg_colonne (string fs_tabella)
public subroutine wf_apri_esistente (string fs_sql)
end prototypes

public function string wf_formatta (string fs_campo, string fs_confronto);// fs_campo     : colonna da cui estrarre il tipo col
// fs_confronto : stringa da formattare
string ls_col_type, ls_ret

choose case Upper(is_db)
	case "SYBASE_ASA"
		SELECT COLTYPE
			into :ls_col_type
			FROM SYS.SYSCOLUMNS
			WHERE CNAME = :fs_campo;
	case "ORACLE"
end choose

if upper(ls_col_type) = "CHAR" or upper(left(ls_col_type,4)) = "DATE" or & 
	upper(left(ls_col_type,4)) = "TIME" or upper(right(ls_col_type,4)) = "CHAR" then 
	ls_ret = "'" + fs_confronto + "'"
else
	ls_ret = fs_confronto
end if
return ls_ret
end function

public subroutine wf_agg_colonne (string fs_tabella);string ls_colonna
integer li_i

if fs_tabella = "" then return

fs_tabella = f_repl_space_with_underscore(fs_tabella)

ddlb_colonne.reset()
ddlb_colonne.text = " VALORE"
dw_imposta_report.reset()

li_i = 1
ib_cod_azienda = false
choose case Upper(is_db)
	case "SYBASE_ASA"
		DECLARE cur_colonne_syb CURSOR FOR
		SELECT DISTINCT PBCATCOL.PBC_CNAM
			FROM PBCATCOL
			WHERE PBCATCOL.PBC_TNAM = :fs_tabella;
			
		OPEN cur_colonne_syb;

		FETCH cur_colonne_syb INTO :ls_colonna;

		do while SQLCA.sqlcode = 0
			if Upper(ls_colonna) <> "COD_AZIENDA" and Upper(ls_colonna) <> "NOTE_ESTERNE" and &
				Upper(ls_colonna) <> "BLOB" and Upper(ls_colonna) <> "DOCUMENTO_ALLEGATO" then
				ddlb_colonne.additem(f_repl_underscore_with_space(ls_colonna))
				dw_imposta_report.SetValue("campo", li_i, f_repl_underscore_with_space(ls_colonna))
				li_i ++
			elseif Upper(ls_colonna) = "COD_AZIENDA" then
				ib_cod_azienda = true
			end if
			FETCH cur_colonne_syb INTO :ls_colonna;	
		loop
		CLOSE cur_colonne_syb;
	case "ORACLE"
		DECLARE cur_colonne_ora CURSOR FOR
		SELECT DISTINCT SYSTEM.PBCATCOL.PBC_CNAM
			FROM SYSTEM.PBCATCOL
			WHERE SYSTEM.PBCATCOL.PBC_TNAM = :fs_tabella;

		OPEN cur_colonne_ora;

		FETCH cur_colonne_ora INTO :ls_colonna;

		do while SQLCA.sqlcode = 0
			if Upper(ls_colonna) <> "COD_AZIENDA" and Upper(ls_colonna) <> "NOTE_ESTERNE" and &
				Upper(ls_colonna) <> "BLOB" and Upper(ls_colonna) <> "DOCUMENTO_ALLEGATO" then
				ddlb_colonne.additem(f_repl_underscore_with_space(ls_colonna))
				dw_imposta_report.SetValue("campo", li_i, f_repl_underscore_with_space(ls_colonna))
				li_i ++
			elseif Upper(ls_colonna) = "COD_AZIENDA" then
				ib_cod_azienda = true
			end if
			FETCH cur_colonne_ora INTO :ls_colonna;	
		loop
		CLOSE cur_colonne_ora;
case else
		g_mb.messagebox ("OMNIA", "DataBase Sconosciuto!")
end choose
end subroutine

public subroutine wf_apri_esistente (string fs_sql);// Funzione che mi setta i vari campi a partire da uno script SQL
// fs_sql : Script SQL da popolare

string ls_sql, ls_colonna, ls_funzione, ls_tabella, ls_campi[], ls_operatori[], ls_confronti[]
string ls_unioni[], ls_null
integer li_pos, li_i


setnull(ls_null)
ls_sql = fs_sql
f_estrai_token(ref ls_sql, " ")
li_pos = pos(ls_sql , "(")
if li_pos = 0 then
	ls_colonna = f_estrai_token(ref ls_sql, " ")
else
	ls_funzione = f_estrai_token(ref ls_sql, "(") + "()"
	ls_colonna = f_estrai_token(ref ls_sql, ")")
	f_estrai_token(ref ls_sql, " ")
end if
f_estrai_token(ref ls_sql, " ")
ls_tabella = f_estrai_token(ref ls_sql," ")
li_i = 1

f_estrai_token(ref ls_sql, " ")
do while len(ls_sql) > 0
//do while not f_estrai_token(ref ls_sql, " ") = ""
	ls_campi[li_i] = f_estrai_token(ref ls_sql, " ")
	ls_operatori[li_i] = f_estrai_token(ref ls_sql, " ")
	if ls_operatori[li_i] = "LIKE" then ls_operatori[li_i] = "="
	ls_confronti[li_i] = f_estrai_token(ref ls_sql, " ")
	li_pos = pos(ls_confronti[li_i], "'")
	do while li_pos > 0
		ls_confronti[li_i] = Replace(ls_confronti[li_i], li_pos, 1, "")
		li_pos = pos(ls_confronti[li_i], "'")
	loop
	if len(ls_sql) > 1 then
		ls_unioni[li_i] = f_estrai_token(ref ls_sql, " ")
	else
		ls_sql = ""
	end if
	li_i ++
loop

ddlb_tabelle.text = f_repl_underscore_with_space(ls_tabella)
wf_agg_colonne( f_repl_underscore_with_space(ls_tabella))
ddlb_funzioni.text = ls_funzione
ddlb_colonne.text = f_repl_underscore_with_space(ls_colonna)

for li_i = 1 to UpperBound(ls_unioni)
	if upper(ls_campi[li_i]) <> "COD_AZIENDA" then
		li_pos = dw_imposta_report.insertrow(0)
		if pos(ls_campi[li_i],"(",1) > 0 then
			dw_imposta_report.setitem(li_pos, "campo", f_repl_underscore_with_space(right(ls_campi[li_i],len(ls_campi[li_i])-1)))
			dw_imposta_report.setitem(li_pos, "apri_par", "(")
			il_aperte++
		else															
			dw_imposta_report.setitem(li_pos, "campo", f_repl_underscore_with_space(ls_campi[li_i]))
			dw_imposta_report.setitem(li_pos, "apri_par", ls_null)
		end if	
		dw_imposta_report.setitem(li_pos, "operatore", ls_operatori[li_i])
		if pos(ls_confronti[li_i],")",1) > 0 then
			dw_imposta_report.setitem(li_pos, "confronto", left(ls_confronti[li_i],len(ls_confronti[li_i])-1))
			dw_imposta_report.setitem(li_pos, "chiudi_par", ")")
			il_chiuse++
		else
			dw_imposta_report.setitem(li_pos, "confronto", ls_confronti[li_i])
			dw_imposta_report.setitem(li_pos, "chiudi_par", ls_null)
		end if	
		dw_imposta_report.setitem(li_pos, "unione", ls_unioni[li_i])
	end if
next
if upper(ls_campi[li_i]) <> "COD_AZIENDA" then
	li_pos = dw_imposta_report.insertrow(0)
	if pos(ls_campi[li_i],"(",1) > 0 then
		dw_imposta_report.setitem(li_pos, "campo", f_repl_underscore_with_space(right(ls_campi[li_i],len(ls_campi[li_i])-1)))
		dw_imposta_report.setitem(li_pos, "apri_par", "(")
		il_aperte++
	else															
		dw_imposta_report.setitem(li_pos, "campo", f_repl_underscore_with_space(ls_campi[li_i]))
		dw_imposta_report.setitem(li_pos, "apri_par", ls_null)
	end if
	dw_imposta_report.setitem(li_pos, "operatore", ls_operatori[li_i])
	if pos(ls_confronti[li_i],")",1) > 0 then
		dw_imposta_report.setitem(li_pos, "confronto", left(ls_confronti[li_i],len(ls_confronti[li_i])-1))
		dw_imposta_report.setitem(li_pos, "chiudi_par", ")")
		il_chiuse++
	else
		dw_imposta_report.setitem(li_pos, "confronto", ls_confronti[li_i])
		dw_imposta_report.setitem(li_pos, "chiudi_par", ls_null)
	end if
end if
//dw_imposta_report.insertrow(0)
end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_tabella,ls_valore
integer li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", is_db)

choose case Upper(is_db)

	case "SYBASE_ASA"
		DECLARE cur_tabelle_syb CURSOR FOR
		SELECT DISTINCT PBCATTBL.PBT_TNAM
			FROM PBCATTBL;
			
		OPEN cur_tabelle_syb;

		FETCH cur_tabelle_syb INTO :ls_tabella;

		do while SQLCA.sqlcode = 0
			ddlb_tabelle.additem(f_repl_underscore_with_space(ls_tabella))
			FETCH cur_tabelle_syb INTO :ls_tabella;	
		loop
		CLOSE cur_tabelle_syb;
						  
	case "ORACLE"
		DECLARE cur_tabelle_ora CURSOR FOR
		SELECT DISTINCT SYSTEM.PBCATTBL.PBT_TNAM
			FROM SYSTEM.PBCATTBL;

		OPEN cur_tabelle_ora;

		FETCH cur_tabelle_ora INTO :ls_tabella;

		do while SQLCA.sqlcode = 0
			ddlb_tabelle.additem(f_repl_underscore_with_space(ls_tabella))
			FETCH cur_tabelle_ora INTO :ls_tabella;	
		loop
		CLOSE cur_tabelle_ora;
						  			
case else
		g_mb.messagebox ("OMNIA", "DataBase Sconosciuto!")
end choose

if s_cs_xx.parametri.parametro_s_15 <> "" and not isnull(s_cs_xx.parametri.parametro_s_15) then
	wf_apri_esistente(s_cs_xx.parametri.parametro_s_15)
end if
end event

on w_genera_campo.create
int iCurrent
call super::create
this.cb_chiudi=create cb_chiudi
this.cb_elimina_riga=create cb_elimina_riga
this.ddlb_tabelle=create ddlb_tabelle
this.ddlb_colonne=create ddlb_colonne
this.ddlb_funzioni=create ddlb_funzioni
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.dw_imposta_report=create dw_imposta_report
this.st_5=create st_5
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_chiudi
this.Control[iCurrent+2]=this.cb_elimina_riga
this.Control[iCurrent+3]=this.ddlb_tabelle
this.Control[iCurrent+4]=this.ddlb_colonne
this.Control[iCurrent+5]=this.ddlb_funzioni
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.st_2
this.Control[iCurrent+8]=this.st_3
this.Control[iCurrent+9]=this.st_4
this.Control[iCurrent+10]=this.dw_imposta_report
this.Control[iCurrent+11]=this.st_5
end on

on w_genera_campo.destroy
call super::destroy
destroy(this.cb_chiudi)
destroy(this.cb_elimina_riga)
destroy(this.ddlb_tabelle)
destroy(this.ddlb_colonne)
destroy(this.ddlb_funzioni)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.dw_imposta_report)
destroy(this.st_5)
end on

type cb_chiudi from commandbutton within w_genera_campo
event clicked pbm_bnclicked
integer x = 2784
integer y = 1236
integer width = 361
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;double  ldoub_valore

string  ls_sql, ls_campo, ls_confronto, ls_operatore, ls_tabella, ls_colonna, &
        ls_funzione, ls_cod_campo, ls_apri, ls_chiudi
		  
integer li_i


ls_funzione = ddlb_funzioni.text

if ddlb_tabelle.text = "" or ddlb_colonne.text = "" then
	g_mb.messagebox("OMNIA","Selezionare Tabella e Colonna Desiderate",Exclamation!)
	return
else
	ls_tabella = f_repl_space_with_underscore(ddlb_tabelle.text)
	ls_colonna = f_repl_space_with_underscore(ddlb_colonne.text)
end if

if il_aperte > il_chiuse or il_chiuse > il_aperte then
	g_mb.messagebox("OMNIA","Sono presenti " + string(il_aperte) + " parentesi aperte e " + string (il_chiuse) + " chiuse",Exclamation!)
	return
end if	

ls_sql = "select "

if not (ls_funzione = "" or ls_funzione = " VALORE") then
	ls_sql = ls_sql + left(ls_funzione, len(ls_funzione) - 1) + ls_colonna + ") "
else
	ls_sql = ls_sql + ls_colonna + " "
end if

ls_sql = ls_sql + "from " + ls_tabella
if ib_cod_azienda then ls_sql = ls_sql + " where cod_azienda = '" + s_cs_xx.cod_azienda + "'"

dw_imposta_report.accepttext()
if not isnull(dw_imposta_report.getitemstring(1,"campo")) then
	if ib_cod_azienda then 
		ls_sql = ls_sql + " and "
	else
		ls_sql = ls_sql + " where "
	end if

	for li_i = 1 to dw_imposta_report.rowcount()
		ls_apri = dw_imposta_report.getitemstring(li_i,"apri_par")
		if isnull(ls_apri) then
			ls_apri = ""
		end if
		ls_campo = dw_imposta_report.getitemstring(li_i,"campo")
		ls_confronto = dw_imposta_report.getitemstring(li_i,"confronto")
		ls_operatore = dw_imposta_report.getitemstring(li_i,"operatore")
		ls_chiudi = dw_imposta_report.getitemstring(li_i,"chiudi_par")
		if isnull(ls_chiudi) then
			ls_chiudi = ""
		end if
		if li_i = dw_imposta_report.rowcount() then
			if isnull(ls_campo) then 
				g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
				return
			else
				ls_sql = ls_sql + ls_apri + f_repl_space_with_underscore(ls_campo) + " "
			end if

			if isnull(ls_operatore) then 
				g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
				return
			else
				if ls_operatore = "=" then ls_operatore = "LIKE"
				ls_sql = ls_sql + ls_operatore + " "
			end if

			if isnull(ls_confronto) then 
				g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
				return
			else
				ls_sql = ls_sql + wf_formatta(f_repl_space_with_underscore(ls_campo), &
				                             ls_confronto) + ls_chiudi + " "
			end if			
		else
			if isnull(ls_campo) then 
				g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
				return
			else
				ls_sql = ls_sql + ls_apri + f_repl_space_with_underscore(ls_campo) + " "
			end if

			if isnull(ls_operatore) then 
				g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
				return
			else				
				if ls_operatore = "=" then ls_operatore = "LIKE"
				ls_sql = ls_sql + ls_operatore + " "
			end if

			if isnull(ls_confronto) then 
				g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
				return
			else
				ls_sql = ls_sql + wf_formatta(f_repl_space_with_underscore(ls_campo), &
				                             ls_confronto) + ls_chiudi + " "
			end if			
		
			if isnull(dw_imposta_report.getitemstring(li_i,"unione") + " ") &
			   and li_i < dw_imposta_report.rowcount() then 
				g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)
				return
			else
				ls_sql = ls_sql + dw_imposta_report.getitemstring(li_i,"unione") + " "
			end if			

		end if
	next
end if
ls_sql = ls_sql + ";"
declare cu_campo dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_campo;
fetch cu_campo into :ldoub_valore;
if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then
	g_mb.messagebox("Apice", "Errore nell'Estrazione dei Dati",Exclamation!)
	return
end if
close cu_campo;

s_cs_xx.parametri.parametro_s_15 = ls_sql
s_cs_xx.parametri.parametro_d_1 = ldoub_valore

close(parent)
end event

type cb_elimina_riga from commandbutton within w_genera_campo
event clicked pbm_bnclicked
integer x = 23
integer y = 1232
integer width = 361
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina Riga"
end type

event clicked;if not isnull(dw_imposta_report.getitemstring(dw_imposta_report.rowcount(),"apri_par")) then
	il_aperte --
end if

if not isnull(dw_imposta_report.getitemstring(dw_imposta_report.rowcount(),"chiudi_par")) then
	il_chiuse --
end if

dw_imposta_report.deleterow(0)
if dw_imposta_report.rowcount() = 0 then
	dw_imposta_report.insertrow(0)
end if
end event

type ddlb_tabelle from dropdownlistbox within w_genera_campo
event selectionchanged pbm_cbnselchange
integer x = 18
integer y = 100
integer width = 1321
integer height = 1040
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;wf_agg_colonne( this.text(index))
dw_imposta_report.insertrow(0)
end event

type ddlb_colonne from dropdownlistbox within w_genera_campo
integer x = 1824
integer y = 100
integer width = 1321
integer height = 1040
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

type ddlb_funzioni from dropdownlistbox within w_genera_campo
integer x = 1367
integer y = 100
integer width = 430
integer height = 500
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
string text = " VALORE"
boolean vscrollbar = true
string item[] = {" VALORE","MAX()","MIN()","SUM()","AVG()","COUNT()"}
end type

type st_1 from statictext within w_genera_campo
integer x = 18
integer y = 196
integer width = 494
integer height = 52
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Criteri di Selezione"
boolean focusrectangle = false
end type

type st_2 from statictext within w_genera_campo
integer x = 18
integer y = 20
integer width = 494
integer height = 52
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Tabella"
boolean focusrectangle = false
end type

type st_3 from statictext within w_genera_campo
integer x = 1367
integer y = 20
integer width = 265
integer height = 52
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Funzione"
boolean focusrectangle = false
end type

type st_4 from statictext within w_genera_campo
integer x = 1824
integer y = 20
integer width = 265
integer height = 52
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Colonna"
boolean focusrectangle = false
end type

type dw_imposta_report from datawindow within w_genera_campo
event itemchanged pbm_dwnitemchange
integer x = 23
integer y = 272
integer width = 3127
integer height = 940
integer taborder = 40
string dataobject = "d_imposta_report"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;choose case dwo.name
	
	case "unione"
		if rowcount() = row then insertrow(0)
	
	case "apri_par"
		if isnull(data) then
			il_aperte --
		else
			il_aperte ++
		end if	
		
	case "chiudi_par"
		if isnull(data) then
			il_chiuse --
		else
			il_chiuse ++
		end if
		

end choose
end event

type st_5 from statictext within w_genera_campo
integer x = 453
integer y = 1256
integer width = 2117
integer height = 72
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Le date vanno inserite nel seguente formato:  yyyy/mm/dd"
boolean focusrectangle = false
end type

