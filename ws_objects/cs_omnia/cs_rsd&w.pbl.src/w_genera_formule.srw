﻿$PBExportHeader$w_genera_formule.srw
$PBExportComments$Finestra di Generazione Formule
forward
global type w_genera_formule from w_cs_xx_risposta
end type
type mle_1 from multilineedit within w_genera_formule
end type
type st_3 from statictext within w_genera_formule
end type
type st_2 from statictext within w_genera_formule
end type
type cb_calcola from commandbutton within w_genera_formule
end type
type dw_elenco_campi_formule from uo_cs_xx_dw within w_genera_formule
end type
type st_risultato from statictext within w_genera_formule
end type
type st_4 from statictext within w_genera_formule
end type
type cb_annulla from commandbutton within w_genera_formule
end type
end forward

global type w_genera_formule from w_cs_xx_risposta
int Width=2657
int Height=1177
boolean TitleBar=true
string Title="Generazione Formula"
mle_1 mle_1
st_3 st_3
st_2 st_2
cb_calcola cb_calcola
dw_elenco_campi_formule dw_elenco_campi_formule
st_risultato st_risultato
st_4 st_4
cb_annulla cb_annulla
end type
global w_genera_formule w_genera_formule

type variables
boolean ib_annulla = false
end variables

forward prototypes
public function string wf_formula_descr (string fs_formula)
end prototypes

public function string wf_formula_descr (string fs_formula);string  ls_campo,ls_risultato, ls_des_campo
integer li_numero_campi,li_i, li_pos_campo

setpointer(hourglass!)

li_numero_campi = dw_elenco_campi_formule.rowcount()
for li_i = 1 to li_numero_campi
	ls_campo = dw_elenco_campi_formule.getitemstring(li_i, "cod_campo")
	li_pos_campo = pos(fs_formula,ls_campo)
	do while li_pos_campo <> 0
		ls_des_campo = dw_elenco_campi_formule.getitemstring(li_i, "des_campo")
		fs_formula = replace(fs_formula, li_pos_campo, len(ls_campo), ls_des_campo)
		li_pos_campo = pos(fs_formula,ls_campo)
	loop
next

setpointer(arrow!)
return fs_formula
end function

event pc_setwindow;call super::pc_setwindow;dw_elenco_campi_formule.set_dw_key("cod_azienda")
dw_elenco_campi_formule.set_dw_key("cod_campo")
dw_elenco_campi_formule.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)

mle_1.text = s_cs_xx.parametri.parametro_s_15
end event

on w_genera_formule.create
int iCurrent
call w_cs_xx_risposta::create
this.mle_1=create mle_1
this.st_3=create st_3
this.st_2=create st_2
this.cb_calcola=create cb_calcola
this.dw_elenco_campi_formule=create dw_elenco_campi_formule
this.st_risultato=create st_risultato
this.st_4=create st_4
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=mle_1
this.Control[iCurrent+2]=st_3
this.Control[iCurrent+3]=st_2
this.Control[iCurrent+4]=cb_calcola
this.Control[iCurrent+5]=dw_elenco_campi_formule
this.Control[iCurrent+6]=st_risultato
this.Control[iCurrent+7]=st_4
this.Control[iCurrent+8]=cb_annulla
end on

on w_genera_formule.destroy
call w_cs_xx_risposta::destroy
destroy(this.mle_1)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.cb_calcola)
destroy(this.dw_elenco_campi_formule)
destroy(this.st_risultato)
destroy(this.st_4)
destroy(this.cb_annulla)
end on

event close;call super::close;if ib_annulla then
	s_cs_xx.parametri.parametro_d_1 = 0
	s_cs_xx.parametri.parametro_s_14 = '%'
	s_cs_xx.parametri.parametro_s_13 = '%'
else
	s_cs_xx.parametri.parametro_d_1 = f_calcola_formula(mle_1.text)
	s_cs_xx.parametri.parametro_s_14 = mle_1.text
	s_cs_xx.parametri.parametro_s_13 = wf_formula_descr(mle_1.text)
end if

end event

type mle_1 from multilineedit within w_genera_formule
int X=23
int Y=601
int Width=2561
int Height=441
int TabOrder=40
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_genera_formule
int X=23
int Y=541
int Width=549
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Operatori ammessi:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_genera_formule
int X=572
int Y=541
int Width=723
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text=" +, -, *, /, (, )"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Courier"
FontFamily FontFamily=Modern!
FontPitch FontPitch=Fixed!
end type

type cb_calcola from commandbutton within w_genera_formule
event clicked pbm_bnclicked
int X=2218
int Y=121
int Width=366
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="&Calcola"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;st_risultato.text = string(f_calcola_formula(mle_1.text))
end event

type dw_elenco_campi_formule from uo_cs_xx_dw within w_genera_formule
int X=23
int Y=21
int Width=2172
int Height=501
int TabOrder=20
string DataObject="d_elenco_campi_formule"
BorderStyle BorderStyle=StyleLowered!
end type

event doubleclicked;call super::doubleclicked;string ls_cod_campo_formula
integer li_position

ls_cod_campo_formula = this.getitemstring(row, "cod_campo")
li_position = mle_1.selectedstart()
mle_1.setfocus()
if isnull(li_position) then 
	mle_1.text = ls_cod_campo_formula + " "
else
	mle_1.replacetext(" " + ls_cod_campo_formula + " ")
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type st_risultato from statictext within w_genera_formule
int X=2218
int Y=261
int Width=366
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
string Text="0"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Courier"
FontFamily FontFamily=Modern!
FontPitch FontPitch=Fixed!
end type

type st_4 from statictext within w_genera_formule
int X=2218
int Y=201
int Width=366
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Risultato"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-10
int Weight=700
string FaceName="Times New Roman"
FontFamily FontFamily=Roman!
FontPitch FontPitch=Variable!
end type

type cb_annulla from commandbutton within w_genera_formule
event clicked pbm_bnclicked
int X=2218
int Y=21
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;ib_annulla = true
close(parent)
end event

