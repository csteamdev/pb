﻿$PBExportHeader$w_indicatori.srw
$PBExportComments$Finestra Gestione Tabella Indicatori
forward
global type w_indicatori from w_cs_xx_principale
end type
type dw_indicatori_lista from uo_cs_xx_dw within w_indicatori
end type
type dw_indicatori_det from uo_cs_xx_dw within w_indicatori
end type
end forward

global type w_indicatori from w_cs_xx_principale
int Width=2012
boolean TitleBar=true
string Title="Tabella Indicatori"
dw_indicatori_lista dw_indicatori_lista
dw_indicatori_det dw_indicatori_det
end type
global w_indicatori w_indicatori

forward prototypes
public subroutine wf_tipo_indicatore (string as_tipo_indicatore)
end prototypes

public subroutine wf_tipo_indicatore (string as_tipo_indicatore);if as_tipo_indicatore = "C" then
	dw_indicatori_det.Object.cod_formula.protect = 0
else
	dw_indicatori_det.Object.cod_formula.protect = 1
end if

end subroutine

event pc_setwindow;call super::pc_setwindow;dw_indicatori_lista.set_dw_key("cod_azienda")
dw_indicatori_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_indicatori_det.set_dw_options(sqlca,dw_indicatori_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_indicatori_lista
end event

on w_indicatori.create
int iCurrent
call w_cs_xx_principale::create
this.dw_indicatori_lista=create dw_indicatori_lista
this.dw_indicatori_det=create dw_indicatori_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_indicatori_lista
this.Control[iCurrent+2]=dw_indicatori_det
end on

on w_indicatori.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_indicatori_lista)
destroy(this.dw_indicatori_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_indicatori_det,"cod_formula",sqlca,&
                 "tes_formule","cod_formula","des_formula",&
                 "tes_formule.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type dw_indicatori_lista from uo_cs_xx_dw within w_indicatori
int X=23
int Y=21
int Width=1921
int Height=501
int TabOrder=10
string DataObject="d_indicatori_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_pickedrow;call super::pcd_pickedrow;integer li_row
li_row = this.getrow()
if li_row > 0 then
	wf_tipo_indicatore (this.getitemstring(li_row, "flag_tipo_indicatore"))
end if
end event

type dw_indicatori_det from uo_cs_xx_dw within w_indicatori
int X=23
int Y=541
int Width=1921
int Height=781
int TabOrder=20
string DataObject="d_indicatori_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;choose case i_colname
	case "flag_tipo_indicatore"
		wf_tipo_indicatore (i_coltext)
end choose
end event

