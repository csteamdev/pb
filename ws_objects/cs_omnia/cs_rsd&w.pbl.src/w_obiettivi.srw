﻿$PBExportHeader$w_obiettivi.srw
$PBExportComments$Finestra Gestione Tabella Obiettivi
forward
global type w_obiettivi from w_cs_xx_principale
end type
type dw_obiettivi_lista from uo_cs_xx_dw within w_obiettivi
end type
type dw_obiettivi_det from uo_cs_xx_dw within w_obiettivi
end type
end forward

global type w_obiettivi from w_cs_xx_principale
int Width=1623
int Height=1277
boolean TitleBar=true
string Title="Tabella Obiettivi"
dw_obiettivi_lista dw_obiettivi_lista
dw_obiettivi_det dw_obiettivi_det
end type
global w_obiettivi w_obiettivi

on w_obiettivi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_obiettivi_lista=create dw_obiettivi_lista
this.dw_obiettivi_det=create dw_obiettivi_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_obiettivi_lista
this.Control[iCurrent+2]=dw_obiettivi_det
end on

on w_obiettivi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_obiettivi_lista)
destroy(this.dw_obiettivi_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_obiettivi_lista.set_dw_key("cod_azienda")
dw_obiettivi_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_obiettivi_det.set_dw_options(sqlca,dw_obiettivi_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_obiettivi_lista
end event

type dw_obiettivi_lista from uo_cs_xx_dw within w_obiettivi
int X=23
int Y=21
int Width=1532
int Height=501
int TabOrder=1
string DataObject="d_obiettivi_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

type dw_obiettivi_det from uo_cs_xx_dw within w_obiettivi
int X=23
int Y=541
int Width=1532
int Height=601
boolean BringToTop=true
string DataObject="d_obiettivi_det"
BorderStyle BorderStyle=StyleRaised!
end type

