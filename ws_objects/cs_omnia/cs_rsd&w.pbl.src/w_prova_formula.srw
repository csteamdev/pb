﻿$PBExportHeader$w_prova_formula.srw
forward
global type w_prova_formula from w_cs_xx_risposta
end type
type dw_columns from uo_cs_xx_dw within w_prova_formula
end type
type dw_tables from uo_cs_xx_dw within w_prova_formula
end type
type st_17 from statictext within w_prova_formula
end type
type st_3 from statictext within w_prova_formula
end type
type st_1 from statictext within w_prova_formula
end type
type st_9 from statictext within w_prova_formula
end type
type st_8 from statictext within w_prova_formula
end type
type st_7 from statictext within w_prova_formula
end type
type st_6 from statictext within w_prova_formula
end type
type st_5 from statictext within w_prova_formula
end type
type st_4 from statictext within w_prova_formula
end type
type st_2 from statictext within w_prova_formula
end type
type st_10 from statictext within w_prova_formula
end type
type mle_comments from multilineedit within w_prova_formula
end type
type gb_1 from groupbox within w_prova_formula
end type
type mle_1 from multilineedit within w_prova_formula
end type
type st_69 from statictext within w_prova_formula
end type
type st_70 from statictext within w_prova_formula
end type
type cb_calcola from commandbutton within w_prova_formula
end type
end forward

global type w_prova_formula from w_cs_xx_risposta
int Width=3050
int Height=1877
dw_columns dw_columns
dw_tables dw_tables
st_17 st_17
st_3 st_3
st_1 st_1
st_9 st_9
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
st_4 st_4
st_2 st_2
st_10 st_10
mle_comments mle_comments
gb_1 gb_1
mle_1 mle_1
st_69 st_69
st_70 st_70
cb_calcola cb_calcola
end type
global w_prova_formula w_prova_formula

type variables
string is_tablename
end variables

on w_prova_formula.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_columns=create dw_columns
this.dw_tables=create dw_tables
this.st_17=create st_17
this.st_3=create st_3
this.st_1=create st_1
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_2=create st_2
this.st_10=create st_10
this.mle_comments=create mle_comments
this.gb_1=create gb_1
this.mle_1=create mle_1
this.st_69=create st_69
this.st_70=create st_70
this.cb_calcola=create cb_calcola
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_columns
this.Control[iCurrent+2]=dw_tables
this.Control[iCurrent+3]=st_17
this.Control[iCurrent+4]=st_3
this.Control[iCurrent+5]=st_1
this.Control[iCurrent+6]=st_9
this.Control[iCurrent+7]=st_8
this.Control[iCurrent+8]=st_7
this.Control[iCurrent+9]=st_6
this.Control[iCurrent+10]=st_5
this.Control[iCurrent+11]=st_4
this.Control[iCurrent+12]=st_2
this.Control[iCurrent+13]=st_10
this.Control[iCurrent+14]=mle_comments
this.Control[iCurrent+15]=gb_1
this.Control[iCurrent+16]=mle_1
this.Control[iCurrent+17]=st_69
this.Control[iCurrent+18]=st_70
this.Control[iCurrent+19]=cb_calcola
end on

on w_prova_formula.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_columns)
destroy(this.dw_tables)
destroy(this.st_17)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_2)
destroy(this.st_10)
destroy(this.mle_comments)
destroy(this.gb_1)
destroy(this.mle_1)
destroy(this.st_69)
destroy(this.st_70)
destroy(this.cb_calcola)
end on

event pc_setwindow;call super::pc_setwindow;string ls_sql
// Open script for w_quick_select

dw_tables.SetTransObject (sqlca)
dw_columns.SetTransObject (sqlca)

// Controllo il DBMS a cui sono attaccato

choose case Upper(Left(sqlca.dbms,3) )

	case "ODB"		// ODBC
		ls_sql = 'SELECT DISTINCT "DBA"."PBCATTBL"."PBT_TNAM", "DBA"."PBCATTBL"."PBT_CMNT" ' &  
			           + ' FROM "DBA"."PBCATTBL" ' &  
				        + ' order by 1 ASC'   ;
						  
	case "ORA", "OR6", "OR7"	// ORACLE
		ls_sql = "SELECT DISTINCT SYSTEM.PBCATTBL.PBT_TNAM, SYSTEM.PBCATTBL.PBT_CMNT " &  
			   		  + " FROM SYSTEM.PBCATTBL " &  
				        + " order by 1 ASC"   ;
	case "O72"	// ORACLE 7.2
//		ls_sql = "SELECT DISTINCT SYSTEM.TAB.TNAME " &  
//			   		  + " FROM SYSTEM.TAB " &  
//				        + " order by 1 ASC"   ;
		ls_sql = "SELECT DISTINCT SYS.CATALOG.TNAME " &  
			   		  + " FROM SYS.CATALOG " &  
						  + " WHERE SYS.CATALOG.CREATOR = 'CS_XX' AND " &
						  + " SYS.CATALOG.TABLETYPE = 'TABLE' " &
				        + " order by 1 ASC"   ;
				
case else
		g_mb.messagebox ("OMNIA", "DataBase Sconosciuto!")
end choose

dw_tables.SetSQLSelect(ls_sql)
dw_tables.Retrieve ()
end event

type dw_columns from uo_cs_xx_dw within w_prova_formula
int X=1075
int Y=361
int Width=983
int Height=537
int TabOrder=20
boolean BringToTop=true
string DataObject="d_column_list"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event rbuttondown;call super::rbuttondown;/***********************************************************************
		Get the row that has been 'clicked' and take the value out of 
		pbc_cmnt column for comments.  If there is no value in this column
		then display the default message - 'TThis column has no comments"

************************************************************************/

	integer lCurrentRow
	string 	lObjectAtPointer, lStrRow, lComments

	lObjectAtPointer = this.GetObjectAtPointer()

	If lObjectAtPointer = "" then return   // all done white space clicked

	lStrRow = Mid(lObjectATPointer, Pos(lObjectAtPointer, "~t") + 1, 2)
//**********************************************************************
//		Comments for the Column DataWindow are in Column 3 pbcatcol pbc_cmnt
//**********************************************************************
	lComments = this.Object.pbc_cmnt[integer(lStrRow)]

	this.setrowfocusindicator(FocusRect!)
	this.setrow(integer(lStrRow))
	
	If Trim(lComments) = "" or IsNull(lComments)  then
		mle_comments.text = "This column has no comments."
	else
		mle_comments.text = lComments
	end if

	this.setredraw(TRUE)
end event

event clicked;call super::clicked;mle_1.text = mle_1.text + " " + This.Object.cname[row] + " "
mle_1.setfocus()
end event

type dw_tables from uo_cs_xx_dw within w_prova_formula
int X=69
int Y=361
int Width=983
int Height=537
int TabOrder=10
string DataObject="d_table_list"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event clicked;call super::clicked;String		ls_tname , ls_sql

SetPointer ( HourGlass! )
	
// Highlight this row
selectRow (0, False )			
selectRow (row, True )

// Get table name
ls_tname = This.Object.tname[row]		
is_TableName = ls_tname
mle_comments.Text = '~tSto estraendo le colonne relative alla Tabella Selezionata ' + Lower ( ls_tname ) + '...'

dw_columns.SetRedraw ( False )

// Verifico il DBMS a cui sono connesso

choose case Upper(Left(sqlca.dbms,3) )

	case "ODB"		// ODBC
		ls_sql = "Select PBC_CNAM, PBC_CMNT " &
				+ "from PBCATCOL " &
				+ "where PBC_TNAM = '" + Upper(is_TableName) &
				+ "' order by 1 ASC"

	case "ORA", "OR6", "OR7"	// ORACLE
		ls_sql = "Select PBC_CNAM, PBC_CMNT " &
				+ "from system.PBCATCOL " &
				+ "where PBC_TNAM = '" + Upper(is_TableName) &
				+ "' order by 1 ASC"
				
					case "O72"	// ORACLE 7.2
		ls_sql = "Select CNAME " &
				+ "from sys.COL " &
				+ "where TNAME = '" + Upper(is_TableName) &
				+ "' order by 1 ASC"

end choose

dw_columns.SetSQLSelect(ls_sql)

dw_columns.Retrieve()

mle_comments.Text = ''
dw_columns.SetRedraw ( True )

end event

event rbuttondown;call super::rbuttondown;/***********************************************************************
		Get the row that has been 'clicked' and take the value out of 
		pbt_cmnt column for comments.  If there is no value in this column
		then display the default message - 'There are no comments for this
		table."
************************************************************************/
	setpointer(hourglass!)

	string 	ls_objectatpointer, ls_strrow, ls_comments

	ls_objectatpointer = this.GetObjectAtPointer() 

	If ls_objectatpointer = "" then return   // all done white space clicked

	ls_strrow = Mid(ls_objectatpointer, Pos(ls_objectatpointer, "~t") + 1, 2)
//**********************************************************************
//		Comments for the Table DataWindow are in column two pbt_cmnt
//**********************************************************************
	ls_comments = this.Object.pbt_cmnt[ integer(ls_strrow) ]

	this.setrowfocusindicator(FocusRect!)
	this.setrow(integer(ls_strrow))
	
	If Trim(ls_comments) = "" or IsNull(ls_comments)  then
		mle_comments.text = "This table has no comments."
	else
		mle_comments.text = ls_comments
	end if

	this.setredraw(TRUE)
end event

type st_17 from statictext within w_prova_formula
int X=1075
int Y=221
int Width=846
int Height=69
boolean Enabled=false
boolean BringToTop=true
string Text="trascinalo nel box ~"Commenti~"."
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_prova_formula
int X=69
int Y=161
int Width=915
int Height=69
boolean Enabled=false
boolean BringToTop=true
string Text="3.  (Opzionale) inserisci i criteri di selezione"
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_prova_formula
int X=69
int Y=41
int Width=755
int Height=69
boolean Enabled=false
boolean BringToTop=true
string Text="1.  Seleziona una tabella."
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_9 from statictext within w_prova_formula
int X=1075
int Y=281
int Width=275
int Height=69
boolean Enabled=false
boolean BringToTop=true
string Text="&Colonne:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_8 from statictext within w_prova_formula
int X=69
int Y=281
int Width=238
int Height=69
boolean Enabled=false
boolean BringToTop=true
string Text="&Tabelle:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_7 from statictext within w_prova_formula
int X=1075
int Y=161
int Width=846
int Height=69
boolean Enabled=false
boolean BringToTop=true
string Text="del mouse ed eventualmente"
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_6 from statictext within w_prova_formula
int X=1075
int Y=101
int Width=846
int Height=69
boolean Enabled=false
boolean BringToTop=true
string Text="tabelle o colonne, premi il tasto destro"
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_prova_formula
int X=1075
int Y=41
int Width=846
int Height=69
boolean Enabled=false
boolean BringToTop=true
string Text="Per visualizzare i commenti di"
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_prova_formula
int X=69
int Y=221
int Width=897
int Height=69
boolean Enabled=false
boolean BringToTop=true
string Text="nel box ~"Colonne Selezionate~""
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_prova_formula
int X=69
int Y=101
int Width=727
int Height=69
boolean Enabled=false
boolean BringToTop=true
string Text="2.  Seleziona una o più colonne."
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_10 from statictext within w_prova_formula
int X=69
int Y=921
int Width=289
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Commenti:"
boolean FocusRectangle=false
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type mle_comments from multilineedit within w_prova_formula
int X=69
int Y=981
int Width=1994
int Height=117
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean AutoVScroll=true
boolean DisplayOnly=true
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within w_prova_formula
int X=23
int Width=2081
int Height=1141
long TextColor=41943040
long BackColor=74481808
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type mle_1 from multilineedit within w_prova_formula
int X=23
int Y=1221
int Width=2081
int Height=441
int TabOrder=30
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_69 from statictext within w_prova_formula
int X=572
int Y=1161
int Width=723
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text=" +, -, *, /, (, )"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Courier"
FontFamily FontFamily=Modern!
FontPitch FontPitch=Fixed!
end type

type st_70 from statictext within w_prova_formula
int X=23
int Y=1161
int Width=549
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Operatori ammessi:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_calcola from commandbutton within w_prova_formula
event clicked pbm_bnclicked
int X=23
int Y=1681
int Width=366
int Height=81
boolean BringToTop=true
string Text="&Calcola"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;//string  ls_formula,ls_campo,ls_sql,ls_new_syntax,ls_errore,ls_errore_creazione,ls_risultato
//integer li_risposta,li_numero_campi,li_i
//double  ldd_valore_campo,ldd_risultato
//long    ll_num_righe,ll_t,ll_prog_formula,ll_prog_risultato
//datastore lds_righe_ingresso
//
//setpointer(hourglass!)
//
//ll_prog_risultato=1
//
//ll_num_righe=s_cs_xx.parametri.parametro_dw_1.rowcount()
//li_numero_campi = upperbound(is_campi[])
//st_nr_corr_tot.text = "0/"+string(ll_num_righe)
//for ll_t = 1 to ll_num_righe
//	ls_formula = mle_1.text
//	for li_i = 1 to li_numero_campi
//		ls_campo = is_campi[li_i]	
//		if pos(ls_formula,ls_campo) <> 0 then
//			ldd_valore_campo  = s_cs_xx.parametri.parametro_dw_1.getitemnumber(ll_t,ls_campo)
//			if isnull(ldd_valore_campo) then ldd_valore_campo=0
//			li_risposta = f_sostituzione(ls_formula, ls_campo, string(ldd_valore_campo))
//		end if
//	next
//	
//	ls_risultato = f_parser_parentesi(ls_formula)
//	ldd_risultato = double(ls_risultato)
//	
//   INSERT INTO tab_risultati  
//          	 (cod_azienda,   
//	           nome_tabella,   
//   	        prog_formula,   
//      	     prog_risultato,   
//         	  risultato )  
//  	 VALUES ( :s_cs_xx.cod_azienda,   
//   	       :is_tabella,   
//      	    :ll_prog_formula,   
//         	 :ll_prog_risultato,   
//           	 :ldd_risultato )  ;
//
//	ll_prog_risultato++
//	st_nr_corr_tot.text = string(ll_t) + "/" + string(ll_num_righe)
//next
//
//setpointer(arrow!)
//commit;
//dw_lista_risultati.triggerevent("pcd_retrieve")
end event

