﻿$PBExportHeader$w_rep_rev_sistema.srw
$PBExportComments$Finestra Gestione Report Revisione Sistema Qualità
forward
global type w_rep_rev_sistema from w_cs_xx_principale
end type
type dw_sel_rep_rev_sistema from uo_cs_xx_dw within w_rep_rev_sistema
end type
type cb_selezione from commandbutton within w_rep_rev_sistema
end type
type cb_report from commandbutton within w_rep_rev_sistema
end type
type cb_annulla from commandbutton within w_rep_rev_sistema
end type
type dw_rep_rev_sistema from uo_cs_xx_dw within w_rep_rev_sistema
end type
type dw_rep_rev_sistema_conf from uo_cs_xx_dw within w_rep_rev_sistema
end type
end forward

global type w_rep_rev_sistema from w_cs_xx_principale
integer x = 59
integer y = 348
integer width = 3566
integer height = 1656
string title = "Report Revisione Sistema Qualità"
dw_sel_rep_rev_sistema dw_sel_rep_rev_sistema
cb_selezione cb_selezione
cb_report cb_report
cb_annulla cb_annulla
dw_rep_rev_sistema dw_rep_rev_sistema
dw_rep_rev_sistema_conf dw_rep_rev_sistema_conf
end type
global w_rep_rev_sistema w_rep_rev_sistema

type variables
string is_flag_tipo_report
end variables

forward prototypes
public subroutine wf_dimensioni ()
end prototypes

public subroutine wf_dimensioni ();x = 1150
y = 690
width = 1134

if is_flag_tipo_report = "N" then // Normale
	height = 705
	dw_sel_rep_rev_sistema.height = 461
	cb_annulla.y = 501
	cb_report.y = 501
else
	height = 965
	dw_sel_rep_rev_sistema.height = 721
	cb_annulla.y = 761
	cb_report.y = 761
end if
end subroutine

event pc_setwindow;call super::pc_setwindow;dw_rep_rev_sistema.ib_dw_report = true
dw_rep_rev_sistema_conf.ib_dw_report = true
dw_sel_rep_rev_sistema.ib_dw_report = true


set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_rep_rev_sistema.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_nomodify + &
                                c_nodelete + &
                                c_newonopen + &
                                c_disableCC, &
                                c_noresizedw + &
                                c_nohighlightselected + &
                                c_nocursorrowpointer +&
                                c_nocursorrowfocusrect )

dw_rep_rev_sistema_conf.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_nomodify + &
                                c_nodelete + &
                                c_newonopen + &
                                c_disableCC, &
                                c_noresizedw + &
                                c_nohighlightselected + &
                                c_nocursorrowpointer +&
                                c_nocursorrowfocusrect )

dw_sel_rep_rev_sistema.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_nomodify + &
                                c_nodelete + &
                                c_newonopen + &
                                c_disableCC, &
                                c_noresizedw + &
                                c_nohighlightselected + &
                                c_cursorrowpointer)

iuo_dw_main = dw_sel_rep_rev_sistema

is_flag_tipo_report = "C"
wf_dimensioni()
end event

on w_rep_rev_sistema.create
int iCurrent
call super::create
this.dw_sel_rep_rev_sistema=create dw_sel_rep_rev_sistema
this.cb_selezione=create cb_selezione
this.cb_report=create cb_report
this.cb_annulla=create cb_annulla
this.dw_rep_rev_sistema=create dw_rep_rev_sistema
this.dw_rep_rev_sistema_conf=create dw_rep_rev_sistema_conf
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sel_rep_rev_sistema
this.Control[iCurrent+2]=this.cb_selezione
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.cb_annulla
this.Control[iCurrent+5]=this.dw_rep_rev_sistema
this.Control[iCurrent+6]=this.dw_rep_rev_sistema_conf
end on

on w_rep_rev_sistema.destroy
call super::destroy
destroy(this.dw_sel_rep_rev_sistema)
destroy(this.cb_selezione)
destroy(this.cb_report)
destroy(this.cb_annulla)
destroy(this.dw_rep_rev_sistema)
destroy(this.dw_rep_rev_sistema_conf)
end on

type dw_sel_rep_rev_sistema from uo_cs_xx_dw within w_rep_rev_sistema
integer x = 23
integer y = 20
integer width = 1051
integer height = 720
integer taborder = 40
string dataobject = "d_sel_rep_rev_sistema"
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;if i_colname = "rs_flag_tipo_report" then
	is_flag_tipo_report = i_coltext
	wf_dimensioni ()
end if
end event

type cb_selezione from commandbutton within w_rep_rev_sistema
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
boolean default = true
end type

event clicked;dw_sel_rep_rev_sistema.show()

wf_dimensioni()

dw_rep_rev_sistema.hide()
dw_rep_rev_sistema_conf.hide()
cb_selezione.hide()

dw_sel_rep_rev_sistema.change_dw_current()
end event

type cb_report from commandbutton within w_rep_rev_sistema
event clicked pbm_bnclicked
integer x = 709
integer y = 760
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
boolean default = true
end type

event clicked;integer li_anno_att, li_anno_prec
datetime ldt_data_comp_att, ldt_data_comp_prec
char lc_flag_consuntivo_att, lc_flag_consuntivo_prec, lc_flag_tipo_report

dw_sel_rep_rev_sistema.AcceptText()
li_anno_att = dw_sel_rep_rev_sistema.getitemnumber(1,"rn_anno_att")
ldt_data_comp_att = dw_sel_rep_rev_sistema.getitemdatetime(1,"rd_data_comp_att")
lc_flag_consuntivo_att = dw_sel_rep_rev_sistema.getitemstring(1,"rs_flag_consuntivo_att")
	
if isnull(lc_flag_consuntivo_att) then
	g_mb.messagebox("Selezione Report Revisione Sistema", "Dati della Selezione Non Validi (Flag Consuntivo Revisione)",StopSign!)
	return
end if

if lc_flag_consuntivo_att = 'S' and isnull(li_anno_att) then
	g_mb.messagebox("Selezione Report Revisione Sistema", "Dati della Selezione Non Validi (Anno Revisione)",StopSign!)
	return
else
	if lc_flag_consuntivo_att = 'S' and isnull(ldt_data_comp_att) then ldt_data_comp_att = datetime(today())
end if

if lc_flag_consuntivo_att = 'N' and isnull(ldt_data_comp_att) then
	g_mb.messagebox("Selezione Report Revisione Sistema", "Dati della Selezione Non Validi (Data Revisione)",StopSign!)
	return
else
	if lc_flag_consuntivo_att = 'N' and isnull(li_anno_att) then li_anno_att = year(today())
end if

if is_flag_tipo_report = "C" then // Confronto

	li_anno_prec = dw_sel_rep_rev_sistema.getitemnumber(1,"rn_anno_prec")
	ldt_data_comp_prec = dw_sel_rep_rev_sistema.getitemdatetime(1,"rd_data_comp_prec")
	lc_flag_consuntivo_prec = dw_sel_rep_rev_sistema.getitemstring(1,"rs_flag_consuntivo_prec")
	
	if isnull(lc_flag_consuntivo_prec) then
		g_mb.messagebox("Selezione Report Revisione Sistema", "Dati della Selezione Non Validi (Flag Consuntivo Confronto)",StopSign!)
		return
	end if
	
	if lc_flag_consuntivo_prec = 'S' and isnull(li_anno_prec) then
		g_mb.messagebox("Selezione Report Revisione Sistema", "Dati della Selezione Non Validi (Anno Confronto)",StopSign!)
		return
	else
		if lc_flag_consuntivo_att = 'S' and isnull(ldt_data_comp_prec) then ldt_data_comp_prec = datetime(today())
	end if
	
	if lc_flag_consuntivo_prec = 'N' and isnull(ldt_data_comp_prec) then
		g_mb.messagebox("Selezione Report Revisione Sistema", "Dati della Selezione Non Validi (Data Confronto)",StopSign!)
		return
	else
		if lc_flag_consuntivo_att = 'N' and isnull(li_anno_prec) then li_anno_prec = year(today())
	end if

	parent.x = 60
	parent.y = 350
	parent.width = 3557
	parent.height = 1669
	dw_rep_rev_sistema_conf.retrieve( s_cs_xx.cod_azienda, li_anno_att, li_anno_prec, date(ldt_data_comp_att), &
                             date(ldt_data_comp_prec), lc_flag_consuntivo_att, lc_flag_consuntivo_prec)
	dw_rep_rev_sistema_conf.show()
	dw_rep_rev_sistema_conf.change_dw_current()

else
	parent.x = 60
	parent.y = 350
	parent.width = 3557
	parent.height = 1669
	dw_rep_rev_sistema.retrieve( s_cs_xx.cod_azienda, li_anno_att, date(ldt_data_comp_att), &
                                lc_flag_consuntivo_att)
	dw_rep_rev_sistema.show()
	dw_rep_rev_sistema.change_dw_current()

end if

dw_sel_rep_rev_sistema.hide()
cb_selezione.show()
end event

type cb_annulla from commandbutton within w_rep_rev_sistema
event clicked pbm_bnclicked
integer x = 320
integer y = 760
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
boolean cancel = true
end type

on clicked;close(parent)
end on

type dw_rep_rev_sistema from uo_cs_xx_dw within w_rep_rev_sistema
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 50
string dataobject = "d_rep_rev_sistema"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_rep_rev_sistema_conf from uo_cs_xx_dw within w_rep_rev_sistema
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
string dataobject = "d_rep_rev_sistema_conf"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

