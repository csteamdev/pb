﻿$PBExportHeader$w_tab_campi_formule.srw
$PBExportComments$Finestra Tabella Formule
forward
global type w_tab_campi_formule from w_cs_xx_principale
end type
type dw_campi_formule_lista from uo_cs_xx_dw within w_tab_campi_formule
end type
type dw_campi_formule_det from uo_cs_xx_dw within w_tab_campi_formule
end type
type cb_estrazione from commandbutton within w_tab_campi_formule
end type
end forward

global type w_tab_campi_formule from w_cs_xx_principale
integer width = 2551
integer height = 1512
string title = "Tabella Campi per Formule"
dw_campi_formule_lista dw_campi_formule_lista
dw_campi_formule_det dw_campi_formule_det
cb_estrazione cb_estrazione
end type
global w_tab_campi_formule w_tab_campi_formule

on w_tab_campi_formule.create
int iCurrent
call super::create
this.dw_campi_formule_lista=create dw_campi_formule_lista
this.dw_campi_formule_det=create dw_campi_formule_det
this.cb_estrazione=create cb_estrazione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_campi_formule_lista
this.Control[iCurrent+2]=this.dw_campi_formule_det
this.Control[iCurrent+3]=this.cb_estrazione
end on

on w_tab_campi_formule.destroy
call super::destroy
destroy(this.dw_campi_formule_lista)
destroy(this.dw_campi_formule_det)
destroy(this.cb_estrazione)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_campi_formule_det,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

event pc_setwindow;call super::pc_setwindow;dw_campi_formule_lista.set_dw_key("cod_azienda")
dw_campi_formule_lista.set_dw_key("cod_obiettivo")
dw_campi_formule_lista.set_dw_key("cod_indicatore")
dw_campi_formule_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_campi_formule_det.set_dw_options(sqlca,dw_campi_formule_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_campi_formule_lista
end event

type dw_campi_formule_lista from uo_cs_xx_dw within w_tab_campi_formule
integer x = 23
integer y = 20
integer width = 2080
integer height = 500
integer taborder = 10
string dataobject = "d_campi_formule_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_delete;call super::pcd_delete;integer li_row
li_row = getrow()
if not(li_row > 0 and not isnull(GetItemstring(li_row,"cod_campo"))) then
	cb_estrazione.enabled = true
else
	cb_estrazione.enabled = false
end if
end event

event pcd_modify;call super::pcd_modify;cb_estrazione.enabled = false
end event

event pcd_new;call super::pcd_new;cb_estrazione.enabled = false
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_campo")) then
	cb_estrazione.enabled = true
else
	cb_estrazione.enabled = false
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_campo")) then
	cb_estrazione.enabled = true
else
	cb_estrazione.enabled = false
end if
end event

event updatestart;call super::updatestart;integer li_i
string ls_cod_campo

if i_extendmode then
	for li_i = 1 to this.deletedcount()
	   ls_cod_campo  = this.getitemstring(li_i, "cod_campo", delete!, true)

	   delete from det_formule
		where cod_azienda=:s_cs_xx.cod_azienda
		and 	cod_campo=:ls_cod_campo;
	next
end if
end event

type dw_campi_formule_det from uo_cs_xx_dw within w_tab_campi_formule
integer x = 23
integer y = 540
integer width = 2469
integer height = 840
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_campi_formule_det"
borderstyle borderstyle = styleraised!
end type

type cb_estrazione from commandbutton within w_tab_campi_formule
event clicked pbm_bnclicked
integer x = 2126
integer y = 20
integer width = 370
integer height = 80
integer taborder = 2
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Estrazione"
end type

event clicked;string ls_cod_campo
long ll_i[]

ll_i[1] = dw_campi_formule_det.getrow()
s_cs_xx.parametri.parametro_s_15 = dw_campi_formule_det.getitemstring(ll_i[1], "sql_script")
window_open(w_genera_campo, 0)

ls_cod_campo = dw_campi_formule_det.getitemstring(ll_i[1], "cod_campo")
UPDATE tab_campi_formule
   SET sql_script = :s_cs_xx.parametri.parametro_s_15
 WHERE cod_azienda = :s_cs_xx.cod_azienda and
 		 cod_campo = :ls_cod_campo;

dw_campi_formule_lista.triggerevent("pcd_retrieve")
dw_campi_formule_lista.set_selected_rows(1, &
                                         ll_i[], &
                                         c_ignorechanges, &
                                         c_refreshchildren, &
                                         c_refreshsame)
end event

