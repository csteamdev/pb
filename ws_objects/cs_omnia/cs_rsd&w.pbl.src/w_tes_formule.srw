﻿$PBExportHeader$w_tes_formule.srw
$PBExportComments$Finestra Testata Gestione Formule Standard
forward
global type w_tes_formule from w_cs_xx_principale
end type
type dw_tes_formule_lista from uo_cs_xx_dw within w_tes_formule
end type
type dw_tes_formule_det from uo_cs_xx_dw within w_tes_formule
end type
type cb_note_esterne from cb_documenti_compilati within w_tes_formule
end type
type cb_componi from cb_documenti_compilati within w_tes_formule
end type
type cb_calcola from commandbutton within w_tes_formule
end type
end forward

global type w_tes_formule from w_cs_xx_principale
integer width = 2528
integer height = 1532
string title = "Formule"
dw_tes_formule_lista dw_tes_formule_lista
dw_tes_formule_det dw_tes_formule_det
cb_note_esterne cb_note_esterne
cb_componi cb_componi
cb_calcola cb_calcola
end type
global w_tes_formule w_tes_formule

on w_tes_formule.create
int iCurrent
call super::create
this.dw_tes_formule_lista=create dw_tes_formule_lista
this.dw_tes_formule_det=create dw_tes_formule_det
this.cb_note_esterne=create cb_note_esterne
this.cb_componi=create cb_componi
this.cb_calcola=create cb_calcola
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_formule_lista
this.Control[iCurrent+2]=this.dw_tes_formule_det
this.Control[iCurrent+3]=this.cb_note_esterne
this.Control[iCurrent+4]=this.cb_componi
this.Control[iCurrent+5]=this.cb_calcola
end on

on w_tes_formule.destroy
call super::destroy
destroy(this.dw_tes_formule_lista)
destroy(this.dw_tes_formule_det)
destroy(this.cb_note_esterne)
destroy(this.cb_componi)
destroy(this.cb_calcola)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_formule_det,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

event pc_setwindow;call super::pc_setwindow;dw_tes_formule_lista.set_dw_key("cod_azienda")
dw_tes_formule_lista.set_dw_key("cod_formula")
dw_tes_formule_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tes_formule_det.set_dw_options(sqlca,dw_tes_formule_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_formule_lista
end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tes_formule_lista.getrow()
if not(li_row > 0 and not isnull(dw_tes_formule_lista.GetItemstring(li_row,"cod_formula"))) then
	cb_note_esterne.enabled = true
	cb_componi.enabled = true
else
	cb_note_esterne.enabled = false
	cb_componi.enabled = true
end if
cb_calcola.enabled = false
end event

type dw_tes_formule_lista from uo_cs_xx_dw within w_tes_formule
integer x = 23
integer y = 20
integer width = 2057
integer height = 500
integer taborder = 30
string dataobject = "d_tes_formule_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false
cb_componi.enabled = false
cb_calcola.enabled = true
end event

event pcd_new;call super::pcd_new;cb_note_esterne.enabled = false
cb_componi.enabled = false
cb_calcola.enabled = true
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_formula")) then
	cb_note_esterne.enabled = true
	cb_componi.enabled = true
else
	cb_note_esterne.enabled = false
	cb_componi.enabled = false
end if
cb_calcola.enabled = false
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_formula")) then
	cb_note_esterne.enabled = true
	cb_componi.enabled = true
else
	cb_note_esterne.enabled = false
	cb_componi.enabled = false
end if
cb_calcola.enabled = false
end event

event updatestart;call super::updatestart;integer li_i
string ls_cod_formula

if i_extendmode then
	for li_i = 1 to this.deletedcount()
	   ls_cod_formula  = this.getitemstring(li_i, "cod_formula", delete!, true)

	   delete from det_formule
		where cod_azienda=:s_cs_xx.cod_azienda
		and 	cod_formula=:ls_cod_formula;
	next
end if
end event

type dw_tes_formule_det from uo_cs_xx_dw within w_tes_formule
integer x = 23
integer y = 540
integer width = 2446
integer height = 876
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_tes_formule_det"
borderstyle borderstyle = styleraised!
end type

type cb_note_esterne from cb_documenti_compilati within w_tes_formule
event clicked pbm_bnclicked
integer x = 2103
integer y = 20
integer height = 80
integer taborder = 10
string text = "Docu&mento"
end type

event clicked;call super::clicked;string ls_cod_formula, ls_cod_blob, ls_db, ls_doc
integer li_risposta
transaction sqlcb
blob lbl_null, lbl_blob
long ll_prog_mimetype
setnull(lbl_null)

ls_cod_formula= dw_tes_formule_lista.getitemstring(dw_tes_formule_lista.getrow(), "cod_formula")

s_cs_xx.parametri.parametro_bl_1 = lbl_null

select prog_mimetype
into :ll_prog_mimetype
from tes_formule
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_formula = :ls_cod_formula;
	
selectblob blob
into :lbl_blob
from tes_formule
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_formula = :ls_cod_formula;
	

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tes_formule
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_formula = :ls_cod_formula;
	else
		updateblob tes_formule
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_formula = :ls_cod_formula;
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
		
	update tes_formule
	set prog_mimetype = :ll_prog_mimetype
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_formula = :ls_cod_formula;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//if ls_db = "MSSQL" then
//
//	li_risposta = f_crea_sqlcb(sqlcb)
//
//	selectblob note_esterne
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tes_formule
//	where      cod_azienda = :s_cs_xx.cod_azienda
//	and		  cod_formula = :ls_cod_formula
//	using      sqlcb;
//	
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//		
//else
//
//	selectblob note_esterne
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tes_formule
//	where      cod_azienda = :s_cs_xx.cod_azienda
//	and		  cod_formula = :ls_cod_formula;
//	
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//end if
//
//window_open(w_ole, 0)
//setpointer(hourglass!)
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob tes_formule
//	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda
//		and		  cod_formula = :ls_cod_formula
//		using      sqlcb;
//		
//	else
//		
//	   updateblob tes_formule
//	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda
//		and		  cod_formula = :ls_cod_formula;
//		
//	end if
//	
// 	commit;
//
//end if
//
setpointer(arrow!)
end event

type cb_componi from cb_documenti_compilati within w_tes_formule
event clicked pbm_bnclicked
integer x = 2103
integer y = 120
integer height = 80
integer taborder = 20
string text = "&Componi"
end type

event clicked;call super::clicked;string ls_cod_formula
long ll_i[]

ll_i[1] = dw_tes_formule_det.getrow()
ls_cod_formula = dw_tes_formule_det.getitemstring(ll_i[1], "cod_formula")
s_cs_xx.parametri.parametro_s_15 = dw_tes_formule_det.getitemstring(ll_i[1], "comp_codici")
window_open(w_genera_formule, 0)

if s_cs_xx.parametri.parametro_s_14 <> '%' and &
	s_cs_xx.parametri.parametro_s_13 <> '%' then

	UPDATE tes_formule
		SET comp_codici = :s_cs_xx.parametri.parametro_s_14, 
			 comp_descrittiva = :s_cs_xx.parametri.parametro_s_13,
			 ult_risultato = :s_cs_xx.parametri.parametro_d_1
	 WHERE cod_azienda = :s_cs_xx.cod_azienda and
			 cod_formula = :ls_cod_formula;
	
	dw_tes_formule_lista.triggerevent("pcd_retrieve")
	dw_tes_formule_lista.set_selected_rows(1, &
														  ll_i[], &
														  c_ignorechanges, &
														  c_refreshchildren, &
														  c_refreshsame)
end if
end event

type cb_calcola from commandbutton within w_tes_formule
event clicked pbm_bnclicked
integer x = 1189
integer y = 1040
integer width = 69
integer height = 80
integer taborder = 31
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;integer li_row
double ldd_risultato
string ls_comp_codici

li_row = dw_tes_formule_det.getrow()
if li_row > 0 then
	ls_comp_codici = dw_tes_formule_det.getitemstring(li_row,"comp_codici")
	if isnull(ls_comp_codici) or ls_comp_codici = "" then 
		g_mb.messagebox("Gestione Formule","La Formula Selezionata non è Stata Compilata")
		return
	else
		ldd_risultato = f_calcola_formula(ls_comp_codici)
		dw_tes_formule_det.setitem(li_row, "ult_risultato", ldd_risultato)
	end if
end if
end event

