﻿$PBExportHeader$w_tes_rev_sistema.srw
$PBExportComments$Finestra Testata Revisione Sistema Qualità
forward
global type w_tes_rev_sistema from w_cs_xx_principale
end type
type cb_dettagli from commandbutton within w_tes_rev_sistema
end type
type cb_note_esterne from cb_documenti_compilati within w_tes_rev_sistema
end type
type cb_report from commandbutton within w_tes_rev_sistema
end type
type dw_tes_rev_sistema_det from uo_cs_xx_dw within w_tes_rev_sistema
end type
type cb_ricerca from commandbutton within w_tes_rev_sistema
end type
type cb_reset from commandbutton within w_tes_rev_sistema
end type
type dw_tes_rev_sistema_lista from uo_cs_xx_dw within w_tes_rev_sistema
end type
type dw_selezione from datawindow within w_tes_rev_sistema
end type
type dw_folder_search from u_folder within w_tes_rev_sistema
end type
type cb_performance from commandbutton within w_tes_rev_sistema
end type
end forward

global type w_tes_rev_sistema from w_cs_xx_principale
integer width = 3154
integer height = 1592
string title = "Testata Revisione Sistema Qualità"
cb_dettagli cb_dettagli
cb_note_esterne cb_note_esterne
cb_report cb_report
dw_tes_rev_sistema_det dw_tes_rev_sistema_det
cb_ricerca cb_ricerca
cb_reset cb_reset
dw_tes_rev_sistema_lista dw_tes_rev_sistema_lista
dw_selezione dw_selezione
dw_folder_search dw_folder_search
cb_performance cb_performance
end type
global w_tes_rev_sistema w_tes_rev_sistema

on w_tes_rev_sistema.create
int iCurrent
call super::create
this.cb_dettagli=create cb_dettagli
this.cb_note_esterne=create cb_note_esterne
this.cb_report=create cb_report
this.dw_tes_rev_sistema_det=create dw_tes_rev_sistema_det
this.cb_ricerca=create cb_ricerca
this.cb_reset=create cb_reset
this.dw_tes_rev_sistema_lista=create dw_tes_rev_sistema_lista
this.dw_selezione=create dw_selezione
this.dw_folder_search=create dw_folder_search
this.cb_performance=create cb_performance
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_dettagli
this.Control[iCurrent+2]=this.cb_note_esterne
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.dw_tes_rev_sistema_det
this.Control[iCurrent+5]=this.cb_ricerca
this.Control[iCurrent+6]=this.cb_reset
this.Control[iCurrent+7]=this.dw_tes_rev_sistema_lista
this.Control[iCurrent+8]=this.dw_selezione
this.Control[iCurrent+9]=this.dw_folder_search
this.Control[iCurrent+10]=this.cb_performance
end on

on w_tes_rev_sistema.destroy
call super::destroy
destroy(this.cb_dettagli)
destroy(this.cb_note_esterne)
destroy(this.cb_report)
destroy(this.dw_tes_rev_sistema_det)
destroy(this.cb_ricerca)
destroy(this.cb_reset)
destroy(this.dw_tes_rev_sistema_lista)
destroy(this.dw_selezione)
destroy(this.dw_folder_search)
destroy(this.cb_performance)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[]

dw_tes_rev_sistema_lista.set_dw_key("cod_azienda")
dw_tes_rev_sistema_lista.set_dw_key("cod_obiettivo")
dw_tes_rev_sistema_lista.set_dw_key("cod_indicatore")
dw_tes_rev_sistema_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tes_rev_sistema_det.set_dw_options(sqlca,dw_tes_rev_sistema_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_rev_sistema_lista


dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_tes_rev_sistema_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_tes_rev_sistema_lista.change_dw_current()
dw_tes_rev_sistema_lista.resetupdate()

dw_selezione.insertrow(0)
dw_selezione.resetupdate()
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_rev_sistema_det,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_rev_sistema_det,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_tes_rev_sistema_lista,"cod_obiettivo",sqlca,&
                 "tab_obiettivi","cod_obiettivo","des_obiettivo",&
                 "(tab_obiettivi.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_obiettivi.flag_blocco <> 'S') or (tab_obiettivi.flag_blocco = 'S' and tab_obiettivi.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")
					  
f_PO_LoadDDDW_DW(dw_tes_rev_sistema_det,"cod_obiettivo",sqlca,&
                 "tab_obiettivi","cod_obiettivo","des_obiettivo",&
                 "(tab_obiettivi.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_obiettivi.flag_blocco <> 'S') or (tab_obiettivi.flag_blocco = 'S' and tab_obiettivi.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_tes_rev_sistema_lista,"cod_indicatore",sqlca,&
                 "tab_indicatori","cod_indicatore","des_indicatore",&
                 "(tab_indicatori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_indicatori.flag_blocco <> 'S') or (tab_indicatori.flag_blocco = 'S' and tab_indicatori.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

					  
f_PO_LoadDDDW_DW(dw_tes_rev_sistema_det,"cod_indicatore",sqlca,&
                 "tab_indicatori","cod_indicatore","des_indicatore",&
                 "(tab_indicatori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_indicatori.flag_blocco <> 'S') or (tab_indicatori.flag_blocco = 'S' and tab_indicatori.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")
					  
f_PO_LoadDDDW_DW(dw_selezione,"cod_obiettivo",sqlca,&
                 "tab_obiettivi","cod_obiettivo","des_obiettivo",&
                 "(tab_obiettivi.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_obiettivi.flag_blocco <> 'S') or (tab_obiettivi.flag_blocco = 'S' and tab_obiettivi.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_selezione,"cod_indicatore",sqlca,&
                 "tab_indicatori","cod_indicatore","des_indicatore",&
                 "(tab_indicatori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_indicatori.flag_blocco <> 'S') or (tab_indicatori.flag_blocco = 'S' and tab_indicatori.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")
					  
end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tes_rev_sistema_lista.getrow()
if not(li_row > 0 and not isnull(dw_tes_rev_sistema_lista.GetItemstring(li_row,"cod_obiettivo"))) then
	cb_dettagli.enabled = true
	cb_note_esterne.enabled = true
	cb_report.enabled = true
else
	cb_dettagli.enabled = false
	cb_note_esterne.enabled = false
	cb_report.enabled = false
end if
end event

type cb_dettagli from commandbutton within w_tes_rev_sistema
event clicked pbm_bnclicked
integer x = 2697
integer y = 20
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

event clicked;window_open_parm(w_det_rev_sistema, -1, dw_tes_rev_sistema_lista)
end event

type cb_note_esterne from cb_documenti_compilati within w_tes_rev_sistema
event clicked pbm_bnclicked
integer x = 2697
integer y = 120
integer height = 80
integer taborder = 60
string text = "Docu&mento"
end type

event clicked;call super::clicked;string ls_cod_indicatore, ls_cod_obiettivo
string ls_cod_blob, ls_db, ls_doc
integer li_risposta
long ll_prog_mimetype

transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)

ls_cod_obiettivo= dw_tes_rev_sistema_lista.getitemstring(dw_tes_rev_sistema_lista.getrow(), "cod_obiettivo")
ls_cod_indicatore= dw_tes_rev_sistema_lista.getitemstring(dw_tes_rev_sistema_lista.getrow(), "cod_indicatore")

s_cs_xx.parametri.parametro_bl_1 = lbl_null

select prog_mimetype
into :ll_prog_mimetype
from tes_rev_sistema
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_obiettivo = :ls_cod_obiettivo and
	cod_indicatore = :ls_cod_indicatore;

selectblob note_esterne
into :lbl_blob
from tes_rev_sistema
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_obiettivo = :ls_cod_obiettivo and
	cod_indicatore = :ls_cod_indicatore;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tes_rev_sistema
		set note_esterne = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_obiettivo = :ls_cod_obiettivo and
			cod_indicatore = :ls_cod_indicatore;
	else
		updateblob tes_rev_sistema
		set note_esterne = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_obiettivo = :ls_cod_obiettivo and
			cod_indicatore = :ls_cod_indicatore;
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
		
	update tes_rev_sistema
	set prog_mimetype = :ll_prog_mimetype
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_obiettivo = :ls_cod_obiettivo and
		cod_indicatore = :ls_cod_indicatore;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//if ls_db = "MSSQL" then
//
//	li_risposta = f_crea_sqlcb(sqlcb)
//	
//	selectblob note_esterne
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tes_rev_sistema
//	where      cod_azienda = :s_cs_xx.cod_azienda
//	and		  cod_obiettivo = :ls_cod_obiettivo
//	and		  cod_indicatore = :ls_cod_indicatore
//	using      sqlcb;
//	
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//	
//else
//	
//	selectblob note_esterne
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tes_rev_sistema
//	where      cod_azienda = :s_cs_xx.cod_azienda
//	and		  cod_obiettivo = :ls_cod_obiettivo
//	and		  cod_indicatore = :ls_cod_indicatore;
//	
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//
//end if
//
//window_open(w_ole, 0)
//setpointer(hourglass!)
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob tes_rev_sistema
//	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda
//		and		  cod_obiettivo = :ls_cod_obiettivo
//		and		  cod_indicatore = :ls_cod_indicatore
//		using      sqlcb;
//		
//		destroy sqlcb;
//		
//	else
//		
//	   updateblob tes_rev_sistema
//	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
//		where      cod_azienda = :s_cs_xx.cod_azienda
//		and		  cod_obiettivo = :ls_cod_obiettivo
//		and		  cod_indicatore = :ls_cod_indicatore;
//		
//	end if
//	
//   commit;
//
//end if

setpointer(arrow!)
end event

type cb_report from commandbutton within w_tes_rev_sistema
event clicked pbm_bnclicked
integer x = 2697
integer y = 220
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;window_open(w_rep_rev_sistema, -1)
end event

type dw_tes_rev_sistema_det from uo_cs_xx_dw within w_tes_rev_sistema
integer x = 23
integer y = 580
integer width = 3040
integer height = 880
integer taborder = 30
string dataobject = "d_tes_rev_sistema_det"
borderstyle borderstyle = styleraised!
end type

type cb_ricerca from commandbutton within w_tes_rev_sistema
integer x = 2286
integer y = 140
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;string ls_null

setnull(ls_null)

dw_selezione.setitem(1, "cod_obiettivo", ls_null)
dw_selezione.setitem(1, "cod_indicatore", ls_null)
dw_selezione.setitem(1, "flag_sistema", ls_null)


end event

type cb_reset from commandbutton within w_tes_rev_sistema
integer x = 2286
integer y = 40
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_folder_search.fu_SelectTab(1)
dw_tes_rev_sistema_lista.change_dw_current()

parent.triggerevent("pc_retrieve")
end event

type dw_tes_rev_sistema_lista from uo_cs_xx_dw within w_tes_rev_sistema
integer x = 137
integer y = 40
integer width = 2171
integer height = 500
integer taborder = 20
string dataobject = "d_tes_rev_sistema_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_obiettivo, new_select, where_clause, ls_count, ls_cod_indicatore, ls_flag_sistema, ls_where_comodo
long ll_i, ll_errore, ll_index

dw_selezione.accepttext()

ls_cod_obiettivo = dw_selezione.GetItemString(1, "cod_obiettivo")
ls_cod_indicatore = dw_selezione.GetItemString(1, "cod_indicatore")
ls_flag_sistema = dw_selezione.GetItemString(1, "flag_sistema")

where_clause = " where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_obiettivo) then
	where_clause = where_clause + " and cod_obiettivo = '" + ls_cod_obiettivo + "'"
end if

if not isnull(ls_cod_indicatore) then
	where_clause = where_clause + " and cod_indicatore = '" + ls_cod_indicatore + "'"
end if	

if not isnull(ls_flag_sistema) then
	where_clause = where_clause + " and flag_sistema like '" + ls_flag_sistema + "'"
end if	
 
ls_where_comodo = upper(string(dw_tes_rev_sistema_lista.GETSQLSELECT()))
ll_index = pos(ls_where_comodo, "WHERE")

if ll_index = 0 then
	new_select = dw_tes_rev_sistema_lista.GETSQLSELECT() + where_clause
else	
	new_select = left(dw_tes_rev_sistema_lista.GETSQLSELECT(), ll_index - 1) + where_clause
end if

ll_errore = dw_tes_rev_sistema_lista.SetSQLSelect(new_select)
if ll_errore = -1 then
	g_mb.messagebox("OMNIA","Errore in ricerca dati con la selezione impostata; cambiare i parametri di ricerca!")
	return
end if
dw_tes_rev_sistema_lista.resetupdate()

ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event updatestart;call super::updatestart;integer li_i
string ls_cod_obiettivo, ls_cod_indicatore

if i_extendmode then
	for li_i = 1 to this.deletedcount()
	   ls_cod_obiettivo  = this.getitemstring(li_i, "cod_obiettivo", delete!, true)
	   ls_cod_indicatore = this.getitemstring(li_i, "cod_indicatore", delete!, true)

	   delete from det_rev_sistema
		where cod_azienda=:s_cs_xx.cod_azienda
		and 	cod_obiettivo=:ls_cod_obiettivo
		and 	cod_indicatore=:ls_cod_indicatore;
	next
end if
end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_obiettivo")) then
	cb_dettagli.enabled = true
	cb_note_esterne.enabled = true
	cb_report.enabled = true
else
	cb_dettagli.enabled = false
	cb_note_esterne.enabled = false
	cb_report.enabled = false
end if
end event

event pcd_new;call super::pcd_new;cb_dettagli.enabled = false
cb_note_esterne.enabled = false
cb_report.enabled = false

dw_tes_rev_sistema_det.setitem(dw_tes_rev_sistema_lista.getrow(), "flag_sistema", "E")
end event

event pcd_modify;call super::pcd_modify;cb_dettagli.enabled = false
cb_note_esterne.enabled = false
cb_report.enabled = false
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_obiettivo")) then
	cb_dettagli.enabled = true
	cb_note_esterne.enabled = true
	cb_report.enabled = true
else
	cb_dettagli.enabled = false
	cb_note_esterne.enabled = false
	cb_report.enabled = false
end if
end event

type dw_selezione from datawindow within w_tes_rev_sistema
integer x = 137
integer y = 40
integer width = 2139
integer height = 420
integer taborder = 40
string dataobject = "d_sel_rev_sistema"
boolean border = false
boolean livescroll = true
end type

event itemchanged;//dw_selezione.setitem(1, "flag_sistema", "E")
end event

type dw_folder_search from u_folder within w_tes_rev_sistema
integer x = 23
integer y = 20
integer width = 2651
integer height = 540
integer taborder = 10
end type

type cb_performance from commandbutton within w_tes_rev_sistema
integer x = 2697
integer y = 320
integer width = 375
integer height = 80
integer taborder = 81
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Performance"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = dw_tes_rev_sistema_det.getitemstring(dw_tes_rev_sistema_lista.getrow(),"cod_obiettivo")
s_cs_xx.parametri.parametro_s_2 = dw_tes_rev_sistema_det.getitemstring(dw_tes_rev_sistema_lista.getrow(),"cod_indicatore")

window_open(w_performance, -1)
end event

