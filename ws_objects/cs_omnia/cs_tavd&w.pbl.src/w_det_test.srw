﻿$PBExportHeader$w_det_test.srw
$PBExportComments$Finestra Testata Gestione Test Autovalutazione
forward
global type w_det_test from w_cs_xx_principale
end type
type dw_det_test from uo_cs_xx_dw within w_det_test
end type
type cb_carica from commandbutton within w_det_test
end type
type cb_report from commandbutton within w_det_test
end type
end forward

global type w_det_test from w_cs_xx_principale
int Width=3402
int Height=1677
boolean TitleBar=true
string Title="Dettaglio Test"
dw_det_test dw_det_test
cb_carica cb_carica
cb_report cb_report
end type
global w_det_test w_det_test

type variables
long il_anno_registrazione, il_num_registrazione
string is_cod_test
end variables

on w_det_test.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_test=create dw_det_test
this.cb_carica=create cb_carica
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_test
this.Control[iCurrent+2]=cb_carica
this.Control[iCurrent+3]=cb_report
end on

on w_det_test.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_test)
destroy(this.cb_carica)
destroy(this.cb_report)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_test, &
                 "cod_paragrafo", &
                 sqlca, &
                 "tab_paragrafi_iso", &
                 "cod_paragrafo", &
                 "des_paragrafo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;//dw_det_test.set_dw_key("cod_azienda")
//dw_det_test.set_dw_key("anno_registrazione")
//dw_det_test.set_dw_key("num_registrazione")

dw_det_test.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent + c_nonew + c_nodelete, &
                                    c_default)
iuo_dw_main = dw_det_test



end event

type dw_det_test from uo_cs_xx_dw within w_det_test
int X=23
int Y=21
int Width=3315
int Height=1421
int TabOrder=10
string DataObject="d_det_test"
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione
string ls_cod_test

il_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
il_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
is_cod_test = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")



parent.title = "Dettagli Test: " + is_cod_test


ll_errore = retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione, is_cod_test)
if ll_errore < 0 then
   pcca.error = c_fatal
end if

if this.rowcount() > 0 then
	cb_carica.enabled = false
	cb_report.enabled = true
else
	cb_carica.enabled = true
	cb_report.enabled = false
end if
end event

event pcd_setkey;call super::pcd_setkey;// No new: non serve 
end event

event itemchanged;call super::itemchanged;string ls_cod_paragrafo_old
long ll_progressivo
if i_extendmode then
   choose case i_colname
		case "cod_paragrafo"		
			// Attenzione: campo cod_paragrafo copiato in det_test da det_test_valutazione
			// solo per prestazioni di "Report Valuatzioni"; in teoria dovrebbero avere lo stesso valore ...
			// il progressivo di det_test_valutazione coincide con prog_det_test di det_test
			// vedi bottone cb_carica
			ll_progressivo = this.getitemnumber(this.getrow(), "progr_det_test")
			if isnull(ll_progressivo) then
				g_mb.messagebox("Omnia", "Errore lettura progressivo riga corrente ")
				return 2
			end if
			select cod_paragrafo
			into :ls_cod_paragrafo_old
			from det_test_valutazione
			where cod_azienda =:s_cs_xx.cod_azienda
			  and cod_test = :is_cod_test
			  and progressivo = :ll_progressivo;
			if sqlca.sqlcode <>0 then
				g_mb.messagebox("Omnia", "Errore lettura det_test_valutazione " + sqlca.sqlerrtext )
				return 2
			end if
			if ls_cod_paragrafo_old <> i_coltext then
				g_mb.messagebox("Omnia", "Attenzione! Questo campo dovrebbe essere concorde con quanto indicato in dettaglio definizione test")
			end if	
	end choose
end if
end event

type cb_carica from commandbutton within w_det_test
int X=2972
int Y=1461
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="Carica Test"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;datetime ldt_oggi

if dw_det_test.rowcount() = 0 then
	// non indicato il campo valutazione
  INSERT INTO det_test  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           progr_det_test,   
           cod_paragrafo )  
  select :s_cs_xx.cod_azienda,
  			:il_anno_registrazione,
			:il_num_registrazione,
			progressivo,
			cod_paragrafo
	from det_test_valutazione
	where cod_test = :is_cod_test
	  and (flag_blocco = 'N' or (flag_blocco = 'S' and data_blocco > :ldt_oggi)) ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore inserimento dettaglio test " + sqlca.sqlerrtext )
		rollback;
	end if
end if

w_det_test.postevent("pc_retrieve")


end event

type cb_report from commandbutton within w_det_test
int X=2583
int Y=1461
int Width=366
int Height=81
int TabOrder=3
boolean BringToTop=true
string Text="Report"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = il_anno_registrazione
s_cs_xx.parametri.parametro_d_2 = il_num_registrazione  
window_open(w_report_test_autoval, -1)


end event

