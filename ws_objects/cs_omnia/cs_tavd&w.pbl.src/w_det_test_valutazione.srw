﻿$PBExportHeader$w_det_test_valutazione.srw
$PBExportComments$Finestra Definizione Test Autovalutazione: dettaglio (domande)
forward
global type w_det_test_valutazione from w_cs_xx_principale
end type
type dw_det_test_valutazione_lista from uo_cs_xx_dw within w_det_test_valutazione
end type
type dw_det_test_valutazione_det from uo_cs_xx_dw within w_det_test_valutazione
end type
end forward

global type w_det_test_valutazione from w_cs_xx_principale
int Width=2894
int Height=1405
boolean TitleBar=true
string Title="Definizione Dettagli Test"
dw_det_test_valutazione_lista dw_det_test_valutazione_lista
dw_det_test_valutazione_det dw_det_test_valutazione_det
end type
global w_det_test_valutazione w_det_test_valutazione

on w_det_test_valutazione.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_test_valutazione_lista=create dw_det_test_valutazione_lista
this.dw_det_test_valutazione_det=create dw_det_test_valutazione_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_test_valutazione_lista
this.Control[iCurrent+2]=dw_det_test_valutazione_det
end on

on w_det_test_valutazione.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_test_valutazione_lista)
destroy(this.dw_det_test_valutazione_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_det_test_valutazione_lista.set_dw_key("cod_azienda")
dw_det_test_valutazione_lista.set_dw_key("cod_test")

dw_det_test_valutazione_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_nohighlightselected)
dw_det_test_valutazione_det.set_dw_options(sqlca, &
                                    dw_det_test_valutazione_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
iuo_dw_main = dw_det_test_valutazione_lista



end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_test_valutazione_det, &
                 "cod_paragrafo", &
                 sqlca, &
                 "tab_paragrafi_iso", &
                 "cod_paragrafo", &
                 "des_paragrafo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")



end event

type dw_det_test_valutazione_lista from uo_cs_xx_dw within w_det_test_valutazione
int X=23
int Y=21
int Width=2812
int Height=541
string DataObject="d_det_test_valutazione_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_test

ls_cod_test = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")
parent.title = "Definizione Dettagli Test: " + ls_cod_test


ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_test)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_test

ls_cod_test = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_test")) or &
      this.getitemstring(ll_i, "cod_test") = "" then
      this.setitem(ll_i, "cod_test", ls_cod_test)
   end if
next

end event

event pcd_new;call super::pcd_new;string ls_cod_test
long ll_progressivo

ls_cod_test = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")


select max(progressivo)
into :ll_progressivo
from det_test_valutazione
where cod_azienda = :s_cs_xx.cod_azienda
  and cod_test = :ls_cod_test;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore lettura dettaglio test" + sqlca.sqlerrtext)
	return
end if

if isnull(ll_progressivo) then ll_progressivo = 0

ll_progressivo = ll_progressivo + 1
setitem(getrow(), "progressivo", ll_progressivo )




end event

type dw_det_test_valutazione_det from uo_cs_xx_dw within w_det_test_valutazione
int X=23
int Y=581
int Width=2812
int Height=701
int TabOrder=2
string DataObject="d_det_test_valutazione_det"
end type

