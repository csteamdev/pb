﻿$PBExportHeader$w_divisioni_blob.srw
$PBExportComments$Finestra di dettaglio documenti per le divisioni
forward
global type w_divisioni_blob from w_cs_xx_principale
end type
type dw_1 from uo_dw_drag within w_divisioni_blob
end type
type dw_anag_divisioni_blob_lista from uo_cs_xx_dw within w_divisioni_blob
end type
end forward

global type w_divisioni_blob from w_cs_xx_principale
integer width = 2121
integer height = 1280
string title = "Documenti"
dw_1 dw_1
dw_anag_divisioni_blob_lista dw_anag_divisioni_blob_lista
end type
global w_divisioni_blob w_divisioni_blob

type variables
blob ib_blob

private:
	string is_cod_divisione
	transaction it_transaction
end variables

on w_divisioni_blob.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.dw_anag_divisioni_blob_lista=create dw_anag_divisioni_blob_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.dw_anag_divisioni_blob_lista
end on

on w_divisioni_blob.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.dw_anag_divisioni_blob_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_anag_divisioni_blob_lista.set_dw_key("cod_azienda")

dw_anag_divisioni_blob_lista.set_dw_options(sqlca, &
											i_openparm, &
											c_scrollparent, &
											c_default)

iuo_dw_main = dw_anag_divisioni_blob_lista
	
dw_1.settransobject( sqlca )
dw_1.insertrow(0)

end event

type dw_1 from uo_dw_drag within w_divisioni_blob
integer x = 23
integer y = 840
integer width = 2034
integer height = 320
integer taborder = 30
string dataobject = "d_drag"
end type

event ue_start_drop;call super::ue_start_drop;string ls_errore

//creo la transazione (di istanza) che sarà usata per il salvataggio dei documenti
if not guo_functions.uof_create_transaction_from( sqlca, it_transaction, ls_errore)  then
	as_message = "Errore in creazione transazione~r~n" + ls_errore
	ab_return = false
	return
end if

ab_return = true

return
end event

event ue_end_drop;call super::ue_end_drop;long					ll_row

if ab_status then
	//commit
	commit using it_transaction;
	disconnect using it_transaction;
	destroy it_transaction;
else
	//rollback
	if isvalid(it_transaction) then
		rollback using it_transaction;
		disconnect using it_transaction;
		destroy it_transaction;
	end if
	
	if as_message<>"" and not isnull(as_message) then g_mb.error(as_message)
	
end if

dw_anag_divisioni_blob_lista.postevent("pcd_retrieve")

return true
end event

event ue_drop_file;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

integer			li_anno_registrazione
long				ll_num_registrazione, ll_row, ll_progressivo, ll_len, ll_prog_mimetype
string				ls_cod_nota, ls_des_blob, ls_dir, ls_file, ls_ext,ls_errore
blob				lb_note_esterne

select 	max(progressivo) 
into 		:ll_progressivo
from 		anag_divisioni_blob
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_divisione = :is_cod_divisione;
			
if ll_progressivo = 0 or isnull(ll_progressivo) then
	ll_progressivo = 1
else
	ll_progressivo += 1
end if

ls_des_blob = "Documento"

guo_functions.uof_file_to_blob(as_filename, lb_note_esterne)

ll_len = lenA(lb_note_esterne)

guo_functions.uof_get_file_info( as_filename, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;
if sqlca.sqlcode <> 0 then
	as_message = "Attenzione: estensione file " + ls_ext + " non prevista da sistema."
	return false
end if

insert into anag_divisioni_blob  
		( cod_azienda,   
		cod_divisione,
		  progressivo,   
		  des_blob, 
		  blob,
		  prog_mimetype,
		  descrizione)  
values ( :s_cs_xx.cod_azienda,   
		  :is_cod_divisione,   
		  :ll_progressivo,
		  :ls_file,   
		  null,
		 :ll_prog_mimetype,
		 :ls_file) 
using it_transaction;

if it_transaction.sqlcode = 0 then
	
	updateblob anag_divisioni_blob
	set blob = :lb_note_esterne
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_divisione = :is_cod_divisione and
			progressivo = :ll_progressivo
	using it_transaction;
			
	if it_transaction.sqlcode <> 0 then
		as_message = "Errore nell'inserimento del documento digitale.~r~n" + it_transaction.sqlerrtext
		return false
	end if
	
else
	
	if it_transaction.sqlcode <> 0 then
		as_message = "Errore nell'inserimento del documento digitale.~r~n" + it_transaction.sqlerrtext
		return false
	end if
end if

return true


end event

type dw_anag_divisioni_blob_lista from uo_cs_xx_dw within w_divisioni_blob
integer x = 23
integer y = 20
integer width = 2034
integer height = 780
integer taborder = 20
string dataobject = "d_anag_divisioni_blob_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_des_divisione
long  l_Error

is_cod_divisione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_divisione")

select des_divisione
into   :ls_des_divisione
from   anag_divisioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_divisione = :is_cod_divisione;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la ricerca della descrizione della divisione:" + sqlca.sqlerrtext, stopsign!)
else
	if isnull(ls_des_divisione) then ls_des_divisione = ""
	parent.title = "DOCUMENTI - " + is_cod_divisione + " " + ls_des_divisione
end if

l_Error = Retrieve(s_cs_xx.cod_azienda, is_cod_divisione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   this.setitem(ll_i, "cod_divisione", s_cs_xx.parametri.parametro_s_10)
next


end event

event pcd_new;
//lascia questo script con ancestor disattivato !!!!!

//nel nuovo non deve fare alcunchè


g_mb.warning("Per inserire una riga trascinare un file nell'area sottostante indicata!")
return
end event

event updatestart;call super::updatestart;long ll_i
if i_extendmode then
	
	for ll_i = 1 to rowcount()
		
		if getitemstatus(ll_i, 0, primary!) = NewModified!	or getitemstatus(ll_i, 0, primary!) = New! then
	
			long ll_progressivo
			double ld_num_protocollo
			
			select max(progressivo)
			into   :ll_progressivo
			from   anag_divisioni_blob
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_divisione = :s_cs_xx.parametri.parametro_s_10;
				
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in lettura dati da tabella DOCUMENTI ANAGRAFICA DIVISIONI:" + sqlca.sqlerrtext)
				pcca.error = c_Fatal
				return
			end if
			
			if ll_progressivo = 0 or isnull(ll_progressivo) then
				ll_progressivo = 1
			else
				ll_progressivo ++
			end if
			
			this.setitem( ll_i, "cod_divisione", s_cs_xx.parametri.parametro_s_10)
			this.setitem( ll_i, "progressivo", ll_progressivo)	
			
		end if	
	next	
	
end if
end event

event doubleclicked;call super::doubleclicked;string ls_temp_dir, ls_rnd, ls_estensione, ls_file_name, ls_cod_divisione
long ll_cont,ll_prog_mimetype, ll_len, ll_progressivo
blob lb_blob
uo_shellexecute luo_run

if row > 0 then
	
	setpointer(Hourglass!)
	
	ls_cod_divisione = getitemstring(row, "cod_divisione")
	ll_progressivo = getitemnumber(row, "progressivo")
	ll_prog_mimetype = getitemnumber(row, "prog_mimetype")
	
	selectblob blob
	into :lb_blob
	from anag_divisioni_blob
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_divisione = :ls_cod_divisione and
			progressivo = :ll_progressivo;
			
	if sqlca.sqlcode = 0  then 
		
		ll_len = lenA(lb_blob)
		
		select estensione
		into :ls_estensione
		from tab_mimetype
		where cod_azienda = :s_cs_xx.cod_azienda and
				prog_mimetype = :ll_prog_mimetype;
		
		ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
		
		ls_rnd = string( hour(now()),"00") +"_" + string( minute(now()),"00") +"_" + string( second(now()),"00") 
		ls_file_name =  ls_temp_dir + ls_rnd + "." + ls_estensione
		guo_functions.uof_blob_to_file( lb_blob, ls_file_name)
		
		
		luo_run = create uo_shellexecute
		luo_run.uof_run( handle(parent), ls_file_name )
		destroy(luo_run)
		
	end if
	
	setpointer(Arrow!)
	
end if
end event

