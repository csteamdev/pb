﻿$PBExportHeader$w_reparti_blob.srw
$PBExportComments$Finestra di dettaglio documenti per i reparti
forward
global type w_reparti_blob from w_cs_xx_principale
end type
type dw_anag_reparti_blob_lista from uo_cs_xx_dw within w_reparti_blob
end type
type cb_note_esterne from commandbutton within w_reparti_blob
end type
end forward

global type w_reparti_blob from w_cs_xx_principale
integer width = 2866
integer height = 1024
string title = "Documenti"
dw_anag_reparti_blob_lista dw_anag_reparti_blob_lista
cb_note_esterne cb_note_esterne
end type
global w_reparti_blob w_reparti_blob

type variables
blob ib_blob


end variables

on w_reparti_blob.create
int iCurrent
call super::create
this.dw_anag_reparti_blob_lista=create dw_anag_reparti_blob_lista
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_anag_reparti_blob_lista
this.Control[iCurrent+2]=this.cb_note_esterne
end on

on w_reparti_blob.destroy
call super::destroy
destroy(this.dw_anag_reparti_blob_lista)
destroy(this.cb_note_esterne)
end on

event pc_setwindow;call super::pc_setwindow;dw_anag_reparti_blob_lista.set_dw_key("cod_azienda")
dw_anag_reparti_blob_lista.set_dw_options(sqlca, &
											i_openparm, &
											c_scrollparent, &
											c_default)

iuo_dw_main = dw_anag_reparti_blob_lista

string ls_des_reparto

select des_reparto
into   :ls_des_reparto
from   anag_reparti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_reparto = :s_cs_xx.parametri.parametro_s_10;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la ricerca della descrizione del reparto:" + sqlca.sqlerrtext, stopsign!)
else
	if isnull(ls_des_reparto) then ls_des_reparto = ""
	this.title = "DOCUMENTI - " + s_cs_xx.parametri.parametro_s_10 + " " + ls_des_reparto
end if
	

end event

type dw_anag_reparti_blob_lista from uo_cs_xx_dw within w_reparti_blob
integer x = 23
integer y = 20
integer width = 2789
integer height = 780
integer taborder = 20
string dataobject = "d_anag_reparti_blob_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_10)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   this.setitem(ll_i, "cod_reparto", s_cs_xx.parametri.parametro_s_10)
next


end event

event pcd_new;call super::pcd_new;if i_extendmode then

//	long ll_progressivo
//	double ld_num_protocollo
//	
//	select max(progressivo) + 1
//	into   :ll_progressivo
//	from   anag_divisioni_blob
//	where  cod_azienda = :s_cs_xx.cod_azienda and 
//			 cod_divisione = :s_cs_xx.parametri.parametro_s_10;
//		
//	if sqlca.sqlcode < 0 then
//		messagebox("Omnia", "Errore in lettura dati da tabella DET_ATTREZZATURE")
//		pcca.error = c_Fatal
//		return
//	end if
//	
//	if ll_progressivo = 0 or isnull(ll_progressivo) then
//		ll_progressivo = 1
//	end if
//	this.setitem(this.getrow(), "progressivo", ll_progressivo)
//	this.setitem(this.getrow(), "progressivo", ll_progressivo)
//	this.setitem(this.getrow(), "flag_blocco", "N")
//	
//	cb_note_esterne.enabled = false		
	
end if	
end event

event pcd_save;call super::pcd_save;cb_note_esterne.enabled = true	
end event

event pcd_view;call super::pcd_view;cb_note_esterne.enabled = true	
end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false	
end event

event updatestart;call super::updatestart;long ll_i
if i_extendmode then
	
	for ll_i = 1 to rowcount()
		
		if getitemstatus(ll_i, 0, primary!) = NewModified!	or getitemstatus(ll_i, 0, primary!) = New! then
	
			long ll_progressivo
			double ld_num_protocollo
			
			select max(progressivo)
			into   :ll_progressivo
			from   anag_reparti_blob
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_reparto = :s_cs_xx.parametri.parametro_s_10;
				
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Omnia", "Errore in lettura dati da tabella DOCUMENTI ANAGRAFICA REPARTI:" + sqlca.sqlerrtext)
				pcca.error = c_Fatal
				return
			end if
			
			if ll_progressivo = 0 or isnull(ll_progressivo) then
				ll_progressivo = 1
			else
				ll_progressivo ++
			end if
			
			this.setitem( ll_i, "cod_reparto", s_cs_xx.parametri.parametro_s_10)
			this.setitem( ll_i, "progressivo", ll_progressivo)	
			
		end if	
	next	
	
end if
end event

type cb_note_esterne from commandbutton within w_reparti_blob
integer x = 2446
integer y = 820
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;long    ll_i, ll_progressivo, ll_prog_mimetype

integer li_risposta

string  ls_db

transaction sqlcb

blob    lbl_null

setnull(lbl_null)

ll_i = dw_anag_reparti_blob_lista.getrow()

if ll_i < 1 then return

ll_progressivo = dw_anag_reparti_blob_lista.getitemnumber(ll_i, "progressivo")
ll_prog_mimetype = dw_anag_reparti_blob_lista.getitemnumber( ll_i, "prog_mimetype")

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_reparti_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_reparto = :s_cs_xx.parametri.parametro_s_10 and 
				  progressivo = :ll_progressivo
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

	
	destroy sqlcb;
	
else
	
	selectblob blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_reparti_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_reparto = :s_cs_xx.parametri.parametro_s_10 and 
				  progressivo = :ll_progressivo;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype

window_open(w_ole_documenti, 0)

ll_prog_mimetype = s_cs_xx.parametri.parametro_d_1

if not isnull(s_cs_xx.parametri.parametro_bl_1) and len(s_cs_xx.parametri.parametro_bl_1) > 0  and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob anag_reparti_blob
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  cod_reparto = :s_cs_xx.parametri.parametro_s_10 and 
					  progressivo = :ll_progressivo
		using      sqlcb;
			
		if sqlcb.sqlcode <> 0 then
		   g_mb.messagebox("OMNIA", sqlcb.sqlerrtext)		
			rollback;
			return						
		end if	

		destroy sqlcb;
	
	else
	
   	updateblob anag_reparti_blob
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  cod_reparto = :s_cs_xx.parametri.parametro_s_10 and 
					  progressivo = :ll_progressivo;
				
		if sqlca.sqlcode <> 0 then
		   g_mb.messagebox("OMNIA", sqlca.sqlerrtext)
			rollback;
			return			
		end if	
		
	end if
				
//   update     anag_divisioni_blob
//   set        prog_mimetype = :ll_prog_mimetype
//   where      cod_azienda = :s_cs_xx.cod_azienda and
//				  cod_divisione = :s_cs_xx.parametri.parametro_s_10 and 
//				  progressivo = :ll_progressivo;
//	if sqlca.sqlcode <> 0 then
//	   messagebox("OMNIA", sqlca.sqlerrtext)
//		rollback;
//		return
//	end if					  
				  
   commit;
	
	dw_anag_reparti_blob_lista.setitem( ll_i, "prog_mimetype", ll_prog_mimetype)
	
	parent.triggerevent("pc_save")
	
	parent.triggerevent("pc_view")
	
end if

return

end event

