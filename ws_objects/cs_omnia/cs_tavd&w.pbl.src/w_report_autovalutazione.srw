﻿$PBExportHeader$w_report_autovalutazione.srw
$PBExportComments$Finestra Report Autovalutazione_statistiche
forward
global type w_report_autovalutazione from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_autovalutazione
end type
type cb_report from commandbutton within w_report_autovalutazione
end type
type cb_selezione from commandbutton within w_report_autovalutazione
end type
type dw_selezione from uo_cs_xx_dw within w_report_autovalutazione
end type
type dw_report from uo_cs_xx_dw within w_report_autovalutazione
end type
type cb_esci from commandbutton within w_report_autovalutazione
end type
type sle_1 from singlelineedit within w_report_autovalutazione
end type
type cb_ricerca_operaio from cb_operai_ricerca within w_report_autovalutazione
end type
end forward

global type w_report_autovalutazione from w_cs_xx_principale
integer width = 3561
integer height = 1676
string title = "Stampa Statistiche Test Autovalutazione"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
cb_esci cb_esci
sle_1 sle_1
cb_ricerca_operaio cb_ricerca_operaio
end type
global w_report_autovalutazione w_report_autovalutazione

type variables
boolean ib_interrupt
end variables

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2263
this.height = 773

end event

on w_report_autovalutazione.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.cb_esci=create cb_esci
this.sle_1=create sle_1
this.cb_ricerca_operaio=create cb_ricerca_operaio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.cb_esci
this.Control[iCurrent+7]=this.sle_1
this.Control[iCurrent+8]=this.cb_ricerca_operaio
end on

on w_report_autovalutazione.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.cb_esci)
destroy(this.sle_1)
destroy(this.cb_ricerca_operaio)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_test", &
							sqlca, &
							"tes_test_valutazione", &
							"cod_test", &
                     "des_test", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_paragrafo", &
							sqlca, &
							"tab_paragrafi_iso", &
							"cod_paragrafo", &
                     "des_paragrafo", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")





end event

type cb_annulla from commandbutton within w_report_autovalutazione
integer x = 1417
integer y = 560
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;ib_interrupt = true


end event

type cb_report from commandbutton within w_report_autovalutazione
integer x = 1806
integer y = 560
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_test_sel, ls_intestazione, ls_cod_operaio_sel, ls_valutazione_sel, ls_cod_paragrafo_sel, &
		 ls_sql, ls_valutazione, ls_cod_test, ls_cod_operaio, ls_cod_paragrafo, ls_des_test , ls_operatore, &
		 ls_domanda, ls_formato_data
datetime ldt_data_da, ldt_data_a, ldt_data_registrazione
long ll_prog_det_test_da, ll_prog_det_test_a, ll_progressivo, ll_newrow

dw_selezione.AcceptText()

dw_report.reset()
ib_interrupt = false

ls_formato_data = s_cs_xx.db_funzioni.formato_data

ls_cod_test_sel = dw_selezione.getItemString(1, "cod_test")
ls_cod_operaio_sel = dw_selezione.getItemString(1, "cod_operaio")
ls_valutazione_sel = dw_selezione.getItemString(1, "valutazione")
ls_cod_paragrafo_sel = dw_selezione.getItemString(1, "cod_paragrafo")

ldt_data_da = dw_selezione.getItemDatetime(1, "data_registrazione_da")
ldt_data_a = dw_selezione.getItemDatetime(1, "data_registrazione_a")
ll_prog_det_test_da = dw_selezione.getItemnumber(1, "prog_det_test_da")
ll_prog_det_test_a = dw_selezione.getItemnumber(1, "prog_det_test_a")
if isnull(ll_prog_det_test_da) and (not isnull(ll_prog_det_test_a)) then
	g_mb.messagebox("Omnia", "Inserire sia il progressivo da, sia il progressivo a, oppure nessuno")
	return
end if
if isnull(ll_prog_det_test_a) and (not isnull(ll_prog_det_test_da)) then
	g_mb.messagebox("Omnia", "Inserire sia il progressivo da, sia il progressivo a, oppure nessuno")
	return
end if


ls_intestazione = "Data da: " + string(ldt_data_da, "dd/mm/yyyy") + " a: " + &
		string(ldt_data_a, "dd/mm/yyyy")

ls_sql = "SELECT det_test.progr_det_test,   " + &
"		det_test.valutazione,  " + &
"		det_test.cod_paragrafo,  " + &
"		tes_test.data_registrazione, " + & 
"		tes_test.cod_test,   " + &
"		tes_test.cod_operaio " + &
" FROM det_test,   " + &
"		tes_test  " + &
"WHERE  tes_test.cod_azienda = det_test.cod_azienda  and   " + &
"		 tes_test.anno_registrazione = det_test.anno_registrazione  and   " + &
"		 tes_test.num_registrazione = det_test.num_registrazione  and   " + &
"		 det_test.cod_azienda = '" + s_cs_xx.cod_azienda + "'  and " + &
"      tes_test.data_registrazione >= '" + string(ldt_data_da, ls_formato_data) + "' and " + &
"      tes_test.data_registrazione <= '" + string(ldt_data_a, ls_formato_data) + "'  "




if not isnull(ls_cod_test_sel) then
	ls_intestazione = ls_intestazione + " cod. test:" + ls_cod_test_sel  + " "
	ls_sql = ls_sql + " and tes_test.cod_test ='" + ls_cod_test_sel + "' "
end if

if not isnull(ls_cod_operaio_sel) then
	ls_intestazione = ls_intestazione + " operatore:" + ls_cod_operaio_sel + " "
	ls_sql = ls_sql + " and tes_test.cod_operaio ='" + ls_cod_operaio_sel + "' "
end if

if not isnull(ls_valutazione_sel) then
	ls_intestazione = ls_intestazione + " valutazione:" + ls_valutazione_sel + " "
	ls_sql = ls_sql + " and det_test.valutazione ='" + ls_valutazione_sel + "' "
end if

if not isnull(ls_cod_paragrafo_sel) then
	ls_intestazione = ls_intestazione + " paragrafo:" + ls_cod_paragrafo_sel + " "
	ls_sql = ls_sql + " and det_test.cod_paragrafo ='" + ls_cod_paragrafo_sel + "' "
end if

if not isnull(ll_prog_det_test_da) then
	ls_intestazione = ls_intestazione + " righe da:" + string(ll_prog_det_test_da) + ", righe a:" + string(ll_prog_det_test_a) + " "
	ls_sql = ls_sql + " and det_test.progr_det_test >= " + string(ll_prog_det_test_da) + " "
	ls_sql = ls_sql + " and det_test.progr_det_test <= " + string(ll_prog_det_test_a) + " "
end if



// messagebox("debug", ls_sql)

				
DECLARE cur_1 DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql;
OPEN DYNAMIC cur_1 ;

DO while 0=0
	Yield()
	if ib_interrupt then  // var set in other script
		g_mb.messagebox("Stampa Test Autovaluatzioni","Operazione interrotta dall'utente")
		exit
	end if
	FETCH cur_1 INTO :ll_progressivo,   
		:ls_valutazione,   
		:ls_cod_paragrafo,   
		:ldt_data_registrazione,   
		:ls_cod_test,   
		:ls_cod_operaio ;

	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		close cur_1;
		return -1
	end if
	sle_1.text= "Test/data:" + ls_cod_test + "/" + string(ldt_data_registrazione, "dd/mm/yyyy")
	ls_des_test = f_des_tabella ( "tes_test_valutazione", "cod_test  = '" +   ls_cod_test + "'", "des_test" )
	ls_operatore = f_des_tabella ( "anag_operai", "cod_operaio = '" + ls_cod_operaio + "'", "cognome" ) +" " + f_des_tabella ( "anag_operai", "cod_operaio = '" +  ls_cod_operaio + "'", "nome" )
	ls_domanda = f_des_tabella ( "det_test_valutazione", "cod_test  = '" +   ls_cod_test + "' and progressivo = " + string(ll_progressivo) , "des_domanda" )
	
	ll_newrow = dw_report.insertrow(0) 
	dw_report.ScrollToRow(ll_newrow)
	dw_report.setitem(ll_newrow, "data_registrazione" , ldt_data_registrazione)
	dw_report.setitem(ll_newrow, "des_test"  , ls_des_test)
	dw_report.setitem(ll_newrow, "cod_paragrafo"  , ls_cod_paragrafo)
	dw_report.setitem(ll_newrow, "operatore"  , ls_operatore)
	dw_report.setitem(ll_newrow, "valutazione"  , ls_valutazione)
	dw_report.setitem(ll_newrow, "domanda"  , ls_domanda)
	dw_report.setitem(ll_newrow, "progressivo"  , ll_progressivo)
	

LOOP
CLOSE cur_1 ;


dw_report.accepttext()

dw_report.setsort("data_registrazione A, des_test A, operatore A, progressivo A")
dw_report.sort()
dw_report.groupcalc()


dw_selezione.visible=false
parent.x = 74
parent.y = 301
parent.width = 3562
parent.height = 1677

dw_report.object.intestazione.text = ls_intestazione

dw_report.visible=true

cb_selezione.visible = true
cb_annulla.visible = false
cb_esci.visible = false
cb_report.visible = false
dw_report.Change_DW_Current()



end event

type cb_selezione from commandbutton within w_report_autovalutazione
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_report.reset()
dw_report.hide()
dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2263
parent.height = 773

cb_selezione.visible = false
cb_annulla.visible = true
cb_esci.visible = true
cb_report.visible = true
cb_ricerca_operaio.visible = true


end event

type dw_selezione from uo_cs_xx_dw within w_report_autovalutazione
integer x = 23
integer y = 20
integer width = 2171
integer height = 520
integer taborder = 20
string dataobject = "d_report_autovalutazione_selezione"
end type

type dw_report from uo_cs_xx_dw within w_report_autovalutazione
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_autovalutazione"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type cb_esci from commandbutton within w_report_autovalutazione
integer x = 1097
integer y = 560
integer width = 297
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esci"
end type

event clicked;close(parent)
end event

type sle_1 from singlelineedit within w_report_autovalutazione
integer x = 23
integer y = 540
integer width = 1029
integer height = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = false
boolean autohscroll = false
end type

type cb_ricerca_operaio from cb_operai_ricerca within w_report_autovalutazione
integer x = 2080
integer y = 200
integer width = 73
integer height = 80
integer taborder = 2
boolean bringtotop = true
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_1 = dw_selezione
s_cs_xx.parametri.parametro_s_1 = "cod_operaio"
end event

