﻿$PBExportHeader$w_report_test_autoval.srw
$PBExportComments$Stampa il singolo Test compilato da operatore
forward
global type w_report_test_autoval from w_cs_xx_principale
end type
type dw_report_test_autoval from uo_cs_xx_dw within w_report_test_autoval
end type
end forward

global type w_report_test_autoval from w_cs_xx_principale
integer width = 3470
integer height = 2108
string title = "Stampa Test Autovalutazione"
dw_report_test_autoval dw_report_test_autoval
end type
global w_report_test_autoval w_report_test_autoval

type variables
long il_anno_registrazione, il_num_registrazione
end variables

event pc_setwindow;call super::pc_setwindow;dw_report_test_autoval.ib_dw_report = true


set_w_options(c_noresizewin)
il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2

dw_report_test_autoval.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
save_on_close(c_socnosave)




end event

on w_report_test_autoval.create
int iCurrent
call super::create
this.dw_report_test_autoval=create dw_report_test_autoval
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_test_autoval
end on

on w_report_test_autoval.destroy
call super::destroy
destroy(this.dw_report_test_autoval)
end on

type dw_report_test_autoval from uo_cs_xx_dw within w_report_test_autoval
integer x = 23
integer y = 20
integer width = 3383
integer height = 1960
string dataobject = "d_report_test_autoval"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
ll_errore = retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione)
if ll_errore < 0 then
   pcca.error = c_fatal
end if

end event

