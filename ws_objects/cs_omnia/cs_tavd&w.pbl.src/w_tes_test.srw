﻿$PBExportHeader$w_tes_test.srw
$PBExportComments$Finestra Compilazione test: testata
forward
global type w_tes_test from w_cs_xx_principale
end type
type dw_tes_test_lista from uo_cs_xx_dw within w_tes_test
end type
type cbx_tutte_date from checkbox within w_tes_test
end type
type cb_dettagli from commandbutton within w_tes_test
end type
type cb_ricerca_operaio from cb_operai_ricerca within w_tes_test
end type
type dw_tes_test_det from uo_cs_xx_dw within w_tes_test
end type
end forward

global type w_tes_test from w_cs_xx_principale
int Width=2967
int Height=1649
boolean TitleBar=true
string Title="Compilazione Test"
dw_tes_test_lista dw_tes_test_lista
cbx_tutte_date cbx_tutte_date
cb_dettagli cb_dettagli
cb_ricerca_operaio cb_ricerca_operaio
dw_tes_test_det dw_tes_test_det
end type
global w_tes_test w_tes_test

type variables
datetime idt_data_riferimento
string is_tutte_date, is_cod_operaio
end variables

on w_tes_test.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tes_test_lista=create dw_tes_test_lista
this.cbx_tutte_date=create cbx_tutte_date
this.cb_dettagli=create cb_dettagli
this.cb_ricerca_operaio=create cb_ricerca_operaio
this.dw_tes_test_det=create dw_tes_test_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tes_test_lista
this.Control[iCurrent+2]=cbx_tutte_date
this.Control[iCurrent+3]=cb_dettagli
this.Control[iCurrent+4]=cb_ricerca_operaio
this.Control[iCurrent+5]=dw_tes_test_det
end on

on w_tes_test.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tes_test_lista)
destroy(this.cbx_tutte_date)
destroy(this.cb_dettagli)
destroy(this.cb_ricerca_operaio)
destroy(this.dw_tes_test_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_tes_test_lista.set_dw_key("cod_azienda")
dw_tes_test_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
dw_tes_test_det.set_dw_options(sqlca, &
                                   dw_tes_test_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
iuo_dw_main = dw_tes_test_lista


idt_data_riferimento = datetime(today())
is_tutte_date = "N"


end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tes_test_det, &
                 "cod_test", &
                 sqlca, &
                 "tes_test_valutazione", &
                 "cod_test", &
                 "des_test", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  

end event

event open;call super::open;// dw_tes_test_lista.change_dw_focus(dw_tes_test_lista)
cb_ricerca_operaio.enabled = false
end event

type dw_tes_test_lista from uo_cs_xx_dw within w_tes_test
int X=23
int Y=21
int Width=2881
int Height=561
int TabOrder=30
string DataObject="d_tes_test_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, idt_data_riferimento, is_tutte_date)

if ll_errore < 0 then
	pcca.error = c_fatal
end if

if this.getrow() > 0 then
	cb_dettagli.enabled = true
else
	cb_dettagli.enabled = false
end if



end event

event pcd_new;call super::pcd_new;long ll_anno_registrazione 
this.setitem(this.getrow(), "data_registrazione", datetime(today()) )
ll_anno_registrazione = f_anno_esercizio()
this.setitem(this.getrow(), "anno_registrazione", ll_anno_registrazione )

cb_dettagli.enabled = false
cb_ricerca_operaio.enabled = true
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max_registrazione


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if this.getitemnumber(ll_i, "anno_registrazione") = 1 or &
      isnull(this.getitemnumber(ll_i, "anno_registrazione")) then

		ll_anno_registrazione = f_anno_esercizio()
      if ll_anno_registrazione > 0 then
         this.setitem(this.getrow(), "anno_registrazione", int(ll_anno_registrazione))
      else
			g_mb.messagebox("Gestione Test","Impostare l'anno di esercizio in parametri aziendali")
		end if
	end if
   if this.getitemnumber(ll_i, "num_registrazione") = 1 or &
      isnull(this.getitemnumber(ll_i, "num_registrazione")) then
		ll_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione")
		ll_max_registrazione = 0
		select max(num_registrazione)
		into   :ll_max_registrazione
		from   tes_test
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
		if not isnull(ll_max_registrazione) then
			if ll_max_registrazione >= 0 then ll_num_registrazione = ll_max_registrazione + 1
		else
			ll_num_registrazione = 1
		end if
      this.setitem(this.getrow(), "num_registrazione", ll_num_registrazione)
    end if
next      


end event

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_flag_elimina, ls_cod_test
	double ld_num_test
	long ll_anno_registrazione, ll_num_registrazione
	
   for li_i = 1 to this.deletedcount()
      ll_anno_registrazione  = this.getitemnumber(li_i, "anno_registrazione", delete!, true)
		ll_num_registrazione = this.getitemnumber(li_i, "num_registrazione", delete!, true)
		// se non è stato utilizzato, allora provo a cancellarne i dettagli		  
		delete from det_test
		where cod_azienda = :s_cs_xx.cod_azienda
		  and anno_registrazione = :ll_anno_registrazione
		  and num_registrazione = : ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore cancellazione dettagli Test: " + sqlca.sqlerrtext)
			dw_tes_test_lista.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if
		
	next
end if

cb_dettagli.enabled = true
cb_ricerca_operaio.enabled = false
end event

type cbx_tutte_date from checkbox within w_tes_test
int X=23
int Y=1441
int Width=796
int Height=81
boolean BringToTop=true
string Text="Visualizza Tutte le Date"
BorderStyle BorderStyle=StyleLowered!
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if this.checked then
	is_tutte_date = "S"
else
	is_tutte_date = "N"
end if

dw_tes_test_lista.change_dw_focus(dw_tes_test_lista)
parent.triggerevent("pc_retrieve")

end event

type cb_dettagli from commandbutton within w_tes_test
int X=2538
int Y=1441
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="Dettagli"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked; window_open_parm(w_det_test, -1, dw_tes_test_lista)

end event

type cb_ricerca_operaio from cb_operai_ricerca within w_tes_test
int X=2629
int Y=801
int TabOrder=10
boolean BringToTop=true
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_1 = dw_tes_test_det
s_cs_xx.parametri.parametro_s_1 = "cod_operaio"
end event

type dw_tes_test_det from uo_cs_xx_dw within w_tes_test
int X=23
int Y=601
int Width=2881
int Height=821
int TabOrder=40
string DataObject="d_tes_test_det"
end type

