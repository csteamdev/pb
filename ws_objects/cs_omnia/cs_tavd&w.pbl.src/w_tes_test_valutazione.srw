﻿$PBExportHeader$w_tes_test_valutazione.srw
$PBExportComments$Finestra Testata Definizione Test Autovalutazione
forward
global type w_tes_test_valutazione from w_cs_xx_principale
end type
type dw_tes_test_valutazione_lista from uo_cs_xx_dw within w_tes_test_valutazione
end type
type dw_tes_test_valutazione_det from uo_cs_xx_dw within w_tes_test_valutazione
end type
type cb_dettagli from commandbutton within w_tes_test_valutazione
end type
type cb_autorizza from commandbutton within w_tes_test_valutazione
end type
end forward

global type w_tes_test_valutazione from w_cs_xx_principale
integer width = 2459
integer height = 1936
string title = "DefinizioneTest di Autovalutazione"
dw_tes_test_valutazione_lista dw_tes_test_valutazione_lista
dw_tes_test_valutazione_det dw_tes_test_valutazione_det
cb_dettagli cb_dettagli
cb_autorizza cb_autorizza
end type
global w_tes_test_valutazione w_tes_test_valutazione

on w_tes_test_valutazione.create
int iCurrent
call super::create
this.dw_tes_test_valutazione_lista=create dw_tes_test_valutazione_lista
this.dw_tes_test_valutazione_det=create dw_tes_test_valutazione_det
this.cb_dettagli=create cb_dettagli
this.cb_autorizza=create cb_autorizza
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_test_valutazione_lista
this.Control[iCurrent+2]=this.dw_tes_test_valutazione_det
this.Control[iCurrent+3]=this.cb_dettagli
this.Control[iCurrent+4]=this.cb_autorizza
end on

on w_tes_test_valutazione.destroy
call super::destroy
destroy(this.dw_tes_test_valutazione_lista)
destroy(this.dw_tes_test_valutazione_det)
destroy(this.cb_dettagli)
destroy(this.cb_autorizza)
end on

event pc_setwindow;call super::pc_setwindow;dw_tes_test_valutazione_lista.set_dw_key("cod_azienda")
dw_tes_test_valutazione_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
dw_tes_test_valutazione_det.set_dw_options(sqlca, &
                             dw_tes_test_valutazione_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)

iuo_dw_main = dw_tes_test_valutazione_lista
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tes_test_valutazione_det, &
                 "cod_area_aziendale", &
                 sqlca, &
                 "tab_aree_aziendali", &
                 "cod_area_aziendale", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'" )

f_po_loaddddw_dw(dw_tes_test_valutazione_det, &
                 "cod_divisione", &
                 sqlca, &
                 "anag_divisioni", &
                 "cod_divisione", &
                 "des_divisione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'" )

f_po_loaddddw_dw(dw_tes_test_valutazione_det, &
                 "cod_reparto", &
                 sqlca, &
                 "tab_reparti_dip", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'" )


end event

type dw_tes_test_valutazione_lista from uo_cs_xx_dw within w_tes_test_valutazione
integer x = 23
integer y = 20
integer width = 2377
integer height = 480
integer taborder = 20
string dataobject = "d_tes_test_valutazione_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_new;call super::pcd_new;
this.setitem(this.getrow(), "data_creazione", datetime(today()))


end event

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_flag_elimina, ls_cod_test
	double ld_num_test
	
	if this.deletedcount() > 0 then		// verifico autorizzazioni cancellazione test
		//Giulio: 10/11/2011 cambio gestione privilegi mansionario
//		SELECT mansionari.flag_elimina
//		INTO :ls_flag_elimina
//		FROM mansionari
//		WHERE (mansionari.cod_azienda=:s_cs_xx.cod_azienda) AND 
//				(mansionari.cod_utente =:s_cs_xx.cod_utente);
		uo_mansionario luo_mansionario
		luo_mansionario = create uo_mansionario
		
		ls_flag_elimina = 'N'
		
		if luo_mansionario.uof_get_privilege(luo_mansionario.elimina) then ls_flag_elimina = 'S'
		//--- Fine modifica Giulio
		
//		if sqlca.sqlcode<>0 then
		if isnull(luo_mansionario.uof_get_cod_mansionario()) then
			g_mb.messagebox("OMNIA", "Errore in ricerca nel DB: " + SQLCA.SQLErrText + "	OPERAZIONE BLOCCATA",StopSign!)
			dw_tes_test_valutazione_lista.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if
		
		if ls_flag_elimina="N" then	
			g_mb.messagebox("Omnia","Non si è in possesso del privilegio di eliminazione Test")
			dw_tes_test_valutazione_lista.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if	
	end if		// fine verifico autorizzazioni cancellazione test
	
	
   for li_i = 1 to this.deletedcount()
      ls_cod_test = this.getitemstring(li_i, "cod_test", delete!, true)
		// verifico che il test non sia stato già utilizzato
		select count(cod_test)
		into :ld_num_test
		from tes_test
		where cod_azienda = :s_cs_xx.cod_azienda
		  and cod_test = :ls_cod_test;
		
		if ld_num_test > 0 then
			g_mb.messagebox("Omnia", "Attenzione, questo test è già stato utilizzato nei test: non si può cancellare!")
			dw_tes_test_valutazione_lista.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if
		// se non è stato utilizzato, allora provo a cancellarne i dettagli		  
		delete from det_test_valutazione
		where cod_azienda = :s_cs_xx.cod_azienda
		  and cod_test = :ls_cod_test;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Omnia", "Errore cancellazione dettagli Test: " + sqlca.sqlerrtext)
			dw_tes_test_valutazione_lista.set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if
		
	next
	
end if
end event

type dw_tes_test_valutazione_det from uo_cs_xx_dw within w_tes_test_valutazione
integer x = 23
integer y = 520
integer width = 2377
integer height = 1180
integer taborder = 30
string dataobject = "d_tes_test_valutazione_det"
borderstyle borderstyle = styleraised!
end type

type cb_dettagli from commandbutton within w_tes_test_valutazione
integer x = 2034
integer y = 1720
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dettagli"
end type

event clicked; window_open_parm(w_det_test_valutazione, -1, dw_tes_test_valutazione_lista)

end event

type cb_autorizza from commandbutton within w_tes_test_valutazione
integer x = 1623
integer y = 1720
integer width = 366
integer height = 80
integer taborder = 4
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Autorizza"
end type

event clicked;string ls_flag_autorizzazione
datetime ldt_oggi

ldt_oggi = datetime(today())

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.flag_autorizzazione
//INTO :ls_flag_autorizzazione
//FROM mansionari
//WHERE (mansionari.cod_azienda=:s_cs_xx.cod_azienda) AND 
//	(mansionari.cod_utente =:s_cs_xx.cod_utente);

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario
//--- Fine modifica Giulio

if sqlca.sqlcode<>0 then
	g_mb.messagebox("OMNIA", "Errore in ricerca utente nei mansionari: " + SQLCA.SQLErrText ,StopSign!)
	return
end if

if ls_flag_autorizzazione ="N" then	
	g_mb.messagebox("Omnia","Non si è in possesso del privilegio di Autorizzazione Test")
	return
end if	

dw_tes_test_valutazione_det.setitem(dw_tes_test_valutazione_det.getrow(), "autorizzato_da", s_cs_xx.cod_utente)
dw_tes_test_valutazione_det.setitem(dw_tes_test_valutazione_det.getrow(), "autorizzato_il", ldt_oggi)
parent.postevent("pc_save")

end event

