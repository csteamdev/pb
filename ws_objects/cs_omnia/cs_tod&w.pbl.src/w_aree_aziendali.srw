﻿$PBExportHeader$w_aree_aziendali.srw
$PBExportComments$Tabella Aree Aziendali
forward
global type w_aree_aziendali from w_cs_xx_principale
end type
type cb_documento from commandbutton within w_aree_aziendali
end type
type cb_reset from commandbutton within w_aree_aziendali
end type
type cb_ricerca from commandbutton within w_aree_aziendali
end type
type dw_selezione from uo_cs_xx_dw within w_aree_aziendali
end type
type dw_aree_aziendali_lista from uo_cs_xx_dw within w_aree_aziendali
end type
type dw_folder_search from u_folder within w_aree_aziendali
end type
type dw_aree_aziendali_det from uo_cs_xx_dw within w_aree_aziendali
end type
end forward

global type w_aree_aziendali from w_cs_xx_principale
integer width = 2423
integer height = 1824
string title = "Aree Aziendali"
event ue_seleziona_riga ( )
cb_documento cb_documento
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_selezione dw_selezione
dw_aree_aziendali_lista dw_aree_aziendali_lista
dw_folder_search dw_folder_search
dw_aree_aziendali_det dw_aree_aziendali_det
end type
global w_aree_aziendali w_aree_aziendali

event ue_seleziona_riga();boolean lb_trovato
long    ll_i

if dw_aree_aziendali_lista.rowcount() < 1 then return
if isnull(s_cs_xx.parametri.parametro_s_12) or s_cs_xx.parametri.parametro_s_12 = "" then return

lb_trovato = false

for ll_i = 1 to dw_aree_aziendali_lista.rowcount()
	if dw_aree_aziendali_lista.getitemstring( ll_i, "cod_area_aziendale") = s_cs_xx.parametri.parametro_s_12 then
		lb_trovato = true
		exit
	end if
next

if lb_trovato then
	dw_aree_aziendali_lista.setrow( ll_i)
	dw_aree_aziendali_lista.SelectRow( ll_i, TRUE)
end if

return
end event

event pc_setddlb;call super::pc_setddlb;string			ls_describe

f_PO_LoadDDDW_DW(dw_aree_aziendali_det,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "anag_divisioni.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  
f_PO_LoadDDDW_DW(dw_selezione,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//-------------------------------------------------------------------------------------------------------------------------------------------
//SINTEXCAL HA LE DW PERSONALIZZATE.
//LA PROSSIMA VOLTA CHE FATE LE MODIFICHE CONTROLLATE PER FAVORE CHE NON CI SIANO PERSONALIZZAZIONI
//OPPURE SE FATE UNA PERSONALIZZAZIONE CONTROLLATE CHE FUNZIONI PER TUTTI I CLIENTI ...
//ZIO SCATENATO!
//se sulla dw c'è il campo, allora fai f_po_loaddddw_dw
ls_describe = dw_aree_aziendali_det.Describe("cod_tipo_lista_dist.visible")
if ls_describe="1" or ls_describe= "0"	 then
	
	f_po_loaddddw_dw(dw_aree_aziendali_det, "cod_tipo_lista_dist", sqlca, &
					  "tab_tipi_liste_dist", "cod_tipo_lista_dist", "descrizione",&
					  "cod_tipo_lista_dist = cod_tipo_lista_dist or cod_tipo_lista_dist <> cod_tipo_lista_dist")
	
	f_po_loaddddw_dw( dw_aree_aziendali_det, "cod_lista_dist", sqlca, &
					"tes_liste_dist", "cod_lista_dist", "des_lista_dist", &
					"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
end if
//-------------------------------------------------------------------------------------------------------------------------------------------


end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[]

dw_aree_aziendali_lista.set_dw_key("cod_azienda")
dw_aree_aziendali_lista.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)
dw_aree_aziendali_det.set_dw_options(sqlca,dw_aree_aziendali_lista,c_sharedata+c_scrollparent,c_default)
dw_selezione.set_dw_options(sqlca, &
								 pcca.null_object, &
								 c_noretrieveonopen + &
								 c_newonopen + &
								 c_nodelete + &
								 c_nomodify + &
								 c_disableCC, &
								 c_default)
dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)								 

iuo_dw_main = dw_aree_aziendali_lista

l_objects[1] = dw_aree_aziendali_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_aree_aziendali_lista.change_dw_current()
dw_aree_aziendali_lista.resetupdate()
end event

on w_aree_aziendali.create
int iCurrent
call super::create
this.cb_documento=create cb_documento
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_selezione=create dw_selezione
this.dw_aree_aziendali_lista=create dw_aree_aziendali_lista
this.dw_folder_search=create dw_folder_search
this.dw_aree_aziendali_det=create dw_aree_aziendali_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_documento
this.Control[iCurrent+2]=this.cb_reset
this.Control[iCurrent+3]=this.cb_ricerca
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_aree_aziendali_lista
this.Control[iCurrent+6]=this.dw_folder_search
this.Control[iCurrent+7]=this.dw_aree_aziendali_det
end on

on w_aree_aziendali.destroy
call super::destroy
destroy(this.cb_documento)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_selezione)
destroy(this.dw_aree_aziendali_lista)
destroy(this.dw_folder_search)
destroy(this.dw_aree_aziendali_det)
end on

event open;call super::open;dw_selezione.postevent("getfocus")

end event

type cb_documento from commandbutton within w_aree_aziendali
integer x = 1989
integer y = 1620
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_aree_aziendali_lista.accepttext()

if dw_aree_aziendali_lista.getrow() > 0 and not isnull(dw_aree_aziendali_lista.getrow()) then 
//	s_cs_xx.parametri.parametro_s_10 = dw_aree_aziendali_lista.getitemstring(dw_aree_aziendali_lista.getrow(), "cod_area_aziendale")	
//	window_open_parm(w_aree_aziendali_blob, -1, dw_aree_aziendali_lista)

	s_cs_xx.parametri.parametro_s_1 = dw_aree_aziendali_lista.getitemstring(dw_aree_aziendali_lista.getrow(), "cod_area_aziendale")
	open(w_aree_aziendali_ole)
end if
end event

type cb_reset from commandbutton within w_aree_aziendali
integer x = 1975
integer y = 160
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla Ric."
end type

event clicked;string ls_null

setnull(ls_null)

dw_selezione.setItem(1, "cod_divisione", ls_null)

end event

type cb_ricerca from commandbutton within w_aree_aziendali
integer x = 1975
integer y = 64
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;// cb_search clicked event
//dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_aree_aziendali_lista.change_dw_current()

parent.triggerevent("pc_retrieve")




end event

type dw_selezione from uo_cs_xx_dw within w_aree_aziendali
integer x = 133
integer y = 52
integer width = 1819
integer height = 160
integer taborder = 30
string dataobject = "d_sel_aree_aziendali"
boolean border = false
end type

event pcd_inactive;call super::pcd_inactive;dw_selezione.Set_DW_Query(c_IgnoreChanges)
end event

event getfocus;call super::getfocus;dw_selezione.postevent("pcd_inactive")
end event

type dw_aree_aziendali_lista from uo_cs_xx_dw within w_aree_aziendali
integer x = 133
integer y = 52
integer width = 1819
integer height = 500
integer taborder = 10
string dataobject = "d_aree_aziendali_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_divisione, ls_old_select, new_select, where_clause, ls_count
long ll_i, ll_errore, ll_index

dw_selezione.accepttext()

ls_cod_divisione = dw_selezione.GetItemString(1, "cod_divisione")

where_clause = " where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_divisione) then
	where_clause = where_clause + " and cod_divisione = '" + ls_cod_divisione + "'"
end if

ls_old_select = dw_aree_aziendali_lista.GETSQLSELECT()

//ls_old_select = upper(ls_old_select)

ll_index = pos(upper(ls_old_select),"WHERE")

if ll_index = 0 then
	new_select = ls_old_select + where_clause
else	
	new_select = left(ls_old_select, ll_index - 1) + where_clause
end if

if dw_aree_aziendali_lista.SetSQLSelect(new_select) = -1 then
	g_mb.messagebox("OMNIA","Errore in impostazione select",stopsign!)
	return -1
end if

dw_aree_aziendali_lista.resetupdate()

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_folder_search from u_folder within w_aree_aziendali
integer x = 23
integer y = 20
integer width = 2341
integer height = 564
integer taborder = 10
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "L."
      SetFocus(dw_aree_aziendali_lista)
   CASE "R."
      SetFocus(dw_folder_search)
END CHOOSE
end event

type dw_aree_aziendali_det from uo_cs_xx_dw within w_aree_aziendali
integer x = 23
integer y = 600
integer width = 2331
integer height = 980
integer taborder = 20
string dataobject = "d_aree_aziendali_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;string ls_null
setnull(ls_null)


choose case dwo.name
		
	case "cod_tipo_lista_dist"
		
		if not isnull(data) and data <> "" then
			setitem( getrow(), "cod_lista_dist", ls_null)
				
			f_po_loaddddw_dw( dw_aree_aziendali_det, "cod_lista_dist", sqlca, &
				"tes_liste_dist", "cod_lista_dist", "des_lista_dist", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + data + "' ")
		end if
	
end choose

end event

