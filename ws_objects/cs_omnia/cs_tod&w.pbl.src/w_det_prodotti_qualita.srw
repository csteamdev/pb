﻿$PBExportHeader$w_det_prodotti_qualita.srw
$PBExportComments$Finestra Dettaglio Dati Qualità Prodotti
forward
global type w_det_prodotti_qualita from w_cs_xx_principale
end type
type dw_det_prodotti_qualita_lista from uo_cs_xx_dw within w_det_prodotti_qualita
end type
type dw_det_prodotti_qualita_det_1 from uo_cs_xx_dw within w_det_prodotti_qualita
end type
type dw_det_prodotti_qualita_det_2 from uo_cs_xx_dw within w_det_prodotti_qualita
end type
end forward

global type w_det_prodotti_qualita from w_cs_xx_principale
int Width=2574
int Height=729
boolean TitleBar=true
string Title="Dettaglio Dati Qualità Prodotti"
dw_det_prodotti_qualita_lista dw_det_prodotti_qualita_lista
dw_det_prodotti_qualita_det_1 dw_det_prodotti_qualita_det_1
dw_det_prodotti_qualita_det_2 dw_det_prodotti_qualita_det_2
end type
global w_det_prodotti_qualita w_det_prodotti_qualita

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_det_prodotti_qualita_lista.set_dw_key("cod_azienda")
dw_det_prodotti_qualita_lista.set_dw_key("cod_prodotto")
dw_det_prodotti_qualita_lista.set_dw_key("flag_req_meccanico")

dw_det_prodotti_qualita_lista.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)
dw_det_prodotti_qualita_det_1.set_dw_options(sqlca,dw_det_prodotti_qualita_lista,c_sharedata+c_scrollparent,c_default)
dw_det_prodotti_qualita_det_2.set_dw_options(sqlca,dw_det_prodotti_qualita_lista,c_sharedata+c_scrollparent,c_default)

end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_prodotti_qualita_det_1,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_prodotti_qualita_det_2,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end on

on activate;call w_cs_xx_principale::activate;if s_cs_xx.parametri.parametro_b_1 then
   dw_det_prodotti_qualita_det_2.Show()
   dw_det_prodotti_qualita_det_1.Hide()
else
   dw_det_prodotti_qualita_det_1.Show()
   dw_det_prodotti_qualita_det_2.Hide()
end if
end on

on w_det_prodotti_qualita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_prodotti_qualita_lista=create dw_det_prodotti_qualita_lista
this.dw_det_prodotti_qualita_det_1=create dw_det_prodotti_qualita_det_1
this.dw_det_prodotti_qualita_det_2=create dw_det_prodotti_qualita_det_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_prodotti_qualita_lista
this.Control[iCurrent+2]=dw_det_prodotti_qualita_det_1
this.Control[iCurrent+3]=dw_det_prodotti_qualita_det_2
end on

on w_det_prodotti_qualita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_prodotti_qualita_lista)
destroy(this.dw_det_prodotti_qualita_det_1)
destroy(this.dw_det_prodotti_qualita_det_2)
end on

type dw_det_prodotti_qualita_lista from uo_cs_xx_dw within w_det_prodotti_qualita
int X=23
int Y=21
int Width=618
int Height=581
int TabOrder=10
string DataObject="d_det_prodotti_qualita_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_new;call uo_cs_xx_dw::pcd_new;long ll_num_registrazione

string ls_prodotto

ls_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

select max(det_prodotti_qualita.prog_riga_requisiti)
  into :ll_num_registrazione
  from det_prodotti_qualita
  where (det_prodotti_qualita.cod_azienda = :s_cs_xx.cod_azienda) and (det_prodotti_qualita.cod_prodotto = :ls_prodotto);

if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
    dw_det_prodotti_qualita_lista.SetItem (dw_det_prodotti_qualita_lista.GetRow ( ),&
                                 "prog_riga_requisiti", 1 )
else
   ll_num_registrazione = ll_num_registrazione + 1
   dw_det_prodotti_qualita_lista.SetItem (dw_det_prodotti_qualita_lista.GetRow ( ),&
                                 "prog_riga_requisiti", ll_num_registrazione)
end if


end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

string ls_prodotto

ls_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_prodotto")) THEN
      SetItem(l_Idx, "cod_prodotto", ls_prodotto)
   END IF
   if s_cs_xx.parametri.parametro_b_1 then
      setitem(l_Idx, "flag_req_meccanico", "S")
   else
      setitem(l_Idx, "flag_req_meccanico", "N")
   end if
NEXT


end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_prodotto


ls_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

if s_cs_xx.parametri.parametro_b_1 = true then
   ll_errore = retrieve(s_cs_xx.cod_azienda, ls_prodotto, "S")
else
   ll_errore = retrieve(s_cs_xx.cod_azienda, ls_prodotto, "N")
end if

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end on

type dw_det_prodotti_qualita_det_1 from uo_cs_xx_dw within w_det_prodotti_qualita
int X=663
int Y=21
int Width=1852
int Height=581
int TabOrder=30
string DataObject="d_det_prodotti_qualita_det_1"
BorderStyle BorderStyle=StyleRaised!
end type

type dw_det_prodotti_qualita_det_2 from uo_cs_xx_dw within w_det_prodotti_qualita
int X=663
int Y=21
int Width=1852
int Height=581
int TabOrder=20
string DataObject="d_det_prodotti_qualita_det_2"
BorderStyle BorderStyle=StyleRaised!
end type

