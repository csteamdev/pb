﻿$PBExportHeader$w_difformita.srw
$PBExportComments$Tabella Errori e Difformità
forward
global type w_difformita from w_cs_xx_principale
end type
type cb_etichette from commandbutton within w_difformita
end type
type dw_difformita_lista from uo_cs_xx_dw within w_difformita
end type
type dw_difformita_dett from uo_cs_xx_dw within w_difformita
end type
end forward

global type w_difformita from w_cs_xx_principale
integer width = 2505
integer height = 2044
string title = "Difformità"
cb_etichette cb_etichette
dw_difformita_lista dw_difformita_lista
dw_difformita_dett dw_difformita_dett
end type
global w_difformita w_difformita

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_difformita_dett,"cod_trattamento",sqlca,&
                 "tab_trattamenti","cod_trattamento","des_trattamento", &
                 "tab_trattamenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_difformita_dett,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area", &
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_difformita_dett,"cod_tipo_lista_dist",sqlca,&
                 "tab_tipi_liste_dist","cod_tipo_lista_dist","descrizione","")
					  
f_PO_LoadDDDW_DW(dw_difformita_dett,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on w_difformita.create
int iCurrent
call super::create
this.cb_etichette=create cb_etichette
this.dw_difformita_lista=create dw_difformita_lista
this.dw_difformita_dett=create dw_difformita_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_etichette
this.Control[iCurrent+2]=this.dw_difformita_lista
this.Control[iCurrent+3]=this.dw_difformita_dett
end on

on w_difformita.destroy
call super::destroy
destroy(this.cb_etichette)
destroy(this.dw_difformita_lista)
destroy(this.dw_difformita_dett)
end on

event pc_setwindow;call super::pc_setwindow;dw_difformita_lista.set_dw_key("cod_azienda")

dw_difformita_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_difformita_dett.set_dw_options(sqlca,dw_difformita_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_difformita_lista
end event

type cb_etichette from commandbutton within w_difformita
integer x = 2080
integer y = 1840
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Etichette"
end type

event clicked;window_open(w_etichette_difformita,-1)
end event

type dw_difformita_lista from uo_cs_xx_dw within w_difformita
integer x = 23
integer y = 20
integer width = 2423
integer height = 500
integer taborder = 10
string dataobject = "d_difformita_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_modify;call super::pcd_modify;if i_extendmode then
	dw_difformita_dett.object.b_ricerca_prodotto.enabled=true
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	dw_difformita_dett.object.b_ricerca_prodotto.enabled=true
end if

return 0
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

event pcd_save;call super::pcd_save;if i_extendmode then
	dw_difformita_dett.object.b_ricerca_prodotto.enabled=false
end if

return 0
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_view;call super::pcd_view;if i_extendmode then
	dw_difformita_dett.object.b_ricerca_prodotto.enabled=false
end if

return 0
end event

event updatestart;call super::updatestart;long ll_i


for ll_i = 1 to rowcount()
	
	if (not isnull(dw_difformita_dett.getitemstring(dw_difformita_dett.getrow(),"cod_tipo_lista_dist")) and &
		isnull(dw_difformita_dett.getitemstring(dw_difformita_dett.getrow(),"cod_lista_dist"))) or &
		(isnull(dw_difformita_dett.getitemstring(dw_difformita_dett.getrow(),"cod_tipo_lista_dist")) and &
		not isnull(dw_difformita_dett.getitemstring(dw_difformita_dett.getrow(),"cod_lista_dist"))) then
		g_mb.messagebox("OMNIA","Impostare sia il tipo lista che il codice lista prima di salvare")
		return 1
	end if
	
next
end event

type dw_difformita_dett from uo_cs_xx_dw within w_difformita
integer x = 23
integer y = 540
integer width = 2423
integer height = 1280
integer taborder = 10
string dataobject = "d_difformita_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;string ls_cod_versione, ls_null


choose case i_colname
		
	case "cod_prodotto"
		
		f_PO_LoadDDDW_DW(dw_difformita_dett,"cod_versione",sqlca,&
							  "distinta_padri","cod_versione","des_versione",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext + "'")
								 
								 
		select cod_versione
		into   :ls_cod_versione
		from   distinta_padri
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:i_coltext
		and    flag_predefinita='S';
	
		if sqlca.sqlcode = 0 then 
			setitem(getrow(), "cod_versione", ls_cod_versione)
		else
			g_mb.messagebox("APICE","Errore in ricerca codice versione distinta predefinita. Verificare che sia stata impostata. Dettaglio errore=" + sqlca.sqlerrtext)
		end if
		
	case "cod_tipo_lista_dist"
		
		setnull(ls_null)
		
		setitem(getrow(),"cod_lista_dist",ls_null)
		
		if isnull(data) then
			
			f_PO_LoadDDDW_DW(dw_difformita_dett,"cod_lista_dist",sqlca,&
						  "tes_liste_dist","cod_lista_dist","des_lista_dist", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist is null")
			
		else	
		
			f_PO_LoadDDDW_DW(dw_difformita_dett,"cod_lista_dist",sqlca,&
						  "tes_liste_dist","cod_lista_dist","des_lista_dist", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + data +"'")
						  
		end if

end choose	

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_difformita_dett,"cod_prodotto")
end choose
end event

