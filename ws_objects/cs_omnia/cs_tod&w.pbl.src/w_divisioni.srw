﻿$PBExportHeader$w_divisioni.srw
$PBExportComments$Anagrafica Divisioni
forward
global type w_divisioni from w_cs_xx_principale
end type
type cb_pratiche from commandbutton within w_divisioni
end type
type cb_documento from commandbutton within w_divisioni
end type
type dw_divisioni_lista from uo_cs_xx_dw within w_divisioni
end type
type dw_divisioni_det from uo_cs_xx_dw within w_divisioni
end type
end forward

global type w_divisioni from w_cs_xx_principale
integer width = 2158
integer height = 2636
string title = "Divisioni"
event ue_seleziona_riga ( )
cb_pratiche cb_pratiche
cb_documento cb_documento
dw_divisioni_lista dw_divisioni_lista
dw_divisioni_det dw_divisioni_det
end type
global w_divisioni w_divisioni

event ue_seleziona_riga();boolean lb_trovato
long    ll_i

if dw_divisioni_lista.rowcount() < 1 then return
if isnull(s_cs_xx.parametri.parametro_s_12) or s_cs_xx.parametri.parametro_s_12 = "" then return

lb_trovato = false

for ll_i = 1 to dw_divisioni_lista.rowcount()
	if dw_divisioni_lista.getitemstring( ll_i, "cod_divisione") = s_cs_xx.parametri.parametro_s_12 then
		lb_trovato = true
		exit
	end if
next

if lb_trovato then
	dw_divisioni_lista.setrow( ll_i)
	dw_divisioni_lista.SelectRow( ll_i, TRUE)
end if

return
end event

event pc_setwindow;call super::pc_setwindow;dw_divisioni_lista.set_dw_key("cod_azienda")
dw_divisioni_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_divisioni_det.set_dw_options(sqlca,dw_divisioni_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_divisioni_lista

//cb_ricerca_cliente.enabled = false
end event

on w_divisioni.create
int iCurrent
call super::create
this.cb_pratiche=create cb_pratiche
this.cb_documento=create cb_documento
this.dw_divisioni_lista=create dw_divisioni_lista
this.dw_divisioni_det=create dw_divisioni_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_pratiche
this.Control[iCurrent+2]=this.cb_documento
this.Control[iCurrent+3]=this.dw_divisioni_lista
this.Control[iCurrent+4]=this.dw_divisioni_det
end on

on w_divisioni.destroy
call super::destroy
destroy(this.cb_pratiche)
destroy(this.cb_documento)
destroy(this.dw_divisioni_lista)
destroy(this.dw_divisioni_det)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_divisioni_det, &
                 "cod_agente", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
f_po_loaddddw_dw(dw_divisioni_det, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")					  


f_po_loaddddw_dw(dw_divisioni_det, &
                 "cod_tipo_commessa", &
                 sqlca, &
                 "tab_tipi_commessa", &
                 "cod_tipo_commessa", &
                 "des_tipo_commessa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")	
end event

type cb_pratiche from commandbutton within w_divisioni
integer x = 1701
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Pratiche"
end type

event clicked;window_open_parm(w_divisioni_tipi_pratiche, -1, dw_divisioni_lista)

end event

type cb_documento from commandbutton within w_divisioni
integer x = 1321
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_divisioni_lista.accepttext()

if dw_divisioni_lista.getrow() > 0 and not isnull(dw_divisioni_lista.getrow()) then 
	s_cs_xx.parametri.parametro_s_10 = dw_divisioni_lista.getitemstring(dw_divisioni_lista.getrow(), "cod_divisione")	
	window_open_parm(w_divisioni_blob, -1, dw_divisioni_lista)
//	s_cs_xx.parametri.parametro_s_1 = dw_divisioni_lista.getitemstring(dw_divisioni_lista.getrow(), "cod_divisione")	
//	open(w_divisioni_ole)
end if
end event

type dw_divisioni_lista from uo_cs_xx_dw within w_divisioni
integer x = 23
integer y = 20
integer width = 2057
integer height = 500
integer taborder = 10
string dataobject = "d_divisioni_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_new;call super::pcd_new;dw_divisioni_det.object.b_ricerca_cliente.enabled = true
end event

event pcd_modify;call super::pcd_modify;dw_divisioni_det.object.b_ricerca_cliente.enabled = true
end event

event pcd_view;call super::pcd_view;dw_divisioni_det.object.b_ricerca_cliente.enabled = false
end event

event doubleclicked;call super::doubleclicked;//if row < 1 then return
//
//if s_cs_xx.admin then
//
//	str_des_multilingua lstr_des_multilingua
//	
//	lstr_des_multilingua.nome_tabella = "anag_divisioni"
//	lstr_des_multilingua.descrizione_origine = getitemstring(row,"des_divisione")
//	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_divisione")
//	setnull(lstr_des_multilingua.chiave_str_2)
//	
//	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
//	
//	openwithparm(w_des_multilingua, lstr_des_multilingua)
//
//end if
end event

type dw_divisioni_det from uo_cs_xx_dw within w_divisioni
integer x = 46
integer y = 540
integer width = 2057
integer height = 1852
integer taborder = 20
string dataobject = "d_divisioni_det"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_divisioni_det,"cod_cliente")
end choose
end event

