﻿$PBExportHeader$w_divisioni_ricerca.srw
$PBExportComments$Finestra Divisioni Ricerca
forward
global type w_divisioni_ricerca from w_cs_xx_principale
end type
type cb_annulla from uo_cb_close within w_divisioni_ricerca
end type
type cb_ok from commandbutton within w_divisioni_ricerca
end type
type cb_des_prodotto from uo_cb_ok within w_divisioni_ricerca
end type
type cb_codice from uo_cb_ok within w_divisioni_ricerca
end type
type st_1 from statictext within w_divisioni_ricerca
end type
type dw_stringa_ricerca_prodotti from datawindow within w_divisioni_ricerca
end type
type st_2 from statictext within w_divisioni_ricerca
end type
type dw_divisioni from datawindow within w_divisioni_ricerca
end type
end forward

global type w_divisioni_ricerca from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2039
integer height = 1604
boolean titlebar = false
string title = ""
boolean controlmenu = false
boolean minbox = false
boolean maxbox = false
boolean resizable = false
boolean border = false
cb_annulla cb_annulla
cb_ok cb_ok
cb_des_prodotto cb_des_prodotto
cb_codice cb_codice
st_1 st_1
dw_stringa_ricerca_prodotti dw_stringa_ricerca_prodotti
st_2 st_2
dw_divisioni dw_divisioni
end type
global w_divisioni_ricerca w_divisioni_ricerca

type variables
long il_row
string is_tipo_ricerca='C'
string is_sql_aree
end variables

forward prototypes
public subroutine wf_pos_ricerca ()
public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca)
public subroutine wf_carica_aree ()
public function string wf_sanatize_string (string fs_stringa_ricerca)
end prototypes

public subroutine wf_pos_ricerca ();// s_cs_xx.parametri.parametro_tipo_ricerca  = 1   ----> ricerca per codice divisione
//															  2   ----> ricerca per descrizione divisione

dw_divisioni.reset()
if isnull(s_cs_xx.parametri.parametro_tipo_ricerca) or s_cs_xx.parametri.parametro_tipo_ricerca <> 2 then
	is_tipo_ricerca = "C"
	wf_dw_select(is_tipo_ricerca, s_cs_xx.parametri.parametro_pos_ricerca)
	st_1.text = "Ricerca per Codice:"
	dw_stringa_ricerca_prodotti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca)
else
	is_tipo_ricerca = "D"
	wf_dw_select(is_tipo_ricerca, s_cs_xx.parametri.parametro_pos_ricerca)
	st_1.text = "Ricerca Des.Div.:"
	dw_stringa_ricerca_prodotti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca)
end if	
setnull(s_cs_xx.parametri.parametro_pos_ricerca)
dw_divisioni.setfocus()


end subroutine

public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca);string ls_sql, ls_stringa,ls_profilo_corrente,ls_db
long ll_i, ll_y, ll_ret, ll_pos
integer li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "profilocorrente", "numero", ls_profilo_corrente)
li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + ls_profilo_corrente, "enginetype", ls_db)

choose case ls_db
	case "SYBASE_ASE"
		ls_sql = "select cod_divisione, des_divisione from anag_divisioni where cod_azienda = '" + s_cs_xx.cod_azienda + "'"// and flag_blocco = 'N'"
	case else
		ls_sql = "select cod_divisione, des_divisione from anag_divisioni where cod_azienda = '" + s_cs_xx.cod_azienda + "' "//and ( flag_blocco = 'N' or ( flag_blocco = 'S' and data_blocco > '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "' ))"
end choose

// ****************** aggiungo in automatico un asterisco alla fine della stringa *******
if right(trim(fs_stringa_ricerca), 1) <> "*" then
	fs_stringa_ricerca += "*"
end if
// ***************************************************************************************

ll_i = len(fs_stringa_ricerca)
for ll_y = 1 to ll_i
	if mid(fs_stringa_ricerca, ll_y, 1) = "*" then
		fs_stringa_ricerca = replace(fs_stringa_ricerca, ll_y, 1, "%")
	end if
next


//****************** correzione ricerca con apostrofo - Michele 23/04/2002 ******************

ll_pos = 1

do

   ll_pos = pos(fs_stringa_ricerca,"'",ll_pos)
	
   if ll_pos <> 0 then
      fs_stringa_ricerca = replace(fs_stringa_ricerca,ll_pos,1,"''")
      ll_pos = ll_pos + 2
   end if	

loop while ll_pos <> 0

//*******************************************************************************************


choose case fs_tipo_ricerca
	case "C"  				// tipo ricerca
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
			if pos(fs_stringa_ricerca, "%") > 0 then 
				ls_sql = ls_sql + " and cod_divisione like '" + fs_stringa_ricerca + "'"
			else
				ls_sql = ls_sql + " and cod_divisione = '" + fs_stringa_ricerca + "'"
			end if			
		end if
	   ls_sql = ls_sql + " order by cod_divisione"
	case "D"
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
			if pos(fs_stringa_ricerca, "%") > 0 then 
				ls_sql = ls_sql + " and des_divisione like '" + fs_stringa_ricerca + "'"
			else
				ls_sql = ls_sql + " and des_divisione = '" + fs_stringa_ricerca + "'"
			end if			
		end if
	   ls_sql = ls_sql + " order by des_divisione"
end choose
ll_ret = dw_divisioni.setsqlselect(ls_sql)
if ll_ret < 1 then
	messagebox("APICE", "Errore nella assegnazione sintassi SQL di ricerca con setsqlselect: comunicare l'errore all'amministratore del sistema")
	return
end if
ll_y = dw_divisioni.retrieve()
if ll_y > 0 then
	st_2.text = "TROVATE " + string(ll_y) + " DIVISIONI"
else
	st_2.text = "NESSUNA DIVISIONE TROVATA"
	dw_stringa_ricerca_prodotti.setfocus()
end if
end subroutine

public subroutine wf_carica_aree ();// Carico le aree filtrando per ubicazione se presente.
string ls_sql, ls_where, ls_cod_divisione, ls_cod_area, ls_ricerca
long	ll_row

ls_where = " where anag_divisioni.cod_azienda='"+s_cs_xx.cod_azienda+"' "

if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
	ll_row = s_cs_xx.parametri.parametro_uo_dw_1.getrow()
	ls_cod_divisione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_row, "cod_divisione")
	ls_cod_area = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_row, "cod_area_aziendale")
elseif not isnull(s_cs_xx.parametri.parametro_uo_dw_search) then
	ll_row = s_cs_xx.parametri.parametro_uo_dw_search.getrow()
	ls_cod_divisione = s_cs_xx.parametri.parametro_uo_dw_search.getitemstring(ll_row, "cod_divisione")
	ls_cod_area = s_cs_xx.parametri.parametro_uo_dw_search.getitemstring(ll_row, "cod_area_aziendale")

elseif not isnull(s_cs_xx.parametri.parametro_dw_1) then
	ll_row = s_cs_xx.parametri.parametro_dw_1.getrow()
	ls_cod_divisione = s_cs_xx.parametri.parametro_dw_1.getitemstring(ll_row, "cod_divisione")
	ls_cod_area = s_cs_xx.parametri.parametro_dw_1.getitemstring(ll_row, "cod_area_aziendale")

end if


// -- Divisione
if not isnull(ls_cod_divisione) and trim(ls_cod_divisione) <> "" then
	ls_where += " AND anag_divisioni.cod_divisione like '"+wf_sanatize_string(ls_cod_divisione)+"'"
end if
// -----------

// -- Area
if not isnull(ls_cod_area) and  trim(ls_cod_area) <> "" then
	ls_where += " AND tab_aree_aziendali.cod_area_aziendale like '"+wf_sanatize_string(ls_cod_area)+"'"
end if
// -----------

// -- Ricerca
dw_stringa_ricerca_prodotti.accepttext()
ls_ricerca = dw_stringa_ricerca_prodotti.getitemstring(dw_stringa_ricerca_prodotti.getrow(), "stringa_ricerca")
if not isnull(ls_ricerca) and trim(ls_ricerca) <> "" then
	ls_where += " AND tab_aree_aziendali.des_area like '" + wf_sanatize_string(ls_ricerca) + "' "
end if
// -----------

if dw_divisioni.setsqlselect(is_sql_aree + ls_where) < 1 then
	messagebox("Ricerca divisioni", "Impossibile impostare il nuovo SQL")
end if
dw_divisioni.retrieve()
end subroutine

public function string wf_sanatize_string (string fs_stringa_ricerca);long ll_pos, ll_i, ll_y

if right(trim(fs_stringa_ricerca), 1) <> "*" then
	fs_stringa_ricerca += "*"
end if
// ***************************************************************************************

ll_i = len(fs_stringa_ricerca)
for ll_y = 1 to ll_i
	if mid(fs_stringa_ricerca, ll_y, 1) = "*" then
		fs_stringa_ricerca = replace(fs_stringa_ricerca, ll_y, 1, "%")
	end if
next


//****************** correzione ricerca con apostrofo - Michele 23/04/2002 ******************

ll_pos = 1

do

   ll_pos = pos(fs_stringa_ricerca,"'",ll_pos)
	
   if ll_pos <> 0 then
      fs_stringa_ricerca = replace(fs_stringa_ricerca,ll_pos,1,"''")
      ll_pos = ll_pos + 2
   end if	

loop while ll_pos <> 0

return fs_stringa_ricerca
end function

on w_divisioni_ricerca.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.cb_des_prodotto=create cb_des_prodotto
this.cb_codice=create cb_codice
this.st_1=create st_1
this.dw_stringa_ricerca_prodotti=create dw_stringa_ricerca_prodotti
this.st_2=create st_2
this.dw_divisioni=create dw_divisioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.cb_des_prodotto
this.Control[iCurrent+4]=this.cb_codice
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.dw_stringa_ricerca_prodotti
this.Control[iCurrent+7]=this.st_2
this.Control[iCurrent+8]=this.dw_divisioni
end on

on w_divisioni_ricerca.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.cb_des_prodotto)
destroy(this.cb_codice)
destroy(this.st_1)
destroy(this.dw_stringa_ricerca_prodotti)
destroy(this.st_2)
destroy(this.dw_divisioni)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


// -- stefanop 02/03/2009
if s_cs_xx.parametri.parametro_s_1 = "cod_area_aziendale" then
	dw_divisioni.dataobject = "d_divisioni_tree"
	dw_divisioni.settransobject(sqlca)
	is_sql_aree = dw_divisioni.getsqlselect()
else
	dw_divisioni.settransobject(sqlca)
end if

dw_stringa_ricerca_prodotti.insertrow(0)
dw_divisioni.setrowfocusindicator(hand!)
// -------------------

//dw_divisioni.settransobject(sqlca)
//dw_stringa_ricerca_prodotti.insertrow(0)
//dw_divisioni.setrowfocusindicator(hand!)

if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) and len(s_cs_xx.parametri.parametro_pos_ricerca) > 0 then
	wf_pos_ricerca()
	dw_divisioni.setfocus()
else
	dw_stringa_ricerca_prodotti.setfocus()	
	//																															<> 2
	if isnull(s_cs_xx.parametri.parametro_tipo_ricerca) or s_cs_xx.parametri.parametro_tipo_ricerca  = 1 then
		is_tipo_ricerca = "C"
		st_1.text = "Ricerca per Codice:"
		dw_stringa_ricerca_prodotti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca)
		
	elseif s_cs_xx.parametri.parametro_s_1 = "cod_area_aziendale" then
		is_tipo_ricerca = "A"
		cb_codice.enabled = false;
		cb_des_prodotto.enabled = false;
		st_1.text = "Ricerca per Area:"
		dw_stringa_ricerca_prodotti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca) 
		wf_carica_aree()
	else
		is_tipo_ricerca = "D"
		st_1.text = "Ricerca Des.Div.:"
		dw_stringa_ricerca_prodotti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca) 
	end if	
end if

end event

event activate;call super::activate;dw_stringa_ricerca_prodotti.setfocus()
end event

type cb_annulla from uo_cb_close within w_divisioni_ricerca
integer x = 1257
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type cb_ok from commandbutton within w_divisioni_ricerca
integer x = 1646
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;if dw_divisioni.getrow() > 0 then
	
	// -- stefanop 03/03/2009: Codice per aree
	if s_cs_xx.parametri.parametro_s_1 = "cod_area_aziendale" then
		string ls_cod_divisione
		long ll_row
		
		if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
			ll_row = s_cs_xx.parametri.parametro_uo_dw_1.getrow()
			ls_cod_divisione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_row, "cod_divisione")
			if isnull(ls_cod_divisione) or trim(ls_cod_divisione) = "" then
				s_cs_xx.parametri.parametro_uo_dw_1.setitem(ll_row, "cod_divisione", dw_divisioni.getitemstring(dw_divisioni.getrow(), "anag_divisioni_cod_divisione"))
			end if
			s_cs_xx.parametri.parametro_uo_dw_1.setitem(ll_row, "cod_area_aziendale", dw_divisioni.getitemstring(dw_divisioni.getrow(), "tab_aree_aziendali_cod_area_aziendale"))

		elseif not isnull(s_cs_xx.parametri.parametro_uo_dw_search) then
			ll_row = s_cs_xx.parametri.parametro_uo_dw_search.getrow()
			ls_cod_divisione = s_cs_xx.parametri.parametro_uo_dw_search.getitemstring(ll_row, "cod_divisione")
			if isnull(ls_cod_divisione) or trim(ls_cod_divisione) = "" then
				s_cs_xx.parametri.parametro_uo_dw_search.setitem(ll_row, "cod_divisione", dw_divisioni.getitemstring(dw_divisioni.getrow(), "anag_divisioni_cod_divisione"))
			end if
			s_cs_xx.parametri.parametro_uo_dw_search.setitem(ll_row, "cod_area_aziendale", dw_divisioni.getitemstring(dw_divisioni.getrow(), "tab_aree_aziendali_cod_area_aziendale"))
			
		elseif not isnull(s_cs_xx.parametri.parametro_dw_1) then
			ll_row = s_cs_xx.parametri.parametro_dw_1.getrow()
			ls_cod_divisione = s_cs_xx.parametri.parametro_dw_1.getitemstring(ll_row, "cod_divisione")
			if isnull(ls_cod_divisione) or trim(ls_cod_divisione) = "" then
				s_cs_xx.parametri.parametro_dw_1.setitem(ll_row, "cod_divisione", dw_divisioni.getitemstring(dw_divisioni.getrow(), "anag_divisioni_cod_divisione"))
			end if
			s_cs_xx.parametri.parametro_dw_1.setitem(ll_row, "cod_area_aziendale", dw_divisioni.getitemstring(dw_divisioni.getrow(), "tab_aree_aziendali_cod_area_aziendale"))

		end if
		
		close(parent)
		return
	end if
	// ---------
	
	if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_1.setText (dw_divisioni.getitemstring(dw_divisioni.getrow(),"cod_divisione"))
		s_cs_xx.parametri.parametro_uo_dw_1.acceptText()
		s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_1.setfocus()
	elseif not isnull(s_cs_xx.parametri.parametro_uo_dw_search) then
		s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_search.settext(dw_divisioni.getitemstring(dw_divisioni.getrow(),"cod_divisione"))
		s_cs_xx.parametri.parametro_uo_dw_search.acceptText()
		s_cs_xx.parametri.parametro_uo_dw_search.setfocus()
	elseif not isnull(s_cs_xx.parametri.parametro_dw_1) then
		s_cs_xx.parametri.parametro_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_dw_1.settext(dw_divisioni.getitemstring(dw_divisioni.getrow(),"cod_divisione"))
		s_cs_xx.parametri.parametro_dw_1.acceptText()
		s_cs_xx.parametri.parametro_dw_1.setfocus()
	end if
	
	if il_row > 0 and not isnull(il_row) then
		dw_divisioni.setrow(il_row - 1)
		il_row = 0
	end if
	
	//parent.hide()
	close(parent)
	
end if
end event

type cb_des_prodotto from uo_cb_ok within w_divisioni_ricerca
integer x = 366
integer y = 140
integer width = 1646
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Descrizione Divisione"
end type

event clicked;call super::clicked;dw_divisioni.reset()
is_tipo_ricerca = "D"
st_1.text = "Ricerca Des.Div.:"
dw_stringa_ricerca_prodotti.setfocus()
end event

type cb_codice from uo_cb_ok within w_divisioni_ricerca
integer x = 23
integer y = 140
integer width = 343
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Codice"
end type

event clicked;call super::clicked;dw_divisioni.reset()
is_tipo_ricerca = "C"
st_1.text = "Ricerca per Codice:"
dw_stringa_ricerca_prodotti.setfocus()

end event

type st_1 from statictext within w_divisioni_ricerca
integer x = 23
integer y = 40
integer width = 686
integer height = 60
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ricerca per Codice:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_stringa_ricerca_prodotti from datawindow within w_divisioni_ricerca
event ue_key pbm_dwnkey
integer x = 731
integer y = 20
integer width = 1280
integer height = 100
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_stringa_ricerca_prodotti"
end type

event ue_key;string ls_stringa
if key = keyenter! then
	ls_stringa = dw_stringa_ricerca_prodotti.gettext()
	if s_cs_xx.parametri.parametro_s_1 = "cod_area_aziendale" then
		wf_carica_aree()
	else
		wf_dw_select(is_tipo_ricerca, ls_stringa)
	end if
	dw_divisioni.setfocus()
end if
end event

event getfocus;string ls_str

if getrow() > 0 then
	ls_str = getitemstring(1,"stringa_ricerca")
	this.setcolumn("stringa_ricerca")
	this.selecttext(1,len(ls_str))
end if
end event

type st_2 from statictext within w_divisioni_ricerca
integer x = 23
integer y = 1420
integer width = 1211
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type dw_divisioni from datawindow within w_divisioni_ricerca
event ue_key pbm_dwnkey
integer x = 23
integer y = 220
integer width = 1989
integer height = 1180
integer taborder = 30
string title = "none"
string dataobject = "d_divisioni_ricerca"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;CHOOSE CASE key
	CASE KeyEnter!
		il_row = this.getrow()
		cb_ok.triggerevent("clicked")
	Case keydownarrow! 
		if il_row < this.rowcount() then
			il_row = this.getrow() + 1
		end if
	Case keyuparrow!
		if il_row > 1 then
			il_row = this.getrow() - 1
		end if
END CHOOSE

end event

event doubleclicked;cb_ok.postevent(clicked!)

end event

