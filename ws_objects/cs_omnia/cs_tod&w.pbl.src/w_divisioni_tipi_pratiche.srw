﻿$PBExportHeader$w_divisioni_tipi_pratiche.srw
forward
global type w_divisioni_tipi_pratiche from w_cs_xx_principale
end type
type dw_divisioni_tipi_pratiche from uo_cs_xx_dw within w_divisioni_tipi_pratiche
end type
end forward

global type w_divisioni_tipi_pratiche from w_cs_xx_principale
integer width = 2103
integer height = 1264
string title = "Pratiche per Divisione/Utente"
dw_divisioni_tipi_pratiche dw_divisioni_tipi_pratiche
end type
global w_divisioni_tipi_pratiche w_divisioni_tipi_pratiche

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_divisioni_tipi_pratiche, &
                 "cod_tipo_pratica", &
                 sqlca, &
                 "tab_tipi_pratiche", &
                 "cod_tipo_pratica", &
                 "des_tipo_pratica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

end event

event pc_setwindow;call super::pc_setwindow;dw_divisioni_tipi_pratiche.set_dw_key("cod_azienda")
dw_divisioni_tipi_pratiche.set_dw_key("cod_divisione")
dw_divisioni_tipi_pratiche.set_dw_options(sqlca,i_openparm,c_default,c_default)

iuo_dw_main = dw_divisioni_tipi_pratiche

end event

on w_divisioni_tipi_pratiche.create
int iCurrent
call super::create
this.dw_divisioni_tipi_pratiche=create dw_divisioni_tipi_pratiche
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_divisioni_tipi_pratiche
end on

on w_divisioni_tipi_pratiche.destroy
call super::destroy
destroy(this.dw_divisioni_tipi_pratiche)
end on

type dw_divisioni_tipi_pratiche from uo_cs_xx_dw within w_divisioni_tipi_pratiche
integer x = 14
integer y = 20
integer width = 2030
integer height = 1120
integer taborder = 10
string dataobject = "d_divisioni_tipi_pratiche"
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;string			ls_cod_divisione, ls_des_divisione
long ll_ret

ls_cod_divisione = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_divisione")

select des_divisione
into   :ls_des_divisione
from   anag_divisioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_divisione = :ls_cod_divisione;
		 
if sqlca.sqlcode <> 0 then
	ls_des_divisione = ""
else
	if isnull(ls_des_divisione) then ls_des_divisione = ""
	parent.title = "Pratiche per Divisione/Utente: " + ls_cod_divisione + " " + ls_des_divisione
end if



ll_ret  = retrieve(s_cs_xx.cod_azienda, ls_cod_divisione)

if ll_ret < 0 then
   pcca.error = c_Fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_divisione, ls_cod_tipo_pratica
long   ll_max, ll_i

ls_cod_divisione = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_divisione")
	
for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_divisione")) or this.getitemstring(ll_i, "cod_divisione") = '' then
		this.setitem(ll_i, "cod_divisione", ls_cod_divisione)
	end if
	
	if isnull(getitemnumber(ll_i, "progressivo")) or getitemnumber(ll_i, "progressivo") < 1  then
		ls_cod_tipo_pratica = getitemstring(ll_i, "cod_tipo_pratica")
		
		if isnull(ls_cod_tipo_pratica) or len(ls_cod_tipo_pratica) < 1 then 
			pcca.error = c_fatal
			return 1
		end if
			
		select 	max(progressivo)
		into 		:ll_max
		from 		anag_divisioni_tipi_pratiche
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_divisione = :ls_cod_divisione and
					cod_tipo_pratica = :ls_cod_tipo_pratica;
					
		if isnull(ll_max) or ll_max < 1 then
			ll_max = 1
		else
			ll_max ++
		end if
		
			this.setitem(ll_i, "progressivo", ll_max)
		
	end if
	
next

end event

