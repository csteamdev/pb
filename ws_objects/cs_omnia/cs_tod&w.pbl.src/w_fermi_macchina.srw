﻿$PBExportHeader$w_fermi_macchina.srw
$PBExportComments$Window Fermi Macchina
forward
global type w_fermi_macchina from w_cs_xx_principale
end type
type dw_fermi_macchina_lista from uo_cs_xx_dw within w_fermi_macchina
end type
type dw_fermi_macchina_dett from uo_cs_xx_dw within w_fermi_macchina
end type
end forward

global type w_fermi_macchina from w_cs_xx_principale
integer width = 3099
integer height = 1460
string title = "Tipi Fermi Macchina"
dw_fermi_macchina_lista dw_fermi_macchina_lista
dw_fermi_macchina_dett dw_fermi_macchina_dett
end type
global w_fermi_macchina w_fermi_macchina

on w_fermi_macchina.create
int iCurrent
call super::create
this.dw_fermi_macchina_lista=create dw_fermi_macchina_lista
this.dw_fermi_macchina_dett=create dw_fermi_macchina_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fermi_macchina_lista
this.Control[iCurrent+2]=this.dw_fermi_macchina_dett
end on

on w_fermi_macchina.destroy
call super::destroy
destroy(this.dw_fermi_macchina_lista)
destroy(this.dw_fermi_macchina_dett)
end on

event pc_setwindow;call super::pc_setwindow;dw_fermi_macchina_lista.set_dw_key("cod_azienda")
dw_fermi_macchina_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_fermi_macchina_dett.set_dw_options(sqlca,dw_fermi_macchina_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_fermi_macchina_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_fermi_macchina_dett,"cod_tipo_fermo",sqlca,&
                 "tab_tipi_fermi_macchina","cod_tipo_fermo","des_tipo_fermo",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event clicked;call super::clicked;dw_fermi_macchina_dett.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_attrezzatura"
end event

event open;call super::open;dw_fermi_macchina_dett.object.b_ricerca_att.enabled=false
end event

type dw_fermi_macchina_lista from uo_cs_xx_dw within w_fermi_macchina
integer x = 23
integer y = 20
integer width = 960
integer height = 1320
string dataobject = "d_fermi_macchina_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_new;call super::pcd_new;if i_extendmode then
	long ll_anno_registrazione,ll_num_registrazione,ll_null

	setnull(ll_null)

	select numero
	into   :ll_anno_registrazione	
	from   parametri_azienda
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_parametro='ESC';

	select max(num_registrazione_fermo)
	into   :ll_num_registrazione
	from   fermi_macchina
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    anno_registrazione_fermo= :ll_anno_registrazione;

	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
   	ll_num_registrazione = 1
	else
   	ll_num_registrazione = ll_num_registrazione + 1
	end if

	this.SetItem (this.GetRow( ),"num_registrazione_fermo", ll_num_registrazione)
	this.SetItem (this.GetRow( ),"anno_registrazione_fermo", ll_anno_registrazione)
   this.SetItem (this.GetRow( ),"num_registrazione", ll_null)
   this.SetItem (this.GetRow( ),"prog_riga_manutenzione", ll_null)

	dw_fermi_macchina_dett.object.b_ricerca_att.enabled = true
	
end if
end event

event updatestart;call super::updatestart;long 	 ll_i

string ls_cod_attrezzatura, ls_cod_guasto, ls_test


for ll_i = 1 to rowcount()
	
	ls_cod_attrezzatura = getitemstring(ll_i,"cod_attrezzatura")
	
	if isnull(ls_cod_attrezzatura) then
		continue
	end if
	
	ls_cod_guasto = getitemstring(ll_i,"cod_guasto")
	
	if isnull(ls_cod_guasto) then
		continue
	end if
	
	select cod_azienda
	into	 :ls_test
	from	 tab_guasti
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_guasto = :ls_cod_guasto;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in controllo esistenza guasto specifico: " + sqlca.sqlerrtext,stopsign!)
		return 1
	elseif sqlca.sqlcode = 100 or isnull(ls_test) then
		
		insert
		into 	 tab_guasti
			  	 (cod_azienda,
			  	 cod_attrezzatura,
			  	 cod_guasto,
			  	 des_guasto)
		select cod_azienda,
				 :ls_cod_attrezzatura,
				 cod_guasto,
				 des_guasto
		from	 tab_guasti_generici
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 cod_guasto = :ls_cod_guasto;
				 
		if sqlca.sqlcode <> 0 then
			rollback;
			g_mb.messagebox("OMNIA","Errore in inserimento guasto specifico da guasto generico: " + sqlca.sqlerrtext,stopsign!)
			return 1
		else
			commit;
			f_PO_LoadDDDW_sort(dw_fermi_macchina_dett,"cod_guasto",sqlca,&
						  "tab_guasti","cod_guasto","des_guasto + ' ' + cod_attrezzatura", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + &
						  "' and cod_attrezzatura = '" + ls_cod_attrezzatura + &
						  "' union all select cod_guasto, des_guasto from tab_guasti_generici where cod_azienda = '" + &
						  s_cs_xx.cod_azienda + "' and cod_guasto not in (select cod_guasto from tab_guasti where " + &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + &
						  ls_cod_attrezzatura + "')","1")
		end if
		
	end if
	
next
end event

event pcd_view;call super::pcd_view;dw_fermi_macchina_dett.object.b_ricerca_att.enabled=false
end event

event pcd_modify;call super::pcd_modify;dw_fermi_macchina_dett.object.b_ricerca_att.enabled=true
end event

type dw_fermi_macchina_dett from uo_cs_xx_dw within w_fermi_macchina
event pcd_retrieve pbm_custom60
event pcd_setkey pbm_custom68
integer x = 1006
integer y = 20
integer width = 2034
integer height = 1320
string dataobject = "d_fermi_macchina_dett"
borderstyle borderstyle = styleraised!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event itemchanged;call super::itemchanged;if i_extendmode then
	
	if i_colname="cod_attrezzatura" then
		
		string ls_null
		
		setnull(ls_null)
		
		f_PO_LoadDDDW_sort(dw_fermi_macchina_dett,"cod_guasto",sqlca,&
						  "tab_guasti","cod_guasto","des_guasto + ' ' + cod_attrezzatura", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + &
						  "' and cod_attrezzatura = '" + i_coltext + &
						  "' union all select cod_guasto, des_guasto from tab_guasti_generici where cod_azienda = '" + &
						  s_cs_xx.cod_azienda + "' and cod_guasto not in (select cod_guasto from tab_guasti where " + &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + &
						  i_coltext + "')","1")
		
		setitem(row,"cod_guasto",ls_null)
		
	end if
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if isnull(currentrow) or currentrow < 1 then
	return 0
end if

f_PO_LoadDDDW_sort(this,"cod_guasto",sqlca,&
						  "tab_guasti","cod_guasto","des_guasto + ' ' + cod_attrezzatura", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + &
						  "' and cod_attrezzatura = '" + getitemstring(currentrow,"cod_attrezzatura") + &
						  "' union all select cod_guasto, des_guasto from tab_guasti_generici where cod_azienda = '" + &
						  s_cs_xx.cod_azienda + "' and cod_guasto not in (select cod_guasto from tab_guasti where " + &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + &
						  getitemstring(currentrow,"cod_attrezzatura") + "')","1")
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_fermi_macchina_dett,"cod_attrezzatura")
end choose
end event

