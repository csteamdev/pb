﻿$PBExportHeader$w_paragrafi_iso.srw
$PBExportComments$Finestra Tabella Paragrafi ISO
forward
global type w_paragrafi_iso from w_cs_xx_principale
end type
type dw_paragrafi_iso from uo_cs_xx_dw within w_paragrafi_iso
end type
end forward

global type w_paragrafi_iso from w_cs_xx_principale
int Width=2414
int Height=1141
boolean TitleBar=true
string Title="Paragrafi Norma ISO"
dw_paragrafi_iso dw_paragrafi_iso
end type
global w_paragrafi_iso w_paragrafi_iso

event pc_setwindow;call super::pc_setwindow;dw_paragrafi_iso.set_dw_key("cod_azienda")
dw_paragrafi_iso.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_paragrafi_iso
end event

on w_paragrafi_iso.create
int iCurrent
call w_cs_xx_principale::create
this.dw_paragrafi_iso=create dw_paragrafi_iso
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_paragrafi_iso
end on

on w_paragrafi_iso.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_paragrafi_iso)
end on

type dw_paragrafi_iso from uo_cs_xx_dw within w_paragrafi_iso
int X=23
int Y=21
int Width=2332
int Height=1001
string DataObject="d_paragrafi_iso"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

