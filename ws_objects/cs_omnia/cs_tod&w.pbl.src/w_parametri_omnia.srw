﻿$PBExportHeader$w_parametri_omnia.srw
$PBExportComments$Parametri Omnia
forward
global type w_parametri_omnia from w_cs_xx_principale
end type
type dw_parametri_omnia_lista from uo_cs_xx_dw within w_parametri_omnia
end type
type dw_parametri_omnia_det from uo_cs_xx_dw within w_parametri_omnia
end type
end forward

global type w_parametri_omnia from w_cs_xx_principale
int Width=2433
int Height=1317
boolean TitleBar=true
string Title="Parametri Omnia"
dw_parametri_omnia_lista dw_parametri_omnia_lista
dw_parametri_omnia_det dw_parametri_omnia_det
end type
global w_parametri_omnia w_parametri_omnia

event open;call super::open;string ls_modify


dw_parametri_omnia_lista.set_dw_key("cod_azienda")
dw_parametri_omnia_lista.set_dw_options(sqlca, &
                                          pcca.null_object, &
                                          c_default, &
                                          c_default)
dw_parametri_omnia_det.set_dw_options(sqlca, &
                                        dw_parametri_omnia_lista, &
                                        c_sharedata + c_scrollparent, &
                                        c_default)

ls_modify = "stringa.protect='0~tif(flag_parametro<>~~'S~~',1,0)'~t"
ls_modify = ls_modify + "flag.protect='0~tif(flag_parametro<>~~'F~~',1,0)'~t"
ls_modify = ls_modify + "data.protect='0~tif(flag_parametro<>~~'D~~',1,0)'~t"
ls_modify = ls_modify + "numero.protect='0~tif(flag_parametro<>~~'N~~',1,0)'~t"
dw_parametri_omnia_det.modify(ls_modify)

end event

on w_parametri_omnia.create
int iCurrent
call w_cs_xx_principale::create
this.dw_parametri_omnia_lista=create dw_parametri_omnia_lista
this.dw_parametri_omnia_det=create dw_parametri_omnia_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_parametri_omnia_lista
this.Control[iCurrent+2]=dw_parametri_omnia_det
end on

on w_parametri_omnia.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_parametri_omnia_lista)
destroy(this.dw_parametri_omnia_det)
end on

event pc_new;call super::pc_new;dw_parametri_omnia_det.setitem(dw_parametri_omnia_det.getrow(), "data", datetime(today()))

end event

type dw_parametri_omnia_lista from uo_cs_xx_dw within w_parametri_omnia
int X=23
int Y=21
int Width=2355
int Height=501
int TabOrder=10
string DataObject="d_parametri_omnia_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_savebefore;call super::pcd_savebefore;long ll_i
datetime ls_date


for ll_i = 1 to this.rowcount()
   choose case this.getitemstring(ll_i, "flag_parametro")
      case "S"
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "data", ls_date)
         this.setitem(ll_i, "numero", 0)
      case "F"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "data", ls_date)
         this.setitem(ll_i, "numero", 0)
      case "D"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "numero", 0)
      case "N"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "data", ls_date)
   end choose
next
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

type dw_parametri_omnia_det from uo_cs_xx_dw within w_parametri_omnia
int X=23
int Y=541
int Width=2355
int Height=661
int TabOrder=20
string DataObject="d_parametri_omnia_det"
BorderStyle BorderStyle=StyleRaised!
end type

event pcd_new;call super::pcd_new;string ls_modify


ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)
end event

event pcd_modify;call super::pcd_modify;string ls_modify


ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)
end event

