﻿$PBExportHeader$w_parametri_val_for.srw
$PBExportComments$Parametri Omnia per Valutazione Fornitori
forward
global type w_parametri_val_for from w_cs_xx_principale
end type
type dw_parametri_val_for_lista from uo_cs_xx_dw within w_parametri_val_for
end type
type dw_parametri_val_for_det from uo_cs_xx_dw within w_parametri_val_for
end type
end forward

global type w_parametri_val_for from w_cs_xx_principale
int Width=2433
int Height=1317
boolean TitleBar=true
string Title="Parametri Valutazione Fornitori"
dw_parametri_val_for_lista dw_parametri_val_for_lista
dw_parametri_val_for_det dw_parametri_val_for_det
end type
global w_parametri_val_for w_parametri_val_for

type variables
string is_cod_parametri[] = {"QCD","QCE","QS1","QS2","QS3","QVQ","QVS","Q03","Q04", &
             "Q05","Q06","Q07","Q08","Q09"}

end variables

event open;call super::open;string ls_modify


dw_parametri_val_for_lista.set_dw_key("cod_azienda")
dw_parametri_val_for_lista.set_dw_options(sqlca, &
                                          pcca.null_object, &
                                          c_default, &
                                          c_default)
dw_parametri_val_for_det.set_dw_options(sqlca, &
                                        dw_parametri_val_for_lista, &
                                        c_sharedata + c_scrollparent, &
                                        c_default)

ls_modify = "stringa.protect='0~tif(flag_parametro<>~~'S~~',1,0)'~t"
ls_modify = ls_modify + "flag.protect='0~tif(flag_parametro<>~~'F~~',1,0)'~t"
ls_modify = ls_modify + "data.protect='0~tif(flag_parametro<>~~'D~~',1,0)'~t"
ls_modify = ls_modify + "numero.protect='0~tif(flag_parametro<>~~'N~~',1,0)'~t"
dw_parametri_val_for_det.modify(ls_modify)

end event

on w_parametri_val_for.create
int iCurrent
call w_cs_xx_principale::create
this.dw_parametri_val_for_lista=create dw_parametri_val_for_lista
this.dw_parametri_val_for_det=create dw_parametri_val_for_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_parametri_val_for_lista
this.Control[iCurrent+2]=dw_parametri_val_for_det
end on

on w_parametri_val_for.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_parametri_val_for_lista)
destroy(this.dw_parametri_val_for_det)
end on

event pc_new;call super::pc_new;dw_parametri_val_for_det.setitem(dw_parametri_val_for_det.getrow(), "data", datetime(today()))

end event

type dw_parametri_val_for_lista from uo_cs_xx_dw within w_parametri_val_for
int X=23
int Y=21
int Width=2355
int Height=501
int TabOrder=10
string DataObject="d_parametri_val_for_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_parametri)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_savebefore;call super::pcd_savebefore;long ll_i
datetime ls_date


for ll_i = 1 to this.rowcount()
   choose case this.getitemstring(ll_i, "flag_parametro")
      case "S"
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "data", ls_date)
         this.setitem(ll_i, "numero", 0)
      case "F"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "data", ls_date)
         this.setitem(ll_i, "numero", 0)
      case "D"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "numero", 0)
      case "N"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "data", ls_date)
   end choose
next
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

type dw_parametri_val_for_det from uo_cs_xx_dw within w_parametri_val_for
int X=23
int Y=541
int Width=2355
int Height=661
int TabOrder=20
string DataObject="d_parametri_val_for_det"
BorderStyle BorderStyle=StyleRaised!
end type

event pcd_new;call super::pcd_new;string ls_modify


ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)
end event

event pcd_modify;call super::pcd_modify;string ls_modify

ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)
end event

event pcd_validaterow;call super::pcd_validaterow;boolean lb_valido
integer ll_i
string ls_codice

ls_codice = getitemstring(getrow(), "cod_parametro")
lb_valido = false
for ll_i=1 to upperbound(is_cod_parametri) 
	if ls_codice = is_cod_parametri[ll_i] then lb_valido = true
NEXT
if not lb_valido then
	g_mb.messagebox("Parametri Valutazione Fornitori", &
				  "Codice Parametro non Valido per la Gestione Valutazione Fornitori", &
				  exclamation!, ok!)

	pcca.error = pcca.window_currentdw.c_fatal
end if

end event

