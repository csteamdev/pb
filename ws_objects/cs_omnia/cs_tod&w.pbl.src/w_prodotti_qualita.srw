﻿$PBExportHeader$w_prodotti_qualita.srw
$PBExportComments$Finestra Gestione Dati Aggiuntivi Qualità Prodotti
forward
global type w_prodotti_qualita from w_cs_xx_principale
end type
type dw_prodotti_qualita_lista from uo_cs_xx_dw within w_prodotti_qualita
end type
type dw_prodotti_qualita_2 from uo_cs_xx_dw within w_prodotti_qualita
end type
type dw_prodotti_qualita_3 from uo_cs_xx_dw within w_prodotti_qualita
end type
type dw_prodotti_qualita_4 from uo_cs_xx_dw within w_prodotti_qualita
end type
type dw_prodotti_qualita_1 from uo_cs_xx_dw within w_prodotti_qualita
end type
type cb_ric_prod from cb_prod_ricerca within w_prodotti_qualita
end type
type dw_folder from u_folder within w_prodotti_qualita
end type
type cb_1 from cb_apri_manuale within w_prodotti_qualita
end type
end forward

global type w_prodotti_qualita from w_cs_xx_principale
int Width=2401
int Height=1829
boolean TitleBar=true
string Title="Dati Qualità Prodotti"
dw_prodotti_qualita_lista dw_prodotti_qualita_lista
dw_prodotti_qualita_2 dw_prodotti_qualita_2
dw_prodotti_qualita_3 dw_prodotti_qualita_3
dw_prodotti_qualita_4 dw_prodotti_qualita_4
dw_prodotti_qualita_1 dw_prodotti_qualita_1
cb_ric_prod cb_ric_prod
dw_folder dw_folder
cb_1 cb_1
end type
global w_prodotti_qualita w_prodotti_qualita

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_prodotti_qualita_2
dw_folder.fu_AssignTab(2, "Requisiti Tecnici", l_Objects[])
l_objects[1] = dw_prodotti_qualita_3
dw_folder.fu_AssignTab(3, "Specifiche Tecniche", l_Objects[])
l_objects[1] = dw_prodotti_qualita_4
dw_folder.fu_AssignTab(4, "Modalità Collaudo", l_Objects[])
l_objects[1] = dw_prodotti_qualita_1
l_objects[2] = cb_ric_prod
dw_folder.fu_AssignTab(1, "Dati Generici", l_Objects[])

dw_folder.fu_FolderCreate(4,4)
dw_folder.fu_SelectTab(1)


dw_prodotti_qualita_lista.set_dw_key("cod_azienda")
dw_prodotti_qualita_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_prodotti_qualita_1.set_dw_options(sqlca,dw_prodotti_qualita_lista,c_sharedata+c_scrollparent,c_default)
dw_prodotti_qualita_2.set_dw_options(sqlca,dw_prodotti_qualita_lista,c_sharedata+c_scrollparent,c_default)
dw_prodotti_qualita_3.set_dw_options(sqlca,dw_prodotti_qualita_lista,c_sharedata+c_scrollparent,c_default)
dw_prodotti_qualita_4.set_dw_options(sqlca,dw_prodotti_qualita_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_prodotti_qualita_lista
end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_prodotti_qualita_1,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_prodotti_qualita_lista,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end on

on w_prodotti_qualita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_prodotti_qualita_lista=create dw_prodotti_qualita_lista
this.dw_prodotti_qualita_2=create dw_prodotti_qualita_2
this.dw_prodotti_qualita_3=create dw_prodotti_qualita_3
this.dw_prodotti_qualita_4=create dw_prodotti_qualita_4
this.dw_prodotti_qualita_1=create dw_prodotti_qualita_1
this.cb_ric_prod=create cb_ric_prod
this.dw_folder=create dw_folder
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_prodotti_qualita_lista
this.Control[iCurrent+2]=dw_prodotti_qualita_2
this.Control[iCurrent+3]=dw_prodotti_qualita_3
this.Control[iCurrent+4]=dw_prodotti_qualita_4
this.Control[iCurrent+5]=dw_prodotti_qualita_1
this.Control[iCurrent+6]=cb_ric_prod
this.Control[iCurrent+7]=dw_folder
this.Control[iCurrent+8]=cb_1
end on

on w_prodotti_qualita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_prodotti_qualita_lista)
destroy(this.dw_prodotti_qualita_2)
destroy(this.dw_prodotti_qualita_3)
destroy(this.dw_prodotti_qualita_4)
destroy(this.dw_prodotti_qualita_1)
destroy(this.cb_ric_prod)
destroy(this.dw_folder)
destroy(this.cb_1)
end on

type dw_prodotti_qualita_lista from uo_cs_xx_dw within w_prodotti_qualita
int X=23
int Y=21
int Width=2309
int Height=481
int TabOrder=60
string DataObject="d_prodotti_qualita_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_prodotti_qualita_2 from uo_cs_xx_dw within w_prodotti_qualita
int X=69
int Y=641
int Width=2195
int Height=917
int TabOrder=50
string DataObject="d_prodotti_qualita_2"
boolean Border=false
end type

type dw_prodotti_qualita_3 from uo_cs_xx_dw within w_prodotti_qualita
int X=69
int Y=641
int Width=2195
int Height=917
int TabOrder=40
string DataObject="d_prodotti_qualita_3"
boolean Border=false
end type

type dw_prodotti_qualita_4 from uo_cs_xx_dw within w_prodotti_qualita
int X=69
int Y=641
int Width=2195
int Height=917
int TabOrder=30
string DataObject="d_prodotti_qualita_4"
boolean Border=false
end type

type dw_prodotti_qualita_1 from uo_cs_xx_dw within w_prodotti_qualita
int X=69
int Y=641
int Width=2195
int Height=921
int TabOrder=20
string DataObject="d_prodotti_qualita_1"
boolean Border=false
end type

type cb_ric_prod from cb_prod_ricerca within w_prodotti_qualita
int X=2149
int Y=681
int TabOrder=70
end type

type dw_folder from u_folder within w_prodotti_qualita
int X=23
int Y=521
int Width=2309
int Height=1081
int TabOrder=80
end type

type cb_1 from cb_apri_manuale within w_prodotti_qualita
int X=1966
int Y=1621
int TabOrder=10
end type

on clicked;call cb_apri_manuale::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SL5")
end on

