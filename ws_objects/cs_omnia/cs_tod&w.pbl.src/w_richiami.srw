﻿$PBExportHeader$w_richiami.srw
$PBExportComments$Finestra Parametrizzazione Richiamo Documenti
forward
global type w_richiami from w_cs_xx_principale
end type
type dw_richiami_lista from uo_cs_xx_dw within w_richiami
end type
type dw_richiami_dett from uo_cs_xx_dw within w_richiami
end type
end forward

global type w_richiami from w_cs_xx_principale
int Width=1953
int Height=1061
boolean TitleBar=true
string Title="Richiami Documenti"
dw_richiami_lista dw_richiami_lista
dw_richiami_dett dw_richiami_dett
end type
global w_richiami w_richiami

event pc_setwindow;call super::pc_setwindow;dw_richiami_lista.set_dw_key("cod_azienda")
dw_richiami_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_richiami_dett.set_dw_options(sqlca,dw_richiami_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_richiami_lista
end event

on w_richiami.create
int iCurrent
call w_cs_xx_principale::create
this.dw_richiami_lista=create dw_richiami_lista
this.dw_richiami_dett=create dw_richiami_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_richiami_lista
this.Control[iCurrent+2]=dw_richiami_dett
end on

on w_richiami.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_richiami_lista)
destroy(this.dw_richiami_dett)
end on

type dw_richiami_lista from uo_cs_xx_dw within w_richiami
int X=23
int Y=1
int Width=1875
int Height=501
int TabOrder=1
string DataObject="d_richiami_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_richiami_dett from uo_cs_xx_dw within w_richiami
int X=23
int Y=521
int Width=1875
int Height=421
boolean BringToTop=true
string DataObject="d_richiami_dett"
BorderStyle BorderStyle=StyleRaised!
end type

