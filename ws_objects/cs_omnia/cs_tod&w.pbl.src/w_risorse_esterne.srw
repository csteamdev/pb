﻿$PBExportHeader$w_risorse_esterne.srw
$PBExportComments$Window risorse_esterne
forward
global type w_risorse_esterne from w_cs_xx_principale
end type
type cb_tipi_richieste from commandbutton within w_risorse_esterne
end type
type dw_anag_risorse_esterne_dett from uo_cs_xx_dw within w_risorse_esterne
end type
type dw_anag_risorse_esterne_lista from uo_cs_xx_dw within w_risorse_esterne
end type
end forward

global type w_risorse_esterne from w_cs_xx_principale
integer width = 2368
integer height = 1828
string title = "Risorse Esterne"
cb_tipi_richieste cb_tipi_richieste
dw_anag_risorse_esterne_dett dw_anag_risorse_esterne_dett
dw_anag_risorse_esterne_lista dw_anag_risorse_esterne_lista
end type
global w_risorse_esterne w_risorse_esterne

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_anag_risorse_esterne_dett,"cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDDW_DW(dw_anag_risorse_esterne_dett,"cod_fornitore",sqlca,&
//                 "anag_fornitori","cod_fornitore","rag_soc_1",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//


//15/06/2011 Donato
//Gestione tipi richieste associate ad operai e risorse esterne
//f_po_loaddddw_dw(dw_anag_risorse_esterne_dett,&
//					"tipo_richiesta",&
//					sqlca,&
//					"tab_tipi_richieste",&
//					"cod_tipo_richiesta",&
//					"des_tipo_richiesta",&
//					"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
/*
f_po_loaddddw_sort(dw_anag_risorse_esterne_dett, &
						"tipo_richiesta", &
						sqlca, &
						"tab_richieste_tipologie", &
						"cod_tipologia_richiesta", &
						"des_tipologia_richiesta", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
*/
//--------------------------------------------------------------

f_po_loaddddw_dw(dw_anag_risorse_esterne_dett, &
                 "cod_utente", &
                 sqlca, &
                 "utenti", &
                 "cod_utente", &
                 "cod_utente + ' ' + cod_azienda + ' ' + nome_cognome ", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_anag_risorse_esterne_lista.set_dw_key("cod_azienda")
dw_anag_risorse_esterne_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_anag_risorse_esterne_dett.set_dw_options(sqlca,dw_anag_risorse_esterne_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_anag_risorse_esterne_lista
end on

on w_risorse_esterne.create
int iCurrent
call super::create
this.cb_tipi_richieste=create cb_tipi_richieste
this.dw_anag_risorse_esterne_dett=create dw_anag_risorse_esterne_dett
this.dw_anag_risorse_esterne_lista=create dw_anag_risorse_esterne_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_tipi_richieste
this.Control[iCurrent+2]=this.dw_anag_risorse_esterne_dett
this.Control[iCurrent+3]=this.dw_anag_risorse_esterne_lista
end on

on w_risorse_esterne.destroy
call super::destroy
destroy(this.cb_tipi_richieste)
destroy(this.dw_anag_risorse_esterne_dett)
destroy(this.dw_anag_risorse_esterne_lista)
end on

event pc_view;call super::pc_view;dw_anag_risorse_esterne_dett.object.b_ricerca_fornitore.enabled=false
end event

event pc_new;call super::pc_new;dw_anag_risorse_esterne_dett.object.b_ricerca_fornitore.enabled=true
end event

event pc_modify;call super::pc_modify;dw_anag_risorse_esterne_dett.object.b_ricerca_fornitore.enabled=true
end event

event open;call super::open;dw_anag_risorse_esterne_dett.object.b_ricerca_fornitore.enabled=false
end event

type cb_tipi_richieste from commandbutton within w_risorse_esterne
integer x = 1806
integer y = 1600
integer width = 480
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tipi Richieste"
end type

event clicked;window_open_parm(w_risorse_esterne_rich, -1, dw_anag_risorse_esterne_lista)
end event

type dw_anag_risorse_esterne_dett from uo_cs_xx_dw within w_risorse_esterne
integer x = 23
integer y = 520
integer width = 2263
integer height = 1060
integer taborder = 30
string dataobject = "d_anag_risorse_esterne_dett"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_anag_risorse_esterne_dett,"cod_fornitore")
end choose
end event

type dw_anag_risorse_esterne_lista from uo_cs_xx_dw within w_risorse_esterne
integer x = 23
integer y = 20
integer width = 2277
integer height = 480
integer taborder = 20
string dataobject = "d_anag_risorse_esterne_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event doubleclicked;call super::doubleclicked;//if row < 1 then return
//
//if s_cs_xx.admin then
//
//	str_des_multilingua lstr_des_multilingua
//	
//	lstr_des_multilingua.nome_tabella = "anag_risorse_esterne"
//	lstr_des_multilingua.descrizione_origine = getitemstring(row,"descrizione")
//	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_cat_risorse_esterne")
//	lstr_des_multilingua.chiave_str_2 = getitemstring(row,"cod_risorsa_esterna")
//	
//	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
//	
//	openwithparm(w_des_multilingua, lstr_des_multilingua)
//
//end if
end event

event pcd_new;call super::pcd_new;cb_tipi_richieste.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_tipi_richieste.enabled = false
end event

event pcd_view;call super::pcd_view;cb_tipi_richieste.enabled = true
end event

