﻿$PBExportHeader$w_risorse_esterne_ricerca.srw
$PBExportComments$Window di ricerca delle risorse_esterne
forward
global type w_risorse_esterne_ricerca from w_cs_xx_principale
end type
type cb_ok from commandbutton within w_risorse_esterne_ricerca
end type
type cb_annulla from uo_cb_close within w_risorse_esterne_ricerca
end type
type dw_risorse_esterne from uo_cs_xx_dw within w_risorse_esterne_ricerca
end type
end forward

global type w_risorse_esterne_ricerca from w_cs_xx_principale
integer width = 2409
integer height = 856
string title = "Risorse Esterne"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
cb_ok cb_ok
cb_annulla cb_annulla
dw_risorse_esterne dw_risorse_esterne
end type
global w_risorse_esterne_ricerca w_risorse_esterne_ricerca

event pc_setwindow;call super::pc_setwindow;dw_risorse_esterne.set_dw_key("cod_azienda")
dw_risorse_esterne.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
iuo_dw_main = dw_risorse_esterne
dw_risorse_esterne.setrowfocusindicator(hand!)
end event

on w_risorse_esterne_ricerca.create
int iCurrent
call super::create
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.dw_risorse_esterne=create dw_risorse_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ok
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.dw_risorse_esterne
end on

on w_risorse_esterne_ricerca.destroy
call super::destroy
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.dw_risorse_esterne)
end on

type cb_ok from commandbutton within w_risorse_esterne_ricerca
integer x = 1989
integer y = 680
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;if dw_risorse_esterne.getrow() > 0 then
	
	s_cs_xx.parametri.parametro_s_1 = dw_risorse_esterne.getitemstring( dw_risorse_esterne.getrow(), "cod_cat_risorse_esterne")
	
	s_cs_xx.parametri.parametro_s_2 = dw_risorse_esterne.getitemstring( dw_risorse_esterne.getrow(), "cod_risorsa_esterna")	

	close(w_risorse_esterne_ricerca)
end if
end event

type cb_annulla from uo_cb_close within w_risorse_esterne_ricerca
integer x = 1600
integer y = 680
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type dw_risorse_esterne from uo_cs_xx_dw within w_risorse_esterne_ricerca
integer x = 23
integer y = 20
integer width = 2331
integer height = 640
integer taborder = 20
string dataobject = "d_risorse_esterne_ricerca"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

