﻿$PBExportHeader$w_tab_cat_risorse_esterne.srw
$PBExportComments$Window tab_cat_risorse_esterne
forward
global type w_tab_cat_risorse_esterne from w_cs_xx_principale
end type
type st_1 from statictext within w_tab_cat_risorse_esterne
end type
type dw_tab_cat_risorse_esterne_lista from uo_cs_xx_dw within w_tab_cat_risorse_esterne
end type
end forward

global type w_tab_cat_risorse_esterne from w_cs_xx_principale
integer width = 2665
integer height = 1464
string title = "Categorie Risorse Esterne"
st_1 st_1
dw_tab_cat_risorse_esterne_lista dw_tab_cat_risorse_esterne_lista
end type
global w_tab_cat_risorse_esterne w_tab_cat_risorse_esterne

event pc_setwindow;call super::pc_setwindow;dw_tab_cat_risorse_esterne_lista.set_dw_key("cod_azienda")
dw_tab_cat_risorse_esterne_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_cat_risorse_esterne_lista.ib_proteggi_chiavi = false
iuo_dw_main = dw_tab_cat_risorse_esterne_lista
end event

on w_tab_cat_risorse_esterne.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_tab_cat_risorse_esterne_lista=create dw_tab_cat_risorse_esterne_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_tab_cat_risorse_esterne_lista
end on

on w_tab_cat_risorse_esterne.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_tab_cat_risorse_esterne_lista)
end on

event resize;call super::resize;st_1.move(20, newheight - st_1.height - 20)

dw_tab_cat_risorse_esterne_lista.resize(newwidth - 40, st_1.y - 40)
end event

type st_1 from statictext within w_tab_cat_risorse_esterne
integer x = 20
integer y = 1264
integer width = 2560
integer height = 64
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 134217728
string text = "Il colore deve essere inserito in formato esadecimale. Ad esempio #ffcc00"
boolean focusrectangle = false
end type

type dw_tab_cat_risorse_esterne_lista from uo_cs_xx_dw within w_tab_cat_risorse_esterne
integer x = 20
integer y = 16
integer width = 2578
integer height = 992
integer taborder = 10
string dataobject = "d_tab_cat_risorse_esterne_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

event doubleclicked;call super::doubleclicked;//if row < 1 then return
//
//if s_cs_xx.admin then
//
//	str_des_multilingua lstr_des_multilingua
//	
//	lstr_des_multilingua.nome_tabella = "tab_cat_risorse_esterne"
//	lstr_des_multilingua.descrizione_origine = getitemstring(row,"des_cat_risorse_esterne")
//	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_cat_risorse_esterne")
//	setnull(lstr_des_multilingua.chiave_str_2)
//	
//	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
//	
//	openwithparm(w_des_multilingua, lstr_des_multilingua)
//
//end if
end event

