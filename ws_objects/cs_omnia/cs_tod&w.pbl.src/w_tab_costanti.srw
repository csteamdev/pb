﻿$PBExportHeader$w_tab_costanti.srw
$PBExportComments$Window tab_costanti
forward
global type w_tab_costanti from w_cs_xx_principale
end type
type dw_tab_costanti_lista from uo_cs_xx_dw within w_tab_costanti
end type
type dw_tab_costanti_dett from uo_cs_xx_dw within w_tab_costanti
end type
end forward

global type w_tab_costanti from w_cs_xx_principale
int Width=2958
int Height=1161
boolean TitleBar=true
string Title="Costanti Statistiche"
dw_tab_costanti_lista dw_tab_costanti_lista
dw_tab_costanti_dett dw_tab_costanti_dett
end type
global w_tab_costanti w_tab_costanti

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tab_costanti_lista.set_dw_key("strumento_test")
dw_tab_costanti_lista.set_dw_key("valore_rif")
dw_tab_costanti_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_costanti_dett.set_dw_options(sqlca,dw_tab_costanti_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_costanti_lista
end on

on w_tab_costanti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_costanti_lista=create dw_tab_costanti_lista
this.dw_tab_costanti_dett=create dw_tab_costanti_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_costanti_lista
this.Control[iCurrent+2]=dw_tab_costanti_dett
end on

on w_tab_costanti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_costanti_lista)
destroy(this.dw_tab_costanti_dett)
end on

type dw_tab_costanti_lista from uo_cs_xx_dw within w_tab_costanti
int X=23
int Y=21
int Width=1121
int Height=1021
int TabOrder=10
string DataObject="d_tab_costanti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_tab_costanti_dett from uo_cs_xx_dw within w_tab_costanti
int X=1166
int Y=21
int Width=1738
int Height=1021
int TabOrder=20
string DataObject="d_tab_costanti_dett"
BorderStyle BorderStyle=StyleRaised!
end type

