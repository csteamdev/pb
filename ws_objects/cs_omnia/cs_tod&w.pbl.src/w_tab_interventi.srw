﻿$PBExportHeader$w_tab_interventi.srw
$PBExportComments$Finestra Codifica Tipologie Interventi
forward
global type w_tab_interventi from w_cs_xx_principale
end type
type dw_tab_interventi_lista from uo_cs_xx_dw within w_tab_interventi
end type
type dw_tab_interventi_1 from uo_cs_xx_dw within w_tab_interventi
end type
type dw_tab_interventi_2 from uo_cs_xx_dw within w_tab_interventi
end type
type dw_folder from u_folder within w_tab_interventi
end type
end forward

global type w_tab_interventi from w_cs_xx_principale
integer width = 2414
integer height = 1844
string title = "Tipologie Interventi"
dw_tab_interventi_lista dw_tab_interventi_lista
dw_tab_interventi_1 dw_tab_interventi_1
dw_tab_interventi_2 dw_tab_interventi_2
dw_folder dw_folder
end type
global w_tab_interventi w_tab_interventi

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_interventi_1,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')"  )

end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_tab_interventi_1
dw_folder.fu_AssignTab(1, "&Anagrafici", l_Objects[])
l_objects[1] = dw_tab_interventi_2
dw_folder.fu_AssignTab(2, "&Normative", l_Objects[])

dw_folder.fu_FolderCreate(2,4)

dw_tab_interventi_lista.set_dw_key("cod_azienda")
dw_tab_interventi_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_interventi_1.set_dw_options(sqlca,dw_tab_interventi_lista,c_sharedata+c_scrollparent,c_default)
dw_tab_interventi_2.set_dw_options(sqlca,dw_tab_interventi_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_tab_interventi_lista

dw_folder.fu_SelectTab(1)

end on

on w_tab_interventi.create
int iCurrent
call super::create
this.dw_tab_interventi_lista=create dw_tab_interventi_lista
this.dw_tab_interventi_1=create dw_tab_interventi_1
this.dw_tab_interventi_2=create dw_tab_interventi_2
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_interventi_lista
this.Control[iCurrent+2]=this.dw_tab_interventi_1
this.Control[iCurrent+3]=this.dw_tab_interventi_2
this.Control[iCurrent+4]=this.dw_folder
end on

on w_tab_interventi.destroy
call super::destroy
destroy(this.dw_tab_interventi_lista)
destroy(this.dw_tab_interventi_1)
destroy(this.dw_tab_interventi_2)
destroy(this.dw_folder)
end on

type dw_tab_interventi_lista from uo_cs_xx_dw within w_tab_interventi
integer x = 23
integer y = 20
integer width = 2331
integer height = 460
integer taborder = 20
string dataobject = "d_tab_interventi_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_tab_interventi_1 from uo_cs_xx_dw within w_tab_interventi
integer x = 69
integer y = 600
integer width = 2240
integer height = 1100
integer taborder = 40
string dataobject = "d_tab_interventi_1"
boolean border = false
end type

on pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol;long ll_num_reg_lista, ll_versione, ll_edizione


choose case i_colname

case "num_reg_lista"
   ll_num_reg_lista = long(i_coltext)
   if ll_num_reg_lista > 0 then
      if f_ultima_versione_edizione(ll_num_reg_lista, ll_versione, ll_edizione) = 0 then
         setitem(getrow(), "num_versione", ll_versione)
         setitem(getrow(), "num_edizione", ll_edizione)
      end if
   end if

end choose
   
end on

type dw_tab_interventi_2 from uo_cs_xx_dw within w_tab_interventi
integer x = 69
integer y = 620
integer width = 2240
integer height = 1080
integer taborder = 50
string dataobject = "d_tab_interventi_2"
boolean border = false
end type

type dw_folder from u_folder within w_tab_interventi
integer x = 23
integer y = 500
integer width = 2331
integer height = 1220
integer taborder = 30
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "&Anagrafici"
      SetFocus(dw_tab_interventi_1)
   CASE "&Normative"
      SetFocus(dw_tab_interventi_2)
END CHOOSE

end on

