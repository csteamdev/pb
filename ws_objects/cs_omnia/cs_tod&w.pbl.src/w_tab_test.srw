﻿$PBExportHeader$w_tab_test.srw
$PBExportComments$Window tab_test
forward
global type w_tab_test from w_cs_xx_principale
end type
type cb_doc_anomalia from commandbutton within w_tab_test
end type
type dw_tab_test_lista from uo_cs_xx_dw within w_tab_test
end type
type dw_tab_test_dettaglio from uo_cs_xx_dw within w_tab_test
end type
end forward

global type w_tab_test from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 1906
integer height = 1652
string title = "Test"
cb_doc_anomalia cb_doc_anomalia
dw_tab_test_lista dw_tab_test_lista
dw_tab_test_dettaglio dw_tab_test_dettaglio
end type
global w_tab_test w_tab_test

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_test_dettaglio,"cod_tipo_test",sqlca,&
                 "tab_tipo_test","cod_tipo_test","des_tipo_test","")
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tab_test_lista.set_dw_key("cod_test")
dw_tab_test_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_test_dettaglio.set_dw_options(sqlca,dw_tab_test_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_test_lista
end on

on w_tab_test.create
int iCurrent
call super::create
this.cb_doc_anomalia=create cb_doc_anomalia
this.dw_tab_test_lista=create dw_tab_test_lista
this.dw_tab_test_dettaglio=create dw_tab_test_dettaglio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_doc_anomalia
this.Control[iCurrent+2]=this.dw_tab_test_lista
this.Control[iCurrent+3]=this.dw_tab_test_dettaglio
end on

on w_tab_test.destroy
call super::destroy
destroy(this.cb_doc_anomalia)
destroy(this.dw_tab_test_lista)
destroy(this.dw_tab_test_dettaglio)
end on

type cb_doc_anomalia from commandbutton within w_tab_test
integer x = 1486
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "De&ttagli"
end type

event clicked;string ls_cod_test

if dw_tab_test_lista.rowcount() > 0 then

	ls_cod_test = dw_tab_test_lista.getitemstring( dw_tab_test_lista.getrow(), "cod_test")
	
	if not isnull(ls_cod_test) and ls_cod_test <> "" then
		
		s_cs_xx.parametri.parametro_s_1 = ls_cod_test
		
		window_open_parm( w_tab_test_valori, -1, dw_tab_test_lista)
		
	end if
else
	
	g_mb.messagebox( "OMNIA", "Attenzione: selezionare una riga per vedere i dettagli!")
	
end if
end event

type dw_tab_test_lista from uo_cs_xx_dw within w_tab_test
integer x = 23
integer y = 20
integer width = 1829
integer height = 500
integer taborder = 10
string dataobject = "d_tab_test_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_tab_test_dettaglio from uo_cs_xx_dw within w_tab_test
integer x = 23
integer y = 540
integer width = 1829
integer height = 900
integer taborder = 20
string dataobject = "d_tab_test_dettaglio"
borderstyle borderstyle = styleraised!
end type

