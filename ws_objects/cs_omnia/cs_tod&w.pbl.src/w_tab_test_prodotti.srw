﻿$PBExportHeader$w_tab_test_prodotti.srw
$PBExportComments$Window tab_test_prodotti
forward
global type w_tab_test_prodotti from w_cs_xx_principale
end type
type dw_tab_test_prodotti_lista from uo_cs_xx_dw within w_tab_test_prodotti
end type
type cb_1 from cb_prod_ricerca within w_tab_test_prodotti
end type
type dw_tab_test_prodotti_dettaglio from uo_cs_xx_dw within w_tab_test_prodotti
end type
end forward

global type w_tab_test_prodotti from w_cs_xx_principale
int Width=2140
int Height=2045
boolean TitleBar=true
string Title="Test Prodotti"
dw_tab_test_prodotti_lista dw_tab_test_prodotti_lista
cb_1 cb_1
dw_tab_test_prodotti_dettaglio dw_tab_test_prodotti_dettaglio
end type
global w_tab_test_prodotti w_tab_test_prodotti

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_test",sqlca,&
                 "tab_test","cod_test","des_test","")

f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_tipo_test",sqlca,&
                 "tab_tipo_test","cod_tipo_test","des_tipo_test","")

f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura",&
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_aql",sqlca,&
                 "tes_aql","cod_aql","des_aql",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tab_test_prodotti_lista.set_dw_key("cod_azienda")
dw_tab_test_prodotti_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_test_prodotti_dettaglio.set_dw_options(sqlca,dw_tab_test_prodotti_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_test_prodotti_lista

cb_1.enabled = false

end on

on w_tab_test_prodotti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_test_prodotti_lista=create dw_tab_test_prodotti_lista
this.cb_1=create cb_1
this.dw_tab_test_prodotti_dettaglio=create dw_tab_test_prodotti_dettaglio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_test_prodotti_lista
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=dw_tab_test_prodotti_dettaglio
end on

on w_tab_test_prodotti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_test_prodotti_lista)
destroy(this.cb_1)
destroy(this.dw_tab_test_prodotti_dettaglio)
end on

type dw_tab_test_prodotti_lista from uo_cs_xx_dw within w_tab_test_prodotti
int X=23
int Y=21
int Width=2058
int Height=501
int TabOrder=20
string DataObject="d_tab_test_prodotti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
	cb_1.enabled = false
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;if i_extendmode then
	cb_1.enabled = true
end if
end on

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
	cb_1.enabled = false
end if
end on

type cb_1 from cb_prod_ricerca within w_tab_test_prodotti
int X=1989
int Y=561
int TabOrder=10
end type

on getfocus;call cb_prod_ricerca::getfocus;dw_tab_test_prodotti_dettaglio.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end on

type dw_tab_test_prodotti_dettaglio from uo_cs_xx_dw within w_tab_test_prodotti
int X=23
int Y=541
int Width=2058
int Height=1381
int TabOrder=30
string DataObject="d_tab_test_prodotti_dettaglio"
BorderStyle BorderStyle=StyleRaised!
end type

on itemchanged;call uo_cs_xx_dw::itemchanged;if i_extendmode then

	string ls_cod_tipo_test
	string ls_cod_test
	
	choose case i_colname
	
		case "cod_test"
			select cod_tipo_test into:ls_cod_tipo_test from tab_test where cod_test =:i_coltext;
			SetItem(getrow(), "cod_tipo_test", ls_cod_tipo_test)
		

		case "cod_cat_attrezzature"
			f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_attrezzatura",sqlca,&
   	              "anag_attrezzature","cod_attrezzatura","descrizione",&
      	           "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_attrezzature ='" + i_coltext + "'")

	
	end choose

end if
end on

