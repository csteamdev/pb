﻿$PBExportHeader$w_tab_test_prodotti_tv.srw
$PBExportComments$Window tab_test_prodotti
forward
global type w_tab_test_prodotti_tv from w_cs_xx_principale
end type
type dw_folder_search from u_folder within w_tab_test_prodotti_tv
end type
type dw_tab_test_prodotti_dettaglio from uo_cs_xx_dw within w_tab_test_prodotti_tv
end type
type tv_1 from treeview within w_tab_test_prodotti_tv
end type
type cb_refresh from commandbutton within w_tab_test_prodotti_tv
end type
type dw_filtro from u_dw_search within w_tab_test_prodotti_tv
end type
type cb_cerca from commandbutton within w_tab_test_prodotti_tv
end type
type cb_reset from commandbutton within w_tab_test_prodotti_tv
end type
type cb_nuovo from commandbutton within w_tab_test_prodotti_tv
end type
end forward

global type w_tab_test_prodotti_tv from w_cs_xx_principale
integer width = 4233
integer height = 2388
string title = "Test Prodotti"
boolean resizable = false
dw_folder_search dw_folder_search
dw_tab_test_prodotti_dettaglio dw_tab_test_prodotti_dettaglio
tv_1 tv_1
cb_refresh cb_refresh
dw_filtro dw_filtro
cb_cerca cb_cerca
cb_reset cb_reset
cb_nuovo cb_nuovo
end type
global w_tab_test_prodotti_tv w_tab_test_prodotti_tv

type variables
datastore ids_prodotti, ids_test
treeviewitem tvi_campo
long         il_handle
string is_cod_prodotto, is_cod_test, is_tipo
datetime idt_validita
boolean ib_delete = false
boolean ib_new = false
boolean ib_modify = false
end variables

forward prototypes
public subroutine wf_cancella_treeview ()
public subroutine wf_imposta_tv ()
public function long wf_inserisci_prodotti (datastore fds_prodotti)
public function integer wf_inserisci_i_test (long fl_handle, string fs_cod_prodotto, datetime fd_data_validita)
end prototypes

public subroutine wf_cancella_treeview ();long tvi_hdl = 0

do 
	tvi_hdl = tv_1.finditem(roottreeitem!,0)
	if tvi_hdl <> -1 then
		tv_1.deleteitem(tvi_hdl)
	end if
loop while tvi_hdl <> -1

return
end subroutine

public subroutine wf_imposta_tv ();string ls_cod_prodotto, ls_cod_test, ls_cod_attrezzatura, ls_cod_cat_attrezzatura
datetime ldt_da_val, ldt_a_val
string ls_sql, ls_errore, ls_sintassi
long ll_righe

tvi_campo.expanded = false
tvi_campo.selected = false
tvi_campo.children = false	//altrimenti mostrava il segno di + anche accanto agli elementi senza sottoelementi

setpointer(HourGlass!)

tv_1.setredraw(false)

dw_filtro.accepttext()

ls_cod_prodotto = dw_filtro.getitemstring( 1, "rs_cod_prodotto")
ls_cod_test = dw_filtro.getitemstring( 1, "rs_cod_test")
ls_cod_attrezzatura = dw_filtro.getitemstring( 1, "rs_cod_attrezzatura")
ls_cod_cat_attrezzatura = dw_filtro.getitemstring( 1, "rs_cod_cat_attrezzature")
ldt_da_val = dw_filtro.getitemdatetime( 1, "rdt_data_validita_dal")
ldt_a_val = dw_filtro.getitemdatetime( 1, "rdt_data_validita_al")

ls_sql = "SELECT DISTINCT " +&
				"tab_test_prodotti.cod_prodotto" + &
				",tab_test_prodotti.data_validita" + &
				",anag_prodotti.des_prodotto " +&
        		"FROM tab_test_prodotti " + &
        		"JOIN anag_prodotti ON anag_prodotti.cod_prodotto=tab_test_prodotti.cod_prodotto " + &
        	"  WHERE tab_test_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' "
			  
if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
	ls_sql += " AND tab_test_prodotti.cod_prodotto = '"+ls_cod_prodotto+"' "
end if
if not isnull(ls_cod_test) and ls_cod_test<>"" then
	ls_sql += " AND tab_test_prodotti.cod_test = '"+ls_cod_test+"' "
end if
if not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura<>"" then
	ls_sql += " AND tab_test_prodotti.cod_attrezzatura = '"+ls_cod_attrezzatura+"' "
end if
if not isnull(ls_cod_cat_attrezzatura) and ls_cod_cat_attrezzatura<>"" then
	ls_sql += " AND tab_test_prodotti.cod_cat_attrezzature = '"+ls_cod_cat_attrezzatura+"' "
end if
if not isnull(ldt_da_val) then
	ls_sql += " AND tab_test_prodotti.data_validita >= '" + string( ldt_da_val, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_a_val) then
	ls_sql += " AND tab_test_prodotti.data_validita <= '" + string( ldt_a_val, s_cs_xx.db_funzioni.formato_data) + "' "
end if

ls_sql += " ORDER BY anag_prodotti.des_prodotto "

ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)

IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "SEP", "Errore durante la creazione della sintassi del datastore:" + ls_errore)
	g_mb.messagebox( "SEP", "Sintassi SQL: " + ls_sql)
	tv_1.setredraw(true)
	setpointer(Arrow!)
   RETURN
END IF

ids_prodotti = create datastore

ids_prodotti.Create( ls_sintassi, ls_errore)
IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "SEP", "Errore durante la creazione del datastore:" + ls_errore)
	destroy ids_prodotti;
	tv_1.setredraw(true)
	setpointer(Arrow!)
   RETURN
END IF

ids_prodotti.settransobject( sqlca)

ll_righe = ids_prodotti.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "SEP", "Errore durante la retrieve delle righe dei prodotti:" + sqlca.sqlerrtext)
	destroy ids_prodotti;
	tv_1.setredraw(true)
	setpointer(Arrow!)
	return 
end if

wf_inserisci_prodotti(ids_prodotti)

tv_1.setredraw(true)

setpointer(arrow!)
return
end subroutine

public function long wf_inserisci_prodotti (datastore fds_prodotti);long ll_i, ll_figli, ll_risposta
string ls_cod_prodotto, ls_des_prodotto, ls_label
string ls_cod_test, ls_des_test
datetime ldt_validita
s_test_prodotti ls_test_prodotti

//string  ls_cod_prodotto, ls_des_prodotto, ls_flag_tipo_avanzamento, ls_str
//long	  ll_anno_commessa, ll_num_commessa, ll_livello, ll_i, ll_risposta, ll_figli 	  
//ws_record lstr_record

//ll_livello = 0

for ll_i = 1 to fds_prodotti.rowcount()
	ls_cod_prodotto = fds_prodotti.getitemstring( ll_i, 1) //"tab_test_prodotti_cod_prodotto")
	ls_des_prodotto = fds_prodotti.getitemstring( ll_i, 3) //"anag_prodotti_des_prodotto")
	ldt_validita =  fds_prodotti.getitemdatetime( ll_i, 2) //"tab_test_prodotti_data_validita")

	ls_test_prodotti.s_cod_prodotto = ls_cod_prodotto
	ls_test_prodotti.s_cod_test = ""
	ls_test_prodotti.dt_validita = ldt_validita
	ls_test_prodotti.s_tipo = "P"

	tvi_campo.data = ls_test_prodotti
	ls_label = ls_des_prodotto + " - " + string(ldt_validita, "dd/mm/yyyy")
	
	tvi_campo.label = ls_label
		
	tvi_campo.pictureindex = 1
	tvi_campo.selectedpictureindex = 1
	tvi_campo.overlaypictureindex = 1

	setnull(ll_figli)

	SELECT count(cod_test)
	INTO   :ll_figli
	FROM   tab_test_prodotti
	WHERE  cod_azienda = :s_cs_xx.cod_azienda AND cod_prodotto = :ls_cod_prodotto;

	if not isnull(ll_figli) and ll_figli > 0 then
		tvi_campo.children = true
		tvi_campo.selected = false
	else	
		tvi_campo.children = false
		tvi_campo.selected = false
	end if
	
	tvi_campo.bold = false
	ll_risposta = tv_1.insertitemlast(0, tvi_campo)
	if not isnull(ll_figli) and ll_figli > 0 then
		//chiama la funzione per recuperare i figli, cioè i test
		wf_inserisci_i_test(ll_risposta, ls_cod_prodotto, ldt_validita)
	end if
	
next

return 0
end function

public function integer wf_inserisci_i_test (long fl_handle, string fs_cod_prodotto, datetime fd_data_validita);//long      ll_livello, ll_righe, ll_i, ll_prog_riga, ll_risposta
//string    ls_sql, ls_sintassi, ls_errore, ls_str
string ls_errore, ls_sql, ls_sintassi, ls_cod_test, ls_des_test, ls_label
string ls_cod_test_search, ls_cod_attrezzatura_search, ls_cod_cat_attrezzatura_search
long ll_righe, ll_risposta, ll_i
datetime ldt_validita, ldt_da_val_search, ldt_a_val_search
datastore lds_test
s_test_prodotti ls_test_prodotti

ls_cod_test_search = dw_filtro.getitemstring( 1, "rs_cod_test")
ls_cod_attrezzatura_search = dw_filtro.getitemstring( 1, "rs_cod_attrezzatura")
ls_cod_cat_attrezzatura_search = dw_filtro.getitemstring( 1, "rs_cod_cat_attrezzature")
ldt_da_val_search = dw_filtro.getitemdatetime( 1, "rdt_data_validita_dal")
ldt_a_val_search = dw_filtro.getitemdatetime( 1, "rdt_data_validita_al")

ls_sql = "SELECT " + &
				"cod_test" + &
				",data_validita" + &
         			",des_test" + &
         		" " + &
    		"FROM tab_test_prodotti " + &
   		" WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
        			 "cod_prodotto = '" + fs_cod_prodotto + "' AND " + &
				"data_validita = '" + string( fd_data_validita, s_cs_xx.db_funzioni.formato_data) + "' "
						
if not isnull(ls_cod_test_search) and ls_cod_test_search<>"" then
	ls_sql += " AND tab_test_prodotti.cod_test = '"+ls_cod_test_search+"' "
end if
if not isnull(ls_cod_attrezzatura_search) and ls_cod_attrezzatura_search<>"" then
	ls_sql += " AND tab_test_prodotti.cod_attrezzatura = '"+ls_cod_attrezzatura_search+"' "
end if
if not isnull(ls_cod_cat_attrezzatura_search) and ls_cod_cat_attrezzatura_search<>"" then
	ls_sql += " AND tab_test_prodotti.cod_cat_attrezzature = '"+ls_cod_cat_attrezzatura_search+"' "
end if
if not isnull(ldt_da_val_search) then
	ls_sql += " AND tab_test_prodotti.data_validita >= '" + string( ldt_da_val_search, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_a_val_search) then
	ls_sql += " AND tab_test_prodotti.data_validita <= '" + string( ldt_a_val_search, s_cs_xx.db_funzioni.formato_data) + "' "
end if
						
ls_sql +="ORDER BY tab_test_prodotti.des_test "
			
ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)

IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la creazione della sintassi del datastore:" + ls_errore)
	g_mb.messagebox( "OMNIA", "Sintassi SQL: " + ls_sql)
	tv_1.setredraw(true)
	setpointer(Arrow!)
   RETURN -1
END IF

lds_test = create datastore

lds_test.Create( ls_sintassi, ls_errore)
IF Len(ls_errore) > 0 THEN
   g_mb.messagebox( "OMNIA", "Errore durante la creazione del datastore:" + ls_errore)
	destroy lds_test;
	tv_1.setredraw(true)
	setpointer(Arrow!)
   RETURN -1
END IF

lds_test.settransobject( sqlca)

ll_righe = lds_test.retrieve()
if ll_righe < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la retrieve delle righe dei Test:" + sqlca.sqlerrtext)
	destroy lds_test;
	tv_1.setredraw(true)
	setpointer(Arrow!)
	return  -1
end if

for ll_i = 1 to lds_test.rowcount()
	ls_cod_test = lds_test.getitemstring( ll_i, "cod_test")
	ls_des_test = lds_test.getitemstring( ll_i, "des_test")
	ldt_validita = lds_test.getitemdatetime( ll_i, "data_validita")
	
	ls_test_prodotti.s_cod_prodotto = fs_cod_prodotto
	ls_test_prodotti.s_cod_test = ls_cod_test
	ls_test_prodotti.dt_validita = ldt_validita
	ls_test_prodotti.s_tipo = "T"
	
	tvi_campo.data = ls_test_prodotti
	
	ls_label = ls_des_test
	
	tvi_campo.label = ls_label
		
	tvi_campo.pictureindex = 2		
	tvi_campo.selectedpictureindex = 2		
	tvi_campo.overlaypictureindex = 2		

	tvi_campo.children = false
	tvi_campo.selected = false
	//tvi_campo.bold = false
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	
next

return 0
end function

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_test",sqlca,&
                 "tab_test","cod_test","des_test","")

f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_tipo_test",sqlca,&
                 "tab_tipo_test","cod_tipo_test","des_tipo_test","")

f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_attrezzatura",sqlca,&
//                 "anag_attrezzature","cod_attrezzatura","descrizione",&
//                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura",&
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_aql",sqlca,&
                 "tes_aql","cod_aql","des_aql",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
//-------------------------------------------
//datawindow di ricerca
f_PO_LoadDDDW_DW(dw_filtro,"rs_cod_test",sqlca,&
                 "tab_test","cod_test","des_test","")

f_PO_LoadDDDW_DW(dw_filtro,"rs_cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDDW_DW(dw_filtro,"rs_cod_attrezzatura",sqlca,&
//                 "anag_attrezzature","cod_attrezzatura","descrizione",&
//                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;datetime ldt_oggi
windowobject lw_oggetti[]

dw_tab_test_prodotti_dettaglio.set_dw_key("cod_azienda")
dw_tab_test_prodotti_dettaglio.set_dw_key("cod_prodotto")
dw_tab_test_prodotti_dettaglio.set_dw_key("cod_test")
dw_tab_test_prodotti_dettaglio.set_dw_key("data_validita")

dw_tab_test_prodotti_dettaglio.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_noresizedw)
//dw_tab_test_prodotti_dettaglio.set_dw_options(sqlca,pcca.null_object,c_default,c_noresizedw)

iuo_dw_main = dw_tab_test_prodotti_dettaglio


//  ------------------------  imposto folder di ricerca ---------------------------

dw_filtro.insertrow(0)

lw_oggetti[1] = tv_1
lw_oggetti[2] = cb_nuovo
lw_oggetti[3] = cb_refresh
dw_folder_search.fu_assigntab(2, "Test/Prodotti", lw_oggetti[])
lw_oggetti[1] = dw_filtro
lw_oggetti[2] = cb_cerca
//lw_oggetti[3] = cb_prodotti_ricerca
lw_oggetti[3] = cb_reset
//lw_oggetti[4] = cb_attrezzature_ricerca

dw_folder_search.fu_assigntab(1, "Filtro", lw_oggetti[])
dw_folder_search.fu_foldercreate(2, 2)
dw_folder_search.fu_selecttab(1)

ldt_oggi = datetime(today(),time("00:00:00"))
dw_filtro.setitem(1,"rdt_data_validita_al",ldt_oggi)

// -----------------------------  impostazioni relative al treeview --------------------------
tv_1.deletepictures()
tv_1.PictureHeight = 16
tv_1.PictureWidth = 16
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "cartella_chiudi.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "test.bmp")

//cb_prodotto_sel.enabled = false
//cb_2.enabled = false
end event

on w_tab_test_prodotti_tv.create
int iCurrent
call super::create
this.dw_folder_search=create dw_folder_search
this.dw_tab_test_prodotti_dettaglio=create dw_tab_test_prodotti_dettaglio
this.tv_1=create tv_1
this.cb_refresh=create cb_refresh
this.dw_filtro=create dw_filtro
this.cb_cerca=create cb_cerca
this.cb_reset=create cb_reset
this.cb_nuovo=create cb_nuovo
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_folder_search
this.Control[iCurrent+2]=this.dw_tab_test_prodotti_dettaglio
this.Control[iCurrent+3]=this.tv_1
this.Control[iCurrent+4]=this.cb_refresh
this.Control[iCurrent+5]=this.dw_filtro
this.Control[iCurrent+6]=this.cb_cerca
this.Control[iCurrent+7]=this.cb_reset
this.Control[iCurrent+8]=this.cb_nuovo
end on

on w_tab_test_prodotti_tv.destroy
call super::destroy
destroy(this.dw_folder_search)
destroy(this.dw_tab_test_prodotti_dettaglio)
destroy(this.tv_1)
destroy(this.cb_refresh)
destroy(this.dw_filtro)
destroy(this.cb_cerca)
destroy(this.cb_reset)
destroy(this.cb_nuovo)
end on

type dw_folder_search from u_folder within w_tab_test_prodotti_tv
integer x = 46
integer width = 2057
integer height = 2280
integer taborder = 20
end type

type dw_tab_test_prodotti_dettaglio from uo_cs_xx_dw within w_tab_test_prodotti_tv
integer x = 2103
integer y = 20
integer width = 2103
integer height = 1760
integer taborder = 30
string dataobject = "d_tab_test_prodotti_tv"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then

	string ls_cod_tipo_test
	string ls_cod_test
	
	choose case i_colname
	
		case "cod_test"
			select cod_tipo_test into:ls_cod_tipo_test from tab_test where cod_test =:i_coltext;
			SetItem(getrow(), "cod_tipo_test", ls_cod_tipo_test)
		

		case "cod_cat_attrezzature"
			f_PO_LoadDDDW_DW(dw_tab_test_prodotti_dettaglio,"cod_attrezzatura",sqlca,&
   	              "anag_attrezzature","cod_attrezzatura","descrizione",&
      	           "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_attrezzature ='" + i_coltext + "'")

		case "flag_tipologia_durata"
			if data = "N" then
				this.setitem(getrow(),"valore_durata",0)
				this.object.valore_durata.Background.color = 12632256 //silver
			else
				this.object.valore_durata.Background.color = 16777215 //white
			end if			
	
	end choose

end if
end event

event pcd_retrieve;call super::pcd_retrieve;long  l_Error
s_test_prodotti ls_test_prodotti

//ws_record lstr_record
if il_handle<1 then
	return
end if
tv_1.getitem(il_handle, tvi_campo)
ls_test_prodotti = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda, ls_test_prodotti.s_cod_prodotto, ls_test_prodotti.s_cod_test, ls_test_prodotti.dt_validita)

ib_delete = false
ib_new = false
ib_modify = false

if l_Error < 0 then
	PCCA.Error = c_Fatal
end if
end event

event updatestart;call super::updatestart;long ll_row, ll_handle
string ls_cod_prodotto, ls_cod_test
datetime ldt_validita
TreeViewItem  ltvi_Item
s_test_prodotti ls_test_prodotti

ll_row = this.GetRow()
if (ib_new or ib_delete or ib_modify) and ll_row > 0 then
	ls_cod_prodotto = this.GetItemString (ll_row,"cod_prodotto")
	ls_cod_test = this.GetItemString (ll_row,"cod_test")
	ldt_validita = this.GetItemDateTime(ll_row, "data_validita")

	 if isnull(this.getitemstring(ll_row, "cod_azienda")) and not ib_delete then
     	this.setitem(ll_row, "cod_azienda", s_cs_xx.cod_azienda)
   	end if
	
	if ib_new then
		//ricarica l'albero
		cb_cerca.postevent(clicked!)
		ib_new = false
	elseif ib_delete then		
//		ll_handle = tvi_campo.itemhandle
//		if ll_handle> 0 then
//				tv_1.DeleteItem(ll_handle)
//				this.triggerevent("pcd_save")
//		end if
		//ricarica l'albero
		cb_cerca.postevent(clicked!)
		ib_delete = false
	else
		ib_modify = false
	end if
end if
end event

event pcd_delete;call super::pcd_delete;ib_delete = true

if PCCA.Error = c_Fatal then
	ib_delete = false
	return
else
	this.triggerevent("pcd_save")
end if




end event

event pcd_new;call super::pcd_new;treeviewitem ltv_item
s_test_prodotti ls_test_prodotti
long				ll_ret, ll_handle

ib_new = true
dw_tab_test_prodotti_dettaglio.visible = true
//cb_prodotto_sel.visible = true
//cb_prodotto_sel.enabled = true
//cb_2.enabled = true
//cb_2.visible = true
dw_tab_test_prodotti_dettaglio.object.b_ricerca_prodotto.enabled = true
dw_tab_test_prodotti_dettaglio.object.b_ricerca_att.enabled = true
dw_tab_test_prodotti_dettaglio.object.b_ricerca_prodotto.visible = true
dw_tab_test_prodotti_dettaglio.object.b_ricerca_att.visible = true

ltv_item = tvi_campo
ll_handle = tvi_campo.itemhandle

if ll_handle > 0 then
	ls_test_prodotti = tvi_campo.data
	is_cod_prodotto = ls_test_prodotti.s_cod_prodotto
	is_cod_test = ls_test_prodotti.s_cod_test
	idt_validita = ls_test_prodotti.dt_validita
	is_tipo = ls_test_prodotti.s_tipo
	il_handle = tvi_campo.itemhandle

	this.SetItem(this.getRow(), "cod_prodotto",is_cod_prodotto)
	this.SetItem(this.getRow(), "data_validita",idt_validita)
//	cb_2.visible = true
end if
end event

event pcd_modify;call super::pcd_modify;ib_modify = true
dw_tab_test_prodotti_dettaglio.object.b_ricerca_prodotto.enabled = true
dw_tab_test_prodotti_dettaglio.object.b_ricerca_att.enabled = true
end event

event pcd_view;call super::pcd_view;//cb_prodotto_sel.enabled = false
//cb_2.enabled = false
dw_tab_test_prodotti_dettaglio.object.b_ricerca_prodotto.enabled = false
dw_tab_test_prodotti_dettaglio.object.b_ricerca_att.enabled = false
ib_delete = false
ib_new = false
ib_modify = false
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_row
string ls_cod_prodotto, ls_cod_test
datetime ldt_validita

ll_row = this.GetRow()

ls_cod_prodotto = this.GetItemString (ll_row,"cod_prodotto")
ls_cod_test = this.GetItemString (ll_row,"cod_test")
ldt_validita = this.GetItemDateTime(ll_row, "data_validita")

if not ib_delete then
	if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
		g_mb.messagebox( "OMNIA", "Il Prodotto è obbligatorio!")
		pcca.error = c_fatal
		return
	end if
	if isnull(ls_cod_test) or ls_cod_test = "" then
		g_mb.messagebox( "OMNIA", "Il Test è obbligatorio!")
		pcca.error = c_fatal
		return
	end if
	if isnull(ldt_validita) then
		g_mb.messagebox( "OMNIA", "La Data di Validità è obbligatoria!")
		pcca.error = c_fatal
		return
	end if
end if

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_tab_test_prodotti_dettaglio,"cod_attrezzatura")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_test_prodotti_dettaglio,"cod_prodotto")
end choose
end event

type tv_1 from treeview within w_tab_test_prodotti_tv
integer x = 69
integer y = 260
integer width = 1989
integer height = 2000
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
boolean linesatroot = true
boolean hideselection = false
boolean fullrowselect = true
string picturename[] = {""}
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event selectionchanged;treeviewitem ltv_item
s_test_prodotti ls_test_prodotti
long				ll_ret, ll_handle

if isnull(newhandle) or newhandle <= 0 then
	return 0
end if

getitem(newhandle,tvi_campo)
ltv_item = tvi_campo
ll_handle = tvi_campo.itemhandle

ls_test_prodotti = tvi_campo.data
is_cod_prodotto = ls_test_prodotti.s_cod_prodotto
is_cod_test = ls_test_prodotti.s_cod_test
idt_validita = ls_test_prodotti.dt_validita
is_tipo = ls_test_prodotti.s_tipo
il_handle = tvi_campo.itemhandle

//choose case is_tipo
//	case "P"
//		//Prodotto
//		dw_tab_test_prodotti_dettaglio.visible = false
//		cb_prodotto_sel.visible = false		
//		cb_2.visible = false
		
//	case "T"
//		//Test
//		dw_tab_test_prodotti_dettaglio.visible = true				
//		parent.triggerevent("pc_retrieve")
//		cb_prodotto_sel.visible = true
//		cb_2.visible = true

//end choose

if is_cod_prodotto<>"" and not isnull(is_cod_prodotto) and &
			is_cod_test<>"" and not isnull(is_cod_test) and not isnull(idt_validita) then
	
	parent.triggerevent("pc_retrieve")
end if



end event

type cb_refresh from commandbutton within w_tab_test_prodotti_tv
integer x = 457
integer y = 160
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Refresh"
end type

event clicked;cb_cerca.postevent(clicked!)
end event

type dw_filtro from u_dw_search within w_tab_test_prodotti_tv
integer x = 69
integer y = 120
integer width = 1989
integer height = 840
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_test_prodotti_filtro"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_filtro,"rs_cod_attrezzatura")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_filtro,"rs_cod_prodotto")
end choose
end event

type cb_cerca from commandbutton within w_tab_test_prodotti_tv
integer x = 1600
integer y = 840
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_filtro.AcceptText()

wf_cancella_treeview()

wf_imposta_tv()

dw_folder_search.fu_selecttab(2)

tv_1.setfocus()
end event

type cb_reset from commandbutton within w_tab_test_prodotti_tv
integer x = 1234
integer y = 840
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reset"
end type

event clicked;datetime ldt_oggi
string ls_null

setnull(ls_null)
setnull(ldt_oggi)

dw_filtro.setitem(1,"rs_cod_prodotto",ls_null)
dw_filtro.setitem(1,"rs_cod_test",ls_null)
dw_filtro.setitem(1,"rs_cod_attrezzatura",ls_null)
dw_filtro.setitem(1,"rs_cod_cat_attrezzature",ls_null)

dw_filtro.setitem(1,"rdt_data_validita_dal",ldt_oggi)

ldt_oggi = datetime(today(),time("00:00:00"))
dw_filtro.setitem(1,"rdt_data_validita_al",ldt_oggi)
end event

type cb_nuovo from commandbutton within w_tab_test_prodotti_tv
integer x = 69
integer y = 160
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nuovo"
end type

event clicked;dw_tab_test_prodotti_dettaglio.visible = true

//cb_prodotto_sel.visible = true
iuo_dw_main = dw_tab_test_prodotti_dettaglio
dw_tab_test_prodotti_dettaglio.change_dw_current()
parent.postevent("pc_new")
end event

