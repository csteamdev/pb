﻿$PBExportHeader$w_tab_test_valori.srw
$PBExportComments$Window tab_test_valori
forward
global type w_tab_test_valori from w_cs_xx_principale
end type
type dw_tab_test_valori_lista from uo_cs_xx_dw within w_tab_test_valori
end type
end forward

global type w_tab_test_valori from w_cs_xx_principale
integer width = 1998
integer height = 992
string title = "Valori Test"
dw_tab_test_valori_lista dw_tab_test_valori_lista
end type
global w_tab_test_valori w_tab_test_valori

type variables
string is_cod_test
end variables

event pc_setwindow;call super::pc_setwindow;dw_tab_test_valori_lista.set_dw_key("cod_test")

dw_tab_test_valori_lista.set_dw_key("cod_valore")

dw_tab_test_valori_lista.set_dw_options( sqlca, i_openparm, c_scrollparent, c_default)

iuo_dw_main = dw_tab_test_valori_lista

//is_cod_test = s_cs_xx.parametri.parametro_s_1


//if isnull(is_cod_test) or is_cod_test = "" then
//
//	messagebox( "OMNIA", "Attenzione, il codice test risulta mancante!")
//
//end if
end event

on w_tab_test_valori.create
int iCurrent
call super::create
this.dw_tab_test_valori_lista=create dw_tab_test_valori_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_test_valori_lista
end on

on w_tab_test_valori.destroy
call super::destroy
destroy(this.dw_tab_test_valori_lista)
end on

type dw_tab_test_valori_lista from uo_cs_xx_dw within w_tab_test_valori
integer x = 23
integer y = 20
integer width = 1920
integer height = 860
integer taborder = 10
string dataobject = "d_tab_test_valori_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

is_cod_test = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")

l_Error = Retrieve(is_cod_test)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event updatestart;call super::updatestart;long ll_i

is_cod_test = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_test")

for ll_i = 1 to rowcount()
	setitem( ll_i, "cod_test", is_cod_test)
next
end event

event itemchanged;call super::itemchanged;//Donato 28-10-2008: modifica per aggiunta flag_default ai valori dei test
long ll_index, ll_tot

if row > 0 then
	choose case dwo.name		
		case "flag_default"
			if data = "S" then
				//controlla che non ce ne sia un altro impostato a default
				ll_tot = this.rowcount()
				for ll_index = 1 to ll_tot
					if ll_index <> row then
						this.setitem(ll_index, "flag_default", "N")						
					end if
				next
			end if
	end choose
end if
end event

