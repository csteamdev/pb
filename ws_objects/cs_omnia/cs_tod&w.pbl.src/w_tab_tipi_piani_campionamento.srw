﻿$PBExportHeader$w_tab_tipi_piani_campionamento.srw
$PBExportComments$Window tab_tipi_piani_campionamento
forward
global type w_tab_tipi_piani_campionamento from w_cs_xx_principale
end type
type dw_tab_tipi_piani_campionamento_lista from uo_cs_xx_dw within w_tab_tipi_piani_campionamento
end type
end forward

global type w_tab_tipi_piani_campionamento from w_cs_xx_principale
int Width=1569
int Height=1165
boolean TitleBar=true
string Title="Tipi Piani Campionamento"
dw_tab_tipi_piani_campionamento_lista dw_tab_tipi_piani_campionamento_lista
end type
global w_tab_tipi_piani_campionamento w_tab_tipi_piani_campionamento

event pc_setwindow;call super::pc_setwindow;dw_tab_tipi_piani_campionamento_lista.set_dw_key("cod_azienda")
dw_tab_tipi_piani_campionamento_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_tipi_piani_campionamento_lista.ib_proteggi_chiavi = false
iuo_dw_main = dw_tab_tipi_piani_campionamento_lista
end event

on w_tab_tipi_piani_campionamento.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_tipi_piani_campionamento_lista=create dw_tab_tipi_piani_campionamento_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_tipi_piani_campionamento_lista
end on

on w_tab_tipi_piani_campionamento.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_tipi_piani_campionamento_lista)
end on

type dw_tab_tipi_piani_campionamento_lista from uo_cs_xx_dw within w_tab_tipi_piani_campionamento
int X=23
int Y=21
int Width=1486
int Height=1021
int TabOrder=10
string DataObject="d_tab_tipi_piani_campionamento_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

