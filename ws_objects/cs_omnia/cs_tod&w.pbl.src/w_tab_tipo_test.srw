﻿$PBExportHeader$w_tab_tipo_test.srw
$PBExportComments$Window tab_tipo_test
forward
global type w_tab_tipo_test from w_cs_xx_principale
end type
type dw_tab_tipo_test_lista from uo_cs_xx_dw within w_tab_tipo_test
end type
type dw_tab_tipo_test_dettaglio from uo_cs_xx_dw within w_tab_tipo_test
end type
end forward

global type w_tab_tipo_test from w_cs_xx_principale
int Width=1747
int Height=1541
boolean TitleBar=true
string Title="Tipi Test"
dw_tab_tipo_test_lista dw_tab_tipo_test_lista
dw_tab_tipo_test_dettaglio dw_tab_tipo_test_dettaglio
end type
global w_tab_tipo_test w_tab_tipo_test

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tab_tipo_test_lista.set_dw_key("cod_tipo_test")
dw_tab_tipo_test_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_tipo_test_dettaglio.set_dw_options(sqlca,dw_tab_tipo_test_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_tipo_test_lista
end on

on w_tab_tipo_test.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_tipo_test_lista=create dw_tab_tipo_test_lista
this.dw_tab_tipo_test_dettaglio=create dw_tab_tipo_test_dettaglio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_tipo_test_lista
this.Control[iCurrent+2]=dw_tab_tipo_test_dettaglio
end on

on w_tab_tipo_test.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_tipo_test_lista)
destroy(this.dw_tab_tipo_test_dettaglio)
end on

type dw_tab_tipo_test_lista from uo_cs_xx_dw within w_tab_tipo_test
int X=23
int Y=21
int Width=1669
int Height=501
int TabOrder=10
string DataObject="d_tab_tipo_test_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_tab_tipo_test_dettaglio from uo_cs_xx_dw within w_tab_tipo_test
int X=23
int Y=541
int Width=1669
int Height=881
int TabOrder=20
string DataObject="d_tab_tipo_test_dettaglio"
BorderStyle BorderStyle=StyleRaised!
end type

