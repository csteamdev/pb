﻿$PBExportHeader$w_tes_prodotti_qualita.srw
$PBExportComments$Finestra Gestione Testata Prodotti Qualità
forward
global type w_tes_prodotti_qualita from w_cs_xx_principale
end type
type dw_tes_prodotti_qualita_det_lista from uo_cs_xx_dw within w_tes_prodotti_qualita
end type
type dw_tes_prodotti_qualita_det_1 from uo_cs_xx_dw within w_tes_prodotti_qualita
end type
type dw_tes_prodotti_qualita_det_3 from uo_cs_xx_dw within w_tes_prodotti_qualita
end type
type dw_tes_prodotti_qualita_det_4 from uo_cs_xx_dw within w_tes_prodotti_qualita
end type
type dw_tes_prodotti_qualita_det_2 from uo_cs_xx_dw within w_tes_prodotti_qualita
end type
type dw_folder from u_folder within w_tes_prodotti_qualita
end type
type cb_collaudo from commandbutton within w_tes_prodotti_qualita
end type
type cb_requisiti from commandbutton within w_tes_prodotti_qualita
end type
type cb_ricerca_prodotti from cb_prod_ricerca within w_tes_prodotti_qualita
end type
end forward

global type w_tes_prodotti_qualita from w_cs_xx_principale
int Width=2670
int Height=1665
boolean TitleBar=true
string Title="Dati Tecnici e Qualitativi Prodotti"
dw_tes_prodotti_qualita_det_lista dw_tes_prodotti_qualita_det_lista
dw_tes_prodotti_qualita_det_1 dw_tes_prodotti_qualita_det_1
dw_tes_prodotti_qualita_det_3 dw_tes_prodotti_qualita_det_3
dw_tes_prodotti_qualita_det_4 dw_tes_prodotti_qualita_det_4
dw_tes_prodotti_qualita_det_2 dw_tes_prodotti_qualita_det_2
dw_folder dw_folder
cb_collaudo cb_collaudo
cb_requisiti cb_requisiti
cb_ricerca_prodotti cb_ricerca_prodotti
end type
global w_tes_prodotti_qualita w_tes_prodotti_qualita

type variables
boolean ib_delete=true
string is_prodotto, is_tipo_requisito
end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_tes_prodotti_qualita_det_2
dw_folder.fu_AssignTab(2, "Requisiti", l_Objects[])
l_objects[1] = dw_tes_prodotti_qualita_det_3
dw_folder.fu_AssignTab(3, "Dati Collaudo", l_Objects[])
l_objects[1] = dw_tes_prodotti_qualita_det_4
dw_folder.fu_AssignTab(4, "Generici", l_Objects[])
l_objects[1] = dw_tes_prodotti_qualita_det_1
l_objects[2] = cb_ricerca_prodotti
dw_folder.fu_AssignTab(1, "Specifiche", l_Objects[])

dw_folder.fu_FolderCreate(4,4)
dw_folder.fu_SelectTab(1)

dw_tes_prodotti_qualita_det_lista.ib_proteggi_chiavi = false
dw_tes_prodotti_qualita_det_1.ib_proteggi_chiavi = false


dw_tes_prodotti_qualita_det_lista.set_dw_key("cod_azienda")
dw_tes_prodotti_qualita_det_lista.set_dw_options(sqlca,pcca.null_object, c_default, c_default)
dw_tes_prodotti_qualita_det_1.set_dw_options(sqlca,dw_tes_prodotti_qualita_det_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_prodotti_qualita_det_2.set_dw_options(sqlca,dw_tes_prodotti_qualita_det_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_prodotti_qualita_det_3.set_dw_options(sqlca,dw_tes_prodotti_qualita_det_lista,c_sharedata+c_scrollparent,c_default)
dw_tes_prodotti_qualita_det_4.set_dw_options(sqlca,dw_tes_prodotti_qualita_det_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_prodotti_qualita_det_lista


end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_prodotti_qualita_det_lista,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_prodotti_qualita_det_1,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")




end on

on w_tes_prodotti_qualita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tes_prodotti_qualita_det_lista=create dw_tes_prodotti_qualita_det_lista
this.dw_tes_prodotti_qualita_det_1=create dw_tes_prodotti_qualita_det_1
this.dw_tes_prodotti_qualita_det_3=create dw_tes_prodotti_qualita_det_3
this.dw_tes_prodotti_qualita_det_4=create dw_tes_prodotti_qualita_det_4
this.dw_tes_prodotti_qualita_det_2=create dw_tes_prodotti_qualita_det_2
this.dw_folder=create dw_folder
this.cb_collaudo=create cb_collaudo
this.cb_requisiti=create cb_requisiti
this.cb_ricerca_prodotti=create cb_ricerca_prodotti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tes_prodotti_qualita_det_lista
this.Control[iCurrent+2]=dw_tes_prodotti_qualita_det_1
this.Control[iCurrent+3]=dw_tes_prodotti_qualita_det_3
this.Control[iCurrent+4]=dw_tes_prodotti_qualita_det_4
this.Control[iCurrent+5]=dw_tes_prodotti_qualita_det_2
this.Control[iCurrent+6]=dw_folder
this.Control[iCurrent+7]=cb_collaudo
this.Control[iCurrent+8]=cb_requisiti
this.Control[iCurrent+9]=cb_ricerca_prodotti
end on

on w_tes_prodotti_qualita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tes_prodotti_qualita_det_lista)
destroy(this.dw_tes_prodotti_qualita_det_1)
destroy(this.dw_tes_prodotti_qualita_det_3)
destroy(this.dw_tes_prodotti_qualita_det_4)
destroy(this.dw_tes_prodotti_qualita_det_2)
destroy(this.dw_folder)
destroy(this.cb_collaudo)
destroy(this.cb_requisiti)
destroy(this.cb_ricerca_prodotti)
end on

type dw_tes_prodotti_qualita_det_lista from uo_cs_xx_dw within w_tes_prodotti_qualita
int X=23
int Y=21
int Width=2583
int Height=501
int TabOrder=80
string DataObject="d_tes_prodotti_qualita_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on updatestart;call uo_cs_xx_dw::updatestart;if ib_delete then
   DELETE FROM det_prodotti_qualita
          WHERE (det_prodotti_qualita.cod_azienda = :s_cs_xx.cod_azienda) and
                (det_prodotti_qualita.cod_prodotto = :is_prodotto);
   ib_delete = false
end if
                
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if dw_tes_prodotti_qualita_det_lista.getrow() > 0 then
   is_prodotto = dw_tes_prodotti_qualita_det_lista.getitemstring(dw_tes_prodotti_qualita_det_lista.getrow(), "cod_prodotto")
end if

cb_collaudo.enabled = true
cb_requisiti.enabled = true
cb_ricerca_prodotti.enabled=false
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_collaudo.enabled = false
cb_requisiti.enabled = false
ib_delete = true
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_collaudo.enabled = false
cb_requisiti.enabled = false

cb_ricerca_prodotti.enabled=true

end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_collaudo.enabled = false
cb_requisiti.enabled = false
cb_ricerca_prodotti.enabled=true
end on

type dw_tes_prodotti_qualita_det_1 from uo_cs_xx_dw within w_tes_prodotti_qualita
int X=115
int Y=681
int Width=2401
int Height=721
int TabOrder=20
string DataObject="d_tes_prodotti_qualita_det_1"
boolean Border=false
end type

type dw_tes_prodotti_qualita_det_3 from uo_cs_xx_dw within w_tes_prodotti_qualita
int X=115
int Y=681
int Width=2401
int Height=721
int TabOrder=60
string DataObject="d_tes_prodotti_qualita_det_3"
boolean Border=false
end type

type dw_tes_prodotti_qualita_det_4 from uo_cs_xx_dw within w_tes_prodotti_qualita
int X=115
int Y=681
int Width=2378
int Height=701
int TabOrder=30
string DataObject="d_tes_prodotti_qualita_det_4"
boolean Border=false
end type

type dw_tes_prodotti_qualita_det_2 from uo_cs_xx_dw within w_tes_prodotti_qualita
int X=115
int Y=681
int Width=2401
int Height=721
int TabOrder=70
string DataObject="d_tes_prodotti_qualita_det_2"
boolean Border=false
end type

type dw_folder from u_folder within w_tes_prodotti_qualita
int X=23
int Y=541
int Width=2583
int Height=901
int TabOrder=90
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Specifiche"
      SetFocus(dw_tes_prodotti_qualita_det_1)
   CASE "Requisiti"
      SetFocus(dw_tes_prodotti_qualita_det_2)
   CASE "Dati Collaudo"
      SetFocus(dw_tes_prodotti_qualita_det_3)
   CASE "Generici"
      SetFocus(dw_tes_prodotti_qualita_det_4)
END CHOOSE



end on

type cb_collaudo from commandbutton within w_tes_prodotti_qualita
int X=1875
int Y=1461
int Width=366
int Height=81
int TabOrder=50
boolean Enabled=false
string Text="Car.Meccan."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;if upperbound(dw_tes_prodotti_qualita_det_lista.i_selectedrows) < 1 then
   g_mb.messagebox("Dati Qualità Prodotti","Attenzione: non risulta selezionato alcun articolo!", Stopsign!)
   return
end if

if not isvalid(w_det_prodotti_qualita) then
   s_cs_xx.parametri.parametro_b_1 = true
	window_open_parm(w_det_prodotti_qualita, -1, dw_tes_prodotti_qualita_det_lista)
end if
end on

type cb_requisiti from commandbutton within w_tes_prodotti_qualita
int X=2263
int Y=1461
int Width=343
int Height=81
int TabOrder=40
boolean Enabled=false
string Text="Requisiti"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;if upperbound(dw_tes_prodotti_qualita_det_lista.i_selectedrows) < 1 then
   g_mb.messagebox("Dati Qualità Prodotti","Attenzione: non risulta selezionato alcun articolo!", Stopsign!)
   return
end if

if not isvalid(w_det_prodotti_qualita) then
   s_cs_xx.parametri.parametro_b_1 = false
	window_open_parm(w_det_prodotti_qualita, -1, dw_tes_prodotti_qualita_det_lista)
end if
end on

type cb_ricerca_prodotti from cb_prod_ricerca within w_tes_prodotti_qualita
int X=2401
int Y=701
int TabOrder=10
boolean Enabled=false
end type

on getfocus;call cb_prod_ricerca::getfocus;dw_tes_prodotti_qualita_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end on

