﻿$PBExportHeader$w_tipi_fermi_macchina.srw
$PBExportComments$Window tipi fermi macchina
forward
global type w_tipi_fermi_macchina from w_cs_xx_principale
end type
type dw_tipi_fermi_macchina from uo_cs_xx_dw within w_tipi_fermi_macchina
end type
end forward

global type w_tipi_fermi_macchina from w_cs_xx_principale
int Width=1907
int Height=1285
boolean TitleBar=true
string Title="Tipi Fermi Macchina"
dw_tipi_fermi_macchina dw_tipi_fermi_macchina
end type
global w_tipi_fermi_macchina w_tipi_fermi_macchina

on w_tipi_fermi_macchina.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_fermi_macchina=create dw_tipi_fermi_macchina
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_fermi_macchina
end on

on w_tipi_fermi_macchina.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_fermi_macchina)
end on

event pc_setwindow;call super::pc_setwindow;dw_tipi_fermi_macchina.set_dw_key("cod_azienda")
dw_tipi_fermi_macchina.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
iuo_dw_main = dw_tipi_fermi_macchina
end event

type dw_tipi_fermi_macchina from uo_cs_xx_dw within w_tipi_fermi_macchina
int X=23
int Y=21
int Width=1829
int Height=1141
int TabOrder=20
string DataObject="d_tipi_fermi_macchina"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

