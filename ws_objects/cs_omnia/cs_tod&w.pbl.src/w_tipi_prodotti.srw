﻿$PBExportHeader$w_tipi_prodotti.srw
$PBExportComments$Tabella Tipi di Prodotti
forward
global type w_tipi_prodotti from w_cs_xx_principale
end type
type dw_tipi_prodotti_lista from uo_cs_xx_dw within w_tipi_prodotti
end type
type dw_tipi_prodotti from uo_cs_xx_dw within w_tipi_prodotti
end type
end forward

global type w_tipi_prodotti from w_cs_xx_principale
int Width=1930
int Height=1469
boolean TitleBar=true
string Title="Tabella Tipologie Prodotti"
dw_tipi_prodotti_lista dw_tipi_prodotti_lista
dw_tipi_prodotti dw_tipi_prodotti
end type
global w_tipi_prodotti w_tipi_prodotti

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tipi_prodotti_lista.set_dw_key("cod_azienda")
dw_tipi_prodotti_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tipi_prodotti.set_dw_options(sqlca,dw_tipi_prodotti_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tipi_prodotti_lista
end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_tipi_prodotti,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione","anag_divisioni.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end on

on w_tipi_prodotti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_prodotti_lista=create dw_tipi_prodotti_lista
this.dw_tipi_prodotti=create dw_tipi_prodotti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_prodotti_lista
this.Control[iCurrent+2]=dw_tipi_prodotti
end on

on w_tipi_prodotti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_prodotti_lista)
destroy(this.dw_tipi_prodotti)
end on

type dw_tipi_prodotti_lista from uo_cs_xx_dw within w_tipi_prodotti
int X=23
int Y=21
int Width=1852
int Height=481
int TabOrder=10
string DataObject="d_tipi_prodotto_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_tipi_prodotti from uo_cs_xx_dw within w_tipi_prodotti
int X=23
int Y=521
int Width=1852
int Height=821
int TabOrder=20
string DataObject="d_tipi_prodotto"
BorderStyle BorderStyle=StyleRaised!
end type

