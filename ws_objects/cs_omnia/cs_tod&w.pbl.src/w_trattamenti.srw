﻿$PBExportHeader$w_trattamenti.srw
$PBExportComments$Tabella Trattamenti
forward
global type w_trattamenti from w_cs_xx_principale
end type
type dw_trattamenti_lista from uo_cs_xx_dw within w_trattamenti
end type
type dw_trattamenti_det from uo_cs_xx_dw within w_trattamenti
end type
end forward

global type w_trattamenti from w_cs_xx_principale
integer width = 2162
integer height = 1328
string title = "Tabella Trattamenti"
dw_trattamenti_lista dw_trattamenti_lista
dw_trattamenti_det dw_trattamenti_det
end type
global w_trattamenti w_trattamenti

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_trattamenti_lista.set_dw_key("cod_azienda")
dw_trattamenti_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_trattamenti_det.set_dw_options(sqlca,dw_trattamenti_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_trattamenti_lista
end on

on w_trattamenti.create
int iCurrent
call super::create
this.dw_trattamenti_lista=create dw_trattamenti_lista
this.dw_trattamenti_det=create dw_trattamenti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_trattamenti_lista
this.Control[iCurrent+2]=this.dw_trattamenti_det
end on

on w_trattamenti.destroy
call super::destroy
destroy(this.dw_trattamenti_lista)
destroy(this.dw_trattamenti_det)
end on

type dw_trattamenti_lista from uo_cs_xx_dw within w_trattamenti
integer x = 23
integer y = 20
integer width = 2080
integer height = 500
integer taborder = 20
string dataobject = "d_trattamenti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_trattamenti_det from uo_cs_xx_dw within w_trattamenti
integer x = 23
integer y = 540
integer width = 2080
integer height = 660
integer taborder = 30
string dataobject = "d_trattamenti_det"
borderstyle borderstyle = styleraised!
end type

