﻿$PBExportHeader$w_det_det_piano_verifiche.srw
$PBExportComments$Finestra Dettaglio Visite Ispettive
forward
global type w_det_det_piano_verifiche from w_cs_xx_principale
end type
type cb_doc_comp from cb_documenti_compilati within w_det_det_piano_verifiche
end type
type dw_det_det_piano_verifiche from uo_cs_xx_dw within w_det_det_piano_verifiche
end type
type dw_det_det_piano_verifiche_2 from uo_cs_xx_dw within w_det_det_piano_verifiche
end type
type dw_det_det_piano_verifiche_1 from uo_cs_xx_dw within w_det_det_piano_verifiche
end type
type dw_folder from u_folder within w_det_det_piano_verifiche
end type
end forward

global type w_det_det_piano_verifiche from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2551
integer height = 1680
string title = "Dettaglio Visita Ispettiva"
cb_doc_comp cb_doc_comp
dw_det_det_piano_verifiche dw_det_det_piano_verifiche
dw_det_det_piano_verifiche_2 dw_det_det_piano_verifiche_2
dw_det_det_piano_verifiche_1 dw_det_det_piano_verifiche_1
dw_folder dw_folder
end type
global w_det_det_piano_verifiche w_det_det_piano_verifiche

type variables
boolean ib_in_new
end variables

forward prototypes
public subroutine wf_tipo_operatore (string as_tipo_operatore)
public subroutine wf_tipo_dettaglio (string as_tipo_dettaglio)
end prototypes

public subroutine wf_tipo_operatore (string as_tipo_operatore);if as_tipo_operatore = "S" then
	dw_det_det_piano_verifiche_1.Object.des_manuale_operaio.Visible = 0
	dw_det_det_piano_verifiche_1.Object.cod_operaio.Visible = 1
	dw_det_det_piano_verifiche_1.Object.cf_cod_operaio.Visible = 1
else
	dw_det_det_piano_verifiche_1.Object.des_manuale_operaio.Visible = 1
	dw_det_det_piano_verifiche_1.Object.cod_operaio.Visible = 0
	dw_det_det_piano_verifiche_1.Object.cf_cod_operaio.Visible = 0
end if

end subroutine

public subroutine wf_tipo_dettaglio (string as_tipo_dettaglio);choose case as_tipo_dettaglio
	case "V"  // Valutatore
		wf_tipo_operatore (dw_det_det_piano_verifiche.getitemstring(dw_det_det_piano_verifiche.getrow(), "flag_operaio_codificato"))
		dw_det_det_piano_verifiche_1.Object.flag_operaio_codificato.visible = 1
		dw_det_det_piano_verifiche_1.Object.flag_operaio_codificato_t.visible = 1
		dw_det_det_piano_verifiche_1.Object.cod_operaio_t.visible = 1
		dw_det_det_piano_verifiche_1.Object.cf_cod_area_aziendale.Visible = 1
		dw_det_det_piano_verifiche_1.Object.cod_area_aziendale.Visible = 1
		dw_det_det_piano_verifiche_1.Object.cod_area_aziendale_t.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.ore_impiegate.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.ore_impiegate_t.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.costi_viaggio.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.costi_viaggio_t.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.costi_trasferta.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.costi_trasferta_t.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.altri_costi_voce.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.altri_costi_voce_t.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.altri_costi_importo.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.altri_costi_importo_t.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.cf_num_reg_lista.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.num_reg_lista.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.num_reg_lista_t.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.prog_liste_con_comp.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.prog_liste_con_comp_t.Visible = 1
	case "C"  // Consultato
		wf_tipo_operatore (dw_det_det_piano_verifiche.getitemstring(dw_det_det_piano_verifiche.getrow(), "flag_operaio_codificato"))
		dw_det_det_piano_verifiche_1.Object.flag_operaio_codificato.visible = 1
		dw_det_det_piano_verifiche_1.Object.flag_operaio_codificato_t.visible = 1
		dw_det_det_piano_verifiche_1.Object.cod_operaio_t.visible = 1
		dw_det_det_piano_verifiche_1.Object.cf_cod_area_aziendale.Visible = 1
		dw_det_det_piano_verifiche_1.Object.cod_area_aziendale.Visible = 1
		dw_det_det_piano_verifiche_1.Object.cod_area_aziendale_t.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.ore_impiegate.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.ore_impiegate_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_viaggio.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_viaggio_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_trasferta.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_trasferta_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_voce.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_voce_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_importo.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_importo_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.cf_num_reg_lista.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.num_reg_lista.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.num_reg_lista_t.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.prog_liste_con_comp.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.prog_liste_con_comp_t.Visible = 1
	case "D"  // Documentazione
		dw_det_det_piano_verifiche_1.Object.flag_operaio_codificato.visible = 0
		dw_det_det_piano_verifiche_1.Object.flag_operaio_codificato_t.visible = 0
		dw_det_det_piano_verifiche_1.Object.cod_operaio.visible = 0
		dw_det_det_piano_verifiche_1.Object.cf_cod_operaio.visible = 0
		dw_det_det_piano_verifiche_1.Object.cod_operaio_t.visible = 0
		dw_det_det_piano_verifiche_1.Object.des_manuale_operaio.visible = 0
		dw_det_det_piano_verifiche_1.Object.cod_operaio_t.visible = 0
		dw_det_det_piano_verifiche_1.Object.cf_cod_area_aziendale.Visible = 0
		dw_det_det_piano_verifiche_1.Object.cod_area_aziendale.Visible = 0
		dw_det_det_piano_verifiche_1.Object.cod_area_aziendale_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.ore_impiegate.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.ore_impiegate_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_viaggio.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_viaggio_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_trasferta.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_trasferta_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_voce.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_voce_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_importo.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_importo_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.cf_num_reg_lista.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.num_reg_lista.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.num_reg_lista_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.prog_liste_con_comp.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.prog_liste_con_comp_t.Visible = 0
	case "A"  // Area Aziendale
		wf_tipo_operatore (dw_det_det_piano_verifiche.getitemstring(dw_det_det_piano_verifiche.getrow(), "flag_operaio_codificato"))
		dw_det_det_piano_verifiche_1.Object.flag_operaio_codificato.visible = 1
		dw_det_det_piano_verifiche_1.Object.flag_operaio_codificato_t.visible = 1
		dw_det_det_piano_verifiche_1.Object.cod_operaio_t.visible = 1
		dw_det_det_piano_verifiche_1.Object.cf_cod_area_aziendale.Visible = 1
		dw_det_det_piano_verifiche_1.Object.cod_area_aziendale.Visible = 1
		dw_det_det_piano_verifiche_1.Object.cod_area_aziendale_t.Visible = 1
//		dw_det_det_piano_verifiche_1.Object.ore_impiegate.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.ore_impiegate_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_viaggio.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_viaggio_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_trasferta.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.costi_trasferta_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_voce.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_voce_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_importo.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.altri_costi_importo_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.cf_num_reg_lista.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.num_reg_lista.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.num_reg_lista_t.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.prog_liste_con_comp.Visible = 0
//		dw_det_det_piano_verifiche_1.Object.prog_liste_con_comp_t.Visible = 0		
end choose

end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_det_det_piano_verifiche_1
dw_folder.fu_AssignTab(1, "&Dettaglio", l_Objects[])

l_objects[1] = dw_det_det_piano_verifiche_2
l_objects[2] = cb_doc_comp
dw_folder.fu_AssignTab(2, "&Note", l_Objects[])

dw_folder.fu_FolderCreate(2,4)

dw_det_det_piano_verifiche.set_dw_key("cod_azienda")
dw_det_det_piano_verifiche.set_dw_key("anno_verifica")
dw_det_det_piano_verifiche.set_dw_key("prog_verifica")
dw_det_det_piano_verifiche.set_dw_key("prog_det_verifica")
dw_det_det_piano_verifiche.set_dw_options(sqlca,i_openparm,c_default,c_default)

dw_det_det_piano_verifiche_1.set_dw_options(sqlca,dw_det_det_piano_verifiche,c_sharedata+c_scrollparent,c_default)
dw_det_det_piano_verifiche_2.set_dw_options(sqlca,dw_det_det_piano_verifiche,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_det_det_piano_verifiche

dw_folder.fu_SelectTab(1)
this.postevent("pc_view")
end event

on w_det_det_piano_verifiche.create
int iCurrent
call super::create
this.cb_doc_comp=create cb_doc_comp
this.dw_det_det_piano_verifiche=create dw_det_det_piano_verifiche
this.dw_det_det_piano_verifiche_2=create dw_det_det_piano_verifiche_2
this.dw_det_det_piano_verifiche_1=create dw_det_det_piano_verifiche_1
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_doc_comp
this.Control[iCurrent+2]=this.dw_det_det_piano_verifiche
this.Control[iCurrent+3]=this.dw_det_det_piano_verifiche_2
this.Control[iCurrent+4]=this.dw_det_det_piano_verifiche_1
this.Control[iCurrent+5]=this.dw_folder
end on

on w_det_det_piano_verifiche.destroy
call super::destroy
destroy(this.cb_doc_comp)
destroy(this.dw_det_det_piano_verifiche)
destroy(this.dw_det_det_piano_verifiche_2)
destroy(this.dw_det_det_piano_verifiche_1)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_det_piano_verifiche_1,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_det_piano_verifiche_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_det_piano_verifiche_1,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )
end event

event pc_view;call super::pc_view;//integer li_row
//li_row = dw_det_visite_ispettive_lista.getrow()
//
//if li_row > 0 and dw_det_visite_ispettive_det_1.GetItemNumber(li_row,"num_reg_visita") <> 0 then
//	cb_controllo.enabled = True
//	cb_note_esterne.enabled = True
//else
//	cb_controllo.enabled = False
//	cb_note_esterne.enabled = False
//end if

end event

event pc_delete;call super::pc_delete;//integer li_row
//li_row = dw_det_det_piano_verifiche.getrow()
//
//if not( li_row > 0 and dw_det_det_piano_verifiche.GetItemNumber(li_row,"num_reg_visita") <> 0 ) then
//	cb_controllo.enabled = False
//	cb_note_esterne.enabled = False
//end if

end event

event pc_modify;call super::pc_modify;//cb_controllo.enabled = False
//cb_note_esterne.enabled = False
//
end event

event pc_new;call super::pc_new;//cb_controllo.enabled = False
//cb_note_esterne.enabled = False
//
end event

event pc_save;call super::pc_save;//integer li_row
//li_row = dw_det_visite_ispettive_lista.getrow()
//
//if li_row > 0 and dw_det_visite_ispettive_det_1.GetItemNumber(li_row ,"num_reg_visita") <> 0 then
//	cb_controllo.enabled = True
//	cb_note_esterne.enabled = True
//else
//	cb_controllo.enabled = False
//	cb_note_esterne.enabled = False
//end if
//
end event

type cb_doc_comp from cb_documenti_compilati within w_det_det_piano_verifiche
integer x = 2149
integer y = 800
integer width = 73
integer height = 80
integer taborder = 40
string text = "..."
end type

event clicked;call super::clicked;string ls_doc

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
window_open(w_det_visite_ispettive_collegamenti,0)

if isnull(s_cs_xx.parametri.parametro_s_1) or isnull(s_cs_xx.parametri.parametro_s_2) then return 

dw_det_det_piano_verifiche_2.setitem(dw_det_det_piano_verifiche_2.getrow(), "doc_compilato", s_cs_xx.parametri.parametro_s_1 + " - " + s_cs_xx.parametri.parametro_s_2)



end event

type dw_det_det_piano_verifiche from uo_cs_xx_dw within w_det_det_piano_verifiche
integer x = 23
integer y = 20
integer width = 2469
integer height = 500
integer taborder = 20
string dataobject = "d_det_det_piano_verifiche_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_anno_visita, ll_num_reg_visita

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

ll_anno_visita = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_verifica")
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "anno_verifica")) or GetItemnumber(l_Idx, "anno_verifica") = 0 THEN
      SetItem(l_Idx, "anno_verifica", ll_anno_visita)
   END IF
NEXT

ll_num_reg_visita = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_verifica")
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "prog_verifica")) or GetItemnumber(l_Idx, "prog_verifica") = 0 THEN
      SetItem(l_Idx, "prog_verifica", ll_num_reg_visita)
   END IF
NEXT

ll_num_reg_visita = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_det_verifica")
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "prog_det_verifica")) or GetItemnumber(l_Idx, "prog_det_verifica") = 0 THEN
      SetItem(l_Idx, "prog_det_verifica", ll_num_reg_visita)
   END IF
NEXT


end event

event pcd_view;call super::pcd_view;ib_in_new = false

end event

event pcd_new;call super::pcd_new;ib_in_new = true
wf_tipo_operatore ("N")
wf_tipo_dettaglio ("V")
end event

event updatestart;call super::updatestart;integer li_row
long    ll_anno_verifica, ll_prog_verifica, ll_prog_det_verifica, ll_progressivo

li_row = this.getrow()
if li_row > 0 and ib_in_new then


   ll_anno_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_verifica")
   ll_prog_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_verifica")
	ll_prog_det_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_det_verifica")

   select max(prog_det_piano)
     into :ll_progressivo
     from det_det_piano_verifiche
    where (cod_azienda = :s_cs_xx.cod_azienda) and
          (anno_verifica = :ll_anno_verifica ) and
          (prog_verifica = :ll_prog_verifica ) and
		    (prog_det_verifica = :ll_prog_det_verifica);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_progressivo) then
      ll_progressivo = 1
   else
      ll_progressivo = ll_progressivo + 1
   end if
   this.SetItem (li_row, "prog_det_piano", ll_progressivo)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (li_row ,"prog_det_piano", 10)
   end if

   ib_in_new = false
end if
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_anno_verifica, ll_prog_verifica, ll_prog_det_verifica, l_error

ll_anno_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_verifica")
ll_prog_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_verifica")
ll_prog_det_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_det_verifica")

l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno_verifica, ll_prog_verifica, ll_prog_det_verifica)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

on pcd_delete;call uo_cs_xx_dw::pcd_delete;ib_in_new = false
end on

event pcd_modify;call super::pcd_modify;ib_in_new = false

end event

event pcd_pickedrow;call super::pcd_pickedrow;integer li_row
li_row = this.getrow()
if li_row > 0 then
	wf_tipo_operatore (this.getitemstring(li_row, "flag_operaio_codificato"))
	wf_tipo_dettaglio (this.getitemstring(li_row, "flag_tipo_dettaglio"))
end if
end event

event pcd_validaterow;call super::pcd_validaterow;integer li_row
string ls_tipo_dettaglio

li_row = this.getrow()
if li_row > 0 then
	
	ls_tipo_dettaglio = this.getitemstring(li_row,"flag_tipo_dettaglio")
	choose case ls_tipo_dettaglio
		case 'A' // Area Aziendale
			if isnull(this.getitemstring(li_row, "cod_area_aziendale")) then
				g_mb.messagebox("Visite Ispettive", "L'indicazione dell'Area Aziendale è obbligatoria", StopSign!)
				pcca.error = c_valfailed
				return
			end if
		case else // Valutatore o Consultato
			if ls_tipo_dettaglio <> "D" then
				string ls_operaio_codificato
				ls_operaio_codificato = this.getitemstring(li_row, "flag_operaio_codificato")
				if (ls_operaio_codificato = 'S' and &
					isnull(this.getitemstring(li_row, "cod_operaio"))) or &
					(ls_operaio_codificato = 'N' and &
					isnull(this.getitemstring(li_row, "des_manuale_operaio"))) then
						g_mb.messagebox("Visite Ispettive", "L'indicazione dell' operatore è obbligatoria", StopSign!)
						pcca.error = c_valfailed
						return
				end if			
			end if
	end choose	
end if
end event

type dw_det_det_piano_verifiche_2 from uo_cs_xx_dw within w_det_det_piano_verifiche
integer x = 41
integer y = 680
integer width = 2400
integer height = 816
integer taborder = 40
string dataobject = "d_det_det_piano_verifiche_2"
boolean border = false
end type

type dw_det_det_piano_verifiche_1 from uo_cs_xx_dw within w_det_det_piano_verifiche
integer x = 46
integer y = 680
integer width = 2400
integer height = 840
integer taborder = 30
string dataobject = "d_det_det_piano_verifiche_1"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

string ls_des_lista, ls_cod_area_aziendale
long ll_num_versione,ll_num_edizione

if i_colname="num_reg_lista" then
    SELECT tes_liste_controllo.num_versione,   
         tes_liste_controllo.num_edizione,   
         tes_liste_controllo.des_lista,   
         tes_liste_controllo.cod_area_aziendale
    INTO :ll_num_versione,   
         :ll_num_edizione,   
         :ls_des_lista,   
         :ls_cod_area_aziendale
    FROM tes_liste_controllo  
   WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( tes_liste_controllo.num_reg_lista = :i_coltext ) AND  
         ( tes_liste_controllo.flag_valido = 'S' )   ;

   if (this.getitemstring(this.getrow(), "cod_area_aziendale") <> ls_cod_area_aziendale)  and &
      (not isnull(this.getitemstring(this.getrow(), "cod_area_aziendale")))   then
         g_mb.messagebox("Visite Ispettive","Attenzione, l'area di riferimento della lista è diversa dall'area indicata: la cambio automaticamente", Information!)
   end if

   setitem(getrow(),"cod_area_aziendale", ls_cod_area_aziendale)
   setitem(getrow(),"num_versione", ll_num_versione)
   setitem(getrow(),"num_edizione", ll_num_edizione)
end if

end if
end event

event itemchanged;call super::itemchanged;long ll_null,ll_num_reg_lista,ll_num_edizione,ll_num_versione, ll_prog_lista

setnull(ll_null)

choose case i_colname
	case "flag_operaio_codificato"
		wf_tipo_operatore (i_coltext)
	case "flag_tipo_dettaglio"
		wf_tipo_dettaglio (i_coltext)
	case "num_reg_lista"
		
		ll_prog_lista = getitemnumber(getrow(),"prog_liste_con_comp")

		if not isnull(ll_prog_lista) then
			g_mb.messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata alla manutenzione dopo aver già eseguito la compilazione")
			return 1
		end if	

		ll_num_reg_lista = long(i_coltext)

		select max(num_edizione)
		into   :ll_num_edizione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_num_reg_lista and
				 approvato_da is not null;
		 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if

		select max(num_versione)
		into   :ll_num_versione
		from   tes_liste_controllo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 num_reg_lista = :ll_num_reg_lista and
				 num_edizione = :ll_num_edizione and
				 approvato_da is not null;
		 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
			return -1
		end if

		setnull(ll_prog_lista)

		setitem(getrow(),"num_edizione",ll_num_edizione)
		setitem(getrow(),"num_versione",ll_num_versione)
		setitem(getrow(),"prog_liste_con_comp",ll_prog_lista)		
		
end choose

end event

type dw_folder from u_folder within w_det_det_piano_verifiche
integer x = 23
integer y = 520
integer width = 2469
integer height = 1040
integer taborder = 10
boolean border = false
end type

