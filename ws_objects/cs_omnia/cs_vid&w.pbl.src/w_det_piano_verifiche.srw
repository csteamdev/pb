﻿$PBExportHeader$w_det_piano_verifiche.srw
$PBExportComments$Window det_piano_verifiche
forward
global type w_det_piano_verifiche from w_cs_xx_principale
end type
type cb_dettagli from commandbutton within w_det_piano_verifiche
end type
type cb_paragrafi_iso from commandbutton within w_det_piano_verifiche
end type
type dw_det_piano_verifiche_lista from uo_cs_xx_dw within w_det_piano_verifiche
end type
type dw_det_piano_verifiche_det from uo_cs_xx_dw within w_det_piano_verifiche
end type
type cb_doc_comp from cb_documenti_compilati within w_det_piano_verifiche
end type
type cb_genera from commandbutton within w_det_piano_verifiche
end type
end forward

global type w_det_piano_verifiche from w_cs_xx_principale
integer width = 2528
integer height = 2184
string title = "Visite Ispettive"
cb_dettagli cb_dettagli
cb_paragrafi_iso cb_paragrafi_iso
dw_det_piano_verifiche_lista dw_det_piano_verifiche_lista
dw_det_piano_verifiche_det dw_det_piano_verifiche_det
cb_doc_comp cb_doc_comp
cb_genera cb_genera
end type
global w_det_piano_verifiche w_det_piano_verifiche

type variables

end variables

forward prototypes
public subroutine wf_tipo_mansionario (string as_tipo_mansionario)
end prototypes

public subroutine wf_tipo_mansionario (string as_tipo_mansionario);if as_tipo_mansionario = "S" then
	dw_det_piano_verifiche_det.Object.des_man_mansionario.Visible = 0
	dw_det_piano_verifiche_det.Object.cod_resp_divisione.Visible = 1
	dw_det_piano_verifiche_det.Object.cf_cod_resp_divisione.Visible = 1
else
	dw_det_piano_verifiche_det.Object.des_man_mansionario.Visible = 1
	dw_det_piano_verifiche_det.Object.cod_resp_divisione.Visible = 0
	dw_det_piano_verifiche_det.Object.cf_cod_resp_divisione.Visible = 0
end if

end subroutine

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_piano_verifiche_det,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")



f_PO_LoadDDDW_DW(dw_det_piano_verifiche_det,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")



end event

event pc_setwindow;call super::pc_setwindow;dw_det_piano_verifiche_lista.set_dw_key("cod_azienda")
dw_det_piano_verifiche_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)
dw_det_piano_verifiche_det.set_dw_options(sqlca,dw_det_piano_verifiche_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_det_piano_verifiche_lista
this.postevent("pc_view")
end event

on w_det_piano_verifiche.create
int iCurrent
call super::create
this.cb_dettagli=create cb_dettagli
this.cb_paragrafi_iso=create cb_paragrafi_iso
this.dw_det_piano_verifiche_lista=create dw_det_piano_verifiche_lista
this.dw_det_piano_verifiche_det=create dw_det_piano_verifiche_det
this.cb_doc_comp=create cb_doc_comp
this.cb_genera=create cb_genera
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_dettagli
this.Control[iCurrent+2]=this.cb_paragrafi_iso
this.Control[iCurrent+3]=this.dw_det_piano_verifiche_lista
this.Control[iCurrent+4]=this.dw_det_piano_verifiche_det
this.Control[iCurrent+5]=this.cb_doc_comp
this.Control[iCurrent+6]=this.cb_genera
end on

on w_det_piano_verifiche.destroy
call super::destroy
destroy(this.cb_dettagli)
destroy(this.cb_paragrafi_iso)
destroy(this.dw_det_piano_verifiche_lista)
destroy(this.dw_det_piano_verifiche_det)
destroy(this.cb_doc_comp)
destroy(this.cb_genera)
end on

type cb_dettagli from commandbutton within w_det_piano_verifiche
integer x = 1189
integer y = 2000
integer width = 411
integer height = 80
integer taborder = 61
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

event clicked;window_open_parm(w_det_det_piano_verifiche, -1, dw_det_piano_verifiche_lista )
end event

type cb_paragrafi_iso from commandbutton within w_det_piano_verifiche
integer x = 1623
integer y = 2000
integer width = 411
integer height = 80
integer taborder = 51
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Paragrafi ISO"
end type

event clicked;if dw_det_piano_verifiche_lista.rowcount() > 0 then
	window_open_parm(w_det_piano_verifiche_iso, -1, dw_det_piano_verifiche_lista)
end if
end event

type dw_det_piano_verifiche_lista from uo_cs_xx_dw within w_det_piano_verifiche
integer x = 23
integer y = 20
integer width = 2446
integer height = 520
integer taborder = 10
string dataobject = "d_det_piano_verifiche_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_prog_verifica
integer li_anno_verifica

li_anno_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_verifica")
ll_prog_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_verifica")

l_Error = Retrieve(s_cs_xx.cod_azienda,li_anno_verifica,ll_prog_verifica)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,ll_prog_verifica,ll_prog_det_verifica
integer li_anno_verifica

li_anno_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_verifica")
ll_prog_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_verifica")

select max(prog_det_verifica)
into   :ll_prog_det_verifica
from   det_piano_verifiche
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_verifica=:li_anno_verifica
and    prog_verifica=:ll_prog_verifica;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return
end if

if isnull(ll_prog_det_verifica) or ll_prog_det_verifica = 0 then
	ll_prog_det_verifica = 1
else
	ll_prog_det_verifica++
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "anno_verifica", li_anno_verifica)
		SetItem(l_Idx, "prog_verifica", ll_prog_verifica)
		SetItem(l_Idx, "prog_det_verifica", ll_prog_det_verifica)
   END IF
NEXT

end event

event pcd_pickedrow;call super::pcd_pickedrow;integer li_row
li_row = this.getrow()
if li_row > 0 then wf_tipo_mansionario (this.getitemstring(li_row, "flag_mansionario_codificato"))
end event

event updatestart;call super::updatestart;integer li_i
long    ll_anno_verifica, ll_prog_verifica, ll_prog_det_verifica

for li_i = 1 to this.deletedcount()
	ll_anno_verifica = this.getitemnumber(li_i, "anno_verifica", delete!, true)
	ll_prog_verifica = this.getitemnumber(li_i, "prog_verifica", delete!, true)
	ll_prog_det_verifica = this.getitemnumber(li_i, "prog_det_verifica", delete!, true)
	
	// devo cancellare tutti i paragrafi
	
	delete from det_piano_verifiche_iso
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_verifica = :ll_anno_verifica and
			prog_verifica = :ll_prog_verifica and 
			prog_det_verifica = :ll_prog_det_verifica;
				
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione dei paragrafi ISO: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	// devo cancellare tutti i dettagli
	
	delete from det_det_piano_verifiche
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_verifica = :ll_anno_verifica and
			prog_verifica = :ll_prog_verifica and 
			prog_det_verifica = :ll_prog_det_verifica;
			
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione dei dettagli: " + sqlca.sqlerrtext)
		rollback;
		return
	end if			
	
	// devo cancellare tutti i dettagli
	
	delete from det_piano_verifiche
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_verifica = :ll_anno_verifica and
			prog_verifica = :ll_prog_verifica and 
			prog_det_verifica = :ll_prog_det_verifica;
				
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione della riga: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
			
next	


if s_cs_xx.num_livello_mail > 0 then
	if getitemstatus(this.getrow(),0,primary!) = datamodified!	then			
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		setnull(s_cs_xx.parametri.parametro_s_4)
		s_cs_xx.parametri.parametro_s_5 = "Modifica del Piano di Verifica " + string(this.getitemnumber(this.getrow(),"anno_verifica")) + "/" + string(this.getitemnumber(this.getrow(),"prog_verifica")) + string(this.getitemnumber(this.getrow(),"prog_det_verifica"))
		s_cs_xx.parametri.parametro_s_6 = "Modifica del Piano di Verifica " + string(this.getitemnumber(this.getrow(),"anno_verifica")) + "/" + string(this.getitemnumber(this.getrow(),"prog_verifica")) + string(this.getitemnumber(this.getrow(),"prog_det_verifica"))
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari?"
		s_cs_xx.parametri.parametro_s_8 = ""
	
		openwithparm(w_invio_messaggi,0)
	elseif getitemstatus(this.getrow(),0,primary!) = newmodified! then
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		setnull(s_cs_xx.parametri.parametro_s_4)
		s_cs_xx.parametri.parametro_s_5 = "Creazione del Piano di Verifica " + string(this.getitemnumber(this.getrow(),"anno_verifica")) + "/" + string(this.getitemnumber(this.getrow(),"prog_verifica")) + string(this.getitemnumber(this.getrow(),"prog_det_verifica"))
		s_cs_xx.parametri.parametro_s_6 = "Creazione del Piano di Verifica " + string(this.getitemnumber(this.getrow(),"anno_verifica")) + "/" + string(this.getitemnumber(this.getrow(),"prog_verifica")) + string(this.getitemnumber(this.getrow(),"prog_det_verifica"))
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari?"
		s_cs_xx.parametri.parametro_s_8 = ""
		
		openwithparm(w_invio_messaggi,0)
		
	end if
end if
end event

event getfocus;call super::getfocus;dw_det_piano_verifiche_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type dw_det_piano_verifiche_det from uo_cs_xx_dw within w_det_piano_verifiche
integer x = 23
integer y = 560
integer width = 2446
integer height = 1420
integer taborder = 20
string dataobject = "d_det_piano_verifiche_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_colname = "flag_mansionario_codificato" then
	wf_tipo_mansionario (i_coltext)
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_det_piano_verifiche_det,"cod_fornitore")
end choose
end event

type cb_doc_comp from cb_documenti_compilati within w_det_piano_verifiche
integer x = 1952
integer y = 1376
integer width = 69
integer height = 80
integer taborder = 40
string text = "..."
end type

event clicked;call super::clicked;//long ll_riga[], ll_anno_reg, ll_num_reg
//string ls_ritorno
//
//s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//s_cs_xx.parametri.parametro_s_1 = "doc_compilato"
//s_cs_xx.parametri.parametro_s_2 = "DQ2"
//s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1)
//
//if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
//   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)
//
//   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
//		ll_anno_reg = dw_tes_visite_ispettive_det.getitemnumber(dw_tes_visite_ispettive_det.getrow(),"anno_reg_visita")
//		ll_num_reg  = dw_tes_visite_ispettive_det.getitemnumber(dw_tes_visite_ispettive_det.getrow(),"num_reg_visita")
//		update tes_visite_ispettive
//		set    doc_compilato = :ls_ritorno
//		where  tes_visite_ispettive.cod_azienda        = :s_cs_xx.cod_azienda and
//		       tes_visite_ispettive.anno_reg_visita = :ll_anno_reg and
//		       tes_visite_ispettive.num_reg_visita  = :ll_num_reg;
//		if sqlca.sqlcode <> 0 then
//			messagebox("OMNIA","Si è verificato un errore: il documento potrebbe non essere registrato",Information!)
//		end if
//   end if      
//else
//   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
//end if
//ll_riga[1] = dw_tes_visite_ispettive_det.getrow()
//dw_tes_visite_ispettive_lista.triggerevent("pcd_retrieve")
//dw_tes_visite_ispettive_lista.Set_Selected_Rows(1, ll_riga, &
//                                            c_CheckForChanges, &
//                                            c_RefreshChildren, &
//                                            c_RefreshView)
end event

type cb_genera from commandbutton within w_det_piano_verifiche
integer x = 2057
integer y = 2000
integer width = 411
integer height = 80
integer taborder = 41
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera "
end type

event clicked;integer  li_anno_reg_visita, li_anno_verifica
long     ll_num_reg_visita, ll_prog_verifica, ll_prog_det_verifica, ll_prog_det_piano
string   ls_cod_fornitore, ls_cod_area_aziendale, ls_cod_resp_divisione, ls_motivo_verifica, ls_flag_tipo_vi, & 
		   ls_flag_mansionario_codificato, ls_des_man_mansionario, ls_prog_paragrafo, ls_flag_riferimento_primario, &
			ls_obiettivi, ls_flag_tipo_dettaglio, ls_flag_operaio_codificato, ls_note, ls_cod_operaio, &
			ls_flag_eseguita, ls_doc_compilato, ls_des_manuale_operaio
datetime ldt_oggi

ls_flag_eseguita = dw_det_piano_verifiche_det.getitemstring(dw_det_piano_verifiche_det.getrow(), "flag_eseguita")

if ls_flag_eseguita = "S" then
	g_mb.messagebox("Omnia","Attenzione: la verifica risulta già generata. ")
	RETURN 
end if

li_anno_verifica= dw_det_piano_verifiche_det.getitemnumber(dw_det_piano_verifiche_det.getrow(),"anno_verifica")
ll_prog_verifica= dw_det_piano_verifiche_det.getitemnumber(dw_det_piano_verifiche_det.getrow(),"prog_verifica")
ll_prog_det_verifica= dw_det_piano_verifiche_det.getitemnumber(dw_det_piano_verifiche_det.getrow(),"prog_det_verifica")

ldt_oggi = datetime(today())

select cod_fornitore,
		 cod_area_aziendale,
		 cod_resp_divisione,
		 motivo_verifica,
		 flag_tipo_vi,
		 flag_mansionario_codificato,
		 des_man_mansionario,
		 obiettivi
into   :ls_cod_fornitore,
 		 :ls_cod_area_aziendale,
		 :ls_cod_resp_divisione,
		 :ls_motivo_verifica,
		 :ls_flag_tipo_vi,
		 :ls_flag_mansionario_codificato,
		 :ls_des_man_mansionario,
		 :ls_obiettivi
from   det_piano_verifiche
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_verifica=:li_anno_verifica
and    prog_verifica=:ll_prog_verifica
and    prog_det_verifica=:ll_prog_det_verifica;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

li_anno_reg_visita = f_anno_esercizio()

select max(num_reg_visita)
into   :ll_num_reg_visita
from   tes_visite_ispettive
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_reg_visita=:li_anno_reg_visita;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

if isnull(ll_num_reg_visita) or ll_num_reg_visita=0 then
	ll_num_reg_visita = 1
else
	ll_num_reg_visita++
end if

insert into tes_visite_ispettive
(cod_azienda,
 anno_reg_visita,
 num_reg_visita,
 cod_resp_divisione,
 data_visita,
 cod_area_aziendale,
 cod_fornitore,
 motivo_verifica,
 flag_tipo_vi,
 flag_mansionario_codificato,
 des_man_mansionario,
 obiettivi)
values
(:s_cs_xx.cod_azienda,
 :li_anno_reg_visita,
 :ll_num_reg_visita,
 :ls_cod_resp_divisione,
 :ldt_oggi,
 :ls_cod_area_aziendale,
 :ls_cod_fornitore,
 :ls_motivo_verifica,
 :ls_flag_tipo_vi,
 :ls_flag_mansionario_codificato,
 :ls_des_man_mansionario,
 :ls_obiettivi);
 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if
 
// ************************************************************************************************** 
// ****************************      inserisco i paragrafi iso       ********************************
// **************************************************************************************************

declare cu_paragrafi cursor for

select prog_paragrafo, flag_riferimento_primario 
from   det_piano_verifiche_iso
where  cod_azienda = :s_cs_xx.cod_azienda
and    anno_verifica = :li_anno_verifica
and    prog_verifica = :ll_prog_verifica
and    prog_det_verifica = :ll_prog_det_verifica;

open cu_paragrafi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore all'apertura del cursore paragrafi ISO: " + sqlca.sqlerrtext,stopsign!)
	close cu_paragrafi;
	rollback;
	return
end if

do while 1 = 1
	fetch cu_paragrafi into :ls_prog_paragrafo,
	                        :ls_flag_riferimento_primario;
	if sqlca.sqlcode = 100 then exit
	
	insert into tes_visite_ispettive_iso
	(cod_azienda,
	 anno_reg_visita,
	 num_reg_visita,
	 prog_paragrafo,
	 flag_riferimento_primario
	)
	values
	(:s_cs_xx.cod_azienda,
 	:li_anno_reg_visita,
 	:ll_num_reg_visita,
	:ls_prog_paragrafo,
	:ls_flag_riferimento_primario
	);
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore in inserimento paragrafi ISO: " + sqlca.sqlerrtext,stopsign!)
		close cu_paragrafi;
		rollback;
		return
	end if	
	
loop
close cu_paragrafi;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore nella chiusura del cursore paragrafi ISO: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if

// ************************************************************************************************** 
// ****************************      inserisco i dettagli       *************************************
// **************************************************************************************************

declare cu_dettagli cursor for

select prog_det_piano,
       flag_tipo_dettaglio,
		 flag_operaio_codificato,
		 cod_operaio,
		 cod_area_aziendale,
		 note,
       doc_compilato,
		 des_manuale_operaio
from   det_det_piano_verifiche
where  cod_azienda = :s_cs_xx.cod_azienda
and    anno_verifica = :li_anno_verifica
and    prog_verifica = :ll_prog_verifica
and    prog_det_verifica = :ll_prog_det_verifica;

open cu_dettagli;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore all'apertura del cursore dettagli piano di verifica: " + sqlca.sqlerrtext,stopsign!)
	close cu_dettagli;
	rollback;
	return
end if

do while 1 = 1
	fetch cu_dettagli into  :ll_prog_det_piano,
	                        :ls_flag_tipo_dettaglio,
									:ls_flag_operaio_codificato,
									:ls_cod_operaio,
									:ls_cod_area_aziendale,
									:ls_note,
									:ls_doc_compilato,
									:ls_des_manuale_operaio;
	if sqlca.sqlcode = 100 then exit
	
	insert into det_visite_ispettive
	(cod_azienda,
	 anno_reg_visita,
	 num_reg_visita,
	 progressivo,
	 flag_tipo_dettaglio,
	 flag_operaio_codificato,
	 cod_operaio,
	 cod_area_aziendale,
	 note,
	 doc_compilato,
	 des_manuale_operaio
	)
	values
	(:s_cs_xx.cod_azienda,
 	:li_anno_reg_visita,
 	:ll_num_reg_visita,
	:ll_prog_det_piano,
	:ls_flag_tipo_dettaglio,
	:ls_flag_operaio_codificato,
	:ls_cod_operaio,
	:ls_cod_area_aziendale,
	:ls_note,
	:ls_doc_compilato,
	:ls_des_manuale_operaio
	);
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore in inserimento dettagli visita ispettiva: " + sqlca.sqlerrtext,stopsign!)
		close cu_dettagli;
		rollback;
		return
	end if	
	
loop

close cu_dettagli;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore nella chiusura del cursore dettagli visita ispettiva: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if

// ************************************************************************************************** 

update det_piano_verifiche
set    flag_eseguita = 'S'   
where  cod_azienda = :s_cs_xx.cod_azienda
and    anno_verifica = :li_anno_verifica
and    prog_verifica = :ll_prog_verifica
and    prog_det_verifica = :ll_prog_det_verifica;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore nell'aggiornamento flag_eseguita : " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if
 
commit; 

g_mb.messagebox("Omnia","Creazione Verifica Ispettiva avvenuta con successo!",information!)
dw_det_piano_verifiche_lista.postevent("pcd_retrieve")
end event

