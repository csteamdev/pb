﻿$PBExportHeader$w_det_piano_verifiche_iso.srw
$PBExportComments$Finestra Associazione dei Paragrafi Iso a al piano verifiche
forward
global type w_det_piano_verifiche_iso from w_cs_xx_principale
end type
type dw_det_piano_verifiche_iso from uo_cs_xx_dw within w_det_piano_verifiche_iso
end type
end forward

global type w_det_piano_verifiche_iso from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2231
integer height = 1140
string title = "Riferimento Paragrafi Normativa"
dw_det_piano_verifiche_iso dw_det_piano_verifiche_iso
end type
global w_det_piano_verifiche_iso w_det_piano_verifiche_iso

type variables
boolean ib_in_new
end variables

event pc_setwindow;call super::pc_setwindow;dw_det_piano_verifiche_iso.set_dw_key("cod_azienda")
dw_det_piano_verifiche_iso.set_dw_key("anno_verifica")
dw_det_piano_verifiche_iso.set_dw_key("prog_verifica")
dw_det_piano_verifiche_iso.set_dw_key("prog_det_verifica")
dw_det_piano_verifiche_iso.set_dw_options(sqlca, i_openparm, c_default, c_default)

iuo_dw_main = dw_det_piano_verifiche_iso
dw_det_piano_verifiche_iso.ib_proteggi_chiavi = false


end event

on w_det_piano_verifiche_iso.create
int iCurrent
call super::create
this.dw_det_piano_verifiche_iso=create dw_det_piano_verifiche_iso
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_piano_verifiche_iso
end on

on w_det_piano_verifiche_iso.destroy
call super::destroy
destroy(this.dw_det_piano_verifiche_iso)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_piano_verifiche_iso,"prog_paragrafo",sqlca,&
                 "tab_paragrafi_iso","cod_paragrafo","des_paragrafo",&
                 "(tab_paragrafi_iso.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_paragrafi_iso.flag_blocco <> 'S') or (tab_paragrafi_iso.flag_blocco = 'S' and tab_paragrafi_iso.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

type dw_det_piano_verifiche_iso from uo_cs_xx_dw within w_det_piano_verifiche_iso
integer x = 23
integer y = 20
integer width = 2149
integer height = 1000
integer taborder = 20
string dataobject = "d_det_piano_verifiche_iso"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_anno_verifica, ll_prog_verifica, ll_prog_det_verifica, l_idx

ll_anno_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_verifica")
ll_prog_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_verifica")
ll_prog_det_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_det_verifica")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "anno_verifica")) or GetItemnumber(l_Idx, "anno_verifica") = 0 THEN
      SetItem(l_Idx, "anno_verifica", ll_anno_verifica)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "prog_verifica")) or GetItemnumber(l_Idx, "prog_verifica") = 0 THEN
      SetItem(l_Idx, "prog_verifica", ll_prog_verifica)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "prog_det_verifica")) or GetItemnumber(l_Idx, "prog_det_verifica") = 0 THEN
      SetItem(l_Idx, "prog_det_verifica", ll_prog_det_verifica)
   END IF
NEXT

end event

event updatestart;call super::updatestart;long ll_anno_verifica, ll_prog_verifica, ll_prog_det_verifica, ll_cont

ll_anno_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_verifica")
ll_prog_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_verifica")
ll_prog_det_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_det_verifica")

SELECT count(flag_riferimento_primario)  
INTO   :ll_cont  
FROM   det_piano_verifiche_iso
WHERE  ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
		 ( anno_verifica = :ll_anno_verifica ) AND  
		 ( prog_verifica = :ll_prog_verifica ) AND  
		 ( prog_det_verifica = :ll_prog_det_verifica ) AND  		 
		 ( flag_riferimento_primario = 'S' )   ;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante la verifica esistenza riferimento primario: " + sqlca.sqlerrtext, Information!)
end if		

if (isnull(ll_cont) or (ll_cont = 0)) and (getitemstring(this.getrow(),"flag_riferimento_primario") <> "S") then
	g_mb.messagebox("OMNIA", "Attenzione: per ogni verifica deve esistere un riferimento primario",StopSign!)
	pcca.error = c_valfailed		
end if   

if getitemstring(this.getrow(),"flag_riferimento_primario") = "S" then
	if ll_cont > 0 then
		g_mb.messagebox("OMNIA", "Attenzione: può esistere un solo riferimento primario",StopSign!)
		pcca.error = c_valfailed		
	end if   
else
	if ll_cont > 1 then
		g_mb.messagebox("OMNIA", "Attenzione: può esistere un solo riferimento primario",StopSign!)
		pcca.error = c_valfailed		
	end if   
end if
end event

event pcd_retrieve;call super::pcd_retrieve;long l_error, ll_anno_verifica, ll_prog_verifica, ll_prog_det_verifica

ll_anno_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_verifica")
ll_prog_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_verifica")
ll_prog_det_verifica = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_det_verifica")

l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno_verifica, ll_prog_verifica, ll_prog_det_verifica)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

