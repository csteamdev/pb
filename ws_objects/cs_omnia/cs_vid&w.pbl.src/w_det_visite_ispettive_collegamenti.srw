﻿$PBExportHeader$w_det_visite_ispettive_collegamenti.srw
$PBExportComments$Window collegamenti
forward
global type w_det_visite_ispettive_collegamenti from w_cs_xx_risposta
end type
type cb_chiudi from commandbutton within w_det_visite_ispettive_collegamenti
end type
type dw_det_visite_ispettive_lista_doc from uo_cs_xx_dw within w_det_visite_ispettive_collegamenti
end type
end forward

global type w_det_visite_ispettive_collegamenti from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 2642
integer height = 1140
string title = "Collegamento documenti"
cb_chiudi cb_chiudi
dw_det_visite_ispettive_lista_doc dw_det_visite_ispettive_lista_doc
end type
global w_det_visite_ispettive_collegamenti w_det_visite_ispettive_collegamenti

type variables
string is_documento, is_titolo
end variables

on w_det_visite_ispettive_collegamenti.create
int iCurrent
call super::create
this.cb_chiudi=create cb_chiudi
this.dw_det_visite_ispettive_lista_doc=create dw_det_visite_ispettive_lista_doc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_chiudi
this.Control[iCurrent+2]=this.dw_det_visite_ispettive_lista_doc
end on

on w_det_visite_ispettive_collegamenti.destroy
call super::destroy
destroy(this.cb_chiudi)
destroy(this.dw_det_visite_ispettive_lista_doc)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_enablepopup)

dw_det_visite_ispettive_lista_doc.set_dw_options(sqlca, &
                                                 pcca.null_object, &
                                                 c_nonew + c_nomodify + c_nodelete, &
                                                 c_default)
							  


end event

type cb_chiudi from commandbutton within w_det_visite_ispettive_collegamenti
integer x = 2217
integer y = 940
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;if dw_det_visite_ispettive_lista_doc.getrow() > 0 then
	s_cs_xx.parametri.parametro_s_1 = dw_det_visite_ispettive_lista_doc.getitemstring( dw_det_visite_ispettive_lista_doc.getrow(), "cf_documento")
	s_cs_xx.parametri.parametro_s_2 = dw_det_visite_ispettive_lista_doc.getitemstring( dw_det_visite_ispettive_lista_doc.getrow(), "nome_documento") + "   vers. " + string(dw_det_visite_ispettive_lista_doc.getitemnumber( dw_det_visite_ispettive_lista_doc.getrow(), "num_versione"))  + "  rev. " + string(dw_det_visite_ispettive_lista_doc.getitemnumber( dw_det_visite_ispettive_lista_doc.getrow(), "num_revisione"))
end if
close(parent)
end event

type dw_det_visite_ispettive_lista_doc from uo_cs_xx_dw within w_det_visite_ispettive_collegamenti
integer x = 23
integer y = 20
integer width = 2560
integer height = 900
integer taborder = 10
string dragicon = "C:\cs_70\framework\RISORSE\CS_SEP.ICO"
string dataobject = "d_det_visite_ispettive_lista_doc"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

