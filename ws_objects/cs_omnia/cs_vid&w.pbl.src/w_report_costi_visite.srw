﻿$PBExportHeader$w_report_costi_visite.srw
$PBExportComments$Finestra Report Costi Visita Ispettiva
forward
global type w_report_costi_visite from w_cs_xx_principale
end type
type dw_report_costi_visite_ispettive from uo_cs_xx_dw within w_report_costi_visite
end type
end forward

global type w_report_costi_visite from w_cs_xx_principale
integer width = 3607
integer height = 1588
string title = "Costi Visita Ispettiva"
boolean resizable = false
dw_report_costi_visite_ispettive dw_report_costi_visite_ispettive
end type
global w_report_costi_visite w_report_costi_visite

event pc_setwindow;call super::pc_setwindow;dw_report_costi_visite_ispettive.ib_dw_report = true

//dw_report_costi_visite_ispettive.set_dw_options(sqlca,pcca.null_object,c_disableCC+c_disableCCinsert,c_default)

set_w_options(c_NoResizeWin + c_noautosize)

dw_report_costi_visite_ispettive.set_dw_options(sqlca, &
                                               pcca.null_object, &
				                                   c_nomodify + &
            				                       c_nodelete + &
                        				           c_nonew + &
				                                   c_disableCC, &
            				                       c_noresizedw + &
                        				           c_nohighlightselected + &
				                                   c_nocursorrowpointer +&
            				                       c_nocursorrowfocusrect )

save_on_close(c_SOCNoSave)
end event

on w_report_costi_visite.create
int iCurrent
call super::create
this.dw_report_costi_visite_ispettive=create dw_report_costi_visite_ispettive
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_costi_visite_ispettive
end on

on w_report_costi_visite.destroy
call super::destroy
destroy(this.dw_report_costi_visite_ispettive)
end on

type dw_report_costi_visite_ispettive from uo_cs_xx_dw within w_report_costi_visite
integer x = 23
integer y = 20
integer width = 3543
integer height = 1460
string dataobject = "d_report_costi_visite_ispettive"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   s_cs_xx.parametri.parametro_i_1, &
                   s_cs_xx.parametri.parametro_d_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

