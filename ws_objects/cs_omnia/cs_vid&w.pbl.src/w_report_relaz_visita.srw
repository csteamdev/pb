﻿$PBExportHeader$w_report_relaz_visita.srw
$PBExportComments$Finestra Report Relazione Visita Ispettiva
forward
global type w_report_relaz_visita from w_cs_xx_principale
end type
type dw_report_visita_ispettiva from uo_cs_xx_dw within w_report_relaz_visita
end type
end forward

global type w_report_relaz_visita from w_cs_xx_principale
integer width = 3438
integer height = 2300
string title = "Relazione Visita Ispettiva"
dw_report_visita_ispettiva dw_report_visita_ispettiva
end type
global w_report_relaz_visita w_report_relaz_visita

event pc_setwindow;call super::pc_setwindow;string ls_path_logo

dw_report_visita_ispettiva.ib_dw_report = true

dw_report_visita_ispettiva.set_dw_options(SQLCA, &
                                          pcca.null_object, &
                                          c_disableCC + &
														c_nonew + &
														c_nomodify + &
														c_nodelete, &
														c_noresizedw + &
														c_nohighlightselected + &
														c_nocursorrowpointer + &
														c_nocursorrowfocusrect)

select stringa
into   :ls_path_logo
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
       flag_parametro = 'S' and &
       cod_parametro = 'LOA';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA","manca il parametro LOA in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)
end if

dw_report_visita_ispettiva.modify("logo.filename='" + s_cs_xx.volume + ls_path_logo + "'")

dw_report_visita_ispettiva.object.datawindow.print.preview = 'Yes'
dw_report_visita_ispettiva.object.datawindow.print.preview.rulers = 'Yes'
end event

on w_report_relaz_visita.create
int iCurrent
call super::create
this.dw_report_visita_ispettiva=create dw_report_visita_ispettiva
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_visita_ispettiva
end on

on w_report_relaz_visita.destroy
call super::destroy
destroy(this.dw_report_visita_ispettiva)
end on

type dw_report_visita_ispettiva from uo_cs_xx_dw within w_report_relaz_visita
integer x = 23
integer y = 20
integer width = 3360
integer height = 2160
string dataobject = "d_report_visite_ispettive"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_aziende_rag_soc_1, ls_aziende_rag_soc_2, ls_motivo_verifica, ls_flag_tipo_visita,&
		 ls_flag_mansionario_codificato, ls_des_mansionario, ls_mansionario_cognome, ls_mansionario_nome,&
		 ls_fornitori_rag_soc_1, ls_fornitori_rag_soc_2, ls_fornitori_indirizzo, ls_fornitori_localita,&
		 ls_fornitori_cap, ls_fornitori_provincia, ls_des_area, ls_des_area_testata, ls_conclusioni_generali, ls_strutt_organizzative,&
		 ls_proc_oper_gestionali, ls_risorse_umane, ls_apparecchi_materiali, ls_aree_lavoro_processi,&
		 ls_prodotti_conformita, ls_documentaz_sistema, ls_flag_tipo_dettaglio, ls_flag_operaio_codificato,&
		 ls_des_operaio, ls_note, ls_operaio, ls_doc_compilato, ls_etichetta, ls_obiettivi, ls_1, ls_2, ls_prog[], ls_des[]

long   ll_return, ll_i, ll_anno_reg_visita, ll_num_reg_visita, ll_anno_non_conf, ll_num_non_conf,&
		 ll_num_reg_lista, ll_prog_liste, ll_num_versione, ll_num_edizione, ll_riga

datetime ldt_data_visita, ldt_prossima_visita

integer  li_ii

datastore lds_visite


lds_visite = create datastore

lds_visite.dataobject = "d_ds_report_visite_ispettive"

lds_visite.settransobject(sqlca)

ll_return = lds_visite.retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_i_1, s_cs_xx.parametri.parametro_d_1)

if ll_return < 0 then
   g_mb.messagebox("OMNIA","Errore nella retrieve del datastore lds_visite")
	return -1
end if

dw_report_visita_ispettiva.setredraw(false)

ll_i = 1

// impostazione header e lettura dati per gli 8 riquadri
ls_aziende_rag_soc_1 = lds_visite.getitemstring(ll_i,"aziende_rag_soc_1")
ls_aziende_rag_soc_2 = lds_visite.getitemstring(ll_i,"aziende_rag_soc_2")
ll_anno_reg_visita = lds_visite.getitemnumber(ll_i,"tes_visite_ispettive_anno_reg_visita")
ll_num_reg_visita = lds_visite.getitemnumber(ll_i,"tes_visite_ispettive_num_reg_visita")
ldt_data_visita  = lds_visite.getitemdatetime(ll_i,"tes_visite_ispettive_data_visita")
ldt_prossima_visita  = lds_visite.getitemdatetime(ll_i,"tes_visite_ispettive_data_prossima_visit")
ls_motivo_verifica = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_motivo_verifica")
ll_anno_non_conf = lds_visite.getitemnumber(ll_i,"tes_visite_ispettive_anno_non_conf")
ll_num_non_conf = lds_visite.getitemnumber(ll_i,"tes_visite_ispettive_num_non_conf")
ls_flag_tipo_visita = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_flag_tipo_vi")
ls_flag_mansionario_codificato = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_flag_mansionario_co")
ls_des_mansionario = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_des_man_mansionari")
ls_mansionario_cognome = lds_visite.getitemstring(ll_i,"mansionari_cognome")
ls_mansionario_nome = lds_visite.getitemstring(ll_i,"mansionari_nome")
ls_fornitori_rag_soc_1 = lds_visite.getitemstring(ll_i,"anag_fornitori_rag_soc_1")
ls_fornitori_rag_soc_2 = lds_visite.getitemstring(ll_i,"anag_fornitori_rag_soc_2")
ls_fornitori_indirizzo = lds_visite.getitemstring(ll_i,"anag_fornitori_indirizzo")
ls_fornitori_cap = lds_visite.getitemstring(ll_i,"anag_fornitori_cap")
ls_fornitori_localita = lds_visite.getitemstring(ll_i,"anag_fornitori_localita")
ls_fornitori_provincia = lds_visite.getitemstring(ll_i,"anag_fornitori_provincia")
ls_des_area_testata = lds_visite.getitemstring(ll_i,"tab_aree_aziendali_des_area_1")
ls_conclusioni_generali = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_conclusioni_general")
ls_strutt_organizzative = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_val_strutt_organizz")
ls_proc_oper_gestionali = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_val_proc_oper_gesti")
ls_risorse_umane = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_val_risorse_umane")
ls_apparecchi_materiali = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_val_apparecchi_mate")
ls_aree_lavoro_processi = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_val_aree_lavoro_pro")
ls_prodotti_conformita = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_val_prodotti_confor")
ls_documentaz_sistema = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_val_documentaz_sist")
ls_obiettivi = lds_visite.getitemstring(ll_i, "tes_visite_ispettive_obiettivi")

if isnull(ls_aziende_rag_soc_2) then
	ls_aziende_rag_soc_2 = ""
end if	

dw_report_visita_ispettiva.object.azienda.text = ls_aziende_rag_soc_1 + " " + ls_aziende_rag_soc_2
dw_report_visita_ispettiva.object.data_visita.text = string(date(ldt_data_visita))
dw_report_visita_ispettiva.object.prossima_visita.text = string(date(ldt_prossima_visita))
dw_report_visita_ispettiva.object.area.text = ls_des_area_testata
dw_report_visita_ispettiva.object.registrazione.text = string(ll_num_reg_visita) + "/" + string(ll_anno_reg_visita)

if ls_flag_mansionario_codificato = 'S' then
	dw_report_visita_ispettiva.object.auditor.text = ls_mansionario_cognome + " " + ls_mansionario_nome
else
	dw_report_visita_ispettiva.object.auditor.text = ls_des_mansionario
end if	

choose case ls_flag_tipo_visita
	case "P"
		dw_report_visita_ispettiva.object.tipo_visita.text = "Prima parte"
	case "S"
		dw_report_visita_ispettiva.object.tipo_visita.text = "Seconda parte"
	case "T"
		dw_report_visita_ispettiva.object.tipo_visita.text = "Terza parte"
end choose

dw_report_visita_ispettiva.object.motivo.text = ls_motivo_verifica
dw_report_visita_ispettiva.object.origine.text = string(ll_num_non_conf) + "/" + string(ll_anno_non_conf)

if isnull(ls_fornitori_rag_soc_2) then
	ls_fornitori_rag_soc_2 = ""
end if	

dw_report_visita_ispettiva.object.rag_soc.text = ls_fornitori_rag_soc_1 + " " + ls_fornitori_rag_soc_2
dw_report_visita_ispettiva.object.indirizzo.text = ls_fornitori_indirizzo
dw_report_visita_ispettiva.object.cap.text = ls_fornitori_cap
dw_report_visita_ispettiva.object.localita.text = ls_fornitori_localita
dw_report_visita_ispettiva.object.provincia.text = ls_fornitori_provincia
dw_report_visita_ispettiva.object.obiettivi.text = ls_obiettivi

// carico i paragrafi iso di riferimento

declare cu_parag cursor for
select prog_paragrafo
from   tes_visite_ispettive_iso
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_reg_visita = :s_cs_xx.parametri.parametro_i_1 and
		 num_reg_visita = :s_cs_xx.parametri.parametro_d_1;
		 
open cu_parag;
li_ii = 1
do while 1 = 1 
	fetch cu_parag into :ls_prog[li_ii];
	
	if sqlca.sqlcode = 100 then exit
	
   select des_paragrafo
	into   :ls_des[li_ii]
	from   tab_paragrafi_iso
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_paragrafo = :ls_prog[li_ii];			 

	li_ii ++
loop
close cu_parag;

for li_ii = 1 to Upperbound(ls_des)
	ll_riga = dw_report_visita_ispettiva.insertrow(0)
	dw_report_visita_ispettiva.setitem(ll_riga, "flag_tipo_riga", "P")
	dw_report_visita_ispettiva.setitem(ll_riga, "prog_paragrafo", ls_prog[li_ii])
	dw_report_visita_ispettiva.setitem(ll_riga, "des_paragrafo", ls_des[li_ii])
next

// scorrimento righe datastore per i dettagli
ls_2 = ""
for ll_i = 1 to lds_visite.rowcount()
		
	// lettura valori ed insertrow
	ls_note = lds_visite.getitemstring(ll_i,"det_visite_ispettive_note")
	ls_operaio = lds_visite.getitemstring(ll_i,"cf_operaio")
	ls_des_operaio = lds_visite.getitemstring(ll_i,"det_visite_ispettive_des_manuale_operaio")
	ls_doc_compilato = lds_visite.getitemstring(ll_i,"det_visite_ispettive_doc_compilato")
	ls_des_area = lds_visite.getitemstring(ll_i,"tab_aree_aziendali_des_area")
	ls_des_mansionario = lds_visite.getitemstring(ll_i,"tes_visite_ispettive_des_man_mansionari")
	ll_num_reg_lista = lds_visite.getitemnumber(ll_i,"det_visite_ispettive_num_reg_lista")
	ll_prog_liste = lds_visite.getitemnumber(ll_i,"det_visite_ispettive_prog_liste_con_comp")
	ll_num_versione = lds_visite.getitemnumber(ll_i,"det_visite_ispettive_num_versione")
	ll_num_edizione = lds_visite.getitemnumber(ll_i,"det_visite_ispettive_num_edizione")
	ls_flag_tipo_dettaglio = lds_visite.getitemstring(ll_i,"det_visite_ispettive_flag_tipo_dettaglio")
	ls_flag_operaio_codificato = lds_visite.getitemstring(ll_i,"det_visite_ispettive_flag_operaio_codifi")
	
	ls_1 = ""
	if ( ls_flag_tipo_dettaglio = 'A' ) then
		ls_1 = "Aree Aziendali"
	elseif (  ls_flag_tipo_dettaglio = 'V' ) then
		ls_1 = "Valutatori"
	elseif ( ls_flag_tipo_dettaglio = 'C' ) then
	 	ls_1 = "Consultati"
	elseif ( ls_flag_tipo_dettaglio = 'D' ) then
		ls_1 = "Documentazione"
	end if
 
 	if ls_2 = "" or ls_2 <> ls_1 then 
		ls_2 = ls_1
		ll_riga = dw_report_visita_ispettiva.insertrow(0)
		dw_report_visita_ispettiva.setitem(ll_riga, "flag_tipo_riga", "E")		
		dw_report_visita_ispettiva.setitem(ll_riga, "flag_tipo_dettaglio", ls_flag_tipo_dettaglio)		
		dw_report_visita_ispettiva.setitem(ll_riga, "des_tipo_dettaglio", ls_1)		
	elseif ls_2 = ls_1 then
		ls_1 = ""
	end if

	ll_riga = dw_report_visita_ispettiva.insertrow(0)

	dw_report_visita_ispettiva.setitem(ll_riga, "flag_tipo_riga", "D")
	dw_report_visita_ispettiva.setitem(ll_riga, "des_tipo_dettaglio", ls_1)
	dw_report_visita_ispettiva.setitem(ll_riga, "note", ls_note)
	dw_report_visita_ispettiva.setitem(ll_riga, "operaio", ls_operaio)
	dw_report_visita_ispettiva.setitem(ll_riga, "des_operaio", ls_des_operaio)
	dw_report_visita_ispettiva.setitem(ll_riga, "doc_compilato", ls_doc_compilato)
	dw_report_visita_ispettiva.setitem(ll_riga, "des_area", ls_des_area)
	dw_report_visita_ispettiva.setitem(ll_riga, "des_mansionario", ls_des_mansionario)
	dw_report_visita_ispettiva.setitem(ll_riga, "num_reg_lista", ll_num_reg_lista)
	dw_report_visita_ispettiva.setitem(ll_riga, "prog_liste", ll_prog_liste)
	dw_report_visita_ispettiva.setitem(ll_riga, "num_versione", ll_num_versione)
	dw_report_visita_ispettiva.setitem(ll_riga, "num_edizione", ll_num_edizione)
	dw_report_visita_ispettiva.setitem(ll_riga, "flag_tipo_dettaglio", ls_flag_tipo_dettaglio)
	dw_report_visita_ispettiva.setitem(ll_riga, "flag_operaio_codificato", ls_flag_operaio_codificato)

next

// impostazione riquadri

// VALUTAZIONE DELLE AREE DI LAVORO IN PRODUZIONE
if len(trim(ls_aree_lavoro_processi)) > 0 and not isnull(ls_aree_lavoro_processi) then
	ll_riga = dw_report_visita_ispettiva.insertrow(0)
	ls_etichetta = "VALUTAZIONE DELLE AREE DI LAVORO IN PRODUZIONE"
	dw_report_visita_ispettiva.setitem(ll_riga,"etichetta_riquadro",ls_etichetta)
	dw_report_visita_ispettiva.setitem(ll_riga,"riquadro",ls_aree_lavoro_processi)
	dw_report_visita_ispettiva.setitem(ll_riga,"flag_tipo_dettaglio","R")
end if	

// VALUTAZIONE DELLE APPARECCHIATURE E DEI MATERIALI
if len(trim(ls_apparecchi_materiali)) > 0 and not isnull(ls_apparecchi_materiali) then
	ll_riga = dw_report_visita_ispettiva.insertrow(0)
	ls_etichetta = "VALUTAZIONE DELLE APPARECCHIATURE E DEI MATERIALI"
	dw_report_visita_ispettiva.setitem(ll_riga,"etichetta_riquadro",ls_etichetta)
	dw_report_visita_ispettiva.setitem(ll_riga,"riquadro",ls_apparecchi_materiali)
	dw_report_visita_ispettiva.setitem(ll_riga,"flag_tipo_dettaglio","R")
end if	

// VALUTAZIONE DELLA CONFORMITA' DEI PRODOTTI
if len(trim(ls_prodotti_conformita)) > 0 and not isnull(ls_prodotti_conformita) then
	ll_riga = dw_report_visita_ispettiva.insertrow(0)
	ls_etichetta = "VALUTAZIONE DELLA CONFORMITA' DEI PRODOTTI"
	dw_report_visita_ispettiva.setitem(ll_riga,"etichetta_riquadro",ls_etichetta)
	dw_report_visita_ispettiva.setitem(ll_riga,"riquadro",ls_prodotti_conformita)
	dw_report_visita_ispettiva.setitem(ll_riga,"flag_tipo_dettaglio","R")
end if	

// VALUTAZIONE DELLE RISORSE UMANE
if len(trim(ls_risorse_umane)) > 0 and not isnull(ls_risorse_umane) then
	ll_riga = dw_report_visita_ispettiva.insertrow(0)
	ls_etichetta = "VALUTAZIONE DELLE RISORSE UMANE"
	dw_report_visita_ispettiva.setitem(ll_riga,"etichetta_riquadro",ls_etichetta)
	dw_report_visita_ispettiva.setitem(ll_riga,"riquadro",ls_risorse_umane)
	dw_report_visita_ispettiva.setitem(ll_riga,"flag_tipo_dettaglio","R")
end if

// VALUTAZIONE DELLE PROCEDURE OPERATIVE GESTIONALI
if len(trim(ls_proc_oper_gestionali)) > 0 and not isnull(ls_proc_oper_gestionali) then
	ll_riga = dw_report_visita_ispettiva.insertrow(0)
	ls_etichetta = "VALUTAZIONE DELLE PROCEDURE OPERATIVE GESTIONALI"
	dw_report_visita_ispettiva.setitem(ll_riga,"etichetta_riquadro",ls_etichetta)
	dw_report_visita_ispettiva.setitem(ll_riga,"riquadro",ls_proc_oper_gestionali)
	dw_report_visita_ispettiva.setitem(ll_riga,"flag_tipo_dettaglio","R")
end if	

// VALUTAZIONE DELLE STRUTTURE ORGANIZZATIVE
if len(trim(ls_strutt_organizzative)) > 0 and not isnull(ls_strutt_organizzative) then
	ll_riga = dw_report_visita_ispettiva.insertrow(0)
	ls_etichetta = "VALUTAZIONE DELLE STRUTTURE ORGANIZZATIVE"
	dw_report_visita_ispettiva.setitem(ll_riga,"etichetta_riquadro",ls_etichetta)
	dw_report_visita_ispettiva.setitem(ll_riga,"riquadro",ls_strutt_organizzative)
	dw_report_visita_ispettiva.setitem(ll_riga,"flag_tipo_dettaglio","R")
end if	

// VALUTAZIONE DELLA DOCUMENTAZIONE DI SISTEMA
if len(trim(ls_documentaz_sistema)) > 0 and not isnull(ls_documentaz_sistema) then
	ll_riga = dw_report_visita_ispettiva.insertrow(0)
	ls_etichetta = "VALUTAZIONE DELLA DOCUMENTAZIONE DI SISTEMA"
	dw_report_visita_ispettiva.setitem(ll_riga,"etichetta_riquadro",ls_etichetta)
	dw_report_visita_ispettiva.setitem(ll_riga,"riquadro",ls_documentaz_sistema)
	dw_report_visita_ispettiva.setitem(ll_riga,"flag_tipo_dettaglio","R")
end if	

// CONCLUSIONI GENERALI
if len(trim(ls_conclusioni_generali)) > 0 and not isnull(ls_conclusioni_generali) then
	ll_riga = dw_report_visita_ispettiva.insertrow(0)
	ls_etichetta = "CONCLUSIONI GENERALI"
	dw_report_visita_ispettiva.setitem(ll_riga,"etichetta_riquadro",ls_etichetta)
	dw_report_visita_ispettiva.setitem(ll_riga,"riquadro",ls_conclusioni_generali)
	dw_report_visita_ispettiva.setitem(ll_riga,"flag_tipo_dettaglio","R")
end if	

dw_report_visita_ispettiva.setredraw(true)
dw_report_visita_ispettiva.resetupdate()

destroy lds_visite
end event

