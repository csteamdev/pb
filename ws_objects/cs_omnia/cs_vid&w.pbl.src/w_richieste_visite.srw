﻿$PBExportHeader$w_richieste_visite.srw
$PBExportComments$Finestra Gestione Richieste Visite Ispettive
forward
global type w_richieste_visite from w_richieste
end type
type cb_visite_isp from commandbutton within w_richieste_visite
end type
end forward

global type w_richieste_visite from w_richieste
integer width = 2331
integer height = 1900
string title = "Rich. Visite Isp."
cb_visite_isp cb_visite_isp
end type
global w_richieste_visite w_richieste_visite

type variables
boolean ib_in_new=FALSE

end variables

forward prototypes
public function integer wf_tipo_richiesta (string ws_tipo_richiesta)
end prototypes

public function integer wf_tipo_richiesta (string ws_tipo_richiesta);dw_richieste_det.Modify("cod_tipo_manutenzione_t.Visible=0")
dw_richieste_det.Modify("cf_cod_tipo_manutenzione.Visible=0")
dw_richieste_det.Modify("cod_tipo_manutenzione.Visible=0")
dw_richieste_det.Modify("cod_attrezzatura_t.Visible=0")
dw_richieste_det.Modify("cf_cod_attrezzatura.Visible=0")
dw_richieste_det.Modify("cod_attrezzatura.Visible=0")

dw_richieste_det.object.b_ricerca_att.visible = false

dw_richieste_det.Modify("ret_1.Visible=0")

dw_richieste_det.Modify("cod_intervento_t.Visible=0")
dw_richieste_det.Modify("cf_cod_intervento.Visible=0")
dw_richieste_det.Modify("cod_intervento.Visible=0")
dw_richieste_det.Modify("num_reg_intervento_t.Visible=0")
dw_richieste_det.Modify("num_reg_intervento.Visible=0")
dw_richieste_det.Modify("anno_reg_intervento_t.Visible=0")
dw_richieste_det.Modify("anno_reg_intervento.Visible=0")
dw_richieste_det.Modify("ret_3.Visible=0")

dw_richieste_det.Modify("cod_intervento.Visible=0")
dw_richieste_det.Modify("num_reg_visita_t.Visible=0")
dw_richieste_det.Modify("num_reg_visita.Visible=0")
dw_richieste_det.Modify("anno_reg_visita_t.Visible=0")
dw_richieste_det.Modify("anno_reg_visita.Visible=0")
dw_richieste_det.Modify("ret_2.Visible=0")



choose case ws_tipo_richiesta
case "M"
   dw_richieste_det.Modify("cod_tipo_manutenzione_t.Visible=1")
   dw_richieste_det.Modify("cf_cod_tipo_manutenzione.Visible=1")
   dw_richieste_det.Modify("cod_tipo_manutenzione.Visible=1")
   dw_richieste_det.Modify("cod_attrezzatura_t.Visible=1")
   dw_richieste_det.Modify("cf_attrezzatura.Visible=1")
   dw_richieste_det.Modify("cod_attrezzatura.Visible=1")
	
	dw_richieste_det.object.b_ricerca_att.visible = true
	
   dw_richieste_det.Modify("ret_1.Visible=1")
	
	
case "I"
   dw_richieste_det.Modify("cod_intervento_t.Visible=1")
   dw_richieste_det.Modify("cf_cod_intervento.Visible=1")
   dw_richieste_det.Modify("cod_intervento.Visible=1")
   dw_richieste_det.Modify("num_reg_intervento_t.Visible=1")
   dw_richieste_det.Modify("num_reg_intervento.Visible=1")
   dw_richieste_det.Modify("anno_reg_intervento_t.Visible=1")
   dw_richieste_det.Modify("anno_reg_intervento.Visible=1")
   dw_richieste_det.Modify("ret_3.Visible=1")
	
	
case "V"
//   dw_richieste_det.Modify("cod_intervento.Visible=1")
   dw_richieste_det.Modify("num_reg_visita_t.Visible=1")
   dw_richieste_det.Modify("num_reg_visita.Visible=1")
   dw_richieste_det.Modify("anno_reg_visita_t.Visible=1")
   dw_richieste_det.Modify("anno_reg_visita.Visible=1")
   dw_richieste_det.Modify("ret_2.Visible=1")
end choose

return 0

end function

on pc_setwindow;call w_richieste::pc_setwindow;dw_richieste_lista.set_dw_key("cod_azienda")
dw_richieste_lista.set_dw_key("flag_tipo_richiesta")
if (s_cs_xx.parametri.parametro_s_1 = "VISITA") and &
   (s_cs_xx.parametri.parametro_d_1 > 0) and &
   (s_cs_xx.parametri.parametro_d_2 > 0)  then
      dw_richieste_lista.set_dw_options(sqlca,pcca.null_object,c_newonopen,c_default)
else
   dw_richieste_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
end if
dw_richieste_det.set_dw_options(sqlca,dw_richieste_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_richieste_lista

wf_tipo_richiesta("V")
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_richieste_det,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//inserito pulsante ricerca attrezzatura
//f_PO_LoadDDDW_DW(dw_richieste_det,"cod_attrezzatura",sqlca,&
//                 "anag_attrezzature","cod_attrezzatura","descrizione",&
//                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_richieste_det,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_richieste_det,"cod_intervento",sqlca,&
                 "tab_interventi","cod_intervento","des_intervento",&
                 "tab_interventi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_richieste_det,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on w_richieste_visite.create
int iCurrent
call super::create
this.cb_visite_isp=create cb_visite_isp
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_visite_isp
end on

on w_richieste_visite.destroy
call super::destroy
destroy(this.cb_visite_isp)
end on

type dw_richieste_lista from w_richieste`dw_richieste_lista within w_richieste_visite
integer width = 2263
end type

on dw_richieste_lista::pcd_view;call w_richieste`dw_richieste_lista::pcd_view;ib_in_new=FALSE
cb_visite_isp.enabled = true


end on

on dw_richieste_lista::pcd_new;call w_richieste`dw_richieste_lista::pcd_new;if (s_cs_xx.parametri.parametro_s_1 = "VISITA") and &
   (s_cs_xx.parametri.parametro_d_1 > 0) and &
   (s_cs_xx.parametri.parametro_d_2 > 0)  then
       this.setitem(this.getrow(),"anno_non_conf", s_cs_xx.parametri.parametro_d_1)
       this.setitem(this.getrow(),"num_non_conf", s_cs_xx.parametri.parametro_d_2)
end if
ib_in_new=true
this.SetItem(this.getrow(), "flag_tipo_richiesta", "V")
this.SetItem(this.getrow(), "data_richiesta", today())
this.SetItem(this.getrow(), "data_scadenza", relativedate(today(), 7))
cb_visite_isp.enabled = false
end on

on dw_richieste_lista::pcd_setkey;call w_richieste`dw_richieste_lista::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "flag_tipo_richiesta")) THEN
      SetItem(l_Idx, "flag_tipo_richiesta", "V")
   END IF
NEXT

end on

on dw_richieste_lista::pcd_retrieve;call w_richieste`dw_richieste_lista::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, "V")

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on dw_richieste_lista::pcd_savebefore;call w_richieste`dw_richieste_lista::pcd_savebefore;setnull(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_d_1 = 0
s_cs_xx.parametri.parametro_d_2 = 0

end on

event dw_richieste_lista::updatestart;call super::updatestart;if ib_in_new then

   long ll_anno,ll_num_registrazione, ll_riga

   dw_richieste_det.SetItem (dw_richieste_det.GetRow ( ),"anno_reg_richiesta", year(today()) )
   ll_anno = dw_richieste_det.GetItemNumber(dw_richieste_det.GetRow ( ), "anno_reg_richiesta" )
   select max(richieste.num_reg_richiesta)
     into :ll_num_registrazione
     from richieste
     where (richieste.cod_azienda = :s_cs_xx.cod_azienda) and (:ll_anno = richieste.anno_reg_richiesta);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   dw_richieste_det.SetItem (dw_richieste_det.GetRow(),"num_reg_richiesta", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      dw_richieste_det.SetItem (dw_richieste_det.GetRow(), "num_reg_richiesta", 1)
   end if
   ib_in_new = false
end if

end event

on dw_richieste_lista::pcd_modify;call w_richieste`dw_richieste_lista::pcd_modify;ib_in_new=FALSE
cb_visite_isp.enabled = false

end on

type dw_richieste_det from w_richieste`dw_richieste_det within w_richieste_visite
integer width = 2263
integer height = 1144
end type

event dw_richieste_det::buttonclicked;call super::buttonclicked;

if row>0 then
else
	return
end if

choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_richieste_det, "cod_attrezzatura")
		
end choose

end event

type cb_visite_isp from commandbutton within w_richieste_visite
integer x = 1911
integer y = 1704
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Visite Isp."
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "RICHIESTA"
s_cs_xx.parametri.parametro_i_1 = dw_richieste_lista.getitemnumber(dw_richieste_lista.getrow(),"anno_reg_richiesta")
s_cs_xx.parametri.parametro_i_2 = dw_richieste_lista.getitemnumber(dw_richieste_lista.getrow(),"anno_non_conf")
s_cs_xx.parametri.parametro_d_1 = dw_richieste_lista.getitemnumber(dw_richieste_lista.getrow(),"num_reg_richiesta")
s_cs_xx.parametri.parametro_d_2 = dw_richieste_lista.getitemnumber(dw_richieste_lista.getrow(),"num_non_conf")
window_open(w_tes_visite_ispettive, -1)
end event

