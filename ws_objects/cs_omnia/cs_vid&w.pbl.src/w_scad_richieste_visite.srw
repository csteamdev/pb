﻿$PBExportHeader$w_scad_richieste_visite.srw
$PBExportComments$Finestra Scsdenzario Richieste Manutenzione
forward
global type w_scad_richieste_visite from w_cs_xx_principale
end type
type cb_aggiorna from commandbutton within w_scad_richieste_visite
end type
type em_data_inizio from editmask within w_scad_richieste_visite
end type
type em_data_fine from editmask within w_scad_richieste_visite
end type
type st_1 from statictext within w_scad_richieste_visite
end type
type st_2 from statictext within w_scad_richieste_visite
end type
type cb_data from commandbutton within w_scad_richieste_visite
end type
type cb_emesso_il from commandbutton within w_scad_richieste_visite
end type
type cb_nome_documento from commandbutton within w_scad_richieste_visite
end type
type cb_progr from commandbutton within w_scad_richieste_visite
end type
type cb_scadenza from commandbutton within w_scad_richieste_visite
end type
type cb_valido_per from commandbutton within w_scad_richieste_visite
end type
type dw_scad_richieste_visite from uo_cs_xx_dw within w_scad_richieste_visite
end type
type dw_folder from u_folder within w_scad_richieste_visite
end type
end forward

global type w_scad_richieste_visite from w_cs_xx_principale
integer width = 4626
integer height = 1716
string title = "Scadenzario Richieste Visite"
cb_aggiorna cb_aggiorna
em_data_inizio em_data_inizio
em_data_fine em_data_fine
st_1 st_1
st_2 st_2
cb_data cb_data
cb_emesso_il cb_emesso_il
cb_nome_documento cb_nome_documento
cb_progr cb_progr
cb_scadenza cb_scadenza
cb_valido_per cb_valido_per
dw_scad_richieste_visite dw_scad_richieste_visite
dw_folder dw_folder
end type
global w_scad_richieste_visite w_scad_richieste_visite

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

dw_scad_richieste_visite.ib_dw_report=true

dw_scad_richieste_visite.set_dw_options(sqlca,pcca.null_object,c_nonew+c_nodelete+c_nomodify+c_disableCC,c_default)

iuo_dw_main = dw_scad_richieste_visite

l_objects[1] = dw_scad_richieste_visite
l_objects[2] = cb_emesso_il
l_objects[3] = cb_nome_documento
l_objects[4] = cb_progr
l_objects[5] = cb_scadenza
l_objects[6] = cb_valido_per
l_objects[7] = cb_data
dw_folder.fu_AssignTab(1, "&Scadenzario", l_Objects[])

dw_folder.fu_FolderCreate(1,4)

dw_folder.fu_SelectTab(1)

em_data_inizio.text = string(relativedate(today(),-30),"dd/mm/yyyy")
em_data_fine.text = string(today(),"dd/mm/yyyy")
end event

on w_scad_richieste_visite.create
int iCurrent
call super::create
this.cb_aggiorna=create cb_aggiorna
this.em_data_inizio=create em_data_inizio
this.em_data_fine=create em_data_fine
this.st_1=create st_1
this.st_2=create st_2
this.cb_data=create cb_data
this.cb_emesso_il=create cb_emesso_il
this.cb_nome_documento=create cb_nome_documento
this.cb_progr=create cb_progr
this.cb_scadenza=create cb_scadenza
this.cb_valido_per=create cb_valido_per
this.dw_scad_richieste_visite=create dw_scad_richieste_visite
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_aggiorna
this.Control[iCurrent+2]=this.em_data_inizio
this.Control[iCurrent+3]=this.em_data_fine
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.cb_data
this.Control[iCurrent+7]=this.cb_emesso_il
this.Control[iCurrent+8]=this.cb_nome_documento
this.Control[iCurrent+9]=this.cb_progr
this.Control[iCurrent+10]=this.cb_scadenza
this.Control[iCurrent+11]=this.cb_valido_per
this.Control[iCurrent+12]=this.dw_scad_richieste_visite
this.Control[iCurrent+13]=this.dw_folder
end on

on w_scad_richieste_visite.destroy
call super::destroy
destroy(this.cb_aggiorna)
destroy(this.em_data_inizio)
destroy(this.em_data_fine)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.cb_data)
destroy(this.cb_emesso_il)
destroy(this.cb_nome_documento)
destroy(this.cb_progr)
destroy(this.cb_scadenza)
destroy(this.cb_valido_per)
destroy(this.dw_scad_richieste_visite)
destroy(this.dw_folder)
end on

type cb_aggiorna from commandbutton within w_scad_richieste_visite
integer x = 2039
integer y = 20
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;dw_scad_richieste_visite.postevent("pcd_retrieve")
end event

type em_data_inizio from editmask within w_scad_richieste_visite
integer x = 594
integer y = 20
integer width = 411
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
double increment = 1
string minmax = "~~"
boolean dropdowncalendar = true
end type

event modified;cb_aggiorna.event clicked()
end event

type em_data_fine from editmask within w_scad_richieste_visite
integer x = 1577
integer y = 20
integer width = 411
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
double increment = 1
string minmax = "~~"
boolean dropdowncalendar = true
end type

event modified;cb_aggiorna.event clicked()
end event

type st_1 from statictext within w_scad_richieste_visite
integer x = 46
integer y = 20
integer width = 549
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Data Inizio Controllo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_scad_richieste_visite
integer x = 1051
integer y = 20
integer width = 526
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Data Fine Controllo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_data from commandbutton within w_scad_richieste_visite
integer x = 777
integer y = 240
integer width = 1029
integer height = 80
integer taborder = 51
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Descrizione Richiesta"
end type

event clicked;dw_scad_richieste_visite.setsort("des_richiesta A")
dw_scad_richieste_visite.sort()
end event

type cb_emesso_il from commandbutton within w_scad_richieste_visite
integer x = 411
integer y = 240
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Richiesta"
end type

event clicked;dw_scad_richieste_visite.setsort("num_reg_richiesta A")
dw_scad_richieste_visite.sort()
end event

type cb_nome_documento from commandbutton within w_scad_richieste_visite
integer x = 2583
integer y = 240
integer width = 1797
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Richiedente"
end type

event clicked;dw_scad_richieste_visite.setsort("cod_operaio A")
dw_scad_richieste_visite.sort()
end event

type cb_progr from commandbutton within w_scad_richieste_visite
integer x = 2194
integer y = 240
integer width = 389
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Scadenza"
end type

event clicked;dw_scad_richieste_visite.setsort("data_scadenza D")
dw_scad_richieste_visite.sort()
end event

type cb_scadenza from commandbutton within w_scad_richieste_visite
integer x = 69
integer y = 240
integer width = 343
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Anno"
end type

event clicked;dw_scad_richieste_visite.setsort("anno_reg_richiesta A")
dw_scad_richieste_visite.sort()
end event

type cb_valido_per from commandbutton within w_scad_richieste_visite
integer x = 1806
integer y = 240
integer width = 389
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Data"
end type

event clicked;dw_scad_richieste_visite.setsort("data_richiesta A")
dw_scad_richieste_visite.sort()
end event

type dw_scad_richieste_visite from uo_cs_xx_dw within w_scad_richieste_visite
integer x = 69
integer y = 320
integer width = 4457
integer height = 1236
integer taborder = 0
string dataobject = "d_scad_richieste_visite"
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;//long ll_progr
//
//if (i_extendmode) and (getrow() > 0) then
//ll_progr = this.getitemnumber(this.getrow(),"progr")
//
//
//SELECT doc_generici.progr  
//INTO   :ll_progr  
//FROM   doc_generici  
//WHERE  (doc_generici.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       (doc_generici.progr = :ll_progr )   ;
//if sqlca.sqlcode = 0 then
//   if not isvalid(w_fold_generici) then
//      window_open(w_fold_generici, -1)
//      return
//   end if
//end if
//
//
//SELECT contratti.progr  
//INTO   :ll_progr  
//FROM   contratti
//WHERE  (contratti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       (contratti.progr = :ll_progr )   ;
//if sqlca.sqlcode = 0 then
//   if not isvalid(w_fold_contratti) then
//      window_open(w_fold_contratti, -1)
//   end if
//end if
//end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,datetime(date(em_data_inizio.text)),datetime(date(em_data_fine.text)))

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_folder from u_folder within w_scad_richieste_visite
integer x = 23
integer y = 120
integer width = 4539
integer height = 1460
integer taborder = 10
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "&Scadenzario"
      SetFocus(dw_scad_richieste_visite)
END CHOOSE

end event

