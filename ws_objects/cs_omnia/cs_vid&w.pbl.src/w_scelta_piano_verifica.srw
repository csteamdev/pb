﻿$PBExportHeader$w_scelta_piano_verifica.srw
$PBExportComments$Window w_scelta_piano_verifica
forward
global type w_scelta_piano_verifica from w_cs_xx_risposta
end type
type dw_tes_piano_verifiche from uo_cs_xx_dw within w_scelta_piano_verifica
end type
type cb_ok from commandbutton within w_scelta_piano_verifica
end type
type cb_chiudi from commandbutton within w_scelta_piano_verifica
end type
end forward

global type w_scelta_piano_verifica from w_cs_xx_risposta
int Width=2323
int Height=1805
dw_tes_piano_verifiche dw_tes_piano_verifiche
cb_ok cb_ok
cb_chiudi cb_chiudi
end type
global w_scelta_piano_verifica w_scelta_piano_verifica

on w_scelta_piano_verifica.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_tes_piano_verifiche=create dw_tes_piano_verifiche
this.cb_ok=create cb_ok
this.cb_chiudi=create cb_chiudi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tes_piano_verifiche
this.Control[iCurrent+2]=cb_ok
this.Control[iCurrent+3]=cb_chiudi
end on

on w_scelta_piano_verifica.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_tes_piano_verifiche)
destroy(this.cb_ok)
destroy(this.cb_chiudi)
end on

event pc_setwindow;call super::pc_setwindow;
dw_tes_piano_verifiche.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)


end event

type dw_tes_piano_verifiche from uo_cs_xx_dw within w_scelta_piano_verifica
int X=23
int Y=21
int Width=2241
int Height=1561
int TabOrder=10
string DataObject="d_tes_piano_verifiche"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type cb_ok from commandbutton within w_scelta_piano_verifica
int X=1509
int Y=1601
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;integer  li_anno_reg_visita,li_anno_verifica
long     ll_num_reg_visita,ll_prog_verifica
string   ls_cod_fornitore,ls_cod_area_aziendale,ls_cod_resp_divisione,ls_motivo_verifica,ls_flag_tipo_vi, & 
		   ls_flag_mansionario_codificato,ls_des_man_mansionario
datetime ldt_oggi

li_anno_verifica= dw_tes_piano_verifiche.getitemnumber(dw_tes_piano_verifiche.getrow(),"anno_verifica")
ll_prog_verifica= dw_tes_piano_verifiche.getitemnumber(dw_tes_piano_verifiche.getrow(),"prog_verifica")

ldt_oggi = datetime(today())

select cod_fornitore,
		 cod_area_aziendale,
		 cod_resp_divisione,
		 motivo_verifica,
		 flag_tipo_vi,
		 flag_mansionario_codificato,
		 des_man_mansionario
into   :ls_cod_fornitore,
 		 :ls_cod_area_aziendale,
		 :ls_cod_resp_divisione,
		 :ls_motivo_verifica,
		 :ls_flag_tipo_vi,
		 :ls_flag_mansionario_codificato,
		 :ls_des_man_mansionario
from   det_piano_verifiche
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_verifica=:li_anno_verifica
and    prog_verifica=:ll_prog_verifica;


if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

li_anno_reg_visita = f_anno_esercizio()

select max(num_reg_visita)
into   :ll_num_reg_visita
from   tes_visite_ispettive
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_reg_visita=:li_anno_reg_visita;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

if isnull(ll_num_reg_visita) or ll_num_reg_visita=0 then
	ll_num_reg_visita = 1
else
	ll_num_reg_visita++
end if

insert into tes_visiste_ispettive
(cod_azienda,
anno_reg_visita,
num_reg_visita,
cod_resp_divisione
data_visita,
cod_area_aziendale,
cod_fornitore,
motivo_verifica,
flag_tipo_vi,
flag_mansionario_codificato,
des_man_mansionario)
values
(:s_cs_xx.cod_azienda,
 :li_anno_reg_visita,
 :ll_num_reg_visita,
 :ls_cod_resp_divisione,
 :ldt_oggi,
 :ls_cod_area_aziendale,
 :ls_cod_fornitore,
 :ls_flag_tipo_vi,
 :ls_flag_mansionario_codificato,
 :ls_des_man_mansionario);
 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if
 
g_mb.messagebox("Omnia","Creazione Verifica Ispettiva avvenuta con successo!",information!)

end event

type cb_chiudi from commandbutton within w_scelta_piano_verifica
int X=1898
int Y=1601
int Width=366
int Height=81
int TabOrder=3
boolean BringToTop=true
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;close(parent)
end event

