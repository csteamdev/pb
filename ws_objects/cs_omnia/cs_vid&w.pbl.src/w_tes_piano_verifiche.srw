﻿$PBExportHeader$w_tes_piano_verifiche.srw
$PBExportComments$Window tes_piano_verifiche
forward
global type w_tes_piano_verifiche from w_cs_xx_principale
end type
type cb_ricerca from commandbutton within w_tes_piano_verifiche
end type
type cb_reset from commandbutton within w_tes_piano_verifiche
end type
type cb_dettagli from commandbutton within w_tes_piano_verifiche
end type
type dw_tes_piano_verifiche from uo_cs_xx_dw within w_tes_piano_verifiche
end type
type dw_folder_search from u_folder within w_tes_piano_verifiche
end type
type dw_ricerca from u_dw_search within w_tes_piano_verifiche
end type
end forward

global type w_tes_piano_verifiche from w_cs_xx_principale
integer width = 2921
integer height = 856
string title = "Piano Annuale delle Verifiche"
cb_ricerca cb_ricerca
cb_reset cb_reset
cb_dettagli cb_dettagli
dw_tes_piano_verifiche dw_tes_piano_verifiche
dw_folder_search dw_folder_search
dw_ricerca dw_ricerca
end type
global w_tes_piano_verifiche w_tes_piano_verifiche

type variables

end variables

event pc_setwindow;call super::pc_setwindow;string       l_searchcolumn[], l_criteriacolumn[], l_searchtable[]
windowobject l_objects[ ]


dw_tes_piano_verifiche.set_dw_key("cod_azienda")
dw_tes_piano_verifiche.set_dw_key("anno_verifica")
dw_tes_piano_verifiche.set_dw_key("num_verifica")
dw_tes_piano_verifiche.set_dw_options(sqlca, &
                                      pcca.null_object, &
												  c_noretrieveonopen, &
												  c_default)

iuo_dw_main = dw_tes_piano_verifiche

l_criteriacolumn[1] = "anno_piano_verifica"
l_criteriacolumn[2] = "prog_piano_verifica"
l_criteriacolumn[3] = "flag_eseguita"

l_searchtable[1] = "tes_piano_verifiche"
l_searchtable[2] = "tes_piano_verifiche"
l_searchtable[3] = "tes_piano_verifiche"

l_searchcolumn[1] = "anno_verifica"
l_searchcolumn[2] = "prog_verifica"
l_searchcolumn[3] = "flag_eseguita"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_tes_piano_verifiche, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)
							
dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)
											 
l_objects[1] = dw_tes_piano_verifiche
l_objects[2] = cb_dettagli
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])

l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)
dw_tes_piano_verifiche.change_dw_current()



end event

on w_tes_piano_verifiche.create
int iCurrent
call super::create
this.cb_ricerca=create cb_ricerca
this.cb_reset=create cb_reset
this.cb_dettagli=create cb_dettagli
this.dw_tes_piano_verifiche=create dw_tes_piano_verifiche
this.dw_folder_search=create dw_folder_search
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ricerca
this.Control[iCurrent+2]=this.cb_reset
this.Control[iCurrent+3]=this.cb_dettagli
this.Control[iCurrent+4]=this.dw_tes_piano_verifiche
this.Control[iCurrent+5]=this.dw_folder_search
this.Control[iCurrent+6]=this.dw_ricerca
end on

on w_tes_piano_verifiche.destroy
call super::destroy
destroy(this.cb_ricerca)
destroy(this.cb_reset)
destroy(this.cb_dettagli)
destroy(this.dw_tes_piano_verifiche)
destroy(this.dw_folder_search)
destroy(this.dw_ricerca)
end on

type cb_ricerca from commandbutton within w_tes_piano_verifiche
integer x = 2025
integer y = 508
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_null

setnull(ls_null)
if dw_ricerca.getitemstring( 1, "flag_eseguita") = "I" then
	dw_ricerca.setitem(1, "flag_eseguita", ls_null)
end if

dw_ricerca.fu_buildsearch(TRUE)
dw_folder_search.fu_selecttab(1)
dw_tes_piano_verifiche.change_dw_current()
parent.triggerevent("pc_retrieve")




end event

type cb_reset from commandbutton within w_tes_piano_verifiche
integer x = 2414
integer y = 508
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;dw_ricerca.fu_reset()
end event

type cb_dettagli from commandbutton within w_tes_piano_verifiche
integer x = 2418
integer y = 648
integer width = 384
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dati Generali"
end type

event clicked;window_open_parm(w_det_piano_verifiche, -1, dw_tes_piano_verifiche)
end event

type dw_tes_piano_verifiche from uo_cs_xx_dw within w_tes_piano_verifiche
integer x = 183
integer y = 64
integer width = 2629
integer height = 560
integer taborder = 10
string dataobject = "d_tes_piano_verifiche"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,ll_progressivo
integer li_anno

li_anno=f_anno_esercizio()

select max(prog_verifica)
into   :ll_progressivo
from   tes_piano_verifiche
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_verifica=:li_anno;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return
end if

if isnull(ll_progressivo) or ll_progressivo = 0 then
	ll_progressivo = 1
else
	ll_progressivo++
end if
		
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
      SetItem(l_Idx, "anno_verifica", li_anno)
	   SetItem(l_Idx, "prog_verifica", ll_progressivo)
   END IF
NEXT

end event

event updatestart;call super::updatestart;integer li_i
long    ll_anno_verifica, ll_prog_verifica

for li_i = 1 to this.deletedcount()
	ll_anno_verifica = this.getitemnumber(li_i, "anno_verifica", delete!, true)
	ll_prog_verifica = this.getitemnumber(li_i, "prog_verifica", delete!, true)
	
	// devo cancellare tutti i paragrafi
	
	delete from det_piano_verifiche_iso
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_verifica = :ll_anno_verifica and
			prog_verifica = :ll_prog_verifica;
				
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione dei paragrafi ISO: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	// devo cancellare tutti i dettagli
	
	delete from det_det_piano_verifiche
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_verifica = :ll_anno_verifica and
			prog_verifica = :ll_prog_verifica;
			
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione dei dettagli: " + sqlca.sqlerrtext)
		rollback;
		return
	end if			
	
	// devo cancellare tutti i dettagli
	
	delete from det_piano_verifiche
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_verifica = :ll_anno_verifica and
			prog_verifica = :ll_prog_verifica;
				
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione della riga: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	// cancello il piano
	
	delete from tes_piano_verifiche
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_verifica = :ll_anno_verifica and
			prog_verifica = :ll_prog_verifica;
				
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione della riga: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
			
next	
end event

type dw_folder_search from u_folder within w_tes_piano_verifiche
integer x = 23
integer y = 20
integer width = 2857
integer height = 720
integer taborder = 10
end type

type dw_ricerca from u_dw_search within w_tes_piano_verifiche
event ue_key pbm_dwnkey
integer x = 183
integer y = 64
integer width = 2629
integer height = 576
integer taborder = 20
string dataobject = "d_ricerca_tes_piano_verifiche"
end type

