﻿$PBExportHeader$w_tes_visite_ispettive.srw
$PBExportComments$Finestra Testata Visite Ispettive
forward
global type w_tes_visite_ispettive from w_cs_xx_principale
end type
type st_filtro_data from statictext within w_tes_visite_ispettive
end type
type sle_filtro_data_registrazione from u_sle_search within w_tes_visite_ispettive
end type
type cb_reset from commandbutton within w_tes_visite_ispettive
end type
type cb_ricerca from commandbutton within w_tes_visite_ispettive
end type
type cb_note_esterne from commandbutton within w_tes_visite_ispettive
end type
type cb_paragrafi_iso from commandbutton within w_tes_visite_ispettive
end type
type cb_relazione from commandbutton within w_tes_visite_ispettive
end type
type cb_dettagli from commandbutton within w_tes_visite_ispettive
end type
type cb_report_visita from commandbutton within w_tes_visite_ispettive
end type
type cb_procedure from cb_apri_manuale within w_tes_visite_ispettive
end type
type cb_report_costi from commandbutton within w_tes_visite_ispettive
end type
type cb_non_conf from commandbutton within w_tes_visite_ispettive
end type
type dw_tes_visite_ispettive_det from uo_cs_xx_dw within w_tes_visite_ispettive
end type
type dw_ricerca from u_dw_search within w_tes_visite_ispettive
end type
type dw_tes_visite_ispettive_lista from uo_cs_xx_dw within w_tes_visite_ispettive
end type
type dw_folder_search from u_folder within w_tes_visite_ispettive
end type
end forward

global type w_tes_visite_ispettive from w_cs_xx_principale
integer width = 2642
integer height = 2164
string title = "Visite Ispettive"
st_filtro_data st_filtro_data
sle_filtro_data_registrazione sle_filtro_data_registrazione
cb_reset cb_reset
cb_ricerca cb_ricerca
cb_note_esterne cb_note_esterne
cb_paragrafi_iso cb_paragrafi_iso
cb_relazione cb_relazione
cb_dettagli cb_dettagli
cb_report_visita cb_report_visita
cb_procedure cb_procedure
cb_report_costi cb_report_costi
cb_non_conf cb_non_conf
dw_tes_visite_ispettive_det dw_tes_visite_ispettive_det
dw_ricerca dw_ricerca
dw_tes_visite_ispettive_lista dw_tes_visite_ispettive_lista
dw_folder_search dw_folder_search
end type
global w_tes_visite_ispettive w_tes_visite_ispettive

type variables
boolean ib_in_new=false, ib_aggiorna_richiesta=false
long    il_anno_richiesta, il_num_richiesta, il_anno_nc, il_num_nc
end variables

forward prototypes
public subroutine wf_tipo_mansionario (string as_tipo_mansionario)
public function integer wf_carica_singola_visita (long fl_anno, long fl_numero)
end prototypes

public subroutine wf_tipo_mansionario (string as_tipo_mansionario);if as_tipo_mansionario = "S" then
	dw_tes_visite_ispettive_det.Object.des_man_mansionario.Visible = 0
	dw_tes_visite_ispettive_det.Object.cod_resp_divisione.Visible = 1
	dw_tes_visite_ispettive_det.Object.cf_cod_resp_divisione.Visible = 1
else
	dw_tes_visite_ispettive_det.Object.des_man_mansionario.Visible = 1
	dw_tes_visite_ispettive_det.Object.cod_resp_divisione.Visible = 0
	dw_tes_visite_ispettive_det.Object.cf_cod_resp_divisione.Visible = 0
end if

end subroutine

public function integer wf_carica_singola_visita (long fl_anno, long fl_numero);string ls_null

//--------------------------------
dw_ricerca.fu_reset()
sle_filtro_data_registrazione.text = ""
f_PO_LoadDDDW_DW(dw_ricerca, "cod_area",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//--------------------------------

if fl_anno > 0 and fl_numero > 0 then
	dw_ricerca.setitem(dw_ricerca.getrow(),"anno_reg_visita", fl_anno)
	dw_ricerca.setitem(dw_ricerca.getrow(),"num_reg_visita", fl_numero)
end if
dw_ricerca.setitem(dw_ricerca.getrow(),"flag_tipo_vi", "A") //imposta su tutte

dw_ricerca.accepttext()
cb_ricerca.postevent("clicked")

return 1
end function

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_visite_ispettive_det,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW(dw_tes_visite_ispettive_det,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_ricerca, "cod_area",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  



end event

event pc_setwindow;call super::pc_setwindow;string       l_searchcolumn[], l_criteriacolumn[], l_searchtable[]
windowobject l_objects[ ]


dw_tes_visite_ispettive_lista.set_dw_key("cod_azienda")
dw_tes_visite_ispettive_lista.set_dw_options(sqlca, pcca.null_object, c_noretrieveonopen, c_default)
dw_tes_visite_ispettive_det.set_dw_options(sqlca,dw_tes_visite_ispettive_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_visite_ispettive_lista

l_criteriacolumn[1] = "anno_reg_visita"
l_criteriacolumn[2] = "num_reg_visita"
l_criteriacolumn[3] = "cod_area"
l_criteriacolumn[4] = "flag_tipo_vi"

l_searchtable[1] = "tes_visite_ispettive"
l_searchtable[2] = "tes_visite_ispettive"
l_searchtable[3] = "tes_visite_ispettive"
l_searchtable[4] = "tes_visite_ispettive"

l_searchcolumn[1] = "anno_reg_visita"
l_searchcolumn[2] = "num_reg_visita"
l_searchcolumn[3] = "cod_area_aziendale"
l_searchcolumn[4] = "flag_tipo_vi"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_tes_visite_ispettive_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)
							
sle_filtro_data_registrazione.fu_WireDW(dw_tes_visite_ispettive_lista, &
												    "tes_visite_ispettive", &
												    "data_visita", &
												    SQLCA)							
							
dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)
											 
l_objects[1] = dw_tes_visite_ispettive_lista
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])

l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
l_objects[4] = sle_filtro_data_registrazione
l_objects[5] = st_filtro_data
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)
dw_tes_visite_ispettive_lista.change_dw_current()

if s_cs_xx.parametri.parametro_s_1 = "RICHIESTA" then ib_aggiorna_richiesta = true

il_anno_richiesta = s_cs_xx.parametri.parametro_i_1
il_num_richiesta = s_cs_xx.parametri.parametro_d_1
il_anno_nc = s_cs_xx.parametri.parametro_i_2
il_num_nc = s_cs_xx.parametri.parametro_d_2


end event

on w_tes_visite_ispettive.create
int iCurrent
call super::create
this.st_filtro_data=create st_filtro_data
this.sle_filtro_data_registrazione=create sle_filtro_data_registrazione
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.cb_note_esterne=create cb_note_esterne
this.cb_paragrafi_iso=create cb_paragrafi_iso
this.cb_relazione=create cb_relazione
this.cb_dettagli=create cb_dettagli
this.cb_report_visita=create cb_report_visita
this.cb_procedure=create cb_procedure
this.cb_report_costi=create cb_report_costi
this.cb_non_conf=create cb_non_conf
this.dw_tes_visite_ispettive_det=create dw_tes_visite_ispettive_det
this.dw_ricerca=create dw_ricerca
this.dw_tes_visite_ispettive_lista=create dw_tes_visite_ispettive_lista
this.dw_folder_search=create dw_folder_search
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_filtro_data
this.Control[iCurrent+2]=this.sle_filtro_data_registrazione
this.Control[iCurrent+3]=this.cb_reset
this.Control[iCurrent+4]=this.cb_ricerca
this.Control[iCurrent+5]=this.cb_note_esterne
this.Control[iCurrent+6]=this.cb_paragrafi_iso
this.Control[iCurrent+7]=this.cb_relazione
this.Control[iCurrent+8]=this.cb_dettagli
this.Control[iCurrent+9]=this.cb_report_visita
this.Control[iCurrent+10]=this.cb_procedure
this.Control[iCurrent+11]=this.cb_report_costi
this.Control[iCurrent+12]=this.cb_non_conf
this.Control[iCurrent+13]=this.dw_tes_visite_ispettive_det
this.Control[iCurrent+14]=this.dw_ricerca
this.Control[iCurrent+15]=this.dw_tes_visite_ispettive_lista
this.Control[iCurrent+16]=this.dw_folder_search
end on

on w_tes_visite_ispettive.destroy
call super::destroy
destroy(this.st_filtro_data)
destroy(this.sle_filtro_data_registrazione)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.cb_note_esterne)
destroy(this.cb_paragrafi_iso)
destroy(this.cb_relazione)
destroy(this.cb_dettagli)
destroy(this.cb_report_visita)
destroy(this.cb_procedure)
destroy(this.cb_report_costi)
destroy(this.cb_non_conf)
destroy(this.dw_tes_visite_ispettive_det)
destroy(this.dw_ricerca)
destroy(this.dw_tes_visite_ispettive_lista)
destroy(this.dw_folder_search)
end on

event pc_close;call super::pc_close;setnull(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_i_1 = 0
s_cs_xx.parametri.parametro_i_2 = 0
s_cs_xx.parametri.parametro_d_1 = 0
s_cs_xx.parametri.parametro_d_2 = 0


end event

event pc_new;call super::pc_new;dw_tes_visite_ispettive_det.setitem(dw_tes_visite_ispettive_det.getrow(), "data_visita", datetime(today()))

cb_dettagli.enabled = false
cb_non_conf.enabled = false
cb_note_esterne.enabled = false
cb_procedure.enabled = false
cb_relazione.enabled = false
cb_report_costi.enabled = false
cb_report_visita.enabled = false
dw_tes_visite_ispettive_det.object.b_ricerca_fornitore.enabled = false
dw_tes_visite_ispettive_det.object.b_ricerca_fornitore.enabled = true
end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tes_visite_ispettive_det.getrow()
if not( li_row > 0 and dw_tes_visite_ispettive_lista.GetItemNumber(dw_tes_visite_ispettive_lista.GetRow(),"num_reg_visita") <> 0 ) then
	cb_dettagli.enabled = false
	cb_non_conf.enabled = false
	cb_note_esterne.enabled = false
	cb_procedure.enabled = false
	cb_relazione.enabled = false
	cb_report_costi.enabled = false
	cb_report_visita.enabled = false
	dw_tes_visite_ispettive_det.object.b_ricerca_fornitore.enabled = false
end if

end event

event pc_modify;call super::pc_modify;cb_dettagli.enabled = false
cb_non_conf.enabled = false
cb_note_esterne.enabled = false
cb_procedure.enabled = false
cb_relazione.enabled = false
cb_report_costi.enabled = false
cb_report_visita.enabled = false
dw_tes_visite_ispettive_det.object.b_ricerca_fornitore.enabled = false
dw_tes_visite_ispettive_det.object.b_ricerca_fornitore.enabled = true
end event

event pc_save;call super::pc_save;integer li_row
li_row = dw_tes_visite_ispettive_det.GetRow()
if li_row > 0 and dw_tes_visite_ispettive_det.GetItemNumber(li_row,"num_reg_visita") <> 0 then
	cb_dettagli.enabled = true
	cb_non_conf.enabled = true
	cb_note_esterne.enabled = true
	cb_procedure.enabled = true
	cb_relazione.enabled = true
	cb_report_costi.enabled = true
	cb_report_visita.enabled = true
	dw_tes_visite_ispettive_det.object.b_ricerca_fornitore.enabled = true
else
	cb_dettagli.enabled = false
	cb_non_conf.enabled = false
	cb_note_esterne.enabled = false
	cb_procedure.enabled = false
	cb_relazione.enabled = false
	cb_report_costi.enabled = false
	cb_report_visita.enabled = false
	dw_tes_visite_ispettive_det.object.b_ricerca_fornitore.enabled = false
end if
	dw_tes_visite_ispettive_det.object.b_ricerca_fornitore.enabled = false
end event

event open;call super::open;long					ll_anno, ll_numero
s_cs_xx_parametri lstr_parametri

lstr_parametri = Message.PowerObjectParm

if not isnull( lstr_parametri) then
	
	if isvalid( lstr_parametri) then
		if not isnull(lstr_parametri.parametro_ul_1) and not isnull(lstr_parametri.parametro_ul_2) then
			
			ll_anno = lstr_parametri.parametro_ul_1
			ll_numero = lstr_parametri.parametro_ul_2
			wf_carica_singola_visita( ll_anno, ll_numero)
		end if
	end if
end if
end event

type st_filtro_data from statictext within w_tes_visite_ispettive
integer x = 251
integer y = 600
integer width = 320
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Data Visita:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_filtro_data_registrazione from u_sle_search within w_tes_visite_ispettive
integer x = 590
integer y = 600
integer width = 823
integer height = 80
integer taborder = 100
end type

type cb_reset from commandbutton within w_tes_visite_ispettive
integer x = 1874
integer y = 600
integer width = 274
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;dw_ricerca.fu_reset()
sle_filtro_data_registrazione.text = ""
f_PO_LoadDDDW_DW(dw_ricerca, "cod_area",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  

end event

type cb_ricerca from commandbutton within w_tes_visite_ispettive
integer x = 1577
integer y = 600
integer width = 274
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_null

if dw_ricerca.getitemstring(1, "flag_tipo_vi") = "A" then
		dw_ricerca.setitem(1, "flag_tipo_vi", ls_null)
end if
dw_ricerca.fu_buildsearch(TRUE)
sle_filtro_data_registrazione.fu_BuildSearch(false)
dw_folder_search.fu_selecttab(1)
dw_tes_visite_ispettive_lista.change_dw_current()
parent.triggerevent("pc_retrieve")






end event

type cb_note_esterne from commandbutton within w_tes_visite_ispettive
integer x = 2217
integer y = 420
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_tes_visite_ispettive_lista.accepttext()

if (isnull(dw_tes_visite_ispettive_lista.getrow()) or dw_tes_visite_ispettive_lista.getrow() < 1) then
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata nessuna visita ispettiva!")
	return
end if	

if isnull(dw_tes_visite_ispettive_lista.getitemnumber(dw_tes_visite_ispettive_lista.getrow(), "anno_reg_visita")) or &
	dw_tes_visite_ispettive_lista.getitemnumber(dw_tes_visite_ispettive_lista.getrow(), "anno_reg_visita") < 2 then
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata nessuna visita ispettiva!")
	return
end if	

s_cs_xx.parametri.parametro_d_10 = dw_tes_visite_ispettive_lista.getitemnumber(dw_tes_visite_ispettive_lista.getrow(), "anno_reg_visita")
s_cs_xx.parametri.parametro_d_11 = dw_tes_visite_ispettive_lista.getitemnumber(dw_tes_visite_ispettive_lista.getrow(), "num_reg_visita")

window_open_parm(w_tes_visite_ispettive_blob, -1, dw_tes_visite_ispettive_lista)
end event

type cb_paragrafi_iso from commandbutton within w_tes_visite_ispettive
integer x = 2217
integer y = 320
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Paragrafi ISO"
end type

event clicked;if dw_tes_visite_ispettive_lista.rowcount() > 0 then
	window_open_parm(w_tes_visite_ispettive_iso, -1, dw_tes_visite_ispettive_lista)
end if
end event

type cb_relazione from commandbutton within w_tes_visite_ispettive
integer x = 2217
integer y = 220
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Relazione"
end type

event clicked;window_open_parm(w_tes_visite_ispettive_note, -1, dw_tes_visite_ispettive_lista)
end event

type cb_dettagli from commandbutton within w_tes_visite_ispettive
integer x = 2217
integer y = 20
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

on clicked;window_open_parm(w_det_visite_ispettive, -1, dw_tes_visite_ispettive_lista)
end on

type cb_report_visita from commandbutton within w_tes_visite_ispettive
integer x = 2217
integer y = 620
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "R&eport"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = dw_tes_visite_ispettive_lista.getitemnumber(dw_tes_visite_ispettive_lista.getrow(),"anno_reg_visita")
s_cs_xx.parametri.parametro_d_1 = dw_tes_visite_ispettive_lista.getitemnumber(dw_tes_visite_ispettive_lista.getrow(),"num_reg_visita")

window_open(w_report_relaz_visita, -1)
end event

type cb_procedure from cb_apri_manuale within w_tes_visite_ispettive
event clicked pbm_bnclicked
boolean visible = false
integer x = 1829
integer y = 20
integer height = 80
integer taborder = 30
boolean enabled = false
string text = "&Procedure"
end type

event clicked;call super::clicked;integer li_ritorno

li_ritorno = f_apri_manuale("SLE")
end event

type cb_report_costi from commandbutton within w_tes_visite_ispettive
event clicked pbm_bnclicked
integer x = 2217
integer y = 520
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "R&eport Costi"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = dw_tes_visite_ispettive_lista.getitemnumber(dw_tes_visite_ispettive_lista.getrow(),"anno_reg_visita")
s_cs_xx.parametri.parametro_d_1 = dw_tes_visite_ispettive_lista.getitemnumber(dw_tes_visite_ispettive_lista.getrow(),"num_reg_visita")

window_open(w_report_costi_visite, -1)
end event

type cb_non_conf from commandbutton within w_tes_visite_ispettive
integer x = 2217
integer y = 120
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Non Conf."
end type

event clicked;string ls_flag_parametro

//window_open(w_non_conformita_std_sis, -1)

//mettere qui la lettura DEL PARAMETRO CHE IDENTIFICA marr

//if lb_marr then
//	window_type_open(w_cs_xx_treeview, "w_non_conformita_marr", -1 )
//else
//	window_type_open(w_cs_xx_treeview, "w_non_conformita", -1 )
//end if

//Giulio 27/02/2012: controllo se il parametro aziendale FPA (Flag Parametro Aziendale di Marr) è 'S'.

select flag_parametro
into :ls_flag_parametro
from parametri_azienda
where 
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_parametro = 'FPA' ;
		
if ls_flag_parametro = 'S' then
	window_type_open(w_cs_xx_treeview, "w_non_conformita_marr", -1 )
else
	window_type_open(w_cs_xx_treeview, "w_non_conformita", -1 )
end if
		 
// Fine modifica




end event

event getfocus;call super::getfocus;dw_tes_visite_ispettive_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type dw_tes_visite_ispettive_det from uo_cs_xx_dw within w_tes_visite_ispettive
integer x = 23
integer y = 744
integer width = 2560
integer height = 1300
integer taborder = 20
string dataobject = "d_tes_visite_ispettive_det"
borderstyle borderstyle = styleraised!
end type

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

string ls_des_lista, ls_cod_area_aziendale
long ll_num_versione,ll_num_edizione

CHOOSE CASE i_colname

CASE "num_reg_lista"
        SELECT tes_liste_controllo.num_versione,   
         tes_liste_controllo.num_edizione,   
         tes_liste_controllo.des_lista,   
         tes_liste_controllo.cod_area_aziendale
    INTO :ll_num_versione,   
         :ll_num_edizione,   
         :ls_des_lista,   
         :ls_cod_area_aziendale
    FROM tes_liste_controllo  
   WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( tes_liste_controllo.num_reg_lista = :i_coltext ) AND  
         ( tes_liste_controllo.flag_valido = 'S' )   ;

   if (this.getitemstring(this.getrow(), "cod_area_aziendale") <> ls_cod_area_aziendale)  and &
      (not isnull(this.getitemstring(this.getrow(), "cod_area_aziendale")))   then
         g_mb.messagebox("Visite Ispettive","Attenzione, l'area di riferimento della lista è diversa dall'area indicata: la cambio automaticamente", Information!)
   end if

   setitem(getrow(),"cod_area_aziendale", ls_cod_area_aziendale)
   setitem(getrow(),"num_versione", ll_num_versione)
   setitem(getrow(),"num_edizione", ll_num_edizione)

end choose

end if
end event

event itemchanged;call super::itemchanged;if i_colname = "flag_mansionario_codificato" then
	wf_tipo_mansionario (i_coltext)
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_visite_ispettive_det,"cod_fornitore")
end choose
end event

type dw_ricerca from u_dw_search within w_tes_visite_ispettive
event ue_key pbm_dwnkey
integer x = 229
integer y = 40
integer width = 1943
integer height = 660
integer taborder = 20
string dataobject = "d_ricerca_tes_visite_ispettive"
end type

type dw_tes_visite_ispettive_lista from uo_cs_xx_dw within w_tes_visite_ispettive
integer x = 229
integer y = 40
integer width = 1943
integer height = 660
integer taborder = 10
string dataobject = "d_tes_visite_ispettive_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event updatestart;call super::updatestart;integer li_i, li_anno_registrazione
long    ll_num_registrazione, ll_anno_reg_visita, ll_num_reg_visita
string  ls_risposta
	
if not i_extendmode then return
	
for li_i = 1 to this.modifiedcount()
	if getitemstatus(li_i, 0, primary!) = newmodified! then
		if ib_aggiorna_richiesta then

			ls_risposta = "~r~nGenerato Visita Ispettiva " + string(getitemnumber(li_i, "anno_reg_visita")) + "/" + string(getitemnumber(li_i, "num_reg_visita"))
			
			update tab_richieste
			set    risposta = risposta + :ls_risposta
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :il_anno_richiesta and
					 num_registrazione = :il_num_richiesta;
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("OMNIA", "Errore aggiornamento risposta richiesta~r~n" + sqlca.sqlerrtext)
				rollback;
				return 1
			end if
			
			this.setitem(li_i,"anno_non_conf", il_anno_nc)   
			this.setitem(li_i,"num_non_conf", il_num_nc)   
			
		end if
		
		ib_in_new = false
	end if
next

for li_i = 1 to this.deletedcount()
	ll_anno_reg_visita = this.getitemnumber(li_i, "anno_reg_visita", delete!, true)
	ll_num_reg_visita= this.getitemnumber(li_i, "num_reg_visita", delete!, true)

	// devo cancellare tutti i dettagli blob

	delete from det_visite_ispettive_blob
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_reg_visita = :ll_anno_reg_visita and
			num_reg_visita = :ll_num_reg_visita;
			
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione dei documenti dei dettagli: " + sqlca.sqlerrtext)
		rollback;
		return 1
	end if
	
	// devo cancellare tutti i dettagli
	
	delete from det_visite_ispettive
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_reg_visita = :ll_anno_reg_visita and
			num_reg_visita = :ll_num_reg_visita;
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione dei dettagli: " + sqlca.sqlerrtext)
		rollback;
		return 1
	end if
	
	
	// cancello i paragrafi iso della testata
	
	delete from tes_visite_ispettive_iso 
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_reg_visita = :ll_anno_reg_visita and
			num_reg_visita = :ll_num_reg_visita;
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione dei paragrafi iso: " + sqlca.sqlerrtext)
		rollback;
		return 1
	end if
	
	
	// cancello i documenti della testata
	
	delete from tes_visite_ispettive_blob 
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_reg_visita = :ll_anno_reg_visita and
			num_reg_visita = :ll_num_reg_visita;
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("OMNIA", "Errore durante la cancellazione dei documenti: " + sqlca.sqlerrtext)
		rollback;
		return 1
	end if
			
next

if s_cs_xx.num_livello_mail > 0 then
	if getitemstatus(this.getrow(),0,primary!) = datamodified!	then			
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		setnull(s_cs_xx.parametri.parametro_s_4)
		s_cs_xx.parametri.parametro_s_5 = "Modifica della Verifica " + string(this.getitemnumber(this.getrow(),"anno_reg_visita")) + "/" + string(this.getitemnumber(this.getrow(),"num_reg_visita")) 
		s_cs_xx.parametri.parametro_s_6 = "Modifica della Verifica " + string(this.getitemnumber(this.getrow(),"anno_reg_visita")) + "/" + string(this.getitemnumber(this.getrow(),"num_reg_visita")) 
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari?"
		s_cs_xx.parametri.parametro_s_8 = ""
	
		openwithparm(w_invio_messaggi,0)
	elseif getitemstatus(this.getrow(),0,primary!) = newmodified! then
		s_cs_xx.parametri.parametro_s_1 = "M"
		s_cs_xx.parametri.parametro_s_2 = ""
		s_cs_xx.parametri.parametro_s_3 = ""
		setnull(s_cs_xx.parametri.parametro_s_4)
		s_cs_xx.parametri.parametro_s_5 = "Creazione della Verifica " + string(this.getitemnumber(this.getrow(),"anno_reg_visita")) + "/" + string(this.getitemnumber(this.getrow(),"num_reg_visita")) 
		s_cs_xx.parametri.parametro_s_6 = "Creazione della Verifica " + string(this.getitemnumber(this.getrow(),"anno_reg_visita")) + "/" + string(this.getitemnumber(this.getrow(),"num_reg_visita")) 
		s_cs_xx.parametri.parametro_s_7 = "Inviare segnalazione ai mansionari?"
		s_cs_xx.parametri.parametro_s_8 = ""
	
		openwithparm(w_invio_messaggi,0)
		
	end if
end if
end event

event pcd_view;call super::pcd_view;ib_in_new = false

integer li_row
li_row = dw_tes_visite_ispettive_det.getrow()
if li_row > 0 and dw_tes_visite_ispettive_det.GetItemNumber(li_row,"num_reg_visita") <> 0 then
	cb_dettagli.enabled = true
	cb_non_conf.enabled = true
	cb_note_esterne.enabled = true
	cb_procedure.enabled = true
	cb_relazione.enabled = true
	cb_report_costi.enabled = true
	cb_report_visita.enabled = true
else
	cb_dettagli.enabled = false
	cb_non_conf.enabled = false
	cb_note_esterne.enabled = false
	cb_procedure.enabled = false
	cb_relazione.enabled = false
	cb_report_costi.enabled = false
	cb_report_visita.enabled = false
end if
	
dw_tes_visite_ispettive_det.object.b_ricerca_fornitore.enabled = false
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_data_visita
integer li_row
li_row = this.getrow()

if li_row > 0 then
	ll_data_visita = long(string(date(this.getitemdatetime(li_row,"data_visita")), "YYYYMMDD"))
	
	if ll_data_visita = 0 then
		g_mb.messagebox("Non Conformità","ATTENZIONE! la data inizio intervento risulta mancante",Exclamation!)
		pcca.error = c_fatal
		return
	end if
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,li_anno_registrazione,ll_num_registrazione

li_anno_registrazione = f_anno_esercizio()

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
	
	if isnull(getitemnumber(l_idx,"anno_reg_visita")) or getitemnumber(l_idx,"anno_reg_visita") < 1 then
		SetItem(l_Idx,"anno_reg_visita", li_anno_registrazione)
	end if
	
	if isnull(getitemnumber(l_idx,"num_reg_visita")) or getitemnumber(l_idx,"num_reg_visita") < 1 then
		select max(num_reg_visita)
		into   :ll_num_registrazione
		from   tes_visite_ispettive
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_reg_visita = :li_anno_registrazione;
		if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
			ll_num_registrazione = 1
		else
			ll_num_registrazione ++
		end if
		
		this.SetItem (l_idx,"num_reg_visita", ll_num_registrazione)
	end if
	
next

end event

event pcd_delete;call super::pcd_delete;ib_in_new = false

end event

event pcd_new;call super::pcd_new;integer li_anno_reg_visita
long ll_num_reg_visita

ib_in_new = true
	dw_tes_visite_ispettive_det.object.b_ricerca_fornitore.enabled = true

wf_tipo_mansionario ("N")
end event

event pcd_modify;call super::pcd_modify;ib_in_new = false
	dw_tes_visite_ispettive_det.object.b_ricerca_fornitore.enabled = true
end event

event pcd_pickedrow;call super::pcd_pickedrow;integer li_row
li_row = this.getrow()
if li_row > 0 then wf_tipo_mansionario (this.getitemstring(li_row, "flag_mansionario_codificato"))
end event

type dw_folder_search from u_folder within w_tes_visite_ispettive
integer x = 23
integer y = 20
integer width = 2171
integer height = 700
integer taborder = 20
end type

