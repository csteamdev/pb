﻿$PBExportHeader$w_tes_visite_ispettive_iso.srw
$PBExportComments$Finestra Associazione dei Paragrafi Iso alle verifiche ispettive
forward
global type w_tes_visite_ispettive_iso from w_cs_xx_principale
end type
type dw_tes_visite_ispettive_iso from uo_cs_xx_dw within w_tes_visite_ispettive_iso
end type
end forward

global type w_tes_visite_ispettive_iso from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2231
integer height = 1140
string title = "Riferimento Paragrafi Normativa"
dw_tes_visite_ispettive_iso dw_tes_visite_ispettive_iso
end type
global w_tes_visite_ispettive_iso w_tes_visite_ispettive_iso

type variables
boolean ib_in_new
end variables

event pc_setwindow;call super::pc_setwindow;dw_tes_visite_ispettive_iso.set_dw_key("cod_azienda")
dw_tes_visite_ispettive_iso.set_dw_key("anno_reg_visita")
dw_tes_visite_ispettive_iso.set_dw_key("num_reg_visita")
dw_tes_visite_ispettive_iso.set_dw_options(sqlca, i_openparm, c_default, c_default)

iuo_dw_main = dw_tes_visite_ispettive_iso
dw_tes_visite_ispettive_iso.ib_proteggi_chiavi = false


end event

on w_tes_visite_ispettive_iso.create
int iCurrent
call super::create
this.dw_tes_visite_ispettive_iso=create dw_tes_visite_ispettive_iso
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_visite_ispettive_iso
end on

on w_tes_visite_ispettive_iso.destroy
call super::destroy
destroy(this.dw_tes_visite_ispettive_iso)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_visite_ispettive_iso,"prog_paragrafo",sqlca,&
                 "tab_paragrafi_iso","cod_paragrafo","des_paragrafo",&
                 "(tab_paragrafi_iso.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((tab_paragrafi_iso.flag_blocco <> 'S') or (tab_paragrafi_iso.flag_blocco = 'S' and tab_paragrafi_iso.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

type dw_tes_visite_ispettive_iso from uo_cs_xx_dw within w_tes_visite_ispettive_iso
integer x = 23
integer y = 20
integer width = 2149
integer height = 1000
integer taborder = 20
string dataobject = "d_tes_visite_ispettive_iso"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_anno, ll_num, ll_prog, l_Idx

ll_anno = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_reg_visita")
ll_num = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_visita")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "anno_reg_visita")) or GetItemnumber(l_Idx, "anno_reg_visita") = 0 THEN
      SetItem(l_Idx, "anno_reg_visita", ll_anno)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "num_reg_visita")) or GetItemnumber(l_Idx, "num_reg_visita") = 0 THEN
      SetItem(l_Idx, "num_reg_visita", ll_num)
   END IF
NEXT


end event

event updatestart;call super::updatestart;long ll_anno, ll_num, ll_prog, ll_cont

ll_anno = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_reg_visita")
ll_num = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_visita")

SELECT count(flag_riferimento_primario)  
INTO   :ll_cont  
FROM   tes_visite_ispettive_iso
WHERE  ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
		 ( anno_reg_visita = :ll_anno ) AND  
		 ( num_reg_visita = :ll_num ) AND  
		 ( flag_riferimento_primario = 'S' )   ;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore durante la verifica esistenza riferimento primario: " + sqlca.sqlerrtext, Information!)
end if		

if (isnull(ll_cont) or (ll_cont = 0)) and (getitemstring(this.getrow(),"flag_riferimento_primario") <> "S") then
	g_mb.messagebox("OMNIA", "Attenzione: per ogni verifica deve esistere un riferimento primario",StopSign!)
	pcca.error = c_valfailed		
end if   

if getitemstring(this.getrow(),"flag_riferimento_primario") = "S" then
	if ll_cont > 0 then
		g_mb.messagebox("OMNIA", "Attenzione: può esistere un solo riferimento primario",StopSign!)
		pcca.error = c_valfailed		
	end if   
else
	if ll_cont > 1 then
		g_mb.messagebox("OMNIA", "Attenzione: può esistere un solo riferimento primario",StopSign!)
		pcca.error = c_valfailed		
	end if   
end if
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_anno, ll_num, l_Error

ll_anno = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_reg_visita")
ll_num = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_visita")

l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno, ll_num)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

