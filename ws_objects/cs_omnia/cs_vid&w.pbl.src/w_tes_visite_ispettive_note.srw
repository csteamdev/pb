﻿$PBExportHeader$w_tes_visite_ispettive_note.srw
$PBExportComments$Finestra Gestione Note e Relazioni Visita Ispettiva
forward
global type w_tes_visite_ispettive_note from w_cs_xx_principale
end type
type dw_tes_visite_ispettive_note_4 from uo_cs_xx_dw within w_tes_visite_ispettive_note
end type
type dw_tes_visite_ispettive_note_3 from uo_cs_xx_dw within w_tes_visite_ispettive_note
end type
type dw_tes_visite_ispettive_note_2 from uo_cs_xx_dw within w_tes_visite_ispettive_note
end type
type dw_tes_visite_ispettive_note_1 from uo_cs_xx_dw within w_tes_visite_ispettive_note
end type
type dw_folder from u_folder within w_tes_visite_ispettive_note
end type
type dw_tes_visite_ispettive_note_8 from uo_cs_xx_dw within w_tes_visite_ispettive_note
end type
type dw_tes_visite_ispettive_note_7 from uo_cs_xx_dw within w_tes_visite_ispettive_note
end type
type dw_tes_visite_ispettive_note_6 from uo_cs_xx_dw within w_tes_visite_ispettive_note
end type
type dw_tes_visite_ispettive_note_5 from uo_cs_xx_dw within w_tes_visite_ispettive_note
end type
end forward

global type w_tes_visite_ispettive_note from w_cs_xx_principale
int Width=2638
int Height=1409
boolean TitleBar=true
string Title="Relazione"
dw_tes_visite_ispettive_note_4 dw_tes_visite_ispettive_note_4
dw_tes_visite_ispettive_note_3 dw_tes_visite_ispettive_note_3
dw_tes_visite_ispettive_note_2 dw_tes_visite_ispettive_note_2
dw_tes_visite_ispettive_note_1 dw_tes_visite_ispettive_note_1
dw_folder dw_folder
dw_tes_visite_ispettive_note_8 dw_tes_visite_ispettive_note_8
dw_tes_visite_ispettive_note_7 dw_tes_visite_ispettive_note_7
dw_tes_visite_ispettive_note_6 dw_tes_visite_ispettive_note_6
dw_tes_visite_ispettive_note_5 dw_tes_visite_ispettive_note_5
end type
global w_tes_visite_ispettive_note w_tes_visite_ispettive_note

type variables
boolean ib_in_new
end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_tes_visite_ispettive_note_1
dw_folder.fu_AssignTab(1, "&Generali", l_Objects[])
l_objects[1] = dw_tes_visite_ispettive_note_2
dw_folder.fu_AssignTab(2, "&Struttura Org.", l_Objects[])
l_objects[1] = dw_tes_visite_ispettive_note_3
dw_folder.fu_AssignTab(3, "&Procedure Gest.", l_Objects[])
l_objects[1] = dw_tes_visite_ispettive_note_4
dw_folder.fu_AssignTab(4, "&Risorse Umane", l_Objects[])
l_objects[1] = dw_tes_visite_ispettive_note_5
dw_folder.fu_AssignTab(5, "&Apparecchi Mater.", l_Objects[])
l_objects[1] = dw_tes_visite_ispettive_note_6
dw_folder.fu_AssignTab(6, "Aree / Processi", l_Objects[])
l_objects[1] = dw_tes_visite_ispettive_note_7
dw_folder.fu_AssignTab(7, "Conf. Prodotti", l_Objects[])
l_objects[1] = dw_tes_visite_ispettive_note_8
dw_folder.fu_AssignTab(8, "Documentaz.Sistema", l_Objects[])

dw_tes_visite_ispettive_note_1.set_dw_options(sqlca,i_openparm,c_nonew+c_nodelete+c_modifyonopen,c_default)
dw_tes_visite_ispettive_note_2.set_dw_options(sqlca,dw_tes_visite_ispettive_note_1,c_sharedata+c_scrollparent,c_default)
dw_tes_visite_ispettive_note_3.set_dw_options(sqlca,dw_tes_visite_ispettive_note_1,c_sharedata+c_scrollparent,c_default)
dw_tes_visite_ispettive_note_4.set_dw_options(sqlca,dw_tes_visite_ispettive_note_1,c_sharedata+c_scrollparent,c_default)
dw_tes_visite_ispettive_note_5.set_dw_options(sqlca,dw_tes_visite_ispettive_note_1,c_sharedata+c_scrollparent,c_default)
dw_tes_visite_ispettive_note_6.set_dw_options(sqlca,dw_tes_visite_ispettive_note_1,c_sharedata+c_scrollparent,c_default)
dw_tes_visite_ispettive_note_7.set_dw_options(sqlca,dw_tes_visite_ispettive_note_1,c_sharedata+c_scrollparent,c_default)
dw_tes_visite_ispettive_note_8.set_dw_options(sqlca,dw_tes_visite_ispettive_note_1,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_visite_ispettive_note_1

dw_folder.fu_FolderCreate(8,4)
                          // il primo parametri indica quanti fogli dentro al folder
                          // il secondo parametro indica quanti fogli per riga
dw_folder.fu_SelectTab(1)

end on

on w_tes_visite_ispettive_note.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tes_visite_ispettive_note_4=create dw_tes_visite_ispettive_note_4
this.dw_tes_visite_ispettive_note_3=create dw_tes_visite_ispettive_note_3
this.dw_tes_visite_ispettive_note_2=create dw_tes_visite_ispettive_note_2
this.dw_tes_visite_ispettive_note_1=create dw_tes_visite_ispettive_note_1
this.dw_folder=create dw_folder
this.dw_tes_visite_ispettive_note_8=create dw_tes_visite_ispettive_note_8
this.dw_tes_visite_ispettive_note_7=create dw_tes_visite_ispettive_note_7
this.dw_tes_visite_ispettive_note_6=create dw_tes_visite_ispettive_note_6
this.dw_tes_visite_ispettive_note_5=create dw_tes_visite_ispettive_note_5
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tes_visite_ispettive_note_4
this.Control[iCurrent+2]=dw_tes_visite_ispettive_note_3
this.Control[iCurrent+3]=dw_tes_visite_ispettive_note_2
this.Control[iCurrent+4]=dw_tes_visite_ispettive_note_1
this.Control[iCurrent+5]=dw_folder
this.Control[iCurrent+6]=dw_tes_visite_ispettive_note_8
this.Control[iCurrent+7]=dw_tes_visite_ispettive_note_7
this.Control[iCurrent+8]=dw_tes_visite_ispettive_note_6
this.Control[iCurrent+9]=dw_tes_visite_ispettive_note_5
end on

on w_tes_visite_ispettive_note.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tes_visite_ispettive_note_4)
destroy(this.dw_tes_visite_ispettive_note_3)
destroy(this.dw_tes_visite_ispettive_note_2)
destroy(this.dw_tes_visite_ispettive_note_1)
destroy(this.dw_folder)
destroy(this.dw_tes_visite_ispettive_note_8)
destroy(this.dw_tes_visite_ispettive_note_7)
destroy(this.dw_tes_visite_ispettive_note_6)
destroy(this.dw_tes_visite_ispettive_note_5)
end on

type dw_tes_visite_ispettive_note_4 from uo_cs_xx_dw within w_tes_visite_ispettive_note
int X=46
int Y=261
int Width=2446
int Height=1001
int TabOrder=60
string DataObject="d_tes_visite_ispettive_note_4"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;ib_in_new = false
end on

on updatestart;call uo_cs_xx_dw::updatestart;if ib_in_new then

   long ll_anno,ll_num_registrazione

   this.SetItem (this.GetRow ( ),"anno_reg_visita", year(today()) )
   ll_anno = this.GetItemNumber(this.GetRow ( ), "anno_reg_visita" )
   select max(tes_visite_ispettive.num_reg_visita)
     into :ll_num_registrazione
     from tes_visite_ispettive
     where (tes_visite_ispettive.cod_azienda = :s_cs_xx.cod_azienda) and
           (:ll_anno = tes_visite_ispettive.anno_reg_visita);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow ( ),"num_reg_visita", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "num_reg_visita", 10)
   end if

   ib_in_new = false
end if

end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;ib_in_new = false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;ib_in_new = false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;ib_in_new = true
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_tes_visite_ispettive_note_3 from uo_cs_xx_dw within w_tes_visite_ispettive_note
int X=46
int Y=261
int Width=2446
int Height=1001
int TabOrder=70
string DataObject="d_tes_visite_ispettive_note_3"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;ib_in_new = false
end on

on updatestart;call uo_cs_xx_dw::updatestart;if ib_in_new then

   long ll_anno,ll_num_registrazione

   this.SetItem (this.GetRow ( ),"anno_reg_visita", year(today()) )
   ll_anno = this.GetItemNumber(this.GetRow ( ), "anno_reg_visita" )
   select max(tes_visite_ispettive.num_reg_visita)
     into :ll_num_registrazione
     from tes_visite_ispettive
     where (tes_visite_ispettive.cod_azienda = :s_cs_xx.cod_azienda) and
           (:ll_anno = tes_visite_ispettive.anno_reg_visita);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow ( ),"num_reg_visita", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "num_reg_visita", 10)
   end if

   ib_in_new = false
end if

end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;ib_in_new = false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;ib_in_new = false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;ib_in_new = true
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_tes_visite_ispettive_note_2 from uo_cs_xx_dw within w_tes_visite_ispettive_note
int X=46
int Y=261
int Width=2446
int Height=1001
int TabOrder=10
string DataObject="d_tes_visite_ispettive_note_2"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;ib_in_new = false
end on

on updatestart;call uo_cs_xx_dw::updatestart;if ib_in_new then

   long ll_anno,ll_num_registrazione

   this.SetItem (this.GetRow ( ),"anno_reg_visita", year(today()) )
   ll_anno = this.GetItemNumber(this.GetRow ( ), "anno_reg_visita" )
   select max(tes_visite_ispettive.num_reg_visita)
     into :ll_num_registrazione
     from tes_visite_ispettive
     where (tes_visite_ispettive.cod_azienda = :s_cs_xx.cod_azienda) and
           (:ll_anno = tes_visite_ispettive.anno_reg_visita);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow ( ),"num_reg_visita", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "num_reg_visita", 10)
   end if

   ib_in_new = false
end if

end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;ib_in_new = false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;ib_in_new = false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;ib_in_new = true
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_tes_visite_ispettive_note_1 from uo_cs_xx_dw within w_tes_visite_ispettive_note
int X=46
int Y=261
int Width=2446
int Height=1001
int TabOrder=90
string DataObject="d_tes_visite_ispettive_note_1"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error, ll_anno_reg_visita, ll_num_reg_visita

ll_anno_reg_visita = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_reg_visita")
ll_num_reg_visita  = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_visita")

l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno_reg_visita, ll_num_reg_visita)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;ib_in_new = false
end on

type dw_folder from u_folder within w_tes_visite_ispettive_note
int X=23
int Y=21
int Width=2561
int Height=1261
int TabOrder=80
end type

on itemchanged;call u_folder::itemchanged;CHOOSE CASE i_SelectedTabName
   CASE "&Generali"
      SetFocus(dw_tes_visite_ispettive_note_1)
   CASE "&Struttura Org."
      SetFocus(dw_tes_visite_ispettive_note_2)
   CASE "&Procedure Gest."
      SetFocus(dw_tes_visite_ispettive_note_3)
   CASE "&Risorse Umane"
      SetFocus(dw_tes_visite_ispettive_note_4)
   CASE "&Apparecchi Mater."
      SetFocus(dw_tes_visite_ispettive_note_5)
   CASE "Aree / Processi"
      SetFocus(dw_tes_visite_ispettive_note_6)
   CASE "Conf. Prodotti"
      SetFocus(dw_tes_visite_ispettive_note_7)
   CASE "Documentaz.Sistema"
      SetFocus(dw_tes_visite_ispettive_note_8)
END CHOOSE

end on

type dw_tes_visite_ispettive_note_8 from uo_cs_xx_dw within w_tes_visite_ispettive_note
int X=46
int Y=261
int Width=2446
int Height=1001
int TabOrder=20
string DataObject="d_tes_visite_ispettive_note_8"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

on updatestart;call uo_cs_xx_dw::updatestart;if ib_in_new then

   long ll_anno,ll_num_registrazione

   this.SetItem (this.GetRow ( ),"anno_reg_visita", year(today()) )
   ll_anno = this.GetItemNumber(this.GetRow ( ), "anno_reg_visita" )
   select max(tes_visite_ispettive.num_reg_visita)
     into :ll_num_registrazione
     from tes_visite_ispettive
     where (tes_visite_ispettive.cod_azienda = :s_cs_xx.cod_azienda) and
           (:ll_anno = tes_visite_ispettive.anno_reg_visita);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow ( ),"num_reg_visita", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "num_reg_visita", 10)
   end if

   ib_in_new = false
end if

end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;ib_in_new = false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;ib_in_new = false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;ib_in_new = true
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_view;call uo_cs_xx_dw::pcd_view;ib_in_new = false
end on

type dw_tes_visite_ispettive_note_7 from uo_cs_xx_dw within w_tes_visite_ispettive_note
int X=46
int Y=261
int Width=2446
int Height=1001
int TabOrder=30
string DataObject="d_tes_visite_ispettive_note_7"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;ib_in_new = false
end on

on updatestart;call uo_cs_xx_dw::updatestart;if ib_in_new then

   long ll_anno,ll_num_registrazione

   this.SetItem (this.GetRow ( ),"anno_reg_visita", year(today()) )
   ll_anno = this.GetItemNumber(this.GetRow ( ), "anno_reg_visita" )
   select max(tes_visite_ispettive.num_reg_visita)
     into :ll_num_registrazione
     from tes_visite_ispettive
     where (tes_visite_ispettive.cod_azienda = :s_cs_xx.cod_azienda) and
           (:ll_anno = tes_visite_ispettive.anno_reg_visita);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow ( ),"num_reg_visita", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "num_reg_visita", 10)
   end if

   ib_in_new = false
end if

end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;ib_in_new = false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;ib_in_new = false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;ib_in_new = true
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_tes_visite_ispettive_note_6 from uo_cs_xx_dw within w_tes_visite_ispettive_note
int X=46
int Y=261
int Width=2446
int Height=1001
int TabOrder=40
string DataObject="d_tes_visite_ispettive_note_6"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;ib_in_new = false
end on

on updatestart;call uo_cs_xx_dw::updatestart;if ib_in_new then

   long ll_anno,ll_num_registrazione

   this.SetItem (this.GetRow ( ),"anno_reg_visita", year(today()) )
   ll_anno = this.GetItemNumber(this.GetRow ( ), "anno_reg_visita" )
   select max(tes_visite_ispettive.num_reg_visita)
     into :ll_num_registrazione
     from tes_visite_ispettive
     where (tes_visite_ispettive.cod_azienda = :s_cs_xx.cod_azienda) and
           (:ll_anno = tes_visite_ispettive.anno_reg_visita);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow ( ),"num_reg_visita", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "num_reg_visita", 10)
   end if

   ib_in_new = false
end if

end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;ib_in_new = false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;ib_in_new = false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;ib_in_new = true
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_tes_visite_ispettive_note_5 from uo_cs_xx_dw within w_tes_visite_ispettive_note
int X=46
int Y=261
int Width=2446
int Height=1001
int TabOrder=50
string DataObject="d_tes_visite_ispettive_note_5"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;ib_in_new = false
end on

on updatestart;call uo_cs_xx_dw::updatestart;if ib_in_new then

   long ll_anno,ll_num_registrazione

   this.SetItem (this.GetRow ( ),"anno_reg_visita", year(today()) )
   ll_anno = this.GetItemNumber(this.GetRow ( ), "anno_reg_visita" )
   select max(tes_visite_ispettive.num_reg_visita)
     into :ll_num_registrazione
     from tes_visite_ispettive
     where (tes_visite_ispettive.cod_azienda = :s_cs_xx.cod_azienda) and
           (:ll_anno = tes_visite_ispettive.anno_reg_visita);

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow ( ),"num_reg_visita", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "num_reg_visita", 10)
   end if

   ib_in_new = false
end if

end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;ib_in_new = false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;ib_in_new = false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;ib_in_new = true
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

