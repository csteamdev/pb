﻿$PBExportHeader$w_calcolo_vendor_list.srw
$PBExportComments$Finestra di Calcolo della Vendor List
forward
global type w_calcolo_vendor_list from w_cs_xx_principale
end type
type dw_sel_calcolo_vendor_list from uo_cs_xx_dw within w_calcolo_vendor_list
end type
type cb_valutazione from commandbutton within w_calcolo_vendor_list
end type
type cb_annulla from commandbutton within w_calcolo_vendor_list
end type
type st_1 from statictext within w_calcolo_vendor_list
end type
type cb_report from commandbutton within w_calcolo_vendor_list
end type
end forward

global type w_calcolo_vendor_list from w_cs_xx_principale
integer width = 1499
integer height = 748
string title = "Calcolo ~"Vendor List~""
dw_sel_calcolo_vendor_list dw_sel_calcolo_vendor_list
cb_valutazione cb_valutazione
cb_annulla cb_annulla
st_1 st_1
cb_report cb_report
end type
global w_calcolo_vendor_list w_calcolo_vendor_list

on w_calcolo_vendor_list.create
int iCurrent
call super::create
this.dw_sel_calcolo_vendor_list=create dw_sel_calcolo_vendor_list
this.cb_valutazione=create cb_valutazione
this.cb_annulla=create cb_annulla
this.st_1=create st_1
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sel_calcolo_vendor_list
this.Control[iCurrent+2]=this.cb_valutazione
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.cb_report
end on

on w_calcolo_vendor_list.destroy
call super::destroy
destroy(this.dw_sel_calcolo_vendor_list)
destroy(this.cb_valutazione)
destroy(this.cb_annulla)
destroy(this.st_1)
destroy(this.cb_report)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_sel_calcolo_vendor_list.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_nomodify + &
                                    c_nodelete + &
                                    c_newonopen + &
                                    c_disableCC, &
                                    c_noresizedw + &
                                    c_nohighlightselected + &
                                    c_cursorrowpointer)

iuo_dw_main = dw_sel_calcolo_vendor_list
dw_sel_calcolo_vendor_list.setfocus()
end event

type dw_sel_calcolo_vendor_list from uo_cs_xx_dw within w_calcolo_vendor_list
integer x = 23
integer y = 20
integer width = 1417
integer height = 416
integer taborder = 10
string dataobject = "d_sel_calcolo_vendor_list"
borderstyle borderstyle = styleraised!
end type

type cb_valutazione from commandbutton within w_calcolo_vendor_list
event clicked pbm_bnclicked
integer x = 1051
integer y = 540
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Valutazione"
end type

event clicked;string ls_flag_certificato, ls_flag_approvato, ls_flag_strategico, ls_flag_terzista
string ls_flag_omologato, ls_cod_fornitore, ls_sql, ls_tipo_flag, ls_codici_forn[]
integer li_row, li_cont
double VCD1,VCD2,VCD,VCE,VS1,VS2,VS3,VS,VQ,VT
datetime ldt_data_da, ldt_data_a

setpointer(hourglass!)
li_row = dw_sel_calcolo_vendor_list.getrow()
ldt_data_da = dw_sel_calcolo_vendor_list.getitemdatetime(li_row, "rdt_data_da")
ldt_data_a = dw_sel_calcolo_vendor_list.getitemdatetime(li_row, "rdt_data_a")
ls_flag_certificato = dw_sel_calcolo_vendor_list.getitemstring(li_row, "rs_flag_certificato")
ls_flag_approvato = dw_sel_calcolo_vendor_list.getitemstring(li_row, "rs_flag_approvato")
ls_flag_strategico = dw_sel_calcolo_vendor_list.getitemstring(li_row, "rs_flag_strategico")
ls_flag_terzista = dw_sel_calcolo_vendor_list.getitemstring(li_row, "rs_flag_terzista")
ls_flag_omologato = dw_sel_calcolo_vendor_list.getitemstring(li_row, "rs_flag_omologato")
ls_tipo_flag = dw_sel_calcolo_vendor_list.getitemstring(li_row, "rs_tipo_flag")

if isnull(ls_flag_certificato) then ls_flag_certificato = 'N'
if isnull(ls_flag_approvato) then ls_flag_approvato = 'N'
if isnull(ls_flag_strategico) then ls_flag_strategico = 'N'
if isnull(ls_flag_terzista) then ls_flag_terzista = 'N'
if isnull(ls_flag_omologato) then ls_flag_omologato = 'N'

if ldt_data_da > ldt_data_a then
   g_mb.messagebox("Calcolo Vendor Rating","ERRORE !! Data di inizio maggiore della data di fine")
   return
end if

if year(date(ldt_data_da)) <> year(date(ldt_data_a)) then
   g_mb.messagebox("Calcolo Vendor Rating","Impossibile eseguire calcolo a cavallo di due anni")
   return
end if

ls_sql = "select distinct anag_fornitori.cod_fornitore from anag_fornitori " + &
			"join acc_materiali on acc_materiali.cod_azienda=anag_fornitori.cod_azienda and " + &
					"acc_materiali.cod_fornitore=anag_fornitori.cod_fornitore " + &
			"where anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "'"

choose case ls_tipo_flag
	case 'C' //Certificati
		ls_sql = ls_sql +	" and anag_fornitori.flag_certificato = '" + ls_flag_certificato + "'"
		if ls_flag_certificato = 'N' then
			ls_sql = ls_sql + " or anag_fornitori.flag_certificato is null;"
		else
			ls_sql = ls_sql + ";"
		end if
	case 'A' //Approvati
		ls_sql = ls_sql +	" and anag_fornitori.flag_approvato = '" + ls_flag_approvato + "'"
		if ls_flag_approvato = 'N' then
			ls_sql = ls_sql + " or anag_fornitori.flag_approvato is null;"
		else
			ls_sql = ls_sql + ";"
		end if
	case 'S' //Strategici
		ls_sql = ls_sql +	" and anag_fornitori.flag_strategico = '" + ls_flag_strategico + "'"
		if ls_flag_strategico = 'N' then
			ls_sql = ls_sql + " or anag_fornitori.flag_strategico is null;"
		else
			ls_sql = ls_sql + ";"
		end if
	case 'T' //Terzisti
		ls_sql = ls_sql +	" and anag_fornitori.flag_terzista = '" + ls_flag_terzista + "'"
		if ls_flag_terzista = 'N' then
			ls_sql = ls_sql + " or anag_fornitori.flag_terzista is null;"
		else
			ls_sql = ls_sql + ";"
		end if
	case 'O' //Omologati
		ls_sql = ls_sql +	" and anag_fornitori.flag_omologato = '" + ls_flag_omologato + "'"
		if ls_flag_omologato = 'N' then
			ls_sql = ls_sql + " or anag_fornitori.flag_omologato is null;"
		else
			ls_sql = ls_sql + ";"
		end if
	case 'U' //Tutti
		ls_sql = ls_sql + ";"
end choose

declare curs_fornitori dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic curs_fornitori;
fetch curs_fornitori into :ls_cod_fornitore;
li_cont = 0
do while sqlca.sqlcode = 0	
	li_cont ++
	ls_codici_forn[li_cont] = ls_cod_fornitore
	fetch curs_fornitori into :ls_cod_fornitore;
loop
close curs_fornitori;

for li_cont = 1 to upperbound(ls_codici_forn)
	ls_cod_fornitore = ls_codici_forn[li_cont]
	delete from det_valutazione_for
		where  ( det_valutazione_for.cod_azienda = :s_cs_xx.cod_azienda ) AND  
				 ( det_valutazione_for.cod_fornitore = :ls_cod_fornitore) ;   
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Cancellazione Precedente Valutazione","Cancellazione precedente valutazione fallita (" &
						+ ls_cod_fornitore + ": " + sqlca.sqlerrtext, stopsign!)
	end if
	st_1.text = "Codice Fornitore: " + ls_cod_fornitore
	li_cont++
	f_carica_val_fornitori(ls_cod_fornitore,VCD1,VCD2,VCD,VCE,VS1,VS2,VS3,VS,VQ,VT,ldt_data_da,ldt_data_a, false)
next
st_1.text = "Aggiornati " + string(li_cont) + " Fornitori"
setpointer(arrow!)
end event

type cb_annulla from commandbutton within w_calcolo_vendor_list
event clicked pbm_bnclicked
integer x = 663
integer y = 540
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
boolean cancel = true
end type

on clicked;close(parent)
end on

type st_1 from statictext within w_calcolo_vendor_list
integer x = 23
integer y = 460
integer width = 1417
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Codice Fornitore in elaborazione:"
boolean focusrectangle = false
end type

type cb_report from commandbutton within w_calcolo_vendor_list
event clicked pbm_bnclicked
integer x = 274
integer y = 540
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
boolean cancel = true
end type

event clicked;window_open(w_report_forn_vendor_list, -1)
end event

