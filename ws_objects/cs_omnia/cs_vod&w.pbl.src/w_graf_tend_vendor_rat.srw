﻿$PBExportHeader$w_graf_tend_vendor_rat.srw
$PBExportComments$Finestra Grafico Tendenza Vendor Rating
forward
global type w_graf_tend_vendor_rat from w_cs_xx_principale
end type
type dw_grafico from uo_cs_graph within w_graf_tend_vendor_rat
end type
end forward

global type w_graf_tend_vendor_rat from w_cs_xx_principale
integer width = 2569
integer height = 1372
string title = "Grafico Tendenza ~"Vendor Rating~""
dw_grafico dw_grafico
end type
global w_graf_tend_vendor_rat w_graf_tend_vendor_rat

forward prototypes
public subroutine wf_retrieve ()
end prototypes

public subroutine wf_retrieve ();string ls_rag_soc_1
datastore lds_dati

SELECT anag_fornitori.rag_soc_1  
INTO   :ls_rag_soc_1
FROM   anag_fornitori  
WHERE  ( anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( anag_fornitori.cod_fornitore = :s_cs_xx.parametri.parametro_s_1 )   ;

lds_dati = create datastore
lds_dati.dataobject = "d_graf_vis_tend_vendor_rating"
lds_dati.settransobject(sqlca)
lds_dati.Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_1)

dw_grafico.ib_datastore = true
dw_grafico.ids_data = lds_dati

dw_grafico.is_str[1] = "des_periodo"
dw_grafico.is_num[1] = "punteggio"

dw_grafico.is_source_categoria_col = "des_periodo"

dw_grafico.is_categoria_col = "str_1"
dw_grafico.is_exp_valore = "num_1"
dw_grafico.is_serie_col = ""

//altre impostazioni del grafico
dw_grafico.is_titolo = "Vendor Rating   Fornitore ("+ s_cs_xx.parametri.parametro_s_1 + ")  " + ls_rag_soc_1
dw_grafico.is_label_categoria= "Periodo"
dw_grafico.is_label_valori = "Punteggio"

//tipo scala: 1 se lineare, 2 se logaritmica in base 10
dw_grafico.ii_scala = 1

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_grafico.ii_tipo_grafico = 9  //Colonne 3D Obj

//colore di sfondo del grafico
//per il bianco (DEFAULT)	: 16777215
//per il silver			: 12632256
dw_grafico.il_backcolor = 12632256 //silver

//metodo che visualizza i dati nel grafico
dw_grafico.uof_retrieve( )

destroy lds_dati
end subroutine

event pc_setwindow;call super::pc_setwindow;//dw_graf_vis_tend_vendor_rating.set_dw_options(sqlca, &
//                                              pcca.null_object, &
//                                              c_nonew +&
//                                              c_nomodify+ &
//                                              c_nodelete + &
//                                              c_retrieveonopen + &
//                                              c_disableCC + &
//                                              c_disableCCInsert,&
//                                              c_default)

wf_retrieve()
end event

on w_graf_tend_vendor_rat.create
int iCurrent
call super::create
this.dw_grafico=create dw_grafico
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_grafico
end on

on w_graf_tend_vendor_rat.destroy
call super::destroy
destroy(this.dw_grafico)
end on

type dw_grafico from uo_cs_graph within w_graf_tend_vendor_rat
integer x = 50
integer y = 16
integer width = 2469
integer taborder = 11
long il_backcolor = 12632256
end type

