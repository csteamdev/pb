﻿$PBExportHeader$w_parametri_valutazione_for.srw
forward
global type w_parametri_valutazione_for from w_cs_xx_principale
end type
type st_1 from statictext within w_parametri_valutazione_for
end type
type st_alert from statictext within w_parametri_valutazione_for
end type
type dw_dettaglio from uo_cs_xx_dw within w_parametri_valutazione_for
end type
type dw_lista from uo_cs_xx_dw within w_parametri_valutazione_for
end type
type dw_folder from u_folder within w_parametri_valutazione_for
end type
end forward

global type w_parametri_valutazione_for from w_cs_xx_principale
integer width = 3666
integer height = 2272
string title = "Parametri Valutazione Fornitori"
st_1 st_1
st_alert st_alert
dw_dettaglio dw_dettaglio
dw_lista dw_lista
dw_folder dw_folder
end type
global w_parametri_valutazione_for w_parametri_valutazione_for

type variables
string is_parametri[]
end variables

forward prototypes
public function integer wf_string_to_array (string as_input, string as_sep, ref string as_array[])
public function string wf_parametri_valutazione ()
end prototypes

public function integer wf_string_to_array (string as_input, string as_sep, ref string as_array[]);/*
Function fof_string_to_array
Created by: Michele
Creation Date: 18/12/2007
Comment: Given an input string and a separator string, returns an array of all elements in input string

Revisions
___________________________________________
Name                                   Date                                     Comments
------------------------------------------------------------
Parameters
___________________________________________
Name                                                                                  Comments
as_input                                                                            The input string to process
as_sep                                                                The string to use as separator
as_array[]                                                          Reference to an array of strings, in which we will store the results
------------------------------------------------------------
Return Values
___________________________________________
Value                                                                   Comments
 0                                                                                           Execution completed succesfully
-1                                                                                          Error during execution
------------------------------------------------------------
*/

long                                                      ll_pos, ll_element
string                                    ls_array[], ls_element

as_array = ls_array
ll_element = 0

do 
	ll_pos = pos(as_input,as_sep,1)
	if ll_pos > 0 then
		ls_element	= left(as_input,(ll_pos - 1))
		as_input		= right(as_input,(len(as_input) - ll_pos))
	else
		ls_element	= as_input
		as_input		= ""
	end if
	 
	ll_element++    
	ls_array[ll_element] = ls_element
loop while ll_pos > 0

as_array = ls_array

return 0

end function

public function string wf_parametri_valutazione ();//DONATO 30/01/2009
/*
Questa funzione imposta una stringa di filtro per visualizzare solo i parametri di valutazione dei fornitori
Il parametro contiene l'elenco dei codici dei tipi causa che servono per la gestione dei reclami, separati da una virgola
es. CD1,CD2,CD3

Se il parametro "as_tipo_causa" contiene uno di questi valori allora la funzione torna "R"
																								 altrimenti torna "N"
																								 
argomenti
	string		as_tipo_causa	(codice del tipo causa della NC corrente)

val. ritorno
	string	(R o N)
*/

string ls_prm_LPV, ls_msg, ls_separetor, ls_array[], ls_filter
integer li_index

ls_separetor = ","

select stringa
into :ls_prm_LPV
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'LPV';
	
if sqlca.sqlcode = 0 then
	ls_msg = ""
elseif sqlca.sqlcode = 100 then
	//il parametro non esiste: quindi Non Conformità
	ls_msg = "Manca il parametro aziendale LPV" + char(13) + &
									"(lista dei parametri di valutazione dei fornitori separati da virgola)"
else
	//errore
	ls_msg = "Errore durante la lettura del valore parametro aziendale LPV: " + sqlca.sqlerrtext
end if	

if isnull(ls_prm_LPV) or ls_prm_LPV = "" then
	ls_msg = "Indicare nel parametro aziendale LPV la lista dei parametri di valutazione dei fornitori separati da virgola!"
end if

if ls_msg <> "" then
	g_mb.messagebox("OMNIA",ls_msg, Exclamation!)
	return "NOPARAM"
end if

if wf_string_to_array(ls_prm_LPV, ls_separetor, ls_array) = 0 then
	for li_index = 1 to upperbound(ls_array)
		if li_index > 1 then ls_filter += " or "
		ls_filter += "cod_parametro = '" + ls_array[li_index] + "'"
	next
	
	return ls_filter
else
	//errore durante l'elaborazione della stringa
	g_mb.messagebox("OMNIA","Errore durante l'interpretazione del valore parametroaziendale LPV", Exclamation!)
	return "NOPARAM"
end if
end function

on w_parametri_valutazione_for.create
int iCurrent
call super::create
this.st_1=create st_1
this.st_alert=create st_alert
this.dw_dettaglio=create dw_dettaglio
this.dw_lista=create dw_lista
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.st_alert
this.Control[iCurrent+3]=this.dw_dettaglio
this.Control[iCurrent+4]=this.dw_lista
this.Control[iCurrent+5]=this.dw_folder
end on

on w_parametri_valutazione_for.destroy
call super::destroy
destroy(this.st_1)
destroy(this.st_alert)
destroy(this.dw_dettaglio)
destroy(this.dw_lista)
destroy(this.dw_folder)
end on

event open;call super::open;string ls_modify, ls_filter
integer li_index

dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_options(sqlca, &
                                          pcca.null_object, &
                                          c_default, &
                                          c_default)
dw_dettaglio.set_dw_options(sqlca, &
                                        dw_lista, &
                                        c_sharedata + c_scrollparent, &
                                        c_default)

ls_modify = "stringa.protect='0~tif(flag_parametro<>~~'S~~',1,0)'~t"
ls_modify = ls_modify + "flag.protect='0~tif(flag_parametro<>~~'F~~',1,0)'~t"
ls_modify = ls_modify + "data.protect='0~tif(flag_parametro<>~~'D~~',1,0)'~t"
ls_modify = ls_modify + "numero.protect='0~tif(flag_parametro<>~~'N~~',1,0)'~t"
dw_dettaglio.modify(ls_modify)
end event

event pc_new;call super::pc_new;dw_dettaglio.setitem(dw_dettaglio.getrow(), "data", datetime(today()))
end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[], l_objects[ ],lw_oggetti_1[]
string ls_modify

lw_oggetti[1] = dw_lista
dw_folder.fu_assigntab(1, "Lista", lw_oggetti[])
lw_oggetti[1] = dw_dettaglio
dw_folder.fu_assigntab(2, "Dettaglio", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

dw_lista.set_dw_options(sqlca, &
                                          pcca.null_object, &
                                          c_default, &
                                          c_default)
dw_dettaglio.set_dw_options(sqlca, &
                                        dw_lista, &
                                        c_sharedata + c_scrollparent, &
                                        c_default)

ls_modify = "stringa.protect='0~tif(flag_parametro<>~~'S~~',1,0)'~t"
ls_modify = ls_modify + "flag.protect='0~tif(flag_parametro<>~~'F~~',1,0)'~t"
ls_modify = ls_modify + "data.protect='0~tif(flag_parametro<>~~'D~~',1,0)'~t"
ls_modify = ls_modify + "numero.protect='0~tif(flag_parametro<>~~'N~~',1,0)'~t"
dw_dettaglio.modify(ls_modify)
end event

type st_1 from statictext within w_parametri_valutazione_for
integer x = 55
integer y = 96
integer width = 3186
integer height = 68
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Nella lista sottostante saranno pertanto elencati solo i parametri indicati in LPV (separati da virgola)"
boolean focusrectangle = false
end type

type st_alert from statictext within w_parametri_valutazione_for
integer x = 55
integer y = 24
integer width = 3191
integer height = 68
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Consente di gestire solo i parametri di valutazione dei Fornitori - (vedi prm aziendale LPV)"
boolean focusrectangle = false
end type

type dw_dettaglio from uo_cs_xx_dw within w_parametri_valutazione_for
integer x = 50
integer y = 328
integer width = 2354
integer height = 660
integer taborder = 20
string dataobject = "d_parametri_omnia_det"
end type

event pcd_modify;call super::pcd_modify;string ls_modify

ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)
end event

event pcd_new;call super::pcd_new;string ls_modify

ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)
end event

type dw_lista from uo_cs_xx_dw within w_parametri_valutazione_for
integer x = 14
integer y = 324
integer width = 3593
integer height = 1836
integer taborder = 10
string dataobject = "d_parametri_omnia_valfor"
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_filter

setfilter("")
ls_filter = wf_parametri_valutazione()
if ls_filter = "NOPARAM" then
	parent.postevent("pc_close")
else
	setfilter(ls_filter)
	ll_errore = retrieve(s_cs_xx.cod_azienda)
	
	if ll_errore < 0 then
		pcca.error = c_fatal
	end if
	
	dw_lista.filter( )
end if
end event

event pcd_savebefore;call super::pcd_savebefore;long ll_i
datetime ls_date

for ll_i = 1 to this.rowcount()
   choose case this.getitemstring(ll_i, "flag_parametro")
      case "S"
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "data", ls_date)
         this.setitem(ll_i, "numero", 0)
      case "F"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "data", ls_date)
         this.setitem(ll_i, "numero", 0)
      case "D"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "numero", 0)
      case "N"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "data", ls_date)
   end choose
next
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_new;call super::pcd_new;//dw_folder.fu_selecttab(2)
end event

event pcd_modify;call super::pcd_modify;//dw_folder.fu_selecttab(2)
end event

event pcd_save;call super::pcd_save;//dw_folder.fu_selecttab(1)
end event

type dw_folder from u_folder within w_parametri_valutazione_for
integer x = 5
integer y = 196
integer width = 3259
integer height = 1968
integer taborder = 30
boolean border = false
end type

