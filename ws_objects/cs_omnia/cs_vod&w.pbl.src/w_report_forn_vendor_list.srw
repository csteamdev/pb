﻿$PBExportHeader$w_report_forn_vendor_list.srw
$PBExportComments$Finestra Gestione Report Fornitori in o non in Vendor List
forward
global type w_report_forn_vendor_list from w_cs_xx_principale
end type
type dw_sel_report_forn_vendor_list from uo_cs_xx_dw within w_report_forn_vendor_list
end type
type cb_annulla from commandbutton within w_report_forn_vendor_list
end type
type cb_report from commandbutton within w_report_forn_vendor_list
end type
type dw_report_forn_vendor_list from uo_cs_xx_dw within w_report_forn_vendor_list
end type
type dw_folder from u_folder within w_report_forn_vendor_list
end type
end forward

global type w_report_forn_vendor_list from w_cs_xx_principale
integer width = 4709
integer height = 2280
string title = "Report ~"Vendor List~""
dw_sel_report_forn_vendor_list dw_sel_report_forn_vendor_list
cb_annulla cb_annulla
cb_report cb_report
dw_report_forn_vendor_list dw_report_forn_vendor_list
dw_folder dw_folder
end type
global w_report_forn_vendor_list w_report_forn_vendor_list

type variables
string is_flag_tipo_nc
end variables

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify
windowobject lw_oggetti[]

dw_report_forn_vendor_list.ib_dw_report = true

//------------------------------------------------------------------------------
lw_oggetti[1] = dw_report_forn_vendor_list
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])
//------------------------------------------------------------------------------
lw_oggetti[1] = dw_sel_report_forn_vendor_list
//lw_oggetti[2] = cb_ricerca_fornitore
lw_oggetti[2] = cb_annulla
lw_oggetti[3] = cb_report
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
//-----------------------------------------------------------------------------

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

set_w_options(c_closenosave + c_autoposition) // + c_noresizewin)

dw_report_forn_vendor_list.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_nomodify + &
                                c_nodelete + &
                                c_newonopen + &
                                c_disableCC, &
                                c_noresizedw + &
                                c_nohighlightselected + &
                                c_nocursorrowpointer +&
                                c_nocursorrowfocusrect )
dw_sel_report_forn_vendor_list.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_nomodify + &
                                    c_nodelete + &
                                    c_newonopen + &
                                    c_disableCC, &
                                    c_noresizedw + &
                                    c_nohighlightselected + &
                                    c_cursorrowpointer)

iuo_dw_main = dw_report_forn_vendor_list

//this.x = 800
//this.y = 600
//this.width = 2650//1380
//this.height = 850//810

//dw_sel_report_forn_vendor_list.setfocus()

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)

if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","Manca il parametro LOA in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"

dw_report_forn_vendor_list.modify(ls_modify)
end event

on w_report_forn_vendor_list.create
int iCurrent
call super::create
this.dw_sel_report_forn_vendor_list=create dw_sel_report_forn_vendor_list
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_report_forn_vendor_list=create dw_report_forn_vendor_list
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sel_report_forn_vendor_list
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.dw_report_forn_vendor_list
this.Control[iCurrent+5]=this.dw_folder
end on

on w_report_forn_vendor_list.destroy
call super::destroy
destroy(this.dw_sel_report_forn_vendor_list)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_report_forn_vendor_list)
destroy(this.dw_folder)
end on

type dw_sel_report_forn_vendor_list from uo_cs_xx_dw within w_report_forn_vendor_list
integer x = 23
integer y = 136
integer width = 2400
integer height = 560
integer taborder = 20
string dataobject = "d_sel_report_forn_vendor_list"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_sel_report_forn_vendor_list,"cod_fornitore")
end choose
end event

type cb_annulla from commandbutton within w_report_forn_vendor_list
integer x = 1330
integer y = 708
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
boolean cancel = true
end type

event clicked;string ls_null
char lc_null
decimal ldc_null
integer li_null

//close(parent)

setnull(ls_null)
setnull(lc_null)
setnull(ldc_null)
setnull(li_null)

dw_sel_report_forn_vendor_list.setitem(1,"rs_tipo_report", 					ls_null)
dw_sel_report_forn_vendor_list.setitem(1,"rs_flag_certificato", 				lc_null)
dw_sel_report_forn_vendor_list.setitem(1,"rdec_punteggio_da", 			ldc_null)
dw_sel_report_forn_vendor_list.setitem(1,"rdec_punteggio_a", 			ldc_null)
dw_sel_report_forn_vendor_list.setitem(1,"rn_valutazione_for_anno", 	li_null)
dw_sel_report_forn_vendor_list.setitem(1,"rs_cod_fornitore", 				ls_null)


end event

type cb_report from commandbutton within w_report_forn_vendor_list
integer x = 1719
integer y = 708
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
boolean default = true
end type

event clicked;integer li_anno_valutazione_for
decimal ldec_punteggio_da, ldec_punteggio_a
string ls_tipo_report, ls_cod_fornitore, ls_filtro, ls_rag_soc_1
char lc_flag_certificato

dw_sel_report_forn_vendor_list.AcceptText()
ls_tipo_report = dw_sel_report_forn_vendor_list.getitemstring(1,"rs_tipo_report")
lc_flag_certificato = dw_sel_report_forn_vendor_list.getitemstring(1,"rs_flag_certificato")
ldec_punteggio_da = dw_sel_report_forn_vendor_list.getitemdecimal(1,"rdec_punteggio_da")
ldec_punteggio_a = dw_sel_report_forn_vendor_list.getitemdecimal(1,"rdec_punteggio_a")
li_anno_valutazione_for = dw_sel_report_forn_vendor_list.getitemnumber(1,"rn_valutazione_for_anno")
ls_cod_fornitore = dw_sel_report_forn_vendor_list.getitemstring(1,"rs_cod_fornitore")

ls_filtro = ""

if isnull(ls_tipo_report) then
	ls_tipo_report = "T"
end if
choose case ls_tipo_report
	case "S" //in Vendor List
		ls_filtro += "Fornitori in Vendor List - "
		
	case "N" //Non in Vendor List
		ls_filtro += "Fornitori Non in Vendor List - "
		
	case "T" //Tutti
		
end choose

if isnull(lc_flag_certificato) then
	lc_flag_certificato = "%"
end if
choose case lc_flag_certificato
	case "S"
		ls_filtro += "Fornitori Certificati - "
		
	case "N"
		ls_filtro += "Fornitori Non Certificati - "
		
end choose

if isnull(ldec_punteggio_da) then
	ldec_punteggio_da = -1.0000
end if
if isnull(ldec_punteggio_a) then
	ldec_punteggio_a = -1.0000
end if

if ldec_punteggio_da > 0 then
	if ldec_punteggio_a > 0 then
		ls_filtro += "con punteggio superiore a " + string(ldec_punteggio_da)
		ls_filtro += "e inferiore a " + string(ldec_punteggio_a) + " - "
	else
		ls_filtro += "con punteggio superiore a " + string(ldec_punteggio_da) + " - "
	end if
else
	if ldec_punteggio_a > 0 then
		ls_filtro += "con punteggio inferiore a " + string(ldec_punteggio_a) + " - "
	end if
end if

if isnull(li_anno_valutazione_for) then
	li_anno_valutazione_for = -1
end if
if li_anno_valutazione_for > 0 then
	ls_filtro += "Valutati nell'anno " + string(li_anno_valutazione_for) + " - "
end if

if isnull(ls_cod_fornitore) or ls_cod_fornitore = "" then
	ls_cod_fornitore = "%"
end if
if ls_cod_fornitore <> "%" then
	select rag_soc_1
	into :ls_rag_soc_1
	from anag_fornitori
	where cod_azienda = :s_cs_xx.cod_azienda and
		cod_fornitore = :ls_cod_fornitore;
		
	if isnull(ls_rag_soc_1) then
		ls_filtro += "relativi al fornitore " + ls_cod_fornitore
	else
		ls_filtro += "relativi al fornitore " + ls_rag_soc_1
	end if	
end if

dw_report_forn_vendor_list.object.t_filtro.text = ls_filtro

//dw_sel_report_forn_vendor_list.hide()

//parent.x = 100
//parent.y = 50
//parent.width = 3835 //3553
//parent.height = 2150 //1665

//dw_report_forn_vendor_list.show()

dw_report_forn_vendor_list.retrieve( s_cs_xx.cod_azienda, ls_tipo_report, &
												 lc_flag_certificato, ldec_punteggio_da, &
												 ldec_punteggio_a, li_anno_valutazione_for , &
												 ls_cod_fornitore)

dw_report_forn_vendor_list.object.datawindow.print.preview = "yes"

//cb_selezione.show()
//cb_ricerca_fornitore.hide()
dw_report_forn_vendor_list.change_dw_current()
dw_folder.fu_selecttab(2)
end event

type dw_report_forn_vendor_list from uo_cs_xx_dw within w_report_forn_vendor_list
boolean visible = false
integer x = 23
integer y = 128
integer width = 4599
integer height = 2012
integer taborder = 10
string dataobject = "d_report_forn_vendor_list"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_forn_vendor_list
integer x = 18
integer y = 16
integer width = 4626
integer height = 2148
integer taborder = 40
end type

