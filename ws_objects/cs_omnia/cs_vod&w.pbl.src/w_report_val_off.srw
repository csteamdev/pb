﻿$PBExportHeader$w_report_val_off.srw
$PBExportComments$Finestra Report Valutazione Offerte Fornitori
forward
global type w_report_val_off from w_report_std
end type
type dw_grafico from uo_cs_xx_dw within w_report_val_off
end type
type cb_sel_grafico from commandbutton within w_report_val_off
end type
type cb_grafico from commandbutton within w_report_val_off
end type
end forward

global type w_report_val_off from w_report_std
integer width = 2409
integer height = 1400
string title = "Selezione Offerte Fornitori"
dw_grafico dw_grafico
cb_sel_grafico cb_sel_grafico
cb_grafico cb_grafico
end type
global w_report_val_off w_report_val_off

forward prototypes
public function integer wf_val_ordine (ref double wd_valore_ordine, ref double wd_valore_spese, ref double wd_valore_sconti, long wl_anno_reg, long wl_num_reg, ref double wd_cambio)
public function long wf_prezzo_idelale (string ws_prodotto, string ws_fonte_dati)
end prototypes

public function integer wf_val_ordine (ref double wd_valore_ordine, ref double wd_valore_spese, ref double wd_valore_sconti, long wl_anno_reg, long wl_num_reg, ref double wd_cambio);SELECT sum(val_riga)  
INTO :wd_valore_ordine
FROM det_off_acq  
WHERE ( det_off_acq.cod_azienda = :s_cs_xx.cod_azienda ) AND  
      ( det_off_acq.anno_registrazione = :wl_anno_reg ) AND  
      ( det_off_acq.num_registrazione = :wl_num_reg ) AND  
      ( det_off_acq.cod_tipo_det_acq NOT in (  SELECT tab_tipi_det_acq.cod_tipo_det_acq  
                                               FROM tab_tipi_det_acq  
                                               WHERE ( tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda ) AND  
                                                     (tab_tipi_det_acq.flag_tipo_det_acq = 'I') OR  
                                                     (tab_tipi_det_acq.flag_tipo_det_acq = 'T') OR  
                                                     (tab_tipi_det_acq.flag_tipo_det_acq = 'B') OR  
                                                     (tab_tipi_det_acq.flag_tipo_det_acq = 'V') OR
                                                     (tab_tipi_det_acq.flag_tipo_det_acq = 'S') ))   ;
SELECT sum(val_riga)  
INTO :wd_valore_spese
FROM det_off_acq  
WHERE ( det_off_acq.cod_azienda = :s_cs_xx.cod_azienda ) AND  
      ( det_off_acq.anno_registrazione = :wl_anno_reg ) AND  
      ( det_off_acq.num_registrazione = :wl_num_reg ) AND  
      ( det_off_acq.cod_tipo_det_acq in (  SELECT tab_tipi_det_acq.cod_tipo_det_acq  
                                             FROM tab_tipi_det_acq  
                                            WHERE ( tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda ) AND  
                                                  (tab_tipi_det_acq.flag_tipo_det_acq = 'I') OR  
                                                  (tab_tipi_det_acq.flag_tipo_det_acq = 'T') OR  
                                                  (tab_tipi_det_acq.flag_tipo_det_acq = 'B') OR  
                                                  (tab_tipi_det_acq.flag_tipo_det_acq = 'V')  ))   ;
SELECT sum(val_riga)  
INTO :wd_valore_sconti 
FROM det_off_acq  
WHERE ( det_off_acq.cod_azienda = :s_cs_xx.cod_azienda ) AND  
      ( det_off_acq.anno_registrazione = :wl_anno_reg ) AND  
      ( det_off_acq.num_registrazione = :wl_num_reg ) AND  
      ( det_off_acq.cod_tipo_det_acq in (  SELECT tab_tipi_det_acq.cod_tipo_det_acq  
                                           FROM tab_tipi_det_acq  
                                           WHERE ( tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda ) AND  
                                                 (tab_tipi_det_acq.flag_tipo_det_acq = 'S') ))   ;

SELECT tes_off_acq.cambio_acq
INTO   :wd_cambio  
FROM   tes_off_acq
WHERE  (tes_off_acq.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       (tes_off_acq.anno_registrazione = :wl_anno_reg ) AND  
       (tes_off_acq.num_registrazione = :wl_num_reg )   ;


if isnull(wd_valore_ordine) then wd_valore_ordine  = 0
if isnull(wd_valore_spese) then wd_valore_spese  = 0
if isnull(wd_valore_sconti) then wd_valore_sconti  = 0
if isnull(wd_cambio) then wd_cambio  = 1
return 0
end function

public function long wf_prezzo_idelale (string ws_prodotto, string ws_fonte_dati);long ll_i, ll_prezzo_ideale, ll_prezzo_netto

ll_prezzo_ideale = 0

choose case ws_fonte_dati
	case "R"
		for ll_i = 1 to dw_report.rowcount()
		   if dw_report.getitemstring(ll_i, "cod_prodotto") = ws_prodotto then
   		   ll_prezzo_netto = ((dw_report.getitemnumber(ll_i, "cval_riga_lire") - dw_report.getitemnumber(ll_i, "ctot_spese_lire")) / dw_report.getitemnumber(ll_i, "quan_ordinata"))
		      if ll_prezzo_ideale = 0 then
     			   ll_prezzo_ideale = ll_prezzo_netto
		      else
      		   if (ll_prezzo_ideale > ll_prezzo_netto) and (ll_prezzo_netto <> 0) then
            		ll_prezzo_ideale = ll_prezzo_netto
		         end if
		      end if
		   end if
		next 
	case "G"
		for ll_i = 1 to dw_grafico.rowcount()
		   if dw_grafico.getitemstring(ll_i, "cod_prodotto") = ws_prodotto then
   		   ll_prezzo_netto = ((dw_grafico.getitemnumber(ll_i, "cval_riga_lire") - dw_grafico.getitemnumber(ll_i, "ctot_spese_lire")) / dw_grafico.getitemnumber(ll_i, "quan_ordinata"))
		      if ll_prezzo_ideale = 0 then
     			   ll_prezzo_ideale = ll_prezzo_netto
		      else
      		   if (ll_prezzo_ideale > ll_prezzo_netto) and (ll_prezzo_netto <> 0) then
            		ll_prezzo_ideale = ll_prezzo_netto
		         end if
		      end if
		   end if
		next 
end choose		

return ll_prezzo_ideale

end function

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"rs_cat_mer",sqlca,&
                 "tab_cat_mer","cod_cat_mer","des_cat_mer", &
                 "tab_cat_mer.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"rs_da_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1", &
                 "anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_selezione,"rs_a_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1", &
                 "anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_selezione,"rs_da_for_pot",sqlca,&
                 "anag_for_pot","cod_for_pot","rag_soc_1", &
                 "anag_for_pot.cod_azienda = '" + s_cs_xx.cod_azienda + "'")					

f_PO_LoadDDDW_DW(dw_selezione,"rs_a_for_pot",sqlca,&
                 "anag_for_pot","cod_for_pot","rag_soc_1", &
                 "anag_for_pot.cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  					  
end event

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

dw_grafico.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )

this.x = 673
this.y = 265
this.width = 2414
this.height = 1401

end event

on w_report_val_off.create
int iCurrent
call super::create
this.dw_grafico=create dw_grafico
this.cb_sel_grafico=create cb_sel_grafico
this.cb_grafico=create cb_grafico
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_grafico
this.Control[iCurrent+2]=this.cb_sel_grafico
this.Control[iCurrent+3]=this.cb_grafico
end on

on w_report_val_off.destroy
call super::destroy
destroy(this.dw_grafico)
destroy(this.cb_sel_grafico)
destroy(this.cb_grafico)
end on

type cb_annulla from w_report_std`cb_annulla within w_report_val_off
integer x = 1211
integer y = 1200
integer taborder = 40
string text = "&Chiudi"
end type

type cb_report from w_report_std`cb_report within w_report_val_off
integer x = 1989
integer y = 1200
integer taborder = 50
end type

event cb_report::clicked;call super::clicked;string ls_prodotto, ls_cat_mer, ls_cod_fornitore, ls_ordinamento, ls_sql, ls_cod_prodotto, &
       ls_str, ls_da_fornitore, ls_a_fornitore, ls_da_for_pot, ls_a_for_pot, ls_tipo_sort, &
       ls_valutaz_for
integer li_ritorno
long   ll_i, ll_anno_registrazione, ll_num_registrazione , ll_new_val_riga, ll_val_off, &
       ll_retrieve, ll_peso_qualita, ll_peso_prezzo, ll_valore_riga, ll_prezzo, &
		 ll_spese_riga, ll_prezzo_ideale, ll_peso_ritardo, ll_peso_anticipo, ll_giorni
double ll_valore_offerta, ll_valore_spese, ll_valore_sconti, ll_valutaz_prezzo, ld_cambio, &
       VCD1,VCD2,VCD,VCE,VS1,VS2,VS3,VS,VQ,VT
datetime   ld_da_data, ld_a_data, ld_da_data_val, ld_a_data_val,ld_data_riferimento, ld_data



dw_selezione.setcolumn(1)
parent.SetMicroHelp ( "Elaborazione Report Valutazione Offerte Fornitori" )
ld_da_data          = dw_selezione.getitemdatetime(1,"rd_da_data")
ld_a_data           = dw_selezione.getitemdatetime(1,"rd_a_data")
ls_prodotto         = dw_selezione.getitemstring(1,"rs_cod_prodotto")
ls_cat_mer          = dw_selezione.getitemstring(1,"rs_cat_mer")
ld_da_data_val      = dw_selezione.getitemdatetime(1,"rd_da_data_val")
ld_a_data_val       = dw_selezione.getitemdatetime(1,"rd_a_data_val")
ll_peso_qualita     = dw_selezione.getitemnumber(1,"rn_peso_qualita")
ll_peso_prezzo      = dw_selezione.getitemnumber(1,"rn_peso_prezzo")
ll_peso_ritardo     = dw_selezione.getitemnumber(1,"rn_penalita_gg_ritardo")
ll_peso_anticipo    = dw_selezione.getitemnumber(1,"rn_penalita_gg_anticipo")
ld_data_riferimento = dw_selezione.getitemdatetime(1,"rd_data_cons_riferimento")
ls_da_fornitore     = dw_selezione.getitemstring(1,"rs_da_fornitore")
ls_a_fornitore      = dw_selezione.getitemstring(1,"rs_a_fornitore")
ls_da_for_pot     = dw_selezione.getitemstring(1,"rs_da_for_pot")
ls_a_for_pot      = dw_selezione.getitemstring(1,"rs_a_for_pot")
ls_valutaz_for    = dw_selezione.getitemstring(1,"rs_valutaz_for")

if (ll_peso_qualita + ll_peso_prezzo) <> 100 then
	g_mb.messagebox("Valutazione Offerte", "La sommatoria dei pesi qualità e prezzo deve essere ugugale a 100", StopSign!)
   return
end if	

if isnull(ls_prodotto) then ls_prodotto = "%"
if isnull(ls_cat_mer)  then  ls_cat_mer  = "%"
if isnull(ld_da_data_val) then  ld_da_data_val = datetime(date(01/01/1900))
if isnull(ld_a_data_val)  then  ld_a_data_val  = datetime(date(31/12/2999))
if isnull(ld_da_data)     then  ld_da_data     = datetime(date(01/01/1900))
if isnull(ld_a_data)      then  ld_a_data      = datetime(date(31/12/2999))

dw_selezione.hide()
parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665
setpointer(hourglass!)

// -------------------------------------------------------------------------------------------
// -----------------------  imposto ordinamento righe desiderato -----------------------------
// -------------------------------------------------------------------------------------------

if dw_selezione.getitemstring(1, "rs_asc_desc") = "C" then
   ls_tipo_sort = " A"
else
   ls_tipo_sort = " D"
end if

ls_ordinamento = "cod_prodotto A, "
choose case dw_selezione.getitemstring(1, "rs_tipo_riordino")
case "P" 
   ls_ordinamento = ls_ordinamento + "anag_for_pot_rag_soc_1" + ls_tipo_sort
case "F" 
   ls_ordinamento = ls_ordinamento + "tes_off_acq_cod_fornitore" + ls_tipo_sort
case "R" 
   ls_ordinamento = ls_ordinamento + "val_riga" + ls_tipo_sort
case "D" 
   ls_ordinamento = ls_ordinamento + "data_consegna" + ls_tipo_sort
end choose


dw_report.setsort(ls_ordinamento)
dw_report.sort()
dw_report.GroupCalc( )

// -------------------------------------------------------------------------------------------
// -----------------------  ricerco dati report  ---------------------------------------------
// -------------------------------------------------------------------------------------------
parent.SetMicroHelp ( "Caricamento dati report in corso ..." )

dw_report.show()
ll_retrieve = dw_report.retrieve(s_cs_xx.cod_azienda, ls_prodotto, ls_cat_mer, ls_da_fornitore, ls_a_fornitore, ld_da_data, ld_a_data, ls_da_for_pot, ls_a_for_pot)
cb_selezione.show()
cb_sel_grafico.hide()
dw_report.change_dw_current()
choose case ll_retrieve
	case 0
		g_mb.messagebox("Valutazione Offerte Fornitori","Nessun dato incluso nella corrente selezione; ritorno nella maschera di selezione", Information!)
   	cb_selezione.triggerevent("clicked")
 	   return
	case is < 0
		g_mb.messagebox("Valutazione Offerte Fornitori","Errore durante la ricerca dei dati", StopSign!)
   	cb_selezione.triggerevent("clicked")
 	   return
end choose
// -------------------------------------------------------------------------------------------

parent.SetMicroHelp ( "Calcolo valutazione dei fornitori in corso ..." )

for ll_i = 1 to dw_report.rowcount()

   ll_anno_registrazione = dw_report.getitemnumber(ll_i, "anno_registrazione")
   ll_num_registrazione  = dw_report.getitemnumber(ll_i, "num_registrazione")

   ll_valore_offerta = 0
   ll_valore_spese = 0
	ll_valore_sconti = 0
	wf_val_ordine(ll_valore_offerta, &
                 ll_valore_spese, &
                 ll_valore_sconti, &
                 ll_anno_registrazione, &
                 ll_num_registrazione, &
					  ld_cambio)

   ll_valore_riga = dw_report.getitemnumber(ll_i, "val_riga") * ld_cambio
	dw_report.setitem(ll_i,"val_riga",ll_valore_riga)
   ll_valore_offerta = round(ll_valore_offerta * ld_cambio, 0)
   ll_valore_spese = round(ll_valore_spese * ld_cambio, 0)
   ll_valore_sconti = round(ll_valore_sconti * ld_cambio, 0)


   if ll_valore_offerta <> 0 then
      ll_spese_riga   = ((ll_valore_spese - ll_valore_sconti)*((ll_valore_riga * 100) / ll_valore_offerta) /100)
      ll_new_val_riga = ll_valore_riga + ll_spese_riga
   else
      ll_new_val_riga = ll_valore_riga
   end if
   dw_report.setitem(ll_i, "ctot_spese_lire", ll_spese_riga)
// --------------  il  valore riga rimane così come è ( Enrico 18/02/97 ) -------------- 
   dw_report.setitem(ll_i, "cval_riga_lire", ll_new_val_riga)


   if ls_valutaz_for = "S" then
	   ls_cod_fornitore = dw_report.getitemstring(ll_i, "tes_off_acq_cod_fornitore")
		if not isnull(ls_cod_fornitore) then
      li_ritorno = f_carica_val_fornitori(ls_cod_fornitore,VCD1,VCD2,VCD,VCE,VS1,VS2,VS3,VS,VQ,VT, ld_da_data_val, ld_a_data_val, true)
      if li_ritorno < 0 then VT = 0
//            se è avvenuto un errore durante la valutazione 
//            del fornitore metto a zero il risultato il punteggio
	   end if
	else
		VT = 0
	end if
   VT = round(VT, 1)
   dw_report.setitem(ll_i, "cvalutaz_qualita", VT)

next

parent.SetMicroHelp ( "Calcolo valutazione della valutazione del prezzo in corso ..." )
for ll_i = 1 to dw_report.rowcount()
   ll_prezzo = ((dw_report.getitemnumber(ll_i, "cval_riga_lire") + dw_report.getitemnumber(ll_i, "ctot_spese_lire")) / dw_report.getitemnumber(ll_i, "quan_ordinata"))
   ls_cod_prodotto = dw_report.getitemstring(ll_i, "cod_prodotto")
   ll_prezzo_ideale = wf_prezzo_idelale(ls_cod_prodotto, "R")
   if ll_prezzo <> 0 then
  		ll_valutaz_prezzo = round((ll_prezzo_ideale/ ll_prezzo)  * 100, 1)
      dw_report.setitem(ll_i, "cvalutaz_prezzo", ll_valutaz_prezzo )
      ll_val_off = ( ll_valutaz_prezzo * ll_peso_prezzo) + (dw_report.getitemnumber(ll_i, "cvalutaz_qualita") * ll_peso_qualita)
   else
		ll_val_off = 0
   end if
   if not isnull(ld_data_riferimento) and (ld_data_riferimento > datetime(date(01/01/1900))) then
      if not isnull(dw_report.getitemdatetime(ll_i, "data_consegna")) or dw_report.getitemdatetime(ll_i, "data_consegna") > datetime(date(01/01/1900)) then
         ll_giorni = daysafter(date(ld_data_riferimento), date(dw_report.getitemdatetime(ll_i, "data_consegna")))
         choose case ll_giorni
			   case is > 0
				   ll_giorni = ll_giorni * ll_peso_ritardo
   			case is < 0
	   			ll_giorni = ll_giorni * ll_peso_anticipo
		   end choose		
   		ll_val_off = round((ll_val_off / 100) - abs(ll_giorni), 1)
	   	if ll_val_off < 0 then ll_val_off = 0
      else
			ll_val_off = 0
	   end if
   	dw_report.setitem(ll_i, "cvalutaz_off_acq", ll_val_off)
   else
   	dw_report.setitem(ll_i, "cvalutaz_off_acq", round(ll_val_off/100, 1))
   end if		
next

parent.SetMicroHelp ( "Report valutazione offerte fornitori" )

setpointer(arrow!)



end event

type cb_selezione from w_report_std`cb_selezione within w_report_val_off
integer taborder = 70
string text = "&Selezione"
end type

event cb_selezione::clicked;call super::clicked;parent.x = 673
parent.y = 265
parent.width = 2414
parent.height = 1401


end event

type dw_selezione from w_report_std`dw_selezione within w_report_val_off
integer width = 2331
integer height = 1160
integer taborder = 30
string dataobject = "d_sel_rep_val_off"
end type

event dw_selezione::itemchanged;call super::itemchanged;if i_extendmode then
	choose case i_colname
		case "rs_da_fornitore"
  		 	if not isnull(i_coltext) then setitem(1, "rs_a_fornitore", i_coltext)
		case "rs_da_for_pot"
  		 	if not isnull(i_coltext) then setitem(1, "rs_a_for_pot", i_coltext)
	end choose
end if		
		
		
		
end event

type dw_report from w_report_std`dw_report within w_report_val_off
integer y = 0
integer taborder = 20
string dataobject = "d_report_val_off"
end type

type dw_folder from w_report_std`dw_folder within w_report_val_off
end type

type dw_grafico from uo_cs_xx_dw within w_report_val_off
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_grafico_val_off"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type cb_sel_grafico from commandbutton within w_report_val_off
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

dw_selezione.show()

dw_grafico.hide()
cb_sel_grafico.hide()

dw_selezione.change_dw_current()

parent.x = 673
parent.y = 265
parent.width = 2414
parent.height = 1401

end event

type cb_grafico from commandbutton within w_report_val_off
event clicked pbm_bnclicked
integer x = 1600
integer y = 1200
integer width = 366
integer height = 80
integer taborder = 41
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Grafico"
end type

event clicked;call super::clicked;string ls_prodotto, ls_cat_mer, ls_cod_fornitore, ls_ordinamento, ls_sql, ls_cod_prodotto, &
       ls_str, ls_da_fornitore, ls_a_fornitore, ls_da_for_pot, ls_a_for_pot, ls_tipo_sort, &
       ls_valutaz_for
integer li_ritorno
long   ll_i, ll_anno_registrazione, ll_num_registrazione , ll_new_val_riga, ll_val_off, &
       ll_retrieve, ll_peso_qualita, ll_peso_prezzo, ll_valore_riga, ll_prezzo, &
		 ll_spese_riga, ll_prezzo_ideale, ll_peso_ritardo, ll_peso_anticipo, ll_giorni
double ll_valore_offerta, ll_valore_spese, ll_valore_sconti, ll_valutaz_prezzo, ld_cambio, &
       VCD1,VCD2,VCD,VCE,VS1,VS2,VS3,VS,VQ,VT
datetime   ld_da_data, ld_a_data, ld_da_data_val, ld_a_data_val,ld_data_riferimento, ld_data



dw_selezione.setcolumn(1)
parent.SetMicroHelp ( "Elaborazione Report Valutazione Offerte Fornitori" )
ld_da_data          = dw_selezione.getitemdatetime(1,"rd_da_data")
ld_a_data           = dw_selezione.getitemdatetime(1,"rd_a_data")
ls_prodotto         = dw_selezione.getitemstring(1,"rs_cod_prodotto")
ls_cat_mer          = dw_selezione.getitemstring(1,"rs_cat_mer")
ld_da_data_val      = dw_selezione.getitemdatetime(1,"rd_da_data_val")
ld_a_data_val       = dw_selezione.getitemdatetime(1,"rd_a_data_val")
ll_peso_qualita     = dw_selezione.getitemnumber(1,"rn_peso_qualita")
ll_peso_prezzo      = dw_selezione.getitemnumber(1,"rn_peso_prezzo")
ll_peso_ritardo     = dw_selezione.getitemnumber(1,"rn_penalita_gg_ritardo")
ll_peso_anticipo    = dw_selezione.getitemnumber(1,"rn_penalita_gg_anticipo")
ld_data_riferimento = dw_selezione.getitemdatetime(1,"rd_data_cons_riferimento")
ls_da_fornitore     = dw_selezione.getitemstring(1,"rs_da_fornitore")
ls_a_fornitore      = dw_selezione.getitemstring(1,"rs_a_fornitore")
ls_da_for_pot     = dw_selezione.getitemstring(1,"rs_da_for_pot")
ls_a_for_pot      = dw_selezione.getitemstring(1,"rs_a_for_pot")
ls_valutaz_for    = dw_selezione.getitemstring(1,"rs_valutaz_for")

if (ll_peso_qualita + ll_peso_prezzo) <> 100 then
	g_mb.messagebox("Valutazione Offerte", "La sommatoria dei pesi qualità e prezzo deve essere ugugale a 100", StopSign!)
   return
end if	

if isnull(ls_prodotto) then ls_prodotto = "%"
if isnull(ls_cat_mer)  then  ls_cat_mer  = "%"
if isnull(ld_da_data_val) then  ld_da_data_val = datetime(date(01/01/1900),time(00:00:00))
if isnull(ld_a_data_val)  then  ld_a_data_val  = datetime(date(31/12/2999),time(00:00:00))
if isnull(ld_da_data)     then  ld_da_data     = datetime(date(01/01/1900),time(00:00:00))
if isnull(ld_a_data)      then  ld_a_data      = datetime(date(31/12/2999),time(00:00:00))

dw_selezione.hide()
parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665
setpointer(hourglass!)

// -------------------------------------------------------------------------------------------
// -----------------------  imposto ordinamento righe desiderato -----------------------------
// -------------------------------------------------------------------------------------------

if dw_selezione.getitemstring(1, "rs_asc_desc") = "C" then
   ls_tipo_sort = " A"
else
   ls_tipo_sort = " D"
end if

ls_ordinamento = "cod_prodotto A, "
choose case dw_selezione.getitemstring(1, "rs_tipo_riordino")
case "P" 
   ls_ordinamento = ls_ordinamento + "anag_for_pot_rag_soc_1" + ls_tipo_sort
case "F" 
   ls_ordinamento = ls_ordinamento + "tes_off_acq_cod_fornitore" + ls_tipo_sort
case "R" 
   ls_ordinamento = ls_ordinamento + "val_riga" + ls_tipo_sort
case "D" 
   ls_ordinamento = ls_ordinamento + "data_consegna" + ls_tipo_sort
end choose


dw_grafico.setsort(ls_ordinamento)
dw_grafico.sort()
dw_grafico.GroupCalc( )

// -------------------------------------------------------------------------------------------
// -----------------------  ricerco dati report  ---------------------------------------------
// -------------------------------------------------------------------------------------------
parent.SetMicroHelp ( "Caricamento dati report in corso ..." )

dw_grafico.show()
ll_retrieve = dw_grafico.retrieve(s_cs_xx.cod_azienda, ls_prodotto, ls_cat_mer, ls_da_fornitore, ls_a_fornitore, ld_da_data, ld_a_data, ls_da_for_pot, ls_a_for_pot)
cb_sel_grafico.show()
cb_selezione.hide()
dw_grafico.change_dw_current()
choose case ll_retrieve
	case 0
		g_mb.messagebox("Valutazione Offerte Fornitori","Nessun dato incluso nella corrente selezione; ritorno nella maschera di selezione", Information!)
   	cb_selezione.triggerevent("clicked")
 	   return
	case is < 0
		g_mb.messagebox("Valutazione Offerte Fornitori","Errore durante la ricerca dei dati", StopSign!)
   	cb_selezione.triggerevent("clicked")
 	   return
end choose
// -------------------------------------------------------------------------------------------

parent.SetMicroHelp ( "Calcolo valutazione dei fornitori in corso ..." )

for ll_i = 1 to dw_grafico.rowcount()

   ll_anno_registrazione = dw_grafico.getitemnumber(ll_i, "anno_registrazione")
   ll_num_registrazione  = dw_grafico.getitemnumber(ll_i, "num_registrazione")

   ll_valore_offerta = 0
   ll_valore_spese = 0
	ll_valore_sconti = 0
	wf_val_ordine(ll_valore_offerta, &
                 ll_valore_spese, &
                 ll_valore_sconti, &
                 ll_anno_registrazione, &
                 ll_num_registrazione, &
					  ld_cambio)

   ll_valore_riga = dw_grafico.getitemnumber(ll_i, "val_riga") * ld_cambio
   ll_valore_offerta = round(ll_valore_offerta * ld_cambio, 0)
   ll_valore_spese = round(ll_valore_spese * ld_cambio, 0)
   ll_valore_sconti = round(ll_valore_sconti * ld_cambio, 0)


   if ll_valore_offerta <> 0 then
      ll_spese_riga   = ((ll_valore_spese - ll_valore_sconti)*((ll_valore_riga * 100) / ll_valore_offerta) /100)
      ll_new_val_riga = ll_valore_riga + ll_spese_riga
   else
      ll_new_val_riga = ll_valore_riga
   end if
   dw_grafico.setitem(ll_i, "ctot_spese_lire", ll_spese_riga)
   dw_grafico.setitem(ll_i, "cval_riga_lire", ll_new_val_riga)


   if ls_valutaz_for = "S" then
	   ls_cod_fornitore = dw_grafico.getitemstring(ll_i, "tes_off_acq_cod_fornitore")
		if not isnull(ls_cod_fornitore) then
      li_ritorno = f_carica_val_fornitori(ls_cod_fornitore,VCD1,VCD2,VCD,VCE,VS1,VS2,VS3,VS,VQ,VT, ld_da_data_val, ld_a_data_val, true)
      if li_ritorno < 0 then VT = 0
//            se è avvenuto un errore durante la valutazione 
//            del fornitore metto a zero il risultato il punteggio
	   end if
	else
		VT = 0
	end if
   VT = round(VT, 1)
   dw_grafico.setitem(ll_i, "cvalutaz_qualita", VT)

next

parent.SetMicroHelp ( "Calcolo valutazione della valutazione del prezzo in corso ..." )
for ll_i = 1 to dw_grafico.rowcount()
   ll_prezzo = ((dw_grafico.getitemnumber(ll_i, "cval_riga_lire") + dw_grafico.getitemnumber(ll_i, "ctot_spese_lire")) / dw_grafico.getitemnumber(ll_i, "quan_ordinata"))
   ls_cod_prodotto = dw_grafico.getitemstring(ll_i, "cod_prodotto")
   ll_prezzo_ideale = wf_prezzo_idelale(ls_cod_prodotto, "G")
   if ll_prezzo <> 0 then
  		ll_valutaz_prezzo = round((ll_prezzo_ideale/ ll_prezzo)  * 100, 1)
      dw_grafico.setitem(ll_i, "cvalutaz_prezzo", ll_valutaz_prezzo )
      ll_val_off = ( ll_valutaz_prezzo * ll_peso_prezzo) + (dw_grafico.getitemnumber(ll_i, "cvalutaz_qualita") * ll_peso_qualita)
   else
		ll_val_off = 0
   end if
   if not isnull(ld_data_riferimento) and (ld_data_riferimento > datetime(date(01/01/1900),time(00:00:00))) then
      if not isnull(dw_grafico.getitemdatetime(ll_i, "data_consegna")) or dw_grafico.getitemdatetime(ll_i, "data_consegna") > datetime(date(01/01/1900)) then
         ll_giorni = daysafter(date(ld_data_riferimento),date( dw_grafico.getitemdatetime(ll_i, "data_consegna")) )
         choose case ll_giorni
			   case is > 0
				   ll_giorni = ll_giorni * ll_peso_ritardo
   			case is < 0
	   			ll_giorni = ll_giorni * ll_peso_anticipo
		   end choose		
   		ll_val_off = round((ll_val_off / 100) - abs(ll_giorni), 1)
	   	if ll_val_off < 0 then ll_val_off = 0
      else
			ll_val_off = 0
	   end if
   	dw_grafico.setitem(ll_i, "cvalutaz_off_acq", ll_val_off)
   else
   	dw_grafico.setitem(ll_i, "cvalutaz_off_acq", round(ll_val_off/100, 1))
   end if		
next

parent.SetMicroHelp ( "Report valutazione offerte fornitori" )

setpointer(arrow!)
end event

