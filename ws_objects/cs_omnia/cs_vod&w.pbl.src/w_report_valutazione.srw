﻿$PBExportHeader$w_report_valutazione.srw
$PBExportComments$Finestra Report Valutazione Fornitori
forward
global type w_report_valutazione from w_cs_xx_principale
end type
type dw_scheda_valutazione from uo_cs_xx_dw within w_report_valutazione
end type
end forward

global type w_report_valutazione from w_cs_xx_principale
integer width = 3790
integer height = 2428
string title = "Report Scheda Valutazione Fornitore"
dw_scheda_valutazione dw_scheda_valutazione
end type
global w_report_valutazione w_report_valutazione

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify

dw_scheda_valutazione.ib_dw_report = true

dw_scheda_valutazione.set_dw_options(sqlca,pcca.null_object,c_disableCC+c_disableCCinsert,c_default)

dw_scheda_valutazione.object.datawindow.print.preview = "Yes"

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)

if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","Manca il parametro LOA in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"

dw_scheda_valutazione.modify(ls_modify)
end event

on w_report_valutazione.create
int iCurrent
call super::create
this.dw_scheda_valutazione=create dw_scheda_valutazione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_scheda_valutazione
end on

on w_report_valutazione.destroy
call super::destroy
destroy(this.dw_scheda_valutazione)
end on

type dw_scheda_valutazione from uo_cs_xx_dw within w_report_valutazione
integer x = 23
integer y = 20
integer width = 3703
integer height = 2280
string dataobject = "r_scheda_valutazione"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   s_cs_xx.parametri.parametro_s_1, &
                   s_cs_xx.parametri.parametro_i_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

