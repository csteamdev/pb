﻿$PBExportHeader$w_report_vendor_list_formule.srw
$PBExportComments$Finestra Gestione Report Fornitori in o non in Vendor List
forward
global type w_report_vendor_list_formule from w_cs_xx_principale
end type
type dw_folder from u_folder within w_report_vendor_list_formule
end type
type dw_report from uo_cs_xx_dw within w_report_vendor_list_formule
end type
type dw_selezione from uo_cs_xx_dw within w_report_vendor_list_formule
end type
end forward

global type w_report_vendor_list_formule from w_cs_xx_principale
integer width = 3927
integer height = 2280
string title = "Report ~"Vendor List~""
dw_folder dw_folder
dw_report dw_report
dw_selezione dw_selezione
end type
global w_report_vendor_list_formule w_report_vendor_list_formule

type variables
string					is_flag_tipo_nc
datastore			ids_tes_report_vendor_list, ids_det_report_vendor_list
end variables

forward prototypes
public subroutine wf_annulla ()
private function integer wf_report (ref string fs_errore)
public function string wf_mese_anno (long fl_mese, long fl_anno)
public subroutine wf_get_fine_mese (long fl_num_mese, long fl_anno, ref datetime fdt_data)
public function long wf_salva_valutazione (ref string fs_errore)
public function integer wf_creazione_datastore (ref string fs_errore, ref long fl_righe_testata, ref long fl_righe_dettaglio)
end prototypes

public subroutine wf_annulla ();string ls_null
datetime ldt_null

setnull(ls_null)
setnull(ldt_null)


dw_selezione.setitem(1,"data_dal", 						ldt_null)
dw_selezione.setitem(1,"data_al", 						ldt_null)
dw_selezione.setitem(1,"cod_area_aziendale", 		ls_null)
dw_selezione.setitem(1,"cod_fornitore", 				ls_null)


end subroutine

private function integer wf_report (ref string fs_errore);string						ls_cod_fornitore, ls_select, ls_mese_anno
datetime					ldt_data_dal, ldt_data_al, ldt_oggi
long						ll_anno, ll_mese, ll_anno_fine, ll_mese_fine, ll_cursore, ll_cursore_fine, ll_new, ll_riga_ds
//boolean					lb_almeno_uno = false
uo_formule_calcolo	luo_formule
decimal					ld_risultato
date						ldt_temp
decimal					ld_forniture_tot
decimal					ld_nc_tot_a, ld_nc_tot_b, ld_nc_tot_c, ld_nc_tot_d
decimal					ld_iqpa_tot_a, ld_iqpa_tot_b, ld_iqpa_tot_c, ld_iqpa_tot_d

dw_selezione.accepttext()

dw_selezione.object.t_log.text = "Inizio elaborazione ..."

dw_report.reset()

ids_tes_report_vendor_list.reset()
ids_det_report_vendor_list.reset()

ldt_oggi = datetime(today(), now())

//----------------------------
ldt_data_dal = dw_selezione.getitemdatetime(1, "data_dal")

if isnull(ldt_data_dal) or year(date(ldt_data_dal))<=1950 then
	fs_errore = "Indicare il valore iniziale del periodo di valutazione!"
	
	return -1
end if

//----------------------------
ldt_data_al = dw_selezione.getitemdatetime(1, "data_al")

if isnull(ldt_data_al) or year(date(ldt_data_al))<=1950 then
	fs_errore = "Indicare il valore finale del periodo di valutazione!"
	
	return -1
end if

if ldt_data_al < ldt_data_dal then
	fs_errore = "Valori iniziale e finale del periodo di valutazione incongruenti!"
	
	return -1
end if

//prepara la data inizio con il primo del mese
ldt_temp = date( ldt_data_dal )
ldt_data_dal = datetime(date(year( ldt_temp ), month( ldt_temp ), 1) , 00:00:00 )

//restituisce la data fine con ultimo giorno del mese
wf_get_fine_mese(month(date(ldt_data_al)), year(date(ldt_data_al)), ldt_data_al)

//----------------------------
ls_cod_fornitore = dw_selezione.getitemstring(1, "cod_fornitore")

if isnull(ls_cod_fornitore) or len(ls_cod_fornitore) < 0 then
	fs_errore = "Indicare il fornitore!"
	
	return -1
end if

ll_mese = month(date(ldt_data_dal))
ll_anno = year(date(ldt_data_dal))
ll_anno_fine = year(date(ldt_data_al))
ll_mese_fine = month(date(ldt_data_al))

ll_cursore_fine = long(string(ll_anno_fine) + right("00"+string(ll_mese_fine), 2) )	//es 201108  agosto 2011 oppure 201111   novembre 2011

luo_formule = create uo_formule_calcolo

//carico variabili
luo_formule.iuo_cache.set("#S:AZIENDA#", "'"+s_cs_xx.cod_azienda+"'" )
luo_formule.iuo_cache.set("#S:F:CODFORN#", "'"+ls_cod_fornitore+"'" )
//luo_formule.iuo_cache.set("#S:TIPI_NC_A#", "'"+"A"+"'" )
//luo_formule.iuo_cache.set("#S:TIPI_NC_B#", "'"+"X"+"'" )
//luo_formule.iuo_cache.set("#S:TIPI_NC_C#", "'"+"X"+"'" )
//luo_formule.iuo_cache.set("#S:TIPI_NC_D#", "'"+"X"+"'" )

luo_formule.iuo_cache.set("#D:DATA_DAL#", "'"+string(ldt_data_dal, "yyyymmdd")+"'" )
luo_formule.iuo_cache.set("#D:DATA_AL#", "'"+string(ldt_data_al, "yyyymmdd")+"'" )


//valuta le formule di testata #####################################
//Totale forniture
dw_selezione.object.t_log.text = "Elaborazione totale forniture ..."
if luo_formule.uof_calcola_formula("[F_TOT]", ls_select, ld_forniture_tot, fs_errore) < 0 then
	destroy luo_formule;
	
	return -1
end if

if ld_forniture_tot>0 then
else
	//inutile continuare
	fs_errore = "Non esistono accettazioni per il fornitore "+ls_cod_fornitore+" - " + &
										f_des_tabella("anag_fornitori","cod_fornitore = '" +  ls_cod_fornitore +"'", "rag_soc_1")+" "+&
										"nel periodo dal "+string(ldt_data_dal, "dd/mm/yyyy")+ " al "+string(ldt_data_al, "dd/mm/yyyy")
	return -1
end if

//Totale NC A
dw_selezione.object.t_log.text = "Elaborazione totale NC tipo A ..."
if luo_formule.uof_calcola_formula("[NC_TOT_A]", ls_select, ld_nc_tot_a, fs_errore) < 0 then
	destroy luo_formule;
	
	return -1
end if
//Totale NC B
dw_selezione.object.t_log.text = "Elaborazione totale NC tipo B ..."
if luo_formule.uof_calcola_formula("[NC_TOT_B]", ls_select, ld_nc_tot_b, fs_errore) < 0 then
	destroy luo_formule;
	
	return -1
end if
//Totale NC C
dw_selezione.object.t_log.text = "Elaborazione totale NC tipo C ..."
if luo_formule.uof_calcola_formula("[NC_TOT_C]", ls_select, ld_nc_tot_c, fs_errore) < 0 then
	destroy luo_formule;
	
	return -1
end if
//Totale NC D
dw_selezione.object.t_log.text = "Elaborazione totale NC tipo D ..."
if luo_formule.uof_calcola_formula("[NC_TOT_D]", ls_select, ld_nc_tot_d, fs_errore) < 0 then
	destroy luo_formule;
	
	return -1
end if
//Totale IQPA A
dw_selezione.object.t_log.text = "Elaborazione totale IQPA tipo A ..."
if luo_formule.uof_calcola_formula("[IQPA_TOT_A]", ls_select, ld_iqpa_tot_a, fs_errore) < 0 then
	destroy luo_formule;
	
	return -1
end if
//Totale IQPA B
dw_selezione.object.t_log.text = "Elaborazione totale IQPA tipo B ..."
if luo_formule.uof_calcola_formula("[IQPA_TOT_B]", ls_select, ld_iqpa_tot_b, fs_errore) < 0 then
	destroy luo_formule;
	
	return -1
end if
//Totale IQPA C
dw_selezione.object.t_log.text = "Elaborazione totale IQPA tipo C ..."
if luo_formule.uof_calcola_formula("[IQPA_TOT_C]", ls_select, ld_iqpa_tot_c, fs_errore) < 0 then
	destroy luo_formule;
	
	return -1
end if
//Totale IQPA D
dw_selezione.object.t_log.text = "Elaborazione totale IQPA tipo D ..."
if luo_formule.uof_calcola_formula("[IQPA_TOT_D]", ls_select, ld_iqpa_tot_d, fs_errore) < 0 then
	destroy luo_formule;
	
	return -1
end if

//salvo nel datastore DI TESTATA -----------------
ll_riga_ds = ids_tes_report_vendor_list.insertrow(0)
ids_tes_report_vendor_list.setitem(ll_riga_ds, "cod_azienda",			s_cs_xx.cod_azienda )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "cod_fornitore",		ls_cod_fornitore )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "data_val_da",			ldt_data_dal )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "data_val_a",			ldt_data_al )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "data_creazione",		datetime(date( ldt_oggi ), 00:00:00) )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "ora_creazione",		datetime(date( 1900, 1, 1 ), time( ldt_oggi ) ) )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "utente",					s_cs_xx.cod_utente )

ids_tes_report_vendor_list.setitem(ll_riga_ds, "pa",						f_dec_tabella("tab_formule_calcolo","cod_formula = '[PA]'", "numero") )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "pb",						f_dec_tabella("tab_formule_calcolo","cod_formula = '[PB]'", "numero") )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "pc",						f_dec_tabella("tab_formule_calcolo","cod_formula = '[PC]'", "numero") )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "pd",						f_dec_tabella("tab_formule_calcolo","cod_formula = '[PD]'", "numero") )

ids_tes_report_vendor_list.setitem(ll_riga_ds, "lqa_a",					f_dec_tabella("tab_formule_calcolo","cod_formula = '[LQAA]'", "numero") )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "lqa_b",					f_dec_tabella("tab_formule_calcolo","cod_formula = '[LQAB]'", "numero") )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "lqa_c",					f_dec_tabella("tab_formule_calcolo","cod_formula = '[LQAC]'", "numero") )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "lqa_d",					f_dec_tabella("tab_formule_calcolo","cod_formula = '[LQAD]'", "numero") )

ids_tes_report_vendor_list.setitem(ll_riga_ds, "iqpa_a",					ld_iqpa_tot_a )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "iqpa_b",					ld_iqpa_tot_b )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "iqpa_c",					ld_iqpa_tot_c )
ids_tes_report_vendor_list.setitem(ll_riga_ds, "iqpa_d",					ld_iqpa_tot_d )

//#####################################################


do until false		//cioè fai sempre, poi ti dico io quando uscire
	ll_cursore = long(string(ll_anno) + right("00"+string(ll_mese), 2 ) )	//es 201108  agosto 2011 oppure 201111   novembre 2011
	
	if ll_cursore>ll_cursore_fine then
		exit
	end if
	
	ll_new = dw_report.insertrow(0)
	
	//inserisco nuova riga nel datastore di dettaglio ----------------------------------------------------------------
	ll_riga_ds = ids_det_report_vendor_list.insertrow(0)
	ids_det_report_vendor_list.setitem(ll_riga_ds, "cod_azienda",			s_cs_xx.cod_azienda )
	//-----------------------------------------------------------------------------------------------------------------------
	
	//lb_almeno_uno = true
	
	//da fare su ogni nuova riga
	dw_report.setitem(ll_new, "data_dal", ldt_data_dal)
	dw_report.setitem(ll_new, "data_al", ldt_data_al)
	dw_report.setitem(ll_new, "cod_fornitore", ls_cod_fornitore)
	
	dw_report.setitem(ll_new, "forniture_tot", ld_forniture_tot )
	dw_report.setitem(ll_new, "nc_a_tot", ld_nc_tot_a )
	dw_report.setitem(ll_new, "nc_b_tot", ld_nc_tot_b )
	dw_report.setitem(ll_new, "nc_c_tot", ld_nc_tot_c )
	dw_report.setitem(ll_new, "nc_d_tot", ld_nc_tot_d )
	dw_report.setitem(ll_new, "iqpa_a_tot", ld_iqpa_tot_a )
	dw_report.setitem(ll_new, "iqpa_b_tot", ld_iqpa_tot_b )
	dw_report.setitem(ll_new, "iqpa_c_tot", ld_iqpa_tot_c )
	dw_report.setitem(ll_new, "iqpa_d_tot", ld_iqpa_tot_d )
	
	//--------------------------------------------------------------------------------
	ls_mese_anno = wf_mese_anno(ll_mese, ll_anno )
	dw_report.setitem(ll_new, "mese_anno", ls_mese_anno)
	
	
	luo_formule.iuo_cache.set("#N:ANNO#", string(ll_anno) )
	luo_formule.iuo_cache.set("#N:MESE#", string(ll_mese) )
	
	//salvo in datastore di dettaglio ---------------------------------------------------------------------------
	ids_det_report_vendor_list.setitem(ll_riga_ds, "mese_calcolo",			ll_mese )
	ids_det_report_vendor_list.setitem(ll_riga_ds, "anno_calcolo",				ll_anno )
	ids_det_report_vendor_list.setitem(ll_riga_ds, "des_mese_anno",		ls_mese_anno )
	// ---------------------------------------------------------------------------------------------------------------
	
	
	
	//valuta formule --------------------------------------------------------------------------------
	
	//forniture mese
	dw_selezione.object.t_log.text = "Elaborazione forniture          "+ls_mese_anno+" ..."
	if luo_formule.uof_calcola_formula("[F_MESE]", ls_select, ld_risultato, fs_errore) < 0 then
		destroy luo_formule;
		
		return -1
	end if
	dw_report.setitem(ll_new, "forniture_mese", ld_risultato)
	
	//salvo in datastore di dettaglio ------------------------------
	ids_det_report_vendor_list.setitem(ll_riga_ds, "n_accettazioni",			ld_risultato )
	
	//--------------------------------------------------------------------------------------------------
	
	//NC tipo A mese
	dw_selezione.object.t_log.text = "Elaborazione          NC tipo A          "+ls_mese_anno+" ..."
	if luo_formule.uof_calcola_formula("[NC_MESE_A]", ls_select, ld_risultato, fs_errore) < 0 then
		destroy luo_formule;
		
		return -1
	end if
	dw_report.setitem(ll_new, "nc_a_mese", ld_risultato)
	
	//salvo in datastore di dettaglio ------------------------------
	ids_det_report_vendor_list.setitem(ll_riga_ds, "n_nc_a_mese",			ld_risultato )
	
	//--------------------------------------------------------------------------------------------------
	
	//NC tipo B mese
	dw_selezione.object.t_log.text = "Elaborazione          NC tipo B          "+ls_mese_anno+" ..."
	if luo_formule.uof_calcola_formula("[NC_MESE_B]", ls_select, ld_risultato, fs_errore) < 0 then
		destroy luo_formule;
		
		return -1
	end if
	dw_report.setitem(ll_new, "nc_b_mese", ld_risultato)
	
	//salvo in datastore di dettaglio ------------------------------
	ids_det_report_vendor_list.setitem(ll_riga_ds, "n_nc_b_mese",			ld_risultato )
	
	//--------------------------------------------------------------------------------------------------
	
	//NC tipo C mese
	dw_selezione.object.t_log.text = "Elaborazione          NC tipo C          "+ls_mese_anno+" ..."
	if luo_formule.uof_calcola_formula("[NC_MESE_C]", ls_select, ld_risultato, fs_errore) < 0 then
		destroy luo_formule;
		
		return -1
	end if
	dw_report.setitem(ll_new, "nc_c_mese", ld_risultato)
	
	//salvo in datastore di dettaglio ------------------------------
	ids_det_report_vendor_list.setitem(ll_riga_ds, "n_nc_c_mese",			ld_risultato )
	
	//--------------------------------------------------------------------------------------------------
	
	//NC tipo D mese
	dw_selezione.object.t_log.text = "Elaborazione          NC tipo D          "+ls_mese_anno+" ..."
	if luo_formule.uof_calcola_formula("[NC_MESE_D]", ls_select, ld_risultato, fs_errore) < 0 then
		destroy luo_formule;
		
		return -1
	end if
	dw_report.setitem(ll_new, "nc_d_mese", ld_risultato)
	
	//salvo in datastore di dettaglio ------------------------------
	ids_det_report_vendor_list.setitem(ll_riga_ds, "n_nc_d_mese",			ld_risultato )
	
	//--------------------------------------------------------------------------------------------------
	
	//#################################################
	//IQPA tipo A mese
	dw_selezione.object.t_log.text = "Elaborazione          IQPA tipo B          "+ls_mese_anno+" ..."
	if luo_formule.uof_calcola_formula("[IQPA_MESE_A]", ls_select, ld_risultato, fs_errore) < 0 then
		destroy luo_formule;
		
		return -1
	end if
	dw_report.setitem(ll_new, "iqpa_a_mese", ld_risultato)
	
	//salvo in datastore di dettaglio ------------------------------
	ids_det_report_vendor_list.setitem(ll_riga_ds, "iqpa_a_mese",			ld_risultato )
	
	//--------------------------------------------------------------------------------------------------
	
	//IQPA tipo B mese
	dw_selezione.object.t_log.text = "Elaborazione          IQPA tipo B          "+ls_mese_anno+" ..."
	if luo_formule.uof_calcola_formula("[IQPA_MESE_B]", ls_select, ld_risultato, fs_errore) < 0 then
		destroy luo_formule;
		
		return -1
	end if
	dw_report.setitem(ll_new, "iqpa_b_mese", ld_risultato)
	
	//salvo in datastore di dettaglio ------------------------------
	ids_det_report_vendor_list.setitem(ll_riga_ds, "iqpa_b_mese",			ld_risultato )
	
	//--------------------------------------------------------------------------------------------------
	
	//IQPA tipo C mese
	dw_selezione.object.t_log.text = "Elaborazione          IQPA tipo C          "+ls_mese_anno+" ..."
	if luo_formule.uof_calcola_formula("[IQPA_MESE_C]", ls_select, ld_risultato, fs_errore) < 0 then
		destroy luo_formule;
		
		return -1
	end if
	dw_report.setitem(ll_new, "iqpa_c_mese", ld_risultato)
	
	//salvo in datastore di dettaglio ------------------------------
	ids_det_report_vendor_list.setitem(ll_riga_ds, "iqpa_c_mese",			ld_risultato )
	
	//--------------------------------------------------------------------------------------------------
	
	//IQPA tipo D mese
	dw_selezione.object.t_log.text = "Elaborazione          IQPA tipo D          "+ls_mese_anno+" ..."
	if luo_formule.uof_calcola_formula("[IQPA_MESE_D]", ls_select, ld_risultato, fs_errore) < 0 then
		destroy luo_formule;
		
		return -1
	end if
	dw_report.setitem(ll_new, "iqpa_d_mese", ld_risultato)
	
	//salvo in datastore di dettaglio ------------------------------
	ids_det_report_vendor_list.setitem(ll_riga_ds, "iqpa_d_mese",			ld_risultato )
	
	//--------------------------------------------------------------------------------------------------
	
	//vai al mese successivo
	ll_mese += 1
	if ll_mese > 12 then
		ll_mese = 1
		ll_anno += 1
	end if
	
loop

destroy luo_formule;

//if not lb_almeno_uno then
//		ll_new = dw_report.insertrow(0)
//	
//		//da fare su ogni nuova riga
//		dw_report.setitem(ll_new, "data_dal", ldt_data_dal)
//		dw_report.setitem(ll_new, "data_al", ldt_data_al)
//		dw_report.setitem(ll_new, "cod_fornitore", ls_cod_fornitore)
//		//--------------------------------------------------------------------------------
//end if

dw_selezione.object.t_log.text = "Elaborazione terminata!"

return 1
end function

public function string wf_mese_anno (long fl_mese, long fl_anno);string ls_ret

choose case fl_mese
	case 1
		ls_ret = "Gennaio"
		
	case 2
		ls_ret = "Febbraio"
		
	case 3
		ls_ret = "Marzo"
		
	case 4
		ls_ret = "Aprile"
		
	case 5
		ls_ret = "Maggio"
		
	case 6
		ls_ret = "Giugno"
		
	case 7
		ls_ret = "Luglio"
		
	case 8
		ls_ret = "Agosto"
		
	case 9
		ls_ret = "Settembre"
		
	case 10
		ls_ret = "Ottobre"
		
	case 11
		ls_ret = "Novembre"
		
	case 12
		ls_ret = "Dicembre"
		
	case else
		ls_ret = "<mese errato>"
		
end choose

ls_ret += " " + string(fl_anno)

return ls_ret
end function

public subroutine wf_get_fine_mese (long fl_num_mese, long fl_anno, ref datetime fdt_data);long ll_ultimo_giorno, ll_index

fdt_data = datetime(date(fl_anno, fl_num_mese, 1), 00:00:00)

choose case fl_num_mese
	case 11,4,6,9		//30 giorni a novembre, con aprile, giugno e settembre -------------------------------------------------------------
		ll_ultimo_giorno = 30
		
	case 2		//di 28 ce n'è uno -----------------------------------------------------------------------------------------------------------------
		//se bisestile sono 29, altrimenti 28
		//sono bisestili gli anni divisibili per quattro, tranne quelli di inizio secolo (cioè divisibili per 100) non divisibili per 400
		if mod(fl_anno, 4) = 0 then
			
			if mod(fl_anno, 100) = 0 then
				
				if mod(fl_anno, 400) = 0 then
					//bisestile
					ll_ultimo_giorno = 28
				else
					//non bisestile
					ll_ultimo_giorno = 29
				end if
				
			else
				//sicuramente bisestile
				ll_ultimo_giorno = 29
			end if
			
		else
			//sicuramente non bisestile
			ll_ultimo_giorno = 28
		end if
		
	case else//tutti gli altri ne han 31 ---------------------------------------------------------------------------------------------------------------
		ll_ultimo_giorno = 31
		
end choose

fdt_data = datetime(date(fl_anno, fl_num_mese, ll_ultimo_giorno), 00:00:00)

return
end subroutine

public function long wf_salva_valutazione (ref string fs_errore);long ll_ret, ll_count, ll_riga, ll_index

select max(progressivo)
into :ll_ret
from tes_report_vendor_list
where 	cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ll_ret) then ll_ret = 0

ll_ret += 1

//conteggio righe di dettaglio
ll_count = ids_det_report_vendor_list.rowcount()

if ids_tes_report_vendor_list.rowcount() <> 1 or ll_count <=0 then
	fs_errore = "No ci sono dati di valutazione da salvare! "
	return -1
else
	ids_tes_report_vendor_list.setitem(1, "progressivo", ll_ret )
end if

//metto progressivo padre e progressivo dettaglio in datastore di dettaglio
for ll_index=1 to ll_count
	ids_det_report_vendor_list.setitem(ll_index, "progressivo",			ll_ret )
	ids_det_report_vendor_list.setitem(ll_index, "prog_dettaglio",		ll_index )
next


//update datastores
if ids_tes_report_vendor_list.update() < 0 then
	g_mb.error("OMNIA", "Errore in update dati testata valutazione fornitore!" )
	return -1
end if

if ids_det_report_vendor_list.update() < 0 then
	g_mb.error("OMNIA", "Errore in update dati dettaglio valutazione fornitore!" )
	return -1
end if


return ll_ret
end function

public function integer wf_creazione_datastore (ref string fs_errore, ref long fl_righe_testata, ref long fl_righe_dettaglio);string ls_sql_testata, ls_sql_dettaglio

ls_sql_testata = "select		cod_azienda,"+&
									"progressivo,"+&
									"cod_fornitore,"+&
									"data_val_da,"+&
									"data_val_a,"+&
									"data_creazione,"+&
									"ora_creazione,"+&
									"utente,"+&
									"pa,"+&
									"pb,"+&
									"pc,"+&
									"pd,"+&
									"lqa_a,"+&
									"lqa_b,"+&
									"lqa_c,"+&
									"lqa_d,"+&
									"iqpa_a,"+&
									"iqpa_b,"+&
									"iqpa_c,"+&
									"iqpa_d "+&
					"from tes_report_vendor_list "+&
					"where cod_azienda='CODICEINESISTENTE' "

ls_sql_dettaglio = "select 	cod_azienda,"+&
									"progressivo,"+&
									"prog_dettaglio,"+&
									"mese_calcolo,"+&
									"anno_calcolo,"+&
									"des_mese_anno,"+&
									"n_accettazioni,"+&
									"n_nc_a_mese,"+&
									"n_nc_b_mese,"+&
									"n_nc_c_mese,"+&
									"n_nc_d_mese,"+&
									"iqpa_a_mese,"+&
									"iqpa_b_mese,"+&
									"iqpa_c_mese,"+&
									"iqpa_d_mese "+&
						"from det_report_vendor_list "+&
						"where cod_azienda='CODICEINESISTENTE' "

fl_righe_testata = guo_functions.uof_crea_datastore( ids_tes_report_vendor_list, ls_sql_testata, fs_errore )
if  fl_righe_testata < 0 then
	//in fs_errore il messaggio
	
	return -1
end if

fl_righe_dettaglio = guo_functions.uof_crea_datastore( ids_det_report_vendor_list, ls_sql_dettaglio, fs_errore )
if fl_righe_dettaglio<0 then
	//in fs_errore il messaggio
	
	return -1
end if

return 1
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify, ls_prova
windowobject lw_oggetti[]
boolean lb_sintexcal
long ll_righe

dw_report.ib_dw_report = true

//------------------------------------------------------------------------------
lw_oggetti[1] = dw_report
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])
//------------------------------------------------------------------------------
lw_oggetti[1] = dw_selezione
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
//-----------------------------------------------------------------------------

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

set_w_options(c_closenosave + c_autoposition)

dw_report.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_nomodify + &
                                c_nodelete + &
                                c_newonopen + &
                                c_disableCC, &
                                c_noresizedw + &
                                c_nohighlightselected + &
                                c_nocursorrowpointer +&
                                c_nocursorrowfocusrect )
										  
dw_selezione.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_nomodify + &
                                    c_nodelete + &
                                    c_newonopen + &
                                    c_disableCC, &
                                    c_noresizedw + &
                                    c_nohighlightselected + &
                                    c_cursorrowpointer)

iuo_dw_main = dw_report

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

if sqlca.sqlcode < 0 then g_mb.error("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext)
if sqlca.sqlcode = 100 or len(ls_path_logo)<=0 then g_mb.error("OMNIA","Manca il parametro LOA oppure non è stato impostato in parametri azienda, pertanto non apparirà il logo aziendale")


select stringa
into   :ls_prova
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
			 
if sqlca.sqlcode = 0 and not isnull(ls_prova) then
	lb_sintexcal = true
else
	lb_sintexcal = false
end if

if lb_sintexcal then
	ls_modify = "intestazione.filename='" + ls_path_logo + "'"
else
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo + "'"
end if

dw_report.modify(ls_modify)

dw_report.object.datawindow.print.preview = "yes"

if wf_creazione_datastore(ls_prova, ll_righe, ll_righe) < 0 then
	g_mb.error("OMNIA", ls_prova)
	
	return
end if

end event

on w_report_vendor_list_formule.create
int iCurrent
call super::create
this.dw_folder=create dw_folder
this.dw_report=create dw_report
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_folder
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.dw_selezione
end on

on w_report_vendor_list_formule.destroy
call super::destroy
destroy(this.dw_folder)
destroy(this.dw_report)
destroy(this.dw_selezione)
end on

event pc_close;call super::pc_close;destroy ids_tes_report_vendor_list;
destroy ids_det_report_vendor_list;
end event

type dw_folder from u_folder within w_report_vendor_list_formule
integer x = 18
integer y = 16
integer width = 3849
integer height = 2148
integer taborder = 40
boolean bringtotop = true
end type

type dw_report from uo_cs_xx_dw within w_report_vendor_list_formule
integer x = 41
integer y = 136
integer width = 3799
integer height = 1996
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_rep_vend_list_formule"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;//NOTA Script ancestor disattivato!!!!!

string			ls_errore
long			ll_progressivo
integer		li_msg

li_msg = messagebox("OMNIA", "Selezionare SI per salvare la valutazione, NO per visualizzare solo l'anteprima, altrimenti CANCEL", Question!, YesNoCancel!, 1)

if li_msg=1 then
	//salva valutazione
	
	ll_progressivo = wf_salva_valutazione(ls_errore)
	if ll_progressivo<0 then
		rollback;
		g_mb.error("OMNIA" , ls_errore)
		return
	else
		commit;
		ids_tes_report_vendor_list.reset( )
		ids_det_report_vendor_list.reset( )
		g_mb.show("OMNIA" , "Salvataggio effettuato con successo. ID progressivo report=" + string(ll_progressivo) )
		return
	end if
	
elseif li_msg=2 then
	//visualizza anteprima report
	
	//chiama l'evento ancestor
	super::event doubleclicked(xpos, ypos, row, dwo)
	
else
	//fuori
	return
end if



end event

type dw_selezione from uo_cs_xx_dw within w_report_vendor_list_formule
integer x = 41
integer y = 136
integer width = 2555
integer height = 920
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_rep_vend_list_formule_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string ls_errore

choose case dwo.name
	case "b_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
		
	case "b_report"
		if wf_report(ls_errore) < 0 then
			dw_selezione.object.t_log.text = "Elaborazione terminata con Errori!"
			ids_tes_report_vendor_list.reset()
			ids_det_report_vendor_list.reset()
			g_mb.error("OMNIA", ls_errore)
			
			return
		end if
		dw_folder.fu_selecttab(2)
		
	case "b_annulla"
		wf_annulla()
		
end choose
end event

