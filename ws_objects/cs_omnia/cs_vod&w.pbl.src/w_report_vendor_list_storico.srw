﻿$PBExportHeader$w_report_vendor_list_storico.srw
$PBExportComments$Finestra Gestione Report Fornitori in o non in Vendor List
forward
global type w_report_vendor_list_storico from w_cs_xx_principale
end type
type dw_folder from u_folder within w_report_vendor_list_storico
end type
type dw_report from uo_cs_xx_dw within w_report_vendor_list_storico
end type
type dw_grafico from uo_cs_graph within w_report_vendor_list_storico
end type
type dw_selezione from uo_cs_xx_dw within w_report_vendor_list_storico
end type
type dw_lista from datawindow within w_report_vendor_list_storico
end type
end forward

global type w_report_vendor_list_storico from w_cs_xx_principale
integer width = 3927
integer height = 2280
string title = "Report ~"Vendor List~" storico"
dw_folder dw_folder
dw_report dw_report
dw_grafico dw_grafico
dw_selezione dw_selezione
dw_lista dw_lista
end type
global w_report_vendor_list_storico w_report_vendor_list_storico

type variables
string is_sql_lista_hold
end variables

forward prototypes
public subroutine wf_annulla ()
public subroutine wf_get_fine_mese (long fl_num_mese, long fl_anno, ref datetime fdt_data)
public function integer wf_ricerca (ref string fs_errore)
private function integer wf_retrieve_report (long fl_progressivo, string fs_cod_fornitore, string fs_periodo, ref string fs_errore)
end prototypes

public subroutine wf_annulla ();string ls_null
datetime ldt_null

setnull(ls_null)
setnull(ldt_null)


dw_selezione.setitem(1,"data_dal", 						ldt_null)
dw_selezione.setitem(1,"data_al", 						ldt_null)
dw_selezione.setitem(1,"cod_area_aziendale", 		ls_null)
dw_selezione.setitem(1,"cod_fornitore", 				ls_null)


end subroutine

public subroutine wf_get_fine_mese (long fl_num_mese, long fl_anno, ref datetime fdt_data);long ll_ultimo_giorno, ll_index

fdt_data = datetime(date(fl_anno, fl_num_mese, 1), 00:00:00)

choose case fl_num_mese
	case 11,4,6,9		//30 giorni a novembre, con aprile, giugno e settembre -------------------------------------------------------------
		ll_ultimo_giorno = 30
		
	case 2		//di 28 ce n'è uno -----------------------------------------------------------------------------------------------------------------
		//se bisestile sono 29, altrimenti 28
		//sono bisestili gli anni divisibili per quattro, tranne quelli di inizio secolo (cioè divisibili per 100) non divisibili per 400
		if mod(fl_anno, 4) = 0 then
			
			if mod(fl_anno, 100) = 0 then
				
				if mod(fl_anno, 400) = 0 then
					//bisestile
					ll_ultimo_giorno = 28
				else
					//non bisestile
					ll_ultimo_giorno = 29
				end if
				
			else
				//sicuramente bisestile
				ll_ultimo_giorno = 29
			end if
			
		else
			//sicuramente non bisestile
			ll_ultimo_giorno = 28
		end if
		
	case else//tutti gli altri ne han 31 ---------------------------------------------------------------------------------------------------------------
		ll_ultimo_giorno = 31
		
end choose

fdt_data = datetime(date(fl_anno, fl_num_mese, ll_ultimo_giorno), 00:00:00)

return
end subroutine

public function integer wf_ricerca (ref string fs_errore);string ls_cod_fornitore, ls_where, ls_sql
datetime ldt_data_dal, ldt_data_al
date ldt_temp
long ll_tot

dw_selezione.accepttext( )

dw_selezione.object.t_log.text = "Inizio ricerca ..."

ls_sql = is_sql_lista_hold

ls_where = " where cod_azienda='"+s_cs_xx.cod_azienda+"' "

////----------------------------
//ldt_data_dal = dw_selezione.getitemdatetime(1, "data_dal")
//
//if not isnull(ldt_data_dal ) and year(date(ldt_data_dal ))<=1950 then
//	
//	//prepara la data inizio con il primo del mese
//	ldt_temp = date( ldt_data_dal )
//	ldt_data_dal = datetime(date(year( ldt_temp ), month( ldt_temp ), 1) , 00:00:00 )
//	
//	ls_where += "and data_val_da>='"+string(ldt_data_dal, s_cs_xx.db_funzioni.formato_data)+"' "
//end if
//
////----------------------------
//ldt_data_al = dw_selezione.getitemdatetime(1, "data_al")
//
//if not isnull(ldt_data_al ) and year(date(ldt_data_al ))<=1950 then
//	
//	//restituisce la data fine con ultimo giorno del mese
//	wf_get_fine_mese(month(date(ldt_data_al )), year(date(ldt_data_al )), ldt_data_al )
//	
//	ls_where += "and data_val_a<='"+string(ldt_data_al, s_cs_xx.db_funzioni.formato_data)+"' "
//end if

//----------------------------
ls_cod_fornitore = dw_selezione.getitemstring(1, "cod_fornitore")

if isnull(ls_cod_fornitore) or len(ls_cod_fornitore) < 0 then
	fs_errore = "Indicare il fornitore!"
	
	return -1
end if

ls_where += "and cod_fornitore='"+ls_cod_fornitore+"' "


ls_sql += ls_where

dw_lista.setsqlselect(ls_sql )

ll_tot = dw_lista.retrieve( )

dw_selezione.object.t_log.text = "Sono state trovate "+string(ll_tot)+" valutazioni."

return 1
end function

private function integer wf_retrieve_report (long fl_progressivo, string fs_cod_fornitore, string fs_periodo, ref string fs_errore);long					ll_ret, ll_index, ll_tot, ll_new
string					ls_sql, ls_des_mese_anno
decimal				ld_iqpa, ld_iqpa_tot_a, ld_iqpa_tot_b, ld_iqpa_tot_c, ld_iqpa_tot_d
datastore			lds_data

ll_tot = dw_report.retrieve( s_cs_xx.cod_azienda, fl_progressivo )

if ll_tot < 0  then
	fs_errore = "Errore in retrieve dati report"
	return -1
end if

//preparazione datastoregrafico
ls_sql = "select '                              ' as anno_mese,"+&
					"0.00 as iqpa,"+&
					"' ' as tipo "+&
			"from aziende "+&
			"where cod_azienda='CODICEINESISTENTE' "

ll_ret = guo_functions.uof_crea_datastore( lds_data,ls_sql, fs_errore )
if  ll_ret < 0 then
	//in fs_errore il messaggio
	
	return -1
end if


ld_iqpa_tot_a = 0
ld_iqpa_tot_b = 0
ld_iqpa_tot_c = 0
ld_iqpa_tot_d = 0

for ll_index=1 to ll_tot
	ls_des_mese_anno = left(dw_report.getitemstring(ll_index, "des_mese_anno" ) , 3) +  right(dw_report.getitemstring(ll_index, "des_mese_anno" ) , 5)
	ls_des_mese_anno = right("00" + string( ll_index ), 2 ) + "."  + ls_des_mese_anno
	
	//---------------------------------------------- 
	ld_iqpa = dw_report.getitemdecimal( ll_index, "iqpa_a_mese")
	ll_new = lds_data.insertrow( 0 )
	lds_data.setitem(ll_new, 1, ls_des_mese_anno )	//anno_mese
	lds_data.setitem(ll_new, 2, ld_iqpa )					//iqpa
	lds_data.setitem(ll_new, 3, "A" )						//tipo
	
	//----------------------------------------------
	ld_iqpa = dw_report.getitemdecimal( ll_index, "iqpa_b_mese" )
	ll_new = lds_data.insertrow( 0 )
	lds_data.setitem(ll_new, 1, ls_des_mese_anno )
	lds_data.setitem(ll_new, 2, ld_iqpa )
	lds_data.setitem(ll_new, 3, "B" )
	
	//----------------------------------------------
	ld_iqpa = dw_report.getitemdecimal( ll_index, "iqpa_c_mese" )
	ll_new = lds_data.insertrow( 0 )
	lds_data.setitem(ll_new, 1, ls_des_mese_anno )
	lds_data.setitem(ll_new, 2, ld_iqpa )
	lds_data.setitem(ll_new, 3, "C" )
	
	//----------------------------------------------
	ld_iqpa = dw_report.getitemdecimal( ll_index, "iqpa_d_mese" )
	ll_new = lds_data.insertrow( 0 )
	lds_data.setitem(ll_new, 1, ls_des_mese_anno )
	lds_data.setitem(ll_new, 2, ld_iqpa )
	lds_data.setitem(ll_new, 3, "D" )
	
next

//alla fine inserisci le righe dei valor medi
if ll_tot>0 then
	ls_des_mese_anno ="Valore medio"
	ls_des_mese_anno = right("00" + string( ll_tot + 1 ), 2 ) + "." +  ls_des_mese_anno
	ld_iqpa_tot_a = dw_report.getitemdecimal( 1, "iqpa_a" )
	ld_iqpa_tot_b = dw_report.getitemdecimal( 1, "iqpa_b" )
	ld_iqpa_tot_c = dw_report.getitemdecimal( 1, "iqpa_c" )
	ld_iqpa_tot_d = dw_report.getitemdecimal( 1, "iqpa_d" )
	
	ll_new = lds_data.insertrow( 0 )
	lds_data.setitem(ll_new, 1, ls_des_mese_anno )
	lds_data.setitem(ll_new, 2, ld_iqpa_tot_a )
	lds_data.setitem(ll_new, 3, "A" )
	//----------------------------------------------
	
	ll_new = lds_data.insertrow( 0 )
	lds_data.setitem(ll_new, 1, ls_des_mese_anno )
	lds_data.setitem(ll_new, 2, ld_iqpa_tot_b )
	lds_data.setitem(ll_new, 3, "B" )
	//----------------------------------------------
	
	ll_new = lds_data.insertrow( 0 )
	lds_data.setitem(ll_new, 1, ls_des_mese_anno )
	lds_data.setitem(ll_new, 2, ld_iqpa_tot_c )
	lds_data.setitem(ll_new, 3, "C" )
	//----------------------------------------------
	
	ll_new = lds_data.insertrow( 0 )
	lds_data.setitem(ll_new, 1, ls_des_mese_anno )
	lds_data.setitem(ll_new, 2, ld_iqpa_tot_d )
	lds_data.setitem(ll_new, 3, "D" )
	//----------------------------------------------
	
end if

dw_grafico.ib_datastore = true
dw_grafico.ids_data = lds_data

dw_grafico.is_str[1] = "anno_mese"
dw_grafico.is_str[2] = "tipo"
dw_grafico.is_num[1] = "iqpa"

dw_grafico.is_source_categoria_col = "anno_mese"

dw_grafico.is_categoria_col = "str_1"
dw_grafico.is_exp_valore = "num_1"
dw_grafico.is_serie_col = "str_2"

//altre impostazioni del grafico
dw_grafico.is_titolo = "Valutazione Fornitore "+fs_cod_fornitore+ " - " + f_des_tabella("anag_fornitori","cod_fornitore = '" + fs_cod_fornitore +"'", "rag_soc_1") +&
							" " + fs_periodo
dw_grafico.is_label_categoria= "Periodo"
dw_grafico.is_label_valori = "Valori"

//tipo scala: 1 se lineare, 2 se logaritmica in base 10
dw_grafico.ii_scala = 1

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_grafico.ii_tipo_grafico = 9  //Colonne

//colore di sfondo del grafico
//per il bianco (DEFAULT)	: 16777215
//per il silver			: 12632256
//dw_grafico.il_backcolor = 16777215 //bianco

//metodo che visualizza i dati nel grafico
dw_grafico.uof_retrieve( )

destroy lds_data



dw_folder.fu_selecttab( 2 )

return 1
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify, ls_prova
windowobject lw_oggetti[], lw_vuoto[]
boolean lb_sintexcal

dw_report.ib_dw_report = true

//------------------------------------------------------------------------------
lw_oggetti[1] = dw_grafico
dw_folder.fu_assigntab(3, "Grafico Valutazione", lw_oggetti[])
//------------------------------------------------------------------------------
lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_report
dw_folder.fu_assigntab(2, "Rep. Valutazione", lw_oggetti[])
//------------------------------------------------------------------------------
lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_selezione
lw_oggetti[1] = dw_lista
dw_folder.fu_assigntab(1, "Ricerca/Lista", lw_oggetti[])
//-----------------------------------------------------------------------------


dw_folder.fu_foldercreate(3, 3 )
dw_folder.fu_selecttab(1)


dw_lista.settransobject( sqlca )
is_sql_lista_hold = dw_lista.getsqlselect()

set_w_options(c_closenosave + c_autoposition )

dw_report.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_nomodify + &
                                c_nodelete + &
                                c_newonopen + &
                                c_disableCC, &
                                c_noresizedw + &
                                c_nohighlightselected + &
                                c_nocursorrowpointer +&
                                c_nocursorrowfocusrect )
										  
dw_selezione.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_nomodify + &
                                    c_nodelete + &
                                    c_newonopen + &
                                    c_disableCC, &
                                    c_noresizedw + &
                                    c_nohighlightselected + &
                                    c_cursorrowpointer)

iuo_dw_main = dw_report

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

if sqlca.sqlcode < 0 then g_mb.error("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext)
if sqlca.sqlcode = 100 or len(ls_path_logo)<=0 then g_mb.error("OMNIA","Manca il parametro LOA oppure non è stato impostato in parametri azienda, pertanto non apparirà il logo aziendale")


select stringa
into   :ls_prova
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
			 
if sqlca.sqlcode = 0 and not isnull(ls_prova) then
	lb_sintexcal = true
else
	lb_sintexcal = false
end if

if lb_sintexcal then
	ls_modify = "intestazione.filename='" + ls_path_logo + "'"
else
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo + "'"
end if

dw_report.modify(ls_modify)

dw_report.object.datawindow.print.preview = "yes"

dw_selezione.object.b_report.text = "Cerca"
dw_selezione.object.data_dal_t.visible = false
dw_selezione.object.data_dal.visible = false
dw_selezione.object.data_al_t.visible = false
dw_selezione.object.data_al.visible = false


end event

on w_report_vendor_list_storico.create
int iCurrent
call super::create
this.dw_folder=create dw_folder
this.dw_report=create dw_report
this.dw_grafico=create dw_grafico
this.dw_selezione=create dw_selezione
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_folder
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.dw_grafico
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_lista
end on

on w_report_vendor_list_storico.destroy
call super::destroy
destroy(this.dw_folder)
destroy(this.dw_report)
destroy(this.dw_grafico)
destroy(this.dw_selezione)
destroy(this.dw_lista)
end on

type dw_folder from u_folder within w_report_vendor_list_storico
integer x = 18
integer y = 16
integer width = 3849
integer height = 2148
integer taborder = 40
boolean bringtotop = true
end type

type dw_report from uo_cs_xx_dw within w_report_vendor_list_storico
integer x = 41
integer y = 136
integer width = 3799
integer height = 1996
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_report_vendor_list_storico_1"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_grafico from uo_cs_graph within w_report_vendor_list_storico
integer x = 41
integer y = 132
integer width = 3799
integer height = 1996
integer taborder = 40
boolean bringtotop = true
end type

type dw_selezione from uo_cs_xx_dw within w_report_vendor_list_storico
integer x = 41
integer y = 136
integer width = 2528
integer height = 652
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_rep_vend_list_formule_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string ls_errore

choose case dwo.name
	case "b_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
		
	case "b_report"		//si tratta della ricerca
		
		if wf_ricerca( ls_errore ) < 0 then
			dw_selezione.object.t_log.text = "Ricerca terminata con Errori!"
			
			g_mb.error("OMNIA", ls_errore)
			
			return
		end if
		
//		if wf_report(ls_errore) < 0 then
//			dw_selezione.object.t_log.text = "Elaborazione terminata con Errori!"
//			
//			g_mb.error("OMNIA", ls_errore)
//			
//			return
//		end if
//		dw_folder.fu_selecttab(2)
		
	case "b_annulla"
		wf_annulla()
		
end choose
end event

type dw_lista from datawindow within w_report_vendor_list_storico
integer x = 41
integer y = 832
integer width = 3803
integer height = 1308
integer taborder = 30
boolean bringtotop = true
string title = "none"
string dataobject = "d_report_vendor_list_storico"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;
if currentrow > 0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

event doubleclicked;long ll_progressivo
string ls_errore, ls_cod_fornitore, ls_periodo
datetime ldt_data_da, ldt_data_a

if row>0 then
else
	return
end if

ll_progressivo = getitemnumber( row, "progressivo")

if ll_progressivo>0 then
	
	ls_cod_fornitore =  getitemstring( row, "cod_fornitore")
	ldt_data_da = getitemdatetime(row, "data_val_da")
	ldt_data_a =  getitemdatetime(row, "data_val_a")
	ls_periodo = "da "+string(ldt_data_da,"dd/mm/yy")+" a "+string(ldt_data_a,"dd/mm/yy")
	
	if wf_retrieve_report( ll_progressivo, ls_cod_fornitore, ls_periodo, ls_errore ) < 0 then
		g_mb.error("OMNIA", ls_errore)
		
		return
	end if
else
	dw_report.reset( )
	dw_grafico.reset( )
	
end if
end event

