﻿$PBExportHeader$w_richiesta_fornitore.srw
$PBExportComments$Finestra Richiesta Fornitore
forward
global type w_richiesta_fornitore from w_cs_xx_risposta
end type
type cb_1 from commandbutton within w_richiesta_fornitore
end type
type cb_2 from commandbutton within w_richiesta_fornitore
end type
type dw_ext_selezione_for from uo_cs_xx_dw within w_richiesta_fornitore
end type
end forward

global type w_richiesta_fornitore from w_cs_xx_risposta
integer width = 2034
integer height = 504
string title = "Richiesta Fornitore da Valutare"
cb_1 cb_1
cb_2 cb_2
dw_ext_selezione_for dw_ext_selezione_for
end type
global w_richiesta_fornitore w_richiesta_fornitore

on w_richiesta_fornitore.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_2=create cb_2
this.dw_ext_selezione_for=create dw_ext_selezione_for
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.dw_ext_selezione_for
end on

on w_richiesta_fornitore.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.dw_ext_selezione_for)
end on

event pc_setwindow;call super::pc_setwindow;dw_ext_selezione_for.set_dw_options(sqlca, &
                                    pcca.null_object, &
												c_newonopen + &
												c_nomodify + &
												c_nodelete + &
												c_noretrieveonopen + &
												c_disableCC + &
												c_disableCCInsert, &
												c_default)

save_on_close(c_socnosave)
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ext_selezione_for, &
                 "cod_fornitore", &
					  sqlca, &
					  "anag_fornitori", &
					  "cod_fornitore", &
					  "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type cb_1 from commandbutton within w_richiesta_fornitore
integer x = 1531
integer y = 300
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;dw_ext_selezione_for.setcolumn(1)
dw_ext_selezione_for.setcolumn(2)

s_cs_xx.parametri.parametro_s_1 = dw_ext_selezione_for.getitemstring(dw_ext_selezione_for.getrow(),"cod_fornitore")
s_cs_xx.parametri.parametro_data_1 = dw_ext_selezione_for.getitemdatetime(dw_ext_selezione_for.getrow(),"data_inizio")
s_cs_xx.parametri.parametro_data_2 = dw_ext_selezione_for.getitemdatetime(dw_ext_selezione_for.getrow(),"data_fine")
if isnull(s_cs_xx.parametri.parametro_s_1) then
	g_mb.messagebox("OMNIA","Selezionare un fornitore dalla lista")
	return
end if

if isnull(s_cs_xx.parametri.parametro_data_1) then
	g_mb.messagebox("OMNIA","Selezionare una data di inizio valida")
	return
end if
	
if isnull(s_cs_xx.parametri.parametro_data_2) then
	g_mb.messagebox("OMNIA","Selezionare una data di fine valida")
	return
end if
	
close(parent)
end event

type cb_2 from commandbutton within w_richiesta_fornitore
integer x = 1143
integer y = 300
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_data_1)
setnull(s_cs_xx.parametri.parametro_data_2)

save_on_close(c_socnosave)
close(parent)
end event

type dw_ext_selezione_for from uo_cs_xx_dw within w_richiesta_fornitore
integer x = 23
integer y = 20
integer width = 1943
integer height = 280
integer taborder = 10
string dataobject = "d_ext_selezione_for"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ext_selezione_for,"cod_fornitore")
end choose
end event

