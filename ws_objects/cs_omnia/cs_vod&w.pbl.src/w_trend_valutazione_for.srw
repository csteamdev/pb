﻿$PBExportHeader$w_trend_valutazione_for.srw
$PBExportComments$Finestra Gestione e Calcolo Vendor Rating Fornitore in 12 Periodi
forward
global type w_trend_valutazione_for from w_cs_xx_principale
end type
type st_1 from statictext within w_trend_valutazione_for
end type
type cb_valutazione from commandbutton within w_trend_valutazione_for
end type
type st_8 from statictext within w_trend_valutazione_for
end type
type sle_vt_1 from singlelineedit within w_trend_valutazione_for
end type
type em_1 from editmask within w_trend_valutazione_for
end type
type em_2 from editmask within w_trend_valutazione_for
end type
type st_10 from statictext within w_trend_valutazione_for
end type
type st_11 from statictext within w_trend_valutazione_for
end type
type em_3 from editmask within w_trend_valutazione_for
end type
type em_4 from editmask within w_trend_valutazione_for
end type
type st_12 from statictext within w_trend_valutazione_for
end type
type st_13 from statictext within w_trend_valutazione_for
end type
type st_14 from statictext within w_trend_valutazione_for
end type
type st_15 from statictext within w_trend_valutazione_for
end type
type st_16 from statictext within w_trend_valutazione_for
end type
type st_17 from statictext within w_trend_valutazione_for
end type
type st_18 from statictext within w_trend_valutazione_for
end type
type st_19 from statictext within w_trend_valutazione_for
end type
type st_20 from statictext within w_trend_valutazione_for
end type
type st_21 from statictext within w_trend_valutazione_for
end type
type st_22 from statictext within w_trend_valutazione_for
end type
type st_23 from statictext within w_trend_valutazione_for
end type
type em_7 from editmask within w_trend_valutazione_for
end type
type em_8 from editmask within w_trend_valutazione_for
end type
type em_9 from editmask within w_trend_valutazione_for
end type
type em_10 from editmask within w_trend_valutazione_for
end type
type em_11 from editmask within w_trend_valutazione_for
end type
type em_12 from editmask within w_trend_valutazione_for
end type
type em_13 from editmask within w_trend_valutazione_for
end type
type em_14 from editmask within w_trend_valutazione_for
end type
type em_15 from editmask within w_trend_valutazione_for
end type
type em_16 from editmask within w_trend_valutazione_for
end type
type em_30 from editmask within w_trend_valutazione_for
end type
type em_31 from editmask within w_trend_valutazione_for
end type
type em_32 from editmask within w_trend_valutazione_for
end type
type em_33 from editmask within w_trend_valutazione_for
end type
type em_34 from editmask within w_trend_valutazione_for
end type
type em_35 from editmask within w_trend_valutazione_for
end type
type em_36 from editmask within w_trend_valutazione_for
end type
type em_37 from editmask within w_trend_valutazione_for
end type
type em_38 from editmask within w_trend_valutazione_for
end type
type em_39 from editmask within w_trend_valutazione_for
end type
type st_30 from statictext within w_trend_valutazione_for
end type
type st_31 from statictext within w_trend_valutazione_for
end type
type st_32 from statictext within w_trend_valutazione_for
end type
type st_33 from statictext within w_trend_valutazione_for
end type
type st_34 from statictext within w_trend_valutazione_for
end type
type st_35 from statictext within w_trend_valutazione_for
end type
type st_36 from statictext within w_trend_valutazione_for
end type
type st_37 from statictext within w_trend_valutazione_for
end type
type st_38 from statictext within w_trend_valutazione_for
end type
type st_39 from statictext within w_trend_valutazione_for
end type
type em_anno_inizio from editmask within w_trend_valutazione_for
end type
type ddlb_mesi from dropdownlistbox within w_trend_valutazione_for
end type
type cb_grafico from commandbutton within w_trend_valutazione_for
end type
type rb_1 from radiobutton within w_trend_valutazione_for
end type
type rb_2 from radiobutton within w_trend_valutazione_for
end type
type rb_3 from radiobutton within w_trend_valutazione_for
end type
type rb_4 from radiobutton within w_trend_valutazione_for
end type
type rb_5 from radiobutton within w_trend_valutazione_for
end type
type gb_1 from groupbox within w_trend_valutazione_for
end type
type sle_vt_2 from singlelineedit within w_trend_valutazione_for
end type
type sle_vt_3 from singlelineedit within w_trend_valutazione_for
end type
type sle_vt_4 from singlelineedit within w_trend_valutazione_for
end type
type sle_vt_5 from singlelineedit within w_trend_valutazione_for
end type
type sle_vt_6 from singlelineedit within w_trend_valutazione_for
end type
type sle_vt_7 from singlelineedit within w_trend_valutazione_for
end type
type sle_vt_8 from singlelineedit within w_trend_valutazione_for
end type
type sle_vt_9 from singlelineedit within w_trend_valutazione_for
end type
type sle_vt_10 from singlelineedit within w_trend_valutazione_for
end type
type sle_vt_12 from singlelineedit within w_trend_valutazione_for
end type
type sle_vt_11 from singlelineedit within w_trend_valutazione_for
end type
type st_50 from statictext within w_trend_valutazione_for
end type
type st_51 from statictext within w_trend_valutazione_for
end type
type st_52 from statictext within w_trend_valutazione_for
end type
type st_53 from statictext within w_trend_valutazione_for
end type
type st_54 from statictext within w_trend_valutazione_for
end type
type st_55 from statictext within w_trend_valutazione_for
end type
type st_56 from statictext within w_trend_valutazione_for
end type
type st_57 from statictext within w_trend_valutazione_for
end type
type st_58 from statictext within w_trend_valutazione_for
end type
type st_9 from statictext within w_trend_valutazione_for
end type
type st_60 from statictext within w_trend_valutazione_for
end type
type dw_trend_valutazione_for_ext from uo_cs_xx_dw within w_trend_valutazione_for
end type
end forward

global type w_trend_valutazione_for from w_cs_xx_principale
integer width = 2363
integer height = 1740
string title = "Carta di Tendenza ~"Vendor Rating~""
st_1 st_1
cb_valutazione cb_valutazione
st_8 st_8
sle_vt_1 sle_vt_1
em_1 em_1
em_2 em_2
st_10 st_10
st_11 st_11
em_3 em_3
em_4 em_4
st_12 st_12
st_13 st_13
st_14 st_14
st_15 st_15
st_16 st_16
st_17 st_17
st_18 st_18
st_19 st_19
st_20 st_20
st_21 st_21
st_22 st_22
st_23 st_23
em_7 em_7
em_8 em_8
em_9 em_9
em_10 em_10
em_11 em_11
em_12 em_12
em_13 em_13
em_14 em_14
em_15 em_15
em_16 em_16
em_30 em_30
em_31 em_31
em_32 em_32
em_33 em_33
em_34 em_34
em_35 em_35
em_36 em_36
em_37 em_37
em_38 em_38
em_39 em_39
st_30 st_30
st_31 st_31
st_32 st_32
st_33 st_33
st_34 st_34
st_35 st_35
st_36 st_36
st_37 st_37
st_38 st_38
st_39 st_39
em_anno_inizio em_anno_inizio
ddlb_mesi ddlb_mesi
cb_grafico cb_grafico
rb_1 rb_1
rb_2 rb_2
rb_3 rb_3
rb_4 rb_4
rb_5 rb_5
gb_1 gb_1
sle_vt_2 sle_vt_2
sle_vt_3 sle_vt_3
sle_vt_4 sle_vt_4
sle_vt_5 sle_vt_5
sle_vt_6 sle_vt_6
sle_vt_7 sle_vt_7
sle_vt_8 sle_vt_8
sle_vt_9 sle_vt_9
sle_vt_10 sle_vt_10
sle_vt_12 sle_vt_12
sle_vt_11 sle_vt_11
st_50 st_50
st_51 st_51
st_52 st_52
st_53 st_53
st_54 st_54
st_55 st_55
st_56 st_56
st_57 st_57
st_58 st_58
st_9 st_9
st_60 st_60
dw_trend_valutazione_for_ext dw_trend_valutazione_for_ext
end type
global w_trend_valutazione_for w_trend_valutazione_for

type variables
string is_Tipo_Trend = "M", is_cod_fornitore
datetime id_date_inizio[12], id_date_fine[12]
end variables

forward prototypes
public function integer f_ultimo_giorno_mese (integer fi_num_mese, long fl_anno)
public function integer wf_calcola_periodi ()
public function datetime calcola_fine_periodo (integer fi_mese_iniziale, string fs_periodo, long fl_anno_iniziale)
end prototypes

public function integer f_ultimo_giorno_mese (integer fi_num_mese, long fl_anno);choose case fi_num_mese
case 11,4,6,9
   return 30
case 2
   if mod(fl_anno,4)=0 then
      return 29
   else
      return 28
   end if
case 1,3,5,7,8,10,12
   return 31
end choose


end function

public function integer wf_calcola_periodi ();long ll_i, ll_mese_inizio, ll_anno_inizio
string ls_data

ll_anno_inizio = long(em_anno_inizio.text)
ll_mese_inizio = long(left(ddlb_mesi.text,2))
ll_i = 1

if ll_anno_inizio < 1 then
   g_mb.messagebox("Calcolo Periodi","Impostare anno iniziale")
   return -1
end if
if ll_mese_inizio < 1 then
   g_mb.messagebox("Calcolo Periodi","Impostare mese iniziale")
   return -1
end if


do While ll_i < 13
   ls_data = "01/"+string(ll_mese_inizio)+"/"+string(ll_anno_inizio)
   id_date_inizio[ll_i] = datetime(date(ls_data))
   id_date_fine[ll_i]   = calcola_fine_periodo(ll_mese_inizio,is_Tipo_Trend,ll_anno_inizio)   
   ll_anno_inizio = year(date(id_date_fine[ll_i]))
   ll_mese_inizio = month(date(id_date_fine[ll_i])) + 1
   if ll_mese_inizio = 13 then
      ll_mese_inizio = 1
      ll_anno_inizio = ll_anno_inizio + 1
   end if
   ll_i++
loop

em_1.text = string(date(id_date_inizio[1]))
em_3.text = string(date(id_date_inizio[2]))
em_16.text = string(date(id_date_inizio[3]))
em_15.text = string(date(id_date_inizio[4]))
em_14.text = string(date(id_date_inizio[5]))
em_13.text = string(date(id_date_inizio[6]))
em_12.text = string(date(id_date_inizio[7]))
em_11.text = string(date(id_date_inizio[8]))
em_10.text = string(date(id_date_inizio[9]))
em_9.text = string(date(id_date_inizio[10]))
em_8.text = string(date(id_date_inizio[11]))
em_7.text = string(date(id_date_inizio[12]))

em_2.text = string(date(id_date_fine[1]))
em_4.text = string(date(id_date_fine[2]))
em_39.text = string(date(id_date_fine[3]))
em_38.text = string(date(id_date_fine[4]))
em_37.text = string(date(id_date_fine[5]))
em_36.text = string(date(id_date_fine[6]))
em_35.text = string(date(id_date_fine[7]))
em_34.text = string(date(id_date_fine[8]))
em_33.text = string(date(id_date_fine[9]))
em_32.text = string(date(id_date_fine[10]))
em_31.text = string(date(id_date_fine[11]))
em_30.text = string(date(id_date_fine[12]))
return 0
end function

public function datetime calcola_fine_periodo (integer fi_mese_iniziale, string fs_periodo, long fl_anno_iniziale);integer li_mese_finale, li_giorno_finale
long    ll_anno_finale
string  ls_data

choose case fs_periodo

case "M"
   li_mese_finale = fi_mese_iniziale

case "B"
   li_mese_finale = fi_mese_iniziale + 1

case "T"
   li_mese_finale = fi_mese_iniziale + 2

case "S"
   li_mese_finale = fi_mese_iniziale + 5

case "A"
   li_mese_finale = fi_mese_iniziale + 11

end choose
ll_anno_finale = fl_anno_iniziale
if li_mese_finale > 12 then
  li_mese_finale = li_mese_finale - 12
  ll_anno_finale = ll_anno_finale + 1
end if
li_giorno_finale = f_ultimo_giorno_mese(li_mese_finale, ll_anno_finale)   
ls_data = string(li_giorno_finale) + "/" + string(li_mese_finale) + "/" + string(ll_anno_finale)
return datetime(date(ls_data))
end function

event pc_setwindow;call super::pc_setwindow;rb_1.checked = true

set_w_options(c_closenosave + c_autoposition + c_noenablepopup)

dw_trend_valutazione_for_ext.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )

end event

on w_trend_valutazione_for.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_valutazione=create cb_valutazione
this.st_8=create st_8
this.sle_vt_1=create sle_vt_1
this.em_1=create em_1
this.em_2=create em_2
this.st_10=create st_10
this.st_11=create st_11
this.em_3=create em_3
this.em_4=create em_4
this.st_12=create st_12
this.st_13=create st_13
this.st_14=create st_14
this.st_15=create st_15
this.st_16=create st_16
this.st_17=create st_17
this.st_18=create st_18
this.st_19=create st_19
this.st_20=create st_20
this.st_21=create st_21
this.st_22=create st_22
this.st_23=create st_23
this.em_7=create em_7
this.em_8=create em_8
this.em_9=create em_9
this.em_10=create em_10
this.em_11=create em_11
this.em_12=create em_12
this.em_13=create em_13
this.em_14=create em_14
this.em_15=create em_15
this.em_16=create em_16
this.em_30=create em_30
this.em_31=create em_31
this.em_32=create em_32
this.em_33=create em_33
this.em_34=create em_34
this.em_35=create em_35
this.em_36=create em_36
this.em_37=create em_37
this.em_38=create em_38
this.em_39=create em_39
this.st_30=create st_30
this.st_31=create st_31
this.st_32=create st_32
this.st_33=create st_33
this.st_34=create st_34
this.st_35=create st_35
this.st_36=create st_36
this.st_37=create st_37
this.st_38=create st_38
this.st_39=create st_39
this.em_anno_inizio=create em_anno_inizio
this.ddlb_mesi=create ddlb_mesi
this.cb_grafico=create cb_grafico
this.rb_1=create rb_1
this.rb_2=create rb_2
this.rb_3=create rb_3
this.rb_4=create rb_4
this.rb_5=create rb_5
this.gb_1=create gb_1
this.sle_vt_2=create sle_vt_2
this.sle_vt_3=create sle_vt_3
this.sle_vt_4=create sle_vt_4
this.sle_vt_5=create sle_vt_5
this.sle_vt_6=create sle_vt_6
this.sle_vt_7=create sle_vt_7
this.sle_vt_8=create sle_vt_8
this.sle_vt_9=create sle_vt_9
this.sle_vt_10=create sle_vt_10
this.sle_vt_12=create sle_vt_12
this.sle_vt_11=create sle_vt_11
this.st_50=create st_50
this.st_51=create st_51
this.st_52=create st_52
this.st_53=create st_53
this.st_54=create st_54
this.st_55=create st_55
this.st_56=create st_56
this.st_57=create st_57
this.st_58=create st_58
this.st_9=create st_9
this.st_60=create st_60
this.dw_trend_valutazione_for_ext=create dw_trend_valutazione_for_ext
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_valutazione
this.Control[iCurrent+3]=this.st_8
this.Control[iCurrent+4]=this.sle_vt_1
this.Control[iCurrent+5]=this.em_1
this.Control[iCurrent+6]=this.em_2
this.Control[iCurrent+7]=this.st_10
this.Control[iCurrent+8]=this.st_11
this.Control[iCurrent+9]=this.em_3
this.Control[iCurrent+10]=this.em_4
this.Control[iCurrent+11]=this.st_12
this.Control[iCurrent+12]=this.st_13
this.Control[iCurrent+13]=this.st_14
this.Control[iCurrent+14]=this.st_15
this.Control[iCurrent+15]=this.st_16
this.Control[iCurrent+16]=this.st_17
this.Control[iCurrent+17]=this.st_18
this.Control[iCurrent+18]=this.st_19
this.Control[iCurrent+19]=this.st_20
this.Control[iCurrent+20]=this.st_21
this.Control[iCurrent+21]=this.st_22
this.Control[iCurrent+22]=this.st_23
this.Control[iCurrent+23]=this.em_7
this.Control[iCurrent+24]=this.em_8
this.Control[iCurrent+25]=this.em_9
this.Control[iCurrent+26]=this.em_10
this.Control[iCurrent+27]=this.em_11
this.Control[iCurrent+28]=this.em_12
this.Control[iCurrent+29]=this.em_13
this.Control[iCurrent+30]=this.em_14
this.Control[iCurrent+31]=this.em_15
this.Control[iCurrent+32]=this.em_16
this.Control[iCurrent+33]=this.em_30
this.Control[iCurrent+34]=this.em_31
this.Control[iCurrent+35]=this.em_32
this.Control[iCurrent+36]=this.em_33
this.Control[iCurrent+37]=this.em_34
this.Control[iCurrent+38]=this.em_35
this.Control[iCurrent+39]=this.em_36
this.Control[iCurrent+40]=this.em_37
this.Control[iCurrent+41]=this.em_38
this.Control[iCurrent+42]=this.em_39
this.Control[iCurrent+43]=this.st_30
this.Control[iCurrent+44]=this.st_31
this.Control[iCurrent+45]=this.st_32
this.Control[iCurrent+46]=this.st_33
this.Control[iCurrent+47]=this.st_34
this.Control[iCurrent+48]=this.st_35
this.Control[iCurrent+49]=this.st_36
this.Control[iCurrent+50]=this.st_37
this.Control[iCurrent+51]=this.st_38
this.Control[iCurrent+52]=this.st_39
this.Control[iCurrent+53]=this.em_anno_inizio
this.Control[iCurrent+54]=this.ddlb_mesi
this.Control[iCurrent+55]=this.cb_grafico
this.Control[iCurrent+56]=this.rb_1
this.Control[iCurrent+57]=this.rb_2
this.Control[iCurrent+58]=this.rb_3
this.Control[iCurrent+59]=this.rb_4
this.Control[iCurrent+60]=this.rb_5
this.Control[iCurrent+61]=this.gb_1
this.Control[iCurrent+62]=this.sle_vt_2
this.Control[iCurrent+63]=this.sle_vt_3
this.Control[iCurrent+64]=this.sle_vt_4
this.Control[iCurrent+65]=this.sle_vt_5
this.Control[iCurrent+66]=this.sle_vt_6
this.Control[iCurrent+67]=this.sle_vt_7
this.Control[iCurrent+68]=this.sle_vt_8
this.Control[iCurrent+69]=this.sle_vt_9
this.Control[iCurrent+70]=this.sle_vt_10
this.Control[iCurrent+71]=this.sle_vt_12
this.Control[iCurrent+72]=this.sle_vt_11
this.Control[iCurrent+73]=this.st_50
this.Control[iCurrent+74]=this.st_51
this.Control[iCurrent+75]=this.st_52
this.Control[iCurrent+76]=this.st_53
this.Control[iCurrent+77]=this.st_54
this.Control[iCurrent+78]=this.st_55
this.Control[iCurrent+79]=this.st_56
this.Control[iCurrent+80]=this.st_57
this.Control[iCurrent+81]=this.st_58
this.Control[iCurrent+82]=this.st_9
this.Control[iCurrent+83]=this.st_60
this.Control[iCurrent+84]=this.dw_trend_valutazione_for_ext
end on

on w_trend_valutazione_for.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_valutazione)
destroy(this.st_8)
destroy(this.sle_vt_1)
destroy(this.em_1)
destroy(this.em_2)
destroy(this.st_10)
destroy(this.st_11)
destroy(this.em_3)
destroy(this.em_4)
destroy(this.st_12)
destroy(this.st_13)
destroy(this.st_14)
destroy(this.st_15)
destroy(this.st_16)
destroy(this.st_17)
destroy(this.st_18)
destroy(this.st_19)
destroy(this.st_20)
destroy(this.st_21)
destroy(this.st_22)
destroy(this.st_23)
destroy(this.em_7)
destroy(this.em_8)
destroy(this.em_9)
destroy(this.em_10)
destroy(this.em_11)
destroy(this.em_12)
destroy(this.em_13)
destroy(this.em_14)
destroy(this.em_15)
destroy(this.em_16)
destroy(this.em_30)
destroy(this.em_31)
destroy(this.em_32)
destroy(this.em_33)
destroy(this.em_34)
destroy(this.em_35)
destroy(this.em_36)
destroy(this.em_37)
destroy(this.em_38)
destroy(this.em_39)
destroy(this.st_30)
destroy(this.st_31)
destroy(this.st_32)
destroy(this.st_33)
destroy(this.st_34)
destroy(this.st_35)
destroy(this.st_36)
destroy(this.st_37)
destroy(this.st_38)
destroy(this.st_39)
destroy(this.em_anno_inizio)
destroy(this.ddlb_mesi)
destroy(this.cb_grafico)
destroy(this.rb_1)
destroy(this.rb_2)
destroy(this.rb_3)
destroy(this.rb_4)
destroy(this.rb_5)
destroy(this.gb_1)
destroy(this.sle_vt_2)
destroy(this.sle_vt_3)
destroy(this.sle_vt_4)
destroy(this.sle_vt_5)
destroy(this.sle_vt_6)
destroy(this.sle_vt_7)
destroy(this.sle_vt_8)
destroy(this.sle_vt_9)
destroy(this.sle_vt_10)
destroy(this.sle_vt_12)
destroy(this.sle_vt_11)
destroy(this.st_50)
destroy(this.st_51)
destroy(this.st_52)
destroy(this.st_53)
destroy(this.st_54)
destroy(this.st_55)
destroy(this.st_56)
destroy(this.st_57)
destroy(this.st_58)
destroy(this.st_9)
destroy(this.st_60)
destroy(this.dw_trend_valutazione_for_ext)
end on

event pc_close;call super::pc_close;dw_trend_valutazione_for_ext.resetupdate()

end event

type st_1 from statictext within w_trend_valutazione_for
integer y = 1320
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "A partire da:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_valutazione from commandbutton within w_trend_valutazione_for
integer x = 1920
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 410
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Valutazione"
end type

event clicked;long ll_i, ll_anno_inizio, ll_mese_inizio
double VCD1,VCD2,VCD,VCE,VS1,VS2,VS3,VS,VQ,VT 
string ls_des_periodo

dw_trend_valutazione_for_ext.accepttext()
is_cod_fornitore = dw_trend_valutazione_for_ext.getitemstring( 1, "cod_fornitore")
if is_cod_fornitore = "" then setnull(is_cod_fornitore)

if wf_calcola_periodi() = -1 then
   return
end if

if isnull(is_cod_fornitore) or len(is_cod_fornitore) < 1 then
   g_mb.messagebox("Trend Valutazione Fornitore","Selezionare il Fornitore dall'apposita ListBox",StopSign!)
   return
end if

ll_anno_inizio = long(em_anno_inizio.text)
ll_mese_inizio = long(left(ddlb_mesi.text,2))
ll_i = 1

if ll_anno_inizio < 1 then
   g_mb.messagebox("Calcolo Periodi","Impostare anno iniziale")
   return
end if
if ll_mese_inizio < 1 then
   g_mb.messagebox("Calcolo Periodi","Impostare mese iniziale")
   return
end if

setpointer(hourglass!)
COMMIT;

DELETE from trend_valutazione_for
WHERE  (trend_valutazione_for.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       (trend_valutazione_for.cod_fornitore = :is_cod_fornitore ) ;   
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Cancellazione Precedente Valutazione","Cancellazione precedente valutazione fallita: " + sqlca.sqlerrtext, stopsign!)
   ROLLBACK;
   return
end if

DELETE from det_valutazione_for
WHERE  (det_valutazione_for.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       (det_valutazione_for.cod_fornitore = :is_cod_fornitore ) ;   
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Cancellazione Precedente Valutazione","Cancellazione precedente valutazione fallita: " + sqlca.sqlerrtext, stopsign!)
   ROLLBACK;
   return
end if


for ll_i = 1 to 12
if (id_date_inizio[ll_i] > datetime(date(01-01-1900))) and (id_date_fine[ll_i] <> datetime(date(00-00-00))) then
   is_cod_fornitore = trim(is_cod_fornitore)
   ls_des_periodo = "Dal "+string(id_date_inizio[ll_i],"dd/mm/yyyy")+" al "+string(id_date_fine[ll_i],"dd/mm/yyyy")

   f_crea_trend_valutazione_for(is_cod_fornitore, ll_i, ls_des_periodo)
   f_carica_trend_fornitori(is_cod_fornitore,VCD1,VCD2,VCD,VCE,VS1,VS2,VS3,VS,VQ,VT,id_date_inizio[ll_i], id_date_fine[ll_i], ll_i)
   choose case ll_i
   case 1 
       sle_vt_1.text = string(VT)
   case 2 
       sle_vt_2.text = string(VT)
   case 3 
       sle_vt_3.text = string(VT)
   case 4
       sle_vt_4.text = string(VT)
   case 5
       sle_vt_5.text = string(VT)
   case 6 
       sle_vt_6.text = string(VT)
   case 7 
       sle_vt_7.text = string(VT)
   case 8 
       sle_vt_8.text = string(VT)
   case 9 
       sle_vt_9.text = string(VT)
   case 10 
       sle_vt_10.text = string(VT)
   case 11
       sle_vt_11.text = string(VT)
   case 12 
       sle_vt_12.text = string(VT)
   end choose

end if
next
setpointer(arrow!)
end event

type st_8 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 20
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_vt_1 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 20
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type em_1 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 20
integer width = 389
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_2 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 20
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type st_10 from statictext within w_trend_valutazione_for
integer y = 20
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo 1 dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_11 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 20
integer width = 82
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_3 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 120
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_4 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 120
integer width = 389
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type st_12 from statictext within w_trend_valutazione_for
integer y = 120
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo 2  dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_13 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 120
integer width = 82
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_14 from statictext within w_trend_valutazione_for
integer y = 920
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo 10  dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_15 from statictext within w_trend_valutazione_for
integer y = 820
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo 9  dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_16 from statictext within w_trend_valutazione_for
integer y = 720
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo 8  dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_17 from statictext within w_trend_valutazione_for
integer y = 620
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo  7  dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_18 from statictext within w_trend_valutazione_for
integer y = 520
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo 6  dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_19 from statictext within w_trend_valutazione_for
integer y = 420
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo 5  dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_20 from statictext within w_trend_valutazione_for
integer y = 320
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo 4  dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_21 from statictext within w_trend_valutazione_for
integer y = 220
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo 3  dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_22 from statictext within w_trend_valutazione_for
integer y = 1120
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo 12  dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_23 from statictext within w_trend_valutazione_for
integer y = 1020
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Periodo 11  dal"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_7 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 1120
integer width = 389
integer height = 80
integer taborder = 340
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_8 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 1020
integer width = 389
integer height = 80
integer taborder = 310
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_9 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 920
integer width = 389
integer height = 80
integer taborder = 280
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_10 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 820
integer width = 389
integer height = 80
integer taborder = 250
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_11 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 720
integer width = 389
integer height = 80
integer taborder = 220
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_12 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 620
integer width = 389
integer height = 80
integer taborder = 190
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_13 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 520
integer width = 389
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_14 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 420
integer width = 389
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_15 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 320
integer width = 389
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_16 from editmask within w_trend_valutazione_for
integer x = 434
integer y = 220
integer width = 389
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_30 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 1120
integer width = 389
integer height = 80
integer taborder = 350
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_31 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 1020
integer width = 389
integer height = 80
integer taborder = 320
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_32 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 920
integer width = 389
integer height = 80
integer taborder = 290
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_33 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 820
integer width = 389
integer height = 80
integer taborder = 260
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_34 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 720
integer width = 389
integer height = 80
integer taborder = 230
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_35 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 620
integer width = 389
integer height = 80
integer taborder = 200
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_36 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 520
integer width = 389
integer height = 80
integer taborder = 170
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_37 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 420
integer width = 389
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_38 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 320
integer width = 389
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type em_39 from editmask within w_trend_valutazione_for
integer x = 937
integer y = 220
integer width = 389
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

type st_30 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 1120
integer width = 82
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_31 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 1020
integer width = 82
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_32 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 920
integer width = 82
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_33 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 820
integer width = 82
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_34 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 720
integer width = 69
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_35 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 620
integer width = 82
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_36 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 520
integer width = 82
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_37 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 420
integer width = 82
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_38 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 320
integer width = 82
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_39 from statictext within w_trend_valutazione_for
integer x = 846
integer y = 220
integer width = 82
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "al"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_anno_inizio from editmask within w_trend_valutazione_for
integer x = 1029
integer y = 1320
integer width = 389
integer height = 80
integer taborder = 390
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
boolean spin = true
double increment = 1
string minmax = "1900~~2999"
end type

type ddlb_mesi from dropdownlistbox within w_trend_valutazione_for
integer x = 434
integer y = 1320
integer width = 571
integer height = 280
integer taborder = 380
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
string item[] = {"01 - Gennaio","02 - Febbraio","03 - Marzo","04 - Aprile","05 - Maggio","06 - Giugno","07 - Luglio","08 - Agosto","09 - Settembre","10 - Ottobre","11 - Novembre","12 - Dicembre"}
end type

type cb_grafico from commandbutton within w_trend_valutazione_for
integer x = 1920
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 420
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Grafico"
end type

event clicked;dw_trend_valutazione_for_ext.accepttext()

is_cod_fornitore = dw_trend_valutazione_for_ext.getitemstring( 1, "cod_fornitore")

if is_cod_fornitore = "" then setnull(is_cod_fornitore)

if len(is_cod_fornitore) > 0 and not isnull(is_cod_fornitore) then
   s_cs_xx.parametri.parametro_s_1 = is_cod_fornitore
   window_open(w_graf_tend_vendor_rat,-1)
end if
end event

type rb_1 from radiobutton within w_trend_valutazione_for
integer x = 46
integer y = 1500
integer width = 283
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Mensile"
end type

on clicked;is_Tipo_Trend = "M"
end on

type rb_2 from radiobutton within w_trend_valutazione_for
integer x = 366
integer y = 1500
integer width = 338
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Bimensile"
end type

on clicked;is_Tipo_Trend = "B"
end on

type rb_3 from radiobutton within w_trend_valutazione_for
integer x = 741
integer y = 1500
integer width = 361
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Timestrale"
end type

on clicked;is_Tipo_Trend = "T"
end on

type rb_4 from radiobutton within w_trend_valutazione_for
integer x = 1138
integer y = 1500
integer width = 384
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Semestrale"
end type

on clicked;is_Tipo_Trend = "S"
end on

type rb_5 from radiobutton within w_trend_valutazione_for
integer x = 1554
integer y = 1500
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Annuale"
end type

on clicked;is_Tipo_Trend = "A"
end on

type gb_1 from groupbox within w_trend_valutazione_for
integer x = 23
integer y = 1420
integer width = 1851
integer height = 180
integer taborder = 400
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Periodi"
end type

type sle_vt_2 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 120
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_vt_3 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 220
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_vt_4 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 320
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_vt_5 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 420
integer width = 366
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_vt_6 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 520
integer width = 366
integer height = 80
integer taborder = 180
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_vt_7 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 620
integer width = 366
integer height = 80
integer taborder = 210
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_vt_8 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 720
integer width = 366
integer height = 80
integer taborder = 240
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_vt_9 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 820
integer width = 366
integer height = 80
integer taborder = 270
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_vt_10 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 920
integer width = 366
integer height = 80
integer taborder = 300
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_vt_12 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 1120
integer width = 366
integer height = 80
integer taborder = 360
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_vt_11 from singlelineedit within w_trend_valutazione_for
integer x = 1920
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 330
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean autohscroll = false
end type

type st_50 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 1020
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_51 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 920
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_52 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 820
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_53 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 720
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_54 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 620
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_55 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 520
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_56 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 420
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_57 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 320
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_58 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 220
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_9 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 120
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_60 from statictext within w_trend_valutazione_for
integer x = 1417
integer y = 1120
integer width = 503
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_trend_valutazione_for_ext from uo_cs_xx_dw within w_trend_valutazione_for
integer x = 137
integer y = 1200
integer width = 2149
integer height = 120
integer taborder = 50
string dataobject = "d_trend_valutazione_for_ext"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_trend_valutazione_for_ext,"cod_fornitore")
end choose
end event

