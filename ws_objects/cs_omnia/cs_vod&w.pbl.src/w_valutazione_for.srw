﻿$PBExportHeader$w_valutazione_for.srw
$PBExportComments$Finestra Visualizzazione Vendor Rating
forward
global type w_valutazione_for from w_cs_xx_principale
end type
type dw_graf_vis_vendor_rating from uo_cs_graph within w_valutazione_for
end type
type sle_vcd from singlelineedit within w_valutazione_for
end type
type st_1 from statictext within w_valutazione_for
end type
type st_2 from statictext within w_valutazione_for
end type
type st_3 from statictext within w_valutazione_for
end type
type sle_vce from singlelineedit within w_valutazione_for
end type
type sle_vq from singlelineedit within w_valutazione_for
end type
type st_4 from statictext within w_valutazione_for
end type
type st_5 from statictext within w_valutazione_for
end type
type st_6 from statictext within w_valutazione_for
end type
type sle_vs1 from singlelineedit within w_valutazione_for
end type
type sle_vs2 from singlelineedit within w_valutazione_for
end type
type sle_vs3 from singlelineedit within w_valutazione_for
end type
type st_7 from statictext within w_valutazione_for
end type
type sle_vs from singlelineedit within w_valutazione_for
end type
type st_8 from statictext within w_valutazione_for
end type
type sle_vt from singlelineedit within w_valutazione_for
end type
type dw_folder from u_folder within w_valutazione_for
end type
type r_1 from rectangle within w_valutazione_for
end type
type dw_ricerca from uo_cs_xx_dw within w_valutazione_for
end type
type cb_annulla from commandbutton within w_valutazione_for
end type
type cb_prm_val_forn from commandbutton within w_valutazione_for
end type
type cb_report from commandbutton within w_valutazione_for
end type
type cb_valutazione from commandbutton within w_valutazione_for
end type
end forward

global type w_valutazione_for from w_cs_xx_principale
integer width = 3726
integer height = 1996
string title = "Calcolo ~"Vendor Rating~""
dw_graf_vis_vendor_rating dw_graf_vis_vendor_rating
sle_vcd sle_vcd
st_1 st_1
st_2 st_2
st_3 st_3
sle_vce sle_vce
sle_vq sle_vq
st_4 st_4
st_5 st_5
st_6 st_6
sle_vs1 sle_vs1
sle_vs2 sle_vs2
sle_vs3 sle_vs3
st_7 st_7
sle_vs sle_vs
st_8 st_8
sle_vt sle_vt
dw_folder dw_folder
r_1 r_1
dw_ricerca dw_ricerca
cb_annulla cb_annulla
cb_prm_val_forn cb_prm_val_forn
cb_report cb_report
cb_valutazione cb_valutazione
end type
global w_valutazione_for w_valutazione_for

type variables
string is_cod_fornitore
datetime idt_da_data, idt_a_data
end variables

forward prototypes
public subroutine wf_retrieve_grafico (string fs_cod_forn)
public subroutine wf_retrieve_grafico_tutti (datetime f_da_data, datetime f_a_data)
end prototypes

public subroutine wf_retrieve_grafico (string fs_cod_forn);string ls_rag_soc_1

SELECT anag_fornitori.rag_soc_1  
INTO   :ls_rag_soc_1
FROM   anag_fornitori  
WHERE  ( anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( anag_fornitori.cod_fornitore = :is_cod_fornitore)   ;

if isnull(ls_rag_soc_1) then ls_rag_soc_1 = ""


//SEZIONE 1 ###############################################################################
//creazione e popolamento del datastore di appoggio
//N.B. potrebbe essere usato anche una datawindow al posto di un datastore, purchè
//     questo venga specificato nell'oggetto grafico (vedi più avanti SEZIONE 2)
//----------------------------------------------------------------------------------------
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_graf_vis_vendor_rating" //immettere qui il nome del datastore
lds_dati.settransobject(sqlca)
lds_dati.Retrieve(s_cs_xx.cod_azienda, is_cod_fornitore) //gli argomenti di retrieve dipendono dal datastore

//########################################################################################

//SEZIONE 2 ###############################################################################
//impostazione dell'oggetto grafico
//----------------------------------------------------------------------------------------

//si vuole usare un datastore (valore di default è TRUE, mettere FALSE se si tratta di datawindow)
//dw_graf_vis_vendor_rating è il nome dato al controllo grafico sulla finestra
dw_graf_vis_vendor_rating.ib_datastore = true

//impostare l'oggetto datastore (o datawindow: dw_grafico.idw_data )
dw_graf_vis_vendor_rating.ids_data = lds_dati

//impostazione corrispondenza campi datastore (o datawindow) e grafico
/*ESEMPIO 1
	se il datastore ha 3 campi stringa 2 datetime e 1 numerico (esempio long, decimal):

	dw_grafico.is_str[1] = "nome prima colonna del ds di tipo stringa"
	dw_grafico.is_str[2] = "nome seconda colonna del ds di tipo stringa"
	dw_grafico.is_str[3] = "nome terza colonna del ds di tipo stringa"
	dw_grafico.is_dtm[1] = "nome prima colonna del ds di tipo datetime"
	dw_grafico.is_dtm[2] = "nome seconda colonna del ds di tipo datetime"
	dw_grafico.is_num[1] = "nome prima colonna del ds di tipo long, decimal, ecc..."

N.B. non è importante l'ordine di specifica dei vari campi
*/
dw_graf_vis_vendor_rating.is_str[1] = "anag_fornitori_rag_soc_1" 
dw_graf_vis_vendor_rating.is_num[1] = "tes_valutazione_for_anno_registrazione"
dw_graf_vis_vendor_rating.is_num[2] = "tes_valutazione_for_punteggio"

//Questo serve per impostare l'ordinamento corretto della categoria prima di popolare il grafico
dw_graf_vis_vendor_rating.is_source_categoria_col = "tes_valutazione_for_anno_registrazione" //nome colonna del datastore che corrisponde alla categoria

//impostazione della categoria
/*ESEMPI
	se la categoria è una colonna di tipo string del datastore indicare "str_i"
	dove i è la posizione nell'array dw_grafico.is_istr[]

	se la categoria è una colonna di tipo datetime del datastore indicare "dtm_j"
	dove j è la posizione nell'array dw_grafico.is_dtm[]

	se la categoria è una colonna di tipo numerico del datastore indicare "num_k"
	dove k è la posizione nell'array dw_grafico.is_num[]
*/
dw_graf_vis_vendor_rating.is_categoria_col = "num_1"

//impostazione della categoria (analogo a quanto fatto per la categoria)
/*
	essa può essere una colonna numerica o una espressione numerica
	ESEMPI
		"num_2"
		"num_1 - num_2"
		"(hour(dtm_3)*60 + minute(dtm_3) - hour(dtm_2)*60 - minute(dtm_2))/60"
*/
dw_graf_vis_vendor_rating.is_exp_valore = "num_2" //default è "num_1"

//impostazione della categoria (analogo a quanto fatto per la categoria)
//lasciare stringa vuota se non si vuole gestire il grafico per serie
dw_graf_vis_vendor_rating.is_serie_col = "str_1"

//altre impostazioni del grafico
dw_graf_vis_vendor_rating.is_titolo = "Vendor Rating Fornitore (" + is_cod_fornitore + ")  " + ls_rag_soc_1
dw_graf_vis_vendor_rating.is_label_categoria= "Anno" //Etichetta sull'asse della categoria (Default ascissa)
dw_graf_vis_vendor_rating.is_label_valori = "Punteggio" //Etichetta sull'asse dei valori (Default valori)

//tipo scala: 1 se lineare, 2 se logaritmica in base 10
dw_graf_vis_vendor_rating.ii_scala = 1

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_graf_vis_vendor_rating.ii_tipo_grafico = 9  //Colonne 3D Obj

//colore di sfondo del grafico
//per il bianco (DEFAULT)	: 16777215
//per il silver			: 12632256
dw_graf_vis_vendor_rating.il_backcolor = 12632256 //silver

//metodo che visualizza i dati nel grafico
dw_graf_vis_vendor_rating.uof_retrieve( )

destroy lds_dati
//########################################################################################
end subroutine

public subroutine wf_retrieve_grafico_tutti (datetime f_da_data, datetime f_a_data);//per il momento in sospeso.....


//string ls_sql
//
//
//ls_sql = &
//"select anag_fornitori.rag_soc_1," + &
//		"tes_valutazione_for.anno_registrazione," + &
//		"tes_valutazione_for.punteggio " + &
//"from tes_valutazione_for " + &
//"join anag_fornitori on anag_fornitori.cod_azienda = tes_valutazione_for.cod_azienda and " + &
//						"anag_fornitori.cod_fornitore = tes_valutazione_for.cod_fornitore " + &
//"join acc_materiali on acc_materiali.cod_azienda = anag_fornitori.cod_azienda and " + &
//						"acc_materiali.cod_fornitore = anag_fornitori.cod_fornitore " + &
//"where anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "'  and " + &
//		"acc_materiali.data_eff_consegna between '"+string(date(f_da_data), s_cs_xx.db_funzioni.formato_data)+"' and '" +&
//												string(date(f_a_data),s_cs_xx.db_funzioni.formato_data)+"' "
end subroutine

event pc_setwindow;call super::pc_setwindow;//dw_graf_vis_vendor_rating.set_dw_options(sqlca, &
//                                         pcca.null_object, &
//													  c_nonew + &
//													  c_nomodify + &
//													  c_nodelete + &
//													  c_noretrieveonopen + &
//													  c_disableCC + &
//													  c_disableCCInsert, &
//													  c_default)
//
//
//iuo_dw_main = dw_graf_vis_vendor_rating
//cb_valutazione.postevent("clicked")

windowobject lw_oggetti[]
datetime ldt_inizio, ldt_fine

set_w_options(c_closenosave + c_autoposition) //+ c_noresizewin)

dw_ricerca.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

lw_oggetti[1] = dw_ricerca
//lw_oggetti[2] = cb_ric_for
lw_oggetti[2] = cb_annulla
lw_oggetti[3] = cb_valutazione
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

lw_oggetti[1] = dw_graf_vis_vendor_rating
lw_oggetti[2] = cb_report
lw_oggetti[3] = st_1
lw_oggetti[4] = st_2
lw_oggetti[5] = st_3
lw_oggetti[6] = st_4
lw_oggetti[7] = st_5
lw_oggetti[8] = st_6
lw_oggetti[9] = st_7
lw_oggetti[10] = st_8
lw_oggetti[11] = r_1
lw_oggetti[12] = sle_vcd
lw_oggetti[13] = sle_vce
lw_oggetti[14] = sle_vq
lw_oggetti[15] = sle_vs
lw_oggetti[16] = sle_vs1
lw_oggetti[17] = sle_vs2
lw_oggetti[18] = sle_vs3
lw_oggetti[19] = sle_vt
lw_oggetti[20] = cb_prm_val_forn

dw_folder.fu_assigntab(2, "Valutazione", lw_oggetti[])

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

cb_annulla.postevent(clicked!)
end event

on w_valutazione_for.create
int iCurrent
call super::create
this.dw_graf_vis_vendor_rating=create dw_graf_vis_vendor_rating
this.sle_vcd=create sle_vcd
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.sle_vce=create sle_vce
this.sle_vq=create sle_vq
this.st_4=create st_4
this.st_5=create st_5
this.st_6=create st_6
this.sle_vs1=create sle_vs1
this.sle_vs2=create sle_vs2
this.sle_vs3=create sle_vs3
this.st_7=create st_7
this.sle_vs=create sle_vs
this.st_8=create st_8
this.sle_vt=create sle_vt
this.dw_folder=create dw_folder
this.r_1=create r_1
this.dw_ricerca=create dw_ricerca
this.cb_annulla=create cb_annulla
this.cb_prm_val_forn=create cb_prm_val_forn
this.cb_report=create cb_report
this.cb_valutazione=create cb_valutazione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_graf_vis_vendor_rating
this.Control[iCurrent+2]=this.sle_vcd
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.st_3
this.Control[iCurrent+6]=this.sle_vce
this.Control[iCurrent+7]=this.sle_vq
this.Control[iCurrent+8]=this.st_4
this.Control[iCurrent+9]=this.st_5
this.Control[iCurrent+10]=this.st_6
this.Control[iCurrent+11]=this.sle_vs1
this.Control[iCurrent+12]=this.sle_vs2
this.Control[iCurrent+13]=this.sle_vs3
this.Control[iCurrent+14]=this.st_7
this.Control[iCurrent+15]=this.sle_vs
this.Control[iCurrent+16]=this.st_8
this.Control[iCurrent+17]=this.sle_vt
this.Control[iCurrent+18]=this.dw_folder
this.Control[iCurrent+19]=this.r_1
this.Control[iCurrent+20]=this.dw_ricerca
this.Control[iCurrent+21]=this.cb_annulla
this.Control[iCurrent+22]=this.cb_prm_val_forn
this.Control[iCurrent+23]=this.cb_report
this.Control[iCurrent+24]=this.cb_valutazione
end on

on w_valutazione_for.destroy
call super::destroy
destroy(this.dw_graf_vis_vendor_rating)
destroy(this.sle_vcd)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.sle_vce)
destroy(this.sle_vq)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_6)
destroy(this.sle_vs1)
destroy(this.sle_vs2)
destroy(this.sle_vs3)
destroy(this.st_7)
destroy(this.sle_vs)
destroy(this.st_8)
destroy(this.sle_vt)
destroy(this.dw_folder)
destroy(this.r_1)
destroy(this.dw_ricerca)
destroy(this.cb_annulla)
destroy(this.cb_prm_val_forn)
destroy(this.cb_report)
destroy(this.cb_valutazione)
end on

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_ricerca, &
//                 "cod_fornitore", &
//					  sqlca, &
//					  "anag_fornitori", &
//					  "cod_fornitore", &
//					  "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type dw_graf_vis_vendor_rating from uo_cs_graph within w_valutazione_for
integer x = 1541
integer y = 172
integer width = 2107
integer height = 1544
integer taborder = 100
end type

type sle_vcd from singlelineedit within w_valutazione_for
integer x = 800
integer y = 316
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
boolean displayonly = true
end type

type st_1 from statictext within w_valutazione_for
integer x = 55
integer y = 316
integer width = 741
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Valutaz. Decisioni (VCD):"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_valutazione_for
integer x = 55
integer y = 416
integer width = 741
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Valutaz. Economica (VCE):"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_valutazione_for
integer x = 55
integer y = 516
integer width = 741
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Valutaz. Qualitativa (VQ):"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_vce from singlelineedit within w_valutazione_for
integer x = 800
integer y = 416
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
boolean displayonly = true
end type

type sle_vq from singlelineedit within w_valutazione_for
integer x = 800
integer y = 516
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
boolean displayonly = true
end type

type st_4 from statictext within w_valutazione_for
integer x = 55
integer y = 996
integer width = 741
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Valutaz. Servizio (VS):"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_5 from statictext within w_valutazione_for
integer x = 55
integer y = 796
integer width = 741
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Errori Consegne (VS2):"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_6 from statictext within w_valutazione_for
integer x = 55
integer y = 696
integer width = 741
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Tempi Consegna (VS1):"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_vs1 from singlelineedit within w_valutazione_for
integer x = 800
integer y = 696
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
boolean displayonly = true
end type

type sle_vs2 from singlelineedit within w_valutazione_for
integer x = 800
integer y = 796
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
boolean displayonly = true
end type

type sle_vs3 from singlelineedit within w_valutazione_for
integer x = 800
integer y = 896
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
boolean displayonly = true
end type

type st_7 from statictext within w_valutazione_for
integer x = 55
integer y = 896
integer width = 741
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Errori Formali (VS3):"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_vs from singlelineedit within w_valutazione_for
integer x = 800
integer y = 996
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
boolean displayonly = true
end type

type st_8 from statictext within w_valutazione_for
integer x = 55
integer y = 1148
integer width = 741
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Punteggio Qualità (VT):"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_vt from singlelineedit within w_valutazione_for
integer x = 800
integer y = 1140
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
boolean displayonly = true
end type

type dw_folder from u_folder within w_valutazione_for
integer x = 27
integer y = 28
integer width = 3653
integer height = 1836
integer taborder = 110
end type

type r_1 from rectangle within w_valutazione_for
integer linethickness = 5
long fillcolor = 12632256
integer x = 114
integer y = 296
integer width = 1211
integer height = 1168
end type

type dw_ricerca from uo_cs_xx_dw within w_valutazione_for
integer x = 73
integer y = 172
integer width = 2350
integer height = 712
integer taborder = 110
boolean bringtotop = true
string dataobject = "d_ext_selezione_for"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca,"cod_fornitore")
end choose
end event

type cb_annulla from commandbutton within w_valutazione_for
integer x = 1079
integer y = 688
integer width = 389
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string ls_null
datetime ldt_null, ldt_inizio, ldt_fine

setnull(ls_null)
setnull(ldt_null)

dw_ricerca.setitem(1, "cod_fornitore", ls_null)

//dw_ricerca.setitem(1, "data_inizio", ldt_null)
//dw_ricerca.setitem(1, "data_fine", ldt_null)
ldt_inizio = datetime(date(year(today()),1,1), 00:00:00)	//1 gennaio dell'anno corrente
ldt_fine = datetime(date(year(today()),12,31), 23:59:59)	//31 dicembre dell'anno corrente

dw_ricerca.setitem(1, "data_inizio", ldt_inizio)
dw_ricerca.setitem(1, "data_fine", ldt_fine)

setnull(is_cod_fornitore)

//setnull(idt_da_data)
//setnull(idt_a_data)
idt_da_data = ldt_inizio
idt_a_data = ldt_fine

end event

type cb_prm_val_forn from commandbutton within w_valutazione_for
integer x = 306
integer y = 1664
integer width = 480
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Param. Val.Forn."
end type

event clicked;window_open(w_parametri_valutazione_for, -1)
end event

type cb_report from commandbutton within w_valutazione_for
integer x = 805
integer y = 1664
integer width = 480
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;if not isvalid(w_report_valutazione) then
	s_cs_xx.parametri.parametro_s_1 = is_cod_fornitore
	if isnull(s_cs_xx.parametri.parametro_s_1) then
		g_mb.messagebox("OMNIA","Non è stato indicato nessun fornitore; impossibile visualizzare il report",StopSign!)
		return
	end if
	s_cs_xx.parametri.parametro_i_1 = year(date(idt_da_data))
	
	//Donato 28-09-2009 passaggio data da-a
	s_cs_xx.parametri.parametro_data_1= idt_da_data
	s_cs_xx.parametri.parametro_data_2 = idt_a_data
	
	window_open(w_report_valutazione, -1)
end if
end event

type cb_valutazione from commandbutton within w_valutazione_for
integer x = 1486
integer y = 688
integer width = 389
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Valutazione"
end type

event clicked;string ls_rag_soc_1
double VCD1,VCD2,VCD,VCE,VS1,VS2,VS3,VS,VQ,VT

setpointer(hourglass!)

//donato 01/07/2009 inserita la dw di ricerca direttamente sulla window in un folder
/*
window_open(w_richiesta_fornitore, 0)

if isnull(s_cs_xx.parametri.parametro_s_1) then return
is_cod_fornitore = s_cs_xx.parametri.parametro_s_1
idt_da_data = s_cs_xx.parametri.parametro_data_1
idt_a_data = s_cs_xx.parametri.parametro_data_2
*/

sle_vcd.text = ""
sle_vce.text = ""
sle_vq.text = ""
sle_vs.text = ""
sle_vs1.text = ""
sle_vs2.text = ""
sle_vs3.text = ""
sle_vt.text = ""

dw_ricerca.accepttext()
is_cod_fornitore = dw_ricerca.getitemstring(1, "cod_fornitore")
idt_da_data = dw_ricerca.getitemdatetime(1, "data_inizio")
idt_a_data = dw_ricerca.getitemdatetime(1, "data_fine")

if isnull(is_cod_fornitore) or len(is_cod_fornitore) < 1 then
	
	//per il momento in sospeso.....
	
//	if g_mb.messagebox("Trend Valutazione Fornitore","Non hai specificato il Fornitore: vuoi eseguire il calcolo per tutti i Fornitori " + &
//											"che hanno accettazioni nel periodo di riferimento?",Question!, YesNo!, 1) = 1 then
//		
//		setnull(is_cod_fornitore)
//	else
//		setnull(is_cod_fornitore)
//		return
//	end if
	
	setnull(is_cod_fornitore)
	g_mb.messagebox("Trend Valutazione Fornitore","Selezionare il Fornitore!",StopSign!)
	return
end if

if year(date(idt_da_data)) <> year(date(idt_a_data)) then
	g_mb.messagebox("Calcolo Vendor Rating","Impossibile eseguire calcolo a cavallo di due anni")
	return
end if

if idt_da_data > idt_a_data then
	g_mb.messagebox("Calcolo Vendor Rating","ERRORE !! Data di inizio maggiore della data di fine")
	return
end if

if idt_da_data < datetime("01/01/1900") or isnull(idt_da_data) then
	setnull(idt_da_data)
	g_mb.messagebox("Calcolo Vendor Rating","Verificare data inizio calcolo")
	return
end if
   
if idt_a_data < datetime("01/01/1900") or isnull(idt_a_data) then
	setnull(idt_a_data)
	g_mb.messagebox("Calcolo Vendor Rating","Verificare data fine calcolo")
	return
end if

if isnull(is_cod_fornitore) then
	//più fornitori per volta
	
	//per il momento in sospeso.....
	
	//wf_retrieve_grafico_tutti(idt_da_data, idt_a_data)
else
	//un solo fornitore
	
	delete from det_valutazione_for
	where  ( det_valutazione_for.cod_azienda = :s_cs_xx.cod_azienda ) AND  
				 ( det_valutazione_for.cod_fornitore = :is_cod_fornitore) ;   
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Cancellazione Precedente Valutazione","Cancellazione precedente valutazione fallita: " + sqlca.sqlerrtext, stopsign!)
	end if
	
	f_carica_val_fornitori(is_cod_fornitore,VCD1,VCD2,VCD,VCE,VS1,VS2,VS3,VS,VQ,VT,idt_da_data,idt_a_data, true)
	
	sle_vcd.text = string(VCD)
	sle_vce.text = string(VCE)
	sle_vq.text = string(VQ)
	sle_vs.text = string(VS)
	sle_vs1.text = string(VS1)
	sle_vs2.text = string(VS2)
	sle_vs3.text = string(VS3)
	sle_vt.text = string(VT)
	
	setnull(s_cs_xx.parametri.parametro_d_10)
	setnull(s_cs_xx.parametri.parametro_d_11)
	
	wf_retrieve_grafico( is_cod_fornitore )
end if

dw_folder.fu_selecttab(2)

setpointer(arrow!)
end event

