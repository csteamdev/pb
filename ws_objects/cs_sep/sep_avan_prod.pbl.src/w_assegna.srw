﻿$PBExportHeader$w_assegna.srw
$PBExportComments$Window Assegna
forward
global type w_assegna from w_cs_xx_risposta
end type
type cb_1 from commandbutton within w_assegna
end type
type em_quan_assegnata from editmask within w_assegna
end type
type st_1 from statictext within w_assegna
end type
type cb_annulla from commandbutton within w_assegna
end type
type cb_stock from commandbutton within w_assegna
end type
end forward

global type w_assegna from w_cs_xx_risposta
int Width=1226
int Height=365
boolean TitleBar=true
string Title="Assegnazione"
boolean ControlMenu=false
cb_1 cb_1
em_quan_assegnata em_quan_assegnata
st_1 st_1
cb_annulla cb_annulla
cb_stock cb_stock
end type
global w_assegna w_assegna

type variables
integer ii_ok
end variables

on w_assegna.create
int iCurrent
call w_cs_xx_risposta::create
this.cb_1=create cb_1
this.em_quan_assegnata=create em_quan_assegnata
this.st_1=create st_1
this.cb_annulla=create cb_annulla
this.cb_stock=create cb_stock
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_1
this.Control[iCurrent+2]=em_quan_assegnata
this.Control[iCurrent+3]=st_1
this.Control[iCurrent+4]=cb_annulla
this.Control[iCurrent+5]=cb_stock
end on

on w_assegna.destroy
call w_cs_xx_risposta::destroy
destroy(this.cb_1)
destroy(this.em_quan_assegnata)
destroy(this.st_1)
destroy(this.cb_annulla)
destroy(this.cb_stock)
end on

event open;call super::open;ii_ok=-1
em_quan_assegnata.text=string(s_cs_xx.parametri.parametro_d_2)
em_quan_assegnata.setfocus()
end event

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

type cb_1 from commandbutton within w_assegna
int X=412
int Y=161
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Ass.Autom."
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = double(em_quan_assegnata.text)
s_cs_xx.parametri.parametro_i_1 = 1

if s_cs_xx.parametri.parametro_d_1 > s_cs_xx.parametri.parametro_d_2 then	
	g_mb.messagebox("Sep","Attenzione! La quantita assegnata supera la quantità in ordine. Reinserire il valore.",exclamation!)
	em_quan_assegnata.setfocus()
	return
end if

if s_cs_xx.parametri.parametro_d_1 =0 then
	g_mb.messagebox("Sep","Attenzione! La quantita assegnata deve essere >0. Reinserire il valore.",exclamation!)
	em_quan_assegnata.setfocus()
	return
end if

ii_ok=0

close (parent)
end event

type em_quan_assegnata from editmask within w_assegna
int X=709
int Y=41
int Width=458
int Height=81
int TabOrder=30
boolean BringToTop=true
Alignment Alignment=Right!
BorderStyle BorderStyle=StyleLowered!
string Mask="########.####"
MaskDataType MaskDataType=DecimalMask!
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_assegna
int X=69
int Y=41
int Width=622
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Quantità da Assegnare:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_annulla from commandbutton within w_assegna
event clicked pbm_bnclicked
int X=801
int Y=161
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 0
ii_ok = 0
close (parent)
end event

type cb_stock from commandbutton within w_assegna
event clicked pbm_bnclicked
int X=23
int Y=161
int Width=366
int Height=81
int TabOrder=3
boolean BringToTop=true
string Text="&Ass. Man."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = double(em_quan_assegnata.text)
s_cs_xx.parametri.parametro_i_1 = 2

if s_cs_xx.parametri.parametro_d_1 > s_cs_xx.parametri.parametro_d_2 then	
	g_mb.messagebox("Sep","Attenzione! La quantita assegnata supera la quantità in ordine. Reinserire il valore.",exclamation!)
	em_quan_assegnata.setfocus()
	return
end if

if s_cs_xx.parametri.parametro_d_1 =0 then
	g_mb.messagebox("Sep","Attenzione! La quantita assegnata deve essere >0. Reinserire il valore.",exclamation!)
	em_quan_assegnata.setfocus()
	return
end if

ii_ok=0

close (parent)
end event

