﻿$PBExportHeader$w_avanzamento_produzione_commesse.srw
$PBExportComments$Window avanzamento_produzione_commesse
forward
global type w_avanzamento_produzione_commesse from w_cs_xx_principale
end type
type dw_avanzamento_produzione_commesse_lista from uo_cs_xx_dw within w_avanzamento_produzione_commesse
end type
type dw_avanzamento_produzione_commesse_det from uo_cs_xx_dw within w_avanzamento_produzione_commesse
end type
type ddlb_operai from dropdownlistbox within w_avanzamento_produzione_commesse
end type
type st_operaio from statictext within w_avanzamento_produzione_commesse
end type
type cb_fine_fase from commandbutton within w_avanzamento_produzione_commesse
end type
type gb_2 from groupbox within w_avanzamento_produzione_commesse
end type
type gb_1 from groupbox within w_avanzamento_produzione_commesse
end type
type rb_attrezzaggio_commessa from radiobutton within w_avanzamento_produzione_commesse
end type
type rb_attrezzaggio from radiobutton within w_avanzamento_produzione_commesse
end type
type rb_lavorazione from radiobutton within w_avanzamento_produzione_commesse
end type
type cb_det_orari from commandbutton within w_avanzamento_produzione_commesse
end type
type cb_inizio from commandbutton within w_avanzamento_produzione_commesse
end type
type rb_risorsa_umana from radiobutton within w_avanzamento_produzione_commesse
end type
type cb_fine from commandbutton within w_avanzamento_produzione_commesse
end type
type em_orario from editmask within w_avanzamento_produzione_commesse
end type
type st_1 from statictext within w_avanzamento_produzione_commesse
end type
type cbx_orario_automatico from checkbox within w_avanzamento_produzione_commesse
end type
type em_data from editmask within w_avanzamento_produzione_commesse
end type
type st_2 from statictext within w_avanzamento_produzione_commesse
end type
type cb_stampa_fasi from commandbutton within w_avanzamento_produzione_commesse
end type
type cb_elimina_det_orari from commandbutton within w_avanzamento_produzione_commesse
end type
end forward

global type w_avanzamento_produzione_commesse from w_cs_xx_principale
integer width = 3543
integer height = 1720
string title = "Fasi Lavorazione Per Commessa (Assegnazione)"
dw_avanzamento_produzione_commesse_lista dw_avanzamento_produzione_commesse_lista
dw_avanzamento_produzione_commesse_det dw_avanzamento_produzione_commesse_det
ddlb_operai ddlb_operai
st_operaio st_operaio
cb_fine_fase cb_fine_fase
gb_2 gb_2
gb_1 gb_1
rb_attrezzaggio_commessa rb_attrezzaggio_commessa
rb_attrezzaggio rb_attrezzaggio
rb_lavorazione rb_lavorazione
cb_det_orari cb_det_orari
cb_inizio cb_inizio
rb_risorsa_umana rb_risorsa_umana
cb_fine cb_fine
em_orario em_orario
st_1 st_1
cbx_orario_automatico cbx_orario_automatico
em_data em_data
st_2 st_2
cb_stampa_fasi cb_stampa_fasi
cb_elimina_det_orari cb_elimina_det_orari
end type
global w_avanzamento_produzione_commesse w_avanzamento_produzione_commesse

type variables

end variables

on w_avanzamento_produzione_commesse.create
int iCurrent
call super::create
this.dw_avanzamento_produzione_commesse_lista=create dw_avanzamento_produzione_commesse_lista
this.dw_avanzamento_produzione_commesse_det=create dw_avanzamento_produzione_commesse_det
this.ddlb_operai=create ddlb_operai
this.st_operaio=create st_operaio
this.cb_fine_fase=create cb_fine_fase
this.gb_2=create gb_2
this.gb_1=create gb_1
this.rb_attrezzaggio_commessa=create rb_attrezzaggio_commessa
this.rb_attrezzaggio=create rb_attrezzaggio
this.rb_lavorazione=create rb_lavorazione
this.cb_det_orari=create cb_det_orari
this.cb_inizio=create cb_inizio
this.rb_risorsa_umana=create rb_risorsa_umana
this.cb_fine=create cb_fine
this.em_orario=create em_orario
this.st_1=create st_1
this.cbx_orario_automatico=create cbx_orario_automatico
this.em_data=create em_data
this.st_2=create st_2
this.cb_stampa_fasi=create cb_stampa_fasi
this.cb_elimina_det_orari=create cb_elimina_det_orari
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_avanzamento_produzione_commesse_lista
this.Control[iCurrent+2]=this.dw_avanzamento_produzione_commesse_det
this.Control[iCurrent+3]=this.ddlb_operai
this.Control[iCurrent+4]=this.st_operaio
this.Control[iCurrent+5]=this.cb_fine_fase
this.Control[iCurrent+6]=this.gb_2
this.Control[iCurrent+7]=this.gb_1
this.Control[iCurrent+8]=this.rb_attrezzaggio_commessa
this.Control[iCurrent+9]=this.rb_attrezzaggio
this.Control[iCurrent+10]=this.rb_lavorazione
this.Control[iCurrent+11]=this.cb_det_orari
this.Control[iCurrent+12]=this.cb_inizio
this.Control[iCurrent+13]=this.rb_risorsa_umana
this.Control[iCurrent+14]=this.cb_fine
this.Control[iCurrent+15]=this.em_orario
this.Control[iCurrent+16]=this.st_1
this.Control[iCurrent+17]=this.cbx_orario_automatico
this.Control[iCurrent+18]=this.em_data
this.Control[iCurrent+19]=this.st_2
this.Control[iCurrent+20]=this.cb_stampa_fasi
this.Control[iCurrent+21]=this.cb_elimina_det_orari
end on

on w_avanzamento_produzione_commesse.destroy
call super::destroy
destroy(this.dw_avanzamento_produzione_commesse_lista)
destroy(this.dw_avanzamento_produzione_commesse_det)
destroy(this.ddlb_operai)
destroy(this.st_operaio)
destroy(this.cb_fine_fase)
destroy(this.gb_2)
destroy(this.gb_1)
destroy(this.rb_attrezzaggio_commessa)
destroy(this.rb_attrezzaggio)
destroy(this.rb_lavorazione)
destroy(this.cb_det_orari)
destroy(this.cb_inizio)
destroy(this.rb_risorsa_umana)
destroy(this.cb_fine)
destroy(this.em_orario)
destroy(this.st_1)
destroy(this.cbx_orario_automatico)
destroy(this.em_data)
destroy(this.st_2)
destroy(this.cb_stampa_fasi)
destroy(this.cb_elimina_det_orari)
end on

event pc_setwindow;call super::pc_setwindow;dw_avanzamento_produzione_commesse_lista.set_dw_options(sqlca, &
                                    						  i_openparm, &
				 		                                      c_scrollparent + c_nonew +  c_nodelete, &
      		                                            c_default)

dw_avanzamento_produzione_commesse_det.set_dw_options(sqlca,dw_avanzamento_produzione_commesse_lista,c_sharedata + c_scrollparent + c_nonew + c_nodelete,c_default)

iuo_dw_main = dw_avanzamento_produzione_commesse_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDLB(ddlb_operai, sqlca, "anag_operai", &	
				  "cod_operaio", "cognome", &
				  "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
end event

type dw_avanzamento_produzione_commesse_lista from uo_cs_xx_dw within w_avanzamento_produzione_commesse
integer x = 23
integer y = 20
integer width = 1714
integer height = 1080
integer taborder = 90
string dataobject = "d_avanzamento_produzione_commesse_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa,ll_prog_riga)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_avanzamento_produzione_commesse_det from uo_cs_xx_dw within w_avanzamento_produzione_commesse
integer x = 1760
integer y = 20
integer width = 1691
integer height = 1260
integer taborder = 130
string dataobject = "d_avanzamento_produzione_commesse_det"
borderstyle borderstyle = styleraised!
end type

type ddlb_operai from dropdownlistbox within w_avanzamento_produzione_commesse
integer x = 2309
integer y = 1300
integer width = 1143
integer height = 1300
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

type st_operaio from statictext within w_avanzamento_produzione_commesse
integer x = 1760
integer y = 1320
integer width = 549
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Operaio Selezionato:"
boolean focusrectangle = false
end type

type cb_fine_fase from commandbutton within w_avanzamento_produzione_commesse
integer x = 2309
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Fine F&ase"
end type

event clicked;long      ll_num_commessa,ll_prog_riga
string    ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio, ls_cod_versione, & 
		    ls_flag_fine_fase,ls_errore
double    ldd_quan_prodotta,ldd_quan_in_produzione
integer   li_anno_commessa,li_risposta,li_flag_ultima

li_anno_commessa = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "num_commessa")
ll_prog_riga = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "prog_riga")
ls_cod_prodotto = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"cod_prodotto")
ls_cod_reparto = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"cod_reparto")
ls_cod_lavorazione = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"cod_lavorazione")
ls_flag_fine_fase = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"flag_fine_fase")
ls_cod_operaio= f_po_selectddlb(ddlb_operai)

li_risposta=f_fine_fase(li_anno_commessa,ll_num_commessa,ll_prog_riga, & 
								ls_cod_prodotto, ls_cod_versione, ls_cod_reparto,ls_cod_lavorazione, & 
								ls_flag_fine_fase,ls_cod_operaio,ldd_quan_prodotta, & 
								ldd_quan_in_produzione,li_flag_ultima,1,2,ls_errore)

if li_risposta= -1 then
	g_mb.messagebox("Sep",ls_errore,information!)
	rollback;
	return
end if

commit;

dw_avanzamento_produzione_commesse_det.setitem(dw_avanzamento_produzione_commesse_det.getrow(),"flag_fine_fase","S")
dw_avanzamento_produzione_commesse_det.resetupdate()

if li_flag_ultima = 1 then
	dw_avanzamento_produzione_commesse_lista.i_parentdw.setitem(dw_avanzamento_produzione_commesse_det.i_parentdw.getrow(),"quan_in_produzione",ldd_quan_in_produzione)
	dw_avanzamento_produzione_commesse_lista.i_parentdw.setitem(dw_avanzamento_produzione_commesse_det.i_parentdw.getrow(),"quan_prodotta",ldd_quan_prodotta)
	dw_avanzamento_produzione_commesse_lista.i_parentdw.resetupdate()
	dw_avanzamento_produzione_commesse_lista.triggerevent("pcd_retrieve")
end if
end event

type gb_2 from groupbox within w_avanzamento_produzione_commesse
integer x = 23
integer y = 1120
integer width = 1714
integer height = 180
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Data/Orario Sessione"
end type

type gb_1 from groupbox within w_avanzamento_produzione_commesse
integer x = 23
integer y = 1320
integer width = 1097
integer height = 180
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Tipo Sessione"
end type

type rb_attrezzaggio_commessa from radiobutton within w_avanzamento_produzione_commesse
integer x = 46
integer y = 1380
integer width = 251
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Atrz.C."
boolean lefttext = true
end type

type rb_attrezzaggio from radiobutton within w_avanzamento_produzione_commesse
integer x = 343
integer y = 1380
integer width = 206
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Atrz."
boolean lefttext = true
end type

type rb_lavorazione from radiobutton within w_avanzamento_produzione_commesse
integer x = 594
integer y = 1380
integer width = 183
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Lav."
boolean checked = true
boolean lefttext = true
end type

type cb_det_orari from commandbutton within w_avanzamento_produzione_commesse
event clicked pbm_bnclicked
integer x = 2697
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Det. Orari"
end type

event clicked;window_open_parm(w_det_orari_produzione,-1,dw_avanzamento_produzione_commesse_det)
end event

type cb_inizio from commandbutton within w_avanzamento_produzione_commesse
event clicked pbm_bnclicked
integer x = 1531
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Inizio Ses."
end type

event clicked;LONG    li_anno_commessa,ll_num_commessa,ll_prog_riga
string  ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio, & 	
		  ls_flag_fine_fase,ls_errore,ls_cod_versione,ls_cod_prodotto_prima, ls_cod_versione_prima
double  ldd_quan_in_produzione
integer li_risposta,li_tipo_sessione

li_anno_commessa = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "num_commessa")
ll_prog_riga = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "prog_riga")
ls_cod_prodotto = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"cod_prodotto")
ls_cod_reparto = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"cod_reparto")
ls_cod_lavorazione = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"cod_lavorazione")
ldd_quan_in_produzione = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "quan_in_produzione")
ls_flag_fine_fase = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"flag_fine_fase")
ls_cod_operaio = f_po_selectddlb(ddlb_operai)

if rb_attrezzaggio.checked = true then li_tipo_sessione = 1
if rb_attrezzaggio_commessa.checked = true then li_tipo_sessione = 2
if rb_lavorazione.checked = true then li_tipo_sessione = 3

select cod_versione
into   :ls_cod_versione
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if rb_risorsa_umana.checked = true then 
	li_tipo_sessione = 4
	ls_flag_fine_fase="N"
end if

if cbx_orario_automatico.checked=false then
	s_cs_xx.parametri.parametro_t_1=datetime(date(s_cs_xx.db_funzioni.data_neutra),time(em_orario.text))
	s_cs_xx.parametri.parametro_data_1=datetime(date(em_data.text),time('00:00:00'))
else
	setnull(s_cs_xx.parametri.parametro_t_1)
	setnull(s_cs_xx.parametri.parametro_data_1)
end if

li_risposta = f_inizio_sessione ( li_anno_commessa, ll_num_commessa, ll_prog_riga, & 
											 ls_cod_prodotto, ls_cod_versione,ls_cod_reparto, ls_cod_lavorazione, & 
											 ls_cod_operaio, ldd_quan_in_produzione, ls_flag_fine_fase, & 
											 li_tipo_sessione, ls_cod_prodotto_prima, ls_cod_versione_prima, ls_errore )

if li_risposta = -1 or li_risposta = -3 then
	g_mb.messagebox("Sep",ls_errore,stopsign!)
	rollback;
	return
end if

commit;

g_mb.messagebox("Sep","Inizio sessione di fase completato con successo.",information!)


end event

type rb_risorsa_umana from radiobutton within w_avanzamento_produzione_commesse
integer x = 823
integer y = 1380
integer width = 274
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Ris.Um."
boolean lefttext = true
end type

type cb_fine from commandbutton within w_avanzamento_produzione_commesse
event clicked pbm_bnclicked
integer x = 1920
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Fine Ses."
end type

event clicked;LONG     ll_num_commessa,ll_prog_riga,ll_prog_orari
string   ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio, & 
		   ls_test,ls_flag_fine_fase,ls_errore, ls_cod_versione
datetime lt_orario_attrezzaggio,lt_orario_attrezzaggio_commessa,lt_orario_lavorazione
integer  li_risposta,li_flag_inizio,li_anno_commessa,li_flag_ultima,li_tipo_sessione
double   ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa,ldd_tempo_lavorazione, & 
		   ldd_quan_prodotta,ldd_quan_in_produzione, ldd_tempo_risorsa_umana	

li_anno_commessa = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "num_commessa")
ll_prog_riga = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "prog_riga")
ls_cod_prodotto = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"cod_prodotto")
ls_cod_reparto = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"cod_reparto")
ls_cod_lavorazione = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"cod_lavorazione")
ls_flag_fine_fase = dw_avanzamento_produzione_commesse_det.getitemstring(dw_avanzamento_produzione_commesse_det.getrow(),"flag_fine_fase")
ls_cod_operaio= f_po_selectddlb(ddlb_operai)

if rb_attrezzaggio.checked = true then li_tipo_sessione = 1
if rb_attrezzaggio_commessa.checked = true then li_tipo_sessione = 2
if rb_lavorazione.checked = true then li_tipo_sessione = 3
if rb_risorsa_umana.checked = true then li_tipo_sessione = 4 

if cbx_orario_automatico.checked=false then
	s_cs_xx.parametri.parametro_t_1=datetime(date(s_cs_xx.db_funzioni.data_neutra),time(em_orario.text))
	s_cs_xx.parametri.parametro_data_1=datetime(date(em_data.text),time('00:00:00'))
else
	setnull(s_cs_xx.parametri.parametro_t_1)
	setnull(s_cs_xx.parametri.parametro_data_1)
end if

select cod_versione
into   :ls_cod_versione
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

li_risposta = f_fine_sessione ( li_anno_commessa, ll_num_commessa, ll_prog_riga, & 
										  ls_cod_prodotto, ls_cod_versione, ls_cod_reparto, ls_cod_lavorazione, & 
										  ls_cod_operaio, ldd_tempo_attrezzaggio, & 
										  ldd_tempo_attrezzaggio_commessa,ldd_tempo_lavorazione, & 
										  ldd_tempo_risorsa_umana,ldd_quan_in_produzione, & 
										  ldd_quan_prodotta, ls_flag_fine_fase, & 
										  1,li_tipo_sessione,ls_errore )


if li_risposta = -1 then
	g_mb.messagebox("Sep",ls_errore,stopsign!)
	rollback;
	return
end if	

choose case li_risposta
	case 1
		dw_avanzamento_produzione_commesse_det.setitem(dw_avanzamento_produzione_commesse_det.getrow(),"tempo_attrezzaggio",ldd_tempo_attrezzaggio)
	case 2
		dw_avanzamento_produzione_commesse_det.setitem(dw_avanzamento_produzione_commesse_det.getrow(),"tempo_attrezzaggio_commessa",ldd_tempo_attrezzaggio_commessa)
	case 3
		dw_avanzamento_produzione_commesse_det.setitem(dw_avanzamento_produzione_commesse_det.getrow(),"quan_in_produzione",ldd_quan_in_produzione)
		dw_avanzamento_produzione_commesse_det.setitem(dw_avanzamento_produzione_commesse_det.getrow(),"quan_prodotta",ldd_quan_prodotta)
		dw_avanzamento_produzione_commesse_det.setitem(dw_avanzamento_produzione_commesse_det.getrow(),"tempo_lavorazione",ldd_tempo_lavorazione)
	case 4
		dw_avanzamento_produzione_commesse_det.setitem(dw_avanzamento_produzione_commesse_det.getrow(),"tempo_risorsa_umana",ldd_tempo_risorsa_umana)
end choose

dw_avanzamento_produzione_commesse_det.resetupdate()

if ldd_quan_in_produzione=0 and li_risposta = 3 then

	g_mb.messagebox("Sep","La quantità in produzione è uguale a 0 pertanto la fase sara chiusa automaticamente.",information!)

	li_risposta=f_fine_fase(li_anno_commessa,ll_num_commessa,ll_prog_riga, & 
									ls_cod_prodotto, &
									ls_cod_versione, &
									ls_cod_reparto,ls_cod_lavorazione, & 
									ls_flag_fine_fase,ls_cod_operaio,ldd_quan_prodotta, & 
									ldd_quan_in_produzione,li_flag_ultima,1,2,ls_errore)

	if li_risposta= -1 then
		g_mb.messagebox("Sep",ls_errore,information!)
		rollback;
		return
	end if

	commit;

	dw_avanzamento_produzione_commesse_det.setitem(dw_avanzamento_produzione_commesse_det.getrow(),"flag_fine_fase","S")
	dw_avanzamento_produzione_commesse_det.resetupdate()

	if li_flag_ultima = 1 then
		dw_avanzamento_produzione_commesse_lista.i_parentdw.setitem(dw_avanzamento_produzione_commesse_det.i_parentdw.getrow(),"quan_in_produzione",ldd_quan_in_produzione)
		dw_avanzamento_produzione_commesse_lista.i_parentdw.setitem(dw_avanzamento_produzione_commesse_det.i_parentdw.getrow(),"quan_prodotta",ldd_quan_prodotta)
		dw_avanzamento_produzione_commesse_lista.i_parentdw.resetupdate()
		dw_avanzamento_produzione_commesse_lista.triggerevent("pcd_retrieve")
	end if

end if

commit;

g_mb.messagebox("Sep","Fine sessione di fase completato con successo.",information!)

end event

type em_orario from editmask within w_avanzamento_produzione_commesse
integer x = 251
integer y = 1200
integer width = 297
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = timemask!
string mask = "hh:mm"
boolean spin = true
string displaydata = ""
end type

type st_1 from statictext within w_avanzamento_produzione_commesse
integer x = 46
integer y = 1200
integer width = 206
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Orario"
alignment alignment = center!
boolean focusrectangle = false
end type

type cbx_orario_automatico from checkbox within w_avanzamento_produzione_commesse
integer x = 1143
integer y = 1200
integer width = 571
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Orario Automatico"
boolean checked = true
end type

type em_data from editmask within w_avanzamento_produzione_commesse
integer x = 731
integer y = 1200
integer width = 366
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "~r"
end type

type st_2 from statictext within w_avanzamento_produzione_commesse
integer x = 571
integer y = 1200
integer width = 160
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Data"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_stampa_fasi from commandbutton within w_avanzamento_produzione_commesse
integer x = 1143
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Stampa F."
end type

event clicked;long    ll_job,ll_num_commessa,ll_prog_riga,ll_num_registrazione
integer li_anno_commessa,li_anno_registrazione
string  ls_anno_commessa,ls_num_commessa,ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione, &
		  ls_prog_riga,ls_buffer,ls_note_fase,ls_quan_in_produzione,ls_cod_prodotto_finito, & 
		  ls_des_prodotto_finito,ls_cod_materia_prima,ls_des_materia_prima,ls_cod_prodotto_1, &
		  ls_cod_cliente,ls_rag_soc_cliente
double  ldd_quan_in_produzione

li_anno_commessa = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "num_commessa")
ll_prog_riga = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "prog_riga")
ldd_quan_in_produzione = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "quan_in_produzione")

setnull(ls_cod_cliente)
setnull(li_anno_registrazione)

select anno_registrazione,
		 num_registrazione
into   :li_anno_registrazione,
		 :ll_num_registrazione
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode=0 then
	select cod_cliente	 
   into   :ls_cod_cliente
	from   tes_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;

	select rag_soc_1
	into   :ls_rag_soc_cliente
	from   anag_clienti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cliente=:ls_cod_cliente;

end if
	
select cod_prodotto
into   :ls_cod_prodotto_finito
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

select des_prodotto
into   :ls_des_prodotto_finito
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto_finito;

ll_job=printopen()
	
ls_anno_commessa = string(li_anno_commessa)
ls_num_commessa = string(ll_num_commessa)
ls_prog_riga = string(ll_prog_riga)
ls_quan_in_produzione= string(ldd_quan_in_produzione)

if len(ls_num_commessa) < 6 then
	ls_num_commessa = ls_num_commessa + fill(" ", 6 - len(ls_num_commessa))
end if

declare righe_avanzamento_pc_1 cursor for
select  cod_prodotto,
		  cod_reparto,
		  cod_lavorazione
from    avan_produzione_com
where   cod_azienda=:s_cs_xx.cod_azienda
and     anno_commessa=:li_anno_commessa
and     num_commessa=:ll_num_commessa
and     prog_riga=:ll_prog_riga;

open righe_avanzamento_pc_1;
	
do while 1=1
	fetch righe_avanzamento_pc_1
	into  :ls_cod_prodotto,	
  		   :ls_cod_reparto,
			:ls_cod_lavorazione;

	if sqlca.sqlcode = 100 then exit
	
	select des_estesa_prodotto
	into   :ls_note_fase
	from   tes_fasi_lavorazione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto
	and    cod_reparto=:ls_cod_reparto
	and    cod_lavorazione=:ls_cod_lavorazione;


	if len(ls_prog_riga) < 4 then
		ls_prog_riga=ls_prog_riga + fill(" ", 4 - len(ls_prog_riga))
	end if
			
	if len(ls_cod_prodotto) < 15 then
		ls_cod_prodotto=ls_cod_prodotto + fill(" ", 15 - len(ls_cod_prodotto))
	end if

	if len(ls_cod_reparto) < 6 then
		ls_cod_reparto=ls_cod_reparto + fill(" ", 6 - len(ls_cod_reparto))
	end if
	
	if len(ls_cod_lavorazione) < 6 then
		ls_cod_lavorazione=ls_cod_lavorazione + fill(" ", 6 - len(ls_cod_lavorazione))
	end if

	if not isnull(ls_cod_cliente) then
		ls_buffer="Cliente:" + char(27) + "G" + char(27) + "E" + ls_cod_cliente + " " + ls_rag_soc_cliente + char(27) + "F" + char(27) + "H" +char(13) + char(10)
		printsend(ll_job,ls_buffer)	
	end if

	if not isnull(li_anno_registrazione) then
		ls_buffer="Rif. Ordine: Anno Reg." + char(27) + "G" + char(27) + "E" + string(li_anno_registrazione) + " Numero:" + string(ll_num_registrazione) + char(27) + "F" + char(27) + "H" +char(13) + char(10)
		printsend(ll_job,ls_buffer)	
	end if

	ls_buffer="Anno Commessa:" + char(27) + "G" + char(27) + "E" + ls_anno_commessa + char(27) + "F" + char(27) + "H" + " Num.Commessa:" + char(27) + "G" + char(27) + "E" + ls_num_commessa + char(27) + "F" + char(27) + "H" + " Progressivo:" + char(27) + "G" + char(27) + "E" + ls_prog_riga + char(27) + "F" + char(27) + "H" +char(13) + char(10)
	printsend(ll_job,ls_buffer)	

	ls_buffer="Prodotto Finito:" + char(27) + "G" + char(27) + "E" + ls_cod_prodotto_finito + "  " + ls_des_prodotto_finito + char(27) + "F" + char(27) + "H" + char(13) + char(10)
	printsend(ll_job,ls_buffer)	

	ls_buffer= char(13) + char(10)
	printsend(ll_job,ls_buffer)	

	ls_buffer="Semilavorato di Fase:" + char(27) + "G" + char(27) + "E" + ls_cod_prodotto + char(27) + "F" + char(27) + "H" + " Quantita' da produrre:" + char(27) + "G" + char(27) + "E" + ls_quan_in_produzione + char(27) + "F" + char(27) + "H" + char(13) + char(10)
	printsend(ll_job,ls_buffer)	

	ls_buffer = "Reparto:" + ls_cod_reparto+ " Lavorazione:" + ls_cod_lavorazione + char(13) + char(10)
	printsend(ll_job,ls_buffer)	

	ls_buffer= char(13) + char(10)
	printsend(ll_job,ls_buffer)	

	ls_buffer = "Materie Prime o Semilavorati da utilizzare:" + char(13) + char(10)
	printsend(ll_job,ls_buffer)	

	ls_cod_prodotto_1 = trim(ls_cod_prodotto)

	declare righe_db cursor for
	select  cod_prodotto_figlio
	from    distinta
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     cod_prodotto_padre=:ls_cod_prodotto_1;

	open righe_db;
	
	do while 1=1
		fetch righe_db
		into  :ls_cod_materia_prima;

		if sqlca.sqlcode = 100 then exit
		
		select des_prodotto
		into   :ls_des_materia_prima
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_materia_prima;

		ls_buffer = "Cod.:" + ls_cod_materia_prima + " Des.:" + ls_des_materia_prima + char(13) + char(10)
		printsend(ll_job,ls_buffer)		
			
	loop

	close righe_db;

	ls_buffer= char(13) + char(10)
	printsend(ll_job,ls_buffer)	

	ls_buffer = "Descrizione Operativa:" + char(13) + char(10)
	printsend(ll_job,ls_buffer)		

	ls_buffer = left(ls_note_fase,70) + char(13) + char(10)
	printsend(ll_job,ls_buffer)		

	ls_buffer = mid(ls_note_fase,71,70) + char(13) + char(10)
	printsend(ll_job,ls_buffer)		

	ls_buffer = mid(ls_note_fase,141,70) + char(13) + char(10)
	printsend(ll_job,ls_buffer)		

	ls_buffer = mid(ls_note_fase,211,70) + char(13) + char(10)
	printsend(ll_job,ls_buffer)		

	ls_buffer = mid(ls_note_fase,281,70) + char(13) + char(10)
	printsend(ll_job,ls_buffer)		

	ls_buffer = mid(ls_note_fase,351,70) + char(13) + char(10)
	printsend(ll_job,ls_buffer)		

	ls_buffer = mid(ls_note_fase,421,70) + char(13) + char(10)
	printsend(ll_job,ls_buffer)		

	ls_buffer = char(13) + char(10) + char(10)
	printsend(ll_job,ls_buffer)		

	printsend(ll_job,"A  ")
	printsend(ll_job,char(27) + char(20) + char(20))
	printsend(ll_job,"R4")
	printsend(ll_job,char(1) + char(1440) + char(1))
	printsend(ll_job,ls_anno_commessa + ls_num_commessa + ls_prog_riga)
	printsend(ll_job,"    B")
	printsend(ll_job,char(10) + char(10)+ char(10)+ char(10)+ char(10) +char(10)+ char(10))

	printsend(ll_job,"A  ")
	printsend(ll_job,char(27) + char(20) + char(21))
	printsend(ll_job,"R4")
	printsend(ll_job,char(1) + char(1440) + char(1))
	printsend(ll_job,ls_cod_prodotto)
	printsend(ll_job,"    B")
	printsend(ll_job,char(10) + char(10)+ char(10)+ char(10)+ char(10) +char(10)+ char(10))

	printsend(ll_job,"A  ")
	printsend(ll_job,char(27) + char(20) + char(18))
	printsend(ll_job,"R4")
	printsend(ll_job,char(1) + char(1440) + char(1))
	printsend(ll_job,ls_cod_reparto + ls_cod_lavorazione)
	printsend(ll_job,"    B")
	printsend(ll_job,char(10) + char(10)+ char(10)+ char(10)+ char(10) +char(10)+ char(10))

	printsend(ll_job,char(12))

loop

close righe_avanzamento_pc_1;	

printclose(ll_job)
end event

type cb_elimina_det_orari from commandbutton within w_avanzamento_produzione_commesse
integer x = 3086
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elim. D.O."
end type

event clicked;integer li_anno_commessa,li_risposta
long    ll_num_commessa,ll_prog_riga,ll_t,ll_prog
string  ls_test,ls_cod_prodotto,ls_cod_prodotto_finito,ls_cod_reparto,ls_cod_lavorazione, & 
		  ls_cod_versione,ls_cod_prodotto_2[]
double  ldd_quan_in_produzione,ldd_quan_in_produzione_copia,ldd_quan_utilizzo[]

li_anno_commessa = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "num_commessa")
ll_prog_riga = dw_avanzamento_produzione_commesse_det.getitemnumber(dw_avanzamento_produzione_commesse_det.getrow(), "prog_riga")

select cod_azienda
into   :ls_test
from   det_bol_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	return
end if

if sqlca.sqlcode <> 100 then
	g_mb.messagebox("Sep","Attenzione esiste già una bolla di vendita legata alla commessa corrente, pertanto non è più possibile eliminare o modificare alcuna parte di essa.",stopsign!)
	return
end if

select cod_prodotto,
		 cod_versione
into   :ls_cod_prodotto_finito,	
		 :ls_cod_versione
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	return
end if

if g_mb.messagebox("Sep","Attenzione, i dettagli orari di produzione stanno per essere cancellati, sei sicuro?",question!,yesno!) = 1 then
	
	setpointer(hourglass!)

	select cod_azienda
	into   :ls_test
	from   det_orari_produzione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga;
	
	if sqlca.sqlcode=100 then
		g_mb.messagebox("Sep","Non esistono dettagli orari di produzione per questa riga di commessa.",information!)
		return
	end if

	select cod_azienda
	into   :ls_test
	from   avan_produzione_com
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga
	and    flag_fine_fase='N';

	if sqlca.sqlcode=100 then
		g_mb.messagebox("Sep","Tutte le fasi sono state chiuse pertanto non è possibile eliminare i dettagli orari produzione.",information!)
		return
	end if

	select quan_in_produzione
	into   :ldd_quan_in_produzione_copia
	from   det_anag_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga;

	declare righe_mc_1 cursor for
	select  cod_prodotto
	from    mat_prime_commessa
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     anno_commessa=:li_anno_commessa
	and     num_commessa=:ll_num_commessa
	and     prog_riga=:ll_prog_riga;

	open righe_mc_1;
	
	do while 1=1
		fetch righe_mc_1
		into  :ls_cod_prodotto;

		choose case sqlca.sqlcode 
			case is < 0
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,exclamation!)
			
			case 100
				exit
		end choose

		ll_prog = 0
		li_risposta = f_calcola_quan_utilizzo_2(ls_cod_prodotto_finito,ls_cod_versione,ls_cod_prodotto_2[], ldd_quan_utilizzo[],ll_prog,ldd_quan_in_produzione,li_anno_commessa,ll_num_commessa)

		for ll_t =1 to upperbound(ls_cod_prodotto_2)
	     if ls_cod_prodotto_2[ll_t]= ls_cod_prodotto then
	       ldd_quan_in_produzione = ldd_quan_utilizzo[ll_t]
	     end if
	   next

		ldd_quan_in_produzione = ldd_quan_in_produzione * ldd_quan_in_produzione_copia
		
		update  mat_prime_commessa
		set     quan_in_produzione=:ldd_quan_in_produzione,
				  quan_utilizzata=0,
				  quan_reso=0,
				  quan_scarto=0,
			     quan_sfrido=0		
		where   cod_azienda=:s_cs_xx.cod_azienda
		and     anno_commessa=:li_anno_commessa
		and     num_commessa=:ll_num_commessa
		and     prog_riga=:ll_prog_riga
		and     cod_prodotto=:ls_cod_prodotto;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Attenzione! Errore sul DB: "+ sqlca.sqlerrtext,exclamation!)
			rollback;
			return
		end if

	loop

	close righe_mc_1;

	declare righe_ap_1 cursor for
	select  cod_prodotto,
			  cod_reparto,
			  cod_lavorazione
	from    avan_produzione_com
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     anno_commessa=:li_anno_commessa
	and     num_commessa=:ll_num_commessa
	and     prog_riga=:ll_prog_riga;

	open righe_ap_1;
	
	do while 1=1
		fetch righe_ap_1
		into  :ls_cod_prodotto,
				:ls_cod_reparto,
				:ls_cod_lavorazione;

		choose case sqlca.sqlcode 
			case is < 0
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,exclamation!)
			
			case 100
				exit
		end choose
		
		ll_prog = 0
		li_risposta = f_calcola_quan_utilizzo_2(ls_cod_prodotto_finito,ls_cod_versione,ls_cod_prodotto_2[], ldd_quan_utilizzo[],ll_prog,ldd_quan_in_produzione,li_anno_commessa,ll_num_commessa)

		for ll_t =1 to upperbound(ls_cod_prodotto_2)
	     if ls_cod_prodotto_2[ll_t]= ls_cod_prodotto_finito then
	       ldd_quan_in_produzione = ldd_quan_utilizzo[ll_t]
	     end if
	   next

		ldd_quan_in_produzione = ldd_quan_in_produzione * ldd_quan_in_produzione_copia

		update  avan_produzione_com
		set     quan_in_produzione=:ldd_quan_in_produzione,
				  quan_prodotta=0,
				  quan_utilizzata=0,
				  quan_reso=0,
				  quan_sfrido=0,
				  quan_scarto=0,
				  tempo_attrezzaggio=0,
				  tempo_attrezzaggio_commessa=0,
				  tempo_lavorazione=0,
				  tempo_risorsa_umana=0,
				  flag_fine_fase='N'
		where   cod_azienda=:s_cs_xx.cod_azienda
		and     anno_commessa=:li_anno_commessa
		and     num_commessa=:ll_num_commessa
		and     prog_riga=:ll_prog_riga
		and     cod_prodotto=:ls_cod_prodotto
		and     cod_reparto=:ls_cod_reparto
		and    cod_lavorazione=:ls_cod_lavorazione;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Attenzione! Errore sul DB: "+ sqlca.sqlerrtext,exclamation!)
			rollback;
			return
		end if

	loop

	close righe_ap_1;

	delete from det_orari_produzione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga;

	if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Attenzione! Errore sul DB: "+ sqlca.sqlerrtext,exclamation!)
			rollback;
			return
	end if

	commit;

	setpointer(arrow!)
	dw_avanzamento_produzione_commesse_lista.triggerevent("pcd_search")

end if
end event

