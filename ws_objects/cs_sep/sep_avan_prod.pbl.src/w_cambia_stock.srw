﻿$PBExportHeader$w_cambia_stock.srw
$PBExportComments$Window che permette di cambiare gli stock assegnati al momento della rilevazione produzione
forward
global type w_cambia_stock from w_cs_xx_risposta
end type
type dw_stock_origine from uo_cs_xx_dw within w_cambia_stock
end type
type st_1 from statictext within w_cambia_stock
end type
type dw_stock_destinazione from uo_cs_xx_dw within w_cambia_stock
end type
type st_2 from statictext within w_cambia_stock
end type
type st_3 from statictext within w_cambia_stock
end type
type st_vecchia_quantita from statictext within w_cambia_stock
end type
type st_nuova_quantita from statictext within w_cambia_stock
end type
type st_4 from statictext within w_cambia_stock
end type
type st_5 from statictext within w_cambia_stock
end type
type st_codice_descrizione from statictext within w_cambia_stock
end type
type cb_annulla from commandbutton within w_cambia_stock
end type
type cb_conferma from commandbutton within w_cambia_stock
end type
end forward

global type w_cambia_stock from w_cs_xx_risposta
integer width = 3419
integer height = 1620
string title = "Cambia Stock"
boolean controlmenu = false
dw_stock_origine dw_stock_origine
st_1 st_1
dw_stock_destinazione dw_stock_destinazione
st_2 st_2
st_3 st_3
st_vecchia_quantita st_vecchia_quantita
st_nuova_quantita st_nuova_quantita
st_4 st_4
st_5 st_5
st_codice_descrizione st_codice_descrizione
cb_annulla cb_annulla
cb_conferma cb_conferma
end type
global w_cambia_stock w_cambia_stock

event pc_setwindow;call super::pc_setwindow;string ls_cod_prodotto,ls_des_prodotto

dw_stock_origine.set_dw_options(sqlca, pcca.null_object, &
		                                 c_nomodify + c_nonew + c_nodelete, &
      		                           c_default)


dw_stock_destinazione.set_dw_options(sqlca, pcca.null_object, &
		                                 c_modifyonopen + c_nonew + c_nodelete, &
      		                           c_default)


ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto;

st_vecchia_quantita.text = string(s_cs_xx.parametri.parametro_d_10)
st_nuova_quantita.text = string(s_cs_xx.parametri.parametro_d_11)

st_codice_descrizione.text = ls_cod_prodotto + " " + ls_des_prodotto

end event

on w_cambia_stock.create
int iCurrent
call super::create
this.dw_stock_origine=create dw_stock_origine
this.st_1=create st_1
this.dw_stock_destinazione=create dw_stock_destinazione
this.st_2=create st_2
this.st_3=create st_3
this.st_vecchia_quantita=create st_vecchia_quantita
this.st_nuova_quantita=create st_nuova_quantita
this.st_4=create st_4
this.st_5=create st_5
this.st_codice_descrizione=create st_codice_descrizione
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_stock_origine
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_stock_destinazione
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.st_3
this.Control[iCurrent+6]=this.st_vecchia_quantita
this.Control[iCurrent+7]=this.st_nuova_quantita
this.Control[iCurrent+8]=this.st_4
this.Control[iCurrent+9]=this.st_5
this.Control[iCurrent+10]=this.st_codice_descrizione
this.Control[iCurrent+11]=this.cb_annulla
this.Control[iCurrent+12]=this.cb_conferma
end on

on w_cambia_stock.destroy
call super::destroy
destroy(this.dw_stock_origine)
destroy(this.st_1)
destroy(this.dw_stock_destinazione)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_vecchia_quantita)
destroy(this.st_nuova_quantita)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_codice_descrizione)
destroy(this.cb_annulla)
destroy(this.cb_conferma)
end on

event close;call super::close;dw_stock_destinazione.Reset_DW_Modified(c_ResetChildren)
end event

type dw_stock_origine from uo_cs_xx_dw within w_cambia_stock
integer x = 23
integer y = 200
integer width = 2263
integer height = 480
integer taborder = 10
string dataobject = "d_stock_origine"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG    l_Error,ll_num_commessa,ll_prog_riga
integer li_anno_commessa
string ls_cod_prodotto

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2

l_Error = Retrieve(s_cs_xx.cod_azienda,li_anno_commessa,ll_num_commessa,ll_prog_riga,ls_cod_prodotto)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type st_1 from statictext within w_cambia_stock
integer x = 23
integer y = 120
integer width = 366
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Stock Origine:"
boolean focusrectangle = false
end type

type dw_stock_destinazione from uo_cs_xx_dw within w_cambia_stock
integer x = 23
integer y = 780
integer width = 3337
integer height = 620
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_stock_destinazione"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG    l_Error,ll_num_commessa,ll_prog_riga
integer li_anno_commessa
string  ls_cod_prodotto

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_prodotto)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type st_2 from statictext within w_cambia_stock
integer x = 23
integer y = 700
integer width = 526
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Stock Destinazione:"
boolean focusrectangle = false
end type

type st_3 from statictext within w_cambia_stock
integer x = 2331
integer y = 200
integer width = 869
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Quantità disponibile precedente:"
boolean focusrectangle = false
end type

type st_vecchia_quantita from statictext within w_cambia_stock
integer x = 2560
integer y = 280
integer width = 411
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0000"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_nuova_quantita from statictext within w_cambia_stock
integer x = 2560
integer y = 500
integer width = 411
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "0000"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_4 from statictext within w_cambia_stock
integer x = 2331
integer y = 420
integer width = 914
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Nuova quantità totale da prelevare:"
boolean focusrectangle = false
end type

type st_5 from statictext within w_cambia_stock
integer x = 23
integer y = 20
integer width = 297
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Prodotto:"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_codice_descrizione from statictext within w_cambia_stock
integer x = 343
integer y = 20
integer width = 2994
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "none"
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_cambia_stock
integer x = 2606
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_stock_destinazione.Reset_DW_Modified(c_ResetChildren)
close(parent)
end event

type cb_conferma from commandbutton within w_cambia_stock
integer x = 2994
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 22
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;integer  li_anno_commessa,li_risposta
long     ll_num_commessa,ll_prog_riga,ll_t,ll_prog_stock,ll_prog_sto[],ll_anno_reg_des_mov, & 
		   ll_num_reg_des_mov,ll_anno_registrazione[],ll_num_registrazione[]
double   ldd_totale,ldd_giacenza_stock,ldd_quan_assegnata,ldd_quan_in_spedizione, & 
		   ldd_quan_prelevare
string   ls_cod_prodotto,ls_errore,ls_messaggio,ls_cod_deposito,ls_cod_ubicazione,ls_cod_lotto, &
			ls_cod_dep[],ls_cod_ubic[], ls_cod_lo[],ls_cod_cliente[], ls_cod_forn[], & 
			ls_cod_tipo_movimento,ls_cod_tipo_commessa
datetime ldt_data_stock,ldt_data_sto[]
uo_magazzino luo_mag


setnull(ls_cod_ubic[1])
setnull(ls_cod_lo[1])
setnull(ldt_data_sto[1])
setnull(ll_prog_sto[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_forn[1])

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

select cod_tipo_mov_prel_mat_prime
into   :ls_cod_tipo_movimento
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:ls_cod_tipo_commessa;

dw_stock_destinazione.accepttext()
dw_stock_destinazione.Reset_DW_Modified(c_ResetChildren)
ldd_totale=dw_stock_destinazione.getitemnumber(dw_stock_destinazione.getrow(),"totale")

if ldd_totale = double(st_nuova_quantita.text) then
	if g_mb.messagebox("Sep","Confermi il cambiamento?",question!,yesno!) = 1 then
		setpointer(hourglass!)
		//ciclo di verifica quantità stock destinazione
		for ll_t = 1 to dw_stock_destinazione.rowcount()
			ldd_giacenza_stock = dw_stock_destinazione.getitemnumber(ll_t,"giacenza_stock")
			ldd_quan_assegnata = dw_stock_destinazione.getitemnumber(ll_t,"quan_assegnata")
			ldd_quan_in_spedizione = dw_stock_destinazione.getitemnumber(ll_t,"quan_in_spedizione")
			ldd_quan_prelevare =	dw_stock_destinazione.getitemnumber(ll_t,"quan_prelevare")
			
			if ldd_giacenza_stock - ldd_quan_assegnata - ldd_quan_in_spedizione < ldd_quan_prelevare then
				if	g_mb.messagebox("Sep","Attenzione! La quantità inserita supera la disponibilità giacente. Vuoi liberare le quantità associate alle commesse non ancora in produzione?", question!,yesno!) = 1 then
					
					ls_cod_deposito = dw_stock_destinazione.getitemstring(ll_t,"cod_deposito")
					ls_cod_ubicazione = dw_stock_destinazione.getitemstring(ll_t,"cod_ubicazione")
					ls_cod_lotto = dw_stock_destinazione.getitemstring(ll_t,"cod_lotto")
					ldt_data_stock = dw_stock_destinazione.getitemdatetime(ll_t,"data_stock")
					ll_prog_stock = dw_stock_destinazione.getitemnumber(ll_t,"prog_stock")

					// chiamata alla funzione che libera le quantità degli stock associate a qualche commessa			
					li_risposta = f_libera_stock (ls_cod_prodotto, ls_cod_deposito, & 
														   ls_cod_ubicazione, ls_cod_lotto,ldt_data_stock, & 
															ll_prog_stock, li_anno_commessa, &
                                             ll_num_commessa,&
														   ls_messaggio, ls_errore )

					if li_risposta = -1 then
						g_mb.messagebox("Sep",ls_errore,exclamation!)
						return
					else
						g_mb.messagebox("Sep",ls_messaggio,information!)
					end if

				else
					g_mb.messagebox("Sep","Cambiare le quantità in modo che non superino la disponibilità e poi confermare.",information!)
					return					
				end if
			end if

		next	
		
		//ciclo che permette di liberare le quantità associate allo stock di origine
		for ll_t = 1 to dw_stock_origine.rowcount()
			ls_cod_deposito = dw_stock_origine.getitemstring(ll_t,"cod_deposito")
			ls_cod_ubicazione = dw_stock_origine.getitemstring(ll_t,"cod_ubicazione")
			ls_cod_lotto = dw_stock_origine.getitemstring(ll_t,"cod_lotto")
			ldt_data_stock = dw_stock_origine.getitemdatetime(ll_t,"data_stock")
			ll_prog_stock = dw_stock_origine.getitemnumber(ll_t,"prog_stock")
			ll_anno_reg_des_mov = dw_stock_origine.getitemnumber(ll_t,"anno_reg_des_mov")
			ll_num_reg_des_mov = dw_stock_origine.getitemnumber(ll_t,"num_reg_des_mov")
			ll_anno_registrazione[1] = dw_stock_origine.getitemnumber(ll_t,"anno_registrazione")
			ll_num_registrazione[1] = dw_stock_origine.getitemnumber(ll_t,"num_registrazione")			

			li_risposta = f_elimina_dest_mov_mag ( ll_anno_reg_des_mov, ll_num_reg_des_mov )

		   if li_risposta = -1 then
		 		g_mb.messagebox("Sep", "Si è verificato un errore in fase di cancellazione destinazioni movimenti.", &
							  exclamation!, ok!)
				return
		   end if

			delete from   mat_prime_commessa_stock
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:li_anno_commessa
			and    num_commessa=:ll_num_commessa
			and    prog_riga=:ll_prog_riga
			and    cod_prodotto=:ls_cod_prodotto
			and    cod_deposito=:ls_cod_deposito
			and    cod_ubicazione=:ls_cod_ubicazione
			and    cod_lotto=:ls_cod_lotto
			and    data_stock=:ldt_data_stock
			and    prog_stock=:ll_prog_stock;
			
			luo_mag = create uo_magazzino
  			li_risposta = luo_mag.uof_elimina_movimenti(ll_anno_registrazione[1], ll_num_registrazione[1],true)
  			destroy luo_mag

		   if li_risposta = -1 then
		 		g_mb.messagebox("Sep", "Si è verificato un errore in fase di cancellazione movimenti.", &
							  exclamation!, ok!)
				return
		   end if
		

		next
		
		// Ciclo di creazione nuovi record in mat_prime_commessa_stock
		for ll_t = 1 to dw_stock_destinazione.rowcount()
			ldd_quan_prelevare =	dw_stock_destinazione.getitemnumber(ll_t,"quan_prelevare")					
			ls_cod_dep[1] = dw_stock_destinazione.getitemstring(ll_t,"cod_deposito")
			ls_cod_ubic[1] = dw_stock_destinazione.getitemstring(ll_t,"cod_ubicazione")
			ls_cod_lo[1] = dw_stock_destinazione.getitemstring(ll_t,"cod_lotto")
			ldt_data_sto[1] = dw_stock_destinazione.getitemdatetime(ll_t,"data_stock")
			ll_prog_sto[1] = dw_stock_destinazione.getitemnumber(ll_t,"prog_stock")
			
			if ldd_quan_prelevare > 0 then
			
				if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_dep[], & 
													  ls_cod_ubic[], ls_cod_lo[], ldt_data_sto[], & 
													  ll_prog_sto[], ls_cod_cliente[], ls_cod_forn[], &
													  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
	
				  g_mb.messagebox("Sep","Si è verificato un errore in creazione movimenti",Exclamation!)
				  return 
				end if
	
				if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
													 ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
	
  				   g_mb.messagebox("Sep","Si è verificato un errore in fase di verifica destinazioni movimenti magazzino",Exclamation!)
					return 
				end if
				
				luo_mag = create uo_magazzino
		
			   li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
	 						 			 				ls_cod_tipo_movimento, &
						  							   "N", &
														 ls_cod_prodotto, &
														 ldd_quan_prelevare, &
														 1, &
														 ll_num_commessa, &
														 datetime(today(),time('00:00:00')), &
														 "", &
														 ll_anno_reg_des_mov, &
														 ll_num_reg_des_mov, &
														 ls_cod_dep[], &
														 ls_cod_ubic[], &
														 ls_cod_lo[], &
														 ldt_data_sto[], &
														 ll_prog_sto[], &
														 ls_cod_forn[], &
														 ls_cod_cliente[], &
														 ll_anno_registrazione[], &
														 ll_num_registrazione[])
														 
				destroy luo_mag

				if li_risposta=-1 then
					g_mb.messagebox("Sep","Errore su movimenti magazzino.",stopsign!)
					return 
				end if

			  INSERT INTO mat_prime_commessa_stock
      	  			   ( cod_azienda,   
				           anno_commessa,   
				           num_commessa,   
				           prog_riga,   
				           cod_prodotto,   
				           cod_deposito,   
				           cod_ubicazione,   
				           cod_lotto,   
				           data_stock,   
				           prog_stock,   
				           anno_registrazione,   
				           num_registrazione,   
				           quan_assegnata,   
				           cod_tipo_movimento,   
				           anno_reg_des_mov,   
				           num_reg_des_mov )  
				  VALUES ( :s_cs_xx.cod_azienda,   
				           :li_anno_commessa,   
					        :ll_num_commessa,   
				           :ll_prog_riga,   
				           :ls_cod_prodotto,   
				           :ls_cod_dep[1],   
				           :ls_cod_ubic[1],   
				           :ls_cod_lo[1],   
				           :ldt_data_sto[1],   
				           :ll_prog_sto[1],   
				           :ll_anno_registrazione[1],   
				           :ll_num_registrazione[1],   
				           0,   
				           :ls_cod_tipo_movimento,   
				           :ll_anno_reg_des_mov,   
				           :ll_num_reg_des_mov )  ;

			   if sqlca.sqlcode<>0 then 
				  g_mb.messagebox("Sep","Si è verificato un errore sul DB:"+ sqlca.sqlerrtext,Exclamation!)
				  return 
			   end if

			end if
		next	

		//aggiornamento valore complessivo collegato
		update mat_prime_commessa
		set    quan_in_produzione=:ldd_totale
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga
		and    cod_prodotto=:ls_cod_prodotto;


		s_cs_xx.parametri.parametro_b_2=true
		setpointer(arrow!)
		close(parent)	
		
	else
		return
	end if
else
	g_mb.messagebox("Sep","Attenzione! La quantità inserita è diversa dalla nuova quantità assegnata. Verificare le quantità da prelevare dal lotto o dai singoli lotti.",Exclamation!)
	return
end if
end event

