﻿$PBExportHeader$w_commesse.srw
$PBExportComments$Anagrafica Commesse
forward
global type w_commesse from w_cs_xx_principale
end type
type dw_commesse_lista from uo_cs_xx_dw within w_commesse
end type
type cb_dettaglio from commandbutton within w_commesse
end type
type cb_assegna from commandbutton within w_commesse
end type
type cb_reset_assegna from commandbutton within w_commesse
end type
type em_anno_commessa from editmask within w_commesse
end type
type cb_aggiorna from commandbutton within w_commesse
end type
type cb_1 from cb_prod_ricerca within w_commesse
end type
type cbx_forza_numerazione from checkbox within w_commesse
end type
type sle_num_commessa from singlelineedit within w_commesse
end type
type cb_varianti from commandbutton within w_commesse
end type
type dw_commesse_1 from uo_cs_xx_dw within w_commesse
end type
type dw_commesse_2 from uo_cs_xx_dw within w_commesse
end type
type dw_folder from u_folder within w_commesse
end type
type cb_chiudi_commessa from commandbutton within w_commesse
end type
type gb_flag_tipo_avanzamento from groupbox within w_commesse
end type
type cbx_assegnazione from checkbox within w_commesse
end type
type cbx_attivazione from checkbox within w_commesse
end type
type cbx_automatico from checkbox within w_commesse
end type
type cbx_chiusa_assegn from checkbox within w_commesse
end type
type cbx_chiusa_attiv from checkbox within w_commesse
end type
type cbx_chiusa_autom from checkbox within w_commesse
end type
type cbx_nessuna from checkbox within w_commesse
end type
type st_num_commesse from statictext within w_commesse
end type
type cb_tutti from commandbutton within w_commesse
end type
type st_anno from statictext within w_commesse
end type
end forward

global type w_commesse from w_cs_xx_principale
integer width = 3209
integer height = 1744
string title = "Commesse (Assegnazione)"
dw_commesse_lista dw_commesse_lista
cb_dettaglio cb_dettaglio
cb_assegna cb_assegna
cb_reset_assegna cb_reset_assegna
em_anno_commessa em_anno_commessa
cb_aggiorna cb_aggiorna
cb_1 cb_1
cbx_forza_numerazione cbx_forza_numerazione
sle_num_commessa sle_num_commessa
cb_varianti cb_varianti
dw_commesse_1 dw_commesse_1
dw_commesse_2 dw_commesse_2
dw_folder dw_folder
cb_chiudi_commessa cb_chiudi_commessa
gb_flag_tipo_avanzamento gb_flag_tipo_avanzamento
cbx_assegnazione cbx_assegnazione
cbx_attivazione cbx_attivazione
cbx_automatico cbx_automatico
cbx_chiusa_assegn cbx_chiusa_assegn
cbx_chiusa_attiv cbx_chiusa_attiv
cbx_chiusa_autom cbx_chiusa_autom
cbx_nessuna cbx_nessuna
st_num_commesse st_num_commesse
cb_tutti cb_tutti
st_anno st_anno
end type
global w_commesse w_commesse

type variables
boolean ib_in_new=FALSE
uo_magazzino iuo_magazzino
end variables

forward prototypes
public function integer wf_imposta_bottoni ()
end prototypes

public function integer wf_imposta_bottoni ();//double  ldd_quan_assegnata,ldd_quan_in_produzione,ldd_quan_prodotta,ldd_quan_ordine
//long    ll_anno_commessa,ll_num_commessa
//
//cb_assegna.enabled=false
//cb_dettaglio.enabled=false
//cb_reset_assegna.enabled=false
//
//if dw_commesse_1.rowcount()=0 then
//	return 0
//end if
//
//ldd_quan_assegnata = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"quan_assegnata")
//ldd_quan_in_produzione = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"quan_in_produzione")
//ldd_quan_prodotta = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"quan_prodotta")
//ldd_quan_ordine = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"quan_ordine")
//ll_anno_commessa = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"anno_commessa")
//ll_num_commessa = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"num_commessa")
//
//if ldd_quan_assegnata + ldd_quan_in_produzione + ldd_quan_prodotta < ldd_quan_ordine then
//	cb_assegna.enabled=true
//end if
//
//if ldd_quan_assegnata > 0 and ldd_quan_in_produzione = 0 and ldd_quan_prodotta = 0 then
//	cb_reset_assegna.enabled=true
//end if
//
//if ldd_quan_in_produzione>0 or ldd_quan_assegnata>0 or ldd_quan_prodotta>0  then
//	cb_dettaglio.enabled=true
//end if
//
//return 0
long ll_num_det, ll_anno_commessa, ll_num_commessa
string  ls_flag_tipo_avanzamento

if dw_commesse_1.rowcount()=0 then
	cb_reset_assegna.enabled = false
	cb_assegna.enabled = false
	cb_chiudi_commessa.enabled = false
	cb_dettaglio.enabled = false
	return 0
end if
ls_flag_tipo_avanzamento = dw_commesse_1.getitemstring(dw_commesse_1.getrow(),"flag_tipo_avanzamento")

choose case ls_flag_tipo_avanzamento
	case "1" // Avanzamento Assegnazione
		cb_reset_assegna.enabled = true
		cb_assegna.enabled = true
		cb_chiudi_commessa.enabled = false
		cb_dettaglio.enabled = true
	case "2" // Avanzamento Attivazione
		cb_reset_assegna.enabled = false
		cb_assegna.enabled = false
		cb_chiudi_commessa.enabled = false
		cb_dettaglio.enabled = false
	case "3" // Avanzamento Automatico
		cb_reset_assegna.enabled = false
		cb_assegna.enabled = false
		cb_chiudi_commessa.enabled = true
		ll_anno_commessa = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"anno_commessa")
		ll_num_commessa = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"num_commessa")
		
		select count(*)
		 into :ll_num_det
		 from det_anag_commesse
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :ll_anno_commessa and
				num_commessa = :ll_num_commessa;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("assegnazione Commesse", "Errore nell'Estrazione Dettagli Commesse")
			return 0
		end if
		if ll_num_det > 0 then
			cb_dettaglio.enabled = true
		else
			cb_dettaglio.enabled = false
		end if
	case "9" // Chiusura Commessa Assegnazione
		cb_reset_assegna.enabled = true
		cb_assegna.enabled = true
		cb_chiudi_commessa.enabled = true
		cb_dettaglio.enabled = true
	case "8" // Chiusura Commessa Attivazione
		cb_reset_assegna.enabled = false
		cb_assegna.enabled = false
		cb_chiudi_commessa.enabled = true
		cb_dettaglio.enabled = false
	case "7" // Chiusura Commessa Automatica
		cb_reset_assegna.enabled = false
		cb_assegna.enabled = false
		cb_chiudi_commessa.enabled = true

		ll_anno_commessa = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"anno_commessa")
		ll_num_commessa = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"num_commessa")
		
		select count(*)
		 into :ll_num_det
		 from det_anag_commesse
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :ll_anno_commessa and
				num_commessa = :ll_num_commessa;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("assegnazione Commesse", "Errore nell'Estrazione Dettagli Commesse")
			return 0
		end if
		if ll_num_det > 0 then
			cb_dettaglio.enabled = true
		else
			cb_dettaglio.enabled = false
		end if
	case else // Nessun Avanzamento
		cb_reset_assegna.enabled = false
		cb_assegna.enabled = true
		cb_chiudi_commessa.enabled = true
		cb_dettaglio.enabled = false
end choose
return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_commesse_1
l_objects[2] = cb_1
dw_folder.fu_AssignTab(1, "&Generici", l_Objects[])
l_objects[1] = dw_commesse_2
dw_folder.fu_AssignTab(2, "&Note", l_Objects[])

dw_folder.fu_FolderCreate(2,4)

dw_commesse_lista.set_dw_key("cod_azienda")
dw_commesse_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_commesse_1.set_dw_options(sqlca,dw_commesse_lista,c_sharedata+c_scrollparent,c_default)
dw_commesse_2.set_dw_options(sqlca,dw_commesse_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_commesse_lista

dw_folder.fu_SelectTab(1)
em_anno_commessa.text=string(year(today()))
end event

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW(dw_commesse_1,"cod_operatore",sqlca,&
                 "tab_operatori","cod_operatore","des_operatore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_commesse_1,"cod_tipo_commessa",sqlca,&
                 "tab_tipi_commessa","cod_tipo_commessa","des_tipo_commessa",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_commesse_1,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in (select cod_prodotto from distinta_padri where cod_azienda='" + s_cs_xx.cod_azienda + "' and flag_blocco='N')")

f_PO_LoadDDDW_DW(dw_commesse_1,"cod_deposito_prelievo",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N'")

f_PO_LoadDDDW_DW(dw_commesse_1,"cod_deposito_versamento",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N'")


//f_PO_LoadDDDW_DW(dw_commesse_1,"cod_versione",sqlca,&
//   	              	  "distinta_padri","cod_versione","des_versione",&
//	      	           "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_commesse.create
int iCurrent
call super::create
this.dw_commesse_lista=create dw_commesse_lista
this.cb_dettaglio=create cb_dettaglio
this.cb_assegna=create cb_assegna
this.cb_reset_assegna=create cb_reset_assegna
this.em_anno_commessa=create em_anno_commessa
this.cb_aggiorna=create cb_aggiorna
this.cb_1=create cb_1
this.cbx_forza_numerazione=create cbx_forza_numerazione
this.sle_num_commessa=create sle_num_commessa
this.cb_varianti=create cb_varianti
this.dw_commesse_1=create dw_commesse_1
this.dw_commesse_2=create dw_commesse_2
this.dw_folder=create dw_folder
this.cb_chiudi_commessa=create cb_chiudi_commessa
this.gb_flag_tipo_avanzamento=create gb_flag_tipo_avanzamento
this.cbx_assegnazione=create cbx_assegnazione
this.cbx_attivazione=create cbx_attivazione
this.cbx_automatico=create cbx_automatico
this.cbx_chiusa_assegn=create cbx_chiusa_assegn
this.cbx_chiusa_attiv=create cbx_chiusa_attiv
this.cbx_chiusa_autom=create cbx_chiusa_autom
this.cbx_nessuna=create cbx_nessuna
this.st_num_commesse=create st_num_commesse
this.cb_tutti=create cb_tutti
this.st_anno=create st_anno
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_commesse_lista
this.Control[iCurrent+2]=this.cb_dettaglio
this.Control[iCurrent+3]=this.cb_assegna
this.Control[iCurrent+4]=this.cb_reset_assegna
this.Control[iCurrent+5]=this.em_anno_commessa
this.Control[iCurrent+6]=this.cb_aggiorna
this.Control[iCurrent+7]=this.cb_1
this.Control[iCurrent+8]=this.cbx_forza_numerazione
this.Control[iCurrent+9]=this.sle_num_commessa
this.Control[iCurrent+10]=this.cb_varianti
this.Control[iCurrent+11]=this.dw_commesse_1
this.Control[iCurrent+12]=this.dw_commesse_2
this.Control[iCurrent+13]=this.dw_folder
this.Control[iCurrent+14]=this.cb_chiudi_commessa
this.Control[iCurrent+15]=this.gb_flag_tipo_avanzamento
this.Control[iCurrent+16]=this.cbx_assegnazione
this.Control[iCurrent+17]=this.cbx_attivazione
this.Control[iCurrent+18]=this.cbx_automatico
this.Control[iCurrent+19]=this.cbx_chiusa_assegn
this.Control[iCurrent+20]=this.cbx_chiusa_attiv
this.Control[iCurrent+21]=this.cbx_chiusa_autom
this.Control[iCurrent+22]=this.cbx_nessuna
this.Control[iCurrent+23]=this.st_num_commesse
this.Control[iCurrent+24]=this.cb_tutti
this.Control[iCurrent+25]=this.st_anno
end on

on w_commesse.destroy
call super::destroy
destroy(this.dw_commesse_lista)
destroy(this.cb_dettaglio)
destroy(this.cb_assegna)
destroy(this.cb_reset_assegna)
destroy(this.em_anno_commessa)
destroy(this.cb_aggiorna)
destroy(this.cb_1)
destroy(this.cbx_forza_numerazione)
destroy(this.sle_num_commessa)
destroy(this.cb_varianti)
destroy(this.dw_commesse_1)
destroy(this.dw_commesse_2)
destroy(this.dw_folder)
destroy(this.cb_chiudi_commessa)
destroy(this.gb_flag_tipo_avanzamento)
destroy(this.cbx_assegnazione)
destroy(this.cbx_attivazione)
destroy(this.cbx_automatico)
destroy(this.cbx_chiusa_assegn)
destroy(this.cbx_chiusa_attiv)
destroy(this.cbx_chiusa_autom)
destroy(this.cbx_nessuna)
destroy(this.st_num_commesse)
destroy(this.cb_tutti)
destroy(this.st_anno)
end on

event pc_delete;call super::pc_delete;long    ll_num_commessa,ll_prog_riga
integer li_i,li_risposta,li_anno_commessa
string  ls_errore

for li_i = 1 to dw_commesse_lista.deletedcount()

	if dw_commesse_lista.getitemstring(li_i, "flag_blocco", delete!, true) = "S" then
	   g_mb.messagebox("Sep", "Commessa non cancellabile: è bloccata.", &
	              exclamation!, ok!)
	   dw_commesse_lista.set_dw_view(c_ignorechanges)
		triggerevent("pc_retrieve")
	   pcca.error = c_fatal
	   return
	end if

	li_anno_commessa = dw_commesse_lista.getitemnumber(li_i, "anno_commessa", delete!, true)
	ll_num_commessa = dw_commesse_lista.getitemnumber(li_i, "num_commessa", delete!, true)

	li_risposta = f_reset_commessa ( li_anno_commessa, ll_num_commessa, ls_errore )

	if li_risposta = -2 then
		g_mb.messagebox("Sep","Attenzione! La commessa è gia stata parzialmente o totalmente prodotta, pertanto non è possibile eliminarla",exclamation!)
		dw_commesse_lista.set_dw_view(c_ignorechanges)
		triggerevent("pc_retrieve")
	   pcca.error = c_fatal
	   return
	end if

	if li_risposta = -1 then 
		g_mb.messagebox("Sep",ls_errore,exclamation!)
		dw_commesse_lista.set_dw_view(c_ignorechanges)
		triggerevent("pc_retrieve")
	   pcca.error = c_fatal
		rollback;
	   return
	end if

next
end event

event pc_new;call super::pc_new;dw_commesse_1.setitem(dw_commesse_1.getrow(), "data_registrazione", datetime(today()))
end event

event open;call super::open;iuo_magazzino = create uo_magazzino 
end event

event close;call super::close;destroy iuo_magazzino
end event

type dw_commesse_lista from uo_cs_xx_dw within w_commesse
integer x = 18
integer y = 20
integer width = 800
integer height = 1120
integer taborder = 70
string dataobject = "d_commesse_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa
string ls_tipo_avanzamento

ls_tipo_avanzamento = ""

if cbx_assegnazione.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "1"
end if
if cbx_attivazione.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "2"
end if
if cbx_automatico.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "3"
end if
if cbx_chiusa_assegn.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "9"
end if
if cbx_chiusa_attiv.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "8"
end if
if cbx_chiusa_autom.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "7"
end if
if cbx_nessuna.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "0"
end if

//if	ls_tipo_avanzamento = "" then ls_tipo_avanzamento = "%"

ll_anno_commessa=long(em_anno_commessa.text)
l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno_commessa, ls_tipo_avanzamento)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
else
	st_num_commesse.text = "Num. Commesse: " + string(l_error)
END IF




end event

event pcd_new;call super::pcd_new;if i_extendmode then
	long ll_anno,ll_num_commessa,ll_test,ll_num_commessa_forzato
	integer li_risposta
		
	select numero
	into   :ll_anno	
	from   parametri_azienda
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_parametro='ESC';

	select max(num_commessa)
	into   :ll_num_commessa
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    anno_commessa= :ll_anno;

	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_commessa) then
  		ll_num_commessa = 1
	else
  		ll_num_commessa = ll_num_commessa + 1
	end if

	if cbx_forza_numerazione.checked=true then
		
		ll_num_commessa_forzato = long(sle_num_commessa.text)
		
		select num_commessa
		into   :ll_test
		from   anag_commesse
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_commessa= :ll_anno
		and    num_commessa=:ll_num_commessa_forzato;
		
		if sqlca.sqlcode=0 then
			g_mb.messagebox("Sep","Numerazione forzata non valida!",exclamation!)
		else
			ll_num_commessa=ll_num_commessa_forzato
		end if

	end if
	
	this.SetItem (this.GetRow ( ),"num_commessa", ll_num_commessa)
	this.SetItem (this.GetRow ( ),"anno_commessa", ll_anno)
	
	li_risposta = wf_imposta_bottoni()
	cb_aggiorna.enabled=false
	cb_assegna.enabled=false
	cb_dettaglio.enabled=false
	cb_reset_assegna.enabled=false
	cb_1.enabled=true
	cb_varianti.enabled=false

end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	integer li_risposta
	li_risposta=wf_imposta_bottoni()

	
	f_PO_LoadDDDW_DW(dw_commesse_1,"cod_versione",sqlca,&
  	              	  "distinta_padri","cod_versione","des_versione",&
      	           "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + getitemstring(getrow(),"cod_prodotto") +"'")

end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	integer li_risposta
	li_risposta = wf_imposta_bottoni()
	cb_aggiorna.enabled=false
	cb_assegna.enabled=false
	cb_dettaglio.enabled=false
	cb_reset_assegna.enabled=false
	cb_1.enabled=true
	cb_varianti.enabled=false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	integer li_risposta
	li_risposta = wf_imposta_bottoni()
	cb_aggiorna.enabled=true
	cb_1.enabled=false
	cb_varianti.enabled=true
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	string ls_cod_prodotto, ls_cod_versione, ls_cod_prodotto_old, ls_cod_versione_old, ls_errore
	dec{4} ldd_quan_assegnata,ldd_quan_prodotta,ldd_quan_in_ordine, &
			 ldd_quan_impegnata,ldd_quan_in_produzione, &
			 ldd_quan_assegnata_old, ldd_quan_prodotta_old,ldd_quan_in_ordine_old, &
			 ldd_quan_impegnata_old,ldd_quan_in_produzione_old
	long ll_i,ll_anno_commessa,ll_num_commessa,ll_null

	ll_anno_commessa = this.getitemnumber(this.getrow(), "anno_commessa")
	ll_num_commessa = this.getitemnumber(this.getrow(), "num_commessa")
	
	select cod_prodotto,
			 cod_versione,
			 quan_in_produzione,
			 quan_assegnata,
			 quan_prodotta,
			 quan_ordine
	into   :ls_cod_prodotto_old,
			 :ls_cod_versione_old,
			 :ldd_quan_in_produzione_old,
			 :ldd_quan_assegnata_old,
			 :ldd_quan_prodotta_old,
			 :ldd_quan_in_ordine_old
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_commessa and
			 num_commessa = :ll_num_commessa;
	
	ls_cod_prodotto = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_prodotto")
	ls_cod_versione = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_versione")
	ldd_quan_in_produzione = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_in_produzione")
	ldd_quan_assegnata = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_assegnata")
	ldd_quan_prodotta = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_prodotta")
	ldd_quan_in_ordine = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_ordine")
			
	setnull(ll_null)
	
	if ls_cod_prodotto <> ls_cod_prodotto_old or ls_cod_versione <> ls_cod_versione_old or &
		ldd_quan_in_produzione <> ldd_quan_in_produzione_old or ldd_quan_assegnata <> ldd_quan_assegnata_old or &
		ldd_quan_prodotta <> ldd_quan_prodotta_old or ldd_quan_in_ordine <> ldd_quan_in_ordine_old then
		ldd_quan_impegnata = ldd_quan_in_ordine - ldd_quan_assegnata - ldd_quan_in_produzione - ldd_quan_prodotta
		ldd_quan_impegnata_old = ldd_quan_in_ordine_old - ldd_quan_assegnata_old - ldd_quan_in_produzione_old - &
										 ldd_quan_prodotta_old
		
		if iuo_magazzino.uof_impegna_mp_commessa(	false, &
																false, &
																ls_cod_prodotto_old, &
																ls_cod_versione_old, &
																ldd_quan_impegnata_old, &
																ll_anno_commessa, &
																ll_num_commessa, &
																ls_errore) = -1 then
			g_mb.messagebox("Apice","Errore durante il disimpegno",stopsign!)
			rollback;
			return 1
		end if
		
//		if f_impegna_mp( ls_cod_prodotto_old, ls_cod_versione_old, ldd_quan_impegnata_old, ll_anno_commessa_old, &
//							ll_num_commessa_old, ll_null, "varianti_commesse", False ) = -1 then // Prima Disimpegno la quantita vecchia
//					messagebox("SEP","Errore durante l'aggiornamento prodotti",stopsign!)
//			return 1
//		end if
//	
		if iuo_magazzino.uof_impegna_mp_commessa(	true, &
																false, &
																ls_cod_prodotto, &
																ls_cod_versione, &
																ldd_quan_impegnata, &
																ll_anno_commessa, &
																ll_num_commessa, &
																ls_errore) = -1 then
			g_mb.messagebox("Apice","Errore durante il disimpegno",stopsign!)
			rollback;
			return 1
		end if
//		if f_impegna_mp( ls_cod_prodotto, ls_cod_versione, ldd_quan_impegnata, ll_anno_commessa, ll_num_commessa, &
//							ll_null, "varianti_commesse", True ) = -1 then // Poi Impegno la nuova quantita
//				messagebox("SEP","Errore durante l'aggiornamento prodotti",stopsign!)
//			return 1
//		end if
	end if

	for ll_i = 1 to this.deletedcount()
      ll_anno_commessa = this.getitemnumber(ll_i, "anno_commessa", delete!, true)
      ll_num_commessa = this.getitemnumber(ll_i, "num_commessa", delete!, true)

		update det_ord_ven
		set    anno_commessa=:ll_null,
				 num_commessa=:ll_null
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:ll_anno_commessa
		and    num_commessa=:ll_num_commessa;
	
		delete varianti_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa = :ll_anno_commessa
		and    num_commessa = :ll_num_commessa;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione varianti commesse",stopsign!)
			return 1
		end if

		ls_cod_prodotto = this.getitemstring(ll_i, "cod_prodotto", delete!, true)
		ls_cod_versione = this.getitemstring(ll_i, "cod_versione", delete!, true)
	
		ldd_quan_in_produzione = this.getitemnumber(ll_i, "quan_in_produzione", delete!, true)
		ldd_quan_assegnata = this.getitemnumber(ll_i, "quan_assegnata", delete!, true)
		ldd_quan_prodotta = this.getitemnumber(ll_i, "quan_prodotta", delete!, true)
		ldd_quan_in_ordine = this.getitemnumber(ll_i, "quan_ordine", delete!, true)
	
		ldd_quan_impegnata = ldd_quan_in_ordine - ldd_quan_assegnata - ldd_quan_in_produzione - ldd_quan_prodotta
		
		if iuo_magazzino.uof_impegna_mp_commessa(	false, &
																false, &
																ls_cod_prodotto, &
																ls_cod_versione, &
																ldd_quan_impegnata, &
																ll_anno_commessa, &
																ll_num_commessa, &
																ls_errore) = -1 then
			g_mb.messagebox("Apice","Errore durante il disimpegno",stopsign!)
			rollback;
			return 1
		end if
//		if f_impegna_mp( ls_cod_prodotto, ls_cod_versione, ldd_quan_impegnata, ll_anno_commessa, ll_num_commessa, &
//							ll_null, "varianti_commesse", False ) = -1 then //Disimpegno la quantita
//			messagebox("Apice","Errore durante l'aggiornamento prodotti",stopsign!)
//			return 1
//		end if
	next
end if

end event

event pcd_validaterow;call super::pcd_validaterow;long ll_num_dettagli, ll_anno_commessa, ll_num_commessa

ll_anno_commessa = this.getitemnumber(this.getrow(), "anno_commessa")
ll_num_commessa = this.getitemnumber(this.getrow(), "num_commessa")

ll_num_dettagli = 0

select count(*)  
into  :ll_num_dettagli  
from  det_anag_commesse
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_commessa = :ll_anno_commessa and
		num_commessa = :ll_num_commessa;

if ll_num_dettagli > 0 then
	g_mb.messagebox("Attivazione Commesse","Commessa già Attivata, Non si può modificare")
	pcca.error = c_fatal
end if

end event

type cb_dettaglio from commandbutton within w_commesse
integer x = 2784
integer y = 1540
integer width = 361
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettaglio"
end type

event clicked;string ls_flag_tipo_avanzamento

ls_flag_tipo_avanzamento = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "flag_tipo_avanzamento")

if ls_flag_tipo_avanzamento = '2' or ls_flag_tipo_avanzamento = '8' then
	g_mb.messagebox("Sep","La commessa è stata iniziata con un'Altro Tipo di Avanzamento di produzione, pertanto non può essere gestita con questa maschera.", information!)
	return
else
	window_open_parm(w_det_anag_commesse,-1,dw_commesse_lista)
end if

end event

type cb_assegna from commandbutton within w_commesse
event clicked pbm_bnclicked
integer x = 1230
integer y = 1540
integer width = 361
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Assegna"
end type

event clicked;double  ldd_quan_assegnata,ldd_quan_in_produzione,ldd_quan_assegnata_test,ldd_quan_in_ordine, & 
		  ldd_quan_prodotta,ldd_quan_da_assegnare,ldd_quantita_possibile, & 
		  ldd_quan_assegnata_st, ldd_quan_assegnata_mp
long    ll_num_commessa,ll_prog_riga,ll_prog_stock[1],ll_anno_reg_des_mov, ll_num_reg_des_mov
string  ls_errore,ls_test,ls_cod_deposito_prelievo, & 
		  ls_cod_tipo_commessa,ls_cod_tipo_movimento,ls_cod_prodotto,ls_cod_deposito[1], & 
		  ls_cod_ubicazione[1],ls_cod_lotto[1],ls_cod_cliente[1],ls_cod_fornitore[1],ls_cod_versione
integer li_risposta,li_anno_commessa
date 	  ld_data_stock[1]

li_anno_commessa = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "anno_commessa")
ll_num_commessa = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "num_commessa")
ldd_quan_in_produzione = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_in_produzione")
ldd_quan_assegnata = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_assegnata")
ldd_quan_prodotta = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_prodotta")
ldd_quan_in_ordine = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_ordine")
ls_cod_prodotto = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_prodotto")
ls_cod_deposito_prelievo = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_deposito_prelievo")
ls_cod_tipo_commessa = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_tipo_commessa")
ls_cod_versione = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_versione")

select cod_azienda
into   :ls_test
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode <> 100 then
	g_mb.messagebox("Sep","Attenzione esistono già dei dettagli commessa, molto probabilmente un'altro operatore sta eseguendo in questo momento delle assegnazioni e lanci di produzione sulla commessa corrente. Aggiornare i dati prima di procedere",information!)
	return
end if

select cod_tipo_mov_ver_prod_finiti
into   :ls_cod_tipo_movimento
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:ls_cod_tipo_commessa;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return
end if

if isnull(ldd_quan_assegnata)     then ldd_quan_assegnata = 0
if isnull(ldd_quan_in_produzione) then ldd_quan_assegnata = 0
if isnull(ldd_quan_prodotta)      then ldd_quan_assegnata = 0
if isnull(ldd_quan_in_ordine)     then ldd_quan_in_ordine = 0

ldd_quan_da_assegnare = ldd_quan_in_ordine - ldd_quan_assegnata - ldd_quan_in_produzione - ldd_quan_prodotta

if ldd_quan_da_assegnare=0 then
	g_mb.messagebox("Sep","Attenzione! Non è possibile assegnare altra quantità poichè la quantità in ordine risulta già essere assegnata e/o in produzione e/o prodotta",exclamation!)
	return
end if

select cod_prodotto_figlio
into   :ls_test
from   distinta
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto_padre=:ls_cod_prodotto;

// NON METTERE IL CONTROLLO SU SQLCA ERRORE

if sqlca.sqlcode = 100 then
	g_mb.messagebox("Sep","Attenzione! Il prodotto della commessa non ha un distinta base associata, pertanto non sarà possibile assegnare le materie prime. Creare la distinta base del prodotto",exclamation!)
	return
end if

select quan_assegnata
into   :ldd_quan_assegnata_test
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa
and    quan_assegnata > 0;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return
end if

if sqlca.sqlcode = 0 and ldd_quan_assegnata_test > 0 then
	g_mb.messagebox("Sep","Attenzione! Non è possibile assegnare altre quantità poichè esiste" + & 
				  " già una quantità assegnata e non ancora lanciata in produzione" + & 
				  " in dettaglio",exclamation!)
	return
end if

s_cs_xx.parametri.parametro_d_2 = ldd_quan_da_assegnare

window_open(w_assegna,0)

if s_cs_xx.parametri.parametro_i_1 = 0 then return

setpointer(hourglass!)

ldd_quan_assegnata = s_cs_xx.parametri.parametro_d_1

li_risposta = f_calcola_max_prod(ls_cod_prodotto,ls_cod_versione,ls_cod_deposito_prelievo,ldd_quantita_possibile,li_anno_commessa,ll_num_commessa,ls_errore)

if li_risposta = -1 then
	g_mb.messagebox("Sep",ls_errore,exclamation!)
	setpointer(arrow!)
	return
end if

if ldd_quantita_possibile <= 0 then
	g_mb.messagebox("Sep","Attenzione! Non è possibile assegnare alcuna quantità utilizzando il deposito " + &
				  ls_cod_deposito_prelievo, stopsign!)
	rollback;
	return
end if

if ldd_quantita_possibile < ldd_quan_assegnata then
	li_risposta = g_mb.messagebox("Sep","Attenzione, usando il deposito " + ls_cod_deposito_prelievo + & 
 			                   ", per il prelievo delle materie prime, si possono assegnare al massimo" + & 
									 string(ldd_quantita_possibile) +  & 
				                " pezzi . Vuoi continuare l'assegnazione con la nuova quantità?", & 
				                Question!,yesno!,1)
	if li_risposta= 2 then 
		setpointer(arrow!)
		return
	end if
	ldd_quan_assegnata = ldd_quantita_possibile
end if

update anag_commesse
set    quan_assegnata=:ldd_quan_assegnata
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return
end if

select max(prog_riga)
into   :ll_prog_riga
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return
end if

if not isnull(ll_prog_riga) then
	ll_prog_riga++
else
	ll_prog_riga=1
end if

INSERT INTO det_anag_commesse
          ( cod_azienda,
            anno_commessa,   
            num_commessa,   
            prog_riga,   
            anno_registrazione,   
            num_registrazione,   
            quan_assegnata,   
            quan_in_produzione,   
            quan_prodotta,
				cod_tipo_movimento )  
VALUES    ( :s_cs_xx.cod_azienda,   
            :li_anno_commessa,   
            :ll_num_commessa,   
            :ll_prog_riga,   
            null,   
            null,   
            :ldd_quan_assegnata,   
            0,   
            0,
				:ls_cod_tipo_movimento);

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return
end if

li_risposta = f_mat_prime_commessa(li_anno_commessa,ll_num_commessa,ll_prog_riga, & 
											  ls_cod_prodotto, ls_cod_versione, ldd_quan_assegnata, & 
											  ls_cod_tipo_commessa,ls_errore)

if li_risposta = -1 then
	g_mb.messagebox("Sep",ls_errore,exclamation!)

	delete mat_prime_commessa
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
		rollback;
		setpointer(arrow!)
		return
	end if

	delete det_anag_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
		rollback;
		setpointer(arrow!)
		return
	end if
	
	update anag_commesse
	set    quan_assegnata=0
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
		rollback;
		setpointer(arrow!)
		return
	end if
	
end if

if s_cs_xx.parametri.parametro_i_1 = 1 then

	li_risposta = f_auto_assegna_stock(ls_cod_prodotto, ls_cod_versione,ls_cod_deposito_prelievo, & 
												  ldd_quan_assegnata,li_anno_commessa,ll_num_commessa,& 
												  ll_prog_riga,ls_cod_tipo_commessa,0,ls_errore)


	if li_risposta = - 1 then 
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		setpointer(arrow!)
		rollback;
	
		delete mat_prime_commessa
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
		
		if sqlca.sqlcode<>0 then
			g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
			rollback;
			setpointer(arrow!)
			return
		end if
		
		delete det_anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
		
		if sqlca.sqlcode<>0 then
			g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
			rollback;
			setpointer(arrow!)
			return
		end if

		update anag_commesse
		set    quan_assegnata=0
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;

		if sqlca.sqlcode<>0 then
			g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
			rollback;
			setpointer(arrow!)
			return
		end if
		
		return
	end if

end if

if s_cs_xx.parametri.parametro_i_1 = 2 then							// assegnazione manuale stock
																					// impostazione parametri
	select cod_tipo_mov_prel_mat_prime									// s_10 = cod_tipo_movimento
	into   :s_cs_xx.parametri.parametro_s_10							
	from   tab_tipi_commessa
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_commessa=:ls_cod_tipo_commessa;

	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
		rollback;
		setpointer(arrow!)
		return
	end if
	
	s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
	s_cs_xx.parametri.parametro_s_2 = ls_cod_deposito_prelievo
	s_cs_xx.parametri.parametro_s_3 = ls_cod_tipo_commessa
	s_cs_xx.parametri.parametro_s_4 = ls_cod_versione
	s_cs_xx.parametri.parametro_d_1 = ldd_quan_assegnata
	s_cs_xx.parametri.parametro_ul_1 = li_anno_commessa
	s_cs_xx.parametri.parametro_ul_2 = ll_num_commessa   
	s_cs_xx.parametri.parametro_ul_3 = ll_prog_riga
	s_cs_xx.parametri.parametro_b_1 = false
	
   window_open(w_elenco_mat_prime_commessa,0)

	if s_cs_xx.parametri.parametro_b_1 = true then
		rollback;
		return
	end if
	
end if

li_risposta=wf_imposta_bottoni()

li_risposta = f_crea_dest_mov_stock ( li_anno_commessa, ll_num_commessa, ll_prog_riga, ls_errore )

if li_risposta = - 1 then 
	g_mb.messagebox("Sep",ls_errore,stopsign!)
	setpointer(arrow!)
	rollback;
	
	delete mat_prime_commessa
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
		rollback;
		setpointer(arrow!)
		return
	end if
	
	delete det_anag_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
		rollback;
		setpointer(arrow!)
		return
	end if

	update anag_commesse
	set    quan_assegnata=0
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
		rollback;
		setpointer(arrow!)
		return
	end if
	
	return

else
	update anag_commesse
	set    flag_tipo_avanzamento = '1'
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	dw_commesse_1.setitem(dw_commesse_1.getrow(),"quan_assegnata",ldd_quan_assegnata)
	dw_commesse_lista.resetupdate()
	commit;

end if

li_risposta=wf_imposta_bottoni()
setpointer(arrow!)
end event

type cb_reset_assegna from commandbutton within w_commesse
event clicked pbm_bnclicked
integer x = 1618
integer y = 1540
integer width = 361
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&nnulla Ass."
end type

event clicked;long    ll_num_commessa,ll_prog_riga
integer li_risposta,li_anno_commessa
string  ls_errore, ls_flag_tipo_avanzamento

li_anno_commessa = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "anno_commessa")
ll_num_commessa = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "num_commessa")
ls_flag_tipo_avanzamento = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "flag_tipo_avanzamento")

if ls_flag_tipo_avanzamento <> '1' and ls_flag_tipo_avanzamento <> '9' then
	g_mb.messagebox("Sep","La commessa è stata iniziata con un Altro tipo di Avanzamento di produzione, pertanto non può essere gestita con questa maschera.", information!)
	return
end if

select prog_riga
into   :ll_prog_riga
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa
and    quan_assegnata > 0
and    quan_in_produzione=0;

if sqlca.sqlcode=100 then
	g_mb.messagebox("Sep","Non è possibile annullare l'assegnazione, bisogna prima annullare il lancio. E' anche possibile che non esistano dei dettagli commessa poichè un'altro operatore sta eseguendo degli annullamenti di assegnazione sulla commessa corrente. Il tasto risulta abilitato in quanto si deve effettuare un refresh dei dati (premere il tasto esegui ricerca).",information!)
	return
end if

li_risposta = f_elimina_mat_prime_commessa(li_anno_commessa, ll_num_commessa, & 
														 ll_prog_riga, ls_errore )

if li_risposta = -1 then
	g_mb.messagebox("Sep",ls_errore,exclamation!)
	setpointer(arrow!)
	rollback;
	return
end if

delete det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa
and    prog_riga=:ll_prog_riga;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return
end if

update anag_commesse
set    quan_assegnata=0,
	    flag_tipo_avanzamento = '0'
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return
end if

dw_commesse_1.setitem(dw_commesse_1.getrow(),"quan_assegnata",0)
dw_commesse_lista.resetupdate()

commit;

li_risposta=wf_imposta_bottoni()
end event

type em_anno_commessa from editmask within w_commesse
integer x = 2057
integer y = 1320
integer width = 293
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
boolean autoskip = true
boolean spin = true
string displaydata = ""
double increment = 1
string minmax = "1900~~2999"
end type

type cb_aggiorna from commandbutton within w_commesse
integer x = 2766
integer y = 1320
integer width = 361
integer height = 80
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&ggiorna"
end type

event clicked;dw_commesse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cb_1 from cb_prod_ricerca within w_commesse
integer x = 3013
integer y = 500
integer width = 73
integer height = 80
integer taborder = 10
end type

event getfocus;call super::getfocus;dw_commesse_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type cbx_forza_numerazione from checkbox within w_commesse
integer x = 18
integer y = 1156
integer width = 498
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Forza Numerazione"
boolean lefttext = true
end type

event clicked;if sle_num_commessa.enabled=true then
	sle_num_commessa.enabled=false
else
	sle_num_commessa.enabled=true
end if
	
end event

type sle_num_commessa from singlelineedit within w_commesse
integer x = 544
integer y = 1156
integer width = 270
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
boolean autohscroll = false
end type

type cb_varianti from commandbutton within w_commesse
integer x = 2007
integer y = 1540
integer width = 361
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Varianti"
end type

event clicked;s_cs_xx.parametri.parametro_dw_1 = dw_commesse_lista
window_open(w_varianti_commesse,-1)
end event

type dw_commesse_1 from uo_cs_xx_dw within w_commesse
integer x = 864
integer y = 136
integer width = 2240
integer height = 1076
integer taborder = 80
string dataobject = "d_commesse_det_1"
boolean border = false
end type

event pcd_validaterow;call super::pcd_validaterow;	string ls_test,ls_cod_prodotto
	
	ls_cod_prodotto=getitemstring(getrow(),"cod_prodotto")
	
	select cod_prodotto_padre
	into   :ls_test
	from   distinta
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto_padre=:ls_cod_prodotto;

	if ls_test="" or isnull(ls_test) then
		g_mb.messagebox("Sep","Attenzione! Il prodotto selezionato non ha una distinta base associata.",exclamation!)
		pcca.error=c_valfailed
		return 
	end if


end event

event itemchanged;call super::itemchanged;choose case i_colname
  case  "cod_tipo_commessa"

	string ls_cod_deposito_versamento,ls_cod_deposito_prelievo,ls_cod_tipo_mov_prel_mat_prime, &
			 ls_cod_tipo_mov_ver_prod_finiti,ls_cod_tipo_mov_reso_mat_prime,ls_cod_tipo_mov_reso_semilav, &
			 ls_cod_tipo_mov_sfrido_mat_prime,ls_cod_tipo_mov_sfrido_semilav,ls_cod_versione
	
	SELECT cod_deposito_versamento,
          cod_deposito_prelievo,
			 cod_tipo_mov_prel_mat_prime,
			 cod_tipo_mov_ver_prod_finiti,
			 cod_tipo_mov_reso_mat_prime,
			 cod_tipo_mov_reso_semilav,
			 cod_tipo_mov_sfrido_mat_prime,
			 cod_tipo_mov_sfrido_semilav
   INTO   :ls_cod_deposito_versamento,
          :ls_cod_deposito_prelievo,
			 :ls_cod_tipo_mov_prel_mat_prime,
			 :ls_cod_tipo_mov_ver_prod_finiti,
			 :ls_cod_tipo_mov_reso_mat_prime,
			 :ls_cod_tipo_mov_reso_semilav,
			 :ls_cod_tipo_mov_sfrido_mat_prime,
			 :ls_cod_tipo_mov_sfrido_semilav
   FROM   tab_tipi_commessa
   WHERE  cod_azienda =:s_cs_xx.cod_azienda
   AND    cod_tipo_commessa =:data ;
 
	if isnull(ls_cod_tipo_mov_prel_mat_prime) or isnull(ls_cod_tipo_mov_ver_prod_finiti) or &
		isnull(ls_cod_tipo_mov_reso_mat_prime) or isnull(ls_cod_tipo_mov_reso_semilav) or &
		isnull(ls_cod_tipo_mov_sfrido_mat_prime) or isnull(ls_cod_tipo_mov_sfrido_semilav) then
		
		g_mb.messagebox("Sep","Attenzione! Il tipo commessa selezionato ha uno o più tipi movimento non impostato, questo provoca dei problemi in fase di avanzamento produzione: verificare il tipo commessa e assegnare gli opportuni movimenti.", exclamation!)
		return
	end if

	setitem(row,"cod_deposito_versamento",ls_cod_deposito_versamento)
	setitem(row,"cod_deposito_prelievo",ls_cod_deposito_prelievo)

	case "cod_prodotto"
		f_PO_LoadDDDW_DW(dw_commesse_1,"cod_versione",sqlca,&
   	              	  "distinta_padri","cod_versione","des_versione",&
	      	           "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext +"'")

		select cod_versione
		into   :ls_cod_versione
		from   distinta_padri
		where  cod_Azienda=:s_cs_xx.cod_Azienda
		and    cod_prodotto=:i_coltext
		and    flag_predefinita='S';

		setitem(row,"cod_versione",ls_cod_versione)
		
end choose


end event

type dw_commesse_2 from uo_cs_xx_dw within w_commesse
integer x = 960
integer y = 140
integer width = 2098
integer height = 780
integer taborder = 40
string dataobject = "d_commesse_det_2"
boolean border = false
end type

type dw_folder from u_folder within w_commesse
integer x = 841
integer y = 20
integer width = 2304
integer height = 1220
integer taborder = 30
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Data &Generici"
      SetFocus(dw_commesse_1)
   CASE "&Note"
      SetFocus(dw_commesse_2)
END CHOOSE

end on

type cb_chiudi_commessa from commandbutton within w_commesse
integer x = 2395
integer y = 1540
integer width = 361
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Avanz. Aut"
end type

event clicked;long ll_row[]
string ls_flag_tipo_avanzamento

ll_row[1] = dw_commesse_lista.getrow()

s_cs_xx.parametri.parametro_i_1 = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "anno_commessa")
s_cs_xx.parametri.parametro_ul_1 = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "num_commessa")
ls_flag_tipo_avanzamento = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "flag_tipo_avanzamento")

if ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" then
	if g_mb.messagebox("Avanzamento Automatico", &
					  "La Commessa è stata già chiusa, si Desidera Riaprirla?", question!, yesno!) = 2 then
		return
	end if
elseif ls_flag_tipo_avanzamento = "1" or ls_flag_tipo_avanzamento = "2" then
	g_mb.messagebox("Avanzamento Automatico", "Impossibile Proseguire, La Commessa è stata già Attivata o Assegnata")
	return
end if

window_open(w_lista_stock_mp_commesse, 0)

dw_commesse_lista.triggerevent("pcd_retrieve")
dw_commesse_lista.set_selected_rows(1, &
												ll_row[], &
												c_ignorechanges, &
												c_refreshchildren, &
												c_refreshsame)

end event

type gb_flag_tipo_avanzamento from groupbox within w_commesse
integer x = 23
integer y = 1260
integer width = 3131
integer height = 260
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
string text = "Estrazione Commesse X Tipo Avanzamento"
end type

type cbx_assegnazione from checkbox within w_commesse
integer x = 69
integer y = 1420
integer width = 471
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Assegnazione"
boolean lefttext = true
end type

type cbx_attivazione from checkbox within w_commesse
integer x = 1074
integer y = 1420
integer width = 379
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Attivazione"
boolean lefttext = true
end type

type cbx_automatico from checkbox within w_commesse
integer x = 1920
integer y = 1420
integer width = 379
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Automatico"
boolean lefttext = true
end type

type cbx_chiusa_assegn from checkbox within w_commesse
integer x = 549
integer y = 1420
integer width = 503
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Assegn. Chiusa"
boolean lefttext = true
end type

type cbx_chiusa_attiv from checkbox within w_commesse
integer x = 1463
integer y = 1420
integer width = 411
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Attiv. Chiusa"
boolean lefttext = true
end type

type cbx_chiusa_autom from checkbox within w_commesse
integer x = 2309
integer y = 1420
integer width = 480
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Autom. Chiusa"
boolean lefttext = true
end type

type cbx_nessuna from checkbox within w_commesse
integer x = 2811
integer y = 1420
integer width = 320
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Nessuno"
boolean checked = true
boolean lefttext = true
end type

type st_num_commesse from statictext within w_commesse
integer x = 69
integer y = 1340
integer width = 1120
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Numero Commesse Estratte: 0"
boolean focusrectangle = false
end type

type cb_tutti from commandbutton within w_commesse
integer x = 2377
integer y = 1320
integer width = 361
integer height = 80
integer taborder = 131
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Tutte"
end type

event clicked;cbx_assegnazione.checked = true
cbx_attivazione.checked = true
cbx_automatico.checked = true
cbx_chiusa_assegn.checked = true
cbx_chiusa_attiv.checked = true
cbx_chiusa_autom.checked = true
cbx_nessuna.checked = true
cb_aggiorna.triggerevent(clicked!)

end event

type st_anno from statictext within w_commesse
integer x = 1897
integer y = 1320
integer width = 160
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Anno:"
boolean focusrectangle = false
end type

