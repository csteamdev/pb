﻿$PBExportHeader$w_det_anag_commesse.srw
$PBExportComments$Window det_anag_commesse
forward
global type w_det_anag_commesse from w_cs_xx_principale
end type
type dw_det_anag_commesse_lista from uo_cs_xx_dw within w_det_anag_commesse
end type
type dw_det_anag_commesse_det from uo_cs_xx_dw within w_det_anag_commesse
end type
type cb_lancia from commandbutton within w_det_anag_commesse
end type
type cb_reset_lancia from commandbutton within w_det_anag_commesse
end type
type cb_mat_prime from commandbutton within w_det_anag_commesse
end type
type cb_avanzamento from commandbutton within w_det_anag_commesse
end type
type cb_stampa_barcode from commandbutton within w_det_anag_commesse
end type
type cb_anticipi from commandbutton within w_det_anag_commesse
end type
end forward

global type w_det_anag_commesse from w_cs_xx_principale
integer width = 3049
integer height = 1424
string title = "Dettaglio Commesse (Assegnazione)"
dw_det_anag_commesse_lista dw_det_anag_commesse_lista
dw_det_anag_commesse_det dw_det_anag_commesse_det
cb_lancia cb_lancia
cb_reset_lancia cb_reset_lancia
cb_mat_prime cb_mat_prime
cb_avanzamento cb_avanzamento
cb_stampa_barcode cb_stampa_barcode
cb_anticipi cb_anticipi
end type
global w_det_anag_commesse w_det_anag_commesse

forward prototypes
public function integer wf_imposta_bottoni ()
end prototypes

public function integer wf_imposta_bottoni ();string ls_test
double ldd_quan_in_produzione, ldd_quan_prodotta, ldd_quan_anticipo
long ll_anno_commessa,ll_num_commessa,ll_prog_riga,ll_conteggio

ll_anno_commessa = dw_det_anag_commesse_lista.i_parentdw.getitemnumber(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = dw_det_anag_commesse_lista.i_parentdw.getitemnumber(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = dw_det_anag_commesse_det.getitemnumber(dw_det_anag_commesse_det.getrow(),"prog_riga")
ldd_quan_in_produzione = dw_det_anag_commesse_det.getitemnumber(dw_det_anag_commesse_det.getrow(),"quan_in_produzione")
ldd_quan_anticipo = dw_det_anag_commesse_det.getitemnumber(dw_det_anag_commesse_det.getrow(),"quan_anticipo")

cb_lancia.enabled=false
cb_reset_lancia.enabled=false
cb_avanzamento.enabled=false
cb_mat_prime.enabled=false

ll_conteggio = 0

select count(*)
into   :ll_conteggio
from   det_orari_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:ll_anno_commessa
and    num_commessa=:ll_num_commessa
and    prog_riga=:ll_prog_riga;

select cod_azienda
into   :ls_test
from   avan_produzione_com
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:ll_anno_commessa
and    num_commessa=:ll_num_commessa
and    prog_riga=:ll_prog_riga;

if ls_test = s_cs_xx.cod_azienda then cb_avanzamento.enabled = true
ls_test = ""

select cod_azienda
into   :ls_test
from   mat_prime_commessa
where  cod_azienda =:s_cs_xx.cod_azienda
and    anno_commessa =:ll_anno_commessa
and    num_commessa =:ll_num_commessa
and    prog_riga =:ll_prog_riga;

if ls_test=s_cs_xx.cod_azienda then cb_mat_prime.enabled = true

if ll_conteggio > 0 then
	cb_reset_lancia.enabled = false
	cb_lancia.enabled=false
else
	if ldd_quan_in_produzione>0 then
		cb_reset_lancia.enabled = true
		cb_lancia.enabled=false
	else
		cb_reset_lancia.enabled = false
		cb_lancia.enabled=true
	end if
end if

if ldd_quan_anticipo > 0 then
	cb_anticipi.enabled = true
else
	cb_anticipi.enabled = false
end if

return 0
end function

on w_det_anag_commesse.create
int iCurrent
call super::create
this.dw_det_anag_commesse_lista=create dw_det_anag_commesse_lista
this.dw_det_anag_commesse_det=create dw_det_anag_commesse_det
this.cb_lancia=create cb_lancia
this.cb_reset_lancia=create cb_reset_lancia
this.cb_mat_prime=create cb_mat_prime
this.cb_avanzamento=create cb_avanzamento
this.cb_stampa_barcode=create cb_stampa_barcode
this.cb_anticipi=create cb_anticipi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_anag_commesse_lista
this.Control[iCurrent+2]=this.dw_det_anag_commesse_det
this.Control[iCurrent+3]=this.cb_lancia
this.Control[iCurrent+4]=this.cb_reset_lancia
this.Control[iCurrent+5]=this.cb_mat_prime
this.Control[iCurrent+6]=this.cb_avanzamento
this.Control[iCurrent+7]=this.cb_stampa_barcode
this.Control[iCurrent+8]=this.cb_anticipi
end on

on w_det_anag_commesse.destroy
call super::destroy
destroy(this.dw_det_anag_commesse_lista)
destroy(this.dw_det_anag_commesse_det)
destroy(this.cb_lancia)
destroy(this.cb_reset_lancia)
destroy(this.cb_mat_prime)
destroy(this.cb_avanzamento)
destroy(this.cb_stampa_barcode)
destroy(this.cb_anticipi)
end on

event pc_setwindow;call super::pc_setwindow;integer li_risposta

dw_det_anag_commesse_lista.set_dw_options(sqlca, &
                                    		i_openparm, &
		                                    c_scrollparent + c_nonew + c_nomodify + c_nodelete, &
      		                              c_default)

dw_det_anag_commesse_det.set_dw_options(sqlca,dw_det_anag_commesse_lista,c_sharedata + c_scrollparent + c_nonew + c_nomodify + c_nodelete,c_default)

iuo_dw_main = dw_det_anag_commesse_lista
end event

type dw_det_anag_commesse_lista from uo_cs_xx_dw within w_det_anag_commesse
integer x = 23
integer y = 20
integer width = 1211
integer height = 1180
integer taborder = 60
string dataobject = "d_det_anag_commesse_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	integer li_risposta
	li_risposta=wf_imposta_bottoni()
end if
end event

type dw_det_anag_commesse_det from uo_cs_xx_dw within w_det_anag_commesse
integer x = 1257
integer y = 20
integer width = 1737
integer height = 1180
integer taborder = 70
string dataobject = "d_det_anag_commesse_det"
borderstyle borderstyle = styleraised!
end type

type cb_lancia from commandbutton within w_det_anag_commesse
event clicked pbm_bnclicked
integer x = 23
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lancia P."
end type

event clicked;double  ldd_quan_assegnata,ldd_somma_quan_in_produzione,ldd_quan_in_produzione,ldd_quan_prodotta, &
		  ldd_quan_assegnata_mp,ldd_quan_necessaria_mp,ldd_nuova_quan_assegnata,ldd_quan_utilizzo, &
		  ldd_quan_in_produzione_mp,ldd_somma_quan_assegnata,ldd_quan_assegnata_st,ldd_quantita[]
long    ll_num_commessa,ll_prog_riga,ll_error,ll_prog,ll_t
integer li_anno_commessa,li_risposta
string  ls_cod_prodotto,ls_errore,ls_cod_prodotto_mp,ls_cod_deposito_prelievo, & 
		  ls_cod_tipo_movimento,ls_cod_tipo_commessa,ls_cod_prodotto_2[],ls_cod_versione


li_anno_commessa = dw_det_anag_commesse_lista.i_parentdw.getitemnumber(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = dw_det_anag_commesse_lista.i_parentdw.getitemnumber(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "num_commessa")
ls_cod_prodotto = dw_det_anag_commesse_lista.i_parentdw.getitemstring(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_cod_tipo_commessa = dw_det_anag_commesse_lista.i_parentdw.getitemstring(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "cod_tipo_commessa")
ls_cod_tipo_movimento = dw_det_anag_commesse_det.getitemstring(dw_det_anag_commesse_det.getrow(), "cod_tipo_movimento")
ls_cod_deposito_prelievo = dw_det_anag_commesse_lista.i_parentdw.getitemstring(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "cod_deposito_prelievo")
ls_cod_versione = dw_det_anag_commesse_lista.i_parentdw.getitemstring(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "cod_versione")

select max(prog_riga)
into   :ll_prog_riga
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if isnull(ll_prog_riga) then ll_prog_riga=1

select sum(quan_in_produzione),
		 sum(quan_assegnata)
into   :ldd_somma_quan_in_produzione,
		 :ldd_somma_quan_assegnata
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if isnull(ldd_somma_quan_in_produzione) then
	ldd_somma_quan_in_produzione=0
end if

if isnull(ldd_somma_quan_assegnata) then
	ldd_somma_quan_assegnata=0
end if

if ldd_somma_quan_assegnata = 0 then
	g_mb.messagebox("Sep","Attenzione! E' già stata lanciata in produzione tutta la quantità assegnata",exclamation!)
	return
end if

s_cs_xx.parametri.parametro_d_2 = ldd_somma_quan_assegnata

window_open(w_lancia_produzione,0)

if s_cs_xx.parametri.parametro_i_1 = 0 then return

ldd_quan_in_produzione = s_cs_xx.parametri.parametro_d_1
ldd_nuova_quan_assegnata = ldd_somma_quan_assegnata - ldd_quan_in_produzione

if ldd_nuova_quan_assegnata > 0 then

	ll_prog_riga++

	INSERT INTO det_anag_commesse
	           (cod_azienda,
   	  	      anno_commessa,   
         	   num_commessa,   
	            prog_riga,   
   	         anno_registrazione,   
      	      num_registrazione,   
         	   quan_assegnata,   
	            quan_in_produzione,   
   	         quan_prodotta,
				   cod_tipo_movimento )  
	VALUES     (:s_cs_xx.cod_azienda,   
   	         :li_anno_commessa,   
         	   :ll_num_commessa,   
      	      :ll_prog_riga,   
	            null,   
   	         null,   
      	      :ldd_nuova_quan_assegnata,
         	   0,   
	            0,
					:ls_cod_tipo_movimento);

	
	li_risposta = f_mat_prime_commessa(li_anno_commessa,ll_num_commessa,ll_prog_riga,ls_cod_prodotto, ls_cod_versione,ldd_nuova_quan_assegnata,ls_cod_tipo_commessa,ls_errore)
	
	if li_risposta=-1 then
		g_mb.messagebox("Sep",ls_errore,exclamation!)
		rollback;
		dw_det_anag_commesse_lista.triggerevent("pcd_search")
		li_risposta = wf_imposta_bottoni()
		return
	end if
	
	li_risposta = f_aggiorna_mat_prime_commessa_stock ( ls_cod_prodotto,ls_cod_versione,ldd_nuova_quan_assegnata, li_anno_commessa, & 
																		 ll_num_commessa, ll_prog_riga, ldd_quan_in_produzione, & 
																		 ls_errore )
	if li_risposta=-1 then
		g_mb.messagebox("Sep",ls_errore,exclamation!)
		rollback;
		dw_det_anag_commesse_lista.triggerevent("pcd_search")
		li_risposta = wf_imposta_bottoni()
		return
	end if

	ll_prog_riga = ll_prog_riga -1

end if

dw_det_anag_commesse_det.setitem(ll_prog_riga,"quan_assegnata",ldd_nuova_quan_assegnata)
dw_det_anag_commesse_det.setitem(ll_prog_riga,"quan_in_produzione",ldd_quan_in_produzione)

dw_det_anag_commesse_det.resetupdate()

update det_anag_commesse
set    quan_in_produzione=:ldd_quan_in_produzione,
		 quan_assegnata=0
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa
and    prog_riga=:ll_prog_riga;

li_risposta = f_avan_prod_com(li_anno_commessa,ll_num_commessa,ll_prog_riga, & 
										ls_cod_prodotto,ls_cod_versione,ldd_quan_in_produzione,1, & 
										ls_cod_tipo_commessa,ls_errore)

if li_risposta = -1 then
	rollback;
	g_mb.messagebox("Sep",ls_errore,exclamation!)
	setpointer(arrow!)
	dw_det_anag_commesse_lista.triggerevent("pcd_search")
	li_risposta = wf_imposta_bottoni()
	return
end if

li_risposta = f_calcola_quan_utilizzo_2 (ls_cod_prodotto, ls_cod_versione,ls_cod_prodotto_2[], ldd_quantita[],ll_prog,1,li_anno_commessa,ll_num_commessa)

declare righe_mat_prime_commessa cursor for 
select  cod_prodotto,
		  quan_necessaria,	
		  quan_assegnata
from    mat_prime_commessa
where   cod_azienda=:s_cs_xx.cod_azienda 
and     anno_commessa=:li_anno_commessa
and     num_commessa=:ll_num_commessa
and     prog_riga=:ll_prog_riga;

open righe_mat_prime_commessa;

do while 1 = 1
	fetch righe_mat_prime_commessa
	into  :ls_cod_prodotto_mp,
			:ldd_quan_necessaria_mp,
			:ldd_quan_assegnata_mp;
	
  	if (sqlca.sqlcode = 100) or (sqlca.sqlcode=-1) then exit

	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Sep","Errore nel DB:" + SQLCA.SQLErrText,exclamation!)
		close righe_mat_prime_commessa;
		rollback;
		dw_det_anag_commesse_lista.triggerevent("pcd_search")
		li_risposta = wf_imposta_bottoni()
		return
	end if
		
	ldd_quan_utilizzo = 0

	//ldd_quan_utilizzo = ldd_quan_necessaria_mp/ldd_somma_quan_assegnata
	for ll_t = 1 to upperbound(ls_cod_prodotto_2)
		if ls_cod_prodotto_mp= ls_cod_prodotto_2[ll_t] then
			ldd_quan_utilizzo = ldd_quan_utilizzo + ldd_quantita[ll_t]
		end if
	next

	ldd_quan_in_produzione_mp = ldd_quan_utilizzo * ldd_quan_in_produzione

	ldd_quan_necessaria_mp = ldd_quan_in_produzione_mp

	update mat_prime_commessa
	set    quan_in_produzione=:ldd_quan_in_produzione_mp,
			 quan_assegnata=0,	
			 quan_necessaria=:ldd_quan_necessaria_mp
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga
	and    cod_prodotto=:ls_cod_prodotto_mp;

   if sqlca.sqlcode<>0 then 
	  g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,exclamation!)
	  close righe_mat_prime_commessa;
	  rollback;
	  dw_det_anag_commesse_lista.triggerevent("pcd_search")
	  li_risposta = wf_imposta_bottoni()
	  return 
   end if

	ldd_quan_assegnata_st = 0

	select quan_assegnata
	into   :ldd_quan_assegnata_st
	from   anag_prodotti
   WHERE  cod_azienda=:s_cs_xx.cod_azienda  
   AND    cod_prodotto=:ls_cod_prodotto_mp;

   if sqlca.sqlcode<>0 then 
	  g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,exclamation!)
	  close righe_mat_prime_commessa;
	  rollback;
	  dw_det_anag_commesse_lista.triggerevent("pcd_search")
	  li_risposta = wf_imposta_bottoni()
	  return 
   end if

	ldd_quan_assegnata_st = ldd_quan_assegnata_st - ldd_quan_in_produzione_mp
	if ldd_quan_assegnata_st < 0 then ldd_quan_assegnata_st = 0

   UPDATE anag_prodotti
   SET    quan_assegnata=:ldd_quan_assegnata_st
   WHERE  cod_azienda=:s_cs_xx.cod_azienda  
   AND    cod_prodotto=:ls_cod_prodotto_mp;

   if sqlca.sqlcode<>0 then 
	  g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,exclamation!)
	  close righe_mat_prime_commessa;
	  rollback;
	  dw_det_anag_commesse_lista.triggerevent("pcd_search")
	  li_risposta = wf_imposta_bottoni()
	  return 
   end if
	
   li_risposta = f_mov_prod_stock(li_anno_commessa, ll_num_commessa, ll_prog_riga, ls_cod_prodotto_mp, ldd_quan_in_produzione_mp, ls_errore )

	if li_risposta = -1 then
		rollback;
		g_mb.messagebox("Sep",ls_errore,exclamation!)
		dw_det_anag_commesse_lista.triggerevent("pcd_search")
		li_risposta = wf_imposta_bottoni()
		setpointer(arrow!)
		return
	end if

loop

close righe_mat_prime_commessa;

select sum(quan_in_produzione)
into   :ldd_somma_quan_in_produzione
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<>0 then 
  g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,exclamation!)
  rollback;
  dw_det_anag_commesse_lista.triggerevent("pcd_search")
  li_risposta = wf_imposta_bottoni()
  return 
end if

update anag_commesse
set    quan_in_produzione=:ldd_somma_quan_in_produzione,
		 quan_assegnata=:ldd_nuova_quan_assegnata
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<>0 then 
  g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,exclamation!)
  rollback;
  dw_det_anag_commesse_lista.triggerevent("pcd_search")
  li_risposta = wf_imposta_bottoni()
  return 
end if

commit;

dw_det_anag_commesse_lista.i_parentdw.setitem(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "quan_in_produzione",ldd_somma_quan_in_produzione)
dw_det_anag_commesse_lista.i_parentdw.setitem(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "quan_assegnata",ldd_nuova_quan_assegnata)
dw_det_anag_commesse_lista.i_parentdw.resetupdate()

ll_Error = dw_det_anag_commesse_lista.Retrieve(s_cs_xx.cod_azienda,li_anno_commessa,ll_num_commessa)

dw_det_anag_commesse_lista.triggerevent("pcd_search")

li_risposta = wf_imposta_bottoni()
end event

type cb_reset_lancia from commandbutton within w_det_anag_commesse
event clicked pbm_bnclicked
integer x = 411
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla L.P."
end type

event clicked;double  ldd_quan_in_produzione, ldd_quan_necessaria_mp,ldd_quan_utilizzo, &
		  ldd_quan_assegnata_mp,ldd_somma_quan_in_produzione,ldd_quan_in_produzione_mp, &
		  ldd_max_quan_assegnata,ldd_nuova_quan_assegnata
long    ll_num_commessa,ll_prog_riga,ll_max_prog_riga,ll_error,ll_anno_registrazione,ll_num_registrazione,ll_prog_stock
string  ls_cod_prodotto_mp,ls_errore,ls_cod_deposito,ls_cod_ubicazione,ls_cod_lotto, ls_test
integer li_anno_commessa,li_risposta
boolean lb_flag
datetime ldt_data_stock
uo_magazzino luo_mag

li_anno_commessa = dw_det_anag_commesse_lista.i_parentdw.getitemnumber(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = dw_det_anag_commesse_lista.i_parentdw.getitemnumber(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = dw_det_anag_commesse_det.getitemnumber(dw_det_anag_commesse_det.getrow(),"prog_riga")
ldd_quan_in_produzione = dw_det_anag_commesse_det.getitemnumber(dw_det_anag_commesse_det.getrow(),"quan_in_produzione")

select cod_azienda
into   :ls_test
from   avan_produzione_com
where   cod_azienda=:s_cs_xx.cod_azienda 
and     anno_commessa=:li_anno_commessa
and     num_commessa=:ll_num_commessa
and     prog_riga=:ll_prog_riga;

if sqlca.sqlcode = 100 then
	g_mb.messagebox("Sep","Attenzione il lancio di produzione della commessa corrente è già stato annullato, molto probabilmente un'altro operatore sta eseguendo in questo momento degli annullamenti di lancio sulla commessa corrente. Aggiornare i dati prima di procedere (tasto esegui ricerca).",information!)
	return
end if

select prog_riga,															// La select consente di 
		 quan_assegnata													// determinare se esistono
into   :ll_max_prog_riga,												//	dei dettagli commessa solo
		 :ldd_max_quan_assegnata										//	in produzione, se si viene
from   det_anag_commesse												// impostato il flag lb_flag a
where  cod_azienda=:s_cs_xx.cod_azienda							//	false.
and    anno_commessa=:li_anno_commessa								//
and    num_commessa=:ll_num_commessa								//
and    quan_assegnata > 0;												//

lb_flag = true  

if sqlca.sqlcode = 100 then
	ll_max_prog_riga = ll_prog_riga
	ldd_max_quan_assegnata = ldd_quan_in_produzione
	lb_flag = false
end if

if lb_flag = true then
	declare righe_mat_prime_commessa cursor for 
	select  cod_prodotto,
			  quan_necessaria,	
			  quan_assegnata
	from    mat_prime_commessa
	where   cod_azienda=:s_cs_xx.cod_azienda 
	and     anno_commessa=:li_anno_commessa
	and     num_commessa=:ll_num_commessa
	and     prog_riga=:ll_max_prog_riga;

	open righe_mat_prime_commessa;

	do while 1 = 1
		fetch righe_mat_prime_commessa
		into  :ls_cod_prodotto_mp,
				:ldd_quan_necessaria_mp,
				:ldd_quan_assegnata_mp;
	
	  	if (sqlca.sqlcode = 100) or (sqlca.sqlcode=-1) then exit
		if sqlca.sqlcode<>0 then
			g_mb.messagebox("Sep","Errore nel DB:" + SQLCA.SQLErrText,exclamation!)
			close righe_mat_prime_commessa;
			exit
		end if

		ldd_quan_utilizzo = ldd_quan_necessaria_mp/ldd_max_quan_assegnata
		ldd_quan_assegnata_mp =  ldd_quan_utilizzo * (ldd_quan_in_produzione + ldd_max_quan_assegnata)
		ldd_quan_necessaria_mp = ldd_quan_assegnata_mp

		update mat_prime_commessa
		set    quan_assegnata=:ldd_quan_assegnata_mp,	
				 quan_necessaria=:ldd_quan_necessaria_mp
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_max_prog_riga
		and    cod_prodotto=:ls_cod_prodotto_mp;
		
		if sqlca.sqlcode<>0 then
			g_mb.messagebox("Sep","Errore nel DB:" + SQLCA.SQLErrText,exclamation!)
			close righe_mat_prime_commessa;
			exit
		end if

	loop

	close righe_mat_prime_commessa;

	li_risposta = f_reset_mat_prime_commessa_stock (li_anno_commessa, &
												 					ll_num_commessa, &
																	ll_prog_riga, &
																	ll_max_prog_riga, &	
																	ls_errore )

	if li_risposta = -1 then
		g_mb.messagebox("Sep",ls_errore,exclamation!)
		rollback;
		setpointer(arrow!)
		return
	end if

	delete mat_prime_commessa
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga;
	
	if li_risposta <0 then
		g_mb.messagebox("Sep",ls_errore,exclamation!)
		rollback;
		setpointer(arrow!)
		return
	end if
	
else
	declare righe_mat_prime_commessa_2 cursor for 
	select  cod_prodotto,
			  quan_in_produzione,	
			  quan_assegnata
	from    mat_prime_commessa
	where   cod_azienda=:s_cs_xx.cod_azienda 
	and     anno_commessa=:li_anno_commessa
	and     num_commessa=:ll_num_commessa
	and     prog_riga=:ll_max_prog_riga;

	open righe_mat_prime_commessa_2;

	do while 1 = 1
		fetch righe_mat_prime_commessa_2
		into  :ls_cod_prodotto_mp,
				:ldd_quan_in_produzione_mp,
				:ldd_quan_assegnata_mp;
	
	  	if (sqlca.sqlcode = 100) or (sqlca.sqlcode=-1) then exit
		if sqlca.sqlcode<>0 then
			g_mb.messagebox("Sep","Errore nel DB:" + SQLCA.SQLErrText,exclamation!)
			close righe_mat_prime_commessa_2;
			exit
		end if

		declare righe_mp_com_stock cursor for
		select  anno_registrazione,
				  num_registrazione,
				  cod_deposito,
				  cod_ubicazione,
				  cod_lotto,
				  data_stock,
			     prog_stock
		from    mat_prime_commessa_stock
		where   cod_azienda=:s_cs_xx.cod_azienda 
		and     anno_commessa=:li_anno_commessa
		and     num_commessa=:ll_num_commessa
		and     prog_riga=:ll_max_prog_riga
		and     cod_prodotto=:ls_cod_prodotto_mp;

		open righe_mp_com_stock;

		do while 1 = 1
			fetch righe_mp_com_stock
			into  :ll_anno_registrazione,
					:ll_num_registrazione,
				   :ls_cod_deposito,
			      :ls_cod_ubicazione,
					:ls_cod_lotto,
					:ldt_data_stock,
					:ll_prog_stock;
	
		  	if (sqlca.sqlcode = 100) or (sqlca.sqlcode=-1) then exit
			if sqlca.sqlcode<>0 then
				g_mb.messagebox("Sep","Errore nel DB:" + SQLCA.SQLErrText,exclamation!)
				close righe_mp_com_stock;
				exit
			end if

			update mat_prime_commessa_stock
		   set    anno_registrazione=null,
				    num_registrazione=null
			where  cod_azienda=:s_cs_xx.cod_azienda 
			and    anno_commessa=:li_anno_commessa
			and    num_commessa=:ll_num_commessa
			and    prog_riga=:ll_max_prog_riga
			and    cod_prodotto=:ls_cod_prodotto_mp
			and    cod_deposito=:ls_cod_deposito
			and    cod_ubicazione=:ls_cod_ubicazione
			and    cod_lotto=:ls_cod_lotto
			and    data_stock=:ldt_data_stock
			and    prog_stock=:ll_prog_stock;

			if sqlca.sqlcode <>0 then
				g_mb.messagebox("Sep","Operazione interrotta, errore sul DB:" + sqlca.sqlerrtext, stopsign!)
				rollback;
				setpointer(arrow!)
				return
			end if

			luo_mag = create uo_magazzino
  			li_risposta = luo_mag.uof_elimina_movimenti(ll_anno_registrazione,ll_num_registrazione,false)
  			destroy luo_mag

			if li_risposta = -1 then
	  		  li_risposta = g_mb.messagebox("Sep","Attenzione si è verificato un errore tentando di cancellare i movimenti" + &
							 " di magazzino della materia prima " + ls_cod_prodotto_mp + ", anno registrazione movimento = " + &
							 string(ll_anno_registrazione) + ", Numero registrazione movimento=" +  & 
							 string(ll_num_registrazione) + & 
							 ". Vuoi proseguire lo stesso (in questo caso verificare le giacenze negli stock della materia prima indicata)",stopsign!,yesno!,2)
	  	  	  if li_risposta = 2 then
				 setpointer(arrow!)
		  		 rollback;
			    return -1
			  end if

		  end if

		loop

		close righe_mp_com_stock;

		ldd_quan_assegnata_mp =  ldd_quan_in_produzione_mp
		ldd_quan_in_produzione_mp = 0

		update mat_prime_commessa
		set    quan_assegnata=:ldd_quan_assegnata_mp,	
				 quan_in_produzione=:ldd_quan_in_produzione_mp
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_max_prog_riga
		and    cod_prodotto=:ls_cod_prodotto_mp;
		
		if sqlca.sqlcode<>0 then
			g_mb.messagebox("Sep","Errore nel DB:" + SQLCA.SQLErrText,exclamation!)
			close righe_mat_prime_commessa_2;
			exit
		end if
		
	loop

	close righe_mat_prime_commessa_2;

end if
	
if lb_flag = true then
	ldd_nuova_quan_assegnata = ldd_max_quan_assegnata + ldd_quan_in_produzione
else
	li_risposta = f_crea_dest_mov_stock(li_anno_commessa, ll_num_commessa, & 
														ll_max_prog_riga, ls_errore)

	if li_risposta = - 1 then 
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		setpointer(arrow!)
		rollback;
		return
	end if

	ldd_nuova_quan_assegnata = ldd_quan_in_produzione
end if

update det_anag_commesse
set    quan_assegnata=:ldd_nuova_quan_assegnata,
		 quan_in_produzione = 0
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa
and    prog_riga=:ll_max_prog_riga;

if sqlca.sqlcode <>0 then
	g_mb.messagebox("Sep","Operazione interrotta, errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	rollback;
	setpointer(arrow!)
	return
end if

delete avan_produzione_com
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa
and    prog_riga=:ll_prog_riga;

if sqlca.sqlcode <>0 then
	g_mb.messagebox("Sep","Operazione interrotta, errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	rollback;
	setpointer(arrow!)
	return
end if

if lb_flag = true then
	delete det_anag_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga;
end if

if sqlca.sqlcode <>0 then
	g_mb.messagebox("Sep","Operazione interrotta, errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	rollback;
	setpointer(arrow!)
	return
end if

select sum(quan_in_produzione)
into   :ldd_somma_quan_in_produzione
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if lb_flag = true then
	ldd_nuova_quan_assegnata = ldd_max_quan_assegnata + ldd_quan_in_produzione
else
	ldd_nuova_quan_assegnata = ldd_quan_in_produzione
end if

update anag_commesse
set    quan_in_produzione=:ldd_somma_quan_in_produzione,
		 quan_assegnata=:ldd_nuova_quan_assegnata
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode <>0 then
	g_mb.messagebox("Sep","Operazione interrotta, errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	rollback;
	setpointer(arrow!)
	return
end if

commit;

dw_det_anag_commesse_lista.i_parentdw.setitem(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "quan_in_produzione",ldd_somma_quan_in_produzione)
dw_det_anag_commesse_lista.i_parentdw.setitem(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "quan_assegnata",ldd_nuova_quan_assegnata)
dw_det_anag_commesse_lista.i_parentdw.resetupdate()

ll_Error = dw_det_anag_commesse_lista.Retrieve(s_cs_xx.cod_azienda, & 
															  li_anno_commessa,ll_num_commessa)

if dw_det_anag_commesse_lista.rowcount()>0 then 
	dw_det_anag_commesse_lista.triggerevent("pcd_search")
end if

li_risposta=wf_imposta_bottoni()
end event

type cb_mat_prime from commandbutton within w_det_anag_commesse
integer x = 2240
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Mat. Prime"
end type

event clicked;window_open_parm(w_mat_prime_commessa,-1,dw_det_anag_commesse_det)
end event

type cb_avanzamento from commandbutton within w_det_anag_commesse
integer x = 2629
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Avanz. Prod."
end type

event clicked;
window_open_parm(w_avanzamento_produzione_commesse,-1,dw_det_anag_commesse_det)
end event

type cb_stampa_barcode from commandbutton within w_det_anag_commesse
event clicked pbm_bnclicked
integer x = 800
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa BC."
end type

event clicked;s_cs_xx.parametri.parametro_dw_1 = dw_det_anag_commesse_lista
window_open(w_report_bolle_lavoro,-1)	

end event

type cb_anticipi from commandbutton within w_det_anag_commesse
integer x = 1851
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 61
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Anticipi"
end type

event clicked;window_open_parm(w_det_commesse_anticipi, -1, dw_det_anag_commesse_det)
end event

