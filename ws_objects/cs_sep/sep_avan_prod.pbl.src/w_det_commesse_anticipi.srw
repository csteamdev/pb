﻿$PBExportHeader$w_det_commesse_anticipi.srw
$PBExportComments$Window dettaglio anticipi
forward
global type w_det_commesse_anticipi from w_cs_xx_principale
end type
type dw_det_commesse_anticipi from uo_cs_xx_dw within w_det_commesse_anticipi
end type
end forward

global type w_det_commesse_anticipi from w_cs_xx_principale
integer width = 3831
integer height = 644
string title = "Anticipi Effetuati"
dw_det_commesse_anticipi dw_det_commesse_anticipi
end type
global w_det_commesse_anticipi w_det_commesse_anticipi

event pc_setwindow;call super::pc_setwindow;dw_det_commesse_anticipi.set_dw_options(sqlca, &
                                    	i_openparm, &
		                                 c_scrollparent + &
													c_nonew + &
													c_nomodify + &
													c_nodelete, &
      		                           c_default)

iuo_dw_main = dw_det_commesse_anticipi

end event

on w_det_commesse_anticipi.create
int iCurrent
call super::create
this.dw_det_commesse_anticipi=create dw_det_commesse_anticipi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_commesse_anticipi
end on

on w_det_commesse_anticipi.destroy
call super::destroy
destroy(this.dw_det_commesse_anticipi)
end on

type dw_det_commesse_anticipi from uo_cs_xx_dw within w_det_commesse_anticipi
integer x = 23
integer y = 20
integer width = 3749
integer height = 500
string dataobject = "d_det_commesse_anticipi"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa, ll_prog_riga

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa  = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga  = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa, ll_prog_riga)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

