﻿$PBExportHeader$w_det_orari_produzione.srw
$PBExportComments$Window det_orari_produzione
forward
global type w_det_orari_produzione from w_cs_xx_principale
end type
type dw_stock from datawindow within w_det_orari_produzione
end type
type dw_det_orari_produzione_lista from uo_cs_xx_dw within w_det_orari_produzione
end type
end forward

global type w_det_orari_produzione from w_cs_xx_principale
integer width = 5015
integer height = 1952
string title = "Dettaglio Orari per Sessione"
dw_stock dw_stock
dw_det_orari_produzione_lista dw_det_orari_produzione_lista
end type
global w_det_orari_produzione w_det_orari_produzione

forward prototypes
public function integer wf_modifica_mov_mag (long al_anno_registrazione, long al_num_registrazione, decimal ad_nuova_quan_scarico, ref string as_errore)
end prototypes

public function integer wf_modifica_mov_mag (long al_anno_registrazione, long al_num_registrazione, decimal ad_nuova_quan_scarico, ref string as_errore);string						ls_str1, ls_str2, ls_cod_tipo_movimento, ls_cod_prodotto, ls_referenza, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente[]
						
datetime					ldt_data_registrazione, ldt_data_stock[], ldt_data_documento

long						ll_num_documento, ll_prog_mov, ll_prog_stock[], ll_i, ll_anno_registrazione[], ll_num_registrazione[], ll_anno_reg

dec{4}					ld_val_movimento, ld_quan_movimento



select prog_mov,
		data_registrazione,
		cod_tipo_movimento,
		cod_prodotto,
		quan_movimento,
		val_movimento,
		num_documento,
		data_documento,
		referenza,
		cod_deposito,
		cod_ubicazione,
		cod_lotto,
		data_stock,
		prog_stock,
		cod_fornitore,
		cod_cliente
into	:ll_prog_mov,
		:ldt_data_registrazione ,
		:ls_cod_tipo_movimento ,
		:ls_cod_prodotto ,
		:ld_quan_movimento ,
		:ld_val_movimento ,
		:ll_num_documento ,
		:ldt_data_documento ,
		:ls_referenza ,
		:ls_cod_deposito[1] ,
		:ls_cod_ubicazione[1] ,
		:ls_cod_lotto[1] ,
		:ldt_data_stock[1],
		:ll_prog_stock[1] ,
		:ls_cod_fornitore[1] ,
		:ls_cod_cliente[1] 		
from mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :al_anno_registrazione and
		num_registrazione = :al_num_registrazione;

if sqlca.sqlcode = 100 then
	as_errore = g_str.format("Impossibile reperire il movimento di magazzino $1/$2", al_anno_registrazione, al_num_registrazione)
	return -1
end if
if sqlca.sqlcode < 0 then
	as_errore = g_str.format("Errore SQL durante recupero del movimento di magazzino $1/$2. ~r~n$3", al_anno_registrazione, al_num_registrazione, sqlca.sqlerrtext)
	return -1
end if

ll_num_registrazione[1] = al_num_registrazione
ll_anno_registrazione[1] = al_anno_registrazione

if f_modifica_movimenti(ldt_data_registrazione, &
								ls_cod_tipo_movimento, &
								ls_cod_prodotto, &
								ad_nuova_quan_scarico, &
								ld_val_movimento, &
								ll_num_documento, &
								ldt_data_documento, &
								ls_referenza, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ll_anno_registrazione[], &
								ll_num_registrazione[]) = 0 then
	return 0
	
else
	as_errore = g_str.format("Errore durante la modifica del movimento di magazzino $1/$2", al_anno_registrazione, al_num_registrazione)
	return -1
	
end if





end function

on w_det_orari_produzione.create
int iCurrent
call super::create
this.dw_stock=create dw_stock
this.dw_det_orari_produzione_lista=create dw_det_orari_produzione_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_stock
this.Control[iCurrent+2]=this.dw_det_orari_produzione_lista
end on

on w_det_orari_produzione.destroy
call super::destroy
destroy(this.dw_stock)
destroy(this.dw_det_orari_produzione_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_det_orari_produzione_lista.set_dw_key("cod_azienda")
dw_det_orari_produzione_lista.set_dw_key("anno_commessa")
dw_det_orari_produzione_lista.set_dw_key("num_commessa")
dw_det_orari_produzione_lista.set_dw_key("prog_riga")
dw_det_orari_produzione_lista.set_dw_key("cod_prodotto")
dw_det_orari_produzione_lista.set_dw_key("cod_reparto")
dw_det_orari_produzione_lista.set_dw_key("cod_lavorazione")


dw_det_orari_produzione_lista.set_dw_options(sqlca, &
                                    		   i_openparm, &
		                                       c_default, &
      		                                 c_default)
		                                      // c_scrollparent + c_nonew + c_nodelete, &


//dw_det_orari_produzione_det.set_dw_options(sqlca, &
//                                    		 dw_det_orari_produzione_lista, &
//		                                     c_sharedata + c_scrollparent + c_nonew + c_nodelete, &
//      		                               c_default)

iuo_dw_main = dw_det_orari_produzione_lista
dw_stock.settransobject(sqlca)

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_orari_produzione_lista,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pc_save;call super::pc_save;postevent("pc_view")
end event

type dw_stock from datawindow within w_det_orari_produzione
event ue_retrieve ( )
integer x = 23
integer y = 1316
integer width = 4937
integer height = 512
integer taborder = 11
string title = "none"
string dataobject = "d_det_orari_produzione_lista_stock"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_retrieve();string	ls_cod_prodotto_fase, ls_cod_reparto, ls_cod_lavorazione
long ll_anno_commessa, ll_num_commessa, ll_num_riga, ll_prog_orari, ll_row

ll_row = dw_det_orari_produzione_lista.getrow()
ll_anno_commessa = dw_det_orari_produzione_lista.getitemnumber(ll_row,"anno_commessa")
if not isnull(ll_anno_commessa) then
	ll_num_commessa = dw_det_orari_produzione_lista.getitemnumber(ll_row,"num_commessa")
	ll_num_riga = dw_det_orari_produzione_lista.getitemnumber(ll_row,"prog_riga")
	ls_cod_prodotto_fase = dw_det_orari_produzione_lista.getitemstring(ll_row,"cod_prodotto")
	ls_cod_lavorazione = dw_det_orari_produzione_lista.getitemstring(ll_row,"cod_lavorazione")
	ls_cod_reparto = dw_det_orari_produzione_lista.getitemstring(ll_row,"cod_reparto")
	ll_prog_orari = dw_det_orari_produzione_lista.getitemnumber(ll_row,"prog_orari")
	
	retrieve(s_cs_xx.cod_azienda, ll_anno_commessa, ll_num_commessa, ll_num_riga, ls_cod_prodotto_fase, ls_cod_lavorazione, ls_cod_reparto, ll_prog_orari)
end if

	
end event

type dw_det_orari_produzione_lista from uo_cs_xx_dw within w_det_orari_produzione
event pcd_retrieve pbm_custom60
integer x = 18
integer y = 28
integer width = 4942
integer height = 1264
string dataobject = "d_det_orari_produzione"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga,ll_prog_orari
string ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")
ls_cod_prodotto =i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_cod_reparto =i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_reparto")
ls_cod_lavorazione =i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lavorazione")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa,ll_prog_riga, ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	dw_stock.event post ue_retrieve()
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	
	setitem(getrow(),"cod_azienda", s_cs_xx.cod_azienda )
	setitem(getrow(),"anno_commessa", i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa") )
	setitem(getrow(),"num_commessa", i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa") )
	setitem(getrow(),"prog_riga",i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga") )
	setitem(getrow(),"cod_prodotto",i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto") )
	setitem(getrow(),"cod_reparto",i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_reparto") )
	setitem(getrow(),"cod_lavorazione",i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lavorazione") )
	setitem(getrow(),"cod_versione",i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_versione") )

end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	
	long			ll_i, ll_prog_orari, ll_anno_commessa, ll_num_commessa, ll_distinta, ll_mp, ll_y, ll_prog_riga, ll_prog_riga_fase
	dec{4}		ld_quan_differenza, ld_quan_prodotta_new, ld_quan_utilizzo, ld_quan_utilizzo_new
	string			ls_cod_prodotto_fase, ls_cod_reparto_fase, ls_cod_prodotto_figlio, ls_sql, ls_cod_prodotto_mp, ls_cod_reparto, ls_cod_lavorazione, ls_cod_lavorazione_fase, &
					ls_errore
	datastore	lds_data, lds_distinta
	
	for ll_i = 1 to rowcount()
		// verifico se è stata cambiata la quantità
		if getitemnumber(ll_i, "quan_prodotta") <> getitemnumber(ll_i, "quan_prodotta", Primary!, true) then
			// se è stata cambiata devo risistemare gli scarichi già eseguiti
			ld_quan_differenza = getitemnumber(ll_i, "quan_prodotta", Primary!, true) - getitemnumber(ll_i, "quan_prodotta")
		
			ld_quan_prodotta_new = getitemnumber(ll_i, "quan_prodotta")
			ls_cod_prodotto_fase = getitemstring(ll_i, "cod_prodotto")
			ls_cod_reparto_fase =  getitemstring(ll_i, "cod_reparto")
			ls_cod_lavorazione_fase =  getitemstring(ll_i, "cod_lavorazione")
			ll_prog_orari = getitemnumber(ll_i, "prog_orari")
			ll_anno_commessa = getitemnumber(ll_i, "anno_commessa")
			ll_num_commessa = getitemnumber(ll_i, "num_commessa")
			ll_prog_riga_fase = getitemnumber(ll_i, "prog_riga")
			
			// per ogni materia prima presente nella disinta base
			
			ls_sql = "select D.cod_prodotto_figlio,  D.quan_utilizzo, P.prog_riga, P.cod_lavorazione, P.cod_reparto from det_orari_produzione P join distinta D on D.cod_prodotto_padre = P.cod_prodotto and D.cod_versione = P.cod_versione " + &
			g_str.format(" where P.cod_prodotto = '$1' and P.cod_reparto = '$2' and P.prog_orari = $3 and P.anno_commessa=$4 and P.num_commessa = $5 ",ls_cod_prodotto_fase,ls_cod_reparto_fase, ll_prog_orari, ll_anno_commessa, ll_num_commessa)

			ll_distinta =  guo_functions.uof_crea_datastore( lds_distinta, ls_sql)
			
			for ll_y = 1 to ll_distinta
				
				ls_cod_prodotto_mp = lds_distinta.getitemstring(ll_y,1)
				ll_prog_riga = lds_distinta.getitemnumber(ll_y,3)
				ls_cod_lavorazione = lds_distinta.getitemstring(ll_y,4)
				ls_cod_reparto = lds_distinta.getitemstring(ll_y,5)
				
				// verifico quanti stock ci sono e se ce ne è solo 1 modifico automaticamente la quantità
				ls_sql = "select cod_prodotto, quan_utilizzata, anno_registrazione, num_registrazione from mp_com_stock_det_orari " + &
				g_str.format(" where anno_commessa=$1 and num_commessa=$2 and cod_prodotto = '$3' and prog_orari = $4 and cod_reparto = '$5' and cod_prodotto_fase = '$6' ", &
				ll_anno_commessa,ll_num_commessa, ls_cod_prodotto_mp, ll_prog_orari, ls_cod_reparto_fase, ls_cod_prodotto_fase)
							
				ll_mp = guo_functions.uof_crea_datastore( lds_data, ls_sql)
				
				if ll_mp > 1 then
					g_mb.warning("Attenzione: sono presenti più lotti dello stesso prodotto nella sessione sessione; impossibile adeguare la quantità automaticamente!")
					continue
				end if
				
				if ll_mp = 1 then
					ld_quan_utilizzo_new =  lds_distinta.getitemnumber(ll_y,2) * ld_quan_prodotta_new

					update mp_com_stock_det_orari
					set quan_utilizzata = :ld_quan_utilizzo_new
					where cod_azienda = :s_cs_xx.cod_azienda  and
							anno_commessa = :ll_anno_commessa and
							num_commessa = :ll_num_commessa  and
							cod_prodotto = :ls_cod_prodotto_mp  and
							prog_riga = :ll_prog_riga  and
							prog_orari = :ll_prog_orari and
							cod_lavorazione = :ls_cod_lavorazione and
							cod_reparto = :ls_cod_reparto;
							
					if sqlca.sqlcode < 0 then
						g_mb.error("Errore durante aggiornamento quantità materia")
						rollback;
						return 1
					end if			
					
					if not isnull(lds_data.getitemnumber(ll_mp,3)) then
						if wf_modifica_mov_mag(lds_data.getitemnumber(ll_mp,3), lds_data.getitemnumber(ll_mp,4), ld_quan_utilizzo_new, ref ls_errore) < 0 then
							g_mb.error("Errore durante modifica movimenti magazzino")
							rollback;
							return 1
						end if			
					end if
				elseif  ll_mp > 1 then
					g_mb.warning( "Attenzione: per il prodotto $1 c'è lo stesso lotto 2 volte nella stessa sessione oraria. Per la modifica è necessario contattare il servizio di assistenza!")
					rollback;
					return 1
				end if					
			next
			
			// se andato a buon fine aggiorno anche la quantità su avan_produzione_com
			update 		avan_produzione_com
			set      		quan_prodotta = quan_prodotta - :ld_quan_differenza
			where 		cod_azienda = :s_cs_xx.cod_azienda and
							anno_commessa = :ll_anno_commessa and
							num_commessa = :ll_num_commessa  and
							cod_prodotto = :ls_cod_prodotto_fase and
							prog_riga = :ll_prog_riga_fase and
							cod_reparto = :ls_cod_reparto_fase and
							cod_lavorazione = :ls_cod_lavorazione_fase;
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore SQL durante aggiornamento fase di lavorazione~r~n" + sqlca.sqlerrtext)
				rollback;
				return 1
			end if			
			
		end if
	next
end if
end event

event updateend;call super::updateend;/*
select  P.prog_riga, P.cod_lavorazione, P.cod_reparto, P.quan_prodotta, P.flag_inizio 
from det_orari_produzione  where 
P.cod_prodotto = 'S200400001/01' and P.cod_reparto = 'RTC'  and P.anno_commessa=2020 and P.num_commessa =  224

*/
end event

event ue_key;call super::ue_key;if key=KeyF1! and keyflags=1 then
	guo_ricerca.uof_ricerca_attrezzatura( dw_det_orari_produzione_lista, "cod_attrezzatura")
end if
end event

