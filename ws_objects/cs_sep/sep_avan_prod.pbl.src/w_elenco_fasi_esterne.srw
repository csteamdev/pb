﻿$PBExportHeader$w_elenco_fasi_esterne.srw
$PBExportComments$Window elenco fasi esterne
forward
global type w_elenco_fasi_esterne from w_cs_xx_principale
end type
type dw_elenco_fasi_esterne from uo_cs_xx_dw within w_elenco_fasi_esterne
end type
type em_anno_commessa from editmask within w_elenco_fasi_esterne
end type
type st_1 from statictext within w_elenco_fasi_esterne
end type
type cb_aggiorna from commandbutton within w_elenco_fasi_esterne
end type
type cb_1 from commandbutton within w_elenco_fasi_esterne
end type
end forward

global type w_elenco_fasi_esterne from w_cs_xx_principale
int Width=3461
int Height=1625
boolean TitleBar=true
string Title="Elenco fasi esterne"
dw_elenco_fasi_esterne dw_elenco_fasi_esterne
em_anno_commessa em_anno_commessa
st_1 st_1
cb_aggiorna cb_aggiorna
cb_1 cb_1
end type
global w_elenco_fasi_esterne w_elenco_fasi_esterne

on w_elenco_fasi_esterne.create
int iCurrent
call w_cs_xx_principale::create
this.dw_elenco_fasi_esterne=create dw_elenco_fasi_esterne
this.em_anno_commessa=create em_anno_commessa
this.st_1=create st_1
this.cb_aggiorna=create cb_aggiorna
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_elenco_fasi_esterne
this.Control[iCurrent+2]=em_anno_commessa
this.Control[iCurrent+3]=st_1
this.Control[iCurrent+4]=cb_aggiorna
this.Control[iCurrent+5]=cb_1
end on

on w_elenco_fasi_esterne.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_elenco_fasi_esterne)
destroy(this.em_anno_commessa)
destroy(this.st_1)
destroy(this.cb_aggiorna)
destroy(this.cb_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_elenco_fasi_esterne.set_dw_options(sqlca,pcca.null_object,c_nonew + & 
												  c_nodelete + c_nomodify +  & 
												  c_disablecc,c_disableccinsert)

em_anno_commessa.text = string(year(today()))
end event

type dw_elenco_fasi_esterne from uo_cs_xx_dw within w_elenco_fasi_esterne
int X=23
int Y=21
int Width=3383
int Height=1381
int TabOrder=20
string DataObject="d_elenco_fasi_esterne"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa

ll_anno_commessa = long(em_anno_commessa.text)
l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type em_anno_commessa from editmask within w_elenco_fasi_esterne
int X=503
int Y=1421
int Width=298
int Height=81
int TabOrder=30
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData="4"
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_elenco_fasi_esterne
int X=23
int Y=1421
int Width=471
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Anno Commesse:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_aggiorna from commandbutton within w_elenco_fasi_esterne
int X=823
int Y=1421
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Aggiorna"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_1 from commandbutton within w_elenco_fasi_esterne
int X=1212
int Y=1421
int Width=526
int Height=81
int TabOrder=4
boolean BringToTop=true
string Text="&Genera Bolle CTL"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

