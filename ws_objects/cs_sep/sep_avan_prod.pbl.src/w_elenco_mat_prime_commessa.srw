﻿$PBExportHeader$w_elenco_mat_prime_commessa.srw
$PBExportComments$Window elenco materie prime per commessa
forward
global type w_elenco_mat_prime_commessa from w_cs_xx_risposta
end type
type cb_ass_stock from commandbutton within w_elenco_mat_prime_commessa
end type
type dw_mat_prime_commessa from uo_cs_xx_dw within w_elenco_mat_prime_commessa
end type
type cb_1 from commandbutton within w_elenco_mat_prime_commessa
end type
type cb_ass_aut_stock from commandbutton within w_elenco_mat_prime_commessa
end type
type cb_annulla from commandbutton within w_elenco_mat_prime_commessa
end type
end forward

global type w_elenco_mat_prime_commessa from w_cs_xx_risposta
int Width=2186
int Height=1645
boolean TitleBar=true
string Title="Assegnazione Manuale Materie Prime Commessa"
boolean ControlMenu=false
cb_ass_stock cb_ass_stock
dw_mat_prime_commessa dw_mat_prime_commessa
cb_1 cb_1
cb_ass_aut_stock cb_ass_aut_stock
cb_annulla cb_annulla
end type
global w_elenco_mat_prime_commessa w_elenco_mat_prime_commessa

type variables
integer ii_test
end variables

event closequery;call super::closequery;long    ll_t,ll_test

if s_cs_xx.parametri.parametro_b_1 = false then
	ll_test = 0

	for ll_t = 1 to dw_mat_prime_commessa.rowcount()
		if dw_mat_prime_commessa.getitemnumber(ll_t,"ok")=0 then ll_test = 1
	next

	if ll_test = 1 then 
		g_mb.messagebox("Sep","Attenzione! Manca ancora l'assegnazione qualche materia prima.",stopsign!)
		return -1
	end if
end if


end event

event pc_setwindow;call super::pc_setwindow;integer li_risposta
long    ll_t
string  ls_mat_prima[],ls_cod_prodotto,ls_des_prodotto,ls_cod_versione
double  ldd_quan_utilizzo[],ldd_quan_assegnata_totale

dw_mat_prime_commessa.set_dw_options(sqlca, &
												 pcca.null_object, &
												 c_multiselect + &
		                               c_nonew + &
       		                         c_nodelete + &
									  			 c_nomodify + &
		  										 c_disablecc, &
		                               c_default)

save_on_close(c_socnosave)

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
ldd_quan_assegnata_totale = s_cs_xx.parametri.parametro_d_1
ls_cod_versione = s_cs_xx.parametri.parametro_s_4
li_risposta = f_trova_mat_prima(ls_cod_prodotto,ls_cod_versione,ls_mat_prima[],ldd_quan_utilizzo[],ldd_quan_assegnata_totale,s_cs_xx.parametri.parametro_ul_1,s_cs_xx.parametri.parametro_ul_2)

for ll_t = upperbound(ls_mat_prima[]) to 1 step -1
    
   select des_prodotto
   into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto =:ls_mat_prima[ll_t];

	dw_mat_prime_commessa.insertrow(1)
	dw_mat_prime_commessa.setitem(dw_mat_prime_commessa.getrow(),"cod_prodotto",ls_mat_prima[ll_t])
	dw_mat_prime_commessa.setitem(dw_mat_prime_commessa.getrow(),"des_prodotto",ls_des_prodotto)
	dw_mat_prime_commessa.setitem(dw_mat_prime_commessa.getrow(),"quan_utilizzo",ldd_quan_utilizzo[ll_t])
	dw_mat_prime_commessa.setitem(dw_mat_prime_commessa.getrow(),"ok",0)
next
end event

on w_elenco_mat_prime_commessa.create
int iCurrent
call w_cs_xx_risposta::create
this.cb_ass_stock=create cb_ass_stock
this.dw_mat_prime_commessa=create dw_mat_prime_commessa
this.cb_1=create cb_1
this.cb_ass_aut_stock=create cb_ass_aut_stock
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_ass_stock
this.Control[iCurrent+2]=dw_mat_prime_commessa
this.Control[iCurrent+3]=cb_1
this.Control[iCurrent+4]=cb_ass_aut_stock
this.Control[iCurrent+5]=cb_annulla
end on

on w_elenco_mat_prime_commessa.destroy
call w_cs_xx_risposta::destroy
destroy(this.cb_ass_stock)
destroy(this.dw_mat_prime_commessa)
destroy(this.cb_1)
destroy(this.cb_ass_aut_stock)
destroy(this.cb_annulla)
end on

type cb_ass_stock from commandbutton within w_elenco_mat_prime_commessa
event clicked pbm_bnclicked
int X=983
int Y=1441
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Ass.Stock"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string   ls_cod_prodotto,ls_cod_deposito,ls_cod_ubicazione,ls_cod_lotto
datetime ld_data_stock
double   ldd_quan_assegnata_mp,ldd_quan_assegnata_st
long     ll_num_commessa,ll_prog_riga,ll_prog_stock
integer  li_anno_commessa,li_risposta

s_cs_xx.parametri.parametro_s_1 = dw_mat_prime_commessa.getitemstring(dw_mat_prime_commessa.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_3 = "%"
s_cs_xx.parametri.parametro_d_1 = dw_mat_prime_commessa.getitemnumber(dw_mat_prime_commessa.getrow(),"quan_utilizzo")
s_cs_xx.parametri.parametro_s_15 = "CO"

dw_mat_prime_commessa.setitem(dw_mat_prime_commessa.getrow(),"ok",1)

ii_test = 1

li_anno_commessa = integer(s_cs_xx.parametri.parametro_ul_1)
ll_num_commessa = s_cs_xx.parametri.parametro_ul_2
ll_prog_riga = s_cs_xx.parametri.parametro_ul_3
ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1   
ls_cod_deposito = s_cs_xx.parametri.parametro_s_2
   
declare righe_mat_prime_commessa_stock cursor for
select  cod_ubicazione,
 		  cod_lotto,
		  data_stock,
		  prog_stock,
		  quan_assegnata
from    mat_prime_commessa_stock
where   cod_azienda =:s_cs_xx.cod_azienda
and     anno_commessa =:li_anno_commessa
and     num_commessa =:ll_num_commessa
and     prog_riga =:ll_prog_riga
and     cod_prodotto=:ls_cod_prodotto
and     cod_deposito=:ls_cod_deposito;
  
open righe_mat_prime_commessa_stock;

do while 1 = 1
  fetch righe_mat_prime_commessa_stock
  into  :ls_cod_ubicazione,
		  :ls_cod_lotto,
		  :ld_data_stock,
		  :ll_prog_stock,
		  :ldd_quan_assegnata_mp;

  if sqlca.sqlcode<>0 then exit

  select quan_assegnata
  into   :ldd_quan_assegnata_st
  from   stock
  where  cod_azienda = :s_cs_xx.cod_azienda  
  and    cod_prodotto = :ls_cod_prodotto  
  and    cod_deposito = :ls_cod_deposito  
  and    cod_ubicazione = :ls_cod_ubicazione  
  and    cod_lotto = :ls_cod_lotto  
  and    data_stock = :ld_data_stock  
  and    prog_stock = :ll_prog_stock;

  ldd_quan_assegnata_st = ldd_quan_assegnata_st - ldd_quan_assegnata_mp

  UPDATE stock
  SET    quan_assegnata =:ldd_quan_assegnata_st
  WHERE  cod_azienda = :s_cs_xx.cod_azienda  
  AND    cod_prodotto = :ls_cod_prodotto  
  AND    cod_deposito = :ls_cod_deposito  
  AND    cod_ubicazione = :ls_cod_ubicazione  
  AND    cod_lotto = :ls_cod_lotto  
  AND    data_stock = :ld_data_stock  
  AND    prog_stock = :ll_prog_stock;

  if sqlca.sqlcode<>0 then 
	 g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	 close righe_mat_prime_commessa_stock;
	 rollback;
	 return 
  end if
loop

close righe_mat_prime_commessa_stock;

delete mat_prime_commessa_stock
where  cod_azienda =:s_cs_xx.cod_azienda
and    anno_commessa =:li_anno_commessa
and    num_commessa =:ll_num_commessa
and    prog_riga =:ll_prog_riga
and    cod_prodotto=:ls_cod_prodotto;

s_cs_xx.parametri.parametro_b_1 = false

window_open(w_ass_stock,0)

if s_cs_xx.parametri.parametro_b_1 = true then dw_mat_prime_commessa.setitem(dw_mat_prime_commessa.getrow(),"ok",0)

ii_test = 0
end event

type dw_mat_prime_commessa from uo_cs_xx_dw within w_elenco_mat_prime_commessa
int X=23
int Y=21
int Width=2103
int Height=1401
int TabOrder=10
string DataObject="d_mat_prime_commessa"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event doubleclicked;call super::doubleclicked;if i_extendmode then
	cb_ass_stock.triggerevent("clicked")
end if
end event

type cb_1 from commandbutton within w_elenco_mat_prime_commessa
int X=1761
int Y=1441
int Width=366
int Height=81
int TabOrder=40
boolean BringToTop=true
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;close(parent)
end event

type cb_ass_aut_stock from commandbutton within w_elenco_mat_prime_commessa
event clicked pbm_bnclicked
int X=595
int Y=1441
int Width=366
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="&Ass.Aut.S."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string   ls_cod_prodotto,ls_cod_deposito,ls_cod_tipo_commessa,ls_errore,ls_cod_ubicazione, &
			ls_cod_lotto,ls_cod_versione
long     ll_num_commessa,ll_prog_riga,ll_selected[],ll_riga,ll_prog_stock
integer  li_anno_commessa,li_risposta
double 	ldd_quan_assegnata,ldd_quan_assegnata_mp
datetime ld_data_stock

setpointer(hourglass!)

ii_test = 1

li_anno_commessa = integer(s_cs_xx.parametri.parametro_ul_1)
ll_num_commessa = s_cs_xx.parametri.parametro_ul_2
ll_prog_riga = s_cs_xx.parametri.parametro_ul_3
ls_cod_deposito = s_cs_xx.parametri.parametro_s_2
ls_cod_tipo_commessa = s_cs_xx.parametri.parametro_s_3
ls_cod_versione = s_cs_xx.parametri.parametro_s_4

dw_mat_prime_commessa.get_selected_rows(ll_selected[])

for ll_riga = upperbound(ll_selected[]) to 1 step -1
	ls_cod_prodotto = dw_mat_prime_commessa.getitemstring(ll_selected[ll_riga],"cod_prodotto")
	ldd_quan_assegnata = dw_mat_prime_commessa.getitemnumber(ll_selected[ll_riga],"quan_utilizzo")

	declare righe_mat_prime_commessa_stock cursor for
	select  cod_ubicazione,
 			  cod_lotto,
			  data_stock,
			  prog_stock,
			  quan_assegnata
	from    mat_prime_commessa_stock
	where   cod_azienda =:s_cs_xx.cod_azienda
	and     anno_commessa =:li_anno_commessa
	and     num_commessa =:ll_num_commessa
	and     prog_riga =:ll_prog_riga
	and     cod_prodotto=:ls_cod_prodotto
	and     cod_deposito=:ls_cod_deposito;
  
	open righe_mat_prime_commessa_stock;

	do while 1 = 1
	  fetch righe_mat_prime_commessa_stock
	  into  :ls_cod_ubicazione,
			  :ls_cod_lotto,
			  :ld_data_stock,
			  :ll_prog_stock,
			  :ldd_quan_assegnata_mp;

	  if sqlca.sqlcode<>0 then exit

//	  UPDATE stock
//	  SET    quan_assegnata = quan_assegnata - :ldd_quan_assegnata_mp
//	  WHERE  cod_azienda = :s_cs_xx.cod_azienda  
//	  AND    cod_prodotto = :ls_cod_prodotto  
//	  AND    cod_deposito = :ls_cod_deposito  
//	  AND    cod_ubicazione = :ls_cod_ubicazione  
//	  AND    cod_lotto = :ls_cod_lotto  
//	  AND    data_stock = :ld_data_stock  
//	  AND    prog_stock = :ll_prog_stock;
//
//	  if sqlca.sqlcode<>0 then 
//		 messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
//		 close righe_mat_prime_commessa_stock;
//		 rollback;
//		 return 
//	  end if
	loop

	close righe_mat_prime_commessa_stock;

	delete mat_prime_commessa_stock
	where  cod_azienda =:s_cs_xx.cod_azienda
	and    anno_commessa =:li_anno_commessa
	and    num_commessa =:ll_num_commessa
	and    prog_riga =:ll_prog_riga
	and    cod_prodotto=:ls_cod_prodotto;
	
	li_risposta = f_auto_assegna_stock(ls_cod_prodotto, ls_cod_versione,ls_cod_deposito, & 
												  ldd_quan_assegnata,li_anno_commessa,ll_num_commessa,& 
												  ll_prog_riga,ls_cod_tipo_commessa,1,ls_errore)

	if li_risposta = - 1 then 
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		setpointer(arrow!)
		rollback;
		return
	else
		dw_mat_prime_commessa.setitem(ll_selected[ll_riga],"ok",1)

	end if

next

setpointer(arrow!)
end event

type cb_annulla from commandbutton within w_elenco_mat_prime_commessa
event clicked pbm_bnclicked
int X=1372
int Y=1441
int Width=366
int Height=81
int TabOrder=31
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_b_1 = true
close(parent)
end event

