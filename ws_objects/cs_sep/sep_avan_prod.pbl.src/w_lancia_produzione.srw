﻿$PBExportHeader$w_lancia_produzione.srw
$PBExportComments$Window lancia_produzione
forward
global type w_lancia_produzione from w_cs_xx_risposta
end type
type cb_ok from commandbutton within w_lancia_produzione
end type
type em_quan_in_produzione from editmask within w_lancia_produzione
end type
type st_1 from statictext within w_lancia_produzione
end type
type cb_annulla from commandbutton within w_lancia_produzione
end type
end forward

global type w_lancia_produzione from w_cs_xx_risposta
int Width=1203
int Height=365
boolean TitleBar=true
string Title="Lancio Produzione"
boolean ControlMenu=false
cb_ok cb_ok
em_quan_in_produzione em_quan_in_produzione
st_1 st_1
cb_annulla cb_annulla
end type
global w_lancia_produzione w_lancia_produzione

type variables
integer ii_ok
end variables

on w_lancia_produzione.create
int iCurrent
call w_cs_xx_risposta::create
this.cb_ok=create cb_ok
this.em_quan_in_produzione=create em_quan_in_produzione
this.st_1=create st_1
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_ok
this.Control[iCurrent+2]=em_quan_in_produzione
this.Control[iCurrent+3]=st_1
this.Control[iCurrent+4]=cb_annulla
end on

on w_lancia_produzione.destroy
call w_cs_xx_risposta::destroy
destroy(this.cb_ok)
destroy(this.em_quan_in_produzione)
destroy(this.st_1)
destroy(this.cb_annulla)
end on

event open;call super::open;double ld_quan_in_produzione,ld_quan_assegnata

ii_ok = -1
ld_quan_assegnata=s_cs_xx.parametri.parametro_d_2

em_quan_in_produzione.text=string(ld_quan_assegnata)

em_quan_in_produzione.setfocus()
end event

event closequery;call super::closequery;if ii_ok = -1 then return 1
end event

type cb_ok from commandbutton within w_lancia_produzione
int X=389
int Y=161
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Ok"
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;double ld_quan_in_produzione,ld_quan_assegnata

ld_quan_assegnata=s_cs_xx.parametri.parametro_d_2
ld_quan_in_produzione=double(em_quan_in_produzione.text)

if ld_quan_in_produzione > ld_quan_assegnata then	
	g_mb.messagebox("Sep","Attenzione! La quantita da produrre supera la quantità assegnata. Reinserire il valore.",exclamation!)
	em_quan_in_produzione.setfocus()
	return
end if

if ld_quan_in_produzione = 0 then	
	g_mb.messagebox("Sep","Attenzione! La quantita da produrre deve essere >0. Reinserire il valore.",exclamation!)
	em_quan_in_produzione.setfocus()
	return
end if

s_cs_xx.parametri.parametro_d_1=double(em_quan_in_produzione.text)
s_cs_xx.parametri.parametro_i_1 = 1
ii_ok = 0
close (parent)
end event

type em_quan_in_produzione from editmask within w_lancia_produzione
int X=686
int Y=41
int Width=458
int Height=81
int TabOrder=20
boolean BringToTop=true
Alignment Alignment=Right!
BorderStyle BorderStyle=StyleLowered!
string Mask="########.####"
MaskDataType MaskDataType=DecimalMask!
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_lancia_produzione
int X=46
int Y=41
int Width=622
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Quantità da Produrre:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_annulla from commandbutton within w_lancia_produzione
event clicked pbm_bnclicked
int X=778
int Y=161
int Width=366
int Height=81
int TabOrder=2
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;ii_ok = 0
s_cs_xx.parametri.parametro_i_1 = 0
close (parent)
end event

