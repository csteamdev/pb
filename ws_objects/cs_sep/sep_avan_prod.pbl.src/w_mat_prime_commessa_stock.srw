﻿$PBExportHeader$w_mat_prime_commessa_stock.srw
$PBExportComments$Window mat_prime_commessa_stock
forward
global type w_mat_prime_commessa_stock from w_cs_xx_principale
end type
type dw_mat_prime_commessa_stock_lista from uo_cs_xx_dw within w_mat_prime_commessa_stock
end type
type dw_mat_prime_commessa_stock_det from uo_cs_xx_dw within w_mat_prime_commessa_stock
end type
end forward

global type w_mat_prime_commessa_stock from w_cs_xx_principale
int Width=3278
int Height=1381
boolean TitleBar=true
string Title="Distribuzione MP per Stock"
dw_mat_prime_commessa_stock_lista dw_mat_prime_commessa_stock_lista
dw_mat_prime_commessa_stock_det dw_mat_prime_commessa_stock_det
end type
global w_mat_prime_commessa_stock w_mat_prime_commessa_stock

on w_mat_prime_commessa_stock.create
int iCurrent
call w_cs_xx_principale::create
this.dw_mat_prime_commessa_stock_lista=create dw_mat_prime_commessa_stock_lista
this.dw_mat_prime_commessa_stock_det=create dw_mat_prime_commessa_stock_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_mat_prime_commessa_stock_lista
this.Control[iCurrent+2]=dw_mat_prime_commessa_stock_det
end on

on w_mat_prime_commessa_stock.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_mat_prime_commessa_stock_lista)
destroy(this.dw_mat_prime_commessa_stock_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_mat_prime_commessa_stock_lista.set_dw_options(sqlca, &
                                    				 i_openparm, &
				                                     c_scrollparent + c_nonew + c_nodelete, &
      				                               c_default)

dw_mat_prime_commessa_stock_det.set_dw_options(sqlca,dw_mat_prime_commessa_stock_lista,c_sharedata + c_scrollparent + c_nonew + c_nodelete,c_default)

iuo_dw_main = dw_mat_prime_commessa_stock_lista
end event

type dw_mat_prime_commessa_stock_lista from uo_cs_xx_dw within w_mat_prime_commessa_stock
int X=23
int Y=21
int Width=1783
int Height=1241
int TabOrder=10
string DataObject="d_mat_prime_commessa_stock_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG   l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga
string ls_cod_prodotto

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "mat_prime_commessa_anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "mat_prime_commessa_num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "mat_prime_commessa_prog_riga")
ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "mat_prime_commessa_cod_prodotto")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa,ll_prog_riga,ls_cod_prodotto)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_mat_prime_commessa_stock_det from uo_cs_xx_dw within w_mat_prime_commessa_stock
int X=1829
int Y=21
int Width=1395
int Height=1241
int TabOrder=20
string DataObject="d_mat_prime_commessa_stock_det"
BorderStyle BorderStyle=StyleRaised!
end type

