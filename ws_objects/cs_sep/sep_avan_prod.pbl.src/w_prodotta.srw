﻿$PBExportHeader$w_prodotta.srw
$PBExportComments$Window che rileva la quantità prodotta
forward
global type w_prodotta from w_cs_xx_risposta
end type
type cb_1 from commandbutton within w_prodotta
end type
type em_quan_prodotta from editmask within w_prodotta
end type
type st_1 from statictext within w_prodotta
end type
type st_2 from statictext within w_prodotta
end type
type st_quan_in_produzione from statictext within w_prodotta
end type
end forward

global type w_prodotta from w_cs_xx_risposta
int Width=1203
int Height=465
boolean TitleBar=true
string Title="Quantità Prodotta"
boolean ControlMenu=false
cb_1 cb_1
em_quan_prodotta em_quan_prodotta
st_1 st_1
st_2 st_2
st_quan_in_produzione st_quan_in_produzione
end type
global w_prodotta w_prodotta

type variables
integer ii_ok
end variables

on w_prodotta.create
int iCurrent
call w_cs_xx_risposta::create
this.cb_1=create cb_1
this.em_quan_prodotta=create em_quan_prodotta
this.st_1=create st_1
this.st_2=create st_2
this.st_quan_in_produzione=create st_quan_in_produzione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_1
this.Control[iCurrent+2]=em_quan_prodotta
this.Control[iCurrent+3]=st_1
this.Control[iCurrent+4]=st_2
this.Control[iCurrent+5]=st_quan_in_produzione
end on

on w_prodotta.destroy
call w_cs_xx_risposta::destroy
destroy(this.cb_1)
destroy(this.em_quan_prodotta)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_quan_in_produzione)
end on

event open;call super::open;ii_ok = -1
st_quan_in_produzione.text=string(s_cs_xx.parametri.parametro_d_2)
em_quan_prodotta.text="0"
em_quan_prodotta.setfocus()
end event

event closequery;call super::closequery;if ii_ok = -1 then return 1
end event

type cb_1 from commandbutton within w_prodotta
int X=778
int Y=261
int Width=366
int Height=81
int TabOrder=1
boolean BringToTop=true
string Text="&Ok"
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if double(em_quan_prodotta.text)<= 0 then
	g_mb.messagebox("Sep","Attenzione! Inserire la quantità prodotta",stopsign!)
	return 
end if

s_cs_xx.parametri.parametro_d_2=double(em_quan_prodotta.text)
ii_ok = 0
close (parent)
end event

type em_quan_prodotta from editmask within w_prodotta
int X=686
int Y=141
int Width=458
int Height=81
int TabOrder=1
boolean BringToTop=true
Alignment Alignment=Right!
BorderStyle BorderStyle=StyleLowered!
string Mask="########.####"
MaskDataType MaskDataType=DecimalMask!
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_prodotta
int X=46
int Y=141
int Width=618
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Quantità Prodotta:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_prodotta
int X=46
int Y=41
int Width=618
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Quantità in Produzione:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_quan_in_produzione from statictext within w_prodotta
int X=686
int Y=41
int Width=458
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

