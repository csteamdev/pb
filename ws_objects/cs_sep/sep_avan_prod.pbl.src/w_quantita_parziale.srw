﻿$PBExportHeader$w_quantita_parziale.srw
$PBExportComments$Window per la rilevazione quantità utilizzata
forward
global type w_quantita_parziale from w_cs_xx_risposta
end type
type cb_ok from commandbutton within w_quantita_parziale
end type
type st_1 from statictext within w_quantita_parziale
end type
type st_codice from statictext within w_quantita_parziale
end type
type st_descrizione from statictext within w_quantita_parziale
end type
type st_2 from statictext within w_quantita_parziale
end type
type sle_quan_prodotta from singlelineedit within w_quantita_parziale
end type
type st_3 from statictext within w_quantita_parziale
end type
type st_4 from statictext within w_quantita_parziale
end type
type st_qta_in_produzione from statictext within w_quantita_parziale
end type
type dw_quantita_utilizzata from uo_cs_xx_dw within w_quantita_parziale
end type
type cb_1 from commandbutton within w_quantita_parziale
end type
type cb_modifca_stock from commandbutton within w_quantita_parziale
end type
end forward

global type w_quantita_parziale from w_cs_xx_risposta
integer width = 3488
integer height = 1364
string title = "Quantità Utilizzate per S.L. e M.P."
boolean controlmenu = false
cb_ok cb_ok
st_1 st_1
st_codice st_codice
st_descrizione st_descrizione
st_2 st_2
sle_quan_prodotta sle_quan_prodotta
st_3 st_3
st_4 st_4
st_qta_in_produzione st_qta_in_produzione
dw_quantita_utilizzata dw_quantita_utilizzata
cb_1 cb_1
cb_modifca_stock cb_modifca_stock
end type
global w_quantita_parziale w_quantita_parziale

type variables
integer ii_ok

string  is_cod_versione
end variables

on w_quantita_parziale.create
int iCurrent
call super::create
this.cb_ok=create cb_ok
this.st_1=create st_1
this.st_codice=create st_codice
this.st_descrizione=create st_descrizione
this.st_2=create st_2
this.sle_quan_prodotta=create sle_quan_prodotta
this.st_3=create st_3
this.st_4=create st_4
this.st_qta_in_produzione=create st_qta_in_produzione
this.dw_quantita_utilizzata=create dw_quantita_utilizzata
this.cb_1=create cb_1
this.cb_modifca_stock=create cb_modifca_stock
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ok
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.st_codice
this.Control[iCurrent+4]=this.st_descrizione
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.sle_quan_prodotta
this.Control[iCurrent+7]=this.st_3
this.Control[iCurrent+8]=this.st_4
this.Control[iCurrent+9]=this.st_qta_in_produzione
this.Control[iCurrent+10]=this.dw_quantita_utilizzata
this.Control[iCurrent+11]=this.cb_1
this.Control[iCurrent+12]=this.cb_modifca_stock
end on

on w_quantita_parziale.destroy
call super::destroy
destroy(this.cb_ok)
destroy(this.st_1)
destroy(this.st_codice)
destroy(this.st_descrizione)
destroy(this.st_2)
destroy(this.sle_quan_prodotta)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.st_qta_in_produzione)
destroy(this.dw_quantita_utilizzata)
destroy(this.cb_1)
destroy(this.cb_modifca_stock)
end on

event pc_setwindow;call super::pc_setwindow;string  ls_cod_prodotto_figlio,ls_cod_prodotto_padre,ls_des_prodotto,ls_test, ls_cod_versione_figlio, & 
		  ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante
long    ll_num_righe,ll_num_commessa,ll_prog_riga,ll_cont_figli,ll_cont_figli_integrazioni
integer li_anno_commessa
double  ldd_quan_prodotta,ldd_quan_in_produzione,ldd_quan_utilizzo,ldd_quan_utilizzo_variante

set_w_options(c_noenablepopup)

dw_quantita_utilizzata.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)

save_on_close(c_socnosave)

ii_ok = -1
ls_cod_prodotto_padre = s_cs_xx.parametri.parametro_s_1
is_cod_versione = s_cs_xx.parametri.parametro_s_5
li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2

//select cod_versione
//into   :ls_cod_versione
//from   anag_commesse
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    anno_commessa=:li_anno_commessa
//and    num_commessa=:ll_num_commessa;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
//	return
//end if

setnull(ls_test)

select cod_prodotto_padre
into   :ls_test
from   distinta
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto_figlio=:ls_cod_prodotto_padre
and    cod_versione =:is_cod_versione;					//verifica se è il prodotto finito

if not isnull(ls_test) then	// se non è null è il semilavorato
	select quan_in_produzione
	into   :ldd_quan_in_produzione
	from   avan_produzione_com
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga
	and    cod_prodotto=:ls_cod_prodotto_padre
	and    cod_versione = :is_cod_versione;
else
	select quan_in_produzione
	into   :ldd_quan_in_produzione
	from   det_anag_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga;
end if

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto_padre;

st_codice.text=ls_cod_prodotto_padre
st_descrizione.text=ls_des_prodotto

st_qta_in_produzione.text=string(ldd_quan_in_produzione)

//sle_quan_prodotta.text=string(ldd_quan_in_produzione)
sle_quan_prodotta.text=string(s_cs_xx.parametri.parametro_dec4_5)
//-----------------------


declare righe_distinta cursor for 
select  cod_prodotto_figlio,
		  quan_utilizzo,
		  flag_materia_prima,
		  cod_versione_figlio
from    distinta 
where   cod_azienda=:s_cs_xx.cod_azienda 
and     cod_prodotto_padre =:ls_cod_prodotto_padre
and     cod_versione =:is_cod_versione
union all
select  cod_prodotto_figlio,
		  quan_utilizzo,
		  flag_materia_prima,
		  cod_versione_figlio
from    integrazioni_commessa
where   cod_azienda=:s_cs_xx.cod_azienda 
and     anno_commessa=:li_anno_commessa
and     num_commessa=:ll_num_commessa
and     cod_prodotto_padre =:ls_cod_prodotto_padre
and     cod_versione_padre = :is_cod_versione;

open righe_distinta;

do while 1 = 1
	fetch righe_distinta 
	into  :ls_cod_prodotto_figlio,
			:ldd_quan_utilizzo,
			:ls_flag_materia_prima,
			:ls_cod_versione_figlio;
	
  	if (sqlca.sqlcode = 100) or (sqlca.sqlcode=-1) then exit
	
	select cod_prodotto,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 anno_commessa = :li_anno_commessa
	and	 num_commessa = :ll_num_commessa
	and	 cod_prodotto_padre = :ls_cod_prodotto_padre
	and	 cod_prodotto_figlio = :ls_cod_prodotto_figlio
	and    cod_versione = :is_cod_versione
	and    cod_versione_figlio = :ls_cod_versione_figlio;
	
	if sqlca.sqlcode = 0 then
		ls_cod_prodotto_figlio = ls_cod_prodotto_variante
		ldd_quan_utilizzo=ldd_quan_utilizzo_variante
		
		if ls_flag_materia_prima_variante = 'N' then
			
			ll_cont_figli = 0 
			ll_cont_figli_integrazioni = 0
			
			select count(cod_prodotto_figlio)
			into   :ll_cont_figli
			from   distinta
			where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			       cod_prodotto_padre = :ls_cod_prodotto_figlio and    
					 cod_versione = :ls_cod_versione_figlio;
			
			select  count(cod_prodotto_figlio)
			into    :ll_cont_figli_integrazioni
			from    integrazioni_commessa
			where   cod_azienda = :s_cs_xx.cod_azienda  	and     
			        anno_commessa = :li_anno_commessa		and     
					  num_commessa = :ll_num_commessa		and     
					  cod_prodotto_padre = :ls_cod_prodotto_figlio and
					  cod_versione_padre = :ls_cod_versione_figlio;
			
			ll_cont_figli = ll_cont_figli + ll_cont_figli_integrazioni
			
			if ll_cont_figli < 1 then 
				ls_flag_materia_prima="S"
			else
				ls_flag_materia_prima="N"
			end if
		else
			ls_flag_materia_prima ='S'
		end if
		
	else

		if ls_flag_materia_prima = 'N' then
			
			ll_cont_figli = 0 
			ll_cont_figli_integrazioni = 0
			
			select count(cod_prodotto_figlio)
			into   :ll_cont_figli
			from   distinta
			where  cod_azienda = :s_cs_xx.cod_azienda and    
			       cod_prodotto_padre = :ls_cod_prodotto_figlio and    
					 cod_versione = :ls_cod_versione_figlio;
			
			select  count(cod_prodotto_figlio)
			into    :ll_cont_figli_integrazioni
			from    integrazioni_commessa
			where   cod_azienda = :s_cs_xx.cod_azienda  	and     
			        anno_commessa = :li_anno_commessa		and     
					  num_commessa = :ll_num_commessa		and     
					  cod_prodotto_padre = :ls_cod_prodotto_figlio and
					  cod_versione_padre = :ls_cod_versione_figlio;
			
			ll_cont_figli = ll_cont_figli + ll_cont_figli_integrazioni
			
			if ll_cont_figli < 1 then 
				ls_flag_materia_prima="S"
			else
				ls_flag_materia_prima="N"
			end if
		end if

	end if
			 
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto_figlio;	

	if ls_flag_materia_prima='S' then
		select quan_in_produzione
		into   :ldd_quan_in_produzione
		from   mat_prime_commessa
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga
		and    cod_prodotto=:ls_cod_prodotto_figlio;

		ldd_quan_prodotta=ldd_quan_in_produzione

	else
		select quan_prodotta
		into   :ldd_quan_prodotta
		from   avan_produzione_com
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       anno_commessa = :li_anno_commessa 	and    
				 num_commessa = :ll_num_commessa 	and    
				 prog_riga = :ll_prog_riga 		   and    
				 cod_prodotto = :ls_cod_prodotto_figlio and
				 cod_versione = :ls_cod_versione_figlio;

	end if

	ll_num_righe=dw_quantita_utilizzata.rowcount()

	if ll_num_righe=0 then
		ll_num_righe=1
	else
		ll_num_righe++
	end if

	dw_quantita_utilizzata.insertrow(ll_num_righe)
	dw_quantita_utilizzata.setitem(ll_num_righe,"cod_semilavorato",ls_cod_prodotto_figlio)
	dw_quantita_utilizzata.setitem(ll_num_righe,"cod_versione", ls_cod_versione_figlio)
 	dw_quantita_utilizzata.setitem(ll_num_righe,"des_semilavorato",ls_des_prodotto)
 	dw_quantita_utilizzata.setitem(ll_num_righe,"quan_prodotta",ldd_quan_prodotta)
	dw_quantita_utilizzata.setitem(ll_num_righe,"quan_utilizzo",ldd_quan_utilizzo)
	dw_quantita_utilizzata.setitem(ll_num_righe,"mat_prima",ls_flag_materia_prima)
	
loop
	
close righe_distinta;

if ls_flag_materia_prima="S" then
	cb_modifca_stock.enabled=true
else
	cb_modifca_stock.enabled=false
end if

dw_quantita_utilizzata.Reset_DW_Modified(c_ResetChildren)

dw_quantita_utilizzata.settaborder("quan_utilizzata",10)
dw_quantita_utilizzata.settaborder("quan_reso",20)
dw_quantita_utilizzata.settaborder("quan_sfrido",30)
dw_quantita_utilizzata.settaborder("quan_scarto",40)

sle_quan_prodotta.setfocus()
end event

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

type cb_ok from commandbutton within w_quantita_parziale
integer x = 3063
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;string   ls_cod_prodotto,ls_flag_mat_prima,ls_cod_deposito,ls_cod_ubicazione,ls_cod_lotto, ls_cod_versione_dw
long     ll_num_righe,ll_t,ll_num_commessa,ll_prog_riga,ll_prog_stock
integer  li_anno_commessa
double   ldd_quan_utilizzata_sl,ldd_quan_in_produzione_mp, ldd_quan_in_produzione_sl, & 
		   ldd_quan_prodotta_sl,ldd_quan_utilizzata,ldd_quan_utilizzata_mp, &
		   ldd_quan_reso,ldd_quan_sfrido,ldd_quan_scarto, &
		   ldd_quan_reso_mp,ldd_quan_sfrido_mp,ldd_quan_scarto_mp, &	
  		   ldd_quan_reso_sl,ldd_quan_sfrido_sl,ldd_quan_scarto_sl, ldd_totale,&
		   ldd_quan_utilizzo,ldd_quan_utilizzata_teorica,ldd_quan_prodotta,ldd_quan_disponibile
datetime ldt_data_stock
			
li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2

if double(sle_quan_prodotta.text)=0 then
	g_mb.messagebox("Sep","Inserire la quantità prodotta nell'apposito spazio",stopsign!)	
	return
end if

s_cs_xx.parametri.parametro_d_1 = double(sle_quan_prodotta.text)
ldd_quan_prodotta = s_cs_xx.parametri.parametro_d_1

ll_num_righe=dw_quantita_utilizzata.rowcount()

for ll_t=1 to ll_num_righe
	dw_quantita_utilizzata.setrow(ll_t)
	ls_cod_prodotto = dw_quantita_utilizzata.getitemstring(ll_t,"cod_semilavorato")	
	ls_cod_versione_dw = dw_quantita_utilizzata.getitemstring(ll_t,"cod_versione")
	
	ls_flag_mat_prima = dw_quantita_utilizzata.getitemstring(ll_t,"mat_prima")
	
	ldd_quan_disponibile = dw_quantita_utilizzata.getitemnumber(ll_t,"quan_prodotta")	

	ldd_quan_utilizzo = dw_quantita_utilizzata.getitemnumber(ll_t,"quan_utilizzo")	
	ldd_quan_utilizzata_teorica = ldd_quan_prodotta*ldd_quan_utilizzo

   dw_quantita_utilizzata.setcolumn("quan_utilizzata")	
	ldd_quan_utilizzata = double(dw_quantita_utilizzata.gettext())
	
   dw_quantita_utilizzata.setcolumn("quan_reso")	
	ldd_quan_reso = double(dw_quantita_utilizzata.gettext())
	
   dw_quantita_utilizzata.setcolumn("quan_sfrido")	
	ldd_quan_sfrido = double(dw_quantita_utilizzata.gettext())
	
   dw_quantita_utilizzata.setcolumn("quan_scarto")	
	ldd_quan_scarto = double(dw_quantita_utilizzata.gettext())

	if ldd_quan_utilizzata=0 then
		g_mb.messagebox("Sep","Attenzione! Inserire la quantità utilizzata per ogni semilavorato o materia prima in elenco",stopsign!)
		return
	end if

	if ls_flag_mat_prima = "S" then
		if ldd_quan_disponibile < ldd_quan_utilizzata + ldd_quan_reso + ldd_quan_sfrido + ldd_quan_scarto then
			if g_mb.messagebox("Sep","Attenzione! La somma delle quantità supera la quantita disponibile programmata. Vuoi riprogrammare la quantità disponibile utilizzando come nuova quantità la somma delle qtà utilizzata, resa, sfrido e scarto?. Se si, è necessario verificare la disponibilità degli stock iniziali, se no, reinserire le qtà in modo che la loro somma non superi la qtà disponibile.",Question!,yesno!) = 2 then
				dw_quantita_utilizzata.setfocus()
				return
			else
				ldd_totale = ldd_quan_utilizzata+ldd_quan_reso+ldd_quan_sfrido+ldd_quan_scarto
				s_cs_xx.parametri.parametro_d_10 = ldd_quan_disponibile
				s_cs_xx.parametri.parametro_d_11 = ldd_totale
				s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto		
				s_cs_xx.parametri.parametro_b_2=false
				window_open(w_cambia_stock,0)

				if s_cs_xx.parametri.parametro_b_2=true then
					dw_quantita_utilizzata.setitem(dw_quantita_utilizzata.getrow(),"quan_prodotta",ldd_totale)
				else
					return
				end if

			end if
		end if
	else
		if ldd_quan_disponibile < ldd_quan_utilizzata + ldd_quan_reso + ldd_quan_sfrido + ldd_quan_scarto then
			   g_mb.messagebox("Sep","Attenzione! La somma delle quantità supera la quantita disponibile programmata. Vuoi riprogrammare la quantità disponibile utilizzando come nuova quantità la somma delle qtà utilizzata, resa, sfrido e scarto?. Se si, è necessario verificare la disponibilità degli stock iniziali, se no, reinserire le qtà in modo che la loro somma non superi la qtà disponibile.",exclamation!) 
				dw_quantita_utilizzata.setfocus()
				return
		end if
	end if

	if ls_flag_mat_prima="S" then
		select quan_in_produzione,
				 quan_utilizzata,
				 quan_reso,
				 quan_sfrido,
				 quan_scarto
		into   :ldd_quan_in_produzione_mp,
				 :ldd_quan_utilizzata_mp,
				 :ldd_quan_reso_mp,
				 :ldd_quan_sfrido_mp,
				 :ldd_quan_scarto_mp
		from   mat_prime_commessa
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga
		and    cod_prodotto=:ls_cod_prodotto;	
		
		ldd_quan_in_produzione_mp = ldd_quan_in_produzione_mp - ldd_quan_utilizzata - &
											 ldd_quan_reso - ldd_quan_sfrido - ldd_quan_scarto

		ldd_quan_utilizzata_mp = ldd_quan_utilizzata_mp + ldd_quan_utilizzata

		ldd_quan_reso_mp = ldd_quan_reso_mp + ldd_quan_reso

		ldd_quan_sfrido_mp = ldd_quan_sfrido_mp + ldd_quan_sfrido

		ldd_quan_scarto_mp = ldd_quan_scarto_mp + ldd_quan_scarto

		update mat_prime_commessa
		set    quan_in_produzione=:ldd_quan_in_produzione_mp,
				 quan_utilizzata=:ldd_quan_utilizzata_mp,
				 quan_reso=:ldd_quan_reso_mp,
				 quan_sfrido=:ldd_quan_sfrido_mp,
				 quan_scarto=:ldd_quan_scarto_mp
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga
		and    cod_prodotto=:ls_cod_prodotto;	
		
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Sep","Attenzione! Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
			rollback;
			return
		end if
	
	else
		select quan_utilizzata,
				 quan_reso,
				 quan_sfrido,
				 quan_scarto
		into   :ldd_quan_utilizzata_sl,
				 :ldd_quan_reso_sl,
				 :ldd_quan_sfrido_sl,
				 :ldd_quan_scarto_sl
		from   avan_produzione_com
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       anno_commessa = :li_anno_commessa 	and    
				 num_commessa = :ll_num_commessa		and    
				 prog_riga = :ll_prog_riga		      and    
				 cod_prodotto = :ls_cod_prodotto		and    
				 cod_versione = :ls_cod_versione_dw;
	
		ldd_quan_utilizzata_sl = ldd_quan_utilizzata_sl + ldd_quan_utilizzata
		ldd_quan_reso_sl = ldd_quan_reso_sl + ldd_quan_reso
		ldd_quan_sfrido_sl = ldd_quan_sfrido_sl + ldd_quan_sfrido
		ldd_quan_scarto_sl = ldd_quan_scarto_sl + ldd_quan_scarto

		update avan_produzione_com
		set    quan_utilizzata =:ldd_quan_utilizzata_sl,
				 quan_reso =:ldd_quan_reso_sl,
				 quan_sfrido =:ldd_quan_sfrido_sl,
				 quan_scarto =:ldd_quan_scarto_sl
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       anno_commessa = :li_anno_commessa 	and    
				 num_commessa = :ll_num_commessa		and    
				 prog_riga = :ll_prog_riga		      and    
				 cod_prodotto = :ls_cod_prodotto    and
				 cod_versione = :ls_cod_versione_dw;

		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Sep","Attenzione! Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
			rollback;
			return
		end if
		
	end if

next

ii_ok=0
	
close(parent)
end event

type st_1 from statictext within w_quantita_parziale
integer x = 23
integer y = 20
integer width = 366
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_codice from statictext within w_quantita_parziale
integer x = 411
integer y = 20
integer width = 526
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "codice"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_descrizione from statictext within w_quantita_parziale
integer x = 960
integer y = 20
integer width = 2469
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "descrizione"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_2 from statictext within w_quantita_parziale
integer x = 23
integer y = 220
integer width = 2226
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Elenco Materiale Utilizzato ( inserire la quantità utilizzata per ogni materiale in elenco):"
boolean focusrectangle = false
end type

type sle_quan_prodotta from singlelineedit within w_quantita_parziale
integer x = 411
integer y = 120
integer width = 526
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

event losefocus;long    ll_num_righe,ll_t
double  ldd_quan_utilizzata,ldd_quan_prodotta,ldd_quan_utilizzo

ll_num_righe=dw_quantita_utilizzata.rowcount()
ldd_quan_prodotta = double(sle_quan_prodotta.text)

for ll_t=1 to ll_num_righe
	ldd_quan_utilizzo=dw_quantita_utilizzata.getitemnumber(ll_t,"quan_utilizzo")	
	ldd_quan_utilizzata=ldd_quan_prodotta*ldd_quan_utilizzo
	dw_quantita_utilizzata.setitem(ll_t,"quan_utilizzata",ldd_quan_utilizzata)
	dw_quantita_utilizzata.setitem(ll_t,"quan_reso",0)
	dw_quantita_utilizzata.setitem(ll_t,"quan_sfrido",0)
	dw_quantita_utilizzata.setitem(ll_t,"quan_scarto",0)
next

dw_quantita_utilizzata.settaborder("quan_utilizzata",10)
dw_quantita_utilizzata.settaborder("quan_reso",20)
dw_quantita_utilizzata.settaborder("quan_sfrido",30)
dw_quantita_utilizzata.settaborder("quan_scarto",40)
end event

type st_3 from statictext within w_quantita_parziale
integer x = 23
integer y = 120
integer width = 366
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Qtà.Prodotta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_quantita_parziale
integer x = 983
integer y = 120
integer width = 526
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Qtà in Produzione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_qta_in_produzione from statictext within w_quantita_parziale
integer x = 1509
integer y = 120
integer width = 526
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_quantita_utilizzata from uo_cs_xx_dw within w_quantita_parziale
integer x = 23
integer y = 300
integer width = 3406
integer height = 840
integer taborder = 10
string dataobject = "d_quantita_utilizzata"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;call super::rowfocuschanged;if currentrow = 0 then return

if getitemstring(currentrow,"mat_prima")="S" then
	cb_modifca_stock.enabled=true
else
	cb_modifca_stock.enabled=false
end if
end event

type cb_1 from commandbutton within w_quantita_parziale
integer x = 2674
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;ii_ok=0
s_cs_xx.parametri.parametro_b_1=true	
close(parent)
end event

type cb_modifca_stock from commandbutton within w_quantita_parziale
integer x = 2286
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Mod. Stock"
end type

event clicked;double ldd_quan_utilizzata,ldd_quan_reso,ldd_quan_sfrido,ldd_quan_scarto,ldd_totale, &
		 ldd_quan_prodotta

dw_quantita_utilizzata.setcolumn("quan_utilizzata")	
ldd_quan_utilizzata = double(dw_quantita_utilizzata.gettext())
	
dw_quantita_utilizzata.setcolumn("quan_reso")	
ldd_quan_reso = double(dw_quantita_utilizzata.gettext())
	
dw_quantita_utilizzata.setcolumn("quan_sfrido")	
ldd_quan_sfrido = double(dw_quantita_utilizzata.gettext())
	
dw_quantita_utilizzata.setcolumn("quan_scarto")	
ldd_quan_scarto = double(dw_quantita_utilizzata.gettext())

ldd_quan_prodotta = dw_quantita_utilizzata.getitemnumber(dw_quantita_utilizzata.getrow(),"quan_prodotta")


ldd_totale = ldd_quan_utilizzata+ldd_quan_reso+ldd_quan_sfrido+ldd_quan_scarto

s_cs_xx.parametri.parametro_d_10 = ldd_quan_prodotta
s_cs_xx.parametri.parametro_d_11 = ldd_totale

s_cs_xx.parametri.parametro_s_1 = dw_quantita_utilizzata.getitemstring(dw_quantita_utilizzata.getrow(),"cod_semilavorato")
s_cs_xx.parametri.parametro_b_2=false

window_open(w_cambia_stock,0)

if s_cs_xx.parametri.parametro_b_2=true then
	dw_quantita_utilizzata.setitem(dw_quantita_utilizzata.getrow(),"quan_prodotta",ldd_totale)
end if

end event

