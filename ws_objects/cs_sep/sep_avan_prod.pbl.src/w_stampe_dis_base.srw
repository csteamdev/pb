﻿$PBExportHeader$w_stampe_dis_base.srw
$PBExportComments$Window per stampare il tree della Distinta Base
forward
global type w_stampe_dis_base from w_cs_xx_principale
end type
type dw_lista_prodotti_finiti from uo_cs_xx_dw within w_stampe_dis_base
end type
type tv_1 from treeview within w_stampe_dis_base
end type
type cb_1 from commandbutton within w_stampe_dis_base
end type
type dw_lista_lavorazioni from datawindow within w_stampe_dis_base
end type
end forward

global type w_stampe_dis_base from w_cs_xx_principale
int Width=4220
int Height=2481
boolean TitleBar=true
string Title="Stampe Distinta Base"
dw_lista_prodotti_finiti dw_lista_prodotti_finiti
tv_1 tv_1
cb_1 cb_1
dw_lista_lavorazioni dw_lista_lavorazioni
end type
global w_stampe_dis_base w_stampe_dis_base

type prototypes

end prototypes

type variables


end variables

forward prototypes
public function integer wf_imposta_ol (string fs_cod_prodotto, integer fi_num_livello_cor, long fl_handle, ref string fs_errore)
end prototypes

public function integer wf_imposta_ol (string fs_cod_prodotto, integer fi_num_livello_cor, long fl_handle, ref string fs_errore);//	Funzione che imposta l'outliner
//
// nome: wf_imposta_ol
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//	Variabili passate: 		nome					 	tipo				passaggio per			commento
//							
//							 fs_cod_prodotto					string				valore    
//							 fi_num_livello_cor				integer				valore
//							 fl_handle							long   				valore
//							 fs_errore							string				riferimento
//
//		Creata il 12-05-97 
//		Autore Diego Ferrari

string  ls_cod_semilavorato_liv_n,ls_cod_reparto_liv_n, & 
		  ls_cod_prodotto_figlio, ls_cod_prodotto,ls_cod_lavorazione,ls_cod_cat_attrezzature, & 		   
		  ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_des_estesa_prodotto,ls_cod_reparto
long    ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_handle,ll_num_righe_lav
integer li_num_priorita,li_risposta
double  ld_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, &
		  ldd_tempo_lavorazione,ldd_tempo_risorsa_umana
treeviewitem tvi_campo
datastore lds_righe_distinta

ll_num_righe_lav = dw_lista_lavorazioni.rowcount()

if ll_num_righe_lav=0 then
	ll_num_righe_lav=1
else
	ll_num_righe_lav++
end if

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio=lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ld_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto_figlio;
	ls_des_prodotto=trim(ls_des_prodotto)

	tvi_campo.itemhandle = fl_handle
	tvi_campo.data = ls_cod_prodotto_figlio
	tvi_campo.label = ls_cod_prodotto_figlio+","+ls_des_prodotto + " qtà:" + string(ld_quan_utilizzo)
	tvi_campo.pictureindex = 2
	tvi_campo.selectedpictureindex = 1
	tvi_campo.overlaypictureindex = 1

	ll_handle=tv_1.insertitemlast(fl_handle, tvi_campo)

	declare righe_lavorazioni cursor for
	select cod_reparto,
			 cod_lavorazione,
			 cod_cat_attrezzature,
			 des_estesa_prodotto,
			 tempo_attrezzaggio,
			 tempo_attrezzaggio_commessa,
			 tempo_lavorazione,
			 tempo_risorsa_umana
	from   tes_fasi_lavorazione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto_figlio;
	
	open righe_lavorazioni;
	
	do while 1=1
		fetch righe_lavorazioni
		into  :ls_cod_reparto,
				:ls_cod_lavorazione,
			 	:ls_cod_cat_attrezzature,
				:ls_des_estesa_prodotto,
				:ldd_tempo_attrezzaggio,
				:ldd_tempo_attrezzaggio_commessa,
				:ldd_tempo_lavorazione,
				:ldd_tempo_risorsa_umana;
				
		if sqlca.sqlcode=100 then exit
		if sqlca.sqlcode<0 then exit
		
		
		dw_lista_lavorazioni.insertrow(ll_num_righe_lav)
		dw_lista_lavorazioni.setitem(ll_num_righe_lav,"cod_prodotto",ls_cod_prodotto_figlio)
		dw_lista_lavorazioni.setitem(ll_num_righe_lav,"cod_reparto",ls_cod_reparto)
		dw_lista_lavorazioni.setitem(ll_num_righe_lav,"cod_lavorazione",ls_cod_lavorazione)
		dw_lista_lavorazioni.setitem(ll_num_righe_lav,"cod_cat_attrezzature",ls_cod_cat_attrezzature)
		dw_lista_lavorazioni.setitem(ll_num_righe_lav,"des_estesa_prodotto",ls_des_estesa_prodotto)
		dw_lista_lavorazioni.setitem(ll_num_righe_lav,"tempo_attrezzaggio",ldd_tempo_attrezzaggio)
		dw_lista_lavorazioni.setitem(ll_num_righe_lav,"tempo_attrezzaggio_commessa",ldd_tempo_attrezzaggio_commessa)
		dw_lista_lavorazioni.setitem(ll_num_righe_lav,"tempo_lavorazione",ldd_tempo_lavorazione)
		dw_lista_lavorazioni.setitem(ll_num_righe_lav,"tempo_risorsa_umana",ldd_tempo_risorsa_umana)
		ll_num_righe_lav++
		
	loop
	
	close righe_lavorazioni;
	
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda=:s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:ls_cod_prodotto_figlio;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		continue
	else
		li_risposta=wf_imposta_ol(ls_cod_prodotto_figlio, fi_num_livello_cor + 1,ll_handle,ls_errore)
	end if
	
next

return 0
end function

on w_stampe_dis_base.create
int iCurrent
call w_cs_xx_principale::create
this.dw_lista_prodotti_finiti=create dw_lista_prodotti_finiti
this.tv_1=create tv_1
this.cb_1=create cb_1
this.dw_lista_lavorazioni=create dw_lista_lavorazioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_prodotti_finiti
this.Control[iCurrent+2]=tv_1
this.Control[iCurrent+3]=cb_1
this.Control[iCurrent+4]=dw_lista_lavorazioni
end on

on w_stampe_dis_base.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_lista_prodotti_finiti)
destroy(this.tv_1)
destroy(this.cb_1)
destroy(this.dw_lista_lavorazioni)
end on

event open;call super::open;dw_lista_prodotti_finiti.set_dw_options(sqlca,pcca.null_object,c_nonew + & 
													 c_nodelete + c_nomodify +  & 
													 c_disablecc,c_disableccinsert)
end event

type dw_lista_prodotti_finiti from uo_cs_xx_dw within w_stampe_dis_base
int X=23
int Y=21
int Width=938
int Height=1321
int TabOrder=20
string DataObject="d_lista_prodotti_finiti"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_pickedrow;call super::pcd_pickedrow;string ls_cod_prodotto,ls_errore,ls_des_prodotto
long ll_risposta
treeviewitem tvi_campo
ls_cod_prodotto = dw_lista_prodotti_finiti.getitemstring(dw_lista_prodotti_finiti.getrow(),"cf_cod_prodotto_padre")

tv_1.setredraw(false)
tv_1.deleteitem(0)

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto;
ls_des_prodotto=trim(ls_des_prodotto)

tvi_campo.itemhandle = 1
tvi_campo.data = ls_cod_prodotto
tvi_campo.label = ls_cod_prodotto + "," + ls_des_prodotto
tvi_campo.pictureindex = 2
tvi_campo.selectedpictureindex = 1
tvi_campo.overlaypictureindex = 1


ll_risposta=tv_1.insertitemlast(0, tvi_campo)
dw_lista_lavorazioni.reset()
wf_imposta_ol(ls_cod_prodotto,1,1,ls_errore)

tv_1.expandall(1)
tv_1.setredraw(true)
end event

type tv_1 from treeview within w_stampe_dis_base
int X=983
int Y=21
int Width=3178
int Height=1421
int TabOrder=30
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
string PictureName[]={"Custom039!"}
long PictureMaskColor=553648127
long StatePictureMaskColor=536870912
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event constructor;this.addpicture(s_cs_xx.volume + "\cs_xx\risorse\menuvoca.bmp")
this.addpicture(s_cs_xx.volume + "\cs_xx\risorse\menuvocc.bmp")
this.addpicture(s_cs_xx.volume + "\cs_xx\risorse\menufina.bmp")
this.addpicture(s_cs_xx.volume + "\cs_xx\risorse\menufinc.bmp")
end event

type cb_1 from commandbutton within w_stampe_dis_base
int X=23
int Y=1361
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Stampa"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long Job

Job = PrintOpen( )
tv_1.Print(Job, 500,1000)
PrintClose(Job)
end event

type dw_lista_lavorazioni from datawindow within w_stampe_dis_base
int X=23
int Y=1461
int Width=4138
int Height=901
int TabOrder=4
boolean BringToTop=true
string DataObject="d_lista_lavorazioni"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

