﻿$PBExportHeader$w_tipi_commessa.srw
$PBExportComments$Tabella Tipologie  Commesse
forward
global type w_tipi_commessa from w_cs_xx_principale
end type
type dw_tipi_commessa_lista from uo_cs_xx_dw within w_tipi_commessa
end type
type dw_tipi_commessa_1 from uo_cs_xx_dw within w_tipi_commessa
end type
type dw_tipi_commessa_3 from uo_cs_xx_dw within w_tipi_commessa
end type
type dw_tipi_commessa_2 from uo_cs_xx_dw within w_tipi_commessa
end type
type dw_tipi_commessa from uo_cs_xx_dw within w_tipi_commessa
end type
type dw_folder from u_folder within w_tipi_commessa
end type
end forward

global type w_tipi_commessa from w_cs_xx_principale
integer width = 2523
integer height = 1580
string title = "Tabella Tipi Commessa"
dw_tipi_commessa_lista dw_tipi_commessa_lista
dw_tipi_commessa_1 dw_tipi_commessa_1
dw_tipi_commessa_3 dw_tipi_commessa_3
dw_tipi_commessa_2 dw_tipi_commessa_2
dw_tipi_commessa dw_tipi_commessa
dw_folder dw_folder
end type
global w_tipi_commessa w_tipi_commessa

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

l_objects[1] = dw_tipi_commessa
dw_folder.fu_AssignTab(1, "&Produzione", l_Objects[])
l_objects[1] = dw_tipi_commessa_1
dw_folder.fu_AssignTab(2, "&Depositi e Anticipi", l_Objects[])
l_objects[1] = dw_tipi_commessa_2
dw_folder.fu_AssignTab(3, "&Semilavorati", l_objects[ ])
l_objects[1] = dw_tipi_commessa_3
dw_folder.fu_AssignTab(4, "&Bolle CTL", l_Objects[])

dw_folder.fu_FolderCreate(4,4)

dw_tipi_commessa_lista.set_dw_key("cod_azienda")
dw_tipi_commessa_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tipi_commessa.set_dw_options(sqlca,dw_tipi_commessa_lista,c_sharedata+c_scrollparent,c_default)
dw_tipi_commessa_1.set_dw_options(sqlca,dw_tipi_commessa_lista,c_sharedata+c_scrollparent,c_default)
dw_tipi_commessa_2.set_dw_options(sqlca,dw_tipi_commessa_lista,c_sharedata+c_scrollparent,c_default)
dw_tipi_commessa_3.set_dw_options(sqlca,dw_tipi_commessa_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tipi_commessa_lista


dw_folder.fu_SelectTab(1)

end event

on w_tipi_commessa.create
int iCurrent
call super::create
this.dw_tipi_commessa_lista=create dw_tipi_commessa_lista
this.dw_tipi_commessa_1=create dw_tipi_commessa_1
this.dw_tipi_commessa_3=create dw_tipi_commessa_3
this.dw_tipi_commessa_2=create dw_tipi_commessa_2
this.dw_tipi_commessa=create dw_tipi_commessa
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_commessa_lista
this.Control[iCurrent+2]=this.dw_tipi_commessa_1
this.Control[iCurrent+3]=this.dw_tipi_commessa_3
this.Control[iCurrent+4]=this.dw_tipi_commessa_2
this.Control[iCurrent+5]=this.dw_tipi_commessa
this.Control[iCurrent+6]=this.dw_folder
end on

on w_tipi_commessa.destroy
call super::destroy
destroy(this.dw_tipi_commessa_lista)
destroy(this.dw_tipi_commessa_1)
destroy(this.dw_tipi_commessa_3)
destroy(this.dw_tipi_commessa_2)
destroy(this.dw_tipi_commessa)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_commessa, &
                 "cod_tipo_mov_prel_mat_prime", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_commessa, &
                 "cod_tipo_mov_ver_prod_finiti", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_commessa, &
                 "cod_tipo_mov_reso_mat_prime", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_commessa, &
                 "cod_tipo_mov_reso_semilav", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_commessa, &
                 "cod_tipo_mov_sfrido_mat_prime", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_commessa, &
                 "cod_tipo_mov_sfrido_semilav", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_tipi_commessa_1, &
                 "cod_tipo_mov_anticipo", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_commessa_1, &
                 "cod_deposito_versamento", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tipi_commessa_1, &
                 "cod_deposito_prelievo", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_tipi_commessa_2, &
                 "cod_deposito_sl", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tipi_commessa_2, &
                 "cod_tipo_mov_ver_sl", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_commessa_2, &
                 "cod_tipo_mov_prel_sl", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_tipi_commessa_3, &
                 "cod_tipo_bol_ven", &
                 sqlca, &
                 "tab_tipi_bol_ven", &
                 "cod_tipo_bol_ven", &
                 "des_tipo_bol_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_tipi_commessa_3, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_tipi_commessa_3, &
                 "cod_tipo_det_ven_sl", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_po_loaddddw_dw(dw_tipi_commessa_3, &
                 "cod_tipo_bol_acq", &
                 sqlca, &
                 "tab_tipi_bol_acq", &
                 "cod_tipo_bol_acq", &
                 "des_tipo_bol_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_tipi_commessa_3, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_commessa_3, &
                 "cod_tipo_mov_scarico_terzista", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type dw_tipi_commessa_lista from uo_cs_xx_dw within w_tipi_commessa
integer x = 23
integer y = 20
integer width = 2446
integer height = 360
integer taborder = 20
string dataobject = "d_tipi_commessa_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_tipi_commessa_1 from uo_cs_xx_dw within w_tipi_commessa
integer x = 91
integer y = 540
integer width = 2286
integer height = 860
integer taborder = 30
string dataobject = "d_tipi_commessa_1"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if dwo.name = "flag_muovi_mp" then
	setitem(row,"flag_scarica_mp_chiusura",'N')
end if
end event

type dw_tipi_commessa_3 from uo_cs_xx_dw within w_tipi_commessa
integer x = 91
integer y = 540
integer width = 2286
integer height = 520
integer taborder = 32
string dataobject = "d_tipi_commessa_3"
borderstyle borderstyle = styleraised!
end type

type dw_tipi_commessa_2 from uo_cs_xx_dw within w_tipi_commessa
integer x = 91
integer y = 540
integer width = 2286
integer height = 380
integer taborder = 40
string dataobject = "d_tipi_commessa_2"
borderstyle borderstyle = styleraised!
end type

type dw_tipi_commessa from uo_cs_xx_dw within w_tipi_commessa
integer x = 91
integer y = 540
integer width = 2286
integer height = 860
integer taborder = 50
string dataobject = "d_tipi_commessa"
borderstyle borderstyle = styleraised!
end type

type dw_folder from u_folder within w_tipi_commessa
integer x = 23
integer y = 400
integer width = 2446
integer height = 1060
integer taborder = 10
end type

