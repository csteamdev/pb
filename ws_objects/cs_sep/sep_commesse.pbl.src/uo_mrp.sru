﻿$PBExportHeader$uo_mrp.sru
forward
global type uo_mrp from nonvisualobject
end type
end forward

global type uo_mrp from nonvisualobject
end type
global uo_mrp uo_mrp

type variables
boolean ib_verifica_avanz_fasi=false
boolean ib_data_fabbisogno_da_varianti=false


private:
	integer	ii_anno_reg_ord_ven
	long		il_num_reg_ord_ven, il_prog_riga_ord_ven


end variables

forward prototypes
public function integer uof_congela_distinta (string prodotto_finito, string cod_versione, long anno_registrazione, long num_registrazione, long prog_registrazione, string tabella_varianti, ref string fs_errore)
public function integer uof_disponibilita_alla_data (string fs_cod_prodotto, datetime fdt_data, string fs_cod_deposito, ref decimal fd_quantita, ref string fs_errore)
public function integer uof_mrp_prodotto (string fs_cod_prodotto, datetime fdt_data, string fs_cod_deposito, ref str_mrp lstr_mrp[], ref string fs_errore)
public function integer uof_calcolo_fabbisogni_comm (boolean fb_semilavorati, string fs_tabella_varianti, long fl_anno_documento, long fl_num_documento, long fl_riga_documento, string fs_cod_prodotto_padre, string fs_cod_versione_padre, decimal fdd_quan_utilizzo_padre, long fl_lead_time_padre, string fs_reparto_padre, datetime fdt_data_pronto_padre, ref long fl_lead_time_cumulato, ref s_fabbisogno_commessa s_fabbisogno[], ref string fs_errore)
public subroutine uof_set_riga_ord_ven (integer ai_anno_reg_ord_ven, long al_num_reg_ord_ven, long al_prog_riga_ord_ven)
public function integer uof_get_reparti_varianti (string as_cod_prodotto_padre, string as_cod_versione_padre, string as_cod_prodotto_figlio, string as_cod_versione_figlio, ref string as_reparti[])
public function integer uof_ricacolo_data_fabbisogno_commessa (long al_anno_commessa, long al_num_commessa, ref string as_messaggio)
end prototypes

public function integer uof_congela_distinta (string prodotto_finito, string cod_versione, long anno_registrazione, long num_registrazione, long prog_registrazione, string tabella_varianti, ref string fs_errore);string   ls_cod_prodotto_figlio[],ls_test,ls_cod_prodotto_variante, ls_sql, ls_flag_materia_prima[], &
		   ls_flag_materia_prima_variante,ls_errore,ls_flag_ramo_desrittivo_cu
long     ll_conteggio,ll_t,ll_max, ll_cont_figli, ll_cont_figli_integrazioni
integer  li_risposta
decimal  ldd_quantita_utilizzo[],ldd_quan_utilizzo_variante
declare cu_varianti dynamic cursor for sqlsa;
DynamicStagingArea sqlsb

long     ll_num_sequenza_cu, ll_test, ll_num_sequenza[]
string   ls_cod_prodotto_figlio_cu, ls_cod_versione_figlio_cu, ls_cod_misura_cu, ls_fase_ciclo_cu, ls_des_estesa_cu, ls_formula_tempo_cu, &
         ls_cod_formula_quan_utilizzo_cu, ls_flag_escludibile_cu, ls_flag_materia_prima_cu, ls_flag_arrotonda_bl_cu, ls_cod_versione_figlio[]
dec{4}   ld_quan_tecnica_cu, ld_quan_utilizzo_cu, ld_dim_x_cu, ld_dim_y_cu, ld_dim_z_cu, ld_dim_t_cu, ld_coef_calcolo_cu, ld_lead_time_cu


if isnull(prodotto_finito) or len(prodotto_finito) < 1 then
	fs_errore = "Manca l'indicazione del prodotto finito."
	return -1
end if

if isnull(cod_versione) or len(cod_versione) < 1 then
	fs_errore = "Al prodotto finito " + prodotto_finito + " manca l'indicazione del prodotto finito."
	return -1
end if


declare cu_distinta cursor for
select  num_sequenza,   
        cod_prodotto_figlio,             
        cod_versione_figlio,   
        cod_misura,   
        quan_tecnica,   
        quan_utilizzo,   
        fase_ciclo,   
        dim_x,   
        dim_y,   
        dim_z,   
        dim_t,   
        coef_calcolo,   
        des_estesa,   
        formula_tempo,   
        cod_formula_quan_utilizzo,   
        flag_escludibile,   
        flag_materia_prima,   
        flag_arrotonda_bl,   
        lead_time,
		  flag_ramo_descrittivo
from    distinta  
where   cod_azienda = :s_cs_xx.cod_azienda and
        cod_prodotto_padre = :prodotto_finito and
        cod_versione = :cod_versione;


open cu_distinta;
if sqlca.sqlcode < 0 then
	fs_errore = "Errore nell'operazione OPEN del cursore cu_distinta (f_congela_distinta)~r~n" + sqlca.sqlerrtext
	close cu_distinta;
	return -1
end if

ll_conteggio = 1

do while true

	fetch cu_distinta
	into  :ll_num_sequenza_cu,   
         :ls_cod_prodotto_figlio_cu,             
         :ls_cod_versione_figlio_cu,   
         :ls_cod_misura_cu,   
         :ld_quan_tecnica_cu,   
         :ld_quan_utilizzo_cu,   
         :ls_fase_ciclo_cu,   
         :ld_dim_x_cu,   
         :ld_dim_y_cu,   
         :ld_dim_z_cu,   
         :ld_dim_t_cu,   
         :ld_coef_calcolo_cu,   
         :ls_des_estesa_cu,   
         :ls_formula_tempo_cu,   
         :ls_cod_formula_quan_utilizzo_cu,   
         :ls_flag_escludibile_cu,   
         :ls_flag_materia_prima_cu,   
         :ls_flag_arrotonda_bl_cu,   
         :ld_lead_time_cu,
			:ls_flag_ramo_desrittivo_cu;

   if sqlca.sqlcode = 100 then 
		exit
	end if

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore nell'operazione FETCH del cursore cu_distinta (f_congela_distinta)~r~n" + sqlca.sqlerrtext
		close cu_distinta;
		return -1
	end if
	
	if ls_flag_ramo_desrittivo_cu = "S" then continue
	
	ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_figlio_cu
	ls_cod_versione_figlio[ll_conteggio] = ls_cod_versione_figlio_cu
	ll_num_sequenza[ll_conteggio] = ll_num_sequenza_cu
	ls_flag_materia_prima[ll_conteggio] = ls_flag_materia_prima_cu
	
	// *** controllo in che tabella devo andare a fare l'insert (solo se non esiste già la variante di quel tipo 
	//     inserita
	
	setnull(ll_test)
	
	choose case tabella_varianti
		case "varianti_commesse"
			
			select count(*)
			into   :ll_test
			from   varianti_commesse
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto_padre = :prodotto_finito and
					 cod_versione = :cod_versione and
					 num_sequenza = :ll_num_sequenza_cu and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio_cu and
					 cod_versione_figlio = :ls_cod_versione_figlio_cu and
					 anno_commessa = :anno_registrazione and
					 num_commessa = :num_registrazione and
					 cod_prodotto = :ls_cod_prodotto_figlio_cu;
					 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante il controllo esistenza varianti commessa: " + sqlca.sqlerrtext
				close cu_distinta;
				return -1
			end if
			
			if isnull(ll_test) or ll_test < 1 then
				
				// *** inserisco la variante per la commessa
				
				insert into varianti_commesse  
								( cod_azienda,   
								  anno_commessa,   
								  num_commessa,   
								  cod_prodotto_padre,   
								  num_sequenza,   
								  cod_prodotto_figlio,   
								  cod_versione,   
								  cod_versione_figlio,   
								  cod_prodotto,   
								  cod_versione_variante,
								  cod_misura,   
								  quan_tecnica,   
								  quan_utilizzo,   
								  dim_x,   
								  dim_y,   
								  dim_z,   
								  dim_t,   
								  coef_calcolo,   
								  flag_esclusione,   
								  formula_tempo,   
								  des_estesa,   
								  flag_materia_prima,   
								  lead_time )  
				values      ( :s_cs_xx.cod_azienda,   
								  :anno_registrazione,   
								  :num_registrazione,   
								  :prodotto_finito,   
								  :ll_num_sequenza_cu,   
								  :ls_cod_prodotto_figlio_cu,   
								  :cod_versione,   
								  :ls_cod_versione_figlio_cu,   
								  :ls_cod_prodotto_figlio_cu,  
								  :ls_cod_versione_figlio_cu,
								  :ls_cod_misura_cu,   
								  :ld_quan_tecnica_cu,   
								  :ld_quan_utilizzo_cu,   
								  :ld_dim_x_cu,   
								  :ld_dim_y_cu,   
								  :ld_dim_z_cu,   
								  :ld_dim_t_cu,   
								  :ld_coef_calcolo_cu,   
								  'N',   
								  :ls_formula_tempo_cu,   
								  :ls_des_estesa_cu,   
								  :ls_flag_materia_prima_cu,   
								  :ld_lead_time_cu )  ;
								  
				if sqlca.sqlcode <> 0 then
					fs_errore = "Errore durante l'inserimento della variante della commessa: " + sqlca.sqlerrtext
					close cu_distinta;
					return -1					
				end if

			end if
			
		case "varianti_det_ord_ven"
			
			select count(*)
			into   :ll_test
			from   varianti_det_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto_padre = :prodotto_finito and
					 cod_versione = :cod_versione and
					 num_sequenza = :ll_num_sequenza_cu and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio_cu and
					 cod_versione_figlio = :ls_cod_versione_figlio_cu and
					 anno_registrazione = :anno_registrazione and
					 num_registrazione = :num_registrazione and
					 prog_riga_ord_ven = :prog_registrazione and
					 cod_prodotto = :ls_cod_prodotto_figlio_cu;
					 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante il controllo esistenza varianti det ord ven: " + sqlca.sqlerrtext
				close cu_distinta;
				return -1
			end if
			
			if isnull(ll_test) or ll_test < 1 then
				
				// *** inserisco la variante per la commessa
				
				insert into varianti_det_ord_ven  
								( cod_azienda,   
								  anno_registrazione,   
								  num_registrazione,   
								  prog_riga_ord_ven,
								  cod_prodotto_padre,   
								  num_sequenza,   
								  cod_prodotto_figlio,   
								  cod_versione,   
								  cod_versione_figlio,   
								  cod_prodotto,
								  cod_versione_variante,
								  cod_misura,   
								  quan_tecnica,   
								  quan_utilizzo,   
								  dim_x,   
								  dim_y,   
								  dim_z,   
								  dim_t,   
								  coef_calcolo,   
								  flag_esclusione,   
								  formula_tempo,   
								  des_estesa,   
								  flag_materia_prima,   
								  lead_time )  
				values      ( :s_cs_xx.cod_azienda,   
								  :anno_registrazione,   
								  :num_registrazione,   
								  :prog_registrazione,
								  :prodotto_finito,   
								  :ll_num_sequenza_cu,   
								  :ls_cod_prodotto_figlio_cu,   
								  :cod_versione,   
								  :ls_cod_versione_figlio_cu,   
								  :ls_cod_prodotto_figlio_cu,   
								  :ls_cod_versione_figlio_cu,
								  :ls_cod_misura_cu,   
								  :ld_quan_tecnica_cu,   
								  :ld_quan_utilizzo_cu,   
								  :ld_dim_x_cu,   
								  :ld_dim_y_cu,   
								  :ld_dim_z_cu,   
								  :ld_dim_t_cu,   
								  :ld_coef_calcolo_cu,   
								  'N',   
								  :ls_formula_tempo_cu,   
								  :ls_des_estesa_cu,   
								  :ls_flag_materia_prima_cu,   
								  :ld_lead_time_cu )  ;
								  
				if sqlca.sqlcode <> 0 then
					fs_errore = "Errore durante l'inserimento della variante dell'ordine di vendita: " + sqlca.sqlerrtext
					close cu_distinta;
					return -1					
				end if

			end if
			
		case "varianti_det_off_ven"
			
			select count(*)
			into   :ll_test
			from   varianti_det_off_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto_padre = :prodotto_finito and
					 cod_versione = :cod_versione and
					 num_sequenza = :ll_num_sequenza_cu and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio_cu and
					 cod_versione_figlio = :ls_cod_versione_figlio_cu and
					 anno_registrazione = :anno_registrazione and
					 num_registrazione = :num_registrazione and
					 prog_riga_off_ven = :prog_registrazione and
					 cod_prodotto = :ls_cod_prodotto_figlio_cu;
					 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante il controllo esistenza varianti dell'offerta di vendita: " + sqlca.sqlerrtext
				close cu_distinta;
				return -1
			end if
			
			if isnull(ll_test) or ll_test < 1 then
				
				// *** inserisco la variante per la commessa
				
				insert into varianti_det_off_ven  
								( cod_azienda,   
								  anno_registrazione,   
								  num_registrazione,   
								  prog_riga_off_ven,
								  cod_prodotto_padre,   
								  num_sequenza,   
								  cod_prodotto_figlio,   
								  cod_versione,   
								  cod_versione_figlio,   
								  cod_prodotto,  
								  cod_versione_variante,
								  cod_misura,   
								  quan_tecnica,   
								  quan_utilizzo,   
								  dim_x,   
								  dim_y,   
								  dim_z,   
								  dim_t,   
								  coef_calcolo,   
								  flag_esclusione,   
								  formula_tempo,   
								  des_estesa,   
								  flag_materia_prima,   
								  lead_time )  
				values      ( :s_cs_xx.cod_azienda,   
								  :anno_registrazione,   
								  :num_registrazione,  
								  :prog_registrazione,
								  :prodotto_finito,   
								  :ll_num_sequenza_cu,   
								  :ls_cod_prodotto_figlio_cu,   
								  :cod_versione,   
								  :ls_cod_versione_figlio_cu,   
								  :ls_cod_prodotto_figlio_cu,  
								  :ls_cod_versione_figlio_cu,
								  :ls_cod_misura_cu,   
								  :ld_quan_tecnica_cu,   
								  :ld_quan_utilizzo_cu,   
								  :ld_dim_x_cu,   
								  :ld_dim_y_cu,   
								  :ld_dim_z_cu,   
								  :ld_dim_t_cu,   
								  :ld_coef_calcolo_cu,   
								  'N',   
								  :ls_formula_tempo_cu,   
								  :ls_des_estesa_cu,   
								  :ls_flag_materia_prima_cu,   
								  :ld_lead_time_cu )  ;
								  
				if sqlca.sqlcode <> 0 then
					fs_errore = "Errore durante l'inserimento della variante dell'offerta di vendita: " + sqlca.sqlerrtext
					close cu_distinta;
					return -1					
				end if

			end if
			
		case "varianti_det_trattative"
			
			select count(*)
			into   :ll_test
			from   varianti_det_trattative
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto_padre = :prodotto_finito and
					 cod_versione = :cod_versione and
					 num_sequenza = :ll_num_sequenza_cu and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio_cu and
					 cod_versione_figlio = :ls_cod_versione_figlio_cu and
					 anno_trattativa = :anno_registrazione and
					 num_trattativa = :num_registrazione and
					 prog_trattativa = :prog_registrazione and
					 cod_prodotto = :ls_cod_prodotto_figlio_cu;
					 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante il controllo esistenza varianti trattative: " + sqlca.sqlerrtext
				close cu_distinta;
				return -1
			end if
			
			if isnull(ll_test) or ll_test < 1 then
				
				// *** inserisco la variante per la commessa
				
				insert into varianti_det_trattative  
								( cod_azienda,   
								  anno_trattativa,   
								  num_trattativa,   
								  prog_trattativa,
								  cod_prodotto_padre,   
								  num_sequenza,   
								  cod_prodotto_figlio,   
								  cod_versione,   
								  cod_versione_figlio,   
								  cod_prodotto,  
								  cod_versione_variante,
								  cod_misura,   
								  quan_tecnica,   
								  quan_utilizzo,   
								  dim_x,   
								  dim_y,   
								  dim_z,   
								  dim_t,   
								  coef_calcolo,   
								  flag_esclusione,   
								  formula_tempo,   
								  des_estesa,   
								  flag_materia_prima,   
								  lead_time )  
				values      ( :s_cs_xx.cod_azienda,   
								  :anno_registrazione,   
								  :num_registrazione,   
								  :prog_registrazione,
								  :prodotto_finito,   
								  :ll_num_sequenza_cu,   
								  :ls_cod_prodotto_figlio_cu,   
								  :cod_versione,   
								  :ls_cod_versione_figlio_cu,   
								  :ls_cod_prodotto_figlio_cu,  
								  :ls_cod_versione_figlio_cu,
								  :ls_cod_misura_cu,   
								  :ld_quan_tecnica_cu,   
								  :ld_quan_utilizzo_cu,   
								  :ld_dim_x_cu,   
								  :ld_dim_y_cu,   
								  :ld_dim_z_cu,   
								  :ld_dim_t_cu,   
								  :ld_coef_calcolo_cu,   
								  'N',   
								  :ls_formula_tempo_cu,   
								  :ls_des_estesa_cu,   
								  :ls_flag_materia_prima_cu,   
								  :ld_lead_time_cu )  ;
								  
				if sqlca.sqlcode <> 0 then
					fs_errore = "Errore durante l'inserimento della variante della trattativa: " + sqlca.sqlerrtext
					close cu_distinta;
					return -1					
				end if

			end if
			
	end choose
	
	ll_conteggio ++
	
loop

close cu_distinta;
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore durante la chiusura del cursore cu_distinta (f_congela_distinta): " + sqlca.sqlerrtext
	return -1
end if

for ll_t = 1 to Upperbound(ls_cod_prodotto_figlio)

   if ls_flag_materia_prima[ll_t] = 'N' then
		
		ll_cont_figli = 0
		
		// controllo se esistono dei sotto-rami di distinta
		select count(*)
		into   :ll_cont_figli
		from   distinta  
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and    
				 cod_versione = :cod_versione;
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in ricerca figli del prodotto " + ls_cod_prodotto_figlio[ll_t] +  "~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		if ll_cont_figli > 0 and not isnull(ll_cont_figli) then
			
			li_risposta = uof_congela_distinta( ls_cod_prodotto_figlio[ll_t], &
			                                  ls_cod_versione_figlio[ll_t], & 
													    anno_registrazione, &
														 num_registrazione, &
														 prog_registrazione, &
														 tabella_varianti, &
														 ls_errore)
	
			if li_risposta = -1 then 
				fs_errore = ls_errore
				return -1
			end if
	
		end if
		
	end if

next

return 0
end function

public function integer uof_disponibilita_alla_data (string fs_cod_prodotto, datetime fdt_data, string fs_cod_deposito, ref decimal fd_quantita, ref string fs_errore);/*		Funzione di calcolo disponibilità assoluta su tutti i depositi
		dato un certo prodotto esamino giacenza/impegnato/assegnato/spedito/ordinato
		e restituisco in output il risultato. 
		
		In maniera molto semplice il problema è stato risolto impostando una vista specifica che 
		punta sui vari documenti.
		
*/

string				ls_chiave[], ls_where
dec{4} 			ld_quan_bolla, ld_quan_fattura, ld_quantita_giacenza[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_giacenza
long				ll_ret
uo_magazzino luo_magazzino

fd_quantita = 0

if isnull(fs_cod_deposito) or fs_cod_deposito = "" then

	SELECT sum(quantita)  
	 INTO 	:fd_quantita  
	 FROM 	view_giacenze  
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_prodotto = :fs_cod_prodotto AND  
				data_riferimento <= :fdt_data;
	
	if sqlca.sqlcode = -1 then
		fs_errore = "Errore in calcolo disponibilità (uof_disponibilita_alla_data).~r~n" + sqlca.sqlerrtext
		return -1
	end if

else
	
	SELECT sum(quantita)  
	 INTO 	:fd_quantita  
	 FROM 	view_giacenze_deposito
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_prodotto = :fs_cod_prodotto AND  
				data_riferimento <= :fdt_data and
				deposito = :fs_cod_deposito;
	
	if sqlca.sqlcode = -1 then
		fs_errore = "2.Errore in calcolo disponibilità (uof_disponibilita_alla_data).~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(fd_quantita) then 	fd_quantita = 0
	
	//	***	TROVO LA GIACENZA PER IL DEPOSITO

	for ll_ret = 1 to 14
		ld_quantita_giacenza[ll_ret] = 0
	next	
	
	luo_magazzino = CREATE uo_magazzino
		
	ll_ret = luo_magazzino.uof_saldo_prod_date_decimal(fs_cod_prodotto, fdt_data, ls_where, ld_quantita_giacenza, fs_errore, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])

	destroy luo_magazzino	
	
	for ll_ret = 1 to upperbound(ls_chiave)
		if ls_chiave[ll_ret] = fs_cod_deposito then
			ld_giacenza = ld_giacenza_stock[ll_ret]
		end if
	next	
	
	if isnull(ld_giacenza) then ld_giacenza = 0
	
	fd_quantita = ld_giacenza + fd_quantita
	
end if

if isnull(fd_quantita) then 	fd_quantita = 0

if isnull(fs_cod_deposito) or fs_cod_deposito = "" then

	select sum(quan_consegnata) * -1
	into   :ld_quan_bolla
	from   tes_bol_ven, det_bol_ven 
	where  (tes_bol_ven.cod_azienda = det_bol_ven.cod_azienda and
			 tes_bol_ven.anno_registrazione = det_bol_ven.anno_registrazione and
			 tes_bol_ven.num_registrazione = det_bol_ven.num_registrazione ) and
			 tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
			 tes_bol_ven.flag_movimenti = 'N' and
			 tes_bol_ven.flag_blocco = 'N' and anno_registrazione_mov_mag is null and
			 cod_prodotto = :fs_cod_prodotto and
			 cod_tipo_bol_ven in (select cod_tipo_bol_ven from tab_tipi_bol_ven where flag_tipo_bol_ven in( 'V','R') ) and
			 cod_tipo_det_ven in (select cod_tipo_det_ven from tab_tipi_det_ven where flag_tipo_det_ven = 'M');
	if sqlca.sqlcode = -1 then
		fs_errore = "Errore in calcolo disponibilità (uof_disponibilita_alla_data).~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ld_quan_bolla) then 	ld_quan_bolla = 0
	
	
	select sum(quan_fatturata) * -1
	into  :ld_quan_fattura
	from  tes_fat_ven, det_fat_ven 
	where (tes_fat_ven.cod_azienda = det_fat_ven.cod_azienda and
			tes_fat_ven.anno_registrazione = det_fat_ven.anno_registrazione and
			tes_fat_ven.num_registrazione = det_fat_ven.num_registrazione ) and
			tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
			tes_fat_ven.flag_movimenti = 'N' and
			tes_fat_ven.flag_blocco = 'N' and anno_registrazione_mov_mag is null and
			cod_prodotto = :fs_cod_prodotto and
			cod_tipo_fat_ven in (select cod_tipo_fat_ven from tab_tipi_fat_ven where flag_tipo_fat_ven = 'N') and
			cod_tipo_det_ven in (select cod_tipo_det_ven from tab_tipi_det_ven where flag_tipo_det_ven = 'M');
	if sqlca.sqlcode = -1 then
		fs_errore = "Errore in calcolo disponibilità (uof_disponibilita_alla_data).~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ld_quan_fattura) then 	ld_quan_fattura = 0
	
else
	
	select sum(quan_consegnata) * -1
	into   :ld_quan_bolla
	from   tes_bol_ven, det_bol_ven 
	where  (tes_bol_ven.cod_azienda = det_bol_ven.cod_azienda and
			 tes_bol_ven.anno_registrazione = det_bol_ven.anno_registrazione and
			 tes_bol_ven.num_registrazione = det_bol_ven.num_registrazione ) and
			 tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
			 tes_bol_ven.flag_movimenti = 'N' and
			 tes_bol_ven.flag_blocco = 'N' and anno_registrazione_mov_mag is null and
			 cod_prodotto = :fs_cod_prodotto and
			 cod_tipo_bol_ven in (select cod_tipo_bol_ven from tab_tipi_bol_ven where flag_tipo_bol_ven in( 'V','R') ) and
			 cod_tipo_det_ven in (select cod_tipo_det_ven from tab_tipi_det_ven where flag_tipo_det_ven = 'M') and
			 det_bol_ven.cod_deposito = :fs_cod_deposito;
	if sqlca.sqlcode = -1 then
		fs_errore = "2.Errore in calcolo disponibilità (uof_disponibilita_alla_data).~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ld_quan_bolla) then 	ld_quan_bolla = 0
	
	
	select sum(quan_fatturata) * -1
	into  :ld_quan_fattura
	from  tes_fat_ven, det_fat_ven 
	where (tes_fat_ven.cod_azienda = det_fat_ven.cod_azienda and
			tes_fat_ven.anno_registrazione = det_fat_ven.anno_registrazione and
			tes_fat_ven.num_registrazione = det_fat_ven.num_registrazione ) and
			tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
			tes_fat_ven.flag_movimenti = 'N' and
			tes_fat_ven.flag_blocco = 'N' and anno_registrazione_mov_mag is null and
			cod_prodotto = :fs_cod_prodotto and
			cod_tipo_fat_ven in (select cod_tipo_fat_ven from tab_tipi_fat_ven where flag_tipo_fat_ven = 'N') and
			cod_tipo_det_ven in (select cod_tipo_det_ven from tab_tipi_det_ven where flag_tipo_det_ven = 'M') and
			det_fat_ven.cod_deposito = :fs_cod_deposito;
	if sqlca.sqlcode = -1 then
		fs_errore = "2.Errore in calcolo disponibilità (uof_disponibilita_alla_data).~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ld_quan_fattura) then 	ld_quan_fattura = 0	
	
end if


fd_quantita += ld_quan_bolla + ld_quan_fattura

return 0
end function

public function integer uof_mrp_prodotto (string fs_cod_prodotto, datetime fdt_data, string fs_cod_deposito, ref str_mrp lstr_mrp[], ref string fs_errore);/*		Funzione di calcolo disponibilità assoluta su tutti i depositi
		dato un certo prodotto esamino giacenza/impegnato/assegnato/spedito/ordinato
		e restituisco in output il risultato. 
		
		In maniera molto semplice il problema è stato risolto impostando una vista specifica che 
		punta sui vari documenti.
		
*/

string			ls_chiave[], ls_where, ls_sql
dec{4} 			ld_quan_bolla, ld_quan_fattura, ld_quantita_giacenza[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_giacenza
long				ll_ret, ll_index_mrp
dec{4}			fd_quantita, ld_quan_mrp
datetime			ldt_data_rif_mrp, ldt_data_vuota
uo_magazzino luo_magazzino

DECLARE cu_mrp_prodotto DYNAMIC CURSOR FOR SQLSA ;

ll_index_mrp = 0
ldt_data_vuota = datetime(date("01/01/1900"),time("00:00"))

//	***	TROVO LA GIACENZA PER IL DEPOSITO

for ll_ret = 1 to 14
	ld_quantita_giacenza[ll_ret] = 0
next	

luo_magazzino = CREATE uo_magazzino
	
ll_ret = luo_magazzino.uof_saldo_prod_date_decimal(fs_cod_prodotto, fdt_data, ls_where, ld_quantita_giacenza, fs_errore, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])

destroy luo_magazzino	

//Donato 31/07/2008
//la somma delle qta in giacenza, nel caso di tutti gli stock deve essere incrementale
ld_giacenza = 0

for ll_ret = 1 to upperbound(ls_chiave)
		// se inizialmente avevo chiesto un deposito, calcolo la giacenza solo di quello
	if not isnull(fs_cod_deposito) and len(fs_cod_deposito) > 0 then
		if ls_chiave[ll_ret] = fs_cod_deposito then			
			ld_giacenza = ld_giacenza_stock[ll_ret]			
		end if
	else
		//altrimento sommo tutti gli stock in modo da avere la giacenza di tutti i depositi
		
		//modificato da Donato da = a += per incrementare le varie giacenze degli stock
		//ld_giacenza = ld_giacenza_stock[ll_ret]		
		ld_giacenza += ld_giacenza_stock[ll_ret]
	end if
next	

if isnull(ld_giacenza) then ld_giacenza = 0

ll_index_mrp ++

lstr_mrp[ll_index_mrp].data = ldt_data_vuota
lstr_mrp[ll_index_mrp].quan_progressiva = ld_giacenza

// vado ad analizzare tutti movimenti MRP

ls_sql = " SELECT data_riferimento, sum(quantita) FROM view_giacenze_deposito " + &
			" WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
			" and cod_prodotto = '" + fs_cod_prodotto + "' "
//			" and data_riferimento <= '" + string(fdt_data, s_cs_xx.db_funzioni.formato_data) + "'"

if not isnull(fs_cod_deposito) and len(fs_cod_deposito) > 0 then
	ls_sql += " and deposito ='" + fs_cod_deposito + "'"
end if

ls_sql += " group by data_riferimento "
ls_sql += " order by data_riferimento ASC "

PREPARE SQLSA FROM :ls_sql ;

OPEN DYNAMIC cu_mrp_prodotto;
if sqlca.sqlcode < 0 then
	fs_errore = "ERRORE IN OPEN CURSORE cu_mrp_prodotto~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_mrp_prodotto into :ldt_data_rif_mrp, :ld_quan_mrp; 
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		fs_errore = "ERRORE IN FETCH CURSORE cu_mrp_prodotto~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if ld_quan_mrp = 0 then continue
	
	ll_index_mrp ++
	
	lstr_mrp[ll_index_mrp].data = ldt_data_rif_mrp
	lstr_mrp[ll_index_mrp].quan_progressiva = lstr_mrp[ll_index_mrp - 1].quan_progressiva + ld_quan_mrp
	
loop

close cu_mrp_prodotto;

return 0
end function

public function integer uof_calcolo_fabbisogni_comm (boolean fb_semilavorati, string fs_tabella_varianti, long fl_anno_documento, long fl_num_documento, long fl_riga_documento, string fs_cod_prodotto_padre, string fs_cod_versione_padre, decimal fdd_quan_utilizzo_padre, long fl_lead_time_padre, string fs_reparto_padre, datetime fdt_data_pronto_padre, ref long fl_lead_time_cumulato, ref s_fabbisogno_commessa s_fabbisogno[], ref string fs_errore);/*
		FUNZIONE CALCOLO DEL FABBISOGNO PER DATA DI UNA CERTA COMMESSA UTILIZZANDO LE VARIANTI DELLA COMMESSA.
		QUESTA FUNZIONE SCORRE LA DISTINTA COLLEGATA ALLA COMMESSA, ED ESTRAE 
		LA QUANTITA NECESSARIA (FABBISOGNO) PER OGNI MATERIA PRIMA NELLA DATA.
		LA DATA VIENE ESTRATTA CON LA TECNICA DEL LEAD TIME PRESENTE IN ANAGRAFICA
		PRODOTTI E IN DISTINTA.
*/

boolean		lb_presenza_fase_ext

string			ls_cod_prodotto_figlio[],ls_cod_versione_figlio[], ls_test,ls_cod_prodotto_variante, ls_cod_versione_variante, &
				ls_sql, ls_flag_materia_prima[], ls_flag_materia_prima_variante,ls_errore,ls_flag_esterna, &
				ls_flag_ramo_descrittivo[], ls_cod_reparto, ls_rep[]
				
long			ll_conteggio,ll_t,ll_max, ll_cont_figli, ll_cont_figli_integrazioni, ll_lead_time[], ll_cont_fasi, ll_num_sequenza,&
				ll_lead_time_max, ll_lead_time_variante,ll_lead_time_cumulato,ll_lead_time_prodotto, ll_lead_sm
				
integer		li_risposta

decimal 		ldd_quantita_utilizzo[],ldd_quan_utilizzo_variante,ld_quan_in_produzione,ld_quan_prodotta

datetime		ldt_data_pronto



declare cu_varianti dynamic cursor for sqlsa;
DynamicStagingArea sqlsb

if isnull(fs_cod_prodotto_padre) or len(fs_cod_prodotto_padre) < 1 then
	fs_errore = "Manca l'indicazione del prodotto padre."
	return -1
end if

if isnull(fs_cod_versione_padre) or len(fs_cod_versione_padre) < 1 then
	fs_errore = "Al prodotto finito " + fs_cod_prodotto_padre + " manca l'indicazione della versione."
	return -1
end if

sqlsb = CREATE DynamicStagingArea
ll_conteggio = 1
ll_lead_time_max=0

//declare righe_distinta_3 cursor for 
declare righe_distinta_3 dynamic cursor for sqlsb;

ls_sql = "select  cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, lead_time, flag_ramo_descrittivo from distinta " + &
         " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
			" and     cod_prodotto_padre = '" + fs_cod_prodotto_padre + "' " + &
			" and     cod_versione= '" + fs_cod_versione_padre + "' " 

// sono in un documento, quindi potrebbero esserci integrazioni
if not isnull(fl_anno_documento) and not isnull(fl_num_documento) then
		choose case fs_tabella_varianti
				
			case "varianti_commesse"
				
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, lead_time, 'N' " + &
							 " FROM integrazioni_commessa " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_commessa = " + string(fl_anno_documento)  + &
							 " and     num_commessa = "  + string(fl_num_documento) + &
							 " and     cod_prodotto_padre = '" + fs_cod_prodotto_padre + "' "
				
			case "varianti_det_off_ven"
				
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, lead_time, 'N' " + &
							 " FROM integrazioni_det_off_ven " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_registrazione = " + string(fl_anno_documento)  + &
							 " and     num_registrazione = "  + string(fl_num_documento) + &
							 " and     prog_riga_off_ven = "  + string(fl_riga_documento) + &
							 " and     cod_prodotto_padre = '" + fs_cod_prodotto_padre + "' "
				
				
			case "varianti_det_ord_ven"
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, lead_time, 'N' " + &
							 " FROM integrazioni_det_ord_ven " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_registrazione = " + string(fl_anno_documento)  + &
							 " and     num_registrazione = "  + string(fl_num_documento) + &
							 " and     prog_riga_ord_ven = "  + string(fl_riga_documento) + &
							 " and     cod_prodotto_padre = '" + fs_cod_prodotto_padre + "' "
				
			case "varianti_det_trattative"
				
				ls_sql += " union all " + &
				          " SELECT  cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, lead_time, 'N' " + &
							 " FROM    integrazioni_det_trattative " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_trattativa = " + string(fl_anno_documento)  + &
							 " and     num_trattativa = "  + string(fl_num_documento) + &
							 " and     prog_trattativa = "  + string(fl_riga_documento) + &
							 " and     cod_prodotto_padre = '" + fs_cod_prodotto_padre + "' "
				
		end choose
end if

prepare sqlsb from :ls_sql;

open dynamic righe_distinta_3;
if sqlca.sqlcode < 0 then
	fs_errore = "Errore nell'operazione OPEN del cursore righe_distinta_3 (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
	close righe_distinta_3;
	return -1
end if

do while true

	fetch righe_distinta_3 
	into  :ls_cod_prodotto_figlio[ll_conteggio],
		   :ls_cod_versione_figlio[ll_conteggio],
		   :ldd_quantita_utilizzo[ll_conteggio],
			:ls_flag_materia_prima[ll_conteggio],
			:ll_lead_time[ll_conteggio],
			:ls_flag_ramo_descrittivo[ll_conteggio];

   if sqlca.sqlcode = 100 then 
		close righe_distinta_3;	
		exit
	end if
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore nell'operazione FETCH del cursore righe_distinta_3 (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
		close righe_distinta_3;
		return -1
	end if
	
	// ramo descrittivo; salto tutto EnMe 9/11/2006
	if ls_flag_ramo_descrittivo[ll_conteggio] = "S" then continue
	
	// se il lead time non è stato impostato vado a prendere il default dal prodotto
	if ll_lead_time[ll_conteggio] = 0 or isnull(ll_lead_time[ll_conteggio]) then
		select lead_time
		into   :ll_lead_time_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_figlio[ll_conteggio];
				 
		if sqlca.sqlcode = 0 then 
			if isnull(ll_lead_time_prodotto) then ll_lead_time_prodotto = 0
			ll_lead_time[ll_conteggio] = ll_lead_time_prodotto
		end if
		
	end if
	
	// verifico se mi trovo in presenza di un documento e quindi di una variante
	if not isnull(fl_anno_documento) and not isnull(fl_num_documento) then
		
		ls_sql = "select quan_utilizzo, " + &
					" cod_prodotto, " + &
					" cod_versione_figlio, " + &
					" flag_materia_prima, " + &
					" lead_time " + &
					" from " + fs_tabella_varianti + &
					" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
		choose case fs_tabella_varianti
			case "varianti_commesse"
					ls_sql = ls_sql + " and anno_commessa=" + string(fl_anno_documento) + &
											" and num_commessa=" + string(fl_num_documento) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_off_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_documento) + &
											" and num_registrazione=" + string(fl_num_documento) + &
											" and prog_riga_off_ven=" + string(fl_riga_documento) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_ord_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_documento) + &
											" and num_registrazione=" + string(fl_num_documento) + &
											" and prog_riga_ord_ven=" + string(fl_riga_documento) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_trattative"
					ls_sql = ls_sql + " and anno_trattativa=" + string(fl_anno_documento) + &
											" and num_trattativa=" + string(fl_num_documento) + &
											" and prog_trattativa=" + string(fl_riga_documento) + &
											" and flag_esclusione<>'S'"
		end choose
		ls_sql = ls_sql + " and cod_prodotto_padre='" + fs_cod_prodotto_padre + "'" + &
								" and cod_versione='" + fs_cod_versione_padre + "'" + &
								" and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_conteggio] + "'" + &
								" and cod_versione_figlio='" + ls_cod_versione_figlio[ll_conteggio] + "'"
		
		prepare sqlsa from :ls_sql;
		
		open dynamic cu_varianti;
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore nell'operazione OPEN del cursore cu_varianti (uof_calcolo_fabbisogni_comm)~r~n" + sqlca.sqlerrtext
			return -1
		end if

		fetch cu_varianti 
		into :ldd_quan_utilizzo_variante, 
			  :ls_cod_prodotto_variante,
			  :ls_cod_versione_variante,
			  :ls_flag_materia_prima_variante,
			  :ll_lead_time_variante;
				
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore nell'operazione FETCH del cursore cu_varianti (uof_calcolo_fabbisogni_comm)~r~n" + sqlca.sqlerrtext
			close cu_varianti;
			return -1
		end if

		if sqlca.sqlcode <> 100  then
			ldd_quantita_utilizzo[ll_conteggio]=ldd_quan_utilizzo_variante
			ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
			ls_cod_versione_figlio[ll_conteggio] = ls_cod_versione_variante
			ls_flag_materia_prima[ll_conteggio] = ls_flag_materia_prima_variante
			ll_lead_time[ll_conteggio] = ll_lead_time_variante
			
			// se il lead time non è stato impostato vado a prendere il default dal prodotto
			if ll_lead_time_variante = 0 or isnull(ll_lead_time_variante) then
				select lead_time
				into   :ll_lead_time_prodotto
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_prodotto = :ls_cod_prodotto_variante;
						 
				if sqlca.sqlcode = 0 then 
					if isnull(ll_lead_time_prodotto) then ll_lead_time_prodotto = 0
					ll_lead_time[ll_conteggio] = ll_lead_time_prodotto
				end if
			end if
			
		end if
	
		close cu_varianti;
	end if

	ldd_quantita_utilizzo[ll_conteggio] = ldd_quantita_utilizzo[ll_conteggio] * fdd_quan_utilizzo_padre
	if ll_lead_time[ll_conteggio] > ll_lead_time_max then ll_lead_time_max = ll_lead_time[ll_conteggio]
	ll_lead_time[ll_conteggio] = ll_lead_time[ll_conteggio] + fl_lead_time_padre
	ll_conteggio++	
loop

close righe_distinta_3;

for ll_t = 1 to ll_conteggio -1

   if ls_flag_materia_prima[ll_t] = 'N' then
		
		ll_cont_figli = 0
		
		// controllo se esistono dei sotto-rami di distinta
		SELECT count(*)
		into   :ll_cont_figli
		FROM   distinta  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda
		AND    cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t]
		and    cod_versione=:ls_cod_versione_figlio[ll_t];
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in ricerca figli del prodotto:"+ls_cod_prodotto_figlio[ll_t]+ " Versione:"+ls_cod_versione_figlio[ll_t]+"~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		// sono in un documento, quindi potrebbero esserci integrazioni
		if not isnull(fl_anno_documento) and not isnull(fl_num_documento) then
			
				choose case fs_tabella_varianti
						
					case "varianti_commesse"
						
						ll_cont_figli_integrazioni = 0
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_commessa
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_commessa = :fl_anno_documento and
								 num_commessa = :fl_num_documento and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto:"+ls_cod_prodotto_figlio[ll_t]+ " Versione:"+ls_cod_versione_figlio[ll_t]+"~r~n" + sqlca.sqlerrtext
							return -1
						end if
						
					case "varianti_det_off_ven"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_off_ven
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :fl_anno_documento and
								 num_registrazione = :fl_num_documento and
								 prog_riga_off_ven = :fl_riga_documento and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto:"+ls_cod_prodotto_figlio[ll_t]+ " Versione:"+ls_cod_versione_figlio[ll_t]+"~r~n" + sqlca.sqlerrtext
							return -1
						end if
					
					case "varianti_det_ord_ven"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_ord_ven
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :fl_anno_documento and
								 num_registrazione = :fl_num_documento and
								 prog_riga_ord_ven = :fl_riga_documento and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto:"+ls_cod_prodotto_figlio[ll_t]+ " Versione:"+ls_cod_versione_figlio[ll_t]+"~r~n" + sqlca.sqlerrtext
							return -1
						end if
					
					case "varianti_det_trattative"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_trattative
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_trattativa = :fl_anno_documento and
								 num_trattativa  = :fl_num_documento and
								 prog_trattativa    = :fl_riga_documento and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto:"+ls_cod_prodotto_figlio[ll_t]+ " Versione:"+ls_cod_versione_figlio[ll_t]+"~r~n" + sqlca.sqlerrtext
							return -1
						end if
					
				end choose
		end if
		
		ll_cont_figli = ll_cont_figli + ll_cont_figli_integrazioni

		// non ci sono figli in distinta, verifico la variante 
		// (quindi non può essere neanche una integrazione, visto che non è ammessa la variante di una integrazione)
		
		// se non ci sono figli (materia prima) e se devo portare fuori anche i semilavorati ....
		// porto fuori i semilav solo se c'è collegata una fase esterna.
		
		lb_presenza_fase_ext = false
		
		if fb_semilavorati then
			select flag_esterna
			into   :ls_flag_esterna
			from   tes_fasi_lavorazione
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_figlio[ll_t] and
					 cod_versione = :ls_cod_versione_figlio[ll_t];
					 
			if isnull(ls_flag_esterna) then ls_flag_esterna = "N"
			if sqlca.sqlcode = 0 and ls_flag_esterna = "S" then lb_presenza_fase_ext = true
		end if					 
			
		if ll_cont_figli < 1 or fb_semilavorati then
			
			ll_max=upperbound(s_fabbisogno)
			ll_max++		

			if ll_cont_figli < 1 then
				s_fabbisogno[ll_max].semilavorato = false
			else
				s_fabbisogno[ll_max].semilavorato = true
			end if
			s_fabbisogno[ll_max].cod_prodotto = ls_cod_prodotto_figlio[ll_t]
			s_fabbisogno[ll_max].cod_versione = ls_cod_versione_figlio[ll_t]
			s_fabbisogno[ll_max].quan_fabbisogno = round(ldd_quantita_utilizzo[ll_t],4)
			s_fabbisogno[ll_max].lead_time = ll_lead_time[ll_t]
			s_fabbisogno[ll_max].flag_mp = "N"
			if lb_presenza_fase_ext then
				s_fabbisogno[ll_max].flag_ordine_servizio = "S"
			else
				s_fabbisogno[ll_max].flag_ordine_servizio = "N"
			end if
			s_fabbisogno[ll_max].cod_reparto = fs_reparto_padre
			s_fabbisogno[ll_max].data_fabbisogno = fdt_data_pronto_padre
				
			if not isnull(fl_anno_documento) and not isnull(fl_num_documento) then
				ls_sql = "select quan_utilizzo, " + &
							" cod_prodotto, " + &
							" cod_versione, " + &
							" num_sequenza, " + &
							" lead_time " + &
							" from " + fs_tabella_varianti + &
							" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
				choose case fs_tabella_varianti
					case "varianti_commesse"
							ls_sql = ls_sql + " and anno_commessa=" + string(fl_anno_documento) + &
													" and num_commessa=" + string(fl_num_documento) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_off_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_documento) + &
													" and num_registrazione=" + string(fl_num_documento) + &
													" and prog_riga_off_ven=" + string(fl_riga_documento) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_ord_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_documento) + &
													" and num_registrazione=" + string(fl_num_documento)  + &
													" and prog_riga_ord_ven=" + string(fl_riga_documento) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_trattative"
							ls_sql = ls_sql + " and anno_trattativa=" + string(fl_anno_documento) + &
													" and num_trattativa=" + string(fl_num_documento) + &
													" and prog_trattativa=" + string(fl_riga_documento) + &
													" and flag_esclusione<>'S'"
				end choose
				ls_sql = ls_sql + " and    cod_prodotto_padre='" + fs_cod_prodotto_padre + "'" + &
										" and    cod_versione = '" + fs_cod_versione_padre + "'" + &
										" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
										" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t]+ "'"    // + "';"
				
				prepare sqlsa from :ls_sql;
	
				open dynamic cu_varianti;
				if sqlca.sqlcode < 0 then
					fs_errore = "Errore nell'operazione OPEN del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
					close cu_varianti;
					return -1
				end if
	
				fetch cu_varianti 
				into  :ldd_quan_utilizzo_variante, 
						:ls_cod_prodotto_variante,
						:ls_cod_versione_variante,
						:ll_num_sequenza,
						:ll_lead_time_variante;
			
				
				if sqlca.sqlcode < 0 then
					fs_errore = "Errore nell'operazione FETCH del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
					close cu_varianti;
					return -1
				end if
			
				if sqlca.sqlcode <> 100  then
					ldd_quan_utilizzo_variante = ldd_quan_utilizzo_variante * fdd_quan_utilizzo_padre
					ldd_quantita_utilizzo[ll_t] = ldd_quan_utilizzo_variante
					ls_cod_prodotto_figlio[ll_t] = ls_cod_prodotto_variante
					s_fabbisogno[ll_max].cod_prodotto = ls_cod_prodotto_variante
					s_fabbisogno[ll_max].cod_versione = ls_cod_versione_variante
					s_fabbisogno[ll_max].quan_fabbisogno = round(ldd_quan_utilizzo_variante,4)
					s_fabbisogno[ll_max].lead_time = ll_lead_time_variante
					s_fabbisogno[ll_max].cod_reparto = fs_reparto_padre
					s_fabbisogno[ll_max].data_fabbisogno = fdt_data_pronto_padre
				
					
					// se il lead time non è stato impostato vado a prendere il default dal prodotto
					if ll_lead_time_variante = 0 or isnull(ll_lead_time_variante) then
						select lead_time
						into   :ll_lead_time_prodotto
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto_variante;
								 
						if sqlca.sqlcode = 0 then 
							if isnull(ll_lead_time_prodotto) then ll_lead_time_prodotto = 0
							s_fabbisogno[ll_max].lead_time = ll_lead_time_prodotto
							ll_lead_time_variante = ll_lead_time_prodotto
						end if
					end if
					
					if ll_lead_time_variante > ll_lead_time_max then ll_lead_time_max = ll_lead_time_variante
					
				end if
	
				close cu_varianti;
			end if
			
		end if
		
		if ll_cont_figli > 0 then
			// presenza di figli sotto al prodotto corrente; procedo con la ricorsione
			
			ll_lead_time_cumulato = 0
			
			if ll_max < 1 or isnull(ll_max) then
				ll_lead_sm = 0
			else
				ll_lead_sm = s_fabbisogno[ll_max].lead_time
			end if
			
			
			if fs_tabella_varianti = "varianti_commesse" and ib_verifica_avanz_fasi then
				// se si tratta di commessa, verifico lo stato di avanzamento, escludendo dal fabbisogno le fasi chiuse (se FASI AVANZAMENTO)
				ll_cont_figli = 0
				
				select count(*)
				into   :ll_cont_fasi
				from   avan_produzione_com
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_commessa = :fl_anno_documento and
						 num_commessa = :fl_num_documento and
						 cod_prodotto = :ls_cod_prodotto_figlio[ll_t] and
						 cod_versione = :ls_cod_versione_figlio[ll_t];
						 
				if ll_cont_fasi > 0 then
					
					ld_quan_in_produzione = 0
					ld_quan_prodotta      = 0
				
					select sum(quan_in_produzione), sum(quan_prodotta)
					into   :ld_quan_in_produzione,  :ld_quan_prodotta
					from   avan_produzione_com
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 anno_commessa = :fl_anno_documento and
							 num_commessa = :fl_num_documento and
							 cod_prodotto = :ls_cod_prodotto_figlio[ll_t] and
							 cod_versione = :ls_cod_versione_figlio[ll_t] and
							 flag_blocco = 'N' and
							 flag_fine_fase = 'N';
					if sqlca.sqlcode < 0 then
						fs_errore = "Errore nel controllo della fase di avanzamento~r~n" + sqlca.sqlerrtext
						close cu_varianti;
						return -1
					end if
					if isnull(ld_quan_in_produzione) then ld_quan_in_produzione = 0
					if isnull(ld_quan_prodotta) then ld_quan_prodotta = 0
							 
					ldd_quantita_utilizzo[ll_t] = ld_quan_in_produzione - ld_quan_prodotta
					
				end if
					
			end if
			
			// estraggo e memorizzo il reparto e la data di fabbisogno del reparto (X MRP gibus 25/06/2013)
			ls_cod_reparto = fs_reparto_padre
			ldt_data_pronto = fdt_data_pronto_padre 
			
			if ib_data_fabbisogno_da_varianti and fs_tabella_varianti = "varianti_det_ord_ven" then
					
				select 	varianti_det_ord_ven_prod.cod_reparto,
							varianti_det_ord_ven_prod.data_pronto
				into 		:ls_cod_reparto,
							:ldt_data_pronto
				from 		varianti_det_ord_ven_prod join
							anag_reparti on 	anag_reparti.cod_azienda = varianti_det_ord_ven_prod.cod_azienda and
													anag_reparti.cod_reparto = varianti_det_ord_ven_prod.cod_reparto
				where 	varianti_det_ord_ven_prod.cod_azienda = :s_cs_xx.cod_azienda and
							varianti_det_ord_ven_prod.anno_registrazione = :fl_anno_documento and
							varianti_det_ord_ven_prod.num_registrazione = :fl_num_documento and
							varianti_det_ord_ven_prod.prog_riga_ord_ven = :fl_riga_documento and
							varianti_det_ord_ven_prod.cod_prodotto_padre = :fs_cod_prodotto_padre and
							varianti_det_ord_ven_prod.cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_t] and
							varianti_det_ord_ven_prod.cod_versione = :fs_cod_versione_padre and
							varianti_det_ord_ven_prod.cod_versione_figlio = :ls_cod_versione_figlio[ll_t] and
							anag_reparti.flag_mrp = 'S';
						
				if sqlca.sqlcode < 0 then
					fs_errore = "Errore in selezione del reparto a cui assegnare impegnato per MRP~r~n" + sqlca.sqlerrtext
					close cu_varianti;
					return -1
				end if
			
				if sqlca.sqlcode = 100 then
					// non ho trovato nulla quindi mi riprendo quelli del padre
					ls_cod_reparto = fs_reparto_padre
					ldt_data_pronto = fdt_data_pronto_padre 
				end if
			end if
			// fine modifica 25/06/2013 gibus
			
			//#############################################################################################
			//SR impegnato_commessa
			if fs_tabella_varianti = "varianti_commesse" and ii_anno_reg_ord_ven>0 and il_num_reg_ord_ven>0 and il_prog_riga_ord_ven>0 then
				
				//è una commessa da ordine di vendita
				//leggo reparto dalla corrispondente variante della riga ordine di vendita
				if uof_get_reparti_varianti(fs_cod_prodotto_padre, fs_cod_versione_padre, ls_cod_prodotto_figlio[ll_t], ls_cod_versione_figlio[ll_t], ls_rep[]) > 0 then
					// se ce n'è più di uno prendo il primo
					ls_cod_reparto = ls_rep[1]
				else
					//non ho trovato nulla quindi mi riprendo quelli del padre
					ls_cod_reparto = fs_reparto_padre
					
//					//Non ho trovato nulla. Se c'è una condizione iniziale impongo quella, altrimenti mi riprendo quelli del padre
//					if is_cod_reparto_iniziale<>"" and not isnull(is_cod_reparto_iniziale) then
//						ls_cod_reparto = is_cod_reparto_iniziale
//						
//						//ora posso azzerare la condizione iniziale
//						setnull(is_cod_reparto_iniziale)
//					else
//						//non ho trovato nulla quindi mi riprendo quelli del padre
//						ls_cod_reparto = fs_reparto_padre
//					end if
				end if
			end if
			//#############################################################################################
			
			uof_calcolo_fabbisogni_comm(fb_semilavorati, fs_tabella_varianti, &
			                            fl_anno_documento, fl_num_documento,fl_riga_documento, &
												 ls_cod_prodotto_figlio[ll_t], ls_cod_versione_figlio[ll_t], round(ldd_quantita_utilizzo[ll_t],4), &
												 ll_lead_sm, &
												 ls_cod_reparto, &
												 ldt_data_pronto,&
												 ref ll_lead_time_cumulato, &
												 ref s_fabbisogno[], &
												 ref fs_errore)
	
			if li_risposta = -1 then 
				fs_errore = ls_errore
				return -1
			end if
			
// 		sommo il lead time dello strato precedente
			
			if ll_lead_time_cumulato > fl_lead_time_cumulato then fl_lead_time_cumulato = ll_lead_time_cumulato
			
//			s_fabbisogno[ll_max].lead_time += ll_lead_time_cumulato
			if ll_max > 0  and not isnull(ll_max) then
				if s_fabbisogno[ll_max].lead_time > ll_lead_time_max then ll_lead_time_max = s_fabbisogno[ll_max].lead_time
			end if
	
		end if
		
	else
		// si tratta di una materia prima forzata (flag_materia_prima=S)
		
		ll_max=upperbound(s_fabbisogno)
		ll_max++		
		s_fabbisogno[ll_max].semilavorato = false
		s_fabbisogno[ll_max].cod_prodotto = ls_cod_prodotto_figlio[ll_t]
		s_fabbisogno[ll_max].cod_versione = ls_cod_versione_figlio[ll_t]
		s_fabbisogno[ll_max].quan_fabbisogno = round(ldd_quantita_utilizzo[ll_t],4)
		s_fabbisogno[ll_max].lead_time = ll_lead_time[ll_t]
		s_fabbisogno[ll_max].flag_mp = "S"
		s_fabbisogno[ll_max].flag_ordine_servizio = "N"
		s_fabbisogno[ll_max].cod_reparto = fs_reparto_padre
		s_fabbisogno[ll_max].data_fabbisogno = fdt_data_pronto_padre
		
		if not isnull(fl_anno_documento) and not isnull(fl_num_documento) then
			ls_sql = "select quan_utilizzo, " + &
						" cod_prodotto, " + &
						" cod_versione, " + &
						" lead_time " + &
						" from " + fs_tabella_varianti + &
						" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
			choose case fs_tabella_varianti
				case "varianti_commesse"
						ls_sql = ls_sql + " and anno_commessa=" + string(fl_anno_documento) + &
												" and num_commessa=" + string(fl_num_documento) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_off_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_documento) + &
												" and num_registrazione=" + string(fl_num_documento) + &
												" and prog_riga_off_ven=" + string(fl_riga_documento) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_ord_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_documento) + &
												" and num_registrazione=" + string(fl_num_documento)  + &
												" and prog_riga_ord_ven=" + string(fl_riga_documento) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_trattative"
						ls_sql = ls_sql + " and anno_trattativa=" + string(fl_anno_documento) + &
												" and num_trattativa=" + string(fl_num_documento) + &
												" and prog_trattativa=" + string(fl_riga_documento) + &
												" and flag_esclusione<>'S'"
			end choose
			ls_sql = ls_sql + " and    cod_prodotto_padre = '" + fs_cod_prodotto_padre + "'" + &
									" and    cod_versione ='" + fs_cod_versione_padre + "'" + &
									" and    cod_prodotto_figlio ='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
									" and    cod_versione_figlio ='" + ls_cod_versione_figlio[ll_t] + "'"  
			
			prepare sqlsa from :ls_sql;

			open dynamic cu_varianti;
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore nell'operazione OPEN del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
				close cu_varianti;
				return -1
			end if

			fetch cu_varianti 
			into  :ldd_quan_utilizzo_variante, 
					:ls_cod_prodotto_variante,
					:ls_cod_versione_variante,
					:ll_lead_time_variante;
		
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore nell'operazione FETCH del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
				close cu_varianti;
				return -1
			end if
		
			if sqlca.sqlcode <> 100  then
				ldd_quan_utilizzo_variante   = ldd_quan_utilizzo_variante * fdd_quan_utilizzo_padre
				ldd_quantita_utilizzo[ll_t]  = ldd_quan_utilizzo_variante
				ls_cod_prodotto_figlio[ll_t] = ls_cod_prodotto_variante
				s_fabbisogno[ll_max].cod_prodotto = ls_cod_prodotto_variante
				s_fabbisogno[ll_max].cod_versione = ls_cod_versione_variante
				s_fabbisogno[ll_max].quan_fabbisogno = round(ldd_quan_utilizzo_variante,4)
				s_fabbisogno[ll_max].lead_time = ll_lead_time_variante
				
				// se il lead time non è stato impostato vado a prendere il default dal prodotto
				if ll_lead_time_variante = 0 or isnull(ll_lead_time_variante) then
					select lead_time
					into   :ll_lead_time_prodotto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto_variante;
							 
					if sqlca.sqlcode = 0 then 
						if isnull(ll_lead_time_prodotto) then ll_lead_time_prodotto = 0
						s_fabbisogno[ll_max].lead_time = ll_lead_time_prodotto
						ll_lead_time_variante = ll_lead_time_prodotto
					end if
				end if
				
				if ll_lead_time_variante > ll_lead_time_max then ll_lead_time_max = ll_lead_time_variante
			end if

			close cu_varianti;
		end if

	end if
next

fl_lead_time_cumulato = fl_lead_time_cumulato + ll_lead_time_max

return 0
end function

public subroutine uof_set_riga_ord_ven (integer ai_anno_reg_ord_ven, long al_num_reg_ord_ven, long al_prog_riga_ord_ven);

if ai_anno_reg_ord_ven>0 and not isnull(ai_anno_reg_ord_ven) then
	ii_anno_reg_ord_ven = ai_anno_reg_ord_ven
	il_num_reg_ord_ven = al_num_reg_ord_ven
	il_prog_riga_ord_ven =al_prog_riga_ord_ven
	
else
	setnull(ii_anno_reg_ord_ven)
	setnull(ii_anno_reg_ord_ven)
	setnull(il_prog_riga_ord_ven)
end if


end subroutine

public function integer uof_get_reparti_varianti (string as_cod_prodotto_padre, string as_cod_versione_padre, string as_cod_prodotto_figlio, string as_cod_versione_figlio, ref string as_reparti[]);
string				ls_sql, ls_errore, ls_vuoto[]
datastore		lds_data
integer			li_ret, li_index, ll_new


ls_sql = &
"select 	varianti_det_ord_ven_prod.cod_reparto "+&
"from 		varianti_det_ord_ven_prod "+&
"join		anag_reparti on 	anag_reparti.cod_azienda = varianti_det_ord_ven_prod.cod_azienda and "+&
									"anag_reparti.cod_reparto = varianti_det_ord_ven_prod.cod_reparto "+&
"where 	varianti_det_ord_ven_prod.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
			"varianti_det_ord_ven_prod.anno_registrazione="+string(ii_anno_reg_ord_ven)+" and "+&
			"varianti_det_ord_ven_prod.num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
			"varianti_det_ord_ven_prod.prog_riga_ord_ven="+string(il_prog_riga_ord_ven)+" and "+&
			"varianti_det_ord_ven_prod.cod_prodotto_padre='"+as_cod_prodotto_padre+"' and "+&
			"varianti_det_ord_ven_prod.cod_prodotto_figlio='"+as_cod_prodotto_figlio+"' and "+&
			"varianti_det_ord_ven_prod.cod_versione='"+as_cod_versione_padre+"' and "+&
			"varianti_det_ord_ven_prod.cod_versione_figlio='"+as_cod_versione_figlio+"' "
			
li_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
ll_new = 0

as_reparti[] = ls_vuoto[]

if li_ret>0 then
	for li_index=1 to li_ret
		if lds_data.getitemstring(1, 1)<>"" and not isnull(lds_data.getitemstring(1, 1)) then
			ll_new += 1
			as_reparti[ll_new] = lds_data.getitemstring(1, 1)
		end if
	next
	
	return li_ret
else
	return 0
end if

end function

public function integer uof_ricacolo_data_fabbisogno_commessa (long al_anno_commessa, long al_num_commessa, ref string as_messaggio);long ll_cur_row, ll_rows
datetime ldt_data_consegna, ldt_data_fabb
string ls_cod_prodotto, ls_sql
decimal ldc_lead_time
integer li_lead_time
datastore lds_data


select 	data_consegna
into 		:ldt_data_consegna
from 		anag_commesse
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:al_anno_commessa and 
			num_commessa=:al_num_commessa;

if sqlca.sqlcode <> 0 then
	as_messaggio = g_str.format("Errore durante la lettura della data consegna della commessa n° $1/$2",al_anno_commessa,al_num_commessa)
	return -1
end if

ls_sql = g_str.format("select cod_prodotto from impegno_mat_prime_commessa where cod_azienda='$1' and anno_commessa=$2 and num_commessa=$3",s_cs_xx.cod_azienda, al_anno_commessa, al_num_commessa)

ll_rows = guo_functions.uof_crea_datastore( lds_data, ls_sql)

for ll_cur_row = 1 to ll_rows
	ls_cod_prodotto = lds_data.getitemstring(ll_cur_row, 1)
	setnull(ldc_lead_time)
	
	select lead_time
	into :ldc_lead_time
	from impegno_mat_prime_commessa
	where cod_azienda=:s_cs_xx.cod_azienda
		and anno_commessa=:al_anno_commessa and num_commessa=:al_num_commessa
		and cod_prodotto=:ls_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		as_messaggio = g_str.format("Errore durante la lettura del lead timedel prodotto $1. Operazione Annullata!",ls_cod_prodotto)
		rollback;
		return -1
	end if
	
	li_lead_time = integer(ldc_lead_time)
	ldt_data_fabb = datetime(relativedate(date(ldt_data_consegna), - li_lead_time), 00:00:00)
	
	if isnull(ldt_data_fabb) then
		as_messaggio = g_str.format("Errore durante il calcolo della nuova data di fabbisogno del prodotto $1! Operazione Annullata!", ls_cod_prodotto)
		rollback;
		return -1
	end if
	
	//esegui l'update
	update impegno_mat_prime_commessa
	set data_fabbisogno=:ldt_data_fabb
	where cod_azienda=:s_cs_xx.cod_azienda
		and anno_commessa=:al_anno_commessa and num_commessa=:al_num_commessa
		and cod_prodotto=:ls_cod_prodotto	;
	if sqlca.sqlcode <> 0 then
		as_messaggio = g_str.format("Errore durante l'aggiornamento della nuova data di fabbisogno del prodotto $1! Operazione Annullata!", ls_cod_prodotto)
		rollback;
		return -1
	end if
	
next

commit;

return 0

end function

on uo_mrp.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_mrp.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

