﻿$PBExportHeader$w_analisi_fabbisogni.srw
$PBExportComments$Finestra Testata Generazione Commesse
forward
global type w_analisi_fabbisogni from w_cs_xx_principale
end type
type cb_raggruppa from commandbutton within w_analisi_fabbisogni
end type
type hpb_1 from hprogressbar within w_analisi_fabbisogni
end type
type cb_reset from commandbutton within w_analisi_fabbisogni
end type
type cb_stampa from commandbutton within w_analisi_fabbisogni
end type
type cb_gen_commessa from commandbutton within w_analisi_fabbisogni
end type
type cb_gen_ordini from commandbutton within w_analisi_fabbisogni
end type
type cb_gen_rda from commandbutton within w_analisi_fabbisogni
end type
type cb_gen_rda_no_mrp from commandbutton within w_analisi_fabbisogni
end type
type dw_folder from u_folder within w_analisi_fabbisogni
end type
type dw_ext_selezione_gen_commesse from uo_cs_xx_dw within w_analisi_fabbisogni
end type
type dw_tes_gen_commesse from uo_cs_xx_dw within w_analisi_fabbisogni
end type
type dw_lista_fabbisogni from uo_cs_xx_dw within w_analisi_fabbisogni
end type
type cb_elabora from commandbutton within w_analisi_fabbisogni
end type
type cb_cerca from commandbutton within w_analisi_fabbisogni
end type
end forward

global type w_analisi_fabbisogni from w_cs_xx_principale
integer width = 4617
integer height = 2576
string title = "Analisi Fabbisogni."
event ue_imposta_data ( )
cb_raggruppa cb_raggruppa
hpb_1 hpb_1
cb_reset cb_reset
cb_stampa cb_stampa
cb_gen_commessa cb_gen_commessa
cb_gen_ordini cb_gen_ordini
cb_gen_rda cb_gen_rda
cb_gen_rda_no_mrp cb_gen_rda_no_mrp
dw_folder dw_folder
dw_ext_selezione_gen_commesse dw_ext_selezione_gen_commesse
dw_tes_gen_commesse dw_tes_gen_commesse
dw_lista_fabbisogni dw_lista_fabbisogni
cb_elabora cb_elabora
cb_cerca cb_cerca
end type
global w_analisi_fabbisogni w_analisi_fabbisogni

type variables

end variables

forward prototypes
public function integer f_elimina_array (ref string fs_array[])
public function integer wf_genera_commessa (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quantita, datetime fdt_data_consegna, string fs_cod_tipo_commessa, ref string fs_errore)
public function integer wf_attivazione (long fl_anno_commessa, long fl_num_commessa, ref string fs_errore)
public function integer wf_ricacolo_quan_ordine (ref string as_messaggio)
end prototypes

event ue_imposta_data();integer li_anno

li_anno = f_anno_esercizio()

dw_ext_selezione_gen_commesse.setitem(1,"data_consegna_inizio",date("01/01/"+string(li_anno)))
dw_ext_selezione_gen_commesse.setitem(1,"data_consegna_fine",date("31/12/"+string(li_anno)))
dw_ext_selezione_gen_commesse.setitem(1,"flag_solo_mancanti", "I")
end event

public function integer f_elimina_array (ref string fs_array[]);string ls_array[]

fs_array[]=ls_array

return 0
end function

public function integer wf_genera_commessa (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quantita, datetime fdt_data_consegna, string fs_cod_tipo_commessa, ref string fs_errore);long		ll_anno_registrazione, ll_num_registrazione, ll_prova
string	ls_cod_tipo_commessa, ls_cod_deposito_versamento, ls_cod_deposito_prelievo, ls_flag_muovi_mp, ls_cod_prodotto, ls_cod_versione, &
         ls_cod_prodotto_new
datetime ldt_data_registrazione, ldt_oggi
dec{4}   ld_quan_ordine
			
uo_magazzino luo_magazzino		

ldt_oggi = datetime( date(today()), 00:00:00)

select numero
into   :ll_anno_registrazione
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
		 cod_parametro = 'ESC';

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la lettura del parametro ESC." + sqlca.sqlerrtext
	rollback;
	return -1
end if

ls_cod_tipo_commessa = fs_cod_tipo_commessa

if isnull(ls_cod_tipo_commessa) or ls_cod_tipo_commessa = "" then
	fs_errore = "Attenzione: specificare il tipo commessa nella politica di riordino."
	rollback;
	return -1
end if

select cod_deposito_versamento,
		 cod_deposito_prelievo,
		 flag_muovi_mp
into   :ls_cod_deposito_versamento,
		 :ls_cod_deposito_prelievo,
		 :ls_flag_muovi_mp
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode <> 0 then
	fs_errore = "Si è verificato un errore durante la lettura Tabella Tipi Commessa." + sqlca.sqlerrtext
	rollback;
	return -1
end if

ldt_data_registrazione = datetime(today())

ls_cod_prodotto = fs_cod_prodotto
ls_cod_versione = fs_cod_versione

if isnull(ls_cod_versione) then
	fs_errore = "Versione del prodotto mancante!"
	rollback;
	return -1
end if
		
ld_quan_ordine = fd_quantita

select max(num_commessa)
into   :ll_num_registrazione
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_commessa = :ll_anno_registrazione;

if sqlca.sqlcode <0 then
	fs_errore = "Errore durante la lettura delle Commesse." + sqlca.sqlerrtext
	rollback;
	return -1
end if

if isnull(ll_num_registrazione) then
	ll_num_registrazione= 1
else
	ll_num_registrazione++
end if

// *** se non esiste nella distinta padri allora devo creare il prodotto finito e poi la riga della distinta

ls_cod_prodotto_new = ls_cod_prodotto + "_PF"

select count(*)
into   :ll_prova
from   distinta_padri
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto and
		 cod_versione = :ls_cod_versione;
		 
if sqlca.sqlcode < 0 then
	fs_errore = "(" + ls_cod_prodotto + ") Errore durante il controllo nella distinta padri:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

if isnull(ll_prova) then ll_prova = 0
if ll_prova < 1 then
	
	// *** non è un prodotto finito, controllo se ho già inserito il padre del SML
	select count(*)
	into   :ll_prova
	from   distinta_padri
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto_new and
			 cod_versione = :ls_cod_versione;
			 
	if sqlca.sqlcode < 0 then
		fs_errore = "(" + ls_cod_prodotto_new + ") Errore durante il controllo nella distinta padri:" + sqlca.sqlerrtext
		rollback;
		return -1
	end if	
	
	if isnull(ll_prova) then ll_prova = 0
	if ll_prova > 0 then
		ls_cod_prodotto = ls_cod_prodotto_new
	else
		
		// *** inserimento anagrafica prodotti
		
		INSERT INTO anag_prodotti  
					( cod_azienda,   
					  cod_prodotto,   
					  cod_comodo,   
					  cod_barre,   
					  des_prodotto,   
					  data_creazione,   
					  cod_deposito,   
					  cod_ubicazione,   
					  flag_decimali,   
					  cod_cat_mer,   
					  cod_responsabile,   
					  cod_misura_mag,   
					  cod_misura_peso,   
					  peso_lordo,   
					  peso_netto,   
					  cod_misura_vol,   
					  volume,   
					  pezzi_collo,   
					  flag_classe_abc,   
					  flag_sotto_scorta,   
					  flag_lifo,   
					  saldo_quan_anno_prec,   
					  saldo_quan_inizio_anno,   
					  val_inizio_anno,   
					  saldo_quan_ultima_chiusura,   
					  prog_quan_entrata,   
					  val_quan_entrata,   
					  prog_quan_uscita,   
					  val_quan_uscita,   
					  prog_quan_acquistata,   
					  val_quan_acquistata,   
					  prog_quan_venduta,   
					  val_quan_venduta,   
					  quan_ordinata,   
					  quan_impegnata,   
					  quan_assegnata,   
					  quan_in_spedizione,   
					  quan_anticipi,   
					  costo_standard,   
					  costo_ultimo,   
					  lotto_economico,   
					  livello_riordino,   
					  scorta_minima,   
					  scorta_massima,   
					  indice_rotazione,   
					  consumo_medio,   
					  cod_prodotto_alt,   
					  cod_misura_acq,   
					  fat_conversione_acq,   
					  cod_fornitore,   
					  cod_gruppo_acq,   
					  prezzo_acquisto,   
					  sconto_acquisto,   
					  quan_minima,   
					  inc_ordine,   
					  quan_massima,   
					  tempo_app,   
					  cod_misura_ven,   
					  fat_conversione_ven,   
					  cod_gruppo_ven,   
					  cod_iva,   
					  prezzo_vendita,   
					  sconto,   
					  provvigione,   
					  flag_blocco,   
					  data_blocco,   
					  cod_livello_prod_1,   
					  cod_livello_prod_2,   
					  cod_livello_prod_3,   
					  cod_livello_prod_4,   
					  cod_livello_prod_5,   
					  cod_nomenclatura,   
					  cod_imballo,   
					  flag_articolo_fiscale,   
					  flag_cauzione,   
					  cod_cauzione,   
					  cod_tipo_politica_riordino,   
					  cod_misura_lead_time,   
					  lead_time,   
					  cod_formula,   
					  cod_reparto,   
					  flag_materia_prima,   
					  flag_escludibile,   
					  cod_rifiuto,   
					  flag_stato_rifiuto,   
					  cod_comodo_2,   
					  cod_tipo_costo_mp,   
					  flag_modifica_disegno,   
					  cod_prodotto_raggruppato )  
			  SELECT :s_cs_xx.cod_azienda,   
						:ls_cod_prodotto_new,   
						anag_prodotti.cod_comodo,   
						anag_prodotti.cod_barre,   
						anag_prodotti.des_prodotto,   
						anag_prodotti.data_creazione,   
						anag_prodotti.cod_deposito,   
						anag_prodotti.cod_ubicazione,   
						anag_prodotti.flag_decimali,   
						anag_prodotti.cod_cat_mer,   
						anag_prodotti.cod_responsabile,   
						anag_prodotti.cod_misura_mag,   
						anag_prodotti.cod_misura_peso,   
						anag_prodotti.peso_lordo,   
						anag_prodotti.peso_netto,   
						anag_prodotti.cod_misura_vol,   
						anag_prodotti.volume,   
						anag_prodotti.pezzi_collo,   
						anag_prodotti.flag_classe_abc,   
						anag_prodotti.flag_sotto_scorta,   
						anag_prodotti.flag_lifo,   
						anag_prodotti.saldo_quan_anno_prec,   
						anag_prodotti.saldo_quan_inizio_anno,   
						anag_prodotti.val_inizio_anno,   
						anag_prodotti.saldo_quan_ultima_chiusura,   
						anag_prodotti.prog_quan_entrata,   
						anag_prodotti.val_quan_entrata,   
						anag_prodotti.prog_quan_uscita,   
						anag_prodotti.val_quan_uscita,   
						anag_prodotti.prog_quan_acquistata,   
						anag_prodotti.val_quan_acquistata,   
						anag_prodotti.prog_quan_venduta,   
						anag_prodotti.val_quan_venduta,   
						anag_prodotti.quan_ordinata,   
						anag_prodotti.quan_impegnata,   
						anag_prodotti.quan_assegnata,   
						anag_prodotti.quan_in_spedizione,   
						anag_prodotti.quan_anticipi,   
						anag_prodotti.costo_standard,   
						anag_prodotti.costo_ultimo,   
						anag_prodotti.lotto_economico,   
						anag_prodotti.livello_riordino,   
						anag_prodotti.scorta_minima,   
						anag_prodotti.scorta_massima,   
						anag_prodotti.indice_rotazione,   
						anag_prodotti.consumo_medio,   
						anag_prodotti.cod_prodotto_alt,   
						anag_prodotti.cod_misura_acq,   
						anag_prodotti.fat_conversione_acq,   
						anag_prodotti.cod_fornitore,   
						anag_prodotti.cod_gruppo_acq,   
						anag_prodotti.prezzo_acquisto,   
						anag_prodotti.sconto_acquisto,   
						anag_prodotti.quan_minima,   
						anag_prodotti.inc_ordine,   
						anag_prodotti.quan_massima,   
						anag_prodotti.tempo_app,   
						anag_prodotti.cod_misura_ven,   
						anag_prodotti.fat_conversione_ven,   
						anag_prodotti.cod_gruppo_ven,   
						anag_prodotti.cod_iva,   
						anag_prodotti.prezzo_vendita,   
						anag_prodotti.sconto,   
						anag_prodotti.provvigione,   
						anag_prodotti.flag_blocco,   
						anag_prodotti.data_blocco,   
						anag_prodotti.cod_livello_prod_1,   
						anag_prodotti.cod_livello_prod_2,   
						anag_prodotti.cod_livello_prod_3,   
						anag_prodotti.cod_livello_prod_4,   
						anag_prodotti.cod_livello_prod_5,   
						anag_prodotti.cod_nomenclatura,   
						anag_prodotti.cod_imballo,   
						'N',   
						anag_prodotti.flag_cauzione,   
						anag_prodotti.cod_cauzione,   
						anag_prodotti.cod_tipo_politica_riordino,   
						anag_prodotti.cod_misura_lead_time,   
						anag_prodotti.lead_time,   
						anag_prodotti.cod_formula,   
						anag_prodotti.cod_reparto,   
						anag_prodotti.flag_materia_prima,   
						anag_prodotti.flag_escludibile,   
						anag_prodotti.cod_rifiuto,   
						anag_prodotti.flag_stato_rifiuto,   
						anag_prodotti.cod_comodo_2,   
						anag_prodotti.cod_tipo_costo_mp,   
						anag_prodotti.flag_modifica_disegno,   
						anag_prodotti.cod_prodotto_raggruppato  
				 FROM anag_prodotti   
				 where cod_azienda = :s_cs_xx.cod_azienda and
				       cod_prodotto = :ls_cod_prodotto;
		
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante l'inserimento in anagrafica prodotti:" + sqlca.sqlerrtext
			rollback;
			return -1			
		end if
		
		// *** inserimento distinta padri
		INSERT INTO distinta_padri  
        ( cod_azienda,   
           cod_prodotto,   
           cod_versione,   
           creato_da,   
           approvato_da,   
           des_versione,   
           data_creazione,   
           data_approvazione,   
           flag_predefinita,   
           flag_blocco,   
           data_blocco,   
           flag_esplodi_in_doc,   
           flag_non_calcolare_dt,   
           flag_chiusura_automatica )  
      VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_prodotto_new,   
           :ls_cod_versione,   
           null,   
           null,   
           'Prodotto finito di appoggio',   
           :ldt_oggi,   
           null,   
           'N',   
           'S',   
           :ldt_oggi,   
           'N',   
           'S',   
           'N' )  ;
			  
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante l'inserimento in distinta padri:" + sqlca.sqlerrtext
			rollback;
			return -1	
		end if
		
		// *** inserimento distinta
		
		insert into distinta( cod_azienda,
		                      cod_prodotto_padre,
									 cod_versione,
									 cod_prodotto_figlio,
									 cod_versione_figlio,
									 num_sequenza)
		values              ( :s_cs_xx.cod_azienda,
		                      :ls_cod_prodotto_new,
									 :ls_cod_versione,
									 :ls_cod_prodotto,
									 :ls_cod_versione,
									 10);
									 
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante l'inserimento del ramo di distinta:" + sqlca.sqlerrtext
			rollback;
			return -1	
		end if		
		
		ls_cod_prodotto = ls_cod_prodotto_new
		
	end if
	
end if


insert into anag_commesse  
				(cod_azienda,   
				 anno_commessa,   
				 num_commessa,   
				 cod_tipo_commessa,   
				 data_registrazione,
				 cod_operatore,
				 nota_testata,
				 nota_piede,
				 flag_blocco,
				 quan_ordine,   
				 quan_prodotta,
				 data_chiusura,
				 data_consegna,
				 cod_prodotto,
				 quan_assegnata,
				 quan_in_produzione,
				 cod_deposito_versamento,
				 cod_deposito_prelievo,
				 cod_versione)
values			(:s_cs_xx.cod_azienda,   
				 :ll_anno_registrazione,   
				 :ll_num_registrazione,   
				 :ls_cod_tipo_commessa,   
				 :ldt_data_registrazione,
				 null,
				 null,
				 null,
				 'N',
				 :ld_quan_ordine,
				 0,
				 null,
				 :fdt_data_consegna,
				 :ls_cod_prodotto,
				 0,
				 0,
				 :ls_cod_deposito_versamento,
				 :ls_cod_deposito_prelievo,
				 :ls_cod_versione);

if sqlca.sqlcode < 0 then
	fs_errore = "Errore durante la scrittura delle Commesse. Errore DB:" + sqlca.sqlerrtext
	rollback;
	return -1			
end if

if ls_flag_muovi_mp = 'S' then
	
	luo_magazzino = create uo_magazzino
	
	if luo_magazzino.uof_impegna_mp_commessa(	true, &
															false, &
															ls_cod_prodotto, &
															ls_cod_versione, &
															ld_quan_ordine, &
															ll_anno_registrazione, &
															ll_num_registrazione, &
															fs_errore) = -1 then
															
		fs_errore = "Errore in fase di impegno magazzino~r~n" + fs_errore
		destroy luo_magazzino;
		rollback;
		return -1
	end if
	
	destroy luo_magazzino;
	
end if


return 0
end function

public function integer wf_attivazione (long fl_anno_commessa, long fl_num_commessa, ref string fs_errore);double  ldd_quan_in_produzione,ldd_quan_in_ordine, & 
		  ldd_quan_prodotta,ldd_quan_da_assegnare,ldd_quantita_possibile, & 
		  ldd_quan_assegnata_st, ldd_quan_assegnata_mp
long    ll_prog_riga,ll_prog_stock[1],ll_anno_reg_des_mov, ll_num_reg_des_mov
string  ls_errore,ls_test,ls_cod_deposito_prelievo, ls_cod_tipo_mov_anticipo, ls_flag_tipo_avanzamento,& 
		  ls_cod_tipo_commessa,ls_cod_tipo_movimento,ls_cod_prodotto,ls_cod_deposito[1], & 
		  ls_cod_ubicazione[1],ls_cod_lotto[1],ls_cod_cliente[1],ls_cod_fornitore[1],ls_cod_versione
long li_risposta
date 	  ld_data_stock[1]

select quan_in_produzione,
       quan_prodotta,
		 quan_ordine,
		 cod_prodotto,
		 cod_deposito_prelievo,
		 cod_tipo_commessa,
		 cod_versione,
		 flag_tipo_avanzamento
into   :ldd_quan_in_produzione,
       :ldd_quan_prodotta,
		 :ldd_quan_in_ordine,
		 :ls_cod_prodotto,
		 :ls_cod_deposito_prelievo,
		 :ls_cod_tipo_commessa,
		 :ls_cod_versione,
		 :ls_flag_tipo_avanzamento
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :fl_anno_commessa and
		 num_commessa = :fl_num_commessa;
		 
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca dati commessa:" + sqlca.sqlerrtext
	return -1
end if

if isnull(ldd_quan_in_produzione) then ldd_quan_in_produzione = 0
if isnull(ldd_quan_prodotta) then ldd_quan_prodotta = 0
if isnull(ldd_quan_in_ordine) then ldd_quan_in_ordine = 0

if ls_flag_tipo_avanzamento <> "2" and ls_flag_tipo_avanzamento <> "0" and ls_flag_tipo_avanzamento <> "8" then
	fs_errore = "Questa commessa è già stata lanciata in produzione con un'altro metodo, pertanto non è possibile compiere alcuna operazione con questo metodo."
	return -1
end if

select cod_prodotto_figlio
into   :ls_test
from   distinta
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_prodotto_padre = :ls_cod_prodotto;

if sqlca.sqlcode = 100 then
	fs_errore = "Attenzione! Il prodotto della commessa non ha un distinta base associata, pertanto non sarà possibile assegnare le materie prime. Creare la distinta base del prodotto"
	return -1
end if

if ldd_quan_in_produzione + ldd_quan_prodotta >= ldd_quan_in_ordine then
	fs_errore = "La somma delle quantità prodotta e in produzione è uguale o supera la quantità in ordine."
	return -1 
else
	ldd_quan_in_ordine = ldd_quan_in_ordine - ldd_quan_in_produzione - ldd_quan_prodotta
end if

select cod_tipo_mov_ver_prod_finiti,
		 cod_tipo_mov_anticipo
into   :ls_cod_tipo_movimento,
		 :ls_cod_tipo_mov_anticipo
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode<>0 then
	fs_errore = "Errore nel DB:" + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_tipo_movimento) or isnull(ls_cod_tipo_mov_anticipo) then
	fs_errore = "Manca la configurazione dei tipi movimento in tabella tipi commessa!"
	return -1
end if

ldd_quan_in_produzione = ldd_quan_in_ordine

select max(prog_riga)
into   :ll_prog_riga
from   det_anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :fl_anno_commessa and    
		 num_commessa = :fl_num_commessa;

if sqlca.sqlcode<>0 then
	fs_errore = "Errore nel DB:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

if not isnull(ll_prog_riga) then
	ll_prog_riga++
else
	ll_prog_riga = 1
end if

INSERT INTO det_anag_commesse
          ( cod_azienda,
            anno_commessa,   
            num_commessa,   
            prog_riga,   
            anno_registrazione,   
            num_registrazione,   
            quan_assegnata,   
            quan_in_produzione,   
            quan_prodotta,
				cod_tipo_movimento,
				anno_reg_des_mov,
				num_reg_des_mov,
				cod_tipo_mov_anticipo,
				quan_anticipo,
				anno_reg_anticipo,
				num_reg_anticipo )  
VALUES    ( :s_cs_xx.cod_azienda,   
            :fl_anno_commessa,   
            :fl_num_commessa,   
            :ll_prog_riga,   
            null,   
            null,
            0,
            :ldd_quan_in_produzione,
            0,
				:ls_cod_tipo_movimento,
				null,
				null,
				:ls_cod_tipo_mov_anticipo,
				0,
				null,
				null);

if sqlca.sqlcode<>0 then
	fs_errore = "Errore nel DB:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

li_risposta = f_avan_prod_com (fl_anno_commessa, fl_num_commessa, ll_prog_riga, & 
										 ls_cod_prodotto, ls_cod_versione, ldd_quan_in_produzione, & 
										 1, ls_cod_tipo_commessa, ls_errore )


if li_risposta = -1 then
	rollback;
	fs_errore = ls_errore
	return -1
end if

select sum(quan_in_produzione),
		 sum(quan_prodotta)
into   :ldd_quan_in_produzione,
		 :ldd_quan_prodotta
from   det_anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :fl_anno_commessa and    
		 num_commessa = :fl_num_commessa;

if sqlca.sqlcode<>0 then
	fs_errore = "Errore nel DB:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

update anag_commesse
set    quan_in_produzione = :ldd_quan_in_produzione,
		 flag_tipo_avanzamento = '2'
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :fl_anno_commessa and    
		 num_commessa = :fl_num_commessa;

if sqlca.sqlcode<>0 then
	fs_errore = "Errore nel DB:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

return 0
end function

public function integer wf_ricacolo_quan_ordine (ref string as_messaggio);string		ls_des_prodotto, ls_cod_tipo_politica_riordino, ls_des_tipo_politica_riordino, ls_flag_modo_riordino, ls_cod_prodotto
long		ll_row
dec{4} 	ld_scorta_minima, ld_lotto_economico, ld_livello_riordino, ld_soglia_riordino, ld_quan_mancante


as_messaggio = ""

for ll_row = 1 to dw_lista_fabbisogni.rowcount()
	
	dw_lista_fabbisogni.setitem(ll_row,"flag_ordine", "N")

	ls_cod_prodotto = dw_lista_fabbisogni.getitemstring(ll_row, "cod_prodotto")
		
	select des_prodotto,
			scorta_minima, 
			lotto_economico, 
			livello_riordino, 
			cod_tipo_politica_riordino
	into	:ls_des_prodotto,
			:ld_scorta_minima, 
			:ld_lotto_economico, 
			:ld_livello_riordino, 
			:ls_cod_tipo_politica_riordino
	from	anag_prodotti
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode < 0 then
		as_messaggio = g_str.format("Errore in select prodotto $1 (wf_ricacolo_quan_ordine).~r~n $2", ls_cod_prodotto, sqlca.sqlerrtext)
		return -1
	end if
	if sqlca.sqlcode = 100 then
		as_messaggio = g_str.format("Prodotto $1 non trovato (wf_ricacolo_quan_ordine).", ls_cod_prodotto)
		return -1
	end if
	
	if isnull(ld_scorta_minima) then ld_scorta_minima = 0
	if isnull(ld_lotto_economico) then ld_lotto_economico = 0
	if isnull(ld_livello_riordino) then ld_livello_riordino = 0
				
	dw_lista_fabbisogni.setitem(ll_row,"des_prodotto",ls_des_prodotto)
	dw_lista_fabbisogni.setitem(ll_row,"cod_politica_riordino",ls_cod_tipo_politica_riordino)
	
	if not isnull(ls_cod_tipo_politica_riordino) then
		select des_tipo_politica_riordino, 
				flag_modo_riordino
		into   :ls_des_tipo_politica_riordino, 
				:ls_flag_modo_riordino  
		from   tab_tipi_politiche_riordino
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;
				 
		if sqlca.sqlcode <> 0 then
			as_messaggio = g_str.format("Errore in ricerca politica riordino $1 (wf_ricacolo_quan_ordine).~r~n $2", ls_cod_tipo_politica_riordino, sqlca.sqlerrtext)
			return -1
		end if
		dw_lista_fabbisogni.setitem(ll_row,"des_politica_riordino",ls_des_tipo_politica_riordino)
	else
		ls_flag_modo_riordino= "M"
	end if
		
	choose case ls_flag_modo_riordino
		case "R"  // livello riordino
			// ok mi tengo il livello di riordino
		case "M" // scorta minima
			// sostituisco il lvello di riordino con la scorta minima
			ld_livello_riordino = ld_scorta_minima
		case "C"  // lo compro indipendentemente dalla giancenza
			ld_livello_riordino = 0
	end choose
	
	if dw_lista_fabbisogni.getitemnumber(ll_row,"quan_fabbisogno") < ld_livello_riordino then
		ld_soglia_riordino = ld_livello_riordino + dw_lista_fabbisogni.getitemnumber(ll_row,"quan_fabbisogno")
	else
		ld_soglia_riordino = dw_lista_fabbisogni.getitemnumber(ll_row,"quan_fabbisogno") + ld_livello_riordino
	end if
	
	if isnull(ld_soglia_riordino) then ld_soglia_riordino = 0
	
	if ls_flag_modo_riordino <> "C" then
		ld_quan_mancante = -1 * (dw_lista_fabbisogni.getitemnumber(ll_row,"disponibilita_alla_data") - ld_soglia_riordino)
	else
		// questo è il caso in cui lo compro indipendentemente dalla giacenza, livello riordino o altro; lo compro sempre.
		ld_quan_mancante = dw_lista_fabbisogni.getitemnumber(ll_row,"quan_fabbisogno")
	end if		

	if ld_quan_mancante > 0 then
		dw_lista_fabbisogni.setitem(ll_row,"flag_ordine", "S")
				
//		ld_quan_mancante = -1 * ( (dw_lista_fabbisogni.getitemnumber(ll_row,"quan_fabbisogno") - ld_soglia_riordino) -  )
		dw_lista_fabbisogni.setitem(ll_row, "quan_mancante", ld_quan_mancante)
		
		dw_lista_fabbisogni.setitem(ll_row, "scorta_minima", ld_scorta_minima)
		dw_lista_fabbisogni.setitem(ll_row, "lotto_economico", ld_lotto_economico)
		
		// da verificare questo script
		if ld_scorta_minima > ld_quan_mancante then
			if ld_lotto_economico > 0 and not isnull(ld_lotto_economico) and ld_lotto_economico > ld_scorta_minima then
				dw_lista_fabbisogni.setitem(ll_row, "quan_ordine",ld_lotto_economico)
			else
				dw_lista_fabbisogni.setitem(ll_row, "quan_ordine",ld_scorta_minima)
			end if
		else
			if ld_lotto_economico > 0 and not isnull(ld_lotto_economico) and ld_lotto_economico > ld_quan_mancante then
				dw_lista_fabbisogni.setitem(ll_row, "quan_ordine",ld_quan_mancante)
			else
				dw_lista_fabbisogni.setitem(ll_row, "quan_ordine",ld_quan_mancante)
			end if
		end if
		
	end if
	
next



return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

set_w_options(c_noenablepopup)

lw_oggetti[1] = dw_tes_gen_commesse
lw_oggetti[2] = cb_elabora
lw_oggetti[3] = cb_gen_rda_no_mrp
dw_folder.fu_assigntab(2, "Commesse", lw_oggetti[])
lw_oggetti[1] = dw_ext_selezione_gen_commesse
lw_oggetti[2] = cb_cerca
lw_oggetti[3] = cb_reset
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])
lw_oggetti[1] = dw_lista_fabbisogni
lw_oggetti[2] = cb_gen_rda
lw_oggetti[3] = cb_gen_ordini
lw_oggetti[4] = hpb_1
lw_oggetti[5] = cb_stampa
lw_oggetti[6] = cb_gen_commessa
lw_oggetti[7] = cb_raggruppa
dw_folder.fu_assigntab(3, "Fabbisogni", lw_oggetti[])
dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_tes_gen_commesse.set_dw_options(sqlca, &
                                   pcca.null_object, &
											  c_noretrieveonopen + &
                                   c_nomultiselect + &
							   		     c_modifyonopen + &
						   			     c_nonew + &
											  c_nodelete + &
											  c_disablecc + &
											  c_disableccinsert, &
                                   c_InactiveDWColorUnchanged + &
											  c_NoCursorRowPointer + &
											  c_NoCursorRowFocusRect + &
											  c_ViewModeBorderUnchanged + &
											  c_ViewModeColorUnchanged)

dw_lista_fabbisogni.set_dw_options(sqlca, &
                        	 pcca.null_object, &
                            c_multiselect + &
									 c_nonew + &
									 c_modifyonopen + &
									 c_nodelete + &
									 c_disablecc + &
									 c_noretrieveonopen + &
									 c_disableccinsert , &
									 c_InactiveDWColorUnchanged + &
									 c_NoCursorRowPointer + &
									 c_NoCursorRowFocusRect + &
									 c_ViewModeBorderUnchanged + &
									 c_ViewModeColorUnchanged)

dw_ext_selezione_gen_commesse.set_dw_options(sqlca, &
                             	  pcca.null_object,&
										  c_newonopen, &
	                             c_default)
													 
save_on_close(c_socnosave)

postevent("ue_imposta_data")
end event

on w_analisi_fabbisogni.create
int iCurrent
call super::create
this.cb_raggruppa=create cb_raggruppa
this.hpb_1=create hpb_1
this.cb_reset=create cb_reset
this.cb_stampa=create cb_stampa
this.cb_gen_commessa=create cb_gen_commessa
this.cb_gen_ordini=create cb_gen_ordini
this.cb_gen_rda=create cb_gen_rda
this.cb_gen_rda_no_mrp=create cb_gen_rda_no_mrp
this.dw_folder=create dw_folder
this.dw_ext_selezione_gen_commesse=create dw_ext_selezione_gen_commesse
this.dw_tes_gen_commesse=create dw_tes_gen_commesse
this.dw_lista_fabbisogni=create dw_lista_fabbisogni
this.cb_elabora=create cb_elabora
this.cb_cerca=create cb_cerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_raggruppa
this.Control[iCurrent+2]=this.hpb_1
this.Control[iCurrent+3]=this.cb_reset
this.Control[iCurrent+4]=this.cb_stampa
this.Control[iCurrent+5]=this.cb_gen_commessa
this.Control[iCurrent+6]=this.cb_gen_ordini
this.Control[iCurrent+7]=this.cb_gen_rda
this.Control[iCurrent+8]=this.cb_gen_rda_no_mrp
this.Control[iCurrent+9]=this.dw_folder
this.Control[iCurrent+10]=this.dw_ext_selezione_gen_commesse
this.Control[iCurrent+11]=this.dw_tes_gen_commesse
this.Control[iCurrent+12]=this.dw_lista_fabbisogni
this.Control[iCurrent+13]=this.cb_elabora
this.Control[iCurrent+14]=this.cb_cerca
end on

on w_analisi_fabbisogni.destroy
call super::destroy
destroy(this.cb_raggruppa)
destroy(this.hpb_1)
destroy(this.cb_reset)
destroy(this.cb_stampa)
destroy(this.cb_gen_commessa)
destroy(this.cb_gen_ordini)
destroy(this.cb_gen_rda)
destroy(this.cb_gen_rda_no_mrp)
destroy(this.dw_folder)
destroy(this.dw_ext_selezione_gen_commesse)
destroy(this.dw_tes_gen_commesse)
destroy(this.dw_lista_fabbisogni)
destroy(this.cb_elabora)
destroy(this.cb_cerca)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ext_selezione_gen_commesse, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
 f_po_loaddddw_dw(dw_ext_selezione_gen_commesse, &
                 "cod_tipo_politica_riordino", &
                 sqlca, &
                 "tab_tipi_politiche_riordino", &
                 "cod_tipo_politica_riordino", &
                 "des_tipo_politica_riordino", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
end event

type cb_raggruppa from commandbutton within w_analisi_fabbisogni
integer x = 2981
integer y = 2352
integer width = 366
integer height = 92
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Raggruppa"
end type

event clicked;// pulsante che esegue il raggruppamento per prodotto.
// scompare quindi il riferimento alla singola commessa
string	ls_cod_prodotto, ls_errore
long	ll_i, ll_null
dec{4} ld_scorta_minima, ld_quan_mancante, ld_lotto_economico

setnull(ll_null)
ll_i = dw_lista_fabbisogni.setsort("cod_prodotto")
ll_i = dw_lista_fabbisogni.sort()
ll_i = 1
do while true
	
	if dw_lista_fabbisogni.rowcount() <= ll_i + 1 then exit
	if dw_lista_fabbisogni.getitemstring(ll_i, "cod_prodotto") = dw_lista_fabbisogni.getitemstring(ll_i + 1, "cod_prodotto") then
		
		dw_lista_fabbisogni.setitem(ll_i,"quan_fabbisogno", dw_lista_fabbisogni.getitemnumber(ll_i,"quan_fabbisogno") + dw_lista_fabbisogni.getitemnumber(ll_i + 1,"quan_fabbisogno"))

		if dw_lista_fabbisogni.getitemdatetime(ll_i, "data_fabbisogno") > dw_lista_fabbisogni.getitemdatetime(ll_i + 1, "data_fabbisogno") then
			// prendo la data fabbisogno più vicina
			dw_lista_fabbisogni.setitem(ll_i, "data_fabbisogno", dw_lista_fabbisogni.getitemdatetime(ll_i + 1, "data_fabbisogno") )
		end if
		
		dw_lista_fabbisogni.setitem(ll_i,"anno_commessa",ll_null)
		dw_lista_fabbisogni.setitem(ll_i,"num_commessa",ll_null)
		
		dw_lista_fabbisogni.deleterow(ll_i + 1)
	else
		ll_i ++
	end if
	
	
loop

if wf_ricacolo_quan_ordine(ls_errore) < 0 then
	g_mb.error(ls_errore)
end if

end event

type hpb_1 from hprogressbar within w_analisi_fabbisogni
integer x = 64
integer y = 2356
integer width = 2537
integer height = 76
unsignedinteger maxposition = 100
integer setstep = 10
end type

type cb_reset from commandbutton within w_analisi_fabbisogni
integer x = 1874
integer y = 1252
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;string ls_null

setnull(ls_null)
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_inizio",datetime(date("01/01/1900"),00:00:00))
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_fine",datetime(date("31/12/2099"),00:00:00))
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"data_registrazione_inizio",datetime(date("01/01/1900"),00:00:00))
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"data_registrazione_fine",datetime(date("31/12/2099"),00:00:00))
dw_ext_selezione_gen_commesse.setitem(dw_ext_selezione_gen_commesse.getrow(),"cod_cliente", ls_null)




end event

type cb_stampa from commandbutton within w_analisi_fabbisogni
integer x = 2606
integer y = 2352
integer width = 366
integer height = 92
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;datastore lds_stampa

if dw_lista_fabbisogni.rowcount() > 0 then
	
	lds_stampa = create datastore
	lds_stampa.dataobject = "d_analisi_fabbisogni_lista_fabb_grid"
	lds_stampa.settransobject( sqlca)
	lds_stampa.Object.DataWindow.Print.Preview ='Yes'
	dw_lista_fabbisogni.RowsCopy( 1, dw_lista_fabbisogni.RowCount(), Primary!, lds_stampa, 1, Primary!)
	
	lds_stampa.print()
	
else
	g_mb.messagebox( "SEP", "Nessun risultato da stampare!")
end if
end event

type cb_gen_commessa from commandbutton within w_analisi_fabbisogni
integer x = 3355
integer y = 2352
integer width = 366
integer height = 92
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen.Comm."
end type

event clicked;long	 ll_righe, ll_i, ll_ret
string ls_cod_tipo_riordino, ls_tipo_riordino, ls_cod_prodotto, ls_cod_versione, ls_errore, ls_cod_tipo_commessa
dec{4} ld_quantita
datetime ldt_data_consegna

if g_mb.messagebox("APICE","Procedo con la generazione automatica delle commesse interne per i prodotti selezionati ?",question!, yesno!,2) = 2 then return

ll_righe = dw_lista_fabbisogni.rowcount()
if ll_righe < 1 then
	g_mb.messagebox("APICE","Nulla di selezionato")
	return
end if

for ll_i = 1 to ll_righe
	if dw_lista_fabbisogni.getitemstring(ll_i, "flag_ordine") = 'S' then
				
		ls_cod_tipo_riordino = dw_lista_fabbisogni.getitemstring(ll_i, "cod_politica_riordino")
		
		if not isnull(ls_cod_tipo_riordino) and ls_cod_tipo_riordino <> "" then
			
			select flag_tipo_riordino,
			       cod_tipo_commessa
			into   :ls_tipo_riordino,
			       :ls_cod_tipo_commessa
			from   tab_tipi_politiche_riordino
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_politica_riordino = :ls_cod_tipo_riordino;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "SEP", "Errore durante la ricerca del tipo di riordino per il codice " + ls_cod_tipo_riordino + " :" + sqlca.sqlerrtext, stopsign!)
				continue
			end if
			
			if not isnull(ls_tipo_riordino) and ls_tipo_riordino = "C" then		//tipo riordino commessa interna
			
				if isnull(ls_cod_tipo_commessa) or ls_cod_tipo_commessa = "" then
					g_mb.messagebox( "SEP", "Attenzione: impostare il tipo di commessa per la politica di riordino " + ls_cod_tipo_riordino + "!", stopsign!)
					continue
				end if
			
				ls_cod_prodotto = dw_lista_fabbisogni.getitemstring( ll_i, "cod_prodotto")
				ls_cod_versione = dw_lista_fabbisogni.getitemstring( ll_i, "cod_versione")
				ldt_data_consegna = dw_lista_fabbisogni.getitemdatetime( ll_i, "data_fabbisogno")
				
				select lotto_economico
				into   :ld_quantita
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_prodotto = :ls_cod_prodotto;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox( "SEP", "Errore durante la ricerca del lotto economico per il prodotto " + ls_cod_prodotto + " :" + sqlca.sqlerrtext, stopsign!)
					continue
				end if
				
				if isnull(ld_quantita) or ld_quantita = 0 then
					g_mb.messagebox( "SEP", "Attenzione: il lotto economico per il prodotto " + ls_cod_prodotto + " risulta nullo o uguale a zero!", stopsign!)
					continue
				end if
				
				ll_ret = wf_genera_commessa( ls_cod_prodotto, ls_cod_versione, ld_quantita, ldt_data_consegna, ls_cod_tipo_commessa, ls_errore)
	
				if ll_ret < 0 then
					g_mb.messagebox( "SEP", ls_errore, stopsign!)
					rollback;
					return -1
				end if
	
			end if
			
		end if
		
	end if
next

commit;

dw_lista_fabbisogni.reset()
g_mb.messagebox("APICE","Generazione Commesse Eseguita con Successo !")
return
end event

type cb_gen_ordini from commandbutton within w_analisi_fabbisogni
integer x = 3762
integer y = 2356
integer width = 366
integer height = 80
integer taborder = 51
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen.Ordini"
end type

event clicked;string ls_cod_prodotto[], ls_des_prodotto[], ls_messaggio, ls_flag_gen_commessa[],ls_flag_tipo_riga, &
       ls_note[], ls_cod_versione
dec{4} ld_quan_ordine[]
long ll_i, ll_righe, ll_y, ll_anno_ordine[], ll_num_ordine[]
datetime ldt_data_fabbisogno[]
uo_generazione_documenti uo_gen_doc


if g_mb.messagebox("APICE","Procedo con la generazione automatica ordini di acquisto ?",question!, yesno!,2) = 2 then return

ll_righe = dw_lista_fabbisogni.rowcount()
if ll_righe < 1 then
	g_mb.messagebox("APICE","Nulla da ordinare")
	return
end if

ll_y = 0

for ll_i = 1 to ll_righe
	if dw_lista_fabbisogni.getitemstring(ll_i, "flag_ordine") = 'S' then
		ll_y ++
		ls_flag_tipo_riga = dw_lista_fabbisogni.getitemstring(ll_i, "flag_tipo_riga")
		ls_cod_prodotto[ll_y] = dw_lista_fabbisogni.getitemstring(ll_i, "cod_prodotto")
		ls_cod_versione = dw_lista_fabbisogni.getitemstring(ll_i, "cod_versione")
		ld_quan_ordine[ll_y] = abs(dw_lista_fabbisogni.getitemnumber(ll_i, "quan_mancante"))
		ldt_data_fabbisogno[ll_y] = dw_lista_fabbisogni.getitemdatetime(ll_i, "data_fabbisogno")
		setnull(ls_des_prodotto[ll_y])
		
		choose case ls_flag_tipo_riga
			case "1"
				ls_flag_gen_commessa[ll_y] = "N"
			
			case "2"
				ls_flag_gen_commessa[ll_y] = "N"
				ls_note[ll_y] = "Ordine di lavorazione codice " + ls_cod_prodotto[ll_y] + " " + ls_des_prodotto[ll_y] + " Vers. " + ls_cod_versione
				ls_des_prodotto[ll_y] = "Ordine Lavorazione cod." + ls_cod_prodotto[ll_y]
				setnull(ls_cod_prodotto[ll_y])
				
			case "3"
				ls_flag_gen_commessa[ll_y] = "S"
				ls_note[ll_y] = "Produzione C/Terzista codice " + ls_cod_prodotto[ll_y] + " " + ls_des_prodotto[ll_y] + " Vers. " + ls_cod_versione
		end choose		
	end if
next

if ll_y < 1 then
	g_mb.messagebox("APICE","Nulla da ordinare")
	return
end if

uo_gen_doc = Create uo_generazione_documenti 


//if uo_gen_doc.uof_genera_ordini_fornitori(ls_cod_prodotto[], &
//														ls_des_prodotto[], &
//													   ld_quan_ordine[], &
//														ldt_data_fabbisogno[], &
//														ls_flag_gen_commessa[], &
//														ls_note[], &
//													   ref ll_anno_ordine, &
//													   ref ll_num_ordine, &
//													   ref ls_messaggio) <> 0 then
//
//	rollback;
//	
//	messagebox("APICE",ls_messaggio)
//	
//else
//	
//	commit;
//	
//end if

destroy uo_gen_doc
dw_lista_fabbisogni.reset()
g_mb.messagebox("APICE","Generazione Ordini Eseguita con Successo !")
return
end event

type cb_gen_rda from commandbutton within w_analisi_fabbisogni
integer x = 4142
integer y = 2356
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen. RDA"
end type

event clicked;string ls_cod_prodotto, ls_cod_versione,ls_cod_tipo_politica_riordino, ls_errore, ls_note, &
       ls_flag_gen_commessa, ls_des_prodotto, ls_flag_tipo_riga
long ll_i, ll_anno_commessa,ll_num_commessa, ll_ret, ll_anno_reg_rda, ll_num_reg_rda
dec{4} ldd_quan_rda
datetime ldt_data_consegna
uo_generazione_documenti luo_gen_doc
										

if g_mb.messagebox("SEP","Procedo con la creazione RDA ?",Question!,YesNo!,2) = 2 then return

luo_gen_doc = create uo_generazione_documenti 

setnull(ll_anno_reg_rda)
setnull(ll_num_reg_rda)

for ll_i = 1 to dw_lista_fabbisogni.rowcount()

	if dw_lista_fabbisogni.getitemstring(ll_i, "flag_ordine") = "N" then continue

	ls_cod_prodotto  = dw_lista_fabbisogni.getitemstring(ll_i, "cod_prodotto")
	ls_des_prodotto  = dw_lista_fabbisogni.getitemstring(ll_i, "des_prodotto")
	ls_cod_versione  = dw_lista_fabbisogni.getitemstring(ll_i, "cod_versione")
	ldd_quan_rda     = abs(dw_lista_fabbisogni.getitemnumber(ll_i, "quan_mancante"))
	ll_anno_commessa = dw_lista_fabbisogni.getitemnumber(ll_i, "anno_commessa")
	ll_num_commessa  = dw_lista_fabbisogni.getitemnumber(ll_i, "num_commessa")
	ldt_data_consegna = dw_lista_fabbisogni.getitemdatetime(ll_i, "data_fabbisogno")
	ls_cod_tipo_politica_riordino	= dw_lista_fabbisogni.getitemstring(ll_i, "cod_politica_riordino")
	ls_flag_tipo_riga	= dw_lista_fabbisogni.getitemstring(ll_i, "flag_tipo_riga")
	
	choose case ls_flag_tipo_riga
		case "1"
			ls_flag_gen_commessa = "N"
			setnull(ls_note)
		case "2"
			ls_flag_gen_commessa = "N"
			ls_note = "Ordine di lavorazione codice " + ls_cod_prodotto + " " + ls_des_prodotto + " Vers. " + ls_cod_versione
			ls_des_prodotto = ls_cod_prodotto +  " " + ls_des_prodotto
			ls_des_prodotto = left( ls_des_prodotto, 40)
			setnull(ls_cod_prodotto)
			
		case "3"
			ls_flag_gen_commessa = "S"
			ls_note = "Produzione C/Terzista codice " + ls_cod_prodotto + " " + ls_des_prodotto + " Vers. " + ls_cod_versione
	end choose
	
	ll_ret = luo_gen_doc.uof_genera_rda(ls_cod_prodotto, &
	 												ls_des_prodotto, &
													ls_cod_versione, &
													ldd_quan_rda, &
													ll_anno_commessa, &
													ll_num_commessa, &
													ldt_data_consegna, &
													ls_cod_tipo_politica_riordino, &
													ls_flag_gen_commessa, &
													ls_note, &
													ref ll_anno_reg_rda, &
													ref ll_num_reg_rda, &
													ref ls_errore)

	if ll_ret < 0 then
		g_mb.messagebox("SEP","Errore durante generazione RDA.~r~n" +ls_errore,stopsign!)
		rollback;
		exit
	end if
next
destroy luo_gen_doc

commit;

return
end event

type cb_gen_rda_no_mrp from commandbutton within w_analisi_fabbisogni
integer x = 3730
integer y = 2352
integer width = 366
integer height = 92
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen. RDA"
end type

event clicked;string ls_cod_prodotto, ls_cod_versione,ls_cod_tipo_politica_riordino, ls_errore, ls_note, ls_flag_gen_commessa, ls_des_prodotto, ls_flag_tipo_riga, &
		ls_cod_fornitore, ls_null ,ls_des_specifica, ls_message
long ll_i, ll_anno_ordine,ll_num_ordine, ll_prog_riga_ord_ven, ll_ret, ll_anno_reg_rda, ll_num_reg_rda, ll_null, ll_num_reg_rda_old
dec{4} ldd_quan_rda, ld_prezzo_vendita, ld_sconto_1, ld_sconto_2, ld_sconto_tot
datetime ldt_data_consegna
uo_generazione_documenti luo_gen_doc
										
setnull(ll_null)
setnull(ls_null)

if g_mb.messagebox("SEP","Procedo con la creazione RDA ?",Question!,YesNo!,2) = 2 then return

luo_gen_doc = create uo_generazione_documenti 

setnull(ll_anno_reg_rda)
setnull(ll_num_reg_rda)

ls_message = "Generato RDA " 
ll_num_reg_rda_old = 0

for ll_i = 1 to dw_tes_gen_commesse.rowcount()
	
	if dw_tes_gen_commesse.getitemstring(ll_i, "flag_calcola") = "N" then continue
	
	ll_anno_ordine  = dw_tes_gen_commesse.getitemnumber(ll_i, "anno_registrazione")
	ll_num_ordine  = dw_tes_gen_commesse.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ord_ven  = dw_tes_gen_commesse.getitemnumber(ll_i, "prog_riga_ord_ven")
	ls_cod_prodotto  = dw_tes_gen_commesse.getitemstring(ll_i, "cod_prodotto")
	ls_des_prodotto  = dw_tes_gen_commesse.getitemstring(ll_i, "des_prodotto")
	ls_cod_versione  = dw_tes_gen_commesse.getitemstring(ll_i, "cod_versione")
	ldt_data_consegna = dw_tes_gen_commesse.getitemdatetime(ll_i, "data_consegna")
	ls_cod_tipo_politica_riordino	= dw_tes_gen_commesse.getitemstring(ll_i, "cod_tipo_politica_riordino")
	ls_flag_gen_commessa = "N"

	select 	quan_ordine, prezzo_vendita, nota_dettaglio
	into 	:ldd_quan_rda, :ld_prezzo_vendita, :ls_note
	from 	det_ord_ven
	where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_ordine and
			num_registrazione = :ll_num_ordine and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
	select 	cod_fornitore
	into  	:ls_cod_fornitore
	from 	tab_tipi_politiche_riordino
	where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;
	if sqlca.sqlcode <> 0 then setnull(ls_cod_fornitore)
	
	luo_gen_doc.ib_carica_sconti_listini_fornitori=false
	//luo_gen_doc.ib_usa_prezzo_listino_fornitori = false
	luo_gen_doc.ib_usa_prezzo_listino_fornitori = true // stefanop 22/06/2012: richiesto da Beatrice,
	
	ls_des_specifica = string(ll_anno_ordine) + "-" + string(ll_num_ordine)
	
	// stefanop 22/06/2012: richiesto da Beatrice, l'RDA deve portarsi dietro le note del dettaglio
	
	ll_ret = luo_gen_doc.uof_genera_rda(ls_cod_prodotto, &
												ls_des_prodotto, &
												ls_cod_versione, &
												 ls_cod_fornitore, &
												ldd_quan_rda, &
												ld_prezzo_vendita, &
												ll_null, &
												ll_null, &
												ldt_data_consegna, &
												ls_cod_tipo_politica_riordino, &
												ls_flag_gen_commessa, &
												ls_note, &
												ls_des_specifica, &
												ll_anno_ordine, &
												ll_num_ordine,&
												ll_prog_riga_ord_ven,&
												ref ll_anno_reg_rda, &
												ref ll_num_reg_rda, &
												ref ls_errore)

	if ll_ret < 0 then
		g_mb.messagebox("SEP","Errore durante generazione RDA.~r~n" +ls_errore,stopsign!)
		rollback;
		return
	end if
	
	if ll_num_reg_rda <> ll_num_reg_rda_old then
		ls_message += string(ll_num_reg_rda)
		if ll_num_reg_rda_old <> 0 then ls_message +=  + " - "
	end if
	
	ll_num_reg_rda_old = ll_num_reg_rda

	update 	det_ord_ven
	set 		flag_elaborato_rda = 'S'
	where		cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_ordine and
				num_registrazione = :ll_num_ordine and
				prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SEP","Errore in fase di conferma elaborazione RDA nella riga d'ordine. Dettaglio:~r~n" + sqlca.sqlerrtext,stopsign!)
		rollback;
		return
	end if
	
next
destroy luo_gen_doc

commit;

g_mb.messagebox("APICE", ls_message)

return
end event

type dw_folder from u_folder within w_analisi_fabbisogni
integer x = 23
integer y = 20
integer width = 4526
integer height = 2436
integer taborder = 30
boolean border = false
end type

type dw_ext_selezione_gen_commesse from uo_cs_xx_dw within w_analisi_fabbisogni
integer x = 69
integer y = 140
integer width = 2697
integer height = 1184
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_analisi_fabbisogni_selezione_ext"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_null


setnull(ls_null)

if isvalid(dwo) then
	choose case dwo.name
		case "flag_solo_ordini"
			if data = "S" then
				setitem(getrow(),"cod_tipo_politica_riordino",ls_null)
				setitem(getrow(),"num_reg_ord_ven_inizio", 0)
				setitem(getrow(),"num_reg_ord_ven_fine", 999999)
				setitem(getrow(),"anno_reg_ord_ven", f_anno_esercizio() )
				setitem(getrow(),"flag_solo_mancanti", "S")
				setitem(getrow(),"num_commessa_inizio", 0)
				setitem(getrow(),"num_commessa_fine", 0)
				setitem(getrow(),"anno_commessa", 0)
			else
				setitem(getrow(),"cod_tipo_politica_riordino",ls_null)
				setitem(getrow(),"num_reg_ord_ven_inizio", 0)
				setitem(getrow(),"num_reg_ord_ven_fine", 0)
				setitem(getrow(),"anno_reg_ord_ven", 0 )
				setitem(getrow(),"flag_solo_mancanti", "I")
				setitem(getrow(),"num_commessa_inizio", 0)
				setitem(getrow(),"num_commessa_fine", 999999)
				setitem(getrow(),"anno_commessa", f_anno_esercizio())
			end if					
	end choose		
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	setitem(getrow(),"anno_reg_ord_ven", f_anno_esercizio() )
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ext_selezione_gen_commesse,"cod_cliente")
end choose
end event

type dw_tes_gen_commesse from uo_cs_xx_dw within w_analisi_fabbisogni
event pcd_retrieve pbm_custom60
integer x = 69
integer y = 140
integer width = 4421
integer height = 2204
integer taborder = 40
string dataobject = "d_analisi_fabbisogni_lista_commesse_1"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;boolean lb_ok
string 	ls_cod_cliente, ls_sql, ls_rag_soc_1, ls_cod_tipo_commessa, ls_cod_prodotto, ls_des_prodotto, &
       	ls_cod_versione, ls_des_versione, ls_cod_deposito, ls_cod_cliente_selezione, ls_flag_solo_ordini, &
		ls_cod_tipo_politica_riordino_selezione,ls_cod_tipo_politica_riordino,ls_des_tipo_politica_riordino, &
		ls_flag_tipo_riordino, ls_flag_modo_riordino, ls_flag_copia_de_prod_ord_ven, ls_colonna_des_prodotto
long 	ll_anno_commessa, ll_num_commessa, ll_num_comm_inizio, ll_num_comm_fine, ll_anno_registrazione, &
     		ll_num_registrazione, ll_i, ll_max, ll_y, ll_z, ll_prog_riga_ord_ven, ll_row, &
	  	ll_anno_ordine_sel, ll_num_ordine_inizio, ll_num_ordine_fine, ll_anno_reg_ord_ven, ll_num_reg_ord_ven
dec{4} ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa
datetime ldt_data_registrazione,ldt_data_consegna_inizio,ldt_data_consegna_fine, ldt_data_reg_inizio, ldt_data_reg_fine, &
         ldt_data_consegna, ldt_data_neutra
			
ldt_data_neutra = datetime(date("19000101"), time("00:00:000"))

ldt_data_consegna_inizio = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_inizio")
ldt_data_consegna_fine = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_consegna_fine")
ldt_data_reg_inizio = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_registrazione_inizio")
ldt_data_reg_fine = dw_ext_selezione_gen_commesse.getitemdatetime(dw_ext_selezione_gen_commesse.getrow(),"data_registrazione_fine")
ls_cod_cliente_selezione = dw_ext_selezione_gen_commesse.getitemstring(dw_ext_selezione_gen_commesse.getrow(),"cod_cliente")
ll_anno_commessa = dw_ext_selezione_gen_commesse.getitemnumber(dw_ext_selezione_gen_commesse.getrow(),"anno_commessa")
ll_num_comm_inizio = dw_ext_selezione_gen_commesse.getitemnumber(dw_ext_selezione_gen_commesse.getrow(),"num_commessa_inizio")
ll_num_comm_fine = dw_ext_selezione_gen_commesse.getitemnumber(dw_ext_selezione_gen_commesse.getrow(),"num_commessa_fine")
ls_cod_deposito = dw_ext_selezione_gen_commesse.getitemstring(dw_ext_selezione_gen_commesse.getrow(),"cod_deposito")

ls_cod_tipo_politica_riordino_selezione = dw_ext_selezione_gen_commesse.getitemstring(dw_ext_selezione_gen_commesse.getrow(),"cod_tipo_politica_riordino")
ls_flag_solo_ordini = dw_ext_selezione_gen_commesse.getitemstring(dw_ext_selezione_gen_commesse.getrow(),"flag_solo_ordini")
ll_anno_ordine_sel = dw_ext_selezione_gen_commesse.getitemnumber(dw_ext_selezione_gen_commesse.getrow(),"anno_reg_ord_ven")
ll_num_ordine_inizio = dw_ext_selezione_gen_commesse.getitemnumber(dw_ext_selezione_gen_commesse.getrow(),"num_reg_ord_ven_inizio")
ll_num_ordine_fine = dw_ext_selezione_gen_commesse.getitemnumber(dw_ext_selezione_gen_commesse.getrow(),"num_reg_ord_ven_fine")

dw_tes_gen_commesse.reset()

select flag_copia_de_prod_ord_ven
into 	:ls_flag_copia_de_prod_ord_ven
from	con_rda
where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode = 100 then
	g_mb.messagebox("Apice","Attenzione: parametri RDA non impostati.")
	rollback;
	return
end if

if ls_flag_copia_de_prod_ord_ven = "S" then
	ls_colonna_des_prodotto = "det_ord_ven.des_prodotto"
else
	ls_colonna_des_prodotto = "anag_prodotti.des_prodotto"
end if	

if isnull(ldt_data_consegna_inizio) then ldt_data_consegna_inizio = datetime(date("01/01/1900"),00:00:00)
if isnull(ldt_data_consegna_fine) then ldt_data_consegna_fine = datetime(date("31/12/2099"),00:00:00)
if isnull(ldt_data_reg_inizio) then ldt_data_reg_inizio = datetime(date("01/01/1900"),00:00:00)
if isnull(ldt_data_reg_fine) then ldt_data_reg_fine = datetime(date("31/12/2099"),00:00:00)

declare cu_commesse dynamic cursor for sqlsa;

// esamino le righe degli ordini che non generano commesse (proodtti commercializzati)
if ls_flag_solo_ordini = "S" then
	
	/* 	14-02-2017 appaiono solo gli ordini che 
	1) non generano la commessa di produzione
	2) non sono passati in RDA
	3) con tipo riordino ACQUISTO */

	ls_sql=" SELECT det_ord_ven.anno_registrazione, det_ord_ven.num_registrazione, det_ord_ven.prog_riga_ord_ven, tes_ord_ven.data_registrazione, tes_ord_ven.data_consegna, det_ord_ven.cod_prodotto, det_ord_ven.cod_versione, det_ord_ven.quan_ordine, det_ord_ven.quan_in_evasione, det_ord_ven.quan_evasa, " + ls_colonna_des_prodotto + ", tab_tipi_politiche_riordino.cod_tipo_politica_riordino, tab_tipi_politiche_riordino.des_tipo_politica_riordino, tes_ord_ven.cod_cliente  " + &
			" FROM det_ord_ven  " + &
			" join tes_ord_ven   on tes_ord_ven.cod_azienda = det_ord_ven.cod_azienda and tes_ord_ven.anno_registrazione = det_ord_ven.anno_registrazione and tes_ord_ven.num_registrazione = det_ord_ven.num_registrazione " + &
			" join anag_prodotti ON anag_prodotti.cod_azienda = det_ord_ven.cod_azienda AND anag_prodotti.cod_prodotto = det_ord_ven.cod_prodotto   " + &
			" join tab_tipi_politiche_riordino on anag_prodotti.cod_azienda =  tab_tipi_politiche_riordino.cod_azienda and anag_prodotti.cod_tipo_politica_riordino =  tab_tipi_politiche_riordino.cod_tipo_politica_riordino  " + &
			" WHERE  det_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
			" and det_ord_ven.flag_gen_commessa = 'N' and " + & 
			" det_ord_ven.flag_elaborato_rda = 'N' and " + & 
			" tab_tipi_politiche_riordino.flag_tipo_riordino = 'A'  "
			//and " + &
			//" tab_tipi_politiche_riordino.flag_modo_riordino = 'C'   "

	if not isnull(ldt_data_consegna_inizio) and ldt_data_consegna_inizio > ldt_data_neutra then
		ls_sql += " and (tes_ord_ven.data_consegna >= '" + string(ldt_data_consegna_inizio, s_cs_xx.db_funzioni.formato_data) + "') "
	end if
	
	if not isnull(ldt_data_consegna_fine) and ldt_data_consegna_fine > ldt_data_neutra then
		ls_sql += " and (tes_ord_ven.data_consegna <= '" + string(ldt_data_consegna_fine, s_cs_xx.db_funzioni.formato_data) + "') "
	end if
	
	if not isnull(ldt_data_reg_inizio) and ldt_data_reg_inizio > ldt_data_neutra then
		ls_sql += " and (tes_ord_ven.data_registrazione >= '" + string(ldt_data_reg_inizio, s_cs_xx.db_funzioni.formato_data) + "') "
	end if
	
	if not isnull(ldt_data_reg_fine) and ldt_data_reg_fine > ldt_data_neutra then
		ls_sql += " and (tes_ord_ven.data_registrazione <= '" + string(ldt_data_reg_fine, s_cs_xx.db_funzioni.formato_data) + "') "
	end if

	if not isnull(ls_cod_cliente_selezione) then
		ls_sql += " and (tes_ord_ven.cod_cliente = '" + ls_cod_cliente_selezione + "') "
	end if
	
	if not isnull(ll_anno_ordine_sel) and ll_anno_ordine_sel > 0 then
		ls_sql += " and (tes_ord_ven.anno_registrazione = " + string(ll_anno_ordine_sel) + ") "
	end if
	
	if not isnull(ll_num_ordine_inizio) and ll_num_ordine_inizio > 0 then
		ls_sql += " and (tes_ord_ven.num_registrazione >= " + string(ll_num_ordine_inizio) + ") "
	end if
	
	if not isnull(ll_num_ordine_fine) and ll_num_ordine_fine > 0 then
		ls_sql += " and (tes_ord_ven.num_registrazione <= " + string(ll_num_ordine_fine) + ") "
	end if

	if not isnull(ls_cod_tipo_politica_riordino_selezione) then
		ls_sql += " and (anag_prodotti.cod_tipo_politica_riordino= '" + ls_cod_tipo_politica_riordino_selezione + "') "
	end if
		
	PREPARE SQLSA FROM :ls_sql;
	
	open dynamic cu_commesse;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore durante la creazione del cursore cu_commesse. Dettaglio " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	do while true
		
		fetch cu_commesse into  :ll_anno_reg_ord_ven,   
										:ll_num_reg_ord_ven,   
										:ll_prog_riga_ord_ven,
										:ldt_data_registrazione,   
										:ldt_data_consegna,
										:ls_cod_prodotto,
										:ls_cod_versione,
										:ld_quan_ordine,
										:ld_quan_in_evasione,
										:ld_quan_evasa,
										:ls_des_prodotto,
										:ls_cod_tipo_politica_riordino,
										:ls_des_tipo_politica_riordino,
										:ls_cod_cliente;
	
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore durante la fetch del cursore cu_commesse. ~r~nDettaglio " + sqlca.sqlerrtext)
			close cu_commesse;
			rollback;
			return
		end if
		
		ll_row = dw_tes_gen_commesse.insertrow(0)
		dw_tes_gen_commesse.setitem(ll_row, "anno_registrazione", ll_anno_reg_ord_ven)
		dw_tes_gen_commesse.setitem(ll_row, "num_registrazione", ll_num_reg_ord_ven)
		dw_tes_gen_commesse.setitem(ll_row, "prog_riga_ord_ven", ll_prog_riga_ord_ven)
		dw_tes_gen_commesse.setitem(ll_row, "data_registrazione", ldt_data_registrazione)
		dw_tes_gen_commesse.setitem(ll_row, "data_consegna", ldt_data_consegna)
		
		dw_tes_gen_commesse.setitem(ll_row, "cod_prodotto", ls_cod_prodotto)
		dw_tes_gen_commesse.setitem(ll_row, "des_prodotto", ls_des_prodotto)
		dw_tes_gen_commesse.setitem(ll_row, "cod_versione", ls_cod_versione)
		
		
		setnull(ls_rag_soc_1)
		
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
					 
		dw_tes_gen_commesse.setitem(ll_row, "cod_cliente", ls_cod_cliente)
		dw_tes_gen_commesse.setitem(ll_row, "rag_soc_1", ls_rag_soc_1)
		dw_tes_gen_commesse.setitem(ll_row, "cod_tipo_politica_riordino", ls_cod_tipo_politica_riordino)
		dw_tes_gen_commesse.setitem(ll_row, "des_tipo_politica_riordino", ls_des_tipo_politica_riordino)
		
		select flag_tipo_riordino,
				flag_modo_riordino
		into   :ls_flag_tipo_riordino,
				:ls_flag_modo_riordino
		from 	tab_tipi_politiche_riordino
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in ricerca politica riordino~r~n" + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
		
		dw_tes_gen_commesse.setitem(ll_row, "flag_elaborabile_mrp", "S")
		/*
		// ENRICO 08/04/2011: nel caso in cui un prodotto abbia la politica di riordino con flag_tipo_riordino="A" (acquisto) e flag_modo_riordino="C"(Commessa)
		// significa che ogni volta che un cliente me lo ordina io lo devo comprare indipendentemente  dalla giacenza presente in azienda.
		// Questo è il caso dei prodotti ordinati a MV Line che vengono comprati su commessa del cliente, ma vengono poi acquistati da MVLine.
		
		if ls_flag_tipo_riordino = "A" and ls_flag_modo_riordino ="C" then
			dw_tes_gen_commesse.setitem(ll_row, "flag_elaborabile_mrp", "N")
		else
			dw_tes_gen_commesse.setitem(ll_row, "flag_elaborabile_mrp", "S")
		end if
		*/
		
		// aggiungere il confronto con la giacenza.
	loop
	
	close cu_commesse;
	
end if


if ls_flag_solo_ordini = "N" then

	ls_sql = " select anno_commessa, num_commessa, data_registrazione, cod_tipo_commessa, data_consegna, cod_prodotto, cod_versione from anag_commesse  " + &
				" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
				" flag_tipo_avanzamento = '0' and " + &
				" (data_consegna between '" + string(ldt_data_consegna_inizio,s_cs_xx.db_funzioni.formato_data) + "' and '" + string(ldt_data_consegna_fine,s_cs_xx.db_funzioni.formato_data) + "') "
				
	if ll_anno_commessa > 0 and not isnull(ll_anno_commessa) then
		
		ls_sql += " and anno_commessa = " + string(ll_anno_commessa)
	
		if ll_num_comm_inizio > 0 and not isnull(ll_num_comm_inizio) then
			ls_sql += " and num_commessa >= " + string(ll_num_comm_inizio)
		end if
	
		if ll_num_comm_fine > 0 and not isnull(ll_num_comm_fine) then
			ls_sql += " and num_commessa <= " + string(ll_num_comm_fine)
		end if
		
	end if
	
	if not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then
		ls_sql += " and cod_deposito_prelievo = '" + ls_cod_deposito + "' " 
	end if
	
	ll_z = 0
	ll_y = 0
	ls_sql = ls_sql + " order by anno_commessa asc, num_commessa asc"
	
	PREPARE SQLSA FROM :ls_sql;
	
	open dynamic cu_commesse;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore durante la creazione del cursore cu_commesse. Dettaglio " + sqlca.sqlerrtext)
		return
	end if
	do while 0 = 0
		fetch cu_commesse into  :ll_anno_commessa,   
										:ll_num_commessa,   
										:ldt_data_registrazione,   
										:ls_cod_tipo_commessa,
										:ldt_data_consegna,
										:ls_cod_prodotto,
										:ls_cod_versione;
	
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore durante la fetch del cursore cu_commesse. Dettaglio " + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		// trovo l'eventuale ordine di origine
		
		ll_anno_registrazione = 0
		ll_num_registrazione = 0
		ll_prog_riga_ord_ven = 0
		setnull(ls_des_prodotto)
		
		select anno_registrazione,
				 num_registrazione,
				 prog_riga_ord_ven,
				 des_prodotto
		into   :ll_anno_registrazione,
				 :ll_num_registrazione,
				 :ll_prog_riga_ord_ven,
				 :ls_des_prodotto
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_commessa = :ll_anno_commessa and 
				 num_commessa = :ll_num_commessa;
	
		if sqlca.sqlcode = 0 then
			setnull(ls_cod_cliente)
			select cod_cliente
			into   :ls_cod_cliente
			from   tes_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 anno_registrazione = :ll_anno_registrazione and 
					 num_registrazione = :ll_num_registrazione;
					 
			if not isnull(ls_cod_cliente_selezione) then 
				if ls_cod_cliente_selezione <> ls_cod_cliente then continue
			end if
			
			
			if sqlca.sqlcode = 0 then
				setnull(ls_rag_soc_1)
				select rag_soc_1
				into   :ls_rag_soc_1
				from   anag_clienti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_cliente = :ls_cod_cliente;
			else
				ls_rag_soc_1 = "COMMESSA INTERNA"
			end if
		end if
		
		ll_row = dw_tes_gen_commesse.insertrow(0)

		select des_versione
		into   :ls_des_versione
		from   distinta_padri
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto  and
				 cod_versione = :ls_cod_versione;
		
		if sqlca.sqlcode <> 0 then 
			dw_tes_gen_commesse.setitem(ll_row, "flag_elaborabile_mrp", "N")
			ls_des_versione = ""
		else
			dw_tes_gen_commesse.setitem(ll_row, "flag_elaborabile_mrp", "S")
		end if
	
		dw_tes_gen_commesse.setitem(ll_row, "anno_commessa", ll_anno_commessa)
		dw_tes_gen_commesse.setitem(ll_row, "num_commessa", ll_num_commessa)
		dw_tes_gen_commesse.setitem(ll_row, "anno_registrazione", ll_anno_registrazione)
		dw_tes_gen_commesse.setitem(ll_row, "num_registrazione", ll_num_registrazione)
		dw_tes_gen_commesse.setitem(ll_row, "prog_riga_ord_ven", ll_prog_riga_ord_ven)
		
		dw_tes_gen_commesse.setitem(ll_row, "cod_prodotto", ls_cod_prodotto)
		dw_tes_gen_commesse.setitem(ll_row, "des_prodotto", ls_des_prodotto)
		dw_tes_gen_commesse.setitem(ll_row, "cod_versione", ls_cod_versione)
		
		dw_tes_gen_commesse.setitem(ll_row, "data_registrazione", ldt_data_registrazione)
		dw_tes_gen_commesse.setitem(ll_row, "data_consegna", ldt_data_consegna)
	
		dw_tes_gen_commesse.setitem(ll_row, "cod_cliente", ls_cod_cliente)
		dw_tes_gen_commesse.setitem(ll_row, "rag_soc_1", ls_rag_soc_1)
		
	loop
	close cu_commesse;
	
end if

resetupdate()
commit;

dw_tes_gen_commesse.groupcalc()
end event

event doubleclicked;string ls_flag_calcola
long ll_i, ll_anno_registrazione, ll_num_registrazione

if isvalid(dwo) then
	choose case dwo.name
		case "flag_calcola_t"
			
			if rowcount() > 0 then
				ls_flag_calcola = getitemstring(1, "flag_calcola")
				
				for ll_i = 1 to rowcount()
					if ls_flag_calcola = "S" then
						setitem(ll_i, "flag_calcola", "N")
					else
						setitem(ll_i, "flag_calcola", "S")
					end if
				next
			end if
		
		case "cf_ordine"
			
			if rowcount() > 0 then
				if row < 1 then return
				ls_flag_calcola = getitemstring(row, "flag_calcola")
				ll_anno_registrazione = getitemnumber(row, "anno_registrazione")
				ll_num_registrazione = getitemnumber(row, "num_registrazione")
				
				for ll_i = 1 to rowcount()
					if ( ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione")) and ( ll_num_registrazione = getitemnumber(ll_i, "num_registrazione")) then
						if ls_flag_calcola = "S" then
							setitem(ll_i, "flag_calcola", "N")
						else
							setitem(ll_i, "flag_calcola", "S")
						end if
					end if
				next
			end if
		
	case else
		
		call super::doubleclicked
		
	end choose
end if


end event

type dw_lista_fabbisogni from uo_cs_xx_dw within w_analisi_fabbisogni
integer x = 69
integer y = 140
integer width = 4421
integer height = 2208
integer taborder = 10
string dataobject = "d_analisi_fabbisogni_lista_fabbisogni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;if row > 0 then
	s_cs_xx.parametri.parametro_i_1 = row
	s_cs_xx.parametri.parametro_s_9 = dw_ext_selezione_gen_commesse.getitemstring( dw_ext_selezione_gen_commesse.getrow(), "cod_deposito")
	window_open_parm(w_analisi_fabbisogni_documenti, -1, dw_lista_fabbisogni)
end if
end event

type cb_elabora from commandbutton within w_analisi_fabbisogni
integer x = 4105
integer y = 2352
integer width = 384
integer height = 92
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora MRP"
end type

event clicked;boolean  lb_flag_trovato, lb_aggiungi
string   	ls_cod_prodotto_finito,ls_des_prodotto,ls_cod_versione, ls_messaggio, ls_errore, ls_cod_tipo_politica_riordino, ls_flag_solo_mancanti, &
			ls_cod_deposito, ls_cp, ls_vr, ls_flag_tipo_riordino,  ls_null, ls_cod_prodotto, ls_flag_modo_riordino
integer  li_risposta
dec{4}   ldd_quan_ordinare,ldd_quan_ordine, ldd_quan_mancante, ldd_disponibilita_alla_data, ld_soglia_riordino, &
         	ldd_quan_riordino, ldd_quan_old, ldd_1, ldd_2
long     ll_t, ll_mp, ll_riga, ll_lead_time_cumulato, ll_prog_riga_ord_ven, ll_row, &
			ll_anno_registrazione,ll_num_registrazione,ll_anno_commessa,ll_num_commessa, ll_riga_riferimento, ll_ctrl, ll_an, ll_nu
datetime ldt_data_consegna, ldt_df
date     ld_data
uo_mrp luo_mrp
s_fabbisogno_commessa lstr_fabbisogno[], lstr_vuoto[]

dw_ext_selezione_gen_commesse.accepttext()
setnull(ls_null)

ls_cod_deposito = dw_ext_selezione_gen_commesse.getitemstring( dw_ext_selezione_gen_commesse.getrow(), "cod_deposito")

dw_folder.fu_selecttab(3)
setpointer(hourglass!)
dw_lista_fabbisogni.reset()
dw_lista_fabbisogni.change_dw_current()
dw_lista_fabbisogni.setredraw(false)
hpb_1.position = 0

luo_mrp = create uo_mrp

for ll_riga = 1 to dw_tes_gen_commesse.rowcount()
	
	// elaboro solo quelli selezionati
	if dw_tes_gen_commesse.getitemstring(ll_riga,"flag_calcola") = "N" then continue
	
	ll_anno_registrazione = dw_tes_gen_commesse.getitemnumber(ll_riga,"anno_registrazione")	
	ll_num_registrazione = dw_tes_gen_commesse.getitemnumber(ll_riga,"num_registrazione")	
	ll_anno_commessa = dw_tes_gen_commesse.getitemnumber(ll_riga,"anno_commessa")	
	ll_num_commessa = dw_tes_gen_commesse.getitemnumber(ll_riga,"num_commessa")	
	ll_prog_riga_ord_ven = dw_tes_gen_commesse.getitemnumber(ll_riga,"prog_riga_ord_ven")
	ls_cod_tipo_politica_riordino = dw_tes_gen_commesse.getitemstring(ll_riga,"cod_tipo_politica_riordino")
	ldt_data_consegna = dw_tes_gen_commesse.getitemdatetime(ll_riga,"data_consegna")
	
	select flag_tipo_riordino,
	  		flag_modo_riordino
	into   :ls_flag_tipo_riordino,
			:ls_flag_modo_riordino
	from 	tab_tipi_politiche_riordino
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in ricerca politica riordino~r~n" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	select  	cod_prodotto,
			  	quan_ordine,
			  	cod_versione
	into  		:ls_cod_prodotto_finito,
				:ldd_quan_ordine,	
				:ls_cod_versione
	from    	det_ord_ven
	where	cod_azienda = :s_cs_xx.cod_azienda	and     
				anno_registrazione = :ll_anno_registrazione	and     
				num_registrazione = :ll_num_registrazione and
				prog_riga_ord_ven = :ll_prog_riga_ord_ven;

	if sqlca.sqlcode < 0 then
		g_mb.error(g_str.format("Errore in ricerca riga ordine: $1", sqlca.sqlerrtext))
		return
	end if
	
	if not isnull(ls_cod_versione) and len(ls_cod_versione) > 0 then
		// se ha la distinta base
		lstr_fabbisogno = lstr_vuoto
	
		luo_mrp.uof_calcolo_fabbisogni_comm(	true, &
															"varianti_det_ord_ven", &
															ll_anno_registrazione, &
															ll_num_registrazione, &
															ll_prog_riga_ord_ven, &
															ls_cod_prodotto_finito, &
															ls_cod_versione, &
															ldd_quan_ordine, &
															0, &
															ls_null,&
															ldt_data_consegna,&
															ref ll_lead_time_cumulato, &
															ref lstr_fabbisogno[], &
															ref ls_messaggio )		
		if li_risposta = -1 then
			g_mb.error(ls_messaggio)
			return
		end if
	else
		lstr_fabbisogno[1].cod_prodotto = ls_cod_prodotto_finito
		lstr_fabbisogno[1].cod_versione = ls_cod_versione
		lstr_fabbisogno[1].semilavorato = false
		lstr_fabbisogno[1].lead_time = 0
		lstr_fabbisogno[1].quan_fabbisogno = ldd_quan_ordine
		lstr_fabbisogno[1].flag_mp = "N"
		lstr_fabbisogno[1].flag_ordine_servizio = "N"
		setnull(lstr_fabbisogno[1].anno_commessa)
		setnull(lstr_fabbisogno[1].num_commessa)
		setnull(lstr_fabbisogno[1].cod_reparto)
		lstr_fabbisogno[1].data_fabbisogno = ldt_data_consegna
	end if
	
	for ll_mp = 1 to upperbound(lstr_fabbisogno[])
		
		hpb_1.position = int((ll_mp * 100 ) / upperbound(lstr_fabbisogno[]))

		// se è un semilavorato e se non c'è flag_mp e non c'è fase, 
		// non necessita di fabbisogno e quindi non lo estraggo
		if lstr_fabbisogno[ll_mp].semilavorato and lstr_fabbisogno[ll_mp].flag_mp = "N" and lstr_fabbisogno[ll_mp].flag_ordine_servizio ="N" then continue
		
		// calcolo data fabbisogno
		if  isnull(lstr_fabbisogno[ll_mp].lead_time) then lstr_fabbisogno[ll_mp].lead_time = 0
		ldt_data_consegna = dw_tes_gen_commesse.getitemdatetime(ll_riga,"data_consegna")	
		ld_data = date(ldt_data_consegna)
		ld_data = relativedate(ld_data, lstr_fabbisogno[ll_mp].lead_time * -1)
		ldt_data_consegna = datetime(ld_data, 00:00:00)
		
		lb_aggiungi = false
		ll_riga_riferimento = 0
		
		for ll_ctrl = 1 to dw_lista_fabbisogni.rowcount()
			
			ll_an = dw_lista_fabbisogni.getitemnumber( ll_ctrl, "anno_commessa")
			ll_nu = dw_lista_fabbisogni.getitemnumber( ll_ctrl, "num_commessa")
			ls_cp = dw_lista_fabbisogni.getitemstring( ll_ctrl, "cod_prodotto")
			ls_vr = dw_lista_fabbisogni.getitemstring( ll_ctrl, "cod_versione")
			ldt_df = dw_lista_fabbisogni.getitemdatetime( ll_ctrl, "data_fabbisogno")
			
			if ll_an = ll_anno_commessa and ll_nu = ll_num_commessa and ls_cp = lstr_fabbisogno[ll_mp].cod_prodotto and ls_vr = lstr_fabbisogno[ll_mp].cod_versione and ldt_df = ldt_data_consegna then
				lb_aggiungi = true
				ll_riga_riferimento = ll_ctrl
				exit
			end if
			
		next
		
		if lb_aggiungi and ll_riga_riferimento > 0 then
			
			if luo_mrp.uof_disponibilita_alla_data(lstr_fabbisogno[ll_mp].cod_prodotto, &
																ldt_data_consegna, &
																ls_cod_deposito, &
																ref ldd_disponibilita_alla_data, &
																ref ls_messaggio) <> 0 then
				g_mb.messagebox("SEP","Errore in calcolo disponibilità alla data~r~n" + ls_messaggio,Stopsign!)
				return
			end if				
			
			ldd_quan_old = dw_lista_fabbisogni.getitemnumber( ll_riga_riferimento, "quan_fabbisogno")
			if isnull(ldd_quan_old) then ldd_quan_old = 0
			ldd_quan_old += lstr_fabbisogno[ll_mp].quan_fabbisogno
			
			dw_lista_fabbisogni.setitem( ll_riga_riferimento, "quan_fabbisogno", ldd_quan_old)
			
			ldd_1 = ldd_quan_old
			
			ldd_quan_old = dw_lista_fabbisogni.getitemnumber( ll_riga_riferimento, "disponibilita_alla_data")
			if isnull(ldd_quan_old) then ldd_quan_old = 0
			
			ldd_2 = ldd_quan_old
			
			ldd_quan_old = dw_lista_fabbisogni.getitemnumber( ll_riga_riferimento, "quan_mancante")
			if isnull(ldd_quan_old) then ldd_quan_old = 0
			
			ldd_quan_riordino = ldd_2 - ldd_1
			
			dw_lista_fabbisogni.setitem(ll_riga_riferimento,"quan_mancante", ldd_quan_riordino)
			
		else
		
			ll_row  = dw_lista_fabbisogni.insertrow(0)
					
			if lstr_fabbisogno[ll_mp].flag_mp = "S" then
				dw_lista_fabbisogni.setitem(ll_row, "flag_tipo_riga","3")
			elseif lstr_fabbisogno[ll_mp].flag_ordine_servizio = "S" then
				dw_lista_fabbisogni.setitem(ll_row, "flag_tipo_riga","2")
			else
				dw_lista_fabbisogni.setitem(ll_row, "flag_tipo_riga","1")
			end if			
			
			dw_lista_fabbisogni.setitem(ll_row, "cod_prodotto",lstr_fabbisogno[ll_mp].cod_prodotto)
			dw_lista_fabbisogni.setitem(ll_row, "cod_versione",lstr_fabbisogno[ll_mp].cod_versione)
			dw_lista_fabbisogni.setitem(ll_row, "quan_fabbisogno",lstr_fabbisogno[ll_mp].quan_fabbisogno)
			dw_lista_fabbisogni.setitem(ll_row, "lead_time",lstr_fabbisogno[ll_mp].lead_time)
			dw_lista_fabbisogni.setitem(ll_row, "anno_commessa", ll_anno_commessa)
			dw_lista_fabbisogni.setitem(ll_row, "num_commessa", ll_num_commessa)

			// calcolo data fabbisogno
			ldt_data_consegna = dw_tes_gen_commesse.getitemdatetime(ll_riga,"data_consegna")	
			ld_data = date(ldt_data_consegna)
			ld_data = relativedate(ld_data, lstr_fabbisogno[ll_mp].lead_time * -1)
			ldt_data_consegna = datetime(ld_data, 00:00:00)
			dw_lista_fabbisogni.setitem(ll_row, "data_fabbisogno", ldt_data_consegna)
			ldd_disponibilita_alla_data = 0
			
			if luo_mrp.uof_disponibilita_alla_data(lstr_fabbisogno[ll_mp].cod_prodotto, &
																ldt_data_consegna, &
																ls_cod_deposito, &
																ref ldd_disponibilita_alla_data, &
																ref ls_messaggio) <> 0 then
				g_mb.messagebox("SEP","Errore in calcolo disponibilità alla data~r~n" + ls_messaggio,Stopsign!)
				return
			end if
	
			dw_lista_fabbisogni.setitem(ll_row,"disponibilita_alla_data",ldd_disponibilita_alla_data)
		
		end if
		
	next
		
next

// ricalcolo la q.ta mancante
if wf_ricacolo_quan_ordine(ls_errore) < 0 then
	g_mb.error(ls_errore)
end if

//	controllo il flag visualizza solo mancanti: in questo caso visualizzo i prodotti in base al valore del flag

ls_flag_solo_mancanti = dw_ext_selezione_gen_commesse.getitemstring( 1, "flag_solo_mancanti")
if not isnull(ls_flag_solo_mancanti) and ls_flag_solo_mancanti = 'S' then
	
	dw_lista_fabbisogni.SetFilter(" flag_ordine = 'S' ")
	dw_lista_fabbisogni.Filter()
	
elseif not isnull(ls_flag_solo_mancanti) and ls_flag_solo_mancanti = 'N' then
	
	dw_lista_fabbisogni.SetFilter(" flag_ordine = 'N' ")
	dw_lista_fabbisogni.Filter()	
	
end if

dw_lista_fabbisogni.settaborder("flag_ordine", 10 )
destroy luo_mrp

hpb_1.position = 0
dw_lista_fabbisogni.resetupdate()
dw_lista_fabbisogni.change_dw_current()
dw_lista_fabbisogni.setredraw(true)

setpointer(arrow!)
rollback;

end event

type cb_cerca from commandbutton within w_analisi_fabbisogni
integer x = 2263
integer y = 1252
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ext_selezione_gen_commesse.accepttext()
dw_tes_gen_commesse.change_dw_current()
parent.triggerevent("pc_retrieve")
dw_folder.fu_selecttab(2)
end event

