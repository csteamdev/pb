﻿$PBExportHeader$w_analisi_fabbisogni_documenti.srw
$PBExportComments$Finestra Testata Generazione Commesse
forward
global type w_analisi_fabbisogni_documenti from w_cs_xx_principale
end type
type dw_analisi_fabbisogni_documenti from uo_cs_xx_dw within w_analisi_fabbisogni_documenti
end type
end forward

global type w_analisi_fabbisogni_documenti from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3145
integer height = 1652
string title = "Dettaglio Fabbisogno"
event ue_close ( )
dw_analisi_fabbisogni_documenti dw_analisi_fabbisogni_documenti
end type
global w_analisi_fabbisogni_documenti w_analisi_fabbisogni_documenti

type variables
string is_cod_deposito, is_cod_prodotto
long   il_row
datetime idt_data_riferimento

end variables

event ue_close();close(this)
end event

event pc_setwindow;call super::pc_setwindow;uo_cs_xx_dw luo_cs_xx_dw

dw_analisi_fabbisogni_documenti.set_dw_options(sqlca, &
                        	 pcca.null_object, &
                            c_multiselect + &
									 c_nonew + &
									 c_modifyonopen + &
									 c_nodelete + &
									 c_disablecc + &
									 c_noretrieveonopen + &
									 c_disableccinsert , &
									 c_InactiveDWColorUnchanged + &
									 c_NoCursorRowPointer + &
									 c_NoCursorRowFocusRect + &
									 c_ViewModeBorderUnchanged + &
									 c_ViewModeColorUnchanged)


luo_cs_xx_dw = message.powerobjectparm

il_row = s_cs_xx.parametri.parametro_i_1

idt_data_riferimento = luo_cs_xx_dw.getitemdatetime(il_row, "data_fabbisogno")

is_cod_prodotto = luo_cs_xx_dw.getitemstring(il_row, "cod_prodotto")

is_cod_deposito = s_cs_xx.parametri.parametro_s_9
setnull(s_cs_xx.parametri.parametro_s_9)

if isnull(is_cod_deposito) or len(is_cod_deposito) < 1 then
	g_mb.messagebox("SEP","L'indicazione del deposito è OBBLIGATORIA")
	postevent("ue_close")
else
	iuo_dw_main = dw_analisi_fabbisogni_documenti
	postevent("pc_retrieve")
end if	
	
	

end event

on w_analisi_fabbisogni_documenti.create
int iCurrent
call super::create
this.dw_analisi_fabbisogni_documenti=create dw_analisi_fabbisogni_documenti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_analisi_fabbisogni_documenti
end on

on w_analisi_fabbisogni_documenti.destroy
call super::destroy
destroy(this.dw_analisi_fabbisogni_documenti)
end on

type dw_analisi_fabbisogni_documenti from uo_cs_xx_dw within w_analisi_fabbisogni_documenti
event ue_giacenza ( )
integer x = 18
integer y = 16
integer width = 3072
integer height = 1512
integer taborder = 10
string dataobject = "d_analisi_fabbisogni_deposito_documenti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_giacenza();// calcolo la giacenza

string ls_where,ls_errore,ls_chiave[]
long ll_ret
dec{4} ld_quantita, ld_giacenza, ld_quantita_giacenza[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime ldt_data_riferimento
uo_magazzino luo_magazzino


ldt_data_riferimento = datetime(today(),00:00:00)

ls_where = ""

for ll_ret = 1 to 14
	ld_quantita_giacenza[ll_ret] = 0
next	

luo_magazzino = CREATE uo_magazzino
	
ll_ret = luo_magazzino.uof_saldo_prod_date_decimal(Is_cod_prodotto, ldt_data_riferimento, ls_where, ld_quantita_giacenza, ls_errore, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])

destroy luo_magazzino	

for ll_ret = 1 to upperbound(ls_chiave)
	if ls_chiave[ll_ret] = is_cod_deposito then
		ld_giacenza = ld_giacenza_stock[ll_ret]
	end if
next	

if isnull(ld_giacenza) then ld_giacenza = 0

ld_quantita = ld_giacenza + ld_quantita

destroy luo_magazzino


ll_ret = dw_analisi_fabbisogni_documenti.insertrow(1)
dw_analisi_fabbisogni_documenti.setitem(ll_ret, "deposito", is_cod_deposito)
dw_analisi_fabbisogni_documenti.setitem(ll_ret, "data_riferimento", datetime(today(),00:00:00))
dw_analisi_fabbisogni_documenti.setitem(ll_ret, "expression1", "ANAG_PRODOTTI")
dw_analisi_fabbisogni_documenti.setitem(ll_ret, "quantita", ld_quantita)
dw_analisi_fabbisogni_documenti.resetupdate()

dw_analisi_fabbisogni_documenti.Object.data_riferimento.Color = "0~tif( DATE(data_riferimento) < today(), rgb(255,0,0), if( DATE(data_riferimento) > today(), rgb(0,0,255), rgb(0,0,0))    )"

end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_deposito
long ll_errore, ll_row

ls_cod_deposito = is_cod_deposito
if isnull(is_cod_deposito) or len(is_cod_deposito) < 1 then
	ls_cod_deposito = "%"
end if

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_prodotto, is_cod_deposito)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

postevent("ue_giacenza")
end event

