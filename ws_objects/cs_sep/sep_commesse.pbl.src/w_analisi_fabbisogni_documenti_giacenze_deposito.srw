﻿$PBExportHeader$w_analisi_fabbisogni_documenti_giacenze_deposito.srw
$PBExportComments$Finestra Testata Generazione Commesse
forward
global type w_analisi_fabbisogni_documenti_giacenze_deposito from window
end type
type dw_depositi from uo_std_dw within w_analisi_fabbisogni_documenti_giacenze_deposito
end type
end forward

global type w_analisi_fabbisogni_documenti_giacenze_deposito from window
integer width = 2011
integer height = 1172
boolean titlebar = true
boolean controlmenu = true
windowtype windowtype = child!
long backcolor = 134217752
string icon = "AppIcon!"
event ue_posiziona ( )
dw_depositi dw_depositi
end type
global w_analisi_fabbisogni_documenti_giacenze_deposito w_analisi_fabbisogni_documenti_giacenze_deposito

type variables
s_report_stato_magazzino is_report_stato_magazzino 
uo_ref_mov_mag iuo_ref_mov_mag					

end variables

forward prototypes
public function integer wf_cliente ()
end prototypes

event ue_posiziona();long ll_x, ll_y

ll_x = is_report_stato_magazzino.xpos - 200
ll_y = is_report_stato_magazzino.ypos + 600

move(ll_x, ll_y)

setredraw(true)

end event

public function integer wf_cliente ();string ls_cliente
long ll_i,ll_anno_documento,ll_num_documento

if	dw_depositi.dataobject = "d_analisi_fabbisogni_documenti_impegnato" then

	for ll_i = 1 to dw_depositi.rowcount()
		if dw_depositi.getitemstring(ll_i,"expression1") = "ORD_VEN" then
			ll_anno_documento = dw_depositi.getitemnumber(ll_i,"anno_documento")
			ll_num_documento = dw_depositi.getitemnumber(ll_i,"num_documento")
			
			select 	tes_ord_ven.cod_cliente + ' ' + anag_clienti.rag_soc_1
			into		:ls_cliente
			from		tes_ord_ven
			join		anag_clienti on tes_ord_ven.cod_azienda = anag_clienti.cod_azienda and
												tes_ord_ven.cod_cliente = anag_clienti.cod_cliente
			where	tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						tes_ord_ven.anno_registrazione  = :ll_anno_documento and
						tes_ord_ven.num_registrazione  = :ll_num_documento ;
						
			if sqlca.sqlcode = 0 then
				dw_depositi.setitem(ll_i, "cliente", ls_cliente)
			end if
		end if
						
	next
	
end if
return 0
end function

on w_analisi_fabbisogni_documenti_giacenze_deposito.create
this.dw_depositi=create dw_depositi
this.Control[]={this.dw_depositi}
end on

on w_analisi_fabbisogni_documenti_giacenze_deposito.destroy
destroy(this.dw_depositi)
end on

event open;setredraw(false)

is_report_stato_magazzino= message.powerobjectparm

postevent("ue_posiziona")

dw_depositi.postevent("ue_retrieve")

iuo_ref_mov_mag = create uo_ref_mov_mag
end event

type dw_depositi from uo_std_dw within w_analisi_fabbisogni_documenti_giacenze_deposito
event ue_retrieve ( )
event ue_sql pbm_dwnsql
event ue_referenze_docs ( )
integer width = 1970
integer height = 1080
integer taborder = 10
string dataobject = "d_analisi_fabbisogni_documenti_giacenze_deposito"
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_retrieve();boolean lb_found=false
string ls_cod_prodotto, ls_where, ls_errore, ls_chiave[],ls_cod_deposito[],ls_des_deposito, ls_cod_fornitore,ls_rag_soc_1, ls_sql
long ll_ret,ll_y,ll_i,ll_anno_registrazione,ll_num_registrazione
dec{4} ld_quantita, ld_giacenza[], ld_quantita_giacenza[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime ldt_data_riferimento
datastore lds_ordinato
uo_magazzino luo_magazzino



choose case is_report_stato_magazzino.tipo_report 
	case "MAGAZZINO"

		dw_depositi.width = 1970
		parent.width = 2000
		
		parent.title = "MAGAZZINO " + is_report_stato_magazzino.cod_prodotto
		
		dw_depositi.dataobject = 'd_analisi_fabbisogni_documenti_giacenze_deposito'
		ldt_data_riferimento = datetime(today(),00:00:00)
		ls_cod_prodotto = is_report_stato_magazzino.cod_prodotto
		ls_where = ""
		
		for ll_ret = 1 to 14
			ld_quantita_giacenza[ll_ret] = 0
		next	
		
		luo_magazzino = CREATE uo_magazzino
		ll_ret = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_riferimento, ls_where, ld_quantita_giacenza, ls_errore, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])
		destroy luo_magazzino	
		
		for ll_y = 1 to upperbound(ls_chiave)
			
			if ld_giacenza_stock[ll_y] = 0 then continue
			
			ll_ret = dw_depositi.insertrow(0)
			
			dw_depositi.setitem(ll_ret, "cod_deposito", ls_chiave[ll_y])
			
			dw_depositi.setitem(ll_ret, "giacenza", ld_giacenza_stock[ll_y])
			
			select des_deposito
			into   :ls_des_deposito
			from   anag_depositi
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_deposito = :ls_chiave[ll_y];
					 
			dw_depositi.setitem(ll_ret, "des_deposito", ls_des_deposito)
		
		next
		
	case "IMPEGNO"
		dw_depositi.width = 2970
		parent.width = 3000
		parent.title = "IMPEGNATO " + is_report_stato_magazzino.cod_prodotto
		
		ls_cod_prodotto = is_report_stato_magazzino.cod_prodotto
		dw_depositi.dataobject = 'd_analisi_fabbisogni_documenti_impegnato'
		dw_depositi.settransobject(sqlca)
		dw_depositi.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, "%", "ORD_VEN", "IMP_COMMESSE")
		
		wf_cliente()
		
	case "ORDINATO"
		
		dw_depositi.width = 2370
		parent.width = 2400
		parent.title = "ORDINATO " + is_report_stato_magazzino.cod_prodotto

		lds_ordinato = Create datastore
		lds_ordinato.dataobject = 'd_analisi_fabbisogni_documenti_impegnato'
		lds_ordinato.settransobject(sqlca)
		ls_cod_prodotto = is_report_stato_magazzino.cod_prodotto
		ll_ret = lds_ordinato.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, "%", "ORD_ACQ", "ORD_ACQ")
		
		dw_depositi.dataobject = 'd_analisi_fabbisogni_documenti_ordinato'
		dw_depositi.reset()
		for ll_i = 1 to ll_ret
			ll_y = insertrow(0)
			ll_anno_registrazione = lds_ordinato.getitemnumber(ll_i, "anno_documento")
			ll_num_registrazione = lds_ordinato.getitemnumber(ll_i, "num_documento")
			dw_depositi.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
			dw_depositi.setitem(ll_i, "num_registrazione", ll_num_registrazione)
			dw_depositi.setitem(ll_i, "prog_riga_ord_acq", 125)
			select cod_fornitore
			into   :ls_cod_fornitore
			from   tes_ord_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione;
			dw_depositi.setitem(ll_i, "cod_fornitore", ls_cod_fornitore)
			dw_depositi.setitem(ll_i, "data_consegna", lds_ordinato.getitemdatetime(ll_i, "data_riferimento"))
			dw_depositi.setitem(ll_i, "quan_ordine", lds_ordinato.getitemnumber(ll_i, "quantita"))
			
			select rag_soc_1
			into   :ls_rag_soc_1
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_fornitore = :ls_cod_fornitore;
			
			dw_depositi.setitem(ll_i, "rag_soc_1", ls_rag_soc_1)
		next
		
	case "COMMESSE"
		dw_depositi.width = 1970
		parent.width = 2000
		parent.title = "PRODUZIONE IN CORSO " + is_report_stato_magazzino.cod_prodotto
		
		ls_cod_prodotto = is_report_stato_magazzino.cod_prodotto
		dw_depositi.dataobject = 'd_analisi_fabbisogni_documenti_impegnato'
		dw_depositi.settransobject(sqlca)
		dw_depositi.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, "%", "ANAG_COMMESSE", "ANAG_COMMESSE")
		
	case "RDA"
		dw_depositi.width = 2370
		parent.width = 2400
		parent.title = "RDA " + is_report_stato_magazzino.cod_prodotto
		
		dw_depositi.dataobject = 'd_analisi_fabbisogni_documenti_rda'
		ls_cod_prodotto = is_report_stato_magazzino.cod_prodotto
		dw_depositi.settransobject(sqlca)
		dw_depositi.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)
		
	case "MOVMAG"
		dw_depositi.width = 3000
		parent.width = 3050
		parent.title = "MOVIMENTI DEL PRODOTTO: " + is_report_stato_magazzino.cod_prodotto
		
		select data_chiusura_annuale
		into	:ldt_data_riferimento
		from	con_magazzino
		where cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then ldt_data_riferimento = datetime(date("01/01/1900"),00:00:00)
		
		dw_depositi.dataobject = 'd_analisi_fabbisogni_elenco_movimenti'
		ls_cod_prodotto = is_report_stato_magazzino.cod_prodotto
		dw_depositi.settransobject(sqlca)
		dw_depositi.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ldt_data_riferimento)
		
	case "DETMOVMAG"
		dw_depositi.width = 3000
		parent.width = 3050
		parent.title = "MOVIMENTI DEL PRODOTTO: " + is_report_stato_magazzino.cod_prodotto
		
		select data_chiusura_annuale
		into	:ldt_data_riferimento
		from	con_magazzino
		where cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then ldt_data_riferimento = datetime(date("01/01/1900"),00:00:00)
		
		dw_depositi.dataobject = 'd_analisi_fabbisogni_elenco_movimenti_det'
		ls_cod_prodotto = is_report_stato_magazzino.cod_prodotto
		dw_depositi.settransobject(sqlca)
		dw_depositi.setredraw(false)
		dw_depositi.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ldt_data_riferimento)
		dw_depositi.event ue_referenze_docs()
		dw_depositi.setredraw(true)
		
	case "MOVVEN"
		// movimenti del venduto
		
		dw_depositi.width = 3000
		parent.width = 3050
		parent.title = "MOVIMENTI VENDUTO DEL PRODOTTO: " + is_report_stato_magazzino.cod_prodotto
		
		select data_chiusura_annuale
		into	:ldt_data_riferimento
		from	con_magazzino
		where cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then ldt_data_riferimento = datetime(date("01/01/1900"),00:00:00)
		
		dw_depositi.dataobject = 'd_analisi_fabbisogni_lista_mov_det_filtro'
		ls_cod_prodotto = is_report_stato_magazzino.cod_prodotto
		dw_depositi.settransobject(sqlca)
		
		//		" join det_tipi_movimenti on tab_tipi_movimenti_det.cod_azienda = det_tipi_movimenti.cod_azienda and tab_tipi_movimenti_det.cod_tipo_mov_det = det_tipi_movimenti.cod_tipo_mov_det " + &

		
		ls_sql = "select mov_magazzino.cod_deposito, mov_magazzino.anno_registrazione,mov_magazzino.num_registrazione,mov_magazzino.cod_tipo_movimento, mov_magazzino.data_registrazione, tab_tipi_movimenti.des_tipo_movimento,mov_magazzino.cod_tipo_mov_det,tab_tipi_movimenti_det.des_tipo_movimento,mov_magazzino.quan_movimento,'' as riferimenti_docs " + &
		" from mov_magazzino " + &
		" join tab_tipi_movimenti on tab_tipi_movimenti.cod_azienda = mov_magazzino.cod_azienda and tab_tipi_movimenti.cod_tipo_movimento = mov_magazzino.cod_tipo_movimento " + &
		" join tab_tipi_movimenti_det on tab_tipi_movimenti_det.cod_azienda = mov_magazzino.cod_azienda and tab_tipi_movimenti_det.cod_tipo_mov_det = mov_magazzino.cod_tipo_mov_det " + &
		g_str.format(" where mov_magazzino.cod_azienda = '$1' and mov_magazzino.cod_prodotto = '$2' and mov_magazzino.data_registrazione >= '$3' and mov_magazzino.data_registrazione <= '$4' and tab_tipi_movimenti_det.flag_prog_quan_venduta <> '=' " ,s_cs_xx.cod_azienda, ls_cod_prodotto, string(is_report_stato_magazzino.data_inizio,s_cs_xx.db_funzioni.formato_data), string(is_report_stato_magazzino.data_fine,s_cs_xx.db_funzioni.formato_data )) + &
		" order by mov_magazzino.cod_deposito, mov_magazzino.data_registrazione, mov_magazzino.cod_tipo_movimento, mov_magazzino.cod_tipo_mov_det "

		dw_depositi.SetSQLSelect(ls_sql)
		dw_depositi.settransobject(sqlca)
		dw_depositi.setredraw(false)
		dw_depositi.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ldt_data_riferimento)
		dw_depositi.event ue_referenze_docs()
		dw_depositi.setredraw(true)
		
		
	case "MOVCONSUMATO"
		// movimenti del consumato
		
		dw_depositi.width = 3000
		parent.width = 3050
		parent.title = "MOVIMENTI CONSUMATO DEL PRODOTTO: " + is_report_stato_magazzino.cod_prodotto
		
		select data_chiusura_annuale
		into	:ldt_data_riferimento
		from	con_magazzino
		where cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then ldt_data_riferimento = datetime(date("01/01/1900"),00:00:00)
		
		dw_depositi.dataobject = 'd_analisi_fabbisogni_lista_mov_det_filtro'
		ls_cod_prodotto = is_report_stato_magazzino.cod_prodotto
		dw_depositi.settransobject(sqlca)
		
		ls_sql = "select mov_magazzino.cod_deposito, mov_magazzino.anno_registrazione,mov_magazzino.num_registrazione,mov_magazzino.cod_tipo_movimento, mov_magazzino.data_registrazione, tab_tipi_movimenti.des_tipo_movimento,mov_magazzino.cod_tipo_mov_det,tab_tipi_movimenti_det.des_tipo_movimento,mov_magazzino.quan_movimento,'' as riferimenti_docs " + &
		" from mov_magazzino " + &
		" join tab_tipi_movimenti on tab_tipi_movimenti.cod_azienda = mov_magazzino.cod_azienda and tab_tipi_movimenti.cod_tipo_movimento = mov_magazzino.cod_tipo_movimento " + &
		" join tab_tipi_movimenti_det on tab_tipi_movimenti_det.cod_azienda = mov_magazzino.cod_azienda and tab_tipi_movimenti_det.cod_tipo_mov_det = mov_magazzino.cod_tipo_mov_det " + &
		" join det_tipi_movimenti on tab_tipi_movimenti_det.cod_azienda = det_tipi_movimenti.cod_azienda and tab_tipi_movimenti_det.cod_tipo_mov_det = det_tipi_movimenti.cod_tipo_mov_det and tab_tipi_movimenti.cod_tipo_movimento = det_tipi_movimenti.cod_tipo_movimento  " + &
		g_str.format(" where mov_magazzino.cod_azienda = '$1' and mov_magazzino.cod_prodotto = '$2' and mov_magazzino.data_registrazione >= '$3' and mov_magazzino.data_registrazione <= '$4' and det_tipi_movimenti.flag_consumato ='S' " ,s_cs_xx.cod_azienda, ls_cod_prodotto, string(is_report_stato_magazzino.data_inizio,s_cs_xx.db_funzioni.formato_data), string(is_report_stato_magazzino.data_fine,s_cs_xx.db_funzioni.formato_data  )) + &
		" order by mov_magazzino.cod_deposito, mov_magazzino.data_registrazione, mov_magazzino.cod_tipo_movimento, mov_magazzino.cod_tipo_mov_det "

		dw_depositi.SetSQLSelect(ls_sql)
		dw_depositi.settransobject(sqlca)
		dw_depositi.setredraw(false)
		dw_depositi.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ldt_data_riferimento)
		dw_depositi.event ue_referenze_docs()
		dw_depositi.setredraw(true)
		
	case "MOVCARICATO"
		// movimenti del caricato
		
		dw_depositi.width = 3000
		parent.width = 3050
		parent.title = "MOVIMENTI CARICO DEL PRODOTTO: " + is_report_stato_magazzino.cod_prodotto
		
		select data_chiusura_annuale
		into	:ldt_data_riferimento
		from	con_magazzino
		where cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then ldt_data_riferimento = datetime(date("01/01/1900"),00:00:00)
		
		dw_depositi.dataobject = 'd_analisi_fabbisogni_lista_mov_det_filtro'
		ls_cod_prodotto = is_report_stato_magazzino.cod_prodotto
		dw_depositi.settransobject(sqlca)
		
		ls_sql = "select mov_magazzino.cod_deposito, mov_magazzino.anno_registrazione,mov_magazzino.num_registrazione,mov_magazzino.cod_tipo_movimento, mov_magazzino.data_registrazione, tab_tipi_movimenti.des_tipo_movimento,mov_magazzino.cod_tipo_mov_det,tab_tipi_movimenti_det.des_tipo_movimento,mov_magazzino.quan_movimento,'' as riferimenti_docs " + &
		" from mov_magazzino " + &
		" join tab_tipi_movimenti on tab_tipi_movimenti.cod_azienda = mov_magazzino.cod_azienda and tab_tipi_movimenti.cod_tipo_movimento = mov_magazzino.cod_tipo_movimento " + &
		" join tab_tipi_movimenti_det on tab_tipi_movimenti_det.cod_azienda = mov_magazzino.cod_azienda and tab_tipi_movimenti_det.cod_tipo_mov_det = mov_magazzino.cod_tipo_mov_det " + &
		g_str.format(" where mov_magazzino.cod_azienda = '$1' and mov_magazzino.cod_prodotto = '$2' and mov_magazzino.data_registrazione >= '$3' and mov_magazzino.data_registrazione <= '$4' and tab_tipi_movimenti_det.flag_prog_quan_acquistata <> '=' " ,s_cs_xx.cod_azienda, ls_cod_prodotto, string(is_report_stato_magazzino.data_inizio,s_cs_xx.db_funzioni.formato_data), string(is_report_stato_magazzino.data_fine,s_cs_xx.db_funzioni.formato_data )) + &
		" order by mov_magazzino.cod_deposito, mov_magazzino.data_registrazione, mov_magazzino.cod_tipo_movimento, mov_magazzino.cod_tipo_mov_det "

		dw_depositi.SetSQLSelect(ls_sql)
		dw_depositi.settransobject(sqlca)
		dw_depositi.setredraw(false)
//		dw_depositi.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ldt_data_riferimento)
		dw_depositi.retrieve()
		dw_depositi.event ue_referenze_docs()
		dw_depositi.setredraw(true)
		
end choose	



end event

event ue_referenze_docs();string	 ls_full_doc_rif, ls_small_doc_rif, ls_errore, ls_visualizza
long	ll_i, ll_anno_reg, ll_num_reg

for ll_i = 1 to rowcount()
	
	ll_anno_reg = getitemnumber(ll_i, 2)
	ll_num_reg = getitemnumber(ll_i, 3)
	
	if isnull(ll_anno_reg) or isnull(ll_num_reg) or ll_anno_reg = 0 or ll_num_reg = 0 then
		continue
	end if
	
	if iuo_ref_mov_mag.uof_ref_mov_mag(ll_anno_reg, ll_num_reg, ref ls_full_doc_rif, ref ls_small_doc_rif, ref ls_errore) <> 0 then
		setitem(ll_i, "riferimenti_docs", "Errore in lettura documenti collegati: " + ls_errore)
		return
	else
		// stefanop 18/06/2010 
		if isnull(ls_small_doc_rif) or ls_small_doc_rif = "" then
			if isnull(ls_full_doc_rif) or ls_full_doc_rif = "" then
				
				//inizio modifica --------------------------
				iuo_ref_mov_mag.uof_ref_mov_mag_prog_mov(ll_anno_reg, ll_num_reg, ref ls_full_doc_rif, ref ls_small_doc_rif, ref ls_errore)
				
				if isnull(ls_small_doc_rif) or ls_small_doc_rif = "" then
					if isnull(ls_full_doc_rif) or ls_full_doc_rif = "" then
						setitem(ll_i, "riferimenti_docs", "MANCA")
					else
						setitem(ll_i, "riferimenti_docs",  ls_full_doc_rif)
					end if
				else
					setitem(ll_i, "riferimenti_docs",  ls_small_doc_rif)
				end if
				
			else
				setitem(ll_i, "riferimenti_docs", ls_full_doc_rif)
			end if
		else
			setitem(ll_i, "riferimenti_docs", ls_small_doc_rif)
		end if
		// ---
	end if
next
return 
end event

