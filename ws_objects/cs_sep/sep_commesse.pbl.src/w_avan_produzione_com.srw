﻿$PBExportHeader$w_avan_produzione_com.srw
$PBExportComments$Window mat_prime_commessa_attivazione
forward
global type w_avan_produzione_com from w_cs_xx_principale
end type
type dw_avan_produzione_com_lista from uo_cs_xx_dw within w_avan_produzione_com
end type
type dw_avan_produzione_com_det from uo_cs_xx_dw within w_avan_produzione_com
end type
end forward

global type w_avan_produzione_com from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2670
integer height = 2292
string title = "Avanzamento Produzione Per Commessa"
dw_avan_produzione_com_lista dw_avan_produzione_com_lista
dw_avan_produzione_com_det dw_avan_produzione_com_det
end type
global w_avan_produzione_com w_avan_produzione_com

on w_avan_produzione_com.create
int iCurrent
call super::create
this.dw_avan_produzione_com_lista=create dw_avan_produzione_com_lista
this.dw_avan_produzione_com_det=create dw_avan_produzione_com_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_avan_produzione_com_lista
this.Control[iCurrent+2]=this.dw_avan_produzione_com_det
end on

on w_avan_produzione_com.destroy
call super::destroy
destroy(this.dw_avan_produzione_com_lista)
destroy(this.dw_avan_produzione_com_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_avan_produzione_com_lista.set_dw_options(sqlca, &
                                    		 i_openparm, &
		                                     c_scrollparent + c_nonew + c_nodelete, &
      		                               c_default)

dw_avan_produzione_com_det.set_dw_options(sqlca,dw_avan_produzione_com_lista,c_sharedata + c_scrollparent + c_nonew + c_nodelete,c_default)

iuo_dw_main = dw_avan_produzione_com_lista
end event

type dw_avan_produzione_com_lista from uo_cs_xx_dw within w_avan_produzione_com
integer width = 2633
integer height = 800
integer taborder = 10
string dataobject = "d_avan_produzione_com_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa,ll_prog_riga)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_avan_produzione_com_det from uo_cs_xx_dw within w_avan_produzione_com
integer y = 800
integer width = 2633
integer height = 1376
integer taborder = 20
string dataobject = "d_avan_produzione_com_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;datetime ld_null

this.AcceptText()

choose case dwo.name
	case "flag_blocco"
		if data="N" then
			setnull(ld_null)
			setitem(row,"data_blocco",ld_null)
			modify("data_blocco.Protect=1")
			Modify("data_blocco.Background.Color=12632256") //trasparent
		else
			modify("data_blocco.Protect=0")
			modify("data_blocco.backcolor=White")
			Modify("data_blocco.Background.Color=16777215") //white
		end if
end choose
end event

event pcd_modify;call super::pcd_modify;Modify("data_blocco.Background.Color=12632256") //trasparent
end event

