﻿$PBExportHeader$w_avanzamento_commessa.srw
forward
global type w_avanzamento_commessa from window
end type
type qta_prodotta from statictext within w_avanzamento_commessa
end type
type st_qta_prodotta from statictext within w_avanzamento_commessa
end type
type des_prodotto from statictext within w_avanzamento_commessa
end type
type cod_prodotto from statictext within w_avanzamento_commessa
end type
type dw_operai from u_dw_search within w_avanzamento_commessa
end type
type mle_log from multilineedit within w_avanzamento_commessa
end type
type val_pf from statictext within w_avanzamento_commessa
end type
type st_val_pf from statictext within w_avanzamento_commessa
end type
type scarico_mp from statictext within w_avanzamento_commessa
end type
type carico_pf from statictext within w_avanzamento_commessa
end type
type st_scarico_mp from statictext within w_avanzamento_commessa
end type
type st_carico_pf from statictext within w_avanzamento_commessa
end type
type st_um_mag from statictext within w_avanzamento_commessa
end type
type qta_residua from statictext within w_avanzamento_commessa
end type
type qta_ordinata from statictext within w_avanzamento_commessa
end type
type st_qta_residua from statictext within w_avanzamento_commessa
end type
type st_qta_da_produrre from statictext within w_avanzamento_commessa
end type
type st_tot_righe from statictext within w_avanzamento_commessa
end type
type st_prodotto from statictext within w_avanzamento_commessa
end type
type st_titolo from statictext within w_avanzamento_commessa
end type
type st_qta_ordinata from statictext within w_avanzamento_commessa
end type
type cb_annulla from commandbutton within w_avanzamento_commessa
end type
type cb_esegui from commandbutton within w_avanzamento_commessa
end type
type dw_lista from datawindow within w_avanzamento_commessa
end type
type st_commessa_chiusa from statictext within w_avanzamento_commessa
end type
type em_qta_da_produrre from editmask within w_avanzamento_commessa
end type
end forward

global type w_avanzamento_commessa from window
integer width = 3323
integer height = 2832
boolean titlebar = true
string title = "Avanzamento Commessa"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
qta_prodotta qta_prodotta
st_qta_prodotta st_qta_prodotta
des_prodotto des_prodotto
cod_prodotto cod_prodotto
dw_operai dw_operai
mle_log mle_log
val_pf val_pf
st_val_pf st_val_pf
scarico_mp scarico_mp
carico_pf carico_pf
st_scarico_mp st_scarico_mp
st_carico_pf st_carico_pf
st_um_mag st_um_mag
qta_residua qta_residua
qta_ordinata qta_ordinata
st_qta_residua st_qta_residua
st_qta_da_produrre st_qta_da_produrre
st_tot_righe st_tot_righe
st_prodotto st_prodotto
st_titolo st_titolo
st_qta_ordinata st_qta_ordinata
cb_annulla cb_annulla
cb_esegui cb_esegui
dw_lista dw_lista
st_commessa_chiusa st_commessa_chiusa
em_qta_da_produrre em_qta_da_produrre
end type
global w_avanzamento_commessa w_avanzamento_commessa

type variables
integer				ii_anno_commessa

long					il_num_commessa

string					is_cod_prodotto, is_des_prodotto, is_um_mag_pf, is_cod_tipo_mov_pf, is_cod_tipo_mov_mp, is_cod_deposito_mp, is_cod_deposito_pf, &
						is_des_deposito_mp, is_des_deposito_pf, is_cod_versione_pf, is_flag_mag_negativo, is_cod_operaio
						
dec{4}				idd_qta_ordinata, idd_qta_da_produrre, idd_qta_residua, idd_val_pf, idd_qta_prodotta
end variables

forward prototypes
public function integer wf_materie_prime (ref string as_errore)
public subroutine wf_log (string as_messaggio, integer ai_tipo)
public function integer wf_scarico_mp (ref string as_errore)
public function integer wf_carica_pf (ref string as_errore)
public function integer wf_crea_det_commessa (ref string as_errore)
public function integer wf_aggiorna_commessa (ref string as_errore)
public subroutine wf_get_giacenza (string as_cod_prodotto, string as_cod_deposito, ref decimal ad_giacenza)
public function integer wf_stampa_etichetta (ref string as_errore)
end prototypes

public function integer wf_materie_prime (ref string as_errore);uo_mrp								luo_mrp
long									ll_lead_time_cumulato, ll_index, ll_tot, ll_new
s_fabbisogno_commessa			lstr_fabbisogno[]
boolean								lb_muovi_sl
integer								li_ret
string									ls_null, ls_des_mp, ls_um_mag
datetime								ldt_null
dec{4}								ldd_giacenza


setpointer(hourglass!)

setnull(ls_null)
setnull(ldt_null)
dw_lista.reset()
cb_esegui.enabled = false
st_tot_righe.text = "Righe Totali: 0"

//estrazione materie prime con relativa quantità in base al residuo del semilavorato da produrre
luo_mrp = create uo_mrp
li_ret = luo_mrp.uof_calcolo_fabbisogni_comm(		lb_muovi_sl, &
																	"varianti_commesse", &
																	ii_anno_commessa, &
																	il_num_commessa, &
																	0, &
																	is_cod_prodotto, &
																	is_cod_versione_pf, &
																	idd_qta_da_produrre, &
																	0, &
																	ls_null, &
																	ldt_null, &
																	ref ll_lead_time_cumulato, &
																	ref lstr_fabbisogno[], &
																	ref as_errore )
destroy luo_mrp

if li_ret = -1 then
	setpointer(arrow!)
	return -1
end if

if upperbound(lstr_fabbisogno[]) > 0 then
else
	setpointer(arrow!)
	as_errore = "Non ci sono materie prime per il prodotto "+is_cod_prodotto+" (versione "+is_cod_versione_pf+") nella commessa!"
	wf_log(as_errore, 1)
	return 1
end if


pcca.mdi_frame.setmicrohelp("Estrazione materie prime in corso ...")

for ll_index=1 to upperbound(lstr_fabbisogno[])
	
	if lstr_fabbisogno[ll_index].cod_prodotto <>"" and not isnull(lstr_fabbisogno[ll_index].cod_prodotto) then
		ll_new = dw_lista.insertrow(0)
		dw_lista.setitem(ll_index, 1, lstr_fabbisogno[ll_index].cod_prodotto)
		
		pcca.mdi_frame.setmicrohelp("Estrazione materie prime in corso ..."+lstr_fabbisogno[ll_index].cod_prodotto)
		
		select des_prodotto, cod_misura_mag
		into :ls_des_mp, :ls_um_mag
		from anag_prodotti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:lstr_fabbisogno[ll_index].cod_prodotto;
		
		dw_lista.setitem(ll_index, 2, ls_des_mp)
		dw_lista.setitem(ll_index, 3, lstr_fabbisogno[ll_index].quan_fabbisogno)
		
		////giacenza nel deposito scarico MP
		//pcca.mdi_frame.setmicrohelp("Estrazione materie prime in corso ..."+lstr_fabbisogno[ll_index].cod_prodotto + " (calcolo giacenza)")
		
		//wf_get_giacenza(lstr_fabbisogno[ll_index].cod_prodotto, is_cod_deposito_mp, ldd_giacenza)
		//dw_lista.setitem(ll_index, 4, ldd_giacenza)
		
		dw_lista.setitem(ll_index, 5, ls_um_mag)
	end if
next

pcca.mdi_frame.setmicrohelp("Pronto!")

st_tot_righe.text = "Materie Prime Totali Totali: " + string(dw_lista.rowcount())
cb_esegui.enabled = true

wf_log("Pronto per l'avanzamento!", 0)
setpointer(arrow!)

return 0

end function

public subroutine wf_log (string as_messaggio, integer ai_tipo);

choose case ai_tipo
	case 0
		//ok
		mle_log.textcolor = rgb(0,0,255)
		
	case else
		//errore o warning
		mle_log.textcolor = rgb(255,0,0)
		
end choose

mle_log.text = as_messaggio
end subroutine

public function integer wf_scarico_mp (ref string as_errore);long				ll_num_stock, ll_index, ll_tot, ll_prog_stock_1, ll_prog_stock[], ll_anno_reg_des_mov, ll_num_reg_des_mov,  ll_anno_registrazione[], ll_num_registrazione[], &
					ll_index2, ll_tot2

dec{4}			ldd_qta_utilizzo, ldd_giacenza_stock, ld_valore_movimento, ldd_quan_impegnata_attuale

string				ls_cod_prodotto, ls_sql, ls_cod_ubicazione_1, ls_cod_lotto_1, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], &
					ls_referenza

integer			li_ret

datetime			ldt_data_stock_1, ldt_data_stock[], ldt_data_mov

datastore		lds_stock_produzione, lds_stock

uo_magazzino	luo_mag


ldt_data_mov = datetime(today(), 00:00:00)
ll_tot = dw_lista.rowcount()

//creazione datastore stock produzione (senza dati)
guo_functions.uof_crea_datastore(	lds_stock_produzione, &
												"select cod_prodotto,"+&
														"cod_deposito,"+&
														"cod_ubicazione,"+&
														"cod_lotto,"+&
														"data_stock,"+&
														"prog_stock,"+&
														"giacenza_stock "+&
   												"from stock "+&
												"where cod_azienda='INESISTENTE'", as_errore)
as_errore = ""

//---------------------------------------------------------------------------
//prelievo gli stocks
ll_num_stock = 0

//ciclo la datawindow delle materie prime per prelevare le quantità dagli stock in un datastore
//che sarà dopo utilizzato per fare gli scarichi
for ll_index = 1 to ll_tot
	ls_cod_prodotto = dw_lista.getitemstring(ll_index, "cod_prodotto")
	ldd_qta_utilizzo = dw_lista.getitemdecimal(ll_index, "quantita")
	if ldd_qta_utilizzo = 0 then continue  


	//creo un datastore con gli stock disponibili della materia prima
	//declare righe_stock dynamic cursor for sqlsa;
	ls_sql = "select cod_ubicazione, cod_lotto, data_stock, prog_stock, giacenza_stock " + &
				"from stock " + &
				"where cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
						"cod_deposito='" + is_cod_deposito_mp + "' and " + &
						"cod_prodotto='" + ls_cod_prodotto + "' and flag_stato_lotto='D' "

	if is_flag_mag_negativo = "N" then
		ls_sql += " and giacenza_stock > 0 "
	end if
	ls_sql += " order by data_stock desc "

	ll_tot2 = guo_functions.uof_crea_datastore(lds_stock, ls_sql, as_errore)
	if ll_tot2<0 then
		destroy lds_stock_produzione
		as_errore = "Errore in creazione ds stock: " + as_errore
		return -1
	end if
	
	for ll_index2=1 to ll_tot2
		ls_cod_ubicazione_1 	= lds_stock.getitemstring(ll_index2, 1)
		ls_cod_lotto_1 			= lds_stock.getitemstring(ll_index2, 2)
		ldt_data_stock_1 		= lds_stock.getitemdatetime(ll_index2, 3)
		ll_prog_stock_1 		= lds_stock.getitemnumber(ll_index2, 4)
		ldd_giacenza_stock 	= lds_stock.getitemdecimal(ll_index2, 5)
		
		if ldd_giacenza_stock >= ldd_qta_utilizzo or is_flag_mag_negativo = "S" then
			//la quantità presente nello stock corrente è sufficiente a soddisfare tutta la quantità utilizzata
			//oppure il magazzino consente di andare in negativo
			//quindi preleva tutto dal primo stock e poi esci dal ciclo for degli stock
			
			ll_num_stock = lds_stock_produzione.insertrow(0)
			lds_stock_produzione.setitem(ll_num_stock, "cod_prodotto", ls_cod_prodotto)
			lds_stock_produzione.setitem(ll_num_stock, "cod_deposito", is_cod_deposito_mp)
			lds_stock_produzione.setitem(ll_num_stock, "cod_ubicazione", ls_cod_ubicazione_1)
			lds_stock_produzione.setitem(ll_num_stock, "cod_lotto", ls_cod_lotto_1)
			lds_stock_produzione.setitem(ll_num_stock, "data_stock", ldt_data_stock_1)
			lds_stock_produzione.setitem(ll_num_stock, "prog_stock", ll_prog_stock_1)
			lds_stock_produzione.setitem(ll_num_stock, "giacenza_stock", ldd_qta_utilizzo)
			
			//impongo l'azzeramento della quantità in modo da far capire che ho coperto tutta la quantià per questa materia prima
			//e posso passare ad elaborare la successiva
			ldd_qta_utilizzo = 0
		
			exit
		else
			//quantità utilizzo NON ANCORA raggiunta, prelevo tutta la quantità dallo stock
			//e poi continuo a iterare altri eventuali stock
			
			ldd_qta_utilizzo = ldd_qta_utilizzo - ldd_giacenza_stock
			
			ll_num_stock = lds_stock_produzione.insertrow(0)
			lds_stock_produzione.setitem(ll_num_stock, "cod_prodotto", ls_cod_prodotto)
			lds_stock_produzione.setitem(ll_num_stock, "cod_deposito", is_cod_deposito_mp)
			lds_stock_produzione.setitem(ll_num_stock, "cod_ubicazione", ls_cod_ubicazione_1)
			lds_stock_produzione.setitem(ll_num_stock, "cod_lotto", ls_cod_lotto_1)
			lds_stock_produzione.setitem(ll_num_stock, "data_stock", ldt_data_stock_1)
			lds_stock_produzione.setitem(ll_num_stock, "prog_stock", ll_prog_stock_1)
			lds_stock_produzione.setitem(ll_num_stock, "giacenza_stock", ldd_giacenza_stock)
		end if
	next
	
	//questo non mi serve più. Sarà ricreato per la successiva materie prima
	destroy lds_stock
	
	
	//giunto fin qui, se la quantità necessaria non è stata raggiunta, segnalo la ocsa (se il magazzino non può andare in negativo)
	//altrimenti vuol dire che la materia prima non aveva nessuno stock: in tal caso lo creo e lo assegno ...
	if  ldd_qta_utilizzo > 0 then
		if is_flag_mag_negativo = "N" then
			destroy lds_stock_produzione
			as_errore = "Errore: Manca la quantità necessaria per la materia prima " + ls_cod_prodotto + " nel deposito "+is_cod_deposito_mp
			return -1
		else
			//creo lo stock standard (con quantità nulla) per la materia prima nel deposito prelievo
			ls_cod_ubicazione_1 = "UB0001"
			ls_cod_lotto_1 = "LT0001"
			ldt_data_stock_1 = datetime(date(2000, 1, 1), 00:00:00)
			ll_prog_stock_1 = 10
			ldd_giacenza_stock = 0
			
			insert into stock
				(cod_azienda,   
				cod_prodotto,   
				cod_deposito,   
				cod_ubicazione,   
				cod_lotto,   
				data_stock,   
				prog_stock,   
				giacenza_stock,   
				quan_assegnata, quan_in_spedizione, costo_medio, cod_fornitore, cod_cliente, giacenza_inizio_anno, flag_stato_lotto, data_cambio_stato, data_scadenza)
			values ( :s_cs_xx.cod_azienda,
					 :ls_cod_prodotto,   
					 :is_cod_deposito_mp,   
					 :ls_cod_ubicazione_1,   
					 :ls_cod_lotto_1,   
					 :ldt_data_stock_1,   
					 :ll_prog_stock_1,   
					 :ldd_giacenza_stock,   
					 0, 0, 0, null, null, 0, 'D', null, null);
				if sqlca.sqlcode<0 then
					destroy lds_stock_produzione
					as_errore = "Errore in creazione stock per il prodotto " + ls_cod_prodotto +  "nel deposito "+is_cod_deposito_mp+" : "+sqlca.sqlerrtext
					return -1
				end if
		end if
	end if
next


//arrivato fin qui nel datastore lds_stock_produzione ho tutto quello che mi serve per fare gli scarichi
setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

//--------------------------------------------------------------------------------------------------
//eseguo movimenti di scarico della MP dal deposito del fornitore del ddt (terzista)
//viene fatto un ciclo sugli stock della MP da prelevare
for ll_index = 1 to lds_stock_produzione.rowcount()
	
	//valorizzo le variabili dello stock
	ls_cod_deposito[1] = lds_stock_produzione.getitemstring(ll_index, "cod_deposito")
	ls_cod_ubicazione[1] = lds_stock_produzione.getitemstring(ll_index, "cod_ubicazione")
	ls_cod_lotto[1] = lds_stock_produzione.getitemstring(ll_index, "cod_lotto")	
	ldt_data_stock[1] = lds_stock_produzione.getitemdatetime(ll_index, "data_stock")	
	ll_prog_stock[1] = lds_stock_produzione.getitemnumber(ll_index, "prog_stock")
	ldd_qta_utilizzo = round(lds_stock_produzione.getitemdecimal(ll_index, "giacenza_stock"), 4)
	
	ld_valore_movimento = 0

	//campo referenza del movimento della materia prima
	ls_referenza = string(ii_anno_commessa)
	
	if f_crea_dest_mov_mag(	is_cod_tipo_mov_mp,&
										lds_stock_produzione.getitemstring(ll_index, "cod_prodotto"), &
										ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
										ll_anno_reg_des_mov, ll_num_reg_des_mov, as_errore) = -1 then
		return -1
	end if

	if f_verifica_dest_mov_mag (	ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
											is_cod_tipo_mov_mp, lds_stock_produzione.getitemstring(ll_index, "cod_prodotto")) = -1 then
		destroy lds_stock_produzione
		as_errore = "Errore in verifica destinazioni movimenti magazzino sulla materia prima  "+lds_stock_produzione.getitemstring(ll_index, "cod_prodotto")
		return -1
	end if
	
	//		in mov_magazzino.num_documento metto il numero della commessa
	//		in mov_magazzino.referenza (max 20 caratteri) metto l'anno della commessa
	luo_mag = create uo_magazzino
	
	luo_mag.uof_set_flag_commessa("S")
	
	li_ret = luo_mag.uof_movimenti_mag(ldt_data_mov, &
										  is_cod_tipo_mov_mp, &
										  "N", &
										  lds_stock_produzione.getitemstring(ll_index, "cod_prodotto"), &
										  ldd_qta_utilizzo, &
										  ld_valore_movimento, &
										  il_num_commessa, &
										  ldt_data_mov, &
										  ls_referenza, &
										  ll_anno_reg_des_mov, &
										  ll_num_reg_des_mov, &
										  ls_cod_deposito[], &
										  ls_cod_ubicazione[], &
										  ls_cod_lotto[], &
										  ldt_data_stock[], &
										  ll_prog_stock[], &
										  ls_cod_fornitore[], &
										  ls_cod_cliente[], &
										  ll_anno_registrazione[], &
										  ll_num_registrazione[], &
										  as_errore)
	destroy luo_mag
	
	if li_ret=-1 then	
		//se la variabile errore è vuota valorizzala cosi ...
		if as_errore="" or isnull(as_errore) then
			as_errore = "Errore su mov. mag, prodotto: " + lds_stock_produzione.getitemstring(ll_index, "cod_prodotto") &
						 + ", stock: " + ls_cod_deposito[1] + "-" + ls_cod_ubicazione[1] &
						 + "-" + ls_cod_lotto[1] + "-" + string(ldt_data_stock[1]) &
						 + "-" + string(ll_prog_stock[1])
		end if
		
		destroy lds_stock_produzione

		return -1
	end if

	f_elimina_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov)
next
//--------------------------------------------------------------------------------------------------

destroy lds_stock_produzione


//aggiornamento impegnato in impegno_mat_prime_commessa e anag_prodotti
ll_tot = dw_lista.rowcount()

for ll_index = 1 to ll_tot
	ls_cod_prodotto = dw_lista.getitemstring(ll_index, "cod_prodotto")
	ldd_qta_utilizzo = dw_lista.getitemdecimal(ll_index, "quantita")
	if ldd_qta_utilizzo = 0 then continue  
	
	
	select quan_impegnata_attuale
	into   :ldd_quan_impegnata_attuale
	from   impegno_mat_prime_commessa
	where  cod_azienda   = :s_cs_xx.cod_azienda and
			 anno_commessa = :ii_anno_commessa and
			 num_commessa  = :il_num_commessa and
			 cod_prodotto  = :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore lettura qta impegnata attuale della MP "+ls_cod_prodotto+" nella commessa : "+ sqlca.sqlerrtext
		return -1
	end if
		
	if ldd_quan_impegnata_attuale >=  ldd_qta_utilizzo then
		update impegno_mat_prime_commessa
		set 	 quan_impegnata_attuale= quan_impegnata_attuale - :ldd_qta_utilizzo
		where  cod_azienda   = :s_cs_xx.cod_azienda and
				 anno_commessa = :ii_anno_commessa and
				 num_commessa  = :il_num_commessa and
				 cod_prodotto  = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore disimpegno MP "+ls_cod_prodotto+" nella commessa : "+ sqlca.sqlerrtext
			return -1
		end if
	
		update anag_prodotti																		
		set 	 quan_impegnata = quan_impegnata - :ldd_qta_utilizzo
		where  cod_azienda  = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore aggiornamento impegnato MP "+ls_cod_prodotto+" in anagrafica prodotto : "+ sqlca.sqlerrtext
			return -1
		end if
			
	else
		// tutta la quantità impegnata deve essere disimpegnata
		update impegno_mat_prime_commessa
		set 	 quan_impegnata_attuale = 0
		where  cod_azienda   = :s_cs_xx.cod_azienda and
				 anno_commessa = :ii_anno_commessa and
				 num_commessa  = :il_num_commessa and
				 cod_prodotto  = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore disimpegno MP "+ls_cod_prodotto+" nella commessa : "+ sqlca.sqlerrtext
			return -1
		end if
	
		update anag_prodotti																		
		set 	 quan_impegnata = quan_impegnata - :ldd_qta_utilizzo
		where  cod_azienda   = :s_cs_xx.cod_azienda and
				 cod_prodotto  = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore aggiornamento impegnato MP "+ls_cod_prodotto+" in anagrafica prodotto : "+ sqlca.sqlerrtext
			return -1
		end if
	end if
next


return 0
end function

public function integer wf_carica_pf (ref string as_errore);string						ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], ls_anno_commessa
datetime					ldt_data_stock[], ldt_data_mov
long						ll_prog_stock[], ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_anno_registrazione[], ll_num_registrazione[]
uo_magazzino			luo_mag
integer					li_ret


ls_cod_deposito[1] = is_cod_deposito_pf
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

ldt_data_mov = datetime(today(), 00:00:00)
ls_anno_commessa = string(ii_anno_commessa)

	
if f_crea_dest_mov_mag(	is_cod_tipo_mov_pf, is_cod_prodotto, ls_cod_deposito[], & 
									ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
									ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
									ll_anno_reg_des_mov, ll_num_reg_des_mov, as_errore) = -1 then
  return -1
end if


luo_mag = create uo_magazzino

luo_mag.uof_set_flag_commessa("S")

li_ret = luo_mag.uof_movimenti_mag(ldt_data_mov, &
									  is_cod_tipo_mov_pf, &
									  "N", &
									  is_cod_prodotto, &
									  idd_qta_da_produrre, &
									  idd_val_pf, &
									  il_num_commessa, &
									  ldt_data_mov, &
									  ls_anno_commessa, &
									  ll_anno_reg_des_mov, &
									  ll_num_reg_des_mov, &
									  ls_cod_deposito[], &
									  ls_cod_ubicazione[], &
									  ls_cod_lotto[], &
									  ldt_data_stock[], &
									  ll_prog_stock[], &
									  ls_cod_fornitore[], &
									  ls_cod_cliente[], &
									  ll_anno_registrazione[], &
									  ll_num_registrazione[], &
									  as_errore)
destroy luo_mag

							  
if li_ret<0 then
	//se per disgrazia la variabile errore è vuota valorizzala cosi ...
	if as_errore="" or isnull(as_errore) then
		as_errore = "Errore su movimento di carico prodotto:" + is_cod_prodotto &
					 + ", deposito:" + ls_cod_deposito[1] + ", ubicazione:" + ls_cod_ubicazione[1] &
					 + ", cod_lotto:" + ls_cod_lotto[1] + ", data_stock:" + string(ldt_data_stock[1]) &
					 + ", prog_stock:" + string(ll_prog_stock[1])
	end if
	
	return -1
end if

f_elimina_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov)

return 0
end function

public function integer wf_crea_det_commessa (ref string as_errore);long					ll_prog_riga
datetime				ldt_oggi, ldt_adesso


select max(prog_riga)
into   :ll_prog_riga
from   det_anag_commesse
where	cod_azienda = :s_cs_xx.cod_azienda and    
			anno_commessa = :ii_anno_commessa and    
			num_commessa = :il_num_commessa;

if sqlca.sqlcode<0 then
	as_errore = "Errore in letura mas prog. rga in det_anag_commesse: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ll_prog_riga) then ll_prog_riga = 0
ll_prog_riga += 1

ldt_oggi = datetime(today(),00:00:00)
ldt_adesso = datetime(date(1900,1,1),now())

//to.do cod operaio e data operazione
insert into det_anag_commesse
			( cod_azienda,
			anno_commessa,
			num_commessa,
			prog_riga,
			anno_registrazione,   
			num_registrazione,
			quan_assegnata,
			quan_in_produzione,
			quan_prodotta,
			cod_tipo_movimento,
			anno_reg_des_mov,
			num_reg_des_mov,
			cod_tipo_mov_anticipo,
			quan_anticipo,
			anno_reg_anticipo,
			num_reg_anticipo,
			cod_operaio,
			data_avanzamento,
			ora_avanzamento)
values    (	:s_cs_xx.cod_azienda,   
				:ii_anno_commessa,   
				:il_num_commessa,   
				:ll_prog_riga,   
				null,   
				null,
				0,
				0,
				:idd_qta_da_produrre,
				:is_cod_tipo_mov_pf,
				null,
				null,
				null,
				0,
				null,
				null,
				:is_cod_operaio,
				:ldt_oggi,
				:ldt_adesso);

if sqlca.sqlcode<0 then
	as_errore = "Errore in inserimento dati in det_anag_commesse: " + sqlca.sqlerrtext
	return -1
end if

return 0

end function

public function integer wf_aggiorna_commessa (ref string as_errore);dec{4}			ldd_quan_ordine, ldd_quan_prodotta
datetime			ldt_data_chiusura
string				ls_messaggio


//preparazione messaggio finale di successo
ls_messaggio = "Operazione effettuata con successo!"


//aggiorna la quantità prodotta
select		quan_ordine,
			quan_prodotta
into	:ldd_quan_ordine,
		:ldd_quan_prodotta
from anag_commesse
where	cod_azienda = :s_cs_xx.cod_azienda and    
			anno_commessa = :ii_anno_commessa and    
			num_commessa = :il_num_commessa;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura dati commessa (prima dell'avanzamento): "+sqlca.sqlerrtext
	return -1
end if

if isnull(ldd_quan_ordine) then ldd_quan_ordine = 0
if isnull(ldd_quan_prodotta) then ldd_quan_prodotta = 0

//incremento la quantità prodotta
ldd_quan_prodotta = ldd_quan_prodotta + idd_qta_da_produrre

update anag_commesse
set		quan_prodotta = :ldd_quan_prodotta,
		flag_tipo_avanzamento = '3'
where	cod_azienda = :s_cs_xx.cod_azienda and    
			anno_commessa = :ii_anno_commessa and    
			num_commessa = :il_num_commessa;

if sqlca.sqlcode<>0 then
	as_errore = "Errore in aggiornamento quantità prodotta per la commessa: "+sqlca.sqlerrtext
	return -1
end if

//se hai raggiunto la quantità ordinata chiudi automaticamente la commessa
if ldd_quan_prodotta >= ldd_quan_ordine then
	ldt_data_chiusura = datetime(today(), 00:00:00)
	
	update anag_commesse
	set		data_chiusura = :ldt_data_chiusura,
			flag_tipo_avanzamento = '7'
	where	cod_azienda = :s_cs_xx.cod_azienda and    
				anno_commessa = :ii_anno_commessa and    
				num_commessa = :il_num_commessa;

	if sqlca.sqlcode<>0 then
		as_errore = "Errore in chiusura commessa: "+sqlca.sqlerrtext
		return -1
	end if
	
	ls_messaggio += " La commessa è stata anche chiusa!"
	
else
	ls_messaggio += " La commessa ha subito un avanzamento di produzione!"
end if

//se arrivi fin qui, tutto è andato bene, carica il messaggio di successo ...
as_errore = ls_messaggio

return 0
end function

public subroutine wf_get_giacenza (string as_cod_prodotto, string as_cod_deposito, ref decimal ad_giacenza);uo_magazzino luo_magazzino

string					ls_where, ls_error, ls_chiave[]
datetime				ldt_data_a
dec{4}				ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_giacenza
integer				li_return

ldt_data_a = datetime(today(), 23:59:59)

ls_where = "and cod_deposito=~~'"+as_cod_deposito+"~~'"

luo_magazzino = CREATE uo_magazzino
li_return = luo_magazzino.uof_saldo_prod_date_decimal(as_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "D", &
																			ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
destroy luo_magazzino

if li_return <0 then
	setnull(ad_giacenza)
	return
end if

//prendo solo il primo (ce ne sarà uno solo perchè gli ho passato il flitro per deposito fornitore)
if upperbound(ld_giacenza_stock[]) > 0 then
	ad_giacenza = ld_giacenza_stock[1]
end if

if isnull(ad_giacenza) then ad_giacenza = 0

return










end subroutine

public function integer wf_stampa_etichetta (ref string as_errore);
long							job, ll_index
string							ls_cod_prodotto, ls_des_prodotto, ls_cod_misura_mag, ls_valore, ls_testo
datetime						ldt_data_consegna, ldt_data_produzione
integer						li_num_copie
s_cs_xx_parametri			lstr_parametri

if not g_mb.confirm("Procedere alla stampa dell'etichetta avanzamento commessa?") then
	as_errore = ""
	return 0
end if

lstr_parametri.parametro_ul_1 = ii_anno_commessa
lstr_parametri.parametro_ul_2 = il_num_commessa


openwithparm(w_stampa_etic_commessa, lstr_parametri)


return 0
end function

on w_avanzamento_commessa.create
this.qta_prodotta=create qta_prodotta
this.st_qta_prodotta=create st_qta_prodotta
this.des_prodotto=create des_prodotto
this.cod_prodotto=create cod_prodotto
this.dw_operai=create dw_operai
this.mle_log=create mle_log
this.val_pf=create val_pf
this.st_val_pf=create st_val_pf
this.scarico_mp=create scarico_mp
this.carico_pf=create carico_pf
this.st_scarico_mp=create st_scarico_mp
this.st_carico_pf=create st_carico_pf
this.st_um_mag=create st_um_mag
this.qta_residua=create qta_residua
this.qta_ordinata=create qta_ordinata
this.st_qta_residua=create st_qta_residua
this.st_qta_da_produrre=create st_qta_da_produrre
this.st_tot_righe=create st_tot_righe
this.st_prodotto=create st_prodotto
this.st_titolo=create st_titolo
this.st_qta_ordinata=create st_qta_ordinata
this.cb_annulla=create cb_annulla
this.cb_esegui=create cb_esegui
this.dw_lista=create dw_lista
this.st_commessa_chiusa=create st_commessa_chiusa
this.em_qta_da_produrre=create em_qta_da_produrre
this.Control[]={this.qta_prodotta,&
this.st_qta_prodotta,&
this.des_prodotto,&
this.cod_prodotto,&
this.dw_operai,&
this.mle_log,&
this.val_pf,&
this.st_val_pf,&
this.scarico_mp,&
this.carico_pf,&
this.st_scarico_mp,&
this.st_carico_pf,&
this.st_um_mag,&
this.qta_residua,&
this.qta_ordinata,&
this.st_qta_residua,&
this.st_qta_da_produrre,&
this.st_tot_righe,&
this.st_prodotto,&
this.st_titolo,&
this.st_qta_ordinata,&
this.cb_annulla,&
this.cb_esegui,&
this.dw_lista,&
this.st_commessa_chiusa,&
this.em_qta_da_produrre}
end on

on w_avanzamento_commessa.destroy
destroy(this.qta_prodotta)
destroy(this.st_qta_prodotta)
destroy(this.des_prodotto)
destroy(this.cod_prodotto)
destroy(this.dw_operai)
destroy(this.mle_log)
destroy(this.val_pf)
destroy(this.st_val_pf)
destroy(this.scarico_mp)
destroy(this.carico_pf)
destroy(this.st_scarico_mp)
destroy(this.st_carico_pf)
destroy(this.st_um_mag)
destroy(this.qta_residua)
destroy(this.qta_ordinata)
destroy(this.st_qta_residua)
destroy(this.st_qta_da_produrre)
destroy(this.st_tot_righe)
destroy(this.st_prodotto)
destroy(this.st_titolo)
destroy(this.st_qta_ordinata)
destroy(this.cb_annulla)
destroy(this.cb_esegui)
destroy(this.dw_lista)
destroy(this.st_commessa_chiusa)
destroy(this.em_qta_da_produrre)
end on

event open;s_cs_xx_parametri					lstr_parametri
uo_mrp								luo_mrp
long									ll_lead_time_cumulato, ll_index, ll_tot, ll_new
s_fabbisogno_commessa			lstr_fabbisogno[]
boolean								lb_muovi_sl, lb_CMN
integer								li_ret
string									ls_null, ls_errore, ls_des_mp, ls_LET
datetime								ldt_null


lstr_parametri = message.powerobjectparm

setnull(ls_null)
setnull(ldt_null)

ii_anno_commessa = lstr_parametri.parametro_ul_1
il_num_commessa = lstr_parametri.parametro_ul_2
st_titolo.text = "Commessa N." + string(ii_anno_commessa) + "/" + string(il_num_commessa)

is_cod_prodotto = lstr_parametri.parametro_s_1_a[1]
is_cod_versione_pf = lstr_parametri.parametro_s_1_a[2]

//recupero info sul prodotto finito della commessa
select des_prodotto, cod_misura_mag
into :is_des_prodotto, :is_um_mag_pf
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:is_cod_prodotto;

cod_prodotto.text = is_cod_prodotto
des_prodotto.text = is_des_prodotto
st_um_mag.text = is_um_mag_pf

is_cod_deposito_mp = lstr_parametri.parametro_s_1_a[3]
is_cod_deposito_pf = lstr_parametri.parametro_s_1_a[4]

//recupero le descrizioni dei depositi coinvolti
select des_deposito
into :is_des_deposito_mp
from anag_depositi
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_deposito=:is_cod_deposito_mp;
			
if isnull(is_des_deposito_mp)  then is_des_deposito_mp = ""
			
select des_deposito
into :is_des_deposito_pf
from anag_depositi
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_deposito=:is_cod_deposito_pf;

if isnull(is_des_deposito_pf)  then is_des_deposito_pf = ""


is_cod_tipo_mov_mp = lstr_parametri.parametro_s_1_a[5]
is_cod_tipo_mov_pf = lstr_parametri.parametro_s_1_a[6]

carico_pf.text = is_cod_deposito_pf + " " + is_des_deposito_pf + " ("+is_cod_tipo_mov_pf+")"
scarico_mp.text = is_cod_deposito_mp + " " + is_des_deposito_mp + " ("+is_cod_tipo_mov_mp+")"


idd_qta_ordinata = lstr_parametri.parametro_d_1_a[1]				//ordinata
idd_qta_da_produrre = lstr_parametri.parametro_d_1_a[2]			//residuo dato da ordinata - prodotta (PROPORRE QUESTO VALORE)
idd_qta_prodotta =  lstr_parametri.parametro_d_1_a[3]				//già prodotta in sessioni precedenti

//calcolo il nuovo residuo da:		 quan_ordinata - quan. proposta - già prodotta
idd_qta_residua = idd_qta_ordinata - idd_qta_da_produrre - idd_qta_prodotta

qta_ordinata.text = string(idd_qta_ordinata, "###,##0.00")
em_qta_da_produrre.text = string(idd_qta_da_produrre, "###,##0.00")
qta_residua.text = string(idd_qta_residua, "###,##0.00")
qta_prodotta.text = string(idd_qta_prodotta, "###,##0.00")

if idd_qta_residua <=0 then
	st_commessa_chiusa.visible = true
else
	st_commessa_chiusa.visible = false
end if


idd_val_pf = lstr_parametri.parametro_d_1_a[4]
val_pf.text = string(idd_val_pf, "###,##0.00")


//magazzino negativo?
guo_functions.uof_get_parametro_azienda("CMN", lb_CMN)
if lb_CMN then
	is_flag_mag_negativo = "S"
else
	is_flag_mag_negativo = "N"
end if

is_cod_operaio = guo_functions.uof_get_operaio_utente()
if is_cod_operaio="" then setnull(is_cod_operaio)

f_PO_LoadDDDW_DW(	dw_operai,	"cod_operaio", sqlca,&
							  "anag_operai","cod_operaio","nome+' '+cognome",&
							  "cod_azienda='" + s_cs_xx.cod_azienda + "'")
dw_operai.setitem(1, "cod_operaio", is_cod_operaio)





//estrazione materie prime con relativa quantità in base al residuo del semilavorato da produrre
li_ret = wf_materie_prime(ls_errore)

em_qta_da_produrre.selecttext(1, len(em_qta_da_produrre.text))
em_qta_da_produrre.setfocus( )

end event

type qta_prodotta from statictext within w_avanzamento_commessa
integer x = 2702
integer y = 568
integer width = 590
integer height = 140
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "0,00"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_qta_prodotta from statictext within w_avanzamento_commessa
integer x = 2048
integer y = 576
integer width = 640
integer height = 140
integer textsize = -20
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Prodotta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type des_prodotto from statictext within w_avanzamento_commessa
integer x = 576
integer y = 396
integer width = 2706
integer height = 140
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cod_prodotto from statictext within w_avanzamento_commessa
integer x = 576
integer y = 240
integer width = 1179
integer height = 140
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_operai from u_dw_search within w_avanzamento_commessa
integer x = 690
integer y = 1264
integer width = 1961
integer height = 92
integer taborder = 30
string dataobject = "d_sel_operaio"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca"
		guo_ricerca.uof_ricerca_operaio(dw_operai,"cod_operaio")
		
end choose
end event

event itemchanged;call super::itemchanged;choose case dwo.name
	case "cod_operaio"
		if data <> "" then
			is_cod_operaio = data
		else
			setnull(is_cod_operaio)
		end if
		
end choose
end event

type mle_log from multilineedit within w_avanzamento_commessa
integer x = 27
integer y = 2276
integer width = 3250
integer height = 452
integer taborder = 20
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean vscrollbar = true
boolean autovscroll = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type val_pf from statictext within w_avanzamento_commessa
integer x = 2702
integer y = 240
integer width = 590
integer height = 140
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "0,00"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_val_pf from statictext within w_avanzamento_commessa
integer x = 1737
integer y = 240
integer width = 955
integer height = 140
integer textsize = -20
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Valore di carico:"
alignment alignment = right!
boolean focusrectangle = false
end type

type scarico_mp from statictext within w_avanzamento_commessa
integer x = 1083
integer y = 1056
integer width = 2217
integer height = 140
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type carico_pf from statictext within w_avanzamento_commessa
integer x = 1083
integer y = 896
integer width = 2217
integer height = 140
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_scarico_mp from statictext within w_avanzamento_commessa
integer x = 32
integer y = 1056
integer width = 1042
integer height = 140
integer textsize = -20
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Scarico MP da:"
boolean focusrectangle = false
end type

type st_carico_pf from statictext within w_avanzamento_commessa
integer x = 32
integer y = 896
integer width = 1042
integer height = 140
integer textsize = -20
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Carico PF in:"
boolean focusrectangle = false
end type

type st_um_mag from statictext within w_avanzamento_commessa
integer x = 1682
integer y = 576
integer width = 315
integer height = 140
integer textsize = -20
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "PZ"
boolean focusrectangle = false
end type

type qta_residua from statictext within w_avanzamento_commessa
integer x = 2702
integer y = 736
integer width = 590
integer height = 140
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "0,00"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type qta_ordinata from statictext within w_avanzamento_commessa
integer x = 1083
integer y = 576
integer width = 590
integer height = 140
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "0,00"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_qta_residua from statictext within w_avanzamento_commessa
integer x = 1893
integer y = 736
integer width = 795
integer height = 140
integer textsize = -20
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Residua:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_qta_da_produrre from statictext within w_avanzamento_commessa
integer x = 27
integer y = 736
integer width = 974
integer height = 140
integer textsize = -20
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Q.tà da produrre:"
boolean focusrectangle = false
end type

type st_tot_righe from statictext within w_avanzamento_commessa
integer x = 873
integer y = 1380
integer width = 1778
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Materie Prime Totali Totali:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_prodotto from statictext within w_avanzamento_commessa
integer x = 27
integer y = 240
integer width = 576
integer height = 140
integer textsize = -20
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Prodotto:"
boolean focusrectangle = false
end type

type st_titolo from statictext within w_avanzamento_commessa
integer x = 18
integer y = 28
integer width = 2176
integer height = 168
integer textsize = -22
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Commessa N."
alignment alignment = center!
boolean focusrectangle = false
end type

type st_qta_ordinata from statictext within w_avanzamento_commessa
integer x = 27
integer y = 576
integer width = 1001
integer height = 140
integer textsize = -20
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Q.tà ordinata:"
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_avanzamento_commessa
integer x = 27
integer y = 1232
integer width = 649
integer height = 212
integer taborder = 20
integer textsize = -26
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;
rollback;
closewithreturn(parent, "Annullato dall'operatore")
end event

type cb_esegui from commandbutton within w_avanzamento_commessa
integer x = 2665
integer y = 1240
integer width = 613
integer height = 212
integer taborder = 20
integer textsize = -26
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Esegui"
end type

event clicked;integer		li_ret
string			ls_errore

setpointer(hourglass!)

if is_cod_operaio="" or isnull(is_cod_operaio) then
	rollback;
	pcca.mdi_frame.setmicrohelp("Pronto!")
	wf_log("Selezionare un operaio!", -1)
	setpointer(arrow!)
	return
end if

//--------------------------------------------------------------------------------------------------------------------------------
//effettua gli scarichi delle materie prime
//			il disimpegno le quantità utilizzate (sia da impegno_mat_prime_commessa che da anag_prodotti)
pcca.mdi_frame.setmicrohelp("Registrazione scarichi materie prime in corso ...")
li_ret = wf_scarico_mp(ls_errore)
if li_ret<0 then
	rollback;
	pcca.mdi_frame.setmicrohelp("Pronto!")
	wf_log(ls_errore, -1)
	setpointer(arrow!)
	return
end if

//--------------------------------------------------------------------------------------------------------------------------------
//effettuare il carico del semilavorato prodotto
pcca.mdi_frame.setmicrohelp("Registrazione carichi prodotto finito in corso ...")
li_ret = wf_carica_pf(ls_errore)
if li_ret<0 then
	rollback;
	pcca.mdi_frame.setmicrohelp("Pronto!")
	wf_log(ls_errore, -1)
	setpointer(arrow!)
	return
end if

//--------------------------------------------------------------------------------------------------------------------------------
//inserire una riga in det_anag_commesse con i dettagli che servono
pcca.mdi_frame.setmicrohelp("Creazione dettaglio avanzamento commessa in corso ...")
li_ret = wf_crea_det_commessa(ls_errore)
if li_ret<0 then
	rollback;
	pcca.mdi_frame.setmicrohelp("Pronto!")
	wf_log(ls_errore, -1)
	setpointer(arrow!)
	return
end if

//--------------------------------------------------------------------------------------------------------------------------------
//aggiorna la testata della commessa con i dati che servono
pcca.mdi_frame.setmicrohelp("Aggiornamento dati commessa in corso ...")
li_ret = wf_aggiorna_commessa(ls_errore)
if li_ret<0 then
	rollback;
	pcca.mdi_frame.setmicrohelp("Pronto!")
	wf_log(ls_errore, -1)
	setpointer(arrow!)
	return
end if

pcca.mdi_frame.setmicrohelp("Pronto!")

setpointer(arrow!)

//arrivato fin qui vuol dire che è tutto OK
commit;

//dopo il commit, se c'è un messaggio dallo
if ls_errore<>"" and not isnull(ls_errore) then
	g_mb.success(ls_errore)
end if

//esegui la stampa etichetta
li_ret = wf_stampa_etichetta(ls_errore)

if li_ret< 0 then
	//errore critico
	g_mb.error(ls_errore)
	
elseif li_ret > 0 then
	//warning, se c'è
	if ls_errore<>"" and not isnull(ls_errore) then
		g_mb.warning(ls_errore)
	end if
else
	//stampa etichetta eseguita
end if

closewithreturn(parent, "")
end event

type dw_lista from datawindow within w_avanzamento_commessa
integer x = 27
integer y = 1456
integer width = 3250
integer height = 796
integer taborder = 10
string title = "none"
string dataobject = "d_lista_materie_prime"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_commessa_chiusa from statictext within w_avanzamento_commessa
boolean visible = false
integer x = 2363
integer y = 32
integer width = 914
integer height = 180
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Su conferma la commessa verrà chiusa!"
boolean focusrectangle = false
end type

type em_qta_da_produrre from editmask within w_avanzamento_commessa
integer x = 1083
integer y = 736
integer width = 658
integer height = 140
integer taborder = 10
boolean bringtotop = true
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
string mask = "###,##0.00"
boolean spin = true
end type

event modified;dec{4}		ld_quantita
string			ls_errore

//nuova quantità che si vuole produrre
ld_quantita = dec(em_qta_da_produrre.text)

cb_esegui.enabled = false

if isnull(ld_quantita) then ld_quantita = 0

if ld_quantita = 0 then
	wf_log("Inserire una quantità da produrre maggiore di zero!", 1)
	return
end if

idd_qta_da_produrre = ld_quantita
idd_qta_residua = idd_qta_ordinata - idd_qta_da_produrre - idd_qta_prodotta
qta_residua.text = string(idd_qta_residua, "###,##0.00")

if idd_qta_residua <=0 then
	st_commessa_chiusa.visible = true
else
	st_commessa_chiusa.visible = false
end if

//ricalcolare
wf_materie_prime(ls_errore)
end event

