﻿$PBExportHeader$w_buffer_chiusura_commesse.srw
forward
global type w_buffer_chiusura_commesse from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_buffer_chiusura_commesse
end type
type dw_ricerca from u_dw_search within w_buffer_chiusura_commesse
end type
end forward

global type w_buffer_chiusura_commesse from w_cs_xx_principale
integer width = 4261
integer height = 2608
string title = "Buffer Chiusura Commesse Produzione"
dw_lista dw_lista
dw_ricerca dw_ricerca
end type
global w_buffer_chiusura_commesse w_buffer_chiusura_commesse

type variables
string			is_sql_base
end variables

forward prototypes
public function integer wf_ricerca ()
end prototypes

public function integer wf_ricerca ();string					ls_sql
integer				li_anno_commessa
long					ll_num_commessa
datetime				ldt_data_pianificazione


dw_ricerca.accepttext()

ls_sql = is_sql_base + "where cod_azienda='"+s_cs_xx.cod_azienda+"'"

li_anno_commessa = dw_ricerca.getitemnumber(1, "anno_commessa")
ll_num_commessa = dw_ricerca.getitemnumber(1, "num_commessa")
ldt_data_pianificazione = dw_ricerca.getitemdatetime(1, "data_pianificazione")

if li_anno_commessa>0 and not isnull(li_anno_commessa) then
	ls_sql += " and anno_commessa="+string(li_anno_commessa)
end if

if ll_num_commessa>0 and not isnull(ll_num_commessa) then
	ls_sql += " and num_commessa="+string(ll_num_commessa)
end if

if not isnull(ldt_data_pianificazione) and year(date(ldt_data_pianificazione)) > 1950 then
	ldt_data_pianificazione = datetime(date(ldt_data_pianificazione), 00:00:00)
	ls_sql += " and data_pianificazione>='"+string(ldt_data_pianificazione, "yyyymmdd") + " 00:00:00'"
	ls_sql += " and data_pianificazione<='"+string(ldt_data_pianificazione, "yyyymmdd") + " 23:59:59'"
end if

ls_sql += " order by progressivo asc"

dw_lista.setsqlselect(ls_sql)

dw_lista.retrieve()



return 0
end function

on w_buffer_chiusura_commesse.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
this.Control[iCurrent+2]=this.dw_ricerca
end on

on w_buffer_chiusura_commesse.destroy
call super::destroy
destroy(this.dw_lista)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;

dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("progressivo")

dw_lista.set_dw_options(		sqlca,pcca.null_object, &
									c_noretrieveonopen + c_nonew + c_nomodify, &
									c_default + &
                                    	c_nohighlightselected + c_ViewModeBorderUnchanged)

iuo_dw_main = dw_lista

is_sql_base = dw_lista.getsqlselect()

end event

type dw_lista from uo_cs_xx_dw within w_buffer_chiusura_commesse
integer x = 37
integer y = 604
integer width = 4151
integer height = 1868
integer taborder = 20
string dataobject = "d_buffer_commesse"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;

wf_ricerca()
end event

event buttonclicked;call super::buttonclicked;longlong				ll_progressivo
string					ls_null
datetime				ldt_null


if row>0 then
else
	return
end if

choose case dwo.name
	case "b_reimposta"
		ll_progressivo = getitemnumber(row, "progressivo")
		
		if ll_progressivo>0 then
			
			setnull(ls_null)
			setnull(ldt_null)
			
			update buffer_chiusura_commesse
			  set 	errore=:ls_null,
			  		data_elaborazione=:ldt_null
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						progressivo=:ll_progressivo;
			
			if sqlca.sqlcode<0 then
				ls_null = sqlca.sqlerrtext
				rollback;
				g_mb.error("Errore in impostazione: "+sqlca.sqlerrtext)
				return
				
			elseif sqlca.sqlcode=100 then
				rollback;
				g_mb.warning("Dato non trovato! Riaggiorna la lista.")
				return
				
			else
				dw_lista.setitem(row, "errore", ls_null)
				dw_lista.setitem(row, "data_elaborazione", ldt_null)
				
				dw_lista.setitemstatus(row, "errore", primary!, notmodified!)
				dw_lista.setitemstatus(row, "data_elaborazione", primary!, notmodified!)
				
				commit;
				
			end if
			
		end if
		
end choose
end event

type dw_ricerca from u_dw_search within w_buffer_chiusura_commesse
integer x = 37
integer y = 24
integer width = 4151
integer height = 556
integer taborder = 10
string dataobject = "d_buffer_commesse_sel"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca"
		dw_lista.change_dw_current( )
		parent.postevent("pc_retrieve")
		
		
end choose
end event

