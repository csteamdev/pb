﻿$PBExportHeader$w_carica_bolle_terzisti.srw
$PBExportComments$Window Carico Bolle Entrata dai Terzisti
forward
global type w_carica_bolle_terzisti from w_cs_xx_principale
end type
type st_info_2 from statictext within w_carica_bolle_terzisti
end type
type st_info_1 from statictext within w_carica_bolle_terzisti
end type
type cb_genera from commandbutton within w_carica_bolle_terzisti
end type
type cbx_bolla_ingresso from checkbox within w_carica_bolle_terzisti
end type
type st_prodotto_ingresso from statictext within w_carica_bolle_terzisti
end type
type em_quan_arrivata from editmask within w_carica_bolle_terzisti
end type
type em_data_ingresso from editmask within w_carica_bolle_terzisti
end type
type st_1 from statictext within w_carica_bolle_terzisti
end type
type st_2 from statictext within w_carica_bolle_terzisti
end type
type cbx_acc_mat from checkbox within w_carica_bolle_terzisti
end type
type cb_togli from commandbutton within w_carica_bolle_terzisti
end type
type dw_det_prod_bolle_in_com from uo_cs_xx_dw within w_carica_bolle_terzisti
end type
type st_3 from statictext within w_carica_bolle_terzisti
end type
type dw_elenco_prod_scarico_fornitore from uo_cs_xx_dw within w_carica_bolle_terzisti
end type
type dw_folder from u_folder within w_carica_bolle_terzisti
end type
type dw_lista_prod_bolle_in_com from uo_cs_xx_dw within w_carica_bolle_terzisti
end type
type dw_lista_prodotti_scarico from uo_cs_xx_dw within w_carica_bolle_terzisti
end type
type dw_scelta_stock_fornitore from uo_cs_xx_dw within w_carica_bolle_terzisti
end type
type cb_aggiungi from commandbutton within w_carica_bolle_terzisti
end type
type st_stquan_impegnata from statictext within w_carica_bolle_terzisti
end type
type st_quan_impegnata from statictext within w_carica_bolle_terzisti
end type
type dw_elenco_fasi_esterne_attivazione from uo_cs_xx_dw within w_carica_bolle_terzisti
end type
type dw_scelta_fornitore from uo_cs_xx_dw within w_carica_bolle_terzisti
end type
end forward

global type w_carica_bolle_terzisti from w_cs_xx_principale
integer width = 4055
integer height = 2280
string title = "Ingresso Semilavorati da Terzisti"
st_info_2 st_info_2
st_info_1 st_info_1
cb_genera cb_genera
cbx_bolla_ingresso cbx_bolla_ingresso
st_prodotto_ingresso st_prodotto_ingresso
em_quan_arrivata em_quan_arrivata
em_data_ingresso em_data_ingresso
st_1 st_1
st_2 st_2
cbx_acc_mat cbx_acc_mat
cb_togli cb_togli
dw_det_prod_bolle_in_com dw_det_prod_bolle_in_com
st_3 st_3
dw_elenco_prod_scarico_fornitore dw_elenco_prod_scarico_fornitore
dw_folder dw_folder
dw_lista_prod_bolle_in_com dw_lista_prod_bolle_in_com
dw_lista_prodotti_scarico dw_lista_prodotti_scarico
dw_scelta_stock_fornitore dw_scelta_stock_fornitore
cb_aggiungi cb_aggiungi
st_stquan_impegnata st_stquan_impegnata
st_quan_impegnata st_quan_impegnata
dw_elenco_fasi_esterne_attivazione dw_elenco_fasi_esterne_attivazione
dw_scelta_fornitore dw_scelta_fornitore
end type
global w_carica_bolle_terzisti w_carica_bolle_terzisti

type variables
long il_anno_commessa,il_num_commessa,il_prog_riga
string is_cod_prodotto_fase,is_cod_reparto,is_cod_lavorazione, is_cod_versione_fase
end variables

forward prototypes
public function integer wf_imposta_stock ()
public function integer f_genera_lista_prodotti (integer fi_anno_commessa, long fl_num_commessa, long fl_prog_riga, string fs_cod_prodotto, string fs_cod_versione, double fd_quan_in_produzione, integer fi_num_livello_cor, string fs_errore)
public function integer wf_imposta_stock (string fs_cod_tipo_commessa, string fs_cod_prodotto, decimal fd_quantita)
public function integer wf_carica_sl_terzista (string fs_cod_prodotto, string fs_cod_fornitore, string fs_cod_tipo_det_ven, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, decimal fd_quantita)
public function integer wf_scarico_nuovo_terzista (long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga_commessa, string fs_cod_prodotto_fase, string fs_cod_versione_fase, string fs_cod_reparto, string fs_cod_lavorazione, string fs_cod_prodotto_spedito, string fs_cod_versione_spedito, decimal fd_quan_necessaria)
public function integer wf_carica_mag_terzista (string fs_cod_fornitore, string fs_cod_tipo_det_ven, string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, decimal fd_quantita)
public function double wf_controlla_qta_pf ()
public function string wf_get_depositi_fornitore (string as_cod_fornitore, ref string as_depositi[])
end prototypes

public function integer wf_imposta_stock ();string				ls_cod_prodotto_mp,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, ls_test,ls_cod_fornitore,& 
					ls_cod_tipo_commessa,ls_flag_mat_prima,ls_modify,ls_cod_reparto, & 
					ls_cod_lavorazione,ls_cod_prodotto_fase, ls_depositi[], ls_where_depositi, ls_sql, ls_errore
					
long				ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_orari, & 
					ll_num_fasi_aperte,ll_prog_spedizione, ll_index, ll_tot, ll_index2
					
datetime			ldt_data_stock

dec{4}			ldd_quan_disponibile,ldd_quan_necessaria,ldd_quan_prelevata,ldd_quan_impegnata

integer			li_anno_commessa

datastore		lds_data


dw_scelta_stock_fornitore.reset()
dw_scelta_fornitore.accepttext()

if dw_lista_prodotti_scarico.rowcount() = 0 then return 0

ls_cod_prodotto_mp = dw_lista_prodotti_scarico.getitemstring(dw_lista_prodotti_scarico.getrow(),"cod_prodotto")
ldd_quan_necessaria = dw_lista_prodotti_scarico.getitemnumber(dw_lista_prodotti_scarico.getrow(),"quan_utilizzo")

ls_cod_fornitore = dw_scelta_fornitore.getitemstring( dw_scelta_fornitore.getrow(), "cod_fornitore")

if isnull(ls_cod_fornitore) or ls_cod_fornitore="" then
	g_mb.messagebox("Sep","Selezionare un fornitore!", information!)
	return 0
end if

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return 0
end if
	
	
ls_where_depositi = wf_get_depositi_fornitore(ls_cod_fornitore, ls_depositi[])
if ls_where_depositi<>"" then
else

	select cod_deposito
	into   :ls_cod_deposito
	from   anag_fornitori
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_fornitore=:ls_cod_fornitore;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if
	
	ls_depositi[1] = ls_cod_deposito
end if

ls_sql = "SELECT  cod_ubicazione,cod_lotto,data_stock,prog_stock,giacenza_stock - quan_assegnata - quan_in_spedizione "+&
 			"FROM   stock "+&
			 "where  cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
			 			"cod_prodotto='"+ls_cod_prodotto_mp+"' and "+&
			 			"giacenza_stock - quan_assegnata - quan_in_spedizione>0 "

for ll_index=1 to upperbound(ls_depositi[])
	
	ls_where_depositi = " and cod_deposito='"+ls_depositi[ll_index]+"' "+&
								"order by data_stock asc "
	
	ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql + ls_where_depositi, ls_errore)
	if ll_tot<0 then
		g_mb.error(ls_errore)
		return 0
	end if
	
	for ll_index2 = 1 to ll_tot
		ls_cod_ubicazione = lds_data.getitemstring(ll_index2, 1)
		ls_cod_lotto = lds_data.getitemstring(ll_index2, 2)
		ldt_data_stock = lds_data.getitemdatetime(ll_index2, 3)
		ll_prog_stock = lds_data.getitemnumber(ll_index2, 4)
		ldd_quan_disponibile = lds_data.getitemdecimal(ll_index2, 5)
		
		ll_num_righe=dw_scelta_stock_fornitore.rowcount()
	
		if ll_num_righe=0 then
			ll_num_righe=1
		else
			ll_num_righe++
		end if
	
		dw_scelta_stock_fornitore.insertrow(ll_num_righe)
		dw_scelta_stock_fornitore.setitem(ll_num_righe,"cod_prodotto",ls_cod_prodotto_mp)
		dw_scelta_stock_fornitore.setitem(ll_num_righe,"cod_deposito", ls_depositi[ll_index])
		dw_scelta_stock_fornitore.setitem(ll_num_righe,"cod_ubicazione",ls_cod_ubicazione)
		dw_scelta_stock_fornitore.setitem(ll_num_righe,"cod_lotto",ls_cod_lotto)
		dw_scelta_stock_fornitore.setitem(ll_num_righe,"data_stock",ldt_data_stock)
		dw_scelta_stock_fornitore.setitem(ll_num_righe,"prog_stock",ll_prog_stock)
		dw_scelta_stock_fornitore.setitem(ll_num_righe,"quan_disponibile",ldd_quan_disponibile)
		
		if ldd_quan_necessaria > 0 then
			if ldd_quan_disponibile >= ldd_quan_necessaria then
				ldd_quan_prelevata = ldd_quan_necessaria
				ldd_quan_necessaria = 0 
			else
				ldd_quan_prelevata = ldd_quan_disponibile
				ldd_quan_necessaria = ldd_quan_necessaria - ldd_quan_prelevata
			end if
		else
			ldd_quan_prelevata = 0
		end if
		
		dw_scelta_stock_fornitore.setitem(ll_num_righe, "quan_prelevata", ldd_quan_prelevata)
	next
	
	destroy lds_data;
	

	dw_scelta_stock_fornitore.Reset_DW_Modified(c_ResetChildren)
	dw_scelta_stock_fornitore.settaborder("quan_prelevata", 10)

	select sum(quan_impegnata)
	into   :ldd_quan_impegnata
	from   impegni_deposito_fasi_com
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_deposito=:ls_depositi[ll_index]
	and    cod_prodotto_spedito=:ls_cod_prodotto_mp;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if

next


st_quan_impegnata.text = string(ldd_quan_impegnata)

return 0

end function

public function integer f_genera_lista_prodotti (integer fi_anno_commessa, long fl_num_commessa, long fl_prog_riga, string fs_cod_prodotto, string fs_cod_versione, double fd_quan_in_produzione, integer fi_num_livello_cor, string fs_errore);string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, &
			 ls_cod_prodotto_inserito,ls_cod_lavorazione,ls_test, ls_flag_fine_fase, ls_cod_versione_figlio, ls_cod_versione_inserito, ls_cod_versione_variante, & 
			 ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante, ls_flag_ramo_descrittivo
long      ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_handle,ll_i,ll_num_righe_dw
integer   li_risposta
dec{4}    ldd_quan_utilizzo,ldd_quan_utilizzo_variante,ldd_quan_utilizzo_1
boolean   lb_flag_trovata
datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli = 1 to ll_num_righe
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fd_quan_in_produzione
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	ls_flag_ramo_descrittivo = lds_righe_distinta.getitemstring(ll_num_figli,"distinta_flag_ramo_descrittivo")	
	
	if ls_flag_ramo_descrittivo ="S" then continue

	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio
	
	select cod_prodotto,
			 cod_versione_variante,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ls_cod_versione_variante,
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda		and    
			 anno_commessa = :fi_anno_commessa		and    
			 num_commessa = :fl_num_commessa			and    
			 cod_prodotto_padre = :fs_cod_prodotto and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
			 cod_versione = :fs_cod_versione 		and
			 cod_versione_figlio = :ls_cod_versione_figlio;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_cod_versione_inserito = ls_cod_versione_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante* fd_quan_in_produzione
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	//***************************
	//ricerca se già inserito
	lb_flag_trovata = false
	for ll_i = 1 to dw_lista_prodotti_scarico.rowcount()
//		dw_lista_prodotti_scarico.setrow(ll_i)		
		if ls_cod_prodotto_inserito = dw_lista_prodotti_scarico.getitemstring(ll_i,"cod_prodotto") then 
			lb_flag_trovata=true
			ldd_quan_utilizzo_1 = dw_lista_prodotti_scarico.getitemnumber(ll_i,"quan_utilizzo")
			ldd_quan_utilizzo = ldd_quan_utilizzo + ldd_quan_utilizzo_1 
			dw_lista_prodotti_scarico.setitem(ll_i,"quan_utilizzo",ldd_quan_utilizzo)
		end if
	next
	
	if lb_flag_trovata then continue
	//***************************

	if ls_flag_materia_prima = 'N' then
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and 	 
				 cod_prodotto_padre = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
		
		if sqlca.sqlcode = 100 then 
			
			//*************************************
			//inserimento in datawindow
			ll_num_righe_dw = dw_lista_prodotti_scarico.rowcount()
			
			if ll_num_righe_dw=0 then
				ll_num_righe_dw=1
			else
				ll_num_righe_dw++
			end if
			
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto_inserito;
		
			if sqlca.sqlcode < 0 then
				ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
			
			dw_lista_prodotti_scarico.insertrow(ll_num_righe_dw)
			dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"cod_prodotto",ls_cod_prodotto_inserito)
			dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"des_prodotto",ls_des_prodotto)
			dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"quan_utilizzo",ldd_quan_utilizzo)
			
			//*********************************
			continue
		end if
	else
		
		//*************************************
		//inserimento in datawindow
		ll_num_righe_dw=dw_lista_prodotti_scarico.rowcount()
	
		if ll_num_righe_dw=0 then
			ll_num_righe_dw=1
		else
			ll_num_righe_dw++
		end if
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto_inserito;
	
		if sqlca.sqlcode < 0 then
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		dw_lista_prodotti_scarico.insertrow(ll_num_righe_dw)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"cod_prodotto",ls_cod_prodotto_inserito)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"des_prodotto",ls_des_prodotto)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"quan_utilizzo",ldd_quan_utilizzo)
		
		//*********************************
		
		continue
	end if

	

	//***************************
	//Verifica se il prodotto è un SL di una fase già finita (flag_fine_fase = 'S'), in questo caso è come se fosse una MP
	select flag_fine_fase
	into   :ls_flag_fine_fase
	from   avan_produzione_com
	where  cod_azienda = :s_cs_xx.cod_azienda		and    
			 anno_commessa = :fi_anno_commessa		and    
			 num_commessa = :fl_num_commessa			and    
			 prog_riga = :fl_prog_riga					and    
			 cod_prodotto = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if sqlca.sqlcode < 0 then
		ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	if ls_flag_fine_fase = 'S' then 
		//*************************************
		//inserimento in datawindow
		ll_num_righe_dw=dw_lista_prodotti_scarico.rowcount()
	
		if ll_num_righe_dw=0 then
			ll_num_righe_dw=1
		else
			ll_num_righe_dw++
		end if
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto_inserito;
	
		if sqlca.sqlcode < 0 then
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		dw_lista_prodotti_scarico.insertrow(ll_num_righe_dw)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"cod_prodotto",ls_cod_prodotto_inserito)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"des_prodotto",ls_des_prodotto)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"quan_utilizzo",ldd_quan_utilizzo)
		
		//*********************************
		continue
	end if
	
	//***************************


	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
	       cod_prodotto_padre = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		continue
	else
		li_risposta=f_genera_lista_prodotti(fi_anno_commessa,fl_num_commessa,fl_prog_riga, ls_cod_prodotto_inserito, &
																													  ls_cod_versione_inserito, &
																													  ldd_quan_utilizzo, fi_num_livello_cor + 1,ls_errore)

		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

return 0
end function

public function integer wf_imposta_stock (string fs_cod_tipo_commessa, string fs_cod_prodotto, decimal fd_quantita);string		ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto
datetime ldt_data_stock
long		ll_prog_stock, ll_num_righe
dec{4}	ldd_quan_disponibile, ldd_quan_prelevata

select cod_deposito_prelievo
into   :ls_cod_deposito
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda 	and    
		 cod_tipo_commessa = :fs_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return 0
end if
		
declare righe_stock cursor for
SELECT		cod_ubicazione,   
		  		cod_lotto,   
		  		data_stock,   
		  		prog_stock,
		  		giacenza_stock - quan_assegnata - quan_in_spedizione  
 FROM   	stock   
 where  		cod_azienda = :s_cs_xx.cod_azienda	and    
 				cod_prodotto = :fs_cod_prodotto			and    
			 	cod_deposito = :ls_cod_deposito			and    
			 	giacenza_stock - quan_assegnata - quan_in_spedizione > 0
 ORDER BY data_stock asc;

 open righe_stock;

 if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)	 
	return 0
 end if

ll_num_righe = 0

do while 1 = 1
	
	fetch righe_stock 	into	:ls_cod_ubicazione,
									:ls_cod_lotto,   
									:ldt_data_stock,   
									:ll_prog_stock,
									:ldd_quan_disponibile;

	choose case sqlca.sqlcode  
		case 100
			exit
		case is < 0 
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext, stopsign!)
			exit
	end choose	

	ll_num_righe ++

	if fd_quantita > 0 then
		if ldd_quan_disponibile >= fd_quantita then
			ldd_quan_prelevata = fd_quantita
			fd_quantita = 0 
		else
			ldd_quan_prelevata = ldd_quan_disponibile
			fd_quantita = fd_quantita - ldd_quan_prelevata
		end if
	else
		ldd_quan_prelevata = 0
	end if

loop

close righe_stock;
	

return 0

end function

public function integer wf_carica_sl_terzista (string fs_cod_prodotto, string fs_cod_fornitore, string fs_cod_tipo_det_ven, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, decimal fd_quantita);string		ls_cod_tipo_movimento, ls_flag_tipo_det_ven, ls_sql, ls_cod_deposito_mov, ls_flag_fornitore, ls_cod_fornitore_mov, ls_cod_fornitore[], ls_cod_cliente[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_deposito_merce[], &
			ls_cod_deposito_fornitore
long		ll_cont_mov, ll_prog_ordinamento, ll_prog_stock[], ll_anno_reg_des_mov, ll_num_reg_des_mov
datetime	ldt_data_stock[]

select	cod_tipo_movimento,
		flag_tipo_det_ven
into	:ls_cod_tipo_movimento,
		:ls_flag_tipo_det_ven
from	tab_tipi_det_ven
where	cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_det_ven = :fs_cod_tipo_det_ven;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "SEP", "Errore durante la ricerca tipo dettaglio vendita~r~n" + sqlca.sqlerrtext, stopsign!)
	return -1
end if

if ls_flag_tipo_det_ven = "M" and isnull(ls_cod_tipo_movimento) then
	g_mb.messagebox( "SEP", "Attenzione: è stato configurato in maniera non corretta il tipo dettaglio vendite.~r~nIl tipo dettaglio è di un prodotto a magazzino ma non si è specificato il relativo tipo movimento di magazzino!~r~nOperazione annullata", stopsign!)
	return -1
end if

if ls_flag_tipo_det_ven = "M" then   // solo se tipo det = prodotti magazzino

	update	anag_prodotti
	set		quan_in_spedizione = quan_in_spedizione + :fd_quantita
	where		cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :fs_cod_prodotto;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox( "SEP", "Si è verificato un errore in fase di aggiornamento magazzino." + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	update	stock
	set		quan_in_spedizione = quan_in_spedizione + :fd_quantita
	where		cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :fs_cod_prodotto and
				cod_deposito = :fs_cod_deposito and
				cod_ubicazione = :fs_cod_ubicazione and
				cod_lotto = :fs_cod_lotto and
				data_stock = :fdt_data_stock and
				prog_stock = :fl_prog_stock;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox( "SEP", "Si è verificato un errore in fase di aggiornamento stock." + sqlca.sqlerrtext, stopsign!)
		return -1
	end if

	ls_cod_deposito_merce[1] = fs_cod_deposito
	ls_cod_ubicazione[1] = fs_cod_ubicazione
	ls_cod_lotto[1] = fs_cod_lotto
	ldt_data_stock[1] = fdt_data_stock
	ll_prog_stock[1] = fl_prog_stock
	
	// -----------------------------------  assegno il deposito corretto --------------------------------------------		

	ll_cont_mov = 0
	
	declare cu_movimenti dynamic cursor for sqlsa;
	
	ls_sql =  " select	cod_deposito, " + &
				"			flag_fornitore,  " + &
				"			cod_fornitore, " + &
				"			ordinamento " + &
				" from		det_tipi_movimenti " + &
				" where 	cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
				"			cod_tipo_movimento = '" + ls_cod_tipo_movimento + "'  " + &
				" order by ordinamento ASC" 
				 
	prepare sqlsa from :ls_sql;
	
	open dynamic cu_movimenti;
	
	do while true
		fetch cu_movimenti INTO :ls_cod_deposito_mov, 
										 :ls_flag_fornitore, 
										 :ls_cod_fornitore_mov, 
										 :ll_prog_ordinamento ;
										 
		if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
		
		ll_cont_mov ++
		
		setnull(ls_cod_fornitore[ll_cont_mov])
		
		setnull(ls_cod_cliente[ll_cont_mov])
		
		if ll_cont_mov > 1 then
			
			setnull(ls_cod_ubicazione[ll_cont_mov])
			setnull(ls_cod_lotto[ll_cont_mov])
			setnull(ldt_data_stock[ll_cont_mov])
			setnull(ll_prog_stock[ll_cont_mov])
			setnull(ls_cod_fornitore[ll_cont_mov])
			setnull(ls_cod_deposito_merce[ll_cont_mov])
			
		end if
		
		if ls_flag_fornitore = "S" then
			
			if not isnull(ls_cod_fornitore_mov) and ls_cod_fornitore_mov <> fs_cod_fornitore then 
					g_mb.messagebox("Creazione Bolle Terzisti","Attenzione il fornitore indicato nel tipo movimento è diverso dal fornitore ~r~nselezionato durante la procedura di generazione bolle~r~nIl fornitore selezionato dall'utente viene sostituito al fornitore indicato nel tipo movimento")
			end if
			
			ls_cod_fornitore[ll_cont_mov] = fs_cod_fornitore
			
			setnull(ls_cod_deposito_fornitore)
			
			select	cod_deposito
			into   :ls_cod_deposito_fornitore
			from	anag_fornitori
			where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_fornitore = :ls_cod_fornitore[ll_cont_mov] ;
					
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "SEP", "Errore durante la ricerca dati fornitore su tabella ANAG_FORNITORI~r~nDettaglio errore "+sqlca.sqlerrtext, stopsign!)
				return -1
			end if
			
			ls_cod_deposito_merce[ll_cont_mov] = ls_cod_deposito_fornitore 
			
			ls_cod_ubicazione[ll_cont_mov] = ls_cod_fornitore[ll_cont_mov]
			
			if isnull(ls_cod_deposito_merce[ll_cont_mov])  then
				
				ls_cod_deposito_merce[ll_cont_mov] = ls_cod_deposito_mov
				
				if isnull(ls_cod_deposito_merce[ll_cont_mov]) then
					
					ls_cod_deposito_merce[ll_cont_mov] = fs_cod_deposito
					
				end if
				
			end if
			
		end if
		
	loop
	
	close cu_movimenti;
	
	if ll_cont_mov = 0 or isnull(ll_cont_mov) then
		g_mb.messagebox( "SEP", "Attenzione il movimento non è configurato correttamente: verificare tipi movimenti; procedura abbandonata ", stopsign!)
		return -1
	end if
	
	// --------------------------------------------------------------------------------------------------------------
	
//	if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, fs_cod_prodotto, ls_cod_deposito_merce[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], ref ll_anno_reg_des_mov, ref ll_num_reg_des_mov) = -1 then
//		g_mb.messagebox( "SEP", "Si è verificato un errore in fase di creazione destinazioni movimenti.", stopsign!)
//		return -1
//	end if
//
//	if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, fs_cod_prodotto) = -1 then
//		return -1
//	end if

end if

return 0
end function

public function integer wf_scarico_nuovo_terzista (long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga_commessa, string fs_cod_prodotto_fase, string fs_cod_versione_fase, string fs_cod_reparto, string fs_cod_lavorazione, string fs_cod_prodotto_spedito, string fs_cod_versione_spedito, decimal fd_quan_necessaria);//	***	Michela 29/06/2007
//		questo metodo impegna il semilavorato che serve al secondo terzista.
//		prodotto fase = semilavorato del secondo terzista (padre del terzista attuale per intenderci)
//		versione fase = versione del semilavorato del secondo terzista
//		reparto, lavorazione = fase di lavorazione del secondo terzista
//		prodotto, versione spedito = prodotto appena caricato dal primo terzista

string		ls_cod_fornitore, ls_cod_deposito_fornitore, ls_test
dec{4}	ldd_quan_impegnata, ldd_nuova_quan_impegnata

dw_scelta_fornitore.accepttext()

ls_cod_fornitore = dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"cod_fornitore_2")

if isnull (ls_cod_fornitore) or len(trim(ls_cod_fornitore)) < 1 then
	g_mb.messagebox("Sep","Selezionare un Fornitore dal folder Spedizione !",stopsign!)
	return -1
end if

select cod_deposito
into   :ls_cod_deposito_fornitore
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 cod_fornitore = :ls_cod_fornitore;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Attenzione: selezionare il fornitore dal folder Spedizione; in questo modo si assegna il deposito di invio. " + sqlca.sqlerrtext,stopsign!)
	return  -1
end if

if isnull (ls_cod_deposito_fornitore) or len(trim(ls_cod_deposito_fornitore)) < 1 then
	g_mb.messagebox("Sep","Attenzione! il fornitore non è associato ad alcun deposito.",stopsign!)
	return -1
end if

select sum(quan_impegnata)
into   :ldd_quan_impegnata
from   impegni_deposito_fasi_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :fl_anno_commessa  and    
		 num_commessa = :fl_num_commessa    and    
		 cod_prodotto_fase = :fs_cod_prodotto_fase and
		 cod_versione = :fs_cod_versione_fase and    
		 cod_reparto = :fs_cod_reparto and    
		 cod_lavorazione = :fs_cod_lavorazione and    
		 prog_riga = :fl_prog_riga_commessa     and    
		 cod_prodotto_spedito = :fs_cod_prodotto_spedito;

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Sep","Errore nella totalizzazione impegnato da  impegni_deposito_fasi_com ~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ldd_quan_impegnata) then ldd_quan_impegnata = 0

ldd_nuova_quan_impegnata = fd_quan_necessaria - ldd_quan_impegnata

update impegni_deposito_fasi_com
set    quan_impegnata_ultima = 0
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :fl_anno_commessa and    
		 num_commessa = :fl_num_commessa and    
		 prog_riga = :fl_prog_riga_commessa and    
		 cod_prodotto_fase = :fs_cod_prodotto_fase and    
		 cod_versione = :fs_cod_versione_fase and
		 cod_reparto = :fs_cod_reparto and    
		 cod_lavorazione = :fs_cod_lavorazione and    
		 cod_deposito = :ls_cod_deposito_fornitore and    
		 cod_prodotto_spedito = :fs_cod_prodotto_spedito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in aggiornamento tabella impegni_deposito_fasi_com ~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select cod_azienda
into   :ls_test
from   impegni_deposito_fasi_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :fl_anno_commessa and    
		 num_commessa = :fl_num_commessa and    
		 prog_riga = :fl_prog_riga_commessa  and    
		 cod_prodotto_fase = :fs_cod_prodotto_fase and
		 cod_versione = :fs_cod_versione_fase and    
		 cod_reparto = :fs_cod_reparto and    
		 cod_lavorazione = :fs_cod_lavorazione and    
		 cod_deposito = :ls_cod_deposito_fornitore and    
		 cod_prodotto_spedito = :fs_cod_prodotto_spedito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in verifica presenza impegnati precedenti in impegni_deposito_fasi_comÈr~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if sqlca.sqlcode = 100 or isnull(ls_test) or ls_test ="" then
	
	insert into impegni_deposito_fasi_com
	(cod_azienda,
	 anno_commessa,
	 num_commessa,
	 prog_riga,
	 cod_prodotto_fase,
	 cod_versione,
	 cod_reparto,
	 cod_lavorazione,
	 cod_deposito,
	 cod_prodotto_spedito,
	 quan_impegnata,
	 quan_impegnata_ultima)
	values
	(:s_cs_xx.cod_azienda,
	 :fl_anno_commessa,
	 :fl_num_commessa,
	 :fl_prog_riga_commessa,
	 :fs_cod_prodotto_fase,
	 :fs_cod_versione_fase,
	 :fs_cod_reparto,
	 :fs_cod_lavorazione,
	 :ls_cod_deposito_fornitore,
	 :fs_cod_prodotto_spedito,
	 :ldd_nuova_quan_impegnata,
	 :ldd_nuova_quan_impegnata);

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore in aggiunta impegnato in impegni_deposito_fasi_com~r~n " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

else

	if sqlca.sqlcode = 0 then
		
		update impegni_deposito_fasi_com
		set    quan_impegnata = quan_impegnata + :ldd_nuova_quan_impegnata,
				 quan_impegnata_ultima =:ldd_nuova_quan_impegnata
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
				 anno_commessa = :fl_anno_commessa 		and    
				 num_commessa = :fl_num_commessa 		and    
				 prog_riga = :fl_prog_riga_commessa 		and    
				 cod_prodotto_fase = :fs_cod_prodotto_fase and
				 cod_versione = :fs_cod_versione_fase 		and    
				 cod_reparto = :fs_cod_reparto 		and    
				 cod_lavorazione = :fs_cod_lavorazione 		and    
				 cod_deposito = :ls_cod_deposito_fornitore 	and    
				 cod_prodotto_spedito = :fs_cod_prodotto_spedito;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore in aggiornamento impegnato in impegni_deposito_fasi_com~r~n" + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
	end if		
	
end if	

return 0
end function

public function integer wf_carica_mag_terzista (string fs_cod_fornitore, string fs_cod_tipo_det_ven, string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, decimal fd_quantita);string ls_cod_operatore, ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
       ls_cod_banca_clien_for, ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_cod_causale, ls_flag_riferimento, ls_cod_tipo_det_ven_rif, ls_des_riferimento, &
		 ls_cod_iva, ls_cod_tipo_movimento, ls_cod_misura, ls_cod_iva_rif, ls_cod_tipo_movimento_rif, &
		 ls_tes_tabella, ls_det_tabella, ls_prog_riga, ls_quantita, ls_messaggio, ls_cod_documento, &
		 ls_numeratore_documento, ls_flag_tipo_det_ven, ls_cod_deposito_merce[], ls_cod_ubicazione[], &
		 ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], ls_sql, ls_cod_deposito_mov, ls_flag_fornitore, &
		 ls_cod_fornitore_mov, ls_cod_deposito_fornitore, ls_cod_iva_det, ls_cod_iva_esente, ls_flag_movimenti, ls_null
long ll_anno_registrazione[], ll_num_registrazione[], ll_max_registrazione, ll_i, ll_num_prodotti, &
     ll_riga, ll_num_documento, ll_anno_esercizio, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
	  ll_prog_stock[], ll_cont_mov, ll_prog_ordinamento
double ld_sconto, ld_cambio_ven
datetime ldt_data_registrazione, ldt_data_inizio_trasporto, ldt_ora_inizio_trasporto, ldt_oggi, ldt_data_stock[]
date	ld_oggi

uo_magazzino luo_mag

ldt_data_registrazione = datetime( date( today()), 00:00:00)
ld_oggi = date(today())
setnull(ls_null)
setnull(ll_num_documento)

// leggo il tipo movimento da tipi dettagli e nel caso cod_iva = null allora leggo anche l'aliquota iva
// di default dal tipo dettaglio ( anche se in realtà non me ne frega niente perchè da queste bolle in
// teoria non seguirà mai fattura )
select cod_tipo_movimento,   
		 flag_tipo_det_ven
into  :ls_cod_tipo_movimento,   
		:ls_flag_tipo_det_ven
from  tab_tipi_det_ven
where cod_azienda = :s_cs_xx.cod_azienda and  
		cod_tipo_det_ven = :fs_cod_tipo_det_ven ;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SEP", "Errore durante la ricerca tipo dettaglio vendita~r~n" + sqlca.sqlerrtext, stopsign!)
	return -1
end if

if ls_flag_tipo_det_ven = "M" and isnull(ls_cod_tipo_movimento) then
	g_mb.messagebox("SEP",  "Attenzione: è stato configurato in maniera non corretta il tipo dettaglio vendite.~r~nIl tipo dettaglio è di un prodotto a magazzino ma non si è specificato il relativo tipo movimento di magazzino!~r~nOperazione annullata", stopsign!)
	return -1
end if

if ls_flag_tipo_det_ven = "M" then   // solo se tipo det = prodotti magazzino

	ls_cod_deposito_merce[1] = fs_cod_deposito
	ls_cod_ubicazione[1] = fs_cod_ubicazione
	ls_cod_lotto[1] = fs_cod_lotto
	ldt_data_stock[1] = fdt_data_stock
	ll_prog_stock[1] = fl_prog_stock
	
	// -----------------------------------  assegno il deposito corretto --------------------------------------------		

	ll_cont_mov = 0
	declare cu_movimenti dynamic cursor for sqlsa;
	
	ls_sql = "select cod_deposito, flag_fornitore, cod_fornitore, ordinamento from det_tipi_movimenti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_movimento = '" + ls_cod_tipo_movimento + "' order by ordinamento ASC" 
	prepare sqlsa from :ls_sql;
	open dynamic cu_movimenti;
	do while 1=1
		fetch cu_movimenti INTO :ls_cod_deposito_mov, :ls_flag_fornitore, :ls_cod_fornitore_mov, :ll_prog_ordinamento ;
		if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
		ll_cont_mov ++
		setnull(ls_cod_fornitore[ll_cont_mov])
		setnull(ls_cod_cliente[ll_cont_mov])
		if ll_cont_mov > 1 then
			setnull(ls_cod_ubicazione[ll_cont_mov])
			setnull(ls_cod_lotto[ll_cont_mov])
			setnull(ldt_data_stock[ll_cont_mov])
			setnull(ll_prog_stock[ll_cont_mov])
			setnull(ls_cod_fornitore[ll_cont_mov])
			setnull(ls_cod_deposito_merce[ll_cont_mov])
		end if
		if ls_flag_fornitore = "S" then
			if not isnull(ls_cod_fornitore_mov) and ls_cod_fornitore_mov <> fs_cod_fornitore then g_mb.messagebox("Creazione Bolle Terzisti","Attenzione il fornitore indicato nel tipo movimento è diverso dal fornitore ~r~nselezionato durante la procedura di generazione bolle~r~nIl fornitore selezionato dall'utente viene sostituito al fornitore indicato nel tipo movimento")
			ls_cod_fornitore[ll_cont_mov] = fs_cod_fornitore
			setnull(ls_cod_deposito_fornitore)
			select cod_deposito
			into   :ls_cod_deposito_fornitore
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_fornitore = :ls_cod_fornitore[ll_cont_mov] ;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "SEP",  "Errore durante la ricerca dati fornitore su tabella ANAG_FORNITORI~r~nDettaglio errore "+sqlca.sqlerrtext, stopsign!)
				return -1
			end if
			ls_cod_deposito_merce[ll_cont_mov] = ls_cod_deposito_fornitore 
			ls_cod_ubicazione[ll_cont_mov] = ls_cod_fornitore[ll_cont_mov]
			if isnull(ls_cod_deposito_merce[ll_cont_mov])  then
				ls_cod_deposito_merce[ll_cont_mov] = ls_cod_deposito_mov
				if isnull(ls_cod_deposito_merce[ll_cont_mov]) then
					ls_cod_deposito_merce[ll_cont_mov] = fs_cod_deposito
				end if
			end if
		end if
	loop
	close cu_movimenti;
	if ll_cont_mov = 0 or isnull(ll_cont_mov) then
		g_mb.messagebox( "SEP",  "Attenzione il movimento non è configurato correttamente: verificare tipi movimenti; procedura abbandonata ", stopsign!)
		return -1
	end if
	
	// --------------------------------------------------------------------------------------------------------------
	
	if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, fs_cod_prodotto, ls_cod_deposito_merce[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], ref ll_anno_reg_des_mov, ref ll_num_reg_des_mov) = -1 then
		g_mb.messagebox( "SEP",  "Si è verificato un errore in fase di creazione destinazioni movimenti.", stopsign!)
		return -1
	end if

	if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, fs_cod_prodotto) = -1 then
		return -1
	end if
	
	luo_mag = create uo_magazzino
	
	if luo_mag.uof_movimenti_mag ( ldt_data_registrazione, &
											ls_cod_tipo_movimento, &
											"S", &
											fs_cod_prodotto, &
											fd_quantita, &
											0, &
											ll_num_documento, &
											ldt_data_registrazione, &
											ls_null, &
											ll_anno_reg_des_mov, &
											ll_num_reg_des_mov, &
											ls_cod_deposito_merce[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_fornitore[], &
											ls_cod_cliente[], &
											ll_anno_registrazione[], &
											ll_num_registrazione[] ) = 0 then
	
		if f_elimina_dest_mov_mag (ll_anno_reg_des_mov, &
											ll_num_reg_des_mov) = -1 then
											destroy luo_mag;
			return -1
		end if
		destroy luo_mag;
		return 0
	else
		destroy luo_mag;
		return -1
	end if	

	destroy luo_mag;

end if

return 0
end function

public function double wf_controlla_qta_pf ();double ld_return = 0
long ll_handle, ll_sl_primo_livello[], ll_i
long ll_anno_commessa, ll_num_commessa, ll_prog_riga
treeviewitem ltv_item
s_chiave_distinta l_chiave_distinta
string ls_cod_prodotto_figlio, ls_cod_versione_figlio, ls_cod_reparto, ls_cod_lavorazione
double ld_quan_utilizzo, ld_quan_prodotta, ld_quan_pf_temp, ld_quan_pf_temp_min
double ld_quan_prodotta_pf, ldd_quan_utilizzata
string ls_cod_prodotto_pf

if isvalid(w_avanz_prod_com_attivazione) then
else
	return 0
end if

//handle del primo SL di 1° livello
ll_handle = w_avanz_prod_com_attivazione.tv_db.finditem(childtreeitem!, 1)
ll_i = 0

//carica tutti gli handles dei SL di primo livello
do while ll_handle > 0
	ll_i += 1
	ll_sl_primo_livello[ll_i] = ll_handle
	ll_handle = w_avanz_prod_com_attivazione.tv_db.finditem(NextTreeItem!, ll_handle)
loop

ll_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")

select quan_prodotta
into :ld_quan_prodotta_pf
from det_anag_commesse
where cod_azienda=:s_cs_xx.cod_azienda
	and anno_commessa=:ll_anno_commessa and num_commessa=:ll_num_commessa
	and prog_riga=:ll_prog_riga;
	
if sqlca.sqlcode = -1 then
	g_mb.messagebox("SEP","Errore durante la lettura della qta PF prodotta!",StopSign!)
	return 0
end if
if isnull(ld_quan_prodotta_pf) then ld_quan_prodotta_pf = 0

//processa tutti i SL di primo livello per calcolare il val minimo possibile di qta di PF caricabile parzialmente
for ll_i = 1 to upperbound(ll_sl_primo_livello)
	ll_handle = ll_sl_primo_livello[ll_i]

	if ll_handle > 0 then
		w_avanz_prod_com_attivazione.tv_db.getitem(ll_handle,ltv_item)
		l_chiave_distinta = ltv_item.data
	
		ls_cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio
		ls_cod_versione_figlio = l_chiave_distinta.cod_versione_figlio
		ls_cod_reparto = l_chiave_distinta.cod_reparto
		ls_cod_lavorazione = l_chiave_distinta.cod_lavorazione
		ld_quan_utilizzo = l_chiave_distinta.quan_utilizzo
	
		//leggi nella avan_produzione_com la qta prodotta del SL
		SELECT quan_prodotta
		INTO  :ld_quan_prodotta
		FROM 	avan_produzione_com
		WHERE cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :ll_anno_commessa and
				num_commessa = :ll_num_commessa and
				cod_prodotto = :ls_cod_prodotto_figlio and
				cod_versione = :ls_cod_versione_figlio and
				prog_riga = :ll_prog_riga and
				cod_reparto = :ls_cod_reparto and
				cod_lavorazione = :ls_cod_lavorazione;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox( "SEP", "Errore in ricerca quantita prodotta.~r~r"+sqlca.sqlerrtext)
			return 0
		end if
		
//		 select cod_prodotto
//		into   :ls_cod_prodotto_pf
//		from   anag_commesse
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and 	 anno_commessa=:ll_anno_commessa
//		and    num_commessa=:ll_num_commessa;
//		
//		//leggi nella mp_com_stock_det_orari quella già utilizzata in questa commessa
//		select sum(quan_utilizzata)
//		 into :ldd_quan_utilizzata
//		 from mp_com_stock_det_orari
//		 WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
//		 anno_commessa = :ll_anno_commessa  AND    
//		 num_commessa = :ll_num_commessa  	AND    
//		 prog_riga = :ll_prog_riga 			AND    
//		 cod_prodotto = :ls_cod_prodotto_figlio  AND
//		 cod_prodotto_fase = :ls_cod_prodotto_pf and
//		 cod_reparto = :ls_cod_reparto  		AND    
//		 cod_lavorazione = :ls_cod_lavorazione;		
//		
//		if isnull(ldd_quan_utilizzata) then ldd_quan_utilizzata = 0
//		
//		//quella effettivamente disponibile è per la produzione del pf è:
//		//(cioè quella finora prodotta meno quella già utilizzata)
//		ld_quan_prodotta = ld_quan_prodotta - ldd_quan_utilizzata
		
		if isnull(ld_quan_prodotta) then ld_quan_prodotta = 0
		
		//determino la qta di PF caricabile con la qta prodotta di tale SL
		ld_quan_pf_temp = truncate(ld_quan_prodotta / ld_quan_utilizzo - ld_quan_prodotta_pf, 0)
		if ll_i = 1 then
			//sono al primo step
			
			//salvo la qta, mi servirà al successivo step (se ci sono altri SL di primo livello)
			ld_quan_pf_temp_min = ld_quan_pf_temp	
		else
			
			//se ho ottenuto 0 non posso produrre PF perchè il SL corrente non ha la qta sufficiente
			if ld_quan_pf_temp = 0 then return 0
			
			if ld_quan_pf_temp < ld_quan_pf_temp_min then
				//ho ottenuto una qta PF inferiore: me lo conservo per il prossimo step (se ci sono altri SL di primo livello)
				ld_quan_pf_temp_min = ld_quan_pf_temp
			end if
		end if
	end if
next

if isnull(ld_quan_pf_temp_min) then ld_quan_pf_temp_min = 0
return ld_quan_pf_temp_min
end function

public function string wf_get_depositi_fornitore (string as_cod_fornitore, ref string as_depositi[]);//torna la lista dei depositi del fornitore in anag_depositi formattata con singoli apici e separati da virgola
//es.	'D01','D02','100','105'
//		'D01'				se unico
//se non ce ne sono torna stringa vuota
//inoltre nella var. by ref torna un array con questi codici deposito

string				ls_sql, ls_return, ls_errore
datastore		lds_data
long				ll_ret, ll_index


ls_sql = "select cod_deposito "+&
			"from   anag_depositi "+&
			"where  cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_fornitore='"+as_cod_fornitore+"'"

//mi disinteresso dell'errore
ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_ret <=0 then return ""

ls_return = ""

for ll_index=1 to ll_ret
	if ll_index>1 then ls_return+=","
	ls_return += "'"+lds_data.getitemstring(ll_index, 1)+"'"
	
	as_depositi[ll_index] = lds_data.getitemstring(ll_index, 1)
next


return ls_return
end function

on w_carica_bolle_terzisti.create
int iCurrent
call super::create
this.st_info_2=create st_info_2
this.st_info_1=create st_info_1
this.cb_genera=create cb_genera
this.cbx_bolla_ingresso=create cbx_bolla_ingresso
this.st_prodotto_ingresso=create st_prodotto_ingresso
this.em_quan_arrivata=create em_quan_arrivata
this.em_data_ingresso=create em_data_ingresso
this.st_1=create st_1
this.st_2=create st_2
this.cbx_acc_mat=create cbx_acc_mat
this.cb_togli=create cb_togli
this.dw_det_prod_bolle_in_com=create dw_det_prod_bolle_in_com
this.st_3=create st_3
this.dw_elenco_prod_scarico_fornitore=create dw_elenco_prod_scarico_fornitore
this.dw_folder=create dw_folder
this.dw_lista_prod_bolle_in_com=create dw_lista_prod_bolle_in_com
this.dw_lista_prodotti_scarico=create dw_lista_prodotti_scarico
this.dw_scelta_stock_fornitore=create dw_scelta_stock_fornitore
this.cb_aggiungi=create cb_aggiungi
this.st_stquan_impegnata=create st_stquan_impegnata
this.st_quan_impegnata=create st_quan_impegnata
this.dw_elenco_fasi_esterne_attivazione=create dw_elenco_fasi_esterne_attivazione
this.dw_scelta_fornitore=create dw_scelta_fornitore
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_info_2
this.Control[iCurrent+2]=this.st_info_1
this.Control[iCurrent+3]=this.cb_genera
this.Control[iCurrent+4]=this.cbx_bolla_ingresso
this.Control[iCurrent+5]=this.st_prodotto_ingresso
this.Control[iCurrent+6]=this.em_quan_arrivata
this.Control[iCurrent+7]=this.em_data_ingresso
this.Control[iCurrent+8]=this.st_1
this.Control[iCurrent+9]=this.st_2
this.Control[iCurrent+10]=this.cbx_acc_mat
this.Control[iCurrent+11]=this.cb_togli
this.Control[iCurrent+12]=this.dw_det_prod_bolle_in_com
this.Control[iCurrent+13]=this.st_3
this.Control[iCurrent+14]=this.dw_elenco_prod_scarico_fornitore
this.Control[iCurrent+15]=this.dw_folder
this.Control[iCurrent+16]=this.dw_lista_prod_bolle_in_com
this.Control[iCurrent+17]=this.dw_lista_prodotti_scarico
this.Control[iCurrent+18]=this.dw_scelta_stock_fornitore
this.Control[iCurrent+19]=this.cb_aggiungi
this.Control[iCurrent+20]=this.st_stquan_impegnata
this.Control[iCurrent+21]=this.st_quan_impegnata
this.Control[iCurrent+22]=this.dw_elenco_fasi_esterne_attivazione
this.Control[iCurrent+23]=this.dw_scelta_fornitore
end on

on w_carica_bolle_terzisti.destroy
call super::destroy
destroy(this.st_info_2)
destroy(this.st_info_1)
destroy(this.cb_genera)
destroy(this.cbx_bolla_ingresso)
destroy(this.st_prodotto_ingresso)
destroy(this.em_quan_arrivata)
destroy(this.em_data_ingresso)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.cbx_acc_mat)
destroy(this.cb_togli)
destroy(this.dw_det_prod_bolle_in_com)
destroy(this.st_3)
destroy(this.dw_elenco_prod_scarico_fornitore)
destroy(this.dw_folder)
destroy(this.dw_lista_prod_bolle_in_com)
destroy(this.dw_lista_prodotti_scarico)
destroy(this.dw_scelta_stock_fornitore)
destroy(this.cb_aggiungi)
destroy(this.st_stquan_impegnata)
destroy(this.st_quan_impegnata)
destroy(this.dw_elenco_fasi_esterne_attivazione)
destroy(this.dw_scelta_fornitore)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

set_w_options(c_noenablepopup)

l_objects[1] = dw_scelta_fornitore
dw_folder.fu_AssignTab(1, "Fornitore", l_Objects[])

l_objects[1] = dw_elenco_prod_scarico_fornitore
l_objects[2] = cb_togli
dw_folder.fu_AssignTab(3, "Riassunto Entrata", l_Objects[])

l_objects[1] = dw_lista_prod_bolle_in_com
l_objects[2] = dw_det_prod_bolle_in_com
dw_folder.fu_AssignTab(4, "Ingressi Merce", l_Objects[])

l_objects[1] = dw_elenco_fasi_esterne_attivazione
l_objects[2] = dw_lista_prodotti_scarico
l_objects[3] = dw_scelta_stock_fornitore
l_objects[4] = cb_aggiungi
l_objects[5] = st_stquan_impegnata
l_objects[6] = st_quan_impegnata
l_objects[7] = st_info_1
l_objects[8] = st_info_2
dw_folder.fu_AssignTab(2, "Scelta Prod.Scarico", l_objects[])

dw_folder.fu_FolderCreate(4,4)

dw_elenco_fasi_esterne_attivazione.set_dw_options(sqlca, i_openparm,c_nonew + & 
												  				  c_nodelete + c_nomodify +  & 
												  				  c_disablecc,c_disableccinsert)

dw_lista_prod_bolle_in_com.set_dw_options(sqlca, &
                                    		   pcca.null_object, &
		                                       c_scrollparent + c_nonew + c_nodelete, &
      		                                 c_default)


//dw_det_prod_bolle_in_com.set_dw_options(sqlca, &
//                                    		 dw_lista_prod_bolle_in_com, &
//		                                     c_sharedata + c_scrollparent + c_nonew + c_nodelete, &
//      		                               c_default)
														 
														 
dw_lista_prodotti_scarico.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)

dw_scelta_stock_fornitore.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)

dw_elenco_prod_scarico_fornitore.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)


dw_scelta_fornitore.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_newonopen + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)


iuo_dw_main = dw_elenco_fasi_esterne_attivazione

dw_folder.fu_SelectTab(1)

save_on_close(c_socnosave)

em_data_ingresso.text = string(today())

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_scelta_fornitore, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_terzista = 'S' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(dw_scelta_fornitore, &
                 "cod_fornitore_2", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_terzista = 'S' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")					  
end event

type st_info_2 from statictext within w_carica_bolle_terzisti
integer x = 50
integer y = 524
integer width = 3497
integer height = 88
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "PRODOTTI  DELLA FASE ATTUALMENTE IN CARICO AL TERZISTA"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_info_1 from statictext within w_carica_bolle_terzisti
integer x = 50
integer y = 1168
integer width = 3497
integer height = 88
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "ELENCO DEI LOTTI ATTUALMENTE IN CARICO AL TERZISTA"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_genera from commandbutton within w_carica_bolle_terzisti
integer x = 3630
integer y = 2036
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera In."
end type

event clicked;string		ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_cod_prodotto_fase,& 
			ls_cod_versione,ls_errore,ls_cod_mov_magazzino, ls_cod_prodotto, ls_cod_versione_fase, &
			ls_cod_tipo_det_ven,ls_cod_tipo_bol_ven,ls_conferma, ls_cod_fornitore_ingresso, ls_cod_prodotto_spedito,& 
			ls_cod_ubicazione[],ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[], ls_flag_tipo_avanzamento,&
			ls_cod_deposito_sl[],ls_cod_tipo_mov_ver_sl,ls_cod_ubicazione_test, ls_cod_deposito_fornitore[],& 
			ls_cod_lotto_test,ls_flag_cliente,ls_flag_fornitore,ls_cod_prodotto_scarico,ls_cod_tipo_mov_scarico_terzista, &
			ls_cod_deposito_semilavorato, ls_cod_ubicazione_semilavorato, ls_cod_lotto_semilavorato, &
			ls_cod_fornitore_terzista, ls_cod_tipo_det_ven_sl, ls_cod_sl, ls_dep[]
			
long		ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_ingresso,ll_t,ll_anno_reg_sl,ll_num_reg_sl, & 
			ll_prog_stock[],ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_anno_registrazione[],ll_num_registrazione[], & 
			ll_righe_scarico,ll_num_fasi_aperte
			
datetime	ldt_oggi,ld_data_stock[], ldt_data_stock_semilavorato

dec{4}	ldd_quan_arrivata,ldd_quan_prelevata,ldd_quan_in_produzione,ldd_quan_in_produzione_prog,ldd_quan_prodotta,&
			ldd_quan_impegnata_attuale,ldd_quan_impegnata_fornitore,ldd_quan_utilizzo,ldd_quan_impegnata,ldd_somma_quan_prelevata, ll_prog_stock_semilavorato
integer	li_anno_commessa,li_risposta

dec{4}	ldd_quan_prodotta_save, ldd_quan_in_produzione_anag

uo_magazzino luo_mag

if (g_mb.messagebox("Sep","Sei sicuro di generare l'ingresso merce?",Exclamation!,yesno!, 2))=2 then return
		 
ll_righe_scarico = dw_elenco_prod_scarico_fornitore.rowcount()

if ll_righe_scarico = 0 then
	g_mb.messagebox("Sep","Attenzione non è stata selezionata alcuna materia prima da scaricare dal magazzino del fornitore. Verificare.",exclamation!)
	return
end if

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ldd_quan_arrivata = double(em_quan_arrivata.text)
ldd_quan_utilizzo = dw_lista_prodotti_scarico.getitemnumber(dw_lista_prodotti_scarico.getrow(),"quan_utilizzo")
ls_cod_prodotto_spedito = dw_lista_prodotti_scarico.getitemstring(dw_lista_prodotti_scarico.getrow(),"cod_prodotto")

ldt_oggi = datetime(today(),time('00:00:00'))

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    anno_commessa = :li_anno_commessa  
AND    num_commessa = :ll_num_commessa;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep", "Errore in ricarca commessa~r~n" + sqlca.sqlerrtext, stopsign!)
	return
end if

select cod_deposito_sl,
		 cod_tipo_mov_ver_sl,
		 cod_tipo_mov_scarico_terzista,
		 cod_tipo_det_ven_sl
into   :ls_cod_deposito_sl[1],
		 :ls_cod_tipo_mov_ver_sl,
		 :ls_cod_tipo_mov_scarico_terzista,
		 :ls_cod_tipo_det_ven_sl
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:ls_cod_tipo_commessa;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep", "Errore in ricerca tipo commessa~r~n" + sqlca.sqlerrtext, stopsign!)
	return
end if

if isnull(ls_cod_deposito_sl[1]) or isnull(ls_cod_tipo_mov_ver_sl) then
	g_mb.messagebox("Sep", "Attenzione! Il tipo commessa selezionato non è correttamente impostato per eseguire le movimentazioni dei Semilavorati (deposito SL o tipo movimento versamento SL mancante).", stopsign!)
	return
end if

select anno_reg_sl,
		 num_reg_sl
into   :ll_anno_reg_sl,
		 :ll_num_reg_sl
from   avan_produzione_com
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
		 anno_commessa = :li_anno_commessa  AND    
		 num_commessa = :ll_num_commessa    AND    
		 prog_riga = :ll_prog_riga 			AND    
		 cod_prodotto = :ls_cod_prodotto_fase and
		 cod_versione = :ls_cod_versione_fase AND    
		 cod_reparto = :ls_cod_reparto  		AND    
		 cod_lavorazione = :ls_cod_lavorazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep", "Errore in ricerca dati avanzamento produzione~r~n" + sqlca.sqlerrtext, stopsign!)
	return
end if

//*************************** Inizio Verifica correttezza tipo movimento mag per SL

declare righe_det_tipi_m cursor for
select  cod_ubicazione,
		  cod_lotto,
		  flag_cliente,
		  flag_fornitore
from    det_tipi_movimenti
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_movimento=:ls_cod_tipo_mov_ver_sl;

open righe_det_tipi_m;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep", "Errore in OPEN cursore righe_det_tipi_m~r~n" + sqlca.sqlerrtext, stopsign!)
	return
end if

do while true
	fetch righe_det_tipi_m
	into  :ls_cod_ubicazione_test,
			:ls_cod_lotto_test,
			:ls_flag_cliente,
			:ls_flag_fornitore;

	if sqlca.sqlcode = 100 then exit

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep", "Errore fetch cursore righe_det_tipi_m~r~n" + sqlca.sqlerrtext, stopsign!)
		close righe_det_tipi_m;
		return
	end if
	

	if isnull(ls_cod_ubicazione_test) then
		g_mb.messagebox("Sep", "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poichè il codice ubicazione non è stato indicato." , stopsign!)
		close righe_det_tipi_m;
		return 
	end if

	if isnull(ls_cod_lotto_test) then
		g_mb.messagebox("Sep", "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poichè il codice lotto non è stato indicato."  , stopsign!)
		close righe_det_tipi_m;
		return
	end if

	if ls_flag_cliente='S' then
		g_mb.messagebox("Sep", "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poichè viene richiesto il codice cliente."  , stopsign!)
		close righe_det_tipi_m;
		return
	end if

	if ls_flag_fornitore='S' then
		g_mb.messagebox("Sep", "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poiche' viene richiesto il codice fornitore."   , stopsign!)
		close righe_det_tipi_m;
		return
	end if

loop

close righe_det_tipi_m;

//*************************** Fine Verifica correttezza tipo movimento mag per SL
//	***	Michela 29/06/2007: in questo punto fa il carico a magazzino del semilavorato

setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ld_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])
			
if not isnull(ll_anno_reg_sl) and not isnull(ll_num_reg_sl) then
	
	select cod_ubicazione,
			 cod_lotto,
			 data_stock,
			 prog_stock
	into   :ls_cod_ubicazione[1],
			 :ls_cod_lotto[1],
			 :ld_data_stock[1],
			 :ll_prog_stock[1]
	from   mov_magazzino
	where  cod_azienda =:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_reg_sl
	and	 num_registrazione=:ll_num_reg_sl;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Sep", "Errore in ricerca del movimento di magazzino di carico SL~r~n" + sqlca.sqlerrtext, stopsign!)
		return
	end if
	
end if

ll_anno_reg_sl = 0
ll_num_reg_sl = 0

if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_ver_sl, ls_cod_prodotto_fase, ls_cod_deposito_sl[], & 
							  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
							  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
							  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then

	g_mb.messagebox("Sep", "Si è verificato un errore in preparazione destinazione movimenti.", stopsign!)
	rollback;
	return 

end if

if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
									ls_cod_tipo_mov_ver_sl, ls_cod_prodotto_fase) = -1 then

	 g_mb.messagebox("Sep", "Si è verificato un errore in fase di verifica destinazioni movimenti magazzino." , stopsign!)
  rollback;
  return

end if

luo_mag = create uo_magazzino

li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
										 ls_cod_tipo_mov_ver_sl, &
										 "N", &
										 ls_cod_prodotto_fase, &
										 ldd_quan_arrivata, &
										 1, &
										 ll_num_commessa, &
										 datetime(today(),time('00:00:00')), &
										 "", &
										 ll_anno_reg_des_mov, &
										 ll_num_reg_des_mov, &
										 ls_cod_deposito_sl[], &
										 ls_cod_ubicazione[], &
										 ls_cod_lotto[], &
										 ld_data_stock[], &
										 ll_prog_stock[], &
										 ls_cod_fornitore[], &
										 ls_cod_cliente[], &
										 ll_anno_registrazione[], &
										 ll_num_registrazione[])
										 
destroy luo_mag

if li_risposta=-1 then
	g_mb.messagebox("Sep", "Errore in creazione movimenti magazzino." , stopsign!)
   rollback;
	return
end if
	
li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)

if li_risposta = -1 then
	g_mb.messagebox("Sep", "Si è verificato un errore in fase di cancellazione destinazioni movimenti." , stopsign!)
   rollback;
	return -1
end if

ll_anno_reg_sl = ll_anno_registrazione[1]
ll_num_reg_sl = ll_num_registrazione[1]

//	*** fine carico a magazzino del semilavorato: quindi mi salvo il riferimento dello stock del semilavorato

ls_cod_deposito_semilavorato =  ls_cod_deposito_sl[1]
ls_cod_ubicazione_semilavorato = ls_cod_ubicazione[1]
ls_cod_lotto_semilavorato = ls_cod_lotto[1]
ldt_data_stock_semilavorato = ld_data_stock[1]
ll_prog_stock_semilavorato = ll_prog_stock[1]

//	***

select quan_in_produzione
into   :ldd_quan_in_produzione
from   avan_produzione_com
WHERE  cod_azienda = :s_cs_xx.cod_azienda    AND    
       anno_commessa = :li_anno_commessa  	AND    
		 num_commessa = :ll_num_commessa       AND    
		 prog_riga = :ll_prog_riga 				AND    
		 cod_prodotto = :ls_cod_prodotto_fase  and
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  			AND    
		 cod_lavorazione = :ls_cod_lavorazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep", "Errore in ricerca quantità in produzione ~r~n" + sqlca.sqlerrtext, stopsign!)
   rollback;
	return
end if

ldd_quan_in_produzione = ldd_quan_in_produzione - ldd_quan_arrivata
if ldd_quan_in_produzione < 0 then ldd_quan_in_produzione = 0

//prima di aggiornare mi salvo la qta in produzione e la qta prodotta
dec{4} ldd_quan_in_prod_old, ldd_quan_prodotta_old, ldd_quan_prodotta_old2, ldd_quan_prodotta_old3

select quan_in_produzione,quan_prodotta
into   :ldd_quan_in_prod_old, :ldd_quan_prodotta_old
from   avan_produzione_com
WHERE  cod_azienda = :s_cs_xx.cod_azienda    AND    
       anno_commessa = :li_anno_commessa  	AND    
		 num_commessa = :ll_num_commessa       AND    
		 prog_riga = :ll_prog_riga 				AND    
		 cod_prodotto = :ls_cod_prodotto_fase  and
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  			AND    
		 cod_lavorazione = :ls_cod_lavorazione;
		 
if isnull(ldd_quan_in_prod_old) then ldd_quan_in_prod_old = 0
if isnull(ldd_quan_prodotta_old) then ldd_quan_prodotta_old = 0
//---------------------------------------------------------------------------------------

UPDATE avan_produzione_com
SET    quan_prodotta = quan_prodotta + :ldd_quan_arrivata,
		 quan_in_produzione = :ldd_quan_in_produzione,
		 anno_reg_sl = :ll_anno_reg_sl,
		 num_reg_sl = :ll_num_reg_sl
WHERE  cod_azienda = :s_cs_xx.cod_azienda 	AND    
       anno_commessa = :li_anno_commessa  	AND    
		 num_commessa = :ll_num_commessa  		AND    
		 prog_riga = :ll_prog_riga 				AND    
		 cod_prodotto = :ls_cod_prodotto_fase  AND
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  			AND    
		 cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep", "Errore in aggiornamento dati avanzamento produzione~r~n" + sqlca.sqlerrtext, stopsign!)
   rollback;
	return
end if

ls_cod_fornitore_ingresso = dw_scelta_fornitore.getitemstring( dw_scelta_fornitore.getrow(), "cod_fornitore")

//***********
//INIZIO INSERIMENTO NUOVO ARRIVO IN TABELLA PROD_BOLLE_IN_COM
select max(prog_ingresso)
into   :ll_prog_ingresso
from   prod_bolle_in_com
where cod_azienda = :s_cs_xx.cod_azienda		and   
		anno_commessa = :li_anno_commessa		and   
		num_commessa = :ll_num_commessa			and   
		prog_riga = :ll_prog_riga					and   
		cod_prodotto_fase = :ls_cod_prodotto_fase and   
		cod_versione = :ls_cod_versione_fase   and
		cod_reparto = :ls_cod_reparto				and   
		cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep", "Errore in calcolo progressivo bolle entrata (prod_bolle_in_com)~r~n" + sqlca.sqlerrtext, stopsign!)
	rollback;
	return
end if

if isnull(ll_prog_ingresso) then
	ll_prog_ingresso = 1
else
	ll_prog_ingresso++
end if

INSERT INTO prod_bolle_in_com  
		( cod_azienda,   
		  anno_commessa,   
		  cod_prodotto_fase, 
		  cod_versione,
		  num_commessa,   
		  prog_riga,   
		  cod_reparto,   
		  cod_lavorazione,   
		  prog_ingresso,   
		  anno_acc_materiali,   
		  num_acc_materiali,   
		  anno_bolla_acq,   
		  num_bolla_acq,   
		  prog_riga_bolla_acq,   
		  cod_prodotto_in,   
		  cod_deposito,   
		  cod_ubicazione,   
		  cod_lotto,   
		  data_stock,   
		  prog_stock,   
		  quan_arrivata,   
		  cod_fornitore,   
		  data_ingresso )  
VALUES ( :s_cs_xx.cod_azienda,   
		  :li_anno_commessa,   
		  :ls_cod_prodotto_fase,
		  :ls_cod_versione_fase,
		  :ll_num_commessa,   
		  :ll_prog_riga,   
		  :ls_cod_reparto,   
		  :ls_cod_lavorazione,   
		  :ll_prog_ingresso,   
		  null,   
		  null,   
		  null,   
		  null,   
		  null,   
		  :ls_cod_prodotto_fase,   
		  :ls_cod_deposito_sl[1],   
		  :ls_cod_ubicazione[1],   
		  :ls_cod_lotto[1],   
		  :ld_data_stock[1],   
		  :ll_prog_stock[1],   
		  :ldd_quan_arrivata,   
		  :ls_cod_fornitore_ingresso,   
		  :ldt_oggi )  ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep", "Errore in registrazione ingresso merce in prod_bolle_in_com~r~n" + sqlca.sqlerrtext, stopsign!)
	rollback;
	return
end if
	

//FINE INSERIMENTO NUOVO ARRIVO


//**********************
//INIZIO SCARICO MAGAZZINO FORNITORE

setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ld_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
ls_cod_fornitore[1] = ls_cod_fornitore_ingresso

ll_righe_scarico = dw_elenco_prod_scarico_fornitore.rowcount()


if wf_get_depositi_fornitore(ls_cod_fornitore_ingresso, ls_dep[]) <>"" then
	//se il fornitore ha + depositi (per anag_depositi.cod_fornitore) prendo il primo della lista
	ls_cod_deposito_fornitore[1] = ls_dep[1]
else

	select cod_deposito
	into   :ls_cod_deposito_fornitore[1]
	from   anag_fornitori
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_fornitore=:ls_cod_fornitore_ingresso;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Sep", "Errore in ricerca deposito in anagrafica fornitore : " + sqlca.sqlerrtext, stopsign!)
		rollback;
		return
	end if

end if


for ll_t = 1 to ll_righe_scarico
	dw_scelta_stock_fornitore.setrow(ll_t)
	ls_cod_prodotto_scarico = dw_elenco_prod_scarico_fornitore.getitemstring(ll_t,"cod_prodotto")
	ls_cod_ubicazione[1]  = dw_elenco_prod_scarico_fornitore.getitemstring(ll_t,"cod_ubicazione")
	ls_cod_lotto[1] = dw_elenco_prod_scarico_fornitore.getitemstring(ll_t,"cod_lotto")
	ld_data_stock[1] = dw_elenco_prod_scarico_fornitore.getitemdatetime(ll_t,"data_stock")
	ll_prog_stock[1] = dw_elenco_prod_scarico_fornitore.getitemnumber(ll_t,"prog_stock")
	ldd_quan_prelevata = 	dw_elenco_prod_scarico_fornitore.getitemnumber(ll_t,"quan_prelevata")
	
	if ldd_quan_prelevata > 0 then
		if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_scarico_terzista, ls_cod_prodotto_scarico, ls_cod_deposito_fornitore[], & 
									  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
									  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
									  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
		
			g_mb.messagebox("Sep", "Si è verificato un errore preparazione movimenti.", stopsign!)
			rollback;
			return 
		
		end if
		
		if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
											ls_cod_tipo_mov_scarico_terzista, ls_cod_prodotto_scarico) = -1 then
		
			 g_mb.messagebox("Sep", "Si è verificato un errore in fase di verifica destinazioni movimenti magazzino." , stopsign!)
		  rollback;
		  return
		
		end if
		
		luo_mag = create uo_magazzino
		
		li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
												 ls_cod_tipo_mov_scarico_terzista, &
												 "N", &
												 ls_cod_prodotto_scarico, &
												 ldd_quan_prelevata, &
												 1, &
												 ll_num_commessa, &
												 datetime(today(),time('00:00:00')), &
												 "", &
												 ll_anno_reg_des_mov, &
												 ll_num_reg_des_mov, &
												 ls_cod_deposito_fornitore[], &
												 ls_cod_ubicazione[], &
												 ls_cod_lotto[], &
												 ld_data_stock[], &
												 ll_prog_stock[], &
												 ls_cod_fornitore[], &
												 ls_cod_cliente[], &
												 ll_anno_registrazione[], &
												 ll_num_registrazione[])
												 
		destroy luo_mag
		
		if li_risposta=-1 then
			g_mb.messagebox("Sep", "Errore in creazione movimenti magazzino." , stopsign!)
			rollback;
			return
		end if
			
		li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)
		
		if li_risposta = -1 then
			g_mb.messagebox("Sep", "Si è verificato un errore in fase di cancellazione destinazioni movimenti." , stopsign!)
			rollback;
			return -1
		end if
		
		//***************
		//INIZIO DISIMPEGNO PRODOTTO SUL DEPOSITO DEL FORNITORE
						
		select quan_impegnata
		into   :ldd_quan_impegnata
		from   impegni_deposito_fasi_com
		where  cod_azienda = :s_cs_xx.cod_azienda   	and    
				 anno_commessa = :li_anno_commessa		and    
				 num_commessa = :ll_num_commessa			and    
				 prog_riga = :ll_prog_riga					and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase and
				 cod_versione = :ls_cod_versione_fase  and    
				 cod_reparto = :ls_cod_reparto			and    
				 cod_lavorazione = :ls_cod_lavorazione and    
				 cod_deposito = :ls_cod_deposito_fornitore[1] and    
				 cod_prodotto_spedito = :ls_cod_prodotto_scarico;	
		
	
		choose case sqlca.sqlcode
			case -1
				g_mb.messagebox("Sep","Errore in ricerca impegno fornitore~r~n" + sqlca.sqlerrtext,stopsign!)
				rollback;
				return -1
				
			case 0				
				if ldd_quan_impegnata > ldd_quan_prelevata then
					update impegni_deposito_fasi_com
					set    quan_impegnata = quan_impegnata - :ldd_quan_prelevata
					where  cod_azienda = :s_cs_xx.cod_azienda    and    
							 anno_commessa = :li_anno_commessa		and    
							 num_commessa = :ll_num_commessa			and    
							 prog_riga = :ll_prog_riga					and    
							 cod_prodotto_fase = :ls_cod_prodotto_fase and    
							 cod_versione = :ls_cod_versione_fase  and
							 cod_reparto = :ls_cod_reparto			and    
							 cod_lavorazione = :ls_cod_lavorazione	and    
							 cod_deposito = :ls_cod_deposito_fornitore[1] and    
							 cod_prodotto_spedito = :ls_cod_prodotto_scarico;	
					
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Sep","Errore in fase di disimpegno dal deposito fornitore (impegni_deposito_fasi_com)~r~n" + sqlca.sqlerrtext,stopsign!)
						rollback;
						return -1
					end if
		
				else
					delete impegni_deposito_fasi_com
					where  cod_azienda = :s_cs_xx.cod_azienda		and    
							 anno_commessa = :li_anno_commessa		and    
							 num_commessa = :ll_num_commessa			and    
							 prog_riga = :ll_prog_riga					and    
							 cod_prodotto_fase = :ls_cod_prodotto_fase and
							 cod_versione = :ls_cod_versione_fase  and    
							 cod_reparto = :ls_cod_reparto			and    
							 cod_lavorazione = :ls_cod_lavorazione and    
							 cod_deposito = :ls_cod_deposito_fornitore[1]  and    
							 cod_prodotto_spedito = :ls_cod_prodotto_scarico;	
					
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Sep","Errore in cancellazione impegno dal deposito fornitore (impegni_deposito_fasi_com)~r~n" + sqlca.sqlerrtext,stopsign!)
						rollback;
						return -1
					end if
					
				end if
		end choose
		
		
		
		//FINE DISIMPEGNO PRODOTTO SUL DEPOSITO DEL FORNITORE
		//***************
		
		//****************************************************************************************************************
		// DISIMPEGNO LE MP CONTROLLANDO CONTEMPORANEAMENTE ANCHE LA TABELLA IMPEGNO_MAT_PRIME_COMMESSA

		select quan_impegnata_attuale
		into   :ldd_quan_impegnata_attuale
		from   impegno_mat_prime_commessa
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    cod_prodotto=:ls_cod_prodotto_scarico;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore in disimpegno MP della commessa~r~n " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then // ROUTINE PER LE COMMESSE GENERATE PRIMA DI AGGIUNGERE LA TAB IMPEGNO_MAT_PRIME COMMESSA
		
			select 	quan_impegnata
			into   		:ldd_quan_impegnata_attuale
			from   	anag_prodotti
			where  	cod_azienda = :s_cs_xx.cod_azienda	and    
						cod_prodotto=:ls_cod_prodotto_scarico;
						
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Sep","Errore in ricerca prodotto " + ls_cod_prodotto_scarico + "~r~n" + sqlca.sqlerrtext,stopsign!)
				rollback;
				return -1
			end if
		
			if ldd_quan_impegnata_attuale >= ldd_quan_prelevata then
				
				update 	anag_prodotti																		
				set 	 	quan_impegnata = quan_impegnata - :ldd_quan_prelevata
				where  	cod_azienda = :s_cs_xx.cod_azienda	and    
							cod_prodotto = :ls_cod_prodotto_scarico;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Sep","Errore in aggiornamento impegno in anagrafica~r~n" + sqlca.sqlerrtext,stopsign!)
					rollback;
					return -1
				end if
				
			else
				
				update 	anag_prodotti																		
				set 	 	quan_impegnata = 0
				where  	cod_azienda =	:s_cs_xx.cod_azienda	and    
							cod_prodotto = :ls_cod_prodotto_scarico;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Sep","Errore in aggiornamento impegno in anagrafica~r~n" + sqlca.sqlerrtext,stopsign!)
					rollback;			
					return -1
				end if
				
			end if
			
		else // ROUTINE PER LE COMMESSE GENERATE DOPO L'AGGIUNTA DELLA TAB IMPEGNO_MAT_PRIME COMMESSA
	
			if ldd_quan_impegnata_attuale >= ldd_quan_prelevata then
				
				update 	impegno_mat_prime_commessa
				set 	 	quan_impegnata_attuale =  quan_impegnata_attuale - :ldd_quan_prelevata
				where		cod_azienda = :s_cs_xx.cod_azienda	and    
							anno_commessa = :li_anno_commessa	and    
							num_commessa = :ll_num_commessa	and    
							cod_prodotto = :ls_cod_prodotto_scarico;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Sep","Errore in aggiornamento impegno commessa~r~n" + sqlca.sqlerrtext,stopsign!)
					rollback;
					return -1
				end if
		
				update 	anag_prodotti																		
				set 	 	quan_impegnata = quan_impegnata - :ldd_quan_prelevata
				where		cod_azienda = :s_cs_xx.cod_azienda	and    
							cod_prodotto = :ls_cod_prodotto_scarico;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Sep","Errore in aggiornamento impegno in anagrafica~r~n" + sqlca.sqlerrtext,stopsign!)
					rollback;					
					return -1
				end if
				
			else
				
				update 	impegno_mat_prime_commessa
				set 	 	quan_impegnata_attuale = 0
				where		cod_azienda = :s_cs_xx.cod_azienda	and    
							anno_commessa = :li_anno_commessa	and    
							num_commessa = :ll_num_commessa	and    
							cod_prodotto = :ls_cod_prodotto_scarico;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Sep","Errore in aggiornamento impegno commessa~r~n" + sqlca.sqlerrtext,stopsign!)
					rollback;					
					return -1
				end if
		
				update 	anag_prodotti																		
				set 	 	quan_impegnata = quan_impegnata - :ldd_quan_impegnata_attuale
				where		cod_azienda = :s_cs_xx.cod_azienda	and    
							cod_prodotto = :ls_cod_prodotto_scarico;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Sep","Errore in aggiornamento impegno in anagrafica~r~n" + sqlca.sqlerrtext,stopsign!)
					rollback;					
					return -1
				end if
						
			
			end if
	
		end if
		//****************************************************************************************************************

	end if
next

//FINE SCARICO MAGAZZINO FORNITORE
//********************************

/// *** MICHELA 07/02/2008: DISIMPEGNO IL SEMILAVORATO

select quan_impegnata_attuale
into   :ldd_quan_impegnata_attuale
from   impegno_mat_prime_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 cod_prodotto = :ls_cod_prodotto_fase;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in disimpegno MP della commessa~r~n " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

if sqlca.sqlcode = 0 then 

	if ldd_quan_impegnata_attuale >= ldd_quan_arrivata then
		
		update 	impegno_mat_prime_commessa
		set 	 	quan_impegnata_attuale =  quan_impegnata_attuale - :ldd_quan_arrivata
		where		cod_azienda = :s_cs_xx.cod_azienda	and    
					anno_commessa = :li_anno_commessa	and    
					num_commessa = :ll_num_commessa	and    
					cod_prodotto = :ls_cod_prodotto_fase;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Sep","Errore in aggiornamento impegno commessa~r~n" + sqlca.sqlerrtext,stopsign!)
			rollback;
			return -1
		end if

		update 	anag_prodotti																		
		set 	 	quan_impegnata = quan_impegnata - :ldd_quan_arrivata //:ldd_quan_prelevata
		where		cod_azienda = :s_cs_xx.cod_azienda	and    
					cod_prodotto = :ls_cod_prodotto_fase;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Sep","Errore in aggiornamento impegno in anagrafica~r~n" + sqlca.sqlerrtext,stopsign!)
			rollback;					
			return -1
		end if
		
	else
		
		update 	impegno_mat_prime_commessa
		set 	 	quan_impegnata_attuale = 0
		where		cod_azienda = :s_cs_xx.cod_azienda	and    
					anno_commessa = :li_anno_commessa	and    
					num_commessa = :ll_num_commessa	and    
					cod_prodotto = :ls_cod_prodotto_fase;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Sep","Errore in aggiornamento impegno commessa~r~n" + sqlca.sqlerrtext,stopsign!)
			rollback;					
			return -1
		end if

		update 	anag_prodotti																		
		set 	 	quan_impegnata = quan_impegnata - :ldd_quan_impegnata_attuale
		where		cod_azienda = :s_cs_xx.cod_azienda	and    
					cod_prodotto = :ls_cod_prodotto_fase;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Sep","Errore in aggiornamento impegno in anagrafica~r~n" + sqlca.sqlerrtext,stopsign!)
			rollback;					
			return -1
		end if
				
	
	end if

end if


// *** FINE MODIFICA 07/02/2008

select quan_in_produzione
into   :ldd_quan_in_produzione		
from   avan_produzione_com
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
		 anno_commessa = :li_anno_commessa  AND    
		 num_commessa = :ll_num_commessa  	AND    
		 prog_riga = :ll_prog_riga 			AND    
		 cod_prodotto = :ls_cod_prodotto_fase  AND
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  		AND    
		 cod_lavorazione = :ls_cod_lavorazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep","Errore ricerca quantità in produzione in avan_produzione_com~r~n" + sqlca.sqlerrtext,stopsign!)
	rollback;					
	return -1
end if

//*****************************
//INIZIO CHIUSURA FASE

//Donato 22/12/2008 fare le altre due verifiche necessarie per capire se deve essere
//fatto il carico parziale del PF (se fase superiore)
double ld_qta_pf

if ldd_quan_in_produzione > 0 then
	//si tratta di uno scarico parziale
	
	//il controllo se si tratta di un SL di 1° livello e se tutte le fasi di lavoro
	//dal 2° livello in giù sono chiuse è stato fatto nella w_avanz_prod_com_attivazione
	//queste condizioni sono verificate se e solo se s_cs_xx.parametri.parametro_d_9=99.99
	
	if s_cs_xx.parametri.parametro_d_9=99.99 then
		setnull(s_cs_xx.parametri.parametro_d_9)
		
		//resta da verificare se i SL di 1° livello hanno una qta sufficiente
		//a produrre una certa qta minima di PF
		ld_qta_pf = wf_controlla_qta_pf()
		if ld_qta_pf > 0 then
			//via libera allo scarico parziale dei SL di 1° livello
			//e conseguente carico parziale del PF della Commessa per una qta pari a "ld_qta_pf"
			
			if g_mb.messagebox("Sep","Eseguire il carico parziale del PF?", Question!, YesNo!,2)=1 then
				//###################################
				//###################################			
			
				setnull(ls_cod_ubicazione[1])
				setnull(ls_cod_lotto[1])
				setnull(ld_data_stock[1])
				setnull(ll_prog_stock[1])
				setnull(ls_cod_cliente[1])
				setnull(ls_cod_fornitore[1])
					
				if not isnull(ll_anno_reg_sl) and not isnull(ll_num_reg_sl) then
					
					select cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 prog_stock
					into   :ls_cod_ubicazione[1],
							 :ls_cod_lotto[1],
							 :ld_data_stock[1],
							 :ll_prog_stock[1]
					from   mov_magazzino
					where  cod_azienda =:s_cs_xx.cod_azienda
					and    anno_registrazione=:ll_anno_reg_sl
					and	 num_registrazione=:ll_num_reg_sl;	
				
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Sep", "Errore in ricerca del movimento di magazzino di carico SL~r~n" + sqlca.sqlerrtext, stopsign!)
						return
					end if
					
				end if
		
				ll_anno_reg_sl = 0
				ll_num_reg_sl = 0
				
				
				ls_cod_tipo_mov_ver_sl="SMP"
				if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_ver_sl, ls_cod_prodotto_fase, ls_cod_deposito_sl[], & 
											  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
											  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
											  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				
					g_mb.messagebox("Sep", "Si è verificato un errore in preparazione destinazione movimenti.", stopsign!)
					rollback;
					return 
				
				end if
		
				if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
													ls_cod_tipo_mov_ver_sl, ls_cod_prodotto_fase) = -1 then
				
					 g_mb.messagebox("Sep", "Si è verificato un errore in fase di verifica destinazioni movimenti magazzino." , stopsign!)
				  rollback;
				  return
				
				end if
				
				luo_mag = create uo_magazzino
				
				//------------------
		
				//è necessario ora leggere la quantità giusta di produzione del semilavorato
				//infatti il rientro del Sl potrebbe essere registrato in due mandati: 
				//ad esempio: prima 2 PZ e poi altri 2 PZ
				//quindi nella variabile "ldd_quan_arrivata" deve esserci 4 e non 2
				
				
				select quan_prodotta
				into   :ldd_quan_arrivata		
				from   avan_produzione_com
				WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
				 anno_commessa = :li_anno_commessa  AND    
				 num_commessa = :ll_num_commessa  	AND    
				 prog_riga = :ll_prog_riga 			AND    
				 cod_prodotto = :ls_cod_prodotto_fase  AND
				 cod_versione = :ls_cod_versione_fase  AND    
				 cod_reparto = :ls_cod_reparto  		AND    
				 cod_lavorazione = :ls_cod_lavorazione;
				 
				 //la qta di SL da movimentare giusta si ottiene così
				 ldd_quan_arrivata = ldd_quan_arrivata - ldd_quan_prodotta_old
		
				li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
														 ls_cod_tipo_mov_ver_sl, &
														 "N", &
														 ls_cod_prodotto_fase, &
														 ldd_quan_arrivata, &
														 1, &
														 ll_num_commessa, &
														 datetime(today(),time('00:00:00')), &
														 "", &
														 ll_anno_reg_des_mov, &
														 ll_num_reg_des_mov, &
														 ls_cod_deposito_sl[], &
														 ls_cod_ubicazione[], &
														 ls_cod_lotto[], &
														 ld_data_stock[], &
														 ll_prog_stock[], &
														 ls_cod_fornitore[], &
														 ls_cod_cliente[], &
														 ll_anno_registrazione[], &
														 ll_num_registrazione[])
														 
				destroy luo_mag
		
				if li_risposta=-1 then
					g_mb.messagebox("Sep", "Errore in creazione movimenti magazzino." , stopsign!)
					rollback;
					return
				end if
					
				li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)
				
				if li_risposta = -1 then
					g_mb.messagebox("Sep", "Si è verificato un errore in fase di cancellazione destinazioni movimenti." , stopsign!)
					rollback;
					return -1
				end if
				//fine modifica
				//#########################################################
				//********************************************************************************
				
				select 	quan_in_produzione
				into   		:ldd_quan_in_produzione_prog
				from   	det_anag_commesse
				where		cod_azienda = :s_cs_xx.cod_azienda	and 	 
							anno_commessa = :li_anno_commessa	and    
							num_commessa = :ll_num_commessa	and    
							prog_riga = :ll_prog_riga;
			
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Sep", "Errore in ricerca quantità in produzione dalla commessa~r~n" + sqlca.sqlerrtext, stopsign!)
					rollback;
					return
				end if
		
				
				select cod_prodotto,
						 flag_tipo_avanzamento
				into   :ls_cod_prodotto,
						 :ls_flag_tipo_avanzamento
				from   anag_commesse
				where  cod_azienda=:s_cs_xx.cod_azienda
				and 	 anno_commessa=:li_anno_commessa
				and    num_commessa=:ll_num_commessa;
			
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Sep", "Errore in ricerca commessa " + sqlca.sqlerrtext, stopsign!)
					rollback;
					return
				end if
		
				
				s_cs_xx.parametri.parametro_i_1 = li_anno_commessa
				s_cs_xx.parametri.parametro_ul_1 = ll_num_commessa
				s_cs_xx.parametri.parametro_ul_2 = ll_prog_riga
				s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
				s_cs_xx.parametri.parametro_s_5 = ls_cod_versione
			
				//passare la qta di PF da produrre - ld_qta_pf
				s_cs_xx.parametri.parametro_dec4_5 = ld_qta_pf
				window_open(w_rileva_quantita_parziale,0)
				
				if s_cs_xx.parametri.parametro_b_1 = true then	
					g_mb.messagebox("Sep", "Operazione annullata", stopsign!)
					rollback;
					return 
				end if
				
				ldd_quan_prodotta_save = s_cs_xx.parametri.parametro_d_1
				
				update det_anag_commesse
				set    quan_in_produzione=quan_in_produzione - :ldd_quan_prodotta_save,
						 quan_prodotta=quan_prodotta + :ldd_quan_prodotta_save
				where  cod_azienda=:s_cs_xx.cod_azienda
				and 	 anno_commessa=:li_anno_commessa
				and    num_commessa=:ll_num_commessa
				and    prog_riga=:ll_prog_riga;
			
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep", "Errore in aggiornamento quantità della commessa~r~n" + sqlca.sqlerrtext, stopsign!)
					rollback;
					return
				end if
		
			
				select quan_prodotta,
						 quan_in_produzione
				into   :ldd_quan_prodotta,
						 :ldd_quan_in_produzione
				from   anag_commesse
				where  cod_azienda=:s_cs_xx.cod_azienda
				and 	 anno_commessa=:li_anno_commessa
				and    num_commessa=:ll_num_commessa;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Sep", "Errore in ricerca dati commessa~r~n" + sqlca.sqlerrtext, stopsign!)
					rollback;
					return
				end if
				
				ldd_quan_in_produzione = ldd_quan_in_produzione - ldd_quan_prodotta_save
				if ldd_quan_in_produzione < 0 then ldd_quan_in_produzione = 0
				ldd_quan_prodotta = ldd_quan_prodotta + ldd_quan_prodotta_save
			
//				choose case ls_flag_tipo_avanzamento //seleziona il tipo avanzamento
//					case '1' // Assegnazione
//						ls_flag_tipo_avanzamento = '9' // Assegnazione Chiusa
//					case '2' // Attivazione
//						ls_flag_tipo_avanzamento = '8' // Attivazione Chiusa
//				end choose 
			
				update anag_commesse
				set    quan_in_produzione=:ldd_quan_in_produzione,	
						 quan_prodotta=:ldd_quan_prodotta,
						 flag_tipo_avanzamento = :ls_flag_tipo_avanzamento
				where  cod_azienda=:s_cs_xx.cod_azienda
				and 	 anno_commessa=:li_anno_commessa
				and    num_commessa=:ll_num_commessa;
			
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep", "Errore in aggiornamento quantità della commessa~r~n" + sqlca.sqlerrtext, stopsign!)
					rollback;
					return
				end if
				
				li_risposta = f_mov_mag_chiusura_fasi (li_anno_commessa,ll_num_commessa, & 
																	ll_prog_riga,ls_errore )
			
				if li_risposta = -1 then 
					g_mb.messagebox("Sep", "Errore in generazione movimento di chiusura commessa~r~n" + ls_errore + sqlca.sqlerrtext, stopsign!)
					rollback;
					return
				end if
				
				//Donato 05/01/2009 sembra che l'aggiornamento automatico della colonna quan_in_produzione
				//		nella tabella anag_prodotti per il PF non sia gestito: allora metto qui questo script di aggiornamento
				select quan_in_produzione
				into :ldd_quan_in_produzione_anag
				from anag_prodotti
				where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode = 0 then
					if isnull(ldd_quan_in_produzione_anag) then ldd_quan_in_produzione_anag = 0
					
					if ldd_quan_in_produzione_anag < ldd_quan_prodotta_save then
						update anag_prodotti
						set quan_in_produzione = 0
						where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto;
					else
						update anag_prodotti
						set quan_in_produzione = quan_in_produzione - :ldd_quan_prodotta_save
						where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto;			
					end if
				end if
				//fine modifica ---------------------------------------------------------------------------------------------------------
					
				close(parent)
				//###################################
				//###################################
			end if							
		end if
	end if
end if

if ldd_quan_in_produzione = 0 then

	update avan_produzione_com
	set    flag_fine_fase='S'
	where  cod_azienda = :s_cs_xx.cod_azienda   and 	 
			 anno_commessa = :li_anno_commessa	  and    
			 num_commessa = :ll_num_commessa		  and    
			 prog_riga = :ll_prog_riga				  and    
			 cod_prodotto = :ls_cod_prodotto_fase and
			 cod_versione = :ls_cod_versione_fase and    
			 cod_reparto = :ls_cod_reparto		  and    
			 cod_lavorazione = :ls_cod_lavorazione;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Sep", "Errore in aggiornamento flag_fine_fase in avan_produzione_com~r~n" + sqlca.sqlerrtext, stopsign!)
		rollback;
		return
	end if

	
	//**** Individua se viene chiusa l'ultima fase
	
	select 	count(*)
	into   		:ll_num_fasi_aperte
	from   	avan_produzione_com
	where		cod_azienda = :s_cs_xx.cod_azienda	and 	 
				anno_commessa = :li_anno_commessa	and    
				num_commessa = :ll_num_commessa	and    
				prog_riga = :ll_prog_riga						and    
				flag_fine_fase = 'N';
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Sep", "Errore in conteggio fasi aperte~r~n" + sqlca.sqlerrtext, stopsign!)
		rollback;
		return
	end if


	if ll_num_fasi_aperte = 0 then
		
		//#######################################################################
		//***************************************************************************************************
		//Donato 04-06-2008 inserire qui le istruzioni per eseguire lo scarico dei semilavorato
		//infatti se viene chiusa l'ultima fase viene generato il movimento di carico del PF
		//e deve essere anche scaricato il SL
		
		setnull(ls_cod_ubicazione[1])
		setnull(ls_cod_lotto[1])
		setnull(ld_data_stock[1])
		setnull(ll_prog_stock[1])
		setnull(ls_cod_cliente[1])
		setnull(ls_cod_fornitore[1])
			
		if not isnull(ll_anno_reg_sl) and not isnull(ll_num_reg_sl) then
			
			select cod_ubicazione,
					 cod_lotto,
					 data_stock,
					 prog_stock
			into   :ls_cod_ubicazione[1],
					 :ls_cod_lotto[1],
					 :ld_data_stock[1],
					 :ll_prog_stock[1]
			from   mov_magazzino
			where  cod_azienda =:s_cs_xx.cod_azienda
			and    anno_registrazione=:ll_anno_reg_sl
			and	 num_registrazione=:ll_num_reg_sl;
		
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Sep", "Errore in ricerca del movimento di magazzino di carico SL~r~n" + sqlca.sqlerrtext, stopsign!)
				return
			end if
			
		end if

		ll_anno_reg_sl = 0
		ll_num_reg_sl = 0
		
		
		ls_cod_tipo_mov_ver_sl="SMP"
		if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_ver_sl, ls_cod_prodotto_fase, ls_cod_deposito_sl[], & 
									  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
									  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
									  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
		
			g_mb.messagebox("Sep", "Si è verificato un errore in preparazione destinazione movimenti.", stopsign!)
			rollback;
			return 
		
		end if

		if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
											ls_cod_tipo_mov_ver_sl, ls_cod_prodotto_fase) = -1 then
		
			 g_mb.messagebox("Sep", "Si è verificato un errore in fase di verifica destinazioni movimenti magazzino." , stopsign!)
		  rollback;
		  return
		
		end if
		
		luo_mag = create uo_magazzino
		
		//è necessario ora leggere la quantità giusta di produzione del semilavorato
		//infatti il rientro del Sl potrebbe essere registrato in due mandati: 
		//ad esempio: prima 2 PZ e poi altri 2 PZ
		//quindi nella variabile "ldd_quan_arrivata" deve esserci 4 e non 2
		
		
		select quan_prodotta
		into   :ldd_quan_arrivata		
		from   avan_produzione_com
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
		 anno_commessa = :li_anno_commessa  AND    
		 num_commessa = :ll_num_commessa  	AND    
		 prog_riga = :ll_prog_riga 			AND    
		 cod_prodotto = :ls_cod_prodotto_fase  AND
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  		AND    
		 cod_lavorazione = :ls_cod_lavorazione;
		 
		//la qta di SL da movimentare giusta si ottiene così
		ldd_quan_arrivata = ldd_quan_arrivata - ldd_quan_prodotta_old		 
		
		/*
		//è necessario ora leggere la quantità giusta di produzione del semilavorato
		//infatti il rientro del Sl potrebbe essere registrato in due mandati: 
		//ad esempio: prima 2 PZ e poi altri 2 PZ
		//quindi nella variabile "ldd_quan_arrivata" deve esserci 4 e non 2
		select quan_prodotta
		into   :ldd_quan_arrivata		
		from   avan_produzione_com
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
		 anno_commessa = :li_anno_commessa  AND    
		 num_commessa = :ll_num_commessa  	AND    
		 prog_riga = :ll_prog_riga 			AND    
		 cod_prodotto = :ls_cod_prodotto_fase  AND
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  		AND    
		 cod_lavorazione = :ls_cod_lavorazione;
		 		 
		 //---------------
		 //la salvo
		 ldd_quan_prodotta_old3 = ldd_quan_arrivata
		 
		  select cod_prodotto
		into   :ls_cod_prodotto
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
		 
		 //vedo quanta ne è già stata utilizzata in mp_com_stock_det_orari
		 select sum(quan_utilizzata)
		 into :ldd_quan_arrivata
		 from mp_com_stock_det_orari
		 WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
		 anno_commessa = :li_anno_commessa  AND    
		 num_commessa = :ll_num_commessa  	AND    
		 prog_riga = :ll_prog_riga 			AND    
		 cod_prodotto = :ls_cod_prodotto_fase  AND
		 cod_prodotto_fase = :ls_cod_prodotto and
		 cod_reparto = :ls_cod_reparto  		AND    
		 cod_lavorazione = :ls_cod_lavorazione;
		 
		 if isnull(ldd_quan_arrivata) then ldd_quan_arrivata=0
		 
		 //da movimentare abbiamo
		 //in ldd_quan_prodotta_old3 quella che risulta prodotta in totale di SL
		 //		(comprende anche quella che è stata appena fatta rientrare)
		 //in ldd_quan_arrivata quella che risulta movimentata
		 
		 //la qta di SL da movimentare giusta si ottiene così
		 ldd_quan_arrivata = ldd_quan_prodotta_old3 - ldd_quan_arrivata
		 */
		 
		li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
												 ls_cod_tipo_mov_ver_sl, &
												 "N", &
												 ls_cod_prodotto_fase, &
												 ldd_quan_arrivata, &
												 1, &
												 ll_num_commessa, &
												 datetime(today(),time('00:00:00')), &
												 "", &
												 ll_anno_reg_des_mov, &
												 ll_num_reg_des_mov, &
												 ls_cod_deposito_sl[], &
												 ls_cod_ubicazione[], &
												 ls_cod_lotto[], &
												 ld_data_stock[], &
												 ll_prog_stock[], &
												 ls_cod_fornitore[], &
												 ls_cod_cliente[], &
												 ll_anno_registrazione[], &
												 ll_num_registrazione[])
												 
		destroy luo_mag

		if li_risposta=-1 then
			g_mb.messagebox("Sep", "Errore in creazione movimenti magazzino." , stopsign!)
			rollback;
			return
		end if
			
		li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)
		
		if li_risposta = -1 then
			g_mb.messagebox("Sep", "Si è verificato un errore in fase di cancellazione destinazioni movimenti." , stopsign!)
			rollback;
			return -1
		end if
		//fine modifica
		//#########################################################
		//********************************************************************************
		
		select 	quan_in_produzione
		into   		:ldd_quan_in_produzione_prog
		from   	det_anag_commesse
		where		cod_azienda = :s_cs_xx.cod_azienda	and 	 
					anno_commessa = :li_anno_commessa	and    
					num_commessa = :ll_num_commessa	and    
					prog_riga = :ll_prog_riga;
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Sep", "Errore in ricerca quantità in produzione dalla commessa~r~n" + sqlca.sqlerrtext, stopsign!)
			rollback;
			return
		end if

		
		select cod_prodotto,
				 flag_tipo_avanzamento
		into   :ls_cod_prodotto,
				 :ls_flag_tipo_avanzamento
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Sep", "Errore in ricerca commessa " + sqlca.sqlerrtext, stopsign!)
 			rollback;
			return
		end if

		
		s_cs_xx.parametri.parametro_i_1 = li_anno_commessa
		s_cs_xx.parametri.parametro_ul_1 = ll_num_commessa
		s_cs_xx.parametri.parametro_ul_2 = ll_prog_riga
		s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
		s_cs_xx.parametri.parametro_s_5 = ls_cod_versione
	
		window_open(w_rileva_quantita,0)
		
		if s_cs_xx.parametri.parametro_b_1 = true then	
			g_mb.messagebox("Sep", "Operazione annullata", stopsign!)
		   rollback;
			return 
		end if
		
		//Donato 05/01/2009 la quantita prodotta di PF deve essere incrementale
		//va bene mettere a 0 quella in produzione perchè siamo in chiusura di fase
		//ma quella prodotta, poichè potrebbe essere incrementale 
		//(più carichi parziali - specifica report_produzione del 27/10/11/08) va incrementata
		/*
		update det_anag_commesse
		set    quan_in_produzione=0,
				 quan_prodotta=:s_cs_xx.parametri.parametro_d_1
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga;
		*/
		ldd_quan_prodotta_save = s_cs_xx.parametri.parametro_d_1
		
		update det_anag_commesse
		set    quan_in_produzione=0,
				 quan_prodotta=quan_prodotta + :ldd_quan_prodotta_save
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga;
		//fine modifica --------------------------------------------------------------------------------
 	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep", "Errore in aggiornamento quantità della commessa~r~n" + sqlca.sqlerrtext, stopsign!)
		   rollback;
			return
		end if

		select quan_prodotta,
				 quan_in_produzione
		into   :ldd_quan_prodotta,
				 :ldd_quan_in_produzione
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Sep", "Errore in ricerca dati commessa~r~n" + sqlca.sqlerrtext, stopsign!)
		   rollback;
			return
		end if
		
		ldd_quan_in_produzione = ldd_quan_in_produzione - ldd_quan_prodotta_save
		if ldd_quan_in_produzione < 0 then ldd_quan_in_produzione = 0
		ldd_quan_prodotta = ldd_quan_prodotta + ldd_quan_prodotta_save
	
		choose case ls_flag_tipo_avanzamento //seleziona il tipo avanzamento
			case '1' // Assegnazione
				ls_flag_tipo_avanzamento = '9' // Assegnazione Chiusa
			case '2' // Attivazione
				ls_flag_tipo_avanzamento = '8' // Attivazione Chiusa
		end choose 
	
		update anag_commesse
		set    quan_in_produzione=:ldd_quan_in_produzione,	
				 quan_prodotta=:ldd_quan_prodotta,
				 flag_tipo_avanzamento = :ls_flag_tipo_avanzamento
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep", "Errore in aggiornamento quantità della commessa~r~n" + sqlca.sqlerrtext, stopsign!)
		   rollback;
			return
		end if
		
		li_risposta = f_mov_mag_chiusura_fasi (li_anno_commessa,ll_num_commessa, & 
															ll_prog_riga,ls_errore )
	
		if li_risposta = -1 then 
			g_mb.messagebox("Sep", "Errore in generazione movimento di chiusura commessa~r~n" + ls_errore + sqlca.sqlerrtext, stopsign!)
		   rollback;
			return
		end if
		
		//Donato 05/01/2009 sembra che l'aggiornamento automatico della colonna quan_in_produzione
		//		nella tabella anag_prodotti per il PF non sia gestito: allora metto qui questo script di aggiornamento
		select quan_in_produzione
		into :ldd_quan_in_produzione_anag
		from anag_prodotti
		where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto;
		
		if sqlca.sqlcode = 0 then
			if isnull(ldd_quan_in_produzione_anag) then ldd_quan_in_produzione_anag = 0
			
			if ldd_quan_in_produzione_anag < ldd_quan_prodotta_save then
				update anag_prodotti
				set quan_in_produzione = 0
				where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto;
			else
				update anag_prodotti
				set quan_in_produzione = quan_in_produzione - :ldd_quan_prodotta_save
				where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto;			
			end if
		end if
		//fine modifica ---------------------------------------------------------------------------------------------------------
			
		close(parent)
	end if

end if
//FINE CHIUSURA FASE
//******************************

//	***	Michela 29/06/2007
//	*** se la fase superiore è esterna, oltre a fare quello che fa di solito, deve anche fare la generazione all'altro terzista solo del semilavorato

if s_cs_xx.parametri.parametro_b_2 and not isnull(s_cs_xx.parametri.parametro_b_2) then
	
	//	*** mi devo prendere i dati della fase di lavorazione precedente

	ls_cod_fornitore_terzista = dw_scelta_fornitore.getitemstring( dw_scelta_fornitore.getrow(), "cod_fornitore_2")
	
//	s_cs_xx.parametri.parametro_s_10 = is_cod_prodotto_prec
//	s_cs_xx.parametri.parametro_s_11 = is_cod_versione_prec
//	s_cs_xx.parametri.parametro_s_12 = is_cod_reparto_prec
//	s_cs_xx.parametri.parametro_s_13 = is_cod_lavorazione_prec
	
	if wf_scarico_nuovo_terzista( li_anno_commessa, ll_num_commessa, ll_prog_riga, s_cs_xx.parametri.parametro_s_10, s_cs_xx.parametri.parametro_s_11, s_cs_xx.parametri.parametro_s_12, s_cs_xx.parametri.parametro_s_13, ls_cod_prodotto_fase, ls_cod_versione_fase, ldd_quan_arrivata) < 0 then
		rollback;
		return -1
	end if
	
//	if wf_carica_sl_terzista( ls_cod_prodotto_fase, ls_cod_fornitore_terzista, ls_cod_tipo_det_ven_sl, ls_cod_deposito_semilavorato, ls_cod_ubicazione_semilavorato, ls_cod_lotto_semilavorato, ldt_data_stock_semilavorato, ll_prog_stock_semilavorato, ldd_quan_arrivata) <0 then
//		rollback;
//		return -1
//	end if
	
	if wf_carica_mag_terzista( ls_cod_fornitore_terzista, ls_cod_tipo_det_ven_sl, ls_cod_prodotto_fase, ls_cod_deposito_semilavorato, ls_cod_ubicazione_semilavorato, ls_cod_lotto_semilavorato, ldt_data_stock_semilavorato, ll_prog_stock_semilavorato, ldd_quan_arrivata) < 0 then
		rollback;
		return -1		
	end if
end if

g_mb.messagebox("Sep","Carico eseguito con successo",information!)
commit;
return

end event

type cbx_bolla_ingresso from checkbox within w_carica_bolle_terzisti
integer x = 2875
integer y = 2048
integer width = 654
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Genera Bolla Ingresso"
boolean lefttext = true
end type

type st_prodotto_ingresso from statictext within w_carica_bolle_terzisti
integer x = 503
integer y = 20
integer width = 3497
integer height = 104
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type em_quan_arrivata from editmask within w_carica_bolle_terzisti
integer x = 1280
integer y = 2036
integer width = 379
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "##,###,###.0000"
string displaydata = ""
end type

event losefocus;string ls_errore,ls_cod_versione
double ldd_quan_in_produzione
integer li_risposta	

ldd_quan_in_produzione = double (em_quan_arrivata.text)

//Donato 06-11-2008 Anomalia 2008/155
//caricare la versione del SL che sta rientrando dal terzista non da quella della commessa
dw_lista_prodotti_scarico.reset()
dw_scelta_stock_fornitore.reset()
li_risposta = f_genera_lista_prodotti(il_anno_commessa,il_num_commessa,il_prog_riga,is_cod_prodotto_fase,is_cod_versione_fase,ldd_quan_in_produzione,1,ls_errore)

//vecchio codice commentato
/*
select cod_versione
into   :ls_cod_versione
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:il_anno_commessa
and    num_commessa=:il_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 
end if

dw_lista_prodotti_scarico.reset()
dw_scelta_stock_fornitore.reset()
li_risposta = f_genera_lista_prodotti(il_anno_commessa,il_num_commessa,il_prog_riga,is_cod_prodotto_fase,ls_cod_versione,ldd_quan_in_produzione,1,ls_errore)
*/
//fine modifica ------------------------------

if li_risposta <0 then
	g_mb.messagebox("Sep",ls_errore,stopsign!)
	return
end if

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_det_prod_bolle_in_com.resetupdate()
dw_lista_prod_bolle_in_com.resetupdate()
dw_lista_prodotti_scarico.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock_fornitore.resetupdate()
dw_lista_prodotti_scarico.Reset_DW_Modified(c_ResetChildren)

dw_lista_prodotti_scarico.change_dw_current()
parent.triggerevent("pc_retrieve")
dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_det_prod_bolle_in_com.resetupdate()
dw_lista_prod_bolle_in_com.resetupdate()
dw_lista_prodotti_scarico.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock_fornitore.resetupdate()
wf_imposta_stock()
dw_scelta_stock_fornitore.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_det_prod_bolle_in_com.resetupdate()
dw_lista_prod_bolle_in_com.resetupdate()
dw_lista_prodotti_scarico.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock_fornitore.resetupdate()

end event

type em_data_ingresso from editmask within w_carica_bolle_terzisti
integer x = 379
integer y = 2036
integer width = 430
integer height = 84
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

type st_1 from statictext within w_carica_bolle_terzisti
integer x = 14
integer y = 2048
integer width = 343
integer height = 68
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Data Arrivo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_carica_bolle_terzisti
integer x = 901
integer y = 2044
integer width = 366
integer height = 68
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Qtà.Arrivata:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cbx_acc_mat from checkbox within w_carica_bolle_terzisti
integer x = 1870
integer y = 2048
integer width = 837
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Genera Registrazione Qualità"
boolean lefttext = true
end type

type cb_togli from commandbutton within w_carica_bolle_terzisti
integer x = 87
integer y = 1300
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Togli"
end type

event clicked;dw_elenco_prod_scarico_fornitore.deleterow(dw_elenco_prod_scarico_fornitore.getrow())
end event

type dw_det_prod_bolle_in_com from uo_cs_xx_dw within w_carica_bolle_terzisti
integer x = 87
integer y = 840
integer width = 3109
integer height = 540
integer taborder = 140
string dataobject = "d_carica_bolla_terz_info"
borderstyle borderstyle = styleraised!
end type

type st_3 from statictext within w_carica_bolle_terzisti
integer x = 23
integer y = 36
integer width = 471
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Prodotto Arrivato:"
boolean focusrectangle = false
end type

type dw_elenco_prod_scarico_fornitore from uo_cs_xx_dw within w_carica_bolle_terzisti
integer x = 87
integer y = 300
integer width = 3131
integer height = 980
integer taborder = 10
string dataobject = "d_carica_bolla_terz_elenco_prod_scarico"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type dw_folder from u_folder within w_carica_bolle_terzisti
integer x = 18
integer y = 160
integer width = 3986
integer height = 1820
integer taborder = 80
end type

type dw_lista_prod_bolle_in_com from uo_cs_xx_dw within w_carica_bolle_terzisti
event pcd_retrieve pbm_custom60
integer x = 87
integer y = 260
integer width = 3109
integer height = 540
integer taborder = 100
string dataobject = "d_carica_bolla_terz_elenco_entrate"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,il_anno_commessa, & 
						 il_num_commessa,il_prog_riga,is_cod_prodotto_fase, is_cod_versione_fase, & 
						 is_cod_reparto,is_cod_lavorazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_lista_prodotti_scarico from uo_cs_xx_dw within w_carica_bolle_terzisti
integer x = 50
integer y = 616
integer width = 3493
integer height = 540
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_carica_bolla_terz_elenco_prodotti_scar"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event clicked;call super::clicked;if i_extendmode then
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	
	
	
	
	wf_imposta_stock()
	
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()

end if
end event

type dw_scelta_stock_fornitore from uo_cs_xx_dw within w_carica_bolle_terzisti
integer x = 50
integer y = 1260
integer width = 3488
integer height = 584
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_carica_bolla_terz_elenco_stock"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type cb_aggiungi from commandbutton within w_carica_bolle_terzisti
integer x = 3557
integer y = 1272
integer width = 366
integer height = 80
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi"
end type

event clicked;string   ls_cod_prodotto_scarico,ls_cod_ubicazione,ls_cod_lotto,ls_cod_deposito
double   ldd_quan_prelevata,ldd_quan_disponibile
long     ll_prog_stock,ll_num_righe,ll_t
datetime ldt_data_stock

for ll_t = 1 to dw_scelta_stock_fornitore.rowcount()
	dw_scelta_stock_fornitore.setrow(ll_t)
	ls_cod_prodotto_scarico = dw_scelta_stock_fornitore.getitemstring(ll_t,"cod_prodotto")
	ls_cod_deposito = dw_scelta_stock_fornitore.getitemstring(ll_t,"cod_deposito")
	ls_cod_ubicazione = dw_scelta_stock_fornitore.getitemstring(ll_t,"cod_ubicazione")
	ls_cod_lotto = dw_scelta_stock_fornitore.getitemstring(ll_t,"cod_lotto")
	ldt_data_stock = dw_scelta_stock_fornitore.getitemdatetime(ll_t,"data_stock")
	ll_prog_stock = dw_scelta_stock_fornitore.getitemnumber(ll_t,"prog_stock")
	dw_scelta_stock_fornitore.setcolumn("quan_prelevata")	
	ldd_quan_prelevata =	double(dw_scelta_stock_fornitore.gettext())
	ldd_quan_disponibile = 	dw_scelta_stock_fornitore.getitemnumber(ll_t,"quan_disponibile")
	
	if ldd_quan_prelevata > 0 then
		ll_num_righe=dw_elenco_prod_scarico_fornitore.rowcount()
	
		if ll_num_righe=0 then
			ll_num_righe=1
		else
			ll_num_righe++
		end if
	
		dw_elenco_prod_scarico_fornitore.insertrow(ll_num_righe)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"cod_prodotto",ls_cod_prodotto_scarico)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"cod_deposito",ls_cod_deposito)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"cod_ubicazione",ls_cod_ubicazione)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"cod_lotto",ls_cod_lotto)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"data_stock",ldt_data_stock)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"prog_stock",ll_prog_stock)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"quan_disponibile",ldd_quan_disponibile)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"quan_prelevata",ldd_quan_prelevata)
//		dw_scelta_stock_fornitore.deleterow(ll_t)
		dw_scelta_stock_fornitore.resetupdate()
		
	end if

next

dw_elenco_prod_scarico_fornitore.Reset_DW_Modified(c_ResetChildren)
dw_elenco_prod_scarico_fornitore.settaborder("quan_prelevata",10)

dw_elenco_prod_scarico_fornitore.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type st_stquan_impegnata from statictext within w_carica_bolle_terzisti
integer x = 448
integer y = 1868
integer width = 535
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Quantità impegnata:"
boolean focusrectangle = false
end type

type st_quan_impegnata from statictext within w_carica_bolle_terzisti
integer x = 997
integer y = 1868
integer width = 379
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "00"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_elenco_fasi_esterne_attivazione from uo_cs_xx_dw within w_carica_bolle_terzisti
integer x = 46
integer y = 260
integer width = 3502
integer height = 256
integer taborder = 90
string dataobject = "d_carica_bolla_terz_elenco_fasi_esterne"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event clicked;call super::clicked;if i_extendmode then
	string ls_errore,ls_cod_versione,ls_des_prodotto
	double ldd_quan_in_produzione
	integer li_risposta
	
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	
	dw_elenco_prod_scarico_fornitore.reset()

	il_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "anno_commessa")
	il_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "num_commessa")
	il_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "prog_riga")
	is_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(), "cod_prodotto")
	is_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(), "cod_versione")
	ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")
	is_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
	is_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		

	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:is_cod_prodotto_fase;
	
	st_prodotto_ingresso.text = is_cod_prodotto_fase + " / " + ls_des_prodotto
	
	dw_det_prod_bolle_in_com.reset()
	dw_lista_prodotti_scarico.reset()
	dw_scelta_stock_fornitore.reset()
	
	em_quan_arrivata.text = string(ldd_quan_in_produzione)
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 
	end if

	li_risposta = f_genera_lista_prodotti(il_anno_commessa,il_num_commessa,il_prog_riga, is_cod_prodotto_fase, is_cod_versione_fase, ldd_quan_in_produzione,1,ls_errore)
	
	if li_risposta <0 then
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		return
	end if
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	dw_lista_prodotti_scarico.Reset_DW_Modified(c_ResetChildren)
	
//	dw_lista_prodotti_spedizione.settaborder("quan_spedizione",10)
	dw_lista_prodotti_scarico.change_dw_current()
	parent.triggerevent("pc_retrieve")
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	wf_imposta_stock()
	dw_scelta_stock_fornitore.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	dw_lista_prod_bolle_in_com.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()

//	dw_prod_uscita_bol_spedite.change_dw_current()
//	parent.triggerevent("pc_retrieve")
//
//	wf_imposta_stock()
//	dw_scelta_stock.Change_DW_Current( )
//	parent.triggerevent("pc_retrieve")
//	dw_elenco_prod_uscita_bol.Change_DW_Current( )
//	parent.triggerevent("pc_retrieve")
//	dw_elenco_fasi_esterne_attivazione.resetupdate()
//	dw_elenco_prod_uscita_bol.resetupdate()
//	dw_lista_prodotti_spedizione.resetupdate()
//	dw_prod_uscita_bol_spedite.resetupdate()
//	dw_scelta_fornitore.resetupdate()
//	dw_scelta_stock.resetupdate()
end if
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto, ls_cod_versione, ls_cod_reparto, ls_cod_lavorazione
LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")
ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_cod_versione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_versione")
ls_cod_reparto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_reparto")
ls_cod_lavorazione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lavorazione")

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   ll_anno_commessa,ll_num_commessa,ll_prog_riga, &
						 ls_cod_prodotto, ls_cod_versione, &
						 ls_cod_reparto, ls_cod_lavorazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

this.postevent("clicked")
end event

type dw_scelta_fornitore from uo_cs_xx_dw within w_carica_bolle_terzisti
integer x = 622
integer y = 528
integer width = 2263
integer height = 400
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_carica_bolla_terz_ricerca_terzista"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	wf_imposta_stock()
	
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()

end if
end event

event pcd_new;call super::pcd_new;if s_cs_xx.parametri.parametro_b_2 and not isnull(s_cs_xx.parametri.parametro_b_2) then
	setitem( getrow(), "flag_visualizza", "S")
end if
end event

event clicked;call super::clicked;choose case dwo.name
		
	case "b_fornitore_2"
		
		guo_ricerca.uof_ricerca_fornitore(dw_scelta_fornitore,"cod_fornitore_2")
		
	case "b_scarica"
		
		//wf_scarico_nuovo_terzista()
		
end choose
end event

event getfocus;call super::getfocus;dw_scelta_fornitore.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore_1"
		guo_ricerca.uof_ricerca_fornitore(dw_scelta_fornitore,"cod_fornitore")
	case "b_ricerca_fornitore_2"
		guo_ricerca.uof_ricerca_fornitore(dw_scelta_fornitore,"cod_fornitore_2")
end choose
end event

