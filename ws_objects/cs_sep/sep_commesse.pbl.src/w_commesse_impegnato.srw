﻿$PBExportHeader$w_commesse_impegnato.srw
forward
global type w_commesse_impegnato from w_cs_xx_principale
end type
type st_giacenze from statictext within w_commesse_impegnato
end type
type hpb_1 from hprogressbar within w_commesse_impegnato
end type
type dw_commesse_impegnato_giacenze from datawindow within w_commesse_impegnato
end type
type cb_giacenze from commandbutton within w_commesse_impegnato
end type
type cb_aggiorna_fabb from commandbutton within w_commesse_impegnato
end type
type st_1 from statictext within w_commesse_impegnato
end type
type dw_commesse_impegnato from uo_cs_xx_dw within w_commesse_impegnato
end type
end forward

global type w_commesse_impegnato from w_cs_xx_principale
integer width = 4160
integer height = 2808
string title = "Ordini di Acquisto Commessa"
boolean maxbox = false
boolean resizable = false
st_giacenze st_giacenze
hpb_1 hpb_1
dw_commesse_impegnato_giacenze dw_commesse_impegnato_giacenze
cb_giacenze cb_giacenze
cb_aggiorna_fabb cb_aggiorna_fabb
st_1 st_1
dw_commesse_impegnato dw_commesse_impegnato
end type
global w_commesse_impegnato w_commesse_impegnato

type variables
long il_anno_commessa, il_num_commessa
end variables

forward prototypes
public function integer wf_giacenze (string as_cod_deposito, string as_cod_prodotto, ref decimal ad_giacenza, ref string as_errore)
public function integer wf_giacenze (string as_cod_deposito, ref string as_errore)
end prototypes

public function integer wf_giacenze (string as_cod_deposito, string as_cod_prodotto, ref decimal ad_giacenza, ref string as_errore);


uo_magazzino luo_magazzino

string ls_where, ls_error, ls_chiave[]
datetime ldt_data_a
dec{4} ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_giacenza
integer li_return

ldt_data_a = datetime(today(), 23:59:59)

ls_where = "and cod_deposito=~~'"+as_cod_deposito+"~~'"

luo_magazzino = CREATE uo_magazzino
		
li_return = luo_magazzino.uof_saldo_prod_date_decimal(as_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "D", &
																			ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

destroy luo_magazzino

if li_return <0 then
	as_errore = "Errore Inventario Magazzino: prodotto " + as_cod_prodotto + " : " + ls_error
	return -1
end if

//prendo solo il primo (ce ne sarà uno solo perchè gli ho passato il flitro per deposito fornitore)
if upperbound(ld_giacenza_stock[]) > 0 then
	ad_giacenza = ld_giacenza_stock[1]
end if

if isnull(ad_giacenza) then ad_giacenza = 0

return 1




return 0
end function

public function integer wf_giacenze (string as_cod_deposito, ref string as_errore);long				ll_tot, ll_index, ll_new
string				ls_cod_prodotto, ls_des_prodotto
dec{4}			ld_giacenza, ld_impegnato
integer			li_ret

dw_commesse_impegnato_giacenze.reset()
ll_tot = dw_commesse_impegnato.rowcount()

hpb_1.maxposition = ll_tot

for ll_index = 1 to ll_tot
	Yield()
	hpb_1.stepit( )
	ls_cod_prodotto = dw_commesse_impegnato.getitemstring(ll_index, "cod_prodotto")
	ls_des_prodotto = dw_commesse_impegnato.getitemstring(ll_index, "anag_prodotti_des_prodotto")
	ld_impegnato = dw_commesse_impegnato.getitemdecimal(ll_index, "quan_impegnata_attuale")
	
	st_giacenze.text = "Calcolo giacenza " + ls_cod_prodotto + " nel deposito " + as_cod_deposito + " in corso ..."
	
	li_ret = wf_giacenze(as_cod_deposito, ls_cod_prodotto, ld_giacenza, as_errore)
	
	if li_ret < 0 then
		return -1
	end if
	
	ll_new = dw_commesse_impegnato_giacenze.insertrow(0)
	dw_commesse_impegnato_giacenze.setitem(ll_new, "cod_prodotto", ls_cod_prodotto)
	dw_commesse_impegnato_giacenze.setitem(ll_new, "des_prodotto", ls_des_prodotto)
	dw_commesse_impegnato_giacenze.setitem(ll_new, "quan_impegnata", ld_impegnato)
	dw_commesse_impegnato_giacenze.setitem(ll_new, "quan_giacenza", ld_giacenza)
	dw_commesse_impegnato_giacenze.setitem(ll_new, "cod_deposito", as_cod_deposito)
	
next


return 0
end function

on w_commesse_impegnato.create
int iCurrent
call super::create
this.st_giacenze=create st_giacenze
this.hpb_1=create hpb_1
this.dw_commesse_impegnato_giacenze=create dw_commesse_impegnato_giacenze
this.cb_giacenze=create cb_giacenze
this.cb_aggiorna_fabb=create cb_aggiorna_fabb
this.st_1=create st_1
this.dw_commesse_impegnato=create dw_commesse_impegnato
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_giacenze
this.Control[iCurrent+2]=this.hpb_1
this.Control[iCurrent+3]=this.dw_commesse_impegnato_giacenze
this.Control[iCurrent+4]=this.cb_giacenze
this.Control[iCurrent+5]=this.cb_aggiorna_fabb
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.dw_commesse_impegnato
end on

on w_commesse_impegnato.destroy
call super::destroy
destroy(this.st_giacenze)
destroy(this.hpb_1)
destroy(this.dw_commesse_impegnato_giacenze)
destroy(this.cb_giacenze)
destroy(this.cb_aggiorna_fabb)
destroy(this.st_1)
destroy(this.dw_commesse_impegnato)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)


//Donato 25/07/2008 ---------------------------------------------------------------------------------------------
//consenti alla dw l'operazione NUOVO
dw_commesse_impegnato.set_dw_options(sqlca, &
                         i_openparm, &                         
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect)
//dw_commesse_impegnato.set_dw_options(sqlca, &
//                         i_openparm, &
//                         c_nonew + &
//                         c_disableCC, &
//                         c_noresizedw + &
//                         c_nohighlightselected + &
//                         c_nocursorrowpointer +&
//                         c_nocursorrowfocusrect)

// fine modifica -------------------------------------------------------------------------------------------------------

iuo_dw_main = dw_commesse_impegnato


end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_commesse_impegnato,"cod_reparto",sqlca,&
//                 "anag_reparti","cod_reparto","cod_reparto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//			




f_PO_LoadDDDW_DW(dw_commesse_impegnato_giacenze,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco<>'S'")
end event

type st_giacenze from statictext within w_commesse_impegnato
integer x = 1746
integer y = 1460
integer width = 1787
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Pronto!"
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_commesse_impegnato
integer x = 23
integer y = 1452
integer width = 1687
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type dw_commesse_impegnato_giacenze from datawindow within w_commesse_impegnato
integer x = 23
integer y = 1532
integer width = 4091
integer height = 1172
integer taborder = 30
string title = "none"
string dataobject = "d_commesse_impegnato_giacenza"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;integer				li_ret
string					ls_errore, ls_cod_prodotto
dec{4}				ld_giacenza


if row>0 then
else
	return
end if

choose case dwo.name
	case "cod_deposito"
		if data<>"" then
			setpointer(Hourglass!)
			ls_cod_prodotto = dw_commesse_impegnato_giacenze.getitemstring(row, "cod_prodotto")
			st_giacenze.text = "Calcolo giacenza " + ls_cod_prodotto + " nel deposito " + data + " in corso ..."
			hpb_1.maxposition = 1
			
			li_ret = wf_giacenze(data, ls_cod_prodotto, ld_giacenza, ls_errore)
			hpb_1.stepit()
			
			if li_ret < 0 then
				setpointer(Arrow!)
				st_giacenze.text = "Errore!"
				hpb_1.position = 0
				g_mb.error(ls_errore)
				return
			end if
			setitem(row, "quan_giacenza", ld_giacenza)
			st_giacenze.text = "Pronto!"
			setpointer(Arrow!)
			
		else
			g_mb.warning("Devi indicare un deposito!")
			return 1
		end if
		
end choose
end event

type cb_giacenze from commandbutton within w_commesse_impegnato
integer x = 3575
integer y = 1440
integer width = 539
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Vedi Giacenze"
end type

event clicked;string				ls_cod_deposito, ls_errore
integer			li_ret, li_anno_commessa
long				ll_num_commessa


if dw_commesse_impegnato.rowcount() > 0 then
	li_anno_commessa = dw_commesse_impegnato.getitemnumber(1, "anno_commessa")
	ll_num_commessa = dw_commesse_impegnato.getitemnumber(1, "num_commessa")
else
	g_mb.warning("nessuna riga da elaborare nell'impegnato di questa commessa!")
	return
end if


if not g_mb.confirm("Visualizzare le giacenze degli articoli impegnati in questa commessa?") then return

setpointer(Hourglass!)

hpb_1.position = 0
st_giacenze.text = ""

select cod_deposito_prelievo
into :ls_cod_deposito
from anag_commesse
where	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:li_anno_commessa and
			num_commessa=:ll_num_commessa;

li_ret = wf_giacenze(ls_cod_deposito, ls_errore)

if li_ret < 0 then
	setpointer(Arrow!)
	st_giacenze.text = "Errore!"
	g_mb.error(ls_errore)
	return
end if

st_giacenze.text = "Pronto!"
setpointer(Arrow!)
end event

type cb_aggiorna_fabb from commandbutton within w_commesse_impegnato
integer x = 3575
integer y = 56
integer width = 539
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;string		ls_errore
long 		ll_anno_commessa, ll_num_commessa
uo_mrp 	luo_mrp

luo_mrp = create uo_mrp

ll_anno_commessa = dw_commesse_impegnato.i_parentdw.getitemnumber(dw_commesse_impegnato.i_parentdw.i_selectedrows[1],"anno_commessa")
ll_num_commessa = dw_commesse_impegnato.i_parentdw.getitemnumber(dw_commesse_impegnato.i_parentdw.i_selectedrows[1],"num_commessa")
if luo_mrp.uof_ricacolo_data_fabbisogno_commessa(ll_anno_commessa, ll_num_commessa, ref ls_errore) < 0 then
	g_mb.error(ls_errore)
	rollback;
end if

/*
//Donato 16/12/2008 aggiunto nuovo pulsante per il
//ricalcolo della data di fabbisogno (Specifica Unifast Report_produzione - F1)

long ll_anno_commessa, ll_num_commessa
long ll_cur_row, ll_rows
datetime ldt_data_consegna, ldt_data_fabb
string ls_cod_prodotto
decimal ldc_lead_time
integer li_lead_time

ll_anno_commessa = dw_commesse_impegnato.i_parentdw.getitemnumber(dw_commesse_impegnato.i_parentdw.i_selectedrows[1],"anno_commessa")
ll_num_commessa = dw_commesse_impegnato.i_parentdw.getitemnumber(dw_commesse_impegnato.i_parentdw.i_selectedrows[1],"num_commessa")

if g_mb.messagebox("APICE", "Aggiornare la Data di Fabbisogno di tutte le materie prime/semilavorati "+&
				"della commessa n° "+string(ll_anno_commessa)+"/"+string(ll_num_commessa)+"?", &
				Question!, YesNo!, 2) = 1 then
	
	select data_consegna
	into :ldt_data_consegna
	from anag_commesse
	where cod_azienda=:s_cs_xx.cod_azienda
		and anno_commessa=:ll_anno_commessa and num_commessa=:ll_num_commessa
	;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE", "Errore durante la lettura della data consegna "+&
				"della commessa n° "+string(ll_anno_commessa)+"/"+string(ll_num_commessa)+"!", &
				StopSign!)
		return
	end if
	
	ll_rows = dw_commesse_impegnato.rowcount()
	for ll_cur_row = 1 to ll_rows
		ls_cod_prodotto = dw_commesse_impegnato.getitemstring(ll_cur_row, "cod_prodotto")
		setnull(ldc_lead_time)
		
		select lead_time
		into :ldc_lead_time
		from impegno_mat_prime_commessa
		where cod_azienda=:s_cs_xx.cod_azienda
			and anno_commessa=:ll_anno_commessa and num_commessa=:ll_num_commessa
			and cod_prodotto=:ls_cod_prodotto
		;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore durante la lettura del lead time "+&
					"del prodotto "+ls_cod_prodotto+"! Operazione Annullata!", &
					StopSign!)
			rollback;
			return
		end if
		
		li_lead_time = integer(ldc_lead_time)
		ldt_data_fabb = datetime(relativedate(date(ldt_data_consegna), - li_lead_time), 00:00:00)
		
		if isnull(ldt_data_fabb) then
			g_mb.messagebox("APICE", "Errore durante il calcolo della nuova data di fabbisogno "+&
					"del prodotto "+ls_cod_prodotto+"! Operazione Annullata!", &
					StopSign!)
			rollback;
			return
		end if
		
		//esegui l'update
		update impegno_mat_prime_commessa
		set data_fabbisogno=:ldt_data_fabb
		where cod_azienda=:s_cs_xx.cod_azienda
			and anno_commessa=:ll_anno_commessa and num_commessa=:ll_num_commessa
			and cod_prodotto=:ls_cod_prodotto
		;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore durante l'aggiornamento della nuova data di fabbisogno "+&
					"del prodotto "+ls_cod_prodotto+"! Operazione Annullata!", &
					StopSign!)
			rollback;
			return
		end if
		
	next
	
	commit;
	dw_commesse_impegnato.postevent("pcd_retrieve")
end if
*/
end event

type st_1 from statictext within w_commesse_impegnato
integer x = 32
integer y = 56
integer width = 3483
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SHIFT - F1 = Ricerca Prodotto"
boolean focusrectangle = false
end type

type dw_commesse_impegnato from uo_cs_xx_dw within w_commesse_impegnato
integer x = 23
integer y = 148
integer width = 4091
integer height = 1212
integer taborder = 10
string dataobject = "d_commesse_impegnato"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_ret, ll_anno_commessa, ll_num_commessa

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_commessa")

parent.title = "Impegnato Commessa " + string(ll_anno_commessa) + "/" + string(ll_num_commessa)

ll_ret = retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa)

if ll_ret < 0 then
	PCCA.Error = c_Fatal
end if



end event

event ue_key;call super::ue_key;choose case this.getcolumnname()
	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_commesse_impegnato,"cod_prodotto")	
		end if
	case "anag_prodotti_des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_commesse_impegnato,"cod_prodotto")		
		end if
				
end choose
end event

event itemchanged;call super::itemchanged;string			ls_cod_prodotto, ls_des_prodotto, ls_null, ls_cod_deposito
long			ll_anno_commessa, ll_num_commessa

setnull(ls_null)

if row > 0 then
	choose case dwo.name
		case "cod_prodotto"
			if data <>"" and not isnull(data) then			
				select des_prodotto
				into :ls_des_prodotto
				from anag_prodotti
				where cod_azienda =:s_cs_xx.cod_azienda and
							cod_prodotto =:data ;
				ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"anno_commessa")
				ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_commessa")
				 
				 setitem(row, "cod_azienda", s_cs_xx.cod_azienda)
				 setitem(row, "anno_commessa", ll_anno_commessa)
				 setitem(row, "num_commessa", ll_num_commessa)
				 setitem(row, "anag_prodotti_des_prodotto", ls_des_prodotto)
			else
				 setitem(row, "cod_prodotto", ls_null)
				 setitem(row, "anag_prodotti_des_prodotto", ls_null)
			end if
			
//		case "cod_reparto"
//			if data="" then
//				//setnull
//				setnull(ls_cod_deposito)
//			else
//				select cod_deposito
//				into :ls_cod_deposito
//				from anag_reparti
//				where 	cod_azienda=:s_cs_xx.cod_azienda and
//							cod_reparto=:data;
//				
//				if ls_cod_deposito="" then setnull(ls_cod_deposito)
//			end if
//			
//			setitem(row, "cod_deposito", ls_cod_deposito)
			
	end choose
end if
end event

event pcd_new;call super::pcd_new;cb_aggiorna_fabb.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_aggiorna_fabb.enabled = false
end event

event pcd_save;call super::pcd_save;cb_aggiorna_fabb.enabled = true
end event

event pcd_view;call super::pcd_view;cb_aggiorna_fabb.enabled = true
end event

