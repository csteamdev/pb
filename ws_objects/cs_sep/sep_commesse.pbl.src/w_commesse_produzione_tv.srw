﻿$PBExportHeader$w_commesse_produzione_tv.srw
$PBExportComments$Finestra commesse
forward
global type w_commesse_produzione_tv from w_cs_xx_treeview
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_2 dw_2
end type
type cbx_forza_numerazione from checkbox within w_commesse_produzione_tv
end type
type sle_num_commessa from singlelineedit within w_commesse_produzione_tv
end type
end forward

global type w_commesse_produzione_tv from w_cs_xx_treeview
integer width = 4338
integer height = 2532
string title = "Commesse Produzione"
cbx_forza_numerazione cbx_forza_numerazione
sle_num_commessa sle_num_commessa
end type
global w_commesse_produzione_tv w_commesse_produzione_tv

type variables
private:
	datastore			ids_store
	long					il_livello, il_handle
	
	// icone COMMESSA E DETTAGLI
	int						ICONA_COMM_GRIGIA, ICONA_COMM_VERDE, ICONA_COMM_GIALLA, ICONA_COMM_ROSSA, ICONA_DETTAGLIO
	
	// ALTRE icone
	int						ICONA_OPERATORE, ICONA_TIPO_COMM, ICONA_ANNO, ICONA_DATA, &
							ICONA_DEPOSITO, ICONA_PRODOTTO
	
	boolean				ib_new

	long					il_max_row = 255
	
	uo_magazzino		iuo_magazzino
	
	boolean				ib_GIB=false
	
	string					is_join_ordven_bolven = ""
end variables

forward prototypes
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function long wf_inserisci_operatori (long al_handle)
public function long wf_inserisci_tipo_commesse (long al_handle)
public function long wf_inserisci_anno (long al_handle)
public function long wf_inserisci_data_registrazione (long al_handle)
public function long wf_inserisci_data_consegna (long al_handle)
public function long wf_inserisci_data_produzione (long al_handle)
public function long wf_inserisci_data_chiusura (long al_handle)
public function long wf_inserisci_depositi_prelievo (long al_handle)
public function long wf_inserisci_depositi_versamento (long al_handle)
public function long wf_inserisci_prodotti (long al_handle)
public function long wf_inserisci_commesse (long al_handle)
public function treeviewitem wf_new_treeviewitem (boolean ab_children, integer ai_icon)
public function long wf_inserisci_singola_commessa (long al_handle, integer ai_anno_commessa, long al_num_commessa, decimal ad_tot_val_pf, integer ai_icon)
public subroutine wf_imposta_ricerca ()
public subroutine wf_treeview_icons ()
public subroutine wf_valori_livelli ()
public function long wf_inserisci_dettagli (long al_handle, integer ai_anno_commessa, long al_numero_commessa)
public function integer wf_imposta_bottoni ()
public function integer wf_imposta_bottoni_dettaglio ()
public function integer wf_proteggi_colonne ()
public function integer wf_attivazione ()
public function integer wf_chiudi_commessa ()
public function integer wf_disimpegno (integer ai_anno_commessa, long al_num_commessa, ref string as_errore)
public subroutine wf_annulla_attivazione ()
end prototypes

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello		
		
		//Operatore
		case "O"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_commesse.cod_operatore is null "
			else
				as_sql += " AND anag_commesse.cod_operatore='" + lstr_data.codice + "' "
			end if
		
		//Tipo Commessa
		case "T"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_commesse.cod_tipo_commessa is null "
			else
				as_sql += " AND anag_commesse.cod_tipo_commessa='" + lstr_data.codice + "' "
			end if
		
		//Anno Commessa
		case "A"
			as_sql += " AND anag_commesse.anno_commessa = " + lstr_data.codice
		
		//Data Commessa
		case "R"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_commesse.data_registrazione is null "
			else
				as_sql += " AND anag_commesse.data_registrazione = '" + lstr_data.codice + "' "
			end if
		
		//Data Consegna
		case "C"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_commesse.data_consegna is null "
			else
				as_sql += " AND anag_commesse.data_consegna = '" + lstr_data.codice + "' "
			end if
			
		//Data Produzione
		case "Z"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_commesse.data_produzione is null "
			else
				as_sql += " AND anag_commesse.data_produzione = '" + lstr_data.codice + "' "
			end if
			
		//Data Chiusura
		case "X"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_commesse.data_chiusura is null "
			else
				as_sql += " AND anag_commesse.data_chiusura = '" + lstr_data.codice + "' "
			end if
		
		//Deposito Prelievo
		case "D"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_commesse.cod_deposito_prelievo is null "
			else
				as_sql += " AND anag_commesse.cod_deposito_prelievo = '" + lstr_data.codice + "' "
			end if
		
		//Deposito Versamento
		case "V"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_commesse.cod_deposito_versamento is null "
			else
				as_sql += " AND anag_commesse.cod_deposito_versamento = '" + lstr_data.codice + "' "
			end if
		
		//Prodotto
		case "P"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_commesse.cod_prodotto is null "
			else
				as_sql += " AND anag_commesse.cod_prodotto = '" + lstr_data.codice + "' "
			end if
						
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "O"
		return wf_inserisci_operatori(al_handle)
		
	case "T"
		return wf_inserisci_tipo_commesse(al_handle)
		
	case "A"
		return wf_inserisci_anno(al_handle)
		
	case "R"
		return wf_inserisci_data_registrazione(al_handle)
		
	case "C"
		return wf_inserisci_data_consegna(al_handle)
	
	case "Z"
		return wf_inserisci_data_produzione(al_handle)
	
	case "X"
		return wf_inserisci_data_chiusura(al_handle)
		
	case "D"
		return wf_inserisci_depositi_prelievo(al_handle)
		
	case "V"
		return wf_inserisci_depositi_versamento(al_handle)
		
	case "P"
		return wf_inserisci_prodotti(al_handle)

	case "N"
			return wf_inserisci_commesse(al_handle)
		
end choose
end function

public function long wf_inserisci_operatori (long al_handle);string ls_sql, ls_label, ls_cod_operatore, ls_des_operatore, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tab_operatori.cod_operatore, tab_operatori.des_operatore FROM anag_commesse " + is_join_ordven_bolven + &
" LEFT OUTER JOIN tab_operatori ON tab_operatori.cod_azienda = anag_commesse.cod_azienda AND &
tab_operatori.cod_operatore = anag_commesse.cod_operatore &
WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_operatore = ids_store.getitemstring(ll_i, 1)
	 ls_des_operatore = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "O"
	lstr_data.codice = ls_cod_operatore
	
	if isnull(ls_cod_operatore) and isnull(ls_des_operatore) then
		ls_label = "<Operatore mancante>"
	elseif isnull(ls_des_operatore) then
		ls_label = ls_cod_operatore
	else
		ls_label = ls_cod_operatore + " - " + ls_des_operatore
	end if
	
	ltvi_item = wf_new_item(true, ICONA_OPERATORE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_tipo_commesse (long al_handle);string ls_sql, ls_label, ls_cod_tipo_commessa, ls_des_tipo_commessa, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tab_tipi_commessa.cod_tipo_commessa, tab_tipi_commessa.des_tipo_commessa FROM anag_commesse " + is_join_ordven_bolven + &
" LEFT OUTER JOIN tab_tipi_commessa ON tab_tipi_commessa.cod_azienda = anag_commesse.cod_azienda AND &
tab_tipi_commessa.cod_tipo_commessa = anag_commesse.cod_tipo_commessa &
WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_tipo_commessa = ids_store.getitemstring(ll_i, 1)
	 ls_des_tipo_commessa = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "T"
	lstr_data.codice = ls_cod_tipo_commessa
	
	if isnull(ls_cod_tipo_commessa) and isnull(ls_des_tipo_commessa) then
		ls_label = "<Tipo Ordine mancante>"
	elseif isnull(ls_des_tipo_commessa) then
		ls_label = ls_cod_tipo_commessa
	else
		ls_label = ls_cod_tipo_commessa + " - " + ls_des_tipo_commessa
	end if
	
	ltvi_item = wf_new_item(true, ICONA_TIPO_COMM)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_anno (long al_handle);string ls_sql, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_commesse.anno_commessa FROM anag_commesse " + is_join_ordven_bolven + &
			" WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 desc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "A"
	lstr_data.codice = string(ids_store.getitemnumber(ll_i, 1))
	
	ltvi_item = wf_new_item(true, ICONA_ANNO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = string(ids_store.getitemnumber(ll_i, 1))
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_data_registrazione (long al_handle);string ls_sql, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_commesse.data_registrazione FROM anag_commesse " + is_join_ordven_bolven + " WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 desc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "R"
	lstr_data.codice = string(ids_store.getitemdatetime(ll_i, 1), s_cs_xx.db_funzioni.formato_data)
	
	ltvi_item = wf_new_item(true, ICONA_DATA)
	
	ltvi_item.data = lstr_data
	if isnull(ids_store.getitemdatetime(ll_i, 1)) then
		ltvi_item.label = "<Data registrazione mancante>"
	else
		ltvi_item.label = string(ids_store.getitemdatetime(ll_i, 1), "dd/mm/yyyy")
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_data_consegna (long al_handle);string ls_sql, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_commesse.data_consegna FROM anag_commesse " + is_join_ordven_bolven + " WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 desc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "C"
	lstr_data.codice = string(ids_store.getitemdatetime(ll_i, 1), s_cs_xx.db_funzioni.formato_data)
	
	ltvi_item = wf_new_item(true, ICONA_DATA)
	
	ltvi_item.data = lstr_data
	if isnull(ids_store.getitemdatetime(ll_i, 1)) then
		ltvi_item.label = "<Data consegna mancante>"
	else
		ltvi_item.label = string(ids_store.getitemdatetime(ll_i, 1), "dd/mm/yyyy")
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_data_produzione (long al_handle);string ls_sql, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_commesse.data_produzione FROM anag_commesse " + is_join_ordven_bolven + " WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 desc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "Z"
	lstr_data.codice = string(ids_store.getitemdatetime(ll_i, 1), s_cs_xx.db_funzioni.formato_data)
	
	ltvi_item = wf_new_item(true, ICONA_DATA)
	
	ltvi_item.data = lstr_data
	if isnull(ids_store.getitemdatetime(ll_i, 1)) then
		ltvi_item.label = "<Data produzione mancante>"
	else
		ltvi_item.label = string(ids_store.getitemdatetime(ll_i, 1), "dd/mm/yyyy")
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_data_chiusura (long al_handle);string ls_sql, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_commesse.data_chiusura FROM anag_commesse " + is_join_ordven_bolven + " WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 desc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "X"
	lstr_data.codice = string(ids_store.getitemdatetime(ll_i, 1), s_cs_xx.db_funzioni.formato_data)
	
	ltvi_item = wf_new_item(true, ICONA_DATA)
	
	ltvi_item.data = lstr_data
	if isnull(ids_store.getitemdatetime(ll_i, 1)) then
		ltvi_item.label = "<Data chiusura mancante>"
	else
		ltvi_item.label = string(ids_store.getitemdatetime(ll_i, 1), "dd/mm/yyyy")
	end if
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_depositi_prelievo (long al_handle);string ls_sql, ls_label, ls_cod_deposito, ls_des_deposito, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_depositi.cod_deposito, anag_depositi.des_deposito FROM anag_commesse " + is_join_ordven_bolven + &
" LEFT OUTER JOIN anag_depositi ON anag_depositi.cod_azienda = anag_commesse.cod_azienda AND &
anag_depositi.cod_deposito = anag_commesse.cod_deposito_prelievo &
WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_deposito = ids_store.getitemstring(ll_i, 1)
	 ls_des_deposito = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "D"
	lstr_data.codice = ls_cod_deposito
	
	if isnull(ls_cod_deposito) and isnull(ls_des_deposito) then
		ls_label = "<Deposito prelievo MP mancante>"
	elseif isnull(ls_des_deposito) then
		ls_label = ls_cod_deposito
	else
		ls_label = ls_cod_deposito + " - " + ls_des_deposito
	end if

	ltvi_item = wf_new_item(true, ICONA_DEPOSITO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_depositi_versamento (long al_handle);string ls_sql, ls_label, ls_cod_deposito, ls_des_deposito, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_depositi.cod_deposito, anag_depositi.des_deposito FROM anag_commesse " + is_join_ordven_bolven + &
" LEFT OUTER JOIN anag_depositi ON anag_depositi.cod_azienda = anag_commesse.cod_azienda AND &
anag_depositi.cod_deposito = anag_commesse.cod_deposito_versamento &
WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_deposito = ids_store.getitemstring(ll_i, 1)
	 ls_des_deposito = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "V"
	lstr_data.codice = ls_cod_deposito
	
	if isnull(ls_cod_deposito) and isnull(ls_des_deposito) then
		ls_label = "<Deposito versamento PF mancante>"
	elseif isnull(ls_des_deposito) then
		ls_label = ls_cod_deposito
	else
		ls_label = ls_cod_deposito + " - " + ls_des_deposito
	end if

	ltvi_item = wf_new_item(true, ICONA_DEPOSITO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_prodotti (long al_handle);string ls_sql, ls_label, ls_cod_prodotto, ls_des_prodotto, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_commesse.cod_prodotto, anag_prodotti.des_prodotto FROM anag_commesse " + is_join_ordven_bolven + &
" LEFT OUTER JOIN anag_prodotti ON anag_prodotti.cod_azienda = anag_commesse.cod_azienda AND &
anag_prodotti.cod_prodotto = anag_commesse.cod_prodotto &
WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_prodotto = ids_store.getitemstring(ll_i, 1)
	 ls_des_prodotto = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "P"
	lstr_data.codice = ls_cod_prodotto
	
	if isnull(ls_cod_prodotto) and isnull(ls_des_prodotto) then
		ls_label = "<Prodotto mancante>"
	elseif isnull(ls_des_prodotto) then
		ls_label = ls_cod_prodotto
	else
		ls_label = ls_cod_prodotto + " - " + ls_des_prodotto
	end if

	ltvi_item = wf_new_item(true, ICONA_PRODOTTO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_commesse (long al_handle);string ls_sql, ls_error, ls_flag_ordinamento
long ll_rows, ll_i
dec{4} ld_tot_valore_pf
treeviewitem ltvi_item
integer li_icon
boolean lb_dettagli = false


ls_flag_ordinamento = tab_ricerca.ricerca.dw_ricerca.getitemstring( 1, "flag_ordinamento")

ls_sql = "SELECT anag_commesse.anno_commessa, anag_commesse.num_commessa, &
					   anag_commesse.flag_tipo_avanzamento, anag_commesse.tot_valore_pf, anag_commesse.data_registrazione, anag_commesse.data_consegna  &
			FROM anag_commesse " + is_join_ordven_bolven + " WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)


//imposto order by
if not isnull(ls_flag_ordinamento) and ls_flag_ordinamento <> "" then
	if ls_flag_ordinamento = "A" then
		ls_sql += " order by anag_commesse.anno_commessa, anag_commesse.num_commessa "
		
	elseif ls_flag_ordinamento = "D" then
		ls_sql += " order by anag_commesse.data_registrazione, anag_commesse.anno_commessa, anag_commesse.num_commessa "
		
	elseif ls_flag_ordinamento = "C" then
		ls_sql += " order by anag_commesse.data_consegna, anag_commesse.anno_commessa, anag_commesse.num_commessa "
	end if
else
	ls_sql += " order by anag_commesse.anno_commessa, anag_commesse.num_commessa "
end if

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

if ll_rows > il_max_row  then
	if not g_mb.confirm("Sono stati recuperate " + string(ll_rows) + " commesse, superando il limite consigliato.~r~nProcedere con la visualizzazione di tutte le commesse?") then
		// Visualizzo solo le richieste nel limite
		ll_rows = il_max_row
	end if
end if

for ll_i = 1 to ll_rows
	
	li_icon = ICONA_COMM_GRIGIA		//imposto inzialmente come fosse 0 o NULL -> NESSUNO
	
	choose case ids_store.getitemstring(ll_i, 3)
		case "1","2"			//ASSEGNAZIONE, ATTIVAZIONE
			li_icon = ICONA_COMM_VERDE
			
		case "3"			//AUTOMATICO
			li_icon = ICONA_COMM_GIALLA
			
		case "7","8","9"		//CHIUSA ASSEGNAZIONE, CHIUSUA ATTIVAZIONE, CHIUSA AUTOMATICO
			li_icon = ICONA_COMM_ROSSA
			
	end choose

	wf_inserisci_singola_commessa(al_handle, ids_store.getitemnumber(ll_i, 1), ids_store.getitemnumber(ll_i, 2), ld_tot_valore_pf, li_icon)

next

return ll_rows
end function

public function treeviewitem wf_new_treeviewitem (boolean ab_children, integer ai_icon);treeviewitem ltvi_campo

ltvi_campo.expanded = false
ltvi_campo.selected = false
ltvi_campo.children = ab_children

ltvi_campo.pictureindex = ai_icon		
ltvi_campo.selectedpictureindex = ai_icon		


return ltvi_campo
end function

public function long wf_inserisci_singola_commessa (long al_handle, integer ai_anno_commessa, long al_num_commessa, decimal ad_tot_val_pf, integer ai_icon);string ls_error
treeviewitem ltvi_item
long ll_count
boolean lb_dettagli = false

str_treeview lstr_data

//stabilisci già adesso se ci sono dettagli
select count(*)
into :ll_count
from det_anag_commesse
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:ai_anno_commessa and
			num_commessa=:al_num_commessa;
	
if ll_count>0 then lb_dettagli = true

ltvi_item = wf_new_treeviewitem(lb_dettagli, ai_icon)


lstr_data.livello = il_livello
lstr_data.tipo_livello = "N"
lstr_data.decimale[1] = ai_anno_commessa
lstr_data.decimale[2] = al_num_commessa
lstr_data.decimale[3] = -1


if isnull(ad_tot_val_pf) then ad_tot_val_pf=0.00

ltvi_item.label = string(ai_anno_commessa) + "/"+ string(al_num_commessa)

if ad_tot_val_pf>0 then ltvi_item.label += " Val.Unit.PF "+string(ad_tot_val_pf, "#,###,###,##0.00") 

ltvi_item.data = lstr_data

tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)

return 1
end function

public subroutine wf_imposta_ricerca ();//string			ls_cod_cliente, ls_cod_contatto, ls_cod_tipo_off_ven, ls_cod_valuta, ls_flag_evasione, ls_num_documento, ls_tes, ls_cod_deposito, &
//					ls_cod_agente_1, ls_cod_agente_2, ls_cod_documento, ls_numeratore_documento, ls_cod_prodotto, ls_det,ls_flag_offerte_web, &
//					ls_cod_tipo_off_web
//long				ll_anno_registrazione, ll_num_registrazione, ll_num_revisione, ll_anno_documento, ll_num_documento
//date				ldt_data

integer			li_anno_commessa, li_anno_ordine
long				ll_num_commessa, ll_num_ordine

string				ls_filtro_1, ls_filtro_2, ls_filtro_3, ls_filtro_4, ls_filtro_5, ls_filtro_6, ls_filtro_7, ls_where_flag_tipo_avanzamento, &
					ls_cod_prodotto, ls_cod_tipo_commessa, ls_cod_operatore, ls_flag_tipo_ord_ddt
					
boolean			lb_check_flag_da_attivare
datetime			ldt_da_reg, ldt_a_reg, ldt_da_consegna, ldt_a_consegna

is_join_ordven_bolven = ""
is_sql_filtro = ""
li_anno_commessa =  tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"anno_commessa")
ll_num_commessa =  tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"num_commessa")

if not isnull(li_anno_commessa) and li_anno_commessa>0 then
	is_sql_filtro += " and anag_commesse.anno_commessa = " + string(li_anno_commessa) + " "
end if
if not isnull(ll_num_commessa) and ll_num_commessa>0 then
	is_sql_filtro += " and anag_commesse.num_commessa = " + string(ll_num_commessa) + " "
end if
if not isnull(li_anno_commessa) and li_anno_commessa>0 and not isnull(ll_num_commessa) and ll_num_commessa>0 then
	// mi fermo non serve altro, filtro solo la singolo ordine
	return
end if

//filtri sullo stato avanzamento
ls_filtro_1 = tab_ricerca.ricerca.dw_ricerca.getitemstring( 1, "flag_assegnazione")
ls_filtro_2 = tab_ricerca.ricerca.dw_ricerca.getitemstring( 1, "flag_assegnazione_chiusa")
ls_filtro_3 = tab_ricerca.ricerca.dw_ricerca.getitemstring( 1, "flag_attivazione")
ls_filtro_4 = tab_ricerca.ricerca.dw_ricerca.getitemstring( 1, "flag_attivazione_chiusa")
ls_filtro_5 = tab_ricerca.ricerca.dw_ricerca.getitemstring( 1, "flag_automatico")
ls_filtro_6 = tab_ricerca.ricerca.dw_ricerca.getitemstring( 1, "flag_automatico_chiusa")
ls_filtro_7 = tab_ricerca.ricerca.dw_ricerca.getitemstring( 1, "flag_da_attivare")

ls_where_flag_tipo_avanzamento = ""
lb_check_flag_da_attivare = false
//Assegnazione
if not isnull(ls_filtro_1) and ls_filtro_1 = "S" then
	ls_where_flag_tipo_avanzamento += "'1'"
end if
//Assegnazione chiusa
if not isnull(ls_filtro_2) and ls_filtro_2 = "S" then
	if ls_where_flag_tipo_avanzamento <> "" then ls_where_flag_tipo_avanzamento += ","
	ls_where_flag_tipo_avanzamento += "'9'"
end if
//Attivazione Chiusa
if not isnull(ls_filtro_4) and ls_filtro_4 = "S" then
	if ls_where_flag_tipo_avanzamento <> "" then ls_where_flag_tipo_avanzamento += ","
	ls_where_flag_tipo_avanzamento += "'8'"
end if
//Automatico
if not isnull(ls_filtro_5) and ls_filtro_5 = "S" then
	if ls_where_flag_tipo_avanzamento <> "" then ls_where_flag_tipo_avanzamento += ","
	ls_where_flag_tipo_avanzamento += "'3'"
end if
//Attivazione
if not isnull(ls_filtro_3) and ls_filtro_3 = "S" then
	if ls_where_flag_tipo_avanzamento <> "" then ls_where_flag_tipo_avanzamento += ","
	ls_where_flag_tipo_avanzamento += "'2'"
end if
//Da Attivare
if not isnull(ls_filtro_7) and ls_filtro_7 = "S" then
	if ls_where_flag_tipo_avanzamento <> "" then ls_where_flag_tipo_avanzamento += ","
	ls_where_flag_tipo_avanzamento += "'0'"
	lb_check_flag_da_attivare = true
end if
//Automatico Chiusa
if not isnull(ls_filtro_6) and ls_filtro_6 = "S" then
	if ls_where_flag_tipo_avanzamento <> "" then ls_where_flag_tipo_avanzamento += ","
	ls_where_flag_tipo_avanzamento += "'7'"
end if

if ls_where_flag_tipo_avanzamento <> "" then
	if lb_check_flag_da_attivare then
		is_sql_filtro += " and (anag_commesse.flag_tipo_avanzamento in (" + ls_where_flag_tipo_avanzamento + ") or anag_commesse.flag_tipo_avanzamento is null) "
	else
		is_sql_filtro += " and anag_commesse.flag_tipo_avanzamento in (" + ls_where_flag_tipo_avanzamento + ") "
	end if
end if


//altri filtri
ldt_da_reg = tab_ricerca.ricerca.dw_ricerca.getitemdatetime( 1, "data_reg_da")
ldt_a_reg = tab_ricerca.ricerca.dw_ricerca.getitemdatetime( 1, "data_reg_a")
ldt_da_consegna = tab_ricerca.ricerca.dw_ricerca.getitemdatetime( 1, "data_consegna_da")
ldt_a_consegna = tab_ricerca.ricerca.dw_ricerca.getitemdatetime( 1, "data_consegna_a")
ls_cod_prodotto = tab_ricerca.ricerca.dw_ricerca.getitemstring( 1, "cod_prodotto")
ls_cod_tipo_commessa = tab_ricerca.ricerca.dw_ricerca.getitemstring( 1, "cod_tipo_commessa")
ls_cod_operatore = tab_ricerca.ricerca.dw_ricerca.getitemstring( 1, "cod_operatore")

li_anno_ordine = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"anno_ordine")
ll_num_ordine = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"num_ordine")

if li_anno_ordine>0 or ll_num_ordine>0 then
	//attivata ricerca per commessa su ddt uscita o su ordine vendita
	ls_flag_tipo_ord_ddt = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_tipo_ord_ddt")
	
	if ls_flag_tipo_ord_ddt = "S" then
		//su ddt di uscita
		if li_anno_ordine>0 then is_sql_filtro += " AND det_bol_ven.anno_registrazione=" + string(li_anno_ordine) +" "
		if ll_num_ordine>0 then is_sql_filtro += " AND det_bol_ven.num_registrazione=" + string(ll_num_ordine) +" "
		
		is_join_ordven_bolven = " join det_bol_ven on det_bol_ven.cod_azienda=anag_commesse.cod_azienda and "+&
																					"det_bol_ven.anno_commessa=anag_commesse.anno_commessa and "+&
																					"det_bol_ven.num_commessa=anag_commesse.num_commessa "
	else
		//su ordine di vendita
		if li_anno_ordine>0 then is_sql_filtro += " AND det_ord_ven.anno_registrazione=" + string(li_anno_ordine) +" "
		if ll_num_ordine>0 then is_sql_filtro += " AND det_ord_ven.num_registrazione=" + string(ll_num_ordine) +" "
		
		is_join_ordven_bolven = " join det_ord_ven on det_ord_ven.cod_azienda=anag_commesse.cod_azienda and "+&
																					"det_ord_ven.anno_commessa=anag_commesse.anno_commessa and "+&
																					"det_ord_ven.num_commessa=anag_commesse.num_commessa "
	end if
	
end if

if not isnull(ldt_da_reg) then
	is_sql_filtro += " and anag_commesse.data_registrazione>='" + string( ldt_da_reg, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_a_reg) then
	is_sql_filtro += " and anag_commesse.data_registrazione<='" + string( ldt_a_reg, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_da_consegna) then
	is_sql_filtro += " and anag_commesse.data_consegna >= '" + string( ldt_da_consegna, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_a_consegna) then
	is_sql_filtro += " and anag_commesse.data_consegna <= '" + string( ldt_a_consegna, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ls_cod_tipo_commessa) and ls_cod_tipo_commessa <> "" then
	is_sql_filtro += " and anag_commesse.cod_tipo_commessa = '" + ls_cod_tipo_commessa + "' "
end if
if not isnull(ls_cod_operatore) and ls_cod_operatore <> "" then
	is_sql_filtro += " and anag_commesse.cod_operatore = '" + ls_cod_operatore + "' "
end if
if not isnull(ls_cod_prodotto) and ls_cod_prodotto <> "" then
	is_sql_filtro += "and anag_commesse.cod_prodotto='" + ls_cod_prodotto + "'"
end if

//if anno_ordine




end subroutine

public subroutine wf_treeview_icons ();ICONA_COMM_GRIGIA = wf_treeview_add_icon("treeview\cartella_grigia.png")
ICONA_COMM_VERDE = wf_treeview_add_icon("treeview\cartella_verde.png")
ICONA_COMM_GIALLA = wf_treeview_add_icon("treeview\cartella.png")
ICONA_COMM_ROSSA = wf_treeview_add_icon("treeview\cartella_rossa.png")

ICONA_DETTAGLIO = wf_treeview_add_icon("treeview\calculator.png")

ICONA_OPERATORE = wf_treeview_add_icon("treeview\operatore.png")
ICONA_TIPO_COMM = wf_treeview_add_icon("treeview\tipo_documento.png")
ICONA_ANNO = wf_treeview_add_icon("treeview\anno.png")
ICONA_DATA = wf_treeview_add_icon("treeview\data_1.png")
ICONA_DEPOSITO = wf_treeview_add_icon("treeview\deposito.png")
ICONA_PRODOTTO = wf_treeview_add_icon("treeview\prodotto.png")
end subroutine

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Operatore", "O")
wf_add_valore_livello("Tipo Commessa", "T")
wf_add_valore_livello("Anno Commessa", "A")
wf_add_valore_livello("Data Commessa", "R")
wf_add_valore_livello("Data Consegna", "C")
wf_add_valore_livello("Data Chiusura", "X")
wf_add_valore_livello("Data Produzione", "Z")
wf_add_valore_livello("Deposito Prelievo", "D")
wf_add_valore_livello("Deposito Versamento", "V")
wf_add_valore_livello("Prodotto", "P")

end subroutine

public function long wf_inserisci_dettagli (long al_handle, integer ai_anno_commessa, long al_numero_commessa);string ls_sql, ls_label, ls_error
long ll_rows, ll_i, ll_prog_riga
treeviewitem ltvi_item


ls_sql = "SELECT det_anag_commesse.prog_riga FROM det_anag_commesse &
WHERE det_anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' and "+&
		 "det_anag_commesse.anno_commessa=" + string(ai_anno_commessa) + " and "+&
		 "det_anag_commesse.num_commessa=" + string(al_numero_commessa)


ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1desc")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ll_prog_riga = ids_store.getitemnumber(ll_i, 1)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "B"
	lstr_data.codice = string(ll_prog_riga)
	lstr_data.decimale[1] = ai_anno_commessa
	lstr_data.decimale[2] = al_numero_commessa
	lstr_data.decimale[3] = ll_prog_riga
	
	ls_label = string(ai_anno_commessa) + " - " + string(al_numero_commessa) + " - " + string(ll_prog_riga)
	
	ltvi_item = wf_new_item(false, ICONA_DETTAGLIO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_imposta_bottoni ();long ll_num_det, ll_anno_commessa, ll_num_commessa
string  ls_flag_tipo_avanzamento

if tab_dettaglio.det_1.dw_1.rowcount() = 0 then
	tab_dettaglio.det_1.dw_1.object.b_annulla_attiva.enabled = false
	tab_dettaglio.det_1.dw_1.object.b_attiva.enabled = false
	tab_dettaglio.det_1.dw_1.object.b_chiudi.enabled = false
	return 0
end if

ls_flag_tipo_avanzamento = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"flag_tipo_avanzamento")

choose case ls_flag_tipo_avanzamento
	case "1" // Avanzamento Assegnazione
		tab_dettaglio.det_1.dw_1.object.b_annulla_attiva.enabled = false
		tab_dettaglio.det_1.dw_1.object.b_attiva.enabled = false
		tab_dettaglio.det_1.dw_1.object.b_chiudi.enabled = false
		tab_dettaglio.det_1.dw_1.object.b_varianti.enabled = false
	case "2" // Avanzamento Attivazione
		
		tab_dettaglio.det_1.dw_1.object.b_annulla_attiva.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_attiva.enabled = true	
		tab_dettaglio.det_1.dw_1.object.b_varianti.enabled = false
		tab_dettaglio.det_1.dw_1.object.b_chiudi.enabled = false
		
	case "3" // Avanzamento Automatico
		tab_dettaglio.det_1.dw_1.object.b_annulla_attiva.enabled = false
		tab_dettaglio.det_1.dw_1.object.b_attiva.enabled = false
		tab_dettaglio.det_1.dw_1.object.b_chiudi.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_varianti.enabled = false
	case "9" // Chiusura Commessa Assegnazione
		tab_dettaglio.det_1.dw_1.object.b_annulla_attiva.enabled = false
		tab_dettaglio.det_1.dw_1.object.b_attiva.enabled = false
		tab_dettaglio.det_1.dw_1.object.b_chiudi.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_varianti.enabled = false
	case "8" // Chiusura Commessa Attivazione
		tab_dettaglio.det_1.dw_1.object.b_annulla_attiva.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_attiva.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_chiudi.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_varianti.enabled = false
	case "7" // Chiusura Commessa Automatica
		tab_dettaglio.det_1.dw_1.object.b_annulla_attiva.enabled = false
		tab_dettaglio.det_1.dw_1.object.b_attiva.enabled = false
		tab_dettaglio.det_1.dw_1.object.b_chiudi.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_varianti.enabled = false
	case else // Nessun Avanzamento
		tab_dettaglio.det_1.dw_1.object.b_annulla_attiva.enabled = false
		tab_dettaglio.det_1.dw_1.object.b_attiva.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_chiudi.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_varianti.enabled = true
end choose
return 0
end function

public function integer wf_imposta_bottoni_dettaglio ();string ls_test
double ldd_quan_in_produzione,ldd_quan_prodotta
long ll_anno_commessa,ll_num_commessa,ll_prog_riga,ll_conteggio, ldd_quan_anticipo

ll_anno_commessa = tab_dettaglio.det_2.dw_2.getitemnumber(tab_dettaglio.det_2.dw_2.getrow(), "anno_commessa")
ll_num_commessa = tab_dettaglio.det_2.dw_2.getitemnumber(tab_dettaglio.det_2.dw_2.getrow(), "num_commessa")
ll_prog_riga = tab_dettaglio.det_2.dw_2.getitemnumber(tab_dettaglio.det_2.dw_2.getrow(),"prog_riga")
ldd_quan_in_produzione = tab_dettaglio.det_2.dw_2.getitemnumber(tab_dettaglio.det_2.dw_2.getrow(),"quan_in_produzione")
ldd_quan_anticipo = tab_dettaglio.det_2.dw_2.getitemnumber(tab_dettaglio.det_2.dw_2.getrow(),"quan_anticipo")

tab_dettaglio.det_2.dw_2.object.b_avanzamento.enabled = false
tab_dettaglio.det_2.dw_2.object.b_mat_prime.enabled = false

select cod_azienda
into   :ls_test
from   avan_produzione_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :ll_anno_commessa and    
		 num_commessa = :ll_num_commessa and    
		 prog_riga = :ll_prog_riga;

if ls_test = s_cs_xx.cod_azienda then tab_dettaglio.det_2.dw_2.object.b_avanzamento.enabled = true
ls_test = ""

select cod_azienda
into   :ls_test
from   mat_prime_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :ll_anno_commessa and    
		 num_commessa = :ll_num_commessa and    
		 prog_riga = :ll_prog_riga;

if ls_test = s_cs_xx.cod_azienda then tab_dettaglio.det_2.dw_2.object.b_mat_prime.enabled = true

if ldd_quan_anticipo > 0 then
	tab_dettaglio.det_2.dw_2.object.b_anticipi.enabled = true
else
	tab_dettaglio.det_2.dw_2.object.b_anticipi.enabled = false
end if

return 0
end function

public function integer wf_proteggi_colonne ();string ls_flag_tipo_avanzamento 

ls_flag_tipo_avanzamento= tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"flag_tipo_avanzamento")

if ls_flag_tipo_avanzamento<>"0" then
	tab_dettaglio.det_1.dw_1.Object.cod_tipo_commessa.Protect=1
	tab_dettaglio.det_1.dw_1.Object.cod_tipo_commessa.Protect=1
	tab_dettaglio.det_1.dw_1.Object.data_registrazione.Protect=1
	tab_dettaglio.det_1.dw_1.Object.cod_operatore.Protect=1
	tab_dettaglio.det_1.dw_1.Object.cod_prodotto.Protect=1
	tab_dettaglio.det_1.dw_1.Object.cod_versione.Protect=1
	tab_dettaglio.det_1.dw_1.Object.cod_deposito_prelievo.Protect=1
	tab_dettaglio.det_1.dw_1.Object.cod_deposito_versamento.Protect=1
	tab_dettaglio.det_1.dw_1.Object.quan_ordine.Protect=1
	tab_dettaglio.det_1.dw_1.Object.data_consegna.Protect=1
else
	tab_dettaglio.det_1.dw_1.Object.cod_tipo_commessa.Protect=0
	tab_dettaglio.det_1.dw_1.Object.cod_tipo_commessa.Protect=0
	tab_dettaglio.det_1.dw_1.Object.data_registrazione.Protect=0
	tab_dettaglio.det_1.dw_1.Object.cod_operatore.Protect=0
	tab_dettaglio.det_1.dw_1.Object.cod_prodotto.Protect=0
	tab_dettaglio.det_1.dw_1.Object.cod_versione.Protect=0
	tab_dettaglio.det_1.dw_1.Object.cod_deposito_prelievo.Protect=0
	tab_dettaglio.det_1.dw_1.Object.cod_deposito_versamento.Protect=0
	tab_dettaglio.det_1.dw_1.Object.quan_ordine.Protect=0
	tab_dettaglio.det_1.dw_1.Object.data_consegna.Protect=0
end if

return 0
end function

public function integer wf_attivazione ();double					ldd_quan_in_produzione,ldd_quan_in_ordine, & 
							ldd_quan_prodotta,ldd_quan_da_assegnare,ldd_quantita_possibile, & 
							ldd_quan_assegnata_st, ldd_quan_assegnata_mp, ldd_quan_impegnata, ldd_quan_assegnata
							
long						ll_num_commessa,ll_prog_riga,ll_prog_stock[1],ll_anno_reg_des_mov, ll_num_reg_des_mov

string						ls_errore,ls_test,ls_cod_deposito_prelievo, ls_cod_tipo_mov_anticipo, ls_flag_tipo_avanzamento,& 
							ls_cod_tipo_commessa,ls_cod_tipo_movimento,ls_cod_prodotto,ls_cod_deposito[1], & 
							ls_cod_ubicazione[1],ls_cod_lotto[1],ls_cod_cliente[1],ls_cod_fornitore[1],ls_cod_versione, ls_flag_tipo_impegno_mp
		  
long						li_risposta,li_anno_commessa

date						ld_data_stock[1]

uo_funzioni_1			luo_funzioni

s_cs_xx_parametri		lstr_parametri


li_anno_commessa = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_commessa")
ll_num_commessa = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_commessa")
ldd_quan_in_produzione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "quan_in_produzione")
ldd_quan_prodotta = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "quan_prodotta")
ldd_quan_in_ordine = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "quan_ordine")
ls_cod_prodotto = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "cod_prodotto")
ls_cod_deposito_prelievo = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "cod_deposito_prelievo")
ls_cod_tipo_commessa = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "cod_tipo_commessa")
ls_cod_versione = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "cod_versione")
ls_flag_tipo_avanzamento = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_tipo_avanzamento")
ldd_quan_assegnata = tab_dettaglio.det_1.dw_1.getitemnumber( tab_dettaglio.det_1.dw_1.getrow(), "quan_assegnata")

if ls_flag_tipo_avanzamento <> "2" and ls_flag_tipo_avanzamento <> "0" and ls_flag_tipo_avanzamento <> "8" then
	g_mb.messagebox("Sep","Questa commessa è già stata lanciata in produzione con un'altro metodo, pertanto non è possibile compiere alcuna operazione con questo metodo.",exclamation!)
	return -1
end if

select cod_prodotto_figlio
into   :ls_test
from   distinta
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_prodotto_padre = :ls_cod_prodotto;

if sqlca.sqlcode = 100 then
	g_mb.messagebox("Sep","Attenzione! Il prodotto della commessa non ha un distinta base associata, pertanto non sarà possibile assegnare le materie prime. Creare la distinta base del prodotto",exclamation!)
	return -1
end if

if ldd_quan_in_produzione + ldd_quan_prodotta >= ldd_quan_in_ordine then
	if g_mb.messagebox("Sep","La somma delle quantità prodotta e in produzione è uguale o supera la quantità in ordine: si vuole produrre una nuova sottocommessa per aumentare la quantità in produzione?",question!,yesno!,1) = 2 then 
		return -1 
	else
		ldd_quan_in_ordine = 0
	end if
else
	ldd_quan_in_ordine = ldd_quan_in_ordine - ldd_quan_in_produzione - ldd_quan_prodotta
end if

select cod_tipo_mov_ver_prod_finiti,
		 cod_tipo_mov_anticipo,
		 flag_tipo_impegno_mp
into   :ls_cod_tipo_movimento,
		 :ls_cod_tipo_mov_anticipo,
		 :ls_flag_tipo_impegno_mp
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode<>0 then
	g_mb.messagebox( "Sep", "Errore nel DB:" + sqlca.sqlerrtext, exclamation!)
	setpointer(arrow!)
	return -1
end if

if isnull(ls_cod_tipo_movimento) or isnull(ls_cod_tipo_mov_anticipo) then
	g_mb.messagebox( "Sep", "Manca la configurazione dei tipi movimento in tabella tipi commessa!", exclamation!)
	setpointer(arrow!)
	return -1
end if

lstr_parametri.parametro_d_2 = ldd_quan_in_ordine
lstr_parametri.parametro_s_1 = ""

openwithparm(w_attiva, lstr_parametri)

lstr_parametri = message.powerobjectparm

if lstr_parametri.parametro_i_1 = 0 then return -1

ldd_quan_in_produzione =lstr_parametri.parametro_d_1

//	***	Michela 04/06/2007: se l'impegno è previsto durante l'attivazione, lo faccio in questo punto

if not isnull(ls_flag_tipo_impegno_mp) and ls_flag_tipo_impegno_mp = "N" then
	// impegno le materie prime.
	//ldd_quan_impegnata = ldd_quan_in_ordine - ldd_quan_assegnata - ldd_quan_in_produzione - ldd_quan_prodotta
	if iuo_magazzino.uof_impegna_mp_commessa(	true, &
															false, &
															ls_cod_prodotto, &
															ls_cod_versione, &
															ldd_quan_in_produzione, &
															li_anno_commessa, &
															ll_num_commessa, &
															ls_errore) = -1 then
		g_mb.messagebox("Apice","Errore durante l'impegno~r~n"+ls_errore,stopsign!)
		rollback;
		return -1
	end if
end if

setpointer(hourglass!)

select max(prog_riga)
into   :ll_prog_riga
from   det_anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa;

if sqlca.sqlcode<>0 then
	g_mb.messagebox( "Sep", "Errore nel DB:" + sqlca.sqlerrtext, exclamation!)
	rollback;
	setpointer(arrow!)
	return -1
end if

if not isnull(ll_prog_riga) then
	ll_prog_riga++
else
	ll_prog_riga = 1
end if

INSERT INTO det_anag_commesse
          ( cod_azienda,
            anno_commessa,   
            num_commessa,   
            prog_riga,   
            anno_registrazione,   
            num_registrazione,   
            quan_assegnata,   
            quan_in_produzione,   
            quan_prodotta,
				cod_tipo_movimento,
				anno_reg_des_mov,
				num_reg_des_mov,
				cod_tipo_mov_anticipo,
				quan_anticipo,
				anno_reg_anticipo,
				num_reg_anticipo )  
VALUES    ( :s_cs_xx.cod_azienda,   
            :li_anno_commessa,   
            :ll_num_commessa,   
            :ll_prog_riga,   
            null,   
            null,
            0,
            :ldd_quan_in_produzione,
            0,
				:ls_cod_tipo_movimento,
				null,
				null,
				:ls_cod_tipo_mov_anticipo,
				0,
				null,
				null);

if sqlca.sqlcode<>0 then
	g_mb.messagebox( "Sep", "Errore nel DB:" + sqlca.sqlerrtext, exclamation!)
	rollback;
	setpointer(arrow!)
	return -1
end if

//li_risposta = f_avan_prod_com (li_anno_commessa, ll_num_commessa, ll_prog_riga, & 
//										 ls_cod_prodotto, ls_cod_versione, ldd_quan_in_produzione, & 
//										 1, ls_cod_tipo_commessa, ls_errore )
luo_funzioni = create uo_funzioni_1
li_risposta = luo_funzioni.uof_avan_prod_com (	true, li_anno_commessa, ll_num_commessa, ll_prog_riga, & 
																ls_cod_prodotto, ls_cod_versione, ldd_quan_in_produzione, & 
																1, ls_cod_tipo_commessa, ls_errore )
destroy luo_funzioni

if li_risposta = -1 then
	rollback;
	g_mb.messagebox( "Sep", ls_errore, exclamation!)
	setpointer(arrow!)
	return -1
end if

select sum(quan_in_produzione),
		 sum(quan_prodotta)
into   :ldd_quan_in_produzione,
		 :ldd_quan_prodotta
from   det_anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return -1
end if

update anag_commesse
set    quan_in_produzione = :ldd_quan_in_produzione,
		 flag_tipo_avanzamento = '2'
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return -1
end if


tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(),"quan_in_produzione",ldd_quan_in_produzione)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(),"flag_tipo_avanzamento",'2')
tab_dettaglio.det_1.dw_1.resetupdate()
commit;


li_risposta = wf_imposta_bottoni()

setpointer(arrow!)
return 0
end function

public function integer wf_chiudi_commessa ();boolean lb_dt1
integer li_anno_commessa,li_risposta,li_flag_errore,li_anno_registrazione
long ll_row[],ll_righe,ll_num_commessa,ll_selected[],ll_num_reg,ll_prog_riga_ord_ven, ll_anni[], ll_numeri[]
string ls_flag_tipo_avanzamento,ls_errore,ls_parametro, ls_log,ls_null

setnull(ls_null)

	
ll_row[1] = tab_dettaglio.det_1.dw_1.getrow()	

li_anno_commessa = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_commessa")
ll_num_commessa = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_commessa")
s_cs_xx.parametri.parametro_i_1 = li_anno_commessa
s_cs_xx.parametri.parametro_ul_1 = ll_num_commessa

ls_flag_tipo_avanzamento = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_tipo_avanzamento")

if ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" then
	if g_mb.messagebox( "SEP", "La Commessa è stata già chiusa, si Desidera Riaprirla?", question!, yesno!) = 2 then
		return -1
	end if
elseif ls_flag_tipo_avanzamento = "1" or ls_flag_tipo_avanzamento = "2" then
	g_mb.messagebox("Avanzamento Automatico", "Impossibile Proseguire, La Commessa è stata già Attivata o Assegnata")
	return -1
end if

window_open(w_lista_stock_mp_commesse, 0)

// cancellazione tabella distinte_taglio_calcolate

select stringa
into   :ls_parametro
from   parametri
where  cod_parametro = 'DT';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("SEP","Si è verificato un errore durante la lettura tabella parametri. Errore sul DB: " + sqlca.sqlerrtext,information!)
	return -1
end if

guo_functions.uof_get_parametro( "DT1", lb_dt1)

if ls_parametro='S' and not lb_dt1 then
	
	select anno_registrazione,
			 num_registrazione,
			 prog_riga_ord_ven
	into   :li_anno_registrazione,
			 :ll_num_reg,
			 :ll_prog_riga_ord_ven
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_commessa = :li_anno_commessa and    
			 num_commessa = :ll_num_commessa;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "SEP", "Si è verificato un errore durante la lettura tabella det_ord_ven. Errore sul DB:" + sqlca.sqlerrtext,information!)
		return -1
	end if

	delete distinte_taglio_calcolate
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_registrazione = :li_anno_registrazione and    
			 num_registrazione = :ll_num_reg and    
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("SEP","Si è verificato un errore durante la cancellazione tabella distinte_taglio_calcolate. Errore sul DB: " + sqlca.sqlerrtext,information!)
		return -1
	end if

end if

tab_dettaglio.det_1.dw_1.change_dw_current()
triggerevent("pc_retrieve")
	
return 0
end function

public function integer wf_disimpegno (integer ai_anno_commessa, long al_num_commessa, ref string as_errore);string				ls_sql, ls_cod_prodotto
datastore		lds_data
long				ll_index, ll_tot
dec{4}			ldd_quantita



ls_sql = 	"select cod_prodotto, quan_impegnata_attuale "+&
			"from impegno_mat_prime_commessa "+&
			"where  cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
			 			"anno_commessa="+string(ai_anno_commessa)+" and "+&
						"num_commessa="+string(al_num_commessa)

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)

if ll_tot<0 then
	return -1
end if

for ll_index = 1 to ll_tot
	ls_cod_prodotto = lds_data.getitemstring(ll_index, 1)
	ldd_quantita = lds_data.getitemdecimal(ll_index, 2)
	
	if ldd_quantita = 0 or isnull(ldd_quantita) then continue  
	
	// tutta la quantità impegnata deve essere disimpegnata
	update impegno_mat_prime_commessa
	set 	 quan_impegnata_attuale = 0
	where  cod_azienda   = :s_cs_xx.cod_azienda and
			 anno_commessa = :ai_anno_commessa and
			 num_commessa  = :al_num_commessa and
			 cod_prodotto  = :ls_cod_prodotto;

	if sqlca.sqlcode < 0 then
		as_errore = "Errore disimpegno MP "+ls_cod_prodotto+" nella commessa : "+ sqlca.sqlerrtext
		return -1
	end if
	
	//detraggo la quantità che era impegnata in anag_prodotti
	update anag_prodotti																		
	set 	 quan_impegnata = quan_impegnata - :ldd_quantita
	where  cod_azienda   = :s_cs_xx.cod_azienda and
			 cod_prodotto  = :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore aggiornamento impegnato MP "+ls_cod_prodotto+" in anagrafica prodotto : "+ sqlca.sqlerrtext
		return -1
	end if

next


return 0
end function

public subroutine wf_annulla_attivazione ();
if g_mb.messagebox("Sep","Tutte le sottocommesse stanno per essere eliminate. Sei Sicuro?",question!,yesno!,2) = 2 then return 

string	ls_errore
long	ll_ret, ll_anno_commessa, ll_num_commessa
uo_funzioni_1 luo_funzioni_1

ll_anno_commessa = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_commessa")
ll_num_commessa = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_commessa")

select count(*)
into	:ll_ret
from	det_anag_comm_anticipi
where cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa and
			num_commessa = :ll_num_commessa;
			
if ll_ret > 0 then
	g_mb.messagebox("SEP","La commessa selezionata ha già alcuni anticipi di spedizioni; impossibile annullare l'attivazione")
	rollback;
	return
end if

luo_funzioni_1 = create uo_funzioni_1

ll_ret = luo_funzioni_1.uof_annulla_attivazione_commessa(	ll_anno_commessa, &
																			ll_num_commessa, &
																			ref ls_errore)

if ll_ret < 0 then
	rollback;
	g_mb.messagebox("SEP", ls_errore)
else
	commit;
end if

destroy luo_funzioni_1

ll_ret = wf_imposta_bottoni()

end subroutine

on w_commesse_produzione_tv.create
int iCurrent
call super::create
this.cbx_forza_numerazione=create cbx_forza_numerazione
this.sle_num_commessa=create sle_num_commessa
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_forza_numerazione
this.Control[iCurrent+2]=this.sle_num_commessa
end on

on w_commesse_produzione_tv.destroy
call super::destroy
destroy(this.cbx_forza_numerazione)
destroy(this.sle_num_commessa)
end on

event pc_setwindow;call super::pc_setwindow;string								ls_wizard, ls_prova, ls_postit, ls_file,ls_expression
s_cs_xx_parametri				lstr_param

is_codice_filtro = "CMP"
iuo_magazzino = create uo_magazzino

tab_dettaglio.det_1.dw_1.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_1.set_dw_key("anno_commessa")
tab_dettaglio.det_1.dw_1.set_dw_key("num_commessa")

//tab_dettaglio.det_1.dw_1.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)
tab_dettaglio.det_1.dw_1.set_dw_options(sqlca, pcca.null_object, c_scrollparent + c_noretrieveonopen, c_default)
tab_dettaglio.det_2.dw_2.set_dw_options(sqlca, pcca.null_object, c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete,c_default)

tab_dettaglio.det_1.dw_1.ib_dw_detail = true
tab_dettaglio.det_2.dw_2.ib_dw_detail = true

iuo_dw_main = tab_dettaglio.det_1.dw_1

il_livello = 0

guo_functions.uof_get_parametro_azienda("GIB", ib_GIB)
tab_dettaglio.det_1.dw_1.object.b_pianificazioni.visible = ib_GIB
tab_dettaglio.det_1.dw_1.object.b_salda.visible = ib_GIB

try
	lstr_param = message.powerobjectparm
	
	if lstr_param.parametro_ul_1>0 and lstr_param.parametro_ul_2 > 0 then
		tab_ricerca.ricerca.dw_ricerca.setitem(1, "anno_commessa", lstr_param.parametro_ul_1)
		tab_ricerca.ricerca.dw_ricerca.setitem(1, "num_commessa", lstr_param.parametro_ul_2)
		
		wf_treeview_search()
	end if
	
	try
		if lstr_param.parametro_s_1<>"" and not isnull(lstr_param.parametro_s_1) then
			this.title = lstr_param.parametro_s_1
		end if
	catch (runtimeerror err2)
	end try
	
catch (runtimeerror err)
end try





end event

event pc_close;call super::pc_close;
destroy iuo_magazzino

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1,"cod_operatore",sqlca,&
                 "tab_operatori","cod_operatore","des_operatore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1,"cod_tipo_commessa",sqlca,&
                 "tab_tipi_commessa","cod_tipo_commessa","des_tipo_commessa",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1,"cod_deposito_prelievo",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N'")

f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1,"cod_deposito_versamento",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N'")

f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1,"cod_centro_costo",sqlca,&
                 "tab_centri_costo","cod_centro_costo","des_centro_costo",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N'")
					  
//Donato 24-09-2009 aggiunti filtri per tipo commessa e operatore
f_PO_LoadDDDW_DW(tab_ricerca.ricerca.dw_ricerca,"cod_operatore",sqlca,&
                 "tab_operatori","cod_operatore","des_operatore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_ricerca.ricerca.dw_ricerca,"cod_tipo_commessa",sqlca,&
                 "tab_tipi_commessa","cod_tipo_commessa","des_tipo_commessa",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//fine modifica -----------------------------------------------------------------------
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_commesse_produzione_tv
integer x = 1463
integer width = 2811
integer height = 2300
det_2 det_2
end type

on tab_dettaglio.create
this.det_2=create det_2
call super::create
this.Control[]={this.det_1,&
this.det_2}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
end on

event tab_dettaglio::selectionchanged;call super::selectionchanged;boolean			lb_visible

if newindex=1 then
	lb_visible = true
else
	lb_visible = false
end if

cbx_forza_numerazione.visible = lb_visible
sle_num_commessa.visible = lb_visible
end event

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 2775
integer height = 2176
string text = "Commessa"
dw_1 dw_1
end type

on det_1.create
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_commesse_produzione_tv
integer width = 1417
integer height = 2372
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer width = 1381
integer height = 2248
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer width = 1367
integer height = 2208
string dataobject = "d_commesse_filtro_tv"
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;string				ls_flag

choose case dwo.name
		
	case "b_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto")
	
	case "b_sel_stato"
		if dwo.text = "Tutti" then
			//vuoi attivare il filtro con tutti gli stati avanzamento
			ls_flag = "S"
			dwo.text = "Indifferente"
		else
			//vuoi disattivare il filtro con tutti gli stati avanzamento
			ls_flag = "N"
			dwo.text = "Tutti"
		end if
		
		dw_ricerca.setitem( 1, "flag_assegnazione", ls_flag)
		dw_ricerca.setitem( 1, "flag_assegnazione_chiusa", ls_flag)
		dw_ricerca.setitem( 1, "flag_attivazione", ls_flag)
		dw_ricerca.setitem( 1, "flag_attivazione_chiusa", ls_flag)
		dw_ricerca.setitem( 1, "flag_automatico", ls_flag)
		dw_ricerca.setitem( 1, "flag_automatico_chiusa", ls_flag)
		dw_ricerca.setitem( 1, "flag_da_attivare", ls_flag)
		
		
end choose
end event

event dw_ricerca::itemchanged;call super::itemchanged;//gestire la disabilitazione e abilitazione dei campi di ricerca flag + altri
//in caso sia valorizzato il filtro per num commessa
datetime			ldt_null
string				ls_nullo

choose case getcolumnname()
	case "num_commessa"
		
		if not isnull(data) and len(data) > 0 and long(data) > 0 then
			this.setitem(1, "flag_assegnazione", "N")
			this.setitem(1, "flag_assegnazione_chiusa", "N")
			this.setitem(1, "flag_attivazione", "N")
			this.setitem(1, "flag_attivazione_chiusa", "N")
			this.setitem(1, "flag_automatico", "N")
			this.setitem(1, "flag_automatico_chiusa", "N")
			this.setitem(1, "flag_da_attivare", "N")
			
			dw_ricerca.object.b_sel_stato.text = "Tutti"
			
			setnull(ldt_null)
			setnull(ls_nullo)
			
			this.setitem(1, "data_reg_da", ldt_null)
			this.setitem(1, "data_reg_a", ldt_null)
			this.setitem(1, "data_consegna_da", ldt_null)
			this.setitem(1, "data_consegna_a", ldt_null)
			
			dw_ricerca.setitem(1, "cod_tipo_commessa", ls_nullo)
			dw_ricerca.setitem(1, "cod_operatore", ls_nullo)
			dw_ricerca.setitem(1, "cod_prodotto", ls_nullo)
		end if
end choose

end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer width = 1381
integer height = 2248
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
integer width = 1371
integer height = 2224
end type

event tv_selezione::itempopulate;call super::itempopulate;long ll_return
treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "N" then
	ll_return =  wf_inserisci_dettagli(handle, int(lstr_data.decimale[1]), long(lstr_data.decimale[2]))
else
	ll_return = wf_leggi_livello(handle, lstr_data.livello + 1)
end if

end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return
il_handle = newhandle
tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data


if istr_data.tipo_livello = "N" then
	//ANAG COMMESSE
	
	//reset det anag commessa relativa
	tab_dettaglio.det_2.dw_2.reset()
	
	//retrieve commessa
	tab_dettaglio.det_1.dw_1.change_dw_current()
	getwindow().triggerevent("pc_retrieve")
	tab_dettaglio.selecttab(1)

elseif istr_data.tipo_livello = "B" then
	//DET ANAG COMMESSE
	
	//retrieve commessa relativa
	tab_dettaglio.det_1.dw_1.change_dw_current()
	getwindow().triggerevent("pc_retrieve")
	
	
	//retrieve det anag commessa relativa
	tab_dettaglio.det_2.dw_2.change_dw_current()
	getwindow().triggerevent("pc_retrieve")
	tab_dettaglio.selecttab(2)
	
end if

	

end event

event tv_selezione::key;call super::key;boolean			ib_anticipo_commessa=false
long				ll_handle,ll_anno_commessa, ll_num_commessa, ll_ret
string 			ls_messaggio, ls_errore, ls_null
datetime			ldt_data_registrazione, ldt_data_commessa
treeviewitem 	ltv_item
str_treeview 	lstr_data
uo_funzioni_1 	luo_funzioni_commesse
uo_magazzino	luo_mag
uo_mrp			luo_mrp

if key = KeyI! and keyflags=3 then
	if g_mb.confirm("Eseguo aggiornamento data fabbisogno per tutte le commesse selezionate?") then
		
		ll_handle = il_handle
		setnull(ls_null)
		
		getitem(ll_handle, ltv_item)
		
		setpointer(HourGlass!)
		luo_mrp = create uo_mrp
		
		do while true
			
			if ll_handle <= 0 or isnull(ll_handle) then exit
			tab_ricerca.selezione.tv_selezione.getitem(ll_handle, ltv_item)
			
			lstr_data = ltv_item.data

			ll_anno_commessa = long(lstr_data.decimale[1])
			ll_num_commessa =  long(lstr_data.decimale[2])
			
			pcca.mdi_frame.setmicrohelp( g_str.format("Elaborazione commessa $1/$2", ll_anno_commessa, ll_num_commessa) )
			Yield()

			if luo_mrp.uof_ricacolo_data_fabbisogno_commessa( ll_anno_commessa, ll_num_commessa, ref ls_errore) < 0 then
				g_mb.error(ls_errore)
				rollback;
				return
			end if
			
			ll_handle = tab_ricerca.selezione.tv_selezione.finditem( NextTreeItem! ,ll_handle )
		loop
		
		destroy luo_mrp
		w_cs_xx_mdi.setmicrohelp("Elaborazione Eseguita Correttamente; calcolo terminato!")
		setpointer(Arrow!)
	end if
end if

if key = KeyX! and keyflags=3 then
	
	
	if g_mb.confirm("Eseguo l'avanzamento automatico per tutte le commesse selezionate?") then
		
		ll_handle = il_handle
		setnull(ls_null)
		
		getitem(ll_handle, ltv_item)
		
		setpointer(HourGlass!)
		
		string		ls_log
		ls_log = guo_functions.uof_get_user_documents_folder( ) + guo_functions.uof_get_random_filename( ) + ".txt"
		
		do while true
			
			if ll_handle <= 0 or isnull(ll_handle) then exit
			tab_ricerca.selezione.tv_selezione.getitem(ll_handle, ltv_item)
			
			lstr_data = ltv_item.data

			ll_anno_commessa = long(lstr_data.decimale[1])
			ll_num_commessa =  long(lstr_data.decimale[2])
			
			
			pcca.mdi_frame.setmicrohelp( g_str.format("Elaborazione commessa $1/$2", ll_anno_commessa, ll_num_commessa) )
			Yield()
			
			luo_funzioni_commesse = create uo_funzioni_1
			
			if luo_funzioni_commesse.uof_chiudi_commessa_fasi( ll_anno_commessa, ll_num_commessa, "RTC", true, ls_log, ref ls_errore) < 0 then
				rollback;
				destroy luo_funzioni_commesse
				g_mb.error(ls_errore)
				return
			else
				destroy luo_funzioni_commesse
				commit;
				//g_mb.show(g_str.format("Commessa $1-$2 chiusa!",ll_anno_commessa, ll_num_commessa))
			end if
			
			Yield()
			ll_handle = tab_ricerca.selezione.tv_selezione.finditem( NextTreeItem! ,ll_handle )
		loop
		
		w_cs_xx_mdi.setmicrohelp("Elaborazione Eseguita Correttamente; calcolo terminato!")
		setpointer(Arrow!)

	end if
	
	/*
	if g_mb.confirm("Eseguo l'avanzamento automatico per tutte le commesse selezionate?") then
		
		ll_handle = il_handle
		setnull(ls_null)
		
		getitem(ll_handle, ltv_item)
		
		setpointer(HourGlass!)
		
		luo_funzioni_commesse = create uo_funzioni_1
		
		do while true
			
			if ll_handle <= 0 or isnull(ll_handle) then exit
			tab_ricerca.selezione.tv_selezione.getitem(ll_handle, ltv_item)
			
			lstr_data = ltv_item.data

			ll_anno_commessa = long(lstr_data.decimale[1])
			ll_num_commessa =  long(lstr_data.decimale[2])
			
			pcca.mdi_frame.setmicrohelp( g_str.format("Elaborazione commessa $1/$2", ll_anno_commessa, ll_num_commessa) )
			Yield()

			select max(tes_bol_ven.data_registrazione)
			into	:ldt_data_registrazione
			from	det_anag_comm_anticipi
					join tes_bol_ven on det_anag_comm_anticipi.cod_azienda = tes_bol_ven.cod_azienda and
											det_anag_comm_anticipi.anno_reg_bol_ven = tes_bol_ven.anno_registrazione and
											det_anag_comm_anticipi.num_reg_bol_ven = tes_bol_ven.num_registrazione 
			where 	det_anag_comm_anticipi.cod_azienda = :s_cs_xx.cod_azienda and
						det_anag_comm_anticipi.anno_commessa = :ll_anno_commessa and
						det_anag_comm_anticipi.num_commessa = :ll_num_commessa;
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("SEP",g_str.format("Errore in ricerca bolla collegata all'anticipo commessa. $1", sqlca.sqlerrtext) )
				rollback;
				return
			end if
			
			if isnull(ldt_data_registrazione) or ldt_data_registrazione <= datetime( date("01/01/1900"),time("00:00:000"))  then
				ib_anticipo_commessa = false
			else
				ib_anticipo_commessa = true
			end if	

			delete		det_anag_comm_anticipi
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						anno_commessa = :ll_anno_commessa and
						num_commessa = :ll_num_commessa;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("SEP","Errore in cancellazione anticipo commessa~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if

			pcca.mdi_frame.setmicrohelp( g_str.format("Annullamento attivazione commessa $1/$2", ll_anno_commessa, ll_num_commessa) )

			ll_ret = luo_funzioni_commesse.uof_annulla_attivazione_commessa(	ll_anno_commessa, &
																									ll_num_commessa, &
																									ref ls_errore)
			if ll_ret < 0 then
				rollback;
				g_mb.messagebox("SEP", ls_errore)
			end if

			pcca.mdi_frame.setmicrohelp( g_str.format("Avanzamento automatico commessa $1/$2", ll_anno_commessa, ll_num_commessa) )
			
			select data_registrazione
			into 	:ldt_data_commessa
			from	anag_commesse
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_commessa = :ll_anno_commessa and
					num_commessa = :ll_num_commessa;
			if sqlca.sqlcode <> 0 then
				rollback;
				g_mb.messagebox("SEP", g_str.format("Commessa $1/$2; impossibile trovare la data di registrazione~r~n" + ls_errore, ll_anno_commessa,ll_num_commessa) )
			end if

			if isnull(ldt_data_commessa) or ldt_data_commessa <= datetime( date("01/01/1900"),time("00:00:000"))  then
				ldt_data_commessa = datetime( date("31/12/" + string(ll_anno_commessa)),time("00:00:000"))
			end if	

			
			luo_funzioni_commesse.idt_data_creazione_mov = ldt_data_commessa

			ll_ret = luo_funzioni_commesse.uof_avanza_commessa(lstr_data.decimale[1], lstr_data.decimale[2], ls_null, ls_errore)
			if ll_ret < 0 then
				rollback;
				g_mb.messagebox("SEP", ls_errore)
			end if
			
			// se l'avanzamento è andato a buon fine ed era stato fatto un anticipo commessa, eseguo movimento di vendita del prodotto 
			
			if ib_anticipo_commessa then
			
				datetime ldt_data_stock[]
				string		ls_cod_prodotto, ls_anno_commessa, ls_cod_deposito[], ls_cod_ubicazione[],ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente[]
				long		ll_prog_stock[], ll_anno_registrazione[], ll_num_registrazione[], ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_null[]
				dec{4}	ldd_quan_prelevata, ld_valore_movimento
			
				ls_anno_commessa = string( long(lstr_data.decimale[1]) )
				setnull(ll_anno_reg_des_mov)
				setnull(ll_num_reg_des_mov)
				setnull(ls_cod_fornitore[1])
				setnull(ls_cod_cliente[1])
				ll_anno_registrazione[] =  ll_null[]
				ll_num_registrazione[] =  ll_null[]
					
				
				pcca.mdi_frame.setmicrohelp( g_str.format("Movimento di scarico del prodotto finito per commessa $1/$2", ll_anno_commessa, ll_num_commessa) )
				// cerco il movimento di versamento del prodotto finito (deve esserci per forza se la commessa è chiusa
				select 	cod_prodotto,
							quan_movimento,
							cod_deposito,
							cod_ubicazione,
							cod_lotto,
							data_stock,
							prog_stock
				into		:ls_cod_prodotto,
							:ldd_quan_prelevata,
							:ls_cod_deposito[1],
							:ls_cod_ubicazione[1],
							:ls_cod_lotto[1],
							:ldt_data_stock[1],
							:ll_prog_stock[1]
				from 		mov_magazzino 
				where 	cod_azienda = :s_cs_xx.cod_azienda and 
							cod_tipo_movimento = 'CPF' and 
							anno_documento = :ll_anno_commessa and
							num_documento = :ll_num_commessa;
							
				if sqlca.sqlcode = 0 then
					
					luo_mag = create uo_magazzino
					
					if f_crea_dest_mov_mag(	"SVE", ls_cod_prodotto, ls_cod_deposito[], & 
														ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
														ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
														ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_errore) = -1 then
						rollback;
						g_mb.messagebox("SEP", g_str.format("Commessa $1/$2; errore in creazione destinazione stock~r~n" + ls_errore, ls_anno_commessa,lstr_data.decimale[2]) )
						return
					end if
				
					if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, "SVE", ls_cod_prodotto) = -1 then
					
						g_mb.messagebox("SEP", g_str.format("Commessa $1/$2; errore in verifica destinazione magazzino~r~n" + ls_errore, ls_anno_commessa,lstr_data.decimale[2]) )
					 	rollback;
					end if
			
					ll_ret = luo_mag.uof_movimenti_mag(ldt_data_registrazione, &
																	  "SVE", &
																	  "N", &
																	  ls_cod_prodotto, &
																	  ldd_quan_prelevata, &
																	  1, &
																	  long(lstr_data.decimale[2]), &
																	  ldt_data_registrazione, &
																	  ls_anno_commessa, &
																	  ll_anno_reg_des_mov, &
																	  ll_num_reg_des_mov, &
																	  ls_cod_deposito[], &
																	  ls_cod_ubicazione[], &
																	  ls_cod_lotto[], &
																	  ldt_data_stock[], &
																	  ll_prog_stock[], &
																	  ls_cod_fornitore[], &
																	  ls_cod_cliente[], &
																	  ll_anno_registrazione[], &
																	  ll_num_registrazione[], &
																	  ls_errore)
											  
					if ll_ret < 0 then
						if ls_errore="" or isnull(ls_errore) then
							ls_errore = "Errore su movimenti magazzino, prodotto: " + ls_cod_prodotto &
										 + ", deposito:" + ls_cod_deposito[1] + ", ubicazione:" + ls_cod_ubicazione[1] &
										 + ", cod_lotto:" + ls_cod_lotto[1] + ", data_stock:" + string(ldt_data_stock[1]) &
										 + ", prog_stock:" + string(ll_prog_stock[1])
						end if
						
						rollback;
						g_mb.messagebox("SEP", ls_errore)
						
					end if		
					destroy luo_mag
				end if
			else
				//g_mb.messagebox("SEP", g_str.format("Commessa $1/$2 impossibile trovare il movimento di carico prodotto finito",long(lstr_data.decimale[1]) ,long(lstr_data.decimale[2]) ))
			end if
			
			commit;
			
			Yield()
			
			ll_handle = tab_ricerca.selezione.tv_selezione.finditem( NextTreeItem! ,ll_handle )

		loop
		destroy luo_funzioni_commesse
		w_cs_xx_mdi.setmicrohelp("Elaborazione Eseguita Correttamente; calcolo terminato!")
		setpointer(Arrow!)

	end if
*/
end if
end event

type dw_1 from uo_cs_xx_dw within det_1
event ue_impegno_nuova_commessa ( )
event ue_forza_chiusura ( )
integer x = 5
integer y = 12
integer width = 2766
integer height = 2160
integer taborder = 30
string dataobject = "d_commesse_prod_1"
boolean border = false
end type

event ue_impegno_nuova_commessa();//evento chiamatao alla creazione della commessa


string					ls_cod_prodotto, ls_cod_versione, ls_errore, ls_cod_tipo_commessa, ls_flag_tipo_impegno_mp
long					ll_anno_commessa, ll_num_commessa
dec{4}				ldd_quan_in_produzione, ldd_quan_assegnata, ldd_quan_prodotta, ldd_quan_in_ordine, ldd_quan_impegnata
		
ll_anno_commessa = getitemnumber(getrow(), "anno_commessa")
ll_num_commessa  = getitemnumber(getrow(), "num_commessa")
ls_cod_prodotto  = getitemstring(getrow(), "cod_prodotto")
ls_cod_versione  = getitemstring(getrow(), "cod_versione")
ldd_quan_in_produzione = getitemnumber(getrow(), "quan_in_produzione")
ldd_quan_assegnata = getitemnumber(getrow(), "quan_assegnata")
ldd_quan_prodotta  = getitemnumber(getrow(), "quan_prodotta")
ldd_quan_in_ordine = getitemnumber(getrow(), "quan_ordine")

//	***		Michela 04/06/2007: controllo il tipo commessa e vedo se fare l'impegno delle materie prime all'attivazione della commessa 
//											o alla creazione della commessa

ls_cod_tipo_commessa = getitemstring( getrow(), "cod_tipo_commessa")

//S = Alla creazione della commessa
//N = All'attivazione della commessa
select flag_tipo_impegno_mp
into	:ls_flag_tipo_impegno_mp
from	tab_tipi_commessa
where	cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	//dai il messaggio di errore
	g_mb.error("Errore durante il tipo di impegno delle materie prime nel tipo commessa:" + sqlca.sqlerrtext)
	g_mb.error("Impegno non eseguito!")
	
elseif sqlca.sqlcode = 0 and not isnull(ls_flag_tipo_impegno_mp) and ls_flag_tipo_impegno_mp = "S" then
	//S = Alla creazione della commessa dev9o impegnare le materie prime, come stabilito dal tipo commessa

	// impegno le materie prime.	
	if iuo_magazzino.uof_impegna_mp_commessa(	true, &
															false, &
															ls_cod_prodotto, &
															ls_cod_versione, &
															ldd_quan_in_ordine, &
															ll_anno_commessa, &
															ll_num_commessa, &
															ls_errore) = -1 then
		g_mb.error("Errore durante l'impegno~r~n" + ls_errore)
		rollback;
		return
	end if	
else
	//dai un alert circa l'impegno non eseguito poichè il tipo commessa non lo prevede		
	g_mb.warning("Impegno non eseguito, poichè il tipo commessa prevede l'impegno all'attivazione della commessa!")
end if


commit;
end event

event ue_forza_chiusura();boolean			ib_anticipo_commessa=false
long				ll_handle,ll_anno_commessa, ll_num_commessa, ll_ret, ll_prog_stock[], ll_anno_registrazione[], ll_num_registrazione[], ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_null[]
dec{4}			ldd_quan_prelevata, ld_valore_movimento
string 			ls_messaggio, ls_errore, ls_null, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[],ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente[]
datetime			ldt_data_registrazione, ldt_data_commessa,ldt_data_stock[]
uo_funzioni_1 	luo_funzioni_commesse
uo_magazzino	luo_mag
uo_mrp			luo_mrp

setnull(ls_null)
ll_anno_commessa = getitemnumber( getrow(), "anno_commessa")
ll_num_commessa = getitemnumber( getrow(), "num_commessa")

if not g_mb.confirm(g_str.format("Procedo con la chiusura forzata della commessa $1/$2? L'operazione è irreversibile.",ll_anno_commessa, ll_num_commessa)) then return
		
luo_funzioni_commesse = create uo_funzioni_1


pcca.mdi_frame.setmicrohelp( g_str.format("Elaborazione commessa $1/$2", ll_anno_commessa, ll_num_commessa) )
Yield()

select max(tes_bol_ven.data_registrazione)
into	:ldt_data_registrazione
from	det_anag_comm_anticipi
		join tes_bol_ven on det_anag_comm_anticipi.cod_azienda = tes_bol_ven.cod_azienda and
								det_anag_comm_anticipi.anno_reg_bol_ven = tes_bol_ven.anno_registrazione and
								det_anag_comm_anticipi.num_reg_bol_ven = tes_bol_ven.num_registrazione 
where 	det_anag_comm_anticipi.cod_azienda = :s_cs_xx.cod_azienda and
			det_anag_comm_anticipi.anno_commessa = :ll_anno_commessa and
			det_anag_comm_anticipi.num_commessa = :ll_num_commessa;
if sqlca.sqlcode < 0 then
	rollback;
	g_mb.messagebox("SEP",g_str.format("Errore in ricerca bolla collegata all'anticipo commessa. $1", sqlca.sqlerrtext) )
	return
end if

if isnull(ldt_data_registrazione) or ldt_data_registrazione <= datetime( date("01/01/1900"),time("00:00:000"))  then
	ib_anticipo_commessa = false
else
	ib_anticipo_commessa = true
end if	

delete		det_anag_comm_anticipi
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa and
			num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SEP","Errore in cancellazione anticipo commessa~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

pcca.mdi_frame.setmicrohelp( g_str.format("Annullamento attivazione commessa $1/$2", ll_anno_commessa, ll_num_commessa) )

ll_ret = luo_funzioni_commesse.uof_annulla_attivazione_commessa(	ll_anno_commessa, &
																						ll_num_commessa, &
																						ref ls_errore)
if ll_ret < 0 then
	rollback;
	g_mb.messagebox("SEP", ls_errore)
end if

pcca.mdi_frame.setmicrohelp( g_str.format("Avanzamento automatico commessa $1/$2", ll_anno_commessa, ll_num_commessa) )

select data_registrazione
into 	:ldt_data_commessa
from	anag_commesse
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_commessa = :ll_anno_commessa and
		num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	rollback;
	g_mb.messagebox("SEP", g_str.format("Commessa $1/$2; impossibile trovare la data di registrazione~r~n" + ls_errore, ll_anno_commessa,ll_num_commessa) )
end if

if isnull(ldt_data_commessa) or ldt_data_commessa <= datetime( date("01/01/1900"),time("00:00:000"))  then
	ldt_data_commessa = datetime( date("31/12/" + string(ll_anno_commessa)),time("00:00:000"))
end if	


luo_funzioni_commesse.idt_data_creazione_mov = ldt_data_commessa

ll_ret = luo_funzioni_commesse.uof_avanza_commessa(ll_anno_commessa, ll_num_commessa, ls_null, ls_errore)
if ll_ret < 0 then
	rollback;
	g_mb.messagebox("SEP", ls_errore)
end if

// se l'avanzamento è andato a buon fine ed era stato fatto un anticipo commessa, eseguo movimento di vendita del prodotto 

if ib_anticipo_commessa then

	setnull(ll_anno_reg_des_mov)
	setnull(ll_num_reg_des_mov)
	setnull(ls_cod_fornitore[1])
	setnull(ls_cod_cliente[1])
	ll_anno_registrazione[] =  ll_null[]
	ll_num_registrazione[] =  ll_null[]
		
	
	pcca.mdi_frame.setmicrohelp( g_str.format("Movimento di scarico del prodotto finito per commessa $1/$2", ll_anno_commessa, ll_num_commessa) )
	// cerco il movimento di versamento del prodotto finito (deve esserci per forza se la commessa è chiusa
	select 	cod_prodotto,
				quan_movimento,
				cod_deposito,
				cod_ubicazione,
				cod_lotto,
				data_stock,
				prog_stock
	into		:ls_cod_prodotto,
				:ldd_quan_prelevata,
				:ls_cod_deposito[1],
				:ls_cod_ubicazione[1],
				:ls_cod_lotto[1],
				:ldt_data_stock[1],
				:ll_prog_stock[1]
	from 		mov_magazzino 
	where 	cod_azienda = :s_cs_xx.cod_azienda and 
				cod_tipo_movimento = 'CPF' and 
				anno_documento = :ll_anno_commessa and
				num_documento = :ll_num_commessa;
				
	if sqlca.sqlcode = 0 then
		
		luo_mag = create uo_magazzino
		
		if f_crea_dest_mov_mag(	"SVE", ls_cod_prodotto, ls_cod_deposito[], & 
											ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
											ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
											ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_errore) = -1 then
			rollback;
			g_mb.messagebox("SEP", g_str.format("Commessa $1/$2; errore in creazione destinazione stock~r~n" + ls_errore, ll_anno_commessa,ll_num_commessa) )
			return
		end if
	
		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, "SVE", ls_cod_prodotto) = -1 then
			rollback;
			g_mb.messagebox("SEP", g_str.format("Commessa $1/$2; errore in verifica destinazione magazzino~r~n" + ls_errore, ll_anno_commessa,ll_num_commessa) )
		end if

		ll_ret = luo_mag.uof_movimenti_mag(ldt_data_registrazione, &
														  "SVE", &
														  "N", &
														  ls_cod_prodotto, &
														  ldd_quan_prelevata, &
														  1, &
														  long(ll_num_commessa), &
														  ldt_data_registrazione, &
														  string(ll_anno_commessa), &
														  ll_anno_reg_des_mov, &
														  ll_num_reg_des_mov, &
														  ls_cod_deposito[], &
														  ls_cod_ubicazione[], &
														  ls_cod_lotto[], &
														  ldt_data_stock[], &
														  ll_prog_stock[], &
														  ls_cod_fornitore[], &
														  ls_cod_cliente[], &
														  ll_anno_registrazione[], &
														  ll_num_registrazione[], &
														  ls_errore)
								  
		if ll_ret < 0 then
			if ls_errore="" or isnull(ls_errore) then
				ls_errore = "Errore su movimenti magazzino, prodotto: " + ls_cod_prodotto &
							 + ", deposito:" + ls_cod_deposito[1] + ", ubicazione:" + ls_cod_ubicazione[1] &
							 + ", cod_lotto:" + ls_cod_lotto[1] + ", data_stock:" + string(ldt_data_stock[1]) &
							 + ", prog_stock:" + string(ll_prog_stock[1])
			end if
			
			rollback;
			g_mb.messagebox("SEP", ls_errore)
			
		end if		
		destroy luo_mag
	end if
else
	//g_mb.messagebox("SEP", g_str.format("Commessa $1/$2 impossibile trovare il movimento di carico prodotto finito",long(ll_anno_commessa) ,long(ll_num_commessa) ))
end if

commit;
	
destroy luo_funzioni_commesse
w_cs_xx_mdi.setmicrohelp(g_str.format("Chiusura commessa $1/$2 eseguita correttamente", ll_anno_commessa, ll_num_commessa))

end event

event pcd_retrieve;call super::pcd_retrieve;

long ll_errore

if not isvalid(istr_data) or isnull(istr_data) or UpperBound(istr_data.decimale) < 2 then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2])

if ll_errore < 0 then
   pcca.error = c_fatal
end if

wf_imposta_bottoni()

change_dw_current()
end event

event buttonclicked;call super::buttonclicked;long					ll_riga, ll_num_commessa, ll_ret
integer				li_anno_commessa
string					ls_appo, ls_cod_prodotto, ls_cod_versione, ls_errore
uo_mrp				luo_mrp
datetime				ldt_oggi
uo_log_sistema		luo_log


choose case dwo.name
		
	case "b_chiudi_forzatamente"
		//event post ue_forza_chiusura( )
		
	case "b_salda"
		ll_riga = getrow()
		if ll_riga < 1 then return
		li_anno_commessa = getitemnumber( ll_riga, "anno_commessa")
		ll_num_commessa = getitemnumber( ll_riga, "num_commessa")
		
		if li_anno_commessa>0 and ll_num_commessa>0 then
			if g_mb.confirm("Saldare la commessa "+string(li_anno_commessa)+"/"+string(ll_num_commessa)+" ? Se confermi la commessa sarà chiusa senza altri movimenti...") then
				
				if wf_disimpegno(li_anno_commessa, ll_num_commessa, ls_errore) <0 then
					//errore in disimpegno quantità residue
					rollback;
					g_mb.error(ls_errore)
					return
				else
					ldt_oggi = datetime(today(), 00:00:00)
				
					//chiudo
					update anag_commesse
					set 	data_chiusura=:ldt_oggi,
							flag_tipo_avanzamento='7'
					where 	cod_azienda=:s_cs_xx.cod_azienda and
								anno_commessa=:li_anno_commessa and
								num_commessa=:ll_num_commessa;
				
					if sqlca.sqlcode<0 then
						rollback;
						g_mb.error("Errore in saldo commessa: "+sqlca.sqlerrtext)
						return
					else
						commit;
						
						//metto in log sistema
						luo_log = create uo_log_sistema
						luo_log.uof_write_log_sistema_not_sqlca("SALDOCOM", "Saldo effettuato su commessa "+string(li_anno_commessa)+"/"+string(ll_num_commessa))
						destroy luo_log	
						
						return
					end if
				end if
			end if
		end if
		
		
	case "b_pianificazioni"
		window_open(w_buffer_chiusura_commesse, -1)
		
		
	case "b_report"
		ll_riga = getrow()
		if ll_riga < 1 then return
		li_anno_commessa = getitemnumber( ll_riga, "anno_commessa")
		ll_num_commessa = getitemnumber( ll_riga, "num_commessa")
		
		s_cs_xx.parametri.parametro_d_1 = li_anno_commessa
		s_cs_xx.parametri.parametro_d_2 = ll_num_commessa
		s_cs_xx.parametri.parametro_d_3 = 0			//se 6969 allora stampa subito
		window_open(w_report_commessa_prod, -1)
		
		
	case "b_prodotto"
		
		ls_appo = dw_1.Describe("b_prodotto.Enabled")
		if Upper(ls_appo) = "0" then return
		
		dw_1.change_dw_current()
		setnull(s_cs_xx.parametri.parametro_uo_dw_search)
		guo_ricerca.uof_ricerca_prodotto(dw_1,"cod_prodotto")
		
		
	case "b_costi_correttivi"
		
		ls_appo = dw_1.Describe("b_costi_correttivi.Enabled")
		if Upper(ls_appo) = "0" then return
		
		ll_riga = getrow()
		if ll_riga < 1 then return
		li_anno_commessa = getitemnumber( ll_riga, "anno_commessa")
		ll_num_commessa = getitemnumber( ll_riga, "num_commessa")
		if isnull(li_anno_commessa) or li_anno_commessa = 0 or isnull(ll_num_commessa) or ll_num_commessa = 0 then return
		window_open_parm(w_costi_correttivi,-1,dw_1)
		
		
	case "b_attiva"
		
		ls_appo = dw_1.Describe("b_attiva.Enabled")
		if Upper(ls_appo) = "0" then return
		
		wf_attivazione()
		
		
	case "b_annulla_attiva"
		
		ls_appo = dw_1.Describe("b_annulla_attiva.Enabled")
		if Upper(ls_appo) = "0" then return
		
		wf_annulla_attivazione()
		
		
	case "b_varianti"
		
		ls_appo = dw_1.Describe("b_varianti.Enabled")
		if Upper(ls_appo) = "0" then return
		
		s_cs_xx.parametri.parametro_dw_1 = dw_1
		window_open(w_varianti_commesse,-1)
		
		
	case "b_chiudi"
		
		ls_appo = dw_1.Describe("b_chiudi.Enabled")
		if Upper(ls_appo) = "0" then return
		
		wf_chiudi_commessa()
		
		
	case "b_rda"
		
		ls_appo = dw_1.Describe("b_rda.Enabled")
		if Upper(ls_appo) = "0" then return
		
		window_open_parm(w_commesse_rda,-1,dw_1)
		
	case "b_ordini"
		
		ls_appo = dw_1.Describe("b_ordini.Enabled")
		if Upper(ls_appo) = "0" then return
		
		window_open_parm(w_commesse_ordini,-1,dw_1)		
		
		
	case "b_congela"
		
		ll_riga = getrow()
		if ll_riga < 1 then return
		li_anno_commessa = getitemnumber( ll_riga, "anno_commessa")
		ll_num_commessa = getitemnumber( ll_riga, "num_commessa")
		if isnull(li_anno_commessa) or li_anno_commessa = 0 or isnull(ll_num_commessa) or ll_num_commessa = 0 then return		
		ls_cod_prodotto = getitemstring( ll_riga, "cod_prodotto")
		if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then return
		ls_cod_versione = getitemstring( ll_riga, "cod_versione")
		if isnull(ls_cod_versione) or ls_cod_versione = "" then return
		
		luo_mrp = create uo_mrp
		
		ll_ret = luo_mrp.uof_congela_distinta( ls_cod_prodotto, ls_cod_versione, li_anno_commessa, ll_num_commessa, 0, "varianti_commesse", ls_errore)
		
		if ll_ret = 0 then
			g_mb.messagebox( "SEP", "Distinta memorizzata con successo!")
			destroy luo_mrp;
			commit;
		else
			g_mb.messagebox( "SEP", ls_errore)
			destroy luo_mrp;
			rollback;
		end if


	case "b_impegnato"
		
		ls_appo = dw_1.Describe("b_impegnato.Enabled")
		if Upper(ls_appo) = "0" then return
		
		window_open_parm(w_commesse_impegnato,-1,dw_1)		
		
end choose
end event

event pcd_setkey;call super::pcd_setkey;long			ll_index, ll_num_commessa,ll_test, ll_num_commessa_forzato
integer		li_anno_commessa, li_risposta



li_anno_commessa = f_anno_esercizio()

for ll_index = 1 to rowcount()
	
   if isnull(getitemstring(ll_index, "cod_azienda")) then
      setitem(ll_index, "cod_azienda", s_cs_xx.cod_azienda)
   end  if

   if IsNull(getitemnumber(ll_index, "anno_commessa")) or getitemnumber(ll_index, "anno_commessa") < 1 then
		setItem(ll_index, "anno_commessa", li_anno_commessa)
	end if
	
   if isnull(getitemnumber(ll_index, "num_commessa")) or getitemnumber(ll_index, "num_commessa") < 1 then
		
		if cbx_forza_numerazione.checked=true then
			//numero commessa forzato dall'operatore
			ll_num_commessa_forzato = long(sle_num_commessa.text)
		
			//controllo se il numero commessa desiderato non è già utilizzato
			select num_commessa
			into   :ll_test
			from   anag_commesse
			where	cod_azienda=:s_cs_xx.cod_azienda and
						anno_commessa=:li_anno_commessa and
						num_commessa=:ll_num_commessa_forzato;
			
			if sqlca.sqlcode=0 then
				//già usato
				g_mb.warning(	"Il numero richiesto " + string(ll_num_commessa_forzato) + &
									" è già utilizzato da un'altra commessa!")
			else
				//OK puoi prenderlo
				ll_num_commessa = ll_num_commessa_forzato
			end if
		else
			//calcola il numero commessa come MAX + 1
			setnull(ll_num_commessa)
		
			select max(num_commessa)
			into   :ll_num_commessa
			from   anag_commesse
			where	cod_azienda = :s_cs_xx.cod_azienda and
						anno_commessa= :li_anno_commessa;
	
			if isnull(ll_num_commessa) then ll_num_commessa = 0
			ll_num_commessa += 1
		end if
			
		setItem(ll_index, "num_commessa", ll_num_commessa)
	end if
next

end event

event itemchanged;call super::itemchanged;string		ls_cod_deposito_versamento,ls_cod_deposito_prelievo,ls_cod_tipo_mov_prel_mat_prime, &
			 ls_cod_tipo_mov_ver_prod_finiti,ls_cod_tipo_mov_reso_mat_prime,ls_cod_tipo_mov_reso_semilav, &
			 ls_cod_tipo_mov_sfrido_mat_prime,ls_cod_tipo_mov_sfrido_semilav, ls_cod_versione


choose case i_colname
	//-------------------------------------------------------------------------------------------------------------------------------------------
	case  "cod_tipo_commessa"
		if data<>"" then

			select	cod_deposito_versamento,
						cod_deposito_prelievo,
						cod_tipo_mov_prel_mat_prime,
						cod_tipo_mov_ver_prod_finiti,
						cod_tipo_mov_reso_mat_prime,
						cod_tipo_mov_reso_semilav,
						cod_tipo_mov_sfrido_mat_prime,
						cod_tipo_mov_sfrido_semilav
			into	:ls_cod_deposito_versamento,
					:ls_cod_deposito_prelievo,
					:ls_cod_tipo_mov_prel_mat_prime,
					:ls_cod_tipo_mov_ver_prod_finiti,
					:ls_cod_tipo_mov_reso_mat_prime,
					:ls_cod_tipo_mov_reso_semilav,
					:ls_cod_tipo_mov_sfrido_mat_prime,
					:ls_cod_tipo_mov_sfrido_semilav
			from   tab_tipi_commessa
			where	cod_azienda =:s_cs_xx.cod_azienda and
						cod_tipo_commessa =:data;
 
		if isnull(ls_cod_tipo_mov_prel_mat_prime) or isnull(ls_cod_tipo_mov_ver_prod_finiti) or &
			isnull(ls_cod_tipo_mov_reso_mat_prime) or isnull(ls_cod_tipo_mov_reso_semilav) or &
			isnull(ls_cod_tipo_mov_sfrido_mat_prime) or isnull(ls_cod_tipo_mov_sfrido_semilav) then
			
			g_mb.warning("Il tipo commessa selezionato ha uno o più tipi movimento non impostato, questo provoca dei problemi in fase di avanzamento produzione. "+&
								"Verificare il tipo commessa e assegnare gli opportuni movimenti.")
		end if

	else
		setnull(ls_cod_deposito_versamento)
		setnull(ls_cod_deposito_prelievo)
	end if

	setitem(row,"cod_deposito_versamento",ls_cod_deposito_versamento)
	setitem(row,"cod_deposito_prelievo",ls_cod_deposito_prelievo)


	//-------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_prodotto"
		if data<>"" then
			f_PO_LoadDDDW_DW(dw_1,"cod_versione",sqlca,&
								  "distinta_padri","cod_versione","des_versione",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + data +"'")
			select cod_versione
			into   :ls_cod_versione
			from   distinta_padri
			where	cod_azienda=:s_cs_xx.cod_azienda and
						cod_prodotto=:data and
						flag_predefinita='S';

			if ls_cod_versione="" then setnull(ls_cod_versione)

		//else
			//setnull(ls_cod_versione)
		end if

		setitem(row,"cod_versione",ls_cod_versione)
		
end choose
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	integer li_risposta
	li_risposta = wf_imposta_bottoni()
	this.object.b_attiva.enabled=false
	this.object.b_chiudi.enabled=false
	this.object.b_prodotto.enabled=true
	this.object.b_varianti.enabled=false
	this.object.b_costi_correttivi.enabled = false
	
	this.object.b_report.enabled = false
	this.object.b_salda.enabled = false
	
end if
end event

event pcd_new;call super::pcd_new;integer				li_risposta
string					ls_cod_operatore



setitem(getrow(),"flag_tassativo","N")

if i_extendmode then
	
	li_risposta = wf_imposta_bottoni()
	this.object.b_attiva.enabled = false
	this.object.b_chiudi.enabled = false
	this.object.b_prodotto.enabled = true
	this.object.b_varianti.enabled = false
	this.object.b_costi_correttivi.enabled = false
	
	this.object.b_report.enabled = false
	this.object.b_salda.enabled = false

	setitem(getrow(), "data_registrazione", datetime(today(), 00:00:00))

	ls_cod_operatore = guo_functions.uof_get_operatore_utente(s_cs_xx.cod_utente)
	if not isnull(ls_cod_operatore) and ls_cod_operatore<>"" then
		setitem(getrow(), "cod_operatore", ls_cod_operatore)
	end if

end if
end event

event pcd_validaterow;call super::pcd_validaterow;long							ll_num_dettagli, ll_num_commessa
string							ls_test,ls_cod_prodotto
dwItemStatus				l_status
string							ls_column_name, ls_count, ls_col_index
integer						li_index, ll_anno_commessa
datetime						ldt_data_consegna


ls_cod_prodotto=getitemstring(getrow(),"cod_prodotto")

select cod_prodotto_padre
into   :ls_test
from   distinta
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto_padre=:ls_cod_prodotto;

if ls_test="" or isnull(ls_test) then
	g_mb.warning("Attenzione! Il prodotto selezionato non ha una distinta base associata!")
	pcca.error=c_valfailed
	return 
end if

// stefanop: 20/04/2012: Imposto il data_consegna come campo obbligatorio
// altrimenti al successivo salvataggio da un errore di "Data consegna mancante per la commessa XXX/YYY"
// perchè durante l'updatestart chiama la funzione uof_impegna_mp_commessa.
// -------------------------------------------------------------------------------------------
ldt_data_consegna = getitemdatetime(getrow(), "data_consegna")
if isnull(ldt_data_consegna) then
	g_mb.error("Attenzione! Inserire una data consegna valida")
	pcca.error=c_valfailed
	setcolumn("data_consegna")
	return 
end if
// -------------------------------------------------------------------------------------------
	

ll_anno_commessa = this.getitemnumber(this.getrow(), "anno_commessa")
ll_num_commessa = this.getitemnumber(this.getrow(), "num_commessa")

ll_num_dettagli = 0

select count(*)  
into  :ll_num_dettagli  
from  det_anag_commesse
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_commessa = :ll_anno_commessa and
		num_commessa = :ll_num_commessa;

if ll_num_dettagli > 0 then
	
	//INIZIO MODIFICA --------------------------------------
	//Donato 16/12/2008 La funzione 1 della specifica unifast Report_produzione deve consentire
	//all'utente di ricalcolare il fabbisogno dopo aver modificato la data consegna
	//solo che nessuna modifica alla commessa è consentita se questa è già attiva
	//QUINDI SE L'UNICO CAMPO MODIFICATO è la data di consegna allora consenti di salvare
	ls_count = this.Describe("DataWindow.Column.Count")
	for li_index = 1 to integer(ls_count)
		ls_col_index = "#"+string(li_index)
		ls_column_name =this.Describe(ls_col_index+".name")
		l_status = this.GetItemStatus(this.getrow(), ls_column_name, Primary!)
		
		if l_status=DataModified! and ls_column_name<>"data_consegna" then
			//c'è una colonna modificata e questa non è la data consegna
			//non permettere di modificare
			g_mb.messagebox("Attivazione Commesse","Commessa già Attivata, Non si può modificare")
			pcca.error = c_fatal
		end if
	next	
	
	//codice commentato
	//g_mb.messagebox("Attivazione Commesse","Commessa già Attivata, Non si può modificare")
	//pcca.error = c_fatal
	//fine modifica --------------------------------
end if	


end event

event pcd_view;call super::pcd_view;if i_extendmode then	
	wf_imposta_bottoni()
	
	this.object.b_report.enabled = true
	this.object.b_prodotto.enabled = false
	this.object.b_salda.enabled = true
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	integer				li_risposta, li_anno_bolla,ll_anno_ordine, ll_anno_commessa
	long   				ll_num_ordine, ll_num_commessa, ll_num_bolla, ll_prog_riga_bolla, ll_row, ll_prog_riga_ord_ven, ll_ret
	datetime				ldt_data_pianificazione
	string					ls_cod_cliente, ls_rag_soc_1

	ll_row = getrow()
	
	if ll_row = 0 then return

	ll_anno_commessa = getitemnumber(ll_row,"anno_commessa")
	ll_num_commessa = getitemnumber(ll_row,"num_commessa")
	
	f_PO_LoadDDDW_DW(dw_1,"cod_versione",sqlca,&
  	              	  "distinta_padri","cod_versione","des_versione",&
      	           "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + getitemstring(ll_row,"cod_prodotto") +"'")
	
	setnull(ldt_data_pianificazione)
	
	select data_pianificazione
	into :ldt_data_pianificazione
	from anag_commesse
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_commessa=:ll_anno_commessa and
				num_commessa=:ll_num_commessa;
	
	if sqlca.sqlcode=0 and not isnull(ldt_data_pianificazione) and year(date(ldt_data_pianificazione))>2000 then
		this.object.t_pianificazione.text = string(ldt_data_pianificazione, "dd/mm/yyyy hh:mm")
	else
		this.object.t_pianificazione.text = ""
	end if
	
	select det_ord_ven.anno_registrazione,
			 det_ord_ven.num_registrazione,
			 det_ord_ven.prog_riga_ord_ven,
			 tes_ord_ven.cod_cliente,
			 anag_clienti.rag_soc_1
	into	:ll_anno_ordine,
			:ll_num_ordine,
			:ll_prog_riga_ord_ven,
			:ls_cod_cliente,
			:ls_rag_soc_1
	from   det_ord_ven
	join tes_ord_ven on 	tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda and
								tes_ord_ven.anno_registrazione=det_ord_ven.anno_registrazione and
								tes_ord_ven.num_registrazione=det_ord_ven.num_registrazione
	join anag_clienti on 	anag_clienti.cod_azienda=tes_ord_ven.cod_azienda and
								anag_clienti.cod_cliente=tes_ord_ven.cod_cliente
	where  det_ord_ven.cod_azienda=:s_cs_xx.cod_azienda
	and    det_ord_ven.anno_commessa=:ll_anno_commessa
	and    det_ord_ven.num_commessa=:ll_num_commessa;

	if sqlca.sqlcode < 0 then
		g_mb.error("Errore sul DB:" + sqlca.sqlerrtext)
		return
	end if

	if sqlca.sqlcode <> 100 then
		ll_ret = guo_functions.uof_replace_string( ls_rag_soc_1, char(34), char(126) + char(34))
		this.object.t_ordine.text = string( ll_anno_ordine) + " / " + string(ll_num_ordine) + " / " + string(ll_prog_riga_ord_ven) + "   -   " + ls_cod_cliente + " " + ls_rag_soc_1
										
		
	else
		this.object.t_ordine.text = ""
		
		//Donato 26/07/2012 se arrivi qui, vuol dire che la commessa non è associata a nessun ordine di vendita
		//prova a vedere se la commessa è relativa ad un ddt vendita, e se si allora mostralo
		select	det_bol_ven.anno_registrazione,
				det_bol_ven.num_registrazione,
				det_bol_ven.prog_riga_bol_ven,
				tes_bol_ven.cod_cliente,
				anag_clienti.rag_soc_1
		into		:li_anno_bolla,
					:ll_num_bolla,
					:ll_prog_riga_bolla,
					:ls_cod_cliente,
					:ls_rag_soc_1
		from   det_bol_ven
		join tes_bol_ven on 	tes_bol_ven.cod_azienda=det_bol_ven.cod_azienda and
									tes_bol_ven.anno_registrazione=det_bol_ven.anno_registrazione and
									tes_bol_ven.num_registrazione=det_bol_ven.num_registrazione
		left outer join anag_clienti on 	anag_clienti.cod_azienda=tes_bol_ven.cod_azienda and
												anag_clienti.cod_cliente=tes_bol_ven.cod_cliente
		where  det_bol_ven.cod_azienda=:s_cs_xx.cod_azienda
		and    det_bol_ven.anno_commessa=:ll_anno_commessa
		and    det_bol_ven.num_commessa=:ll_num_commessa;
		
		this.object.t_bolla.text = ""
		
		if sqlca.sqlcode = 0 and li_anno_bolla>0 and ll_num_bolla>0 and ll_prog_riga_bolla>0 then
			
			if isnull(ls_cod_cliente) then ls_cod_cliente = ""
			if isnull(ls_rag_soc_1) then 
				ls_rag_soc_1 = ""
			else
				ll_ret = guo_functions.uof_replace_string( ls_rag_soc_1, '"', '~"')
			end if

			this.object.t_bolla.text =  string( li_anno_bolla) + " / " + string(ll_num_bolla)+" / "+string(ll_prog_riga_bolla) + &
										"   -   " + ls_cod_cliente + " " + ls_rag_soc_1
		end if
		//--------------------------------------------------------------------------------------------------------------------
	end if
end if
end event

event ue_key;call super::ue_key;choose case this.getcolumnname()

	case "cod_ricambio"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_1,"cod_prodotto")	
		end if
end choose
end event

event updatestart;call super::updatestart;if i_extendmode then
	string  ls_cod_prodotto, ls_cod_versione, ls_cod_prodotto_old, ls_cod_versione_old,ls_cod_tipo_commessa,&
			  ls_flag_muovi_mp,ls_parametro,ls_messaggio,ls_errore, ls_flag_tipo_impegno_mp
	dec{6}  ldd_quan_assegnata,ldd_quan_prodotta,ldd_quan_in_ordine, &
			  ldd_quan_impegnata,ldd_quan_in_produzione, &
			  ldd_quan_assegnata_old, ldd_quan_prodotta_old,ldd_quan_in_ordine_old, &
			  ldd_quan_impegnata_old,ldd_quan_in_produzione_old
	long    ll_i,ll_anno_commessa,ll_num_commessa,ll_null, ll_num_reg,ll_prog_riga_ord_ven, ll_dettagli
	integer li_anno_registrazione
	
	for ll_i = 1 to rowcount()
		
		// se non è cambiato nulla salta tutto.
		if getitemstatus(ll_i, 0, primary!) <> DataModified! and getitemstatus(ll_i, 0,primary!) <>  NewModified!	then continue
		
		ll_anno_commessa = getitemnumber(ll_i, "anno_commessa")
		ll_num_commessa  = getitemnumber(ll_i, "num_commessa")
		ls_cod_prodotto  = getitemstring(ll_i, "cod_prodotto")
		ls_cod_tipo_commessa = getitemstring(ll_i, "cod_tipo_commessa")
		ls_cod_versione  = getitemstring(ll_i, "cod_versione")
		ldd_quan_in_produzione = getitemnumber(ll_i, "quan_in_produzione")
		ldd_quan_assegnata = getitemnumber(ll_i, "quan_assegnata")
		ldd_quan_prodotta  = getitemnumber(ll_i, "quan_prodotta")
		ldd_quan_in_ordine = getitemnumber(ll_i, "quan_ordine")
		
		select flag_muovi_mp,
				flag_tipo_impegno_mp
		into   :ls_flag_muovi_mp,
				:ls_flag_tipo_impegno_mp
		from   tab_tipi_commessa
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_tipo_commessa = :ls_cod_tipo_commessa;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore in ricerca flag_muovi_mp su tab_tipi_commessa. Verificare il database~r~n" + sqlca.sqlerrtext,stopsign!)
			return 1
		end if
		
		// modificato una commessa esistente
		choose case getitemstatus(ll_i, 0,primary!)
			case DataModified!
		
				select cod_prodotto,
						 cod_versione,
						 quan_in_produzione,
						 quan_assegnata,
						 quan_prodotta,
						 quan_ordine
				into   :ls_cod_prodotto_old,
						 :ls_cod_versione_old,
						 :ldd_quan_in_produzione_old,
						 :ldd_quan_assegnata_old,
						 :ldd_quan_prodotta_old,
						 :ldd_quan_in_ordine_old
				from   anag_commesse
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_commessa = :ll_anno_commessa and
						 num_commessa = :ll_num_commessa;
			
			
				setnull(ll_null)
			
				if (ls_cod_prodotto <> ls_cod_prodotto_old or ls_cod_versione <> ls_cod_versione_old or &
					ldd_quan_in_produzione <> ldd_quan_in_produzione_old or ldd_quan_assegnata <> ldd_quan_assegnata_old or &
					ldd_quan_prodotta <> ldd_quan_prodotta_old or ldd_quan_in_ordine <> ldd_quan_in_ordine_old) and & 
					ls_flag_muovi_mp='S' then
					
					ldd_quan_impegnata = ldd_quan_in_ordine - ldd_quan_assegnata - ldd_quan_in_produzione - ldd_quan_prodotta
					ldd_quan_impegnata_old = ldd_quan_in_ordine_old - ldd_quan_assegnata_old - ldd_quan_in_produzione_old - ldd_quan_prodotta_old				
					
					//	***	disimpegno con la vecchia quantità
					if not isnull(ls_cod_prodotto_old) and ls_cod_prodotto_old <> "" then
						
						//	***	impegno e disimpegno in base al tipo_commessa: se flag_tipo_impegno_mp = S allora impegno alla creazione, altrimenti all'attivazione quindi devo 
						//		controllare che ci siano dettagli.
						
						if not isnull(ls_flag_tipo_impegno_mp) and ls_flag_tipo_impegno_mp = "S" then						//	*** impegno alla creazione quindi disimpegno
						
							if iuo_magazzino.uof_impegna_mp_commessa(false, &
																					false, &
																					ls_cod_prodotto_old, &
																					ls_cod_versione_old, &
																					ldd_quan_impegnata_old, &
																					ll_anno_commessa, &
																					ll_num_commessa, &
																					ls_errore) = -1 then
								g_mb.messagebox("Apice","Errore durante il disimpegno.~r~n"+ls_errore,stopsign!)
								rollback;
								return 1
							end if
							
						else
						
							select count(*)
							into	:ll_dettagli
							from	det_anag_commesse
							where	cod_azienda = :s_cs_xx.cod_azienda and
									anno_commessa = :ll_anno_commessa and
									num_commessa = :ll_num_commessa;			
									
							if sqlca.sqlcode = 0 and not isnull(ll_dettagli) and ll_dettagli > 0 then
								
								if iuo_magazzino.uof_impegna_mp_commessa(false, &
																						false, &
																						ls_cod_prodotto_old, &
																						ls_cod_versione_old, &
																						ldd_quan_impegnata_old, &
																						ll_anno_commessa, &
																						ll_num_commessa, &
																						ls_errore) = -1 then
									g_mb.messagebox("Apice","Errore durante il disimpegno.~r~n"+ls_errore,stopsign!)
									rollback;
									return 1
								end if		
							end if
							
						end if		
					end if
				end if		
					
				if not isnull(ls_flag_tipo_impegno_mp) and ls_flag_tipo_impegno_mp = "S" then
					
					if iuo_magazzino.uof_impegna_mp_commessa(true, &
																			false, &
																			ls_cod_prodotto, &
																			ls_cod_versione, &
																			ldd_quan_impegnata, &
																			ll_anno_commessa, &
																			ll_num_commessa, &
																			ls_errore) = -1 then
						g_mb.messagebox("Apice","Errore durante l'impegno~r~n"+ls_errore,stopsign!)
						rollback;
						return 1
					end if
					
				else
					
					ll_dettagli = 0
					
					select count(*)
					into	:ll_dettagli
					from	det_anag_commesse
					where	cod_azienda = :s_cs_xx.cod_azienda and
							anno_commessa = :ll_anno_commessa and
							num_commessa = :ll_num_commessa;		
							
					if sqlca.sqlcode = 0 and not isnull(ll_dettagli) and ll_dettagli > 0 then
							
						if iuo_magazzino.uof_impegna_mp_commessa(true, &
																				false, &
																				ls_cod_prodotto, &
																				ls_cod_versione, &
																				ldd_quan_impegnata, &
																				ll_anno_commessa, &
																				ll_num_commessa, &
																				ls_errore) = -1 then
							g_mb.messagebox("Apice","Errore durante l'impegno~r~n"+ls_errore,stopsign!)
							rollback;
							return 1
						end if												
					end if					
				end if
			
			case NewModified!			// si tratta di una nuova commessa
				
				this.postevent("ue_impegno_nuova_commessa")			
		end choose
	next	
	

	for ll_i = 1 to deletedcount()
		
		long    ll_prog_riga
		integer li_risposta
		
		ll_anno_commessa = getitemnumber(ll_i, "anno_commessa", delete!, true)
		ll_num_commessa  = getitemnumber(ll_i, "num_commessa" , delete!, true)
		
		if dw_1.getitemstring(getrow(), "flag_blocco") = "S" then
			g_mb.messagebox("Sep", "Commessa non cancellabile: è bloccata.", exclamation!, ok!)
			rollback;
			return 1
		end if
		
		uo_funzioni_1 luo_funzioni_commesse
		
		luo_funzioni_commesse = CREATE uo_funzioni_1
		
		if luo_funzioni_commesse.uof_cancella_commessa(ll_anno_commessa,ll_num_commessa, ls_messaggio) = -1 then
			g_mb.messagebox("Sep", ls_messaggio)
			destroy luo_funzioni_commesse
			return 1
		end if		
		
		// CANCELLA TUTTI I DATI DI PRODUZIONE ECC....
		li_risposta = f_reset_commessa( ll_anno_commessa, ll_num_commessa, ls_errore )
		
		if li_risposta = -2 then
			g_mb.messagebox("Sep","Attenzione! La commessa è gia stata parzialmente o totalmente prodotta, pertanto non è possibile eliminarla",exclamation!)
			rollback;
			return 1
		end if
		
		if li_risposta = -1 then 
			g_mb.messagebox("Sep",ls_errore,exclamation!)
			rollback;
			return 1
		end if

	next
	
end if
end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2775
integer height = 2176
long backcolor = 12632256
string text = "Dettagli"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from uo_cs_xx_dw within det_2
integer x = 5
integer y = 12
integer width = 2720
integer height = 2120
integer taborder = 11
string dataobject = "d_commesse_prod_2"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;
long ll_errore

if not isvalid(istr_data) or isnull(istr_data) or UpperBound(istr_data.decimale) < 3 then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2],istr_data.decimale[3])

if ll_errore < 0 then
   pcca.error = c_fatal
end if


change_dw_current()
end event

event buttonclicked;call super::buttonclicked;long					ll_riga, ll_num_commessa
integer				li_anno_commessa
string					ls_appo


choose case dwo.name
		
	case "b_stampa_barcode"
		
		ls_appo = dw_2.Describe("b_stampa_barcode.Enabled")
		if Upper(ls_appo) = "0" then return		
		
		if ib_GIB then
			//report personalizzato per GIBUS
			ll_riga = getrow()
			if ll_riga < 1 then return
			li_anno_commessa = getitemnumber( ll_riga, "anno_commessa")
			ll_num_commessa = getitemnumber( ll_riga, "num_commessa")
			
			s_cs_xx.parametri.parametro_d_1 = li_anno_commessa
			s_cs_xx.parametri.parametro_d_2 = ll_num_commessa
			s_cs_xx.parametri.parametro_d_3 = 0			//se 6969 allora stampa subito
			window_open(w_report_commessa_prod, -1)
			
		else
			//s_cs_xx.parametri.parametro_dw_1 = dw_2
			s_cs_xx.parametri.parametro_i_1 = dw_2.getitemnumber(this.getrow(),"anno_commessa")
			s_cs_xx.parametri.parametro_dec4_1 = dw_2.getitemnumber(this.getrow(), "num_commessa")
			s_cs_xx.parametri.parametro_i_2 = dw_2.getitemnumber(this.getrow(), "prog_riga")
			
			window_open(w_report_bl_attivazione,-1)
		end if
			

		
	case "b_out_terzisti"
		
		ls_appo = dw_2.Describe("b_out_terzisti.Enabled")
		if Upper(ls_appo) = "0" then return			
		
		window_open_parm(w_uscite_terzisti,-1, dw_2)
		
	case "b_in_terzisti"
		
		ls_appo = dw_2.Describe("b_in_terzisti.Enabled")
		if Upper(ls_appo) = "0" then return			
		
		window_open_parm(w_ingressi_terzisti,-1,dw_2)
				
	case "b_difetti"
		
		ls_appo = dw_2.Describe("b_difetti.Enabled")
		if Upper(ls_appo) = "0" then return			
		
		window_open_parm(w_rileva_difetti_out_vista, -1, dw_2)
		
	case "b_anticipi"	
		
		ls_appo = dw_2.Describe("b_anticipi.Enabled")
		if Upper(ls_appo) = "0" then return			
		
		window_open_parm(w_det_commesse_anticipi, -1, dw_2)
		
	case "b_mat_prime"
		
		ls_appo = dw_2.Describe("b_mat_prime.Enabled")
		if Upper(ls_appo) = "0" then return			
		
		window_open_parm(w_mat_prime_commessa_attivazione,-1, dw_2)
		
	case "b_avanzamento"
		
		ls_appo = dw_2.Describe("b_avanzamento.Enabled")
		if Upper(ls_appo) = "0" then return			
		
		window_open_parm(w_avanz_prod_com_attivazione,-1, dw_2)
		
	//Donato 03-06-2008 visualizzazione della avan_produzione_comm per modifica	
	case "b_fasi"
		
		ls_appo = dw_2.Describe("b_fasi.Enabled")
		if Upper(ls_appo) = "0" then return
		
		window_open_parm(w_avan_produzione_com, -1, dw_2)
		
end choose
end event

type cbx_forza_numerazione from checkbox within w_commesse_produzione_tv
integer x = 1522
integer y = 2336
integer width = 709
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Forza Numerazione"
boolean lefttext = true
end type

event clicked;if sle_num_commessa.enabled=true then
	sle_num_commessa.enabled=false
else
	sle_num_commessa.enabled=true
end if
end event

type sle_num_commessa from singlelineedit within w_commesse_produzione_tv
integer x = 2254
integer y = 2336
integer width = 270
integer height = 80
integer taborder = 180
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

