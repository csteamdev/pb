﻿$PBExportHeader$w_commesse_rda.srw
forward
global type w_commesse_rda from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_commesse_rda
end type
end forward

global type w_commesse_rda from w_cs_xx_principale
integer width = 3863
integer height = 2360
string title = "Richieste di Acquisto Commessa"
boolean maxbox = false
boolean resizable = false
dw_report dw_report
end type
global w_commesse_rda w_commesse_rda

type variables
long il_anno_commessa, il_num_commessa
end variables

on w_commesse_rda.create
int iCurrent
call super::create
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
end on

on w_commesse_rda.destroy
call super::destroy
destroy(this.dw_report)
end on

event pc_setwindow;call super::pc_setwindow;dw_report.set_dw_options(sqlca,i_openparm, c_scrollparent + c_nonew + c_nomodify + c_nodelete + c_noenablepopup,c_default)

iuo_dw_main = dw_report

dw_report.change_dw_current()
end event

type dw_report from uo_cs_xx_dw within w_commesse_rda
integer x = 23
integer y = 20
integer width = 3794
integer height = 2240
integer taborder = 10
string dataobject = "d_commesse_rda"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;il_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"anno_commessa")

il_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_commessa")

parent.title = "Richieste di Acquisto Commessa " + string(il_anno_commessa) + "/" + string(il_num_commessa)

retrieve(s_cs_xx.cod_azienda,il_anno_commessa,il_num_commessa)
end event

event ue_key;if this.getcolumnname() = "flag_particolare" then
	if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
		this.triggerevent("pcd_save")
		this.postevent("pcd_new")
		this.setfocus()
	end if
end if 
end event

