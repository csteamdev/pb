﻿$PBExportHeader$w_commesse_selezione.srw
forward
global type w_commesse_selezione from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_commesse_selezione
end type
type cb_1 from commandbutton within w_commesse_selezione
end type
type dw_selezione from datawindow within w_commesse_selezione
end type
end forward

global type w_commesse_selezione from w_cs_xx_principale
integer width = 2149
integer height = 956
string title = "Selezione Commesse Per Chiusura Multipla"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
cb_2 cb_2
cb_1 cb_1
dw_selezione dw_selezione
end type
global w_commesse_selezione w_commesse_selezione

type variables

end variables

on w_commesse_selezione.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_selezione
end on

on w_commesse_selezione.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_selezione)
end on

event pc_setwindow;call super::pc_setwindow;//dw_report.set_dw_options(sqlca,i_openparm, c_scrollparent + c_nonew + c_nomodify + c_nodelete + c_noenablepopup,c_default)

//iuo_dw_main = dw_report

//dw_report.change_dw_current()

long      ll_i, ll_riga
datastore lds_appo

dw_selezione.settransobject( sqlca)

lds_appo = s_cs_xx.parametri.parametro_ds_1

for ll_i = 1 to lds_appo.rowcount()
	ll_riga = dw_selezione.insertrow( 0)
	dw_selezione.setitem( ll_riga, "anno_commessa", lds_appo.getitemnumber( ll_i, "anno_commessa"))
	dw_selezione.setitem( ll_riga, "num_commessa", lds_appo.getitemnumber( ll_i, "num_commessa"))
	dw_selezione.setitem( ll_riga, "cod_prodotto", lds_appo.getitemstring( ll_i, "cod_prodotto"))
	dw_selezione.setitem( ll_riga, "flag_selezione", "N")
next

end event

type cb_2 from commandbutton within w_commesse_selezione
integer x = 1257
integer y = 780
integer width = 402
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;long ll_vuoto[]

s_cs_xx.parametri.parametro_d_1_a = ll_vuoto
s_cs_xx.parametri.parametro_d_2_a = ll_vuoto

close(parent)
end event

type cb_1 from commandbutton within w_commesse_selezione
integer x = 1691
integer y = 780
integer width = 402
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;string ls_selezione
long   ll_i, ll_cont, ll_anni[], ll_numeri[], ll_vuoto[]

if g_mb.messagebox( "SEP", "Continuare con la chiusura multipla delle commesse selezionate?", Exclamation!, OKCancel!, 2) <> 1 then return 

ll_cont = 0
for ll_i = 1 to dw_selezione.rowcount()
	ls_selezione = dw_selezione.getitemstring( ll_i, "flag_selezione")
	if not isnull(ls_selezione) and ls_selezione = "S" then
		ll_cont ++
		ll_anni[ll_cont] = dw_selezione.getitemnumber( ll_i, "anno_commessa")
		ll_numeri[ll_cont] = dw_selezione.getitemnumber( ll_i, "num_commessa")
	end if
next

if upperbound( ll_anni) > 0 then
	s_cs_xx.parametri.parametro_d_1_a = ll_anni
	s_cs_xx.parametri.parametro_d_2_a = ll_numeri
else
	s_cs_xx.parametri.parametro_d_1_a = ll_vuoto
	s_cs_xx.parametri.parametro_d_2_a = ll_vuoto
end if

close(parent)
end event

type dw_selezione from datawindow within w_commesse_selezione
integer x = 23
integer y = 20
integer width = 2080
integer height = 720
integer taborder = 10
string title = "none"
string dataobject = "d_commesse_selezione"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

