﻿$PBExportHeader$w_gen_bolle_terzisti.srw
$PBExportComments$Generazione Bolle Uscita per Terzisti
forward
global type w_gen_bolle_terzisti from w_cs_xx_principale
end type
type dw_depositi_lista from datawindow within w_gen_bolle_terzisti
end type
type st_1 from statictext within w_gen_bolle_terzisti
end type
type dw_stato_stock_terzista from uo_cs_xx_dw within w_gen_bolle_terzisti
end type
type dw_elenco_fasi_esterne_attivazione from uo_cs_xx_dw within w_gen_bolle_terzisti
end type
type dw_bolle_pronte from datawindow within w_gen_bolle_terzisti
end type
type cb_elimina from commandbutton within w_gen_bolle_terzisti
end type
type cbx_tutta from checkbox within w_gen_bolle_terzisti
end type
type cb_aggiungi_sl from commandbutton within w_gen_bolle_terzisti
end type
type cb_conferma_stock from commandbutton within w_gen_bolle_terzisti
end type
type cb_sblocca_stock from commandbutton within w_gen_bolle_terzisti
end type
type dw_elenco_prod_uscita_bol from uo_cs_xx_dw within w_gen_bolle_terzisti
end type
type dw_folder from u_folder within w_gen_bolle_terzisti
end type
type dw_scelta_stock from uo_cs_xx_dw within w_gen_bolle_terzisti
end type
type dw_lista_prodotti_spedizione from uo_cs_xx_dw within w_gen_bolle_terzisti
end type
type cb_carica_note from commandbutton within w_gen_bolle_terzisti
end type
type dw_note_spedizione from datawindow within w_gen_bolle_terzisti
end type
type dw_prod_uscita_bol_spedite from uo_cs_xx_dw within w_gen_bolle_terzisti
end type
type cb_bolla_uscita from commandbutton within w_gen_bolle_terzisti
end type
type dw_scelta_fornitore from uo_cs_xx_dw within w_gen_bolle_terzisti
end type
end forward

global type w_gen_bolle_terzisti from w_cs_xx_principale
integer width = 4558
integer height = 2576
string title = "Bolle CTL Uscita"
dw_depositi_lista dw_depositi_lista
st_1 st_1
dw_stato_stock_terzista dw_stato_stock_terzista
dw_elenco_fasi_esterne_attivazione dw_elenco_fasi_esterne_attivazione
dw_bolle_pronte dw_bolle_pronte
cb_elimina cb_elimina
cbx_tutta cbx_tutta
cb_aggiungi_sl cb_aggiungi_sl
cb_conferma_stock cb_conferma_stock
cb_sblocca_stock cb_sblocca_stock
dw_elenco_prod_uscita_bol dw_elenco_prod_uscita_bol
dw_folder dw_folder
dw_scelta_stock dw_scelta_stock
dw_lista_prodotti_spedizione dw_lista_prodotti_spedizione
cb_carica_note cb_carica_note
dw_note_spedizione dw_note_spedizione
dw_prod_uscita_bol_spedite dw_prod_uscita_bol_spedite
cb_bolla_uscita cb_bolla_uscita
dw_scelta_fornitore dw_scelta_fornitore
end type
global w_gen_bolle_terzisti w_gen_bolle_terzisti

type variables
long il_anno_commessa,il_num_commessa,il_prog_riga
string is_cod_prodotto_fase,is_cod_reparto,is_cod_lavorazione
boolean ib_ok_note
end variables

forward prototypes
public function integer wf_imposta_prodotti_spedizione (long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga, string fs_cod_prodotto, string fs_cod_versione, double fd_quan_in_produzione, long fl_num_livello_cor, ref string fs_errore)
public function integer wf_imposta_stock ()
public function integer wf_stampa_bolla (long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_impegna_deposito ()
public function integer wf_annulla_impegno_deposito ()
public function string wf_get_depositi_fornitore (string as_cod_fornitore, ref string as_depositi[])
end prototypes

public function integer wf_imposta_prodotti_spedizione (long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga, string fs_cod_prodotto, string fs_cod_versione, double fd_quan_in_produzione, long fl_num_livello_cor, ref string fs_errore);string    ls_cod_prodotto_figlio, ls_flag_esterna,ls_test, ls_cod_prodotto_ap, ls_cod_prodotto_fase, ls_cod_versione_inserito, ls_cod_versione_variante, & 		   
		    ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, ls_cod_prodotto_inserito, ls_mp, ls_cod_versione_ap, & 
			 ls_cod_lavorazione,ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante, ls_cod_versione_fase, ls_cod_versione_figlio, &
			 ls_cod_mp[], ls_ver_mp[],ls_flag_ramo_descrittivo
long      ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_handle, ll_prog_spedizione, & 
			 ll_num_righe_dw,ll_i,li_anno_commessa,ll_num_commessa,ll_prog_riga, ll_cont
integer   li_num_priorita,li_risposta
dec{4}    ldd_quan_utilizzo,ldd_quan_utilizzo_variante,ldd_quan_prodotta,ldd_quan_in_produzione,&
			 ldd_quan_utilizzata,ldd_quan_utilizzo_1
boolean   lb_flag_trovata 
datastore lds_righe_distinta

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ll_prog_spedizione = dw_scelta_fornitore.getitemnumber(dw_scelta_fornitore.getrow(),"num_nuova_spedizione")

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve( s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione)

ll_num_figli = 0
	
for ll_cont = 1 to ll_num_righe
	
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_cont,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_cont,"cod_versione_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_cont,"quan_utilizzo") * fd_quan_in_produzione
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_cont,"flag_materia_prima")	
	ls_flag_ramo_descrittivo = lds_righe_distinta.getitemstring(ll_cont,"distinta_flag_ramo_descrittivo")	
	
	if ls_flag_ramo_descrittivo = "S"  then continue
	
	ll_num_figli ++

	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio
	
	ls_cod_mp[ll_num_figli] = ls_cod_prodotto_figlio
	ls_ver_mp[ll_num_figli] = ls_cod_versione_figlio
	
	select cod_prodotto,
			 quan_utilizzo,
			 flag_materia_prima,
			 cod_versione
	into   :ls_cod_prodotto_variante,	
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante,
			 :ls_cod_versione_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			 anno_commessa = :fl_anno_commessa  	and    
			 num_commessa = :fl_num_commessa			and    
			 cod_prodotto_padre = :fs_cod_prodotto and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
			 cod_versione = :fs_cod_versione  		and
			 cod_versione_figlio = :ls_cod_versione_figlio;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_cod_versione_inserito = ls_cod_versione_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante* fd_quan_in_produzione
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	//*************************************
	//ricerca se già inserito
	//*************************************

	lb_flag_trovata = false
	for ll_i = 1 to dw_lista_prodotti_spedizione.rowcount()
//		dw_lista_prodotti_spedizione.setrow(ll_i)		
		if ls_cod_prodotto_inserito = dw_lista_prodotti_spedizione.getitemstring(ll_i,"cod_prodotto")  and ls_cod_versione_inserito = dw_lista_prodotti_spedizione.getitemstring(ll_i,"cod_versione") then 
			lb_flag_trovata=true
			ldd_quan_utilizzo_1 = dw_lista_prodotti_spedizione.getitemnumber(ll_i,"quan_necessaria")
			ldd_quan_utilizzo = ldd_quan_utilizzo + ldd_quan_utilizzo_1 
			dw_lista_prodotti_spedizione.setitem(ll_i,"quan_necessaria",ldd_quan_utilizzo)
		end if
	next
	
	if lb_flag_trovata then 
		ldd_quan_utilizzo=0
		continue
	end if
	
	
// ----------  modificato da enrico per Unifast.  ------------------------------------
// devo poter spedire anche i figli che sono dei semilavorati prodotti. 
//	ls_mp = "S"
//	ldd_quan_prodotta = 0
//	ldd_quan_in_produzione = ldd_quan_utilizzo
//	ldd_quan_utilizzata = 0

	

	// verifico se si tratta di un sl o di una mp
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda  and 	 
			 cod_prodotto_padre = :ls_cod_prodotto_inserito and
	       cod_versione = :ls_cod_versione_inserito;
	
	if sqlca.sqlcode=100  or ls_flag_materia_prima = "S" then 
		// caso MP
		ls_mp = "S"
		ldd_quan_prodotta = 0
		ldd_quan_in_produzione = ldd_quan_utilizzo
		ldd_quan_utilizzata = 0
		
	else
		// caso SL
		select quan_prodotta,
				 quan_in_produzione,
				 quan_utilizzata
		into   :ldd_quan_prodotta,
				 :ldd_quan_in_produzione,
				 :ldd_quan_utilizzata
		from   avan_produzione_com
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
				 anno_commessa = :fl_anno_commessa  	and    
				 num_commessa = :fl_num_commessa			and    
				 prog_riga = :fl_prog_riga					and    
				 cod_prodotto = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
	
		if sqlca.sqlcode < 0 then
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		ls_mp ="N"
	end if
	
// --------------------------  fine modifica per Unifast  -------------------------------------------
	
	
	
	ll_num_righe_dw = dw_lista_prodotti_spedizione.rowcount()

	if ll_num_righe_dw=0 then
		ll_num_righe_dw=1
	else
		ll_num_righe_dw++
	end if

	if ls_mp = "S" then
		
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 cod_prodotto = :ls_cod_prodotto_inserito;
	
		if sqlca.sqlcode < 0 then
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if

		select cod_azienda
		into   :ls_test
		from   prod_bolle_out_com
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
				 anno_commessa = :li_anno_commessa		and    
				 num_commessa = :ll_num_commessa			and    
				 prog_riga = :ll_prog_riga					and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase	and  
				 cod_versione = :ls_cod_versione_fase  and
				 cod_reparto = :ls_cod_reparto		and    
				 cod_lavorazione = :ls_cod_lavorazione		and    
				 prog_spedizione = :ll_prog_spedizione 	and    
				 cod_prodotto_spedito = :ls_cod_prodotto_inserito;

		if sqlca.sqlcode = 100 then
			dw_lista_prodotti_spedizione.insertrow(ll_num_righe_dw)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"cod_prodotto",ls_cod_prodotto_inserito)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"cod_versione",ls_cod_versione_inserito)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"des_prodotto",ls_des_prodotto)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"quan_disponibile",ldd_quan_prodotta - ldd_quan_utilizzata)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"quan_necessaria",ldd_quan_utilizzo)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"flag_mat_prima",ls_mp)
		end if
		
	end if
	
	if ldd_quan_prodotta >= ldd_quan_in_produzione then continue
	
	if ls_flag_materia_prima = 'N' then
		
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and 	 
				 cod_prodotto_padre = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
		
		if sqlca.sqlcode = 100 then continue

	else
		continue
	end if

	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
			 cod_prodotto_padre = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		continue
	else
		li_risposta=wf_imposta_prodotti_spedizione( fl_anno_commessa, &
																  fl_num_commessa, &
																  fl_prog_riga, &
																  ls_cod_prodotto_inserito, &
																  ls_cod_versione_inserito, &
																  ldd_quan_utilizzo, &
																  fl_num_livello_cor + 1, &
																  ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next


if upperbound(ls_cod_mp) < 1 then
	g_mb.messagebox("SEP", "Nessun prodotto da spedire: controllare che non siano tutti prodotti descrittivi!")
	return 0
end if


declare righe_ap_com cursor for
select  cod_prodotto,
		  cod_versione,
		  quan_prodotta,
		  quan_utilizzata
from    avan_produzione_com
where   cod_azienda = :s_cs_xx.cod_azienda and     
		  anno_commessa = :fl_anno_commessa  and     
		  num_commessa = :fl_num_commessa    and     
		  prog_riga = :fl_prog_riga          and     
		  quan_prodotta - quan_utilizzata > 0 ;

if sqlca.sqlcode < 0 then
	ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

open righe_ap_com;

if sqlca.sqlcode < 0 then
	ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch righe_ap_com 	into  :ls_cod_prodotto_ap,
										:ls_cod_versione_ap,
										:ldd_quan_prodotta,
										:ldd_quan_utilizzata;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close righe_ap_com;
		return -1
	end if
	
	// enrico 12/1/2007 AGGIUNTO PER EVITARE DI VISUALIZZARE SEMILAVORATI PRESENTI NELL'AVANZAMENTO MA NON DI COMPETENZA
	//                  DEL RAMO DI DISTINTA SELEZIONATO.
	lb_flag_trovata = false
	for ll_i = 1 to ll_num_figli
		if ls_cod_prodotto_ap = ls_cod_mp[ll_i] and ls_cod_versione_ap = ls_ver_mp[ll_i] then
			lb_flag_trovata = true
			exit
		end if
	next
	if lb_flag_trovata = false then continue
	// FINE MODIFICA
	

	lb_flag_trovata = false

	for ll_i = 1 to dw_lista_prodotti_spedizione.rowcount()
//		dw_lista_prodotti_spedizione.setrow(ll_i)		
		if ls_cod_prodotto_ap = dw_lista_prodotti_spedizione.getitemstring(ll_i,"cod_prodotto") then lb_flag_trovata=true
	next

	if lb_flag_trovata then continue
	
	ll_num_righe_dw=dw_lista_prodotti_spedizione.rowcount()

	if ll_num_righe_dw=0 then
		ll_num_righe_dw=1
	else
		ll_num_righe_dw++
	end if

	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto_ap;

	if sqlca.sqlcode < 0 then
		ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	select cod_azienda
	into   :ls_test
	from   prod_bolle_out_com
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			 anno_commessa = :li_anno_commessa	and    
			 num_commessa = :ll_num_commessa	and    
			 prog_riga = :ll_prog_riga	and    
			 cod_prodotto_fase = :ls_cod_prodotto_fase	and    
			 cod_versione = :ls_cod_versione_fase  and
			 cod_reparto = :ls_cod_reparto	and    
			 cod_lavorazione = :ls_cod_lavorazione	and    
			 prog_spedizione = :ll_prog_spedizione	and    
			 cod_prodotto_spedito = :ls_cod_prodotto_ap;

	if sqlca.sqlcode = 100 then
		dw_lista_prodotti_spedizione.insertrow(ll_num_righe_dw)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"cod_prodotto",ls_cod_prodotto_ap)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"cod_versione",ls_cod_versione_ap)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"des_prodotto",ls_des_prodotto)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"quan_disponibile",ldd_quan_prodotta - ldd_quan_utilizzata)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"quan_necessaria",0)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"flag_mat_prima","N")
	end if
	
loop

close righe_ap_com;

dw_lista_prodotti_spedizione.resetupdate()
return 0
end function

public function integer wf_imposta_stock ();string   ls_cod_prodotto_mp,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, ls_test, ls_cod_versione_fase, & 
			ls_cod_tipo_commessa,ls_flag_mat_prima,ls_modify,ls_cod_reparto, & 
			ls_cod_lavorazione,ls_cod_prodotto_fase
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_orari, & 
			ll_num_fasi_aperte,ll_prog_spedizione
datetime ldt_data_stock
dec{4}   ldd_quan_disponibile,ldd_quan_necessaria,ldd_quan_prelevata
integer  li_anno_commessa

dw_scelta_stock.reset()
dw_scelta_stock.Reset_DW_Modified(c_ResetChildren)
if dw_lista_prodotti_spedizione.rowcount() = 0 then return 0

ls_cod_prodotto_mp = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"cod_prodotto")
ldd_quan_necessaria = dw_lista_prodotti_spedizione.getitemnumber(dw_lista_prodotti_spedizione.getrow(),"quan_necessaria")
ls_flag_mat_prima = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"flag_mat_prima")

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")	
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")	
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ll_prog_spedizione = dw_scelta_fornitore.getitemnumber(dw_scelta_fornitore.getrow(),"num_nuova_spedizione")

//select cod_azienda
//into   :ls_test
//from   prod_bolle_out_com
//where  cod_azienda = :s_cs_xx.cod_azienda and    
//		 anno_commessa = :li_anno_commessa  and    
//		 num_commessa = :ll_num_commessa    and    
//		 prog_riga = :ll_prog_riga     and    
//		 cod_prodotto_fase = :ls_cod_prodotto_fase and  
//		 cod_versione = :ls_cod_versione_fase and
//		 cod_reparto = :ls_cod_reparto and    
//		 cod_lavorazione = :ls_cod_lavorazione and    
//		 prog_spedizione = :ll_prog_spedizione and    
//		 cod_prodotto_spedito = :ls_cod_prodotto_mp;
//	
//if (ls_flag_mat_prima="S")  and sqlca.sqlcode = 100 then

	select cod_tipo_commessa
	into   :ls_cod_tipo_commessa
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :li_anno_commessa 	and    
			 num_commessa = :ll_num_commessa;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		 
		return 0
	end if
	
	if isnull(dw_depositi_lista.getitemstring(dw_depositi_lista.getrow(),"cod_deposito")) then
	
		select cod_deposito_prelievo
		into   :ls_cod_deposito
		from   tab_tipi_commessa
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
				 cod_tipo_commessa = :ls_cod_tipo_commessa;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return 0
		end if
		
	else
		
		ls_cod_deposito = dw_depositi_lista.getitemstring(dw_depositi_lista.getrow(),"cod_deposito")
	
	end if

	declare righe_stock cursor for
	SELECT  cod_ubicazione,   
			  cod_lotto,   
			  data_stock,   
			  prog_stock,
			  giacenza_stock - quan_assegnata - quan_in_spedizione  
	 FROM   stock   
	 where  cod_azienda=:s_cs_xx.cod_azienda
	 and    cod_prodotto =:ls_cod_prodotto_mp
	 and    cod_deposito =:ls_cod_deposito
	 and    giacenza_stock - quan_assegnata - quan_in_spedizione >0
	 ORDER BY data_stock asc;
 
	 open righe_stock;
	
	 if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		 
		return 0
	 end if

	do while 1 = 1
		fetch righe_stock 
		into  :ls_cod_ubicazione,
				:ls_cod_lotto,   
				:ldt_data_stock,   
				:ll_prog_stock,
				:ldd_quan_disponibile;

		choose case sqlca.sqlcode  
			case 100
				exit
			case is < 0 
				g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext, stopsign!)
				
					
				exit
		end choose

		ll_num_righe=dw_scelta_stock.rowcount()
	
		if ll_num_righe=0 then
			ll_num_righe=1
		else
			ll_num_righe++
		end if
	
		
		dw_scelta_stock.insertrow(ll_num_righe)
		dw_scelta_stock.setitem(ll_num_righe,"cod_prodotto",ls_cod_prodotto_mp)
		dw_scelta_stock.setitem(ll_num_righe,"cod_deposito",ls_cod_deposito)
		dw_scelta_stock.setitem(ll_num_righe,"cod_ubicazione",ls_cod_ubicazione)
		dw_scelta_stock.setitem(ll_num_righe,"cod_lotto",ls_cod_lotto)
		dw_scelta_stock.setitem(ll_num_righe,"data_stock",ldt_data_stock)
		dw_scelta_stock.setitem(ll_num_righe,"prog_stock",ll_prog_stock)
		dw_scelta_stock.setitem(ll_num_righe,"quan_disponibile",ldd_quan_disponibile)
		
		if ldd_quan_necessaria > 0 then
			if ldd_quan_disponibile >= ldd_quan_necessaria then
				ldd_quan_prelevata = ldd_quan_necessaria
				ldd_quan_necessaria = 0 
			else
				ldd_quan_prelevata = ldd_quan_disponibile
				ldd_quan_necessaria = ldd_quan_necessaria - ldd_quan_prelevata
			end if
		else
			ldd_quan_prelevata = 0
		end if
		
		dw_scelta_stock.setitem(ll_num_righe,"quan_prelevata",ldd_quan_prelevata)

	loop
	
	close righe_stock;
	
	dw_scelta_stock.Reset_DW_Modified(c_ResetChildren)
	dw_scelta_stock.settaborder("quan_prelevata",10)
	cb_conferma_stock.enabled = true
	dw_scelta_stock.enabled = true
	dw_elenco_prod_uscita_bol.resetupdate()
	dw_lista_prodotti_spedizione.resetupdate()
	dw_scelta_stock.resetupdate()
	
//else
//	dw_scelta_stock.Reset_DW_Modified(c_ResetChildren)
//	cb_conferma_stock.enabled = false
//	dw_scelta_stock.enabled = false
//	dw_elenco_prod_uscita_bol.resetupdate()
//	dw_lista_prodotti_spedizione.resetupdate()
//	dw_scelta_stock.resetupdate()
//	return 0
//	
//end if
return 0

end function

public function integer wf_stampa_bolla (long fl_anno_registrazione, long fl_num_registrazione);long   ll_anno_registrazione, ll_num_registrazione, ll_num_documento, ll_count
string ls_cod_documento, ls_cod_cliente, ls_parametro
	
s_cs_xx.parametri.parametro_i_1 = 0
s_cs_xx.parametri.parametro_d_1 = fl_anno_registrazione
s_cs_xx.parametri.parametro_d_2 = fl_num_registrazione


window_open(w_tipo_stampa_bol_ven, 0)

if s_cs_xx.parametri.parametro_i_1 <> -1 then
	
	string   ls_window = "w_report_bol_ven_euro", ls_test
	
	window   lw_window
	
	select stringa
	into   :ls_test
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_parametro = 'BVE';
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore nella select di parametri_azienda: " + sqlca.sqlerrtext)
		return -1
	elseif sqlca.sqlcode = 100 then		
		window_open(w_report_bol_ven, -1)
	elseif sqlca.sqlcode = 0 then			
		window_type_open(lw_window,ls_window, -1)
	end if
	
end if

return 0
end function

public function integer wf_impegna_deposito ();string   ls_cod_prodotto_spedito,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, ls_cod_deposito_fornitore, &
			ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_cod_fornitore, & 
			ls_cod_prodotto_fase,ls_cod_versione,ls_errore, ls_cod_versione_fase, ls_cod_versione_spedito
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, & 
			ll_prog_prodotto_spedito,ll_t, ll_i, ll_ret
datetime ldt_data_stock
dec{4}   ldd_quan_spedita,ldd_quan_in_produzione,ldd_quan_disponibile,ldd_quan_impegnata,ldd_quan_necessaria, & 
			ldd_nuova_quan_impegnata,ldd_somma_quan_spedita
integer  li_anno_commessa,li_risposta

ll_ret = g_mb.messagebox( "SEP", "Continuare con l'impegno del deposito senza generazione DDT?", Information!, YesNo!, 2)

if ll_ret <> 1 then
	
	g_mb.messagebox( "SEP", "Operazione Annullata!", Information!)
	rollback;
	return -1
	
end if

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

dw_scelta_fornitore.accepttext()

ls_cod_fornitore = dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"cod_fornitore")

if isnull (ls_cod_fornitore) or len(trim(ls_cod_fornitore)) < 1 then
	g_mb.messagebox("Sep","Selezionare un Fornitore dal folder Spedizione !",stopsign!)
	return -1
end if

select cod_deposito
into   :ls_cod_deposito_fornitore
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 cod_fornitore = :ls_cod_fornitore;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Attenzione: selezionare il fornitore dal folder Spedizione; in questo modo si assegna il deposito di invio. " + sqlca.sqlerrtext,stopsign!)
	return  -1
end if

if isnull (ls_cod_deposito_fornitore) or len(trim(ls_cod_deposito_fornitore)) < 1 then
	g_mb.messagebox("Sep","Attenzione! il fornitore non è associato ad alcun deposito.",stopsign!)
	return -1
end if

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")

//	*** scorro la lista dei prodotti in spedizione 
for ll_i = 1 to dw_lista_prodotti_spedizione.rowcount()	
	
	ldd_quan_disponibile = dw_lista_prodotti_spedizione.getitemnumber( ll_i, "quan_disponibile")
	ldd_quan_necessaria = dw_lista_prodotti_spedizione.getitemnumber( ll_i, "quan_necessaria")
	ls_cod_prodotto_spedito = dw_lista_prodotti_spedizione.getitemstring( ll_i, "cod_prodotto")
	ls_cod_versione_spedito = dw_lista_prodotti_spedizione.getitemstring( ll_i, "cod_versione")
	
	if isnull(ldd_quan_disponibile) then ldd_quan_disponibile = 0
	if isnull(ldd_quan_necessaria) then ldd_quan_necessaria = 0
	
	select sum(quan_impegnata)
	into   :ldd_quan_impegnata
	from   impegni_deposito_fasi_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_commessa = :li_anno_commessa  and    
			 num_commessa = :ll_num_commessa    and    
			 cod_prodotto_fase = :ls_cod_prodotto_fase and
			 cod_versione = :ls_cod_versione_fase and    
			 cod_reparto = :ls_cod_reparto and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 prog_riga = :ll_prog_riga     and    
			 cod_prodotto_spedito = :ls_cod_prodotto_spedito;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore nella totalizzazione impegnato da  impegni_deposito_fasi_com ~r~n" + sqlca.sqlerrtext,stopsign!)
		rollback;
		return -1
	end if
	
	if isnull(ldd_quan_impegnata) then ldd_quan_impegnata = 0
	
	ldd_nuova_quan_impegnata = ldd_quan_necessaria - ldd_quan_impegnata
	
	update impegni_deposito_fasi_com
	set    quan_impegnata_ultima = 0
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_commessa = :li_anno_commessa and    
			 num_commessa = :ll_num_commessa and    
			 prog_riga = :ll_prog_riga and    
			 cod_prodotto_fase = :ls_cod_prodotto_fase and    
			 cod_versione = :ls_cod_versione_fase and
			 cod_reparto = :ls_cod_reparto and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 cod_deposito = :ls_cod_deposito_fornitore and    
			 cod_prodotto_spedito = :ls_cod_prodotto_spedito;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore in aggiornamento tabella impegni_deposito_fasi_com ~r~n" + sqlca.sqlerrtext,stopsign!)
		rollback;
		return -1
	end if
	
	select cod_azienda
	into   :ls_test
	from   impegni_deposito_fasi_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_commessa = :li_anno_commessa and    
			 num_commessa = :ll_num_commessa and    
			 prog_riga = :ll_prog_riga  and    
			 cod_prodotto_fase = :ls_cod_prodotto_fase and
			 cod_versione = :ls_cod_versione_fase and    
			 cod_reparto = :ls_cod_reparto and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 cod_deposito = :ls_cod_deposito_fornitore and    
			 cod_prodotto_spedito = :ls_cod_prodotto_spedito;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore in verifica presenza impegnati precedenti in impegni_deposito_fasi_comÈr~n" + sqlca.sqlerrtext,stopsign!)
		rollback;
		return -1
	end if
	
	if sqlca.sqlcode = 100 or isnull(ls_test) or ls_test ="" then
		
		insert into impegni_deposito_fasi_com
		(cod_azienda,
		 anno_commessa,
		 num_commessa,
		 prog_riga,
		 cod_prodotto_fase,
		 cod_versione,
		 cod_reparto,
		 cod_lavorazione,
		 cod_deposito,
		 cod_prodotto_spedito,
		 quan_impegnata,
		 quan_impegnata_ultima)
		values
		(:s_cs_xx.cod_azienda,
		 :li_anno_commessa,
		 :ll_num_commessa,
		 :ll_prog_riga,
		 :ls_cod_prodotto_fase,
		 :ls_cod_versione_fase,
		 :ls_cod_reparto,
		 :ls_cod_lavorazione,
		 :ls_cod_deposito_fornitore,
		 :ls_cod_prodotto_spedito,
		 :ldd_nuova_quan_impegnata,
		 :ldd_nuova_quan_impegnata);
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore in aggiunta impegnato in impegni_deposito_fasi_com~r~n " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return -1
		end if
	
	else
	
		if sqlca.sqlcode = 0 then
			
			update impegni_deposito_fasi_com
			set    quan_impegnata = quan_impegnata + :ldd_nuova_quan_impegnata,
					 quan_impegnata_ultima =:ldd_nuova_quan_impegnata
			where  cod_azienda = :s_cs_xx.cod_azienda 	and    
					 anno_commessa = :li_anno_commessa 		and    
					 num_commessa = :ll_num_commessa 		and    
					 prog_riga = :ll_prog_riga 		and    
					 cod_prodotto_fase = :ls_cod_prodotto_fase and
					 cod_versione = :ls_cod_versione_fase 		and    
					 cod_reparto = :ls_cod_reparto 		and    
					 cod_lavorazione = :ls_cod_lavorazione 		and    
					 cod_deposito = :ls_cod_deposito_fornitore 	and    
					 cod_prodotto_spedito = :ls_cod_prodotto_spedito;
	
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore in aggiornamento impegnato in impegni_deposito_fasi_com~r~n" + sqlca.sqlerrtext,stopsign!)
				rollback;
				return -1
			end if
			
		end if		
		
	end if	

next

commit;

g_mb.messagebox( "SEP", "Impegno generato con successo!", Information!)

close(this)

return 0
end function

public function integer wf_annulla_impegno_deposito ();string   ls_cod_prodotto_spedito,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, ls_cod_deposito_fornitore, &
			ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_cod_fornitore, & 
			ls_cod_prodotto_fase,ls_cod_versione,ls_errore, ls_cod_versione_fase, ls_cod_versione_spedito
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, & 
			ll_prog_prodotto_spedito,ll_t, ll_i, ll_ret
datetime ldt_data_stock
dec{4}   ldd_quan_spedita,ldd_quan_in_produzione,ldd_quan_disponibile,ldd_quan_impegnata,ldd_quan_necessaria, & 
			ldd_nuova_quan_impegnata,ldd_somma_quan_spedita
integer  li_anno_commessa,li_risposta

ll_ret = g_mb.messagebox( "SEP", "Continuare con con il dis-impegno del deposito?", Information!, YesNo!, 2)

if ll_ret <> 1 then
	
	g_mb.messagebox( "SEP", "Operazione Annullata!", Information!)
	rollback;
	return -1
	
end if

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

dw_scelta_fornitore.accepttext()

ls_cod_fornitore = dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"cod_fornitore")

if isnull (ls_cod_fornitore) or len(trim(ls_cod_fornitore)) < 1 then
	g_mb.messagebox("Sep","Selezionare un Fornitore dal folder Spedizione !",stopsign!)
	return -1
end if

select cod_deposito
into   :ls_cod_deposito_fornitore
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 cod_fornitore = :ls_cod_fornitore;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Attenzione: selezionare il fornitore dal folder Spedizione; in questo modo si assegna il deposito di invio. " + sqlca.sqlerrtext,stopsign!)
	return  -1
end if

if isnull (ls_cod_deposito_fornitore) or len(trim(ls_cod_deposito_fornitore)) < 1 then
	g_mb.messagebox("Sep","Attenzione! il fornitore non è associato ad alcun deposito.",stopsign!)
	return -1
end if

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")

//	*** scorro la lista dei prodotti in spedizione 
for ll_i = 1 to dw_lista_prodotti_spedizione.rowcount()	
	
	ldd_quan_necessaria = dw_lista_prodotti_spedizione.getitemnumber( ll_i, "quan_necessaria")
	ls_cod_prodotto_spedito = dw_lista_prodotti_spedizione.getitemstring( ll_i, "cod_prodotto")
	ls_cod_versione_spedito = dw_lista_prodotti_spedizione.getitemstring( ll_i, "cod_versione")
	
	if isnull(ldd_quan_necessaria) then ldd_quan_necessaria = 0
	
	//***************
	//INIZIO DISIMPEGNO PRODOTTO SUL DEPOSITO DEL FORNITORE
					
	select quan_impegnata
	into   :ldd_quan_impegnata
	from   impegni_deposito_fasi_com
	where  cod_azienda = :s_cs_xx.cod_azienda   	and    
			 anno_commessa = :li_anno_commessa		and    
			 num_commessa = :ll_num_commessa			and    
			 prog_riga = :ll_prog_riga					and    
			 cod_prodotto_fase = :ls_cod_prodotto_fase and
			 cod_versione = :ls_cod_versione_fase  and    
			 cod_reparto = :ls_cod_reparto			and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 cod_deposito = :ls_cod_deposito_fornitore and    
			 cod_prodotto_spedito = :ls_cod_prodotto_spedito;	
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore in ricerca impegno fornitore~r~n" + sqlca.sqlerrtext,stopsign!)
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then 
		continue
	end if
	
//	if isnull(ldd_quan_impegnata) then ldd_quan_impegnata = 0
//	
//	if ldd_quan_impegnata > ldd_quan_necessaria then
//		
//		update impegni_deposito_fasi_com
//		set    quan_impegnata = quan_impegnata - :ldd_quan_necessaria
//		where  cod_azienda = :s_cs_xx.cod_azienda    and    
//				 anno_commessa = :li_anno_commessa		and    
//				 num_commessa = :ll_num_commessa			and    
//				 prog_riga = :ll_prog_riga					and    
//				 cod_prodotto_fase = :ls_cod_prodotto_fase and    
//				 cod_versione = :ls_cod_versione_fase  and
//				 cod_reparto = :ls_cod_reparto			and    
//				 cod_lavorazione = :ls_cod_lavorazione	and    
//				 cod_deposito = :ls_cod_deposito_fornitore and    
//				 cod_prodotto_spedito = :ls_cod_prodotto_spedito;	
//		
//		if sqlca.sqlcode <> 0 then
//			g_mb.messagebox("Sep","Errore in fase di disimpegno dal deposito fornitore (impegni_deposito_fasi_com)~r~n" + sqlca.sqlerrtext,stopsign!)
//			rollback;
//			return -1
//		end if
//
//	else
		delete impegni_deposito_fasi_com
		where  cod_azienda = :s_cs_xx.cod_azienda		and    
				 anno_commessa = :li_anno_commessa		and    
				 num_commessa = :ll_num_commessa			and    
				 prog_riga = :ll_prog_riga					and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase and
				 cod_versione = :ls_cod_versione_fase  and    
				 cod_reparto = :ls_cod_reparto			and    
				 cod_lavorazione = :ls_cod_lavorazione and    
				 cod_deposito = :ls_cod_deposito_fornitore  and    
				 cod_prodotto_spedito = :ls_cod_prodotto_spedito;	
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Sep","Errore in cancellazione impegno dal deposito fornitore (impegni_deposito_fasi_com)~r~n" + sqlca.sqlerrtext,stopsign!)
			rollback;
			return -1
		end if
		
//	end if
	
	//FINE DISIMPEGNO PRODOTTO SUL DEPOSITO DEL FORNITORE

next

commit;

g_mb.messagebox( "SEP", "Dis-Impegno generato con successo!", Information!)

close(this)

return 0
end function

public function string wf_get_depositi_fornitore (string as_cod_fornitore, ref string as_depositi[]);//torna la lista dei depositi del fornitore in anag_depositi formattata con singoli apici e separati da virgola
//es.	'D01','D02','100','105'
//		'D01'				se unico
//se non ce ne sono torna stringa vuota
//inoltre nella var. by ref torna un array con questi codici deposito

string				ls_sql, ls_return, ls_errore
datastore		lds_data
long				ll_ret, ll_index


ls_sql = "select cod_deposito "+&
			"from   anag_depositi "+&
			"where  cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_fornitore='"+as_cod_fornitore+"'"

//mi disinteresso dell'errore
ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_ret <=0 then return ""

ls_return = ""

for ll_index=1 to ll_ret
	if ll_index>1 then ls_return+=","
	ls_return += "'"+lds_data.getitemstring(ll_index, 1)+"'"
	
	as_depositi[ll_index] = lds_data.getitemstring(ll_index, 1)
next


return ls_return
end function

on w_gen_bolle_terzisti.create
int iCurrent
call super::create
this.dw_depositi_lista=create dw_depositi_lista
this.st_1=create st_1
this.dw_stato_stock_terzista=create dw_stato_stock_terzista
this.dw_elenco_fasi_esterne_attivazione=create dw_elenco_fasi_esterne_attivazione
this.dw_bolle_pronte=create dw_bolle_pronte
this.cb_elimina=create cb_elimina
this.cbx_tutta=create cbx_tutta
this.cb_aggiungi_sl=create cb_aggiungi_sl
this.cb_conferma_stock=create cb_conferma_stock
this.cb_sblocca_stock=create cb_sblocca_stock
this.dw_elenco_prod_uscita_bol=create dw_elenco_prod_uscita_bol
this.dw_folder=create dw_folder
this.dw_scelta_stock=create dw_scelta_stock
this.dw_lista_prodotti_spedizione=create dw_lista_prodotti_spedizione
this.cb_carica_note=create cb_carica_note
this.dw_note_spedizione=create dw_note_spedizione
this.dw_prod_uscita_bol_spedite=create dw_prod_uscita_bol_spedite
this.cb_bolla_uscita=create cb_bolla_uscita
this.dw_scelta_fornitore=create dw_scelta_fornitore
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_depositi_lista
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_stato_stock_terzista
this.Control[iCurrent+4]=this.dw_elenco_fasi_esterne_attivazione
this.Control[iCurrent+5]=this.dw_bolle_pronte
this.Control[iCurrent+6]=this.cb_elimina
this.Control[iCurrent+7]=this.cbx_tutta
this.Control[iCurrent+8]=this.cb_aggiungi_sl
this.Control[iCurrent+9]=this.cb_conferma_stock
this.Control[iCurrent+10]=this.cb_sblocca_stock
this.Control[iCurrent+11]=this.dw_elenco_prod_uscita_bol
this.Control[iCurrent+12]=this.dw_folder
this.Control[iCurrent+13]=this.dw_scelta_stock
this.Control[iCurrent+14]=this.dw_lista_prodotti_spedizione
this.Control[iCurrent+15]=this.cb_carica_note
this.Control[iCurrent+16]=this.dw_note_spedizione
this.Control[iCurrent+17]=this.dw_prod_uscita_bol_spedite
this.Control[iCurrent+18]=this.cb_bolla_uscita
this.Control[iCurrent+19]=this.dw_scelta_fornitore
end on

on w_gen_bolle_terzisti.destroy
call super::destroy
destroy(this.dw_depositi_lista)
destroy(this.st_1)
destroy(this.dw_stato_stock_terzista)
destroy(this.dw_elenco_fasi_esterne_attivazione)
destroy(this.dw_bolle_pronte)
destroy(this.cb_elimina)
destroy(this.cbx_tutta)
destroy(this.cb_aggiungi_sl)
destroy(this.cb_conferma_stock)
destroy(this.cb_sblocca_stock)
destroy(this.dw_elenco_prod_uscita_bol)
destroy(this.dw_folder)
destroy(this.dw_scelta_stock)
destroy(this.dw_lista_prodotti_spedizione)
destroy(this.cb_carica_note)
destroy(this.dw_note_spedizione)
destroy(this.dw_prod_uscita_bol_spedite)
destroy(this.cb_bolla_uscita)
destroy(this.dw_scelta_fornitore)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[], l_vuoto[]

set_w_options(c_noenablepopup)


l_objects[1] = dw_elenco_prod_uscita_bol
l_objects[2] = cb_sblocca_stock
dw_folder.fu_AssignTab(3, "Prodotti in spedizione", l_objects[])


l_objects[] = l_vuoto[]
l_objects[1] = dw_prod_uscita_bol_spedite
l_objects[2] = cb_elimina
l_objects[3] = cbx_tutta
dw_folder.fu_AssignTab(4, "DDT Generati", l_Objects[])


l_objects[] = l_vuoto[]
l_objects[1] = dw_note_spedizione
l_objects[2] = cb_bolla_uscita
l_objects[3] = cb_carica_note
dw_folder.fu_AssignTab(5, "Genera DDT", l_Objects[])


l_objects[] = l_vuoto[]
l_objects[1] = dw_bolle_pronte
l_objects[2] = dw_scelta_fornitore
dw_folder.fu_AssignTab(1, "Selezione Terzista", l_Objects[])


l_objects[] = l_vuoto[]
l_objects[1] = dw_lista_prodotti_spedizione
l_objects[2] = cb_aggiungi_sl
l_objects[3] = dw_scelta_stock
l_objects[4] = dw_stato_stock_terzista
l_objects[5] = dw_depositi_lista
l_objects[6] = cb_conferma_stock
dw_folder.fu_AssignTab(2, "Aggiungi Prodotti", l_objects[])

dw_folder.fu_FolderCreate(5,5)

dw_elenco_fasi_esterne_attivazione.set_dw_options(sqlca, &
																	i_openparm, &
																	c_nonew + & 
																	c_nodelete + &
																	c_nomodify +  & 
																	c_disablecc + &
																	c_disableccinsert, &
																	c_ViewModeBorderUnchanged + &
																	c_ViewModeColorUnchanged + &
																	c_InactiveDWColorUnchanged )

dw_lista_prodotti_spedizione.set_dw_options(sqlca, &
															pcca.null_object, &
															c_nonew + &
															c_nomodify + &
															c_nodelete +  & 
															c_disablecc + &
															c_disableccinsert, &
															c_ViewModeBorderUnchanged + &
															c_ViewModeColorUnchanged + &
															c_InactiveDWColorUnchanged )

dw_scelta_stock.set_dw_options(sqlca, &
										pcca.null_object, &
										c_nonew + &
										c_nomodify + &
										c_nodelete +  & 
										c_disablecc + &
										c_disableccinsert, &
										c_ViewModeBorderUnchanged + &
										c_ViewModeColorUnchanged + &
										c_InactiveDWColorUnchanged )
													
dw_elenco_prod_uscita_bol.set_dw_options(sqlca, &
														pcca.null_object, &
														c_nonew + &
														c_nomodify + &
														c_nodelete + &
														c_disablecc + &
														c_noretrieveonopen + &
														c_disableccinsert , &
														c_default)	
													
dw_prod_uscita_bol_spedite.set_dw_options(sqlca, &
														pcca.null_object, &
														c_nonew + &
														c_nomodify + &
														c_nodelete + &
														c_disablecc + &
														c_noretrieveonopen + &
														c_disableccinsert , &
														c_default)	

													

dw_scelta_fornitore.set_dw_options(sqlca, &
												pcca.null_object, &
												c_newonopen + &
												c_nomodify + &
												c_nodelete + &
												c_disablecc + &
												c_noretrieveonopen + &
												c_disableccinsert , &
												c_viewmodewhite)

dw_stato_stock_terzista.set_dw_options(sqlca, & 
													pcca.null_object, &
													c_nonew + & 
												  	c_nodelete + &
													c_nomodify +  & 
													c_disablecc + &
													c_disableccinsert, &
													c_ViewModeBorderUnchanged + &
													c_ViewModeColorUnchanged + &
													c_InactiveDWColorUnchanged )


dw_folder.fu_SelectTab(1)

save_on_close(c_socnosave)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_scelta_fornitore, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_terzista = 'S' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end event

event closequery;call super::closequery;delete prod_bolle_out_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :il_anno_commessa  and    
		 num_commessa = :il_num_commessa    and    
		 prog_riga = :il_prog_riga          and    
		 anno_registrazione is null         and    
		 num_registrazione is null          and    
		 prog_riga_bol_ven is null;



if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	 
	rollback;
	
else
	commit;
	
end if

end event

type dw_depositi_lista from datawindow within w_gen_bolle_terzisti
event ue_retrieve ( )
event ue_imposta_stock ( )
integer x = 3387
integer y = 1260
integer width = 1070
integer height = 400
integer taborder = 150
string title = "none"
string dataobject = "d_gen_bolle_terzisti_depositi_lista"
boolean border = false
boolean livescroll = true
end type

event ue_retrieve();
string ls_select, ls_cod_prodotto

dw_depositi_lista.reset()

dw_depositi_lista.insertrow(0)

if dw_lista_prodotti_spedizione.rowcount() > 0 then
	
	ls_cod_prodotto = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"cod_prodotto")
	
	if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) = 0 then return
	
	ls_select = " select distinct cod_deposito FROM stock " + &
					" where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto ='" + ls_cod_prodotto + "' and giacenza_stock - quan_assegnata - quan_in_spedizione > 0" ;
	
	f_po_loaddddw_dw(dw_depositi_lista, &
						  "cod_deposito", &
						  sqlca, &
						  "anag_depositi", &
						  "cod_deposito", &
						  "des_deposito", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and " + &
						  "cod_deposito in ("+ls_select+")" )

end if

end event

event ue_imposta_stock();wf_imposta_stock()
end event

event itemchanged;postevent("ue_imposta_stock")
end event

type st_1 from statictext within w_gen_bolle_terzisti
integer width = 4480
integer height = 136
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SPEDIZIONE MATERIE PRIME E SEMILAVORATI DEL CICLO DI LAVORO AL TERZISTA"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_stato_stock_terzista from uo_cs_xx_dw within w_gen_bolle_terzisti
integer x = 55
integer y = 1792
integer width = 4379
integer height = 608
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_gen_bolle_terzisti_stato_stock_terzist"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string				ls_cod_prodotto, ls_cod_deposito, ls_cod_fornitore, ls_dep[], ls_where_depositi, ls_sql, ls_errore
LONG				ll_i, ll_k, ll_tot, ll_pos
double			ldd_quan_impegnata
datastore		lds_data


if dw_lista_prodotti_spedizione.rowcount() > 0 then
	ls_cod_prodotto = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"cod_prodotto")
	ls_cod_fornitore = dw_scelta_fornitore.gettext()
	
	//recupera il deposito da anag_depositi (del cod_fornitore): attento che potrebbero essere + di uno ...
	//altrimenti da anag_fornitori (com'era prima di questa modifica)
	ls_where_depositi = wf_get_depositi_fornitore(ls_cod_fornitore, ls_dep[])
	
	if ls_where_depositi<>"" then
	else
	
		select cod_deposito
		into   :ls_cod_deposito
		from   anag_fornitori
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_fornitore=:ls_cod_fornitore;
		
		 ls_dep[1] = ls_cod_deposito
		 ls_where_depositi = "'"+ls_cod_deposito+"'"
		
	end if
	
	
//	ll_tot = Retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ls_where_depositi)
//	IF ll_tot < 0 THEN
//		PCCA.Error = c_Fatal
//		return
//	END IF
	
	reset()
	ls_sql = getsqlselect()
	ll_pos = pos(upper(ls_sql), "WHERE", 1)
	
	ls_sql = left(ls_sql, ll_pos - 1)
	ls_sql += " where stock.cod_azienda = anag_depositi.cod_azienda and "+&
							"stock.cod_deposito = anag_depositi.cod_deposito and "+&
							"stock.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"stock.cod_prodotto='"+ls_cod_prodotto+"' and "+&
							"stock.cod_deposito in ("+ls_where_depositi+") and "+&
							"giacenza_stock - quan_assegnata - quan_in_spedizione > 0"
	ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
	if ll_tot<=0 then return
	
	lds_data.rowscopy(1, ll_tot, primary!, this, 1, primary!)
	ll_tot = rowcount()
	
	//visualizzo il totale dell'impegnato per ogni deposito del terzista
	for ll_k=1 to upperbound(ls_dep[])
		
		ls_cod_deposito = ls_dep[ll_k]
		
		select sum(quan_impegnata_ultima)
		into   :ldd_quan_impegnata
		from   impegni_deposito_fasi_com
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_deposito=:ls_cod_deposito
		and    cod_prodotto_spedito=:ls_cod_prodotto;
	
		for ll_i = 1 to ll_tot
			if getitemstring(ll_i, "cod_deposito") = ls_cod_deposito then setitem(ll_i, "quan_impegnata_deposito", ldd_quan_impegnata)
		next
	
	next
	
	resetupdate()

end if


end event

event getfocus;call super::getfocus;dw_scelta_fornitore.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type dw_elenco_fasi_esterne_attivazione from uo_cs_xx_dw within w_gen_bolle_terzisti
integer x = 23
integer y = 148
integer width = 4439
integer height = 188
integer taborder = 200
boolean bringtotop = true
string dataobject = "d_gen_bolle_terzisti_lista_fasi"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto, ls_cod_versione, ls_cod_reparto, ls_cod_lavorazione
LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")
ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_cod_versione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_versione")
ls_cod_reparto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_reparto")
ls_cod_lavorazione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lavorazione")

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   ll_anno_commessa,ll_num_commessa,ll_prog_riga, &
						 ls_cod_prodotto, ls_cod_versione, &
						 ls_cod_reparto, ls_cod_lavorazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

this.postevent("clicked")
end event

event clicked;call super::clicked;if i_extendmode then
	string ls_errore,ls_cod_versione,ls_cod_prodotto_finito,ls_des_prodotto_finito, ls_cod_versione_fase
	long ll_prog_spedizione
	dec{4} ldd_quan_in_produzione
	integer li_risposta
	
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_elenco_prod_uscita_bol.resetupdate()
	dw_lista_prodotti_spedizione.resetupdate()
	dw_prod_uscita_bol_spedite.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock.resetupdate()
	
	il_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "anno_commessa")
	il_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "num_commessa")
	il_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "prog_riga")
	is_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(), "cod_prodotto")
	ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(), "cod_versione")
	ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")
	is_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
	is_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		

	select max(prog_spedizione)
	into   :ll_prog_spedizione
	from   prod_bolle_out_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :il_anno_commessa  and    
			 num_commessa = :il_num_commessa    and    
			 cod_prodotto_fase = :is_cod_prodotto_fase and   
			 cod_versione = :ls_cod_versione_fase and
			 cod_reparto = :is_cod_reparto 	   and    
			 cod_lavorazione = :is_cod_lavorazione     and    
			 anno_registrazione is not null     and    
			 num_registrazione is not null      and    
			 prog_riga_bol_ven is not null;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 
	end if
	
	if isnull(ll_prog_spedizione) then
		ll_prog_spedizione=1
	else
		ll_prog_spedizione++
	end if

	dw_scelta_fornitore.setitem(dw_scelta_fornitore.getrow(),"num_nuova_spedizione", ll_prog_spedizione)
	
	if dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"flag_bolla_unica") = "N" then dw_elenco_prod_uscita_bol.reset()
	
	dw_lista_prodotti_spedizione.reset()
	dw_scelta_stock.reset()
	
	
// *** Michela 06/10/2006: la versione è nella fase di lavorazione quindi commento la
//                         select dall'anagrafica commesse
//	select cod_versione
//	into   :ls_cod_versione
//	from   anag_commesse
//	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
//	       anno_commessa = :il_anno_commessa     and    
//			 num_commessa = :il_num_commessa;
//	
//	if sqlca.sqlcode < 0 then
//		messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//		return 
//	end if
		
	dw_lista_prodotti_spedizione.setredraw(false)

	li_risposta = wf_imposta_prodotti_spedizione( il_anno_commessa, &
																 il_num_commessa,  &
																 il_prog_riga,     &
																 is_cod_prodotto_fase, &
																 ls_cod_versione_fase, &
																 ldd_quan_in_produzione, &
																 1, &
																 ls_errore)
	
	if li_risposta <0 then
		dw_lista_prodotti_spedizione.setredraw(true)
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		return
	end if
	
	dw_lista_prodotti_spedizione.Reset_DW_Modified(c_ResetChildren)

	dw_lista_prodotti_spedizione.settaborder("quan_spedizione",10)
	dw_lista_prodotti_spedizione.change_dw_current()
	parent.triggerevent("pc_retrieve")
	dw_prod_uscita_bol_spedite.change_dw_current()
	parent.triggerevent("pc_retrieve")

	wf_imposta_stock()
	dw_scelta_stock.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_elenco_prod_uscita_bol.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_stato_stock_terzista.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_elenco_prod_uscita_bol.resetupdate()
	dw_lista_prodotti_spedizione.resetupdate()
	dw_prod_uscita_bol_spedite.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock.resetupdate()
	
end if
end event

type dw_bolle_pronte from datawindow within w_gen_bolle_terzisti
integer x = 59
integer y = 756
integer width = 3237
integer height = 516
integer taborder = 190
boolean bringtotop = true
string title = "none"
string dataobject = "d_gen_bolle_terzisti_elenco_bolle_pronte"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type cb_elimina from commandbutton within w_gen_bolle_terzisti
integer x = 4046
integer y = 1932
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina"
end type

event clicked;string   ls_cod_prodotto_spedito,ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_cod_fornitore,& 
			ls_cod_prodotto_fase,ls_flag_movimenti,ls_cod_prodotto_in_spedizione,ls_cod_deposito,ls_cod_prodotto_test, ls_cod_versione_fase
			
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, ll_i, & 
			ll_prog_prodotto_spedito,ll_t,ll_anno_registrazione,ll_num_registrazione, ll_prog_riga_bol_ven
integer  li_anno_commessa
double   ldd_quan_necessaria,ldd_quan_impegnata,ldd_somma_giacenza,ldd_somma_quan_spedita,ldd_quan_spedita
if (g_mb.messagebox("Sep","Sei sicuro di eliminare la spedizione merce?",Exclamation!,yesno!, 2))=2 then return

if dw_prod_uscita_bol_spedite.rowcount() > 0 then
	
	li_anno_commessa = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"anno_commessa")
	ll_num_commessa = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"num_commessa")
	ll_prog_riga = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"prog_riga")
	ls_cod_prodotto_fase = dw_prod_uscita_bol_spedite.getitemstring(dw_prod_uscita_bol_spedite.getrow(),"cod_prodotto_fase")
	ls_cod_versione_fase = dw_prod_uscita_bol_spedite.getitemstring(dw_prod_uscita_bol_spedite.getrow(),"cod_versione")
	ls_cod_prodotto_spedito = dw_prod_uscita_bol_spedite.getitemstring(dw_prod_uscita_bol_spedite.getrow(),"cod_prodotto_spedito")
	ls_cod_reparto = dw_prod_uscita_bol_spedite.getitemstring(dw_prod_uscita_bol_spedite.getrow(),"cod_reparto")
	ls_cod_lavorazione = dw_prod_uscita_bol_spedite.getitemstring(dw_prod_uscita_bol_spedite.getrow(),"cod_lavorazione")
	ll_prog_spedizione = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"prog_spedizione")
	ll_prog_prodotto_spedito = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"prog_prod_spedito")
	ll_anno_registrazione = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"anno_registrazione")
	ll_num_registrazione = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"num_registrazione")
	ll_prog_riga_bol_ven = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"prog_riga_bol_ven")
	ldd_quan_spedita = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"quan_spedita")
	
	select flag_movimenti
	into   :ls_flag_movimenti
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
	       anno_registrazione = :ll_anno_registrazione 	and    
			 num_registrazione = :ll_num_registrazione;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 
	end if
	
	if ls_flag_movimenti = 'S' then
		g_mb.messagebox("Sep","Attenzione non è più possibile eliminare la spedizione/bolla poichè la bolla è già stata confermata.",stopsign!)
		return
	end if
	
	select cod_fornitore
	into   :ls_cod_fornitore
	from   tes_bol_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 
	end if
	
	select cod_deposito
	into   :ls_cod_deposito
	from   anag_fornitori
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_fornitore=:ls_cod_fornitore;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 
	end if

	
	if cbx_tutta.checked = true then

		//*******	
		//INIZIO DISIMPEGNO DEPOSITI DEI FORNITORI TERZISTI (caso tutta la bolla)
				
		//****************************
		//     ATTENZIONE!!!!!
		//		FATTO IN QUESTO MODO NON VA BENE IN QUANTO IN UNA STESSA BOLLA POTREBBERO ESSERCI SPEDIZIONI DIVERSE PERTANTO
		//    PER ORA SI LASCIA LA POSSIBILITA' ALL'UTENTE DI TOGLIERE UN PRODOTTO ALLA VOLTA DALLA BOLLA ( VEDI SOTTO)
		//    check box cbx_tutta è stata disabilitata: è sempre a false
		//**********************
		
		for ll_i = 1 to dw_lista_prodotti_spedizione.rowcount()
			
			ls_cod_prodotto_in_spedizione = dw_lista_prodotti_spedizione.getitemstring(ll_i,"cod_prodotto")			
			ldd_quan_necessaria = dw_lista_prodotti_spedizione.getitemnumber(ll_i,"quan_necessaria")
			
			select quan_impegnata
			into   :ldd_quan_impegnata
			from   impegni_deposito_fasi_com
			where  cod_azienda = :s_cs_xx.cod_azienda 	and    
					 anno_commessa = :li_anno_commessa 		and    
					 num_commessa = :ll_num_commessa 		and    
					 prog_riga = :ll_prog_riga					and    
					 cod_reparto = :ls_cod_reparto			and    
					 cod_lavorazione = :ls_cod_lavorazione	and    
					 cod_deposito = :ls_cod_deposito 		and    
					 cod_prodotto_fase = :ls_cod_prodotto_fase and   
					 cod_versione = :ls_cod_versione_fase  and
					 cod_prodotto_spedito = :ls_cod_prodotto_in_spedizione;
			
			if sqlca.sqlcode< 0 then
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				rollback;
				return 
			end if
			
			ldd_quan_impegnata = ldd_quan_impegnata - ldd_quan_necessaria
			
			if ldd_quan_impegnata < 0 then ldd_quan_impegnata = 0
			
			update impegni_deposito_fasi_com
			set    quan_impegnata = :ldd_quan_impegnata
			where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			       anno_commessa = :li_anno_commessa  	and    
					 num_commessa = :ll_num_commessa			and    
					 prog_riga = :ll_prog_riga					and    
					 cod_reparto = :ls_cod_reparto			and    
					 cod_lavorazione = :ls_cod_lavorazione	and    
					 cod_deposito = :ls_cod_deposito			and    
					 cod_prodotto_fase = :ls_cod_prodotto_fase and    
					 cod_versione = :ls_cod_versione_fase  and
					 cod_prodotto_spedito = :ls_cod_prodotto_in_spedizione;

			if sqlca.sqlcode< 0 then
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				rollback;
				return 
			end if

		next

				

		//FINE DISIMPEGNO DEPOSITI DEI FORNITORI TERZISTI
		//****************************

		
		delete prod_bolle_out_com
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven_stat
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven_corrispondenze
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete tes_bol_ven_corrispondenze
		where  cod_azienda=:s_cs_xx.cod_azienda

		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete tes_bol_ven_note
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete tes_bol_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
	
	else
		//*******
		//INIZIO DISIMPEGNO DEPOSITI DEI FORNITORI TERZISTI (caso singolo prodotto)		
		
		// PER ORA E' ABILITATO SOLO QUESTO CASO LA CHECK BOX CHE PERMETTEVA DI IMPOSTARE LA ELIMINAZIONE DI 
		// TUTTA LA BOLLA E' STATA DISABILITATA
		
		
//		
//		select sum(giacenza_stock)
//		into   :ldd_somma_giacenza
//		from   stock
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_deposito=:ls_cod_deposito
//		and    cod_prodotto=:ls_cod_prodotto_spedito;
//		
//		if sqlca.sqlcode< 0 then
//			messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//			rollback;
//			return 
//		end if

		select sum(quan_spedita)
		into   :ldd_somma_quan_spedita
		from   prod_bolle_out_com
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
				 anno_commessa = :li_anno_commessa 		and    
				 num_commessa = :ll_num_commessa			and    
				 prog_riga = :ll_prog_riga 				and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase and    
				 cod_versione = :ls_cod_versione_fase  and
				 cod_reparto = :ls_cod_reparto 			and    
				 cod_lavorazione = :ls_cod_lavorazione and    
				 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if

		// correggo la somma togliendo la quan_spedita con la riga corrente poichè dopo la riga di spedizione sarà eliminata
		ldd_somma_quan_spedita = ldd_somma_quan_spedita - ldd_quan_spedita 

		for ll_i = 1 to dw_lista_prodotti_spedizione.rowcount()
			ls_cod_prodotto_in_spedizione = dw_lista_prodotti_spedizione.getitemstring(ll_i,"cod_prodotto")
			ldd_quan_necessaria = dw_lista_prodotti_spedizione.getitemnumber(ll_i,"quan_necessaria")
			
			if ls_cod_prodotto_spedito = ls_cod_prodotto_in_spedizione then
				
				select quan_impegnata
				into   :ldd_quan_impegnata
				from   impegni_deposito_fasi_com
				where  cod_azienda = :s_cs_xx.cod_azienda and    
						 anno_commessa = :li_anno_commessa 	and    
						 num_commessa = :ll_num_commessa 	and    
						 prog_riga = :ll_prog_riga				and    
						 cod_reparto = :ls_cod_reparto		and    
						 cod_lavorazione = :ls_cod_lavorazione and    
						 cod_deposito = :ls_cod_deposito 		and    
						 cod_prodotto_fase = :ls_cod_prodotto_fase and
						 cod_versione = :ls_cod_versione_fase  and    
						 cod_prodotto_spedito = :ls_cod_prodotto_in_spedizione;
				
				if sqlca.sqlcode< 0 then
					g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					rollback;
					return 
				end if
		
				if ldd_somma_quan_spedita <= ldd_quan_necessaria then //se la somma della quantià spedita risulta minore della quan_necessaria devo diminuire la quan_impegnata
																				   	//altrimenti non fare niente
					ldd_quan_impegnata=ldd_somma_quan_spedita   	
					
					update impegni_deposito_fasi_com
					set    quan_impegnata = :ldd_quan_impegnata
					where  cod_azienda = :s_cs_xx.cod_azienda and    
							 anno_commessa = :li_anno_commessa  and    
							 num_commessa = :ll_num_commessa 	and    
							 prog_riga = :ll_prog_riga 			and    
							 cod_reparto = :ls_cod_reparto 		and    
							 cod_lavorazione = :ls_cod_lavorazione	and    
							 cod_deposito = :ls_cod_deposito			and    
							 cod_prodotto_fase = :ls_cod_prodotto_fase and
							 cod_versione = :ls_cod_versione_fase 		 and    
							 cod_prodotto_spedito = :ls_cod_prodotto_in_spedizione;
		
					if sqlca.sqlcode< 0 then
						g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
						rollback;
						return 
					end if
					
				end if
				
			end if
		next

		//FINE DISIMPEGNO DEPOSITI DEI FORNITORI TERZISTI
		//****************************
				
		delete prod_bolle_out_com
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 anno_commessa = :li_anno_commessa 		and    
				 num_commessa = :ll_num_commessa 		and    
				 prog_riga = :ll_prog_riga 		and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase and
				 cod_versione = :ls_cod_versione_fase 		 and    
				 cod_lavorazione = :ls_cod_lavorazione		and    
				 cod_reparto = :ls_cod_reparto		and    
				 prog_spedizione = :ll_prog_spedizione 		and    
				 prog_prod_spedito = :ll_prog_prodotto_spedito 	and    
				 anno_registrazione = :ll_anno_registrazione 	and    
				 num_registrazione = :ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven_stat
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    prog_riga_bol_ven=:ll_prog_riga_bol_ven;
		
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven_corrispondenze
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    prog_riga_bol_ven=:ll_prog_riga_bol_ven;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    prog_riga_bol_ven=:ll_prog_riga_bol_ven;
		
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
	end if
	commit;
	dw_prod_uscita_bol_spedite.change_dw_current()
	parent.triggerevent("pc_retrieve")
	g_mb.messagebox("Sep","Eliminazione spedizione/bolla avvenuta con successo",information!)
end if
end event

type cbx_tutta from checkbox within w_gen_bolle_terzisti
integer x = 3579
integer y = 1932
integer width = 434
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Tutta la Bolla"
boolean lefttext = true
end type

type cb_aggiungi_sl from commandbutton within w_gen_bolle_terzisti
integer x = 1874
integer y = 1936
integer width = 366
integer height = 80
integer taborder = 160
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi &SL"
end type

event clicked;string   ls_cod_prodotto_spedito,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, & 
			ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione,ls_cod_prodotto_fase,&
			ls_flag_mp,ls_cod_versione,ls_errore,ls_cod_versione_fase, ls_cod_versione_spedito
			
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, & 
			ll_prog_prodotto_spedito,ll_t,ll_anno_reg_sl,ll_num_reg_sl
datetime ldt_data_stock
double   ldd_quan_spedita,ldd_quan_in_produzione,ldd_quan_disponibile
integer  li_anno_commessa,li_risposta

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

dw_lista_prodotti_spedizione.setredraw( false)

dw_scelta_stock.resetupdate()

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")

ll_prog_spedizione = dw_scelta_fornitore.getitemnumber(dw_scelta_fornitore.getrow(),"num_nuova_spedizione")

select max(prog_prod_spedito)
into   :ll_prog_prodotto_spedito
from   prod_bolle_out_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and   
		 cod_versione = :ls_cod_versione_fase and
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 prog_spedizione = :ll_prog_spedizione;

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 
end if

if isnull(ll_prog_prodotto_spedito) then
	ll_prog_prodotto_spedito = 0
end if

ll_num_righe = dw_lista_prodotti_spedizione.rowcount()

for ll_t = 1 to ll_num_righe
	
//	dw_lista_prodotti_spedizione.setrow(ll_t)
	ls_flag_mp = dw_lista_prodotti_spedizione.getitemstring(ll_t,"flag_mat_prima")
	ldd_quan_disponibile = dw_lista_prodotti_spedizione.getitemnumber(ll_t,"quan_disponibile")
	dw_lista_prodotti_spedizione.setcolumn("quan_spedizione")	
	ldd_quan_spedita = double(dw_lista_prodotti_spedizione.gettext())

	if ldd_quan_spedita > ldd_quan_disponibile then
		g_mb.messagebox("Sep","Attenzione la quantita disponibile è inferiore alla quantita da spedire, reinseire la quantita.",stopsign!)
		return
	end if
	
	if ls_flag_mp = 'N' and ldd_quan_spedita > 0 then
		
		ll_prog_prodotto_spedito++			
		ls_cod_prodotto_spedito = dw_lista_prodotti_spedizione.getitemstring( ll_t, "cod_prodotto")
		ls_cod_versione_spedito = dw_lista_prodotti_spedizione.getitemstring( ll_t, "cod_versione")
	
		select anno_reg_sl,
				 num_reg_sl
		into   :ll_anno_reg_sl,
				 :ll_num_reg_sl
		from   avan_produzione_com
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 anno_commessa = :li_anno_commessa 	and    
				 num_commessa = :ll_num_commessa		and    
				 prog_riga = :ll_prog_riga				and    
				 cod_prodotto = :ls_cod_prodotto_spedito and
				 cod_versione = :ls_cod_versione_spedito;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return
		end if
 
		select cod_deposito,
				 cod_ubicazione,
				 cod_lotto,
				 data_stock,
				 prog_stock
		into   :ls_cod_deposito,
				 :ls_cod_ubicazione,
				 :ls_cod_lotto,
				 :ldt_data_stock,
				 :ll_prog_stock
		from   mov_magazzino
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 anno_registrazione = :ll_anno_reg_sl and    
				 num_registrazione = :ll_num_reg_sl;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
		INSERT INTO prod_bolle_out_com
						( cod_azienda,   
						  anno_commessa,   
						  num_commessa,   
						  cod_prodotto_fase, 
						  cod_versione,
						  prog_riga,  
						  cod_reparto,
						  cod_lavorazione,
						  prog_spedizione,
						  prog_prod_spedito,
						  anno_registrazione,
						  num_registrazione,
						  prog_riga_bol_ven,
						  cod_prodotto_spedito,
						  cod_deposito,   
						  cod_ubicazione,   
						  cod_lotto,   
						  data_stock,   
						  prog_stock,   				  
						  quan_spedita,
						  flag_mp)  
			  VALUES ( :s_cs_xx.cod_azienda,   
						  :li_anno_commessa,   
						  :ll_num_commessa,   
						  :ls_cod_prodotto_fase, 
						  :ls_cod_versione_fase,
						  :ll_prog_riga,   
						  :ls_cod_reparto,
						  :ls_cod_lavorazione,
						  :ll_prog_spedizione,
						  :ll_prog_prodotto_spedito,
						  null,
						  null,
						  null,
						  :ls_cod_prodotto_spedito,
						  :ls_cod_deposito,   
						  :ls_cod_ubicazione,   
						  :ls_cod_lotto,   
						  :ldt_data_stock,   
						  :ll_prog_stock, 
						  :ldd_quan_spedita,
						  'N');
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
	end if
next

commit;

dw_lista_prodotti_spedizione.reset()


// *** Michela 06/10/06: la versione del prodotto ce l'ho collegata allo stesso
//select cod_versione
//into   :ls_cod_versione
//from   anag_commesse
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    anno_commessa=:li_anno_commessa
//and    num_commessa=:ll_num_commessa;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//	return 
//end if

li_risposta = wf_imposta_prodotti_spedizione( li_anno_commessa, &
															 ll_num_commessa, &
															 ll_prog_riga, &
															 ls_cod_prodotto_fase, &
															 ls_cod_versione_fase, &
															 ldd_quan_in_produzione, &
															 1, &
															 ls_errore)

if li_risposta <0 then
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		return
end if

wf_imposta_stock()
dw_scelta_stock.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
dw_elenco_prod_uscita_bol.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()
dw_lista_prodotti_spedizione.setredraw(true)

end event

type cb_conferma_stock from commandbutton within w_gen_bolle_terzisti
integer x = 3392
integer y = 1592
integer width = 366
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi M.P."
end type

event clicked;string   ls_cod_prodotto_spedito,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, ls_cod_deposito_fornitore, &
			ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_cod_fornitore, & 
			ls_cod_prodotto_fase,ls_cod_versione,ls_errore, ls_cod_versione_fase, ls_cod_versione_spedito
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, & 
			ll_prog_prodotto_spedito,ll_t
datetime ldt_data_stock
dec{4}   ldd_quan_spedita,ldd_quan_in_produzione,ldd_quan_disponibile,ldd_quan_impegnata,ldd_quan_necessaria, & 
			ldd_nuova_quan_impegnata,ldd_somma_quan_spedita
integer  li_anno_commessa,li_risposta

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

dw_scelta_fornitore.accepttext()

ls_cod_fornitore = dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"cod_fornitore")

if isnull (ls_cod_fornitore) or len(trim(ls_cod_fornitore)) < 1 then
	g_mb.messagebox("Sep","Selezionare un Fornitore dal folder Spedizione !",stopsign!)
	return
end if

select cod_deposito
into   :ls_cod_deposito_fornitore
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 cod_fornitore = :ls_cod_fornitore;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Attenzione: selezionare il fornitore dal folder Spedizione; in questo modo si assegna il deposito di invio. " + sqlca.sqlerrtext,stopsign!)
	return 
end if

if isnull (ls_cod_deposito_fornitore) or len(trim(ls_cod_deposito_fornitore)) < 1 then
	g_mb.messagebox("Sep","Attenzione! il fornitore non è associato ad alcun deposito.",stopsign!)
	return
end if


li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")

ldd_quan_necessaria = dw_lista_prodotti_spedizione.getitemnumber(dw_lista_prodotti_spedizione.getrow(),"quan_necessaria")
ls_cod_prodotto_spedito = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"cod_prodotto")
ls_cod_versione_spedito = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"cod_versione")

select sum(quan_impegnata)
into   :ldd_quan_impegnata
from   impegni_deposito_fasi_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and
		 cod_versione = :ls_cod_versione_fase and    
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 prog_riga = :ll_prog_riga          and    
		 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Sep","Errore nella totalizzazione impegnato da  impegni_deposito_fasi_com ~r~n" + sqlca.sqlerrtext,stopsign!)
	return 
end if

if isnull(ldd_quan_impegnata) then ldd_quan_impegnata = 0

ldd_nuova_quan_impegnata = ldd_quan_necessaria - ldd_quan_impegnata

update impegni_deposito_fasi_com
set    quan_impegnata_ultima = 0
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa and    
		 prog_riga = :ll_prog_riga and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and    
		 cod_versione = :ls_cod_versione_fase and
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 cod_deposito = :ls_cod_deposito_fornitore and    
		 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in aggiornamento tabella impegni_deposito_fasi_com ~r~n" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 
end if

ll_prog_spedizione = dw_scelta_fornitore.getitemnumber(dw_scelta_fornitore.getrow(),"num_nuova_spedizione")

select max(prog_prod_spedito)
into   :ll_prog_prodotto_spedito
from   prod_bolle_out_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa  and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and
		 cod_versione = :ls_cod_versione_fase and    
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 prog_spedizione = :ll_prog_spedizione;

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Sep","Errore in ricerca nuovo progressivo spedizione~r~n" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 
end if

if isnull(ll_prog_prodotto_spedito) then
	ll_prog_prodotto_spedito=0
end if

ll_num_righe = dw_scelta_stock.rowcount()

dw_scelta_stock.accepttext()

for ll_t=1 to ll_num_righe
	ll_prog_prodotto_spedito++	
//	dw_scelta_stock.setrow(ll_t)
	ls_cod_deposito = dw_scelta_stock.getitemstring(ll_t,"cod_deposito")		
	ls_cod_ubicazione = dw_scelta_stock.getitemstring(ll_t,"cod_ubicazione")
	ls_cod_lotto = dw_scelta_stock.getitemstring(ll_t,"cod_lotto")
	ldt_data_stock = dw_scelta_stock.getitemdatetime(ll_t,"data_stock")
	ll_prog_stock = dw_scelta_stock.getitemnumber(ll_t,"prog_stock")
	ls_cod_prodotto_spedito=dw_scelta_stock.getitemstring(ll_t,"cod_prodotto")
   ldd_quan_disponibile = dw_scelta_stock.getitemnumber(ll_t,"quan_disponibile")
//   dw_scelta_stock.setcolumn("quan_prelevata")	
	ldd_quan_spedita = dw_scelta_stock.getitemnumber(ll_t, "quan_prelevata")

	if  ldd_quan_disponibile < ldd_quan_spedita then
		g_mb.messagebox("Sep","Attenzione! La quantita da spedire supera la quantità disponibile nello stock. Reinserire la quantita da spedire.",stopsign!)
		rollback;
		return
	end if
	
	if ldd_quan_spedita > 0 then
		
		INSERT INTO prod_bolle_out_com
				( cod_azienda,   
				  anno_commessa,   
				  num_commessa,   
				  cod_prodotto_fase, 
				  cod_versione,
				  prog_riga,  
				  cod_reparto,
				  cod_lavorazione,
				  prog_spedizione,
				  prog_prod_spedito,
				  anno_registrazione,
				  num_registrazione,
				  prog_riga_bol_ven,
				  cod_prodotto_spedito,
				  cod_deposito,   
				  cod_ubicazione,   
				  cod_lotto,   
				  data_stock,   
				  prog_stock,   				  
				  quan_spedita,
				  flag_mp)  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :li_anno_commessa,   
				  :ll_num_commessa,   
				  :ls_cod_prodotto_fase, 
				  :ls_cod_versione_fase,
				  :ll_prog_riga,   
				  :ls_cod_reparto,
				  :ls_cod_lavorazione,
				  :ll_prog_spedizione,
				  :ll_prog_prodotto_spedito,
				  null,
				  null,
				  null,
				  :ls_cod_prodotto_spedito,
				  :ls_cod_deposito,   
				  :ls_cod_ubicazione,   
				  :ls_cod_lotto,   
				  :ldt_data_stock,   
				  :ll_prog_stock, 
				  :ldd_quan_spedita,
				  'S');
				  
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore in registrazione spedizione in prod_bolle_out_com~r~n" + sqlca.sqlerrtext,stopsign!)
			rollback;
			return
		end if
		
		ldd_somma_quan_spedita = ldd_somma_quan_spedita + ldd_quan_spedita
		
	end if
	
next

if ldd_somma_quan_spedita <= ldd_nuova_quan_impegnata then ldd_nuova_quan_impegnata = ldd_somma_quan_spedita

select cod_azienda
into   :ls_test
from   impegni_deposito_fasi_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa and    
		 prog_riga = :ll_prog_riga  and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and
		 cod_versione = :ls_cod_versione_fase and    
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 cod_deposito = :ls_cod_deposito_fornitore and    
		 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in verifica presenza impegnati precedenti in impegni_deposito_fasi_comÈr~n" + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if

if sqlca.sqlcode = 100 or isnull(ls_test) or ls_test ="" then
	
	insert into impegni_deposito_fasi_com
	(cod_azienda,
	 anno_commessa,
	 num_commessa,
	 prog_riga,
	 cod_prodotto_fase,
	 cod_versione,
	 cod_reparto,
	 cod_lavorazione,
	 cod_deposito,
	 cod_prodotto_spedito,
	 quan_impegnata,
	 quan_impegnata_ultima)
	values
	(:s_cs_xx.cod_azienda,
	 :li_anno_commessa,
	 :ll_num_commessa,
	 :ll_prog_riga,
	 :ls_cod_prodotto_fase,
	 :ls_cod_versione_fase,
	 :ls_cod_reparto,
	 :ls_cod_lavorazione,
	 :ls_cod_deposito_fornitore,
	 :ls_cod_prodotto_spedito,
	 :ldd_nuova_quan_impegnata,
	 :ldd_nuova_quan_impegnata);

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore in aggiunta impegnato in impegni_deposito_fasi_com~r~n " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return
	end if

else

	if sqlca.sqlcode = 0 then
		
		update impegni_deposito_fasi_com
		set    quan_impegnata = quan_impegnata + :ldd_nuova_quan_impegnata,
				 quan_impegnata_ultima =:ldd_nuova_quan_impegnata
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
				 anno_commessa = :li_anno_commessa 		and    
				 num_commessa = :ll_num_commessa 		and    
				 prog_riga = :ll_prog_riga 		and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase and
				 cod_versione = :ls_cod_versione_fase 		and    
				 cod_reparto = :ls_cod_reparto 		and    
				 cod_lavorazione = :ls_cod_lavorazione 		and    
				 cod_deposito = :ls_cod_deposito_fornitore 	and    
				 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore in aggiornamento impegnato in impegni_deposito_fasi_com~r~n" + sqlca.sqlerrtext,stopsign!)
			rollback;
			return
		end if
		
	end if		
	
end if


commit;

dw_lista_prodotti_spedizione.reset()


// Michela 06/10/06: la versione è collegata al prodotto, non al prodotto principale
//select cod_versione
//into   :ls_cod_versione
//from   anag_commesse
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    anno_commessa=:li_anno_commessa
//and    num_commessa=:ll_num_commessa;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//	return 
//end if

li_risposta = wf_imposta_prodotti_spedizione( li_anno_commessa, &
															 ll_num_commessa, &
															 ll_prog_riga, &
															 ls_cod_prodotto_fase, &
															 ls_cod_versione_fase, &
															 ldd_quan_in_produzione, &
															 1, &
															 ls_errore)

if li_risposta <0 then
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		rollback;
		return
end if


dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

wf_imposta_stock()

dw_scelta_stock.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
dw_elenco_prod_uscita_bol.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

commit;
end event

type cb_sblocca_stock from commandbutton within w_gen_bolle_terzisti
integer x = 4046
integer y = 1928
integer width = 366
integer height = 80
integer taborder = 150
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Togli"
end type

event clicked;string   ls_cod_prodotto_spedito,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, ls_errore,& 
			ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione,ls_cod_prodotto_fase, &
			ls_cod_versione,ls_cod_fornitore, ls_cod_versione_fase
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, & 
			ll_prog_prodotto_spedito,ll_t,ll_prog_prod_spedito
datetime ldt_data_stock
double   ldd_quan_spedita,ldd_quan_in_produzione,ldd_quan_impegnata_ultima,ldd_quan_impegnata
integer  li_anno_commessa,li_risposta

if dw_elenco_prod_uscita_bol.rowcount() = 0 then return

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

ls_cod_fornitore = dw_scelta_fornitore.gettext()

select cod_deposito
into   :ls_cod_deposito
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_fornitore=:ls_cod_fornitore;

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")
ll_prog_spedizione = dw_elenco_prod_uscita_bol.getitemnumber(dw_elenco_prod_uscita_bol.getrow(),"prog_spedizione")
ll_prog_prod_spedito = dw_elenco_prod_uscita_bol.getitemnumber(dw_elenco_prod_uscita_bol.getrow(),"prog_prod_spedito")
ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")
ls_cod_prodotto_spedito = dw_elenco_prod_uscita_bol.getitemstring(dw_elenco_prod_uscita_bol.getrow(),"cod_prodotto_spedito")

select quan_impegnata,
		 quan_impegnata_ultima
into   :ldd_quan_impegnata,
       :ldd_quan_impegnata_ultima
from   impegni_deposito_fasi_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 prog_riga = :ll_prog_riga          and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and
		 cod_versione = :ls_cod_versione_fase and    
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 cod_deposito = :ls_cod_deposito and    
		 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

if ldd_quan_impegnata = ldd_quan_impegnata_ultima then
	
	delete impegni_deposito_fasi_com
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			 anno_commessa = :li_anno_commessa 		and    
			 num_commessa = :ll_num_commessa 		and    
			 prog_riga = :ll_prog_riga 				and    
			 cod_prodotto_fase = :ls_cod_prodotto_fase 	and   
			 cod_versione = :ls_cod_versione_fase  and
			 cod_reparto = :ls_cod_reparto 			and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 cod_deposito = :ls_cod_deposito 		and    
			 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if 
	
else
	update impegni_deposito_fasi_com
	set    quan_impegnata=quan_impegnata - quan_impegnata_ultima,
			 quan_impegnata_ultima=0
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			 anno_commessa = :li_anno_commessa 		and    
			 num_commessa = :ll_num_commessa 		and    
			 prog_riga = :ll_prog_riga 				and    
			 cod_prodotto_fase = :ls_cod_prodotto_fase and
			 cod_versione = :ls_cod_versione_fase  and
			 cod_reparto = :ls_cod_reparto 			and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 cod_deposito = :ls_cod_deposito 	and    
			 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if 

end if

delete prod_bolle_out_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa		and    
		 prog_riga = :ll_prog_riga 			and    
		 cod_reparto = :ls_cod_reparto 		and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 prog_spedizione = :ll_prog_spedizione and    
		 prog_prod_spedito = :ll_prog_prod_spedito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

commit;

dw_lista_prodotti_spedizione.reset()


// *** Michela 06/10/06: la versione è collegata al prodotto della fase
//select cod_versione
//into   :ls_cod_versione
//from   anag_commesse
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    anno_commessa=:li_anno_commessa
//and    num_commessa=:ll_num_commessa;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//	return 
//end if

li_risposta = wf_imposta_prodotti_spedizione( li_anno_commessa, &
															 ll_num_commessa, &
															 ll_prog_riga, &
															 ls_cod_prodotto_fase, &
															 ls_cod_versione_fase, &
															 ldd_quan_in_produzione, &
															 1, &
															 ls_errore)

if li_risposta <0 then
	g_mb.messagebox("Sep",ls_errore,stopsign!)
	return
end if

wf_imposta_stock()
dw_elenco_prod_uscita_bol.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
dw_stato_stock_terzista.change_dw_current()
parent.triggerevent("pc_retrieve")

dw_scelta_stock.change_dw_current()
parent.triggerevent("pc_retrieve")

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
//dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

end event

type dw_elenco_prod_uscita_bol from uo_cs_xx_dw within w_gen_bolle_terzisti
integer x = 91
integer y = 500
integer width = 4347
integer height = 1412
integer taborder = 50
string dataobject = "d_gen_bolle_terzisti_elenco_prod_spediti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga,ll_prog_spedizione
string ls_cod_prodotto_fase,ls_cod_reparto,ls_cod_lavorazione, ls_cod_versione_fase

ll_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")	
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		

if dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(), "flag_bolla_unica") = "S" then
	ls_cod_prodotto_fase = '%'
	ls_cod_reparto = '%'
	ls_cod_lavorazione ='%'
end if

l_Error = Retrieve( s_cs_xx.cod_azienda, &
						  ll_anno_commessa, &
						  ll_num_commessa, &
						  ll_prog_riga, &
						  ls_cod_prodotto_fase, & 
						  ls_cod_versione_fase, &
						  ls_cod_reparto, &
						  ls_cod_lavorazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_folder from u_folder within w_gen_bolle_terzisti
integer x = 23
integer y = 376
integer width = 4448
integer height = 2048
integer taborder = 130
end type

type dw_scelta_stock from uo_cs_xx_dw within w_gen_bolle_terzisti
integer x = 55
integer y = 1264
integer width = 3319
integer height = 520
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_gen_bolle_terzisti_selezione_stock"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;if i_extendmode then
	if isvalid(dwo) then
		if dwo.name = "cb_aggiungi" then
			
			
			string   ls_cod_prodotto_spedito,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, ls_cod_deposito_fornitore, &
						ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_cod_fornitore, & 
						ls_cod_prodotto_fase,ls_cod_versione,ls_errore, ls_cod_versione_fase, ls_cod_versione_spedito
			long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, & 
						ll_prog_prodotto_spedito,ll_t
			datetime ldt_data_stock
			dec{4}   ldd_quan_spedita,ldd_quan_in_produzione,ldd_quan_disponibile,ldd_quan_impegnata,ldd_quan_necessaria, & 
						ldd_nuova_quan_impegnata,ldd_somma_quan_spedita
			integer  li_anno_commessa,li_risposta
			
			dw_elenco_fasi_esterne_attivazione.resetupdate()
			dw_elenco_prod_uscita_bol.resetupdate()
			dw_lista_prodotti_spedizione.resetupdate()
			dw_prod_uscita_bol_spedite.resetupdate()
			dw_scelta_fornitore.resetupdate()
			dw_scelta_stock.resetupdate()
			
			dw_scelta_fornitore.accepttext()
			
			ls_cod_fornitore = dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"cod_fornitore")
			
			if isnull (ls_cod_fornitore) or len(trim(ls_cod_fornitore)) < 1 then
				g_mb.messagebox("Sep","Selezionare un Fornitore dal folder Spedizione !",stopsign!)
				return
			end if
			
			select cod_deposito
			into   :ls_cod_deposito_fornitore
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and    
					 cod_fornitore = :ls_cod_fornitore;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Attenzione: selezionare il fornitore dal folder Spedizione; in questo modo si assegna il deposito di invio. " + sqlca.sqlerrtext,stopsign!)
				return 
			end if
			
			if isnull (ls_cod_deposito_fornitore) or len(trim(ls_cod_deposito_fornitore)) < 1 then
				g_mb.messagebox("Sep","Attenzione! il fornitore non è associato ad alcun deposito.",stopsign!)
				return
			end if
			
			
			li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
			ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
			ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
			ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
			ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
			ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
			ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
			ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")
			
			ldd_quan_necessaria = dw_lista_prodotti_spedizione.getitemnumber(dw_lista_prodotti_spedizione.getrow(),"quan_necessaria")
			ls_cod_prodotto_spedito = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"cod_prodotto")
			ls_cod_versione_spedito = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"cod_versione")
			
			select sum(quan_impegnata)
			into   :ldd_quan_impegnata
			from   impegni_deposito_fasi_com
			where  cod_azienda = :s_cs_xx.cod_azienda and    
					 anno_commessa = :li_anno_commessa  and    
					 num_commessa = :ll_num_commessa    and    
					 cod_prodotto_fase = :ls_cod_prodotto_fase and
					 cod_versione = :ls_cod_versione_fase and    
					 cod_reparto = :ls_cod_reparto and    
					 cod_lavorazione = :ls_cod_lavorazione and    
					 prog_riga = :ll_prog_riga          and    
					 cod_prodotto_spedito = :ls_cod_prodotto_spedito;
			
			if sqlca.sqlcode< 0 then
				g_mb.messagebox("Sep","Errore nella totalizzazione impegnato da  impegni_deposito_fasi_com ~r~n" + sqlca.sqlerrtext,stopsign!)
				return 
			end if
			
			if isnull(ldd_quan_impegnata) then ldd_quan_impegnata = 0
			
			ldd_nuova_quan_impegnata = ldd_quan_necessaria - ldd_quan_impegnata
			
			update impegni_deposito_fasi_com
			set    quan_impegnata_ultima = 0
			where  cod_azienda = :s_cs_xx.cod_azienda and    
					 anno_commessa = :li_anno_commessa and    
					 num_commessa = :ll_num_commessa and    
					 prog_riga = :ll_prog_riga and    
					 cod_prodotto_fase = :ls_cod_prodotto_fase and    
					 cod_versione = :ls_cod_versione_fase and
					 cod_reparto = :ls_cod_reparto and    
					 cod_lavorazione = :ls_cod_lavorazione and    
					 cod_deposito = :ls_cod_deposito_fornitore and    
					 cod_prodotto_spedito = :ls_cod_prodotto_spedito;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore in aggiornamento tabella impegni_deposito_fasi_com ~r~n" + sqlca.sqlerrtext,stopsign!)
				rollback;
				return 
			end if
			
			ll_prog_spedizione = dw_scelta_fornitore.getitemnumber(dw_scelta_fornitore.getrow(),"num_nuova_spedizione")
			
			select max(prog_prod_spedito)
			into   :ll_prog_prodotto_spedito
			from   prod_bolle_out_com
			where  cod_azienda = :s_cs_xx.cod_azienda and    
					 anno_commessa = :li_anno_commessa and    
					 num_commessa = :ll_num_commessa  and    
					 cod_prodotto_fase = :ls_cod_prodotto_fase and
					 cod_versione = :ls_cod_versione_fase and    
					 cod_reparto = :ls_cod_reparto and    
					 cod_lavorazione = :ls_cod_lavorazione and    
					 prog_spedizione = :ll_prog_spedizione;
			
			if sqlca.sqlcode< 0 then
				g_mb.messagebox("Sep","Errore in ricerca nuovo progressivo spedizione~r~n" + sqlca.sqlerrtext,stopsign!)
				rollback;
				return 
			end if
			
			if isnull(ll_prog_prodotto_spedito) then
				ll_prog_prodotto_spedito=0
			end if
			
			ll_num_righe = dw_scelta_stock.rowcount()
			
			dw_scelta_stock.accepttext()
			
			for ll_t=1 to ll_num_righe
				ll_prog_prodotto_spedito++	
			//	dw_scelta_stock.setrow(ll_t)
				ls_cod_deposito = dw_scelta_stock.getitemstring(ll_t,"cod_deposito")		
				ls_cod_ubicazione = dw_scelta_stock.getitemstring(ll_t,"cod_ubicazione")
				ls_cod_lotto = dw_scelta_stock.getitemstring(ll_t,"cod_lotto")
				ldt_data_stock = dw_scelta_stock.getitemdatetime(ll_t,"data_stock")
				ll_prog_stock = dw_scelta_stock.getitemnumber(ll_t,"prog_stock")
				ls_cod_prodotto_spedito=dw_scelta_stock.getitemstring(ll_t,"cod_prodotto")
				ldd_quan_disponibile = dw_scelta_stock.getitemnumber(ll_t,"quan_disponibile")
			//   dw_scelta_stock.setcolumn("quan_prelevata")	
				ldd_quan_spedita = dw_scelta_stock.getitemnumber(ll_t, "quan_prelevata")
			
				if  ldd_quan_disponibile < ldd_quan_spedita then
					g_mb.messagebox("Sep","Attenzione! La quantita da spedire supera la quantità disponibile nello stock. Reinserire la quantita da spedire.",stopsign!)
					rollback;
					return
				end if
				
				if ldd_quan_spedita > 0 then
					
					INSERT INTO prod_bolle_out_com
							( cod_azienda,   
							  anno_commessa,   
							  num_commessa,   
							  cod_prodotto_fase, 
							  cod_versione,
							  prog_riga,  
							  cod_reparto,
							  cod_lavorazione,
							  prog_spedizione,
							  prog_prod_spedito,
							  anno_registrazione,
							  num_registrazione,
							  prog_riga_bol_ven,
							  cod_prodotto_spedito,
							  cod_deposito,   
							  cod_ubicazione,   
							  cod_lotto,   
							  data_stock,   
							  prog_stock,   				  
							  quan_spedita,
							  flag_mp)  
				  VALUES ( :s_cs_xx.cod_azienda,   
							  :li_anno_commessa,   
							  :ll_num_commessa,   
							  :ls_cod_prodotto_fase, 
							  :ls_cod_versione_fase,
							  :ll_prog_riga,   
							  :ls_cod_reparto,
							  :ls_cod_lavorazione,
							  :ll_prog_spedizione,
							  :ll_prog_prodotto_spedito,
							  null,
							  null,
							  null,
							  :ls_cod_prodotto_spedito,
							  :ls_cod_deposito,   
							  :ls_cod_ubicazione,   
							  :ls_cod_lotto,   
							  :ldt_data_stock,   
							  :ll_prog_stock, 
							  :ldd_quan_spedita,
							  'S');
							  
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Sep","Errore in registrazione spedizione in prod_bolle_out_com~r~n" + sqlca.sqlerrtext,stopsign!)
						rollback;
						return
					end if
					
					ldd_somma_quan_spedita = ldd_somma_quan_spedita + ldd_quan_spedita
					
				end if
				
			next
			
			if ldd_somma_quan_spedita <= ldd_nuova_quan_impegnata then ldd_nuova_quan_impegnata = ldd_somma_quan_spedita
			
			select cod_azienda
			into   :ls_test
			from   impegni_deposito_fasi_com
			where  cod_azienda = :s_cs_xx.cod_azienda and    
					 anno_commessa = :li_anno_commessa and    
					 num_commessa = :ll_num_commessa and    
					 prog_riga = :ll_prog_riga  and    
					 cod_prodotto_fase = :ls_cod_prodotto_fase and
					 cod_versione = :ls_cod_versione_fase and    
					 cod_reparto = :ls_cod_reparto and    
					 cod_lavorazione = :ls_cod_lavorazione and    
					 cod_deposito = :ls_cod_deposito_fornitore and    
					 cod_prodotto_spedito = :ls_cod_prodotto_spedito;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore in verifica presenza impegnati precedenti in impegni_deposito_fasi_comÈr~n" + sqlca.sqlerrtext,stopsign!)
				rollback;
				return
			end if
			
			if sqlca.sqlcode = 100 or isnull(ls_test) or ls_test ="" then
				
				insert into impegni_deposito_fasi_com
				(cod_azienda,
				 anno_commessa,
				 num_commessa,
				 prog_riga,
				 cod_prodotto_fase,
				 cod_versione,
				 cod_reparto,
				 cod_lavorazione,
				 cod_deposito,
				 cod_prodotto_spedito,
				 quan_impegnata,
				 quan_impegnata_ultima)
				values
				(:s_cs_xx.cod_azienda,
				 :li_anno_commessa,
				 :ll_num_commessa,
				 :ll_prog_riga,
				 :ls_cod_prodotto_fase,
				 :ls_cod_versione_fase,
				 :ls_cod_reparto,
				 :ls_cod_lavorazione,
				 :ls_cod_deposito_fornitore,
				 :ls_cod_prodotto_spedito,
				 :ldd_nuova_quan_impegnata,
				 :ldd_nuova_quan_impegnata);
			
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore in aggiunta impegnato in impegni_deposito_fasi_com~r~n " + sqlca.sqlerrtext,stopsign!)
					rollback;
					return
				end if
			
			else
			
				if sqlca.sqlcode = 0 then
					
					update impegni_deposito_fasi_com
					set    quan_impegnata = quan_impegnata + :ldd_nuova_quan_impegnata,
							 quan_impegnata_ultima =:ldd_nuova_quan_impegnata
					where  cod_azienda = :s_cs_xx.cod_azienda 	and    
							 anno_commessa = :li_anno_commessa 		and    
							 num_commessa = :ll_num_commessa 		and    
							 prog_riga = :ll_prog_riga 		and    
							 cod_prodotto_fase = :ls_cod_prodotto_fase and
							 cod_versione = :ls_cod_versione_fase 		and    
							 cod_reparto = :ls_cod_reparto 		and    
							 cod_lavorazione = :ls_cod_lavorazione 		and    
							 cod_deposito = :ls_cod_deposito_fornitore 	and    
							 cod_prodotto_spedito = :ls_cod_prodotto_spedito;
			
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Sep","Errore in aggiornamento impegnato in impegni_deposito_fasi_com~r~n" + sqlca.sqlerrtext,stopsign!)
						rollback;
						return
					end if
					
				end if		
				
			end if
			
			
			commit;
			
			dw_lista_prodotti_spedizione.reset()
			
			
			// Michela 06/10/06: la versione è collegata al prodotto, non al prodotto principale
			//select cod_versione
			//into   :ls_cod_versione
			//from   anag_commesse
			//where  cod_azienda=:s_cs_xx.cod_azienda
			//and    anno_commessa=:li_anno_commessa
			//and    num_commessa=:ll_num_commessa;
			//
			//if sqlca.sqlcode < 0 then
			//	messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			//	return 
			//end if
			
			li_risposta = wf_imposta_prodotti_spedizione( li_anno_commessa, &
																		 ll_num_commessa, &
																		 ll_prog_riga, &
																		 ls_cod_prodotto_fase, &
																		 ls_cod_versione_fase, &
																		 ldd_quan_in_produzione, &
																		 1, &
																		 ls_errore)
			
			if li_risposta <0 then
					g_mb.messagebox("Sep",ls_errore,stopsign!)
					rollback;
					return
			end if
			
			
			dw_elenco_fasi_esterne_attivazione.resetupdate()
			dw_elenco_prod_uscita_bol.resetupdate()
			dw_lista_prodotti_spedizione.resetupdate()
			dw_prod_uscita_bol_spedite.resetupdate()
			dw_scelta_fornitore.resetupdate()
			dw_scelta_stock.resetupdate()
			
			wf_imposta_stock()
			
			dw_scelta_stock.Change_DW_Current( )
			parent.triggerevent("pc_retrieve")
			dw_elenco_prod_uscita_bol.Change_DW_Current( )
			parent.triggerevent("pc_retrieve")
			dw_elenco_fasi_esterne_attivazione.resetupdate()
			dw_elenco_prod_uscita_bol.resetupdate()
			dw_lista_prodotti_spedizione.resetupdate()
			dw_prod_uscita_bol_spedite.resetupdate()
			dw_scelta_fornitore.resetupdate()
			dw_scelta_stock.resetupdate()
			
			commit;			
		end if
	end if
end if


end event

type dw_lista_prodotti_spedizione from uo_cs_xx_dw within w_gen_bolle_terzisti
integer x = 41
integer y = 500
integer width = 4384
integer height = 752
integer taborder = 90
boolean bringtotop = true
string dataobject = "d_lista_prodotti_spedizione"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event clicked;call super::clicked;if i_extendmode then
	wf_imposta_stock()
	dw_stato_stock_terzista.change_dw_current()
	parent.triggerevent("pc_retrieve")
end if
end event

event rowfocuschanged;call super::rowfocuschanged;//if i_extendmode then
//	if currentrow = 0 then return
//	wf_imposta_stock()
//end if

dw_depositi_lista.postevent("ue_retrieve")
end event

type cb_carica_note from commandbutton within w_gen_bolle_terzisti
integer x = 3598
integer y = 532
integer width = 366
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Carica Note"
end type

event clicked;string ls_cod_prodotto_finito,ls_des_prodotto_finito
long   ll_row

select cod_prodotto
into   :ls_cod_prodotto_finito
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :il_anno_commessa and    
		 num_commessa = :il_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

select des_prodotto
into   :ls_des_prodotto_finito
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_prodotto = :ls_cod_prodotto_finito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

dw_note_spedizione.reset()
ll_row = dw_note_spedizione.insertrow(0)

dw_note_spedizione.setitem(ll_row,"des_riferimento", "Ns. Commessa Anno:" + string(il_anno_commessa) + " Nr.:" + string(il_num_commessa))

//sle_riga_riferimento.text = "Ns. Commessa Anno:" + string(il_anno_commessa) + " Nr.:" + string(il_num_commessa)

dw_note_spedizione.setitem(ll_row,"des_riferimento", "Prodotto finito: " + ls_cod_prodotto_finito + " / " + ls_des_prodotto_finito)

//mle_nota_testata.text="Prodotto finito: " + ls_cod_prodotto_finito + " / " + ls_des_prodotto_finito

end event

type dw_note_spedizione from datawindow within w_gen_bolle_terzisti
integer x = 64
integer y = 504
integer width = 3351
integer height = 988
integer taborder = 200
string title = "none"
string dataobject = "d_gen_bolle_terzisti_note_spedizione"
boolean border = false
boolean livescroll = true
end type

type dw_prod_uscita_bol_spedite from uo_cs_xx_dw within w_gen_bolle_terzisti
integer x = 91
integer y = 500
integer width = 4347
integer height = 1412
integer taborder = 120
string dataobject = "d_gen_bolle_terzisti_elenco_spedizioni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga,ll_prog_spedizione
string ls_cod_prodotto_fase,ls_cod_reparto,ls_cod_lavorazione, ls_cod_versione_fase

ll_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		

l_Error = Retrieve( s_cs_xx.cod_azienda, &
						  ll_anno_commessa, &
						  ll_num_commessa, &
						  ll_prog_riga, &
						  ls_cod_prodotto_fase, & 
						  ls_cod_versione_fase, &
						  ls_cod_reparto, &
						  ls_cod_lavorazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type cb_bolla_uscita from commandbutton within w_gen_bolle_terzisti
integer x = 3991
integer y = 532
integer width = 366
integer height = 80
integer taborder = 180
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera Bolla"
end type

event clicked;string   ls_cod_prodotto_spedito,ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_flag_mp, & 
			ls_cod_prodotto_fase,ls_cod_versione,ls_errore,ls_cod_fornitore,ls_cod_mov_magazzino,ls_cod_tipo_det_ven_sl,&
			ls_cod_tipo_det_ven,ls_cod_tipo_bol_ven,ls_conferma,ls_des_prodotto_finito,ls_cod_prodotto_finito,ls_des_estesa_prodotto, &
			ls_cod_versione_fase,ls_aggiungi_bolla,ls_bolla_unica, ls_des_riferimento, ls_nota_testata, ls_nota_piede
			
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga, & 
			ll_prog_prodotto_spedito,ll_t,ll_anno_registrazione,ll_num_registrazione, ll_prog_riga_bol_ven[]
			
datetime ldt_data_stock

dec{4}   ldd_quan_spedita,ldd_quan_in_produzione

integer  li_anno_commessa,li_risposta

boolean  lb_nuova_bolla

uo_genera_doc_produzione luo_crea_bolle_produzione

if (g_mb.messagebox("Sep","Sei sicuro di generare la spedizione merce?",Exclamation!,yesno!, 2))=2 then return

if dw_note_spedizione.rowcount() < 1 then
	if (g_mb.messagebox("Sep","Non ci sono annotazioni, procedo?",Exclamation!,yesno!, 2))=2 then return
	dw_note_spedizione.insertrow(0)
end if

ls_conferma = dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"flag_conferma_bolla")
ls_aggiungi_bolla = dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"flag_aggiungi_bolla")
ls_bolla_unica = dw_scelta_fornitore.getitemstring(dw_scelta_fornitore.getrow(),"flag_bolla_unica")

luo_crea_bolle_produzione = create uo_genera_doc_produzione

dw_scelta_stock.resetupdate()

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")	
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")	
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")
ls_des_riferimento = dw_note_spedizione.getitemstring(dw_note_spedizione.getrow(),"des_riferimento")
ls_nota_testata = dw_note_spedizione.getitemstring(dw_note_spedizione.getrow(),"nota_testata")
ls_nota_piede = dw_note_spedizione.getitemstring(dw_note_spedizione.getrow(),"nota_piede")

select cod_tipo_commessa,
		 cod_prodotto
into   :ls_cod_tipo_commessa,
		 :ls_cod_prodotto_finito
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

select des_estesa_prodotto
into   :ls_des_estesa_prodotto
from   tes_fasi_lavorazione
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 cod_prodotto = :ls_cod_prodotto_fase and    
		 cod_versione = :ls_cod_versione_fase and
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

if not isnull(ls_des_riferimento) then luo_crea_bolle_produzione.is_riferimento_riga = ls_des_riferimento
if not isnull(ls_nota_testata) then luo_crea_bolle_produzione.is_nota_testata = ls_nota_testata
if not isnull(ls_nota_piede) then luo_crea_bolle_produzione.is_nota_piede = ls_nota_piede
if not isnull(ls_des_estesa_prodotto) then luo_crea_bolle_produzione.is_riferimento_riga_fine = ls_des_estesa_prodotto

ls_cod_fornitore = dw_scelta_fornitore.getitemstring( dw_scelta_fornitore.getrow(), "cod_fornitore")

select cod_tipo_bol_ven,
		 cod_tipo_det_ven,
		 cod_tipo_det_ven_sl
into   :ls_cod_tipo_bol_ven,
		 :ls_cod_tipo_det_ven,
		 :ls_cod_tipo_det_ven_sl
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

// PASSARE ANNO E NUM_REGISTRAZIONE SE SI VUOLE AGGIUNGERE AD UNA BOLLA ESISTENTE

lb_nuova_bolla = true

if ls_aggiungi_bolla = "S" then
	// aggiungo a bolla esistente
	if dw_bolle_pronte.rowcount() < 1 then
		g_mb.messagebox("SEP", "Non ci sono bolle pronte per questo fornitore: procedere con una nuova bolla.")
		return
	end if
	ll_anno_registrazione = dw_bolle_pronte.getitemnumber(dw_bolle_pronte.getrow(),"tes_bol_ven_anno_registrazione")
	ll_num_registrazione = dw_bolle_pronte.getitemnumber(dw_bolle_pronte.getrow(),"tes_bol_ven_num_registrazione")
	lb_nuova_bolla = false
end if

for ll_t = 1 to dw_elenco_prod_uscita_bol.rowcount()
//	dw_prod_uscita_bol_spedite.setrow(ll_t)
	luo_crea_bolle_produzione.is_cod_prodotto[ll_t]=dw_elenco_prod_uscita_bol.getitemstring(ll_t,"cod_prodotto_spedito")
	luo_crea_bolle_produzione.is_cod_deposito[ll_t]=dw_elenco_prod_uscita_bol.getitemstring(ll_t,"cod_deposito")
	luo_crea_bolle_produzione.is_cod_ubicazione[ll_t]=dw_elenco_prod_uscita_bol.getitemstring(ll_t,"cod_ubicazione")
	luo_crea_bolle_produzione.is_cod_lotto[ll_t]=dw_elenco_prod_uscita_bol.getitemstring(ll_t,"cod_lotto")
	luo_crea_bolle_produzione.idt_data_stock[ll_t]=dw_elenco_prod_uscita_bol.getitemdatetime(ll_t,"data_stock")
	luo_crea_bolle_produzione.il_prog_stock[ll_t]=dw_elenco_prod_uscita_bol.getitemnumber(ll_t,"prog_stock")
	luo_crea_bolle_produzione.id_quantita[ll_t]=dw_elenco_prod_uscita_bol.getitemnumber(ll_t,"quan_spedita")		
	ls_flag_mp = dw_elenco_prod_uscita_bol.getitemstring(ll_t,"flag_mp")
	if ls_flag_mp = 'S' then
		luo_crea_bolle_produzione.is_cod_tipo_det_ven[ll_t] = ls_cod_tipo_det_ven
	else
		luo_crea_bolle_produzione.is_cod_tipo_det_ven[ll_t] = ls_cod_tipo_det_ven_sl
	end if
next


li_risposta = luo_crea_bolle_produzione.f_crea_bolla_uscita(ls_cod_fornitore, &
												                       ls_cod_mov_magazzino, & 
																			  ls_cod_tipo_bol_ven, &
																			  ls_conferma, & 
																			  lb_nuova_bolla,&
																			  ll_anno_registrazione, &
																			  ll_num_registrazione, &
																			  ll_prog_riga_bol_ven[])

if li_risposta < 0 then
	g_mb.messagebox("Sep",luo_crea_bolle_produzione.is_messaggio)
	rollback;
	destroy uo_genera_doc_produzione
	return
end if

for ll_t = 1 to upperbound(ll_prog_riga_bol_ven[])
	
	update prod_bolle_out_com
	set    anno_registrazione = :ll_anno_registrazione,
			 num_registrazione = :ll_num_registrazione,
			 prog_riga_bol_ven = :ll_prog_riga_bol_ven[ll_t]
	where  cod_azienda = : s_cs_xx.cod_azienda 	and	 
			 anno_commessa = :li_anno_commessa 	and	 
			 num_commessa = :ll_num_commessa 	and	 
			 prog_riga = :ll_prog_riga 	and    
			 cod_prodotto_spedito = :luo_crea_bolle_produzione.is_cod_prodotto[ll_t] 	and    
			 anno_registrazione is null 	and    
			 num_registrazione is null  	and    
			 prog_riga_bol_ven is null;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return
	end if
	
	//***  QUESTO UPDATE SERVIREBBE NEL CASO IL SL VENISSE SCARICATO DAL MAGAZZINO INVECE IL SL RIMANE SEMPRE NEL MAGAZZINO
	//*** NOSTRO POICHE' IL TIPO_DET_VEN INDICATO IN TAB TIPI COMMESSA PER GLI SL NON DEVE ESSERE ASSOCIATO A NESSUN MOVIMENTO
//	update avan_produzione_com
//	set 	 quan_utilizzata=quan_utilizzata + :luo_crea_bolle_produzione.id_quantita[ll_t]
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    anno_commessa=:li_anno_commessa
//	and    num_commessa=:ll_num_commessa
//	and    prog_riga=:ll_prog_riga
//	and    cod_prodotto=:luo_crea_bolle_produzione.is_cod_prodotto[ll_t];
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db in fase di aggiornamente tabella avan_produzione_com: "+ sqlca.sqlerrtext,stopsign!)
		rollback;
		return
	end if
next


commit;
destroy uo_genera_doc_produzione

dw_prod_uscita_bol_spedite.change_dw_current()
parent.triggerevent("pc_retrieve")

dw_elenco_prod_uscita_bol.reset()
if lb_nuova_bolla then
	g_mb.messagebox("Sep","E' stata creata la bolla di Conto Lavoro, Anno: " + string(ll_anno_registrazione) + " Numero: " + string(ll_num_registrazione),information!)
else
	g_mb.messagebox("Sep","E' stata aggiornata la bolla di Conto Lavoro, Anno: " + string(ll_anno_registrazione) + " Numero: " + string(ll_num_registrazione),information!)	
end if


if g_mb.messagebox("Sep","Vuoi stampare la bolla di uscita?",Question!, Yesno!, 2) = 1 then
	
	wf_stampa_bolla(ll_anno_registrazione, ll_num_registrazione)
	
end if

commit;

end event

type dw_scelta_fornitore from uo_cs_xx_dw within w_gen_bolle_terzisti
integer x = 320
integer y = 520
integer width = 3817
integer height = 220
integer taborder = 100
boolean bringtotop = true
string dataobject = "d_gen_bolle_terzisti_selezione_terzista"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_fornitore, ls_cod_tipo_bol_ven, ls_cod_tipo_commessa
long   ll_ret,ll_anno_commessa,ll_num_commessa,ll_prog_riga

choose case i_colname
	case "cod_fornitore"
		if dw_lista_prodotti_spedizione.rowcount() > 0 then
			dw_stato_stock_terzista.change_dw_current()
			parent.triggerevent("pc_retrieve")
			
			//se hai selezionato il fornitore seleziona il folder successivo
			dw_folder.fu_SelectTab(2)
			
		end if
	
	case "flag_aggiungi_bolla"
		
		if data = "S" then

			ll_anno_commessa = dw_elenco_fasi_esterne_attivazione.i_parentdw.getitemnumber(dw_elenco_fasi_esterne_attivazione.i_parentdw.i_selectedrows[1], "anno_commessa")
			ll_num_commessa = dw_elenco_fasi_esterne_attivazione.i_parentdw.getitemnumber(dw_elenco_fasi_esterne_attivazione.i_parentdw.i_selectedrows[1], "num_commessa")
			ll_prog_riga = dw_elenco_fasi_esterne_attivazione.i_parentdw.getitemnumber(dw_elenco_fasi_esterne_attivazione.i_parentdw.i_selectedrows[1], "prog_riga")
	
			select cod_tipo_commessa
			into   :ls_cod_tipo_commessa
			from   anag_commesse
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_commessa = :ll_anno_commessa and
					 num_commessa = :ll_num_commessa;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("SEP", "Impossibile caricare la lista bolle pronte a causa di un errore in ricerca del tipo commessa")
				return
			end if
			
			select cod_tipo_bol_ven  
			into   :ls_cod_tipo_bol_ven  
			from   tab_tipi_commessa  
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_commessa = :ls_cod_tipo_commessa ;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("SEP", "Impossibile caricare la lista bolle pronte a causa di un errore in ricerca del tipo bolla collegato al tipo commessa", stopsign!)
				return
			end if
			
			if isnull(ls_cod_tipo_bol_ven) then
				g_mb.messagebox("SEP", "Impostare il tipo bolla nel tipo commessa.", stopsign!)
				return
			end if
			dw_bolle_pronte.settransobject(sqlca)
			ll_ret = dw_bolle_pronte.retrieve(s_cs_xx.cod_azienda, getitemstring(getrow(),"cod_fornitore"), ls_cod_tipo_bol_ven)
			
			dw_bolle_pronte.object.datawindow.color = '16777215'
		else
			dw_bolle_pronte.reset()
			dw_bolle_pronte.object.datawindow.color = '12632256'
			
	end if
	
end choose

end event

event clicked;call super::clicked;choose case dwo.name
	case "b_impegna_deposito"
		wf_impegna_deposito()
	case "b_annulla_impegno"
		wf_annulla_impegno_deposito()
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_scelta_fornitore,"cod_fornitore")
end choose
end event

