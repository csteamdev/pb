﻿$PBExportHeader$w_imposta_versione.srw
$PBExportComments$Window imposta versione nelle lavorazioni e collegate
forward
global type w_imposta_versione from w_cs_xx_principale
end type
type cb_3 from commandbutton within w_imposta_versione
end type
type cb_2 from commandbutton within w_imposta_versione
end type
type cb_1 from commandbutton within w_imposta_versione
end type
end forward

global type w_imposta_versione from w_cs_xx_principale
integer width = 2126
integer height = 1624
string title = "Lavorazioni"
cb_3 cb_3
cb_2 cb_2
cb_1 cb_1
end type
global w_imposta_versione w_imposta_versione

forward prototypes
public function integer wf_crea_pk ()
public function integer wf_crea_pk_versione ()
public function integer wf_elimina_pk ()
public function integer wf_riempi_versione (string fs_tabella)
end prototypes

public function integer wf_crea_pk ();string ls_sql

ls_sql = " CREATE UNIQUE INDEX cu_tes_fasi_lavorazione ON tes_fasi_lavorazione (cod_azienda, cod_prodotto, cod_reparto, cod_lavorazione) "

execute immediate :ls_sql using sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "Errore durante la creazione dell'indice della PK senza cod_versione: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

ls_sql = "ALTER TABLE tes_fasi_lavorazione ADD PRIMARY KEY ( cod_azienda, cod_prodotto, cod_reparto, cod_lavorazione)"

execute immediate :ls_sql using sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "Errore durante la creazione della PK senza cod_versione: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if
return 0
end function

public function integer wf_crea_pk_versione ();string ls_sql

ls_sql = "ALTER TABLE tes_fasi_lavorazione MODIFY cod_versione NOT NULL "

execute immediate :ls_sql using sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "Errore durante il MODIFY NOT NULL del codice versione: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

ls_sql = " CREATE UNIQUE INDEX cu_tes_fasi_lavorazione ON tes_fasi_lavorazione (cod_azienda, cod_prodotto, cod_versione, cod_reparto, cod_lavorazione) "

execute immediate :ls_sql using sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "Errore durante la creazione dell'indice della PK senza cod_versione: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

ls_sql = "ALTER TABLE tes_fasi_lavorazione ADD PRIMARY KEY ( cod_azienda, cod_prodotto, cod_versione, cod_reparto, cod_lavorazione)"

execute immediate :ls_sql using sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "Errore durante la creazione della PK con cod_versione: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if
return 0
end function

public function integer wf_elimina_pk ();string ls_sql

ls_sql = " DROP INDEX tes_fasi_lavorazione.cu_tes_fasi_lavorazione "

execute immediate :ls_sql using sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "Errore durante la cancellazione dell'indice della pk:" + sqlca.sqlerrtext)
	rollback;
	return -1	
end if


ls_sql = "ALTER TABLE tes_fasi_lavorazione DROP PRIMARY KEY"

execute immediate :ls_sql using sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "Errore durante la cancellazione della pk:" + sqlca.sqlerrtext)
	rollback;
	return -1	
end if

return 0
end function

public function integer wf_riempi_versione (string fs_tabella);string ls_sql, ls_cod_versione
long   ll_anno, ll_numero

ls_sql = "SELECT anno_commessa, num_commessa from anag_commesse where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

DECLARE cu_1 DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql ;
OPEN DYNAMIC cu_1 ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "SEP", "Errore durante apertura cursore:" + sqlca.sqlerrtext)
	return -1
end if

do while true
	FETCH cu_1 INTO :ll_anno,
	                :ll_numero;
	if sqlca.sqlcode <> 0 then exit
	
	select cod_versione 
	into   :ls_cod_versione
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_commessa = :ll_anno and
			 num_commessa = :ll_numero;
			 
	if not isnull(ls_cod_versione) and ls_cod_versione <> "" then
		
//		choose case fs_tabella
//			case "avan_produzione_com"
				
				update avan_produzione_com
				set 	 cod_versione = :ls_cod_versione
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_commessa = :ll_anno and
						 num_commessa = :ll_numero;						 
						 
				update det_orari_produzione
				set 	 cod_versione = :ls_cod_versione
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_commessa = :ll_anno and
						 num_commessa = :ll_numero;
						 
				update impegni_deposito_fasi_com
				set 	 cod_versione = :ls_cod_versione
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_commessa = :ll_anno and
						 num_commessa = :ll_numero;	
						 
				update prod_bolle_in_com
				set 	 cod_versione = :ls_cod_versione
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_commessa = :ll_anno and
						 num_commessa = :ll_numero;						 
						 
				update prod_bolle_out_com
				set 	 cod_versione = :ls_cod_versione
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_commessa = :ll_anno and
						 num_commessa = :ll_numero;	
						 
				update difetti_det_orari_in
				set 	 cod_versione = :ls_cod_versione
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_commessa = :ll_anno and
						 num_commessa = :ll_numero;	
						 
				update difetti_det_orari_out
				set 	 cod_versione = :ls_cod_versione
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_commessa = :ll_anno and
						 num_commessa = :ll_numero;	
						 
				update tab_sessioni
				set    cod_versione = :ls_cod_versione
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_commessa = :ll_anno and
						 num_commessa = :ll_numero;
						 
//			case "commesse_reparti"
						
				update commesse_reparti
				set    cod_versione = :ls_cod_versione
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_commessa = :ll_anno and
				       num_commessa = :ll_numero;
						 
//			case "tes_storico_fasi_commesse"
						
				update tes_storico_fasi_commesse
				set    cod_versione = :ls_cod_versione
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_commessa = :ll_anno and
				       num_commessa = :ll_numero;	
						 
//			case "tempi_fase_commesse"
				
				update tempi_fase_commesse
				set    cod_versione = :ls_cod_versione
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_commessa = :ll_anno and
				       num_commessa = :ll_numero;					
				
//		end choose
		
	end if
	
loop

CLOSE cu_1 ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "SEP", "Errore durante apertura cursore:" + sqlca.sqlerrtext)
	rollback;
	return -1
end if

return 0


end function

on w_imposta_versione.create
int iCurrent
call super::create
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_3
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_1
end on

on w_imposta_versione.destroy
call super::destroy
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_1)
end on

type cb_3 from commandbutton within w_imposta_versione
integer x = 27
integer y = 396
integer width = 2039
integer height = 112
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Versione Nelle fasi avanzamento"
end type

event clicked;long   ll_ret, ll_i, ll_riga
STRING ls_sql, ls_sintassi, ls_errore, ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_cat_attrezzature, ls_versione, ls_cod_versione[], ls_vuoto[]
datastore lds_fasi
string ls_tabelle[] = {"avan_produzione_com", "commesse_reparti", "det_orari_produzione", "tes_storico_fasi_commesse"}

ll_ret = g_mb.messagebox( "APICE", "Continuare con l'impostazione delle versioni nelle fasi di lavorazione?", Exclamation!, OKCancel!, 2)

IF ll_ret <> 1 THEN
	g_mb.messagebox("APICE", "Operazione Annullata!")
	return -1
END IF


ls_sql = " ALTER TABLE fasi_avanzamento ADD cod_versione_liv_1 VARCHAR(3)"
	
EXECUTE IMMEDIATE :ls_sql USING sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "1.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

ls_sql = " ALTER TABLE fasi_avanzamento ADD cod_versione_liv_n VARCHAR(3)"

EXECUTE IMMEDIATE :ls_sql USING sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "2.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

update fasi_avanzamento
set    cod_versione_liv_1 = cod_versione,
       cod_versione_liv_n = cod_versione
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "2.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
	rollback;
	return -1	
end if
	
COMMIT;

ls_sql = " ALTER TABLE fasi_avanzamento MODIFY cod_versione_liv_1 NOT NULL"

EXECUTE IMMEDIATE :ls_sql USING sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if	

ls_sql = " ALTER TABLE fasi_avanzamento MODIFY cod_versione_liv_n NOT NULL"

EXECUTE IMMEDIATE :ls_sql USING sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if	

ls_sql = " DROP INDEX fasi_avanzamento.cu_fasi_avanzamento"

EXECUTE IMMEDIATE :ls_sql USING sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "DROP INDEX: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if	

ls_sql = " ALTER TABLE fasi_avanzamento DROP PRIMARY KEY "

EXECUTE IMMEDIATE :ls_sql USING sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "DROP INDEX: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if	


ls_sql = " CREATE UNIQUE INDEX cu_fasi_avanzamento ON fasi_avanzamento " + &
		   "   (cod_azienda, cod_prodotto_finito, cod_semilavorato_liv_1, cod_versione_liv_1, cod_reparto, cod_lavorazione, " + &
			"    cod_semilavorato_liv_n, cod_versione_liv_n, cod_reparto_liv_n, cod_lavorazione_liv_n, num_livello_fasi, num_sequenza_fasi_livello) "

EXECUTE IMMEDIATE :ls_sql USING sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "DROP INDEX: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if	

ls_sql = " ALTER TABLE fasi_avanzamento ADD PRIMARY KEY " + &
		   "   (cod_azienda, cod_prodotto_finito, cod_semilavorato_liv_1, cod_versione_liv_1, cod_reparto, cod_lavorazione, " + &
			"    cod_semilavorato_liv_n, cod_versione_liv_n, cod_reparto_liv_n, cod_lavorazione_liv_n, num_livello_fasi, num_sequenza_fasi_livello) "

EXECUTE IMMEDIATE :ls_sql USING sqlca;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "DROP INDEX: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if	


g_mb.messagebox("SEP", "Procedura terminata!")
return 0
end event

type cb_2 from commandbutton within w_imposta_versione
integer x = 27
integer y = 208
integer width = 2039
integer height = 112
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Versione Nelle tabelle collegate"
end type

event clicked;long   ll_ret, ll_i, ll_riga
STRING ls_sql, ls_sintassi, ls_errore, ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_cat_attrezzature, ls_versione, ls_cod_versione[], ls_vuoto[]
datastore lds_fasi
string ls_tabelle[] = {"avan_produzione_com", "commesse_reparti", "det_orari_produzione", "tes_storico_fasi_commesse"}

ll_ret = g_mb.messagebox( "APICE", "Continuare con l'impostazione delle versioni nelle fasi di lavorazione?", Exclamation!, OKCancel!, 2)

IF ll_ret <> 1 THEN
	g_mb.messagebox("APICE", "Operazione Annullata!")
	return -1
END IF


//ls_sql = " ALTER TABLE avan_produzione_com ADD cod_versione VARCHAR(3)"
//	
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "1.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if
//
//ls_sql = " ALTER TABLE commesse_reparti ADD cod_versione VARCHAR(3)"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "2.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if
//
//ls_sql = " ALTER TABLE det_orari_produzione ADD cod_versione VARCHAR(3)"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "3.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if
//
//ls_sql = " ALTER TABLE tes_storico_fasi_commesse ADD cod_versione VARCHAR(3)"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "4.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if
//
//ls_sql = " ALTER TABLE impegni_deposito_fasi_com ADD cod_versione VARCHAR(3)"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "5.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if
//
//ls_sql = " ALTER TABLE prod_bolle_in_com ADD cod_versione VARCHAR(3)"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "6.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if
//
//ls_sql = " ALTER TABLE prod_bolle_out_com ADD cod_versione VARCHAR(3)"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "7.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE difetti_det_orari_in ADD cod_versione VARCHAR(3)"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "8.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE difetti_det_orari_out ADD cod_versione VARCHAR(3)"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE tempi_fase_commesse ADD cod_versione VARCHAR(3)"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "10.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	

ll_ret = wf_riempi_versione("avan_produzione_com")

if ll_ret < 0 then
	rollback;
	return -1
end if
	
COMMIT;

//ls_sql = " ALTER TABLE difetti_det_orari_out MODIFY cod_versione NOT NULL"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE difetti_det_orari_out MODIFY cod_versione NOT NULL"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE difetti_det_orari_out MODIFY cod_versione NOT NULL"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE difetti_det_orari_out MODIFY cod_versione NOT NULL"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE difetti_det_orari_out MODIFY cod_versione NOT NULL"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE difetti_det_orari_out MODIFY cod_versione NOT NULL"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE difetti_det_orari_out MODIFY cod_versione NOT NULL"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE difetti_det_orari_out MODIFY cod_versione NOT NULL"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE difetti_det_orari_out MODIFY cod_versione NOT NULL"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	
//
//ls_sql = " ALTER TABLE difetti_det_orari_out MODIFY cod_versione NOT NULL"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "9.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	

//ls_sql = " ALTER TABLE tempi_fase_commesse MODIFY cod_versione NOT NULL"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "10.Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if	

g_mb.messagebox("SEP", "Procedura terminata!")
return 0
end event

type cb_1 from commandbutton within w_imposta_versione
integer x = 27
integer y = 32
integer width = 2039
integer height = 112
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Versione Nelle lavorazioni"
end type

event clicked;long   ll_ret, ll_i, ll_riga, ll_count, ll_prog
STRING ls_sql, ls_sintassi, ls_errore, ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_cat_attrezzature, ls_versione, ls_cod_versione[], ls_vuoto[]
datastore lds_fasi

ll_ret = g_mb.messagebox( "APICE", "Continuare con l'impostazione delle versioni nelle fasi di lavorazione?", Exclamation!, OKCancel!, 2)

IF ll_ret <> 1 THEN
	g_mb.messagebox("APICE", "Operazione Annullata!")
	return -1
END IF

//// *** AGGIUNGO LA COLONNA VERSIONE
//
//ls_sql = " ALTER TABLE tes_fasi_lavorazione ADD cod_versione VARCHAR(3)"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if
//
//ls_sql = " ALTER TABLE det_fasi_lavorazione ADD cod_versione VARCHAR(3)"
//
//EXECUTE IMMEDIATE :ls_sql USING sqlca;
//
//if sqlca.sqlcode < 0 then
//	messagebox( "SEP", "Errore durante l'aggiunta del campo cod_versione: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if
//
//// *** metto la versione = "!"
//
//update tes_fasi_lavorazione
//set    cod_versione = '!'
//where  cod_azienda = :s_cs_xx.cod_azienda;
//
//update det_fasi_lavorazione
//set    cod_versione = '!'
//where  cod_azienda = :s_cs_xx.cod_azienda;

// *** leggo tutte le righe delle fasi di lavorazione e trovo la versione

ls_sql = " SELECT cod_prodotto,    			 " + &
         "			cod_reparto,     			 " + &
         "			cod_lavorazione  			 " + &
         " FROM   tes_fasi_lavorazione 	 " + &
         " WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_versione = '!' "
			
ls_sintassi = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)

IF Len(ls_errore) > 0 THEN
   g_mb.messagebox("SEP", "Errore durante la creazione della struttura del datastore:" + ls_errore)
   RETURN -1
END IF

lds_fasi = create datastore
lds_fasi.Create( ls_sintassi, ls_errore)

IF Len(ls_errore) > 0 THEN
   g_mb.messagebox("SEP", "Errore durante la creazione della struttura del datastore:" + ls_errore)
   RETURN -1
END IF			

lds_fasi.settransobject( sqlca)

ll_ret = lds_fasi.retrieve()

if ll_ret > 0 then

//	ll_i = wf_elimina_pk()
//	if ll_i < 0 then
//		destroy lds_fasi;
//		messagebox( "SEP", "Procedura Interrotta!")
//		return -1		
//	end if

	for ll_i = 1 to ll_ret
		
		ls_cod_prodotto = lds_fasi.getitemstring( ll_i, "cod_prodotto")
		ls_cod_reparto = lds_fasi.getitemstring( ll_i, "cod_reparto")
		ls_cod_lavorazione = lds_fasi.getitemstring( ll_i, "cod_lavorazione")
		
		declare cu_versioni cursor for
		select  distinct cod_versione_figlio
		from    distinta
		where   cod_azienda = :s_cs_xx.cod_azienda and
				  cod_prodotto_figlio = :ls_cod_prodotto;
				  
		open cu_versioni;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "SEP", "Errore durante l'apertura del cursore:" + sqlca.sqlerrtext)
			rollback;
			destroy lds_fasi;
//			wf_crea_pk()
			return -1
		end if
		
		ll_riga = 0
		ls_cod_versione = ls_vuoto
		
		do while true
			fetch cu_versioni into :ls_versione;
			if sqlca.sqlcode <> 0 then exit;
			ll_riga ++
			ls_cod_versione[ll_riga] = ls_versione
		loop
		
		close cu_versioni;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "SEP", "Errore durante la chiusura del cursore:" + sqlca.sqlerrtext)
			rollback;
			destroy lds_fasi;
//			wf_crea_pk()
			return -1
		end if
		
		if ll_riga = 1 then
			
			update tes_fasi_lavorazione 
			set    cod_versione = :ls_cod_versione[1]
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :ls_cod_prodotto and
					 cod_reparto = :ls_cod_reparto and
					 cod_lavorazione = :ls_cod_lavorazione and
					 cod_versione = '!';
					
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "SEP", "Errore durante l'aggiornamento della fase di lavorazione: " + ls_cod_prodotto + " " + ls_cod_versione[ll_riga] + " " + ls_cod_reparto + " " + ls_cod_lavorazione + " .Errore:" + sqlca.sqlerrtext)
				rollback;
				destroy lds_fasi;
//				wf_crea_pk()
				return -1
			end if
			
			update det_fasi_lavorazione
			set    cod_versione = :ls_cod_versione[1]
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :ls_cod_prodotto and
					 cod_reparto = :ls_cod_reparto and
					 cod_lavorazione = :ls_cod_lavorazione and
					 cod_versione = '!';
			
		else
			
			ll_count = 0
			
			select count(*)
			into   :ll_count
			from   det_fasi_lavorazione
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and
					 cod_reparto = :ls_cod_reparto and
					 cod_lavorazione = :ls_cod_lavorazione and
					 cod_versione = '!';			
		
			for ll_riga = 1 to upperbound(ls_cod_versione)
			
				insert into tes_fasi_lavorazione( cod_azienda,
															 cod_prodotto, 
															 cod_versione,
															 cod_reparto,     			
															 cod_lavorazione,    		
															 cod_cat_attrezzature,    
															 des_estesa_prodotto,     
															 num_priorita,    			 
															 quan_prodotta,    		 
															 quan_non_conforme,    	 
															 tempo_attrezzaggio,    	
															 tempo_attrezzaggio_commessa,    
															 tempo_lavorazione,    	 
															 cod_cat_attrezzature_2, 
															 tempo_risorsa_umana,    
															 cod_prodotto_sfrido,     
															 quan_sfrido,    			
															 flag_esterna,    		
															 flag_contigua,    		
															 costo_orario,    		
															 costo_pezzo,     		
															 tempo_movimentazione,  
															 flag_uso,    				
															 flag_stampa_fase,    	
															 cod_centro_costo)
											select       :s_cs_xx.cod_azienda,
															 :ls_cod_prodotto,
															 :ls_cod_versione[ll_riga],
															 cod_reparto,
															 cod_lavorazione,
															 cod_cat_attrezzature,
															 des_estesa_prodotto,
															 num_priorita,
															 quan_prodotta,
															 quan_non_conforme,
															 tempo_attrezzaggio,    	
															 tempo_attrezzaggio_commessa,    
															 tempo_lavorazione,    	 
															 cod_cat_attrezzature_2, 
															 tempo_risorsa_umana,    
															 cod_prodotto_sfrido,     
															 quan_sfrido,    			
															 flag_esterna,    		
															 flag_contigua,    		
															 costo_orario,    		
															 costo_pezzo,     		
															 tempo_movimentazione,  
															 flag_uso,    				
															 flag_stampa_fase,    	
															 cod_centro_costo
											from         tes_fasi_lavorazione
											where        cod_azienda = :s_cs_xx.cod_azienda and
															 cod_prodotto = :ls_cod_prodotto and
															 cod_reparto = :ls_cod_reparto and
															 cod_lavorazione = :ls_cod_lavorazione and
															 cod_versione = '!';
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox( "SEP", "Errore durante l'inserimento della fase di lavorazione: " + ls_cod_prodotto + " " + ls_cod_versione[ll_riga] + " " + ls_cod_reparto + " " + ls_cod_lavorazione + " .Errore:" + sqlca.sqlerrtext)
					rollback;
					destroy lds_fasi;
					return -1
				end if
				
				// *** copio il dettaglio fasi
						 
				if not isnull(ll_count) and ll_count > 0 then
					
					DECLARE cu_det cursor for
					select prog_riga_fasi_lavorazione
					from   det_fasi_lavorazione
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_prodotto = :ls_cod_prodotto and
							 cod_reparto = :ls_cod_reparto and
							 cod_lavorazione = :ls_cod_lavorazione and
							 cod_versione = '!';
							 
					open cu_det;
					do while true
						fetch cu_det into :ll_prog;
						if sqlca.sqlcode <> 0 then exit
						
					
						insert into det_fasi_lavorazione( cod_azienda,
																	 cod_prodotto,
																	 cod_reparto,
																	 cod_lavorazione,
																	 prog_riga_fasi_lavorazione,
																	 cod_prodotto_materia_prima,
																	 descrizione,
																	 progressivo_documento,
																	 cod_versione)
												select          :s_cs_xx.cod_azienda,
																	 :ls_cod_prodotto,
																	 :ls_cod_reparto,
																	 :ls_cod_lavorazione,
																	 prog_riga_fasi_lavorazione,
																	 cod_prodotto_materia_prima,
																	 descrizione,
																	 progressivo_documento,
																	 :ls_cod_versione[ll_riga]
												from            det_fasi_lavorazione
												where           cod_azienda = :s_cs_xx.cod_azienda and
																	 cod_prodotto = :ls_cod_prodotto and
																	 cod_reparto = :ls_cod_reparto and
																	 cod_lavorazione = :ls_cod_lavorazione and
																	 cod_versione = '!' and
																	 prog_riga_fasi_lavorazione = :ll_prog;
																	 
						if sqlca.sqlcode <> 0 then
							g_mb.messagebox( "SEP", "Errore durante l'inserimento della det fase di lavorazione: " + ls_cod_prodotto + " " + ls_cod_versione[ll_riga] + " " + ls_cod_reparto + " " + ls_cod_lavorazione + " .Errore:" + sqlca.sqlerrtext)
							close cu_det;
							rollback;
							destroy lds_fasi;
							return -1
						end if
					loop
					
					close cu_det;
					
				end if

			next
			
			// *** cancello la riga di origine con la versione nulla
			
			delete from tes_fasi_lavorazione
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and
					 cod_reparto = :ls_cod_reparto and
					 cod_lavorazione = :ls_cod_lavorazione and
					 cod_versione = '!';
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "SEP", "Errore durante la cancellazione della riga con versione vuota: " + ls_cod_prodotto + " " + ls_cod_versione[ll_riga] + " " + ls_cod_reparto + " " + ls_cod_lavorazione + " .Errore:" + sqlca.sqlerrtext)
				rollback;
				destroy lds_fasi;
				return -1
			end if
			
			if not isnull(ll_count) and ll_count > 0 then
				
				delete from det_fasi_lavorazione
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto and
						 cod_reparto = :ls_cod_reparto and
						 cod_lavorazione = :ls_cod_lavorazione and
						 cod_versione = '!';		
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox( "SEP", "2.Errore durante la cancellazione della riga con versione vuota: " + ls_cod_prodotto + " " + ls_cod_versione[ll_riga] + " " + ls_cod_reparto + " " + ls_cod_lavorazione + " .Errore:" + sqlca.sqlerrtext)
					rollback;
					destroy lds_fasi;
					return -1
				end if		
				
			end if
			
			
		end if
		
	next
	
//	wf_crea_pk_versione()	
	
end if
COMMIT;
g_mb.messagebox("SEP", "Procedura terminata!")
return 0
end event

