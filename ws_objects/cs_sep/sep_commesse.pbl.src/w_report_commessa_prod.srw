﻿$PBExportHeader$w_report_commessa_prod.srw
forward
global type w_report_commessa_prod from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_report_commessa_prod
end type
type dw_report from datawindow within w_report_commessa_prod
end type
end forward

global type w_report_commessa_prod from w_cs_xx_principale
integer width = 3680
integer height = 2944
string title = "Report Commessa Produzione"
boolean maxbox = false
boolean resizable = false
cb_stampa cb_stampa
dw_report dw_report
end type
global w_report_commessa_prod w_report_commessa_prod

type variables
integer				ii_anno_commessa
long					il_num_commessa
boolean				ib_stampa_subito = false
end variables

forward prototypes
public function integer wf_note_e_sessioni ()
end prototypes

public function integer wf_note_e_sessioni ();datastore				lds_data
string						ls_text, ls_errore, ls_des_operaio, ls_cod_operaio, ls_quantita, ls_cod_misura_mag, ls_data, ls_ora, ls_nota_testata, ls_nota_piede, ls_sql
long						ll_tot, ll_index, ll_new
dec{4}					ldd_quan_prodotta
datetime					ldt_data, ldt_ora


//leggo UMmag del semuilavorato, note testata e piede della commessa
select anag_prodotti.cod_misura_mag, anag_commesse.nota_testata, anag_commesse.nota_piede
into :ls_cod_misura_mag, :ls_nota_testata, :ls_nota_piede
from anag_commesse
join anag_prodotti on 	anag_prodotti.cod_azienda=anag_commesse.cod_azienda and
								anag_prodotti.cod_prodotto=anag_commesse.cod_prodotto
where 	anag_commesse.cod_azienda=:s_cs_xx.cod_azienda and
			anag_commesse.anno_commessa=:ii_anno_commessa and
			anag_commesse.num_commessa=:il_num_commessa;

ls_text = ""


//se c'è una nota testata la inserisco nel testo
if not isnull(ls_nota_testata) and ls_nota_testata<>"" then
	ls_text += ls_nota_testata
end if


//cerco se ci sono sessioni di avanzamento
ls_sql = "select		prog_riga,"+&
						"quan_prodotta,"+&
						"cod_operaio,"+&
						"data_avanzamento,"+&
						"ora_avanzamento "+&
			"from det_anag_commesse "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"anno_commessa="+string(ii_anno_commessa)+" and "+&
						"num_commessa="+string(il_num_commessa)+" and "+&
						"cod_operaio is not null and cod_operaio<>'' "+&
			"order by data_avanzamento desc,ora_avanzamento desc"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot > 0 then
	//ci sono sessioni di avanzamento

	//se c'è già la nota piede, prima di accodare le sessioni mando 2 volte a capo = lascia una riga vuota
	if ls_text<>"" then ls_text += "~r~n~r~n"

	ls_text += "SESSIONI DI AVANZAMENTO COMMESSA~r~n"

	for ll_index=1 to ll_tot
		ldt_data = lds_data.getitemdatetime(ll_index, 4)
		ldt_ora =  lds_data.getitemdatetime(ll_index, 5)
		
		if isnull(ldt_data) or year(date(ldt_data))<1950 then
			ls_data = "00/00/0000"
		else
			ls_data = string(ldt_data, "dd/mm/yyyy")
		end if
		
		if isnull(ldt_ora) then
			ls_ora = "00:00"
		else
			ls_ora = string(ldt_ora, "hh:mm")
		end if
		
		ls_cod_operaio = lds_data.getitemstring(ll_index, 3)
		ls_des_operaio = trim(f_des_tabella("anag_operai","cod_operaio='" + ls_cod_operaio + "'" ,"nome+' ' +cognome"))
		
		ldd_quan_prodotta = lds_data.getitemdecimal(ll_index, 2)
		if isnull(ldd_quan_prodotta) then ldd_quan_prodotta = 0
		ls_quantita = string(ldd_quan_prodotta, "###,##0.00") + " " + ls_cod_misura_mag
		
		ls_text += "~r~n"+ls_data+" "+ls_ora+" "+ls_des_operaio+" "+ls_quantita
	next
end if

//metto un carattere "." a capo per estendere per bene il campo (autosize)
if ls_text<>"" then ls_text += "~r~n."


//gestione nota testata e/o sessioni
if ls_text <> "" then
	//nota_testata e/o sessioni di avanzamento
	ll_new = dw_report.insertrow(0)
	
	//inserisco una riga per la nota testata (eventuale) e l'elenco delle sessioni
	dw_report.setitem(ll_new, "anno_commessa", 						dw_report.getitemnumber(1, "anno_commessa"))
	dw_report.setitem(ll_new, "num_commessa", 						dw_report.getitemnumber(1, "num_commessa"))
	dw_report.setitem(ll_new, "cod_tipo_commessa", 				dw_report.getitemstring(1, "cod_tipo_commessa"))
	dw_report.setitem(ll_new, "des_tipo_commessa", 				dw_report.getitemstring(1, "des_tipo_commessa"))
	dw_report.setitem(ll_new, "flag_tipo_avanzamento", 				dw_report.getitemstring(1, "flag_tipo_avanzamento"))
	dw_report.setitem(ll_new, "data_registrazione", 					dw_report.getitemdatetime(1, "data_registrazione"))
	dw_report.setitem(ll_new, "data_consegna", 						dw_report.getitemdatetime(1, "data_consegna"))
	dw_report.setitem(ll_new, "cod_prodotto", 							dw_report.getitemstring(1, "cod_prodotto"))
	dw_report.setitem(ll_new, "des_prodotto", 							dw_report.getitemstring(1, "des_prodotto"))
	dw_report.setitem(ll_new, "cod_versione", 							dw_report.getitemstring(1, "cod_versione"))
	dw_report.setitem(ll_new, "cod_misura_pf", 						dw_report.getitemstring(1, "cod_misura_pf"))
	dw_report.setitem(ll_new, "quan_ordine", 							dw_report.getitemdecimal(1, "quan_ordine"))
	dw_report.setitem(ll_new, "quan_prodotta", 						dw_report.getitemdecimal(1, "quan_prodotta"))
	dw_report.setitem(ll_new, "cod_deposito_versamento", 			dw_report.getitemstring(1, "cod_deposito_versamento"))
	dw_report.setitem(ll_new, "des_deposito_versamento", 			dw_report.getitemstring(1, "des_deposito_versamento"))
	dw_report.setitem(ll_new, "cod_tipo_mov_ver_prod_finiti", 		dw_report.getitemstring(1, "cod_tipo_mov_ver_prod_finiti"))
	dw_report.setitem(ll_new, "cod_deposito_prelievo", 				dw_report.getitemstring(1, "cod_deposito_prelievo"))
	dw_report.setitem(ll_new, "des_deposito_prelievo", 				dw_report.getitemstring(1, "des_deposito_prelievo"))
	dw_report.setitem(ll_new, "cod_tipo_mov_prel_mat_prime", 	dw_report.getitemstring(1, "cod_tipo_mov_prel_mat_prime"))
	dw_report.setitem(ll_new, "cod_operatore", 						dw_report.getitemstring(1, "cod_operatore"))
	dw_report.setitem(ll_new, "des_operatore", 						dw_report.getitemstring(1, "des_operatore"))
	
	dw_report.setitem(ll_new, "nota_servizio", ls_text)
	
	//questa riga andrà all'inizio
	dw_report.setitem(ll_new, "ordinamento", 0)
end if


//gestione nota piede, se c'è
if not isnull(ls_nota_piede) and ls_nota_piede<>"" then
	
	//c'è una nota piede
	ls_text = ls_nota_piede + "~r~n."
	
	ll_new = dw_report.insertrow(0)
	
	//inserisco una riga per la nota testata (eventuale) e l'elenco delle sessioni
	dw_report.setitem(ll_new, "anno_commessa", 						dw_report.getitemnumber(1, "anno_commessa"))
	dw_report.setitem(ll_new, "num_commessa", 						dw_report.getitemnumber(1, "num_commessa"))
	dw_report.setitem(ll_new, "cod_tipo_commessa", 				dw_report.getitemstring(1, "cod_tipo_commessa"))
	dw_report.setitem(ll_new, "des_tipo_commessa", 				dw_report.getitemstring(1, "des_tipo_commessa"))
	dw_report.setitem(ll_new, "flag_tipo_avanzamento", 				dw_report.getitemstring(1, "flag_tipo_avanzamento"))
	dw_report.setitem(ll_new, "data_registrazione", 					dw_report.getitemdatetime(1, "data_registrazione"))
	dw_report.setitem(ll_new, "data_consegna", 						dw_report.getitemdatetime(1, "data_consegna"))
	dw_report.setitem(ll_new, "cod_prodotto", 							dw_report.getitemstring(1, "cod_prodotto"))
	dw_report.setitem(ll_new, "des_prodotto", 							dw_report.getitemstring(1, "des_prodotto"))
	dw_report.setitem(ll_new, "cod_versione", 							dw_report.getitemstring(1, "cod_versione"))
	dw_report.setitem(ll_new, "cod_misura_pf", 						dw_report.getitemstring(1, "cod_misura_pf"))
	dw_report.setitem(ll_new, "quan_ordine", 							dw_report.getitemdecimal(1, "quan_ordine"))
	dw_report.setitem(ll_new, "quan_prodotta", 						dw_report.getitemdecimal(1, "quan_prodotta"))
	dw_report.setitem(ll_new, "cod_deposito_versamento", 			dw_report.getitemstring(1, "cod_deposito_versamento"))
	dw_report.setitem(ll_new, "des_deposito_versamento", 			dw_report.getitemstring(1, "des_deposito_versamento"))
	dw_report.setitem(ll_new, "cod_tipo_mov_ver_prod_finiti", 		dw_report.getitemstring(1, "cod_tipo_mov_ver_prod_finiti"))
	dw_report.setitem(ll_new, "cod_deposito_prelievo", 				dw_report.getitemstring(1, "cod_deposito_prelievo"))
	dw_report.setitem(ll_new, "des_deposito_prelievo", 				dw_report.getitemstring(1, "des_deposito_prelievo"))
	dw_report.setitem(ll_new, "cod_tipo_mov_prel_mat_prime", 	dw_report.getitemstring(1, "cod_tipo_mov_prel_mat_prime"))
	dw_report.setitem(ll_new, "cod_operatore", 						dw_report.getitemstring(1, "cod_operatore"))
	dw_report.setitem(ll_new, "des_operatore", 						dw_report.getitemstring(1, "des_operatore"))
	
	dw_report.setitem(ll_new, "nota_servizio", ls_text)
	
	//questa riga andrà all'inizio
	ll_tot = dw_report.rowcount()
	
	//in questo modo va alla fine, sicuramente
	dw_report.setitem(ll_new, "ordinamento", ll_tot + 100)
	
end if





return 0
end function

on w_report_commessa_prod.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.dw_report
end on

on w_report_commessa_prod.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.dw_report)
end on

event pc_setwindow;call super::pc_setwindow;string						ls_filename
long						ll_ret, ll_index


ii_anno_commessa = s_cs_xx.parametri.parametro_d_1
il_num_commessa  = s_cs_xx.parametri.parametro_d_2
ll_ret = s_cs_xx.parametri.parametro_d_3						//se 6969 allora stampa subito

setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)
setnull(s_cs_xx.parametri.parametro_d_3)

set_w_options(c_closenosave + c_autoposition + c_noresizewin)
dw_report.settransobject(sqlca)
dw_report.object.datawindow.print.preview = "Yes"

if ll_ret = 6969 then
	ib_stampa_subito = true
end if


select stringa
into   :ls_filename
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_parametro = 'LET';

if sqlca.sqlcode < 0 then
	g_mb.error("Errore in lettura parametro LET per il logo: " + sqlca.sqlerrtext)
	
elseif sqlca.sqlcode = 100 then
	g_mb.warning("Manca il parametro aziendale stringa LET per il logo (percorso file, escluso il volume)")
	
else
	ls_filename = s_cs_xx.volume + ls_filename
	dw_report.Object.p_logo.filename = ls_filename
end if

//retrieve


ll_ret = dw_report.retrieve(s_cs_xx.cod_azienda, ii_anno_commessa, il_num_commessa)
if ll_ret < 0 then
   pcca.error = c_fatal
	return
end if

for ll_index=1 to ll_ret
	dw_report.setitem(ll_index, "ordinamento", ll_index)
next

//visualizzo eventuali note (testat e piede) e sessioni di produzione
wf_note_e_sessioni()

dw_report.setsort("ordinamento asc")
dw_report.sort()

if ib_stampa_subito then
	cb_stampa.postevent(clicked!)
end if




end event

type cb_stampa from commandbutton within w_report_commessa_prod
integer x = 37
integer y = 16
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;dw_report.print(true, true)
end event

type dw_report from datawindow within w_report_commessa_prod
integer x = 37
integer y = 124
integer width = 3616
integer height = 2728
integer taborder = 10
string dataobject = "d_report_commessa_prod"
boolean vscrollbar = true
boolean livescroll = true
end type

