﻿$PBExportHeader$w_report_ordine_lavoro.srw
$PBExportComments$Window Report Bolle Lavoro
forward
global type w_report_ordine_lavoro from w_cs_xx_principale
end type
type dw_report_bolle_lavoro from uo_cs_xx_dw within w_report_ordine_lavoro
end type
end forward

global type w_report_ordine_lavoro from w_cs_xx_principale
integer width = 3662
integer height = 4052
string title = "Bolla di Lavorazione"
dw_report_bolle_lavoro dw_report_bolle_lavoro
end type
global w_report_ordine_lavoro w_report_ordine_lavoro

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();long    ll_num_commessa,ll_prog_riga,ll_num_registrazione,ll_i,ll_num_ord_cliente,ll_t,ll_prog_stock
integer li_anno_commessa,li_anno_registrazione
string  ls_anno_commessa,ls_num_commessa,ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione, &
		  ls_prog_riga,ls_buffer,ls_note_fase,ls_quan_in_produzione,ls_cod_prodotto_finito, & 
		  ls_des_prodotto_finito,ls_cod_materia_prima,ls_des_materia_prima,ls_cod_prodotto_1, &
		  ls_cod_cliente,ls_rag_soc_cliente,ls_cod_barre_1,ls_cod_barre_2,ls_cod_barre_3,ls_cod_mezzo, & 
		  ls_des_mezzo,ls_des_prodotto,ls_mat_prime,ls_cod_deposito,ls_des_deposito,ls_cod_lotto, & 
		  ls_cod_ubicazione,ls_cod_prodotto_variante,ldd_quan_utilizzo_variante,ls_cod_versione, &
		  ls_quan_in_produzione_mp, ls_sql,ls_cod_versione_sl,ls_cod_versione_mp, ls_cod_versione_variante, &
		  ls_quan_in_produzione_sl, ls_quantita
dec{4}  ldd_quan_in_produzione,ldd_quan_in_produzione_mp,ldd_quan_in_produzione_sl,ld_quan_utilizzo,ld_quan_utilizzo_variante
datetime ldt_data_ord_cliente,ldt_data_consegna,ldt_data_stock

//li_anno_commessa = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(), "anno_commessa")
//ll_num_commessa = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(), "num_commessa")
//ll_prog_riga = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(), "prog_riga")

li_anno_commessa = dw_report_bolle_lavoro.i_parentdw.getitemnumber(dw_report_bolle_lavoro.i_parentdw.getrow(),"anno_commessa")
ll_num_commessa = dw_report_bolle_lavoro.i_parentdw.getitemnumber(dw_report_bolle_lavoro.i_parentdw.getrow(),"num_commessa")
ll_prog_riga = dw_report_bolle_lavoro.i_parentdw.getitemnumber(dw_report_bolle_lavoro.i_parentdw.getrow(),"prog_riga")
ls_cod_prodotto = dw_report_bolle_lavoro.i_parentdw.getitemstring(dw_report_bolle_lavoro.i_parentdw.getrow(),"cod_prodotto")
ls_cod_versione_sl = dw_report_bolle_lavoro.i_parentdw.getitemstring(dw_report_bolle_lavoro.i_parentdw.getrow(),"cod_versione")
ls_cod_reparto = dw_report_bolle_lavoro.i_parentdw.getitemstring(dw_report_bolle_lavoro.i_parentdw.getrow(),"cod_reparto")
ls_cod_lavorazione = dw_report_bolle_lavoro.i_parentdw.getitemstring(dw_report_bolle_lavoro.i_parentdw.getrow(),"cod_lavorazione")
ldd_quan_in_produzione_sl = dw_report_bolle_lavoro.i_parentdw.getitemnumber(dw_report_bolle_lavoro.i_parentdw.getrow(),"quan_in_produzione")

setnull(ls_cod_cliente)
setnull(li_anno_registrazione)
dw_report_bolle_lavoro.reset()

select anno_registrazione,
		 num_registrazione
into   :li_anno_registrazione,
		 :ll_num_registrazione
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode=0 then
	select cod_cliente,
			 num_ord_cliente,
			 data_ord_cliente,	 
			 cod_mezzo
   into   :ls_cod_cliente,
			 :ll_num_ord_cliente,
			 :ldt_data_ord_cliente,
			 :ls_cod_mezzo
	from   tes_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;

	select rag_soc_1
	into   :ls_rag_soc_cliente
	from   anag_clienti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cliente=:ls_cod_cliente;

	select des_mezzo
	into   :ls_des_mezzo
	from   tab_mezzi
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_mezzo=:ls_cod_mezzo;

end if
	
select cod_prodotto,
		 quan_in_produzione,
		 data_consegna,
		 cod_versione
into   :ls_cod_prodotto_finito,
		 :ldd_quan_in_produzione,
		 :ldt_data_consegna,
		 :ls_cod_versione
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

select des_prodotto
into   :ls_des_prodotto_finito
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto_finito;
	
ls_anno_commessa = string(li_anno_commessa)
ls_num_commessa = string(ll_num_commessa)
ls_prog_riga = string(ll_prog_riga)
if int(ldd_quan_in_produzione) <> ldd_quan_in_produzione then
	ls_quan_in_produzione = string(ldd_quan_in_produzione, "0,00#.0###")
else
	ls_quan_in_produzione = string(ldd_quan_in_produzione, "0,00#")
end if

if int(ldd_quan_in_produzione_sl) <> ldd_quan_in_produzione_sl then
	ls_quan_in_produzione_sl = string(ldd_quan_in_produzione_sl, "0,00#.0###")
else
	ls_quan_in_produzione_sl = string(ldd_quan_in_produzione_sl, "0,00#")
end if

dw_report_bolle_lavoro.insertrow(0)
ll_i = dw_report_bolle_lavoro.rowcount()

dw_report_bolle_lavoro.setitem(ll_i,"anno_commessa",ls_anno_commessa)
dw_report_bolle_lavoro.setitem(ll_i,"num_commessa",ls_num_commessa)
dw_report_bolle_lavoro.setitem(ll_i,"prog_riga",string(ll_prog_riga))
dw_report_bolle_lavoro.setitem(ll_i,"cod_prodotto",ls_cod_prodotto_finito)
dw_report_bolle_lavoro.setitem(ll_i,"des_prodotto",ls_des_prodotto_finito)
dw_report_bolle_lavoro.setitem(ll_i,"data_consegna",left(string(ldt_data_consegna),10))
dw_report_bolle_lavoro.setitem(ll_i,"mezzo",ls_des_mezzo)
dw_report_bolle_lavoro.setitem(ll_i,"quan_in_produzione",ls_quan_in_produzione)
dw_report_bolle_lavoro.setitem(ll_i,"anno_ordine",string(li_anno_registrazione))
dw_report_bolle_lavoro.setitem(ll_i,"num_ordine",string(ll_num_registrazione))
dw_report_bolle_lavoro.setitem(ll_i,"cod_cliente",ls_cod_cliente)
dw_report_bolle_lavoro.setitem(ll_i,"rag_sociale",ls_rag_soc_cliente)
dw_report_bolle_lavoro.setitem(ll_i,"num_ord_cliente",string(ll_num_ord_cliente))
dw_report_bolle_lavoro.setitem(ll_i,"data_ord_cliente",left(string(ldt_data_ord_cliente),10))
dw_report_bolle_lavoro.setitem(ll_i,"quan_in_produzione_sl",ls_quan_in_produzione_sl)

ls_mat_prime=""

select des_prodotto 
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto;

dw_report_bolle_lavoro.setitem(ll_i,"cod_semilavorato",ls_cod_prodotto)
dw_report_bolle_lavoro.setitem(ll_i,"des_semilavorato",ls_des_prodotto)

ls_cod_prodotto_1 = trim(ls_cod_prodotto)

declare righe_db cursor for
	select  cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo
	from    distinta
	where   cod_azienda=:s_cs_xx.cod_azienda and
			  cod_prodotto_padre=:ls_cod_prodotto_1 and
			  cod_versione = :ls_cod_versione_sl and
			  flag_ramo_descrittivo = 'N'
union all 
	select  cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo
	from    integrazioni_commessa
	where   cod_azienda=:s_cs_xx.cod_azienda and
			  anno_commessa=:li_anno_commessa and
			  num_commessa=:ll_num_commessa and
			  cod_prodotto_padre=:ls_cod_prodotto_1;

open righe_db;
if sqlca.sqlcode <> 0 then return -1

do while true
	fetch righe_db	into  :ls_cod_materia_prima, :ls_cod_versione_mp, :ld_quan_utilizzo;

	if sqlca.sqlcode = 100 or sqlca.sqlcode <0 then exit

	select cod_prodotto, cod_versione_variante, quan_utilizzo
	into   :ls_cod_prodotto_variante, :ls_cod_versione_variante, :ld_quan_utilizzo_variante
	from   varianti_commesse
	where  cod_azienda   = :s_cs_xx.cod_azienda and
			 anno_commessa = :li_anno_commessa and
			 num_commessa  = :ll_num_commessa and
			 cod_prodotto_padre  = :ls_cod_prodotto_1 and
			 cod_prodotto_figlio = :ls_cod_materia_prima and
			 cod_versione_figlio = :ls_cod_versione_mp;

	if sqlca.sqlcode = 0 and not isnull(ls_cod_prodotto_variante) then
		ls_cod_materia_prima = ls_cod_prodotto_variante
		ld_quan_utilizzo     = ld_quan_utilizzo_variante
	end if

	select des_prodotto
	into   :ls_des_materia_prima
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda and
			 cod_prodotto=:ls_cod_materia_prima;

	select quan_in_produzione
	into   :ldd_quan_in_produzione_mp
	from   mat_prime_commessa
	where  cod_azienda=:s_cs_xx.cod_azienda and
			 anno_commessa=:li_anno_commessa  and
			 num_commessa=:ll_num_commessa and
			 prog_riga=:ll_prog_riga;
	

	if int(ld_quan_utilizzo) <> ld_quan_utilizzo then
		ls_quantita = string(ld_quan_utilizzo, "0,00#.0###")
	else
		ls_quantita = string(ld_quan_utilizzo, "0,00#")
	end if
	
	ls_mat_prime = ls_mat_prime + "Materia Prima:" + ls_cod_materia_prima + " " + ls_des_materia_prima + "   QTA'= " + ls_quantita + char(13) + char(13)		

	if int(ldd_quan_in_produzione_mp) <> ldd_quan_in_produzione_mp then
		ls_quan_in_produzione_mp = string(ldd_quan_in_produzione_mp, "0,00#.0###")
	else
		ls_quan_in_produzione_mp = string(ldd_quan_in_produzione_mp, "0,00#")
	end if
	
//	ls_mat_prime =ls_mat_prime + "Quantità Disponibile: " + ls_quan_in_produzione_mp + " Nei seguenti Stock:" + char(13) + char(13)
//
//	declare righe_mp_stock cursor for
//	select cod_deposito,
//			 cod_ubicazione,
//			 cod_lotto,
//			 data_stock,
//			 prog_stock
//	from   mat_prime_commessa_stock
//	where  cod_azienda=:s_cs_xx.cod_azienda and
//			 anno_commessa=:li_anno_commessa and
//			 num_commessa=:ll_num_commessa and
//			 prog_riga=:ll_prog_riga and
//			 cod_prodotto=:ls_cod_materia_prima;
//
//	open righe_mp_stock;
//
//	do while true
//	  fetch righe_mp_stock
//	  into  :ls_cod_deposito,
//			  :ls_cod_ubicazione,
//			  :ls_cod_lotto,	
//			  :ldt_data_stock,
//			  :ll_prog_stock;
//
//		if sqlca.sqlcode = 100 or sqlca.sqlcode <0 then exit
//					
//		ls_mat_prime = ls_mat_prime + "Deposito: " + ls_cod_deposito + " " + "Ubicazione:"+ ls_cod_ubicazione + char(13)
//		ls_mat_prime = ls_mat_prime + "Lotto: " + ls_cod_lotto + " " + "Data Stock:"+ left(string(ldt_data_stock),10) + " " + "Progr.:" + string(ll_prog_stock) + char(13) + char(13)			
//		
//	loop
//	close righe_mp_stock;
loop

close righe_db;

dw_report_bolle_lavoro.setitem(ll_i,"mat_prime",ls_mat_prime)

select des_estesa_prodotto
into   :ls_note_fase
from   tes_fasi_lavorazione
where  cod_azienda=:s_cs_xx.cod_azienda and
		 cod_prodotto=:ls_cod_prodotto and
		 cod_reparto=:ls_cod_reparto and
		 cod_lavorazione=:ls_cod_lavorazione and
		 cod_versione = :ls_cod_versione_sl;


if len(ls_prog_riga) < 4 then
	ls_prog_riga=ls_prog_riga + fill("_", 4 - len(ls_prog_riga))
end if
		
if len(ls_cod_prodotto) < 15 then
	ls_cod_prodotto=ls_cod_prodotto + fill("_", 15 - len(ls_cod_prodotto))
end if

if len(ls_cod_reparto) < 6 then
	ls_cod_reparto=ls_cod_reparto + fill("_", 6 - len(ls_cod_reparto))
end if

if len(ls_cod_lavorazione) < 6 then
	ls_cod_lavorazione=ls_cod_lavorazione + fill("_", 6 - len(ls_cod_lavorazione))
end if

ls_cod_barre_1 = "*" + ls_anno_commessa + ls_num_commessa + ls_prog_riga + "*"
ls_cod_barre_2 = "*" + ls_cod_prodotto + "*"
ls_cod_barre_3 = "*" + ls_cod_reparto + ls_cod_lavorazione + "*"

dw_report_bolle_lavoro.setitem(ll_i,"des_fase",ls_note_fase)
dw_report_bolle_lavoro.setitem(ll_i,"cod_barre_1",ls_cod_barre_1)
dw_report_bolle_lavoro.setitem(ll_i,"cod_barre_2",ls_cod_barre_2)
dw_report_bolle_lavoro.setitem(ll_i,"cod_barre_3",ls_cod_barre_3)

dw_report_bolle_lavoro.resetupdate()

dw_report_bolle_lavoro.object.datawindow.print.preview = 'Yes'

triggerevent("pc_retrieve")

return 0
end function

on w_report_ordine_lavoro.create
int iCurrent
call super::create
this.dw_report_bolle_lavoro=create dw_report_bolle_lavoro
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_bolle_lavoro
end on

on w_report_ordine_lavoro.destroy
call super::destroy
destroy(this.dw_report_bolle_lavoro)
end on

event pc_setwindow;call super::pc_setwindow;dw_report_bolle_lavoro.ib_dw_report = true

set_w_options(c_noresizewin)

dw_report_bolle_lavoro.set_dw_options(sqlca, &
                                 i_openparm, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)

iuo_dw_main = dw_report_bolle_lavoro

save_on_close(c_socnosave)

dw_report_bolle_lavoro.postevent("ue_carica_report")
end event

type dw_report_bolle_lavoro from uo_cs_xx_dw within w_report_ordine_lavoro
event ue_carica_report ( )
integer x = 23
integer y = 20
integer width = 3520
integer height = 3892
string dataobject = "r_report_ORDINE_lavoro"
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_carica_report();change_dw_current()

setredraw(false)

wf_report()

setredraw(true)

postevent("ue_anteprima")
end event

