﻿$PBExportHeader$w_report_prod_commesse.srw
$PBExportComments$situazione magazzino
forward
global type w_report_prod_commesse from w_cs_xx_principale
end type
type cb_desel_tutte from commandbutton within w_report_prod_commesse
end type
type cb_sel_tutte from commandbutton within w_report_prod_commesse
end type
type cb_cerca from commandbutton within w_report_prod_commesse
end type
type st_1 from statictext within w_report_prod_commesse
end type
type hpb_1 from hprogressbar within w_report_prod_commesse
end type
type cb_annulla from commandbutton within w_report_prod_commesse
end type
type dw_selezione from uo_cs_xx_dw within w_report_prod_commesse
end type
type dw_report from uo_cs_xx_dw within w_report_prod_commesse
end type
type dw_folder from u_folder within w_report_prod_commesse
end type
type cb_report from commandbutton within w_report_prod_commesse
end type
type dw_sel_commesse from uo_cs_xx_dw within w_report_prod_commesse
end type
end forward

global type w_report_prod_commesse from w_cs_xx_principale
integer x = 73
integer y = 300
integer width = 3762
integer height = 1932
string title = "Report Produzione"
cb_desel_tutte cb_desel_tutte
cb_sel_tutte cb_sel_tutte
cb_cerca cb_cerca
st_1 st_1
hpb_1 hpb_1
cb_annulla cb_annulla
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
cb_report cb_report
dw_sel_commesse dw_sel_commesse
end type
global w_report_prod_commesse w_report_prod_commesse

type variables
datastore ids_situazione_mag
end variables

forward prototypes
public function integer wf_descrizione_impegnato (string fs_cod_prodotto, ref string fs_descrizione, ref string fs_errore)
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
public function long wf_cerca_commesse ()
public function boolean wf_check_prodotto ()
end prototypes

public function integer wf_descrizione_impegnato (string fs_cod_prodotto, ref string fs_descrizione, ref string fs_errore);long   ll_anno, ll_numero
dec{4} ld_quantita

fs_descrizione = ""

declare cu_ordini cursor for
select  det_ord_ven.anno_registrazione,   
        det_ord_ven.num_registrazione,
		  sum(det_ord_ven.quan_ordine)
from    {oj det_ord_ven RIGHT OUTER JOIN tes_ord_ven ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda AND det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione AND det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione}, {oj tab_tipi_det_ven RIGHT OUTER JOIN det_ord_ven ON tab_tipi_det_ven.cod_azienda = det_ord_ven.cod_azienda AND tab_tipi_det_ven.cod_tipo_det_ven = det_ord_ven.cod_tipo_det_ven}  
where   ( tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda ) AND  
        ( tab_tipi_det_ven.flag_tipo_det_ven = 'M' ) AND  
        ( det_ord_ven.cod_prodotto = :fs_cod_prodotto ) AND  
        ( det_ord_ven.flag_evasione <> 'E' ) AND  
        ( det_ord_ven.flag_blocco = 'N' ) AND  
        ( tes_ord_ven.flag_blocco = 'N' )
group   by det_ord_ven.anno_registrazione, det_ord_ven.num_registrazione
order   by det_ord_ven.anno_registrazione ASC, det_ord_ven.num_registrazione ASC;

open cu_ordini;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante l'apertura del cursore ordini:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

do while true
	
	fetch cu_ordini into :ll_anno,
	                     :ll_numero,
								:ld_quantita;
								
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore durante la fetch del cursore degli ordini:" + sqlca.sqlerrtext
		close cu_ordini;
		rollback;
		return -1
	end if
	
	if isnull(ll_anno) then ll_anno = 0
	if isnull(ll_numero) then ll_numero = 0
	if isnull(ld_quantita) then ld_quantita = 0
	
	fs_descrizione += " Nr. " + string(ld_quantita, "###,###,##0.00") + " ordine " + string(ll_anno) + "/" + string(ll_numero) + "~r~n"
	
loop

close cu_ordini;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la chiusura del cursore ordini:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

declare cu_commesse cursor for
select  anno_commessa ,
        num_commessa  ,
		   sum(quan_impegnata_attuale)
from    impegno_mat_prime_commessa      
where   ( impegno_mat_prime_commessa.cod_azienda = :s_cs_xx.cod_azienda ) and 
        ( impegno_mat_prime_commessa.cod_prodotto = :fs_cod_prodotto )
group by anno_commessa, num_commessa
order by anno_commessa ASC, num_commessa ASC;

open cu_commesse;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante l'apertura del cursore commesse:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

do while true
	
	fetch cu_commesse into :ll_anno,
	                       :ll_numero,
								  :ld_quantita;
								
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore durante la fetch del cursore delle commesse:" + sqlca.sqlerrtext
		close cu_commesse;
		rollback;
		return -1
	end if
	
	if isnull(ll_anno) then ll_anno = 0
	if isnull(ll_numero) then ll_numero = 0
	if isnull(ld_quantita) then ld_quantita = 0
	
	fs_descrizione += " Nr. " + string(ld_quantita, "###,###,##0.00") + " commessa " + string(ll_anno) + "/" + string(ll_numero) + "~r~n"
	
loop

close cu_commesse;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la chiusura del cursore commesse:" + sqlca.sqlerrtext
	rollback;
	return -1
end if
		  
return 0
end function

public subroutine wf_memorizza_filtro ();//string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
//long ll_i, ll_colonne, ll_numero,ll_cont
//datetime ldt_data
//
//if g_mb.messagebox("Omnia","Memorizza l'attuale impostazione dei filtro come predefinito?",Question!,YesNo!,2) = 2 then return
//
//ls_colcount = dw_selezione.Object.DataWindow.Column.Count
//ll_colonne = long(ls_colcount)
//
//ls_cod_utente = s_cs_xx.cod_utente
//
//if ls_cod_utente = "CS_SYSTEM" then
//	ls_cod_utente = "SYSTEM"
//end if
//
//ls_memo = ""
//for ll_i = 1 to ll_colonne
//	ls_nome_colonna = dw_selezione.describe("#"+string(ll_i)+".name")
//	ls_tipo_colonna = dw_selezione.describe("#"+string(ll_i)+".coltype")
//
//	if lower(ls_tipo_colonna) = "datetime" then
//		ldt_data = dw_selezione.getitemdatetime(dw_selezione.getrow(), ls_nome_colonna)
//		if isnull(ldt_data) then
//			ls_memo += "NULL~t"
//		else
//			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
//		end if
//	end if
//	
//	if lower(left(ls_tipo_colonna,4)) = "char" then
//		ls_stringa = dw_selezione.getitemstring(dw_selezione.getrow(), ls_nome_colonna)
//		if isnull(ls_stringa) then
//			ls_memo += "NULL~t"
//		else
//			ls_memo += ls_stringa + "~t"
//		end if
//	end if
//
//	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
//		ll_numero = dw_selezione.getitemnumber(dw_selezione.getrow(), ls_nome_colonna)
//		if isnull(ll_numero) then
//			ls_memo += "NULL~t"
//		else
//			ls_memo += string(ll_numero) + "~t"
//		end if
//	end if
//
//next
//
//select count(*)
//into   :ll_cont
//from   filtri_manutenzioni
//where cod_azienda = :s_cs_xx.cod_azienda and
//      cod_utente =  :ls_cod_utente and
//		 tipo_filtro = 'SITMAG';
//		
//if ll_cont > 0 then
//	update filtri_manutenzioni
//	set filtri = :ls_memo
//	where cod_azienda = :s_cs_xx.cod_azienda and
//			cod_utente =  :ls_cod_utente and
//		   tipo_filtro = 'SITMAG';
//	if sqlca.sqlcode <> 0 then
//		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
//		rollback;
//		return
//	end if
//else
//	insert into filtri_manutenzioni
//		(cod_azienda,
//		 cod_utente,
//		 filtri,
//		 tipo_filtro)
//	 values
//		 (:s_cs_xx.cod_azienda,
//		  :ls_cod_utente,
//		  :ls_memo,
//		  'SITMAG');
//	if sqlca.sqlcode <> 0 then
//		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
//		rollback;
//		return
//	end if
//end if
//
//commit;
//
//return
end subroutine

public subroutine wf_leggi_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero
datetime ldt_data

ls_colcount = dw_selezione.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'SITMAG';
if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_selezione.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_selezione.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_selezione.setitem(dw_selezione.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_selezione.setitem(dw_selezione.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_selezione.setitem(dw_selezione.getrow(),ls_nome_colonna, ll_numero)
	end if

next

return
end subroutine

public function long wf_cerca_commesse ();datetime ldt_data_consegna_da, ldt_data_consegna_a
long ll_rows, ll_index, ll_anno_commessa, ll_num_commessa
string ls_sql, ls_cod_azienda

setpointer(HourGlass!)

setnull(ldt_data_consegna_da)
setnull(ldt_data_consegna_a)

dw_selezione.accepttext()
dw_sel_commesse.reset()

ldt_data_consegna_da = dw_selezione.getitemdatetime(1, "data_consegna_inizio")
ldt_data_consegna_a = dw_selezione.getitemdatetime(1, "data_consegna_fine")

ll_rows = dw_sel_commesse.retrieve(s_cs_xx.cod_azienda, ldt_data_consegna_da, ldt_data_consegna_a)

ls_sql = "select "+&
				 "cod_azienda"+&
         		",anno_commessa"+&
         		",num_commessa"+&   
        		 " "+&
			"from anag_commesse "+&
   		"where cod_azienda='"+s_cs_xx.cod_azienda+"' "

if not isnull(ldt_data_consegna_da) then
	ls_sql += "and  data_consegna>='" + string(ldt_data_consegna_da, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_consegna_a) then
	ls_sql += "and  data_consegna<='" + string(ldt_data_consegna_a, s_cs_xx.db_funzioni.formato_data) + "' "
end if

ls_sql += "order by anno_commessa, num_commessa"

declare cu_cursore dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_cursore;

if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore delle commesse. Dettaglio = " + sqlca.sqlerrtext)
	setpointer(Arrow!)
	return -1
end if

DO WHILE TRUE
	FETCH cu_cursore INTO 
			  :ls_cod_azienda
			 ,:ll_anno_commessa
			 ,:ll_num_commessa
	;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore delle commesse. Dettaglio = " + sqlca.sqlerrtext)
		close cu_cursore;
		setpointer(Arrow!)
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	ll_index = dw_sel_commesse.insertrow(0)
	dw_sel_commesse.setitem(ll_index, "sel", "N")
	dw_sel_commesse.setitem(ll_index, "anno_commessa", ll_anno_commessa)
	dw_sel_commesse.setitem(ll_index, "num_commessa", ll_num_commessa)
	dw_sel_commesse.setitem(ll_index, "commessa", "Commessa "+ string(ll_anno_commessa)+"/"+string(ll_num_commessa))
	
loop

ll_rows = dw_sel_commesse.rowcount()

setpointer(Arrow!)
close cu_cursore;

return ll_rows
end function

public function boolean wf_check_prodotto ();long ll_rows, ll_index, ll_anno_commessa, ll_num_commessa, ll_count
string ls_commesse, ls_sel, ls_sql
boolean lb_prima = true

ls_sql = "select count(cod_prodotto) "+&
			 "from impegno_mat_prime_commessa "+& 
			"where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

ll_rows = dw_sel_commesse.rowcount()
ls_commesse = ""

for ll_index = 1 to ll_rows
	ls_sel = dw_sel_commesse.getitemstring(ll_index, "sel")
	if ls_sel = "S" then
		ll_anno_commessa = dw_sel_commesse.getitemnumber(ll_index, "anno_commessa")
		ll_num_commessa = dw_sel_commesse.getitemnumber(ll_index, "num_commessa")
		
		if lb_prima then
			lb_prima = false
			ls_commesse = " and ("
			ls_commesse += "(anno_commessa="+string(ll_anno_commessa)+" and num_commessa="+string(ll_num_commessa)+")"
		else
			ls_commesse += "or (anno_commessa="+string(ll_anno_commessa)+" and num_commessa="+string(ll_num_commessa)+")"
		end if
	end if
next
if not lb_prima then
	ls_commesse +=")"
end if

ls_sql += ls_commesse

//declare cu_cursore dynamic cursor for sqlsa;
//prepare sqlsa from :ls_sql;
//open dynamic cu_cursore;
//
//if sqlca.sqlcode <> 0 then 
//	g_mb.messagebox("OMNIA","Errore nella open del cursore delle mp/sl. Dettaglio = " + sqlca.sqlerrtext)
//	setpointer(Arrow!)
//	return false
//end if
//
//
//DO WHILE TRUE
//	FETCH cu_cursore INTO 
//			 :variables.....
//	;
//	
//	if sqlca.sqlcode < 0 then
//		g_mb.messagebox("OMNIA","Errore nella fetch del cursore dei test. Dettaglio = " + sqlca.sqlerrtext)
//		close cu_cursore;
//		rollback;
//		setpointer(Arrow!)
//		return
//	elseif sqlca.sqlcode = 100 then
//		exit
//	end if	
//	
//	......
//	......
//	......
//loop
//
//setpointer(Arrow!)
//
//close cu_cursore;

return true
end function

on w_report_prod_commesse.create
int iCurrent
call super::create
this.cb_desel_tutte=create cb_desel_tutte
this.cb_sel_tutte=create cb_sel_tutte
this.cb_cerca=create cb_cerca
this.st_1=create st_1
this.hpb_1=create hpb_1
this.cb_annulla=create cb_annulla
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.cb_report=create cb_report
this.dw_sel_commesse=create dw_sel_commesse
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_desel_tutte
this.Control[iCurrent+2]=this.cb_sel_tutte
this.Control[iCurrent+3]=this.cb_cerca
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.hpb_1
this.Control[iCurrent+6]=this.cb_annulla
this.Control[iCurrent+7]=this.dw_selezione
this.Control[iCurrent+8]=this.dw_report
this.Control[iCurrent+9]=this.dw_folder
this.Control[iCurrent+10]=this.cb_report
this.Control[iCurrent+11]=this.dw_sel_commesse
end on

on w_report_prod_commesse.destroy
call super::destroy
destroy(this.cb_desel_tutte)
destroy(this.cb_sel_tutte)
destroy(this.cb_cerca)
destroy(this.st_1)
destroy(this.hpb_1)
destroy(this.cb_annulla)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.cb_report)
destroy(this.dw_sel_commesse)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
dw_sel_commesse.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
iuo_dw_main = dw_report
//

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = cb_annulla
l_objects[4] = dw_sel_commesse
l_objects[5] = cb_cerca
l_objects[6] = st_1
l_objects[7] = cb_sel_tutte
l_objects[8] = cb_desel_tutte
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

move(0,0)

resize(w_cs_xx_mdi.mdi_1.width - 50,w_cs_xx_mdi.mdi_1.height - 50)

end event

event resize;call super::resize;dw_folder.width = newwidth - 20
dw_folder.height = newheight - 20
dw_report.width = newwidth - 120
dw_report.height = newheight - 170
end event

type cb_desel_tutte from commandbutton within w_report_prod_commesse
integer x = 1097
integer y = 460
integer width = 297
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nessuna"
end type

event clicked;long ll_rows, ll_index
string ls_sel = "N"

ll_rows = dw_sel_commesse.rowcount()

for ll_index = 1 to ll_rows
	dw_sel_commesse.setitem(ll_index, "sel", ls_sel)
next
end event

type cb_sel_tutte from commandbutton within w_report_prod_commesse
integer x = 1097
integer y = 380
integer width = 297
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tutte"
end type

event clicked;long ll_rows, ll_index
string ls_sel = "S"

ll_rows = dw_sel_commesse.rowcount()

for ll_index = 1 to ll_rows
	dw_sel_commesse.setitem(ll_index, "sel", ls_sel)
next
end event

type cb_cerca from commandbutton within w_report_prod_commesse
integer x = 1399
integer y = 380
integer width = 274
integer height = 160
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;wf_cerca_commesse()
end event

type st_1 from statictext within w_report_prod_commesse
integer x = 2094
integer y = 1500
integer width = 347
integer height = 72
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_report_prod_commesse
integer x = 55
integer y = 1500
integer width = 1257
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 10
end type

type cb_annulla from commandbutton within w_report_prod_commesse
integer x = 1326
integer y = 1496
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string		ls_null
datetime		ldt_null

setnull(ls_null)
setnull(ldt_null)

dw_selezione.setitem(dw_selezione.getrow(),"data_riferimento", ldt_null)
dw_selezione.setitem(dw_selezione.getrow(),"flag_mancanti", 'N')
dw_selezione.setitem(dw_selezione.getrow(),"data_consegna_inizio", ldt_null)
dw_selezione.setitem(dw_selezione.getrow(),"data_consegna_fine", ldt_null)

dw_sel_commesse.reset()
end event

type dw_selezione from uo_cs_xx_dw within w_report_prod_commesse
event ue_imposta_filtro ( )
event ue_reset ( )
integer x = 46
integer y = 120
integer width = 3611
integer height = 440
integer taborder = 40
string dataobject = "d_sel_report_prod_commesse"
boolean border = false
end type

event ue_imposta_filtro();s_report_stato_magazzino ls_report_stato_magazzino

ls_report_stato_magazzino = message.powerobjectparm

setitem(1,"cod_prodotto_da", ls_report_stato_magazzino.cod_prodotto)
setitem(1,"cod_prodotto_a", ls_report_stato_magazzino.cod_prodotto)

cb_report.postevent("clicked")
end event

event ue_reset();dw_selezione.resetupdate()
end event

event itemchanged;call super::itemchanged;string ls_nomecol, ls_nulla

setnull(ls_nulla)

choose case i_colname
	case "cod_prodotto_da"
		setitem(getrow(), "cod_cat_mer", ls_nulla)
		if isnull(getitemstring(getrow(), "cod_prodotto_a")) then
			setitem(getrow(), "cod_prodotto_a", i_coltext)
		end if
		
	case "cod_prodotto_a"
		setitem(getrow(), "cod_cat_mer", ls_nulla)
		
	case "cod_cat_mer"
		setitem(getrow(), "cod_prodotto_da", ls_nulla)
		setitem(getrow(), "cod_prodotto_a", ls_nulla)
		
	case "flag_det_depositi"
		if i_coltext = "N" then
			setitem(getrow(), "cod_deposito", ls_nulla)
		else
			setitem(getrow(), "flag_det_stock", "N")
		end if
		
	case "flag_visualizza_val"
		if i_coltext = "N" then
			setitem(getrow(), "flag_tipo_valorizzazione", "N")
		end if
		if i_coltext = "S" then
			setitem(getrow(), "flag_tipo_valorizzazione", "S")
		end if
	case "flag_tipo_valorizzazione"
		if i_coltext = "C" then
			dw_selezione.Object.path.Protect=0
///			pb_1.enabled=true
			setitem(getrow(),"flag_det_stock","N")
			dw_selezione.Object.flag_det_stock.Protect=1
			setitem(getrow(),"flag_det_depositi","N")
			dw_selezione.Object.flag_det_depositi.Protect=1
		else
			setitem(getrow(), "path", "")
			dw_selezione.Object.path.Protect=1
///			pb_1.enabled=false
			dw_selezione.Object.flag_det_stock.Protect=0
			dw_selezione.Object.flag_det_depositi.Protect=0
		end if
		
	case "flag_det_stock"
		if i_coltext = "S" then
			setitem(getrow(), "flag_det_depositi", "N")
		end if
		
	case "flag_tipo_layout"
		if i_coltext = "3" then
			setitem(getrow(), "flag_mancanti", "S")
			this.object.flag_mancanti.protect = 1
		else
			this.object.flag_mancanti.protect = 0
		end if
end choose
end event

event pcd_new;call super::pcd_new;dw_selezione.setitem(1,"data_riferimento",datetime(date(today()), 00:00:00))



end event

type dw_report from uo_cs_xx_dw within w_report_prod_commesse
integer x = 46
integer y = 124
integer width = 3611
integer height = 1676
integer taborder = 20
string dataobject = "d_report_prod_commesse"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;//string ls_cod_prodotto,ls_cod_deposito,ls_errore
//long ll_ret, ll_i, ll_riga
//datetime ldt_data_riferimento_sel
//dec{4} ld_scorta_minima
//s_report_stato_magazzino ls_report_stato_magazzino
//uo_mrp luo_mrp
//window l_window
//str_mrp lstr_mrp[]
//datastore dw_grafico
//
//if isvalid(dwo) then
//	// apro il grafico MRP
//	if dataobject = "d_report_stato_magazzino_layout3" then
//		
//		ldt_data_riferimento_sel = dw_selezione.getitemdatetime(1, "data_riferimento")
//		ls_cod_prodotto = getitemstring(getrow(), "rs_cod_prodotto")
//		
//		select scorta_minima
//		into   :ld_scorta_minima
//		from   anag_prodotti
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//		       cod_prodotto = :ls_cod_prodotto;
//
//		if isnull(ld_scorta_minima) then ld_scorta_minima = 0
//		
//		luo_mrp = create uo_mrp
//		
//		ll_ret =luo_mrp.uof_mrp_prodotto(ls_cod_prodotto, &
//													 ldt_data_riferimento_sel, &
//													 ls_cod_deposito, &
//													 ref lstr_mrp[], &
//													 ref ls_errore )
//		
//		if ll_ret < 0 then
//			g_mb.messagebox("Errore calcolo MRP prodotto " + ls_cod_prodotto, ls_errore)
//			destroy luo_mrp
//			return -1
//		end if
//		
//		destroy luo_mrp
//		
//		dw_grafico = create datastore
//		
//		dw_grafico.dataobject = "d_report_stato_magazzino_layout3_gr"
//		
//		for ll_i = 1 to upperbound(lstr_mrp[])
//			ll_riga = dw_grafico.insertrow(0)
//			dw_grafico.setitem(ll_riga, "data_riferimento", lstr_mrp[ll_i].data)
//			dw_grafico.setitem(ll_riga, "quan_fabbisogno", lstr_mrp[ll_i].quan_progressiva)
//			dw_grafico.setitem(ll_riga, "quan_sottoscorta", 0)
//			dw_grafico.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
//
//			ll_riga = dw_grafico.insertrow(0)
//			dw_grafico.setitem(ll_riga, "data_riferimento", lstr_mrp[ll_i].data)
//			dw_grafico.setitem(ll_riga, "quan_fabbisogno", ld_scorta_minima)
//			dw_grafico.setitem(ll_riga, "quan_sottoscorta", 1)
//			dw_grafico.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
//			
//		next
//		
//		window_open_parm(w_report_stato_magazzino_graph, -1, dw_grafico)
//		
//	end if
//	
//	
//	
//	choose case dwo.name
//		case "rd_giacenza"
//			if row > 0 then
//				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
//					
//					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
//					ls_report_stato_magazzino.xpos = xpos
//					ls_report_stato_magazzino.ypos = ypos
//					ls_report_stato_magazzino.tipo_report = "MAGAZZINO"
//					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
//				end if
//			end if
//			
//		case "rd_impegnato"
//			if row > 0 then
//				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
//					
//					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
//					ls_report_stato_magazzino.xpos = xpos
//					ls_report_stato_magazzino.ypos = ypos
//					ls_report_stato_magazzino.tipo_report = "IMPEGNO"
//					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
//				end if
//			end if
//			
//		case "rd_ordinato"
//			if row > 0 then
//				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
//					
//					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
//					ls_report_stato_magazzino.xpos = xpos
//					ls_report_stato_magazzino.ypos = ypos
//					ls_report_stato_magazzino.tipo_report = "ORDINATO"
//					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
//				end if
//			end if
//			
//		case "rd_commesse"
//			if row > 0 then
//				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
//					
//					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
//					ls_report_stato_magazzino.xpos = xpos
//					ls_report_stato_magazzino.ypos = ypos
//					ls_report_stato_magazzino.tipo_report = "COMMESSE"
//					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
//				end if
//			end if
//			
//		case "rd_quan_rda"
//			if row > 0 then
//				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
//					
//					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
//					ls_report_stato_magazzino.xpos = xpos
//					ls_report_stato_magazzino.ypos = ypos
//					ls_report_stato_magazzino.tipo_report = "RDA"
//					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
//				end if
//			end if
//			
//	end choose		
//end if
end event

event itemchanged;call super::itemchanged;resetupdate()

end event

type dw_folder from u_folder within w_report_prod_commesse
integer x = 9
integer y = 12
integer width = 3694
integer height = 1804
integer taborder = 90
boolean border = false
end type

type cb_report from commandbutton within w_report_prod_commesse
integer x = 1714
integer y = 1496
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;boolean lb_presenza_ord_acq_successivi=false

string ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_flag_consegna_sel, &
		 ls_flag_mancanti_sel, ls_data_riferimento_sel, ls_data_inizio, ls_des_prodotto_sel, ls_where, ls_error, &
		 ls_chiave[], ls_cod_prodotto, ls_des_azienda, ls_intestazione, ls_cod_deposito, ls_vettore_nullo[], &
		 ls_cod_deposito_prodotto, ls_des_deposito, ls_des_impegnato, ls_errore, ls_cod_comodo_2, &
		 ls_des_filtro_report, ls_flag_mp_commesse,ls_data_fine, ls_flag_tipo_layout,ls_cod_fornitore, ls_fornitore, &
		 ls_rag_soc_1

datetime ldt_data_riferimento_sel_2, ldt_data_chiusura_annuale, ldt_data_riferimento_sel, ldt_data_comm_inizio, ldt_data_comm_fine, &
         ldt_data_fine, ldt_data_sottoscorta, ldt_data_consegna,ldt_data_vuota

integer li_perc

long ll_i, ll_ret, ll_commesse, li_cont, ll_num_stock, ll_j, ll_riga, ll_num_rows, ll_anno_reg_ord_acq, ll_num_reg_ord_acq

dec{4}	ld_quan_venduta, ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[], ld_vettore_nullo[], &
			ld_scorta_minima,ld_quan_richiesta,ld_quan_in_produzione,ld_impegnato_comm, ld_impegnato_ord_ven, ld_giacenza_prodotto, &
			ld_giacenza, ld_impegnato, ld_ordinato, ld_commesse, ld_scorta, ld_disponibilita, ld_quan_riordino, ld_quan_necessaria

long ll_anno_commessa[], ll_num_commessa[], ll_index, ll_rows_commesse, ll_step
boolean lb_commesse = false
string ls_sel

uo_magazzino luo_magazzino
uo_situazione_magazzino iuo_situazione_magazzino
uo_mrp luo_mrp
str_mrp lstr_mrp[], lstr_vuoto[]

dec{4} ld_impegnato_temp, ld_quan_in_produzione_temp
long ll_count, ll_indice

SetPointer(HourGlass!)

ldt_data_fine = datetime(date("31/12/2099"),00:00:00)
ldt_data_vuota = datetime(date("01/01/1900"),00:00:00)				


hpb_1.setrange(0,100)
hpb_1.position = 0
Yield()

ids_situazione_mag = create datastore
ids_situazione_mag.DataObject = 'd_ds_report_stato_magazzino'
ids_situazione_mag.SetTransObject (sqlca)

dw_report.reset() 

dw_report.setredraw(false)
dw_selezione.accepttext()
ldt_data_riferimento_sel = dw_selezione.getitemdatetime(1, "data_riferimento")
ls_flag_mancanti_sel = dw_selezione.getitemstring(1, "flag_mancanti")
ldt_data_comm_inizio = dw_selezione.getitemdatetime( 1, "data_consegna_inizio")
ldt_data_comm_fine = dw_selezione.getitemdatetime( 1, "data_consegna_fine")

ldt_data_riferimento_sel_2 = datetime(date(ldt_data_riferimento_sel), 00:00:00)
ls_des_filtro_report = "Data Riferimento: " + string(ldt_data_riferimento_sel, "dd/mm/yyyy")

ll_rows_commesse = dw_sel_commesse.rowcount()
ll_step = 0
for ll_index = 1 to ll_rows_commesse
	ls_sel = dw_sel_commesse.getitemstring(ll_index, "sel")
	if ls_sel = "S" then
		lb_commesse = true
		
		if not isnull(dw_sel_commesse.getitemnumber(ll_index, "anno_commessa")) &
			and not isnull(dw_sel_commesse.getitemnumber(ll_index, "num_commessa")) &
			and dw_sel_commesse.getitemnumber(ll_index, "anno_commessa")>0 &
			and dw_sel_commesse.getitemnumber(ll_index, "num_commessa")>0 then
			
			ll_step += 1
			ll_anno_commessa[ll_step] = dw_sel_commesse.getitemnumber(ll_index, "anno_commessa")
			ll_num_commessa[ll_step] = dw_sel_commesse.getitemnumber(ll_index, "num_commessa")
			
			if ll_step = 1 then
				ls_des_filtro_report += " - Commesse: "+ string(ll_anno_commessa[ll_step])+"/"+string(ll_num_commessa[ll_step])
			else
				ls_des_filtro_report += ", " + string(ll_anno_commessa[ll_step])+"/"+string(ll_num_commessa[ll_step])
			end if			
		end if
	end if
next

if not lb_commesse or ll_step = 0 then
	g_mb.messagebox("Apice","E' necessario selezionare almeno una commessa!")	
	return
end if

if ls_flag_mancanti_sel = "S" then
	ls_des_filtro_report += " - SOLO PRODOTTI MANCANTI "
end if

dw_report.object.filtri_report_t.text = ls_des_filtro_report


// *** CONTROLLO DATA

select data_chiusura_annuale
into   :ldt_data_chiusura_annuale
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

if ldt_data_riferimento_sel < ldt_data_chiusura_annuale then
	g_mb.messagebox("Inventario Magazzino", "La data di inventario deve essere posteriore alla data della chiusura annuale")
	return -1
end if

select rag_soc_1
into   :ls_des_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

if not isnull(ls_des_azienda) then ls_intestazione = ls_des_azienda

if isnull(ls_flag_mancanti_sel) or ls_flag_mancanti_sel = "" then ls_flag_mancanti_sel = "N"

ls_data_inizio = string(today(), s_cs_xx.db_funzioni.formato_data)
ls_data_riferimento_sel = string(ldt_data_riferimento_sel, s_cs_xx.db_funzioni.formato_data)
ls_data_fine = string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data)

iuo_situazione_magazzino = create uo_situazione_magazzino

// Ciclo Ordini Vendita Commesse
if iuo_situazione_magazzino.uf_ordini_vendita_commesse(ll_anno_commessa, ll_num_commessa, ids_situazione_mag) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo ordini di vendita commesse")	
	return
end if
hpb_1.position = 10
Yield()

// Ciclo Bolle Vendita Commesse
if iuo_situazione_magazzino.uf_bolle_vendita_commesse(ll_anno_commessa, ll_num_commessa, ids_situazione_mag) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo bolle di vendita commesse")	
	return
end if
hpb_1.position = 20
Yield()

// Ciclo Fatture Vendita Commesse
if iuo_situazione_magazzino.uf_fatture_vendita_commesse(ll_anno_commessa, ll_num_commessa, ids_situazione_mag) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo fatture di vendita")	
	return
end if
hpb_1.position = 30
Yield()

// Ciclo Ordini Acquisto Commesse
if iuo_situazione_magazzino.uf_ordini_acquisto_commesse(ll_anno_commessa, ll_num_commessa, ids_situazione_mag) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo ordini di acquisto")	
	return
end if
hpb_1.position = 40
Yield()

// Ciclo Commesse Commesse
if iuo_situazione_magazzino.uf_commesse_commesse(ll_anno_commessa, ll_num_commessa, ids_situazione_mag) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo commesse")	
	return
end if
hpb_1.position = 50
Yield()

// Ciclo Generale Prodotti Commesse
if iuo_situazione_magazzino.uf_prodotti_generale_commesse(ll_anno_commessa, ll_num_commessa, ids_situazione_mag) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo ordini di vendita")	
	return
end if	

destroy iuo_situazione_magazzino

ll_num_rows = ids_situazione_mag.RowCount()

for ll_i = 1 to ll_num_rows
	
	st_1.text = string(ll_i) + "/" + string(ll_num_rows)
	
	li_perc = round((ll_i * 50) / ll_num_rows,0)
	hpb_1.position = 50 + li_perc
	Yield()
	
	//se il prodotto in esame non è una mp o un sl di una delle commesse selezionate allora ignoralo

	
	ll_riga = dw_report.insertrow(0)		
	dw_report.setitem(ll_riga, "rs_cod_prodotto", ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com"))
	dw_report.setitem(ll_riga, "rs_descrizione", ids_situazione_mag.getitemstring(ll_i, "des_prodotto_com"))

	dw_report.setitem(ll_riga, "rd_giacenza", ids_situazione_mag.getitemdecimal(ll_i, "giacenza_com"))
	dw_report.setitem(ll_riga, "flag_tipo_riga", 1)
	// *** impegnato
	
	ls_cod_prodotto = ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com")
	
	ls_cod_prodotto = ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com")
	
//		select scorta_minima
//		into   :ld_scorta_minima
//		from   anag_prodotti
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_prodotto = :ls_cod_prodotto;
	setnull(ld_scorta_minima)

	if sqlca.sqlcode = 100 then
		g_mb.messagebox( "APICE", "Prodotto " + ls_cod_prodotto + " inesistente", stopsign!)
		return -1
	end if

	if sqlca.sqlcode = -1 then
		g_mb.messagebox( "APICE", "Errore in ricerca prodotto " + ls_cod_prodotto + ":" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if

	if isnull(ld_scorta_minima) then ld_scorta_minima = 0

	dw_report.setitem(ll_riga, "rd_scorta_minima", ld_scorta_minima)
	
	// sommatoria delle richieste di acquisto non evase
	
	ld_quan_richiesta = 0
	
	select sum(quan_richiesta)
	into   :ld_quan_richiesta	
	from   det_rda
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto and
			flag_gen_ord_acq = 'N' and 
			flag_gen_commessa = 'N';		
			
	if isnull(ld_quan_richiesta) then ld_quan_richiesta = 0		
	
	dw_report.setitem(ll_riga, "rd_quan_rda", ld_quan_richiesta	)
		
	SELECT sum(quan_ordine)  
	INTO   :ld_impegnato_ord_ven
	FROM   det_ord_ven LEFT OUTER JOIN tab_tipi_det_ven ON det_ord_ven.cod_azienda = tab_tipi_det_ven.cod_azienda AND det_ord_ven.cod_tipo_det_ven = tab_tipi_det_ven.cod_tipo_det_ven 
							LEFT OUTER JOIN tes_ord_ven      ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda AND det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione AND det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione  
	WHERE  ( tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( tab_tipi_det_ven.flag_tipo_det_ven = 'M' ) AND  
			( det_ord_ven.cod_prodotto = :ls_cod_prodotto ) AND  
			( det_ord_ven.flag_evasione <> 'E' ) AND  
			( det_ord_ven.flag_blocco = 'N' ) AND  
			( tes_ord_ven.flag_blocco = 'N' )    ;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "APICE", "Errore durante lettura impegnato da ordine:" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	//impegnato solo delle commesse selezionate
	//nei vettori ll_anno_commessa[] e ll_num_commessa[]	
	ll_count = upperbound(ll_num_commessa)
	ld_impegnato_temp = 0
	ld_impegnato_comm = 0
	
	for ll_indice = 1 to ll_count
		select quan_impegnata_attuale
		into :ld_impegnato_temp
		from    impegno_mat_prime_commessa 
		left outer join  anag_commesse  on anag_commesse.cod_azienda = impegno_mat_prime_commessa.cod_azienda and 
							anag_commesse.anno_commessa = impegno_mat_prime_commessa.anno_commessa and 
							anag_commesse.num_commessa = impegno_mat_prime_commessa.num_commessa 
		where   impegno_mat_prime_commessa.cod_azienda = :s_cs_xx.cod_azienda and
				  impegno_mat_prime_commessa.cod_prodotto = :ls_cod_prodotto and 				  
				  anag_commesse.anno_commessa = :ll_anno_commessa[ll_indice] and
				  anag_commesse.num_commessa = :ll_num_commessa[ll_indice];
				/*  
		anag_commesse.flag_tipo_avanzamento in ('2', '3') and
					
				  */
		choose case sqlca.sqlcode
			case 0
				ld_impegnato_comm += ld_impegnato_temp
				
			case 100
				//non fare niente
				
			case else
				g_mb.messagebox( "APICE", "Errore durante lettura impegnato da commesse:" + sqlca.sqlerrtext, stopsign!)
				return -1
				
		end choose				
	next

	
	if isnull(ld_impegnato_ord_ven) then ld_impegnato_ord_ven = 0
	if isnull(ld_impegnato_comm) then ld_impegnato_comm = 0
	
	//ld_impegnato_comm += ld_impegnato_ord_ven		
	
	dw_report.setitem(ll_riga, "rd_impegnato", ld_impegnato_comm)
	
	dw_report.setitem(ll_riga, "rd_disponibilita", ids_situazione_mag.getitemdecimal(ll_i, "disp_reale_com"))
	dw_report.setitem(ll_riga, "rd_ordinato", ids_situazione_mag.getitemdecimal(ll_i, "ord_a_fornitore_com"))	
	
	/*
	//Q.tà in produzione delle sole commesse selezionate
	//nei vettori ll_anno_commessa[] e ll_num_commessa[]
	ld_quan_in_produzione = 0
	ld_quan_in_produzione_temp = 0
	
	for ll_indice = 1 to ll_count
		select quan_in_produzione
		into   :ld_quan_in_produzione_temp
		from   anag_commesse
		where  cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto and
					flag_blocco = 'N' and
					flag_tipo_avanzamento <> '9' and
					flag_tipo_avanzamento <> '8' and
					flag_tipo_avanzamento <> '7'and
					anag_commesse.anno_commessa = :ll_anno_commessa[ll_indice] and
					anag_commesse.num_commessa = :ll_num_commessa[ll_indice];
		
		choose case sqlca.sqlcode
			case 0
				ld_quan_in_produzione += ld_quan_in_produzione_temp
				
			case 100
				//non fare niente
				
			case else
				g_mb.messagebox( "APICE", "Errore durante lettura numero commesse in corso:" + sqlca.sqlerrtext, stopsign!)
				return -1
		end choose		
	next	
	*/
	
	//select sum(quan_in_produzione - quan_prodotta)
	select sum(quan_in_produzione)
	into   :ld_quan_in_produzione
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto and
			 flag_blocco = 'N' and
			 flag_tipo_avanzamento <> '9' and
			 flag_tipo_avanzamento <> '8' and
			 flag_tipo_avanzamento <> '7';
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "APICE", "Errore durante lettura numero commesse in corso:" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if		
	
	if isnull(ld_quan_in_produzione) then ld_quan_in_produzione = 0		
	
	dw_report.setitem(ll_riga, "rd_commesse", ld_quan_in_produzione)
	
	// *** giacenza
	
	for li_cont = 1 to 14
		ld_quant_val[li_cont] = 0
	next

	ls_chiave = ls_vettore_nullo
	ld_giacenza_stock = ld_vettore_nullo
	
	luo_magazzino = CREATE uo_magazzino
		
	ll_ret = luo_magazzino.uof_saldo_prod_date_decimal_comm(ls_cod_prodotto, ldt_data_riferimento_sel, ls_where, ld_quant_val, ls_error, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

	destroy luo_magazzino
	
	if ll_ret < 0 then
		g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
		return -1
	end if
	
	ll_num_stock = upperbound(ls_chiave[])

	if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
		dw_report.setitem(ll_riga, "rd_giacenza", ids_situazione_mag.getitemdecimal(ll_i, "giacenza_com"))
	else		
		for ll_j = 1 to ll_num_stock
			if ls_chiave[ll_j] = ls_cod_deposito then
				dw_report.setitem(ll_riga, "rd_giacenza", ld_giacenza_stock[ll_j])
			end if
		next
	end if
	
	ld_giacenza = dw_report.getitemnumber(ll_riga, "rd_giacenza")
	ld_impegnato = dw_report.getitemnumber(ll_riga, "rd_impegnato")
	ld_ordinato = dw_report.getitemnumber(ll_riga, "rd_ordinato")
	
	//era la qta in produzione
	ld_commesse = dw_report.getitemnumber(ll_riga, "rd_commesse")
	
	ld_scorta = dw_report.getitemnumber(ll_riga, "rd_scorta_minima")
	
	ld_disponibilita = ld_giacenza - ld_impegnato + ld_ordinato + ld_commesse
	
	dw_report.setitem(ll_riga, "rd_disponibilita", ld_disponibilita)

next	

dw_report.object.intestazione.text = ls_intestazione
dw_report.setredraw(true)

dw_report.Object.DataWindow.Print.Preview	='Yes'
dw_report.Object.DataWindow.Print.Preview.rulers	='Yes'
dw_report.SetSort("rs_cod_prodotto A, flag_tipo_riga A")
dw_report.Sort()

dw_folder.fu_selecttab(2)
dw_report.Change_DW_Current()

destroy ids_situazione_mag

if ls_flag_mancanti_sel = "S" then//and ls_flag_tipo_layout <> "3" then
	dw_report.setfilter("compute_6 = 'SI'")
	dw_report.filter()
end if

dw_report.setredraw(true)

SetPointer(Arrow!)

end event

type dw_sel_commesse from uo_cs_xx_dw within w_report_prod_commesse
integer x = 46
integer y = 560
integer width = 1646
integer height = 900
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_sel_report_prod_commesse_lista"
boolean vscrollbar = true
end type

