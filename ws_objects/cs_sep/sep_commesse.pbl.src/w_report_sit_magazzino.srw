﻿$PBExportHeader$w_report_sit_magazzino.srw
$PBExportComments$situazione magazzino
forward
global type w_report_sit_magazzino from w_cs_xx_principale
end type
type cb_report from commandbutton within w_report_sit_magazzino
end type
type cb_annulla from commandbutton within w_report_sit_magazzino
end type
type dw_selezione from uo_cs_xx_dw within w_report_sit_magazzino
end type
type cb_prodotti_ricerca_da from cb_prod_ricerca within w_report_sit_magazzino
end type
type cb_prodotti_ricerca_a from cb_prod_ricerca within w_report_sit_magazzino
end type
type dw_report from uo_cs_xx_dw within w_report_sit_magazzino
end type
type dw_folder from u_folder within w_report_sit_magazzino
end type
end forward

global type w_report_sit_magazzino from w_cs_xx_principale
integer x = 73
integer y = 300
integer width = 3776
integer height = 1968
string title = "Report Situazione Magazzino/Impegnato"
cb_report cb_report
cb_annulla cb_annulla
dw_selezione dw_selezione
cb_prodotti_ricerca_da cb_prodotti_ricerca_da
cb_prodotti_ricerca_a cb_prodotti_ricerca_a
dw_report dw_report
dw_folder dw_folder
end type
global w_report_sit_magazzino w_report_sit_magazzino

type variables
datastore ids_situazione_mag
end variables

forward prototypes
public function integer wf_descrizione_impegnato (string fs_cod_prodotto, ref string fs_descrizione, ref string fs_errore)
end prototypes

public function integer wf_descrizione_impegnato (string fs_cod_prodotto, ref string fs_descrizione, ref string fs_errore);long   ll_anno, ll_numero
dec{4} ld_quantita

fs_descrizione = ""

declare cu_ordini cursor for
select  det_ord_ven.anno_registrazione,   
        det_ord_ven.num_registrazione,
		  sum(det_ord_ven.quan_ordine)
from    {oj det_ord_ven RIGHT OUTER JOIN tes_ord_ven ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda AND det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione AND det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione}, {oj tab_tipi_det_ven RIGHT OUTER JOIN det_ord_ven ON tab_tipi_det_ven.cod_azienda = det_ord_ven.cod_azienda AND tab_tipi_det_ven.cod_tipo_det_ven = det_ord_ven.cod_tipo_det_ven}  
where   ( tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda ) AND  
        ( tab_tipi_det_ven.flag_tipo_det_ven = 'M' ) AND  
        ( det_ord_ven.cod_prodotto = :fs_cod_prodotto ) AND  
        ( det_ord_ven.flag_evasione <> 'E' ) AND  
        ( det_ord_ven.flag_blocco = 'N' ) AND  
        ( tes_ord_ven.flag_blocco = 'N' )
group   by det_ord_ven.anno_registrazione, det_ord_ven.num_registrazione
order   by det_ord_ven.anno_registrazione ASC, det_ord_ven.num_registrazione ASC;

open cu_ordini;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante l'apertura del cursore ordini:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

do while true
	
	fetch cu_ordini into :ll_anno,
	                     :ll_numero,
								:ld_quantita;
								
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore durante la fetch del cursore degli ordini:" + sqlca.sqlerrtext
		close cu_ordini;
		rollback;
		return -1
	end if
	
	if isnull(ll_anno) then ll_anno = 0
	if isnull(ll_numero) then ll_numero = 0
	if isnull(ld_quantita) then ld_quantita = 0
	
	fs_descrizione += " Nr. " + string(ld_quantita, "###,###,##0.00") + " ordine " + string(ll_anno) + "/" + string(ll_numero) + "~r~n"
	
loop

close cu_ordini;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la chiusura del cursore ordini:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

declare cu_commesse cursor for
select  anno_commessa ,
        num_commessa  ,
		   sum(quan_impegnata_attuale)
from    impegno_mat_prime_commessa      
where   ( impegno_mat_prime_commessa.cod_azienda = :s_cs_xx.cod_azienda ) and 
        ( impegno_mat_prime_commessa.cod_prodotto = :fs_cod_prodotto )
group by anno_commessa, num_commessa
order by anno_commessa ASC, num_commessa ASC;

open cu_commesse;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante l'apertura del cursore commesse:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

do while true
	
	fetch cu_commesse into :ll_anno,
	                       :ll_numero,
								  :ld_quantita;
								
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore durante la fetch del cursore delle commesse:" + sqlca.sqlerrtext
		close cu_commesse;
		rollback;
		return -1
	end if
	
	if isnull(ll_anno) then ll_anno = 0
	if isnull(ll_numero) then ll_numero = 0
	if isnull(ld_quantita) then ld_quantita = 0
	
	fs_descrizione += " Nr. " + string(ld_quantita, "###,###,##0.00") + " commessa " + string(ll_anno) + "/" + string(ll_numero) + "~r~n"
	
loop

close cu_commesse;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la chiusura del cursore commesse:" + sqlca.sqlerrtext
	rollback;
	return -1
end if
		  
return 0
end function

on w_report_sit_magazzino.create
int iCurrent
call super::create
this.cb_report=create cb_report
this.cb_annulla=create cb_annulla
this.dw_selezione=create dw_selezione
this.cb_prodotti_ricerca_da=create cb_prodotti_ricerca_da
this.cb_prodotti_ricerca_a=create cb_prodotti_ricerca_a
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_report
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.cb_prodotti_ricerca_da
this.Control[iCurrent+5]=this.cb_prodotti_ricerca_a
this.Control[iCurrent+6]=this.dw_report
this.Control[iCurrent+7]=this.dw_folder
end on

on w_report_sit_magazzino.destroy
call super::destroy
destroy(this.cb_report)
destroy(this.cb_annulla)
destroy(this.dw_selezione)
destroy(this.cb_prodotti_ricerca_da)
destroy(this.cb_prodotti_ricerca_a)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_cat_mer", &
							sqlca, &
							"tab_cat_mer", &
							"cod_cat_mer", &
                     "des_cat_mer", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_deposito", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
                     "des_deposito", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
iuo_dw_main = dw_report
//

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = cb_annulla
//l_objects[4] = sle_1
l_objects[4] = cb_prodotti_ricerca_da
l_objects[5] = cb_prodotti_ricerca_a
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)
end event

event resize;call super::resize;dw_folder.width = newwidth - 20
dw_folder.height = newheight - 20
dw_report.width = newwidth - 120
dw_report.height = newheight - 170
end event

type cb_report from commandbutton within w_report_sit_magazzino
integer x = 1641
integer y = 1060
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, ls_flag_consegna_sel, &
		 ls_flag_mancanti_sel, ls_data_riferimento_sel, ls_data_inizio, ls_des_prodotto_sel, ls_where, ls_error, &
		 ls_chiave[], ls_cod_prodotto, ls_des_azienda, ls_intestazione, ls_cod_deposito, ls_flag_impegnato, ls_vettore_nullo[], &
		 ls_cod_deposito_prodotto, ls_flag_deposito, ls_des_deposito, ls_des_impegnato, ls_errore

datetime ldt_data_riferimento_sel_2, ldt_data_chiusura_annuale, ldt_data_riferimento_sel
long ll_i, ll_i_mancanti, ll_ret
uo_situazione_magazzino iuo_situazione_magazzino
dec{4}	ld_quan_venduta, ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[], ld_vettore_nullo[]

dec{4}   ld_impegnato_comm, ld_impegnato_ord_ven, ld_giacenza_prodotto
long		ll_commesse, li_cont, ll_num_stock, ll_j, ll_riga

uo_magazzino luo_magazzino

SetPointer(HourGlass!)

ids_situazione_mag = create datastore
ids_situazione_mag.DataObject = 'd_sit_magazzino_comodo'
ids_situazione_mag.SetTransObject (sqlca)

dw_report.reset() 
dw_selezione.accepttext()
ldt_data_riferimento_sel = dw_selezione.getitemdatetime(1, "data_riferimento")
ls_cod_prodotto_da_sel = dw_selezione.getitemstring(1, "cod_prodotto_da")
ls_cod_prodotto_a_sel = dw_selezione.getitemstring(1, "cod_prodotto_a")
ls_cod_deposito = dw_selezione.getitemstring( 1, "cod_deposito")
ls_cod_cat_merce_sel = dw_selezione.getitemstring(1, "cod_cat_mer")
ls_flag_mancanti_sel = dw_selezione.getitemstring(1, "flag_mancanti")
ls_flag_impegnato = dw_selezione.getitemstring( 1, "flag_impegnato")
ls_flag_deposito = dw_selezione.getitemstring( 1, "flag_deposito")

ldt_data_riferimento_sel_2 = datetime(date(ldt_data_riferimento_sel), 00:00:00)

dw_report.object.titolo.text = "Situazione Magazzino al " + string(ldt_data_riferimento_sel, "dd/mm/yyyy")

// *** CONTROLLO DATA

select data_chiusura_annuale
into   :ldt_data_chiusura_annuale
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

if ldt_data_riferimento_sel < ldt_data_chiusura_annuale then
	g_mb.messagebox("Inventario Magazzino", "La data di inventario deve essere posteriore alla data della chiusura annuale")
	return -1
end if

select rag_soc_1
into   :ls_des_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

if not isnull(ls_des_azienda) then ls_intestazione = ls_des_azienda

if isnull(ls_flag_mancanti_sel) or ls_flag_mancanti_sel = "" then ls_flag_mancanti_sel = "N"
if isnull(ls_flag_impegnato) or ls_flag_impegnato = "" then ls_flag_impegnato = "N"
if isnull(ls_flag_deposito) or ls_flag_deposito = "" then ls_flag_deposito = "N"


if isnull(ls_cod_cat_merce_sel) then
	ls_cod_cat_merce_sel = "%"
end if	

ls_data_inizio = string(today(), s_cs_xx.db_funzioni.formato_data)
ls_data_riferimento_sel = string(ldt_data_riferimento_sel, s_cs_xx.db_funzioni.formato_data)

iuo_situazione_magazzino = create uo_situazione_magazzino

///////////////st_stato_avanzamento.text = "Ciclo Ordini Vendita"
if iuo_situazione_magazzino.wf_ordini_vendita(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, "N", ls_data_inizio, ls_data_riferimento_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo ordini di vendita")	
	return
end if

///////////////st_stato_avanzamento.text = "Ciclo Bolle Vendita"
if iuo_situazione_magazzino.wf_bolle_vendita(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, "N", ls_data_inizio, ls_data_riferimento_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo bolle di vendita")	
	return
end if

///////////////st_stato_avanzamento.text = "Ciclo Fatture Vendita"
if iuo_situazione_magazzino.wf_fatture_vendita(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, "N", ls_data_inizio, ls_data_riferimento_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo fatture di vendita")	
	return
end if

///////////////st_stato_avanzamento.text = "Ciclo Ordini Acquisto"
if iuo_situazione_magazzino.wf_ordini_acquisto(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, "N", ls_data_inizio, ls_data_riferimento_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo ordini di acquisto")	
	return
end if

///////////////st_stato_avanzamento.text = "Ciclo Commesse"
if iuo_situazione_magazzino.wf_commesse(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, "N", ls_data_inizio, ls_data_riferimento_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo commesse")	
	return
end if

//////if ls_flag_prodotti_sel = "S" then
	////////////////st_stato_avanzamento.text = "Ciclo Generale Prodotti"
	if iuo_situazione_magazzino.wf_prodotti_generale(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
		g_mb.messagebox("Apice","Errore in funzione ciclo ordini di vendita")	
		return
	end if	
//////end if

destroy iuo_situazione_magazzino

ll_i_mancanti = 1 

for ll_i = 1 to ids_situazione_mag.RowCount()
	if ls_flag_mancanti_sel = "S" then
		if (ids_situazione_mag.getitemdecimal(ll_i, "da_produrre_com")) > 0 or &
			(ids_situazione_mag.getitemdecimal(ll_i, "da_ordinare_com")) > 0 or &
			(ids_situazione_mag.getitemdecimal(ll_i, "c_lavorazione_com")) > 0 then 

			dw_report.insertrow(0)
			
			ls_cod_prodotto = ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com")
			
			dw_report.setitem(ll_i_mancanti, "rs_cod_prodotto", ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com"))
			dw_report.setitem(ll_i_mancanti, "rs_descrizione", ids_situazione_mag.getitemstring(ll_i, "des_prodotto_com"))
			
			// *** impegnato
			
		   SELECT sum(quan_ordine) 
			into   :ld_impegnato_ord_ven
			FROM   det_ord_ven RIGHT OUTER JOIN tes_ord_ven ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda AND det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione AND det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione, 
					 tab_tipi_det_ven RIGHT OUTER JOIN det_ord_ven ON tab_tipi_det_ven.cod_azienda = det_ord_ven.cod_azienda AND tab_tipi_det_ven.cod_tipo_det_ven = det_ord_ven.cod_tipo_det_ven 
			WHERE  ( tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda ) AND  
					 ( tab_tipi_det_ven.flag_tipo_det_ven = 'M' ) AND  
					 ( det_ord_ven.cod_prodotto = :ls_cod_prodotto ) AND  
					 ( det_ord_ven.flag_evasione <> 'E' ) AND  
					 ( det_ord_ven.flag_blocco = 'N' ) AND  
					 ( tes_ord_ven.flag_blocco = 'N' );
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "APICE", "Errore durante lettura impegnato da ordine:" + sqlca.sqlerrtext, stopsign!)
				return -1
			end if

			SELECT  sum(quan_impegnata_attuale)
			into    :ld_impegnato_comm
		   FROM    impegno_mat_prime_commessa      
			WHERE   ( impegno_mat_prime_commessa.cod_azienda = :s_cs_xx.cod_azienda ) and          
			        ( impegno_mat_prime_commessa.cod_prodotto = :ls_cod_prodotto );
					  
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "APICE", "Errore durante lettura impegnato da commesse:" + sqlca.sqlerrtext, stopsign!)
				return -1
			end if					  
			
			if isnull(ld_impegnato_ord_ven) then ld_impegnato_ord_ven = 0
			if isnull(ld_impegnato_comm) then ld_impegnato_comm = 0
			
			ld_impegnato_comm += ld_impegnato_ord_ven
			
			dw_report.setitem(ll_i_mancanti, "rd_impegnato", ld_impegnato_comm)
			
			if ls_flag_impegnato = "S" then
				ll_ret = wf_descrizione_impegnato( ls_cod_prodotto, ls_des_impegnato, ls_errore)
				if ll_ret = 0 then
					ls_des_impegnato = "Totale " + string( ld_impegnato_comm, "###,###,##0.00") + " ~r~n" + ls_des_impegnato
					dw_report.setitem(ll_i_mancanti, "rs_des_impegnato", ls_des_impegnato)
				end if
			end if
			
			dw_report.setitem(ll_i_mancanti, "rd_disponibilita", ids_situazione_mag.getitemdecimal(ll_i, "disp_reale_com"))
			dw_report.setitem(ll_i_mancanti, "rd_ordinato", ids_situazione_mag.getitemdecimal(ll_i, "ord_a_fornitore_com"))	
			dw_report.setitem(ll_i_mancanti, "flag_tipo_riga", 1)
			
			// *** commesse in corso
			
			select count(*)
			into   :ll_commesse
			from   anag_commesse
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :ls_cod_prodotto and
					 flag_blocco = 'N' and
					 flag_tipo_avanzamento <> '9' and
					 flag_tipo_avanzamento <> '8' and
					 flag_tipo_avanzamento <> '7';
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "APICE", "Errore durante lettura numero commesse in corso:" + sqlca.sqlerrtext, stopsign!)
				return -1
			end if		
			
			if isnull(ll_commesse) then ll_commesse = 0
			
			dw_report.setitem(ll_i_mancanti, "rd_commesse", ll_commesse)			
			
			// *** giacenza
			
			for li_cont = 1 to 14
				ld_quant_val[li_cont] = 0
			next
	
			ls_chiave = ls_vettore_nullo
			ld_giacenza_stock = ld_vettore_nullo
			
			luo_magazzino = CREATE uo_magazzino
				
			ll_ret = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_riferimento_sel, ls_where, ld_quant_val, ls_error, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
	
			destroy luo_magazzino
			
			if ll_ret < 0 then
				g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
				return -1
			end if
			
			ll_num_stock = upperbound(ls_chiave[])

			if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
				dw_report.setitem(ll_i_mancanti, "rd_giacenza", ids_situazione_mag.getitemdecimal(ll_i, "giacenza_com"))
			else		
				for ll_j = 1 to ll_num_stock
					if ls_chiave[ll_j] = ls_cod_deposito then
						dw_report.setitem(ll_i_mancanti, "rd_giacenza", ld_giacenza_stock[ll_j])
					end if
				next
			end if
		
			// *** visualizzazione dettagli depositi
			if ls_flag_deposito = "S" then
	
				if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
					
					if ll_num_stock > 0 then	// più depositi
					
						for ll_j = 1 to (ll_num_stock)
					
							ll_i_mancanti ++
							
							dw_report.insertrow(0)
							
							ls_cod_prodotto = ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com")
							
							dw_report.setitem(ll_i_mancanti, "rs_cod_prodotto", ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com"))
							dw_report.setitem(ll_i_mancanti, "rs_descrizione", ids_situazione_mag.getitemstring(ll_i, "des_prodotto_com"))						
							dw_report.setitem(ll_i_mancanti, "rs_cod_deposito", ls_chiave[ll_j])
							
							select des_deposito
							into   :ls_des_deposito
							from   anag_depositi
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_deposito = :ls_chiave[ll_j];								
								
							dw_report.setitem(ll_i_mancanti, "rs_des_deposito", ls_des_deposito)		
							dw_report.setitem(ll_i_mancanti, "rd_giacenza_dep", ld_giacenza_stock[ll_j])
							
						next
						
					end if					
					
				end if	
				
			end if
			
			ll_i_mancanti ++
		end if
	else 			
		
		ll_riga = dw_report.insertrow(0)		
		dw_report.setitem(ll_riga, "rs_cod_prodotto", ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com"))
		dw_report.setitem(ll_riga, "rs_descrizione", ids_situazione_mag.getitemstring(ll_i, "des_prodotto_com"))
		dw_report.setitem(ll_riga, "rd_giacenza", ids_situazione_mag.getitemdecimal(ll_i, "giacenza_com"))
		dw_report.setitem(ll_riga, "flag_tipo_riga", 1)
		
		// *** impegnato
		
		ls_cod_prodotto = ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com")
		
		SELECT sum(quan_ordine) 
		into   :ld_impegnato_ord_ven
		FROM   det_ord_ven RIGHT OUTER JOIN tes_ord_ven ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda AND det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione AND det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione, 
		       tab_tipi_det_ven RIGHT OUTER JOIN det_ord_ven ON tab_tipi_det_ven.cod_azienda = det_ord_ven.cod_azienda AND tab_tipi_det_ven.cod_tipo_det_ven = det_ord_ven.cod_tipo_det_ven
		WHERE  ( tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda ) AND  
				 ( tab_tipi_det_ven.flag_tipo_det_ven = 'M' ) AND  
				 ( det_ord_ven.cod_prodotto = :ls_cod_prodotto ) AND  
				 ( det_ord_ven.flag_evasione <> 'E' ) AND  
				 ( det_ord_ven.flag_blocco = 'N' ) AND  
				 ( tes_ord_ven.flag_blocco = 'N' );
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "APICE", "Errore durante lettura impegnato da ordine:" + sqlca.sqlerrtext, stopsign!)
			return -1
		end if

		SELECT  sum(quan_impegnata_attuale)
		into    :ld_impegnato_comm
		FROM    impegno_mat_prime_commessa      
		WHERE   ( impegno_mat_prime_commessa.cod_azienda = :s_cs_xx.cod_azienda ) and          
				  ( impegno_mat_prime_commessa.cod_prodotto = :ls_cod_prodotto );
				  
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "APICE", "Errore durante lettura impegnato da commesse:" + sqlca.sqlerrtext, stopsign!)
			return -1
		end if					  
		
		if isnull(ld_impegnato_ord_ven) then ld_impegnato_ord_ven = 0
		if isnull(ld_impegnato_comm) then ld_impegnato_comm = 0
		
		ld_impegnato_comm += ld_impegnato_ord_ven		
		
		dw_report.setitem(ll_riga, "rd_impegnato", ld_impegnato_comm)
		
		if ls_flag_impegnato = "S" then
			ll_ret = wf_descrizione_impegnato( ls_cod_prodotto, ls_des_impegnato, ls_errore)
			if ll_ret = 0 then
				ls_des_impegnato = "Totale " + string( ld_impegnato_comm, "###,###,##0.00") + " ~r~n" + ls_des_impegnato
				dw_report.setitem(ll_riga, "rs_des_impegnato", ls_des_impegnato)
			end if
		end if		
		
		dw_report.setitem(ll_riga, "rd_disponibilita", ids_situazione_mag.getitemdecimal(ll_i, "disp_reale_com"))
		dw_report.setitem(ll_riga, "rd_ordinato", ids_situazione_mag.getitemdecimal(ll_i, "ord_a_fornitore_com"))	
		
		// *** commesse in corso
		
		select count(*)
		into   :ll_commesse
		from   anag_commesse
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto and
				 flag_blocco = 'N' and
				 flag_tipo_avanzamento <> '9' and
				 flag_tipo_avanzamento <> '8' and
				 flag_tipo_avanzamento <> '7';
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "APICE", "Errore durante lettura numero commesse in corso:" + sqlca.sqlerrtext, stopsign!)
			return -1
		end if		
		
		if isnull(ll_commesse) then ll_commesse = 0		
		
		dw_report.setitem(ll_riga, "rd_commesse", ll_commesse)
		
		// *** giacenza
		
		for li_cont = 1 to 14
			ld_quant_val[li_cont] = 0
		next

		ls_chiave = ls_vettore_nullo
		ld_giacenza_stock = ld_vettore_nullo
		
		luo_magazzino = CREATE uo_magazzino
			
		ll_ret = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_riferimento_sel, ls_where, ld_quant_val, ls_error, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

		destroy luo_magazzino
		
		if ll_ret < 0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
			return -1
		end if
		
		ll_num_stock = upperbound(ls_chiave[])

		if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
			dw_report.setitem(ll_riga, "rd_giacenza", ids_situazione_mag.getitemdecimal(ll_i, "giacenza_com"))
		else		
			for ll_j = 1 to ll_num_stock
				if ls_chiave[ll_j] = ls_cod_deposito then
					dw_report.setitem(ll_riga, "rd_giacenza", ld_giacenza_stock[ll_j])
				end if
			next
		end if
	
		// *** visualizzazione dettagli depositi
		if ls_flag_deposito = "S" then

			if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
				
				if ll_num_stock > 0 then	// più depositi
				
					for ll_j = 1 to (ll_num_stock)
				
						ll_riga = dw_report.insertrow(0)
						dw_report.setitem( ll_riga, "rs_cod_prodotto", ls_cod_prodotto)
						dw_report.setitem( ll_riga, "rs_descrizione", ids_situazione_mag.getitemstring(ll_i, "des_prodotto_com"))						
						dw_report.setitem( ll_riga, "rs_cod_deposito", ls_chiave[ll_j])
						
						select des_deposito
						into   :ls_des_deposito
						from   anag_depositi
						where  cod_azienda = :s_cs_xx.cod_azienda and
						       cod_deposito = :ls_chiave[ll_j];								 						
						
						dw_report.setitem( ll_riga, "rs_des_deposito", ls_des_deposito)		
						dw_report.setitem( ll_riga, "rd_giacenza_dep", ld_giacenza_stock[ll_j])
						dw_report.setitem( ll_riga, "flag_tipo_riga", 2)
						
					next
					
				end if					
				
			end if	
			
		end if		
		
	end if
next	

dw_report.object.intestazione.text = ls_intestazione
dw_report.setredraw(true)

dw_report.Object.DataWindow.Print.Preview	='Yes'
dw_report.Object.DataWindow.Print.Preview.rulers	='Yes'
dw_report.SetSort("rs_cod_prodotto A, flag_tipo_riga A")
dw_report.Sort()

dw_folder.fu_selecttab(2)
dw_report.Change_DW_Current()

////////////////st_stato_avanzamento.text = "Elaborazione Eseguita"

destroy ids_situazione_mag


SetPointer(Arrow!)
end event

type cb_annulla from commandbutton within w_report_sit_magazzino
integer x = 2030
integer y = 1060
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

type dw_selezione from uo_cs_xx_dw within w_report_sit_magazzino
integer x = 41
integer y = 168
integer width = 2688
integer height = 868
integer taborder = 40
string dataobject = "d_sel_report_sit_magazzino"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_nomecol, ls_nulla

setnull(ls_nulla)

choose case i_colname
	case "cod_prodotto_da"
		setitem(getrow(), "cod_cat_mer", ls_nulla)
		if isnull(getitemstring(getrow(), "cod_prodotto_a")) then
			setitem(getrow(), "cod_prodotto_a", i_coltext)
		end if
		
	case "cod_prodotto_a"
		setitem(getrow(), "cod_cat_mer", ls_nulla)
		
	case "cod_cat_mer"
		setitem(getrow(), "cod_prodotto_da", ls_nulla)
		setitem(getrow(), "cod_prodotto_a", ls_nulla)
		
	case "flag_det_depositi"
		if i_coltext = "N" then
			setitem(getrow(), "cod_deposito", ls_nulla)
		else
			setitem(getrow(), "flag_det_stock", "N")
		end if
		
	case "flag_visualizza_val"
		if i_coltext = "N" then
			setitem(getrow(), "flag_tipo_valorizzazione", "N")
		end if
		if i_coltext = "S" then
			setitem(getrow(), "flag_tipo_valorizzazione", "S")
		end if
	case "flag_tipo_valorizzazione"
		if i_coltext = "C" then
			dw_selezione.Object.path.Protect=0
///			pb_1.enabled=true
			setitem(getrow(),"flag_det_stock","N")
			dw_selezione.Object.flag_det_stock.Protect=1
			setitem(getrow(),"flag_det_depositi","N")
			dw_selezione.Object.flag_det_depositi.Protect=1
		else
			setitem(getrow(), "path", "")
			dw_selezione.Object.path.Protect=1
///			pb_1.enabled=false
			dw_selezione.Object.flag_det_stock.Protect=0
			dw_selezione.Object.flag_det_depositi.Protect=0
		end if
		
	case "flag_det_stock"
		if i_coltext = "S" then
			setitem(getrow(), "flag_det_depositi", "N")
		end if
end choose
end event

event pcd_new;call super::pcd_new;dw_selezione.setitem(1,"data_riferimento",datetime(date(today()), 00:00:00))



end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_a")
end choose
end event

type cb_prodotti_ricerca_da from cb_prod_ricerca within w_report_sit_magazzino
integer x = 2327
integer y = 272
integer width = 73
integer height = 72
integer taborder = 10
end type

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto_da"
end event

type cb_prodotti_ricerca_a from cb_prod_ricerca within w_report_sit_magazzino
integer x = 2327
integer y = 352
integer width = 73
integer height = 72
integer taborder = 3
end type

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto_a"
end event

type dw_report from uo_cs_xx_dw within w_report_sit_magazzino
integer x = 46
integer y = 124
integer width = 3611
integer height = 1676
integer taborder = 20
string dataobject = "d_report_sit_magazzino"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_sit_magazzino
integer x = 9
integer y = 12
integer width = 3694
integer height = 1828
integer taborder = 90
boolean border = false
end type

