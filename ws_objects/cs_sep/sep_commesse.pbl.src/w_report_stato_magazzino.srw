﻿$PBExportHeader$w_report_stato_magazzino.srw
$PBExportComments$situazione magazzino
forward
global type w_report_stato_magazzino from w_cs_xx_principale
end type
type dw_depositi_giacenza from uo_dddw_checkbox within w_report_stato_magazzino
end type
type cb_filtro_predefinito from commandbutton within w_report_stato_magazzino
end type
type cb_memo_filtro from commandbutton within w_report_stato_magazzino
end type
type st_2 from statictext within w_report_stato_magazzino
end type
type cb_gen_rda from commandbutton within w_report_stato_magazzino
end type
type st_1 from statictext within w_report_stato_magazzino
end type
type hpb_1 from hprogressbar within w_report_stato_magazzino
end type
type cb_annulla from commandbutton within w_report_stato_magazzino
end type
type cb_report from commandbutton within w_report_stato_magazzino
end type
type dw_folder from u_folder within w_report_stato_magazzino
end type
type dw_selezione from uo_cs_xx_dw within w_report_stato_magazzino
end type
type dw_report from uo_cs_xx_dw within w_report_stato_magazzino
end type
end forward

global type w_report_stato_magazzino from w_cs_xx_principale
integer x = 73
integer y = 300
integer width = 3762
integer height = 2216
string title = "Report Stato Magazzini / Deposito"
dw_depositi_giacenza dw_depositi_giacenza
cb_filtro_predefinito cb_filtro_predefinito
cb_memo_filtro cb_memo_filtro
st_2 st_2
cb_gen_rda cb_gen_rda
st_1 st_1
hpb_1 hpb_1
cb_annulla cb_annulla
cb_report cb_report
dw_folder dw_folder
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_stato_magazzino w_report_stato_magazzino

type variables
datastore ids_situazione_mag
end variables

forward prototypes
public function integer wf_descrizione_impegnato (string fs_cod_prodotto, ref string fs_descrizione, ref string fs_errore)
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
public subroutine wf_proteggi_campi (string as_tipo_layout)
public subroutine wf_giacenza (string as_cod_prodotto, ref decimal ad_quantita)
public subroutine wf_totale_venduto (datetime adt_data_inizio, datetime adt_data_fine, string as_cod_prodotto, long al_row)
public subroutine wf_totale_consumato (datetime adt_data_inizio, datetime adt_data_fine, string as_cod_prodotto, long al_row)
public subroutine wf_totale_caricato (datetime adt_data_inizio, datetime adt_data_fine, string as_cod_prodotto, long al_row)
end prototypes

public function integer wf_descrizione_impegnato (string fs_cod_prodotto, ref string fs_descrizione, ref string fs_errore);long   ll_anno, ll_numero
dec{4} ld_quantita

fs_descrizione = ""

declare cu_ordini cursor for
select  det_ord_ven.anno_registrazione,   
        det_ord_ven.num_registrazione,
		  sum(det_ord_ven.quan_ordine)
from    {oj det_ord_ven RIGHT OUTER JOIN tes_ord_ven ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda AND det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione AND det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione}, {oj tab_tipi_det_ven RIGHT OUTER JOIN det_ord_ven ON tab_tipi_det_ven.cod_azienda = det_ord_ven.cod_azienda AND tab_tipi_det_ven.cod_tipo_det_ven = det_ord_ven.cod_tipo_det_ven}  
where   ( tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda ) AND  
        ( tab_tipi_det_ven.flag_tipo_det_ven = 'M' ) AND  
        ( det_ord_ven.cod_prodotto = :fs_cod_prodotto ) AND  
        ( det_ord_ven.flag_evasione <> 'E' ) AND  
        ( det_ord_ven.flag_blocco = 'N' ) AND  
        ( tes_ord_ven.flag_blocco = 'N' )
group   by det_ord_ven.anno_registrazione, det_ord_ven.num_registrazione
order   by det_ord_ven.anno_registrazione ASC, det_ord_ven.num_registrazione ASC;

open cu_ordini;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante l'apertura del cursore ordini:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

do while true
	
	fetch cu_ordini into :ll_anno,
	                     :ll_numero,
								:ld_quantita;
								
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore durante la fetch del cursore degli ordini:" + sqlca.sqlerrtext
		close cu_ordini;
		rollback;
		return -1
	end if
	
	if isnull(ll_anno) then ll_anno = 0
	if isnull(ll_numero) then ll_numero = 0
	if isnull(ld_quantita) then ld_quantita = 0
	
	fs_descrizione += " Nr. " + string(ld_quantita, "###,###,##0.00") + " ordine " + string(ll_anno) + "/" + string(ll_numero) + "~r~n"
	
loop

close cu_ordini;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la chiusura del cursore ordini:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

declare cu_commesse cursor for
select  anno_commessa ,
        num_commessa  ,
		   sum(quan_impegnata_attuale)
from    impegno_mat_prime_commessa      
where   ( impegno_mat_prime_commessa.cod_azienda = :s_cs_xx.cod_azienda ) and 
        ( impegno_mat_prime_commessa.cod_prodotto = :fs_cod_prodotto )
group by anno_commessa, num_commessa
order by anno_commessa ASC, num_commessa ASC;

open cu_commesse;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante l'apertura del cursore commesse:" + sqlca.sqlerrtext
	rollback;
	return -1
end if

do while true
	
	fetch cu_commesse into :ll_anno,
	                       :ll_numero,
								  :ld_quantita;
								
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore durante la fetch del cursore delle commesse:" + sqlca.sqlerrtext
		close cu_commesse;
		rollback;
		return -1
	end if
	
	if isnull(ll_anno) then ll_anno = 0
	if isnull(ll_numero) then ll_numero = 0
	if isnull(ld_quantita) then ld_quantita = 0
	
	fs_descrizione += " Nr. " + string(ld_quantita, "###,###,##0.00") + " commessa " + string(ll_anno) + "/" + string(ll_numero) + "~r~n"
	
loop

close cu_commesse;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la chiusura del cursore commesse:" + sqlca.sqlerrtext
	rollback;
	return -1
end if
		  
return 0
end function

public subroutine wf_memorizza_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero,ll_cont
datetime ldt_data

if g_mb.messagebox("Omnia","Memorizza l'attuale impostazione dei filtro come predefinito?",Question!,YesNo!,2) = 2 then return

ls_colcount = dw_selezione.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""
for ll_i = 1 to ll_colonne
	ls_nome_colonna = dw_selezione.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_selezione.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		ldt_data = dw_selezione.getitemdatetime(dw_selezione.getrow(), ls_nome_colonna)
		if isnull(ldt_data) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
		end if
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		ls_stringa = dw_selezione.getitemstring(dw_selezione.getrow(), ls_nome_colonna)
		if isnull(ls_stringa) then
			ls_memo += "NULL~t"
		else
			ls_memo += ls_stringa + "~t"
		end if
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		ll_numero = dw_selezione.getitemnumber(dw_selezione.getrow(), ls_nome_colonna)
		if isnull(ll_numero) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ll_numero) + "~t"
		end if
	end if

next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'SITMAG';
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente =  :ls_cod_utente and
		   tipo_filtro = 'SITMAG';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
else
	insert into filtri_manutenzioni
		(cod_azienda,
		 cod_utente,
		 filtri,
		 tipo_filtro)
	 values
		 (:s_cs_xx.cod_azienda,
		  :ls_cod_utente,
		  :ls_memo,
		  'SITMAG');
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

commit;

return
end subroutine

public subroutine wf_leggi_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente, ls_null
long ll_i, ll_colonne, ll_numero
datetime ldt_data, ldt_data_chiusura_annuale
date ld_data

ls_colcount = dw_selezione.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente
setnull(ls_null)

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'SITMAG';
if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	goto imposta_date_venduto
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_selezione.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_selezione.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_selezione.setitem(dw_selezione.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_selezione.setitem(dw_selezione.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_selezione.setitem(dw_selezione.getrow(),ls_nome_colonna, ll_numero)
	end if

next

imposta_date_venduto:
dw_selezione.setitem(1,"cod_deposito", ls_null)

if isnull(dw_selezione.getitemdatetime(1,"data_venduto_inizio")) then
	select data_chiusura_annuale
	into	:ldt_data_chiusura_annuale
	from 	con_magazzino
	where cod_azienda = :s_cs_xx.cod_azienda;
	
	if isnull(ldt_data_chiusura_annuale) then
		ld_data = relativedate(date("01/01/" + string(Year(today()))),1)
	else
		ld_data = relativedate(date(ldt_data_chiusura_annuale),1)
	end if
	
	ldt_data_chiusura_annuale = datetime(ld_data,00:00:00)
	
	dw_selezione.setitem(1,"data_venduto_inizio", ldt_data_chiusura_annuale)
end if

if isnull(dw_selezione.getitemdatetime(1,"data_venduto_fine")) then
	ldt_data_chiusura_annuale = datetime(today(),00:00:00)
	dw_selezione.setitem(1,"data_venduto_fine", ldt_data_chiusura_annuale)
end if


return
end subroutine

public subroutine wf_proteggi_campi (string as_tipo_layout);//questa funzione protegge o sprotegge i campi di ricerca in base al tipo layout scelto (da 1 a 4)

string 	ls_count, ls_column_name, ls_protect, ls_col_index
long 		li_index

ls_count = dw_selezione.Describe("DataWindow.Column.Count")
//cb_fornitori_ricerca.enabled = true //sempre abilitato tranne nel layout 4

choose case as_tipo_layout
	case "1", "2"	//tutti sprotetti
		ls_protect = "0"
		
		for li_index = 1 to integer(ls_count)
			ls_col_index = "#"+string(li_index)
			ls_column_name =dw_selezione.Describe(ls_col_index+".name")
			
			if ls_column_name="flag_tipo_layout" then continue
			
			dw_selezione.modify(ls_column_name+'.protect='+ls_protect)
		next		
		
	case "3" //tutti sprotetti meno che flag_mancanti che viene però impostato a SI
		ls_protect = "0"
		
		for li_index = 1 to integer(ls_count)
			ls_col_index = "#"+string(li_index)
			ls_column_name =dw_selezione.Describe(ls_col_index+".name")
			
			if ls_column_name="flag_tipo_layout" then continue
			
			dw_selezione.modify(ls_column_name+'.protect='+ls_protect)
		next
		ls_protect = "1"
		dw_selezione.modify('flag_mancanti.protect='+ls_protect)
		
	case "4" //tutti PROTETTI tranne data riferimento, da prodotto a prodotto, cat merceolog. codice comodo 2
		ls_protect = "1"
		
		for li_index = 1 to integer(ls_count)
			ls_col_index = "#"+string(li_index)
			ls_column_name =dw_selezione.Describe(ls_col_index+".name")
			
			if ls_column_name="flag_tipo_layout" then continue
			
			dw_selezione.modify(ls_column_name+'.protect='+ls_protect)
		next
		ls_protect = "0"
		dw_selezione.modify('data_riferimento.protect='+ls_protect)
		dw_selezione.modify('cod_prodotto_da.protect='+ls_protect)
		dw_selezione.modify('cod_prodotto_a.protect='+ls_protect)
		dw_selezione.modify('cod_comodo_2.protect='+ls_protect)
		dw_selezione.modify('cod_cat_mer.protect='+ls_protect)
		
		//ricordarsi di ablitare anche il campo per il tipo layout
		//dw_selezione.modify('flag_tipo_layout.protect='+ls_protect)
		
		//disabilita la selezione del fornitore
//		cb_fornitori_ricerca.enabled = false
		
end choose
end subroutine

public subroutine wf_giacenza (string as_cod_prodotto, ref decimal ad_quantita);string ls_where,ls_errore,ls_chiave[]
long ll_ret
dec{4} 				ld_giacenza, ld_quantita_giacenza[], ld_giacenza_stock[]
dec{4} 				ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime 			ldt_data_riferimento
uo_magazzino 	luo_magazzino


ldt_data_riferimento = datetime(today(),00:00:00)

ls_where = ""

for ll_ret = 1 to 14
	ld_quantita_giacenza[ll_ret] = 0
next	

luo_magazzino = CREATE uo_magazzino

ll_ret = luo_magazzino.uof_saldo_prod_date_decimal(as_cod_prodotto, ldt_data_riferimento, ls_where, ld_quantita_giacenza, ls_errore, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])

destroy luo_magazzino	

//for ll_ret = 1 to upperbound(ls_chiave)
//	if ls_chiave[ll_ret] = is_cod_deposito then
//		ld_giacenza = ld_giacenza_stock[ll_ret]
//	end if
//next
ld_giacenza = ld_quantita_giacenza[1] + ld_quantita_giacenza[4] - ld_quantita_giacenza[6]

if isnull(ld_giacenza) then ld_giacenza = 0

ad_quantita = ld_giacenza //+ ad_quantita

destroy luo_magazzino


//ll_ret = dw_analisi_fabbisogni_documenti.insertrow(1)
//dw_analisi_fabbisogni_documenti.setitem(ll_ret, "deposito", is_cod_deposito)
//dw_analisi_fabbisogni_documenti.setitem(ll_ret, "data_riferimento", datetime(today(),00:00:00))
//dw_analisi_fabbisogni_documenti.setitem(ll_ret, "expression1", "ANAG_PRODOTTI")
//dw_analisi_fabbisogni_documenti.setitem(ll_ret, "quantita", ld_quantita)
//dw_analisi_fabbisogni_documenti.resetupdate()

//dw_analisi_fabbisogni_documenti.Object.data_riferimento.Color = "0~tif( DATE(data_riferimento) < today(), rgb(255,0,0), if( DATE(data_riferimento) > today(), rgb(0,0,255), rgb(0,0,0))    )"

end subroutine

public subroutine wf_totale_venduto (datetime adt_data_inizio, datetime adt_data_fine, string as_cod_prodotto, long al_row);dec{4}			ld_tot_venduto


ld_tot_venduto = 0
adt_data_inizio = datetime(date(adt_data_inizio), 00:00:00)
adt_data_fine = datetime(date(adt_data_fine), 23:59:59)

if as_cod_prodotto<>"" and not isnull(as_cod_prodotto) then

	select sum(m.quan_movimento) 
	into :ld_tot_venduto
	from det_fat_ven as d
	join mov_magazzino as m on	m.cod_azienda=d.cod_azienda and
								m.anno_registrazione=d.anno_registrazione_mov_mag and
								m.num_registrazione=d.num_registrazione_mov_mag
	where 	d.cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione_mov_mag>0 and
				num_registrazione_mov_mag>0 and
				m.data_registrazione >= :adt_data_inizio and 
				m.data_registrazione <= :adt_data_fine and
				m.cod_prodotto=:as_cod_prodotto;

end if

try
	dw_report.setitem(al_row, "rd_tot_venduto", ld_tot_venduto)
catch (throwable err)
end try



return
end subroutine

public subroutine wf_totale_consumato (datetime adt_data_inizio, datetime adt_data_fine, string as_cod_prodotto, long al_row);dec{4}			ld_tot_consumato

ld_tot_consumato = 0
adt_data_inizio = datetime(date(adt_data_inizio), 00:00:00)
adt_data_fine = datetime(date(adt_data_fine), 00:00:00)

if as_cod_prodotto<>"" and not isnull(as_cod_prodotto) then
	
select sum(mov_magazzino.quan_movimento)
	into :ld_tot_consumato
 from mov_magazzino 
 	join tab_tipi_movimenti on tab_tipi_movimenti.cod_azienda = mov_magazzino.cod_azienda and tab_tipi_movimenti.cod_tipo_movimento = mov_magazzino.cod_tipo_movimento 
	 join tab_tipi_movimenti_det on tab_tipi_movimenti_det.cod_azienda = mov_magazzino.cod_azienda and tab_tipi_movimenti_det.cod_tipo_mov_det = mov_magazzino.cod_tipo_mov_det 
	 join det_tipi_movimenti on tab_tipi_movimenti_det.cod_azienda = det_tipi_movimenti.cod_azienda and tab_tipi_movimenti_det.cod_tipo_mov_det = det_tipi_movimenti.cod_tipo_mov_det and tab_tipi_movimenti.cod_tipo_movimento = det_tipi_movimenti.cod_tipo_movimento
 where	mov_magazzino.cod_azienda = :s_cs_xx.cod_azienda and 
 			mov_magazzino.cod_prodotto = :as_cod_prodotto  and 
			det_tipi_movimenti.flag_consumato ='S' and
			mov_magazzino.data_registrazione >= :adt_data_inizio and 
			mov_magazzino.data_registrazione <= :adt_data_fine;
	
/*
	select sum(A.quan_movimento) 
	into :ld_tot_consumato
	from mov_magazzino A
	join tab_tipi_movimenti_det B on	A.cod_azienda=B.cod_azienda and A.cod_tipo_mov_det = B.cod_tipo_mov_det
	where 	A.cod_azienda=:s_cs_xx.cod_azienda and
				A.data_registrazione >= :adt_data_inizio and 
				A.data_registrazione <= :adt_data_fine and
				A.cod_prodotto=:as_cod_prodotto and
				(B.flag_prog_quan_uscita <> "=" or B.flag_prog_quan_venduta <> "=");
*/
end if

try
	dw_report.setitem(al_row, "rd_tot_consumato", ld_tot_consumato)
catch (throwable err)
end try



return
end subroutine

public subroutine wf_totale_caricato (datetime adt_data_inizio, datetime adt_data_fine, string as_cod_prodotto, long al_row);dec{4}			ld_tot_caricato

ld_tot_caricato = 0
adt_data_inizio = datetime(date(adt_data_inizio), 00:00:00)
adt_data_fine = datetime(date(adt_data_fine), 00:00:00)

if as_cod_prodotto<>"" and not isnull(as_cod_prodotto) then
	
select sum(mov_magazzino.quan_movimento)
	into :ld_tot_caricato
 from mov_magazzino 
 	join tab_tipi_movimenti on tab_tipi_movimenti.cod_azienda = mov_magazzino.cod_azienda and tab_tipi_movimenti.cod_tipo_movimento = mov_magazzino.cod_tipo_movimento 
	 join tab_tipi_movimenti_det on tab_tipi_movimenti_det.cod_azienda = mov_magazzino.cod_azienda and tab_tipi_movimenti_det.cod_tipo_mov_det = mov_magazzino.cod_tipo_mov_det 
 where	mov_magazzino.cod_azienda = :s_cs_xx.cod_azienda and 
 			mov_magazzino.cod_prodotto = :as_cod_prodotto  and 
			mov_magazzino.data_registrazione >= :adt_data_inizio and 
			mov_magazzino.data_registrazione <= :adt_data_fine and
			tab_tipi_movimenti_det.flag_prog_quan_acquistata <> '=' ;
end if

try
	dw_report.setitem(al_row, "rd_tot_caricato", ld_tot_caricato)
catch (throwable err)
end try



return
end subroutine

on w_report_stato_magazzino.create
int iCurrent
call super::create
this.dw_depositi_giacenza=create dw_depositi_giacenza
this.cb_filtro_predefinito=create cb_filtro_predefinito
this.cb_memo_filtro=create cb_memo_filtro
this.st_2=create st_2
this.cb_gen_rda=create cb_gen_rda
this.st_1=create st_1
this.hpb_1=create hpb_1
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_folder=create dw_folder
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_depositi_giacenza
this.Control[iCurrent+2]=this.cb_filtro_predefinito
this.Control[iCurrent+3]=this.cb_memo_filtro
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.cb_gen_rda
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.hpb_1
this.Control[iCurrent+8]=this.cb_annulla
this.Control[iCurrent+9]=this.cb_report
this.Control[iCurrent+10]=this.dw_folder
this.Control[iCurrent+11]=this.dw_selezione
this.Control[iCurrent+12]=this.dw_report
end on

on w_report_stato_magazzino.destroy
call super::destroy
destroy(this.dw_depositi_giacenza)
destroy(this.cb_filtro_predefinito)
destroy(this.cb_memo_filtro)
destroy(this.st_2)
destroy(this.cb_gen_rda)
destroy(this.st_1)
destroy(this.hpb_1)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_folder)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_cat_mer", &
							sqlca, &
							"tab_cat_mer", &
							"cod_cat_mer", &
                     "des_cat_mer", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

/*
f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_deposito", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
                     "des_deposito", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
*/

f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_reparto", &
							sqlca, &
							"anag_reparti", &
							"cod_reparto", &
                     "des_reparto", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_nomenclatura", &
							sqlca, &
							"tab_nomenclature", &
							"cod_nomenclatura", &
                     "des_nomenclatura", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_tipo_politica_riordino", &
							sqlca, &
							"tab_tipi_politiche_riordino", &
							"cod_tipo_politica_riordino", &
                     "des_tipo_politica_riordino", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;string ls_classname
windowobject l_objects[]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
iuo_dw_main = dw_report
//

l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = cb_annulla
l_objects[4] = st_1
l_objects[5] = st_2
l_objects[6] = cb_gen_rda
l_objects[7] = cb_memo_filtro
l_objects[8] = cb_filtro_predefinito
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

move(0,0)

if isvalid(message.powerobjectparm) then
	ls_classname = classname(message.powerobjectparm)
	ls_classname = message.powerobjectparm.classname( )
	dw_selezione.postevent("ue_imposta_filtro")
end if

resize(w_cs_xx_mdi.mdi_1.width - 50,w_cs_xx_mdi.mdi_1.height - 50)

cb_filtro_predefinito.postevent("clicked")

dw_depositi_giacenza.visible = false
dw_depositi_giacenza.uof_set_parent_dw( dw_selezione, "cod_deposito", "cod_deposito" )
dw_depositi_giacenza.uof_set_column("cod_deposito, des_deposito", "anag_depositi", &
										"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))", &
										"des_deposito")


end event

event resize;call super::resize;dw_folder.width = newwidth - 20
dw_folder.height = newheight - 20
dw_report.width = newwidth - 120
dw_report.height = newheight - 170 
end event

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = dw_selezione
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type dw_depositi_giacenza from uo_dddw_checkbox within w_report_stato_magazzino
boolean visible = false
integer x = 942
integer y = 1336
integer width = 2386
integer taborder = 110
end type

type cb_filtro_predefinito from commandbutton within w_report_stato_magazzino
integer x = 1934
integer y = 136
integer width = 379
integer height = 80
integer taborder = 100
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Filtro Predef."
end type

event clicked;wf_leggi_filtro()

dw_selezione.postevent("ue_reset")
end event

type cb_memo_filtro from commandbutton within w_report_stato_magazzino
integer x = 1531
integer y = 136
integer width = 379
integer height = 80
integer taborder = 100
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Memo Filtro"
end type

event clicked;dw_selezione.accepttext()
wf_memorizza_filtro()
end event

type st_2 from statictext within w_report_stato_magazzino
integer x = 59
integer y = 1912
integer width = 2528
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "DOPO AVER ELABORATO IL REPORT E~' POSSIBILE GENERARE LE RDA PER I PRODOTTI MANCANTI."
boolean focusrectangle = false
end type

type cb_gen_rda from commandbutton within w_report_stato_magazzino
integer x = 55
integer y = 1988
integer width = 379
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen. R.D.A."
end type

event clicked;string						ls_cod_prodotto, ls_cod_versione,ls_cod_tipo_politica_riordino, ls_errore, ls_note, &
							ls_flag_gen_commessa, ls_des_prodotto, ls_flag_tipo_riga
long						ll_i, ll_anno_commessa,ll_num_commessa, ll_ret, ll_anno_reg_rda, ll_num_reg_rda
dec{4}					ldd_quan_rda, ld_disponibilita, ld_scorta
datetime					ldt_data_consegna

uo_generazione_documenti				luo_gen_doc
										

if g_mb.messagebox("SEP","Procedo con la creazione RDA dei prodotti indicati com mancanti ?",Question!,YesNo!,2) = 2 then return

luo_gen_doc = create uo_generazione_documenti 

setnull(ll_anno_reg_rda)
setnull(ll_num_reg_rda)

setnull(ll_anno_commessa)
setnull(ll_num_commessa)

for ll_i = 1 to dw_report.rowcount()
	
	ls_cod_prodotto  = dw_report.getitemstring(ll_i, "rs_cod_prodotto")
	ls_des_prodotto  = dw_report.getitemstring(ll_i, "rs_descrizione")
	
	ld_disponibilita = dw_report.getitemnumber(ll_i, "rd_disponibilita")
	ld_scorta        = dw_report.getitemnumber(ll_i, "rd_scorta_minima")
	
	if ld_scorta <= ld_disponibilita then continue

	ldd_quan_rda = ld_scorta - ld_disponibilita
	
	setnull(ldt_data_consegna)
	
	select cod_tipo_politica_riordino
	into   :ls_cod_tipo_politica_riordino
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SEP","Errore in ricerca prodotto in anagrafica prodotti")
		rollback;
		return
	end if
	
	ls_flag_gen_commessa = "N"
	
	ll_ret = luo_gen_doc.uof_genera_rda(ls_cod_prodotto, &
	 											   ls_des_prodotto, &
													ls_cod_versione, &
													ldd_quan_rda, &
													ll_anno_commessa, &
													ll_num_commessa, &
													ldt_data_consegna, &
													ls_cod_tipo_politica_riordino, &
													ls_flag_gen_commessa, &
													ls_note, &
													ref ll_anno_reg_rda, &
													ref ll_num_reg_rda, &
													ref ls_errore)
	if ll_ret < 0 then
		g_mb.messagebox("SEP","Errore durante generazione RDA.~r~n" +ls_errore,stopsign!)
		rollback;
		exit
	end if
next
destroy luo_gen_doc

commit;

return
end event

type st_1 from statictext within w_report_stato_magazzino
integer x = 2094
integer y = 1836
integer width = 347
integer height = 72
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_report_stato_magazzino
integer x = 55
integer y = 1836
integer width = 1257
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 10
end type

type cb_annulla from commandbutton within w_report_stato_magazzino
integer x = 1326
integer y = 1832
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string		ls_null
datetime		ldt_null

setnull(ls_null)
setnull(ldt_null)

dw_selezione.setitem(dw_selezione.getrow(),"cod_prodotto_da", ls_null)
dw_selezione.setitem(dw_selezione.getrow(),"cod_prodotto_a", ls_null)
dw_selezione.setitem(dw_selezione.getrow(),"cod_cat_mer", ls_null)
dw_selezione.setitem(dw_selezione.getrow(),"cod_deposito", ls_null)
dw_selezione.setitem(dw_selezione.getrow(),"flag_mancanti", 'N')
dw_selezione.setitem(dw_selezione.getrow(),"cod_comodo_2", ls_null)
dw_selezione.setitem(dw_selezione.getrow(),"flag_mp_commessa", 'N')
dw_selezione.setitem(dw_selezione.getrow(),"cod_tipo_politica_riordino", ls_null)
dw_selezione.setitem(dw_selezione.getrow(),"cod_reparto", ls_null)
dw_selezione.setitem(dw_selezione.getrow(),"cod_nomenclatura", ls_null)
dw_selezione.setitem(dw_selezione.getrow(),"cod_fornitore", ls_null)
dw_selezione.setitem(dw_selezione.getrow(),"flag_tipo_layout", ls_null)

dw_selezione.setitem(dw_selezione.getrow(),"data_riferimento", ldt_null)
dw_selezione.setitem(dw_selezione.getrow(),"data_consegna_inizio", ldt_null)
dw_selezione.setitem(dw_selezione.getrow(),"data_consegna_fine", ldt_null)

end event

type cb_report from commandbutton within w_report_stato_magazzino
integer x = 1714
integer y = 1832
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;boolean lb_presenza_ord_acq_successivi=false

string ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, ls_flag_consegna_sel, &
		 ls_flag_mancanti_sel, ls_data_riferimento_sel, ls_data_inizio, ls_des_prodotto_sel, ls_where, ls_error, &
		 ls_chiave[], ls_cod_prodotto, ls_des_azienda, ls_intestazione, ls_cod_deposito, ls_vettore_nullo[], &
		 ls_cod_deposito_prodotto, ls_des_deposito, ls_des_impegnato, ls_errore, ls_cod_comodo_2, &
		 ls_des_filtro_report, ls_flag_mp_commesse,ls_data_fine, ls_flag_tipo_layout,ls_cod_fornitore, ls_fornitore, &
		 ls_rag_soc_1, ls_cod_tipo_politica_riordino, ls_cod_tipo_politica_riordino_sel, ls_cod_nomenclatura_sel, &
		 ls_cod_depositi[], ls_cod_deposito_2, ls_flag_blocco

datetime ldt_data_riferimento_sel_2, ldt_data_chiusura_annuale, ldt_data_riferimento_sel, ldt_data_comm_inizio, ldt_data_comm_fine, &
         ldt_data_fine, ldt_data_sottoscorta, ldt_data_consegna,ldt_data_vuota, ldt_data_venduto_fine,ldt_data_venduto_inizio, &
		 ldt_data_consumato_inizio, ldt_data_consumato_fine, ldt_data_caricato_inizio, ldt_data_caricato_fine

integer li_perc

long ll_i, ll_ret, ll_commesse, li_cont, ll_num_stock, ll_j, ll_riga, ll_num_rows, ll_anno_reg_ord_acq, ll_num_reg_ord_acq

dec{4}	ld_quan_venduta, ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[], ld_vettore_nullo[], &
			ld_scorta_minima,ld_quan_richiesta,ld_quan_in_produzione,ld_impegnato_comm, ld_impegnato_ord_ven, ld_giacenza_prodotto, &
			ld_giacenza, ld_impegnato, ld_ordinato, ld_commesse, ld_scorta, ld_disponibilita, ld_quan_riordino, ld_quan_necessaria, ld_giacenza_2

uo_magazzino luo_magazzino
uo_situazione_magazzino iuo_situazione_magazzino
uo_mrp luo_mrp
str_mrp lstr_mrp[], lstr_vuoto[]

declare  cu_ord_acq cursor for
select   tes_ord_acq.anno_registrazione, tes_ord_acq.num_registrazione, cod_fornitore
from     tes_ord_acq LEFT OUTER JOIN det_ord_acq ON tes_ord_acq.cod_azienda = det_ord_acq.cod_azienda AND tes_ord_acq.anno_registrazione = det_ord_acq.anno_registrazione AND tes_ord_acq.num_registrazione = det_ord_acq.num_registrazione  
where    tes_ord_acq.cod_azienda  = :s_cs_xx.cod_azienda  and
			det_ord_acq.cod_prodotto = :ls_cod_prodotto and
			det_ord_acq.flag_blocco  = 'N' and
			det_ord_acq.data_consegna >= :ldt_data_consegna
order by tes_ord_acq.anno_registrazione DESC, 
			tes_ord_acq.num_registrazione DESC;

SetPointer(HourGlass!)

ldt_data_fine = datetime(date("31/12/2099"),00:00:00)
ldt_data_vuota = datetime(date("01/01/1900"),00:00:00)				


hpb_1.setrange(0,100)
hpb_1.position = 0
Yield()

ids_situazione_mag = create datastore
ids_situazione_mag.DataObject = 'd_ds_report_stato_magazzino'
ids_situazione_mag.SetTransObject (sqlca)

dw_report.reset() 

dw_report.setredraw(false)
dw_selezione.accepttext()
ldt_data_riferimento_sel = dw_selezione.getitemdatetime(1, "data_riferimento")

if isnull(ldt_data_riferimento_sel) or year(date(ldt_data_riferimento_sel)) < 1950 then
	g_mb.warning("Indicare la data di riferimento!")
	return
end if

ls_cod_prodotto_da_sel = dw_selezione.getitemstring(1, "cod_prodotto_da")
ls_cod_prodotto_a_sel = dw_selezione.getitemstring(1, "cod_prodotto_a")
dw_depositi_giacenza.uof_get_selected_column_as_array( "cod_deposito", ls_cod_depositi[])

choose case upperbound(ls_cod_depositi[]) 
	case 1
		ls_cod_deposito = ls_cod_depositi[1]
		setnull(ls_cod_deposito_2)
	case 2
		ls_cod_deposito = ls_cod_depositi[1]
		ls_cod_deposito_2 = ls_cod_depositi[2]
	case is < 1
		setnull(ls_cod_deposito)
		setnull(ls_cod_deposito_2)
	case is > 2
	g_mb.warning("E' possibile ottenere la giacenza al massimo per 2 depositi!")
	return
end choose


ls_cod_nomenclatura_sel = dw_selezione.getitemstring( 1, "cod_nomenclatura")

ls_cod_cat_merce_sel = dw_selezione.getitemstring(1, "cod_cat_mer")
ls_flag_mancanti_sel = dw_selezione.getitemstring(1, "flag_mancanti")
ls_cod_comodo_2 = dw_selezione.getitemstring( 1, "cod_comodo_2")
ls_flag_mp_commesse = dw_selezione.getitemstring( 1, "flag_mp_commessa")
ldt_data_comm_inizio = dw_selezione.getitemdatetime( 1, "data_consegna_inizio")
ldt_data_comm_fine = dw_selezione.getitemdatetime( 1, "data_consegna_fine")
ls_flag_tipo_layout = dw_selezione.getitemstring( 1, "flag_tipo_layout")
ls_flag_blocco = dw_selezione.getitemstring( 1, "flag_blocco")

choose case ls_flag_tipo_layout
	case "1"
		dw_report.dataobject = 'd_report_stato_magazzino'
		ldt_data_venduto_inizio = dw_selezione.getitemdatetime(1,"data_venduto_inizio")
		ldt_data_venduto_fine = dw_selezione.getitemdatetime(1,"data_venduto_fine")
		ldt_data_consumato_inizio = dw_selezione.getitemdatetime(1,"data_consumato_inizio")
		ldt_data_consumato_fine = dw_selezione.getitemdatetime(1,"data_consumato_fine")
		ldt_data_caricato_inizio = dw_selezione.getitemdatetime(1,"data_caricato_inizio")
		ldt_data_caricato_fine = dw_selezione.getitemdatetime(1,"data_caricato_fine")
		
	case "2"
		dw_report.dataobject = 'd_report_stato_magazzino_layout2'
		
	case "3"
		if upperbound(ls_cod_depositi[]) > 1 then
			g_mb.warning("Il layout tipo 3 può essere eseguito solo per 1 deposito!")
			return
		end if
		dw_report.dataobject = 'd_report_stato_magazzino_layout3'
		
end choose

ldt_data_riferimento_sel_2 = datetime(date(ldt_data_riferimento_sel), 00:00:00)

ls_des_filtro_report = "Data Riferimento: " + string(ldt_data_riferimento_sel, "dd/mm/yyyy")
if not isnull(ls_cod_prodotto_da_sel) then
	ls_des_filtro_report += " - Dal prodotto:" + ls_cod_prodotto_da_sel
else
	ls_des_filtro_report += " - Dal primo prodotto "
end if	

if not isnull(ls_cod_prodotto_da_sel) then
	ls_des_filtro_report += " - Al prodotto:" + ls_cod_prodotto_da_sel
else
	ls_des_filtro_report += " - All'ultimo prodotto "
end if	

if not isnull(ls_cod_nomenclatura_sel) and ls_cod_nomenclatura_sel<>"" then
	ls_des_filtro_report += " - Nomenclatura Articolo:" + ls_cod_nomenclatura_sel
end if

if not isnull(ls_cod_cat_merce_sel) then
	ls_des_filtro_report += " - Categoria=" + ls_cod_cat_merce_sel
end if	

if not isnull(ls_cod_comodo_2) then
	ls_des_filtro_report += " - Cod Comodo 2= " + ls_cod_comodo_2
end if	

dw_depositi_giacenza.uof_get_selected_column_as_array( "cod_deposito", ls_cod_depositi[])

choose case upperbound(ls_cod_depositi[]) 
	case 1
		ls_des_filtro_report += " - Deposito=" + ls_cod_deposito
		dw_report.object.rd_giacenza_dep_1_t.text = "Giacenza~n" + ls_cod_deposito
		dw_report.object.rd_giacenza_dep_2_t.visible=0
		dw_report.object.rd_giacenza_2.visible=0
	case 2
		ls_des_filtro_report += " - Deposito Filtro=" + ls_cod_deposito + " - Deposito Confronto= " + ls_cod_deposito_2
		dw_report.object.rd_giacenza_dep_1_t.text = "Giacenza~n" + ls_cod_deposito
		if ls_flag_tipo_layout = "1" then
			dw_report.object.rd_giacenza_dep_2_t.text = "Giacenza~n" + ls_cod_deposito_2
			dw_report.object.rd_giacenza_dep_2_t.visible=1
			dw_report.object.rd_giacenza_2.visible=1
		else
			dw_report.object.rd_giacenza_dep_2_t.visible=0
			dw_report.object.rd_giacenza_2.visible=0
		end if
	case is < 1
		ls_des_filtro_report += " - Deposito=TUTTI "
		dw_report.object.rd_giacenza_dep_2_t.visible=0
		dw_report.object.rd_giacenza_2.visible=0
end choose

if ls_flag_mancanti_sel = "S" then
	ls_des_filtro_report += " - SOLO PRODOTTI MANCANTI "
end if

dw_report.object.filtri_report_t.text = ls_des_filtro_report


// *** CONTROLLO DATA

select data_chiusura_annuale
into   :ldt_data_chiusura_annuale
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

if ldt_data_riferimento_sel < ldt_data_chiusura_annuale then
	g_mb.messagebox("Inventario Magazzino", "La data di inventario deve essere posteriore alla data della chiusura annuale")
	return -1
end if

select rag_soc_1
into   :ls_des_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

if not isnull(ls_des_azienda) then ls_intestazione = ls_des_azienda

if isnull(ls_flag_mancanti_sel) or ls_flag_mancanti_sel = "" then ls_flag_mancanti_sel = "N"


if isnull(ls_cod_cat_merce_sel) then
	ls_cod_cat_merce_sel = "%"
end if	

ls_data_inizio = string(today(), s_cs_xx.db_funzioni.formato_data)
ls_data_riferimento_sel = string(ldt_data_riferimento_sel, s_cs_xx.db_funzioni.formato_data)
ls_data_fine = string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data)

iuo_situazione_magazzino = create uo_situazione_magazzino
iuo_situazione_magazzino.is_prodotti_bloccati = ls_flag_blocco

if not isnull(ls_cod_nomenclatura_sel) and ls_cod_nomenclatura_sel<>"" then iuo_situazione_magazzino.uof_set_nomenclatura(ls_cod_nomenclatura_sel)

if not isnull(ls_cod_deposito) and len(ls_cod_deposito) > 0 then
	iuo_situazione_magazzino.is_cod_deposito_filtro = ls_cod_deposito
end if

if ls_flag_mp_commesse = "S" then
	iuo_situazione_magazzino.ib_filtro_commesse=true
	iuo_situazione_magazzino.idt_data_filtro_commessa_inizio = ldt_data_comm_inizio
	iuo_situazione_magazzino.idt_data_filtro_commessa_fine   = ldt_data_comm_fine
end if

// Ciclo Ordini Vendita
if iuo_situazione_magazzino.wf_ordini_vendita(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, "N", ls_data_inizio, ls_data_fine, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo ordini di vendita")	
	return
end if
hpb_1.position = 10
Yield()

// Ciclo Bolle Vendita
if iuo_situazione_magazzino.wf_bolle_vendita(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, "N", ls_data_inizio, ls_data_fine, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo bolle di vendita")	
	return
end if
hpb_1.position = 20
Yield()

// Ciclo Fatture Vendita
if iuo_situazione_magazzino.wf_fatture_vendita(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, "N", ls_data_inizio, ls_data_fine, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo fatture di vendita")	
	return
end if
hpb_1.position = 30
Yield()

// Ciclo Ordini Acquisto
if iuo_situazione_magazzino.wf_ordini_acquisto(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, "N", ls_data_inizio, ls_data_fine, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo ordini di acquisto")	
	return
end if
hpb_1.position = 40
Yield()

// Ciclo Commesse
if iuo_situazione_magazzino.wf_commesse(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, "N", ls_data_inizio, ls_data_fine, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo commesse")	
	return
end if
hpb_1.position = 50
Yield()

// Ciclo Generale Prodotti"
if iuo_situazione_magazzino.wf_prodotti_generale(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo generico prodotti")	
	return
end if	

destroy iuo_situazione_magazzino

ll_num_rows = ids_situazione_mag.RowCount()

for ll_i = 1 to ll_num_rows
	
	st_1.text = string(ll_i) + "/" + string(ll_num_rows)
	
	li_perc = round((ll_i * 50) / ll_num_rows,0)
	hpb_1.position = 50 + li_perc
	Yield()
	
	if ls_flag_tipo_layout = "1" or ls_flag_tipo_layout = "2" then
		// aggiunto nuovo layout tipo 3: vedi specifica report_situazione_mag_modifica 29/05/2008
	
		ll_riga = dw_report.insertrow(0)		
		dw_report.setitem(ll_riga, "rs_cod_prodotto", ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com"))
		dw_report.setitem(ll_riga, "rs_descrizione", ids_situazione_mag.getitemstring(ll_i, "des_prodotto_com"))
	
		//dw_report.setitem(ll_riga, "rd_giacenza", ids_situazione_mag.getitemdecimal(ll_i, "giacenza_com"))
		dw_report.setitem(ll_riga, "flag_tipo_riga", 1)
		// *** impegnato
		
		ls_cod_prodotto = ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com")
		
		ls_cod_prodotto = ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com")
		
		select scorta_minima
		into   :ld_scorta_minima
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
	
		if sqlca.sqlcode = 100 then
			g_mb.messagebox( "APICE", "Prodotto " + ls_cod_prodotto + " inesistente", stopsign!)
			return -1
		end if
	
		if sqlca.sqlcode = -1 then
			g_mb.messagebox( "APICE", "Errore in ricerca prodotto " + ls_cod_prodotto + ":" + sqlca.sqlerrtext, stopsign!)
			return -1
		end if
	
		if isnull(ld_scorta_minima) then ld_scorta_minima = 0
	
		dw_report.setitem(ll_riga, "rd_scorta_minima", ld_scorta_minima)
		
		// sommatoria delle richieste di acquisto non evase
		
		ld_quan_richiesta = 0
		
		select sum(quan_richiesta)
		into   :ld_quan_richiesta	
		from   det_rda
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto and
				flag_gen_ord_acq = 'N' and 
				flag_gen_commessa = 'N';		
				
		if isnull(ld_quan_richiesta) then ld_quan_richiesta = 0		
		
		dw_report.setitem(ll_riga, "rd_quan_rda", ld_quan_richiesta	)
		
		
	// sommatoria quantità in ordine
	//		ld_impegnato_ord_ven = 0
			
		SELECT sum( isnull(quan_ordine,0) - isnull(quan_evasa,0) )  
		INTO   :ld_impegnato_ord_ven
		FROM   det_ord_ven LEFT OUTER JOIN tab_tipi_det_ven ON det_ord_ven.cod_azienda = tab_tipi_det_ven.cod_azienda AND det_ord_ven.cod_tipo_det_ven = tab_tipi_det_ven.cod_tipo_det_ven 
								LEFT OUTER JOIN tes_ord_ven      ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda AND det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione AND det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione  
		WHERE  ( tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda ) AND  
				( tab_tipi_det_ven.flag_tipo_det_ven = 'M' ) AND  
				( det_ord_ven.cod_prodotto = :ls_cod_prodotto ) AND  
				( det_ord_ven.flag_evasione <> 'E' ) AND  
				( det_ord_ven.flag_blocco = 'N' ) AND  
				( tes_ord_ven.flag_blocco = 'N' )    ;
			
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "APICE", "Errore durante lettura impegnato da ordine:" + sqlca.sqlerrtext, stopsign!)
			return -1
		end if
	
		SELECT  sum(quan_impegnata_attuale)
		into    :ld_impegnato_comm
		FROM    impegno_mat_prime_commessa left outer join  anag_commesse  on anag_commesse.cod_azienda = impegno_mat_prime_commessa.cod_azienda and 
																									 anag_commesse.anno_commessa = impegno_mat_prime_commessa.anno_commessa and 
																									 anag_commesse.num_commessa = impegno_mat_prime_commessa.num_commessa 
		WHERE   impegno_mat_prime_commessa.cod_azienda = :s_cs_xx.cod_azienda and
				  impegno_mat_prime_commessa.cod_prodotto = :ls_cod_prodotto and 
				  anag_commesse.flag_tipo_avanzamento in ('0' ,'2', '3');
	
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "APICE", "Errore durante lettura impegnato da commesse:" + sqlca.sqlerrtext, stopsign!)
			return -1
		end if					  
		
		if isnull(ld_impegnato_ord_ven) then ld_impegnato_ord_ven = 0
		if isnull(ld_impegnato_comm) then ld_impegnato_comm = 0
		
		ld_impegnato_comm += ld_impegnato_ord_ven		
		
		dw_report.setitem(ll_riga, "rd_impegnato", ld_impegnato_comm)
		
		dw_report.setitem(ll_riga, "rd_disponibilita", ids_situazione_mag.getitemdecimal(ll_i, "disp_reale_com"))
		dw_report.setitem(ll_riga, "rd_ordinato", ids_situazione_mag.getitemdecimal(ll_i, "ord_a_fornitore_com"))	
		
		// *** commesse in corso
		
		select sum(quan_in_produzione - quan_prodotta)
		into   :ld_quan_in_produzione
		from   anag_commesse
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto and
				 flag_blocco = 'N' and
				 flag_tipo_avanzamento <> '9' and
				 flag_tipo_avanzamento <> '8' and
				 flag_tipo_avanzamento <> '7';
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "APICE", "Errore durante lettura numero commesse in corso:" + sqlca.sqlerrtext, stopsign!)
			return -1
		end if		
		
		if isnull(ld_quan_in_produzione) then ld_quan_in_produzione = 0		
		
		dw_report.setitem(ll_riga, "rd_commesse", ld_quan_in_produzione)
		
		// *** giacenza 
		
		for li_cont = 1 to 14
			ld_quant_val[li_cont] = 0
		next
	
		ls_chiave = ls_vettore_nullo
		ld_giacenza_stock = ld_vettore_nullo
		
		luo_magazzino = CREATE uo_magazzino
			
		ll_ret = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_riferimento_sel, ls_where, ld_quant_val, ls_error, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
	
		destroy luo_magazzino
		
		if ll_ret < 0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
			return -1
		end if
		
		ll_num_stock = upperbound(ls_chiave[])
		
		// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
		// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
		// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven

		if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
			ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
			dw_report.setitem(ll_riga, "rd_giacenza", ld_giacenza)
//			dw_report.setitem(ll_riga, "rd_giacenza", ids_situazione_mag.getitemdecimal(ll_i, "giacenza_com"))
		else
			for ll_j = 1 to ll_num_stock
				if ls_chiave[ll_j] = ls_cod_deposito then
					dw_report.setitem(ll_riga, "rd_giacenza", ld_giacenza_stock[ll_j])
				end if
			next
			if not isnull(ls_cod_deposito_2) then
				for ll_j = 1 to ll_num_stock
					if ls_chiave[ll_j] = ls_cod_deposito_2 then
						dw_report.setitem(ll_riga, "rd_giacenza_2", ld_giacenza_stock[ll_j])
					end if
				next
			end if
		end if
		
		ld_giacenza = dw_report.getitemnumber(ll_riga, "rd_giacenza")
		ld_giacenza_2 = dw_report.getitemnumber(ll_riga, "rd_giacenza_2")
		ld_impegnato = dw_report.getitemnumber(ll_riga, "rd_impegnato")
		ld_ordinato = dw_report.getitemnumber(ll_riga, "rd_ordinato")
		ld_commesse = dw_report.getitemnumber(ll_riga, "rd_commesse")
		ld_scorta = dw_report.getitemnumber(ll_riga, "rd_scorta_minima")
		
		if isnull(ls_cod_deposito_2) then
			ld_disponibilita = ld_giacenza - ld_impegnato + ld_ordinato + ld_commesse
		else
			ld_disponibilita = (ld_giacenza + ld_giacenza_2) - ld_impegnato + ld_ordinato + ld_commesse
		end if			
		dw_report.setitem(ll_riga, "rd_disponibilita", ld_disponibilita)
	
		//metto sul report il valore totale venduto negli utlimi 180 GG (circa 6 mesi)
		//prendo il totale dai movimenti legati a fattura
		if ls_flag_tipo_layout = "1" then
			wf_totale_venduto(ldt_data_venduto_inizio, ldt_data_venduto_fine, ls_cod_prodotto, ll_riga)
			wf_totale_consumato(ldt_data_consumato_inizio, ldt_data_consumato_fine, ls_cod_prodotto, ll_riga)
			wf_totale_caricato(ldt_data_caricato_inizio, ldt_data_caricato_fine, ls_cod_prodotto, ll_riga)
		end if
		
		
	
	//Donato 18-02-2009 aggiunto nuovo layout (SCORTA MASSIMA: 4)
	elseif ls_flag_tipo_layout = "3" then
	//else
		// report layout tipo 3; report MRP sintetico
		
		ls_cod_prodotto = ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com")
		
		//Donato 05/01/2009 acquisizione del tipo della politica di riordino del prodotto
		//C=Commessa interna, A=Acquisto, T=C/Terzi		
		select b.flag_tipo_riordino
		into :ls_cod_tipo_politica_riordino
		from anag_prodotti a		
		left outer join tab_tipi_politiche_riordino b
			on b.cod_azienda = a.cod_azienda and b.cod_tipo_politica_riordino = a.cod_tipo_politica_riordino
		where a.cod_azienda = :s_cs_xx.cod_azienda and a.cod_prodotto = :ls_cod_prodotto;
			
		if isnull(ls_cod_tipo_politica_riordino) then ls_cod_tipo_politica_riordino = ""
		
		//leggo il filtro impostato
		//T=tutte indifferentemente
		//A=solo quelle da Acquistare (quindi Acquisto e/o C/Terzi), C=solo quelle da produrre internamente
		ls_cod_tipo_politica_riordino_sel = dw_selezione.getitemstring(1, "flag_visualizza_tipo_riordino")
		
		if ls_cod_tipo_politica_riordino_sel<>"T" then			
			if ls_cod_tipo_politica_riordino_sel = "A" then
				//visualizzare solo i prodotti con tipo riordino pari a A o T
				if ls_cod_tipo_politica_riordino<>"A" and ls_cod_tipo_politica_riordino<>"T" then continue
			else
				//visualizzare solo i prodotti con tipo riordino pari a C
				if ls_cod_tipo_politica_riordino<>"C" then continue
			end if
		end if
		
		//fine modifica --------------------------------------------------------------------------
		
		select scorta_minima
		into   :ld_scorta_minima
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;

		if isnull(ld_scorta_minima) then ld_scorta_minima = 0
		
		luo_mrp = create uo_mrp
		lstr_mrp = lstr_vuoto
		
		ll_ret =luo_mrp.uof_mrp_prodotto(ls_cod_prodotto, &
													 ldt_data_riferimento_sel, &
													 ls_cod_deposito, &
													 ref lstr_mrp[], &
													 ref ls_errore )
		
		if ll_ret < 0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com"), ls_error)
			destroy luo_mrp
			return -1
		end if
		
		destroy luo_mrp				
		
		setnull(ldt_data_sottoscorta)
		ld_quan_riordino = 0
		ld_quan_necessaria = 0
					
		for ll_j = 1 to upperbound(lstr_mrp[])
			if lstr_mrp[ll_j].quan_progressiva < ld_scorta_minima then
				if isnull(ldt_data_sottoscorta) then
					ldt_data_sottoscorta = lstr_mrp[ll_j].data 
					ld_quan_riordino = abs(ld_scorta_minima - lstr_mrp[ll_j].quan_progressiva)
				else
					ld_quan_necessaria = abs(ld_scorta_minima - lstr_mrp[ll_j].quan_progressiva)
					if ld_quan_necessaria > ld_quan_riordino then
						ld_quan_riordino = ld_quan_necessaria
					end if
				end if
			end if
		next
		/* ora da qui procedo come segue:
		1) analizzo ogni lstr_mrp[]
		2) verifico se la quantità progressiva è minore del sottoscorta
		3) se è < sottoscorta, imposto la data sottoscorta nel report e calcolo la quantià necessaria
		4) altrimenti se è tutto OK, salto il prodotto
		*/
		
		if isnull(ldt_data_sottoscorta) then continue
		
		setnull(ls_fornitore)
		
		// cerco l'ultimo ordine di acquisto per questo prodotto
		ldt_data_consegna = ldt_data_vuota	
		
		open cu_ord_acq;
		
		fetch cu_ord_acq into :ll_anno_reg_ord_acq, :ll_num_reg_ord_acq, :ls_cod_fornitore;
		
		choose case sqlca.sqlcode
				
			case 100
				ls_fornitore = "NESSUN ORDINE TROVATO"
				
			case 0
				select rag_soc_1
				into   :ls_rag_soc_1
				from   anag_fornitori
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_fornitore = :ls_cod_fornitore;
				
				ls_fornitore = ls_cod_fornitore + " - " + ls_rag_soc_1 + " (ORD: "  + string(ll_anno_reg_ord_acq) + "/" + + string(ll_num_reg_ord_acq) + ")"
				
			case -1
				g_mb.messagebox("SEP", "Errore fetch cursore cu_ord_acq~r~n" + sqlca.sqlerrtext)
				return -1
				
		end choose
				
		
		close cu_ord_acq;
		
		// cerco eventuali ordini successivi alla data di sottoscorta
		ldt_data_consegna = ldt_data_vuota				
		lb_presenza_ord_acq_successivi = false
		
		if not isnull(ldt_data_sottoscorta) and ldt_data_sottoscorta > ldt_data_vuota then
			
			ldt_data_consegna = ldt_data_sottoscorta
			
			open cu_ord_acq;
			
			fetch cu_ord_acq into :ll_anno_reg_ord_acq, :ll_num_reg_ord_acq, :ls_cod_fornitore;
			
			choose case sqlca.sqlcode
					
				case 100
					//Donato 29/08/2008
					//se la fetch precedente (quella che riguarda l'ultimo ordine di acquisto per questo prodotto)
					//ha trovato un risultato non nullo allora propongo questo come ultimo ordine
					if ls_fornitore <> "NESSUN ORDINE TROVATO" then
						//non sovrascrivo quindi la variabile ls_fornitore
						//con la string "NESSUN ORDINE TROVATO"
					else						
						ls_fornitore = "NESSUN ORDINE TROVATO"
					end if
					
					//ls_fornitore = "NESSUN ORDINE TROVATO"
					//FINE MODIFICA ------------------------------------------------
					
				case 0
					lb_presenza_ord_acq_successivi = true	
					
				case -1
					g_mb.messagebox("SEP", "Errore fetch cursore cu_ord_acq~r~n" + sqlca.sqlerrtext)
					return -1
					
			end choose
			
			close cu_ord_acq;
		end if
		
		//Donato 05/01/2009 a seconda della politica di riordino associata al prodotto
		//							C (commessa interna)			proporre la scritta fissa "PRODUZIONE INTERNA"
		//							A o T (acquisti o C/Terzi)		proporre il fornitore (come fa adesso)
		//							<nessuna specificata>			proporre la colonna vuota
		choose case ls_cod_tipo_politica_riordino
			case "C"
				//"PRODUZIONE INTERNA"
				ls_fornitore = "PRODUZIONE INTERNA"				
				
			case "A", "T"
				//lascia così com'è
				
			case else
				//colonna vuota
				ls_fornitore = ""
		end choose
		//fine modifica --------------------------------------------------------------------			
		
		//Donato 05/01/2009 nel fabbisogno tenere conto anche della quan in produzione
		select quan_in_produzione
		into   :ld_quan_in_produzione
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "APICE", "Errore durante lettura della quan. in produzione:" + sqlca.sqlerrtext, stopsign!)
			return -1
		end if		
		
		if isnull(ld_quan_in_produzione) then ld_quan_in_produzione = 0	
		ld_quan_riordino -= ld_quan_in_produzione
		//fine modifica ---------------------------------------------------------------------
		
		if ld_quan_riordino > 0 then
			ll_riga = dw_report.insertrow(0)		
		
			dw_report.setitem(ll_riga, "flag_tipo_riga", 1)
			dw_report.setitem(ll_riga, "rs_cod_prodotto", ls_cod_prodotto)
			dw_report.setitem(ll_riga, "rs_descrizione", ids_situazione_mag.getitemstring(ll_i, "des_prodotto_com"))
			dw_report.setitem(ll_riga, "rd_quan_riordino", ld_quan_riordino)
			dw_report.setitem(ll_riga, "rdd_data_sottoscorta", ldt_data_sottoscorta)
			dw_report.setitem(ll_riga, "rs_fornitore", ls_fornitore)
			
			if lb_presenza_ord_acq_successivi then
				dw_report.setitem(ll_riga, "rs_flag_presenza_ord_acq", "S")
			else
				dw_report.setitem(ll_riga, "rs_flag_presenza_ord_acq", "N")
			end if
		end if			
	end if

next	


dw_report.object.intestazione.text = ls_intestazione
dw_report.setredraw(true)

dw_report.Object.DataWindow.Print.Preview	='Yes'
dw_report.Object.DataWindow.Print.Preview.rulers	='Yes'

dw_report.SetSort("rs_cod_prodotto A, flag_tipo_riga A")

dw_report.Sort()

dw_folder.fu_selecttab(2)
dw_report.Change_DW_Current()

destroy ids_situazione_mag

//Donato 18-02-2009 aggiunto nuovo layout "SCORTA MASSIMA": 4
//if ls_flag_mancanti_sel = "S" and ls_flag_tipo_layout <> "3" then
if ls_flag_mancanti_sel = "S" and (ls_flag_tipo_layout = "1" or ls_flag_tipo_layout = "2") then
	
	dw_report.setfilter("compute_6 = 'SI'")
	dw_report.filter()
end if

dw_report.setredraw(true)

SetPointer(Arrow!)

end event

type dw_folder from u_folder within w_report_stato_magazzino
integer x = 9
integer y = 12
integer width = 3694
integer height = 2080
integer taborder = 90
boolean border = false
end type

type dw_selezione from uo_cs_xx_dw within w_report_stato_magazzino
event ue_imposta_filtro ( )
event ue_reset ( )
integer x = 46
integer y = 124
integer width = 3625
integer height = 1700
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_sel_report_stato_magazzino"
boolean border = false
end type

event ue_imposta_filtro();s_report_stato_magazzino ls_report_stato_magazzino

ls_report_stato_magazzino = message.powerobjectparm

setitem(1,"cod_prodotto_da", ls_report_stato_magazzino.cod_prodotto)
setitem(1,"cod_prodotto_a", ls_report_stato_magazzino.cod_prodotto)

cb_report.postevent("clicked")
end event

event ue_reset();dw_selezione.resetupdate()
end event

event itemchanged;call super::itemchanged;string ls_nomecol, ls_nulla
date ldd_data
datetime ldt_data

setnull(ls_nulla)

choose case i_colname
	case "cod_prodotto_da"
		setitem(getrow(), "cod_cat_mer", ls_nulla)
		if isnull(getitemstring(getrow(), "cod_prodotto_a")) then
			setitem(getrow(), "cod_prodotto_a", i_coltext)
		end if
		
	case "cod_prodotto_a"
		setitem(getrow(), "cod_cat_mer", ls_nulla)
		
	case "cod_cat_mer"
		setitem(getrow(), "cod_prodotto_da", ls_nulla)
		setitem(getrow(), "cod_prodotto_a", ls_nulla)
		
	case "flag_det_depositi"
		if i_coltext = "N" then
			setitem(getrow(), "cod_deposito", ls_nulla)
		else
			setitem(getrow(), "flag_det_stock", "N")
		end if
		
	case "flag_visualizza_val"
		if i_coltext = "N" then
			setitem(getrow(), "flag_tipo_valorizzazione", "N")
		end if
		if i_coltext = "S" then
			setitem(getrow(), "flag_tipo_valorizzazione", "S")
		end if
	case "flag_tipo_valorizzazione"
		if i_coltext = "C" then
			dw_selezione.Object.path.Protect=0
///			pb_1.enabled=true
			setitem(getrow(),"flag_det_stock","N")
			dw_selezione.Object.flag_det_stock.Protect=1
			setitem(getrow(),"flag_det_depositi","N")
			dw_selezione.Object.flag_det_depositi.Protect=1
		else
			setitem(getrow(), "path", "")
			dw_selezione.Object.path.Protect=1
///			pb_1.enabled=false
			dw_selezione.Object.flag_det_stock.Protect=0
			dw_selezione.Object.flag_det_depositi.Protect=0
		end if
		
	case "flag_det_stock"
		if i_coltext = "S" then
			setitem(getrow(), "flag_det_depositi", "N")
		end if
		
	case "data_venduto_inizio"
		ldt_data = datetime(i_coltext)
		setitem(getrow(),"data_consumato_inizio", ldt_data)
		setitem(getrow(),"data_caricato_inizio", ldt_data)

	case "data_venduto_fine"
		ldt_data = datetime(i_coltext)
		setitem(getrow(),"data_consumato_fine", ldt_data)
		setitem(getrow(),"data_caricato_fine", ldt_data)


	case "flag_tipo_layout"
		wf_proteggi_campi(i_coltext)
		
		if i_coltext = "3" then
			setitem(getrow(), "flag_mancanti", "S")
			this.object.flag_mancanti.protect = 1
			
			//Donato 05/01/2009 abilita filtro tipologia riordino
			setitem(getrow(), "flag_visualizza_tipo_riordino", "T")
			this.object.flag_visualizza_tipo_riordino.visible = 1
			this.object.flag_visualizza_tipo_riordino_t.visible = 1
			//fine modifica ------------------------------------------------------
		elseif (i_coltext = "1" or i_coltext = "2") then
			this.object.flag_mancanti.protect = 0
			
			//Donato 05/01/2009 disabilita filtro tipologia riordino
			setitem(getrow(), "flag_visualizza_tipo_riordino", "T")
			this.object.flag_visualizza_tipo_riordino.visible = 0
			this.object.flag_visualizza_tipo_riordino_t.visible = 0
			//fine modifica ------------------------------------------------------
		
		//Donato 18-02-2009 aggiunto nuovo layout SCORTA MASSIMA: 4
		elseif i_coltext = "4" then
			//unici filtri abilitati devono essere:
			//data_riferimento, da prodotto a prodotto, categ. merceologica, codice comodo 2						
		end if
end choose
end event

event pcd_new;call super::pcd_new;dw_selezione.setitem(1,"data_riferimento",datetime(date(today()), 00:00:00))



end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_a")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
	case "b_nomenclatura"
		guo_ricerca.uof_ricerca_nomenclature(dw_selezione, "cod_nomenclatura")
end choose
end event

event clicked;call super::clicked;choose case dwo.name
		
	case "cod_deposito"
		dw_depositi_giacenza.show()
		
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_stato_magazzino
integer x = 46
integer y = 124
integer width = 3611
integer height = 1676
integer taborder = 20
string dataobject = "d_report_stato_magazzino"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;string ls_cod_prodotto,ls_cod_deposito,ls_errore
long ll_ret, ll_i, ll_riga
datetime ldt_data_riferimento_sel
dec{4} ld_scorta_minima
s_report_stato_magazzino ls_report_stato_magazzino
uo_mrp luo_mrp
window l_window
str_mrp lstr_mrp[]
datastore dw_grafico

if isvalid(dwo) then
	// apro il grafico MRP
	if dataobject = "d_report_stato_magazzino_layout3" then
		
		ldt_data_riferimento_sel = dw_selezione.getitemdatetime(1, "data_riferimento")
		ls_cod_prodotto = getitemstring(getrow(), "rs_cod_prodotto")
		
		select scorta_minima
		into   :ld_scorta_minima
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;

		if isnull(ld_scorta_minima) then ld_scorta_minima = 0
		
		luo_mrp = create uo_mrp
		
		ll_ret =luo_mrp.uof_mrp_prodotto(ls_cod_prodotto, &
													 ldt_data_riferimento_sel, &
													 ls_cod_deposito, &
													 ref lstr_mrp[], &
													 ref ls_errore )
		
		if ll_ret < 0 then
			g_mb.messagebox("Errore calcolo MRP prodotto " + ls_cod_prodotto, ls_errore)
			destroy luo_mrp
			return -1
		end if
		
		destroy luo_mrp
		
		dw_grafico = create datastore
		
		dw_grafico.dataobject = "d_report_stato_magazzino_layout3_gr"
		
		for ll_i = 1 to upperbound(lstr_mrp[])
			ll_riga = dw_grafico.insertrow(0)
			dw_grafico.setitem(ll_riga, "data_riferimento", lstr_mrp[ll_i].data)
			dw_grafico.setitem(ll_riga, "quan_fabbisogno", lstr_mrp[ll_i].quan_progressiva)
			dw_grafico.setitem(ll_riga, "quan_sottoscorta", 0)
			dw_grafico.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)

			ll_riga = dw_grafico.insertrow(0)
			dw_grafico.setitem(ll_riga, "data_riferimento", lstr_mrp[ll_i].data)
			dw_grafico.setitem(ll_riga, "quan_fabbisogno", ld_scorta_minima)
			dw_grafico.setitem(ll_riga, "quan_sottoscorta", 1)
			dw_grafico.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
			
		next
		
		window_open_parm(w_report_stato_magazzino_graph, -1, dw_grafico)
	
	end if
	
	
	choose case dwo.name
		case "rd_giacenza"
			if row > 0 then
				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
					
					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
					ls_report_stato_magazzino.xpos = xpos
					ls_report_stato_magazzino.ypos = ypos
					ls_report_stato_magazzino.tipo_report = "MAGAZZINO"
					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
				end if
			end if
			
		case "rd_impegnato"
			if row > 0 then
				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
					
					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
					ls_report_stato_magazzino.xpos = xpos
					ls_report_stato_magazzino.ypos = ypos
					ls_report_stato_magazzino.tipo_report = "IMPEGNO"
					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
				end if
			end if
			
		case "rd_ordinato"
			if row > 0 then
				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
					
					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
					ls_report_stato_magazzino.xpos = xpos
					ls_report_stato_magazzino.ypos = ypos
					ls_report_stato_magazzino.tipo_report = "ORDINATO"
					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
				end if
			end if
			
		case "rd_commesse"
			if row > 0 then
				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
					
					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
					ls_report_stato_magazzino.xpos = xpos
					ls_report_stato_magazzino.ypos = ypos
					ls_report_stato_magazzino.tipo_report = "COMMESSE"
					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
				end if
			end if
			
		case "rd_quan_rda"
			if row > 0 then
				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
					
					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
					ls_report_stato_magazzino.xpos = xpos
					ls_report_stato_magazzino.ypos = ypos
					ls_report_stato_magazzino.tipo_report = "RDA"
					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
				end if
			end if
			
		case "rd_tot_venduto"
			if row > 0 then
				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
					
					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
					ls_report_stato_magazzino.xpos = xpos
					ls_report_stato_magazzino.ypos = ypos
					ls_report_stato_magazzino.tipo_report = "MOVVEN"
					ls_report_stato_magazzino.data_inizio = dw_selezione.getitemdatetime(1, "data_venduto_inizio")
					ls_report_stato_magazzino.data_fine = dw_selezione.getitemdatetime(1, "data_venduto_fine")
					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
				end if
			end if
		case "cf_prodotto","compute_5"
			if row > 0 then
				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
					
					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
					ls_report_stato_magazzino.xpos = xpos
					ls_report_stato_magazzino.ypos = ypos
					ls_report_stato_magazzino.tipo_report = "DETMOVMAG"
					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
				end if
			end if
		case "rd_tot_consumato"
			if row > 0 then
				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
					
					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
					ls_report_stato_magazzino.xpos = xpos
					ls_report_stato_magazzino.ypos = ypos
					ls_report_stato_magazzino.tipo_report = "MOVCONSUMATO"
					ls_report_stato_magazzino.data_inizio = dw_selezione.getitemdatetime(1, "data_consumato_inizio")
					ls_report_stato_magazzino.data_fine = dw_selezione.getitemdatetime(1, "data_consumato_fine")
					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
				end if
			end if
		case "rd_tot_caricato"
			if row > 0 then
				if not isnull(getitemstring(row, "rs_cod_prodotto")) then
					
					ls_report_stato_magazzino.cod_prodotto = getitemstring(row, "rs_cod_prodotto")
					ls_report_stato_magazzino.xpos = xpos
					ls_report_stato_magazzino.ypos = ypos
					ls_report_stato_magazzino.tipo_report = "MOVCARICATO"
					ls_report_stato_magazzino.data_inizio = dw_selezione.getitemdatetime(1, "data_caricato_inizio")
					ls_report_stato_magazzino.data_fine = dw_selezione.getitemdatetime(1, "data_caricato_fine")
					openwithparm(l_window, ls_report_stato_magazzino, "w_analisi_fabbisogni_documenti_giacenze_deposito" ,PARENT)
				end if
			end if
	end choose		
end if
end event

event itemchanged;call super::itemchanged;resetupdate()

end event

