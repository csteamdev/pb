﻿$PBExportHeader$w_report_stato_magazzino_graph.srw
$PBExportComments$situazione magazzino
forward
global type w_report_stato_magazzino_graph from w_cs_xx_principale
end type
type dw_graph from uo_std_dw within w_report_stato_magazzino_graph
end type
end forward

global type w_report_stato_magazzino_graph from w_cs_xx_principale
integer width = 2894
integer height = 1644
dw_graph dw_graph
end type
global w_report_stato_magazzino_graph w_report_stato_magazzino_graph

type variables
str_mrp istr_mrp[]
end variables

event pc_setwindow;call super::pc_setwindow;string 	ls_des_prodotto, ls_cod_prodotto
long 		ll_ret 
datastore dw_origine

dw_origine = create datastore
dw_origine = message.powerobjectparm

ll_ret = dw_origine.rowscopy(1, dw_origine.rowcount(), primary!, dw_graph, 1, primary!)

if dw_graph.rowcount() > 0 then
	ls_cod_prodotto = dw_graph.getitemstring(1, "cod_prodotto")
	
	title = "GRAFICO MRP"
	
	select des_prodotto
	into :ls_des_prodotto
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
	 		cod_prodotto = :ls_cod_prodotto;
			 
	dw_graph.Object.gr_1.Title = ls_cod_prodotto + " - " + ls_des_prodotto
end if

end event

on w_report_stato_magazzino_graph.create
int iCurrent
call super::create
this.dw_graph=create dw_graph
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_graph
end on

on w_report_stato_magazzino_graph.destroy
call super::destroy
destroy(this.dw_graph)
end on

type dw_graph from uo_std_dw within w_report_stato_magazzino_graph
integer width = 2839
integer height = 1532
integer taborder = 10
string dataobject = "d_report_stato_magazzino_layout3_gr"
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

