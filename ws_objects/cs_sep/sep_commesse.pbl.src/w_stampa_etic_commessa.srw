﻿$PBExportHeader$w_stampa_etic_commessa.srw
forward
global type w_stampa_etic_commessa from window
end type
type cb_ok from commandbutton within w_stampa_etic_commessa
end type
type cb_chiudi from commandbutton within w_stampa_etic_commessa
end type
type em_quantita from editmask within w_stampa_etic_commessa
end type
type mle_testo from multilineedit within w_stampa_etic_commessa
end type
type st_3 from statictext within w_stampa_etic_commessa
end type
type st_2 from statictext within w_stampa_etic_commessa
end type
type dw_report from datawindow within w_stampa_etic_commessa
end type
type st_1 from statictext within w_stampa_etic_commessa
end type
end forward

global type w_stampa_etic_commessa from window
integer width = 3008
integer height = 2244
boolean titlebar = true
string title = "Stampa Etichetta Commessa"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_ok cb_ok
cb_chiudi cb_chiudi
em_quantita em_quantita
mle_testo mle_testo
st_3 st_3
st_2 st_2
dw_report dw_report
st_1 st_1
end type
global w_stampa_etic_commessa w_stampa_etic_commessa

type variables


integer				ii_anno_commessa
long					il_num_commessa
datetime				idt_data_consegna, idt_data_produzione
string					is_cod_prodotto, is_des_prodotto, is_cod_misura_mag
end variables

forward prototypes
public function string wf_formato_data (date adt_data)
end prototypes

public function string wf_formato_data (date adt_data);// funzione che trasforma la data nel formato stringa
// con la descrizione del giorno.

integer li_giorno
string  ls_string

li_giorno = daynumber(adt_data)

choose case li_giorno
	case 1 
		ls_string = "DOM"
	case 2 
		ls_string = "LUN"
	case 3 
		ls_string = "MAR"
	case 4 
		ls_string = "MER"
	case 5 
		ls_string = "GIO"
	case 6 
		ls_string = "VEN"
	case 7 
		ls_string = "SAB"
	case else 
		ls_string = " "
end choose

ls_string += " " + string(adt_data, "dd/mm/yy")

return ls_string

end function

on w_stampa_etic_commessa.create
this.cb_ok=create cb_ok
this.cb_chiudi=create cb_chiudi
this.em_quantita=create em_quantita
this.mle_testo=create mle_testo
this.st_3=create st_3
this.st_2=create st_2
this.dw_report=create dw_report
this.st_1=create st_1
this.Control[]={this.cb_ok,&
this.cb_chiudi,&
this.em_quantita,&
this.mle_testo,&
this.st_3,&
this.st_2,&
this.dw_report,&
this.st_1}
end on

on w_stampa_etic_commessa.destroy
destroy(this.cb_ok)
destroy(this.cb_chiudi)
destroy(this.em_quantita)
destroy(this.mle_testo)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.dw_report)
destroy(this.st_1)
end on

event open;
s_cs_xx_parametri			lstr_parametri
string							ls_LET



lstr_parametri = message.powerobjectparm

ii_anno_commessa = lstr_parametri.parametro_ul_1
il_num_commessa = lstr_parametri.parametro_ul_2


select 	a.data_consegna,
			a.cod_prodotto,
			b.des_prodotto,
			b.cod_misura_mag
into 	:idt_data_consegna,
		:is_cod_prodotto,
		:is_des_prodotto,
		:is_cod_misura_mag
from anag_commesse as a
join anag_prodotti as b on 	b.cod_azienda=a.cod_azienda and
									b.cod_prodotto=a.cod_prodotto
where 	a.cod_azienda=:s_cs_xx.cod_azienda and
			a.anno_commessa=:ii_anno_commessa and
			a.num_commessa=:il_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore in lettura dati Commessa: "+sqlca.sqlerrtext)
	cb_chiudi.postevent(clicked!)
	return
	
elseif sqlca.sqlcode=100 then
	g_mb.warning("La Commessa "+string(ii_anno_commessa)+"/"+string(il_num_commessa)+" non è stata trovata!")
	cb_chiudi.postevent(clicked!)
	return
end if


//logo su etichetta richiesta trasferimento
guo_functions.uof_get_parametro_azienda("LET", ls_LET)

ls_LET = s_cs_xx.volume + ls_LET
dw_report.Object.p_logo.filename = ls_LET


return
end event

type cb_ok from commandbutton within w_stampa_etic_commessa
integer x = 1518
integer y = 1864
integer width = 1088
integer height = 248
integer taborder = 30
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
string text = "Conferma"
boolean default = true
end type

event clicked;string				ls_valore, ls_testo, ls_bar
long				job, ll_index
integer			li_num_copie


dw_report.reset()

li_num_copie = integer(em_quantita.text)

if li_num_copie>0 then
else
	g_mb.warning("Inserire il numero di copie etichette da stampare!")
	return
end if

ls_testo = mle_testo.text


dw_report.insertrow(0)

dw_report.setitem(1, "anno_commessa", string(ii_anno_commessa))
dw_report.setitem(1, "num_commessa", string(il_num_commessa))
dw_report.setitem(1, "cod_prodotto", is_cod_prodotto)
dw_report.setitem(1, "des_prodotto", is_des_prodotto)
dw_report.setitem(1, "um_mag", is_cod_misura_mag)


idt_data_produzione = datetime(today(), 00:00:00)
ls_valore = wf_formato_data(date(idt_data_produzione))
dw_report.setitem(1, "data_produzione", ls_valore)

if not isnull(idt_data_consegna) and year(date(idt_data_consegna)) > 1950 then
	ls_valore = wf_formato_data(date(idt_data_consegna))
	dw_report.setitem(1, "data_consegna", ls_valore)
end if


dw_report.setitem(1, "testo", ls_testo)

//codice a barre del prodotto semilavorato
ls_bar = "*" + is_cod_prodotto + "*"
dw_report.setitem(1, "barcode1", ls_bar)
dw_report.setitem(1, "barcode2", ls_bar)


//ora procedi alla stampa
job = PrintOpen()
for ll_index = 1 to li_num_copie
	PrintDataWindow(job, dw_report)
next
PrintClose(job)


			

close(parent)
end event

type cb_chiudi from commandbutton within w_stampa_etic_commessa
integer x = 402
integer y = 1864
integer width = 1088
integer height = 248
integer taborder = 30
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
string text = "Chiudi"
end type

event clicked;close(parent)
end event

type em_quantita from editmask within w_stampa_etic_commessa
integer x = 1335
integer y = 376
integer width = 590
integer height = 212
integer taborder = 20
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
string text = "1"
boolean border = false
alignment alignment = center!
string mask = "###"
boolean spin = true
double increment = 1
string minmax = "0~~200"
end type

type mle_testo from multilineedit within w_stampa_etic_commessa
integer x = 46
integer y = 812
integer width = 2907
integer height = 992
integer taborder = 20
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
boolean border = false
boolean vscrollbar = true
boolean autovscroll = true
end type

type st_3 from statictext within w_stampa_etic_commessa
integer x = 32
integer y = 620
integer width = 1221
integer height = 168
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
string text = "Testo Libero:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_stampa_etic_commessa
integer x = 32
integer y = 376
integer width = 1221
integer height = 212
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
string text = "Q.tà Etichette:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_report from datawindow within w_stampa_etic_commessa
boolean visible = false
integer x = 247
integer y = 236
integer width = 933
integer height = 552
integer taborder = 10
string title = "none"
string dataobject = "d_label_commessa"
boolean border = false
boolean livescroll = true
end type

type st_1 from statictext within w_stampa_etic_commessa
integer x = 37
integer y = 24
integer width = 2926
integer height = 192
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 67108864
string text = "STAMPA ETICHETTE COMMESSA"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

