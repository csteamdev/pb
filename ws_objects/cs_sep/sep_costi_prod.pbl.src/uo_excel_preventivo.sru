﻿$PBExportHeader$uo_excel_preventivo.sru
forward
global type uo_excel_preventivo from nonvisualobject
end type
end forward

global type uo_excel_preventivo from nonvisualobject
end type
global uo_excel_preventivo uo_excel_preventivo

type variables
OLEObject myoleobject
boolean ib_OleObjectVisible = true
end variables

forward prototypes
public function string uof_nome_cella (long fl_riga, long fl_colonna)
public function integer uof_init ()
public function integer uof_crea_riga_pf (long fl_riga, long fl_colonna, string fs_descrizione, string fs_allineamento)
public function integer uof_crea_riga_lavorazione (long fl_riga, long fl_colonna, string fs_descrizione, string fs_allineamento)
public function integer uof_crea_riga_mp (long fl_riga, long fl_colonna, string fs_descrizione, string fs_allineamento)
public function integer uof_crea_riga_lavorazione_costo (long fl_riga, long fl_colonna, string fs_descrizione, string fs_allineamento, long fl_riga_inizio, long fl_riga_fine)
public function integer uof_intestazione (long fl_riga, long fl_colonna, string fs_descrizione)
public function integer uof_crea_riga_pf_costo (long fl_riga, long fl_colonna, string fs_allineamento, long fl_riga_inizio, long fl_riga_fine)
end prototypes

public function string uof_nome_cella (long fl_riga, long fl_colonna);string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if fl_colonna > 26 then
	ll_resto = mod(fl_colonna, 26)
	ll_i = long(fl_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	fl_colonna = ll_resto 
end if

if fl_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[fl_colonna] + string(fl_riga)
end if	

return ls_char
end function

public function integer uof_init ();long	ll_errore

ll_errore = myoleobject.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return -1
end if

//myoleobject.Visible = ib_OleObjectVisible
myoleobject.application.visible = ib_OleObjectVisible

myoleobject.Workbooks.Add

return 0
end function

public function integer uof_crea_riga_pf (long fl_riga, long fl_colonna, string fs_descrizione, string fs_allineamento);myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.name = "ARIAL"
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.size = 10
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.ColorIndex = 1
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.Bold = true
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).HorizontalAlignment = fs_allineamento
//myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).borders.linestyle = 1
//myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).borders.Weight = -4138
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).value = fs_descrizione

return 0
end function

public function integer uof_crea_riga_lavorazione (long fl_riga, long fl_colonna, string fs_descrizione, string fs_allineamento);myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.name = "ARIAL"
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.size = 10
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.ColorIndex = 1

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.underline = true

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).HorizontalAlignment = fs_allineamento
//myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).borders.linestyle = 1
//myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).borders.Weight = -4138
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).value = fs_descrizione


return 0
end function

public function integer uof_crea_riga_mp (long fl_riga, long fl_colonna, string fs_descrizione, string fs_allineamento);myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.name = "ARIAL"
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.size = 8
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.ColorIndex = 1

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.italic = true

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).HorizontalAlignment = fs_allineamento
//myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).borders.linestyle = 1
//myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).borders.Weight = -4138
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).value = fs_descrizione

return 0
end function

public function integer uof_crea_riga_lavorazione_costo (long fl_riga, long fl_colonna, string fs_descrizione, string fs_allineamento, long fl_riga_inizio, long fl_riga_fine);string ls_colonna, ls_formula

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.name = "ARIAL"
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.size = 10
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.ColorIndex = 1

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.underline = true

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).HorizontalAlignment = fs_allineamento

ls_colonna = char(64 + fl_colonna + 1) 

ls_formula = "=SOMMA("+ls_colonna+string(fl_riga_inizio)+&
																							":"+ls_colonna + string(fl_riga_fine) + ")" + "+"+fs_descrizione

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + &
			uof_nome_cella(fl_riga , fl_colonna) ).Formula = ls_formula
																							

return 0
end function

public function integer uof_intestazione (long fl_riga, long fl_colonna, string fs_descrizione);myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.name = "ARIAL"
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.size = 16
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).Font.Bold = True
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).Font.Italic = False
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.ColorIndex = 1
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).HorizontalAlignment = "2"
//myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).borders.linestyle = 1
//myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).borders.Weight = -4138
//myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).Interior.ColorIndex = 3
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).value = fs_descrizione

//myoleobject.worksheets(1).columns(1).ColumnWidth = 40


return 0
end function

public function integer uof_crea_riga_pf_costo (long fl_riga, long fl_colonna, string fs_allineamento, long fl_riga_inizio, long fl_riga_fine);string ls_colonna, ls_formula

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.name = "ARIAL"
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.size = 10
myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.ColorIndex = 1

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).font.Bold = true

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + uof_nome_cella(fl_riga , fl_colonna) ).HorizontalAlignment = fs_allineamento

ls_colonna = char(64 + fl_colonna + 1) 

ls_formula = "=SOMMA("+ls_colonna+string(fl_riga_inizio)+&
																							":"+ls_colonna + string(fl_riga_fine) + ")"

myoleobject.worksheets(1).range(uof_nome_cella(fl_riga, fl_colonna) + ":" + &
			uof_nome_cella(fl_riga , fl_colonna) ).Formula = ls_formula
																							

return 0
end function

on uo_excel_preventivo.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_excel_preventivo.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;myoleobject = CREATE OLEObject



end event

event destructor;destroy myoleobject;
end event

