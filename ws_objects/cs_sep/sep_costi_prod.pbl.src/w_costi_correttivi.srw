﻿$PBExportHeader$w_costi_correttivi.srw
forward
global type w_costi_correttivi from w_cs_xx_principale
end type
type cb_chiudi from commandbutton within w_costi_correttivi
end type
type dw_costi_correttivi_det from uo_cs_xx_dw within w_costi_correttivi
end type
type dw_costi_correttivi_lista from uo_cs_xx_dw within w_costi_correttivi
end type
end forward

global type w_costi_correttivi from w_cs_xx_principale
integer width = 3867
integer height = 1044
string title = "Costi Correttivi - Commessa"
boolean maxbox = false
boolean resizable = false
cb_chiudi cb_chiudi
dw_costi_correttivi_det dw_costi_correttivi_det
dw_costi_correttivi_lista dw_costi_correttivi_lista
end type
global w_costi_correttivi w_costi_correttivi

type variables
long il_anno_commessa, il_num_commessa
end variables

on w_costi_correttivi.create
int iCurrent
call super::create
this.cb_chiudi=create cb_chiudi
this.dw_costi_correttivi_det=create dw_costi_correttivi_det
this.dw_costi_correttivi_lista=create dw_costi_correttivi_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_chiudi
this.Control[iCurrent+2]=this.dw_costi_correttivi_det
this.Control[iCurrent+3]=this.dw_costi_correttivi_lista
end on

on w_costi_correttivi.destroy
call super::destroy
destroy(this.cb_chiudi)
destroy(this.dw_costi_correttivi_det)
destroy(this.dw_costi_correttivi_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_costi_correttivi_lista.set_dw_key("cod_azienda")

dw_costi_correttivi_lista.set_dw_key("anno_commessa")

dw_costi_correttivi_lista.set_dw_key("num_commessa")

dw_costi_correttivi_lista.set_dw_key("prog_costo_correttivo")

dw_costi_correttivi_lista.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)

dw_costi_correttivi_det.set_dw_options(sqlca,dw_costi_correttivi_lista,c_sharedata + c_scrollparent,c_default)

iuo_dw_main = dw_costi_correttivi_lista

dw_costi_correttivi_lista.change_dw_current()
end event

type cb_chiudi from commandbutton within w_costi_correttivi
integer x = 2537
integer y = 840
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

event getfocus;call super::getfocus;dw_costi_correttivi_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type dw_costi_correttivi_det from uo_cs_xx_dw within w_costi_correttivi
integer x = 23
integer y = 820
integer width = 2469
integer height = 120
integer taborder = 10
string dataobject = "d_costi_correttivi_det"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_costi_correttivi_det,"cod_fornitore")
end choose
end event

type dw_costi_correttivi_lista from uo_cs_xx_dw within w_costi_correttivi
integer x = 23
integer y = 20
integer width = 3817
integer height = 780
integer taborder = 10
string dataobject = "d_costi_correttivi_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;long ll_prog


select max(prog_costo_correttivo)
into   :ll_prog
from   costi_correttivi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_commessa = :il_anno_commessa and
		 num_commessa = :il_num_commessa;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("SEP","Errore in lettura massimo progressivo da costi_correttivi: " + sqlca.sqlerrtext)
	return -1
end if

if isnull(ll_prog) then
	ll_prog = 0
end if

ll_prog ++

setitem(getrow(),"prog_costo_correttivo",ll_prog)

dw_costi_correttivi_det.object.b_ricerca_fornitore.enabled = true

setfocus()
end event

event pcd_retrieve;call super::pcd_retrieve;il_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"anno_commessa")

il_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_commessa")

parent.title = "Costi Correttivi - Commessa " + string(il_anno_commessa) + "/" + string(il_num_commessa)

retrieve(s_cs_xx.cod_azienda,il_anno_commessa,il_num_commessa)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	setitem(ll_i,"anno_commessa",il_anno_commessa)
	setitem(ll_i,"num_commessa",il_num_commessa)
next
end event

event ue_key;if this.getcolumnname() = "flag_particolare" then
	if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
		this.triggerevent("pcd_save")
		this.postevent("pcd_new")
		this.setfocus()
	end if
end if 
end event

