﻿$PBExportHeader$w_costo_consuntivo_commessa.srw
$PBExportComments$Window Costo Consuntivo Commessa
forward
global type w_costo_consuntivo_commessa from w_cs_xx_principale
end type
type cb_calcola from commandbutton within w_costo_consuntivo_commessa
end type
type cb_report from commandbutton within w_costo_consuntivo_commessa
end type
type dw_elenco_commesse from uo_cs_xx_dw within w_costo_consuntivo_commessa
end type
type tab_1 from tab within w_costo_consuntivo_commessa
end type
type tab_materie_prime from userobject within tab_1
end type
type rb_prezzo_ac from radiobutton within tab_materie_prime
end type
type rb_costo_st from radiobutton within tab_materie_prime
end type
type rb_costo_ul from radiobutton within tab_materie_prime
end type
type dw_costi_materie_prime from datawindow within tab_materie_prime
end type
type tab_materie_prime from userobject within tab_1
rb_prezzo_ac rb_prezzo_ac
rb_costo_st rb_costo_st
rb_costo_ul rb_costo_ul
dw_costi_materie_prime dw_costi_materie_prime
end type
type tab_lavorazioni from userobject within tab_1
end type
type cbx_tempo_lavorazione from checkbox within tab_lavorazioni
end type
type cbx_tempo_attrezzaggio from checkbox within tab_lavorazioni
end type
type cbx_tempo_attrezzaggio_commessa from checkbox within tab_lavorazioni
end type
type dw_costo_lavorazioni from datawindow within tab_lavorazioni
end type
type tab_lavorazioni from userobject within tab_1
cbx_tempo_lavorazione cbx_tempo_lavorazione
cbx_tempo_attrezzaggio cbx_tempo_attrezzaggio
cbx_tempo_attrezzaggio_commessa cbx_tempo_attrezzaggio_commessa
dw_costo_lavorazioni dw_costo_lavorazioni
end type
type tab_risorse_umane from userobject within tab_1
end type
type dw_costo_risorse_umane from datawindow within tab_risorse_umane
end type
type cbx_tempo_risorsa_umana from checkbox within tab_risorse_umane
end type
type tab_risorse_umane from userobject within tab_1
dw_costo_risorse_umane dw_costo_risorse_umane
cbx_tempo_risorsa_umana cbx_tempo_risorsa_umana
end type
type tabpage_1 from userobject within tab_1
end type
type st_4 from statictext within tabpage_1
end type
type st_5 from statictext within tabpage_1
end type
type st_7 from statictext within tabpage_1
end type
type st_6 from statictext within tabpage_1
end type
type tabpage_1 from userobject within tab_1
st_4 st_4
st_5 st_5
st_7 st_7
st_6 st_6
end type
type tab_1 from tab within w_costo_consuntivo_commessa
tab_materie_prime tab_materie_prime
tab_lavorazioni tab_lavorazioni
tab_risorse_umane tab_risorse_umane
tabpage_1 tabpage_1
end type
type st_1 from statictext within w_costo_consuntivo_commessa
end type
type st_costo_totale_complessivo from statictext within w_costo_consuntivo_commessa
end type
type st_2 from statictext within w_costo_consuntivo_commessa
end type
type st_pezzi from statictext within w_costo_consuntivo_commessa
end type
type em_anno from editmask within w_costo_consuntivo_commessa
end type
type st_3 from statictext within w_costo_consuntivo_commessa
end type
type cb_aggiorna from commandbutton within w_costo_consuntivo_commessa
end type
type st_8 from statictext within w_costo_consuntivo_commessa
end type
type st_costo_totale_complessivo_preventivo from statictext within w_costo_consuntivo_commessa
end type
type st_9 from statictext within w_costo_consuntivo_commessa
end type
type st_delta from statictext within w_costo_consuntivo_commessa
end type
end forward

global type w_costo_consuntivo_commessa from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 4014
integer height = 1964
string title = "Costo Consuntivo per Commessa"
cb_calcola cb_calcola
cb_report cb_report
dw_elenco_commesse dw_elenco_commesse
tab_1 tab_1
st_1 st_1
st_costo_totale_complessivo st_costo_totale_complessivo
st_2 st_2
st_pezzi st_pezzi
em_anno em_anno
st_3 st_3
cb_aggiorna cb_aggiorna
st_8 st_8
st_costo_totale_complessivo_preventivo st_costo_totale_complessivo_preventivo
st_9 st_9
st_delta st_delta
end type
global w_costo_consuntivo_commessa w_costo_consuntivo_commessa

forward prototypes
public function integer wf_fasi_lavorazione (integer fi_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, string fs_errore)
end prototypes

public function integer wf_fasi_lavorazione (integer fi_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, string fs_errore);// Funzione che calcola i costi consuntivi di lavorazione
// nome: wf_fasi_lavorazione
// tipo: integer
//       -1 failed
//  		 0 passed
//
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							 fi_anno_commessa			 integer			valore
//                    fl_num_commessa			 long				valore
//							 fs_cod_prodotto	 		 string  		valore
//							 fdd_quantita_richiesta	 double			valore
//
//		Creata il 24-07-97 
//		Autore Diego Ferrari

string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, & 
			 ls_cod_lavorazione,ls_test,ls_cod_cat_attrezzature,ls_des_cat_attrezzature, &
			 ls_cod_cat_attrezzature_2,ls_des_cat_attrezzature_2,ls_flag_materia_prima,ls_cod_prodotto_inserito,&
			 ls_cod_prodotto_variante,ls_flag_materia_prima_variante
long      ll_num_righe,ll_num_figli
integer   li_risposta
dec{4}    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione, ldd_tempo_totale, ldd_costo_medio_orario, ldd_costo_totale, &
			 ldd_tempo_risorsa_umana,ldd_costo_medio_orario_2,ldd_costo_totale_2,ldd_quan_prodotta, & 
			 ldd_quan_utilizzo_variante,ldd_tempo_attrezzaggio_prev,ldd_tempo_attrezzaggio_commessa_prev, & 
			 ldd_tempo_lavorazione_prev, ldd_tempo_risorsa_umana_prev,ldd_tempo_totale_prev,ldd_costo_prev, & 
			 ldd_quan_prodotta_fase, ldd_costo_totale_2_prev

datastore lds_righe_distinta

ldd_quan_prodotta = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_quan_prodotta")

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fdd_quantita_richiesta
	ls_flag_materia_prima=lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	
//****************************
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio

	select cod_prodotto,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:fi_anno_commessa
	and    num_commessa=:fl_num_commessa
	and    cod_prodotto_padre=:fs_cod_prodotto
	and    cod_prodotto_figlio=:ls_cod_prodotto_figlio
	and    cod_versione=:fs_cod_versione;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante* fdd_quantita_richiesta
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	if ls_flag_materia_prima = 'N' then
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda=:s_cs_xx.cod_azienda 
		and 	 cod_prodotto_padre=:ls_cod_prodotto_inserito;
		
		if sqlca.sqlcode=100 then continue
	else
		continue
	end if

//*************************
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda=:s_cs_xx.cod_azienda 
	and 	 cod_prodotto_padre=:ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode=100 then continue

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 cod_cat_attrezzature_2,
			 tempo_lavorazione,
			 tempo_attrezzaggio,
			 tempo_attrezzaggio_commessa,
			 tempo_risorsa_umana
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_lavorazione_prev,
			 :ldd_tempo_attrezzaggio_prev,
			 :ldd_tempo_attrezzaggio_commessa_prev,
			 :ldd_tempo_risorsa_umana_prev
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda  
	AND   cod_prodotto = :ls_cod_prodotto_figlio;

	if sqlca.sqlcode=100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		return -1
	end if
  
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto =:ls_cod_prodotto_figlio;

	select des_cat_attrezzature,
		    costo_medio_orario
	into   :ls_des_cat_attrezzature,
			 :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;

	select des_cat_attrezzature,
			 costo_medio_orario
	into   :ls_des_cat_attrezzature_2,
			 :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature_2;
	
	select sum(tempo_attrezzaggio),
			 sum(tempo_attrezzaggio_commessa),
		    sum(tempo_lavorazione),
			 sum(tempo_risorsa_umana),
			 sum(quan_prodotta)
	into   :ldd_tempo_attrezzaggio,
			 :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ldd_tempo_risorsa_umana,
			 :ldd_quan_prodotta_fase
	from   avan_produzione_com
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:fi_anno_commessa
	and    num_commessa =:fl_num_commessa	
	and    cod_prodotto =:ls_cod_prodotto_figlio
	and    cod_lavorazione=:ls_cod_lavorazione
	and    cod_reparto=:ls_cod_reparto;


	if isnull(ldd_tempo_attrezzaggio) then ldd_tempo_attrezzaggio = 0
	
	if isnull(ldd_tempo_attrezzaggio_commessa) then ldd_tempo_attrezzaggio_commessa = 0
	
	if isnull(ldd_tempo_lavorazione) then ldd_tempo_lavorazione = 0
	
	if isnull(ldd_tempo_risorsa_umana) then ldd_tempo_risorsa_umana = 0
	
	if isnull(ldd_tempo_attrezzaggio_prev) then ldd_tempo_attrezzaggio_prev = 0
	
	if isnull(ldd_tempo_attrezzaggio_commessa_prev) then ldd_tempo_attrezzaggio_commessa_prev = 0
	
	if isnull(ldd_tempo_lavorazione_prev) then ldd_tempo_lavorazione_prev = 0
	
	if isnull(ldd_tempo_risorsa_umana_prev) then ldd_tempo_risorsa_umana_prev = 0
	

	ldd_tempo_attrezzaggio_prev = ldd_tempo_attrezzaggio_prev
	
	ldd_tempo_attrezzaggio_commessa_prev = ldd_tempo_attrezzaggio_commessa_prev

	ldd_tempo_lavorazione_prev = ldd_tempo_lavorazione_prev * ldd_quan_prodotta_fase
	
	ldd_tempo_risorsa_umana_prev = ldd_tempo_risorsa_umana_prev * ldd_quan_prodotta_fase

	ldd_tempo_totale = 0
	ldd_tempo_totale_prev = 0
	
	if tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio
		ldd_tempo_totale_prev = ldd_tempo_totale_prev + ldd_tempo_attrezzaggio_prev
	end if

	if tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio_commessa.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio_commessa
		ldd_tempo_totale_prev = ldd_tempo_totale_prev + ldd_tempo_attrezzaggio_commessa_prev
	end if

	if tab_1.tab_lavorazioni.cbx_tempo_lavorazione.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_lavorazione
		ldd_tempo_totale_prev = ldd_tempo_totale_prev + ldd_tempo_lavorazione_prev
	end if

	ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
	ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2

	ldd_costo_totale_2_prev = (ldd_tempo_risorsa_umana_prev/60) * ldd_costo_medio_orario_2
	ldd_costo_prev = (ldd_tempo_totale_prev/60) * ldd_costo_medio_orario

	
   tab_1.tab_lavorazioni.dw_costo_lavorazioni.insertrow(1)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"num_pezzi",ldd_quan_prodotta)
   tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"cod_prodotto",ls_cod_prodotto_figlio)
 	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"des_prodotto",ls_des_prodotto)
 	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"cod_lavorazione",ls_cod_lavorazione)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"cod_cat_attrezzature",ls_cod_cat_attrezzature)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"des_cat_attrezzature",ls_des_cat_attrezzature)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_attrezzaggio",ldd_tempo_attrezzaggio)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_attrezzaggio_commessa",ldd_tempo_attrezzaggio_commessa)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_lavorazione",ldd_tempo_lavorazione)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_totale",ldd_tempo_totale)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_attrezzaggio_prev",ldd_tempo_attrezzaggio_prev)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_attrezzaggio_com_prev",ldd_tempo_attrezzaggio_commessa_prev)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_lavorazione_prev",ldd_tempo_lavorazione_prev)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_totale_prev",ldd_tempo_totale_prev)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"costo_totale_prev",ldd_costo_prev)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"costo_medio_orario",ldd_costo_medio_orario)  
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"costo_totale",ldd_costo_totale)  
	
   tab_1.tab_risorse_umane.dw_costo_risorse_umane.insertrow(1)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"cod_cat_attrezzature",ls_cod_cat_attrezzature_2)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"des_cat_attrezzature",ls_des_cat_attrezzature_2)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"tempo_totale",ldd_tempo_risorsa_umana)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"tempo_totale_prev",ldd_tempo_risorsa_umana_prev)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"costo_medio_orario",ldd_costo_medio_orario_2)  
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"costo_totale",ldd_costo_totale_2)  
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"costo_totale_prev",ldd_costo_totale_2_prev)  

	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta=wf_fasi_lavorazione(fi_anno_commessa,fl_num_commessa,ls_cod_prodotto_figlio,fs_cod_versione,ldd_quan_utilizzo,ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

return 0
end function

on w_costo_consuntivo_commessa.create
int iCurrent
call super::create
this.cb_calcola=create cb_calcola
this.cb_report=create cb_report
this.dw_elenco_commesse=create dw_elenco_commesse
this.tab_1=create tab_1
this.st_1=create st_1
this.st_costo_totale_complessivo=create st_costo_totale_complessivo
this.st_2=create st_2
this.st_pezzi=create st_pezzi
this.em_anno=create em_anno
this.st_3=create st_3
this.cb_aggiorna=create cb_aggiorna
this.st_8=create st_8
this.st_costo_totale_complessivo_preventivo=create st_costo_totale_complessivo_preventivo
this.st_9=create st_9
this.st_delta=create st_delta
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_calcola
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_elenco_commesse
this.Control[iCurrent+4]=this.tab_1
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.st_costo_totale_complessivo
this.Control[iCurrent+7]=this.st_2
this.Control[iCurrent+8]=this.st_pezzi
this.Control[iCurrent+9]=this.em_anno
this.Control[iCurrent+10]=this.st_3
this.Control[iCurrent+11]=this.cb_aggiorna
this.Control[iCurrent+12]=this.st_8
this.Control[iCurrent+13]=this.st_costo_totale_complessivo_preventivo
this.Control[iCurrent+14]=this.st_9
this.Control[iCurrent+15]=this.st_delta
end on

on w_costo_consuntivo_commessa.destroy
call super::destroy
destroy(this.cb_calcola)
destroy(this.cb_report)
destroy(this.dw_elenco_commesse)
destroy(this.tab_1)
destroy(this.st_1)
destroy(this.st_costo_totale_complessivo)
destroy(this.st_2)
destroy(this.st_pezzi)
destroy(this.em_anno)
destroy(this.st_3)
destroy(this.cb_aggiorna)
destroy(this.st_8)
destroy(this.st_costo_totale_complessivo_preventivo)
destroy(this.st_9)
destroy(this.st_delta)
end on

event pc_setwindow;call super::pc_setwindow;dw_elenco_commesse.set_dw_options(sqlca,pcca.null_object,c_nonew + & 
													 c_nodelete + c_nomodify +  & 
													 c_disablecc,c_disableccinsert)

em_anno.text=string(year(today()))
end event

type cb_calcola from commandbutton within w_costo_consuntivo_commessa
integer x = 846
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 51
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcola"
end type

event clicked;integer li_risposta,li_anno_commessa
string  ls_mat_prima[],ls_cod_prodotto,ls_des_prodotto,ls_errore,ls_cod_versione
double  ldd_quan_utilizzo[],ldd_quan_prodotta,ldd_quantita_totale,ldd_costo_materia_prima, &
		  ldd_costo_totale,ldd_costo_totale_mp, ldd_costo_totale_lav,ldd_costo_totale_ru, &
		  ldd_quan_utilizzata,ldd_quan_scarto,ldd_costo_utilizzo,ldd_costo_scarto,ldd_quan_spedita,ldd_costo_preventivo, & 
		  ldd_costo_totale_preventivo,ldd_costo_totale_lav_prev,ldd_costo_totale_ru_prev
long    ll_t,ll_num_commessa

li_anno_commessa = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_anno_commessa")
ll_num_commessa = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_num_commessa")

if li_anno_commessa=0 or ll_num_commessa=0 then
	g_mb.messagebox("Sep","Attenzione! Selezionare una commessa prima di calcolare il costo consuntivo.",stopsign!)
	return
end if	

ldd_quan_prodotta = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_quan_prodotta")

if ldd_quan_prodotta <=0 then
	g_mb.messagebox("Sep","Attenzione! Non è stata prodotta alcuna quantità della commessa selezionata",stopsign!)
	return
end if

st_pezzi.text=string(ldd_quan_prodotta)

ls_cod_prodotto = dw_elenco_commesse.getitemstring(dw_elenco_commesse.getrow(),"anag_commesse_cod_prodotto")	
ls_cod_versione = dw_elenco_commesse.getitemstring(dw_elenco_commesse.getrow(),"anag_commesse_cod_versione")	

tab_1.tab_materie_prime.dw_costi_materie_prime.reset()
tab_1.tab_lavorazioni.dw_costo_lavorazioni.reset()
tab_1.tab_risorse_umane.dw_costo_risorse_umane.reset()

li_risposta=f_trova_mat_prima(ls_cod_prodotto,ls_cod_versione,ls_mat_prima[],ldd_quan_utilizzo[],ldd_quan_prodotta,li_anno_commessa,ll_num_commessa)

for ll_t=1 to upperbound(ls_mat_prima)

	select sum(quan_utilizzata),
			sum(quan_scarto)
	into   :ldd_quan_utilizzata,
			:ldd_quan_scarto
	from   mat_prime_commessa
	where  cod_azienda=:s_cs_xx.cod_azienda
	and		anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    cod_prodotto=:ls_mat_prima[ll_t];
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore nel DB"+ sqlca.sqlerrtext,stopsign!)
		return 
	end if
	
	if isnull(ldd_quan_utilizzata) then ldd_quan_utilizzata = 0
	if isnull(ldd_quan_scarto) then ldd_quan_scarto = 0
	
	select sum(quan_spedita)
	into   :ldd_quan_spedita
	from   prod_bolle_out_com
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    cod_prodotto_spedito=:ls_mat_prima[ll_t];
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore nel DB"+ sqlca.sqlerrtext,stopsign!)
		return 
	end if
	
	if isnull(ldd_quan_spedita) then 
		ldd_quan_spedita = 0
	else
		ldd_quan_utilizzata = ldd_quan_spedita
	end if
	
	ldd_quantita_totale = ldd_quan_utilizzata + ldd_quan_scarto
	
	if tab_1.tab_materie_prime.rb_prezzo_ac.checked = true then
		select des_prodotto,prezzo_acquisto
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_prodotto = :ls_mat_prima[ll_t];
	end if
	
	if tab_1.tab_materie_prime.rb_costo_st.checked = true then
		select des_prodotto,costo_standard
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_prodotto = :ls_mat_prima[ll_t];
	end if
	
	if tab_1.tab_materie_prime.rb_costo_ul.checked = true then
		select des_prodotto,costo_ultimo
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_prodotto = :ls_mat_prima[ll_t];
	end if
	

 	ldd_costo_utilizzo = ldd_quan_utilizzata * ldd_costo_materia_prima
	ldd_costo_scarto = ldd_quan_scarto * ldd_costo_materia_prima 
	ldd_costo_totale = ldd_costo_materia_prima * ldd_quantita_totale
	ldd_costo_preventivo = ldd_costo_materia_prima * ldd_quan_utilizzo[ll_t]
	tab_1.tab_materie_prime.dw_costi_materie_prime.insertrow(1)
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem(1,"cod_prodotto",ls_mat_prima[ll_t])
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem(1,"des_prodotto",ls_des_prodotto)
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem(1,"quan_scarto",ldd_quan_scarto)  
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem(1,"quan_utilizzo",ldd_quan_utilizzata)  
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem(1,"quan_totale",ldd_quantita_totale)  
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem(1,"costo_unitario",ldd_costo_materia_prima)  	
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem(1,"costo_scarto",ldd_costo_scarto)  	
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem(1,"costo_utilizzo",ldd_costo_utilizzo)  	
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem(1,"costo_totale",ldd_costo_totale)  		
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem(1,"quan_utilizzo_prev",ldd_quan_utilizzo[ll_t])  		
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem(1,"costo_totale_prev",ldd_costo_preventivo)  		

next

li_risposta=wf_fasi_lavorazione(li_anno_commessa,ll_num_commessa,ls_cod_prodotto,ls_cod_versione,1,ls_errore)

if tab_1.tab_materie_prime.dw_costi_materie_prime.rowcount() > 0 then
	ldd_costo_totale_mp =  tab_1.tab_materie_prime.dw_costi_materie_prime.getitemnumber(1,"cf_costo_totale")
end if

if tab_1.tab_lavorazioni.dw_costo_lavorazioni.rowcount() > 0 then
	ldd_costo_totale_lav = tab_1.tab_lavorazioni.dw_costo_lavorazioni.getitemnumber(1,"cf_costo_totale")
	ldd_costo_totale_lav_prev = tab_1.tab_lavorazioni.dw_costo_lavorazioni.getitemnumber(1,"cf_costo_totale_prev")
end if

if tab_1.tab_risorse_umane.dw_costo_risorse_umane.rowcount() > 0 then
	ldd_costo_totale_ru = tab_1.tab_risorse_umane.dw_costo_risorse_umane.getitemnumber(1,"cf_costo_totale")
	ldd_costo_totale_ru_prev = tab_1.tab_risorse_umane.dw_costo_risorse_umane.getitemnumber(1,"cf_costo_totale_prev")
end if

ldd_costo_totale_preventivo = tab_1.tab_materie_prime.dw_costi_materie_prime.getitemnumber(1,"cf_costo_totale_preventivo")

if not(tab_1.tab_risorse_umane.cbx_tempo_risorsa_umana.checked) then 
	ldd_costo_totale_ru = 0
	ldd_costo_totale_ru_prev = 0
end if

ldd_costo_totale_preventivo = ldd_costo_totale_preventivo + ldd_costo_totale_lav_prev +ldd_costo_totale_ru_prev

st_costo_totale_complessivo.text = string(ldd_costo_totale_mp + ldd_costo_totale_lav + ldd_costo_totale_ru, "[currency]")
st_costo_totale_complessivo_preventivo.text = string(ldd_costo_totale_preventivo, "[currency]")

if ldd_costo_totale_preventivo <> 0 then
	st_delta.text = string(truncate(((ldd_costo_totale_mp + ldd_costo_totale_lav + ldd_costo_totale_ru - ldd_costo_totale_preventivo)/ldd_costo_totale_preventivo)*100,1))
end if
end event

type cb_report from commandbutton within w_costo_consuntivo_commessa
integer x = 3589
integer y = 1760
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_prodotto, ls_cod_versione

ls_cod_prodotto = dw_elenco_commesse.getitemstring(dw_elenco_commesse.getrow(),"anag_commesse_cod_prodotto")
ls_cod_versione = dw_elenco_commesse.getitemstring(dw_elenco_commesse.getrow(),"anag_commesse_cod_versione")

if isnull(ls_cod_prodotto) or isnull(ls_cod_versione) or ls_cod_prodotto="" or ls_cod_versione = "" then
	g_mb.messagebox("Sep","Non è possibile calcolare il costo poichè non è selezionato alcun prodotto!",stopsign!)
	return
end if

s_cs_xx.parametri.parametro_ul_1 = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_anno_commessa")
s_cs_xx.parametri.parametro_ul_2 = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_num_commessa")
s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
s_cs_xx.parametri.parametro_s_2 = ls_cod_versione
	
if tab_1.tab_materie_prime.rb_prezzo_ac.checked = true then
	s_cs_xx.parametri.parametro_s_3 = "A"
end if

if tab_1.tab_materie_prime.rb_costo_st.checked = true then
	s_cs_xx.parametri.parametro_s_3 = "S"
end if

if tab_1.tab_materie_prime.rb_costo_ul.checked = true then
	s_cs_xx.parametri.parametro_s_3 = "U"
end if
	
s_cs_xx.parametri.parametro_d_1 = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_quan_prodotta")

window_open( w_report_costo_consuntivo, -1)
end event

type dw_elenco_commesse from uo_cs_xx_dw within w_costo_consuntivo_commessa
event pcd_pickedrow pbm_custom55
event pcd_retrieve pbm_custom60
integer x = 23
integer y = 20
integer width = 800
integer height = 1720
integer taborder = 10
string dataobject = "d_elenco_commesse"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,long(em_anno.text))

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event rowfocuschanged;call super::rowfocuschanged;//if dw_elenco_commesse.rowcount() > 0 then
//	cb_calcola.triggerevent(clicked!)
//end if
end event

type tab_1 from tab within w_costo_consuntivo_commessa
integer x = 846
integer y = 20
integer width = 3109
integer height = 1620
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean raggedright = true
boolean powertips = true
integer selectedtab = 1
tab_materie_prime tab_materie_prime
tab_lavorazioni tab_lavorazioni
tab_risorse_umane tab_risorse_umane
tabpage_1 tabpage_1
end type

on tab_1.create
this.tab_materie_prime=create tab_materie_prime
this.tab_lavorazioni=create tab_lavorazioni
this.tab_risorse_umane=create tab_risorse_umane
this.tabpage_1=create tabpage_1
this.Control[]={this.tab_materie_prime,&
this.tab_lavorazioni,&
this.tab_risorse_umane,&
this.tabpage_1}
end on

on tab_1.destroy
destroy(this.tab_materie_prime)
destroy(this.tab_lavorazioni)
destroy(this.tab_risorse_umane)
destroy(this.tabpage_1)
end on

type tab_materie_prime from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3072
integer height = 1496
long backcolor = 79741120
string text = "Materie Prime"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 553648127
rb_prezzo_ac rb_prezzo_ac
rb_costo_st rb_costo_st
rb_costo_ul rb_costo_ul
dw_costi_materie_prime dw_costi_materie_prime
end type

on tab_materie_prime.create
this.rb_prezzo_ac=create rb_prezzo_ac
this.rb_costo_st=create rb_costo_st
this.rb_costo_ul=create rb_costo_ul
this.dw_costi_materie_prime=create dw_costi_materie_prime
this.Control[]={this.rb_prezzo_ac,&
this.rb_costo_st,&
this.rb_costo_ul,&
this.dw_costi_materie_prime}
end on

on tab_materie_prime.destroy
destroy(this.rb_prezzo_ac)
destroy(this.rb_costo_st)
destroy(this.rb_costo_ul)
destroy(this.dw_costi_materie_prime)
end on

type rb_prezzo_ac from radiobutton within tab_materie_prime
integer x = 5
integer y = 1432
integer width = 503
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Prezzo Acquisto"
boolean checked = true
boolean lefttext = true
end type

type rb_costo_st from radiobutton within tab_materie_prime
integer x = 645
integer y = 1432
integer width = 485
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Costo Standard"
boolean lefttext = true
end type

type rb_costo_ul from radiobutton within tab_materie_prime
integer x = 1285
integer y = 1432
integer width = 411
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Costo Ultimo"
boolean lefttext = true
end type

type dw_costi_materie_prime from datawindow within tab_materie_prime
integer x = 5
integer y = 32
integer width = 3017
integer height = 1400
integer taborder = 41
string dataobject = "d_costi_materie_prime_cons"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tab_lavorazioni from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3072
integer height = 1496
long backcolor = 79741120
string text = "Lavorazioni"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
cbx_tempo_lavorazione cbx_tempo_lavorazione
cbx_tempo_attrezzaggio cbx_tempo_attrezzaggio
cbx_tempo_attrezzaggio_commessa cbx_tempo_attrezzaggio_commessa
dw_costo_lavorazioni dw_costo_lavorazioni
end type

on tab_lavorazioni.create
this.cbx_tempo_lavorazione=create cbx_tempo_lavorazione
this.cbx_tempo_attrezzaggio=create cbx_tempo_attrezzaggio
this.cbx_tempo_attrezzaggio_commessa=create cbx_tempo_attrezzaggio_commessa
this.dw_costo_lavorazioni=create dw_costo_lavorazioni
this.Control[]={this.cbx_tempo_lavorazione,&
this.cbx_tempo_attrezzaggio,&
this.cbx_tempo_attrezzaggio_commessa,&
this.dw_costo_lavorazioni}
end on

on tab_lavorazioni.destroy
destroy(this.cbx_tempo_lavorazione)
destroy(this.cbx_tempo_attrezzaggio)
destroy(this.cbx_tempo_attrezzaggio_commessa)
destroy(this.dw_costo_lavorazioni)
end on

type cbx_tempo_lavorazione from checkbox within tab_lavorazioni
integer x = 5
integer y = 1632
integer width = 558
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "tempo lavorazione"
boolean checked = true
boolean lefttext = true
end type

type cbx_tempo_attrezzaggio from checkbox within tab_lavorazioni
integer x = 690
integer y = 1632
integer width = 581
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "tempo attrezzaggio"
boolean checked = true
boolean lefttext = true
end type

type cbx_tempo_attrezzaggio_commessa from checkbox within tab_lavorazioni
integer x = 1399
integer y = 1632
integer width = 887
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "tempo attrezzaggio commessa"
boolean checked = true
boolean lefttext = true
end type

type dw_costo_lavorazioni from datawindow within tab_lavorazioni
integer x = 5
integer y = 32
integer width = 3040
integer height = 1460
integer taborder = 31
string dataobject = "d_costo_lavorazioni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tab_risorse_umane from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3072
integer height = 1496
long backcolor = 79741120
string text = "Risorse Umane"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_costo_risorse_umane dw_costo_risorse_umane
cbx_tempo_risorsa_umana cbx_tempo_risorsa_umana
end type

on tab_risorse_umane.create
this.dw_costo_risorse_umane=create dw_costo_risorse_umane
this.cbx_tempo_risorsa_umana=create cbx_tempo_risorsa_umana
this.Control[]={this.dw_costo_risorse_umane,&
this.cbx_tempo_risorsa_umana}
end on

on tab_risorse_umane.destroy
destroy(this.dw_costo_risorse_umane)
destroy(this.cbx_tempo_risorsa_umana)
end on

type dw_costo_risorse_umane from datawindow within tab_risorse_umane
integer x = 5
integer y = 32
integer width = 3040
integer height = 1460
integer taborder = 41
boolean bringtotop = true
string dataobject = "d_costo_risorse_umane"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cbx_tempo_risorsa_umana from checkbox within tab_risorse_umane
integer x = 5
integer y = 1632
integer width = 709
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Tempo Risorsa Umana"
boolean checked = true
boolean lefttext = true
end type

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3072
integer height = 1496
long backcolor = 79741120
string text = "Help"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
st_4 st_4
st_5 st_5
st_7 st_7
st_6 st_6
end type

on tabpage_1.create
this.st_4=create st_4
this.st_5=create st_5
this.st_7=create st_7
this.st_6=create st_6
this.Control[]={this.st_4,&
this.st_5,&
this.st_7,&
this.st_6}
end on

on tabpage_1.destroy
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_7)
destroy(this.st_6)
end on

type st_4 from statictext within tabpage_1
integer x = 73
integer y = 132
integer width = 2514
integer height = 80
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 79741120
boolean enabled = false
string text = "Attenzione il consuntivo dei costi di commessa può essere fatto solo sulle commesse"
boolean focusrectangle = false
end type

type st_5 from statictext within tabpage_1
integer x = 73
integer y = 352
integer width = 2720
integer height = 76
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 79741120
boolean enabled = false
string text = "Per quelle automatiche il consuntivo coinciderebbe con "
boolean focusrectangle = false
end type

type st_7 from statictext within tabpage_1
integer x = 73
integer y = 472
integer width = 1275
integer height = 76
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 79741120
boolean enabled = false
string text = "il preventivo dei costi di produzione."
boolean focusrectangle = false
end type

type st_6 from statictext within tabpage_1
integer x = 73
integer y = 232
integer width = 2149
integer height = 76
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 79741120
boolean enabled = false
string text = "per le commesse con l~'avanzamento di produzione completo."
boolean focusrectangle = false
end type

type st_1 from statictext within w_costo_consuntivo_commessa
integer x = 1760
integer y = 1660
integer width = 594
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Costo Tot.Consuntivo:"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_costo_totale_complessivo from statictext within w_costo_consuntivo_commessa
integer x = 2377
integer y = 1660
integer width = 731
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_2 from statictext within w_costo_consuntivo_commessa
integer x = 846
integer y = 1760
integer width = 389
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Pezzi Prodotti:"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_pezzi from statictext within w_costo_consuntivo_commessa
integer x = 1257
integer y = 1760
integer width = 480
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type em_anno from editmask within w_costo_consuntivo_commessa
integer x = 549
integer y = 1760
integer width = 274
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
boolean spin = true
string displaydata = ""
end type

type st_3 from statictext within w_costo_consuntivo_commessa
integer x = 389
integer y = 1760
integer width = 160
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Anno:"
alignment alignment = right!
long bordercolor = 79741120
boolean focusrectangle = false
end type

type cb_aggiorna from commandbutton within w_costo_consuntivo_commessa
integer x = 23
integer y = 1760
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;parent.triggerevent("pc_retrieve")
end event

type st_8 from statictext within w_costo_consuntivo_commessa
integer x = 1760
integer y = 1760
integer width = 594
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Costo Tot.Preventivo:"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_costo_totale_complessivo_preventivo from statictext within w_costo_consuntivo_commessa
integer x = 2377
integer y = 1760
integer width = 731
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_9 from statictext within w_costo_consuntivo_commessa
integer x = 3223
integer y = 1660
integer width = 247
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Delta % :"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_delta from statictext within w_costo_consuntivo_commessa
integer x = 3497
integer y = 1660
integer width = 343
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

