﻿$PBExportHeader$w_costo_consuntivo_commessa_new.srw
$PBExportComments$Window Costo Consuntivo Commessa
forward
global type w_costo_consuntivo_commessa_new from w_cs_xx_principale
end type
type cb_calcola from commandbutton within w_costo_consuntivo_commessa_new
end type
type st_1 from statictext within w_costo_consuntivo_commessa_new
end type
type cb_cerca from commandbutton within w_costo_consuntivo_commessa_new
end type
type dw_selezione from uo_cs_xx_dw within w_costo_consuntivo_commessa_new
end type
type dw_report from uo_cs_xx_dw within w_costo_consuntivo_commessa_new
end type
type dw_folder from u_folder within w_costo_consuntivo_commessa_new
end type
type dw_elenco_commesse from datawindow within w_costo_consuntivo_commessa_new
end type
end forward

global type w_costo_consuntivo_commessa_new from w_cs_xx_principale
integer width = 3781
integer height = 2528
string title = "Report Costo Consuntivo Commessa"
boolean resizable = false
event ue_inizio ( )
cb_calcola cb_calcola
st_1 st_1
cb_cerca cb_cerca
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
dw_elenco_commesse dw_elenco_commesse
end type
global w_costo_consuntivo_commessa_new w_costo_consuntivo_commessa_new

forward prototypes
public function integer wf_calcola ()
public function integer wf_fasi_lavorazione (integer fi_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, decimal fdd_quantita_richiesta, ref string fs_errore, ref datawindowchild fds_fasi, ref datawindowchild fds_risorse)
end prototypes

event ue_inizio();// 
LONG  l_Error, ll_anno, ll_riga

dw_selezione.deleterow( 1)
dw_selezione.insertrow( 0)
ll_riga = dw_selezione.getrow()
dw_selezione.setitem( ll_riga, "anno_commessa", year(today()))


dw_selezione.accepttext()
ll_anno = dw_selezione.getitemnumber( 1, "anno_commessa")
if isnull(ll_anno) or ll_anno = 0 then
	ll_anno = year(today())
end if

l_Error = dw_elenco_commesse.Retrieve( s_cs_xx.cod_azienda, ll_anno)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

public function integer wf_calcola ();integer li_risposta,li_anno_commessa
string  ls_mat_prima[],ls_cod_prodotto,ls_des_prodotto,ls_errore,ls_cod_versione, ls_tipo_calcolo
double  ldd_quan_utilizzo[],ldd_quan_prodotta,ldd_quantita_totale,ldd_costo_materia_prima, &
		  ldd_costo_totale,ldd_costo_totale_mp, ldd_costo_totale_lav,ldd_costo_totale_ru, &
		  ldd_quan_utilizzata,ldd_quan_scarto,ldd_costo_utilizzo,ldd_costo_scarto,ldd_quan_spedita,ldd_costo_preventivo, & 
		  ldd_costo_totale_preventivo,ldd_costo_totale_lav_prev,ldd_costo_totale_ru_prev
long    ll_t,ll_num_commessa





li_anno_commessa = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_anno_commessa")
ll_num_commessa = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_num_commessa")
ls_tipo_calcolo = dw_selezione.getitemstring( 1, "tipo_costo")

if li_anno_commessa = 0 or ll_num_commessa = 0 then
	g_mb.messagebox("Sep","Attenzione! Selezionare una commessa prima di calcolare il costo consuntivo.",stopsign!)
	return -1
end if	

ldd_quan_prodotta = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_quan_prodotta")

if ldd_quan_prodotta <=0 then
	g_mb.messagebox("Sep","Attenzione! Non è stata prodotta alcuna quantità della commessa selezionata",stopsign!)
	return -1
end if

//st_pezzi.text = string(ldd_quan_prodotta)

ls_cod_prodotto = dw_elenco_commesse.getitemstring(dw_elenco_commesse.getrow(),"anag_commesse_cod_prodotto")	
ls_cod_versione = dw_elenco_commesse.getitemstring(dw_elenco_commesse.getrow(),"anag_commesse_cod_versione")	

dw_report.reset()

dw_report.dataobject = "d_report_consuntivo_commessa"

dw_report.settransobject(sqlca)

dw_report.insertrow( 0)

li_risposta = f_trova_mat_prima( ls_cod_prodotto, &
                                 ls_cod_versione, &
											ls_mat_prima[], &
											ldd_quan_utilizzo[], &
											ldd_quan_prodotta, &
											li_anno_commessa, &
											ll_num_commessa)
											
// *** creo il datastore di appoggio
//datastore lds_appo
//long      ll_row
//
//lds_appo = create datastore 
//lds_appo.dataobject = "d_costi_materie_prime_cons" 
//
//lds_appo.object.data = dw_report.object.dw_costi_materie_prime.object.data


datawindowchild lds_appo, lds_fasi, lds_risorse 
long            ll_row
ll_row = dw_report.getchild("dw_costi_materie_prime", lds_appo) 
if (ll_row <> 1) then g_mb.messagebox("Error", "Error a") 

for ll_t = 1 to upperbound(ls_mat_prima)

	select sum(quan_utilizzata),
			 sum(quan_scarto)
	into   :ldd_quan_utilizzata,
			 :ldd_quan_scarto
	from   mat_prime_commessa
	where  cod_azienda = :s_cs_xx.cod_azienda and	
	       anno_commessa = :li_anno_commessa and    
			 num_commessa = :ll_num_commessa and    
			 cod_prodotto = :ls_mat_prima[ll_t];
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore nella ricerca quantità utilizzata e quantità scarto della materia prima " + ls_mat_prima[ll_t] + ".Dettaglio:" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	if isnull(ldd_quan_utilizzata) then ldd_quan_utilizzata = 0
	if isnull(ldd_quan_scarto) then ldd_quan_scarto = 0
	
	select sum(quan_spedita)
	into   :ldd_quan_spedita
	from   prod_bolle_out_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :li_anno_commessa and    
			 num_commessa = :ll_num_commessa and    
			 cod_prodotto_spedito = :ls_mat_prima[ll_t];
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore nella ricerca quantità spedita della materia prima " + ls_mat_prima[ll_t] + ".Dettaglio:" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	if isnull(ldd_quan_spedita) then 
		ldd_quan_spedita = 0
	else
		ldd_quan_utilizzata = ldd_quan_spedita
	end if
	
	ldd_quantita_totale = ldd_quan_utilizzata + ldd_quan_scarto
	
	if ls_tipo_calcolo = "A" then
		
		select des_prodotto,prezzo_acquisto
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       cod_prodotto = :ls_mat_prima[ll_t];
				 
	end if
	
	if ls_tipo_calcolo = "S" then
		
		select des_prodotto,costo_standard
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       cod_prodotto = :ls_mat_prima[ll_t];
				 
	end if
	
	if ls_tipo_calcolo = "U" then
		
		select des_prodotto,costo_ultimo
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       cod_prodotto = :ls_mat_prima[ll_t];
				 
	end if
	

 	ldd_costo_utilizzo = ldd_quan_utilizzata * ldd_costo_materia_prima
	ldd_costo_scarto = ldd_quan_scarto * ldd_costo_materia_prima 
	ldd_costo_totale = ldd_costo_materia_prima * ldd_quantita_totale
	ldd_costo_preventivo = ldd_costo_materia_prima * ldd_quan_utilizzo[ll_t]
	
	// carico il ds di appoggio
	
	ll_row = lds_appo.insertrow(0) 
	if (ll_row <= 0) then g_mb.messagebox("Error", "Error b") 
   lds_appo.setitem(ll_row,"cod_prodotto",ls_mat_prima[ll_t]) 
   lds_appo.setitem(ll_row,"des_prodotto",ls_des_prodotto)
	lds_appo.setitem(ll_row,"quan_scarto",ldd_quan_scarto)  
	lds_appo.setitem(ll_row,"quan_utilizzo",ldd_quan_utilizzata)  
	lds_appo.setitem(ll_row,"quan_totale",ldd_quantita_totale)  
	lds_appo.setitem(ll_row,"costo_unitario",ldd_costo_materia_prima)  	
	lds_appo.setitem(ll_row,"costo_scarto",ldd_costo_scarto)  	
	lds_appo.setitem(ll_row,"costo_utilizzo",ldd_costo_utilizzo)  	
	lds_appo.setitem(ll_row,"costo_totale",ldd_costo_totale)  		
	lds_appo.setitem(ll_row,"quan_utilizzo_prev",ldd_quan_utilizzo[ll_t])  		
	lds_appo.setitem(ll_row,"costo_totale_prev",ldd_costo_preventivo)  	

    	
	
//	dw_report.object.dw_costi_materie_prime.insertrow(1)
//	dw_report.object.dw_costi_materie_prime[1].setitem(1,"cod_prodotto",ls_mat_prima[ll_t])
//	dw_report.object.dw_costi_materie_prime[1].setitem(1,"des_prodotto",ls_des_prodotto)
//	dw_report.object.dw_costi_materie_prime[1].setitem(1,"quan_scarto",ldd_quan_scarto)  
//	dw_report.object.dw_costi_materie_prime[1].setitem(1,"quan_utilizzo",ldd_quan_utilizzata)  
//	dw_report.object.dw_costi_materie_prime[1].setitem(1,"quan_totale",ldd_quantita_totale)  
//	dw_report.object.dw_costi_materie_prime[1].setitem(1,"costo_unitario",ldd_costo_materia_prima)  	
//	dw_report.object.dw_costi_materie_prime[1].setitem(1,"costo_scarto",ldd_costo_scarto)  	
//	dw_report.object.dw_costi_materie_prime[1].setitem(1,"costo_utilizzo",ldd_costo_utilizzo)  	
//	dw_report.object.dw_costi_materie_prime[1].setitem(1,"costo_totale",ldd_costo_totale)  		
//	dw_report.object.dw_costi_materie_prime[1].setitem(1,"quan_utilizzo_prev",ldd_quan_utilizzo[ll_t])  		
//	dw_report.object.dw_costi_materie_prime[1].setitem(1,"costo_totale_prev",ldd_costo_preventivo)  		

next


// *** uso il datastore per le fasi di lavorazione e per le risorse umane

ll_row = dw_report.getchild("dw_costo_lavorazioni", lds_fasi) 
if (ll_row <> 1) then g_mb.messagebox("SEP", "Errore durante l'assegnazione del datastore figlio per le fasi!") 

ll_row = dw_report.getchild("dw_costo_risorse_umane", lds_risorse) 
if (ll_row <> 1) then g_mb.messagebox("SEP", "Errore durante l'assegnazione del datastore figlio per le fasi!") 

li_risposta = wf_fasi_lavorazione(li_anno_commessa,ll_num_commessa,ls_cod_prodotto,ls_cod_versione,1,ls_errore, ref lds_fasi, ref lds_risorse)

if lds_appo.rowcount() > 0 then
	ldd_costo_totale_mp =  lds_appo.getitemnumber(1,"cf_costo_totale")
end if

//if tab_1.tab_lavorazioni.dw_costo_lavorazioni.rowcount() > 0 then
//	ldd_costo_totale_lav = tab_1.tab_lavorazioni.dw_costo_lavorazioni.getitemnumber(1,"cf_costo_totale")
//	ldd_costo_totale_lav_prev = tab_1.tab_lavorazioni.dw_costo_lavorazioni.getitemnumber(1,"cf_costo_totale_prev")
//end if
//
//if tab_1.tab_risorse_umane.dw_costo_risorse_umane.rowcount() > 0 then
//	ldd_costo_totale_ru = tab_1.tab_risorse_umane.dw_costo_risorse_umane.getitemnumber(1,"cf_costo_totale")
//	ldd_costo_totale_ru_prev = tab_1.tab_risorse_umane.dw_costo_risorse_umane.getitemnumber(1,"cf_costo_totale_prev")
//end if

//ldd_costo_totale_preventivo = tab_1.tab_materie_prime.dw_costi_materie_prime.getitemnumber(1,"cf_costo_totale_preventivo")

//if not(tab_1.tab_risorse_umane.cbx_tempo_risorsa_umana.checked) then 
//	ldd_costo_totale_ru = 0
//	ldd_costo_totale_ru_prev = 0
//end if
//
//ldd_costo_totale_preventivo = ldd_costo_totale_preventivo + ldd_costo_totale_lav_prev +ldd_costo_totale_ru_prev
//
//st_costo_totale_complessivo.text = string(ldd_costo_totale_mp + ldd_costo_totale_lav + ldd_costo_totale_ru, "[currency]")
//st_costo_totale_complessivo_preventivo.text = string(ldd_costo_totale_preventivo, "[currency]")
//
//if ldd_costo_totale_preventivo <> 0 then
//	st_delta.text = string(truncate(((ldd_costo_totale_mp + ldd_costo_totale_lav + ldd_costo_totale_ru - ldd_costo_totale_preventivo)/ldd_costo_totale_preventivo)*100,1))
//end if
return 0
end function

public function integer wf_fasi_lavorazione (integer fi_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, decimal fdd_quantita_richiesta, ref string fs_errore, ref datawindowchild fds_fasi, ref datawindowchild fds_risorse);// Funzione che calcola i costi consuntivi di lavorazione
// nome: wf_fasi_lavorazione
// tipo: integer
//       -1 failed
//  		 0 passed
//
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							 fi_anno_commessa			 integer			valore
//                    fl_num_commessa			 long				valore
//							 fs_cod_prodotto	 		 string  		valore
//							 fdd_quantita_richiesta	 double			valore
//
//		Creata il 24-07-97 
//		Autore Diego Ferrari

string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, & 
			 ls_cod_lavorazione,ls_test,ls_cod_cat_attrezzature,ls_des_cat_attrezzature, &
			 ls_cod_cat_attrezzature_2,ls_des_cat_attrezzature_2,ls_flag_materia_prima,ls_cod_prodotto_inserito,&
			 ls_cod_prodotto_variante,ls_flag_materia_prima_variante
long      ll_num_righe,ll_num_figli, ll_riga
integer   li_risposta
dec{4}    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione, ldd_tempo_totale, ldd_costo_medio_orario, ldd_costo_totale, &
			 ldd_tempo_risorsa_umana,ldd_costo_medio_orario_2,ldd_costo_totale_2,ldd_quan_prodotta, & 
			 ldd_quan_utilizzo_variante,ldd_tempo_attrezzaggio_prev,ldd_tempo_attrezzaggio_commessa_prev, & 
			 ldd_tempo_lavorazione_prev, ldd_tempo_risorsa_umana_prev,ldd_tempo_totale_prev,ldd_costo_prev, & 
			 ldd_quan_prodotta_fase, ldd_costo_totale_2_prev

datastore lds_righe_distinta

ldd_quan_prodotta = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_quan_prodotta")

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fdd_quantita_richiesta
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	
//****************************
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio

	select cod_prodotto,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :fi_anno_commessa and    
			 num_commessa = :fl_num_commessa and    
			 cod_prodotto_padre = :fs_cod_prodotto and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and    
			 cod_versione = :fs_cod_versione;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante * fdd_quantita_richiesta
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	if ls_flag_materia_prima = 'N' then
		
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and 	 
		       cod_prodotto_padre = :ls_cod_prodotto_inserito;
		
		if sqlca.sqlcode = 100 then continue
		
	else
		continue
	end if

//*************************
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and 	 
	       cod_prodotto_padre = :ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode = 100 then continue

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 cod_cat_attrezzature_2,
			 tempo_lavorazione,
			 tempo_attrezzaggio,
			 tempo_attrezzaggio_commessa,
			 tempo_risorsa_umana
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_lavorazione_prev,
			 :ldd_tempo_attrezzaggio_prev,
			 :ldd_tempo_attrezzaggio_commessa_prev,
			 :ldd_tempo_risorsa_umana_prev
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda  
	AND   cod_prodotto = :ls_cod_prodotto_figlio;

	if sqlca.sqlcode=100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		return -1
	end if
  
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto = :ls_cod_prodotto_figlio;

	select des_cat_attrezzature,
		    costo_medio_orario
	into   :ls_des_cat_attrezzature,
			 :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature;

	select des_cat_attrezzature,
			 costo_medio_orario
	into   :ls_des_cat_attrezzature_2,
			 :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature_2;
	
	select sum(tempo_attrezzaggio),
			 sum(tempo_attrezzaggio_commessa),
		    sum(tempo_lavorazione),
			 sum(tempo_risorsa_umana),
			 sum(quan_prodotta)
	into   :ldd_tempo_attrezzaggio,
			 :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ldd_tempo_risorsa_umana,
			 :ldd_quan_prodotta_fase
	from   avan_produzione_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :fi_anno_commessa and    
			 num_commessa = :fl_num_commessa	and    
			 cod_prodotto = :ls_cod_prodotto_figlio and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 cod_reparto = :ls_cod_reparto;


	if isnull(ldd_tempo_attrezzaggio) then ldd_tempo_attrezzaggio = 0
	
	if isnull(ldd_tempo_attrezzaggio_commessa) then ldd_tempo_attrezzaggio_commessa = 0
	
	if isnull(ldd_tempo_lavorazione) then ldd_tempo_lavorazione = 0
	
	if isnull(ldd_tempo_risorsa_umana) then ldd_tempo_risorsa_umana = 0
	
	if isnull(ldd_tempo_attrezzaggio_prev) then ldd_tempo_attrezzaggio_prev = 0
	
	if isnull(ldd_tempo_attrezzaggio_commessa_prev) then ldd_tempo_attrezzaggio_commessa_prev = 0
	
	if isnull(ldd_tempo_lavorazione_prev) then ldd_tempo_lavorazione_prev = 0
	
	if isnull(ldd_tempo_risorsa_umana_prev) then ldd_tempo_risorsa_umana_prev = 0
	

	ldd_tempo_attrezzaggio_prev = ldd_tempo_attrezzaggio_prev
	
	ldd_tempo_attrezzaggio_commessa_prev = ldd_tempo_attrezzaggio_commessa_prev

	ldd_tempo_lavorazione_prev = ldd_tempo_lavorazione_prev * ldd_quan_prodotta_fase
	
	ldd_tempo_risorsa_umana_prev = ldd_tempo_risorsa_umana_prev * ldd_quan_prodotta_fase

	ldd_tempo_totale = 0
	ldd_tempo_totale_prev = 0
	
	ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio
	ldd_tempo_totale_prev = ldd_tempo_totale_prev + ldd_tempo_attrezzaggio_prev

	ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio_commessa
	ldd_tempo_totale_prev = ldd_tempo_totale_prev + ldd_tempo_attrezzaggio_commessa_prev


	ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_lavorazione
	ldd_tempo_totale_prev = ldd_tempo_totale_prev + ldd_tempo_lavorazione_prev


	ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
	ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2

	ldd_costo_totale_2_prev = (ldd_tempo_risorsa_umana_prev/60) * ldd_costo_medio_orario_2
	ldd_costo_prev = (ldd_tempo_totale_prev/60) * ldd_costo_medio_orario

	
   ll_riga = fds_fasi.insertrow( 1)
	fds_fasi.setitem(1,"num_pezzi",ldd_quan_prodotta)
   fds_fasi.setitem(1,"cod_prodotto",ls_cod_prodotto_figlio)
 	fds_fasi.setitem(1,"des_prodotto",ls_des_prodotto)
 	fds_fasi.setitem(1,"cod_lavorazione",ls_cod_lavorazione)
	fds_fasi.setitem(1,"cod_cat_attrezzature",ls_cod_cat_attrezzature)
	fds_fasi.setitem(1,"des_cat_attrezzature",ls_des_cat_attrezzature)
	fds_fasi.setitem(1,"tempo_attrezzaggio",ldd_tempo_attrezzaggio)
	fds_fasi.setitem(1,"tempo_attrezzaggio_commessa",ldd_tempo_attrezzaggio_commessa)
	fds_fasi.setitem(1,"tempo_lavorazione",ldd_tempo_lavorazione)
	fds_fasi.setitem(1,"tempo_totale",ldd_tempo_totale)
	fds_fasi.setitem(1,"tempo_attrezzaggio_prev",ldd_tempo_attrezzaggio_prev)
	fds_fasi.setitem(1,"tempo_attrezzaggio_com_prev",ldd_tempo_attrezzaggio_commessa_prev)
	fds_fasi.setitem(1,"tempo_lavorazione_prev",ldd_tempo_lavorazione_prev)
	fds_fasi.setitem(1,"tempo_totale_prev",ldd_tempo_totale_prev)
	fds_fasi.setitem(1,"costo_totale_prev",ldd_costo_prev)
	fds_fasi.setitem(1,"costo_medio_orario",ldd_costo_medio_orario)  
	fds_fasi.setitem(1,"costo_totale",ldd_costo_totale)  	
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"num_pezzi",ldd_quan_prodotta)
//   dw_report.object.dw_costo_lavorazioni.setitem(1,"cod_prodotto",ls_cod_prodotto_figlio)
// 	dw_report.object.dw_costo_lavorazioni.setitem(1,"des_prodotto",ls_des_prodotto)
// 	dw_report.object.dw_costo_lavorazioni.setitem(1,"cod_lavorazione",ls_cod_lavorazione)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"cod_cat_attrezzature",ls_cod_cat_attrezzature)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"des_cat_attrezzature",ls_des_cat_attrezzature)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"tempo_attrezzaggio",ldd_tempo_attrezzaggio)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"tempo_attrezzaggio_commessa",ldd_tempo_attrezzaggio_commessa)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"tempo_lavorazione",ldd_tempo_lavorazione)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"tempo_totale",ldd_tempo_totale)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"tempo_attrezzaggio_prev",ldd_tempo_attrezzaggio_prev)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"tempo_attrezzaggio_com_prev",ldd_tempo_attrezzaggio_commessa_prev)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"tempo_lavorazione_prev",ldd_tempo_lavorazione_prev)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"tempo_totale_prev",ldd_tempo_totale_prev)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"costo_totale_prev",ldd_costo_prev)
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"costo_medio_orario",ldd_costo_medio_orario)  
//	dw_report.object.dw_costo_lavorazioni.setitem(1,"costo_totale",ldd_costo_totale)  
	
   fds_risorse.insertrow(1)
	fds_risorse.setitem(1,"cod_cat_attrezzature",ls_cod_cat_attrezzature_2)
	fds_risorse.setitem(1,"des_cat_attrezzature",ls_des_cat_attrezzature_2)
	fds_risorse.setitem(1,"tempo_totale",ldd_tempo_risorsa_umana)
	fds_risorse.setitem(1,"tempo_totale_prev",ldd_tempo_risorsa_umana_prev)
	fds_risorse.setitem(1,"costo_medio_orario",ldd_costo_medio_orario_2)  
	fds_risorse.setitem(1,"costo_totale",ldd_costo_totale_2)  
	fds_risorse.setitem(1,"costo_totale_prev",ldd_costo_totale_2_prev) 	
	
//   dw_report.object.dw_costo_risorse_umane.insertrow(1)
//	dw_report.object.dw_costo_risorse_umane.setitem(1,"cod_cat_attrezzature",ls_cod_cat_attrezzature_2)
//	dw_report.object.dw_costo_risorse_umane.setitem(1,"des_cat_attrezzature",ls_des_cat_attrezzature_2)
//	dw_report.object.dw_costo_risorse_umane.setitem(1,"tempo_totale",ldd_tempo_risorsa_umana)
//	dw_report.object.dw_costo_risorse_umane.setitem(1,"tempo_totale_prev",ldd_tempo_risorsa_umana_prev)
//	dw_report.object.dw_costo_risorse_umane.setitem(1,"costo_medio_orario",ldd_costo_medio_orario_2)  
//	dw_report.object.dw_costo_risorse_umane.setitem(1,"costo_totale",ldd_costo_totale_2)  
//	dw_report.object.dw_costo_risorse_umane.setitem(1,"costo_totale_prev",ldd_costo_totale_2_prev)  

	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
	       cod_prodotto_padre = :ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta = wf_fasi_lavorazione(fi_anno_commessa,fl_num_commessa,ls_cod_prodotto_figlio,fs_cod_versione,ldd_quan_utilizzo, ls_errore, ref fds_fasi, ref fds_risorse)
		if li_risposta = -1 then
			fs_errore = ls_errore
			return -1
		end if
	end if
	
next

return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo, ls_modify
windowobject l_objects[ ]

set_w_options(c_closenosave + c_autoposition)

dw_elenco_commesse.settransobject(sqlca)

dw_report.ib_dw_report=true

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report


l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_cerca
l_objects[3] = dw_elenco_commesse
l_objects[4] = st_1
l_objects[5] = cb_calcola
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

postevent("ue_inizio")
end event

on w_costo_consuntivo_commessa_new.create
int iCurrent
call super::create
this.cb_calcola=create cb_calcola
this.st_1=create st_1
this.cb_cerca=create cb_cerca
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_elenco_commesse=create dw_elenco_commesse
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_calcola
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.cb_cerca
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.dw_folder
this.Control[iCurrent+7]=this.dw_elenco_commesse
end on

on w_costo_consuntivo_commessa_new.destroy
call super::destroy
destroy(this.cb_calcola)
destroy(this.st_1)
destroy(this.cb_cerca)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_elenco_commesse)
end on

type cb_calcola from commandbutton within w_costo_consuntivo_commessa_new
integer x = 3337
integer y = 140
integer width = 343
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcola"
end type

event clicked;dw_report.setredraw( false)
wf_calcola()
dw_report.setredraw( true)
dw_report.change_dw_current()
dw_folder.fu_selecttab(2)
end event

type st_1 from statictext within w_costo_consuntivo_commessa_new
integer x = 69
integer y = 2100
integer width = 3616
integer height = 240
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Attenzione il consuntivo dei costi di commessa può essere fatto solo per le commesse con l~'avanzamento di produzione completo.Per quelle automatiche il consuntivo coinciderebbe con il preventivo dei costi di produzione."
boolean focusrectangle = false
end type

type cb_cerca from commandbutton within w_costo_consuntivo_commessa_new
integer x = 663
integer y = 140
integer width = 91
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C"
end type

event clicked;LONG  l_Error, ll_anno

dw_selezione.accepttext()
ll_anno = dw_selezione.getitemnumber( 1, "anno_commessa")
if isnull(ll_anno) or ll_anno = 0 then
	ll_anno = year(today())
end if

l_Error = dw_elenco_commesse.Retrieve( s_cs_xx.cod_azienda, ll_anno)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_selezione from uo_cs_xx_dw within w_costo_consuntivo_commessa_new
integer x = 23
integer y = 120
integer width = 3657
integer height = 120
integer taborder = 20
string dataobject = "d_costo_consuntivo_comm_sel"
boolean border = false
end type

event itemchanged;call super::itemchanged;//string ls_cod_divisione, ls_cod_area_aziendale
//
//dw_selezione.accepttext()
//choose case i_colname
//	case "rs_cod_divisione"
//		ls_cod_divisione = dw_selezione.getitemstring(1,"rs_cod_divisione")		
//		if isnull(ls_cod_divisione) then
//			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
//						  "tab_aree_aziendali","cod_area_aziendale","des_area", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
//		else			  
//			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_area_aziendale",sqlca,&
//						  "tab_aree_aziendali","cod_area_aziendale","des_area", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + i_coltext + "'")					  					  
//		end if				  
//		
//	case "rs_cod_area_aziendale"
//		ls_cod_area_aziendale = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")		
//		if isnull(ls_cod_area_aziendale) then
//			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
//						  "anag_reparti","cod_reparto","des_reparto", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
//		else						  
//			f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_reparto",sqlca,&
//						  "anag_reparti","cod_reparto","des_reparto", &
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_area_aziendale = '" + i_coltext + "'")						
//		end if	
//end choose

end event

type dw_report from uo_cs_xx_dw within w_costo_consuntivo_commessa_new
integer x = 23
integer y = 128
integer width = 3680
integer height = 2248
integer taborder = 10
string dataobject = "d_report_consuntivo_commessa"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_costo_consuntivo_commessa_new
integer x = 9
integer y = 12
integer width = 3707
integer height = 2372
integer taborder = 40
boolean border = false
end type

type dw_elenco_commesse from datawindow within w_costo_consuntivo_commessa_new
integer x = 46
integer y = 260
integer width = 3634
integer height = 1820
integer taborder = 50
string title = "none"
string dataobject = "d_elenco_commesse"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event rowfocuschanged;scrolltorow(currentrow)
setrow(currentrow)
end event

event rowfocuschanging;SelectRow( currentrow, FALSE)
SelectRow( newrow, TRUE)
end event

