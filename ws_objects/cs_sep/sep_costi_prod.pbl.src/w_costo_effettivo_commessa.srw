﻿$PBExportHeader$w_costo_effettivo_commessa.srw
$PBExportComments$Window Costo Effettivo Commessa (tiene conto degli ordini di acquisto legati alla commessa)
forward
global type w_costo_effettivo_commessa from w_cs_xx_principale
end type
type cb_calcola from commandbutton within w_costo_effettivo_commessa
end type
type dw_elenco_commesse from uo_cs_xx_dw within w_costo_effettivo_commessa
end type
type tab_1 from tab within w_costo_effettivo_commessa
end type
type tab_acquisti from userobject within tab_1
end type
type dw_costi_acquisto from datawindow within tab_acquisti
end type
type tab_acquisti from userobject within tab_1
dw_costi_acquisto dw_costi_acquisto
end type
type tab_lavorazioni from userobject within tab_1
end type
type cbx_tempo_lavorazione from checkbox within tab_lavorazioni
end type
type cbx_tempo_attrezzaggio from checkbox within tab_lavorazioni
end type
type cbx_tempo_attrezzaggio_commessa from checkbox within tab_lavorazioni
end type
type dw_costo_lavorazioni from datawindow within tab_lavorazioni
end type
type tab_lavorazioni from userobject within tab_1
cbx_tempo_lavorazione cbx_tempo_lavorazione
cbx_tempo_attrezzaggio cbx_tempo_attrezzaggio
cbx_tempo_attrezzaggio_commessa cbx_tempo_attrezzaggio_commessa
dw_costo_lavorazioni dw_costo_lavorazioni
end type
type tab_risorse_umane from userobject within tab_1
end type
type dw_costo_risorse_umane from datawindow within tab_risorse_umane
end type
type cbx_tempo_risorsa_umana from checkbox within tab_risorse_umane
end type
type tab_risorse_umane from userobject within tab_1
dw_costo_risorse_umane dw_costo_risorse_umane
cbx_tempo_risorsa_umana cbx_tempo_risorsa_umana
end type
type tabpage_costi_correttivi from userobject within tab_1
end type
type cbx_particolare from checkbox within tabpage_costi_correttivi
end type
type dw_costi_correttivi from datawindow within tabpage_costi_correttivi
end type
type tabpage_costi_correttivi from userobject within tab_1
cbx_particolare cbx_particolare
dw_costi_correttivi dw_costi_correttivi
end type
type tabpage_1 from userobject within tab_1
end type
type st_4 from statictext within tabpage_1
end type
type st_5 from statictext within tabpage_1
end type
type st_6 from statictext within tabpage_1
end type
type tabpage_1 from userobject within tab_1
st_4 st_4
st_5 st_5
st_6 st_6
end type
type tab_1 from tab within w_costo_effettivo_commessa
tab_acquisti tab_acquisti
tab_lavorazioni tab_lavorazioni
tab_risorse_umane tab_risorse_umane
tabpage_costi_correttivi tabpage_costi_correttivi
tabpage_1 tabpage_1
end type
type st_1 from statictext within w_costo_effettivo_commessa
end type
type st_costo_totale_complessivo from statictext within w_costo_effettivo_commessa
end type
type st_2 from statictext within w_costo_effettivo_commessa
end type
type st_pezzi from statictext within w_costo_effettivo_commessa
end type
type em_anno from editmask within w_costo_effettivo_commessa
end type
type st_3 from statictext within w_costo_effettivo_commessa
end type
type cb_aggiorna from commandbutton within w_costo_effettivo_commessa
end type
end forward

global type w_costo_effettivo_commessa from w_cs_xx_principale
integer width = 3689
integer height = 1964
string title = "Costo Effettivo Commessa"
cb_calcola cb_calcola
dw_elenco_commesse dw_elenco_commesse
tab_1 tab_1
st_1 st_1
st_costo_totale_complessivo st_costo_totale_complessivo
st_2 st_2
st_pezzi st_pezzi
em_anno em_anno
st_3 st_3
cb_aggiorna cb_aggiorna
end type
global w_costo_effettivo_commessa w_costo_effettivo_commessa

forward prototypes
public function integer wf_fasi_lavorazione (integer fi_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, string fs_errore)
public function integer wf_acquisti (integer fi_anno_commessa, long fl_num_commessa)
end prototypes

public function integer wf_fasi_lavorazione (integer fi_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, string fs_errore);// Funzione che calcola i costi consuntivi di lavorazione
// nome: wf_fasi_lavorazione
// tipo: integer
//       -1 failed
//  		 0 passed
//
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							 fi_anno_commessa			 integer			valore
//                    fl_num_commessa			 long				valore
//							 fs_cod_prodotto	 		 string  		valore
//							 fdd_quantita_richiesta	 double			valore
//
//		Creata il 24-07-97 
//		Autore Diego Ferrari

string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, & 
			 ls_cod_lavorazione,ls_test,ls_cod_cat_attrezzature,ls_des_cat_attrezzature, &
			 ls_cod_cat_attrezzature_2,ls_des_cat_attrezzature_2,ls_flag_materia_prima,ls_cod_prodotto_inserito,&
			 ls_cod_prodotto_variante,ls_flag_materia_prima_variante
long      ll_num_righe,ll_num_figli
integer   li_risposta
dec{4}    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione, ldd_tempo_totale, ldd_costo_medio_orario, ldd_costo_totale, &
			 ldd_tempo_risorsa_umana,ldd_costo_medio_orario_2,ldd_costo_totale_2,ldd_quan_prodotta, & 
			 ldd_quan_utilizzo_variante,ldd_tempo_attrezzaggio_commessa_prev, & 
			 ldd_tempo_lavorazione_prev, ldd_tempo_risorsa_umana_prev,ldd_quan_prodotta_fase

datastore lds_righe_distinta

ldd_quan_prodotta = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_quan_prodotta")

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fdd_quantita_richiesta
	ls_flag_materia_prima=lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	
//****************************
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio

	select cod_prodotto,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:fi_anno_commessa
	and    num_commessa=:fl_num_commessa
	and    cod_prodotto_padre=:fs_cod_prodotto
	and    cod_prodotto_figlio=:ls_cod_prodotto_figlio
	and    cod_versione=:fs_cod_versione;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante* fdd_quantita_richiesta
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	if ls_flag_materia_prima = 'N' then
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda=:s_cs_xx.cod_azienda 
		and 	 cod_prodotto_padre=:ls_cod_prodotto_inserito;
		
		if sqlca.sqlcode=100 then continue
	else
		continue
	end if

//*************************
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda=:s_cs_xx.cod_azienda 
	and 	 cod_prodotto_padre=:ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode=100 then continue

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 cod_cat_attrezzature_2
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
			 :ls_cod_cat_attrezzature_2
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda  
	AND   cod_prodotto = :ls_cod_prodotto_figlio;

	if sqlca.sqlcode=100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		return -1
	end if
  
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto =:ls_cod_prodotto_figlio;

	select des_cat_attrezzature,
		    costo_medio_orario
	into   :ls_des_cat_attrezzature,
			 :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;

	select des_cat_attrezzature,
			 costo_medio_orario
	into   :ls_des_cat_attrezzature_2,
			 :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature_2;
	
	select sum(tempo_attrezzaggio),
			 sum(tempo_attrezzaggio_commessa),
		    sum(tempo_lavorazione),
			 sum(tempo_risorsa_umana),
			 sum(quan_prodotta)
	into   :ldd_tempo_attrezzaggio,
			 :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ldd_tempo_risorsa_umana,
			 :ldd_quan_prodotta_fase
	from   avan_produzione_com
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:fi_anno_commessa
	and    num_commessa =:fl_num_commessa	
	and    cod_prodotto =:ls_cod_prodotto_figlio
	and    cod_lavorazione=:ls_cod_lavorazione
	and    cod_reparto=:ls_cod_reparto;


	if isnull(ldd_tempo_attrezzaggio) then ldd_tempo_attrezzaggio = 0
	
	if isnull(ldd_tempo_attrezzaggio_commessa) then ldd_tempo_attrezzaggio_commessa = 0
	
	if isnull(ldd_tempo_lavorazione) then ldd_tempo_lavorazione = 0
	
	if isnull(ldd_tempo_risorsa_umana) then ldd_tempo_risorsa_umana = 0
	
	ldd_tempo_totale = 0
	
	if tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio
	
	end if

	if tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio_commessa.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio_commessa
	
	end if

	if tab_1.tab_lavorazioni.cbx_tempo_lavorazione.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_lavorazione
	
	end if

	ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
	ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2

	
	
   tab_1.tab_lavorazioni.dw_costo_lavorazioni.insertrow(1)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"num_pezzi",ldd_quan_prodotta)
   tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"cod_prodotto",ls_cod_prodotto_figlio)
 	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"des_prodotto",ls_des_prodotto)
 	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"cod_lavorazione",ls_cod_lavorazione)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"cod_cat_attrezzature",ls_cod_cat_attrezzature)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"des_cat_attrezzature",ls_des_cat_attrezzature)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_attrezzaggio",round(ldd_tempo_attrezzaggio/60,4))
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_attrezzaggio_commessa",round(ldd_tempo_attrezzaggio_commessa/60,4))
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_lavorazione",round(ldd_tempo_lavorazione/60,4))
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"tempo_totale",round(ldd_tempo_totale/60,4))
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"costo_medio_orario",ldd_costo_medio_orario)  
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem(1,"costo_totale",ldd_costo_totale)  
	
   tab_1.tab_risorse_umane.dw_costo_risorse_umane.insertrow(1)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"cod_cat_attrezzature",ls_cod_cat_attrezzature_2)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"des_cat_attrezzature",ls_des_cat_attrezzature_2)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"tempo_totale",round(ldd_tempo_risorsa_umana/60,4))
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"costo_medio_orario",ldd_costo_medio_orario_2)  
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem(1,"costo_totale",ldd_costo_totale_2)  
	
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta=wf_fasi_lavorazione(fi_anno_commessa,fl_num_commessa,ls_cod_prodotto_figlio,fs_cod_versione,ldd_quan_utilizzo,ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

return 0
end function

public function integer wf_acquisti (integer fi_anno_commessa, long fl_num_commessa);// calcola tutti i costi di acquisto legati alla commessa

integer li_anno_registrazione
long    ll_num_registrazione,ll_prog_riga_ordine_acq,ll_riga
dec{4}  ld_quan_ordinata,ld_val_riga,ld_prezzo_acquisto,ld_tot_quan_arrivata,ld_sconto_1,ld_sconto_2,ld_sconto_3,ld_sconto_4,ld_sconto_5,ld_sconto_6,ld_sconto_7, &
		  ld_sconto_8,ld_sconto_9,ld_sconto_10
string  ls_cod_prodotto,ls_des_prodotto,ls_des_prodotto_anag


declare r_det_ord_acq cursor for
select anno_registrazione,
		 num_registrazione,
		 prog_riga_ordine_acq,
		 cod_prodotto,
		 quan_ordinata,
		 prezzo_acquisto,
		 des_prodotto,
		 sconto_1,
 		 sconto_2,
		 sconto_3,
		 sconto_4,
		 sconto_5,
  		 sconto_6,
		 sconto_7,
		 sconto_8,
  		 sconto_9,
		 sconto_10
from   det_ord_acq
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:fi_anno_commessa
and    num_commessa=:fl_num_commessa;

open r_det_ord_acq;

do while 1 = 1
	fetch r_det_ord_acq
	into  :li_anno_registrazione,
			:ll_num_registrazione,
			:ll_prog_riga_ordine_acq,
			:ls_cod_prodotto,
			:ld_quan_ordinata,
			:ld_prezzo_acquisto,
			:ls_des_prodotto,
			:ld_sconto_1,
			:ld_sconto_2,
			:ld_sconto_3,
			:ld_sconto_4,
			:ld_sconto_5,
			:ld_sconto_6,
			:ld_sconto_7,
			:ld_sconto_8,
			:ld_sconto_9,
			:ld_sconto_10;
			
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode< 0 then 
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_det_ord_acq;
		return -1
	end if

	if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		select des_prodotto
		into   :ls_des_prodotto_anag
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;
		
		if sqlca.sqlcode< 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_ord_acq;
			return -1
		end if
	end if
	
	if isnull(ls_des_prodotto) or ls_des_prodotto="" then ls_des_prodotto = ls_des_prodotto_anag
	
	select sum(quan_arrivata)
	into   :ld_tot_quan_arrivata
	from   det_bol_acq
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and  	 prog_riga_ordine_acq=:ll_prog_riga_ordine_acq;
	
	if sqlca.sqlcode< 0 then 
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_det_ord_acq;
		return -1
	end if
	
	//ld_val_riga= ld_prezzo_acquisto * ld_quan_ordinata
	
	ld_sconto_1 = ld_sconto_1/100
	ld_sconto_2 = ld_sconto_2/100
	ld_sconto_3 = ld_sconto_3/100
	ld_sconto_4 = ld_sconto_4/100
	ld_sconto_5 = ld_sconto_5/100
	ld_sconto_6 = ld_sconto_6/100
	ld_sconto_7 = ld_sconto_7/100
	ld_sconto_8 = ld_sconto_8/100
	ld_sconto_9 = ld_sconto_9/100
	ld_sconto_10 = ld_sconto_10/100
	
	ld_prezzo_acquisto = ld_prezzo_acquisto - ld_prezzo_acquisto*ld_sconto_1
	ld_prezzo_acquisto = ld_prezzo_acquisto - ld_prezzo_acquisto*ld_sconto_2
	ld_prezzo_acquisto = ld_prezzo_acquisto - ld_prezzo_acquisto*ld_sconto_3
	ld_prezzo_acquisto = ld_prezzo_acquisto - ld_prezzo_acquisto*ld_sconto_4
	ld_prezzo_acquisto = ld_prezzo_acquisto - ld_prezzo_acquisto*ld_sconto_5
	ld_prezzo_acquisto = ld_prezzo_acquisto - ld_prezzo_acquisto*ld_sconto_6
	ld_prezzo_acquisto = ld_prezzo_acquisto - ld_prezzo_acquisto*ld_sconto_7
	ld_prezzo_acquisto = ld_prezzo_acquisto - ld_prezzo_acquisto*ld_sconto_8
	ld_prezzo_acquisto = ld_prezzo_acquisto - ld_prezzo_acquisto*ld_sconto_9
	ld_prezzo_acquisto = ld_prezzo_acquisto - ld_prezzo_acquisto*ld_sconto_10
	
	ld_val_riga= ld_prezzo_acquisto * ld_tot_quan_arrivata
	
	ll_riga = tab_1.tab_acquisti.dw_costi_acquisto.insertrow(0)
	tab_1.tab_acquisti.dw_costi_acquisto.setitem(ll_riga,"anno_registrazione",li_anno_registrazione)
	tab_1.tab_acquisti.dw_costi_acquisto.setitem(ll_riga,"num_registrazione",ll_num_registrazione)
	tab_1.tab_acquisti.dw_costi_acquisto.setitem(ll_riga,"prog_riga_ordine",ll_prog_riga_ordine_acq)
	tab_1.tab_acquisti.dw_costi_acquisto.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
	tab_1.tab_acquisti.dw_costi_acquisto.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
	tab_1.tab_acquisti.dw_costi_acquisto.setitem(ll_riga,"quan_ordine",ld_tot_quan_arrivata)
	tab_1.tab_acquisti.dw_costi_acquisto.setitem(ll_riga,"val_riga",ld_val_riga)
	
loop

close r_det_ord_acq;

return 0
end function

on w_costo_effettivo_commessa.create
int iCurrent
call super::create
this.cb_calcola=create cb_calcola
this.dw_elenco_commesse=create dw_elenco_commesse
this.tab_1=create tab_1
this.st_1=create st_1
this.st_costo_totale_complessivo=create st_costo_totale_complessivo
this.st_2=create st_2
this.st_pezzi=create st_pezzi
this.em_anno=create em_anno
this.st_3=create st_3
this.cb_aggiorna=create cb_aggiorna
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_calcola
this.Control[iCurrent+2]=this.dw_elenco_commesse
this.Control[iCurrent+3]=this.tab_1
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.st_costo_totale_complessivo
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.st_pezzi
this.Control[iCurrent+8]=this.em_anno
this.Control[iCurrent+9]=this.st_3
this.Control[iCurrent+10]=this.cb_aggiorna
end on

on w_costo_effettivo_commessa.destroy
call super::destroy
destroy(this.cb_calcola)
destroy(this.dw_elenco_commesse)
destroy(this.tab_1)
destroy(this.st_1)
destroy(this.st_costo_totale_complessivo)
destroy(this.st_2)
destroy(this.st_pezzi)
destroy(this.em_anno)
destroy(this.st_3)
destroy(this.cb_aggiorna)
end on

event pc_setwindow;call super::pc_setwindow;dw_elenco_commesse.set_dw_options(sqlca,pcca.null_object,c_nonew + & 
													 c_nodelete + c_nomodify +  & 
													 c_disablecc,c_disableccinsert)
													 
tab_1.tabpage_costi_correttivi.dw_costi_correttivi.settransobject(sqlca)

em_anno.text=string(year(today()))
end event

type cb_calcola from commandbutton within w_costo_effettivo_commessa
integer x = 846
integer y = 1760
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcola"
end type

event clicked;// formato che può essere utile #,0#.##
integer li_risposta,li_anno_commessa
string  ls_mat_prima[],ls_cod_prodotto,ls_des_prodotto,ls_errore,ls_cod_versione, ls_particolare
dec{4}  ldd_quan_utilizzo[],ldd_quan_prodotta,ldd_quantita_totale, &
		  ldd_costo_totale,ldd_costo_totale_acquisti, ldd_costo_totale_lav,ldd_costo_totale_ru, &
		  ldd_quan_utilizzata,ldd_quan_scarto,ldd_costo_utilizzo,ldd_costo_scarto, ld_costi_correttivi

long    ll_t,ll_num_commessa

li_anno_commessa = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_anno_commessa")
ll_num_commessa = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_num_commessa")

if li_anno_commessa=0 or ll_num_commessa=0 then
	g_mb.messagebox("Sep","Attenzione! Selezionare una commessa prima di calcolare il costo consuntivo.",stopsign!)
	return
end if	

ldd_quan_prodotta = dw_elenco_commesse.getitemnumber(dw_elenco_commesse.getrow(),"anag_commesse_quan_prodotta")

if ldd_quan_prodotta <=0 then
	g_mb.messagebox("Sep","Attenzione! Non è stata prodotta alcuna quantità della commessa selezionata",stopsign!)
	return
end if

st_pezzi.text=string(ldd_quan_prodotta, "#,##0.00")

ls_cod_prodotto = dw_elenco_commesse.getitemstring(dw_elenco_commesse.getrow(),"anag_commesse_cod_prodotto")	
ls_cod_versione = dw_elenco_commesse.getitemstring(dw_elenco_commesse.getrow(),"anag_commesse_cod_versione")	

tab_1.tab_acquisti.dw_costi_acquisto.reset()
tab_1.tab_lavorazioni.dw_costo_lavorazioni.reset()
tab_1.tab_risorse_umane.dw_costo_risorse_umane.reset()

if tab_1.tabpage_costi_correttivi.cbx_particolare.checked then
	ls_particolare = "N"
else
	ls_particolare = "S"
end if
	
li_risposta=wf_acquisti(li_anno_commessa,ll_num_commessa)
li_risposta=wf_fasi_lavorazione(li_anno_commessa,ll_num_commessa,ls_cod_prodotto,ls_cod_versione,1,ls_errore)
tab_1.tabpage_costi_correttivi.dw_costi_correttivi.retrieve(s_cs_xx.cod_azienda,li_anno_commessa,ll_num_commessa,ls_particolare)

if tab_1.tab_acquisti.dw_costi_acquisto.rowcount() > 0 then
	ldd_costo_totale_acquisti =  tab_1.tab_acquisti.dw_costi_acquisto.getitemnumber(1,"cf_costo_totale")
end if

if tab_1.tab_lavorazioni.dw_costo_lavorazioni.rowcount() > 0 then
	ldd_costo_totale_lav = tab_1.tab_lavorazioni.dw_costo_lavorazioni.getitemnumber(1,"cf_costo_totale")

end if

if tab_1.tab_risorse_umane.dw_costo_risorse_umane.rowcount() > 0 then
	ldd_costo_totale_ru = tab_1.tab_risorse_umane.dw_costo_risorse_umane.getitemnumber(1,"cf_costo_totale")

end if

if not(tab_1.tab_risorse_umane.cbx_tempo_risorsa_umana.checked) then 
	ldd_costo_totale_ru = 0
end if

if tab_1.tabpage_costi_correttivi.dw_costi_correttivi.rowcount() > 0 then
	ld_costi_correttivi = tab_1.tabpage_costi_correttivi.dw_costi_correttivi.getitemnumber(1,"cf_costo_totale")
end if

if isnull(ld_costi_correttivi) then
	ld_costi_correttivi = 0
end if

st_costo_totale_complessivo.text = string(ldd_costo_totale_acquisti+ldd_costo_totale_lav+ldd_costo_totale_ru+ld_costi_correttivi, "[currency]")
end event

type dw_elenco_commesse from uo_cs_xx_dw within w_costo_effettivo_commessa
event pcd_pickedrow pbm_custom55
event pcd_retrieve pbm_custom60
integer x = 23
integer y = 20
integer width = 800
integer height = 1720
integer taborder = 10
string dataobject = "d_elenco_commesse"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,long(em_anno.text))

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event rowfocuschanged;call super::rowfocuschanged;//if dw_elenco_commesse.rowcount() > 0 then
//	cb_calcola.triggerevent(clicked!)
//end if
end event

type tab_1 from tab within w_costo_effettivo_commessa
integer x = 846
integer y = 20
integer width = 2789
integer height = 1720
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean raggedright = true
boolean powertips = true
integer selectedtab = 1
tab_acquisti tab_acquisti
tab_lavorazioni tab_lavorazioni
tab_risorse_umane tab_risorse_umane
tabpage_costi_correttivi tabpage_costi_correttivi
tabpage_1 tabpage_1
end type

on tab_1.create
this.tab_acquisti=create tab_acquisti
this.tab_lavorazioni=create tab_lavorazioni
this.tab_risorse_umane=create tab_risorse_umane
this.tabpage_costi_correttivi=create tabpage_costi_correttivi
this.tabpage_1=create tabpage_1
this.Control[]={this.tab_acquisti,&
this.tab_lavorazioni,&
this.tab_risorse_umane,&
this.tabpage_costi_correttivi,&
this.tabpage_1}
end on

on tab_1.destroy
destroy(this.tab_acquisti)
destroy(this.tab_lavorazioni)
destroy(this.tab_risorse_umane)
destroy(this.tabpage_costi_correttivi)
destroy(this.tabpage_1)
end on

type tab_acquisti from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2752
integer height = 1596
long backcolor = 79741120
string text = "Acquisti"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 553648127
dw_costi_acquisto dw_costi_acquisto
end type

on tab_acquisti.create
this.dw_costi_acquisto=create dw_costi_acquisto
this.Control[]={this.dw_costi_acquisto}
end on

on tab_acquisti.destroy
destroy(this.dw_costi_acquisto)
end on

type dw_costi_acquisto from datawindow within tab_acquisti
integer x = 5
integer y = 32
integer width = 2743
integer height = 1560
integer taborder = 41
boolean bringtotop = true
string dataobject = "d_costi_acquisto"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tab_lavorazioni from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2752
integer height = 1596
long backcolor = 79741120
string text = "Lavorazioni"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
cbx_tempo_lavorazione cbx_tempo_lavorazione
cbx_tempo_attrezzaggio cbx_tempo_attrezzaggio
cbx_tempo_attrezzaggio_commessa cbx_tempo_attrezzaggio_commessa
dw_costo_lavorazioni dw_costo_lavorazioni
end type

on tab_lavorazioni.create
this.cbx_tempo_lavorazione=create cbx_tempo_lavorazione
this.cbx_tempo_attrezzaggio=create cbx_tempo_attrezzaggio
this.cbx_tempo_attrezzaggio_commessa=create cbx_tempo_attrezzaggio_commessa
this.dw_costo_lavorazioni=create dw_costo_lavorazioni
this.Control[]={this.cbx_tempo_lavorazione,&
this.cbx_tempo_attrezzaggio,&
this.cbx_tempo_attrezzaggio_commessa,&
this.dw_costo_lavorazioni}
end on

on tab_lavorazioni.destroy
destroy(this.cbx_tempo_lavorazione)
destroy(this.cbx_tempo_attrezzaggio)
destroy(this.cbx_tempo_attrezzaggio_commessa)
destroy(this.dw_costo_lavorazioni)
end on

type cbx_tempo_lavorazione from checkbox within tab_lavorazioni
integer x = 5
integer y = 1632
integer width = 558
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "tempo lavorazione"
boolean checked = true
boolean lefttext = true
end type

type cbx_tempo_attrezzaggio from checkbox within tab_lavorazioni
integer x = 690
integer y = 1632
integer width = 581
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "tempo attrezzaggio"
boolean checked = true
boolean lefttext = true
end type

type cbx_tempo_attrezzaggio_commessa from checkbox within tab_lavorazioni
integer x = 1399
integer y = 1632
integer width = 887
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "tempo attrezzaggio commessa"
boolean checked = true
boolean lefttext = true
end type

type dw_costo_lavorazioni from datawindow within tab_lavorazioni
integer x = 5
integer y = 32
integer width = 2743
integer height = 1460
integer taborder = 31
string dataobject = "d_costo_lavorazioni_eff"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tab_risorse_umane from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2752
integer height = 1596
long backcolor = 79741120
string text = "Risorse Umane"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_costo_risorse_umane dw_costo_risorse_umane
cbx_tempo_risorsa_umana cbx_tempo_risorsa_umana
end type

on tab_risorse_umane.create
this.dw_costo_risorse_umane=create dw_costo_risorse_umane
this.cbx_tempo_risorsa_umana=create cbx_tempo_risorsa_umana
this.Control[]={this.dw_costo_risorse_umane,&
this.cbx_tempo_risorsa_umana}
end on

on tab_risorse_umane.destroy
destroy(this.dw_costo_risorse_umane)
destroy(this.cbx_tempo_risorsa_umana)
end on

type dw_costo_risorse_umane from datawindow within tab_risorse_umane
integer x = 5
integer y = 32
integer width = 2743
integer height = 1460
integer taborder = 41
boolean bringtotop = true
string dataobject = "d_costo_risorse_umane_eff"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cbx_tempo_risorsa_umana from checkbox within tab_risorse_umane
integer x = 5
integer y = 1632
integer width = 709
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Tempo Risorsa Umana"
boolean checked = true
boolean lefttext = true
end type

type tabpage_costi_correttivi from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2752
integer height = 1596
long backcolor = 79741120
string text = "Costi Correttivi"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
cbx_particolare cbx_particolare
dw_costi_correttivi dw_costi_correttivi
end type

on tabpage_costi_correttivi.create
this.cbx_particolare=create cbx_particolare
this.dw_costi_correttivi=create dw_costi_correttivi
this.Control[]={this.cbx_particolare,&
this.dw_costi_correttivi}
end on

on tabpage_costi_correttivi.destroy
destroy(this.cbx_particolare)
destroy(this.dw_costi_correttivi)
end on

type cbx_particolare from checkbox within tabpage_costi_correttivi
integer x = 5
integer y = 1532
integer width = 2720
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Escludi dal calcolo del costo effettivo totale tutti i costi correttivi con flag PARTICOLARE impostato a S"
borderstyle borderstyle = stylelowered!
end type

type dw_costi_correttivi from datawindow within tabpage_costi_correttivi
integer x = 5
integer y = 32
integer width = 2743
integer height = 1480
integer taborder = 51
string dataobject = "d_costi_correttivi"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2752
integer height = 1596
long backcolor = 79741120
string text = "Help"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
st_4 st_4
st_5 st_5
st_6 st_6
end type

on tabpage_1.create
this.st_4=create st_4
this.st_5=create st_5
this.st_6=create st_6
this.Control[]={this.st_4,&
this.st_5,&
this.st_6}
end on

on tabpage_1.destroy
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_6)
end on

type st_4 from statictext within tabpage_1
integer x = 73
integer y = 132
integer width = 2715
integer height = 80
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 79741120
boolean enabled = false
string text = "Attenzione costo effettivo di commessa può essere fatto solo"
boolean focusrectangle = false
end type

type st_5 from statictext within tabpage_1
integer x = 73
integer y = 352
integer width = 2720
integer height = 76
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 79741120
boolean enabled = false
string text = "che abbiano degli ordini di acquisto collegati."
boolean focusrectangle = false
end type

type st_6 from statictext within tabpage_1
integer x = 73
integer y = 232
integer width = 2149
integer height = 76
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 79741120
boolean enabled = false
string text = "per le commesse con l~'avanzamento di produzione completo"
boolean focusrectangle = false
end type

type st_1 from statictext within w_costo_effettivo_commessa
integer x = 2286
integer y = 1760
integer width = 594
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Costo Tot.Effettivo:"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_costo_totale_complessivo from statictext within w_costo_effettivo_commessa
integer x = 2903
integer y = 1760
integer width = 731
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_2 from statictext within w_costo_effettivo_commessa
integer x = 1371
integer y = 1760
integer width = 389
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Pezzi Prodotti:"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_pezzi from statictext within w_costo_effettivo_commessa
integer x = 1783
integer y = 1760
integer width = 480
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type em_anno from editmask within w_costo_effettivo_commessa
integer x = 549
integer y = 1760
integer width = 274
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
boolean spin = true
string displaydata = ""
end type

type st_3 from statictext within w_costo_effettivo_commessa
integer x = 389
integer y = 1760
integer width = 160
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Anno:"
alignment alignment = right!
long bordercolor = 79741120
boolean focusrectangle = false
end type

type cb_aggiorna from commandbutton within w_costo_effettivo_commessa
integer x = 23
integer y = 1760
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;parent.triggerevent("pc_retrieve")
end event

