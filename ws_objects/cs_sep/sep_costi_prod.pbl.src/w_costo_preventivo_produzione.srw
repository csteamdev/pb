﻿$PBExportHeader$w_costo_preventivo_produzione.srw
$PBExportComments$Window costo preventivo
forward
global type w_costo_preventivo_produzione from w_cs_xx_principale
end type
type tab_1 from tab within w_costo_preventivo_produzione
end type
type tab_materie_prime from userobject within tab_1
end type
type dw_costi_materie_prime from datawindow within tab_materie_prime
end type
type tab_materie_prime from userobject within tab_1
dw_costi_materie_prime dw_costi_materie_prime
end type
type tab_lavorazioni from userobject within tab_1
end type
type dw_costo_lavorazioni from datawindow within tab_lavorazioni
end type
type cbx_tempo_lavorazione from checkbox within tab_lavorazioni
end type
type cbx_tempo_attrezzaggio from checkbox within tab_lavorazioni
end type
type cbx_tempo_attrezzaggio_commessa from checkbox within tab_lavorazioni
end type
type tab_lavorazioni from userobject within tab_1
dw_costo_lavorazioni dw_costo_lavorazioni
cbx_tempo_lavorazione cbx_tempo_lavorazione
cbx_tempo_attrezzaggio cbx_tempo_attrezzaggio
cbx_tempo_attrezzaggio_commessa cbx_tempo_attrezzaggio_commessa
end type
type tab_risorse_umane from userobject within tab_1
end type
type dw_costo_risorse_umane from datawindow within tab_risorse_umane
end type
type cbx_tempo_risorsa_umana from checkbox within tab_risorse_umane
end type
type tab_risorse_umane from userobject within tab_1
dw_costo_risorse_umane dw_costo_risorse_umane
cbx_tempo_risorsa_umana cbx_tempo_risorsa_umana
end type
type tab_1 from tab within w_costo_preventivo_produzione
tab_materie_prime tab_materie_prime
tab_lavorazioni tab_lavorazioni
tab_risorse_umane tab_risorse_umane
end type
type dw_ricerca_origine from u_dw_search within w_costo_preventivo_produzione
end type
type cb_1 from commandbutton within w_costo_preventivo_produzione
end type
end forward

global type w_costo_preventivo_produzione from w_cs_xx_principale
integer width = 3694
integer height = 2540
string title = "Costo Preventivo Prodotto"
tab_1 tab_1
dw_ricerca_origine dw_ricerca_origine
cb_1 cb_1
end type
global w_costo_preventivo_produzione w_costo_preventivo_produzione

type variables
uo_excel_preventivo iuo_excel
s_chiave_distinta is_chiave_distinta
double idd_qta_pf

uo_formule_calcolo	iuo_formule
datastore				ids_variabili
end variables

forward prototypes
public function integer wf_fasi_lavorazione (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore)
public subroutine wf_elabora_costo_pf_mp (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, string fs_tipo_valorizzazione, ref double fdd_costo_totale)
public subroutine wf_costo_sl (string fs_cod_prodotto_sl, string fs_cod_versione_sl, double fdd_quan_utilizzo, long fl_num_sequenza, ref double fdd_costo_totale)
public function integer wf_fasi_lavorazione_excel (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore, ref long fl_riga, long fl_colonna, long fl_celle_sep_riga, string fs_mat_prima[], double fdd_qta_utilizzo[])
public subroutine wf_costo_mp2 (string fs_mat_prima, double fdd_quan_utilizzo, string fs_cod_mp, string fs_valorizzazione, ref double fdd_costo_totale)
protected subroutine wf_costo_mp (string fs_mat_prima[], double fdd_quan_utilizzo[], string fs_cod_mp, string fs_valorizzazione, ref double fdd_costo_totale)
public function integer wf_trova_mat_prime (string fs_cod_prodotto_padre, string fs_cod_versione, ref string fs_materia_prima[], ref decimal fd_quan_utilizzo[], decimal fd_quan_utilizzo_prec, decimal fd_dim_x, decimal fd_dim_y, ref string fs_errore)
public subroutine wf_calcola_costo ()
public function integer wf_genera_commessa (string fs_cod_prodotto_finito, string fs_cod_versione_finito, string fs_cod_tipo_commessa, decimal fd_quan_produzione, ref long fl_anno_commessa, ref long fl_num_commessa, ref string fs_errore)
end prototypes

public function integer wf_fasi_lavorazione (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore);// Funzione che calcola i costi di lavorazione
// nome: wf_fasi_lavorazione
// tipo: integer
//       -1 failed
//  		 0 passed
//
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//							 fs_cod_prodotto	 		 string  		valore
//							 fdd_quantita_richiesta	 double			valore
//
//		Creata il 24-07-97 
//		Autore Diego Ferrari

string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, & 
			 ls_cod_lavorazione,ls_test,ls_cod_cat_attrezzature,ls_des_cat_attrezzature, &
			 ls_cod_cat_attrezzature_2,ls_des_cat_attrezzature_2, ls_cod_versione_figlio, ls_flag_fase_esterna, ls_flag_uso, &
			 ls_flag_mp, ls_flag_valorizza
long      ll_num_righe,ll_num_figli, ll_num_sequenza, ll_new
integer   li_risposta
dec{4}    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione, ldd_tempo_totale, ldd_costo_medio_orario, ldd_costo_totale, &
			 ldd_tempo_risorsa_umana,ldd_costo_medio_orario_2,ldd_costo_totale_2, ld_costo_orario, ld_costo_pezzo
datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fdd_quantita_richiesta
	
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda=:s_cs_xx.cod_azienda 
	and 	 cod_prodotto_padre=:ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode=100 then continue
	
	select flag_materia_prima,
			 flag_valorizza_mp
	into	 :ls_flag_mp,
			 :ls_flag_valorizza
	from	 distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto_padre = :fs_cod_prodotto and
			 cod_versione = :fs_cod_versione and
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and
			 cod_versione_figlio = :ls_cod_versione_figlio and
			 num_sequenza = :ll_num_sequenza;
			 
	if sqlca.sqlcode = 0 and not isnull( ls_flag_mp) and ls_flag_mp = "S" and ls_flag_valorizza = "N" and not isnull(ls_flag_valorizza) then
	elseif sqlca.sqlcode = 0 and not isnull( ls_flag_mp) and ls_flag_mp = "S" and ls_flag_valorizza = "S" then		
		continue
	end if	

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 tempo_attrezzaggio,
		    tempo_attrezzaggio_commessa,
			 tempo_lavorazione,
			 cod_cat_attrezzature_2,
			 tempo_risorsa_umana,
			 flag_esterna,
			 costo_orario,
			 costo_pezzo,
			 flag_uso
   INTO   :ls_cod_reparto,   
          	:ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
 			 :ldd_tempo_attrezzaggio,
		    :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_risorsa_umana,
			 :ls_flag_fase_esterna,
			 :ld_costo_orario,
			 :ld_costo_pezzo,
			 :ls_flag_uso
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda  AND   
			cod_prodotto = :ls_cod_prodotto_figlio AND
			cod_versione = :ls_cod_versione_figlio;

	if sqlca.sqlcode=100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		//messagebox( "SEP", fs_errore, stopsign!)
		continue
	end if
  
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto =:ls_cod_prodotto_figlio;

	select des_cat_attrezzature,
		    costo_medio_orario
	into   :ls_des_cat_attrezzature,
			 :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;

	select des_cat_attrezzature,
			 costo_medio_orario
	into   :ls_des_cat_attrezzature_2,
			 :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature_2;
	
	
	ldd_tempo_attrezzaggio = ldd_tempo_attrezzaggio 
	ldd_tempo_attrezzaggio_commessa = ldd_tempo_attrezzaggio_commessa 
	ldd_tempo_lavorazione = ldd_tempo_lavorazione * ldd_quan_utilizzo
	ldd_tempo_risorsa_umana = ldd_tempo_risorsa_umana * ldd_quan_utilizzo

	ldd_tempo_totale = 0

	if tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio
	end if

	if tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio_commessa.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio_commessa
	end if

	if tab_1.tab_lavorazioni.cbx_tempo_lavorazione.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_lavorazione
	end if	
	
	if not isnull(ls_flag_fase_esterna) and ls_flag_fase_esterna = "S" then		
		
		if ls_flag_uso = "O" then
			if isnull(ld_costo_orario) then ld_costo_orario = 0
			ldd_costo_medio_orario = ld_costo_orario
			ldd_costo_medio_orario_2 = ld_costo_orario
			ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
		else
			if isnull(ld_costo_pezzo) then ld_costo_pezzo = 0
			ldd_costo_totale = fdd_quantita_richiesta * ld_costo_pezzo
		end if
		
		ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2	
		
	else			
		ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
		ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2		
	end if

	ll_new = tab_1.tab_lavorazioni.dw_costo_lavorazioni.insertrow(0)	
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "cod_prodotto", ls_cod_prodotto_figlio)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "des_prodotto", ls_des_prodotto)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "cod_lavorazione", ls_cod_lavorazione)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "cod_cat_attrezzature", ls_cod_cat_attrezzature)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "des_cat_attrezzature", ls_des_cat_attrezzature)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "tempo_attrezzaggio", ldd_tempo_attrezzaggio)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "tempo_attrezzaggio_commessa", ldd_tempo_attrezzaggio_commessa)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "tempo_lavorazione", ldd_tempo_lavorazione)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "tempo_totale", ldd_tempo_totale)
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "costo_medio_orario", ldd_costo_medio_orario)  
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "costo_totale", ldd_costo_totale)  
	tab_1.tab_lavorazioni.dw_costo_lavorazioni.setitem( ll_new, "num_pezzi", fdd_quantita_richiesta)  
	
	ll_new = tab_1.tab_risorse_umane.dw_costo_risorse_umane.insertrow(0)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem( ll_new, "cod_cat_attrezzature", ls_cod_cat_attrezzature_2)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem( ll_new, "des_cat_attrezzature", ls_des_cat_attrezzature_2)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem( ll_new, "tempo_totale", ldd_tempo_risorsa_umana)
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem( ll_new, "costo_medio_orario", ldd_costo_medio_orario_2)  
	tab_1.tab_risorse_umane.dw_costo_risorse_umane.setitem( ll_new, "costo_totale", ldd_costo_totale_2)

	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta = wf_fasi_lavorazione(ls_cod_prodotto_figlio,ls_cod_versione_figlio,ldd_quan_utilizzo,ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			messagebox( "SEP", fs_errore, stopsign!)
			//return -1
		end if
	end if
	
next

return 0
end function

public subroutine wf_elabora_costo_pf_mp (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, string fs_tipo_valorizzazione, ref double fdd_costo_totale);string ls_errore
uo_calcolo_costi_pf luo_calcolo_costi_pf
decimal ld_quan_utilizzo, ld_costo_singola_fase
integer ll_ret

luo_calcolo_costi_pf = create uo_calcolo_costi_pf

////ls_cod_prodotto = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_prodotto")
////ls_cod_versione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_versione")
fdd_costo_totale = luo_calcolo_costi_pf.uof_costo_totale_prodotto(fs_cod_prodotto, fs_cod_versione, fs_tipo_valorizzazione, ref ls_errore)


if fdd_costo_totale < 0 then
	messagebox("SEP",ls_errore)
	return
else
	//messagebox("OK",ld_totale)
	fdd_costo_totale = idd_qta_pf * fdd_costo_totale * fdd_quantita_richiesta
end if

end subroutine

public subroutine wf_costo_sl (string fs_cod_prodotto_sl, string fs_cod_versione_sl, double fdd_quan_utilizzo, long fl_num_sequenza, ref double fdd_costo_totale);string 		ls_flag_mp, ls_flag_valorizza, ls_cod_reparto, ls_cod_lavorazione, ls_cod_cat_attrezzature, &
				ls_cod_cat_attrezzature_2, ls_flag_fase_esterna, ls_flag_uso, ls_errore

double 		ldd_tempo_attrezzaggio, ldd_tempo_attrezzaggio_commessa, ldd_tempo_lavorazione, &
				ldd_tempo_risorsa_umana, ld_costo_orario, ld_costo_pezzo, ldd_costo_medio_orario, &
				ldd_costo_medio_orario_2, ldd_tempo_totale, ldd_costo_totale
				

fdd_costo_totale = 0

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 tempo_attrezzaggio,
		    tempo_attrezzaggio_commessa,
			 tempo_lavorazione,
			 cod_cat_attrezzature_2,
			 tempo_risorsa_umana,
			 flag_esterna,
			 costo_orario,
			 costo_pezzo,
			 flag_uso
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
 			 :ldd_tempo_attrezzaggio,
		    :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_risorsa_umana,
			 :ls_flag_fase_esterna,
			 :ld_costo_orario,
			 :ld_costo_pezzo,
			 :ls_flag_uso
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda  AND   
			cod_prodotto = :fs_cod_prodotto_sl AND
			cod_versione = :fs_cod_versione_sl;

	if sqlca.sqlcode=100 then
		ls_errore = "Manca la fase di lavorazione per il prodotto: " + fs_cod_prodotto_sl
		g_mb.messagebox("SEP", ls_errore)
		return
	end if

	select  costo_medio_orario
	into   :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;

	select costo_medio_orario
	into   :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature_2;
	
	ldd_tempo_attrezzaggio_commessa = ldd_tempo_attrezzaggio_commessa 
	ldd_tempo_lavorazione = ldd_tempo_lavorazione * fdd_quan_utilizzo
	//ldd_tempo_risorsa_umana = ldd_tempo_risorsa_umana * fdd_quan_utilizzo

	ldd_tempo_totale = 0

	if tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio
	end if

	if tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio_commessa.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio_commessa
	end if

	if tab_1.tab_lavorazioni.cbx_tempo_lavorazione.checked=true then
		ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_lavorazione
	end if	
	
	if not isnull(ls_flag_fase_esterna) and ls_flag_fase_esterna = "S" then		
		
		if ls_flag_uso = "O" then
			if isnull(ld_costo_orario) then ld_costo_orario = 0
			ldd_costo_medio_orario = ld_costo_orario
			ldd_costo_medio_orario_2 = ld_costo_orario
			ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
		else
			if isnull(ld_costo_pezzo) then ld_costo_pezzo = 0
			ldd_costo_totale = fdd_quan_utilizzo * ld_costo_pezzo
//			ldd_costo_totale = fdd_quantita_richiesta * ld_costo_pezzo
		end if
		
//		ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2	
		// 24/04/2008: correzione eseguita da enrico: considerava sempre il costo unitario.
//		ldd_costo_totale_2 = ldd_costo_totale_2 * ldd_quan_utilizzo
		
	else			
		ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
//		ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2		
	end if

//   ll_riga = dw_report.insertrow(0)
//   dw_report.setitem( ll_riga, "cod_prodotto_l", ls_cod_prodotto_figlio)
// 	dw_report.setitem( ll_riga, "des_prodotto_l", ls_des_prodotto)
//	dw_report.setitem( ll_riga, "cod_lavorazione_l", ls_cod_lavorazione)
//	dw_report.setitem( ll_riga, "cod_cat_attrezzature_l", ls_cod_cat_attrezzature)
//	dw_report.setitem( ll_riga, "des_cat_attrezzature_l", ls_des_cat_attrezzature)
//	dw_report.setitem( ll_riga, "tempo_attrezzaggio_l", ldd_tempo_attrezzaggio)
//	dw_report.setitem( ll_riga, "tempo_attrezzaggio_commessa_l", ldd_tempo_attrezzaggio_commessa)
//	dw_report.setitem( ll_riga, "tempo_lavorazione_l", ldd_tempo_lavorazione)
//	dw_report.setitem( ll_riga, "tempo_totale_l", ldd_tempo_totale)
//	dw_report.setitem( ll_riga, "costo_medio_orario", ldd_costo_medio_orario)  
//	dw_report.setitem( ll_riga, "costo_totale", ldd_costo_totale)  
//	dw_report.setitem( ll_riga, "num_pezzi", fdd_quantita_richiesta) 
//	dw_report.setitem( ll_riga, "num_pezzi", ldd_quan_utilizzo)


fdd_costo_totale = ldd_costo_totale
end subroutine

public function integer wf_fasi_lavorazione_excel (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore, ref long fl_riga, long fl_colonna, long fl_celle_sep_riga, string fs_mat_prima[], double fdd_qta_utilizzo[]);string  ls_test_prodotto_f, ls_errore,ls_des_prodotto, ls_flag_materia_prima,ls_cod_misura_mag, ls_flag_tipo_costo
long    ll_num_figli,ll_handle,ll_num_righe
integer li_num_priorita,li_risposta
s_chiave_distinta l_chiave_distinta

long ll_colonna_prod, ll_colonna_qta, ll_riga_temp, ll_delta_riga
string ls_valore, ls_flag_valorizza_mp
double ld_costo_totale

datastore lds_righe_distinta

ll_num_figli = 1
ls_flag_tipo_costo =  dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"flag_tipo_costo")

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)

lds_righe_distinta.setsort("cod_prodotto_figlio A, num_sequenza A")
ll_num_figli = lds_righe_distinta.sort()

//imposta la posizione di colonna 
ll_colonna_prod = fl_colonna

for ll_num_figli = 1 to ll_num_righe

	l_chiave_distinta.cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	l_chiave_distinta.quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	l_chiave_distinta.cod_prodotto_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_padre")
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")
	l_chiave_distinta.cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	l_chiave_distinta.cod_versione_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione")
	ls_des_prodotto = lds_righe_distinta.getitemstring(ll_num_figli,"anag_prodotti_des_prodotto")
	ls_cod_misura_mag = lds_righe_distinta.getitemstring(ll_num_figli,"anag_prodotti_cod_misura_mag")
	l_chiave_distinta.flag_ramo_descrittivo = lds_righe_distinta.getitemstring(ll_num_figli,"distinta_flag_ramo_descrittivo")
	
	ls_des_prodotto = trim(ls_des_prodotto)

	if isnull(ls_cod_misura_mag) then ls_cod_misura_mag = ""

	if ls_flag_materia_prima = 'N' then
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and	 
		       cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and    
				 cod_versione = :l_chiave_distinta.cod_versione_figlio;

		if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
			if l_chiave_distinta.flag_ramo_descrittivo = "S" then
				//ramo descrittivo
			else
				//MP
			end if
			
			//SCRIVI SU EXCEL come MP (materia prima o ramo descrittivo)
			fl_riga += 1
			
			//incrementa la colonna solo quando inserisci la prima riga, quindi solo la prima volta
			//ll_colonna_prod = fl_colonna
			if ll_num_figli = 1 then 	ll_colonna_prod += 1
			
			ls_valore = l_chiave_distinta.cod_prodotto_figlio + ", " + ls_des_prodotto
			
			iuo_excel.uof_crea_riga_mp( fl_riga, ll_colonna_prod, ls_valore ,"2")
			
			ll_colonna_qta = ll_colonna_prod + fl_celle_sep_riga
			
			setnull(ld_costo_totale)

			//wf_costo_mp(fs_mat_prima[], fdd_qta_utilizzo[], l_chiave_distinta.cod_prodotto_figlio, ls_valorizzazione, ld_costo_totale)
			wf_costo_mp2(l_chiave_distinta.cod_prodotto_figlio, &
								l_chiave_distinta.quan_utilizzo * fdd_quantita_richiesta, &
								l_chiave_distinta.cod_prodotto_figlio, ls_flag_tipo_costo, ld_costo_totale)

			//iuo_excel.uof_crea_riga_mp( fl_riga, ll_colonna_qta, string(ld_costo_totale, "[currency]") ,"4")
			iuo_excel.uof_crea_riga_mp( fl_riga, ll_colonna_qta, string(ld_costo_totale) ,"4")
			//-------------------------------------------------
			
			if is_chiave_distinta.cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio and &
				is_chiave_distinta.cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre and &
				is_chiave_distinta.cod_versione_figlio = l_chiave_distinta.cod_versione_figlio and &
				is_chiave_distinta.cod_versione_padre = l_chiave_distinta.cod_versione_padre then
			end if
			
			continue
		else
			//SCRIVI SU EXCEL come SL
			fl_riga += 1
			
			//incrementa la colonna solo quando inserisci la prima riga, quindi solo la prima volta
			if ll_num_figli = 1 then 	ll_colonna_prod += 1
			
			ls_valore = l_chiave_distinta.cod_prodotto_figlio + ", " + ls_des_prodotto
			
			iuo_excel.uof_crea_riga_lavorazione( fl_riga, ll_colonna_prod, ls_valore ,"2")
			
			ll_colonna_qta = ll_colonna_prod + fl_celle_sep_riga
			
			setnull(ld_costo_totale)
			
			wf_costo_sl(l_chiave_distinta.cod_prodotto_figlio, &
											l_chiave_distinta.cod_versione_figlio, &
											l_chiave_distinta.quan_utilizzo * fdd_quantita_richiesta, &
											l_chiave_distinta.num_sequenza, &
											ld_costo_totale)
						
			//memorizza la riga
			ll_riga_temp = fl_riga
			
			//prima di scrivere il totale chiama ricorsivamente e poi metti la formula
			li_risposta = wf_fasi_lavorazione_excel(l_chiave_distinta.cod_prodotto_figlio, &
																	l_chiave_distinta.cod_versione_figlio, &
																	l_chiave_distinta.quan_utilizzo * fdd_quantita_richiesta, &
																	ls_errore, fl_riga, ll_colonna_prod, fl_celle_sep_riga, &
																	fs_mat_prima[], fdd_qta_utilizzo[])
			if li_risposta = -1 then
				fs_errore=ls_errore
				messagebox( "SEP", fs_errore, stopsign!)
				//return -1
			end if
			
			ll_delta_riga = fl_riga - ll_riga_temp
			if ll_delta_riga > 0 then
				//piazza la formula
				iuo_excel.uof_crea_riga_lavorazione_costo( ll_riga_temp, ll_colonna_qta, string(ld_costo_totale) ,"4", ll_riga_temp+1, fl_riga)
			end if
			
			//-------------------------------------------------
			
			if is_chiave_distinta.cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio and &
				is_chiave_distinta.cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre and &
				is_chiave_distinta.cod_versione_figlio = l_chiave_distinta.cod_versione_figlio and &
				is_chiave_distinta.cod_versione_padre = l_chiave_distinta.cod_versione_padre then
			end if
			
			setnull(ls_test_prodotto_f)
			
		end if
	else		
		//VERIFICA PRIMA SE è VALORIZZATO COME SEMILAVORATO
		select  flag_valorizza_mp
		into	  :ls_flag_valorizza_mp
		from    distinta 
		where   cod_azienda = :s_cs_xx.cod_azienda	and     
				  cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and     
				  cod_versione = :l_chiave_distinta.cod_versione_padre	and
				  cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
				  cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio and
				  flag_materia_prima = 'S';
				  
		if sqlca.sqlcode = 0 and not isnull(ls_flag_valorizza_mp) and ls_flag_valorizza_mp = 'N' then			
			// *** lo considero un semilavorato
			//SCRIVI SU EXCEL come SL
			fl_riga += 1
			
			//incrementa la colonna solo quando inserisci la prima riga, quindi solo la prima volta
			if ll_num_figli = 1 then 	ll_colonna_prod += 1
			
			ls_valore = l_chiave_distinta.cod_prodotto_figlio + ", " + ls_des_prodotto
			
			iuo_excel.uof_crea_riga_lavorazione( fl_riga, ll_colonna_prod, ls_valore ,"2")
			
			ll_colonna_qta = ll_colonna_prod + fl_celle_sep_riga
			
			setnull(ld_costo_totale)
			
			wf_costo_sl(l_chiave_distinta.cod_prodotto_figlio, &
											l_chiave_distinta.cod_versione_figlio, &
											l_chiave_distinta.quan_utilizzo * fdd_quantita_richiesta, &
											l_chiave_distinta.num_sequenza, &
											ld_costo_totale)
						
			//memorizza la riga
			ll_riga_temp = fl_riga
			
			//prima di scrivere il totale chiama ricorsivamente e poi metti la formula
			li_risposta = wf_fasi_lavorazione_excel(l_chiave_distinta.cod_prodotto_figlio, &
																	l_chiave_distinta.cod_versione_figlio, &
																	l_chiave_distinta.quan_utilizzo * fdd_quantita_richiesta, &
																	ls_errore, fl_riga, ll_colonna_prod, fl_celle_sep_riga, &
																	fs_mat_prima[], fdd_qta_utilizzo[])
			if li_risposta = -1 then
				fs_errore=ls_errore
				messagebox( "SEP", fs_errore, stopsign!)
				//return -1
			end if
			
			ll_delta_riga = fl_riga - ll_riga_temp
			if ll_delta_riga > 0 then
				//piazza la formula
				iuo_excel.uof_crea_riga_lavorazione_costo( ll_riga_temp, ll_colonna_qta, string(ld_costo_totale) ,"4", ll_riga_temp+1, fl_riga)
			end if
			
			//-------------------------------------------------
			
			if is_chiave_distinta.cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio and &
				is_chiave_distinta.cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre and &
				is_chiave_distinta.cod_versione_figlio = l_chiave_distinta.cod_versione_figlio and &
				is_chiave_distinta.cod_versione_padre = l_chiave_distinta.cod_versione_padre then
			end if
			
			setnull(ls_test_prodotto_f)
			//*** ------------------------------------
		else
			//tutto come prima ---------------
			//SCRIVI SU EXCEL come MP
			fl_riga += 1
					
			//incrementa la colonna solo quando inserisci la prima riga, quindi solo la prima volta
			if ll_num_figli = 1 then 	ll_colonna_prod += 1
			
			ls_valore = l_chiave_distinta.cod_prodotto_figlio + ", " + ls_des_prodotto
			
			iuo_excel.uof_crea_riga_mp( fl_riga, ll_colonna_prod, ls_valore ,"2")
			
			ll_colonna_qta = ll_colonna_prod + fl_celle_sep_riga
			
			setnull(ld_costo_totale)
	
			//wf_costo_mp(fs_mat_prima[], fdd_qta_utilizzo[], l_chiave_distinta.cod_prodotto_figlio, ls_valorizzazione, ld_costo_totale)
			wf_costo_mp2(l_chiave_distinta.cod_prodotto_figlio, &
									l_chiave_distinta.quan_utilizzo * fdd_quantita_richiesta, &
									l_chiave_distinta.cod_prodotto_figlio, ls_flag_tipo_costo, ld_costo_totale)
	
			//iuo_excel.uof_crea_riga_mp( fl_riga, ll_colonna_qta, string(ld_costo_totale, "[currency]") ,"4")
			iuo_excel.uof_crea_riga_mp( fl_riga, ll_colonna_qta, string(ld_costo_totale) ,"4")
			//-------------------------------------------------
			
			if is_chiave_distinta.cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio and &
				is_chiave_distinta.cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre and &
				is_chiave_distinta.cod_versione_figlio = l_chiave_distinta.cod_versione_figlio and &
				is_chiave_distinta.cod_versione_padre = l_chiave_distinta.cod_versione_padre then
			end if
			// ****					
			
			//continue
		end if
		
		/*
		//SCRIVI SU EXCEL come MP
		fl_riga += 1
				
		//incrementa la colonna solo quando inserisci la prima riga, quindi solo la prima volta
		if ll_num_figli = 1 then 	ll_colonna_prod += 1
		
		ls_valore = l_chiave_distinta.cod_prodotto_figlio + ", " + ls_des_prodotto
		
		iuo_excel.uof_crea_riga_mp( fl_riga, ll_colonna_prod, ls_valore ,"2")
		
		ll_colonna_qta = ll_colonna_prod + fl_celle_sep_riga
		
		setnull(ld_costo_totale)

		//wf_costo_mp(fs_mat_prima[], fdd_qta_utilizzo[], l_chiave_distinta.cod_prodotto_figlio, ls_valorizzazione, ld_costo_totale)
		wf_costo_mp2(l_chiave_distinta.cod_prodotto_figlio, &
								l_chiave_distinta.quan_utilizzo * fdd_quantita_richiesta, &
								l_chiave_distinta.cod_prodotto_figlio, ls_valorizzazione, ld_costo_totale)

		//iuo_excel.uof_crea_riga_mp( fl_riga, ll_colonna_qta, string(ld_costo_totale, "[currency]") ,"4")
		iuo_excel.uof_crea_riga_mp( fl_riga, ll_colonna_qta, string(ld_costo_totale) ,"4")
		//-------------------------------------------------
		
		if is_chiave_distinta.cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio and &
			is_chiave_distinta.cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre and &
			is_chiave_distinta.cod_versione_figlio = l_chiave_distinta.cod_versione_figlio and &
			is_chiave_distinta.cod_versione_padre = l_chiave_distinta.cod_versione_padre then
		end if
		// ****					
		
		continue
		*/
	end if
next

destroy(lds_righe_distinta)

return 0


return 0







end function

public subroutine wf_costo_mp2 (string fs_mat_prima, double fdd_quan_utilizzo, string fs_cod_mp, string fs_valorizzazione, ref double fdd_costo_totale);long ll_t
double ldd_costo_materia_prima, ldd_quantita_totale

fdd_costo_totale = 0

ldd_quantita_totale = fdd_quan_utilizzo //* idd_qta_pf
		
if fs_valorizzazione = "A" then

	select prezzo_acquisto
	into	:ldd_costo_materia_prima
	from 	 anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
			 cod_prodotto = :fs_mat_prima;				 		

end if

if fs_valorizzazione = "S" then

	select costo_standard
	into	 :ldd_costo_materia_prima
	from 	 anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
			 cod_prodotto = :fs_mat_prima;
			
end if

if fs_valorizzazione = "U" then

	select costo_ultimo
	into	:ldd_costo_materia_prima
	from 	 anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
			 cod_prodotto = :fs_mat_prima;

end if

if isnull(ldd_costo_materia_prima) then ldd_costo_materia_prima = 0

if isnull(ldd_quantita_totale) then  ldd_quantita_totale = 0

fdd_costo_totale = ldd_costo_materia_prima * ldd_quantita_totale

/*
for ll_t = 1 to upperbound(fs_mat_prima)
	
	if fs_cod_mp = fs_mat_prima[ll_t] then
		
		if isnull(fdd_quan_utilizzo[ll_t]) then fdd_quan_utilizzo[ll_t] = 0 
			
		ldd_quantita_totale = fdd_quan_utilizzo[ll_t] * idd_qta_pf
		
		if fs_valorizzazione = "A" then
		
			select prezzo_acquisto
			into	:ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda	and    
					 cod_prodotto = :fs_mat_prima[ll_t];				 		
		
		end if
		
		if fs_valorizzazione = "S" then
		
			select costo_standard
			into	 :ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda	and    
					 cod_prodotto = :fs_mat_prima[ll_t];
					
		end if
		
		if fs_valorizzazione = "U" then
		
			select costo_ultimo
			into	:ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda	and    
					 cod_prodotto = :fs_mat_prima[ll_t];
		
		end if
		
		if isnull(ldd_costo_materia_prima) then ldd_costo_materia_prima = 0
		
		if isnull(ldd_quantita_totale) then  ldd_quantita_totale = 0
		
		fdd_costo_totale = ldd_costo_materia_prima * ldd_quantita_totale
		
		exit
	end if
next
*/
end subroutine

protected subroutine wf_costo_mp (string fs_mat_prima[], double fdd_quan_utilizzo[], string fs_cod_mp, string fs_valorizzazione, ref double fdd_costo_totale);long ll_t
double ldd_costo_materia_prima, ldd_quantita_totale
boolean lb_trovata = false

fdd_costo_totale = 0

LONG I

for ll_t = 1 to upperbound(fs_mat_prima)
		
	if fs_cod_mp = fs_mat_prima[ll_t] then
		
		lb_trovata = true
		
		if isnull(fdd_quan_utilizzo[ll_t]) then fdd_quan_utilizzo[ll_t] = 0 
			
		ldd_quantita_totale = fdd_quan_utilizzo[ll_t] * idd_qta_pf
		
		if fs_valorizzazione = "A" then
		
			select prezzo_acquisto
			into	:ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda	and    
					 cod_prodotto = :fs_mat_prima[ll_t];				 		
		
		end if
		
		if fs_valorizzazione = "S" then
		
			select costo_standard
			into	 :ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda	and    
					 cod_prodotto = :fs_mat_prima[ll_t];
					
		end if
		
		if fs_valorizzazione = "U" then
		
			select costo_ultimo
			into	:ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda	and    
					 cod_prodotto = :fs_mat_prima[ll_t];
		
		end if
		
		if isnull(ldd_costo_materia_prima) then ldd_costo_materia_prima = 0
		
		if isnull(ldd_quantita_totale) then  ldd_quantita_totale = 0
		
		fdd_costo_totale = ldd_costo_materia_prima * ldd_quantita_totale
		
		exit
	end if
next
end subroutine

public function integer wf_trova_mat_prime (string fs_cod_prodotto_padre, string fs_cod_versione, ref string fs_materia_prima[], ref decimal fd_quan_utilizzo[], decimal fd_quan_utilizzo_prec, decimal fd_dim_x, decimal fd_dim_y, ref string fs_errore);//		Funzione che trova le materie prime di un prodotto finito
//
// 		nome: f_trova_mat_prima
// 		tipo: integer
//  
//		Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//							 fs_cod_prodotto_padre	 	string			valore
//							 fs_cod_versione				string			valore
//							 fs_materia_prima[]			string			riferimento
//							 fd_quan_utilizzo[]    			decimal		riferimento 
//

string			ls_cod_prodotto_figlio[],ls_test,ls_cod_prodotto_variante,ls_flag_fs_materia_prima[], ls_formula, ls_risultato, ls_cod_formula_prod_figlio, & 
				ls_flag_materia_prima_variante, ls_cod_versione_figlio[], ls_flag_valorizza_mp, ls_cod_formula_quan_utilizzo, ls_temp
				
long			ll_conteggio,ll_t, ll_max, ll_num_sequenza

integer		li_risposta

dec{4}		ldd_quantita_utilizzo[], ldd_quan_utilizzo_variante, ld_risultato
//uo_formule_calcolo luo_formule

ll_conteggio = 1

declare 	righe_distinta_3 cursor for 
select  	cod_prodotto_figlio,
			num_sequenza,
        		quan_utilizzo,
		  	flag_materia_prima,
		  	cod_versione_figlio,
			cod_formula_quan_utilizzo,
			cod_formula_prod_figlio
from    	distinta 
where   	cod_azienda = :s_cs_xx.cod_azienda	and     
		  	cod_prodotto_padre = :fs_cod_prodotto_padre and     
		  	cod_versione = :fs_cod_versione;

open righe_distinta_3;

do while true
	fetch 	righe_distinta_3 
	into  	:ls_cod_prodotto_figlio[ll_conteggio],
			:ll_num_sequenza,
		   	:ldd_quantita_utilizzo[ll_conteggio],
			:ls_flag_fs_materia_prima[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
			:ls_cod_formula_quan_utilizzo,
			:ls_cod_formula_prod_figlio;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	if sqlca.sqlcode<>0 then
		close righe_distinta_3;
		g_mb.error("Errore nel DB", sqlca.sqlerrtext)
		return -1
	end if
	
	
	iuo_formule.is_cod_prodotto_padre = fs_cod_prodotto_padre
	iuo_formule.is_cod_versione_padre = fs_cod_versione
	iuo_formule.is_cod_prodotto_figlio = ls_cod_prodotto_figlio[ll_conteggio]
	iuo_formule.is_cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
	iuo_formule.il_num_sequenza = ll_num_sequenza
	
	
	if not isnull(ls_cod_formula_prod_figlio) then
		// leggo il testo della formula
		select testo_formula
		into  :ls_formula
		from  tab_formule_db
		where cod_azienda = :s_cs_xx.cod_azienda and
					cod_formula = :ls_cod_formula_prod_figlio;
					
		if sqlca.sqlcode = 100 then
			close righe_distinta_3;
			fs_errore = "Attenzione, la formula " + ls_cod_formula_quan_utilizzo + " non esiste !"
			return  -1
		end if
		
		if iuo_formule.uof_calcola_formula_veloce(ls_cod_formula_prod_figlio, ls_formula, "2", ids_variabili, ld_risultato, ls_risultato, fs_errore) < 0 then
			return -1
		end if
		
		if not isnull(ls_risultato) and ls_risultato<>"" and ls_risultato<>ls_cod_prodotto_figlio[ll_conteggio] then
		
		select count(*)
		into :li_risposta
		from anag_prodotti
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_risultato;
		
		if li_risposta > 0 then
			//valutato dalla formula un codice prodotto esistente e valido
			ls_cod_prodotto_figlio[ll_conteggio] = ls_risultato
			iuo_formule.is_cod_prodotto_figlio = ls_risultato
		end if
		
		end if
		
	end if
	
	
	if not isnull( ls_cod_formula_quan_utilizzo ) then
		// leggo il testo della formula
		select testo_formula
		into  :ls_formula
		from  tab_formule_db
		where cod_azienda = :s_cs_xx.cod_azienda and
					cod_formula = :ls_cod_formula_quan_utilizzo;
					
		if sqlca.sqlcode = 100 then
			fs_errore = "Attenzione, la formula " + ls_cod_formula_quan_utilizzo + " non esiste !"
			rollback;
			return  -1
		end if
		
		if iuo_formule.uof_calcola_formula_veloce(ls_cod_formula_quan_utilizzo, ls_formula, "1", ids_variabili, ld_risultato, ls_temp, fs_errore) < 0 then
			return -1
		end if
		
		ldd_quantita_utilizzo[ll_conteggio] = ld_risultato * fd_quan_utilizzo_prec
	else
		ldd_quantita_utilizzo[ll_conteggio] = ldd_quantita_utilizzo[ll_conteggio] * fd_quan_utilizzo_prec
	end if
	
	ll_conteggio++	

loop

close righe_distinta_3;


for ll_t = 1 to ll_conteggio -1

	if ls_flag_fs_materia_prima[ll_t] = "N" then
		
		declare 	righe_test cursor for
		select		cod_prodotto_figlio
		FROM   	distinta  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		    			cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
		    			cod_versione=:ls_cod_versione_figlio[ll_t];
	
		open righe_test;
		
		fetch righe_test
		into  :ls_test;
		
		if sqlca.sqlcode<0 then
			
			close righe_test;
			return -1
		end if
	
		if sqlca.sqlcode=100 then
			close righe_test;
			ll_max=upperbound(fs_materia_prima)
			ll_max++		
			fs_materia_prima[ll_max] = ls_cod_prodotto_figlio[ll_t]
			fd_quan_utilizzo[ll_max] =ldd_quantita_utilizzo[ll_t]
	
		else
			// esiste un figlio per cui vado in profondità della distinta
			close righe_test;
			li_risposta = wf_trova_mat_prime(ls_cod_prodotto_figlio[ll_t],ls_cod_versione_figlio[ll_t], fs_materia_prima[], fd_quan_utilizzo[], ldd_quantita_utilizzo[ll_t], fd_dim_x, fd_dim_y, fs_errore)
			if li_risposta < 0 then
				return -1
			end if
		end if
	else
		
		if s_cs_xx.parametri.parametro_s_5 = "REPORT_COSTO_PREVENTIVO" then
			
			// *** controllo il tipo di valorizzazione
			select  flag_valorizza_mp
			into	  :ls_flag_valorizza_mp
			from    distinta 
			where   cod_azienda = :s_cs_xx.cod_azienda	and     
					  cod_prodotto_padre = :fs_cod_prodotto_padre and     
					  cod_versione = :fs_cod_versione	and
					  cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_t] and
					  cod_versione_figlio = :ls_cod_versione_figlio[ll_t] and
					  flag_materia_prima = 'S';
					  
			if sqlca.sqlcode = 0 and not isnull(ls_flag_valorizza_mp) and ls_flag_valorizza_mp = 'N' then
				
				// *** lo considero un semilavorato
				
				declare righe_test_2 cursor for
				SELECT cod_prodotto_figlio
				FROM   distinta  
				WHERE  cod_azienda = :s_cs_xx.cod_azienda	AND    
						 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t]	and    
						 cod_versione = :ls_cod_versione_figlio[ll_t];
			
				open righe_test_2;
				
				fetch righe_test_2	into :ls_test;
				
				if sqlca.sqlcode < 0 then					
					close righe_test_2;
					return -1
				end if
			
				if sqlca.sqlcode = 100 then
					close righe_test_2;
					ll_max = upperbound(fs_materia_prima)
					ll_max++		
					fs_materia_prima[ll_max] = ls_cod_prodotto_figlio[ll_t]
					fd_quan_utilizzo[ll_max] = ldd_quantita_utilizzo[ll_t]
			
					
				else
					// esiste un figlio per cui vado in profondità della distinta
					close righe_test_2;
					li_risposta = wf_trova_mat_prime(ls_cod_prodotto_figlio[ll_t], ls_cod_versione_figlio[ll_t], fs_materia_prima[],  fd_quan_utilizzo[], ldd_quantita_utilizzo[ll_t], fd_dim_x, fd_dim_y, fs_errore)
					if li_risposta < 0 then
						return -1
					end if
				end if	
				
			else
				// *** continuo come previsto
				ll_max = upperbound(fs_materia_prima)
				ll_max++		
				fs_materia_prima[ll_max] = ls_cod_prodotto_figlio[ll_t]
				fd_quan_utilizzo[ll_max] =ldd_quantita_utilizzo[ll_t]
		
			end if			
		else
		
			ll_max = upperbound(fs_materia_prima)
			ll_max++		
			fs_materia_prima[ll_max] = ls_cod_prodotto_figlio[ll_t]
			fd_quan_utilizzo[ll_max] =ldd_quantita_utilizzo[ll_t]
	
		end if
		
		// *** fine modifica Michela
		
	end if
next

return 0
end function

public subroutine wf_calcola_costo ();string			ls_mat_prima[],ls_cod_prodotto,ls_des_prodotto,ls_errore,ls_cod_versione,ls_test,ls_error, ls_chiave[], ls_vuoto[], ls_where, ls_flag_tipo_costo
integer		li_risposta, li_ret_funz
long			ll_t,ll_null, ll_new
dec{4}		ldd_quantita,ldd_quantita_totale,ldd_costo_materia_prima,ldd_costo_totale, ldd_dim_x, ldd_dim_y,  ldd_quan_utilizzo[], ld_quan_produzione, &
				ldd_costo_totale_mp, ldd_costo_totale_lav,ldd_costo_totale_ru, ld_quant_val[] , ld_vuoto[], ld_giacenza[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime		ldt_data_oggi
uo_magazzino			luo_magazzino
s_cs_xx_parametri		lstr_parametri



if ids_variabili.rowcount()>0 then
	//if not g_mb.confirm("Esistono valori per variabili impostate in calcoli precedenti. Le conservo?") then ids_variabili.reset()
	
	lstr_parametri.parametro_ds_1 = ids_variabili
	openwithparm(w_lista_variabili_veloce, lstr_parametri)
	lstr_parametri = message.powerobjectparm
	ids_variabili = lstr_parametri.parametro_ds_1
end if


ldt_data_oggi = datetime(today(), time("00:00:000"))
dw_ricerca_origine.accepttext()

ls_cod_prodotto = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_prodotto")
ls_cod_versione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_versione")
ls_flag_tipo_costo =  dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"flag_tipo_costo")
setnull(ls_test)

select cod_azienda
into   :ls_test
from   distinta_padri
where  cod_azienda=:s_cs_xx.cod_azienda and
		 cod_prodotto=:ls_cod_prodotto and
		 cod_versione=:ls_cod_versione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if

if isnull(ls_test) or ls_test = "" then
	g_mb.messagebox("Sep","Attenzione il prodotto selezionato non è un prodotto finito",stopsign!)
	return
end if

ldd_quantita = dw_ricerca_origine.getitemnumber(dw_ricerca_origine.getrow(),"quantita")
ldd_dim_x = dw_ricerca_origine.getitemnumber(dw_ricerca_origine.getrow(),"dim_x")
ldd_dim_y = dw_ricerca_origine.getitemnumber(dw_ricerca_origine.getrow(),"dim_y")
setnull(ll_null)

if ldd_quantita = 0 or isnull(ls_cod_prodotto) then
	g_mb.messagebox("Sep","Selezionare un prodotto e inserire una quantità",stopsign!)
	return
end if

tab_1.tab_materie_prime.dw_costi_materie_prime.reset()
tab_1.tab_lavorazioni.dw_costo_lavorazioni.reset()
tab_1.tab_risorse_umane.dw_costo_risorse_umane.reset()

s_cs_xx.parametri.parametro_s_5 = "REPORT_COSTO_PREVENTIVO"

li_risposta = wf_trova_mat_prime(ls_cod_prodotto,ls_cod_versione,ls_mat_prima[],ldd_quan_utilizzo[],ldd_quantita,ldd_dim_x,ldd_dim_y, ls_error)

if li_risposta<0 then
	g_mb.error(ls_error)
	return
end if

setnull(s_cs_xx.parametri.parametro_s_5)

for ll_t=1 to upperbound(ls_mat_prima)

	ldd_quantita_totale = ldd_quan_utilizzo[ll_t] 
	
	if ldd_quantita_totale>0 then
	else
		continue
	end if
	
	choose case ls_flag_tipo_costo
		case "A"
			select des_prodotto,prezzo_acquisto
			into	 :ls_des_prodotto,
					 :ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    cod_prodotto = :ls_mat_prima[ll_t];

		case "S"
			select des_prodotto,costo_standard
			into	 :ls_des_prodotto,
					 :ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    cod_prodotto = :ls_mat_prima[ll_t];

		case "U"
			select des_prodotto,costo_ultimo
			into	 :ls_des_prodotto,
					 :ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    cod_prodotto = :ls_mat_prima[ll_t];
  
		case "M"
	  
			luo_magazzino = CREATE uo_magazzino
			ls_where = ""
			ld_quant_val = ld_vuoto
			ld_giacenza  = ld_vuoto
			ld_costo_medio_stock = ld_vuoto
			ld_quan_costo_medio_stock = ld_vuoto
			ls_chiave = ls_vuoto
		
			li_ret_funz = luo_magazzino.uof_saldo_prod_date_decimal ( ls_mat_prima[ll_t], ldt_data_oggi, ls_where, ref ld_quant_val[], ref ls_error, 'S', ref ls_chiave[], ref ld_giacenza[], ref ld_costo_medio_stock[], ref ld_quan_costo_medio_stock[])
			
			destroy luo_magazzino
		
			// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
			// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
			// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
			// [12]=qta_costo_medio, [13]=val_costo_medio, [14]=val_costo_ultimo
			// costo medio = [13]/[12]; [12] e [13]: mov_magazzino con flag_costo_ultimo=S
			// costo ultimo: ultimo costo di acquisto alla data di riferimento
			
			select des_prodotto
			into	 :ls_des_prodotto	
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    cod_prodotto = :ls_mat_prima[ll_t];
			
			if ld_quant_val[12] > 0 then
				ldd_costo_materia_prima = ld_quant_val[13] / ld_quant_val[12]
			else
				select costo_medio_ponderato
				into   :ldd_costo_materia_prima
				from   lifo
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto and
						 anno_lifo = (select max(anno_lifo)
										  from   lifo
										  where  cod_azienda = :s_cs_xx.cod_azienda and
													cod_prodotto = :ls_cod_prodotto);
														
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Chiusure di magazzino: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
					rollback;
					return 
				elseif sqlca.sqlcode = 100 or isnull(ldd_costo_materia_prima) then
					ldd_costo_materia_prima = 0
				end if
			end if
		
			ldd_costo_materia_prima = round(ldd_costo_materia_prima, 4)
				
	end choose
  
  
	ldd_costo_totale = ldd_costo_materia_prima * ldd_quantita_totale
	
	ll_new = tab_1.tab_materie_prime.dw_costi_materie_prime.insertrow(0)
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem( ll_new, "cod_prodotto", ls_mat_prima[ll_t])
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem( ll_new, "des_prodotto", ls_des_prodotto)
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem( ll_new, "quan_utilizzo", ldd_quantita_totale)  
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem( ll_new, "costo_unitario", ldd_costo_materia_prima)  	
	tab_1.tab_materie_prime.dw_costi_materie_prime.setitem( ll_new, "costo_totale", ldd_costo_totale)  		

next

li_risposta=wf_fasi_lavorazione(ls_cod_prodotto,ls_cod_versione,ldd_quantita,ls_errore)

if tab_1.tab_materie_prime.dw_costi_materie_prime.rowcount() > 0 then
	ldd_costo_totale_mp =  tab_1.tab_materie_prime.dw_costi_materie_prime.getitemnumber(tab_1.tab_materie_prime.dw_costi_materie_prime.getrow(),"cf_costo_totale")
end if

if tab_1.tab_lavorazioni.dw_costo_lavorazioni.rowcount() > 0 then
	ldd_costo_totale_lav = tab_1.tab_lavorazioni.dw_costo_lavorazioni.getitemnumber(tab_1.tab_materie_prime.dw_costi_materie_prime.getrow(),"cf_costo_totale")
end if

if tab_1.tab_risorse_umane.dw_costo_risorse_umane.rowcount() > 0 then
	ldd_costo_totale_ru = tab_1.tab_risorse_umane.dw_costo_risorse_umane.getitemnumber(tab_1.tab_materie_prime.dw_costi_materie_prime.getrow(),"cf_costo_totale")
end if

if not(tab_1.tab_risorse_umane.cbx_tempo_risorsa_umana.checked) then ldd_costo_totale_ru = 0

dw_ricerca_origine.setitem(dw_ricerca_origine.getrow(), "costo_totale", ldd_costo_totale_mp + ldd_costo_totale_lav + ldd_costo_totale_ru)

g_mb.success("Procedura calcolo terminata!")


end subroutine

public function integer wf_genera_commessa (string fs_cod_prodotto_finito, string fs_cod_versione_finito, string fs_cod_tipo_commessa, decimal fd_quan_produzione, ref long fl_anno_commessa, ref long fl_num_commessa, ref string fs_errore);string 	ls_cod_operatore, ls_cod_deposito_versamento, ls_cod_deposito_prelievo, ls_errore,ls_flag_muovi_mp, ls_flag_tipo_impegno_mp
long 		ll_anno_commessa, ll_num_commessa, ll_ret
datetime	ldt_data_registrazione
uo_funzioni_1 luo_funzioni_1

ldt_data_registrazione = datetime (today(), now())
ll_anno_commessa = f_anno_esercizio()

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	select cod_operatore
	into :ls_cod_operatore
	from tab_operatori_utenti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente = :s_cs_xx.cod_utente and
			flag_default = 'S';
	if sqlca.sqlcode<> 0 then
		fs_errore = "Errore in ricerca associazione utente - operatore; verificare la tabella utenti-operatori. Dettaglio~r~n" + sqlca.sqlerrtext
		return -1
	end if
end if

select 	max(num_commessa)
into 		:ll_num_commessa
from 		anag_commesse
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa;

if isnull(ll_num_commessa) or ll_num_commessa < 1 then
	ll_num_commessa = 1 
else
	ll_num_commessa ++
end if

SELECT 	cod_deposito_prelievo,   
			cod_deposito_versamento,
			flag_muovi_mp,
			flag_tipo_impegno_mp
INTO 		:ls_cod_deposito_prelievo,   
			:ls_cod_deposito_versamento,
			:ls_flag_muovi_mp,
			:ls_flag_tipo_impegno_mp
FROM 	tab_tipi_commessa  
WHERE 	( cod_azienda = :s_cs_xx.cod_azienda ) and  
			( cod_tipo_commessa = :fs_cod_tipo_commessa )   ;
if sqlca.sqlcode = 100 then
	fs_errore = "Attenzione: non è stato specificato il tipo commessa oppure il tipo commesa non esiste"
	return -1
end if
if sqlca.sqlcode< 0 then
	fs_errore = "Errore in select tipo_commessa. Dettaglio~r~n" + sqlca.sqlerrtext
	return -1
end if


INSERT INTO anag_commesse  
	( cod_azienda,   
	  anno_commessa,   
	  num_commessa,   
	  cod_prodotto,   
	  cod_versione,   
	  cod_tipo_commessa,   
	  data_registrazione,   
	  cod_operatore,   
	  nota_testata,   
	  nota_piede,   
	  flag_blocco,   
	  quan_ordine,   
	  quan_prodotta,   
	  data_chiusura,   
	  data_consegna,   
	  quan_assegnata,   
	  quan_in_produzione,   
	  cod_deposito_versamento,   
	  cod_deposito_prelievo,   
	  flag_tipo_avanzamento,   
	  ubicazione,   
	  lotto,   
	  cod_centro_costo,   
	  num_ore_preventivo,   
	  tot_valore_prodotti,   
	  tot_valore_servizi,   
	  tot_valore_pf,   
	  data_produzione,   
	  flag_tassativo )  
VALUES ( :s_cs_xx.cod_azienda,   
	  :ll_anno_commessa,   
	  :ll_num_commessa,   
	  :fs_cod_prodotto_finito,   
	  :fs_cod_versione_finito,   
	  :fs_cod_tipo_commessa,   
	  :ldt_data_registrazione,   
	  :ls_cod_operatore,   
	  null,   
	  null,   
	  'N',   
	  :fd_quan_produzione,   
	  0,   
	  null,   
	  :ldt_data_registrazione,   
	  0,   
	  0,   
	  :ls_cod_deposito_versamento,   
	  :ls_cod_deposito_prelievo,   
	  '0',   
	  null,   
	  null,   
	  null,   
	  0,   
	  0,   
	  0,   
	  0,   
	  null,   
	  'S')  ;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in Inserimento della Commessa~r~n" + sqlca.sqlerrtext
	return -1
end if

// inizio ciclo di scorrimento della distinta e inserimento varianti

luo_funzioni_1 = create uo_funzioni_1

luo_funzioni_1.id_dim_x = dw_ricerca_origine.getitemnumber(dw_ricerca_origine.getrow(),"dim_x")
luo_funzioni_1.id_dim_y = dw_ricerca_origine.getitemnumber(dw_ricerca_origine.getrow(),"dim_y")

ll_ret = luo_funzioni_1.uof_valorizza_formule_distinta(fs_cod_prodotto_finito, fs_cod_versione_finito, ll_anno_commessa, ll_num_commessa, 0, "COMMESSE", ls_errore)

if ll_ret < 0 then
	fs_errore = "Errore nella funzione di valorizzazione formule (uof_valorizza_formule_distinta)~r~n" + ls_errore
	return -1
end if

destroy luo_funzioni_1

if ls_flag_muovi_mp = 'S' then
	
	if not isnull(ls_flag_tipo_impegno_mp) and ls_flag_tipo_impegno_mp = "S" then
		
		uo_magazzino luo_magazzino
		luo_magazzino = create uo_magazzino
	
		if luo_magazzino.uof_impegna_mp_commessa(	true, &
																false, &
																fs_cod_prodotto_finito, &
																fs_cod_versione_finito, &
																fd_quan_produzione, &
																ll_anno_commessa, &
																ll_num_commessa, &
																ls_errore) = -1 then
			fs_errore = "Errore nella funzione di impegno magazzino~r~n" + ls_errore
			return -1
		end if
		
		destroy luo_magazzino
		
	end if
end if	

fl_anno_commessa = ll_anno_commessa
fl_num_commessa = ll_num_commessa

return 0
end function

on w_costo_preventivo_produzione.create
int iCurrent
call super::create
this.tab_1=create tab_1
this.dw_ricerca_origine=create dw_ricerca_origine
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
this.Control[iCurrent+2]=this.dw_ricerca_origine
this.Control[iCurrent+3]=this.cb_1
end on

on w_costo_preventivo_produzione.destroy
call super::destroy
destroy(this.tab_1)
destroy(this.dw_ricerca_origine)
destroy(this.cb_1)
end on

event open;call super::open;if s_cs_xx.parametri.parametro_i_1 = 1 then
	dw_ricerca_origine.setitem(1,"cod_prodotto",s_cs_xx.parametri.parametro_s_1)
	
	
	dw_ricerca_origine.setitem(1,"cod_versione",s_cs_xx.parametri.parametro_s_2)
	dw_ricerca_origine.setitem(1,"quantita",long(s_cs_xx.parametri.parametro_s_3))
	wf_calcola_costo()
	
end if
end event

event close;call super::close;s_cs_xx.parametri.parametro_i_1 = 0

destroy ids_variabili
destroy iuo_formule
end event

event pc_setwindow;call super::pc_setwindow;Set_W_Options(c_NoResizeWin)


iuo_formule = create  uo_formule_calcolo
		
//modalità gibus
iuo_formule.ib_gibus = true
		
//modalità testing
iuo_formule.ii_anno_reg_ord_ven = 0
iuo_formule.il_num_reg_ord_ven = 0
iuo_formule.il_prog_riga_ord_ven = 0
iuo_formule.is_tipo_ritorno = "1"		//numerico

iuo_formule.uof_crea_ds_variabili_vuoto(ids_variabili)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ricerca_origine, &
							"cod_tipo_commessa",&
							sqlca,&
                 				"tab_tipi_commessa",&
							"cod_tipo_commessa",&
							"des_tipo_commessa",&
                 				"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event resize;// tenere il flag_extende disattivato.
dw_ricerca_origine.x = 25
dw_ricerca_origine.x = 25
dw_ricerca_origine.height = 700 
dw_ricerca_origine.width = this.width - 100

tab_1.x = 25
tab_1.y = 780
tab_1.height = this.height - tab_1.y - 200
tab_1.width = this.width - 100

tab_1.tab_materie_prime.dw_costi_materie_prime.x = 10
tab_1.tab_materie_prime.dw_costi_materie_prime.y = 10 
tab_1.tab_materie_prime.dw_costi_materie_prime.height = tab_1.height - 150
tab_1.tab_materie_prime.dw_costi_materie_prime.width = tab_1.width - 20

tab_1.tab_lavorazioni.dw_costo_lavorazioni.x = 10
tab_1.tab_lavorazioni.dw_costo_lavorazioni.y = 10 
tab_1.tab_lavorazioni.dw_costo_lavorazioni.height = tab_1.height - 250
tab_1.tab_lavorazioni.dw_costo_lavorazioni.width = tab_1.width - 20

tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio.x = 25
tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio.y = tab_1.height -200
tab_1.tab_lavorazioni.cbx_tempo_lavorazione.x = 725
tab_1.tab_lavorazioni.cbx_tempo_lavorazione.y = tab_1.height -200
tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio_commessa.x = 1450
tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio_commessa.y = tab_1.height -200

tab_1.tab_risorse_umane.dw_costo_risorse_umane.x = 10
tab_1.tab_risorse_umane.dw_costo_risorse_umane.y = 10 
tab_1.tab_risorse_umane.dw_costo_risorse_umane.height = tab_1.height - 250
tab_1.tab_risorse_umane.dw_costo_risorse_umane.width = tab_1.width - 20

tab_1.tab_risorse_umane.cbx_tempo_risorsa_umana.x = 25
tab_1.tab_risorse_umane.cbx_tempo_risorsa_umana.y = tab_1.height -200
end event

type tab_1 from tab within w_costo_preventivo_produzione
integer x = 23
integer y = 780
integer width = 3602
integer height = 1628
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean powertips = true
integer selectedtab = 1
tab_materie_prime tab_materie_prime
tab_lavorazioni tab_lavorazioni
tab_risorse_umane tab_risorse_umane
end type

on tab_1.create
this.tab_materie_prime=create tab_materie_prime
this.tab_lavorazioni=create tab_lavorazioni
this.tab_risorse_umane=create tab_risorse_umane
this.Control[]={this.tab_materie_prime,&
this.tab_lavorazioni,&
this.tab_risorse_umane}
end on

on tab_1.destroy
destroy(this.tab_materie_prime)
destroy(this.tab_lavorazioni)
destroy(this.tab_risorse_umane)
end on

type tab_materie_prime from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3566
integer height = 1504
long backcolor = 12632256
string text = "Materie Prime"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 553648127
dw_costi_materie_prime dw_costi_materie_prime
end type

on tab_materie_prime.create
this.dw_costi_materie_prime=create dw_costi_materie_prime
this.Control[]={this.dw_costi_materie_prime}
end on

on tab_materie_prime.destroy
destroy(this.dw_costi_materie_prime)
end on

type dw_costi_materie_prime from datawindow within tab_materie_prime
integer x = 5
integer y = 28
integer width = 3529
integer height = 1420
integer taborder = 41
string dataobject = "d_costo_previstivo_produzione_materie_prime"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tab_lavorazioni from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3566
integer height = 1504
long backcolor = 12632256
string text = "Lavorazioni"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_costo_lavorazioni dw_costo_lavorazioni
cbx_tempo_lavorazione cbx_tempo_lavorazione
cbx_tempo_attrezzaggio cbx_tempo_attrezzaggio
cbx_tempo_attrezzaggio_commessa cbx_tempo_attrezzaggio_commessa
end type

on tab_lavorazioni.create
this.dw_costo_lavorazioni=create dw_costo_lavorazioni
this.cbx_tempo_lavorazione=create cbx_tempo_lavorazione
this.cbx_tempo_attrezzaggio=create cbx_tempo_attrezzaggio
this.cbx_tempo_attrezzaggio_commessa=create cbx_tempo_attrezzaggio_commessa
this.Control[]={this.dw_costo_lavorazioni,&
this.cbx_tempo_lavorazione,&
this.cbx_tempo_attrezzaggio,&
this.cbx_tempo_attrezzaggio_commessa}
end on

on tab_lavorazioni.destroy
destroy(this.dw_costo_lavorazioni)
destroy(this.cbx_tempo_lavorazione)
destroy(this.cbx_tempo_attrezzaggio)
destroy(this.cbx_tempo_attrezzaggio_commessa)
end on

type dw_costo_lavorazioni from datawindow within tab_lavorazioni
integer x = 5
integer y = 52
integer width = 3474
integer height = 1240
integer taborder = 31
boolean bringtotop = true
string dataobject = "d_costo_lavorazioni_prev"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cbx_tempo_lavorazione from checkbox within tab_lavorazioni
integer x = 27
integer y = 1396
integer width = 558
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "tempo lavorazione"
boolean checked = true
boolean lefttext = true
end type

type cbx_tempo_attrezzaggio from checkbox within tab_lavorazioni
integer x = 713
integer y = 1396
integer width = 581
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "tempo attrezzaggio"
boolean checked = true
boolean lefttext = true
end type

type cbx_tempo_attrezzaggio_commessa from checkbox within tab_lavorazioni
integer x = 1413
integer y = 1392
integer width = 887
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "tempo attrezzaggio commessa"
boolean checked = true
boolean lefttext = true
end type

type tab_risorse_umane from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3566
integer height = 1504
long backcolor = 12632256
string text = "Risorse Umane"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_costo_risorse_umane dw_costo_risorse_umane
cbx_tempo_risorsa_umana cbx_tempo_risorsa_umana
end type

on tab_risorse_umane.create
this.dw_costo_risorse_umane=create dw_costo_risorse_umane
this.cbx_tempo_risorsa_umana=create cbx_tempo_risorsa_umana
this.Control[]={this.dw_costo_risorse_umane,&
this.cbx_tempo_risorsa_umana}
end on

on tab_risorse_umane.destroy
destroy(this.dw_costo_risorse_umane)
destroy(this.cbx_tempo_risorsa_umana)
end on

type dw_costo_risorse_umane from datawindow within tab_risorse_umane
integer x = 5
integer y = 52
integer width = 3474
integer height = 1220
integer taborder = 41
boolean bringtotop = true
string dataobject = "d_costo_risorse_umane_prev"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cbx_tempo_risorsa_umana from checkbox within tab_risorse_umane
integer x = 5
integer y = 1292
integer width = 754
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tempo Risorsa Umana"
boolean checked = true
boolean lefttext = true
end type

type dw_ricerca_origine from u_dw_search within w_costo_preventivo_produzione
event ue_key pbm_dwnkey
integer x = 23
integer y = 16
integer width = 3602
integer height = 732
integer taborder = 20
string dataobject = "d_costo_preventivo_produzione_sel"
end type

event itemchanged;call super::itemchanged;string			ls_null, ls_cod_versione_default


setnull(ls_null)

choose case dwo.name
		
	case "cod_prodotto"
		
		setitem(row,"cod_versione",ls_null)

		if not isnull(data) and len(data) > 0 then
		
			f_po_loaddddw_dw(dw_ricerca_origine, &
										"cod_versione",&
										sqlca,&
										"distinta_padri",&
										"cod_versione",&
										"des_versione",&
										"cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + data + "'")
			select cod_versione
			into :ls_cod_versione_default
			from distinta_padri
			where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto = :data and flag_predefinita = 'S' ;
			
			if sqlca.sqlcode = 0 then
				setitem(row,"cod_versione",ls_cod_versione_default)
			end if	
		
		end if
		
	
		
end choose
end event

event buttonclicked;call super::buttonclicked;string ls_mat_prima[],ls_cod_prodotto,ls_des_prodotto,ls_errore,ls_cod_versione,ls_test,ls_error, ls_chiave[], ls_vuoto[], ls_where, ls_flag_tipo_costo, &
		ls_cod_tipo_commessa, ls_flag_avanza_produzione, ls_null, ls_cod_deposito_scarico_mp
integer li_risposta, li_ret_funz
long   ll_t,ll_null, ll_anno_commessa, ll_num_commessa
dec{4} ldd_quantita,ldd_quantita_totale,ldd_costo_materia_prima,ldd_costo_totale, ldd_dim_x, ldd_dim_y,  ldd_quan_utilizzo[], ld_quan_produzione, &
       ldd_costo_totale_mp, ldd_costo_totale_lav,ldd_costo_totale_ru, ld_quant_val[] , ld_vuoto[], ld_giacenza[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime ldt_data_oggi
uo_magazzino luo_magazzino
uo_funzioni_1 luo_funzioni_1

setnull(ls_null)

choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca_origine,"cod_prodotto")
		
		
	case "b_gen_commessa"
		ls_cod_prodotto = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_prodotto")
		ls_cod_versione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_versione")
		ld_quan_produzione = dw_ricerca_origine.getitemnumber(dw_ricerca_origine.getrow(),"quantita")
		ls_cod_tipo_commessa =  dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_tipo_commessa")
		
		if isnull(ls_cod_tipo_commessa) then
			g_mb.error("Tipo Commessa Obbligatorio!")
			return
		end if
		
		if wf_genera_commessa(ls_cod_prodotto, ls_cod_versione, ls_cod_tipo_commessa, ld_quan_produzione, ref ll_anno_commessa, ref ll_num_commessa, ref ls_errore) = 0 then
		
			ls_flag_avanza_produzione =  dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"flag_avanza_produzione")
			
			if ls_flag_avanza_produzione = "S" then
				luo_funzioni_1 = create uo_funzioni_1
				
				select cod_deposito_prelievo
				into :ls_cod_deposito_scarico_mp
				from tab_tipi_commessa
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_tipo_commessa = :ls_cod_tipo_commessa;
				if sqlca.sqlcode <> 0 then
					g_mb.error("Errore in ricerca Tipo Commessa " + sqlca.sqlerrtext)
					return
				end if
				
				luo_funzioni_1.ib_forza_deposito_scarico_mp = true
				luo_funzioni_1.is_cod_deposito_scarico_mp = ls_cod_deposito_scarico_mp
				luo_funzioni_1.idt_data_creazione_mov = dw_ricerca_origine.getitemdatetime(dw_ricerca_origine.getrow(),"data_chiusura")
				if luo_funzioni_1.uof_avanza_commessa(ll_anno_commessa, ll_num_commessa, ls_null,ls_errore) = 0 then
					commit ;
					g_mb.messagebox("SEP","GENERATA ED ESEGUITO AVANZAMENTO DELLA COMMESSA " + string(ll_anno_commessa, "####") + "-" + string(ll_num_commessa,"######"))
				else
					rollback;
					g_mb.messagebox("SEP","ERRORE IN AVANZAMENTO DELLA COMMESSA " + string(ll_anno_commessa, "####") + "-" + string(ll_num_commessa,"######") + "ATTENZIONE: tutta la commessa viene anuulata~r~nDettaglio Errore:~r~n" + ls_errore)
				end if					
				destroy luo_funzioni_1
			else
				commit;
				g_mb.messagebox("SEP","GENERATA COMMESSA " + string(ll_anno_commessa, "####") + "-" + string(ll_num_commessa,"######"))
			end if
		else
			rollback;
			g_mb.messagebox("SEP","ERRORE IN CREAZIONE DELLA COMMESSA " + string(ll_anno_commessa, "####") + "-" + string(ll_num_commessa,"######") +"~r~n" + ls_errore)
		end if
		
		
	case "b_report"
		dw_ricerca_origine.accepttext()
		
		ls_cod_prodotto = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_prodotto")
		ls_cod_versione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_versione")
		ls_flag_tipo_costo =  dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"flag_tipo_costo")
		
		if isnull(ls_cod_prodotto) or isnull(ls_cod_versione) or ls_cod_prodotto="" or ls_cod_versione = "" then
			g_mb.messagebox("Sep","Non è possibile calcolare il costo poichè non è selezionato alcun prodotto!",stopsign!)
			return
		end if
		
		s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
		s_cs_xx.parametri.parametro_s_2 = ls_cod_versione
		s_cs_xx.parametri.parametro_s_3 = string(tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio.checked)
		s_cs_xx.parametri.parametro_s_4 = string(tab_1.tab_lavorazioni.cbx_tempo_attrezzaggio_commessa.checked)
		s_cs_xx.parametri.parametro_s_5 = string(tab_1.tab_lavorazioni.cbx_tempo_lavorazione.checked)
		s_cs_xx.parametri.parametro_s_6 = "false"
		s_cs_xx.parametri.parametro_s_7 = "false"
		s_cs_xx.parametri.parametro_s_8 = "false"
		
		choose case ls_flag_tipo_costo
			case "A"
				s_cs_xx.parametri.parametro_s_8 = "true"
			case "S"
				s_cs_xx.parametri.parametro_s_6 = "true"
			case "U"
				s_cs_xx.parametri.parametro_s_7 = "true"
		end choose
		
		s_cs_xx.parametri.parametro_s_9 = string(tab_1.tab_risorse_umane.cbx_tempo_risorsa_umana.checked)
		s_cs_xx.parametri.parametro_d_1 = dw_ricerca_origine.getitemnumber(dw_ricerca_origine.getrow(),"quantita")
		
		
		window_open( w_report_costo_preventivo, -1)		
		

	case "b_calcola"
		tab_1.tab_lavorazioni.dw_costo_lavorazioni.setredraw(false)
		tab_1.tab_materie_prime.dw_costi_materie_prime.setredraw(false)
		tab_1.tab_risorse_umane.dw_costo_risorse_umane.setredraw(false)
		wf_calcola_costo()
		tab_1.tab_lavorazioni.dw_costo_lavorazioni.setredraw(true)
		tab_1.tab_materie_prime.dw_costi_materie_prime.setredraw(true)
		tab_1.tab_risorse_umane.dw_costo_risorse_umane.setredraw(true)
		

	case "b_excel"
		string   ls_rag_soc
		double  ldd_quan_utilizzo1[]
		
		dw_ricerca_origine.accepttext()
		
		ls_cod_prodotto = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_prodotto")
		ls_cod_versione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_versione")
		setnull(ls_test)
		
		select cod_azienda
		into   :ls_test
		from   distinta_padri
		where  cod_azienda=:s_cs_xx.cod_azienda and
				 cod_prodotto=:ls_cod_prodotto and
				 cod_versione=:ls_cod_versione;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
		if isnull(ls_test) or ls_test = "" then
			g_mb.messagebox("Sep","Attenzione il prodotto selezionato non è un prodotto finito",stopsign!)
			return
		end if
		
		ldd_quantita = dw_ricerca_origine.getitemnumber(dw_ricerca_origine.getrow(),"quantita")
		//setnull(ll_null)
		
		if ldd_quantita = 0 and isnull(ls_cod_prodotto) then
			g_mb.messagebox("Sep","Selezionare un prodotto e inserire una quantità",stopsign!)
			return
		end if
		
		long ll_riga, ll_colonna, ll_celle_sep_riga, ll_colonna_temp,  ll_riga_temp, ll_delta_riga
		string ls_valore
		double ld_costo_totale
		
		
		//PREPARA L'ARRAY DELLE mp
		SETNULL(ll_null)
		s_cs_xx.parametri.parametro_s_5 = "REPORT_COSTO_PREVENTIVO"
		f_trova_mat_prima(ls_cod_prodotto,ls_cod_versione,ls_mat_prima[],ldd_quan_utilizzo1[],1,ll_null,ll_null)
		
		iuo_excel = create uo_excel_preventivo
		iuo_excel.uof_init()
		
		//n° di celle spostamento dal prodotto al costo
		ll_celle_sep_riga = 6
		
		//valori iniziali di partenza
		ll_riga = 1
		ll_colonna = 1
		
		
		//titolo
		iuo_excel.uof_intestazione( ll_riga, ll_colonna, "PREVENTIVO COSTI PRODUZIONE del " + string(today(), "dd/mm/yyyy") )
		
		//Prodotto
		ll_riga += 2
		ls_valore = ls_cod_prodotto + ", " + dw_ricerca_origine.object.cf_des_prodotto[1]
		iuo_excel.uof_crea_riga_pf( ll_riga, ll_colonna, "Prodotto:" ,"2")
		iuo_excel.uof_crea_riga_pf( ll_riga, ll_colonna + 1, ls_valore ,"2")
		
		//Versione e Quantità
		ll_riga += 1
		
		iuo_excel.uof_crea_riga_pf( ll_riga, ll_colonna, "Versione:" ,"2")
		iuo_excel.uof_crea_riga_pf( ll_riga, ll_colonna + 1, "'"+ls_cod_versione ,"2")
		
		iuo_excel.uof_crea_riga_pf( ll_riga, ll_colonna + 6, "Quantità:" ,"2")
		iuo_excel.uof_crea_riga_pf( ll_riga, ll_colonna + 7, string(ldd_quantita, "0.000") ,"2")
		
		ll_riga += 2
		//-----------------------------
		
		
		ll_colonna_temp = ll_colonna
		//codice e descrizione
		iuo_excel.uof_crea_riga_pf( ll_riga, ll_colonna_temp, ls_valore ,"2")
		
		//quantita
		//spostati a dx di "ll_celle_sep_riga" celle
		ll_colonna_temp += ll_celle_sep_riga
		
		setnull(idd_qta_pf)
		
		//n° pezzi che si vogliono produrre di PF
		idd_qta_pf = ldd_quantita
		
		//####################################################
		//inserita formula sul valore totale del PF
		
		//memorizza la riga prima
		ll_riga_temp = ll_riga
		
		//prima di elaborare il costo totale del pf popola tutte le lavorazioni o Mp di primo livello
		wf_fasi_lavorazione_excel(	ls_cod_prodotto, &
												ls_cod_versione, &
												idd_qta_pf, &
												ls_errore, &
												ll_riga, &
												ll_colonna, &
												ll_celle_sep_riga, &
												ls_mat_prima[],&
												ldd_quan_utilizzo1[])
		
		
		ll_delta_riga = ll_riga - ll_riga_temp
		if ll_delta_riga > 0 then
			//piazza la formula
			iuo_excel.uof_crea_riga_pf_costo( 	ll_riga_temp, &
																ll_colonna_temp, &
																"4", &
																ll_riga_temp+1, &
																ll_riga)
		end if
		
		destroy iuo_excel
		
end choose
end event

type cb_1 from commandbutton within w_costo_preventivo_produzione
boolean visible = false
integer x = 2651
integer y = 100
integer width = 320
integer height = 120
integer taborder = 51
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
end type

event clicked;string ls_cod_prodotto, ls_cod_versione, ls_errore
dec{4} ld_totale
uo_calcolo_costi_pf luo_calcolo_costi_pf

luo_calcolo_costi_pf = create uo_calcolo_costi_pf

ls_cod_prodotto = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_prodotto")
ls_cod_versione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_versione")
ld_totale = luo_calcolo_costi_pf.uof_costo_totale_prodotto(ls_cod_prodotto, ls_cod_versione, "A", ref ls_errore)
if ld_totale < 0 then
	messagebox("",ls_errore)
else
	messagebox("OK",ld_totale)
end if
end event

