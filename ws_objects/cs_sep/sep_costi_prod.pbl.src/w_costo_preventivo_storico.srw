﻿$PBExportHeader$w_costo_preventivo_storico.srw
$PBExportComments$Window costo preventivo dello storico di una distinta
forward
global type w_costo_preventivo_storico from w_cs_xx_principale
end type
type ddlb_prog_storico from dropdownlistbox within w_costo_preventivo_storico
end type
type st_3 from statictext within w_costo_preventivo_storico
end type
type cb_calcola from commandbutton within w_costo_preventivo_storico
end type
type tab_1 from tab within w_costo_preventivo_storico
end type
type tab_materie_prime from userobject within tab_1
end type
type rb_prezzo_ac from radiobutton within tab_materie_prime
end type
type rb_costo_ul from radiobutton within tab_materie_prime
end type
type dw_costi_materie_prime from datawindow within tab_materie_prime
end type
type rb_costo_st from radiobutton within tab_materie_prime
end type
type tab_materie_prime from userobject within tab_1
rb_prezzo_ac rb_prezzo_ac
rb_costo_ul rb_costo_ul
dw_costi_materie_prime dw_costi_materie_prime
rb_costo_st rb_costo_st
end type
type tab_1 from tab within w_costo_preventivo_storico
tab_materie_prime tab_materie_prime
end type
type st_1 from statictext within w_costo_preventivo_storico
end type
type st_costo_totale_complessivo from statictext within w_costo_preventivo_storico
end type
type dw_ricerca_origine from u_dw_search within w_costo_preventivo_storico
end type
end forward

global type w_costo_preventivo_storico from w_cs_xx_principale
integer width = 3607
integer height = 2068
string title = "Costo Preventivo Prodotto"
ddlb_prog_storico ddlb_prog_storico
st_3 st_3
cb_calcola cb_calcola
tab_1 tab_1
st_1 st_1
st_costo_totale_complessivo st_costo_totale_complessivo
dw_ricerca_origine dw_ricerca_origine
end type
global w_costo_preventivo_storico w_costo_preventivo_storico

type variables
long il_progressivo, il_progressivi[]
end variables

on w_costo_preventivo_storico.create
int iCurrent
call super::create
this.ddlb_prog_storico=create ddlb_prog_storico
this.st_3=create st_3
this.cb_calcola=create cb_calcola
this.tab_1=create tab_1
this.st_1=create st_1
this.st_costo_totale_complessivo=create st_costo_totale_complessivo
this.dw_ricerca_origine=create dw_ricerca_origine
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ddlb_prog_storico
this.Control[iCurrent+2]=this.st_3
this.Control[iCurrent+3]=this.cb_calcola
this.Control[iCurrent+4]=this.tab_1
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.st_costo_totale_complessivo
this.Control[iCurrent+7]=this.dw_ricerca_origine
end on

on w_costo_preventivo_storico.destroy
call super::destroy
destroy(this.ddlb_prog_storico)
destroy(this.st_3)
destroy(this.cb_calcola)
destroy(this.tab_1)
destroy(this.st_1)
destroy(this.st_costo_totale_complessivo)
destroy(this.dw_ricerca_origine)
end on

event open;call super::open;integer li_i

if s_cs_xx.parametri.parametro_i_1 = 1 then
	
	dw_ricerca_origine.setitem(1,"cod_prodotto",s_cs_xx.parametri.parametro_s_1)
	
	f_po_loaddddw_dw(dw_ricerca_origine,"cod_versione",sqlca,"distinta_padri_storico","DISTINCT cod_versione","cod_versione", &
				  "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + s_cs_xx.parametri.parametro_s_1 + "'")

	dw_ricerca_origine.setitem(1,"cod_versione",s_cs_xx.parametri.parametro_s_2)
	
	dw_ricerca_origine.setitem(1,"quantita",long(s_cs_xx.parametri.parametro_s_3))
	
	ddlb_prog_storico.triggerevent("getfocus")
	
	if upperbound(il_progressivi) > 0 then
	
		for li_i = 1 to upperbound(il_progressivi)
			
			if il_progressivi[li_i] = s_cs_xx.parametri.parametro_d_1 then
				
				ddlb_prog_storico.selectitem(li_i)
				
				il_progressivo = il_progressivi[li_I]
				
			end if
			
		next
	
	end if
	
	cb_calcola.triggerevent("clicked")
	
end if
end event

event close;call super::close;s_cs_xx.parametri.parametro_s_1 = ""

s_cs_xx.parametri.parametro_s_2 = ""

s_cs_xx.parametri.parametro_s_3 = ""

s_cs_xx.parametri.parametro_d_1 = 0

s_cs_xx.parametri.parametro_i_1 = 0

end event

type ddlb_prog_storico from dropdownlistbox within w_costo_preventivo_storico
integer x = 1600
integer y = 200
integer width = 686
integer height = 820
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;il_progressivo = index
end event

event getfocus;string   ls_cod_versione, ls_cod_prodotto

long     ll_prog_storico, ll_vuoto[], ll_i

datetime ldt_data_ora_storico


ls_cod_prodotto = dw_ricerca_origine.getitemstring( dw_ricerca_origine.getrow(), "cod_prodotto")

ls_cod_versione = dw_ricerca_origine.getitemstring( dw_ricerca_origine.getrow(), "cod_versione")

if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then	
	return -1
end if

if isnull(ls_cod_versione) or ls_cod_versione = "" then	
	return -1
end if

ddlb_prog_storico.reset()

il_progressivi = ll_vuoto

declare cu_date cursor for
select prog_storico,
       data_ora_storico
from   distinta_padri_storico
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto and
		 cod_versione = :ls_cod_versione
order  by data_ora_storico;
		 
open cu_date;

ll_i = 0

do while 1 = 1 
	fetch cu_date into :ll_prog_storico,
	                   :ldt_data_ora_storico;
							 
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = -1 then exit
	
	ddlb_prog_storico.additem( string( ldt_data_ora_storico, "dd/mm/yyyy hh:mm"))
	
	ll_i = ll_i + 1
	
	il_progressivi[ll_i] = ll_prog_storico
	
loop
	
if ll_i > 0 then
	
	ddlb_prog_storico.selectitem( 1)
	
	il_progressivo = 1
	
end if
	
close cu_date;


end event

type st_3 from statictext within w_costo_preventivo_storico
integer x = 1326
integer y = 200
integer width = 251
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Data:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_calcola from commandbutton within w_costo_preventivo_storico
integer x = 23
integer y = 1860
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcola"
end type

event clicked;integer li_risposta

string ls_mat_prima[],ls_cod_prodotto,ls_des_prodotto,ls_errore,ls_cod_versione,ls_test

dec{4} ldd_quantita,ldd_quantita_totale,ldd_costo_materia_prima,ldd_costo_totale, &
       ldd_costo_totale_mp, ldd_costo_totale_lav,ldd_costo_totale_ru
		 
long   ll_t,ll_null, ll_progressivo

double ldd_quan_utilizzo[]


dw_ricerca_origine.accepttext()

ls_cod_prodotto = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_prodotto")

ls_cod_versione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_versione")

ll_progressivo = il_progressivi[il_progressivo]

ldd_quantita = dw_ricerca_origine.getitemnumber(dw_ricerca_origine.getrow(),"quantita")

setnull(ll_null)

setnull(ls_test)

select cod_azienda
into   :ls_test
from   distinta_padri_storico
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_prodotto = :ls_cod_prodotto and    
		 cod_versione = :ls_cod_versione and
		 prog_storico = :ll_progressivo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore durante controllo esistenza prodotto:" + sqlca.sqlerrtext,stopsign!)
	return
end if

if isnull(ls_test) or ls_test = "" then
	g_mb.messagebox("Sep","Attenzione il prodotto selezionato non è un prodotto finito",stopsign!)
	return
end if

if ldd_quantita = 0 or isnull(ls_cod_prodotto) then
	g_mb.messagebox("Sep","Selezionare un prodotto e inserire una quantità",stopsign!)
	return
end if

tab_1.tab_materie_prime.dw_costi_materie_prime.reset()

li_risposta = f_trova_mat_prima_storico( ls_cod_prodotto, &
                                         ls_cod_versione, &
													  il_progressivo, &
													  ls_mat_prima[],  &
													  ldd_quan_utilizzo[], &
													  1)

for ll_t = 1 to upperbound(ls_mat_prima)

  ldd_quantita_totale = ldd_quan_utilizzo[ll_t] * ldd_quantita

  if tab_1.tab_materie_prime.rb_prezzo_ac.checked = true then
	
		select des_prodotto,prezzo_acquisto
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       cod_prodotto = :ls_mat_prima[ll_t];
		
  end if

  if tab_1.tab_materie_prime.rb_costo_st.checked = true then
	
		select des_prodotto,costo_standard
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda	and    
		       cod_prodotto = :ls_mat_prima[ll_t];
		
  end if

  if tab_1.tab_materie_prime.rb_costo_ul.checked = true then
	
		select des_prodotto,costo_ultimo
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda	and    
		       cod_prodotto = :ls_mat_prima[ll_t];
		
  end if
	
  ldd_costo_totale = ldd_costo_materia_prima * ldd_quantita_totale

  tab_1.tab_materie_prime.dw_costi_materie_prime.insertrow(1)
  tab_1.tab_materie_prime.dw_costi_materie_prime.setitem( 1, "cod_prodotto", ls_mat_prima[ll_t])
  tab_1.tab_materie_prime.dw_costi_materie_prime.setitem( 1, "des_prodotto", ls_des_prodotto)
  tab_1.tab_materie_prime.dw_costi_materie_prime.setitem( 1, "quan_utilizzo", ldd_quantita_totale)  
  tab_1.tab_materie_prime.dw_costi_materie_prime.setitem( 1, "costo_unitario", ldd_costo_materia_prima)  	
  tab_1.tab_materie_prime.dw_costi_materie_prime.setitem( 1, "costo_totale", ldd_costo_totale)  		

next

if tab_1.tab_materie_prime.dw_costi_materie_prime.rowcount() > 0 then
	ldd_costo_totale_mp =  tab_1.tab_materie_prime.dw_costi_materie_prime.getitemnumber(tab_1.tab_materie_prime.dw_costi_materie_prime.getrow(),"cf_costo_totale")
end if

st_costo_totale_complessivo.text = string(ldd_costo_totale_mp + ldd_costo_totale_lav + ldd_costo_totale_ru, "[currency]")
end event

type tab_1 from tab within w_costo_preventivo_storico
integer x = 23
integer y = 340
integer width = 3520
integer height = 1500
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean powertips = true
integer selectedtab = 1
tab_materie_prime tab_materie_prime
end type

on tab_1.create
this.tab_materie_prime=create tab_materie_prime
this.Control[]={this.tab_materie_prime}
end on

on tab_1.destroy
destroy(this.tab_materie_prime)
end on

type tab_materie_prime from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3483
integer height = 1376
long backcolor = 12632256
string text = "Materie Prime"
long tabtextcolor = 8388608
long tabbackcolor = 79741120
long picturemaskcolor = 553648127
rb_prezzo_ac rb_prezzo_ac
rb_costo_ul rb_costo_ul
dw_costi_materie_prime dw_costi_materie_prime
rb_costo_st rb_costo_st
end type

on tab_materie_prime.create
this.rb_prezzo_ac=create rb_prezzo_ac
this.rb_costo_ul=create rb_costo_ul
this.dw_costi_materie_prime=create dw_costi_materie_prime
this.rb_costo_st=create rb_costo_st
this.Control[]={this.rb_prezzo_ac,&
this.rb_costo_ul,&
this.dw_costi_materie_prime,&
this.rb_costo_st}
end on

on tab_materie_prime.destroy
destroy(this.rb_prezzo_ac)
destroy(this.rb_costo_ul)
destroy(this.dw_costi_materie_prime)
destroy(this.rb_costo_st)
end on

type rb_prezzo_ac from radiobutton within tab_materie_prime
integer x = 27
integer y = 1292
integer width = 503
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Prezzo Acquisto"
boolean checked = true
boolean lefttext = true
end type

type rb_costo_ul from radiobutton within tab_materie_prime
integer x = 576
integer y = 1292
integer width = 411
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Costo Ultimo"
boolean lefttext = true
end type

type dw_costi_materie_prime from datawindow within tab_materie_prime
integer x = 5
integer y = 52
integer width = 3474
integer height = 1220
integer taborder = 41
string dataobject = "d_costi_materie_prime"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type rb_costo_st from radiobutton within tab_materie_prime
integer x = 1033
integer y = 1292
integer width = 485
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Costo Standard"
boolean lefttext = true
end type

type st_1 from statictext within w_costo_preventivo_storico
integer x = 1989
integer y = 1860
integer width = 549
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Costo Totale"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_costo_totale_complessivo from statictext within w_costo_preventivo_storico
integer x = 2560
integer y = 1860
integer width = 983
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_ricerca_origine from u_dw_search within w_costo_preventivo_storico
event ue_key pbm_dwnkey
integer x = 23
integer y = 20
integer width = 2437
integer height = 280
integer taborder = 20
string dataobject = "d_costo_preventivo_sel"
end type

event itemchanged;call super::itemchanged;string			ls_null


setnull(ls_null)

choose case dwo.name
		
	case "cod_prodotto"
		
		setitem(row,"cod_versione",ls_null)
		
end choose
end event

event itemfocuschanged;call super::itemfocuschanged;choose case dwo.name
		
	case "cod_versione"
		
		f_po_loaddddw_dw(dw_ricerca_origine,"cod_versione",sqlca,"distinta_padri_storico","DISTINCT cod_versione","cod_versione", &
				  "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + s_cs_xx.parametri.parametro_s_1 + "'")
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca_origine,"cod_prodotto")
end choose
end event

