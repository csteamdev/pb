﻿$PBExportHeader$w_origine_det_orari.srw
$PBExportComments$Window origine det orari
forward
global type w_origine_det_orari from w_cs_xx_risposta
end type
type st_operaio from statictext within w_origine_det_orari
end type
type st_2 from statictext within w_origine_det_orari
end type
type st_data from statictext within w_origine_det_orari
end type
type st_1 from statictext within w_origine_det_orari
end type
type dw_origine_det_orari from uo_cs_xx_dw within w_origine_det_orari
end type
end forward

global type w_origine_det_orari from w_cs_xx_risposta
integer width = 3369
integer height = 1780
string title = "Dettaglio Orari"
st_operaio st_operaio
st_2 st_2
st_data st_data
st_1 st_1
dw_origine_det_orari dw_origine_det_orari
end type
global w_origine_det_orari w_origine_det_orari

on w_origine_det_orari.create
int iCurrent
call super::create
this.st_operaio=create st_operaio
this.st_2=create st_2
this.st_data=create st_data
this.st_1=create st_1
this.dw_origine_det_orari=create dw_origine_det_orari
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_operaio
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.st_data
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.dw_origine_det_orari
end on

on w_origine_det_orari.destroy
call super::destroy
destroy(this.st_operaio)
destroy(this.st_2)
destroy(this.st_data)
destroy(this.st_1)
destroy(this.dw_origine_det_orari)
end on

event pc_setwindow;call super::pc_setwindow;string ls_operaio

dw_origine_det_orari.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_nonew + c_nodelete + c_nomodify, &
                              c_default)

select cognome
into   :ls_operaio
from   anag_operai
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_operaio =: s_cs_xx.parametri.parametro_s_1;

st_operaio.text = ls_operaio
st_data.text = string(date(s_cs_xx.parametri.parametro_data_1))
end event

type st_operaio from statictext within w_origine_det_orari
integer x = 2034
integer y = 20
integer width = 1280
integer height = 100
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "."
boolean focusrectangle = false
end type

type st_2 from statictext within w_origine_det_orari
integer x = 1714
integer y = 20
integer width = 320
integer height = 100
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Operaio"
boolean focusrectangle = false
end type

type st_data from statictext within w_origine_det_orari
integer x = 640
integer y = 20
integer width = 571
integer height = 100
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "."
boolean focusrectangle = false
end type

type st_1 from statictext within w_origine_det_orari
integer x = 23
integer y = 20
integer width = 617
integer height = 100
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Dettagli orari"
boolean focusrectangle = false
end type

type dw_origine_det_orari from uo_cs_xx_dw within w_origine_det_orari
integer x = 23
integer y = 140
integer width = 3291
integer height = 1520
integer taborder = 10
string dataobject = "d_origine_det_orari"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_operaio
datetime ldt_data_giorno

ls_cod_operaio = s_cs_xx.parametri.parametro_s_1
ldt_data_giorno = s_cs_xx.parametri.parametro_data_1

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_operaio,ldt_data_giorno)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

