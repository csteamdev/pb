﻿$PBExportHeader$w_report_costo_consuntivo.srw
$PBExportComments$Window report costo consuntivo produzione
forward
global type w_report_costo_consuntivo from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_report_costo_consuntivo
end type
type dw_report from uo_std_dw within w_report_costo_consuntivo
end type
type ws_risorse_umane from structure within w_report_costo_consuntivo
end type
type ws_fasi_lavorazione from structure within w_report_costo_consuntivo
end type
end forward

type ws_risorse_umane from structure
	string		cod_cat_attrezzature_ru
	string		des_cat_attrezzature_ru
	decimal { 4 }		tempo_totale_ru
	decimal { 4 }		costo_medio_orario_ru
	decimal { 4 }		costo_totale_ru
	decimal { 4 }		costo_totale_prev_ru
	decimal { 4 }		tmp_totale_prev_ru
	decimal { 4 }		delta_ru
end type

type ws_fasi_lavorazione from structure
	string		cod_prodotto_l
	string		des_prodotto_l
	string		cod_lavorazione_l
	string		cod_cat_attrezzature_l
	string		des_cat_attrezzature_l
	decimal { 4 }		tempo_attrezzaggio_l
	decimal { 4 }		tempo_attrezzaggio_commessa_l
	decimal { 4 }		tempo_lavorazione_l
	decimal { 4 }		tempo_totale_l
	decimal { 4 }		costo_medio_orario
	decimal { 4 }		costo_totale
	decimal { 4 }		num_pezzi
	decimal { 4 }		tmp_lavorazione_prev
	decimal { 4 }		tmp_attrezzaggio_prev
	decimal { 4 }		tmp_attrezzaggio_com_prev
	decimal { 4 }		tmp_totale_prev
	decimal { 4 }		costo_totale_prev
	decimal { 4 }		delta_l
end type

global type w_report_costo_consuntivo from w_cs_xx_principale
integer width = 4146
integer height = 2096
string title = "Report Costo Consuntivo Produzione"
cb_stampa cb_stampa
dw_report dw_report
end type
global w_report_costo_consuntivo w_report_costo_consuntivo

type variables
string  is_cod_prodotto, is_cod_versione

double  id_quantita

boolean ib_tempo_attrezzaggio, ib_tempo_attrezzaggio_commessa, ib_tempo_lavorazione, ib_costo_st, ib_costo_ul, &
        ib_prezzo_ac, ib_tempo_risorsa_umana
		  
dec{4}  idd_costo_totale_mp, idd_costo_totale_lav, idd_costo_totale_ru		


end variables

forward prototypes
public function integer wf_fasi_lavorazione (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore)
public subroutine wf_report ()
public function integer wf_fasi_risorse_umane (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore)
public function integer wf_fasi (long fi_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, decimal fdd_quantita_richiesta, ref string fs_errore, ref ws_risorse_umane fws_risorse_umane[], ref ws_fasi_lavorazione fws_fasi_lavorazione[])
end prototypes

public function integer wf_fasi_lavorazione (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore);string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, & 
			 ls_cod_lavorazione,ls_test,ls_cod_cat_attrezzature,ls_des_cat_attrezzature, &
			 ls_cod_cat_attrezzature_2,ls_des_cat_attrezzature_2
			 
long      ll_num_righe,ll_num_figli, ll_riga, ll_i

integer   li_risposta

dec{4}    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione, ldd_tempo_totale, ldd_costo_medio_orario, ldd_costo_totale, &
			 ldd_tempo_risorsa_umana,ldd_costo_medio_orario_2,ldd_costo_totale_2
			 
datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli = 1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fdd_quantita_richiesta
		
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda	and 	 
	       cod_prodotto_padre = :ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode = 100 then continue

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 tempo_attrezzaggio,
		    tempo_attrezzaggio_commessa,
			 tempo_lavorazione,
			 cod_cat_attrezzature_2,
			 tempo_risorsa_umana
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
 			 :ldd_tempo_attrezzaggio,
		    :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_risorsa_umana
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda AND   
	      cod_prodotto = :ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		return -1
	end if
  
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_prodotto = :ls_cod_prodotto_figlio;

	select des_cat_attrezzature,
		    costo_medio_orario
	into   :ls_des_cat_attrezzature,
			 :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature;

	select des_cat_attrezzature,
			 costo_medio_orario
	into   :ls_des_cat_attrezzature_2,
			 :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_cat_attrezzature=:ls_cod_cat_attrezzature_2;
	
	ldd_tempo_attrezzaggio = ldd_tempo_attrezzaggio 
	ldd_tempo_attrezzaggio_commessa = ldd_tempo_attrezzaggio_commessa 
	ldd_tempo_lavorazione = ldd_tempo_lavorazione * ldd_quan_utilizzo
	ldd_tempo_risorsa_umana = ldd_tempo_risorsa_umana * ldd_quan_utilizzo

	ldd_tempo_totale = 0

	if ib_tempo_attrezzaggio = true then
		ldd_tempo_totale = ldd_tempo_totale + ldd_tempo_attrezzaggio
	end if

	if ib_tempo_attrezzaggio_commessa = true then
		ldd_tempo_totale = ldd_tempo_totale + ldd_tempo_attrezzaggio_commessa
	end if

	if ib_tempo_lavorazione = true then
		ldd_tempo_totale = ldd_tempo_totale + ldd_tempo_lavorazione
	end if

	ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
	ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2

   ll_riga = dw_report.insertrow(0)
   dw_report.setitem( ll_riga, "cod_prodotto_l", ls_cod_prodotto_figlio)
 	dw_report.setitem( ll_riga, "des_prodotto_l", ls_des_prodotto)
	dw_report.setitem( ll_riga, "cod_lavorazione_l", ls_cod_lavorazione)
	dw_report.setitem( ll_riga, "cod_cat_attrezzature_l", ls_cod_cat_attrezzature)
	dw_report.setitem( ll_riga, "des_cat_attrezzature_l", ls_des_cat_attrezzature)
	dw_report.setitem( ll_riga, "tempo_attrezzaggio_l", ldd_tempo_attrezzaggio)
	dw_report.setitem( ll_riga, "tempo_attrezzaggio_commessa_l", ldd_tempo_attrezzaggio_commessa)
	dw_report.setitem( ll_riga, "tempo_lavorazione_l", ldd_tempo_lavorazione)
	dw_report.setitem( ll_riga, "tempo_totale_l", ldd_tempo_totale)
	dw_report.setitem( ll_riga, "costo_medio_orario", ldd_costo_medio_orario)  
	dw_report.setitem( ll_riga, "costo_totale", ldd_costo_totale)  
	dw_report.setitem( ll_riga, "num_pezzi", fdd_quantita_richiesta) 
	dw_report.setitem( ll_riga, "tipo_riga", 6)
	
	idd_costo_totale_lav = idd_costo_totale_lav + ldd_costo_totale
	
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta=wf_fasi_lavorazione(ls_cod_prodotto_figlio,fs_cod_versione,ldd_quan_utilizzo,ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

return 0
end function

public subroutine wf_report ();integer li_risposta,li_anno_commessa
string  ls_mat_prima[],ls_cod_prodotto,ls_des_prodotto,ls_errore,ls_cod_versione, ls_tipo_calcolo
double  ldd_quan_utilizzo[],ldd_quan_prodotta,ldd_quantita_totale,ldd_costo_materia_prima, &
		  ldd_costo_totale,ldd_costo_totale_mp, ldd_costo_totale_lav,ldd_costo_totale_ru, &
		  ldd_quan_utilizzata,ldd_quan_scarto,ldd_costo_utilizzo,ldd_costo_scarto,ldd_quan_spedita,ldd_costo_preventivo, & 
		  ldd_costo_totale_preventivo,ldd_costo_totale_lav_prev,ldd_costo_totale_ru_prev, ldd_quantita, idd_costo_totale_delta_mp, idd_costo_totale_mp_prev, idd_costo_totale_lav_prev
long    ll_t,ll_num_commessa, ll_righe, ll_riga, ll_ret

ws_fasi_lavorazione lws_fasi_lavorazione[]
ws_risorse_umane    lws_risorse_umane[]


li_anno_commessa = s_cs_xx.parametri.parametro_ul_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_2
ldd_quan_prodotta = s_cs_xx.parametri.parametro_d_1
ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1	
ls_cod_versione = s_cs_xx.parametri.parametro_s_2
ls_tipo_calcolo = s_cs_xx.parametri.parametro_s_3

if li_anno_commessa = 0 or ll_num_commessa = 0 then
	g_mb.messagebox("Sep","Attenzione! Selezionare una commessa prima di calcolare il costo consuntivo.",stopsign!)
	return
end if	

if ldd_quan_prodotta <=0 then
	g_mb.messagebox("Sep","Attenzione! Non è stata prodotta alcuna quantità della commessa selezionata",stopsign!)
	return
end if

dw_report.reset()

dw_report.setredraw( false)

li_risposta = f_trova_mat_prima(ls_cod_prodotto,ls_cod_versione,ls_mat_prima[],ldd_quan_utilizzo[],ldd_quan_prodotta,li_anno_commessa,ll_num_commessa)

ll_righe = 0

ll_riga = dw_report.insertrow(0)
	
dw_report.setitem( ll_riga, "tipo_riga", 1)

ll_riga = dw_report.insertrow(0)
	
dw_report.setitem( ll_riga, "tipo_riga", 2)

idd_costo_totale_mp = 0
idd_costo_totale_lav = 0
idd_costo_totale_ru = 0
idd_costo_totale_mp_prev = 0
idd_costo_totale_delta_mp = 0
idd_costo_totale_lav_prev = 0

for ll_t = 1 to upperbound(ls_mat_prima)
	
	select sum(quan_utilizzata),
			 sum(quan_scarto)
	into   :ldd_quan_utilizzata,
			 :ldd_quan_scarto
	from   mat_prime_commessa
	where  cod_azienda = :s_cs_xx.cod_azienda and		
	       anno_commessa = :li_anno_commessa 	and    
			 num_commessa = :ll_num_commessa and    
			 cod_prodotto = :ls_mat_prima[ll_t];
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore nel DB"+ sqlca.sqlerrtext,stopsign!)
		return 
	end if
	
	if isnull(ldd_quan_utilizzata) then ldd_quan_utilizzata = 0
	if isnull(ldd_quan_scarto) then ldd_quan_scarto = 0
	
	select sum(quan_spedita)
	into   :ldd_quan_spedita
	from   prod_bolle_out_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :li_anno_commessa and    
			 num_commessa = :ll_num_commessa and    
			 cod_prodotto_spedito = :ls_mat_prima[ll_t];
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore nel DB"+ sqlca.sqlerrtext,stopsign!)
		return 
	end if
	
	if isnull(ldd_quan_spedita) then 
		ldd_quan_spedita = 0
	else
		ldd_quan_utilizzata = ldd_quan_spedita
	end if
	
	ldd_quantita_totale = ldd_quan_utilizzata + ldd_quan_scarto
	
	if ls_tipo_calcolo = "A" then
		select des_prodotto,prezzo_acquisto
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_prodotto = :ls_mat_prima[ll_t];
	end if
	
	if ls_tipo_calcolo = "S" then
		select des_prodotto,costo_standard
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_prodotto = :ls_mat_prima[ll_t];
	end if
	
	if ls_tipo_calcolo = "U" then
		select des_prodotto,costo_ultimo
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_prodotto = :ls_mat_prima[ll_t];
	end if
	

 	ldd_costo_utilizzo = ldd_quan_utilizzata * ldd_costo_materia_prima
	ldd_costo_scarto = ldd_quan_scarto * ldd_costo_materia_prima 
	ldd_costo_totale = ldd_costo_materia_prima * ldd_quantita_totale
	ldd_costo_preventivo = ldd_costo_materia_prima * ldd_quan_utilizzo[ll_t]

	ll_riga = dw_report.insertrow(0)
	
	dw_report.setitem( ll_riga, "cod_prodotto", ls_mat_prima[ll_t])	
	dw_report.setitem( ll_riga, "des_prodotto", ls_des_prodotto)	
	dw_report.setitem( ll_riga, "cambiato_mp", "N")  	
	dw_report.setitem( ll_riga, "quan_utilizzo_mp", ldd_quan_utilizzata)  		
	dw_report.setitem( ll_riga, "quan_scarto_mp", ldd_quan_scarto) 	
	dw_report.setitem( ll_riga, "quan_totale_mp", ldd_quantita_totale)
	dw_report.setitem( ll_riga, "costo_unitario_mp", ldd_costo_materia_prima)
	dw_report.setitem( ll_riga, "costo_utilizzo_mp", ldd_costo_utilizzo)
	dw_report.setitem( ll_riga, "costo_scarto_mp", ldd_costo_scarto)
	dw_report.setitem( ll_riga, "costo_totale_mp", ldd_costo_totale)
	dw_report.setitem( ll_riga, "quan_prev_mp", ldd_quan_utilizzo[ll_t])
	dw_report.setitem( ll_riga, "costo_prev_mp", ldd_costo_preventivo)
	
	if not isnull(ldd_costo_preventivo) and ldd_costo_preventivo > 0 then
		dw_report.setitem( ll_riga, "delta_mp", truncate(( (ldd_costo_totale - ldd_costo_preventivo) / ldd_costo_preventivo ) *100,1))
	else
		dw_report.setitem( ll_riga, "delta_mp", 0)
	end if
	
	dw_report.setitem( ll_riga, "tipo_riga", 3)
	
	idd_costo_totale_mp = idd_costo_totale_mp + ldd_costo_totale
	idd_costo_totale_mp_prev = idd_costo_totale_mp_prev + ldd_costo_preventivo
next

ll_riga = dw_report.insertrow(0)
	
dw_report.setitem( ll_riga, "tipo_riga", 10)

dw_report.setitem( ll_riga, "tot_mp", idd_costo_totale_mp)
dw_report.setitem( ll_riga, "tot_costo_prev_mp", idd_costo_totale_mp_prev)
dw_report.setitem( ll_riga, "tot_delta_mp", idd_costo_totale_delta_mp)

ll_ret = wf_fasi( li_anno_commessa, ll_num_commessa, ls_cod_prodotto, ls_cod_versione, ldd_quan_prodotta, ref ls_errore, ref lws_risorse_umane, ref lws_fasi_lavorazione)

// *** lavorazioni

ll_riga = dw_report.insertrow(0)		
dw_report.setitem( ll_riga, "tipo_riga", 4) 
ll_riga = dw_report.insertrow(0)		
dw_report.setitem( ll_riga, "tipo_riga", 5) 
idd_costo_totale_lav = 0

for ll_ret = 1 to Upperbound( lws_fasi_lavorazione)
	ll_riga = dw_report.insertrow( 0)
	dw_report.setitem( ll_riga, "cod_prodotto_l", lws_fasi_lavorazione[ll_ret].cod_prodotto_l)
	dw_report.setitem( ll_riga, "des_prodotto_l", lws_fasi_lavorazione[ll_ret].des_prodotto_l)
	dw_report.setitem( ll_riga, "cod_lavorazione_l", lws_fasi_lavorazione[ll_ret].cod_lavorazione_l)
	dw_report.setitem( ll_riga, "cod_cat_attrezzature_l", lws_fasi_lavorazione[ll_ret].cod_cat_attrezzature_l)
	dw_report.setitem( ll_riga, "des_cat_attrezzature_l", lws_fasi_lavorazione[ll_ret].des_cat_attrezzature_l)
	dw_report.setitem( ll_riga, "tempo_attrezzaggio_l", lws_fasi_lavorazione[ll_ret].tempo_attrezzaggio_l)
	dw_report.setitem( ll_riga, "tempo_attrezzaggio_commessa_l", lws_fasi_lavorazione[ll_ret].tempo_attrezzaggio_commessa_l)
	dw_report.setitem( ll_riga, "tempo_lavorazione_l", lws_fasi_lavorazione[ll_ret].tempo_lavorazione_l)
	dw_report.setitem( ll_riga, "tempo_totale_l", lws_fasi_lavorazione[ll_ret].tempo_totale_l)
	dw_report.setitem( ll_riga, "costo_medio_orario", lws_fasi_lavorazione[ll_ret].costo_medio_orario)
	dw_report.setitem( ll_riga, "costo_totale", lws_fasi_lavorazione[ll_ret].costo_totale)
	dw_report.setitem( ll_riga, "num_pezzi", lws_fasi_lavorazione[ll_ret].num_pezzi)
	dw_report.setitem( ll_riga, "tmp_attrezzaggio_prev", lws_fasi_lavorazione[ll_ret].tmp_attrezzaggio_prev)
	dw_report.setitem( ll_riga, "tmp_attrezzaggio_com_prev", lws_fasi_lavorazione[ll_ret].tmp_attrezzaggio_com_prev)
	dw_report.setitem( ll_riga, "tmp_lavorazione_prev", lws_fasi_lavorazione[ll_ret].tmp_lavorazione_prev)
	dw_report.setitem( ll_riga, "tmp_totale_prev", lws_fasi_lavorazione[ll_ret].tmp_totale_prev)
	dw_report.setitem( ll_riga, "costo_totale_prev", lws_fasi_lavorazione[ll_ret].costo_totale_prev)
	dw_report.setitem( ll_riga, "delta_l", lws_fasi_lavorazione[ll_ret].delta_l)
	idd_costo_totale_lav += lws_fasi_lavorazione[ll_ret].costo_totale
	idd_costo_totale_lav_prev += lws_fasi_lavorazione[ll_ret].costo_totale_prev
	dw_report.setitem( ll_riga, "tipo_riga", 6)
next

ll_riga = dw_report.insertrow(0)	
dw_report.setitem( ll_riga, "tipo_riga", 11)
dw_report.setitem( ll_riga, "tot_lav", idd_costo_totale_lav)
dw_report.setitem( ll_riga, "tot_lav_prev", idd_costo_totale_lav_prev)

// *** gestione risorse umane

ll_riga = dw_report.insertrow(0)
dw_report.setitem( ll_riga, "tipo_riga", 7) 
ll_riga = dw_report.insertrow(0)
dw_report.setitem( ll_riga, "tipo_riga", 8) 
ldd_costo_totale_ru = 0

for ll_ret = 1 to Upperbound( lws_risorse_umane)
	ll_riga = dw_report.insertrow( 0)
	dw_report.setitem( ll_riga, "cod_cat_attrezzature_ru", lws_risorse_umane[ll_ret].cod_cat_attrezzature_ru)
	dw_report.setitem( ll_riga, "des_cat_attrezzature_ru", lws_risorse_umane[ll_ret].des_cat_attrezzature_ru)
	dw_report.setitem( ll_riga, "tempo_totale_ru", lws_risorse_umane[ll_ret].tempo_totale_ru)
	dw_report.setitem( ll_riga, "tmp_totale_prev_ru", lws_risorse_umane[ll_ret].tmp_totale_prev_ru)
	dw_report.setitem( ll_riga, "costo_medio_orario_ru", lws_risorse_umane[ll_ret].costo_medio_orario_ru)
	dw_report.setitem( ll_riga, "costo_totale_ru", lws_risorse_umane[ll_ret].costo_totale_ru)
	dw_report.setitem( ll_riga, "costo_totale_prev_ru", lws_risorse_umane[ll_ret].costo_totale_prev_ru)
	dw_report.setitem( ll_riga, "delta_ru", lws_risorse_umane[ll_ret].delta_ru )
	idd_costo_totale_ru += lws_risorse_umane[ll_ret].costo_totale_ru
	dw_report.setitem( ll_riga, "tipo_riga", 9)
next

ll_riga = dw_report.insertrow(0)
dw_report.setitem( ll_riga, "tipo_riga", 12)
dw_report.setitem( ll_riga, "tot_ru", idd_costo_totale_ru)

// *** totali riga

idd_costo_totale_mp = idd_costo_totale_mp + idd_costo_totale_lav + idd_costo_totale_ru
ll_riga = dw_report.insertrow(0)
dw_report.setitem( ll_riga, "tipo_riga", 15)
dw_report.Modify("e_totale.text='" + string(idd_costo_totale_mp) + "'")

dw_report.setredraw( true)
end subroutine

public function integer wf_fasi_risorse_umane (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore);string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, & 
			 ls_cod_lavorazione,ls_test,ls_cod_cat_attrezzature,ls_des_cat_attrezzature, &
			 ls_cod_cat_attrezzature_2,ls_des_cat_attrezzature_2
			 
long      ll_num_righe,ll_num_figli, ll_riga, ll_i

integer   li_risposta

dec{4}    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione, ldd_tempo_totale, ldd_costo_medio_orario, ldd_costo_totale, &
			 ldd_tempo_risorsa_umana,ldd_costo_medio_orario_2,ldd_costo_totale_2, ldd_tempo_risorsa_umana_prev
			 
datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli = 1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fdd_quantita_richiesta
		
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda	and 	 
	       cod_prodotto_padre = :ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode = 100 then continue

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 tempo_attrezzaggio,
		    tempo_attrezzaggio_commessa,
			 tempo_lavorazione,
			 cod_cat_attrezzature_2,
			 tempo_risorsa_umana,
			 tempo_risorsa_umana
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
 			 :ldd_tempo_attrezzaggio,
		    :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_risorsa_umana,
			 :ldd_tempo_risorsa_umana_prev
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda AND   
	      cod_prodotto = :ls_cod_prodotto_figlio;
			
	if sqlca.sqlcode=100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		return -1
	end if			

	if sqlca.sqlcode = 100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		return -1
	end if
	
	if isnull(ldd_tempo_risorsa_umana_prev) then ldd_tempo_risorsa_umana_prev = 0
	
//	ldd_tempo_risorsa_umana_prev = ldd_tempo_risorsa_umana_prev * ldd_quan_prodotta_fase
  
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_prodotto = :ls_cod_prodotto_figlio;

	select des_cat_attrezzature,
		    costo_medio_orario
	into   :ls_des_cat_attrezzature,
			 :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature;

	select des_cat_attrezzature,
			 costo_medio_orario
	into   :ls_des_cat_attrezzature_2,
			 :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature_2;
	
	ldd_tempo_risorsa_umana = ldd_tempo_risorsa_umana * ldd_quan_utilizzo

	ldd_tempo_totale = 0

	ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2
	
//	ldd_costo_totale_2_prev = (ldd_tempo_risorsa_umana_prev/60) * ldd_costo_medio_orario_2

   ll_riga = dw_report.insertrow(0)
   dw_report.setitem( ll_riga, "cod_cat_attrezzature_ru", ls_cod_cat_attrezzature_2)
   dw_report.setitem( ll_riga, "des_cat_attrezzature_ru", ls_des_cat_attrezzature_2)
   dw_report.setitem( ll_riga, "tempo_totale_ru", ldd_tempo_risorsa_umana)
   dw_report.setitem( ll_riga, "costo_medio_orario_ru", ldd_costo_medio_orario_2)  
   dw_report.setitem( ll_riga, "costo_totale_ru", ldd_costo_totale_2)  
	dw_report.setitem( ll_riga, "costo_totale_prev_ru", ldd_tempo_risorsa_umana)
	dw_report.setitem( ll_riga, "tmp_totale_prev_ru", ldd_tempo_risorsa_umana)
	//dw_report.setitem( ll_riga, "delta_ru", truncate(((costo_totale-costo_totale_prev)/costo_totale_prev*100),1))
	dw_report.setitem( ll_riga, "tipo_riga", 9)
	
	idd_costo_totale_ru = idd_costo_totale_ru + ldd_costo_totale_2	
	
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre = :ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta = wf_fasi_risorse_umane( ls_cod_prodotto_figlio, fs_cod_versione, ldd_quan_utilizzo, ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

return 0
end function

public function integer wf_fasi (long fi_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, decimal fdd_quantita_richiesta, ref string fs_errore, ref ws_risorse_umane fws_risorse_umane[], ref ws_fasi_lavorazione fws_fasi_lavorazione[]);// Funzione che calcola i costi consuntivi di lavorazione
// nome: wf_fasi_lavorazione
// tipo: integer
//       -1 failed
//  		 0 passed
//
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							 fi_anno_commessa			 integer			valore
//                    fl_num_commessa			 long				valore
//							 fs_cod_prodotto	 		 string  		valore
//							 fdd_quantita_richiesta	 double			valore
//
//		Creata il 24-07-97 
//		Autore Diego Ferrari

string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, & 
			 ls_cod_lavorazione,ls_test,ls_cod_cat_attrezzature,ls_des_cat_attrezzature, &
			 ls_cod_cat_attrezzature_2,ls_des_cat_attrezzature_2,ls_flag_materia_prima,ls_cod_prodotto_inserito,&
			 ls_cod_prodotto_variante,ls_flag_materia_prima_variante
long      ll_num_righe,ll_num_figli, ll_risorse_umane, ll_fasi_lavorazione
integer   li_risposta
dec{4}    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione, ldd_tempo_totale, ldd_costo_medio_orario, ldd_costo_totale, &
			 ldd_tempo_risorsa_umana,ldd_costo_medio_orario_2,ldd_costo_totale_2,ldd_quan_prodotta, & 
			 ldd_quan_utilizzo_variante,ldd_tempo_attrezzaggio_prev,ldd_tempo_attrezzaggio_commessa_prev, & 
			 ldd_tempo_lavorazione_prev, ldd_tempo_risorsa_umana_prev,ldd_tempo_totale_prev,ldd_costo_prev, & 
			 ldd_quan_prodotta_fase, ldd_costo_totale_2_prev
			 
ws_risorse_umane prova			 

datastore lds_righe_distinta

select quan_prodotta 
into   :ldd_quan_prodotta
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :fi_anno_commessa and
		 num_commessa = :fl_num_commessa;


ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fdd_quantita_richiesta
	ls_flag_materia_prima=lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	
//****************************
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio

	select cod_prodotto,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :fi_anno_commessa and    
			 num_commessa = :fl_num_commessa and    
			 cod_prodotto_padre = :fs_cod_prodotto and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and    
			 cod_versione = :fs_cod_versione;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante * fdd_quantita_richiesta
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	if ls_flag_materia_prima = 'N' then
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and 	 
		       cod_prodotto_padre = :ls_cod_prodotto_inserito;
		
		if sqlca.sqlcode = 100 then continue
	else
		continue
	end if

//*************************
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and 	 
	       cod_prodotto_padre = :ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode = 100 then continue

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 cod_cat_attrezzature_2,
			 tempo_lavorazione,
			 tempo_attrezzaggio,
			 tempo_attrezzaggio_commessa,
			 tempo_risorsa_umana
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_lavorazione_prev,
			 :ldd_tempo_attrezzaggio_prev,
			 :ldd_tempo_attrezzaggio_commessa_prev,
			 :ldd_tempo_risorsa_umana_prev
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda AND   
	      cod_prodotto = :ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		return -1
	end if
  
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto = :ls_cod_prodotto_figlio;

	select des_cat_attrezzature,
		    costo_medio_orario
	into   :ls_des_cat_attrezzature,
			 :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature;

	select des_cat_attrezzature,
			 costo_medio_orario
	into   :ls_des_cat_attrezzature_2,
			 :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature_2;
	
	select sum(tempo_attrezzaggio),
			 sum(tempo_attrezzaggio_commessa),
		    sum(tempo_lavorazione),
			 sum(tempo_risorsa_umana),
			 sum(quan_prodotta)
	into   :ldd_tempo_attrezzaggio,
			 :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ldd_tempo_risorsa_umana,
			 :ldd_quan_prodotta_fase
	from   avan_produzione_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :fi_anno_commessa and    
			 num_commessa = :fl_num_commessa	and    
			 cod_prodotto = :ls_cod_prodotto_figlio and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 cod_reparto = :ls_cod_reparto;


	if isnull(ldd_tempo_attrezzaggio) then ldd_tempo_attrezzaggio = 0
	
	if isnull(ldd_tempo_attrezzaggio_commessa) then ldd_tempo_attrezzaggio_commessa = 0
	
	if isnull(ldd_tempo_lavorazione) then ldd_tempo_lavorazione = 0
	
	if isnull(ldd_tempo_risorsa_umana) then ldd_tempo_risorsa_umana = 0
	
	if isnull(ldd_tempo_attrezzaggio_prev) then ldd_tempo_attrezzaggio_prev = 0
	
	if isnull(ldd_tempo_attrezzaggio_commessa_prev) then ldd_tempo_attrezzaggio_commessa_prev = 0
	
	if isnull(ldd_tempo_lavorazione_prev) then ldd_tempo_lavorazione_prev = 0
	
	if isnull(ldd_tempo_risorsa_umana_prev) then ldd_tempo_risorsa_umana_prev = 0
	

	ldd_tempo_attrezzaggio_prev = ldd_tempo_attrezzaggio_prev
	
	ldd_tempo_attrezzaggio_commessa_prev = ldd_tempo_attrezzaggio_commessa_prev

	ldd_tempo_lavorazione_prev = ldd_tempo_lavorazione_prev * ldd_quan_prodotta_fase
	
	ldd_tempo_risorsa_umana_prev = ldd_tempo_risorsa_umana_prev * ldd_quan_prodotta_fase

	ldd_tempo_totale = 0
	ldd_tempo_totale_prev = 0
	
	ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio
	ldd_tempo_totale_prev = ldd_tempo_totale_prev + ldd_tempo_attrezzaggio_prev

	ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio_commessa
	ldd_tempo_totale_prev = ldd_tempo_totale_prev + ldd_tempo_attrezzaggio_commessa_prev

	ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_lavorazione
	ldd_tempo_totale_prev = ldd_tempo_totale_prev + ldd_tempo_lavorazione_prev


	ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
	ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2

	ldd_costo_totale_2_prev = (ldd_tempo_risorsa_umana_prev/60) * ldd_costo_medio_orario_2
	ldd_costo_prev = (ldd_tempo_totale_prev/60) * ldd_costo_medio_orario

	ll_fasi_lavorazione = upperbound( fws_fasi_lavorazione)
	if isnull(ll_fasi_lavorazione) then ll_fasi_lavorazione = 0
	ll_fasi_lavorazione ++
	
	fws_fasi_lavorazione[ll_fasi_lavorazione].cod_prodotto_l = ls_cod_prodotto_figlio
	fws_fasi_lavorazione[ll_fasi_lavorazione].des_prodotto_l = ls_des_prodotto
	fws_fasi_lavorazione[ll_fasi_lavorazione].cod_lavorazione_l = ls_cod_lavorazione
	fws_fasi_lavorazione[ll_fasi_lavorazione].cod_cat_attrezzature_l = ls_cod_cat_attrezzature
	fws_fasi_lavorazione[ll_fasi_lavorazione].des_cat_attrezzature_l = ls_des_cat_attrezzature
	fws_fasi_lavorazione[ll_fasi_lavorazione].tempo_attrezzaggio_l = ldd_tempo_attrezzaggio
	fws_fasi_lavorazione[ll_fasi_lavorazione].tempo_attrezzaggio_commessa_l = ldd_tempo_attrezzaggio_commessa
	fws_fasi_lavorazione[ll_fasi_lavorazione].tempo_lavorazione_l = ldd_tempo_lavorazione
	fws_fasi_lavorazione[ll_fasi_lavorazione].tempo_totale_l = ldd_tempo_totale
	fws_fasi_lavorazione[ll_fasi_lavorazione].costo_medio_orario = ldd_costo_medio_orario
	fws_fasi_lavorazione[ll_fasi_lavorazione].costo_totale = ldd_costo_totale
	fws_fasi_lavorazione[ll_fasi_lavorazione].num_pezzi = ldd_quan_prodotta
	fws_fasi_lavorazione[ll_fasi_lavorazione].tmp_attrezzaggio_prev = ldd_tempo_attrezzaggio_prev
	fws_fasi_lavorazione[ll_fasi_lavorazione].tmp_attrezzaggio_com_prev = ldd_tempo_attrezzaggio_commessa_prev
	fws_fasi_lavorazione[ll_fasi_lavorazione].tmp_lavorazione_prev = ldd_tempo_lavorazione_prev
	fws_fasi_lavorazione[ll_fasi_lavorazione].tmp_totale_prev = ldd_tempo_totale_prev
	fws_fasi_lavorazione[ll_fasi_lavorazione].costo_totale_prev = ldd_costo_prev
	fws_fasi_lavorazione[ll_fasi_lavorazione].delta_l = truncate(((ldd_costo_totale - ldd_costo_prev)/ldd_costo_prev*100),1)
	
	
	ll_risorse_umane = upperbound(fws_risorse_umane)
	if isnull(ll_risorse_umane) then ll_risorse_umane = 0
	ll_risorse_umane ++
	
	fws_risorse_umane[ll_risorse_umane].cod_cat_attrezzature_ru = ls_cod_cat_attrezzature_2
	fws_risorse_umane[ll_risorse_umane].des_cat_attrezzature_ru = ls_des_cat_attrezzature_2
	fws_risorse_umane[ll_risorse_umane].tempo_totale_ru = ldd_tempo_risorsa_umana
	fws_risorse_umane[ll_risorse_umane].tmp_totale_prev_ru = ldd_tempo_risorsa_umana_prev
	fws_risorse_umane[ll_risorse_umane].costo_medio_orario_ru = ldd_costo_medio_orario_2
	fws_risorse_umane[ll_risorse_umane].costo_totale_ru = ldd_costo_totale_2
	fws_risorse_umane[ll_risorse_umane].costo_totale_prev_ru = ldd_costo_totale_2_prev
	fws_risorse_umane[ll_risorse_umane].delta_ru = truncate(((ldd_costo_totale_2 - ldd_costo_totale_2_prev)/ldd_costo_totale_2_prev*100),1)

	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
	       cod_prodotto_padre = :ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta = wf_fasi( fi_anno_commessa, fl_num_commessa, ls_cod_prodotto_figlio, fs_cod_versione, ldd_quan_utilizzo, ref fs_errore, ref fws_risorse_umane, ref fws_fasi_lavorazione)
		if li_risposta = -1 then
			fs_errore = ls_errore
			return -1
		end if
	end if
	
next

return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

//set_w_options(c_closenosave + c_autoposition + c_noresizewin)

wf_report()
end event

on w_report_costo_consuntivo.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.dw_report
end on

on w_report_costo_consuntivo.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.dw_report)
end on

type cb_stampa from commandbutton within w_report_costo_consuntivo
integer x = 3726
integer y = 1900
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;dw_report.print()
end event

type dw_report from uo_std_dw within w_report_costo_consuntivo
integer x = 23
integer y = 20
integer width = 4069
integer height = 1860
string dataobject = "d_report_costo_consuntivo"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

