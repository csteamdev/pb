﻿$PBExportHeader$w_report_costo_preventivo.srw
$PBExportComments$Window report costo preventivo produzione
forward
global type w_report_costo_preventivo from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_report_costo_preventivo
end type
type dw_report from uo_std_dw within w_report_costo_preventivo
end type
end forward

global type w_report_costo_preventivo from w_cs_xx_principale
integer width = 4146
integer height = 2096
string title = "Report Costo Preventivo Produzione"
cb_stampa cb_stampa
dw_report dw_report
end type
global w_report_costo_preventivo w_report_costo_preventivo

type variables
string  is_cod_prodotto, is_cod_versione

double  id_quantita

boolean ib_tempo_attrezzaggio, ib_tempo_attrezzaggio_commessa, ib_tempo_lavorazione, ib_costo_st, ib_costo_ul, &
        ib_prezzo_ac, ib_tempo_risorsa_umana
		  
dec{4}  idd_costo_totale_mp, idd_costo_totale_lav, idd_costo_totale_ru		  


end variables

forward prototypes
public function integer wf_fasi_lavorazione (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore)
public subroutine wf_report ()
public function integer wf_fasi_risorse_umane (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore)
public function integer wf_costo_rami_distinta (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quantita, integer fi_num_livello_stop, ref integer fi_num_livello_corrente, ref string fs_errore)
end prototypes

public function integer wf_fasi_lavorazione (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore);// Funzione che calcola i costi di lavorazione
// nome: wf_fasi_lavorazione
// tipo: integer
//       -1 failed
//  		 0 passed
//
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//							 fs_cod_prodotto	 		 string  		valore
//							 fdd_quantita_richiesta	 double			valore
//
//		Creata il 24-07-97 
//		Autore Diego Ferrari

string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, & 
			 ls_cod_lavorazione,ls_test,ls_cod_cat_attrezzature,ls_des_cat_attrezzature, &
			 ls_cod_cat_attrezzature_2,ls_des_cat_attrezzature_2, ls_cod_versione_figlio, ls_flag_fase_esterna, ls_flag_uso, &
			 ls_flag_mp, ls_flag_valorizza
long      ll_num_righe,ll_num_figli, ll_num_sequenza, ll_riga
integer   li_risposta
dec{4}    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione, ldd_tempo_totale, ldd_costo_medio_orario, ldd_costo_totale, &
			 ldd_tempo_risorsa_umana,ldd_costo_medio_orario_2,ldd_costo_totale_2, ld_costo_orario, ld_costo_pezzo
datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fdd_quantita_richiesta
	
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda 
	and 	 cod_prodotto_padre = :ls_cod_prodotto_figlio and
	       cod_versione = :ls_cod_versione_figlio;
	
	if sqlca.sqlcode=100 then continue
	
	select flag_materia_prima,
			 flag_valorizza_mp
	into	 :ls_flag_mp,
			 :ls_flag_valorizza
	from	 distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto_padre = :fs_cod_prodotto and
			 cod_versione = :fs_cod_versione and
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and
			 cod_versione_figlio = :ls_cod_versione_figlio and
			 num_sequenza = :ll_num_sequenza;
			 
	if sqlca.sqlcode = 0 and not isnull( ls_flag_mp) and ls_flag_mp = "S" and ls_flag_valorizza = "N" and not isnull(ls_flag_valorizza) then
	elseif sqlca.sqlcode = 0 and not isnull( ls_flag_mp) and ls_flag_mp = "S" and ls_flag_valorizza = "S" then		
		continue
	end if	

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 tempo_attrezzaggio,
		    tempo_attrezzaggio_commessa,
			 tempo_lavorazione,
			 cod_cat_attrezzature_2,
			 tempo_risorsa_umana,
			 flag_esterna,
			 costo_orario,
			 costo_pezzo,
			 flag_uso
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
 			 :ldd_tempo_attrezzaggio,
		    :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_risorsa_umana,
			 :ls_flag_fase_esterna,
			 :ld_costo_orario,
			 :ld_costo_pezzo,
			 :ls_flag_uso
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda  AND   
			cod_prodotto = :ls_cod_prodotto_figlio AND
			cod_versione = :ls_cod_versione_figlio;

	if sqlca.sqlcode=100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		continue
	end if
  
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto =:ls_cod_prodotto_figlio;

	select des_cat_attrezzature,
		    costo_medio_orario
	into   :ls_des_cat_attrezzature,
			 :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;

	select des_cat_attrezzature,
			 costo_medio_orario
	into   :ls_des_cat_attrezzature_2,
			 :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature_2;
	
	
	ldd_tempo_attrezzaggio = ldd_tempo_attrezzaggio 
	ldd_tempo_attrezzaggio_commessa = ldd_tempo_attrezzaggio_commessa 
	ldd_tempo_lavorazione = ldd_tempo_lavorazione * ldd_quan_utilizzo
	ldd_tempo_risorsa_umana = ldd_tempo_risorsa_umana * ldd_quan_utilizzo

	ldd_tempo_totale = 0

	if ib_tempo_attrezzaggio = true then
		ldd_tempo_totale = ldd_tempo_totale + ldd_tempo_attrezzaggio
	end if

	if ib_tempo_attrezzaggio_commessa = true then
		ldd_tempo_totale = ldd_tempo_totale + ldd_tempo_attrezzaggio_commessa
	end if

	if ib_tempo_lavorazione = true then
		ldd_tempo_totale = ldd_tempo_totale + ldd_tempo_lavorazione
	end if	
	
	
	if not isnull(ls_flag_fase_esterna) and ls_flag_fase_esterna = "S" then		
		
		if ls_flag_uso = "O" then
			if isnull(ld_costo_orario) then ld_costo_orario = 0
			ldd_costo_medio_orario = ld_costo_orario
			ldd_costo_medio_orario_2 = ld_costo_orario
			ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
		else
			if isnull(ld_costo_pezzo) then ld_costo_pezzo = 0
			ldd_costo_totale = ldd_quan_utilizzo * ld_costo_pezzo
//			ldd_costo_totale = fdd_quantita_richiesta * ld_costo_pezzo
		end if
		
		ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2	
		// 24/04/2008: correzione eseguita da enrico: considerava sempre il costo unitario.
		ldd_costo_totale_2 = ldd_costo_totale_2 * ldd_quan_utilizzo
		
	else			
		ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
		ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2		
	end if

   ll_riga = dw_report.insertrow(0)
   dw_report.setitem( ll_riga, "cod_prodotto_l", ls_cod_prodotto_figlio)
 	dw_report.setitem( ll_riga, "des_prodotto_l", ls_des_prodotto)
	dw_report.setitem( ll_riga, "cod_lavorazione_l", ls_cod_lavorazione)
	dw_report.setitem( ll_riga, "cod_cat_attrezzature_l", ls_cod_cat_attrezzature)
	dw_report.setitem( ll_riga, "des_cat_attrezzature_l", ls_des_cat_attrezzature)
	dw_report.setitem( ll_riga, "tempo_attrezzaggio_l", ldd_tempo_attrezzaggio)
	dw_report.setitem( ll_riga, "tempo_attrezzaggio_commessa_l", ldd_tempo_attrezzaggio_commessa)
	dw_report.setitem( ll_riga, "tempo_lavorazione_l", ldd_tempo_lavorazione)
	dw_report.setitem( ll_riga, "tempo_totale_l", ldd_tempo_totale)
	dw_report.setitem( ll_riga, "costo_medio_orario", ldd_costo_medio_orario)  
	dw_report.setitem( ll_riga, "costo_totale", ldd_costo_totale)  
//	dw_report.setitem( ll_riga, "num_pezzi", fdd_quantita_richiesta) 
	dw_report.setitem( ll_riga, "num_pezzi", ldd_quan_utilizzo)
	dw_report.setitem( ll_riga, "tipo_riga", 6)
	
	idd_costo_totale_lav = idd_costo_totale_lav + ldd_costo_totale

	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta = wf_fasi_lavorazione(ls_cod_prodotto_figlio,ls_cod_versione_figlio,ldd_quan_utilizzo,ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			continue
		end if
	end if
	
next

return 0
end function

public subroutine wf_report ();integer li_risposta,ll_livello=1

string  ls_mat_prima[], ls_cod_prodotto, ls_des_prodotto, ls_errore, ls_cod_versione, ls_test, ls_des_prodotto_etichetta, &
        ls_quantita

dec{4}  ldd_quantita, ldd_quantita_totale, ldd_costo_materia_prima, ldd_costo_totale
		  
long    ll_t, ll_null, ll_righe, ll_riga

double  ldd_quan_utilizzo[]

dec{4} ld_totale

uo_calcolo_costi_pf luo_calcolo_costi_pf

//Donato 24-10-2008
//questa variabile è stata definita perchè il report deve calcolare questa ultima parte 
//tenendo presente se l'operatore ha selezionato il calcolo per "prezzo_acquisto", "costo_standard", "costo_ultimo"
string ls_tipo_calcolo
//--------------------------

ls_cod_prodotto = is_cod_prodotto

ls_cod_versione = is_cod_versione

setnull(ls_test)

select cod_azienda
into   :ls_test
from   distinta_padri
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_prodotto = :ls_cod_prodotto
and    cod_versione = :ls_cod_versione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return 
end if

if isnull(ls_test) or ls_test = "" then
	g_mb.messagebox("Sep","Attenzione il prodotto selezionato non è un prodotto finito",stopsign!)
	return 
end if

ldd_quantita = id_quantita
setnull(ll_null)

if ldd_quantita = 0 or isnull(ls_cod_prodotto) then
	g_mb.messagebox("Sep","Selezionare un prodotto e inserire una quantità",stopsign!)
	return
end if

select des_prodotto
into   :ls_des_prodotto_etichetta
from   anag_prodotti 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto;

dw_report.reset()

dw_report.setredraw( false)

dw_report.Modify("e_prodotto.text='" + ls_cod_prodotto + " " + ls_des_prodotto_etichetta + "'")

dw_report.Modify("e_versione.text='" + ls_cod_versione + "'")

ls_quantita = string(ldd_quantita)

dw_report.Modify("e_quantita.text='" + ls_quantita + "'")

s_cs_xx.parametri.parametro_s_5 = "REPORT_COSTO_PREVENTIVO"

li_risposta = f_trova_mat_prima(ls_cod_prodotto,ls_cod_versione,ls_mat_prima[],ldd_quan_utilizzo[],1,ll_null,ll_null)

setnull(s_cs_xx.parametri.parametro_s_5)

ll_righe = 0

ll_riga = dw_report.insertrow(0)
	
dw_report.setitem( ll_riga, "tipo_riga", 1)

ll_riga = dw_report.insertrow(0)
	
dw_report.setitem( ll_riga, "tipo_riga", 2)

idd_costo_totale_mp = 0

idd_costo_totale_lav = 0

idd_costo_totale_ru = 0

for ll_t = 1 to upperbound(ls_mat_prima)
	
	if isnull(ldd_quan_utilizzo[ll_t]) then ldd_quan_utilizzo[ll_t] = 0 
	
	if isnull(ldd_quantita) then ldd_quantita = 0
	
   ldd_quantita_totale = ldd_quan_utilizzo[ll_t] * ldd_quantita

   if ib_prezzo_ac = true then
	
		select des_prodotto,
		       prezzo_acquisto
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda	and    
		       cod_prodotto = :ls_mat_prima[ll_t];
				 
		//Donato 24-10-2008 -----------
		ls_tipo_calcolo = "A"
		//--------------------------------
		
	end if

	if ib_costo_st = true then
	
		select des_prodotto,
		       costo_standard
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda	and    
		       cod_prodotto = :ls_mat_prima[ll_t];
				 
		//Donato 24-10-2008 -----------
		ls_tipo_calcolo = "S"
		//--------------------------------
		
	end if

	if ib_costo_ul = true then
	
		select des_prodotto,
		       costo_ultimo
		into	 :ls_des_prodotto,
				 :ldd_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda	and    
		       cod_prodotto = :ls_mat_prima[ll_t];
				 
		//Donato 24-10-2008 -----------
		ls_tipo_calcolo = "U"
		//--------------------------------
				 
	end if
	
	if isnull(ldd_costo_materia_prima) then ldd_costo_materia_prima = 0
	
	if isnull(ldd_quantita_totale) then  ldd_quantita_totale = 0
	
	ldd_costo_totale = ldd_costo_materia_prima * ldd_quantita_totale

	ll_riga = dw_report.insertrow(0)
	
	dw_report.setitem( ll_riga, "cod_prodotto", ls_mat_prima[ll_t])
	
	dw_report.setitem( ll_riga, "des_prodotto", ls_des_prodotto)
	
	dw_report.setitem( ll_riga, "quan_utilizzo_mp", ldd_quantita_totale)  
	
	dw_report.setitem( ll_riga, "costo_unitario_mp", ldd_costo_materia_prima)  	
	
	dw_report.setitem( ll_riga, "costo_totale_mp", ldd_costo_totale) 
	
	dw_report.setitem( ll_riga, "tipo_riga", 3)
	
	idd_costo_totale_mp = idd_costo_totale_mp + ldd_costo_totale

next

ll_riga = dw_report.insertrow(0)
	
dw_report.setitem( ll_riga, "tipo_riga", 10)

dw_report.setitem( ll_riga, "tot_mp", idd_costo_totale_mp)

// *** lavorazioni

ll_riga = dw_report.insertrow(0)
		
dw_report.setitem( ll_riga, "tipo_riga", 4) 

ll_riga = dw_report.insertrow(0)
		
dw_report.setitem( ll_riga, "tipo_riga", 5) 

li_risposta = wf_fasi_lavorazione( ls_cod_prodotto, ls_cod_versione, ldd_quantita, ls_errore)

ll_riga = dw_report.insertrow(0)
	
dw_report.setitem( ll_riga, "tipo_riga", 11)

dw_report.setitem( ll_riga, "tot_lav", idd_costo_totale_lav)

// *** gestione risorse umane

ll_riga = dw_report.insertrow(0)
		
dw_report.setitem( ll_riga, "tipo_riga", 7) 

ll_riga = dw_report.insertrow(0)
		
dw_report.setitem( ll_riga, "tipo_riga", 8) 

li_risposta = wf_fasi_risorse_umane( ls_cod_prodotto, ls_cod_versione, ldd_quantita, ls_errore)

ll_riga = dw_report.insertrow(0)
	
dw_report.setitem( ll_riga, "tipo_riga", 12)

dw_report.setitem( ll_riga, "tot_ru", idd_costo_totale_ru)

// *** totali riga

idd_costo_totale_mp = idd_costo_totale_mp + idd_costo_totale_lav + idd_costo_totale_ru

ll_riga = dw_report.insertrow(0)
	
dw_report.setitem( ll_riga, "tipo_riga", 15)

dw_report.Modify("e_totale.text='" + string(idd_costo_totale_mp) + "'")

dw_report.setredraw( true)

//  aggiunta della sezione con totali dei semilavorati fino al terzo livello
//  EnMe 15/04/2008  x Unifast "Integrazione_2008"

ll_riga = dw_report.insertrow(0)

dw_report.setitem( ll_riga, "tipo_riga", 20)
dw_report.setitem( ll_riga, "des_sez_valorizzazione", "RIEPILOGO COSTO PREVENTIVO DI PRODUZIONE")

luo_calcolo_costi_pf = create uo_calcolo_costi_pf

//Donato 24-10-2008 -------------------------

ld_totale = luo_calcolo_costi_pf.uof_costo_totale_prodotto(is_cod_prodotto, is_cod_versione, ls_tipo_calcolo, ref ls_errore)
//ld_totale = luo_calcolo_costi_pf.uof_costo_totale_prodotto(is_cod_prodotto, is_cod_versione, "A", ref ls_errore)
//-----------------------------------------------

if ld_totale < 0 then
	messagebox("SEP",ls_errore)
	ld_totale = 0
end if

destroy luo_calcolo_costi_pf

ld_totale = ld_totale * id_quantita

ls_des_prodotto = f_des_tabella("anag_prodotti", "cod_prodotto ='" + is_cod_prodotto + "'", "des_prodotto")

ll_riga = dw_report.insertrow(0)
dw_report.setitem( ll_riga, "tipo_riga", 21)
dw_report.setitem( ll_riga, "des_valorizzazione", is_cod_prodotto + " " +is_cod_versione + " - costo totale prodotto finito " + string(ld_totale,"###,##0.00"))
dw_report.setitem( ll_riga, "num_livello_sl", ll_livello)

wf_costo_rami_distinta(is_cod_prodotto, is_cod_versione, id_quantita, 3, ll_livello, ls_errore)

dw_report.setredraw(true)

end subroutine

public function integer wf_fasi_risorse_umane (string fs_cod_prodotto, string fs_cod_versione, double fdd_quantita_richiesta, ref string fs_errore);string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, & 
			 ls_cod_lavorazione,ls_test,ls_cod_cat_attrezzature,ls_des_cat_attrezzature, &
			 ls_cod_cat_attrezzature_2,ls_des_cat_attrezzature_2
			 
long      ll_num_righe,ll_num_figli, ll_riga, ll_i

integer   li_risposta

dec{4}    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione, ldd_tempo_totale, ldd_costo_medio_orario, ldd_costo_totale, &
			 ldd_tempo_risorsa_umana,ldd_costo_medio_orario_2,ldd_costo_totale_2
			 
datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli = 1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fdd_quantita_richiesta
		
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda	and 	 
	       cod_prodotto_padre = :ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode = 100 then continue

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 tempo_attrezzaggio,
		    tempo_attrezzaggio_commessa,
			 tempo_lavorazione,
			 cod_cat_attrezzature_2,
			 tempo_risorsa_umana
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
 			 :ldd_tempo_attrezzaggio,
		    :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_risorsa_umana
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda AND   
	      cod_prodotto = :ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		return -1
	end if
  
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_prodotto = :ls_cod_prodotto_figlio;

	select des_cat_attrezzature,
		    costo_medio_orario
	into   :ls_des_cat_attrezzature,
			 :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature;

	select des_cat_attrezzature,
			 costo_medio_orario
	into   :ls_des_cat_attrezzature_2,
			 :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature_2;
	
	ldd_tempo_risorsa_umana = ldd_tempo_risorsa_umana * ldd_quan_utilizzo

	ldd_tempo_totale = 0

	ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2

   ll_riga = dw_report.insertrow(0)
   dw_report.setitem( ll_riga, "cod_cat_attrezzature_ru", ls_cod_cat_attrezzature_2)
   dw_report.setitem( ll_riga, "des_cat_attrezzature_ru", ls_des_cat_attrezzature_2)
   dw_report.setitem( ll_riga, "tempo_totale_ru", ldd_tempo_risorsa_umana)
   dw_report.setitem( ll_riga, "costo_medio_orario_ru", ldd_costo_medio_orario_2)  
   dw_report.setitem( ll_riga, "costo_totale_ru", ldd_costo_totale_2)  
	dw_report.setitem( ll_riga, "tipo_riga", 9)
	
	idd_costo_totale_ru = idd_costo_totale_ru + ldd_costo_totale_2	
	
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre = :ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta = wf_fasi_risorse_umane( ls_cod_prodotto_figlio, fs_cod_versione, ldd_quan_utilizzo, ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

return 0
end function

public function integer wf_costo_rami_distinta (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quantita, integer fi_num_livello_stop, ref integer fi_num_livello_corrente, ref string fs_errore);/* Funzione che estrare i rami fino ad un certo livello e li valorizza tutti.

fs_cod_prodotto		= prodotto finito
fs_cod_versione		= versione del prodotto finito
fd_quantita  			= quantità del prodotto finito
fi_num_livello_stop  = numero del livello a cui fermarsi (0= arriva fino alla fine)


*/

string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_test,ls_cod_versione_figlio, ls_flag_mp, &
 			 ls_flag_valorizza, ld_des_prodotto
long      ll_num_righe,ll_num_figli, ll_num_sequenza, ll_riga
integer   li_risposta, li_ret
dec{4}    ldd_quan_utilizzo, ld_totale_costo, ld_costo_fase_pf
datastore lds_righe_distinta
uo_calcolo_costi_pf luo_calcolo_costi_pf

//Donato 24-10-2008
//questa variabile è stata definita perchè il report deve calcolare questa ultima parte 
//tenendo presente se l'operatore ha selezionato il calcolo per "prezzo_acquisto", "costo_standard", "costo_ultimo"
string ls_tipo_calcolo
//--------------------------

ll_num_figli = 1
fi_num_livello_corrente ++

lds_righe_distinta = Create DataStore
lds_righe_distinta.DataObject = "d_data_store_distinta"
lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fd_quantita
	
	// verifico se si tratta di SL o di una MP
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda 
	and 	 cod_prodotto_padre = :ls_cod_prodotto_figlio and
	       cod_versione = :ls_cod_versione_figlio ;
	
	// se MP salto.
	if sqlca.sqlcode=100 then continue
	
	select flag_materia_prima,
			 flag_valorizza_mp
	into	 :ls_flag_mp,
			 :ls_flag_valorizza
	from	 distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto_padre = :fs_cod_prodotto and
			 cod_versione = :fs_cod_versione and
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and
			 num_sequenza = :ll_num_sequenza;
			 
	if sqlca.sqlcode = 0 and not isnull( ls_flag_mp) and ls_flag_mp = "S" and ls_flag_valorizza = "N" and not isnull(ls_flag_valorizza) then
	elseif sqlca.sqlcode = 0 and not isnull( ls_flag_mp) and ls_flag_mp = "S" and ls_flag_valorizza = "S" then		
		continue
	end if	
	
	// procedo con la valorizzazione
	luo_calcolo_costi_pf = create uo_calcolo_costi_pf
	
	//Donato 24-10-2008 ---------------------------------------
	if ib_prezzo_ac then ls_tipo_calcolo = "A"
	if ib_costo_st then ls_tipo_calcolo = "S"
	if ib_costo_ul then ls_tipo_calcolo = "U"
		
	ld_totale_costo = luo_calcolo_costi_pf.uof_costo_totale_prodotto(ls_cod_prodotto_figlio, ls_cod_versione_figlio, ls_tipo_calcolo, ref ls_errore)
	//ld_totale_costo = luo_calcolo_costi_pf.uof_costo_totale_prodotto(ls_cod_prodotto_figlio, ls_cod_versione_figlio, "A", ref ls_errore)
	//-------------------------------------------------------------
		
	if ld_totale_costo < 0 then
		messagebox("SEP","CALCOLO COSTO TOTALE PRODOTTO.~r~n" + ls_errore)
		ld_totale_costo = 0
	end if
	
	ld_totale_costo = ld_totale_costo * ldd_quan_utilizzo
	
	ls_des_prodotto = f_des_tabella("anag_prodotti", "cod_prodotto ='" + ls_cod_prodotto_figlio + "'", "des_prodotto")
	
	ll_riga = dw_report.insertrow(0)
	
	//	Calcolo del costo della fase del prodotto corrente.
	ld_costo_fase_pf = 0
	li_ret = luo_calcolo_costi_pf.uof_costo_singola_fase(ls_cod_prodotto_figlio, ls_cod_versione_figlio, ldd_quan_utilizzo, ref ld_costo_fase_pf, ref ls_errore)
	if li_ret < 0 then
		messagebox("SEP","CALCOLO COSTO FASE PRODOTTO.~r~n" + ls_errore)
		ld_costo_fase_pf = 0
	end if
	
	ld_totale_costo = ld_totale_costo + ld_costo_fase_pf
	
	destroy luo_calcolo_costi_pf
	
	dw_report.setitem( ll_riga, "tipo_riga", 21)
	dw_report.setitem( ll_riga, "des_valorizzazione", ls_cod_prodotto_figlio + " " +ls_cod_versione_figlio + " " + ls_des_prodotto + " - costo parziale fase " + string(ld_totale_costo, "###,##0.00"))
	dw_report.setitem( ll_riga, "num_livello_sl", fi_num_livello_corrente)

	// verifico se ho raggiunto il livello desiderato: se raggiunto non vado più a findo nella distinta.
	if fi_num_livello_stop > 0 then
		if fi_num_livello_corrente >= fi_num_livello_stop  then continue
	end if

	// verifico se lo stesso prodotto è padre di un'altra distinta.
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto_padre=:ls_cod_prodotto_figlio and
	       cod_versione = :ls_cod_versione_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta = wf_costo_rami_distinta(ls_cod_prodotto_figlio,ls_cod_versione_figlio,ldd_quan_utilizzo,fi_num_livello_stop, ref fi_num_livello_corrente, ref fs_errore)
		if li_risposta = -1 then
			return -1
		end if
	end if
	
next

destroy lds_righe_distinta

return 0

end function

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report=true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

is_cod_prodotto = s_cs_xx.parametri.parametro_s_1

is_cod_versione = s_cs_xx.parametri.parametro_s_2

ib_tempo_attrezzaggio = false

ib_tempo_attrezzaggio_commessa = false

ib_tempo_lavorazione = false

ib_costo_st = false

ib_costo_ul = false

ib_prezzo_ac = false

ib_tempo_risorsa_umana = false

if s_cs_xx.parametri.parametro_s_3 = "true" then ib_tempo_attrezzaggio = true

if s_cs_xx.parametri.parametro_s_4 = "true" then ib_tempo_attrezzaggio_commessa = true

if s_cs_xx.parametri.parametro_s_5 = "true" then ib_tempo_lavorazione = true

if s_cs_xx.parametri.parametro_s_6 = "true" then ib_costo_st = true

if s_cs_xx.parametri.parametro_s_7 = "true" then ib_costo_ul = true

if s_cs_xx.parametri.parametro_s_8 = "true" then ib_prezzo_ac = true

if s_cs_xx.parametri.parametro_s_9 = "true" then ib_tempo_risorsa_umana = true

id_quantita = s_cs_xx.parametri.parametro_d_1		

wf_report()
end event

on w_report_costo_preventivo.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.dw_report
end on

on w_report_costo_preventivo.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.dw_report)
end on

type cb_stampa from commandbutton within w_report_costo_preventivo
integer x = 3726
integer y = 1900
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;

dw_report.print()
end event

type dw_report from uo_std_dw within w_report_costo_preventivo
integer x = 23
integer y = 20
integer width = 4069
integer height = 1860
string dataobject = "d_report_costo_preventivo"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

