﻿$PBExportHeader$w_report_ore_lavorate.srw
$PBExportComments$Report ore lavorate per dipendente
forward
global type w_report_ore_lavorate from w_cs_xx_principale
end type
type em_percentuale from editmask within w_report_ore_lavorate
end type
type cbx_escludi from checkbox within w_report_ore_lavorate
end type
type cbx_attrezzaggio_commessa from checkbox within w_report_ore_lavorate
end type
type cbx_attrezzaggio from checkbox within w_report_ore_lavorate
end type
type cbx_lavorazione from checkbox within w_report_ore_lavorate
end type
type rb_prezzo_ac from radiobutton within w_report_ore_lavorate
end type
type rb_costo_ul from radiobutton within w_report_ore_lavorate
end type
type rb_costo_st from radiobutton within w_report_ore_lavorate
end type
type cb_stampa from commandbutton within w_report_ore_lavorate
end type
type cb_report from commandbutton within w_report_ore_lavorate
end type
type st_3 from statictext within w_report_ore_lavorate
end type
type ddlb_operai from dropdownlistbox within w_report_ore_lavorate
end type
type em_a_data from editmask within w_report_ore_lavorate
end type
type st_2 from statictext within w_report_ore_lavorate
end type
type st_1 from statictext within w_report_ore_lavorate
end type
type em_da_data from editmask within w_report_ore_lavorate
end type
type dw_report_ore_dipendente from uo_cs_xx_dw within w_report_ore_lavorate
end type
type gb_1 from groupbox within w_report_ore_lavorate
end type
type gb_2 from groupbox within w_report_ore_lavorate
end type
type gb_3 from groupbox within w_report_ore_lavorate
end type
end forward

global type w_report_ore_lavorate from w_cs_xx_principale
integer width = 3808
integer height = 2380
string title = "Ore lavorate per dipendente"
em_percentuale em_percentuale
cbx_escludi cbx_escludi
cbx_attrezzaggio_commessa cbx_attrezzaggio_commessa
cbx_attrezzaggio cbx_attrezzaggio
cbx_lavorazione cbx_lavorazione
rb_prezzo_ac rb_prezzo_ac
rb_costo_ul rb_costo_ul
rb_costo_st rb_costo_st
cb_stampa cb_stampa
cb_report cb_report
st_3 st_3
ddlb_operai ddlb_operai
em_a_data em_a_data
st_2 st_2
st_1 st_1
em_da_data em_da_data
dw_report_ore_dipendente dw_report_ore_dipendente
gb_1 gb_1
gb_2 gb_2
gb_3 gb_3
end type
global w_report_ore_lavorate w_report_ore_lavorate

forward prototypes
public function integer wf_costomp (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quantita, string fs_tipo_costo, ref decimal fd_costo, ref string fs_errore)
public function integer wf_costo_scarto (string fs_cod_operaio, datetime fdt_data_giorno, string fs_tipo_costo, ref decimal fd_costo_scarto, ref string fs_errore)
public function integer wf_costo_fasi (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quantita_richiesta, ref decimal fd_costo_fasi, ref string fs_errore)
public function integer wf_inserisci (string fs_tipo, datetime fdt_data_corrente, string fs_giorno_it, decimal fd_ore_rilevate, decimal fd_ore_calcolate, decimal fd_scostamento_minuti, decimal fd_scostamento_lire, decimal fd_costo_scarto)
end prototypes

public function integer wf_costomp (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quantita, string fs_tipo_costo, ref decimal fd_costo, ref string fs_errore);integer li_risposta
string ls_mat_prima[]
decimal ld_quantita_totale,ld_costo_materia_prima
double ld_quan_utilizzo[]
long ll_t,ll_null

setnull(ll_null)

li_risposta=f_trova_mat_prima(fs_cod_prodotto,fs_cod_versione,ls_mat_prima[],ld_quan_utilizzo[],1,ll_null,ll_null)

for ll_t=1 to upperbound(ls_mat_prima)

  ld_quantita_totale = ld_quan_utilizzo[ll_t] * fd_quantita

  choose case fs_tipo_costo
    case 'A'
		select prezzo_acquisto
		into	 :ld_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_prodotto = :ls_mat_prima[ll_t];


	 case 'S'
	   select costo_standard
		into	 :ld_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_prodotto = :ls_mat_prima[ll_t];

 	 case 'U'
		select costo_ultimo
		into	 :ld_costo_materia_prima
		from 	 anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    cod_prodotto = :ls_mat_prima[ll_t];
		
  end choose
	
  fd_costo = fd_costo + ld_costo_materia_prima * ld_quantita_totale

next

return 0
end function

public function integer wf_costo_scarto (string fs_cod_operaio, datetime fdt_data_giorno, string fs_tipo_costo, ref decimal fd_costo_scarto, ref string fs_errore);//Calcolo del costo dello scarto
string  ls_cod_errore,ls_flag_causa_operaio,ls_cod_prodotto,ls_cod_versione,ls_errore
decimal ld_quan_scarto,ld_costo_mp,ld_costo_fasi,ld_costo_difformita
integer li_risposta

declare r_difetti_out cursor for
select  cod_errore,
		  quan_scarto
from    difetti_det_orari_out
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_operaio=:fs_cod_operaio
and     data_difetto=:fdt_data_giorno;


open r_difetti_out;

do while 1=1
	fetch r_difetti_out
	into  :ls_cod_errore,
			:ld_quan_scarto;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close r_difetti_out;
		return -1
	end if
	
	select flag_causa_operaio,
			 cod_prodotto,
			 cod_versione,
			 costo_difformita
	into   :ls_flag_causa_operaio,
			 :ls_cod_prodotto,
			 :ls_cod_versione,
			 :ld_costo_difformita
	from   tab_difformita
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_errore=:ls_cod_errore;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close r_difetti_out;
		return -1
	end if

	
	// per i tempi da usare per il calcolo del costo usiamo i preventivi (quelli nelle fasi di lavoro) o quelli 
	// consuntivi provenienti quindi dall'avanzamento di produzione?
	// per ora usiamo i preventivi
	
	if ls_flag_causa_operaio ='S' then
		
		if ld_costo_difformita<>0 and not isnull(ld_costo_difformita) then
			fd_costo_scarto = fd_costo_scarto + ld_costo_difformita
		else
			li_risposta = wf_costomp (ls_cod_prodotto, &
											  ls_cod_versione, &
											  ld_quan_scarto, &
											  fs_tipo_costo, &
											  ld_costo_mp, &
											  ls_errore )
			
			
			if li_risposta<0 then
				fs_errore = ls_errore
				return -1
			end if
			
			li_risposta = wf_costo_fasi (ls_cod_prodotto, &
												  ls_cod_versione, &
												  ld_quan_scarto, &
												  ld_costo_fasi, &
												  ls_errore )
					
			if li_risposta<0 then
				fs_errore = ls_errore
				return -1
			end if
			
			fd_costo_scarto = fd_costo_scarto + ld_costo_mp + ld_costo_fasi
			
		end if
		
	end if

loop

close r_difetti_out;

return 0
end function

public function integer wf_costo_fasi (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quantita_richiesta, ref decimal fd_costo_fasi, ref string fs_errore);string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore, & 
			 ls_test,ls_cod_cat_attrezzature,&
			 ls_cod_cat_attrezzature_2
long      ll_num_righe,ll_num_figli
integer   li_risposta
decimal   ld_quan_utilizzo,ld_tempo_attrezzaggio,ld_tempo_attrezzaggio_commessa, & 
			 ld_tempo_lavorazione, ld_tempo_totale, ld_costo_medio_orario, ld_costo_totale, &
			 ld_tempo_risorsa_umana,ld_costo_medio_orario_2,ld_costo,ld_totale

datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ld_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fd_quantita_richiesta
		
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda=:s_cs_xx.cod_azienda 
	and 	 cod_prodotto_padre=:ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode=100 then continue

   SELECT cod_cat_attrezzature,
			 tempo_attrezzaggio,
		    tempo_attrezzaggio_commessa,
			 tempo_lavorazione,
			 cod_cat_attrezzature_2,
			 tempo_risorsa_umana
   INTO   :ls_cod_cat_attrezzature,
 			 :ld_tempo_attrezzaggio,
		    :ld_tempo_attrezzaggio_commessa,
			 :ld_tempo_lavorazione,
			 :ls_cod_cat_attrezzature_2,
			 :ld_tempo_risorsa_umana
   FROM   tes_fasi_lavorazione
   WHERE  cod_azienda = :s_cs_xx.cod_azienda  
	AND    cod_prodotto = :ls_cod_prodotto_figlio;

	if sqlca.sqlcode=100 then
		fs_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_figlio
		return -1
	end if
  
	select costo_medio_orario
	into   :ld_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;

	select costo_medio_orario
	into   :ld_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature_2;
	
	ld_tempo_lavorazione = ld_tempo_lavorazione * ld_quan_utilizzo
	ld_tempo_risorsa_umana = ld_tempo_risorsa_umana * ld_quan_utilizzo

	ld_tempo_totale = 0

	ld_tempo_totale = ld_tempo_totale + ld_tempo_lavorazione + ld_tempo_attrezzaggio + ld_tempo_attrezzaggio_commessa

	ld_costo_totale = (ld_tempo_totale/60) * ld_costo_medio_orario + (ld_tempo_risorsa_umana/60) * ld_costo_medio_orario_2

	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:ls_cod_prodotto_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta=wf_costo_fasi(ls_cod_prodotto_figlio,fs_cod_versione,ld_quan_utilizzo,ld_costo,ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
		ld_costo_totale = ld_costo_totale+ld_costo
	end if
	
	ld_totale = ld_totale + ld_costo_totale
	
next

fd_costo_fasi = ld_totale

return 0
end function

public function integer wf_inserisci (string fs_tipo, datetime fdt_data_corrente, string fs_giorno_it, decimal fd_ore_rilevate, decimal fd_ore_calcolate, decimal fd_scostamento_minuti, decimal fd_scostamento_lire, decimal fd_costo_scarto);long ll_righe

ll_righe = dw_report_ore_dipendente.insertrow(0)
dw_report_ore_dipendente.setitem(ll_righe,"tipo",fs_tipo)
dw_report_ore_dipendente.setitem(ll_righe,"data_giorno",fdt_data_corrente)
dw_report_ore_dipendente.setitem(ll_righe,"giorno",fs_giorno_it)
dw_report_ore_dipendente.setitem(ll_righe,"ore_rilevate",fd_ore_rilevate)
dw_report_ore_dipendente.setitem(ll_righe,"ore_calcolate",fd_ore_calcolate)
dw_report_ore_dipendente.setitem(ll_righe,"scostamento_min",fd_scostamento_minuti)
dw_report_ore_dipendente.setitem(ll_righe,"scostamento_lire",fd_scostamento_lire)
dw_report_ore_dipendente.setitem(ll_righe,"costo_scarto",fd_costo_scarto)

return 0
end function

on w_report_ore_lavorate.create
int iCurrent
call super::create
this.em_percentuale=create em_percentuale
this.cbx_escludi=create cbx_escludi
this.cbx_attrezzaggio_commessa=create cbx_attrezzaggio_commessa
this.cbx_attrezzaggio=create cbx_attrezzaggio
this.cbx_lavorazione=create cbx_lavorazione
this.rb_prezzo_ac=create rb_prezzo_ac
this.rb_costo_ul=create rb_costo_ul
this.rb_costo_st=create rb_costo_st
this.cb_stampa=create cb_stampa
this.cb_report=create cb_report
this.st_3=create st_3
this.ddlb_operai=create ddlb_operai
this.em_a_data=create em_a_data
this.st_2=create st_2
this.st_1=create st_1
this.em_da_data=create em_da_data
this.dw_report_ore_dipendente=create dw_report_ore_dipendente
this.gb_1=create gb_1
this.gb_2=create gb_2
this.gb_3=create gb_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_percentuale
this.Control[iCurrent+2]=this.cbx_escludi
this.Control[iCurrent+3]=this.cbx_attrezzaggio_commessa
this.Control[iCurrent+4]=this.cbx_attrezzaggio
this.Control[iCurrent+5]=this.cbx_lavorazione
this.Control[iCurrent+6]=this.rb_prezzo_ac
this.Control[iCurrent+7]=this.rb_costo_ul
this.Control[iCurrent+8]=this.rb_costo_st
this.Control[iCurrent+9]=this.cb_stampa
this.Control[iCurrent+10]=this.cb_report
this.Control[iCurrent+11]=this.st_3
this.Control[iCurrent+12]=this.ddlb_operai
this.Control[iCurrent+13]=this.em_a_data
this.Control[iCurrent+14]=this.st_2
this.Control[iCurrent+15]=this.st_1
this.Control[iCurrent+16]=this.em_da_data
this.Control[iCurrent+17]=this.dw_report_ore_dipendente
this.Control[iCurrent+18]=this.gb_1
this.Control[iCurrent+19]=this.gb_2
this.Control[iCurrent+20]=this.gb_3
end on

on w_report_ore_lavorate.destroy
call super::destroy
destroy(this.em_percentuale)
destroy(this.cbx_escludi)
destroy(this.cbx_attrezzaggio_commessa)
destroy(this.cbx_attrezzaggio)
destroy(this.cbx_lavorazione)
destroy(this.rb_prezzo_ac)
destroy(this.rb_costo_ul)
destroy(this.rb_costo_st)
destroy(this.cb_stampa)
destroy(this.cb_report)
destroy(this.st_3)
destroy(this.ddlb_operai)
destroy(this.em_a_data)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_da_data)
destroy(this.dw_report_ore_dipendente)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.gb_3)
end on

event pc_setwindow;call super::pc_setwindow;dw_report_ore_dipendente.ib_dw_report = true

f_PO_LoadDDLB(ddlb_operai, sqlca, "anag_operai", &	
				  "cod_operaio", "cognome", &
				  "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
				  
return 0
end event

type em_percentuale from editmask within w_report_ore_lavorate
integer x = 2537
integer y = 2000
integer width = 389
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
string text = "none"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
end type

type cbx_escludi from checkbox within w_report_ore_lavorate
integer x = 1829
integer y = 2000
integer width = 603
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Escludi righe fuori %"
boolean lefttext = true
end type

event clicked;if em_percentuale.enabled then 
	em_percentuale.enabled = false
else
	em_percentuale.enabled = true
end if
end event

type cbx_attrezzaggio_commessa from checkbox within w_report_ore_lavorate
integer x = 914
integer y = 2000
integer width = 718
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Attrezzaggio Commessa"
boolean lefttext = true
end type

type cbx_attrezzaggio from checkbox within w_report_ore_lavorate
integer x = 1221
integer y = 2080
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Attrezzaggio"
boolean lefttext = true
end type

type cbx_lavorazione from checkbox within w_report_ore_lavorate
integer x = 1230
integer y = 2160
integer width = 402
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Lavorazione"
boolean checked = true
boolean lefttext = true
end type

type rb_prezzo_ac from radiobutton within w_report_ore_lavorate
integer x = 50
integer y = 1992
integer width = 503
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Prezzo Acquisto"
boolean checked = true
boolean lefttext = true
end type

type rb_costo_ul from radiobutton within w_report_ore_lavorate
integer x = 142
integer y = 2080
integer width = 411
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Costo Ultimo"
boolean lefttext = true
end type

type rb_costo_st from radiobutton within w_report_ore_lavorate
integer x = 69
integer y = 2164
integer width = 485
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Costo Standard"
boolean lefttext = true
end type

type cb_stampa from commandbutton within w_report_ore_lavorate
integer x = 2994
integer y = 2180
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;dw_report_ore_dipendente.print()
end event

type cb_report from commandbutton within w_report_ore_lavorate
integer x = 3383
integer y = 2180
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string   ls_cod_operaio,ls_giorno,ls_giorno_it,ls_cod_prodotto,ls_cod_cat_attrezzature,ls_problemi_produzione, & 
			ls_cognome,ls_errore,ls_tipo_costo,ls_cod_prodotto_test
datetime ldt_da_data,ldt_a_data,ldt_data_corrente,lt_orario_attrezzaggio,lt_orario_attrezzaggio_commessa, &
			lt_orario_lavorazione,lt_orario_risorsa_umana,lt_orario_attrezzaggio_fine,lt_orario_attrezzaggio_commessa_fine, &
			lt_orario_lavorazione_fine,lt_orario_risorsa_umana_fine
decimal  ld_secondi,ld_minuti,ld_ore,ld_quan_prodotta,ld_tempo_attrezzaggio,ld_tempo_attrezzaggio_commessa,ld_tempo_lavorazione, &
			ld_tempo_risorsa_umana,ld_scostamento_min_attrezzaggio,ld_scostamento_min_attrezzaggio_commessa, & 
			ld_scostamento_min_lavorazione,ld_costo_scarto,ld_costo_medio_orario, & 
			ld_somma_costo_medio_orario,ld_media_costo_medio_orario,ld_scostamento_valuta_lavorazione, & 
			ld_scostamento_valuta_attrezzaggio_commessa,ld_scostamento_valuta_attrezzaggio, &
			ld_minuti_lavorazione,ld_minuti_attrezzaggio,ld_minuti_attrezzaggio_commessa,ld_ore_lavorazione,ld_ore_attrezzaggio, & 
			ld_ore_attrezzaggio_commessa,ld_somma_tempo_attrezzaggio,ld_somma_tempo_attrezzaggio_commessa,ld_somma_tempo_lavorazione
long     ll_righe,ll_conteggio
integer  li_risposta


// Problemi aperti:
// 1. distribuzione dei tempi di attrezzaggio nei primi giorni di produzione commessa, pertanto anche se il dip. non produce
//    in realtà ha fatto del lavoro
// 2. tempi di attrezzaggio commessa e attrezzaggio non proporzionali nel senso che non sono calcolati, nel tempo teorico, 1 volta per commessa/prodotto
//    ma ogni giorno ogni volta che appare quel ben determinato prodotto (eventualmente pensare ad una media da fare alla fine)
// 3. media del costo medio (abbastanza buono il calcolo)
// 4. verificare eventuale possibilità di escludere dal calcolo i tempi di attrezz e attrezz com con delle check box  
// 5. Manca ancora l'algoritmo di calcolo del costo dello scarto.


if rb_prezzo_ac.checked = true then
	ls_tipo_costo = "A"
end if

if rb_costo_st.checked = true then
	ls_tipo_costo = "S"
end if

if rb_costo_ul.checked = true then
	ls_tipo_costo = "U"
end if

dw_report_ore_dipendente.reset()
ls_cod_operaio = f_po_selectddlb(ddlb_operai)
ldt_da_data = datetime(date(em_da_data.text),00:00:00)
ldt_a_data = datetime(date(em_a_data.text),00:00:00)

select cognome
into   :ls_cognome
from   anag_operai
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_operaio=:ls_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

dw_report_ore_dipendente.modify("dipendente.text='"+ls_cognome+"'")
dw_report_ore_dipendente.modify("periodo.text=' dal "+string(date(ldt_da_data)) + " al " + string(date(ldt_a_data)) +"'" )
ldt_data_corrente = ldt_da_data

do while ldt_data_corrente <= ldt_a_data
	
	declare r_det_orari cursor for 
	select orario_attrezzaggio,
			 orario_attrezzaggio_commessa,
			 orario_lavorazione,
			 orario_risorsa_umana,
			 cod_prodotto
	from   det_orari_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    data_giorno = :ldt_data_corrente
	and    flag_inizio = 'S'
	and    cod_operaio = :ls_cod_operaio
	order by prog_orari;
	
	open r_det_orari;
	
	do while 1 = 1 
	
		fetch r_det_orari
		into  :lt_orario_attrezzaggio,
				:lt_orario_attrezzaggio_commessa,
				:lt_orario_lavorazione,
				:lt_orario_risorsa_umana,
				:ls_cod_prodotto_test;
		
		if sqlca.sqlcode = 100 then exit

		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_orari;
			return
		end if
	
		if time(lt_orario_attrezzaggio) <> 00:00:00 then

			declare r_d1 cursor for 
			select orario_attrezzaggio
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio = :ls_cod_operaio
			and    orario_attrezzaggio is not null
			and    orario_attrezzaggio > :lt_orario_attrezzaggio
			and    cod_prodotto=:ls_cod_prodotto_test
			order by orario_attrezzaggio;
			
			open r_d1;
			
			fetch r_d1
			into  :lt_orario_attrezzaggio_fine;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d1;
				close r_det_orari;
				return
			end if
			
			close r_d1;
			ld_secondi = SecondsAfter ( time(lt_orario_attrezzaggio),time(lt_orario_attrezzaggio_fine))
			ld_minuti_attrezzaggio = ld_minuti_attrezzaggio + ld_secondi/60
			ld_secondi=0
		end if	

		if time(lt_orario_attrezzaggio_commessa) <> 00:00:00 then
			declare r_d2 cursor for 
			select orario_attrezzaggio_commessa
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio = :ls_cod_operaio
			and    orario_attrezzaggio_commessa is not null
			and    orario_attrezzaggio_commessa > :lt_orario_attrezzaggio_commessa
			and    cod_prodotto=:ls_cod_prodotto_test
			order by orario_attrezzaggio_commessa;
			
			open r_d2;
			
			fetch r_d2
			into  :lt_orario_attrezzaggio_commessa_fine;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d2;
				close r_det_orari;
				return
			end if
			
			close r_d2;
			ld_secondi = SecondsAfter ( time(lt_orario_attrezzaggio_commessa),time(lt_orario_attrezzaggio_commessa_fine))
			ld_minuti_attrezzaggio_commessa = ld_minuti_attrezzaggio_commessa + ld_secondi/60
			ld_secondi = 0
		end if	

		if time(lt_orario_lavorazione) <> 00:00:00  then
			declare r_d3 cursor for 
			select orario_lavorazione,
					 problemi_produzione
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio = :ls_cod_operaio
			and    orario_lavorazione is not null
			and    orario_lavorazione > :lt_orario_lavorazione
			and    cod_prodotto=:ls_cod_prodotto_test
			order by orario_lavorazione;
			
			open r_d3;
			
			fetch r_d3
			into  :lt_orario_lavorazione_fine,
					:ls_problemi_produzione;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d3;
				close r_det_orari;
				return
			end if
			
			close r_d3;
			if ls_problemi_produzione ='N' or isnull(ls_problemi_produzione) then
				ld_secondi = SecondsAfter ( time(lt_orario_lavorazione),time(lt_orario_lavorazione_fine))
				ld_minuti_lavorazione = ld_minuti_lavorazione + ld_secondi/60			
			end if
		end if	

//		if time(lt_orario_risorsa_umana) <> 00:00:00 then
//			declare r_d4 cursor for 
//			select orario_risorsa_umana
//			from   det_orari_produzione
//			where  cod_azienda = :s_cs_xx.cod_azienda
//			and    data_giorno = :ldt_data_corrente
//			and    flag_inizio = 'N'
//			and    cod_operaio = :ls_cod_operaio
//			and    orario_risorsa_umana is not null
//			and    orario_risorsa_umana >= :lt_orario_risorsa_umana
//			order by orario_risorsa_umana;
//			
//			open r_d4;
//			
//			fetch r_d4
//			into  :lt_orario_risorsa_umana_fine;
//			
//			if sqlca.sqlcode < 0 then 
//				messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//				close r_d4;
//				close r_det_orari;
//				return
//			end if
//			
//			close r_d4;
//			ld_secondi = SecondsAfter ( time(lt_orario_risorsa_umana),time(lt_orario_risorsa_umana_fine))
//			ld_minuti = ld_minuti + ld_secondi/60			
//		
//		end if	

	loop
	
	
	close r_det_orari;

// ********************************** Calcola le ore teoriche in base alle qtà prodotte

	declare r_somma cursor for 
	select sum(quan_prodotta),
			 cod_prodotto 
	from   det_orari_produzione 
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_operaio =:ls_cod_operaio
	and    data_giorno = :ldt_data_corrente
	and    flag_inizio = 'N'
	group by cod_prodotto;
	
	open r_somma;
	
	do while 1 = 1
		fetch r_somma
		into   :ld_quan_prodotta,
				 :ls_cod_prodotto;
	
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_somma;
			return
		end if
	
		if sqlca.sqlcode = 100 then exit
		
		select tempo_attrezzaggio,
				 tempo_attrezzaggio_commessa,
				 tempo_lavorazione,
				 tempo_risorsa_umana,
				 cod_cat_attrezzature_2
		into   :ld_tempo_attrezzaggio,
				 :ld_tempo_attrezzaggio_commessa,
				 :ld_tempo_lavorazione,
				 :ld_tempo_risorsa_umana,
				 :ls_cod_cat_attrezzature
		from   tes_fasi_lavorazione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_somma;
			return
		end if
	
		select costo_medio_orario
		into   :ld_costo_medio_orario
		from   tab_cat_attrezzature
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;
	
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_somma;
			return
		end if
	
		if isnull(ld_tempo_attrezzaggio) then ld_tempo_attrezzaggio = 0
		if isnull(ld_tempo_attrezzaggio_commessa) then ld_tempo_attrezzaggio_commessa = 0
		if isnull(ld_tempo_lavorazione) then ld_tempo_lavorazione = 0
		if isnull(ld_tempo_risorsa_umana) then ld_tempo_risorsa_umana = 0
				
		ld_somma_tempo_attrezzaggio = ld_somma_tempo_attrezzaggio + ld_tempo_attrezzaggio
		ld_somma_tempo_attrezzaggio_commessa = ld_somma_tempo_attrezzaggio_commessa + ld_tempo_attrezzaggio_commessa
		ld_somma_tempo_lavorazione = ld_somma_tempo_lavorazione + ld_tempo_lavorazione*ld_quan_prodotta
		
		ld_somma_costo_medio_orario =	ld_somma_costo_medio_orario + ld_costo_medio_orario 
		ll_conteggio++	
	loop
	
	close r_somma;
	if ll_conteggio > 0 then
		ld_media_costo_medio_orario = ld_somma_costo_medio_orario/ll_conteggio
	else
		ld_media_costo_medio_orario = 0
	end if

// ********************************** fine del calcolo ore teoriche

	ls_giorno = DayName (date(ldt_data_corrente))
	choose case ls_giorno
		case "Sunday"
			ls_giorno_it = "Dom."
		case "Monday"
			ls_giorno_it = "Lun."
		case "Tuesday"
			ls_giorno_it = "Mar."
		case "Wednesday"
			ls_giorno_it = "Mer."
		case "Thursday"
			ls_giorno_it = "Gio."
		case "Friday"
			ls_giorno_it = "Ven."
		case "Saturday"
			ls_giorno_it = "Sab."
	end choose
	
	ld_scostamento_min_attrezzaggio = ld_somma_tempo_attrezzaggio - ld_minuti_attrezzaggio
	ld_scostamento_min_attrezzaggio_commessa = ld_somma_tempo_attrezzaggio_commessa - ld_minuti_attrezzaggio_commessa
	ld_scostamento_min_lavorazione = ld_somma_tempo_lavorazione - ld_minuti_lavorazione
	
	ld_scostamento_valuta_attrezzaggio = ld_media_costo_medio_orario * ld_scostamento_min_attrezzaggio/60
	ld_scostamento_valuta_attrezzaggio_commessa = ld_media_costo_medio_orario * ld_scostamento_min_attrezzaggio_commessa/60
	ld_scostamento_valuta_lavorazione = ld_media_costo_medio_orario * ld_scostamento_min_lavorazione/60
	
	li_risposta = wf_costo_scarto(ls_cod_operaio,ldt_data_corrente,ls_tipo_costo,ld_costo_scarto,ls_errore)
	
	if li_risposta < 0 then
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		return
	end if
	
	// attrezzaggio
	if (ld_minuti_attrezzaggio <> 0 or ld_somma_tempo_attrezzaggio <> 0) and cbx_attrezzaggio.checked = true then
		if	cbx_escludi.checked = true then	
			if abs(ld_scostamento_min_attrezzaggio/ld_somma_tempo_attrezzaggio)*100 < double(em_percentuale.text) then
				wf_inserisci("A",& 
								 ldt_data_corrente,&
								 ls_giorno_it,&
								 ld_minuti_attrezzaggio/60,&
								 ld_somma_tempo_attrezzaggio/60,&
								 ld_scostamento_min_attrezzaggio,&
								 ld_scostamento_valuta_attrezzaggio,&
								 0)
			end if
		else
			wf_inserisci("A",& 
							 ldt_data_corrente,&
							 ls_giorno_it,&
							 ld_minuti_attrezzaggio/60,&
							 ld_somma_tempo_attrezzaggio/60,&
							 ld_scostamento_min_attrezzaggio,&
							 ld_scostamento_valuta_attrezzaggio,&
							 0)
		end if
	end if
	
	// attrezzaggio_commessa
	if (ld_minuti_attrezzaggio_commessa <> 0 or ld_somma_tempo_attrezzaggio_commessa <> 0) and cbx_attrezzaggio_commessa.checked = true then
		if	cbx_escludi.checked = true then	
			if abs(ld_scostamento_min_attrezzaggio_commessa/ld_somma_tempo_attrezzaggio_commessa)*100 < double(em_percentuale.text) then
				wf_inserisci("AC",& 
								 ldt_data_corrente,&
								 ls_giorno_it,&
								 ld_minuti_attrezzaggio_commessa/60,&
								 ld_somma_tempo_attrezzaggio_commessa/60,&
								 ld_scostamento_min_attrezzaggio_commessa,&
								 ld_scostamento_valuta_attrezzaggio_commessa,&
								 0)
			end if
		else
			wf_inserisci("AC",& 
							 ldt_data_corrente,&
							 ls_giorno_it,&
							 ld_minuti_attrezzaggio_commessa/60,&
							 ld_somma_tempo_attrezzaggio_commessa/60,&
							 ld_scostamento_min_attrezzaggio_commessa,&
							 ld_scostamento_valuta_attrezzaggio_commessa,&
							 0)
		end if
	end if
	
	// lavorazione
	if (ld_minuti_lavorazione <> 0 or ld_somma_tempo_lavorazione <> 0) and cbx_lavorazione.checked = true then
		if	cbx_escludi.checked = true then	
			if abs(ld_scostamento_min_lavorazione/ld_somma_tempo_lavorazione)*100 < double(em_percentuale.text) then
				wf_inserisci("L",& 
								 ldt_data_corrente,&
								 ls_giorno_it,&
								 ld_minuti_lavorazione/60,&
								 ld_somma_tempo_lavorazione/60,&
								 ld_scostamento_min_lavorazione,&
								 ld_scostamento_valuta_lavorazione,&
								 ld_costo_scarto)
			end if
		else
			wf_inserisci("L",& 
							 ldt_data_corrente,&
							 ls_giorno_it,&
							 ld_minuti_lavorazione/60,&
							 ld_somma_tempo_lavorazione/60,&
							 ld_scostamento_min_lavorazione,&
							 ld_scostamento_valuta_lavorazione,&
							 ld_costo_scarto)
		end if
	end if
	
	ldt_data_corrente = datetime(RelativeDate ( date(ldt_data_corrente) , 1 ),00:00:00)
	
   ld_costo_scarto = 0
	
	ld_somma_tempo_attrezzaggio = 0
	ld_minuti_attrezzaggio = 0
	ld_somma_tempo_attrezzaggio_commessa  = 0
	ld_minuti_attrezzaggio_commessa =0
	ld_somma_tempo_lavorazione = 0
	ld_minuti_lavorazione = 0
		
loop
end event

type st_3 from statictext within w_report_ore_lavorate
integer x = 1394
integer y = 40
integer width = 306
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Dipendente"
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_operai from dropdownlistbox within w_report_ore_lavorate
integer x = 1714
integer y = 20
integer width = 937
integer height = 1480
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type em_a_data from editmask within w_report_ore_lavorate
integer x = 960
integer y = 20
integer width = 411
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_2 from statictext within w_report_ore_lavorate
integer x = 709
integer y = 40
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "A data"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_report_ore_lavorate
integer x = 23
integer y = 40
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Da data"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_da_data from editmask within w_report_ore_lavorate
integer x = 274
integer y = 20
integer width = 411
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type dw_report_ore_dipendente from uo_cs_xx_dw within w_report_ore_lavorate
integer x = 23
integer y = 140
integer width = 3726
integer height = 1760
string title = "none"
string dataobject = "d_report_ore_dipendente"
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;s_cs_xx.parametri.parametro_s_1 = f_po_selectddlb(ddlb_operai)
s_cs_xx.parametri.parametro_data_1 = dw_report_ore_dipendente.getitemdatetime(dw_report_ore_dipendente.getrow(),"data_giorno")

window_open(w_origine_det_orari,0)
end event

type gb_1 from groupbox within w_report_ore_lavorate
integer x = 23
integer y = 1920
integer width = 800
integer height = 340
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Tipo costo materia prima"
end type

type gb_2 from groupbox within w_report_ore_lavorate
integer x = 869
integer y = 1920
integer width = 869
integer height = 340
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Includi tempo"
end type

type gb_3 from groupbox within w_report_ore_lavorate
integer x = 1783
integer y = 1920
integer width = 1189
integer height = 340
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Esclusioni"
end type

