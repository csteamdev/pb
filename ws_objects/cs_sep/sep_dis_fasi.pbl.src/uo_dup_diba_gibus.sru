﻿$PBExportHeader$uo_dup_diba_gibus.sru
forward
global type uo_dup_diba_gibus from nonvisualobject
end type
end forward

global type uo_dup_diba_gibus from nonvisualobject
end type
global uo_dup_diba_gibus uo_dup_diba_gibus

type variables
transaction itran_gibus

string is_cod_azienda_gibus = "A01"
string is_reg_chiave_root = "HKEY_LOCAL_MACHINE\SOFTWARE\Consulting&Software\"
string	is_ini_section = ""
string is_path_log = ""
boolean ib_log = false
datawindow idw_dw_log
end variables

forward prototypes
public subroutine uof_disconnetti ()
public function integer uof_connetti (ref string fs_errore)
public function integer uof_duplica_formule_legami (string fs_cod_prodotto_finito_destinazione, string fs_cod_versione_finito_destinazione, string fs_cod_prodotto_figlio, string fs_cod_versione_destinazione, string fs_cod_prodotto_finito_origine, string fs_cod_versione_finito_origine, string fs_cod_versione_partenza, ref string fs_errore)
public function integer uof_duplica_gruppi_varianti (string fs_cod_prodotto_destinazione, string fs_cod_versione_destinazione, string fs_cod_prodotto_partenza, string fs_cod_versione_partenza, long fl_num_sequenza, string fs_cod_prodotto_figlio, string fs_cod_versione_figlio, ref string fs_errore)
public function integer uof_duplica_distinta_ricors (string fs_cod_prodotto_finito_origine, string fs_cod_versione_finito_origine, string fs_cod_prodotto_finito_destinazione, string fs_cod_versione_finito_destinazione, string fs_cod_prodotto_partenza, string fs_cod_prodotto_destinazione, string fs_cod_versione_partenza, string fs_cod_versione_destinazione, long fl_num_livello_cor, ref string fs_errore)
public function integer uof_duplica_fasi_lavorazione (string fs_cod_prodotto_figlio, string fs_cod_versione_figlio, string fs_cod_versione_destinazione, ref string fs_errore)
public function integer uof_duplica_distinta (string fs_cod_prodotto_partenza, string fs_cod_versione_partenza, string fs_cod_prodotto_destinazione, string fs_cod_versione_destinazione, string fs_des_versione, ref datawindow fdw_datawindow, ref string fs_errore)
public function integer uof_duplica_tabelle_taglio (string fs_cod_prodotto_destinazione, string fs_cod_versione_destinazione, string fs_cod_prodotto_partenza, string fs_cod_versione_partenza, long fl_num_sequenza, string fs_cod_prodotto_figlio, string fs_cod_versione_figlio, ref string fs_errore)
end prototypes

public subroutine uof_disconnetti ();//esegue la disconnessione dal database delle presenze e la distruzione della transazione

rollback using itran_gibus;
disconnect using itran_gibus;
destroy itran_gibus;

//if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)

return
end subroutine

public function integer uof_connetti (ref string fs_errore);string ls_logpass, ls_option
int li_risposta, ll_ret

string ls_reg_chiave_root
string	ls_ini_section

ls_reg_chiave_root = is_reg_chiave_root
ls_ini_section = is_ini_section

itran_gibus = create transaction

// -- Leggo i dati dal registro per la connessione al db gibus
li_risposta = registryget(ls_reg_chiave_root + ls_ini_section, "servername", itran_gibus.ServerName)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazioni del database sul registro: servername."
	return -1
end if

li_risposta = registryget(ls_reg_chiave_root + ls_ini_section, "dbms", itran_gibus.DBMS)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazioni del database sul registro: dbms."
	return -1
end if

li_risposta = registryget(ls_reg_chiave_root + ls_ini_section, "database", itran_gibus.Database)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro: database."
	return -1
end if

li_risposta = registryget(ls_reg_chiave_root + ls_ini_section, "logid", itran_gibus.LogId)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazioni del database sul registro: logid."
	return -1
end if

li_risposta = registryget(ls_reg_chiave_root + ls_ini_section, "logpass", ls_logpass)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazioni del database sul registro: logpass."
	return -1
end if

	
if isnull(ls_logpass) or ls_logpass="" then		
	fs_errore = "Manca la password per l'accesso al database è necessario impostarla nel registro altrimenti non è possibile accedere al sistema."
	return -1	
end if	
	
n_cst_crypto luo_crypto
luo_crypto = create n_cst_crypto
ll_ret = luo_crypto.decryptdata( ls_logpass, ls_logpass)  
destroy luo_crypto;
if ll_ret < 0 then
	fs_errore = "La password contiene caratteri non consentiti.I caratteri consentiti comprendono:" + &
						"- tutte le cifre numeriche 0,1,2,...,9  tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z" + &
						"- alcuni simboli.Modificare la password e riprovare"
	return -1
end if

itran_gibus.LogPass = ls_logpass
	

li_risposta = registryget(ls_reg_chiave_root + ls_ini_section, "userid", itran_gibus.UserId)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazioni del database sul registro: userid."
	return -1
end if

li_risposta = registryget(ls_reg_chiave_root + ls_ini_section, "dbpass", itran_gibus.DBPass)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazioni del database sul registro: dbpass."
	return -1
end if

li_risposta = registryget(ls_reg_chiave_root + ls_ini_section, "dbparm", itran_gibus.DBParm)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro: dbparm."
	return -1
end if

li_risposta = registryget(ls_reg_chiave_root + ls_ini_section, "option", ls_option)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro: option."
	return -1
end if

itran_gibus.dbparm = itran_gibus.dbparm + ls_option

li_risposta = registryget(ls_reg_chiave_root + ls_ini_section, "lock", itran_gibus.Lock)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazioni del database sul registro: lock."
	return -1
end if
	 
disconnect using itran_gibus;
connect using itran_gibus;
if itran_gibus.sqlcode <> 0 then
	fs_errore = "Errore durante la connessione al database~r~n" + itran_gibus.sqlerrtext
	destroy itran_gibus;
	return -1
end if

return 0
end function

public function integer uof_duplica_formule_legami (string fs_cod_prodotto_finito_destinazione, string fs_cod_versione_finito_destinazione, string fs_cod_prodotto_figlio, string fs_cod_versione_destinazione, string fs_cod_prodotto_finito_origine, string fs_cod_versione_finito_origine, string fs_cod_versione_partenza, ref string fs_errore);datastore lds_data
string ls_sql, ls_cod_prodotto_figlio_ds, ls_cod_formula_ds, ls_nota_operativa_ds, ls_cod_prodotto_padre_ds, ls_cod_versione_padre_ds, ls_flag_blocco_ds, ls_note
datetime ldt_data_blocco_ds
long ll_tot, ll_index, ll_count

ls_sql = "select	 	cod_azienda,"+&
						"cod_prodotto_figlio,"+&
					 	"cod_formula,"+&
					 	"nota_operativa,"+&
					 	"cod_prodotto_padre,"+&
						"cod_versione_padre,"+&
					  	"flag_blocco,"+&
					  	"data_blocco "+&
			"from tab_formule_legami "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"cod_prodotto_finito='"+fs_cod_prodotto_finito_origine+"' and "+&
					"cod_versione='"+fs_cod_versione_finito_origine+"' and "+&
					"cod_prodotto_padre='"+fs_cod_prodotto_figlio+"' and "+&
					"cod_versione_padre='"+fs_cod_versione_partenza+"' "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, sqlca, fs_errore)
if ll_tot<0 then
	//in fs_errore il messaggio
	return -1
end if

for ll_index =  1 to ll_tot
	ls_cod_prodotto_figlio_ds = lds_data.getitemstring(ll_index, "cod_prodotto_figlio")
	ls_cod_formula_ds = lds_data.getitemstring(ll_index, "cod_formula")
	ls_nota_operativa_ds = lds_data.getitemstring(ll_index, "nota_operativa")
	ls_cod_prodotto_padre_ds = lds_data.getitemstring(ll_index, "cod_prodotto_padre")
	ls_cod_versione_padre_ds = lds_data.getitemstring(ll_index, "cod_versione_padre")
	
	//ls_flag_blocco_ds = lds_data.getitemstring(ll_index, "flag_blocco")
	//ldt_data_blocco_ds = lds_data.getitemdatetime(ll_index, "data_blocco")
	ls_flag_blocco_ds = "N"
	setnull(ldt_data_blocco_ds)
	
	ls_note = "Duplicato da " + ls_cod_prodotto_padre_ds + " Ver " + ls_cod_versione_padre_ds
	
	select count(*)
	into :ll_count
	from tab_formule_legami
	where		cod_azienda=:is_cod_azienda_gibus and
				cod_prodotto_finito=:fs_cod_prodotto_finito_destinazione and
				cod_versione=:fs_cod_versione_finito_destinazione and
				cod_prodotto_padre=:fs_cod_prodotto_figlio and
				cod_versione_padre=:fs_cod_versione_destinazione and
				cod_prodotto_figlio=:ls_cod_prodotto_figlio_ds and
				cod_versione_figlio=:fs_cod_versione_destinazione and
				cod_formula=:ls_cod_formula_ds
	using itran_gibus;
	
	if ll_count>0 then
		//già esiste, salta l'inserimento
		continue
	else
		//il record non esiste, inseriscilo
	end if
				
	insert into tab_formule_legami  
		(	cod_azienda,   
			cod_prodotto_finito,   
			cod_versione,   
			cod_prodotto_padre,   
			cod_versione_padre,   
			cod_prodotto_figlio,   
			cod_versione_figlio,   
			cod_formula,   
			nota_operativa,   
			note,   
			flag_blocco,   
			data_blocco  	)
	values (	  :is_cod_azienda_gibus,   
				  :fs_cod_prodotto_finito_destinazione,   
				  :fs_cod_versione_finito_destinazione,   
				  :fs_cod_prodotto_figlio,   
				  :fs_cod_versione_destinazione,   
				  :ls_cod_prodotto_figlio_ds,   
				  :fs_cod_versione_destinazione,   
				  :ls_cod_formula_ds,   
				  :ls_nota_operativa_ds,   
				  :ls_note,   
				  :ls_flag_blocco_ds,   
				  :ldt_data_blocco_ds )
	using itran_gibus;
	
	if itran_gibus.sqlcode < 0 then
		fs_errore =  itran_gibus.sqlerrtext
		return -1
	end if
	
next

//se arrivi fin qui torna 1
return 1

	
			
end function

public function integer uof_duplica_gruppi_varianti (string fs_cod_prodotto_destinazione, string fs_cod_versione_destinazione, string fs_cod_prodotto_partenza, string fs_cod_versione_partenza, long fl_num_sequenza, string fs_cod_prodotto_figlio, string fs_cod_versione_figlio, ref string fs_errore);datastore lds_data
string ls_sql, ls_cod_prodotto_figlio_ds, ls_cod_gruppo_variante_ds
long ll_tot, ll_index, ll_num_sequenza_ds, ll_count

ls_sql = "select	 	cod_azienda,"+&
						"cod_gruppo_variante,"+&
					 	"num_sequenza,"+&
					 	"cod_prodotto_figlio "+&
			"from distinta_gruppi_varianti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"cod_prodotto_padre='"+fs_cod_prodotto_partenza+"' and "+&
					"cod_versione='"+fs_cod_versione_partenza+"' and "+&
					"num_sequenza="+string(fl_num_sequenza)+" and "+&
					"cod_prodotto_figlio='"+fs_cod_prodotto_figlio+"' and "+&
					"cod_versione_figlio='"+fs_cod_versione_figlio+"' "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, sqlca, fs_errore)
if ll_tot<0 then
	//in fs_errore il messaggio
	return -1
end if

for ll_index =  1 to ll_tot
	ls_cod_gruppo_variante_ds = lds_data.getitemstring(ll_index, "cod_gruppo_variante")
	ll_num_sequenza_ds = lds_data.getitemnumber(ll_index, "num_sequenza")
	ls_cod_prodotto_figlio_ds = lds_data.getitemstring(ll_index, "cod_prodotto_figlio")
	
	select count(*)
	into :ll_count
	from distinta_gruppi_varianti
	where		cod_azienda=:is_cod_azienda_gibus and
				cod_gruppo_variante=:ls_cod_gruppo_variante_ds and
				cod_prodotto_padre=:fs_cod_prodotto_destinazione and
				num_sequenza=:ll_num_sequenza_ds and
				cod_prodotto_figlio=:ls_cod_prodotto_figlio_ds and
				cod_versione=:fs_cod_versione_destinazione and
				cod_versione_figlio=:fs_cod_versione_destinazione
	using itran_gibus;
	
	if ll_count>0 then
		//già esiste, salta l'inserimento
		continue
	else
		//il record non esiste, inseriscilo
	end if
	
	insert into distinta_gruppi_varianti
				( 	cod_azienda,   
					cod_gruppo_variante,   
					cod_prodotto_padre,   
					num_sequenza,   
					cod_prodotto_figlio,   
					cod_versione,   
					cod_versione_figlio  )
	values	(	:is_cod_azienda_gibus,   
					:ls_cod_gruppo_variante_ds,   
					:fs_cod_prodotto_destinazione,   
					:ll_num_sequenza_ds,   
					:ls_cod_prodotto_figlio_ds,   
					:fs_cod_versione_destinazione,   
					:fs_cod_versione_destinazione)
	using itran_gibus;
	
	if itran_gibus.sqlcode < 0 then
		fs_errore =  itran_gibus.sqlerrtext
		return -1
	end if
	
next

//se arrivi fin qui torna 1
return 1
end function

public function integer uof_duplica_distinta_ricors (string fs_cod_prodotto_finito_origine, string fs_cod_versione_finito_origine, string fs_cod_prodotto_finito_destinazione, string fs_cod_versione_finito_destinazione, string fs_cod_prodotto_partenza, string fs_cod_prodotto_destinazione, string fs_cod_versione_partenza, string fs_cod_versione_destinazione, long fl_num_livello_cor, ref string fs_errore);string  ls_test_prodotto_f, ls_flag_materia_prima,ls_cod_prodotto_figlio,ls_cod_misura,ls_des_estesa,&
		  ls_formula_tempo,ls_cod_formula_quan_utilizzo,ls_flag_escludibile,ls_test_1, ls_cod_versione_figlio,ls_flag_ramo_descrittivo
long    ll_num_figli,ll_num_righe,ll_num_sequenza,ll_fase_ciclo,ll_lead_time
integer li_num_priorita,li_risposta
DEC{4}  ldd_quan_tecnica,ldd_quan_utilizzo,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t,ldd_coef_calcolo
datastore lds_righe_distinta

ll_num_figli = 1

//duplicazione formule legami --------------------------------------------------------
//la funzione verifica prima se già esiste
pcca.mdi_frame.setmicrohelp("Padre: " + fs_cod_prodotto_partenza + " - Figlio: " + ls_cod_prodotto_figlio + " .... DUPLICAZIONE FORMULE LEGAMI")

if uof_duplica_formule_legami(	fs_cod_prodotto_finito_destinazione, fs_cod_versione_finito_destinazione, fs_cod_prodotto_partenza, fs_cod_versione_partenza, &
										fs_cod_prodotto_finito_origine, fs_cod_versione_finito_origine, fs_cod_versione_partenza, fs_errore) < 0 then
	fs_errore = "Errore in inserimento formule legami nel ramo di distinta.~rPadre: " + fs_cod_prodotto_destinazione +&
																							" Vers.:" + fs_cod_versione_destinazione + &
																							" Figlio: "  + ls_cod_prodotto_figlio + &
																							" Vers.:" + fs_cod_versione_destinazione +  "~r~n" + fs_errore
	return -1
										
end if
	



lds_righe_distinta = Create DataStore
lds_righe_distinta.DataObject = "d_data_store_distinta_estesa"

//leggo dal database di provenienza: SQLCA
lds_righe_distinta.SetTransObject(sqlca)
ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto_partenza,fs_cod_versione_partenza)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	ls_cod_misura = lds_righe_distinta.getitemstring(ll_num_figli,"cod_misura")
	ldd_quan_tecnica = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_tecnica")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	ll_fase_ciclo = lds_righe_distinta.getitemnumber(ll_num_figli,"fase_ciclo")
	ldd_dim_x = lds_righe_distinta.getitemnumber(ll_num_figli,"dim_x")
	ldd_dim_y = lds_righe_distinta.getitemnumber(ll_num_figli,"dim_y")
	ldd_dim_z = lds_righe_distinta.getitemnumber(ll_num_figli,"dim_z")
	ldd_dim_t = lds_righe_distinta.getitemnumber(ll_num_figli,"dim_t")
	ldd_coef_calcolo = lds_righe_distinta.getitemnumber(ll_num_figli,"coef_calcolo")
	ls_des_estesa = lds_righe_distinta.getitemstring(ll_num_figli,"des_estesa")
	ls_formula_tempo = lds_righe_distinta.getitemstring(ll_num_figli,"formula_tempo")
	ls_cod_formula_quan_utilizzo = lds_righe_distinta.getitemstring(ll_num_figli,"cod_formula_quan_utilizzo")
	ls_flag_escludibile = lds_righe_distinta.getitemstring(ll_num_figli,"flag_escludibile")
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")
	ls_flag_ramo_descrittivo = lds_righe_distinta.getitemstring(ll_num_figli,"flag_ramo_descrittivo")
	ll_lead_time = lds_righe_distinta.getitemnumber(ll_num_figli,"lead_time")
	
	pcca.mdi_frame.setmicrohelp("Padre: " + fs_cod_prodotto_partenza + " - Figlio: " + ls_cod_prodotto_figlio + " .... DUPLICAZIONE RAMI DI DISTINTA")
	
	
	//leggo se presente in destinazione
	select 	cod_azienda
	into   	:ls_test_1
	from   	distinta
	where  cod_azienda = :is_cod_azienda_gibus and    
	       	cod_prodotto_padre = :fs_cod_prodotto_destinazione	and    
			 num_sequenza = :ll_num_sequenza	and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
			 cod_versione = :fs_cod_versione_destinazione	and	 
			 cod_versione_figlio = :fs_cod_versione_destinazione
	using itran_gibus;

	if itran_gibus.sqlcode < 0 then
		fs_errore = "Errore sul DB:" + itran_gibus.sqlerrtext
		return -1
	end if
	
	if itran_gibus.sqlcode = 100 then
		
		insert into distinta  
				( cod_azienda,   
				  cod_prodotto_padre,   
				  num_sequenza,   
				  cod_prodotto_figlio,   
				  cod_versione,
				  cod_versione_figlio,
				  cod_misura,   
				  quan_tecnica,   
				  quan_utilizzo,   
				  fase_ciclo,   
				  dim_x,   
				  dim_y,   
				  dim_z,   
				  dim_t,   
				  coef_calcolo,   
				  des_estesa,   
				  formula_tempo,   
				  cod_formula_quan_utilizzo,   
				  flag_escludibile,   
				  flag_materia_prima,
				  flag_ramo_descrittivo,
				  lead_time)  
		values (:is_cod_azienda_gibus,   
				  :fs_cod_prodotto_destinazione,   
				  :ll_num_sequenza,   
				  :ls_cod_prodotto_figlio,   
				  :fs_cod_versione_destinazione,
				  :fs_cod_versione_destinazione,
				  :ls_cod_misura,   
				  :ldd_quan_tecnica,   
				  :ldd_quan_utilizzo,   
				  :ll_fase_ciclo,   
				  :ldd_dim_x,   
				  :ldd_dim_y,   
				  :ldd_dim_z,   
				  :ldd_dim_t,   
				  :ldd_coef_calcolo,   
				  :ls_des_estesa,   
				  :ls_formula_tempo,   
				  :ls_cod_formula_quan_utilizzo,   
				  :ls_flag_escludibile,   
				  :ls_flag_materia_prima,
				  :ls_flag_ramo_descrittivo,
				  :ll_lead_time )
		using itran_gibus;
	
		if itran_gibus.sqlcode < 0 then
			fs_errore = "Errore in inserimento ramo di distinta.~rPadre: " + fs_cod_prodotto_destinazione + &
																				" Vers.:" + fs_cod_versione_destinazione + &
																				" Figlio: "  + ls_cod_prodotto_figlio + &
																				" Vers.:" + fs_cod_versione_destinazione +  "~r~n" + itran_gibus.sqlerrtext
			return -1
		end if
		
	else
		//già esiste
		
	end if

	//duplicazione fase lavorazione -------------------------------------------------
	pcca.mdi_frame.setmicrohelp("Padre: " + fs_cod_prodotto_partenza + " - Figlio: " + ls_cod_prodotto_figlio + " .... DUPLICAZIONE FASI")
	
	if uof_duplica_fasi_lavorazione(ls_cod_prodotto_figlio, ls_cod_versione_figlio, fs_cod_versione_destinazione, fs_errore) < 0 then
		fs_errore = "Errore in inserimento fase lavorazione nel ramo di distinta.~rPadre: " + fs_cod_prodotto_destinazione +&
																								" Vers.:" + fs_cod_versione_destinazione + &
																								" Figlio: "  + ls_cod_prodotto_figlio + &
																								" Vers.:" + fs_cod_versione_destinazione +  "~r~n" + fs_errore
		return -1
	end if
	
	//duplicazione varianti -------------------------------------------------
	//la funzione verifica prima se già esiste
	pcca.mdi_frame.setmicrohelp("Padre: " + fs_cod_prodotto_partenza + " - Figlio: " + ls_cod_prodotto_figlio + " .... DUPLICAZIONE VARIANTI")
	
	if uof_duplica_gruppi_varianti(	fs_cod_prodotto_destinazione, fs_cod_versione_destinazione, fs_cod_prodotto_partenza, &
											fs_cod_versione_partenza, ll_num_sequenza,ls_cod_prodotto_figlio, ls_cod_versione_figlio, fs_errore) < 0 then
		
		fs_errore = "Errore in inserimento gruppi varianti nel ramo di distinta.~rPadre: " + fs_cod_prodotto_destinazione +&
																								" Vers.:" + fs_cod_versione_destinazione + &
																								" Figlio: "  + ls_cod_prodotto_figlio + &
																								" Vers.:" + fs_cod_versione_destinazione +  "~r~n" + fs_errore
		return -1
	end if
		

//	//duplicazione formule legami --------------------------------------------------------
//	//la funzione verifica prima se già esiste
//	pcca.mdi_frame.setmicrohelp("Padre: " + fs_cod_prodotto_partenza + " - Figlio: " + ls_cod_prodotto_figlio + " .... DUPLICAZIONE FORMULE LEGAMI")
//	
//	if uof_duplica_formule_legami(	fs_cod_prodotto_finito_origine, fs_cod_versione_finito_origine, fs_cod_prodotto_finito_destinazione, fs_cod_versione_destinazione, &
//											fs_cod_prodotto_finito_origine, fs_cod_versione_finito_origine, fs_cod_versione_partenza, fs_errore) < 0 then
//		fs_errore = "Errore in inserimento formule legami nel ramo di distinta.~rPadre: " + fs_cod_prodotto_destinazione +&
//																								" Vers.:" + fs_cod_versione_destinazione + &
//																								" Figlio: "  + ls_cod_prodotto_figlio + &
//																								" Vers.:" + fs_cod_versione_destinazione +  "~r~n" + fs_errore
//		return -1
//											
//	end if
//	
//	
	//duplicazione varianti -------------------------------------------------
	//la funzione verifica prima se già esiste
	pcca.mdi_frame.setmicrohelp("Padre: " + fs_cod_prodotto_partenza + " - Figlio: " + ls_cod_prodotto_figlio + " .... DUPLICAZIONE TABELLE DI TAGLIO")
	
	if uof_duplica_tabelle_taglio(	fs_cod_prodotto_destinazione, fs_cod_versione_destinazione, fs_cod_prodotto_partenza, &
											fs_cod_versione_partenza, ll_num_sequenza,ls_cod_prodotto_figlio, ls_cod_versione_figlio, fs_errore) < 0 then
		
		fs_errore = "Errore in inserimento tabelle di taglio nel ramo di distinta.~rPadre: " + fs_cod_prodotto_destinazione +&
																								" Vers.:" + fs_cod_versione_destinazione + &
																								" Figlio: "  + ls_cod_prodotto_figlio + &
																								" Vers.:" + fs_cod_versione_destinazione +  "~r~n" + fs_errore
		return -1
	end if
		


	//vedo se devo proseguire lungo il ramo
	select 	cod_prodotto_figlio 
	into   	:ls_test_prodotto_f
	from   	distinta
	where  cod_azienda = :s_cs_xx.cod_azienda 	and	 
	      		cod_prodotto_padre = :ls_cod_prodotto_figlio	and    
			cod_versione = :ls_cod_versione_figlio
	using sqlca;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		continue
	else
		li_risposta = uof_duplica_distinta_ricors(		fs_cod_prodotto_finito_origine, & 
																fs_cod_versione_finito_origine, &
																fs_cod_prodotto_finito_destinazione, &
																fs_cod_versione_finito_destinazione , &
																ls_cod_prodotto_figlio,&
																ls_cod_prodotto_figlio,&
																ls_cod_versione_figlio,&
																fs_cod_versione_destinazione, &
																fl_num_livello_cor + 1, &
																fs_errore)
		if li_risposta < 0 then
			//in fs_errore il messaggio
			//fs_errore = ls_errore
			
			return -1
		end if
	end if
next

destroy(lds_righe_distinta)

return 1
end function

public function integer uof_duplica_fasi_lavorazione (string fs_cod_prodotto_figlio, string fs_cod_versione_figlio, string fs_cod_versione_destinazione, ref string fs_errore);datastore	lds_data
string			ls_sql
long			ll_tot, ll_index, ll_count

string			ls_cod_reparto_ds, ls_cod_lavorazione_ds, ls_cod_cat_attrezzature_ds, ls_des_estesa_prodotto_ds, ls_cod_centro_costo_ds, ls_cod_cat_attrezzature_2_ds, &
				ls_cod_prodotto_sfrido_ds, ls_flag_esterna_ds, ls_flag_contigua_ds, ls_flag_uso_ds, ls_flag_stampa_fase_ds
				
decimal		ld_num_priorita_ds, ld_quan_prodotta_ds, ld_quan_non_conforme_ds, ld_tempo_attrezzaggio_ds, ld_tempo_attrezzaggio_commessa_ds, &
				ld_tempo_lavorazione_ds, ld_tempo_risorsa_umana_ds, ld_quan_sfrido_ds, ld_costo_orario_ds,ld_costo_pezzo_ds, ld_tempo_movimentazione_ds

ls_sql = &
"select 	cod_azienda,"+&
			"cod_prodotto,"+&
			"cod_reparto,"+&
			"cod_lavorazione,"+&
			"cod_versione,"+&
			"cod_cat_attrezzature,"+&
			"des_estesa_prodotto,"+&
			"num_priorita,"+&
			"cod_centro_costo,"+&
			"quan_prodotta,"+&
			"quan_non_conforme,"+&
			"tempo_attrezzaggio,"+&
			"tempo_attrezzaggio_commessa,"+&
			"tempo_lavorazione,"+&
			"cod_cat_attrezzature_2,"+&
			"tempo_risorsa_umana,"+&
			"cod_prodotto_sfrido,"+&
			"quan_sfrido,"+&
			"flag_esterna,"+&
			"flag_contigua,"+&
			"costo_orario,"+&
			"costo_pezzo,"+&
			"flag_uso,"+&
			"tempo_movimentazione,"+&
			"flag_stampa_fase "+&
"from tes_fasi_lavorazione "+&
"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"cod_prodotto='"+fs_cod_prodotto_figlio+"' and "+&
					"cod_versione='"+fs_cod_versione_figlio+"'  "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, sqlca, fs_errore)
if ll_tot<0 then
	//in fs_errore il messaggio
	return -1
end if

for ll_index =  1 to ll_tot
	
	select count(*)
	into :ll_count
	from tes_fasi_lavorazione
	where		cod_azienda=:is_cod_azienda_gibus and
				cod_prodotto=:fs_cod_prodotto_figlio and
				cod_versione=:fs_cod_versione_destinazione
	using itran_gibus;
	
	if ll_count>0 then
		//già esiste, salta l'inserimento
		continue
	else
		//il record non esiste, inseriscilo
	end if
	
	ls_cod_reparto_ds 							= lds_data.getitemstring(ll_index, "cod_reparto")
	ls_cod_lavorazione_ds 						= lds_data.getitemstring(ll_index, "cod_lavorazione")
	ls_cod_cat_attrezzature_ds 					= lds_data.getitemstring(ll_index, "cod_cat_attrezzature")
	ls_des_estesa_prodotto_ds 					= lds_data.getitemstring(ll_index, "des_estesa_prodotto")
	ld_num_priorita_ds 							= lds_data.getitemdecimal(ll_index, "num_priorita")
	ls_cod_centro_costo_ds 						= lds_data.getitemstring(ll_index, "cod_centro_costo")
	ld_quan_prodotta_ds 							= lds_data.getitemdecimal(ll_index, "quan_prodotta")
	ld_quan_non_conforme_ds 					= lds_data.getitemdecimal(ll_index, "quan_non_conforme")
	ld_tempo_attrezzaggio_ds 					= lds_data.getitemdecimal(ll_index, "tempo_attrezzaggio")
	ld_tempo_attrezzaggio_commessa_ds 	= lds_data.getitemdecimal(ll_index, "tempo_attrezzaggio_commessa")
	ld_tempo_lavorazione_ds 					= lds_data.getitemdecimal(ll_index, "tempo_lavorazione")
	ls_cod_cat_attrezzature_2_ds				= lds_data.getitemstring(ll_index, "cod_cat_attrezzature_2")
	ld_tempo_risorsa_umana_ds 				= lds_data.getitemdecimal(ll_index, "tempo_risorsa_umana")
	ls_cod_prodotto_sfrido_ds 					= lds_data.getitemstring(ll_index, "cod_prodotto_sfrido")
	ld_quan_sfrido_ds 							= lds_data.getitemdecimal(ll_index, "quan_sfrido")
	ls_flag_esterna_ds 							= lds_data.getitemstring(ll_index, "flag_esterna")
	ls_flag_contigua_ds 							= lds_data.getitemstring(ll_index, "flag_contigua")
	ld_costo_orario_ds 							= lds_data.getitemdecimal(ll_index, "costo_orario")
	ld_costo_pezzo_ds 							= lds_data.getitemdecimal(ll_index, "costo_pezzo")
	ls_flag_uso_ds 									= lds_data.getitemstring(ll_index, "flag_uso")
	ld_tempo_movimentazione_ds 				= lds_data.getitemdecimal(ll_index, "tempo_movimentazione")
	ls_flag_stampa_fase_ds 						= lds_data.getitemstring(ll_index, "flag_stampa_fase")

	insert into tes_fasi_lavorazione  
		(	cod_azienda,   
			cod_prodotto,   
			cod_reparto,   
			cod_lavorazione,
			cod_versione,
			cod_cat_attrezzature,   
			des_estesa_prodotto,   
			num_priorita,   
			cod_centro_costo,   
			quan_prodotta,   
			quan_non_conforme,   
			tempo_attrezzaggio,   
			tempo_attrezzaggio_commessa,   
			tempo_lavorazione,   
			cod_cat_attrezzature_2,   
			tempo_risorsa_umana,   
			cod_prodotto_sfrido,   
			quan_sfrido,   
			flag_esterna,   
			flag_contigua,   
			costo_orario,   
			costo_pezzo,   
			flag_uso,   
			tempo_movimentazione,   
			flag_stampa_fase  	)
	values (	     :is_cod_azienda_gibus,   
					:fs_cod_prodotto_figlio,   
					:ls_cod_reparto_ds,
					:ls_cod_lavorazione_ds,
					:fs_cod_versione_destinazione,
					:ls_cod_cat_attrezzature_ds,
					:ls_des_estesa_prodotto_ds,
					:ld_num_priorita_ds,
					:ls_cod_centro_costo_ds,
					:ld_quan_prodotta_ds,
					:ld_quan_non_conforme_ds,
					:ld_tempo_attrezzaggio_ds,
					:ld_tempo_attrezzaggio_commessa_ds,
					:ld_tempo_lavorazione_ds,
					:ls_cod_cat_attrezzature_2_ds,
					:ld_tempo_risorsa_umana_ds,
					:ls_cod_prodotto_sfrido_ds,
					:ld_quan_sfrido_ds,
					:ls_flag_esterna_ds,
					:ls_flag_contigua_ds,
					:ld_costo_orario_ds,
					:ld_costo_pezzo_ds,
					:ls_flag_uso_ds,
					:ld_tempo_movimentazione_ds,
					:ls_flag_stampa_fase_ds )
	using itran_gibus;
	
	if itran_gibus.sqlcode < 0 then
		fs_errore =  itran_gibus.sqlerrtext
		return -1
	end if
	
next

//se arrivi fin qui torna 1
return 1

	
			
end function

public function integer uof_duplica_distinta (string fs_cod_prodotto_partenza, string fs_cod_versione_partenza, string fs_cod_prodotto_destinazione, string fs_cod_versione_destinazione, string fs_des_versione, ref datawindow fdw_datawindow, ref string fs_errore);string ls_test_padre, ls_cod_versione_esistente, ls_test, ls_check_duplica_tutto
integer li_risposta
long ll_count
boolean lb_flag_ok
datetime ldt_oggi

//questa è la funzione principale
//creo la transazione e mi connetto
//alla fine faccio commit e poi distruggo la transazione
//se capita un errore durante questa elaborazione faccio anche rollback (funzione di disconnessione)

//if ib_apri_win_log then open(w_stato_elaborazione)
idw_dw_log = fdw_datawindow

if uof_connetti(fs_errore)<0 then
	//in fs_errore il messaggio: transazione già distrutta
	//if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	return -1
end if

ldt_oggi = datetime(today(),00:00:00)

if isnull(fs_cod_prodotto_partenza) or fs_cod_prodotto_partenza="" then
	fs_errore = "Non è stato indicato il codice del prodotto di partenza!"
	uof_disconnetti()
	return -1
end if

select cod_azienda
into   :ls_test_padre
from   anag_prodotti
where  cod_azienda  = :s_cs_xx.cod_azienda and
       cod_prodotto = :fs_cod_prodotto_partenza
using sqlca;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore sul DB (check prodotto partenza):" + sqlca.sqlerrtext
	uof_disconnetti()
	return -1
end if

if sqlca.sqlcode = 100 then
	fs_errore = "Il codice del prodotto di partenza '"+fs_cod_prodotto_partenza+"' non esiste in anagrafica prodotti!"
	uof_disconnetti()
	return -1
end if


if isnull(fs_cod_prodotto_destinazione) or fs_cod_prodotto_destinazione="" then
	fs_errore = "Non è stato indicato il codice del prodotto di destinazione!"
	uof_disconnetti()
	return -1
end if

select cod_azienda
into :ls_test_padre
from  anag_prodotti
where		cod_azienda=:is_cod_azienda_gibus and
			cod_prodotto=:fs_cod_prodotto_destinazione
using itran_gibus;

if itran_gibus.sqlcode < 0 then
	fs_errore = "Errore sul DB (check prodotto destinazione):" + itran_gibus.sqlerrtext
	uof_disconnetti()
	return -1
end if

if itran_gibus.sqlcode = 100 then
	fs_errore = "Il codice del prodotto di destinazione '"+fs_cod_prodotto_destinazione+"' non esiste in anagrafica prodotti!"
	uof_disconnetti()
	return -1
end if

if isnull(fs_cod_versione_destinazione) or fs_cod_versione_destinazione="" then
	fs_errore = "Non è stata indicata la versione di destinazione!"
	uof_disconnetti()
	return -1
end if

if isnull(fs_cod_versione_partenza) or fs_cod_versione_partenza="" then
	fs_errore = "Non è stata indicata la versione di partenza!"
	uof_disconnetti()
	return -1
end if

//***************	ROUTINE DI CONTROLLO DI CONGRUENZA VERSIONI
setnull(ls_test_padre)

//select cod_azienda
//into   :ls_test_padre
select count(*)
into :ll_count
from   distinta_padri
where  cod_azienda=:is_cod_azienda_gibus and
		  cod_prodotto=:fs_cod_prodotto_destinazione
using itran_gibus;     //verifico se il prodotto_destinazione è già prodotto finito

if itran_gibus.sqlcode < 0 then
	fs_errore = "Errore sul DB (se prodotto destinazione è PF):" + itran_gibus.sqlerrtext
	uof_disconnetti()
	return -1
end if

if isnull(ll_count) then ll_count = 0

//if isnull(ls_test_padre) then
if ll_count=0 then
	 //se il prodotto non è un PF allora verifico se è figlio in distinta
	 
//	declare righe_test cursor for
//	select cod_prodotto_figlio
//	from   distinta
//	where  	cod_azienda=:is_cod_azienda_gibus and
//			    cod_prodotto_figlio=:fs_cod_prodotto_destinazione
//	using itran_gibus;
//	
//	open righe_test;
//	
//	if itran_gibus.sqlcode < 0 then
//		fs_errore = "Errore apertura cursore righe_test:" + itran_gibus.sqlerrtext
//		uof_disconnetti()
//		return -1
//	end if
//	
//	fetch righe_test
//	into  :ls_test;
//	
//	if itran_gibus.sqlcode < 0 then
//		fs_errore = "Errore fetch cursore righe_test: "+itran_gibus.sqlerrtext
//		close righe_test;
//		uof_disconnetti()
//		return -1
//	end if
//	
//	if itran_gibus.sqlcode <> 100 then 	// se è figlio allora verifico che la versione_destinazione non sia una delle 
//		close righe_test;			  			// 'n' versione già esistenti
//		lb_flag_ok = false
//	
//		declare righe_db_1 cursor for
//		select  cod_versione_figlio
//		from    distinta 
//		where   cod_azienda=:is_cod_azienda_gibus
//		and     cod_prodotto_figlio =:fs_cod_prodotto_destinazione
//		using itran_gibus;
//		
//		open righe_db_1;
//		
//		if itran_gibus.sqlcode < 0 then
//			fs_errore = "Errore apertura cursore righe_db_1:" + itran_gibus.sqlerrtext
//			uof_disconnetti()
//			return -1
//		end if
//		
//		do while 1 = 1
//			fetch righe_db_1
//			into  :ls_cod_versione_esistente;
//			
//			if (itran_gibus.sqlcode = 100) then exit
//			if itran_gibus.sqlcode<0 then
//				fs_errore = "Errore fetch cursore righe_db_1: " + itran_gibus.sqlerrtext
//				close righe_db_1;
//				uof_disconnetti()
//				return -1
//			end if
//			
//			if ls_cod_versione_esistente = fs_cod_versione_destinazione then lb_flag_ok = true
//		loop
//	
//		close righe_db_1;
//	
//		if lb_flag_ok = true then	//se la versione_destinazione è uguale a quelle esistenti allora costringo l'utente a impostarne un'altra
//			fs_errore = "La versione scelta è già esistente. Reinserire una nuova versione diversa da quelle esistenti."
//			uof_disconnetti()
//			return -1
//		end if
//	
//	else
//		close righe_test;
//	end if
	//close righe_test;
	
else //	se il prodotto destinazione è un PF allora verifico che la versione di destinazione non sia già utilizzata

	setnull(ls_test)
	
	select cod_azienda
	into   :ls_test
	from   distinta_padri
	where  cod_azienda=:is_cod_azienda_gibus
	and    cod_prodotto=:fs_cod_prodotto_destinazione
	and    cod_versione=:fs_cod_versione_destinazione
	using itran_gibus;
		
	if itran_gibus.sqlcode<0 then
		fs_errore = "Errore nel DB: " + itran_gibus.sqlerrtext
		uof_disconnetti()
		return -1
	end if
		
	if itran_gibus.sqlcode = 0 then
		fs_errore = "La versione di destinazione è già esistente. Inserire una codice versione non utilizzato"
		uof_disconnetti()
		return -1
	end if
	
end if

//*************** FINE ROUTINE DI CONTROLLO

insert into distinta_padri  
			(cod_azienda,   
			cod_prodotto,   
			cod_versione,   
			creato_da,   
			approvato_da,   
			des_versione,   
			data_creazione,   
			data_approvazione,   
			flag_predefinita,   
			flag_blocco,   
			data_blocco,   
			flag_esplodi_in_doc )  
	values	(:is_cod_azienda_gibus,
				:fs_cod_prodotto_destinazione,
				:fs_cod_versione_destinazione,
				:s_cs_xx.cod_utente,
				null, 
				:fs_des_versione,   
				:ldt_oggi,   
				null,   
				'N',   
				'N',   
				null,   
				'N' ) 
using itran_gibus;

if itran_gibus.sqlcode < 0 then
	fs_errore = "Errore durante la creazione del PADRE in tabella distinta_padri~r~n:" + itran_gibus.sqlerrtext
	uof_disconnetti()
	return -1
end if
 
li_risposta = uof_duplica_distinta_ricors(		fs_cod_prodotto_partenza, &
														fs_cod_versione_partenza, &
														fs_cod_prodotto_destinazione, & 
														fs_cod_versione_destinazione, & 
														fs_cod_prodotto_partenza, & 
														fs_cod_prodotto_destinazione, & 
														fs_cod_versione_partenza, & 
														fs_cod_versione_destinazione, & 
														1, & 
														fs_errore)

if li_risposta < 0 then
	//in fs_errore il messaggio
	uof_disconnetti()
	return -1
end if

//se arrivi fin qui fai commit
commit using itran_gibus;
uof_disconnetti()			//anche se questa fa pure rollback, ho fatto comunque commit prima

pcca.mdi_frame.setmicrohelp("Padre: " + fs_cod_prodotto_partenza + " - Versione: " + fs_cod_versione_partenza + " .... duplicazione eseguita ! ")

return 1

end function

public function integer uof_duplica_tabelle_taglio (string fs_cod_prodotto_destinazione, string fs_cod_versione_destinazione, string fs_cod_prodotto_partenza, string fs_cod_versione_partenza, long fl_num_sequenza, string fs_cod_prodotto_figlio, string fs_cod_versione_figlio, ref string fs_errore);string	ls_flag_distinte_taglio, ls_sql,ls_cod_prodotto_figlio, ls_des_distinta_taglio,ls_um_quantita, ls_formula_quantita, ls_um_misura, 	ls_formula_misura, &
		ls_tutti_comandi, ls_cod_comando
long ll_tot, ll_i, ll_num_sequenza,ll_progressivo, ll_cont
datastore lds_data, lds_data1
		
// duplico anche le distinte di taglio se presenti
setnull(ls_flag_distinte_taglio)

select stringa
into 	:ls_flag_distinte_taglio
from	parametri
where cod_parametro = 'DT';

if not isnull(ls_flag_distinte_taglio) and ls_flag_distinte_taglio = "S" then
	
	select 	count(*)
	into 		:ll_cont
	from 		tab_distinte_taglio
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto_padre = :fs_cod_prodotto_figlio and
				cod_versione = :fs_cod_versione_partenza;
				
	if ll_cont = 0 or isnull(ll_cont) then
		return 1
	end if

	
	
	ls_sql = 	" SELECT num_sequenza , " + &  
				" cod_prodotto_figlio,  " + &
				" progressivo,   " + &
				" des_distinta_taglio, " + &   
				" um_quantita,   " + &
				" formula_quantita,   " + &
				" um_misura,   " + &
				" formula_misura,   " + &
				" tutti_comandi  " + &
				" FROM tab_distinte_taglio " + &
				" WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto_padre = '" + fs_cod_prodotto_figlio + "' and cod_versione = '" + fs_cod_versione_partenza + "' "
	
	
	ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, sqlca, fs_errore)
	if ll_tot<0 then
		//in fs_errore il messaggio
		return -1
	end if

	for ll_i =  1 to ll_tot
		
		ll_num_sequenza = lds_data.getitemnumber(ll_i, 1)
		ls_cod_prodotto_figlio = lds_data.getitemstring(ll_i, 2)
		ll_progressivo = lds_data.getitemnumber(ll_i, 3)
		ls_des_distinta_taglio = lds_data.getitemstring(ll_i, 4)
		ls_um_quantita = lds_data.getitemstring(ll_i, 5)
		ls_formula_quantita = lds_data.getitemstring(ll_i, 6)
		ls_um_misura = lds_data.getitemstring(ll_i, 7)
		ls_formula_misura = lds_data.getitemstring(ll_i, 8)
		ls_tutti_comandi = lds_data.getitemstring(ll_i, 9)
		
	
		INSERT INTO tab_distinte_taglio  
		( cod_azienda,   
		  cod_prodotto_padre,   
		  num_sequenza,   
		  cod_prodotto_figlio,   
		  cod_versione,   
		  cod_versione_figlio,   
		  progressivo,   
		  des_distinta_taglio,   
		  um_quantita,   
		  formula_quantita,   
		  um_misura,   
		  formula_misura,   
		  tutti_comandi )  
	 	VALUES (
		 	:s_cs_xx.cod_azienda,   
			:fs_cod_prodotto_figlio,   
			:ll_num_sequenza,   
			:ls_cod_prodotto_figlio,   
			:fs_cod_versione_destinazione,   
			:fs_cod_versione_destinazione,   
			:ll_progressivo,   
			:ls_des_distinta_taglio,   
			:ls_um_quantita,   
			:ls_formula_quantita,   
			:ls_um_misura,   
			:ls_formula_misura,   
			:ls_tutti_comandi  )
			USING itran_gibus;
			
		if itran_gibus.sqlcode < 0 then
			fs_errore = "Errore in duplicazione tabelle di taglio.~r~n"+itran_gibus.sqlerrtext
			rollback using itran_gibus ;
			return -1
		end if
	
	next
	
	
	ls_sql = " SELECT num_sequenza, cod_prodotto_figlio, progressivo, cod_comando FROM distinte_taglio_comandi " + &
			   " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' AND cod_prodotto_padre = '" + fs_cod_prodotto_figlio + "' AND cod_versione = '" + fs_cod_versione_partenza + "'"
	
	ll_tot = guo_functions.uof_crea_datastore(lds_data1, ls_sql, sqlca, fs_errore)
	if ll_tot<0 then
		//in fs_errore il messaggio
		return -1
	end if
	
	for ll_i = 1 to ll_tot
		
		ll_num_sequenza = lds_data1.getitemnumber(ll_i, 1)
		ls_cod_prodotto_figlio = lds_data1.getitemstring(ll_i, 2)
		ll_progressivo = lds_data1.getitemnumber(ll_i, 3)
		ls_cod_comando = lds_data1.getitemstring(ll_i, 4)
		
	
		INSERT INTO distinte_taglio_comandi
			( 	cod_azienda,   
				cod_prodotto_padre,   
				num_sequenza,   
				cod_prodotto_figlio,   
				cod_versione,   
				cod_versione_figlio,   
				progressivo,   
				cod_comando )  
		VALUES (
				:s_cs_xx.cod_azienda,
				:fs_cod_prodotto_figlio,
				:ll_num_sequenza,
				:ls_cod_prodotto_figlio,
				:fs_cod_versione_destinazione,   
				:fs_cod_versione_destinazione,   
				:ll_progressivo,
				:ls_cod_comando)
		using itran_gibus ;
		 
		if itran_gibus.sqlcode < 0 then
			fs_errore = "Errore in duplicazione tabelle di taglio dei comandi.~r~n"+itran_gibus.sqlerrtext
			rollback using itran_gibus;
			return -1
		end if
	
	next
	
end if


//se arrivi fin qui torna 1
return 1
end function

on uo_dup_diba_gibus.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_dup_diba_gibus.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;
is_path_log = guo_functions.uof_get_user_documents_folder( )

if is_path_log="" or isnull(is_path_log) then
	ib_log = false
else
	//aggiungo il nome del file al percorso
	is_path_log += "Dup_Dist_"+string(today(),"ddmmyy")+"_"+string(now(),"hhmmss")+".txt"
	ib_log = true
end if
end event

