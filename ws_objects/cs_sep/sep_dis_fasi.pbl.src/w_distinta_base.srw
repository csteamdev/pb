﻿$PBExportHeader$w_distinta_base.srw
$PBExportComments$Finestra per la gestione Distinta Base
forward
global type w_distinta_base from w_cs_xx_principale
end type
type st_tipo from statictext within w_distinta_base
end type
type st_13 from statictext within w_distinta_base
end type
type tv_db from treeview within w_distinta_base
end type
type cb_distinte_taglio from commandbutton within w_distinta_base
end type
type cb_comprimi from commandbutton within w_distinta_base
end type
type cb_espandi from commandbutton within w_distinta_base
end type
type dw_folder from u_folder within w_distinta_base
end type
type dw_distinta_base_padre from uo_cs_xx_dw within w_distinta_base
end type
type em_1 from editmask within w_distinta_base
end type
type dw_ricerca from u_dw_search within w_distinta_base
end type
type dw_distinta_base_quantita from u_dw_search within w_distinta_base
end type
type dw_distinta_base_chiavi_prodotti from uo_cs_xx_dw within w_distinta_base
end type
type dw_distinta_gruppi_varianti from uo_cs_xx_dw within w_distinta_base
end type
type dw_mat_prime_automatiche from uo_std_dw within w_distinta_base
end type
type dw_prodotti_finiti from uo_cs_xx_dw within w_distinta_base
end type
type dw_distinta_det from uo_cs_xx_dw within w_distinta_base
end type
type dw_fasi_lavorazione from uo_cs_xx_dw within w_distinta_base
end type
end forward

global type w_distinta_base from w_cs_xx_principale
integer width = 4603
integer height = 3196
string title = "Distinta Base"
event ue_seleziona_tab ( )
st_tipo st_tipo
st_13 st_13
tv_db tv_db
cb_distinte_taglio cb_distinte_taglio
cb_comprimi cb_comprimi
cb_espandi cb_espandi
dw_folder dw_folder
dw_distinta_base_padre dw_distinta_base_padre
em_1 em_1
dw_ricerca dw_ricerca
dw_distinta_base_quantita dw_distinta_base_quantita
dw_distinta_base_chiavi_prodotti dw_distinta_base_chiavi_prodotti
dw_distinta_gruppi_varianti dw_distinta_gruppi_varianti
dw_mat_prime_automatiche dw_mat_prime_automatiche
dw_prodotti_finiti dw_prodotti_finiti
dw_distinta_det dw_distinta_det
dw_fasi_lavorazione dw_fasi_lavorazione
end type
global w_distinta_base w_distinta_base

type variables
long il_handle, il_cicli, il_modifica, il_ganne
string is_cod_versione, is_cod_prodotto_finito
s_chiave_distinta is_chiave_distinta

boolean ib_trovato, ib_configuratore


end variables

forward prototypes
public function integer wf_inizio ()
public function integer wf_visual_testo_formula (string fs_cod_formula)
public function integer wf_trova_prodotto (string fs_cod_prodotto_padre, string fs_cod_prodotto_inserito, string fs_cod_versione, ref string fs_messaggio, ref string fs_errore)
public function integer wf_trova (string fs_cod_prodotto, long al_handle)
public function integer wf_espandi (long al_handle, long al_livello, long al_cicli)
public function integer wf_pre_inizio ()
public function integer wf_comprimi (long al_handle)
public function integer wf_imposta_tv (string fs_cod_prodotto, string fs_cod_versione, integer fi_num_livello_cor, long fl_handle, ref string fs_errore)
public function integer wf_get_versione (string as_cod_prodotto_figlio, ref string as_cod_versione_figlio, ref string as_errore)
public subroutine wf_visualizza_distinta ()
end prototypes

event ue_seleziona_tab();dw_folder.fu_SelectTab(1)
end event

public function integer wf_inizio ();string ls_errore,ls_des_prodotto
long ll_risposta, ll_cont_fasi
treeviewitem tvi_campo


//st_cod_prodotto.text = is_cod_prodotto_finito
//st_cod_versione.text = is_cod_versione

s_chiave_distinta l_chiave_distinta
l_chiave_distinta.cod_prodotto_padre = ""
l_chiave_distinta.cod_prodotto_figlio = is_cod_prodotto_finito
l_chiave_distinta.cod_versione_padre = ""
l_chiave_distinta.cod_versione_figlio = is_cod_versione
tv_db.setredraw(false)
tv_db.deleteitem(0)

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio;
		 
ls_des_prodotto=trim(ls_des_prodotto)

tvi_campo.itemhandle = 15
tvi_campo.data = l_chiave_distinta
tvi_campo.label = l_chiave_distinta.cod_prodotto_figlio + "," + ls_des_prodotto
tvi_campo.pictureindex = 15
tvi_campo.selectedpictureindex = 15
tvi_campo.overlaypictureindex = 15

il_modifica = 0

ll_cont_fasi = 0

select	count(*)
into	:ll_cont_fasi
from	tes_fasi_lavorazione
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio and
		cod_versione = :l_chiave_distinta.cod_versione_figlio ;
if ll_cont_fasi > 0 then 
	tvi_campo.bold = true
else
	tvi_campo.bold = false
end if

ll_risposta=tv_db.insertitemlast(0, tvi_campo)

wf_imposta_tv(l_chiave_distinta.cod_prodotto_figlio,l_chiave_distinta.cod_versione_figlio,1,1,ls_errore)

tv_db.expandall(1)
tv_db.setredraw(true)
//ll_risposta = tv_db.FindItem(RootTreeItem! , 0)
//tv_db.SelectItem ( ll_risposta )
//tv_db.triggerevent("pcd_clicked")

this.title = "Distinta Base del prodotto finito:   " + is_cod_prodotto_finito + "   Versione: " + is_cod_versione

return 0
end function

public function integer wf_visual_testo_formula (string fs_cod_formula);string ls_testo_formula

//select testo_formula
// into  :ls_testo_formula
// from  tab_formule_db
//where cod_azienda = :s_cs_xx.cod_azienda and
//		cod_formula = :fs_cod_formula;
//
//if sqlca.sqlcode = -1 then
//	g_mb.messagebox("Codice Formula Utilizzo", "Errore durante l'Estrazione Formule!")
//	return -1
//end if
//
//dw_distinta_det.object.cf_testo_formula.text = f_sost_des_var_in_formula(ls_testo_formula, is_cod_prodotto_finito)
return 0
end function

public function integer wf_trova_prodotto (string fs_cod_prodotto_padre, string fs_cod_prodotto_inserito, string fs_cod_versione, ref string fs_messaggio, ref string fs_errore);string    ls_cod_prodotto_figlio, ls_cod_versione_figlio, ls_test_prodotto_f, ls_errore, &
			 ls_flag_materia_prima,ls_test,ls_messaggio
long      ll_num_figli,ll_num_righe
integer   li_risposta

datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto_padre,fs_cod_versione)
	
for ll_num_figli = 1 to ll_num_righe
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	
	if ls_cod_prodotto_figlio = upper(fs_cod_prodotto_inserito) or ls_cod_prodotto_figlio = lower(fs_cod_prodotto_inserito) then
		fs_messaggio="Attenzione! Prodotto già presente in questo ramo!"
		return 1
	end if
	
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
	       cod_prodotto_padre = :ls_cod_prodotto_figlio and    
			 cod_versione = :ls_cod_versione_figlio;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		continue
	else
		li_risposta = wf_trova_prodotto(ls_cod_prodotto_figlio,fs_cod_prodotto_inserito,ls_cod_versione_figlio, ls_messaggio,ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			destroy lds_righe_distinta
			return -1
		end if
		if li_risposta = 1 then
			fs_messaggio = ls_messaggio
			destroy lds_righe_distinta
			return 1
		end if
	end if

next

destroy lds_righe_distinta

return 0
end function

public function integer wf_trova (string fs_cod_prodotto, long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_db.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
		tv_db.setfocus()
		tv_db.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_handle = tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_db.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
		tv_db.setfocus()
		tv_db.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_padre = al_handle

do
	
	ll_padre = tv_db.finditem(parenttreeitem!,ll_padre)

	if ll_padre > 0 then
		
		ll_handle = tv_db.finditem(nexttreeitem!,ll_padre)
		
		if ll_handle > 0 then
		
			tv_db.getitem(ll_handle,ltv_item)
			
			if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
				tv_db.setfocus()
				tv_db.selectitem(ll_handle)
				il_handle = ll_handle
				return 0
			else
				if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
					return 0
				else
					return 100
				end if
			end if
			
		end if
		
	end if

loop while ll_padre > 0

return 100
end function

public function integer wf_espandi (long al_handle, long al_livello, long al_cicli);long ll_handle, ll_padre, ll_i

treeviewitem ltv_item


il_cicli ++

if il_cicli > al_livello then
	return 100
end if

tv_db.getitem( al_handle, ltv_item)
tv_db.ExpandItem ( al_handle )	

ll_handle = tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	wf_espandi(ll_handle, al_livello, il_cicli)
	il_cicli --
	
end if

ll_handle = tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	il_cicli --
	wf_espandi(ll_handle, al_livello, il_cicli)
//	il_cicli --
	
end if

return 100
end function

public function integer wf_pre_inizio ();uo_dw_main = dw_distinta_det

tv_db.setfocus()
tv_db.triggerevent("clicked")
postevent("ue_seleziona_tab")

return 0
end function

public function integer wf_comprimi (long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_db.getitem(ll_handle,ltv_item)	
	wf_comprimi(ll_handle)

end if

tv_db.collapseitem( ll_handle)

ll_handle = tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_db.getitem(ll_handle,ltv_item)	
	wf_comprimi(ll_handle)
	
end if

tv_db.collapseitem( ll_handle)

tv_db.collapseitem( al_handle)

return 100
end function

public function integer wf_imposta_tv (string fs_cod_prodotto, string fs_cod_versione, integer fi_num_livello_cor, long fl_handle, ref string fs_errore);//	Funzione che imposta l'outliner
//
// nome: wf_imposta_ol
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//	Variabili passate: 		nome					 	tipo				passaggio per			commento
//							
//							 fs_cod_prodotto					string				valore
//							 fs_cod_prodotto_finito			string		  		valore             
//							 fi_num_livello_cor				integer				valore
//							 fl_handle							long   				valore
//							 fs_errore							string				riferimento


string  ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_flag_materia_prima,ls_cod_misura_mag
long    ll_num_figli,ll_handle,ll_num_righe, ll_cont_fasi
integer li_num_priorita,li_risposta
s_chiave_distinta l_chiave_distinta

treeviewitem tvi_campo
datastore lds_righe_distinta

ll_num_figli = 1
lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)

choose case dw_distinta_base_quantita.getitemstring(dw_distinta_base_quantita.getrow(), "ordinamento")
	case "C"
		lds_righe_distinta.setsort("cod_prodotto_figlio A, num_sequenza A")
	case "D"
		lds_righe_distinta.setsort("anag_prodotti_des_prodotto A")
	case "S"
		lds_righe_distinta.setsort("num_sequenza A, cod_prodotto_figlio A")
end choose

ll_num_figli = lds_righe_distinta.sort()
		

for ll_num_figli = 1 to ll_num_righe

	l_chiave_distinta.cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	l_chiave_distinta.quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	l_chiave_distinta.cod_prodotto_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_padre")
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")
	l_chiave_distinta.cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	l_chiave_distinta.cod_versione_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione")
	ls_des_prodotto = lds_righe_distinta.getitemstring(ll_num_figli,"anag_prodotti_des_prodotto")
	ls_cod_misura_mag = lds_righe_distinta.getitemstring(ll_num_figli,"anag_prodotti_cod_misura_mag")
	l_chiave_distinta.flag_ramo_descrittivo = lds_righe_distinta.getitemstring(ll_num_figli,"distinta_flag_ramo_descrittivo")
	
	ll_cont_fasi = 0
	
	select	count(*)
	into	:ll_cont_fasi
	from	tes_fasi_lavorazione
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione = :l_chiave_distinta.cod_versione_figlio ;
	if ll_cont_fasi > 0 then 
		tvi_campo.bold = true
	else
		tvi_campo.bold = false
	end if

	ls_des_prodotto = trim(ls_des_prodotto)

	if isnull(ls_cod_misura_mag) then ls_cod_misura_mag = ""
	
	tvi_campo.itemhandle = fl_handle
	tvi_campo.data = l_chiave_distinta
	tvi_campo.label = l_chiave_distinta.cod_prodotto_figlio + "," + ls_des_prodotto + " " + string(l_chiave_distinta.quan_utilizzo) + " " + ls_cod_misura_mag

	if ls_flag_materia_prima = 'N' then
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and	 
		       cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and    
				 cod_versione = :l_chiave_distinta.cod_versione_figlio;

		if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
			if l_chiave_distinta.flag_ramo_descrittivo = "S" then
				tvi_campo.pictureindex = 10
				tvi_campo.selectedpictureindex = 10
				tvi_campo.overlaypictureindex = 10
			else
				// EnMe 20.10.15: distinguo con icona i rami MP a cui è associata una formula
				if isnull(lds_righe_distinta.getitemstring(ll_num_figli,"cod_formula_quan_utilizzo")) and isnull(lds_righe_distinta.getitemstring(ll_num_figli,"cod_formula_prod_figlio"))  and isnull (lds_righe_distinta.getitemstring(ll_num_figli,"cod_formula_vers_figlio")) then
					tvi_campo.pictureindex = 14
					tvi_campo.selectedpictureindex = 14
					tvi_campo.overlaypictureindex = 14
				else
					tvi_campo.pictureindex = 12
					tvi_campo.selectedpictureindex = 12
					tvi_campo.overlaypictureindex = 12
				end if
			end if
			ll_handle=tv_db.insertitemlast(fl_handle, tvi_campo)
			
			// ****
		
			if is_chiave_distinta.cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio and &
				is_chiave_distinta.cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre and &
				is_chiave_distinta.cod_versione_figlio = l_chiave_distinta.cod_versione_figlio and &
				is_chiave_distinta.cod_versione_padre = l_chiave_distinta.cod_versione_padre then
				
				il_modifica = ll_handle
				il_ganne = ll_handle
				ib_trovato = true
			end if
			// ****			
			
			
			continue
		else
			// EnMe 20.10.15: distinguo con icona i rami MP a cui è associata una formula
			if isnull(lds_righe_distinta.getitemstring(ll_num_figli,"cod_formula_quan_utilizzo")) and isnull(lds_righe_distinta.getitemstring(ll_num_figli,"cod_formula_prod_figlio"))  and isnull (lds_righe_distinta.getitemstring(ll_num_figli,"cod_formula_vers_figlio")) then
				tvi_campo.pictureindex = 16
				tvi_campo.selectedpictureindex = 16
				tvi_campo.overlaypictureindex = 16
			else
				tvi_campo.pictureindex = 13
				tvi_campo.selectedpictureindex = 13
				tvi_campo.overlaypictureindex = 13
			end if
			ll_handle=tv_db.insertitemlast(fl_handle, tvi_campo)
			
			// ****
		
			if is_chiave_distinta.cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio and &
				is_chiave_distinta.cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre and &
				is_chiave_distinta.cod_versione_figlio = l_chiave_distinta.cod_versione_figlio and &
				is_chiave_distinta.cod_versione_padre = l_chiave_distinta.cod_versione_padre then
				
				il_modifica = ll_handle
				il_ganne = ll_handle
				ib_trovato = true
			end if
			// ****						
			
			setnull(ls_test_prodotto_f)
			li_risposta=wf_imposta_tv(l_chiave_distinta.cod_prodotto_figlio,l_chiave_distinta.cod_versione_figlio,fi_num_livello_cor + 1,ll_handle,ls_errore)
		end if
	else
		// EnMe 20.10.15: distinguo con icona i rami MP a cui è associata una formula
		if isnull(lds_righe_distinta.getitemstring(ll_num_figli,"cod_formula_quan_utilizzo")) and isnull(lds_righe_distinta.getitemstring(ll_num_figli,"cod_formula_prod_figlio"))  and isnull (lds_righe_distinta.getitemstring(ll_num_figli,"cod_formula_vers_figlio")) then
			tvi_campo.pictureindex = 12
			tvi_campo.selectedpictureindex = 12
			tvi_campo.overlaypictureindex = 12
		else
			tvi_campo.pictureindex = 14
			tvi_campo.selectedpictureindex = 14
			tvi_campo.overlaypictureindex = 14
		end if
		ll_handle=tv_db.insertitemlast(fl_handle, tvi_campo)
		
		// ****
	
		if is_chiave_distinta.cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio and &
			is_chiave_distinta.cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre and &
			is_chiave_distinta.cod_versione_figlio = l_chiave_distinta.cod_versione_figlio and &
			is_chiave_distinta.cod_versione_padre = l_chiave_distinta.cod_versione_padre then
			
			il_modifica = ll_handle
			il_ganne = ll_handle
			ib_trovato = true
		end if
		// ****					
		
		continue
	end if
next

destroy(lds_righe_distinta)

return 0
end function

public function integer wf_get_versione (string as_cod_prodotto_figlio, ref string as_cod_versione_figlio, ref string as_errore);
integer			li_count
string				ls_cod_versione_predefinita

//se il prodotto figlio ha una versione (distinta_padri), cerca la versione predefinita (se non risulta impostata avvisa l'operatore)

select count(*)
into :li_count
from distinta_padri
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto_figlio;

if sqlca.sqlcode<0 then
	as_errore = "Errore in controllo esistenza distinta base per il prodotto "+as_cod_prodotto_figlio+ " : "+sqlca.sqlerrtext
	return -1
end if

if li_count > 0 then
	//esiste distinta base, recupera la versione predefinita
	select cod_versione
	into :ls_cod_versione_predefinita
	from distinta_padri
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:as_cod_prodotto_figlio and
				flag_predefinita='S';
				
	if sqlca.sqlcode<0 then
		as_errore = "Errore in lettura versione predefinita per il prodotto "+as_cod_prodotto_figlio+ " : "+sqlca.sqlerrtext
		return -1
	end if
	
	if ls_cod_versione_predefinita="" or isnull(ls_cod_versione_predefinita) then
		//no versione predefinita prevista
		as_errore = "manca la versione predefinita per la distinta base del prodotto "+as_cod_prodotto_figlio+ ". Impostare una versione predefinita e riprovare! "
		return -1
	else
		as_cod_versione_figlio = ls_cod_versione_predefinita
		return 0
	end if
	
else
	//no distinta base, quindi lascia invariata la variabile "as_cod_versione_figlio"
	return 0
end if
end function

public subroutine wf_visualizza_distinta ();// sulla base delle variabili di istanza is_cod_prodotto, is_cod_versione, genera la visualizzazione della distinta

wf_inizio()
il_handle = tv_db.finditem(roottreeitem!,0)
dw_distinta_det.Change_DW_Current( )
triggerevent("pc_retrieve")
dw_fasi_lavorazione.Change_DW_Current( )
triggerevent("pc_retrieve")
dw_distinta_gruppi_varianti.Change_DW_Current( )
triggerevent("pc_retrieve")			
dw_distinta_base_padre.Change_DW_Current( )
triggerevent("pc_retrieve")

tv_db.setfocus( )

end subroutine

on w_distinta_base.create
int iCurrent
call super::create
this.st_tipo=create st_tipo
this.st_13=create st_13
this.tv_db=create tv_db
this.cb_distinte_taglio=create cb_distinte_taglio
this.cb_comprimi=create cb_comprimi
this.cb_espandi=create cb_espandi
this.dw_folder=create dw_folder
this.dw_distinta_base_padre=create dw_distinta_base_padre
this.em_1=create em_1
this.dw_ricerca=create dw_ricerca
this.dw_distinta_base_quantita=create dw_distinta_base_quantita
this.dw_distinta_base_chiavi_prodotti=create dw_distinta_base_chiavi_prodotti
this.dw_distinta_gruppi_varianti=create dw_distinta_gruppi_varianti
this.dw_mat_prime_automatiche=create dw_mat_prime_automatiche
this.dw_prodotti_finiti=create dw_prodotti_finiti
this.dw_distinta_det=create dw_distinta_det
this.dw_fasi_lavorazione=create dw_fasi_lavorazione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_tipo
this.Control[iCurrent+2]=this.st_13
this.Control[iCurrent+3]=this.tv_db
this.Control[iCurrent+4]=this.cb_distinte_taglio
this.Control[iCurrent+5]=this.cb_comprimi
this.Control[iCurrent+6]=this.cb_espandi
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.dw_distinta_base_padre
this.Control[iCurrent+9]=this.em_1
this.Control[iCurrent+10]=this.dw_ricerca
this.Control[iCurrent+11]=this.dw_distinta_base_quantita
this.Control[iCurrent+12]=this.dw_distinta_base_chiavi_prodotti
this.Control[iCurrent+13]=this.dw_distinta_gruppi_varianti
this.Control[iCurrent+14]=this.dw_mat_prime_automatiche
this.Control[iCurrent+15]=this.dw_prodotti_finiti
this.Control[iCurrent+16]=this.dw_distinta_det
this.Control[iCurrent+17]=this.dw_fasi_lavorazione
end on

on w_distinta_base.destroy
call super::destroy
destroy(this.st_tipo)
destroy(this.st_13)
destroy(this.tv_db)
destroy(this.cb_distinte_taglio)
destroy(this.cb_comprimi)
destroy(this.cb_espandi)
destroy(this.dw_folder)
destroy(this.dw_distinta_base_padre)
destroy(this.em_1)
destroy(this.dw_ricerca)
destroy(this.dw_distinta_base_quantita)
destroy(this.dw_distinta_base_chiavi_prodotti)
destroy(this.dw_distinta_gruppi_varianti)
destroy(this.dw_mat_prime_automatiche)
destroy(this.dw_prodotti_finiti)
destroy(this.dw_distinta_det)
destroy(this.dw_fasi_lavorazione)
end on

event pc_setwindow;call super::pc_setwindow;string	ls_class
long ll_cont
windowobject l_objects_1[]

set_w_options(c_noresizewin)

// controllo se devo visualizzare i dati del configuratore
select count(*)
into :ll_cont
from tab_flags_configuratore
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 or ll_cont = 0 or isnull(ib_configuratore) then
	ib_configuratore =false
else
	ib_configuratore = true
end if
// fine controllo configuratore


l_objects_1[1] = dw_distinta_det
dw_folder.fu_AssignTab(1, "Dettaglio", l_Objects_1[])

l_objects_1[1] = dw_fasi_lavorazione
dw_folder.fu_AssignTab(2, "Fasi", l_Objects_1[])

l_objects_1[1] = dw_prodotti_finiti
dw_folder.fu_AssignTab(6, "Elenco PF", l_Objects_1[])

l_objects_1[1] = dw_distinta_base_padre
dw_folder.fu_AssignTab(5, "Prodotto Padre", l_Objects_1[])

l_objects_1[1] = dw_distinta_base_chiavi_prodotti
dw_folder.fu_AssignTab(4, "Chiavi", l_Objects_1[])

l_objects_1[1] = dw_distinta_gruppi_varianti
if ib_configuratore then 
	l_objects_1[2] = dw_mat_prime_automatiche
else
	dw_mat_prime_automatiche.visible = false
end if

dw_folder.fu_AssignTab(3, "Varianti", l_Objects_1[])

dw_fasi_lavorazione.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_noresizedw + c_default)
dw_distinta_det.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_disableCC+c_disableCCinsert,c_default + c_noresizedw)
dw_distinta_gruppi_varianti.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_disableCC+c_disableCCinsert,c_default + c_noresizedw)													 
dw_prodotti_finiti.set_dw_options( sqlca, pcca.null_object, c_disableCC + c_nonew + c_nomodify + c_nodelete, c_default + c_noresizedw)
dw_distinta_base_padre.set_dw_options( sqlca, pcca.null_object, c_disableCC + c_nonew + c_nomodify + c_nodelete, c_default + c_noresizedw)
dw_distinta_base_chiavi_prodotti.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete + c_disableCC + c_disableCCinsert,c_default + c_noresizedw)													 
dw_fasi_lavorazione.ib_proteggi_chiavi = false
dw_distinta_det.ib_proteggi_chiavi = false

dw_folder.fu_FolderCreate(6,6)

// *** michela: metto la lettura del prodotto e della versione fuori della wf_inizio
if not isnull(i_openparm) then
	ls_class = i_openparm.classname( )
	if lower(ls_class)= "s_distinta_open_parm" then
		s_distinta_open_parm s_distinta
		s_distinta = i_openparm
		is_cod_prodotto_finito = s_distinta.cod_prodotto_padre
		is_cod_versione = s_distinta.cod_versione
		wf_pre_inizio()
		wf_visualizza_distinta( )
	else
		is_cod_versione = s_cs_xx.parametri.parametro_dw_2.getitemstring(s_cs_xx.parametri.parametro_dw_2.getrow(),"cod_versione")
		is_cod_prodotto_finito = s_cs_xx.parametri.parametro_dw_2.getitemstring(s_cs_xx.parametri.parametro_dw_2.getrow(),"cod_prodotto")
		wf_pre_inizio()
	end if
else
	dw_folder.fu_SelectTab(4)
end if

// *** fine

em_1.text = "1"

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_distinta_det,"cod_formula_quan_utilizzo",sqlca,&
                 "tab_formule_db","cod_formula","des_formula", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
					  		" flag_tipo='1' ")


f_PO_LoadDDDW_DW(dw_fasi_lavorazione,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_fasi_lavorazione,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N'")

f_PO_LoadDDDW_DW(dw_fasi_lavorazione,"cod_cat_attrezzature_2",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'S'")

f_PO_LoadDDDW_DW(dw_fasi_lavorazione,"cod_centro_costo",sqlca,&
                 "tab_centri_costo","cod_centro_costo","des_centro_costo",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_distinta_gruppi_varianti,"cod_gruppo_variante",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
				  
f_po_loaddddw_dw(dw_fasi_lavorazione, &
                 "cod_deposito_origine", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//nuovi campi formula
f_PO_LoadDDDW_DW(dw_distinta_det,"cod_formula_quan_tecnica",sqlca,&
                 "tab_formule_db","cod_formula","des_formula", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
					  " flag_tipo='1' ")

f_PO_LoadDDDW_DW(dw_distinta_det,"cod_formula_prod_figlio",sqlca,&
                 "tab_formule_db","cod_formula","des_formula", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
					  " flag_tipo='2' ")

f_PO_LoadDDDW_DW(dw_distinta_det,"cod_formula_vers_figlio",sqlca,&
                 "tab_formule_db","cod_formula","des_formula", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
					  " flag_tipo='2' ")
					  
					  
					  
					  
					  
					  
end event

type st_tipo from statictext within w_distinta_base
integer x = 1934
integer y = 120
integer width = 274
integer height = 88
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type st_13 from statictext within w_distinta_base
integer x = 23
integer y = 260
integer width = 457
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Espandi a livello:"
boolean focusrectangle = false
end type

type tv_db from treeview within w_distinta_base
event ue_seleziona ( )
integer x = 23
integer y = 360
integer width = 2103
integer height = 2704
integer taborder = 320
string dragicon = "Exclamation!"
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean border = false
boolean disabledragdrop = false
boolean hideselection = false
long picturemaskcolor = 553648127
long statepicturemaskcolor = 553648127
end type

event ue_seleziona();if il_ganne > 0 and not isnull( il_ganne) then 
	tv_db.selectitem( il_ganne)
//	MESSAGEBOX("ue_seleziona", il_modifica)
//	MESSAGEBOX("", il_handle)
	il_ganne = 0
end if

end event

event constructor;this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "pf.bmp")								//1
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "semil.bmp")							//2
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "mp.bmp")								//3
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "integrazione.bmp")					//4
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "variante.bmp")						//5
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_aperta.bmp")					//6
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_in_corso.bmp")				//7
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_chiusa.bmp")					//8
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_esterna.bmp")				//9
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "ramo_descrittivo.bmp")			//10
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "variante_vincol.bmp")				//11
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "12.5\Materia_prima_fx.png")		//12
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "12.5\Semilavorato_fx.png")		//13
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "12.5\Materia_prima.png")		//14
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "12.5\Radice_pf.ico")				//15
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "12.5\Semilavorato.png")			//16

end event

event clicked;if handle<>0 then
	il_handle = handle	
	dw_distinta_det.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_fasi_lavorazione.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_distinta_gruppi_varianti.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")	
	dw_distinta_base_chiavi_prodotti.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")	
end if

end event

event key;
if key=keydelete! then
	if g_mb.messagebox("sep","Sei sicuro di voler cancellare il ramo di distinta?",Question!,yesno!,2) = 1 then

		s_chiave_distinta l_chiave_distinta
		treeviewitem tvi_campo
		long ll_risposta
		
		tv_db.GetItem ( il_handle, tvi_campo )
		l_chiave_distinta = tvi_campo.data
		
		delete from distinta_gruppi_varianti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto_padre=:l_chiave_distinta.cod_prodotto_padre and
					num_sequenza=:l_chiave_distinta.num_sequenza and 
					cod_prodotto_figlio=:l_chiave_distinta.cod_prodotto_figlio and 
					cod_versione=:l_chiave_distinta.cod_versione_padre and 
					cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore in cancellazione GRUPPI VARIANTI del ramo di distinta.~r~n" + sqlca.sqlerrtext,stopsign!)
			rollback;
			return
		end if
		
		delete from distinta
		where cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto_padre=:l_chiave_distinta.cod_prodotto_padre and
					num_sequenza=:l_chiave_distinta.num_sequenza and
					cod_prodotto_figlio=:l_chiave_distinta.cod_prodotto_figlio and
					cod_versione=:l_chiave_distinta.cod_versione_padre and
					cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore in cancellazione del ramo di distinta.~r~n" + sqlca.sqlerrtext,stopsign!)
			rollback;
			return
		end if
		
		commit;
		
		ll_risposta=wf_inizio()	

	end if
end if
end event

event dragwithin;TreeViewItem		ltvi_Over

If GetItem(handle, ltvi_Over) = -1 Then
	SetDropHighlight(0)

	Return
End If


SetDropHighlight(handle)

il_handle = handle
end event

event dragdrop;s_chiave_distinta			l_chiave_distinta, l_chiave_distinta_2

treeviewitem				tvi_campo, tvi_campo_2

long							ll_risposta, ll_num_sequenza, ll_test,ll_i, ll_handle

double						ldd_quan_utilizzo, ldd_quan_tecnica

string							ls_cod_formula_quan_utilizzo, ls_cod_prodotto_figlio, ls_cod_misura, ls_flag_materia_prima, ls_flag_escludibile, & 
								ls_cod_prodotto_padre, ls_messaggio, ls_errore, ls_cod_versione_padre, ls_cod_versione_figlio, &
								ls_cod_prodotto, ls_cod_figlio
								
integer						li_risposta

boolean						lb_GIB



GetItem (il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data
dw_ricerca.accepttext()
dw_distinta_base_quantita.accepttext()

ll_num_sequenza = dw_distinta_base_quantita.getitemnumber( 1, "sequenza")
ls_cod_prodotto_figlio = upper(dw_ricerca.getitemstring(dw_ricerca.getrow(), "rs_cod_prodotto"))

if isnull(ls_cod_prodotto_figlio) then
	 g_mb.error("Attenzione: non hai selezionato alcun prodotto da inserire in distinta!")
	 return
end if
	
if l_chiave_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio   then
	 g_mb.error("Attenzione: padre e figlio non possono coincidere!")
	 return
end if

// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06

ll_i = 1

//numero del nodo da cui parto il_handle
ll_handle = il_handle

do while 1 = ll_i 
	
	//carico i dati del nodo numero ll_handle nel nodo tvi_campo_2
	GetItem (ll_handle, tvi_campo_2 )
	l_chiave_distinta_2 = tvi_campo_2.data
	
	ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
	
	if upper(ls_cod_prodotto) = upper(ls_cod_prodotto_figlio) then
		g_mb.error("Impossibile inserire un prodotto come figlio di se stesso! Operazione annullata")
		return
	end if
	//cerca il padre del nodo numero ll_handle
	ll_handle = FindItem ( ParentTreeItem!	,  ll_handle)
	if ll_handle = -1 then exit   // allora sono alla radice

loop

//*********fine*** 

SELECT cod_misura_mag,
		 flag_escludibile,
		 flag_materia_prima
INTO   :ls_cod_misura,
		 :ls_flag_escludibile,
		 :ls_flag_materia_prima
FROM   anag_prodotti  
WHERE  cod_azienda = :s_cs_xx.cod_azienda 
AND    cod_prodotto = :ls_cod_prodotto_figlio;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore sul DB: " + sqlca.sqlerrtext)
	return
end if

if isnull(ls_flag_escludibile) then ls_flag_escludibile =  'N'
if isnull(ls_flag_materia_prima) then ls_flag_materia_prima =  'N'

if ll_num_sequenza = 0 then
	select max(distinta.num_sequenza)
	into   :ll_num_sequenza
	from   distinta
	where  distinta.cod_azienda = :s_cs_xx.cod_azienda and 
			 distinta.cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
			 distinta.cod_versione = :l_chiave_distinta.cod_versione_figlio;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore sul DB: " + sqlca.sqlerrtext)
		return
	end if
	
	if isnull(ll_num_sequenza) then ll_num_sequenza = 0
	
	ll_num_sequenza = ll_num_sequenza + 10
end if

wf_visual_testo_formula(ls_cod_formula_quan_utilizzo)

if ls_cod_formula_quan_utilizzo = "" then setnull(ls_cod_formula_quan_utilizzo)

ldd_quan_utilizzo = dw_distinta_base_quantita.getitemnumber( 1, "qta_utilizzo")
ldd_quan_tecnica = dw_distinta_base_quantita.getitemnumber( 1, "qta_tecnica")
ls_cod_versione_figlio = dw_distinta_base_quantita.getitemstring(1,"versione")

ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_figlio
ls_cod_versione_padre = l_chiave_distinta.cod_versione_figlio

li_risposta = wf_trova_prodotto (ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione_padre,ls_messaggio,ls_errore)

if li_risposta = 1 then g_mb.warning(ls_messaggio)

if li_risposta = -1 then
	g_mb.error(ls_errore)
	return 
end if


guo_functions.uof_get_parametro_azienda("GIB", lb_GIB)
if lb_GIB then
	//Donato 27/03/2014
	//per gibus ragiona diversamente: SR Varie_per_produzione
	if wf_get_versione(ls_cod_prodotto_figlio, ls_cod_versione_figlio, ls_errore) < 0 then
		g_mb.error(ls_errore)
		return
	end if
	
else
	// *** Michela 15/09/2006: con EnMe si è deciso che la versione della materia prima diventa la versione del padre
	//                         diretto e non del prodotto finito
	setnull( ll_test)
	
	select count(*)
	into   :ll_test
	from   distinta 
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto_padre = :ls_cod_prodotto_figlio;
			 
	if ( isnull(ll_test) or ll_test = 0 ) and sqlca.sqlcode = 0 then
		setnull(ll_test)
		
		select count(*)
		into   :ll_test
		from   distinta_padri
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_figlio;
				 
		if ( isnull(ll_test) or ll_test = 0 ) and sqlca.sqlcode = 0 then
			
			ls_cod_versione_figlio = ls_cod_versione_padre
			
		end if
	end if
	// *** fine

end if

insert into distinta
(cod_azienda,
 cod_prodotto_padre,
 num_sequenza,
 cod_prodotto_figlio,
 cod_versione,
 cod_versione_figlio,
 cod_misura,
 quan_tecnica,
 quan_utilizzo,
 fase_ciclo,
 dim_x,
 dim_y,
 dim_z,
 dim_t,
 coef_calcolo,
 des_estesa,
 formula_tempo,
 cod_formula_quan_utilizzo,
 flag_escludibile,
 flag_materia_prima)
 values
 (:s_cs_xx.cod_azienda,
  :ls_cod_prodotto_padre,
  :ll_num_sequenza,
  :ls_cod_prodotto_figlio,
  :ls_cod_versione_padre,
  :ls_cod_versione_figlio,
  :ls_cod_misura,
  :ldd_quan_tecnica,
  :ldd_quan_utilizzo,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  :ls_cod_formula_quan_utilizzo,
  :ls_flag_escludibile,
  :ls_flag_materia_prima);

if sqlca.sqlcode < 0 then
	g_mb.error("Errore sul DB:" + sqlca.sqlerrtext)
	return
end if

commit;

ll_risposta = wf_inizio()	
end event

event selectionchanged;if newhandle<>0 then
	il_handle = newhandle
	dw_distinta_det.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_fasi_lavorazione.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_distinta_gruppi_varianti.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")		
end if
end event

type cb_distinte_taglio from commandbutton within w_distinta_base
integer x = 1600
integer y = 260
integer width = 320
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Distinte T."
end type

event clicked;s_cs_xx.parametri.parametro_s_10 = is_cod_prodotto_finito
s_cs_xx.parametri.parametro_s_11 = is_cod_versione

window_open_parm(w_distinte_taglio, -1, dw_distinta_det)
end event

type cb_comprimi from commandbutton within w_distinta_base
integer x = 1257
integer y = 260
integer width = 320
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Comprimi"
end type

event clicked;long ll_tvi

tv_db.setredraw( false)
ll_tvi = tv_db.FindItem(RootTreeItem! , 0)
wf_comprimi(ll_tvi)
tv_db.setredraw( true)

end event

type cb_espandi from commandbutton within w_distinta_base
integer x = 914
integer y = 260
integer width = 320
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Espandi"
end type

event clicked;long ll_handle, ll_livelli

ll_livelli = long(em_1.text)
il_cicli = 0

tv_db.setredraw( false)

il_handle = tv_db.finditem(roottreeitem!,0)
wf_comprimi(il_handle)

tv_db.setfocus()

tv_db.selectitem(il_handle)

if il_handle > 0 then
	wf_espandi(il_handle, ll_livelli, il_cicli)
end if

tv_db.setredraw( true)
end event

type dw_folder from u_folder within w_distinta_base
integer x = 2171
integer y = 240
integer width = 2377
integer height = 2820
integer taborder = 240
boolean border = false
end type

event po_tabclicked;call super::po_tabclicked;choose case i_selectedtab

	case 1		
		iuo_dw_main = dw_distinta_det
		wf_visual_testo_formula(dw_distinta_det.getitemstring(dw_distinta_det.getrow(), "cod_formula_quan_utilizzo"))
		
	case 2
		s_chiave_distinta l_chiave_distinta
		treeviewitem tvi_campo
		if tv_db.GetItem ( il_handle, tvi_campo )= -1 then return
		l_chiave_distinta = tvi_campo.data
		string ls_test
		
		iuo_dw_main = dw_fasi_lavorazione
	
		select cod_azienda
		into   :ls_test
		from   tes_fasi_lavorazione
		where  cod_azienda  = :s_cs_xx.cod_azienda
		and    cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio and
		       cod_versione = :l_chiave_distinta.cod_versione_figlio;
				 
		//Donato 19/12/2011: visualizzare NEW anche in caso di root
		//if sqlca.sqlcode<>100 or il_handle = 1 then
		if sqlca.sqlcode<>100 then
			dw_fasi_lavorazione.set_dw_options(sqlca,pcca.null_object,c_nonew,c_default)
		else
			dw_fasi_lavorazione.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
		end if

	case 3	
		iuo_dw_main = dw_distinta_gruppi_varianti
		if il_handle = 1 then
			dw_distinta_gruppi_varianti.set_dw_options(sqlca,pcca.null_object,c_nonew + c_disableCC+c_disableCCinsert,c_default)													 
		else
			dw_distinta_gruppi_varianti.set_dw_options(sqlca,pcca.null_object,c_disableCC+c_disableCCinsert,c_default)													 
		end if
		
	case 4	
		

end choose
end event

type dw_distinta_base_padre from uo_cs_xx_dw within w_distinta_base
integer x = 2217
integer y = 340
integer width = 2240
integer height = 1260
integer taborder = 220
boolean bringtotop = true
string dataobject = "d_distinta_base_padre"
boolean border = false
end type

event pcd_delete;call super::pcd_delete;//triggerevent("pcd_save")
end event

event pcd_modify;call super::pcd_modify;//if i_extendmode then
//	
//	ib_proteggi_chiavi=false
//	
//   cb_ricerca_figlio_dist.enabled=true
//	cb_inserisci.enabled = true
//	cb_inserisci_formula.enabled = true
//	cb_modifica_testo_formula.enabled = true
//end if
end event

event pcd_new;call super::pcd_new;//if i_extendmode then
//	integer li_risposta
//	long ll_sequenza
//	string ls_cod_prodotto_padre
//	ib_proteggi_chiavi=false
//	s_chiave_distinta l_chiave_distinta
//	treeviewitem tvi_campo
//	
//	li_risposta = tv_db.GetItem ( il_handle, tvi_campo )
//
//	if li_risposta = -1 then
//		messagebox("Sep","Selezionare almeno un elemento dalla struttura ad albero!", Exclamation!)
// 	   triggerevent("pcd_delete")
//	   triggerevent("pcd_save")
//	   return
//	end if 
//
//	l_chiave_distinta = tvi_campo.data
//   cb_ricerca_figlio_dist.enabled=true
//	cb_inserisci.enabled = true
//	cb_inserisci_formula.enabled = true
//	cb_modifica_testo_formula.enabled = true
//
//	if l_chiave_distinta.cod_prodotto_figlio="" then
//		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
//	else
//		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_figlio
//	end if
//	
//	setitem(getrow(), "cod_prodotto_padre",ls_cod_prodotto_padre)		
//   triggerevent("itemchanged")
//
//   select max(distinta.num_sequenza)
//   into   :ll_sequenza
//   from   distinta
//   where  distinta.cod_azienda = :s_cs_xx.cod_azienda and 
//          distinta.cod_prodotto_padre = :ls_cod_prodotto_padre;
//
//   if isnull(ll_sequenza) then ll_sequenza = 0
//   ll_sequenza = ll_sequenza + 10
//   setitem(getrow(), "num_sequenza", ll_sequenza)
//	triggerevent("itemchanged")
//   setcolumn("cod_prodotto_figlio")
//	wf_visual_testo_formula(this.getitemstring(this.getrow(), "cod_formula_quan_utilizzo"))
//	
//end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta


l_Error = Retrieve( s_cs_xx.cod_azienda, is_cod_prodotto_finito, is_cod_versione)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_save;call super::pcd_save;//if i_extendmode then
//	integer li_risposta
//	string ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_messaggio,ls_errore
//
//
//	ls_cod_prodotto_padre = getitemstring(getrow(),"cod_prodotto_padre")
//	ls_cod_prodotto_figlio = getitemstring(getrow(),"cod_prodotto_figlio")
//	ls_cod_versione = getitemstring(getrow(),"cod_versione")
//	
//	li_risposta = wf_trova_prodotto(ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_messaggio,ls_errore)
//	
//	if li_risposta = 1 then messagebox("SEP",ls_messaggio,information!)
//
//	if li_risposta = -1 then
//		messagebox("SEP",ls_errore,stopsign!)
//		return 
//	end if
//
//end if
end event

event clicked;call super::clicked;choose case dwo.name
		
	case "b_duplica"
		
		window_open(w_duplica_distinta,-1)
		
	case "b_report"
		
		s_cs_xx.parametri.parametro_s_1 = is_cod_prodotto_finito
		s_cs_xx.parametri.parametro_s_2 = is_cod_versione
		
		window_open(w_report_distinta,-1)		
		
	case "b_costo"
		
		if isnull(is_cod_prodotto_finito) or isnull(is_cod_versione) or is_cod_prodotto_finito="" or is_cod_versione = "" then
			g_mb.messagebox( "Sep", "Non è possibile calcolare il costo poichè non è selezionato alcun prodotto!",stopsign!)
			return
		end if
		
		s_cs_xx.parametri.parametro_s_1 = is_cod_prodotto_finito
		s_cs_xx.parametri.parametro_s_2 = is_cod_versione
		s_cs_xx.parametri.parametro_s_3 = "1"
		s_cs_xx.parametri.parametro_i_1 = 1

		window_open(w_costo_preventivo_produzione,-1)
		
	case "b_padre"
		
		window_open( w_distinta_padri, -1)
		if isvalid( w_distinta_padri ) then
			
			s_cs_xx.parametri.parametro_s_1 = is_cod_prodotto_finito
			s_cs_xx.parametri.parametro_s_2 = is_cod_versione
			w_distinta_padri.triggerevent("ue_cerca_prodotto")
			
		end if
		
end choose
end event

type em_1 from editmask within w_distinta_base
integer x = 503
integer y = 260
integer width = 389
integer height = 80
integer taborder = 250
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
double increment = 1
string minmax = "0~~99999"
end type

type dw_ricerca from u_dw_search within w_distinta_base
event ue_key pbm_dwnkey
integer x = 46
integer y = 20
integer width = 2446
integer height = 100
integer taborder = 40
string dragicon = "H:\CS_XX_50\cs_sep\Cs_sep.ico"
string dataobject = "d_distinta_base_ricerca_pf"
boolean border = false
end type

event constructor;call super::constructor;this.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "cs_sep.ico"
end event

event itemchanged;call super::itemchanged;string					ls_cod_versione, ls_flag_blocco
long					ll_test
datetime				ldt_data_blocco

// per default la versione del figlio viene proposta uguale
// alla versione del padre
if not isnull(data) then
	dw_distinta_base_quantita.setitem(dw_ricerca.getrow(),"versione",is_cod_versione)
end if

choose case dwo.name
	case "rs_cod_prodotto"
		
		if isnull(data) or data = "" then
			dw_distinta_base_quantita.object.versione.protect = '1'
			dw_distinta_base_quantita.object.versione.color = string(RGB(128,128,128))
			st_tipo.text = ""
		else
			dw_distinta_base_quantita.object.versione.protect = '0'
			dw_distinta_base_quantita.object.versione.color = string(RGB(255,255,255))
			
			f_PO_LoadDDDW_DW(dw_distinta_base_quantita,"versione",sqlca,&
						  "distinta"," cod_versione ","'VERSIONE ' " + guo_functions.uof_concat_op() + " cod_versione",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto_padre = '" + data + "' ")
						  
			select cod_versione
			into   :ls_cod_versione
			from   distinta_padri
			where  cod_azienda  = :s_cs_xx.cod_azienda and
			       cod_prodotto = :data and
					 flag_predefinita = 'S' ;
					 
			if sqlca.sqlcode = 0 then 
				st_tipo.text = "PF"
				dw_distinta_base_quantita.setitem(dw_distinta_base_quantita.getrow(),"versione",ls_cod_versione)
			else
				setnull(ll_test)
				
				select count(*)
				into   :ll_test
				from   distinta 
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_prodotto_padre = :data;
						 
				if not isnull(ll_test) and ll_test > 0 then
					st_tipo.text = "SL"
				else
					st_tipo.text = "MP"
				end if
			end if
			
			// (EnMe 17-05-2016) Aggiunto controllo del blocco prodotto
			select 	flag_blocco, 
						data_blocco
			into		:ls_flag_blocco,
						:ldt_data_blocco
			from		anag_prodotti
			where		cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :data  ;
			if sqlca.sqlcode = 0 and ls_flag_blocco = "S" then
				g_mb.warning(g_str.format("Prodotto Bloccato dal $1", ldt_data_blocco))
			end if
		end if
		
end choose
end event

event clicked;call super::clicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
		
	case "b_trova"
		string ls_cod_prodotto
		
		ls_cod_prodotto = dw_ricerca.getitemstring( 1, "rs_cod_prodotto")
		
		if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
			g_mb.messagebox( "SEP", "Attenzione:selezionare un prodotto per eseguire la ricerca!")
			return -1
		end if
		
		if isnull(il_handle) or il_handle = 0 then
			il_handle = tv_db.finditem(roottreeitem!,0)
		end if
		
		tv_db.setfocus()
		
		tv_db.selectitem(il_handle)
		
		if il_handle > 0 then
			if wf_trova(ls_cod_prodotto,il_handle) = 100 then
				g_mb.messagebox("Menu Principale","Prodotto non trovato",information!)
				tv_db.setfocus()
			end if
		end if		
		
	case else
		drag(Begin!)
end choose
end event

type dw_distinta_base_quantita from u_dw_search within w_distinta_base
event ue_key pbm_dwnkey
event ue_ricarica_distinta ( )
integer x = 23
integer width = 4526
integer height = 236
integer taborder = 50
string dataobject = "d_distinta_base_quantita"
end type

event ue_ricarica_distinta();wf_inizio()
end event

event itemchanged;call super::itemchanged;if isvalid(dwo) then
	choose case dwo.name
		case "ordinamento"
			postevent("ue_ricarica_distinta")
	end choose
end if
end event

type dw_distinta_base_chiavi_prodotti from uo_cs_xx_dw within w_distinta_base
integer x = 2309
integer y = 380
integer width = 2103
integer height = 1616
integer taborder = 270
string dataobject = "d_distinta_base_chiavi_prodotti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda,l_chiave_distinta.cod_prodotto_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_distinta_gruppi_varianti from uo_cs_xx_dw within w_distinta_base
event pcd_retrieve pbm_custom60
integer x = 2213
integer y = 348
integer width = 2290
integer height = 448
integer taborder = 300
boolean bringtotop = true
string dataobject = "d_distinta_base_gruppi_varianti"
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

dw_mat_prime_automatiche.reset()

l_Error = Retrieve(s_cs_xx.cod_azienda,l_chiave_distinta.cod_prodotto_padre, &
						 l_chiave_distinta.num_sequenza,l_chiave_distinta.cod_prodotto_figlio, &
						 l_chiave_distinta.cod_versione_padre, l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_new;call super::pcd_new;LONG  l_Idx
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "cod_prodotto_padre", l_chiave_distinta.cod_prodotto_padre)
		SetItem(l_Idx, "cod_prodotto_figlio", l_chiave_distinta.cod_prodotto_figlio)
		SetItem(l_Idx, "num_sequenza", l_chiave_distinta.num_sequenza)
		SetItem(l_Idx, "cod_versione", l_chiave_distinta.cod_versione_padre)
		SetItem(l_Idx, "cod_versione_figlio", l_chiave_distinta.cod_versione_figlio)
   END IF
NEXT
end event

event rowfocuschanged;call super::rowfocuschanged;if ib_configuratore then
	if this.rowcount() > 0 then
	
		string ls_cod_gruppo_variante
		long   ll_ret
		
		ls_cod_gruppo_variante = getitemstring(getrow(), "cod_gruppo_variante")
		
		if not isnull(ls_cod_gruppo_variante) then
			dw_mat_prime_automatiche.settransobject(sqlca)
			ll_ret = dw_mat_prime_automatiche.retrieve(s_cs_xx.cod_azienda, is_cod_prodotto_finito, ls_cod_gruppo_variante)
		end if
			
	end if
end if
end event

type dw_mat_prime_automatiche from uo_std_dw within w_distinta_base
integer x = 2213
integer y = 804
integer width = 2290
integer height = 1680
integer taborder = 270
boolean bringtotop = true
string dataobject = "d_distinta_det_varianti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

type dw_prodotti_finiti from uo_cs_xx_dw within w_distinta_base
integer x = 2213
integer y = 348
integer width = 2286
integer height = 2136
integer taborder = 290
boolean bringtotop = true
string dataobject = "d_prodotti_finiti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event doubleclicked;call super::doubleclicked;string ls_cod_prodotto, ls_cod_versione
long   ll_ret, ll_riga

ll_riga = getrow()
//messagebox("", ll_riga)
if ll_riga < 1 or isnull( ll_riga) then return

ls_cod_prodotto = getitemstring( ll_riga, "cod_prodotto")
ls_cod_versione = getitemstring( ll_riga, "cod_versione")

ll_ret = g_mb.messagebox( "SEP", "Vuoi visualizzare la distinta base del prodotto " + ls_cod_prodotto + " ?", Exclamation!, OKCancel!, 2)

IF ll_ret = 1 THEN
	if not isnull(ls_cod_prodotto) and ls_cod_prodotto <> "" and ls_cod_versione <> "" and not isnull(ls_cod_versione) then
		
		is_cod_prodotto_finito = ls_cod_prodotto
		is_cod_versione = ls_cod_versione
		wf_pre_inizio()
		wf_inizio()
		il_handle = tv_db.finditem(roottreeitem!,0)
		dw_distinta_det.Change_DW_Current( )
		parent.triggerevent("pc_retrieve")
		dw_fasi_lavorazione.Change_DW_Current( )
		parent.triggerevent("pc_retrieve")
		dw_distinta_gruppi_varianti.Change_DW_Current( )
		parent.triggerevent("pc_retrieve")			
		dw_distinta_base_padre.Change_DW_Current( )
		parent.triggerevent("pc_retrieve")
	end if
END IF
end event

type dw_distinta_det from uo_cs_xx_dw within w_distinta_base
event ue_visualizza_leadtime ( )
integer x = 2194
integer y = 340
integer width = 2309
integer height = 2708
integer taborder = 310
string dataobject = "d_distinta_det"
boolean border = false
end type

event ue_visualizza_leadtime();
string ls_cod_prodotto
long	ll_lead_time

if rowcount() < 1 then return

ls_cod_prodotto = getitemstring(1,"cod_prodotto_figlio")

if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) < 1 then return

select lead_time
into   :ll_lead_time
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_prodotto;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep","Codice prodotto inesistente!~r~nCorreggere il dato."+ sqlca.sqlerrtext)	
	return
end if

this.object.lead_time_t.text = "Lead Time Anagrafica=" + string(ll_lead_time)

wf_visual_testo_formula(getitemstring(1,"cod_formula_quan_utilizzo"))

if not isnull(getitemstring(1,"flag_materia_prima")) and getitemstring(1,"flag_materia_prima") = 'S' then
	this.object.t_flag_valorizza_mp.visible = 1
	this.object.flag_valorizza_mp.visible = 1
else	
	this.object.t_flag_valorizza_mp.visible = 0
	this.object.flag_valorizza_mp.visible = 0
end if

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )

l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   l_chiave_distinta.cod_prodotto_padre, & 
						 l_chiave_distinta.num_sequenza, &
						 l_chiave_distinta.cod_prodotto_figlio, &
						 l_chiave_distinta.cod_versione_padre, &
						 l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

postevent("ue_visualizza_leadtime")
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	integer li_risposta
	long ll_sequenza
	string ls_cod_prodotto_padre, ls_cod_versione_padre
	ib_proteggi_chiavi=false
	s_chiave_distinta l_chiave_distinta
	treeviewitem tvi_campo
	
	li_risposta = tv_db.GetItem ( il_handle, tvi_campo )

	if li_risposta = -1 then
		g_mb.messagebox("Sep","Selezionare almeno un elemento dalla struttura ad albero!", Exclamation!)
 	   triggerevent("pcd_delete")
	   triggerevent("pcd_save")
	   return
	end if 

	l_chiave_distinta = tvi_campo.data

	// Michele: modificato ramo radice ora ha padre vuoto e solo codice figlio
	//if l_chiave_distinta.cod_prodotto_figlio="" then
	//	ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
	//	ls_cod_versione_padre = l_chiave_distinta.cod_versione_padre
	//else
		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_figlio
		ls_cod_versione_padre = l_chiave_distinta.cod_versione_figlio
	//end if
	
	setitem(getrow(), "cod_prodotto_padre",ls_cod_prodotto_padre)		
	setitem(getrow(), "cod_versione",ls_cod_versione_padre)
   triggerevent("itemchanged")

   select max(distinta.num_sequenza)
   into   :ll_sequenza
   from   distinta
   where  distinta.cod_azienda = :s_cs_xx.cod_azienda and 
          distinta.cod_prodotto_padre = :ls_cod_prodotto_padre and
			 cod_versione = :ls_cod_versione_padre;

   if isnull(ll_sequenza) then ll_sequenza = 0
   ll_sequenza = ll_sequenza + 10
   setitem(getrow(), "num_sequenza", ll_sequenza)
	triggerevent("itemchanged")
    setcolumn("cod_prodotto_figlio")
	wf_visual_testo_formula(this.getitemstring(this.getrow(), "cod_formula_quan_utilizzo"))
	
	
	this.object.b_ricerca_figlio.enabled=true
	this.object.b_formule_legami.enabled=false
	
		
	this.object.b_ricerca_formule_db.enabled=true
	this.object.b_modifica_testo_formula.enabled=true
	this.object.b_inserisci_formula.enabled=true
	
	this.object.b_ricerca_formule_db_pfiglio.enabled=true
	this.object.b_modifica_testo_formula_pfiglio.enabled=true
	this.object.b_inserisci_formula_pfiglio.enabled=true
	
	this.object.b_ricerca_formule_db_vfiglio.enabled=true
	this.object.b_modifica_testo_formula_vfiglio.enabled=true
	this.object.b_inserisci_formula_vfiglio.enabled=true
	
	this.object.b_ricerca_formule_db_qtec.enabled=true
	this.object.b_modifica_testo_formula_qtec.enabled=true
	this.object.b_inserisci_formula_qtec.enabled=true
	
	
	dw_fasi_lavorazione.object.b_lavorazione.enabled=true


end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	
	ib_proteggi_chiavi=false
	
	this.object.b_ricerca_figlio.enabled=true
	this.object.b_inserisci_formula.enabled=true
	this.object.b_formule_legami.enabled=false
	this.object.b_modifica_testo_formula.enabled=true
	dw_fasi_lavorazione.object.b_lavorazione.enabled=true
	this.object.b_ricerca_formule_db.enabled=true
	
	
	this.object.b_ricerca_formule_db_pfiglio.enabled=true
	this.object.b_modifica_testo_formula_pfiglio.enabled=true
	this.object.b_inserisci_formula_pfiglio.enabled=true
	
	this.object.b_ricerca_formule_db_vfiglio.enabled=true
	this.object.b_modifica_testo_formula_vfiglio.enabled=true
	this.object.b_inserisci_formula_vfiglio.enabled=true
	
	this.object.b_ricerca_formule_db_qtec.enabled=true
	this.object.b_modifica_testo_formula_qtec.enabled=true
	this.object.b_inserisci_formula_qtec.enabled=true
	
	
end if
end event

event pcd_saveafter;call super::pcd_saveafter;if i_extendmode then
	long ll_risposta
	
	if not isvalid(w_distinte_taglio) then	
		
		ll_risposta = wf_inizio()			
		
	END IF

end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)				
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_versione")) THEN
      SetItem(l_Idx, "cod_versione", is_cod_versione)				
   END IF	
	
NEXT

end event

event pcd_delete;call super::pcd_delete;triggerevent("pcd_save")
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	
	this.object.b_ricerca_figlio.enabled=false
	this.object.b_inserisci_formula.enabled=false
	this.object.b_formule_legami.enabled=true
	this.object.b_modifica_testo_formula.enabled=false
	dw_fasi_lavorazione.object.b_lavorazione.enabled=false
	this.object.b_ricerca_formule_db.enabled=false
	
	if ib_trovato  then
		tv_db.triggerevent("ue_seleziona")
		ib_trovato = false
	end if			
	
	
	this.object.b_ricerca_formule_db_pfiglio.enabled=false
	this.object.b_modifica_testo_formula_pfiglio.enabled=false
	this.object.b_inserisci_formula_pfiglio.enabled=false
	
	this.object.b_ricerca_formule_db_vfiglio.enabled=false
	this.object.b_modifica_testo_formula_vfiglio.enabled=false
	this.object.b_inserisci_formula_vfiglio.enabled=false
	
	this.object.b_ricerca_formule_db_qtec.enabled=false
	this.object.b_modifica_testo_formula_qtec.enabled=false
	this.object.b_inserisci_formula_qtec.enabled=false
	
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_misura,ls_cod_prodotto_padre,ls_flag_escludibile,ls_flag_materia_prima, ls_descrizione, ls_tipo
	long ll_lead_time
	
	
	
 choose case i_colname
	
	case "cod_prodotto_figlio"

		if len(data) < 1 then
          PCCA.Error = c_ValFailed
          return
      end if  
		
		this.object.lead_time_t.text = ""

      ls_cod_prodotto_padre = upper(this.getitemstring(this.getrow(), "cod_prodotto_padre"))
		
		//controllo che esista il codice del prodotto
		if not isnull(data) and (len(data) > 0) then
			
			// per default la versione del figlio viene proposta uguale
			// alla versione del padre
			setitem(row,"cod_versione_figlio",is_cod_versione)
			
			select cod_prodotto, lead_time
			into   :ls_descrizione, :ll_lead_time
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :data;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Sep","Codice prodotto inesistente!~r~nCorreggere il dato."+ sqlca.sqlerrtext)	
				return 1
			end if
			
			this.object.lead_time_t.text = "Lead Time Anagrafica=" + string(ll_lead_time)
			
		end if
		
  	   if (ls_cod_prodotto_padre = data) and (len(data) > 0)  then
          g_mb.messagebox("Sep","Attenzione: padre e figlio non possono coincidere!")
          PCCA.Error = c_ValFailed
          return
		end if
	

		// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06
		long ll_handle
		string  ls_cod_prodotto, ls_cod_figlio
		treeviewitem  tvi_campo_2
		s_chiave_distinta l_chiave_distinta_2
		//numero del nodo da cui parto il_handle
		ll_handle = il_handle

		do while true
			//carico i dati del nodo numero ll_handle nel nodo tvi_campo_2
			tv_db.getItem (ll_handle, tvi_campo_2 )
			l_chiave_distinta_2 = tvi_campo_2.data
			if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
				//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
				ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
			else
				ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
			end if
			
			if upper(ls_cod_prodotto) = upper(data) then
				g_mb.messagebox("Sep","Impossibile inserire un prodotto come figlio di se stesso!~r~nCorreggere il dato.", stopsign!)
				return 1
			end if
			//cerca il padre del nodo numero ll_handle
			ll_handle = tv_db.findItem ( ParentTreeItem!	,  ll_handle)
			if ll_handle = -1 then exit   // allora sono alla radice
		
		loop

//*********fine*** 	
		
	
      if (ls_cod_prodotto_padre = data) and (len(data) > 0)  then
          g_mb.messagebox("Sep","Attenzione: padre e figlio non possono coincidere!")
          PCCA.Error = c_ValFailed
          return
      end if
	
		SELECT cod_misura_mag,
		       flag_escludibile,
				 flag_materia_prima
      INTO   :ls_cod_misura,
		       :ls_flag_escludibile,
				 :ls_flag_materia_prima
      FROM   anag_prodotti  
      WHERE  cod_azienda = :s_cs_xx.cod_azienda AND
		       cod_prodotto = :data;

      if len(ls_cod_misura) > 0  and not isnull(ls_cod_misura) then
         setitem(getrow(), "cod_misura", ls_cod_misura)
      end if
		
      if len(ls_flag_materia_prima) > 0  and not isnull(ls_flag_materia_prima) then
         setitem(getrow(), "flag_materia_prima", ls_flag_materia_prima)
      end if
		
      if len(ls_flag_escludibile) > 0  and not isnull(ls_flag_escludibile) then
         setitem(getrow(), "flag_escludibile", ls_flag_escludibile)
      end if

	case "cod_formula_quan_utilizzo", "cod_formula_quan_tecnica", "cod_formula_prd_figlio", "cod_formula_vers_figlio"
		//wf_visual_testo_formula(data)
		
		if data <>"" then
		
			select flag_tipo
			 into  :ls_tipo
			 from  tab_formule_db
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_formula = :data;
		
			choose case dwo.name
				case "cod_formula_prd_figlio", "cod_formula_vers_figlio"
					if ls_tipo <> "2" then
						g_mb.warning("La formula selezionata torna un NUMERO, mentre è necessario selezionarne una che torni una STRINGA!")
						return 1
					end if
				
				case "cod_formula_quan_utilizzo", "cod_formula_quan_tecnica"
					if ls_tipo <> "1" then
						g_mb.warning("La formula selezionata torna una STRINGA, mentre è necessario selezionarne una che torni un NUMERO!")
						return 1
					end if
					
			end choose
		end if
	
	
		
	case "flag_materia_prima"
		if not isnull(data) and data = "S" then
			
			this.object.t_flag_valorizza_mp.visible = 1
			this.object.flag_valorizza_mp.visible = 1		
			setitem( getrow(), "flag_valorizza_mp", "S")
		else
			this.object.t_flag_valorizza_mp.visible = 0
			this.object.flag_valorizza_mp.visible = 0		
		end if
		
	end choose
end if
end event

event updateend;call super::updateend;//treeviewitem

if not isnull(rowsupdated) and rowsupdated > 0 then
	
	treeviewitem tvi_campo
	s_chiave_distinta is_appo
	
	tv_db.getitem( il_handle, tvi_campo)
	is_chiave_distinta = tvi_campo.data
	
else
	
	is_chiave_distinta = is_appo
	
end if
end event

event buttonclicked;call super::buttonclicked;if row>0 then
else
	return
end if


string				ls_messaggio, ls_colonna, ls_where

if isvalid(dwo) then
	choose case dwo.name
		//-----------------------------------------------------------------------------------------------------------------------------
		case "b_formule_legami"
			treeviewitem tvi_campo
			long ll_risposta, ll_line
			string ls_cod_prodotto_figlio, ls_cod_prodotto_padre, ls_cod_versione_padre, ls_cod_versione_figlio
			integer li_risposta
			s_chiave_distinta l_chiave_distinta
			
			
			li_risposta = tv_db.GetItem ( il_handle, tvi_campo )
			
			if li_risposta = -1 then
				g_mb.warning("Selezionare almeno un elemento dalla struttura ad albero!")
				return
			end if 
			
			tv_db.GetItem ( il_handle, tvi_campo )
			
			l_chiave_distinta = tvi_campo.data
			
			if not isnull(l_chiave_distinta.cod_prodotto_padre) then
				if l_chiave_distinta.cod_prodotto_figlio <> "" then
				
				ll_line = dw_distinta_det.getrow()
				dw_distinta_det.accepttext()
				ls_cod_prodotto_padre = dw_distinta_det.getitemstring(ll_line,"cod_prodotto_padre")
				ls_cod_prodotto_figlio = dw_distinta_det.getitemstring(ll_line,"cod_prodotto_figlio")
				ls_cod_versione_padre = dw_distinta_det.getitemstring(ll_line,"cod_versione")
				
				s_cs_xx.parametri.parametro_s_2 = is_cod_prodotto_finito
				s_cs_xx.parametri.parametro_s_3 = is_cod_versione
				s_cs_xx.parametri.parametro_s_4 = ls_cod_prodotto_padre
				s_cs_xx.parametri.parametro_s_5 = ls_cod_versione_padre
				
				window_open(w_formule_legami, 0)
				end if
			end if			
		
		
		//-----------------------------------------------------------------------------------------------------------------------------
		case "b_ricerca_figlio"
			dw_distinta_det.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(pcca.window_currentdw, "cod_prodotto_figlio")
		
		
		//-----------------------------------------------------------------------------------------------------------------------------
		case "b_modifica_testo_formula", "b_modifica_testo_formula_pfiglio", "b_modifica_testo_formula_vfiglio", "b_modifica_testo_formula_qtec"
			
			string ls_cod_formula, ls_testo_formula, ls_testo_des_formula
			
			choose case dwo.name
				case "b_modifica_testo_formula_pfiglio"
					ls_colonna = "cod_formula_prod_figlio"
					ls_messaggio = "Codice Formula Prodotto Figlio"
				
				case "b_modifica_testo_formula_vfiglio"
					ls_colonna = "cod_formula_vers_figlio"
					ls_messaggio = "Codice Formula Versione Figlio"
					
				case "b_modifica_testo_formula_qtec"
					ls_colonna = "cod_formula_quan_tecnica"
					ls_messaggio = "Codice Formula Quantità Tecnica"
					
				case else		//b_modifica_testo_formula (sulla q.tà utilizzo)
					ls_colonna = "cod_formula_quan_utilizzo"
					ls_messaggio = "Codice Formula Quantità Utilizzo"
					
			end choose
			
			ls_cod_formula = dw_distinta_det.getitemstring(dw_distinta_det.getrow(), ls_colonna)
			if isnull(ls_cod_formula) or ls_cod_formula = "" then
				g_mb.messagebox(ls_messaggio, "Definire la Formula da Modificare!")
				return
			end if
				
			select testo_formula,
					 testo_des_formula
			 into  :ls_testo_formula,
					 :ls_testo_des_formula
			 from  tab_formule_db
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_formula = :ls_cod_formula;
			
			if sqlca.sqlcode = -1 then
				g_mb.error(ls_messaggio, "Errore durante l'Estrazione Formule!")
				return
			end if
			
			s_cs_xx.parametri.parametro_s_15 = ls_testo_formula
			s_cs_xx.parametri.parametro_s_14 = ls_testo_des_formula
			s_cs_xx.parametri.parametro_s_12 = is_cod_prodotto_finito
			window_open(w_gen_formule_x_db, 0)
			
			
			if s_cs_xx.parametri.parametro_s_15 <> "%" then //Non ho premuto su Annulla, quindi Aggiorno il campo Testo_Formula
			
				update tab_formule_db
				  set testo_formula = :s_cs_xx.parametri.parametro_s_15,
						testo_des_formula = :s_cs_xx.parametri.parametro_s_14
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_formula = :ls_cod_formula;
				
				if sqlca.sqlcode = -1 then
					g_mb.error(ls_messaggio, "Errore durante l'Aggiornamento Formule!")
					rollback;
					return
				end if
				
				//dw_distinta_det.object.cf_testo_formula.text = s_cs_xx.parametri.parametro_s_13
			end if
			
			setnull(s_cs_xx.parametri.parametro_s_15) // testo_codici_variabili
			setnull(s_cs_xx.parametri.parametro_s_14) // testo_campi_database
			setnull(s_cs_xx.parametri.parametro_s_13) // testo_des_variabili
			setnull(s_cs_xx.parametri.parametro_s_12) // cod_prodotto			
			
			
		//-----------------------------------------------------------------------------------------------------------------------------
		case "b_ricerca_formule_db","b_ricerca_formule_db_pfiglio","b_ricerca_formule_db_vfiglio","b_ricerca_formule_db_qtec"
			dw_distinta_det.change_dw_current()
			
			choose case dwo.name
				case "b_ricerca_formule_db_pfiglio"
					ls_colonna = "cod_formula_prod_figlio"
				
				case "b_ricerca_formule_db_vfiglio"
					ls_colonna = "cod_formula_vers_figlio"
					
				case "b_ricerca_formule_db_qtec"
					ls_colonna = "cod_formula_quan_tecnica"
					
				case else		//b_ricerca_formule_db (sulla q.tà utilizzo)
					ls_colonna = "cod_formula_quan_utilizzo"
					
			end choose
			
			guo_ricerca.uof_ricerca_formule_db(pcca.window_currentdw, ls_colonna)
			
			
		//-----------------------------------------------------------------------------------------------------------------------------
		case "b_inserisci_formula","b_inserisci_formula_pfiglio","b_inserisci_formula_vfiglio","b_inserisci_formula_qtec"
			s_cs_xx.parametri.parametro_s_1 = "nuovo"
			s_cs_xx.parametri.parametro_s_12 = is_cod_prodotto_finito
			
			choose case dwo.name
				case "b_inserisci_formula_pfiglio"
					ls_colonna = "cod_formula_prod_figlio"
					ls_where = " and flag_tipo='2' "
				
				case "b_inserisci_formula_vfiglio"
					ls_colonna = "cod_formula_vers_figlio"
					ls_where = " and flag_tipo='2' "
					
				case "b_inserisci_formula_qtec"
					ls_colonna = "cod_formula_quan_tecnica"
					ls_where = " and flag_tipo='1' "
					
				case else		//b_inserisci_formula (sulla q.tà utilizzo)
					ls_colonna = "cod_formula_quan_utilizzo"
					ls_where = " and flag_tipo='1' "
					
			end choose
			
			window_open(w_tab_formule_db, 0)
			
			f_PO_LoadDDDW_DW(dw_distinta_det, ls_colonna, sqlca,&
								  "tab_formule_db","cod_formula","des_formula", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
								  "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) "+&
								  ls_where)	
								  
								  
	end choose
end if
end event

type dw_fasi_lavorazione from uo_cs_xx_dw within w_distinta_base
event ue_getfocus_sfrido ( )
event ue_prodotto ( )
event ue_getfocus_lavorazione ( )
event ue_lavorazioni ( )
event ue_stabilimenti_produzione ( string fs_azione )
integer x = 2194
integer y = 380
integer width = 2331
integer height = 2280
integer taborder = 230
boolean bringtotop = true
string dataobject = "d_fasi_lavorazione"
boolean border = false
end type

event ue_getfocus_sfrido();//dw_fasi_lavorazione.change_dw_current()
//s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//s_cs_xx.parametri.parametro_s_1 = "cod_prodotto_sfrido"
//
//triggerevent("ue_prodotto")

guo_ricerca.uof_ricerca_prodotto(dw_fasi_lavorazione,"cod_prodotto_sfrido")
end event

event ue_prodotto();//if not isvalid(w_prodotti_ricerca) then
//   window_open(w_prodotti_ricerca, 0)
//end if
//
//w_prodotti_ricerca.show()
end event

event ue_getfocus_lavorazione();dw_fasi_lavorazione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_lavorazione"

triggerevent("ue_lavorazioni")
end event

event ue_lavorazioni();if not isvalid(w_lavorazioni_ricerca_response) then
   window_open(w_lavorazioni_ricerca_response, 0)
end if
end event

event ue_stabilimenti_produzione(string fs_azione);long		ll_row
string		ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_versione

ll_row = dw_fasi_lavorazione.getrow()
if ll_row>0 then
else
	return
end if

ls_cod_prodotto = dw_fasi_lavorazione.getitemstring(ll_row, "cod_prodotto")
ls_cod_reparto = dw_fasi_lavorazione.getitemstring(ll_row, "cod_reparto")
ls_cod_lavorazione = dw_fasi_lavorazione.getitemstring(ll_row, "cod_lavorazione")
ls_cod_versione = dw_fasi_lavorazione.getitemstring(ll_row, "cod_versione")
	
if 		ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) and &
		ls_cod_reparto<>"" and not isnull(ls_cod_reparto) and &
		ls_cod_lavorazione<>"" and not isnull(ls_cod_lavorazione) and &
		ls_cod_versione<>"" and not isnull(ls_cod_versione) 				then
else
	return
end if

//fs_azione = "stabilimenti"   "produzione"
choose case fs_azione
	case "stabilimenti"
		window_open_parm(w_tes_fasi_lavorazione_det, -1, dw_fasi_lavorazione)
		
	case "produzione"
		window_open_parm(w_tes_fasi_lavorazione_prod, -1, dw_fasi_lavorazione)
		
end choose
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda,l_chiave_distinta.cod_prodotto_figlio,l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "cod_prodotto", l_chiave_distinta.cod_prodotto_figlio)
		SetItem(l_Idx, "cod_versione", l_chiave_distinta.cod_versione_figlio)
   END IF
NEXT
end event

event pcd_delete;call super::pcd_delete;if i_extendmode then
	triggerevent("pcd_save")
	dw_fasi_lavorazione.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	dw_fasi_lavorazione.set_dw_options(sqlca,pcca.null_object,c_nonew,c_default)

end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then

	object.b_lavorazione.enabled = true
	object.b_sfrido.enabled = true

	object.b_stabilimenti.enabled = false
	object.b_reparti.enabled = false
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then

	object.b_lavorazione.enabled = true
	object.b_sfrido.enabled = true
	
	object.b_stabilimenti.enabled = false
	object.b_reparti.enabled = false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then

	object.b_lavorazione.enabled = false
	object.b_sfrido.enabled = false

	object.b_stabilimenti.enabled = true
	object.b_reparti.enabled = true
end if
end event

event itemchanged;call super::itemchanged;string ls_null

setnull(ls_null)

if row>0 then
else
	return
end if

choose case dwo.name
	case "flag_stampa_sempre"
//		if data="S" then
//			//annulla deposito origine
//			setitem(row, "cod_deposito_origine", ls_null)
//		else
//			//annulla selezione stampa multistabilimento
//			setitem(row, "flag_stampa_sempre", "N")
//		end if
	
	case "cod_deposito_origine"
//		if data<>"" and not isnull(data) then
//			//annulla selezione stampa multistabilimento
//			setitem(row, "flag_stampa_sempre", "N")
//		else
//			//imposta selezione stampa multistabilimento
//			setitem(row, "flag_stampa_sempre", "S")
//		end if
	
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_sfrido"
		triggerevent("ue_getfocus_sfrido")
		
	case "b_lavorazione"
		triggerevent("ue_getfocus_lavorazione")
		
	case "b_stabilimenti"
		event trigger ue_stabilimenti_produzione("stabilimenti")
		
	case "b_reparti"
		event trigger ue_stabilimenti_produzione("produzione")
		
end choose
end event

