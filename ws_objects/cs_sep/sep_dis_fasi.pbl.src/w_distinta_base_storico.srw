﻿$PBExportHeader$w_distinta_base_storico.srw
$PBExportComments$Finestra per la gestione Distinta Base storico
forward
global type w_distinta_base_storico from w_cs_xx_principale
end type
type cb_comprimi from commandbutton within w_distinta_base_storico
end type
type cb_espandi from commandbutton within w_distinta_base_storico
end type
type st_2 from statictext within w_distinta_base_storico
end type
type st_cod_prodotto from statictext within w_distinta_base_storico
end type
type st_4 from statictext within w_distinta_base_storico
end type
type st_cod_versione from statictext within w_distinta_base_storico
end type
type dw_distinta_storico_det from uo_cs_xx_dw within w_distinta_base_storico
end type
type dw_folder from u_folder within w_distinta_base_storico
end type
type tv_db from treeview within w_distinta_base_storico
end type
type s_chiave_distinta from structure within w_distinta_base_storico
end type
end forward

type s_chiave_distinta from structure
	string		cod_prodotto_padre
	long		num_sequenza
	string		cod_prodotto_figlio
	double		quan_utilizzo
	long		prog_storico
end type

global type w_distinta_base_storico from w_cs_xx_principale
integer width = 3328
integer height = 2160
string title = "Storico Distinta Base"
cb_comprimi cb_comprimi
cb_espandi cb_espandi
st_2 st_2
st_cod_prodotto st_cod_prodotto
st_4 st_4
st_cod_versione st_cod_versione
dw_distinta_storico_det dw_distinta_storico_det
dw_folder dw_folder
tv_db tv_db
end type
global w_distinta_base_storico w_distinta_base_storico

type variables
long il_handle, il_prog_storico
string is_cod_versione, is_cod_prodotto_finito


end variables

forward prototypes
public function integer wf_inizio ()
public function integer wf_visual_testo_formula (string fs_cod_formula)
public function integer wf_trova_prodotto (string fs_cod_prodotto_padre, string fs_cod_prodotto_inserito, string fs_cod_versione, ref string fs_messaggio, ref string fs_errore)
public function integer wf_imposta_tv (string fs_cod_prodotto, integer fi_num_livello_cor, long fl_handle, long fl_prog_storico, ref string fs_errore)
end prototypes

public function integer wf_inizio ();string       ls_errore,ls_des_prodotto

long         ll_risposta

treeviewitem tvi_campo

is_cod_versione = s_cs_xx.parametri.parametro_dw_2.getitemstring(s_cs_xx.parametri.parametro_dw_2.getrow(),"cod_versione")

is_cod_prodotto_finito = s_cs_xx.parametri.parametro_dw_2.getitemstring(s_cs_xx.parametri.parametro_dw_2.getrow(),"cod_prodotto")

il_prog_storico = s_cs_xx.parametri.parametro_dw_2.getitemnumber(s_cs_xx.parametri.parametro_dw_2.getrow(),"prog_storico")

st_cod_prodotto.text = is_cod_prodotto_finito

st_cod_versione.text = is_cod_versione

s_chiave_distinta l_chiave_distinta

l_chiave_distinta.cod_prodotto_padre = is_cod_prodotto_finito

l_chiave_distinta.cod_prodotto_figlio = ""

l_chiave_distinta.prog_storico = il_prog_storico

tv_db.setredraw(false)

tv_db.deleteitem(0)

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_prodotto = :l_chiave_distinta.cod_prodotto_padre;

ls_des_prodotto = trim(ls_des_prodotto)

tvi_campo.itemhandle = 1

tvi_campo.data = l_chiave_distinta

tvi_campo.label = l_chiave_distinta.cod_prodotto_padre + "," + ls_des_prodotto

tvi_campo.pictureindex = 1

tvi_campo.selectedpictureindex = 1

tvi_campo.overlaypictureindex = 1

ll_risposta = tv_db.insertitemlast(0, tvi_campo)

wf_imposta_tv( l_chiave_distinta.cod_prodotto_padre, 1, 1, il_prog_storico, ls_errore)

tv_db.expandall(1)

tv_db.setredraw(true)

tv_db.triggerevent("pcd_clicked")

return 0
end function

public function integer wf_visual_testo_formula (string fs_cod_formula);string ls_testo_formula

select testo_formula
 into  :ls_testo_formula
 from  tab_formule_db
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_formula = :fs_cod_formula;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Codice Formula Utilizzo", "Errore durante l'Estrazione Formule!")
	return -1
end if

dw_distinta_storico_det.object.cf_testo_formula.text = f_sost_des_var_in_formula(ls_testo_formula, is_cod_prodotto_finito)
return 0
end function

public function integer wf_trova_prodotto (string fs_cod_prodotto_padre, string fs_cod_prodotto_inserito, string fs_cod_versione, ref string fs_messaggio, ref string fs_errore);string    ls_cod_prodotto_figlio,  ls_test_prodotto_f, ls_errore, &
			 ls_flag_materia_prima,ls_test,ls_messaggio
long      ll_num_figli,ll_num_righe
integer   li_risposta

datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto_padre,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe
	ls_cod_prodotto_figlio=lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	
	if ls_cod_prodotto_figlio = upper(fs_cod_prodotto_inserito) or ls_cod_prodotto_figlio = lower(fs_cod_prodotto_inserito) then
		fs_messaggio="Attenzione! Prodotto già presente in questo ramo!"
		return 1
	end if
	
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:ls_cod_prodotto_figlio
	and    cod_versione=:fs_cod_versione;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		continue
	else
		li_risposta=wf_trova_prodotto(ls_cod_prodotto_figlio,fs_cod_prodotto_inserito,fs_cod_versione, ls_messaggio,ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			destroy lds_righe_distinta
			return -1
		end if
		if li_risposta = 1 then
			fs_messaggio = ls_messaggio
			destroy lds_righe_distinta
			return 1
		end if
	end if

next

destroy lds_righe_distinta

return 0
end function

public function integer wf_imposta_tv (string fs_cod_prodotto, integer fi_num_livello_cor, long fl_handle, long fl_prog_storico, ref string fs_errore);//	Funzione che imposta l'outliner
//
// nome: wf_imposta_ol
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//	Variabili passate: 		nome					 	tipo				passaggio per			commento
//							
//							 fs_cod_prodotto					string				valore
//							 fs_cod_prodotto_finito			string		  		valore             
//							 fi_num_livello_cor				integer				valore
//							 fl_handle							long   				valore
//							 fs_errore							string				riferimento
//
//		Creata il 12-05-97 
//		Autore Diego Ferrari

string  ls_cod_prodotto,ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_versione,ls_flag_materia_prima,ls_cod_misura_mag

long    ll_num_figli,ll_handle,ll_num_righe

integer li_num_priorita,li_risposta

s_chiave_distinta l_chiave_distinta

treeviewitem tvi_campo

datastore lds_righe_distinta

ll_num_figli = 1

ls_cod_versione = is_cod_versione

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta_storico"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve( s_cs_xx.cod_azienda, fs_cod_prodotto, ls_cod_versione, il_prog_storico)
	
for ll_num_figli = 1 to ll_num_righe

	l_chiave_distinta.cod_prodotto_figlio=lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	
	l_chiave_distinta.quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	
	l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	
	l_chiave_distinta.cod_prodotto_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_padre")
	
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")

   select des_prodotto,
			 cod_misura_mag
	into   :ls_des_prodotto,
			 :ls_cod_misura_mag
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:l_chiave_distinta.cod_prodotto_figlio;
	
	ls_des_prodotto=trim(ls_des_prodotto)

	if isnull(ls_cod_misura_mag) then ls_cod_misura_mag = ""
	
	tvi_campo.itemhandle = fl_handle
	
	tvi_campo.data = l_chiave_distinta
	
	tvi_campo.label = l_chiave_distinta.cod_prodotto_figlio + "," + ls_des_prodotto + " " + string(l_chiave_distinta.quan_utilizzo) + " " + ls_cod_misura_mag

	if ls_flag_materia_prima = 'N' then
		
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta_storico
		where  cod_azienda = :s_cs_xx.cod_azienda	and	 
		       cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and    
				 cod_versione = :ls_cod_versione and 
				 prog_storico = :il_prog_storico;

		if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
			
			tvi_campo.pictureindex = 3
			
			tvi_campo.selectedpictureindex = 3
			
			tvi_campo.overlaypictureindex = 3
			
			ll_handle = tv_db.insertitemlast(fl_handle, tvi_campo)
			
			continue
			
		else
			
			tvi_campo.pictureindex = 2
			
			tvi_campo.selectedpictureindex = 2
			
			tvi_campo.overlaypictureindex = 2
			
			ll_handle = tv_db.insertitemlast(fl_handle, tvi_campo)
			
			setnull(ls_test_prodotto_f)
			
			li_risposta = wf_imposta_tv( l_chiave_distinta.cod_prodotto_figlio, fi_num_livello_cor + 1, ll_handle, il_prog_storico, ls_errore)
			
		end if
		
	else
		
		tvi_campo.pictureindex = 3
		
		tvi_campo.selectedpictureindex = 3
		
		tvi_campo.overlaypictureindex = 3
		
		ll_handle = tv_db.insertitemlast(fl_handle, tvi_campo)
		
		continue
		
	end if
	
next

destroy(lds_righe_distinta)

return 0
end function

on w_distinta_base_storico.create
int iCurrent
call super::create
this.cb_comprimi=create cb_comprimi
this.cb_espandi=create cb_espandi
this.st_2=create st_2
this.st_cod_prodotto=create st_cod_prodotto
this.st_4=create st_4
this.st_cod_versione=create st_cod_versione
this.dw_distinta_storico_det=create dw_distinta_storico_det
this.dw_folder=create dw_folder
this.tv_db=create tv_db
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_comprimi
this.Control[iCurrent+2]=this.cb_espandi
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.st_cod_prodotto
this.Control[iCurrent+5]=this.st_4
this.Control[iCurrent+6]=this.st_cod_versione
this.Control[iCurrent+7]=this.dw_distinta_storico_det
this.Control[iCurrent+8]=this.dw_folder
this.Control[iCurrent+9]=this.tv_db
end on

on w_distinta_base_storico.destroy
call super::destroy
destroy(this.cb_comprimi)
destroy(this.cb_espandi)
destroy(this.st_2)
destroy(this.st_cod_prodotto)
destroy(this.st_4)
destroy(this.st_cod_versione)
destroy(this.dw_distinta_storico_det)
destroy(this.dw_folder)
destroy(this.tv_db)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects_1[]
windowobject l_objects_2[]
windowobject l_objects_3[]
windowobject l_objects_4[]


l_objects_1[1] = tv_db
l_objects_1[2] = cb_comprimi
l_objects_1[3] = cb_espandi
dw_folder.fu_AssignTab(1, "&Struttura", l_Objects_1[])

l_objects_2[1] = dw_distinta_storico_det
dw_folder.fu_AssignTab(2, "&Dettaglio", l_Objects_2[])

			 
dw_distinta_storico_det.set_dw_options( sqlca, &
                                        pcca.null_object, &
													 c_noretrieveonopen + c_disableCC + c_disableCCinsert + c_NoNew + c_NoModify + c_NoDelete, &
													 c_default)													 
dw_distinta_storico_det.ib_proteggi_chiavi = false

uo_dw_main = dw_distinta_storico_det


dw_folder.fu_FolderCreate(2,2)
dw_folder.fu_SelectTab(1)


end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_distinta_storico_det,"cod_formula_quan_utilizzo",sqlca,&
                 "tab_formule_db","cod_formula","des_formula", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

type cb_comprimi from commandbutton within w_distinta_base_storico
integer x = 2857
integer y = 1820
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Comprimi"
end type

event clicked;long ll_tvi

ll_tvi = tv_db.FindItem(RootTreeItem! , 0)
tv_db.CollapseItem (ll_tvi)
end event

type cb_espandi from commandbutton within w_distinta_base_storico
integer x = 2857
integer y = 1920
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Espandi"
end type

event clicked;tv_db.ExpandItem ( il_handle )
end event

type st_2 from statictext within w_distinta_base_storico
integer x = 23
integer y = 20
integer width = 1074
integer height = 80
boolean bringtotop = true
integer textsize = -11
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Distinta Base del prodotto finito:"
boolean focusrectangle = false
end type

type st_cod_prodotto from statictext within w_distinta_base_storico
integer x = 1097
integer y = 20
integer width = 1074
integer height = 80
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type st_4 from statictext within w_distinta_base_storico
integer x = 2194
integer y = 20
integer width = 366
integer height = 80
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Versione:"
boolean focusrectangle = false
end type

type st_cod_versione from statictext within w_distinta_base_storico
integer x = 2560
integer y = 20
integer width = 247
integer height = 80
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type dw_distinta_storico_det from uo_cs_xx_dw within w_distinta_base_storico
integer x = 91
integer y = 220
integer width = 2354
integer height = 1360
integer taborder = 180
string dataobject = "d_distinta_storico_det"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
                    l_chiave_distinta.cod_prodotto_padre, & 
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  is_cod_versione, &
						  il_prog_storico)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_saveafter;call super::pcd_saveafter;//if i_extendmode then
//	long ll_risposta
//
//	if not isvalid(w_distinte_taglio) then	ll_risposta=wf_inizio()	
//
//end if
end event

event pcd_setkey;call super::pcd_setkey;//LONG  l_Idx
//
//FOR l_Idx = 1 TO RowCount()
//   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
//      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)	
//		SetItem(l_Idx, "cod_versione", is_cod_versione)	
//   END IF
//NEXT
//
end event

event pcd_view;call super::pcd_view;//if i_extendmode then
//   cb_ricerca_figlio_dist.enabled=false	
//	cb_inserisci.enabled = false
//	cb_modifica_testo_formula.enabled = false
//	cb_inserisci_formula.enabled = false
//end if
end event

event pcd_save;call super::pcd_save;//if i_extendmode then
//	integer li_risposta
//	string ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_messaggio,ls_errore
//
//
//	ls_cod_prodotto_padre = getitemstring(getrow(),"cod_prodotto_padre")
//	ls_cod_prodotto_figlio = getitemstring(getrow(),"cod_prodotto_figlio")
//	ls_cod_versione = getitemstring(getrow(),"cod_versione")
//	
//	li_risposta = wf_trova_prodotto(ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_messaggio,ls_errore)
//	
//	if li_risposta = 1 then messagebox("SEP",ls_messaggio,information!)
//
//	if li_risposta = -1 then
//		messagebox("SEP",ls_errore,stopsign!)
//		return 
//	end if
//
//end if
end event

type dw_folder from u_folder within w_distinta_base_storico
integer y = 100
integer width = 3269
integer height = 1940
integer taborder = 120
boolean border = false
end type

event po_tabclicked;call super::po_tabclicked;choose case i_selectedtab
	case 1
		tv_db.setfocus()
		tv_db.triggerevent("clicked")

	case 2		
		wf_visual_testo_formula(dw_distinta_storico_det.getitemstring(dw_distinta_storico_det.getrow(), "cod_formula_quan_utilizzo"))

end choose
end event

type tv_db from treeview within w_distinta_base_storico
integer x = 91
integer y = 300
integer width = 2743
integer height = 1720
integer taborder = 190
string dragicon = "Exclamation!"
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean border = false
boolean disabledragdrop = false
long picturemaskcolor = 553648127
long statepicturemaskcolor = 553648127
end type

event constructor;this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "pf.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "semil.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "mp.bmp")

end event

event clicked;if handle<>0 then
	il_handle = handle
	dw_distinta_storico_det.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
end if
end event

event key;//
//if key=keydelete! then
//	if messagebox("sep","Sei sicuro di voler cancellare il ramo di distinta?",Question!,yesno!,2) = 1 then
//
//		s_chiave_distinta l_chiave_distinta
//		treeviewitem tvi_campo
//		long ll_risposta
//		
//		tv_db.GetItem ( il_handle, tvi_campo )
//		l_chiave_distinta = tvi_campo.data
//		
//		delete from distinta
//		where cod_azienda=:s_cs_xx.cod_azienda
//		and   cod_prodotto_padre=:l_chiave_distinta.cod_prodotto_padre
//		and   num_sequenza=:l_chiave_distinta.num_sequenza
//		and   cod_prodotto_figlio=:l_chiave_distinta.cod_prodotto_figlio
//		and   cod_versione=:is_cod_versione;
//		
//		if sqlca.sqlcode < 0 then
//			messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
//			return
//		end if
//		
//		commit;
//		
//		ll_risposta=wf_inizio()	
//
//	end if
//end if
end event

event dragwithin;//TreeViewItem		ltvi_Over
//
//If GetItem(handle, ltvi_Over) = -1 Then
//	SetDropHighlight(0)
//
//	Return
//End If
//
//
//SetDropHighlight(handle)
//
//il_handle = handle
end event

event dragdrop;//s_chiave_distinta l_chiave_distinta
//treeviewitem tvi_campo
//long ll_risposta,ll_num_sequenza
//double ldd_quan_utilizzo,ldd_quan_tecnica
//string ls_cod_formula_quan_utilizzo,ls_cod_prodotto_figlio,ls_cod_misura,ls_flag_materia_prima,ls_flag_escludibile, & 
//		 ls_cod_prodotto_padre,ls_messaggio,ls_errore
//integer li_risposta
//
//GetItem (il_handle, tvi_campo )
//l_chiave_distinta = tvi_campo.data
//ll_num_sequenza = long(em_num_sequenza.text)
//
//ls_cod_prodotto_figlio = dw_ricerca.getitemstring(dw_ricerca.getrow(), "rs_cod_prodotto")
//
//
//
//if isnull(ls_cod_prodotto_figlio) then
//	 messagebox("Sep","Attenzione: non hai selezionato alcun prodotto da inserire in distinta!",stopsign!)
//	 return
//end if
//	
//if l_chiave_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio   then
//	 messagebox("Sep","Attenzione: padre e figlio non possono coincidere!")
//	 return
//end if
//
//SELECT cod_misura_mag,
//		 flag_escludibile,
//		 flag_materia_prima
//INTO   :ls_cod_misura,
//		 :ls_flag_escludibile,
//		 :ls_flag_materia_prima
//FROM   anag_prodotti  
//WHERE  cod_azienda = :s_cs_xx.cod_azienda 
//AND    cod_prodotto = :ls_cod_prodotto_figlio;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
//	return
//end if
//
//if isnull(ls_flag_escludibile) then ls_flag_escludibile =  'N'
//if isnull(ls_flag_materia_prima) then ls_flag_materia_prima =  'N'
//
//if ll_num_sequenza = 0 then
//	select max(distinta.num_sequenza)
//	into   :ll_num_sequenza
//	from   distinta
//	where  distinta.cod_azienda = :s_cs_xx.cod_azienda and 
//			 distinta.cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio;
//	
//	if sqlca.sqlcode < 0 then
//		messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
//		return
//	end if
//	
//	if isnull(ll_num_sequenza) then ll_num_sequenza = 0
//	
//	ll_num_sequenza = ll_num_sequenza + 10
//end if
//
//wf_visual_testo_formula(ls_cod_formula_quan_utilizzo)
//
//if ls_cod_formula_quan_utilizzo = "" then setnull(ls_cod_formula_quan_utilizzo)
//
//ldd_quan_utilizzo = double(em_quan_utilizzo.text)
//ldd_quan_tecnica = double(em_quan_tecnica.text)
//
//if tvi_campo.level = 1 then
//	ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
//else
//	ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_figlio
//end if
//
//li_risposta = wf_trova_prodotto (ls_cod_prodotto_padre,ls_cod_prodotto_figlio,is_cod_versione,ls_messaggio,ls_errore)
//
//if li_risposta = 1 then messagebox("SEP",ls_messaggio,information!)
//
//if li_risposta = -1 then
//	messagebox("SEP",ls_errore,stopsign!)
//	return 
//end if
//
//insert into distinta
//(cod_azienda,
// cod_prodotto_padre,
// num_sequenza,
// cod_prodotto_figlio,
// cod_versione,
// cod_misura,
// quan_tecnica,
// quan_utilizzo,
// fase_ciclo,
// dim_x,
// dim_y,
// dim_z,
// dim_t,
// coef_calcolo,
// des_estesa,
// formula_tempo,
// cod_formula_quan_utilizzo,
// flag_escludibile,
// flag_materia_prima)
// values
// (:s_cs_xx.cod_azienda,
//  :ls_cod_prodotto_padre,
//  :ll_num_sequenza,
//  :ls_cod_prodotto_figlio,
//  :is_cod_versione,
//  :ls_cod_misura,
//  :ldd_quan_tecnica,
//  :ldd_quan_utilizzo,
//  null,
//  null,
//  null,
//  null,
//  null,
//  null,
//  null,
//  null,
//  :ls_cod_formula_quan_utilizzo,
//  :ls_flag_escludibile,
//  :ls_flag_materia_prima);
//
//if sqlca.sqlcode < 0 then
//	messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
//	return
//end if
//
//commit;
//
//ll_risposta=wf_inizio()	
end event

