﻿$PBExportHeader$w_distinta_padri.srw
$PBExportComments$Window distinta padri
forward
global type w_distinta_padri from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_distinta_padri
end type
type cb_storicizza from commandbutton within w_distinta_padri
end type
type cb_approva from commandbutton within w_distinta_padri
end type
type cb_componenti from commandbutton within w_distinta_padri
end type
type dw_distinta_padri_det from uo_cs_xx_dw within w_distinta_padri
end type
type cb_reset from commandbutton within w_distinta_padri
end type
type cb_ricerca from commandbutton within w_distinta_padri
end type
type dw_distinta_padri_lista from uo_cs_xx_dw within w_distinta_padri
end type
type cb_duplica from commandbutton within w_distinta_padri
end type
type cb_costo from commandbutton within w_distinta_padri
end type
type cb_stampa from commandbutton within w_distinta_padri
end type
type cbx_completa from checkbox within w_distinta_padri
end type
type st_1 from statictext within w_distinta_padri
end type
type sle_percorso from singlelineedit within w_distinta_padri
end type
type cb_sfoglia from commandbutton within w_distinta_padri
end type
type cbx_fasi from checkbox within w_distinta_padri
end type
type cbx_righe_lista from checkbox within w_distinta_padri
end type
type cb_report from commandbutton within w_distinta_padri
end type
type dw_ricerca from u_dw_search within w_distinta_padri
end type
end forward

global type w_distinta_padri from w_cs_xx_principale
integer width = 2807
integer height = 2136
string title = "Distinta Padri"
event ue_cerca_prodotto ( )
event ue_seleziona_versione ( )
cb_1 cb_1
cb_storicizza cb_storicizza
cb_approva cb_approva
cb_componenti cb_componenti
dw_distinta_padri_det dw_distinta_padri_det
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_distinta_padri_lista dw_distinta_padri_lista
cb_duplica cb_duplica
cb_costo cb_costo
cb_stampa cb_stampa
cbx_completa cbx_completa
st_1 st_1
sle_percorso sle_percorso
cb_sfoglia cb_sfoglia
cbx_fasi cbx_fasi
cbx_righe_lista cbx_righe_lista
cb_report cb_report
dw_ricerca dw_ricerca
end type
global w_distinta_padri w_distinta_padri

type variables
string		is_SPD = ""
end variables

forward prototypes
public function integer f_crea_file (string fs_cod_prodotto, string fs_cod_versione, long fl_num_livello_cor, ref string fs_errore)
end prototypes

event ue_cerca_prodotto();dw_ricerca.setitem( 1, "rs_cod_prodotto", s_cs_xx.parametri.parametro_s_1)
dw_ricerca.fu_buildsearch(TRUE)
dw_distinta_padri_lista.change_dw_current()
triggerevent("pc_retrieve")
postevent("ue_seleziona_versione")
end event

event ue_seleziona_versione();string ls_cod_versione
long ll_i

for ll_i = 1 to dw_distinta_padri_lista.rowcount()
	
	ls_cod_versione = dw_distinta_padri_lista.getitemstring( ll_i, "cod_versione")
	
	if not isnull(ls_cod_versione) and ls_cod_versione <> "" and ls_cod_versione = s_cs_xx.parametri.parametro_s_2 and not isnull(s_cs_xx.parametri.parametro_s_2) and s_cs_xx.parametri.parametro_s_2 <> "" then
		dw_distinta_padri_lista.setrow( ll_i)
		dw_distinta_padri_lista.scrolltorow( ll_i)
		dw_distinta_padri_lista.selectrow( ll_i, true)
	else
		dw_distinta_padri_lista.selectrow( ll_i, false)
	end if
	
next

cb_componenti.visible = false
end event

public function integer f_crea_file (string fs_cod_prodotto, string fs_cod_versione, long fl_num_livello_cor, ref string fs_errore);string  ls_cod_prodotto,ls_test_prodotto_f, ls_des_prodotto,ls_cod_versione,ls_flag_materia_prima,ls_cod_misura_mag,&
		  ls_cod_prodotto_figlio,ls_cod_prodotto_padre,ls_des_estesa_prodotto
long    ll_num_figli,ll_num_righe,ll_num_sequenza,ll_t
integer li_num_priorita,li_risposta,li_filenum
double  ldd_quan_utilizzo

datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio=lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	ls_cod_prodotto_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_padre")
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")

   select des_prodotto,
			 cod_misura_mag
	into   :ls_des_prodotto,
			 :ls_cod_misura_mag
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	end if

	ls_des_prodotto=trim(ls_des_prodotto)

	if isnull(ls_cod_misura_mag) then ls_cod_misura_mag = ""

	if ls_flag_materia_prima = 'S' then ls_cod_misura_mag = ls_cod_misura_mag + "(*)"
	
	if cbx_fasi.checked = true then
		select des_estesa_prodotto
		into   :ls_des_estesa_prodotto
		from   tes_fasi_lavorazione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto_figlio;
		
//		if sqlca.sqlcode < 0 then
//			messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//		end if

	end if
	
	li_FileNum = FileOpen(sle_percorso.text,LineMode!, Write!, LockWrite!, Append!)
	FileWrite(li_FileNum,space(4*fl_num_livello_cor) + ls_cod_prodotto_figlio + "," + ls_des_prodotto + " " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag )
	if cbx_fasi.checked = true and (ls_des_estesa_prodotto <> "" and not isnull(ls_des_estesa_prodotto)) then
		if len(ls_des_estesa_prodotto) > 40 then
			for ll_t = 0 to int(len(ls_des_estesa_prodotto)/40) - 1
				FileWrite(li_FileNum,space(4*fl_num_livello_cor) + ">>>" + mid(ls_des_estesa_prodotto,ll_t*40 + 1,40))
			next
		else
			FileWrite(li_FileNum,space(4*fl_num_livello_cor) + ">>>" + ls_des_estesa_prodotto)
		end if
	end if 
	fileclose(li_filenum)

	if ls_flag_materia_prima = 'N' then
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta
		where  cod_azienda=:s_cs_xx.cod_azienda
		and	 cod_prodotto_padre=:ls_cod_prodotto_figlio;

		if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
			continue
		else
			li_risposta=f_crea_file(ls_cod_prodotto_figlio, fs_cod_versione,fl_num_livello_cor + 1,fs_errore)

		end if
	else
		if cbx_completa.checked = true then
			select cod_prodotto_figlio 
			into   :ls_test_prodotto_f
			from   distinta
			where  cod_azienda=:s_cs_xx.cod_azienda
			and	 cod_prodotto_padre=:ls_cod_prodotto_figlio;
	
			if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
				continue
			else
				li_risposta=f_crea_file(ls_cod_prodotto_figlio, fs_cod_versione,fl_num_livello_cor + 1,fs_errore)

			end if
		else
			continue
		end if
		
	end if

next

destroy(lds_righe_distinta)

return 0
end function

on w_distinta_padri.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_storicizza=create cb_storicizza
this.cb_approva=create cb_approva
this.cb_componenti=create cb_componenti
this.dw_distinta_padri_det=create dw_distinta_padri_det
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_distinta_padri_lista=create dw_distinta_padri_lista
this.cb_duplica=create cb_duplica
this.cb_costo=create cb_costo
this.cb_stampa=create cb_stampa
this.cbx_completa=create cbx_completa
this.st_1=create st_1
this.sle_percorso=create sle_percorso
this.cb_sfoglia=create cb_sfoglia
this.cbx_fasi=create cbx_fasi
this.cbx_righe_lista=create cbx_righe_lista
this.cb_report=create cb_report
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_storicizza
this.Control[iCurrent+3]=this.cb_approva
this.Control[iCurrent+4]=this.cb_componenti
this.Control[iCurrent+5]=this.dw_distinta_padri_det
this.Control[iCurrent+6]=this.cb_reset
this.Control[iCurrent+7]=this.cb_ricerca
this.Control[iCurrent+8]=this.dw_distinta_padri_lista
this.Control[iCurrent+9]=this.cb_duplica
this.Control[iCurrent+10]=this.cb_costo
this.Control[iCurrent+11]=this.cb_stampa
this.Control[iCurrent+12]=this.cbx_completa
this.Control[iCurrent+13]=this.st_1
this.Control[iCurrent+14]=this.sle_percorso
this.Control[iCurrent+15]=this.cb_sfoglia
this.Control[iCurrent+16]=this.cbx_fasi
this.Control[iCurrent+17]=this.cbx_righe_lista
this.Control[iCurrent+18]=this.cb_report
this.Control[iCurrent+19]=this.dw_ricerca
end on

on w_distinta_padri.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_storicizza)
destroy(this.cb_approva)
destroy(this.cb_componenti)
destroy(this.dw_distinta_padri_det)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_distinta_padri_lista)
destroy(this.cb_duplica)
destroy(this.cb_costo)
destroy(this.cb_stampa)
destroy(this.cbx_completa)
destroy(this.st_1)
destroy(this.sle_percorso)
destroy(this.cb_sfoglia)
destroy(this.cbx_fasi)
destroy(this.cbx_righe_lista)
destroy(this.cb_report)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[],ls_test

boolean	lb_SPD


dw_distinta_padri_lista.set_dw_key("cod_azienda")

dw_distinta_padri_lista.set_dw_options(sqlca,pcca.null_object, & 
													c_noretrieveonopen,c_default)

dw_distinta_padri_det.set_dw_options(sqlca,dw_distinta_padri_lista, & 
												 c_sharedata + c_scrollparent,c_default)

iuo_dw_main = dw_distinta_padri_lista



l_criteriacolumn[1] = "rs_cod_prodotto"
l_criteriacolumn[2] = "cod_versione"
l_criteriacolumn[3] = "flag_default"
l_criteriacolumn[4] = "flag_default_ipertech"
l_searchtable[1] = "distinta_padri"
l_searchtable[2] = "distinta_padri"
l_searchtable[3] = "distinta_padri"
l_searchtable[4] = "distinta_padri"
l_searchcolumn[1] = "cod_prodotto"
l_searchcolumn[2] = "cod_versione"
l_searchcolumn[3] = "flag_default"
l_searchcolumn[4] = "flag_default_ipertech"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_distinta_padri_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)

dw_distinta_padri_lista.change_dw_current()

select stringa
into   :ls_test
from   parametri
where  flag_parametro='S'
and    cod_parametro='PDB';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
end if

guo_functions.uof_get_parametro_azienda("SPD", lb_SPD)

if lb_SPD then
	is_SPD = "S"
	dw_distinta_padri_lista.object.cf_associato.visible = true
else
	is_SPD = "N"
	dw_distinta_padri_lista.object.cf_associato.visible = false
end if
	
sle_percorso.text = ls_test
end event

event close;call super::close;if isvalid(w_distinta_base) then
	close(w_distinta_base)
end if
end event

type cb_1 from commandbutton within w_distinta_padri
integer x = 1179
integer y = 1928
integer width = 411
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica su DB"
end type

event clicked;window_open(w_duplica_distinta_altro_db,-1)
end event

type cb_storicizza from commandbutton within w_distinta_padri
integer x = 2373
integer y = 1828
integer width = 366
integer height = 80
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Storicizza"
end type

event clicked;long ll_riga

string ls_cod_prodotto, ls_cod_versione

ll_riga = dw_distinta_padri_lista.getrow()

if isnull(ll_riga) or ll_riga < 1 then return -1

ls_cod_prodotto = dw_distinta_padri_lista.getitemstring(dw_distinta_padri_lista.getrow(),"cod_prodotto")

ls_cod_versione = dw_distinta_padri_lista.getitemstring(dw_distinta_padri_lista.getrow(),"cod_versione")

if isnull(ls_cod_prodotto) or isnull(ls_cod_versione) or ls_cod_prodotto="" or ls_cod_versione = "" then
	
	g_mb.messagebox( "Sep", "Non è possibile storicizzare la distinta poichè non è selezionato alcun prodotto!", stopsign!)
	
	return -1
	
end if

setnull(s_cs_xx.parametri.parametro_s_1)

setnull(s_cs_xx.parametri.parametro_s_2)

s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto

s_cs_xx.parametri.parametro_s_2 = ls_cod_versione

window_open(w_storicizza_distinta,-1)
end event

type cb_approva from commandbutton within w_distinta_padri
integer x = 1595
integer y = 1928
integer width = 366
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Approva"
end type

type cb_componenti from commandbutton within w_distinta_padri
integer x = 2373
integer y = 1928
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Componenti"
end type

event clicked;long ll_risposta

s_cs_xx.parametri.parametro_dw_2 = dw_distinta_padri_det

window_open_parm(w_distinta_base,-1,dw_distinta_padri_lista)

if isvalid(w_distinta_base) then
	ll_risposta=w_distinta_base.wf_inizio()
	w_distinta_base.tv_db.SelectItem (1)
end if

end event

type dw_distinta_padri_det from uo_cs_xx_dw within w_distinta_padri
integer x = 23
integer y = 916
integer width = 2720
integer height = 784
integer taborder = 140
string dataobject = "d_distinta_padri_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_distinta_padri_det,"cod_prodotto")
end choose
end event

type cb_reset from commandbutton within w_distinta_padri
integer x = 2336
integer y = 232
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_buildsearch(TRUE)
dw_distinta_padri_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_ricerca from commandbutton within w_distinta_padri
integer x = 1947
integer y = 232
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_reset()



end event

type dw_distinta_padri_lista from uo_cs_xx_dw within w_distinta_padri
integer x = 27
integer y = 360
integer width = 2711
integer height = 540
integer taborder = 130
string dataobject = "d_distinta_padri_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "creato_da", s_cs_xx.cod_utente)
		SetItem(l_Idx, "data_creazione", datetime(today()))
   END IF
NEXT

end event

event pcd_new;call super::pcd_new;if i_extendmode then
	dw_distinta_padri_det.object.b_ricerca_prodotto.enabled=true
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
//	cb_ric_prod.enabled=false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	dw_distinta_padri_det.object.b_ricerca_prodotto.enabled=false
end if
end event

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then
	string ls_cod_prodotto,ls_cod_versione,ls_cod_versione_esistente,ls_test,ls_test_padre
	boolean lb_flag_ok

	ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
	ls_cod_versione = getitemstring(getrow(),"cod_versione")	

	setnull(ls_test_padre)
	
	select cod_azienda
	into   :ls_test_padre
	from   distinta_padri
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;              //verifico se il prodotto è già prodotto finito
	
	if isnull(ls_test_padre) then // se il prodotto non è un PF allora verifico se è figlio in distinta
		
		declare righe_test cursor for
		select cod_prodotto_figlio
		from   distinta
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto_padre=:ls_cod_prodotto;
		
		open righe_test;
		
		fetch righe_test
		into  :ls_test;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul DB: "+sqlca.sqlerrtext,stopsign!)
			close righe_test;
			pcca.error = c_fatal
			return
		end if
		
		if sqlca.sqlcode <> 100 then // se è figlio allora verifico che la versione corrente sia anche una delle 
			close righe_test;			  // 'n' versione già esistenti
			lb_flag_ok = false
		
			declare righe_db_1 cursor for
			select  cod_versione
			from    distinta 
			where   cod_azienda=:s_cs_xx.cod_azienda 
			and     cod_prodotto_padre =:ls_cod_prodotto;
			
			open righe_db_1;
			
			do while 1 = 1
				fetch righe_db_1
				into  :ls_cod_versione_esistente;
				
				if (sqlca.sqlcode = 100) then exit
				if sqlca.sqlcode<>0 then
					g_mb.messagebox("Sep","Errore nel DB: " + SQLCA.SQLErrText,stopsign!)
					close righe_db_1;
					pcca.error = c_fatal
					return
				end if
				
				if ls_cod_versione_esistente = ls_cod_versione then lb_flag_ok = true
			
			loop
		
			close righe_db_1;
		
			if lb_flag_ok = false then	//se la versione corrente (quella appena impostata) è diversa da quelle esistenti allora costringo l'utente a impostarne una esistente
				g_mb.messagebox("Sep","Poichè il prodotto scelto come PF è un semilavorato già utilizzato all'interno di altre distinte base è necessario che la versione immessa sia compatibile con le versioni già esistenti.",information!)
				s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
				window_open(w_selezione_versione,0)
				if s_cs_xx.parametri.parametro_s_1 = "-1" then
					pcca.error = c_fatal
					return
				else
					if s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto then
						pcca.error = c_fatal
						return
					else
						setitem(getrow(),"cod_versione",s_cs_xx.parametri.parametro_s_1)
					end if		
				end if
			end if
		end if
		close righe_test;
	end if
end if
end event

event clicked;call super::clicked;long ll_risposta
string	ls_cod_prodotto

if row>0 then
	if is_SPD = "S" then
		choose case dwo.name
			case "cf_associato"
				ls_cod_prodotto = dw_distinta_padri_lista.getitemstring(row, "cod_prodotto")
				
				if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
					s_cs_xx.parametri.parametro_b_1 = true
					s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
					window_open(w_tab_stat_prodotti, -1)
					return
				end if
				
		end choose
	end if
end if

s_cs_xx.parametri.parametro_dw_2 = dw_distinta_padri_det

if isvalid(w_distinta_base) then
	ll_risposta=w_distinta_base.wf_inizio()
end if


end event

event rowfocuschanged;call super::rowfocuschanged;//long ll_risposta
//
//s_cs_xx.parametri.parametro_dw_2 = dw_distinta_padri_det
//
//if isvalid(w_distinta_base) then
//	ll_risposta=w_distinta_base.wf_inizio()
//end if
end event

event pcd_modify;call super::pcd_modify;dw_distinta_padri_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_delete;call super::pcd_delete;dw_distinta_padri_det.object.b_ricerca_prodotto.enabled=false
end event

event updatestart;call super::updatestart;// stefanop 18/01/2016
// Cancello la distinta in caso di cancellazione del padre
string ls_cod_prodotto
int li_i

for li_i = 1 to DeletedCount()
	ls_cod_prodotto = getItemString(li_i, "cod_prodotto", Delete!, false)
	
	delete from distinta
	where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto_padre = :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la cancellazione delle distinte del prodotto padre " + ls_cod_prodotto, sqlca)
		rollback;
		return 1
	end if
next
end event

type cb_duplica from commandbutton within w_distinta_padri
integer x = 1984
integer y = 1928
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Duplica"
end type

event clicked;window_open(w_duplica_distinta,-1)
end event

type cb_costo from commandbutton within w_distinta_padri
integer x = 1595
integer y = 1828
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&osto"
end type

event clicked;string ls_cod_prodotto,ls_cod_versione

ls_cod_prodotto = dw_distinta_padri_lista.getitemstring(dw_distinta_padri_lista.getrow(),"cod_prodotto")
ls_cod_versione = dw_distinta_padri_lista.getitemstring(dw_distinta_padri_lista.getrow(),"cod_versione")

if isnull(ls_cod_prodotto) or isnull(ls_cod_versione) or ls_cod_prodotto="" or ls_cod_versione = "" then
	g_mb.messagebox("Sep","Non è possibile calcolare il costo poichè non è selezionato alcun prodotto!",stopsign!)
	return
end if

s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
s_cs_xx.parametri.parametro_s_2 = ls_cod_versione
s_cs_xx.parametri.parametro_s_3 = "1"
s_cs_xx.parametri.parametro_i_1 = 1

window_open(w_costo_preventivo_produzione,-1)
end event

type cb_stampa from commandbutton within w_distinta_padri
integer x = 23
integer y = 1928
integer width = 617
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea &File di Stampa DB"
end type

event clicked;string ls_cod_prodotto,ls_cod_versione,ls_errore,ls_des_prodotto
integer li_risposta,li_filenum
long ll_i

if dw_distinta_padri_lista.rowcount() = 0 then
	g_mb.messagebox("Sep","Selezionare una distinta base!",information!)
	return
end if

if isnull(sle_percorso) or sle_percorso.text="" then
	g_mb.messagebox("Sep","Manca il percorso del file di stampa distinta base!",information!)
	return
end if

filedelete(sle_percorso.text) 

setpointer(hourglass!)

if cbx_righe_lista.checked = true then 
	
	for ll_i = 1 to dw_distinta_padri_lista.rowcount()
		ls_cod_prodotto = dw_distinta_padri_lista.getitemstring(ll_i,"cod_prodotto")
		ls_cod_versione = dw_distinta_padri_lista.getitemstring(ll_i,"cod_versione")
		
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		end if
		

		li_FileNum = FileOpen(sle_percorso.text,LineMode!, Write!, LockWrite!, Append!)
		FileWrite(li_FileNum,"Distinta Base del prodotto finito " + ls_cod_prodotto + " Versione " + ls_cod_versione)
		FileWrite(li_FileNum," ")
		FileWrite(li_FileNum,ls_cod_prodotto + " " + ls_des_prodotto)
		fileclose(li_filenum)
		
		li_risposta = f_crea_file(ls_cod_prodotto,ls_cod_versione,1,ls_errore)
		
		if li_risposta < 0 then
			g_mb.messagebox("Sep",ls_errore,stopsign!)
			return
		end if
		
		li_FileNum = FileOpen(sle_percorso.text,LineMode!, Write!, LockWrite!, Append!)
		FileWrite(li_FileNum," ")
		FileWrite(li_FileNum," ")
		FileWrite(li_FileNum," ")
		FileWrite(li_FileNum," ")
		fileclose(li_filenum)
		
	next
	li_FileNum = FileOpen(sle_percorso.text,LineMode!, Write!, LockWrite!, Append!)
	FileWrite(li_FileNum,"(il simbolo * indica che il flag materia prima è attivo sul quel prodotto)")
	FileWrite(li_FileNum," ")
	fileclose(li_filenum)		
	
else
	ls_cod_prodotto = dw_distinta_padri_lista.getitemstring(dw_distinta_padri_lista.getrow(),"cod_prodotto")
	ls_cod_versione = dw_distinta_padri_lista.getitemstring(dw_distinta_padri_lista.getrow(),"cod_versione")
	
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	end if
	
	filedelete(sle_percorso.text) 
	li_FileNum = FileOpen(sle_percorso.text,LineMode!, Write!, LockWrite!, Append!)
	FileWrite(li_FileNum,"Distinta Base del prodotto finito " + ls_cod_prodotto + " Versione " + ls_cod_versione)
	FileWrite(li_FileNum," ")
	FileWrite(li_FileNum,ls_cod_prodotto + " " + ls_des_prodotto)
	fileclose(li_filenum)
	
	li_risposta = f_crea_file(ls_cod_prodotto,ls_cod_versione,1,ls_errore)
	
	if li_risposta < 0 then
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		return
	end if
	
	li_FileNum = FileOpen(sle_percorso.text,LineMode!, Write!, LockWrite!, Append!)
	FileWrite(li_FileNum," ")
	FileWrite(li_FileNum,"(il simbolo (*) indica che il flag materia prima è attivo sul quel prodotto)")
	fileclose(li_filenum)

end if

setpointer(arrow!)
g_mb.messagebox("Sep","File di distinta completato!",information!)
end event

type cbx_completa from checkbox within w_distinta_padri
integer x = 434
integer y = 1828
integer width = 366
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Completo"
end type

type st_1 from statictext within w_distinta_padri
integer x = 23
integer y = 1728
integer width = 411
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "File di Stampa:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_percorso from singlelineedit within w_distinta_padri
integer x = 457
integer y = 1728
integer width = 2190
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean autohscroll = false
boolean displayonly = true
borderstyle borderstyle = styleraised!
end type

type cb_sfoglia from commandbutton within w_distinta_padri
integer x = 2665
integer y = 1732
integer width = 69
integer height = 76
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;integer value
string docname, named,ls_test


value = GetFileSaveName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT,Ascii Files (*.ASC),*.ASC")


if value <> 1 then return

sle_percorso.text = docname

if g_mb.messagebox("Sep","Vuoi memorizzare il percorso per le prossime volte?",Information!,YesNo!,2) = 1 then
	
	select cod_parametro
	into   :ls_test
	from   parametri
	where  flag_parametro='S'
	and    cod_parametro='PDB';
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	end if
	
	if isnull(ls_test) or ls_test="" then
		insert into parametri  
				( flag_parametro,   
				  cod_parametro,   
				  des_parametro,   
				  stringa,   
				  flag,   
				  data,   
				  numero,   
				  flag_ini )  
		values ( 'S',   
				  'PDB',   
				  'Percorso file di stampa per distinta base',   
				  :docname,   
				  null,   
				  null,   
				  null,   
				  'N' )  ;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		end if
	else
		update parametri
		set stringa = :docname
		WHERE flag_parametro = 'S'  AND  
				cod_parametro = 'PDB';
				
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		end if
	end if
	
	
end if
end event

type cbx_fasi from checkbox within w_distinta_padri
integer x = 23
integer y = 1828
integer width = 379
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Fasi Lavoro"
end type

type cbx_righe_lista from checkbox within w_distinta_padri
integer x = 800
integer y = 1828
integer width = 640
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tutti i prodotti in lista"
end type

type cb_report from commandbutton within w_distinta_padri
integer x = 1984
integer y = 1828
integer width = 366
integer height = 80
integer taborder = 61
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report DB"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = dw_distinta_padri_lista.getitemstring(dw_distinta_padri_lista.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_2 = dw_distinta_padri_lista.getitemstring(dw_distinta_padri_lista.getrow(),"cod_versione")

window_open_parm(w_report_distinta,-1,dw_distinta_padri_lista)
end event

type dw_ricerca from u_dw_search within w_distinta_padri
event ue_key pbm_dwnkey
integer x = 5
integer y = 20
integer width = 2747
integer height = 316
integer taborder = 40
string dataobject = "d_distinta_padri_cerca"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_versione
long ll_test

if isvalid (dwo) then
	choose case dwo.name
		case "rs_cod_prodotto"
			
			if isnull(data) or data = "" then
				dw_ricerca.object.cod_versione.protect = '1'
				dw_ricerca.object.cod_versione.color = string(RGB(128,128,128))
			else
				dw_ricerca.object.cod_versione.protect = '0'
				dw_ricerca.object.cod_versione.color = string(RGB(255,255,255))
				
				f_PO_LoadDDDW_DW(this,"cod_versione",sqlca,&
							  "distinta_padri","cod_versione","'VERSIONE ' "+guo_functions.uof_concat_op()+" cod_versione",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + data + "'")
			end if
			
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
end choose
end event

