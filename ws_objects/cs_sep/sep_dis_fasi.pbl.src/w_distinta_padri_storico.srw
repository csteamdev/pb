﻿$PBExportHeader$w_distinta_padri_storico.srw
$PBExportComments$Window distinta padri storico
forward
global type w_distinta_padri_storico from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_distinta_padri_storico
end type
type cb_componenti from commandbutton within w_distinta_padri_storico
end type
type cb_reset from commandbutton within w_distinta_padri_storico
end type
type cb_ricerca from commandbutton within w_distinta_padri_storico
end type
type cb_costo from commandbutton within w_distinta_padri_storico
end type
type cb_report from commandbutton within w_distinta_padri_storico
end type
type dw_distinta_padri_storico_det from uo_cs_xx_dw within w_distinta_padri_storico
end type
type dw_distinta_padri_storico_lista from uo_cs_xx_dw within w_distinta_padri_storico
end type
type dw_ricerca from u_dw_search within w_distinta_padri_storico
end type
end forward

global type w_distinta_padri_storico from w_cs_xx_principale
integer width = 2670
integer height = 1756
string title = "Storico Distinta Padri"
cb_1 cb_1
cb_componenti cb_componenti
cb_reset cb_reset
cb_ricerca cb_ricerca
cb_costo cb_costo
cb_report cb_report
dw_distinta_padri_storico_det dw_distinta_padri_storico_det
dw_distinta_padri_storico_lista dw_distinta_padri_storico_lista
dw_ricerca dw_ricerca
end type
global w_distinta_padri_storico w_distinta_padri_storico

on w_distinta_padri_storico.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_componenti=create cb_componenti
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.cb_costo=create cb_costo
this.cb_report=create cb_report
this.dw_distinta_padri_storico_det=create dw_distinta_padri_storico_det
this.dw_distinta_padri_storico_lista=create dw_distinta_padri_storico_lista
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_componenti
this.Control[iCurrent+3]=this.cb_reset
this.Control[iCurrent+4]=this.cb_ricerca
this.Control[iCurrent+5]=this.cb_costo
this.Control[iCurrent+6]=this.cb_report
this.Control[iCurrent+7]=this.dw_distinta_padri_storico_det
this.Control[iCurrent+8]=this.dw_distinta_padri_storico_lista
this.Control[iCurrent+9]=this.dw_ricerca
end on

on w_distinta_padri_storico.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_componenti)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.cb_costo)
destroy(this.cb_report)
destroy(this.dw_distinta_padri_storico_det)
destroy(this.dw_distinta_padri_storico_lista)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[],ls_test


dw_distinta_padri_storico_lista.set_dw_key("cod_azienda")

dw_distinta_padri_storico_lista.set_dw_options(sqlca, &
                                               pcca.null_object, & 
													        c_noretrieveonopen + c_NoNew + c_NoModify + c_NoDelete, &
															  c_default)

dw_distinta_padri_storico_det.set_dw_options(sqlca, &
                                             dw_distinta_padri_storico_lista, & 
												         c_sharedata + c_scrollparent + c_noretrieveonopen + c_NoNew + c_NoModify + c_NoDelete, &
															c_default)

iuo_dw_main = dw_distinta_padri_storico_lista



l_criteriacolumn[1] = "rs_cod_prodotto"
l_searchtable[1] = "distinta_padri_storico"
l_searchcolumn[1] = "cod_prodotto"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_distinta_padri_storico_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)

dw_distinta_padri_storico_lista.change_dw_current()


	
end event

event close;call super::close;if isvalid(w_distinta_base) then
	close(w_distinta_base)
end if
end event

type cb_1 from commandbutton within w_distinta_padri_storico
integer x = 1851
integer y = 1560
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ripristina"
end type

event clicked;string ls_cod_prodotto, ls_cod_versione, ls_errore, ls_creato_da, ls_approvato_da, ls_des_versione, ls_flag_predefinita, &
       ls_flag_blocco, ls_flag_esplodi_in_doc, ls_flag_non_calcolare_dt, ls_flag_chiusura_automatica, ls_note, ls_test

datetime ldt_data_creazione, ldt_data_approvazione, ldt_data_blocco

long   ll_ret, ll_prog_storico, ll_riga

ll_riga = dw_distinta_padri_storico_lista.getrow()

if ll_riga <= 0 then return -1

ls_cod_prodotto = dw_distinta_padri_storico_lista.getitemstring( ll_riga, "cod_prodotto")

ls_cod_versione = dw_distinta_padri_storico_lista.getitemstring( ll_riga, "cod_versione")

ll_prog_storico = dw_distinta_padri_storico_lista.getitemnumber( ll_riga, "prog_storico")

if ls_cod_prodotto = "" or isnull(ls_cod_prodotto) or ls_cod_versione = "" or isnull(ls_cod_versione) then
	
	g_mb.messagebox( "SEP", "Attenzione: selezionare uno storico distinta per poterla ripristinare!", Exclamation!)
	
	return -1
	
end if

if ll_prog_storico < 1 or isnull(ll_prog_storico) then
	
	g_mb.messagebox( "SEP", "Attenzione: selezionare uno storico distinta per poterla ripristinare!", Exclamation!)
	
	return -1
	
end if	
	
ll_ret = g_mb.messagebox( "SEP", "Ripristinare la distinta selezionata ? ", Exclamation!, OKCancel!, 2)

IF ll_ret <>  1 THEN return -1

ll_ret = -1

select cod_azienda 
into   :ls_test
from   distinta_padri
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto and
		 cod_versione = :ls_cod_versione;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "SEP", "Errore durante controllo esistenza distinta: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

if sqlca.sqlcode <> 100 then 
	
//	delete from distinta_padri
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_prodotto = :ls_cod_prodotto and
//			 cod_versione = :ls_cod_versione;
//			 
//	if sqlca.sqlcode < 0 then
//		messagebox( "SEP", "Errore durante cancellazione distinta padri: " + sqlca.sqlerrtext)
//		rollback;
//		return -1
//	end if		
	
	ll_ret = f_elimina_distinta( ls_cod_prodotto, ls_cod_versione, ls_errore)

	if ll_ret <> 0 then		
		g_mb.messagebox( "SEP", ls_errore, exclamation!)		
		rollback;		
		return -1		
	end if
	
end if

ll_ret = -1

ls_errore = ""

SELECT distinta_padri_storico.creato_da,   
       distinta_padri_storico.approvato_da,   
       distinta_padri_storico.des_versione,   
       distinta_padri_storico.data_creazione,   
       distinta_padri_storico.data_approvazione,   
       distinta_padri_storico.flag_predefinita,   
       distinta_padri_storico.flag_blocco,   
       distinta_padri_storico.data_blocco,   
       distinta_padri_storico.flag_esplodi_in_doc,   
       distinta_padri_storico.flag_non_calcolare_dt,   
       distinta_padri_storico.flag_chiusura_automatica  
INTO   :ls_creato_da,   
       :ls_approvato_da,   
       :ls_des_versione,   
       :ldt_data_creazione,   
       :ldt_data_approvazione,   
       :ls_flag_predefinita,   
       :ls_flag_blocco,   
       :ldt_data_blocco,   
       :ls_flag_esplodi_in_doc,   
       :ls_flag_non_calcolare_dt,   
       :ls_flag_chiusura_automatica  
FROM   distinta_padri_storico  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto and
		 cod_versione = :ls_cod_versione and
		 prog_storico = :ll_prog_storico;

if sqlca.sqlcode < 0 then	
	g_mb.messagebox( "Sep", "Errore durante la select dalla distinta padri storico: " + sqlca.sqlerrtext, stopsign!)		
	rollback;
	return	
end if

if sqlca.sqlcode = 100 then	
	g_mb.messagebox( "SEP", "Attenzione: nessun risultato corrisponde a questo prodotto con questa versione.", stopsign!)	
	rollback;		
	return
end if

//INSERT INTO distinta_padri
//          ( cod_azienda,   
//            cod_prodotto,   
//            cod_versione,      
//            creato_da,   
//            approvato_da,   
//            des_versione,   
//            data_creazione,   
//            data_approvazione,   
//            flag_predefinita,   
//            flag_blocco,   
//            data_blocco,   
//            flag_esplodi_in_doc,   
//            flag_non_calcolare_dt,   
//            flag_chiusura_automatica )  
//  VALUES (  :s_cs_xx.cod_azienda,   
//            :ls_cod_prodotto,   
//            :ls_cod_versione,   
//            :ls_creato_da,   
//            :ls_approvato_da,   
//            :ls_des_versione,   
//            :ldt_data_creazione,   
//            :ldt_data_approvazione,   
//            :ls_flag_predefinita,   
//            :ls_flag_blocco,   
//            :ldt_data_blocco,   
//            :ls_flag_esplodi_in_doc,   
//            :ls_flag_non_calcolare_dt,   
//            :ls_flag_chiusura_automatica)  ;

update distinta_padri 
set    creato_da = :ls_creato_da,   
       approvato_da = :ls_approvato_da,   
       des_versione = :ls_des_versione,   
       data_creazione = :ldt_data_creazione,   
       data_approvazione = :ldt_data_approvazione,   
       flag_predefinita = :ls_flag_predefinita,   
       flag_blocco = :ls_flag_blocco,   
       data_blocco = :ldt_data_blocco,   
       flag_esplodi_in_doc = :ls_flag_esplodi_in_doc,   
       flag_non_calcolare_dt = :ls_flag_non_calcolare_dt,   
       flag_chiusura_automatica = :ls_flag_chiusura_automatica
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto and
		 cod_versione = :ls_cod_versione;

if sqlca.sqlcode < 0 then	
	g_mb.messagebox( "Sep", "Errore durante l'inserimento testata distinta padri: " + sqlca.sqlerrtext, stopsign!)		
	rollback;
	return	
end if

ll_ret = f_ripristina_distinta( ls_cod_prodotto, ls_cod_versione, ll_prog_storico, ls_errore)

if ll_ret <> 0 then	
	g_mb.messagebox( "SEP", ls_errore, exclamation!)	
	rollback;		
	return -1		
end if

commit;

g_mb.messagebox( "SEP", "Operazione avvenuta con successo!")
end event

type cb_componenti from commandbutton within w_distinta_padri_storico
integer x = 2240
integer y = 1560
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Componenti"
end type

event clicked;long ll_risposta

window_open(w_distinta_base_storico,-1)

s_cs_xx.parametri.parametro_dw_2 = dw_distinta_padri_storico_det

if isvalid(w_distinta_base_storico) then
	
	ll_risposta = w_distinta_base_storico.wf_inizio()
	
	w_distinta_base_storico.tv_db.SelectItem (1)
	
end if

end event

type cb_reset from commandbutton within w_distinta_padri_storico
integer x = 2350
integer y = 492
integer width = 274
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_buildsearch(TRUE)

dw_distinta_padri_storico_lista.change_dw_current()

parent.triggerevent("pc_retrieve")


end event

type cb_ricerca from commandbutton within w_distinta_padri_storico
integer x = 2354
integer y = 392
integer width = 274
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;dw_ricerca.fu_reset()



end event

type cb_costo from commandbutton within w_distinta_padri_storico
integer x = 1074
integer y = 1560
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&osto"
end type

event clicked;string ls_cod_prodotto, ls_cod_versione

long   ll_prog_storico

ls_cod_prodotto = dw_distinta_padri_storico_lista.getitemstring(dw_distinta_padri_storico_lista.getrow(),"cod_prodotto")

ls_cod_versione = dw_distinta_padri_storico_lista.getitemstring(dw_distinta_padri_storico_lista.getrow(),"cod_versione")

ll_prog_storico = dw_distinta_padri_storico_lista.getitemnumber(dw_distinta_padri_storico_lista.getrow(),"prog_storico")

if isnull(ls_cod_prodotto) or isnull(ls_cod_versione) or ls_cod_prodotto="" or ls_cod_versione = "" then
	g_mb.messagebox("Sep","Non è possibile calcolare il costo poichè non è selezionato alcun prodotto!",stopsign!)
	return
end if

s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto

s_cs_xx.parametri.parametro_s_2 = ls_cod_versione

s_cs_xx.parametri.parametro_s_3 = "1"

s_cs_xx.parametri.parametro_i_1 = 1

s_cs_xx.parametri.parametro_d_1 = ll_prog_storico

window_open(w_costo_preventivo_storico,-1)
end event

type cb_report from commandbutton within w_distinta_padri_storico
integer x = 1463
integer y = 1560
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report DB"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = dw_distinta_padri_storico_lista.getitemstring( dw_distinta_padri_storico_lista.getrow(), "cod_prodotto")

s_cs_xx.parametri.parametro_s_2 = dw_distinta_padri_storico_lista.getitemstring( dw_distinta_padri_storico_lista.getrow(), "cod_versione")

s_cs_xx.parametri.parametro_d_1 = dw_distinta_padri_storico_lista.getitemnumber( dw_distinta_padri_storico_lista.getrow(), "prog_storico")

window_open(w_report_distinta_storico, -1)
end event

type dw_distinta_padri_storico_det from uo_cs_xx_dw within w_distinta_padri_storico
integer x = 55
integer y = 660
integer width = 2583
integer height = 860
integer taborder = 90
string dataobject = "d_distinta_padri_storico_det"
borderstyle borderstyle = styleraised!
end type

type dw_distinta_padri_storico_lista from uo_cs_xx_dw within w_distinta_padri_storico
integer x = 23
integer y = 20
integer width = 2286
integer height = 540
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_distinta_padri_storico_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;//LONG  l_Idx
//
//FOR l_Idx = 1 TO RowCount()
//   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
//      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
//		SetItem(l_Idx, "creato_da", s_cs_xx.cod_utente)
//		SetItem(l_Idx, "data_creazione", datetime(today()))
//   END IF
//NEXT
//
end event

event clicked;call super::clicked;long ll_risposta

s_cs_xx.parametri.parametro_dw_2 = dw_distinta_padri_storico_det

if isvalid(w_distinta_base) then
	
	ll_risposta = w_distinta_base.wf_inizio()
	
end if


end event

type dw_ricerca from u_dw_search within w_distinta_padri_storico
event ue_key pbm_dwnkey
integer x = 23
integer y = 560
integer width = 2450
integer height = 120
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_distinta_padri_cerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
end choose
end event

