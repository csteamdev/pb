﻿$PBExportHeader$w_distinte_taglio.srw
$PBExportComments$Window distinte di taglio
forward
global type w_distinte_taglio from w_cs_xx_principale
end type
type dw_elenco_variabili from datawindow within w_distinte_taglio
end type
type dw_legenda from datawindow within w_distinte_taglio
end type
type cbx_azzera from checkbox within w_distinte_taglio
end type
type dw_distinte_taglio_lista from uo_cs_xx_dw within w_distinte_taglio
end type
type lb_operatori from listbox within w_distinte_taglio
end type
type cb_tutti from commandbutton within w_distinte_taglio
end type
type cb_duplica from commandbutton within w_distinte_taglio
end type
type dw_distinte_taglio_det from uo_cs_xx_dw within w_distinte_taglio
end type
type dw_comandi_distinte_taglio_lista from uo_cs_xx_dw within w_distinte_taglio
end type
end forward

global type w_distinte_taglio from w_cs_xx_principale
integer width = 4494
integer height = 2172
string title = "Distinte di taglio"
dw_elenco_variabili dw_elenco_variabili
dw_legenda dw_legenda
cbx_azzera cbx_azzera
dw_distinte_taglio_lista dw_distinte_taglio_lista
lb_operatori lb_operatori
cb_tutti cb_tutti
cb_duplica cb_duplica
dw_distinte_taglio_det dw_distinte_taglio_det
dw_comandi_distinte_taglio_lista dw_comandi_distinte_taglio_lista
end type
global w_distinte_taglio w_distinte_taglio

type variables
integer ii_formula
string is_cod_prodotto_finito, is_cod_versione
end variables

forward prototypes
public subroutine wf_set_valore (string fs_valore)
end prototypes

public subroutine wf_set_valore (string fs_valore);long ll_pos, ll_fine


ll_pos = dw_distinte_taglio_det.position()

dw_distinte_taglio_det.ReplaceText (fs_valore )
end subroutine

event pc_setwindow;call super::pc_setwindow;string					ls_sql, ls_syntax, ls_error
 
 is_cod_prodotto_finito = s_cs_xx.parametri.parametro_s_10
 is_cod_versione = s_cs_xx.parametri.parametro_s_11
 
setnull( s_cs_xx.parametri.parametro_s_10)
setnull( s_cs_xx.parametri.parametro_s_11)


choose case f_db()
	case "MSSQL"
		// metto il massimoi per sql server
		dw_distinte_taglio_lista.il_limit_campo_note = 8000
		dw_distinte_taglio_det.il_limit_campo_note = 8000
	case else
		// per gli altri lascio quello preimpostato
		dw_distinte_taglio_lista.il_limit_campo_note = 0
		dw_distinte_taglio_det.il_limit_campo_note = 0
end choose


dw_distinte_taglio_lista.set_dw_options(sqlca, &
													i_openparm, &
													c_default , &
													c_default)									 

dw_comandi_distinte_taglio_lista.set_dw_options(sqlca,&
													dw_distinte_taglio_lista, &
													c_scrollparent,& 
													c_default)													 

dw_distinte_taglio_det.set_dw_options(sqlca,&
													dw_distinte_taglio_lista, &
													c_sharedata + c_scrollparent,& 
													c_default)													 

iuo_dw_main = dw_distinte_taglio_lista


move(600, 20)
this.width = w_cs_xx_mdi.mdi_1.width - 60
this.height = w_cs_xx_mdi.mdi_1.height - 50

ls_sql = 		"select cod_variabile, nome_campo_database "+&
				"from tab_variabili_formule "+&
				"where cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
						"nome_campo_database<>'' and nome_campo_database is not null "+&
				"order by cod_variabile"
//if not isnull(is_cod_prodotto_finito) and is_cod_prodotto_finito <> "" then
//	//Ho il Codice Prodotto
//	dw_elenco_variabili.dataobject = "d_elenco_des_variabili"
//	ls_sql = ""
//else
//	dw_elenco_variabili.dataobject = "d_elenco_variabili"
//	ls_sql = ""
//end if

ls_syntax = SQLCA.SyntaxFromSQL(ls_sql, 'Style(Type=Grid)', ls_error)

if Len(ls_error) > 0 then
	g_mb.error("APICE", "Errore creazione sintassi SQL! " + ls_error)
else
	dw_elenco_variabili.create(ls_syntax, ls_error)
	if Len(ls_error) > 0 then
		g_mb.error("APICE", "Errore creazione datawindow variabili! " + ls_error)
	end if
end if

dw_elenco_variabili.SetTransObject(SQLCA)
dw_elenco_variabili.Retrieve()

dw_elenco_variabili.hscrollbar = true
dw_elenco_variabili.vscrollbar = true







end event

on w_distinte_taglio.create
int iCurrent
call super::create
this.dw_elenco_variabili=create dw_elenco_variabili
this.dw_legenda=create dw_legenda
this.cbx_azzera=create cbx_azzera
this.dw_distinte_taglio_lista=create dw_distinte_taglio_lista
this.lb_operatori=create lb_operatori
this.cb_tutti=create cb_tutti
this.cb_duplica=create cb_duplica
this.dw_distinte_taglio_det=create dw_distinte_taglio_det
this.dw_comandi_distinte_taglio_lista=create dw_comandi_distinte_taglio_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_elenco_variabili
this.Control[iCurrent+2]=this.dw_legenda
this.Control[iCurrent+3]=this.cbx_azzera
this.Control[iCurrent+4]=this.dw_distinte_taglio_lista
this.Control[iCurrent+5]=this.lb_operatori
this.Control[iCurrent+6]=this.cb_tutti
this.Control[iCurrent+7]=this.cb_duplica
this.Control[iCurrent+8]=this.dw_distinte_taglio_det
this.Control[iCurrent+9]=this.dw_comandi_distinte_taglio_lista
end on

on w_distinte_taglio.destroy
call super::destroy
destroy(this.dw_elenco_variabili)
destroy(this.dw_legenda)
destroy(this.cbx_azzera)
destroy(this.dw_distinte_taglio_lista)
destroy(this.lb_operatori)
destroy(this.cb_tutti)
destroy(this.cb_duplica)
destroy(this.dw_distinte_taglio_det)
destroy(this.dw_comandi_distinte_taglio_lista)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_comandi_distinte_taglio_lista,"cod_comando",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in (select cod_comando from tab_limiti_comando where cod_modello_tenda='"+ is_cod_prodotto_finito +"' group by cod_comando)")
end event

event resize;call super::resize;long ll_offset

long ll_x1						//posizione x della dw dt lista e della dw dt dettaglio

long ll_x2						//posizione x della dw comandi dt, di cb_duplica e della lista variabili

long ll_y1						//posizione y della dw dt lista e della dw comandi dt

long ll_y2						//posizione y  della dw dt dettaglio, della lista variabili e della legenda

long ll_x3, ll_x4				//posizioni x di cbx_azzera e cb_tutti

long ll_y3						//posizione y cb_duplica, cbx_azzera, cb_tutti

long ll_x6						//posizione x legenda

integer li_perc_w, li_perc_h, ll_param

li_perc_w = 50
li_perc_h = 40

ll_offset = 10

ll_param = 3


/*
---------------------------------+---------------------------------
distinte taglio						comandi DT
										cbdup|checkbox|cbtutti
---------------------------------+---------------------------------
dw_principale						var.	legenda
---------------------------------+---------------------------------
*/

ll_param = ll_param * ll_offset
//######################################################################
ll_x1 = 20
ll_y1 = 20
dw_distinte_taglio_lista.x = ll_x1
dw_distinte_taglio_lista.y = ll_y1
dw_distinte_taglio_lista.width = this.width * (li_perc_w / 100) - 2 * ll_offset
dw_distinte_taglio_lista.height = this.height * (li_perc_h / 100) - 2 * ll_offset
//######################################################################
ll_x2 = this.width * (li_perc_w / 100) + ll_offset
dw_comandi_distinte_taglio_lista.x = ll_x2
dw_comandi_distinte_taglio_lista.y = dw_distinte_taglio_lista.y
dw_comandi_distinte_taglio_lista.width = dw_distinte_taglio_lista.width - ll_param
dw_comandi_distinte_taglio_lista.height = dw_distinte_taglio_lista.height - (80 + 2 * ll_offset)
//-------------------------------------------------------------------
ll_y3 = ll_x1 + dw_comandi_distinte_taglio_lista.height + ll_offset
cb_duplica.x = dw_comandi_distinte_taglio_lista.x
cb_duplica.y = ll_y3
cb_duplica.width = 366
cb_duplica.height = 80
//-------------------------------------------------------------------
ll_x3 = cb_duplica.x + cb_duplica.width + ll_offset
cbx_azzera.x = ll_x3
cbx_azzera.y = cb_duplica.y
cbx_azzera.width = 1050
cbx_azzera.height = cb_duplica.height
//-------------------------------------------------------------------
ll_x4 = cbx_azzera.x + cbx_azzera.width + ll_offset
cb_tutti.x = ll_x4
cb_tutti.y = cbx_azzera.y
cb_tutti.width = 366
cb_tutti.height = cbx_azzera.height
//######################################################################
ll_y2 = this.height * (li_perc_h / 100) + ll_offset
dw_distinte_taglio_det.x = dw_distinte_taglio_lista.x
dw_distinte_taglio_det.y = ll_y2
dw_distinte_taglio_det.width =  dw_distinte_taglio_lista.width
dw_distinte_taglio_det.height = dw_distinte_taglio_lista.height
//######################################################################
ll_x6 = ll_x2 + (dw_comandi_distinte_taglio_lista.width * 0.2) + ll_offset
dw_elenco_variabili.x = ll_x2
dw_elenco_variabili.y = ll_y2
dw_elenco_variabili.width = long(dw_comandi_distinte_taglio_lista.width * 0.28) - ll_offset				//ll_x6 - ll_offset
dw_elenco_variabili.height = dw_distinte_taglio_det.height
//------------------------------------------------------------------
ll_x6 = ll_x2 + dw_elenco_variabili.width + ll_offset
dw_legenda.x = ll_x6
dw_legenda.y = dw_elenco_variabili.y
dw_legenda.width = long(dw_comandi_distinte_taglio_lista.width * 0.72) - ll_offset		//(dw_comandi_distinte_taglio_lista.width * 0.8) - ll_offset
dw_legenda.height = dw_distinte_taglio_det.height
//######################################################################








end event

type dw_elenco_variabili from datawindow within w_distinte_taglio
integer x = 3694
integer y = 1000
integer width = 686
integer height = 1040
integer taborder = 70
boolean titlebar = true
string title = "Variabili (doppio clic per selez.)"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;if currentrow>0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

event doubleclicked;string			ls_cod_variabile

if row>0 then
else
	return
end if

choose case dwo.name
	case "cod_variabile", "nome_campo_database"
		ls_cod_variabile = getitemstring(row, "cod_variabile")
		if ls_cod_variabile<>"" and not isnull(ls_cod_variabile) then wf_set_valore(ls_cod_variabile)
		
		
end  choose
end event

type dw_legenda from datawindow within w_distinte_taglio
integer x = 2789
integer y = 1032
integer width = 686
integer height = 400
integer taborder = 60
boolean enabled = false
string title = "none"
string dataobject = "d_legenda_formule_x_db"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;

choose case dwo.name
		
	case 	"potenza_t","int_t","truncate_t","round_t","sqr_t","exp_t","abs_t","mod_t","log_t","logten_t", &
			"pi_t","sin_t","cos_t","tan_t","asin_t","acos_t","atan_t", &
			"len_t",&
			"case_t","if_t"
				
		wf_set_valore(dwo.text)
		
end choose
end event

type cbx_azzera from checkbox within w_distinte_taglio
integer x = 2505
integer y = 904
integer width = 1051
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "(Cancella e Ricarica Tutti i comandi)"
boolean checked = true
boolean lefttext = true
end type

type dw_distinte_taglio_lista from uo_cs_xx_dw within w_distinte_taglio
integer x = 23
integer width = 2034
integer height = 960
integer taborder = 30
string dataobject = "d_distinte_taglio_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_modify;call super::pcd_modify;if i_extendmode then
	lb_operatori.enabled = true
	dw_elenco_variabili.enabled = true
	cb_duplica.enabled = false
	
	dw_elenco_variabili.hscrollbar = dw_elenco_variabili.enabled
	dw_elenco_variabili.vscrollbar = dw_elenco_variabili.enabled
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	
	string		ls_cod_prodotto_padre, ls_cod_prodotto_figlio,ls_cod_versione, ls_cod_versione_figlio
	long		ll_num_sequenza, ll_progressivo
	
	lb_operatori.enabled = true
	dw_elenco_variabili.enabled = true
	cb_duplica.enabled = false
	
	dw_elenco_variabili.hscrollbar = dw_elenco_variabili.enabled
	dw_elenco_variabili.vscrollbar = dw_elenco_variabili.enabled
	
	ls_cod_prodotto_padre = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto_padre")
	ls_cod_prodotto_figlio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto_figlio")
	ls_cod_versione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_versione")
	ls_cod_versione_figlio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_versione_figlio")
	ll_num_sequenza = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_sequenza")
	
	select max(num_ordinamento)
	into   :ll_progressivo
	from   tab_distinte_taglio
	where  cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto_padre=:ls_cod_prodotto_padre and
			num_sequenza=:ll_num_sequenza and
			cod_prodotto_figlio=:ls_cod_prodotto_figlio and
			cod_versione=:ls_cod_versione and
			cod_versione_figlio = :ls_cod_versione_figlio;
	
	if isnull(ll_progressivo) or ll_progressivo=0 then
		ll_progressivo = 10
	else
		ll_progressivo += 10
	end if
	
	setitem(getrow(),"num_ordinamento", ll_progressivo)
	
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG				l_Error
string				ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_cod_versione_figlio, ls_titolo
long				ll_num_sequenza

ls_cod_prodotto_padre = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto_padre")
ls_cod_prodotto_figlio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto_figlio")
ls_cod_versione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_versione")
ls_cod_versione_figlio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_versione_figlio")
ll_num_sequenza = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_sequenza")

ls_titolo = "Formule DT si Distinta ("+is_cod_prodotto_finito+" - "+is_cod_versione+") - "+&
									"Ramo ( Padre: "+ls_cod_prodotto_padre+" - "+ls_cod_versione+"  " + &
											   "Figlio"+ls_cod_prodotto_figlio+" - "+ls_cod_versione_figlio+"  N.Seq. "+string(ll_num_sequenza)+")"
parent.title = ls_titolo


l_Error = Retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto_padre, ll_num_sequenza,ls_cod_prodotto_figlio, ls_cod_versione, ls_cod_versione_figlio)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_cod_versione_figlio
long   ll_num_sequenza,ll_i,ll_progressivo

ls_cod_prodotto_padre = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto_padre")
ls_cod_prodotto_figlio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto_figlio")
ls_cod_versione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_versione")
ls_cod_versione_figlio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_versione_figlio")
ll_num_sequenza = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"num_sequenza")

select max(progressivo)
into   :ll_progressivo
from   tab_distinte_taglio
where  cod_azienda=:s_cs_xx.cod_azienda and
		cod_prodotto_padre=:ls_cod_prodotto_padre and
		num_sequenza=:ll_num_sequenza and
		cod_prodotto_figlio=:ls_cod_prodotto_figlio and
		cod_versione=:ls_cod_versione and
		cod_versione_figlio = :ls_cod_versione_figlio;

if isnull(ll_progressivo) or ll_progressivo=0 then
	ll_progressivo = 1
else
	ll_progressivo++
end if

for ll_i = 1 to rowcount()
   if isnull(getitemstring(ll_i, "cod_azienda")) then
		setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
		setitem(ll_i, "cod_prodotto_padre", ls_cod_prodotto_padre)
		setitem(ll_i, "num_sequenza", ll_num_sequenza)
		setitem(ll_i, "cod_prodotto_figlio", ls_cod_prodotto_figlio)
		setitem(ll_i, "cod_versione", ls_cod_versione)
		setitem(ll_i, "progressivo", ll_progressivo)
		setitem(ll_i, "cod_versione_figlio", ls_cod_versione_figlio)
   end if
   
next     
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	lb_operatori.enabled = false
	dw_elenco_variabili.enabled = false
	cb_duplica.enabled = true
	ii_formula = 0
	
	dw_elenco_variabili.hscrollbar = dw_elenco_variabili.enabled
	dw_elenco_variabili.vscrollbar = dw_elenco_variabili.enabled
end if
end event

event getfocus;call super::getfocus;iuo_dw_main = this
end event

type lb_operatori from listbox within w_distinte_taglio
boolean visible = false
integer x = 3982
integer y = 44
integer width = 411
integer height = 1052
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
boolean vscrollbar = true
boolean sorted = false
string item[] = {"+","-","*","/","SQR","INT","SIN","COS","TAN","ASIN","ACOS","ATAN","IF"}
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;string ls_buffer

choose case ii_formula
	case 1 
		ls_buffer = dw_distinte_taglio_det.getitemstring(dw_distinte_taglio_det.getrow(),"formula_quantita") 
		if isnull(ls_buffer) then ls_buffer = ""
		ls_buffer = ls_buffer + item[index]
		dw_distinte_taglio_det.setitem(dw_distinte_taglio_det.getrow(),"formula_quantita",ls_buffer)
	case 2
		ls_buffer = dw_distinte_taglio_det.getitemstring(dw_distinte_taglio_det.getrow(),"formula_misura")
		if isnull(ls_buffer) then ls_buffer = ""
		ls_buffer = ls_buffer + item[index]
		dw_distinte_taglio_det.setitem(dw_distinte_taglio_det.getrow(),"formula_misura",ls_buffer)
		
end choose

end event

type cb_tutti from commandbutton within w_distinte_taglio
integer x = 3566
integer y = 904
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Tutti i Com."
end type

event clicked;string ls_cod_comando,ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_test,	 ls_cod_versione_figlio

integer li_risposta,ll_num_sequenza,ll_progressivo

if dw_distinte_taglio_lista.rowcount() = 0 then return

ls_cod_prodotto_padre = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_prodotto_padre")
ls_cod_prodotto_figlio = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_prodotto_figlio")
ls_cod_versione = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_versione")
ls_cod_versione_figlio = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_versione_figlio")
ll_num_sequenza = dw_distinte_taglio_lista.getitemnumber(dw_distinte_taglio_lista.getrow(),"num_sequenza")
ll_progressivo = dw_distinte_taglio_lista.getitemnumber(dw_distinte_taglio_lista.getrow(),"progressivo")

if ls_cod_prodotto_padre = "" or isnull(ls_cod_prodotto_padre) or ls_cod_prodotto_figlio="" or isnull(ls_cod_prodotto_figlio) or ls_cod_versione="" or isnull(ls_cod_versione) or ls_cod_versione_figlio="" or isnull(ls_cod_versione_figlio) then return

if cbx_azzera.checked then
	li_risposta = g_mb.messagebox("Sep", "Cancelli tutti i comandi in precedenza precaricati?",Question!, YesNo!, 1)
	if li_risposta = 2 then return
	
	delete   distinte_taglio_comandi
	where  cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto_padre=:ls_cod_prodotto_padre and
			num_sequenza=:ll_num_sequenza and
			cod_prodotto_figlio=:ls_cod_prodotto_figlio and
			cod_versione=:ls_cod_versione and
			cod_versione_figlio=:ls_cod_versione_figlio and
			progressivo=:ll_progressivo ;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore in canccellazione vecchi comandi~r~n: " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return
	end if
end if	


li_risposta = g_mb.messagebox("Sep", "Si è sicuri di voler aggiungere alla lista tutti i comandi?",Question!, YesNo!, 1)

if li_risposta = 2 then return

declare r_limiti cursor for
select  cod_comando
from    tab_limiti_comando
where   cod_azienda=:s_cs_xx.cod_azienda and cod_modello_tenda=:is_cod_prodotto_finito 
group by cod_comando;

open r_limiti;
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore in open cursore r_limiti~r~n: " + sqlca.sqlerrtext,stopsign!)
		close r_limiti;
		rollback;
		return
	end if
	

do while true
	fetch r_limiti
	into  :ls_cod_comando;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_limiti;
		rollback;
		return
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	select cod_comando
	into   :ls_test
	from   distinte_taglio_comandi
	where  cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto_padre=:ls_cod_prodotto_padre and
			num_sequenza=:ll_num_sequenza and
			cod_prodotto_figlio=:ls_cod_prodotto_figlio and
			cod_versione=:ls_cod_versione and
			cod_versione_figlio=:ls_cod_versione_figlio and
			progressivo=:ll_progressivo and
			cod_comando=:ls_cod_comando;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_limiti;
		rollback;
		return
	end if
	
	if sqlca.sqlcode = 100 then
		insert into distinte_taglio_comandi
		(cod_azienda,
		 cod_prodotto_padre,
		 num_sequenza,
		 cod_prodotto_figlio,
		 cod_versione_figlio,
		 cod_versione,
		 progressivo,
		 cod_comando)
		values
		(:s_cs_xx.cod_azienda,
		 :ls_cod_prodotto_padre,
		 :ll_num_sequenza,
		 :ls_cod_prodotto_figlio,
		 :ls_cod_versione_figlio,
		 :ls_cod_versione,
		 :ll_progressivo,
		 :ls_cod_comando);
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_limiti;
			rollback;
			return
		end if
		
	end if
	
loop

close r_limiti;

commit;

g_mb.messagebox("Sep","Inserimento di tutti i comandi completato con successo!" + sqlca.sqlerrtext,information!)

dw_comandi_distinte_taglio_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cb_duplica from commandbutton within w_distinte_taglio
integer x = 2080
integer y = 900
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Duplica D.T."
end type

event clicked;//
//
//
//s_cs_xx.parametri.parametro_s_10 = dw_distinte_taglio_lista.i_parentdw.getitemstring(dw_distinte_taglio_lista.i_parentdw.i_selectedrows[1],"cod_prodotto_padre")
//s_cs_xx.parametri.parametro_s_11 = dw_distinte_taglio_lista.i_parentdw.getitemstring(dw_distinte_taglio_lista.i_parentdw.i_selectedrows[1],"cod_prodotto_figlio")
//s_cs_xx.parametri.parametro_s_12 = dw_distinte_taglio_lista.i_parentdw.getitemstring(dw_distinte_taglio_lista.i_parentdw.i_selectedrows[1],"cod_versione")
//s_cs_xx.parametri.parametro_i_2 = dw_distinte_taglio_lista.i_parentdw.getitemnumber(dw_distinte_taglio_lista.i_parentdw.i_selectedrows[1],"num_sequenza")

window_open_parm(w_distinte_taglio_copia,0, dw_distinte_taglio_lista)

dw_comandi_distinte_taglio_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type dw_distinte_taglio_det from uo_cs_xx_dw within w_distinte_taglio
integer x = 27
integer y = 1000
integer width = 2318
integer height = 1040
integer taborder = 30
string dataobject = "d_distinte_taglio_det"
end type

event pcd_view;call super::pcd_view;if i_extendmode then
	lb_operatori.enabled = false
	dw_elenco_variabili.enabled = false
	cb_duplica.enabled = true
	
	dw_elenco_variabili.hscrollbar = dw_elenco_variabili.enabled
	dw_elenco_variabili.vscrollbar = dw_elenco_variabili.enabled
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	lb_operatori.enabled = true
	dw_elenco_variabili.enabled = true
	cb_duplica.enabled = false
	
	dw_elenco_variabili.hscrollbar = dw_elenco_variabili.enabled
	dw_elenco_variabili.vscrollbar = dw_elenco_variabili.enabled
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	lb_operatori.enabled = true
	dw_elenco_variabili.enabled = true
	cb_duplica.enabled = false
	
	dw_elenco_variabili.hscrollbar = dw_elenco_variabili.enabled
	dw_elenco_variabili.vscrollbar = dw_elenco_variabili.enabled
end if


end event

event itemfocuschanged;call super::itemfocuschanged;//if i_extendmode then
//	choose case dwo.Name 
//		case "formula_quantita" 
//			ii_formula = 1
//		case "formula_misura"
//			ii_formula= 2
//		case else
//			ii_formula = 0
//	end choose
//end if
end event

event losefocus;call super::losefocus;//accepttext()
end event

event getfocus;call super::getfocus;iuo_dw_main = dw_distinte_taglio_lista
end event

event buttonclicked;call super::buttonclicked;string							ls_testo_formula, ls_errore
uo_formule_calcolo		luo_formule
dec{4}						ld_risultato
integer						li_ret

accepttext()

if row>0 then
else
	return
end if

ls_testo_formula = ""

choose case dwo.name
	case "b_test_quantita"
		ls_testo_formula = getitemstring(row, "formula_quantita")
		
	case "b_test_misura"
		ls_testo_formula = getitemstring(row, "formula_misura")
		
end choose

if ls_testo_formula<>"" and not isnull(ls_testo_formula) and ls_testo_formula<>"." then
	luo_formule = create uo_formule_calcolo
	luo_formule.uof_calcola_formula_dtaglio(  ls_testo_formula, true, 0, 0, 0,ld_risultato, ls_errore)
//	luo_formule.uof_calcola_formula_dtaglio( ls_testo_formula, true, 0, 0, 0, 0, 0, "", 0, 0, "", )
	destroy luo_formule
end if





//luo_formule = create  uo_formule_calcolo
//
////modalità testing
//luo_formule.ii_anno_reg_ord_ven = 0
//luo_formule.il_num_reg_ord_ven = 0
//luo_formule.il_prog_riga_ord_ven = 0
//
//li_ret = luo_formule.uof_calcola_formula_testo_per_test(ls_testo_formula, ld_risultato, ls_errore)
//destroy luo_formule
//
//if li_ret < 0 then
//	ls_errore = "ERR:~r~n" + ls_errore
//	g_mb.error("Errore Test Formula", ls_errore)
//	return
//end if
//
end event

type dw_comandi_distinte_taglio_lista from uo_cs_xx_dw within w_distinte_taglio
integer x = 2075
integer y = 20
integer width = 1851
integer height = 860
integer taborder = 20
string dataobject = "d_comandi_distinte_taglio_lista"
boolean vscrollbar = true
end type

event pcd_setkey;call super::pcd_setkey;string ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_cod_comando,ls_cod_versione_figlio
long   ll_num_sequenza,ll_i,ll_progressivo

ls_cod_prodotto_padre = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_prodotto_padre")
ls_cod_prodotto_figlio = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_prodotto_figlio")
ls_cod_versione = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_versione")
ls_cod_versione_figlio = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_versione_figlio")
ll_num_sequenza = dw_distinte_taglio_lista.getitemnumber(dw_distinte_taglio_lista.getrow(),"num_sequenza")
ll_progressivo = dw_distinte_taglio_lista.getitemnumber(dw_distinte_taglio_lista.getrow(),"progressivo")

for ll_i = 1 to rowcount()
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
		setitem(ll_i, "cod_prodotto_padre", ls_cod_prodotto_padre)
		setitem(ll_i, "num_sequenza", ll_num_sequenza)
		setitem(ll_i, "cod_prodotto_figlio", ls_cod_prodotto_figlio)
		setitem(ll_i, "cod_versione", ls_cod_versione)
		setitem(ll_i, "cod_versione_figlio", ls_cod_versione_figlio)
		setitem(ll_i, "progressivo", ll_progressivo)
   end if
   
next     
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_cod_versione_figlio
long   ll_num_sequenza,ll_progressivo

ls_cod_prodotto_padre = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_prodotto_padre")
ls_cod_prodotto_figlio = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_prodotto_figlio")
ls_cod_versione = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_versione")
ls_cod_versione_figlio = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_versione_figlio")
ll_num_sequenza = dw_distinte_taglio_lista.getitemnumber(dw_distinte_taglio_lista.getrow(),"num_sequenza")
ll_progressivo = dw_distinte_taglio_lista.getitemnumber(dw_distinte_taglio_lista.getrow(),"progressivo")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_prodotto_padre, ll_num_sequenza,ls_cod_prodotto_figlio,ls_cod_versione, ls_cod_versione_figlio, ll_progressivo)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event getfocus;call super::getfocus;iuo_dw_main = this
end event

event pcd_new;call super::pcd_new;dw_legenda.enabled = true
dw_elenco_variabili.enabled = dw_legenda.enabled

dw_elenco_variabili.hscrollbar = dw_elenco_variabili.enabled
dw_elenco_variabili.vscrollbar = dw_elenco_variabili.enabled
end event

event pcd_delete;call super::pcd_delete;dw_legenda.enabled = false
dw_elenco_variabili.enabled = dw_legenda.enabled

dw_elenco_variabili.hscrollbar = dw_elenco_variabili.enabled
dw_elenco_variabili.vscrollbar = dw_elenco_variabili.enabled
end event

event pcd_modify;call super::pcd_modify;dw_legenda.enabled = true
dw_elenco_variabili.enabled = dw_legenda.enabled

dw_elenco_variabili.hscrollbar = dw_elenco_variabili.enabled
dw_elenco_variabili.vscrollbar = dw_elenco_variabili.enabled
end event

event pcd_view;call super::pcd_view;dw_legenda.enabled = false
dw_elenco_variabili.enabled = dw_legenda.enabled

dw_elenco_variabili.hscrollbar = dw_elenco_variabili.enabled
dw_elenco_variabili.vscrollbar = dw_elenco_variabili.enabled
end event

event buttonclicked;call super::buttonclicked;s_distinte_taglio_comandi lstr_comandi

lstr_comandi.cod_prodotto_finito = is_cod_prodotto_finito
lstr_comandi.cod_prodotto_padre = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_prodotto_padre")
lstr_comandi.cod_prodotto_figlio = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_prodotto_figlio")
lstr_comandi.cod_versione_padre = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_versione")
lstr_comandi.cod_versione_figlio = dw_distinte_taglio_lista.getitemstring(dw_distinte_taglio_lista.getrow(),"cod_versione_figlio")
lstr_comandi.num_sequenza = dw_distinte_taglio_lista.getitemnumber(dw_distinte_taglio_lista.getrow(),"num_sequenza")
lstr_comandi.progressivo = dw_distinte_taglio_lista.getitemnumber(dw_distinte_taglio_lista.getrow(),"progressivo")


OpenWithParm ( w_distinte_taglio_add_comandi, lstr_comandi )

dw_comandi_distinte_taglio_lista.change_dw_current()
parent.triggerevent("pc_retrieve")

end event

