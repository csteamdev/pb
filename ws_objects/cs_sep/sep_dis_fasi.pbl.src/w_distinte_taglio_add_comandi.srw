﻿$PBExportHeader$w_distinte_taglio_add_comandi.srw
forward
global type w_distinte_taglio_add_comandi from window
end type
type cb_conferma from commandbutton within w_distinte_taglio_add_comandi
end type
type dw_comandi from datawindow within w_distinte_taglio_add_comandi
end type
end forward

global type w_distinte_taglio_add_comandi from window
integer width = 2272
integer height = 2072
boolean titlebar = true
string title = "Multi-Selezione Comandi"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 15793151
string icon = "AppIcon!"
boolean center = true
cb_conferma cb_conferma
dw_comandi dw_comandi
end type
global w_distinte_taglio_add_comandi w_distinte_taglio_add_comandi

type variables
s_distinte_taglio_comandi istr_comandi
end variables

forward prototypes
public subroutine wf_carica_comandi ()
end prototypes

public subroutine wf_carica_comandi ();string 	ls_cod_comando,ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_test,	 ls_cod_versione_figlio, ls_cod_prodotto_finito, &
			ls_des_comando

long		li_risposta,ll_num_sequenza,ll_progressivo, ll_row

ls_cod_prodotto_finito = istr_comandi.cod_prodotto_finito
ls_cod_prodotto_padre = istr_comandi.cod_prodotto_padre
ls_cod_prodotto_figlio = istr_comandi.cod_prodotto_figlio
ls_cod_versione = istr_comandi.cod_versione_padre
ls_cod_versione_figlio = istr_comandi.cod_versione_figlio
ll_num_sequenza = istr_comandi.num_sequenza
ll_progressivo = istr_comandi.progressivo

if ls_cod_prodotto_padre = "" or isnull(ls_cod_prodotto_padre) or ls_cod_prodotto_figlio="" or isnull(ls_cod_prodotto_figlio) or ls_cod_versione="" or isnull(ls_cod_versione) or ls_cod_versione_figlio="" or isnull(ls_cod_versione_figlio) then return


declare r_limiti cursor for
select  A.cod_comando , B.des_prodotto
from    tab_limiti_comando A
join 		anag_prodotti B on A.cod_azienda = B.cod_azienda and A.cod_comando = B.cod_prodotto
where   A.cod_azienda=:s_cs_xx.cod_azienda and A.cod_modello_tenda=:ls_cod_prodotto_finito 
group by A.cod_comando , B.des_prodotto;

open r_limiti;
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore in open cursore r_limiti~r~n: " + sqlca.sqlerrtext,stopsign!)
		close r_limiti;
		rollback;
		return
	end if
	
dw_comandi.reset()

do while true
	fetch r_limiti
	into  :ls_cod_comando, :ls_des_comando;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_limiti;
		rollback;
		return
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	select cod_comando
	into   :ls_test
	from   distinte_taglio_comandi
	where  cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto_padre=:ls_cod_prodotto_padre and
			num_sequenza=:ll_num_sequenza and
			cod_prodotto_figlio=:ls_cod_prodotto_figlio and
			cod_versione=:ls_cod_versione and
			cod_versione_figlio=:ls_cod_versione_figlio and
			progressivo=:ll_progressivo and
			cod_comando=:ls_cod_comando;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_limiti;
		rollback;
		return
	end if
	
	ll_row = dw_comandi.insertrow(0)
	
	dw_comandi.setitem(ll_row, "cod_comando", ls_cod_comando)
	dw_comandi.setitem(ll_row, "des_comando", ls_des_comando)
	
	if sqlca.sqlcode = 100 then
		dw_comandi.setitem(ll_row, "flag_selezione", "N")
	else
		dw_comandi.setitem(ll_row, "flag_selezione", "S")
	end if
	
loop

close r_limiti;


end subroutine

event open;istr_comandi= message.powerobjectparm

wf_carica_comandi()


end event

on w_distinte_taglio_add_comandi.create
this.cb_conferma=create cb_conferma
this.dw_comandi=create dw_comandi
this.Control[]={this.cb_conferma,&
this.dw_comandi}
end on

on w_distinte_taglio_add_comandi.destroy
destroy(this.cb_conferma)
destroy(this.dw_comandi)
end on

type cb_conferma from commandbutton within w_distinte_taglio_add_comandi
integer x = 1874
integer y = 1904
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;string ls_cod_comando,ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_test,	 ls_cod_versione_figlio, ls_cod_prodotto_finito

long	li_risposta,ll_num_sequenza,ll_progressivo, ll_i

if dw_comandi.rowcount() = 0 then return



ls_cod_prodotto_finito = istr_comandi.cod_prodotto_finito
ls_cod_prodotto_padre = istr_comandi.cod_prodotto_padre
ls_cod_prodotto_figlio = istr_comandi.cod_prodotto_figlio
ls_cod_versione = istr_comandi.cod_versione_padre
ls_cod_versione_figlio = istr_comandi.cod_versione_figlio
ll_num_sequenza = istr_comandi.num_sequenza
ll_progressivo = istr_comandi.progressivo

delete   distinte_taglio_comandi
where  cod_azienda=:s_cs_xx.cod_azienda and
		cod_prodotto_padre=:ls_cod_prodotto_padre and
		num_sequenza=:ll_num_sequenza and
		cod_prodotto_figlio=:ls_cod_prodotto_figlio and
		cod_versione=:ls_cod_versione and
		cod_versione_figlio=:ls_cod_versione_figlio and
		progressivo=:ll_progressivo ;
	
if sqlca.sqlcode< 0 then
	g_mb.messagebox("Sep","Errore in canccellazione vecchi comandi~r~n: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	close(parent)
end if

for ll_i = 1 to dw_comandi.rowcount()
	if dw_comandi.getitemstring(ll_i, "flag_selezione") = "S" then
		ls_cod_comando = dw_comandi.getitemstring(ll_i, "cod_comando")

		insert into distinte_taglio_comandi
			(cod_azienda,
			 cod_prodotto_padre,
			 num_sequenza,
			 cod_prodotto_figlio,
			 cod_versione_figlio,
			 cod_versione,
			 progressivo,
			 cod_comando)
		values
			(:s_cs_xx.cod_azienda,
			 :ls_cod_prodotto_padre,
			 :ll_num_sequenza,
			 :ls_cod_prodotto_figlio,
			 :ls_cod_versione_figlio,
			 :ls_cod_versione,
			 :ll_progressivo,
			 :ls_cod_comando);
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			close(parent)
		end if
		
	end if
	
next

commit;

close(parent)
end event

type dw_comandi from datawindow within w_distinte_taglio_add_comandi
integer x = 5
integer y = 8
integer width = 2231
integer height = 1884
integer taborder = 10
string title = "none"
string dataobject = "d_disinte_taglio_add_comandi"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;long ll_i

choose case dwo.name
	case "t_selezione"
		for ll_i = 1 to rowcount()
			if this.object.t_selezione.text = "Sel.*" then
				setitem(ll_i, "flag_selezione","N")
			else
				setitem(ll_i, "flag_selezione","S")
			end if
		next
		
		if this.object.t_selezione.text = "Sel.*" then
			this.object.t_selezione.text = "Sel."
		else
			this.object.t_selezione.text = "Sel.*"
		end if

	case "t_codice"
		if this.object.t_codice.text = "Codice*" then
			setsort("cod_comando ASC")
		else
			setsort("cod_comando DESC")
		end if
		sort()
		
		if this.object.t_codice.text = "Codice*" then
			this.object.t_codice.text = "Codice"
		else
			this.object.t_codice.text = "Codice*"
		end if
	case "t_descrizione"
		if this.object.t_descrizione.text = "Descrizione*" then
			setsort("des_comando ASC")
		else
			setsort("des_comando DESC")
		end if
		sort()
		
		if this.object.t_descrizione.text = "Descrizione*" then
			this.object.t_descrizione.text = "Descrizione"
		else
			this.object.t_descrizione.text = "Descrizione*"
		end if
		
end choose
end event

