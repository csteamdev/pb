﻿$PBExportHeader$w_distinte_taglio_copia.srw
$PBExportComments$Window distinte di taglio per copia
forward
global type w_distinte_taglio_copia from w_cs_xx_risposta
end type
type cbx_comandi from checkbox within w_distinte_taglio_copia
end type
type cb_chiudi from commandbutton within w_distinte_taglio_copia
end type
type cb_copia from commandbutton within w_distinte_taglio_copia
end type
type dw_distinte_taglio_lista_copia from uo_cs_xx_dw within w_distinte_taglio_copia
end type
end forward

global type w_distinte_taglio_copia from w_cs_xx_risposta
integer width = 3186
integer height = 1864
string title = "Copia distinte di taglio"
cbx_comandi cbx_comandi
cb_chiudi cb_chiudi
cb_copia cb_copia
dw_distinte_taglio_lista_copia dw_distinte_taglio_lista_copia
end type
global w_distinte_taglio_copia w_distinte_taglio_copia

on w_distinte_taglio_copia.create
int iCurrent
call super::create
this.cbx_comandi=create cbx_comandi
this.cb_chiudi=create cb_chiudi
this.cb_copia=create cb_copia
this.dw_distinte_taglio_lista_copia=create dw_distinte_taglio_lista_copia
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_comandi
this.Control[iCurrent+2]=this.cb_chiudi
this.Control[iCurrent+3]=this.cb_copia
this.Control[iCurrent+4]=this.dw_distinte_taglio_lista_copia
end on

on w_distinte_taglio_copia.destroy
call super::destroy
destroy(this.cbx_comandi)
destroy(this.cb_chiudi)
destroy(this.cb_copia)
destroy(this.dw_distinte_taglio_lista_copia)
end on

event pc_setwindow;call super::pc_setwindow;dw_distinte_taglio_lista_copia.setredraw( false)

dw_distinte_taglio_lista_copia.set_dw_options(sqlca, &
															i_openparm, &
															c_nonew+c_nomodify+c_nodelete+ c_multiselect,& 
															c_default)													 

dw_distinte_taglio_lista_copia.change_dw_current( )
dw_distinte_taglio_lista_copia.triggerevent("pcd_retrieve")

dw_distinte_taglio_lista_copia.setredraw( true)

end event

type cbx_comandi from checkbox within w_distinte_taglio_copia
integer x = 1467
integer y = 1664
integer width = 864
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Includi comandi nella copia"
boolean checked = true
end type

type cb_chiudi from commandbutton within w_distinte_taglio_copia
integer x = 2757
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&hiudi"
end type

event clicked;close(parent)
end event

type cb_copia from commandbutton within w_distinte_taglio_copia
integer x = 2368
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Copia"
end type

event clicked;string  ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_des_distinta,ls_um_quantita,ls_formula_quantita, &
		  ls_um_misura,ls_formula_misura,ls_flag_tutti_comandi,ls_cod_versione_figlio, &
		  ls_cod_prodotto_padre_old,ls_cod_prodotto_figlio_old,ls_cod_versione_figlio_old,ls_cod_versione_padre_old   
long	ll_num_sequenza, ll_num_ordinamento, ll_selected[],ll_righe,ll_progressivo,ll_num_sequenza_old, ll_progressivo_old
		

ls_cod_prodotto_padre = dw_distinte_taglio_lista_copia.i_parentdw.i_parentdw.getitemstring(dw_distinte_taglio_lista_copia.i_parentdw.i_parentdw.i_selectedrows[1], "cod_prodotto_padre")
ls_cod_prodotto_figlio = dw_distinte_taglio_lista_copia.i_parentdw.i_parentdw.getitemstring(dw_distinte_taglio_lista_copia.i_parentdw.i_parentdw.i_selectedrows[1], "cod_prodotto_figlio")
ls_cod_versione = dw_distinte_taglio_lista_copia.i_parentdw.i_parentdw.getitemstring(dw_distinte_taglio_lista_copia.i_parentdw.i_parentdw.i_selectedrows[1], "cod_versione")
ls_cod_versione_figlio = dw_distinte_taglio_lista_copia.i_parentdw.i_parentdw.getitemstring(dw_distinte_taglio_lista_copia.i_parentdw.i_parentdw.i_selectedrows[1], "cod_versione_figlio")
ll_num_sequenza = dw_distinte_taglio_lista_copia.i_parentdw.i_parentdw.getitemnumber(dw_distinte_taglio_lista_copia.i_parentdw.i_parentdw.i_selectedrows[1], "num_sequenza")


select max(progressivo)
into   :ll_progressivo
from   tab_distinte_taglio
where  cod_azienda=:s_cs_xx.cod_azienda and
		cod_prodotto_padre=:ls_cod_prodotto_padre and
		num_sequenza=:ll_num_sequenza and
		cod_prodotto_figlio=:ls_cod_prodotto_figlio and
		cod_versione=:ls_cod_versione and
		cod_versione_figlio = :ls_cod_versione_figlio;

if isnull(ll_progressivo) or ll_progressivo=0 then
	ll_progressivo = 1
else
	ll_progressivo++
end if

dw_distinte_taglio_lista_copia.get_selected_rows(ll_selected[])


for ll_righe = 1 to upperbound(ll_selected)
	ls_des_distinta = dw_distinte_taglio_lista_copia.getitemstring(ll_selected[ll_righe], "des_distinta_taglio")
	ls_um_quantita = dw_distinte_taglio_lista_copia.getitemstring(ll_selected[ll_righe], "um_quantita")
	ls_formula_quantita = dw_distinte_taglio_lista_copia.getitemstring(ll_selected[ll_righe], "formula_quantita")
	ls_um_misura = dw_distinte_taglio_lista_copia.getitemstring(ll_selected[ll_righe], "um_misura")
	ls_formula_misura = dw_distinte_taglio_lista_copia.getitemstring(ll_selected[ll_righe], "formula_misura")
	ls_flag_tutti_comandi = dw_distinte_taglio_lista_copia.getitemstring(ll_selected[ll_righe], "tutti_comandi")
	ll_num_ordinamento = dw_distinte_taglio_lista_copia.getitemnumber(ll_selected[ll_righe], "num_ordinamento")
						
	insert into tab_distinte_taglio
	(cod_azienda,
	 cod_prodotto_padre,
	 num_sequenza,
	 cod_prodotto_figlio,
	 cod_versione,
	 cod_versione_figlio,
	 progressivo,
	 des_distinta_taglio,
	 um_quantita,
	 formula_quantita,
	 um_misura,
	 formula_misura,
	 tutti_comandi,
	 num_ordinamento)
	values
	(:s_cs_xx.cod_azienda,
	 :ls_cod_prodotto_padre,
	 :ll_num_sequenza,
	 :ls_cod_prodotto_figlio,
	 :ls_cod_versione,
	 :ls_cod_versione_figlio,
	 :ll_progressivo,
	 :ls_des_distinta,
	 :ls_um_quantita,
	 :ls_formula_quantita,
	 :ls_um_misura,
	 :ls_formula_misura,
	 :ls_flag_tutti_comandi,
	 :ll_num_ordinamento);
	
	if sqlca.sqlcode < 0 then
		g_mb.error( g_str.format("Errore in copia distinta di taglio. $1 ",sqlca.sqlerrtext))
		rollback;
		return
	end if
	
	if cbx_comandi.checked then

			ls_cod_prodotto_padre_old = dw_distinte_taglio_lista_copia.getitemstring(ll_selected[ll_righe], "cod_prodotto_padre")
			ll_num_sequenza_old = dw_distinte_taglio_lista_copia.getitemnumber(ll_selected[ll_righe], "num_sequenza")
			ls_cod_prodotto_figlio_old = dw_distinte_taglio_lista_copia.getitemstring(ll_selected[ll_righe], "cod_prodotto_figlio")
			ls_cod_versione_figlio_old = dw_distinte_taglio_lista_copia.getitemstring(ll_selected[ll_righe], "cod_versione_figlio")
			ls_cod_versione_padre_old = dw_distinte_taglio_lista_copia.getitemstring(ll_selected[ll_righe], "cod_versione")
			ll_progressivo_old = dw_distinte_taglio_lista_copia.getitemnumber(ll_selected[ll_righe], "progressivo")

			INSERT INTO distinte_taglio_comandi  
				( cod_azienda,   
				  cod_prodotto_padre,   
				  num_sequenza,   
				  cod_prodotto_figlio,   
				  cod_versione_figlio,   
				  cod_versione,   
				  progressivo,   
				  cod_comando )  
			select  :s_cs_xx.cod_azienda,
					 :ls_cod_prodotto_padre,
					 :ll_num_sequenza,
					 :ls_cod_prodotto_figlio,
					 :ls_cod_versione,
					 :ls_cod_versione_figlio,
					 :ll_progressivo,
					 cod_comando
			from 	distinte_taglio_comandi
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto_padre = :ls_cod_prodotto_padre_old and
					num_sequenza = :ll_num_sequenza_old and
					cod_prodotto_figlio = :ls_cod_prodotto_figlio_old and
					cod_versione_figlio = :ls_cod_versione_figlio_old and
					cod_versione = :ls_cod_versione_padre_old and
					progressivo = :ll_progressivo_old;
			if sqlca.sqlcode < 0 then
				g_mb.error( g_str.format("Errore in copia comandi della distinta di taglio. $1 ",sqlca.sqlerrtext))
				rollback;
				return
			end if
					
	end if	
	ll_progressivo++
	
next

commit;
g_mb.messagebox("Sep","Copia eseguita con successo!",information!)
	
end event

type dw_distinte_taglio_lista_copia from uo_cs_xx_dw within w_distinte_taglio_copia
integer x = 23
integer y = 20
integer width = 3104
integer height = 1620
integer taborder = 10
string dataobject = "d_distinte_taglio_lista_copia"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

