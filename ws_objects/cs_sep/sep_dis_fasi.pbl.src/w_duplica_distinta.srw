﻿$PBExportHeader$w_duplica_distinta.srw
$PBExportComments$Window di duplicazione DB
forward
global type w_duplica_distinta from w_cs_xx_principale
end type
type dw_ricerca_origine from u_dw_search within w_duplica_distinta
end type
end forward

global type w_duplica_distinta from w_cs_xx_principale
integer width = 2949
integer height = 1756
string title = "Duplicazione Distinta Base"
dw_ricerca_origine dw_ricerca_origine
end type
global w_duplica_distinta w_duplica_distinta

type variables
//OLEObject iole_excel
end variables

forward prototypes
public subroutine wf_duplica ()
public subroutine wf_duplica_legami ()
public function long wf_duplica_legami (string fs_cod_prodotto_origine, string fs_cod_versione_origine, string fs_cod_prodotto_destinazione, string fs_cod_versione_destinazione, string fs_flag_sovrascrivi, ref string fs_errore)
public function integer wf_duplica_excel (ref string fs_errore)
public subroutine wf_path_excel (ref string fs_path)
public function integer wf_duplica_semilavorati_excel (string fs_cod_prodotto_origine, string fs_cod_versione_origine, string fs_cod_prodotto_dest, string fs_cod_versione_dest, ref string fs_errore)
public function integer wf_duplica_gibus (string as_cod_prodotto_partenza, string as_cod_versione_partenza, string as_cod_prodotto_destinazione, string as_cod_versione_destinazione, ref string as_errore)
public function integer wf_get_versione (string as_cod_prodotto_figlio, ref string as_cod_versione_figlio, ref string as_errore)
end prototypes

public subroutine wf_duplica ();string				ls_cod_prodotto_partenza,ls_cod_prodotto_destinazione,ls_cod_versione_partenza,ls_cod_versione_destinazione,ls_errore,&
					ls_des_versione,ls_test_padre,ls_cod_versione_esistente,ls_test, ls_check_duplica_tutto, ls_flag_duplica_varianti, ls_flag_duplica_formule, &
					ls_flag_distinte_taglio
					
integer			li_risposta
boolean			lb_flag_ok, lb_GIB
datetime			ldt_oggi


ldt_oggi = datetime(today(),00:00:00)
ls_cod_prodotto_partenza = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"rs_cod_prodotto")
ls_cod_prodotto_destinazione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"rs_cod_prodotto_destinazione")
ls_cod_versione_partenza = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_versione")
ls_cod_versione_destinazione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_versione_destinazione")
ls_des_versione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"des_versione")
ls_check_duplica_tutto = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"flag_duplica_tutto")

if ls_check_duplica_tutto = "N" then 
	ls_flag_duplica_formule = "N"
	ls_flag_duplica_varianti = "N"
else
	ls_flag_duplica_formule = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(), "flag_duplica_formule")
	ls_flag_duplica_varianti = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(), "flag_duplica_varianti")
end if

if isnull(ls_cod_prodotto_partenza) or ls_cod_prodotto_partenza="" then
	g_mb.messagebox("Sep","Manca il codice del prodotto di partenza",stopsign!)
	return
end if

select cod_azienda
into   :ls_test_padre
from   anag_prodotti
where  cod_azienda  = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto_partenza;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore sul DB:" + sqlca.sqlerrtext)
	return
end if

if sqlca.sqlcode = 100 then
	g_mb.warning("Il codice del prodotto di partenza non esiste in anagrafica prodotti. Reinserire un codice esistente!")
	return
end if


if isnull(ls_cod_prodotto_destinazione) or ls_cod_prodotto_destinazione="" then
	g_mb.warning("Manca il codice del prodotto di destinazione!")
	return
end if

select cod_azienda
into :ls_test_padre
from  anag_prodotti
where cod_azienda=:s_cs_xx.cod_azienda
and   cod_prodotto=:ls_cod_prodotto_destinazione;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore sul DB:" + sqlca.sqlerrtext)
	return
end if

if sqlca.sqlcode = 100 then
	g_mb.warning("Il codice del prodotto di destinazione non esiste in anagrafica prodotti. Reinserire un codice esistente!")
	return
end if

if isnull(ls_cod_versione_destinazione) or ls_cod_versione_destinazione="" then
	g_mb.warning("Manca la versione di destinazione!")
	return
end if

if isnull(ls_cod_versione_partenza) or ls_cod_versione_partenza="" then
	g_mb.warning("Manca la versione di partenza!")
	return
end if

//***************	ROUTINE DI CONTROLLO DI CONGRUENZA VERSIONI
	
setnull(ls_test_padre)

select cod_azienda
into   :ls_test_padre
from   distinta_padri
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto_destinazione;     //verifico se il prodotto_destinazione è già prodotto finito

if isnull(ls_test_padre) then // se il prodotto non è un PF allora verifico se è figlio in distinta
	
	declare righe_test cursor for
	select cod_prodotto_figlio
	from   distinta
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto_figlio=:ls_cod_prodotto_destinazione;
	
	open righe_test;
	
	fetch righe_test
	into  :ls_test;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore sul DB: "+sqlca.sqlerrtext)
		close righe_test;
		return
	end if
	
	if sqlca.sqlcode <> 100 then // se è figlio allora verifico che la versione_destinazione non sia una delle 
		close righe_test;			  // 'n' versione già esistenti
		lb_flag_ok = false
	
		declare righe_db_1 cursor for
		select  cod_versione_figlio
		from    distinta 
		where   cod_azienda=:s_cs_xx.cod_azienda 
		and     cod_prodotto_figlio =:ls_cod_prodotto_destinazione;
		
		open righe_db_1;
		
		do while 1 = 1
			fetch righe_db_1
			into  :ls_cod_versione_esistente;
			
			if (sqlca.sqlcode = 100) then exit
			if sqlca.sqlcode<0 then
				g_mb.error("Errore nel DB: " + SQLCA.SQLErrText)
				close righe_db_1;
				return
			end if
			
			if ls_cod_versione_esistente = ls_cod_versione_destinazione then lb_flag_ok = true
		
		loop
	
		close righe_db_1;
	
		if lb_flag_ok = true then	//se la versione_destinazione è uguale a quelle esistenti allora costringo l'utente a impostarne un'altra
			g_mb.warning("La versione scelta è già esistente. Reinserire una nuova versione diversa da quelle esistenti!")
			return
		end if

	end if
	close righe_test;
	
else //	se il prodotto destinazione è un PF allora verifico che la versione di destinazione non si già utilizzata

	setnull(ls_test)
	select cod_azienda
	into   :ls_test
	from   distinta_padri
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto_destinazione
	and    cod_versione=:ls_cod_versione_destinazione;
		
	if sqlca.sqlcode<0 then
		g_mb.error("Errore nel DB: " + SQLCA.SQLErrText)
		return
	end if
		
	if sqlca.sqlcode = 0 then
		g_mb.warning("La versione di destinazione è già esistente. Inserire una codice versione non utilizzato!")
		return
	end if
	
end if

//*************** FINE ROUTINE DI CONTROLLO

insert into distinta_padri  
           (cod_azienda,   
            cod_prodotto,   
            cod_versione,   
            creato_da,   
            approvato_da,   
            des_versione,   
            data_creazione,   
            data_approvazione,   
            flag_predefinita,   
            flag_blocco,   
            data_blocco,   
            flag_esplodi_in_doc )  
  values (:s_cs_xx.cod_azienda,   
          :ls_cod_prodotto_destinazione,   
          :ls_cod_versione_destinazione,   
          :s_cs_xx.cod_utente,   
          null,   
          :ls_des_versione,   
          :ldt_oggi,   
          null,   
          'N',   
          'N',   
          null,   
          'N' )  ;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante la creazione del PADRE in tabella distinta_padri: " + sqlca.sqlerrtext)
	rollback;
	return
end if

if ls_check_duplica_tutto = "S"  then
	li_risposta = f_duplica_distinta(	ls_cod_prodotto_partenza, &
										ls_cod_versione_partenza, &
										ls_cod_prodotto_destinazione, & 
										ls_cod_versione_destinazione, & 
										ls_cod_prodotto_partenza, & 
										ls_cod_prodotto_destinazione, & 
										ls_cod_versione_partenza, & 
										ls_cod_versione_destinazione, & 
										1, & 
										ls_flag_duplica_varianti, &
										ls_flag_duplica_formule, &
										ls_errore)
	
	if li_risposta < 0 then
		rollback;
		g_mb.error(ls_errore)
		return
	end if
	
else
	
	
	guo_functions.uof_get_parametro_azienda("GIB", lb_GIB)
	
	if lb_GIB then
		//Donato 27/03/2014
		//per GIBUS ragiona diversamente (SR Varie_per_produzione)
		if wf_duplica_gibus(ls_cod_prodotto_partenza, ls_cod_versione_partenza, ls_cod_prodotto_destinazione, ls_cod_versione_destinazione, ls_errore) < 0 then
			rollback;
			g_mb.error(ls_errore)
			return
		end if
		
		//commit fatto sotto
	else
	
		//inserimento in tabella distinta dei componenti di primo livello del prodotto finito
		  INSERT INTO distinta  
				( cod_azienda,   
				  cod_prodotto_padre,   
				  num_sequenza,   
				  cod_prodotto_figlio,   
				  cod_versione,   
				  cod_versione_figlio,   
				  cod_misura,   
				  quan_tecnica,   
				  quan_utilizzo,   
				  fase_ciclo,   
				  dim_x,   
				  dim_y,   
				  dim_z,   
				  dim_t,   
				  coef_calcolo,   
				  des_estesa,   
				  formula_tempo,   
				  cod_formula_quan_utilizzo,   
				  flag_escludibile,   
				  flag_materia_prima,   
				  flag_arrotonda_bl,   
				  flag_ramo_descrittivo,   
				  lead_time )  
		  SELECT cod_azienda,   
					:ls_cod_prodotto_destinazione,   
					num_sequenza,   
					cod_prodotto_figlio,   
					:ls_cod_versione_destinazione,   
					cod_versione_figlio,   
					cod_misura,   
					quan_tecnica,   
					quan_utilizzo,   
					fase_ciclo,   
					dim_x,   
					dim_y,   
					dim_z,   
					dim_t,   
					coef_calcolo,   
					des_estesa,   
					formula_tempo,   
					cod_formula_quan_utilizzo,   
					flag_escludibile,   
					flag_materia_prima,   
					flag_arrotonda_bl,   
					flag_ramo_descrittivo,   
					lead_time  
			 FROM distinta  
			WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
					cod_prodotto_padre = :ls_cod_prodotto_partenza AND  
					cod_versione = :ls_cod_versione_partenza ;
					
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in duplicazione: "+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		// duplico anche le distinte di taglio se presenti
		setnull(ls_flag_distinte_taglio)
		
		select stringa
		into 	:ls_flag_distinte_taglio
		from	parametri
		where cod_parametro = 'DT';
		
		if not isnull(ls_flag_distinte_taglio) and ls_flag_distinte_taglio = "S" then
			
			  INSERT INTO tab_distinte_taglio  
				( cod_azienda,   
				  cod_prodotto_padre,   
				  num_sequenza,   
				  cod_prodotto_figlio,   
				  cod_versione,   
				  cod_versione_figlio,   
				  progressivo,   
				  des_distinta_taglio,   
				  um_quantita,   
				  formula_quantita,   
				  um_misura,   
				  formula_misura,   
				  tutti_comandi )  
			  SELECT cod_azienda,   
						:ls_cod_prodotto_destinazione,   
						num_sequenza,   
						cod_prodotto_figlio,   
						:ls_cod_versione_destinazione,   
						cod_versione_figlio,   
						progressivo,   
						des_distinta_taglio,   
						um_quantita,   
						formula_quantita,   
						um_misura,   
						formula_misura,   
						tutti_comandi  
				 FROM tab_distinte_taglio 
				WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
						cod_prodotto_padre = :ls_cod_prodotto_partenza AND  
						cod_versione = :ls_cod_versione_partenza ;
			 
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore in duplicazione tabelle di taglio: "+sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			  INSERT INTO distinte_taglio_comandi
				( cod_azienda,   
				  cod_prodotto_padre,   
				  num_sequenza,   
				  cod_prodotto_figlio,   
				  cod_versione,   
				  cod_versione_figlio,   
				  progressivo,   
			  cod_comando )  
			  SELECT cod_azienda,   
						:ls_cod_prodotto_destinazione,   
						num_sequenza,   
						cod_prodotto_figlio,   
						:ls_cod_versione_destinazione,   
						cod_versione_figlio,   
						progressivo,
						cod_comando
				 FROM distinte_taglio_comandi
				WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
						cod_prodotto_padre = :ls_cod_prodotto_partenza AND  
						cod_versione = :ls_cod_versione_partenza ;
			 
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore in duplicazione tabelle di taglio dei comandi:"+sqlca.sqlerrtext)
				rollback;
				return
			end if
			
		end if
		
		// duplico anche le formule legami
		li_risposta = wf_duplica_legami(	ls_cod_prodotto_partenza,  &
												ls_cod_versione_partenza, &
												ls_cod_prodotto_destinazione, &
												ls_cod_versione_destinazione, &
												'N',&
												ref ls_errore)
		if sqlca.sqlcode < 0 then
			rollback;
			g_mb.error(ls_errore)
			return
		end if

	end if

end if

commit;

g_mb.success("Duplicazione della distinta base avvenuta con successo!")

end subroutine

public subroutine wf_duplica_legami ();string ls_cod_prodotto_partenza, ls_cod_versione_partenza,ls_cod_prodotto_destinazione,ls_cod_versione_destinazione, ls_errore
long ll_ret


ls_cod_prodotto_partenza = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"rs_cod_prodotto")
ls_cod_versione_partenza = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_versione")
ls_cod_prodotto_destinazione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"rs_cod_prodotto_destinazione")
ls_cod_versione_destinazione = dw_ricerca_origine.getitemstring(dw_ricerca_origine.getrow(),"cod_versione_destinazione")

if g_mb.messagebox("SEP", "Copio le formule legami del prodotto " +  ls_cod_prodotto_partenza + " Vers.:" +  ls_cod_versione_partenza + &
								" sovrascrivendo le formule legami del prodotto " + ls_cod_prodotto_destinazione + " Vers.:" +  ls_cod_versione_destinazione + " ?",Question!, YesNo!, 2) = 2 then
	return
end if

ll_ret = wf_duplica_legami(	ls_cod_prodotto_partenza, &
						ls_cod_versione_partenza, &
						ls_cod_prodotto_destinazione, &
						ls_cod_versione_destinazione,&
						'S', &
						ref ls_errore)

if ll_ret < 0 then
	rollback;
	g_mb.messagebox("APICE",ls_errore)
else
	commit;
end if

g_mb.messagebox("SEP", "Le formule legami del prodotto " + ls_cod_prodotto_destinazione + " Vers.:" + ls_cod_versione_destinazione + " sono state sovrascritte!", Information!) 

return
end subroutine

public function long wf_duplica_legami (string fs_cod_prodotto_origine, string fs_cod_versione_origine, string fs_cod_prodotto_destinazione, string fs_cod_versione_destinazione, string fs_flag_sovrascrivi, ref string fs_errore);string ls_cod_prodotto_padre, ls_cod_versione_padre, ls_cod_prodotto_figlio, ls_cod_versione_figlio, ls_cod_formula, ls_nota_operativa, ls_note,ls_flag_blocco
long ll_rows, ll_i
datetime ldt_data_blocco
datastore lds_legami

lds_legami = CREATE datastore
lds_legami.dataobject = 'd_ds_duplica_distinta_formule_legami'
lds_legami.settransobject( sqlca )

ll_rows = lds_legami.retrieve( s_cs_xx.cod_azienda, fs_cod_prodotto_origine,fs_cod_versione_origine)

delete 	tab_formule_legami
where 	cod_azienda 			= :s_cs_xx.cod_azienda and
		cod_prodotto_finito 	= :fs_cod_prodotto_destinazione and
		cod_versione 		= :fs_cod_versione_destinazione;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in cancellazione formule legami del prodotto di destinazione.~r~n" + sqlca.sqlerrtext
	return -1
end if



for ll_i = 1 to ll_rows
	
	ls_cod_prodotto_padre = lds_legami.getitemstring(ll_i, "cod_prodotto_padre")
	ls_cod_versione_padre =  lds_legami.getitemstring(ll_i, "cod_prodotto_padre")
	ls_cod_prodotto_figlio =  lds_legami.getitemstring(ll_i, "cod_prodotto_figlio")
	ls_cod_versione_figlio =  lds_legami.getitemstring(ll_i, "cod_versione_figlio")
	ls_cod_formula =  lds_legami.getitemstring(ll_i, "cod_formula")
	ls_nota_operativa =  lds_legami.getitemstring(ll_i, "nota_operativa")
	ls_note =  lds_legami.getitemstring(ll_i, "note")
	ls_flag_blocco =  lds_legami.getitemstring(ll_i, "flag_blocco")
	ldt_data_blocco =  lds_legami.getitemdatetime(ll_i, "data_blocco")
	
	if upper( right( fs_cod_prodotto_destinazione, 1)) <> "X" and upper( right( fs_cod_prodotto_destinazione, 1)) <> "Y" then
		choose case upper( right( ls_cod_prodotto_padre, 1))
			case "X"
				ls_cod_prodotto_padre = fs_cod_prodotto_destinazione + "X"
			case "Y"
				ls_cod_prodotto_padre = fs_cod_prodotto_destinazione + "Y"
			case else
				ls_cod_prodotto_padre = fs_cod_prodotto_destinazione
		end choose
		
	else
		ls_cod_prodotto_padre = fs_cod_prodotto_destinazione
		
	end if
	
	INSERT INTO tab_formule_legami  
		( cod_azienda,   
		  cod_prodotto_finito,   
		  cod_versione,   
		  cod_prodotto_padre,   
		  cod_versione_padre,   
		  cod_prodotto_figlio,   
		  cod_versione_figlio,   
		  cod_formula,   
		  nota_operativa,   
		  note,   
		  flag_blocco,   
		  data_blocco )  
	VALUES ( :s_cs_xx.cod_azienda,   
		  :fs_cod_prodotto_destinazione,   
		  :fs_cod_versione_destinazione,   
		  :ls_cod_prodotto_padre,   
		  :fs_cod_versione_origine,   
		  :ls_cod_prodotto_figlio,   
		  :fs_cod_versione_origine,   
		  :ls_cod_formula,   
		  :ls_nota_operativa,   
		  :ls_note,   
		  :ls_flag_blocco,   
		  :ldt_data_blocco )  ;
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in duplicazione formule legami.~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
next

return 0
end function

public function integer wf_duplica_excel (ref string fs_errore);string ls_path_file, ls_nome_foglio, ls_tipo
string ls_cod_prodotto_origine, ls_cod_versione_origine, ls_cod_prodotto_dest, ls_cod_versione_dest
long ll_errore, ll_riga, ll_count
decimal ld_valore
OLEObject lole_excel

dw_ricerca_origine.accepttext()

//il file deve avere la seguente struttura
//INTESTAZIONE fatta da 
	//COD.PRODOTTO ORIGINE - COD.VERSIONE ORIGINE - COD.PRODOTTO DESTINAZIONE - COD.VERSIONE DESTINAZIONE
//a seguire le righe dei dati, quindi si comincia a leggere dalla seconda riga ...

ls_path_file = dw_ricerca_origine.getitemstring(1, "path_file")
if isnull(ls_path_file) or ls_path_file="" then
	fs_errore = "Specificare un file excel da importare!"
	return -1
end if

ls_nome_foglio = dw_ricerca_origine.getitemstring(1, "nome_foglio")
if isnull(ls_nome_foglio) or ls_nome_foglio="" then
	fs_errore = "Specificare il nome del foglio dati excel da cui importare i dati!"
	return -1
end if

if g_mb.confirm("Effettuare la duplicazione delle distinte come specificato nel file excel '"+ls_path_file+"' ?") then
else
	fs_errore = "Operazione interrotta dall'utente!"
	return 0
end if


//se arrivi fin qui prosegui

setpointer(Hourglass!)

//inizio
lole_excel = create OLEObject
ll_errore = lole_excel.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	fs_errore = "Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza!"
	return -1
end if

lole_excel.Application.Workbooks.Open(ls_path_file)
lole_excel.application.visible = true
ll_riga = 1

do while true	
	ll_riga += 1

	// 1. leggo prodotto origine
	guo_functions.uof_get_value(ls_nome_foglio,ll_riga, 1, ls_cod_prodotto_origine, ld_valore, ls_tipo, lole_excel)
	
	//vedo se sono giunto alla fine della lettura
	if isnull(ls_cod_prodotto_origine) or len(ls_cod_prodotto_origine) < 1 then exit
	
	// 2. leggo versione origine
	guo_functions.uof_get_value(ls_nome_foglio,ll_riga, 2, ls_cod_versione_origine, ld_valore, ls_tipo, lole_excel)
	if isnull(ls_cod_versione_origine) or ls_cod_versione_origine="" then
		fs_errore = "Versione origine non specificata nel foglio excel (riga: "+string(ll_riga)+")"
		destroy lole_excel;
		setpointer(Arrow!)
		
		return -1
	end if
	
	// 3. leggo prodotto destinazione
	guo_functions.uof_get_value(ls_nome_foglio,ll_riga, 3, ls_cod_prodotto_dest, ld_valore, ls_tipo, lole_excel)
	if isnull(ls_cod_prodotto_dest) or ls_cod_prodotto_dest="" then
		fs_errore = "Prodotto destinazione non specificato nel foglio excel (riga: "+string(ll_riga)+")"
		destroy lole_excel;
		setpointer(Arrow!)
		
		return -1
	end if
	
	// 4. leggo versione destinazione
	guo_functions.uof_get_value(ls_nome_foglio,ll_riga, 4, ls_cod_versione_dest, ld_valore, ls_tipo, lole_excel)
	if isnull(ls_cod_versione_dest) or ls_cod_versione_dest="" then
		fs_errore = "Versione destinazione non specificata nel foglio excel (riga: "+string(ll_riga)+")"
		destroy lole_excel;
		setpointer(Arrow!)
		
		return -1
	end if
	
	//INIZIO IMPORTAZIONE
	//verifico se già esiste in distinta_padri (se esiste non inserisco ma continuo per i semilavorati di primo livello, altrimenti duplico distinta_padri)
	setnull(ll_count)
	
	select count(*)
	into :ll_count
	from distinta_padri
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:ls_cod_prodotto_dest and
				cod_versione=:ls_cod_versione_dest;
				
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in controllo esistenza distinta padre per prodotto '"+ls_cod_prodotto_dest+"' non specificata nel foglio excel (riga: "+string(ll_riga)+"): "+sqlca.sqlerrtext
		destroy lole_excel;
		setpointer(Arrow!)
		
		return -1
	end if
	
	
	if ll_count>0 then
		//già esiste la distinta padre
	else
		//non esiste, copiala:
		
		//controllo prima che il record da cui copiare effettivamente sia presente
		setnull(ll_count)
	
		select count(*)
		into :ll_count
		from distinta_padri
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:ls_cod_prodotto_origine and
					cod_versione=:ls_cod_versione_origine;
					
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in controllo  distinta padre originale per prodotto '"+ls_cod_prodotto_dest+"' nel foglio excel (riga: "+string(ll_riga)+"): "+sqlca.sqlerrtext
			destroy lole_excel;
			setpointer(Arrow!)
			
			return -1
		end if
		
		if ll_count>0 then
			//OK
		else
			fs_errore = "La distina padre del prodotto '"+ls_cod_prodotto_origine+"' versione '"+ls_cod_versione_origine+"' non è presente nel database (riga: "+string(ll_riga)+")"
			destroy lole_excel;
			setpointer(Arrow!)
			
			return -1
		end if
		
		//-------------------
		
		insert into distinta_padri  
					( cod_azienda,   
					  cod_prodotto,   
					  cod_versione,   
					  creato_da,   
					  approvato_da,   
					  des_versione,   
					  data_creazione,   
					  data_approvazione,   
					  flag_predefinita,   
					  flag_blocco,   
					  data_blocco,   
					  flag_esplodi_in_doc,   
					  flag_non_calcolare_dt,   
					  flag_chiusura_automatica )  
				select :s_cs_xx.cod_azienda,   
						:ls_cod_prodotto_dest,   
						:ls_cod_versione_dest,   
						creato_da,   
						approvato_da,   
						des_versione,   
						data_creazione,   
						data_approvazione,   
						flag_predefinita,   
						flag_blocco,   
						data_blocco,   
						flag_esplodi_in_doc,   
						flag_non_calcolare_dt,   
						flag_chiusura_automatica
				from distinta_padri
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							cod_prodotto=:ls_cod_prodotto_origine and
							cod_versione=:ls_cod_versione_origine;

		if sqlca.sqlcode<0 then
			fs_errore = "Errore in inserimento distinta padre originale per il prodotto '"+ls_cod_prodotto_dest+"' nel foglio excel (riga: "+string(ll_riga)+"): "+sqlca.sqlerrtext
			destroy lole_excel;
			setpointer(Arrow!)
			
			return -1
		end if
	end if
				
	
	//leggo i semilavorati di primo livello di prodotto-versione origine e li inserisco come semilavorati di prodotto-versione destinazione
	//NOTA (se qualche combinazione già esiste allora la ignoro: questo mi da la possibilità di rilanciare la procedura senza problemi)
	if wf_duplica_semilavorati_excel(ls_cod_prodotto_origine, ls_cod_versione_origine, ls_cod_prodotto_dest, ls_cod_versione_dest, fs_errore) < 0 then
		//in fs_errore il messaggio
		fs_errore = "Riga: " + string(ll_riga) + " - " +  fs_errore
		
		destroy lole_excel;
		setpointer(Arrow!)
			
		return -1
	end if
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + "... Duplicazione distinta da " &
						+ ls_cod_prodotto_origine+"/"+ls_cod_versione_origine + " a " +&
						+ ls_cod_prodotto_dest+"/"+ls_cod_versione_dest	+ "  Eseguita !!! - ")
	Yield()
	
loop

lole_excel.Application.Quit()
destroy lole_excel;


setpointer(Arrow!)


return 1
end function

public subroutine wf_path_excel (ref string fs_path);string		ls_path, docpath, docname[]
integer	li_count, li_ret

fs_path = ""

ls_path = s_cs_xx.volume
li_ret = GetFileOpenName("Seleziona File XLS da importare", docpath, docname[], "DOC", 	+ "Files Excel (*.XLS),*.XLS,", 	ls_path)

if li_ret < 1 then return
li_count = Upperbound(docname)

if li_count = 1 then
	fs_path = docpath
end if
end subroutine

public function integer wf_duplica_semilavorati_excel (string fs_cod_prodotto_origine, string fs_cod_versione_origine, string fs_cod_prodotto_dest, string fs_cod_versione_dest, ref string fs_errore);long ll_num_sequenza, ll_count
string ls_cod_prodotto_figlio, ls_cod_versione_figlio

declare cu_distinta cursor for  
	select num_sequenza,
			cod_prodotto_figlio,
			cod_versione_figlio
	from distinta
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto_padre=:fs_cod_prodotto_origine and
				cod_versione=:fs_cod_versione_origine
	order by num_sequenza;

open cu_distinta;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in OPEN cursore 'cu_distinta'~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_distinta into 	:ll_num_sequenza,
								:ls_cod_prodotto_figlio,
								:ls_cod_versione_figlio;

	if sqlca.sqlcode < 0 then
		close cu_distinta;
		fs_errore = "Errore in FETCH cursore 'cu_distinta'~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	//verifica se già esiste
	setnull(ll_count)
	
	select count(*)
	into :ll_count
	from distinta
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto_padre=:fs_cod_prodotto_dest and
				num_sequenza=:ll_num_sequenza and
				cod_prodotto_figlio=:ls_cod_prodotto_figlio and
				cod_versione=:fs_cod_versione_dest and
				cod_versione_figlio=:ls_cod_versione_figlio;
				
	if sqlca.sqlcode < 0 then
		close cu_distinta;
		fs_errore = "Errore in controllo esistenza distinta per semilavorato "+ls_cod_prodotto_figlio+" versione "+ls_cod_versione_figlio+"~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if ll_count>0 then continue
	
	insert into distinta  
			( cod_azienda,   
			cod_prodotto_padre,   
			num_sequenza,   
			cod_prodotto_figlio,   
			cod_versione,   
			cod_versione_figlio,   
			cod_misura,   
			quan_tecnica,   
			quan_utilizzo,   
			fase_ciclo,   
			dim_x,   
			dim_y,   
			dim_z,   
			dim_t,   
			coef_calcolo,   
			des_estesa,   
			formula_tempo,   
			cod_formula_quan_utilizzo,   
			flag_escludibile,   
			flag_materia_prima,   
			flag_arrotonda_bl,   
			lead_time,   
			flag_ramo_descrittivo,   
			flag_valorizza_mp,   
			flag_variante_vincol )  
		select			:s_cs_xx.cod_azienda,   
						:fs_cod_prodotto_dest,   
						distinta.num_sequenza,   
						distinta.cod_prodotto_figlio,   
						:fs_cod_versione_dest,   
						distinta.cod_versione_figlio,   
						distinta.cod_misura,   
						distinta.quan_tecnica,   
						distinta.quan_utilizzo,   
						distinta.fase_ciclo,   
						distinta.dim_x,   
						distinta.dim_y,   
						distinta.dim_z,   
						distinta.dim_t,   
						distinta.coef_calcolo,   
						distinta.des_estesa,   
						distinta.formula_tempo,   
						distinta.cod_formula_quan_utilizzo,   
						distinta.flag_escludibile,   
						distinta.flag_materia_prima,   
						distinta.flag_arrotonda_bl,   
						distinta.lead_time,   
						distinta.flag_ramo_descrittivo,   
						distinta.flag_valorizza_mp,   
						distinta.flag_variante_vincol  
		from distinta
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto_padre=:fs_cod_prodotto_origine and
					num_sequenza=:ll_num_sequenza and
					cod_prodotto_figlio=:ls_cod_prodotto_figlio and
					cod_versione=:fs_cod_versione_origine and
					cod_versione_figlio=:ls_cod_versione_figlio;
		
		if sqlca.sqlcode < 0 then
			close cu_distinta;
			fs_errore = "Errore in inserimento in distinta del semilavorato "+ls_cod_prodotto_figlio+" versione "+ls_cod_versione_figlio+"~r~n" + sqlca.sqlerrtext
			return -1
		end if
	
	
loop
close cu_distinta;

return 1
end function

public function integer wf_duplica_gibus (string as_cod_prodotto_partenza, string as_cod_versione_partenza, string as_cod_prodotto_destinazione, string as_cod_versione_destinazione, ref string as_errore);string			ls_flag_distinte_taglio, ls_sql
datastore	lds_data
long			ll_count, ll_index, ll_conteggio

long			ll_num_sequenza_ds, ll_progressivo_ds
string			ls_cod_prodotto_figlio_ds, ls_cod_versione_figlio_ds, ls_cod_misura_ds, ls_des_estesa_ds,ls_formula_tempo_ds,ls_cod_formula_quan_utilizzo_ds, &
				ls_flag_escludibile_ds,ls_flag_materia_prima_ds,ls_flag_arrotonda_bl_ds,ls_flag_ramo_descrittivo_ds, ls_cod_formula_quan_tecnica_ds, &
				ls_cod_formula_prod_figlio_ds, ls_cod_formula_vers_figlio_ds, ls_des_distinta_taglio_ds, ls_um_quantita_ds, ls_formula_quantita_ds, &
				ls_um_misura_ds, ls_formula_misura_ds, ls_tutti_comandi_ds, ls_cod_prodotto_padre_ds, ls_cod_versione_padre_ds, ls_cod_formula_ds, &
				ls_nota_operativa_ds, ls_note_ds, ls_flag_blocco_ds
				
datetime		ldt_data_blocco_ds

dec{4}		ld_quan_tecnica_ds, ld_quan_utilizzo_ds,ld_fase_ciclo_ds,ld_dim_x_ds,ld_dim_y_ds,ld_dim_z_ds,ld_dim_t_ds,ld_coef_calcolo_ds, &
				ld_lead_time_ds


//duplicazione componenti primo livello in distinta base
ls_sql = "select	num_sequenza,"+&
					"cod_prodotto_figlio,"+&
					"cod_versione_figlio,"+&
					"cod_misura,"+&
					"quan_tecnica,"+&
					"quan_utilizzo,"+&
					"fase_ciclo,"+&
					"dim_x,dim_y,dim_z,dim_t,"+&
					"coef_calcolo,"+&
					"des_estesa,"+&
					"formula_tempo,"+&
					"cod_formula_quan_utilizzo,"+&
					"flag_escludibile,"+&
					"flag_materia_prima,"+&
					"flag_arrotonda_bl,"+&
					"flag_ramo_descrittivo,"+&
					"lead_time,"+&
					"cod_formula_quan_tecnica,"+&
					"cod_formula_prod_figlio,"+&
					"cod_formula_vers_figlio "+&
			"from distinta "+&
			"where cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
					"cod_prodotto_padre = '"+as_cod_prodotto_partenza+"' and "+&
					"cod_versione = '"+as_cod_versione_partenza+"'"

ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_count<0 then
	return -1
end if

for ll_index=1 to ll_count
	ll_num_sequenza_ds = lds_data.getitemnumber(ll_index, "num_sequenza")
	ls_cod_prodotto_figlio_ds = lds_data.getitemstring(ll_index, "cod_prodotto_figlio")
	ls_cod_versione_figlio_ds = lds_data.getitemstring(ll_index, "cod_versione_figlio")
	
	//se il figlio ha una distinta usa la versione predefinita
	if wf_get_versione(ls_cod_prodotto_figlio_ds, ls_cod_versione_figlio_ds, as_errore)<0 then
		destroy lds_data
		return -1
	end if
	
	ls_cod_misura_ds = lds_data.getitemstring(ll_index, "cod_misura")
	ld_quan_tecnica_ds = lds_data.getitemnumber(ll_index, "quan_tecnica")
	ld_quan_utilizzo_ds = lds_data.getitemnumber(ll_index, "quan_utilizzo")
	ld_fase_ciclo_ds = lds_data.getitemnumber(ll_index, "fase_ciclo")
	ld_dim_x_ds = lds_data.getitemnumber(ll_index, "dim_x")
	ld_dim_y_ds = lds_data.getitemnumber(ll_index, "dim_y")
	ld_dim_z_ds = lds_data.getitemnumber(ll_index, "dim_z")
	ld_dim_t_ds = lds_data.getitemnumber(ll_index, "dim_t")
	ld_coef_calcolo_ds = lds_data.getitemnumber(ll_index, "coef_calcolo")
	ls_des_estesa_ds = lds_data.getitemstring(ll_index, "des_estesa")
	ls_formula_tempo_ds = lds_data.getitemstring(ll_index, "formula_tempo")
	ls_cod_formula_quan_utilizzo_ds = lds_data.getitemstring(ll_index, "cod_formula_quan_utilizzo")
	ls_flag_escludibile_ds = lds_data.getitemstring(ll_index, "flag_escludibile")
	ls_flag_materia_prima_ds = lds_data.getitemstring(ll_index, "flag_materia_prima")
	ls_flag_arrotonda_bl_ds = lds_data.getitemstring(ll_index, "flag_arrotonda_bl")
	ls_flag_ramo_descrittivo_ds = lds_data.getitemstring(ll_index, "flag_ramo_descrittivo")
	ld_lead_time_ds = lds_data.getitemnumber(ll_index, "lead_time")
	ls_cod_formula_quan_tecnica_ds = lds_data.getitemstring(ll_index, "cod_formula_quan_tecnica")
	ls_cod_formula_prod_figlio_ds = lds_data.getitemstring(ll_index, "cod_formula_prod_figlio")
	ls_cod_formula_vers_figlio_ds = lds_data.getitemstring(ll_index, "cod_formula_vers_figlio")
	
	//inserimento in tabella distinta dei componenti di primo livello del prodotto finito
	insert into distinta  
         ( cod_azienda,   
           cod_prodotto_padre,   
           num_sequenza,   
           cod_prodotto_figlio,   
           cod_versione,   
           cod_versione_figlio,   
           cod_misura,   
           quan_tecnica,   
           quan_utilizzo,   
           fase_ciclo,   
           dim_x,   
           dim_y,   
           dim_z,   
           dim_t,   
           coef_calcolo,   
           des_estesa,   
           formula_tempo,   
           cod_formula_quan_utilizzo,   
           flag_escludibile,   
           flag_materia_prima,   
           flag_arrotonda_bl,   
           flag_ramo_descrittivo,   
           lead_time,
		  cod_formula_quan_tecnica,
		  cod_formula_prod_figlio,
		  cod_formula_vers_figlio)  
     values	(	:s_cs_xx.cod_azienda,   
					:as_cod_prodotto_destinazione,   
					:ll_num_sequenza_ds,   
					:ls_cod_prodotto_figlio_ds,   
					:as_cod_versione_destinazione,   
					:ls_cod_versione_figlio_ds,   
					:ls_cod_misura_ds,   
					:ld_quan_tecnica_ds,   
					:ld_quan_utilizzo_ds,   
					:ld_fase_ciclo_ds,   
					:ld_dim_x_ds,   
					:ld_dim_y_ds,   
					:ld_dim_z_ds,   
					:ld_dim_t_ds,   
					:ld_coef_calcolo_ds,   
					:ls_des_estesa_ds,   
					:ls_formula_tempo_ds,   
					:ls_cod_formula_quan_utilizzo_ds,   
					:ls_flag_escludibile_ds,   
					:ls_flag_materia_prima_ds,   
					:ls_flag_arrotonda_bl_ds,   
					:ls_flag_ramo_descrittivo_ds,   
					:ld_lead_time_ds,
					:ls_cod_formula_quan_tecnica_ds,
					:ls_cod_formula_prod_figlio_ds,
					:ls_cod_formula_vers_figlio_ds);
				
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in inserimento componente ("+ls_cod_prodotto_figlio_ds+" - "+ls_cod_versione_figlio_ds+"): "+sqlca.sqlerrtext
		destroy lds_data
		return -1
	end if
next

destroy lds_data


//-------------------------------------------------------------------------------------------------------------
// duplico anche le distinte di taglio se presenti nei componenti di primo livello
setnull(ls_flag_distinte_taglio)
	
select stringa
into 	:ls_flag_distinte_taglio
from	parametri
where cod_parametro = 'DT';

if not isnull(ls_flag_distinte_taglio) and ls_flag_distinte_taglio = "S" then
	ls_sql = "select num_sequenza,"+&
					"cod_prodotto_figlio,"+&
					"cod_versione_figlio,"+&
					"progressivo,"+&
					"des_distinta_taglio,"+&
					"um_quantita,"+&
					"formula_quantita,"+&
					"um_misura,"+&
					"formula_misura,"+&
					"tutti_comandi "+&
			 	"from tab_distinte_taglio "+&
				"where 	cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
							"cod_prodotto_padre = '"+as_cod_prodotto_partenza+"' and "+&
							"cod_versione = '"+as_cod_versione_partenza+"'"
	
	ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
	if ll_count<0 then
		return -1
	end if

	for ll_index=1 to ll_count
		ll_num_sequenza_ds = lds_data.getitemnumber(ll_index, "num_sequenza")
		ls_cod_prodotto_figlio_ds = lds_data.getitemstring(ll_index, "cod_prodotto_figlio")
		ls_cod_versione_figlio_ds = lds_data.getitemstring(ll_index, "cod_versione_figlio")
		
		//se il figlio ha una distinta usa la versione predefinita
		if wf_get_versione(ls_cod_prodotto_figlio_ds, ls_cod_versione_figlio_ds, as_errore)<0 then
			destroy lds_data
			return -1
		end if
		
		ll_progressivo_ds = lds_data.getitemnumber(ll_index, "progressivo")
		ls_des_distinta_taglio_ds = lds_data.getitemstring(ll_index, "des_distinta_taglio")
		ls_um_quantita_ds = lds_data.getitemstring(ll_index, "um_quantita")
		ls_formula_quantita_ds = lds_data.getitemstring(ll_index, "formula_quantita")
		ls_um_misura_ds = lds_data.getitemstring(ll_index, "um_misura")
		ls_formula_misura_ds = lds_data.getitemstring(ll_index, "formula_misura")
		ls_tutti_comandi_ds = lds_data.getitemstring(ll_index, "tutti_comandi")

		//inserimento in tabella distinte_taglio delle formule dei componenti di primo livello del prodotto finito
		 insert into tab_distinte_taglio  
         ( cod_azienda,   
           cod_prodotto_padre,   
           num_sequenza,   
           cod_prodotto_figlio,   
           cod_versione,   
           cod_versione_figlio,   
           progressivo,   
           des_distinta_taglio,   
           um_quantita,   
           formula_quantita,   
           um_misura,   
           formula_misura,   
           tutti_comandi )  
		  values	(	:s_cs_xx.cod_azienda,
						:as_cod_prodotto_destinazione,   
						:ll_num_sequenza_ds,   
						:ls_cod_prodotto_figlio_ds,   
						:as_cod_versione_destinazione,   
						:ls_cod_versione_figlio_ds,   
						:ll_progressivo_ds,   
						:ls_des_distinta_taglio_ds,   
						:ls_um_quantita_ds,   
						:ls_formula_quantita_ds,   
						:ls_um_misura_ds,   
						:ls_formula_misura_ds,   
						:ls_tutti_comandi_ds);
		 
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in duplicazione tabelle di taglio del componente ("+ls_cod_prodotto_figlio_ds+" - "+ls_cod_versione_figlio_ds+"): "+sqlca.sqlerrtext
			destroy lds_data
			return -1
		end if
	next
	
	destroy lds_data
end if

	
// duplico anche le formule legami
lds_data = CREATE datastore
lds_data.dataobject = 'd_ds_duplica_distinta_formule_legami'
lds_data.settransobject(sqlca)

delete 	tab_formule_legami
where 	cod_azienda 		= :s_cs_xx.cod_azienda and
		cod_prodotto_finito 	= :as_cod_prodotto_destinazione and
		cod_versione 		     = :as_cod_versione_destinazione;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in cancellazione formule legami del prodotto di destinazione: " + sqlca.sqlerrtext
	destroy lds_data
	return -1
end if

ll_count = lds_data.retrieve( s_cs_xx.cod_azienda, as_cod_prodotto_partenza, as_cod_versione_partenza)

for ll_index = 1 to ll_count	
	ls_cod_prodotto_padre_ds = lds_data.getitemstring(ll_index, "cod_prodotto_padre")
	ls_cod_versione_padre_ds =  lds_data.getitemstring(ll_index, "cod_prodotto_padre")
	
	ls_cod_prodotto_figlio_ds =  lds_data.getitemstring(ll_index, "cod_prodotto_figlio")
	ls_cod_versione_figlio_ds =  lds_data.getitemstring(ll_index, "cod_versione_figlio")
	
	//se il figlio ha una distinta usa la versione predefinita
	if wf_get_versione(ls_cod_prodotto_figlio_ds, ls_cod_versione_figlio_ds, as_errore)<0 then
		destroy lds_data
		return -1
	end if
	
	ls_cod_formula_ds =  lds_data.getitemstring(ll_index, "cod_formula")
	ls_nota_operativa_ds =  lds_data.getitemstring(ll_index, "nota_operativa")
	ls_note_ds =  lds_data.getitemstring(ll_index, "note")
	ls_flag_blocco_ds =  lds_data.getitemstring(ll_index, "flag_blocco")
	ldt_data_blocco_ds =  lds_data.getitemdatetime(ll_index, "data_blocco")
	
	if upper( right( as_cod_prodotto_destinazione, 1)) <> "X" and upper( right( as_cod_prodotto_destinazione, 1)) <> "Y" and upper( right( as_cod_prodotto_destinazione, 1)) <> "Z" then
		//il prodotto destinazione non è un semilavorato X o Y o Z
		choose case upper( right( ls_cod_prodotto_padre_ds, 1))
			case "X"
				//il padre è un X
				ls_cod_prodotto_padre_ds = as_cod_prodotto_destinazione + "X"
			case "Y"
				//il padre è un Y
				ls_cod_prodotto_padre_ds = as_cod_prodotto_destinazione + "Y"
			case "Z"
				//il padre è un Z
				ls_cod_prodotto_padre_ds = as_cod_prodotto_destinazione + "Z"
			
			case else
				//il padre è nè un X nè un Y
				ls_cod_prodotto_padre_ds = as_cod_prodotto_destinazione
		end choose
		
	else
		//il prodotto destinazione è un semilavorato X o Y o Z
		ls_cod_prodotto_padre_ds = as_cod_prodotto_destinazione
	end if
	
	//se il padre ha una distinta usa la versione predefinita
	if wf_get_versione(ls_cod_prodotto_padre_ds, ls_cod_versione_padre_ds, as_errore)<0 then
		destroy lds_data
		return -1
	end if
	
	//se la combinazione destinazione,padre,figlio già esiste in tabella legami non inserirla
	select count(*)
	into :ll_conteggio
	from tab_formule_legami
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto_finito=:as_cod_prodotto_destinazione and
				cod_versione=:as_cod_versione_destinazione and
				cod_prodotto_padre=:ls_cod_prodotto_padre_ds and
				cod_versione_padre=:ls_cod_versione_padre_ds and
				cod_prodotto_figlio=:ls_cod_prodotto_figlio_ds and
				cod_versione_figlio=:ls_cod_versione_figlio_ds;

	if ll_conteggio > 0 then
	else
		//non c'è, quindi inserisco
		insert into tab_formule_legami  
			( cod_azienda,   
			  cod_prodotto_finito,   
			  cod_versione,   
			  cod_prodotto_padre,   
			  cod_versione_padre,   
			  cod_prodotto_figlio,   
			  cod_versione_figlio,   
			  cod_formula,   
			  nota_operativa,   
			  note,   
			  flag_blocco,   
			  data_blocco )  
		values ( :s_cs_xx.cod_azienda,   
			  :as_cod_prodotto_destinazione,   
			  :as_cod_versione_destinazione,   
			  :ls_cod_prodotto_padre_ds,   
			  :ls_cod_versione_padre_ds,   
			  :ls_cod_prodotto_figlio_ds,   
			  :ls_cod_versione_figlio_ds,   
			  :ls_cod_formula_ds,   
			  :ls_nota_operativa_ds,   
			  :ls_note_ds,   
			  :ls_flag_blocco_ds,   
			  :ldt_data_blocco_ds )  ;
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in dup. f.le legami: ("+as_cod_prodotto_destinazione+" - "+as_cod_versione_destinazione+" - "+&
																					ls_cod_prodotto_padre_ds+" - "+ls_cod_versione_padre_ds+" - "+&
																					ls_cod_prodotto_figlio_ds+" - "+ls_cod_versione_figlio_ds+")" + sqlca.sqlerrtext
			destroy lds_data
			return -1
		end if
	end if
	
next

destroy lds_data

return 0
end function

public function integer wf_get_versione (string as_cod_prodotto_figlio, ref string as_cod_versione_figlio, ref string as_errore);integer			li_count
string				ls_cod_versione_predefinita

//se il prodotto figlio ha una versione (distinta_padri), cerca la versione predefinita (se non risulta impostata avvisa l'operatore)

select count(*)
into :li_count
from distinta_padri
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto_figlio;

if sqlca.sqlcode<0 then
	as_errore = "Errore in controllo esistenza distinta base per il prodotto "+as_cod_prodotto_figlio+ " : "+sqlca.sqlerrtext
	return -1
end if

if li_count > 0 then
	//esiste distinta base, recupera la versione predefinita
	select cod_versione
	into :ls_cod_versione_predefinita
	from distinta_padri
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:as_cod_prodotto_figlio and
				flag_predefinita='S';
				
	if sqlca.sqlcode<0 then
		as_errore = "Errore in lettura versione predefinita per il prodotto "+as_cod_prodotto_figlio+ " : "+sqlca.sqlerrtext
		return -1
	end if
	
	if ls_cod_versione_predefinita="" or isnull(ls_cod_versione_predefinita) then
		//no versione predefinita prevista
		as_errore = "manca la versione predefinita per la distinta base del prodotto "+as_cod_prodotto_figlio+ ". Impostare una versione predefinita e riprovare! "
		return -1
	else
		as_cod_versione_figlio = ls_cod_versione_predefinita
		return 0
	end if
	
else
	//no distinta base, quindi lascia invariata la variabile "as_cod_versione_figlio"
	return 0
end if
end function

on w_duplica_distinta.create
int iCurrent
call super::create
this.dw_ricerca_origine=create dw_ricerca_origine
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca_origine
end on

on w_duplica_distinta.destroy
call super::destroy
destroy(this.dw_ricerca_origine)
end on

type dw_ricerca_origine from u_dw_search within w_duplica_distinta
event ue_key pbm_dwnkey
integer x = 27
integer y = 20
integer width = 2857
integer height = 1608
integer taborder = 10
string dataobject = "d_duplica_distinta_selezione"
boolean border = false
end type

event itemchanged;call super::itemchanged;if isvalid(dwo) then
	if dwo.name = "rs_cod_prodotto" then
		
		if not isnull(data) and len(data) > 0 then
	
			f_PO_LoadDDDW_DW(dw_ricerca_origine,"cod_versione",sqlca,&
						  "distinta"," cod_versione ","'VERSIONE ' "+guo_functions.uof_concat_op()+" cod_versione",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto_padre = '" + data + "' ")


			setitem(row, "rs_cod_prodotto_destinazione", data)
		end if
	end if
end if
end event

event buttonclicked;call super::buttonclicked;string ls_errore, ls_path
integer li_ret

if isvalid(dwo) then
	choose case upper(dwo.name)
		
		case "CB_FILE"
			wf_path_excel(ls_path)
			setitem(1, "path_file", ls_path)
		
		case "CB_DUPLICA_EXCEL"
			//Donato 08/02/2012: duplicazione di una distinta da excel (solo il primo livello dei semilavorati + riga distinta padri)
			//-------------------------------------------------------------------------------------------------------------------------------------
			li_ret = wf_duplica_excel(ls_errore)
			if  li_ret <= 0 then
				//errore o operazione annullata dall'utente
				rollback;
				setpointer(Arrow!)
				
				if li_ret<0 then
					//errore
					g_mb.error(ls_errore)
				else  //=0
					//annullata dall'utente
					g_mb.show(ls_errore)
				end if
			else
				commit;
				setpointer(Arrow!)
				g_mb.success("Duplicazione da Excel!", "Operazione effettuata con successo!")
			end if
			return
			//-------------------------------------------------------------------------------------------------------------------------------------
			
		case "CB_DUPLICA"
			wf_duplica()
			
		case "CB_FORMULE_LEGAMI"
			wf_duplica_legami()
			
		case "B_RICERCA_PRODOTTO_DA"		
				guo_ricerca.uof_ricerca_prodotto(dw_ricerca_origine,"rs_cod_prodotto")
		
		case "B_RICERCA_PRODOTTO_A"
			guo_ricerca.uof_ricerca_prodotto(dw_ricerca_origine,"rs_cod_prodotto_destinazione")
			
		end choose
end if

end event

