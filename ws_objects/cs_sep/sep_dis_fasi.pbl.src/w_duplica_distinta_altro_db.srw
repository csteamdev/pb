﻿$PBExportHeader$w_duplica_distinta_altro_db.srw
$PBExportComments$Window di duplicazione DB
forward
global type w_duplica_distinta_altro_db from w_cs_xx_principale
end type
type dw_ricerca_origine from u_dw_search within w_duplica_distinta_altro_db
end type
end forward

global type w_duplica_distinta_altro_db from w_cs_xx_principale
integer width = 3081
integer height = 1468
string title = "Duplicazione Distinta Base su altro DB"
event ue_imposta_ricerca ( )
dw_ricerca_origine dw_ricerca_origine
end type
global w_duplica_distinta_altro_db w_duplica_distinta_altro_db

type variables
OLEObject iole_excel
end variables

forward prototypes
public subroutine wf_get_value (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref string fs_tipo)
public function string wf_nome_cella (long fl_riga, long fl_colonna)
public function integer wf_duplica (string fs_cod_prodotto_partenza, string fs_cod_versione_partenza, ref string fs_errore)
end prototypes

event ue_imposta_ricerca();long ll_PDD

select numero
into :ll_PDD
from parametri_azienda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro='PDD' and 
			flag_parametro='N';

if not isnull(ll_PDD) and ll_PDD>0 then
	dw_ricerca_origine.setitem(dw_ricerca_origine.getrow(), "num_profilo_dest", ll_PDD)
else
	g_mb.show("Parametro az. PDD (numerico) non impostato (lettura profilo DB destinazione)")
end if
end event

public subroutine wf_get_value (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref string fs_tipo);OLEObject		lole_foglio
any				lany_ret
string ls_1

//legge dalla cella (fl_riga, fl_colonna) del foglio di lavoro fs_foglio
/*
prova prima con valore decimale: 	se ok torna il valore in fd_valore e mette fs_tipo="D"
poi prova con valore string:				se ok torna il valore in fs_valore e mette fs_tipo="S"

se proprio non riesce a leggere mette a fs_tipo=NULL e ritorna
*/

fs_tipo = ""
setnull(fd_valore)
setnull(fs_valore)

lole_foglio = iole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)
ls_1 = wf_nome_cella(fl_riga, fl_colonna)

//lany_ret = lole_foglio.range(wf_nome_cella(fl_riga, fl_colonna) + ":" + wf_nome_cella(fl_riga , fl_colonna) ).value
lany_ret = lole_foglio.cells[fl_riga,fl_colonna].value

if isnull(lany_ret) then 
	fs_tipo = "S"
	setnull(fs_valore)
	return
end if

try
	//prova con valore stringa
	fs_valore = lany_ret
	fs_tipo = "S"
catch (RuntimeError rte2)
	setnull(fs_valore)
	setnull(fs_tipo)
end try


//try
//	//prova con valore decimale
//	fd_valore = lany_ret
//	fs_tipo = "D"
//catch (RuntimeError rte1)
//	
//	setnull(fd_valore)
//	setnull(fs_tipo)
//	try
//		//prova con valore stringa
//		fs_valore = lany_ret
//		fs_tipo = "S"
//	catch (RuntimeError rte2)
//		setnull(fs_valore)
//		setnull(fs_tipo)
//	end try
//	
//end try
end subroutine

public function string wf_nome_cella (long fl_riga, long fl_colonna);string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if fl_colonna > 26 then
	ll_resto = mod(fl_colonna, 26)
	ll_i = long(fl_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	fl_colonna = ll_resto 
end if

if fl_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[fl_colonna] + string(fl_riga)
end if	

return ls_char
end function

public function integer wf_duplica (string fs_cod_prodotto_partenza, string fs_cod_versione_partenza, ref string fs_errore);uo_dup_diba_gibus 		luo_duplica
string 						ls_errore, ls_cod_prodotto_destinazione, &
								ls_cod_versione_destinazione, ls_des_versione, ls_chiave_reg, ls_ini_section, ls_appo
integer						li_ret
long							ll_row, ll_num_profilo_dest



dw_ricerca_origine.accepttext()

ll_row = dw_ricerca_origine.getrow()

ls_cod_prodotto_destinazione = fs_cod_prodotto_partenza
//versione di arrivo
ls_cod_versione_destinazione = dw_ricerca_origine.getitemstring(ll_row,"cod_versione_destinazione")
ls_des_versione = dw_ricerca_origine.getitemstring(ll_row,"des_versione")
ll_num_profilo_dest = dw_ricerca_origine.getitemnumber(ll_row,"num_profilo_dest")
ls_chiave_reg = dw_ricerca_origine.getitemstring(ll_row,"chiave_reg")

if fs_cod_prodotto_partenza="" or isnull(fs_cod_prodotto_partenza) then
	g_mb.error("Indicare il Prodotto Origine")
	return -1
end if
if fs_cod_versione_partenza="" or isnull(fs_cod_versione_partenza) then
	g_mb.error("Indicare la Versione Origine")
	return -1
end if
if ls_cod_versione_destinazione="" or isnull(ls_cod_versione_destinazione) then
	g_mb.error("Indicare la Versione Destinazione")
	return -1
end if
if ls_des_versione="" or isnull(ls_des_versione) then
	if not g_mb.confirm("Manca la descrizione della nuova versione. Continuare?") then
		return -1
	end if
end if

if ll_num_profilo_dest>0 then
else
	g_mb.error("Indicare il numero del profilo DB Destinazione")
	return -1
end if

ls_ini_section = "database_"+string(ll_num_profilo_dest)
li_ret = registryget(ls_chiave_reg + ls_ini_section, "servername", ls_appo)
if li_ret = -1 then
	g_mb.error("Controllare la chiave di registro "+ls_chiave_reg + ls_ini_section)
	return -1
end if


luo_duplica = create uo_dup_diba_gibus

//specifico quale profilo contiene la connessione al db di destinazione
luo_duplica.is_ini_section = ls_ini_section

li_ret = luo_duplica.uof_duplica_distinta( 	fs_cod_prodotto_partenza, &
														fs_cod_versione_partenza, &
														ls_cod_prodotto_destinazione, &
														ls_cod_versione_destinazione, &
														ls_des_versione, &
														dw_ricerca_origine, &
														ls_errore)
if li_ret<0 then
	//rollback già fatto nella funzione, mostra solo il messaggio di errore
	fs_errore = ls_errore
	return -1
else
	//commit già fatto nella funzione
//	g_mb.show("Duplicazione terminata con successo!")
end if

destroy luo_duplica;


return 0
end function

on w_duplica_distinta_altro_db.create
int iCurrent
call super::create
this.dw_ricerca_origine=create dw_ricerca_origine
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca_origine
end on

on w_duplica_distinta_altro_db.destroy
call super::destroy
destroy(this.dw_ricerca_origine)
end on

event pc_setwindow;call super::pc_setwindow;

postevent("ue_imposta_ricerca")
end event

type dw_ricerca_origine from u_dw_search within w_duplica_distinta_altro_db
event ue_key pbm_dwnkey
integer x = 32
integer y = 28
integer width = 2949
integer height = 1300
integer taborder = 10
string dataobject = "d_duplica_distinta_sel_altro_db"
boolean border = false
end type

event itemchanged;call super::itemchanged;if isvalid(dwo) then
	if dwo.name = "rs_cod_prodotto" then
		
		if not isnull(data) and len(data) > 0 then
	
			f_PO_LoadDDDW_DW(dw_ricerca_origine,"cod_versione",sqlca,&
						  "distinta"," cod_versione ","'VERSIONE ' "+guo_functions.uof_concat_op()+" cod_versione",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto_padre = '" + data + "' ")

		end if
	end if
end if
end event

event buttonclicked;call super::buttonclicked;string ls_cod_prodotto_partenza, ls_cod_versione_partenza, ls_errore
long ll_ret

if isvalid(dwo) then
	choose case upper(dwo.name)
		case "CB_DUPLICA"
			setpointer(Hourglass!)
			
			ls_cod_prodotto_partenza = dw_ricerca_origine.getitemstring(1,"rs_cod_prodotto")
			ls_cod_versione_partenza = dw_ricerca_origine.getitemstring(1,"cod_versione")
			
			ll_ret = wf_duplica(ls_cod_prodotto_partenza, ls_cod_versione_partenza, ref ls_errore)
			if ll_ret < 0 then
				g_mb.messagebox("APICE", "Errore in duplicazione distinta del PADRE " +ls_cod_prodotto_partenza + " Dettaglio errore: " + ls_errore)
				rollback;
			end if
			
			setpointer(Arrow!)
	
		case "B_RICERCA_PRODOTTO_DA"
				guo_ricerca.uof_ricerca_prodotto(dw_ricerca_origine,"rs_cod_prodotto")
		
		case "CB_DUPLICA_EXCEL"
			setpointer(Hourglass!)

			string ls_valore, ls_tipo
			long ll_errore, ll_count, ll_index, ll_riga
			decimal ld_valore
			
			//inizio
			iole_excel = CREATE OLEObject
			ll_errore = iole_excel.ConnectToNewObject("excel.application")
			
			if ll_errore < 0 then
				messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
				return -1
			end if
			
			iole_excel.Application.Workbooks.Open(getitemstring(1,"path_file"))
			
			iole_excel.application.visible = true
			
			ll_riga = 1
			
			do while true	
				ll_riga ++
				
				// leggo sede origine
				wf_get_value( "DISTINTE", ll_riga, 1, ls_valore, ld_valore, ls_tipo)
				ls_cod_prodotto_partenza = upper(ls_valore)
				
				if isnull(ls_cod_prodotto_partenza) or len(ls_cod_prodotto_partenza) < 1 then exit

	
				wf_get_value( "DISTINTE", ll_riga, 2, ls_valore, ld_valore, ls_tipo)
				ls_cod_versione_partenza = upper(ls_valore)
			
				ll_ret = wf_duplica(ls_cod_prodotto_partenza, ls_cod_versione_partenza, ref ls_errore)
				if ll_ret < 0 then
					g_mb.messagebox("APICE", "Errore in duplicazione distinta del PADRE " +ls_cod_prodotto_partenza + " Dettaglio errore: " + ls_errore)
					exit
				end if
				
			loop
			
			
			setpointer(Arrow!)
	

		case "CB_PATH"
			string		ls_path, docpath, docname[]
			integer	li_count, li_ret
			
			ls_path = s_cs_xx.volume
			li_ret = GetFileOpenName("Seleziona File XLS da importare", docpath, docname[], "DOC", 	+ "Files Excel (*.XLS),*.XLS,", 	ls_path)
			
			if li_ret < 1 then return
			li_count = Upperbound(docname)
			
			if li_count = 1 then
				setitem(1, "path_file", docpath)
			end if

		end choose
end if

end event

