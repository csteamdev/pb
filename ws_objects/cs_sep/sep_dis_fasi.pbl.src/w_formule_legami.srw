﻿$PBExportHeader$w_formule_legami.srw
forward
global type w_formule_legami from w_cs_xx_risposta
end type
type dw_formule_legami_lista from uo_cs_xx_dw within w_formule_legami
end type
type dw_formule_legami_dett from uo_cs_xx_dw within w_formule_legami
end type
end forward

global type w_formule_legami from w_cs_xx_risposta
integer width = 2619
integer height = 1384
string title = "Formule Legami"
dw_formule_legami_lista dw_formule_legami_lista
dw_formule_legami_dett dw_formule_legami_dett
end type
global w_formule_legami w_formule_legami

type variables
string is_cod_prodotto_finito, is_cod_versione, is_cod_prodotto_padre, is_cod_versione_padre
integer ii_modifica_sn = 0
integer ii_manca_figlio = 0
end variables

on w_formule_legami.create
int iCurrent
call super::create
this.dw_formule_legami_lista=create dw_formule_legami_lista
this.dw_formule_legami_dett=create dw_formule_legami_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_formule_legami_lista
this.Control[iCurrent+2]=this.dw_formule_legami_dett
end on

on w_formule_legami.destroy
call super::destroy
destroy(this.dw_formule_legami_lista)
destroy(this.dw_formule_legami_dett)
end on

event open;call super::open;//cb_prodotti_ricerca.enabled = false
end event

event pc_modify;call super::pc_modify;ii_modifica_sn = 1

//cb_prodotti_ricerca.enabled = false


end event

event pc_new;call super::pc_new;//cb_prodotti_ricerca.enabled = true
end event

event pc_save;call super::pc_save;ii_modifica_sn = 0

if ii_manca_figlio = 0 then
//	cb_prodotti_ricerca.enabled = false
end if	
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_formule_legami_dett, &
                 "cod_formula", &
                 sqlca, &
					  "tab_formule_db", &
                 "cod_formula", &
					  "des_formula", &					  
                 "")			
end event

event pc_setwindow;call super::pc_setwindow;dw_formule_legami_lista.set_dw_key("cod_azienda")

set_w_options(c_EnablePopup)

dw_formule_legami_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_formule_legami_dett.set_dw_options(sqlca, &
                                  dw_formule_legami_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
is_cod_prodotto_finito = s_cs_xx.parametri.parametro_s_2
is_cod_versione = s_cs_xx.parametri.parametro_s_3
is_cod_prodotto_padre = s_cs_xx.parametri.parametro_s_4
is_cod_versione_padre = s_cs_xx.parametri.parametro_s_5

end event

event pc_view;call super::pc_view;//cb_prodotti_ricerca.enabled = false
end event

type dw_formule_legami_lista from uo_cs_xx_dw within w_formule_legami
integer x = 23
integer y = 20
integer width = 2528
integer height = 500
integer taborder = 10
string dataobject = "d_formule_legami_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_prodotto_finito, is_cod_versione, is_cod_prodotto_padre, is_cod_versione_padre)
		
if ll_errore < 0 then
	pcca.error = c_fatal
end if

	
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if

	if isnull(this.getitemstring(ll_i, "cod_prodotto_finito")) then
      this.setitem(ll_i, "cod_prodotto_finito", is_cod_prodotto_finito)	
	end if

	if isnull(this.getitemstring(ll_i, "cod_versione")) then
      this.setitem(ll_i, "cod_versione", is_cod_versione)	
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_prodotto_padre")) then
      this.setitem(ll_i, "cod_prodotto_padre", is_cod_prodotto_padre)	
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_versione_padre")) then
      this.setitem(ll_i, "cod_versione_padre", is_cod_versione_padre)	
	end if
next
end event

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then
		string ls_cod_prodotto_figlio, ls_cod_azienda, ls_cod_formula, ls_cod_versione_figlio
	
		ls_cod_prodotto_figlio = getitemstring(getrow(),"cod_prodotto_figlio")
		if isnull(ls_cod_prodotto_figlio) then
			g_mb.messagebox("SEP", "Prodotto figlio obbligatorio")
//			cb_prodotti_ricerca.enabled = true
			ii_manca_figlio = 1
			pcca.error = c_valfailed
			return
		else 
			ii_manca_figlio = 0
		end if	
		
		ls_cod_versione_figlio = getitemstring(getrow(),"cod_versione_figlio")
		if isnull(ls_cod_versione_figlio) then
			g_mb.messagebox("SEP", "Versione Figlio obbligatorio")
//			cb_prodotti_ricerca.enabled = true
			ii_manca_figlio = 1
			pcca.error = c_valfailed
			return
		else 
			ii_manca_figlio = 0
		end if		
		
		
		ls_cod_formula = getitemstring(getrow(),"cod_formula")
		if isnull(ls_cod_formula) then
			g_mb.messagebox("SEP", "L'indicazione della formula è obbligatoria")
//			cb_prodotti_ricerca.enabled = true
			ii_manca_figlio = 1
			pcca.error = c_valfailed
			return
		else 
			ii_manca_figlio = 0			
		end if		
	
	end if
end event

event itemchanged;call super::itemchanged;string ls_cod_prodotto_figlio,ls_cod_versione_figlio,ls_cod_azienda

choose case i_colname
	case "cod_prodotto_figlio"
		
		if isnull(i_coltext) or len(i_coltext) < 1 then return 0
		
		ls_cod_versione_figlio = getitemstring(getrow(), "cod_versione_figlio")
	
		select cod_azienda 
		  into :ls_cod_azienda
		  from tab_formule_legami
		 where  cod_azienda = : s_cs_xx.cod_azienda and
				  cod_prodotto_finito = :is_cod_prodotto_finito and
				  cod_versione = :is_cod_versione and
				  cod_prodotto_padre = :is_cod_prodotto_padre and
				  cod_versione_padre = :is_cod_versione_padre and
				  cod_prodotto_figlio = :i_coltext and
				  cod_versione_figlio = :ls_cod_versione_figlio;
			
		if sqlca.sqlcode = 0 then
			return 1
		end if

		
		
	case "cod_versione_figlio"
		
		if isnull(i_coltext) or len(i_coltext) < 1 then return 0

		ls_cod_prodotto_figlio = getitemstring(getrow(), "cod_prodotto_figlio")

		select cod_azienda 
		  into :ls_cod_azienda
		  from tab_formule_legami
		 where  cod_azienda = : s_cs_xx.cod_azienda and
				  cod_prodotto_finito = :is_cod_prodotto_finito and
				  cod_versione = :is_cod_versione and
				  cod_prodotto_padre = :is_cod_prodotto_padre and
				  cod_versione_padre = :is_cod_versione_padre and
				  cod_prodotto_figlio = :ls_cod_prodotto_figlio and
				  cod_versione_figlio = :i_coltext;
			
		if sqlca.sqlcode = 0 then
			return 1
		end if

end choose	
end event

type dw_formule_legami_dett from uo_cs_xx_dw within w_formule_legami
integer y = 520
integer width = 2555
integer height = 732
integer taborder = 20
string dataobject = "d_formule_legami_dett"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_formule_legami_dett,"cod_prodotto_figlio")
end choose
end event

