﻿$PBExportHeader$w_grafico_media_fasi.srw
$PBExportComments$Window grafico media delle fasi di lavoro
forward
global type w_grafico_media_fasi from w_cs_xx_risposta
end type
type ddlb_grafico from dropdownlistbox within w_grafico_media_fasi
end type
type st_3 from statictext within w_grafico_media_fasi
end type
type rb_attrezzaggio_com from radiobutton within w_grafico_media_fasi
end type
type rb_attrezzaggio from radiobutton within w_grafico_media_fasi
end type
type rb_lavorazione from radiobutton within w_grafico_media_fasi
end type
type st_teorico from statictext within w_grafico_media_fasi
end type
type st_2 from statictext within w_grafico_media_fasi
end type
type em_moltiplicatore from editmask within w_grafico_media_fasi
end type
type st_1 from statictext within w_grafico_media_fasi
end type
type st_9 from statictext within w_grafico_media_fasi
end type
type em_step from editmask within w_grafico_media_fasi
end type
type st_titolo from statictext within w_grafico_media_fasi
end type
type cbx_tutti from checkbox within w_grafico_media_fasi
end type
type ddlb_operai from dropdownlistbox within w_grafico_media_fasi
end type
type st_7 from statictext within w_grafico_media_fasi
end type
type em_a_data from editmask within w_grafico_media_fasi
end type
type st_6 from statictext within w_grafico_media_fasi
end type
type em_da_data from editmask within w_grafico_media_fasi
end type
type st_5 from statictext within w_grafico_media_fasi
end type
type cb_esegui from commandbutton within w_grafico_media_fasi
end type
type dw_grafico from datawindow within w_grafico_media_fasi
end type
type gb_1 from groupbox within w_grafico_media_fasi
end type
end forward

global type w_grafico_media_fasi from w_cs_xx_risposta
integer width = 4466
integer height = 1948
string title = "Grafico media tempi di fase per periodo"
ddlb_grafico ddlb_grafico
st_3 st_3
rb_attrezzaggio_com rb_attrezzaggio_com
rb_attrezzaggio rb_attrezzaggio
rb_lavorazione rb_lavorazione
st_teorico st_teorico
st_2 st_2
em_moltiplicatore em_moltiplicatore
st_1 st_1
st_9 st_9
em_step em_step
st_titolo st_titolo
cbx_tutti cbx_tutti
ddlb_operai ddlb_operai
st_7 st_7
em_a_data em_a_data
st_6 st_6
em_da_data em_da_data
st_5 st_5
cb_esegui cb_esegui
dw_grafico dw_grafico
gb_1 gb_1
end type
global w_grafico_media_fasi w_grafico_media_fasi

on w_grafico_media_fasi.create
int iCurrent
call super::create
this.ddlb_grafico=create ddlb_grafico
this.st_3=create st_3
this.rb_attrezzaggio_com=create rb_attrezzaggio_com
this.rb_attrezzaggio=create rb_attrezzaggio
this.rb_lavorazione=create rb_lavorazione
this.st_teorico=create st_teorico
this.st_2=create st_2
this.em_moltiplicatore=create em_moltiplicatore
this.st_1=create st_1
this.st_9=create st_9
this.em_step=create em_step
this.st_titolo=create st_titolo
this.cbx_tutti=create cbx_tutti
this.ddlb_operai=create ddlb_operai
this.st_7=create st_7
this.em_a_data=create em_a_data
this.st_6=create st_6
this.em_da_data=create em_da_data
this.st_5=create st_5
this.cb_esegui=create cb_esegui
this.dw_grafico=create dw_grafico
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ddlb_grafico
this.Control[iCurrent+2]=this.st_3
this.Control[iCurrent+3]=this.rb_attrezzaggio_com
this.Control[iCurrent+4]=this.rb_attrezzaggio
this.Control[iCurrent+5]=this.rb_lavorazione
this.Control[iCurrent+6]=this.st_teorico
this.Control[iCurrent+7]=this.st_2
this.Control[iCurrent+8]=this.em_moltiplicatore
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.st_9
this.Control[iCurrent+11]=this.em_step
this.Control[iCurrent+12]=this.st_titolo
this.Control[iCurrent+13]=this.cbx_tutti
this.Control[iCurrent+14]=this.ddlb_operai
this.Control[iCurrent+15]=this.st_7
this.Control[iCurrent+16]=this.em_a_data
this.Control[iCurrent+17]=this.st_6
this.Control[iCurrent+18]=this.em_da_data
this.Control[iCurrent+19]=this.st_5
this.Control[iCurrent+20]=this.cb_esegui
this.Control[iCurrent+21]=this.dw_grafico
this.Control[iCurrent+22]=this.gb_1
end on

on w_grafico_media_fasi.destroy
call super::destroy
destroy(this.ddlb_grafico)
destroy(this.st_3)
destroy(this.rb_attrezzaggio_com)
destroy(this.rb_attrezzaggio)
destroy(this.rb_lavorazione)
destroy(this.st_teorico)
destroy(this.st_2)
destroy(this.em_moltiplicatore)
destroy(this.st_1)
destroy(this.st_9)
destroy(this.em_step)
destroy(this.st_titolo)
destroy(this.cbx_tutti)
destroy(this.ddlb_operai)
destroy(this.st_7)
destroy(this.em_a_data)
destroy(this.st_6)
destroy(this.em_da_data)
destroy(this.st_5)
destroy(this.cb_esegui)
destroy(this.dw_grafico)
destroy(this.gb_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_7
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_6
ls_cod_reparto = s_cs_xx.parametri.parametro_s_5

st_titolo.text = "Fase di lavoro del prodotto: " + ls_cod_prodotto +"  Lavorazione: " + ls_cod_lavorazione + "  Reparto: "+ls_cod_reparto

st_teorico.text = string(round(s_cs_xx.parametri.parametro_d_1,4))
em_a_data.text = string(date(s_cs_xx.parametri.parametro_data_2))
em_da_data.text = string(date(s_cs_xx.parametri.parametro_data_1))

ddlb_grafico.additem("Area")
ddlb_grafico.additem("Barre 2D")
ddlb_grafico.additem("Barre 3D")
ddlb_grafico.additem("Linee")
ddlb_grafico.additem("Area 3D")
ddlb_grafico.additem("Linee 3D")
ddlb_grafico.additem("Istogramma 3D")

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDLB(ddlb_operai, sqlca, "anag_operai", &	
				  "cod_operaio", "cognome", &
				  "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
	
end event

type ddlb_grafico from dropdownlistbox within w_grafico_media_fasi
integer x = 891
integer y = 1740
integer width = 663
integer height = 860
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean sorted = false
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;choose case index
	case 1
		dw_grafico.Object.gr_1.graphtype = 1
	case 2
		dw_grafico.Object.gr_1.graphtype = 7
	case 3
		dw_grafico.Object.gr_1.graphtype = 8
	case 4
		dw_grafico.Object.gr_1.graphtype = 12
	case 5
		dw_grafico.Object.gr_1.graphtype = 15
	case 6
		dw_grafico.Object.gr_1.graphtype = 16

	case 7
		dw_grafico.Object.gr_1.graphtype = 10

end choose
//
//1  Area
//2  Bar
//3  Bar3D
//4  Bar3DObj
//5  BarStacked
//6  BarStacked3DObj
//7  Col
//8  Col3D
//9  Col3DObj
//10  ColStacked
//11  ColStacked3DObj
//12  Line
//13  Pie
//14  Scatter
//15  Area3D
//16  Line3D
//17  Pie3D
end event

type st_3 from statictext within w_grafico_media_fasi
integer x = 526
integer y = 1760
integer width = 343
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tipo Grafico"
boolean focusrectangle = false
end type

type rb_attrezzaggio_com from radiobutton within w_grafico_media_fasi
integer x = 3314
integer y = 1740
integer width = 549
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Attrezzaggio com."
borderstyle borderstyle = stylelowered!
end type

event clicked;dw_grafico.reset()
st_teorico.text = string(round(s_cs_xx.parametri.parametro_d_3,4))
end event

type rb_attrezzaggio from radiobutton within w_grafico_media_fasi
integer x = 3863
integer y = 1680
integer width = 407
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Attrezzaggio"
borderstyle borderstyle = stylelowered!
end type

event clicked;dw_grafico.reset()

st_teorico.text = string(round( s_cs_xx.parametri.parametro_d_2,4))
end event

type rb_lavorazione from radiobutton within w_grafico_media_fasi
integer x = 3314
integer y = 1680
integer width = 402
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Lavorazione"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

event clicked;dw_grafico.reset()

st_teorico.text = string(round(s_cs_xx.parametri.parametro_d_1,4))

end event

type st_teorico from statictext within w_grafico_media_fasi
integer x = 2880
integer y = 1760
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_2 from statictext within w_grafico_media_fasi
integer x = 2560
integer y = 1760
integer width = 343
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Val.Teorico="
boolean focusrectangle = false
end type

type em_moltiplicatore from editmask within w_grafico_media_fasi
integer x = 2263
integer y = 1740
integer width = 274
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
end type

type st_1 from statictext within w_grafico_media_fasi
integer x = 1691
integer y = 1760
integer width = 571
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Val.Max=val.Teorico x"
boolean focusrectangle = false
end type

type st_9 from statictext within w_grafico_media_fasi
integer x = 2834
integer y = 1660
integer width = 224
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "gg. step"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_step from editmask within w_grafico_media_fasi
integer x = 3063
integer y = 1640
integer width = 206
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "10"
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
end type

type st_titolo from statictext within w_grafico_media_fasi
integer x = 23
integer y = 20
integer width = 3497
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cbx_tutti from checkbox within w_grafico_media_fasi
integer x = 2560
integer y = 1660
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tutti"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

type ddlb_operai from dropdownlistbox within w_grafico_media_fasi
integer x = 1623
integer y = 1640
integer width = 937
integer height = 1480
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type st_7 from statictext within w_grafico_media_fasi
integer x = 1303
integer y = 1660
integer width = 306
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Dipendente"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_a_data from editmask within w_grafico_media_fasi
integer x = 869
integer y = 1640
integer width = 411
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_6 from statictext within w_grafico_media_fasi
integer x = 686
integer y = 1660
integer width = 183
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "A data"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_da_data from editmask within w_grafico_media_fasi
integer x = 251
integer y = 1640
integer width = 411
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_5 from statictext within w_grafico_media_fasi
integer x = 23
integer y = 1660
integer width = 229
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Da data"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_esegui from commandbutton within w_grafico_media_fasi
integer x = 23
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;long     ll_i,ll_riga,ll_num_cicli,ll_step,ll_giorni_aggiunta
datetime ldt_giorno
decimal  ld_media
string   ls_cod_operaio,ls_giorno,ls_giorno_it,ls_cod_prodotto,ls_problemi_produzione, & 
			ls_cognome,ls_errore,ls_cod_prodotto_test,ls_cod_lavorazione,ls_cod_reparto
datetime ldt_da_data,ldt_a_data,ldt_data_corrente,lt_orario_attrezzaggio,lt_orario_attrezzaggio_commessa, ldt_data_corrente_generale, &
			lt_orario_lavorazione,lt_orario_risorsa_umana,lt_orario_attrezzaggio_fine,lt_orario_attrezzaggio_commessa_fine, &
			lt_orario_lavorazione_fine,lt_orario_risorsa_umana_fine,ldt_data_inizio_generale,ldt_data_fine_generale
decimal  ld_secondi,ld_minuti_risorsa_umana,ld_ore,ld_quan_prodotta,ld_minuti_lavorazione,ld_minuti_attrezzaggio, & 
			ld_minuti_attrezzaggio_commessa,ld_ore_lavorazione,ld_ore_attrezzaggio,ld_ore_attrezzaggio_commessa, & 
			ld_somma_tempo_attrezzaggio,ld_somma_tempo_attrezzaggio_commessa,ld_somma_tempo_lavorazione,ld_tempo_lavorazione_t,&
			ld_tempo_attrezzaggio_t,ld_tempo_attrezzaggio_com_t
integer  li_risposta
long     ll_num_anno_commessa,ll_num_num_commessa,ll_num_commesse

dw_grafico.reset()
setpointer (hourglass!)

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_7
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_6
ls_cod_reparto = s_cs_xx.parametri.parametro_s_5

ld_tempo_lavorazione_t = s_cs_xx.parametri.parametro_d_1
ld_tempo_attrezzaggio_t = s_cs_xx.parametri.parametro_d_2 
ld_tempo_attrezzaggio_com_t = s_cs_xx.parametri.parametro_d_3

if rb_lavorazione.checked = true then dw_grafico.Object.gr_1.values.maximumvalue = string(round(ld_tempo_lavorazione_t*integer(em_moltiplicatore.text),2))
if rb_attrezzaggio.checked = true then dw_grafico.Object.gr_1.values.maximumvalue = string(round(ld_tempo_attrezzaggio_t*integer(em_moltiplicatore.text),2))
if rb_attrezzaggio_com.checked = true then dw_grafico.Object.gr_1.values.maximumvalue = string(round(ld_tempo_attrezzaggio_com_t*integer(em_moltiplicatore.text),2))

ls_cod_operaio = f_po_selectddlb(ddlb_operai)
ll_step = long(em_step.text)

if cbx_tutti.checked = true then ls_cod_operaio = "%" 

if isnull(ls_cod_operaio) or ls_cod_operaio="" or ls_cod_operaio ="none" then
	g_mb.messagebox("Sep","Attenzione! Selezionare almeno un operaio oppure l'opzione tutti.",stopsign!)
	return
end if

if ll_step <=0 then
	g_mb.messagebox("Sep","Attenzione! Impostare correttamente lo step.",stopsign!)
	return
end if

ldt_data_inizio_generale = datetime(date(em_da_data.text),00:00:00)
ldt_data_fine_generale = datetime(date(em_a_data.text),00:00:00)

ldt_data_corrente_generale = ldt_data_inizio_generale
	
do while ldt_data_corrente_generale <= ldt_data_fine_generale

	ldt_da_data = datetime(relativedate(date(ldt_data_inizio_generale),ll_num_cicli * ll_step))
	ldt_data_corrente = ldt_da_data
	ldt_data_corrente_generale = ldt_da_data
	ldt_a_data = datetime(relativedate(date(ldt_data_inizio_generale),(ll_num_cicli+1) * ll_step))
	
	do while ldt_data_corrente <= ldt_a_data
		
		declare r_det_orari cursor for 
		select orario_attrezzaggio,
				 orario_attrezzaggio_commessa,
				 orario_lavorazione,
				 orario_risorsa_umana,
				 cod_operaio
		from   det_orari_produzione
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    data_giorno = :ldt_data_corrente
		and    flag_inizio = 'S'
		and    cod_reparto = :ls_cod_reparto
		and    cod_lavorazione=:ls_cod_lavorazione
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_operaio like :ls_cod_operaio
		order by prog_orari;
		
		open r_det_orari;
		
		do while 1 = 1 
		
			fetch r_det_orari
			into  :lt_orario_attrezzaggio,
					:lt_orario_attrezzaggio_commessa,
					:lt_orario_lavorazione,
					:lt_orario_risorsa_umana,
					:ls_cod_operaio;
	
			
			if sqlca.sqlcode = 100 then exit
	
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_det_orari;
				return
			end if
		
			if time(lt_orario_attrezzaggio) <> 00:00:00 then
	
				declare r_d1 cursor for 
				select orario_attrezzaggio
				from   det_orari_produzione
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    data_giorno = :ldt_data_corrente
				and    flag_inizio = 'N'
				and    cod_operaio like :ls_cod_operaio
				and    orario_attrezzaggio is not null
				and    orario_attrezzaggio > :lt_orario_attrezzaggio
				and    cod_prodotto=:ls_cod_prodotto
				and    cod_lavorazione=:ls_cod_lavorazione
				and    cod_reparto=:ls_cod_reparto
				order by orario_attrezzaggio;
				
				open r_d1;
				
				fetch r_d1
				into  :lt_orario_attrezzaggio_fine;
				
				if sqlca.sqlcode < 0 then 
					g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					close r_d1;
					close r_det_orari;
					return
				end if
				
				close r_d1;
				ld_secondi = SecondsAfter ( time(lt_orario_attrezzaggio),time(lt_orario_attrezzaggio_fine))
				ld_minuti_attrezzaggio = ld_minuti_attrezzaggio + ld_secondi/60
				ld_secondi=0
			end if	
	
			if time(lt_orario_attrezzaggio_commessa) <> 00:00:00 then
				declare r_d2 cursor for 
				select orario_attrezzaggio_commessa
				from   det_orari_produzione
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    data_giorno = :ldt_data_corrente
				and    flag_inizio = 'N'
				and    cod_operaio like :ls_cod_operaio
				and    orario_attrezzaggio_commessa is not null
				and    orario_attrezzaggio_commessa > :lt_orario_attrezzaggio_commessa
				and    cod_prodotto=:ls_cod_prodotto
				and    cod_lavorazione=:ls_cod_lavorazione
				and    cod_reparto=:ls_cod_reparto
	
				order by orario_attrezzaggio_commessa;
				
				open r_d2;
				
				fetch r_d2
				into  :lt_orario_attrezzaggio_commessa_fine;
				
				if sqlca.sqlcode < 0 then 
					g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					close r_d2;
					close r_det_orari;
					return
				end if
				
				close r_d2;
				ld_secondi = SecondsAfter ( time(lt_orario_attrezzaggio_commessa),time(lt_orario_attrezzaggio_commessa_fine))
				ld_minuti_attrezzaggio_commessa = ld_minuti_attrezzaggio_commessa + ld_secondi/60
				ld_secondi = 0
			end if	
	
			if time(lt_orario_lavorazione) <> 00:00:00  then
				declare r_d3 cursor for 
				select orario_lavorazione,
						 problemi_produzione
				from   det_orari_produzione
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    data_giorno = :ldt_data_corrente
				and    flag_inizio = 'N'
				and    cod_operaio like :ls_cod_operaio
				and    orario_lavorazione is not null
				and    orario_lavorazione > :lt_orario_lavorazione
				and    cod_prodotto=:ls_cod_prodotto
				and    cod_lavorazione=:ls_cod_lavorazione
				and    cod_reparto=:ls_cod_reparto
	
				order by orario_lavorazione;
				
				open r_d3;
				
				fetch r_d3
				into  :lt_orario_lavorazione_fine,
						:ls_problemi_produzione;
				
				if sqlca.sqlcode < 0 then 
					g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					close r_d3;
					close r_det_orari;
					return
				end if
				
				close r_d3;
				if ls_problemi_produzione ='N' or isnull(ls_problemi_produzione) then
					ld_secondi = SecondsAfter ( time(lt_orario_lavorazione),time(lt_orario_lavorazione_fine))
					ld_minuti_lavorazione = ld_minuti_lavorazione + ld_secondi/60			
				end if
			end if	
	
			if time(lt_orario_risorsa_umana) <> 00:00:00 then
				declare r_d4 cursor for 
				select orario_risorsa_umana
				from   det_orari_produzione
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    data_giorno = :ldt_data_corrente
				and    flag_inizio = 'N'
				and    cod_operaio = :ls_cod_operaio
				and    orario_risorsa_umana is not null
				and    orario_risorsa_umana >= :lt_orario_risorsa_umana
				order by orario_risorsa_umana;
				
				open r_d4;
				
				fetch r_d4
				into  :lt_orario_risorsa_umana_fine;
				
				if sqlca.sqlcode < 0 then 
					g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					close r_d4;
					close r_det_orari;
					return
				end if
				
				close r_d4;
				ld_secondi = SecondsAfter ( time(lt_orario_risorsa_umana),time(lt_orario_risorsa_umana_fine))
				ld_minuti_risorsa_umana = ld_minuti_risorsa_umana + ld_secondi/60			
			
			end if	
	
		loop
		
		
		close r_det_orari;
	
	
		ldt_data_corrente = datetime(RelativeDate ( date(ldt_data_corrente) , 1 ),00:00:00)
			
	loop
	
	select sum(quan_prodotta)
	into   :ld_quan_prodotta
	from   det_orari_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    data_giorno between :ldt_da_data and :ldt_a_data
	and    cod_reparto = :ls_cod_reparto
	and    cod_lavorazione=:ls_cod_lavorazione
	and    cod_prodotto=:ls_cod_prodotto
	and    cod_operaio like :ls_cod_operaio;
	
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	select count(distinct anno_commessa)
	into   :ll_num_anno_commessa
	from   det_orari_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    data_giorno between :ldt_da_data and :ldt_a_data
	and    cod_reparto = :ls_cod_reparto
	and    cod_lavorazione=:ls_cod_lavorazione
	and    cod_prodotto=:ls_cod_prodotto
	and    cod_operaio like :ls_cod_operaio;
	
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	select count(distinct num_commessa)
	into   :ll_num_num_commessa
	from   det_orari_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    data_giorno between :ldt_da_data and :ldt_a_data
	and    cod_reparto = :ls_cod_reparto
	and    cod_lavorazione=:ls_cod_lavorazione
	and    cod_prodotto=:ls_cod_prodotto
	and    cod_operaio like :ls_cod_operaio;
	
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	if ll_num_num_commessa > ll_num_anno_commessa then
		ll_num_commesse = ll_num_num_commessa
	else
		ll_num_commesse = ll_num_anno_commessa
	end if
	
	if ll_num_commesse>0 then
		ld_minuti_attrezzaggio = round(ld_minuti_attrezzaggio/ll_num_commesse,4)
		ld_minuti_attrezzaggio_commessa = round(ld_minuti_attrezzaggio_commessa/ll_num_commesse,4)
	end if
	
	if ld_quan_prodotta > 0 then
		ld_minuti_lavorazione = round(ld_minuti_lavorazione/ld_quan_prodotta,4)
		ld_minuti_risorsa_umana = round(ld_minuti_risorsa_umana/ld_quan_prodotta,4)
	end if

	ll_riga = dw_grafico.insertrow(0)
	dw_grafico.setitem(ll_riga,"giorno",ldt_data_corrente_generale)
	
	if rb_lavorazione.checked = true then dw_grafico.setitem(ll_riga,"media",ld_minuti_lavorazione)
	if rb_attrezzaggio.checked = true then dw_grafico.setitem(ll_riga,"media",ld_minuti_attrezzaggio)
	if rb_attrezzaggio_com.checked = true then dw_grafico.setitem(ll_riga,"media",ld_minuti_attrezzaggio_commessa)
	
//	dw_grafico.setitem(ll_riga,"descrizione","Media Tempo Lavorazione")
//	ll_riga = dw_grafico.insertrow(0)
//	dw_grafico.setitem(ll_riga,"giorno",ldt_data_corrente_generale)
//	dw_grafico.setitem(ll_riga,"media",ld_tempo_lavorazione_t)
//	dw_grafico.setitem(ll_riga,"descrizione","Tempo lavorazione Teorico")
	
	ll_num_cicli++
	ld_minuti_attrezzaggio = 0
	ld_minuti_attrezzaggio_commessa = 0
	ld_minuti_lavorazione = 0
	ld_minuti_risorsa_umana = 0 
	
loop

setpointer (arrow!)
end event

type dw_grafico from datawindow within w_grafico_media_fasi
integer x = 23
integer y = 120
integer width = 4389
integer height = 1500
integer taborder = 10
string title = "none"
string dataobject = "d_grafico_media_fasi"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type gb_1 from groupbox within w_grafico_media_fasi
integer x = 3291
integer y = 1620
integer width = 1006
integer height = 200
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tipo tempo fase"
borderstyle borderstyle = stylelowered!
end type

