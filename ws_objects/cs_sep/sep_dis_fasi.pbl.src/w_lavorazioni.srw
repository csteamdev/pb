﻿$PBExportHeader$w_lavorazioni.srw
$PBExportComments$Window Lavorazioni
forward
global type w_lavorazioni from w_cs_xx_principale
end type
type dw_lavorazioni from uo_cs_xx_dw within w_lavorazioni
end type
end forward

global type w_lavorazioni from w_cs_xx_principale
int Width=2090
int Height=1521
boolean TitleBar=true
string Title="Lavorazioni"
dw_lavorazioni dw_lavorazioni
end type
global w_lavorazioni w_lavorazioni

on w_lavorazioni.create
int iCurrent
call w_cs_xx_principale::create
this.dw_lavorazioni=create dw_lavorazioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lavorazioni
end on

on w_lavorazioni.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_lavorazioni)
end on

event pc_setwindow;call super::pc_setwindow;dw_lavorazioni.set_dw_key("cod_azienda")
dw_lavorazioni.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main =dw_lavorazioni
end event

type dw_lavorazioni from uo_cs_xx_dw within w_lavorazioni
int X=23
int Y=21
int Width=2012
int Height=1381
string DataObject="d_lavorazioni"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

