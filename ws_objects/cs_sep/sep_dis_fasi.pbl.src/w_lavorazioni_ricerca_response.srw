﻿$PBExportHeader$w_lavorazioni_ricerca_response.srw
$PBExportComments$Window ricerca lavorazioni
forward
global type w_lavorazioni_ricerca_response from w_cs_xx_risposta
end type
type cb_ok from commandbutton within w_lavorazioni_ricerca_response
end type
type dw_find from u_dw_find within w_lavorazioni_ricerca_response
end type
type cb_codice from uo_cb_ok within w_lavorazioni_ricerca_response
end type
type dw_lavorazioni from uo_cs_xx_dw within w_lavorazioni_ricerca_response
end type
type cb_des from uo_cb_ok within w_lavorazioni_ricerca_response
end type
type st_1 from statictext within w_lavorazioni_ricerca_response
end type
end forward

global type w_lavorazioni_ricerca_response from w_cs_xx_risposta
int Width=1953
int Height=1465
boolean TitleBar=true
string Title="Rierca Lavorazioni"
cb_ok cb_ok
dw_find dw_find
cb_codice cb_codice
dw_lavorazioni dw_lavorazioni
cb_des cb_des
st_1 st_1
end type
global w_lavorazioni_ricerca_response w_lavorazioni_ricerca_response

on w_lavorazioni_ricerca_response.create
int iCurrent
call w_cs_xx_risposta::create
this.cb_ok=create cb_ok
this.dw_find=create dw_find
this.cb_codice=create cb_codice
this.dw_lavorazioni=create dw_lavorazioni
this.cb_des=create cb_des
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_ok
this.Control[iCurrent+2]=dw_find
this.Control[iCurrent+3]=cb_codice
this.Control[iCurrent+4]=dw_lavorazioni
this.Control[iCurrent+5]=cb_des
this.Control[iCurrent+6]=st_1
end on

on w_lavorazioni_ricerca_response.destroy
call w_cs_xx_risposta::destroy
destroy(this.cb_ok)
destroy(this.dw_find)
destroy(this.cb_codice)
destroy(this.dw_lavorazioni)
destroy(this.cb_des)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_lavorazioni.set_dw_key("cod_azienda")
dw_lavorazioni.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_retrieveasneeded + c_selectonrowfocuschange, &
                                 c_default)

dw_lavorazioni.setsort("cod_lavorazione A")
dw_find.fu_wiredw(dw_lavorazioni, "des_lavorazione")

dw_lavorazioni.setfocus()
cb_des.postevent("clicked")
end event

type cb_ok from commandbutton within w_lavorazioni_ricerca_response
event clicked pbm_bnclicked
int X=1532
int Y=1261
int Width=366
int Height=81
int TabOrder=40
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_lavorazioni.getitemstring(dw_lavorazioni.getrow(),"cod_lavorazione"))
	s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
	s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
else
	s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_search.setitem(s_cs_xx.parametri.parametro_uo_dw_search.getrow(), s_cs_xx.parametri.parametro_s_1, dw_lavorazioni.getitemstring(dw_lavorazioni.getrow(),"cod_lavorazione"))
	s_cs_xx.parametri.parametro_uo_dw_search.triggerevent(itemchanged!)
end if
close(parent)
end event

type dw_find from u_dw_find within w_lavorazioni_ricerca_response
int X=732
int Y=21
int Width=1166
int Height=81
int TabOrder=50
end type

type cb_codice from uo_cb_ok within w_lavorazioni_ricerca_response
event clicked pbm_bnclicked
int X=46
int Y=221
int Width=458
int Height=81
int TabOrder=30
string Text="Codice"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;dw_find.fu_unwiredw()

dw_lavorazioni.setsort("cod_lavorazione A")
dw_lavorazioni.sort()

dw_find.fu_wiredw(dw_lavorazioni, "cod_lavorazione")

st_1.text = "Ricerca per Codice:"
dw_find.setfocus()

end event

type dw_lavorazioni from uo_cs_xx_dw within w_lavorazioni_ricerca_response
event doubleclicked pbm_dwnlbuttondblclk
event pcd_retrieve pbm_custom60
int X=46
int Y=301
int Width=1852
int Height=941
int TabOrder=20
string DataObject="d_lavorazioni"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on doubleclicked;call uo_cs_xx_dw::doubleclicked;if i_extendmode then
   cb_ok.postevent(clicked!)
end if
end on

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type cb_des from uo_cb_ok within w_lavorazioni_ricerca_response
event clicked pbm_bnclicked
int X=503
int Y=221
int Width=1395
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="Descrizione"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;dw_find.fu_unwiredw()

dw_lavorazioni.setsort("des_lavorazione A")
dw_lavorazioni.sort()

dw_find.fu_wiredw(dw_lavorazioni, "des_lavorazione")

st_1.text = "Descrizione:"
dw_find.setfocus()
end event

type st_1 from statictext within w_lavorazioni_ricerca_response
int X=23
int Y=21
int Width=686
int Height=61
boolean Enabled=false
string Text="Descrizione:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

