﻿$PBExportHeader$w_note_trasf_componenti.srw
forward
global type w_note_trasf_componenti from window
end type
type dw_sospensioni from u_dw_search within w_note_trasf_componenti
end type
end forward

global type w_note_trasf_componenti from window
integer width = 2208
integer height = 2232
boolean titlebar = true
string title = "Sospensione"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
dw_sospensioni dw_sospensioni
end type
global w_note_trasf_componenti w_note_trasf_componenti

type variables
string is_azione = ""
end variables

on w_note_trasf_componenti.create
this.dw_sospensioni=create dw_sospensioni
this.Control[]={this.dw_sospensioni}
end on

on w_note_trasf_componenti.destroy
destroy(this.dw_sospensioni)
end on

event open;s_cs_xx_parametri lstr_parametri
string ls_cod_operaio

lstr_parametri = message.powerobjectparm

is_azione = lstr_parametri.parametro_s_1_a[1]
ls_cod_operaio = lstr_parametri.parametro_s_1_a[2]


if is_azione = "SOSP" then
	dw_sospensioni.object.cod_tipo_causa_sospensione_t.visible = true
	dw_sospensioni.object.cf_cod_tipo_causa_sospensione.visible = true
	dw_sospensioni.object.cf_sigla_causa.visible = true
	dw_sospensioni.object.cod_tipo_causa_sospensione.visible = true
else
	dw_sospensioni.object.cod_tipo_causa_sospensione_t.visible = false
	dw_sospensioni.object.cf_cod_tipo_causa_sospensione.visible = false
	dw_sospensioni.object.cf_sigla_causa.visible = false
	dw_sospensioni.object.cod_tipo_causa_sospensione.visible = false
end if

if is_azione="TRASF" then
	this.title = "Operaio Richiesta Trasferimento"
end if


dw_sospensioni.reset()
dw_sospensioni.insertrow(0)

if ls_cod_operaio<>"" and not isnull(ls_cod_operaio) then
	dw_sospensioni.setitem(dw_sospensioni.getrow(), "cod_operaio", ls_cod_operaio)
end if

f_po_loaddddw_dw(	dw_sospensioni, &
							"cod_tipo_causa_sospensione", &
							sqlca, &
							"tab_tipi_cause_sospensione", &
							"cod_tipo_causa_sospensione", &
							"des_tipo_causa_sospensione", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

dw_sospensioni.setfocus()
end event

type dw_sospensioni from u_dw_search within w_note_trasf_componenti
integer x = 37
integer y = 28
integer width = 2117
integer height = 1704
integer taborder = 10
string dataobject = "d_operatore_trasf_comp"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;datawindow				ldw_data
any						la_any[]
string						ls_cod_operaio, ls_cod_tipo_causa_sospensione
s_cs_xx_parametri	lstr_parametri


if row>0 then
else
	return
end if

choose case dwo.name
	case "b_operaio"
		setnull(ldw_data)
		guo_ricerca.uof_set_response()
		guo_ricerca.uof_ricerca_operaio(dw_sospensioni,"cod_operaio")
		
		guo_ricerca.uof_get_results( la_any)
		if(upperbound(la_any)>0) then
			ls_cod_operaio=la_any[1]
			dw_sospensioni.setitem(row, "cod_operaio", ls_cod_operaio)
		else 
			return
		end if
		
	case "b_conferma"
		
		dw_sospensioni.accepttext()
		
		if is_azione= "SOSP" then
			ls_cod_tipo_causa_sospensione = dw_sospensioni.getitemstring(row, "cod_tipo_causa_sospensione")
			if ls_cod_tipo_causa_sospensione="" or isnull(ls_cod_tipo_causa_sospensione) then
				g_mb.warning("Indicare la Causa della sopsensione!")
				return
			end if
		else
			ls_cod_tipo_causa_sospensione = ""
		end if
		
		
		ls_cod_operaio = dw_sospensioni.getitemstring(row, "cod_operaio")
		
		if ls_cod_operaio="" or isnull(ls_cod_operaio) then
				g_mb.warning("Indicare l'operatore!")
				return
			end if
		
		
		if g_mb.confirm("Confermi l'operazione?", 1) then
			lstr_parametri.parametro_b_1 = true
			lstr_parametri.parametro_s_1_a[1] = ls_cod_operaio
			lstr_parametri.parametro_s_1_a[2] = ls_cod_tipo_causa_sospensione
			closewithreturn(parent, lstr_parametri)
		end if
	
	
	case "b_annulla"
		if g_mb.confirm("Annullare l'operazione?", 1) then
			lstr_parametri.parametro_b_1 = false
			closewithreturn(parent, lstr_parametri)
		end if
	
end choose
end event

