﻿$PBExportHeader$w_recupera_distinta.srw
$PBExportComments$Window per recupero dati vecchia distinta
forward
global type w_recupera_distinta from w_cs_xx_principale
end type
type cb_recupera from commandbutton within w_recupera_distinta
end type
type cb_crea_distinta_padri from commandbutton within w_recupera_distinta
end type
type sle_cod_utente from singlelineedit within w_recupera_distinta
end type
type st_1 from statictext within w_recupera_distinta
end type
type cb_lavorazioni from commandbutton within w_recupera_distinta
end type
type cb_allinea_commesse from commandbutton within w_recupera_distinta
end type
type lb_1 from listbox within w_recupera_distinta
end type
type st_2 from statictext within w_recupera_distinta
end type
end forward

global type w_recupera_distinta from w_cs_xx_principale
integer width = 1513
integer height = 1436
string title = "Recupero Dati vecchia distinta"
cb_recupera cb_recupera
cb_crea_distinta_padri cb_crea_distinta_padri
sle_cod_utente sle_cod_utente
st_1 st_1
cb_lavorazioni cb_lavorazioni
cb_allinea_commesse cb_allinea_commesse
lb_1 lb_1
st_2 st_2
end type
global w_recupera_distinta w_recupera_distinta

on w_recupera_distinta.create
int iCurrent
call super::create
this.cb_recupera=create cb_recupera
this.cb_crea_distinta_padri=create cb_crea_distinta_padri
this.sle_cod_utente=create sle_cod_utente
this.st_1=create st_1
this.cb_lavorazioni=create cb_lavorazioni
this.cb_allinea_commesse=create cb_allinea_commesse
this.lb_1=create lb_1
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_recupera
this.Control[iCurrent+2]=this.cb_crea_distinta_padri
this.Control[iCurrent+3]=this.sle_cod_utente
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.cb_lavorazioni
this.Control[iCurrent+6]=this.cb_allinea_commesse
this.Control[iCurrent+7]=this.lb_1
this.Control[iCurrent+8]=this.st_2
end on

on w_recupera_distinta.destroy
call super::destroy
destroy(this.cb_recupera)
destroy(this.cb_crea_distinta_padri)
destroy(this.sle_cod_utente)
destroy(this.st_1)
destroy(this.cb_lavorazioni)
destroy(this.cb_allinea_commesse)
destroy(this.lb_1)
destroy(this.st_2)
end on

type cb_recupera from commandbutton within w_recupera_distinta
integer x = 23
integer y = 160
integer width = 768
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Recupera vecchia distinta"
end type

event clicked;string ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_misura
double ldd_quan_tecnica,ldd_quan_utilizzo
long   ll_num_sequenza,ll_fase_ciclo


declare righe_dis_old cursor for 
select  cod_prodotto_padre,
		  num_sequenza,
		  cod_prodotto_figlio,
		  cod_misura,
		  quan_tecnica,
		  quan_utilizzo,
		  fase_ciclo
from    distinta_old
where   cod_azienda=:s_cs_xx.cod_azienda;

open righe_dis_old;

do while 1=1
	fetch righe_dis_old 
	into  :ls_cod_prodotto_padre,
			:ll_num_sequenza,
		   :ls_cod_prodotto_figlio,
			:ls_cod_misura,
		   :ldd_quan_tecnica,
			:ldd_quan_utilizzo,
			:ll_fase_ciclo;

	choose case sqlca.sqlcode
		case 100
			exit

		case is <0
			g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	
	end choose

	insert 
	into distinta
		  (cod_azienda,
			cod_prodotto_padre,
			num_sequenza,
			cod_prodotto_figlio,
			cod_versione,
			cod_versione_figlio,
			cod_misura,
			quan_tecnica,
			quan_utilizzo,
			fase_ciclo,
			dim_x,
			dim_y,
			dim_z,
			dim_t,
			coef_calcolo,
			des_estesa)
	values(:s_cs_xx.cod_azienda,
			 :ls_cod_prodotto_padre,
			 :ll_num_sequenza,
			 :ls_cod_prodotto_figlio,
			 '1',
			 '1',
			 :ls_cod_misura,
			 :ldd_quan_tecnica,
			 :ldd_quan_utilizzo,
			 :ll_fase_ciclo,
			 0,
			 0,
			 0,
		    0,
			 0,
			 null);
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore in inserimento:"+sqlca.sqlerrtext,stopsign!)
		exit
	end if

loop

close righe_dis_old;

g_mb.messagebox("Sep","Recupero avvenuto con successo!",Information!)


end event

type cb_crea_distinta_padri from commandbutton within w_recupera_distinta
integer x = 823
integer y = 160
integer width = 617
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea Distinta Padri"
end type

event clicked;string ls_cod_prodotto_padre,ls_cod_utente


ls_cod_utente = sle_cod_utente.text

declare righe_pf cursor for 
SELECT cod_prodotto_padre
FROM   distinta
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    cod_prodotto_padre in ( SELECT cod_prodotto_padre  
                               FROM   distinta  
                               WHERE cod_azienda = :s_cs_xx.cod_azienda)
AND    cod_prodotto_padre not in ( SELECT cod_prodotto_figlio  
                                   FROM   distinta  
                                   WHERE  cod_azienda = :s_cs_xx.cod_azienda)
GROUP BY cod_prodotto_padre;

open righe_pf;

do while 1=1
	fetch righe_pf
	into  :ls_cod_prodotto_padre;

	choose case sqlca.sqlcode
		case 100
			exit

		case is <0
			g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	
	end choose

	insert 
	into distinta_padri
		  (cod_azienda,
			cod_prodotto,
			cod_versione,
			creato_da,
			approvato_da,
			des_versione,
		   data_creazione,
			data_approvazione,
			flag_predefinita,
		   flag_blocco,
			data_blocco)
	values(:s_cs_xx.cod_azienda,
			 :ls_cod_prodotto_padre,
			 '1',
			 :ls_cod_utente,
			 :ls_cod_utente,
			 null,
			 null,
			 null,
			 'S',
			 'N',
			 null);
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore in inserimento:"+sqlca.sqlerrtext,stopsign!)
		exit
	end if

loop

close righe_pf;

g_mb.messagebox("Sep","Creazione distinta padri avvenuta con successo!",Information!)

end event

type sle_cod_utente from singlelineedit within w_recupera_distinta
integer x = 1006
integer y = 40
integer width = 320
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
end type

type st_1 from statictext within w_recupera_distinta
integer x = 46
integer y = 40
integer width = 928
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Inserire un codice utente esistente:"
boolean focusrectangle = false
end type

type cb_lavorazioni from commandbutton within w_recupera_distinta
integer x = 23
integer y = 260
integer width = 777
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Recupera Lavorazioni"
end type

event clicked;string ls_cod_lavorazione


declare righe_lav cursor for 
SELECT cod_lavorazione
FROM   tes_fasi_lavorazione
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
GROUP BY cod_lavorazione;

open righe_lav;

do while 1=1
	fetch righe_lav
	into  :ls_cod_lavorazione;

	choose case sqlca.sqlcode
		case 100
			exit

		case is <0
			g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	
	end choose

	insert 
	into tab_lavorazioni
		  (cod_azienda,
			cod_lavorazione,
			des_lavorazione)
	values(:s_cs_xx.cod_azienda,
			 :ls_cod_lavorazione,
			 null);
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore in inserimento:"+sqlca.sqlerrtext,stopsign!)
		exit
	end if

loop

close righe_lav;

g_mb.messagebox("Sep","Creazione tab_lavorazioni avvenuta con successo!",Information!)

end event

type cb_allinea_commesse from commandbutton within w_recupera_distinta
integer x = 23
integer y = 360
integer width = 1417
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Allinea anag_commesse con distinta_padri"
end type

event clicked;string ls_cod_prodotto,ls_cod_utente,ls_test


ls_cod_utente = sle_cod_utente.text

declare righe_com cursor for 
SELECT cod_prodotto
FROM   anag_commesse
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    cod_prodotto not in ( SELECT cod_prodotto
                                   FROM   distinta_padri
                                   WHERE  cod_azienda = :s_cs_xx.cod_azienda)
GROUP BY cod_prodotto;

open righe_com;

do while 1=1
	fetch righe_com
	into  :ls_cod_prodotto;

	choose case sqlca.sqlcode
		case 100
			exit

		case is <0
			g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	
	end choose

	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto_figlio=:ls_cod_prodotto;


	if sqlca.sqlcode = 0 then 
		lb_1.additem(ls_cod_prodotto)
	end if
	
	insert 
	into distinta_padri
		  (cod_azienda,
			cod_prodotto,
			cod_versione,
			creato_da,
			approvato_da,
			des_versione,
		   data_creazione,
			data_approvazione,
			flag_predefinita,
		   flag_blocco,
			data_blocco)
	values(:s_cs_xx.cod_azienda,
			 :ls_cod_prodotto,
			 '1',
			 :ls_cod_utente,
			 :ls_cod_utente,
			 null,
			 null,
			 null,
			 'S',
			 'N',
			 null);
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore in inserimento:"+sqlca.sqlerrtext,stopsign!)
		exit
	end if

loop

close righe_com;

g_mb.messagebox("Sep","Allineamento commesse con distinta padri avvenuta con successo!",Information!)

end event

type lb_1 from listbox within w_recupera_distinta
integer x = 23
integer y = 540
integer width = 1120
integer height = 760
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

type st_2 from statictext within w_recupera_distinta
integer x = 23
integer y = 460
integer width = 1157
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Elenco prodotto non inseriti in Distinta_padri"
boolean focusrectangle = false
end type

