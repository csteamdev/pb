﻿$PBExportHeader$w_report_db_varianti_componenti.srw
forward
global type w_report_db_varianti_componenti from w_cs_xx_principale
end type
type dw_folder from u_folder within w_report_db_varianti_componenti
end type
type dw_report from datawindow within w_report_db_varianti_componenti
end type
type cb_salva from commandbutton within w_report_db_varianti_componenti
end type
type dw_invio_pdf from u_dw_search within w_report_db_varianti_componenti
end type
type dw_destinatari from datawindow within w_report_db_varianti_componenti
end type
type dw_stampa from uo_cs_xx_dw within w_report_db_varianti_componenti
end type
end forward

global type w_report_db_varianti_componenti from w_cs_xx_principale
integer width = 4082
integer height = 2628
string title = "Report Richiesta Componenti"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
event ue_resize ( )
event ue_operatore_richiedente ( )
dw_folder dw_folder
dw_report dw_report
cb_salva cb_salva
dw_invio_pdf dw_invio_pdf
dw_destinatari dw_destinatari
dw_stampa dw_stampa
end type
global w_report_db_varianti_componenti w_report_db_varianti_componenti

type variables
string			is_cod_prodotto, is_des_prodotto, is_note_rdt, is_cod_tipo_lista_dist


//deposito, reparto di arrivo del materiale e operaio che fa la richiesta
string			is_cod_deposito_arrivo, is_des_deposito_arrivo, is_cod_reparto_arrivo, is_cod_operaio_richiesta


//dati associati alla riga ordine di vendita collegata eventualmente
string			is_cod_verniciatura, is_cod_comando, is_des_verniciatura, is_des_comando
long			il_anno_registrazione, il_num_registrazione, il_prog_riga_ord_ven, il_barcode, il_anno_commessa, il_num_commessa
decimal		id_quan_ordinata, id_dim_x, id_dim_y, id_dim_z, id_dim_t


long			il_max_livelli, il_num_rdt
datetime		idt_data_consegna, idt_data_pronto, idt_data_rdt
integer		il_anno_rdt


//può valere "ORDINE" se si proviene da codice a barre di produzione oppure "LIBERO" se si vuole comporre la richiesta in maniera libera
string			is_origine





end variables

forward prototypes
public subroutine wf_get_operaio_richiesta (ref string as_nome_cognome)
public subroutine wf_set_nota_riga (long al_row)
public subroutine wf_set_nota_testata ()
public function integer wf_report (ref string as_errore)
public subroutine wf_impostazioni ()
public function string wf_barcode_prodotto (long al_row)
public function integer wf_crea_rdt (ref string as_errore)
public function integer wf_set_recipients (string as_cod_lista_dist, ref string as_errore)
public function integer wf_email (ref string as_errore)
public function integer wf_memorizza_blob (integer ai_anno_rdt, long al_num_rdt, string as_path, ref string as_errore)
public subroutine wf_imposta_campi ()
end prototypes

event ue_resize();
dw_folder.width = this.width - 50
dw_folder.height = this.height

dw_report.width = dw_folder.width - 50
dw_report.height = this.height - 320

dw_stampa.width = dw_report.width
dw_stampa.height = dw_report.height - 150

cb_salva.height = 84


end event

event ue_operatore_richiedente();s_cs_xx_parametri			lstr_parametri

string							ls_operaio, ls_nome, ls_cognome



//operatore richiedente (questa funzione cerca di impostare il cod_operaio dell'utente collegato)
wf_get_operaio_richiesta(ls_operaio)

if is_cod_operaio_richiesta<>"" and not isnull(is_cod_operaio_richiesta) then
	//vuol dire che la funzione precendente è riuscita ad impostare il codice operaio dell'utente collegato
	ls_operaio += "~r~n"+is_des_deposito_arrivo
	
	if not isnull(is_cod_reparto_arrivo) and is_cod_reparto_arrivo<>"" then ls_operaio += " ("+is_cod_reparto_arrivo+")"
	
	dw_report.Modify("operatore_deposito_t.text='" + ls_operaio + "'")
	
	return
end if

//se arrivi fin qui chiedi espressamnte il codice operaio
lstr_parametri.parametro_s_1_a[1] = "TRASF"
lstr_parametri.parametro_s_1_a[2] = ""

openwithparm(w_note_trasf_componenti, lstr_parametri)

lstr_parametri = message.powerobjectparm

if lstr_parametri.parametro_b_1 then
	is_cod_operaio_richiesta = lstr_parametri.parametro_s_1_a[1]
	//ls_cod_tipo_causa_sospensione = lstr_parametri.parametro_s_1_a[2]
	
	select cognome, nome
	into :ls_cognome, :ls_nome
	from anag_operai
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_operaio=:is_cod_operaio_richiesta;
	
	ls_operaio = ls_nome + " " + ls_cognome
	ls_operaio += "~r~n"+is_des_deposito_arrivo
	
	if not isnull(is_cod_reparto_arrivo) and is_cod_reparto_arrivo<>"" then ls_operaio += " ("+is_cod_reparto_arrivo+")"
	
	dw_report.Modify("operatore_deposito_t.text='" + ls_operaio + "'")
	return
	
else
	//annullato dall'operatore
	close(this)
end if

end event

public subroutine wf_get_operaio_richiesta (ref string as_nome_cognome);datastore		lds_data
string				ls_sql, ls_errore, ls_nome, ls_cognome
integer			li_ret

setnull(is_cod_operaio_richiesta)
as_nome_cognome = ""

ls_sql = 	"select cod_operaio, cognome, nome "+&
			"from anag_operai "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_utente='"+s_cs_xx.cod_utente+"' "
						
li_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if li_ret > 0 then
	is_cod_operaio_richiesta = lds_data.getitemstring(1, 1)
	ls_cognome = lds_data.getitemstring(1, 2)
	ls_nome = lds_data.getitemstring(1, 3)
	
	if isnull(ls_cognome) then ls_cognome = ""
	if isnull(ls_nome) then ls_nome = ""
	
	as_nome_cognome = ls_nome + " " + ls_cognome
	
end if

return
end subroutine

public subroutine wf_set_nota_riga (long al_row);string				ls_nota


ls_nota = dw_report.getitemstring(al_row, "note")

s_cs_xx.parametri.parametro_d_10 = 9999
s_cs_xx.parametri.parametro_d_11 = 6666
openwithparm(w_inserisci_altro_valore, ls_nota)

ls_nota = message.stringparm

if ls_nota="cs_team_esc_pressed" then
else
	dw_report.setitem(al_row, "note", ls_nota)
end if
end subroutine

public subroutine wf_set_nota_testata ();string				ls_nota


ls_nota = dw_report.getitemstring(1, "note_testata")

s_cs_xx.parametri.parametro_d_10 = 9999
s_cs_xx.parametri.parametro_d_11 = 6666
openwithparm(w_inserisci_altro_valore, ls_nota)

ls_nota = message.stringparm

if ls_nota="cs_team_esc_pressed" then
else
	dw_report.setitem(1, "note_testata", ls_nota)
end if
end subroutine

public function integer wf_report (ref string as_errore);
long			ll_tot, ll_index, ll_new, ll_num_livello
string			ls_sel, ls_cod_componente, ls_des_componente, ls_um_componente,  ls_note_componente, ls_barcode_prodotto, ls_val
dec{4}		ld_qta_richiesta

dw_report.accepttext()
dw_stampa.reset()
cb_salva.enabled = false

ll_tot = dw_report.rowcount()

if ll_tot <= 0 then
	as_errore = "Nessuna riga presente nella lista!"
	return 1
end if

//le note della testata le prendo dalla riga n°1
is_note_rdt = dw_report.getitemstring(1, "note_testata")

ll_new = 0

for ll_index=1 to ll_tot
	ls_sel = dw_report.getitemstring(ll_index, "flag_sel")
	
	if ls_sel="S" then
		ll_num_livello = dw_report.getitemnumber(ll_index, "num_livello")
		
		ls_cod_componente = dw_report.getitemstring(ll_index, "cod_l_"+string(ll_num_livello))
		ld_qta_richiesta = dw_report.getitemdecimal(ll_index, "qta_l_"+string(ll_num_livello))
		
		if isnull(ld_qta_richiesta) or ld_qta_richiesta<=0 then
			if not g_mb.confirm("Il componente selezionato ("+ls_cod_componente+") "+&
									"ha una quantità richiesta pari a zero e quindi sarà escluso dalla richiesta! Procedere lo stesso?") then
				//hai scelto di non procedere
				as_errore = "Annullato dall'operatore!"
				return 1
			else
				//hai scelto di procedere lo stesso, perciò salta questa riga dall'elaborazione!
				continue
			end if
		end if
		
		ls_des_componente = dw_report.getitemstring(ll_index, "des_l_"+string(ll_num_livello))
		ls_um_componente = dw_report.getitemstring(ll_index, "um_l_"+string(ll_num_livello))
		ls_note_componente = dw_report.getitemstring(ll_index, "note")

		ll_new = dw_stampa.insertrow(0)
		
		dw_stampa.setitem(ll_new, "cod_operaio_richiedente", is_cod_operaio_richiesta)
		dw_stampa.setitem(ll_new, "operatore", dw_report.object.operatore_deposito_t.text)
		dw_stampa.setitem(ll_new, "cod_deposito_richiedente", is_cod_deposito_arrivo)
		dw_stampa.setitem(ll_new, "cod_reparto_richiedente", is_cod_reparto_arrivo)
		dw_stampa.setitem(ll_new, "barcode_produzione", il_barcode)
		
		dw_stampa.object.barcode_produzione_t.text = "*" + string(il_barcode) + "*"
		
		dw_stampa.setitem(ll_new, "anno_ordine", il_anno_registrazione)
		dw_stampa.setitem(ll_new, "num_ordine", il_num_registrazione)
		dw_stampa.setitem(ll_new, "riga_ordine", il_prog_riga_ord_ven)
		
		ls_val = "*" + string(il_anno_registrazione) + string(il_num_registrazione, "000000") + string(il_prog_riga_ord_ven, "0000") + "*"
		dw_stampa.object.barcode_riga_t.text = ls_val
		
		dw_stampa.setitem(ll_new, "data_pronto", idt_data_pronto)
		dw_stampa.setitem(ll_new, "cod_prodotto", is_cod_prodotto)
		dw_stampa.setitem(ll_new, "des_prodotto", is_des_prodotto)
		dw_stampa.setitem(ll_new, "quan_ordine", id_quan_ordinata)
		dw_stampa.setitem(ll_new, "cod_verniciatura", is_cod_verniciatura)
		dw_stampa.setitem(ll_new, "des_verniciatura", is_des_verniciatura)
		dw_stampa.setitem(ll_new, "cod_comando", is_cod_comando)
		dw_stampa.setitem(ll_new, "des_comando", is_des_comando)

		dw_stampa.setitem(ll_new, "dim_x", id_dim_x)
		dw_stampa.setitem(ll_new, "dim_y", id_dim_y)
		dw_stampa.setitem(ll_new, "dim_z", id_dim_z)
		dw_stampa.setitem(ll_new, "dim_t", id_dim_t)
		
		dw_stampa.setitem(ll_new, "cod_componente", ls_cod_componente)
		dw_stampa.setitem(ll_new, "des_componente", ls_des_componente)
		dw_stampa.setitem(ll_new, "quan_utilizzo", ld_qta_richiesta)
		dw_stampa.setitem(ll_new, "um_componente", ls_um_componente)
		
		ls_barcode_prodotto = wf_barcode_prodotto(ll_new)
		dw_stampa.setitem(ll_new, "barcode_prodotto", ls_barcode_prodotto)	

		if trim(ls_note_componente)<>"" and not isnull(ls_note_componente) then dw_stampa.setitem(ll_new, "note_componente", ls_note_componente)

	end if
next

if ll_new = 0 then
	as_errore = "Devi richiedere almeno un componente per procedere!"
	return 1
end if

//alla fine, se c'è una nota generale la inserisco come ultima riga
if is_note_rdt <>"" and not isnull(is_note_rdt) then
	ll_new = dw_stampa.insertrow(0)
	dw_stampa.setitem(ll_new, "note_richiesta", is_note_rdt)
	dw_stampa.setitem(ll_new, "tipo_riga", "N")
end if

cb_salva.enabled = true

return 0
end function

public subroutine wf_impostazioni ();string			ls_filename, ls_temp

//logo su report ----------------------------------------------------------------------
select stringa
into   :ls_filename
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_parametro = 'LET';

ls_filename = s_cs_xx.volume+ls_filename
dw_stampa.Object.p_logo.filename = ls_filename


//tipo lista distribuzione da usare (LRC)------------------------------------------
//			tipo Lista Dist. Richiesta Componenti
guo_functions.uof_get_parametro_azienda('LRC', ls_temp)
if ls_temp<>"" and not isnull(ls_temp) then
	is_cod_tipo_lista_dist = ls_temp
	
	f_po_loaddddw_dw(dw_invio_pdf, &
                 "cod_lista_dist", &
                 sqlca, &
                 "tes_liste_dist", &
                 "cod_lista_dist", &
                 "des_lista_dist", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist='"+is_cod_tipo_lista_dist+"'")
	
else
	is_cod_tipo_lista_dist = ""
	g_mb.warning("Manca il param. aziendale LRC: codice tipo lista distribuzione da usare per l'invio. Non sarà possibile inviare l'e-mail!")
end if

//print preview=YES --------------------------------------------------------------
dw_stampa.object.datawindow.print.preview = "yes"

return
end subroutine

public function string wf_barcode_prodotto (long al_row);string			ls_cod_prodotto, ls_qta, ls_barcode_prodotto
dec{4}		ld_qta

ls_barcode_prodotto = ""

ls_cod_prodotto = dw_stampa.getitemstring(al_row, "cod_componente")
ld_qta = dw_stampa.getitemdecimal(al_row, "quan_utilizzo")

ls_qta = string(ld_qta, "#0.####")
guo_functions.uof_replace_string(ls_qta, ",", ".")

//se l'ultimo non è numerico eliminalo! infatti se è un valore intero scrivilo senza decimal separetor
if not isnumber(right(ls_qta, 1)) then ls_qta = left(ls_qta, len(ls_qta) - 1)

ls_barcode_prodotto = "*" + ls_cod_prodotto + ".." + ls_qta + "*"
		
		
return ls_barcode_prodotto
end function

public function integer wf_crea_rdt (ref string as_errore);integer		li_anno
long			ll_numero, ll_riga, ll_index
datetime		ldt_adesso, ldt_data_fabbisogno
string			ls_cod_prodotto, ls_des_prodotto, ls_note_riga
dec{4}		ld_quantita


if not g_mb.confirm("Creare una nuova Richiesta di trasferimento componenti?") then
	as_errore = "Operazione annullata!"
	return 1
end if

li_anno = f_anno_esercizio()

select max(num_registrazione)
into :ll_numero
from tes_rdt
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura max num.reg. in tes_rdt: "+sqlca.sqlerrtext
	return -1
end if

if isnull(ll_numero) then ll_numero = 0
ll_numero += 1

idt_data_rdt = datetime(today(), 00:00:00)
ldt_adesso = datetime(date(1900,1,1), now())

if is_origine<>"ORDINE" and not isnull(idt_data_pronto) and year(date(idt_data_pronto))>1950 then
	ldt_data_fabbisogno = idt_data_pronto
else
	setnull(ldt_data_fabbisogno)
end if


//inserimento testata ------------------------------------------------------------------------------------
insert into tes_rdt(
	cod_azienda,   
	anno_registrazione,   
	num_registrazione,   
	data_registrazione,   
	ora_registrazione,   
	cod_reparto_richiedente,   
	cod_operaio_richiedente,   
	anno_ordine,   
	num_ordine,   
	riga_ordine,   
	barcode_det,   
	note_richiesta,   
	cod_deposito_evasione,   
	cod_utente_evasione,   
	data_evasione,   
	ora_evasione,
	data_fabbisogno)  
values (	:s_cs_xx.cod_azienda,   
			:li_anno,   
			:ll_numero,   
			:idt_data_rdt,   
			:ldt_adesso,   
			:is_cod_reparto_arrivo,
			:is_cod_operaio_richiesta,   
			:il_anno_registrazione,   
			:il_num_registrazione,   
			:il_prog_riga_ord_ven,   
			:il_barcode,   
			:is_note_rdt,   
			null,   
			null,   
			null,   
			null,
			:ldt_data_fabbisogno);

if sqlca.sqlcode<0 then
	as_errore = "Errore in inserimento testata RDT: "+sqlca.sqlerrtext
	return -1
end if

ll_riga = 0

//inserimento dettagli -------------------------------------------------------------------------------------
for ll_index=1 to dw_stampa.rowcount()
	//la riga di tipo N è una riga fittizia per visualizzare la nota generale di tutta la RDT
	if dw_stampa.getitemstring(ll_index, "tipo_riga") = "D" then
		ls_cod_prodotto	= dw_stampa.getitemstring(ll_index, "cod_componente")
		ls_des_prodotto	= dw_stampa.getitemstring(ll_index, "des_componente")
		ld_quantita			= dw_stampa.getitemdecimal(ll_index, "quan_utilizzo")
		ls_note_riga			= dw_stampa.getitemstring(ll_index, "note_componente")

		if ld_quantita>0 and not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		else
			continue
		end if

		ll_riga += 10

		insert into det_rdt(
			cod_azienda,   
			anno_registrazione,   
			num_registrazione,   
			prog_riga_rdt,   
			cod_prodotto,   
			des_prodotto,   
			quan_richiesta,   
			note_riga)
		values (	:s_cs_xx.cod_azienda,   
					:li_anno,   
					:ll_numero,
					:ll_riga,
					:ls_cod_prodotto,
					:ls_des_prodotto,
					:ld_quantita,
					:ls_note_riga);
		
		if sqlca.sqlcode<0 then
			as_errore = "Errore in inserimento riga " + string(ll_riga) + " nel dettaglio RDT: "+sqlca.sqlerrtext
			return -1
		end if
		
		dw_stampa.setitem(ll_index, "riga_richiesta", ll_riga)
	
	end if
	
	dw_stampa.setitem(ll_index, "anno_richiesta", li_anno)
	dw_stampa.setitem(ll_index, "num_richiesta", ll_numero)

next

//imposto anno e numero RDT nelle variabili di istanza
il_anno_rdt = li_anno
il_num_rdt = ll_numero


return 0
end function

public function integer wf_set_recipients (string as_cod_lista_dist, ref string as_errore);


return dw_destinatari.retrieve(s_cs_xx.cod_azienda, is_cod_tipo_lista_dist, as_cod_lista_dist)
end function

public function integer wf_email (ref string as_errore);

// funzione che provvedere all'invio MAIL della RDT


string 				ls_path, ls_messaggio, ls_destinatari[], ls_allegati[], ls_subject, ls_message, ls_mittente, &
						ls_nota, ls_azienda, ls_nome, ls_cognome, ls_from, ls_smtp, ls_usr_smtp, ls_pwd_smtp, ls_cc[]
uo_outlook  		    luo_outlook
uo_archivia_pdf 	luo_pdf
boolean				lb_ret
long					ll_index
uo_sendmail		luo_send


//leggo l'azienda per metterlo nel testo dell'e-mail ---------------------------------------------------
select rag_soc_1
into :ls_azienda
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ls_azienda) then ls_azienda=""

//recupero operaio che invia
select cognome, nome
into :ls_cognome, :ls_nome
from anag_operai
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_operaio=:is_cod_operaio_richiesta;

luo_send = create uo_sendmail

//PARAMETRI MITTENTE (serve per le credenziali di invio via smtp) ---------------------------------
//lo leggo dal deposito operatore
select email, smtp, usr_smtp, pwd_smtp
into :ls_from, :ls_smtp, :ls_usr_smtp, :ls_pwd_smtp
from anag_depositi_mittenti
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_deposito=:is_cod_deposito_arrivo;

if sqlca.sqlcode<0 then
	destroy luo_send
	as_errore = "Errore in lettura parametri sender: "+sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode=100 or isnull(ls_from) or ls_from="" or isnull(ls_smtp) or ls_smtp="" then
	destroy luo_send
	as_errore = "Mancano i parametri sender per stabilimento "+is_des_deposito_arrivo + " (tabella anag_depositi_mittenti)"
	return -1
end if

if isnull(ls_usr_smtp) then ls_usr_smtp = ""
if isnull(ls_usr_smtp) then ls_pwd_smtp = ""

luo_send.uof_set_from( ls_from )

//per conoscenza, solo se l'utente collegato ha l'indirizzo email inserito in tabella utenti
select e_mail
into :ls_mittente
from utenti
where cod_utente=:s_cs_xx.cod_utente;

if not isnull(ls_mittente) and ls_mittente<>"" then
	ls_cc[1] = ls_mittente
	luo_send.uof_add_cc( ls_cc[])
end if

//imposto i destinatari dell'email ---------------------------------------------------------------------
for ll_index=1 to dw_destinatari.rowcount()
	ls_destinatari[ll_index] = dw_destinatari.getitemstring(ll_index, "indirizzo")
next

luo_send.uof_add_to( ls_destinatari[])

//imposto l'allegato pdf nell'email -------------------------------------------------------------------

//creo il pdf da allegare ########
luo_pdf = CREATE uo_archivia_pdf
ls_path = luo_pdf.uof_crea_pdf_path(dw_stampa)
destroy luo_pdf

if ls_path = "errore" then
	as_errore = "Errore nella creazione del file pdf"
	return -1
end if
//############################

ls_allegati[1]    = ls_path
luo_send.uof_add_attachments(ls_allegati[])

//oggetto dell'email ----------------------------------------------------------------------------------
ls_subject = ls_azienda+" - Richiesta Trasf.Componenti n° " + string(il_anno_rdt) + "/" + string(il_num_rdt) + &
				" del " + string(idt_data_rdt,"dd/mm/yyyy") + " da "+is_cod_reparto_arrivo + " ("+is_des_deposito_arrivo+")"
luo_send.uof_set_subject( ls_subject )

//testo dell'email -------------------------------------------------------------------------------------
ls_message = 	"Richiesta Materiale n° "+string(il_anno_rdt) + "/" + string(il_num_rdt)+" da  " + ls_cognome + " " + ls_nome + " ("+is_cod_reparto_arrivo+") come da file allegato"

if il_anno_registrazione>0 and il_num_registrazione>0 and il_prog_riga_ord_ven>0 then
	ls_message += " per riga ordine n° "+string(il_anno_registrazione)+"/"+string(il_num_registrazione)+"/"+string(il_prog_riga_ord_ven)
end if

				
luo_send.uof_set_message( ls_message )

luo_send.uof_set_smtp( ls_smtp, ls_usr_smtp, ls_pwd_smtp)

if not luo_send.uof_send( ) then
	as_errore = "Errore in fase di invio RDT"
	
	//per pulizia cancello il file
	filedelete(ls_path)
	
	destroy luo_send;
	return -1
else
	
	destroy luo_send;
	
	//Messaggio inviato quindi salvo il blob dell'allegato
	if wf_memorizza_blob(il_anno_rdt, il_num_rdt, ls_path, as_errore) < 0 then
		//per pulizia cancello il file
		filedelete(ls_path)
		rollback;
		return -2
	else
		//per pulizia cancello il file e poi faccio commit
		filedelete(ls_path)
		commit;
		return 0
	end if
end if


end function

public function integer wf_memorizza_blob (integer ai_anno_rdt, long al_num_rdt, string as_path, ref string as_errore);long					ll_prog_mimetype, ll_cont
blob					l_blob
string					ls_des_note, ls_cod_nota

//il file è sicuramente PDF, lo metto in una variabile blob
guo_functions.uof_file_to_blob(as_path, l_blob)


//recupero progressivo mimetype
SELECT prog_mimetype  
INTO   :ll_prog_mimetype  
FROM   tab_mimetype  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 estensione = 'PDF'  OR estensione = 'pdf';
		 
		 
if sqlca.sqlcode = 100 then
	as_errore = "Manca il mimetype PDF in tabella: mail inviata ma risulta impossibile archiviare il PDF"
	return -1
end if

setnull(ls_cod_nota)

select 	max(cod_nota) 
into 		:ls_cod_nota
from 		tes_rdt_note
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno_rdt and
			num_registrazione = :al_num_rdt and
			isnumeric(cod_nota)=1;

if isnull(ls_cod_nota) or ls_cod_nota="" then ls_cod_nota = "0"
ll_cont = long(ls_cod_nota)
ll_cont += 1
ls_cod_nota = string(ll_cont, "000") 

ls_des_note = "Utente: " + s_cs_xx.cod_utente + " - Data Invio: "+string(datetime(today(), now()), "dd/mm/yyyy hh:mm:ss")

insert into tes_rdt_note  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,
		  cod_nota,
		  des_nota,
		  note,
		  prog_mimetype)
values (	:s_cs_xx.cod_azienda,
			:ai_anno_rdt,
			:al_num_rdt,
			:ls_cod_nota,
			'RDT inviata via e-mail',
			:ls_des_note,
			:ll_prog_mimetype);

if sqlca.sqlcode = 0 then

	updateblob tes_rdt_note
	set note_esterne = :l_blob
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno_rdt and
			num_registrazione = :al_num_rdt and
			cod_nota = :ls_cod_nota;

	if sqlca.sqlcode <> 0 then
		as_errore = "(1) Mail inviata ma c'è stato un errore nell'archiviazione del PDF: " + sqlca.sqlerrtext
		return -1
	end if

else

	if sqlca.sqlcode <> 0 then
		as_errore = "(2) Mail inviata ma c'è stato un errore nell'archiviazione del PDF: " + sqlca.sqlerrtext
		return -1
	end if
end if

return 0
end function

public subroutine wf_imposta_campi ();//imposta sulla dw_report il campo data fabbisogno (pronto reparto)
//nome e cognome dell'operatore, deposito e reparto di appartenenza


string				ls_cognome, ls_nome, ls_operaio


dw_report.object.data_pronto_t.text = string(idt_data_pronto, "dd/mm/yyyy")

select a.cod_deposito,b. des_deposito
into :is_cod_deposito_arrivo, :is_des_deposito_arrivo
from anag_reparti as a
left outer join anag_depositi as b on 	b.cod_azienda=a.cod_azienda and
												b.cod_deposito=a.cod_deposito
where 	a.cod_azienda=:s_cs_xx.cod_azienda and
			a.cod_reparto=:is_cod_reparto_arrivo;


select cognome, nome
into :ls_cognome, :ls_nome
from anag_operai
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_operaio=:is_cod_operaio_richiesta;

ls_operaio = is_cod_operaio_richiesta
if ls_cognome<>"" and not isnull(ls_cognome) then ls_operaio += " " + ls_cognome
if ls_nome<>"" and not isnull(ls_nome) then ls_operaio += " " + ls_nome

if is_des_deposito_arrivo<>"" and not isnull(is_des_deposito_arrivo) then ls_operaio += "~r~n" + is_des_deposito_arrivo
if not isnull(is_cod_reparto_arrivo) and is_cod_reparto_arrivo<>"" then ls_operaio += " ("+is_cod_reparto_arrivo+")"

dw_report.Modify("operatore_deposito_t.text='" +ls_operaio + "'")
end subroutine

on w_report_db_varianti_componenti.create
int iCurrent
call super::create
this.dw_folder=create dw_folder
this.dw_report=create dw_report
this.cb_salva=create cb_salva
this.dw_invio_pdf=create dw_invio_pdf
this.dw_destinatari=create dw_destinatari
this.dw_stampa=create dw_stampa
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_folder
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.cb_salva
this.Control[iCurrent+4]=this.dw_invio_pdf
this.Control[iCurrent+5]=this.dw_destinatari
this.Control[iCurrent+6]=this.dw_stampa
end on

on w_report_db_varianti_componenti.destroy
call super::destroy
destroy(this.dw_folder)
destroy(this.dw_report)
destroy(this.cb_salva)
destroy(this.dw_invio_pdf)
destroy(this.dw_destinatari)
destroy(this.dw_stampa)
end on

event pc_setwindow;call super::pc_setwindow;
string						ls_vuoto[], ls_barcode_riga, ls_operaio, ls_dimensioni, ls_valore, ls_cognome, ls_nome
decimal					ld_vuoto[]
datawindow				ldw_vuoto
datetime					ldt_null
integer					li_ret, ll_index
windowobject 			l_objects[], l_vuoto[]


dw_stampa.ib_dw_report = true
set_w_options(c_closenosave)

setnull(ldt_null)


if s_cs_xx.parametri.parametro_d_1_a[1] = -767 then
	is_origine = "LIBERO"
	
	dw_report.object.composizione_libera_t.visible = true
	
	//proveni dal comando di compilazione amnuale della richiesta e non da barcode ordine di produzione
	is_cod_operaio_richiesta = s_cs_xx.parametri.parametro_s_1_a[1]
	is_cod_reparto_arrivo = s_cs_xx.parametri.parametro_s_1_a[2]
	idt_data_pronto = s_cs_xx.parametri.parametro_data_3

	il_max_livelli = 1
	
	dw_report.object.b_aggiungi.visible = true
	dw_report.object.b_elimina.visible = true
	dw_report.object.b_prodotto.text = "Sel.Articolo"
	
	dw_report.object.data_pronto_label_t.text = "Data Fabb.:"
	dw_stampa.object.data_pronto_t.text =  "Data Fabb.:"
	
	wf_imposta_campi()

	//annullo invece i parametri della riga ordine, in quanto non esiste in questo caso
	is_cod_prodotto=""
	is_des_prodotto=""
	setnull(il_anno_registrazione)
	setnull(il_num_registrazione)
	setnull(il_prog_riga_ord_ven)
	setnull(il_barcode)
	setnull(il_anno_commessa)
	setnull(il_num_commessa)
	is_cod_verniciatura=""
	is_cod_comando=""
	is_des_verniciatura=""
	is_des_comando=""
	setnull(id_quan_ordinata)
	setnull(id_dim_x)
	setnull(id_dim_y)
	setnull(id_dim_z)
	setnull(id_dim_t)
	setnull(idt_data_consegna)
	

else
	is_origine = "ORDINE"
	
	//da codice a barre di produzione, quindi leggi le varianti di secondo livello della distinta base del prodotto finito della riga ordine corrispondente
	is_cod_prodotto = s_cs_xx.parametri.parametro_s_1_a[1]
	is_des_prodotto = s_cs_xx.parametri.parametro_s_1_a[2]
	il_anno_registrazione = s_cs_xx.parametri.parametro_d_1_a[1]
	il_num_registrazione = s_cs_xx.parametri.parametro_d_1_a[2]
	il_prog_riga_ord_ven = s_cs_xx.parametri.parametro_d_1_a[3]
	il_max_livelli = s_cs_xx.parametri.parametro_d_1_a[4]
	il_barcode = s_cs_xx.parametri.parametro_d_1_a[5]
	id_quan_ordinata = s_cs_xx.parametri.parametro_d_1_a[6]
	il_anno_commessa = s_cs_xx.parametri.parametro_d_1_a[7]
	il_num_commessa = s_cs_xx.parametri.parametro_d_1_a[8]
	idt_data_consegna = s_cs_xx.parametri.parametro_data_3
	idt_data_pronto = s_cs_xx.parametri.parametro_data_4
	
end if


//reset struttura globale
s_cs_xx.parametri.parametro_s_1_a[] = ls_vuoto[]
s_cs_xx.parametri.parametro_d_1_a[] = ld_vuoto[]
s_cs_xx.parametri.parametro_data_3 = ldt_null
s_cs_xx.parametri.parametro_data_4 = ldt_null

l_objects[1] = dw_report
dw_folder.fu_AssignTab(1, "Preparazione Report", l_objects[])

l_objects[] = l_vuoto[]
l_objects[1] = dw_stampa
l_objects[2] = cb_salva
dw_folder.fu_AssignTab(2, "Stampa Report", l_objects[])

l_objects[] = l_vuoto[]
l_objects[1] = dw_invio_pdf
l_objects[2] = dw_destinatari
dw_folder.fu_AssignTab(3, "Invio Report", l_objects[])
dw_folder.fu_disabletab(3)

dw_folder.fu_FolderCreate(3, 3)

dw_destinatari.settransobject(sqlca)

dw_stampa.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )


this.height = w_cs_xx_mdi.mdi_1.height - 50
postevent("ue_resize")

dw_report.reset()
dw_stampa.reset()

//immagine presenza note
ls_valore = s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eliminata.png"
dw_report.object.p_note.FileName = ls_valore


if is_origine = "ORDINE" then
	//prodotto
	ls_valore = is_cod_prodotto + " " + is_des_prodotto
	dw_report.Modify("prodotto_t.text='" + ls_valore + "'")
	
	//numero riga ordine
	dw_report.Modify("ordine_t.text='" + string(il_anno_registrazione) + " / " +  string(il_num_registrazione) + " / " +  string(il_prog_riga_ord_ven) + "'")
	
	//quantità ordinata
	if isnull(id_quan_ordinata) or id_quan_ordinata=0 then id_quan_ordinata = 1
	dw_report.Modify("quan_ordinata_t.text='" + string(id_quan_ordinata,"###,###,##0.00##") + "'")
	
	//barcode di produzione
	if il_barcode>0 then dw_report.Modify("barcode_t.text='*" + string(il_barcode) + "*'")


	//recupero deposito, reparto e data pronto del reparto che fa richiesta, tramite il barcode scansionato
	select anag_reparti.cod_deposito,anag_depositi.des_deposito,det_ordini_produzione.cod_reparto,det_ordini_produzione.data_pronto
	into		:is_cod_deposito_arrivo, :is_des_deposito_arrivo, :is_cod_reparto_arrivo, :idt_data_pronto
	from det_ordini_produzione
	join anag_reparti on anag_reparti.cod_azienda=det_ordini_produzione.cod_azienda and
								anag_reparti.cod_reparto=det_ordini_produzione.cod_reparto
	join anag_depositi on anag_depositi.cod_azienda=anag_reparti.cod_azienda and
								 anag_depositi.cod_deposito=anag_reparti.cod_deposito
	where 	det_ordini_produzione.cod_azienda=:s_cs_xx.cod_azienda and
				det_ordini_produzione.progr_det_produzione=:il_barcode;

	//data pronto reparto richiedente
	ls_valore = string(idt_data_pronto,"dd/mm/yyyy")
	dw_report.Modify("data_pronto_t.text='" + ls_valore + "'")

	//leggo la verniciatura, il comando e le 4 dimensioni dalla comp_det_ord_ven
	select cod_verniciatura, cod_comando, dim_x, dim_y, dim_z, dim_t
	into :is_cod_verniciatura, :is_cod_comando, :id_dim_x, :id_dim_y, :id_dim_z, :id_dim_t
	from comp_det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:il_anno_registrazione and
				num_registrazione=:il_num_registrazione and
				prog_riga_ord_ven=:il_prog_riga_ord_ven;

	//set verniciatura
	if is_cod_verniciatura<>"" and not isnull(is_cod_verniciatura) then
		select des_prodotto
		into :is_des_verniciatura
		from anag_prodotti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:is_cod_verniciatura;
		
		ls_valore = is_cod_verniciatura + " " + is_des_verniciatura
		dw_report.Modify("verniciatura_t.text='" + ls_valore + "'")
	else
		dw_report.Modify("verniciatura_t.text=''")
	end if

	//set comando
	if is_cod_comando<>"" and not isnull(is_cod_comando) then
		select des_prodotto
		into :is_des_comando
		from anag_prodotti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:is_cod_comando;
		
		ls_valore = is_cod_comando + " " + is_des_comando
		dw_report.Modify("comando_t.text='" + ls_valore + "'")
	else
		dw_report.Modify("comando_t.text=''")
	end if

	//set 4 dimensioni
	ls_dimensioni = ""
	if id_dim_x>0 then ls_dimensioni += "DIM_x: " + string(id_dim_x, "###,##0.00") + "   "
	if id_dim_y>0 then ls_dimensioni += "DIM_y: " + string(id_dim_y, "###,##0.00") + "   "
	if id_dim_z>0 then ls_dimensioni += "DIM_z: " + string(id_dim_z, "###,##0.00") + "   "
	if id_dim_t>0 then ls_dimensioni += "DIM_t: " + string(id_dim_t, "###,##0.00")
	dw_report.Modify("dimensioni_t.text='" + ls_dimensioni + "'")

	//a seconda del livello massimo fino al quale esplodere, metti in protezione i campi
	for ll_index=1 to 7
		if ll_index = il_max_livelli then
			//non fare nulla, il campo è già formattato correttamente per l'editing ...
		else
			dw_report.Modify("qta_l_"+string(ll_index)+".tabsequence=0")
		end if
	next
	
	li_ret = s_cs_xx.parametri.parametro_dw_2.RowsCopy(1, s_cs_xx.parametri.parametro_dw_2.RowCount(), Primary!, dw_report, 1, Primary!)
	//alla fine pulisco
	s_cs_xx.parametri.parametro_dw_2 = ldw_vuoto

	//imposta operaio richiedente
	event post ue_operatore_richiedente()

else
	dw_report.Modify("prodotto_t.text=''")
	dw_report.Modify("ordine_t.text=''")
	dw_report.Modify("quan_ordinata_t.text=''")
	dw_report.Modify("barcode_t.text=''")
	
	//data fabbisogno
	ls_valore = string(idt_data_pronto,"dd/mm/yyyy")
	dw_report.Modify("data_pronto_t.text='" + ls_valore + "'")
	dw_report.Modify("verniciatura_t.text=''")
	dw_report.Modify("comando_t.text=''")
	dw_report.Modify("dimensioni_t.text=''")
	
	//-------------------------------------------------------------------------------------------------------
	//inserisco la prima riga, che sarà vuota, serve per le note di testata
	dw_report.insertrow(0)
	dw_report.setitem(1, "flag_sel", "N")
	dw_report.setitem(1, "tipo_riga", -1)
	dw_report.setitem(1, "num_livello", -1)
	dw_report.setitem(1, "max_livello", il_max_livelli)
	dw_report.setitem(1, "cod_l_"+string(il_max_livelli), "")
	dw_report.setitem(1, "des_l_"+string(il_max_livelli), "")
	dw_report.setitem(1, "qta_l_"+string(il_max_livelli), 0)
	dw_report.setitem(1, "um_l_"+string(il_max_livelli), "")
	//-------------------------------------------------------------------------------------------------------
end if



dw_folder.fu_SelectTab(1)

wf_impostazioni()

iuo_dw_main = dw_stampa




end event

type dw_folder from u_folder within w_report_db_varianti_componenti
integer x = 5
integer y = 20
integer width = 4055
integer height = 2512
integer taborder = 10
boolean border = false
end type

type dw_report from datawindow within w_report_db_varianti_componenti
integer x = 23
integer y = 124
integer width = 4037
integer height = 2396
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_report_db_varianti_componenti"
boolean vscrollbar = true
boolean livescroll = true
end type

event buttonclicked;string				ls_errore, ls_colonna, ls_cod_prodotto, ls_des_prodotto, ls_cod_misura_mag
integer			li_ret, ll_num_livello
long				ll_new
datawindow		ldw_data
any				la_any[]


choose case dwo.name
	
	//#####################################################################
	//questi due pulsanti sono visibili solo nel caso di libera composizione della RDT
	case "b_aggiungi"
		if is_origine <>"LIBERO" then return
		
		//seleziona il prodotto da aggiungere, prima ...
		
		
		setnull(ldw_data)
		guo_ricerca.uof_set_response()
		guo_ricerca.uof_ricerca_prodotto(ldw_data,"cod_l_"+string(il_max_livelli))
		
		guo_ricerca.uof_get_results( la_any)
		if upperbound(la_any[])>0 then
		
			ls_cod_prodotto=la_any[1]
			if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
		
				//recupera descrizione e UMmag del prodotto
				select des_prodotto, cod_misura_mag
				into :ls_des_prodotto, :ls_cod_misura_mag
				from anag_prodotti
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode<0 then
					g_mb.error("Errore in ricerca dati prodotto: " + sqlca.sqlerrtext)
					return
				end if
				
				if ls_cod_misura_mag="" then setnull(ls_cod_misura_mag)
			
				ll_new = dw_report.insertrow(0)
				dw_report.setitem(ll_new, "flag_sel", "S")
				dw_report.setitem(ll_new, "tipo_riga", 3)
				dw_report.setitem(ll_new, "num_livello", il_max_livelli)
				dw_report.setitem(ll_new, "max_livello", il_max_livelli)
				dw_report.setitem(ll_new, "cod_l_"+string(il_max_livelli), ls_cod_prodotto)
				dw_report.setitem(ll_new, "des_l_"+string(il_max_livelli), ls_des_prodotto)
				dw_report.setitem(ll_new, "qta_l_"+string(il_max_livelli), 0)
				dw_report.setitem(ll_new, "um_l_"+string(il_max_livelli), ls_cod_misura_mag)
				
				dw_report.setrow(ll_new)
				dw_report.setfocus()
				dw_report.setcolumn("qta_l_"+string(il_max_livelli))
				dw_report.selecttext(1,20)
				
			else
				return
			end if
		else 
			return
		end if
		
		
	case "b_elimina"
		if is_origine <>"LIBERO" then return
		
		if row>0 then
			if g_mb.confirm("Eliminare la riga dalla lista?") then
				dw_report.deleterow(row)
			end if
		end if
	//#####################################################################


	case "b_stampa"
		dw_stampa.setredraw(false)
		li_ret = wf_report(ls_errore)
		dw_stampa.setredraw(true)
		
		if li_ret < 0 then
			g_mb.error(ls_errore)
			return
			
		elseif li_ret> 0 then
			g_mb.warning(ls_errore)
			return
			
		else
			//tutto OK
			dw_folder.fu_SelectTab(2)
		end if
	
	case "b_note"
		if row>0 then
			wf_set_nota_riga(row)
		end if
		
	case "b_note_testata"
		//solo sulla prima riga
		if row=1 then
			wf_set_nota_testata()
		end if
		
	case "b_prodotto"
		if row>0 then
			ll_num_livello = dw_report.getitemnumber(row, "num_livello")
			ls_colonna = "cod_l_"+string(ll_num_livello)
			setrow(row)
			guo_ricerca.uof_ricerca_prodotto(dw_report, ls_colonna)
		end if
end choose
end event

event itemchanged;
string			ls_des_prodotto
long			ll_num_livello



if row>0 then
	choose case left(dwo.name, 5)
	
		case "cod_l"
			if data<>"" then
				select des_prodotto
				into :ls_des_prodotto
				from anag_prodotti
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							cod_prodotto=:data;
			else
				ls_des_prodotto = ""
			end if
			
			ll_num_livello = dw_report.getitemnumber(row, "num_livello")
			
			setitem(row, "des_l_"+string(ll_num_livello), ls_des_prodotto)

			
	end choose
end if



end event

event doubleclicked;s_cs_xx_parametri				lstr_parametri
string								ls_cod_operaio, ls_cod_reparto
datetime							ldt_data_fabbisogno


choose case dwo.name

	case "operatore_deposito_t", "data_pronto_t", "data_pronto_label_t"
		if is_origine <>"LIBERO" then return


		lstr_parametri.parametro_s_1 = is_cod_operaio_richiesta
		lstr_parametri.parametro_s_2 = is_cod_reparto_arrivo
		lstr_parametri.parametro_data_1 = idt_data_pronto

		openwithparm(w_sel_reparto_data, lstr_parametri)
		lstr_parametri = message.powerobjectparm
		
		if not lstr_parametri.parametro_b_1 then
			//operazione annullata dall'utente
			return
		end if
		
		is_cod_operaio_richiesta = lstr_parametri.parametro_s_1_a[1]
		is_cod_reparto_arrivo = lstr_parametri.parametro_s_1_a[2]
		idt_data_pronto = lstr_parametri.parametro_data_1
		
		wf_imposta_campi()
	
end choose
end event

type cb_salva from commandbutton within w_report_db_varianti_componenti
integer x = 78
integer y = 152
integer width = 370
integer height = 84
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Crea RDT"
end type

event clicked;integer			li_ret
string				ls_errore


li_ret = wf_crea_rdt(ls_errore)

if li_ret < 0 then
	rollback;
	g_mb.error(ls_errore)
	return
	
elseif li_ret > 0 then
	rollback;
	g_mb.warning(ls_errore)
	return
else
	//creata RDT:	Disabilito il folder della generazione, 
	//					Disabilito il pulsante SALVA
	//					Abilito il folder per l'invio mail (solo se il parametro LRC è impostato)
	commit;
	if is_cod_tipo_lista_dist <> "" then dw_folder.fu_enabletab(3)
	
	g_mb.success("E' stata creata la RDT n° "+string(il_anno_rdt) + "/" + string(il_num_rdt))
	
	//disabilito il folder della creazione
	this.enabled = false
	dw_folder.fu_disabletab(1)
	
	return
end if

end event

type dw_invio_pdf from u_dw_search within w_report_db_varianti_componenti
integer x = 27
integer y = 252
integer width = 2926
integer height = 148
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_lista_dist_richiesta_comp"
boolean border = false
end type

event itemchanged;call super::itemchanged;string			ls_errore
long			ll_tot

choose case dwo.name
	case "cod_lista_dist"
		object.b_invia.enabled = false
		dw_destinatari.reset()
		
		if data <>"" then
			ll_tot = wf_set_recipients(data, ls_errore)
			
			if ll_tot < 0 then
				g_mb.error(ls_errore)
				return
			elseif ll_tot = 0 then
				//no destinatari trovati
				dw_destinatari.insertrow(0)
				dw_destinatari.setitem(1, 2, "Nessun destinatario in questa lista!")
			else
				//OK
				object.b_invia.enabled = true
			end if
		end if
		
		
end choose
end event

event buttonclicked;call super::buttonclicked;string			ls_errore
integer		li_ret

choose case dwo.name
	case "b_invia"
		setpointer(Hourglass!)
		pcca.mdi_frame.setmicrohelp("Invio e-mail in corso, attendere ...")		
		object.t_log.text = "Invio e-mail in corso, attendere ..."
		
		li_ret = wf_email(ls_errore)
		
		pcca.mdi_frame.setmicrohelp("Pronto!")
		object.t_log.text = "Pronto!"
		setpointer(Arrow!)
		
		if li_ret<0 then
			
			if li_ret = -1 then
				g_mb.error(ls_errore)
			else
				//mail inviata ma c'è stato un problema nell'archiviazione dell'allegato
				g_mb.warning(ls_errore)
			end if
			
		else
			//tutto OK (commit già fatto)
			g_mb.success("E-mail inviata con successo!")
		end if
		
		return
		
		
end choose
end event

type dw_destinatari from datawindow within w_report_db_varianti_componenti
integer x = 110
integer y = 404
integer width = 2725
integer height = 1056
integer taborder = 50
boolean bringtotop = true
string title = "none"
string dataobject = "d_destinatari_richiesta_comp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

type dw_stampa from uo_cs_xx_dw within w_report_db_varianti_componenti
integer x = 23
integer y = 252
integer width = 4032
integer height = 2240
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_report_db_varianti_comp_stampa"
boolean hscrollbar = true
boolean vscrollbar = true
long il_limit_campo_note = 1000
end type

