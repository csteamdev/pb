﻿$PBExportHeader$w_report_distinta_storico.srw
$PBExportComments$Window report dello storico della distinta base
forward
global type w_report_distinta_storico from w_cs_xx_principale
end type
type ddlb_prog_storico from dropdownlistbox within w_report_distinta_storico
end type
type st_2 from statictext within w_report_distinta_storico
end type
type cb_report from commandbutton within w_report_distinta_storico
end type
type cb_stampa from commandbutton within w_report_distinta_storico
end type
type dw_ricerca from u_dw_search within w_report_distinta_storico
end type
type dw_report from uo_cs_xx_dw within w_report_distinta_storico
end type
type ddlb_versione_origine from dropdownlistbox within w_report_distinta_storico
end type
type st_1 from statictext within w_report_distinta_storico
end type
end forward

global type w_report_distinta_storico from w_cs_xx_principale
integer width = 4146
integer height = 2104
string title = "Report Storico Distinta Base"
ddlb_prog_storico ddlb_prog_storico
st_2 st_2
cb_report cb_report
cb_stampa cb_stampa
dw_ricerca dw_ricerca
dw_report dw_report
ddlb_versione_origine ddlb_versione_origine
st_1 st_1
end type
global w_report_distinta_storico w_report_distinta_storico

type variables
long il_progressivi[], il_progressivo
end variables

forward prototypes
public function integer wf_report (string fs_cod_prodotto, string fs_cod_versione, long fl_prog_storico, long fl_num_livello_cor, ref string fs_errore)
end prototypes

public function integer wf_report (string fs_cod_prodotto, string fs_cod_versione, long fl_prog_storico, long fl_num_livello_cor, ref string fs_errore);integer li_num_priorita, li_risposta, li_filenum

long    ll_i, ll_num_figli, ll_num_sequenza, ll_t, ll_num_righe, ll_j, ll_giri

double  ldd_quan_utilizzo, ldd_tempo_lavorazione

string  ls_des_prodotto, ls_cod_prodotto_figlio, ls_cod_prodotto_padre, ls_flag_materia_prima, ls_cod_misura_mag, &
        ls_cod_formula_quan_utilizzo, ls_test_prodotto_f, ls_test_prodotto_f_1

setpointer(hourglass!)

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_prodotto = :fs_cod_prodotto;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta_storico"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve( s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione, fl_prog_storico)

do while ll_giri < 2
	
	if ll_giri = 0 then
		ll_giri = 1
	else
		ll_giri++
	end if
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")

	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	
	ls_cod_prodotto_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_padre")
	
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")
	
	
	if ll_giri = 1 then
			
		setnull(ls_test_prodotto_f_1)
			
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f_1
		from   distinta_storico
		where  cod_azienda = :s_cs_xx.cod_azienda	and	 
		       cod_prodotto_padre = :ls_cod_prodotto_figlio;
	
		if not (isnull(ls_test_prodotto_f_1) or ls_test_prodotto_f_1 = "") then continue
			
	else
			
		setnull(ls_test_prodotto_f_1)
			
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f_1
		from   distinta_storico
		where  cod_azienda = :s_cs_xx.cod_azienda	and	 
		       cod_prodotto_padre = :ls_cod_prodotto_figlio;
	
		if isnull(ls_test_prodotto_f_1) or ls_test_prodotto_f_1 = "" then continue
			
	end if	
	
	select des_prodotto,
			 cod_misura_mag
	into   :ls_des_prodotto,
			 :ls_cod_misura_mag
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ls_des_prodotto = trim(ls_des_prodotto)

	if isnull(ls_cod_misura_mag) then ls_cod_misura_mag = ""

	select cod_formula_quan_utilizzo
	into   :ls_cod_formula_quan_utilizzo
	from   distinta_storico
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_prodotto_padre = :ls_cod_prodotto_padre	and    
			 num_sequenza = :ll_num_sequenza	and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and    
			 cod_versione = :fs_cod_versione and
			 prog_storico = :fl_prog_storico;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
			 
	ll_j = dw_report.insertrow(0)
	dw_report.setitem(ll_j,"num_livello",fl_num_livello_cor)

	choose case fl_num_livello_cor
		case 1
			dw_report.setitem(ll_j,"cod_l_1",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_1",ls_des_prodotto)
		case 2
			dw_report.setitem(ll_j,"cod_l_2",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_2",ls_des_prodotto)
		case 3
			dw_report.setitem(ll_j,"cod_l_3",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_3",ls_des_prodotto)
		case 4
			dw_report.setitem(ll_j,"cod_l_4",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_4",ls_des_prodotto)
		case 5
			dw_report.setitem(ll_j,"cod_l_5",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_5",ls_des_prodotto)
		case 6
			dw_report.setitem(ll_j,"cod_l_6",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_6",ls_des_prodotto)
		case 7
			dw_report.setitem(ll_j,"cod_l_7",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_7",ls_des_prodotto)
			
	end choose 
		
	dw_report.setitem(ll_j,"flag_mp",ls_flag_materia_prima)
	dw_report.setitem(ll_j,"cod_formula",ls_cod_formula_quan_utilizzo)
	
	setnull(ls_flag_materia_prima)
	setnull(ls_cod_formula_quan_utilizzo)
	setnull(ldd_tempo_lavorazione)
	
	if ls_flag_materia_prima = 'N' then
		
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta_storico
		where  cod_azienda = :s_cs_xx.cod_azienda and	 
		       cod_prodotto_padre = :ls_cod_prodotto_figlio and
				 cod_versione = :fs_cod_versione and
				 prog_storico = :fl_prog_storico;

		if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
			continue
		else
			li_risposta = wf_report( ls_cod_prodotto_figlio, fs_cod_versione, fl_prog_storico, fl_num_livello_cor + 1, fs_errore)
		end if
	else
		
			select cod_prodotto_figlio 
			into   :ls_test_prodotto_f
			from   distinta_storico
			where  cod_azienda = :s_cs_xx.cod_azienda	and	 
			       cod_prodotto_padre = :ls_cod_prodotto_figlio and
					 cod_versione = :fs_cod_versione and
					 prog_storico = :fl_prog_storico;
	
			if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
				continue
			else
				li_risposta = wf_report( ls_cod_prodotto_figlio, fs_cod_versione, fl_prog_storico, fl_num_livello_cor + 1, fs_errore)

			end if
		
	end if

next

loop

destroy(lds_righe_distinta)

	

setpointer(arrow!)


return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_cod_prodotto,ls_cod_versione,ls_errore,ls_des_prodotto
integer li_risposta
long    ll_prog_storico

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)
								 
ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1

ls_cod_versione = s_cs_xx.parametri.parametro_s_2

ll_prog_storico = s_cs_xx.parametri.parametro_d_1

if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then return 

dw_report.reset()

li_risposta = wf_report(ls_cod_prodotto, ls_cod_versione, ll_prog_storico, 1, ls_errore )

if li_risposta < 0 then return

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return
end if

dw_report.Modify("cod_prodotto.text='"+ ls_cod_prodotto + "'")
dw_report.Modify("des_prodotto.text='"+ ls_des_prodotto + "'")
dw_report.Modify("cod_versione.text='"+ ls_cod_versione + "'")



end event

on w_report_distinta_storico.create
int iCurrent
call super::create
this.ddlb_prog_storico=create ddlb_prog_storico
this.st_2=create st_2
this.cb_report=create cb_report
this.cb_stampa=create cb_stampa
this.dw_ricerca=create dw_ricerca
this.dw_report=create dw_report
this.ddlb_versione_origine=create ddlb_versione_origine
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ddlb_prog_storico
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.cb_stampa
this.Control[iCurrent+5]=this.dw_ricerca
this.Control[iCurrent+6]=this.dw_report
this.Control[iCurrent+7]=this.ddlb_versione_origine
this.Control[iCurrent+8]=this.st_1
end on

on w_report_distinta_storico.destroy
call super::destroy
destroy(this.ddlb_prog_storico)
destroy(this.st_2)
destroy(this.cb_report)
destroy(this.cb_stampa)
destroy(this.dw_ricerca)
destroy(this.dw_report)
destroy(this.ddlb_versione_origine)
destroy(this.st_1)
end on

type ddlb_prog_storico from dropdownlistbox within w_report_distinta_storico
integer x = 3406
integer y = 1800
integer width = 686
integer height = 820
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;il_progressivo = index
end event

type st_2 from statictext within w_report_distinta_storico
integer x = 3223
integer y = 1800
integer width = 160
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Data:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_report from commandbutton within w_report_distinta_storico
integer x = 23
integer y = 1900
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_prodotto,ls_cod_versione,ls_errore,ls_des_prodotto

integer li_risposta

long    ll_prog_storico

ls_cod_prodotto = dw_ricerca.getitemstring(dw_ricerca.getrow(),"rs_cod_prodotto")

ls_cod_versione = f_po_selectddlb(ddlb_versione_origine)

ll_prog_storico = il_progressivi[il_progressivo]

if isnull(ls_cod_prodotto) or ls_cod_prodotto ="" then return 

dw_report.reset()

li_risposta = wf_report(ls_cod_prodotto, ls_cod_versione, ll_prog_storico, 1, ls_errore )

if li_risposta < 0 then 
	g_mb.messagebox( "SEP", "Errore durante la creazione del report: " + ls_errore)
	return
end if

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_prodotto = :ls_cod_prodotto;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return
end if

dw_report.Modify("cod_prodotto.text='"+ ls_cod_prodotto + "'")
dw_report.Modify("des_prodotto.text='"+ ls_des_prodotto + "'")
dw_report.Modify("cod_versione.text='"+ ls_cod_versione + "'")

end event

type cb_stampa from commandbutton within w_report_distinta_storico
integer x = 411
integer y = 1900
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long job

job = PrintOpen( ) 
PrintDataWindow(job, dw_report) 
PrintClose(job)
end event

type dw_ricerca from u_dw_search within w_report_distinta_storico
event ue_key pbm_dwnkey
integer x = 23
integer y = 1780
integer width = 2373
integer height = 120
integer taborder = 20
string dataobject = "d_distinta_padri_cerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_distinta_storico
integer x = 23
integer y = 20
integer width = 4069
integer height = 1760
integer taborder = 40
string dataobject = "report_db_storico"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type ddlb_versione_origine from dropdownlistbox within w_report_distinta_storico
integer x = 2674
integer y = 1800
integer width = 503
integer height = 820
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event getfocus;ddlb_versione_origine.reset()
	
f_po_loadddlb(ddlb_versione_origine, &
				  sqlca, &
				  "distinta_padri_storico", &
				  "cod_versione", &
				  "cod_versione", &
				  "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + dw_ricerca.getitemstring(dw_ricerca.getrow(),"rs_cod_prodotto") + "'","")



end event

event selectionchanged;string   ls_cod_versione, ls_cod_prodotto

long     ll_prog_storico, ll_vuoto[], ll_i

datetime ldt_data_ora_storico

ls_cod_versione = f_po_selectddlb(ddlb_versione_origine)

ls_cod_prodotto = dw_ricerca.getitemstring( dw_ricerca.getrow(), "rs_cod_prodotto")

if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
	//messagebox( "SEP", "Selezionare un prodotto prima di continuare!")
	return -1
end if

if isnull(ls_cod_versione) or ls_cod_versione = "" then
	//messagebox( "SEP", "Selezionare una versione prima di continuare!")
	return -1
end if

ddlb_prog_storico.reset()

il_progressivi = ll_vuoto

declare cu_date cursor for
select prog_storico,
       data_ora_storico
from   distinta_padri_storico
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto and
		 cod_versione = :ls_cod_versione
order  by data_ora_storico;
		 
open cu_date;

ll_i = 0

do while 1 = 1 
	fetch cu_date into :ll_prog_storico,
	                   :ldt_data_ora_storico;
							 
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = -1 then exit
	
	ddlb_prog_storico.additem( string( ldt_data_ora_storico, "dd/mm/yyyy hh:mm"))
	
	ll_i = ll_i + 1
	
	il_progressivi[ll_i] = ll_prog_storico
	
loop
	
if ll_i > 0 then
	
	ddlb_prog_storico.selectitem( 1)
	
	il_progressivo = 1
	
end if
	
close cu_date;


end event

type st_1 from statictext within w_report_distinta_storico
integer x = 2400
integer y = 1800
integer width = 251
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Versione:"
boolean focusrectangle = false
end type

