﻿$PBExportHeader$w_report_distinta_varianti.srw
$PBExportComments$Window report distinta base con varianti
forward
global type w_report_distinta_varianti from w_cs_xx_principale
end type
type st_13 from statictext within w_report_distinta_varianti
end type
type em_1 from editmask within w_report_distinta_varianti
end type
type dw_report from uo_cs_xx_dw within w_report_distinta_varianti
end type
type cb_report from commandbutton within w_report_distinta_varianti
end type
type cb_stampa from commandbutton within w_report_distinta_varianti
end type
type dw_ricerca from u_dw_search within w_report_distinta_varianti
end type
end forward

global type w_report_distinta_varianti from w_cs_xx_principale
integer width = 4101
integer height = 2208
string title = "Report Distinta Base"
event ue_header ( )
st_13 st_13
em_1 em_1
dw_report dw_report
cb_report cb_report
cb_stampa cb_stampa
dw_ricerca dw_ricerca
end type
global w_report_distinta_varianti w_report_distinta_varianti

type variables
string is_cod_prodotto, is_cod_versione, is_note, is_tipo_gestione
long   il_anno_registrazione, il_num_registrazione, il_prog_riga
end variables

forward prototypes
public function integer wf_report (string fs_cod_prodotto, string fs_cod_versione, long fl_num_livello_cor, ref string fs_errore)
end prototypes

event ue_header();long ld_y, ld_height

ld_y = LONG(dw_report.Describe("note_prodotto.y"))
	
ld_height = LONG(dw_report.Describe("note_prodotto.Height"))
	
ld_height = ld_height + ld_y + 27
	
//dw_report.Modify("Datawindow.Header.1.Height=" + string(ld_height) )
STRING LS_RET
//LS_RET = dw_report.Modify("Datawindow.Header.1.Height=" + string(5000) )
LS_RET = dw_report.Modify("Datawindow.Header.1.Height=" + string(ld_height) )

ld_height = LONG(dw_report.Describe("Datawindow.Header.1.Height"))

end event

public function integer wf_report (string fs_cod_prodotto, string fs_cod_versione, long fl_num_livello_cor, ref string fs_errore);string  ls_errore, ls_test_prodotto_f, ls_des_prodotto,ls_flag_materia_prima,ls_cod_misura_mag,&
		  ls_cod_prodotto_figlio,ls_cod_prodotto_padre,ls_des_estesa_prodotto,ls_cod_formula_quan_utilizzo, &
		  ls_cod_gruppo_variante,ls_cod_reparto,ls_cod_lavorazione, ls_note, ls_cod_versione_figlio, &
		  ls_flag_note_prodotto, ls_flag_ignora_mp, ls_flag_des_fasi,ls_ordinamento
		  
integer li_num_priorita,li_risposta,li_filenum
long    ll_i,ll_num_figli,ll_num_sequenza,ll_t,ll_num_righe,ll_j, ll_livello
dec{4}  ldd_quan_utilizzo,ldd_tempo_lavorazione

setpointer(hourglass!)

ls_flag_note_prodotto = dw_ricerca.getitemstring(dw_ricerca.getrow(),"flag_stampa_note")
ls_flag_ignora_mp = dw_ricerca.getitemstring(dw_ricerca.getrow(),"flag_ignora_flag_mp")
ls_flag_des_fasi = dw_ricerca.getitemstring(dw_ricerca.getrow(),"flag_des_fasi")
ls_ordinamento = dw_ricerca.getitemstring(dw_ricerca.getrow(),"ordinamento")

ll_livello = long(em_1.text)
if isnull(ll_livello) or ll_livello<0 then ll_livello=0

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:fs_cod_prodotto;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)

choose case ls_ordinamento
	case "C"
		lds_righe_distinta.setsort("cod_prodotto_figlio ASC, num_sequenza ASC")
		lds_righe_distinta.sort()
	case "D"		
		lds_righe_distinta.setsort("anag_prodotti_des_prodotto ASC, num_sequenza ASC")
		lds_righe_distinta.sort()
	case else
		// è già ordinato per num_sequenza.....non serve fare nulla
end choose 

for ll_num_figli = 1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ldd_quan_utilizzo      = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	ll_num_sequenza        = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	ls_cod_prodotto_padre  = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_padre")
	ls_flag_materia_prima  = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")

   select des_prodotto,
			 cod_misura_mag
	into   :ls_des_prodotto,
			 :ls_cod_misura_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto = :ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ls_des_prodotto=trim(ls_des_prodotto)
	
		// *** note prodotto
		
		if ls_flag_note_prodotto = "S" then
			
			declare cu_note cursor for
			  SELECT   nota_prodotto  
			  FROM     anag_prodotti_note  
			  WHERE    cod_azienda = :s_cs_xx.cod_azienda AND  
						  cod_prodotto = :ls_cod_prodotto_figlio
			  ORDER BY cod_prodotto ASC;
			  
			open cu_note;
			
			do while true
				
				fetch cu_note into :ls_note;
				
				if sqlca.sqlcode <> 0 then exit
				
				if not isnull(ls_note) and ls_note <> "" then 
					
					if isnull(is_note) then is_note = ""
					
					if is_note <> "" then
						is_note = is_note + "~r~n"
					end if
					
					is_note = is_note + ls_note
					
				end if
				
			loop
			
			close cu_note;
			
		end if		
		
		// ***	

	if isnull(ls_cod_misura_mag) then ls_cod_misura_mag = ""

	select cod_formula_quan_utilizzo
	into   :ls_cod_formula_quan_utilizzo
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_prodotto_padre = :ls_cod_prodotto_padre	and    
			 num_sequenza = :ll_num_sequenza	and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and    
			 cod_versione = :fs_cod_versione and
			 cod_versione_figlio = :ls_cod_versione_figlio;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	select cod_gruppo_variante
	into   :ls_cod_gruppo_variante
	from   distinta_gruppi_varianti
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_prodotto_padre = :ls_cod_prodotto_padre	and    
			 num_sequenza = :ll_num_sequenza	and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and    
			 cod_versione = :fs_cod_versione and
			 cod_versione_figlio = :ls_cod_versione_figlio;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Attenzione! Potrebbe esserci più di un gruppo variante collegato ad un ramo di distinta. E' comunque possibile continuare a generare il report, il gruppo variante visualizzato sarà il primo dell'elenco dei gruppi varianti associati al ramo di distinta. Errore sul DB: " + sqlca.sqlerrtext,Information!)
	end if

	select cod_reparto,
			 cod_lavorazione,
			 tempo_lavorazione,
			 des_estesa_prodotto
	into   :ls_cod_reparto,
			 :ls_cod_lavorazione,
			 :ldd_tempo_lavorazione,
			 :ls_des_estesa_prodotto
	from   tes_fasi_lavorazione
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
	       cod_prodotto = :ls_cod_prodotto_figlio and
			 cod_versione = :ls_cod_versione_figlio;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","E' probabile che ci siano più fasi associate allo stesso semilavorato (controllare il messaggio d'errore sul DB). Tuttavia è possibile proseguire con l'elaborazione del report. Errore sul DB: " + sqlca.sqlerrtext,information!)
	end if
			 
	ll_j = dw_report.insertrow(0)
	
	dw_report.setitem( ll_j, "tipo_riga", 3)		
	
	dw_report.setitem( ll_j, "num_livello", fl_num_livello_cor)

	choose case fl_num_livello_cor
		case 1
			dw_report.setitem(ll_j,"cod_l_1",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_1",ls_des_prodotto)
		case 2
			dw_report.setitem(ll_j,"cod_l_2",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_2",ls_des_prodotto)
		case 3
			dw_report.setitem(ll_j,"cod_l_3",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_3",ls_des_prodotto)
		case 4
			dw_report.setitem(ll_j,"cod_l_4",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_4",ls_des_prodotto)
		case 5
			dw_report.setitem(ll_j,"cod_l_5",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_5",ls_des_prodotto)
		case 6
			dw_report.setitem(ll_j,"cod_l_6",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_6",ls_des_prodotto)
		case 7
			dw_report.setitem(ll_j,"cod_l_7",ls_cod_prodotto_figlio + "    " + string(ldd_quan_utilizzo) + " " + ls_cod_misura_mag)
			dw_report.setitem(ll_j,"des_l_7",ls_des_prodotto)
			
	end choose 
		
	dw_report.setitem( ll_j, "flag_mp", ls_flag_materia_prima)
	dw_report.setitem( ll_j, "cod_gruppo_variante", ls_cod_gruppo_variante)
	dw_report.setitem( ll_j, "cod_formula", ls_cod_formula_quan_utilizzo)
	dw_report.setitem( ll_j, "cod_reparto", ls_cod_reparto)
	dw_report.setitem( ll_j, "cod_lavorazione", ls_cod_lavorazione)
	dw_report.setitem( ll_j, "tempo_lavorazione", ldd_tempo_lavorazione)
	
	if ls_flag_des_fasi= "S" then
		// ENME 21/10/2006  aggiunto per eurocablaggi la stampa della fase di lavorazione.
		if len(trim(ls_des_estesa_prodotto)) > 0 and not isnull(ls_des_estesa_prodotto) then
			ll_j = dw_report.insertrow(0)
			dw_report.setitem( ll_j, "tipo_riga", 4)		
			dw_report.setitem( ll_j, "des_estesa_prodotto", ls_des_estesa_prodotto)
		end if
	end if
	
	if ls_flag_materia_prima = 'N' then
		
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda	and	 
		       cod_prodotto_padre = :ls_cod_prodotto_figlio and
				 cod_versione = :ls_cod_versione_figlio;

		if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
			continue
		else
			
			//Donato 14/03/2012 Controllo sul livello di espansione
			//inoltre se il livello impostato è nullo o ZERO ignora il controllo livelli
			if (fl_num_livello_cor + 1) > ll_livello and ll_livello > 0 then continue
			//-------------------------------------------------------------------------
			
			li_risposta = wf_report( ls_cod_prodotto_figlio, ls_cod_versione_figlio, fl_num_livello_cor + 1, fs_errore)
		end if
		
	else
		
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda	and	 
		       cod_prodotto_padre = :ls_cod_prodotto_figlio and
				 cod_versione = :ls_cod_versione_figlio;
	
		if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
			continue
		else
						
			// *** 17/11/2004 decido se stampare o no gli eventuali componenti di un prodotto per il quale si è impostato il flag MP = 's'
			// *** storico distinta base unifast rev.7 
			
			if ls_flag_ignora_mp = "S" then continue
			
			//Donato 14/03/2012 Controllo sul livello di espansione
			//inoltre se il livello impostato è nullo o ZERO ignora il controllo livelli
			if (fl_num_livello_cor + 1) > ll_livello and ll_livello > 0 then continue
			//-------------------------------------------------------------------------
			
			li_risposta = wf_report( ls_cod_prodotto_figlio, ls_cod_versione_figlio, fl_num_livello_cor + 1, fs_errore)
			
		end if
	
	end if
	
	setnull(ls_flag_materia_prima)
	setnull(ls_cod_gruppo_variante)
	setnull(ls_cod_formula_quan_utilizzo)
	setnull(ls_cod_reparto)
	setnull(ls_cod_lavorazione)
	setnull(ldd_tempo_lavorazione)	
	setnull(ls_des_estesa_prodotto)

next

destroy(lds_righe_distinta)

	

setpointer(arrow!)


return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

//if isvalid(i_openparm) then
//	
//	dw_report.set_dw_options(sqlca, &
//									 i_openparm, &
//									 c_nomodify + &
//									 c_nodelete + &
//									 c_newonopen + &
//									 c_disableCC, &
//									 c_nohighlightselected + &
//									 c_nocursorrowpointer +&
//									 c_nocursorrowfocusrect )
//	
//	is_cod_prodotto = dw_report.i_parentdw.getitemstring(dw_report.i_parentdw.getrow(), "cod_prodotto")
//	is_cod_versione = dw_report.i_parentdw.getitemstring(dw_report.i_parentdw.getrow(), "cod_versione")
//	
//else
	dw_report.set_dw_options(sqlca, &
									 pcca.null_object, &
									 c_nomodify + &
									 c_nodelete + &
									 c_newonopen + &
									 c_disableCC, &
									 c_nohighlightselected + &
									 c_nocursorrowpointer +&
									 c_nocursorrowfocusrect )
//end if
//                         c_noresizedw + &


is_cod_prodotto = s_cs_xx.parametri.parametro_s_1
is_cod_versione = s_cs_xx.parametri.parametro_s_2
is_tipo_gestione = s_cs_xx.parametri.parametro_s_3

il_anno_registrazione  = s_cs_xx.parametri.parametro_i_1
il_num_registrazione = s_cs_xx.parametri.parametro_d_1
il_prog_riga = s_cs_xx.parametri.parametro_d_2
								 
dw_report.postevent("ue_report")



end event

on w_report_distinta_varianti.create
int iCurrent
call super::create
this.st_13=create st_13
this.em_1=create em_1
this.dw_report=create dw_report
this.cb_report=create cb_report
this.cb_stampa=create cb_stampa
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_13
this.Control[iCurrent+2]=this.em_1
this.Control[iCurrent+3]=this.dw_report
this.Control[iCurrent+4]=this.cb_report
this.Control[iCurrent+5]=this.cb_stampa
this.Control[iCurrent+6]=this.dw_ricerca
end on

on w_report_distinta_varianti.destroy
call super::destroy
destroy(this.st_13)
destroy(this.em_1)
destroy(this.dw_report)
destroy(this.cb_report)
destroy(this.cb_stampa)
destroy(this.dw_ricerca)
end on

type st_13 from statictext within w_report_distinta_varianti
integer x = 23
integer y = 176
integer width = 457
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Espandi a livello:"
boolean focusrectangle = false
end type

type em_1 from editmask within w_report_distinta_varianti
integer x = 503
integer y = 176
integer width = 389
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
double increment = 1
string minmax = "0~~99999"
end type

type dw_report from uo_cs_xx_dw within w_report_distinta_varianti
event ue_report ( )
integer x = 5
integer y = 272
integer width = 4027
integer height = 1808
integer taborder = 70
string dataobject = "d_report_db_varianti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_report();string ls_cod_prodotto, ls_cod_versione, ls_errore, ls_des_prodotto, ls_note_globali, ls_note, ls_flag_note_prodotto, &
       ls_flag_ignora_mp,ls_flag_des_fasi

integer li_risposta

long    ll_riga, ll_max_livelli

decimal ldd_quan_ordinata

uo_imposta_tv_varianti uo_carica_report

ls_cod_prodotto = is_cod_prodotto
ls_cod_versione = is_cod_versione

ls_flag_note_prodotto = dw_ricerca.getitemstring(dw_ricerca.getrow(),"flag_stampa_note")
ls_flag_ignora_mp = dw_ricerca.getitemstring(dw_ricerca.getrow(),"flag_ignora_flag_mp")
ls_flag_des_fasi = dw_ricerca.getitemstring(dw_ricerca.getrow(),"flag_des_fasi")

ll_max_livelli = long(em_1.text)
if isnull(ll_max_livelli) or ll_max_livelli<0 then ll_max_livelli=0


if isnull(ls_cod_prodotto) or ls_cod_prodotto ="" then return 

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_prodotto = :ls_cod_prodotto;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return
end if

dw_report.reset()

dw_report.setredraw(false)

dw_report.Modify("cod_prodotto.text='" + ls_cod_prodotto + "'")

dw_report.Modify("des_prodotto.text='" + ls_des_prodotto + "'")

dw_report.Modify("cod_versione.text='" + ls_cod_versione + "'")

dw_report.Modify("ordine_t.text='" + "Ordine " + string(il_anno_registrazione) + " / " +  string(il_num_registrazione) + " / " +  string(il_prog_riga) + "'")


//per visualizzare la corretta quantità utilizzo in base alla quantità ordinata
select quan_ordine
into :ldd_quan_ordinata
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:il_anno_registrazione and
			num_registrazione=:il_num_registrazione and
			prog_riga_ord_ven=:il_prog_riga;
			
if isnull(ldd_quan_ordinata) or ldd_quan_ordinata=0 then ldd_quan_ordinata=1
//-----------------------------------------------------------------------

if ls_flag_note_prodotto = "S" then
	
	ls_note_globali = ""
	
	declare cu_note cursor for
	  SELECT   nota_prodotto  
	  FROM     anag_prodotti_note  
	  WHERE    cod_azienda = :s_cs_xx.cod_azienda AND  
				  cod_prodotto = :ls_cod_prodotto
	  ORDER BY cod_prodotto ASC;
	  
	open cu_note;
	
	do while 1 = 1
		fetch cu_note into :ls_note;
		if sqlca.sqlcode <> 0 then exit
		if not isnull(ls_note) and ls_note <> "" then 
			if ls_note_globali <> "" then
				ls_note_globali = ls_note_globali + "~r~n"
			end if
			ls_note_globali = ls_note_globali + ls_note
		end if
		
	loop
	
	close cu_note;
	
	ll_riga = dw_report.insertrow(0)	
	
	dw_report.setitem( ll_riga, "tipo_riga", 1)
	
	dw_report.Modify("note_prodotto.text='" + ls_note_globali + "'")
	
end if

ll_riga = dw_report.insertrow(0)	
	
dw_report.setitem( ll_riga, "tipo_riga", 2)

uo_carica_report = create uo_imposta_tv_varianti

uo_carica_report.is_tipo_gestione = is_tipo_gestione
uo_carica_report.il_anno_registrazione = il_anno_registrazione
uo_carica_report.il_num_registrazione = il_num_registrazione
uo_carica_report.il_prog_riga_registrazione = il_prog_riga
uo_carica_report.id_qta_ordine = ldd_quan_ordinata

if dw_ricerca.getitemstring(dw_ricerca.getrow(),"flag_vis_mp") = "N" then
	uo_carica_report.ib_salta_materie_prime=true
else
	uo_carica_report.ib_salta_materie_prime=false
end if

//Donato 14/03/2012 Controllo sul livello di espansione
//se il livello impostato è nullo o ZERO ignora il controllo livelli, cioè tutto come prima
uo_carica_report.il_livello_max = ll_max_livelli
//------------------------------------------------------------------
uo_carica_report.uof_imposta_report(ref dw_report, is_cod_prodotto, is_cod_versione, 1, ref ls_errore)
	
if is_note <> "" then
	
	is_note = ls_note_globali + "~r~n" + is_note
	
	dw_report.modify("note_prodotto.text='" + is_note + "'")
	
end if

dw_report.setredraw(true)

destroy uo_carica_report

commit;
end event

type cb_report from commandbutton within w_report_distinta_varianti
integer x = 3269
integer y = 176
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;dw_report.triggerevent( "ue_report")


end event

type cb_stampa from commandbutton within w_report_distinta_varianti
integer x = 3657
integer y = 176
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;dw_report.print()
end event

type dw_ricerca from u_dw_search within w_report_distinta_varianti
event ue_key pbm_dwnkey
integer x = 9
integer y = 20
integer width = 4069
integer height = 160
integer taborder = 20
string dataobject = "d_report_db_selezione_varianti"
boolean border = false
end type

