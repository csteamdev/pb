﻿$PBExportHeader$w_sel_reparto_data.srw
forward
global type w_sel_reparto_data from window
end type
type dw_selezione from datawindow within w_sel_reparto_data
end type
end forward

global type w_sel_reparto_data from window
integer width = 2523
integer height = 744
boolean titlebar = true
string title = "Selezione Data Fabbisogno, Operaio e Reparto"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
dw_selezione dw_selezione
end type
global w_sel_reparto_data w_sel_reparto_data

on w_sel_reparto_data.create
this.dw_selezione=create dw_selezione
this.Control[]={this.dw_selezione}
end on

on w_sel_reparto_data.destroy
destroy(this.dw_selezione)
end on

event open;s_cs_xx_parametri				lstr_parametri
string								ls_cod_operaio, ls_where_deposito, ls_cod_deposito, ls_errore, ls_cod_reparto
datetime							ldt_data_fabbisogno

//impostazione per data fabbisogno, operaio e reparto

lstr_parametri = message.powerobjectparm

dw_selezione.insertrow(0)

//--------------------------------------------------------------------------------------------------------------------------------------------------------
//nella drop visualizza solo i reparti non bloccati e del deposito dell'utente corrente
ls_where_deposito = ""

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	guo_functions.uof_get_stabilimento_utente(s_cs_xx.cod_utente, ls_cod_deposito, ls_errore)

	if ls_cod_deposito<>"" and not isnull(ls_cod_deposito) then ls_where_deposito = " and cod_deposito='"+ls_cod_deposito+"'"
end if

f_PO_LoadDDDW_DW(dw_selezione, "cod_reparto", sqlca, &
							  "anag_reparti","cod_reparto","des_reparto",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N'" + ls_where_deposito)
//--------------------------------------------------------------------------------------------------------------------------------------------------------

try
	ls_cod_operaio = lstr_parametri.parametro_s_1
catch (runtimeerror err1)
end try

try
	ls_cod_reparto = lstr_parametri.parametro_s_2
catch (runtimeerror err2)
end try

try
	ldt_data_fabbisogno = lstr_parametri.parametro_data_1
catch (runtimeerror err3)
end try

if ls_cod_operaio="" or isnull(ls_cod_operaio) then
	//prova magari c'è un operaio dell'utente collegato (ma solo se non sei CS_SYSTEM)
	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		ls_cod_operaio = guo_functions.uof_get_operaio_utente(s_cs_xx.cod_utente)
	end if
end if

//imposta campi
if ls_cod_operaio<>"" and not isnull(ls_cod_operaio) then dw_selezione.setitem(1, "cod_operaio", ls_cod_operaio)
if ls_cod_reparto<>"" and not isnull(ls_cod_reparto) then dw_selezione.setitem(1, "cod_reparto", ls_cod_reparto)
if not isnull(ldt_data_fabbisogno) and year(date(ldt_data_fabbisogno))>1950 then dw_selezione.setitem(1, "data_fabbisogno", ldt_data_fabbisogno)




end event

type dw_selezione from datawindow within w_sel_reparto_data
integer x = 27
integer y = 32
integer width = 2441
integer height = 588
integer taborder = 10
string title = "none"
string dataobject = "d_sel_reparto_data"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;datawindow				ldw_data
any						la_any[]
string						ls_cod_operaio, ls_cod_reparto
datetime					ldt_data_fabbisogno
s_cs_xx_parametri		lstr_parametri



choose case dwo.name
	case "b_operaio"
		setnull(ldw_data)
		guo_ricerca.uof_set_response()
		guo_ricerca.uof_ricerca_operaio(dw_selezione,"cod_operaio")
		
		guo_ricerca.uof_get_results( la_any)
		if(upperbound(la_any)>0) then
			ls_cod_operaio=la_any[1]
			dw_selezione.setitem(row, "cod_operaio", ls_cod_operaio)
		else 
			return
		end if
		
	case "b_conferma"
		
		dw_selezione.accepttext()
		
		ls_cod_operaio = dw_selezione.getitemstring(row, "cod_operaio")
		if ls_cod_operaio="" or isnull(ls_cod_operaio) then
			g_mb.warning("Indicare l'operatore richiedente!")
			return
		end if
		
		ls_cod_reparto = dw_selezione.getitemstring(row, "cod_reparto")
		if ls_cod_reparto="" or isnull(ls_cod_reparto) then
			g_mb.warning("Indicare il reparto richiedente!")
			return
		end if
		
		ldt_data_fabbisogno = dw_selezione.getitemdatetime(row, "data_fabbisogno")
		if isnull(ldt_data_fabbisogno) or year(date(ldt_data_fabbisogno))<1950 then
			if not g_mb.confirm("Non hai indicato la data del fabbisogno del materiale. Vuoi continuare lo stesso?") then return

			setnull(ldt_data_fabbisogno)

		end if
		
		lstr_parametri.parametro_b_1 = true
		lstr_parametri.parametro_s_1_a[1] = ls_cod_operaio
		lstr_parametri.parametro_s_1_a[2] = ls_cod_reparto
		lstr_parametri.parametro_data_1 = ldt_data_fabbisogno
		closewithreturn(parent, lstr_parametri)
	
	
	case "b_annulla"
		//if g_mb.confirm("Annullare l'operazione?", 1) then
			lstr_parametri.parametro_b_1 = false
			closewithreturn(parent, lstr_parametri)
		//end if
		
		
		
end choose
end event

