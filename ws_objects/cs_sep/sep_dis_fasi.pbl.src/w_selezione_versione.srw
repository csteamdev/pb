﻿$PBExportHeader$w_selezione_versione.srw
$PBExportComments$Window che consente di selezionare una versione già esistente
forward
global type w_selezione_versione from w_cs_xx_risposta
end type
type dw_selezione_versione from uo_cs_xx_dw within w_selezione_versione
end type
type cb_annulla from commandbutton within w_selezione_versione
end type
type cb_ok from commandbutton within w_selezione_versione
end type
end forward

global type w_selezione_versione from w_cs_xx_risposta
int Width=951
int Height=1085
boolean TitleBar=true
string Title="Selezione Versione Esistente"
dw_selezione_versione dw_selezione_versione
cb_annulla cb_annulla
cb_ok cb_ok
end type
global w_selezione_versione w_selezione_versione

on w_selezione_versione.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_selezione_versione=create dw_selezione_versione
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_selezione_versione
this.Control[iCurrent+2]=cb_annulla
this.Control[iCurrent+3]=cb_ok
end on

on w_selezione_versione.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_selezione_versione)
destroy(this.cb_annulla)
destroy(this.cb_ok)
end on

event pc_setwindow;call super::pc_setwindow;dw_selezione_versione.set_dw_options(sqlca, &
												 pcca.null_object, &
												 c_multiselect + &
												 c_nonew + &
												 c_nomodify + &
												 c_nodelete + &
												 c_disablecc + &
												 c_disableccinsert , &
												 c_default)
end event

type dw_selezione_versione from uo_cs_xx_dw within w_selezione_versione
int X=23
int Y=21
int Width=869
int Height=841
int TabOrder=10
string DataObject="d_selezione_versione"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto,ls_cod_versione
long li_riga

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1

declare righe_db_1 cursor for
select  cod_versione
from    distinta 
where   cod_azienda=:s_cs_xx.cod_azienda 
and     cod_prodotto_padre =:ls_cod_prodotto
group by cod_versione;

open righe_db_1;

do while 1 = 1
	fetch righe_db_1
	into  :ls_cod_versione;

	if (sqlca.sqlcode = 100) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Sep","Errore nel DB: " + SQLCA.SQLErrText)
		close righe_db_1;
		return -1
	end if
	li_riga = dw_selezione_versione.insertrow(0)
	dw_selezione_versione.setitem(li_riga,"cod_versione",ls_cod_versione)

loop
close righe_db_1;

dw_selezione_versione.resetupdate()
end event

type cb_annulla from commandbutton within w_selezione_versione
int X=138
int Y=881
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "-1"
close(parent)
end event

type cb_ok from commandbutton within w_selezione_versione
int X=526
int Y=881
int Width=366
int Height=81
int TabOrder=3
boolean BringToTop=true
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if dw_selezione_versione.rowcount() > 0 then
	s_cs_xx.parametri.parametro_s_1 = dw_selezione_versione.getitemstring(dw_selezione_versione.getrow(),"cod_versione")
else
	s_cs_xx.parametri.parametro_s_1 = "-1"
end if
close(parent)
end event

