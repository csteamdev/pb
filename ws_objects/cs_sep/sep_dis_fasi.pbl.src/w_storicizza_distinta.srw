﻿$PBExportHeader$w_storicizza_distinta.srw
$PBExportComments$Window per la storicizzazione della distinta base
forward
global type w_storicizza_distinta from w_cs_xx_principale
end type
type mle_note from multilineedit within w_storicizza_distinta
end type
type cb_ok from commandbutton within w_storicizza_distinta
end type
type cb_annulla from commandbutton within w_storicizza_distinta
end type
type st_3 from statictext within w_storicizza_distinta
end type
type em_ora from editmask within w_storicizza_distinta
end type
type st_4 from statictext within w_storicizza_distinta
end type
type st_1 from statictext within w_storicizza_distinta
end type
type gb_destinazione from groupbox within w_storicizza_distinta
end type
type em_data from editmask within w_storicizza_distinta
end type
end forward

global type w_storicizza_distinta from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2208
integer height = 652
string title = "Storicizzazione Distinta Base"
mle_note mle_note
cb_ok cb_ok
cb_annulla cb_annulla
st_3 st_3
em_ora em_ora
st_4 st_4
st_1 st_1
gb_destinazione gb_destinazione
em_data em_data
end type
global w_storicizza_distinta w_storicizza_distinta

type variables
string is_cod_prodotto, is_cod_versione
end variables

on w_storicizza_distinta.create
int iCurrent
call super::create
this.mle_note=create mle_note
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.st_3=create st_3
this.em_ora=create em_ora
this.st_4=create st_4
this.st_1=create st_1
this.gb_destinazione=create gb_destinazione
this.em_data=create em_data
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.mle_note
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.st_3
this.Control[iCurrent+5]=this.em_ora
this.Control[iCurrent+6]=this.st_4
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.gb_destinazione
this.Control[iCurrent+9]=this.em_data
end on

on w_storicizza_distinta.destroy
call super::destroy
destroy(this.mle_note)
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.st_3)
destroy(this.em_ora)
destroy(this.st_4)
destroy(this.st_1)
destroy(this.gb_destinazione)
destroy(this.em_data)
end on

event pc_setwindow;call super::pc_setwindow;em_data.text = string( date( today()), "dd/mm/yyyy")

em_ora.text = string( Now(), "hh:mm")

is_cod_prodotto = s_cs_xx.parametri.parametro_s_1

is_cod_versione = s_cs_xx.parametri.parametro_s_2

setnull(s_cs_xx.parametri.parametro_s_1)

setnull(s_cs_xx.parametri.parametro_s_2)

end event

type mle_note from multilineedit within w_storicizza_distinta
integer x = 274
integer y = 200
integer width = 1851
integer height = 200
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = true
boolean autovscroll = true
integer limit = 100
borderstyle borderstyle = stylelowered!
end type

type cb_ok from commandbutton within w_storicizza_distinta
integer x = 1394
integer y = 460
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;datetime ldt_data_ora_storico, ldt_data_creazione, ldt_data_approvazione, ldt_data_blocco

string   ls_note, ls_creato_da, ls_approvato_da, ls_des_versione, ls_flag_predefinita, ls_flag_blocco, ls_flag_esplodi_in_doc, &
         ls_flag_non_calcolare_dt, ls_flag_chiusura_automatica, ls_errore
			
long     ll_prog_storico, ll_ret

integer  li_risposta

ll_ret = g_mb.messagebox( "SEP", "Continuare con la Storicizzazione della Distinta selezionata?", Exclamation!, OKCancel!, 2)

if ll_ret <> 1 then 
	
	g_mb.messagebox( "SEP", "Operazione annullata!")
	
	return 0
	
end if

ldt_data_ora_storico = datetime( date(em_data.text), time(em_ora.text))

ls_note = mle_note.text

if ls_note = "" then setnull(ls_note)

if isnull(is_cod_prodotto) or is_cod_prodotto = "" then
	
	g_mb.messagebox("Sep","Manca il codice del prodotto della distinta!",stopsign!)
	
	return
	
end if

if isnull(is_cod_versione) or is_cod_versione = "" then
	
	g_mb.messagebox("Sep","Manca la versione del prodotto della distinta!",stopsign!)
	
	return
	
end if

SELECT distinta_padri.creato_da,   
       distinta_padri.approvato_da,   
       distinta_padri.des_versione,   
       distinta_padri.data_creazione,   
       distinta_padri.data_approvazione,   
       distinta_padri.flag_predefinita,   
       distinta_padri.flag_blocco,   
       distinta_padri.data_blocco,   
       distinta_padri.flag_esplodi_in_doc,   
       distinta_padri.flag_non_calcolare_dt,   
       distinta_padri.flag_chiusura_automatica  
INTO   :ls_creato_da,   
       :ls_approvato_da,   
       :ls_des_versione,   
       :ldt_data_creazione,   
       :ldt_data_approvazione,   
       :ls_flag_predefinita,   
       :ls_flag_blocco,   
       :ldt_data_blocco,   
       :ls_flag_esplodi_in_doc,   
       :ls_flag_non_calcolare_dt,   
       :ls_flag_chiusura_automatica  
FROM   distinta_padri  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :is_cod_prodotto and
		 cod_versione = :is_cod_versione;

if sqlca.sqlcode < 0 then	
	g_mb.messagebox( "Sep", "Errore durante la select dalla distinta padri: " + sqlca.sqlerrtext, stopsign!)		
	return	
end if

if sqlca.sqlcode = 100 then	
	g_mb.messagebox( "SEP", "Attenzione: nessun risultato corrisponde a questo prodotto con questa versione.", stopsign!)	
	return
end if

select MAX(prog_storico)
into   :ll_prog_storico
from   distinta_padri_storico
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :is_cod_prodotto and
		 cod_versione = :is_cod_versione;
		 
if sqlca.sqlcode < 0 then	
	g_mb.messagebox( "Sep", "Errore durante lettura chiave dallo storico: " + sqlca.sqlerrtext, stopsign!)		
	return	
end if

if isnull(ll_prog_storico) then ll_prog_storico = 0

ll_prog_storico = ll_prog_storico + 1

INSERT INTO distinta_padri_storico  
          ( cod_azienda,   
            cod_prodotto,   
            cod_versione,   
            prog_storico,   
            data_ora_storico,   
            creato_da,   
            approvato_da,   
            des_versione,   
            data_creazione,   
            data_approvazione,   
            flag_predefinita,   
            flag_blocco,   
            data_blocco,   
            flag_esplodi_in_doc,   
            flag_non_calcolare_dt,   
            flag_chiusura_automatica,   
            note )  
  VALUES (  :s_cs_xx.cod_azienda,   
            :is_cod_prodotto,   
            :is_cod_versione,   
            :ll_prog_storico,   
            :ldt_data_ora_storico,   
            :ls_creato_da,   
            :ls_approvato_da,   
            :ls_des_versione,   
            :ldt_data_creazione,   
            :ldt_data_approvazione,   
            :ls_flag_predefinita,   
            :ls_flag_blocco,   
            :ldt_data_blocco,   
            :ls_flag_esplodi_in_doc,   
            :ls_flag_non_calcolare_dt,   
            :ls_flag_chiusura_automatica,   
            :ls_note )  ;

if sqlca.sqlcode < 0 then	
	g_mb.messagebox( "Sep", "Errore durante l'inserimento testata storico: " + sqlca.sqlerrtext, stopsign!)		
	rollback;
	return	
end if


li_risposta = f_storicizza_distinta(is_cod_prodotto, & 
											   is_cod_versione, & 
											   1, &
												ll_prog_storico, &
											   ls_errore)

if li_risposta <> 0 then
	g_mb.messagebox( "SEP", ls_errore, stopsign!)
	rollback;
	return
end if

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep",ls_errore,stopsign!)
	rollback;
	return
end if

commit;

g_mb.messagebox( "Sep", "Storicizzazione della distinta base avvenuta con successo!", information!)

end event

type cb_annulla from commandbutton within w_storicizza_distinta
integer x = 1783
integer y = 460
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;
close (parent)
end event

type st_3 from statictext within w_storicizza_distinta
integer x = 46
integer y = 180
integer width = 206
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Note:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_ora from editmask within w_storicizza_distinta
integer x = 846
integer y = 100
integer width = 183
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = timemask!
string mask = "hh:mm"
end type

type st_4 from statictext within w_storicizza_distinta
integer x = 69
integer y = 100
integer width = 183
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Data:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_storicizza_distinta
integer x = 709
integer y = 100
integer width = 114
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ora:"
alignment alignment = right!
boolean focusrectangle = false
end type

type gb_destinazione from groupbox within w_storicizza_distinta
integer x = 23
integer y = 20
integer width = 2126
integer height = 420
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Informazioni Storicizzazione"
end type

type em_data from editmask within w_storicizza_distinta
integer x = 274
integer y = 100
integer width = 320
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

