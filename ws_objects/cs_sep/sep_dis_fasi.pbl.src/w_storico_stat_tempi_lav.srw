﻿$PBExportHeader$w_storico_stat_tempi_lav.srw
$PBExportComments$Window storico stat tempi lavorazione
forward
global type w_storico_stat_tempi_lav from w_cs_xx_risposta
end type
type cb_elimina from commandbutton within w_storico_stat_tempi_lav
end type
type cbx_attrezzaggio_com from checkbox within w_storico_stat_tempi_lav
end type
type cbx_risorsa_umana from checkbox within w_storico_stat_tempi_lav
end type
type cbx_attrezzaggio from checkbox within w_storico_stat_tempi_lav
end type
type cbx_lavorazione from checkbox within w_storico_stat_tempi_lav
end type
type cb_aggiorna from commandbutton within w_storico_stat_tempi_lav
end type
type dw_storico_stat_fasi_lav_det from uo_cs_xx_dw within w_storico_stat_tempi_lav
end type
type dw_storico_stat_fasi_lav_lista from uo_cs_xx_dw within w_storico_stat_tempi_lav
end type
end forward

global type w_storico_stat_tempi_lav from w_cs_xx_risposta
integer width = 1865
integer height = 1640
string title = "Storico Statistiche Tempi Lavorazione"
cb_elimina cb_elimina
cbx_attrezzaggio_com cbx_attrezzaggio_com
cbx_risorsa_umana cbx_risorsa_umana
cbx_attrezzaggio cbx_attrezzaggio
cbx_lavorazione cbx_lavorazione
cb_aggiorna cb_aggiorna
dw_storico_stat_fasi_lav_det dw_storico_stat_fasi_lav_det
dw_storico_stat_fasi_lav_lista dw_storico_stat_fasi_lav_lista
end type
global w_storico_stat_tempi_lav w_storico_stat_tempi_lav

on w_storico_stat_tempi_lav.create
int iCurrent
call super::create
this.cb_elimina=create cb_elimina
this.cbx_attrezzaggio_com=create cbx_attrezzaggio_com
this.cbx_risorsa_umana=create cbx_risorsa_umana
this.cbx_attrezzaggio=create cbx_attrezzaggio
this.cbx_lavorazione=create cbx_lavorazione
this.cb_aggiorna=create cb_aggiorna
this.dw_storico_stat_fasi_lav_det=create dw_storico_stat_fasi_lav_det
this.dw_storico_stat_fasi_lav_lista=create dw_storico_stat_fasi_lav_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_elimina
this.Control[iCurrent+2]=this.cbx_attrezzaggio_com
this.Control[iCurrent+3]=this.cbx_risorsa_umana
this.Control[iCurrent+4]=this.cbx_attrezzaggio
this.Control[iCurrent+5]=this.cbx_lavorazione
this.Control[iCurrent+6]=this.cb_aggiorna
this.Control[iCurrent+7]=this.dw_storico_stat_fasi_lav_det
this.Control[iCurrent+8]=this.dw_storico_stat_fasi_lav_lista
end on

on w_storico_stat_tempi_lav.destroy
call super::destroy
destroy(this.cb_elimina)
destroy(this.cbx_attrezzaggio_com)
destroy(this.cbx_risorsa_umana)
destroy(this.cbx_attrezzaggio)
destroy(this.cbx_lavorazione)
destroy(this.cb_aggiorna)
destroy(this.dw_storico_stat_fasi_lav_det)
destroy(this.dw_storico_stat_fasi_lav_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_storico_stat_fasi_lav_lista.set_dw_options(sqlca, &
                              					 pcca.null_object, &
					                               c_nonew + c_nomodify, &
					                               c_default)
										 
dw_storico_stat_fasi_lav_det.set_dw_options(sqlca, &
                            					  dw_storico_stat_fasi_lav_lista, &
					                             c_sharedata + c_scrollparent + c_nonew + c_nomodify, &
               					              c_default)


end event

type cb_elimina from commandbutton within w_storico_stat_tempi_lav
integer x = 1417
integer y = 1440
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina"
end type

event clicked;string ls_cod_reparto,ls_cod_lavorazione,ls_cod_prodotto
decimal ld_tempo_lavorazione,ld_tempo_attrezzaggio,ld_tempo_attrezzaggio_commessa,ld_tempo_risorsa_umana
datetime ldt_data_giorno
long ll_progressivo

ls_cod_prodotto = dw_storico_stat_fasi_lav_lista.getitemstring(dw_storico_stat_fasi_lav_lista.getrow(),"cod_prodotto")
ls_cod_lavorazione = dw_storico_stat_fasi_lav_lista.getitemstring(dw_storico_stat_fasi_lav_lista.getrow(),"cod_lavorazione")
ls_cod_reparto = dw_storico_stat_fasi_lav_lista.getitemstring(dw_storico_stat_fasi_lav_lista.getrow(),"cod_reparto")
ldt_data_giorno = dw_storico_stat_fasi_lav_lista.getitemdatetime(dw_storico_stat_fasi_lav_lista.getrow(),"data_giorno")
ll_progressivo = dw_storico_stat_fasi_lav_lista.getitemnumber(dw_storico_stat_fasi_lav_lista.getrow(),"progressivo")

if g_mb.messagebox("Sep", "Sei sicuro di voler eliminare la riga di storico selezionata?",Exclamation!, YesNo!, 1) = 1 then

	delete storico_stat_fasi_lav
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto
	and    cod_lavorazione=:ls_cod_lavorazione
	and    cod_reparto=:ls_cod_reparto
	and    data_giorno=:ldt_data_giorno
	and    progressivo=:ll_progressivo;
	
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	parent.triggerevent("pc_retrieve")
end if
end event

type cbx_attrezzaggio_com from checkbox within w_storico_stat_tempi_lav
integer x = 526
integer y = 1380
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Attrezz.Com."
boolean lefttext = true
borderstyle borderstyle = stylelowered!
end type

type cbx_risorsa_umana from checkbox within w_storico_stat_tempi_lav
integer x = 457
integer y = 1460
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Risorsa Umana"
boolean lefttext = true
borderstyle borderstyle = stylelowered!
end type

type cbx_attrezzaggio from checkbox within w_storico_stat_tempi_lav
integer x = 23
integer y = 1380
integer width = 407
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Attrezzaggio"
boolean lefttext = true
borderstyle borderstyle = stylelowered!
end type

type cbx_lavorazione from checkbox within w_storico_stat_tempi_lav
integer x = 32
integer y = 1460
integer width = 398
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Lavorazione"
boolean lefttext = true
borderstyle borderstyle = stylelowered!
end type

type cb_aggiorna from commandbutton within w_storico_stat_tempi_lav
integer x = 1006
integer y = 1440
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna T."
end type

event clicked;string ls_cod_reparto,ls_cod_lavorazione,ls_cod_prodotto
decimal ld_tempo_lavorazione,ld_tempo_attrezzaggio,ld_tempo_attrezzaggio_commessa,ld_tempo_risorsa_umana

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_7
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_6
ls_cod_reparto = s_cs_xx.parametri.parametro_s_5

if g_mb.messagebox("Sep", "Sei sicuro di voler aggiornare i tempi di produzione con i tempi calcolati?",Exclamation!, YesNo!, 1) = 1 then

	if cbx_attrezzaggio.checked = false and cbx_attrezzaggio_com.checked=false and cbx_lavorazione.checked and cbx_risorsa_umana.checked then 
		g_mb.messagebox("Sep","Non è stato selezionato alcun tempo per l'aggiornamento. Verificare.",information!)
		return
	end if
	
	select tempo_lavorazione,
			 tempo_attrezzaggio,
			 tempo_attrezzaggio_commessa,
			 tempo_risorsa_umana
	into   :ld_tempo_lavorazione,
			 :ld_tempo_attrezzaggio,
			 :ld_tempo_attrezzaggio_commessa,
			 :ld_tempo_risorsa_umana
	from   tes_fasi_lavorazione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto
	and    cod_lavorazione=:ls_cod_lavorazione
	and    cod_reparto=:ls_cod_reparto;

	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if

	if cbx_attrezzaggio.checked = true then
		ld_tempo_attrezzaggio = dw_storico_stat_fasi_lav_det.getitemnumber(dw_storico_stat_fasi_lav_det.getrow(),"tempo_attrezzaggio")
	end if
	
	if cbx_attrezzaggio_com.checked=true then
		ld_tempo_attrezzaggio_commessa = dw_storico_stat_fasi_lav_det.getitemnumber(dw_storico_stat_fasi_lav_det.getrow(),"tempo_attrezzaggio_commessa")
	end if
	
	if cbx_lavorazione.checked = true then
		ld_tempo_lavorazione = dw_storico_stat_fasi_lav_det.getitemnumber(dw_storico_stat_fasi_lav_det.getrow(),"tempo_lavorazione")
	end if
	
	if cbx_risorsa_umana.checked = true then
		ld_tempo_risorsa_umana = dw_storico_stat_fasi_lav_det.getitemnumber(dw_storico_stat_fasi_lav_det.getrow(),"tempo_risorsa_umana")
	end if

	update tes_fasi_lavorazione
	set    tempo_lavorazione = :ld_tempo_lavorazione,
			 tempo_attrezzaggio = :ld_tempo_attrezzaggio,
			 tempo_attrezzaggio_commessa = :ld_tempo_attrezzaggio_commessa,
			 tempo_risorsa_umana = :ld_tempo_risorsa_umana
	from   tes_fasi_lavorazione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto
	and    cod_lavorazione=:ls_cod_lavorazione
	and    cod_reparto=:ls_cod_reparto;

	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
end if
end event

type dw_storico_stat_fasi_lav_det from uo_cs_xx_dw within w_storico_stat_tempi_lav
integer x = 23
integer y = 660
integer width = 1783
integer height = 700
integer taborder = 20
string dataobject = "d_storico_stat_fasi_lav_det"
borderstyle borderstyle = styleraised!
end type

type dw_storico_stat_fasi_lav_lista from uo_cs_xx_dw within w_storico_stat_tempi_lav
integer x = 23
integer y = 20
integer width = 1783
integer height = 620
integer taborder = 10
string dataobject = "d_storico_stat_fasi_lav_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_7
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_6
ls_cod_reparto = s_cs_xx.parametri.parametro_s_5

ll_errore = retrieve(s_cs_xx.cod_azienda,ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

