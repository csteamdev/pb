﻿$PBExportHeader$w_tes_fasi_lavorazione.srw
$PBExportComments$Testata Fasi Lavorazione
forward
global type w_tes_fasi_lavorazione from w_cs_xx_principale
end type
type dw_tes_fasi_lavorazione_lista from uo_cs_xx_dw within w_tes_fasi_lavorazione
end type
type cb_dettagli from commandbutton within w_tes_fasi_lavorazione
end type
type dw_fasi_lavorazione_det from uo_cs_xx_dw within w_tes_fasi_lavorazione
end type
end forward

global type w_tes_fasi_lavorazione from w_cs_xx_principale
integer width = 2981
integer height = 1980
string title = "Fasi Lavorazione - Schede Tecniche Produzione"
dw_tes_fasi_lavorazione_lista dw_tes_fasi_lavorazione_lista
cb_dettagli cb_dettagli
dw_fasi_lavorazione_det dw_fasi_lavorazione_det
end type
global w_tes_fasi_lavorazione w_tes_fasi_lavorazione

type variables
boolean ib_delete=true, ib_new=false
string is_prodotto, is_reparto, is_lavorazione
end variables

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_fasi_lavorazione_det,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_fasi_lavorazione_det,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N'")

f_PO_LoadDDDW_DW(dw_fasi_lavorazione_det,"cod_cat_attrezzature_2",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'S'")

f_PO_LoadDDDW_DW(dw_fasi_lavorazione_det,"cod_lavorazione",sqlca,&
                 "tab_lavorazioni","cod_lavorazione","des_lavorazione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_fasi_lavorazione_det,"cod_centro_costo",sqlca,&
                 "tab_centri_costo","cod_centro_costo","des_centro_costo",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pc_setwindow;call super::pc_setwindow;dw_tes_fasi_lavorazione_lista.ib_proteggi_chiavi = false
dw_fasi_lavorazione_det.ib_proteggi_chiavi = false


dw_tes_fasi_lavorazione_lista.set_dw_key("cod_azienda")
dw_tes_fasi_lavorazione_lista.set_dw_options(sqlca,pcca.null_object, c_default, c_default)
dw_fasi_lavorazione_det.set_dw_options(sqlca,dw_tes_fasi_lavorazione_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_fasi_lavorazione_lista


end event

on w_tes_fasi_lavorazione.create
int iCurrent
call super::create
this.dw_tes_fasi_lavorazione_lista=create dw_tes_fasi_lavorazione_lista
this.cb_dettagli=create cb_dettagli
this.dw_fasi_lavorazione_det=create dw_fasi_lavorazione_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_fasi_lavorazione_lista
this.Control[iCurrent+2]=this.cb_dettagli
this.Control[iCurrent+3]=this.dw_fasi_lavorazione_det
end on

on w_tes_fasi_lavorazione.destroy
call super::destroy
destroy(this.dw_tes_fasi_lavorazione_lista)
destroy(this.cb_dettagli)
destroy(this.dw_fasi_lavorazione_det)
end on

event open;call super::open;	dw_fasi_lavorazione_det.object.b_ricerca_prodotto.enabled=false
	dw_fasi_lavorazione_det.object.b_ricerca_prod_sfrido.enabled=false
end event

type dw_tes_fasi_lavorazione_lista from uo_cs_xx_dw within w_tes_fasi_lavorazione
integer x = 23
integer y = 20
integer width = 2217
integer height = 500
integer taborder = 50
string dataobject = "d_tes_fasi_lavorazione_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on updatestart;call uo_cs_xx_dw::updatestart;if ib_delete = true then
   DELETE FROM det_fasi_lavorazione
          WHERE (det_fasi_lavorazione.cod_azienda = :s_cs_xx.cod_azienda) and
                (det_fasi_lavorazione.cod_prodotto = :is_prodotto) and
                (det_fasi_lavorazione.cod_reparto = :is_reparto) and
                (det_fasi_lavorazione.cod_lavorazione = :is_lavorazione);
   ib_delete = false
end if
                
end on

event pcd_view;call super::pcd_view;if i_extendmode then
	if dw_tes_fasi_lavorazione_lista.getrow() > 0 then
   	is_prodotto = dw_tes_fasi_lavorazione_lista.getitemstring(dw_tes_fasi_lavorazione_lista.getrow(), "cod_prodotto")
	   is_reparto = dw_tes_fasi_lavorazione_lista.getitemstring(dw_tes_fasi_lavorazione_lista.getrow(), "cod_reparto")
   	is_lavorazione = dw_tes_fasi_lavorazione_lista.getitemstring(dw_tes_fasi_lavorazione_lista.getrow(), "cod_lavorazione")
	end if

	cb_dettagli.enabled = true
	dw_fasi_lavorazione_det.object.b_ricerca_prodotto.enabled=false
	dw_fasi_lavorazione_det.object.b_ricerca_prod_sfrido.enabled=false

end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_dettagli.enabled = false
ib_delete = true
end on

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_dettagli.enabled = false
	dw_fasi_lavorazione_det.object.b_ricerca_prodotto.enabled=true
	dw_fasi_lavorazione_det.object.b_ricerca_prod_sfrido.enabled=true
end if

end event

event pcd_new;call super::pcd_new;if i_extendmode then
	cb_dettagli.enabled = false
	dw_fasi_lavorazione_det.object.b_ricerca_prodotto.enabled=true
	dw_fasi_lavorazione_det.object.b_ricerca_prod_sfrido.enabled=true
	ib_new =true
end if
end event

type cb_dettagli from commandbutton within w_tes_fasi_lavorazione
integer x = 2263
integer y = 440
integer width = 370
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Dettagli"
end type

on clicked;if not isvalid(w_det_fasi_lavorazione) then
	window_open_parm(w_det_fasi_lavorazione, -1, dw_tes_fasi_lavorazione_lista)
end if
end on

type dw_fasi_lavorazione_det from uo_cs_xx_dw within w_tes_fasi_lavorazione
integer x = 23
integer y = 540
integer width = 2903
integer height = 1320
integer taborder = 30
string dataobject = "d_fasi_lavorazione_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;choose case i_colname
	case "cod_prodotto"
		
		if not isnull(i_coltext) and i_coltext <> "" then
			
			f_PO_LoadDDDW_DW(dw_fasi_lavorazione_det,"cod_versione",sqlca,&
                 "distinta","cod_versione","cod_versione",&
                 " cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto_padre = '" + i_coltext + "' ")
			
			
		end if
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_fasi_lavorazione_det,"cod_prodotto")
		
	case "b_ricerca_prod_sfrido"
		guo_ricerca.uof_ricerca_prodotto(dw_fasi_lavorazione_det,"cod_prodotto_sfrido")
		
end choose
end event

