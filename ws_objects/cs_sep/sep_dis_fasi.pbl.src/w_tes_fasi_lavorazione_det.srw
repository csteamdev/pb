﻿$PBExportHeader$w_tes_fasi_lavorazione_det.srw
forward
global type w_tes_fasi_lavorazione_det from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_tes_fasi_lavorazione_det
end type
end forward

global type w_tes_fasi_lavorazione_det from w_cs_xx_principale
integer width = 3049
integer height = 1432
string title = "Stabilimenti Origine e Produzione"
dw_lista dw_lista
end type
global w_tes_fasi_lavorazione_det w_tes_fasi_lavorazione_det

event pc_setddlb;call super::pc_setddlb;
//f_po_loaddddw_dw(dw_lista, &
//                 "cod_deposito_origine", &
//                 sqlca, &
//                 "anag_depositi", &
//                 "cod_deposito", &
//                 "des_deposito", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_lista, &
                 "cod_deposito_origine", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_lista, &
                 "cod_deposito_produzione", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")






end event

on w_tes_fasi_lavorazione_det.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_tes_fasi_lavorazione_det.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;
dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("cod_prodotto")
dw_lista.set_dw_key("cod_reparto")
dw_lista.set_dw_key("cod_lavorazione")
dw_lista.set_dw_key("cod_versione")

dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default)
end event

type dw_lista from uo_cs_xx_dw within w_tes_fasi_lavorazione_det
integer x = 23
integer y = 28
integer width = 2958
integer height = 1280
integer taborder = 10
string dataobject = "d_tes_fasi_lavorazione_det"
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_versione
long ll_errore, ll_anno_registrazione, ll_num_registrazione

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_cod_reparto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_reparto")
ls_cod_lavorazione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lavorazione")
ls_cod_versione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_versione")

parent.title = "Stabilimenti Origine e Produzione (" + ls_cod_prodotto + " - " + ls_cod_versione + ") " + &
						" - Fase Lav. " + ls_cod_lavorazione

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_versione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;string		ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_versione
long		ll_i, ll_selected_row

ll_selected_row = i_parentdw.i_selectedrows[1]

ls_cod_prodotto =  i_parentdw.getitemstring(ll_selected_row, "cod_prodotto")
ls_cod_reparto =  i_parentdw.getitemstring(ll_selected_row, "cod_reparto")
ls_cod_lavorazione =  i_parentdw.getitemstring(ll_selected_row, "cod_lavorazione")
ls_cod_versione =  i_parentdw.getitemstring(ll_selected_row, "cod_versione")

for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i, "cod_azienda")) or this.getitemstring(ll_i, "cod_azienda")="" then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_prodotto")) or this.getitemstring(ll_i, "cod_prodotto") = "" then
		this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_reparto")) or this.getitemstring(ll_i, "cod_reparto") = "" then
		this.setitem(ll_i, "cod_reparto", ls_cod_reparto)
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_lavorazione")) or this.getitemstring(ll_i, "cod_lavorazione") = "" then
		this.setitem(ll_i, "cod_lavorazione", ls_cod_lavorazione)
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_versione")) or this.getitemstring(ll_i, "cod_versione") = "" then
		this.setitem(ll_i, "cod_versione", ls_cod_versione)
	end if
	
next

end event

event pcd_validaterow;call super::pcd_validaterow;long		ll_i

for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i, "cod_deposito_origine")) or this.getitemstring(ll_i, "cod_deposito_origine")="" then
		g_mb.error("APICE","Manca il Deposito Origine in qualche riga!")
		pcca.error = c_fatal
		return
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_deposito_produzione")) or this.getitemstring(ll_i, "cod_deposito_produzione") = "" then
		g_mb.error("APICE","Manca il Deposito Produzione in qualche riga!")
		pcca.error = c_fatal
		return
	end if
	
next

end event

