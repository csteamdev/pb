﻿$PBExportHeader$w_tes_fasi_lavorazione_prod.srw
forward
global type w_tes_fasi_lavorazione_prod from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_tes_fasi_lavorazione_prod
end type
end forward

global type w_tes_fasi_lavorazione_prod from w_cs_xx_principale
integer width = 3342
integer height = 1884
string title = "Stabilimenti Produzione e relativi Reparti"
dw_lista dw_lista
end type
global w_tes_fasi_lavorazione_prod w_tes_fasi_lavorazione_prod

forward prototypes
public function integer wf_filtra_reparti (string fs_cod_deposito)
end prototypes

public function integer wf_filtra_reparti (string fs_cod_deposito);datawindowchild  		ldwc_child
integer					li_child
string						ls_filter

li_child = dw_lista.GetChild("cod_reparto_produzione", ldwc_child)


if fs_cod_deposito="" or isnull(fs_cod_deposito) then
	//fai vedere tutti i reparti
	ls_filter = ""
else
	//filtra i reparti per deposito
	ls_filter = "cod_deposito='"+fs_cod_deposito+"'"
end if

ldwc_child.setfilter(ls_filter)
ldwc_child.filter()

return 1

end function

event pc_setddlb;call super::pc_setddlb;
//f_po_loaddddw_dw(dw_lista, &
//                 "cod_deposito_origine", &
//                 sqlca, &
//                 "anag_depositi", &
//                 "cod_deposito", &
//                 "des_deposito", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_lista, &
                 "cod_deposito_produzione", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDDW_DW(dw_lista,"cod_reparto_produzione",sqlca,&
//                 "anag_reparti","cod_reparto","des_reparto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
datawindowchild		ldwc_child

dw_lista.getchild("cod_reparto_produzione", ldwc_child)

ldwc_child.SetTransObject(sqlca)
ldwc_child.Retrieve()






end event

on w_tes_fasi_lavorazione_prod.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_tes_fasi_lavorazione_prod.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;
dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("cod_prodotto")
dw_lista.set_dw_key("cod_reparto")
dw_lista.set_dw_key("cod_lavorazione")
dw_lista.set_dw_key("cod_versione")

dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default)
end event

type dw_lista from uo_cs_xx_dw within w_tes_fasi_lavorazione_prod
integer x = 23
integer y = 28
integer width = 3255
integer height = 1724
integer taborder = 10
string dataobject = "d_tes_fasi_lavorazione_prod"
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_versione
long ll_errore, ll_anno_registrazione, ll_num_registrazione

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_cod_reparto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_reparto")
ls_cod_lavorazione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lavorazione")
ls_cod_versione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_versione")

parent.title = "Stabilimenti Produzione e relativi Reparti (" + ls_cod_prodotto + " - " + ls_cod_versione + ") " + &
						" - Fase Lav. " + ls_cod_lavorazione

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_versione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;string		ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_versione
long		ll_i, ll_selected_row

ll_selected_row = i_parentdw.i_selectedrows[1]

ls_cod_prodotto =  i_parentdw.getitemstring(ll_selected_row, "cod_prodotto")
ls_cod_reparto =  i_parentdw.getitemstring(ll_selected_row, "cod_reparto")
ls_cod_lavorazione =  i_parentdw.getitemstring(ll_selected_row, "cod_lavorazione")
ls_cod_versione =  i_parentdw.getitemstring(ll_selected_row, "cod_versione")

for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i, "cod_azienda")) or this.getitemstring(ll_i, "cod_azienda")="" then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_prodotto")) or this.getitemstring(ll_i, "cod_prodotto") = "" then
		this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_reparto")) or this.getitemstring(ll_i, "cod_reparto") = "" then
		this.setitem(ll_i, "cod_reparto", ls_cod_reparto)
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_lavorazione")) or this.getitemstring(ll_i, "cod_lavorazione") = "" then
		this.setitem(ll_i, "cod_lavorazione", ls_cod_lavorazione)
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_versione")) or this.getitemstring(ll_i, "cod_versione") = "" then
		this.setitem(ll_i, "cod_versione", ls_cod_versione)
	end if
	
next

end event

event pcd_validaterow;call super::pcd_validaterow;long		ll_i

for ll_i = 1 to this.rowcount()
	
	if isnull(this.getitemstring(ll_i, "cod_deposito_produzione")) or this.getitemstring(ll_i, "cod_deposito_produzione") = "" then
		g_mb.error("APICE","Manca il Deposito Produzione in qualche riga!")
		pcca.error = c_fatal
		return
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_reparto_produzione")) or this.getitemstring(ll_i, "cod_reparto_produzione") = "" then
		g_mb.error("APICE","Manca il Reparto Produzione in qualche riga!")
		pcca.error = c_fatal
		return
	end if
	
next

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_deposito

if currentrow > 0 then
else
	return
end if

ls_cod_deposito = getitemstring(currentrow, "cod_deposito_produzione")
wf_filtra_reparti(ls_cod_deposito)
end event

event itemchanged;call super::itemchanged;

if row > 0 then
else
	return
end if

choose case dwo.name
	case "cod_deposito_produzione"
		wf_filtra_reparti(data)
		
end choose

end event

