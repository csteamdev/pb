﻿$PBExportHeader$uo_service_avan_prod.sru
forward
global type uo_service_avan_prod from uo_service_base
end type
end forward

global type uo_service_avan_prod from uo_service_base
end type
global uo_service_avan_prod uo_service_avan_prod

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);/* 	Questo servizio provvede alla verifica delle commesse di produzione eseguite tramite TABLET
	Se tutte le fasi sono chiuse il sistema provvede ad eseguire i movimenti di magazzino che il tablet non fa;
	in questo modo non serve portare la logica dei movimenti di magazzino in java
	
##########################################################################################
nota
il processo cs_team.exe va lanciato così						cs_team.exe S=avan_prod,A01,NUM_GG, ANNO_COMM, NUM_COMM, n
																					NUM_GG = numero di giorni retroattivi per esaminare le commesse (basato sulla data registrazione commessa)
																					ANNO_COMM / NUM_COMM = anno e numer ocommessa specifica da elaborare; in questo caso il parametro NUM_GG viene ignorato
																					n è un numero che indica la verbosità del log (min 1, max 5, default 2)
#########################################################################################  */
string			ls_null[], ls_path_log, ls_sql, ls_cod_prodotto, ls_cod_tipo_mov_prel_mat_prime, ls_cod_deposito[],  ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], as_errore
long			ll_null[], ll_num_gg, ll_par_anno_comm, ll_par_num_comm, ll_rows, ll_i, ll_anno_comm, ll_num_comm, ll_num_fasi_aperte, ll_y, ll_tot_stock, ll_prog_stock[], &
				ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_ret, ll_anno_registrazione[], ll_num_registrazione[], ll_anno_registrazione_rag[], ll_num_registrazione_rag[]
decimal		ld_val_movimento,ld_quan_prelevata
date 			ldt_today
datetime		ldt_null[], ldt_data_rif_mrp, ldt_data_stock[], ldt_oggi
time 			ltm_now
datastore 	lds_data1, lds_data2
uo_magazzino luo_mag


ldt_oggi = datetime(today(),00:00:00)
uof_set_s_cs_xx(auo_params.uof_get_string(1,"A01"))

ll_num_gg = auo_params.uof_get_long(2, ll_num_gg)

ll_par_anno_comm = auo_params.uof_get_long(3, ll_par_anno_comm)

ll_par_num_comm = auo_params.uof_get_long(4, ll_par_num_comm)

//verbosity
iuo_log.set_log_level( iuo_log.info_level )

//assegno il nome al file di log    bufferMRP_YYYYMMDD.log
ldt_today = today()

ldt_data_rif_mrp = datetime(ldt_today, 00:00:00)

ltm_now = now()
ls_path_log = s_cs_xx.volume + s_cs_xx.risorse + "logs\avan_prod_" + &
								string(year(ldt_today)) + "-" + right("00" + string(month(ldt_today)), 2) + "-" + right("00" + string(day(ldt_today)), 2) + ".log"

//fai l'append nel file del giorno
iuo_log.set_file( ls_path_log, false)
iuo_log.warn("### INIZIO ELABORAZIONE AVANZAMENTO PRODUZIONE COMMESSE ###")

// per prima cosa controllo se ci sono fasi aperte

ls_sql = g_str.format("select anno_commessa, num_commessa from anag_commesse where cod_azienda = '$1' and data_registrazione >= '$2' ", s_cs_xx.cod_azienda, string(relativedate(today(), ll_num_gg * -1), s_cs_xx.db_funzioni.formato_data) )
if ll_par_anno_comm > 0 and not isnull(ll_par_anno_comm) then
	ls_sql += g_str.format(" and anno_commessa = $1 ", ll_par_anno_comm)
end if
if ll_par_anno_comm > 0 and not isnull(ll_par_anno_comm) then
	ls_sql += g_str.format(" and num_commessa = $1 " ,ll_par_num_comm)
end if

ll_rows = guo_functions.uof_crea_datastore( lds_data1, ls_sql)

for ll_i = 1 to ll_rows
	// per prima cosa verifico che non ci siano fasi aperte; devono essee tutte chiuse
	
	ll_anno_comm = lds_data1.getitemnumber(ll_i,1)
	ll_num_comm = lds_data1.getitemnumber(ll_i,2)
	
	iuo_log.info(fill("-",50))
	iuo_log.info(g_str.format("Elaborazione commessa $1-$2", ll_anno_comm, ll_num_comm))

	
	select 	count(*)
	into		:ll_num_fasi_aperte
	from 		avan_produzione_com 
	where  	cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :ll_anno_comm and
				num_commessa = :ll_num_comm and
				flag_fine_fase = 'N' and
				flag_esterna ='N';
	if ll_num_fasi_aperte = 0 then
		
		lds_data2= create datastore
		lds_data2.dataobject="d_ds_service_avan_prod"
		lds_data2.settransobject(sqlca)
		
		ll_tot_stock = lds_data2.retrieve(s_cs_xx.cod_azienda, ll_anno_comm, ll_num_comm)
		
		for ll_y = 1 to ll_tot_stock
			
			// -----------------------------------
			// per ogni semilavorato è necessario eseguire il movimento di carico e memorizzarlo nelle colonne anno_reg_sl, num_reg_sl
			
			
			// -------------------------------------
			
			ls_cod_deposito[] = ls_null[]
			ls_cod_ubicazione[] = ls_null[]
			ls_cod_lotto[] = ls_null[]
			ldt_data_stock[] = ldt_null[]
			ll_prog_stock[] = ll_null[]
			ls_cod_cliente[] = ls_null[]
			ls_cod_fornitore[] = ls_null[]
			
			
			ls_cod_prodotto = lds_data2.getitemstring(ll_y, 10)
			if isnull(ls_cod_prodotto) then 
				iuo_log.error(g_str.format("Fase semilavorato $1 Reparto $2: non è stato specificato il prodotto da scaricare", lds_data2.getitemstring(ll_y, 7), lds_data2.getitemstring(ll_y, 8) ) )
				continue
			end if
			
			ls_cod_tipo_mov_prel_mat_prime =  lds_data2.getitemstring(ll_y, 5)
			if isnull(ls_cod_tipo_mov_prel_mat_prime) then 
				iuo_log.error(g_str.format("Fase semilavorato $1 Reparto $2: non è stato specificato il tipo di movimento scarico MP", lds_data2.getitemstring(ll_y, 7), lds_data2.getitemstring(ll_y, 8) ) )
				continue
			end if
			
			ls_cod_deposito[1] = lds_data2.getitemstring(ll_y, 11)
			if isnull(ls_cod_deposito[1]) then 
				iuo_log.error(g_str.format("Fase semilavorato $1 Reparto $2: non è stato specificato il DEPOSITO", lds_data2.getitemstring(ll_y, 7), lds_data2.getitemstring(ll_y, 8) ) )
				continue
			end if
			
			ls_cod_ubicazione[1] = lds_data2.getitemstring(ll_y, 12)
			if isnull(ls_cod_ubicazione[1]) then 
				iuo_log.error(g_str.format("Fase semilavorato $1 Reparto $2: non è stato specificata l'UBIVAZIONE.", lds_data2.getitemstring(ll_y, 7), lds_data2.getitemstring(ll_y, 8) ) )
				continue
			end if
			
			ls_cod_lotto[1] =  lds_data2.getitemstring(ll_y, 13)
			if isnull(ls_cod_lotto[1]) then 
				iuo_log.error(g_str.format("Fase semilavorato $1 Reparto $2: non è stato specificato il LOTTO ", lds_data2.getitemstring(ll_y, 7), lds_data2.getitemstring(ll_y, 8) ) )
				continue
			end if
			
			ldt_data_stock[1] =  lds_data2.getitemdatetime(ll_y, 14)
			if isnull(ldt_data_stock[1]) then 
				iuo_log.error(g_str.format("Fase semilavorato $1 Reparto $2: non è stato specificato la DATA LOTTO ", lds_data2.getitemstring(ll_y, 7), lds_data2.getitemstring(ll_y, 8) ) )
				continue
			end if
			
			ll_prog_stock[1] =  lds_data2.getitemnumber(ll_y, 15)
			if isnull(ll_prog_stock[1]) then 
				iuo_log.error(g_str.format("Fase semilavorato $1 Reparto $2: non è stato specificato la PROGRESSIVO LOTTO ", lds_data2.getitemstring(ll_y, 7), lds_data2.getitemstring(ll_y, 8) ) )
				continue
			end if
			setnull(ls_cod_cliente[1])
			setnull(ls_cod_fornitore[1])
			
			iuo_log.info(g_str.format("Generazione movimento magazzino $1 per prodotto", ls_cod_tipo_mov_prel_mat_prime, ls_cod_prodotto) )
			iuo_log.info(g_str.format("Lotto $1 - $2 - $3 - $4 - $5", ls_cod_deposito[1], ls_cod_ubicazione[1], ls_cod_lotto[1], ldt_data_stock[1], ll_prog_stock[1]) )
			
			if f_crea_dest_mov_mag(ls_cod_tipo_mov_prel_mat_prime, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov, as_errore) = -1 then
				iuo_log.error( g_str.format("Commessa $1-$2; f_crea_dest_mov_mag(): Errore in creazione destinazioni stock movimento $3. Errore $4",ll_anno_comm, ll_num_comm, ls_cod_tipo_mov_prel_mat_prime, as_errore))
				return -1
			end if
		
			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_mov_prel_mat_prime, ls_cod_prodotto) = -1 then
				as_errore = "Si è verificato un errore in fase di verifica destinazioni movimenti magazzino."
				iuo_log.error(g_str.format("Commessa $1-$2; f_verifica_dest_mov_mag(): $3 ",ll_anno_comm, ll_num_comm, as_errore))
			  	return -1
			end if
			
			luo_mag = create uo_magazzino
		
			luo_mag.uof_set_flag_commessa("S")
			
			luo_mag.ib_salta_controllo=true
			luo_mag.ib_avvisa=false
			
			ld_quan_prelevata =  lds_data2.getitemnumber(ll_y, 16)
			ld_val_movimento = 1
			
			ll_anno_registrazione[] = ll_null[]
			ll_num_registrazione[] = ll_null
			ll_ret = luo_mag.uof_movimenti_mag(ldt_oggi, &
												  ls_cod_tipo_mov_prel_mat_prime, &
												  "N", &
												  ls_cod_prodotto, &
												  ld_quan_prelevata, &
												  ld_val_movimento, &
												  ll_num_comm, &
												  ldt_oggi, &
												  string(ll_anno_comm), &
												  ll_anno_reg_des_mov, &
												  ll_num_reg_des_mov, &
												  ls_cod_deposito[], &
												  ls_cod_ubicazione[], &
												  ls_cod_lotto[], &
												  ldt_data_stock[], &
												  ll_prog_stock[], &
												  ls_cod_fornitore[], &
												  ls_cod_cliente[], &
												  ll_anno_registrazione[], &
												  ll_num_registrazione[], &
												  as_errore)
									  
			
			if ll_ret=-1 then
				
				//se per disgrazia la variabile errore è vuota valorizzala cosi ...
				if as_errore="" or isnull(as_errore) then
					as_errore = "Errore su movimenti magazzino, prodotto: " + ls_cod_prodotto &
								 + ", deposito:" + ls_cod_deposito[1] + ", ubicazione:" + ls_cod_ubicazione[1] &
								 + ", cod_lotto:" + ls_cod_lotto[1] + ", data_stock:" + string(ldt_data_stock[1]) &
								 + ", prog_stock:" + string(ll_prog_stock[1])
				end if
				
				iuo_log.error(as_errore)
				return -1
			end if
			
			f_elimina_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov)
			
			iuo_log.info(g_str.format("Generato movimento magazzino $1-$2", ll_anno_registrazione[1],  ll_num_registrazione[1]))

			
			// stefanop 06/12/2011: movimento anche il prodotto raggruppato
			
			if luo_mag.uof_movimenti_mag_ragguppato(ldt_oggi, &
												  ls_cod_tipo_mov_prel_mat_prime, &
												  "N", &
												  ls_cod_prodotto, &
												  ld_quan_prelevata, &
												  1, &
												  ll_num_comm, &
												  ldt_oggi, &
												  string(ll_anno_comm), &
												  ll_anno_reg_des_mov, &
												  ll_num_reg_des_mov, &
												  ls_cod_deposito[], &
												  ls_cod_ubicazione[], &
												  ls_cod_lotto[], &
												  ldt_data_stock[], &
												  ll_prog_stock[], &
												  ls_cod_fornitore[], &
												  ls_cod_cliente[], &
												  ll_anno_registrazione_rag[], &
												  ll_num_registrazione_rag[],&
												  ll_num_comm,&
												  ll_anno_registrazione[1],&
												  ll_num_registrazione[1], &
												  as_errore) = -1 then
				
				//se per disgrazia la variabile errore è vuota valorizzala cosi ...
				if as_errore="" or isnull(as_errore) then
					as_errore = "Errore su movimenti magazzino, prodotto raggruppato: " + ls_cod_prodotto
				end if
							 
				iuo_log.error(as_errore)
				destroy luo_mag
				return -1
			end if	
		next
		
		// eseguo update della commessa per la sua chiusura
		update anag_commesse 
		set	 flag_tipo_avanzamento = '8'
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa=:ll_anno_comm and
				num_commessa = :ll_num_comm;
		if sqlca.sqlcode < 0 then
			as_errore = g_str.format("Commessa $1-$2 . Errore SQL durante update flag_tipo_avanzamento su anag_commesse." + sqlca.sqlerrtext, ll_anno_comm, ll_num_comm)
		end if
				
	else
		
		iuo_log.info(g_str.format("Commessa $1-$2 saltata perchè vi sono ancora $3 fasi aperte", ll_anno_comm, ll_num_comm,ll_num_fasi_aperte))
	end if
next

// -------------------- 
// Se è andato bene allora devo fare il movimento di scarico per ogni semilavorato e il movimenti di carico del prodotto finito
// --------------------

iuo_log.warn("### FINE ELABORAZIONE AVANZAMENTO PRODUZIONE COMMESSE  ###")

return 0
end function

on uo_service_avan_prod.create
call super::create
end on

on uo_service_avan_prod.destroy
call super::destroy
end on

