﻿$PBExportHeader$w_grafici_difetti_out.srw
$PBExportComments$Window gestione grafici difetti Prodotti Finiti di Fase
forward
global type w_grafici_difetti_out from w_cs_xx_principale
end type
type gb_1 from groupbox within w_grafici_difetti_out
end type
type st_1 from statictext within w_grafici_difetti_out
end type
type st_3 from statictext within w_grafici_difetti_out
end type
type em_data_inizio from editmask within w_grafici_difetti_out
end type
type em_data_fine from editmask within w_grafici_difetti_out
end type
type tab_1 from tab within w_grafici_difetti_out
end type
type tabpage_operai from userobject within tab_1
end type
type dw_grafico_difetti_operaio from uo_cs_graph within tabpage_operai
end type
type tabpage_operai from userobject within tab_1
dw_grafico_difetti_operaio dw_grafico_difetti_operaio
end type
type tabpage_difetti from userobject within tab_1
end type
type dw_grafico_difetti_difetto from uo_cs_graph within tabpage_difetti
end type
type tabpage_difetti from userobject within tab_1
dw_grafico_difetti_difetto dw_grafico_difetti_difetto
end type
type tabpage_prodotti from userobject within tab_1
end type
type dw_grafico_difetti_prodotto from uo_cs_graph within tabpage_prodotti
end type
type tabpage_prodotti from userobject within tab_1
dw_grafico_difetti_prodotto dw_grafico_difetti_prodotto
end type
type tabpage_attrezzature from userobject within tab_1
end type
type dw_grafico_difetti_attrezzature from uo_cs_graph within tabpage_attrezzature
end type
type tabpage_attrezzature from userobject within tab_1
dw_grafico_difetti_attrezzature dw_grafico_difetti_attrezzature
end type
type tab_1 from tab within w_grafici_difetti_out
tabpage_operai tabpage_operai
tabpage_difetti tabpage_difetti
tabpage_prodotti tabpage_prodotti
tabpage_attrezzature tabpage_attrezzature
end type
type cb_aggiorna from commandbutton within w_grafici_difetti_out
end type
end forward

global type w_grafici_difetti_out from w_cs_xx_principale
integer width = 3429
integer height = 1856
string title = "Grafici Difetti"
gb_1 gb_1
st_1 st_1
st_3 st_3
em_data_inizio em_data_inizio
em_data_fine em_data_fine
tab_1 tab_1
cb_aggiorna cb_aggiorna
end type
global w_grafici_difetti_out w_grafici_difetti_out

on w_grafici_difetti_out.create
int iCurrent
call super::create
this.gb_1=create gb_1
this.st_1=create st_1
this.st_3=create st_3
this.em_data_inizio=create em_data_inizio
this.em_data_fine=create em_data_fine
this.tab_1=create tab_1
this.cb_aggiorna=create cb_aggiorna
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.gb_1
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.st_3
this.Control[iCurrent+4]=this.em_data_inizio
this.Control[iCurrent+5]=this.em_data_fine
this.Control[iCurrent+6]=this.tab_1
this.Control[iCurrent+7]=this.cb_aggiorna
end on

on w_grafici_difetti_out.destroy
call super::destroy
destroy(this.gb_1)
destroy(this.st_1)
destroy(this.st_3)
destroy(this.em_data_inizio)
destroy(this.em_data_fine)
destroy(this.tab_1)
destroy(this.cb_aggiorna)
end on

event open;call super::open;em_data_fine.text = string(today())
em_data_inizio.text = string(relativedate(today(), -10))
end event

type gb_1 from groupbox within w_grafici_difetti_out
integer x = 23
integer y = 20
integer width = 1760
integer height = 160
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
string text = "Periodo"
end type

type st_1 from statictext within w_grafici_difetti_out
integer x = 69
integer y = 100
integer width = 274
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
string text = "Data Inizio"
boolean focusrectangle = false
end type

type st_3 from statictext within w_grafici_difetti_out
integer x = 891
integer y = 100
integer width = 274
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean enabled = false
string text = "Data Fine"
boolean focusrectangle = false
end type

type em_data_inizio from editmask within w_grafici_difetti_out
integer x = 366
integer y = 80
integer width = 389
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

type em_data_fine from editmask within w_grafici_difetti_out
integer x = 1166
integer y = 80
integer width = 389
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

type tab_1 from tab within w_grafici_difetti_out
integer x = 23
integer y = 200
integer width = 3337
integer height = 1520
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
boolean raggedright = true
integer selectedtab = 1
tabpage_operai tabpage_operai
tabpage_difetti tabpage_difetti
tabpage_prodotti tabpage_prodotti
tabpage_attrezzature tabpage_attrezzature
end type

on tab_1.create
this.tabpage_operai=create tabpage_operai
this.tabpage_difetti=create tabpage_difetti
this.tabpage_prodotti=create tabpage_prodotti
this.tabpage_attrezzature=create tabpage_attrezzature
this.Control[]={this.tabpage_operai,&
this.tabpage_difetti,&
this.tabpage_prodotti,&
this.tabpage_attrezzature}
end on

on tab_1.destroy
destroy(this.tabpage_operai)
destroy(this.tabpage_difetti)
destroy(this.tabpage_prodotti)
destroy(this.tabpage_attrezzature)
end on

type tabpage_operai from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3301
integer height = 1396
long backcolor = 79741120
string text = "Operai"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_grafico_difetti_operaio dw_grafico_difetti_operaio
end type

on tabpage_operai.create
this.dw_grafico_difetti_operaio=create dw_grafico_difetti_operaio
this.Control[]={this.dw_grafico_difetti_operaio}
end on

on tabpage_operai.destroy
destroy(this.dw_grafico_difetti_operaio)
end on

type dw_grafico_difetti_operaio from uo_cs_graph within tabpage_operai
integer x = 14
integer y = 20
integer width = 3273
integer height = 1352
integer taborder = 60
end type

type tabpage_difetti from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3301
integer height = 1396
long backcolor = 79741120
string text = "Difetti"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_grafico_difetti_difetto dw_grafico_difetti_difetto
end type

on tabpage_difetti.create
this.dw_grafico_difetti_difetto=create dw_grafico_difetti_difetto
this.Control[]={this.dw_grafico_difetti_difetto}
end on

on tabpage_difetti.destroy
destroy(this.dw_grafico_difetti_difetto)
end on

type dw_grafico_difetti_difetto from uo_cs_graph within tabpage_difetti
integer x = 27
integer y = 16
integer width = 3259
integer height = 1372
integer taborder = 60
end type

type tabpage_prodotti from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3301
integer height = 1396
long backcolor = 79741120
string text = "Prodotti"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_grafico_difetti_prodotto dw_grafico_difetti_prodotto
end type

on tabpage_prodotti.create
this.dw_grafico_difetti_prodotto=create dw_grafico_difetti_prodotto
this.Control[]={this.dw_grafico_difetti_prodotto}
end on

on tabpage_prodotti.destroy
destroy(this.dw_grafico_difetti_prodotto)
end on

type dw_grafico_difetti_prodotto from uo_cs_graph within tabpage_prodotti
integer x = 23
integer y = 8
integer width = 3259
integer height = 1368
integer taborder = 60
end type

type tabpage_attrezzature from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3301
integer height = 1396
long backcolor = 79741120
string text = "Attrezzature"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_grafico_difetti_attrezzature dw_grafico_difetti_attrezzature
end type

on tabpage_attrezzature.create
this.dw_grafico_difetti_attrezzature=create dw_grafico_difetti_attrezzature
this.Control[]={this.dw_grafico_difetti_attrezzature}
end on

on tabpage_attrezzature.destroy
destroy(this.dw_grafico_difetti_attrezzature)
end on

type dw_grafico_difetti_attrezzature from uo_cs_graph within tabpage_attrezzature
integer x = 32
integer y = 16
integer width = 3250
integer height = 1376
integer taborder = 60
end type

type cb_aggiorna from commandbutton within w_grafici_difetti_out
integer x = 1829
integer y = 80
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;datastore lds_dati

lds_dati = create datastore

//grafico operai --------------------------------------------------------------------------------------------------------
lds_dati.dataobject = "dw_grafico_difetti_operaio"
lds_dati.settransobject(sqlca)
lds_dati.retrieve( 	s_cs_xx.cod_azienda, &
							datetime(date(em_data_inizio.text),00:00:00),&
							datetime(date(em_data_fine.text),00:00:00) )

tab_1.tabpage_operai.dw_grafico_difetti_operaio.ids_data = lds_dati
tab_1.tabpage_operai.dw_grafico_difetti_operaio.ib_datastore = true
tab_1.tabpage_operai.dw_grafico_difetti_operaio.ii_tipo_grafico = 7 //Colonne
tab_1.tabpage_operai.dw_grafico_difetti_operaio.is_titolo = ""
tab_1.tabpage_operai.dw_grafico_difetti_operaio.is_label_categoria = "Cod. Operaio"
tab_1.tabpage_operai.dw_grafico_difetti_operaio.is_label_valori = "Nr. Scatti"

tab_1.tabpage_operai.dw_grafico_difetti_operaio.is_str[1] = "difetti_det_orari_out_cod_operaio"
tab_1.tabpage_operai.dw_grafico_difetti_operaio.is_num[1] = "difetti_det_orari_out_quan_scarto"

tab_1.tabpage_operai.dw_grafico_difetti_operaio.is_source_categoria_col = "difetti_det_orari_out_cod_operaio"

tab_1.tabpage_operai.dw_grafico_difetti_operaio.is_categoria_col = "str_1"
tab_1.tabpage_operai.dw_grafico_difetti_operaio.is_exp_valore = "sum(num_1 for graph)"

tab_1.tabpage_operai.dw_grafico_difetti_operaio.uof_retrieve( )
//tab_1.tabpage_operai.dw_grafico_difetti_operaio.SetTransObject ( SQLCA )
//tab_1.tabpage_operai.dw_grafico_difetti_operaio.Retrieve(s_cs_xx.cod_azienda, datetime(date(em_data_inizio.text),00:00:00),datetime(date(em_data_fine.text),00:00:00)) 
//-----------------------------------------------------------------------------------------------------------------------

//grafico difetti --------------------------------------------------------------------------------------------------------
lds_dati.dataobject = "dw_grafico_difetti_difetto"
lds_dati.settransobject(sqlca)
lds_dati.retrieve( 	s_cs_xx.cod_azienda, &
							datetime(date(em_data_inizio.text),00:00:00),&
							datetime(date(em_data_fine.text),00:00:00) )

tab_1.tabpage_difetti.dw_grafico_difetti_difetto.ids_data = lds_dati
tab_1.tabpage_difetti.dw_grafico_difetti_difetto.ib_datastore = true
tab_1.tabpage_difetti.dw_grafico_difetti_difetto.ii_tipo_grafico = 7 //Colonne
tab_1.tabpage_difetti.dw_grafico_difetti_difetto.is_titolo = ""
tab_1.tabpage_difetti.dw_grafico_difetti_difetto.is_label_categoria = "Tipo Errore"
tab_1.tabpage_difetti.dw_grafico_difetti_difetto.is_label_valori = "Nr. Scatti"

tab_1.tabpage_difetti.dw_grafico_difetti_difetto.is_str[1] = "difetti_det_orari_out_cod_errore"
tab_1.tabpage_difetti.dw_grafico_difetti_difetto.is_num[1] = "difetti_det_orari_out_quan_scarto"

tab_1.tabpage_difetti.dw_grafico_difetti_difetto.is_source_categoria_col = "difetti_det_orari_out_cod_errore"

tab_1.tabpage_difetti.dw_grafico_difetti_difetto.is_categoria_col = "str_1"
tab_1.tabpage_difetti.dw_grafico_difetti_difetto.is_exp_valore = "sum(num_1 for graph)"

tab_1.tabpage_difetti.dw_grafico_difetti_difetto.uof_retrieve( )
//tab_1.tabpage_difetti.dw_grafico_difetti_difetto.SetTransObject ( SQLCA )
//tab_1.tabpage_difetti.dw_grafico_difetti_difetto.Retrieve(s_cs_xx.cod_azienda, datetime(date(em_data_inizio.text),00:00:00),datetime(date(em_data_fine.text),00:00:00)) 
//-----------------------------------------------------------------------------------------------------------------------

//grafico prodotti --------------------------------------------------------------------------------------------------------
lds_dati.dataobject = "dw_grafico_difetti_prodotto"
lds_dati.settransobject(sqlca)
lds_dati.retrieve( 	s_cs_xx.cod_azienda, &
							datetime(date(em_data_inizio.text),00:00:00),&
							datetime(date(em_data_fine.text),00:00:00) )

tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.ids_data = lds_dati
tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.ib_datastore = true
tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.ii_tipo_grafico = 7 //Colonne
tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.is_titolo = ""
tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.is_label_categoria = "Cod. Prodotto"
tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.is_label_valori = "Nr. Scatti"

tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.is_str[1] = "difetti_det_orari_out_cod_prodotto"
tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.is_num[1] = "difetti_det_orari_out_quan_scarto"

tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.is_source_categoria_col = "difetti_det_orari_out_cod_prodotto"

tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.is_categoria_col = "str_1"
tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.is_exp_valore = "sum(num_1 for graph)"

tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.uof_retrieve( )
//tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.SetTransObject ( SQLCA )
//tab_1.tabpage_prodotti.dw_grafico_difetti_prodotto.Retrieve(s_cs_xx.cod_azienda, datetime(date(em_data_inizio.text),00:00:00),datetime(date(em_data_fine.text),00:00:00)) 
//-----------------------------------------------------------------------------------------------------------------------

//grafico attrezzature --------------------------------------------------------------------------------------------------------
lds_dati.dataobject = "dw_grafico_difetti_attrezzature"
lds_dati.settransobject(sqlca)
lds_dati.retrieve( 	s_cs_xx.cod_azienda, &
							datetime(date(em_data_inizio.text),00:00:00),&
							datetime(date(em_data_fine.text),00:00:00) )

tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.ids_data = lds_dati
tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.ib_datastore = true
tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.ii_tipo_grafico = 7 //Colonne
tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.is_titolo = ""
tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.is_label_categoria = "Cod. Attrezzatura"
tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.is_label_valori = "Nr. Scatti"

tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.is_str[1] = "difetti_det_orari_out_cod_attrezzatura"
tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.is_num[1] = "difetti_det_orari_out_quan_scarto"

tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.is_source_categoria_col = "difetti_det_orari_out_cod_attrezzatura"

tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.is_categoria_col = "str_1"
tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.is_exp_valore = "sum(num_1 for graph)"

tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.uof_retrieve( )
//tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.SetTransObject ( SQLCA )
//tab_1.tabpage_attrezzature.dw_grafico_difetti_attrezzature.Retrieve(s_cs_xx.cod_azienda, datetime(date(em_data_inizio.text),00:00:00),datetime(date(em_data_fine.text),00:00:00)) 
//-----------------------------------------------------------------------------------------------------------------------

destroy lds_dati
end event

