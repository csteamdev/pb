﻿$PBExportHeader$w_grafici_difetti_out_difetti.srw
$PBExportComments$Window gestione grafici difetti Prodotti Finiti di Fase dettaglio Difetti
forward
global type w_grafici_difetti_out_difetti from w_cs_xx_risposta
end type
type tab_1 from tab within w_grafici_difetti_out_difetti
end type
type tabpage_operai from userobject within tab_1
end type
type dw_grafico_difetti_difetto_operaio from datawindow within tabpage_operai
end type
type tabpage_prodotti from userobject within tab_1
end type
type dw_grafico_difetti_difetto_prodotto from datawindow within tabpage_prodotti
end type
type tabpage_attrezzature from userobject within tab_1
end type
type dw_grafico_difetti_difetto_attrezzatura from datawindow within tabpage_attrezzature
end type
type st_descrizione from statictext within w_grafici_difetti_out_difetti
end type
type cb_1 from commandbutton within w_grafici_difetti_out_difetti
end type
type tabpage_operai from userobject within tab_1
dw_grafico_difetti_difetto_operaio dw_grafico_difetti_difetto_operaio
end type
type tabpage_prodotti from userobject within tab_1
dw_grafico_difetti_difetto_prodotto dw_grafico_difetti_difetto_prodotto
end type
type tabpage_attrezzature from userobject within tab_1
dw_grafico_difetti_difetto_attrezzatura dw_grafico_difetti_difetto_attrezzatura
end type
type tab_1 from tab within w_grafici_difetti_out_difetti
tabpage_operai tabpage_operai
tabpage_prodotti tabpage_prodotti
tabpage_attrezzature tabpage_attrezzature
end type
end forward

global type w_grafici_difetti_out_difetti from w_cs_xx_risposta
int Width=3393
int Height=1821
boolean TitleBar=true
string Title="Scarti per Difetto"
tab_1 tab_1
st_descrizione st_descrizione
cb_1 cb_1
end type
global w_grafici_difetti_out_difetti w_grafici_difetti_out_difetti

on w_grafici_difetti_out_difetti.create
int iCurrent
call w_cs_xx_risposta::create
this.tab_1=create tab_1
this.st_descrizione=create st_descrizione
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=tab_1
this.Control[iCurrent+2]=st_descrizione
this.Control[iCurrent+3]=cb_1
end on

on w_grafici_difetti_out_difetti.destroy
call w_cs_xx_risposta::destroy
destroy(this.tab_1)
destroy(this.st_descrizione)
destroy(this.cb_1)
end on

event open;call super::open;string ls_des

select des_difformita
into   :ls_des
from   tab_difformita
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_errore=:s_cs_xx.parametri.parametro_s_1;

st_descrizione.text = s_cs_xx.parametri.parametro_s_1 + "  " + ls_des

tab_1.tabpage_operai.dw_grafico_difetti_difetto_operaio.SetTransObject ( SQLCA )
tab_1.tabpage_operai.dw_grafico_difetti_difetto_operaio.Retrieve(s_cs_xx.cod_azienda, datetime(date(s_cs_xx.parametri.parametro_s_2),00:00:00),datetime(date(s_cs_xx.parametri.parametro_s_3),00:00:00),s_cs_xx.parametri.parametro_s_1) 
tab_1.tabpage_prodotti.dw_grafico_difetti_difetto_prodotto.SetTransObject ( SQLCA )
tab_1.tabpage_prodotti.dw_grafico_difetti_difetto_prodotto.Retrieve(s_cs_xx.cod_azienda, datetime(date(s_cs_xx.parametri.parametro_s_2),00:00:00),datetime(date(s_cs_xx.parametri.parametro_s_3),00:00:00),s_cs_xx.parametri.parametro_s_1) 
tab_1.tabpage_attrezzature.dw_grafico_difetti_difetto_attrezzatura.SetTransObject ( SQLCA )
tab_1.tabpage_attrezzature.dw_grafico_difetti_difetto_attrezzatura.Retrieve(s_cs_xx.cod_azienda, datetime(date(s_cs_xx.parametri.parametro_s_2),00:00:00),datetime(date(s_cs_xx.parametri.parametro_s_3),00:00:00),s_cs_xx.parametri.parametro_s_1) 
end event

type tab_1 from tab within w_grafici_difetti_out_difetti
int X=23
int Y=161
int Width=3315
int Height=1441
int TabOrder=10
boolean BringToTop=true
boolean RaggedRight=true
int SelectedTab=1
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
tabpage_operai tabpage_operai
tabpage_prodotti tabpage_prodotti
tabpage_attrezzature tabpage_attrezzature
end type

on tab_1.create
this.tabpage_operai=create tabpage_operai
this.tabpage_prodotti=create tabpage_prodotti
this.tabpage_attrezzature=create tabpage_attrezzature
this.Control[]={ this.tabpage_operai,&
this.tabpage_prodotti,&
this.tabpage_attrezzature}
end on

on tab_1.destroy
destroy(this.tabpage_operai)
destroy(this.tabpage_prodotti)
destroy(this.tabpage_attrezzature)
end on

type tabpage_operai from userobject within tab_1
int X=19
int Y=109
int Width=3278
int Height=1317
long BackColor=79741120
string Text="Operai"
long TabBackColor=79741120
long TabTextColor=8388608
long PictureMaskColor=536870912
dw_grafico_difetti_difetto_operaio dw_grafico_difetti_difetto_operaio
end type

on tabpage_operai.create
this.dw_grafico_difetti_difetto_operaio=create dw_grafico_difetti_difetto_operaio
this.Control[]={ this.dw_grafico_difetti_difetto_operaio}
end on

on tabpage_operai.destroy
destroy(this.dw_grafico_difetti_difetto_operaio)
end on

type dw_grafico_difetti_difetto_operaio from datawindow within tabpage_operai
int X=28
int Y=33
int Width=3223
int Height=1261
int TabOrder=2
boolean BringToTop=true
string DataObject="dw_grafico_difetti_difetto_operaio"
boolean LiveScroll=true
end type

type tabpage_prodotti from userobject within tab_1
int X=19
int Y=109
int Width=3278
int Height=1317
long BackColor=79741120
string Text="Prodotti"
long TabBackColor=79741120
long TabTextColor=8388608
long PictureMaskColor=536870912
dw_grafico_difetti_difetto_prodotto dw_grafico_difetti_difetto_prodotto
end type

on tabpage_prodotti.create
this.dw_grafico_difetti_difetto_prodotto=create dw_grafico_difetti_difetto_prodotto
this.Control[]={ this.dw_grafico_difetti_difetto_prodotto}
end on

on tabpage_prodotti.destroy
destroy(this.dw_grafico_difetti_difetto_prodotto)
end on

type dw_grafico_difetti_difetto_prodotto from datawindow within tabpage_prodotti
int X=28
int Y=33
int Width=3223
int Height=1261
int TabOrder=2
boolean BringToTop=true
string DataObject="dw_grafico_difetti_difetto_prodotto"
boolean LiveScroll=true
end type

type tabpage_attrezzature from userobject within tab_1
int X=19
int Y=109
int Width=3278
int Height=1317
long BackColor=79741120
string Text="Attrezzature"
long TabBackColor=79741120
long TabTextColor=8388608
long PictureMaskColor=536870912
dw_grafico_difetti_difetto_attrezzatura dw_grafico_difetti_difetto_attrezzatura
end type

on tabpage_attrezzature.create
this.dw_grafico_difetti_difetto_attrezzatura=create dw_grafico_difetti_difetto_attrezzatura
this.Control[]={ this.dw_grafico_difetti_difetto_attrezzatura}
end on

on tabpage_attrezzature.destroy
destroy(this.dw_grafico_difetti_difetto_attrezzatura)
end on

type dw_grafico_difetti_difetto_attrezzatura from datawindow within tabpage_attrezzature
int X=28
int Y=33
int Width=3223
int Height=1261
int TabOrder=2
boolean BringToTop=true
string DataObject="dw_grafico_difetti_difetto_attrezzatura"
boolean LiveScroll=true
end type

type st_descrizione from statictext within w_grafici_difetti_out_difetti
int X=23
int Y=41
int Width=3315
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Spazio per Descrizione "
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_1 from commandbutton within w_grafici_difetti_out_difetti
int X=2972
int Y=1621
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;close(parent)
end event

