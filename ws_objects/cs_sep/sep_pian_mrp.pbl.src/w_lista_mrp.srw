﻿$PBExportHeader$w_lista_mrp.srw
$PBExportComments$Window lista MRP
forward
global type w_lista_mrp from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_lista_mrp
end type
type dw_lista_mrp from uo_cs_xx_dw within w_lista_mrp
end type
type cb_calcola from commandbutton within w_lista_mrp
end type
type cbx_maggiore_zero from checkbox within w_lista_mrp
end type
type em_da_data from editmask within w_lista_mrp
end type
type em_a_data from editmask within w_lista_mrp
end type
type st_2 from statictext within w_lista_mrp
end type
type st_3 from statictext within w_lista_mrp
end type
end forward

global type w_lista_mrp from w_cs_xx_principale
integer width = 3369
integer height = 1584
string title = "Material Resource Planning"
cb_1 cb_1
dw_lista_mrp dw_lista_mrp
cb_calcola cb_calcola
cbx_maggiore_zero cbx_maggiore_zero
em_da_data em_da_data
em_a_data em_a_data
st_2 st_2
st_3 st_3
end type
global w_lista_mrp w_lista_mrp

forward prototypes
public function integer f_elimina_array (ref string fs_array[])
end prototypes

public function integer f_elimina_array (ref string fs_array[]);string ls_array[]

fs_array[]=ls_array

return 0
end function

on w_lista_mrp.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_lista_mrp=create dw_lista_mrp
this.cb_calcola=create cb_calcola
this.cbx_maggiore_zero=create cbx_maggiore_zero
this.em_da_data=create em_da_data
this.em_a_data=create em_a_data
this.st_2=create st_2
this.st_3=create st_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_lista_mrp
this.Control[iCurrent+3]=this.cb_calcola
this.Control[iCurrent+4]=this.cbx_maggiore_zero
this.Control[iCurrent+5]=this.em_da_data
this.Control[iCurrent+6]=this.em_a_data
this.Control[iCurrent+7]=this.st_2
this.Control[iCurrent+8]=this.st_3
end on

on w_lista_mrp.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_lista_mrp)
destroy(this.cb_calcola)
destroy(this.cbx_maggiore_zero)
destroy(this.em_da_data)
destroy(this.em_a_data)
destroy(this.st_2)
destroy(this.st_3)
end on

event pc_setwindow;call super::pc_setwindow;
save_on_close(c_socnosave)


dw_lista_mrp.set_dw_options(sqlca, &
                        	 pcca.null_object, &
                            c_multiselect + &
									 c_nonew + &
									 c_nodelete + &
									 c_disablecc + &
									 c_noretrieveonopen + &
									 c_disableccinsert , &
                            c_viewmodeblack)
													
													
em_da_data.text = string(today())
em_a_data.text=string(RelativeDate(today(), 30))
end event

type cb_1 from commandbutton within w_lista_mrp
integer x = 2949
integer y = 1384
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Calcola"
end type

event clicked;string   ls_cod_prodotto[],ls_cod_prodotto_ven,ls_cod_prodotto_acq,ls_cod_prodotto_com, &
		   ls_materie_prime[],ls_test,ls_des_prodotto,ls_flag_sotto_scorta,ls_cod_versione
double   ldd_disponibilita[],ldd_quan_ordine_ven[],ldd_quan_ordine_acq[],ldd_quan_commesse[], & 
		   ldd_quan_ordine_v,ldd_quan_ordine_a,ldd_quan_c,ldd_disp,ldd_quan_utilizzo[], &
		   ldd_quan_ordinare[],ldd_scorta_minima,ldd_quan_ripristino
long     ll_i,ll_t,ll_mp,ll_num_commessa,ll_num_commessa_ordine
boolean  lb_flag_trovato
integer  li_risposta,li_anno_commessa,li_anno_commessa_ordine
datetime ldt_oggi,ldt_da_data,ldt_a_data

ldt_a_data = datetime(date(em_a_data.text))
ldt_da_data = datetime(date(em_da_data.text))

setnull(li_anno_commessa)
setnull(li_anno_commessa_ordine)
setnull(ll_num_commessa)
setnull(ll_num_commessa_ordine)

setpointer(hourglass!)
dw_lista_mrp.reset()

ldt_oggi = datetime(today())

declare righe_det_ord_ven_1 cursor for
select  cod_prodotto,
		  quan_ordine - quan_in_evasione - quan_evasa,
		  anno_commessa,
		  num_commessa,
		  cod_versione
from    det_ord_ven
where   cod_azienda =: s_cs_xx.cod_azienda
and     flag_blocco = 'N'
and     flag_evasione = 'A'
and     data_consegna between :ldt_da_data and :ldt_a_data;

open righe_det_ord_ven_1;

do while 1=1
	fetch righe_det_ord_ven_1 
	into  :ls_cod_prodotto_ven,
			:ldd_quan_ordine_v,	
			:li_anno_commessa_ordine,
			:ll_num_commessa_ordine,
			:ls_cod_versione;
			
	if sqlca.sqlcode<>0 then 
		choose case sqlca.sqlcode
			case 100
				exit

			case is <0
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
				exit
		end choose
	end if
	
	select cod_azienda
	into   :ls_test
	from   det_anag_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa_ordine
	and    num_commessa=:ll_num_commessa_ordine;

	if sqlca.sqlcode = 0 then continue //possibilita di leggere la quan_assegnata nella commessa ed elaborarla

	if isnull(ls_cod_prodotto_ven) then continue
	
	if isnull(ldd_quan_ordine_v) then ldd_quan_ordine_v = 0
	
	lb_flag_trovato = false
	
	for ll_t = 1 to upperbound(ls_cod_prodotto[])
		 if ls_cod_prodotto[ll_t] = ls_cod_prodotto_ven then
		    lb_flag_trovato = true
			 ldd_quan_ordine_ven[ll_t] = ldd_quan_ordine_ven[ll_t] + ldd_quan_ordine_v
			 exit
		 end if
	next
	
	if lb_flag_trovato=false then
		ll_i=upperbound(ls_cod_prodotto[])
		ll_i++
		
		select saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita & 
				 - quan_impegnata - quan_assegnata - quan_anticipi - quan_in_spedizione
		into   :ldd_disp
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto_ven;
		
		if isnull(ldd_disp) then ldd_disp = 0
		ls_cod_prodotto[ll_i]=ls_cod_prodotto_ven
		ldd_disponibilita[ll_i]=ldd_disp
		ldd_quan_ordine_ven[ll_i]=ldd_quan_ordine_v
		ldd_quan_ordine_acq[ll_i]=0
		ldd_quan_commesse[ll_i]=0
		ldd_quan_ordinare[ll_i]=0
	end if
	
	f_elimina_array(ls_materie_prime[])

	li_risposta = f_trova_mat_prima(ls_cod_prodotto_ven, ls_cod_versione,ls_materie_prime[], & 
											  ldd_quan_utilizzo[],ldd_quan_ordine_v,li_anno_commessa_ordine,ll_num_commessa_ordine)
		
	for ll_mp = 1 to upperbound(ls_materie_prime[])
		lb_flag_trovato = false
	
		for ll_t = 1 to upperbound(ls_cod_prodotto[])
			 if ls_cod_prodotto[ll_t] = ls_materie_prime[ll_mp] then
			    lb_flag_trovato = true
				 ldd_quan_ordine_ven[ll_t]=ldd_quan_ordine_ven[ll_t] + ldd_quan_utilizzo[ll_mp]
				 exit
			 end if
		next
	
		if lb_flag_trovato=false then
			ll_i=upperbound(ls_cod_prodotto[])
			ll_i++
		
			select saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita & 
					 - quan_impegnata - quan_assegnata - quan_anticipi - quan_in_spedizione
			into   :ldd_disp
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_materie_prime[ll_mp];

			if isnull(ldd_disp) then ldd_disp = 0
			
			ls_cod_prodotto[ll_i]=ls_materie_prime[ll_mp]
			ldd_disponibilita[ll_i]=ldd_disp
			ldd_quan_ordine_ven[ll_i]=ldd_quan_utilizzo[ll_mp]	
			ldd_quan_ordine_acq[ll_i] = 0
			ldd_quan_commesse[ll_i] = 0
			ldd_quan_ordinare[ll_i] = 0
			
		end if
	next
loop

close righe_det_ord_ven_1;

declare righe_det_ord_acq_1 cursor for
select  cod_prodotto,
		  quan_ordinata - quan_arrivata
from    det_ord_acq
where   cod_azienda=:s_cs_xx.cod_azienda
and     flag_blocco='N'
and     flag_saldo='N'
and     data_consegna between :ldt_da_data and :ldt_a_data;

open righe_det_ord_acq_1;

do while 1=1
	fetch righe_det_ord_acq_1 
	into  :ls_cod_prodotto_acq,
			:ldd_quan_ordine_a;
			
	if sqlca.sqlcode<>0 then 
		choose case sqlca.sqlcode
			case 100
				exit

			case is <0
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
				exit
				
		end choose
	end if
	
	lb_flag_trovato = false
	if isnull(ldd_quan_ordine_a) then ldd_quan_ordine_a = 0
	
	for ll_t = 1 to upperbound(ls_cod_prodotto[])
		 if ls_cod_prodotto[ll_t] = ls_cod_prodotto_ven then
		    lb_flag_trovato = true
			 ldd_quan_ordine_acq[ll_t] = ldd_quan_ordine_acq[ll_t] + ldd_quan_ordine_a
			 exit
		 end if
	next
	
	if lb_flag_trovato=false then
		ll_i=upperbound(ls_cod_prodotto[])
		ll_i++
		
		select saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita & 
				 - quan_impegnata - quan_assegnata - quan_anticipi - quan_in_spedizione
		into   :ldd_disp
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto_acq;

		if isnull(ldd_disp) then ldd_disp = 0
		
		ls_cod_prodotto[ll_i] = ls_cod_prodotto_acq
		ldd_disponibilita[ll_i]=ldd_disp
		ldd_quan_ordine_acq[ll_i]=ldd_quan_ordine_a
		ldd_quan_commesse[ll_i]=0
		ldd_quan_ordine_ven[ll_i]=0
		ldd_quan_ordinare[ll_i] = 0
		
	end if
	
loop

close righe_det_ord_acq_1;

declare righe_com_1 cursor for
select  cod_prodotto,
		  cod_versione,
		  quan_ordine - quan_in_produzione - quan_assegnata - quan_prodotta,
		  anno_commessa,
		  num_commessa
from    anag_commesse
where   cod_azienda=:s_cs_xx.cod_azienda
and     quan_ordine - quan_in_produzione - quan_assegnata - quan_prodotta > 0
and     data_consegna between :ldt_da_data and :ldt_a_data;

open righe_com_1;

do while 1=1
	fetch righe_com_1 
	into  :ls_cod_prodotto_com,
			:ls_cod_versione,
			:ldd_quan_c,
			:li_anno_commessa,
			:ll_num_commessa;
			
	if sqlca.sqlcode<>0 then 
		choose case sqlca.sqlcode
			case 100
				exit

			case is <0
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
				exit
				
		end choose
	end if
	
	if isnull(ldd_quan_c) then ldd_quan_c = 0
	
	select cod_azienda
	into   :ls_test
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;
	
	if sqlca.sqlcode = 0 then continue
	
	lb_flag_trovato = false
	
	for ll_t = 1 to upperbound(ls_cod_prodotto[])
		 if ls_cod_prodotto[ll_t] = ls_cod_prodotto_com then
		    lb_flag_trovato = true
			 ldd_quan_commesse[ll_t] = ldd_quan_commesse[ll_t] + ldd_quan_c
			 exit
		 end if
	next
	
	if lb_flag_trovato=false then
		ll_i=upperbound(ls_cod_prodotto[])
		ll_i++
		
		select saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita & 
				 - quan_impegnata - quan_assegnata - quan_anticipi - quan_in_spedizione
		into   :ldd_disp
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto_com;

		if isnull(ldd_disp) then ldd_disp = 0
		ls_cod_prodotto[ll_i]=ls_cod_prodotto_com
		ldd_disponibilita[ll_i]=ldd_disp
		ldd_quan_commesse[ll_i]=ldd_quan_c
		ldd_quan_ordine_ven[ll_i]=0
		ldd_quan_ordine_acq[ll_i]=0
		ldd_quan_ordinare[ll_i]=0
		
	end if
	
	f_elimina_array(ls_materie_prime[])
	
	li_risposta = f_trova_mat_prima(ls_cod_prodotto_com, ls_cod_versione,ls_materie_prime[], & 
											  ldd_quan_utilizzo[],ldd_quan_c,li_anno_commessa,ll_num_commessa)
		
	for ll_mp = 1 to upperbound(ls_materie_prime[])
		lb_flag_trovato = false
	
		for ll_t = 1 to upperbound(ls_cod_prodotto[])
			 if ls_cod_prodotto[ll_t] = ls_materie_prime[ll_mp] then
			    lb_flag_trovato = true
				 ldd_quan_commesse[ll_t]=ldd_quan_commesse[ll_t]+ldd_quan_utilizzo[ll_mp]
				 exit
			 end if
		next
	
		if lb_flag_trovato=false then
			ll_i=upperbound(ls_cod_prodotto[])
			ll_i++
		
			select saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita & 
					 - quan_impegnata - quan_assegnata - quan_anticipi - quan_in_spedizione
			into   :ldd_disp
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_materie_prime[ll_mp];

			if isnull(ldd_disp) then ldd_disp=0
			ls_cod_prodotto[ll_i]=ls_materie_prime[ll_mp]
			ldd_disponibilita[ll_i]=ldd_disp
			ldd_quan_commesse[ll_i]=ldd_quan_utilizzo[ll_mp]
			ldd_quan_ordine_ven[ll_i]=0
			ldd_quan_ordine_acq[ll_i] = 0
			ldd_quan_ordinare[ll_i] = 0

		end if
	next
loop

close righe_com_1;

ll_i = 1

for ll_t = 1 to upperbound(ls_cod_prodotto[])
	ldd_quan_ordinare[ll_t] = -ldd_disponibilita[ll_t] + ldd_quan_ordine_ven[ll_t] - ldd_quan_ordine_acq[ll_t] + ldd_quan_commesse[ll_t]
	if cbx_maggiore_zero.checked = true  and ldd_quan_ordinare[ll_t] <= 0 then continue
	
	dw_lista_mrp.insertrow(0)
	dw_lista_mrp.setitem(ll_i,"cod_prodotto",ls_cod_prodotto[ll_t])

	select des_prodotto,
			 scorta_minima
	into   :ls_des_prodotto,
			 :ldd_scorta_minima
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto[ll_t];
	
	ldd_quan_ripristino = ldd_quan_ordinare[ll_t] + ldd_scorta_minima
	
	dw_lista_mrp.setitem(ll_i,"des_prodotto",ls_des_prodotto)
	dw_lista_mrp.setitem(ll_i,"quan_disponibile",ldd_disponibilita[ll_t])
	dw_lista_mrp.setitem(ll_i,"quan_ordine_cliente",ldd_quan_ordine_ven[ll_t])
	dw_lista_mrp.setitem(ll_i,"quan_ordine_fornitore",ldd_quan_ordine_acq[ll_t])
	dw_lista_mrp.setitem(ll_i,"quan_ordine_commessa",ldd_quan_commesse[ll_t])
	dw_lista_mrp.setitem(ll_i,"quan_ordinare",ldd_quan_ordinare[ll_t])
	dw_lista_mrp.setitem(ll_i,"quan_ripristino",ldd_quan_ripristino)
	ll_i ++
next

dw_lista_mrp.resetupdate()			

setpointer(arrow!)
end event

type dw_lista_mrp from uo_cs_xx_dw within w_lista_mrp
integer y = 8
integer width = 3310
integer height = 1276
integer taborder = 10
string dataobject = "d_lista_mrp"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type cb_calcola from commandbutton within w_lista_mrp
integer x = 2555
integer y = 1384
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen.Ordini"
end type

event clicked;string ls_prodotti[], ls_messaggio
dec{4} ld_quan_ordine[]
long ll_i, ll_righe, ll_y, ll_anno_ordine[], ll_num_ordine[]
uo_generazione_documenti uo_gen_doc


if g_mb.messagebox("APICE","Procedo con la generazione automatica ordini di acquisto ?",question!, yesno!,2) = 2 then return

ll_righe = dw_lista_mrp.rowcount()
if ll_righe < 1 then
	g_mb.messagebox("APICE","Nulla da ordinare")
	return
end if

ll_y = 0

for ll_i = 1 to ll_righe
	if dw_lista_mrp.getitemstring(ll_i, "flag_eseguito") = 'S' then
		ll_y ++
		ls_prodotti[ll_y] = dw_lista_mrp.getitemstring(ll_i, "cod_prodotto")
		ld_quan_ordine[ll_y] = dw_lista_mrp.getitemnumber(ll_i, "quan_ripristino")
	end if
next

if ll_y < 1 then
	g_mb.messagebox("APICE","Nulla da ordinare")
	return
end if

//uo_gen_doc = Create uo_generazione_documenti 
//
//if uo_gen_doc.uof_genera_ordini_fornitori(ls_prodotti, &
//													   ld_quan_ordine, &
//													   ref ll_anno_ordine, &
//													   ref ll_num_ordine, &
//													   ref ls_messaggio) <> 0 then
//
//	rollback;
//	
//	messagebox("APICE",ls_messaggio)
//	
//else
//	
//	commit;
//	
//end if

destroy uo_gen_doc

dw_lista_mrp.reset()

g_mb.messagebox("APICE","Generazione Ordini Eseguita con Successo !")

return
end event

type cbx_maggiore_zero from checkbox within w_lista_mrp
integer x = 23
integer y = 1384
integer width = 1435
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza solo prodotti di cui ripristinare la quantità"
end type

type em_da_data from editmask within w_lista_mrp
integer x = 297
integer y = 1300
integer width = 389
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

type em_a_data from editmask within w_lista_mrp
integer x = 1029
integer y = 1300
integer width = 411
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "À"
end type

type st_2 from statictext within w_lista_mrp
integer x = 23
integer y = 1300
integer width = 247
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Da data:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_lista_mrp
integer x = 754
integer y = 1300
integer width = 247
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "A data:"
alignment alignment = right!
boolean focusrectangle = false
end type

