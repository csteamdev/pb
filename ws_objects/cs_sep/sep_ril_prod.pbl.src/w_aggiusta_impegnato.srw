﻿$PBExportHeader$w_aggiusta_impegnato.srw
forward
global type w_aggiusta_impegnato from window
end type
type cb_4 from commandbutton within w_aggiusta_impegnato
end type
type cb_3 from commandbutton within w_aggiusta_impegnato
end type
type dw_impegnato from datawindow within w_aggiusta_impegnato
end type
type cb_2 from commandbutton within w_aggiusta_impegnato
end type
type st_prodotto_versione from statictext within w_aggiusta_impegnato
end type
type st_3 from statictext within w_aggiusta_impegnato
end type
type cb_1 from commandbutton within w_aggiusta_impegnato
end type
type em_numero from editmask within w_aggiusta_impegnato
end type
type em_anno from editmask within w_aggiusta_impegnato
end type
type st_2 from statictext within w_aggiusta_impegnato
end type
type st_1 from statictext within w_aggiusta_impegnato
end type
end forward

global type w_aggiusta_impegnato from window
integer width = 3168
integer height = 1932
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_4 cb_4
cb_3 cb_3
dw_impegnato dw_impegnato
cb_2 cb_2
st_prodotto_versione st_prodotto_versione
st_3 st_3
cb_1 cb_1
em_numero em_numero
em_anno em_anno
st_2 st_2
st_1 st_1
end type
global w_aggiusta_impegnato w_aggiusta_impegnato

type variables
long	     il_anno, il_numero
string	  is_cod_mat_prima[]
decimal{4} id_utilizzo[]
end variables

forward prototypes
public function integer wf_carica_commessa (string fs_cod_prodotto, string fs_cod_versione)
public function integer wf_imposta_tv (string fs_cod_prodotto, string fs_cod_versione)
end prototypes

public function integer wf_carica_commessa (string fs_cod_prodotto, string fs_cod_versione);string	ls_vuoto[]
dec{4}   ld_vuoto[]

is_cod_mat_prima = ls_vuoto
id_utilizzo = ld_vuoto

wf_imposta_tv( fs_cod_prodotto, fs_cod_versione)

return 0
end function

public function integer wf_imposta_tv (string fs_cod_prodotto, string fs_cod_versione);string  ls_flag_ramo_descrittivo, ls_cod_prodotto_figlio,ls_test_prodotto_f, ls_errore,ls_des_prodotto,/*ls_cod_versione,*/ls_cod_versione_figlio,ls_flag_materia_prima,ls_cod_misura_mag
long    ll_num_figli,ll_handle,ll_num_righe
long	  li_num_priorita,li_risposta, ll_indice, ll_appo
dec{4}  ld_quan_utilizzo
boolean lb_trovato

datastore lds_righe_distinta

ll_num_figli = 1
lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)

lds_righe_distinta.setsort("num_sequenza A, cod_prodotto_figlio A")

ll_num_figli = lds_righe_distinta.sort()
		

for ll_num_figli = 1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ld_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	ls_flag_ramo_descrittivo = lds_righe_distinta.getitemstring(ll_num_figli,"distinta_flag_ramo_descrittivo")
	
	if left(ls_cod_prodotto_figlio,5) = "54N16" or ls_cod_prodotto_figlio = "03215013" then
		string ls_ciao 
		ls_ciao = "ciao"
	end if
	
	if ls_flag_materia_prima = 'N' then
		
		setnull(ls_test_prodotto_f)
		
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and	 
		       cod_prodotto_padre = :ls_cod_prodotto_figlio and    
				 cod_versione = :ls_cod_versione_figlio;

		if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
			if ls_flag_ramo_descrittivo = "S" then

			else
				lb_trovato = false
				
				for ll_appo = 1 to upperbound(is_cod_mat_prima)
					if is_cod_mat_prima[ll_appo] = ls_cod_prodotto_figlio then
						id_utilizzo[ll_appo] += ld_quan_utilizzo
						lb_trovato = true
						exit
					end if
				next
				
				if not lb_trovato then
					ll_indice = upperbound(is_cod_mat_prima)
					ll_indice ++
					is_cod_mat_prima[ll_indice] = ls_cod_prodotto_figlio
					id_utilizzo[ll_indice] = ld_quan_utilizzo
				end if
				
			end if			
			continue
		else
			
			lb_trovato = false
			
			for ll_appo = 1 to upperbound(is_cod_mat_prima)
				if is_cod_mat_prima[ll_appo] = ls_cod_prodotto_figlio then
					id_utilizzo[ll_appo] += ld_quan_utilizzo
					lb_trovato = true
					exit
				end if
			next
			
			if not lb_trovato then
				ll_indice = upperbound(is_cod_mat_prima)
				ll_indice ++
				is_cod_mat_prima[ll_indice] = ls_cod_prodotto_figlio
				id_utilizzo[ll_indice] = ld_quan_utilizzo
			end if			
			
			wf_imposta_tv( ls_cod_prodotto_figlio, ls_cod_versione_figlio)
		end if
	else	
		lb_trovato = false		
		for ll_appo = 1 to upperbound(is_cod_mat_prima)
			if is_cod_mat_prima[ll_appo] = ls_cod_prodotto_figlio then
				id_utilizzo[ll_appo] += ld_quan_utilizzo
				lb_trovato = true
				exit
			end if
		next
		
		if not lb_trovato then
			ll_indice = upperbound(is_cod_mat_prima)
			ll_indice ++
			is_cod_mat_prima[ll_indice] = ls_cod_prodotto_figlio
			id_utilizzo[ll_indice] = ld_quan_utilizzo
		end if	
	end if
next

destroy(lds_righe_distinta)

return 0
end function

on w_aggiusta_impegnato.create
this.cb_4=create cb_4
this.cb_3=create cb_3
this.dw_impegnato=create dw_impegnato
this.cb_2=create cb_2
this.st_prodotto_versione=create st_prodotto_versione
this.st_3=create st_3
this.cb_1=create cb_1
this.em_numero=create em_numero
this.em_anno=create em_anno
this.st_2=create st_2
this.st_1=create st_1
this.Control[]={this.cb_4,&
this.cb_3,&
this.dw_impegnato,&
this.cb_2,&
this.st_prodotto_versione,&
this.st_3,&
this.cb_1,&
this.em_numero,&
this.em_anno,&
this.st_2,&
this.st_1}
end on

on w_aggiusta_impegnato.destroy
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.dw_impegnato)
destroy(this.cb_2)
destroy(this.st_prodotto_versione)
destroy(this.st_3)
destroy(this.cb_1)
destroy(this.em_numero)
destroy(this.em_anno)
destroy(this.st_2)
destroy(this.st_1)
end on

type cb_4 from commandbutton within w_aggiusta_impegnato
integer x = 663
integer y = 1700
integer width = 571
integer height = 100
integer taborder = 30
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "STAMPA Distinta"
end type

event clicked;datastore lds_appo
long	ll_i, ll_riga

lds_appo = create datastore
lds_appo.dataobject = "d_aggiusta_impegnato_commessa"
lds_appo.settransobject( sqlca)

for ll_i = 1 to upperbound( is_cod_mat_prima)
	ll_riga = lds_appo.insertrow(0)
	
	lds_appo.setitem( ll_riga, "cod_prodotto", is_cod_mat_prima[ll_i])
	lds_appo.setitem( ll_riga, "quan_impegnata_attuale", id_utilizzo[ll_i])
	
next
lds_appo.setsort( "cod_prodotto ASC")
lds_appo.sort()

lds_appo.Object.DataWindow.Zoom = 60
lds_appo.print()
destroy lds_appo
end event

type cb_3 from commandbutton within w_aggiusta_impegnato
integer x = 69
integer y = 1700
integer width = 571
integer height = 100
integer taborder = 30
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "STAMPA IMPEGNATO"
end type

event clicked;dw_impegnato.Object.DataWindow.Zoom = 60
dw_impegnato.print()
end event

type dw_impegnato from datawindow within w_aggiusta_impegnato
integer x = 69
integer y = 360
integer width = 2971
integer height = 1320
integer taborder = 30
string title = "none"
string dataobject = "d_aggiusta_impegnato_commessa"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_2 from commandbutton within w_aggiusta_impegnato
integer x = 2149
integer y = 1700
integer width = 887
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "AGGIORNA IMPEGNATO"
end type

event clicked;long	ll_i, ll_j
string	ls_cod_prodotto
dec{4}   ld_imp_inizio, ld_imp_attuale,ld_residuo

for ll_i = 1 to dw_impegnato.rowcount()
	
	ls_cod_prodotto = dw_impegnato.getitemstring( ll_i, "cod_prodotto")
	ld_imp_inizio = dw_impegnato.getitemnumber( ll_i, "quan_impegnata_iniziale")
	ld_imp_attuale = dw_impegnato.getitemnumber( ll_i, "quan_impegnata_attuale")
	
	for ll_j = 1 to upperbound( is_cod_mat_prima)
		if ls_cod_prodotto = is_cod_mat_prima[ll_j] then
			
			if isnull(ld_imp_inizio) then ld_imp_inizio = 0 
			if isnull(ld_imp_attuale) then ld_imp_attuale = 0
			if isnull(id_utilizzo[ll_j]) then id_utilizzo[ll_j] = 0
			
			if ld_imp_inizio > id_utilizzo[ll_j] then
				
				dw_impegnato.setitem( ll_i, "quan_impegnata_iniziale", id_utilizzo[ll_j])
				
				ld_residuo = ld_imp_attuale - ( ld_imp_inizio - id_utilizzo[ll_j])
				if ld_residuo < 0 then ld_residuo = 0
				
				dw_impegnato.setitem( ll_i, "quan_impegnata_attuale", ld_residuo)
				
			end if
			exit
			
		end if
	next
	
next

ll_i = dw_impegnato.Update()

IF ll_i = 1 THEN
	COMMIT USING SQLCA;
	messagebox("", "Tutto ok")
	dw_impegnato.retrieve( s_cs_xx.cod_azienda, il_anno, il_numero)
ELSE
	messagebox("", "Errore:" + sqlca.sqlerrtext)
    ROLLBACK USING SQLCA;
END IF
end event

type st_prodotto_versione from statictext within w_aggiusta_impegnato
integer x = 869
integer y = 280
integer width = 2171
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_3 from statictext within w_aggiusta_impegnato
integer x = 69
integer y = 280
integer width = 777
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Prodotto finito / versione:"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_aggiusta_impegnato
integer x = 1303
integer y = 140
integer width = 887
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "VISUALIZZA DATI COMMESSA"
end type

event clicked;long	ll_anno, ll_numero
string	ls_cod_prodotto_finito, ls_cod_versione

ll_anno = long(em_anno.text)
ll_numero = long( em_numero.text)

select cod_prodotto,   
       cod_versione  
into	 :ls_cod_prodotto_finito,   
       :ls_cod_versione  
from	 anag_commesse  
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno and
		 num_commessa = :ll_numero;
		 
if sqlca.sqlcode = 0 then
	st_prodotto_versione.text = ls_cod_prodotto_finito + "/" + ls_cod_versione
	il_anno = ll_anno 
	il_numero = ll_numero
	dw_impegnato.settransobject( sqlca)
	dw_impegnato.retrieve( s_cs_xx.cod_azienda, il_anno, il_numero)
	wf_carica_commessa( ls_cod_prodotto_finito, ls_cod_versione)
else
	st_prodotto_versione.text = sqlca.sqlerrtext
	il_anno = 0
	il_numero = 0
end if

end event

type em_numero from editmask within w_aggiusta_impegnato
integer x = 731
integer y = 140
integer width = 471
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_anno from editmask within w_aggiusta_impegnato
integer x = 731
integer y = 40
integer width = 471
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "0"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type st_2 from statictext within w_aggiusta_impegnato
integer x = 69
integer y = 160
integer width = 617
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Numero Commessa:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_aggiusta_impegnato
integer x = 69
integer y = 60
integer width = 617
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Anno Commessa:"
boolean focusrectangle = false
end type

