﻿$PBExportHeader$w_attiva.srw
$PBExportComments$Window per input quan da attivare
forward
global type w_attiva from w_cs_xx_risposta
end type
type cb_ok from commandbutton within w_attiva
end type
type em_quan_da_produrre from editmask within w_attiva
end type
type st_quantita from statictext within w_attiva
end type
type cb_annulla from commandbutton within w_attiva
end type
end forward

global type w_attiva from w_cs_xx_risposta
integer width = 2432
integer height = 1056
string title = "Attivazione"
boolean controlmenu = false
cb_ok cb_ok
em_quan_da_produrre em_quan_da_produrre
st_quantita st_quantita
cb_annulla cb_annulla
end type
global w_attiva w_attiva

type variables
integer ii_ok
end variables

on w_attiva.create
int iCurrent
call super::create
this.cb_ok=create cb_ok
this.em_quan_da_produrre=create em_quan_da_produrre
this.st_quantita=create st_quantita
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ok
this.Control[iCurrent+2]=this.em_quan_da_produrre
this.Control[iCurrent+3]=this.st_quantita
this.Control[iCurrent+4]=this.cb_annulla
end on

on w_attiva.destroy
call super::destroy
destroy(this.cb_ok)
destroy(this.em_quan_da_produrre)
destroy(this.st_quantita)
destroy(this.cb_annulla)
end on

event open;call super::open;s_cs_xx_parametri				lstr_parametri


lstr_parametri = message.powerobjectparm

if lstr_parametri.parametro_s_1<>"" and not isnull(lstr_parametri.parametro_s_1) then
	//cambio label
	st_quantita.text = lstr_parametri.parametro_s_1 + ":"
	this.title = lstr_parametri.parametro_s_1
end if


ii_ok=-1
em_quan_da_produrre.text=string(lstr_parametri.parametro_d_2)
em_quan_da_produrre.setfocus()
end event

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

type cb_ok from commandbutton within w_attiva
integer x = 439
integer y = 624
integer width = 677
integer height = 264
integer taborder = 20
boolean bringtotop = true
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
boolean default = true
end type

event clicked;s_cs_xx_parametri				lstr_parametri



lstr_parametri.parametro_d_1 = double(em_quan_da_produrre.text)
lstr_parametri.parametro_i_1 = 1

ii_ok=0

closewithreturn(parent, lstr_parametri)

end event

type em_quan_da_produrre from editmask within w_attiva
integer x = 626
integer y = 284
integer width = 1061
integer height = 240
integer taborder = 30
boolean bringtotop = true
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "########.####"
string displaydata = "~r"
end type

type st_quantita from statictext within w_attiva
integer x = 82
integer y = 68
integer width = 2208
integer height = 152
boolean bringtotop = true
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Quantità da produrre:"
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_attiva
event clicked pbm_bnclicked
integer x = 1294
integer y = 624
integer width = 677
integer height = 264
integer taborder = 10
boolean bringtotop = true
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;s_cs_xx_parametri				lstr_parametri


lstr_parametri.parametro_i_1 = 0
ii_ok = 0

closewithreturn(parent, lstr_parametri)
end event

