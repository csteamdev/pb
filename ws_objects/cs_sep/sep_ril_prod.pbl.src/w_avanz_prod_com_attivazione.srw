﻿$PBExportHeader$w_avanz_prod_com_attivazione.srw
$PBExportComments$Window Avanzamento Produzione con Attivazione
forward
global type w_avanz_prod_com_attivazione from w_cs_xx_principale
end type
type cb_blocco from commandbutton within w_avanz_prod_com_attivazione
end type
type cbx_flag_ramo_descrittivo from checkbox within w_avanz_prod_com_attivazione
end type
type cbx_vis_mp from checkbox within w_avanz_prod_com_attivazione
end type
type dw_selezione from datawindow within w_avanz_prod_com_attivazione
end type
type dw_avanzamento_produzione_commesse from uo_cs_xx_dw within w_avanz_prod_com_attivazione
end type
type tv_db from treeview within w_avanz_prod_com_attivazione
end type
type cb_fine_fase from commandbutton within w_avanz_prod_com_attivazione
end type
type cb_inizio from commandbutton within w_avanz_prod_com_attivazione
end type
type cb_fine from commandbutton within w_avanz_prod_com_attivazione
end type
type cb_esterna_interna from commandbutton within w_avanz_prod_com_attivazione
end type
type cb_genera_bolle_out from commandbutton within w_avanz_prod_com_attivazione
end type
type cb_carica_bolle_in from commandbutton within w_avanz_prod_com_attivazione
end type
end forward

global type w_avanz_prod_com_attivazione from w_cs_xx_principale
integer width = 4590
integer height = 2708
string title = "Fasi Lavorazione Per Commessa (Attivazione)"
event ue_det_orari ( )
event ue_fine_fase ( )
event ue_inizio_sessione ( )
event ue_fine_sessione ( )
event ue_gen_bolla_uscita ( )
event ue_carico_acquisti ( )
event ue_stampa_ordine_fase ( )
event ue_elimina_det_orari ( )
event ue_controllo_giacenze ( )
event ue_fine_sessione_automatico ( )
event ue_invianuovoterzista ( )
cb_blocco cb_blocco
cbx_flag_ramo_descrittivo cbx_flag_ramo_descrittivo
cbx_vis_mp cbx_vis_mp
dw_selezione dw_selezione
dw_avanzamento_produzione_commesse dw_avanzamento_produzione_commesse
tv_db tv_db
cb_fine_fase cb_fine_fase
cb_inizio cb_inizio
cb_fine cb_fine
cb_esterna_interna cb_esterna_interna
cb_genera_bolle_out cb_genera_bolle_out
cb_carica_bolle_in cb_carica_bolle_in
end type
global w_avanz_prod_com_attivazione w_avanz_prod_com_attivazione

type variables
boolean ib_trovato, ib_disimpegnato = false
string is_cod_prodotto_finito,is_cod_versione_pf
long   il_modifica,il_ganne,il_handle
s_chiave_distinta is_chiave_distinta
uo_magazzino iuo_magazzino

string	is_cod_prodotto_prec, is_cod_versione_prec, is_cod_reparto_prec, is_cod_lavorazione_prec

end variables

forward prototypes
public function integer wf_inizio ()
public subroutine wf_calcola_stato_handle (long fl_handle)
public function integer wf_controllo_stock (long fl_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, decimal fd_quan_necessaria, ref string fs_messaggio)
public function integer wf_controllo_giacenze_totale (string fs_cod_prodotto_padre, string fs_cod_versione_padre, long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga, ref datastore fds_elenco_prodotti, ref string fs_messaggio)
public subroutine wf_verifica_fasi_start ()
public function integer wf_verifica_fasi (long al_handle, ref boolean ab_trovato_nuovo_componente)
public function integer wf_controlla_fasi_sl_ricorsivo (long al_handle, long fl_handle_primo_livello[], ref boolean ab_tutte_chiuse)
public function boolean wf_controlla_fasi_sl ()
public function double wf_controlla_qta_pf ()
public function integer wf_mov_mag_chiusura_fasi_parziale (long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga, double fd_qta_pf, string fs_errore)
public function integer wf_carico_parziale_pf (double fd_quan_pf, integer fl_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, long fl_prog_riga, string fs_cod_reparto, string fs_cod_lavorazione, string fs_flag_fine_fase, string fs_cod_operaio, integer fi_interfaccia, ref double fdd_quan_prodotta, ref double fdd_quan_in_produzione, ref integer fi_flag_ultima, integer fi_step, ref string fs_errore)
public function integer wf_controlla_fase_prodotto_finito (integer ai_anno_commessa, long al_num_commessa, long al_prog_riga, ref s_chiave_distinta astr_chiave_distinta, ref integer ai_pictureindex)
end prototypes

event ue_det_orari();window_open_parm(w_det_orari_produzione,-1,dw_avanzamento_produzione_commesse)
end event

event ue_fine_fase();cb_fine_fase.postevent("clicked")
end event

event ue_inizio_sessione();cb_inizio.postevent("clicked")
end event

event ue_fine_sessione();cb_fine.postevent("clicked")
end event

event ue_gen_bolla_uscita();cb_genera_bolle_out.postevent("clicked")


end event

event ue_carico_acquisti();cb_carica_bolle_in.postevent("clicked")

end event

event ue_stampa_ordine_fase();// stampa ordine di lavoro della fase corrente.
window_open_parm(w_report_ordine_lavoro,-1,dw_avanzamento_produzione_commesse)

end event

event ue_elimina_det_orari();integer				li_anno_commessa,li_risposta,li_anno_registrazione

long					ll_num_commessa,ll_prog_riga,ll_t,ll_prog,ll_num_registrazione,ll_prog_orari,ll_num_fasi_aperte, &
						ll_anno_reg_sl,ll_num_reg_sl
						
string					ls_test,ls_cod_prodotto,ls_cod_prodotto_finito,ls_cod_reparto,ls_cod_lavorazione, ls_cod_versione_figlio, ls_cod_versione_finito, ls_cod_versione_variante, & 
						ls_cod_versione,ls_cod_prodotto_figlio,ls_cod_prodotto_variante,ls_cod_prodotto_padre, & 
						ls_flag_materia_prima, ls_flag_materia_prima_variante,ls_cod_prodotto_2[]
						
double 				ldd_quan_in_produzione,ldd_quan_utilizzata,ldd_quan_utilizzo, & 
						ldd_quan_utilizzo_variante,ldd_quan_reso,ldd_quan_sfrido, & 
						ldd_quan_scarto,ldd_quan_utilizzo_mp, ldd_test, & 
						ldd_quan_utilizzo_1[], ldd_quan_in_produzione_1,ldd_giacenza_prima,ldd_giacenza_dopo
						
uo_magazzino		luo_mag

if g_mb.messagebox("SEP","Sei sicuro di voler eliminare i dati delle sessioni di lavorazione?",Question!,YesNo!,2) = 2 then return

li_anno_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "num_commessa")
ll_prog_riga = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "prog_riga")
ldd_quan_utilizzata = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "quan_utilizzata")
ls_cod_prodotto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(), "cod_prodotto")
ls_cod_versione = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(), "cod_versione")
ls_cod_lavorazione = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(), "cod_lavorazione")
ls_cod_reparto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(), "cod_reparto")

if ldd_quan_utilizzata > 0 then
	g_mb.messagebox("Sep","Attenzione non è possibile eliminare i dettagli orari di questa fase di lavoro in quanto la quantità utilizzata del semilavorato è maggiore di zero.",stopsign!)
	return
end if

select count(*)
into   :ll_num_fasi_aperte
from   avan_produzione_com
where  cod_azienda = :s_cs_xx.cod_azienda and 	 
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 prog_riga = :ll_prog_riga          and    
		 flag_fine_fase = 'N';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	return
end if

if ll_num_fasi_aperte = 0 then
	g_mb.messagebox("Sep","Non è più possibile eliminare i dettagli orari poichè tutte le fasi sono state chiuse.", stopsign!)
	return
end if

setnull(ls_test)

select cod_azienda
into   :ls_test
from   det_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and    
	    anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa;

if not isnull(ls_test) then
	if (g_mb.messagebox("Sep","Attenzione esiste già una bolla di vendita legata alla" + & 
		 " commessa corrente, pertanto non è più possibile eliminare o modificare alcuna" + & 
		 " parte di essa. E' anche possibile, visto che alcune fasi sono ancora aperte, che " + & 
		 " sia stato fatto un anticipo spedizioni, in questo caso si può procedere con " + & 
		 "l'eliminazione dei dettagli orari di fase. Vuoi continuare?",Exclamation!,yesno!, 2))=2 then return
end if

select cod_prodotto,
		 cod_versione
into   :ls_cod_prodotto_finito,	
		 :ls_cod_versione_finito
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	return
end if

select quan_in_produzione
into   :ldd_quan_in_produzione_1
from   det_anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa		and    
		 prog_riga = :ll_prog_riga;

ll_prog = 0

li_risposta = f_calcola_quan_utilizzo_2(ls_cod_prodotto_finito,ls_cod_versione_finito, & 
													 ls_cod_prodotto_2[], ldd_quan_utilizzo_1[],& 
													 ll_prog,ldd_quan_in_produzione_1,li_anno_commessa, & 
													 ll_num_commessa)

for ll_t =1 to upperbound(ls_cod_prodotto_2)
	if ls_cod_prodotto_2[ll_t]= ls_cod_prodotto then
		ldd_quan_in_produzione = ldd_quan_utilizzo_1[ll_t]
	end if
next

//ldd_quan_in_produzione = ldd_quan_in_produzione * ldd_quan_in_produzione_1

declare righe_distinta cursor for 
select  cod_prodotto_figlio,
		  cod_versione_figlio,
		  quan_utilizzo,
		  flag_materia_prima
from    distinta 
where   cod_azienda = :s_cs_xx.cod_azienda  and     
        cod_prodotto_padre = :ls_cod_prodotto and     
		  cod_versione = :ls_cod_versione;

open righe_distinta;

do while 1 = 1
	fetch righe_distinta 
	into  :ls_cod_prodotto_figlio,
			:ls_cod_versione_figlio,
			:ldd_quan_utilizzo,
			:ls_flag_materia_prima;
	
  	if (sqlca.sqlcode = 100) or (sqlca.sqlcode=-1) then exit
	
	select cod_prodotto,
			 cod_versione_variante,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,
			 :ls_cod_versione_variante,
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
			 anno_commessa = :li_anno_commessa 	and	 
			 num_commessa = :ll_num_commessa		and	 
			 cod_prodotto_padre = :ls_cod_prodotto and	 
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio 	and    
			 cod_versione = :ls_cod_versione and
			 cod_versione_figlio = :ls_cod_versione_figlio;

	if sqlca.sqlcode = 0 then
		
		ls_cod_prodotto_figlio = ls_cod_prodotto_variante
		ls_cod_versione_figlio = ls_cod_versione_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante

		if ls_flag_materia_prima_variante='N' then
			
			select cod_prodotto_figlio
			into   :ls_test
			from   distinta
			where  cod_azienda = :s_cs_xx.cod_azienda and    
					 cod_prodotto_padre = :ls_cod_prodotto_figlio and    
					 cod_versione = :ls_cod_versione_figlio;
			
			if sqlca.sqlcode = 100 then 
				ls_flag_materia_prima="S"
			else
				ls_flag_materia_prima="N"
			end if
		end if
	else
		if ls_flag_materia_prima = 'N'then
			
			select cod_prodotto_figlio
			into   :ls_test
			from   distinta
			where  cod_azienda = :s_cs_xx.cod_azienda and    
					 cod_prodotto_padre = :ls_cod_prodotto_figlio and    
					 cod_versione = :ls_cod_versione_figlio;
	
			if sqlca.sqlcode = 100 then 
				ls_flag_materia_prima="S"
			else
				ls_flag_materia_prima="N"
			end if
		end if
	end if
			 	
	// distinzione tra MP e SL

	if ls_flag_materia_prima = "S" then

		declare righe_mp_de cursor for
		select anno_registrazione,
			    num_registrazione,
				 quan_utilizzata,
				 prog_orari,
				 quan_reso,
				 quan_sfrido,		
				 quan_scarto
		from   mp_com_stock_det_orari
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 anno_commessa = :li_anno_commessa  and    
				 num_commessa = :ll_num_commessa		and    
				 prog_riga = :ll_prog_riga				and    
				 cod_prodotto = :ls_cod_prodotto_figlio;

		open righe_mp_de;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
			return
		end if
		
		do while 1=1 
			fetch righe_mp_de 
			into  :li_anno_registrazione,
				   :ll_num_registrazione,
					:ldd_quan_utilizzo_mp,
				   :ll_prog_orari,
					:ldd_quan_reso,
					:ldd_quan_sfrido,
					:ldd_quan_scarto;

			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
				close righe_mp_de;
				return
			end if
			
			if sqlca.sqlcode = 100 then exit
			
			select sum(giacenza_stock)
			into   :ldd_giacenza_prima
			from   stock
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto_figlio;
	
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
				rollback;
				return
			end if
	
			delete mp_com_stock_det_orari
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:li_anno_commessa
			and    num_commessa=:ll_num_commessa
			and    prog_riga=:ll_prog_riga
			and    cod_prodotto =:ls_cod_prodotto_figlio
			and    prog_orari=:ll_prog_orari;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
				rollback;
				return
			end if
	
			
			luo_mag = create uo_magazzino
  			li_risposta = luo_mag.uof_elimina_movimenti(li_anno_registrazione, ll_num_registrazione,true)
  			destroy luo_mag
	  
			if li_risposta = -1 then
				 g_mb.messagebox("Sep","Errore su elimina movimenti magazzino, "  + sqlca.sqlerrtext, stopsign!)
				 close righe_mp_de;
				 return
			end if
			
			if isnull(ldd_quan_utilizzo_mp) then ldd_quan_utilizzo_mp = 0
			if isnull(ldd_quan_reso) then ldd_quan_reso = 0
			if isnull(ldd_quan_sfrido) then ldd_quan_sfrido = 0
			if isnull(ldd_quan_scarto) then ldd_quan_scarto = 0
			
	
			update mat_prime_commessa
			set    quan_utilizzata = quan_utilizzata - :ldd_quan_utilizzo_mp,
					 quan_reso = quan_reso - :ldd_quan_reso,
					 quan_sfrido = quan_sfrido - :ldd_quan_sfrido,
					 quan_scarto = quan_scarto - :ldd_quan_scarto
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:li_anno_commessa
			and    num_commessa=:ll_num_commessa
			and    prog_riga=:ll_prog_riga
			and    cod_prodotto =:ls_cod_prodotto_figlio;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
				close righe_mp_de;
				rollback;
				return
			end if
	
			select sum(giacenza_stock)
			into   :ldd_giacenza_dopo
			from   stock
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto_figlio;
	
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
				rollback;
				return
			end if
			
			ldd_test = ldd_giacenza_prima + ldd_quan_utilizzo_mp
	
			if round(ldd_test,6) <> round(ldd_giacenza_dopo,6) then
				g_mb.messagebox("Sep","Errore nel magazzino: l'eliminazione dei dettagli orari non è avvenuta correttamente. Il processo di eliminazione viene bloccato e viene fatto un rollback. Riprovare l'eliminazione dei dettagli orari in un successivo momento.",stopsign!)
				rollback;
				return
			end if
			
			delete mp_com_stock_det_orari_temp
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:li_anno_commessa
			and    num_commessa=:ll_num_commessa
			and    prog_riga=:ll_prog_riga
			and    cod_prodotto =:ls_cod_prodotto_figlio
			and    prog_orari=:ll_prog_orari;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
				rollback;
				return
			end if
	
			
			
			// *************** AGGIORNO TABELLA IMPEGNO_MAT_PRIME_COMMESSA E QUAN_IMPEGNATA IN ANAG_PRODOTTI
			update impegno_mat_prime_commessa
			set 	 quan_impegnata_attuale = quan_impegnata_attuale + :ldd_quan_utilizzo_mp
			where  cod_azienda = :s_cs_xx.cod_azienda and    
					 anno_commessa = :li_anno_commessa	and    
					 num_commessa = :ll_num_commessa		and    
					 cod_prodotto = :ls_cod_prodotto_figlio;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
				rollback;
				return
			end if
	
			update anag_prodotti																		
			set 	 quan_impegnata = quan_impegnata + :ldd_quan_utilizzo_mp
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto_figlio;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
				rollback;
				return
			end if
			
			// **********************************************************************************************
			
			
			li_risposta = f_scrivi_log (" ->PRODUZIONE ->Commessa:"+ string(li_anno_commessa)  + "/" + string(ll_num_commessa) +  & 
											  "->Eliminazione det orari MP: Cod_prodotto=" + ls_cod_prodotto_figlio + "~t" + &
											  "~t" +"quantità=" + string(ldd_quan_utilizzo_mp) + "~t" +"Movimento (anno/num): " + & 
											  string(li_anno_registrazione) + "/" + string(ll_num_registrazione))
		
			if li_risposta= -1 then
				rollback;
				return
			end if
	
		loop

		close righe_mp_de;

	else
		
		update avan_produzione_com
		set    quan_utilizzata=0,
				 quan_reso=0,
				 quan_sfrido=0,
				 quan_scarto=0
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       anno_commessa = :li_anno_commessa  and    
				 num_commessa = :ll_num_commessa    and    
				 prog_riga = :ll_prog_riga          and    
				 cod_prodotto = :ls_cod_prodotto_figlio and
				 cod_versione = :ls_cod_versione_figlio;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
			close righe_distinta;
			rollback;
			return
		end if

	end if
	
loop
	
close righe_distinta;

update avan_produzione_com
set    quan_in_produzione = :ldd_quan_in_produzione,
		 quan_prodotta=0,
		 quan_reso=0,
		 quan_sfrido=0,
		 quan_scarto=0,
		 tempo_attrezzaggio=0,
		 tempo_lavorazione=0,	
		 tempo_attrezzaggio_commessa=0,
		 tempo_movimentazione=0,
		 tempo_risorsa_umana=0,
		 flag_fine_fase='N'
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 prog_riga = :ll_prog_riga          and    
		 cod_prodotto = :ls_cod_prodotto	   and
		 cod_versione = :ls_cod_versione    and    
		 cod_reparto = :ls_cod_reparto      and    
		 cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	rollback;
	return
end if

delete difetti_det_orari_in
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 prog_riga = :ll_prog_riga			   and    
		 cod_prodotto = :ls_cod_prodotto	   and
		 cod_versione = :ls_cod_versione    and    
		 cod_reparto = :ls_cod_reparto      and    
		 cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	rollback;
	return
end if

delete difetti_det_orari_out
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa		and    
		 prog_riga = :ll_prog_riga				and    
		 cod_prodotto = :ls_cod_prodotto		and    
		 cod_versione = :ls_cod_versione 	and    
		 cod_reparto = :ls_cod_reparto		and    
		 cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	rollback;
	return
end if

delete det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa	and    
		 num_commessa = :ll_num_commessa		and    
		 prog_riga = :ll_prog_riga				and    
		 cod_prodotto = :ls_cod_prodotto		and    
		 cod_versione = :ls_cod_versione		and    
		 cod_reparto = :ls_cod_reparto		and    
		 cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	rollback;
	return
end if

if not isnull(ll_anno_reg_sl) and not isnull(ll_num_reg_sl) then
	
	select anno_reg_sl,
			 num_reg_sl
	into   :ll_anno_reg_sl,
			 :ll_num_reg_sl
	from   avan_produzione_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_commessa = :li_anno_commessa	and    
			 num_commessa = :ll_num_commessa		and    
			 prog_riga = :ll_prog_riga				and    
			 cod_prodotto = :ls_cod_prodotto		and    
			 cod_versione = :ls_cod_versione		and    
			 cod_reparto = :ls_cod_reparto		and    
			 cod_lavorazione = :ls_cod_lavorazione;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
		rollback;
		return
	end if
	
	if (ll_anno_reg_sl <> 0) or (ll_num_reg_sl <> 0) then
		
		update avan_produzione_com
		set	 anno_reg_sl = null,
				 num_reg_sl =null
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 anno_commessa = :li_anno_commessa	and    
				 num_commessa = :ll_num_commessa		and    
				 prog_riga = :ll_prog_riga				and    
				 cod_prodotto = :ls_cod_prodotto		and    
				 cod_versione = :ls_cod_versione		and    
				 cod_reparto = :ls_cod_reparto		and    
				 cod_lavorazione = :ls_cod_lavorazione;
			
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
			rollback;
			return
		end if

		luo_mag = create uo_magazzino
		li_risposta = luo_mag.uof_elimina_movimenti(ll_anno_reg_sl, ll_num_reg_sl,true)
		destroy luo_mag
	
		if li_risposta = -1 then
			 g_mb.messagebox("Sep","Errore su elimina movimenti magazzino, " + sqlca.sqlerrtext, stopsign!)
			 close righe_mp_de;
			 rollback;
			 return
		end if
	end if
end if

commit;

setpointer(arrow!)
dw_avanzamento_produzione_commesse.triggerevent("pcd_search")
end event

event ue_controllo_giacenze();string  ls_cod_prodotto_figlio,ls_cod_prodotto_padre,ls_des_prodotto, ls_test,ls_cod_tipo_commessa,ls_flag_muovi_mp, ls_cod_versione_figlio, & 
		  ls_flag_materia_prima, ls_cod_prodotto_variante,ls_flag_materia_prima_variante,ls_flag_muovi_sl, ls_cod_versione, &
		  ls_messaggio, ls_flag_fine_fase
		  
long    ll_num_righe,ll_num_fasi_aperte, ll_cont_figli, ll_cont_figli_integrazioni,&
        ll_anno_commessa, ll_num_commessa, ll_prog_riga, ll_i
		  
dec{4}  ldd_quan_prodotta,ldd_quan_in_produzione,ldd_quan_utilizzo,ldd_quan_utilizzo_variante,ldd_quan_impegnata,ldd_quan_ordinata

datastore lds_elenco_prodotti, lds_stock

setpointer( hourglass!)

//	***		In questi cursori leggo tutti i figli e le integrazioni con le relative quantita

declare righe_distinta cursor for 

	select  cod_prodotto_figlio,
			  quan_utilizzo,
			  flag_materia_prima,
			  cod_versione_figlio
	from    distinta 
	where   cod_azienda = :s_cs_xx.cod_azienda  and     
	        cod_prodotto_padre = :ls_cod_prodotto_padre and     
			  cod_versione = :ls_cod_versione and
			  flag_ramo_descrittivo = 'N'
union all 
	select  cod_prodotto_figlio,
			  quan_utilizzo,
			  flag_materia_prima,
			  cod_versione_figlio
	from    integrazioni_commessa
	where   cod_azienda = :s_cs_xx.cod_azienda and     
	        anno_commessa = :ll_anno_commessa  and     
			  num_commessa = :ll_num_commessa 	and     
			  cod_prodotto_padre = :ls_cod_prodotto_padre and     
			  cod_versione_padre = :ls_cod_versione;

lds_elenco_prodotti=CREATE datastore
lds_elenco_prodotti.dataobject = 'd_report_controllo_giacenze1'
lds_elenco_prodotti.settransobject(sqlca)

ls_cod_prodotto_padre = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_prodotto")
ls_cod_versione = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_versione")

setnull(ls_test)

ll_anno_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(),"anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(),"num_commessa")
ll_prog_riga = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(),"prog_riga")

select cod_prodotto,
		 cod_tipo_commessa
into   :ls_test,
		 :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :ll_anno_commessa  and    
		 num_commessa = :ll_num_commessa;

if sqlca.sqlcode <> 0 then
	setpointer( arrow!)
	g_mb.messagebox( "SEP","Errore in ricerca tipo commessa da angrafica commessa" + sqlca.sqlerrtext, stopsign!)
	return
end if

// se non è il prodotto finito leggo la quantità da avan_produzione com

if ls_test <> ls_cod_prodotto_padre then
	
	select quan_in_produzione,
			 flag_fine_fase
	into   :ldd_quan_in_produzione,
			 :ls_flag_fine_fase
	from   avan_produzione_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :ll_anno_commessa 	and    
			 num_commessa  = :ll_num_commessa 	and    
			 prog_riga     = :ll_prog_riga	         and    
			 cod_prodotto  = :ls_cod_prodotto_padre and
			 cod_versione  = :ls_cod_versione;

	if sqlca.sqlcode <> 0 then
		setpointer( arrow!)
		g_mb.messagebox( "SEP","Errore in ricerca quan_in_produzione su avan_produzione_com~r~n" + sqlca.sqlerrtext, stopsign!)
		return
	end if
	
	if not isnull(ls_flag_fine_fase) and ls_flag_fine_fase = "S" then
		setpointer( arrow!)
		g_mb.messagebox( "SEP", "Attenzione: la fase di cui si vogliono controllare le materie prime risulta chiusa!", stopsign!)
		return
	end if

else
	
	select quan_in_produzione
	into   :ldd_quan_in_produzione
	from   det_anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :ll_anno_commessa 	and    
			 num_commessa = :ll_num_commessa	   and    
			 prog_riga = :ll_prog_riga;

	if sqlca.sqlcode <> 0 then
		setpointer( arrow!)
		g_mb.messagebox( "SEP","Errore in ricerca su det_anag_commesse~r~n" + sqlca.sqlerrtext, stopsign!)
		return
	end if

end if

open righe_distinta;

if sqlca.sqlcode <> 0 then
	setpointer( arrow!)
	g_mb.messagebox( "SEP","Errore OPEN cursore righe_distinta~r~n" + sqlca.sqlerrtext, stopsign!)
	return
end if

do while true
	
	fetch righe_distinta into	:ls_cod_prodotto_figlio,	
									:ldd_quan_utilizzo,
									:ls_flag_materia_prima,
									:ls_cod_versione_figlio;
									
  	if sqlca.sqlcode = 100 then exit
	  
	if sqlca.sqlcode < 0 then
		setpointer( arrow!)
		g_mb.messagebox( "SEP","Errore FETCH cursore righe_distinta~r~n" + sqlca.sqlerrtext, stopsign!)
		return
	end if

	select cod_prodotto,
			 quan_utilizzo,
			 flag_materia_prima
	into   	:ls_cod_prodotto_variante,
			:ldd_quan_utilizzo_variante,
			:ls_flag_materia_prima_variante
	from   varianti_commesse
	where cod_azienda = :s_cs_xx.cod_azienda and	 
	       	 anno_commessa = :ll_anno_commessa 	and	 
			 num_commessa = :ll_num_commessa 	and	 
			 cod_prodotto_padre = :ls_cod_prodotto_padre and	 
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio 	and    
			 cod_versione = :ls_cod_versione  and    
			 cod_versione_figlio = :ls_cod_versione_figlio;

	if sqlca.sqlcode < 0 then
		setpointer( arrow!)
		g_mb.messagebox( "SEP","Errore SELECT su VARIANTI_COMMESSE~r~n" + sqlca.sqlerrtext, stopsign!)
		return
	end if

	// *** in questo if controlla se è una materia prima oppure no

	if sqlca.sqlcode = 0 then
		
		ls_cod_prodotto_figlio = ls_cod_prodotto_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante
		
		if ls_flag_materia_prima_variante = 'N' then
			
			SELECT count(*)
			into   :ll_cont_figli
			from   distinta												  
			where  cod_azienda = :s_cs_xx.cod_azienda	and		 	  
					 cod_prodotto_padre = :ls_cod_prodotto_figlio and	  
					 cod_versione = :ls_cod_versione_figlio and 		  
					 flag_ramo_descrittivo = 'N';
					 
			select  count(*)
			into    :ll_cont_figli_integrazioni
			from    integrazioni_commessa
			where   cod_azienda = :s_cs_xx.cod_azienda and
					anno_commessa = :ll_anno_commessa and
					num_commessa = :ll_num_commessa and
					cod_prodotto_padre = :ls_cod_prodotto_figlio and
					cod_versione_padre = :ls_cod_versione_figlio;
					
			ll_cont_figli = ll_cont_figli + 	ll_cont_figli_integrazioni
			
			if ll_cont_figli < 1 then
				ls_flag_materia_prima="S"
			else
				ls_flag_materia_prima="N"
			end if
		else
			ls_flag_materia_prima='S'
		end if
		
	else		// non si tratta di una variante.
		
		if ls_flag_materia_prima = 'N' then
			
			SELECT count(*)
			into   :ll_cont_figli
			FROM   distinta  
			WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
					 cod_prodotto_padre = :ls_cod_prodotto_figlio and    
					 cod_versione = :ls_cod_versione_figlio and
					 flag_ramo_descrittivo = 'N';
	
			select  count(*)	
			into    :ll_cont_figli_integrazioni
			from    integrazioni_commessa
			where   cod_azienda = :s_cs_xx.cod_azienda and     
					  anno_commessa = :ll_anno_commessa  and     
					  num_commessa = :ll_num_commessa	 and     
					  cod_prodotto_padre = :ls_cod_prodotto_figlio and
					  cod_versione_padre = :ls_cod_versione_figlio;
			
			ll_cont_figli = ll_cont_figli + 	ll_cont_figli_integrazioni
	
			if ll_cont_figli < 1 then
				ls_flag_materia_prima="S"
			else
				ls_flag_materia_prima="N"
			end if
		end if
	end if
	
	select des_prodotto, 
			 quan_impegnata,  
			 quan_ordinata
	into   :ls_des_prodotto, 
			 :ldd_quan_impegnata, 
			 :ldd_quan_ordinata
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto = :ls_cod_prodotto_figlio;
	
	if ls_flag_materia_prima = 'S' then

		choose case wf_controllo_stock(ll_anno_commessa, ll_num_commessa, ls_cod_prodotto_figlio, ldd_quan_utilizzo, ref ls_messaggio)
			case is < 0 
				setpointer( arrow!)
				g_mb.messagebox( "SEP",ls_messaggio, stopsign!)
				rollback;
				return
			case 1
				ll_num_righe = lds_elenco_prodotti.insertrow(0)
				lds_elenco_prodotti.setitem(ll_num_righe,"cod_semilavorato",ls_cod_prodotto_figlio)
				lds_elenco_prodotti.setitem(ll_num_righe,"cod_versione", ls_cod_versione_figlio)
				lds_elenco_prodotti.setitem(ll_num_righe,"des_semilavorato",ls_des_prodotto)
				lds_elenco_prodotti.setitem(ll_num_righe,"quan_utilizzo",ldd_quan_utilizzo)
				lds_elenco_prodotti.setitem(ll_num_righe,"mat_prima",ls_flag_materia_prima)
				lds_elenco_prodotti.setitem(ll_num_righe,"quan_utilizzata",ldd_quan_utilizzo)		
				lds_elenco_prodotti.setitem(ll_num_righe,"quan_impegnata_ap",ldd_quan_impegnata)
				lds_elenco_prodotti.setitem(ll_num_righe,"quan_ordinata_ap",ldd_quan_ordinata)
		end choose
	end if
	
	if ls_flag_materia_prima = "N" then
		
		// *** Michela Mantoan 21/11/2007: se non è una materia prima allora è un semilavorato. se la fase di lavorazione del semilavorato risulta chiusa non vale
		//														la pena controllare se ci sono le materie prime
		
		select flag_fine_fase
		into   :ls_flag_fine_fase
		from   avan_produzione_com
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 anno_commessa = :ll_anno_commessa 	and    
				 num_commessa  = :ll_num_commessa 	and    
				 prog_riga     = :ll_prog_riga	         and    
				 cod_prodotto  = :ls_cod_prodotto_figlio and
				 cod_versione  = :ls_cod_versione_figlio;
	
		if sqlca.sqlcode = 0 and not isnull(ls_flag_fine_fase) and ls_flag_fine_fase = "S" then
				// *** non faccio nulla
				
		else
				// *** controllo se ci sono le giacenze
			if wf_controllo_giacenze_totale( ls_cod_prodotto_figlio, ls_cod_versione_figlio, ll_anno_commessa, ll_num_commessa, ll_prog_riga, lds_elenco_prodotti, ls_messaggio) < 0 then
				setpointer( arrow!)
				g_mb.messagebox( "SEP", ls_messaggio, stopsign!)
				destroy lds_elenco_prodotti;
				close righe_distinta;
				return
			end if				
		end if		

	end if
	
loop
	
close righe_distinta;

for ll_i = 1 to lds_elenco_prodotti.rowcount()
	if isnull(lds_elenco_prodotti.getitemstring(ll_i, "cod_prodotto_padre")) then
		lds_elenco_prodotti.setitem(ll_i,"cod_prodotto_padre",ls_cod_prodotto_padre)
	end if
	
	if isnull(lds_elenco_prodotti.getitemstring(ll_i, "cod_versione_padre")) then
		lds_elenco_prodotti.setitem(ll_i,"cod_versione_padre",ls_cod_versione)
	end if
next

setpointer( arrow!)

if lds_elenco_prodotti.rowcount() > 0 then

	if g_mb.messagebox("SEP","PROCEDERE CON LA STAMPA DEI PRODOTTI MANCANTI ?",Question!,YesNo!,1) = 1 then
		lds_elenco_prodotti.print( )
	end if
	
else
	g_mb.messagebox("SEP","TUTTI I PRODOTTI RISULTANO DISPONIBILI!", Information!)
end if

destroy lds_elenco_prodotti

rollback;

return
end event

event ue_invianuovoterzista();integer li_anno_commessa
long    ll_num_commessa
string  ls_cod_tipo_commessa,ls_flag_muovi_sl,ls_flag_muovi_mp

li_anno_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "num_commessa")

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in ricerca commessa.~r~n" + sqlca.sqlerrtext,stopsign!)
	return
end if
	
select flag_muovi_sl,
		 flag_muovi_mp
into   :ls_flag_muovi_sl,
		 :ls_flag_muovi_mp
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in ricerca tipo commessa.~r~n " + sqlca.sqlerrtext,stopsign!)
	return
end if

if ls_flag_muovi_sl = "S" and ls_flag_muovi_mp = 'S' then
	
	if isnull(is_cod_prodotto_prec) or is_cod_prodotto_prec = "" then
		g_mb.messagebox( "SEP", "Codice prodotto padre mancante!", stopsign!)
		return 
	end if
	if isnull(is_cod_versione_prec) or is_cod_versione_prec = "" then
		g_mb.messagebox( "SEP", "Codice versione padre mancante!", stopsign!)
		return 		
	end if
	if isnull(is_cod_reparto_prec) or is_cod_reparto_prec = "" then
		g_mb.messagebox( "SEP", "Codice reparto padre mancante!", stopsign!)
		return 		
	end if
	if isnull(is_cod_lavorazione_prec) or is_cod_lavorazione_prec = "" then
		g_mb.messagebox( "SEP", "Codice lavorazione padre mancante!", stopsign!)
		return 		
	end if
	
	s_cs_xx.parametri.parametro_b_2 = true
	s_cs_xx.parametri.parametro_s_10 = is_cod_prodotto_prec
	s_cs_xx.parametri.parametro_s_11 = is_cod_versione_prec
	s_cs_xx.parametri.parametro_s_12 = is_cod_reparto_prec
	s_cs_xx.parametri.parametro_s_13 = is_cod_lavorazione_prec
	
	window_open_parm(w_carica_bolle_terzisti,-1,dw_avanzamento_produzione_commesse)
	
//	s_cs_xx.parametri.parametro_b_2 = false
else
	g_mb.messagebox("Sep","Questa commessa appartiene ad un tipo commessa che non genera i movimenti dei Semilavorati o delle materie prime, pertanto non è possibile gestire i terzisti.",stopsign!)
	return
end if
end event

public function integer wf_inizio ();string				ls_errore,ls_des_prodotto
long				ll_risposta, ll_anno_commessa, ll_num_commessa, ll_prog_riga
treeviewitem	tvi_campo
integer			li_pictureindex


ll_anno_commessa = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"anno_commessa")
ll_num_commessa  = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"num_commessa")
ll_prog_riga     = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"prog_riga")

select cod_prodotto,
       cod_versione
into   :is_cod_prodotto_finito, 
       :is_cod_versione_pf
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :ll_anno_commessa and
		 num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SEP", "Commessa non trovata.", Stopsign!)
	return -1
end if



uo_imposta_tv_varianti luo_varianti

s_chiave_distinta l_chiave_distinta
l_chiave_distinta.cod_prodotto_padre = is_cod_prodotto_finito
tv_db.setredraw(false)
tv_db.deleteitem(0)

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:l_chiave_distinta.cod_prodotto_padre;
ls_des_prodotto=trim(ls_des_prodotto)

li_pictureindex = 1


////se esiste un record in avan_produzione_com relativo al prodotto finito della commessa allora predisponi la chiave del tree
wf_controlla_fase_prodotto_finito(ll_anno_commessa, ll_num_commessa, ll_prog_riga, l_chiave_distinta, li_pictureindex)


tvi_campo.itemhandle = 1
tvi_campo.data = l_chiave_distinta
tvi_campo.label = l_chiave_distinta.cod_prodotto_padre + "," + ls_des_prodotto +  ", ver. " + is_cod_versione_PF
tvi_campo.pictureindex = li_pictureindex
tvi_campo.selectedpictureindex = li_pictureindex
tvi_campo.overlaypictureindex = li_pictureindex

ll_risposta = tv_db.insertitemlast(0, tvi_campo)

luo_varianti = CREATE uo_imposta_tv_varianti
luo_varianti.is_tipo_gestione = "varianti_commesse"
luo_varianti.is_cod_versione = is_cod_versione_PF
luo_varianti.il_anno_registrazione = ll_anno_commessa
luo_varianti.il_num_registrazione = ll_num_commessa
luo_varianti.il_prog_riga_registrazione = ll_prog_riga
luo_varianti.ib_verifica_fasi_avanzamento=true
luo_varianti.ib_salta_materie_prime = NOT(cbx_vis_mp.checked)
luo_varianti.ib_salta_rami_descrittivi = NOT(cbx_flag_ramo_descrittivo.checked)

luo_varianti.uof_imposta_tv(ref tv_db, l_chiave_distinta.cod_prodotto_padre, is_cod_versione_pf, 1, 1, "S", ref ls_errore)
destroy luo_varianti
tv_db.expandall(1)
tv_db.setredraw(true)
tv_db.triggerevent("pcd_clicked")

// tenere questa chiamata sempre per ultima perchè al suo interno ha un COMMIT o un ROLLBACK a seconda del corretto funzionamento.
tv_db.postevent("ue_verifica_fasi")

return 0
end function

public subroutine wf_calcola_stato_handle (long fl_handle);string					ls_stato_fase, ls_flag_fase_conclusa, ls_flag_fase_esterna
long					ll_anno_commessa, ll_num_commessa, ll_prog_riga,ll_cont,ll_pictureindex

treeviewitem		tvi_campo
s_chiave_distinta	l_chiave_distinta

ll_anno_commessa = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"anno_commessa")
ll_num_commessa  = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"num_commessa")
ll_prog_riga     = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"prog_riga")

tv_db.GetItem (fl_handle , tvi_campo )

l_chiave_distinta = tvi_campo.data

//salvo l'attuale pictureindex
ll_pictureindex = tvi_campo.pictureindex

setnull(ls_stato_fase)
		
ls_stato_fase = "A"		//fase aperta (non Iniziata e non Finita)
		
// controllo se terminata
SELECT flag_fine_fase,   
		 flag_esterna  
 INTO  :ls_flag_fase_conclusa,   
		 :ls_flag_fase_esterna  
 FROM  avan_produzione_com  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_commessa = :ll_anno_commessa and
		 num_commessa = : ll_num_commessa and
		 cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio and
		 cod_versione = :l_chiave_distinta.cod_versione_figlio and
		 prog_riga = :ll_prog_riga  and
		 cod_reparto = :l_chiave_distinta.cod_reparto  and
		 cod_lavorazione = :l_chiave_distinta.cod_lavorazione  ;
				 
if sqlca.sqlcode = 0 then
			
	if ls_flag_fase_esterna = "S" then	ls_stato_fase = "E"		//fase aperta ESTERNA (non Iniziata e non Finita)
		//se non terminata controllo se iniziata			
		
	if ls_flag_fase_conclusa = "N"  then
		SELECT count(*)
		INTO   :ll_cont
		FROM   det_orari_produzione
		WHERE  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_commessa = :ll_anno_commessa and 
				 num_commessa = : ll_num_commessa and
				 cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio and
				 cod_versione = :l_chiave_distinta.cod_versione_figlio and
				 prog_riga = :ll_prog_riga and
				 cod_reparto = :l_chiave_distinta.cod_reparto  and
				 cod_lavorazione = :l_chiave_distinta.cod_lavorazione and
				 flag_inizio = 'S' ;
		
		if ll_cont > 0 then
			ls_stato_fase = "I"		// Fase Iniziata
		end if 
	else
		ls_stato_fase = "F"  // fase conclusa
	end if
	
//non esiste la fase di avanzamento nell'avanzamento di produzione
else
	setnull(ls_stato_fase)
end if			


if not isnull(ls_stato_fase) then //stato fase
	choose case ls_stato_fase
		case "A"
			ll_pictureindex = 6
		case "I"
			ll_pictureindex = 7
		case "F"
			ll_pictureindex = 8
		case "E"  // fase esterna aperta
			ll_pictureindex = 9
	end choose
end if

if l_chiave_distinta.flag_ramo_descrittivo = "S" then ll_pictureindex = 10
			

tvi_campo.pictureindex = ll_pictureindex

tvi_campo.selectedpictureindex = ll_pictureindex

tvi_campo.overlaypictureindex = ll_pictureindex

tv_db.setitem(fl_handle, tvi_campo)

end subroutine

public function integer wf_controllo_stock (long fl_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, decimal fd_quan_necessaria, ref string fs_messaggio);string   ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, & 
			ls_cod_tipo_commessa,ls_modify,ls_cod_reparto,ls_cod_lavorazione, ls_messaggio
long     ll_prog_stock,ll_num_righe,ll_prog_orari,ll_num_fasi_aperte
datetime ldt_data_stock
dec{4}   ldd_quan_disponibile, ldd_quan_totale, ldd_disponibilita_alla_data

uo_mrp luo_mrp
datetime	ldt_data_oggi

ldt_data_oggi = datetime( date( today()), 00:00:00)

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:fl_anno_commessa
and    num_commessa=:fl_num_commessa;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore ricerca tipo commessa su commessa "+string(fl_anno_commessa)+"/"+string(fl_num_commessa) +  "~r~n" + sqlca.sqlerrtext
	return 0
end if

select cod_deposito_prelievo
into   :ls_cod_deposito
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:ls_cod_tipo_commessa;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore ricerca deposito prelievo su tab_tipi_commessa "+ls_cod_tipo_commessa +  "~r~n" + sqlca.sqlerrtext
	return 0
end if

declare righe_stock cursor for
select  cod_ubicazione,   
		  cod_lotto,   
		  data_stock,   
		  prog_stock,
		  giacenza_stock - quan_assegnata - quan_in_spedizione  
FROM    stock   
where   cod_azienda=:s_cs_xx.cod_azienda and
		  cod_prodotto =:fs_cod_prodotto and
		  cod_deposito =:ls_cod_deposito and
		  giacenza_stock - quan_assegnata - quan_in_spedizione > 0
ORDER BY data_stock;

open righe_stock;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore open cursore righe_stock~r~n" + sqlca.sqlerrtext
	return 0
end if

ldd_quan_totale = 0

do while true
	fetch righe_stock 
	into  :ls_cod_ubicazione,
			:ls_cod_lotto,   
			:ldt_data_stock,   
			:ll_prog_stock,
			:ldd_quan_disponibile;

	choose case sqlca.sqlcode  
		case 100
			exit
		case is < 0 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore fetch cursore righe_stock~r~n" + sqlca.sqlerrtext
				return 0
			end if
	end choose
	
	ldd_quan_totale = ldd_quan_totale + ldd_quan_disponibile

loop
close righe_stock;

//luo_mrp = create uo_mrp
//
//if luo_mrp.uof_disponibilita_alla_data(		fs_cod_prodotto, &
//													ldt_data_oggi, &
//													ls_cod_deposito, &
//													ref ldd_disponibilita_alla_data, &
//													ref ls_messaggio) <> 0 then
//	fs_messaggio = "Errore in calcolo disponibilità alla data~r~n" + ls_messaggio
//	destroy luo_mrp;
//	return -1
//end if				
//
//destroy luo_mrp;

if ldd_quan_totale >= fd_quan_necessaria then
	return 0
else
	return 1
end if

end function

public function integer wf_controllo_giacenze_totale (string fs_cod_prodotto_padre, string fs_cod_versione_padre, long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga, ref datastore fds_elenco_prodotti, ref string fs_messaggio);string  ls_cod_prodotto_figlio,ls_cod_prodotto_padre,ls_des_prodotto, ls_test,ls_cod_tipo_commessa,ls_flag_muovi_mp, ls_cod_versione_figlio, & 
		  ls_flag_materia_prima, ls_cod_prodotto_variante,ls_flag_materia_prima_variante,ls_flag_muovi_sl, ls_cod_versione, &
		  ls_messaggio, ls_flag_ramo_descrittivo, ls_cod_versione_variante, ls_des_prodotto_variante, ls_flag_fine_fase
		  
long    ll_num_righe,ll_num_fasi_aperte, ll_cont_figli, ll_cont_figli_integrazioni, ll_num_figli, ll_num_sequenza, ll_riga
		  
dec{4}  ldd_quan_prodotta,ldd_quan_in_produzione,ldd_quan_utilizzo,ldd_quan_utilizzo_variante, ld_quan_utilizzo, ldd_quan_impegnata, ldd_quan_ordinata

datastore         lds_righe_distinta

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda   and    
		 cod_prodotto = :ls_cod_prodotto_padre;

lds_righe_distinta = Create DataStore
lds_righe_distinta.DataObject = "d_data_store_distinta_comm"
lds_righe_distinta.SetTransObject(sqlca)
ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto_padre, fs_cod_versione_padre, fl_anno_commessa, fl_num_commessa)
	
for ll_num_figli = 1 to ll_num_righe
	
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring( ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring( ll_num_figli,"cod_versione_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber( ll_num_figli,"quan_utilizzo")
	ls_flag_materia_prima = lds_righe_distinta.getitemstring( ll_num_figli,"flag_materia_prima")
	ls_flag_ramo_descrittivo = lds_righe_distinta.getitemstring( ll_num_figli,"flag_ramo_descrittivo")
	
	
	if ls_flag_ramo_descrittivo = "S" then continue

	select cod_prodotto,
			 quan_utilizzo,
			 flag_materia_prima
	into   	:ls_cod_prodotto_variante,
			:ldd_quan_utilizzo_variante,
			:ls_flag_materia_prima_variante
	from   varianti_commesse
	where cod_azienda = :s_cs_xx.cod_azienda and	 
	       	 anno_commessa = :fl_anno_commessa 	and	 
			 num_commessa = :fl_num_commessa 	and	 
			 cod_prodotto_padre = :fs_cod_prodotto_padre and	 
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio 	and    
			 cod_versione = :fs_cod_versione_padre  and    
			 cod_versione_figlio = :ls_cod_versione_figlio;

	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore SELECT su VARIANTI_COMMESSE~r~n" + sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode = 0 then
		
		ls_cod_prodotto_figlio = ls_cod_prodotto_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante
		
		if ls_flag_materia_prima_variante = 'N' then
			
			SELECT count(*)
			into   :ll_cont_figli
			from   distinta												  
			where  cod_azienda = :s_cs_xx.cod_azienda	and		 	  
					 cod_prodotto_padre = :ls_cod_prodotto_figlio and	  
					 cod_versione = :ls_cod_versione_figlio and 		  
					 flag_ramo_descrittivo = 'N';
					 
			select  count(*)
			into    :ll_cont_figli_integrazioni
			from    integrazioni_commessa
			where   cod_azienda = :s_cs_xx.cod_azienda and
					anno_commessa = :fl_anno_commessa and
					num_commessa = :fl_num_commessa and
					cod_prodotto_padre = :ls_cod_prodotto_figlio and
					cod_versione_padre = :ls_cod_versione_figlio;
					
			ll_cont_figli = ll_cont_figli + 	ll_cont_figli_integrazioni
			
			if ll_cont_figli < 1 then
				ls_flag_materia_prima="S"
			else
				ls_flag_materia_prima="N"
			end if
		else
			ls_flag_materia_prima='S'
		end if
		
	else		// non si tratta di una variante.
		
		if ls_flag_materia_prima = 'N' then
			
			SELECT count(*)
			into   :ll_cont_figli
			FROM   distinta  
			WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
					 cod_prodotto_padre = :ls_cod_prodotto_figlio and    
					 cod_versione = :ls_cod_versione_figlio and
					 flag_ramo_descrittivo = 'N';
	
			select  count(*)	
			into    :ll_cont_figli_integrazioni
			from    integrazioni_commessa
			where   cod_azienda = :s_cs_xx.cod_azienda and     
					  anno_commessa = :fl_anno_commessa  and     
					  num_commessa = :fl_num_commessa	 and     
					  cod_prodotto_padre = :ls_cod_prodotto_figlio and
					  cod_versione_padre = :ls_cod_versione_figlio;
			
			ll_cont_figli = ll_cont_figli + 	ll_cont_figli_integrazioni
	
			if ll_cont_figli < 1 then
				ls_flag_materia_prima="S"
			else
				ls_flag_materia_prima="N"
			end if
		end if
	end if
	
	select des_prodotto,
	       quan_impegnata,
			 quan_ordinata
	into   :ls_des_prodotto,
			 :ldd_quan_impegnata,
			 :ldd_quan_ordinata
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto = :ls_cod_prodotto_figlio;
	
	if ls_flag_materia_prima = 'S' then

		choose case wf_controllo_stock(fl_anno_commessa, fl_num_commessa, ls_cod_prodotto_figlio, ldd_quan_utilizzo, ref ls_messaggio)
			case is < 0 
				fs_messaggio = ls_messaggio
				return -1
			case 1
				
				ll_riga = fds_elenco_prodotti.insertrow(0)
				fds_elenco_prodotti.setitem( ll_riga, "cod_semilavorato", ls_cod_prodotto_figlio)
				fds_elenco_prodotti.setitem( ll_riga, "cod_versione", ls_cod_versione_figlio)
				fds_elenco_prodotti.setitem( ll_riga, "des_semilavorato", ls_des_prodotto)
				fds_elenco_prodotti.setitem( ll_riga, "quan_utilizzo", ldd_quan_utilizzo)
				fds_elenco_prodotti.setitem( ll_riga, "mat_prima", ls_flag_materia_prima)
				fds_elenco_prodotti.setitem( ll_riga, "quan_utilizzata", ldd_quan_utilizzo)	
				fds_elenco_prodotti.setitem( ll_riga, "quan_impegnata_ap",ldd_quan_impegnata)
				fds_elenco_prodotti.setitem( ll_riga, "quan_ordinata_ap",ldd_quan_ordinata)
				
		end choose
	end if
	
	if ls_flag_materia_prima = "N" then
		
		// *** Michela Mantoan 21/11/2007: se non è una materia prima allora è un semilavorato. se la fase di lavorazione del semilavorato risulta chiusa non vale
		//														la pena controllare se ci sono le materie prime
		
		select flag_fine_fase
		into   :ls_flag_fine_fase
		from   avan_produzione_com
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 anno_commessa = :fl_anno_commessa 	and    
				 num_commessa  = :fl_num_commessa 	and    
				 prog_riga     = :fl_prog_riga	         and    
				 cod_prodotto  = :ls_cod_prodotto_figlio and
				 cod_versione  = :ls_cod_versione_figlio;
	
		if sqlca.sqlcode = 0 and not isnull(ls_flag_fine_fase) and ls_flag_fine_fase = "S" then
				// *** non faccio nulla
		else		
		
			if wf_controllo_giacenze_totale( ls_cod_prodotto_figlio, ls_cod_versione_figlio, fl_anno_commessa, fl_num_commessa, fl_prog_riga, fds_elenco_prodotti, ls_messaggio) < 0 then
				fs_messaggio = ls_messaggio
				destroy lds_righe_distinta;
				return -1
			end if
			
		end if
			
	end if

next

destroy(lds_righe_distinta)

return 0
end function

public subroutine wf_verifica_fasi_start ();boolean lb_nuovo_compomente = false
string ls_errore
long ll_handle, ll_ret, ll_anno_commessa, ll_num_commessa
dec{4} ld_quan_impegno
uo_magazzino luo_magazzino

ll_handle = tv_db.finditem(roottreeitem!,0)

if ll_handle = -1 then return

if cbx_vis_mp.checked or cbx_flag_ramo_descrittivo.checked  then return

if ll_handle > 0 then
	
	// metto il commit in modo che le cose fatte prima vengano salvate.
	commit;
	
	ll_ret = wf_verifica_fasi(ll_handle, ref lb_nuovo_compomente)
	
	choose case ll_ret
			
		case 100
			rollback;
			tv_db.setfocus()
			return
			
		case is < 0
			rollback;
			
		case else
			// nel caso in cui si sia trovato un nuovo componente è necessario azzera e ricreare l'impegnato.
			if lb_nuovo_compomente then
				luo_magazzino = create uo_magazzino
				//disimpegno tutte le MP
				ll_anno_commessa = iuo_dw_main.i_parentdw.getitemnumber(iuo_dw_main.i_parentdw.getrow(),"anno_commessa")
				ll_num_commessa  = iuo_dw_main.i_parentdw.getitemnumber(iuo_dw_main.i_parentdw.getrow(),"num_commessa")
				
				select quan_in_produzione - quan_prodotta
				into   :ld_quan_impegno
				from   anag_commesse
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_commessa = :ll_anno_commessa and
						 num_commessa = :ll_num_commessa;
				
				if luo_magazzino.uof_impegna_mp_commessa(true, &
																	true, &
				                                       false, &
																	is_cod_prodotto_finito, &
																	is_cod_versione_pf, &
																	ld_quan_impegno, &
																	ll_anno_commessa, &
																	ll_num_commessa, &
																	ref ls_errore) < 0 then
					g_mb.messagebox("SEP","Errore in impegno MP~r~n" + ls_errore,stopsign!)
					rollback;
				end if
				
			end if
			
			commit;
			
	end choose
	
end if
return
end subroutine

public function integer wf_verifica_fasi (long al_handle, ref boolean ab_trovato_nuovo_componente);string ls_cod_prodotto_figlio,ls_cod_versione_figlio,ls_cod_reparto,ls_cod_lavorazione,ls_cod_reparto_fase,ls_cod_lavorazione_fase,ls_cod_tipo_commessa, &
       ls_cod_tipo_mov_reso, ls_cod_tipo_mov_sfrido, ls_errore
long ll_handle, ll_padre,ll_anno_commessa,ll_num_commessa,ll_prog_riga,ll_cont,ll_ret
dec{4} ld_quan_in_produzione,ld_quan_utilizzo,ld_quan_impegno

// parte dichiarativa aggiunta per impegno MP se aggiunta
string ls_cod_prodotto_figlio_imp, ls_cod_versione_imp, ls_flag_materia_prima_imp, ls_flag_ramo_descrittivo_imp
long ll_cont_figli_impegno,ll_count_imp
dec{4} ld_quan_imp
datetime ldt_data_fabbisogno


treeviewitem ltv_item
s_chiave_distinta l_chiave_distinta
s_avan_produzione_com_qta l_avan_produzione_com_qta


ll_handle = tv_db.finditem(childtreeitem!,al_handle)

ll_anno_commessa = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"anno_commessa")
ll_num_commessa  = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"num_commessa")
ll_prog_riga     = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"prog_riga")

if ll_handle > 0 then
	
	tv_db.getitem(ll_handle,ltv_item)
	l_chiave_distinta = ltv_item.data
	
	ls_cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio
	ls_cod_versione_figlio = l_chiave_distinta.cod_versione_figlio
	ls_cod_reparto = l_chiave_distinta.cod_reparto
	ls_cod_lavorazione = l_chiave_distinta.cod_lavorazione
	ld_quan_utilizzo = l_chiave_distinta.quan_utilizzo
	
	SELECT  count(*)
	INTO  :ll_cont
	FROM 	avan_produzione_com
	WHERE cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa and
			num_commessa = :ll_num_commessa and
			cod_prodotto = :ls_cod_prodotto_figlio and
			cod_versione = :ls_cod_versione_figlio and
			prog_riga = :ll_prog_riga and
			cod_reparto = :ls_cod_reparto and
			cod_lavorazione = :ls_cod_lavorazione;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox( "SEP", "Errore in ricerca fase avanzamento.~r~r"+sqlca.sqlerrtext)
		return -1
	end if

	if ll_cont = 0 then
		
		//verifico se questo prodotto ha una fase di lavorazione associata; altrimenti segnalo e vado avanti
		SELECT cod_reparto,   
				cod_lavorazione 
		INTO	:ls_cod_reparto_fase,   
				:ls_cod_lavorazione_fase
		FROM 	tes_fasi_lavorazione
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto_figlio and
				cod_versione = :ls_cod_versione_figlio;

		// attenzione!! non controllare mail l'errore -1 perchè potrebbero esserci più fasi delle stesso prodotto; in questo caso prendo quella che viene su.
		if sqlca.sqlcode = 100 then
			g_mb.messagebox("SEP","Attenzione. E' stato aggiunto un semilavorato in distinta a cui non è associata alcuna fase di avanzamento.")
		end if	
		
		select cod_tipo_commessa
		into   :ls_cod_tipo_commessa
		from   anag_commesse
		where	 cod_azienda =:s_cs_xx.cod_azienda and
				 anno_commessa = :ll_anno_commessa and
				 num_commessa = :ll_num_commessa;
		
		SELECT cod_tipo_mov_reso_semilav,
				cod_tipo_mov_sfrido_semilav  
		 INTO :ls_cod_tipo_mov_reso  ,
		      :ls_cod_tipo_mov_sfrido
		 FROM tab_tipi_commessa
		 where cod_azienda =:s_cs_xx.cod_azienda and
				 cod_tipo_commessa = :ls_cod_tipo_commessa;
				 
		l_avan_produzione_com_qta.cod_prodotto = ls_cod_prodotto_figlio
		l_avan_produzione_com_qta.quan_utilizzo = ld_quan_utilizzo
		
		if not ab_trovato_nuovo_componente then
			// in caso di nuovo componente, disimpegno (solo una volta)
			select quan_in_produzione - quan_prodotta
			into   :ld_quan_impegno
			from   anag_commesse
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_commessa = :ll_anno_commessa and
					 num_commessa = :ll_num_commessa;

			if iuo_magazzino.uof_impegna_mp_commessa(true, &
																false, &
																false, &
																is_cod_prodotto_finito, &
																is_cod_versione_pf, &
																ld_quan_impegno, &
																ll_anno_commessa, &
																ll_num_commessa, &
																ref ls_errore) < 0  then
				g_mb.messagebox("SEP","Errore in disimpegno MP~r~n" + ls_errore,stopsign!)
				rollback;
			end if			
			ab_trovato_nuovo_componente = true		// questo messo a TRUE impedisce di disimpegnare una seconda volta
			
		end if		
		
		openwithparm(w_avanz_prod_com_attivazione_qta, l_avan_produzione_com_qta )
		
		ld_quan_in_produzione = dec(message.doubleparm)
		
		//inserisco la fase di avanzamento
		INSERT INTO avan_produzione_com  
					( cod_azienda,   
					  anno_commessa,   
					  num_commessa,   
					  cod_prodotto,   
					  cod_versione,   
					  prog_riga,   
					  cod_reparto,   
					  cod_lavorazione,   
					  anno_reg_reso,   
					  num_reg_reso,   
					  tempo_attrezzaggio,   
					  tempo_attrezzaggio_commessa,   
					  tempo_lavorazione,   
					  quan_in_produzione,   
					  quan_prodotta,   
					  flag_fine_fase,   
					  cod_tipo_mov_reso,   
					  quan_utilizzata,   
					  cod_tipo_mov_sfrido,   
					  anno_reg_sfrido,   
					  num_reg_sfrido,   
					  quan_reso,   
					  quan_sfrido,   
					  quan_scarto,   
					  tempo_risorsa_umana,   
					  anno_reg_des_mov_reso,   
					  num_reg_des_mov_reso,   
					  anno_reg_des_mov_sfrido,   
					  num_reg_des_mov_sfrido,   
					  flag_esterna,   
					  tempo_movimentazione,   
					  flag_bolla_uscita,   
					  anno_reg_bol_ven,   
					  num_reg_bol_ven,   
					  prog_riga_bol_ven,   
					  anno_reg_sl,   
					  num_reg_sl,   
					  ordinamento )  
		  VALUES ( :s_cs_xx.cod_azienda,   
					  :ll_anno_commessa,   
					  :ll_num_commessa,   
					  :ls_cod_prodotto_figlio,   
					  :ls_cod_versione_figlio,   
					  :ll_prog_riga,   
					  :ls_cod_reparto_fase,   
					  :ls_cod_lavorazione_fase,   
					  null,   
					  null,   
					  0,   
					  0,   
					  0,   
					  :ld_quan_in_produzione,   
					  0,   
					  'N',   
					  :ls_cod_tipo_mov_reso,   
					  0,   
					  :ls_cod_tipo_mov_sfrido,   
					  null,   
					  null,   
					  0,   
					  0,   
					  0,   
					  0,   
					  null,   
					  null,   
					  null,   
					  null,   
					  'N',   
					  0,   
					  'N',   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  0)  ;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("SEP","Errore durante l'inserimento della fase di avanzamento.~r~n"+sqlca.sqlerrtext)
			return -1
		end if

	end if
		
	if wf_verifica_fasi(ll_handle, ab_trovato_nuovo_componente) <> 0 then
		return 100
	end if
	
end if

ll_handle = tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_db.getitem(ll_handle,ltv_item)
	l_chiave_distinta = ltv_item.data
	
	ls_cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio
	ls_cod_versione_figlio = l_chiave_distinta.cod_versione_figlio
	ls_cod_reparto = l_chiave_distinta.cod_reparto
	ls_cod_lavorazione = l_chiave_distinta.cod_lavorazione

	SELECT  count(*)
	INTO  :ll_cont
	FROM 	avan_produzione_com
	WHERE cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa and
			num_commessa = :ll_num_commessa and
			cod_prodotto = :ls_cod_prodotto_figlio and
			cod_versione = :ls_cod_versione_figlio and
			prog_riga = :ll_prog_riga and
			cod_reparto = :ls_cod_reparto and
			cod_lavorazione = :ls_cod_lavorazione;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox( "SEP", "Errore in ricerca fase avanzamento~r~r"+sqlca.sqlerrtext)
		return -1
	end if

	if ll_cont = 0 then
		//messagebox ("SEP","Fase Mancante")
		
		//verifico se questo prodotto ha una fase di lavorazione associata; altrimenti segnalo e vado avanti
		SELECT cod_reparto,   
				cod_lavorazione 
		INTO	:ls_cod_reparto_fase,   
				:ls_cod_lavorazione_fase
		FROM 	tes_fasi_lavorazione
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto_figlio and
				cod_versione = :ls_cod_versione_figlio;

		// attenzione!! non controllare mail l'errore -1 perchè potrebbero esserci più fasi delle stesso prodotto; in questo caso prendo quella che viene su.
		if sqlca.sqlcode = 100 then
			g_mb.messagebox("SEP","Attenzione. E' stato aggiunto un semilavorato in distinta a cui non è associata alcuna fase di avanzamento.")
		end if	
		
		select cod_tipo_commessa
		into   :ls_cod_tipo_commessa
		from   anag_commesse
		where	 cod_azienda =:s_cs_xx.cod_azienda and
				 anno_commessa = :ll_anno_commessa and
				 num_commessa = :ll_num_commessa;
		
		SELECT cod_tipo_mov_reso_semilav,
				cod_tipo_mov_sfrido_semilav  
		 INTO :ls_cod_tipo_mov_reso  ,
		      :ls_cod_tipo_mov_sfrido
		 FROM tab_tipi_commessa
		 where cod_azienda =:s_cs_xx.cod_azienda and
				 cod_tipo_commessa = :ls_cod_tipo_commessa;
				 
		l_avan_produzione_com_qta.cod_prodotto = ls_cod_prodotto_figlio
		l_avan_produzione_com_qta.quan_utilizzo = ld_quan_utilizzo
		
		if not ab_trovato_nuovo_componente then
			// in caso di nuovo componente, disimpegno (solo una volta)
			select quan_in_produzione - quan_prodotta
			into   :ld_quan_impegno
			from   anag_commesse
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_commessa = :ll_anno_commessa and
					 num_commessa = :ll_num_commessa;

			if iuo_magazzino.uof_impegna_mp_commessa(true, &
																false, &
																false, &
																is_cod_prodotto_finito, &
																is_cod_versione_pf, &
																ld_quan_impegno, &
																ll_anno_commessa, &
																ll_num_commessa, &
																ref ls_errore) < 0  then
				g_mb.messagebox("SEP","Errore in disimpegno MP~r~n" + ls_errore,stopsign!)
				rollback;
			end if			
			ab_trovato_nuovo_componente = true		// questo messo a TRUE impedisce di disimpegnare una seconda volta
			
		end if				
				 
		openwithparm(w_avanz_prod_com_attivazione_qta, l_avan_produzione_com_qta )
		
		ld_quan_in_produzione = dec(message.doubleparm)
		
		//inserisco la fase di avanzamento
	  INSERT INTO avan_produzione_com  
         ( cod_azienda,   
           anno_commessa,   
           num_commessa,   
           cod_prodotto,   
           cod_versione,   
           prog_riga,   
           cod_reparto,   
           cod_lavorazione,   
           anno_reg_reso,   
           num_reg_reso,   
           tempo_attrezzaggio,   
           tempo_attrezzaggio_commessa,   
           tempo_lavorazione,   
           quan_in_produzione,   
           quan_prodotta,   
           flag_fine_fase,   
           cod_tipo_mov_reso,   
           quan_utilizzata,   
           cod_tipo_mov_sfrido,   
           anno_reg_sfrido,   
           num_reg_sfrido,   
           quan_reso,   
           quan_sfrido,   
           quan_scarto,   
           tempo_risorsa_umana,   
           anno_reg_des_mov_reso,   
           num_reg_des_mov_reso,   
           anno_reg_des_mov_sfrido,   
           num_reg_des_mov_sfrido,   
           flag_esterna,   
           tempo_movimentazione,   
           flag_bolla_uscita,   
           anno_reg_bol_ven,   
           num_reg_bol_ven,   
           prog_riga_bol_ven,   
           anno_reg_sl,   
           num_reg_sl,   
           ordinamento )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ll_anno_commessa,   
           :ll_num_commessa,   
           :ls_cod_prodotto_figlio,   
           :ls_cod_versione_figlio,   
           :ll_prog_riga,   
           :ls_cod_reparto_fase,   
           :ls_cod_lavorazione_fase,   
           null,   
           null,   
           0,   
           0,   
           0,   
           :ld_quan_in_produzione,   
           0,   
           'N',   
           :ls_cod_tipo_mov_reso,   
           0,   
           :ls_cod_tipo_mov_sfrido,   
           null,   
           null,   
           0,   
           0,   
           0,   
           0,   
           null,   
           null,   
           null,   
           null,   
           'N',   
           0,   
           'N',   
           null,   
           null,   
           null,   
           null,   
           null,   
           0)  ;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("SEP","Errore durante l'inseriemnto della fase di avanzamento.~r~n"+sqlca.sqlerrtext)
			return -1
		end if
		
	end if
	
	if wf_verifica_fasi(ll_handle, ab_trovato_nuovo_componente) <> 0 then
		return 100
	end if
	
end if

return 0
end function

public function integer wf_controlla_fasi_sl_ricorsivo (long al_handle, long fl_handle_primo_livello[], ref boolean ab_tutte_chiuse);string ls_cod_prodotto_figlio,ls_cod_versione_figlio,ls_cod_reparto,ls_cod_lavorazione
long ll_handle, ll_padre, ll_anno_commessa, ll_num_commessa, ll_prog_riga, ll_index
dec{4} ld_quan_utilizzo
string ls_flag_fine_fase
treeviewitem ltv_item
s_chiave_distinta l_chiave_distinta
boolean lb_sl_primo_livello = false

if not ab_tutte_chiuse then return 100
if al_handle <= 0 or isnull(al_handle) then return 100

//comincio dal primo figlio
ll_handle = tv_db.finditem(childtreeitem!,al_handle)

ll_anno_commessa = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"anno_commessa")
ll_num_commessa  = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"num_commessa")
ll_prog_riga     = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"prog_riga")

if ll_handle > 0 then
	
	tv_db.getitem(ll_handle,ltv_item)
	l_chiave_distinta = ltv_item.data
	
	ls_cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio
	ls_cod_versione_figlio = l_chiave_distinta.cod_versione_figlio
	ls_cod_reparto = l_chiave_distinta.cod_reparto
	ls_cod_lavorazione = l_chiave_distinta.cod_lavorazione
	ld_quan_utilizzo = l_chiave_distinta.quan_utilizzo
	
	//leggi nella avan_produzione_com se la fase del semilavorato figlio risulta chiusa
	SELECT flag_fine_fase
	INTO  :ls_flag_fine_fase
	FROM 	avan_produzione_com
	WHERE cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa and
			num_commessa = :ll_num_commessa and
			cod_prodotto = :ls_cod_prodotto_figlio and
			cod_versione = :ls_cod_versione_figlio and
			prog_riga = :ll_prog_riga and
			cod_reparto = :ls_cod_reparto and
			cod_lavorazione = :ls_cod_lavorazione;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox( "SEP", "Errore in ricerca flag fine fase.~r~r"+sqlca.sqlerrtext)
		return -1
	end if	
	if sqlca.sqlcode = 100 then
		//fase non trovata: forse è un nodo MP perchè l'utente le ha rese visibili sull'albero
		//in ogni caso considera come se la fase fosse chiusa
		ls_flag_fine_fase = "S"
	end if

	/*
	if wf_controlla_fasi_sl_ricorsivo(ll_handle, fl_handle_primo_livello, ab_tutte_chiuse) <> 0 then
		return 100
	end if
	*/
	if ls_flag_fine_fase = "S" then
		if wf_controlla_fasi_sl_ricorsivo(ll_handle, fl_handle_primo_livello, ab_tutte_chiuse) <> 0 then
			return 100
		end if
	else
		//ho trovato un prodotto con fase non chiusa
		//metto il semaforo di "tutte chiuse" a false
		ab_tutte_chiuse = false
		return 100
	end if	
end if

//proseguo nell'albero allo stesso livello
ll_handle = tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	//se si tratta di un handle di un SL di primo livello escludilo dal controllo
	for ll_index = 1 to upperbound(fl_handle_primo_livello)
		if ll_handle = fl_handle_primo_livello[ll_index] then
			lb_sl_primo_livello = true
			exit
		end if
	next
	
	if not lb_sl_primo_livello then
		tv_db.getitem(ll_handle,ltv_item)
		l_chiave_distinta = ltv_item.data
		
		ls_cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio
		ls_cod_versione_figlio = l_chiave_distinta.cod_versione_figlio
		ls_cod_reparto = l_chiave_distinta.cod_reparto
		ls_cod_lavorazione = l_chiave_distinta.cod_lavorazione
	
		//leggi nella avan_produzione_com se la fase del semilavorato risulta chiusa
		SELECT flag_fine_fase
		INTO  :ls_flag_fine_fase
		FROM 	avan_produzione_com
		WHERE cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :ll_anno_commessa and
				num_commessa = :ll_num_commessa and
				cod_prodotto = :ls_cod_prodotto_figlio and
				cod_versione = :ls_cod_versione_figlio and
				prog_riga = :ll_prog_riga and
				cod_reparto = :ls_cod_reparto and
				cod_lavorazione = :ls_cod_lavorazione;
	
		if sqlca.sqlcode = -1 then
			g_mb.messagebox( "SEP", "Errore in ricerca flag fine fase.~r~r"+sqlca.sqlerrtext)
			return -1
		end if
		if sqlca.sqlcode = 100 then
			//fase non trovata: forse è un nodo MP perchè l'utente le ha rese visibili sull'albero
			//in ogni caso considera come se la fase fosse chiusa
			ls_flag_fine_fase = "S"
		end if
		
		/*
		if wf_controlla_fasi_sl_ricorsivo(ll_handle, fl_handle_primo_livello, ab_tutte_chiuse) <> 0 then
			return 100
		end if
		*/
		if ls_flag_fine_fase = "S" then		
			if wf_controlla_fasi_sl_ricorsivo(ll_handle, fl_handle_primo_livello, ab_tutte_chiuse) <> 0 then
				return 100
			end if
		else
			//ho trovato un prodotto con fase non chiusa
			//metto il semaforo di "tutte chiuse" a false
			ab_tutte_chiuse = false
			return 100
		end if	
	else
		//si tratta di un handle di un SL di primo livello escludilo dal controllo
		//e vai a controllare i nodi successivi, se ce ne sono
		if wf_controlla_fasi_sl_ricorsivo(ll_handle, fl_handle_primo_livello, ab_tutte_chiuse) <> 0 then
			return 100
		end if
		/*
		if ls_flag_fine_fase = "S" then		
			if wf_controlla_fasi_sl_ricorsivo(ll_handle, fl_handle_primo_livello, ab_tutte_chiuse) <> 0 then
				return 100
			end if
		else
			//ho trovato un prodotto con fase non chiusa
			//metto il semaforo di "tutte chiuse" a false
			ab_tutte_chiuse = false
			return 100
		end if	
		*/
	end if
end if

return 0
end function

public function boolean wf_controlla_fasi_sl ();boolean lb_tutte_chiuse = true
long ll_handle, ll_handle_2, ll_anno_commessa, ll_num_commessa, ll_prog_riga
long ll_sl_primo_livello[], ll_i
s_chiave_distinta l_chiave_distinta
string ls_cod_prodotto_figlio, ls_cod_versione_figlio, ls_cod_reparto, ls_cod_lavorazione, ls_flag_fine_fase
decimal{4} ld_quan_utilizzo

//N.B passare per iniziare l'handle del primo SL di 1° livello
ll_handle = tv_db.finditem(childtreeitem!, 1)
ll_handle_2 = ll_handle
ll_i = 0

//carica tutti gli handles dei SL di primo livello
do while ll_handle_2 > 0
	ll_i += 1
	ll_sl_primo_livello[ll_i] = ll_handle_2
	ll_handle_2 = tv_db.finditem(NextTreeItem!, ll_handle_2)
loop

wf_controlla_fasi_sl_ricorsivo(ll_handle, ll_sl_primo_livello, lb_tutte_chiuse)

//if not lb_tutte_chiuse then
//	messagebox("","alcune fasi non sono chiuse")
//else
//	messagebox("","tutte chiuse")
//end if

//VALORE DI RITORNO --------------------------------
//true:	le fasi di 2° livello sono tutte chiuse
//false:	alcune fasi di 2° livello non sono chiuse
return lb_tutte_chiuse
end function

public function double wf_controlla_qta_pf ();double ld_return = 0
long ll_handle, ll_sl_primo_livello[], ll_i
long ll_anno_commessa, ll_num_commessa, ll_prog_riga
treeviewitem ltv_item
s_chiave_distinta l_chiave_distinta
string ls_cod_prodotto_figlio, ls_cod_versione_figlio, ls_cod_reparto, ls_cod_lavorazione
double ld_quan_utilizzo, ld_quan_prodotta, ld_quan_pf_temp, ld_quan_pf_temp_min
double ld_quan_prodotta_pf, ldd_quan_utilizzata
string ls_cod_prodotto_pf

//handle del primo SL di 1° livello
ll_handle = tv_db.finditem(childtreeitem!, 1)
ll_i = 0

//carica tutti gli handles dei SL di primo livello
do while ll_handle > 0
	ll_i += 1
	ll_sl_primo_livello[ll_i] = ll_handle
	ll_handle = tv_db.finditem(NextTreeItem!, ll_handle)
loop

ll_anno_commessa = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"anno_commessa")
ll_num_commessa  = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"num_commessa")
ll_prog_riga     = dw_avanzamento_produzione_commesse.i_parentdw.getitemnumber(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"prog_riga")

select quan_prodotta
into :ld_quan_prodotta_pf
from det_anag_commesse
where cod_azienda=:s_cs_xx.cod_azienda
	and anno_commessa=:ll_anno_commessa and num_commessa=:ll_num_commessa
	and prog_riga=:ll_prog_riga;
	
if sqlca.sqlcode = -1 then
	g_mb.messagebox("SEP","Errore durante la lettura della qta PF prodotta!",StopSign!)
	return 0
end if
if isnull(ld_quan_prodotta_pf) then ld_quan_prodotta_pf = 0

//processa tutti i SL di primo livello e per calcolare il val minimo possibile di qta di PF caricabile parzialmente
for ll_i = 1 to upperbound(ll_sl_primo_livello)
	ll_handle = ll_sl_primo_livello[ll_i]

	if ll_handle > 0 then
		tv_db.getitem(ll_handle,ltv_item)
		l_chiave_distinta = ltv_item.data
	
		ls_cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio
		ls_cod_versione_figlio = l_chiave_distinta.cod_versione_figlio
		ls_cod_reparto = l_chiave_distinta.cod_reparto
		ls_cod_lavorazione = l_chiave_distinta.cod_lavorazione
		ld_quan_utilizzo = l_chiave_distinta.quan_utilizzo
	
		
		//leggi nella avan_produzione_com la qta prodotta del SL
		SELECT quan_prodotta
		INTO  :ld_quan_prodotta
		FROM 	avan_produzione_com
		WHERE cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :ll_anno_commessa and
				num_commessa = :ll_num_commessa and
				cod_prodotto = :ls_cod_prodotto_figlio and
				cod_versione = :ls_cod_versione_figlio and
				prog_riga = :ll_prog_riga and
				cod_reparto = :ls_cod_reparto and
				cod_lavorazione = :ls_cod_lavorazione;
		
		
		 select cod_prodotto
		into   :ls_cod_prodotto_pf
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:ll_anno_commessa
		and    num_commessa=:ll_num_commessa;
				 				 
		//leggi nella mp_com_stock_det_orari quella già utilizzata in questa commessa
		select sum(quan_utilizzata)
		 into :ldd_quan_utilizzata
		 from mp_com_stock_det_orari
		 WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
		 anno_commessa = :ll_anno_commessa  AND    
		 num_commessa = :ll_num_commessa  	AND    
		 prog_riga = :ll_prog_riga 			AND    
		 cod_prodotto = :ls_cod_prodotto_figlio  AND
		 cod_prodotto_fase = :ls_cod_prodotto_pf and
		 cod_reparto = :ls_cod_reparto  		AND    
		 cod_lavorazione = :ls_cod_lavorazione;		
		
		if isnull(ldd_quan_utilizzata) then ldd_quan_utilizzata = 0
		
		//quella effettivamente disponibile è per la produzione del pf è:
		//(cioè quella finora prodotta meno quella già utilizzata)
		ld_quan_prodotta = ld_quan_prodotta - ldd_quan_utilizzata

		if sqlca.sqlcode = -1 then
			g_mb.messagebox( "SEP", "Errore in ricerca quantita prodotta.~r~r"+sqlca.sqlerrtext)
			return 0
		end if
		
		if isnull(ld_quan_prodotta) then ld_quan_prodotta = 0
		
		//determino la qta di PF caricabile con la qta prodotta di tale SL
		ld_quan_pf_temp = truncate(ld_quan_prodotta / ld_quan_utilizzo - ld_quan_prodotta_pf, 0)
		if ll_i = 1 then
			//sono al primo step
			
			//salvo la qta, mi servirà al successivo step (se ci sono altri SL di primo livello)
			ld_quan_pf_temp_min = ld_quan_pf_temp	
		else
			
			//se ho ottenuto 0 non posso produrre PF perchè il SL corrente non ha la qta sufficiente
			if ld_quan_pf_temp = 0 then return 0
			
			if ld_quan_pf_temp < ld_quan_pf_temp_min then
				//ho ottenuto una qta PF inferiore: me lo conservo per il prossimo step (se ci sono altri SL di primo livello)
				ld_quan_pf_temp_min = ld_quan_pf_temp
			end if
		end if
	end if
next

if isnull(ld_quan_pf_temp_min) then ld_quan_pf_temp_min = 0
return ld_quan_pf_temp_min
end function

public function integer wf_mov_mag_chiusura_fasi_parziale (long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga, double fd_qta_pf, string fs_errore);//	Funzione che esegue i movimenti di magazzino del PF, dei semilavorati (reso e sfrido) 
//	e mat_prime (reso e sfrido) al momento della chiusura di tutte le fasi di lavoro
// 
// tipo integer: 0 passed
//					 -1 failed
//

double   ldd_quan_assegnata_st, ldd_quan_assegnata_mp,ldd_quan_prodotta, &
		   ldd_quan_reso, ldd_quan_sfrido,ldd_quan_impegnata_st, &
			ldd_quan_tecnica,ldd_quan_utilizzo,ldd_dim_x,ldd_dim_y,ldd_dim_z, &
			ldd_dim_t, ldd_coef_calcolo,ldd_quan_anticipo
long     ll_prog_stock[],ll_anno_reg_des_mov, ll_num_reg_des_mov,ll_anno_registrazione[], &
		   ll_num_registrazione[],ll_t,ll_num_sequenza,ll_prog_riga_anticipo, ll_anno_registrazione_a[], & 
			ll_num_registrazione_a[],ll_anno_reg_mov_mag_a,ll_num_reg_mov_mag_a,ll_progressivo
string   ls_errore,ls_cod_tipo_commessa,ls_cod_tipo_movimento,ls_cod_prodotto_finito, & 
		   ls_cod_deposito[], ls_cod_tipo_mov_reso, ls_cod_tipo_mov_sfrido, ls_cod_prodotto,& 
		   ls_cod_ubicazione[],ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[], &
		   ls_cod_reparto,ls_cod_lavorazione,ls_test,ls_cod_prodotto_padre,ls_cod_prodotto_figlio, ls_cod_versione_avan_prod, &
			ls_cod_versione,ls_flag_esclusione,ls_formula_tempo,ls_cod_prodotto_variante, & 
			ls_cod_tipo_mov_anticipo,ls_cod_lotto_test,ls_cod_ubicazione_test, & 
			ls_flag_fornitore,ls_flag_cliente,ls_flag_materia_prima,ls_des_estesa, ls_cod_versione_figlio, ls_cod_versione_variante, &
			ls_ubicazione,ls_lotto,ls_cod_ubicazione_pf,ls_cod_lotto_pf,ls_anno_commessa,ls_flag_calcola_costo_pf, ls_cod_versione_finito, ls_flag
integer  li_risposta
datetime ld_data_stock[]
dec{4}   ld_costo_mp, ld_costo_lav, ld_costo_tot

uo_magazzino luo_mag
uo_costificazione_pf luo_costo


// (inizio) MUOVE PRODOTTO FINITO***********************************************************

ls_anno_commessa = string(fl_anno_commessa)
setnull(ls_cod_ubicazione_pf)
setnull(ls_cod_lotto_pf)

select cod_prodotto,
		 cod_versione,
		 cod_deposito_versamento,
		 ubicazione,
		 lotto,
		 cod_tipo_commessa
into   :ls_cod_prodotto_finito,
		 :ls_cod_versione_finito,
		 :ls_cod_deposito[1],
		 :ls_ubicazione,
		 :ls_lotto,
		 :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:fl_anno_commessa
and    num_commessa=:fl_num_commessa;


if sqlca.sqlcode < 0 then
	fs_errore = "Errore sul db: " + sqlca.sqlerrtext
	return -1
end if


// *** se il carico prodotto finito / scarico sml è disattivato non fo nulla

select flag_disattiva_cpf
into   :ls_flag
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore durante ricerca disattiva cpf:" + sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_flag) and ls_flag = "S" then return 0

select flag_calcola_costo_pf
into   :ls_flag_calcola_costo_pf
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore sul db: " + sqlca.sqlerrtext
	return -1
end if

select cod_tipo_movimento,
		 quan_prodotta,
		 quan_anticipo,
		 cod_tipo_mov_anticipo
into   :ls_cod_tipo_movimento,
	 	 :ldd_quan_prodotta,
		 :ldd_quan_anticipo,
		 :ls_cod_tipo_mov_anticipo
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:fl_anno_commessa
and    num_commessa=:fl_num_commessa
and    prog_riga=:fl_prog_riga;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore sul db: " + sqlca.sqlerrtext
	return -1
end if

//metto in "ldd_quan_prodotta" la qta di PF da movimentare, sovrascrivendola
ldd_quan_prodotta = fd_qta_pf

declare righe_det_tipi_m cursor for
select  cod_ubicazione,
		  cod_lotto,
		  flag_cliente,
		  flag_fornitore
from    det_tipi_movimenti
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_movimento=:ls_cod_tipo_movimento;

open righe_det_tipi_m;

do while 1=1
	fetch righe_det_tipi_m
	into  :ls_cod_ubicazione_test,
			:ls_cod_lotto_test,
			:ls_flag_cliente,
			:ls_flag_fornitore;

	if sqlca.sqlcode = 100 then exit
		
	if sqlca.sqlcode < 0 then
		fs_errore="Si e' verificato un errore in lettura det_tipi_movimenti: " + sqlca.sqlerrtext
		close righe_det_tipi_m;
		return -1
	end if

	if isnull(ls_ubicazione) or ls_ubicazione="" then
		if isnull(ls_cod_ubicazione_test) then
			fs_errore="Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_movimento +" come versamento di prodotto finito per commesse, poiche' il codice ubicazione non e' stato indicato." 
			close righe_det_tipi_m;
			return -1
		end if
	else
		ls_cod_ubicazione_pf=ls_ubicazione
	end if

	if isnull(ls_lotto) or ls_lotto="" then
		if isnull(ls_cod_lotto_test) then
			fs_errore="Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_movimento +" come versamento di prodotto finito per commesse, poiche' il codice lotto non e' stato indicato." 
			close righe_det_tipi_m;
			return -1
		end if
	else
		ls_cod_lotto_pf=ls_lotto
	end if

	if ls_flag_cliente='S' then
		fs_errore="Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_movimento +" come versamento di prodotto finito per commesse, poiche' viene richiesto il codice cliente." 
		close righe_det_tipi_m;
		return -1
	end if

	if ls_flag_fornitore='S' then
		fs_errore="Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_movimento +" come versamento di prodotto finito per commesse, poiche' viene richiesto il codice fornitore." 
		close righe_det_tipi_m;
		return -1
	end if

loop

close righe_det_tipi_m;

setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ld_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

if not isnull(ls_cod_ubicazione_pf)  then
	ls_cod_ubicazione[1] = ls_cod_ubicazione_pf
end if

if not isnull(ls_cod_lotto_pf)  then
	ls_cod_lotto[1] = ls_cod_lotto_pf
end if

if ldd_quan_anticipo > 0 then // se > 0 allora dalla tabella det_anag_comm_anticipi 
								  // vado a prendere lo stock usato dal primo record di anticipo
	select min(prog_riga_anticipo)
	into   :ll_prog_riga_anticipo
	from   det_anag_comm_anticipi
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:fl_anno_commessa
	and    num_commessa=:fl_num_commessa
	and    prog_riga=:fl_prog_riga;

	if sqlca.sqlcode<>0 then 
	  fs_errore = "Si è verificato un errore sul DB:"+ sqlca.sqlerrtext
	  return -1
	end if

	select anno_reg_mov_mag,
			 num_reg_mov_mag
	into   :ll_anno_reg_mov_mag_a,
			 :ll_num_reg_mov_mag_a
	from   det_anag_comm_anticipi
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:fl_anno_commessa
	and    num_commessa=:fl_num_commessa
	and    prog_riga=:fl_prog_riga
	and    prog_riga_anticipo=:ll_prog_riga_anticipo;

	if sqlca.sqlcode<>0 then 
	  fs_errore = "Si è verificato un errore sul DB:"+ sqlca.sqlerrtext
	  return -1
	end if

	select cod_ubicazione,
			 cod_lotto,
			 data_stock,
			 prog_stock
	into 	 :ls_cod_ubicazione[1],
			 :ls_cod_lotto[1],
			 :ld_data_stock[1],
			 :ll_prog_stock[1]
	from   mov_magazzino
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_reg_mov_mag_a
	and    num_registrazione=:ll_num_reg_mov_mag_a;

	if sqlca.sqlcode<>0 then 
	  fs_errore = "Si è verificato un errore sul DB:"+ sqlca.sqlerrtext
	  return -1
	end if

end if

if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto_finito, ls_cod_deposito[], & 
									  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
									  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
									  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then

	fs_errore="Si è verificato un errore in creazione movimenti"

	return -1

end if
	
if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
									ls_cod_tipo_movimento, ls_cod_prodotto_finito) = -1 then
		
  fs_errore="Si è verificato un errore in fase di verifica destinazioni movimenti magazzino"

  return -1

end if

//***************** INIZIO CALCOLO DEL COSTO DEL PRODOTTO FINITO

if ls_flag_calcola_costo_pf ='S' then
	luo_costo = create uo_costificazione_pf
	
	li_risposta = luo_costo.uof_costificazione_pf(sqlca,&
																 s_cs_xx.cod_azienda,&
																 long(fl_anno_commessa),&
																 fl_num_commessa,&
																 ldd_quan_prodotta, &
													 			 ld_costo_mp,&
																 ld_costo_lav,&
																 ld_costo_tot,&
																 ls_errore)
	destroy luo_costo
else
	ld_costo_mp = 0
	ld_costo_lav = 0
	ld_costo_tot = 0
end if

//***************** FINE CALCOLO DEL COSTO DEL PRODOTTO FINITO


luo_mag = create uo_magazzino

li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
	 						 			 ls_cod_tipo_movimento, &
			  							 "N", &
										 ls_cod_prodotto_finito, &
										 ldd_quan_prodotta, &
										 (ld_costo_tot/ldd_quan_prodotta), &
										 fl_num_commessa, &
										 datetime(today(),time('00:00:00')), &
										 ls_anno_commessa, &
										 ll_anno_reg_des_mov, &
										 ll_num_reg_des_mov, &
										 ls_cod_deposito[], &
										 ls_cod_ubicazione[], &
										 ls_cod_lotto[], &
										 ld_data_stock[], &
										 ll_prog_stock[], &
										 ls_cod_fornitore[], &
										 ls_cod_cliente[], &
										 ll_anno_registrazione[], &
										 ll_num_registrazione[])
										 
destroy luo_mag

if li_risposta=-1 then
	fs_errore="Errore su movimenti magazzino."
	return -1
end if

li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)

if li_risposta = -1 then
	fs_errore="Si è verificato un errore in fase di cancellazione destinazioni movimenti."
	return -1
end if

// dopo il carico del prodotto finito a magazzino aggiorno la tabella varianti_stock

for ll_t = 1 to upperbound(ls_cod_deposito)
	declare righe_var_com cursor for
	select cod_prodotto_padre,
			 cod_versione,
			 num_sequenza,
			 cod_prodotto_figlio,
			 cod_versione_figlio,
			 cod_prodotto,
			 cod_versione_variante,
			 quan_tecnica,
			 quan_utilizzo,
			 dim_x,
			 dim_y,
			 dim_z,
			 dim_t,
			 coef_calcolo,
			 flag_esclusione,
			 formula_tempo,
			 flag_materia_prima,
			 des_estesa
	from   varianti_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:fl_anno_commessa
	and    num_commessa=:fl_num_commessa;
	
	open righe_var_com;
	
	do while 1=1
		fetch righe_var_com
		into  :ls_cod_prodotto_padre,
				:ls_cod_versione,
				:ll_num_sequenza,
				:ls_cod_prodotto_figlio,
				:ls_cod_versione_figlio,
				:ls_cod_prodotto_variante,
				:ls_cod_versione_variante,
				:ldd_quan_tecnica,
				:ldd_quan_utilizzo,
				:ldd_dim_x,
				:ldd_dim_y,
				:ldd_dim_z,
				:ldd_dim_t,
				:ldd_coef_calcolo,
				:ls_flag_esclusione,
				:ls_formula_tempo,
				:ls_flag_materia_prima,
				:ls_des_estesa;
		
		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode < 0 then
			fs_errore="Si è verificato un errore in fase di aggiornamento tabella varianti stock:" + sqlca.sqlerrtext
		  	close righe_var_com;
			return -1
		end if

		select max(progressivo)
		into   :ll_progressivo
		from   varianti_stock
		where  cod_azienda = :s_cs_xx.cod_azienda 		and    
		       cod_prodotto = :ls_cod_prodotto_finito   and
				 cod_prodotto_padre = :ls_cod_prodotto_padre and
				 cod_versione = :ls_cod_versione				and
				 cod_prodotto_figlio = :ls_cod_prodotto_figlio and
				 cod_versione_figlio = :ls_cod_versione_figlio and    
				 cod_deposito = :ls_cod_deposito[ll_t]		and    
				 cod_ubicazione = :ls_cod_ubicazione[ll_t]	and    
				 cod_lotto = :ls_cod_lotto[ll_t]				and    
				 data_stock = :ld_data_stock[ll_t]			and    
				 prog_stock = :ll_prog_stock[ll_t]			and    
				 num_sequenza = :ll_num_sequenza;
		
		if sqlca.sqlcode < 0 then
			fs_errore="Si è verificato un errore in fase di aggiornamento tabella varianti stock:" + sqlca.sqlerrtext
		  	close righe_var_com;
			return -1
		end if
		
		if isnull(ll_progressivo) or ll_progressivo=0 then
			ll_progressivo = 1
		else
			ll_progressivo++
		end if

	   INSERT INTO varianti_stock  
         ( cod_azienda,   
           cod_prodotto, 			  
           cod_deposito,   
           cod_ubicazione,   
           cod_lotto,   
           data_stock,   
           prog_stock,   
           cod_prodotto_padre,   
           num_sequenza,   
           cod_prodotto_figlio,   
           cod_versione, 
			  cod_versione_figlio,
			  progressivo,
           cod_prodotto_variante, 
			  cod_versione_variante,
           quan_tecnica,   
           quan_utilizzo,   
           dim_x,   
           dim_y,   
           dim_z,   
           dim_t,   
           coef_calcolo,   
           flag_esclusione,   
           formula_tempo,
			  flag_materia_prima,
			  des_estesa)  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_prodotto_finito,   
           :ls_cod_deposito[ll_t],   
           :ls_cod_ubicazione[ll_t],   
           :ls_cod_lotto[ll_t],   
           :ld_data_stock[ll_t],   
           :ll_prog_stock[ll_t],   
           :ls_cod_prodotto_padre,   
           :ll_num_sequenza,   
           :ls_cod_prodotto_figlio,   
           :ls_cod_versione,  
			  :ls_cod_versione_figlio,
			  :ll_progressivo,
           :ls_cod_prodotto_variante, 
			  :ls_cod_versione_variante,
           :ldd_quan_tecnica,   
           :ldd_quan_utilizzo,   
           :ldd_dim_x,   
           :ldd_dim_y,   
           :ldd_dim_z,   
           :ldd_dim_t,   
           :ldd_coef_calcolo,   
           :ls_flag_esclusione,   
           :ls_formula_tempo,
			  :ls_flag_materia_prima,
			  :ls_des_estesa)  ;
		
		if sqlca.sqlcode < 0 then
			fs_errore="Si è verificato un errore in fase di aggiornamento tabella varianti stock:" + sqlca.sqlerrtext
		  	close righe_var_com;
			return -1
		end if

	loop
	
	close righe_var_com;

next

// fine aggiornamento tabella varianti_stock	

// se anticipo > 0 effettuo movimento di scarico anticipo
setnull(ll_anno_registrazione_a[1])
setnull(ll_num_registrazione_a[1])
if ldd_quan_anticipo > 0 then
   if isnull(ls_cod_tipo_mov_anticipo) then 
		fs_errore="Il tipo movimento anticipi non è stato configurato, perciò l'operazione viene annullata."
		return -1
	end if

	if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_anticipo, ls_cod_prodotto_finito, ls_cod_deposito[], & 
										  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
										  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
										  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
	
		fs_errore="Si è verificato un errore in creazione movimenti anticipo"
	
		return -1
	
	end if
		
	if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
										ls_cod_tipo_mov_anticipo, ls_cod_prodotto_finito) = -1 then
			
	  fs_errore="Si è verificato un errore in fase di verifica destinazioni movimenti magazzino anticipo"
	
	  return -1
	
	end if
	
	luo_mag = create uo_magazzino
	
	li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
											 ls_cod_tipo_mov_anticipo, &
											 "N", &
											 ls_cod_prodotto_finito, &
											 ldd_quan_anticipo, &
											 1, &
											 fl_num_commessa, &
											 datetime(today(),time('00:00:00')), &
											 ls_anno_commessa, &
											 ll_anno_reg_des_mov, &
											 ll_num_reg_des_mov, &
											 ls_cod_deposito[], &
											 ls_cod_ubicazione[], &
											 ls_cod_lotto[], &
											 ld_data_stock[], &
											 ll_prog_stock[], &
											 ls_cod_fornitore[], &
											 ls_cod_cliente[], &
											 ll_anno_registrazione_a[], &
											 ll_num_registrazione_a[])
											 
	destroy luo_mag
	
	if li_risposta=-1 then
		fs_errore="Errore su movimenti magazzino (anticipi)."
		return -1
	end if
	
	li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)
	
	if li_risposta = -1 then
		fs_errore="Si è verificato un errore in fase di cancellazione destinazioni movimenti."
		return -1
	end if
end if

// fine movimento per gli anticipi

update det_anag_commesse
set    anno_registrazione=:ll_anno_registrazione[1],
		 num_registrazione=:ll_num_registrazione[1],
		 anno_reg_anticipo=:ll_anno_registrazione_a[1],
		 num_reg_anticipo=:ll_num_registrazione_a[1]
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:fl_anno_commessa
and    num_commessa=:fl_num_commessa
and    prog_riga=:fl_prog_riga;

if sqlca.sqlcode<>0 then 
  fs_errore = "Si è verificato un errore sul DB:"+ sqlca.sqlerrtext
  return -1
end if

update anag_commesse
set	 tot_valore_prodotti = tot_valore_prodotti + :ld_costo_mp,
		 tot_valore_servizi=tot_valore_servizi + :ld_costo_lav,
		 tot_valore_pf= tot_valore_pf + :ld_costo_tot
where	 cod_azienda = :s_cs_xx.cod_azienda  
and	 anno_commessa = :fl_anno_commessa
and	 num_commessa = :fl_num_commessa;

if sqlca.sqlcode<>0 then 
  fs_errore = "Si è verificato un errore sul DB:"+ sqlca.sqlerrtext
  return -1
end if

select cod_azienda
into   :ls_test
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:fl_anno_commessa
and    num_commessa=:fl_num_commessa;


//*************************************************************************************
//LA PRODUZIONE NON DEVE MODIFICARE LA QUAN_IMPEGNATA DEL PRODOTTO FINITO MA SOLO LE MP
//if sqlca.sqlcode=100 then // se commessa interna (non legata ad un ordine) allora
//								  // disimpegno la qtà. prodotta
//	select quan_impegnata
//	into   :ldd_quan_impegnata_st
//	from   anag_prodotti
//	where  cod_azienda = :s_cs_xx.cod_azienda  
//	and    cod_prodotto = :ls_cod_prodotto_finito;
//
//	ldd_quan_impegnata_st = ldd_quan_impegnata_st - ldd_quan_prodotta
//	
//	UPDATE anag_prodotti
//	SET    quan_impegnata =:ldd_quan_impegnata_st
//	WHERE  cod_azienda = :s_cs_xx.cod_azienda  
//	AND    cod_prodotto = :ls_cod_prodotto_finito;
//	
//	if sqlca.sqlcode<>0 then 
//	  fs_errore = "Si è verificato un errore sul DB:"+ sqlca.sqlerrtext
//	  return -1
//	end if
//
//end if
//
//******************************************************************************************



// (fine) MUOVE PRODOTTO FINITO*************************************************************


// (inizio) MUOVE SEMILAVORATI (RESI/SFRIDI)************************************************

declare righe_avan_prod_com cursor for
select  cod_reparto,
		  cod_lavorazione,
		  cod_prodotto,
		  cod_versione,
		  quan_reso,
		  quan_sfrido,
		  cod_tipo_mov_reso,
		  cod_tipo_mov_sfrido
from    avan_produzione_com
where   cod_azienda=:s_cs_xx.cod_azienda
and     anno_commessa=:fl_anno_commessa
and     num_commessa=:fl_num_commessa
and     prog_riga=:fl_prog_riga;

open righe_avan_prod_com;

do while 1=1
	fetch righe_avan_prod_com
	into  :ls_cod_reparto,
			:ls_cod_lavorazione,
			:ls_cod_prodotto,
			:ls_cod_versione_avan_prod,
			:ldd_quan_reso,
			:ldd_quan_sfrido,
			:ls_cod_tipo_mov_reso,
		   :ls_cod_tipo_mov_sfrido;

	if sqlca.sqlcode<> 0 then exit 

	if ldd_quan_reso=0 and ldd_quan_sfrido=0 then continue

	
	setnull(ls_cod_deposito[1])
   setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ld_data_stock[1])
	setnull(ll_prog_stock[1]) 
	setnull(ls_cod_cliente[1]) 
	setnull(ls_cod_fornitore[1])
	setnull(ll_anno_reg_des_mov) 
	setnull(ll_num_reg_des_mov)

	// (Inizio) RESI*************************************************************************
	if ldd_quan_reso <> 0 then
		if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_reso, ls_cod_prodotto, ls_cod_deposito[], & 
											  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
											  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
											  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then

		  fs_errore="Si è verificato un errore in creazione movimenti"
 		  close righe_avan_prod_com;
		  return -1
		end if

		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
											 ls_cod_tipo_mov_reso, ls_cod_prodotto) = -1 then

		  fs_errore="Si è verificato un errore in fase di verifica destinazioni movimenti magazzino"
 		  close righe_avan_prod_com;
		  return -1

		end if
		
		luo_mag = uo_magazzino

		li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
	 							 			 	ls_cod_tipo_mov_reso, &
				  							 	"N", &
												ls_cod_prodotto, &
											   ldd_quan_reso, &
											   1, &
											   fl_num_commessa, &
											   datetime(today(),time('00:00:00')), &
											   ls_anno_commessa, &
											   ll_anno_reg_des_mov, &
											   ll_num_reg_des_mov, &
											   ls_cod_deposito[], &
											   ls_cod_ubicazione[], &
											   ls_cod_lotto[], &
											   ld_data_stock[], &
											   ll_prog_stock[], &
											   ls_cod_fornitore[], &
											   ls_cod_cliente[], &
											   ll_anno_registrazione[], &
											   ll_num_registrazione[])
												
		destroy luo_mag

		if li_risposta=-1 then
			fs_errore="Errore su movimenti magazzino."
			return -1
		end if

		li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)

		if li_risposta = -1 then
			fs_errore="Si è verificato un errore in fase di cancellazione destinazioni movimenti."
			return -1
		end if

		update avan_produzione_com
		set    anno_reg_reso=:ll_anno_registrazione[1],
				 num_reg_reso=:ll_num_registrazione[1]
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
		       anno_commessa = :fl_anno_commessa		and    
				 num_commessa = :fl_num_commessa			and    
				 prog_riga = :fl_prog_riga					and    
				 cod_prodotto = :ls_cod_prodotto			and
				 cod_versione = :ls_cod_versione_avan_prod 		and    
				 cod_reparto = :ls_cod_reparto			and    
				 cod_lavorazione = :ls_cod_lavorazione;
		
      if sqlca.sqlcode<>0 then 
		  fs_errore = "Si è verificato un errore sul DB:"+ sqlca.sqlerrtext
		  return -1
		end if

	end if
	// (fine) RESI***************************************************************************	



	// (Inizio) SFRIDI***********************************************************************
	if ldd_quan_sfrido <> 0 then
		if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_sfrido, ls_cod_prodotto, ls_cod_deposito[], & 
											  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
											  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
											  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then

		  fs_errore="Si è verificato un errore in creazione movimenti"
 		  close righe_avan_prod_com;
		  return -1
		end if

		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
											 ls_cod_tipo_mov_sfrido, ls_cod_prodotto) = -1 then
			
		  fs_errore="Si è verificato un errore in fase di verifica destinazioni movimenti magazzino"
 		  close righe_avan_prod_com;
		  return -1
		end if
		
		luo_mag = create uo_magazzino

		li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
	 							 			 	ls_cod_tipo_mov_sfrido, &
				  							 	"N", &
												ls_cod_prodotto, &
											   ldd_quan_reso, &
											   1, &
											   fl_num_commessa, &
											   datetime(today(),time('00:00:00')), &
											   ls_anno_commessa, &
											   ll_anno_reg_des_mov, &
											   ll_num_reg_des_mov, &
											   ls_cod_deposito[], &
											   ls_cod_ubicazione[], &
											   ls_cod_lotto[], &
											   ld_data_stock[], &
											   ll_prog_stock[], &
											   ls_cod_fornitore[], &
											   ls_cod_cliente[], &
											   ll_anno_registrazione[], &
											   ll_num_registrazione[])
												
		destroy luo_mag

		if li_risposta=-1 then
			fs_errore="Errore su movimenti magazzino."
			return -1
		end if

		li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)

		if li_risposta = -1 then
			fs_errore="Si è verificato un errore in fase di cancellazione destinazioni movimenti."
			return -1
		end if
		
		update avan_produzione_com
		set    anno_reg_sfrido=:ll_anno_registrazione[1],
				 num_reg_sfrido=:ll_num_registrazione[1]
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
		       anno_commessa = :fl_anno_commessa		and    
				 num_commessa = :fl_num_commessa			and    
				 prog_riga = :fl_prog_riga					and    
				 cod_prodotto = :ls_cod_prodotto			and 
				 cod_versione = :ls_cod_versione_avan_prod       and
				 cod_reparto = :ls_cod_reparto			and    
				 cod_lavorazione = :ls_cod_lavorazione;
		
      if sqlca.sqlcode<>0 then 
		  fs_errore = "Si è verificato un errore sul DB:"+ sqlca.sqlerrtext
		  return -1
		end if
	end if
	// (fine) SFRIDI*************************************************************************

loop

close righe_avan_prod_com;

// (fine) MUOVE SEMILAVORATI (RESI/SFRIDI)**************************************************



// (Inizio) MUOVE MATERIE PRIME (RESI/SFRIDI)**************************************************

declare righe_mat_prime cursor for
select  cod_prodotto,
		  quan_reso,
		  quan_sfrido,
		  cod_tipo_mov_reso,
		  cod_tipo_mov_sfrido
from    mat_prime_commessa
where   cod_azienda=:s_cs_xx.cod_azienda
and     anno_commessa=:fl_anno_commessa
and     num_commessa=:fl_num_commessa
and     prog_riga=:fl_prog_riga;

open righe_mat_prime;

do while 1=1
	fetch righe_mat_prime
	into  :ls_cod_prodotto,
			:ldd_quan_reso,
			:ldd_quan_sfrido,
			:ls_cod_tipo_mov_reso,
		   :ls_cod_tipo_mov_sfrido;

	if sqlca.sqlcode<> 0 then exit 

	if ldd_quan_reso=0 and ldd_quan_sfrido=0 then continue
	
	setnull(ls_cod_deposito[1])
   setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ld_data_stock[1])
	setnull(ll_prog_stock[1]) 
	setnull(ls_cod_cliente[1]) 
	setnull(ls_cod_fornitore[1])
	setnull(ll_anno_reg_des_mov) 
	setnull(ll_num_reg_des_mov)

	// (Inizio) RESI*************************************************************************
	if ldd_quan_reso <> 0 then
		if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_reso, ls_cod_prodotto, ls_cod_deposito[], & 
											  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
											  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
											  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then

		  fs_errore="Si è verificato un errore in creazione movimenti"
 		  close righe_avan_prod_com;
		  return -1
		end if

		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
											 ls_cod_tipo_mov_reso, ls_cod_prodotto) = -1 then

		  fs_errore="Si è verificato un errore in fase di verifica destinazioni movimenti magazzino"
 		  close righe_avan_prod_com;
		  return -1

		end if	
		
		luo_mag = create uo_magazzino

		li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
	 							 			 	ls_cod_tipo_mov_reso, &
				  							 	"N", &
												ls_cod_prodotto, &
											   ldd_quan_reso, &
											   1, &
											   fl_num_commessa, &
											   datetime(today(),time('00:00:00')), &
											   ls_anno_commessa, &
											   ll_anno_reg_des_mov, &
											   ll_num_reg_des_mov, &
											   ls_cod_deposito[], &
											   ls_cod_ubicazione[], &
											   ls_cod_lotto[], &
											   ld_data_stock[], &
											   ll_prog_stock[], &
											   ls_cod_fornitore[], &
											   ls_cod_cliente[], &
											   ll_anno_registrazione[], &
											   ll_num_registrazione[])
												
		destroy luo_mag

		if li_risposta=-1 then
			fs_errore="Errore su movimenti magazzino."
			return -1
		end if

		li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)

		if li_risposta = -1 then
			fs_errore="Si è verificato un errore in fase di cancellazione destinazioni movimenti."
			return -1
		end if

		update mat_prime_commessa
		set    anno_reg_reso=:ll_anno_registrazione[1],
				 num_reg_reso=:ll_num_registrazione[1]
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:fl_anno_commessa
		and    num_commessa=:fl_num_commessa
		and    prog_riga=:fl_prog_riga
		and    cod_prodotto=:ls_cod_prodotto;
		
      if sqlca.sqlcode<>0 then 
		  fs_errore = "Si è verificato un errore sul DB:"+ sqlca.sqlerrtext
		  return -1
		end if

	end if
	// (fine) RESI***************************************************************************	



	// (Inizio) SFRIDI***********************************************************************
	if ldd_quan_sfrido <> 0 then
		if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_sfrido, ls_cod_prodotto, ls_cod_deposito[], & 
											  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
											  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
											  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then

		  fs_errore="Si è verificato un errore in creazione movimenti"
 		  close righe_avan_prod_com;
		  return -1
		end if

		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
											 ls_cod_tipo_mov_sfrido, ls_cod_prodotto) = -1 then
			
		  fs_errore="Si è verificato un errore in fase di verifica destinazioni movimenti magazzino"
 		  close righe_avan_prod_com;
		  return -1
		end if
		
		luo_mag = create uo_magazzino

		li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
	 							 			 	ls_cod_tipo_mov_sfrido, &
				  							 	"N", &
												ls_cod_prodotto, &
											   ldd_quan_reso, &
											   1, &
											   fl_num_commessa, &
											   datetime(today(),time('00:00:00')), &
											   ls_anno_commessa, &
											   ll_anno_reg_des_mov, &
											   ll_num_reg_des_mov, &
											   ls_cod_deposito[], &
											   ls_cod_ubicazione[], &
											   ls_cod_lotto[], &
											   ld_data_stock[], &
											   ll_prog_stock[], &
											   ls_cod_fornitore[], &
											   ls_cod_cliente[], &
											   ll_anno_registrazione[], &
											   ll_num_registrazione[])
												
		destroy luo_mag

		if li_risposta=-1 then
			fs_errore="Errore su movimenti magazzino."
			return -1
		end if

		li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)

		if li_risposta = -1 then
			fs_errore="Si è verificato un errore in fase di cancellazione destinazioni movimenti."
			return -1
		end if
		
		update mat_prime_commessa
		set    anno_reg_sfrido=:ll_anno_registrazione[1],
				 num_reg_sfrido=:ll_num_registrazione[1]
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:fl_anno_commessa
		and    num_commessa=:fl_num_commessa
		and    prog_riga=:fl_prog_riga
		and    cod_prodotto=:ls_cod_prodotto;
		
      if sqlca.sqlcode<>0 then 
		  fs_errore = "Si è verificato un errore sul DB:"+ sqlca.sqlerrtext
		  return -1
		end if

	end if
	// (fine) SFRIDI*************************************************************************

loop

close righe_avan_prod_com;
// (Fine) MUOVE MATERIE PRIME (RESI/SFRIDI)*************************************************

return 0
end function

public function integer wf_carico_parziale_pf (double fd_quan_pf, integer fl_anno_commessa, long fl_num_commessa, string fs_cod_prodotto, string fs_cod_versione, long fl_prog_riga, string fs_cod_reparto, string fs_cod_lavorazione, string fs_flag_fine_fase, string fs_cod_operaio, integer fi_interfaccia, ref double fdd_quan_prodotta, ref double fdd_quan_in_produzione, ref integer fi_flag_ultima, integer fi_step, ref string fs_errore);string ls_cod_prodotto, ls_cod_versione, ls_flag_tipo_avanzamento, ls_parametro
double ldd_quan_prodotta, ldd_quan_in_produzione
integer li_risposta
dec{4} ldd_quan_prodotta_save, ldd_quan_in_produzione_anag

if fd_quan_pf = 0 then return 1
//select quan_in_produzione
//into   :ldd_quan_in_produzione_prog
//from   det_anag_commesse
//where  cod_azienda=:s_cs_xx.cod_azienda
//and 	 anno_commessa=:fi_anno_commessa
//and    num_commessa=:fl_num_commessa
//and    prog_riga=:fl_prog_riga;

//if sqlca.sqlcode < 0 then
//	fs_errore = "Attenzione! Problema con il Database. :" + sqlca.sqlerrtext
//	return -1
//end if

//#############################
//#############################
long ll_num_fasi, ll_prog_orari, ll_num_fasi_aperte
string ls_test

select count(*)
into   :ll_num_fasi
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :fl_anno_commessa  and    
		 num_commessa = :fl_num_commessa		and    
		 prog_riga = :fl_prog_riga				and    
		 cod_prodotto = :fs_cod_prodotto		and    
		 cod_versione = :fs_cod_versione    and
		 cod_reparto = :fs_cod_reparto      and    
		 cod_lavorazione = :fs_cod_lavorazione;

if sqlca.sqlcode < 0 then
	fs_errore = "Attenzione! Problema con il Database. :" + sqlca.sqlerrtext
	return -1
end if

if ll_num_fasi = 0 then
	fs_errore = "Attenzione! Non esistono sessioni iniziate per questa fase."
	return -1
end if

select max(prog_orari)
into   :ll_prog_orari
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda  and    
       anno_commessa = :fl_anno_commessa   and    
		 num_commessa = :fl_num_commessa     and    
		 prog_riga = :fl_prog_riga				 and    
		 cod_prodotto = :fs_cod_prodotto		 and    
		 cod_versione = :fs_cod_versione     and
		 cod_reparto = :fs_cod_reparto		 and    
		 cod_lavorazione = :fs_cod_lavorazione;

if sqlca.sqlcode < 0 then
	fs_errore = "Attenzione! Problema con il Database. :" + sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode=0 then
	
	SELECT cod_azienda
	INTO   :ls_test  
	FROM   det_orari_produzione
	WHERE  cod_azienda = :s_cs_xx.cod_azienda  	AND    
			 anno_commessa = :fl_anno_commessa  	AND    
			 num_commessa = :fl_num_commessa  		AND    
			 prog_riga = :fl_prog_riga 				AND    
			 cod_prodotto = :fs_cod_prodotto  		AND    
			 cod_versione = :fs_cod_versione       AND
			 cod_reparto = :fs_cod_reparto   		AND    
			 cod_lavorazione = :fs_cod_lavorazione AND    
			 prog_orari = :ll_prog_orari  			AND    
			 flag_inizio = 'N';

	if sqlca.sqlcode = 100 then
		fs_errore = "Attenzione! La sessione di lavoro corrente e' ancora aperta, chiuderla prima di terminare la fase."
		return -1
	end if
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Attenzione! Problema con il Database. :" + sqlca.sqlerrtext
		return -1
	end if
	
else
	fs_errore = "Attenzione! Non esistono sessioni per questa fase."
	return -1
end if

//update avan_produzione_com
//set    flag_fine_fase = 'S'
//where  cod_azienda = :s_cs_xx.cod_azienda  and 	 
//		 anno_commessa = :fl_anno_commessa	 and    
//		 num_commessa = :fl_num_commessa		 and    
//		 prog_riga = :fl_prog_riga				 and    
//		 cod_prodotto = :fs_cod_prodotto		 and    
//		 cod_versione = :fs_cod_versione     and
//		 cod_reparto = :fs_cod_reparto		 and    
//		 cod_lavorazione = :fs_cod_lavorazione;
//
//if sqlca.sqlcode < 0 then
//		fs_errore = "Attenzione! Problema con il Database. :" + sqlca.sqlerrtext
//		return -1
//end if

////**** Individua se viene chiusa l'ultima fase
//
//select count(*)
//into   :ll_num_fasi_aperte
//from   avan_produzione_com
//where  cod_azienda = :s_cs_xx.cod_azienda  and 	 
//		 anno_commessa = :fl_anno_commessa	 and    
//		 num_commessa = :fl_num_commessa		 and    
//		 prog_riga = :fl_prog_riga				 and    
//		 flag_fine_fase = 'N';
//
//if sqlca.sqlcode < 0 then
//		fs_errore = "Attenzione! Problema con il Database. :" + sqlca.sqlerrtext
//		return -1
//end if
//
//if fi_step = 1 and fi_interfaccia=2 then
//	if ll_num_fasi_aperte=0 then
//		fi_flag_ultima=1
//	else
//		fi_flag_ultima=0
//	end if
//	return 0
//end if

//#############################
//#############################

select cod_prodotto,
		 cod_versione,
		 flag_tipo_avanzamento
into   :ls_cod_prodotto,
		 :ls_cod_versione,
		 :ls_flag_tipo_avanzamento
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and 	 
		 anno_commessa = :fl_anno_commessa  and    
		 num_commessa = :fl_num_commessa;

if sqlca.sqlcode < 0 then
	fs_errore = "Attenzione! Problema con il Database. :" + sqlca.sqlerrtext
	return -1
end if

s_cs_xx.parametri.parametro_i_1 = fl_anno_commessa
s_cs_xx.parametri.parametro_ul_1 = fl_num_commessa
s_cs_xx.parametri.parametro_ul_2 = fl_prog_riga
s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
s_cs_xx.parametri.parametro_s_2 = ls_cod_versione
s_cs_xx.parametri.parametro_s_5 = ls_cod_versione   // parametro letto da w_rileva_quantita
s_cs_xx.parametri.parametro_s_6 = fs_cod_reparto
s_cs_xx.parametri.parametro_s_7 = fs_cod_lavorazione

if fi_interfaccia=1 then	//imposta distinzione tra terminale a caratteri e grafico
	s_cs_xx.parametri.parametro_b_1 = false
	choose case ls_flag_tipo_avanzamento //seleziona il tipo avanzamento
		case '1'
//			s_cs_xx.parametri.parametro_dec4_5 = fd_quan_pf
//			window_open(w_quantita_parziale,0) 
//			s_cs_xx.parametri.parametro_dec4_5 = 0
//			
//			if s_cs_xx.parametri.parametro_b_1 = true then	
//				fs_errore = "Operazione annullata"
//				return -1
//			end if
//
//			return 1
			
		case '2'
			s_cs_xx.parametri.parametro_dec4_5 = fd_quan_pf
			window_open(w_quantita_fine_fase_parziale,0)
			s_cs_xx.parametri.parametro_dec4_5 = 0
			
			if s_cs_xx.parametri.parametro_b_1 = true then	
			
// --- Michele 31/05/2007 - Non serve in quanto si deve solo fare un rollback alla fine della transazione se qualcosa da esito negativo ---
// il problema derivava probabilmente dal fatto che la maschera w_quantita_fine_fase conteneva erroneamente dei COMMIT i quali
// andavano a confermare l'update del flag a "S" fatto qualche riga più sopra. Ora che la maschera w_quantita_fine_fase è stata
// corretta, questo update a "N" non serve più. In ogni caso si sarebbe dovuto fare su una tansazione separata per non interferire con
// la transazionalità del processo corrrente
//
//
//					update avan_produzione_com
//					set    flag_fine_fase = 'N'
//					where  cod_azienda = :s_cs_xx.cod_azienda  and 	 
//							 anno_commessa = :fi_anno_commessa	 and    
//							 num_commessa = :fl_num_commessa		 and    
//							 prog_riga = :fl_prog_riga				 and    
//							 cod_prodotto = :fs_cod_prodotto		 and    
//							 cod_versione = :fs_cod_versione     and
//							 cod_reparto = :fs_cod_reparto		 and    
//							 cod_lavorazione = :fs_cod_lavorazione;
//							 
//					commit;
//					
//					if sqlca.sqlcode < 0 then
//							fs_errore = "Attenzione! Problema con il Database. :" + sqlca.sqlerrtext
//							return -1
//					end if
				
				fs_errore = "Operazione annullata"
				return -1
			end if
			
	end choose 
else

end if

//aggiorna la qta pf della sotto-commessa
ldd_quan_prodotta_save = s_cs_xx.parametri.parametro_d_1

update det_anag_commesse
set    quan_in_produzione = quan_in_produzione - :ldd_quan_prodotta_save,
		 quan_prodotta = quan_prodotta + :ldd_quan_prodotta_save
where  cod_azienda=:s_cs_xx.cod_azienda
and 	 anno_commessa=:fl_anno_commessa
and    num_commessa=:fl_num_commessa
and    prog_riga=:fl_prog_riga;

if sqlca.sqlcode<>0 then
	fs_errore = "Attenzione! Problema con il DB:" + sqlca.sqlerrtext
	return -1
end if

select quan_prodotta,
		 quan_in_produzione
into   :ldd_quan_prodotta,
		 :ldd_quan_in_produzione
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and 	 anno_commessa=:fl_anno_commessa
and    num_commessa=:fl_num_commessa;

if sqlca.sqlcode < 0 then
	fs_errore = "Attenzione! Problema con il Database. :" + sqlca.sqlerrtext
	return -1
end if

ldd_quan_in_produzione = ldd_quan_in_produzione - ldd_quan_prodotta_save
if ldd_quan_in_produzione < 0 then ldd_quan_in_produzione = 0
ldd_quan_prodotta = ldd_quan_prodotta + ldd_quan_prodotta_save

//tutto questo doveva essere fatto solo se la sotto-commessa veniva chiusa
//questo in tale funzione non succede perchè si tratta di carico PF parziale
/*
choose case ls_flag_tipo_avanzamento //seleziona il tipo avanzamento
	case '1' // Assegnazione
		ls_flag_tipo_avanzamento = '9' // Assegnazione Chiusa
	case '2' // Attivazione
		
		//Donato 29/09/2008 -----------------------------------------------------------------------------
		//Anomalia segnalata da Marco: se chiudo una sotto-commessa lo stato non deve passare
		//ad Attivazione Chiuso se ci sono sottocommesse ancora aperte
		decimal ld_sum_quan_in_produzione
		
		select sum(quan_in_produzione)
		into :ld_sum_quan_in_produzione
		from det_anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
				and 	 anno_commessa=:fi_anno_commessa
				and    num_commessa=:fl_num_commessa
				and    prog_riga<>:fl_prog_riga
		;
		if sqlca.sqlcode < 0 then
			fs_errore = "Attenzione! Errore durante il controllo sulle sotto-commesse aperte. :" + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ld_sum_quan_in_produzione) then ld_sum_quan_in_produzione = 0
		if ld_sum_quan_in_produzione > 0 then
			//esistono ancora sotto-commesse aperte: non chiudere la commessa
		else
			//non esistono sotto-commesse ancora aperte: chiudere la commessa
			ls_flag_tipo_avanzamento = '8' // Attivazione Chiusa
		end if
		
		//VECCHIO CODICE ############################
		//ls_flag_tipo_avanzamento = '8' // Attivazione Chiusa
		//FINE VECCHIO CODICE########################
		//fine modifica -------------------------------------------------------------------------------------
end choose 
*/

update anag_commesse
set    quan_in_produzione = :ldd_quan_in_produzione, //quan_in_produzione - :ldd_quan_in_produzione,	
		 quan_prodotta = :ldd_quan_prodotta //quan_prodotta + :ldd_quan_prodotta//,
		 //flag_tipo_avanzamento = :ls_flag_tipo_avanzamento
where  cod_azienda=:s_cs_xx.cod_azienda
and 	 anno_commessa=:fl_anno_commessa
and    num_commessa=:fl_num_commessa;

if sqlca.sqlcode<>0 then
	fs_errore="Attenzione! Problema con il DB:" + sqlca.sqlerrtext
	return -1
end if

fdd_quan_in_produzione = ldd_quan_in_produzione
fdd_quan_prodotta = ldd_quan_prodotta
fi_flag_ultima = 1


li_risposta = wf_mov_mag_chiusura_fasi_parziale(fl_anno_commessa,fl_num_commessa, & 
											fl_prog_riga, fd_quan_pf, fs_errore )

if li_risposta = -1 then return -1


//Aggiornamento della quan_in produzione della anag_prodotti per il PF
select quan_in_produzione
into :ldd_quan_in_produzione_anag
from anag_prodotti
where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto;

if sqlca.sqlcode = 0 then
	if isnull(ldd_quan_in_produzione_anag) then ldd_quan_in_produzione_anag = 0
	
	if ldd_quan_in_produzione_anag < ldd_quan_prodotta_save then
		update anag_prodotti
		set quan_in_produzione = 0
		where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto;
	else
		update anag_prodotti
		set quan_in_produzione = quan_in_produzione - :ldd_quan_prodotta_save
		where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto;			
	end if
end if
//--------------------------------------------------------------------------------------

// cancellazione tabella distinte_taglio_calcolate

select stringa
into   :ls_parametro
from   parametri
where  cod_parametro='DT';

if sqlca.sqlcode < 0 then
	fs_errore = "Si è verificato un errore durante la lettura tabella parametri. Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

//delego la cancellazione tabella distinte_taglio_calcolate alla chiusura della sotto-commessa
/*
if ls_parametro='S' then
	
	select anno_registrazione,
			 num_registrazione,
			 prog_riga_ord_ven
	into   :li_anno_registrazione,
			 :ll_num_reg,
			 :ll_prog_riga_ord_ven
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:fi_anno_commessa
	and    num_commessa=:fl_num_commessa;

	if sqlca.sqlcode < 0 then
		fs_errore = "Si è verificato un errore durante la lettura tabella det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	delete distinte_taglio_calcolate
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_reg
	and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Si è verificato un errore durante la cancellazione tabella distinte_taglio_calcolate. Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

end if
// FINE cancellazione tabella distinte_taglio_calcolate
*/

return 1
end function

public function integer wf_controlla_fase_prodotto_finito (integer ai_anno_commessa, long al_num_commessa, long al_prog_riga, ref s_chiave_distinta astr_chiave_distinta, ref integer ai_pictureindex);string					ls_stato_fase,ls_cod_reparto, ls_cod_lavorazione, ls_flag_fase_conclusa, ls_flag_fase_esterna
long					ll_cont


setnull(ls_stato_fase)
ai_pictureindex = 1
			
select cod_reparto, cod_lavorazione  
into  :ls_cod_reparto, :ls_cod_lavorazione  
from  tes_fasi_lavorazione  
where	cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto = :is_cod_prodotto_finito and  
			cod_versione = :is_cod_versione_pf;

if sqlca.sqlcode = 0 then		// esiste una fase

	astr_chiave_distinta.cod_prodotto_figlio = is_cod_prodotto_finito
	astr_chiave_distinta.cod_versione_figlio = is_cod_versione_pf
	astr_chiave_distinta.cod_lavorazione = ls_cod_lavorazione
	astr_chiave_distinta.cod_reparto = ls_cod_reparto
	

	astr_chiave_distinta.flag_fase_aperta = "A"
	ls_stato_fase = "A"		// fase aperta (non Iniziata e non Finita)
	
	 //controllo se terminata
	select		flag_fine_fase, flag_esterna  
	into  :ls_flag_fase_conclusa, :ls_flag_fase_esterna  
	from  avan_produzione_com  
	where	cod_azienda = :s_cs_xx.cod_azienda and
				 anno_commessa = :ai_anno_commessa and
				 num_commessa = : al_num_commessa and
				 cod_prodotto = :is_cod_prodotto_finito and
				 cod_versione = :is_cod_versione_pf and
				 prog_riga = :al_prog_riga  and
				 cod_reparto = :ls_cod_reparto  and
				 cod_lavorazione = :ls_cod_lavorazione;

	if sqlca.sqlcode = 0 then
		
		// se non terminata controllo se iniziata		
		if ls_flag_fase_esterna = "S" then	
			ls_stato_fase = "E"		// fase aperta ESTERNA (non Iniziata e non Finita)
			astr_chiave_distinta.flag_fase_aperta = "A"
			astr_chiave_distinta.flag_fase_esterna = "S"
		end if

		if ls_flag_fase_conclusa = "N"  then
			
			select count(*)
			into   :ll_cont
			from   det_orari_produzione
			where	cod_azienda = :s_cs_xx.cod_azienda and
						anno_commessa = :ai_anno_commessa and 
						num_commessa = : al_num_commessa and
						cod_prodotto = :is_cod_prodotto_finito and
						cod_versione = :is_cod_versione_pf and
						prog_riga = :al_prog_riga and
						cod_reparto = :ls_cod_reparto  and
						cod_lavorazione = :ls_cod_lavorazione and
						flag_inizio = 'S' ;
			
			if ll_cont > 0 then
				ls_stato_fase = "I"		// Fase Iniziata
				astr_chiave_distinta.flag_fase_aperta = "I"
			end if
		else
			ls_stato_fase = "F"  // fase conclusa
			astr_chiave_distinta.flag_fase_aperta = "F"
		end if

	 //non esiste la fase di avanzamento nell'avanzamento di produzione
	else
		setnull(ls_stato_fase)
		astr_chiave_distinta.flag_fase_aperta = "N"
	end if
	
	choose case ls_stato_fase
		case "A"
			ai_pictureindex = 6
		case "I"
			ai_pictureindex = 7
		case "F"
			ai_pictureindex = 8
		case "E"  // fase esterna aperta
			ai_pictureindex = 9
		case else
			ai_pictureindex = 1
	end choose
	
	
	return 0

else
	return 1
end if

return 1
end function

on w_avanz_prod_com_attivazione.create
int iCurrent
call super::create
this.cb_blocco=create cb_blocco
this.cbx_flag_ramo_descrittivo=create cbx_flag_ramo_descrittivo
this.cbx_vis_mp=create cbx_vis_mp
this.dw_selezione=create dw_selezione
this.dw_avanzamento_produzione_commesse=create dw_avanzamento_produzione_commesse
this.tv_db=create tv_db
this.cb_fine_fase=create cb_fine_fase
this.cb_inizio=create cb_inizio
this.cb_fine=create cb_fine
this.cb_esterna_interna=create cb_esterna_interna
this.cb_genera_bolle_out=create cb_genera_bolle_out
this.cb_carica_bolle_in=create cb_carica_bolle_in
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_blocco
this.Control[iCurrent+2]=this.cbx_flag_ramo_descrittivo
this.Control[iCurrent+3]=this.cbx_vis_mp
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_avanzamento_produzione_commesse
this.Control[iCurrent+6]=this.tv_db
this.Control[iCurrent+7]=this.cb_fine_fase
this.Control[iCurrent+8]=this.cb_inizio
this.Control[iCurrent+9]=this.cb_fine
this.Control[iCurrent+10]=this.cb_esterna_interna
this.Control[iCurrent+11]=this.cb_genera_bolle_out
this.Control[iCurrent+12]=this.cb_carica_bolle_in
end on

on w_avanz_prod_com_attivazione.destroy
call super::destroy
destroy(this.cb_blocco)
destroy(this.cbx_flag_ramo_descrittivo)
destroy(this.cbx_vis_mp)
destroy(this.dw_selezione)
destroy(this.dw_avanzamento_produzione_commesse)
destroy(this.tv_db)
destroy(this.cb_fine_fase)
destroy(this.cb_inizio)
destroy(this.cb_fine)
destroy(this.cb_esterna_interna)
destroy(this.cb_genera_bolle_out)
destroy(this.cb_carica_bolle_in)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(C_NOENABLEPOPUP)
//dw_avanzamento_produzione_commesse.set_dw_options(sqlca, &
//                                    						  i_openparm, &
//				 		                                      c_scrollparent + c_nonew +  c_nodelete, &
//      		                                            c_default)

//dw_avanzamento_produzione_commesse.set_dw_options(sqlca,i_openparm,C_NORETRIEVEONOPEN + c_sharedata + c_scrollparent + c_nonew + c_nodelete,c_default)


dw_avanzamento_produzione_commesse.set_dw_options(sqlca, &
																  i_openparm, &
																  c_noretrieveonopen + c_scrollparent + c_nonew +  c_nodelete, &
																  c_default)

iuo_dw_main = dw_avanzamento_produzione_commesse

dw_selezione.insertrow(0)
dw_selezione.Object.data_inizio.Protect=1
dw_selezione.Object.ora_inizio.Protect=1

wf_inizio()
end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDLB(ddlb_operai, sqlca, "anag_operai", &	
//				  "cod_operaio", "cognome", &
//				  "cod_azienda='" + s_cs_xx.cod_azienda + "'","")

f_PO_LoadDDDW_DW(dw_selezione,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ', ' + nome",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

event open;call super::open;iuo_magazzino = CREATE uo_magazzino
end event

event pc_close;call super::pc_close;destroy iuo_magazzino
end event

type cb_blocco from commandbutton within w_avanz_prod_com_attivazione
integer x = 4142
integer y = 432
integer width = 366
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Blocco"
end type

event clicked;string ls_cod_prodotto,ls_cod_versione,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio, ls_errore,ls_flag_fine_fase

integer ll_anno_commessa,li_flag_ultima

long ll_ret,ll_num_commessa,ll_prog_riga

double ldd_quan_prodotta, ldd_quan_in_produzione, ld_quan_impegnata

dec{4} ld_quan_impegno, ld_quan_in_produzione, ld_quan_prodotta

uo_magazzino luo_magazzino

ll_ret = g_mb.messagebox("SEP","Sei sicuro di voler bloccare questa fase. Poi non sarà più possibile sbloccarla.",Question!,Yesno!,2)
if ll_ret = 2 then return

ll_anno_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "num_commessa")
ll_prog_riga    = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "prog_riga")
ls_cod_prodotto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_prodotto")
ls_cod_versione = dw_avanzamento_produzione_commesse.getitemstring( dw_avanzamento_produzione_commesse.getrow(), "cod_versione")
ls_cod_reparto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_reparto")
ls_cod_lavorazione = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_lavorazione")
ls_cod_operaio = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_operaio")
ls_flag_fine_fase = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_fine_fase")

if ls_flag_fine_fase = "S" and not isnull(ls_flag_fine_fase) then
	g_mb.messagebox("SEP","Impossibile Bloccare una Fase già Conclusa")
	return
end if

// DISIMPEGNO LE MATERIE PRIME
luo_magazzino = create uo_magazzino

select quan_in_produzione, quan_prodotta
into   :ld_quan_in_produzione, :ld_quan_prodotta
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_commessa = :ll_anno_commessa and
		 num_commessa = :ll_num_commessa;
if sqlca.sqlcode = 0 then
	if isnull(ld_quan_in_produzione) then ld_quan_in_produzione = 0
	if isnull(ld_quan_prodotta) 		then ld_quan_prodotta = 0
	ld_quan_impegno = ld_quan_in_produzione - ld_quan_prodotta
	
	if luo_magazzino.uof_impegna_mp_commessa(	true, &
															false, &
															false, &
															is_cod_prodotto_finito, &
															is_cod_versione_pf, &
															ld_quan_impegno, &
															ll_anno_commessa, &
															ll_num_commessa, &
															ref ls_errore) < 0  then
		g_mb.messagebox("SEP","Errore in disimpegno MP~r~n" + ls_errore,stopsign!)
		rollback;
		return
	end if
	
else
	g_mb.messagebox("SEP","Commessa " + string(ll_anno_commessa) + "/" + string(ll_num_commessa) + " non trovata.~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if


ll_ret = f_blocca_fase(ll_anno_commessa,&
							  ll_num_commessa,&
							  ll_prog_riga,&
							  ls_cod_prodotto,&
							  ls_cod_versione,&
							  ls_cod_reparto,&
							  ls_cod_lavorazione,&
							  ls_cod_operaio,&
							  ref ldd_quan_prodotta, &
							  ref ldd_quan_in_produzione,&
							  ref li_flag_ultima,&
							  ref ls_errore )
							  
if ll_ret = -1 then
	rollback;
	g_mb.messagebox("SEP", ls_errore)
else
	// PROCEDO CON RICACOLO IMPEGNATO DELL'INTERA COMMESSA
	if luo_magazzino.uof_impegna_mp_commessa(true, &
															true, &
															false, &
															is_cod_prodotto_finito, &
															is_cod_versione_pf, &
															ld_quan_impegno, &
															ll_anno_commessa, &
															ll_num_commessa, &
															ref ls_errore) < 0 then
		g_mb.messagebox("SEP","Errore in impegno MP~r~n" + ls_errore,stopsign!)
		rollback;
		return
	end if
	
	// PROCEDO CON DISIMPEGNO DEL PRODOTTO FINITO DA PRODUZIONE E MAGAZZINO
	select quan_impegnata
	into   :ld_quan_impegnata
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SEP","Errore in ricerca prodotto "+ls_cod_prodotto+" in anagrafica~r~n" + sqlca.sqlerrtext)
		return
	end if
	
	if ld_quan_impegnata > ld_quan_impegno then
		update anag_prodotti
		set    quan_impegnata = quan_impegnata - :ld_quan_impegno
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("SEP","Errore in aggiormamento impegnato prodotto "+ls_cod_prodotto+" in anagrafica~r~n" + sqlca.sqlerrtext)
			return
		end if
	else
		update anag_prodotti
		set    quan_impegnata = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("SEP","Errore in aggiormamento impegnato prodotto "+ls_cod_prodotto+" in anagrafica~r~n" + sqlca.sqlerrtext)
			return
		end if
	end if
	
	select quan_impegnata_attuale
	into   :ld_quan_impegnata
	from   impegno_mat_prime_commessa
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_commessa and
			 num_commessa = :ll_num_commessa and
			 cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SEP","Errore in ricerca prodotto "+ls_cod_prodotto+" in anagrafica~r~n" + sqlca.sqlerrtext)
		return
	end if
	
	if ld_quan_impegnata > ld_quan_impegno then
		update impegno_mat_prime_commessa
		set    quan_impegnata_attuale = quan_impegnata_attuale - :ld_quan_impegno
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_commessa = :ll_anno_commessa and
				 num_commessa = :ll_num_commessa and
				 cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("SEP","Errore in aggiormamento impegnato prodotto "+ls_cod_prodotto+" in anagrafica~r~n" + sqlca.sqlerrtext)
			return
		end if
	else
		update impegno_mat_prime_commessa
		set    quan_impegnata_attuale = 0
		where  cod_azienda = :s_cs_xx.cod_azienda  and
				 anno_commessa = :ll_anno_commessa and
				 num_commessa = :ll_num_commessa and
				 cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("SEP","Errore in aggiormamento impegnato prodotto "+ls_cod_prodotto+" in anagrafica~r~n" + sqlca.sqlerrtext)
			return
		end if
	end if
	
	commit;
	
	g_mb.messagebox("SEP","FASE BLOCCATA CON SUCCESSO!" + sqlca.sqlerrtext)
	
end if

tv_db.postevent("clicked")

end event

type cbx_flag_ramo_descrittivo from checkbox within w_avanz_prod_com_attivazione
integer x = 3625
integer y = 2488
integer width = 763
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Visualizza Rami Descrittivi"
boolean lefttext = true
end type

event clicked;wf_inizio()
end event

type cbx_vis_mp from checkbox within w_avanz_prod_com_attivazione
integer x = 2793
integer y = 2484
integer width = 736
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Visualizza Materia Prima"
boolean lefttext = true
end type

event clicked;wf_inizio()
end event

type dw_selezione from datawindow within w_avanz_prod_com_attivazione
integer x = 2784
integer y = 1480
integer width = 1746
integer height = 996
integer taborder = 90
string title = "none"
string dataobject = "d_avanzamento_produzione_commesse_selez"
boolean livescroll = true
end type

event itemchanged;datetime ldt_oggi

if isvalid(dwo) then
	choose case dwo.name
		case "flag_ora_automatica"
			if data = "S" then
				
				this.Object.data_inizio.Protect=1
				this.object.data_inizio.background.mode = '1'
				this.object.data_inizio.background.color = '553648127'
				setnull(ldt_oggi)
				setitem(row,"data_inizio", ldt_oggi)
				
				this.Object.ora_inizio.Protect=1
				this.object.ora_inizio.background.mode = '1'
				this.object.ora_inizio.background.color = '553648127'
				setnull(ldt_oggi)
				setitem(row,"ora_inizio", ldt_oggi)
				
			else
				
				this.Object.data_inizio.Protect=0
				ldt_oggi = datetime( today(), 00:00:00)
				setitem(row,"data_inizio", ldt_oggi)
				this.object.data_inizio.background.mode = '0'
				this.object.data_inizio.background.color = '16777215'
				
				this.Object.ora_inizio.Protect=0
				ldt_oggi = datetime( date( string("01/01/1900") ), now() )
				setitem(row,"ora_inizio", ldt_oggi)
				this.object.ora_inizio.background.mode = '0'
				this.object.ora_inizio.background.color = '16777215'
				
			end if
	end choose
end if

end event

type dw_avanzamento_produzione_commesse from uo_cs_xx_dw within w_avanz_prod_com_attivazione
integer x = 2784
integer y = 8
integer width = 1746
integer height = 1464
integer taborder = 140
string dataobject = "d_avanzamento_produzione_commesse"
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_anno_commessa, ll_num_commessa, ll_prog_riga,l_Error

treeviewitem  tvi_campo
s_chiave_distinta l_chiave_distinta

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.getrow(),"anno_commessa")
ll_num_commessa  = i_parentdw.getitemnumber(i_parentdw.getrow(),"num_commessa")
ll_prog_riga     = i_parentdw.getitemnumber(i_parentdw.getrow(),"prog_riga")

tv_db.GetItem ( il_handle, tvi_campo )

l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 ll_anno_commessa, &
						 ll_num_commessa, &
						 l_chiave_distinta.cod_prodotto_figlio, &
						 l_chiave_distinta.cod_versione_figlio, &
						 ll_prog_riga, &
						 l_chiave_distinta.cod_reparto, &
						 l_chiave_distinta.cod_lavorazione	 )
						 
if l_Error  < 0 then
	   PCCA.Error = c_Fatal
end if		

if l_Error = 0 then
		cb_esterna_interna.enabled = false
		cb_genera_bolle_out.enabled = false
		cb_carica_bolle_in.enabled = false
		cb_inizio.enabled = false
		cb_fine.enabled = false
		cb_fine_fase.enabled = false
		dw_selezione.enabled=false
else
	if getitemstring(getrow(),"flag_fine_fase") = "S" then
		cb_esterna_interna.enabled = false
		cb_genera_bolle_out.enabled = false
		cb_carica_bolle_in.enabled = false
		cb_inizio.enabled = false
		cb_fine.enabled = false
		cb_fine_fase.enabled = false
		dw_selezione.enabled=false
	else	
		cb_esterna_interna.enabled = true
		cb_genera_bolle_out.enabled = true
		cb_carica_bolle_in.enabled = true
		cb_inizio.enabled = true
		cb_fine.enabled = true
		cb_fine_fase.enabled = true
		dw_selezione.enabled=true
	end if	
end if
end event

event buttonclicked;call super::buttonclicked;if dwo.name = "b_ricalcola_quan_prodotta" then
	
	string		ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione
	long		ll_anno_commessa, ll_num_commessa, ll_prog_riga
	dec{4} ld_quan_prodotta
	
	ll_anno_commessa = getitemnumber(getrow(),"anno_commessa")
	ll_num_commessa = getitemnumber(getrow(),"num_commessa")
	ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
	ll_prog_riga =  getitemnumber(getrow(),"prog_riga")
	ls_cod_reparto = getitemstring(getrow(),"cod_reparto")
	ls_cod_lavorazione = getitemstring(getrow(),"cod_lavorazione")
	
	select sum( isnull(quan_prodotta,0) ) 
	into	:ld_quan_prodotta
	from det_orari_produzione
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa and
			num_commessa = :ll_num_commessa and
			prog_riga = :ll_prog_riga and
			cod_prodotto = :ls_cod_prodotto and
			cod_reparto = :ls_cod_reparto and
			cod_lavorazione = :ls_cod_lavorazione;
	if sqlca.sqlcode = 0 then
		update avan_produzione_com
		set quan_prodotta = :ld_quan_prodotta
		where cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa and
			num_commessa = :ll_num_commessa and
			prog_riga = :ll_prog_riga and
			cod_prodotto = :ls_cod_prodotto and
			cod_reparto = :ls_cod_reparto and
			cod_lavorazione = :ls_cod_lavorazione;
		if sqlca.sqlcode = 0 then
			commit;
		else
			g_mb.error("Errore SQL in aggiornamento tabella Avan_Produzione_com." + sqlca.sqlerrtext)
			rollback;
		end if
	else
		g_mb.error("Errore SQL in somma quantita prodotta da tabella Dettaglio Orari (det_orari_produzione)." + sqlca.sqlerrtext)
		rollback;
	end if
	
	postevent("pcd_retrieve")
end if
		
		
end event

type tv_db from treeview within w_avanz_prod_com_attivazione
event ue_verifica_fasi ( )
integer x = 9
integer y = 8
integer width = 2757
integer height = 2572
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event ue_verifica_fasi();wf_verifica_fasi_start( )
end event

event constructor;this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "pf.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "semil.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "mp.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "integrazione.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "variante.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_aperta.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_in_corso.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_chiusa.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_esterna.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "ramo_descrittivo.bmp")

end event

event clicked;if handle <> 0 then
	
	il_handle = handle	
	dw_avanzamento_produzione_commesse.change_dw_current( )
	parent.triggerevent("pc_retrieve")

end if
end event

event rightclicked;treeviewitem ltv_padre
long			 ll_item_padre, ll_ret
s_chiave_distinta ls_chiave_distinta

setnull(is_cod_prodotto_prec)
setnull(is_cod_versione_prec)
setnull(is_cod_reparto_prec)
setnull(is_cod_lavorazione_prec)

if handle <> 0 then
	
	selectitem(handle)
	
	il_handle = handle
	
	dw_avanzamento_produzione_commesse.change_dw_current( )
	parent.triggerevent("pc_retrieve")
	
	m_avan_produzione l_menu
	
	l_menu = create m_avan_produzione
	l_menu.m_invianuovoterzista.enabled = false
	
	if dw_avanzamento_produzione_commesse.rowcount() < 1 then
		l_menu.m_dettaglisessioni.enabled = false
		l_menu.m_eliminasessioni.enabled = false
		l_menu.m_generabollaterzista.enabled = false
		l_menu.m_iniziosessione.enabled = false
		l_menu.m_rientromerceterzista.enabled = false
		l_menu.m_stampaordinelavoro.enabled = false
		l_menu.m_finesessione.enabled = false
		l_menu.m_terminafase.enabled = false
	end if
	
	if dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_fine_fase")="S" then
		l_menu.m_dettaglisessioni.enabled = true
		l_menu.m_eliminasessioni.HIDE()
		l_menu.m_generabollaterzista.enabled = false
		l_menu.m_iniziosessione.enabled = false
		l_menu.m_rientromerceterzista.enabled = false
		l_menu.m_stampaordinelavoro.enabled = true
		l_menu.m_finesessione.enabled = false
		l_menu.m_terminafase.enabled = false
	else
		l_menu.m_dettaglisessioni.enabled = true
		l_menu.m_eliminasessioni.show()
		l_menu.m_stampaordinelavoro.enabled = true
		l_menu.m_terminafase.enabled = true
		if dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_esterna")="S" then
			l_menu.m_generabollaterzista.enabled = true
			l_menu.m_rientromerceterzista.enabled = true
			
			//	*** Michela 12/06/2007: trovo il padre dell'item selezionato. se il padre è un terzista allora blocco il rientro e sblocco la nuova opzione
			
			ll_item_padre = FindItem( ParentTreeItem!, il_handle)
			
			if ll_item_padre > 0 and not isnull(ll_item_padre) then
				
				ll_ret = GetItem ( ll_item_padre, ltv_padre)
				
				if ll_ret > 0 then
					
					ls_chiave_distinta = ltv_padre.data
					
					if ls_chiave_distinta.flag_fase_esterna = "S" and ls_chiave_distinta.flag_fase_aperta = "A" then
						l_menu.m_rientromerceterzista.enabled = true
						l_menu.m_invianuovoterzista.enabled = true
						
						// *** mi salvo i precedenti
						is_cod_prodotto_prec = ls_chiave_distinta.cod_prodotto_figlio
						is_cod_versione_prec = ls_chiave_distinta.cod_versione_figlio
						is_cod_reparto_prec = ls_chiave_distinta.cod_reparto
						is_cod_lavorazione_prec = ls_chiave_distinta.cod_lavorazione
						
					end if
					
				end if
				
			end if			
			
			l_menu.m_iniziosessione.enabled = false
			l_menu.m_finesessione.enabled = false
			l_menu.m_controllagiacenzemag.enabled = false
			l_menu.m_finesessioneautomatico.enabled = false
		else
			l_menu.m_generabollaterzista.enabled = false
			l_menu.m_rientromerceterzista.enabled = false
			l_menu.m_iniziosessione.enabled = true
			l_menu.m_finesessione.enabled = true
			l_menu.m_controllagiacenzemag.enabled = true
			l_menu.m_finesessioneautomatico.enabled = true
		end if

	end if	
	
	l_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
	
	l_menu.idw_datawindow = dw_avanzamento_produzione_commesse
	
end if
end event

event selectionchanged;if newhandle <> 0 then
	
	il_handle = newhandle	
	dw_avanzamento_produzione_commesse.change_dw_current( )
	parent.triggerevent("pc_retrieve")

end if
end event

event getfocus;if il_handle <> 0 then 
	
	wf_calcola_stato_handle(il_handle)
	
end if
end event

type cb_fine_fase from commandbutton within w_avanz_prod_com_attivazione
integer x = 4091
integer y = 2368
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Fine F&ase"
end type

event clicked;long      ll_num_commessa,ll_prog_riga

string    ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio, ls_cod_versione, & 
		    ls_flag_fine_fase,ls_errore, ls_flag_blocco

double    ldd_quan_prodotta,ldd_quan_in_produzione

integer   li_anno_commessa,li_risposta,li_flag_ultima


li_anno_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "num_commessa")
ll_prog_riga = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "prog_riga")
ls_cod_prodotto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_prodotto")
ls_cod_versione = dw_avanzamento_produzione_commesse.getitemstring( dw_avanzamento_produzione_commesse.getrow(), "cod_versione")
ls_cod_reparto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_reparto")
ls_cod_lavorazione = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_lavorazione")
ls_flag_fine_fase = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_fine_fase")
ls_cod_operaio = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_operaio")
ls_flag_fine_fase = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_fine_fase")
ls_flag_blocco = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_blocco")

if ls_flag_blocco = "S" and not isnull(ls_flag_blocco) then
	g_mb.messagebox("SEP","Fase Bloccata")
	return
end if
li_risposta = f_fine_fase_commesse( li_anno_commessa, &
									ll_num_commessa,  &
									ll_prog_riga,     & 
									ls_cod_prodotto,  &
									ls_cod_versione,  &
									ls_cod_reparto,   &
									ls_cod_lavorazione, & 
								   ls_flag_fine_fase,  &
									ls_cod_operaio,  &
									ldd_quan_prodotta, & 
								   ldd_quan_in_produzione,  &
									li_flag_ultima,  &
									1,  &
									2,  &
									ls_errore)

if li_risposta= -1 then
	g_mb.messagebox("Sep",ls_errore,information!)
	rollback;
	return
end if

commit;

dw_avanzamento_produzione_commesse.setitem(dw_avanzamento_produzione_commesse.getrow(),"flag_fine_fase","S")
dw_avanzamento_produzione_commesse.resetupdate()

if li_flag_ultima = 1 then
	dw_avanzamento_produzione_commesse.i_parentdw.setitem(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"quan_in_produzione",ldd_quan_in_produzione)
	dw_avanzamento_produzione_commesse.i_parentdw.setitem(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"quan_prodotta",ldd_quan_prodotta)
	dw_avanzamento_produzione_commesse.i_parentdw.resetupdate()
	dw_avanzamento_produzione_commesse.triggerevent("pcd_retrieve")
end if

wf_calcola_stato_handle(il_handle)

end event

type cb_inizio from commandbutton within w_avanz_prod_com_attivazione
event clicked pbm_bnclicked
integer x = 2816
integer y = 2360
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Inizio Ses."
end type

event clicked;long    li_anno_commessa,ll_num_commessa,ll_prog_riga

string  ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio, ls_cod_versione, ls_cod_versione_prima, & 	
		  ls_flag_fine_fase,ls_errore,ls_cod_prodotto_prima,ls_flag_tipo_sessione,ls_flag_ora_automatica, ls_flag_blocco

double  ldd_quan_in_produzione

integer li_risposta,li_tipo_sessione


dw_selezione.accepttext()

li_anno_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "num_commessa")
ll_prog_riga = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "prog_riga")
ls_cod_prodotto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_prodotto")
ls_cod_versione = dw_avanzamento_produzione_commesse.getitemstring( dw_avanzamento_produzione_commesse.getrow(), "cod_versione")
ls_cod_reparto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_reparto")
ls_cod_lavorazione = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_lavorazione")
ldd_quan_in_produzione = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "quan_in_produzione")
ls_flag_fine_fase = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_fine_fase")
ls_cod_operaio = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_operaio")
ls_flag_tipo_sessione = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_tipo_sessione")
ls_flag_ora_automatica = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_ora_automatica")
ls_flag_fine_fase = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_fine_fase")
ls_flag_blocco = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_blocco")

if ls_flag_blocco = "S" and not isnull(ls_flag_blocco) then
	g_mb.messagebox("SEP","Fase Bloccata")
	return
end if
// *** Michela 05/10/2006: ho commentato questa parte perchè adesso nella distinta anche il prodotto figlio ha la versione
//                         quindi prendo direttamente quella e non quella del padre

//select cod_versione
//into   :ls_cod_versione
//from   anag_commesse
//where  cod_azienda = :s_cs_xx.cod_azienda and    
//		 anno_commessa = :li_anno_commessa and    
//		 num_commessa = :ll_num_commessa;
		 
choose case ls_flag_tipo_sessione
	case "L"
		li_tipo_sessione = 3
	case "A"
		li_tipo_sessione = 1
	case "C"
		li_tipo_sessione = 2
	case "R"
	li_tipo_sessione = 4
	ls_flag_fine_fase="N"
end choose

//if rb_attrezzaggio.checked = true then li_tipo_sessione = 1
//if rb_attrezzaggio_commessa.checked = true then li_tipo_sessione = 2
//if rb_lavorazione.checked = true then li_tipo_sessione = 3
//
//if rb_risorsa_umana.checked = true then 
//	li_tipo_sessione = 4
//	ls_flag_fine_fase="N"
//end if

if ls_flag_ora_automatica = "N" then
	s_cs_xx.parametri.parametro_t_1 = datetime(date(s_cs_xx.db_funzioni.data_neutra),time(dw_selezione.getitemdatetime(dw_selezione.getrow(),"ora_inizio") ))
	s_cs_xx.parametri.parametro_data_1 = datetime(date(dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_inizio")),time('00:00:00'))
else
	setnull(s_cs_xx.parametri.parametro_t_1)
	setnull(s_cs_xx.parametri.parametro_data_1)
end if


//if cbx_orario_automatico.checked=false then
//	s_cs_xx.parametri.parametro_t_1=datetime(date(s_cs_xx.db_funzioni.data_neutra),time(em_orario.text))
//	s_cs_xx.parametri.parametro_data_1=datetime(date(em_data.text),time('00:00:00'))
//else
//	setnull(s_cs_xx.parametri.parametro_t_1)
//	setnull(s_cs_xx.parametri.parametro_data_1)
//end if

li_risposta = f_inizio_sessione ( li_anno_commessa, &
											 ll_num_commessa, &
											 ll_prog_riga, & 
											 ls_cod_prodotto, &
											 ls_cod_versione, &
											 ls_cod_reparto, &
											 ls_cod_lavorazione, & 
											 ls_cod_operaio, &
											 ldd_quan_in_produzione, &
											 ls_flag_fine_fase, & 
											 li_tipo_sessione, &
											 ls_cod_prodotto_prima, &
											 ls_cod_versione_prima, &
											 ls_errore )

if li_risposta = -1 or li_risposta = -3 then
	g_mb.messagebox("Sep",ls_errore,stopsign!)
	rollback;
	return
end if

commit;

wf_calcola_stato_handle(il_handle)

g_mb.messagebox("Sep","Inizio sessione di fase completato con successo.",information!)


end event

type cb_fine from commandbutton within w_avanz_prod_com_attivazione
event clicked pbm_bnclicked
integer x = 3461
integer y = 2360
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Fine Ses."
end type

event clicked;integer  li_risposta,li_flag_inizio,li_anno_commessa,li_flag_ultima,li_tipo_sessione

long     ll_num_commessa,ll_prog_riga,ll_prog_orari

string   ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio, ls_flag_ora_automatica,& 
		   ls_test,ls_flag_fine_fase,ls_errore, ls_cod_versione,ls_flag_tipo_sessione, ls_flag_blocco

double   ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa,ldd_tempo_lavorazione, & 
		   ldd_quan_prodotta,ldd_quan_in_produzione, ldd_tempo_risorsa_umana	

datetime lt_orario_attrezzaggio,lt_orario_attrezzaggio_commessa,lt_orario_lavorazione


dw_selezione.accepttext()

li_anno_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "num_commessa")
ll_prog_riga = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "prog_riga")
ls_cod_prodotto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_prodotto")
ls_cod_versione = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_versione")
ls_cod_reparto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_reparto")
ls_cod_lavorazione = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_lavorazione")
ls_cod_operaio = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_operaio")
ls_flag_tipo_sessione = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_tipo_sessione")
ls_flag_ora_automatica = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_ora_automatica")
ls_flag_fine_fase = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_fine_fase")
ls_flag_blocco = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_blocco")

if ls_flag_blocco = "S" and not isnull(ls_flag_blocco) then
	g_mb.messagebox("SEP","Fase Bloccata")
	return
end if

choose case ls_flag_tipo_sessione
	case "L"
		li_tipo_sessione = 3
	case "A"
		li_tipo_sessione = 1
	case "C"
		li_tipo_sessione = 2
	case "R"
	li_tipo_sessione = 4
	ls_flag_fine_fase="N"
end choose


//if rb_attrezzaggio.checked = true then li_tipo_sessione = 1
//if rb_attrezzaggio_commessa.checked = true then li_tipo_sessione = 2
//if rb_lavorazione.checked = true then li_tipo_sessione = 3
//if rb_risorsa_umana.checked = true then li_tipo_sessione = 4 

if ls_flag_ora_automatica = "N" then
	s_cs_xx.parametri.parametro_t_1 = datetime(date(s_cs_xx.db_funzioni.data_neutra),time(dw_selezione.getitemdatetime(dw_selezione.getrow(),"ora_inizio") ))
	s_cs_xx.parametri.parametro_data_1 = datetime(date(dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_inizio")),time('00:00:00'))
else
	setnull(s_cs_xx.parametri.parametro_t_1)
	setnull(s_cs_xx.parametri.parametro_data_1)
end if
//if cbx_orario_automatico.checked=false then
//	s_cs_xx.parametri.parametro_t_1=datetime(date(s_cs_xx.db_funzioni.data_neutra),time(em_orario.text))
//	s_cs_xx.parametri.parametro_data_1=datetime(date(em_data.text),time('00:00:00'))
//else
//	setnull(s_cs_xx.parametri.parametro_t_1)
//	setnull(s_cs_xx.parametri.parametro_data_1)
//end if

li_risposta = f_fine_sessione ( li_anno_commessa, ll_num_commessa, ll_prog_riga, & 
										  ls_cod_prodotto, &
										  ls_cod_versione, &
										  ls_cod_reparto, ls_cod_lavorazione, & 
										  ls_cod_operaio, ldd_tempo_attrezzaggio, & 
										  ldd_tempo_attrezzaggio_commessa,ldd_tempo_lavorazione, & 
										  ldd_tempo_risorsa_umana,ldd_quan_in_produzione, & 
										  ldd_quan_prodotta, ls_flag_fine_fase, & 
										  1,li_tipo_sessione,ls_errore )


if li_risposta = -1 then
	g_mb.messagebox("Sep",ls_errore,stopsign!)
	rollback;
	return
end if	

choose case li_risposta
	case 1
		dw_avanzamento_produzione_commesse.setitem(dw_avanzamento_produzione_commesse.getrow(),"tempo_attrezzaggio",ldd_tempo_attrezzaggio)
	case 2
		dw_avanzamento_produzione_commesse.setitem(dw_avanzamento_produzione_commesse.getrow(),"tempo_attrezzaggio_commessa",ldd_tempo_attrezzaggio_commessa)
	case 3
		dw_avanzamento_produzione_commesse.setitem(dw_avanzamento_produzione_commesse.getrow(),"quan_in_produzione",ldd_quan_in_produzione)
		dw_avanzamento_produzione_commesse.setitem(dw_avanzamento_produzione_commesse.getrow(),"quan_prodotta",ldd_quan_prodotta)
		dw_avanzamento_produzione_commesse.setitem(dw_avanzamento_produzione_commesse.getrow(),"tempo_lavorazione",ldd_tempo_lavorazione)
	case 4
		dw_avanzamento_produzione_commesse.setitem(dw_avanzamento_produzione_commesse.getrow(),"tempo_risorsa_umana",ldd_tempo_risorsa_umana)
end choose

dw_avanzamento_produzione_commesse.resetupdate()


//Donato 17/12/2008 permettere scarico parziale del PF della commessa
long ll_num_righe, ll_index
datastore lds_righe_distinta
string ls_cod_sl, ls_cod_versione_sl
boolean lb_continua = false
double ld_qta_pf

if ldd_quan_in_produzione<>0 and li_risposta = 3 then
	//fase non chiusa: provare a vedere se si tratta di un SL di 1° livello 
	//e se si riesce a caricare, anche parzialmente il PF della commessa (Specifica Report_Produzione F5)
	
	//verifica si tratta di un SL di primo livello (cioè immediatamente sotto il PF della commessa)
	lds_righe_distinta = Create DataStore
	lds_righe_distinta.DataObject = "d_data_store_distinta_comm"
	lds_righe_distinta.SetTransObject(sqlca)
	
	//in "is_cod_prodotto_finito" e "is_cod_versione_pf" ci sono i dati del PF della commessa
	ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, is_cod_prodotto_finito, is_cod_versione_pf, li_anno_commessa, ll_num_commessa)
	
	//ora verifica che nelle righe del ds sia presente il SL (se si allora è di primo livello)
	for ll_index = 1 to ll_num_righe
		ls_cod_sl = lds_righe_distinta.getitemstring(ll_index, "cod_prodotto_figlio")
		ls_cod_versione_sl = lds_righe_distinta.getitemstring(ll_index, "cod_versione_figlio")
		
		if ls_cod_prodotto=ls_cod_sl and ls_cod_versione=ls_cod_versione_sl then
			//si tratta di un SL di primo livello
			lb_continua = true
			exit
		end if
	next
	
	if lb_continua then
		//trovato SL di primo livello
		//in "ldd_quan_prodotta" si ha la qta caricata
		lb_continua = false
		
		//verificare se tutte le fasi di lavoro dei SL dal 2° livello in giù sono chiuse
		lb_continua = wf_controlla_fasi_sl()
		if lb_continua then
			//via libera (tutte le fasi dal 2° livello in giù sono chiuse)
			lb_continua = false
			
			//adesso verifichiamo se i SL di 1° livello hanno una qta prodotta sufficiente, altrimenti
			//la qta di PF che eventualmente sarà scaricata potrebbe essere ridimensionata
			//o addirittura potrebbe essere inibito il carico PF
			ld_qta_pf = wf_controlla_qta_pf()
			if ld_qta_pf > 0 then
				//via libera allo scarico parziale dei SL di 1° livello
				//e conseguente carico parziale del PF della Commessa per una qta pari a "ld_qta_pf"
				lb_continua = true
				
				if g_mb.messagebox("Sep","Eseguire il carico parziale del PF?", Question!, YesNo!,2)=1 then
					li_risposta = wf_carico_parziale_pf(ld_qta_pf, &
																	li_anno_commessa, ll_num_commessa, &
																	ls_cod_prodotto, ls_cod_versione, &
																	ll_prog_riga, ls_cod_reparto,ls_cod_lavorazione, &
																	ls_flag_fine_fase, ls_cod_operaio, 1, &
																	ldd_quan_prodotta, ldd_quan_in_produzione, &
																	li_flag_ultima, 2, ls_errore)				
				
					if li_risposta= -1 then
						g_mb.messagebox("Sep",ls_errore,information!)
						rollback;
						return
					end if
					
					dw_avanzamento_produzione_commesse.i_parentdw.setitem(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"quan_in_produzione",ldd_quan_in_produzione)
					dw_avanzamento_produzione_commesse.i_parentdw.setitem(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"quan_prodotta",ldd_quan_prodotta)
					dw_avanzamento_produzione_commesse.i_parentdw.resetupdate()
					dw_avanzamento_produzione_commesse.triggerevent("pcd_retrieve")					
				end if				
			end if
		end if
	end if	
end if
//fine modifica -----------------------------------------------------------------------


if ldd_quan_in_produzione=0 and li_risposta = 3 then

	g_mb.messagebox("Sep","La quantità in produzione è uguale a 0 pertanto la fase sarà chiusa automaticamente.",information!)

	li_risposta=f_fine_fase_commesse(li_anno_commessa,ll_num_commessa,ll_prog_riga, & 
									ls_cod_prodotto, &
									ls_cod_versione, &
									ls_cod_reparto,ls_cod_lavorazione, & 
									ls_flag_fine_fase,ls_cod_operaio,ldd_quan_prodotta, & 
									ldd_quan_in_produzione,li_flag_ultima,1,2,ls_errore)

	if li_risposta= -1 then
		g_mb.messagebox("Sep",ls_errore,information!)
		rollback;
		return
	end if

	commit;

	dw_avanzamento_produzione_commesse.setitem(dw_avanzamento_produzione_commesse.getrow(),"flag_fine_fase","S")
	dw_avanzamento_produzione_commesse.resetupdate()

	if li_flag_ultima = 1 then
		dw_avanzamento_produzione_commesse.i_parentdw.setitem(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"quan_in_produzione",ldd_quan_in_produzione)
		dw_avanzamento_produzione_commesse.i_parentdw.setitem(dw_avanzamento_produzione_commesse.i_parentdw.getrow(),"quan_prodotta",ldd_quan_prodotta)
		dw_avanzamento_produzione_commesse.i_parentdw.resetupdate()
		dw_avanzamento_produzione_commesse.triggerevent("pcd_retrieve")
	end if

end if

commit;

wf_calcola_stato_handle(il_handle)


g_mb.messagebox("Sep","Fine sessione di fase completato con successo.",information!)

end event

type cb_esterna_interna from commandbutton within w_avanz_prod_com_attivazione
integer x = 4142
integer y = 1288
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Est./Int."
end type

event clicked;integer li_anno_commessa,li_anno_registrazione
long    ll_num_commessa,ll_prog_riga
string  ls_test,ls_cod_prodotto,ls_cod_prodotto_finito,ls_cod_reparto,ls_cod_lavorazione, & 
		  ls_flag_bolla_uscita,ls_flag_esterna, ls_cod_versione, ls_flag_blocco
double  ldd_quan_prodotta

setpointer(hourglass!)

li_anno_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "num_commessa")
ll_prog_riga = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "prog_riga")
ls_cod_prodotto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(), "cod_prodotto")
ls_cod_versione = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(), "cod_versione")
ls_cod_lavorazione = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(), "cod_lavorazione")
ls_cod_reparto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(), "cod_reparto")
ls_flag_blocco = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_blocco")

if ls_flag_blocco = "S" and not isnull(ls_flag_blocco) then
	g_mb.messagebox("SEP","Fase Bloccata")
	return
end if

select flag_bolla_uscita,
		 quan_prodotta,
		 flag_esterna
into   :ls_flag_bolla_uscita,
		 :ldd_quan_prodotta,
		 :ls_flag_esterna
from   avan_produzione_com
where  cod_azienda = :s_cs_xx.cod_azienda and 	 
		 anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa		and    
		 prog_riga = :ll_prog_riga				and    
		 cod_prodotto = :ls_cod_prodotto		and    
		 cod_versione = :ls_cod_versione 	and
		 cod_reparto = :ls_cod_reparto		and    
		 cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	setpointer(arrow!)
	return
end if

if ls_flag_bolla_uscita = 'S' then
	g_mb.messagebox("Sep","Non è più possibile cambiare lo stato della fase poichè è già stata spedita la merce con una bolla di uscita.", information!)
	setpointer(arrow!)
	return
end if

if ldd_quan_prodotta > 0 then
	g_mb.messagebox("Sep","Non è più possibile cambiare lo stato della fase poichè è già stata prodotta della quantità.", information!)
	setpointer(arrow!)
	return
end if

if ls_flag_esterna = "N" then 
	ls_flag_esterna = "S"
else
	ls_flag_esterna = "N"
end if

update  avan_produzione_com
set	  flag_esterna = :ls_flag_esterna 
where   cod_azienda = :s_cs_xx.cod_azienda and 	  
		  anno_commessa = :li_anno_commessa	 and     
		  num_commessa = :ll_num_commessa	 and     
		  prog_riga = :ll_prog_riga			 and     
		  cod_prodotto = :ls_cod_prodotto	 and     
		  cod_versione = :ls_cod_versione    and
		  cod_reparto = :ls_cod_reparto		 and     
		  cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
	setpointer(arrow!)
	return
end if

dw_avanzamento_produzione_commesse.setitem(dw_avanzamento_produzione_commesse.getrow(),"flag_esterna",ls_flag_esterna)
dw_avanzamento_produzione_commesse.resetupdate()

setpointer(arrow!)
end event

type cb_genera_bolle_out from commandbutton within w_avanz_prod_com_attivazione
integer x = 4142
integer y = 1196
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Invio Terz."
end type

event clicked;long    ll_anno_commessa, ll_num_commessa,ll_prog_riga
string  ls_cod_tipo_commessa,ls_flag_muovi_sl,ls_flag_muovi_mp,ls_flag_blocco

ll_anno_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "num_commessa")
ll_prog_riga    = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "prog_riga")
ls_flag_blocco = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_blocco")

if ls_flag_blocco = "S" and not isnull(ls_flag_blocco) then
	g_mb.messagebox("SEP","Fase Bloccata")
	return
end if


select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda and
		 anno_commessa=:ll_anno_commessa and
		 num_commessa=:ll_num_commessa;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in ricerca tipo Commessa~r~n " + sqlca.sqlerrtext,stopsign!)
	return
end if

if isnull(ls_cod_tipo_commessa) then
	g_mb.messagebox("Sep","Attenzione! Nella commessa non è stato specificato il tipo commessa",stopsign!)
	return
end if
	
select flag_muovi_sl,
		 flag_muovi_mp
into   :ls_flag_muovi_sl,
		 :ls_flag_muovi_mp
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in ricerca tipo commessa~r~n " + sqlca.sqlerrtext,stopsign!)
	return
end if

if ls_flag_muovi_sl = "S" and ls_flag_muovi_mp = 'S' then
	window_open_parm(w_gen_bolle_terzisti,-1,dw_avanzamento_produzione_commesse)
else
	g_mb.messagebox("Sep","Questa commessa appartiene ad un tipo commessa che non genera i movimenti dei Semilavorati o delle materie prime, pertanto non è possibile gestire i terzisti.",stopsign!)
	return
end if
end event

type cb_carica_bolle_in from commandbutton within w_avanz_prod_com_attivazione
integer x = 4142
integer y = 1380
integer width = 366
integer height = 80
integer taborder = 81
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carico Terz."
end type

event clicked;integer li_anno_commessa
long    ll_num_commessa
string  ls_cod_tipo_commessa,ls_flag_muovi_sl,ls_flag_muovi_mp,ls_flag_blocco

li_anno_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "anno_commessa")
ll_num_commessa = dw_avanzamento_produzione_commesse.getitemnumber(dw_avanzamento_produzione_commesse.getrow(), "num_commessa")
ls_flag_blocco = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"flag_blocco")

if ls_flag_blocco = "S" and not isnull(ls_flag_blocco) then
	g_mb.messagebox("SEP","Fase Bloccata")
	return
end if

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in ricerca commessa.~r~n" + sqlca.sqlerrtext,stopsign!)
	return
end if
	
select flag_muovi_sl,
		 flag_muovi_mp
into   :ls_flag_muovi_sl,
		 :ls_flag_muovi_mp
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in ricerca tipo commessa.~r~n " + sqlca.sqlerrtext,stopsign!)
	return
end if

if ls_flag_muovi_sl = "S" and ls_flag_muovi_mp = 'S' then
	
	s_cs_xx.parametri.parametro_b_2 = false
	
	//----------------------------------------------------------------------------------------------------------------------
	//Donato: 22/12/2008 eseguire qui il controllo se deve essere abilitato il carico PARZIALE del PF
	//al seguito di uno scarico PARZIALE di Sl di primo livello
	long ll_num_righe, ll_index
	datastore lds_righe_distinta
	string ls_cod_sl, ls_cod_versione_sl, ls_cod_prodotto, ls_cod_versione
	boolean lb_continua = false
	double ld_qta_pf
	
	//verifica si tratta di un SL di primo livello (cioè immediatamente sotto il PF della commessa)
	lds_righe_distinta = Create DataStore
	lds_righe_distinta.DataObject = "d_data_store_distinta_comm"
	lds_righe_distinta.SetTransObject(sqlca)
	
	//in "is_cod_prodotto_finito" e "is_cod_versione_pf" ci sono i dati del PF della commessa
	ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, is_cod_prodotto_finito, is_cod_versione_pf, li_anno_commessa, ll_num_commessa)
	
	//dati del Sl selezionato sul treeview
	ls_cod_prodotto = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_prodotto")
	ls_cod_versione = dw_avanzamento_produzione_commesse.getitemstring(dw_avanzamento_produzione_commesse.getrow(),"cod_versione")
	//-----------------------
	
	//ora verifica che nelle righe del ds sia presente il SL (se si allora è di primo livello)
	for ll_index = 1 to ll_num_righe
		ls_cod_sl = lds_righe_distinta.getitemstring(ll_index, "cod_prodotto_figlio")
		ls_cod_versione_sl = lds_righe_distinta.getitemstring(ll_index, "cod_versione_figlio")
		
		if ls_cod_prodotto=ls_cod_sl and ls_cod_versione=ls_cod_versione_sl then
			//si tratta di un SL di primo livello
			lb_continua = true
			exit
		end if
	next
	
	if lb_continua then
		//trovato SL di primo livello
		lb_continua = false
		
		//verificare se tutte le fasi di lavoro dei SL dal 2° livello in giù sono chiuse
		lb_continua = wf_controlla_fasi_sl()
		if lb_continua then
			//via libera (tutte le fasi dal 2° livello in giù sono chiuse)
			lb_continua = false
			
			//la verifica se i SL di 1° livello hanno una qta prodotta sufficiente sarà fatta
			//direttamente nella w_carica_bolle_terzisti
			
			//abilito qui a fare questo controllo
			s_cs_xx.parametri.parametro_d_9 = 99.99
		else
			setnull(s_cs_xx.parametri.parametro_d_9)
		end if
	end if	
	//fine modifica -----------------------------------------------------------------------------------------------------
	
	window_open_parm(w_carica_bolle_terzisti,-1,dw_avanzamento_produzione_commesse)
else
	g_mb.messagebox("Sep","Questa commessa appartiene ad un tipo commessa che non genera i movimenti dei Semilavorati o delle materie prime, pertanto non è possibile gestire i terzisti.",stopsign!)
	return
end if
end event

