﻿$PBExportHeader$w_avanz_prod_com_attivazione_qta.srw
$PBExportComments$Window Avanzamento Produzione con Attivazione
forward
global type w_avanz_prod_com_attivazione_qta from window
end type
type st_4 from statictext within w_avanz_prod_com_attivazione_qta
end type
type cb_1 from commandbutton within w_avanz_prod_com_attivazione_qta
end type
type st_3 from statictext within w_avanz_prod_com_attivazione_qta
end type
type st_2 from statictext within w_avanz_prod_com_attivazione_qta
end type
type st_1 from statictext within w_avanz_prod_com_attivazione_qta
end type
type em_1 from editmask within w_avanz_prod_com_attivazione_qta
end type
end forward

global type w_avanz_prod_com_attivazione_qta from window
integer width = 2487
integer height = 800
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_4 st_4
cb_1 cb_1
st_3 st_3
st_2 st_2
st_1 st_1
em_1 em_1
end type
global w_avanz_prod_com_attivazione_qta w_avanz_prod_com_attivazione_qta

event open;string ls_des_prodotto
s_avan_produzione_com_qta l_avan_produzione_com_qta

l_avan_produzione_com_qta = message.powerobjectparm

select des_prodotto
into  :ls_des_prodotto
from  anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_prodotto = :l_avan_produzione_com_qta.cod_prodotto;
		
if sqlca.sqlcode <> 0 then
	st_2.text = "ERRORE IN RICERCA PRODOTTO"
else
	st_2.text = l_avan_produzione_com_qta.cod_prodotto + ", " + ls_des_prodotto
end if

st_3.text = "Quatità unitaria indicata in distinta base = " + string(l_avan_produzione_com_qta.quan_utilizzo, "###,##0.0000")

end event

on w_avanz_prod_com_attivazione_qta.create
this.st_4=create st_4
this.cb_1=create cb_1
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.em_1=create em_1
this.Control[]={this.st_4,&
this.cb_1,&
this.st_3,&
this.st_2,&
this.st_1,&
this.em_1}
end on

on w_avanz_prod_com_attivazione_qta.destroy
destroy(this.st_4)
destroy(this.cb_1)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_1)
end on

event closequery;message.doubleparm = double(em_1.text)
end event

type st_4 from statictext within w_avanz_prod_com_attivazione_qta
integer x = 27
integer y = 508
integer width = 1650
integer height = 92
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 255
long backcolor = 16777215
string text = "Nuova Quantità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_avanz_prod_com_attivazione_qta
integer x = 960
integer y = 660
integer width = 457
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Conferma"
end type

event clicked;decimal ld_quantita

ld_quantita = dec(em_1.text)

if ld_quantita = 0 then
	messagebox("SEP", "Impostare una quantità maggiore di Zero", StopSign!)
	em_1.setfocus()
	return
end if

close(parent)
end event

type st_3 from statictext within w_avanz_prod_com_attivazione_qta
integer x = 23
integer y = 372
integer width = 2304
integer height = 96
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_2 from statictext within w_avanz_prod_com_attivazione_qta
integer x = 23
integer y = 248
integer width = 2309
integer height = 104
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_1 from statictext within w_avanz_prod_com_attivazione_qta
integer x = 23
integer y = 20
integer width = 2309
integer height = 216
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 255
long backcolor = 16777215
string text = "Indicare la Q.ta da Produrre del nuovo Semilavorato inserito in distinta."
alignment alignment = center!
boolean focusrectangle = false
end type

type em_1 from editmask within w_avanz_prod_com_attivazione_qta
integer x = 1696
integer y = 500
integer width = 626
integer height = 108
integer taborder = 10
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###,##0.00"
boolean spin = true
end type

