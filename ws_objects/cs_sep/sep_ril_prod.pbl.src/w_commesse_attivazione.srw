﻿$PBExportHeader$w_commesse_attivazione.srw
$PBExportComments$Window per attivazione produzione commesse
forward
global type w_commesse_attivazione from w_cs_xx_principale
end type
type cb_costi_correttivi from commandbutton within w_commesse_attivazione
end type
type dw_commesse_lista from uo_cs_xx_dw within w_commesse_attivazione
end type
type cb_dettaglio from commandbutton within w_commesse_attivazione
end type
type cb_attiva from commandbutton within w_commesse_attivazione
end type
type em_anno_commessa from editmask within w_commesse_attivazione
end type
type cbx_forza_numerazione from checkbox within w_commesse_attivazione
end type
type sle_num_commessa from singlelineedit within w_commesse_attivazione
end type
type cb_varianti from commandbutton within w_commesse_attivazione
end type
type cb_annulla_attiva from commandbutton within w_commesse_attivazione
end type
type st_4 from statictext within w_commesse_attivazione
end type
type cb_chiudi_commessa from commandbutton within w_commesse_attivazione
end type
type gb_flag_tipo_avanzamento from groupbox within w_commesse_attivazione
end type
type cbx_assegnazione from checkbox within w_commesse_attivazione
end type
type cbx_attivazione from checkbox within w_commesse_attivazione
end type
type cbx_automatico from checkbox within w_commesse_attivazione
end type
type cbx_chiusa_assegn from checkbox within w_commesse_attivazione
end type
type cbx_chiusa_attiv from checkbox within w_commesse_attivazione
end type
type cbx_chiusa_autom from checkbox within w_commesse_attivazione
end type
type cbx_nessuna from checkbox within w_commesse_attivazione
end type
type st_num_commesse from statictext within w_commesse_attivazione
end type
type cb_tutti from commandbutton within w_commesse_attivazione
end type
type st_5 from statictext within w_commesse_attivazione
end type
type st_3 from statictext within w_commesse_attivazione
end type
type cbx_simultaneo from checkbox within w_commesse_attivazione
end type
type st_cliente from statictext within w_commesse_attivazione
end type
type st_anno from statictext within w_commesse_attivazione
end type
type st_numero from statictext within w_commesse_attivazione
end type
type st_2 from statictext within w_commesse_attivazione
end type
type st_1 from statictext within w_commesse_attivazione
end type
type dw_commesse_1 from uo_cs_xx_dw within w_commesse_attivazione
end type
type mle_log from multilineedit within w_commesse_attivazione
end type
type dw_commesse_3 from uo_cs_xx_dw within w_commesse_attivazione
end type
type dw_commesse_2 from uo_cs_xx_dw within w_commesse_attivazione
end type
type dw_folder from u_folder within w_commesse_attivazione
end type
end forward

global type w_commesse_attivazione from w_cs_xx_principale
integer width = 3305
integer height = 1684
string title = "Commesse (Attivazione)"
cb_costi_correttivi cb_costi_correttivi
dw_commesse_lista dw_commesse_lista
cb_dettaglio cb_dettaglio
cb_attiva cb_attiva
em_anno_commessa em_anno_commessa
cbx_forza_numerazione cbx_forza_numerazione
sle_num_commessa sle_num_commessa
cb_varianti cb_varianti
cb_annulla_attiva cb_annulla_attiva
st_4 st_4
cb_chiudi_commessa cb_chiudi_commessa
gb_flag_tipo_avanzamento gb_flag_tipo_avanzamento
cbx_assegnazione cbx_assegnazione
cbx_attivazione cbx_attivazione
cbx_automatico cbx_automatico
cbx_chiusa_assegn cbx_chiusa_assegn
cbx_chiusa_attiv cbx_chiusa_attiv
cbx_chiusa_autom cbx_chiusa_autom
cbx_nessuna cbx_nessuna
st_num_commesse st_num_commesse
cb_tutti cb_tutti
st_5 st_5
st_3 st_3
cbx_simultaneo cbx_simultaneo
st_cliente st_cliente
st_anno st_anno
st_numero st_numero
st_2 st_2
st_1 st_1
dw_commesse_1 dw_commesse_1
mle_log mle_log
dw_commesse_3 dw_commesse_3
dw_commesse_2 dw_commesse_2
dw_folder dw_folder
end type
global w_commesse_attivazione w_commesse_attivazione

type variables
boolean ib_in_new=FALSE
boolean ib_attivata=false
uo_magazzino iuo_magazzino
end variables

forward prototypes
public function integer wf_proteggi_colonne ()
public function integer wf_imposta_bottoni ()
end prototypes

public function integer wf_proteggi_colonne ();string ls_flag_tipo_avanzamento 

ls_flag_tipo_avanzamento= dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(),"flag_tipo_avanzamento")

if ls_flag_tipo_avanzamento<>"0" then
	dw_commesse_1.Object.cod_tipo_commessa.Protect=1
	dw_commesse_1.Object.cod_tipo_commessa.Protect=1
	dw_commesse_1.Object.data_registrazione.Protect=1
	dw_commesse_1.Object.cod_operatore.Protect=1
	dw_commesse_1.Object.cod_prodotto.Protect=1
	dw_commesse_1.Object.cod_versione.Protect=1
	dw_commesse_1.Object.cod_deposito_prelievo.Protect=1
	dw_commesse_1.Object.cod_deposito_versamento.Protect=1
	dw_commesse_1.Object.quan_ordine.Protect=1
	dw_commesse_1.Object.data_consegna.Protect=1
else
	dw_commesse_1.Object.cod_tipo_commessa.Protect=0
	dw_commesse_1.Object.cod_tipo_commessa.Protect=0
	dw_commesse_1.Object.data_registrazione.Protect=0
	dw_commesse_1.Object.cod_operatore.Protect=0
	dw_commesse_1.Object.cod_prodotto.Protect=0
	dw_commesse_1.Object.cod_versione.Protect=0
	dw_commesse_1.Object.cod_deposito_prelievo.Protect=0
	dw_commesse_1.Object.cod_deposito_versamento.Protect=0
	dw_commesse_1.Object.quan_ordine.Protect=0
	dw_commesse_1.Object.data_consegna.Protect=0
end if

return 0
end function

public function integer wf_imposta_bottoni ();long ll_num_det, ll_anno_commessa, ll_num_commessa
string  ls_flag_tipo_avanzamento

if dw_commesse_1.rowcount()=0 then
	cb_annulla_attiva.enabled = false
	cb_attiva.enabled = false
	cb_chiudi_commessa.enabled = false
	cb_dettaglio.enabled = false
	return 0
end if
ls_flag_tipo_avanzamento = dw_commesse_1.getitemstring(dw_commesse_1.getrow(),"flag_tipo_avanzamento")

choose case ls_flag_tipo_avanzamento
	case "1" // Avanzamento Assegnazione
		cb_annulla_attiva.enabled = false
		cb_attiva.enabled = false
		cb_chiudi_commessa.enabled = false
		cb_dettaglio.enabled = false
		cb_varianti.enabled = false
	case "2" // Avanzamento Attivazione
		cb_annulla_attiva.enabled = true
		cb_attiva.enabled = true
		cb_chiudi_commessa.enabled = false
		cb_dettaglio.enabled = true
		cb_varianti.enabled = false
	case "3" // Avanzamento Automatico
		cb_annulla_attiva.enabled = false
		cb_attiva.enabled = false
		cb_chiudi_commessa.enabled = true
		ll_anno_commessa = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"anno_commessa")
		ll_num_commessa = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"num_commessa")
		
		select count(*)
		 into :ll_num_det
		 from det_anag_commesse
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :ll_anno_commessa and
				num_commessa = :ll_num_commessa;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attivazione Commesse", "Errore nell'Estrazione Dettagli Commesse")
			return 0
		end if
		if ll_num_det > 0 then
			cb_dettaglio.enabled = true
		else
			cb_dettaglio.enabled = false
		end if
		cb_varianti.enabled = false
	case "9" // Chiusura Commessa Assegnazione
		cb_annulla_attiva.enabled = false
		cb_attiva.enabled = false
		cb_chiudi_commessa.enabled = true
		cb_dettaglio.enabled = false
		cb_varianti.enabled = false
	case "8" // Chiusura Commessa Attivazione
		cb_annulla_attiva.enabled = true
		cb_attiva.enabled = true
		cb_chiudi_commessa.enabled = true
		cb_dettaglio.enabled = true
		cb_varianti.enabled = false
	case "7" // Chiusura Commessa Automatica
		cb_annulla_attiva.enabled = false
		cb_attiva.enabled = false
		cb_chiudi_commessa.enabled = true
		cb_varianti.enabled = false
		ll_anno_commessa = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"anno_commessa")
		ll_num_commessa = dw_commesse_1.getitemnumber(dw_commesse_1.getrow(),"num_commessa")
		
		select count(*)
		 into :ll_num_det
		 from det_anag_commesse
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :ll_anno_commessa and
				num_commessa = :ll_num_commessa;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attivazione Commesse", "Errore nell'Estrazione Dettagli Commesse")
			return 0
		end if
		if ll_num_det > 0 then
			cb_dettaglio.enabled = true
		else
			cb_dettaglio.enabled = false
		end if
	case else // Nessun Avanzamento
		cb_annulla_attiva.enabled = false
		cb_attiva.enabled = true
		cb_chiudi_commessa.enabled = true
		cb_dettaglio.enabled = false
		cb_varianti.enabled = true
end choose
return 0
end function

event pc_setwindow;call super::pc_setwindow;integer li_risposta
string ls_db

windowobject l_objects[ ],l1_objects[ ]

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

if ls_db = 'ORACLE' then
	dw_commesse_lista.DataObject = 'd_commesse_lista_ora'
end if

l_objects[1] = dw_commesse_1
dw_folder.fu_AssignTab(1, "&Principale", l_Objects[])
l_objects[1] = dw_commesse_2
l_objects[2] = st_3
l_objects[3] = st_2
l_objects[4] = st_1
l_objects[5] = st_5
l_objects[6] = st_anno
l_objects[7] = st_numero
l_objects[8] = st_cliente
dw_folder.fu_AssignTab(2, "&Note/Cliente", l_Objects[])
l1_objects[1] = dw_commesse_3
dw_folder.fu_AssignTab(3, "&Costi", l1_Objects[])
l1_objects[1] = mle_log
dw_folder.fu_AssignTab(4, "&Log Errori", l1_Objects[])
dw_folder.fu_FolderCreate(4,4)

dw_commesse_lista.set_dw_key("cod_azienda")
dw_commesse_lista.set_dw_options(sqlca,pcca.null_object,c_default + c_multiselect,c_default)
dw_commesse_1.set_dw_options(sqlca,dw_commesse_lista,c_sharedata+c_scrollparent,c_default)
dw_commesse_2.set_dw_options(sqlca,dw_commesse_lista,c_sharedata+c_scrollparent,c_default)
dw_commesse_3.set_dw_options(sqlca,dw_commesse_lista,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_commesse_lista

dw_folder.fu_SelectTab(1)
em_anno_commessa.text=string(f_anno_esercizio ( ))

end event

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW(dw_commesse_1,"cod_operatore",sqlca,&
                 "tab_operatori","cod_operatore","des_operatore",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_commesse_1,"cod_tipo_commessa",sqlca,&
                 "tab_tipi_commessa","cod_tipo_commessa","des_tipo_commessa",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_commesse_1,"cod_deposito_prelievo",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N'")

f_PO_LoadDDDW_DW(dw_commesse_1,"cod_deposito_versamento",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N'")

f_PO_LoadDDDW_DW(dw_commesse_3,"cod_centro_costo",sqlca,&
                 "tab_centri_costo","cod_centro_costo","des_centro_costo",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N'")


//f_PO_LoadDDDW_DW(dw_commesse_1,"cod_versione",sqlca,&
//   	              	  "distinta_padri","cod_versione","des_versione",&
//	      	           "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_commesse_attivazione.create
int iCurrent
call super::create
this.cb_costi_correttivi=create cb_costi_correttivi
this.dw_commesse_lista=create dw_commesse_lista
this.cb_dettaglio=create cb_dettaglio
this.cb_attiva=create cb_attiva
this.em_anno_commessa=create em_anno_commessa
this.cbx_forza_numerazione=create cbx_forza_numerazione
this.sle_num_commessa=create sle_num_commessa
this.cb_varianti=create cb_varianti
this.cb_annulla_attiva=create cb_annulla_attiva
this.st_4=create st_4
this.cb_chiudi_commessa=create cb_chiudi_commessa
this.gb_flag_tipo_avanzamento=create gb_flag_tipo_avanzamento
this.cbx_assegnazione=create cbx_assegnazione
this.cbx_attivazione=create cbx_attivazione
this.cbx_automatico=create cbx_automatico
this.cbx_chiusa_assegn=create cbx_chiusa_assegn
this.cbx_chiusa_attiv=create cbx_chiusa_attiv
this.cbx_chiusa_autom=create cbx_chiusa_autom
this.cbx_nessuna=create cbx_nessuna
this.st_num_commesse=create st_num_commesse
this.cb_tutti=create cb_tutti
this.st_5=create st_5
this.st_3=create st_3
this.cbx_simultaneo=create cbx_simultaneo
this.st_cliente=create st_cliente
this.st_anno=create st_anno
this.st_numero=create st_numero
this.st_2=create st_2
this.st_1=create st_1
this.dw_commesse_1=create dw_commesse_1
this.mle_log=create mle_log
this.dw_commesse_3=create dw_commesse_3
this.dw_commesse_2=create dw_commesse_2
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_costi_correttivi
this.Control[iCurrent+2]=this.dw_commesse_lista
this.Control[iCurrent+3]=this.cb_dettaglio
this.Control[iCurrent+4]=this.cb_attiva
this.Control[iCurrent+5]=this.em_anno_commessa
this.Control[iCurrent+6]=this.cbx_forza_numerazione
this.Control[iCurrent+7]=this.sle_num_commessa
this.Control[iCurrent+8]=this.cb_varianti
this.Control[iCurrent+9]=this.cb_annulla_attiva
this.Control[iCurrent+10]=this.st_4
this.Control[iCurrent+11]=this.cb_chiudi_commessa
this.Control[iCurrent+12]=this.gb_flag_tipo_avanzamento
this.Control[iCurrent+13]=this.cbx_assegnazione
this.Control[iCurrent+14]=this.cbx_attivazione
this.Control[iCurrent+15]=this.cbx_automatico
this.Control[iCurrent+16]=this.cbx_chiusa_assegn
this.Control[iCurrent+17]=this.cbx_chiusa_attiv
this.Control[iCurrent+18]=this.cbx_chiusa_autom
this.Control[iCurrent+19]=this.cbx_nessuna
this.Control[iCurrent+20]=this.st_num_commesse
this.Control[iCurrent+21]=this.cb_tutti
this.Control[iCurrent+22]=this.st_5
this.Control[iCurrent+23]=this.st_3
this.Control[iCurrent+24]=this.cbx_simultaneo
this.Control[iCurrent+25]=this.st_cliente
this.Control[iCurrent+26]=this.st_anno
this.Control[iCurrent+27]=this.st_numero
this.Control[iCurrent+28]=this.st_2
this.Control[iCurrent+29]=this.st_1
this.Control[iCurrent+30]=this.dw_commesse_1
this.Control[iCurrent+31]=this.mle_log
this.Control[iCurrent+32]=this.dw_commesse_3
this.Control[iCurrent+33]=this.dw_commesse_2
this.Control[iCurrent+34]=this.dw_folder
end on

on w_commesse_attivazione.destroy
call super::destroy
destroy(this.cb_costi_correttivi)
destroy(this.dw_commesse_lista)
destroy(this.cb_dettaglio)
destroy(this.cb_attiva)
destroy(this.em_anno_commessa)
destroy(this.cbx_forza_numerazione)
destroy(this.sle_num_commessa)
destroy(this.cb_varianti)
destroy(this.cb_annulla_attiva)
destroy(this.st_4)
destroy(this.cb_chiudi_commessa)
destroy(this.gb_flag_tipo_avanzamento)
destroy(this.cbx_assegnazione)
destroy(this.cbx_attivazione)
destroy(this.cbx_automatico)
destroy(this.cbx_chiusa_assegn)
destroy(this.cbx_chiusa_attiv)
destroy(this.cbx_chiusa_autom)
destroy(this.cbx_nessuna)
destroy(this.st_num_commesse)
destroy(this.cb_tutti)
destroy(this.st_5)
destroy(this.st_3)
destroy(this.cbx_simultaneo)
destroy(this.st_cliente)
destroy(this.st_anno)
destroy(this.st_numero)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_commesse_1)
destroy(this.mle_log)
destroy(this.dw_commesse_3)
destroy(this.dw_commesse_2)
destroy(this.dw_folder)
end on

event pc_delete;call super::pc_delete;long    ll_num_commessa,ll_prog_riga
integer li_i,li_risposta,li_anno_commessa
string  ls_errore

for li_i = 1 to dw_commesse_lista.deletedcount()

	if dw_commesse_lista.getitemstring(li_i, "flag_blocco", delete!, true) = "S" then
	   g_mb.messagebox("Sep", "Commessa non cancellabile: è bloccata.", &
	              exclamation!, ok!)
	   dw_commesse_lista.set_dw_view(c_ignorechanges)
		triggerevent("pc_retrieve")
	   pcca.error = c_fatal
	   return
	end if

	li_anno_commessa = dw_commesse_lista.getitemnumber(li_i, "anno_commessa", delete!, true)
	ll_num_commessa = dw_commesse_lista.getitemnumber(li_i, "num_commessa", delete!, true)

	li_risposta = f_reset_commessa ( li_anno_commessa, ll_num_commessa, ls_errore )

	if li_risposta = -2 then
		g_mb.messagebox("Sep","Attenzione! La commessa è gia stata parzialmente o totalmente prodotta, pertanto non è possibile eliminarla",exclamation!)
		dw_commesse_lista.set_dw_view(c_ignorechanges)
		triggerevent("pc_retrieve")
	   pcca.error = c_fatal
	   return
	end if

	if li_risposta = -1 then 
		g_mb.messagebox("Sep",ls_errore,exclamation!)
		dw_commesse_lista.set_dw_view(c_ignorechanges)
		triggerevent("pc_retrieve")
	   pcca.error = c_fatal
		rollback;
	   return
	end if

next
end event

event pc_new;call super::pc_new;dw_commesse_1.setitem(dw_commesse_1.getrow(), "data_registrazione", datetime(today()))
end event

event open;call super::open;iuo_magazzino = create uo_magazzino
end event

event close;call super::close;destroy iuo_magazzino
end event

type cb_costi_correttivi from commandbutton within w_commesse_attivazione
integer x = 686
integer y = 1480
integer width = 343
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Costi C."
end type

event clicked;long ll_row


ll_row = dw_commesse_lista.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.messagebox("SEP","Selezionare una commessa prima di continuare!")
	return -1
end if

window_open_parm(w_costi_correttivi,-1,dw_commesse_lista)
end event

type dw_commesse_lista from uo_cs_xx_dw within w_commesse_attivazione
event post_rowfocuschange ( )
integer x = 23
integer y = 20
integer width = 800
integer height = 1220
integer taborder = 60
string dataobject = "d_commesse_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event post_rowfocuschange();integer li_risposta
	
	li_risposta=wf_imposta_bottoni()
	li_risposta=wf_proteggi_colonne()
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa
string ls_tipo_avanzamento

ls_tipo_avanzamento = ""

if cbx_assegnazione.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "1"
end if
if cbx_attivazione.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "2"
end if
if cbx_automatico.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "3"
end if
if cbx_chiusa_assegn.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "9"
end if
if cbx_chiusa_attiv.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "8"
end if
if cbx_chiusa_autom.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "7"
end if
if cbx_nessuna.checked then
	ls_tipo_avanzamento = ls_tipo_avanzamento + "0"
end if

//if	ls_tipo_avanzamento = "" then ls_tipo_avanzamento = "%"

ll_anno_commessa=long(em_anno_commessa.text)
l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno_commessa, ls_tipo_avanzamento)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
else
	st_num_commesse.text = "Nr. Commesse: " + string(l_error)
END IF
end event

event pcd_new;call super::pcd_new;setitem(getrow(),"flag_tassativo","N")

if i_extendmode then
	long ll_anno,ll_num_commessa,ll_test,ll_num_commessa_forzato
	integer li_risposta
		
	select numero
	into   :ll_anno	
	from   parametri_azienda
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_parametro='ESC';

	select max(num_commessa)
	into   :ll_num_commessa
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    anno_commessa= :ll_anno;

	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_commessa) then
  		ll_num_commessa = 1
	else
  		ll_num_commessa = ll_num_commessa + 1
	end if

	if cbx_forza_numerazione.checked=true then
		
		ll_num_commessa_forzato = long(sle_num_commessa.text)
		
		select num_commessa
		into   :ll_test
		from   anag_commesse
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_commessa= :ll_anno
		and    num_commessa=:ll_num_commessa_forzato;
		
		if sqlca.sqlcode=0 then
			g_mb.messagebox("Sep","Numerazione forzata non valida!",exclamation!)
		else
			ll_num_commessa=ll_num_commessa_forzato
		end if

	end if
	
	this.SetItem (this.GetRow ( ),"num_commessa", ll_num_commessa)
	this.SetItem (this.GetRow ( ),"anno_commessa", ll_anno)
	
	li_risposta = wf_imposta_bottoni()
//	cb_aggiorna.enabled=false
	cb_attiva.enabled=false
	cb_dettaglio.enabled=false
	cb_chiudi_commessa.enabled=false
	dw_commesse_1.object.b_ricerca_prodotto.enabled = true
	cb_varianti.enabled=false
	cb_costi_correttivi.enabled = false

end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	integer li_risposta 
	string ls_cod_cliente,ls_rag_soc
	long   ll_anno_ordine,ll_num_ordine,ll_anno_commessa,ll_num_commessa

	postevent("rowfocuschange")

	ll_anno_commessa = getitemnumber(getrow(),"anno_commessa")
	ll_num_commessa = getitemnumber(getrow(),"num_commessa")
	
	f_PO_LoadDDDW_DW(dw_commesse_1,"cod_versione",sqlca,&
  	              	  "distinta_padri","cod_versione","des_versione",&
      	           "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + getitemstring(getrow(),"cod_prodotto") +"'")

	select anno_registrazione,
			 num_registrazione
	into   :ll_anno_ordine,
			 :ll_num_ordine
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:ll_anno_commessa and
			num_commessa=:ll_num_commessa;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		return
	end if

	if sqlca.sqlcode <> 100 then
		select cod_cliente
		into   :ls_cod_cliente
		from   tes_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_ordine
		and    num_registrazione=:ll_num_ordine;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			return
		end if

		select rag_soc_1
		into   :ls_rag_soc
		from   anag_clienti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_cliente=:ls_cod_cliente;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			return
		end if

		st_anno.text = string(ll_anno_ordine)
		st_numero.text = string(ll_num_ordine)
		st_cliente.text = ls_cod_cliente + " " +ls_rag_soc 
	else
		st_anno.text = ""
		st_numero.text = ""
		st_cliente.text = ""
	end if
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	integer li_risposta
	li_risposta = wf_imposta_bottoni()
//	cb_aggiorna.enabled=false
	cb_attiva.enabled=false
	cb_dettaglio.enabled=false
	cb_chiudi_commessa.enabled=false
	dw_commesse_1.object.b_ricerca_prodotto.enabled = true
	cb_varianti.enabled=false
	cb_costi_correttivi.enabled = false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	integer li_risposta
	li_risposta = wf_imposta_bottoni()
//	cb_aggiorna.enabled=true
	dw_commesse_1.object.b_ricerca_prodotto.enabled = false
	cb_varianti.enabled=true
	cb_attiva.enabled=true
	cb_costi_correttivi.enabled = true
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	string  ls_cod_prodotto, ls_cod_versione, ls_cod_prodotto_old, ls_cod_versione_old,ls_cod_tipo_commessa,&
			  ls_flag_muovi_mp,ls_parametro,ls_messaggio,ls_errore
	dec{4}  ldd_quan_assegnata,ldd_quan_prodotta,ldd_quan_in_ordine, &
			  ldd_quan_impegnata,ldd_quan_in_produzione, &
			  ldd_quan_assegnata_old, ldd_quan_prodotta_old,ldd_quan_in_ordine_old, &
			  ldd_quan_impegnata_old,ldd_quan_in_produzione_old
	long    ll_i,ll_anno_commessa,ll_num_commessa,ll_null, ll_num_reg,ll_prog_riga_ord_ven
	integer li_anno_registrazione
	
	


	for ll_i = 1 to rowcount()
		
		ll_anno_commessa = getitemnumber(ll_i, "anno_commessa")
		ll_num_commessa = getitemnumber(ll_i, "num_commessa")
		
		select cod_prodotto,
				 cod_versione,
				 quan_in_produzione,
				 quan_assegnata,
				 quan_prodotta,
				 quan_ordine
		into   :ls_cod_prodotto_old,
				 :ls_cod_versione_old,
				 :ldd_quan_in_produzione_old,
				 :ldd_quan_assegnata_old,
				 :ldd_quan_prodotta_old,
				 :ldd_quan_in_ordine_old
		from   anag_commesse
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_commessa = :ll_anno_commessa and
				 num_commessa = :ll_num_commessa;
		
		ls_cod_prodotto = getitemstring(ll_i, "cod_prodotto")
		ls_cod_tipo_commessa = getitemstring(ll_i, "cod_tipo_commessa")
		ls_cod_versione = getitemstring(ll_i, "cod_versione")
		ldd_quan_in_produzione = getitemnumber(ll_i, "quan_in_produzione")
		ldd_quan_assegnata = getitemnumber(ll_i, "quan_assegnata")
		ldd_quan_prodotta = getitemnumber(ll_i, "quan_prodotta")
		ldd_quan_in_ordine = getitemnumber(ll_i, "quan_ordine")
		
		select flag_muovi_mp
		into   :ls_flag_muovi_mp
		from   tab_tipi_commessa
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_tipo_commessa=:ls_cod_tipo_commessa;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
		setnull(ll_null)
		
		if (ls_cod_prodotto <> ls_cod_prodotto_old or ls_cod_versione <> ls_cod_versione_old or &
			ldd_quan_in_produzione <> ldd_quan_in_produzione_old or ldd_quan_assegnata <> ldd_quan_assegnata_old or &
			ldd_quan_prodotta <> ldd_quan_prodotta_old or ldd_quan_in_ordine <> ldd_quan_in_ordine_old) and & 
			ls_flag_muovi_mp='S' then
			
			ldd_quan_impegnata = ldd_quan_in_ordine - ldd_quan_assegnata - ldd_quan_in_produzione - ldd_quan_prodotta
			ldd_quan_impegnata_old = ldd_quan_in_ordine_old - ldd_quan_assegnata_old - ldd_quan_in_produzione_old - ldd_quan_prodotta_old
		
			
			if not isnull(ls_cod_prodotto_old) and ls_cod_prodotto_old <> "" then
				if iuo_magazzino.uof_impegna_mp_commessa(	false, &
																		false, &
																		ls_cod_prodotto_old, &
																		ls_cod_versione_old, &
																		ldd_quan_impegnata_old, &
																		ll_anno_commessa, &
																		ll_num_commessa, &
																		ls_errore) = -1 then
					g_mb.messagebox("Apice","Errore durante il disimpegno.~r~n"+ls_errore,stopsign!)
					rollback;
					return 1
				end if
//				if f_impegna_mp( ls_cod_prodotto_old, ls_cod_versione_old, ldd_quan_impegnata_old, ll_anno_commessa, &
//									ll_num_commessa, ll_null, "varianti_commesse", False ) = -1 then // Prima Disimpegno la quantita vecchia
//					messagebox("SEP","Errore durante l'aggiornamento prodotti",stopsign!)
//					return 1
//				end if
			end if
			
			if iuo_magazzino.uof_impegna_mp_commessa(	true, &
																	false, &
																	ls_cod_prodotto, &
																	ls_cod_versione, &
																	ldd_quan_impegnata, &
																	ll_anno_commessa, &
																	ll_num_commessa, &
																	ls_errore) = -1 then
				g_mb.messagebox("Apice","Errore durante il disimpegno.~r~n"+ls_errore,stopsign!)
				rollback;
				return 1
			end if
//			if f_impegna_mp( ls_cod_prodotto, ls_cod_versione, ldd_quan_impegnata, ll_anno_commessa, ll_num_commessa, &
//								ll_null, "varianti_commesse", True ) = -1 then // Poi Impegno la nuova quantita
//				messagebox("SEP","Errore durante l'aggiornamento prodotti",stopsign!)
//				return 1
//			end if
	
		end if
		
	next	

	for ll_i = 1 to deletedcount()
      ll_anno_commessa = getitemnumber(ll_i, "anno_commessa", delete!, true)
      ll_num_commessa = getitemnumber(ll_i, "num_commessa", delete!, true)
		
		uo_funzioni_1 luo_funzioni_commesse
		luo_funzioni_commesse = CREATE uo_funzioni_1
		if luo_funzioni_commesse.uof_cancella_commessa(ll_anno_commessa,ll_num_commessa, ls_messaggio) = -1 then
			g_mb.messagebox("Sep", ls_messaggio)
			destroy luo_funzioni_commesse
			return 1
		end if

	next
end if

end event

event pcd_validaterow;call super::pcd_validaterow;long ll_num_dettagli, ll_anno_commessa, ll_num_commessa

ll_anno_commessa = this.getitemnumber(this.getrow(), "anno_commessa")
ll_num_commessa = this.getitemnumber(this.getrow(), "num_commessa")

ll_num_dettagli = 0

select count(*)  
into  :ll_num_dettagli  
from  det_anag_commesse
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_commessa = :ll_anno_commessa and
		num_commessa = :ll_num_commessa;

if ll_num_dettagli > 0 then
	g_mb.messagebox("Attivazione Commesse","Commessa già Attivata, Non si può modificare")
	pcca.error = c_fatal
end if

end event

event pcd_save;call super::pcd_save;cb_costi_correttivi.enabled = true
end event

type cb_dettaglio from commandbutton within w_commesse_attivazione
integer x = 2903
integer y = 1480
integer width = 343
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettaglio"
end type

event clicked;string ls_flag_tipo_avanzamento

ls_flag_tipo_avanzamento = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "flag_tipo_avanzamento")

if ls_flag_tipo_avanzamento = '1' or ls_flag_tipo_avanzamento = '9' then
	g_mb.messagebox("Sep","La commessa è stata iniziata con la programmazione di produzione, pertanto non può essere gestita con questa maschera.", information!)
	return
else
	window_open_parm(w_det_anag_commesse_attivazione,-1,dw_commesse_lista)
end if


end event

type cb_attiva from commandbutton within w_commesse_attivazione
event clicked pbm_bnclicked
integer x = 1417
integer y = 1480
integer width = 343
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Attiva"
end type

event clicked;double  ldd_quan_in_produzione,ldd_quan_in_ordine, & 
		  ldd_quan_prodotta,ldd_quan_da_assegnare,ldd_quantita_possibile, & 
		  ldd_quan_assegnata_st, ldd_quan_assegnata_mp
long    ll_num_commessa,ll_prog_riga,ll_prog_stock[1],ll_anno_reg_des_mov, ll_num_reg_des_mov
string  ls_errore,ls_test,ls_cod_deposito_prelievo, ls_cod_tipo_mov_anticipo, ls_flag_tipo_avanzamento,& 
		  ls_cod_tipo_commessa,ls_cod_tipo_movimento,ls_cod_prodotto,ls_cod_deposito[1], & 
		  ls_cod_ubicazione[1],ls_cod_lotto[1],ls_cod_cliente[1],ls_cod_fornitore[1],ls_cod_versione
integer li_risposta,li_anno_commessa
date 	  ld_data_stock[1]

s_cs_xx_parametri				lstr_parametri

li_anno_commessa = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "anno_commessa")
ll_num_commessa = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "num_commessa")
ldd_quan_in_produzione = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_in_produzione")
ldd_quan_prodotta = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_prodotta")
ldd_quan_in_ordine = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_ordine")
ls_cod_prodotto = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_prodotto")
ls_cod_deposito_prelievo = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_deposito_prelievo")
ls_cod_tipo_commessa = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_tipo_commessa")
ls_cod_versione = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_versione")
ls_flag_tipo_avanzamento = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "flag_tipo_avanzamento")

if ls_flag_tipo_avanzamento <> "2" and ls_flag_tipo_avanzamento <> "0" and ls_flag_tipo_avanzamento <> "8" then
	g_mb.messagebox("Sep","Questa commessa è già stata lanciata in produzione con un'altro metodo, pertanto non è possibile compiere alcuna operazione con questo metodo.",exclamation!)
	return
end if

select cod_prodotto_figlio
into   :ls_test
from   distinta
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto_padre=:ls_cod_prodotto;

if sqlca.sqlcode = 100 then
	g_mb.messagebox("Sep","Attenzione! Il prodotto della commessa non ha un distinta base associata, pertanto non sarà possibile assegnare le materie prime. Creare la distinta base del prodotto",exclamation!)
	return
end if

if ldd_quan_in_produzione + ldd_quan_prodotta >= ldd_quan_in_ordine then
	if g_mb.messagebox("Sep","La somma delle quantità prodotta e in produzione è uguale o supera la quantità in ordine: si vuole produrre una nuova sottocommessa per aumentare la quantità in produzione?",question!,yesno!,1) = 2 then 
		return
	else
		ldd_quan_in_ordine = 0
	end if
else
	ldd_quan_in_ordine = ldd_quan_in_ordine - ldd_quan_in_produzione - ldd_quan_prodotta
end if

select cod_tipo_mov_ver_prod_finiti,
		 cod_tipo_mov_anticipo
into   :ls_cod_tipo_movimento,
		 :ls_cod_tipo_mov_anticipo
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:ls_cod_tipo_commessa;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	setpointer(arrow!)
	return
end if

if isnull(ls_cod_tipo_movimento) or isnull(ls_cod_tipo_mov_anticipo) then
	g_mb.messagebox("Sep","Manca la configurazione dei tipi movimento in tabella tipi commessa!",exclamation!)
	setpointer(arrow!)
	return
end if


lstr_parametri.parametro_d_2 = ldd_quan_in_ordine
lstr_parametri.parametro_s_1 = ""

openwithparm(w_attiva, lstr_parametri)

lstr_parametri = message.powerobjectparm

if lstr_parametri.parametro_i_1 = 0 then return

ldd_quan_in_produzione =lstr_parametri.parametro_d_1

setpointer(hourglass!)

select max(prog_riga)
into   :ll_prog_riga
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return
end if

if not isnull(ll_prog_riga) then
	ll_prog_riga++
else
	ll_prog_riga=1
end if

INSERT INTO det_anag_commesse
          ( cod_azienda,
            anno_commessa,   
            num_commessa,   
            prog_riga,   
            anno_registrazione,   
            num_registrazione,   
            quan_assegnata,   
            quan_in_produzione,   
            quan_prodotta,
				cod_tipo_movimento,
				anno_reg_des_mov,
				num_reg_des_mov,
				cod_tipo_mov_anticipo,
				quan_anticipo,
				anno_reg_anticipo,
				num_reg_anticipo )  
VALUES    ( :s_cs_xx.cod_azienda,   
            :li_anno_commessa,   
            :ll_num_commessa,   
            :ll_prog_riga,   
            null,   
            null,
            0,
            :ldd_quan_in_produzione,
            0,
				:ls_cod_tipo_movimento,
				null,
				null,
				:ls_cod_tipo_mov_anticipo,
				0,
				null,
				null);

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return
end if

li_risposta = f_avan_prod_com (li_anno_commessa, ll_num_commessa, ll_prog_riga, & 
										 ls_cod_prodotto, ls_cod_versione, ldd_quan_in_produzione, & 
										 1, ls_cod_tipo_commessa, ls_errore )


if li_risposta = -1 then
	rollback;
	g_mb.messagebox("Sep",ls_errore,exclamation!)
	setpointer(arrow!)
	return
end if

select sum(quan_in_produzione),
		 sum(quan_prodotta)
into   :ldd_quan_in_produzione,
		 :ldd_quan_prodotta
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return
end if

update anag_commesse
set    quan_in_produzione=:ldd_quan_in_produzione,
		 flag_tipo_avanzamento = '2'
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<>0 then
	g_mb.messagebox("Sep","Errore nel DB:" + sqlca.sqlerrtext,exclamation!)
	rollback;
	setpointer(arrow!)
	return
end if


dw_commesse_1.setitem(dw_commesse_1.getrow(),"quan_in_produzione",ldd_quan_in_produzione)
dw_commesse_1.setitem(dw_commesse_1.getrow(),"flag_tipo_avanzamento",'2')
dw_commesse_lista.resetupdate()
commit;


li_risposta=wf_imposta_bottoni()

setpointer(arrow!)
end event

type em_anno_commessa from editmask within w_commesse_attivazione
integer x = 2834
integer y = 1300
integer width = 274
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
boolean autoskip = true
boolean spin = true
string displaydata = ""
double increment = 1
string minmax = "1900~~2999"
end type

event modified;dw_commesse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cbx_forza_numerazione from checkbox within w_commesse_attivazione
integer x = 23
integer y = 1260
integer width = 503
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Forza Numerazione"
boolean lefttext = true
end type

event clicked;if sle_num_commessa.enabled=true then
	sle_num_commessa.enabled=false
else
	sle_num_commessa.enabled=true
end if
	
end event

type sle_num_commessa from singlelineedit within w_commesse_attivazione
integer x = 23
integer y = 1360
integer width = 270
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

type cb_varianti from commandbutton within w_commesse_attivazione
integer x = 2149
integer y = 1480
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Varianti Com."
end type

event clicked;s_cs_xx.parametri.parametro_dw_1 = dw_commesse_lista
window_open(w_varianti_commesse,-1)
end event

type cb_annulla_attiva from commandbutton within w_commesse_attivazione
integer x = 1783
integer y = 1480
integer width = 343
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&nnulla A."
end type

event clicked;double  ldd_quan_in_produzione,ldd_quan_in_ordine, & 
		  ldd_quan_prodotta,ldd_quan_da_assegnare,ldd_quantita_possibile, & 
		  ldd_quan_assegnata_st, ldd_quan_assegnata_mp
long    ll_num_commessa,ll_prog_riga,ll_prog_stock[1],ll_anno_reg_des_mov, ll_num_reg_des_mov
string  ls_errore,ls_test,ls_cod_deposito_prelievo, ls_cod_tipo_mov_anticipo, ls_flag_tipo_avanzamento, & 
		  ls_cod_tipo_commessa,ls_cod_tipo_movimento,ls_cod_prodotto,ls_cod_deposito[1], & 
		  ls_cod_ubicazione[1],ls_cod_lotto[1],ls_cod_cliente[1],ls_cod_fornitore[1],ls_cod_versione
integer li_risposta,li_anno_commessa
date 	  ld_data_stock[1]

li_anno_commessa = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "anno_commessa")
ll_num_commessa = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "num_commessa")
ldd_quan_in_produzione = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_in_produzione")
ldd_quan_prodotta = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_prodotta")
ldd_quan_in_ordine = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "quan_ordine")
ls_cod_prodotto = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_prodotto")
ls_cod_deposito_prelievo = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_deposito_prelievo")
ls_cod_tipo_commessa = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_tipo_commessa")
ls_cod_versione = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "cod_versione")
ls_flag_tipo_avanzamento = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "flag_tipo_avanzamento")

if ls_flag_tipo_avanzamento <> "2" and ls_flag_tipo_avanzamento <> "8" then
	g_mb.messagebox("Sep","Questa commessa è già stata lanciata in produzione con un altro metodo, pertanto non è possibile compiere alcuna operazione con questo metodo.",exclamation!)
	return
end if

if g_mb.messagebox("Sep","Tutte le sottocommesse stanno per essere eliminate. Sei Sicuro?",question!,yesno!,2) = 2 then return

select cod_azienda
into   :ls_test
from   det_orari_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode = 100 then
	
	setpointer(hourglass!)
	
	delete avan_produzione_com
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, information!)
		rollback;
		setpointer(arrow!)
		return
	end if

	delete mat_prime_commessa
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, information!)
		rollback;
		setpointer(arrow!)
		return
	end if

	delete det_anag_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, information!)
		rollback;
		setpointer(arrow!)
		return
	end if

	update anag_commesse
	set    quan_in_produzione = 0,
			 flag_tipo_avanzamento = '0'
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext, information!)
		rollback;
		setpointer(arrow!)
		return
	end if

	dw_commesse_1.setitem(dw_commesse_1.getrow(),"quan_in_produzione",0)
	dw_commesse_lista.resetupdate()
	commit;

li_risposta=wf_imposta_bottoni()

else
	g_mb.messagebox("Sep","Questa commessa ha dei dettagli orari in alcune sottocommesse, pertanto non è possibile annullarne l'attivazione: eliminare prima i dettagli orari di tutte le eventuali sottocommesse.",exclamation!)
	return
end if
end event

type st_4 from statictext within w_commesse_attivazione
integer x = 2674
integer y = 1300
integer width = 137
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Anno:"
boolean focusrectangle = false
end type

type cb_chiudi_commessa from commandbutton within w_commesse_attivazione
integer x = 2537
integer y = 1480
integer width = 343
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Avan&z. Aut"
end type

event clicked;integer li_anno_commessa,li_risposta,li_flag_errore,li_anno_registrazione
long ll_row[],ll_righe,ll_num_commessa,ll_selected[],ll_num_reg,ll_prog_riga_ord_ven
string ls_flag_tipo_avanzamento,ls_errore,ls_parametro,ls_null

setnull(ls_null)

if cbx_simultaneo.checked = true then
	if g_mb.messagebox("SEP","Attenzione! Sarà eseguito l'avanzamento automatico per tutte le commesse selezionate nella lista. Si ricorda che le commesse saranno chiuse definitivamente senza possibilità di riaprirle per eventuali aggiunte di produzione. Vuoi proseguire? ",  &
		Exclamation!, YesNo!, 1) = 2 then return


	dw_commesse_lista.get_selected_rows(ll_selected[])
	li_flag_errore = 0
	for ll_righe = 1 to upperbound(ll_selected)
		li_anno_commessa = dw_commesse_lista.getitemnumber(ll_selected[ll_righe], "anno_commessa")
		ll_num_commessa = dw_commesse_lista.getitemnumber(ll_selected[ll_righe], "num_commessa")
		ls_flag_tipo_avanzamento = dw_commesse_lista.getitemstring(ll_selected[ll_righe], "flag_tipo_avanzamento")

		if ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" or &
			ls_flag_tipo_avanzamento = "1" or ls_flag_tipo_avanzamento = "2" then
			continue
		else
			
			uo_funzioni_1 luo_funzioni_1
			luo_funzioni_1 = create uo_funzioni_1
			
			li_risposta = luo_funzioni_1.uof_avanza_commessa(li_anno_commessa,ll_num_commessa,ls_null,ls_errore)
			
			if li_risposta < 0 then 
				li_risposta = f_scrivi_log ("Errore sulla commessa anno " + string(li_anno_commessa) +" numero " + string(ll_num_commessa) + ". Dettaglio errore: " + ls_errore)
				mle_log.text = mle_log.text + "Errore sulla commessa anno " + string(li_anno_commessa) +" numero " + string(ll_num_commessa) + ". Dettaglio errore: " + ls_errore + char(13)
				li_flag_errore = 1
				if li_risposta= -1 then
					if g_mb.messagebox("Sep","Non è stato possibile scrivere il file di log. Questo errore non è bloccante (riguarda esclusivamente la creazione del file log molto probabilmente manca il parametro log.), la produzione può continuare normalmente, vuoi proseguire?",question!,YesNo!,1) = 2 then
						rollback;
						destroy luo_funzioni_1
						return					
					end if
				end if					
				
				destroy luo_funzioni_1
				rollback;
				continue
			end if
			
			destroy luo_funzioni_1
			commit;
		end if 
		
	next
	
	dw_commesse_lista.change_dw_current()
	parent.triggerevent("pc_retrieve")
	
	if li_flag_errore = 1 then
		g_mb.messagebox("SEP","Durante l'avanzamento automatico si è verificato almeno un errore. Controllare il folder Log Errori oppure il file Log.", information!)
	
	end if
	
	g_mb.messagebox("SEP","Avanzamento Automatico Simultaneo eseguito con successo!", information!)
	
else
	
	ll_row[1] = dw_commesse_lista.getrow()
	
	li_anno_commessa = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "anno_commessa")
	ll_num_commessa = dw_commesse_lista.getitemnumber(dw_commesse_lista.getrow(), "num_commessa")
	s_cs_xx.parametri.parametro_i_1 = li_anno_commessa
	s_cs_xx.parametri.parametro_ul_1 = ll_num_commessa
	
	ls_flag_tipo_avanzamento = dw_commesse_lista.getitemstring(dw_commesse_lista.getrow(), "flag_tipo_avanzamento")
	
	if ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" then
		if g_mb.messagebox("Avanzamento Automatico", &
						  "La Commessa è stata già chiusa, si Desidera Riaprirla?", question!, yesno!) = 2 then
			return
		end if
	elseif ls_flag_tipo_avanzamento = "1" or ls_flag_tipo_avanzamento = "2" then
		g_mb.messagebox("Avanzamento Automatico", "Impossibile Proseguire, La Commessa è stata già Attivata o Assegnata")
		return
	end if
	
	window_open(w_lista_stock_mp_commesse, 0)
	
	// cancellazione tabella distinte_taglio_calcolate
	
	select stringa
	into   :ls_parametro
	from   parametri
	where  cod_parametro='DT';
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("SEP","Si è verificato un errore durante la lettura tabella parametri. Errore sul DB: " + sqlca.sqlerrtext,information!)
		return 
	end if
	
	if ls_parametro='S' then
		
		select anno_registrazione,
				 num_registrazione,
				 prog_riga_ord_ven
		into   :li_anno_registrazione,
				 :ll_num_reg,
				 :ll_prog_riga_ord_ven
		from   det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("SEP","Si è verificato un errore durante la lettura tabella det_ord_ven. Errore sul DB:" + sqlca.sqlerrtext,information!)
			return 
		end if
	
		delete distinte_taglio_calcolate
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_reg
		and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("SEP","Si è verificato un errore durante la cancellazione tabella distinte_taglio_calcolate. Errore sul DB: " + sqlca.sqlerrtext,information!)
			return
		end if
	
	end if
	
	dw_commesse_lista.change_dw_current()
	parent.triggerevent("pc_retrieve")
	
end if
end event

type gb_flag_tipo_avanzamento from groupbox within w_commesse_attivazione
integer x = 686
integer y = 1240
integer width = 2560
integer height = 220
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Filtro Commesse per tipo Avanzamento"
end type

type cbx_assegnazione from checkbox within w_commesse_attivazione
integer x = 709
integer y = 1300
integer width = 480
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Assegnazione"
end type

event clicked;dw_commesse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cbx_attivazione from checkbox within w_commesse_attivazione
integer x = 1257
integer y = 1300
integer width = 389
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Attivazione"
end type

event clicked;dw_commesse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cbx_automatico from checkbox within w_commesse_attivazione
integer x = 1714
integer y = 1300
integer width = 457
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Automatico"
end type

event clicked;dw_commesse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cbx_chiusa_assegn from checkbox within w_commesse_attivazione
integer x = 709
integer y = 1380
integer width = 480
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Assegn. Chiusa"
end type

event clicked;dw_commesse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cbx_chiusa_attiv from checkbox within w_commesse_attivazione
integer x = 1257
integer y = 1380
integer width = 389
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Attiv. Chiusa"
end type

event clicked;dw_commesse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cbx_chiusa_autom from checkbox within w_commesse_attivazione
integer x = 1714
integer y = 1380
integer width = 507
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Autom.Chiusa"
end type

event clicked;dw_commesse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cbx_nessuna from checkbox within w_commesse_attivazione
integer x = 2263
integer y = 1300
integer width = 343
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Nessuno"
boolean checked = true
end type

event clicked;dw_commesse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type st_num_commesse from statictext within w_commesse_attivazione
integer x = 2263
integer y = 1380
integer width = 800
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nr.Commesse: 0"
boolean focusrectangle = false
end type

type cb_tutti from commandbutton within w_commesse_attivazione
integer x = 1051
integer y = 1480
integer width = 343
integer height = 80
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Visual.&Tutte"
end type

event clicked;cbx_assegnazione.checked = true
cbx_attivazione.checked = true
cbx_automatico.checked = true
cbx_chiusa_assegn.checked = true
cbx_chiusa_attiv.checked = true
cbx_chiusa_autom.checked = true
cbx_nessuna.checked = true
//cb_aggiorna.triggerevent(clicked!)
dw_commesse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")

end event

type st_5 from statictext within w_commesse_attivazione
integer x = 1641
integer y = 916
integer width = 224
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Cliente"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_3 from statictext within w_commesse_attivazione
integer x = 978
integer y = 916
integer width = 613
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = " Ordine Collegato"
boolean focusrectangle = false
end type

type cbx_simultaneo from checkbox within w_commesse_attivazione
integer x = 23
integer y = 1500
integer width = 640
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Av.Aut.Simult. Comm.sel."
boolean lefttext = true
end type

type st_cliente from statictext within w_commesse_attivazione
integer x = 1664
integer y = 980
integer width = 1440
integer height = 196
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_anno from statictext within w_commesse_attivazione
integer x = 1253
integer y = 980
integer width = 338
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_numero from statictext within w_commesse_attivazione
integer x = 1253
integer y = 1060
integer width = 338
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_2 from statictext within w_commesse_attivazione
integer x = 978
integer y = 980
integer width = 247
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Anno:"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_1 from statictext within w_commesse_attivazione
integer x = 978
integer y = 1060
integer width = 247
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Numero:"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_commesse_1 from uo_cs_xx_dw within w_commesse_attivazione
integer x = 869
integer y = 120
integer width = 2354
integer height = 1080
integer taborder = 70
string dataobject = "d_commesse_det_1"
boolean border = false
end type

event pcd_validaterow;call super::pcd_validaterow;	string ls_test,ls_cod_prodotto
	
	ls_cod_prodotto=getitemstring(getrow(),"cod_prodotto")
	
	select cod_prodotto_padre
	into   :ls_test
	from   distinta
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto_padre=:ls_cod_prodotto;

	if ls_test="" or isnull(ls_test) then
		g_mb.messagebox("Sep","Attenzione! Il prodotto selezionato non ha una distinta base associata.",exclamation!)
		pcca.error=c_valfailed
		return 
	end if


end event

event itemchanged;call super::itemchanged;choose case i_colname
  case  "cod_tipo_commessa"

	string ls_cod_deposito_versamento,ls_cod_deposito_prelievo,ls_cod_tipo_mov_prel_mat_prime, &
			 ls_cod_tipo_mov_ver_prod_finiti,ls_cod_tipo_mov_reso_mat_prime,ls_cod_tipo_mov_reso_semilav, &
			 ls_cod_tipo_mov_sfrido_mat_prime,ls_cod_tipo_mov_sfrido_semilav, ls_cod_versione
	
	SELECT cod_deposito_versamento,
          cod_deposito_prelievo,
			 cod_tipo_mov_prel_mat_prime,
			 cod_tipo_mov_ver_prod_finiti,
			 cod_tipo_mov_reso_mat_prime,
			 cod_tipo_mov_reso_semilav,
			 cod_tipo_mov_sfrido_mat_prime,
			 cod_tipo_mov_sfrido_semilav
   INTO   :ls_cod_deposito_versamento,
          :ls_cod_deposito_prelievo,
			 :ls_cod_tipo_mov_prel_mat_prime,
			 :ls_cod_tipo_mov_ver_prod_finiti,
			 :ls_cod_tipo_mov_reso_mat_prime,
			 :ls_cod_tipo_mov_reso_semilav,
			 :ls_cod_tipo_mov_sfrido_mat_prime,
			 :ls_cod_tipo_mov_sfrido_semilav
   FROM   tab_tipi_commessa
   WHERE  cod_azienda =:s_cs_xx.cod_azienda
   AND    cod_tipo_commessa =:data ;
 
	if isnull(ls_cod_tipo_mov_prel_mat_prime) or isnull(ls_cod_tipo_mov_ver_prod_finiti) or &
		isnull(ls_cod_tipo_mov_reso_mat_prime) or isnull(ls_cod_tipo_mov_reso_semilav) or &
		isnull(ls_cod_tipo_mov_sfrido_mat_prime) or isnull(ls_cod_tipo_mov_sfrido_semilav) then
		
		g_mb.messagebox("Sep","Attenzione! Il tipo commessa selezionato ha uno o più tipi movimento non impostato, questo provoca dei problemi in fase di avanzamento produzione: verificare il tipo commessa e assegnare gli opportuni movimenti.", exclamation!)
		return
	end if

	setitem(row,"cod_deposito_versamento",ls_cod_deposito_versamento)
	setitem(row,"cod_deposito_prelievo",ls_cod_deposito_prelievo)

	case "cod_prodotto"
		f_PO_LoadDDDW_DW(dw_commesse_1,"cod_versione",sqlca,&
   	              	  "distinta_padri","cod_versione","des_versione",&
	      	           "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext +"'")

		select cod_versione
		into   :ls_cod_versione
		from   distinta_padri
		where  cod_Azienda=:s_cs_xx.cod_Azienda
		and    cod_prodotto=:i_coltext
		and    flag_predefinita='S';

		setitem(row,"cod_versione",ls_cod_versione)
		
end choose


end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_commesse_1, "cod_prodotto")
end choose
end event

type mle_log from multilineedit within w_commesse_attivazione
integer x = 891
integer y = 160
integer width = 2309
integer height = 1020
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
end type

type dw_commesse_3 from uo_cs_xx_dw within w_commesse_attivazione
integer x = 891
integer y = 160
integer width = 2331
integer height = 1060
integer taborder = 21
string dataobject = "d_commesse_det_3"
boolean border = false
end type

type dw_commesse_2 from uo_cs_xx_dw within w_commesse_attivazione
integer x = 960
integer y = 140
integer width = 2098
integer height = 780
integer taborder = 40
string dataobject = "d_commesse_det_2"
boolean border = false
end type

type dw_folder from u_folder within w_commesse_attivazione
integer x = 846
integer y = 20
integer width = 2400
integer height = 1220
integer taborder = 30
end type

on po_tabclicked;call u_folder::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Data &Generici"
      SetFocus(dw_commesse_1)
   CASE "&Note"
      SetFocus(dw_commesse_2)
END CHOOSE

end on

