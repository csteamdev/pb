﻿$PBExportHeader$w_det_anag_commesse_attivazione.srw
$PBExportComments$Window dettaglio attivazione commesse
forward
global type w_det_anag_commesse_attivazione from w_cs_xx_principale
end type
type dw_det_anag_commesse_lista from uo_cs_xx_dw within w_det_anag_commesse_attivazione
end type
type dw_det_anag_commesse_det from uo_cs_xx_dw within w_det_anag_commesse_attivazione
end type
type cb_mat_prime from commandbutton within w_det_anag_commesse_attivazione
end type
type cb_avanzamento from commandbutton within w_det_anag_commesse_attivazione
end type
type cb_stampa_barcode from commandbutton within w_det_anag_commesse_attivazione
end type
type cb_anticipi from commandbutton within w_det_anag_commesse_attivazione
end type
type cb_difetti from commandbutton within w_det_anag_commesse_attivazione
end type
type cb_in_terzisti from commandbutton within w_det_anag_commesse_attivazione
end type
type cb_out_terzisti from commandbutton within w_det_anag_commesse_attivazione
end type
end forward

global type w_det_anag_commesse_attivazione from w_cs_xx_principale
integer width = 3049
integer height = 1424
string title = "Dettaglio Commesse (Attivazione)"
dw_det_anag_commesse_lista dw_det_anag_commesse_lista
dw_det_anag_commesse_det dw_det_anag_commesse_det
cb_mat_prime cb_mat_prime
cb_avanzamento cb_avanzamento
cb_stampa_barcode cb_stampa_barcode
cb_anticipi cb_anticipi
cb_difetti cb_difetti
cb_in_terzisti cb_in_terzisti
cb_out_terzisti cb_out_terzisti
end type
global w_det_anag_commesse_attivazione w_det_anag_commesse_attivazione

forward prototypes
public function integer wf_imposta_bottoni ()
end prototypes

public function integer wf_imposta_bottoni ();string ls_test
double ldd_quan_in_produzione,ldd_quan_prodotta
long ll_anno_commessa,ll_num_commessa,ll_prog_riga,ll_conteggio, ldd_quan_anticipo

ll_anno_commessa = dw_det_anag_commesse_lista.i_parentdw.getitemnumber(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = dw_det_anag_commesse_lista.i_parentdw.getitemnumber(dw_det_anag_commesse_lista.i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = dw_det_anag_commesse_det.getitemnumber(dw_det_anag_commesse_det.getrow(),"prog_riga")
ldd_quan_in_produzione = dw_det_anag_commesse_det.getitemnumber(dw_det_anag_commesse_det.getrow(),"quan_in_produzione")
ldd_quan_anticipo = dw_det_anag_commesse_det.getitemnumber(dw_det_anag_commesse_det.getrow(),"quan_anticipo")

cb_avanzamento.enabled=false
cb_mat_prime.enabled=false

select cod_azienda
into   :ls_test
from   avan_produzione_com
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:ll_anno_commessa
and    num_commessa=:ll_num_commessa
and    prog_riga=:ll_prog_riga;

if ls_test = s_cs_xx.cod_azienda then cb_avanzamento.enabled = true
ls_test = ""

select cod_azienda
into   :ls_test
from   mat_prime_commessa
where  cod_azienda =:s_cs_xx.cod_azienda
and    anno_commessa =:ll_anno_commessa
and    num_commessa =:ll_num_commessa
and    prog_riga =:ll_prog_riga;

if ls_test=s_cs_xx.cod_azienda then cb_mat_prime.enabled = true

if ldd_quan_anticipo > 0 then
	cb_anticipi.enabled = true
else
	cb_anticipi.enabled = false
end if

return 0
end function

on w_det_anag_commesse_attivazione.create
int iCurrent
call super::create
this.dw_det_anag_commesse_lista=create dw_det_anag_commesse_lista
this.dw_det_anag_commesse_det=create dw_det_anag_commesse_det
this.cb_mat_prime=create cb_mat_prime
this.cb_avanzamento=create cb_avanzamento
this.cb_stampa_barcode=create cb_stampa_barcode
this.cb_anticipi=create cb_anticipi
this.cb_difetti=create cb_difetti
this.cb_in_terzisti=create cb_in_terzisti
this.cb_out_terzisti=create cb_out_terzisti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_anag_commesse_lista
this.Control[iCurrent+2]=this.dw_det_anag_commesse_det
this.Control[iCurrent+3]=this.cb_mat_prime
this.Control[iCurrent+4]=this.cb_avanzamento
this.Control[iCurrent+5]=this.cb_stampa_barcode
this.Control[iCurrent+6]=this.cb_anticipi
this.Control[iCurrent+7]=this.cb_difetti
this.Control[iCurrent+8]=this.cb_in_terzisti
this.Control[iCurrent+9]=this.cb_out_terzisti
end on

on w_det_anag_commesse_attivazione.destroy
call super::destroy
destroy(this.dw_det_anag_commesse_lista)
destroy(this.dw_det_anag_commesse_det)
destroy(this.cb_mat_prime)
destroy(this.cb_avanzamento)
destroy(this.cb_stampa_barcode)
destroy(this.cb_anticipi)
destroy(this.cb_difetti)
destroy(this.cb_in_terzisti)
destroy(this.cb_out_terzisti)
end on

event pc_setwindow;call super::pc_setwindow;integer li_risposta

dw_det_anag_commesse_lista.set_dw_options(sqlca, &
                                    		i_openparm, &
		                                    c_scrollparent + c_nonew + c_nomodify + c_nodelete, &
      		                              c_default)

dw_det_anag_commesse_det.set_dw_options(sqlca,dw_det_anag_commesse_lista,c_sharedata + c_scrollparent + c_nonew + c_nomodify + c_nodelete,c_default)

iuo_dw_main = dw_det_anag_commesse_lista
end event

type dw_det_anag_commesse_lista from uo_cs_xx_dw within w_det_anag_commesse_attivazione
event post_rowfocuschanged ( )
integer x = 23
integer y = 20
integer width = 1211
integer height = 1180
integer taborder = 70
string dataobject = "d_det_anag_commesse_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event post_rowfocuschanged();integer li_risposta
	li_risposta=wf_imposta_bottoni()
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	postevent("post_rowfocuschanged")
end if
end event

type dw_det_anag_commesse_det from uo_cs_xx_dw within w_det_anag_commesse_attivazione
integer x = 1257
integer y = 20
integer width = 1737
integer height = 1180
integer taborder = 80
string dataobject = "d_det_anag_commesse_det"
borderstyle borderstyle = styleraised!
end type

type cb_mat_prime from commandbutton within w_det_anag_commesse_attivazione
integer x = 2240
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Mat. Prime"
end type

event clicked;window_open_parm(w_mat_prime_commessa_attivazione,-1,dw_det_anag_commesse_det)
end event

type cb_avanzamento from commandbutton within w_det_anag_commesse_attivazione
integer x = 2629
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Avanz. Prod."
end type

event clicked;
window_open_parm(w_avanz_prod_com_attivazione,-1,dw_det_anag_commesse_lista)
end event

type cb_stampa_barcode from commandbutton within w_det_anag_commesse_attivazione
event clicked pbm_bnclicked
integer x = 23
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa BC."
end type

event clicked;s_cs_xx.parametri.parametro_dw_1 = dw_det_anag_commesse_lista
window_open(w_report_bl_attivazione,-1)	

end event

type cb_anticipi from commandbutton within w_det_anag_commesse_attivazione
event clicked pbm_bnclicked
integer x = 1851
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&nticipi"
end type

event clicked;window_open_parm(w_det_commesse_anticipi, -1, dw_det_anag_commesse_det)
end event

type cb_difetti from commandbutton within w_det_anag_commesse_attivazione
integer x = 1463
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Difetti/Scarti"
end type

event clicked;window_open_parm(w_rileva_difetti_out_vista, -1, dw_det_anag_commesse_det)
end event

type cb_in_terzisti from commandbutton within w_det_anag_commesse_attivazione
integer x = 1074
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ingressi CTL."
end type

event clicked;
window_open_parm(w_ingressi_terzisti,-1,dw_det_anag_commesse_det)
end event

type cb_out_terzisti from commandbutton within w_det_anag_commesse_attivazione
integer x = 686
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 31
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Uscite CTL."
end type

event clicked;
window_open_parm(w_uscite_terzisti,-1,dw_det_anag_commesse_det)
end event

