﻿$PBExportHeader$w_lista_stock_mp_commesse.srw
$PBExportComments$Window Lista Stock Materie Prime per Chiusura Automatica Commessa
forward
global type w_lista_stock_mp_commesse from w_cs_xx_risposta
end type
type st_4 from statictext within w_lista_stock_mp_commesse
end type
type st_3 from statictext within w_lista_stock_mp_commesse
end type
type st_2 from statictext within w_lista_stock_mp_commesse
end type
type em_tot from editmask within w_lista_stock_mp_commesse
end type
type em_ru from editmask within w_lista_stock_mp_commesse
end type
type em_mp from editmask within w_lista_stock_mp_commesse
end type
type cb_1 from commandbutton within w_lista_stock_mp_commesse
end type
type dw_ext_lista_mp from uo_cs_xx_dw within w_lista_stock_mp_commesse
end type
type cb_esegui from commandbutton within w_lista_stock_mp_commesse
end type
type em_quan_prodotta from editmask within w_lista_stock_mp_commesse
end type
type st_1 from statictext within w_lista_stock_mp_commesse
end type
type cbx_flag_chiudi_commessa from checkbox within w_lista_stock_mp_commesse
end type
type cb_annulla from commandbutton within w_lista_stock_mp_commesse
end type
type dw_ext_elenco_stock from datawindow within w_lista_stock_mp_commesse
end type
type wstr_lista_stock from structure within w_lista_stock_mp_commesse
end type
end forward

type wstr_lista_stock from structure
	string		cod_prodotto
	string		cod_deposito
	string		cod_ubicazione
	string		cod_lotto
	datetime		data_stock
	long		prog_stock
	double		quan_disponibile		descriptor "comment" = "disponibilità dello stock"
	double		quantita		descriptor "comment" = "Quantità del movimento di magazzino"
	boolean		flag_movimento		descriptor "comment" = "Stabilisce se eseguire oppure no il movimento di magazzino"
end type

global type w_lista_stock_mp_commesse from w_cs_xx_risposta
integer width = 3547
integer height = 2376
string title = "Avanzamento Automatico"
st_4 st_4
st_3 st_3
st_2 st_2
em_tot em_tot
em_ru em_ru
em_mp em_mp
cb_1 cb_1
dw_ext_lista_mp dw_ext_lista_mp
cb_esegui cb_esegui
em_quan_prodotta em_quan_prodotta
st_1 st_1
cbx_flag_chiudi_commessa cbx_flag_chiudi_commessa
cb_annulla cb_annulla
dw_ext_elenco_stock dw_ext_elenco_stock
end type
global w_lista_stock_mp_commesse w_lista_stock_mp_commesse

type variables
integer ii_anno_commessa
long il_num_commessa
wstr_lista_stock istr_lista_stock[]
end variables

forward prototypes
public function integer wf_crea_lista_mp ()
public function integer wf_estrai_stock (string fs_cod_prodotto, string fs_cod_deposito)
public function integer wf_carica_str_lista_stock (string fs_cod_prodotto, string fs_cod_deposito, ref double fdd_quan_utilizzo)
end prototypes

public function integer wf_crea_lista_mp ();long ll_anno_commessa, ll_num_commessa, ll_null
string ls_cod_prodotto, ls_cod_versione, ls_materia_prima[], ls_cod_deposito_prelievo,ls_errore,ls_stringa,ls_cod_misura, ls_versione_prima[], ls_null
double ldd_quan_prodotta, ldd_quantita_utilizzo[]
integer li_i, li_row[]
uo_funzioni_1 luo_funzioni

dw_ext_lista_mp.reset()

setnull(ll_null)
setnull(ls_null)

select cod_prodotto,
		cod_versione,
		cod_deposito_prelievo
 into :ls_cod_prodotto,   
		:ls_cod_versione,
		:ls_cod_deposito_prelievo
 from anag_commesse
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_commessa = :ii_anno_commessa and
		num_commessa = :il_num_commessa;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Chiusura Automatica Commessa","Errore durante lettura commesse.",stopsign!)
	return -1
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("Chiusura Automatica Commessa","Commessa non Trovata in Anagrafica Commesse",stopsign!)
	return -1
end if	

ldd_quan_prodotta = double(em_quan_prodotta.text)

luo_funzioni = create uo_funzioni_1

if luo_funzioni.uof_trova_mat_prime_varianti(ls_cod_prodotto, &
										ls_cod_versione, &
										ls_null, &
										ref ls_materia_prima[], &
										ref ls_versione_prima[], &
										ref ldd_quantita_utilizzo[], &
										ldd_quan_prodotta, &
										ii_anno_commessa, &
										il_num_commessa, &
										ll_null, &
										"varianti_commesse",&
										ls_null, &
										ls_errore) = -1 then
	g_mb.messagebox("Sep", "Errore sulla lettura distinta base: " + ls_errore , exclamation!, ok!)
	destroy luo_funzioni
	return -1
end if

destroy luo_funzioni

for li_i = 1 to upperbound(ls_materia_prima)
	select cod_misura_mag
	into   :ls_cod_misura
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_materia_prima[li_i];

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Sep","Errore durante lettura unità misura su anagrafica prodotti.",stopsign!)
		return -1
	end if
	
	li_row[1] = dw_ext_lista_mp.insertrow(0)
	dw_ext_lista_mp.setitem(li_row[1], "cod_prodotto", ls_materia_prima[li_i])
	dw_ext_lista_mp.setitem(li_row[1], "cod_deposito", ls_cod_deposito_prelievo)
	dw_ext_lista_mp.setitem(li_row[1], "quan_utilizzo", ldd_quantita_utilizzo[li_i])
	dw_ext_lista_mp.setitem(li_row[1], "unita_misura", ls_cod_misura)
	wf_carica_str_lista_stock(ls_materia_prima[li_i], ls_cod_deposito_prelievo,ldd_quantita_utilizzo[li_i])
	dw_ext_lista_mp.setitem(li_row[1], "quan_residua", ldd_quantita_utilizzo[li_i])
next

if dw_ext_lista_mp.rowcount() > 0 then
	li_row[1] = 1
	dw_ext_lista_mp.set_selected_rows(1, &
												 li_row[], &
												 c_ignorechanges, &
												 c_refreshchildren, &
												 c_refreshsame)
end if
return upperbound(ls_materia_prima)
end function

public function integer wf_estrai_stock (string fs_cod_prodotto, string fs_cod_deposito);long ll_row[]

dw_ext_elenco_stock.reset()

long ll_i
for ll_i = 1 to UpperBound(istr_lista_stock)
	if istr_lista_stock[ll_i].cod_prodotto = fs_cod_prodotto then
		ll_row[1] = dw_ext_elenco_stock.insertrow(0)
		dw_ext_elenco_stock.setitem(ll_row[1], "cod_prodotto", istr_lista_stock[ll_i].cod_prodotto)
		dw_ext_elenco_stock.setitem(ll_row[1], "cod_deposito", istr_lista_stock[ll_i].cod_deposito)
		dw_ext_elenco_stock.setitem(ll_row[1], "cod_ubicazione", istr_lista_stock[ll_i].cod_ubicazione)
		dw_ext_elenco_stock.setitem(ll_row[1], "cod_lotto", istr_lista_stock[ll_i].cod_lotto)
		dw_ext_elenco_stock.setitem(ll_row[1], "data_stock", istr_lista_stock[ll_i].data_stock)
		dw_ext_elenco_stock.setitem(ll_row[1], "prog_stock", istr_lista_stock[ll_i].prog_stock)
		dw_ext_elenco_stock.setitem(ll_row[1], "quan_disponibile", istr_lista_stock[ll_i].quan_disponibile)
//		dw_ext_elenco_stock.setitem(ll_row[1], "quantita", 0)
		dw_ext_elenco_stock.setitem(ll_row[1], "quantita", istr_lista_stock[ll_i].quantita)
	end if
next

dw_ext_elenco_stock.SetTabOrder ( "quantita",10 )
//if dw_ext_elenco_stock.rowcount() > 0 then
//	ll_row[1] = 1
//	dw_ext_elenco_stock.set_selected_rows(1, &
//													  ll_row[], &
//													  c_ignorechanges, &
//													  c_refreshchildren, &
//													  c_refreshsame)
//end if
return dw_ext_lista_mp.rowcount()
end function

public function integer wf_carica_str_lista_stock (string fs_cod_prodotto, string fs_cod_deposito, ref double fdd_quan_utilizzo);boolean lb_magazzino_negativo=false
string ls_cod_ubicazione, ls_cod_lotto, ls_mag_neg
datetime ldt_data_stock
long ll_prog_stock, ll_i
double ldd_quan_disponibile, ldd_quan_residuo
boolean lb_trovato

select flag
into	:ls_mag_neg
from	parametri_azienda
where	cod_azienda = :s_cs_xx.cod_azienda and
		cod_parametro = 'CMN';
		
if sqlca.sqlcode = 0 and not isnull(ls_mag_neg) and ls_mag_neg = "S" then
	//ls_mag_neg = "OK!"
	lb_magazzino_negativo = true
//else
	//ls_mag_neg = "KO"
end if

ldd_quan_residuo = fdd_quan_utilizzo


//and
//		( giacenza_stock > quan_assegnata + quan_in_spedizione or :ls_mag_neg = 'OK!' )

declare cur_stock cursor for
select cod_ubicazione,
		cod_lotto,
		data_stock,
		prog_stock,
		giacenza_stock - quan_assegnata - quan_in_spedizione
 from stock
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :fs_cod_prodotto and
		cod_deposito = :fs_cod_deposito 
order by data_stock ASC;

open cur_stock;

fetch cur_stock into
		:ls_cod_ubicazione,   
		:ls_cod_lotto,   
		:ldt_data_stock,   
		:ll_prog_stock,
		:ldd_quan_disponibile;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep", "Errore Durante lettura Stock" + sqlca.sqlerrtext,stopsign!)
	close cur_stock;
	return -1
end if

do while true
	
	if sqlca.sqlcode = 100 then
		// sono arrivato all'ultimo lotto; controllo se c'è del residuo
		if ldd_quan_residuo > 0 and lb_magazzino_negativo then
			// prelevo la quantità dall'ultimo lotto (quello più recente) anche se va in negativo
			istr_lista_stock[ll_i].quantita += ldd_quan_residuo
		end if
		exit
	end if
	
	lb_trovato = false
	
	for ll_i = 1 to upperbound(istr_lista_stock)
		
		if istr_lista_stock[ll_i].cod_prodotto = fs_cod_prodotto and &
			istr_lista_stock[ll_i].cod_deposito = fs_cod_deposito and &
			istr_lista_stock[ll_i].cod_ubicazione = ls_cod_ubicazione and &
			istr_lista_stock[ll_i].cod_lotto = ls_cod_lotto and &
			istr_lista_stock[ll_i].data_stock = ldt_data_stock and &
			istr_lista_stock[ll_i].prog_stock = ll_prog_stock then
			
			lb_trovato = true
			
			exit
			
		end if
		
	next
	
	if not lb_trovato then
		ll_i = UpperBound(istr_lista_stock) + 1
		istr_lista_stock[ll_i].cod_prodotto = fs_cod_prodotto
		istr_lista_stock[ll_i].cod_deposito = fs_cod_deposito
		istr_lista_stock[ll_i].cod_ubicazione = ls_cod_ubicazione
		istr_lista_stock[ll_i].cod_lotto = ls_cod_lotto
		istr_lista_stock[ll_i].data_stock = ldt_data_stock
		istr_lista_stock[ll_i].prog_stock = ll_prog_stock
	end if
	
	istr_lista_stock[ll_i].quan_disponibile = ldd_quan_disponibile
	
	if ldd_quan_residuo > 0 then
	
		if ldd_quan_disponibile >= ldd_quan_residuo then
			// la disponibilità del lotto copre il fabbisogno
			istr_lista_stock[ll_i].quantita += ldd_quan_residuo
			ldd_quan_residuo = 0

		else

			istr_lista_stock[ll_i].quantita += ldd_quan_disponibile
			ldd_quan_residuo = ldd_quan_residuo - ldd_quan_disponibile
			ldd_quan_residuo = round(ldd_quan_residuo, 4)

			// *** Michela 09/10/2007: se il magazzino è in negativo allora la quantita utilizzo non cambia
			
//			if ldd_quan_disponibile < 0 then
//				istr_lista_stock[ll_i].quantita = ldd_quan_residuo
//				ldd_quan_residuo = round(ldd_quan_residuo,4)
//			else
//				istr_lista_stock[ll_i].quantita = ldd_quan_disponibile
//				ldd_quan_residuo = ldd_quan_residuo - ldd_quan_disponibile
//				ldd_quan_residuo = round(ldd_quan_residuo,4)
//			end if

			// *** fine modifica
		end if

	else
		
		istr_lista_stock[ll_i].quantita = 0

	end if
	
	
	fetch cur_stock into
			:ls_cod_ubicazione,   
			:ls_cod_lotto,   
			:ldt_data_stock,   
			:ll_prog_stock,
			:ldd_quan_disponibile;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep", "Errore Durante lettura Stock" + sqlca.sqlerrtext,stopsign!)
		close cur_stock;
		return -1
	end if

loop
close cur_stock;

return UpperBound(istr_lista_stock)
end function

event pc_setwindow;call super::pc_setwindow;double ldd_quantita, ldd_quan_in_ordine, ldd_quan_prodotta, ldd_quan_assegnata, ldd_quan_in_produzione

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_ext_lista_mp.set_dw_options(sqlca, pcca.null_object, c_nonew + c_nomodify + c_nodelete + c_disableCC, &
										 c_noresizedw + c_cursorrowpointer)

if s_cs_xx.parametri.parametro_i_1 > 0 then
	ii_anno_commessa = s_cs_xx.parametri.parametro_i_1
end if

if s_cs_xx.parametri.parametro_ul_1 > 0 then
	il_num_commessa = s_cs_xx.parametri.parametro_ul_1
end if

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_ul_1)

select quan_ordine,
		quan_prodotta,
		quan_assegnata,
		quan_in_produzione
 into :ldd_quan_in_ordine,   
		:ldd_quan_prodotta,   
		:ldd_quan_assegnata,   
		:ldd_quan_in_produzione
from  anag_commesse
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_commessa = :ii_anno_commessa and
		num_commessa = :il_num_commessa;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Chiusura Automatica Commessa","Errore durante l'Estrazione Dati Commessa",stopsign!)
	close(this)
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("Chiusura Automatica Commessa","Commessa non Trovata in Anagrafica Commesse",stopsign!)
	close(this)
end if	

ldd_quantita = ldd_quan_in_ordine - ldd_quan_assegnata - ldd_quan_in_produzione - ldd_quan_prodotta

if ldd_quantita > 0 then
	em_quan_prodotta.text = string(ldd_quantita)
else
	em_quan_prodotta.text = "0"
end if

end event

on w_lista_stock_mp_commesse.create
int iCurrent
call super::create
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.em_tot=create em_tot
this.em_ru=create em_ru
this.em_mp=create em_mp
this.cb_1=create cb_1
this.dw_ext_lista_mp=create dw_ext_lista_mp
this.cb_esegui=create cb_esegui
this.em_quan_prodotta=create em_quan_prodotta
this.st_1=create st_1
this.cbx_flag_chiudi_commessa=create cbx_flag_chiudi_commessa
this.cb_annulla=create cb_annulla
this.dw_ext_elenco_stock=create dw_ext_elenco_stock
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_4
this.Control[iCurrent+2]=this.st_3
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.em_tot
this.Control[iCurrent+5]=this.em_ru
this.Control[iCurrent+6]=this.em_mp
this.Control[iCurrent+7]=this.cb_1
this.Control[iCurrent+8]=this.dw_ext_lista_mp
this.Control[iCurrent+9]=this.cb_esegui
this.Control[iCurrent+10]=this.em_quan_prodotta
this.Control[iCurrent+11]=this.st_1
this.Control[iCurrent+12]=this.cbx_flag_chiudi_commessa
this.Control[iCurrent+13]=this.cb_annulla
this.Control[iCurrent+14]=this.dw_ext_elenco_stock
end on

on w_lista_stock_mp_commesse.destroy
call super::destroy
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.em_tot)
destroy(this.em_ru)
destroy(this.em_mp)
destroy(this.cb_1)
destroy(this.dw_ext_lista_mp)
destroy(this.cb_esegui)
destroy(this.em_quan_prodotta)
destroy(this.st_1)
destroy(this.cbx_flag_chiudi_commessa)
destroy(this.cb_annulla)
destroy(this.dw_ext_elenco_stock)
end on

type st_4 from statictext within w_lista_stock_mp_commesse
integer x = 891
integer y = 2100
integer width = 347
integer height = 56
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Totale"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_3 from statictext within w_lista_stock_mp_commesse
integer x = 457
integer y = 2100
integer width = 347
integer height = 56
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Risrose Umane"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_lista_stock_mp_commesse
integer x = 23
integer y = 2100
integer width = 347
integer height = 56
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Materie Prime"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_tot from editmask within w_lista_stock_mp_commesse
integer x = 891
integer y = 2160
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
string text = "none"
alignment alignment = center!
string mask = "###,##0.0000"
end type

type em_ru from editmask within w_lista_stock_mp_commesse
integer x = 457
integer y = 2160
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
string text = "none"
alignment alignment = center!
string mask = "###,##0.0000"
end type

type em_mp from editmask within w_lista_stock_mp_commesse
integer x = 23
integer y = 2160
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
string text = "none"
alignment alignment = center!
string mask = "###,##0.0000"
end type

type cb_1 from commandbutton within w_lista_stock_mp_commesse
integer x = 23
integer y = 2020
integer width = 411
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Calcola Costo"
end type

event clicked;string ls_errore
long   li_risposta
dec{4} ld_costo_mp,ld_costo_lav,ld_costo_tot, ldd_quantita_prodotta_pf
uo_costificazione_pf luo_costo


em_mp.text = ""
em_ru.text = ""
em_tot.text = ""

luo_costo = create uo_costificazione_pf

ldd_quantita_prodotta_pf = dec(em_quan_prodotta.text)

li_risposta = luo_costo.uof_costificazione_pf( sqlca, s_cs_xx.cod_azienda, ii_anno_commessa, il_num_commessa, ldd_quantita_prodotta_pf, ref ld_costo_mp, ref ld_costo_lav, ref ld_costo_tot, ref ls_errore)

destroy luo_costo

if li_risposta < 0 then
	g_mb.error(g_str.format("Errore in costificazione prodotto finito: $1",ls_errore))
	return -1
end if

em_mp.text = string(ld_costo_mp)
em_ru.text = string(ld_costo_lav)
em_tot.text = string(ld_costo_tot)

end event

type dw_ext_lista_mp from uo_cs_xx_dw within w_lista_stock_mp_commesse
integer x = 23
integer y = 128
integer width = 3383
integer height = 780
integer taborder = 50
string dataobject = "d_ext_lista_mp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;wf_crea_lista_mp()
wf_estrai_stock(getitemstring(1, "cod_prodotto"), getitemstring(1, "cod_deposito"))
end event

event rowfocuschanged;call super::rowfocuschanged;if currentrow > 0 then
	wf_estrai_stock(this.getitemstring(currentrow, "cod_prodotto"), this.getitemstring(currentrow, "cod_deposito"))
end if
end event

type cb_esegui from commandbutton within w_lista_stock_mp_commesse
integer x = 3031
integer y = 20
integer width = 361
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;double   ldd_quan_prelevata, ldd_quantita_prodotta_pf, ldd_quan_tecnica, ldd_quan_utilizzo, &
			ldd_dim_x, ldd_dim_y, ldd_dim_z, ldd_dim_t, ldd_coef_calcolo, ldd_quan_ordine, ldd_quan_prodotta, &
			ldd_quan_assegnata, ldd_quan_in_produzione,ldd_quan_impegnata_attuale
long		ll_prog_stock[],ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_anno_registrazione[],ll_num_registrazione[], &
			ll_i, ll_num_sequenza,ll_progressivo, ll_anno_registrazione_rag[], ll_num_registrazione_rag[]
string   ls_cod_tipo_commessa,ls_cod_tipo_mov_prel_mat_prime,ls_cod_deposito[], ls_cod_prodotto_finito, & 
		   ls_cod_ubicazione[],ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[], ls_cod_deposito_versamento, ls_cod_versione_figlio, ls_cod_versione_variante, & 
			ls_cod_tipo_mov_ver_prod_finiti, ls_cod_prodotto, ls_flag_tipo_avanzamento, ls_cod_prodotto_padre, &
			ls_cod_prodotto_figlio, ls_cod_versione, ls_cod_prodotto_variante, ls_flag_esclusione, ls_formula_tempo, &
			ls_cod_ubicazione_test, ls_cod_lotto_test, ls_flag_cliente, ls_flag_fornitore, & 
			ls_flag_materia_prima,ls_des_estesa,ls_lotto,ls_ubicazione,ls_cod_lotto_pf,ls_cod_ubicazione_pf,ls_anno_commessa, &
			ls_cod_prodotto_raggruppato,  ls_mag_neg,ls_flag_calcola_costo_pf, ls_errore
integer  li_risposta
datetime ldt_data_stock[],ldt_oggi

uo_magazzino luo_mag

select flag
into	:ls_mag_neg
from	parametri_azienda
where	cod_azienda = :s_cs_xx.cod_azienda and
		cod_parametro = 'CMN';
		
if sqlca.sqlcode = 0 and not isnull(ls_mag_neg) and ls_mag_neg = "S" then
else
	ls_mag_neg = "N"
	for ll_i = 1 to dw_ext_lista_mp.rowcount()
		if dw_ext_lista_mp.getitemnumber(ll_i, "quan_residua") > 0 then
			g_mb.messagebox("SEP", "Mancano ancora i prelievi di alcune materie prime, quindi non è possibile procedere. Controllare che tutte le materie prime abbiano una quantità residua = 0.")
			return
		end if
	next	
end if

ls_anno_commessa=string(ii_anno_commessa)

setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

setnull(ls_cod_ubicazione_pf)
setnull(ls_cod_lotto_pf)

ldt_oggi = datetime(today())
ldd_quantita_prodotta_pf = double(em_quan_prodotta.text)

select cod_tipo_commessa,
		 cod_prodotto,
		 quan_ordine,
		 quan_prodotta,
		 quan_assegnata,
		 quan_in_produzione,
		 lotto,
		 ubicazione
into   :ls_cod_tipo_commessa,
		 :ls_cod_prodotto_finito,
		 :ldd_quan_ordine,
		 :ldd_quan_prodotta,
		 :ldd_quan_assegnata,
		 :ldd_quan_in_produzione,
		 :ls_lotto,
		 :ls_ubicazione
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:ii_anno_commessa
and    num_commessa=:il_num_commessa;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
	return
end if
	
select cod_tipo_mov_prel_mat_prime,
		 cod_tipo_mov_ver_prod_finiti,
		 cod_deposito_versamento,
		 flag_calcola_costo_pf
into   :ls_cod_tipo_mov_prel_mat_prime,
		 :ls_cod_tipo_mov_ver_prod_finiti,
		 :ls_cod_deposito_versamento,
		 :ls_flag_calcola_costo_pf
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in ricerca tipo commessa: " + sqlca.sqlerrtext,stopsign!)
	return
end if

// (inizio) TEST DET_TIPI_MOVIMENTI ***********************************************************

declare righe_det_tipi_m cursor for
select  cod_ubicazione,
		  cod_lotto,
		  flag_cliente,
		  flag_fornitore
from    det_tipi_movimenti
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_movimento=:ls_cod_tipo_mov_ver_prod_finiti;

open righe_det_tipi_m;

do while 1=1
	fetch righe_det_tipi_m
	into  :ls_cod_ubicazione_test,
			:ls_cod_lotto_test,
			:ls_flag_cliente,
			:ls_flag_fornitore;

	if sqlca.sqlcode = 100 then exit
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Si e' verificato un errore in lettura det_tipi_movimenti: " + sqlca.sqlerrtext,stopsign!)
		close righe_det_tipi_m;
		return 
	end if

	if isnull(ls_ubicazione) or ls_ubicazione="" then
		if isnull(ls_cod_ubicazione_test) then
			g_mb.messagebox("Sep","Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_prod_finiti + &
						  " come versamento di prodotto finito per commesse, poiche' il codice ubicazione non e' stato indicato." , &
						  stopsign!)
			close righe_det_tipi_m;
			return 
		end if

	else
		ls_cod_ubicazione_pf = ls_ubicazione
		
	end if
	
	if isnull(ls_lotto) or ls_lotto="" then
		
		if isnull(ls_cod_lotto_test) then
			g_mb.messagebox("Sep","Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_prod_finiti + &
						  " come versamento di prodotto finito per commesse, poiche' il codice lotto non e' stato indicato.", &
						  stopsign!)
			close righe_det_tipi_m;
			return 
		end if

		
	else
		ls_cod_lotto_pf = ls_lotto
	end if
	
	if ls_flag_cliente='S' then
		g_mb.messagebox("Sep","Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_prod_finiti + " come versamento di prodotto finito per commesse, poiche' viene richiesto il codice cliente." ,stopsign!)
		close righe_det_tipi_m;
		return 
	end if

	if ls_flag_fornitore='S' then
		g_mb.messagebox("Sep","Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_prod_finiti + " come versamento di prodotto finito per commesse, poiche' viene richiesto il codice fornitore." ,stopsign!)
		close righe_det_tipi_m;
		return 
	end if

loop

close righe_det_tipi_m;

// (fine) TEST DET_TIPI_MOVIMENTI 

// MOVIMENTI MAGAZZINO MATERIE PRIME (inizio)
for ll_i = 1 to UpperBound(istr_lista_stock)
	
	if istr_lista_stock[ll_i].quantita > 0 then
		
		ls_cod_prodotto = istr_lista_stock[ll_i].cod_prodotto
		ls_cod_deposito[1] = istr_lista_stock[ll_i].cod_deposito
		ls_cod_ubicazione[1] = istr_lista_stock[ll_i].cod_ubicazione
		ls_cod_lotto[1] = istr_lista_stock[ll_i].cod_lotto
		ldt_data_stock[1] = istr_lista_stock[ll_i].data_stock
		ll_prog_stock[1] = istr_lista_stock[ll_i].prog_stock
		ldd_quan_prelevata = istr_lista_stock[ll_i].quantita
		
		// enme 08/1/2006 gestione prodotto raggruppato
		
		
		if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_prel_mat_prime, ls_cod_prodotto, ls_cod_deposito[], & 
											  ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
											  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
											  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
		
		  g_mb.messagebox("Sep","Si è verificato un errore in creazione destinazione movimenti.",stopsign!)
		  rollback;
		  return 
		end if
	
		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
										 ls_cod_tipo_mov_prel_mat_prime, ls_cod_prodotto) = -1 then
		
		  g_mb.messagebox("Sep","Si è verificato un errore in fase di verifica destinazioni movimenti magazzino.",stopsign!)
		  rollback;
		  return 
		end if
		
		luo_mag = create uo_magazzino
	
		li_risposta = luo_mag.uof_movimenti_mag(ldt_oggi, &
											  ls_cod_tipo_mov_prel_mat_prime, &
											  "N", &
											  ls_cod_prodotto, &
											  ldd_quan_prelevata, &
											  0, &
											  il_num_commessa, &
											  ldt_oggi, &
											  ls_anno_commessa, &
											  ll_anno_reg_des_mov, &
											  ll_num_reg_des_mov, &
											  ls_cod_deposito[], &
											  ls_cod_ubicazione[], &
											  ls_cod_lotto[], &
											  ldt_data_stock[], &
											  ll_prog_stock[], &
											  ls_cod_fornitore[], &
											  ls_cod_cliente[], &
											  ll_anno_registrazione[], &
											  ll_num_registrazione[])
											  
		if li_risposta=-1 then
			g_mb.messagebox("Sep","Errore su movimenti magazzino, prodotto: " + ls_cod_prodotto &
						 + ", deposito:" + ls_cod_deposito[1] + ", ubicazione:" + ls_cod_ubicazione[1] &
						 + ", cod_lotto:" + ls_cod_lotto[1] + ", data_stock:" + string(ldt_data_stock[1]) &
						 + ", prog_stock:" + string(ll_prog_stock[1]),stopsign!)
		   rollback;
			return
		end if
		
		f_elimina_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov)
		
		
		// stefanop 06/12/2011: movimento anche il prodotto raggruppato
		if luo_mag.uof_movimenti_mag_ragguppato(ldt_oggi, &
											  ls_cod_tipo_mov_prel_mat_prime, &
											  "N", &
											  ls_cod_prodotto, &
											  ldd_quan_prelevata, &
											  0, &
											  il_num_commessa, &
											  ldt_oggi, &
											  ls_anno_commessa, &
											  ll_anno_reg_des_mov, &
											  ll_num_reg_des_mov, &
											  ls_cod_deposito[], &
											  ls_cod_ubicazione[], &
											  ls_cod_lotto[], &
											  ldt_data_stock[], &
											  ll_prog_stock[], &
											  ls_cod_fornitore[], &
											  ls_cod_cliente[], &
											  ll_anno_registrazione_rag[], &
											  ll_num_registrazione_rag[],&
											  long(ls_anno_commessa),&
											  ll_anno_registrazione[1],&
											  ll_num_registrazione[1]) = -1 then
			destroy luo_mag
			return -1
		end if					
		// ----
		
		
		destroy luo_mag


//****************************************************************************************************************
// DISIMPEGNO LE MP CONTROLLANDO CONTEMPORANEAMENTE ANCHE LA TABELLA IMPEGNO_MAT_PRIME_COMMESSA

		select quan_impegnata_attuale
		into   :ldd_quan_impegnata_attuale
		from   impegno_mat_prime_commessa
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:ii_anno_commessa
		and    num_commessa=:il_num_commessa
		and    cod_prodotto=:ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
		if sqlca.sqlcode = 100 then // ROUTINE PER LE COMMESSE GENERATE PRIMA DI AGGIUNGERE LA TAB IMPEGNO_MAT_PRIME COMMESSA
		
			select quan_impegnata
			into   :ldd_quan_impegnata_attuale
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
		
			if ldd_quan_impegnata_attuale >= ldd_quan_prelevata then
				update anag_prodotti																		
				set 	 quan_impegnata = quan_impegnata - :ldd_quan_prelevata
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					return
				end if
				
			else
				update anag_prodotti																		
				set 	 quan_impegnata = 0
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					return
				end if
				
			end if
			
		else // ROUTINE PER LE COMMESSE GENERATE DOPO L'AGGIUNTA DELLA TAB IMPEGNO_MAT_PRIME COMMESSA
	
			if ldd_quan_impegnata_attuale >= ldd_quan_prelevata then
				
				update impegno_mat_prime_commessa
				set 	 quan_impegnata_attuale= quan_impegnata_attuale - :ldd_quan_prelevata
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_commessa=:ii_anno_commessa
				and    num_commessa=:il_num_commessa
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					return
				end if
		
				update anag_prodotti																		
				set 	 quan_impegnata = quan_impegnata - :ldd_quan_prelevata
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					return
				end if
				
			else
				update impegno_mat_prime_commessa
				set 	 quan_impegnata_attuale = 0
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_commessa=:ii_anno_commessa
				and    num_commessa=:il_num_commessa
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					return
				end if
		
				update anag_prodotti																		
				set 	 quan_impegnata = quan_impegnata - :ldd_quan_impegnata_attuale
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					return
				end if
						
			
			end if
	
		end if
//****************************************************************************************************************		
	end if
next
// MOVIMENTI MAGAZZINO MATERIE PRIME (fine)

//***************** INIZIO CALCOLO DEL COSTO DEL PRODOTTO FINITO
if ls_flag_calcola_costo_pf ='S' then
	dec{4} ld_costo_mp,ld_costo_lav,ld_costo_tot
	uo_costificazione_pf luo_costo
	luo_costo = create uo_costificazione_pf
	
	luo_costo.uof_costificazione_pf( sqlca, s_cs_xx.cod_azienda, ii_anno_commessa, il_num_commessa, ldd_quantita_prodotta_pf, ref ld_costo_mp, ref ld_costo_lav, ref ld_costo_tot, ref ls_errore)

	// uso il costo preventivo perchè in genere con il configuratore si genera una distinta abbastanza verosimile.
	/* per usare il calcoilo sul consuntivo bisognerebbe usare la rilevazione della produzione.
	li_risposta = luo_costo.uof_costificazione_pf(sqlca,&
																 s_cs_xx.cod_azienda,&
																 ls_cod_prodotto_finito,&
																 ls_cod_versione,&
																 ldd_quantita_prodotta_pf,&
																 ld_costo_mp,&
																 ld_costo_lav,&
																 ld_costo_tot,&
															 ls_errore) */
	destroy luo_costo
	
	if li_risposta < 0 then
		g_mb.error(g_str.format("Errore in costificazione prodotto finito: $1",ls_errore))
		return -1
	end if
else
	ld_costo_mp = 0
	ld_costo_lav = 0
	ld_costo_tot = 0
end if

//***************** FINE CALCOLO DEL COSTO DEL PRODOTTO FINITO


// MOVIMENTI MAGAZZINO PRODOTTO FINITO (inizio)
ls_cod_deposito[1] = ls_cod_deposito_versamento
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

if not isnull(ls_cod_ubicazione_pf) then
	ls_cod_ubicazione[1] = ls_cod_ubicazione_pf
end if

if not isnull(ls_cod_lotto_pf) then
	ls_cod_lotto[1] = ls_cod_lotto_pf
end if

if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_ver_prod_finiti, ls_cod_prodotto_finito, ls_cod_deposito[], & 
									  ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
									  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
									  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then

  g_mb.messagebox("Sep","Si è verificato un errore in creazione destinazione movimenti.",stopsign!)
  rollback;
  return 
end if

if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
								 ls_cod_tipo_mov_ver_prod_finiti, ls_cod_prodotto_finito) = -1 then

  g_mb.messagebox("Sep","Si è verificato un errore in fase di verifica destinazioni movimenti magazzino.",stopsign!)
  rollback;
  return 
end if

luo_mag = create uo_magazzino

li_risposta = luo_mag.uof_movimenti_mag(ldt_oggi, &
									  ls_cod_tipo_mov_ver_prod_finiti, &
									  "N", &
									  ls_cod_prodotto_finito, &
									  ldd_quantita_prodotta_pf , &
									  round(ld_costo_tot / ldd_quantita_prodotta_pf,4), &
									  il_num_commessa, &
									  ldt_oggi, &
									  ls_anno_commessa, &
									  ll_anno_reg_des_mov, &
									  ll_num_reg_des_mov, &
									  ls_cod_deposito[], &
									  ls_cod_ubicazione[], &
									  ls_cod_lotto[], &
									  ldt_data_stock[], &
									  ll_prog_stock[], &
									  ls_cod_fornitore[], &
									  ls_cod_cliente[], &
									  ll_anno_registrazione[], &
									  ll_num_registrazione[])
									  
if li_risposta=-1 then
	g_mb.messagebox("Sep","Errore su movimenti magazzino, prodotto:" + ls_cod_prodotto &
				 + ", deposito:" + ls_cod_deposito[1] + ", ubicazione:" + ls_cod_ubicazione[1] &
				 + ", cod_lotto:" + ls_cod_lotto[1] + ", data_stock:" + string(ldt_data_stock[1]) &
				 + ", prog_stock:" + string(ll_prog_stock[1]),stopsign!)
	rollback;
	return
end if

f_elimina_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov)

// stefanop 06/12/2011: movimento anche il prodotto raggruppato
if luo_mag.uof_movimenti_mag_ragguppato(ldt_oggi, &
									  ls_cod_tipo_mov_ver_prod_finiti, &
									  "N", &
									  ls_cod_prodotto_finito, &
									  ldd_quantita_prodotta_pf, &
									  0, &
									  il_num_commessa, &
									  ldt_oggi, &
									  ls_anno_commessa, &
									  ll_anno_reg_des_mov, &
									  ll_num_reg_des_mov, &
									  ls_cod_deposito[], &
									  ls_cod_ubicazione[], &
									  ls_cod_lotto[], &
									  ldt_data_stock[], &
									  ll_prog_stock[], &
									  ls_cod_fornitore[], &
									  ls_cod_cliente[], &
									  ll_anno_registrazione_rag[], &
									  ll_num_registrazione_rag[],&
									  long(ls_anno_commessa),&
									  ll_anno_registrazione[1],&
									  ll_num_registrazione[1]) = -1 then
	destroy luo_mag
	return -1
end if					
// ----

destroy luo_mag

// MOVIMENTI MAGAZZINO PRODOTTO FINITO (fine)


if cbx_flag_chiudi_commessa.checked then
	ls_flag_tipo_avanzamento = "7" // Chiusa da Chiusura Automatica
else
	ls_flag_tipo_avanzamento = "3" // Aperta da Chiusura Automatica
end if

update anag_commesse
set	 quan_prodotta = quan_prodotta + :ldd_quantita_prodotta_pf,
		 flag_tipo_avanzamento = :ls_flag_tipo_avanzamento,
		 data_chiusura = :ldt_oggi
where	 cod_azienda = :s_cs_xx.cod_azienda  
and	 anno_commessa = :ii_anno_commessa
and	 num_commessa = :il_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if

// dopo il carico del prodotto finito a magazzino aggiorno la tabella varianti_stock


for ll_i = 1 to upperbound(ls_cod_deposito)
	declare righe_var_com cursor for
	select cod_prodotto_padre,
			 num_sequenza,
			 cod_prodotto_figlio,
			 cod_versione_figlio,
			 cod_versione,
			 cod_prodotto,
			 cod_versione_variante,
			 quan_tecnica,
			 quan_utilizzo,
			 dim_x,
			 dim_y,
			 dim_z,
			 dim_t,
			 coef_calcolo,
			 flag_esclusione,
			 formula_tempo,
			 des_estesa,
			 flag_materia_prima
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :ii_anno_commessa  and    
			 num_commessa = :il_num_commessa;
	
	open righe_var_com;
	
	do while 1=1
		fetch righe_var_com
		into  :ls_cod_prodotto_padre,
				:ll_num_sequenza,
				:ls_cod_prodotto_figlio,
				:ls_cod_versione_figlio,
				:ls_cod_versione,
				:ls_cod_prodotto_variante,
				:ls_cod_versione_variante,
				:ldd_quan_tecnica,
				:ldd_quan_utilizzo,
				:ldd_dim_x,
				:ldd_dim_y,
				:ldd_dim_z,
				:ldd_dim_t,
				:ldd_coef_calcolo,
				:ls_flag_esclusione,
				:ls_formula_tempo,
				:ls_des_estesa,
				:ls_flag_materia_prima;
		
		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Si è verificato un errore in fase di aggiornamento tabella varianti stock:" + sqlca.sqlerrtext,stopsign!)
			close righe_var_com;
			rollback;
			return			
		end if

		select max(progressivo)
		into   :ll_progressivo
		from   varianti_stock
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 cod_prodotto = :ls_cod_prodotto_finito and    
				 cod_deposito = :ls_cod_deposito[ll_i]  and    
				 cod_ubicazione = :ls_cod_ubicazione[ll_i] and    
				 cod_lotto = :ls_cod_lotto[ll_i] 		and    
				 data_stock = :ldt_data_stock[ll_i]		and    
				 prog_stock = :ll_prog_stock[ll_i]		and    
				 cod_prodotto_padre = :ls_cod_prodotto_padre and  	 
				 num_sequenza = :ll_num_sequenza       and    
				 cod_prodotto_figlio = :ls_cod_prodotto_figlio and
				 cod_versione_figlio = :ls_cod_versione_figlio and    
				 cod_versione = :ls_cod_versione;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Si è verificato un errore in fase di aggiornamento tabella varianti stock:" + sqlca.sqlerrtext,stopsign!)
			close righe_var_com;
			rollback;
			return			
		end if
		
		if isnull(ll_progressivo) or ll_progressivo=0 then
			ll_progressivo = 1
		else
			ll_progressivo++
		end if
		
	   INSERT INTO varianti_stock  
         ( cod_azienda,   
           cod_prodotto,   
           cod_deposito,   
           cod_ubicazione,   
           cod_lotto,   
           data_stock,   
           prog_stock,   
           cod_prodotto_padre,   
           num_sequenza,   
           cod_prodotto_figlio,   
			  cod_versione_figlio,
           cod_versione, 
			  progressivo,
           cod_prodotto_variante,   
			  cod_versione_variante,
           quan_tecnica,   
           quan_utilizzo,   
           dim_x,   
           dim_y,   
           dim_z,   
           dim_t,   
           coef_calcolo,   
           flag_esclusione,   
           formula_tempo,
			  des_estesa,
			  flag_materia_prima)  
  		VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_prodotto_finito,   
           :ls_cod_deposito[ll_i],   
           :ls_cod_ubicazione[ll_i],   
           :ls_cod_lotto[ll_i],   
           :ldt_data_stock[ll_i],   
           :ll_prog_stock[ll_i],   
           :ls_cod_prodotto_padre,   
           :ll_num_sequenza,   
           :ls_cod_prodotto_figlio, 
			  :ls_cod_versione_figlio,
           :ls_cod_versione,
			  :ll_progressivo,
           :ls_cod_prodotto_variante,   
			  :ls_cod_versione_variante,
           :ldd_quan_tecnica,   
           :ldd_quan_utilizzo,   
           :ldd_dim_x,   
           :ldd_dim_y,   
           :ldd_dim_z,   
           :ldd_dim_t,   
           :ldd_coef_calcolo,   
           :ls_flag_esclusione,   
           :ls_formula_tempo,
			  :ls_des_estesa,
			  :ls_flag_materia_prima)  ;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Si è verificato un errore in fase di aggiornamento tabella varianti stock:" + sqlca.sqlerrtext,stopsign!)
			close righe_var_com;
			rollback;
			return			
		end if
	loop
	close righe_var_com;
next

// fine aggiornamento tabella varianti_stock	

commit; //consolida sul DB la transazione

close(parent)
end event

type em_quan_prodotta from editmask within w_lista_stock_mp_commesse
integer x = 562
integer y = 28
integer width = 571
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = right!
string mask = "##,###,###.0000"
string displaydata = ""
end type

event modified;wf_crea_lista_mp()
wf_estrai_stock(dw_ext_lista_mp.getitemstring(1, "cod_prodotto"), dw_ext_lista_mp.getitemstring(1, "cod_deposito"))
end event

type st_1 from statictext within w_lista_stock_mp_commesse
integer x = 37
integer y = 28
integer width = 512
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Q.tà Prodotta (P.F.):"
boolean focusrectangle = false
end type

type cbx_flag_chiudi_commessa from checkbox within w_lista_stock_mp_commesse
integer x = 1179
integer y = 28
integer width = 553
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Chiudi Commessa"
boolean lefttext = true
end type

type cb_annulla from commandbutton within w_lista_stock_mp_commesse
integer x = 3035
integer y = 1912
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;close(parent)
end event

type dw_ext_elenco_stock from datawindow within w_lista_stock_mp_commesse
integer x = 23
integer y = 932
integer width = 3383
integer height = 960
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_ext_elenco_stock"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;double ldd_quantita_old, ldd_quan_residua
long ll_i

		ldd_quantita_old = this.getitemnumber(row, "quantita")
		ldd_quan_residua = dw_ext_lista_mp.getitemnumber(dw_ext_lista_mp.getrow(), "quan_residua") + &
										ldd_quantita_old - double(data)
		

		if double(data) > this.getitemnumber(row, "quan_disponibile") then
			g_mb.messagebox("Quantità Scarico X Stock", "Quantità da Scaricare maggiore della Quantità Disponibile nello Stock")
			return 2
//		elseif ldd_quan_residua < 0 then
//			messagebox("Quantità Scarico X Stock", "Quantità da Scaricare maggiore della Quantità Residua")
//			return 2
		else
			dw_ext_lista_mp.setitem(dw_ext_lista_mp.getrow(), "quan_residua", ldd_quan_residua)
											
			for ll_i = 1 to UpperBound(istr_lista_stock)

				if istr_lista_stock[ll_i].cod_prodotto = this.getitemstring(row, "cod_prodotto") and &
					istr_lista_stock[ll_i].cod_deposito = this.getitemstring(row, "cod_deposito") and &
					istr_lista_stock[ll_i].cod_ubicazione = this.getitemstring(row, "cod_ubicazione") and &
					istr_lista_stock[ll_i].cod_lotto = this.getitemstring(row, "cod_lotto") and &
					istr_lista_stock[ll_i].data_stock = this.getitemdatetime(row, "data_stock") and &
					istr_lista_stock[ll_i].prog_stock = this.getitemnumber(row, "prog_stock") then
					
					istr_lista_stock[ll_i].quantita = double(data)
				end if
			next
		end if

end event

