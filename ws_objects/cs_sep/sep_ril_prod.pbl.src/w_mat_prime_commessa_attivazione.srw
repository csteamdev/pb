﻿$PBExportHeader$w_mat_prime_commessa_attivazione.srw
$PBExportComments$Window mat_prime_commessa_attivazione
forward
global type w_mat_prime_commessa_attivazione from w_cs_xx_principale
end type
type dw_mat_prime_commessa_lista from uo_cs_xx_dw within w_mat_prime_commessa_attivazione
end type
type dw_mat_prime_commessa_det from uo_cs_xx_dw within w_mat_prime_commessa_attivazione
end type
type cb_1 from commandbutton within w_mat_prime_commessa_attivazione
end type
end forward

global type w_mat_prime_commessa_attivazione from w_cs_xx_principale
int Width=2003
int Height=2045
boolean TitleBar=true
string Title="Materie Prime Per Commessa (Attivazione)"
dw_mat_prime_commessa_lista dw_mat_prime_commessa_lista
dw_mat_prime_commessa_det dw_mat_prime_commessa_det
cb_1 cb_1
end type
global w_mat_prime_commessa_attivazione w_mat_prime_commessa_attivazione

on w_mat_prime_commessa_attivazione.create
int iCurrent
call w_cs_xx_principale::create
this.dw_mat_prime_commessa_lista=create dw_mat_prime_commessa_lista
this.dw_mat_prime_commessa_det=create dw_mat_prime_commessa_det
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_mat_prime_commessa_lista
this.Control[iCurrent+2]=dw_mat_prime_commessa_det
this.Control[iCurrent+3]=cb_1
end on

on w_mat_prime_commessa_attivazione.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_mat_prime_commessa_lista)
destroy(this.dw_mat_prime_commessa_det)
destroy(this.cb_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_mat_prime_commessa_lista.set_dw_options(sqlca, &
                                    		 i_openparm, &
		                                     c_scrollparent + c_nonew + c_nodelete, &
      		                               c_default)

dw_mat_prime_commessa_det.set_dw_options(sqlca,dw_mat_prime_commessa_lista,c_sharedata + c_scrollparent + c_nonew + c_nodelete,c_default)

iuo_dw_main = dw_mat_prime_commessa_lista
end event

type dw_mat_prime_commessa_lista from uo_cs_xx_dw within w_mat_prime_commessa_attivazione
int X=23
int Y=21
int Width=1921
int Height=781
int TabOrder=10
string DataObject="d_mat_prime_commessa_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa,ll_prog_riga)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_mat_prime_commessa_det from uo_cs_xx_dw within w_mat_prime_commessa_attivazione
int X=23
int Y=821
int Width=1921
int Height=1001
int TabOrder=20
string DataObject="d_mat_prime_commessa_det"
BorderStyle BorderStyle=StyleRaised!
end type

type cb_1 from commandbutton within w_mat_prime_commessa_attivazione
int X=1578
int Y=1841
int Width=366
int Height=81
int TabOrder=3
boolean BringToTop=true
string Text="&Stock"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_mp_com_stock_det_orari,-1,dw_mat_prime_commessa_lista)
end event

