﻿$PBExportHeader$w_mp_com_stock_det_orari.srw
$PBExportComments$Window mp com stock det orari
forward
global type w_mp_com_stock_det_orari from w_cs_xx_principale
end type
type dw_mp_com_stock_de_det from uo_cs_xx_dw within w_mp_com_stock_det_orari
end type
type dw_mp_com_stock_de_lista from uo_cs_xx_dw within w_mp_com_stock_det_orari
end type
end forward

global type w_mp_com_stock_det_orari from w_cs_xx_principale
integer width = 3785
integer height = 1236
string title = "Dettaglio Stock  per Sessione (Attivazione)"
dw_mp_com_stock_de_det dw_mp_com_stock_de_det
dw_mp_com_stock_de_lista dw_mp_com_stock_de_lista
end type
global w_mp_com_stock_det_orari w_mp_com_stock_det_orari

on w_mp_com_stock_det_orari.create
int iCurrent
call super::create
this.dw_mp_com_stock_de_det=create dw_mp_com_stock_de_det
this.dw_mp_com_stock_de_lista=create dw_mp_com_stock_de_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_mp_com_stock_de_det
this.Control[iCurrent+2]=this.dw_mp_com_stock_de_lista
end on

on w_mp_com_stock_det_orari.destroy
call super::destroy
destroy(this.dw_mp_com_stock_de_det)
destroy(this.dw_mp_com_stock_de_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_mp_com_stock_de_lista.set_dw_options(sqlca, &
                                    		   i_openparm, &
		                                       c_scrollparent + c_nonew + c_nodelete, &
      		                                 c_default)


dw_mp_com_stock_de_det.set_dw_options(sqlca, &
                                    		 dw_mp_com_stock_de_lista, &
		                                     c_sharedata + c_scrollparent + c_nonew + c_nodelete, &
      		                               c_default)

iuo_dw_main = dw_mp_com_stock_de_lista
end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_det_orari_produzione_det,"cod_operaio",sqlca,&
//                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_mp_com_stock_de_det from uo_cs_xx_dw within w_mp_com_stock_det_orari
integer x = 23
integer y = 496
integer width = 3703
integer height = 620
integer taborder = 10
string dataobject = "d_mp_com_stock_de_det"
borderstyle borderstyle = styleraised!
end type

type dw_mp_com_stock_de_lista from uo_cs_xx_dw within w_mp_com_stock_det_orari
event pcd_retrieve pbm_custom60
integer x = 23
integer y = 20
integer width = 3703
integer height = 460
string dataobject = "d_mp_com_stock_de_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga,ll_prog_orari
string ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")
ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa,ll_prog_riga,ls_cod_prodotto)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

