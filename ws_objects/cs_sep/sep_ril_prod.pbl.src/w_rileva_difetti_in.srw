﻿$PBExportHeader$w_rileva_difetti_in.srw
$PBExportComments$Window che rileva i difetti materie prime di fase
forward
global type w_rileva_difetti_in from w_cs_xx_risposta
end type
type dw_lista_difetti_in from uo_cs_xx_dw within w_rileva_difetti_in
end type
type dw_det_difetti_in from uo_cs_xx_dw within w_rileva_difetti_in
end type
end forward

global type w_rileva_difetti_in from w_cs_xx_risposta
int Width=3073
int Height=1521
dw_lista_difetti_in dw_lista_difetti_in
dw_det_difetti_in dw_det_difetti_in
end type
global w_rileva_difetti_in w_rileva_difetti_in

on w_rileva_difetti_in.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_lista_difetti_in=create dw_lista_difetti_in
this.dw_det_difetti_in=create dw_det_difetti_in
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_difetti_in
this.Control[iCurrent+2]=dw_det_difetti_in
end on

on w_rileva_difetti_in.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_lista_difetti_in)
destroy(this.dw_det_difetti_in)
end on

type dw_lista_difetti_in from uo_cs_xx_dw within w_rileva_difetti_in
int X=23
int Y=21
int Width=2995
int Height=541
int TabOrder=1
string DataObject="d_lista_difetti_in"
BorderStyle BorderStyle=StyleLowered!
end type

type dw_det_difetti_in from uo_cs_xx_dw within w_rileva_difetti_in
int X=23
int Y=581
int Width=2995
int Height=821
int TabOrder=2
string DataObject="d_det_difetti_in"
BorderStyle BorderStyle=StyleRaised!
end type

