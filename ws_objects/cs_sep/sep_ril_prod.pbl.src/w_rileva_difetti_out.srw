﻿$PBExportHeader$w_rileva_difetti_out.srw
$PBExportComments$Window che rileva i difetti prodotti finiti di  fase
forward
global type w_rileva_difetti_out from w_cs_xx_risposta
end type
type dw_lista_difetti_out from uo_cs_xx_dw within w_rileva_difetti_out
end type
type dw_det_difetti_out from uo_cs_xx_dw within w_rileva_difetti_out
end type
type cb_chiudi from commandbutton within w_rileva_difetti_out
end type
end forward

global type w_rileva_difetti_out from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 2409
integer height = 1456
string title = "Difetti per Prodotto Finito di Fase (Uscita Fase)"
dw_lista_difetti_out dw_lista_difetti_out
dw_det_difetti_out dw_det_difetti_out
cb_chiudi cb_chiudi
end type
global w_rileva_difetti_out w_rileva_difetti_out

on w_rileva_difetti_out.create
int iCurrent
call super::create
this.dw_lista_difetti_out=create dw_lista_difetti_out
this.dw_det_difetti_out=create dw_det_difetti_out
this.cb_chiudi=create cb_chiudi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista_difetti_out
this.Control[iCurrent+2]=this.dw_det_difetti_out
this.Control[iCurrent+3]=this.cb_chiudi
end on

on w_rileva_difetti_out.destroy
call super::destroy
destroy(this.dw_lista_difetti_out)
destroy(this.dw_det_difetti_out)
destroy(this.cb_chiudi)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_enablepopup)

dw_lista_difetti_out.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_default, &
                                       c_default)

dw_det_difetti_out.set_dw_options(sqlca, &
                                  dw_lista_difetti_out, &
											 c_sharedata+c_scrollparent, &
                                  c_default)

//iuo_dw_main = dw_det_difetti_out
end event

event pc_setddlb;call super::pc_setddlb;string ls_cod_prodotto,ls_cod_lavorazione,ls_cod_reparto,ls_cod_cat_attrezzature, ls_cod_versione
integer li_anno_commessa
long ll_num_commessa,ll_prog_riga,ll_prog_orari

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_2
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_3
ls_cod_versione = s_cs_xx.parametri.parametro_s_5

select max(prog_orari)
into   :ll_prog_orari
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa	and    
		 num_commessa = :ll_num_commessa		and    
		 prog_riga = :ll_prog_riga				and    
		 cod_prodotto = :ls_cod_prodotto		and    
		 cod_versione = :ls_cod_versione		and
		 cod_reparto = :ls_cod_reparto		and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 flag_inizio = 'N';

if sqlca.sqlcode < 0 then	
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	
	close(this)
	return
end if

select cod_cat_attrezzature
into   :ls_cod_cat_attrezzature
from   tes_fasi_lavorazione
where  cod_azienda = :s_cs_xx.cod_azienda	and    
		 cod_prodotto = :ls_cod_prodotto		and
		 cod_versione = :ls_cod_versione		and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 cod_reparto = :ls_cod_reparto;

if sqlca.sqlcode < 0 then	
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	close(this)
	return
end if

f_PO_LoadDDDW_DW(dw_det_difetti_out,"cod_errore",sqlca,&
                 "tab_difformita","cod_errore","des_difformita",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and tipo_difformita='P'")

f_PO_LoadDDDW_DW(dw_det_difetti_out,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_attrezzature='" + ls_cod_cat_attrezzature + "'")

end event

event closequery;call super::closequery;LONG   l_Error,ll_anno_commessa,ll_prog_orari,ll_num_commessa,ll_prog_riga
string ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio, ls_cod_versione
double ldd_quan_scarto

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
ll_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_2
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_3
ls_cod_operaio = s_cs_xx.parametri.parametro_s_4
ls_cod_versione = s_cs_xx.parametri.parametro_s_5

select max(prog_orari)
into   :ll_prog_orari
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda   and    
		 anno_commessa = :ll_anno_commessa	  and    
		 num_commessa = :ll_num_commessa		  and    
		 prog_riga = :ll_prog_riga				  and    
		 cod_prodotto = :ls_cod_prodotto		  and    
		 cod_versione = :ls_cod_versione		  and
		 cod_reparto = :ls_cod_reparto		  and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 flag_inizio = 'N';

select sum(quan_scarto)
into   :ldd_quan_scarto
from   difetti_det_orari_out
where  cod_azienda = :s_cs_xx.cod_azienda  and    
       anno_commessa = :ll_anno_commessa	 and    
		 num_commessa = :ll_num_commessa		 and    
		 prog_riga = :ll_prog_riga				 and    
		 cod_prodotto = :ls_cod_prodotto     and    
		 cod_versione = :ls_cod_versione		 and
		 cod_reparto = :ls_cod_reparto		 and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 prog_orari = :ll_prog_orari;

if isnull(ldd_quan_scarto) then ldd_quan_scarto = 0
s_cs_xx.parametri.parametro_d_2 = ldd_quan_scarto
end event

type dw_lista_difetti_out from uo_cs_xx_dw within w_rileva_difetti_out
integer x = 23
integer y = 20
integer width = 2331
integer height = 540
integer taborder = 10
string dataobject = "d_lista_difetti_out"
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_prog_orari,ll_num_commessa,ll_prog_riga
string ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio, ls_cod_versione

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
ll_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_2
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_3
ls_cod_operaio = s_cs_xx.parametri.parametro_s_4
ls_cod_versione = s_cs_xx.parametri.parametro_s_5

select max(prog_orari)
into   :ll_prog_orari
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda  	and    
       anno_commessa = :ll_anno_commessa		and    
		 num_commessa = :ll_num_commessa			and    
		 prog_riga = :ll_prog_riga					and    
		 cod_prodotto = :ls_cod_prodotto			and
		 cod_versione = :ls_cod_versione			and    
		 cod_reparto = :ls_cod_reparto			and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 flag_inizio = 'N';

l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 ll_anno_commessa, 	 &
						 ls_cod_prodotto, & 
						 ls_cod_versione, &
						 ll_num_commessa, &
						 ls_cod_reparto,  &
						 ll_prog_riga,    &
						 ls_cod_lavorazione, & 
						 ls_cod_operaio,	&
						 ll_prog_orari)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF



end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,ll_anno_commessa,ll_num_commessa,ll_prog_riga,ll_prog_orari,ll_prog_difetto
string ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_operaio, ls_cod_versione

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
ll_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_2
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_3
ls_cod_operaio = s_cs_xx.parametri.parametro_s_4
ls_cod_versione = s_cs_xx.parametri.parametro_s_5

select max(prog_orari)
into   :ll_prog_orari
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda		and    
		 anno_commessa = :ll_anno_commessa		and    
		 num_commessa = :ll_num_commessa			and    
		 prog_riga = :ll_prog_riga					and    
		 cod_prodotto = :ls_cod_prodotto			and    
		 cod_versione = :ls_cod_versione			and
		 cod_reparto = :ls_cod_reparto			and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 flag_inizio = 'N';

select max(prog_difetto)
into   :ll_prog_difetto
from   difetti_det_orari_out
where  cod_azienda = :s_cs_xx.cod_azienda    and    
		 anno_commessa = :ll_anno_commessa		and    
		 num_commessa = :ll_num_commessa			and    
		 prog_riga = :ll_prog_riga					and    
		 cod_prodotto = :ls_cod_prodotto			and    
		 cod_versione = :ls_cod_versione			and
		 cod_reparto = :ls_cod_reparto			and    
		 cod_lavorazione = :ls_cod_lavorazione	and    
		 cod_operaio = :ls_cod_operaio			and    
		 prog_orari = :ll_prog_orari;

if isnull(ll_prog_difetto) then
	ll_prog_difetto=1
else
	ll_prog_difetto++
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
      SetItem(l_Idx, "anno_commessa", ll_anno_commessa)
		SetItem(l_Idx, "num_commessa", ll_num_commessa)
	   SetItem(l_Idx, "prog_riga", ll_prog_riga)
		SetItem(l_Idx, "cod_prodotto", ls_cod_prodotto)
		SetItem(l_Idx, "cod_versione", ls_cod_versione)
		SetItem(l_Idx, "cod_reparto", ls_cod_reparto)
		SetItem(l_Idx, "cod_lavorazione", ls_cod_lavorazione)
		SetItem(l_Idx, "cod_operaio", ls_cod_operaio)
		SetItem(l_Idx, "prog_orari", ll_prog_orari)
		SetItem(l_Idx, "prog_difetto", ll_prog_difetto)
		SetItem(l_Idx, "data_difetto", datetime(today(),00:00:00))

   END IF
NEXT

end event

event pcd_commit;////******************************************************************
////  PC Module     : uo_DW_Main
////  Event         : pcd_Commit
////  Description   : Commit the changes to the database.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1992-1995.  All Rights Reserved.
////******************************************************************
//
////----------
////  If this is an external DataWindow, bypass this operation.
////----------
//
//IF IsNull(i_DBCA) THEN
//   GOTO Finished
//END IF
//
////----------
////  Commit the updates made to the database.
////----------
//
//COMMIT USING i_DBCA;
//
////----------
////  Check for errors that occurred during the COMMIT.
////----------
//
//IF i_DBCA.SQLCode <> 0 THEN
//   PCCA.Error = c_Fatal
//END IF
//
//Finished:
//
//i_ExtendMode = i_InUse
end event

type dw_det_difetti_out from uo_cs_xx_dw within w_rileva_difetti_out
integer x = 23
integer y = 580
integer width = 2331
integer height = 660
integer taborder = 20
string dataobject = "d_det_difetti_out"
borderstyle borderstyle = styleraised!
end type

type cb_chiudi from commandbutton within w_rileva_difetti_out
integer x = 1989
integer y = 1260
integer width = 366
integer height = 80
integer taborder = 3
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

