﻿$PBExportHeader$w_rileva_difetti_out_vista.srw
$PBExportComments$Window vista difetti out
forward
global type w_rileva_difetti_out_vista from w_cs_xx_principale
end type
type dw_lista_difetti_out from uo_cs_xx_dw within w_rileva_difetti_out_vista
end type
type dw_det_difetti_out from uo_cs_xx_dw within w_rileva_difetti_out_vista
end type
end forward

global type w_rileva_difetti_out_vista from w_cs_xx_principale
int Width=2433
int Height=1461
boolean TitleBar=true
string Title="Difetti per dettaglio commessa"
dw_lista_difetti_out dw_lista_difetti_out
dw_det_difetti_out dw_det_difetti_out
end type
global w_rileva_difetti_out_vista w_rileva_difetti_out_vista

event pc_setwindow;call super::pc_setwindow;dw_lista_difetti_out.set_dw_options(sqlca, &
                                       i_openparm, &
												   c_default + c_scrollparent, &
                                       c_default)

dw_det_difetti_out.set_dw_options(sqlca, &
                                  dw_lista_difetti_out, &
											 c_sharedata+c_scrollparent + c_nonew + c_nomodify + c_nodelete, &
                                  c_default)

iuo_dw_main = dw_lista_difetti_out
end event

on w_rileva_difetti_out_vista.create
int iCurrent
call w_cs_xx_principale::create
this.dw_lista_difetti_out=create dw_lista_difetti_out
this.dw_det_difetti_out=create dw_det_difetti_out
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_difetti_out
this.Control[iCurrent+2]=dw_det_difetti_out
end on

on w_rileva_difetti_out_vista.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_lista_difetti_out)
destroy(this.dw_det_difetti_out)
end on

type dw_lista_difetti_out from uo_cs_xx_dw within w_rileva_difetti_out_vista
int X=23
int Y=21
int Width=2355
int Height=621
string DataObject="d_lista_difetti_out_vista"
BorderStyle BorderStyle=StyleLowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_prog_orari,ll_num_commessa,ll_prog_riga

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")




l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa,ll_prog_riga)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF



end event

type dw_det_difetti_out from uo_cs_xx_dw within w_rileva_difetti_out_vista
int X=23
int Y=661
int Width=2355
int Height=681
int TabOrder=2
string DataObject="d_det_difetti_out_vista"
BorderStyle BorderStyle=StyleRaised!
end type

