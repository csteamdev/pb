﻿$PBExportHeader$w_rileva_quantita.srw
$PBExportComments$Window w_rileva_quantita
forward
global type w_rileva_quantita from w_cs_xx_risposta
end type
type st_2 from statictext within w_rileva_quantita
end type
type st_1 from statictext within w_rileva_quantita
end type
type dw_rileva_quantita_info from datawindow within w_rileva_quantita
end type
type cbx_problemi from checkbox within w_rileva_quantita
end type
type cb_ok from commandbutton within w_rileva_quantita
end type
type cb_annulla from commandbutton within w_rileva_quantita
end type
type dw_quantita_utilizzata from uo_cs_xx_dw within w_rileva_quantita
end type
type cb_elimina from commandbutton within w_rileva_quantita
end type
type cb_inserisci from commandbutton within w_rileva_quantita
end type
type cb_cambio_mp from commandbutton within w_rileva_quantita
end type
type cb_conferma_stock from commandbutton within w_rileva_quantita
end type
type cb_sblocca from commandbutton within w_rileva_quantita
end type
type dw_ricerca from u_dw_search within w_rileva_quantita
end type
type dw_scelta_stock from uo_cs_xx_dw within w_rileva_quantita
end type
type dw_lista_stock from uo_cs_xx_dw within w_rileva_quantita
end type
type dw_folder from u_folder within w_rileva_quantita
end type
type dw_rileva_difetti_out from datawindow within w_rileva_quantita
end type
end forward

global type w_rileva_quantita from w_cs_xx_risposta
integer width = 4663
integer height = 2608
string title = "Quantità Utilizzate per S.L. e M.P."
boolean controlmenu = false
st_2 st_2
st_1 st_1
dw_rileva_quantita_info dw_rileva_quantita_info
cbx_problemi cbx_problemi
cb_ok cb_ok
cb_annulla cb_annulla
dw_quantita_utilizzata dw_quantita_utilizzata
cb_elimina cb_elimina
cb_inserisci cb_inserisci
cb_cambio_mp cb_cambio_mp
cb_conferma_stock cb_conferma_stock
cb_sblocca cb_sblocca
dw_ricerca dw_ricerca
dw_scelta_stock dw_scelta_stock
dw_lista_stock dw_lista_stock
dw_folder dw_folder
dw_rileva_difetti_out dw_rileva_difetti_out
end type
global w_rileva_quantita w_rileva_quantita

type variables
integer ii_ok
dec{4} idd_quan_anticipo
long il_prog_orari
string is_cod_prodotto, is_cod_versione
long il_controllo //contiene il numero totale di stock che 
                         //vengono scelti, al momento dell'ok
                        //viene confrontata con il numero di righe
                        //trovate in tabella mp_com_stock_det_orari
end variables

forward prototypes
public function integer wf_imposta_stock ()
public function integer wf_difetti_out ()
end prototypes

public function integer wf_imposta_stock ();string   ls_cod_prodotto_mp,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, & 
			ls_cod_tipo_commessa,ls_test,ls_flag_mat_prima,ls_modify,ls_cod_reparto,ls_cod_lavorazione
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_orari,ll_num_fasi_aperte
datetime ldt_data_stock
dec{4}   ldd_quan_disponibile
integer  li_anno_commessa

li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_2
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_3

dw_scelta_stock.reset()

select count(*)
into   :ll_num_fasi_aperte
from   avan_produzione_com
where  cod_azienda = :s_cs_xx.cod_azienda and 	 
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 prog_riga = :ll_prog_riga          and    
		 flag_fine_fase = 'N';

if ll_num_fasi_aperte > 0 then
	
	select max(prog_orari)
	into   :ll_prog_orari
	from   det_orari_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :li_anno_commessa 	and    
			 num_commessa = :ll_num_commessa    and    
			 prog_riga = :ll_prog_riga   	      and    
			 cod_prodotto = :is_cod_prodotto    and
			 cod_versione = :is_cod_versione    and    
			 cod_reparto = :ls_cod_reparto      and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 flag_inizio = 'N';
else
	
	ll_prog_orari = il_prog_orari
	
end if	

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return 0
end if
	
ls_cod_prodotto_mp = dw_quantita_utilizzata.getitemstring(dw_quantita_utilizzata.getrow(),"cod_semilavorato")

select cod_azienda
into   :ls_test
from   mp_com_stock_det_orari
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 prog_riga = :ll_prog_riga          and    
		 cod_prodotto = :ls_cod_prodotto_mp and    
		 prog_orari = :ll_prog_orari        and    
		 cod_reparto = :ls_cod_reparto      and    
		 cod_lavorazione = :ls_cod_lavorazione;
	 
//	if sqlca.sqlcode < 0 then
//		messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
//		 
//		return 0
//	end if
	
ls_flag_mat_prima = dw_quantita_utilizzata.getitemstring(dw_quantita_utilizzata.getrow(),"mat_prima")

if ls_flag_mat_prima="S" then 
	cb_sblocca.enabled=true
else
end if
	
if (ls_flag_mat_prima = "S") and (sqlca.sqlcode = 100) then

	select cod_tipo_commessa
	into   :ls_cod_tipo_commessa
	from   anag_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if
	
	select cod_deposito_prelievo
	into   :ls_cod_deposito
	from   tab_tipi_commessa
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_commessa=:ls_cod_tipo_commessa;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		 
		return 0
	end if

	declare righe_stock cursor for
	select  cod_ubicazione,   
			  cod_lotto,   
			  data_stock,   
			  prog_stock,
			  giacenza_stock - quan_assegnata - quan_in_spedizione  
	 FROM   stock   
	 where  cod_azienda=:s_cs_xx.cod_azienda
	 and    cod_prodotto =:ls_cod_prodotto_mp
	 and    cod_deposito =:ls_cod_deposito
	 and    giacenza_stock - quan_assegnata - quan_in_spedizione >0
	 ORDER BY data_stock;
 
	 open righe_stock;
	
	 if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		 
		return 0
	 end if

	do while 1 = 1
		fetch righe_stock 
		into  :ls_cod_ubicazione,
				:ls_cod_lotto,   
				:ldt_data_stock,   
				:ll_prog_stock,
				:ldd_quan_disponibile;

		choose case sqlca.sqlcode  
			case 100
				exit
			case is < 0 
				g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext, stopsign!)
				
					
				exit
		end choose

		ll_num_righe=dw_scelta_stock.rowcount()
	
		if ll_num_righe=0 then
			ll_num_righe=1
		else
			ll_num_righe++
		end if
	
		dw_scelta_stock.insertrow(ll_num_righe)
		dw_scelta_stock.setitem(ll_num_righe,"cod_prodotto",ls_cod_prodotto_mp)
		dw_scelta_stock.setitem(ll_num_righe,"cod_deposito",ls_cod_deposito)
		dw_scelta_stock.setitem(ll_num_righe,"cod_ubicazione",ls_cod_ubicazione)
		dw_scelta_stock.setitem(ll_num_righe,"cod_lotto",ls_cod_lotto)
		dw_scelta_stock.setitem(ll_num_righe,"data_stock",ldt_data_stock)
		dw_scelta_stock.setitem(ll_num_righe,"prog_stock",ll_prog_stock)
		dw_scelta_stock.setitem(ll_num_righe,"quan_disponibile",ldd_quan_disponibile)
		dw_scelta_stock.setitem(ll_num_righe,"quan_prelevata",0)

	loop
	
	close righe_stock;
	
	dw_scelta_stock.Reset_DW_Modified(c_ResetChildren)
	dw_scelta_stock.settaborder("quan_prelevata",10)
	dw_scelta_stock.settaborder("quan_reso",20)
	dw_scelta_stock.settaborder("quan_sfrido",30)
	dw_scelta_stock.settaborder("quan_scarto",40)
	dw_scelta_stock.triggerevent("pcd_search")
	cb_conferma_stock.enabled=true
	dw_scelta_stock.enabled = true
	cb_sblocca.enabled=false
	dw_lista_stock.Change_DW_Current( )
	triggerevent("pc_retrieve")
	return 0
else
	cb_conferma_stock.enabled=false
	dw_scelta_stock.enabled = false
	dw_lista_stock.Change_DW_Current( )
	triggerevent("pc_retrieve")
	return 0
end if

end function

public function integer wf_difetti_out ();LONG     ll_anno_commessa,ll_prog_orari,ll_num_commessa,ll_prog_riga,ll_riga
string   ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione,ls_cod_operaio,ls_cod_errore,ls_cod_attrezzatura
dec{4}   ldd_quan_scarto
datetime ldt_data_difetto

dw_rileva_difetti_out.AcceptText()
ldt_data_difetto = datetime(today (),00:00:00)

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
ll_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_2
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_3
ls_cod_operaio = s_cs_xx.parametri.parametro_s_4

select max(prog_orari)
into   :ll_prog_orari
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :ll_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 prog_riga = :ll_prog_riga          and    
		 cod_prodotto = :ls_cod_prodotto    and    
		 cod_versione = :is_cod_versione    and
		 cod_reparto = :ls_cod_reparto      and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 flag_inizio = 'N';

if sqlca.sqlcode<0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

for ll_riga = 1 to dw_rileva_difetti_out.rowcount()
	ls_cod_errore = dw_rileva_difetti_out.getitemstring(ll_riga,"cod_errore")
	ls_cod_attrezzatura = dw_rileva_difetti_out.getitemstring(ll_riga,"cod_attrezzatura")
	ldd_quan_scarto = dw_rileva_difetti_out.getitemnumber(ll_riga,"quan_scarto")
	
	insert into difetti_det_orari_out
	(cod_azienda,
	 cod_prodotto,
	 cod_versione,
	 anno_commessa,
	 num_commessa,
	 cod_reparto,
	 cod_lavorazione,
	 prog_riga,
	 cod_operaio,
	 prog_orari,
	 cod_errore,
	 prog_difetto,
	 cod_attrezzatura,
	 quan_scarto,
	 data_difetto)
	 values
	 (:s_cs_xx.cod_azienda,
	  :ls_cod_prodotto,
	  :is_cod_versione,
	  :ll_anno_commessa,
	  :ll_num_commessa,
	  :ls_cod_reparto,
	  :ls_cod_lavorazione,
	  :ll_prog_riga,
	  :ls_cod_operaio,
	  :ll_prog_orari,
	  :ls_cod_errore,
	  :ll_riga,
	  :ls_cod_attrezzatura,
	  :ldd_quan_scarto,
	  :ldt_data_difetto);
	  
	if sqlca.sqlcode<0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
next

return 0
end function

on w_rileva_quantita.create
int iCurrent
call super::create
this.st_2=create st_2
this.st_1=create st_1
this.dw_rileva_quantita_info=create dw_rileva_quantita_info
this.cbx_problemi=create cbx_problemi
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.dw_quantita_utilizzata=create dw_quantita_utilizzata
this.cb_elimina=create cb_elimina
this.cb_inserisci=create cb_inserisci
this.cb_cambio_mp=create cb_cambio_mp
this.cb_conferma_stock=create cb_conferma_stock
this.cb_sblocca=create cb_sblocca
this.dw_ricerca=create dw_ricerca
this.dw_scelta_stock=create dw_scelta_stock
this.dw_lista_stock=create dw_lista_stock
this.dw_folder=create dw_folder
this.dw_rileva_difetti_out=create dw_rileva_difetti_out
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_2
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_rileva_quantita_info
this.Control[iCurrent+4]=this.cbx_problemi
this.Control[iCurrent+5]=this.cb_ok
this.Control[iCurrent+6]=this.cb_annulla
this.Control[iCurrent+7]=this.dw_quantita_utilizzata
this.Control[iCurrent+8]=this.cb_elimina
this.Control[iCurrent+9]=this.cb_inserisci
this.Control[iCurrent+10]=this.cb_cambio_mp
this.Control[iCurrent+11]=this.cb_conferma_stock
this.Control[iCurrent+12]=this.cb_sblocca
this.Control[iCurrent+13]=this.dw_ricerca
this.Control[iCurrent+14]=this.dw_scelta_stock
this.Control[iCurrent+15]=this.dw_lista_stock
this.Control[iCurrent+16]=this.dw_folder
this.Control[iCurrent+17]=this.dw_rileva_difetti_out
end on

on w_rileva_quantita.destroy
call super::destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_rileva_quantita_info)
destroy(this.cbx_problemi)
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.dw_quantita_utilizzata)
destroy(this.cb_elimina)
destroy(this.cb_inserisci)
destroy(this.cb_cambio_mp)
destroy(this.cb_conferma_stock)
destroy(this.cb_sblocca)
destroy(this.dw_ricerca)
destroy(this.dw_scelta_stock)
destroy(this.dw_lista_stock)
destroy(this.dw_folder)
destroy(this.dw_rileva_difetti_out)
end on

event pc_setwindow;call super::pc_setwindow;string  ls_cod_prodotto_figlio,ls_cod_prodotto_padre,ls_des_prodotto, ls_test,ls_cod_tipo_commessa,ls_flag_muovi_mp, ls_cod_versione_figlio, & 
		  ls_flag_materia_prima, ls_cod_prodotto_variante,ls_flag_materia_prima_variante
long    ll_num_righe,ll_num_commessa,ll_prog_riga,ll_num_fasi_aperte
integer li_anno_commessa
dec{4}  ldd_quan_prodotta,ldd_quan_in_produzione,ldd_quan_utilizzo,ldd_quan_utilizzo_variante

is_cod_prodotto = s_cs_xx.parametri.parametro_s_1
is_cod_versione = s_cs_xx.parametri.parametro_s_5
set_w_options(c_noenablepopup)
s_cs_xx.parametri.parametro_d_1=0
s_cs_xx.parametri.parametro_d_2=0

windowobject l_objects_1[]
windowobject l_objects_2[]
windowobject l_objects_3[]
windowobject l_objects_4[]


dw_quantita_utilizzata.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)

dw_scelta_stock.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)

dw_lista_stock.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_default)
save_on_close(c_socnosave)

//l_objects_1[1] = dw_quantita_utilizzata
//l_objects_1[2] = cb_cambio_mp
//l_objects_1[3] = dw_ricerca
//l_objects_1[4] = cb_ricerca
//dw_folder.fu_AssignTab(1, "&MP & SL", l_Objects_1[])

l_objects_2[1] = dw_scelta_stock
l_objects_2[2] = cb_conferma_stock
l_objects_2[3] = cb_sblocca
dw_folder.fu_AssignTab(1, "&Stock Disponibili", l_Objects_2[])

l_objects_3[1] = dw_lista_stock
dw_folder.fu_AssignTab(2, "&Stock Usati", l_Objects_3[])

l_objects_4[1] = dw_rileva_difetti_out
l_objects_4[2] = cb_elimina
l_objects_4[3] = cb_inserisci
dw_folder.fu_AssignTab(3, "&Difetti", l_Objects_4[])

dw_folder.fu_FolderCreate(3,4)
dw_folder.fu_SelectTab(1)

dw_rileva_quantita_info.insertrow(0)

ii_ok = -1
ls_cod_prodotto_padre = is_cod_prodotto
li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2


select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if

setnull(ls_test)

//select cod_prodotto_padre
//into   :ls_test
//from   distinta
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    cod_prodotto_figlio=:ls_cod_prodotto_padre
//and    cod_versione =:ls_cod_versione;					//verifica se è il prodotto finito

select cod_prodotto,
		 cod_tipo_commessa
into   :ls_test,
		 :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if

if ls_test <> ls_cod_prodotto_padre then
	
	select quan_in_produzione
	into   :ldd_quan_in_produzione
	from   avan_produzione_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :li_anno_commessa 	and    
			 num_commessa = :ll_num_commessa 	and    
			 prog_riga = :ll_prog_riga	         and    
			 cod_prodotto = :ls_cod_prodotto_padre and
			 cod_versione = :is_cod_versione;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		return
	end if

else
	
	select quan_in_produzione,	
			 quan_anticipo
	into   :ldd_quan_in_produzione,
			 :idd_quan_anticipo
	from   det_anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :li_anno_commessa 	and    
			 num_commessa = :ll_num_commessa	   and    
			 prog_riga = :ll_prog_riga;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		return
	end if

	dw_rileva_quantita_info.setitem(1,"quan_anticipo", idd_quan_anticipo)
	
end if

select flag_muovi_mp
into   :ls_flag_muovi_mp
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return
end if

if ls_flag_muovi_mp ='N' then
	dw_folder.fu_HideTab(2)
	dw_folder.fu_HideTab(3)
end if

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda   and    
		 cod_prodotto = :ls_cod_prodotto_padre;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if

dw_rileva_quantita_info.setitem(1,"cod_prodotto", ls_cod_prodotto_padre)
dw_rileva_quantita_info.setitem(1,"des_prodotto", ls_des_prodotto)
dw_rileva_quantita_info.setitem(1,"cod_versione", is_cod_versione)
dw_rileva_quantita_info.setitem(1,"quan_in_produzione", ldd_quan_in_produzione)
dw_rileva_quantita_info.setitem(1,"quan_prodotta", ldd_quan_in_produzione)


// modificato per gestione integrazioni EnMe 15/11/2004
declare righe_distinta cursor for 

	select  cod_prodotto_figlio,
			  quan_utilizzo,
			  flag_materia_prima,
			  cod_versione_figlio
	from    distinta 
	where   cod_azienda = :s_cs_xx.cod_azienda  and     
	        cod_prodotto_padre = :ls_cod_prodotto_padre and     
			  cod_versione = :is_cod_versione
union all 
	select  cod_prodotto_figlio,
			  quan_utilizzo,
			  flag_materia_prima,
			  cod_versione_figlio
	from    integrazioni_commessa
	where   cod_azienda = :s_cs_xx.cod_azienda and     
	        anno_commessa = :li_anno_commessa  and     
			  num_commessa = :ll_num_commessa 	and     
			  cod_prodotto_padre = :ls_cod_prodotto_padre and     
			  cod_versione_padre = :is_cod_versione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if

open righe_distinta;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if

do while 1 = 1
	fetch righe_distinta 
	into  :ls_cod_prodotto_figlio,
			:ldd_quan_utilizzo,
			:ls_flag_materia_prima,
			:ls_cod_versione_figlio;
	
  	if (sqlca.sqlcode = 100) then exit

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		return 
	end if

	select cod_prodotto,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
	       anno_commessa = :li_anno_commessa 	and	 
			 num_commessa = :ll_num_commessa 	and	 
			 cod_prodotto_padre = :ls_cod_prodotto_padre and	 
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio 	and    
			 cod_versione = :is_cod_versione  and    
			 cod_versione_figlio = :ls_cod_versione_figlio;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		return
	end if

	if sqlca.sqlcode = 0 then
		ls_cod_prodotto_figlio = ls_cod_prodotto_variante
		ldd_quan_utilizzo=ldd_quan_utilizzo_variante
		
		// modificato per gestione integrazioni EnMe 15/11/2004
		if ls_flag_materia_prima_variante = 'N' then            //Qui devo andare a fare la verifica di materia prima
			declare righe_test cursor for				//nella tabella distinta e non nella variante poichè 
				
				SELECT cod_prodotto_figlio									  //non è detto che esista un'altra variante sotto alla prima
				from   distinta												  //questo farebbe presumere che la variante corrente sia una MP
				where  cod_azienda = :s_cs_xx.cod_azienda				  //anche se non lo è. Mentre andando a testare la distinta so che
				and    cod_prodotto_padre = :ls_cod_prodotto_figlio	  //se non esiste il ramo sottostante allora è MP. In questo momento
				and    cod_versione = :ls_cod_versione_figlio			 		  //mi interessa sapere se c'è o non c'è il prodotto sottostante è non che prodotto è
			
			union	all 		
				select  cod_prodotto_figlio
				from    integrazioni_commessa
				where   cod_azienda = :s_cs_xx.cod_azienda
				and     anno_commessa = :li_anno_commessa
				and     num_commessa = :ll_num_commessa
				and     cod_prodotto_padre = :ls_cod_prodotto_figlio
				and     cod_versione_padre = :ls_cod_versione_figlio;
		
			open righe_test;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Sep","Errore in OPEN cursore righe test~r~n" + sqlca.sqlerrtext,stopsign!)
				return 
			end if
				
			fetch righe_test
			into  :ls_test;

			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
				close righe_test;
				return 
			end if

			if sqlca.sqlcode = 100 then 
				close righe_test;
				ls_flag_materia_prima="S"
			else
				close righe_test;
				ls_flag_materia_prima="N"
			end if
		else
			ls_flag_materia_prima='S'
		end if
	else
		if ls_flag_materia_prima='N' then
			
			declare righe_test_1 cursor for
			
				SELECT cod_prodotto_figlio
				FROM   distinta  
				WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
				       cod_prodotto_padre = :ls_cod_prodotto_figlio and    
						 cod_versione = :ls_cod_versione_figlio
				union all 
				select  cod_prodotto_figlio				
				from    integrazioni_commessa
				where   cod_azienda = :s_cs_xx.cod_azienda and     
				        anno_commessa = :li_anno_commessa  and     
						  num_commessa = :ll_num_commessa	 and     
						  cod_prodotto_padre = :ls_cod_prodotto_figlio and
						  cod_versione_padre = :ls_cod_versione_figlio;
		
			open righe_test_1;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Sep","Errore in OPEN cursore righe test_1~r~n" + sqlca.sqlerrtext,stopsign!)
				return 
			end if
			
			fetch righe_test_1
			into  :ls_test;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
				close righe_test_1;
				return 
			end if
	
			if sqlca.sqlcode = 100 then 
				close righe_test_1;
				ls_flag_materia_prima="S"
			else
				close righe_test_1;
				ls_flag_materia_prima="N"
			end if
		end if
	end if
	
	close righe_test_1;
	
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto = :ls_cod_prodotto_figlio;
	
	if ls_flag_materia_prima = 'S' and ls_flag_muovi_mp = 'S' then

		// fare inserimento record in mat prime commessa 
		// se non esiste già (potrebbe esserci un'altra sessione che lo ha già fatto)
		
		select cod_prodotto
		into   :ls_test
		from   mat_prime_commessa
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       anno_commessa = :li_anno_commessa 	and    
				 num_commessa = :ll_num_commessa		and    
				 prog_riga = :ll_prog_riga 		   and    
				 cod_prodotto = :ls_cod_prodotto_figlio;
		
		if sqlca.sqlcode =100 then
		
			INSERT INTO mat_prime_commessa
				( cod_azienda,   
				  anno_commessa,   
				  num_commessa,   
				  prog_riga,   
				  cod_prodotto,   
				  anno_reg_reso,   
				  num_reg_reso,   
				  quan_necessaria,   
				  quan_assegnata,   
				  quan_in_produzione,   
				  quan_utilizzata,   
				  cod_tipo_mov_reso,   
				  cod_tipo_mov_sfrido,   
				  anno_reg_sfrido,   
				  num_reg_sfrido,   
				  quan_reso,   
				  quan_sfrido,   
				  quan_scarto,   
				  anno_reg_des_mov_reso,   
				  num_reg_des_mov_reso,   
				  anno_reg_des_mov_sfrido,   
				  num_reg_des_mov_sfrido )  
			VALUES ( :s_cs_xx.cod_azienda,   
						:li_anno_commessa,   
						:ll_num_commessa,   
						:ll_prog_riga,   
						:ls_cod_prodotto_figlio,   
						null,   
						null,   
						0,   
						0,   
						0,   
						0,   
						null,   
						null,   
						null,   
						null,   
						0,   
						0,   
						0,   
						null,   
						null,   
						null,   
						null )  ;
	
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Sep","Errore INSERT mat_prime_commessa~r~n" + sqlca.sqlerrtext,stopsign!)
				return 
			end if
		end if
	end if

	ll_num_righe = dw_quantita_utilizzata.rowcount()

	if ll_num_righe=0 then
		ll_num_righe=1
	else
		ll_num_righe++
	end if

	dw_quantita_utilizzata.insertrow(ll_num_righe)
	dw_quantita_utilizzata.setitem(ll_num_righe,"cod_semilavorato",ls_cod_prodotto_figlio)
	dw_quantita_utilizzata.setitem(ll_num_righe,"cod_versione", ls_cod_versione_figlio)
 	dw_quantita_utilizzata.setitem(ll_num_righe,"des_semilavorato",ls_des_prodotto)
	dw_quantita_utilizzata.setitem(ll_num_righe,"quan_utilizzo",ldd_quan_utilizzo)
	dw_quantita_utilizzata.setitem(ll_num_righe,"mat_prima",ls_flag_materia_prima)
	
loop
	
close righe_distinta;

dw_quantita_utilizzata.Reset_DW_Modified(c_ResetChildren)

dw_quantita_utilizzata.settaborder("quan_utilizzata",10)
dw_quantita_utilizzata.settaborder("quan_reso",20)
dw_quantita_utilizzata.settaborder("quan_sfrido",30)
dw_quantita_utilizzata.settaborder("quan_scarto",40)

select count(*)										// questa parte di controllo serve
into   :ll_num_fasi_aperte							// quando esistono delle materie prime
from   avan_produzione_com							// direttamente collegate sotto al PF
where  cod_azienda = :s_cs_xx.cod_azienda and 	 
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 prog_riga = :ll_prog_riga          and    
		 flag_fine_fase = 'N';

if ll_num_fasi_aperte = 0 then					// se tutte le fasi sono chiuse allora sto 
	select max(prog_orari)							// imputando i dati relativi agli SL e alle 
	into   :il_prog_orari							// eventuali MP che si trovano direttamente
	from   mp_com_stock_det_orari					// sotto al PF.
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga;
		
	il_prog_orari ++
	
end if

il_controllo=0 //questa variabile serve per memorizzare il numero totale di stock scelti per lo scarico
					//nel momento in cui si preme il tasto ok questa variabile viene confrontata con il numero
					//di record trovati in mat_prime_com_stock_det_orari che deve essere lo stesso se è diverso viene 
					//segnalato un errore di possibile movimento di magazzino errato.

dw_rileva_quantita_info.accepttext()

dw_rileva_quantita_info.setfocus()
end event

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_rileva_difetti_out, &
                 "cod_errore", &
                 sqlca, &
                 "tab_difformita", &
                 "cod_errore", &
                 "des_difformita", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_rileva_difetti_out, &
                 "cod_attrezzatura", &
                 sqlca, &
                 "anag_attrezzature", &
                 "cod_attrezzatura", &
                 "descrizione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type st_2 from statictext within w_rileva_quantita
integer x = 3355
integer y = 16
integer width = 1248
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SOSTITUZIONE PRODOTTI DA SCARICARE"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_1 from statictext within w_rileva_quantita
integer x = 3355
integer y = 516
integer width = 1248
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "INFORMAZIONI GENERALI"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_rileva_quantita_info from datawindow within w_rileva_quantita
integer x = 3355
integer y = 596
integer width = 1253
integer height = 868
integer taborder = 50
string title = "none"
string dataobject = "d_rileva_quantita_info"
boolean livescroll = true
end type

event itemchanged;if isvalid(dwo) then
	
	choose case dwo.name
			
		case "quan_prodotta"
			
			long    ll_num_righe,ll_t
			dec{4}  ldd_quan_utilizzata,ldd_quan_prodotta,ldd_quan_utilizzo
			
			ll_num_righe = dw_quantita_utilizzata.rowcount()
			ldd_quan_prodotta = getitemnumber(row,"quan_prodotta")
			
			for ll_t=1 to ll_num_righe
				ldd_quan_utilizzo   = dw_quantita_utilizzata.getitemnumber(ll_t,"quan_utilizzo")	
				ldd_quan_utilizzata = ldd_quan_prodotta*ldd_quan_utilizzo
				dw_quantita_utilizzata.setitem(ll_t,"quan_utilizzata",ldd_quan_utilizzata)
				dw_quantita_utilizzata.setitem(ll_t,"quan_reso",0)
				dw_quantita_utilizzata.setitem(ll_t,"quan_sfrido",0)
				dw_quantita_utilizzata.setitem(ll_t,"quan_scarto",0)
			next
			
			dw_quantita_utilizzata.settaborder("quan_utilizzata",10)
			dw_quantita_utilizzata.settaborder("quan_reso",20)
			dw_quantita_utilizzata.settaborder("quan_sfrido",30)
			dw_quantita_utilizzata.settaborder("quan_scarto",40)		
			
	end choose
end if

end event

type cbx_problemi from checkbox within w_rileva_quantita
integer x = 1810
integer y = 2412
integer width = 677
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Problemi in produzione"
boolean lefttext = true
end type

type cb_ok from commandbutton within w_rileva_quantita
integer x = 2569
integer y = 2408
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok "
end type

event clicked;dec{4}   ldd_quan_prelevata,ldd_quan_utilizzata,ldd_quan_reso, ldd_quan_sfrido, ldd_quan_scarto, & 
			ldd_quan_impegnata_attuale, ld_quan_effettiva, ld_quan_scarto
long     ll_prog_stock[],ll_anno_reg_des_mov, ll_num_reg_des_mov,ll_num_commessa, ll_t,ll_prog_stock_prec,& 
			ll_prog_orari,ll_prog_riga,ll_anno_registrazione[],ll_num_registrazione[], ll_num_righe, & 
			ll_righe_mp_com_stock_det_orari,ll_num_fasi_aperte,ll_anno_reg_sl,ll_num_reg_sl,ll_num_righe_trovate
string   ls_errore,ls_cod_tipo_commessa,ls_cod_tipo_movimento,ls_cod_prodotto,ls_cod_deposito[], & 
		   ls_cod_ubicazione[],ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[], & 
			ls_flag_mat_prima, ls_cod_reparto, ls_cod_lavorazione, & 
			ls_flag_muovi_sl,ls_cod_tipo_mov_prel_sl,ls_anno_commessa, ls_flag_muovi_mp, &
			ls_cod_prodotto_prec,ls_cod_deposito_prec,ls_cod_ubicazione_prec,ls_cod_lotto_prec, ls_cod_versione_dw
integer  li_risposta,li_anno_commessa
datetime ldt_data_stock[],ldt_oggi,ldt_data_stock_prec

uo_magazzino luo_mag


ll_righe_mp_com_stock_det_orari = 0

ld_quan_scarto = dw_rileva_quantita_info.getitemnumber(1,"quan_scarto")
//dec(st_quan_scarto.text)
ld_quan_effettiva = dw_rileva_quantita_info.getitemnumber(1, "quan_prodotta") - ld_quan_scarto

dw_rileva_quantita_info.setitem(1, "quan_effettiva", ld_quan_effettiva)
//st_quan_effettiva.text =  string(ld_quan_effettiva)

if dw_rileva_quantita_info.getitemnumber(1,"quan_effettiva") < 0 then
	g_mb.messagebox("Sep","Attenzione non è possibile che gli scarti superino la quantità prodotta.",stopsign!)
	return
end if

ldt_oggi = datetime(today(),time('00:00:00'))

setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_2
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_3

ls_anno_commessa= string(li_anno_commessa)

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return
end if
	
select cod_tipo_mov_prel_mat_prime,
		 flag_muovi_sl,
		 cod_tipo_mov_prel_sl,
		 flag_muovi_mp
into   :ls_cod_tipo_movimento,
		 :ls_flag_muovi_sl,
		 :ls_cod_tipo_mov_prel_sl,
		 :ls_flag_muovi_mp
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return
end if

select count(*)										// questa parte di controllo serve
into   :ll_num_fasi_aperte							// quando esistono delle materie prime
from   avan_produzione_com							// direttamente collegate sotto al PF
where  cod_azienda=:s_cs_xx.cod_azienda
and 	 anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa
and    prog_riga=:ll_prog_riga
and    flag_fine_fase='N';

if ll_num_fasi_aperte > 0 then
	
	select max(prog_orari)
	into   :ll_prog_orari
	from   det_orari_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
	       anno_commessa = :li_anno_commessa		and    
			 num_commessa = :ll_num_commessa			and    
			 prog_riga = :ll_prog_riga					and    
			 cod_prodotto = :is_cod_prodotto			and    
			 cod_versione = :is_cod_versione			and    
			 cod_reparto = :ls_cod_reparto			and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 flag_inizio = 'N';
else
	
	ll_prog_orari = il_prog_orari
	
end if


if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return
end if

//questo conteggio verifica il numero di righe trovate per poi confrontarlo con la variabile il_controllo
select  count(*)
into    :ll_num_righe_trovate
from    mp_com_stock_det_orari
where   cod_azienda = :s_cs_xx.cod_azienda	and     
		  anno_commessa = :li_anno_commessa		and     
		  num_commessa = :ll_num_commessa		and     
		  prog_riga = :ll_prog_riga				and     
		  prog_orari = :ll_prog_orari				and     
		  cod_lavorazione = :ls_cod_lavorazione and     
		  cod_reparto = :ls_cod_reparto			 and     
		  anno_registrazione is null				and     
		  num_registrazione is null;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return
end if

//if ll_num_righe_trovate<>il_controllo then
//	messagebox("Sep","Attenzione! Si è verificato un errore: il numero di stock selezionati non è giusto, non è possibile procedere.",stopsign!)
//	return
//end if

// messagebox("Sep","il_controllo = " + string(il_controllo) + "  ll_num_righe_trovate = " + string(ll_num_righe_trovate),information!)


if ls_flag_muovi_mp ='S' then // ****************** Controllo per il movimento delle materie prime
										// ****************** Quando non si vogliono movimentare le MP, non occorre nemmeno muovere la 
										// ****************** corrispondente quantità impegnata
	
	declare righe_mp_com_sd cursor for
	select  cod_prodotto,
			  cod_deposito,
			  cod_ubicazione,
			  cod_lotto,
			  data_stock,
			  prog_stock,
			  quan_utilizzata
	from    mp_com_stock_det_orari
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     anno_commessa=:li_anno_commessa
	and     num_commessa=:ll_num_commessa
	and     prog_riga=:ll_prog_riga
	and     prog_orari=:ll_prog_orari
	and     cod_lavorazione=:ls_cod_lavorazione
	and     cod_reparto=:ls_cod_reparto
	and     anno_registrazione is null
	and     num_registrazione is null;
	
	open righe_mp_com_sd;
	
	do while 1=1
		fetch righe_mp_com_sd
		into :ls_cod_prodotto,
			  :ls_cod_deposito[1],
			  :ls_cod_ubicazione[1],
			  :ls_cod_lotto[1],
			  :ldt_data_stock[1],
			  :ll_prog_stock[1],
			  :ldd_quan_prelevata;
	
		if sqlca.sqlcode = 100 then exit
	
		if sqlca.sqlcode<0 then
			g_mb.messagebox("Sep","Errore sul db" + sqlca.sqlerrtext,stopsign!)
			close righe_mp_com_sd;
			ii_ok=0
			s_cs_xx.parametri.parametro_b_1=true	
			close(parent)
			return
		end if
	
		if (ls_cod_prodotto = ls_cod_prodotto_prec) and (ls_cod_ubicazione_prec = ls_cod_ubicazione[1]) and (ls_cod_lotto_prec=ls_cod_lotto[1]) and (ldt_data_stock_prec=ldt_data_stock[1]) and (ll_prog_stock_prec=ll_prog_stock[1]) then
			g_mb.messagebox("Sep","Attenzione! E' avvenuto uno scarico di magazzino errato.",stopsign!)
			close righe_mp_com_sd;
			ii_ok=0
			s_cs_xx.parametri.parametro_b_1=true	
			close(parent)
			return
		end if
		
		ll_righe_mp_com_stock_det_orari++
	
		if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], & 
											  ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
											  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
											  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
		
		  g_mb.messagebox("Sep","Si è verificato un errore in creazione destinazione movimenti",stopsign!)
		  close righe_mp_com_sd;
	
		  ii_ok=0
		  s_cs_xx.parametri.parametro_b_1=true	
		  close(parent)
		  return
		end if
	
		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
										 ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
		
		  g_mb.messagebox("Sep","Si è verificato un errore in fase di verifica destinazioni movimenti magazzino",stopsign!)
		  close righe_mp_com_sd;
	
		  ii_ok=0
		  s_cs_xx.parametri.parametro_b_1=true	
		  close(parent)
		  return
		end if
		
		luo_mag = create uo_magazzino
	
		li_risposta = luo_mag.uof_movimenti_mag(ldt_oggi, &
											  ls_cod_tipo_movimento, &
											  "N", &
											  ls_cod_prodotto, &
											  ldd_quan_prelevata, &
											  1, &
											  ll_num_commessa, &
											  ldt_oggi, &
											  ls_anno_commessa, &
											  ll_anno_reg_des_mov, &
											  ll_num_reg_des_mov, &
											  ls_cod_deposito[], &
											  ls_cod_ubicazione[], &
											  ls_cod_lotto[], &
											  ldt_data_stock[], &
											  ll_prog_stock[], &
											  ls_cod_fornitore[], &
											  ls_cod_cliente[], &
											  ll_anno_registrazione[], &
											  ll_num_registrazione[])
											  
		destroy luo_mag
	
		if li_risposta=-1 then
			g_mb.messagebox("Sep","Errore su movimenti magazzino, prodotto:" + ls_cod_prodotto &
						 + ", deposito:" + ls_cod_deposito[1] + ", ubicazione:" + ls_cod_ubicazione[1] &
						 + ", cod_lotto:" + ls_cod_lotto[1] + ", data_stock:" + string(ldt_data_stock[1]) &
						 + ", prog_stock:" + string(ll_prog_stock[1]),stopsign!)
			close righe_mp_com_sd;
	
			ii_ok=0
			s_cs_xx.parametri.parametro_b_1=true	
			close(parent)
			return
		end if
	
		li_risposta = f_scrivi_log (" ->PRODUZIONE ->Commessa:"+ ls_anno_commessa  + "/" + string(ll_num_commessa) +  & 
											  "->Scarico MP: Cod_prodotto=" + ls_cod_prodotto + "~t" + "cod_deposito=" + ls_cod_deposito[1] + & 
											  "~t" +"cod_ubicazione="+ ls_cod_ubicazione[1] + "~t" +"cod_lotto=" + ls_cod_lotto[1] + & 
											  "~t" +"data_stock=" + string(ldt_data_stock[1]) + "~t" +"prog_stock="+ string(ll_prog_stock[1]) + & 
											  "~t" +"quan_prelevata=" + string(ldd_quan_prelevata) + "~t" + " prog.orari:"+string(ll_prog_orari)+ "~t" +"Movimento (anno/num): " + & 
											  string(ll_anno_registrazione[1]) + "/" + string(ll_num_registrazione[1]))
		
		if li_risposta= -1 then
			if g_mb.messagebox("Sep","Questo errore non è bloccante (riguarda esclusivamente la creazione del file log), la produzione può continuare normalmente, vuoi proseguire?",question!,YesNo!,1) = 2 then
				close righe_mp_com_sd;
				ii_ok=0
				s_cs_xx.parametri.parametro_b_1=true	
				close(parent)
				return
			end if
		end if
		
		update mp_com_stock_det_orari
		set    anno_registrazione = :ll_anno_registrazione[1],
				 num_registrazione = :ll_num_registrazione[1]
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa              
		and    cod_prodotto=:ls_cod_prodotto             
		and    prog_riga=:ll_prog_riga						 
		and    prog_orari=:ll_prog_orari
		and    cod_deposito=:ls_cod_deposito[1]
		and    cod_ubicazione=:ls_cod_ubicazione[1]
		and    cod_lotto=:ls_cod_lotto[1]
		and    data_stock=:ldt_data_stock[1]
		and    prog_stock=:ll_prog_stock[1]
		and    cod_lavorazione=:ls_cod_lavorazione
		and    cod_reparto=:ls_cod_reparto;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			ii_ok=0
			s_cs_xx.parametri.parametro_b_1=true	
			close(parent)
			return
		end if
	
	//****************************************************************************************************************
	// DISIMPEGNO LE MP CONTROLLANDO CONTEMPORANEAMENTE ANCHE LA TABELLA IMPEGNO_MAT_PRIME_COMMESSA
	
		select quan_impegnata_attuale
		into   :ldd_quan_impegnata_attuale
		from   impegno_mat_prime_commessa
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    cod_prodotto=:ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			close righe_mp_com_sd;
			ii_ok=0
			s_cs_xx.parametri.parametro_b_1=true	
			close(parent)
			return
		end if
		
		if sqlca.sqlcode = 100 then // ROUTINE PER LE COMMESSE GENERATE PRIMA DI AGGIUNGERE LA TAB IMPEGNO_MAT_PRIME COMMESSA
		
			select quan_impegnata
			into   :ldd_quan_impegnata_attuale
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
		
			if ldd_quan_impegnata_attuale >= ldd_quan_prelevata then
				update anag_prodotti																		
				set 	 quan_impegnata = quan_impegnata - :ldd_quan_prelevata
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					close righe_mp_com_sd;
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
				
			else
				update anag_prodotti																		
				set 	 quan_impegnata = 0
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					close righe_mp_com_sd;
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
				
			end if
			
		else // ROUTINE PER LE COMMESSE GENERATE DOPO L'AGGIUNTA DELLA TAB IMPEGNO_MAT_PRIME COMMESSA
	
			if ldd_quan_impegnata_attuale >= ldd_quan_prelevata then
				
				update impegno_mat_prime_commessa
				set 	 quan_impegnata_attuale= quan_impegnata_attuale - :ldd_quan_prelevata
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_commessa=:li_anno_commessa
				and    num_commessa=:ll_num_commessa
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					close righe_mp_com_sd;
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
		
				update anag_prodotti																		
				set 	 quan_impegnata = quan_impegnata - :ldd_quan_prelevata
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					close righe_mp_com_sd;
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
				
			else
				update impegno_mat_prime_commessa
				set 	 quan_impegnata_attuale = 0
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_commessa=:li_anno_commessa
				and    num_commessa=:ll_num_commessa
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					close righe_mp_com_sd;
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
		
				update anag_prodotti																		
				set 	 quan_impegnata = quan_impegnata - :ldd_quan_impegnata_attuale
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					close righe_mp_com_sd;
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
						
			
			end if
	
		end if
	//****************************************************************************************************************
		ls_cod_prodotto_prec=ls_cod_prodotto
		ls_cod_ubicazione_prec = ls_cod_ubicazione[1]
		ls_cod_lotto_prec = ls_cod_lotto[1]
		ldt_data_stock_prec = ldt_data_stock[1]
		ll_prog_stock_prec = ll_prog_stock[1]
	
		setnull(ls_cod_ubicazione[1])
		setnull(ls_cod_lotto[1])
		setnull(ldt_data_stock[1])
		setnull(ll_prog_stock[1])
		setnull(ls_cod_cliente[1])
		setnull(ls_cod_fornitore[1])
	
	loop
	
	close righe_mp_com_sd;

end if  //******** Fine dello script usato quando si vogliono movimentare le MP


ll_num_righe=dw_quantita_utilizzata.rowcount()

for ll_t=1 to ll_num_righe
	dw_quantita_utilizzata.setrow(ll_t)
	ls_cod_prodotto = dw_quantita_utilizzata.getitemstring(ll_t,"cod_semilavorato")	
	ls_cod_versione_dw = dw_quantita_utilizzata.getitemstring(ll_t,"cod_versione")
	ls_flag_mat_prima = dw_quantita_utilizzata.getitemstring(ll_t,"mat_prima")
	
	dw_quantita_utilizzata.setcolumn("quan_utilizzata")	
	ldd_quan_utilizzata = dec(dw_quantita_utilizzata.gettext())
	
   dw_quantita_utilizzata.setcolumn("quan_reso")	
	ldd_quan_reso = dec(dw_quantita_utilizzata.gettext())
	
   dw_quantita_utilizzata.setcolumn("quan_sfrido")	
	ldd_quan_sfrido = dec(dw_quantita_utilizzata.gettext())
	
   dw_quantita_utilizzata.setcolumn("quan_scarto")	
	ldd_quan_scarto = dec(dw_quantita_utilizzata.gettext())

	if ls_flag_mat_prima="S" and ls_flag_muovi_mp ='S' then			//è stato aggiunto il controllo del flag muovi materie prime
		if ll_righe_mp_com_stock_det_orari=0 then
			g_mb.messagebox("SEP","Attenzione! Manca l'impostazione degli stock!",stopsign!)
			
			ii_ok=0
			s_cs_xx.parametri.parametro_b_1=true	
			close(parent)
			return
		end if
		
		select sum(quan_utilizzata),
				 sum(quan_reso),
				 sum(quan_sfrido),
				 sum(quan_scarto)
		into   :ldd_quan_utilizzata,
				 :ldd_quan_reso,
				 :ldd_quan_sfrido,
				 :ldd_quan_scarto
		from   mp_com_stock_det_orari
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga
		and    cod_prodotto=:ls_cod_prodotto
		and    prog_orari=:ll_prog_orari;

		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Sep","Attenzione! Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
			
			ii_ok=0
			s_cs_xx.parametri.parametro_b_1=true	
			close(parent)
			return
		end if

		update mat_prime_commessa
		set    quan_utilizzata=quan_utilizzata + :ldd_quan_utilizzata,
				 quan_reso=quan_reso + :ldd_quan_reso,
				 quan_sfrido=quan_sfrido + :ldd_quan_sfrido,
				 quan_scarto=quan_scarto + :ldd_quan_scarto
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga
		and    cod_prodotto=:ls_cod_prodotto;	
		
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Sep","Attenzione! Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
			ii_ok=0
			s_cs_xx.parametri.parametro_b_1=true	
			close(parent)
			return
		end if
	
	else
		if ls_flag_mat_prima='N' then
			
			update avan_produzione_com
			set    quan_utilizzata=quan_utilizzata + :ldd_quan_utilizzata,
					 quan_reso=quan_reso + :ldd_quan_reso,
					 quan_sfrido=quan_sfrido + :ldd_quan_sfrido,
					 quan_scarto=quan_scarto + :ldd_quan_scarto
			where  cod_azienda = :s_cs_xx.cod_azienda and    
			       anno_commessa = :li_anno_commessa  and    
					 num_commessa = :ll_num_commessa    and    
					 prog_riga = :ll_prog_riga 			 and    
					 cod_prodotto = :ls_cod_prodotto    and
					 cod_versione = :ls_cod_versione_dw;
			
			if sqlca.sqlcode <> 0 then	
				g_mb.messagebox("Sep","Attenzione! 523. Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
				ii_ok=0
				s_cs_xx.parametri.parametro_b_1=true	
				close(parent)
				return
			end if
			
			//*************************************************************
			//*****INIZIO SCARICO SEMILAVORATI
			//*************************************************************
			
			if ls_flag_muovi_sl = 'S' then
				
				select anno_reg_sl,
						 num_reg_sl
				into   :ll_anno_reg_sl,
						 :ll_num_reg_sl
				from   avan_produzione_com
				WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
				       anno_commessa = :li_anno_commessa  AND    
						 num_commessa = :ll_num_commessa  	AND    
						 prog_riga = :ll_prog_riga 			AND    
						 cod_prodotto = :ls_cod_prodotto    and
						 cod_versione = :ls_cod_versione_dw;
				
				if sqlca.sqlcode <> 0 then	
					g_mb.messagebox("Sep","Attenzione! 547. Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
				
				if isnull(ll_anno_reg_sl) or isnull(ll_num_reg_sl) then
					g_mb.messagebox("Sep","Attenzione! Non sono stati registrati movimenti di magazzino per i semilavorati. Conviene eliminare tutti i dettagli orari, disattivare e riattivare la commessa imputando nuovamente i dettagli orari.", stopsign!)
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
					
	
				setnull(ls_cod_ubicazione[1])
				setnull(ls_cod_lotto[1])
				setnull(ldt_data_stock[1])
				setnull(ll_prog_stock[1])
				setnull(ls_cod_cliente[1])
				setnull(ls_cod_fornitore[1])
	
				select cod_ubicazione,
						 cod_lotto,
						 data_stock,
						 prog_stock,
						 cod_deposito
				into   :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ldt_data_stock[1],
						 :ll_prog_stock[1],
						 :ls_cod_deposito[1]
				from   mov_magazzino
				where  cod_azienda =:s_cs_xx.cod_azienda
				and    anno_registrazione=:ll_anno_reg_sl
				and	 num_registrazione=:ll_num_reg_sl;
				
				if sqlca.sqlcode <> 0 then	
					g_mb.messagebox("Sep","Attenzione! 586. Errore sul DB:" + sqlca.sqlerrtext, stopsign!)
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
				
				
				if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_prel_sl, ls_cod_prodotto, ls_cod_deposito[], & 
													  ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
													  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
													  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
		
				  g_mb.messagebox("Sep","Si è verificato un errore in creazione destinazione movimenti.",stopsign!)
			
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
	
				if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
													 ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
			
					g_mb.messagebox("Sep","Si è verificato un errore in fase di verifica destinazioni movimenti magazzino.",stopsign!)
			
		
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
				
				luo_mag = create uo_magazzino
	
				li_risposta = luo_mag.uof_movimenti_mag(ldt_oggi, &
													  ls_cod_tipo_mov_prel_sl, &
													  "N", &
													  ls_cod_prodotto, &
													  ldd_quan_utilizzata, &
													  1, &
													  ll_num_commessa, &
													  ldt_oggi, &
													  ls_anno_commessa, &
													  ll_anno_reg_des_mov, &
													  ll_num_reg_des_mov, &
													  ls_cod_deposito[], &
													  ls_cod_ubicazione[], &
													  ls_cod_lotto[], &
													  ldt_data_stock[], &
													  ll_prog_stock[], &
													  ls_cod_fornitore[], &
													  ls_cod_cliente[], &
													  ll_anno_registrazione[], &
													  ll_num_registrazione[])
													  
				destroy luo_mag
	
				if li_risposta=-1 then
					g_mb.messagebox("Sep","Errore su movimenti magazzino, prodotto:" + ls_cod_prodotto &
								 + ", deposito:" + ls_cod_deposito[1] + ", ubicazione:" + ls_cod_ubicazione[1] &
								 + ", cod_lotto:" + ls_cod_lotto[1] + ", data_stock:" + string(ldt_data_stock[1]) &
								 + ", prog_stock:" + string(ll_prog_stock[1]),stopsign!)
		
					ii_ok=0
					s_cs_xx.parametri.parametro_b_1=true	
					close(parent)
					return
				end if
				
			//*************************************************************
			//*****FINE SCARICO SEMILAVORATI
			//*************************************************************
			
			end if
		end if
	end if
next

li_risposta = wf_difetti_out()

if li_risposta< 0 then	
	ii_ok=0
	s_cs_xx.parametri.parametro_b_1=true	
	close(parent)
	return
end if

s_cs_xx.parametri.parametro_d_1 = dw_rileva_quantita_info.getitemnumber(1, "quan_effettiva")  
//dec(st_quan_effettiva.text)
s_cs_xx.parametri.parametro_d_2 = dw_rileva_quantita_info.getitemnumber(1, "quan_scarto")     
//dec(st_quan_scarto.text)

if cbx_problemi.checked = true then
	s_cs_xx.parametri.parametro_s_15 ='S'   //serve per il flag problemi produzione impostato dall funzione f_tempo_lavorazione
else
	s_cs_xx.parametri.parametro_s_15 ='N'
end if

ii_ok=0
	
close(parent)
end event

type cb_annulla from commandbutton within w_rileva_quantita
integer x = 2958
integer y = 2408
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;
ii_ok=0
s_cs_xx.parametri.parametro_b_1=true	
close(parent)
end event

type dw_quantita_utilizzata from uo_cs_xx_dw within w_rileva_quantita
integer x = 18
integer y = 16
integer width = 3314
integer height = 1100
integer taborder = 40
string dataobject = "d_quantita_utilizzata_attivazione"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	if currentrow = 0 then return
	wf_imposta_stock()
end if
end event

event clicked;call super::clicked;if i_extendmode then
	wf_imposta_stock()
end if
end event

type cb_elimina from commandbutton within w_rileva_quantita
integer x = 2926
integer y = 2280
integer width = 366
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina"
end type

event clicked;dw_rileva_difetti_out.deleterow(dw_rileva_difetti_out.getrow())
end event

type cb_inserisci from commandbutton within w_rileva_quantita
integer x = 2537
integer y = 2280
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Inserisci"
end type

event clicked;dw_rileva_difetti_out.insertrow(0)
end event

type cb_cambio_mp from commandbutton within w_rileva_quantita
integer x = 4215
integer y = 184
integer width = 366
integer height = 80
integer taborder = 111
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cambio &MP"
end type

event clicked;string ls_cod_prodotto,ls_des_prodotto,ls_mat_prima
integer li_anno_commessa
long ll_num_commessa,ll_prog_riga

if g_mb.messagebox("Sep","Attenzione! Se si cambia la materia prima non sarà più possibile eliminare i dettagli orari relativi a questa fase. Sei sicuro di proseguire?",information!,YesNo!,2) = 2 then return

ls_mat_prima = dw_quantita_utilizzata.getitemstring(dw_quantita_utilizzata.getrow(),"mat_prima")

if ls_mat_prima <> "S" then 
	g_mb.messagebox("Sep","Attenzione! Non è possibile cambiare i semilavorati",exclamation!)
	return
end if

ls_cod_prodotto = dw_ricerca.getitemstring(dw_ricerca.getrow(),"rs_cod_prodotto")

if isnull(ls_cod_prodotto) or ls_cod_prodotto ="" then
	g_mb.messagebox("Sep","Attenzione! Selezionare il prodotto con cui sostituire la materia prima.",exclamation!)
	return
end if

ls_des_prodotto = dw_ricerca.getitemstring(dw_ricerca.getrow(),"cf_des_prodotto")

dw_quantita_utilizzata.setitem(dw_quantita_utilizzata.getrow(),"cod_semilavorato",ls_cod_prodotto)
dw_quantita_utilizzata.setitem(dw_quantita_utilizzata.getrow(),"des_semilavorato",ls_des_prodotto)

li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2

INSERT INTO mat_prime_commessa
	( cod_azienda,   
	  anno_commessa,   
	  num_commessa,   
	  prog_riga,   
	  cod_prodotto,   
	  anno_reg_reso,   
	  num_reg_reso,   
	  quan_necessaria,   
	  quan_assegnata,   
	  quan_in_produzione,   
	  quan_utilizzata,   
	  cod_tipo_mov_reso,   
	  cod_tipo_mov_sfrido,   
	  anno_reg_sfrido,   
	  num_reg_sfrido,   
	  quan_reso,   
	  quan_sfrido,   
	  quan_scarto,   
	  anno_reg_des_mov_reso,   
	  num_reg_des_mov_reso,   
	  anno_reg_des_mov_sfrido,   
	  num_reg_des_mov_sfrido )  
VALUES ( :s_cs_xx.cod_azienda,   
			:li_anno_commessa,   
			:ll_num_commessa,   
			:ll_prog_riga,   
			:ls_cod_prodotto,   
			null,   
			null,   
			0,   
			0,   
			0,   
			0,   
			null,   
			null,   
			null,   
			null,   
			0,   
			0,   
			0,   
			null,   
			null,   
			null,   
			null )  ;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if

end event

type cb_conferma_stock from commandbutton within w_rileva_quantita
integer x = 2217
integer y = 2280
integer width = 457
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma Stock"
end type

event clicked;// generare tabella mp_com_stock_det_orari 

string   ls_cod_prodotto_mp,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, & 
			ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_orari, & 
			ll_t,ll_num_fasi_aperte
datetime ldt_data_stock
dec{4}   ldd_quan_disponibile,ldd_quan_prelevata,ldd_quan_reso,ldd_quan_sfrido,ldd_quan_scarto
integer  li_anno_commessa

li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_2
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_3

ls_cod_prodotto_mp = dw_quantita_utilizzata.getitemstring(dw_quantita_utilizzata.getrow(),"cod_semilavorato")

select count(*)										// questa parte di controllo serve
into   :ll_num_fasi_aperte							// quando esistono delle materie prime
from   avan_produzione_com							// direttamente collegate sotto al PF
where  cod_azienda=:s_cs_xx.cod_azienda
and 	 anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa
and    prog_riga=:ll_prog_riga
and    flag_fine_fase='N';

if ll_num_fasi_aperte > 0 then		
	select max(prog_orari)
	into   :ll_prog_orari
	from   det_orari_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda   and    
	       anno_commessa = :li_anno_commessa 	  and    
			 num_commessa = :ll_num_commessa	     and    
			 prog_riga=:ll_prog_riga	and    
			 cod_prodotto=:is_cod_prodotto and
			 cod_versione = :is_cod_versione 	and    
			 cod_reparto = :ls_cod_reparto 	and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 flag_inizio = 'N';
else			
	
	ll_prog_orari = il_prog_orari
	
end if

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return
end if

ll_num_righe=dw_scelta_stock.rowcount()

for ll_t=1 to ll_num_righe
	dw_scelta_stock.setrow(ll_t)
	ls_cod_deposito = dw_scelta_stock.getitemstring(ll_t,"cod_deposito")		
	ls_cod_ubicazione = dw_scelta_stock.getitemstring(ll_t,"cod_ubicazione")
	ls_cod_lotto = dw_scelta_stock.getitemstring(ll_t,"cod_lotto")
	ldt_data_stock = dw_scelta_stock.getitemdatetime(ll_t,"data_stock")
	ll_prog_stock = dw_scelta_stock.getitemnumber(ll_t,"prog_stock")
   dw_scelta_stock.setcolumn("quan_prelevata")	
	ldd_quan_prelevata = dec(dw_scelta_stock.gettext())
	dw_scelta_stock.setcolumn("quan_reso")	
	ldd_quan_reso = dec(dw_scelta_stock.gettext())
	dw_scelta_stock.setcolumn("quan_sfrido")	
	ldd_quan_sfrido = dec(dw_scelta_stock.gettext())
	dw_scelta_stock.setcolumn("quan_scarto")	
	ldd_quan_scarto = dec(dw_scelta_stock.gettext())

	if ldd_quan_prelevata > 0 then
		il_controllo++
		INSERT INTO mp_com_stock_det_orari
				( cod_azienda,   
				  anno_commessa,   
				  num_commessa,   
				  cod_prodotto,   
				  prog_riga,   
				  cod_deposito,   
				  cod_ubicazione,   
				  cod_lotto,   
				  data_stock,   
				  prog_stock,   
				  prog_orari,  
				  cod_lavorazione,
				  cod_reparto, 
				  cod_tipo_movimento,   
				  anno_registrazione,   
				  num_registrazione,   
				  quan_utilizzata,
				  quan_reso,
				  quan_sfrido,
				  quan_scarto )  
		  VALUES ( :s_cs_xx.cod_azienda,   
					  :li_anno_commessa,   
					  :ll_num_commessa,   
					  :ls_cod_prodotto_mp,   
					  :ll_prog_riga,   
					  :ls_cod_deposito,   
					  :ls_cod_ubicazione,   
					  :ls_cod_lotto,   
					  :ldt_data_stock,   
					  :ll_prog_stock,   
					  :ll_prog_orari,   
					  :ls_cod_lavorazione,
					  :ls_cod_reparto,
					  null,   
					  null,   
					  null,   
					  :ldd_quan_prelevata,
					  :ldd_quan_reso,
					  :ldd_quan_sfrido,
					  :ldd_quan_scarto )  ;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return
		end if
	end if
next

wf_imposta_stock()
end event

type cb_sblocca from commandbutton within w_rileva_quantita
integer x = 2697
integer y = 2280
integer width = 457
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sblocca Stock"
end type

event clicked;string   ls_cod_prodotto_mp,ls_cod_reparto,ls_cod_lavorazione
long     ll_num_commessa,ll_prog_riga,ll_prog_orari,ll_num_fasi_aperte,ll_num_righe_eliminate
integer  li_anno_commessa

li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_2
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_3

ls_cod_prodotto_mp = dw_quantita_utilizzata.getitemstring(dw_quantita_utilizzata.getrow(),"cod_semilavorato")

select count(*)
into   :ll_num_fasi_aperte
from   avan_produzione_com
where  cod_azienda = :s_cs_xx.cod_azienda		and 	 
		 anno_commessa = :li_anno_commessa		and    
		 num_commessa = :ll_num_commessa			and    
		 prog_riga = :ll_prog_riga					and    
		 flag_fine_fase = 'N';

if ll_num_fasi_aperte > 0 then
	
	select max(prog_orari)
	into   :ll_prog_orari
	from   det_orari_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda		and    
			 anno_commessa = :li_anno_commessa		and    
			 num_commessa = :ll_num_commessa			and    
			 prog_riga = :ll_prog_riga					and    
			 cod_prodotto = :is_cod_prodotto			and    
			 cod_versione = :is_cod_versione			and    
			 cod_reparto = :ls_cod_reparto			and    
			 cod_lavorazione = :ls_cod_lavorazione	and    
			 flag_inizio = 'N';
	
else
	ll_prog_orari = il_prog_orari
	
end if

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return
end if

select count(*)
into   :ll_num_righe_eliminate
from   mp_com_stock_det_orari
where  cod_azienda = :s_cs_xx.cod_azienda		and    
		 anno_commessa = :li_anno_commessa		and    
		 num_commessa = :ll_num_commessa			and    
		 prog_riga = :ll_prog_riga					and    
		 cod_prodotto = :ls_cod_prodotto_mp		and    
		 prog_orari = :ll_prog_orari;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return
end if

delete
from   mp_com_stock_det_orari
where  cod_azienda = :s_cs_xx.cod_azienda		and    
		 anno_commessa = :li_anno_commessa		and    
		 num_commessa = :ll_num_commessa			and    
		 prog_riga = :ll_prog_riga					and    
		 cod_prodotto = :ls_cod_prodotto_mp		and    
		 prog_orari = :ll_prog_orari;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return
end if

il_controllo = il_controllo - ll_num_righe_eliminate

if il_controllo<0 then
	g_mb.messagebox("Sep","Si è verificato un errore sull'impostazione degli stock. Chiudere la maschera con il tasto annulla e ripetere l'operazione.",stopsign!)
	return
end if

wf_imposta_stock()

end event

type dw_ricerca from u_dw_search within w_rileva_quantita
event ue_key pbm_dwnkey
integer x = 3355
integer y = 96
integer width = 1253
integer height = 372
integer taborder = 10
string dataobject = "d_ricerca_cambio_materia_prima"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")
end choose
end event

type dw_scelta_stock from uo_cs_xx_dw within w_rileva_quantita
integer x = 55
integer y = 1256
integer width = 3237
integer height = 1000
integer taborder = 60
string dataobject = "d_scelta_stock"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

type dw_lista_stock from uo_cs_xx_dw within w_rileva_quantita
event clicked pbm_dwnlbuttonclk
event rowfocuschanged pbm_dwnrowchange
integer x = 55
integer y = 1256
integer width = 3237
integer height = 1000
integer taborder = 50
string dataobject = "d_mp_com_stock_de_lista_2"
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_num_commessa,ll_prog_riga,ll_prog_orari,ll_num_fasi_aperte
string ls_cod_prodotto_mp,ls_cod_lavorazione,ls_cod_reparto
integer li_anno_commessa

li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_ul_1
ll_prog_riga = s_cs_xx.parametri.parametro_ul_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_2
ls_cod_lavorazione = s_cs_xx.parametri.parametro_s_3

ls_cod_prodotto_mp = dw_quantita_utilizzata.getitemstring(dw_quantita_utilizzata.getrow(),"cod_semilavorato")

select count(*)
into   :ll_num_fasi_aperte
from   avan_produzione_com
where  cod_azienda=:s_cs_xx.cod_azienda
and 	 anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa
and    prog_riga=:ll_prog_riga
and    flag_fine_fase='N';

if ll_num_fasi_aperte > 0 then
	select max(prog_orari)
	into   :ll_prog_orari
	from   det_orari_produzione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga
	and    cod_prodotto=:is_cod_prodotto
	and    cod_prodotto = :is_cod_prodotto
	and    cod_reparto =:ls_cod_reparto
	and    cod_lavorazione=:ls_cod_lavorazione
	and    flag_inizio = 'N';
else
	ll_prog_orari = il_prog_orari
	
end if


if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return 0
end if

l_Error = Retrieve(s_cs_xx.cod_azienda,li_anno_commessa,ll_num_commessa,ls_cod_prodotto_mp,ll_prog_riga,ll_prog_orari)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_folder from u_folder within w_rileva_quantita
integer x = 18
integer y = 1124
integer width = 3319
integer height = 1256
integer taborder = 30
end type

type dw_rileva_difetti_out from datawindow within w_rileva_quantita
integer x = 55
integer y = 1256
integer width = 3237
integer height = 1000
integer taborder = 70
string title = "none"
string dataobject = "d_rileva_difetti_out"
boolean border = false
boolean livescroll = true
end type

