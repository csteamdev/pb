﻿$PBExportHeader$w_commesse_fuori_data.srw
$PBExportComments$Window commesse fuori data consegna
forward
global type w_commesse_fuori_data from w_cs_xx_principale
end type
type dw_elenco_commesse_schedulate from uo_cs_xx_dw within w_commesse_fuori_data
end type
type dw_commesse_fuori_data from uo_cs_xx_dw within w_commesse_fuori_data
end type
type st_1 from statictext within w_commesse_fuori_data
end type
type st_2 from statictext within w_commesse_fuori_data
end type
end forward

global type w_commesse_fuori_data from w_cs_xx_principale
int Width=2999
int Height=1437
boolean TitleBar=true
string Title="Commesse Schedulate"
dw_elenco_commesse_schedulate dw_elenco_commesse_schedulate
dw_commesse_fuori_data dw_commesse_fuori_data
st_1 st_1
st_2 st_2
end type
global w_commesse_fuori_data w_commesse_fuori_data

on w_commesse_fuori_data.create
int iCurrent
call w_cs_xx_principale::create
this.dw_elenco_commesse_schedulate=create dw_elenco_commesse_schedulate
this.dw_commesse_fuori_data=create dw_commesse_fuori_data
this.st_1=create st_1
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_elenco_commesse_schedulate
this.Control[iCurrent+2]=dw_commesse_fuori_data
this.Control[iCurrent+3]=st_1
this.Control[iCurrent+4]=st_2
end on

on w_commesse_fuori_data.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_elenco_commesse_schedulate)
destroy(this.dw_commesse_fuori_data)
destroy(this.st_1)
destroy(this.st_2)
end on

event pc_setwindow;call super::pc_setwindow;dw_commesse_fuori_data.set_dw_options(sqlca,pcca.null_object,c_nonew + & 
													 c_nodelete + c_nomodify +  & 
													 c_disablecc,c_disableccinsert)

dw_elenco_commesse_schedulate.set_dw_options(sqlca,pcca.null_object,c_nonew + & 
													 c_nodelete + c_nomodify +  & 
													 c_disablecc,c_disableccinsert)
end event

type dw_elenco_commesse_schedulate from uo_cs_xx_dw within w_commesse_fuori_data
event pcd_retrieve pbm_custom60
int X=23
int Y=101
int Width=823
int Height=1221
string DataObject="d_elenco_commesse_schedulate"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_commesse_fuori_data from uo_cs_xx_dw within w_commesse_fuori_data
int X=869
int Y=101
int Width=2081
int Height=1221
string DataObject="d_commesse_fuori_data"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type st_1 from statictext within w_commesse_fuori_data
int X=869
int Y=21
int Width=1719
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Elenco Commesse con data fine lavori maggiore di data consegna"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_commesse_fuori_data
int X=23
int Y=21
int Width=823
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Elenco Commesse Schedulate"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

