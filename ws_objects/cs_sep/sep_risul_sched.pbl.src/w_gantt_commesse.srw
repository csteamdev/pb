﻿$PBExportHeader$w_gantt_commesse.srw
$PBExportComments$Window per grafico Gantt Commesse
forward
global type w_gantt_commesse from w_grafici
end type
type em_1 from editmask within w_gantt_commesse
end type
type em_2 from editmask within w_gantt_commesse
end type
type cbx_solo from checkbox within w_gantt_commesse
end type
end forward

global type w_gantt_commesse from w_grafici
boolean TitleBar=true
string Title="Gantt Commesse"
em_1 em_1
em_2 em_2
cbx_solo cbx_solo
end type
global w_gantt_commesse w_gantt_commesse

type variables
datetime id_data_inf, id_data_sup
end variables

on w_gantt_commesse.create
int iCurrent
call w_grafici::create
this.em_1=create em_1
this.em_2=create em_2
this.cbx_solo=create cbx_solo
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=em_1
this.Control[iCurrent+2]=em_2
this.Control[iCurrent+3]=cbx_solo
end on

on w_gantt_commesse.destroy
call w_grafici::destroy
destroy(this.em_1)
destroy(this.em_2)
destroy(this.cbx_solo)
end on

type cb_2 from w_grafici`cb_2 within w_gantt_commesse
boolean BringToTop=true
end type

event cb_2::clicked;call super::clicked;long 	  ll_num_commessa
integer li_anno_commessa,li_durata,li_num_giorni,li_t
datetime    ld_data_inizio,ld_data_fine,ld_data_min,ld_data_max

ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))
id_data_inf = ld_data_inizio
id_data_sup = ld_data_fine

li_num_giorni=DaysAfter(date(ld_data_inizio), date(ld_data_fine))

ch.Plot.Axis(1).dateScale.minimum =date(ld_data_inizio)
ch.Plot.Axis(1).dateScale.Maximum =date(ld_data_fine)
ch.Plot.Axis(1).dateScale.Majint =1
ch.title.vtfont.size=10
ch.title.text="Distribuzione temporale commesse"
ch.legend.vtfont.size=9
ch.legend.vtfont.style=0
//ch.Plot.Axis(1).CategoryScale.auto=true
ch.Plot.Axis(1).Labels.Item(1).VtFont.size =8
ch.Plot.Axis(1).Labels.Item(1).VtFont.style =0
//ch.Plot.Axis(1).Labels.Item(1).auto=true
ch.Plot.Axis(1).Labels.Item(1).format="dd/mm"
ch.dataGrid.rowLabel( 1,1,"")

declare  righe_com cursor for 
select   anno_commessa,
		   num_commessa
from     det_cal_attivita
where    cod_azienda=:s_cs_xx.cod_azienda
and      data_giorno between:ld_data_inizio 
and      :ld_data_fine
group by anno_commessa,
			num_commessa;

open righe_com;
li_t=1
do while 1=1
	fetch righe_com 
	into  :li_anno_commessa,
		   :ll_num_commessa;

	if sqlca.sqlcode<>0  then exit

	ch.DataGrid.SetSize(1,1,1,li_t+1)

	select min(data_giorno),
			 max(data_giorno)
	into   :ld_data_min,
			 :ld_data_max
	from   det_cal_attivita
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;

	li_durata=DaysAfter(date(ld_data_min), date(ld_data_max))
	if li_durata < 1 then li_durata = 1
	ch.dataGrid.columnLabel( li_t,1,string(ll_num_commessa))

	if (ld_data_min < ld_data_inizio) and cbx_solo.checked=false then
		ch.Plot.Axis(1).dateScale.minimum =ld_data_min	
		ld_data_inizio = ld_data_min
		id_data_inf=ld_data_min
	end if

	if (ld_data_max > ld_data_fine) and cbx_solo.checked=false then
		ch.Plot.Axis(1).dateScale.maximum =ld_data_max
		ld_data_fine = ld_data_max
		id_data_sup = ld_data_max
	end if

	ch.datagrid.SetData (1, li_t, daysafter(1900-01-01,date(ld_data_min))+2, False)	
	li_t++
	ch.datagrid.SetData (1, li_t, li_durata, False)	
	li_t++
	
loop

close righe_com;

end event

type ole_1 from w_grafici`ole_1 within w_gantt_commesse
int TabOrder=40
boolean BringToTop=true
end type

event ole_1::seriesselected;call super::seriesselected;long 	  ll_num_commessa
integer li_anno_commessa,li_durata,li_num_giorni,li_t
datetime    ld_data_inizio,ld_data_fine,ld_data_min,ld_data_max,ld_nuovo_min,ld_nuovo_max
string  ls_cod_cat_attrezzature,ls_des_cat

ll_num_commessa = long(ch.plot.SeriesCollection.item(series).SeriesLabel.text)
li_anno_commessa = integer(year(date(em_1.text)))
ld_data_inizio = id_data_inf
ld_data_fine = id_data_sup

ch.title.vtfont.size=10
ch.title.text="Dettaglio commessa nr. " + string(ll_num_commessa) + " anno " + string(li_anno_commessa)
ch.legend.vtfont.size=8

declare  righe_com cursor for 
select   cod_cat_attrezzature
from     det_cal_attivita
where    cod_azienda=:s_cs_xx.cod_azienda
and      anno_commessa=:li_anno_commessa
and      num_commessa=:ll_num_commessa
group by cod_cat_attrezzature;

open righe_com;
li_t=1
do while 1=1
	fetch righe_com 
	into  :ls_cod_cat_attrezzature;

	if sqlca.sqlcode<>0  then exit
	
	ch.DataGrid.SetSize(1,1,1,li_t+1)

	select min(data_giorno),
			 max(data_giorno)
	into   :ld_data_min,
			 :ld_data_max
	from   det_cal_attivita
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;

	if li_t=1 then
		ld_nuovo_min = ld_data_min
		ld_nuovo_max = ld_data_max
	end if

	if ld_nuovo_min > ld_data_min then ld_nuovo_min = ld_data_min
	if ld_nuovo_max < ld_data_max then ld_nuovo_max = ld_data_max
	
	li_durata=DaysAfter(date(ld_data_min), date(ld_data_max))
	if li_durata < 1 then li_durata = 1

	select des_cat_attrezzature
	into   :ls_des_cat
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;

	ch.dataGrid.columnLabel( li_t,1,string(ls_des_cat))

	if (ld_data_min < ld_data_inizio) then
		ch.Plot.Axis(1).dateScale.minimum =ld_data_min	
		ld_data_inizio = ld_data_min
	end if

	if (ld_data_max > ld_data_fine) then
		ch.Plot.Axis(1).dateScale.maximum =ld_data_max
		ld_data_fine = ld_data_max
	end if

	ch.datagrid.SetData (1, li_t, daysafter(1900-01-01,date(ld_data_min))+2, False)	
	li_t++
	ch.datagrid.SetData (1, li_t, li_durata, False)	
	li_t++
	
loop

close righe_com;

ch.Plot.Axis(1).dateScale.minimum =ld_nuovo_min	
ch.Plot.Axis(1).dateScale.maximum =ld_nuovo_max
end event

type em_1 from editmask within w_gantt_commesse
int X=435
int Y=1441
int Width=389
int Height=81
int TabOrder=30
boolean BringToTop=true
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_2 from editmask within w_gantt_commesse
int X=846
int Y=1441
int Width=389
int Height=81
int TabOrder=20
boolean BringToTop=true
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_solo from checkbox within w_gantt_commesse
int X=1281
int Y=1441
int Width=622
int Height=77
boolean BringToTop=true
string Text="solo periodo indicato"
BorderStyle BorderStyle=StyleLowered!
boolean LeftText=true
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

