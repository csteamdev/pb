﻿$PBExportHeader$w_gantt_complessivo_g.srw
$PBExportComments$Gantt complessivo impegno attrezzature giornaliero
forward
global type w_gantt_complessivo_g from w_grafici
end type
type em_data from editmask within w_gantt_complessivo_g
end type
type cb_scelta from commandbutton within w_gantt_complessivo_g
end type
type cb_ok from commandbutton within w_gantt_complessivo_g
end type
type dw_lista_cat_attrezzature from uo_cs_xx_dw within w_gantt_complessivo_g
end type
type cb_salva from commandbutton within w_gantt_complessivo_g
end type
type st_1 from statictext within w_gantt_complessivo_g
end type
type cb_stampa from commandbutton within w_gantt_complessivo_g
end type
type sle_path_documento from singlelineedit within w_gantt_complessivo_g
end type
type st_3 from statictext within w_gantt_complessivo_g
end type
end forward

global type w_gantt_complessivo_g from w_grafici
int Width=3210
int Height=1741
boolean TitleBar=true
string Title="Gantt Complessivo"
em_data em_data
cb_scelta cb_scelta
cb_ok cb_ok
dw_lista_cat_attrezzature dw_lista_cat_attrezzature
cb_salva cb_salva
st_1 st_1
cb_stampa cb_stampa
sle_path_documento sle_path_documento
st_3 st_3
end type
global w_gantt_complessivo_g w_gantt_complessivo_g

type variables
datetime id_data_inf, id_data_sup
string is_cod_cat_atr[]
string is_des_cat_atr[]
integer ii_num_cat
end variables

on w_gantt_complessivo_g.create
int iCurrent
call w_grafici::create
this.em_data=create em_data
this.cb_scelta=create cb_scelta
this.cb_ok=create cb_ok
this.dw_lista_cat_attrezzature=create dw_lista_cat_attrezzature
this.cb_salva=create cb_salva
this.st_1=create st_1
this.cb_stampa=create cb_stampa
this.sle_path_documento=create sle_path_documento
this.st_3=create st_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=em_data
this.Control[iCurrent+2]=cb_scelta
this.Control[iCurrent+3]=cb_ok
this.Control[iCurrent+4]=dw_lista_cat_attrezzature
this.Control[iCurrent+5]=cb_salva
this.Control[iCurrent+6]=st_1
this.Control[iCurrent+7]=cb_stampa
this.Control[iCurrent+8]=sle_path_documento
this.Control[iCurrent+9]=st_3
end on

on w_gantt_complessivo_g.destroy
call w_grafici::destroy
destroy(this.em_data)
destroy(this.cb_scelta)
destroy(this.cb_ok)
destroy(this.dw_lista_cat_attrezzature)
destroy(this.cb_salva)
destroy(this.st_1)
destroy(this.cb_stampa)
destroy(this.sle_path_documento)
destroy(this.st_3)
end on

event pc_setwindow;call super::pc_setwindow;dw_lista_cat_attrezzature.set_dw_options(sqlca,pcca.null_object,c_multiselect + &
                                         c_nonew + c_nomodify + c_nodelete,c_default)


ole_1.visible=false
cb_scelta.enabled=false
cb_2.enabled=false
em_data.enabled=false
cb_salva.enabled = false
cb_stampa.enabled = false


end event

type cb_2 from w_grafici`cb_2 within w_gantt_complessivo_g
int Y=1541
int TabOrder=40
boolean BringToTop=true
end type

event cb_2::clicked;call super::clicked;long 	  ll_num_commessa,ll_ar_num_commessa[],ll_righe,ll_durata
integer li_anno_commessa,li_num_giorni,li_t,li_r,li_i
datetime ld_data_inizio,ld_data_fine,ld_data_min,ld_data_max,ldt_ora_min,ldt_ora_max
string  ls_cod_cat_attrezzature,ls_des_cat_attrezzature
boolean lb_flag

ld_data_inizio = datetime(date(em_data.text))
id_data_inf = ld_data_inizio
id_data_sup = ld_data_inizio

li_num_giorni = 1
ch.Plot.Axis(1).dateScale.minimum =00:00:00
ch.Plot.Axis(1).dateScale.maximum =10:00:00
ch.Plot.Axis(1).dateScale.Majint =1
ch.title.vtfont.size=10
ch.title.text="Distribuzione temporale carico macchine/commesse"
ch.legend.vtfont.size=9
ch.legend.vtfont.style=0
//ch.object.legend.text = "Commesse:"
//ch.Plot.Axis(1).CategoryScale.auto=true
ch.Plot.Axis(1).Labels.Item(1).VtFont.size =8
ch.Plot.Axis(1).Labels.Item(1).VtFont.style =0
ch.Plot.Axis(1).Labels.Item(1).auto=true
ch.Plot.Axis(1).Labels.Item(1).format="hh:mm"
ch.Plot.Axis(0).Labels.Item(1).VtFont.size =6
ch.Plot.Axis(0).Labels.Item(1).VtFont.style =0
//ch.Plot.Axis(0).Labels.Item(1).auto=true

li_t=1
li_r = 1

for ll_righe=1 to ii_num_cat

	ls_cod_cat_attrezzature=is_cod_cat_atr[ll_righe]	
	ls_des_cat_attrezzature=is_des_cat_atr[ll_righe]
	ch.DataGrid.rowcount=li_r

	declare  righe_com cursor for 
	select   anno_commessa,
			   num_commessa
	from     det_cal_attivita
	where    cod_azienda=:s_cs_xx.cod_azienda
	and      data_giorno=:ld_data_inizio 
	and      cod_cat_attrezzature=:ls_cod_cat_attrezzature
	group by anno_commessa,
				num_commessa;

	open righe_com;
	
	do while 1=1
		fetch righe_com 
		into  :li_anno_commessa,
			   :ll_num_commessa;
	
		if sqlca.sqlcode<>0  then exit

		lb_flag=false
	
		for li_i = 1 to upperbound(ll_ar_num_commessa)
			if ll_ar_num_commessa[li_i] = ll_num_commessa then 
				lb_flag=true
				li_t=li_i
				exit
			end if
		next

		ll_ar_num_commessa[li_t] = ll_num_commessa	

		if lb_flag=false then 
			li_t=upperbound(ll_ar_num_commessa)
			ch.DataGrid.columncount=li_t+1
		end if

		select min(ora_inizio),
				 max(ora_fine)
		into   :ldt_ora_min,
				 :ldt_ora_max
		from   det_cal_attivita
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;

		ll_durata=secondsAfter(time(ldt_ora_min), time(ldt_ora_max))
		if ll_durata < 1 then ll_durata = 1
		ch.dataGrid.columnLabel( li_t,1,string(ll_num_commessa))

//		if (ldt_ora_min < ldt_data_inizio) and cbx_solo.checked=false then
//			ch.Plot.Axis(1).dateScale.minimum =ldt_ora_min	
//			ld_data_inizio = ld_data_min
//			id_data_inf=ld_data_min
//		end if

//		if (ld_data_max > ld_data_fine) and cbx_solo.checked=false then
//			ch.Plot.Axis(1).dateScale.maximum =ldt_ora_max
//			ld_data_fine = ld_data_max
//			id_data_sup = ld_data_max
//		end if

		ch.datagrid.SetData(li_r, li_t, secondsafter(00:00:00,time(ldt_ora_min)), False)	
		li_t++
		ch.datagrid.SetData (li_r, li_t, ll_durata, False)	
		//if lb_flag=false then 
		li_t++
	
	loop

	close righe_com;

	ch.dataGrid.rowLabel( li_r,1, left(ls_des_cat_attrezzature,15))	
	li_r++

next
//ch.refresh
parent.resize((parent.width + 10),(parent.height +10 ))


end event

type ole_1 from w_grafici`ole_1 within w_gantt_complessivo_g
int Width=3132
int TabOrder=90
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
end type

event ole_1::seriesselected;call super::seriesselected;//long 	  ll_num_commessa
//integer li_anno_commessa,li_durata,li_num_giorni,li_t
//datetime    ld_data_inizio,ld_data_fine,ld_data_min,ld_data_max,ld_nuovo_min,ld_nuovo_max
//string  ls_cod_cat_attrezzature,ls_des_cat
//
//ll_num_commessa = long(ch.plot.SeriesCollection.item(series).SeriesLabel.text)
//li_anno_commessa = integer(year(date(em_1.text)))
//ld_data_inizio = id_data_inf
//ld_data_fine = id_data_sup
//
//ch.title.vtfont.size=10
//ch.title.text="Dettaglio commessa nr. " + string(ll_num_commessa) + " anno " + string(li_anno_commessa)
//ch.legend.vtfont.size=8
//
//declare  righe_com cursor for 
//select   cod_cat_attrezzature
//from     det_cal_attivita
//where    cod_azienda=:s_cs_xx.cod_azienda
//and      anno_commessa=:li_anno_commessa
//and      num_commessa=:ll_num_commessa
//group by cod_cat_attrezzature;
//
//open righe_com;
//li_t=1
//do while 1=1
//	fetch righe_com 
//	into  :ls_cod_cat_attrezzature;
//
//	if sqlca.sqlcode<>0  then exit
//	
//	ch.DataGrid.SetSize(1,1,1,li_t+1)
//
//	select min(data_giorno),
//			 max(data_giorno)
//	into   :ld_data_min,
//			 :ld_data_max
//	from   det_cal_attivita
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    anno_commessa=:li_anno_commessa
//	and    num_commessa=:ll_num_commessa
//	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;
//
//	if li_t=1 then
//		ld_nuovo_min = ld_data_min
//		ld_nuovo_max = ld_data_max
//	end if
//
//	if ld_nuovo_min > ld_data_min then ld_nuovo_min = ld_data_min
//	if ld_nuovo_max < ld_data_max then ld_nuovo_max = ld_data_max
//	
//	li_durata=DaysAfter(date(ld_data_min), date(ld_data_max))
//	if li_durata < 1 then li_durata = 1
//
//	select des_cat_attrezzature
//	into   :ls_des_cat
//	from   tab_cat_attrezzature
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;
//
//	ch.dataGrid.columnLabel( li_t,1,string(ls_des_cat))
//
//	if (ld_data_min < ld_data_inizio) then
//		ch.Plot.Axis(1).dateScale.minimum =ld_data_min	
//		ld_data_inizio = ld_data_min
//	end if
//
//	if (ld_data_max > ld_data_fine) then
//		ch.Plot.Axis(1).dateScale.maximum =ld_data_max
//		ld_data_fine = ld_data_max
//	end if
//
//	ch.datagrid.SetData (1, li_t, daysafter(1900-01-01,date(ld_data_min))+2, False)	
//	li_t++
//	ch.datagrid.SetData (1, li_t, li_durata, False)	
//	li_t++
//	
//loop
//
//close righe_com;
//
//ch.Plot.Axis(1).dateScale.minimum =ld_nuovo_min	
//ch.Plot.Axis(1).dateScale.maximum =ld_nuovo_max
end event

type em_data from editmask within w_gantt_complessivo_g
int X=275
int Y=1441
int Width=389
int Height=81
int TabOrder=10
boolean BringToTop=true
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_scelta from commandbutton within w_gantt_complessivo_g
int X=2401
int Y=1541
int Width=366
int Height=81
int TabOrder=70
boolean BringToTop=true
string Text="&Scelta Attr."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;
ole_1.visible=false
cb_scelta.enabled=false
cb_2.enabled=false
em_data.enabled=false
cb_ok.enabled=true
cb_salva.enabled = false
cb_stampa.enabled = false

end event

type cb_ok from commandbutton within w_gantt_complessivo_g
int X=2789
int Y=1541
int Width=366
int Height=81
int TabOrder=80
boolean BringToTop=true
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_selected[],ll_riga

dw_lista_cat_attrezzature.get_selected_rows(ll_selected[])

ii_num_cat=upperbound(ll_selected[])

for ll_riga = 1 to upperbound(ll_selected[])
	is_cod_cat_atr[ll_riga] = dw_lista_cat_attrezzature.getitemstring(ll_selected[ll_riga],"cod_cat_attrezzature")
	is_des_cat_atr[ll_riga] = dw_lista_cat_attrezzature.getitemstring(ll_selected[ll_riga],"des_cat_attrezzature")
next	

ole_1.visible=true
cb_scelta.enabled=true
cb_2.enabled=true
em_data.enabled=true
cb_ok.enabled=false
cb_salva.enabled = true
cb_stampa.enabled = true

end event

type dw_lista_cat_attrezzature from uo_cs_xx_dw within w_gantt_complessivo_g
event pcd_retrieve pbm_custom60
int X=23
int Y=21
int Width=3132
int Height=1401
int TabOrder=20
boolean BringToTop=true
string DataObject="d_lista_cat_attrezzature"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type cb_salva from commandbutton within w_gantt_complessivo_g
int X=2012
int Y=1541
int Width=366
int Height=81
int TabOrder=60
boolean BringToTop=true
string Text="Sal&va"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_nome_documento, ls_nome

//integer li_risposta
//
//li_risposta = GetFileSaveName("Salva con nome",  & 
//	ls_nome_documento, ls_nome, "BMP",  &
//	"Bit Map (*.BMP),*.BMP")

ls_nome_documento = sle_path_documento.text
ch.WritePictureToFile(ls_nome_documento,1,1)

//IF li_risposta = 1 THEN
//	ch.WritePictureToFile(ls_nome_documento,1,1)
//end if
end event

type st_1 from statictext within w_gantt_complessivo_g
int X=46
int Y=1441
int Width=229
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Data:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_stampa from commandbutton within w_gantt_complessivo_g
int X=1623
int Y=1541
int Width=366
int Height=81
int TabOrder=50
boolean BringToTop=true
string Text="Stam&pa"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;ch.PrintInformation.scaleType = 2
ch.PrintChart
end event

type sle_path_documento from singlelineedit within w_gantt_complessivo_g
int X=1623
int Y=1441
int Width=1532
int Height=81
int TabOrder=30
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_gantt_complessivo_g
int X=801
int Y=1441
int Width=801
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Path Salvataggio Immagine:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

