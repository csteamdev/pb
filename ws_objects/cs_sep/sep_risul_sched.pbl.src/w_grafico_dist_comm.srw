﻿$PBExportHeader$w_grafico_dist_comm.srw
$PBExportComments$Grafico distribuzione giorni/ore commessa
forward
global type w_grafico_dist_comm from w_cs_xx_principale
end type
type dw_grafico from uo_cs_graph within w_grafico_dist_comm
end type
type cb_1 from uo_cb_close within w_grafico_dist_comm
end type
end forward

global type w_grafico_dist_comm from w_cs_xx_principale
integer width = 3538
integer height = 1892
string title = "Impegno Attrezzature"
dw_grafico dw_grafico
cb_1 cb_1
end type
global w_grafico_dist_comm w_grafico_dist_comm

type variables

end variables

event pc_setwindow;call super::pc_setwindow;//dw_grafico.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete ,c_default)

LONG  l_Error
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_dw_garfico_commessa_atrz"
lds_dati.settransobject(sqlca)
l_Error = lds_dati.Retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_ul_1,s_cs_xx.parametri.parametro_i_1)
//l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_ul_1,s_cs_xx.parametri.parametro_i_1)

//IF l_Error < 0 THEN
//   PCCA.Error = c_Fatal
//END IF

dw_grafico.ids_data = lds_dati
dw_grafico.ib_datastore = true

dw_grafico.is_dtm[1] = "data_giorno"
dw_grafico.is_dtm[2] = "ora_inizio"
dw_grafico.is_dtm[3] = "ora_fine"
dw_grafico.is_str[1] = "cod_cat_attrezzature"
dw_grafico.is_str[2] = "anag_attrezzature_descrizione"

dw_grafico.is_source_categoria_col = "data_giorno"

dw_grafico.is_categoria_col = "dtm_1"
dw_grafico.is_exp_valore = "(hour(dtm_3)*60 + minute(dtm_3) - hour(dtm_2)*60 - minute(dtm_2))/60"
dw_grafico.is_serie_col = "str_2"

dw_grafico.is_titolo = "Distribuzione Commessa/Cat. Attrz."
dw_grafico.is_label_categoria= "Giorni"
dw_grafico.is_label_valori = "Ore Impegno"

dw_grafico.ii_scala = 1
dw_grafico.ii_tipo_grafico = 9  //Col 3D Obj
dw_grafico.il_backcolor = 12632256 //silver

dw_grafico.uof_retrieve( )

destroy lds_dati
end event

on w_grafico_dist_comm.create
int iCurrent
call super::create
this.dw_grafico=create dw_grafico
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_grafico
this.Control[iCurrent+2]=this.cb_1
end on

on w_grafico_dist_comm.destroy
call super::destroy
destroy(this.dw_grafico)
destroy(this.cb_1)
end on

type dw_grafico from uo_cs_graph within w_grafico_dist_comm
integer x = 23
integer y = 24
integer width = 3447
integer height = 1628
integer taborder = 10
end type

type cb_1 from uo_cb_close within w_grafico_dist_comm
integer x = 3086
integer y = 1680
integer width = 384
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

