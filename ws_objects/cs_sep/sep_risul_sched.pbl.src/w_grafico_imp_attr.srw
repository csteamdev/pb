﻿$PBExportHeader$w_grafico_imp_attr.srw
$PBExportComments$Grafico impegno attrezzature
forward
global type w_grafico_imp_attr from w_cs_xx_principale
end type
type dw_grafico from uo_cs_graph within w_grafico_imp_attr
end type
type cb_1 from uo_cb_close within w_grafico_imp_attr
end type
type ddlb_cat_attrezzature from dropdownlistbox within w_grafico_imp_attr
end type
type ddlb_attivita from dropdownlistbox within w_grafico_imp_attr
end type
type ddlb_attrezzature from dropdownlistbox within w_grafico_imp_attr
end type
type st_1 from statictext within w_grafico_imp_attr
end type
type st_2 from statictext within w_grafico_imp_attr
end type
type st_3 from statictext within w_grafico_imp_attr
end type
type st_4 from statictext within w_grafico_imp_attr
end type
type em_1 from editmask within w_grafico_imp_attr
end type
type st_5 from statictext within w_grafico_imp_attr
end type
type em_2 from editmask within w_grafico_imp_attr
end type
end forward

global type w_grafico_imp_attr from w_cs_xx_principale
integer width = 3566
integer height = 1796
string title = "Impegno Attrezzature"
dw_grafico dw_grafico
cb_1 cb_1
ddlb_cat_attrezzature ddlb_cat_attrezzature
ddlb_attivita ddlb_attivita
ddlb_attrezzature ddlb_attrezzature
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
em_1 em_1
st_5 st_5
em_2 em_2
end type
global w_grafico_imp_attr w_grafico_imp_attr

type variables
string is_cod_attivita
string is_cod_cat_attrezzature
string is_cod_attrezzatura
datetime id_data_giorno
end variables

forward prototypes
public subroutine wf_retrieve_grafico (string fs_cod_attivita, string fs_cod_cat_attrez, string fs_cod_attrez, datetime fdt_data_inizio, datetime fdt_data_fine)
end prototypes

public subroutine wf_retrieve_grafico (string fs_cod_attivita, string fs_cod_cat_attrez, string fs_cod_attrez, datetime fdt_data_inizio, datetime fdt_data_fine);
//SEZIONE 1 ###############################################################################
//creazione e popolamento del datastore di appoggio
//N.B. potrebbe essere usato anche una datawindow al posto di un datastore, purchè
//     questo venga specificato nell'oggetto grafico (vedi più avanti SEZIONE 2)
//----------------------------------------------------------------------------------------
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_grafico_cal_attr" //immettere qui il nome del datastore
lds_dati.settransobject(sqlca)
lds_dati.Retrieve(	s_cs_xx.cod_azienda, &
							fs_cod_attivita, &
							fs_cod_cat_attrez, &
							fs_cod_attrez, &
							fdt_data_inizio, &
							fdt_data_fine )

//########################################################################################

//SEZIONE 2 ###############################################################################
//impostazione dell'oggetto grafico
//----------------------------------------------------------------------------------------

//si vuole usare un datastore (valore di default è TRUE, mettere FALSE se si tratta di datawindow)
//dw_grafico è il nome dato al controllo grafico sulla finestra
dw_grafico.ib_datastore = true

//impostare l'oggetto datastore (o datawindow: dw_grafico.idw_data )
dw_grafico.ids_data = lds_dati

//impostazione corrispondenza campi datastore (o datawindow) e grafico
/*ESEMPIO 1
	se il datastore ha 3 campi stringa 2 datetime e 1 numerico (esempio long, decimal):

	dw_grafico.is_str[1] = "nome prima colonna del ds di tipo stringa"
	dw_grafico.is_str[2] = "nome seconda colonna del ds di tipo stringa"
	dw_grafico.is_str[3] = "nome terza colonna del ds di tipo stringa"
	dw_grafico.is_dtm[1] = "nome prima colonna del ds di tipo datetime"
	dw_grafico.is_dtm[2] = "nome seconda colonna del ds di tipo datetime"
	dw_grafico.is_num[1] = "nome prima colonna del ds di tipo long, decimal, ecc..."

N.B. non è importante l'ordine di specifica dei vari campi
*/
dw_grafico.is_dtm[1] = "data_giorno"
dw_grafico.is_str[1] = "cod_attrezzatura"
dw_grafico.is_str[2] = "anag_attrezzature_descrizione"
dw_grafico.is_num[1] = "num_ore_impegnate"

//Questo serve per impostare l'ordinamento corretto della categoria prima di popolare il grafico
dw_grafico.is_source_categoria_col = "data_giorno" //nome colonna del datastore che corrisponde alla categoria

//impostazione della categoria
/*ESEMPI
	se la categoria è una colonna di tipo string del datastore indicare "str_i"
	dove i è la posizione nell'array dw_grafico.is_istr[]

	se la categoria è una colonna di tipo datetime del datastore indicare "dtm_j"
	dove j è la posizione nell'array dw_grafico.is_dtm[]

	se la categoria è una colonna di tipo numerico del datastore indicare "num_k"
	dove k è la posizione nell'array dw_grafico.is_num[]
*/
dw_grafico.is_categoria_col = "dtm_1"

//impostazione della categoria (analogo a quanto fatto per la categoria)
/*
	essa può essere una colonna numerica o una espressione numerica
	ESEMPI
		"num_2"
		"num_1 - num_2"
		"(hour(dtm_3)*60 + minute(dtm_3) - hour(dtm_2)*60 - minute(dtm_2))/60"
*/
dw_grafico.is_exp_valore = "num_1" //default è "num_1"

//impostazione della categoria (analogo a quanto fatto per la categoria)
//lasciare stringa vuota se non si vuole gestire il grafico per serie
dw_grafico.is_serie_col = "str_2"

//altre impostazioni del grafico
dw_grafico.is_titolo = "Impegno Attrezzature" //Titolo del grafico
dw_grafico.is_label_categoria= "giorni" //Etichetta sull'asse della categoria (Default ascissa)
dw_grafico.is_label_valori = "ore impegnate" //Etichetta sull'asse dei valori (Default valori)

//tipo scala: 1 se lineare, 2 se logaritmica in base 10
dw_grafico.ii_scala = 1

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_grafico.ii_tipo_grafico = 9

//colore di sfondo del grafico
//per il bianco (DEFAULT)	: 16777215
//per il silver			: 12632256
dw_grafico.il_backcolor = 12632256 //silver

//metodo che visualizza i dati nel grafico
dw_grafico.uof_retrieve( )

destroy lds_dati
//########################################################################################
end subroutine

event pc_setwindow;call super::pc_setwindow;long ll_ppr
string ls_data_inizio

//dw_grafico.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete + c_disablecc ,c_disableccinsert)

select numero into:ll_ppr
from   parametri_azienda
where	 cod_azienda=:s_cs_xx.cod_azienda
and	 cod_parametro='PPR';

select stringa into:ls_data_inizio
from   parametri_azienda
where	 cod_azienda=:s_cs_xx.cod_azienda
and	 cod_parametro='DIN';

em_1.text = ls_data_inizio
em_2.text = string(RelativeDate ( date(ls_data_inizio), ll_ppr )) 


end event

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loadddlb(ddlb_cat_attrezzature, &
                 sqlca, &
                 "tab_cat_attrezzature", &
                 "cod_cat_attrezzature", &
                 "des_cat_attrezzature", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
end on

on w_grafico_imp_attr.create
int iCurrent
call super::create
this.dw_grafico=create dw_grafico
this.cb_1=create cb_1
this.ddlb_cat_attrezzature=create ddlb_cat_attrezzature
this.ddlb_attivita=create ddlb_attivita
this.ddlb_attrezzature=create ddlb_attrezzature
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.em_1=create em_1
this.st_5=create st_5
this.em_2=create em_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_grafico
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.ddlb_cat_attrezzature
this.Control[iCurrent+4]=this.ddlb_attivita
this.Control[iCurrent+5]=this.ddlb_attrezzature
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.st_2
this.Control[iCurrent+8]=this.st_3
this.Control[iCurrent+9]=this.st_4
this.Control[iCurrent+10]=this.em_1
this.Control[iCurrent+11]=this.st_5
this.Control[iCurrent+12]=this.em_2
end on

on w_grafico_imp_attr.destroy
call super::destroy
destroy(this.dw_grafico)
destroy(this.cb_1)
destroy(this.ddlb_cat_attrezzature)
destroy(this.ddlb_attivita)
destroy(this.ddlb_attrezzature)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.em_1)
destroy(this.st_5)
destroy(this.em_2)
end on

type dw_grafico from uo_cs_graph within w_grafico_imp_attr
integer x = 32
integer y = 24
integer width = 3470
integer height = 1376
integer taborder = 10
end type

type cb_1 from uo_cb_close within w_grafico_imp_attr
integer x = 3145
integer y = 1592
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type ddlb_cat_attrezzature from dropdownlistbox within w_grafico_imp_attr
integer x = 23
integer y = 1492
integer width = 1152
integer height = 1292
integer taborder = 50
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;datetime ld_data_inizio,ld_data_fine

ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))

is_cod_cat_attrezzature = f_po_selectddlb(ddlb_cat_attrezzature)
is_cod_attivita = f_po_selectddlb(ddlb_attivita)
is_cod_attrezzatura = f_po_selectddlb(ddlb_attrezzature)

ddlb_attivita.reset()
ddlb_attrezzature.reset()

f_po_loadddlb(ddlb_attrezzature, &
                 sqlca, &
                 "anag_attrezzature", &
                 "cod_attrezzatura", &
                 "descrizione", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_cat_attrezzature = '" + is_cod_cat_attrezzature + "'","")

f_po_loadddlb(ddlb_attivita, &
                 sqlca, &
                 "tab_attivita", &
                 "cod_attivita", &
                 "des_attivita", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_cat_attrezzature = '" + is_cod_cat_attrezzature + "'","")

//dw_grafico.setfocus()
//parent.triggerevent("pc_retrieve")
wf_retrieve_grafico(	is_cod_attivita, &
								is_cod_cat_attrezzature, &
								is_cod_attrezzatura, &
								ld_data_inizio, &
								ld_data_fine)
end event

type ddlb_attivita from dropdownlistbox within w_grafico_imp_attr
integer x = 2354
integer y = 1488
integer width = 1152
integer height = 1296
integer taborder = 70
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;datetime ld_data_inizio,ld_data_fine

ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))

is_cod_cat_attrezzature = f_po_selectddlb(ddlb_cat_attrezzature)
is_cod_attivita = f_po_selectddlb(ddlb_attivita)
is_cod_attrezzatura = f_po_selectddlb(ddlb_attrezzature)

//dw_grafico.setfocus()
//parent.triggerevent("pc_retrieve")
wf_retrieve_grafico(	is_cod_attivita, &
								is_cod_cat_attrezzature, &
								is_cod_attrezzatura, &
								ld_data_inizio, &
								ld_data_fine)
end event

type ddlb_attrezzature from dropdownlistbox within w_grafico_imp_attr
integer x = 1189
integer y = 1492
integer width = 1152
integer height = 1288
integer taborder = 60
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;datetime ld_data_inizio,ld_data_fine

ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))

is_cod_cat_attrezzature = f_po_selectddlb(ddlb_cat_attrezzature)
is_cod_attivita = f_po_selectddlb(ddlb_attivita)
is_cod_attrezzatura = f_po_selectddlb(ddlb_attrezzature)

//dw_grafico.setfocus()
//parent.triggerevent("pc_retrieve")
wf_retrieve_grafico(	is_cod_attivita, &
								is_cod_cat_attrezzature, &
								is_cod_attrezzatura, &
								ld_data_inizio, &
								ld_data_fine)
end event

type st_1 from statictext within w_grafico_imp_attr
integer x = 27
integer y = 1424
integer width = 1152
integer height = 64
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Categorie Attrezzature"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_2 from statictext within w_grafico_imp_attr
integer x = 1189
integer y = 1424
integer width = 1152
integer height = 64
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Attrezzature"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_3 from statictext within w_grafico_imp_attr
integer x = 2350
integer y = 1424
integer width = 1152
integer height = 64
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Attivita"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_4 from statictext within w_grafico_imp_attr
integer x = 27
integer y = 1600
integer width = 206
integer height = 72
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "da data"
boolean focusrectangle = false
end type

type em_1 from editmask within w_grafico_imp_attr
integer x = 242
integer y = 1596
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

event losefocus;datetime ld_data_inizio,ld_data_fine

ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))

//dw_grafico.setfocus()
//parent.triggerevent("pc_retrieve")
wf_retrieve_grafico(	is_cod_attivita, &
								is_cod_cat_attrezzature, &
								is_cod_attrezzatura, &
								ld_data_inizio, &
								ld_data_fine)
end event

type st_5 from statictext within w_grafico_imp_attr
integer x = 626
integer y = 1600
integer width = 174
integer height = 72
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "a data"
boolean focusrectangle = false
end type

type em_2 from editmask within w_grafico_imp_attr
integer x = 814
integer y = 1596
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

event losefocus;datetime ld_data_inizio,ld_data_fine

ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))

//dw_grafico.setfocus()
//parent.triggerevent("pc_retrieve")
wf_retrieve_grafico(	is_cod_attivita, &
								is_cod_cat_attrezzature, &
								is_cod_attrezzatura, &
								ld_data_inizio, &
								ld_data_fine)
end event

