﻿$PBExportHeader$w_grafico_tab_cal_attivita.srw
$PBExportComments$Window grafico tab_cal_attivita
forward
global type w_grafico_tab_cal_attivita from w_cs_xx_principale
end type
type dw_grafico from uo_cs_graph within w_grafico_tab_cal_attivita
end type
type cb_1 from uo_cb_close within w_grafico_tab_cal_attivita
end type
type dw_tab_cat_attrezzature_lista from uo_cs_xx_dw within w_grafico_tab_cal_attivita
end type
type em_1 from editmask within w_grafico_tab_cal_attivita
end type
type em_2 from editmask within w_grafico_tab_cal_attivita
end type
type st_1 from statictext within w_grafico_tab_cal_attivita
end type
type st_2 from statictext within w_grafico_tab_cal_attivita
end type
end forward

global type w_grafico_tab_cal_attivita from w_cs_xx_principale
integer width = 3497
integer height = 1828
string title = "Impegno Attrezzature"
dw_grafico dw_grafico
cb_1 cb_1
dw_tab_cat_attrezzature_lista dw_tab_cat_attrezzature_lista
em_1 em_1
em_2 em_2
st_1 st_1
st_2 st_2
end type
global w_grafico_tab_cal_attivita w_grafico_tab_cal_attivita

type variables

end variables

forward prototypes
public subroutine wf_retrieve_grafico (datetime fdt_data_inizio, datetime fdt_data_fine, string fs_cod_cat_attrez)
end prototypes

public subroutine wf_retrieve_grafico (datetime fdt_data_inizio, datetime fdt_data_fine, string fs_cod_cat_attrez);//SEZIONE 1 ###############################################################################
//creazione e popolamento del datastore di appoggio
//N.B. potrebbe essere usato anche una datawindow al posto di un datastore, purchè
//     questo venga specificato nell'oggetto grafico (vedi più avanti SEZIONE 2)
//----------------------------------------------------------------------------------------
datastore lds_dati

lds_dati = create datastore
lds_dati.dataobject = "d_grafico_attrezzature_tab_cal_attivita" //immettere qui il nome del datastore
lds_dati.settransobject(sqlca)
lds_dati.Retrieve(fdt_data_inizio, fdt_data_fine, fs_cod_cat_attrez,s_cs_xx.cod_azienda) //gli argomenti di retrieve dipendono dal datastore

//########################################################################################

//SEZIONE 2 ###############################################################################
//impostazione dell'oggetto grafico
//----------------------------------------------------------------------------------------

//si vuole usare un datastore (valore di default è TRUE, mettere FALSE se si tratta di datawindow)
//dw_grafico è il nome dato al controllo grafico sulla finestra
dw_grafico.ib_datastore = true

//impostare l'oggetto datastore (o datawindow: dw_grafico.idw_data )
dw_grafico.ids_data = lds_dati

//impostazione corrispondenza campi datastore (o datawindow) e grafico
/*ESEMPIO 1
	se il datastore ha 3 campi stringa 2 datetime e 1 numerico (esempio long, decimal):

	dw_grafico.is_str[1] = "nome prima colonna del ds di tipo stringa"
	dw_grafico.is_str[2] = "nome seconda colonna del ds di tipo stringa"
	dw_grafico.is_str[3] = "nome terza colonna del ds di tipo stringa"
	dw_grafico.is_dtm[1] = "nome prima colonna del ds di tipo datetime"
	dw_grafico.is_dtm[2] = "nome seconda colonna del ds di tipo datetime"
	dw_grafico.is_num[1] = "nome prima colonna del ds di tipo long, decimal, ecc..."

N.B. non è importante l'ordine di specifica dei vari campi
*/
dw_grafico.is_dtm[1] = "tab_cal_attivita_data_giorno"
dw_grafico.is_str[1] = "tab_cal_attivita_cod_attrezzatura"
dw_grafico.is_str[2] = "anag_attrezzature_descrizione"
dw_grafico.is_num[1] = "tab_cal_attivita_num_ore_impegnate"

//Questo serve per impostare l'ordinamento corretto della categoria prima di popolare il grafico
dw_grafico.is_source_categoria_col = "tab_cal_attivita_data_giorno"

//impostazione della categoria
/*ESEMPI
	se la categoria è una colonna di tipo string del datastore indicare "str_i"
	dove i è la posizione nell'array dw_grafico.is_istr[]

	se la categoria è una colonna di tipo datetime del datastore indicare "dtm_j"
	dove j è la posizione nell'array dw_grafico.is_dtm[]

	se la categoria è una colonna di tipo numerico del datastore indicare "num_k"
	dove k è la posizione nell'array dw_grafico.is_num[]
*/
dw_grafico.is_categoria_col = "dtm_1"

//impostazione della categoria (analogo a quanto fatto per la categoria)
/*
	essa può essere una colonna numerica o una espressione numerica
	ESEMPI
		"num_2"
		"num_1 - num_2"
		"(hour(dtm_3)*60 + minute(dtm_3) - hour(dtm_2)*60 - minute(dtm_2))/60"
*/
dw_grafico.is_exp_valore = "num_1"

//impostazione della categoria (analogo a quanto fatto per la categoria)
dw_grafico.is_serie_col = "str_2"

//altre impostazioni del grafico
dw_grafico.is_titolo = "Ore impegnate per attrezzatura" //Titolo del grafico
dw_grafico.is_label_categoria= "Data" //Etichetta sull'asse della categoria (Default ascissa)
dw_grafico.is_label_valori = "Ore" //Etichetta sull'asse dei valori (Default valori)

//tipo scala: 1 se lineare, 2 se logaritmica in base 10
dw_grafico.ii_scala = 1

//tipo grafico
/*
	1 Area			 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj	16 Linee 3D
 	2 Barre			 7 Colonne				12 Linee //default		17 Torta 3D
 	3 Barre 3D		 8 Colonne 3D				13 Torta
 	4 Barre 3D Obj		 9  Colonne 3D Obj			14 Scatter
 	5 Barre a Stack		10 Colonne a Stack			15 Area3D
*/
dw_grafico.ii_tipo_grafico = 9

//colore di sfondo del grafico
//per il bianco (DEFAULT)	: 16777215
//per il silver			: 12632256
dw_grafico.il_backcolor = 12632256 //silver

//metodo che visualizza i dati nel grafico
dw_grafico.uof_retrieve( )

destroy lds_dati
//########################################################################################

end subroutine

event pc_setwindow;call super::pc_setwindow;long ll_ppr
string ls_data_inizio

dw_tab_cat_attrezzature_lista.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete ,c_default)
//dw_grafico.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete ,c_default)

select numero into:ll_ppr
from   parametri_azienda
where	 cod_azienda=:s_cs_xx.cod_azienda
and	 cod_parametro='PPR';

select stringa into:ls_data_inizio
from   parametri_azienda
where	 cod_azienda=:s_cs_xx.cod_azienda
and	 cod_parametro='DIN';

em_1.text = ls_data_inizio
em_2.text = string(RelativeDate ( date(ls_data_inizio), ll_ppr )) 

end event

on w_grafico_tab_cal_attivita.create
int iCurrent
call super::create
this.dw_grafico=create dw_grafico
this.cb_1=create cb_1
this.dw_tab_cat_attrezzature_lista=create dw_tab_cat_attrezzature_lista
this.em_1=create em_1
this.em_2=create em_2
this.st_1=create st_1
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_grafico
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_tab_cat_attrezzature_lista
this.Control[iCurrent+4]=this.em_1
this.Control[iCurrent+5]=this.em_2
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.st_2
end on

on w_grafico_tab_cal_attivita.destroy
call super::destroy
destroy(this.dw_grafico)
destroy(this.cb_1)
destroy(this.dw_tab_cat_attrezzature_lista)
destroy(this.em_1)
destroy(this.em_2)
destroy(this.st_1)
destroy(this.st_2)
end on

type dw_grafico from uo_cs_graph within w_grafico_tab_cal_attivita
integer x = 27
integer y = 32
integer width = 3401
integer taborder = 10
end type

type cb_1 from uo_cb_close within w_grafico_tab_cal_attivita
integer x = 3072
integer y = 1620
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type dw_tab_cat_attrezzature_lista from uo_cs_xx_dw within w_grafico_tab_cal_attivita
integer x = 18
integer y = 1300
integer width = 1504
integer height = 400
integer taborder = 20
string dataobject = "d_tab_cat_attrezzature_lista"
boolean vscrollbar = true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end on

event clicked;call super::clicked;//dw_grafico.change_dw_focus(dw_grafico)
//parent.triggerevent("pc_retrieve")

long  l_Error
string ls_cod_cat_attrezzature
datetime ld_data_inizio,ld_data_fine

ls_cod_cat_attrezzature = dw_tab_cat_attrezzature_lista.getitemstring(dw_tab_cat_attrezzature_lista.getrow(),"cod_cat_attrezzature")
ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))

wf_retrieve_grafico(ld_data_inizio, ld_data_fine, ls_cod_cat_attrezzature)

/*
l_Error = Retrieve(ld_data_inizio,ld_data_fine,ls_cod_cat_attrezzature,s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
*/
end event

type em_1 from editmask within w_grafico_tab_cal_attivita
integer x = 1755
integer y = 1300
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

event losefocus;//dw_grafico.change_dw_focus(dw_grafico)
//parent.triggerevent("pc_retrieve")
end event

type em_2 from editmask within w_grafico_tab_cal_attivita
integer x = 2327
integer y = 1300
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

event losefocus;//dw_grafico.change_dw_focus(dw_grafico)
//parent.triggerevent("pc_retrieve")
end event

type st_1 from statictext within w_grafico_tab_cal_attivita
integer x = 1541
integer y = 1304
integer width = 206
integer height = 72
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "da data"
boolean focusrectangle = false
end type

type st_2 from statictext within w_grafico_tab_cal_attivita
integer x = 2139
integer y = 1304
integer width = 174
integer height = 72
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "a data"
boolean focusrectangle = false
end type

