﻿$PBExportHeader$w_produzione_commesse.srw
$PBExportComments$Window Produzione Commesse
forward
global type w_produzione_commesse from w_cs_xx_principale
end type
type dw_lista_commesse from uo_cs_xx_dw within w_produzione_commesse
end type
type dw_det_commessa from uo_cs_xx_dw within w_produzione_commesse
end type
type cb_1 from uo_cb_close within w_produzione_commesse
end type
type em_1 from editmask within w_produzione_commesse
end type
type cb_4 from commandbutton within w_produzione_commesse
end type
type st_1 from statictext within w_produzione_commesse
end type
type st_state from statictext within w_produzione_commesse
end type
type lb_1 from listbox within w_produzione_commesse
end type
type st_2 from statictext within w_produzione_commesse
end type
type cb_schedula from commandbutton within w_produzione_commesse
end type
type st_3 from statictext within w_produzione_commesse
end type
type st_state_1 from statictext within w_produzione_commesse
end type
type cb_stampa_errori from commandbutton within w_produzione_commesse
end type
type em_2 from editmask within w_produzione_commesse
end type
type em_3 from editmask within w_produzione_commesse
end type
type st_12 from statictext within w_produzione_commesse
end type
type st_13 from statictext within w_produzione_commesse
end type
type st_7 from statictext within w_produzione_commesse
end type
type st_4 from statictext within w_produzione_commesse
end type
type st_20 from statictext within w_produzione_commesse
end type
type rb_fase from radiobutton within w_produzione_commesse
end type
type rb_commessa from radiobutton within w_produzione_commesse
end type
type rb_consegna from radiobutton within w_produzione_commesse
end type
type gb_4 from groupbox within w_produzione_commesse
end type
type gb_3 from groupbox within w_produzione_commesse
end type
type gb_2 from groupbox within w_produzione_commesse
end type
type gb_1 from groupbox within w_produzione_commesse
end type
type cbx_mantieni from checkbox within w_produzione_commesse
end type
type sle_giorni_scarto from singlelineedit within w_produzione_commesse
end type
type st_8 from statictext within w_produzione_commesse
end type
type em_anno_commessa from editmask within w_produzione_commesse
end type
type rb_data_consegna from radiobutton within w_produzione_commesse
end type
type rb_selezione_lista from radiobutton within w_produzione_commesse
end type
type lb_commesse_ok from listbox within w_produzione_commesse
end type
type st_6 from statictext within w_produzione_commesse
end type
type st_anno from statictext within w_produzione_commesse
end type
type gb_5 from groupbox within w_produzione_commesse
end type
type st_ppr from statictext within w_produzione_commesse
end type
type cb_elimina_schedulazione_comm from commandbutton within w_produzione_commesse
end type
type cb_com_fuori from commandbutton within w_produzione_commesse
end type
type st_9 from statictext within w_produzione_commesse
end type
type sle_ore_medie from singlelineedit within w_produzione_commesse
end type
type cbx_forza_inizio from checkbox within w_produzione_commesse
end type
type em_ultima_data_inizio from editmask within w_produzione_commesse
end type
end forward

global type w_produzione_commesse from w_cs_xx_principale
int Width=3415
int Height=1745
boolean TitleBar=true
string Title="Schedulazione con capacità finita"
dw_lista_commesse dw_lista_commesse
dw_det_commessa dw_det_commessa
cb_1 cb_1
em_1 em_1
cb_4 cb_4
st_1 st_1
st_state st_state
lb_1 lb_1
st_2 st_2
cb_schedula cb_schedula
st_3 st_3
st_state_1 st_state_1
cb_stampa_errori cb_stampa_errori
em_2 em_2
em_3 em_3
st_12 st_12
st_13 st_13
st_7 st_7
st_4 st_4
st_20 st_20
rb_fase rb_fase
rb_commessa rb_commessa
rb_consegna rb_consegna
gb_4 gb_4
gb_3 gb_3
gb_2 gb_2
gb_1 gb_1
cbx_mantieni cbx_mantieni
sle_giorni_scarto sle_giorni_scarto
st_8 st_8
em_anno_commessa em_anno_commessa
rb_data_consegna rb_data_consegna
rb_selezione_lista rb_selezione_lista
lb_commesse_ok lb_commesse_ok
st_6 st_6
st_anno st_anno
gb_5 gb_5
st_ppr st_ppr
cb_elimina_schedulazione_comm cb_elimina_schedulazione_comm
cb_com_fuori cb_com_fuori
st_9 st_9
sle_ore_medie sle_ore_medie
cbx_forza_inizio cbx_forza_inizio
em_ultima_data_inizio em_ultima_data_inizio
end type
global w_produzione_commesse w_produzione_commesse

type variables

end variables

event pc_setwindow;call super::pc_setwindow;integer li_ppr

dw_lista_commesse.set_dw_options(sqlca,pcca.null_object,c_multiselect + c_nonew + c_nomodify + c_nodelete,c_default)
dw_det_commessa.set_dw_options(sqlca,dw_lista_commesse,c_sharedata + c_scrollparent,c_default)

iuo_dw_main = dw_lista_commesse

SELECT stringa  
INTO   :em_1.text
FROM   parametri_azienda  
WHERE  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda
AND    parametri_azienda.cod_parametro = 'DIN';

SELECT numero
INTO   :li_ppr
FROM   parametri_azienda  
WHERE  cod_azienda = :s_cs_xx.cod_azienda
AND    cod_parametro = 'PPR';

st_ppr.text=string(li_ppr)
st_state.text="Ok."
sle_giorni_scarto.text="0"
em_anno_commessa.text=string(year(today()))
em_ultima_data_inizio.text=string(today())
end event

on w_produzione_commesse.create
int iCurrent
call w_cs_xx_principale::create
this.dw_lista_commesse=create dw_lista_commesse
this.dw_det_commessa=create dw_det_commessa
this.cb_1=create cb_1
this.em_1=create em_1
this.cb_4=create cb_4
this.st_1=create st_1
this.st_state=create st_state
this.lb_1=create lb_1
this.st_2=create st_2
this.cb_schedula=create cb_schedula
this.st_3=create st_3
this.st_state_1=create st_state_1
this.cb_stampa_errori=create cb_stampa_errori
this.em_2=create em_2
this.em_3=create em_3
this.st_12=create st_12
this.st_13=create st_13
this.st_7=create st_7
this.st_4=create st_4
this.st_20=create st_20
this.rb_fase=create rb_fase
this.rb_commessa=create rb_commessa
this.rb_consegna=create rb_consegna
this.gb_4=create gb_4
this.gb_3=create gb_3
this.gb_2=create gb_2
this.gb_1=create gb_1
this.cbx_mantieni=create cbx_mantieni
this.sle_giorni_scarto=create sle_giorni_scarto
this.st_8=create st_8
this.em_anno_commessa=create em_anno_commessa
this.rb_data_consegna=create rb_data_consegna
this.rb_selezione_lista=create rb_selezione_lista
this.lb_commesse_ok=create lb_commesse_ok
this.st_6=create st_6
this.st_anno=create st_anno
this.gb_5=create gb_5
this.st_ppr=create st_ppr
this.cb_elimina_schedulazione_comm=create cb_elimina_schedulazione_comm
this.cb_com_fuori=create cb_com_fuori
this.st_9=create st_9
this.sle_ore_medie=create sle_ore_medie
this.cbx_forza_inizio=create cbx_forza_inizio
this.em_ultima_data_inizio=create em_ultima_data_inizio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_commesse
this.Control[iCurrent+2]=dw_det_commessa
this.Control[iCurrent+3]=cb_1
this.Control[iCurrent+4]=em_1
this.Control[iCurrent+5]=cb_4
this.Control[iCurrent+6]=st_1
this.Control[iCurrent+7]=st_state
this.Control[iCurrent+8]=lb_1
this.Control[iCurrent+9]=st_2
this.Control[iCurrent+10]=cb_schedula
this.Control[iCurrent+11]=st_3
this.Control[iCurrent+12]=st_state_1
this.Control[iCurrent+13]=cb_stampa_errori
this.Control[iCurrent+14]=em_2
this.Control[iCurrent+15]=em_3
this.Control[iCurrent+16]=st_12
this.Control[iCurrent+17]=st_13
this.Control[iCurrent+18]=st_7
this.Control[iCurrent+19]=st_4
this.Control[iCurrent+20]=st_20
this.Control[iCurrent+21]=rb_fase
this.Control[iCurrent+22]=rb_commessa
this.Control[iCurrent+23]=rb_consegna
this.Control[iCurrent+24]=gb_4
this.Control[iCurrent+25]=gb_3
this.Control[iCurrent+26]=gb_2
this.Control[iCurrent+27]=gb_1
this.Control[iCurrent+28]=cbx_mantieni
this.Control[iCurrent+29]=sle_giorni_scarto
this.Control[iCurrent+30]=st_8
this.Control[iCurrent+31]=em_anno_commessa
this.Control[iCurrent+32]=rb_data_consegna
this.Control[iCurrent+33]=rb_selezione_lista
this.Control[iCurrent+34]=lb_commesse_ok
this.Control[iCurrent+35]=st_6
this.Control[iCurrent+36]=st_anno
this.Control[iCurrent+37]=gb_5
this.Control[iCurrent+38]=st_ppr
this.Control[iCurrent+39]=cb_elimina_schedulazione_comm
this.Control[iCurrent+40]=cb_com_fuori
this.Control[iCurrent+41]=st_9
this.Control[iCurrent+42]=sle_ore_medie
this.Control[iCurrent+43]=cbx_forza_inizio
this.Control[iCurrent+44]=em_ultima_data_inizio
end on

on w_produzione_commesse.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_lista_commesse)
destroy(this.dw_det_commessa)
destroy(this.cb_1)
destroy(this.em_1)
destroy(this.cb_4)
destroy(this.st_1)
destroy(this.st_state)
destroy(this.lb_1)
destroy(this.st_2)
destroy(this.cb_schedula)
destroy(this.st_3)
destroy(this.st_state_1)
destroy(this.cb_stampa_errori)
destroy(this.em_2)
destroy(this.em_3)
destroy(this.st_12)
destroy(this.st_13)
destroy(this.st_7)
destroy(this.st_4)
destroy(this.st_20)
destroy(this.rb_fase)
destroy(this.rb_commessa)
destroy(this.rb_consegna)
destroy(this.gb_4)
destroy(this.gb_3)
destroy(this.gb_2)
destroy(this.gb_1)
destroy(this.cbx_mantieni)
destroy(this.sle_giorni_scarto)
destroy(this.st_8)
destroy(this.em_anno_commessa)
destroy(this.rb_data_consegna)
destroy(this.rb_selezione_lista)
destroy(this.lb_commesse_ok)
destroy(this.st_6)
destroy(this.st_anno)
destroy(this.gb_5)
destroy(this.st_ppr)
destroy(this.cb_elimina_schedulazione_comm)
destroy(this.cb_com_fuori)
destroy(this.st_9)
destroy(this.sle_ore_medie)
destroy(this.cbx_forza_inizio)
destroy(this.em_ultima_data_inizio)
end on

type dw_lista_commesse from uo_cs_xx_dw within w_produzione_commesse
int X=23
int Y=21
int Width=1966
int Height=421
int TabOrder=10
string DataObject="d_anag_commesse_lista_prod"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
integer li_anno_commessa

li_anno_commessa = integer(em_anno_commessa.text)
l_Error = Retrieve(s_cs_xx.cod_azienda,li_anno_commessa)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

type dw_det_commessa from uo_cs_xx_dw within w_produzione_commesse
int X=23
int Y=461
int Width=1966
int Height=641
int TabOrder=30
string DataObject="d_commesse_det_produzione"
BorderStyle BorderStyle=StyleRaised!
end type

type cb_1 from uo_cb_close within w_produzione_commesse
int X=2995
int Y=1541
int Width=366
int Height=81
int TabOrder=80
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_1 from editmask within w_produzione_commesse
int X=2675
int Y=1321
int Width=321
int Height=81
int TabOrder=210
TextCase TextCase=Upper!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_4 from commandbutton within w_produzione_commesse
int X=2606
int Y=1541
int Width=366
int Height=81
int TabOrder=70
boolean Enabled=false
string Text="&Riprist.Cal."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_prodotto,ls_cod_lavorazione,ls_cod_cat_attrezzature,ls_cod_attivita
string ls_errore
long  ll_anno_commessa,ll_num_commessa
datetime ld_data_giorno,ld_ultima_data_inizio
double ldd_tempo_totale_previsto
integer li_risposta

li_risposta = g_mb.messagebox("Sep", "Il Calendario per la commessa corrente sta per essere azzerato. Contiunuare?",  &
											  Exclamation!, OKCancel!, 1)

if li_risposta = 1 then

	setpointer(hourglass!)
	
	ll_anno_commessa = dw_lista_commesse.getitemnumber(dw_lista_commesse.getrow(),"anno_commessa")
	ll_num_commessa = dw_lista_commesse.getitemnumber(dw_lista_commesse.getrow(),"num_commessa")

	st_state.text="Cancello fasi comm."
	DELETE FROM fasi_avanzamento_commessa
	WHERE fasi_avanzamento_commessa.cod_azienda = :s_cs_xx.cod_azienda 
	AND   fasi_avanzamento_commessa.anno_commessa = :ll_anno_commessa
	AND   fasi_avanzamento_commessa.num_commessa = :ll_num_commessa;

	st_state.text="Cancello comm. rep."

	DELETE FROM commesse_reparti
	WHERE commesse_reparti.cod_azienda = :s_cs_xx.cod_azienda 
	AND   commesse_reparti.anno_commessa = :ll_anno_commessa
	AND   commesse_reparti.num_commessa = :ll_num_commessa;

	declare righe_commesse_reparti_1 cursor for 
	select  cod_prodotto,
			  cod_lavorazione,
			  tempo_totale_previsto, 
			  ultima_data_inizio, 
			  anno_commessa,
			  num_commessa 
	from    commesse_reparti 
	where   cod_azienda=:s_cs_xx.cod_azienda 
	and     tempo_totale_lavorazione = 0 
	ORDER BY num_sequenza_fasi,
				cod_reparto,
				cod_lavorazione,
				tempo_totale_previsto DESC;

	open righe_commesse_reparti_1;

	st_state.text="Ripristino Calendario"

	do while 1 = 1
  		fetch righe_commesse_reparti_1 into :ls_cod_prodotto, :ls_cod_lavorazione, :ldd_tempo_totale_previsto, :ld_ultima_data_inizio, :ll_anno_commessa, :ll_num_commessa;

	   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

		if sqlca.sqlcode<>0 then
			g_mb.messagebox("Errore nel DB",SQLCA.SQLErrText,Information!)
		end if
	
		li_risposta=f_reset_cal_attivita(ll_num_commessa,ll_anno_commessa,ls_errore)
		if li_risposta=-1 then 
			close righe_commesse_reparti_1;
			g_mb.messagebox("Sep",ls_errore,exclamation!)
			st_state.text="Errore in rip. cal."
			return 
		end if	

	loop

	close righe_commesse_reparti_1;

	setpointer(arrow!)

	g_mb.messagebox("Sep","Ripristino Calendario completato con successo.",information!)
	st_state.text="Ok"
else
	return

end if
end event

type st_1 from statictext within w_produzione_commesse
int X=2058
int Y=1341
int Width=618
int Height=61
boolean Enabled=false
string Text="Data Inizio Produzione:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_state from statictext within w_produzione_commesse
int X=46
int Y=1241
int Width=1143
int Height=61
boolean Enabled=false
string Text="Stato"
boolean FocusRectangle=false
long TextColor=255
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type lb_1 from listbox within w_produzione_commesse
int X=2012
int Y=101
int Width=915
int Height=681
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_produzione_commesse
int X=2012
int Y=21
int Width=481
int Height=61
boolean Enabled=false
string Text="Riepilogo Errori"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_schedula from commandbutton within w_produzione_commesse
int X=2218
int Y=1541
int Width=366
int Height=81
int TabOrder=60
string Text="&Schedula"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string   ls_errore,ls_cod_prodotto,ls_item,ls_cod_prodotto_finito,ls_cod_versione
integer  li_risposta,li_anno_commessa,li_prevalenza,li_giorni_scarto
long     ll_num_righe,ll_larghezza,ll_num_commessa,ll_conteggio,& 
	      ll_num_errori,ll_selected[],ll_riga
datetime ld_data_inizio,ld_data_fine,ld_data_consegna,ld_data_fine_produzione, & 
	      ld_data_inizio_produzione,ld_ultima

li_risposta=g_mb.messagebox("Sep","Attenzione questo comando lancia la produzione delle commesse selezionate, vuoi procedere?",question!,YesNo!,1)
if li_risposta = 2 then return

setpointer(hourglass!)
lb_1.reset()
lb_commesse_ok.reset()

s_cs_xx.parametri.parametro_d_11=double(sle_ore_medie.text)
s_cs_xx.parametri.parametro_data_3=datetime(date(em_ultima_data_inizio.text))
s_cs_xx.parametri.parametro_b_1 = cbx_forza_inizio.checked
ld_data_inizio = datetime(date(em_2.text))
ld_data_fine = datetime(date(em_3.text))

if rb_fase.checked=true then li_prevalenza = 1
if rb_commessa.checked=true then li_prevalenza = 2
if rb_consegna.checked=true then li_prevalenza = 3

delete from fasi_avanzamento_commessa
where  cod_azienda=:s_cs_xx.cod_azienda;

commit;

delete from commesse_reparti
where  cod_azienda=:s_cs_xx.cod_azienda;

commit;

if cbx_mantieni.checked = false then
	st_state.text = "Eliminazione schedulazione precedente"

	declare  righe_det_cal cursor for
	select   anno_commessa,
	     	   num_commessa
	from     det_cal_attivita 
	where    cod_azienda=:s_cs_xx.cod_azienda
	group by anno_commessa,
				num_commessa;

	open  righe_det_cal;

	do while 1 = 1
		fetch righe_det_cal
	   into  :li_anno_commessa,
   	      :ll_num_commessa;

	   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
		li_risposta = f_reset_cal_attivita(ll_num_commessa,li_anno_commessa,ls_errore)

		if li_risposta=-1 then
			g_mb.messagebox("Sep",ls_errore,exclamation!)
			close righe_det_cal;
			rollback;
			return
		end if

	loop
	close righe_det_cal;
end if


if rb_data_consegna.checked = true then
	select  count(*)
	into    :ll_num_righe
	from    anag_commesse
	where   cod_azienda=:s_cs_xx.cod_azienda
	and	  data_consegna 
	between :ld_data_inizio 
	and     :ld_data_fine;

	if ll_num_righe = 0 then
		g_mb.messagebox("Sep","Attenzione, non esistono commesse con data consegna compresa nel periodo selezionato",exclamation!)
		return
	end if

	ll_larghezza = int(st_4.width/ll_num_righe)
	st_4.width = 0

	declare righe_commesse_produzione cursor for
	select  anno_commessa,
	     	  num_commessa,
			  cod_prodotto,
			  cod_versione,
	        data_consegna
	from    anag_commesse 
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     data_consegna
	between :ld_data_inizio
	and     :ld_data_fine;
	
	open  righe_commesse_produzione;

	do while 1 = 1
		fetch righe_commesse_produzione 
	   into  :li_anno_commessa,
	         :ll_num_commessa,
				:ls_cod_prodotto_finito,
			   :ls_cod_versione,
	         :ld_data_consegna;
	
	   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then 
			st_4.width = ll_larghezza*ll_conteggio
			st_3.text = string(int((ll_conteggio/ll_num_righe)*100)) + "%"
			exit
		end if
	
		st_state_1.text=string(ll_num_commessa)

		st_state.text="Fasi Avanzamento Commessa"
		li_risposta = f_fasi_avanzamento_commessa(ll_num_commessa,li_anno_commessa,ls_cod_prodotto_finito,ls_cod_versione,ld_data_consegna,ls_errore)
		if li_risposta = -1 then 
			ll_num_errori++
			lb_1.additem("Errore in Fase Avanzamento Commessa: " + ls_errore)
			st_state.text="Errore in Fasi Avanzamento Commessa"
			ll_conteggio++
			st_4.width = ll_larghezza*ll_conteggio
			st_3.text = string(int((ll_conteggio/ll_num_righe)*100)) + "%"
			continue
		end if
	
		st_state.text="Commesse Reparti"
		li_risposta = f_commesse_reparto(ll_num_commessa,li_anno_commessa,ld_data_consegna,datetime(date(em_1.text)),ls_errore)
	
		if li_risposta = -1 then 
			ll_num_errori++
			lb_1.additem("Errore in Commesse Reparti: " +ls_errore)
			st_state.text="Errore in Commesse Reparti"
			ll_conteggio++
			st_4.width = ll_larghezza*ll_conteggio
			st_3.text = string(int((ll_conteggio/ll_num_righe)*100)) + "%"
			continue
		end if
	
		ll_conteggio++
		st_4.width = ll_larghezza*ll_conteggio
		st_3.text = string(int((ll_conteggio/ll_num_righe)*100)) + "%"
		
	loop
	
	close righe_commesse_produzione;
else
	dw_lista_commesse.get_selected_rows(ll_selected[])

	ll_num_righe=upperbound(ll_selected[])

	ll_larghezza = int(st_4.width/ll_num_righe)
	st_4.width = 0

	for ll_riga = 1 to upperbound(ll_selected[])
	  	 li_anno_commessa = dw_lista_commesse.getitemnumber(ll_selected[ll_riga],"anno_commessa")
		 ll_num_commessa = dw_lista_commesse.getitemnumber(ll_selected[ll_riga],"num_commessa")
		 ls_cod_prodotto_finito = dw_lista_commesse.getitemstring(ll_selected[ll_riga],"cod_prodotto")
		 ls_cod_versione = dw_lista_commesse.getitemstring(ll_selected[ll_riga],"cod_versione")
		 ld_data_consegna = dw_lista_commesse.getitemdatetime(ll_selected[ll_riga],"data_consegna")
	
		 st_state_1.text=string(ll_num_commessa)

		 st_state.text="Fasi Avanzamento Commessa"
		 li_risposta = f_fasi_avanzamento_commessa(ll_num_commessa,li_anno_commessa,ls_cod_prodotto_finito,ls_cod_versione,ld_data_consegna,ls_errore)
 		 if li_risposta = -1 then 
			 ll_num_errori++
			 lb_1.additem("Errore in Fasi Avanzamento Commessa: " + ls_errore)
			 st_state.text="Errore in Fasi Avanzamento Commessa"
			 ll_conteggio++
			 st_4.width = ll_larghezza*ll_conteggio
			 st_3.text = string(int((ll_conteggio/ll_num_righe)*100)) + "%"
			 continue
		 end if

	
		st_state.text="Commesse Reparti"
		li_risposta = f_commesse_reparto(ll_num_commessa,li_anno_commessa,ld_data_consegna,datetime(date(em_1.text)),ls_errore)
	
		if li_risposta = -1 then 
			ll_num_errori++
			lb_1.additem("Errore in Commesse Reparti: " + ls_errore)
			st_state.text="Errore in Commesse Reparti"
			ll_conteggio++
			st_4.width = ll_larghezza*ll_conteggio
			st_3.text = string(int((ll_conteggio/ll_num_righe)*100)) + "%"
			continue
		end if
		
		ll_conteggio++
		st_4.width = ll_larghezza*ll_conteggio
		st_3.text = string(int((ll_conteggio/ll_num_righe)*100)) + "%"
	
	next 
end if

st_4.width = st_20.width 
st_state.text="Aggiornamento ultima data inizio..."
li_giorni_scarto=integer(sle_giorni_scarto.text)

li_risposta=f_aggiorna_ultima_data_inizio(li_giorni_scarto,ls_errore)

if li_risposta=-1 then 
	g_mb.messagebox("Sep",ls_errore,exclamation!)
	ll_num_errori++
	setpointer(arrow!)
	st_state.text="Errore in aggiornamento ultima data inizio"
	return
end if

ld_data_inizio_produzione=datetime(date(em_1.text))
ld_data_fine_produzione = datetime(relativedate(date(em_1.text),integer(st_ppr.text)))

declare  righe_com_rep cursor for
select   num_commessa,
	     	ultima_data_inizio
from     commesse_reparti
where    cod_azienda=:s_cs_xx.cod_azienda
and      ultima_data_inizio
between  :ld_data_inizio_produzione
and      :ld_data_fine_produzione
group by num_commessa,
			ultima_data_inizio;

open  righe_com_rep;

do while 1 = 1
	fetch righe_com_rep
   into  :ll_num_commessa,
     	   :ld_ultima;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	ls_item=string(ll_num_commessa) + " " + string(ld_ultima)
	lb_commesse_ok.additem(ls_item)	

loop

close  righe_com_rep;
st_4.width = 	st_20.width 
st_state.text="Inserimento in Calendario"

li_risposta=f_ins_cal_attivita(datetime(date(em_1.text)),li_prevalenza,ls_errore)

if li_risposta=-1 then 
	g_mb.messagebox("Sep",ls_errore,exclamation!)
	ll_num_errori++
	setpointer(arrow!)
	st_state.text="Errore in inserimento calendario"
	return
end if

if ll_num_errori>0 then
	commit;
	g_mb.messagebox("Sep","Attenzione si sono verificati " + string(ll_num_errori) + " durante la procedura, consultare il riepilogo errori e tentare di corregerli, successivamente riattivare la produzione.",Exclamation!)
else
	g_mb.messagebox("Sep","Programmazione completata con successo.",information!)
	commit;
	st_state.text="Ok."
end if

setpointer(arrow!)
end event

type st_3 from statictext within w_produzione_commesse
int X=46
int Y=1181
int Width=138
int Height=61
boolean Enabled=false
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_state_1 from statictext within w_produzione_commesse
int X=1715
int Y=1241
int Width=252
int Height=61
boolean Enabled=false
boolean FocusRectangle=false
long TextColor=255
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_stampa_errori from commandbutton within w_produzione_commesse
int X=1829
int Y=1541
int Width=366
int Height=81
int TabOrder=50
string Text="Stam&pa Err."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;long Job,ll_t,ll_max
string ls_stringa_stampa

ll_max =	lb_1.TotalItems ( )
Job = PrintOpen( )

PrintDefineFont(Job, 1, "Arial", -10, 700, Variable!, Roman!, FALSE, FALSE)
PrintSetFont(Job, 1)
Print(Job,500, " ")
Print(Job,500, "ERRORI VERIFICATI IN CREAZIONE PRODUZIONE:")
Print(Job,500, " ")

PrintDefineFont(Job, 1, "Arial", -8, 500, Variable!, Roman!, FALSE, FALSE)
PrintSetFont(Job, 1)

for ll_t = 1 to ll_max

	lb_1.SelectItem (ll_t)
	ls_stringa_stampa = lb_1.SelectedItem ( )
	Print(Job,500, ls_stringa_stampa)

next

PrintClose(Job)
end on

type em_2 from editmask within w_produzione_commesse
int X=2561
int Y=1061
int Width=321
int Height=81
int TabOrder=150
Alignment Alignment=Center!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
long TextColor=33554432
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_3 from editmask within w_produzione_commesse
int X=2949
int Y=1061
int Width=321
int Height=81
int TabOrder=160
Alignment Alignment=Center!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
long TextColor=33554432
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_12 from statictext within w_produzione_commesse
int X=2881
int Y=1081
int Width=69
int Height=61
boolean Enabled=false
string Text="e:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_13 from statictext within w_produzione_commesse
int X=1189
int Y=1241
int Width=526
int Height=61
boolean Enabled=false
string Text="Numero Commessa"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_7 from statictext within w_produzione_commesse
int X=778
int Y=1381
int Width=618
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Periodo di produzione (gg.):"
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_produzione_commesse
int X=183
int Y=1181
int Width=1783
int Height=41
boolean Enabled=false
boolean Border=true
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=16711680
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_20 from statictext within w_produzione_commesse
int X=183
int Y=1181
int Width=1783
int Height=41
boolean Enabled=false
boolean Border=true
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_fase from radiobutton within w_produzione_commesse
int X=2035
int Y=881
int Width=206
int Height=61
boolean BringToTop=true
string Text="Fase"
boolean Checked=true
boolean LeftText=true
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_commessa from radiobutton within w_produzione_commesse
int X=2401
int Y=881
int Width=321
int Height=61
boolean BringToTop=true
string Text="Commessa"
boolean LeftText=true
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_consegna from radiobutton within w_produzione_commesse
int X=2881
int Y=881
int Width=412
int Height=61
boolean BringToTop=true
string Text="Data Consegna"
boolean LeftText=true
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_4 from groupbox within w_produzione_commesse
int X=23
int Y=1121
int Width=1966
int Height=201
int TabOrder=140
string Text="Stato Schedulazione"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_3 from groupbox within w_produzione_commesse
int X=2012
int Y=1261
int Width=1349
int Height=261
int TabOrder=170
string Text="Tempi di schedulazione"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_2 from groupbox within w_produzione_commesse
int X=2012
int Y=981
int Width=1349
int Height=261
int TabOrder=130
string Text="Selezione commesse da schedulare"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within w_produzione_commesse
int X=2012
int Y=801
int Width=1349
int Height=161
int TabOrder=40
string Text="Prevalenze di schedulazione"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_mantieni from checkbox within w_produzione_commesse
int X=778
int Y=1441
int Width=595
int Height=61
boolean BringToTop=true
string Text="Mantieni Sch. esistente"
boolean Checked=true
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_giorni_scarto from singlelineedit within w_produzione_commesse
int X=2401
int Y=1421
int Width=321
int Height=81
int TabOrder=190
boolean BringToTop=true
boolean AutoHScroll=false
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_8 from statictext within w_produzione_commesse
int X=2035
int Y=1441
int Width=366
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="giorni di scarto:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_anno_commessa from editmask within w_produzione_commesse
int X=458
int Y=1401
int Width=298
int Height=81
int TabOrder=110
boolean BringToTop=true
Alignment Alignment=Center!
string Mask="yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event modified;dw_lista_commesse.change_dw_focus(dw_lista_commesse)
parent.triggerevent("pc_retrieve")
end event

type rb_data_consegna from radiobutton within w_produzione_commesse
int X=2035
int Y=1061
int Width=526
int Height=81
boolean BringToTop=true
string Text="Data consegna tra:"
boolean Checked=true
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;em_2.enabled=true
em_3.enabled=true
end event

type rb_selezione_lista from radiobutton within w_produzione_commesse
int X=2035
int Y=1141
int Width=951
int Height=77
boolean BringToTop=true
string Text="Commesse selezionate nella lista"
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;em_2.enabled=false
em_3.enabled=false
end event

type lb_commesse_ok from listbox within w_produzione_commesse
int X=2949
int Y=101
int Width=412
int Height=681
int TabOrder=20
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_6 from statictext within w_produzione_commesse
int X=2949
int Y=21
int Width=398
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Commesse Ok"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_anno from statictext within w_produzione_commesse
int X=92
int Y=1421
int Width=366
int Height=61
boolean Enabled=false
string Text="Anno Comm.:"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_5 from groupbox within w_produzione_commesse
int X=23
int Y=1321
int Width=1966
int Height=201
int TabOrder=200
string Text="Parametri Generali"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_ppr from statictext within w_produzione_commesse
int X=1395
int Y=1381
int Width=138
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="ee"
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_elimina_schedulazione_comm from commandbutton within w_produzione_commesse
int X=1441
int Y=1541
int Width=366
int Height=81
int TabOrder=100
boolean BringToTop=true
string Text="&Elim.S.Com."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;integer li_anno_commessa,li_risposta
long    ll_num_commessa,ll_num_righe,ll_riga,ll_selected[]
string  ls_cod_prodotto,ls_errore

li_risposta=g_mb.messagebox("Sep","Attenzione! La schedulazione delle commesse selezionate sopra sarà eliminata, vuoi procedere?",question!,YesNo!,1)
if li_risposta = 2 then return

setpointer(hourglass!)

dw_lista_commesse.get_selected_rows(ll_selected[])

ll_num_righe=upperbound(ll_selected[])

for ll_riga = 1 to upperbound(ll_selected[])
  	 li_anno_commessa = dw_lista_commesse.getitemnumber(ll_selected[ll_riga],"anno_commessa")
	 ll_num_commessa = dw_lista_commesse.getitemnumber(ll_selected[ll_riga],"num_commessa")
	 
    li_risposta = f_reset_cal_attivita(ll_num_commessa,li_anno_commessa,ls_errore)

	 if li_risposta=-1 then
		g_mb.messagebox("Sep",ls_errore,exclamation!)
		rollback;
		return
	 end if
next

setpointer(arrow!)
end event

type cb_com_fuori from commandbutton within w_produzione_commesse
int X=1052
int Y=1541
int Width=366
int Height=81
int TabOrder=90
boolean BringToTop=true
string Text="Com. Sched."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open(w_commesse_fuori_data,-1)
end event

type st_9 from statictext within w_produzione_commesse
int X=2812
int Y=1441
int Width=330
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="ore medie lav.:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_ore_medie from singlelineedit within w_produzione_commesse
int X=3155
int Y=1421
int Width=183
int Height=81
int TabOrder=180
boolean BringToTop=true
boolean AutoHScroll=false
string Text="8"
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_forza_inizio from checkbox within w_produzione_commesse
int X=1578
int Y=1381
int Width=389
int Height=61
boolean BringToTop=true
string Text="Forza Inizio a"
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_ultima_data_inizio from editmask within w_produzione_commesse
int X=1646
int Y=1441
int Width=321
int Height=61
int TabOrder=120
boolean BringToTop=true
Alignment Alignment=Center!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

