﻿$PBExportHeader$w_produzione_prodotti.srw
$PBExportComments$Window Gestione Produzione Prodotti
forward
global type w_produzione_prodotti from w_cs_xx_principale
end type
type dw_vista_prodotti_totale from uo_cs_xx_dw within w_produzione_prodotti
end type
type cb_chiudi from commandbutton within w_produzione_prodotti
end type
type cb_avvia_prodotto from commandbutton within w_produzione_prodotti
end type
type st_2 from statictext within w_produzione_prodotti
end type
type sle_1 from singlelineedit within w_produzione_prodotti
end type
type st_3 from statictext within w_produzione_prodotti
end type
type st_4 from statictext within w_produzione_prodotti
end type
type st_state from statictext within w_produzione_prodotti
end type
type st_5 from statictext within w_produzione_prodotti
end type
type st_state_1 from statictext within w_produzione_prodotti
end type
type cb_interrompi from commandbutton within w_produzione_prodotti
end type
type st_1 from statictext within w_produzione_prodotti
end type
type st_6 from statictext within w_produzione_prodotti
end type
type lb_1 from listbox within w_produzione_prodotti
end type
type st_7 from statictext within w_produzione_prodotti
end type
type st_10 from statictext within w_produzione_prodotti
end type
type cb_stampa_errori from commandbutton within w_produzione_prodotti
end type
type st_8 from statictext within w_produzione_prodotti
end type
type st_9 from statictext within w_produzione_prodotti
end type
type cb_avvia_tutti from commandbutton within w_produzione_prodotti
end type
end forward

global type w_produzione_prodotti from w_cs_xx_principale
int Width=3516
int Height=1725
boolean TitleBar=true
string Title="Produzione Prodotti"
dw_vista_prodotti_totale dw_vista_prodotti_totale
cb_chiudi cb_chiudi
cb_avvia_prodotto cb_avvia_prodotto
st_2 st_2
sle_1 sle_1
st_3 st_3
st_4 st_4
st_state st_state
st_5 st_5
st_state_1 st_state_1
cb_interrompi cb_interrompi
st_1 st_1
st_6 st_6
lb_1 lb_1
st_7 st_7
st_10 st_10
cb_stampa_errori cb_stampa_errori
st_8 st_8
st_9 st_9
cb_avvia_tutti cb_avvia_tutti
end type
global w_produzione_prodotti w_produzione_prodotti

type variables
boolean ib_interrompi 
end variables

event pc_setwindow;call super::pc_setwindow;dw_vista_prodotti_totale.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nodelete + c_nomodify + c_disablecc,c_disableccinsert)
st_state.text="Ok."
cb_interrompi.enabled=false
end event

on w_produzione_prodotti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_vista_prodotti_totale=create dw_vista_prodotti_totale
this.cb_chiudi=create cb_chiudi
this.cb_avvia_prodotto=create cb_avvia_prodotto
this.st_2=create st_2
this.sle_1=create sle_1
this.st_3=create st_3
this.st_4=create st_4
this.st_state=create st_state
this.st_5=create st_5
this.st_state_1=create st_state_1
this.cb_interrompi=create cb_interrompi
this.st_1=create st_1
this.st_6=create st_6
this.lb_1=create lb_1
this.st_7=create st_7
this.st_10=create st_10
this.cb_stampa_errori=create cb_stampa_errori
this.st_8=create st_8
this.st_9=create st_9
this.cb_avvia_tutti=create cb_avvia_tutti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_vista_prodotti_totale
this.Control[iCurrent+2]=cb_chiudi
this.Control[iCurrent+3]=cb_avvia_prodotto
this.Control[iCurrent+4]=st_2
this.Control[iCurrent+5]=sle_1
this.Control[iCurrent+6]=st_3
this.Control[iCurrent+7]=st_4
this.Control[iCurrent+8]=st_state
this.Control[iCurrent+9]=st_5
this.Control[iCurrent+10]=st_state_1
this.Control[iCurrent+11]=cb_interrompi
this.Control[iCurrent+12]=st_1
this.Control[iCurrent+13]=st_6
this.Control[iCurrent+14]=lb_1
this.Control[iCurrent+15]=st_7
this.Control[iCurrent+16]=st_10
this.Control[iCurrent+17]=cb_stampa_errori
this.Control[iCurrent+18]=st_8
this.Control[iCurrent+19]=st_9
this.Control[iCurrent+20]=cb_avvia_tutti
end on

on w_produzione_prodotti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_vista_prodotti_totale)
destroy(this.cb_chiudi)
destroy(this.cb_avvia_prodotto)
destroy(this.st_2)
destroy(this.sle_1)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.st_state)
destroy(this.st_5)
destroy(this.st_state_1)
destroy(this.cb_interrompi)
destroy(this.st_1)
destroy(this.st_6)
destroy(this.lb_1)
destroy(this.st_7)
destroy(this.st_10)
destroy(this.cb_stampa_errori)
destroy(this.st_8)
destroy(this.st_9)
destroy(this.cb_avvia_tutti)
end on

type dw_vista_prodotti_totale from uo_cs_xx_dw within w_produzione_prodotti
int X=23
int Y=81
int Width=2081
int Height=1161
int TabOrder=10
string DataObject="d_distinta_padri_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

st_9.text = string(rowcount())
end on

type cb_chiudi from commandbutton within w_produzione_prodotti
int X=3086
int Y=1521
int Width=366
int Height=81
int TabOrder=20
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;close(parent)
end on

type cb_avvia_prodotto from commandbutton within w_produzione_prodotti
int X=1921
int Y=1521
int Width=366
int Height=81
int TabOrder=70
string Text="Avvia &Prod."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_prodotto,ls_errore,ls_cod_versione
integer li_risposta

if sle_1.text="" or isnull(sle_1.text)  then 
	g_mb.messagebox("Sep","Attenzione! Manca il numero di livello distinta, inserire un numero (>1) e riavviare",exclamation!)
	return 
end if


setpointer(hourglass!)

ls_cod_prodotto=dw_vista_prodotti_totale.getitemstring(dw_vista_prodotti_totale.getrow(),"cod_prodotto")
ls_cod_versione=dw_vista_prodotti_totale.getitemstring(dw_vista_prodotti_totale.getrow(),"cod_versione")

st_state_1.text = dw_vista_prodotti_totale.getitemstring(dw_vista_prodotti_totale.getrow(),"cod_prodotto")
st_state.text="Verifica rep. 1° liv."

li_risposta=f_ver_rep(ls_cod_prodotto,ls_cod_versione,ls_errore)

if li_risposta=-1 then
	g_mb.messagebox("Sep",ls_errore)
	st_state.text="Errore in verifica reparti"	
	return
end if	

DELETE FROM fasi_avanzamento  
WHERE fasi_avanzamento.cod_azienda = :s_cs_xx.cod_azienda 
AND  fasi_avanzamento.cod_prodotto_finito = :ls_cod_prodotto;

st_state.text="Fasi avanzamento"
//li_risposta=f_fasi_avanzamento(ls_cod_prodotto,long(sle_1.text),ls_cod_prodotto,ls_cod_versione,1,ls_errore)

if li_risposta=-1 then
	g_mb.messagebox("Sep",ls_errore)
	st_state.text="Errore fasi avanzamento"	
	rollback;
	setpointer(arrow!)
	return
else
	commit;
	setpointer(arrow!)
	g_mb.messagebox("Sep","Fase avanzamento di " + ls_cod_prodotto + " terminata con successo.",information!)
	st_state.text="Ok."	
end if
end event

type st_2 from statictext within w_produzione_prodotti
int X=46
int Y=21
int Width=2058
int Height=61
boolean Enabled=false
string Text="Vista  Prodotti Finiti"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_1 from singlelineedit within w_produzione_prodotti
int X=2707
int Y=1417
int Width=202
int Height=81
int TabOrder=60
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_produzione_prodotti
int X=2122
int Y=1425
int Width=567
int Height=61
boolean Enabled=false
string Text="Livello max di distinta"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_produzione_prodotti
int X=23
int Y=1521
int Width=567
int Height=61
boolean Enabled=false
string Text="Stato Esecuzione"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_state from statictext within w_produzione_prodotti
int X=618
int Y=1521
int Width=1212
int Height=61
boolean Enabled=false
string Text="none"
boolean FocusRectangle=false
long TextColor=255
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_produzione_prodotti
int X=23
int Y=1441
int Width=567
int Height=61
boolean Enabled=false
string Text="Prodotto corrente"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_state_1 from statictext within w_produzione_prodotti
int X=618
int Y=1441
int Width=1418
int Height=61
boolean Enabled=false
boolean FocusRectangle=false
long TextColor=255
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_interrompi from commandbutton within w_produzione_prodotti
int X=2698
int Y=1521
int Width=366
int Height=81
int TabOrder=50
string Text="&Interrompi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;ib_interrompi=true
end event

type st_1 from statictext within w_produzione_prodotti
int X=229
int Y=1341
int Width=3223
int Height=41
boolean Enabled=false
boolean Border=true
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=255
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_6 from statictext within w_produzione_prodotti
int X=23
int Y=1341
int Width=183
int Height=61
boolean Enabled=false
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type lb_1 from listbox within w_produzione_prodotti
int X=2126
int Y=81
int Width=1326
int Height=1241
int TabOrder=40
boolean HScrollBar=true
boolean VScrollBar=true
boolean Sorted=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_7 from statictext within w_produzione_prodotti
int X=2126
int Y=21
int Width=1326
int Height=61
boolean Enabled=false
string Text="Riepilogo Errori"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_10 from statictext within w_produzione_prodotti
int X=229
int Y=1341
int Width=3223
int Height=41
boolean Enabled=false
boolean Border=true
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_stampa_errori from commandbutton within w_produzione_prodotti
int X=3086
int Y=1421
int Width=366
int Height=81
int TabOrder=30
string Text="&Stampa Err."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;long Job,ll_t,ll_max
string ls_stringa_stampa

ll_max =	lb_1.TotalItems ( )
Job = PrintOpen( )

PrintDefineFont(Job, 1, "Arial", -10, 700, Variable!, Roman!, FALSE, FALSE)
PrintSetFont(Job, 1)
Print(Job,500, " ")
Print(Job,500, "ERRORI VERIFICATI IN CREAZIONE FASI PRODOTTI:")
Print(Job,500, " ")

PrintDefineFont(Job, 1, "Arial", -8, 500, Variable!, Roman!, FALSE, FALSE)
PrintSetFont(Job, 1)

for ll_t = 1 to ll_max

	lb_1.SelectItem (ll_t)
	ls_stringa_stampa = lb_1.SelectedItem ( )
	Print(Job,500, ls_stringa_stampa)

next

PrintClose(Job)
end on

type st_8 from statictext within w_produzione_prodotti
int Y=1261
int Width=572
int Height=61
boolean Enabled=false
string Text="Numero Prodotti Finiti:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_9 from statictext within w_produzione_prodotti
int X=572
int Y=1261
int Width=526
int Height=61
boolean Enabled=false
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_avvia_tutti from commandbutton within w_produzione_prodotti
event clicked pbm_bnclicked
int X=2309
int Y=1521
int Width=366
int Height=81
int TabOrder=21
string Text="Avvia &Tutti"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_prodotto,ls_errore,ls_cod_versione
long ll_numero_righe,ll_t,ll_conteggio,ll_larghezza
integer li_risposta

if sle_1.text="" or isnull(sle_1.text)  then 
	g_mb.messagebox("Sep","Attenzione! Manca il numero di livello distinta: inserire un numero (>1) e riavviare",exclamation!)
	return 
end if

cb_avvia_prodotto.enabled=false
cb_avvia_tutti.enabled=false
cb_chiudi.enabled=false
cb_stampa_errori.enabled=false
cb_interrompi.enabled=true

ib_interrompi=false

ll_numero_righe = dw_vista_prodotti_totale.rowcount()
ll_larghezza = int(st_1.width/ll_numero_righe)
st_1.width = 0
for ll_t = 1 to ll_numero_righe
	yield()
	if ib_interrompi=true then
		ll_t=ll_numero_righe
		continue
	end if

	dw_vista_prodotti_totale.SetRow ( ll_t )
	ls_cod_prodotto = dw_vista_prodotti_totale.getitemstring(dw_vista_prodotti_totale.getrow(),"cod_prodotto")
	ls_cod_versione = dw_vista_prodotti_totale.getitemstring(dw_vista_prodotti_totale.getrow(),"cod_versione")
	st_state_1.text = dw_vista_prodotti_totale.getitemstring(dw_vista_prodotti_totale.getrow(),"cod_prodotto")
	st_state.text="Verifica rep. 1° liv."

	li_risposta=f_ver_rep(ls_cod_prodotto,ls_cod_versione,ls_errore)

	if li_risposta=-1 then
		st_state.text="Errore in verifica reparti"	
		ll_conteggio++
		lb_1.additem(ls_errore)
	end if	

	DELETE FROM fasi_avanzamento  
	WHERE fasi_avanzamento.cod_azienda = :s_cs_xx.cod_azienda 
	AND  fasi_avanzamento.cod_prodotto_finito = :ls_cod_prodotto;

	st_state.text="Fasi avanzamento"
//	li_risposta=f_fasi_avanzamento(ls_cod_prodotto,long(sle_1.text),ls_cod_prodotto,ls_cod_versione,1,ls_errore)

	if li_risposta=-1 then
		st_state.text="Errore fasi avanzamento"	
		rollback;
		ll_conteggio++
		lb_1.additem(ls_errore)
	else
		commit;
	end if	
	st_1.width = ll_larghezza*ll_t
	st_6.text = string(int((ll_t/ll_numero_righe)*100)) + "%"
next

st_1.width=st_10.width

if ll_conteggio > 0 then
	g_mb.messagebox("Sep","Si sono verificati " + string(ll_conteggio) + " errori durante la creazione fasi avanzamento, consultare il riepilogo e correggere gli errori",exclamation!)
	st_state.text="Errori."	
else
	g_mb.messagebox("Sep","Fasi avanzamento completate con successo.",information!)
	st_state.text="Ok."	
end if

cb_avvia_prodotto.enabled=true
cb_avvia_tutti.enabled=true
cb_chiudi.enabled=true
cb_stampa_errori.enabled=true
cb_interrompi.enabled=false

end event

