﻿$PBExportHeader$w_carico_attrezzature.srw
$PBExportComments$Window grafico carico attrezzature
forward
global type w_carico_attrezzature from w_cs_xx_principale
end type
type dw_grafico_carico_attrezzature from uo_cs_xx_dw within w_carico_attrezzature
end type
type st_1 from statictext within w_carico_attrezzature
end type
type st_2 from statictext within w_carico_attrezzature
end type
type st_4 from statictext within w_carico_attrezzature
end type
type st_5 from statictext within w_carico_attrezzature
end type
type st_3 from statictext within w_carico_attrezzature
end type
type st_7 from statictext within w_carico_attrezzature
end type
type st_8 from statictext within w_carico_attrezzature
end type
type st_9 from statictext within w_carico_attrezzature
end type
type cb_tabella from commandbutton within w_carico_attrezzature
end type
end forward

global type w_carico_attrezzature from w_cs_xx_principale
int Width=3594
int Height=1921
boolean TitleBar=true
string Title="Carico % Categorie Attrezzature"
dw_grafico_carico_attrezzature dw_grafico_carico_attrezzature
st_1 st_1
st_2 st_2
st_4 st_4
st_5 st_5
st_3 st_3
st_7 st_7
st_8 st_8
st_9 st_9
cb_tabella cb_tabella
end type
global w_carico_attrezzature w_carico_attrezzature

event pc_setwindow;call super::pc_setwindow;dw_grafico_carico_attrezzature.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)
end event

on w_carico_attrezzature.create
int iCurrent
call w_cs_xx_principale::create
this.dw_grafico_carico_attrezzature=create dw_grafico_carico_attrezzature
this.st_1=create st_1
this.st_2=create st_2
this.st_4=create st_4
this.st_5=create st_5
this.st_3=create st_3
this.st_7=create st_7
this.st_8=create st_8
this.st_9=create st_9
this.cb_tabella=create cb_tabella
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_grafico_carico_attrezzature
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=st_2
this.Control[iCurrent+4]=st_4
this.Control[iCurrent+5]=st_5
this.Control[iCurrent+6]=st_3
this.Control[iCurrent+7]=st_7
this.Control[iCurrent+8]=st_8
this.Control[iCurrent+9]=st_9
this.Control[iCurrent+10]=cb_tabella
end on

on w_carico_attrezzature.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_grafico_carico_attrezzature)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_3)
destroy(this.st_7)
destroy(this.st_8)
destroy(this.st_9)
destroy(this.cb_tabella)
end on

type dw_grafico_carico_attrezzature from uo_cs_xx_dw within w_carico_attrezzature
int X=23
int Y=21
int Width=3521
int Height=1681
string DataObject="d_grafico_vci_carico_attrezzature"
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_data_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

type st_1 from statictext within w_carico_attrezzature
int X=23
int Y=1721
int Width=92
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long TextColor=16777215
long BackColor=255
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_carico_attrezzature
int X=412
int Y=1721
int Width=92
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long TextColor=16777215
long BackColor=65280
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_carico_attrezzature
int X=961
int Y=1721
int Width=92
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long TextColor=16777215
long BackColor=16711680
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_carico_attrezzature
int X=1555
int Y=1721
int Width=92
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long TextColor=16777215
long BackColor=65535
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_carico_attrezzature
int X=138
int Y=1721
int Width=229
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="% Totale"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_7 from statictext within w_carico_attrezzature
int X=526
int Y=1721
int Width=385
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="% Lavorazione"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_8 from statictext within w_carico_attrezzature
int X=1075
int Y=1721
int Width=394
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="% Attrezzaggio"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_9 from statictext within w_carico_attrezzature
int X=1669
int Y=1721
int Width=705
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="% Attrezzaggio Commessa"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_tabella from commandbutton within w_carico_attrezzature
int X=3178
int Y=1721
int Width=366
int Height=81
int TabOrder=2
boolean BringToTop=true
string Text="&Tabella"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open(w_impegno_cat_attrezzature,-1)
end event

