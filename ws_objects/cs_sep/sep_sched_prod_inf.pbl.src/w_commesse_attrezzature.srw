﻿$PBExportHeader$w_commesse_attrezzature.srw
$PBExportComments$Window commesse per attrezzature
forward
global type w_commesse_attrezzature from w_cs_xx_principale
end type
type dw_commesse_attrezzature from uo_cs_xx_dw within w_commesse_attrezzature
end type
type ddlb_cat_attrezzature from dropdownlistbox within w_commesse_attrezzature
end type
type st_14 from statictext within w_commesse_attrezzature
end type
type cb_grafico from commandbutton within w_commesse_attrezzature
end type
end forward

global type w_commesse_attrezzature from w_cs_xx_principale
int Width=3415
int Height=1565
boolean TitleBar=true
string Title="Commesse per Categoria Attrezzature"
dw_commesse_attrezzature dw_commesse_attrezzature
ddlb_cat_attrezzature ddlb_cat_attrezzature
st_14 st_14
cb_grafico cb_grafico
end type
global w_commesse_attrezzature w_commesse_attrezzature

event pc_setwindow;call super::pc_setwindow;dw_commesse_attrezzature.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen +c_nonew + c_nomodify + c_nodelete,c_default)

f_po_loadddlb(ddlb_cat_attrezzature, &
                 sqlca, &
                 "tab_cat_attrezzature", &
                 "cod_cat_attrezzature", &
                 "des_cat_attrezzature", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
end event

on w_commesse_attrezzature.create
int iCurrent
call w_cs_xx_principale::create
this.dw_commesse_attrezzature=create dw_commesse_attrezzature
this.ddlb_cat_attrezzature=create ddlb_cat_attrezzature
this.st_14=create st_14
this.cb_grafico=create cb_grafico
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_commesse_attrezzature
this.Control[iCurrent+2]=ddlb_cat_attrezzature
this.Control[iCurrent+3]=st_14
this.Control[iCurrent+4]=cb_grafico
end on

on w_commesse_attrezzature.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_commesse_attrezzature)
destroy(this.ddlb_cat_attrezzature)
destroy(this.st_14)
destroy(this.cb_grafico)
end on

type dw_commesse_attrezzature from uo_cs_xx_dw within w_commesse_attrezzature
int X=23
int Y=21
int Width=3338
int Height=1321
int TabOrder=10
string DataObject="d_commesse_attrezzature"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_cat_attrezzature

ls_cod_cat_attrezzature = f_po_selectddlb(ddlb_cat_attrezzature)

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_cat_attrezzature,s_cs_xx.parametri.parametro_data_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

type ddlb_cat_attrezzature from dropdownlistbox within w_commesse_attrezzature
int X=641
int Y=1361
int Width=1029
int Height=781
int TabOrder=20
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;dw_commesse_attrezzature.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type st_14 from statictext within w_commesse_attrezzature
int X=23
int Y=1381
int Width=613
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Categoria Attrezzatura:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_grafico from commandbutton within w_commesse_attrezzature
int X=2995
int Y=1361
int Width=366
int Height=81
int TabOrder=21
boolean BringToTop=true
string Text="&Grafico"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open(w_grafico_commesse_attrezzature,-1)
end event

