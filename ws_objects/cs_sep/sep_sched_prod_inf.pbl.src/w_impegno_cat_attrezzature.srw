﻿$PBExportHeader$w_impegno_cat_attrezzature.srw
$PBExportComments$Window impegno_cat_attrezzature
forward
global type w_impegno_cat_attrezzature from w_cs_xx_principale
end type
type dw_impegno_cat_attrezzature from uo_cs_xx_dw within w_impegno_cat_attrezzature
end type
end forward

global type w_impegno_cat_attrezzature from w_cs_xx_principale
int Width=3598
int Height=1825
boolean TitleBar=true
string Title="Tabella Impegno Categorie Attrezzature"
dw_impegno_cat_attrezzature dw_impegno_cat_attrezzature
end type
global w_impegno_cat_attrezzature w_impegno_cat_attrezzature

event pc_setwindow;call super::pc_setwindow;dw_impegno_cat_attrezzature.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)
end event

on w_impegno_cat_attrezzature.create
int iCurrent
call w_cs_xx_principale::create
this.dw_impegno_cat_attrezzature=create dw_impegno_cat_attrezzature
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_impegno_cat_attrezzature
end on

on w_impegno_cat_attrezzature.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_impegno_cat_attrezzature)
end on

type dw_impegno_cat_attrezzature from uo_cs_xx_dw within w_impegno_cat_attrezzature
int X=23
int Y=21
int Width=3521
int Height=1681
int TabOrder=10
string DataObject="d_impegno_cat_attrezzature"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_data_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

