﻿$PBExportHeader$w_misuratore_risorse.srw
$PBExportComments$Window Misuratore Risorse
forward
global type w_misuratore_risorse from w_cs_xx_principale
end type
type cb_calcola from commandbutton within w_misuratore_risorse
end type
type em_giorno_schedulazione from editmask within w_misuratore_risorse
end type
type st_2 from statictext within w_misuratore_risorse
end type
type cb_grafico from commandbutton within w_misuratore_risorse
end type
type em_da_data from editmask within w_misuratore_risorse
end type
type em_a_data from editmask within w_misuratore_risorse
end type
type st_da_data from statictext within w_misuratore_risorse
end type
type st_a_data from statictext within w_misuratore_risorse
end type
type st_1 from statictext within w_misuratore_risorse
end type
type gb_2 from groupbox within w_misuratore_risorse
end type
type lb_elenco_campi from listbox within w_misuratore_risorse
end type
type st_3 from statictext within w_misuratore_risorse
end type
type lb_elenco_campi_ordinati from listbox within w_misuratore_risorse
end type
type st_4 from statictext within w_misuratore_risorse
end type
type mle_2 from multilineedit within w_misuratore_risorse
end type
type cb_aggiungi from commandbutton within w_misuratore_risorse
end type
type cb_togli from commandbutton within w_misuratore_risorse
end type
type dw_ricerca from u_dw_search within w_misuratore_risorse
end type
type mle_1 from multilineedit within w_misuratore_risorse
end type
type gb_1 from groupbox within w_misuratore_risorse
end type
type em_quantita from editmask within w_misuratore_risorse
end type
type st_5 from statictext within w_misuratore_risorse
end type
type dw_elenco_prodotti_preventivo from datawindow within w_misuratore_risorse
end type
type cb_agg from commandbutton within w_misuratore_risorse
end type
type cb_tog from commandbutton within w_misuratore_risorse
end type
type ddlb_versione from dropdownlistbox within w_misuratore_risorse
end type
type st_6 from statictext within w_misuratore_risorse
end type
type cb_elimina from commandbutton within w_misuratore_risorse
end type
type rb_tutto from radiobutton within w_misuratore_risorse
end type
type rb_giorno_sched from radiobutton within w_misuratore_risorse
end type
type st_conta from statictext within w_misuratore_risorse
end type
type st_totale from statictext within w_misuratore_risorse
end type
type st_9 from statictext within w_misuratore_risorse
end type
type st_7 from statictext within w_misuratore_risorse
end type
type st_stato from statictext within w_misuratore_risorse
end type
type cbx_no_esplosione from checkbox within w_misuratore_risorse
end type
type mle_3 from multilineedit within w_misuratore_risorse
end type
type cb_commesse from commandbutton within w_misuratore_risorse
end type
type cb_tempi_fasi from commandbutton within w_misuratore_risorse
end type
end forward

global type w_misuratore_risorse from w_cs_xx_principale
integer width = 3739
integer height = 1820
string title = "Misuratore di Risorse"
cb_calcola cb_calcola
em_giorno_schedulazione em_giorno_schedulazione
st_2 st_2
cb_grafico cb_grafico
em_da_data em_da_data
em_a_data em_a_data
st_da_data st_da_data
st_a_data st_a_data
st_1 st_1
gb_2 gb_2
lb_elenco_campi lb_elenco_campi
st_3 st_3
lb_elenco_campi_ordinati lb_elenco_campi_ordinati
st_4 st_4
mle_2 mle_2
cb_aggiungi cb_aggiungi
cb_togli cb_togli
dw_ricerca dw_ricerca
mle_1 mle_1
gb_1 gb_1
em_quantita em_quantita
st_5 st_5
dw_elenco_prodotti_preventivo dw_elenco_prodotti_preventivo
cb_agg cb_agg
cb_tog cb_tog
ddlb_versione ddlb_versione
st_6 st_6
cb_elimina cb_elimina
rb_tutto rb_tutto
rb_giorno_sched rb_giorno_sched
st_conta st_conta
st_totale st_totale
st_9 st_9
st_7 st_7
st_stato st_stato
cbx_no_esplosione cbx_no_esplosione
mle_3 mle_3
cb_commesse cb_commesse
cb_tempi_fasi cb_tempi_fasi
end type
global w_misuratore_risorse w_misuratore_risorse

type variables

end variables

event pc_setwindow;call super::pc_setwindow;em_giorno_schedulazione.text = string (today())
em_da_data.text = string (today())
em_a_data.text = string (today())

lb_elenco_campi_ordinati.reset()
lb_elenco_campi.additem("num_commessa")
lb_elenco_campi.additem("cod_prodotto")
lb_elenco_campi.additem("cod_reparto")
lb_elenco_campi_ordinati.additem("cod_lavorazione")
lb_elenco_campi.additem("cod_cat_attrezzature")
lb_elenco_campi_ordinati.additem("data_consegna")


lb_elenco_campi.selectitem(1)
lb_elenco_campi_ordinati.selectitem(1)
end event

on w_misuratore_risorse.create
int iCurrent
call super::create
this.cb_calcola=create cb_calcola
this.em_giorno_schedulazione=create em_giorno_schedulazione
this.st_2=create st_2
this.cb_grafico=create cb_grafico
this.em_da_data=create em_da_data
this.em_a_data=create em_a_data
this.st_da_data=create st_da_data
this.st_a_data=create st_a_data
this.st_1=create st_1
this.gb_2=create gb_2
this.lb_elenco_campi=create lb_elenco_campi
this.st_3=create st_3
this.lb_elenco_campi_ordinati=create lb_elenco_campi_ordinati
this.st_4=create st_4
this.mle_2=create mle_2
this.cb_aggiungi=create cb_aggiungi
this.cb_togli=create cb_togli
this.dw_ricerca=create dw_ricerca
this.mle_1=create mle_1
this.gb_1=create gb_1
this.em_quantita=create em_quantita
this.st_5=create st_5
this.dw_elenco_prodotti_preventivo=create dw_elenco_prodotti_preventivo
this.cb_agg=create cb_agg
this.cb_tog=create cb_tog
this.ddlb_versione=create ddlb_versione
this.st_6=create st_6
this.cb_elimina=create cb_elimina
this.rb_tutto=create rb_tutto
this.rb_giorno_sched=create rb_giorno_sched
this.st_conta=create st_conta
this.st_totale=create st_totale
this.st_9=create st_9
this.st_7=create st_7
this.st_stato=create st_stato
this.cbx_no_esplosione=create cbx_no_esplosione
this.mle_3=create mle_3
this.cb_commesse=create cb_commesse
this.cb_tempi_fasi=create cb_tempi_fasi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_calcola
this.Control[iCurrent+2]=this.em_giorno_schedulazione
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.cb_grafico
this.Control[iCurrent+5]=this.em_da_data
this.Control[iCurrent+6]=this.em_a_data
this.Control[iCurrent+7]=this.st_da_data
this.Control[iCurrent+8]=this.st_a_data
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.gb_2
this.Control[iCurrent+11]=this.lb_elenco_campi
this.Control[iCurrent+12]=this.st_3
this.Control[iCurrent+13]=this.lb_elenco_campi_ordinati
this.Control[iCurrent+14]=this.st_4
this.Control[iCurrent+15]=this.mle_2
this.Control[iCurrent+16]=this.cb_aggiungi
this.Control[iCurrent+17]=this.cb_togli
this.Control[iCurrent+18]=this.dw_ricerca
this.Control[iCurrent+19]=this.mle_1
this.Control[iCurrent+20]=this.gb_1
this.Control[iCurrent+21]=this.em_quantita
this.Control[iCurrent+22]=this.st_5
this.Control[iCurrent+23]=this.dw_elenco_prodotti_preventivo
this.Control[iCurrent+24]=this.cb_agg
this.Control[iCurrent+25]=this.cb_tog
this.Control[iCurrent+26]=this.ddlb_versione
this.Control[iCurrent+27]=this.st_6
this.Control[iCurrent+28]=this.cb_elimina
this.Control[iCurrent+29]=this.rb_tutto
this.Control[iCurrent+30]=this.rb_giorno_sched
this.Control[iCurrent+31]=this.st_conta
this.Control[iCurrent+32]=this.st_totale
this.Control[iCurrent+33]=this.st_9
this.Control[iCurrent+34]=this.st_7
this.Control[iCurrent+35]=this.st_stato
this.Control[iCurrent+36]=this.cbx_no_esplosione
this.Control[iCurrent+37]=this.mle_3
this.Control[iCurrent+38]=this.cb_commesse
this.Control[iCurrent+39]=this.cb_tempi_fasi
end on

on w_misuratore_risorse.destroy
call super::destroy
destroy(this.cb_calcola)
destroy(this.em_giorno_schedulazione)
destroy(this.st_2)
destroy(this.cb_grafico)
destroy(this.em_da_data)
destroy(this.em_a_data)
destroy(this.st_da_data)
destroy(this.st_a_data)
destroy(this.st_1)
destroy(this.gb_2)
destroy(this.lb_elenco_campi)
destroy(this.st_3)
destroy(this.lb_elenco_campi_ordinati)
destroy(this.st_4)
destroy(this.mle_2)
destroy(this.cb_aggiungi)
destroy(this.cb_togli)
destroy(this.dw_ricerca)
destroy(this.mle_1)
destroy(this.gb_1)
destroy(this.em_quantita)
destroy(this.st_5)
destroy(this.dw_elenco_prodotti_preventivo)
destroy(this.cb_agg)
destroy(this.cb_tog)
destroy(this.ddlb_versione)
destroy(this.st_6)
destroy(this.cb_elimina)
destroy(this.rb_tutto)
destroy(this.rb_giorno_sched)
destroy(this.st_conta)
destroy(this.st_totale)
destroy(this.st_9)
destroy(this.st_7)
destroy(this.st_stato)
destroy(this.cbx_no_esplosione)
destroy(this.mle_3)
destroy(this.cb_commesse)
destroy(this.cb_tempi_fasi)
end on

type cb_calcola from commandbutton within w_misuratore_risorse
integer x = 3177
integer y = 1620
integer width = 503
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Programma Prod."
end type

event clicked;long ll_selected[],ll_num_commessa,ll_num_righe,ll_riga,ll_progressivo,ll_t,ll_num_commessa_prec,ll_totale,ll_conta
integer li_anno_commessa,li_risposta,li_anno_commessa_prec
string ls_cod_prodotto_finito,ls_cod_versione,ls_errore,ls_cod_cat_attrezzature,ls_order_by,ls_sql, & 
		 ls_cod_reparto,ls_cod_lavorazione,ls_cod_reparto_prec,ls_cod_lavorazione_prec
double ldd_quan_da_produrre,ldd_somma_ore_disponibili,ldd_tempo_lavorazione,&
		 ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa,ldd_tempo_risorsa_umana,ldd_tempo_movimentazione, &
		 ldd_perc_carico_lavorazione,ldd_perc_carico_atrz,ldd_perc_carico_atrz_com,ldd_perc_carico_risorsa_umana, &
		 ldd_perc_carico_movimentazione,ldd_perc_carico_totale,ldd_ore_disponibili
datetime ldt_data_giorno,ldt_da_data,ldt_a_data,ld_data_consegna

setpointer(hourglass!)

ldt_da_data = datetime(date(em_da_data.text), 00:00:00)
ldt_a_data = datetime(date(em_a_data.text), 00:00:00)
ldt_data_giorno = datetime(date(em_giorno_schedulazione.text), 00:00:00)
st_stato.text = "Esplosione Fasi"

//**********************
	

//**********************

if cbx_no_esplosione.checked = false then
	select  count(*)
	into    :ll_totale
	from    anag_commesse 
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     data_consegna
	between :ldt_da_data
	and     :ldt_a_data
	and     quan_ordine > quan_prodotta;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
		return
	end if
	
	st_totale.text = string(ll_totale)
	
	delete from tempi_fase_commesse
	where cod_azienda=:s_cs_xx.cod_azienda
	and data_schedulazione=:ldt_data_giorno;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
		rollback;
		return
	end if

	declare righe_commesse_produzione cursor for
	select  anno_commessa,
			  num_commessa,
			  cod_prodotto,
			  cod_versione,
			  data_consegna,
			  quan_ordine - quan_prodotta
	from    anag_commesse 
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     data_consegna
	between :ldt_da_data
	and     :ldt_a_data
	and     quan_ordine > quan_prodotta;
	
	open  righe_commesse_produzione;
	
	do while 1 = 1
		fetch righe_commesse_produzione 
		into  :li_anno_commessa,
				:ll_num_commessa,
				:ls_cod_prodotto_finito,
				:ls_cod_versione,
				:ld_data_consegna,
				:ldd_quan_da_produrre;
	
		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
			close righe_commesse_produzione;
			rollback;
			st_stato.text = ""
			return
		end if
				
		li_risposta = f_tempi_fase ( li_anno_commessa, ll_num_commessa, & 
											  ls_cod_prodotto_finito,ls_cod_prodotto_finito,ls_cod_versione, ldd_quan_da_produrre, & 
											  1, ldt_data_giorno,ls_errore)	
	
		if li_risposta < 0 then
			g_mb.messagebox("Sep",ls_errore,stopsign!)
			close righe_commesse_produzione;
			rollback;
			st_stato.text = ""
			return
		end if
		
		update tempi_fase_commesse
		set 	 data_consegna=:ld_data_consegna
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    data_schedulazione=:ldt_data_giorno
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
			close righe_commesse_produzione;
			rollback;
			st_stato.text = ""
			return
		end if
		
		ll_conta++
		
		st_conta.text = string(ll_conta)
		
	loop
	
	close righe_commesse_produzione;
	
	for ll_t = 1 to dw_elenco_prodotti_preventivo.rowcount()
		ls_cod_prodotto_finito = dw_elenco_prodotti_preventivo.getitemstring(dw_elenco_prodotti_preventivo.getrow(),"cod_prodotto")
		ls_cod_versione = dw_elenco_prodotti_preventivo.getitemstring(dw_elenco_prodotti_preventivo.getrow(),"cod_versione")	
		ldd_quan_da_produrre = dw_elenco_prodotti_preventivo.getitemnumber(dw_elenco_prodotti_preventivo.getrow(),"quantita")	
		
		setnull(li_anno_commessa)
		setnull(ll_num_commessa)
		
		li_risposta = f_tempi_fase ( li_anno_commessa, ll_num_commessa, & 
											  ls_cod_prodotto_finito,ls_cod_prodotto_finito,ls_cod_versione, ldd_quan_da_produrre, & 
											  1, ldt_data_giorno,ls_errore)	
	
		if li_risposta < 0 then
			g_mb.messagebox("Sep",ls_errore,stopsign!)
			rollback;
			st_stato.text = ""
			return
		end if
	
	next

end if //fine controllo esplosione fasi


//********
//ROUTINE CHE OTTIMIZZA I TEMPI ATTREZZAGGIO E ATTREZZAGGIO COMMESSA
st_stato.text = "Ottimizzazione Tempi"

if lb_elenco_campi_ordinati.TotalItems ( ) = 0 then
	ls_order_by ="" 
else
	ls_order_by = "order by "
	for ll_t = 1 to lb_elenco_campi_ordinati.TotalItems ( )
		lb_elenco_campi_ordinati.selectitem(ll_t)
		ls_order_by = ls_order_by + lb_elenco_campi_ordinati.selecteditem() + ","
	next
	ls_order_by = left(ls_order_by,len(ls_order_by)-1)
end if


ls_sql= "select tempo_attrezzaggio,tempo_attrezzaggio_commessa,anno_commessa,num_commessa,cod_reparto,cod_lavorazione,progressivo "+ &
		  " from tempi_fase_commesse "+ &
		  " where cod_azienda = '" + string(s_cs_xx.cod_azienda) + "'" + &
		  " and data_schedulazione='" + string(date(ldt_data_giorno),s_cs_xx.db_funzioni.formato_data) + "' " + ls_order_by

declare ottimizza_tempi dynamic cursor for sqlsa ;

prepare sqlsa from :ls_sql;

open dynamic ottimizza_tempi;

do while 1=1
	fetch ottimizza_tempi
	into  :ldd_tempo_attrezzaggio,
			:ldd_tempo_attrezzaggio_commessa,
			:li_anno_commessa,
			:ll_num_commessa,
			:ls_cod_reparto,
			:ls_cod_lavorazione,
			:ll_progressivo;

	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
		close  ottimizza_tempi;
		rollback;
		st_stato.text = ""
		return
	end if

	if ls_cod_lavorazione = ls_cod_lavorazione_prec then
		update tempi_fase_commesse
		set 	 tempo_attrezzaggio = 0
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    data_schedulazione=:ldt_data_giorno
		and    progressivo=:ll_progressivo;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
			close  ottimizza_tempi;
			rollback;
			st_stato.text = ""
			return
		end if

	end if

	if li_anno_commessa=li_anno_commessa_prec and ll_num_commessa=ll_num_commessa_prec then
		update tempi_fase_commesse
		set 	 tempo_attrezzaggio_commessa = 0
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    data_schedulazione=:ldt_data_giorno
		and    progressivo=:ll_progressivo;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
			close  ottimizza_tempi;
			rollback;
			st_stato.text = ""
			return
		end if

	end if
	
	li_anno_commessa_prec = li_anno_commessa
	ll_num_commessa_prec= ll_num_commessa
 	ls_cod_reparto_prec = ls_cod_reparto
	ls_cod_lavorazione_prec = ls_cod_lavorazione
		
loop

close  ottimizza_tempi;

ldd_tempo_attrezzaggio_commessa=0
ldd_tempo_attrezzaggio=0

//*******

ll_totale = 0
ll_conta = 0

st_stato.text = "Calcolo disponibilità"
select   count(*)
into     :ll_totale
from     tab_cal_attivita
where    cod_azienda=:s_cs_xx.cod_azienda
and      data_giorno between :ldt_da_data and :ldt_a_data
group by cod_cat_attrezzature;   

st_totale.text = string (ll_totale)

delete from impegno_cat_attrezzature
where cod_azienda=:s_cs_xx.cod_azienda
and data_schedulazione=:ldt_data_giorno;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	rollback;
	return
end if


declare  somma_ore_attivita cursor for
select   cod_cat_attrezzature,
		   sum(num_ore)
from     tab_cal_attivita
where    cod_azienda=:s_cs_xx.cod_azienda
and      data_giorno between :ldt_da_data and :ldt_a_data
group by cod_cat_attrezzature;   

open somma_ore_attivita;

do while 1=1
	fetch somma_ore_attivita 
	into  :ls_cod_cat_attrezzature,
			:ldd_somma_ore_disponibili;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
		close somma_ore_attivita;
		rollback;
		st_stato.text = ""
		return
	end if
	
	insert into impegno_cat_attrezzature  
			( cod_azienda,   
			  cod_cat_attrezzature,   
			  data_schedulazione,
			  ore_disponibili,   
			  ore_occ_lavorazione,   
			  ore_occ_atrz,   
			  ore_occ_atrz_com,   
			  ore_occ_risorsa_umana,   
			  ore_occ_movimentazione )  
   values (:s_cs_xx.cod_azienda,   
           :ls_cod_cat_attrezzature,   
			  :ldt_data_giorno,
           :ldd_somma_ore_disponibili,   
           null,   
           null,   
           null,   
           null,   
           null )  ;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
		close somma_ore_attivita;
		rollback;
		st_stato.text = ""
		return
	end if
	ll_conta ++
	st_conta.text = string(ll_conta)	
loop

close somma_ore_attivita;

ll_totale = 0
ll_conta = 0
st_stato.text = "Calcolo carico"

select   count(*)
into     :ll_totale
from     tempi_fase_commesse
where    cod_azienda=:s_cs_xx.cod_azienda
and      data_schedulazione = :ldt_data_giorno
group by cod_cat_attrezzature;   

st_totale.text = string(ll_totale)

declare  somma_ore_impegnate cursor for
select   cod_cat_attrezzature,
		   sum(tempo_lavorazione),
			sum(tempo_attrezzaggio),
			sum(tempo_attrezzaggio_commessa),
			sum(tempo_risorsa_umana),
			sum(tempo_movimentazione)
from     tempi_fase_commesse
where    cod_azienda=:s_cs_xx.cod_azienda
and      data_schedulazione = :ldt_data_giorno
group by cod_cat_attrezzature;   

open somma_ore_impegnate;

do while 1=1
	fetch somma_ore_impegnate 
	into  :ls_cod_cat_attrezzature,
			:ldd_tempo_lavorazione,
			:ldd_tempo_attrezzaggio,
			:ldd_tempo_attrezzaggio_commessa,
			:ldd_tempo_risorsa_umana,
			:ldd_tempo_movimentazione;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
		close somma_ore_impegnate;
		rollback;
		st_stato.text = ""
		return
	end if
	
//	select ore_disponibili
//	into   :ldd_ore_disponibili
//	from   impegno_cat_attrezzature
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature;
//	
//	if sqlca.sqlcode < 0 then
//		messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
//		close somma_ore_impegnate;
//		rollback;
//		return
//	end if
	
	ldd_tempo_lavorazione = round(ldd_tempo_lavorazione / 60,4)
	ldd_tempo_attrezzaggio = round(ldd_tempo_attrezzaggio / 60,4)
	ldd_tempo_attrezzaggio_commessa = round(ldd_tempo_attrezzaggio_commessa / 60,4)
	ldd_tempo_risorsa_umana = round(ldd_tempo_risorsa_umana / 60,4)
	ldd_tempo_movimentazione = round(ldd_tempo_movimentazione / 60,4)
	
//	ldd_perc_carico_lavorazione = round(ldd_tempo_lavorazione/ldd_ore_disponibili*100,4)
//	ldd_perc_carico_atrz = round(ldd_tempo_attrezzaggio/ldd_ore_disponibili*100,4)
//	ldd_perc_carico_atrz_com = round(ldd_tempo_attrezzaggio_commessa/ldd_ore_disponibili*100,4)
//	ldd_perc_carico_movimentazione = round(ldd_tempo_movimentazione/ldd_ore_disponibili*100,4)
//	ldd_perc_carico_risorsa_umana = round(ldd_tempo_risorsa_umana/ldd_ore_disponibili*100,4)
//	ldd_perc_carico_totale = round((ldd_tempo_risorsa_umana+ldd_tempo_lavorazione+ldd_tempo_attrezzaggio+ldd_tempo_attrezzaggio_commessa + ldd_tempo_movimentazione)/ldd_ore_disponibili*100,4)

	update impegno_cat_attrezzature  
	set    ore_occ_lavorazione = :ldd_tempo_lavorazione,
			 ore_occ_atrz=:ldd_tempo_attrezzaggio,
			 ore_occ_atrz_com=:ldd_tempo_attrezzaggio_commessa,
			 ore_occ_risorsa_umana=:ldd_tempo_risorsa_umana,
			 ore_occ_movimentazione=:ldd_tempo_movimentazione
//			 perc_carico_lavorazione=:ldd_perc_carico_lavorazione,
//			 perc_carico_atrz=:ldd_perc_carico_atrz,
//			 perc_carico_atrz_com=:ldd_perc_carico_atrz_com,
//			 perc_carico_risorsa_umana=:ldd_perc_carico_risorsa_umana,
//			 perc_carico_movimentazione=:ldd_perc_carico_movimentazione,
//			 perc_carico_totale=:ldd_perc_carico_totale
			 
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cat_attrezzature=:ls_cod_cat_attrezzature
	and    data_schedulazione=:ldt_data_giorno;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
		close somma_ore_impegnate;
		rollback;
		st_stato.text = ""
		return
	end if
	ll_conta ++
	st_conta.text = string(ll_conta)	
	
loop

close somma_ore_impegnate;

commit;

st_stato.text = ""
st_totale.text = ""
st_conta.text = ""

setpointer(arrow!)
end event

type em_giorno_schedulazione from editmask within w_misuratore_risorse
integer x = 731
integer y = 380
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "¬"
end type

type st_2 from statictext within w_misuratore_risorse
integer x = 91
integer y = 400
integer width = 640
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Giorno di Schedulazione:"
boolean focusrectangle = false
end type

type cb_grafico from commandbutton within w_misuratore_risorse
integer x = 1531
integer y = 1620
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Grafico"
end type

event clicked;s_cs_xx.parametri.parametro_data_1 = datetime(date(em_giorno_schedulazione.text), 00:00:00)
window_open(w_carico_attrezzature,-1)

end event

type em_da_data from editmask within w_misuratore_risorse
integer x = 183
integer y = 560
integer width = 389
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

type em_a_data from editmask within w_misuratore_risorse
integer x = 754
integer y = 560
integer width = 389
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

type st_da_data from statictext within w_misuratore_risorse
integer x = 69
integer y = 560
integer width = 114
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "da:"
boolean focusrectangle = false
end type

type st_a_data from statictext within w_misuratore_risorse
integer x = 640
integer y = 560
integer width = 91
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "a:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_misuratore_risorse
integer x = 23
integer y = 20
integer width = 3657
integer height = 100
boolean bringtotop = true
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Misuratore di Risorse a Capacità Finita e Programmazione Produzione"
alignment alignment = center!
boolean focusrectangle = false
end type

type gb_2 from groupbox within w_misuratore_risorse
integer x = 23
integer y = 1020
integer width = 2537
integer height = 580
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Ottimizzazione dei Tempi - Prevalenze di Ordinamento"
end type

type lb_elenco_campi from listbox within w_misuratore_risorse
integer x = 69
integer y = 1180
integer width = 754
integer height = 380
integer taborder = 210
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type st_3 from statictext within w_misuratore_risorse
integer x = 69
integer y = 1120
integer width = 434
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Elenco Campi:"
boolean focusrectangle = false
end type

type lb_elenco_campi_ordinati from listbox within w_misuratore_risorse
integer x = 983
integer y = 1180
integer width = 754
integer height = 380
integer taborder = 160
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
boolean sorted = false
borderstyle borderstyle = stylelowered!
end type

type st_4 from statictext within w_misuratore_risorse
integer x = 983
integer y = 1120
integer width = 640
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Sequenza Ordinamento:"
boolean focusrectangle = false
end type

type mle_2 from multilineedit within w_misuratore_risorse
integer x = 1783
integer y = 1100
integer width = 754
integer height = 460
integer taborder = 150
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "N.B. : L~'impostazione  dell~'ordinamento consente di ottimizzare in diversi modi i tempi di attrezzaggio e attrezzaggio commesse nonchè la priorità di schedulazione."
boolean border = false
boolean displayonly = true
end type

type cb_aggiungi from commandbutton within w_misuratore_risorse
integer x = 846
integer y = 1260
integer width = 114
integer height = 80
integer taborder = 200
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">>"
end type

event clicked;lb_elenco_campi_ordinati.additem(lb_elenco_campi.selecteditem())

lb_elenco_campi.DeleteItem(lb_elenco_campi.SelectedIndex( ))

lb_elenco_campi_ordinati.selectitem(1)
lb_elenco_campi.selectitem(1)
end event

type cb_togli from commandbutton within w_misuratore_risorse
integer x = 846
integer y = 1380
integer width = 119
integer height = 80
integer taborder = 170
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<<"
end type

event clicked;lb_elenco_campi.additem(lb_elenco_campi_ordinati.selecteditem())

lb_elenco_campi_ordinati.DeleteItem(lb_elenco_campi_ordinati.SelectedIndex())
lb_elenco_campi_ordinati.selectitem(1)
lb_elenco_campi.selectitem(1)
end event

type dw_ricerca from u_dw_search within w_misuratore_risorse
event ue_key pbm_dwnkey
integer x = 23
integer y = 140
integer width = 2437
integer height = 100
integer taborder = 20
string dataobject = "d_distinta_padri_cerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
end choose
end event

type mle_1 from multilineedit within w_misuratore_risorse
integer x = 69
integer y = 660
integer width = 1760
integer height = 300
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "N.B. : sono selezionate le commesse con data consegna compresa nel periodo indicato e con quantità in ordine maggiore della quantita già prodotta. La quantità per il calcolo dell~'impegno è data dalla differenza tra la quantità in ordine e la quantità prodotta. Il periodo indicato permette di selezionare il calendario attività da considerare"
boolean border = false
boolean displayonly = true
end type

type gb_1 from groupbox within w_misuratore_risorse
integer x = 23
integer y = 480
integer width = 1851
integer height = 520
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Selezione commesse e periodo di calendario attività "
end type

type em_quantita from editmask within w_misuratore_risorse
integer x = 2926
integer y = 160
integer width = 480
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "##,###,###.0000"
string displaydata = ""
end type

type st_5 from statictext within w_misuratore_risorse
integer x = 2651
integer y = 180
integer width = 247
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Quantità:"
boolean focusrectangle = false
end type

type dw_elenco_prodotti_preventivo from datawindow within w_misuratore_risorse
integer x = 2080
integer y = 260
integer width = 1600
integer height = 740
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_elenco_prodotti_preventivo"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_agg from commandbutton within w_misuratore_risorse
integer x = 1509
integer y = 260
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi  >>"
end type

event clicked;string ls_cod_prodotto,ls_cod_versione
double ldd_quantita
long ll_num_righe

ls_cod_prodotto = dw_ricerca.getitemstring(dw_ricerca.getrow(),"rs_cod_prodotto")

if isnull(ls_cod_prodotto) then
	g_mb.messagebox("Sep","Attenzione! Non è stato selezionato alcun prodotto.",stopsign!)
	return -1
end if

ldd_quantita = double(em_quantita.text)

if ldd_quantita = 0 then
	g_mb.messagebox("Sep","Attenzione! La quantità deve essere maggiore di zero.",stopsign!)
	return -1
end if

ls_cod_versione = f_po_selectddlb(ddlb_versione)

if isnull(ls_cod_versione) or ls_cod_versione = "" then
	g_mb.messagebox("Sep","Attenzione! Non è stata selezionata alcuna versione.",stopsign!)
	return -1
end if

//ll_num_righe = dw_elenco_prodotti_preventivo.rowcount()

dw_elenco_prodotti_preventivo.insertrow(1)
dw_elenco_prodotti_preventivo.setitem(1,"cod_prodotto",ls_cod_prodotto)
dw_elenco_prodotti_preventivo.setitem(1,"cod_versione",ls_cod_versione)
dw_elenco_prodotti_preventivo.setitem(1,"quantita",ldd_quantita)

end event

type cb_tog from commandbutton within w_misuratore_risorse
integer x = 1509
integer y = 360
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Togli       <<"
end type

event clicked;dw_elenco_prodotti_preventivo.deleterow(dw_elenco_prodotti_preventivo.getrow())
end event

type ddlb_versione from dropdownlistbox within w_misuratore_risorse
integer x = 274
integer y = 260
integer width = 480
integer height = 580
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event getfocus;ddlb_versione.reset()
	
	f_po_loadddlb(ddlb_versione, &
					  sqlca, &
				  "distinta_padri", &
				  "cod_versione", &
				  "cod_versione", &
				  "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + dw_ricerca.getitemstring(dw_ricerca.getrow(),"rs_cod_prodotto") + "'","")

end event

type st_6 from statictext within w_misuratore_risorse
integer x = 23
integer y = 260
integer width = 247
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Versione"
boolean focusrectangle = false
end type

type cb_elimina from commandbutton within w_misuratore_risorse
integer x = 23
integer y = 1620
integer width = 366
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina"
end type

event clicked;datetime ldt_giorno

if rb_tutto.checked = true then
	delete from tempi_fase_commesse
	where cod_azienda=:s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	delete from impegno_cat_attrezzature
	where cod_azienda=:s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
end if

if rb_giorno_sched.checked = true then
	ldt_giorno = datetime(date(em_giorno_schedulazione.text), 00:00:00)
	
	delete from tempi_fase_commesse
	where cod_azienda=:s_cs_xx.cod_azienda
	and data_schedulazione=:ldt_giorno;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	delete from impegno_cat_attrezzature
	where cod_azienda=:s_cs_xx.cod_azienda
	and data_schedulazione=:ldt_giorno;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
end if

g_mb.messagebox("Sep","Eliminazione avvenuta con successo",information!)
end event

type rb_tutto from radiobutton within w_misuratore_risorse
integer x = 411
integer y = 1620
integer width = 247
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tutto"
boolean checked = true
end type

type rb_giorno_sched from radiobutton within w_misuratore_risorse
integer x = 663
integer y = 1620
integer width = 457
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Giorno Sched."
end type

type st_conta from statictext within w_misuratore_risorse
integer x = 2811
integer y = 1200
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
alignment alignment = right!
boolean border = true
boolean focusrectangle = false
end type

type st_totale from statictext within w_misuratore_risorse
integer x = 3200
integer y = 1200
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
alignment alignment = right!
boolean border = true
boolean focusrectangle = false
end type

type st_9 from statictext within w_misuratore_risorse
integer x = 3154
integer y = 1200
integer width = 46
integer height = 100
boolean bringtotop = true
integer textsize = -16
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "/"
boolean focusrectangle = false
end type

type st_7 from statictext within w_misuratore_risorse
integer x = 2789
integer y = 1020
integer width = 763
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Stato della Programmazione"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_stato from statictext within w_misuratore_risorse
integer x = 2789
integer y = 1100
integer width = 777
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type cbx_no_esplosione from checkbox within w_misuratore_risorse
integer x = 2606
integer y = 1620
integer width = 558
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "No esplosione fasi"
boolean lefttext = true
end type

type mle_3 from multilineedit within w_misuratore_risorse
integer x = 2789
integer y = 1320
integer width = 777
integer height = 280
integer taborder = 190
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "No esplosione fasi quando non si cambia il periodo e non si aggiungono nuovi prodotti"
boolean border = false
end type

type cb_commesse from commandbutton within w_misuratore_risorse
integer x = 1143
integer y = 1620
integer width = 366
integer height = 80
integer taborder = 180
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Commesse"
end type

event clicked;s_cs_xx.parametri.parametro_data_1 = datetime(date(em_giorno_schedulazione.text), 00:00:00)
window_open(w_commesse_attrezzature,-1)
end event

type cb_tempi_fasi from commandbutton within w_misuratore_risorse
integer x = 1920
integer y = 1620
integer width = 663
integer height = 80
integer taborder = 91
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Visualizza Programma"
end type

event clicked;s_cs_xx.parametri.parametro_data_1 = datetime(date(em_giorno_schedulazione.text), 00:00:00)
window_open(w_tempi_fase,-1)

end event

