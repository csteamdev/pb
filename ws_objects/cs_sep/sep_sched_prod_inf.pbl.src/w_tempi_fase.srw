﻿$PBExportHeader$w_tempi_fase.srw
$PBExportComments$Window Tempi Fase
forward
global type w_tempi_fase from w_cs_xx_principale
end type
type st_2 from statictext within w_tempi_fase
end type
type st_1 from statictext within w_tempi_fase
end type
type dw_tempi_fase from uo_cs_xx_dw within w_tempi_fase
end type
end forward

global type w_tempi_fase from w_cs_xx_principale
integer width = 3593
integer height = 1920
string title = "Programma di Produzione"
st_2 st_2
st_1 st_1
dw_tempi_fase dw_tempi_fase
end type
global w_tempi_fase w_tempi_fase

event pc_setwindow;call super::pc_setwindow;dw_tempi_fase.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)
end event

on w_tempi_fase.create
int iCurrent
call super::create
this.st_2=create st_2
this.st_1=create st_1
this.dw_tempi_fase=create dw_tempi_fase
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_2
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_tempi_fase
end on

on w_tempi_fase.destroy
call super::destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_tempi_fase)
end on

type st_2 from statictext within w_tempi_fase
integer x = 2491
integer y = 60
integer width = 1051
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
string text = "(i tempi sono espressi in minuti)"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within w_tempi_fase
integer x = 23
integer y = 20
integer width = 1349
integer height = 100
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
string text = "Programma di Produzione"
boolean focusrectangle = false
end type

type dw_tempi_fase from uo_cs_xx_dw within w_tempi_fase
integer x = 23
integer y = 140
integer width = 3520
integer height = 1660
integer taborder = 10
string dataobject = "d_tempi_fase"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_data_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

