﻿$PBExportHeader$w_report_commesse.srw
$PBExportComments$report commesse con distinta base
forward
global type w_report_commesse from w_cs_xx_principale
end type
type dw_ricerca from u_dw_search within w_report_commesse
end type
type cb_1 from commandbutton within w_report_commesse
end type
type st_4 from statictext within w_report_commesse
end type
type em_1 from editmask within w_report_commesse
end type
type st_5 from statictext within w_report_commesse
end type
type em_2 from editmask within w_report_commesse
end type
type cb_aggiorna from commandbutton within w_report_commesse
end type
type dw_tes_lista_stato_commesse from uo_cs_xx_dw within w_report_commesse
end type
end forward

global type w_report_commesse from w_cs_xx_principale
integer width = 3465
integer height = 1516
string title = "Report Commesse"
dw_ricerca dw_ricerca
cb_1 cb_1
st_4 st_4
em_1 em_1
st_5 st_5
em_2 em_2
cb_aggiorna cb_aggiorna
dw_tes_lista_stato_commesse dw_tes_lista_stato_commesse
end type
global w_report_commesse w_report_commesse

type variables
string is_cod_attivita
string is_cod_cat_attrezzature
string is_cod_attrezzatura
datetime id_data_giorno
end variables

event pc_setwindow;call super::pc_setwindow;dw_tes_lista_stato_commesse.ib_dw_report = true

dw_tes_lista_stato_commesse.set_dw_options(sqlca,pcca.null_object, & 
													c_nonew + c_nomodify + c_nodelete + c_disablecc , & 
													c_disableccinsert)


iuo_dw_main = dw_tes_lista_stato_commesse

em_1.text = string(RelativeDate(today(),  - 30), "dd/mm/yyyy")

em_2.text = string( today(), "dd/mm/yyyy")
end event

on w_report_commesse.create
int iCurrent
call super::create
this.dw_ricerca=create dw_ricerca
this.cb_1=create cb_1
this.st_4=create st_4
this.em_1=create em_1
this.st_5=create st_5
this.em_2=create em_2
this.cb_aggiorna=create cb_aggiorna
this.dw_tes_lista_stato_commesse=create dw_tes_lista_stato_commesse
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.st_4
this.Control[iCurrent+4]=this.em_1
this.Control[iCurrent+5]=this.st_5
this.Control[iCurrent+6]=this.em_2
this.Control[iCurrent+7]=this.cb_aggiorna
this.Control[iCurrent+8]=this.dw_tes_lista_stato_commesse
end on

on w_report_commesse.destroy
call super::destroy
destroy(this.dw_ricerca)
destroy(this.cb_1)
destroy(this.st_4)
destroy(this.em_1)
destroy(this.st_5)
destroy(this.em_2)
destroy(this.cb_aggiorna)
destroy(this.dw_tes_lista_stato_commesse)
end on

type dw_ricerca from u_dw_search within w_report_commesse
event ue_key pbm_dwnkey
integer x = 23
integer y = 1200
integer width = 2414
integer height = 120
integer taborder = 120
string dataobject = "d_distinta_padri_cerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
end choose
end event

type cb_1 from commandbutton within w_report_commesse
integer x = 2971
integer y = 1240
integer width = 439
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Distinta &Base"
end type

event clicked;long ll_riga 

ll_riga = dw_tes_lista_stato_commesse.getrow()

if ll_riga < 1 then return -1

s_cs_xx.parametri.parametro_s_1 = dw_tes_lista_stato_commesse.getitemstring( ll_riga, "cod_prodotto")

s_cs_xx.parametri.parametro_s_2 = dw_tes_lista_stato_commesse.getitemstring( ll_riga, "cod_versione")

s_cs_xx.parametri.parametro_d_1 = dw_tes_lista_stato_commesse.getitemnumber( ll_riga, "anno_commessa")

s_cs_xx.parametri.parametro_d_2 = dw_tes_lista_stato_commesse.getitemnumber( ll_riga, "num_commessa")

window_open( w_report_varianti_commesse, -1)
end event

type st_4 from statictext within w_report_commesse
integer x = 23
integer y = 1320
integer width = 219
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "da data:"
boolean focusrectangle = false
end type

type em_1 from editmask within w_report_commesse
integer x = 251
integer y = 1320
integer width = 411
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "0"
double increment = 1
end type

type st_5 from statictext within w_report_commesse
integer x = 686
integer y = 1320
integer width = 187
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "a data:"
boolean focusrectangle = false
end type

type em_2 from editmask within w_report_commesse
integer x = 869
integer y = 1320
integer width = 411
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "<€w_grafico_dist_comm"
double increment = 1
end type

type cb_aggiorna from commandbutton within w_report_commesse
integer x = 1943
integer y = 1320
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;dw_tes_lista_stato_commesse.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type dw_tes_lista_stato_commesse from uo_cs_xx_dw within w_report_commesse
integer x = 23
integer y = 20
integer width = 3383
integer height = 1180
integer taborder = 60
string dataobject = "d_report_commesse_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
datetime ld_data_inizio,ld_data_fine
string   ls_cod_prodotto

ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))
ls_cod_prodotto = dw_ricerca.getitemstring(dw_ricerca.getrow(),"rs_cod_prodotto")

if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
	ls_cod_prodotto = "%"
end if

l_Error = Retrieve(s_cs_xx.cod_azienda,ld_data_inizio,ld_data_fine, ls_cod_prodotto)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

