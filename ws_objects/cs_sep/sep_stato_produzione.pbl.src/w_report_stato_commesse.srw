﻿$PBExportHeader$w_report_stato_commesse.srw
$PBExportComments$report commesse con distinta base
forward
global type w_report_stato_commesse from w_cs_xx_principale
end type
type tab_1 from tab within w_report_stato_commesse
end type
type tab_selezione from userobject within tab_1
end type
type dw_selezione from uo_std_dw within tab_selezione
end type
type tab_selezione from userobject within tab_1
dw_selezione dw_selezione
end type
type tab_report from userobject within tab_1
end type
type cb_2 from commandbutton within tab_report
end type
type cb_1 from commandbutton within tab_report
end type
type dw_report from uo_std_dw within tab_report
end type
type tab_report from userobject within tab_1
cb_2 cb_2
cb_1 cb_1
dw_report dw_report
end type
type tab_1 from tab within w_report_stato_commesse
tab_selezione tab_selezione
tab_report tab_report
end type
end forward

global type w_report_stato_commesse from w_cs_xx_principale
integer width = 4695
integer height = 2716
string title = "Report Controllo Produzione"
tab_1 tab_1
end type
global w_report_stato_commesse w_report_stato_commesse

forward prototypes
public function integer wf_report ()
public function long wf_calcola_tempo_lavorazione (long al_anno_commessa, long al_num_commessa, string as_cod_prodotto_reparto, string as_cod_reparto, string as_cod_operaio, ref long al_tot_minuti, ref string as_message)
end prototypes

public function integer wf_report ();string ls_sql, ls_messaggio, ls_log_errori, ls_cod_operaio_inizio,ls_cod_operaio_fine,ls_cod_reparto,ls_cod_lavorazione,ls_flag_dettaglio, &
		ls_cod_prodotto_inizio, ls_cod_prodotto_fine,ls_flag_totali_parziali, ls_cod_attrezzatura
long	ll_rows, ll_i, ll_riga, ll_anno_commessa,ll_num_commessa_inizio, ll_num_commessa_fine, ll_tot_minuti, ll_anno_comm, ll_num_comm, ll_anno_comm_prec, ll_num_comm_prec
dec{4} ld_costo_manodopera, ld_quan_prodotta_commessa
datetime ldt_data_prod_inizio,ldt_data_prod_fine
datastore lds_data

// SQL BASE da cui estraggo le fasi eseguite per operaio

ll_anno_commessa = tab_1.tab_selezione.dw_selezione.getitemnumber(1,"anno_commessa")
ll_num_commessa_inizio = tab_1.tab_selezione.dw_selezione.getitemnumber(1,"num_commessa_inizio")
ll_num_commessa_fine = tab_1.tab_selezione.dw_selezione.getitemnumber(1,"num_commessa_fine")
ldt_data_prod_inizio = datetime(tab_1.tab_selezione.dw_selezione.getitemdate(1,"data_prod_inizio"),00:00:00)
ldt_data_prod_fine = datetime(tab_1.tab_selezione.dw_selezione.getitemdate(1,"data_prod_fine"),00:00:00)
ls_cod_operaio_inizio = tab_1.tab_selezione.dw_selezione.getitemstring(1,"cod_operaio_inizio")
ls_cod_operaio_fine = tab_1.tab_selezione.dw_selezione.getitemstring(1,"cod_operaio_fine")
ls_cod_reparto = tab_1.tab_selezione.dw_selezione.getitemstring(1,"cod_reparto")
ls_cod_lavorazione = tab_1.tab_selezione.dw_selezione.getitemstring(1,"cod_lavorazione")
ls_flag_dettaglio = tab_1.tab_selezione.dw_selezione.getitemstring(1,"flag_dettaglio")
ls_cod_attrezzatura = tab_1.tab_selezione.dw_selezione.getitemstring(1,"cod_attrezzatura")

ls_cod_prodotto_inizio = tab_1.tab_selezione.dw_selezione.getitemstring(1,"cod_prodotto_inizio")
ls_cod_prodotto_fine = tab_1.tab_selezione.dw_selezione.getitemstring(1,"cod_prodotto_inizio")

ls_sql = " &
select COM.anno_commessa, COM.num_commessa, COM.cod_prodotto cod_prodotto_finito, isnull(COM.quan_prodotta,0) , isnull(COM.quan_ordine,0), isnull(COM.quan_prodotta,0), isnull(COM.quan_in_produzione,0), COM.flag_tipo_avanzamento, &
ANAG.des_prodotto, &
DET.prog_riga, &
AVANZ.cod_prodotto cod_prodotto_reparto, AVANZ.cod_reparto, AVANZ.cod_lavorazione, isnull(AVANZ.quan_in_produzione,0), isnull(AVANZ.quan_prodotta,0), &
ANAG1.des_prodotto, &
O.cod_operaio, OPE.cognome, OPE.nome, OPE.cod_cat_attrezzature, CAT.costo_medio_orario, &
REP.des_reparto, &
sum( isnull(O.quan_prodotta,0) ),  &
AVANZ.ordinamento &
from anag_commesse COM &
left join det_anag_commesse DET on COM.cod_azienda=DET.cod_azienda and COM.anno_commessa=DET.anno_commessa and COM.num_commessa=DET.num_commessa &
left join avan_produzione_com AVANZ on DET.cod_azienda=AVANZ.cod_azienda and DET.anno_commessa=AVANZ.anno_commessa and DET.num_commessa=AVANZ.num_commessa and DET.prog_riga=AVANZ.prog_riga  &
left join det_orari_produzione O on AVANZ.cod_azienda=O.cod_azienda and AVANZ.anno_commessa=O.anno_commessa and AVANZ.num_commessa=O.num_commessa and AVANZ.prog_riga=O.prog_riga and AVANZ.cod_prodotto=O.cod_prodotto and AVANZ.cod_reparto=O.cod_reparto and AVANZ.cod_lavorazione=O.cod_lavorazione  &
left join anag_prodotti ANAG on COM.cod_azienda=ANAG.cod_azienda and COM.cod_prodotto=ANAG.cod_prodotto  &
left join anag_prodotti ANAG1 on AVANZ.cod_azienda=ANAG1.cod_azienda and AVANZ.cod_prodotto=ANAG1.cod_prodotto  &
left join anag_operai OPE on O.cod_azienda=OPE.cod_azienda and O.cod_operaio=OPE.cod_operaio  &
left join tab_cat_attrezzature CAT on OPE.cod_azienda=CAT.cod_azienda and OPE.cod_cat_attrezzature=CAT.cod_cat_attrezzature &
left join anag_reparti REP on AVANZ.cod_azienda=REP.cod_azienda and AVANZ.cod_reparto=REP.cod_reparto &
"


// imposto where
ls_sql += g_str.format(" where COM.cod_azienda='$1' " ,s_cs_xx.cod_azienda)

if ll_anno_commessa > 0 and not isnull(ll_anno_commessa) then ls_sql += g_str.format(" and COM.anno_commessa=$1 " , ll_anno_commessa)

if ll_num_commessa_inizio > 0 and not isnull(ll_num_commessa_inizio) then ls_sql += g_str.format(" and COM.num_commessa >= $1  " ,ll_num_commessa_inizio)

if ll_num_commessa_fine > 0 and not isnull(ll_num_commessa_fine) then ls_sql += g_str.format(" and COM.num_commessa <= $1  " ,ll_num_commessa_fine)

if not isnull(ldt_data_prod_inizio) then ls_sql += g_str.format(" and O.data_giorno >= '$1'  " ,string(ldt_data_prod_inizio,s_cs_xx.db_funzioni.formato_data))

if not isnull(ldt_data_prod_fine) then ls_sql += g_str.format(" and O.data_giorno <= '$1'  " ,string(ldt_data_prod_fine,s_cs_xx.db_funzioni.formato_data))

if len(ls_cod_operaio_inizio) > 0 and not isnull(ls_cod_operaio_inizio) then ls_sql += g_str.format(" and O.cod_operaio >= '$1'  " ,ls_cod_operaio_inizio)

if len(ls_cod_operaio_fine) > 0 and not isnull(ls_cod_operaio_fine) then ls_sql += g_str.format(" and O.cod_operaio <= '$1'  " ,ls_cod_operaio_fine)

if not isnull(ls_cod_reparto) then ls_sql += g_str.format(" and O.cod_reparto = '$1'  " ,ls_cod_reparto)

if not isnull(ls_cod_lavorazione) then ls_sql += g_str.format(" and O.cod_lavorazione = '$1'  " ,ls_cod_lavorazione)

if len(ls_cod_prodotto_inizio) > 0 and not isnull(ls_cod_prodotto_inizio) then ls_sql += g_str.format(" and COM.cod_prodotto >= '$1'  " ,ls_cod_prodotto_inizio)

if len(ls_cod_prodotto_fine) > 0 and not isnull(ls_cod_prodotto_fine) then ls_sql += g_str.format(" and COM.cod_prodotto <= '$1'  " ,ls_cod_prodotto_fine)

// in questo modo tiro fuori solo le fasi completate
ls_sql += " and O.quan_prodotta>0 "

// imposto group by
ls_sql += " &
group by &
COM.anno_commessa, COM.num_commessa, COM.cod_prodotto, COM.quan_prodotta , COM.quan_ordine, COM.quan_prodotta, COM.quan_in_produzione, COM.flag_tipo_avanzamento, &
ANAG.cod_prodotto, ANAG.des_prodotto, &
DET.prog_riga,&
AVANZ.cod_prodotto, AVANZ.cod_reparto, AVANZ.cod_lavorazione, AVANZ.quan_in_produzione, AVANZ.quan_prodotta, &
ANAG1.des_prodotto, &
O.cod_operaio, OPE.cognome, OPE.nome, OPE.cod_cat_attrezzature, CAT.costo_medio_orario, &
REP.des_reparto, &
AVANZ.ordinamento &
order by COM.anno_commessa, COM.num_commessa, AVANZ.ordinamento "


ll_rows = guo_functions.uof_crea_datastore( lds_data, ls_sql)

tab_1.tab_report.dw_report.reset()

if ls_flag_dettaglio = "S" then
	tab_1.tab_report.dw_report.Modify("DataWindow.Detail.Height=80")
else
	tab_1.tab_report.dw_report.Modify("DataWindow.Detail.Height=0")
	
end if

ll_anno_comm_prec = 0
ll_num_comm_prec = 0

for ll_i = 1 to ll_rows
	ll_riga = tab_1.tab_report.dw_report.insertrow(0)
	
	ll_anno_comm = lds_data.getitemnumber(ll_i,1)
	ll_num_comm = lds_data.getitemnumber(ll_i,2)
	
	if ll_anno_comm <> ll_anno_comm_prec or ll_num_comm <> ll_num_comm_prec then
		ld_quan_prodotta_commessa = lds_data.getitemnumber(ll_i,4)
		if ld_quan_prodotta_commessa = 0 or isnull(ld_quan_prodotta_commessa) then
			// ricerco per la commessa i prezzi prodotti dall'ultima fase chiusa
			select 	top 1 quan_prodotta
			into		:ld_quan_prodotta_commessa
			from 		avan_produzione_com
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						anno_commessa=:ll_anno_comm and 
						num_commessa=:ll_num_comm and 
						quan_in_produzione = 0
			order by ordinamento desc;
			ls_flag_totali_parziali = "S"
		else
			ls_flag_totali_parziali = "N"
		end if
		ll_anno_comm_prec = ll_anno_comm
		ll_num_comm_prec = ll_num_comm 
	end if
	tab_1.tab_report.dw_report.setitem(ll_riga, "quan_prodotta_commessa", ld_quan_prodotta_commessa)
	tab_1.tab_report.dw_report.setitem(ll_riga, "flag_totali_parziali", ls_flag_totali_parziali)
	tab_1.tab_report.dw_report.setitem(ll_riga, "anno_commessa", lds_data.getitemnumber(ll_i,1))
	tab_1.tab_report.dw_report.setitem(ll_riga, "num_commessa", lds_data.getitemnumber(ll_i,2))
	tab_1.tab_report.dw_report.setitem(ll_riga, "cod_prodotto", lds_data.getitemstring(ll_i,3))
	tab_1.tab_report.dw_report.setitem(ll_riga, "des_prodotto", lds_data.getitemstring(ll_i,9))
	tab_1.tab_report.dw_report.setitem(ll_riga, "operaio", g_str.format( "$1 $2", lds_data.getitemstring(ll_i,18), lds_data.getitemstring(ll_i,19) ) )
	tab_1.tab_report.dw_report.setitem(ll_riga, "fase_lavorazione", lds_data.getitemstring(ll_i,22) ) 
	tab_1.tab_report.dw_report.setitem(ll_riga, "quan_prodotta_fase", lds_data.getitemnumber(ll_i,23) ) 
	tab_1.tab_report.dw_report.setitem(ll_riga, "ordinamento_reparti", lds_data.getitemnumber(ll_i,24) ) 
	ld_costo_manodopera = lds_data.getitemnumber(ll_i,21)
	if isnull(ld_costo_manodopera) then ld_costo_manodopera = 0
	tab_1.tab_report.dw_report.setitem(ll_riga, "costo_manodopoera", ld_costo_manodopera ) 
	
	ls_messaggio = ""
	if wf_calcola_tempo_lavorazione(lds_data.getitemnumber(ll_i,1), lds_data.getitemnumber(ll_i,2), lds_data.getitemstring(ll_i,11),lds_data.getitemstring(ll_i,12),lds_data.getitemstring(ll_i,17), ll_tot_minuti, ls_messaggio) < 0 then
		ls_log_errori += ls_messaggio
	else
		tab_1.tab_report.dw_report.setitem(ll_riga, "minuti_totali", ll_tot_minuti ) 
	end if
	/*
	"quan_prodotta_fase" 15
	"minuti_totali" (prog_riga=10   cod_prodotto_fase=11    reparto=12 lavorazione=13 cod_operaio=17)
	costo medio orario = 21
	*/
	
	
next

tab_1.tab_report.dw_report.groupcalc()
return 0

end function

public function long wf_calcola_tempo_lavorazione (long al_anno_commessa, long al_num_commessa, string as_cod_prodotto_reparto, string as_cod_reparto, string as_cod_operaio, ref long al_tot_minuti, ref string as_message);string ls_sql
long ll_i, ll_ret, ll_row, ll_minuti
time lt_1, lt_2
datastore lds_data

ls_sql = " &
select prog_orari, flag_inizio, orario_attrezzaggio, orario_lavorazione, data_giorno &
from det_orari_produzione "

ls_sql += g_str.format(" where cod_azienda = '$1' and anno_commessa=$2 and num_commessa=$3 and cod_prodotto='$4' ", s_cs_xx.cod_azienda, al_anno_commessa, al_num_commessa, as_cod_prodotto_reparto)
ls_sql += g_str.format(" and cod_reparto='$1' and cod_operaio='$2' ", as_cod_reparto, as_cod_operaio)

ls_sql += " order by prog_orari "

ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql)
al_tot_minuti = 0
setnull(lt_1)
setnull(lt_2)
for ll_i = 1 to ll_ret
	
	choose case lds_data.getitemstring(ll_i,2)
		case "S"  	// inizio fase
			if isnull(lt_1)  then
				// mi metto via l'orario per il giro successivo
				if isnull(lds_data.getitemdatetime(ll_i,"orario_lavorazione") ) then
					// vuol dire che è un attrezzaggio
					lt_1 = time(lds_data.getitemdatetime(ll_i, 3))
				else
					// è una lavorazione
					lt_1 = time(lds_data.getitemdatetime(ll_i, 4))
				end if				
			else
				as_message = g_str.format("Commessa $1/$2 Reparto $3 c'è una anomalia sugli orari; forse manca una fine fase.",al_anno_commessa,al_num_commessa,as_cod_reparto)
				return -1
			end if
		case "N"
			if not isnull(lt_1)  then
				// mi metto via l'orario per il giro successivo
				if isnull(lds_data.getitemdatetime(ll_i,"orario_lavorazione") ) then
					// vuol dire che è un attrezzaggio
					lt_2 = time(lds_data.getitemdatetime(ll_i, 3))
				else
					// è una lavorazione
					lt_2 = time(lds_data.getitemdatetime(ll_i, 4))
				end if		
				
				ll_minuti = Round(secondsafter(lt_1, lt_2) / 60,0)
				setnull(lt_1)
				setnull(lt_2)
				al_tot_minuti += ll_minuti
			else
				as_message = g_str.format("Commessa $1/$2 Reparto $3 c'è una anomalia sugli orari; forse manca un inizio fase.",al_anno_commessa,al_num_commessa,as_cod_reparto)
				return -1
			end if	
	end choose
next

return 0
end function

on w_report_stato_commesse.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_report_stato_commesse.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;// impostazioni dw selezione //
long ll_anno_esercizio
tab_1.tab_selezione.dw_selezione.insertrow(0)
guo_functions.uof_get_parametro_azienda( "ESC", ll_anno_esercizio)
tab_1.tab_selezione.dw_selezione.setitem(1,"anno_commessa", ll_anno_esercizio)





end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(tab_1.tab_selezione.dw_selezione,"cod_reparto",sqlca,&
								  "anag_reparti","cod_reparto","des_reparto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_1.tab_selezione.dw_selezione,"cod_lavorazione",sqlca,&
								  "tab_lavorazioni","cod_lavorazione","des_lavorazione",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

type tab_1 from tab within w_report_stato_commesse
integer width = 4594
integer height = 2580
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tab_selezione tab_selezione
tab_report tab_report
end type

on tab_1.create
this.tab_selezione=create tab_selezione
this.tab_report=create tab_report
this.Control[]={this.tab_selezione,&
this.tab_report}
end on

on tab_1.destroy
destroy(this.tab_selezione)
destroy(this.tab_report)
end on

type tab_selezione from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4558
integer height = 2452
long backcolor = 12632256
string text = "Selezione"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_selezione dw_selezione
end type

on tab_selezione.create
this.dw_selezione=create dw_selezione
this.Control[]={this.dw_selezione}
end on

on tab_selezione.destroy
destroy(this.dw_selezione)
end on

type dw_selezione from uo_std_dw within tab_selezione
integer x = 5
integer y = 8
integer width = 3543
integer height = 1320
integer taborder = 20
string dataobject = "d_report_stato_commesse_sel"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_prodotto_inizio"
		guo_ricerca.uof_ricerca_prodotto(tab_1.tab_selezione.dw_selezione, "cod_prodotto_inizio")
	case "b_ricerca_prodotto_fine"
		guo_ricerca.uof_ricerca_prodotto(tab_1.tab_selezione.dw_selezione, "cod_prodotto_fine")
	
	case "b_ricerca_operaio_inizio"
		guo_ricerca.uof_ricerca_operaio(tab_1.tab_selezione.dw_selezione, "cod_operaio_inizio")
	case "b_ricerca_operaio_fine"
		guo_ricerca.uof_ricerca_operaio(tab_1.tab_selezione.dw_selezione, "cod_operaio_fine")
		
	case "b_attrezzatura"
		guo_ricerca.uof_ricerca_attrezzatura(tab_1.tab_selezione.dw_selezione, "cod_attrezzatura")
		
		
	case "b_report"
		this.accepttext()
		wf_report()
		tab_1.selecttab(tab_1.tab_report)
		rollback;

end choose
end event

type tab_report from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4558
integer height = 2452
long backcolor = 12632256
string text = "Report"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_2 cb_2
cb_1 cb_1
dw_report dw_report
end type

on tab_report.create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_report=create dw_report
this.Control[]={this.cb_2,&
this.cb_1,&
this.dw_report}
end on

on tab_report.destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_report)
end on

type cb_2 from commandbutton within tab_report
integer x = 3707
integer y = 2328
integer width = 402
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Invio Mail"
end type

event clicked;tab_1.tab_report.dw_report.event ue_anteprima_pdf()
end event

type cb_1 from commandbutton within tab_report
integer x = 4142
integer y = 2328
integer width = 389
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;dw_report.print()

end event

type dw_report from uo_std_dw within tab_report
integer x = 5
integer y = 8
integer width = 4549
integer height = 2240
integer taborder = 20
string dataobject = "d_report_stato_commesse_1"
boolean border = false
borderstyle borderstyle = stylebox!
boolean ib_dw_report = true
end type

