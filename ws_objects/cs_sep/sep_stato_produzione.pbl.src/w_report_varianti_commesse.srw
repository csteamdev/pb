﻿$PBExportHeader$w_report_varianti_commesse.srw
$PBExportComments$Window report varianti commesse
forward
global type w_report_varianti_commesse from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_report_varianti_commesse
end type
type dw_report from uo_cs_xx_dw within w_report_varianti_commesse
end type
type cbx_figli_mp from checkbox within w_report_varianti_commesse
end type
type cbx_note_prodotto from checkbox within w_report_varianti_commesse
end type
type cb_report from commandbutton within w_report_varianti_commesse
end type
end forward

global type w_report_varianti_commesse from w_cs_xx_principale
integer width = 4146
integer height = 2244
string title = "Report Varianti Commesse"
cb_1 cb_1
dw_report dw_report
cbx_figli_mp cbx_figli_mp
cbx_note_prodotto cbx_note_prodotto
cb_report cb_report
end type
global w_report_varianti_commesse w_report_varianti_commesse

type variables
string is_cod_prodotto, is_cod_versione, is_note

long   il_anno_commessa, il_num_commessa
end variables

forward prototypes
public function integer wf_report (string fs_cod_prodotto, string fs_cod_versione, long fl_num_livello_cor, ref string fs_errore)
end prototypes

public function integer wf_report (string fs_cod_prodotto, string fs_cod_versione, long fl_num_livello_cor, ref string fs_errore);long   ll_num_figli, ll_num_righe, ll_num_sequenza, ll_j, li_risposta, ll_cont_figli, ll_cont_figli_integrazioni, &
       ll_giri, ll_nota

string ls_cod_prodotto_figlio, ls_cod_prodotto_padre, ls_flag_materia_prima, ls_cod_prodotto_variante, ls_flag_materia_prima_variante, &
       ls_cod_misura_mag_variante, ls_cod_prodotto_definitivo, ls_flag_materia_prima_definitivo, ls_cod_misura_mag_definitivo, &
		 ls_des_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_des_estesa_prodotto, ls_cod_misura_mag, ls_test_prodotto_f, &
		 ls_cod_formula_quan_utilizzo, ls_cod_gruppo_variante, ls_note

double ldd_quan_utilizzo_variante, ldd_quan_utilizzo, ldd_quan_utilizzo_definitivo, ldd_tempo_lavorazione

boolean lb_flag_variante

setpointer(hourglass!)

datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta_comm"

lds_righe_distinta.SetTransObject(sqlca)

lds_righe_distinta.setsort( " cod_prodotto_figlio ASC ")

ll_num_righe = lds_righe_distinta.Retrieve( s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione, il_anno_commessa, il_num_commessa)
	
do while ll_giri < 2
	
	if ll_giri = 0 then
		ll_giri = 1
	else
		ll_giri++
	end if
	
	for ll_num_figli = 1 to ll_num_righe

		ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	
		ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	
		ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	
		ls_cod_prodotto_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_padre")
	
		ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")

		if ll_giri = 1 then
			
			setnull(ls_test_prodotto_f)
			
			select cod_prodotto_figlio 
			into   :ls_test_prodotto_f
			from   distinta
			where  cod_azienda = :s_cs_xx.cod_azienda	and	 
			       cod_prodotto_padre = :ls_cod_prodotto_figlio;
	
			if not (isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "") then continue
			
		else
			
			setnull(ls_test_prodotto_f)
			
			select cod_prodotto_figlio 
			into   :ls_test_prodotto_f
			from   distinta
			where  cod_azienda = :s_cs_xx.cod_azienda	and	 
			       cod_prodotto_padre = :ls_cod_prodotto_figlio;
	
			if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then continue
			
		end if

		select cod_prodotto,
				 quan_utilizzo,
				 flag_materia_prima,
				 cod_misura
		into   :ls_cod_prodotto_variante,
				 :ldd_quan_utilizzo_variante,
				 :ls_flag_materia_prima_variante,
				 :ls_cod_misura_mag_variante
		from   varianti_commesse
		where  cod_azienda = :s_cs_xx.cod_azienda	and    
				 anno_commessa = :il_anno_commessa	and    
				 num_commessa = :il_num_commessa	and    
				 cod_prodotto_padre = :ls_cod_prodotto_padre	and    
				 num_sequenza = :ll_num_sequenza	and    
				 cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
				 cod_versione = :is_cod_versione;

		if sqlca.sqlcode = 0 then
			
			lb_flag_variante = true
			
		else
			
			lb_flag_variante = false
			
		end if
	
		setnull(ls_cod_prodotto_definitivo)

		if lb_flag_variante = true then
	
			ls_cod_prodotto_definitivo = ls_cod_prodotto_variante
			ldd_quan_utilizzo_definitivo = ldd_quan_utilizzo_variante
			ls_flag_materia_prima_definitivo = ls_flag_materia_prima_variante
			ls_cod_misura_mag_definitivo = ls_cod_misura_mag_variante
			
		else
			
			ls_cod_prodotto_definitivo = ls_cod_prodotto_figlio
			ldd_quan_utilizzo_definitivo = ldd_quan_utilizzo
			ls_flag_materia_prima_definitivo = ls_flag_materia_prima
			
			select cod_misura_mag
			into   :ls_cod_misura_mag
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and    
					 cod_prodotto = :ls_cod_prodotto_figlio;		
			
			ls_cod_misura_mag_definitivo = ls_cod_misura_mag
		
		end if	
	
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 cod_prodotto = :ls_cod_prodotto_definitivo;
	
		ls_des_prodotto = trim(ls_des_prodotto)
		
		// *** note prodotto
		
		is_note = ""
		
		if cbx_note_prodotto.checked then
			
			declare cu_note cursor for
			  SELECT   anag_prodotti_note.nota_prodotto  
			  FROM     anag_prodotti_note  
			  WHERE    anag_prodotti_note.cod_azienda = :s_cs_xx.cod_azienda AND  
						  anag_prodotti_note.cod_prodotto = :ls_cod_prodotto_definitivo
			  ORDER BY anag_prodotti_note.cod_prodotto ASC;
			  
			open cu_note;
			
			do while 1 = 1
				
				fetch cu_note into :ls_note;
				
				if sqlca.sqlcode <> 0 then exit
				
				if not isnull(ls_note) and ls_note <> "" then 
					
					if isnull(is_note) then is_note = ""
					
					if is_note <> "" then
						is_note = is_note + "~r~n"
					end if
					
					is_note = is_note + ls_note
					
				end if
				
			loop
			
			close cu_note;
		end if		
		
		// ***		
	
		// ** dati di lavorazione

		select cod_reparto,
				 cod_lavorazione,
				 tempo_lavorazione,
				 des_estesa_prodotto
		into   :ls_cod_reparto,
				 :ls_cod_lavorazione,
				 :ldd_tempo_lavorazione,
				 :ls_des_estesa_prodotto
		from   tes_fasi_lavorazione
		where  cod_azienda = :s_cs_xx.cod_azienda	and    
				 cod_prodotto = :ls_cod_prodotto_definitivo;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","E' probabile che ci siano più fasi associate allo stesso semilavorato (controllare il messaggio d'errore sul DB). Tuttavia è possibile proseguire con l'elaborazione del report. Errore sul DB: " + sqlca.sqlerrtext,information!)
		end if
			 
		// *** se la quantità utilizzo è zero allora non lo stampo:specifica storico distinta base rev 7 unifast
		
		if ldd_quan_utilizzo_definitivo <= 0 then continue
				 
		// ***
				 
		ll_j = dw_report.insertrow(0)
			
		dw_report.setitem( ll_j, "tipo_riga", 3)		
		
		dw_report.setitem( ll_j, "num_livello", fl_num_livello_cor)
		
		if isnull(ldd_quan_utilizzo_definitivo) then ldd_quan_utilizzo_definitivo = 0
		if isnull(ls_cod_misura_mag_definitivo) then ls_cod_misura_mag_definitivo = ""

		choose case fl_num_livello_cor
			case 1
				dw_report.setitem( ll_j, "cod_l_1", ls_cod_prodotto_definitivo)
				dw_report.setitem( ll_j, "des_l_1", ls_des_prodotto)

				if is_note <> "" then
					ll_nota = dw_report.insertrow(0)						
					dw_report.setitem( ll_nota, "tipo_riga", 1000)	
					dw_report.setitem( ll_nota, "num_livello", 1)
					dw_report.setitem( ll_nota, "note_1", is_note)
				end if
				
			case 2
				dw_report.setitem(ll_j,"cod_l_2",ls_cod_prodotto_definitivo) //+ "    " + string(ldd_quan_utilizzo_definitivo) + " " + ls_cod_misura_mag_definitivo)
				dw_report.setitem(ll_j,"des_l_2",ls_des_prodotto)
				if is_note <> "" then
					ll_nota = dw_report.insertrow(0)						
					dw_report.setitem( ll_nota, "tipo_riga", 1001)						
					dw_report.setitem( ll_nota, "num_livello", 2)
					dw_report.setitem( ll_nota, "note_2", is_note)
				end if				
			case 3
				dw_report.setitem(ll_j,"cod_l_3",ls_cod_prodotto_definitivo) //+ "    " + string(ldd_quan_utilizzo_definitivo) + " " + ls_cod_misura_mag_definitivo)
				dw_report.setitem(ll_j,"des_l_3",ls_des_prodotto)
				if is_note <> "" then
					ll_nota = dw_report.insertrow(0)						
					dw_report.setitem( ll_nota, "tipo_riga", 1002)						
					dw_report.setitem( ll_nota, "num_livello", 3)
					dw_report.setitem( ll_nota, "note_3", is_note)
				end if				
			case 4
				dw_report.setitem(ll_j,"cod_l_4",ls_cod_prodotto_definitivo) //+ "    " + string(ldd_quan_utilizzo_definitivo) + " " + ls_cod_misura_mag_definitivo)
				dw_report.setitem(ll_j,"des_l_4",ls_des_prodotto)
				if is_note <> "" then
					ll_nota = dw_report.insertrow(0)						
					dw_report.setitem( ll_nota, "tipo_riga", 1003)						
					dw_report.setitem( ll_nota, "num_livello", 4)
					dw_report.setitem( ll_nota, "note_4", is_note)
				end if				
			case 5
				dw_report.setitem(ll_j,"cod_l_5",ls_cod_prodotto_definitivo) //+ "    " + string(ldd_quan_utilizzo_definitivo) + " " + ls_cod_misura_mag_definitivo)
				dw_report.setitem(ll_j,"des_l_5",ls_des_prodotto)
				if is_note <> "" then
					ll_nota = dw_report.insertrow(0)						
					dw_report.setitem( ll_nota, "tipo_riga", 1004)						
					dw_report.setitem( ll_nota, "num_livello", 5)
					dw_report.setitem( ll_nota, "note_5", is_note)
				end if				
			case 6
				dw_report.setitem(ll_j,"cod_l_6",ls_cod_prodotto_definitivo) //+ "    " + string(ldd_quan_utilizzo_definitivo) + " " + ls_cod_misura_mag_definitivo)
				dw_report.setitem(ll_j,"des_l_6",ls_des_prodotto)
				if is_note <> "" then
					ll_nota = dw_report.insertrow(0)						
					dw_report.setitem( ll_nota, "tipo_riga", 1005)						
					dw_report.setitem( ll_nota, "num_livello", 6)
					dw_report.setitem( ll_nota, "note_6", is_note)
				end if				
			case 7
				dw_report.setitem(ll_j,"cod_l_7",ls_cod_prodotto_definitivo) //+ "    " + string(ldd_quan_utilizzo_definitivo) + " " + ls_cod_misura_mag_definitivo)
				dw_report.setitem(ll_j,"des_l_7",ls_des_prodotto)
				if is_note <> "" then
					ll_nota = dw_report.insertrow(0)						
					dw_report.setitem( ll_nota, "tipo_riga", 1006)						
					dw_report.setitem( ll_nota, "num_livello", 7)
					dw_report.setitem( ll_nota, "note_7", is_note)
				end if			
		end choose 
		
		dw_report.setitem( ll_j, "flag_mp", ls_flag_materia_prima)
		dw_report.setitem( ll_j, "cod_gruppo_variante", ls_cod_gruppo_variante)
		dw_report.setitem( ll_j, "cod_formula", ls_cod_formula_quan_utilizzo)
		dw_report.setitem( ll_j, "cod_reparto", ls_cod_reparto)
		dw_report.setitem( ll_j, "cod_lavorazione", ls_cod_lavorazione)
		dw_report.setitem( ll_j, "tempo_lavorazione", ldd_tempo_lavorazione)
		dw_report.setitem( ll_j, "quan_utilizzo", string(ldd_quan_utilizzo_definitivo) + " " + ls_cod_misura_mag_definitivo)
		
		if ls_flag_materia_prima_definitivo = 'N' then
				
			ll_cont_figli = 0
				
			select count(*)
			into   :ll_cont_figli
			from   distinta
			where  cod_azienda = :s_cs_xx.cod_azienda	and	 
					 cod_prodotto_padre = :ls_cod_prodotto_definitivo;
		
			ll_cont_figli_integrazioni = 0
				
			select count(*)
			into   :ll_cont_figli_integrazioni
			from   integrazioni_commessa
			where  cod_azienda = :s_cs_xx.cod_azienda	and	 
					 cod_prodotto_padre = :ls_cod_prodotto_definitivo and
					 anno_commessa = :il_anno_commessa	and    
					 num_commessa = :il_num_commessa;
		
			if ll_cont_figli + ll_cont_figli_integrazioni = 0 then
				continue
			else
					
				li_risposta = wf_report( ls_cod_prodotto_definitivo, fs_cod_versione, fl_num_livello_cor + 1, fs_errore)
		
			end if
				
		else
			
			ll_cont_figli = 0
				
			select count(*)
			into   :ll_cont_figli
			from   distinta
			where  cod_azienda = :s_cs_xx.cod_azienda	and	 
					 cod_prodotto_padre = :ls_cod_prodotto_definitivo;
		
			ll_cont_figli_integrazioni = 0
				
			select count(*)
			into   :ll_cont_figli_integrazioni
			from   integrazioni_commessa
			where  cod_azienda = :s_cs_xx.cod_azienda	and	 
					 cod_prodotto_padre = :ls_cod_prodotto_definitivo and
					 anno_commessa = :il_anno_commessa	and    
					 num_commessa = :il_num_commessa;
	
			if ll_cont_figli + ll_cont_figli_integrazioni = 0 then
				
				continue
						
			else
					
				// *** decido se stampare o no i figli di un prodotto per il quale si è impostato il flag MP
					
				if cbx_figli_mp.checked = false then continue
					
				li_risposta = wf_report( ls_cod_prodotto_definitivo, fs_cod_versione, fl_num_livello_cor + 1, fs_errore)
	
			end if
				
		end if
		
		setnull(ls_flag_materia_prima)
		setnull(ls_cod_gruppo_variante)
		setnull(ls_cod_formula_quan_utilizzo)
		setnull(ls_cod_reparto)
		setnull(ls_cod_lavorazione)
		setnull(ldd_tempo_lavorazione)		
		
	next
	
loop

destroy(lds_righe_distinta)

setpointer(arrow!)

return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report= true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

is_cod_prodotto = s_cs_xx.parametri.parametro_s_1

is_cod_versione = s_cs_xx.parametri.parametro_s_2

il_anno_commessa = s_cs_xx.parametri.parametro_d_1

il_num_commessa = s_cs_xx.parametri.parametro_d_2

//dw_report.set_dw_options(sqlca, &
//                         pcca.null_object, &
//                         c_nomodify + &
//                         c_nodelete + &
//                         c_noretrieveonopen, &
//                         c_noresizedw + &
//                         c_nohighlightselected + &
//                         c_nocursorrowpointer + &
//                         c_nocursorrowfocusrect + &
//								 c_ViewModeColorUnchanged + &
//								 c_InactiveDWColorUnchanged)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete , &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer + &
                         c_nocursorrowfocusrect + &
								 c_ViewModeColorUnchanged + &
								 c_InactiveDWColorUnchanged)

iuo_dw_main = dw_report

dw_report.object.datawindow.print.preview = "Yes"
								 




end event

on w_report_varianti_commesse.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_report=create dw_report
this.cbx_figli_mp=create cbx_figli_mp
this.cbx_note_prodotto=create cbx_note_prodotto
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.cbx_figli_mp
this.Control[iCurrent+4]=this.cbx_note_prodotto
this.Control[iCurrent+5]=this.cb_report
end on

on w_report_varianti_commesse.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_report)
destroy(this.cbx_figli_mp)
destroy(this.cbx_note_prodotto)
destroy(this.cb_report)
end on

type cb_1 from commandbutton within w_report_varianti_commesse
integer x = 3726
integer y = 2040
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long job

job = PrintOpen( ) 
PrintDataWindow(job, dw_report) 
PrintClose(job)


end event

type dw_report from uo_cs_xx_dw within w_report_varianti_commesse
event ue_report ( )
integer x = 23
integer y = 20
integer width = 4069
integer height = 2000
integer taborder = 10
string dataobject = "d_report_varianti_commesse"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_report();string ls_cod_prodotto, ls_cod_versione, ls_errore, ls_des_prodotto, ls_note_globali, ls_note, ls_rag_soc_1

integer li_risposta

long    ll_riga

change_dw_current()

setpointer(hourglass!)

ls_cod_prodotto = is_cod_prodotto

ls_cod_versione = is_cod_versione

if isnull(ls_cod_prodotto) or ls_cod_prodotto ="" then return 

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_prodotto = :ls_cod_prodotto;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return
end if

dw_report.reset()

dw_report.setredraw(false)

select rag_soc_1
into   :ls_rag_soc_1
from   aziende 
where  cod_azienda = :s_cs_xx.cod_azienda;

dw_report.Modify("commessa_t.text='" + string(il_anno_commessa) + "/" + string(il_num_commessa) + "'")

dw_report.Modify("ragione_sociale.text='" + ls_rag_soc_1 + "'")

dw_report.Modify("cod_prodotto.text='" + ls_cod_prodotto + "'")

dw_report.Modify("des_prodotto.text='" + ls_des_prodotto + "'")

dw_report.Modify("cod_versione.text='" + ls_cod_versione + "'")

if cbx_note_prodotto.checked then
	
	ls_note_globali = ""
	
	declare cu_note cursor for
	  SELECT   anag_prodotti_note.nota_prodotto  
	  FROM     anag_prodotti_note  
	  WHERE    anag_prodotti_note.cod_azienda = :s_cs_xx.cod_azienda AND  
				  anag_prodotti_note.cod_prodotto = :ls_cod_prodotto
	  ORDER BY anag_prodotti_note.cod_prodotto ASC;
	  
	open cu_note;
	
	do while 1 = 1
		fetch cu_note into :ls_note;
		if sqlca.sqlcode <> 0 then exit
		if not isnull(ls_note) and ls_note <> "" then 
			if ls_note_globali <> "" then
				ls_note_globali = ls_note_globali + "~r~n"
			end if
			ls_note_globali = ls_note_globali + ls_note
		end if
		
	loop
	
	close cu_note;
	
	ll_riga = dw_report.insertrow(1)	
	
	dw_report.setitem( ll_riga, "tipo_riga", 1)
	
	dw_report.Modify("note_prodotto.text='" + ls_note_globali + "'")
	
end if

ll_riga = dw_report.insertrow(0)	
	
dw_report.setitem( ll_riga, "tipo_riga", 2)
	
li_risposta = wf_report( is_cod_prodotto, is_cod_versione, 1, ls_errore)

if li_risposta < 0 then return

dw_report.setredraw(true)

i_IsEmpty = false

setpointer(arrow!)
end event

event pcd_retrieve;call super::pcd_retrieve;dw_report.triggerevent("ue_report")
end event

type cbx_figli_mp from checkbox within w_report_varianti_commesse
integer x = 709
integer y = 2040
integer width = 731
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stampa Componenti M.P."
boolean checked = true
end type

type cbx_note_prodotto from checkbox within w_report_varianti_commesse
integer x = 23
integer y = 2040
integer width = 663
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stampa Note Prodotto"
boolean checked = true
end type

type cb_report from commandbutton within w_report_varianti_commesse
integer x = 1463
integer y = 2040
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_prodotto,ls_cod_versione,ls_errore,ls_des_prodotto
integer li_risposta

ls_cod_prodotto = is_cod_prodotto

ls_cod_versione = is_cod_versione

if isnull(ls_cod_prodotto) or ls_cod_prodotto ="" then return 

dw_report.reset()

is_cod_prodotto = ls_cod_prodotto

is_cod_versione = ls_cod_versione

is_note = "" 

dw_report.triggerevent( "pcd_retrieve")



end event

