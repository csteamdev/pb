﻿$PBExportHeader$w_stato_commesse.srw
$PBExportComments$Window stato commesse
forward
global type w_stato_commesse from w_cs_xx_principale
end type
type dw_lista_stato_commesse from uo_cs_xx_dw within w_stato_commesse
end type
type st_4 from statictext within w_stato_commesse
end type
type em_1 from editmask within w_stato_commesse
end type
type st_5 from statictext within w_stato_commesse
end type
type em_2 from editmask within w_stato_commesse
end type
type cb_aggiorna from commandbutton within w_stato_commesse
end type
end forward

global type w_stato_commesse from w_cs_xx_principale
int Width=4791
int Height=2137
boolean TitleBar=true
string Title="Stato Avanzamento Commesse"
dw_lista_stato_commesse dw_lista_stato_commesse
st_4 st_4
em_1 em_1
st_5 st_5
em_2 em_2
cb_aggiorna cb_aggiorna
end type
global w_stato_commesse w_stato_commesse

type variables
string is_cod_attivita
string is_cod_cat_attrezzature
string is_cod_attrezzatura
datetime id_data_giorno
end variables

event pc_setwindow;call super::pc_setwindow;long ll_ppr
string ls_data_inizio

dw_lista_stato_commesse.set_dw_options(sqlca,pcca.null_object, & 
													c_nonew + c_nomodify + c_nodelete + c_disablecc , & 
													c_disableccinsert)



end event

on w_stato_commesse.create
int iCurrent
call w_cs_xx_principale::create
this.dw_lista_stato_commesse=create dw_lista_stato_commesse
this.st_4=create st_4
this.em_1=create em_1
this.st_5=create st_5
this.em_2=create em_2
this.cb_aggiorna=create cb_aggiorna
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_stato_commesse
this.Control[iCurrent+2]=st_4
this.Control[iCurrent+3]=em_1
this.Control[iCurrent+4]=st_5
this.Control[iCurrent+5]=em_2
this.Control[iCurrent+6]=cb_aggiorna
end on

on w_stato_commesse.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_lista_stato_commesse)
destroy(this.st_4)
destroy(this.em_1)
destroy(this.st_5)
destroy(this.em_2)
destroy(this.cb_aggiorna)
end on

type dw_lista_stato_commesse from uo_cs_xx_dw within w_stato_commesse
int X=23
int Y=21
int Width=4709
int Height=1901
int TabOrder=10
string DataObject="d_lista_stato_commesse"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
datetime ld_data_inizio,ld_data_fine

ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))

l_Error = Retrieve(s_cs_xx.cod_azienda,ld_data_inizio,ld_data_fine)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

type st_4 from statictext within w_stato_commesse
int X=46
int Y=1941
int Width=220
int Height=73
boolean Enabled=false
string Text="da data:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_1 from editmask within w_stato_commesse
int X=275
int Y=1941
int Width=412
int Height=81
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData="0"
double Increment=1
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_stato_commesse
int X=709
int Y=1941
int Width=188
int Height=73
boolean Enabled=false
string Text="a data:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_2 from editmask within w_stato_commesse
int X=892
int Y=1941
int Width=412
int Height=81
int TabOrder=30
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData="<€w_grafico_dist_comm"
double Increment=1
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_aggiorna from commandbutton within w_stato_commesse
int X=4366
int Y=1941
int Width=366
int Height=81
int TabOrder=11
boolean BringToTop=true
string Text="&Aggiorna"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;dw_lista_stato_commesse.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

