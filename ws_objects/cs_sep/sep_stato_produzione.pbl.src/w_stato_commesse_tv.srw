﻿$PBExportHeader$w_stato_commesse_tv.srw
forward
global type w_stato_commesse_tv from w_cs_xx_treeview
end type
type dw_totali from uo_std_dw within det_1
end type
type dw_1 from uo_std_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_fasi from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_fasi dw_fasi
end type
end forward

global type w_stato_commesse_tv from w_cs_xx_treeview
integer width = 5819
integer height = 2272
string title = "Stato Commesse"
end type
global w_stato_commesse_tv w_stato_commesse_tv

type variables
// Colori
constant long COLORE_INIZIO_ATTREZZAGGIO =  13303807
constant long COLORE_FINE_ATTREZZAGGIO =  8563455
constant long COLORE_INIZIO_LAVORAZIONE =  12582847
constant long COLORE_FINE_LAVORAZIONE =  8444672
//8421376

	
private:
	long il_livello
	datastore ids_store
	
	// Icone
	int ICONA_COMMESSA, ICONA_COMMESSA_CHIUSA, ICONA_COMMESSA_BLOCCATA
	int ICONA_DET_COMMESSA
	int ICONA_FASE_ROSSA, ICONA_FASE_GIALLA, ICONA_FASE_VERDE
	
	// Colori
	long COLORE_TEMPI[] = {13434828, 13434879, 13421823, 6711039}
	
end variables

forward prototypes
public subroutine wf_imposta_ricerca ()
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function integer wf_inserisci_commesse (long al_handle)
public subroutine wf_treeview_icons ()
public function integer wf_inserisci_dettagli_commessa (long al_handle, integer ai_anno_commessa, long al_num_commessa)
public function integer wf_inserisci_fasi (long al_handle, integer ai_anno, long al_numero, long al_prog_riga)
public function long wf_ottiene_colori_tempi (long al_minuti)
public function integer wf_retrieve_commessa_fasi (integer ai_anno, long al_numero, long al_prog_riga)
end prototypes

public subroutine wf_imposta_ricerca ();string ls_flag_aggiorna, ls_cod_attrezzatura
long ll_anno_registrazione, ll_num_registrazione
date ldt_date

is_sql_filtro = ""
ll_anno_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "num_registrazione")
ls_flag_aggiorna = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_aggiorna_aut")

if not isnull(ll_anno_registrazione) and ll_anno_registrazione > 1900 then
	is_sql_filtro += " AND anag_commesse.anno_commessa=" + string(ll_anno_registrazione)
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione > 0 then
	is_sql_filtro += " AND  anag_commesse.num_commessa=" + string(ll_num_registrazione)
end if

if not isnull(ll_anno_registrazione) and ll_anno_registrazione>0 and not isnull(ll_num_registrazione) and ll_num_registrazione>0 then
	// mi fermo non serve altro, filtro solo la singola commessa
	return
end if

ldt_date = tab_ricerca.ricerca.dw_ricerca.getitemdate(1, "data_registrazione_da")
if not isnull(ldt_date) then
	is_sql_filtro += " AND  det_orari_produzione.data_giorno >= '" + string(ldt_date, s_cs_xx.db_funzioni.formato_data) + "' "
end if

ldt_date = tab_ricerca.ricerca.dw_ricerca.getitemdate(1, "data_registrazione_a")
if not isnull(ldt_date) then
	is_sql_filtro += " AND  det_orari_produzione.data_giorno <= '" + string(ldt_date, s_cs_xx.db_funzioni.formato_data) + "' "
end if

ls_cod_attrezzatura = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_attrezzatura")
if not isnull(ls_cod_attrezzatura) then
	is_sql_filtro += " AND  det_orari_produzione.cod_attrezzatura = '" + ls_cod_attrezzatura + "' "
end if


if ls_flag_aggiorna = "S" then
	//tab_ricerca.selezione.tv_selezione.
	// imposto Timer 15 minuti
end if

end subroutine

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "O"
		return 1
//		return wf_inserisci_operatori(al_handle)
//		
//	case "T"
//		return wf_inserisci_tipo_ordine(al_handle)
//		
//	case "A"
//		return wf_inserisci_anno(al_handle)
//		
//	case "R"
//		return wf_inserisci_data_registrazione(al_handle)
//		
//	case "C"
//		return wf_inserisci_data_consegna(al_handle)
//		
//	case "D"
//		return wf_inserisci_depositi(al_handle)
//		
//	case "P"
//		return wf_inserisci_depositi_trasf(al_handle)
//		
//	case "F"
//		return wf_inserisci_fornitori(al_handle)
//		
//	case "X"
//		return wf_inserisci_prodotti(al_handle)
				
	case else
		return wf_inserisci_commesse(al_handle)	
		
end choose

return 1
end function

public function integer wf_inserisci_commesse (long al_handle);string ls_sql, ls_error, ls_label
int li_icona
long ll_rows, ll_i, ll_anno, ll_numero
treeviewitem ltvi_item
str_treeview lstr_data


ls_sql = "SELECT anag_commesse.anno_commessa, anag_commesse.num_commessa, anag_commesse.data_chiusura, anag_commesse.flag_blocco, det_ord_ven.anno_registrazione, det_ord_ven.num_registrazione, tes_ord_ven.cod_cliente, anag_clienti.rag_soc_1, max(det_orari_produzione.data_giorno) data_produzione, max(isnull(det_orari_produzione.orario_lavorazione, det_orari_produzione.orario_attrezzaggio)) ora_produzione  &
FROM anag_commesse  &
left join det_orari_produzione on det_orari_produzione.cod_azienda = anag_commesse.cod_azienda and det_orari_produzione.anno_commessa = anag_commesse.anno_commessa and det_orari_produzione.num_commessa = anag_commesse.num_commessa  &
left join det_ord_ven on det_ord_ven.cod_azienda=anag_commesse.cod_azienda and det_ord_ven.anno_commessa=anag_commesse.anno_commessa and det_ord_ven.num_commessa=anag_commesse.num_commessa &
left join tes_ord_ven on tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda and tes_ord_ven.anno_registrazione=det_ord_ven.anno_registrazione  and tes_ord_ven.num_registrazione=det_ord_ven.num_registrazione &
left join anag_clienti on tes_ord_ven.cod_azienda=anag_clienti.cod_azienda and tes_ord_ven.cod_cliente=anag_clienti.cod_cliente &
WHERE anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)
ls_sql += " group by anag_commesse.anno_commessa, anag_commesse.num_commessa, anag_commesse.data_chiusura, anag_commesse.flag_blocco, det_ord_ven.anno_registrazione, det_ord_ven.num_registrazione, tes_ord_ven.cod_cliente, anag_clienti.rag_soc_1 "
ls_sql += " ORDER BY data_produzione DESC,ora_produzione DESC , anag_commesse.anno_commessa, anag_commesse.num_commessa "

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

if ll_rows > il_max_treeview_results then
	if g_mb.confirm("Sono state recuperate più di " + string(il_max_treeview_results) + " righe.~r~nVuoi limitare la ricerca per avere prestazioni maggiorni?") then
		ll_rows = il_max_treeview_results
	end if
end if

for ll_i = 1 to ll_rows
	
	ll_anno = ids_store.getitemnumber(ll_i, 1) 
	ll_numero = ids_store.getitemnumber(ll_i, 2) 

	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "N"
	lstr_data.codice = string(ll_anno) + ":" + string(ll_numero)
	lstr_data.decimale[1] = ll_anno
	lstr_data.decimale[2] = ll_numero
	
	
	lstr_data.data[1] = ids_store.getitemdatetime(ll_i, 9)
	lstr_data.data[2] = ids_store.getitemdatetime(ll_i, 10)
		
	ls_label = g_str.format("$1/$2 Ordine $3/$4 -  $5 ($6)",ll_anno, ll_numero, ids_store.getitemnumber(ll_i, 5) , ids_store.getitemnumber(ll_i, 6), ids_store.getitemstring(ll_i, 7),ids_store.getitemstring(ll_i, 8) )
	
	// Calcolo icona
	li_icona = ICONA_COMMESSA
	
	if not isnull(ids_store.getitemdatetime(ll_i, 3)) then
		li_icona = ICONA_COMMESSA_CHIUSA
	elseif ids_store.getitemstring(ll_i, 4) = "S" then
		li_icona = ICONA_COMMESSA_BLOCCATA
	end if

	ltvi_item = wf_new_item(false, li_icona)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public subroutine wf_treeview_icons ();ICONA_COMMESSA = wf_treeview_add_icon("treeview\tipo_documento.png")
ICONA_COMMESSA_CHIUSA = wf_treeview_add_icon("treeview\tag_green.png")
ICONA_COMMESSA_BLOCCATA = wf_treeview_add_icon("treeview\tag_red.png")
ICONA_DET_COMMESSA = wf_treeview_add_icon("treeview\documento_blu.png")
ICONA_FASE_ROSSA = wf_treeview_add_icon("treeview\olo_rosso_p.png")
ICONA_FASE_GIALLA = wf_treeview_add_icon("treeview\olo_giallo_p.png")
ICONA_FASE_VERDE = wf_treeview_add_icon("treeview\olo_verde_p.png")
end subroutine

public function integer wf_inserisci_dettagli_commessa (long al_handle, integer ai_anno_commessa, long al_num_commessa);string ls_sql, ls_label, ls_error
long ll_rows, ll_i, ll_prog_riga
treeviewitem ltvi_item


ls_sql = "SELECT det_anag_commesse.prog_riga FROM det_anag_commesse &
WHERE det_anag_commesse.cod_azienda='" + s_cs_xx.cod_azienda + "' and &
det_anag_commesse.anno_commessa=" + string(ai_anno_commessa) + " and &
det_anag_commesse.num_commessa=" + string(al_num_commessa) + &
" ORDER BY det_anag_commesse.anno_commessa, det_anag_commesse.num_commessa, det_anag_commesse.prog_riga"


ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows
	
	 ll_prog_riga = ids_store.getitemnumber(ll_i, 1)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "B"
	lstr_data.codice = string(ll_prog_riga)
	lstr_data.decimale[1] = ai_anno_commessa
	lstr_data.decimale[2] = al_num_commessa
	lstr_data.decimale[3] = ll_prog_riga
	
	ls_label = string(ai_anno_commessa) + " - " + string(al_num_commessa) + " - " + string(ll_prog_riga)
	
	ltvi_item = wf_new_item(true, ICONA_DET_COMMESSA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows

return 1
end function

public function integer wf_inserisci_fasi (long al_handle, integer ai_anno, long al_numero, long al_prog_riga);string ls_sql, ls_error, ls_label, ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione
int li_icona, li_count_orari
long ll_rows, ll_i
treeviewitem ltvi_item, ltv_parent
str_treeview lstr_data

ls_sql = "SELECT cod_prodotto, cod_reparto, cod_lavorazione, quan_prodotta, flag_fine_fase FROM avan_produzione_com &
WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " 

tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_parent)
lstr_data = ltv_parent.data

ls_sql += " AND anno_commessa=" + string(ai_anno)
ls_sql += " AND num_commessa=" + string(al_numero)
ls_sql += " AND prog_riga=" + string(al_prog_riga)

ls_sql += " ORDER BY  cod_prodotto, cod_reparto, cod_lavorazione"

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

if ll_rows > il_max_treeview_results then
	if g_mb.confirm("Sono state recuperate più di " + string(il_max_treeview_results) + " righe.~r~nVuoi limitare la ricerca per avere prestazioni maggiorni?") then
		ll_rows = il_max_treeview_results
	end if
end if

for ll_i = 1 to ll_rows
	
	ls_cod_prodotto = ids_store.getitemstring(ll_i, 1)
	ls_cod_reparto = ids_store.getitemstring(ll_i, 2)
	ls_cod_lavorazione = ids_store.getitemstring(ll_i, 3)
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "F"
	lstr_data.codice = ls_cod_prodotto
	lstr_data.decimale[1] = ai_anno
	lstr_data.decimale[2] = al_numero
	lstr_data.decimale[3] = al_prog_riga
	lstr_data.stringa[1] = ls_cod_prodotto
	lstr_data.stringa[2] = ls_cod_reparto
	lstr_data.stringa[3] = ls_cod_lavorazione
		
	ls_label = ls_cod_prodotto + " - " + ls_cod_reparto
	
	// Calcolo icona
	li_icona = ICONA_FASE_ROSSA
	
	if ids_store.getitemstring(ll_i, 5) = "S" then
		// fase terminata
		li_icona = ICONA_FASE_VERDE
	elseif ids_store.getitemdecimal(ll_i, 4) > 0 then
		// fase iniziata
		li_icona = ICONA_FASE_GIALLA
	elseif ids_store.getitemdecimal(ll_i, 4) = 0 then
		// controllo se ci sono inizi di sessione
		select count(*)
		into :li_count_orari
		from det_orari_produzione
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :ai_anno and
				num_commessa = :al_numero and
				prog_riga = :al_prog_riga and
				cod_prodotto = :ls_cod_prodotto and
				cod_reparto = :ls_cod_reparto and
				cod_lavorazione = :ls_cod_lavorazione;
				
		if sqlca.sqlcode = 0 and li_count_orari > 0 then
			li_icona = ICONA_FASE_GIALLA
		end if
		
	end if
	
	ltvi_item = wf_new_item(false, li_icona)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_ottiene_colori_tempi (long al_minuti);/**
 * stefanop
 * 08/02/2016
 *
 * Decido il colore da applicare alla riga in base ai tempi
 **/
 
if al_minuti <= 15 then
	// da 0 a 15 minuti
	return COLORE_TEMPI[1]
elseif al_minuti >= 16 and al_minuti <= 30 then
	// fino a 30 minuti
	return COLORE_TEMPI[2]
elseif al_minuti >= 31 and al_minuti <= 45 then
	// fino a 45 minuti
	return COLORE_TEMPI[3]
else
	// tutto il resto superiore ai 45 minuti
	return COLORE_TEMPI[4]
end if
	

end function

public function integer wf_retrieve_commessa_fasi (integer ai_anno, long al_numero, long al_prog_riga);string ls_sql, ls_error, ls_cod_operaio, ls_des_operaio, ls_flag_inizio, ls_des_tipo, ls_cod_reparto, ls_des_reparto,ls_cod_prodotto, ls_des_prodotto, ls_titolo, &
		ls_cod_attrezzatura, ls_des_attrezzatura
datastore lds_store
long 	ll_row, ll_i, ll_r, ll_colore_riga, ll_minuti, ll_minuti_attrezzaggio, ll_minuti_lavorazione, ll_anno_commessa, ll_num_commessa, ll_prog_sessione, &
		ll_anno_ordine, ll_num_ordine, ll_ordinamento
decimal ld_totale_prodotta, ld_quan_ordine
boolean lb_attrezzaggio = false
datetime ldt_data_giorno, ldt_orario_attrezzaggio, ldt_orario_lavorazione, ldt_orario, ldt_orario_precedente,ldt_data_consegna
datawindow ldw_fasi, ldw_totali


//tab_dettaglio.selecttab(2)
ldw_fasi = tab_dettaglio.det_2.dw_fasi
ldw_totali =  tab_dettaglio.det_1.dw_totali

ls_sql = " select O.anno_commessa, &
O.num_commessa, &
O.prog_orari, &
O.cod_operaio, &
O.flag_inizio, &
O.data_giorno, &
O.orario_attrezzaggio, &
O.orario_lavorazione, &
O.cod_reparto, &
O.quan_scarto, &
O.quan_prodotta,  &
A.ordinamento, &
O.cod_attrezzatura, &
D.descrizione &
from det_orari_produzione O &
left join avan_produzione_com A on A.cod_azienda=O.cod_azienda and A.anno_commessa=O.anno_commessa and A.num_commessa=O.num_commessa and A.prog_riga=O.prog_riga and A.cod_prodotto=O.cod_prodotto and A.cod_lavorazione=O.cod_lavorazione and A.cod_reparto=O.cod_reparto  &
left join anag_attrezzature D on O.cod_azienda=D.cod_azienda and O.cod_attrezzatura = D.cod_attrezzatura "

ls_sql = ls_sql + " where O.cod_azienda='" + s_cs_xx.cod_azienda + "'  and O.anno_commessa=" + string (ai_anno) + " and O.num_commessa=" + string(al_numero) + " and O.prog_riga=" + string(al_prog_riga) + &
" order by A.ordinamento,O.cod_reparto, O.cod_operaio, O.prog_orari, O.data_giorno, isnull(O.orario_attrezzaggio, O.orario_lavorazione)"

/*
ls_sql = "select * from det_orari_produzione
" and num_commessa=" + string(al_numero) + " and prog_riga=" + string(al_prog_riga) +&
" order by cod_reparto, cod_operaio, prog_orari, data_giorno, isnull(orario_attrezzaggio, orario_lavorazione) "
*/
// Reset
lb_attrezzaggio = false
setnull(ldt_orario_precedente)
ll_minuti = 0
ll_minuti_attrezzaggio = 0
ll_minuti_lavorazione = 0
ld_totale_prodotta = 0
ldw_fasi.reset()
ldw_totali.reset()
ldw_fasi.setredraw(false)
ldw_totali.setredraw(false)

select C.cod_prodotto, 
		P.des_prodotto,
		D.quan_ordine,
		D.anno_registrazione,
		D.num_registrazione,
		D.data_consegna
into	:ls_cod_prodotto, 
		:ls_des_prodotto,
		:ld_quan_ordine,
		:ll_anno_ordine ,
		:ll_num_ordine,
		:ldt_data_consegna
from	anag_commesse C
left join 	anag_prodotti P on C.cod_azienda=P.cod_azienda and C.cod_prodotto=P.cod_prodotto
left join	det_ord_ven D on D.cod_azienda=C.cod_azienda and D.anno_commessa=C.anno_commessa and D.num_commessa=C.num_commessa
where	C.cod_azienda=:s_cs_xx.cod_azienda and
			C.anno_commessa = :ai_anno and
			C.num_commessa = :al_numero;
if sqlca.sqlcode < 0 then
	g_mb.error("Errore in ricerca SQL ordine di vendita: " + sqlca.sqlerrtext)
end if
	

ll_row = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
if ll_row < 0 then
	g_mb.error("Errore creazione datastore: " + ls_error)
	ldw_fasi.setredraw(true)
	return -1
end if

ls_titolo = g_str.format("COMMESSA $1/$2 [$3 $4]",ai_anno, al_numero, ls_cod_prodotto, ls_des_prodotto)
ls_titolo = g_str.format("$1~r~nORDINE $2/$3 ",ls_titolo,ll_anno_ordine,ll_num_ordine)
ls_titolo = g_str.format("$1  Q.TA: $2   CONSEGNA: $3 ",ls_titolo,string(ld_quan_ordine,"###,##0"),STRING(ldt_data_consegna,"dd/mm/yyyy"))

for ll_i = 1 to ll_row
	ll_anno_commessa = lds_store.getitemnumber(ll_i, 1)
	ll_num_commessa = lds_store.getitemnumber(ll_i, 2)
	ll_prog_sessione = lds_store.getitemnumber(ll_i, 3)
	ls_cod_operaio = lds_store.getitemstring(ll_i, 4)
	ls_flag_inizio = lds_store.getitemstring(ll_i, 5) 
	ldt_data_giorno = lds_store.getitemdatetime(ll_i, 6)
	ldt_orario_attrezzaggio = lds_store.getitemdatetime(ll_i, 7)
	ldt_orario_lavorazione = lds_store.getitemdatetime(ll_i, 8)
	ls_cod_reparto = 	lds_store.getitemstring(ll_i, 9)
	ll_ordinamento = 	lds_store.getitemnumber(ll_i, 12)
	
	ls_cod_attrezzatura = lds_store.getitemstring(ll_i, 13)
	ls_des_attrezzatura = lds_store.getitemstring(ll_i, 14)
	
	// Orario
	if isnull(ldt_orario_lavorazione) then
		ldt_orario = datetime(date(ldt_data_giorno), time(ldt_orario_attrezzaggio))
		lb_attrezzaggio = true
	else
		ldt_orario = datetime(date(ldt_data_giorno), time(ldt_orario_lavorazione))
		lb_attrezzaggio = false
	end if
	// -------
		
	select nome + ' ' + cognome
	into :ls_des_operaio
	from anag_operai
	where cod_azienda = :s_cs_xx.cod_azienda and cod_operaio=:ls_cod_operaio;
	
	select des_reparto
	into :ls_des_reparto
	from anag_reparti
	where cod_azienda = :s_cs_xx.cod_azienda and cod_reparto = :ls_cod_reparto;
	
	ll_r = ldw_fasi.insertrow(0)
	ldw_fasi.setitem(ll_r, "titolo", ls_titolo)
	ldw_fasi.setitem(ll_r, "prog_sessione", ll_prog_sessione)
	ldw_fasi.setitem(ll_r, "anno_commessa", ll_anno_commessa)
	ldw_fasi.setitem(ll_r, "num_commessa", ll_num_commessa)
	ldw_fasi.setitem(ll_r, "cod_operaio", ls_cod_operaio)
	ldw_fasi.setitem(ll_r, "des_operaio", ls_des_operaio)
	ldw_fasi.setitem(ll_r, "flag_inizio", ls_flag_inizio)
	ldw_fasi.setitem(ll_r, "quan_scarto", lds_store.getitemdecimal(ll_i, 10))
	ldw_fasi.setitem(ll_r, "quan_prodotta", lds_store.getitemdecimal(ll_i, 11))
	ldw_fasi.setitem(ll_r, "orario", ldt_orario)
	
	ld_totale_prodotta += lds_store.getitemdecimal(ll_i, 11)
	
	// Minuti
	if ls_flag_inizio = 'S' then
		ll_minuti = 0
	elseif not isnull(ldt_orario_precedente) then
		ll_minuti = secondsafter(time(ldt_orario_precedente), time(ldt_orario))
		ll_minuti = ll_minuti /60 // minuti
		
		if lb_attrezzaggio then
			ll_minuti_attrezzaggio += ll_minuti
		else 
			ll_minuti_lavorazione += ll_minuti
		end if
	end if
	
	ldw_fasi.setitem(ll_r, "differenza_minuti", ll_minuti)
	ldw_fasi.setitem(ll_r, "colore_tempi", wf_ottiene_colori_tempi(ll_minuti))
	ldt_orario_precedente = ldt_orario
	// ---
	
	// Colore
	if ls_flag_inizio = "S" then
		if lb_attrezzaggio then
			ll_colore_riga = COLORE_INIZIO_ATTREZZAGGIO
			ls_des_tipo = g_str.format("$1 - Inizio Attrezzaggio", ls_cod_reparto)
		else
			ll_colore_riga = COLORE_INIZIO_LAVORAZIONE
			ls_des_tipo = g_str.format("$1 - Inizio Lavorazione", ls_cod_reparto)
		end if
	else
		if lb_attrezzaggio then
			ll_colore_riga = COLORE_FINE_ATTREZZAGGIO
			ls_des_tipo = g_str.format("$1 - Fine Attrezzaggio", ls_cod_reparto)
		else
			ll_colore_riga = COLORE_FINE_LAVORAZIONE
			ls_des_tipo = g_str.format("$1 - Fine Lavorazione", ls_cod_reparto)
		end if
	end if
	if not isnull(ls_cod_attrezzatura) then ldw_fasi.setitem(ll_r, "attrezzatura", g_str.format("$1 - $2",upper(ls_cod_attrezzatura), ls_des_attrezzatura))
	
	ldw_fasi.setitem(ll_r, "colore_tipo", ll_colore_riga)
	ldw_fasi.setitem(ll_r, "des_tipo", ls_des_tipo)
	ldw_fasi.setitem(ll_r, "cod_reparto", g_str.format("$2 ($1)", ls_cod_reparto, ls_des_reparto))
	ldw_fasi.setitem(ll_r, "ordinamento_reparto", ll_ordinamento)
	// ---
next

ll_r = tab_dettaglio.det_1.dw_totali.insertrow(0)
tab_dettaglio.det_1.dw_totali.setitem(ll_r, "totale_minuti_attrezzaggio", ll_minuti_attrezzaggio)
tab_dettaglio.det_1.dw_totali.setitem(ll_r, "totale_minuti_lavorazione", ll_minuti_lavorazione)
tab_dettaglio.det_1.dw_totali.setitem(ll_r, "totale_minuti", ll_minuti_attrezzaggio + ll_minuti_lavorazione)
tab_dettaglio.det_1.dw_totali.setitem(ll_r, "totale_prodotta", ld_totale_prodotta)

ldw_fasi.groupcalc()
ldw_fasi.resetupdate()

ldw_fasi.setredraw(true)
 tab_dettaglio.det_1.dw_totali.setredraw(true)

return ll_row
end function

on w_stato_commesse_tv.create
int iCurrent
call super::create
end on

on w_stato_commesse_tv.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;is_codice_filtro = "COM"

tab_dettaglio.det_1.dw_1.settransobject(sqlca)
//tab_dettaglio.det_2.dw_fasi.settransobject(sqlca)

//tab_dettaglio.det_2.dw_fasi.set_dw_options(sqlca,pcca.null_object, & 
//													c_nonew + c_nomodify + c_nodelete + c_disablecc , & 
//													c_disableccinsert)

tab_dettaglio.det_2.dw_fasi.ib_dw_report=true

tab_dettaglio.det_2.dw_fasi.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
							  c_disablecc, &
							  c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
//			dw_anteprima.modify(ls_nome_colonna + ".background.color='16777215'")

end event

event ue_custom_color;call super::ue_custom_color;tab_dettaglio.det_1.backcolor = rgb(255,255,255)
tab_dettaglio.det_2.backcolor = rgb(255,255,255)
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_stato_commesse_tv
integer x = 2240
integer width = 3520
integer height = 2120
det_2 det_2
end type

on tab_dettaglio.create
this.det_2=create det_2
call super::create
this.Control[]={this.det_1,&
this.det_2}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
end on

event tab_dettaglio::ue_resize;call super::ue_resize;tab_dettaglio.det_1.dw_totali.move(20, det_2.height -  tab_dettaglio.det_1.dw_totali.height)
tab_dettaglio.det_1.dw_totali.width =  tab_dettaglio.det_1.width - 50
det_2.dw_fasi.resize(det_2.width,  det_2.height)
end event

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 3483
integer height = 1996
long backcolor = 16777215
string text = "Commessa"
dw_totali dw_totali
dw_1 dw_1
end type

on det_1.create
this.dw_totali=create dw_totali
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_totali
this.Control[iCurrent+2]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_totali)
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_stato_commesse_tv
integer width = 2194
integer height = 2120
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer width = 2158
integer height = 1996
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer width = 2149
integer height = 1980
string dataobject = "d_stato_commesse_search"
end type

event dw_ricerca::ue_key;call super::ue_key;if key=KeyF1! and keyflags=1 then
	guo_ricerca.uof_ricerca_attrezzatura( tab_ricerca.ricerca.dw_ricerca, "cod_attrezzatura")
end if
end event

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_attrezzatura"
		guo_ricerca.uof_ricerca_attrezzatura( tab_ricerca.ricerca.dw_ricerca, "cod_attrezzatura")
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer width = 2158
integer height = 1996
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
integer width = 2149
integer height = 1960
end type

event tv_selezione::itempopulate;call super::itempopulate;long ll_return
treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

//if lstr_data.tipo_livello = "N" then
//	ll_return =  wf_inserisci_dettagli_commessa(handle, int(lstr_data.decimale[1]), long(lstr_data.decimale[2]))
//elseif lstr_data.tipo_livello = "B" then
//	ll_return =  wf_inserisci_fasi(handle, int(lstr_data.decimale[1]), long(lstr_data.decimale[2]), long(lstr_data.decimale[3]))
//else
	ll_return = wf_leggi_livello(handle, lstr_data.livello + 1)
//end if

if ll_return < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data

wf_retrieve_commessa_fasi(istr_data.decimale[1], istr_data.decimale[2], 1)
		
tab_dettaglio.det_1.dw_1.retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2])

tab_dettaglio.det_1.dw_1.object.t_lastupdate.text = g_str.format("Ultimo aggiornamento: $1 $2", string(istr_data.data[1],"dd/mm/yyyy"), string(istr_data.data[2],"hh:mm:ss") )


end event

type dw_totali from uo_std_dw within det_1
integer x = 27
integer y = 1592
integer width = 1897
integer height = 340
integer taborder = 20
string dataobject = "d_stato_commesse_3"
borderstyle borderstyle = stylebox!
boolean ib_colora = false
end type

type dw_1 from uo_std_dw within det_1
integer x = 27
integer y = 12
integer width = 3131
integer height = 1540
integer taborder = 21
string dataobject = "d_stato_commesse_1"
borderstyle borderstyle = stylebox!
boolean ib_colora = false
end type

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3483
integer height = 1996
long backcolor = 16777215
string text = "Fasi"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_fasi dw_fasi
end type

on det_2.create
this.dw_fasi=create dw_fasi
this.Control[]={this.dw_fasi}
end on

on det_2.destroy
destroy(this.dw_fasi)
end on

type dw_fasi from uo_cs_xx_dw within det_2
integer x = 5
integer y = 12
integer width = 3497
integer height = 1440
integer taborder = 21
string dataobject = "d_stato_commesse_2_tv"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean ib_dw_report = true
end type

