﻿$PBExportHeader$w_tes_stato_commesse.srw
$PBExportComments$Window testata stato commesse
forward
global type w_tes_stato_commesse from w_cs_xx_principale
end type
type st_4 from statictext within w_tes_stato_commesse
end type
type em_1 from editmask within w_tes_stato_commesse
end type
type st_5 from statictext within w_tes_stato_commesse
end type
type em_2 from editmask within w_tes_stato_commesse
end type
type cb_aggiorna from commandbutton within w_tes_stato_commesse
end type
type mle_1 from multilineedit within w_tes_stato_commesse
end type
type dw_det_lista_stato_commesse from uo_cs_xx_dw within w_tes_stato_commesse
end type
type dw_tes_lista_stato_commesse from uo_cs_xx_dw within w_tes_stato_commesse
end type
type st_1 from statictext within w_tes_stato_commesse
end type
type st_2 from statictext within w_tes_stato_commesse
end type
type st_3 from statictext within w_tes_stato_commesse
end type
type cb_calcola from commandbutton within w_tes_stato_commesse
end type
type rb_costo_standard from radiobutton within w_tes_stato_commesse
end type
type rb_costo_ultimo from radiobutton within w_tes_stato_commesse
end type
type rb_prezzo_acquisto from radiobutton within w_tes_stato_commesse
end type
type st_totale_ore_spese from editmask within w_tes_stato_commesse
end type
type st_costo_totale_ore_spese from editmask within w_tes_stato_commesse
end type
type st_costo_totale_mat_prime from editmask within w_tes_stato_commesse
end type
type st_6 from statictext within w_tes_stato_commesse
end type
type st_costo_totale from editmask within w_tes_stato_commesse
end type
end forward

global type w_tes_stato_commesse from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3465
string title = "Stato Avanzamento Commesse"
st_4 st_4
em_1 em_1
st_5 st_5
em_2 em_2
cb_aggiorna cb_aggiorna
mle_1 mle_1
dw_det_lista_stato_commesse dw_det_lista_stato_commesse
dw_tes_lista_stato_commesse dw_tes_lista_stato_commesse
st_1 st_1
st_2 st_2
st_3 st_3
cb_calcola cb_calcola
rb_costo_standard rb_costo_standard
rb_costo_ultimo rb_costo_ultimo
rb_prezzo_acquisto rb_prezzo_acquisto
st_totale_ore_spese st_totale_ore_spese
st_costo_totale_ore_spese st_costo_totale_ore_spese
st_costo_totale_mat_prime st_costo_totale_mat_prime
st_6 st_6
st_costo_totale st_costo_totale
end type
global w_tes_stato_commesse w_tes_stato_commesse

type variables
string is_cod_attivita
string is_cod_cat_attrezzature
string is_cod_attrezzatura
datetime id_data_giorno
end variables

event pc_setwindow;call super::pc_setwindow;
dw_tes_lista_stato_commesse.set_dw_options(sqlca,pcca.null_object, & 
													c_nonew + c_nomodify + c_nodelete + c_disablecc , & 
													c_disableccinsert)


dw_det_lista_stato_commesse.set_dw_options(sqlca,dw_tes_lista_stato_commesse,c_sharedata + & 
													 	 c_scrollparent + c_nonew + c_nomodify + c_nodelete + & 
														 c_disablecc,c_disableccinsert)

iuo_dw_main = dw_tes_lista_stato_commesse
end event

on w_tes_stato_commesse.create
int iCurrent
call super::create
this.st_4=create st_4
this.em_1=create em_1
this.st_5=create st_5
this.em_2=create em_2
this.cb_aggiorna=create cb_aggiorna
this.mle_1=create mle_1
this.dw_det_lista_stato_commesse=create dw_det_lista_stato_commesse
this.dw_tes_lista_stato_commesse=create dw_tes_lista_stato_commesse
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.cb_calcola=create cb_calcola
this.rb_costo_standard=create rb_costo_standard
this.rb_costo_ultimo=create rb_costo_ultimo
this.rb_prezzo_acquisto=create rb_prezzo_acquisto
this.st_totale_ore_spese=create st_totale_ore_spese
this.st_costo_totale_ore_spese=create st_costo_totale_ore_spese
this.st_costo_totale_mat_prime=create st_costo_totale_mat_prime
this.st_6=create st_6
this.st_costo_totale=create st_costo_totale
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_4
this.Control[iCurrent+2]=this.em_1
this.Control[iCurrent+3]=this.st_5
this.Control[iCurrent+4]=this.em_2
this.Control[iCurrent+5]=this.cb_aggiorna
this.Control[iCurrent+6]=this.mle_1
this.Control[iCurrent+7]=this.dw_det_lista_stato_commesse
this.Control[iCurrent+8]=this.dw_tes_lista_stato_commesse
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.st_2
this.Control[iCurrent+11]=this.st_3
this.Control[iCurrent+12]=this.cb_calcola
this.Control[iCurrent+13]=this.rb_costo_standard
this.Control[iCurrent+14]=this.rb_costo_ultimo
this.Control[iCurrent+15]=this.rb_prezzo_acquisto
this.Control[iCurrent+16]=this.st_totale_ore_spese
this.Control[iCurrent+17]=this.st_costo_totale_ore_spese
this.Control[iCurrent+18]=this.st_costo_totale_mat_prime
this.Control[iCurrent+19]=this.st_6
this.Control[iCurrent+20]=this.st_costo_totale
end on

on w_tes_stato_commesse.destroy
call super::destroy
destroy(this.st_4)
destroy(this.em_1)
destroy(this.st_5)
destroy(this.em_2)
destroy(this.cb_aggiorna)
destroy(this.mle_1)
destroy(this.dw_det_lista_stato_commesse)
destroy(this.dw_tes_lista_stato_commesse)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.cb_calcola)
destroy(this.rb_costo_standard)
destroy(this.rb_costo_ultimo)
destroy(this.rb_prezzo_acquisto)
destroy(this.st_totale_ore_spese)
destroy(this.st_costo_totale_ore_spese)
destroy(this.st_costo_totale_mat_prime)
destroy(this.st_6)
destroy(this.st_costo_totale)
end on

type st_4 from statictext within w_tes_stato_commesse
integer x = 23
integer y = 1240
integer width = 219
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "da data:"
boolean focusrectangle = false
end type

type em_1 from editmask within w_tes_stato_commesse
integer x = 251
integer y = 1240
integer width = 411
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "0"
double increment = 1
end type

type st_5 from statictext within w_tes_stato_commesse
integer x = 686
integer y = 1240
integer width = 187
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "a data:"
boolean focusrectangle = false
end type

type em_2 from editmask within w_tes_stato_commesse
integer x = 869
integer y = 1240
integer width = 411
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = "<€w_grafico_dist_comm"
double increment = 1
end type

type cb_aggiorna from commandbutton within w_tes_stato_commesse
integer x = 1326
integer y = 1240
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;dw_tes_lista_stato_commesse.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type mle_1 from multilineedit within w_tes_stato_commesse
integer x = 23
integer y = 20
integer width = 3383
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "L~'elenco comprende solo le commesse con almeno una sessione di fase iniziata nel periodo selezionato"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
end type

type dw_det_lista_stato_commesse from uo_cs_xx_dw within w_tes_stato_commesse
integer x = 1714
integer y = 120
integer width = 1691
integer height = 680
integer taborder = 10
string dataobject = "d_det_lista_stato_commesse"
boolean hscrollbar = true
borderstyle borderstyle = styleraised!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
datetime ld_data_inizio,ld_data_fine

ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))

l_Error = Retrieve(s_cs_xx.cod_azienda,ld_data_inizio,ld_data_fine)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

type dw_tes_lista_stato_commesse from uo_cs_xx_dw within w_tes_stato_commesse
integer x = 23
integer y = 120
integer width = 1669
integer height = 1100
integer taborder = 60
string dataobject = "d_tes_lista_stato_commesse"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
datetime ld_data_inizio,ld_data_fine

ld_data_inizio = datetime(date(em_1.text))
ld_data_fine = datetime(date(em_2.text))

l_Error = Retrieve(s_cs_xx.cod_azienda,ld_data_inizio,ld_data_fine)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


end event

event rowfocuschanged;call super::rowfocuschanged;if dw_tes_lista_stato_commesse.rowcount() > 0  then
	cb_calcola.triggerevent(clicked!)
end if
end event

type st_1 from statictext within w_tes_stato_commesse
integer x = 1714
integer y = 820
integer width = 823
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Totale Ore Spese:"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_2 from statictext within w_tes_stato_commesse
integer x = 1714
integer y = 1020
integer width = 823
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Costo Totale Materie prime:"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_3 from statictext within w_tes_stato_commesse
integer x = 1714
integer y = 920
integer width = 823
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Costo Totale Ore:"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_calcola from commandbutton within w_tes_stato_commesse
integer x = 3040
integer y = 1240
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcola"
end type

event clicked;double ldd_totale_ore_spese,ldd_costo_totale_ore,ldd_costo_totale_mat_prime,ldd_tempo_lavorazione, ldd_somma_tempi,& 
		 ldd_tempo_attrezzaggio_commessa,ldd_tempo_attrezzaggio,ldd_costo_medio_orario,ldd_somma_ore,ldd_somma_costo_ore,&
		 ldd_tempo_risorsa_umana,ldd_quan_utilizzata,ldd_costo_standard,ldd_costo_ultimo,ldd_prezzo_acquisto,ldd_somma_costo_mp
string ls_cod_prodotto_fase,ls_cod_reparto,ls_cod_lavorazione,ls_cod_cat_attrezzature,ls_cod_cat_attrezzature_2, &
		 ls_cod_prodotto_mp, ls_cod_versione
long ll_num_commessa
integer li_anno_commessa

if dw_det_lista_stato_commesse.rowcount() > 0 and dw_det_lista_stato_commesse.getrow() > 0 then

	li_anno_commessa=dw_det_lista_stato_commesse.getitemnumber(dw_det_lista_stato_commesse.getrow(),"anno_commessa")
	ll_num_commessa=dw_det_lista_stato_commesse.getitemnumber(dw_det_lista_stato_commesse.getrow(),"num_commessa")
else
	return
end if

declare righe_ap cursor for
select  cod_prodotto,
		  cod_versione,
		  cod_reparto,
		  cod_lavorazione,
		  tempo_lavorazione,
		  tempo_attrezzaggio_commessa,
		  tempo_attrezzaggio,
		  tempo_risorsa_umana
from    avan_produzione_com
where   cod_azienda = :s_cs_xx.cod_azienda and   
		  anno_commessa = :li_anno_commessa  and   
		  num_commessa = :ll_num_commessa;

open righe_ap;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "SEP", "Errore durante l'apertura del cursore delle fasi di lavorazione:" + sqlca.sqlerrtext)
	return
end if


do while 1 = 1
	fetch righe_ap 
	into  :ls_cod_prodotto_fase,
	      :ls_cod_versione,
			:ls_cod_reparto,
			:ls_cod_lavorazione,
			:ldd_tempo_lavorazione,
			:ldd_tempo_attrezzaggio_commessa,
			:ldd_tempo_attrezzaggio,
			:ldd_tempo_risorsa_umana;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext)
		close righe_ap;
		return
	end if
	
	ldd_somma_tempi = (ldd_tempo_lavorazione + ldd_tempo_attrezzaggio_commessa + ldd_tempo_attrezzaggio) / 60
	
	select cod_cat_attrezzature,
			 cod_cat_attrezzature_2
	into   :ls_cod_cat_attrezzature,
			 :ls_cod_cat_attrezzature_2
	from   tes_fasi_lavorazione
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			 cod_prodotto = :ls_cod_prodotto_fase 	and    
			 cod_versione = :ls_cod_versione       and
			 cod_reparto = :ls_cod_reparto 			and    
			 cod_lavorazione = :ls_cod_lavorazione;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext)
		close righe_ap;
		return
	end if
	
	select costo_medio_orario
	into   :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_cat_attrezzature = :ls_cod_cat_attrezzature;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext)
		close righe_ap;
		return
	end if
	
	ldd_somma_ore = ldd_somma_ore + ldd_somma_tempi
	ldd_somma_costo_ore = ldd_somma_costo_ore + ldd_somma_tempi * ldd_costo_medio_orario
	
	
	
loop

close righe_ap;

st_costo_totale_ore_spese.text = string (ldd_somma_costo_ore)
st_totale_ore_spese.text = string (ldd_somma_ore)

declare righe_mp_com cursor for
select  cod_prodotto,
		  quan_utilizzata
from    mat_prime_commessa
where   cod_azienda = :s_cs_xx.cod_azienda and     
		  anno_commessa = :li_anno_commessa  and     
		  num_commessa = :ll_num_commessa;

open righe_mp_com;

do while 1 = 1
	fetch righe_mp_com 
	into  :ls_cod_prodotto_mp,
			:ldd_quan_utilizzata;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "Sep", "Errore durante la fetch del cursore delle materie prime: " + sqlca.sqlerrtext)
		close righe_mp_com;
		return
	end if
	
	select costo_standard,
			 costo_ultimo,
			 prezzo_acquisto
	into   :ldd_costo_standard,
			 :ldd_costo_ultimo,
			 :ldd_prezzo_acquisto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto = :ls_cod_prodotto_mp;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "Sep", "Errore durante la ricerca dei costi/prezzi in anagrafica prodotti: " + sqlca.sqlerrtext)
		close righe_ap;
		return
	end if
	
	if rb_costo_standard.checked = true then
		ldd_somma_costo_mp = ldd_somma_costo_mp + ldd_quan_utilizzata * ldd_costo_standard
	end if
	
	if rb_costo_ultimo.checked = true then
		ldd_somma_costo_mp = ldd_somma_costo_mp + ldd_quan_utilizzata * ldd_costo_ultimo
	end if

	if rb_prezzo_acquisto.checked = true then
		ldd_somma_costo_mp = ldd_somma_costo_mp + ldd_quan_utilizzata * ldd_prezzo_acquisto
	end if
	
loop

close righe_mp_com;
		 
st_costo_totale_mat_prime.text = string (ldd_somma_costo_mp)
st_costo_totale.text = string(ldd_somma_costo_mp + ldd_somma_costo_ore)

end event

type rb_costo_standard from radiobutton within w_tes_stato_commesse
integer x = 1714
integer y = 1240
integer width = 434
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Costo Standard"
boolean checked = true
boolean lefttext = true
end type

type rb_costo_ultimo from radiobutton within w_tes_stato_commesse
integer x = 2194
integer y = 1240
integer width = 343
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Costo Ultimo"
boolean lefttext = true
end type

type rb_prezzo_acquisto from radiobutton within w_tes_stato_commesse
integer x = 2583
integer y = 1240
integer width = 434
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Prezzo Acquisto"
boolean lefttext = true
end type

type st_totale_ore_spese from editmask within w_tes_stato_commesse
integer x = 2560
integer y = 820
integer width = 594
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
alignment alignment = right!
borderstyle borderstyle = styleraised!
string displaydata = "ˆ\~t€°$®Ð°x\~tdragenter"
end type

type st_costo_totale_ore_spese from editmask within w_tes_stato_commesse
integer x = 2560
integer y = 920
integer width = 594
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
alignment alignment = right!
borderstyle borderstyle = styleraised!
string displaydata = "ˆ\~tpbm_bndragdrop"
end type

type st_costo_totale_mat_prime from editmask within w_tes_stato_commesse
integer x = 2560
integer y = 1020
integer width = 594
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
alignment alignment = right!
borderstyle borderstyle = styleraised!
string displaydata = "ˆ\~tpbm_bndragdrop"
end type

type st_6 from statictext within w_tes_stato_commesse
integer x = 1714
integer y = 1120
integer width = 823
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Costo Totale:"
alignment alignment = right!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_costo_totale from editmask within w_tes_stato_commesse
integer x = 2560
integer y = 1120
integer width = 594
integer height = 80
integer taborder = 32
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
alignment alignment = right!
borderstyle borderstyle = styleraised!
string displaydata = "ˆ\~tpbm_bndragdrop"
end type

