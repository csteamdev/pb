﻿$PBExportHeader$w_prod_bolle_in_com.srw
$PBExportComments$Window prod_bolle_in_com
forward
global type w_prod_bolle_in_com from w_cs_xx_principale
end type
type dw_scelta_fornitore from uo_cs_xx_dw within w_prod_bolle_in_com
end type
type cb_genera from commandbutton within w_prod_bolle_in_com
end type
type cbx_bolla_ingresso from checkbox within w_prod_bolle_in_com
end type
type st_prodotto_ingresso from statictext within w_prod_bolle_in_com
end type
type em_quan_arrivata from editmask within w_prod_bolle_in_com
end type
type em_data_ingresso from editmask within w_prod_bolle_in_com
end type
type st_1 from statictext within w_prod_bolle_in_com
end type
type st_2 from statictext within w_prod_bolle_in_com
end type
type cbx_acc_mat from checkbox within w_prod_bolle_in_com
end type
type dw_scelta_stock_fornitore from uo_cs_xx_dw within w_prod_bolle_in_com
end type
type cb_aggiungi from commandbutton within w_prod_bolle_in_com
end type
type cb_togli from commandbutton within w_prod_bolle_in_com
end type
type dw_det_prod_bolle_in_com from uo_cs_xx_dw within w_prod_bolle_in_com
end type
type st_3 from statictext within w_prod_bolle_in_com
end type
type st_stquan_impegnata from statictext within w_prod_bolle_in_com
end type
type st_quan_impegnata from statictext within w_prod_bolle_in_com
end type
type dw_folder from u_folder within w_prod_bolle_in_com
end type
type dw_lista_prod_bolle_in_com from uo_cs_xx_dw within w_prod_bolle_in_com
end type
type dw_elenco_fasi_esterne_attivazione from uo_cs_xx_dw within w_prod_bolle_in_com
end type
type dw_lista_prodotti_scarico from uo_cs_xx_dw within w_prod_bolle_in_com
end type
type dw_elenco_prod_scarico_fornitore from uo_cs_xx_dw within w_prod_bolle_in_com
end type
end forward

global type w_prod_bolle_in_com from w_cs_xx_principale
integer width = 3351
integer height = 1704
string title = "Bolle CTL ingresso"
dw_scelta_fornitore dw_scelta_fornitore
cb_genera cb_genera
cbx_bolla_ingresso cbx_bolla_ingresso
st_prodotto_ingresso st_prodotto_ingresso
em_quan_arrivata em_quan_arrivata
em_data_ingresso em_data_ingresso
st_1 st_1
st_2 st_2
cbx_acc_mat cbx_acc_mat
dw_scelta_stock_fornitore dw_scelta_stock_fornitore
cb_aggiungi cb_aggiungi
cb_togli cb_togli
dw_det_prod_bolle_in_com dw_det_prod_bolle_in_com
st_3 st_3
st_stquan_impegnata st_stquan_impegnata
st_quan_impegnata st_quan_impegnata
dw_folder dw_folder
dw_lista_prod_bolle_in_com dw_lista_prod_bolle_in_com
dw_elenco_fasi_esterne_attivazione dw_elenco_fasi_esterne_attivazione
dw_lista_prodotti_scarico dw_lista_prodotti_scarico
dw_elenco_prod_scarico_fornitore dw_elenco_prod_scarico_fornitore
end type
global w_prod_bolle_in_com w_prod_bolle_in_com

type variables
long il_anno_commessa,il_num_commessa,il_prog_riga
string is_cod_prodotto_fase,is_cod_reparto,is_cod_lavorazione, is_cod_versione_fase
end variables

forward prototypes
public function integer wf_imposta_stock ()
public function integer f_genera_lista_prodotti (integer fi_anno_commessa, long fl_num_commessa, long fl_prog_riga, string fs_cod_prodotto, string fs_cod_versione, double fd_quan_in_produzione, integer fi_num_livello_cor, string fs_errore)
end prototypes

public function integer wf_imposta_stock ();string   ls_cod_prodotto_mp,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, ls_test,ls_cod_fornitore,& 
			ls_cod_tipo_commessa,ls_flag_mat_prima,ls_modify,ls_cod_reparto, & 
			ls_cod_lavorazione,ls_cod_prodotto_fase
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_orari, & 
			ll_num_fasi_aperte,ll_prog_spedizione
datetime ldt_data_stock
double   ldd_quan_disponibile,ldd_quan_necessaria,ldd_quan_prelevata,ldd_quan_impegnata
integer  li_anno_commessa

dw_scelta_stock_fornitore.reset()

if dw_lista_prodotti_scarico.rowcount() = 0 then return 0

ls_cod_prodotto_mp = dw_lista_prodotti_scarico.getitemstring(dw_lista_prodotti_scarico.getrow(),"cod_prodotto")
ldd_quan_necessaria = dw_lista_prodotti_scarico.getitemnumber(dw_lista_prodotti_scarico.getrow(),"quan_utilizzo")
ls_cod_fornitore = dw_scelta_fornitore.gettext()

if isnull(ls_cod_fornitore) or ls_cod_fornitore="" then
	g_mb.messagebox("Sep","Selezionare un fornitore!", information!)
	return 0
end if

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return 0
end if
	
select cod_deposito
into   :ls_cod_deposito
from   anag_fornitori
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_fornitore=:ls_cod_fornitore;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	 
	return 0
end if

declare righe_stock cursor for
SELECT  cod_ubicazione,   
		  cod_lotto,   
		  data_stock,   
		  prog_stock,
		  giacenza_stock - quan_assegnata - quan_in_spedizione  
 FROM   stock   
 where  cod_azienda=:s_cs_xx.cod_azienda
 and    cod_prodotto =:ls_cod_prodotto_mp
 and    cod_deposito =:ls_cod_deposito
 and    giacenza_stock - quan_assegnata - quan_in_spedizione >0
 ORDER BY data_stock asc;

 open righe_stock;

 if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	 
	return 0
 end if

do while 1 = 1
	fetch righe_stock 
	into  :ls_cod_ubicazione,
			:ls_cod_lotto,   
			:ldt_data_stock,   
			:ll_prog_stock,
			:ldd_quan_disponibile;

	choose case sqlca.sqlcode  
		case 100
			exit
		case is < 0 
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext, stopsign!)
			
				
			exit
	end choose

	ll_num_righe=dw_scelta_stock_fornitore.rowcount()

	if ll_num_righe=0 then
		ll_num_righe=1
	else
		ll_num_righe++
	end if

	dw_scelta_stock_fornitore.insertrow(ll_num_righe)
	dw_scelta_stock_fornitore.setitem(ll_num_righe,"cod_prodotto",ls_cod_prodotto_mp)
	dw_scelta_stock_fornitore.setitem(ll_num_righe,"cod_deposito",ls_cod_deposito)
	dw_scelta_stock_fornitore.setitem(ll_num_righe,"cod_ubicazione",ls_cod_ubicazione)
	dw_scelta_stock_fornitore.setitem(ll_num_righe,"cod_lotto",ls_cod_lotto)
	dw_scelta_stock_fornitore.setitem(ll_num_righe,"data_stock",ldt_data_stock)
	dw_scelta_stock_fornitore.setitem(ll_num_righe,"prog_stock",ll_prog_stock)
	dw_scelta_stock_fornitore.setitem(ll_num_righe,"quan_disponibile",ldd_quan_disponibile)
	
	if ldd_quan_necessaria > 0 then
			if ldd_quan_disponibile >= ldd_quan_necessaria then
				ldd_quan_prelevata = ldd_quan_necessaria
				ldd_quan_necessaria = 0 
			else
				ldd_quan_prelevata = ldd_quan_disponibile
				ldd_quan_necessaria = ldd_quan_necessaria - ldd_quan_prelevata
			end if
	else
		ldd_quan_prelevata = 0
	end if
	
	dw_scelta_stock_fornitore.setitem(ll_num_righe,"quan_prelevata",ldd_quan_prelevata)

loop

close righe_stock;

dw_scelta_stock_fornitore.Reset_DW_Modified(c_ResetChildren)
dw_scelta_stock_fornitore.settaborder("quan_prelevata",10)

select sum(quan_impegnata)
into   :ldd_quan_impegnata
from   impegni_deposito_fasi_com
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_deposito=:ls_cod_deposito
and    cod_prodotto_spedito=:ls_cod_prodotto_mp;
	
if sqlca.sqlcode< 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 0
end if

st_quan_impegnata.text = string(ldd_quan_impegnata)

return 0

end function

public function integer f_genera_lista_prodotti (integer fi_anno_commessa, long fl_num_commessa, long fl_prog_riga, string fs_cod_prodotto, string fs_cod_versione, double fd_quan_in_produzione, integer fi_num_livello_cor, string fs_errore);string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, &
			 ls_cod_prodotto_inserito,ls_cod_lavorazione,ls_test, ls_flag_fine_fase, ls_cod_versione_figlio, ls_cod_versione_inserito, ls_cod_versione_variante, & 
			 ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante
long      ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_handle,ll_i,ll_num_righe_dw
integer   li_risposta
double    ldd_quan_utilizzo,ldd_quan_utilizzo_variante,ldd_quan_utilizzo_1
boolean   lb_flag_trovata
datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli = 1 to ll_num_righe
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fd_quan_in_produzione
	ls_flag_materia_prima=lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	

	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio
	
	select cod_prodotto,
			 cod_versione_variante,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ls_cod_versione_variante,
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda		and    
			 anno_commessa = :fi_anno_commessa		and    
			 num_commessa = :fl_num_commessa			and    
			 cod_prodotto_padre = :fs_cod_prodotto and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
			 cod_versione = :fs_cod_versione 		and
			 cod_versione_figlio = :ls_cod_versione_figlio;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_cod_versione_inserito = ls_cod_versione_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante* fd_quan_in_produzione
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	//***************************
	//ricerca se già inserito
	lb_flag_trovata = false
	for ll_i = 1 to dw_lista_prodotti_scarico.rowcount()
		dw_lista_prodotti_scarico.setrow(ll_i)		
		if ls_cod_prodotto_inserito = dw_lista_prodotti_scarico.getitemstring(ll_i,"cod_prodotto") then 
			lb_flag_trovata=true
			ldd_quan_utilizzo_1 = dw_lista_prodotti_scarico.getitemnumber(ll_i,"quan_utilizzo")
			ldd_quan_utilizzo = ldd_quan_utilizzo + ldd_quan_utilizzo_1 
			dw_lista_prodotti_scarico.setitem(ll_i,"quan_utilizzo",ldd_quan_utilizzo)
		end if
	next
	
	if lb_flag_trovata then continue
	//***************************

	if ls_flag_materia_prima = 'N' then
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and 	 
				 cod_prodotto_padre = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
		
		if sqlca.sqlcode = 100 then 
			
			//*************************************
			//inserimento in datawindow
			ll_num_righe_dw = dw_lista_prodotti_scarico.rowcount()
			
			if ll_num_righe_dw=0 then
				ll_num_righe_dw=1
			else
				ll_num_righe_dw++
			end if
			
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto_inserito;
		
			if sqlca.sqlcode < 0 then
				ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
			
			dw_lista_prodotti_scarico.insertrow(ll_num_righe_dw)
			dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"cod_prodotto",ls_cod_prodotto_inserito)
			dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"des_prodotto",ls_des_prodotto)
			dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"quan_utilizzo",ldd_quan_utilizzo)
			
			//*********************************
			continue
		end if
	else
		
		//*************************************
		//inserimento in datawindow
		ll_num_righe_dw=dw_lista_prodotti_scarico.rowcount()
	
		if ll_num_righe_dw=0 then
			ll_num_righe_dw=1
		else
			ll_num_righe_dw++
		end if
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto_inserito;
	
		if sqlca.sqlcode < 0 then
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		dw_lista_prodotti_scarico.insertrow(ll_num_righe_dw)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"cod_prodotto",ls_cod_prodotto_inserito)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"des_prodotto",ls_des_prodotto)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"quan_utilizzo",ldd_quan_utilizzo)
		
		//*********************************
		
		continue
	end if

	

	//***************************
	//Verifica se il prodotto è un SL di una fase già finita (flag_fine_fase = 'S'), in questo caso è come se fosse una MP
	select flag_fine_fase
	into   :ls_flag_fine_fase
	from   avan_produzione_com
	where  cod_azienda = :s_cs_xx.cod_azienda		and    
			 anno_commessa = :fi_anno_commessa		and    
			 num_commessa = :fl_num_commessa			and    
			 prog_riga = :fl_prog_riga					and    
			 cod_prodotto = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if sqlca.sqlcode < 0 then
		ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	if ls_flag_fine_fase = 'S' then 
		//*************************************
		//inserimento in datawindow
		ll_num_righe_dw=dw_lista_prodotti_scarico.rowcount()
	
		if ll_num_righe_dw=0 then
			ll_num_righe_dw=1
		else
			ll_num_righe_dw++
		end if
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto_inserito;
	
		if sqlca.sqlcode < 0 then
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		dw_lista_prodotti_scarico.insertrow(ll_num_righe_dw)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"cod_prodotto",ls_cod_prodotto_inserito)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"des_prodotto",ls_des_prodotto)
		dw_lista_prodotti_scarico.setitem(ll_num_righe_dw,"quan_utilizzo",ldd_quan_utilizzo)
		
		//*********************************
		continue
	end if
	
	//***************************


	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
	       cod_prodotto_padre = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		continue
	else
		li_risposta=f_genera_lista_prodotti(fi_anno_commessa,fl_num_commessa,fl_prog_riga, ls_cod_prodotto_inserito, &
																													  ls_cod_versione_inserito, &
																													  ldd_quan_utilizzo, fi_num_livello_cor + 1,ls_errore)

		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

return 0
end function

on w_prod_bolle_in_com.create
int iCurrent
call super::create
this.dw_scelta_fornitore=create dw_scelta_fornitore
this.cb_genera=create cb_genera
this.cbx_bolla_ingresso=create cbx_bolla_ingresso
this.st_prodotto_ingresso=create st_prodotto_ingresso
this.em_quan_arrivata=create em_quan_arrivata
this.em_data_ingresso=create em_data_ingresso
this.st_1=create st_1
this.st_2=create st_2
this.cbx_acc_mat=create cbx_acc_mat
this.dw_scelta_stock_fornitore=create dw_scelta_stock_fornitore
this.cb_aggiungi=create cb_aggiungi
this.cb_togli=create cb_togli
this.dw_det_prod_bolle_in_com=create dw_det_prod_bolle_in_com
this.st_3=create st_3
this.st_stquan_impegnata=create st_stquan_impegnata
this.st_quan_impegnata=create st_quan_impegnata
this.dw_folder=create dw_folder
this.dw_lista_prod_bolle_in_com=create dw_lista_prod_bolle_in_com
this.dw_elenco_fasi_esterne_attivazione=create dw_elenco_fasi_esterne_attivazione
this.dw_lista_prodotti_scarico=create dw_lista_prodotti_scarico
this.dw_elenco_prod_scarico_fornitore=create dw_elenco_prod_scarico_fornitore
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_scelta_fornitore
this.Control[iCurrent+2]=this.cb_genera
this.Control[iCurrent+3]=this.cbx_bolla_ingresso
this.Control[iCurrent+4]=this.st_prodotto_ingresso
this.Control[iCurrent+5]=this.em_quan_arrivata
this.Control[iCurrent+6]=this.em_data_ingresso
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.st_2
this.Control[iCurrent+9]=this.cbx_acc_mat
this.Control[iCurrent+10]=this.dw_scelta_stock_fornitore
this.Control[iCurrent+11]=this.cb_aggiungi
this.Control[iCurrent+12]=this.cb_togli
this.Control[iCurrent+13]=this.dw_det_prod_bolle_in_com
this.Control[iCurrent+14]=this.st_3
this.Control[iCurrent+15]=this.st_stquan_impegnata
this.Control[iCurrent+16]=this.st_quan_impegnata
this.Control[iCurrent+17]=this.dw_folder
this.Control[iCurrent+18]=this.dw_lista_prod_bolle_in_com
this.Control[iCurrent+19]=this.dw_elenco_fasi_esterne_attivazione
this.Control[iCurrent+20]=this.dw_lista_prodotti_scarico
this.Control[iCurrent+21]=this.dw_elenco_prod_scarico_fornitore
end on

on w_prod_bolle_in_com.destroy
call super::destroy
destroy(this.dw_scelta_fornitore)
destroy(this.cb_genera)
destroy(this.cbx_bolla_ingresso)
destroy(this.st_prodotto_ingresso)
destroy(this.em_quan_arrivata)
destroy(this.em_data_ingresso)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.cbx_acc_mat)
destroy(this.dw_scelta_stock_fornitore)
destroy(this.cb_aggiungi)
destroy(this.cb_togli)
destroy(this.dw_det_prod_bolle_in_com)
destroy(this.st_3)
destroy(this.st_stquan_impegnata)
destroy(this.st_quan_impegnata)
destroy(this.dw_folder)
destroy(this.dw_lista_prod_bolle_in_com)
destroy(this.dw_elenco_fasi_esterne_attivazione)
destroy(this.dw_lista_prodotti_scarico)
destroy(this.dw_elenco_prod_scarico_fornitore)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ], l_1_objects[ ],l_2_objects[ ],l_3_objects[ ]

set_w_options(c_noenablepopup)

l_objects[1] = dw_elenco_fasi_esterne_attivazione
dw_folder.fu_AssignTab(1, "&Fasi Esterne", l_Objects[])

l_1_objects[1] = dw_lista_prodotti_scarico
l_1_objects[2] = dw_scelta_stock_fornitore
l_1_objects[3] = cb_aggiungi
l_1_objects[4] = st_stquan_impegnata
l_1_objects[5] = st_quan_impegnata
dw_folder.fu_AssignTab(2, "&Scelta Prod.Scarico", l_1_Objects[])

l_2_objects[1] = dw_elenco_prod_scarico_fornitore
l_2_objects[2] = cb_togli
dw_folder.fu_AssignTab(3, "&Elenco Prod.Scarico", l_2_Objects[])

l_3_objects[1] = dw_lista_prod_bolle_in_com
l_3_objects[2] = dw_det_prod_bolle_in_com
dw_folder.fu_AssignTab(4, "&Ingressi Merce", l_3_Objects[])

dw_folder.fu_FolderCreate(4,4)

dw_elenco_fasi_esterne_attivazione.set_dw_options(sqlca, i_openparm,c_nonew + & 
												  				  c_nodelete + c_nomodify +  & 
												  				  c_disablecc,c_disableccinsert)

dw_lista_prod_bolle_in_com.set_dw_options(sqlca, &
                                    		   pcca.null_object, &
		                                       c_scrollparent + c_nonew + c_nodelete, &
      		                                 c_default)


dw_det_prod_bolle_in_com.set_dw_options(sqlca, &
                                    		 dw_lista_prod_bolle_in_com, &
		                                     c_sharedata + c_scrollparent + c_nonew + c_nodelete, &
      		                               c_default)
														 
														 
dw_lista_prodotti_scarico.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)

dw_scelta_stock_fornitore.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)

dw_elenco_prod_scarico_fornitore.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)


dw_scelta_fornitore.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_newonopen + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)


iuo_dw_main = dw_elenco_fasi_esterne_attivazione

dw_folder.fu_SelectTab(1)

save_on_close(c_socnosave)

em_data_ingresso.text = string(today())

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_scelta_fornitore, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_terzista = 'S' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end event

type dw_scelta_fornitore from uo_cs_xx_dw within w_prod_bolle_in_com
integer x = 23
integer y = 1300
integer width = 2286
integer height = 80
integer taborder = 50
string dataobject = "d_scelta_fornitore_terzista"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	wf_imposta_stock()
	
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()

end if
end event

event getfocus;call super::getfocus;dw_scelta_fornitore.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_scelta_fornitore,"cod_fornitore")
end choose
end event

type cb_genera from commandbutton within w_prod_bolle_in_com
integer x = 2926
integer y = 1500
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera In."
end type

event clicked;string   ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_cod_prodotto_fase,& 
			ls_cod_versione,ls_errore,ls_cod_mov_magazzino, ls_cod_prodotto, ls_cod_versione_fase, &
			ls_cod_tipo_det_ven,ls_cod_tipo_bol_ven,ls_conferma, ls_cod_fornitore_ingresso, ls_cod_prodotto_spedito,& 
			ls_cod_ubicazione[],ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[], ls_flag_tipo_avanzamento,&
			ls_cod_deposito_sl[],ls_cod_tipo_mov_ver_sl,ls_cod_ubicazione_test, ls_cod_deposito_fornitore[],& 
			ls_cod_lotto_test,ls_flag_cliente,ls_flag_fornitore,ls_cod_prodotto_scarico,ls_cod_tipo_mov_scarico_terzista
long     ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_ingresso,ll_t,ll_anno_reg_sl,ll_num_reg_sl, & 
			ll_prog_stock[],ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_anno_registrazione[],ll_num_registrazione[], & 
		   ll_righe_scarico,ll_num_fasi_aperte
datetime ldt_oggi,ld_data_stock[]
double   ldd_quan_arrivata,ldd_quan_prelevata,ldd_quan_in_produzione,ldd_quan_in_produzione_prog,ldd_quan_prodotta,&
			ldd_quan_impegnata_attuale,ldd_quan_impegnata_fornitore,ldd_quan_utilizzo,ldd_quan_impegnata,ldd_somma_quan_prelevata
integer  li_anno_commessa,li_risposta

uo_magazzino luo_mag


if (g_mb.messagebox("Sep","Sei sicuro di generare l'ingresso merce?",Exclamation!,yesno!, 2))=2 then return
		 
ll_righe_scarico = dw_elenco_prod_scarico_fornitore.rowcount()

if ll_righe_scarico = 0 then
	g_mb.messagebox("Sep","Attenzione non è stata selezionata alcuna materia prima da scaricare dal magazzino del fornitore. Verificare.",exclamation!)
	return
end if

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ldd_quan_arrivata = double(em_quan_arrivata.text)
ldd_quan_utilizzo = dw_lista_prodotti_scarico.getitemnumber(dw_lista_prodotti_scarico.getrow(),"quan_utilizzo")
ls_cod_prodotto_spedito = dw_lista_prodotti_scarico.getitemstring(dw_lista_prodotti_scarico.getrow(),"cod_prodotto")

ldt_oggi = datetime(today(),time('00:00:00'))

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   anag_commesse
WHERE  cod_azienda = :s_cs_xx.cod_azienda  
AND    anno_commessa = :li_anno_commessa  
AND    num_commessa = :ll_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
	return
end if

select cod_deposito_sl,
		 cod_tipo_mov_ver_sl,
		 cod_tipo_mov_scarico_terzista
into   :ls_cod_deposito_sl[1],
		 :ls_cod_tipo_mov_ver_sl,
		 :ls_cod_tipo_mov_scarico_terzista
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
	return
end if

if isnull(ls_cod_deposito_sl[1]) or isnull(ls_cod_tipo_mov_ver_sl) then
	g_mb.messagebox("Sep", "Attenzione! Il tipo commessa selezionato non è correttamente impostato per eseguire le movimentazioni dei Semilavorati (deposito SL o tipo movimento versamento SL mancante).", stopsign!)
	return
end if

select anno_reg_sl,
		 num_reg_sl
into   :ll_anno_reg_sl,
		 :ll_num_reg_sl
from   avan_produzione_com
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
		 anno_commessa = :li_anno_commessa  AND    
		 num_commessa = :ll_num_commessa    AND    
		 prog_riga = :ll_prog_riga 			AND    
		 cod_prodotto = :ls_cod_prodotto_fase and
		 cod_versione = :ls_cod_versione_fase AND    
		 cod_reparto = :ls_cod_reparto  		AND    
		 cod_lavorazione = :ls_cod_lavorazione;

//*************************** Inizio Verifica correttezza tipo movimento mag per SL

declare righe_det_tipi_m cursor for
select  cod_ubicazione,
		  cod_lotto,
		  flag_cliente,
		  flag_fornitore
from    det_tipi_movimenti
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_movimento=:ls_cod_tipo_mov_ver_sl;

open righe_det_tipi_m;

do while 1=1
	fetch righe_det_tipi_m
	into  :ls_cod_ubicazione_test,
			:ls_cod_lotto_test,
			:ls_flag_cliente,
			:ls_flag_fornitore;

	if sqlca.sqlcode = 100 then exit

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
		close righe_det_tipi_m;
		return
	end if
	

	if isnull(ls_cod_ubicazione_test) then
		g_mb.messagebox("Sep", "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poiche' il codice ubicazione non e' stato indicato." , stopsign!)
		close righe_det_tipi_m;
		return 
	end if

	if isnull(ls_cod_lotto_test) then
		g_mb.messagebox("Sep", "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poiche' il codice lotto non e' stato indicato."  , stopsign!)
		close righe_det_tipi_m;
		return
	end if

	if ls_flag_cliente='S' then
		g_mb.messagebox("Sep", "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poiche' viene richiesto il codice cliente."  , stopsign!)
		close righe_det_tipi_m;
		return
	end if

	if ls_flag_fornitore='S' then
		g_mb.messagebox("Sep", "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_sl +" come versamento di Semilavorato per commesse, poiche' viene richiesto il codice fornitore."   , stopsign!)
		close righe_det_tipi_m;
		return
	end if

loop

close righe_det_tipi_m;

//*************************** Fine Verifica correttezza tipo movimento mag per SL

setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ld_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])
			
if not isnull(ll_anno_reg_sl) and not isnull(ll_num_reg_sl) then
	select cod_ubicazione,
			 cod_lotto,
			 data_stock,
			 prog_stock
	into   :ls_cod_ubicazione[1],
			 :ls_cod_lotto[1],
			 :ld_data_stock[1],
			 :ll_prog_stock[1]
	from   mov_magazzino
	where  cod_azienda =:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_reg_sl
	and	 num_registrazione=:ll_num_reg_sl;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
		return
	end if
	
end if

ll_anno_reg_sl = 0
ll_num_reg_sl = 0

if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_ver_sl, ls_cod_prodotto_fase, ls_cod_deposito_sl[], & 
							  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
							  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
							  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then

	g_mb.messagebox("Sep", "Si è verificato un errore in creazione movimenti.", stopsign!)
	rollback;
	return 

end if

if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
									ls_cod_tipo_mov_ver_sl, ls_cod_prodotto_fase) = -1 then

	 g_mb.messagebox("Sep", "Si è verificato un errore in fase di verifica destinazioni movimenti magazzino." , stopsign!)
  rollback;
  return

end if

luo_mag = create uo_magazzino

li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
										 ls_cod_tipo_mov_ver_sl, &
										 "N", &
										 ls_cod_prodotto_fase, &
										 ldd_quan_arrivata, &
										 1, &
										 ll_num_commessa, &
										 datetime(today(),time('00:00:00')), &
										 "", &
										 ll_anno_reg_des_mov, &
										 ll_num_reg_des_mov, &
										 ls_cod_deposito_sl[], &
										 ls_cod_ubicazione[], &
										 ls_cod_lotto[], &
										 ld_data_stock[], &
										 ll_prog_stock[], &
										 ls_cod_fornitore[], &
										 ls_cod_cliente[], &
										 ll_anno_registrazione[], &
										 ll_num_registrazione[])
										 
destroy luo_mag

if li_risposta=-1 then
	g_mb.messagebox("Sep", "Errore su movimenti magazzino." , stopsign!)
   rollback;
	return
end if
	
li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)

if li_risposta = -1 then
	g_mb.messagebox("Sep", "Si è verificato un errore in fase di cancellazione destinazioni movimenti." , stopsign!)
   rollback;
	return -1
end if

ll_anno_reg_sl = ll_anno_registrazione[1]
ll_num_reg_sl = ll_num_registrazione[1]

select quan_in_produzione
into   :ldd_quan_in_produzione
from   avan_produzione_com
WHERE  cod_azienda = :s_cs_xx.cod_azienda    AND    
       anno_commessa = :li_anno_commessa  	AND    
		 num_commessa = :ll_num_commessa       AND    
		 prog_riga = :ll_prog_riga 				AND    
		 cod_prodotto = :ls_cod_prodotto_fase  and
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  			AND    
		 cod_lavorazione = :ls_cod_lavorazione;

ldd_quan_in_produzione = ldd_quan_in_produzione - ldd_quan_arrivata
if ldd_quan_in_produzione < 0 then ldd_quan_in_produzione = 0

UPDATE avan_produzione_com
SET    quan_prodotta = quan_prodotta + :ldd_quan_arrivata,
		 quan_in_produzione = :ldd_quan_in_produzione,
		 anno_reg_sl = :ll_anno_reg_sl,
		 num_reg_sl = :ll_num_reg_sl
WHERE  cod_azienda = :s_cs_xx.cod_azienda 	AND    
       anno_commessa = :li_anno_commessa  	AND    
		 num_commessa = :ll_num_commessa  		AND    
		 prog_riga = :ll_prog_riga 				AND    
		 cod_prodotto = :ls_cod_prodotto_fase  AND
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  			AND    
		 cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
   rollback;
	return
end if

ls_cod_fornitore_ingresso = dw_scelta_fornitore.gettext()

//***********
//INIZIO INSERIMENTO NUOVO ARRIVO IN TABELLA PROD_BOLLE_IN_COM
select max(prog_ingresso)
into   :ll_prog_ingresso
from   prod_bolle_in_com
where cod_azienda = :s_cs_xx.cod_azienda		and   
		anno_commessa = :li_anno_commessa		and   
		num_commessa = :ll_num_commessa			and   
		prog_riga = :ll_prog_riga					and   
		cod_prodotto_fase = :ls_cod_prodotto_fase and   
		cod_versione = :ls_cod_versione_fase   and
		cod_reparto = :ls_cod_reparto				and   
		cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
	rollback;
	return
end if

if isnull(ll_prog_ingresso) then
	ll_prog_ingresso = 1
else
	ll_prog_ingresso++
end if

INSERT INTO prod_bolle_in_com  
		( cod_azienda,   
		  anno_commessa,   
		  cod_prodotto_fase, 
		  cod_versione,
		  num_commessa,   
		  prog_riga,   
		  cod_reparto,   
		  cod_lavorazione,   
		  prog_ingresso,   
		  anno_acc_materiali,   
		  num_acc_materiali,   
		  anno_bolla_acq,   
		  num_bolla_acq,   
		  progressivo,   
		  prog_riga_bolla_acq,   
		  cod_prodotto_in,   
		  cod_deposito,   
		  cod_ubicazione,   
		  cod_lotto,   
		  data_stock,   
		  prog_stock,   
		  quan_arrivata,   
		  cod_fornitore,   
		  data_ingresso )  
VALUES ( :s_cs_xx.cod_azienda,   
		  :li_anno_commessa,   
		  :ls_cod_prodotto_fase,
		  :ls_cod_versione_fase,
		  :ll_num_commessa,   
		  :ll_prog_riga,   
		  :ls_cod_reparto,   
		  :ls_cod_lavorazione,   
		  :ll_prog_ingresso,   
		  null,   
		  null,   
		  null,   
		  null,   
		  null,   
		  null,   
		  :ls_cod_prodotto_fase,   
		  :ls_cod_deposito_sl[1],   
		  :ls_cod_ubicazione[1],   
		  :ls_cod_lotto[1],   
		  :ld_data_stock[1],   
		  :ll_prog_stock[1],   
		  :ldd_quan_arrivata,   
		  :ls_cod_fornitore_ingresso,   
		  :ldt_oggi )  ;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
	rollback;
	return
end if
	

//FINE INSERIMENTO NUOVO ARRIVO


//**********************
//INIZIO SCARICO MAGAZZINO FORNITORE

setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ld_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
ls_cod_fornitore[1] = ls_cod_fornitore_ingresso

ll_righe_scarico = dw_elenco_prod_scarico_fornitore.rowcount()

select cod_deposito
into   :ls_cod_deposito_fornitore[1]
from   anag_fornitori
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_fornitore=:ls_cod_fornitore_ingresso;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
   rollback;
	return
end if


for ll_t = 1 to ll_righe_scarico
	dw_scelta_stock_fornitore.setrow(ll_t)
	ls_cod_prodotto_scarico = dw_elenco_prod_scarico_fornitore.getitemstring(ll_t,"cod_prodotto")
	ls_cod_ubicazione[1]  = dw_elenco_prod_scarico_fornitore.getitemstring(ll_t,"cod_ubicazione")
	ls_cod_lotto[1] = dw_elenco_prod_scarico_fornitore.getitemstring(ll_t,"cod_lotto")
	ld_data_stock[1] = dw_elenco_prod_scarico_fornitore.getitemdatetime(ll_t,"data_stock")
	ll_prog_stock[1] = dw_elenco_prod_scarico_fornitore.getitemnumber(ll_t,"prog_stock")
	ldd_quan_prelevata = 	dw_elenco_prod_scarico_fornitore.getitemnumber(ll_t,"quan_prelevata")
	
	if ldd_quan_prelevata > 0 then
		if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_scarico_terzista, ls_cod_prodotto_scarico, ls_cod_deposito_fornitore[], & 
									  ls_cod_ubicazione[], ls_cod_lotto[], ld_data_stock[], & 
									  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
									  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
		
			g_mb.messagebox("Sep", "Si è verificato un errore in creazione movimenti.", stopsign!)
			rollback;
			return 
		
		end if
		
		if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
											ls_cod_tipo_mov_scarico_terzista, ls_cod_prodotto_scarico) = -1 then
		
			 g_mb.messagebox("Sep", "Si è verificato un errore in fase di verifica destinazioni movimenti magazzino." , stopsign!)
		  rollback;
		  return
		
		end if
		
		luo_mag = create uo_magazzino
		
		li_risposta = luo_mag.uof_movimenti_mag( datetime(today(),time('00:00:00')), &
												 ls_cod_tipo_mov_scarico_terzista, &
												 "N", &
												 ls_cod_prodotto_scarico, &
												 ldd_quan_prelevata, &
												 1, &
												 ll_num_commessa, &
												 datetime(today(),time('00:00:00')), &
												 "", &
												 ll_anno_reg_des_mov, &
												 ll_num_reg_des_mov, &
												 ls_cod_deposito_fornitore[], &
												 ls_cod_ubicazione[], &
												 ls_cod_lotto[], &
												 ld_data_stock[], &
												 ll_prog_stock[], &
												 ls_cod_fornitore[], &
												 ls_cod_cliente[], &
												 ll_anno_registrazione[], &
												 ll_num_registrazione[])
												 
		destroy luo_mag
		
		if li_risposta=-1 then
			g_mb.messagebox("Sep", "Errore su movimenti magazzino." , stopsign!)
			rollback;
			return
		end if
			
		li_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)
		
		if li_risposta = -1 then
			g_mb.messagebox("Sep", "Si è verificato un errore in fase di cancellazione destinazioni movimenti." , stopsign!)
			rollback;
			return -1
		end if
		
		//***************
		//INIZIO DISIMPEGNO PRODOTTO SUL DEPOSITO DEL FORNITORE
						
		select quan_impegnata
		into   :ldd_quan_impegnata
		from   impegni_deposito_fasi_com
		where  cod_azienda = :s_cs_xx.cod_azienda   	and    
				 anno_commessa = :li_anno_commessa		and    
				 num_commessa = :ll_num_commessa			and    
				 prog_riga = :ll_prog_riga					and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase and
				 cod_versione = :ls_cod_versione_fase  and    
				 cod_reparto = :ls_cod_reparto			and    
				 cod_lavorazione = :ls_cod_lavorazione and    
				 cod_deposito = :ls_cod_deposito_fornitore[1] and    
				 cod_prodotto_spedito = :ls_cod_prodotto_scarico;	
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return -1
		end if
		
		if ldd_quan_impegnata > ldd_quan_prelevata then
			update impegni_deposito_fasi_com
			set    quan_impegnata = quan_impegnata - :ldd_quan_prelevata
			where  cod_azienda = :s_cs_xx.cod_azienda    and    
					 anno_commessa = :li_anno_commessa		and    
					 num_commessa = :ll_num_commessa			and    
					 prog_riga = :ll_prog_riga					and    
					 cod_prodotto_fase = :ls_cod_prodotto_fase and    
					 cod_versione = :ls_cod_versione_fase  and
					 cod_reparto = :ls_cod_reparto			and    
					 cod_lavorazione = :ls_cod_lavorazione	and    
					 cod_deposito = :ls_cod_deposito_fornitore[1] and    
					 cod_prodotto_spedito = :ls_cod_prodotto_scarico;	
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
				rollback;
				return -1
			end if

		else
			delete impegni_deposito_fasi_com
			where  cod_azienda = :s_cs_xx.cod_azienda		and    
					 anno_commessa = :li_anno_commessa		and    
					 num_commessa = :ll_num_commessa			and    
					 prog_riga = :ll_prog_riga					and    
					 cod_prodotto_fase = :ls_cod_prodotto_fase and
					 cod_versione = :ls_cod_versione_fase  and    
					 cod_reparto = :ls_cod_reparto			and    
					 cod_lavorazione = :ls_cod_lavorazione and    
					 cod_deposito = :ls_cod_deposito_fornitore[1]  and    
					 cod_prodotto_spedito = :ls_cod_prodotto_scarico;	
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
				rollback;
				return -1
			end if
			
		end if
		
		//FINE DISIMPEGNO PRODOTTO SUL DEPOSITO DEL FORNITORE
		//***************
		
		//****************************************************************************************************************
		// DISIMPEGNO LE MP CONTROLLANDO CONTEMPORANEAMENTE ANCHE LA TABELLA IMPEGNO_MAT_PRIME_COMMESSA

		select quan_impegnata_attuale
		into   :ldd_quan_impegnata_attuale
		from   impegno_mat_prime_commessa
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    cod_prodotto=:ls_cod_prodotto_scarico;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then // ROUTINE PER LE COMMESSE GENERATE PRIMA DI AGGIUNGERE LA TAB IMPEGNO_MAT_PRIME COMMESSA
		
			select quan_impegnata
			into   :ldd_quan_impegnata_attuale
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
		
			if ldd_quan_impegnata_attuale >= ldd_quan_prelevata then
				update anag_prodotti																		
				set 	 quan_impegnata = quan_impegnata - :ldd_quan_prelevata
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					rollback;
					return -1
				end if
				
			else
				update anag_prodotti																		
				set 	 quan_impegnata = 0
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					rollback;			
					return -1
				end if
				
			end if
			
		else // ROUTINE PER LE COMMESSE GENERATE DOPO L'AGGIUNTA DELLA TAB IMPEGNO_MAT_PRIME COMMESSA
	
			if ldd_quan_impegnata_attuale >= ldd_quan_prelevata then
				
				update impegno_mat_prime_commessa
				set 	 quan_impegnata_attuale= quan_impegnata_attuale - :ldd_quan_prelevata
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_commessa=:li_anno_commessa
				and    num_commessa=:ll_num_commessa
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					rollback;
					return -1
				end if
		
				update anag_prodotti																		
				set 	 quan_impegnata = quan_impegnata - :ldd_quan_prelevata
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					rollback;					
					return -1
				end if
				
			else
				update impegno_mat_prime_commessa
				set 	 quan_impegnata_attuale = 0
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_commessa=:li_anno_commessa
				and    num_commessa=:ll_num_commessa
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					rollback;					
					return -1
				end if
		
				update anag_prodotti																		
				set 	 quan_impegnata = quan_impegnata - :ldd_quan_impegnata_attuale
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
					rollback;					
					return -1
				end if
						
			
			end if
	
		end if
		//****************************************************************************************************************

	end if
next

//FINE SCARICO MAGAZZINO FORNITORE
//********************************


select quan_in_produzione
into   :ldd_quan_in_produzione		
from   avan_produzione_com
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
		 anno_commessa = :li_anno_commessa  AND    
		 num_commessa = :ll_num_commessa  	AND    
		 prog_riga = :ll_prog_riga 			AND    
		 cod_prodotto = :ls_cod_prodotto_fase  AND
		 cod_versione = :ls_cod_versione_fase  AND    
		 cod_reparto = :ls_cod_reparto  		AND    
		 cod_lavorazione = :ls_cod_lavorazione;

//*****************************
//INIZIO CHIUSURA FASE

if ldd_quan_in_produzione = 0 then

	update avan_produzione_com
	set    flag_fine_fase='S'
	where  cod_azienda = :s_cs_xx.cod_azienda   and 	 
			 anno_commessa = :li_anno_commessa	  and    
			 num_commessa = :ll_num_commessa		  and    
			 prog_riga = :ll_prog_riga				  and    
			 cod_prodotto = :ls_cod_prodotto_fase and
			 cod_versione = :ls_cod_versione_fase and    
			 cod_reparto = :ls_cod_reparto		  and    
			 cod_lavorazione = :ls_cod_lavorazione;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
		rollback;
		return
	end if

	
	//**** Individua se viene chiusa l'ultima fase
	
	select count(*)
	into   :ll_num_fasi_aperte
	from   avan_produzione_com
	where  cod_azienda=:s_cs_xx.cod_azienda
	and 	 anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa
	and    prog_riga=:ll_prog_riga
	and    flag_fine_fase='N';
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
		rollback;
		return
	end if


	if ll_num_fasi_aperte = 0 then
		select quan_in_produzione
		into   :ldd_quan_in_produzione_prog
		from   det_anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
			rollback;
			return
		end if

		
		select cod_prodotto,
				 flag_tipo_avanzamento
		into   :ls_cod_prodotto,
				 :ls_flag_tipo_avanzamento
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
 			rollback;
			return
		end if

		
		s_cs_xx.parametri.parametro_i_1 = li_anno_commessa
		s_cs_xx.parametri.parametro_ul_1 = ll_num_commessa
		s_cs_xx.parametri.parametro_ul_2 = ll_prog_riga
		s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
		s_cs_xx.parametri.parametro_s_5 = ls_cod_versione
	
		window_open(w_rileva_quantita,0)
		
		if s_cs_xx.parametri.parametro_b_1 = true then	
			g_mb.messagebox("Sep", "Operazione annullata", stopsign!)
		   rollback;
			return 
		end if
		
		update det_anag_commesse
		set    quan_in_produzione=0,
				 quan_prodotta=:s_cs_xx.parametri.parametro_d_1
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
		   rollback;
			return
		end if

	
		select quan_prodotta,
				 quan_in_produzione
		into   :ldd_quan_prodotta,
				 :ldd_quan_in_produzione
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
		   rollback;
			return
		end if
		
		ldd_quan_in_produzione = ldd_quan_in_produzione - s_cs_xx.parametri.parametro_d_1
		if ldd_quan_in_produzione < 0 then ldd_quan_in_produzione = 0
		ldd_quan_prodotta = ldd_quan_prodotta + s_cs_xx.parametri.parametro_d_1
	
		choose case ls_flag_tipo_avanzamento //seleziona il tipo avanzamento
			case '1' // Assegnazione
				ls_flag_tipo_avanzamento = '9' // Assegnazione Chiusa
			case '2' // Attivazione
				ls_flag_tipo_avanzamento = '8' // Attivazione Chiusa
		end choose 
	
		update anag_commesse
		set    quan_in_produzione=:ldd_quan_in_produzione,	
				 quan_prodotta=:ldd_quan_prodotta,
				 flag_tipo_avanzamento = :ls_flag_tipo_avanzamento
		where  cod_azienda=:s_cs_xx.cod_azienda
		and 	 anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep", "Errore sul Database : " + sqlca.sqlerrtext, stopsign!)
		   rollback;
			return
		end if
		
		li_risposta = f_mov_mag_chiusura_fasi (li_anno_commessa,ll_num_commessa, & 
															ll_prog_riga,ls_errore )
	
		if li_risposta = -1 then 
			g_mb.messagebox("Sep", ls_errore + sqlca.sqlerrtext, stopsign!)
		   rollback;
			return
		end if
		
//		if isvalid(w_avanz_prod_com_attivazione) then
//			w_avanz_prod_com_attivazione.dw_avanzamento_produzione_commesse_lista.change_dw_current()
//			w_avanz_prod_com_attivazione.triggerevent("pc_retrieve")
//		end if
			
		close(parent)
	end if

end if
//FINE CHIUSURA FASE
//******************************

//if isvalid(w_avanz_prod_com_attivazione) then
//	w_avanz_prod_com_attivazione.dw_avanzamento_produzione_commesse_lista.change_dw_current()
//	w_avanz_prod_com_attivazione.triggerevent("pc_retrieve")
//end if

g_mb.messagebox("Sep","Carico eseguito con successo",information!)
end event

type cbx_bolla_ingresso from checkbox within w_prod_bolle_in_com
integer x = 2583
integer y = 1520
integer width = 297
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Bolla In."
boolean lefttext = true
end type

type st_prodotto_ingresso from statictext within w_prod_bolle_in_com
integer x = 503
integer y = 1400
integer width = 2354
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type em_quan_arrivata from editmask within w_prod_bolle_in_com
integer x = 1143
integer y = 1500
integer width = 503
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "##,###,###.0000"
string displaydata = ""
end type

event losefocus;	string ls_errore,ls_cod_versione
	double ldd_quan_in_produzione
	integer li_risposta
	
   ldd_quan_in_produzione = double (em_quan_arrivata.text)
	
	select cod_versione
	into   :ls_cod_versione
	from   anag_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:il_anno_commessa
	and    num_commessa=:il_num_commessa;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 
	end if

	dw_lista_prodotti_scarico.reset()
	dw_scelta_stock_fornitore.reset()
	li_risposta = f_genera_lista_prodotti(il_anno_commessa,il_num_commessa,il_prog_riga,is_cod_prodotto_fase,ls_cod_versione,ldd_quan_in_produzione,1,ls_errore)
	
	if li_risposta <0 then
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		return
	end if

	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	dw_lista_prodotti_scarico.Reset_DW_Modified(c_ResetChildren)
	
	dw_lista_prodotti_scarico.change_dw_current()
	parent.triggerevent("pc_retrieve")
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	wf_imposta_stock()
	dw_scelta_stock_fornitore.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()

end event

type em_data_ingresso from editmask within w_prod_bolle_in_com
integer x = 366
integer y = 1500
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

type st_1 from statictext within w_prod_bolle_in_com
integer y = 1500
integer width = 343
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Data Arrivo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_prod_bolle_in_com
integer x = 754
integer y = 1500
integer width = 366
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Qtà.Arrivata:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cbx_acc_mat from checkbox within w_prod_bolle_in_com
integer x = 2080
integer y = 1520
integer width = 457
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Acc. Materiale"
boolean lefttext = true
end type

type dw_scelta_stock_fornitore from uo_cs_xx_dw within w_prod_bolle_in_com
integer x = 91
integer y = 700
integer width = 3131
integer height = 440
integer taborder = 20
string dataobject = "d_scelta_stock_fornitore"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type cb_aggiungi from commandbutton within w_prod_bolle_in_com
integer x = 91
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi"
end type

event clicked;string   ls_cod_prodotto_scarico,ls_cod_ubicazione,ls_cod_lotto,ls_cod_deposito
double   ldd_quan_prelevata,ldd_quan_disponibile
long     ll_prog_stock,ll_num_righe,ll_t
datetime ldt_data_stock

for ll_t = 1 to dw_scelta_stock_fornitore.rowcount()
	dw_scelta_stock_fornitore.setrow(ll_t)
	ls_cod_prodotto_scarico = dw_scelta_stock_fornitore.getitemstring(ll_t,"cod_prodotto")
	ls_cod_deposito = dw_scelta_stock_fornitore.getitemstring(ll_t,"cod_deposito")
	ls_cod_ubicazione = dw_scelta_stock_fornitore.getitemstring(ll_t,"cod_ubicazione")
	ls_cod_lotto = dw_scelta_stock_fornitore.getitemstring(ll_t,"cod_lotto")
	ldt_data_stock = dw_scelta_stock_fornitore.getitemdatetime(ll_t,"data_stock")
	ll_prog_stock = dw_scelta_stock_fornitore.getitemnumber(ll_t,"prog_stock")
	dw_scelta_stock_fornitore.setcolumn("quan_prelevata")	
	ldd_quan_prelevata =	double(dw_scelta_stock_fornitore.gettext())
	ldd_quan_disponibile = 	dw_scelta_stock_fornitore.getitemnumber(ll_t,"quan_disponibile")
	
	if ldd_quan_prelevata > 0 then
		ll_num_righe=dw_elenco_prod_scarico_fornitore.rowcount()
	
		if ll_num_righe=0 then
			ll_num_righe=1
		else
			ll_num_righe++
		end if
	
		dw_elenco_prod_scarico_fornitore.insertrow(ll_num_righe)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"cod_prodotto",ls_cod_prodotto_scarico)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"cod_deposito",ls_cod_deposito)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"cod_ubicazione",ls_cod_ubicazione)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"cod_lotto",ls_cod_lotto)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"data_stock",ldt_data_stock)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"prog_stock",ll_prog_stock)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"quan_disponibile",ldd_quan_disponibile)
		dw_elenco_prod_scarico_fornitore.setitem(ll_num_righe,"quan_prelevata",ldd_quan_prelevata)
//		dw_scelta_stock_fornitore.deleterow(ll_t)
		dw_scelta_stock_fornitore.resetupdate()
		
	end if

next

dw_elenco_prod_scarico_fornitore.Reset_DW_Modified(c_ResetChildren)
dw_elenco_prod_scarico_fornitore.settaborder("quan_prelevata",10)

dw_elenco_prod_scarico_fornitore.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cb_togli from commandbutton within w_prod_bolle_in_com
integer x = 91
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Togli"
end type

event clicked;dw_elenco_prod_scarico_fornitore.deleterow(dw_elenco_prod_scarico_fornitore.getrow())
end event

type dw_det_prod_bolle_in_com from uo_cs_xx_dw within w_prod_bolle_in_com
integer x = 91
integer y = 700
integer width = 3109
integer height = 540
integer taborder = 140
string dataobject = "d_det_prod_bolle_in_com"
borderstyle borderstyle = styleraised!
end type

type st_3 from statictext within w_prod_bolle_in_com
integer x = 23
integer y = 1400
integer width = 471
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Prodotto Arrivato:"
boolean focusrectangle = false
end type

type st_stquan_impegnata from statictext within w_prod_bolle_in_com
integer x = 480
integer y = 1180
integer width = 535
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Quantità impegnata:"
boolean focusrectangle = false
end type

type st_quan_impegnata from statictext within w_prod_bolle_in_com
integer x = 1029
integer y = 1180
integer width = 503
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 79741120
boolean enabled = false
string text = "00"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_folder from u_folder within w_prod_bolle_in_com
integer x = 23
integer y = 20
integer width = 3269
integer height = 1260
integer taborder = 80
end type

type dw_lista_prod_bolle_in_com from uo_cs_xx_dw within w_prod_bolle_in_com
event pcd_retrieve pbm_custom60
integer x = 91
integer y = 120
integer width = 3109
integer height = 540
integer taborder = 100
string dataobject = "d_lista_prod_bolle_in_com"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,il_anno_commessa, & 
						 il_num_commessa,il_prog_riga,is_cod_prodotto_fase, is_cod_versione_fase, & 
						 is_cod_reparto,is_cod_lavorazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_elenco_fasi_esterne_attivazione from uo_cs_xx_dw within w_prod_bolle_in_com
integer x = 91
integer y = 120
integer width = 2011
integer height = 920
integer taborder = 90
string dataobject = "d_elenco_fasi_esterne_attivazione"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event clicked;call super::clicked;if i_extendmode then
	string ls_errore,ls_cod_versione,ls_des_prodotto
	double ldd_quan_in_produzione
	integer li_risposta
	
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	
	dw_elenco_prod_scarico_fornitore.reset()

	il_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "anno_commessa")
	il_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "num_commessa")
	il_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "prog_riga")
	is_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(), "cod_prodotto")
	is_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(), "cod_versione")
	ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")
	is_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
	is_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		

	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:is_cod_prodotto_fase;
	
	st_prodotto_ingresso.text = is_cod_prodotto_fase + " / " + ls_des_prodotto
	
	dw_det_prod_bolle_in_com.reset()
	dw_lista_prodotti_scarico.reset()
	dw_scelta_stock_fornitore.reset()
	
	em_quan_arrivata.text = string(ldd_quan_in_produzione)
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 
	end if

	li_risposta = f_genera_lista_prodotti(il_anno_commessa,il_num_commessa,il_prog_riga, is_cod_prodotto_fase, is_cod_versione_fase, ldd_quan_in_produzione,1,ls_errore)
	
	if li_risposta <0 then
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		return
	end if
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	dw_lista_prodotti_scarico.Reset_DW_Modified(c_ResetChildren)
	
//	dw_lista_prodotti_spedizione.settaborder("quan_spedizione",10)
	dw_lista_prodotti_scarico.change_dw_current()
	parent.triggerevent("pc_retrieve")
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	wf_imposta_stock()
	dw_scelta_stock_fornitore.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	dw_lista_prod_bolle_in_com.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()

//	dw_prod_uscita_bol_spedite.change_dw_current()
//	parent.triggerevent("pc_retrieve")
//
//	wf_imposta_stock()
//	dw_scelta_stock.Change_DW_Current( )
//	parent.triggerevent("pc_retrieve")
//	dw_elenco_prod_uscita_bol.Change_DW_Current( )
//	parent.triggerevent("pc_retrieve")
//	dw_elenco_fasi_esterne_attivazione.resetupdate()
//	dw_elenco_prod_uscita_bol.resetupdate()
//	dw_lista_prodotti_spedizione.resetupdate()
//	dw_prod_uscita_bol_spedite.resetupdate()
//	dw_scelta_fornitore.resetupdate()
//	dw_scelta_stock.resetupdate()
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa,ll_prog_riga)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_lista_prodotti_scarico from uo_cs_xx_dw within w_prod_bolle_in_com
integer x = 91
integer y = 120
integer width = 2354
integer height = 540
integer taborder = 30
string dataobject = "d_lista_prodotti_scarico"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event clicked;call super::clicked;if i_extendmode then
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()
	
	
	
	
	wf_imposta_stock()
	
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_det_prod_bolle_in_com.resetupdate()
	dw_lista_prod_bolle_in_com.resetupdate()
	dw_lista_prodotti_scarico.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock_fornitore.resetupdate()

end if
end event

type dw_elenco_prod_scarico_fornitore from uo_cs_xx_dw within w_prod_bolle_in_com
integer x = 91
integer y = 160
integer width = 3131
integer height = 980
integer taborder = 10
string dataobject = "d_elenco_prod_scarico_fornitore"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

