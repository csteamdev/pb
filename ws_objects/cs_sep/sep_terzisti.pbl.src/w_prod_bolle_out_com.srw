﻿$PBExportHeader$w_prod_bolle_out_com.srw
$PBExportComments$Window prod_bolle_out_com
forward
global type w_prod_bolle_out_com from w_cs_xx_principale
end type
type cb_sblocca_stock from commandbutton within w_prod_bolle_out_com
end type
type cb_conferma_stock from commandbutton within w_prod_bolle_out_com
end type
type st_4 from statictext within w_prod_bolle_out_com
end type
type cb_aggiungi_sl from commandbutton within w_prod_bolle_out_com
end type
type em_spedizione from statictext within w_prod_bolle_out_com
end type
type cb_bolla_uscita from commandbutton within w_prod_bolle_out_com
end type
type dw_scelta_fornitore from uo_cs_xx_dw within w_prod_bolle_out_com
end type
type cbx_conferma_bolla from checkbox within w_prod_bolle_out_com
end type
type cbx_bolla_unica from checkbox within w_prod_bolle_out_com
end type
type st_stato_nostro from statictext within w_prod_bolle_out_com
end type
type st_stato_terzista from statictext within w_prod_bolle_out_com
end type
type cb_elimina from commandbutton within w_prod_bolle_out_com
end type
type cbx_tutta from checkbox within w_prod_bolle_out_com
end type
type cbx_aggiungi from checkbox within w_prod_bolle_out_com
end type
type em_anno_bolla from editmask within w_prod_bolle_out_com
end type
type st_1 from statictext within w_prod_bolle_out_com
end type
type em_num_registrazione from editmask within w_prod_bolle_out_com
end type
type st_2 from statictext within w_prod_bolle_out_com
end type
type mle_nota_testata from multilineedit within w_prod_bolle_out_com
end type
type mle_nota_piede from multilineedit within w_prod_bolle_out_com
end type
type st_nota_testata from statictext within w_prod_bolle_out_com
end type
type st_nota_piede from statictext within w_prod_bolle_out_com
end type
type sle_riga_riferimento from singlelineedit within w_prod_bolle_out_com
end type
type st_riga_riferimento from statictext within w_prod_bolle_out_com
end type
type st_quan_impegnata from statictext within w_prod_bolle_out_com
end type
type dw_scelta_stock from uo_cs_xx_dw within w_prod_bolle_out_com
end type
type dw_prod_uscita_bol_spedite from uo_cs_xx_dw within w_prod_bolle_out_com
end type
type dw_folder from u_folder within w_prod_bolle_out_com
end type
type dw_stato_stock_terzista from uo_cs_xx_dw within w_prod_bolle_out_com
end type
type cb_carica_note from commandbutton within w_prod_bolle_out_com
end type
type st_stquan_impegnata from statictext within w_prod_bolle_out_com
end type
type dw_elenco_fasi_esterne_attivazione from uo_cs_xx_dw within w_prod_bolle_out_com
end type
type dw_elenco_prod_uscita_bol from uo_cs_xx_dw within w_prod_bolle_out_com
end type
type dw_lista_prodotti_spedizione from uo_cs_xx_dw within w_prod_bolle_out_com
end type
end forward

global type w_prod_bolle_out_com from w_cs_xx_principale
integer width = 3415
integer height = 1664
string title = "Bolle CTL Uscita"
cb_sblocca_stock cb_sblocca_stock
cb_conferma_stock cb_conferma_stock
st_4 st_4
cb_aggiungi_sl cb_aggiungi_sl
em_spedizione em_spedizione
cb_bolla_uscita cb_bolla_uscita
dw_scelta_fornitore dw_scelta_fornitore
cbx_conferma_bolla cbx_conferma_bolla
cbx_bolla_unica cbx_bolla_unica
st_stato_nostro st_stato_nostro
st_stato_terzista st_stato_terzista
cb_elimina cb_elimina
cbx_tutta cbx_tutta
cbx_aggiungi cbx_aggiungi
em_anno_bolla em_anno_bolla
st_1 st_1
em_num_registrazione em_num_registrazione
st_2 st_2
mle_nota_testata mle_nota_testata
mle_nota_piede mle_nota_piede
st_nota_testata st_nota_testata
st_nota_piede st_nota_piede
sle_riga_riferimento sle_riga_riferimento
st_riga_riferimento st_riga_riferimento
st_quan_impegnata st_quan_impegnata
dw_scelta_stock dw_scelta_stock
dw_prod_uscita_bol_spedite dw_prod_uscita_bol_spedite
dw_folder dw_folder
dw_stato_stock_terzista dw_stato_stock_terzista
cb_carica_note cb_carica_note
st_stquan_impegnata st_stquan_impegnata
dw_elenco_fasi_esterne_attivazione dw_elenco_fasi_esterne_attivazione
dw_elenco_prod_uscita_bol dw_elenco_prod_uscita_bol
dw_lista_prodotti_spedizione dw_lista_prodotti_spedizione
end type
global w_prod_bolle_out_com w_prod_bolle_out_com

type variables
long il_anno_commessa,il_num_commessa,il_prog_riga
string is_cod_prodotto_fase,is_cod_reparto,is_cod_lavorazione
boolean ib_ok_note
end variables

forward prototypes
public function integer wf_imposta_prodotti_spedizione (long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga, string fs_cod_prodotto, string fs_cod_versione, double fd_quan_in_produzione, long fl_num_livello_cor, ref string fs_errore)
public function integer wf_imposta_stock ()
end prototypes

public function integer wf_imposta_prodotti_spedizione (long fl_anno_commessa, long fl_num_commessa, long fl_prog_riga, string fs_cod_prodotto, string fs_cod_versione, double fd_quan_in_produzione, long fl_num_livello_cor, ref string fs_errore);string    ls_cod_prodotto_figlio, ls_flag_esterna,ls_test, ls_cod_prodotto_ap, ls_cod_prodotto_fase, ls_cod_versione_inserito, ls_cod_versione_variante, & 		   
		    ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, ls_cod_prodotto_inserito, ls_mp, ls_cod_versione_ap, & 
			 ls_cod_lavorazione,ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante, ls_cod_versione_fase, ls_cod_versione_figlio
long      ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_handle, ll_prog_spedizione, & 
			 ll_num_righe_dw,ll_i,li_anno_commessa,ll_num_commessa,ll_prog_riga
integer   li_num_priorita,li_risposta
double    ldd_quan_utilizzo,ldd_quan_utilizzo_variante,ldd_quan_prodotta,ldd_quan_in_produzione,&
			 ldd_quan_utilizzata,ldd_quan_utilizzo_1
boolean   lb_flag_trovata 
datastore lds_righe_distinta

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ll_prog_spedizione = long(em_spedizione.text)

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve( s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione)
	
for ll_num_figli = 1 to ll_num_righe
	
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fd_quan_in_produzione
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	

	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio
	
	select cod_prodotto,
			 quan_utilizzo,
			 flag_materia_prima,
			 cod_versione
	into   :ls_cod_prodotto_variante,	
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante,
			 :ls_cod_versione_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			 anno_commessa = :fl_anno_commessa  	and    
			 num_commessa = :fl_num_commessa			and    
			 cod_prodotto_padre = :fs_cod_prodotto and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
			 cod_versione = :fs_cod_versione  		and
			 cod_versione_figlio = :ls_cod_versione_figlio;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_cod_versione_inserito = ls_cod_versione_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante* fd_quan_in_produzione
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	//*************************************
	//ricerca se già inserito
	//*************************************

	lb_flag_trovata = false
	for ll_i = 1 to dw_lista_prodotti_spedizione.rowcount()
		dw_lista_prodotti_spedizione.setrow(ll_i)		
		if ls_cod_prodotto_inserito = dw_lista_prodotti_spedizione.getitemstring(ll_i,"cod_prodotto")  and &
			ls_cod_versione_inserito = dw_lista_prodotti_spedizione.getitemstring(ll_i,"cod_versione") then 
			
			lb_flag_trovata=true
			ldd_quan_utilizzo_1 = dw_lista_prodotti_spedizione.getitemnumber(ll_i,"quan_necessaria")
			ldd_quan_utilizzo = ldd_quan_utilizzo + ldd_quan_utilizzo_1 
			dw_lista_prodotti_spedizione.setitem(ll_i,"quan_necessaria",ldd_quan_utilizzo)
			
		end if
	next
	
	if lb_flag_trovata then 
		ldd_quan_utilizzo=0
		continue
	end if
	
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda  and 	 
			 cod_prodotto_padre = :ls_cod_prodotto_inserito and
	       cod_versione = :ls_cod_versione_inserito;
	
	if sqlca.sqlcode=100 then 
		ls_mp = "S"
		ldd_quan_prodotta = 0
		ldd_quan_in_produzione = ldd_quan_utilizzo
		ldd_quan_utilizzata = 0
		
	else
		select quan_prodotta,
				 quan_in_produzione,
				 quan_utilizzata
		into   :ldd_quan_prodotta,
				 :ldd_quan_in_produzione,
				 :ldd_quan_utilizzata
		from   avan_produzione_com
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
				 anno_commessa = :fl_anno_commessa  	and    
				 num_commessa = :fl_num_commessa			and    
				 prog_riga = :fl_prog_riga					and    
				 cod_prodotto = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
	
		if sqlca.sqlcode < 0 then
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		ls_mp ="N"
		
		
	end if
	
	ll_num_righe_dw = dw_lista_prodotti_spedizione.rowcount()

	if ll_num_righe_dw=0 then
		ll_num_righe_dw=1
	else
		ll_num_righe_dw++
	end if

	if ls_mp = "S" then
		
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 cod_prodotto = :ls_cod_prodotto_inserito;
	
		if sqlca.sqlcode < 0 then
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if

		select cod_azienda
		into   :ls_test
		from   prod_bolle_out_com
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
				 anno_commessa = :li_anno_commessa		and    
				 num_commessa = :ll_num_commessa			and    
				 prog_riga = :ll_prog_riga					and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase	and  
				 cod_versione = :ls_cod_versione_fase  and
				 cod_reparto = :ls_cod_reparto		and    
				 cod_lavorazione = :ls_cod_lavorazione		and    
				 prog_spedizione = :ll_prog_spedizione 	and    
				 cod_prodotto_spedito = :ls_cod_prodotto_inserito;

		if sqlca.sqlcode = 100 then
			dw_lista_prodotti_spedizione.insertrow(ll_num_righe_dw)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"cod_prodotto",ls_cod_prodotto_inserito)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"cod_versione",ls_cod_versione_inserito)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"des_prodotto",ls_des_prodotto)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"quan_disponibile",ldd_quan_prodotta - ldd_quan_utilizzata)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"quan_necessaria",ldd_quan_utilizzo)
			dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"flag_mat_prima",ls_mp)
		end if
		
	end if
	
	if ldd_quan_prodotta >= ldd_quan_in_produzione then continue
	
	if ls_flag_materia_prima = 'N' then
		
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and 	 
				 cod_prodotto_padre = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
		
		if sqlca.sqlcode = 100 then continue

	else
		continue
	end if

	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
			 cod_prodotto_padre = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		continue
	else
		li_risposta=wf_imposta_prodotti_spedizione( fl_anno_commessa, &
																  fl_num_commessa, &
																  fl_prog_riga, &
																  ls_cod_prodotto_inserito, &
																  ls_cod_versione_inserito, &
																  ldd_quan_utilizzo, &
																  fl_num_livello_cor + 1, &
																  ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

declare righe_ap_com cursor for
select  cod_prodotto,
		  cod_versione,
		  quan_prodotta,
		  quan_utilizzata
from    avan_produzione_com
where   cod_azienda = :s_cs_xx.cod_azienda and     
		  anno_commessa = :fl_anno_commessa  and     
		  num_commessa = :fl_num_commessa    and     
		  prog_riga = :fl_prog_riga          and     
		  quan_prodotta - quan_utilizzata > 0 ;

if sqlca.sqlcode < 0 then
	ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

open righe_ap_com;

if sqlca.sqlcode < 0 then
	ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

do while 1=1
	fetch righe_ap_com 	into  :ls_cod_prodotto_ap,
										:ls_cod_versione_ap,
										:ldd_quan_prodotta,
										:ldd_quan_utilizzata;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close righe_ap_com;
		return -1
	end if

	lb_flag_trovata = false

	for ll_i = 1 to dw_lista_prodotti_spedizione.rowcount()
		dw_lista_prodotti_spedizione.setrow(ll_i)		
		if ls_cod_prodotto_ap = dw_lista_prodotti_spedizione.getitemstring(ll_i,"cod_prodotto") then lb_flag_trovata=true
	next

	if lb_flag_trovata then continue
	
	ll_num_righe_dw=dw_lista_prodotti_spedizione.rowcount()

	if ll_num_righe_dw=0 then
		ll_num_righe_dw=1
	else
		ll_num_righe_dw++
	end if

	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto_ap;

	if sqlca.sqlcode < 0 then
		ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	select cod_azienda
	into   :ls_test
	from   prod_bolle_out_com
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			 anno_commessa = :li_anno_commessa	and    
			 num_commessa = :ll_num_commessa	and    
			 prog_riga = :ll_prog_riga	and    
			 cod_prodotto_fase = :ls_cod_prodotto_fase	and    
			 cod_versione = :ls_cod_versione_fase  and
			 cod_reparto = :ls_cod_reparto	and    
			 cod_lavorazione = :ls_cod_lavorazione	and    
			 prog_spedizione = :ll_prog_spedizione	and    
			 cod_prodotto_spedito = :ls_cod_prodotto_ap;

	if sqlca.sqlcode = 100 then
		dw_lista_prodotti_spedizione.insertrow(ll_num_righe_dw)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"cod_prodotto",ls_cod_prodotto_ap)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"cod_versione",ls_cod_versione_ap)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"des_prodotto",ls_des_prodotto)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"quan_disponibile",ldd_quan_prodotta - ldd_quan_utilizzata)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"quan_necessaria",0)
		dw_lista_prodotti_spedizione.setitem(ll_num_righe_dw,"flag_mat_prima","N")
	end if
	
loop

close righe_ap_com;

dw_lista_prodotti_spedizione.resetupdate()
return 0
end function

public function integer wf_imposta_stock ();string   ls_cod_prodotto_mp,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, ls_test, ls_cod_versione_fase, & 
			ls_cod_tipo_commessa,ls_flag_mat_prima,ls_modify,ls_cod_reparto, & 
			ls_cod_lavorazione,ls_cod_prodotto_fase
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_orari, & 
			ll_num_fasi_aperte,ll_prog_spedizione
datetime ldt_data_stock
double   ldd_quan_disponibile,ldd_quan_necessaria,ldd_quan_prelevata
integer  li_anno_commessa

dw_scelta_stock.reset()
dw_scelta_stock.Reset_DW_Modified(c_ResetChildren)
if dw_lista_prodotti_spedizione.rowcount() = 0 then return 0

ls_cod_prodotto_mp = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"cod_prodotto")
ldd_quan_necessaria = dw_lista_prodotti_spedizione.getitemnumber(dw_lista_prodotti_spedizione.getrow(),"quan_necessaria")
ls_flag_mat_prima = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"flag_mat_prima")

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")	
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")	
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ll_prog_spedizione = long(em_spedizione.text)

select cod_azienda
into   :ls_test
from   prod_bolle_out_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 prog_riga = :ll_prog_riga     and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and  
		 cod_versione = :ls_cod_versione_fase and
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 prog_spedizione = :ll_prog_spedizione and    
		 cod_prodotto_spedito = :ls_cod_prodotto_mp;
	
if (ls_flag_mat_prima="S")  and sqlca.sqlcode = 100 then

	select cod_tipo_commessa
	into   :ls_cod_tipo_commessa
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :li_anno_commessa 	and    
			 num_commessa = :ll_num_commessa;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		 
		return 0
	end if
	
	select cod_deposito_prelievo
	into   :ls_cod_deposito
	from   tab_tipi_commessa
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
	       cod_tipo_commessa = :ls_cod_tipo_commessa;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		 
		return 0
	end if

	declare righe_stock cursor for
	SELECT  cod_ubicazione,   
			  cod_lotto,   
			  data_stock,   
			  prog_stock,
			  giacenza_stock - quan_assegnata - quan_in_spedizione  
	 FROM   stock   
	 where  cod_azienda=:s_cs_xx.cod_azienda
	 and    cod_prodotto =:ls_cod_prodotto_mp
	 and    cod_deposito =:ls_cod_deposito
	 and    giacenza_stock - quan_assegnata - quan_in_spedizione >0
	 ORDER BY data_stock asc;
 
	 open righe_stock;
	
	 if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		 
		return 0
	 end if

	do while 1 = 1
		fetch righe_stock 
		into  :ls_cod_ubicazione,
				:ls_cod_lotto,   
				:ldt_data_stock,   
				:ll_prog_stock,
				:ldd_quan_disponibile;

		choose case sqlca.sqlcode  
			case 100
				exit
			case is < 0 
				g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext, stopsign!)
				
					
				exit
		end choose

		ll_num_righe=dw_scelta_stock.rowcount()
	
		if ll_num_righe=0 then
			ll_num_righe=1
		else
			ll_num_righe++
		end if
	
		
		dw_scelta_stock.insertrow(ll_num_righe)
		dw_scelta_stock.setitem(ll_num_righe,"cod_prodotto",ls_cod_prodotto_mp)
		dw_scelta_stock.setitem(ll_num_righe,"cod_deposito",ls_cod_deposito)
		dw_scelta_stock.setitem(ll_num_righe,"cod_ubicazione",ls_cod_ubicazione)
		dw_scelta_stock.setitem(ll_num_righe,"cod_lotto",ls_cod_lotto)
		dw_scelta_stock.setitem(ll_num_righe,"data_stock",ldt_data_stock)
		dw_scelta_stock.setitem(ll_num_righe,"prog_stock",ll_prog_stock)
		dw_scelta_stock.setitem(ll_num_righe,"quan_disponibile",ldd_quan_disponibile)
		
		if ldd_quan_necessaria > 0 then
			if ldd_quan_disponibile >= ldd_quan_necessaria then
				ldd_quan_prelevata = ldd_quan_necessaria
				ldd_quan_necessaria = 0 
			else
				ldd_quan_prelevata = ldd_quan_disponibile
				ldd_quan_necessaria = ldd_quan_necessaria - ldd_quan_prelevata
			end if
		else
			ldd_quan_prelevata = 0
		end if
		
		dw_scelta_stock.setitem(ll_num_righe,"quan_prelevata",ldd_quan_prelevata)

	loop
	
	close righe_stock;
	
	dw_scelta_stock.Reset_DW_Modified(c_ResetChildren)
	dw_scelta_stock.settaborder("quan_prelevata",10)
	cb_conferma_stock.enabled = true
	dw_scelta_stock.enabled = true
	dw_elenco_prod_uscita_bol.resetupdate()
	dw_lista_prodotti_spedizione.resetupdate()
	dw_scelta_stock.resetupdate()
	return 0
	
else
	dw_scelta_stock.Reset_DW_Modified(c_ResetChildren)
	cb_conferma_stock.enabled = false
	dw_scelta_stock.enabled = false
	dw_elenco_prod_uscita_bol.resetupdate()
	dw_lista_prodotti_spedizione.resetupdate()
	dw_scelta_stock.resetupdate()
	return 0
	
end if

end function

on w_prod_bolle_out_com.create
int iCurrent
call super::create
this.cb_sblocca_stock=create cb_sblocca_stock
this.cb_conferma_stock=create cb_conferma_stock
this.st_4=create st_4
this.cb_aggiungi_sl=create cb_aggiungi_sl
this.em_spedizione=create em_spedizione
this.cb_bolla_uscita=create cb_bolla_uscita
this.dw_scelta_fornitore=create dw_scelta_fornitore
this.cbx_conferma_bolla=create cbx_conferma_bolla
this.cbx_bolla_unica=create cbx_bolla_unica
this.st_stato_nostro=create st_stato_nostro
this.st_stato_terzista=create st_stato_terzista
this.cb_elimina=create cb_elimina
this.cbx_tutta=create cbx_tutta
this.cbx_aggiungi=create cbx_aggiungi
this.em_anno_bolla=create em_anno_bolla
this.st_1=create st_1
this.em_num_registrazione=create em_num_registrazione
this.st_2=create st_2
this.mle_nota_testata=create mle_nota_testata
this.mle_nota_piede=create mle_nota_piede
this.st_nota_testata=create st_nota_testata
this.st_nota_piede=create st_nota_piede
this.sle_riga_riferimento=create sle_riga_riferimento
this.st_riga_riferimento=create st_riga_riferimento
this.st_quan_impegnata=create st_quan_impegnata
this.dw_scelta_stock=create dw_scelta_stock
this.dw_prod_uscita_bol_spedite=create dw_prod_uscita_bol_spedite
this.dw_folder=create dw_folder
this.dw_stato_stock_terzista=create dw_stato_stock_terzista
this.cb_carica_note=create cb_carica_note
this.st_stquan_impegnata=create st_stquan_impegnata
this.dw_elenco_fasi_esterne_attivazione=create dw_elenco_fasi_esterne_attivazione
this.dw_elenco_prod_uscita_bol=create dw_elenco_prod_uscita_bol
this.dw_lista_prodotti_spedizione=create dw_lista_prodotti_spedizione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_sblocca_stock
this.Control[iCurrent+2]=this.cb_conferma_stock
this.Control[iCurrent+3]=this.st_4
this.Control[iCurrent+4]=this.cb_aggiungi_sl
this.Control[iCurrent+5]=this.em_spedizione
this.Control[iCurrent+6]=this.cb_bolla_uscita
this.Control[iCurrent+7]=this.dw_scelta_fornitore
this.Control[iCurrent+8]=this.cbx_conferma_bolla
this.Control[iCurrent+9]=this.cbx_bolla_unica
this.Control[iCurrent+10]=this.st_stato_nostro
this.Control[iCurrent+11]=this.st_stato_terzista
this.Control[iCurrent+12]=this.cb_elimina
this.Control[iCurrent+13]=this.cbx_tutta
this.Control[iCurrent+14]=this.cbx_aggiungi
this.Control[iCurrent+15]=this.em_anno_bolla
this.Control[iCurrent+16]=this.st_1
this.Control[iCurrent+17]=this.em_num_registrazione
this.Control[iCurrent+18]=this.st_2
this.Control[iCurrent+19]=this.mle_nota_testata
this.Control[iCurrent+20]=this.mle_nota_piede
this.Control[iCurrent+21]=this.st_nota_testata
this.Control[iCurrent+22]=this.st_nota_piede
this.Control[iCurrent+23]=this.sle_riga_riferimento
this.Control[iCurrent+24]=this.st_riga_riferimento
this.Control[iCurrent+25]=this.st_quan_impegnata
this.Control[iCurrent+26]=this.dw_scelta_stock
this.Control[iCurrent+27]=this.dw_prod_uscita_bol_spedite
this.Control[iCurrent+28]=this.dw_folder
this.Control[iCurrent+29]=this.dw_stato_stock_terzista
this.Control[iCurrent+30]=this.cb_carica_note
this.Control[iCurrent+31]=this.st_stquan_impegnata
this.Control[iCurrent+32]=this.dw_elenco_fasi_esterne_attivazione
this.Control[iCurrent+33]=this.dw_elenco_prod_uscita_bol
this.Control[iCurrent+34]=this.dw_lista_prodotti_spedizione
end on

on w_prod_bolle_out_com.destroy
call super::destroy
destroy(this.cb_sblocca_stock)
destroy(this.cb_conferma_stock)
destroy(this.st_4)
destroy(this.cb_aggiungi_sl)
destroy(this.em_spedizione)
destroy(this.cb_bolla_uscita)
destroy(this.dw_scelta_fornitore)
destroy(this.cbx_conferma_bolla)
destroy(this.cbx_bolla_unica)
destroy(this.st_stato_nostro)
destroy(this.st_stato_terzista)
destroy(this.cb_elimina)
destroy(this.cbx_tutta)
destroy(this.cbx_aggiungi)
destroy(this.em_anno_bolla)
destroy(this.st_1)
destroy(this.em_num_registrazione)
destroy(this.st_2)
destroy(this.mle_nota_testata)
destroy(this.mle_nota_piede)
destroy(this.st_nota_testata)
destroy(this.st_nota_piede)
destroy(this.sle_riga_riferimento)
destroy(this.st_riga_riferimento)
destroy(this.st_quan_impegnata)
destroy(this.dw_scelta_stock)
destroy(this.dw_prod_uscita_bol_spedite)
destroy(this.dw_folder)
destroy(this.dw_stato_stock_terzista)
destroy(this.cb_carica_note)
destroy(this.st_stquan_impegnata)
destroy(this.dw_elenco_fasi_esterne_attivazione)
destroy(this.dw_elenco_prod_uscita_bol)
destroy(this.dw_lista_prodotti_spedizione)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ], l_1_objects[ ],l_2_objects[ ]

set_w_options(c_noenablepopup)

l_objects[1] = dw_elenco_fasi_esterne_attivazione
dw_folder.fu_AssignTab(1, "&Fasi Esterne", l_Objects[])
l_1_objects[1] = dw_lista_prodotti_spedizione
l_1_objects[2] = cb_aggiungi_sl
dw_folder.fu_AssignTab(2, "&Prodotti da spedire", l_1_Objects[])
l_2_objects[1] = dw_scelta_stock
l_2_objects[2] = cb_conferma_stock
l_2_objects[3] = dw_stato_stock_terzista
l_2_objects[4] = st_stato_nostro
l_2_objects[5] = st_stato_terzista
l_2_objects[6] = st_stquan_impegnata
l_2_objects[7] = st_quan_impegnata
dw_folder.fu_AssignTab(3, "&Scelta Stock", l_2_objects[ ])
l_1_objects[1] = dw_elenco_prod_uscita_bol
l_1_objects[2] = cb_sblocca_stock
dw_folder.fu_AssignTab(4, "Prodot&ti in Spedizione", l_1_Objects[])
l_objects[1] = dw_prod_uscita_bol_spedite
l_objects[2] = cb_elimina
l_objects[3] = cbx_tutta
dw_folder.fu_AssignTab(5, "Spedizioni &Effettuate", l_Objects[])
l_objects[1] = st_riga_riferimento
l_objects[2] = sle_riga_riferimento
l_objects[3] = st_nota_testata
l_objects[4] = mle_nota_testata
l_objects[5] = st_nota_piede
l_objects[6] = mle_nota_piede
l_objects[7] = cb_carica_note
dw_folder.fu_AssignTab(6, "&Note e Riferimenti", l_Objects[])

dw_folder.fu_FolderCreate(6,6)

dw_elenco_fasi_esterne_attivazione.set_dw_options(sqlca, i_openparm,c_nonew + & 
												  				  c_nodelete + c_nomodify +  & 
												  				  c_disablecc,c_disableccinsert)

dw_lista_prodotti_spedizione.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)

dw_scelta_stock.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)
													
dw_elenco_prod_uscita_bol.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_default)	
													
dw_prod_uscita_bol_spedite.set_dw_options(sqlca, &
														pcca.null_object, &
														c_nonew + &
														c_nomodify + &
														c_nodelete + &
														c_disablecc + &
														c_noretrieveonopen + &
														c_disableccinsert , &
														c_default)	

													

dw_scelta_fornitore.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_newonopen + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodewhite)

dw_stato_stock_terzista.set_dw_options(sqlca,pcca.null_object,c_nonew + & 
												  	c_nodelete + c_nomodify +  & 
												  	c_disablecc + c_noretrieveonopen,c_disableccinsert)

dw_folder.fu_SelectTab(1)

save_on_close(c_socnosave)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_scelta_fornitore, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_terzista = 'S' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end event

event closequery;call super::closequery;delete prod_bolle_out_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :il_anno_commessa  and    
		 num_commessa = :il_num_commessa    and    
		 prog_riga = :il_prog_riga          and    
		 anno_registrazione is null         and    
		 num_registrazione is null          and    
		 prog_riga_bol_ven is null;



if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	 
	rollback;
	
else
	commit;
	
end if

end event

type cb_sblocca_stock from commandbutton within w_prod_bolle_out_com
integer x = 2834
integer y = 1220
integer width = 457
integer height = 80
integer taborder = 150
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Togli"
end type

event clicked;string   ls_cod_prodotto_spedito,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, ls_errore,& 
			ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione,ls_cod_prodotto_fase, &
			ls_cod_versione,ls_cod_fornitore, ls_cod_versione_fase
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, & 
			ll_prog_prodotto_spedito,ll_t,ll_prog_prod_spedito
datetime ldt_data_stock
double   ldd_quan_spedita,ldd_quan_in_produzione,ldd_quan_impegnata_ultima,ldd_quan_impegnata
integer  li_anno_commessa,li_risposta

if dw_elenco_prod_uscita_bol.rowcount() = 0 then return

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

ls_cod_fornitore = dw_scelta_fornitore.gettext()

select cod_deposito
into   :ls_cod_deposito
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_fornitore=:ls_cod_fornitore;

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")
ll_prog_spedizione = dw_elenco_prod_uscita_bol.getitemnumber(dw_elenco_prod_uscita_bol.getrow(),"prog_spedizione")
ll_prog_prod_spedito = dw_elenco_prod_uscita_bol.getitemnumber(dw_elenco_prod_uscita_bol.getrow(),"prog_prod_spedito")
ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")
ls_cod_prodotto_spedito = dw_elenco_prod_uscita_bol.getitemstring(dw_elenco_prod_uscita_bol.getrow(),"cod_prodotto_spedito")

select quan_impegnata,
		 quan_impegnata_ultima
into   :ldd_quan_impegnata,
       :ldd_quan_impegnata_ultima
from   impegni_deposito_fasi_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 prog_riga = :ll_prog_riga          and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and
		 cod_versione = :ls_cod_versione_fase and    
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 cod_deposito = :ls_cod_deposito and    
		 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

if ldd_quan_impegnata = ldd_quan_impegnata_ultima then
	
	delete impegni_deposito_fasi_com
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			 anno_commessa = :li_anno_commessa 		and    
			 num_commessa = :ll_num_commessa 		and    
			 prog_riga = :ll_prog_riga 				and    
			 cod_prodotto_fase = :ls_cod_prodotto_fase 	and   
			 cod_versione = :ls_cod_versione_fase  and
			 cod_reparto = :ls_cod_reparto 			and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 cod_deposito = :ls_cod_deposito 		and    
			 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if 
	
else
	update impegni_deposito_fasi_com
	set    quan_impegnata=quan_impegnata - quan_impegnata_ultima,
			 quan_impegnata_ultima=0
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			 anno_commessa = :li_anno_commessa 		and    
			 num_commessa = :ll_num_commessa 		and    
			 prog_riga = :ll_prog_riga 				and    
			 cod_prodotto_fase = :ls_cod_prodotto_fase and
			 cod_versione = :ls_cod_versione_fase  and
			 cod_reparto = :ls_cod_reparto 			and    
			 cod_lavorazione = :ls_cod_lavorazione and    
			 cod_deposito = :ls_cod_deposito 	and    
			 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if 

end if

delete prod_bolle_out_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa		and    
		 prog_riga = :ll_prog_riga 			and    
		 cod_reparto = :ls_cod_reparto 		and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 prog_spedizione = :ll_prog_spedizione and    
		 prog_prod_spedito = :ll_prog_prod_spedito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

commit;

dw_lista_prodotti_spedizione.reset()


// *** Michela 06/10/06: la versione è collegata al prodotto della fase
//select cod_versione
//into   :ls_cod_versione
//from   anag_commesse
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    anno_commessa=:li_anno_commessa
//and    num_commessa=:ll_num_commessa;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//	return 
//end if

li_risposta = wf_imposta_prodotti_spedizione( li_anno_commessa, &
															 ll_num_commessa, &
															 ll_prog_riga, &
															 ls_cod_prodotto_fase, &
															 ls_cod_versione_fase, &
															 ldd_quan_in_produzione, &
															 1, &
															 ls_errore)

if li_risposta <0 then
	g_mb.messagebox("Sep",ls_errore,stopsign!)
	return
end if

wf_imposta_stock()
dw_elenco_prod_uscita_bol.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
dw_stato_stock_terzista.change_dw_current()
parent.triggerevent("pc_retrieve")

dw_scelta_stock.change_dw_current()
parent.triggerevent("pc_retrieve")

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
//dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

end event

type cb_conferma_stock from commandbutton within w_prod_bolle_out_com
integer x = 2354
integer y = 1220
integer width = 457
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi M.P."
end type

event clicked;string   ls_cod_prodotto_spedito,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, ls_cod_deposito_fornitore, &
			ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_cod_fornitore, & 
			ls_cod_prodotto_fase,ls_cod_versione,ls_errore, ls_cod_versione_fase, ls_cod_versione_spedito
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, & 
			ll_prog_prodotto_spedito,ll_t
datetime ldt_data_stock
double   ldd_quan_spedita,ldd_quan_in_produzione,ldd_quan_disponibile,ldd_quan_impegnata,ldd_quan_necessaria, & 
			ldd_nuova_quan_impegnata,ldd_somma_quan_spedita
integer  li_anno_commessa,li_risposta

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

ls_cod_fornitore = dw_scelta_fornitore.gettext()

if isnull (ls_cod_fornitore) then
	g_mb.messagebox("Sep","Selezionare un fornitore!",stopsign!)
	return
end if

select cod_deposito
into   :ls_cod_deposito_fornitore
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 cod_fornitore = :ls_cod_fornitore;

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 
end if

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")

ldd_quan_necessaria = dw_lista_prodotti_spedizione.getitemnumber(dw_lista_prodotti_spedizione.getrow(),"quan_necessaria")
ls_cod_prodotto_spedito = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"cod_prodotto")
ls_cod_versione_spedito = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"cod_versione")

select sum(quan_impegnata)
into   :ldd_quan_impegnata
from   impegni_deposito_fasi_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa  and    
		 num_commessa = :ll_num_commessa    and    
		 prog_riga = :ll_prog_riga          and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and
		 cod_versione = :ls_cod_versione_fase and    
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 
end if

if isnull(ldd_quan_impegnata) then ldd_quan_impegnata = 0

ldd_nuova_quan_impegnata = ldd_quan_necessaria - ldd_quan_impegnata

update impegni_deposito_fasi_com
set    quan_impegnata_ultima = 0
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa and    
		 prog_riga = :ll_prog_riga and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and    
		 cod_versione = :ls_cod_versione_fase and
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 cod_deposito = :ls_cod_deposito_fornitore and    
		 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 
end if

ll_prog_spedizione = long (em_spedizione.text)

select max(prog_prod_spedito)
into   :ll_prog_prodotto_spedito
from   prod_bolle_out_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa  and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and
		 cod_versione = :ls_cod_versione_fase and    
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 prog_spedizione = :ll_prog_spedizione;

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 
end if

if isnull(ll_prog_prodotto_spedito) then
	ll_prog_prodotto_spedito=0
end if

ll_num_righe = dw_scelta_stock.rowcount()

for ll_t=1 to ll_num_righe
	ll_prog_prodotto_spedito++	
	dw_scelta_stock.setrow(ll_t)
	ls_cod_deposito = dw_scelta_stock.getitemstring(ll_t,"cod_deposito")		
	ls_cod_ubicazione = dw_scelta_stock.getitemstring(ll_t,"cod_ubicazione")
	ls_cod_lotto = dw_scelta_stock.getitemstring(ll_t,"cod_lotto")
	ldt_data_stock = dw_scelta_stock.getitemdatetime(ll_t,"data_stock")
	ll_prog_stock = dw_scelta_stock.getitemnumber(ll_t,"prog_stock")
	ls_cod_prodotto_spedito=dw_scelta_stock.getitemstring(ll_t,"cod_prodotto")
   ldd_quan_disponibile = dw_scelta_stock.getitemnumber(ll_t,"quan_disponibile")
   dw_scelta_stock.setcolumn("quan_prelevata")	
	ldd_quan_spedita = double(dw_scelta_stock.gettext())

	if  ldd_quan_disponibile < ldd_quan_spedita then
		g_mb.messagebox("Sep","Attenzione! La quantita da spedire supera la quantità disponibile nello stock. Reinserire la quantita da spedire.",stopsign!)
		return
	end if
	
	if ldd_quan_spedita > 0 then
		INSERT INTO prod_bolle_out_com
				( cod_azienda,   
				  anno_commessa,   
				  num_commessa,   
				  cod_prodotto_fase, 
				  cod_versione,
				  prog_riga,  
				  cod_reparto,
				  cod_lavorazione,
				  prog_spedizione,
				  prog_prod_spedito,
				  anno_registrazione,
				  num_registrazione,
				  prog_riga_bol_ven,
				  cod_prodotto_spedito,
				  cod_deposito,   
				  cod_ubicazione,   
				  cod_lotto,   
				  data_stock,   
				  prog_stock,   				  
				  quan_spedita,
				  flag_mp)  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :li_anno_commessa,   
				  :ll_num_commessa,   
				  :ls_cod_prodotto_fase, 
				  :ls_cod_versione_fase,
				  :ll_prog_riga,   
				  :ls_cod_reparto,
				  :ls_cod_lavorazione,
				  :ll_prog_spedizione,
				  :ll_prog_prodotto_spedito,
				  null,
				  null,
				  null,
				  :ls_cod_prodotto_spedito,
				  :ls_cod_deposito,   
				  :ls_cod_ubicazione,   
				  :ls_cod_lotto,   
				  :ldt_data_stock,   
				  :ll_prog_stock, 
				  :ldd_quan_spedita,
				  'S');
				  
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
		ldd_somma_quan_spedita = ldd_somma_quan_spedita + ldd_quan_spedita
		
	end if
	
next

if ldd_somma_quan_spedita <= ldd_nuova_quan_impegnata then ldd_nuova_quan_impegnata = ldd_somma_quan_spedita

select cod_azienda
into   :ls_test
from   impegni_deposito_fasi_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa and    
		 prog_riga = :ll_prog_riga  and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and
		 cod_versione = :ls_cod_versione_fase and    
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 cod_deposito = :ls_cod_deposito_fornitore and    
		 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	return
end if

if sqlca.sqlcode = 100 or isnull(ls_test) or ls_test ="" then
	insert into impegni_deposito_fasi_com
	(cod_azienda,
	 anno_commessa,
	 num_commessa,
	 prog_riga,
	 cod_prodotto_fase,
	 cod_versione,
	 cod_reparto,
	 cod_lavorazione,
	 cod_deposito,
	 cod_prodotto_spedito,
	 quan_impegnata,
	 quan_impegnata_ultima)
	values
	(:s_cs_xx.cod_azienda,
	 :li_anno_commessa,
	 :ll_num_commessa,
	 :ll_prog_riga,
	 :ls_cod_prodotto_fase,
	 :ls_cod_versione_fase,
	 :ls_cod_reparto,
	 :ls_cod_lavorazione,
	 :ls_cod_deposito_fornitore,
	 :ls_cod_prodotto_spedito,
	 :ldd_nuova_quan_impegnata,
	 :ldd_nuova_quan_impegnata);

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		return
	end if

else

	if sqlca.sqlcode = 0 then
		
		update impegni_deposito_fasi_com
		set    quan_impegnata = quan_impegnata + :ldd_nuova_quan_impegnata,
				 quan_impegnata_ultima =:ldd_nuova_quan_impegnata
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
				 anno_commessa = :li_anno_commessa 		and    
				 num_commessa = :ll_num_commessa 		and    
				 prog_riga = :ll_prog_riga 		and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase and
				 cod_vesione = :ls_cod_versione_fase 		and    
				 cod_reparto = :ls_cod_reparto 		and    
				 cod_lavorazione = :ls_cod_lavorazione 		and    
				 cod_deposito = :ls_cod_deposito_fornitore 	and    
				 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
	end if		
	
end if


commit;

dw_lista_prodotti_spedizione.reset()


// Michela 06/10/06: la versione è collegata al prodotto, non al prodotto principale
//select cod_versione
//into   :ls_cod_versione
//from   anag_commesse
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    anno_commessa=:li_anno_commessa
//and    num_commessa=:ll_num_commessa;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//	return 
//end if

li_risposta = wf_imposta_prodotti_spedizione( li_anno_commessa, &
															 ll_num_commessa, &
															 ll_prog_riga, &
															 ls_cod_prodotto_fase, &
															 ls_cod_versione_fase, &
															 ldd_quan_in_produzione, &
															 1, &
															 ls_errore)

if li_risposta <0 then
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		return
end if

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()
wf_imposta_stock()
dw_scelta_stock.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
dw_elenco_prod_uscita_bol.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()


end event

type st_4 from statictext within w_prod_bolle_out_com
integer x = 2400
integer y = 1360
integer width = 594
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Nr.Nuova Spedizione:"
alignment alignment = center!
long bordercolor = 16777215
boolean focusrectangle = false
end type

type cb_aggiungi_sl from commandbutton within w_prod_bolle_out_com
integer x = 1874
integer y = 1220
integer width = 457
integer height = 80
integer taborder = 160
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi &SL"
end type

event clicked;string   ls_cod_prodotto_spedito,ls_cod_deposito,ls_cod_ubicazione, ls_cod_lotto, & 
			ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione,ls_cod_prodotto_fase,&
			ls_flag_mp,ls_cod_versione,ls_errore,ls_cod_versione_fase, ls_cod_versione_spedito
			
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, & 
			ll_prog_prodotto_spedito,ll_t,ll_anno_reg_sl,ll_num_reg_sl
datetime ldt_data_stock
double   ldd_quan_spedita,ldd_quan_in_produzione,ldd_quan_disponibile
integer  li_anno_commessa,li_risposta

dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()

dw_lista_prodotti_spedizione.setredraw( false)

dw_scelta_stock.resetupdate()

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")

ll_prog_spedizione = long (em_spedizione.text)

select max(prog_prod_spedito)
into   :ll_prog_prodotto_spedito
from   prod_bolle_out_com
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa and    
		 cod_prodotto_fase = :ls_cod_prodotto_fase and   
		 cod_versione = :ls_cod_versione_fase and
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione and    
		 prog_spedizione = :ll_prog_spedizione;

if sqlca.sqlcode< 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 
end if

if isnull(ll_prog_prodotto_spedito) then
	ll_prog_prodotto_spedito = 0
end if

ll_num_righe = dw_lista_prodotti_spedizione.rowcount()

for ll_t = 1 to ll_num_righe
	
	dw_lista_prodotti_spedizione.setrow(ll_t)
	ls_flag_mp = dw_lista_prodotti_spedizione.getitemstring(ll_t,"flag_mat_prima")
	ldd_quan_disponibile = dw_lista_prodotti_spedizione.getitemnumber(ll_t,"quan_disponibile")
	dw_lista_prodotti_spedizione.setcolumn("quan_spedizione")	
	ldd_quan_spedita = double(dw_lista_prodotti_spedizione.gettext())

	if ldd_quan_spedita > ldd_quan_disponibile then
		g_mb.messagebox("Sep","Attenzione la quantita disponibile è inferiore alla quantita da spedire, reinseire la quantita.",stopsign!)
		return
	end if
	
	if ls_flag_mp = 'N' and ldd_quan_spedita > 0 then
		
		ll_prog_prodotto_spedito++			
		ls_cod_prodotto_spedito = dw_lista_prodotti_spedizione.getitemstring( ll_t, "cod_prodotto")
		ls_cod_versione_spedito = dw_lista_prodotti_spedizione.getitemstring( ll_t, "cod_versione")
	
		select anno_reg_sl,
				 num_reg_sl
		into   :ll_anno_reg_sl,
				 :ll_num_reg_sl
		from   avan_produzione_com
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 anno_commessa = :li_anno_commessa 	and    
				 num_commessa = :ll_num_commessa		and    
				 prog_riga = :ll_prog_riga				and    
				 cod_prodotto = :ls_cod_prodotto_spedito and
				 cod_versione = :ls_cod_versione_spedito;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return
		end if
 
		select cod_deposito,
				 cod_ubicazione,
				 cod_lotto,
				 data_stock,
				 prog_stock
		into   :ls_cod_deposito,
				 :ls_cod_ubicazione,
				 :ls_cod_lotto,
				 :ldt_data_stock,
				 :ll_prog_stock
		from   mov_magazzino
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 anno_registrazione = :ll_anno_reg_sl and    
				 num_registrazione = :ll_num_reg_sl;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
		INSERT INTO prod_bolle_out_com
						( cod_azienda,   
						  anno_commessa,   
						  num_commessa,   
						  cod_prodotto_fase, 
						  cod_versione,
						  prog_riga,  
						  cod_reparto,
						  cod_lavorazione,
						  prog_spedizione,
						  prog_prod_spedito,
						  anno_registrazione,
						  num_registrazione,
						  prog_riga_bol_ven,
						  cod_prodotto_spedito,
						  cod_deposito,   
						  cod_ubicazione,   
						  cod_lotto,   
						  data_stock,   
						  prog_stock,   				  
						  quan_spedita,
						  flag_mp)  
			  VALUES ( :s_cs_xx.cod_azienda,   
						  :li_anno_commessa,   
						  :ll_num_commessa,   
						  :ls_cod_prodotto_fase, 
						  :ls_cod_versione_fase,
						  :ll_prog_riga,   
						  :ls_cod_reparto,
						  :ls_cod_lavorazione,
						  :ll_prog_spedizione,
						  :ll_prog_prodotto_spedito,
						  null,
						  null,
						  null,
						  :ls_cod_prodotto_spedito,
						  :ls_cod_deposito,   
						  :ls_cod_ubicazione,   
						  :ls_cod_lotto,   
						  :ldt_data_stock,   
						  :ll_prog_stock, 
						  :ldd_quan_spedita,
						  'N');
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
	end if
next

commit;

dw_lista_prodotti_spedizione.reset()


// *** Michela 06/10/06: la versione del prodotto ce l'ho collegata allo stesso
//select cod_versione
//into   :ls_cod_versione
//from   anag_commesse
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    anno_commessa=:li_anno_commessa
//and    num_commessa=:ll_num_commessa;
//
//if sqlca.sqlcode < 0 then
//	messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//	return 
//end if

li_risposta = wf_imposta_prodotti_spedizione( li_anno_commessa, &
															 ll_num_commessa, &
															 ll_prog_riga, &
															 ls_cod_prodotto_fase, &
															 ls_cod_versione_fase, &
															 ldd_quan_in_produzione, &
															 1, &
															 ls_errore)

if li_risposta <0 then
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		return
end if

wf_imposta_stock()
dw_scelta_stock.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
dw_elenco_prod_uscita_bol.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
dw_elenco_fasi_esterne_attivazione.resetupdate()
dw_elenco_prod_uscita_bol.resetupdate()
dw_lista_prodotti_spedizione.resetupdate()
dw_prod_uscita_bol_spedite.resetupdate()
dw_scelta_fornitore.resetupdate()
dw_scelta_stock.resetupdate()
dw_lista_prodotti_spedizione.setredraw(true)

end event

type em_spedizione from statictext within w_prod_bolle_out_com
integer x = 2994
integer y = 1340
integer width = 366
integer height = 100
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_bolla_uscita from commandbutton within w_prod_bolle_out_com
integer x = 2994
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 180
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera Bolla"
end type

event clicked;string   ls_cod_prodotto_spedito,ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_flag_mp, & 
			ls_cod_prodotto_fase,ls_cod_versione,ls_errore,ls_cod_fornitore,ls_cod_mov_magazzino,ls_cod_tipo_det_ven_sl,&
			ls_cod_tipo_det_ven,ls_cod_tipo_bol_ven,ls_conferma,ls_des_prodotto_finito,ls_cod_prodotto_finito,ls_des_estesa_prodotto, ls_cod_versione_fase
			
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, & 
			ll_prog_prodotto_spedito,ll_t,ll_anno_registrazione,ll_num_registrazione, ll_prog_riga_bol_ven[]
			
datetime ldt_data_stock

double   ldd_quan_spedita,ldd_quan_in_produzione

integer  li_anno_commessa,li_risposta

boolean  lb_nuova_bolla

uo_genera_doc_produzione luo_crea_bolle_produzione

if (g_mb.messagebox("Sep","Sei sicuro di generare la spedizione merce?",Exclamation!,yesno!, 2))=2 then return

if cbx_conferma_bolla.checked = true then
	ls_conferma = 	"S"
else
	ls_conferma = 	"N"
end if

luo_crea_bolle_produzione = create uo_genera_doc_produzione

dw_scelta_stock.resetupdate()

li_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")	
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")	
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		
ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")

select cod_tipo_commessa,
		 cod_prodotto
into   :ls_cod_tipo_commessa,
		 :ls_cod_prodotto_finito
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :li_anno_commessa and    
		 num_commessa = :ll_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

select des_estesa_prodotto
into   :ls_des_estesa_prodotto
from   tes_fasi_lavorazione
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 cod_prodotto = :ls_cod_prodotto_fase and    
		 cod_versione = :ls_cod_versione_fase and
		 cod_reparto = :ls_cod_reparto and    
		 cod_lavorazione = :ls_cod_lavorazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

if not isnull(sle_riga_riferimento.text) then luo_crea_bolle_produzione.is_riferimento_riga = sle_riga_riferimento.text
if not isnull(mle_nota_testata.text) then luo_crea_bolle_produzione.is_nota_testata = mle_nota_testata.text
if not isnull(mle_nota_piede.text) then luo_crea_bolle_produzione.is_nota_piede = mle_nota_piede.text
if not isnull(ls_des_estesa_prodotto) then luo_crea_bolle_produzione.is_riferimento_riga_fine = ls_des_estesa_prodotto

ls_cod_fornitore = dw_scelta_fornitore.gettext()

select cod_tipo_bol_ven,
		 cod_tipo_det_ven,
		 cod_tipo_det_ven_sl
into   :ls_cod_tipo_bol_ven,
		 :ls_cod_tipo_det_ven,
		 :ls_cod_tipo_det_ven_sl
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

for ll_t = 1 to dw_elenco_prod_uscita_bol.rowcount()
	dw_prod_uscita_bol_spedite.setrow(ll_t)
	luo_crea_bolle_produzione.is_cod_prodotto[ll_t]=dw_elenco_prod_uscita_bol.getitemstring(ll_t,"cod_prodotto_spedito")
	luo_crea_bolle_produzione.is_cod_deposito[ll_t]=dw_elenco_prod_uscita_bol.getitemstring(ll_t,"cod_deposito")
	luo_crea_bolle_produzione.is_cod_ubicazione[ll_t]=dw_elenco_prod_uscita_bol.getitemstring(ll_t,"cod_ubicazione")
	luo_crea_bolle_produzione.is_cod_lotto[ll_t]=dw_elenco_prod_uscita_bol.getitemstring(ll_t,"cod_lotto")
	luo_crea_bolle_produzione.idt_data_stock[ll_t]=dw_elenco_prod_uscita_bol.getitemdatetime(ll_t,"data_stock")
	luo_crea_bolle_produzione.il_prog_stock[ll_t]=dw_elenco_prod_uscita_bol.getitemnumber(ll_t,"prog_stock")
	luo_crea_bolle_produzione.id_quantita[ll_t]=dw_elenco_prod_uscita_bol.getitemnumber(ll_t,"quan_spedita")		
	ls_flag_mp = dw_elenco_prod_uscita_bol.getitemstring(ll_t,"flag_mp")
	if ls_flag_mp = 'S' then
		luo_crea_bolle_produzione.is_cod_tipo_det_ven[ll_t] = ls_cod_tipo_det_ven
	else
		luo_crea_bolle_produzione.is_cod_tipo_det_ven[ll_t] = ls_cod_tipo_det_ven_sl
	end if
next

// PASSARE ANNO E NUM_REGISTRAZIONE SE SI VUOLE AGGIUNGERE AD UNA BOLLA ESISTENTE

lb_nuova_bolla = true

if cbx_aggiungi.checked = true then
	ll_anno_registrazione = long(em_anno_bolla.text)
	ll_num_registrazione = long(em_num_registrazione.text)
	lb_nuova_bolla = false
end if

li_risposta = luo_crea_bolle_produzione.f_crea_bolla_uscita(ls_cod_fornitore, &
												                       ls_cod_mov_magazzino, & 
																			  ls_cod_tipo_bol_ven, &
																			  ls_conferma, & 
																			  lb_nuova_bolla,&
																			  ll_anno_registrazione, &
																			  ll_num_registrazione, &
																			  ll_prog_riga_bol_ven[])

if li_risposta < 0 then
	g_mb.messagebox("Sep",luo_crea_bolle_produzione.is_messaggio)
	rollback;
	destroy uo_genera_doc_produzione
	return
end if

ll_prog_spedizione = long (em_spedizione.text)

for ll_t = 1 to upperbound(ll_prog_riga_bol_ven[])
	
	update prod_bolle_out_com
	set    anno_registrazione = :ll_anno_registrazione,
			 num_registrazione = :ll_num_registrazione,
			 prog_riga_bol_ven = :ll_prog_riga_bol_ven[ll_t]
	where  cod_azienda = : s_cs_xx.cod_azienda 	and	 
			 anno_commessa = :li_anno_commessa 	and	 
			 num_commessa = :ll_num_commessa 	and	 
			 prog_riga = :ll_prog_riga 	and    
			 cod_prodotto_spedito = :luo_crea_bolle_produzione.is_cod_prodotto[ll_t] 	and    
			 anno_registrazione is null 	and    
			 num_registrazione is null  	and    
			 prog_riga_bol_ven is null;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return
	end if
	
	//***  QUESTO UPDATE SERVIREBBE NEL CASO IL SL VENISSE SCARICATO DAL MAGAZZINO INVECE IL SL RIMANE SEMPRE NEL MAGAZZINO
	//*** NOSTRO POICHE' IL TIPO_DET_VEN INDICATO IN TAB TIPI COMMESSA PER GLI SL NON DEVE ESSERE ASSOCIATO A NESSUN MOVIMENTO
//	update avan_produzione_com
//	set 	 quan_utilizzata=quan_utilizzata + :luo_crea_bolle_produzione.id_quantita[ll_t]
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    anno_commessa=:li_anno_commessa
//	and    num_commessa=:ll_num_commessa
//	and    prog_riga=:ll_prog_riga
//	and    cod_prodotto=:luo_crea_bolle_produzione.is_cod_prodotto[ll_t];
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul db in fase di aggiornamente tabella avan_produzione_com: "+ sqlca.sqlerrtext,stopsign!)
		rollback;
		return
	end if
next


commit;
destroy uo_genera_doc_produzione

dw_prod_uscita_bol_spedite.change_dw_current()
parent.triggerevent("pc_retrieve")

dw_elenco_prod_uscita_bol.reset()
if lb_nuova_bolla then
	g_mb.messagebox("Sep","E' stata creata la bolla di Conto Lavoro, Anno: " + string(ll_anno_registrazione) + " Numero: " + string(ll_num_registrazione),information!)
else
	g_mb.messagebox("Sep","E' stata aggiornata la bolla di Conto Lavoro, Anno: " + string(ll_anno_registrazione) + " Numero: " + string(ll_num_registrazione),information!)	
end if

end event

type dw_scelta_fornitore from uo_cs_xx_dw within w_prod_bolle_out_com
integer x = 23
integer y = 1340
integer width = 2240
integer height = 80
integer taborder = 100
string dataobject = "d_scelta_fornitore_terzista"
boolean border = false
end type

event itemchanged;call super::itemchanged;if dw_lista_prodotti_spedizione.rowcount() > 0 then
	dw_stato_stock_terzista.change_dw_current()
	parent.triggerevent("pc_retrieve")
end if
end event

event getfocus;call super::getfocus;dw_scelta_fornitore.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_scelta_fornitore,"cod_fornitore")
end choose
end event

type cbx_conferma_bolla from checkbox within w_prod_bolle_out_com
integer x = 503
integer y = 1460
integer width = 480
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Conferma Bolla"
boolean lefttext = true
end type

type cbx_bolla_unica from checkbox within w_prod_bolle_out_com
integer x = 23
integer y = 1460
integer width = 425
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Stessa Bolla"
boolean lefttext = true
end type

type st_stato_nostro from statictext within w_prod_bolle_out_com
integer x = 91
integer y = 120
integer width = 731
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Stato del Nostro Magazzino:"
boolean focusrectangle = false
end type

type st_stato_terzista from statictext within w_prod_bolle_out_com
integer x = 91
integer y = 660
integer width = 782
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Stato Magazzino del Terzista:"
boolean focusrectangle = false
end type

type cb_elimina from commandbutton within w_prod_bolle_out_com
integer x = 91
integer y = 1200
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina"
end type

event clicked;string   ls_cod_prodotto_spedito,ls_cod_tipo_commessa,ls_test,ls_cod_reparto,ls_cod_lavorazione, ls_cod_fornitore,& 
			ls_cod_prodotto_fase,ls_flag_movimenti,ls_cod_prodotto_in_spedizione,ls_cod_deposito,ls_cod_prodotto_test, ls_cod_versione_fase
			
long     ll_prog_stock,ll_num_righe,ll_num_commessa,ll_prog_riga,ll_prog_spedizione, ll_i, & 
			ll_prog_prodotto_spedito,ll_t,ll_anno_registrazione,ll_num_registrazione, ll_prog_riga_bol_ven
integer  li_anno_commessa
double   ldd_quan_necessaria,ldd_quan_impegnata,ldd_somma_giacenza,ldd_somma_quan_spedita,ldd_quan_spedita
if (g_mb.messagebox("Sep","Sei sicuro di eliminare la spedizione merce?",Exclamation!,yesno!, 2))=2 then return

if dw_prod_uscita_bol_spedite.rowcount() > 0 then
	
	li_anno_commessa = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"anno_commessa")
	ll_num_commessa = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"num_commessa")
	ll_prog_riga = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"prog_riga")
	ls_cod_prodotto_fase = dw_prod_uscita_bol_spedite.getitemstring(dw_prod_uscita_bol_spedite.getrow(),"cod_prodotto_fase")
	ls_cod_versione_fase = dw_prod_uscita_bol_spedite.getitemstring(dw_prod_uscita_bol_spedite.getrow(),"cod_versione")
	ls_cod_prodotto_spedito = dw_prod_uscita_bol_spedite.getitemstring(dw_prod_uscita_bol_spedite.getrow(),"cod_prodotto_spedito")
	ls_cod_reparto = dw_prod_uscita_bol_spedite.getitemstring(dw_prod_uscita_bol_spedite.getrow(),"cod_reparto")
	ls_cod_lavorazione = dw_prod_uscita_bol_spedite.getitemstring(dw_prod_uscita_bol_spedite.getrow(),"cod_lavorazione")
	ll_prog_spedizione = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"prog_spedizione")
	ll_prog_prodotto_spedito = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"prog_prod_spedito")
	ll_anno_registrazione = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"anno_registrazione")
	ll_num_registrazione = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"num_registrazione")
	ll_prog_riga_bol_ven = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"prog_riga_bol_ven")
	ldd_quan_spedita = dw_prod_uscita_bol_spedite.getitemnumber(dw_prod_uscita_bol_spedite.getrow(),"quan_spedita")
	
	select flag_movimenti
	into   :ls_flag_movimenti
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
	       anno_registrazione = :ll_anno_registrazione 	and    
			 num_registrazione = :ll_num_registrazione;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 
	end if
	
	if ls_flag_movimenti = 'S' then
		g_mb.messagebox("Sep","Attenzione non è più possibile eliminare la spedizione/bolla poichè la bolla è già stata confermata.",stopsign!)
		return
	end if
	
	select cod_fornitore
	into   :ls_cod_fornitore
	from   tes_bol_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 
	end if
	
	select cod_deposito
	into   :ls_cod_deposito
	from   anag_fornitori
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_fornitore=:ls_cod_fornitore;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 
	end if

	
	if cbx_tutta.checked = true then

		//*******	
		//INIZIO DISIMPEGNO DEPOSITI DEI FORNITORI TERZISTI (caso tutta la bolla)
				
		//****************************
		//     ATTENZIONE!!!!!
		//		FATTO IN QUESTO MODO NON VA BENE IN QUANTO IN UNA STESSA BOLLA POTREBBERO ESSERCI SPEDIZIONI DIVERSE PERTANTO
		//    PER ORA SI LASCIA LA POSSIBILITA' ALL'UTENTE DI TOGLIERE UN PRODOTTO ALLA VOLTA DALLA BOLLA ( VEDI SOTTO)
		//    check box cbx_tutta è stata disabilitata: è sempre a false
		//**********************
		
		for ll_i = 1 to dw_lista_prodotti_spedizione.rowcount()
			
			ls_cod_prodotto_in_spedizione = dw_lista_prodotti_spedizione.getitemstring(ll_i,"cod_prodotto")			
			ldd_quan_necessaria = dw_lista_prodotti_spedizione.getitemnumber(ll_i,"quan_necessaria")
			
			select quan_impegnata
			into   :ldd_quan_impegnata
			from   impegni_deposito_fasi_com
			where  cod_azienda = :s_cs_xx.cod_azienda 	and    
					 anno_commessa = :li_anno_commessa 		and    
					 num_commessa = :ll_num_commessa 		and    
					 prog_riga = :ll_prog_riga					and    
					 cod_reparto = :ls_cod_reparto			and    
					 cod_lavorazione = :ls_cod_lavorazione	and    
					 cod_deposito = :ls_cod_deposito 		and    
					 cod_prodotto_fase = :ls_cod_prodotto_fase and   
					 cod_versione = :ls_cod_versione_fase  and
					 cod_prodotto_spedito = :ls_cod_prodotto_in_spedizione;
			
			if sqlca.sqlcode< 0 then
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				rollback;
				return 
			end if
			
			ldd_quan_impegnata = ldd_quan_impegnata - ldd_quan_necessaria
			
			if ldd_quan_impegnata < 0 then ldd_quan_impegnata = 0
			
			update impegni_deposito_fasi_com
			set    quan_impegnata = :ldd_quan_impegnata
			where  cod_azienda = :s_cs_xx.cod_azienda 	and    
			       anno_commessa = :li_anno_commessa  	and    
					 num_commessa = :ll_num_commessa			and    
					 prog_riga = :ll_prog_riga					and    
					 cod_reparto = :ls_cod_reparto			and    
					 cod_lavorazione = :ls_cod_lavorazione	and    
					 cod_deposito = :ls_cod_deposito			and    
					 cod_prodotto_fase = :ls_cod_prodotto_fase and    
					 cod_versione = :ls_cod_versione_fase  and
					 cod_prodotto_spedito = :ls_cod_prodotto_in_spedizione;

			if sqlca.sqlcode< 0 then
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				rollback;
				return 
			end if

		next

				

		//FINE DISIMPEGNO DEPOSITI DEI FORNITORI TERZISTI
		//****************************

		
		delete prod_bolle_out_com
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven_stat
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven_corrispondenze
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete tes_bol_ven_corrispondenze
		where  cod_azienda=:s_cs_xx.cod_azienda

		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete tes_bol_ven_note
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete tes_bol_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
	
	else
		//*******
		//INIZIO DISIMPEGNO DEPOSITI DEI FORNITORI TERZISTI (caso singolo prodotto)		
		
		// PER ORA E' ABILITATO SOLO QUESTO CASO LA CHECK BOX CHE PERMETTEVA DI IMPOSTARE LA ELIMINAZIONE DI 
		// TUTTA LA BOLLA E' STATA DISABILITATA
		
		
//		
//		select sum(giacenza_stock)
//		into   :ldd_somma_giacenza
//		from   stock
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_deposito=:ls_cod_deposito
//		and    cod_prodotto=:ls_cod_prodotto_spedito;
//		
//		if sqlca.sqlcode< 0 then
//			messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//			rollback;
//			return 
//		end if

		select sum(quan_spedita)
		into   :ldd_somma_quan_spedita
		from   prod_bolle_out_com
		where  cod_azienda = :s_cs_xx.cod_azienda 	and    
				 anno_commessa = :li_anno_commessa 		and    
				 num_commessa = :ll_num_commessa			and    
				 prog_riga = :ll_prog_riga 				and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase and    
				 cod_versione = :ls_cod_versione_fase  and
				 cod_reparto = :ls_cod_reparto 			and    
				 cod_lavorazione = :ls_cod_lavorazione and    
				 cod_prodotto_spedito = :ls_cod_prodotto_spedito;

		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if

		// correggo la somma togliendo la quan_spedita con la riga corrente poichè dopo la riga di spedizione sarà eliminata
		ldd_somma_quan_spedita = ldd_somma_quan_spedita - ldd_quan_spedita 

		for ll_i = 1 to dw_lista_prodotti_spedizione.rowcount()
			ls_cod_prodotto_in_spedizione = dw_lista_prodotti_spedizione.getitemstring(ll_i,"cod_prodotto")
			ldd_quan_necessaria = dw_lista_prodotti_spedizione.getitemnumber(ll_i,"quan_necessaria")
			
			if ls_cod_prodotto_spedito = ls_cod_prodotto_in_spedizione then
				
				select quan_impegnata
				into   :ldd_quan_impegnata
				from   impegni_deposito_fasi_com
				where  cod_azienda = :s_cs_xx.cod_azienda and    
						 anno_commessa = :li_anno_commessa 	and    
						 num_commessa = :ll_num_commessa 	and    
						 prog_riga = :ll_prog_riga				and    
						 cod_reparto = :ls_cod_reparto		and    
						 cod_lavorazione = :ls_cod_lavorazione and    
						 cod_deposito = :ls_cod_deposito 		and    
						 cod_prodotto_fase = :ls_cod_prodotto_fase and
						 cod_versione = :ls_cod_versione_fase  and    
						 cod_prodotto_spedito = :ls_cod_prodotto_in_spedizione;
				
				if sqlca.sqlcode< 0 then
					g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					rollback;
					return 
				end if
		
				if ldd_somma_quan_spedita <= ldd_quan_necessaria then //se la somma della quantià spedita risulta minore della quan_necessaria devo diminuire la quan_impegnata
																				   	//altrimenti non fare niente
					ldd_quan_impegnata=ldd_somma_quan_spedita   	
					
					update impegni_deposito_fasi_com
					set    quan_impegnata = :ldd_quan_impegnata
					where  cod_azienda = :s_cs_xx.cod_azienda and    
							 anno_commessa = :li_anno_commessa  and    
							 num_commessa = :ll_num_commessa 	and    
							 prog_riga = :ll_prog_riga 			and    
							 cod_reparto = :ls_cod_reparto 		and    
							 cod_lavorazione = :ls_cod_lavorazione	and    
							 cod_deposito = :ls_cod_deposito			and    
							 cod_prodotto_fase = :ls_cod_prodotto_fase and
							 cod_versione = :ls_cod_versione_fase 		 and    
							 cod_prodotto_spedito = :ls_cod_prodotto_in_spedizione;
		
					if sqlca.sqlcode< 0 then
						g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
						rollback;
						return 
					end if
					
				end if
				
			end if
		next

		//FINE DISIMPEGNO DEPOSITI DEI FORNITORI TERZISTI
		//****************************
				
		delete prod_bolle_out_com
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 anno_commessa = :li_anno_commessa 		and    
				 num_commessa = :ll_num_commessa 		and    
				 prog_riga = :ll_prog_riga 		and    
				 cod_prodotto_fase = :ls_cod_prodotto_fase and
				 cod_versione = :ls_cod_versione_fase 		 and    
				 cod_lavorazione = :ls_cod_lavorazione		and    
				 cod_reparto = :ls_cod_reparto		and    
				 prog_spedizione = :ll_prog_spedizione 		and    
				 prog_prod_spedito = :ll_prog_prodotto_spedito 	and    
				 anno_registrazione = :ll_anno_registrazione 	and    
				 num_registrazione = :ll_num_registrazione;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven_stat
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    prog_riga_bol_ven=:ll_prog_riga_bol_ven;
		
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven_corrispondenze
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    prog_riga_bol_ven=:ll_prog_riga_bol_ven;
	
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
		delete det_bol_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:ll_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    prog_riga_bol_ven=:ll_prog_riga_bol_ven;
		
		if sqlca.sqlcode< 0 then
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return 
		end if
		
	end if
	commit;
	dw_prod_uscita_bol_spedite.change_dw_current()
	parent.triggerevent("pc_retrieve")
	g_mb.messagebox("Sep","Eliminazione spedizione/bolla avvenuta con successo",information!)
end if
end event

type cbx_tutta from checkbox within w_prod_bolle_out_com
integer x = 480
integer y = 1200
integer width = 434
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Tutta la Bolla"
boolean lefttext = true
end type

type cbx_aggiungi from checkbox within w_prod_bolle_out_com
integer x = 1097
integer y = 1460
integer width = 750
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Aggiungi a Bolla Esistente"
boolean lefttext = true
end type

event clicked;if cbx_aggiungi.checked = true then
	em_anno_bolla.enabled=true
	em_num_registrazione.enabled = true
	em_anno_bolla.text = string(f_anno_esercizio())
else
	em_anno_bolla.enabled=false
	em_num_registrazione.enabled = false
end if
	
end event

type em_anno_bolla from editmask within w_prod_bolle_out_com
integer x = 2034
integer y = 1460
integer width = 251
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
boolean spin = true
string displaydata = "˜"
end type

type st_1 from statictext within w_prod_bolle_out_com
integer x = 1851
integer y = 1460
integer width = 183
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Anno:"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_num_registrazione from editmask within w_prod_bolle_out_com
integer x = 2583
integer y = 1460
integer width = 343
integer height = 80
integer taborder = 170
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "######"
string displaydata = "À"
end type

type st_2 from statictext within w_prod_bolle_out_com
integer x = 2309
integer y = 1460
integer width = 274
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Num.Reg.:"
alignment alignment = center!
boolean focusrectangle = false
end type

type mle_nota_testata from multilineedit within w_prod_bolle_out_com
integer x = 869
integer y = 340
integer width = 2423
integer height = 420
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type mle_nota_piede from multilineedit within w_prod_bolle_out_com
integer x = 869
integer y = 780
integer width = 2423
integer height = 420
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

type st_nota_testata from statictext within w_prod_bolle_out_com
integer x = 457
integer y = 340
integer width = 416
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nota di Testata:"
boolean focusrectangle = false
end type

type st_nota_piede from statictext within w_prod_bolle_out_com
integer x = 571
integer y = 780
integer width = 297
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nota Piede:"
boolean focusrectangle = false
end type

type sle_riga_riferimento from singlelineedit within w_prod_bolle_out_com
integer x = 869
integer y = 200
integer width = 2423
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

type st_riga_riferimento from statictext within w_prod_bolle_out_com
integer x = 416
integer y = 200
integer width = 457
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga Riferimento:"
boolean focusrectangle = false
end type

type st_quan_impegnata from statictext within w_prod_bolle_out_com
integer x = 640
integer y = 1220
integer width = 526
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "00"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_scelta_stock from uo_cs_xx_dw within w_prod_bolle_out_com
integer x = 91
integer y = 180
integer width = 3177
integer height = 480
integer taborder = 70
string dataobject = "d_scelta_stock_2"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type dw_prod_uscita_bol_spedite from uo_cs_xx_dw within w_prod_bolle_out_com
integer x = 91
integer y = 120
integer width = 3177
integer height = 1060
integer taborder = 120
string dataobject = "d_prod_uscita_bol_spedite"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga,ll_prog_spedizione
string ls_cod_prodotto_fase,ls_cod_reparto,ls_cod_lavorazione, ls_cod_versione_fase

ll_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		

l_Error = Retrieve( s_cs_xx.cod_azienda, &
						  ll_anno_commessa, &
						  ll_num_commessa, &
						  ll_prog_riga, &
						  ls_cod_prodotto_fase, & 
						  ls_cod_versione_fase, &
						  ls_cod_reparto, &
						  ls_cod_lavorazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_folder from u_folder within w_prod_bolle_out_com
integer x = 23
integer y = 20
integer width = 3337
integer height = 1300
integer taborder = 130
end type

type dw_stato_stock_terzista from uo_cs_xx_dw within w_prod_bolle_out_com
integer x = 91
integer y = 720
integer width = 3200
integer height = 480
integer taborder = 20
string dataobject = "d_stato_stock_terzista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto,ls_cod_deposito,ls_cod_fornitore
LONG  l_Error
double ldd_quan_impegnata

if dw_lista_prodotti_spedizione.rowcount() > 0 then
	ls_cod_prodotto = dw_lista_prodotti_spedizione.getitemstring(dw_lista_prodotti_spedizione.getrow(),"cod_prodotto")
	ls_cod_fornitore = dw_scelta_fornitore.gettext()
	
	select cod_deposito
	into   :ls_cod_deposito
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    cod_fornitore=:ls_cod_fornitore;
	
	l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_prodotto,ls_cod_deposito)
	
	IF l_Error < 0 THEN
	PCCA.Error = c_Fatal
	END IF
	
	select sum(quan_impegnata)
	into   :ldd_quan_impegnata
	from   impegni_deposito_fasi_com
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_deposito=:ls_cod_deposito
	and    cod_prodotto_spedito=:ls_cod_prodotto;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	st_quan_impegnata.text = string(ldd_quan_impegnata)
	
end if


end event

type cb_carica_note from commandbutton within w_prod_bolle_out_com
integer x = 91
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Carica Note"
end type

event clicked;string ls_cod_prodotto_finito,ls_des_prodotto_finito

select cod_prodotto
into   :ls_cod_prodotto_finito
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :il_anno_commessa and    
		 num_commessa = :il_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

select des_prodotto
into   :ls_des_prodotto_finito
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_prodotto = :ls_cod_prodotto_finito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return 0
end if

sle_riga_riferimento.text = "Ns. Commessa Anno:" + string(il_anno_commessa) + " Nr.:" + string(il_num_commessa)
mle_nota_testata.text="Prodotto finito: " + ls_cod_prodotto_finito + " / " + ls_des_prodotto_finito

end event

type st_stquan_impegnata from statictext within w_prod_bolle_out_com
integer x = 91
integer y = 1220
integer width = 535
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Quantità Impegnata:"
boolean focusrectangle = false
end type

type dw_elenco_fasi_esterne_attivazione from uo_cs_xx_dw within w_prod_bolle_out_com
integer x = 91
integer y = 140
integer width = 2377
integer height = 1060
integer taborder = 190
boolean bringtotop = true
string dataobject = "d_elenco_fasi_esterne_attivazione"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa,ll_prog_riga)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event clicked;call super::clicked;if i_extendmode then
	string ls_errore,ls_cod_versione,ls_cod_prodotto_finito,ls_des_prodotto_finito, ls_cod_versione_fase
	long ll_prog_spedizione
	double ldd_quan_in_produzione
	integer li_risposta
	
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_elenco_prod_uscita_bol.resetupdate()
	dw_lista_prodotti_spedizione.resetupdate()
	dw_prod_uscita_bol_spedite.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock.resetupdate()
	
	il_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "anno_commessa")
	il_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "num_commessa")
	il_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "prog_riga")
	is_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(), "cod_prodotto")
	ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(), "cod_versione")
	ldd_quan_in_produzione = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(), "quan_in_produzione")
	is_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
	is_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		

	select max(prog_spedizione)
	into   :ll_prog_spedizione
	from   prod_bolle_out_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :il_anno_commessa  and    
			 num_commessa = :il_num_commessa    and    
			 cod_prodotto_fase = :is_cod_prodotto_fase and   
			 cod_versione = :ls_cod_versione_fase and
			 cod_reparto = :is_cod_reparto 	   and    
			 cod_lavorazione = :is_cod_lavorazione     and    
			 anno_registrazione is not null     and    
			 num_registrazione is not null      and    
			 prog_riga_bol_ven is not null;
	
	if sqlca.sqlcode< 0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return 
	end if
	
	if isnull(ll_prog_spedizione) then
		ll_prog_spedizione=1
	else
		ll_prog_spedizione++
	end if

	em_spedizione.text = string(ll_prog_spedizione)
	
	if cbx_bolla_unica.checked = false then dw_elenco_prod_uscita_bol.reset()
	dw_lista_prodotti_spedizione.reset()
	dw_scelta_stock.reset()
	
	
	// *** Michela 06/10/2006: la versione è nella fase di lavorazione quindi commento la
	//                         select dall'anagrafica commesse
//	select cod_versione
//	into   :ls_cod_versione
//	from   anag_commesse
//	where  cod_azienda = :s_cs_xx.cod_azienda 	and    
//	       anno_commessa = :il_anno_commessa     and    
//			 num_commessa = :il_num_commessa;
//	
//	if sqlca.sqlcode < 0 then
//		messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//		return 
//	end if
		
	dw_lista_prodotti_spedizione.reset()

	li_risposta = wf_imposta_prodotti_spedizione( il_anno_commessa, &
																 il_num_commessa,  &
																 il_prog_riga,     &
																 is_cod_prodotto_fase, &
																 ls_cod_versione_fase, &
																 ldd_quan_in_produzione, &
																 1, &
																 ls_errore)
	
	if li_risposta <0 then
		g_mb.messagebox("Sep",ls_errore,stopsign!)
		return
	end if
	
	dw_lista_prodotti_spedizione.Reset_DW_Modified(c_ResetChildren)

	dw_lista_prodotti_spedizione.settaborder("quan_spedizione",10)
	dw_lista_prodotti_spedizione.change_dw_current()
	parent.triggerevent("pc_retrieve")
	dw_prod_uscita_bol_spedite.change_dw_current()
	parent.triggerevent("pc_retrieve")

	wf_imposta_stock()
	dw_scelta_stock.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_elenco_prod_uscita_bol.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_stato_stock_terzista.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_elenco_fasi_esterne_attivazione.resetupdate()
	dw_elenco_prod_uscita_bol.resetupdate()
	dw_lista_prodotti_spedizione.resetupdate()
	dw_prod_uscita_bol_spedite.resetupdate()
	dw_scelta_fornitore.resetupdate()
	dw_scelta_stock.resetupdate()
	
end if
end event

type dw_elenco_prod_uscita_bol from uo_cs_xx_dw within w_prod_bolle_out_com
integer x = 91
integer y = 120
integer width = 3177
integer height = 940
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_elenco_prod_uscita_bol"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga,ll_prog_spedizione
string ls_cod_prodotto_fase,ls_cod_reparto,ls_cod_lavorazione, ls_cod_versione_fase

ll_anno_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"anno_commessa")
ll_num_commessa = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"num_commessa")
ll_prog_riga = dw_elenco_fasi_esterne_attivazione.getitemnumber(dw_elenco_fasi_esterne_attivazione.getrow(),"prog_riga")
ls_cod_prodotto_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_prodotto")		
ls_cod_versione_fase = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_versione")	
ls_cod_reparto = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_reparto")		
ls_cod_lavorazione = dw_elenco_fasi_esterne_attivazione.getitemstring(dw_elenco_fasi_esterne_attivazione.getrow(),"cod_lavorazione")		

if cbx_bolla_unica.checked = true then
	ls_cod_prodotto_fase = '%'
	ls_cod_reparto = '%'
	ls_cod_lavorazione ='%'
end if

l_Error = Retrieve( s_cs_xx.cod_azienda, &
						  ll_anno_commessa, &
						  ll_num_commessa, &
						  ll_prog_riga, &
						  ls_cod_prodotto_fase, & 
						  ls_cod_versione_fase, &
						  ls_cod_reparto, &
						  ls_cod_lavorazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_lista_prodotti_spedizione from uo_cs_xx_dw within w_prod_bolle_out_com
integer x = 91
integer y = 140
integer width = 3200
integer height = 1060
integer taborder = 90
boolean bringtotop = true
string dataobject = "d_lista_prodotti_spedizione"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event clicked;call super::clicked;if i_extendmode then
	wf_imposta_stock()
	dw_stato_stock_terzista.change_dw_current()
	parent.triggerevent("pc_retrieve")
end if
end event

event rowfocuschanged;call super::rowfocuschanged;//if i_extendmode then
//	if currentrow = 0 then return
//	wf_imposta_stock()
//end if
end event

