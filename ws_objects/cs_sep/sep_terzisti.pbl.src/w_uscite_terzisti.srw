﻿$PBExportHeader$w_uscite_terzisti.srw
$PBExportComments$Window uscite terzisti
forward
global type w_uscite_terzisti from w_cs_xx_principale
end type
type dw_lista_uscite_terzisti from uo_cs_xx_dw within w_uscite_terzisti
end type
end forward

global type w_uscite_terzisti from w_cs_xx_principale
int Width=3100
int Height=1485
boolean TitleBar=true
string Title="Lista UsciteTerzisti"
dw_lista_uscite_terzisti dw_lista_uscite_terzisti
end type
global w_uscite_terzisti w_uscite_terzisti

type variables

end variables

on w_uscite_terzisti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_lista_uscite_terzisti=create dw_lista_uscite_terzisti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_uscite_terzisti
end on

on w_uscite_terzisti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_lista_uscite_terzisti)
end on

event pc_setwindow;call super::pc_setwindow;dw_lista_uscite_terzisti.set_dw_options(sqlca, &
                                    						  i_openparm, &
				 		                                      c_nomodify + c_nonew +  c_nodelete, &
      		                                            c_default)

//dw_det_ingressi_terzisti.set_dw_options(sqlca,dw_lista_ingressi_terzisti,c_sharedata + c_scrollparent + c_nonew + c_nodelete,c_default)

iuo_dw_main = dw_lista_uscite_terzisti
end event

type dw_lista_uscite_terzisti from uo_cs_xx_dw within w_uscite_terzisti
int X=23
int Y=21
int Width=3018
int Height=1341
int TabOrder=20
string DataObject="d_lista_uscite_terzisti"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa,ll_num_commessa,ll_prog_riga

ll_anno_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_commessa")
ll_num_commessa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_commessa")
ll_prog_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_anno_commessa,ll_num_commessa,ll_prog_riga)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

