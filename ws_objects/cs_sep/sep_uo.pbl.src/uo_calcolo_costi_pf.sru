﻿$PBExportHeader$uo_calcolo_costi_pf.sru
forward
global type uo_calcolo_costi_pf from nonvisualobject
end type
end forward

global type uo_calcolo_costi_pf from nonvisualobject
end type
global uo_calcolo_costi_pf uo_calcolo_costi_pf

forward prototypes
public function decimal uof_costo_totale_prodotto (string fs_cod_prodotto, string fs_cod_versione, string fs_tipo_valorizzazione, ref string fs_errore)
public function integer uof_costo_fasi (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quantita, ref decimal fd_costo_totale, ref string fs_errore)
public function integer uof_costo_singola_fase (string fs_cod_prodotto_fase, string fs_cod_versione, decimal fd_quan_utilizzo, ref decimal fd_costo_fase, ref string fs_errore)
end prototypes

public function decimal uof_costo_totale_prodotto (string fs_cod_prodotto, string fs_cod_versione, string fs_tipo_valorizzazione, ref string fs_errore);/* Calcola il costo totale di un componente di distinta con la stessa logoca del costo preventivo.

fs_cod_prodotto 				prodotto finito di cui si desidera calcolare il costo
fs_cod_versione  				versione del prodotto finito
fs_tipo_valorizzazione		A=Prezzo Acquisto   U=Ultimo costo acquisto  S=Costo Standard

*/
integer li_risposta
string ls_mat_prima[],ls_des_prodotto,ls_errore,ls_test, ls_vuoto[]
dec{4} ldd_quantita,ldd_quantita_totale,ldd_costo_materia_prima,ldd_costo_totale, ldd_costo_totale_mp, ldd_costo_totale_lav, &
       ldd_costo_totale_ru,ld_costo_totale_fasi, ld_costo_complessivo 
long   ll_t,ll_null,ll_cont
double ldd_quan_utilizzo[], ldd_vuoto[]


ldd_quantita = 1
setnull(ls_test)
ls_mat_prima[] = ls_vuoto[]
ldd_quan_utilizzo[] = ldd_vuoto[]

ll_cont = 0

select count(*)
into   :ll_cont
from   distinta
where  cod_azienda        = :s_cs_xx.cod_azienda and
		 cod_prodotto_padre = :fs_cod_prodotto and
		 cod_versione       = :fs_cod_versione;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in select tabella distinta_padri.~r~n:" + sqlca.sqlerrtext
	return -1
end if

if ll_cont = 0 or isnull(ll_cont) then
	// trattasi di materia prima; calcolo il costo della materia prima
	ls_mat_prima[1] = fs_cod_prodotto
	ldd_quan_utilizzo[1] = 1
else
	// si tratta di un semilavorato o comunque di un prodotto con la sua distinta base
	setnull(ll_null)
	
	if ldd_quantita = 0 or isnull(fs_cod_prodotto) then
		fs_errore = "Selezionare un prodotto e inserire una quantità"
		return -1
	end if
	
	// restituisce le materie prime di u ncerto prodotto finito
	s_cs_xx.parametri.parametro_s_5 = "REPORT_COSTO_PREVENTIVO"
	li_risposta = f_trova_mat_prima(fs_cod_prodotto,fs_cod_versione,ls_mat_prima[],ldd_quan_utilizzo[],1,ll_null,ll_null)
	setnull(s_cs_xx.parametri.parametro_s_5)
	
end if
	
for ll_t=1 to upperbound(ls_mat_prima)

	ldd_quantita_totale = ldd_quan_utilizzo[ll_t] * ldd_quantita
  
	choose case fs_tipo_valorizzazione
		case "A"
			select des_prodotto,prezzo_acquisto
			into	 :ls_des_prodotto,
					 :ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    cod_prodotto = :ls_mat_prima[ll_t];
		
		case "S"
			select des_prodotto,costo_standard
			into	 :ls_des_prodotto,
					 :ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    cod_prodotto = :ls_mat_prima[ll_t];
			
		case "U"
			select des_prodotto,costo_ultimo
			into	 :ls_des_prodotto,
					 :ldd_costo_materia_prima
			from 	 anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    cod_prodotto = :ls_mat_prima[ll_t];
	end choose
	
	ldd_costo_totale_mp = ldd_costo_totale_mp + (ldd_costo_materia_prima * ldd_quantita_totale)

next

ld_costo_totale_fasi = 0
	
li_risposta = uof_costo_fasi(fs_cod_prodotto,fs_cod_versione,ldd_quantita,ld_costo_totale_fasi,ls_errore)
	
ld_costo_complessivo = ldd_costo_totale_mp + ld_costo_totale_fasi
	
return ld_costo_complessivo
end function

public function integer uof_costo_fasi (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quantita, ref decimal fd_costo_totale, ref string fs_errore); /*Funzione che calcola i costi di lavorazione
 nome: wf_fasi_lavorazione
 tipo: integer
       -1 failed
  		 0 passed

	Variabili passate: 		nome					 tipo				passaggio per			commento
							
							 fs_cod_prodotto	 		 string  		valore
							 fs_cod_versione			 string			valore
							 fdd_quantita				 decimal			valore
							 fd_costo_totale			 decimal			ref						valore totale finale
							 fs_errore					 string			ref						errore in fase di cacolo

		Creata il 15/4/2008
		Autore Enrico Menegotto
*/
string    ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, & 
			 ls_cod_lavorazione,ls_test,ls_cod_cat_attrezzature,ls_des_cat_attrezzature, &
			 ls_cod_cat_attrezzature_2,ls_des_cat_attrezzature_2, ls_cod_versione_figlio, ls_flag_fase_esterna, ls_flag_uso, &
			 ls_flag_mp, ls_flag_valorizza
long      ll_num_righe,ll_num_figli, ll_num_sequenza
integer   li_risposta, li_ret
dec{4}    ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
			 ldd_tempo_lavorazione, ldd_tempo_totale, ldd_costo_medio_orario, ldd_costo_totale, &
			 ldd_tempo_risorsa_umana,ldd_costo_medio_orario_2,ldd_costo_totale_2, ld_costo_orario, ld_costo_pezzo, &
			 ld_costo_totale_fase
datastore lds_righe_distinta



ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)


	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * fd_quantita
	
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda=:s_cs_xx.cod_azienda 
	and 	 cod_prodotto_padre=:ls_cod_prodotto_figlio and
	       cod_versione = :ls_cod_versione_figlio;
	
	if sqlca.sqlcode=100 then continue
	
	select flag_materia_prima,
			 flag_valorizza_mp
	into	 :ls_flag_mp,
			 :ls_flag_valorizza
	from	 distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto_padre = :fs_cod_prodotto and
			 cod_versione = :fs_cod_versione and
			 cod_versione_figlio = :ls_cod_versione_figlio and
			 num_sequenza = :ll_num_sequenza;
			 
	if sqlca.sqlcode = 0 and not isnull( ls_flag_mp) and ls_flag_mp = "S" and ls_flag_valorizza = "N" and not isnull(ls_flag_valorizza) then
	elseif sqlca.sqlcode = 0 and not isnull( ls_flag_mp) and ls_flag_mp = "S" and ls_flag_valorizza = "S" then		
		continue
	end if	
	
//	Calcolo del costo di una fase
	li_ret = uof_costo_singola_fase(ls_cod_prodotto_figlio, ls_cod_versione_figlio, ldd_quan_utilizzo, ref ld_costo_totale_fase, ref fs_errore)
	if li_ret < 0 then
		fs_errore = "Errore in calcolo costo singola fase~r~n" + fs_errore
		return -1
	end if
	

//   SELECT cod_reparto,   
//          cod_lavorazione,
//			 cod_cat_attrezzature,
//			 tempo_attrezzaggio,
//		    tempo_attrezzaggio_commessa,
//			 tempo_lavorazione,
//			 cod_cat_attrezzature_2,
//			 tempo_risorsa_umana,
//			 flag_esterna,
//			 costo_orario,
//			 costo_pezzo,
//			 flag_uso
//   INTO   :ls_cod_reparto,   
//          :ls_cod_lavorazione,
//			 :ls_cod_cat_attrezzature,
// 			 :ldd_tempo_attrezzaggio,
//		    :ldd_tempo_attrezzaggio_commessa,
//			 :ldd_tempo_lavorazione,
//			 :ls_cod_cat_attrezzature_2,
//			 :ldd_tempo_risorsa_umana,
//			 :ls_flag_fase_esterna,
//			 :ld_costo_orario,
//			 :ld_costo_pezzo,
//			 :ls_flag_uso
//   FROM  tes_fasi_lavorazione
//   WHERE cod_azienda = :s_cs_xx.cod_azienda  AND   
//			cod_prodotto = :ls_cod_prodotto_figlio AND
//			cod_versione = :ls_cod_versione_figlio;
//
//	// manca la fase di lavorazione; vado avanti lo stesso
//	if sqlca.sqlcode=100 then continue
//
//  
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda and
			 cod_prodotto =:ls_cod_prodotto_figlio;
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in select anag_prodotti~r~n" + sqlca.sqlerrtext
		return -1
	end if
	

//	select des_cat_attrezzature,
//		    costo_medio_orario
//	into   :ls_des_cat_attrezzature,
//			 :ldd_costo_medio_orario
//	from   tab_cat_attrezzature
//	where  cod_azienda=:s_cs_xx.cod_azienda and
//			 cod_cat_attrezzature=:ls_cod_cat_attrezzature;
//	if sqlca.sqlcode < 0 then
//		fs_errore = "Errore in select tab_cat_attrezzature~r~n" + sqlca.sqlerrtext
//		return -1
//	end if
//
//	select des_cat_attrezzature,
//			 costo_medio_orario
//	into   :ls_des_cat_attrezzature_2,
//			 :ldd_costo_medio_orario_2
//	from   tab_cat_attrezzature
//	where  cod_azienda=:s_cs_xx.cod_azienda and
//	       cod_cat_attrezzature=:ls_cod_cat_attrezzature_2;
//	if sqlca.sqlcode < 0 then
//		fs_errore = "Errore in select tab_cat_attrezzature~r~n" + sqlca.sqlerrtext
//		return -1
//	end if
//	
//	ldd_tempo_attrezzaggio = ldd_tempo_attrezzaggio 
//	ldd_tempo_attrezzaggio_commessa = ldd_tempo_attrezzaggio_commessa 
//	ldd_tempo_lavorazione = ldd_tempo_lavorazione * ldd_quan_utilizzo
//	ldd_tempo_risorsa_umana = ldd_tempo_risorsa_umana * ldd_quan_utilizzo
//
//	ldd_tempo_totale = 0
//
//	ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio + ldd_tempo_attrezzaggio_commessa + ldd_tempo_lavorazione
//
//	if not isnull(ls_flag_fase_esterna) and ls_flag_fase_esterna = "S" then		
//		
//		if ls_flag_uso = "O" then
//			if isnull(ld_costo_orario) then ld_costo_orario = 0
//			ldd_costo_medio_orario = ld_costo_orario
//			ldd_costo_medio_orario_2 = ld_costo_orario
//			ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
//		else
//			if isnull(ld_costo_pezzo) then ld_costo_pezzo = 0
//			ldd_costo_totale = ldd_quan_utilizzo * ld_costo_pezzo
//		end if
//		
//		ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2	
//		
//	else			
//		ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
//		ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2		
//	end if


	// costo lavorazione + costo risorsa umana
//	fd_costo_totale = fd_costo_totale + (ldd_costo_totale + ldd_costo_totale_2)
	fd_costo_totale = fd_costo_totale + ld_costo_totale_fase

	// verifico se lo stesso prodotto è padre di un'altra distinta.
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:ls_cod_prodotto_figlio and
	       cod_versione = :ls_cod_versione_figlio;

	if sqlca.sqlcode = 100 then
		continue
	else
		li_risposta = uof_costo_fasi(ls_cod_prodotto_figlio,ls_cod_versione_figlio,ldd_quan_utilizzo,ref fd_costo_totale, ref ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

destroy lds_righe_distinta

return 0
end function

public function integer uof_costo_singola_fase (string fs_cod_prodotto_fase, string fs_cod_versione, decimal fd_quan_utilizzo, ref decimal fd_costo_fase, ref string fs_errore);string ls_cod_reparto, ls_cod_lavorazione,ls_cod_cat_attrezzature,ls_cod_cat_attrezzature_2,ls_flag_fase_esterna,ls_flag_uso,ls_des_cat_attrezzature, &
		 ls_des_cat_attrezzature_2
dec{4} ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa,ldd_tempo_lavorazione,ldd_tempo_risorsa_umana,ld_costo_orario,ld_costo_pezzo,ldd_costo_medio_orario, &
       ldd_costo_medio_orario_2,ldd_tempo_totale,ldd_costo_totale,ldd_costo_totale_2
		 

   SELECT cod_reparto,   
          cod_lavorazione,
			 cod_cat_attrezzature,
			 tempo_attrezzaggio,
		    tempo_attrezzaggio_commessa,
			 tempo_lavorazione,
			 cod_cat_attrezzature_2,
			 tempo_risorsa_umana,
			 flag_esterna,
			 costo_orario,
			 costo_pezzo,
			 flag_uso
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
 			 :ldd_tempo_attrezzaggio,
		    :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_risorsa_umana,
			 :ls_flag_fase_esterna,
			 :ld_costo_orario,
			 :ld_costo_pezzo,
			 :ls_flag_uso
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :s_cs_xx.cod_azienda  AND   
			cod_prodotto = :fs_cod_prodotto_fase AND
			cod_versione = :fs_cod_versione;

	// manca la fase di lavorazione; vado avanti lo stesso
	if sqlca.sqlcode=100 then
		fd_costo_fase = 0 
		return 0
	end if	


	select des_cat_attrezzature,
		    costo_medio_orario
	into   :ls_des_cat_attrezzature,
			 :ldd_costo_medio_orario
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda and
			 cod_cat_attrezzature=:ls_cod_cat_attrezzature;
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in select tab_cat_attrezzature~r~n" + sqlca.sqlerrtext
		return -1
	end if
//
	select des_cat_attrezzature,
			 costo_medio_orario
	into   :ls_des_cat_attrezzature_2,
			 :ldd_costo_medio_orario_2
	from   tab_cat_attrezzature
	where  cod_azienda=:s_cs_xx.cod_azienda and
	       cod_cat_attrezzature=:ls_cod_cat_attrezzature_2;
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in select tab_cat_attrezzature~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	ldd_tempo_attrezzaggio = ldd_tempo_attrezzaggio 
	ldd_tempo_attrezzaggio_commessa = ldd_tempo_attrezzaggio_commessa 
	ldd_tempo_lavorazione = ldd_tempo_lavorazione * fd_quan_utilizzo
	ldd_tempo_risorsa_umana = ldd_tempo_risorsa_umana * fd_quan_utilizzo

	ldd_tempo_totale = 0

	ldd_tempo_totale =ldd_tempo_totale + ldd_tempo_attrezzaggio + ldd_tempo_attrezzaggio_commessa + ldd_tempo_lavorazione

	if not isnull(ls_flag_fase_esterna) and ls_flag_fase_esterna = "S" then		
		
		if ls_flag_uso = "O" then
			if isnull(ld_costo_orario) then ld_costo_orario = 0
			ldd_costo_medio_orario = ld_costo_orario
			ldd_costo_medio_orario_2 = ld_costo_orario
			ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
		else
			if isnull(ld_costo_pezzo) then ld_costo_pezzo = 0
			ldd_costo_totale = fd_quan_utilizzo * ld_costo_pezzo
		end if
		
		ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2	
		
	else			
		ldd_costo_totale = (ldd_tempo_totale/60) * ldd_costo_medio_orario
		ldd_costo_totale_2 = (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2		
	end if

fd_costo_fase = ldd_costo_totale_2 + ldd_costo_totale
return 0
end function

on uo_calcolo_costi_pf.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_calcolo_costi_pf.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

