﻿$PBExportHeader$uo_genera_doc_produzione.sru
$PBExportComments$User Object Creazione Documenti per la Produzione
forward
global type uo_genera_doc_produzione from nonvisualobject
end type
end forward

global type uo_genera_doc_produzione from nonvisualobject
end type
global uo_genera_doc_produzione uo_genera_doc_produzione

type variables
// -----------------------  VARIABILI DI INGRESSO -------------------
string is_cod_tipo_det_ven[]
string is_cod_prodotto[]
string is_cod_deposito[]
string is_cod_ubicazione[]
string is_cod_lotto[]
datetime idt_data_stock[]
long il_prog_stock[]
double id_quantita[]
string is_riferimento_riga
string is_riferimento_riga_fine
string is_nota_testata
string is_nota_piede
long il_anno_commessa
long il_num_commessa
datetime idt_data_commessa
// -----------------------  VARIABILI DI USCITA -------------------
string is_messaggio
end variables

forward prototypes
public function long f_crea_bolla_uscita (string fs_cod_fornitore, string fs_cod_mov_magazzino, string fs_cod_tipo_bol_ven, string fs_flag_conferma, boolean fb_flag_nuova_bolla, ref long fl_anno_bolla_ven, ref long fl_num_bolla_ven, ref long fl_prog_riga_ven[])
end prototypes

public function long f_crea_bolla_uscita (string fs_cod_fornitore, string fs_cod_mov_magazzino, string fs_cod_tipo_bol_ven, string fs_flag_conferma, boolean fb_flag_nuova_bolla, ref long fl_anno_bolla_ven, ref long fl_num_bolla_ven, ref long fl_prog_riga_ven[]);// ---------------------------------------------------------------------------------------------------------------
//		FUNZIONE DI GENERAZIONE BOLLA DA FASI PRODUZIONE PER TERZISTI
//		ENME 26/02/1999
//
//		------------------------------------------------------------------------------
//		VARIABILI IN INGRESSO		TIPO DI DATO
//    ------------------------------------------------------------------------------
//		fs_cod_fornitore				codice fornitore
//		fs_cod_tipo_bol_ven			tipo bolla di uscita
//		fs_flag_conferma				eseguo anche la conferma bolla ?
//
//		------------------------------------------------------------------------------
//		VARIABILI INGRESSO/USCITA	TIPO DI DATO
//    ------------------------------------------------------------------------------
//		fl_anno_bolla_ven				anno_bolla_generata
//		fl_num_bolla_ven				numero bolla generata
//    fl_prog_riga_ven[]         progressivi dettagli righe generate
//
//
//
//
// ---------------------------------------------------------------------------------------------------------------


string ls_cod_operatore, ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
       ls_cod_banca_clien_for, ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_cod_causale, ls_flag_riferimento, ls_cod_tipo_det_ven_rif, ls_des_riferimento, &
		 ls_cod_iva, ls_cod_tipo_movimento, ls_cod_misura, ls_cod_iva_rif, ls_cod_tipo_movimento_rif, &
		 ls_tes_tabella, ls_det_tabella, ls_prog_riga, ls_quantita, ls_messaggio, ls_cod_documento, &
		 ls_numeratore_documento, ls_flag_tipo_det_ven, ls_cod_deposito_merce[], ls_cod_ubicazione[], &
		 ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], ls_sql, ls_cod_deposito_mov, ls_flag_fornitore, &
		 ls_cod_fornitore_mov, ls_cod_deposito_fornitore, ls_cod_iva_det, ls_cod_iva_esente, ls_flag_movimenti
long ll_anno_registrazione, ll_num_registrazione, ll_max_registrazione, ll_i, ll_num_prodotti, &
     ll_riga, ll_num_documento, ll_anno_esercizio, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
	  ll_prog_stock[], ll_cont_mov, ll_prog_ordinamento
double ld_sconto, ld_cambio_ven
datetime ldt_data_registrazione, ldt_data_inizio_trasporto, ldt_ora_inizio_trasporto, ldt_oggi, ldt_data_stock[]

uo_calcola_documento_euro luo_calcolo


if not(fb_flag_nuova_bolla) then
	select cod_fornitore,
	       flag_movimenti
	into   :fs_cod_fornitore,
	       :ls_flag_movimenti
	from   tes_bol_ven
	where  cod_azienda        = :s_cs_xx.cod_azienda and
	       anno_registrazione = :fl_anno_bolla_ven   and
			 num_registrazione  = :fl_num_bolla_ven;
	if sqlca.sqlcode = 100 then
		is_messaggio = "La bolla specificata è inesistente"
		return -1
	elseif sqlca.sqlcode <> 0 then
		is_messaggio = "Errore in ricerca bolla di uscita.~r~nDettaglio " + sqlca.sqlerrtext
		return -1
	end if
	if ls_flag_movimenti = "S" then
		is_messaggio = "Bolla già confermata: impossibile proseguire!"
		return -1
	end if
	ll_anno_registrazione = fl_anno_bolla_ven
	ll_num_registrazione = fl_num_bolla_ven
end if

ll_anno_esercizio = f_anno_esercizio()
ll_num_prodotti = upperbound(is_cod_prodotto)
ll_prog_riga = 0
if ll_num_prodotti = 0 or isnull(ll_i) then
	is_messaggio = "Non vi sono prodotti da inserire in bolla"
	return -1
end if
ll_riga = 0
select cod_deposito,
		 cod_valuta,
		 cod_tipo_listino_prodotto,
		 cod_pagamento,
		 sconto,
		 cod_banca_clien_for,
		 cod_imballo,
		 cod_vettore,
		 cod_inoltro,
		 cod_mezzo,
		 cod_porto,
		 cod_resa
  into :ls_cod_deposito,
		 :ls_cod_valuta,
		 :ls_cod_tipo_listino_prodotto,
		 :ls_cod_pagamento,
		 :ld_sconto,
		 :ls_cod_banca_clien_for,
		 :ls_cod_imballo,
		 :ls_cod_vettore,
		 :ls_cod_inoltro,
		 :ls_cod_mezzo,
		 :ls_cod_porto,
		 :ls_cod_resa
from   anag_fornitori
where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
		 anag_fornitori.cod_fornitore = :fs_cod_fornitore;
if sqlca.sqlcode <> 0 then
	is_messaggio = "Codice fornitore inesistente in anagrafica fornitori"
	return -1
end if


SELECT cod_causale,   
       flag_abilita_rif_ord_ven,   
       cod_tipo_det_ven_rif_ord_ven,   
       des_riferimento_ord_ven
  INTO :ls_cod_causale,   
       :ls_flag_riferimento,   
       :ls_cod_tipo_det_ven_rif,   
       :ls_des_riferimento  
  FROM tab_tipi_bol_ven  
 WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
       cod_tipo_bol_ven = :fs_cod_tipo_bol_ven ;
if sqlca.sqlcode <> 0 then
	is_messaggio = "Tipo bolla non trovato in Tipi Bolle Uscita: verificare"
	return -1
end if

if ls_flag_riferimento = "D" then
	SELECT cod_tipo_movimento,   
			 cod_iva  
	INTO   :ls_cod_tipo_movimento_rif,   
			 :ls_cod_iva_rif
	FROM   tab_tipi_det_ven
	WHERE  cod_azienda = :s_cs_xx.cod_azienda AND
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven_rif;
	if sqlca.sqlcode <> 0 then
		is_messaggio = "Attenzione: non esiste il tipo dettaglio vendita usato per riferimenti che è stato indicato in tipi bolle uscota"
		return -1
	end if
else
	setnull(ls_cod_tipo_movimento_rif)
	setnull(ls_cod_iva_rif)
end if	


// ------------------------  TROVO IL NUMERO BOLLA ---------------------------------------------------------------

if fb_flag_nuova_bolla then
	ll_anno_registrazione = f_anno_esercizio()
	if ll_anno_registrazione <= 0 then
		is_messaggio = "Impostare l'anno di esercizio in parametri aziendali"
		return -1
	end if
	
	select con_bol_ven.num_registrazione
	into   :ll_num_registrazione
	from   con_bol_ven
	where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
	choose case sqlca.sqlcode
		case 0
			ll_num_registrazione ++
		case 100
			is_messaggio = "Errore in assegnazione numero bolla interno: verificare di aver impostato i parametri bolle di uscita. Posso proseguire ugulamente~r~n"
		case else
			is_messaggio = "Errore in assegnazione numero bolla interno.~r~nDettaglio errore "+ sqlca.sqlerrtext +"~r~nIl salvataggio verrà effettuato ugualmente"
	end choose
	
	ll_max_registrazione = 0
	select max(num_registrazione)
	into   :ll_max_registrazione
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione;
	if not isnull(ll_max_registrazione) then
		if ll_max_registrazione >= ll_num_registrazione then ll_num_registrazione = ll_max_registrazione + 1
	end if
	
	update con_bol_ven
	set    con_bol_ven.num_registrazione = :ll_num_registrazione
	where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;

else
	if isnull(fl_anno_bolla_ven) or fl_anno_bolla_ven < 1 or isnull(fl_num_bolla_ven) or fl_num_bolla_ven < 1 then
		is_messaggio = "Si è scelto di aggiungere ad una bolla già esistente, ma non risulta specificato anno/numero bolla a ciu aggiungere i dati "
		return -1
	end if
end if

// ------------------------ ESEGUO INSERT TESTATA BOLLA USCITA ----------------------------------------------------

if fb_flag_nuova_bolla then
	ldt_data_registrazione = datetime(today(),00:00:00)
	ld_cambio_ven = f_cambio_val_ven(ls_cod_valuta, ldt_data_registrazione) 
	setnull(ls_cod_operatore)
	if ls_flag_riferimento = "T" then
		if isnull(is_nota_testata) then is_nota_testata = ""
		is_nota_testata = "Commessa " + string(il_anno_commessa) + "/" + string(il_num_commessa,"######") + " del " + string(idt_data_commessa,"dd/mm/yyyy")
	end if
	
	INSERT INTO tes_bol_ven  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  cod_operatore,   
				  cod_tipo_bol_ven,   
				  cod_cliente,   
				  cod_fornitore,   
				  cod_des_cliente,   
				  cod_fil_fornitore,   
				  rag_soc_1,   
				  rag_soc_2,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia,   
				  cod_deposito,   
				  cod_deposito_tras,   
				  cod_ubicazione,   
				  cod_valuta,   
				  cambio_ven,   
				  cod_tipo_listino_prodotto,   
				  cod_pagamento,   
				  sconto,   
				  cod_agente_1,   
				  cod_agente_2,   
				  cod_banca,   
				  num_ord_cliente,   
				  data_ord_cliente,   
				  cod_imballo,   
				  aspetto_beni,   
				  peso_netto,   
				  peso_lordo,   
				  num_colli,   
				  cod_vettore,   
				  cod_inoltro,   
				  cod_mezzo,   
				  causale_trasporto,   
				  cod_porto,   
				  cod_resa,   
				  cod_documento,   
				  numeratore_documento,   
				  anno_documento,   
				  num_documento,   
				  data_bolla,   
				  nota_testata,   
				  nota_piede,   
				  flag_fuori_fido,   
				  flag_blocco,   
				  flag_movimenti,   
				  flag_riep_fat,   
				  cod_banca_clien_for,   
				  cod_des_fornitore,   
				  flag_gen_fat,   
				  cod_causale,   
				  data_inizio_trasporto,   
				  ora_inizio_trasporto )  
	VALUES   ( :s_cs_xx.cod_azienda,   
				  :ll_anno_registrazione,   
				  :ll_num_registrazione,   
				  :ldt_data_registrazione,
				  :ls_cod_operatore,   
				  :fs_cod_tipo_bol_ven,   
				  null,   
				  :fs_cod_fornitore,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  :ls_cod_deposito,   
				  :ls_cod_deposito,   
				  null,   
				  :ls_cod_valuta,   
				  :ld_cambio_ven,   
				  :ls_cod_tipo_listino_prodotto,   
				  :ls_cod_pagamento,   
				  :ld_sconto,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  :ls_cod_imballo,   
				  null,   
				  0,   
				  0,   
				  0,   
				  :ls_cod_vettore,   
				  :ls_cod_inoltro,   
				  :ls_cod_mezzo,   
				  null,   
				  :ls_cod_porto,   
				  :ls_cod_resa,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  :is_nota_testata,   
				  :is_nota_piede,   
				  'N',   
				  'N',   
				  'N',   
				  'N',   
				  :ls_cod_banca_clien_for,   
				  null,   
				  'N',   
				  :ls_cod_causale,   
				  :ldt_data_inizio_trasporto,   
				  :ldt_ora_inizio_trasporto)  ;
	if sqlca.sqlcode <> 0 then
		is_messaggio = "Errore in creazione testata bolla. Errore numero " + string(sqlca.sqlcode) +"~r~nDettaglio errore " + sqlca.sqlerrtext
		return -1
	end if
else
	ll_riga = 0
	select max(prog_riga_bol_ven)
	into   :ll_riga
	from   det_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
	if sqlca.sqlcode <> 0 then
		is_messaggio = "Errore in ricerca numero riga ~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
	if isnull(ll_riga) then ll_riga = 0
end if
// -------------------------  INSERIMENTO DETTAGLI BOLLA ----------------------------------------------------

if len(is_riferimento_riga) > 0  and ls_flag_riferimento = "D" then
	
  ll_riga = ll_riga + 10
  INSERT INTO det_bol_ven  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_bol_ven,   
           cod_prodotto,   
           cod_tipo_det_ven,   
           cod_deposito,   
           cod_ubicazione,   
           cod_lotto,   
           progr_stock,   
           data_stock,   
           cod_misura,   
           des_prodotto,   
           quan_consegnata,   
           prezzo_vendita,   
           fat_conversione_ven,   
           sconto_1,   
           sconto_2,   
           provvigione_1,   
           provvigione_2,   
           cod_iva,   
           cod_tipo_movimento,   
           num_registrazione_mov_mag,   
           nota_dettaglio,   
           anno_registrazione_ord_ven,   
           num_registrazione_ord_ven,   
           prog_riga_ord_ven,   
           anno_commessa,   
           num_commessa,   
           cod_centro_costo,   
           sconto_3,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           anno_registrazione_mov_mag,   
           anno_reg_bol_acq,   
           num_reg_bol_acq,   
           prog_riga_bol_acq,   
           anno_reg_des_mov,   
           num_reg_des_mov,   
           cod_versione,   
           num_confezioni,   
           num_pezzi_confezione )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ll_anno_registrazione,   
           :ll_num_registrazione,   
           :ll_riga,   
           null,   
           :ls_cod_tipo_det_ven_rif,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           :is_riferimento_riga,   
           0,   
           0,   
           1,   
           0,   
           0,   
           0,   
           0,   
           :ls_cod_iva_rif,   
           :ls_cod_tipo_movimento_rif,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0 )  ;
	if sqlca.sqlcode <> 0 then
		is_messaggio = "Errore durante inserimento righe bolla uscita.~r~nErrore numero:" + string(sqlca.sqlcode) + "~r~nDettaglio errore = " + sqlca.sqlerrtext
		return -1
	end if
end if

for ll_i = 1 to ll_num_prodotti
	
	// leggo unità di misura e aliquota iva del prodotto
	select cod_misura_mag,
	       cod_iva
	into   :ls_cod_misura,
	       :ls_cod_iva
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :is_cod_prodotto[ll_i] ;
	if sqlca.sqlcode <> 0 then
		is_messaggio = "Errore durante la select da anag_prodotti~r~n" + sqlca.sqlerrtext
		return -1
	end if
			 
	// leggo il tipo movimento da tipi dettagli e nel caso cod_iva = null allora leggo anche l'aliquota iva
	// di default dal tipo dettaglio ( anche se in realtà non me ne frega niente perchè da queste bolle in
	// teoria non seguirà mai fattura )
	select cod_tipo_movimento,   
          cod_iva,
			 flag_tipo_det_ven
	into  :ls_cod_tipo_movimento,   
         :ls_cod_iva_det,
			:ls_flag_tipo_det_ven
   from  tab_tipi_det_ven
   where cod_azienda = :s_cs_xx.cod_azienda and  
         cod_tipo_det_ven = :is_cod_tipo_det_ven[ll_i] ;
	if sqlca.sqlcode <> 0 then
		is_messaggio = "Errore durante la ricerca tipo dettaglio vendita~r~n" + sqlca.sqlerrtext
		return -1
	else
		if isnull(ls_cod_iva) then
			ls_cod_iva = ls_cod_iva_det
		end if
	end if
	
	if ls_flag_tipo_det_ven = "M" and isnull(ls_cod_tipo_movimento) then
		is_messaggio = "Attenzione: è stato configurato in maniera non corretta il tipo dettaglio vendite.~r~nIl tipo dettaglio è di un prodotto a magazzino ma non si è specificato il relativo tipo movimento di magazzino!~r~nOperazione annullata"
		return -1
	end if

	// nel caso in cui il fornitore sia esente iva
	select cod_iva
	into   :ls_cod_iva_esente
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_fornitore = :fs_cod_fornitore;
	if sqlca.sqlcode <> 0 then
		is_messaggio = "Errore durante la select da  anag_fornitori~r~n" + sqlca.sqlerrtext
		return -1
	else
		if not isnull(ls_cod_iva_esente) then ls_cod_iva = ls_cod_iva_esente
	end if
	
			 
	ll_riga = ll_riga + 10			 
	INSERT INTO det_bol_ven  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_bol_ven,   
           cod_prodotto,   
           cod_tipo_det_ven,   
           cod_deposito,   
           cod_ubicazione,   
           cod_lotto,   
           progr_stock,   
           data_stock,   
           cod_misura,   
           des_prodotto,   
           quan_consegnata,   
           prezzo_vendita,   
           fat_conversione_ven,   
           sconto_1,   
           sconto_2,   
           provvigione_1,   
           provvigione_2,   
           cod_iva,   
           cod_tipo_movimento,   
           num_registrazione_mov_mag,   
           nota_dettaglio,   
           anno_registrazione_ord_ven,   
           num_registrazione_ord_ven,   
           prog_riga_ord_ven,   
           anno_commessa,   
           num_commessa,   
           cod_centro_costo,   
           sconto_3,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           anno_registrazione_mov_mag,   
           anno_reg_bol_acq,   
           num_reg_bol_acq,   
           prog_riga_bol_acq,   
           anno_reg_des_mov,   
           num_reg_des_mov,   
           cod_versione,   
           num_confezioni,   
           num_pezzi_confezione )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ll_anno_registrazione,   
           :ll_num_registrazione,   
           :ll_riga,   
           :is_cod_prodotto[ll_i],   
           :is_cod_tipo_det_ven[ll_i],   
           :is_cod_deposito[ll_i],   
           :is_cod_ubicazione[ll_i],   
           :is_cod_lotto[ll_i],   
           :il_prog_stock[ll_i],   
           :idt_data_stock[ll_i],   
           :ls_cod_misura,   
           null,   
           :id_quantita[ll_i],   
           0,   
           1,   
           0,   
           0,   
           0,   
           0,   
           :ls_cod_iva,   
           :ls_cod_tipo_movimento,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0 )  ;	
	if sqlca.sqlcode <> 0 then
		is_messaggio = "Errore durante inserimento righe bolla uscita.~r~nErrore numero:" + string(sqlca.sqlcode) + "~r~nDettaglio errore = " + sqlca.sqlerrtext
		return -1
	end if

	if ls_flag_tipo_det_ven = "M" then   // solo se tipo det = prodotti magazzino
		update anag_prodotti  
			set quan_in_spedizione = quan_in_spedizione + :id_quantita[ll_i]
		 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :is_cod_prodotto[ll_i];
		
		if sqlca.sqlcode = -1 then
			is_messaggio = "Si è verificato un errore in fase di aggiornamento magazzino." + sqlca.sqlerrtext
			return -1
		end if

		update stock
			set quan_in_spedizione = quan_in_spedizione + :id_quantita[ll_i]
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_prodotto = :is_cod_prodotto[ll_i] and
				 cod_deposito = :is_cod_deposito[ll_i] and
				 cod_ubicazione = :is_cod_ubicazione[ll_i] and
				 cod_lotto = :is_cod_lotto[ll_i] and
				 data_stock = :idt_data_stock[ll_i] and
				 prog_stock = :il_prog_stock[ll_i];
		
		if sqlca.sqlcode = -1 then
			is_messaggio = "Si è verificato un errore in fase di aggiornamento stock." + sqlca.sqlerrtext
			return -1
		end if

		ls_cod_deposito_merce[1] = is_cod_deposito[ll_i]
		ls_cod_ubicazione[1] = is_cod_ubicazione[ll_i]
		ls_cod_lotto[1] = is_cod_lotto[ll_i]
		ldt_data_stock[1] = idt_data_stock[ll_i]
		ll_prog_stock[1] = il_prog_stock[ll_i]
		
		// -----------------------------------  assegno il deposito corretto --------------------------------------------		

		ll_cont_mov = 0
		declare cu_movimenti dynamic cursor for sqlsa;
		
		ls_sql = "select cod_deposito, flag_fornitore, cod_fornitore, ordinamento from det_tipi_movimenti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_movimento = '" + ls_cod_tipo_movimento + "' order by ordinamento ASC" 
		prepare sqlsa from :ls_sql;
		open dynamic cu_movimenti;
		do while 1=1
			fetch cu_movimenti INTO :ls_cod_deposito_mov, :ls_flag_fornitore, :ls_cod_fornitore_mov, :ll_prog_ordinamento ;
			if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
			ll_cont_mov ++
			setnull(ls_cod_fornitore[ll_cont_mov])
			setnull(ls_cod_cliente[ll_cont_mov])
			if ll_cont_mov > 1 then
				setnull(ls_cod_ubicazione[ll_cont_mov])
				setnull(ls_cod_lotto[ll_cont_mov])
				setnull(ldt_data_stock[ll_cont_mov])
				setnull(ll_prog_stock[ll_cont_mov])
				setnull(ls_cod_fornitore[ll_cont_mov])
				setnull(ls_cod_deposito_merce[ll_cont_mov])
			end if
			if ls_flag_fornitore = "S" then
				if not isnull(ls_cod_fornitore_mov) and ls_cod_fornitore_mov <> fs_cod_fornitore then g_mb.messagebox("Creazione Bolle Terzisti","Attenzione il fornitore indicato nel tipo movimento è diverso dal fornitore ~r~nselezionato durante la procedura di generazione bolle~r~nIl fornitore selezionato dall'utente viene sostituito al fornitore indicato nel tipo movimento")
				ls_cod_fornitore[ll_cont_mov] = fs_cod_fornitore
				setnull(ls_cod_deposito_fornitore)
				select cod_deposito
				into   :ls_cod_deposito_fornitore
				from   anag_fornitori
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_fornitore = :ls_cod_fornitore[ll_cont_mov] ;
				if sqlca.sqlcode <> 0 then
					is_messaggio = "Errore durante la ricerca dati fornitore su tabella ANAG_FORNITORI~r~nDettaglio errore "+sqlca.sqlerrtext
					return -1
				end if
				ls_cod_deposito_merce[ll_cont_mov] = ls_cod_deposito_fornitore 
				ls_cod_ubicazione[ll_cont_mov] = ls_cod_fornitore[ll_cont_mov]
				if isnull(ls_cod_deposito_merce[ll_cont_mov])  then
					ls_cod_deposito_merce[ll_cont_mov] = ls_cod_deposito_mov
					if isnull(ls_cod_deposito_merce[ll_cont_mov]) then
						ls_cod_deposito_merce[ll_cont_mov] = is_cod_deposito[ll_i]
					end if
				end if
			end if
		loop
		close cu_movimenti;
		if ll_cont_mov = 0 or isnull(ll_cont_mov) then
			is_messaggio = "Attenzione il movimento non è configurato correttamente: verificare tipi movimenti; procedura abbandonata "
			return -1
		end if
		
		// --------------------------------------------------------------------------------------------------------------
		
		if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, is_cod_prodotto[ll_i], ls_cod_deposito_merce[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], ref ll_anno_reg_des_mov, ref ll_num_reg_des_mov) = -1 then
			is_messaggio = "Si è verificato un errore in fase di creazione destinazioni movimenti."
			return -1
		end if

		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, is_cod_prodotto[ll_i]) = -1 then
			return -1
		end if
		update det_bol_ven  
		  set anno_reg_des_mov = :ll_anno_reg_des_mov,   
				num_reg_des_mov = :ll_num_reg_des_mov  
		where cod_azienda = :s_cs_xx.cod_azienda  AND  
				anno_registrazione = :ll_anno_registrazione  AND  
				num_registrazione = :ll_num_registrazione  AND  
				prog_riga_bol_ven = :ll_riga  ;
		if sqlca.sqlcode <> 0 then
			is_messaggio = "Errore in aggiornamento destinazione stock su dettaglio bolla~r~n" + sqlca.sqlerrtext
			return -1
		end if
	end if
	ll_prog_riga ++
	fl_prog_riga_ven[ll_prog_riga] = ll_riga
next


if len(is_riferimento_riga_fine) > 0  and ls_flag_riferimento = "D" then
	
  ll_riga = ll_riga + 10
  INSERT INTO det_bol_ven  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_bol_ven,   
           cod_prodotto,   
           cod_tipo_det_ven,   
           cod_deposito,   
           cod_ubicazione,   
           cod_lotto,   
           progr_stock,   
           data_stock,   
           cod_misura,   
           des_prodotto,   
           quan_consegnata,   
           prezzo_vendita,   
           fat_conversione_ven,   
           sconto_1,   
           sconto_2,   
           provvigione_1,   
           provvigione_2,   
           cod_iva,   
           cod_tipo_movimento,   
           num_registrazione_mov_mag,   
           nota_dettaglio,   
           anno_registrazione_ord_ven,   
           num_registrazione_ord_ven,   
           prog_riga_ord_ven,   
           anno_commessa,   
           num_commessa,   
           cod_centro_costo,   
           sconto_3,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           anno_registrazione_mov_mag,   
           anno_reg_bol_acq,   
           num_reg_bol_acq,   
           prog_riga_bol_acq,   
           anno_reg_des_mov,   
           num_reg_des_mov,   
           cod_versione,   
           num_confezioni,   
           num_pezzi_confezione )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ll_anno_registrazione,   
           :ll_num_registrazione,   
           :ll_riga,   
           null,   
           :ls_cod_tipo_det_ven_rif,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,   
           1,   
           0,   
           0,   
           0,   
           0,   
           :ls_cod_iva_rif,   
           :ls_cod_tipo_movimento_rif,   
           null,   
           :is_riferimento_riga_fine,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0 )  ;
	if sqlca.sqlcode <> 0 then
		is_messaggio = "Errore durante inserimento righe bolla uscita.~r~nErrore numero:" + string(sqlca.sqlcode) + "~r~nDettaglio errore = " + sqlca.sqlerrtext
		return -1
	end if
end if

luo_calcolo = create uo_calcola_documento_euro

if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",is_messaggio) <> 0 then
	destroy luo_calcolo
	return -1
end if

destroy luo_calcolo

fl_anno_bolla_ven = ll_anno_registrazione
fl_num_bolla_ven  = ll_num_registrazione

// -------------------------------------- SE LA BOLLA E' DA CONFERMA ALLORA ESEGUO   ---------------------------------
// -------------------------------------- ASSEGNAZIONE DEL NUMERO FISCALE ALLA BOLLA ---------------------------------

if fs_flag_conferma = "S" then

	select anag_depositi.cod_documento,   
          anag_depositi.numeratore_documento  
   into   :ls_cod_documento,   
          :ls_numeratore_documento  
   from   anag_depositi  
   where  anag_depositi.cod_azienda = :s_cs_xx.cod_azienda and
          anag_depositi.cod_deposito = :ls_cod_deposito;
	
	if sqlca.sqlcode <> 0 then
		is_messaggio = "Errore durante la lettura dell'Anagrafica Depositi."
		return -1
	end if

	if isnull(ls_cod_documento) then
		select tab_tipi_bol_ven.cod_documento,   
             tab_tipi_bol_ven.numeratore_documento
	   into   :ls_cod_documento,   
             :ls_numeratore_documento  
      from   tab_tipi_bol_ven
      where  cod_azienda = :s_cs_xx.cod_azienda and
             cod_tipo_bol_ven = :fs_cod_tipo_bol_ven;

		if sqlca.sqlcode <> 0 then
			is_messaggio = "Errore durante la lettura della tabella tipi bolle."
			return -1
		end if
	end if

	select num_documento
	into   :ll_num_documento  
	from   numeratori  
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_documento = :ls_cod_documento and
			 numeratore_documento = :ls_numeratore_documento and
			 anno_documento = :ll_anno_esercizio;

	if sqlca.sqlcode = -1 then
		is_messaggio = "Errore durante la lettura della tabella numeratori."
		return -1
	elseif isnull(ll_num_documento) then
		ll_num_documento  = 1

		insert into numeratori 
						(cod_azienda,   
						 cod_documento,   
						 numeratore_documento,   
						 anno_documento,   
						 num_documento)  
		values 		(:s_cs_xx.cod_azienda,   
						 :ls_cod_documento,   
						 :ls_numeratore_documento,   
						 :ll_anno_esercizio,   
						 :ll_num_documento);

		if sqlca.sqlcode <> 0 then
			is_messaggio = "Errore durante l'inserimento del numeratore.~r~n" + sqlca.sqlerrtext
			return -1
		end if
	elseif not isnull(ll_num_documento) then
		ll_num_documento ++

		update numeratori
		set    num_documento = :ll_num_documento  
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_documento = :ls_cod_documento and
				 numeratore_documento = :ls_numeratore_documento;
	
		if sqlca.sqlcode <> 0 then
			is_messaggio = "Errore durante l'aggiornamento della tabella numeratori."
			return -1
		end if
	end if

	update tes_bol_ven
	set    cod_documento = :ls_cod_documento,   
		    numeratore_documento = :ls_numeratore_documento,   
		    anno_documento = :ll_anno_esercizio,   
		    num_documento = :ll_num_documento,   
		    data_bolla = :ldt_oggi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;

	if sqlca.sqlcode <> 0 then
		is_messaggio = "Errore durante l'aggiornamento della testata bolla."
		return -1
	end if

	// -------------------------------------- CONFERMA DELLA BOLLA DI USCITA ---------------------------------------------


	ls_tes_tabella = "tes_bol_ven"
	ls_det_tabella = "det_bol_ven"
	ls_prog_riga = "prog_riga_bol_ven"
	ls_quantita = "quan_consegnata"
	
	if f_conferma_mov_mag(ls_tes_tabella, &
								 ls_det_tabella, &
								 ls_prog_riga, &
								 ls_quantita, &
								 ll_anno_registrazione, &
								 ll_num_registrazione, &
								 ls_messaggio) = -1 then
		is_messaggio = "Errore durante la conferma della bolla~r~nDettaglio errore:" + ls_messaggio + "~r~n"
		return -1
	end if


end if

return 0
end function

on uo_genera_doc_produzione.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_genera_doc_produzione.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

