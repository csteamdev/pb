﻿$PBExportHeader$uo_cs_ws.sru
$PBExportComments$Generated Web service object
forward
global type uo_cs_ws from nonvisualobject
end type
end forward

global type uo_cs_ws from nonvisualobject descriptor "PB_ObjectCodeAssistants" = "{1E00F051-675A-11D2-BCA5-000086095DDA}" 
end type
global uo_cs_ws uo_cs_ws

type variables
public:
long	il_anno_documento=0, il_num_documento=0

protected:
string s_cs_xx_chiave_root = "HKEY_LOCAL_MACHINE\Software\Consulting&Software\"
string s_cs_xx_profilodefault=""
string s_cs_xx_cod_azienda =""
string s_cs_xx_db_funzioni_formato_data ="", s_cs_xx_db_funzioni_oggi="", s_cs_xx_db_funzioni_data_neutra

end variables

forward prototypes
public function integer uof_get_transactioninformation () throws exception
public function exception uof_exception (long fl_codice_errore, string fs_messaggio)
public function uo_return_base uof_error (long fl_codice_errore, string fs_messaggio)
public function uo_return_base uof_esempio_chiamata ()
public function uo_return_base uof_test_validazione (string cod_prodotto_finito, string cod_valore, string valore_effettivo, string valido) throws exception
public function uo_return_base uof_get_azienda_default () throws exception
public function uo_return_base uof_valida_valori (ref str_return_valida_valori auo_valori) throws exception
public function uo_return_base uof_test_carica_sfuso (string as_cod_cliente, string as_tipo_gestione, string as_cod_prodotto_modello, string as_cod_verniciatura, string as_cod_comando, string as_cod_tipo_supporto, decimal ad_dim_x, decimal ad_dim_y, decimal ad_dim_z, decimal ad_dim_t, decimal ad_quan_vendita) throws exception
public function uo_return_base uof_carica_sfuso (str_carica_sfuso_input astr_carica_sfuso_input, ref str_carica_sfuso_output astr_carica_sfuso_output) throws exception
public function integer uof_get_variabile_valore (str_variabili astr_variabili[], string as_nome_variabile, ref string as_valore, ref string as_messaggio)
public function integer uof_leggi_variabili (str_variabili_input astr_variabili_input[], ref str_variabili astr_variabili[])
public function integer uof_get_variabile_valore (str_variabili astr_variabili[], string as_nome_variabile, ref decimal ad_valore, ref string as_messaggio)
public function integer uof_disconnect_db () throws exception
public function integer uof_get_cambio_vendita (datetime ad_data_riferimento, string as_cod_valuta, ref decimal ad_cambio_ven, ref string as_errore)
public function uo_return_base uof_carica_confezionato (str_carica_confezionato_input astr_carica_confezionato_input, ref str_carica_confezionato_output astr_carica_confezionato_output) throws exception
public function integer uof_calcola_condizioni (string as_tipo_gestione, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, string as_cod_cliente, string as_cod_tipo_listino_prodotto, string as_cod_valuta, decimal ad_cambio_ven, string as_cod_lingua, string as_cod_agente_1, datetime adt_data_riferimento, string as_cod_prodotto, string as_cod_versione, decimal ad_quan_vendita, decimal ad_dim_1, decimal ad_dim_2, ref decimal ad_prezzo_vendita_mag, ref decimal ad_prezzo_vendita_ven, ref decimal ad_fat_conversione_ven, ref decimal ad_sconto_1, ref decimal ad_sconto_2, ref decimal ad_provvigione_1, ref decimal ad_provvigione_2, ref string as_errore)
public function integer uof_optionals (string as_tipo_documento, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, ref str_optionals astr_righe_optionals[], ref string as_errore)
protected function integer uof_crea_note (string as_tipo_documento, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, ref string as_nota_prodotto, ref string as_nota_dettaglio, ref string as_errore)
public function uo_return_base uof_connect_db () throws exception
public function long uof_crea_ordine (str_carica_sfuso_input astr_carica_sfuso_input, ref str_carica_sfuso_output astr_carica_sfuso_output, ref uo_return_base auo_return_base) throws exception
end prototypes

public function integer uof_get_transactioninformation () throws exception;STRING l_ErrorStrings[1],ls_dbparm,ls_logpass, ls_ini_section, ls_db
integer li_risposta

li_risposta = registryget(s_cs_xx_chiave_root + "profilows2", "numero", s_cs_xx_profilodefault)
if li_risposta = -1 then
	throw uof_exception(-1, "Profilo default registro non impostato.")
end if

ls_ini_section = "database_" + s_cs_xx_profilodefault

li_risposta = registryget(s_cs_xx_chiave_root + ls_ini_section, "servername", sqlca.ServerName)
if li_risposta = -1 then
	uof_error(-1, "uof_crea_transazione(): Errore nella lettura del parametro di registro: SERVERNAME.")
	return -1
end if

li_risposta = registryget(s_cs_xx_chiave_root + ls_ini_section, "logid", sqlca.LogId)
if li_risposta = -1 then
	uof_error(-1,"uof_crea_transazione(): Errore nella lettura del parametro di registro: LOGID.")
	return -1
end if

li_risposta = registryget(s_cs_xx_chiave_root + ls_ini_section, "logpass", ls_logpass)
if li_risposta = -1 then
	uof_error(-1,"uof_crea_transazione(): Errore nella lettura del parametro di registro: LOGPASS.")
	return -1
end if

li_risposta = registryget(s_cs_xx_chiave_root + ls_ini_section, "dbms", sqlca.DBMS)
if li_risposta = -1 then
	uof_error(-1,"uof_crea_transazione(): Errore nella lettura del parametro di registro: DBMS.")
	return -1
end if

li_risposta = registryget(s_cs_xx_chiave_root + ls_ini_section, "database", sqlca.Database)
if li_risposta = -1 then
	uof_error(-1, "uof_crea_transazione(): Errore nella lettura del parametro di registro: DATABASE." )
	return -1
end if

li_risposta = registryget(s_cs_xx_chiave_root + ls_ini_section, "userid", sqlca.UserId)
if li_risposta = -1 then
	uof_error(-1,"uof_crea_transazione(): Errore nella lettura del parametro di registro: USERID.")
	return -1
end if

li_risposta = registryget(s_cs_xx_chiave_root + ls_ini_section, "dbpass", sqlca.DBPass)
if li_risposta = -1 then
	uof_error(-1,"uof_crea_transazione(): Errore nella lettura del parametro di registro: DBPASS.")
	return -1
end if

li_risposta = registryget(s_cs_xx_chiave_root + ls_ini_section, "dbparm", ls_dbparm)
if li_risposta = -1 then
	uof_error(-1,"uof_crea_transazione(): Errore nella lettura del parametro di registro: DBPARM.")
	return -1
end if

li_risposta = registryget(s_cs_xx_chiave_root+ ls_ini_section, "enginetype", ls_db)
if ls_db = "SYBASE_ASE" then
	if pos( lower(ls_dbparm), "appname") = 0 then
		ls_dbparm += ",appname='WebServicePBApp'"
	end if
end if

sqlca.DBParm = ls_dbparm

li_risposta = registryget(s_cs_xx_chiave_root + ls_ini_section, "lock", sqlca.Lock)
if li_risposta = -1 then
	uof_error(-1,"uof_crea_transazione(): Errore nella lettura del parametro di registro: LOCK.")
	return -1
end if


if sqlca.DBMS <> "ODBC" then
	n_cst_crypto lnv_crypt
	lnv_crypt = CREATE n_cst_crypto
	
	if isnull(ls_logpass) or ls_logpass="" then
		
		uof_error(-1,"uof_crea_transazione(): nel registro manca la password per la connessione al DB")
		return -1
		
	end if
	
	//guo_functions.uof_log("Password Prima." + ls_logpass)

	if lnv_crypt.DecryptData(ls_logpass, ls_logpass) < 0 then
		ls_logpass = ""
	end if

	//guo_functions.uof_log("Password Dopo." + ls_logpass)

	sqlca.LogPass = ls_logpass
//  per DEMOTENDE
//	sqlca.LogPass = "demotende"

//  per LUPO
//	sqlca.LogPass = "DEKadm13*"
// 	sqlca.LogPass = "luposolution"

//  per MArinello
//	sqlca.LogPass = "M@r1nell0"
// 	sqlca.LogPass = "marinello"

//  per Gibus
//	sqlca.LogPass = "Ap1c3.15CS"

//	per Zanzar
	sqlca.LogPass = "CS20.z@nz"

//	per KITA
//	sqlca.LogPass = "K1taCSt3am!"

//	per PRACAL
//	sqlca.LogPass = "Pr@ca1-19"

//	per ALUSISTEMI
//	sqlca.LogPass = "csteam"
//	sqlca.LogPass = "alusistemi"

//	per zanzar
//	sqlca.LogPass = "zanzar"

DESTROY lnv_crypt

end if

// carico il parametro per la definizionedel formato data corretto

if registryget(s_cs_xx_chiave_root+ ls_ini_section, "enginetype", ls_db) < 0 then
	uof_error(-1,"uof_crea_transazione(): Parametro di Registro 'enginetype' non trovato o non impostato.")
	return -1
end if

choose case ls_db
	case "SYBASE_ASA"
		s_cs_xx_db_funzioni_oggi = "today()"
		s_cs_xx_db_funzioni_formato_data = "yyyymmdd"
		s_cs_xx_db_funzioni_data_neutra = "01/01/2000"
	case "ORACLE"
		s_cs_xx_db_funzioni_oggi = "to_date(sysdate)"
		s_cs_xx_db_funzioni_formato_data = "DD-MMM-YY"
		s_cs_xx_db_funzioni_data_neutra = "01/01/2000"
	case "SYBASE_ASE", "MSSQL"
		s_cs_xx_db_funzioni_oggi = "'" + string(today(),"yyyy") + string(today(),"mm") + string(today(),"dd") + "'"
		s_cs_xx_db_funzioni_formato_data = "yyyymmdd"	
		s_cs_xx_db_funzioni_data_neutra = "01/01/2000"
	case else
		uof_error(-1,"uof_crea_transazione(): Parametro di Registro 'enginetype' non trovato o non impostato.")
		return -1
end  choose


RETURN 0
end function

public function exception uof_exception (long fl_codice_errore, string fs_messaggio);Exception     ex
ex = create Exception
ex.text = "Errore codice " + string(fl_codice_errore) + " " + fs_messaggio
return ex

end function

public function uo_return_base uof_error (long fl_codice_errore, string fs_messaggio);uo_return_error luo_err
uo_return_base luo_base

luo_err = create uo_return_error
luo_err.errorcode  =fl_codice_errore
luo_err.errortext  = fs_messaggio

luo_base = create uo_return_base
luo_base.ib_error = true
luo_base.iuo_error = luo_err

disconnect;

return luo_base

end function

public function uo_return_base uof_esempio_chiamata ();try 

//uof_connect_db()

catch (Exception ex)
	return uof_error(-1, ex.text)
end try

return uo_return_base
end function

public function uo_return_base uof_test_validazione (string cod_prodotto_finito, string cod_valore, string valore_effettivo, string valido) throws exception;try

str_return_valida_valori luo_valori


luo_valori.atr_valida_valori[1].cod_prodotto_finito = cod_prodotto_finito
luo_valori.atr_valida_valori[1].cod_valore = cod_valore
luo_valori.atr_valida_valori[1].valore = valore_effettivo
luo_valori.atr_valida_valori[1].valido = "0"


return uof_valida_valori( luo_valori )

catch (Exception ex)
	return uof_error(-1, ex.text)
end try

return uo_return_base

end function

public function uo_return_base uof_get_azienda_default () throws exception;STRING l_ErrorStrings[1],ls_dbparm,ls_logpass, ls_ini_section
integer li_risposta

try
	li_risposta = registryget(s_cs_xx_chiave_root + "profilows2", "numero", s_cs_xx_profilodefault)
	if li_risposta = -1 then
		return uof_error(-1, "uof_set_azienda_default(). Errore in accesso al profilo di default per Webservices")
	end if
	
	ls_ini_section = "applicazione_" + s_cs_xx_profilodefault
		
	li_risposta = registryget(s_cs_xx_chiave_root + ls_ini_section, "AZI", s_cs_xx_cod_azienda)
	if li_risposta = -1 then
		return uof_error(-1, "uof_crea_transazione(): Errore nella lettura del parametro di registro: AZI.")
	end if

catch (Exception ex)
	return uof_error(-1, ex.text)
end try

return uo_return_base

end function

public function uo_return_base uof_valida_valori (ref str_return_valida_valori auo_valori) throws exception;string ls_testo_formula[],ls_flag_tipo_dato[], ls_errore, ls_des_variabile, ls_str1, ls_str2
long ll_max, ll_i, ll_y
decimal ld_risultato
uo_functions	luo_functions
uo_formule_calcolo luo_formule
uo_return_base luo_return_base

//luo_return_base = create uo_return_base

id_sessione = "WS" + string(date(now()),"yyyy-mm-dd") + "-" + string(time(now()),"hh-mm-ss")

try 
	
luo_return_base = uof_connect_db()		
if luo_return_base.ib_error then
	return luo_return_base
end if

uof_get_azienda_default()

guo_functions = create uo_functions
guo_functions.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
guo_functions.s_cs_xx_db_funzioni_formato_data = s_cs_xx_db_funzioni_formato_data

guo_functions.uof_get_parametro_azienda( "LWS", gb_log)

guo_functions.uof_log("Avvio funzione uof_valida_valori()")


ll_max = upperbound(auo_valori.atr_valida_valori[] )

// creazione variabili globali
guo_des_tabella = create uo_des_tabella
guo_des_tabella.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda

for ll_i = 1 to ll_max
	
	ls_str1 = auo_valori.atr_valida_valori[ll_i].valore
	ls_str2 = auo_valori.atr_valida_valori[ll_i].cod_valore
	
	guo_functions.uof_log("Validazione Valore" + ls_str2 + " - " + ls_str1)
	
	// carico testo formula e tipo di dato della variabile
	select 	des_variabile
	into		:ls_des_variabile
	from 		tab_des_variabili
	where 	cod_azienda = :s_cs_xx_cod_azienda and
				cod_prodotto = :auo_valori.atr_valida_valori[ll_i].cod_prodotto_finito and
				cod_variabile = :auo_valori.atr_valida_valori[ll_i].cod_valore ;
	if sqlca.sqlcode = 100 then
		// la variabile non esiste
		return uof_error(-1, "Variabile " + auo_valori.atr_valida_valori[ll_i].cod_valore + " non abilitata per il prodotto " +auo_valori.atr_valida_valori[ll_i].cod_prodotto_finito + " nella tabella tab_des_variabili" )
	elseif sqlca.sqlcode < 0 then
		return uof_error(-1, "Errore in ricerca formula per il prodotto "+auo_valori.atr_valida_valori[ll_i].cod_prodotto_finito+" e la variabile " +auo_valori.atr_valida_valori[ll_i].cod_valore + ".Dettaglio errore " + sqlca.sqlerrtext)
	end if
	
	
	
	select 	tab_formule_db.testo_formula, 
			 	tab_variabili_formule.flag_tipo_dato
	into		:ls_testo_formula[ll_i],
				:ls_flag_tipo_dato[ll_i]
	from 		tab_des_variabili
				left outer join tab_variabili_formule on tab_des_variabili.cod_azienda = tab_variabili_formule.cod_azienda and
														 tab_des_variabili.cod_variabile = tab_variabili_formule.cod_variabile
				left outer join tab_formule_db 		on tab_des_variabili.cod_azienda = tab_formule_db.cod_azienda and
														 tab_des_variabili.cod_formula_validazione = tab_formule_db.cod_formula
	where 	tab_des_variabili.cod_azienda = :s_cs_xx_cod_azienda and
				tab_des_variabili.cod_prodotto = :auo_valori.atr_valida_valori[ll_i].cod_prodotto_finito and
				tab_des_variabili.cod_variabile = :auo_valori.atr_valida_valori[ll_i].cod_valore;
				
	if sqlca.sqlcode = 100 or isnull(ls_testo_formula[ll_i]) or len( trim( ls_testo_formula[ll_i] )) < 1   then
		// nessuna formula; valore valido a prescindere.
		auo_valori.atr_valida_valori[ll_i].valido = "1"
		guo_functions.uof_log("Valore" + ls_str2 + " - Nessuna formula oppure valore non codificato")
		continue
	elseif sqlca.sqlcode < 0 then
		guo_functions.uof_log("Errore in ricerca formula per il prodotto "+auo_valori.atr_valida_valori[ll_i].cod_prodotto_finito +" e la variabile " +auo_valori.atr_valida_valori[ll_i].cod_valore + ".Dettaglio errore " + sqlca.sqlerrtext)
		return uof_error(-1, "Errore in ricerca formula per il prodotto "+auo_valori.atr_valida_valori[ll_i].cod_prodotto_finito +" e la variabile " +auo_valori.atr_valida_valori[ll_i].cod_valore + ".Dettaglio errore " + sqlca.sqlerrtext)
	end if	
	
next

luo_functions = create uo_functions
luo_functions.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda

luo_formule = create uo_formule_calcolo
luo_formule.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda

for ll_i = 1 to upperbound(ls_testo_formula)
	
	guo_functions.uof_log("uof_valida_valori(): Valuto la formula NR " + guo_functions.uof_format(ll_i) + " - " +guo_functions.uof_format(ls_testo_formula[ll_i]))
	
	// non aveva la formula, quindi è valido a prescindere
	if auo_valori.atr_valida_valori[ll_i].valido = "1" then 
		guo_functions.uof_log("uof_valida_valori(): formula NR " + guo_functions.uof_format(ll_i) + " vuota, quindi valida a priori")
		continue
	end if
	
	for ll_y = 1 to ll_max
		
		guo_functions.uof_log("uof_valida_valori(): Cod Valore = " + guo_functions.uof_format(auo_valori.atr_valida_valori[ll_y].cod_valore))
		guo_functions.uof_log("uof_valida_valori(): Valore = " + guo_functions.uof_format(auo_valori.atr_valida_valori[ll_y].valore))
		guo_functions.uof_log("uof_valida_valori(): Tipo Dato = " + ls_flag_tipo_dato[ll_y])
		choose case ls_flag_tipo_dato[ll_y]
		case "N"		// numerico
			
			string ls_1, ls_2
			ls_1 = auo_valori.atr_valida_valori[ll_y].cod_valore
			ls_2 =  auo_valori.atr_valida_valori[ll_y].valore
			
			luo_functions.uof_replace_string(auo_valori.atr_valida_valori[ll_y].valore, ",", ".")		

			guo_functions.uof_log("uof_valida_valori(): formula PRIMA del replace" + guo_functions.uof_format(ls_testo_formula[ll_i]))
			luo_functions.uof_replace_string(ls_testo_formula[ll_i], ls_1, ls_2)
			guo_functions.uof_log("uof_valida_valori(): formula DOPO del replace" + guo_functions.uof_format(ls_testo_formula[ll_i]))
			

		case "S"		// string
			
			guo_functions.uof_log("uof_valida_valori(): formula PRIMA del replace" + guo_functions.uof_format(ls_testo_formula[ll_i]))
			luo_functions.uof_replace_string(ls_testo_formula[ll_i], auo_valori.atr_valida_valori[ll_y].cod_valore, "'" + auo_valori.atr_valida_valori[ll_y].valore + "'" )
			guo_functions.uof_log("uof_valida_valori(): formula DOPO del replace" + guo_functions.uof_format(ls_testo_formula[ll_i]))

		end choose			
	next
	
	
	// chiamata alla funzione di validazione formula
	if luo_formule.uof_calcola_formula_testo( ls_testo_formula[ll_i], ld_risultato, ls_errore) < 0 then
		return uof_error(-1, "Errore nel calcolo della formula: " + ls_testo_formula[ll_i] + ". Dettaglio errore: " + ls_errore )
	end if
	
	if ld_risultato > 0 then
		auo_valori.atr_valida_valori[ll_i].valido = "1"
	else
		auo_valori.atr_valida_valori[ll_i].valido = "0"
	end if
	
next

guo_functions.uof_log("uof_valida_valori(): fine ciclo validazione formule")

catch (Exception ex)
	return uof_error(-1, ex.text)
	
finally
	disconnect;
end try

//return uo_return_base
end function

public function uo_return_base uof_test_carica_sfuso (string as_cod_cliente, string as_tipo_gestione, string as_cod_prodotto_modello, string as_cod_verniciatura, string as_cod_comando, string as_cod_tipo_supporto, decimal ad_dim_x, decimal ad_dim_y, decimal ad_dim_z, decimal ad_dim_t, decimal ad_quan_vendita) throws exception;//try
//
	str_carica_sfuso_input astr_carica_sfuso_input
	str_carica_sfuso_output astr_carica_sfuso_output
//	
//	astr_carica_sfuso_input.as_cod_cliente = as_cod_cliente
//	astr_carica_sfuso_input.as_tipo_gestione = as_tipo_gestione
//	astr_carica_sfuso_input.as_cod_prodotto_modello = as_cod_prodotto_modello
//	astr_carica_sfuso_input.as_cod_verniciatura = as_cod_verniciatura
//	astr_carica_sfuso_input.as_cod_comando = as_cod_comando
//	astr_carica_sfuso_input.as_cod_tipo_supporto = as_cod_tipo_supporto
//	astr_carica_sfuso_input.ad_dim_x = ad_dim_x
//	astr_carica_sfuso_input.ad_dim_y = ad_dim_y
//	astr_carica_sfuso_input.ad_dim_z = ad_dim_z
//	astr_carica_sfuso_input.ad_dim_t = ad_dim_t
//	astr_carica_sfuso_input.ad_quan_vendita = ad_quan_vendita
//	
	return uof_carica_sfuso( astr_carica_sfuso_input, astr_carica_sfuso_output )
//
//	catch (Exception ex)
//		return uof_error(-1, ex.text)
//end try
//	

end function

public function uo_return_base uof_carica_sfuso (str_carica_sfuso_input astr_carica_sfuso_input, ref str_carica_sfuso_output astr_carica_sfuso_output) throws exception;long ll_ret
uo_return_base luo_return_base

try
	luo_return_base = uof_connect_db()		
	
	uof_get_azienda_default()
	
	guo_functions = create uo_functions
	guo_functions.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
	guo_functions.s_cs_xx_db_funzioni_formato_data = s_cs_xx_db_funzioni_formato_data
	
	guo_functions.uof_get_parametro_azienda( "LWS", gb_log)
	
	guo_functions.uof_log("Inizio procedura uof_carica_sfuso().")
	
	ll_ret = uof_crea_ordine(astr_carica_sfuso_input, astr_carica_sfuso_output, luo_return_base)
	if ll_ret < 0 then
		if luo_return_base.iuo_error.errorcode < 0 then 
			guo_functions.uof_log("Uscita per errore " + guo_functions.uof_format(luo_return_base.iuo_error.errorcode) + " Messaggio " + guo_functions.uof_format(luo_return_base.iuo_error.errortext))
			return uof_error(luo_return_base.iuo_error.errorcode,luo_return_base.iuo_error.errortext)
		end if
	end if

catch (Exception ex)
	guo_functions.uof_log("ECCEZIONE: " + guo_functions.uof_format(ex.getmessage() ))
	rollback;
	uof_disconnect_db()
	return uof_error(-1, ex.text)
end try

guo_functions.uof_log("Fine procedura uof_carica_sfuso().")

destroy guo_functions

commit;

uof_disconnect_db()

return luo_return_base


end function

public function integer uof_get_variabile_valore (str_variabili astr_variabili[], string as_nome_variabile, ref string as_valore, ref string as_messaggio);long ll_num_variabili, ll_i

ll_num_variabili = upperbound(astr_variabili[])
as_valore = ""

for ll_i = 1 to ll_num_variabili
	
	if upper(as_nome_variabile) = upper(astr_variabili[ll_i].as_cod_variabile) and astr_variabili[ll_i].as_flag_datatype = "S" then
		as_valore =  astr_variabili[ll_i].as_string
		if isnull(as_valore) then as_valore =""
		guo_functions.uof_log("Variabile:" + as_nome_variabile + "  Valore:" + astr_variabili[ll_i].as_string )
		return 0
	elseif ll_i = ll_num_variabili then
		as_messaggio = "Nessun valore trovato per la variabile " + as_nome_variabile
		setnull(as_valore)
		return -1
	end if
next

return 0
		
	
end function

public function integer uof_leggi_variabili (str_variabili_input astr_variabili_input[], ref str_variabili astr_variabili[]);long ll_num_variabili, ll_i


ll_num_variabili = upperbound( astr_variabili_input[] )

for ll_i = 1 to ll_num_variabili
	
	
	
next

return 0
end function

public function integer uof_get_variabile_valore (str_variabili astr_variabili[], string as_nome_variabile, ref decimal ad_valore, ref string as_messaggio);long ll_num_variabili, ll_i

ll_num_variabili = upperbound(astr_variabili[])
ad_valore = 0

for ll_i = 1 to ll_num_variabili
	
	if upper(as_nome_variabile) = upper(astr_variabili[ll_i].as_cod_variabile) and astr_variabili[ll_i].as_flag_datatype = "N" then
		if isnull(astr_variabili[ll_i].ad_number) then astr_variabili[ll_i].ad_number = 0
		ad_valore =  astr_variabili[ll_i].ad_number
		if isnull(ad_valore) then ad_valore = 0
		guo_functions.uof_log("Variabile:" + as_nome_variabile + "  Valore:" + string(astr_variabili[ll_i].ad_number))
		return 0
	elseif ll_i = ll_num_variabili then
		as_messaggio = "Ness valore trovato per la variabile " + as_nome_variabile
		ad_valore = 0
		return -1
	end if
next

return 0
		
	
end function

public function integer uof_disconnect_db () throws exception;
disconnect using sqlca;


RETURN 0
end function

public function integer uof_get_cambio_vendita (datetime ad_data_riferimento, string as_cod_valuta, ref decimal ad_cambio_ven, ref string as_errore);string		ls_errore, ls_sql
datetime 	ldt_data_cambio
long 			ll_i, ll_ret
datastore 	lds_data


ad_cambio_ven = 1

select 	cambio_ven
into   	:ad_cambio_ven    
from   	tab_valute
where  	cod_azienda = :s_cs_xx_cod_azienda and 
       		cod_valuta = :as_cod_valuta;

if sqlca.sqlcode = 0 then
	
	ls_sql = "select cambio_ven from tab_cambi where cod_azienda = '" + s_cs_xx_cod_azienda + "' and cod_valuta = '" + as_cod_valuta + "' and data_cambio <= '" + string(ad_data_riferimento, s_cs_xx_db_funzioni_formato_data) + "' order by cod_azienda, cod_valuta, data_cambio desc "
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data,ls_sql , ls_errore)
	
	if ll_ret < 0 then
		as_errore = "uof_get_cambio_vendita() " + ls_errore
		return -1
	end if
	
	if ll_ret = 0 then 
		destroy lds_data
		return 0
	end if
	
	ad_cambio_ven = lds_data.getitemnumber(1,1)
	
end if

return 0
end function

public function uo_return_base uof_carica_confezionato (str_carica_confezionato_input astr_carica_confezionato_input, ref str_carica_confezionato_output astr_carica_confezionato_output) throws exception;long ll_i, ll_rows, ll_num_documento, ll_anno_documento,ll_ret
uo_return_base luo_return_base
str_carica_sfuso_output lstr_carica_sfuso_output

//try
	luo_return_base = create uo_return_base
	uof_connect_db()		
	uof_get_azienda_default()
	
	guo_functions = create uo_functions
	guo_functions.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
	guo_functions.s_cs_xx_db_funzioni_formato_data = s_cs_xx_db_funzioni_formato_data
	
	guo_functions.uof_get_parametro_azienda( "LWS", gb_log)
	
	
	guo_functions.uof_log("Inizio procedura uof_carica_confezionato().")
	
	ll_anno_documento = astr_carica_confezionato_input.al_anno_documento 
	ll_num_documento = astr_carica_confezionato_input.al_num_documento 
	
	if not isnull(ll_anno_documento) and ll_anno_documento > 0 and not isnull(ll_num_documento) and ll_num_documento > 0 then
		il_anno_documento = ll_anno_documento
		il_num_documento = ll_num_documento
		guo_functions.uof_log("I prodotti saranno aggiunti al documento." + guo_functions.uof_format(ll_anno_documento) + "/" + guo_functions.uof_format(ll_num_documento))
	end if
	
	ll_rows = upperbound(astr_carica_confezionato_input.astr_carica_sfuso_input[])
	guo_functions.uof_log("uof_carica_confezionato(): trovato nr "+string(ll_rows)+" elementi da elaborare")
	for ll_i = 1 to ll_rows
		guo_functions.uof_log("Inizio Elaborazione confezionato nr " + guo_functions.uof_format(ll_i) + " Codice finito:" + astr_carica_confezionato_input.astr_carica_sfuso_input[ll_i].as_cod_prodotto_modello)
		ll_ret = uof_crea_ordine(astr_carica_confezionato_input.astr_carica_sfuso_input[ll_i], lstr_carica_sfuso_output, ref luo_return_base)
		if ll_ret < 0 then
			if luo_return_base.iuo_error.errorcode < 0 then 
				guo_functions.uof_log("Uscita per errore " + guo_functions.uof_format(luo_return_base.iuo_error.errorcode) + " Messaggio " + guo_functions.uof_format(luo_return_base.iuo_error.errortext))
				
				// ----
				exception luo_exp
				
				luo_exp = create exception
				luo_exp.setmessage(luo_return_base.iuo_error.errortext)
				throw luo_exp
				
				//return uof_error(luo_return_base.iuo_error.errorcode,luo_return_base.iuo_error.errortext)
			end if
		end if
		
		guo_functions.uof_log("Termine Elaborazione confezionato nr " + guo_functions.uof_format(ll_i) + " Codice finito:" + astr_carica_confezionato_input.astr_carica_sfuso_input[ll_i].as_cod_prodotto_modello)
	next
	
	astr_carica_confezionato_output.al_anno_documento = il_anno_documento
	astr_carica_confezionato_output.al_num_documento = il_num_documento
	
	guo_functions.uof_log("Fine procedura uof_carica_confezionato().")

	
//catch (Exception ex)
//	guo_functions.uof_log("uof_carica_confezionato(): uscita per eccezione." + ex.getmessage())
//	rollback;
//	uof_disconnect_db()
//	return uof_error(-1, ex.text)
//end try

destroy guo_functions

commit;

uof_disconnect_db()


return luo_return_base
end function

public function integer uof_calcola_condizioni (string as_tipo_gestione, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, string as_cod_cliente, string as_cod_tipo_listino_prodotto, string as_cod_valuta, decimal ad_cambio_ven, string as_cod_lingua, string as_cod_agente_1, datetime adt_data_riferimento, string as_cod_prodotto, string as_cod_versione, decimal ad_quan_vendita, decimal ad_dim_1, decimal ad_dim_2, ref decimal ad_prezzo_vendita_mag, ref decimal ad_prezzo_vendita_ven, ref decimal ad_fat_conversione_ven, ref decimal ad_sconto_1, ref decimal ad_sconto_2, ref decimal ad_provvigione_1, ref decimal ad_provvigione_2, ref string as_errore);string 	ls_errore, ls_flag_prezzo_superficie, ls_flag_uso_quan_distinta,  ls_cod_gruppo_var_quan_distinta, ls_flag_prodotto_configurato, ls_cod_variabile_x, ls_cod_variabile_y, ls_cod_tessuto
long		ll_ret
dec{4} 	ld_dimensione_1,ld_dimensione_2, ld_quan_tessuto, ld_divisore
uo_condizioni_cliente 		luo_listini
uo_conf_varianti 			luo_varianti

guo_functions.uof_log("Calcolo condizioni (Inizio)")
luo_listini = create uo_condizioni_cliente

if as_tipo_gestione = "ORD_VEN" then
	luo_listini.s_cs_xx_listino_db_tipo_gestione = "varianti_det_ord_ven"
else
	luo_listini.s_cs_xx_listino_db_tipo_gestione = "varianti_det_off_ven"
end if

select 	flag_prezzo_superficie, 
			flag_uso_quan_distinta, 
			cod_gruppo_var_quan_distinta,
			cod_variabile_dim_x,
			cod_variabile_dim_y
into 		:ls_flag_prezzo_superficie, 
			:ls_flag_uso_quan_distinta,  
			:ls_cod_gruppo_var_quan_distinta,
			:ls_cod_variabile_x,
			:ls_cod_variabile_y
from		tab_flags_configuratore
where		cod_azienda = :s_cs_xx_cod_azienda and
			cod_modello = :as_cod_prodotto;
if sqlca.sqlcode <> 0 then
	// non è un prodotto configurato
	ls_flag_prezzo_superficie = "N"
	ls_flag_uso_quan_distinta = "N"
	setnull(ls_cod_gruppo_var_quan_distinta)
	ls_flag_prodotto_configurato = "N"
else
	ls_flag_prodotto_configurato = "S"
end if

if isnull(ls_flag_uso_quan_distinta) then ls_flag_uso_quan_distinta = "N"
if isnull(ls_flag_prezzo_superficie) then ls_flag_prezzo_superficie = "N"


luo_listini.s_cs_xx_listino_db_flag_calcola = "S"
luo_listini.s_cs_xx_listino_db_cod_versione = as_cod_versione
luo_listini.s_cs_xx_listino_db_cod_tipo_listino_prodotto = as_cod_tipo_listino_prodotto
luo_listini.s_cs_xx_listino_db_cod_valuta = as_cod_valuta
luo_listini.s_cs_xx_listino_db_data_inizio_val = adt_data_riferimento
luo_listini.s_cs_xx_listino_db_anno_documento = al_anno_documento
luo_listini.s_cs_xx_listino_db_num_documento = al_num_documento
luo_listini.s_cs_xx_listino_db_prog_riga = al_prog_riga_documento
//luo_listini.s_cs_xx_listino_db_quan_prodotto_finito =ad_quan_vendita
luo_listini.s_cs_xx_listino_db_quan_prodotto_finito =1


luo_listini.str_parametri.cod_tipo_listino_prodotto = as_cod_tipo_listino_prodotto
guo_functions.uof_log("Tipo listino prodotto=" + guo_functions.uof_format(as_cod_tipo_listino_prodotto))

luo_listini.str_parametri.cod_valuta = as_cod_valuta
guo_functions.uof_log("Valuta=" + guo_functions.uof_format(as_cod_valuta))

luo_listini.str_parametri.cambio_ven = double(ad_cambio_ven)
guo_functions.uof_log("Cambio=" + guo_functions.uof_format(ad_cambio_ven))

luo_listini.ib_provvigioni=true
luo_listini.ib_setitem=false
luo_listini.is_flag_gruppi_sconto="S"
luo_listini.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
luo_listini.s_cs_xx_db_funzioni_formato_data = s_cs_xx_db_funzioni_formato_data

luo_listini.str_parametri.data_riferimento = adt_data_riferimento
guo_functions.uof_log("data riferimento=" + guo_functions.uof_format(adt_data_riferimento))
luo_listini.str_parametri.cod_cliente = as_cod_cliente
guo_functions.uof_log("cod_cliente=" + guo_functions.uof_format(as_cod_cliente))
luo_listini.str_parametri.cod_prodotto = as_cod_prodotto
guo_functions.uof_log("cod_prodotto=" + guo_functions.uof_format(as_cod_prodotto))
luo_listini.str_parametri.cod_agente_1 = as_cod_agente_1
guo_functions.uof_log("cod_agente_1=" + guo_functions.uof_format(as_cod_agente_1))
setnull(luo_listini.str_parametri.cod_agente_2)

luo_listini.str_parametri.dim_1 = ad_dim_1
guo_functions.uof_log("dim_x=" + guo_functions.uof_format(ad_dim_1))
luo_listini.str_parametri.dim_2 = ad_dim_2
guo_functions.uof_log("dim_y=" + guo_functions.uof_format(ad_dim_2))

luo_listini.str_parametri.quantita = double(ad_quan_vendita)
guo_functions.uof_log("quantita=" + guo_functions.uof_format(ad_quan_vendita))

luo_listini.str_parametri.valore=0
luo_listini.lb_flag_escludi_linee_sconto = false

ls_errore = ""
luo_listini.wf_condizioni_cliente( ref ls_errore )
if len(ls_errore) > 0 then
	as_errore =  "Errore in ricerca prezzo del prodotto " + as_cod_prodotto + ". Dettaglio Errore " + ls_errore
	return -1
end if

guo_functions.uof_log("Minimale Superficie=" + guo_functions.uof_format(  luo_listini.str_output.min_fat_superficie))

if ls_flag_prodotto_configurato = "N" then
	select 	fat_conversione_ven
	into	:ad_fat_conversione_ven
	from	anag_prodotti
	where	cod_azienda = :s_cs_xx_cod_azienda and
			cod_prodotto = :as_cod_prodotto;	
				
	if sqlca.sqlcode = 100 then
		as_errore = "uof_calcola_condizioni(). Prodotto " + as_cod_prodotto + " non trovato in anagrafica magazzino "
		return -1
	elseif sqlca.sqlcode < 0 then
		as_errore = "uof_calcola_condizioni(). Errore SQL in ricerca prodotto " + as_cod_prodotto + ". Dettaglio Errore " + sqlca.sqlerrtext
		return -1
	end if	
	
	guo_functions.uof_log("Fattore conversione=" + guo_functions.uof_format(ad_fat_conversione_ven))
	
	if upperbound(luo_listini.str_output.variazioni) < 1 or isnull(upperbound(luo_listini.str_output.variazioni)) then
		guo_functions.uof_log("Nessuna variazione Trovata")
		ad_prezzo_vendita_mag = 0
		ad_prezzo_vendita_ven = 0
	else
		guo_functions.uof_log("Trovato nr."+ guo_functions.uof_format(upperbound(luo_listini.str_output.variazioni)) +" Variazioni")
		ad_prezzo_vendita_mag = luo_listini.str_output.variazioni[upperbound(luo_listini.str_output.variazioni)]
			guo_functions.uof_log("Fatt Conversione0=" + guo_functions.uof_format(ad_fat_conversione_ven))
		if ad_fat_conversione_ven <> 0 then
			ad_prezzo_vendita_ven = round(ad_prezzo_vendita_mag / ad_fat_conversione_ven,2)
		else
			ad_prezzo_vendita_ven = 0
		end if
	end if
	guo_functions.uof_log("Prezzo Vendita Mag0=" + guo_functions.uof_format(ad_prezzo_vendita_mag))
	guo_functions.uof_log("Prezzo Vendita Ven0=" + guo_functions.uof_format(ad_prezzo_vendita_ven))

else
	// sezione gestione prezzo prodotto configurato; eventuale gestione del prezzo per superficie.

	if ls_flag_prezzo_superficie <> "S" then
		guo_functions.uof_log("Flag calcolo prezzo MQ=N")
		if upperbound(luo_listini.str_output.variazioni) < 1 or isnull(upperbound(luo_listini.str_output.variazioni)) then
			guo_functions.uof_log("Nessuna variazione Trovata")
			ad_prezzo_vendita_mag = 0
			ad_prezzo_vendita_ven = 0
		else
			guo_functions.uof_log("Trovato nr."+ guo_functions.uof_format(upperbound(luo_listini.str_output.variazioni)) +" Variazioni")
			ad_prezzo_vendita_mag = luo_listini.str_output.variazioni[upperbound(luo_listini.str_output.variazioni)]
			guo_functions.uof_log("Fatt Conversione1=" + guo_functions.uof_format(ad_fat_conversione_ven))
			if ad_fat_conversione_ven <> 0 then
				ad_prezzo_vendita_ven = round(ad_prezzo_vendita_mag / ad_fat_conversione_ven,2)
			else
				ad_prezzo_vendita_ven = 0
			end if
		end if
		guo_functions.uof_log("Prezzo Vendita Mag1=" + guo_functions.uof_format(ad_prezzo_vendita_mag))
		guo_functions.uof_log("Prezzo Vendita Ven1=" + guo_functions.uof_format(ad_prezzo_vendita_ven))
	else
		guo_functions.uof_log("Flag calcolo prezzo MQ=S")
		if not isnull(ls_cod_variabile_x)  then
			if luo_listini.str_output.min_fat_larghezza > 0 and not isnull(luo_listini.str_output.min_fat_larghezza) and ad_dim_1 < luo_listini.str_output.min_fat_larghezza then
				ld_dimensione_1 = luo_listini.str_output.min_fat_larghezza
			else
				ld_dimensione_1 = ad_dim_1
			end if
		end if
		if not isnull(ls_cod_variabile_y)  then
			if luo_listini.str_output.min_fat_altezza > 0 and not isnull(luo_listini.str_output.min_fat_altezza) and ad_dim_2 < luo_listini.str_output.min_fat_altezza then
				ld_dimensione_2 = luo_listini.str_output.min_fat_altezza
			else
				ld_dimensione_2 = ad_dim_2
			end if
		end if
		
		if ls_flag_uso_quan_distinta = "N" then
			// semplice moltiplicazione dim_x * dim_y
			
			guo_functions.uof_log("dimensione_1="+guo_functions.uof_format(ld_dimensione_1))
			guo_functions.uof_log("dimensione_2="+guo_functions.uof_format(ld_dimensione_2))
			
			ld_divisore = 0
			guo_functions.uof_get_parametro_azienda( "DIV", ld_divisore)
			
			if not isnull(ld_divisore) and ld_divisore > 0 then
				ad_fat_conversione_ven = round((ld_dimensione_1 * ld_dimensione_2) / ld_divisore,2)
			else
				ad_fat_conversione_ven = round((ld_dimensione_1 * ld_dimensione_2) / 1000000,2)
			end if
			
			if ad_fat_conversione_ven <= 0 or isnull(ad_fat_conversione_ven) then
				ad_fat_conversione_ven = 1
			end if
			guo_functions.uof_log("risultato Fattore Conv="+guo_functions.uof_format(ad_fat_conversione_ven))
		else
			
			guo_functions.uof_log("Flag ricerca quan distinta=S")
			guo_functions.uof_log("Prodotto="+guo_functions.uof_format(as_cod_prodotto))
			guo_functions.uof_log("Versione="+guo_functions.uof_format(as_cod_versione))
			guo_functions.uof_log("Gruppo Variante="+guo_functions.uof_format(ls_cod_gruppo_var_quan_distinta))
			
			luo_varianti = CREATE uo_conf_varianti
			luo_varianti.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
			luo_varianti.s_cs_xx_db_funzioni_formato_data = s_cs_xx_db_funzioni_formato_data
			ll_ret =  luo_varianti.uof_ricerca_quan_mp_variante(as_cod_prodotto,  &
																		as_cod_versione, &
																		1, &
																		ls_cod_gruppo_var_quan_distinta, &
																		al_anno_documento, &
																		al_num_documento, &
																		al_prog_riga_documento, &
																		as_tipo_gestione, &
																		ls_cod_tessuto, &
																		ld_quan_tessuto,&
																		ls_errore)
			choose case ll_ret
				case -1 
					//errore
					as_errore =  "(uo_conf_varianti.uof_ricerca_quan_mp) Errore in ricerca quantità tessuto in distinta base del prodotto" + as_cod_prodotto + ". Dettaglio Errore " + ls_errore
					return -1
				case 0		
					// trovato variante
					guo_functions.uof_log("uof_ricerca_quan_mp_variante: TESS="+guo_functions.uof_format(ls_cod_tessuto) + "  Q.TA="+guo_functions.uof_format(ld_quan_tessuto) )
					if ld_quan_tessuto <= 0 or isnull(ld_quan_tessuto) then
			//			g_mb.messagebox("Configuratore", "Attenzione la quantità trovata nel gruppo variante " + istr_flags.cod_gruppo_var_quan_distinta + " è pari a zero o è nulla; verrà FORZATA 1, ma è necessario il controllo dell'operatore!")
						ld_quan_tessuto = 1
					end if
					destroy luo_varianti
					ad_fat_conversione_ven = round(ld_quan_tessuto,2)
				case 1
					// trovato niente nella distinta
			end choose
			
		end if
	
	//
		// ---------------------- fine vuoto per pieno ----------------------------------
		// controllo il minimale di superficie
		if luo_listini.str_output.min_fat_superficie > 0 and not isnull(luo_listini.str_output.min_fat_superficie) and ad_fat_conversione_ven < luo_listini.str_output.min_fat_superficie then
			ad_fat_conversione_ven = luo_listini.str_output.min_fat_superficie
			guo_functions.uof_log("Minimale superficie applicato."+ guo_functions.uof_format(ad_fat_conversione_ven) )
		end if
		
		
		if upperbound(luo_listini.str_output.variazioni) < 1 or isnull(upperbound(luo_listini.str_output.variazioni)) then
			guo_functions.uof_log("Nessuna variazione Trovata")
			ad_prezzo_vendita_mag = 0
			ad_prezzo_vendita_ven = 0
		else
			guo_functions.uof_log("Trovato nr."+ guo_functions.uof_format(upperbound(luo_listini.str_output.variazioni)) +" Variazioni")
			ad_prezzo_vendita_mag = luo_listini.str_output.variazioni[upperbound(luo_listini.str_output.variazioni)]
			if ad_fat_conversione_ven <> 0 then
				ad_prezzo_vendita_ven = round(ad_prezzo_vendita_mag * ad_fat_conversione_ven,2)
				guo_functions.uof_log("Prezzo vendita = prezzo al MQ  X tot MQ -->>"+ guo_functions.uof_format(ad_prezzo_vendita_ven) +"= " + guo_functions.uof_format(ad_prezzo_vendita_mag) + " * " + guo_functions.uof_format(ad_fat_conversione_ven) )
			else
				ad_prezzo_vendita_ven = 0
			end if
		end if
		
	end if	


end if

ad_sconto_1 = dec( luo_listini.str_output.sconti[1] )
guo_functions.uof_log("Sconto_1=" + guo_functions.uof_format(ad_sconto_1))

ad_sconto_2 = dec( luo_listini.str_output.sconti[2] )
guo_functions.uof_log("Sconto_2=" + guo_functions.uof_format(ad_sconto_2))

ad_provvigione_1 = luo_listini.str_output.provvigione_1
guo_functions.uof_log("Provvigione_1=" + guo_functions.uof_format(ad_provvigione_1))

destroy luo_listini

guo_functions.uof_log("Calcolo condizioni (Fine)")

return 0
end function

public function integer uof_optionals (string as_tipo_documento, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, ref str_optionals astr_righe_optionals[], ref string as_errore);/* questa funzione carica OPTIONAL e DESCRIZIONE AGGIUNTIVA basandosi sul codice della variabile
	se il codice inizia per OPT allora è un optional
	se il codice inizia per NOT allora è un valore stringa che deve essere aggiunto alla nota di dettaglio */

string		ls_sql, ls_errore, ls_cod_modello, ls_cod_variabile, ls_flag_addizionale, ls_flag_optional, ls_cod_prodotto_verifica, ls_cod_prodotto, &
			ls_cod_gruppo_var_addizionale, ls_cod_versione_modello, ls_cod_materia_prima
long		ll_ret, ll_i, ll_riga_optional, ll_cont, ll_rows
dec{4}	ld_quan_materia_prima
datastore 	lds_data
uo_crea_documento luo_doc
uo_conf_varianti luo_conf_varianti


ls_sql = "select cod_variabile, flag_tipo_dato, valore_stringa, valore_numero, valore_datetime from "

choose case as_tipo_documento
	case "ORD_VEN"
		
		select 	cod_prodotto,
					cod_versione
		into		:ls_cod_modello,
					:ls_cod_versione_modello
		from		det_ord_ven
		where	cod_azienda = :s_cs_xx_cod_azienda and
					anno_registrazione = :al_anno_documento and
					num_registrazione = :al_num_documento and
					prog_riga_ord_ven = :al_prog_riga_documento;
		
		ls_sql += " det_ord_ven_conf_variabili where cod_azienda = '" + s_cs_xx_cod_azienda + "' and anno_registrazione = " + string(al_anno_documento) + " and num_registrazione = " + string(al_num_documento) + " and prog_riga_ord_ven = " + string(al_prog_riga_documento)
	case "OFF_VEN"

		select 	cod_prodotto,
					cod_versione
		into		:ls_cod_modello,
					:ls_cod_versione_modello
		from		det_off_ven
		where	cod_azienda = :s_cs_xx_cod_azienda and
					anno_registrazione = :al_anno_documento and
					num_registrazione = :al_num_documento and
					prog_riga_off_ven = :al_prog_riga_documento;
		
		ls_sql += " det_off_ven_conf_variabili where cod_azienda = '" + s_cs_xx_cod_azienda + "' and anno_registrazione = " + string(al_anno_documento) + " and num_registrazione = " + string(al_num_documento) + " and prog_riga_off_ven = " + string(al_prog_riga_documento)
end choose

guo_functions.uof_log("UOF_OPTIONALS del modello "+guo_functions.uof_format(ls_cod_modello)+": SQL ESTRAZ. VAR.:" + guo_functions.uof_format(ls_sql) )

ll_rows = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)
if ll_rows < 0 then
	as_errore = "Errore in creazione datastore variabili per optionals. " + ls_sql
	return -1
end if

// nessuna variabile e quindi niente optionals
if ll_rows = 0 then
	guo_functions.uof_log("UOF_OPTIONALS: nessuna variabile trovata, quindi niente OPTIONALS  ")
	return 0
else
	guo_functions.uof_log("UOF_OPTIONALS: estratto nr." + guo_functions.uof_format(ll_rows) + " variabili ")
end if

ll_cont = 0

// trovato un optional e quindi ora cerco la riga in cui inserire ed inserisco la riga
luo_doc = create uo_crea_documento
luo_doc.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
luo_doc.s_cs_xx_db_funzioni_formato_data=s_cs_xx_db_funzioni_formato_data

for ll_i = 1 to ll_rows
	ls_cod_variabile = lds_data.getitemstring(ll_i, "cod_variabile")
	guo_functions.uof_log("UOF_OPTIONALS: esamino variabile " + ls_cod_variabile + "  " +  guo_functions.uof_format(ll_i) + "/" + guo_functions.uof_format(ll_rows)  )
	
	// enrico 23-05-2016 per Marinello 
	select 	flag_addizionale,
				flag_optional,
				cod_gruppo_var_addizionale
	into		:ls_flag_addizionale,
				:ls_flag_optional,
				:ls_cod_gruppo_var_addizionale
	from		tab_des_variabili
	where		cod_azienda = :s_cs_xx_cod_azienda and
				cod_prodotto = :ls_cod_modello and
				cod_variabile = :ls_cod_variabile;
				
	if sqlca.sqlcode < 0 then
		guo_functions.uof_log("UOF_OPTIONALS: errore SQL in ricerca variabile " + guo_functions.uof_format(sqlca.sqlerrtext))
		return 0
	end if
	
	ls_cod_prodotto = lds_data.getitemstring(ll_i, "valore_stringa")
	

	if ls_flag_optional = "S" then
		guo_functions.uof_log("UOF_OPTIONALS: variabile OPTional." + guo_functions.uof_format(ls_cod_variabile) + " - Valore stringa:" + guo_functions.uof_format( ls_cod_prodotto )  + " - Valore Num:" + guo_functions.uof_format( lds_data.getitemnumber(ll_i, "valore_numero")))
		
		// passo la riga di appartenenza per optional
		ll_riga_optional = al_prog_riga_documento
		
		if not isvalid(luo_doc) then
			guo_functions.uof_log("UOF_OPTIONALS: uo_crea_documento non valido")
		elseif isnull(luo_doc) then
			guo_functions.uof_log("UOF_OPTIONALS: uo_crea_documento null value")
		else			
			guo_functions.uof_log("UOF_OPTIONALS: uo_crea_documento valid object: azienda " + luo_doc.s_cs_xx_cod_azienda)
		end if
		
		// Verifico che l'optional sia un articolo di magazzino
		select 	cod_prodotto
		into		:ls_cod_prodotto_verifica
		from		anag_prodotti
		where		cod_azienda = :s_cs_xx_cod_azienda and
					cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			guo_functions.uof_log("UOF_OPTIONALS: uo_crea_documento valid object: prodotto " + guo_functions.uof_format( ls_cod_prodotto ) + " non presente in anagrafica: impossibile aggiungere addizionale/optional ")
			continue
		end if
		
		ll_ret = luo_doc.uof_crea_riga_documento( as_tipo_documento, ls_cod_prodotto, 1, al_anno_documento, al_num_documento, "O",  ls_cod_modello, ref ll_riga_optional, ref ls_errore)
		if ll_ret < 0 then
			guo_functions.uof_log("UOF_OPTIONALS: Errore in creazione riga optional della variabile " + guo_functions.uof_format(ls_cod_variabile) + " ERRORE: " + guo_functions.uof_format(ls_errore))
			as_errore = ls_errore
			destroy luo_doc
			return -1
		end if
		
		ll_cont ++
		astr_righe_optionals[ll_cont].riga_ordine = ll_riga_optional
		astr_righe_optionals[ll_cont].cod_optional = ls_cod_prodotto
		astr_righe_optionals[ll_cont].quan_optional = 1
		setnull(astr_righe_optionals[ll_cont].cod_versione)
		
		guo_functions.uof_log("UOF_OPTIONALS: creata riga OPT nr "+guo_functions.uof_format(ll_riga_optional)+" COD=" + guo_functions.uof_format(astr_righe_optionals[ll_cont].cod_optional) + "  QTA=" + guo_functions.uof_format(astr_righe_optionals[ll_cont].quan_optional)   )
		
	end if
	
	if ls_flag_addizionale = "S" then
		guo_functions.uof_log("UOF_OPTIONALS: variabile ADDizionale." + guo_functions.uof_format(ls_cod_variabile) + " - Valore stringa:" + guo_functions.uof_format( ls_cod_prodotto )  + " - Valore Num:" + guo_functions.uof_format( lds_data.getitemnumber(ll_i, "valore_numero")))
		
		if isnull(ls_cod_gruppo_var_addizionale) then
			guo_functions.uof_log("UOF_OPTIONALS: per la variabile " + guo_functions.uof_format(ls_cod_variabile) + " non è stato indicato il gruppo variante per la ricerca in distinta")
			continue
		end if
		
		// passo la riga di appartenenza per optional
		ll_riga_optional = al_prog_riga_documento
		
		if not isvalid(luo_doc) then
			guo_functions.uof_log("UOF_OPTIONALS: uo_crea_documento non valido")
		elseif isnull(luo_doc) then
			guo_functions.uof_log("UOF_OPTIONALS: uo_crea_documento null value")
		else			
			guo_functions.uof_log("UOF_OPTIONALS: uo_crea_documento valid object: azienda " + luo_doc.s_cs_xx_cod_azienda)
		end if
		
		luo_conf_varianti = create uo_conf_varianti
		luo_conf_varianti.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
		luo_conf_varianti.s_cs_xx_db_funzioni_formato_data = s_cs_xx_db_funzioni_formato_data
		
		// Nel caso dell'addizionale vado a verificare la quantità della MP nella distinta base
		setnull(ls_cod_materia_prima)
		ld_quan_materia_prima = 0
		ls_errore = ""
		ll_ret = luo_conf_varianti.uof_ricerca_quan_mp_variante( ls_cod_modello, &
																				ls_cod_versione_modello, &
																				1, &
																				ls_cod_gruppo_var_addizionale, &
																				al_anno_documento, &
																				al_num_documento, &
																				al_prog_riga_documento, &
																				as_tipo_documento, &
																				ref ls_cod_materia_prima, &
																				ref ld_quan_materia_prima, &
																				ref ls_errore)
		
		if ll_ret < 0 then
			guo_functions.uof_log("UOF_OPTIONALS: errore funzione uof_ricerca_quan_mp_variante(): " + guo_functions.uof_format(ls_errore) )
			as_errore = ls_errore
			destroy luo_conf_varianti
			destroy luo_doc
			return -1
		end if
		
		guo_functions.uof_log("UOF_OPTIONALS: ricerca prodotto per addizionale uof_ricerca_quan_mp_variante per gruppo var "+ls_cod_gruppo_var_addizionale+"; trovato " + guo_functions.uof_format(ls_cod_materia_prima) + " Qta: " + guo_functions.uof_format(ld_quan_materia_prima) )
		
		destroy luo_conf_varianti
		
		// Creo la riga solo se la quantità è maggiore di zero
		if ld_quan_materia_prima > 0 and not isnull(ls_cod_materia_prima) then
				
			ll_ret = luo_doc.uof_crea_riga_documento( as_tipo_documento, ls_cod_materia_prima, ld_quan_materia_prima, al_anno_documento, al_num_documento, "A",  ls_cod_modello, ref ll_riga_optional, ref ls_errore)
			if ll_ret < 0 then
				guo_functions.uof_log("UOF_OPTIONALS: Errore in creazione riga optional della variabile " + guo_functions.uof_format(ls_cod_variabile) + " ERRORE: " + guo_functions.uof_format(ls_errore))
				as_errore = ls_errore
				destroy luo_doc
				return -1
			end if
			
		
			ll_cont ++
			astr_righe_optionals[ll_cont].riga_ordine = ll_riga_optional
			astr_righe_optionals[ll_cont].cod_optional = ls_cod_materia_prima
			astr_righe_optionals[ll_cont].quan_optional = ld_quan_materia_prima
			setnull(astr_righe_optionals[ll_cont].cod_versione)
			
			guo_functions.uof_log("UOF_OPTIONALS: creata riga ADD nr "+guo_functions.uof_format(ll_riga_optional)+" COD=" + guo_functions.uof_format(astr_righe_optionals[ll_cont].cod_optional) + "  QTA=" + guo_functions.uof_format(astr_righe_optionals[ll_cont].quan_optional)   )
		else
			guo_functions.uof_log("UOF_OPTIONALS:  ADDizionale non creata per quantità a ZERO;  COD=" + guo_functions.uof_format(ls_cod_materia_prima) + "  QTA=" + guo_functions.uof_format(ld_quan_materia_prima)   )
		end if		
	end if
	
next

destroy luo_doc
destroy luo_conf_varianti

guo_functions.uof_log("UOF_OPTIONALS: uscita funzione")


return 0
end function

protected function integer uof_crea_note (string as_tipo_documento, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, ref string as_nota_prodotto, ref string as_nota_dettaglio, ref string as_errore);
string		ls_sql, ls_errore, ls_str, ls_cod_modello
long			ll_ret, ll_i, ll_num, ll_riga_optional, ll_cont
datastore 	lds_data

guo_functions.uof_log("Elaborazione note prodotto")

as_nota_prodotto = ""
as_nota_dettaglio = ""

ls_sql = "select cod_variabile, flag_tipo_dato, valore_stringa from "

choose case as_tipo_documento
	case "ORD_VEN"		
		ls_sql += " det_ord_ven_conf_variabili where cod_azienda = '" + s_cs_xx_cod_azienda + "' and anno_registrazione = " + string(al_anno_documento) + " and num_registrazione = " + string(al_num_documento) + " and prog_riga_ord_ven = " + string(al_prog_riga_documento)
	case "OFF_VEN"
		
		ls_sql += " det_off_ven_conf_variabili where cod_azienda = '" + s_cs_xx_cod_azienda + "' and anno_registrazione = " + string(al_anno_documento) + " and num_registrazione = " + string(al_num_documento) + " and prog_riga_off_ven = " + string(al_prog_riga_documento)
end choose

ls_sql += " order by cod_variabile "

guo_functions.uof_log(ls_sql)


ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)
if ll_ret < 0 then
	as_errore = "Errore in creazione datastore variabili per note prodotto e dettaglio. " + ls_sql
	return -1
end if

guo_functions.uof_log("Eseguita lettura di " + string(ll_Ret) + " variabili da cui selezionare le note")


// nessuna variabile e quindi niente optionals
if ll_ret = 0 then return 0

ll_cont = 0


for ll_i = 1 to ll_ret
	if upper( left( lds_data.getitemstring(ll_i, "cod_variabile"),4 )) = "NOTP" and lds_data.getitemstring(ll_i, "flag_tipo_dato") ="S" and len(lds_data.getitemstring(ll_i, "valore_stringa")) > 0 and not isnull(lds_data.getitemstring(ll_i, "valore_stringa")) then
		guo_functions.uof_log("Aggiungo nota prodotto " + guo_functions.uof_format(lds_data.getitemstring(ll_i, "valore_stringa")))
		if len(as_nota_prodotto) > 0 then 
			as_nota_prodotto += " - " + lds_data.getitemstring(ll_i, "valore_stringa")
		else
			as_nota_prodotto = lds_data.getitemstring(ll_i, "valore_stringa")
		end if
	end if
	
	if upper( left( lds_data.getitemstring(ll_i, "cod_variabile"),4 )) = "NOTD" and lds_data.getitemstring(ll_i, "flag_tipo_dato") ="S" and len(lds_data.getitemstring(ll_i, "valore_stringa")) > 0 and not isnull(lds_data.getitemstring(ll_i, "valore_stringa")) then
		guo_functions.uof_log("Aggiungo nota dettaglio " + guo_functions.uof_format(lds_data.getitemstring(ll_i, "valore_stringa")))
		if len(as_nota_dettaglio) > 0 then 
			as_nota_dettaglio += " - " + lds_data.getitemstring(ll_i, "valore_stringa")
		else
			as_nota_dettaglio = lds_data.getitemstring(ll_i, "valore_stringa")
		end if
	end if
	
	
next

if len(as_nota_dettaglio) > 255 then as_nota_dettaglio = left(as_nota_dettaglio,222) + "..."
if len(as_nota_prodotto) > 100 then as_nota_prodotto = left(as_nota_prodotto,97) + "..."

guo_functions.uof_log("nota dettaglio finale=" + guo_functions.uof_format(as_nota_dettaglio))
guo_functions.uof_log("nota prodotto finale=" + guo_functions.uof_format(as_nota_prodotto))

return 0
end function

public function uo_return_base uof_connect_db () throws exception;uo_return_base luo_return_base

uof_get_transactioninformation()
	
connect using sqlca;

if sqlca.SQLCode <> 0 then
	return uof_error(-1,"uof_connect_db(): Errore durante la connessione al database")
END IF

luo_return_base = create uo_return_base
return luo_return_base
end function

public function long uof_crea_ordine (str_carica_sfuso_input astr_carica_sfuso_input, ref str_carica_sfuso_output astr_carica_sfuso_output, ref uo_return_base auo_return_base) throws exception;// Funzione di caricamento ordine da parametri esterni provenienti da chiamata esterna.
//		astr_carica_sfuso_input			Struttura di input dati
//		ab_sfuso_confezionato			configuro prodotti nell'ordine, oppure estraggo lo sfuso (C=Confezionato  S=SFuso)
//		astr_carica_sfuso_output			Nel caso di sfuso(ab_sfuso_confezionato=S) contiene l'elenco dello sfuso
//
boolean						lb_found
string							ls_errore, ls_null, ls_cod_versione_modello, ls_tabella_varianti,ls_cod_lingua_cliente, ls_des_prodotto_lingua, ls_tabella_comp, &
								ls_colonna_riga_documento, ls_cod_verniciatura, ls_cod_comando, ls_cod_tipo_supporto,ls_flag_cancella_offerte_webservice, &
								ls_cod_gruppo_variante_supporti, ls_cod_gruppo_variante_comandi, ls_cod_agente_1,ls_cod_agente_2, ls_cod_prodotto_telo,&
								ls_cod_variabile_quan_finito, ls_cod_variabile_dim_x, ls_cod_variabile_dim_y, ls_cod_variabile_dim_z, ls_cod_variabile_dim_t, ls_cod_variabile_vernic, &
								ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_anagrafica_listino, ls_sql, ls_nota_prodotto, ls_nota_dettaglio, ls_cod_variabile_telo, ls_cod_versione_telo
long 							ll_null[], ll_prog_riga_documento, ll_ret, ll_i, ll_j, ll_secondi, ll_prog_riga_optional
dec{4}						ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_quan_vendita, ld_prezzo_vendita_mag, ld_prezzo_vendita_ven, ld_fat_conversione_ven, &
								ld_sconto_1, ld_sconto_2, ld_provvigione_1, ld_provvigione_2,ld_quan_vendita_listino, ld_cambio_ven, &
								ld_prezzo_vendita_mag_telo, ld_prezzo_vendita_ven_telo,ld_fat_conversione_ven_telo, ld_sconto_1_telo,ld_sconto_2_telo, ld_provvigione_1_telo, ld_provvigione_2_telo
datetime						ldt_data_consegna
time							lt_inizio, lt_fine
str_optionals				lstr_optionals[], lstr_vuoto[]
uo_crea_documento 		luo_docs
uo_conf_varianti			luo_conf_varianti
str_varianti					lstr_varianti[]
s_trova_mp_varianti		lstr_trova_mp_varianti_qta_utilizzo[], lstr_trova_mp_varianti_qta_tecnica[]
str_materie_prime			lstr_materie_prime[],lstr_materie_prime_vuoto[]
uo_condizioni_cliente		luo_listini


// MEMO nei parametri mettere un array con l'oggetto uo_elenco_sfuso.

lt_inizio = now()

id_sessione = "WS" + string(date(now()),"yyyy-mm-dd") + "-" + string(time(now()),"hh-mm-ss")

try 
	// connessione DB e creazione variabili globali
	astr_carica_sfuso_output.astr_materie_prime[] = lstr_materie_prime_vuoto[]
//	uof_connect_db()		
//	uof_get_azienda_default()
	guo_des_tabella = create uo_des_tabella
	guo_des_tabella.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
	
	guo_functions.uof_get_parametro_azienda( "LWS", gb_log)
	guo_functions.uof_log("Valore parametro astr_carica_sfuso_input.as_flag_tipo_anagrafica= " +guo_functions.uof_format(astr_carica_sfuso_input.as_flag_tipo_anagrafica ) )
	guo_functions.uof_log("Valore parametro astr_carica_sfuso_input.as_cod_anagrafica= " +guo_functions.uof_format(astr_carica_sfuso_input.as_cod_anagrafica) )
	guo_functions.uof_log("Valore parametro astr_carica_sfuso_input.as_tipo_gestione= " +guo_functions.uof_format(astr_carica_sfuso_input.as_tipo_gestione) )  
	guo_functions.uof_log("Valore parametro astr_carica_sfuso_input.as_tipo_configurazione= " +guo_functions.uof_format(astr_carica_sfuso_input.as_tipo_configurazione) )
	guo_functions.uof_log("Valore parametro astr_carica_sfuso_input.as_cod_prodotto_modello= " +guo_functions.uof_format(astr_carica_sfuso_input.as_cod_prodotto_modello) )
	
	choose case astr_carica_sfuso_input.as_tipo_configurazione
		case "S"
			guo_functions.uof_log("Inizio Configurazione ....sfuso")
		case "F"
			guo_functions.uof_log("Inizio Configurazione ....finito/confezionato")
		case else
			guo_functions.uof_log("Inizio Configurazione ....tipo non specificato Sfuso/Confezionato")
	end choose

	guo_functions.uof_log("Passate variabili per un totale di:" +  string(upperbound(astr_carica_sfuso_input.astr_variabili[])))

	for ll_i = 1 to upperbound(astr_carica_sfuso_input.astr_variabili[])
		guo_functions.uof_log("Variabile nr " + guo_functions.uof_format(ll_i) + " :" + guo_functions.uof_format(astr_carica_sfuso_input.astr_variabili[ll_i].as_cod_variabile ) + " - VALORE STR=" + guo_functions.uof_format(astr_carica_sfuso_input.astr_variabili[ll_i].as_string) +  " - VALORE NUM=" + guo_functions.uof_format(astr_carica_sfuso_input.astr_variabili[ll_i].ad_number) )
	next

	// --------------  carico le variabili locali con i valori di cui ho bisogno localmente ----------------------
	
	select 	cod_variabile_quan_finito, 
				cod_variabile_dim_x,
				cod_variabile_dim_y,
				cod_variabile_dim_z,
				cod_variabile_dim_t,
				cod_variabile_vernic,
				cod_variabile_telo
		into	:ls_cod_variabile_quan_finito, 
				:ls_cod_variabile_dim_x,
				:ls_cod_variabile_dim_y,
				:ls_cod_variabile_dim_z,
				:ls_cod_variabile_dim_t,
				:ls_cod_variabile_vernic,
				:ls_cod_variabile_telo
	from  	tab_flags_configuratore
	where 	cod_azienda = :s_cs_xx_cod_azienda and
				cod_modello = :astr_carica_sfuso_input.as_cod_prodotto_modello;
	if sqlca.sqlcode < 0 then
		uof_disconnect_db()
		auo_return_base = uof_error(-1, "Errore SQL in ricerca modello in tabella parametri configuratore. " + sqlca.sqlerrtext )
		return -1
	elseif sqlca.sqlcode = 100 then
		uof_disconnect_db()
		auo_return_base = uof_error(-1, "Prodotto " +astr_carica_sfuso_input.as_cod_prodotto_modello + " non trovato nei parametri configuratore: impossibile proseguire")
		return -1
	end if		
	
	uof_get_variabile_valore( astr_carica_sfuso_input.astr_variabili[], ls_cod_variabile_quan_finito, ref ld_quan_vendita, ref ls_errore)
	uof_get_variabile_valore( astr_carica_sfuso_input.astr_variabili[], ls_cod_variabile_vernic, ref ls_cod_verniciatura, ref ls_errore)
	uof_get_variabile_valore( astr_carica_sfuso_input.astr_variabili[], ls_cod_variabile_dim_x, ref ld_dim_x, ref ls_errore)
	uof_get_variabile_valore( astr_carica_sfuso_input.astr_variabili[], ls_cod_variabile_dim_y, ref ld_dim_y, ref ls_errore)
	uof_get_variabile_valore( astr_carica_sfuso_input.astr_variabili[], ls_cod_variabile_dim_z, ref ld_dim_z, ref ls_errore)
	uof_get_variabile_valore( astr_carica_sfuso_input.astr_variabili[], ls_cod_variabile_dim_t, ref ld_dim_t, ref ls_errore)
	
	// --------------   inizio script di caricamento sfuso  -----------------------------------------------
	
	setnull(ls_null)

	// procedo intanto con la creazione delle testata e della riga de ldocumento di riferimento
	guo_functions.uof_log("Avvio procedura creazione documento offerta.")
	luo_docs = create uo_crea_documento
	luo_docs.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
	luo_docs.s_cs_xx_db_funzioni_formato_data = s_cs_xx_db_funzioni_formato_data
	
	astr_carica_sfuso_input.as_flag_tipo_anagrafica = upper( astr_carica_sfuso_input.as_flag_tipo_anagrafica )
	
	if astr_carica_sfuso_input.as_flag_tipo_anagrafica <> "C" and astr_carica_sfuso_input.as_flag_tipo_anagrafica <> "O" then
		auo_return_base = uof_error(-1, "Elaborazione interrotta: il tipo anagrafica passato non è compreso fra i valori ammessi C=Cliente O=Contatto. " + ls_errore )
		return -1
	end if
	
	guo_functions.uof_log("Inizio creazione documento.")
	if isnull(ld_quan_vendita) then
		guo_functions.uof_log("Quantità vendita =null")
	else
		guo_functions.uof_log("Quantità vendita =" + guo_functions.uof_format(ld_quan_vendita))
	end if
	
	if luo_docs.uof_crea_documento( 	astr_carica_sfuso_input.as_tipo_gestione, &
													astr_carica_sfuso_input.as_flag_tipo_anagrafica, &
													astr_carica_sfuso_input.as_cod_anagrafica, &
													astr_carica_sfuso_input.as_cod_prodotto_modello, &
													ld_quan_vendita, &
													ref il_anno_documento, &
													ref il_num_documento, &
													ref ll_prog_riga_documento, &
													ref ls_errore) < 0 then
		rollback;
		uof_disconnect_db()
		auo_return_base = uof_error(-1, "Errore nella estrazione della configurazione (uof_crea_documento). " + ls_errore )
		return  -1
	end if

	guo_functions.uof_log("Creato documento offerta." + string(il_anno_documento) + "/" + string(il_num_documento) + "/" + string(ll_prog_riga_documento ))

	// ---------- carico le variabili ---------------------------
	guo_functions.uof_log("Avvio procedura archiviazione variabili.")

	if luo_docs.uof_scrivi_variabili( astr_carica_sfuso_input.as_tipo_gestione, &
											astr_carica_sfuso_input.as_cod_prodotto_modello, &
											astr_carica_sfuso_input.astr_variabili[], &
											il_anno_documento, &
											il_num_documento, &
											ll_prog_riga_documento, &
											ref ls_errore) < 0 then
		rollback;
		uof_disconnect_db()
		auo_return_base = uof_error(-1, "Errore in fase di archiviazione delle variabili (uof_scrivi_variabili). " + ls_errore )
		return -1
	end if

	guo_functions.uof_log("Termine procedura archiviazione variabili.")

	// metto il commit per evitare che un altro utente si prenda la stessa testata.
	commit;

	destroy luo_docs

	// procedo con la sequenza di caricamento delle varianti (mp automatiche, calcolo formule)

	// estraggo MP automatiche
	luo_conf_varianti = create uo_conf_varianti
	luo_conf_varianti.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
	luo_conf_varianti.s_cs_xx_db_funzioni_formato_data = s_cs_xx_db_funzioni_formato_data
	
	// estraggo le materie prime automatiche generiche

/*	
sistema obsoleto deprecato non più in uso.

	if luo_conf_varianti.uof_carica_mp_automatiche(astr_carica_sfuso_input.as_cod_prodotto_modello, &
																		ld_dim_x, &
																		ld_dim_y, &
																		ld_dim_z, &
																		ld_dim_t, &
																		0, &
																		ls_cod_verniciatura, &
																		ls_null, &
																		ls_null, &
																		ls_null, &
																		ls_null , &
																		ref lstr_varianti[], &
																		ref ls_errore) < 0 then
		uof_disconnect_db()
		auo_return_base = uof_error(-1, "Errore nella estrazione delle materie prime automatiche. " + ls_errore )
		return -1
	end if
*/

	// estraggo l'eventuale comando oppure tutti i comandi
	/*
	ls_cod_gruppo_variante_supporti = ""
	guo_functions.uof_get_parametro_azienda("MP1",ls_cod_gruppo_variante_supporti )
	if len(ls_cod_gruppo_variante_supporti) < 1 then setnull(ls_cod_gruppo_variante_supporti)
	if len(ls_cod_tipo_supporto) < 1 then setnull(ls_cod_tipo_supporto)
	
	if luo_conf_varianti.uof_carica_mp_automatiche(astr_carica_sfuso_input.as_cod_prodotto_modello, &
																		ld_dim_x, &
																		ld_dim_y, &
																		ld_dim_z, &
																		ld_dim_t, &
																		0, &
																		ls_cod_verniciatura, &
																		ls_null, ls_null, &
																		ls_cod_gruppo_variante_supporti , &
																		ls_cod_tipo_supporto , &
																		ref lstr_varianti[], &
																		ref ls_errore) < 0 then
		uof_disconnect_db()
		return uof_error(-1, "Errore nella estrazione delle materie prime automatiche. " + ls_errore )
	end if
	
	// estraggo l'eventuale comando oppure tutti i comandi
	ls_cod_gruppo_variante_comandi = ""
	guo_functions.uof_get_parametro_azienda("MP2",ls_cod_gruppo_variante_comandi )
	if len(ls_cod_gruppo_variante_comandi) < 1 then setnull(ls_cod_gruppo_variante_comandi)
	if len(ls_cod_comando) < 1 then setnull(ls_cod_comando)
	
	if luo_conf_varianti.uof_carica_mp_automatiche(astr_carica_sfuso_input.as_cod_prodotto_modello, &
																		ld_dim_x, &
																		ld_dim_y, &
																		ld_dim_z, &
																		ld_dim_t, &
																		0, &
																		ls_cod_verniciatura, &
																		ls_null, ls_null, &
																		ls_cod_gruppo_variante_comandi , &
																		ls_cod_comando , &
																		ref lstr_varianti[], &
																		ref ls_errore) < 0 then
		uof_disconnect_db()
		return uof_error(-1, "Errore nella estrazione delle materie prime automatiche. " + ls_errore )
	end if
	*/

	
	choose case upper(astr_carica_sfuso_input.as_tipo_gestione)
		case "OFF_VEN"
			
			select 	cod_valuta,
						cambio_ven,
						data_consegna
			into		:ls_cod_valuta,
						:ld_cambio_ven,
						:ldt_data_consegna
			from		tes_off_ven
			where 	cod_azienda = :s_cs_xx_cod_azienda and
						anno_registrazione = :il_anno_documento and
						num_registrazione = :il_num_documento;
			if sqlca.sqlcode = 100 then
				auo_return_base= uof_error(-1, "uof_crea_ordine(). Errore 100 ricerca cambio in testata documento." )
				return -1
			elseif sqlca.sqlcode < 0 then 
				auo_return_base = uof_error(-1, "Errore SQL in ricerca cambio in testata documento. " + sqlca.sqlerrtext )
				return -1
			end if
			
			select 	cod_versione
			into		:ls_cod_versione_modello
			from		det_off_ven
			where 	cod_azienda = :s_cs_xx_cod_azienda and
						anno_registrazione = :il_anno_documento and
						num_registrazione = :il_num_documento and
						prog_riga_off_ven = :ll_prog_riga_documento;
			if sqlca.sqlcode = 100 then
				auo_return_base= uof_error(-1, "Versione distinta base di default per IperTech non caricata" )
				return -1
			elseif sqlca.sqlcode < 0 then 
				auo_return_base = uof_error(-1, "Errore in ricerca distinta default in distinta padri. " + sqlca.sqlerrtext )
				return -1
			end if
			
			ls_tabella_varianti = "varianti_det_off_ven"
			ls_tabella_comp = "comp_det_off_ven"
			ls_colonna_riga_documento = "prog_riga_off_ven"
			
		case "ORD_VEN"
			select 	cod_valuta,
						cambio_ven,
						data_consegna
			into		:ls_cod_valuta,
						:ld_cambio_ven,
						:ldt_data_consegna
			from		tes_ord_ven
			where 	cod_azienda = :s_cs_xx_cod_azienda and
						anno_registrazione = :il_anno_documento and
						num_registrazione = :il_num_documento;
			if sqlca.sqlcode = 100 then
				auo_return_base= uof_error(-1, "uof_crea_ordine(). Errore 100 ricerca cambio in testata documento." )
				return -1
			elseif sqlca.sqlcode < 0 then 
				auo_return_base = uof_error(-1, "Errore SQL in ricerca cambio in testata documento. " + sqlca.sqlerrtext )
				return -1
			end if
			
			select 	cod_versione
			into		:ls_cod_versione_modello
			from		det_ord_ven
			where 	cod_azienda = :s_cs_xx_cod_azienda and
						anno_registrazione = :il_anno_documento and
						num_registrazione = :il_num_documento and
						prog_riga_ord_ven = :ll_prog_riga_documento;
			if sqlca.sqlcode = 100 then
				auo_return_base = uof_error(-1, "Versione distinta base di default per IperTech non caricata" )
				return -1
			elseif sqlca.sqlcode < 0 then 
				auo_return_base = uof_error(-1, "Errore in ricerca distinta default in distinta padri. " + sqlca.sqlerrtext )
				return -1
			end if
			
			ls_tabella_varianti = "varianti_det_ord_ven"
			ls_tabella_comp = "comp_det_ord_ven"
			ls_colonna_riga_documento = "prog_riga_ord_ven"
	end choose
	
/*
Dismesso perchè obsoleto
	// creo le varianti usando le MP automatiche
	guo_functions.uof_log("inserimento varianti MP automatiche ")
	if luo_conf_varianti.uof_ins_varianti( astr_carica_sfuso_input.as_cod_prodotto_modello, ls_cod_versione_modello, astr_carica_sfuso_input.as_tipo_gestione, lstr_varianti[], il_anno_documento, il_num_documento, ll_prog_riga_documento, ls_errore) < 0 then
		guo_functions.uof_log("Errore nella valorizzazione delle varianti. " + ls_errore)
		rollback;
		uof_disconnect_db()
		auo_return_base = uof_error(-1, "Errore nella valorizzazione delle varianti. " + ls_errore )
		return -1
	end if
*/

	// calcola le formule per i rami senza variante.
	guo_functions.uof_log("Calcolo formule rami distinta")
	if luo_conf_varianti.uof_valorizza_formule(astr_carica_sfuso_input.as_cod_prodotto_modello, ls_cod_versione_modello, astr_carica_sfuso_input.as_tipo_gestione, il_anno_documento, il_num_documento, ll_prog_riga_documento, ref ls_errore ) < 0 then
		auo_return_base = uof_error(-1, "luo_conf_varianti.uof_valorizza_formule()."+ ls_errore)
		return -1
	end if
	
	// carico i dati del cliente utili al calcolo del prezzo dei prodotti
	choose case astr_carica_sfuso_input.as_flag_tipo_anagrafica
		case "O"

			select 	cod_tipo_listino_prodotto,
						cod_lingua,
						cod_agente_1
			into		:ls_cod_tipo_listino_prodotto,
						:ls_cod_lingua_cliente,
						:ls_cod_agente_1
			from		anag_contatti
			where	cod_azienda = :s_cs_xx_cod_azienda and
						cod_contatto = :astr_carica_sfuso_input.as_cod_anagrafica;			
			if sqlca.sqlcode = 100 then
				auo_return_base = uof_error(-1, "uof_carica_sfuso(). Codice Valuta o Tipo listino non caricato in anagrafica contatto " + astr_carica_sfuso_input.as_cod_anagrafica)
				return -1
			elseif sqlca.sqlcode < 0 then
				auo_return_base = uof_error(-1, "uof_carica_sfuso(). Errore SQL in ricerca Valuta e Tipo listino in anagrafica contatto " + astr_carica_sfuso_input.as_cod_anagrafica + ". Dettaglio Errore " + sqlca.sqlerrtext)
				return -1
			end if	
			
			setnull( ls_cod_anagrafica_listino )
	
		case "C"

			select 	cod_tipo_listino_prodotto,
						cod_lingua,
						cod_agente_1
			into		:ls_cod_tipo_listino_prodotto,
						:ls_cod_lingua_cliente,
						:ls_cod_agente_1
			from		anag_clienti
			where	cod_azienda = :s_cs_xx_cod_azienda and
						cod_cliente = :astr_carica_sfuso_input.as_cod_anagrafica;			
			if sqlca.sqlcode = 100 then
				auo_return_base = uof_error(-1, "uof_carica_sfuso(). Codice Valuta o Tipo listino non caricato in anagrafica cliente " + astr_carica_sfuso_input.as_cod_anagrafica)
				return -1
			elseif sqlca.sqlcode < 0 then
				auo_return_base = uof_error(-1, "uof_carica_sfuso(). Errore SQL in ricerca Valuta e Tipo listino nel cliente " + astr_carica_sfuso_input.as_cod_anagrafica + ". Dettaglio Errore " + sqlca.sqlerrtext)
				return -1
			end if	
			
			ls_cod_anagrafica_listino = astr_carica_sfuso_input.as_cod_anagrafica
			
	end choose
	
	ld_fat_conversione_ven = 1
	// da qui iniziano le differenze fra il calcolo dello sfuso e del confezionato
	
	if  astr_carica_sfuso_input.as_tipo_configurazione = "S" then
		// ELABORAZIONE SFUSO
		// primo giro carico le quantità in base alla quan_utilizzo
		if luo_conf_varianti.uof_trova_mat_prime_varianti( 	astr_carica_sfuso_input.as_cod_prodotto_modello, &
																				ls_cod_versione_modello, &
																				0, &
																				ls_tabella_varianti, &
																				il_anno_documento, &
																				il_num_documento, &
																				ll_prog_riga_documento, &
																				"quan_utilizzo", &
																				ld_quan_vendita , &
																				ref lstr_trova_mp_varianti_qta_utilizzo[], &
																				ref ls_errore) < 0 then
			guo_functions.uof_log("Errore estrazione varianti per quan_utilizzo. (uof_trova_mat_prime_varianti)" + ls_errore)
			auo_return_base = uof_error(-1, "uof_carica_sfuso()."+ ls_errore)
			return -1
		end if
		
		// secondo giro in base alla quantità tecnica
		if luo_conf_varianti.uof_trova_mat_prime_varianti( 	astr_carica_sfuso_input.as_cod_prodotto_modello, &
																				ls_cod_versione_modello, &
																				0, &
																				ls_tabella_varianti, &
																				il_anno_documento, &
																				il_num_documento, &
																				ll_prog_riga_documento, &
																				"quan_tecnica", &
																				ld_quan_vendita , &
																				ref lstr_trova_mp_varianti_qta_tecnica[], &
																				ref ls_errore) < 0 then
			guo_functions.uof_log("Errore estrazione varianti per quan_utilizzo. (uof_trova_mat_prime_varianti)" + ls_errore)
			auo_return_base = uof_error(-1, "uof_carica_sfuso()."+ ls_errore)
			return -1
		end if
		
		
		ls_tabella_varianti = ls_tabella_varianti
		lb_found = false
		// merge dei due array di MP
		for ll_i = 1 to upperbound(lstr_trova_mp_varianti_qta_tecnica[])
			
			lb_found = false
			for ll_j = 1 to upperbound(lstr_materie_prime[])
				if lstr_trova_mp_varianti_qta_tecnica[ll_i].cod_materia_prima = lstr_materie_prime[ll_j].cod_prodotto then
					lstr_materie_prime[ll_j].quan_tecnica_mag += lstr_trova_mp_varianti_qta_tecnica[ll_i].quan_tecnica
					guo_functions.uof_log("Materia Prima (Aggiunta): " + lstr_materie_prime[ll_j].cod_prodotto + " Q.ta Tecnica:" + string(lstr_trova_mp_varianti_qta_tecnica[ll_i].quan_tecnica) + " Ord:" + string(lstr_materie_prime[ll_j].ordinamento))
					lb_found=true
					exit
				end if
			next
				
			if not lb_found then
				ll_j = upperbound(lstr_materie_prime) + 1
				lstr_materie_prime[ll_j].cod_prodotto = lstr_trova_mp_varianti_qta_tecnica[ll_i].cod_materia_prima
				lstr_materie_prime[ll_j].quan_tecnica_mag = lstr_trova_mp_varianti_qta_tecnica[ll_i].quan_tecnica
				lstr_materie_prime[ll_j].quan_utilizzo_mag = 0
				lstr_materie_prime[ll_j].ordinamento = lstr_trova_mp_varianti_qta_tecnica[ll_i].ordinamento
				guo_functions.uof_log("Materia Prima: " + lstr_materie_prime[ll_j].cod_prodotto + " Q.ta Tecnica:" + string(lstr_materie_prime[ll_j].quan_tecnica_mag) + " Ord:" + string(lstr_materie_prime[ll_j].ordinamento))
			end if
		next
		
		lb_found = false
		for ll_i = 1 to upperbound(lstr_trova_mp_varianti_qta_utilizzo[])
			
			lb_found = false
			for ll_j = 1 to upperbound(lstr_materie_prime[])
				if lstr_trova_mp_varianti_qta_utilizzo[ll_i].cod_materia_prima = lstr_materie_prime[ll_j].cod_prodotto then
					lstr_materie_prime[ll_j].quan_utilizzo_mag += lstr_trova_mp_varianti_qta_utilizzo[ll_i].quan_utilizzo
					guo_functions.uof_log("Materia Prima (Aggiunta): " + lstr_materie_prime[ll_j].cod_prodotto + " Q.ta Utilizzo:" + string(lstr_trova_mp_varianti_qta_utilizzo[ll_i].quan_utilizzo) + " Ord:" + string(lstr_materie_prime[ll_j].ordinamento))
					lb_found = true
					exit				
				end if
			next
			
			if not lb_found then
				ll_j = upperbound(lstr_materie_prime) + 1
				lstr_materie_prime[ll_j].cod_prodotto = lstr_trova_mp_varianti_qta_tecnica[ll_i].cod_materia_prima
				lstr_materie_prime[ll_j].quan_utilizzo_mag = lstr_trova_mp_varianti_qta_utilizzo[ll_i].quan_utilizzo
				lstr_materie_prime[ll_j].quan_tecnica_mag = 0
				lstr_materie_prime[ll_j].ordinamento = lstr_trova_mp_varianti_qta_tecnica[ll_i].ordinamento
				guo_functions.uof_log("Materia Prima: " + lstr_materie_prime[ll_j].cod_prodotto + " Q.ta Utilizzo:" + string(lstr_materie_prime[ll_j].quan_utilizzo_mag) + " Ord:" + string(lstr_materie_prime[ll_j].ordinamento))
			end if			
			
		next
		
		// calcolo prezzi e sconti dei prodotti
		
		for ll_i = 1 to upperbound(lstr_materie_prime)
			select 	des_prodotto,
						cod_misura_mag,
						cod_misura_ven,
						fat_conversione_ven
			into		:lstr_materie_prime[ll_i].des_prodotto,
						:lstr_materie_prime[ll_i].cod_misura_mag,
						:lstr_materie_prime[ll_i].cod_misura_ven,
						:lstr_materie_prime[ll_i].fat_conversione_ven
			from		anag_prodotti
			where	cod_azienda = :s_cs_xx_cod_azienda and
						cod_prodotto = :lstr_materie_prime[ll_i].cod_prodotto;			
			if sqlca.sqlcode = 100 then
				auo_return_base = uof_error(-1, "uof_carica_sfuso(). Prodotto " + lstr_materie_prime[ll_i].cod_prodotto + " non trovato in anagrafica magazzino ")
				return -1
			elseif sqlca.sqlcode < 0 then
				auo_return_base = uof_error(-1, "uof_carica_sfuso(). Errore SQL in ricerca prodotto " + lstr_materie_prime[ll_i].cod_prodotto + ". Dettaglio Errore " + sqlca.sqlerrtext)
				return -1
			end if	
			
			if isnull(lstr_materie_prime[ll_i].quan_utilizzo_mag) or lstr_materie_prime[ll_i].quan_utilizzo_mag = 0 then
				ld_quan_vendita_listino = 1
			else
				ld_quan_vendita_listino = lstr_materie_prime[ll_i].quan_utilizzo_mag
			end if
			
			
			if uof_calcola_condizioni( 	upper(astr_carica_sfuso_input.as_tipo_gestione), &
												il_anno_documento, &
												il_num_documento, &
												ll_prog_riga_documento, &
												ls_cod_anagrafica_listino, &
												ls_cod_tipo_listino_prodotto, &
												ls_cod_valuta, &
												ld_cambio_ven, &
												ls_cod_lingua_cliente, &
												ls_cod_agente_1, &
												datetime(today(),00:00:00), &
												lstr_materie_prime[ll_i].cod_prodotto, &
												ls_cod_versione_modello, &
												ld_quan_vendita, &
												0, &
												0, &
												ref lstr_materie_prime[ll_i].prezzo_vendita_mag, &
												ref lstr_materie_prime[ll_i].prezzo_vendita_ven, &
												ref lstr_materie_prime[ll_i].fat_conversione_ven, &
												ref lstr_materie_prime[ll_i].sconto_1, &
												ref lstr_materie_prime[ll_i].sconto_2, &
												ref lstr_materie_prime[ll_i].provvigione_1, &
												ref lstr_materie_prime[ll_i].provvigione_2, &
												ref ls_errore) < 0 then
				auo_return_base= uof_error(-1, "Errore nel calcolo del prezzo." + guo_functions.uof_format(ls_errore ))
				return -1
			end if
			
			// converto la quantità se necessario
			if lstr_materie_prime[ll_i].fat_conversione_ven = 0 then
				lstr_materie_prime[ll_i].quan_utilizzo_ven = 0
				lstr_materie_prime[ll_i].quan_tecnica_ven = 0
			else
				lstr_materie_prime[ll_i].quan_utilizzo_ven = round(lstr_materie_prime[ll_i].quan_utilizzo_mag * lstr_materie_prime[ll_i].fat_conversione_ven,4)
				lstr_materie_prime[ll_i].quan_tecnica_ven = round(lstr_materie_prime[ll_i].quan_tecnica_mag * lstr_materie_prime[ll_i].fat_conversione_ven,4)
			end if
			
			// carico l'eventuale descrizione in lingua
			if not isnull(ls_cod_lingua_cliente) then
				select 	des_prodotto
				into 		:ls_des_prodotto_lingua
				from 		anag_prodotti_lingue
				where 	cod_azienda = :s_cs_xx_cod_azienda and
							cod_prodotto = :lstr_materie_prime[ll_i].cod_prodotto and
							cod_lingua = :ls_cod_lingua_cliente;
				
				if sqlca.sqlcode = 0 and not isnull(ls_des_prodotto_lingua) then
					lstr_materie_prime[ll_i].des_prodotto = ls_des_prodotto_lingua
				end if
			end if
			
			lb_found=true
		next	
		
	else		
		// da qui iniziano gli algoritmi dedicati al caricamento del confezionato
		
		guo_functions.uof_log("Inizio calcolo prezzo confezionato nel confezionato")
	
		if uof_calcola_condizioni( 	upper(astr_carica_sfuso_input.as_tipo_gestione), &
											il_anno_documento, &
											il_num_documento, &
											ll_prog_riga_documento, &
											ls_cod_anagrafica_listino, &
											ls_cod_tipo_listino_prodotto, &
											ls_cod_valuta, &
											ld_cambio_ven, &
											ls_cod_lingua_cliente, &
											ls_cod_agente_1, &
											datetime(today(),00:00:00), &
											astr_carica_sfuso_input.as_cod_prodotto_modello, &
											ls_cod_versione_modello, &
											ld_quan_vendita, &
											ld_dim_x, &
											ld_dim_y, &
											ref ld_prezzo_vendita_mag, &
											ref ld_prezzo_vendita_ven, &
											ref ld_fat_conversione_ven, &
											ref ld_sconto_1, &
											ref ld_sconto_2, &
											ref ld_provvigione_1, &
											ref ld_provvigione_2, &
											ref ls_errore) < 0 then
			auo_return_base= uof_error(-1, "Errore nel calcolo del prezzo." + guo_functions.uof_format(ls_errore ))
			return -1
		end if
			
		guo_functions.uof_log("Fine calcolo prezzo confezionato")
		
		// ---------------  EnMe 24-03-2016 Calcolo del costo del TELO che va sommato al costo della struttura --------
		
		guo_functions.uof_log("Verifico se calcolare anche il prezzo del TELO")
		
		if not isnull(ls_cod_variabile_telo) then
			
			guo_functions.uof_log("Inizio calcolo prezzo TELO da sommare alla struttura")
			
			select 	valore_stringa
			into	:ls_cod_prodotto_telo
			from	det_off_ven_conf_variabili
			where 	cod_azienda = :s_cs_xx_cod_azienda and
					anno_registrazione = :il_anno_documento and
					num_registrazione = :il_num_documento and
					prog_riga_off_ven = :ll_prog_riga_documento and
					cod_variabile = :ls_cod_variabile_telo;
			if sqlca.sqlcode =  0 then
				// ho trovato la variabile del telo confezionato, altrimenti salto
				if isnull(ls_cod_prodotto_telo) then
					guo_functions.uof_log( "Calcolo prezzo TELO; codice telo finito = <null>")
				else
					// cerco eventuale versione TELO
					ld_prezzo_vendita_ven_telo = 0
					
					select cod_versione
					into	:ls_cod_versione_telo
					from	distinta_padri
					where cod_azienda = :s_cs_xx_cod_azienda and
							cod_prodotto = :ls_cod_prodotto_telo and
							flag_predefinita = 'S';
							
					if sqlca.sqlcode < 0 then 
						guo_functions.uof_log( "Versione default TELO = Errore SQL nella ricerca della versione nella Distinta Base." + sqlca.sqlerrtext)
						setnull(ls_cod_versione_telo)
					elseif sqlca.sqlcode= 100 then
						guo_functions.uof_log( "Versione default TELO = <null>; verificare flag_ipertech in distinta padri; calcolo TELO saltato!")
						setnull(ls_cod_versione_telo)
					end if
					
					guo_functions.uof_log( "Versione default TELO = " + ls_cod_versione_telo)
				
					guo_functions.uof_log( "Calcolo prezzo TELO; codice telo finito = " + ls_cod_prodotto_telo)
					
					ld_fat_conversione_ven_telo = 1
				
					if uof_calcola_condizioni( 	upper(astr_carica_sfuso_input.as_tipo_gestione), &
														il_anno_documento, &
														il_num_documento, &
														ll_prog_riga_documento, &
														ls_cod_anagrafica_listino, &
														ls_cod_tipo_listino_prodotto, &
														ls_cod_valuta, &
														ld_cambio_ven, &
														ls_cod_lingua_cliente, &
														ls_cod_agente_1, &
														datetime(today(),00:00:00), &
														ls_cod_prodotto_telo, &
														ls_cod_versione_telo, &
														ld_quan_vendita, &
														ld_dim_x, &
														ld_dim_y, &
														ref ld_prezzo_vendita_mag_telo, &
														ref ld_prezzo_vendita_ven_telo, &
														ref ld_fat_conversione_ven_telo, &
														ref ld_sconto_1_telo, &
														ref ld_sconto_2_telo, &
														ref ld_provvigione_1_telo, &
														ref ld_provvigione_2_telo, &
														ref ls_errore) < 0 then
						auo_return_base= uof_error(-1, "Errore nel calcolo del prezzo." + guo_functions.uof_format(ls_errore ))
						return -1
					end if
					if not isnull(ld_prezzo_vendita_ven_telo) and ld_prezzo_vendita_ven_telo > 0 then
						ld_prezzo_vendita_ven += ld_prezzo_vendita_ven_telo
						guo_functions.uof_log("Fine calcolo prezzo telo. Prezzo = " +string(ld_prezzo_vendita_ven_telo))
					else
						guo_functions.uof_log("Fine calcolo prezzo telo. Prezzo = 0")
					end if
						
//					end if						
				end if
			end if
			// ---------------  EnMe 24-03-2016 FINE Calcolo del costo del TELO  -------------
			
		end if

		// carico eventuali descrizioni di dettaglio oppure descrizioni prodotto provenienti da variabili
		// le variabili usate per la nota prodotto iniziano con NOTP01, NOTP02, etc ...
		// le variabili usate per la nota dettaglio iniziano con NOTP01, NOTP02, etc ...
			
		if uof_crea_note(upper(astr_carica_sfuso_input.as_tipo_gestione), il_anno_documento, il_num_documento, ll_prog_riga_documento, ls_nota_prodotto, ls_nota_dettaglio, ls_errore)	< 0 then
			return -1
		end if

		
		choose case upper(astr_carica_sfuso_input.as_tipo_gestione)
			case "OFF_VEN"
			
				update	det_off_ven
				set 		data_consegna = :ldt_data_consegna,
							prezzo_vendita = :ld_prezzo_vendita_ven,
							sconto_1 = :ld_sconto_1,
							sconto_2 = :ld_sconto_2,
							provvigione_1 = :ld_provvigione_1,
							provvigione_2 = :ld_provvigione_2,
							nota_dettaglio = :ls_nota_dettaglio,
							fat_conversione_ven = :ld_fat_conversione_ven
				where 	cod_azienda = :s_cs_xx_cod_azienda and
							anno_registrazione = :il_anno_documento and
							num_registrazione = :il_num_documento and
							prog_riga_off_ven = :ll_prog_riga_documento;
				if sqlca.sqlcode < 0 then
					auo_return_base= uof_error(-1, "Errore in aggiornamento condizioni commerciali riga " + guo_functions.uof_format(ll_prog_riga_documento) + " del documento." + guo_functions.uof_format(sqlca.sqlerrtext))
					return -1
				end if
				guo_functions.uof_log("Eseguito Update riga offerta con prezzi,sconti e provvigioni")
			
			case "ORD_VEN"
			
				update	det_ord_ven
				set 		data_consegna = :ldt_data_consegna,
							prezzo_vendita = :ld_prezzo_vendita_ven,
							sconto_1 = :ld_sconto_1,
							sconto_2 = :ld_sconto_2,
							provvigione_1 = :ld_provvigione_1,
							provvigione_2 = :ld_provvigione_2,
							nota_dettaglio = :ls_nota_dettaglio,
							nota_prodotto = :ls_nota_prodotto,
							fat_conversione_ven = :ld_fat_conversione_ven
				where 	cod_azienda = :s_cs_xx_cod_azienda and
							anno_registrazione = :il_anno_documento and
							num_registrazione = :il_num_documento and
							prog_riga_ord_ven = :ll_prog_riga_documento;
				if sqlca.sqlcode < 0 then
					auo_return_base= uof_error(-1, "Errore in aggiornamento condizioni commerciali riga " + guo_functions.uof_format(ll_prog_riga_documento) + " del documento." + guo_functions.uof_format(sqlca.sqlerrtext))
					return -1
				end if
				guo_functions.uof_log("Eseguito Update riga ordine con prezzi,sconti e provvigioni")
			
		end choose		
		
		// EnMe x Dekora 28/8/2014: caricamento optional e descrizione del prodotto
		guo_functions.uof_log("Procedo con creazione di eventuali optionals")
		lstr_optionals = lstr_vuoto
		ll_ret = uof_optionals( astr_carica_sfuso_input.as_tipo_gestione ,il_anno_documento, il_num_documento, ll_prog_riga_documento, ref lstr_optionals[], ref ls_errore)
		
		if ll_ret < 0 then
			auo_return_base= uof_error(-1, "Errore in inserimento optionals riga ordine " + guo_functions.uof_format(ll_prog_riga_documento) + " del documento." + ls_errore)
			return -1
		end if
		
		// calcolo condizioni per ogni optional
		
		for ll_i = 1 to upperbound(lstr_optionals)
		
			guo_functions.uof_log("Calcolo Prezzo optional:" + guo_functions.uof_format(lstr_optionals[ll_i].cod_optional))
			
			// EnMe 24-5-2016 aggiunto variabili dim_x, dim_y per ADDizionali con prezzo a griglia (chiesto da MArco Marinello)
			
			if uof_calcola_condizioni( 	upper(astr_carica_sfuso_input.as_tipo_gestione), &
												il_anno_documento, &
												il_num_documento, &
												lstr_optionals[ll_i].riga_ordine, &
												ls_cod_anagrafica_listino, &
												ls_cod_tipo_listino_prodotto, &
												ls_cod_valuta, &
												ld_cambio_ven, &
												ls_cod_lingua_cliente, &
												ls_cod_agente_1, &
												datetime(today(),00:00:00), &
												lstr_optionals[ll_i].cod_optional, &
												ls_cod_versione_modello, &
												lstr_optionals[ll_i].quan_optional, &
												ld_dim_x, &
												ld_dim_y, &
												ref ld_prezzo_vendita_mag, &
												ref ld_prezzo_vendita_ven, &
												ref ld_fat_conversione_ven, &
												ref ld_sconto_1, &
												ref ld_sconto_2, &
												ref ld_provvigione_1, &
												ref ld_provvigione_2, &
												ref ls_errore) < 0 then
				auo_return_base= uof_error(-1, "Errore nel calcolo del prezzo." + guo_functions.uof_format(ls_errore ))
				return -1
			end if
			
			choose case upper(astr_carica_sfuso_input.as_tipo_gestione)
				case "OFF_VEN"
				
					update	det_off_ven
					set 		data_consegna = :ldt_data_consegna,
								prezzo_vendita = :ld_prezzo_vendita_ven,
								sconto_1 = :ld_sconto_1,
								sconto_2 = :ld_sconto_2,
								provvigione_1 = :ld_provvigione_1,
								provvigione_2 = :ld_provvigione_2,
								quan_offerta=:lstr_optionals[ll_i].quan_optional
					where 	cod_azienda = :s_cs_xx_cod_azienda and
								anno_registrazione = :il_anno_documento and
								num_registrazione = :il_num_documento and
								prog_riga_off_ven = :lstr_optionals[ll_i].riga_ordine;
					if sqlca.sqlcode < 0 then
						auo_return_base= uof_error(-1, "Errore in aggiornamento condizioni commerciali riga " + guo_functions.uof_format(lstr_optionals[ll_i].riga_ordine) + " del documento." + guo_functions.uof_format(sqlca.sqlerrtext))
						return -1
					end if
					guo_functions.uof_log("Eseguito Update riga offerta con prezzi,sconti e provvigioni")
				
				case "ORD_VEN"
				
					update	det_ord_ven
					set 		data_consegna = :ldt_data_consegna,
								prezzo_vendita = :ld_prezzo_vendita_ven,
								sconto_1 = :ld_sconto_1,
								sconto_2 = :ld_sconto_2,
								provvigione_1 = :ld_provvigione_1,
								provvigione_2 = :ld_provvigione_2,
								quan_ordine =:lstr_optionals[ll_i].quan_optional
					where 	cod_azienda = :s_cs_xx_cod_azienda and
								anno_registrazione = :il_anno_documento and
								num_registrazione = :il_num_documento and
								prog_riga_ord_ven = :lstr_optionals[ll_i].riga_ordine;
					if sqlca.sqlcode < 0 then
						auo_return_base= uof_error(-1, "Errore in aggiornamento condizioni commerciali riga " + guo_functions.uof_format(lstr_optionals[ll_i].riga_ordine) + " del documento." + guo_functions.uof_format(sqlca.sqlerrtext))
						return -1
					end if
					guo_functions.uof_log("Eseguito Update riga ordine con prezzi,sconti e provvigioni")
				
			end choose		
		
		next
		
		
		
		// Fine 28/8/2014 ----------------------------------------------------------------------------
		
	
	end if
	// --------------- fine script caricamento sfuso  ----------------------

catch (Exception ex)
	rollback;
	uof_disconnect_db()
	auo_return_base = uof_error(-1, ex.text)
	return -1
end try

astr_carica_sfuso_output.astr_materie_prime = lstr_materie_prime

guo_functions.uof_log("Fine Procedura:  eseguo commit complessivo")

commit;

// fine delle transazioni sul DB

lt_fine= now()

ll_secondi = secondsafter(lt_inizio, lt_fine)
guo_functions.uof_log("Fine Configurazione; durata elaborazione " + string(ll_secondi) + " secondi")

astr_carica_sfuso_output.ai_time = ll_secondi

if astr_carica_sfuso_input.as_tipo_gestione="OFF_VEN" then
	astr_carica_sfuso_output.as_tipo_doc="O"
else
	astr_carica_sfuso_output.as_tipo_doc="V"
end if	

astr_carica_sfuso_output.al_anno_documento =il_anno_documento
astr_carica_sfuso_output.al_num_documento =il_num_documento
astr_carica_sfuso_output.al_riga_documento =ll_prog_riga_documento

return 0

end function

on uo_cs_ws.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cs_ws.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

