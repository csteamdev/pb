﻿$PBExportHeader$uo_array.sru
forward
global type uo_array from nonvisualobject
end type
end forward

global type uo_array from nonvisualobject
end type
global uo_array uo_array

type variables
/* */
private:
	string is_keys[]
	any ia_values[]
	
	int ii_last_index_search
end variables

forward prototypes
public function boolean haskey (string as_key)
public function boolean remove (string as_key)
public function any get (string as_key)
public function boolean set (string as_key, any aa_value)
public subroutine removeall ()
public function integer uof_get_keys (ref string as_keys[])
end prototypes

public function boolean haskey (string as_key);/**
 * stefanop
 * 12/10/2011
 *
 * Controllo se esiste la chiave all'interno dell'array
 **/
 
int li_i

if UpperBound(is_keys) = 0 then
	return false
else
	
	for li_i = 1 to upperbound(is_keys)
		
		if is_keys[li_i] = as_key then 
			ii_last_index_search = li_i
			return true
		end if
		
	next
	
	return false
	
end if
end function

public function boolean remove (string as_key);/**
 * stefanop
 * 12/10/2011
 *
 * Rimuovo l'elemento se presente
 **/
 
 if hasKey(as_key) then
	
	setnull(is_keys[ii_last_index_search])
	setnull(ia_values[ii_last_index_search])
	return true

else
	
	return false
	
end if
end function

public function any get (string as_key);/**
 * stefanop
 * 12/10/2011
 *
 * Ottiene il valore in base alla chiave passata
 **/
 
any la_value

setnull(la_value)
 
if hasKey(as_key) then
	
	la_value = ia_values[ii_last_index_search]
	
end if

return la_value
end function

public function boolean set (string as_key, any aa_value);/**
 * stefanop
 * 12/10/2011
 *
 * Inserisco o aggiorno l'oggetto nella posizione indicata
 **/
 
if hasKey(as_key) then
	
	ia_values[ii_last_index_search] = aa_value
	
else
	
	ii_last_index_search = upperbound(is_keys) + 1
	
	is_keys[ii_last_index_search] = as_key
	ia_values[ii_last_index_search] = aa_value
	
end if

return true
end function

public subroutine removeall ();/**
 * stefanop
 * 13/10/2011
 *
 * Pulisce tutte le variabiliù
 **/
 
string ls_empty_array[]
any la_empty_array[]

is_keys = ls_empty_array
ia_values = la_empty_array

ii_last_index_search = -1
end subroutine

public function integer uof_get_keys (ref string as_keys[]);as_keys = is_keys
return upperbound(as_keys)
end function

on uo_array.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_array.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

