﻿$PBExportHeader$uo_crea_documento.sru
forward
global type uo_crea_documento from nonvisualobject
end type
end forward

global type uo_crea_documento from nonvisualobject
end type
global uo_crea_documento uo_crea_documento

type variables
public:
string			s_cs_xx_cod_azienda, s_cs_xx_db_funzioni_formato_data

private:
long			il_anno_esercizio

end variables

forward prototypes
private function integer uof_crea_testata_documento (string as_gestione, string as_flag_cliente_contatto, string as_cod_anagrafica, ref long al_anno_documento, ref long al_num_documento, ref string as_errore)
public function integer uof_crea_documento (string as_gestione, string as_flag_cliente_contatto, string as_cod_anagrafica, string as_cod_prodotto_finito, decimal ad_quan_prodotto_finito, ref long al_anno_documento, ref long al_num_documento, ref long al_prog_riga_documento, ref string as_errore)
public function integer uof_get_cambio_vendita (datetime ad_data_riferimento, string as_cod_valuta, ref decimal ad_cambio_ven, ref string as_errore)
public function integer uof_scrivi_variabili (string as_gestione, string as_cod_modello, str_variabili astr_variabili[], ref long al_anno_documento, ref long al_num_documento, ref long al_prog_riga_documento, ref string as_errore)
private function integer uof_get_tipo_det_ven (string as_flag_tipo_riga, string as_cod_prodotto, ref string as_cod_tipo_det_ven, ref string as_errore)
public function integer uof_crea_riga_documento (string as_gestione, string as_cod_prodotto, decimal ad_quan_prodotto_finito, long al_anno_documento, long al_num_documento, string as_flag_tipo_riga, string as_cod_modello, ref long al_prog_riga_documento, ref string as_errore)
public function integer uof_get_dati_prodotto (string as_cod_prodotto, ref string as_cod_versione_distinta, ref string as_flag_gen_commessa, ref string as_cod_tipo_politica_riordino, ref string as_flag_tipo_riordino, ref string as_errore)
end prototypes

private function integer uof_crea_testata_documento (string as_gestione, string as_flag_cliente_contatto, string as_cod_anagrafica, ref long al_anno_documento, ref long al_num_documento, ref string as_errore);/*
FUNZIONE CHE PROVVEDE ALLA CREAZIONE DELLA COMPOSIZIONE

Spiegazione dei parametri
Val		as_gestione					Serve ad identificare l'ambito in cui avviene la configurazione; i valori ammessi sono ORD_VEN / OFF_VEN
val		as_flag_cliente_contatto	C=Cliente		O=Contatto
val		as_cod_anagrafica			Codice del cliente o del contatto
Ref		al_anno_documento		Sono 2 variabili indentificative della chiave della riga del documento creata
			al_num_documento
Ref		as_errore						Messaggio do errore in uscita

Return 	0=tutto bene			-1=Errore
*/
string 		ls_cod_tipo_off_ven,ls_cod_deposito,ls_cod_valuta,ls_cod_pagamento, ls_cod_cliente ,ls_cod_contatto
dec{4}		ld_cambio_ven
datetime	ldt_oggi


if isnull(al_anno_documento) or al_anno_documento = 0 then
	
	il_anno_esercizio = 0
	guo_functions.uof_get_parametro_azienda("ESC", il_anno_esercizio)
	
	if il_anno_esercizio = 0 or isnull(il_anno_esercizio) then
		as_errore = "Parametro ESC - anno eservizio non trovato"
		return -1
	end if
	
	al_anno_documento = il_anno_esercizio
	
	setnull(al_num_documento)
	
	select 	max(num_registrazione)
	into		:al_num_documento
	from 		tes_off_ven
	where 	cod_azienda = :s_cs_xx_cod_azienda and
				anno_registrazione = :al_anno_documento;
	if sqlca.sqlcode < 0 then
		as_errore = "Errore nel calcolo del progressivo ordine"
		return -1
	end if
	
	if isnull(al_num_documento) or al_num_documento < 1 then
		al_num_documento = 1
	else
		al_num_documento ++
	end if
	
	setnull(ls_cod_contatto)
	setnull(ls_cod_cliente)
	ldt_oggi = datetime(today(),00:00:00)
	
	
	choose case as_flag_cliente_contatto
		case "C"
	
			select 	cod_deposito, 
						cod_valuta, 
						cod_pagamento
			into		:ls_cod_deposito, 
						:ls_cod_valuta, 
						:ls_cod_pagamento
			from		anag_clienti
			where	cod_azienda = :s_cs_xx_cod_azienda and
						cod_cliente = :as_cod_anagrafica;
						
			if sqlca.sqlcode = 100 then
				as_errore = "Cliente " + as_cod_anagrafica + " non trovato in anagrafica "
				return -1
			elseif sqlca.sqlcode < 0 then
				as_errore = "Errore SQL in ricerca codice cliente " + as_cod_anagrafica +". Dettaglio" + sqlca.sqlerrtext
				return -1
			end if
			
			ls_cod_cliente = as_cod_anagrafica
		
		case "O"
	
			select 	cod_deposito, 
						cod_valuta, 
						cod_pagamento
			into		:ls_cod_deposito, 
						:ls_cod_valuta, 
						:ls_cod_pagamento
			from		anag_contatti
			where	cod_azienda = :s_cs_xx_cod_azienda and
						cod_contatto = :as_cod_anagrafica;
						
			if sqlca.sqlcode = 100 then
				as_errore = "Contatto " + as_cod_anagrafica + " non trovato in anagrafica "
				return -1
			elseif sqlca.sqlcode < 0 then
				as_errore = "Errore SQL in ricerca codice cliente " + as_cod_anagrafica +". Dettaglio" + sqlca.sqlerrtext
				return -1
			end if
			
			ls_cod_contatto = as_cod_anagrafica
			
	end choose
	
	if uof_get_cambio_vendita(ldt_oggi, ls_cod_valuta, ld_cambio_ven, ref as_errore) < 0 then
		as_errore = "uof_crea_testata_documento.Errore estrazione cambio vendita.  " + as_errore
		return -1
	end if
	
	
	choose case as_gestione
		case "ORD_VEN"
			as_errore = "TIPO GESTIONE ORD_VEN non supportata attualmente"
			return -1
		case "OFF_VEN"
			
			select cod_tipo_off_ven_ipertech_hide
			into	:ls_cod_tipo_off_ven
			from	con_off_ven
			where cod_azienda = 'A01';
			
			if sqlca.sqlcode = 100 or isnull(ls_cod_tipo_off_ven) then
				as_errore = "Attenzione manca il TIPO OFFERTA DEFAULT nei parametri vendita."
				return -1
			end if
			if sqlca.sqlcode < 0 then
				as_errore = "Errore SQL in ricerca tipo offerta HIDE del sistema in tabella con_vendite. " + sqlca.sqlerrtext
				return -1
			end if
			
			if isnull(ls_cod_tipo_off_ven) then
				guo_functions.uof_log("uof_crea_testata_documento(): tipo offerta hide NULL")
			else
				guo_functions.uof_log("uof_crea_testata_documento(): tipo offerta hide " + ls_cod_tipo_off_ven)
			end if
			
			insert into tes_off_ven (
				cod_azienda,
				anno_registrazione,
				num_registrazione,
				cod_tipo_off_ven,
				cod_deposito,
				cod_valuta,
				cod_pagamento,
				cod_cliente,
				cod_contatto,
				data_registrazione,
				cambio_ven)
			values (
				:s_cs_xx_cod_azienda,
				:al_anno_documento,
				:al_num_documento,
				:ls_cod_tipo_off_ven,
				:ls_cod_deposito,
				:ls_cod_valuta,
				:ls_cod_pagamento,
				:ls_cod_cliente,
				:ls_cod_contatto,
				:ldt_oggi,
				:ld_cambio_ven
				);
				
			if sqlca.sqlcode < 0 then
				as_errore = "Errore SQL in inserimento offerta. " + sqlca.sqlerrtext
				return -1
			end if
					
	end choose		
	
end if

return 0
end function

public function integer uof_crea_documento (string as_gestione, string as_flag_cliente_contatto, string as_cod_anagrafica, string as_cod_prodotto_finito, decimal ad_quan_prodotto_finito, ref long al_anno_documento, ref long al_num_documento, ref long al_prog_riga_documento, ref string as_errore);/*
FUNZIONE CHE PROVVEDE ALLA CREAZIONE DEL DOCUMENTO DI RIFERIMENTO PER LA COMPOSIZIONE (TESTATA A RIGA DOCUMENTO)

Spiegazione dei parametri
Val		is_gestione					Serve ad identificare l'ambito in cui avviene la configurazione; i valori ammessi sono ORD_VEN / OFF_VEN
Val		as_flag_cliente_contatto	
Val		as_cod_anagrafica			Codice del cliente o del contatto (se c'è il cliente uso quello e non il contatto; il contatto viene usato solo in assenza del cliente)
Ref		al_anno_documento		Sono 3 variabili indentificative della chiave della riga del documento creata
			al_num_documento
			al_prog_riga_documento
Ref		as_errore						Messaggio do errore in uscita

Return 	0=tutto bene			-1=Errore
*/

//guo_functions = create uo_functions
//guo_functions.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda

guo_functions.uof_log("Avvio Funzione uof_crea_testata_documento()")

if uof_crea_testata_documento( as_gestione, as_flag_cliente_contatto, as_cod_anagrafica, ref al_anno_documento,ref al_num_documento, ref as_errore) < 0 then
	guo_functions.uof_log("uof_crea_testata_documento().Errore:" + guo_functions.uof_format(as_errore) )
	return -1
end if

guo_functions.uof_log("Avvio Funzione uof_crea_riga_documento()")

if uof_crea_riga_documento( as_gestione, as_cod_prodotto_finito, ad_quan_prodotto_finito, al_anno_documento, al_num_documento, "N", "", ref al_prog_riga_documento, ref as_errore) < 0 then
	guo_functions.uof_log("uof_crea_riga_documento().Errore:" + guo_functions.uof_format(as_errore) )
	return -1
end if

//destroy guo_functions

return 0
end function

public function integer uof_get_cambio_vendita (datetime ad_data_riferimento, string as_cod_valuta, ref decimal ad_cambio_ven, ref string as_errore);string		ls_errore, ls_sql
datetime 	ldt_data_cambio
long 			ll_i, ll_ret
datastore 	lds_data


ad_cambio_ven = 1

select 	cambio_ven
into   	:ad_cambio_ven    
from   	tab_valute
where  	cod_azienda = :s_cs_xx_cod_azienda and 
       		cod_valuta = :as_cod_valuta;

if sqlca.sqlcode = 0 then
	
	ls_sql = "select cambio_ven from tab_cambi where cod_azienda = '" + s_cs_xx_cod_azienda + "' and cod_valuta = '" + as_cod_valuta + "' and data_cambio <= '" + string(ad_data_riferimento, s_cs_xx_db_funzioni_formato_data) + "' order by cod_azienda, cod_valuta, data_cambio desc "
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data,ls_sql , ls_errore)
	
	if ll_ret < 0 then
		as_errore = "uof_get_cambio_vendita() " + ls_errore
		return -1
	end if
	
	if ll_ret = 0 then 
		destroy lds_data
		return 0
	end if
	
	ad_cambio_ven = lds_data.getitemnumber(1,1)
	
end if

return 0
end function

public function integer uof_scrivi_variabili (string as_gestione, string as_cod_modello, str_variabili astr_variabili[], ref long al_anno_documento, ref long al_num_documento, ref long al_prog_riga_documento, ref string as_errore);/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNZIONE CHE PROVVEDE ALLA MEMORIZZAZIONE DELLE VARIABILI NELLE TABELLE COLLEGATE AGLI ORDINI.

Spiegazione dei parametri
Val		as_gestione					Serve ad identificare l'ambito in cui avviene la configurazione; i valori ammessi sono ORD_VEN / OFF_VEN
Val		str_variabili					Elenco delle variabili da scrivere
Ref		al_anno_documento		Sono 3 variabili indentificative della chiave della riga del documento creata
			al_num_documento
			al_prog_riga_documento
Ref		as_errore						Messaggio do errore in uscita

Return 	0=tutto bene			-1=Errore
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

string 	ls_sql, ls_sql_columns, ls_sql_values,ls_nome_campo_database,ls_nome_tabella_database,ls_flag_tipo_dato
long		ll_num_variabili, ll_i


choose case upper(as_gestione)
	case "ORD_VEN"
		ls_sql_columns = " insert into comp_det_ord_ven (cod_azienda, anno_registrazione, num_registrazione, prog_riga_ord_ven "
		ls_sql_values   = " values ('" + s_cs_xx_cod_azienda + "'," + string(al_anno_documento) + "," + string(al_num_documento) + "," + string(al_prog_riga_documento) 
	case "OFF_VEN"
		ls_sql_columns = " insert into comp_det_off_ven (cod_azienda, anno_registrazione, num_registrazione, prog_riga_off_ven "
		ls_sql_values   = " values ('" + s_cs_xx_cod_azienda + "'," + string(al_anno_documento) + "," + string(al_num_documento) + "," + string(al_prog_riga_documento) 
	case else
		as_errore = "TIPO GESTIONE NON PREVISTA"
		return -1
end choose

ll_num_variabili = upperbound( astr_variabili[] )

guo_functions.uof_log("uof_scrivi_variabili(): Esamino numero " + string(ll_num_variabili) + " variabili ")

for ll_i = 1 to ll_num_variabili
	
	guo_functions.uof_log("uof_scrivi_variabili(): Variabile " + guo_functions.uof_format(ll_i) + " Valore string:" +  guo_functions.uof_format(astr_variabili[ll_i].as_string) + " Valore Number:"  + guo_functions.uof_format(astr_variabili[ll_i].ad_number)  + " flag_uso_formule=" + guo_functions.uof_format(astr_variabili[ll_i].as_uso_formule) )
	
	if astr_variabili[ll_i].as_uso_formule = "S" then 
	
		select 	nome_campo_database, 
					nome_tabella_database, 
					flag_tipo_dato
		into		:ls_nome_campo_database, 
					:ls_nome_tabella_database, 
					:ls_flag_tipo_dato
		from		tab_variabili_formule
		where	cod_azienda = :s_cs_xx_cod_azienda and
					cod_variabile = :astr_variabili[ll_i].as_cod_variabile;
					
		if sqlca.sqlcode = 100 then
			as_errore = "Variabile " + astr_variabili[ll_i].as_cod_variabile + " inesistente nella tabella variabili "
			guo_functions.uof_log(as_errore)
			return -1
		end if
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore SQL in Select variabile " + astr_variabili[ll_i].as_cod_variabile + " da tab_variabili_formule. " + sqlca.sqlerrtext
			guo_functions.uof_log(as_errore)
			return -1
		end if
		
		if isnull(ls_nome_campo_database) or len(trim(ls_nome_campo_database)) < 1 or isnull(ls_nome_tabella_database) or len(trim(ls_nome_tabella_database)) < 1 then
			as_errore = "Operazione Bloccata (uof_scrivi_variabili): La variabile " + astr_variabili[ll_i].as_cod_variabile + " non ha il campo/tabella database impostato. "
			guo_functions.uof_log(as_errore)
			return -1
		end if
		
		guo_functions.uof_log("uof_scrivi_variabili(): Variabile " + guo_functions.uof_format(astr_variabili[ll_i].as_cod_variabile) + " - Tabella: " +  guo_functions.uof_format(ls_nome_tabella_database) + " - Campo DB: " +  guo_functions.uof_format(ls_nome_campo_database) )

		
		if lower(ls_nome_tabella_database) = "comp_det_ord_ven" and ls_nome_campo_database <> "quan_ordine" then
			
			ls_sql_columns += ", " + ls_nome_campo_database
			guo_functions.uof_log("uof_scrivi_variabili(): Progressione Variabile Colonne: " + guo_functions.uof_format(ls_sql_columns) )
			choose case upper(ls_flag_tipo_dato)
				case "S"
					if isnull( astr_variabili[ll_i].as_string) then
						ls_sql_values += ", null "
					else
						ls_sql_values += ",'" + astr_variabili[ll_i].as_string + "' "
					end if
				case "N"
					if isnull(astr_variabili[ll_i].ad_number) then
						ls_sql_values += ", null "
					else
						ls_sql_values += "," + f_decimal_to_string(astr_variabili[ll_i].ad_number)
					end if
				case "D"
					// caso non previsto
			end choose
			guo_functions.uof_log("uof_scrivi_variabili(): Progressione Variabile Valori: " + guo_functions.uof_format(ls_sql_values) )
		end if
		guo_functions.uof_log("uof_scrivi_variabili(): SQL progressivo scrivi variabili: Colonne= " + guo_functions.uof_format(ls_sql_columns) + " - Valori=" +  guo_functions.uof_format(ls_sql_values) )
		
	else
		ls_flag_tipo_dato = astr_variabili[ll_i].as_flag_datatype
	end if		
	
	choose case upper(as_gestione)
		case "ORD_VEN"
			
			guo_functions.uof_log("uof_scrivi_variabili(): Variabile " + guo_functions.uof_format(astr_variabili[ll_i].as_cod_variabile) + " - Procedo con insert into det_ord_ven_conf_variabili")
			
			insert into det_ord_ven_conf_variabili
						(cod_azienda,
						anno_registrazione,
						num_registrazione,
						prog_riga_ord_ven,
						prog_variabile,
						cod_variabile,
						flag_tipo_dato,
						valore_stringa,
						valore_numero)
			values	(:s_cs_xx_cod_azienda,
						:al_anno_documento,
						:al_num_documento,
						:al_prog_riga_documento,
						:ll_i,
						:astr_variabili[ll_i].as_cod_variabile,
						:ls_flag_tipo_dato,
						:astr_variabili[ll_i].as_string,
						:astr_variabili[ll_i].ad_number);
						
			if sqlca.sqlcode < 0 then
				as_errore = "Errore in insert variabile " + astr_variabili[ll_i].as_cod_variabile + ": " + sqlca.sqlerrtext
				guo_functions.uof_log(as_errore)
				return -1
			end if
				
						
		case "OFF_VEN"
			
				guo_functions.uof_log("uof_scrivi_variabili(): Codice Variabile " + guo_functions.uof_format(astr_variabili[ll_i].as_cod_variabile) + " - Procedo con insert into det_off_ven_conf_variabili")
				
				insert into det_off_ven_conf_variabili
							(cod_azienda,
							anno_registrazione,
							num_registrazione,
							prog_riga_off_ven,
							prog_variabile,
							cod_variabile,
							flag_tipo_dato,
							valore_stringa,
							valore_numero)
				values	(:s_cs_xx_cod_azienda,
							:al_anno_documento,
							:al_num_documento,
							:al_prog_riga_documento,
							:ll_i,
							:astr_variabili[ll_i].as_cod_variabile,
							:ls_flag_tipo_dato,
							:astr_variabili[ll_i].as_string,
							:astr_variabili[ll_i].ad_number);
							
				if sqlca.sqlcode < 0 then
					as_errore = "Errore in insert variabile " + astr_variabili[ll_i].as_cod_variabile + ": " + sqlca.sqlerrtext
					guo_functions.uof_log(as_errore)
					return -1
				end if
		case else
			as_errore = "TIPO GESTIONE NON PREVISTA"
			return -1
	end choose
	
next


ls_sql_columns += " )"
ls_sql_values += " )"

ls_sql = ls_sql_columns + "  " +  ls_sql_values

guo_functions.uof_log("SQL INSERT COMP: " + ls_sql)
// da levare
commit;

execute immediate :ls_sql;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in insert dettaglio complementare (uof_scrivi_variabili): " +  sqlca.sqlerrtext
	guo_functions.uof_log(as_errore)
	return -1
end if


return 0
end function

private function integer uof_get_tipo_det_ven (string as_flag_tipo_riga, string as_cod_prodotto, ref string as_cod_tipo_det_ven, ref string as_errore);choose case as_flag_tipo_riga
	case "N"
		select 	cod_tipo_det_ven_prodotto
		into		:as_cod_tipo_det_ven
		from		tab_flags_configuratore
		where	cod_azienda = :s_cs_xx_cod_azienda and
					cod_modello = :as_cod_prodotto	;
		if sqlca.sqlcode = 100 then
			as_errore = "Ricerca Tipo Dettaglio Prodotto Normale: Modello di prodotto "+guo_functions.uof_format(as_cod_prodotto)+" non caricato nella tabella flag_configuratore"
			return -1
		elseif sqlca.sqlcode < 0 then 
			as_errore = "Errore in ricerca tipo dettaglio vendita in flags_configuratore. " + sqlca.sqlerrtext
			return -1
		end if
			
	case "O"
		select 	cod_tipo_det_ven_optionals
		into		:as_cod_tipo_det_ven
		from		tab_flags_configuratore
		where	cod_azienda = :s_cs_xx_cod_azienda and
					cod_modello = :as_cod_prodotto	;
		if sqlca.sqlcode = 100 then
			as_errore = "Ricerca Tipo Dettaglio Prodotto Optional: Modello di prodotto "+guo_functions.uof_format(as_cod_prodotto)+" non caricato nella tabella flag_configuratore"
			return -1
		elseif sqlca.sqlcode < 0 then 
			as_errore = "Errore in ricerca tipo dettaglio vendita in flags_configuratore. " + sqlca.sqlerrtext
			return -1
		end if
			
	case "A"
		select 	cod_tipo_det_ven_addizionali
		into		:as_cod_tipo_det_ven
		from		tab_flags_configuratore
		where	cod_azienda = :s_cs_xx_cod_azienda and
					cod_modello = :as_cod_prodotto	;
		if sqlca.sqlcode = 100 then
			as_errore = "Ricerca Tipo Dettaglio Prodotto Addizionale: Modello di prodotto "+guo_functions.uof_format(as_cod_prodotto)+" non caricato nella tabella flag_configuratore"
			return -1
		elseif sqlca.sqlcode < 0 then 
			as_errore = "Errore in ricerca tipo dettaglio vendita in flags_configuratore. " + sqlca.sqlerrtext
			return -1
		end if
end choose	
			

return 0
end function

public function integer uof_crea_riga_documento (string as_gestione, string as_cod_prodotto, decimal ad_quan_prodotto_finito, long al_anno_documento, long al_num_documento, string as_flag_tipo_riga, string as_cod_modello, ref long al_prog_riga_documento, ref string as_errore);/*
FUNZIONE CHE PROVVEDE ALLA CREAZIONE DELLA COMPOSIZIONE

Spiegazione dei parametri
Val		as_gestione					Serve ad identificare l'ambito in cui avviene la configurazione; i valori ammessi sono ORD_VEN / OFF_VEN
val		as_flag_cliente_contatto	
val		as_cod_anagrafica			Codice del cliente o del contatto (se c'è il cliente uso quello e non il contatto; il contatto viene usato solo in assenza del cliente)

val		al_anno_documento		Sono 2 variabili indentificative della chiave della riga del documento creata
			al_num_documento
			as_flag_tipo_riga			P=Normale riga prodotto 		O=Optional		A=Addizionale
			
Ref		al_prog_riga_documento	INPUT=riga originale prodotto finito 			OUTPUT=Riga del documento che è stata generata
Ref		as_errore						Messaggio do errore in uscita

Return 	0=tutto bene			-1=Errore
*/
long			ll_ret, ll_num_riga_appartenenza
string		ls_cod_tipo_det_ven, ls_cod_iva, ls_des_prodotto, ls_cod_misura_ven,ls_cod_versione, ls_cod_cliente,ls_flag_gen_commessa, ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino
datetime 	ldt_data_registrazione


guo_functions.uof_log("uof_crea_riga_documento(). Entrato nella function=" + guo_functions.uof_format(as_flag_tipo_riga) )

choose case as_flag_tipo_riga
	case "N"
		ll_ret = uof_get_tipo_det_ven( as_flag_tipo_riga, as_cod_prodotto, ref ls_cod_tipo_det_ven, ref as_errore)
		ll_num_riga_appartenenza = 0
		
	case "O"
		ll_ret = uof_get_tipo_det_ven( as_flag_tipo_riga, as_cod_modello, ref ls_cod_tipo_det_ven, ref as_errore)
		ll_num_riga_appartenenza = al_prog_riga_documento	
		
	case "A"
		ll_ret = uof_get_tipo_det_ven( as_flag_tipo_riga, as_cod_modello, ref ls_cod_tipo_det_ven, ref as_errore)
		ll_num_riga_appartenenza = al_prog_riga_documento	
end choose	
		
guo_functions.uof_log("uof_crea_riga_documento(). Tipo det ven=" + guo_functions.uof_format(ls_cod_tipo_det_ven) )

		
if ll_ret < 0 then  return -1

select 	cod_iva, 
			des_prodotto,
			cod_misura_ven
into		:ls_cod_iva, 
			:ls_des_prodotto,
			:ls_cod_misura_ven
from		anag_prodotti
where	cod_azienda = :s_cs_xx_cod_azienda and
			cod_prodotto = :as_cod_prodotto	;
if sqlca.sqlcode = 100 then
	as_errore = "Prodotto " + as_cod_prodotto +" non presente in anagrafica"
	return -1
elseif sqlca.sqlcode < 0 then 
	as_errore = "Errore in ricerca prodotto in anagrafica prodotti. " + sqlca.sqlerrtext
	return -1
end if

guo_functions.uof_log("uof_crea_riga_documento(). Trovato prodotto=" + guo_functions.uof_format(as_cod_prodotto) )

if isnull(ls_cod_iva) then
	select 	cod_iva
	into		:ls_cod_iva
	from		tab_tipi_det_ven
	where	cod_azienda = :s_cs_xx_cod_azienda and
				cod_tipo_det_ven = :ls_cod_tipo_det_ven	;
	if sqlca.sqlcode = 100 then
		as_errore = "Tipo dettaglio vendita " + ls_cod_tipo_det_ven + " non trovato "
		return -1
	elseif sqlca.sqlcode < 0 then 
		as_errore = "Errore in ricerca tipo dettaglio vendita. " + sqlca.sqlerrtext
		return -1
	end if
end if	

guo_functions.uof_log("uof_crea_riga_documento(). Trovato tipo dettaglioo=" + guo_functions.uof_format(ls_cod_tipo_det_ven) )


choose case as_gestione
	case "ORD_VEN"
		select 	cod_cliente,
					data_registrazione
		into		:ls_cod_cliente,
					:ldt_data_registrazione
		from		tes_ord_ven
		where	cod_azienda = :s_cs_xx_cod_azienda and
					anno_registrazione = :al_anno_documento and
					num_registrazione = :al_num_documento;
	case "OFF_VEN"			
		select 	cod_cliente,
					data_registrazione
		into		:ls_cod_cliente,
					:ldt_data_registrazione
		from		tes_off_ven
		where	cod_azienda = :s_cs_xx_cod_azienda and
					anno_registrazione = :al_anno_documento and
					num_registrazione = :al_num_documento;
		
end choose

f_trova_codice_iva (s_cs_xx_cod_azienda, ldt_data_registrazione, ls_cod_tipo_det_ven, "C", ls_cod_cliente, ref ls_cod_iva )

guo_functions.uof_log("uof_crea_riga_documento(). Trovato aliquota iva cliente=" + guo_functions.uof_format(ls_cod_iva) )


if uof_get_dati_prodotto(as_cod_prodotto, ls_cod_versione, ls_flag_gen_commessa, ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ref as_errore)	 < 0 then
	return -1
end if

guo_functions.uof_log("uof_crea_riga_documento(). uof_get_dati_prodotto()= VERSIONE:" + guo_functions.uof_format(ls_cod_versione) + " - GEN COMMESSA=" + guo_functions.uof_format(ls_flag_gen_commessa) + " - POLIT RIORD=" + guo_functions.uof_format(ls_cod_tipo_politica_riordino) + " - TIPO RIORD=" + guo_functions.uof_format(ls_flag_tipo_riordino) )

choose case as_gestione
	case "ORD_VEN"
		
		select 	max(prog_riga_ord_ven)
		into		:al_prog_riga_documento
		from		det_ord_ven
		where	cod_azienda = :s_cs_xx_cod_azienda and
					anno_registrazione = :al_anno_documento and
					num_registrazione = :al_num_documento;
					
		if isnull(al_prog_riga_documento) or al_prog_riga_documento < 1 then
			al_prog_riga_documento = 1
		else
			al_prog_riga_documento ++
		end if
		
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_azienda=" + guo_functions.uof_format(s_cs_xx_cod_azienda) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore anno_registrazione=" + guo_functions.uof_format(al_anno_documento) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore num_registrazione=" + guo_functions.uof_format(al_num_documento) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore prog_riga_off_ven=" + guo_functions.uof_format(al_prog_riga_documento) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_tipo_det_ven=" + guo_functions.uof_format(ls_cod_tipo_det_ven) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_prodotto=" + guo_functions.uof_format(as_cod_prodotto) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore des_prodotto=" + guo_functions.uof_format(ls_des_prodotto) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_misura=" + guo_functions.uof_format(ls_cod_misura_ven) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore quan_offerta=" + guo_functions.uof_format(ad_quan_prodotto_finito) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore fat_conversione_ven=" + guo_functions.uof_format(1) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_iva=" + guo_functions.uof_format(ls_cod_iva) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_versione=" + guo_functions.uof_format(ls_cod_versione) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore num_riga_appartenenza=" + guo_functions.uof_format(ll_num_riga_appartenenza) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore flag_gen_commessa=" + guo_functions.uof_format(ls_flag_gen_commessa) )
		
		insert into det_ord_ven (
					cod_azienda,
					anno_registrazione,
					num_registrazione,
					prog_riga_ord_ven,
					cod_tipo_det_ven,
					cod_prodotto,
					des_prodotto,
					cod_misura,
					quan_ordine,
					fat_conversione_ven,
					cod_iva,
					cod_versione,
					num_riga_appartenenza,
					flag_gen_commessa
					)
		values (
					:s_cs_xx_cod_azienda,
					:al_anno_documento,
					:al_num_documento,
					:al_prog_riga_documento,
					:ls_cod_tipo_det_ven,
					:as_cod_prodotto,
					:ls_des_prodotto,
					:ls_cod_misura_ven,
					:ad_quan_prodotto_finito,
					1,
					:ls_cod_iva,
					:ls_cod_versione,
					:ll_num_riga_appartenenza,
					:ls_flag_gen_commessa);
					
		if  sqlca.sqlcode < 0 then 
			guo_functions.uof_log("uof_crea_riga_documento(). Errore SQL =" + sqlca.sqlerrtext)
			as_errore = "Errore in insertimenti riga ordine del prodotto finito. " + sqlca.sqlerrtext
			return -1
		end if
		
	case "OFF_VEN"
	
		select 	max(prog_riga_off_ven)
		into		:al_prog_riga_documento
		from		det_off_ven
		where	cod_azienda = :s_cs_xx_cod_azienda and
					anno_registrazione = :al_anno_documento and
					num_registrazione = :al_num_documento;
					
		if isnull(al_prog_riga_documento) or al_prog_riga_documento < 1 then
			al_prog_riga_documento = 1
		else
			al_prog_riga_documento ++
		end if
		
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_azienda=" + guo_functions.uof_format(s_cs_xx_cod_azienda) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore anno_registrazione=" + guo_functions.uof_format(al_anno_documento) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore num_registrazione=" + guo_functions.uof_format(al_num_documento) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore prog_riga_off_ven=" + guo_functions.uof_format(al_prog_riga_documento) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_tipo_det_ven=" + guo_functions.uof_format(ls_cod_tipo_det_ven) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_prodotto=" + guo_functions.uof_format(as_cod_prodotto) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore des_prodotto=" + guo_functions.uof_format(ls_des_prodotto) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_misura=" + guo_functions.uof_format(ls_cod_misura_ven) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore quan_offerta=" + guo_functions.uof_format(ad_quan_prodotto_finito) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore fat_conversione_ven=" + guo_functions.uof_format(1) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_iva=" + guo_functions.uof_format(ls_cod_iva) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore cod_versione=" + guo_functions.uof_format(ls_cod_versione) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore num_riga_appartenenza=" + guo_functions.uof_format(ll_num_riga_appartenenza) )
		guo_functions.uof_log("uof_crea_riga_documento(). Valore flag_gen_commessa=" + guo_functions.uof_format(ls_flag_gen_commessa) )
		
		
		
		insert into det_off_ven (
					cod_azienda,
					anno_registrazione,
					num_registrazione,
					prog_riga_off_ven,
					cod_tipo_det_ven,
					cod_prodotto,
					des_prodotto,
					cod_misura,
					quan_offerta,
					fat_conversione_ven,
					cod_iva,
					cod_versione,
					num_riga_appartenenza,
					flag_gen_commessa
					)
		values (
					:s_cs_xx_cod_azienda,
					:al_anno_documento,
					:al_num_documento,
					:al_prog_riga_documento,
					:ls_cod_tipo_det_ven,
					:as_cod_prodotto,
					:ls_des_prodotto,
					:ls_cod_misura_ven,
					:ad_quan_prodotto_finito,
					1,
					:ls_cod_iva,
					:ls_cod_versione,
					:ll_num_riga_appartenenza,
					:ls_flag_gen_commessa
					);
					
		if  sqlca.sqlcode < 0 then 
			guo_functions.uof_log("uof_crea_riga_documento(). Errore SQL =" + sqlca.sqlerrtext)
			as_errore = "Errore in insert riga offerta del prodotto finito. " + sqlca.sqlerrtext
			return -1
		end if
	
end choose			

guo_functions.uof_log("uof_crea_riga_documento(). Fine function; tutto ok")

return 0
end function

public function integer uof_get_dati_prodotto (string as_cod_prodotto, ref string as_cod_versione_distinta, ref string as_flag_gen_commessa, ref string as_cod_tipo_politica_riordino, ref string as_flag_tipo_riordino, ref string as_errore);setnull(as_cod_versione_distinta)
setnull(as_flag_gen_commessa)
setnull(as_cod_tipo_politica_riordino)
setnull(as_flag_tipo_riordino)


select 	cod_versione
into   		:as_cod_versione_distinta
from   	distinta_padri
where  	cod_azienda = :s_cs_xx_cod_azienda and
			cod_prodotto = :as_cod_prodotto and
			flag_default_ipertech = 'S';

if sqlca.sqlcode = 0 then
	as_flag_gen_commessa = "S"
elseif sqlca.sqlcode = 100 then
	as_flag_gen_commessa = "N"
	return 1
else
	as_errore = "Errore in ricerca versione in distinta padri " + sqlca.sqlerrtext
	return -1
end if

select 	cod_tipo_politica_riordino
into   		:as_cod_tipo_politica_riordino
from   	anag_prodotti
where  	cod_azienda = :s_cs_xx_cod_azienda and
		 	cod_prodotto = :as_cod_prodotto;
if sqlca.sqlcode = 0 then
	if not isnull(as_cod_tipo_politica_riordino) then
		select flag_tipo_riordino
		into   :as_flag_tipo_riordino
		from   tab_tipi_politiche_riordino
		where  cod_azienda = :s_cs_xx_cod_azienda and
				 cod_tipo_politica_riordino = :as_cod_tipo_politica_riordino;
		if sqlca.sqlcode = 0 and not isnull(as_flag_tipo_riordino) then
			choose case as_flag_tipo_riordino
				case "C"
					as_flag_gen_commessa = "S"
				case "A", "T"
					as_flag_gen_commessa = "N"
				case else
					as_flag_gen_commessa = "N"
			end choose					
		end if			
	end if
end if


return 0
end function

on uo_crea_documento.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_crea_documento.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

