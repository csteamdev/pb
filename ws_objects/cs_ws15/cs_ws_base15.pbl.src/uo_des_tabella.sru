﻿$PBExportHeader$uo_des_tabella.sru
forward
global type uo_des_tabella from nonvisualobject
end type
end forward

global type uo_des_tabella from nonvisualobject
end type
global uo_des_tabella uo_des_tabella

type variables
string s_cs_xx_cod_azienda
string s_cs_xx_cod_lingua_applicazione
end variables

forward prototypes
public function string f_des_tabella (string f_nome_tabella, string f_where, string f_colonna_des)
public function decimal f_dec_tabella (string f_nome_tabella, string f_where, string f_colonna_des)
public function datetime f_datetime_tabella (string f_nome_tabella, string f_where, string f_colonna_des)
end prototypes

public function string f_des_tabella (string f_nome_tabella, string f_where, string f_colonna_des);long   ll_return, ll_pos
string ls_sql, ls_return1, ls_return2, ls_null,ls_profilo_corrente, ls_db

setnull(ls_null)
if not isnull(f_where) then
	
	if not isnull(s_cs_xx_cod_lingua_applicazione)  then
		string ls_nome_pk_1, ls_nome_pk_2
		
		select nome_pk_1, nome_pk_2
		into   :ls_nome_pk_1, :ls_nome_pk_2
		from   tab_tabelle_lingue
		where  nome_tabella = :f_nome_tabella;
	
		if sqlca.sqlcode = 0 then
			ls_sql =  " SELECT "+ f_nome_tabella +"." + f_colonna_des +", tab_tabelle_lingue_des.descrizione_lingua " + &
			          " FROM   "+f_nome_tabella+" LEFT OUTER JOIN tab_tabelle_lingue_des ON " +  &
						 " tab_tabelle_lingue_des.cod_azienda = " + f_nome_tabella + ".cod_azienda "
							if not isnull(ls_nome_pk_1) and len(ls_nome_pk_1) > 0 then
								ls_sql += " and tab_tabelle_lingue_des.chiave_str_1 = " + f_nome_tabella + "." + ls_nome_pk_1
							end if
							
							if not isnull(ls_nome_pk_2) and len(ls_nome_pk_2) > 0 then
								ls_sql += " and tab_tabelle_lingue_des.chiave_str_2 = " + f_nome_tabella + "." + ls_nome_pk_2
							end if
							ls_sql += " and tab_tabelle_lingue_des.cod_lingua = '" + s_cs_xx_cod_lingua_applicazione + "' "
							ls_sql += " WHERE " + f_where + " AND " + f_nome_tabella + ".cod_azienda = '" + s_cs_xx_cod_azienda + "'"
						 
						 
						 
//						 " WHERE " + f_where + " AND " + f_nome_tabella + ".cod_azienda = '" + s_cs_xx_cod_azienda + "'" + &
//						 " and tab_tabelle_lingue_des.cod_azienda = " + f_nome_tabella + ".cod_azienda"
	
	
		else
			ls_sql = "SELECT " + f_colonna_des + ", " + f_colonna_des + " FROM " + f_nome_tabella + " WHERE " + f_where + " AND cod_azienda = '" + s_cs_xx_cod_azienda + "'"
			
		end if
		
	else
		ls_sql = "SELECT " + f_colonna_des + ", " + f_colonna_des + " FROM " + f_nome_tabella + " WHERE " + f_where + " AND cod_azienda = '" + s_cs_xx_cod_azienda + "'"
	end if
	// Verifico tipo di database
	
	
	
	DECLARE cursore DYNAMIC CURSOR FOR SQLSA ;
	PREPARE SQLSA FROM :ls_sql ;
	OPEN DYNAMIC cursore ;
	FETCH cursore INTO :ls_return1, :ls_return2 ;
	if sqlca.sqlcode = 100 then
		ls_return1 = "< CODICE INESISTENTE >"
		ls_return2 = "< UNEXISTING CODE >"
	end if
	CLOSE cursore ;
		
	if not isnull(ls_return2) and len(trim(ls_return2)) > 0 then 
		return ls_return2 
	else
		return ls_return1
	end if
else
	return ls_null
end if

return ls_null
end function

public function decimal f_dec_tabella (string f_nome_tabella, string f_where, string f_colonna_des);string ls_sql

dec{6} ld_return, ld_null

setnull(ld_null)

if not isnull(f_where) then
	
	ls_sql = "SELECT " + f_colonna_des + " FROM " + f_nome_tabella + " WHERE " + f_where + " AND cod_azienda = '" + s_cs_xx_cod_azienda + "'"
	
	declare cursore dynamic cursor for sqlsa;
	
	prepare sqlsa from :ls_sql;
	
	open dynamic cursore;
	
	fetch cursore into :ld_return;
	
	if sqlca.sqlcode = 100 then
		ld_return = 0
	end if
	
	close cursore;
	
	return ld_return
	
else
	
	return ld_null
	
end if
end function

public function datetime f_datetime_tabella (string f_nome_tabella, string f_where, string f_colonna_des);string ls_sql

datetime ldt_return, ldt_null

setnull(ldt_null)

if not isnull(f_where) then
	
	ls_sql = "SELECT " + f_colonna_des + " FROM " + f_nome_tabella + " WHERE " + f_where + " AND cod_azienda = '" + s_cs_xx_cod_azienda + "'"
	
	declare cursore dynamic cursor for sqlsa;
	
	prepare sqlsa from :ls_sql;
	
	open dynamic cursore;
	
	fetch cursore into :ldt_return;
	
	if sqlca.sqlcode = 100 then
		ldt_return = datetime(date(1900, 1, 1), 00:00:00)
	end if
	
	close cursore;
	
	return ldt_return
	
else
	
	return ldt_null
	
end if
end function

on uo_des_tabella.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_des_tabella.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

