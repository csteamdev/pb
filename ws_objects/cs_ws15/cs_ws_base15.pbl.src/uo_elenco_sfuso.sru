﻿$PBExportHeader$uo_elenco_sfuso.sru
forward
global type uo_elenco_sfuso from nonvisualobject
end type
end forward

global type uo_elenco_sfuso from nonvisualobject
end type
global uo_elenco_sfuso uo_elenco_sfuso

type variables
public:
string	cod_prodotto_sfuso
string	des_prodotto_sfuso
decimal	quan_tecnica
decimal	quan_commerciale
decimal	prezzo_vendita
end variables

on uo_elenco_sfuso.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_elenco_sfuso.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

