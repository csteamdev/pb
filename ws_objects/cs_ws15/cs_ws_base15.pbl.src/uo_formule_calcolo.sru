﻿$PBExportHeader$uo_formule_calcolo.sru
forward
global type uo_formule_calcolo from nonvisualobject
end type
end forward

global type uo_formule_calcolo from nonvisualobject
end type
global uo_formule_calcolo uo_formule_calcolo

type variables
constant string FORMULA_OK = "FORMULA_@K"
constant string ERRORE_FORMULA = "ERROREFORMUL@"
constant string PATTERN_VARIABLES = "([^0-9;=><\[\]/+,-/*/(/)//^]\w{0,})"				//"([^0-9+,-/*/(/)//^]\w+)"


private:
	boolean ib_ferma_calcolo = false
	string is_formule[], is_valori[], is_tipo[], is_vuoto[], is_cache_cod_variabile[], is_cache_val_variabile[]
	dec{4} id_costanti[], id_vuoto[], idc_internal_result
	
	boolean ib_lista_variabili = false
	string is_lista_variabili[]

public:
	uo_array		iuo_cache
	integer		ii_anno_reg_ord_ven = 0
	long			il_num_reg_ord_ven = 0
	long			il_prog_riga_ord_ven = 0
	string			is_nome_tabella = ""
	boolean		ib_gibus = false
	
	string			is_cod_prodotto_padre = ""
	string			is_cod_prodotto_figlio = ""
	string			is_cod_versione_padre = ""
	string			is_cod_versione_figlio = ""
	long			il_num_sequenza=0
	
	integer		ii_anno_commessa = 0
	long			il_num_commessa = 0
	
	string			is_tipo_ritorno="1"		//"1" numerico    "2"  stringa
	string			is_contesto = ""

// Aggiunte rispetto all'oggetto standard
	string s_cs_xx_cod_azienda, s_cs_xx_db_funzioni_formato_data


end variables

forward prototypes
public subroutine uof_rimpiazza_carattere (ref string fs_stringa, string fs_old_char, string fs_new_char)
public function integer uof_calcola_formula (string fs_espressione, ref string fs_select, ref decimal fdc_risultato, ref string fs_errore)
public function string uof_variabili (string fs_formula, string fs_cod_formula, ref string fs_errore)
public function integer uof_if_condition (ref string fs_formula, ref string fs_errore)
public function integer uof_lista_variabili (string as_cod_formula, ref string as_lista_variabili[], ref string as_errore)
public function string uof_estrapola (string fs_formula, ref string fs_errore)
public function string uof_formula (string fs_formula, ref string fs_errore)
private function string uof_remove_quote (string as_valore)
public function string uof_get_parametro_funzione (string as_parametro)
public function integer uof_calcola_formula_testo (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore)
public function integer uof_calcola_formula_db (string fs_cod_formula, ref decimal fdc_risultato, ref string fs_errore)
public function integer uof_calcola_formula_testo_per_test (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore)
public subroutine uof_pre_calcolo_formula_db (ref string fs_testo_formula)
public function long uof_estrai_stringhe_espress_reg (string fs_testo_formula, ref string fs_variabili[], ref string fs_errore)
public function integer uof_setvariables_formule_db (string fs_cod_formula, string fs_variabili[], boolean fb_test_mode, ref string fs_testo_formula, ref string fs_testo_formula_2, ref string fs_errore)
public function integer uof_eval_with_datastore (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore)
public function integer uof_setvariables_auto_formule_db (string fs_variabili[], ref string fs_testo_formula, ref string fs_errore)
public subroutine uof_setvariables_dtaglio (ref string fs_testo_formula, decimal fd_quan_utilizzo, decimal fd_dim_x, decimal fd_dim_y, decimal fd_dim_z, decimal fd_dim_t, string fs_cod_tessuto, decimal fd_alt_mantovana, decimal fd_quan_ordine, string fs_luce_finita)
public function integer uof_calcola_formula_dtaglio (string fs_testo_formula, boolean fb_test, decimal fd_quan_utilizzo, decimal fd_dim_x, decimal fd_dim_y, decimal fd_dim_z, decimal fd_dim_t, string fs_cod_tessuto, decimal fd_alt_mantovana, decimal fd_quan_ordine, string fs_luce_finita, ref decimal fdc_risultato, ref string fs_errore)
public function integer uof_elabora_select (string fs_espressione, ref string fs_select, ref decimal fdc_risultato, ref string fs_errore)
public function integer uof_eval_with_datastore (string fs_testo_formula, ref string fs_risultato, ref string fs_errore)
public function integer uof_calcola_formula_db (string fs_cod_formula, ref decimal fdc_risultato, ref string fs_risultato, ref string fs_errore)
public function integer uof_calcola_formula_testo_per_test (string fs_testo_formula, ref string fs_risultato, ref string fs_errore)
public function integer uof_set_variabile (string fs_cod_variabile, ref string fs_testo_formula, ref string fs_errore)
public function integer uof_check_variabile (string fs_cod_variabile_originale, ref string fs_cod_variabile)
public function integer uof_setvariables_formule_db (string fs_variabili[], ref string fs_testo_formula, ref string fs_errore)
public function integer uof_get_tabella_variabile (string as_cod_formula, ref string as_tabella_variabile, ref string as_errore)
public function integer uof_calcola_variabile1 (string as_tabella_variabile, string as_tipo_ritorno, string as_colonna, ref string as_valore, ref decimal ad_valore, ref datetime adt_valore, ref string as_errore)
public function integer uof_calcola_variabile (string as_tabella_variabile, string as_tipo_ritorno, string as_cod_variabile, string as_colonna, ref string as_valore, ref decimal ad_valore, ref datetime adt_valore, ref string as_errore)
public function integer uof_calcola_formula_db_v2 (string as_cod_azienda, string as_cod_formula, long al_riga_documento[], s_chiave_distinta as_distinta, string as_contesto, ref decimal ad_valore, ref string as_valore, ref string as_tipo_ritorno, ref string as_errore)
end prototypes

public subroutine uof_rimpiazza_carattere (ref string fs_stringa, string fs_old_char, string fs_new_char);//long start_pos=1
//
////rimpiazza fs_old_char con fs_new_char
//
//if not isnull(fs_stringa) and fs_stringa<>"" then
//	// Find the first occurrence of old_str.
//	start_pos = Pos(fs_stringa, fs_old_char, start_pos)
//	
//	// Only enter the loop if you find old_str.
//	DO WHILE start_pos > 0
//		 // Replace old_str with new_str.
//		 fs_stringa = Replace(fs_stringa, start_pos, Len(fs_old_char), fs_new_char)
//		 // Find the next occurrence of old_str.
//		 start_pos = Pos(fs_stringa, fs_old_char, start_pos+Len(fs_new_char))
//	LOOP	
//else
//	fs_stringa="0.0000"
//end if


fs_stringa = ""
return
end subroutine

public function integer uof_calcola_formula (string fs_espressione, ref string fs_select, ref decimal fdc_risultato, ref string fs_errore);string ls_risultato
datastore lds_data
long ll_tot

if ib_ferma_calcolo then return 1

ls_risultato = uof_formula(fs_espressione, fs_errore)

if ls_risultato = ERRORE_FORMULA then
	ib_ferma_calcolo = true
	return -1
	
elseif ls_risultato = FORMULA_OK then
	fdc_risultato = idc_internal_result
	return 1
end if

//uof_rimpiazza_carattere(ls_risultato, ",", ".")
guo_functions.uof_replace_string(ls_risultato, ",", ".")


if uof_elabora_select(ls_risultato,fs_select, fdc_risultato, fs_errore)<0 then
	//in fs_errore il messaggio
	ib_ferma_calcolo = true
	
	return -1
else
	//in fdc_risultato/ il risultato
	return 1
end if

return 1
end function

public function string uof_variabili (string fs_formula, string fs_cod_formula, ref string fs_errore);string ls_return, ls_cod_variabile, ls_start_variabile, ls_end_variabile, ls_risultato, ls_tmp
long ll_pos, ll_pos2
date ldt_data
s_cs_xx_parametri lstr_parametri

if ib_ferma_calcolo then return ""

ls_start_variabile = "#"
ls_end_variabile = "#"

do while true
	
	//cerca sempre dall'inizio
	ll_pos = pos(fs_formula, ls_start_variabile, 1)
	
	if isnull(ll_pos) or ll_pos < 1 then
		exit
	else
		ll_pos2 = pos(fs_formula, ls_end_variabile, ll_pos + 1)
	end if
	
	if ll_pos2 < 1 then
		//carattere fine variabile non trovato
		fs_errore = "Carattere "+ls_end_variabile+" fine variabile non trovato in "+fs_formula
		ib_ferma_calcolo = true
		return ERRORE_FORMULA
	end if
	
	//tra ll_pos e ll_pos2 c'è il codice variabile
	ls_cod_variabile = mid(fs_formula, ll_pos, ll_pos2 - ll_pos + 1)
	
	if not ib_lista_variabili then
		//GESTIONE CACHE DELLE VARIABILI -----------------------------------------------------------------------------------------
		//verifico se per caso questa variabile l'ho già incontrata, in tal caso restituisco subito il valore precedente
		if not iuo_cache.haskey(ls_cod_variabile) then
			//la variabile non era in cache, quindi chiedila e poi inseriscila in cache...
																													//es. se #S:F:FORNITORE# o #N:ANNO#
//			lstr_parametri.parametro_s_1_a[1] = fs_cod_formula									//codice formula
//			lstr_parametri.parametro_s_1_a[2] = ls_cod_variabile									//codice variabile -> #S:F:FORNITORE# o #N:ANNO#
//			lstr_parametri.parametro_s_1_a[3] = upper(mid(ls_cod_variabile, 2, 2))			//identificatore di tipo variabile -> S: o N:
//			lstr_parametri.parametro_s_1_a[4] = upper(mid(ls_cod_variabile, 4, 2))			//identificatore (eventuale) di selezione campo -> F: o AN(in tal caso viene ignorato)
//			lstr_parametri.parametro_b_1 = ib_gibus
//			
//			openwithparm(w_compila_variabili, lstr_parametri)
//			ls_risultato = message.stringparm
//			
//			if ls_risultato = "ANNULLACALCOLOTEST" then
				//stoppa tutto
//				fs_errore = "Calcolo annullato dall'utente!"
				fs_errore = "L'elenco dei valori della variabili richieste dalla formula non è completo"
				ib_ferma_calcolo = true
				return ERRORE_FORMULA
//			end if
			
			//memorizza in cache la variabile appena letta
			iuo_cache.set(ls_cod_variabile, ls_risultato)
		else
			//variabile in cache, in ls_risultato il valore letto
			ls_risultato = iuo_cache.get(ls_cod_variabile)
		end if
	else
		
		if not guo_functions.uof_in_array(ls_cod_variabile, is_lista_variabili) then
			is_lista_variabili[upperbound(is_lista_variabili) + 1 ] = ls_cod_variabile
		end if
		
	end if
	
	fs_formula = replace(fs_formula, ll_pos, len(ls_cod_variabile), ls_risultato)
loop


return fs_formula
end function

public function integer uof_if_condition (ref string fs_formula, ref string fs_errore);long			ll_inizio_condizione, ll_inizio_vero, ll_inizio_falso,ll_fine_falso, ll_pos, ll_inizio_operatore, ll_lunghezza_condizione, ll_inizio, ll_fine
integer		li_lunghezza_operatore, li_ret
string			ls_car_inizio_if, ls_car_fine_if, ls_sep, ls_condizione, ls_formula_vero, ls_formula_falso, ls_operatore, ls_primo_membro, ls_secondo_membro, ls_soluzione, ls_select
decimal		ld_primo_membro, ld_secondo_membro
boolean		lb_confronto_numerico = true

//es:  IF{condizione;formula_se_vero;formula_se_falso}
ls_car_inizio_if	="{" 
ls_car_fine_if	= "}"
ls_sep			= ";"

ll_inizio_condizione = pos(upper(fs_formula),"IF")

if ll_inizio_condizione = 0 then return 0		//condizione IF non presente

ll_inizio = ll_inizio_condizione

//posizione prima occorenza di }
ll_fine = pos(fs_formula, ls_car_fine_if, ll_inizio)

//posizione dell'inizio della espressione se vero (comincio a cercare dopo IF{...)
ll_inizio_condizione = ll_inizio_condizione + 3
ll_inizio_vero = pos(fs_formula, ls_sep, ll_inizio_condizione )

if ll_inizio_vero>0 then
else
	fs_errore = "Carattere apertura IF "+ls_car_inizio_if+ " non trovato nella formula!"
	return -1
end if


//espressione della condizione IF da verificare: faccio mid da inizio condizione a inizio vero
ls_condizione = mid(fs_formula, ll_inizio_condizione, ll_inizio_vero - ll_inizio_condizione)

if len(ls_condizione)>0 then
else
	fs_errore = "Condizione in IF non trovata nella formula!"
	return -1
end if

//posizione dell'inizio della espressione se falso (comincio a cercare dopo la pos. dell'inizio del vero)
ll_inizio_falso = pos(fs_formula, ls_sep, ll_inizio_vero + 1)

if ll_inizio_falso>0 then
else
	fs_errore = "Carattere  IF "+ls_car_inizio_if+ " non trovato nella formula!"
	return -1
end if

//posizione della fine della espressione se falso (cerco dopo l'inizio del falso)
ll_fine_falso = pos(fs_formula, ls_car_fine_if, ll_inizio_falso + 1)

//formula in caso IF verificata
ls_formula_vero = mid(fs_formula, ll_inizio_vero + 1, ll_inizio_falso - (ll_inizio_vero + 1))

//elimino eventuali spazi all'inizio e alla fine
ls_formula_vero = trim(ls_formula_vero)

//formula in caso IF non verificata
ls_formula_falso = mid(fs_formula, ll_inizio_falso + 1, ll_fine_falso - (ll_inizio_falso + 1 ))

//elimino eventuali spazi all'inizio e alla fine
ls_formula_falso = trim(ls_formula_falso)

//------------------------------------------------------------------------
do while 1=1
	ll_pos = pos( ls_condizione, " " , 1)
	if ll_pos = 0 then exit
	ls_condizione = replace( ls_condizione, ll_pos, 1, "")
loop

//cerco l'operatore =
ll_inizio_operatore = pos(ls_condizione,"=",1)

if ll_inizio_operatore>0 then
	//se ho trovato = verifico se c'è '>=' oppure '<=' oppure '<>'
	if pos(ls_condizione,">=",1) > 0 or pos(ls_condizione,"<=",1) > 0 or pos(ls_condizione,"<>",1) > 0 then
		//rimetto a 0 la posizione dell'operatore, cercherò in seguito >=, <= oppure <>
		ll_inizio_operatore = 0
	else
		li_lunghezza_operatore = 1
		ls_operatore = "="
	end if
end if

if ll_inizio_operatore = 0 then
	//se non ho trovato niente prima cerco '>'
	ll_inizio_operatore = pos(ls_condizione,">",1)
	if ll_inizio_operatore>0 then
		if pos(ls_condizione,">=",1) > 0 or pos(ls_condizione,"<=",1) > 0 or pos(ls_condizione,"<>",1)> 0 then
			ll_inizio_operatore = 0
		else
			li_lunghezza_operatore = 1
			ls_operatore = ">"
		end if
	end if
end if

if ll_inizio_operatore = 0 then
	ll_inizio_operatore = pos(ls_condizione,"<",1)
	if ll_inizio_operatore>0 then
		if pos(ls_condizione,">=",1) > 0 or pos(ls_condizione,"<=",1) > 0 or pos(ls_condizione,"<>",1) > 0 then
			ll_inizio_operatore = 0
		else
			li_lunghezza_operatore = 1
			ls_operatore = "<"
		end if
	end if
end if
	
if ll_inizio_operatore = 0 then
	ll_inizio_operatore = pos(ls_condizione,">=",1)
	if ll_inizio_operatore>0 then
		li_lunghezza_operatore = 2
		ls_operatore = ">="
	end if
end if

if ll_inizio_operatore = 0 then
	ll_inizio_operatore = pos(ls_condizione,"<=",1)
	if ll_inizio_operatore>0 then
		li_lunghezza_operatore = 2
		ls_operatore = "<="
	end if
end if

if ll_inizio_operatore = 0 then
	ll_inizio_operatore = pos(ls_condizione,"<>",1)
	if ll_inizio_operatore>0 then
		li_lunghezza_operatore = 2
		ls_operatore = "<>"
	end if
end if

if pos(ls_condizione,"=>",1) > 0 or pos(ls_condizione,"=<",1) > 0 or pos(ls_condizione,">>",1)> 0 or pos(ls_condizione,"<<",1)> 0 or pos(ls_condizione,"><",1)> 0 then
	ll_inizio_operatore = 0
end if


//--------------------------------------
if ll_inizio_operatore>0 then	
	ls_primo_membro = mid(ls_condizione,1,ll_inizio_operatore -1)
	ll_lunghezza_condizione = len(ls_condizione)
	ls_secondo_membro = mid(ls_condizione,ll_inizio_operatore+li_lunghezza_operatore,ll_lunghezza_condizione - ll_inizio_operatore)
	
	//****************         							    ATTENZIONE!!!!!         ******************************
	//***************
	//**************     SI E' SCELTO DI FARE UN CONFRONTO TRA VARIABILI NUMERICHE NEI CASI
	//**************     DI >,<,>=,<= MENTRE SI FA UN CONFRONTO TRA STRINGHE NEI CASI 
	//**************     DI = E <> (UGUALE E DIVERSO)
	
	//in primo menbro e/o secondo membro potrebbero esserci espressioni (es. altre formule es: [XXX]+5 o variabili #N:ANNO#-25 ) che devono prima essere risolte/calcolate
	//
	
	//-------------
	//vedo se ho formule nel 1° membro
	ls_primo_membro = uof_estrapola(ls_primo_membro, fs_errore)
	
//	//se ci sono variabili sostituisco
//	ls_primo_membro = uof_variabili(ls_primo_membro, "1°membro IF", fs_errore)
//	if ls_primo_membro="ERROREFORMUL@" then
//		ib_ferma_calcolo = true
//		//return "ERROREFORMUL@"
//		fs_errore = "Errore in primo membro condizione IF"
//		return -1
//	end if
	
	//adesso valuta la espressione numerica del 1° membro se l'operatore implica il confronto tra numeri
	if uof_elabora_select(ls_primo_membro, ls_select, ld_primo_membro, fs_errore)<0 then
		//in fs_errore il messaggio
		//return -1
		
		//disattiva il confronto tra numeri
		lb_confronto_numerico = false
	else
		//in ld_primo_membro il risultato
	end if
	
	
	//-------------
	//vedo se ho formule nel 2° membro
	uof_estrapola(ls_secondo_membro, fs_errore)
	
//	//se ci sono variabili sostituisco
//	ls_secondo_membro = uof_variabili(ls_secondo_membro, "2°membro IF", fs_errore)
//	if ls_secondo_membro="ERROREFORMUL@" then
//		ib_ferma_calcolo = true
//		//return "ERROREFORMUL@"
//		fs_errore = "Errore in secondo membro condizione IF"
//		return -1
//	end if
	
	if lb_confronto_numerico then
		if uof_elabora_select(ls_secondo_membro, ls_select, ld_secondo_membro, fs_errore)<0 then
			//in fs_errore il messaggio
			//return -1
			
			//disattiva il confronto tra numeri
			lb_confronto_numerico = false
		else
			//in ld_primo_membro il risultato
		end if
	end if

	
	//ld_primo_membro = dec(ls_primo_membro)	          
	//ld_secondo_membro = dec(ls_secondo_membro)	    
	
	choose case ls_operatore
		case "="
			if lb_confronto_numerico then
				
				if ld_primo_membro = ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro = ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			end if
			
		case ">"
			if lb_confronto_numerico then
				
				if ld_primo_membro > ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro > ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			end if
			
		case "<"
			if lb_confronto_numerico then
				
				if ld_primo_membro < ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro < ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			end if
			
		case ">="
			if lb_confronto_numerico then
				
				if ld_primo_membro >= ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro >= ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			end if
			
		case "<="
			if lb_confronto_numerico then
				
				if ld_primo_membro <= ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro <= ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			end if
			
		case "<>"
			if lb_confronto_numerico then
				
				if ld_primo_membro <> ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro <> ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
			end if
			
	end choose
end if

if ll_inizio_operatore = 0 then
	fs_errore = "Errore: nella condizione manca l'operatore di condizione (>,<,>=,<=,=) oppure si sono introdotti dei simboli errati (=<,=>)!"
	return -1
end if

//sostituisci la espressione dell'IF con la espressione risolta (soluzione)
fs_formula = mid(fs_formula, 1, ll_inizio - 1) + "(" + ls_soluzione + ")" + mid(fs_formula,ll_fine+1, len(fs_formula))

//se c'è più di un IF
//(es. IF{condizione1;vero;falso} + IF{cond_new;exp1; exp2}  )
if pos(upper(fs_formula),"IF") > 0 then
	//se nella condizione è previsto un altro IF re-itera
	li_ret = uof_if_condition(fs_formula, fs_errore)
	if li_ret < 0 then
		
		return -1
	end if
end if

//fs_formula = ls_soluzione

return 1
end function

public function integer uof_lista_variabili (string as_cod_formula, ref string as_lista_variabili[], ref string as_errore);/**
 * stefanop
 * 15/05/2012
 *
 * Estrae l'aray delle variabili che sono necessarie per la formula passata come parametro
 *
 **/
 
string ls_empty[]

is_lista_variabili = ls_empty
as_lista_variabili = ls_empty

ib_lista_variabili = true

uof_formula(as_cod_formula, as_errore)

ib_lista_variabili = false

if not isnull(as_errore) and as_errore <> "" then
	return -1
end if

as_lista_variabili = is_lista_variabili
is_lista_variabili = ls_empty

return 1
end function

public function string uof_estrapola (string fs_formula, ref string fs_errore);string ls_return, ls_cod_formula, ls_start_formula, ls_end_formula, ls_risultato
long ll_pos, ll_pos2

if ib_ferma_calcolo then return ""

ls_start_formula = "["
ls_end_formula = "]"

do while true
	//cerca sempre dall'inizio
	ll_pos = pos(fs_formula, ls_start_formula, 1)
	
	if ll_pos < 1 then
		exit
	else
		ll_pos2 = pos(fs_formula, ls_end_formula, ll_pos)
	end if
	
	if ll_pos < 1 then
		//carattere ] fine codice formula non trovato
		fs_errore = "Carattere "+ls_end_formula+" fine codice formula non trovato in "+fs_formula
		ib_ferma_calcolo = true
		return ""
	else
		ll_pos2 = pos(fs_formula, ls_end_formula, ll_pos)
	end if
	
	//tra ll_pos e ll_pos2 c'è il codice formula
	ls_cod_formula = mid(fs_formula, ll_pos, ll_pos2 - ll_pos + 1)
	
	ls_risultato = uof_formula(ls_cod_formula, fs_errore)
	
	if ib_ferma_calcolo then return "ERROREFORMUL@"
	
	//metto al posto di ls_cod_formula ls_risultato, nella stessa posizione di cod_formula
	
	fs_formula = replace(fs_formula, ll_pos, len(ls_cod_formula), ls_risultato)
loop


return fs_formula
end function

public function string uof_formula (string fs_formula, ref string fs_errore);string ls_return, ls_flag_tipo, ls_sql_statement, ls_tabella, ls_colonna, ls_composta, ls_format, ls_funzione,ls_column_type
dec{4} ld_numero
datastore lds_data
long ll_ret

if ib_ferma_calcolo then return ""

ls_format = "###########0.0000"

select flag_tipo, numero, sql, tabella, colonna, composta, funzione
into :ls_flag_tipo, :ld_numero, :ls_sql_statement, :ls_tabella, :ls_colonna, :ls_composta, :ls_funzione
from tab_formule_calcolo
where 	cod_azienda=:s_cs_xx_cod_azienda and
			cod_formula=:fs_formula;

if sqlca.sqlcode<0 then
	ib_ferma_calcolo = true
	fs_errore = "Errore in lettura codice formula: "+sqlca.sqlerrtext
	return ERRORE_FORMULA
	
elseif sqlca.sqlcode=100 then
	ib_ferma_calcolo = true
	fs_errore = "Formula con codice "+fs_formula+" non trovata! "+sqlca.sqlerrtext
	return ERRORE_FORMULA
	
end if

choose case ls_flag_tipo
	case "N"
		//costante numerica
		if isnull(ld_numero) then return "0.0000"
		
		ls_return = string(ld_numero, ls_format)
		return ls_return
		
	case "S"
		//sql puro (al più variabili delimitate da #..#)
		
		//se ci sono variabili sostituisco
		ls_return = uof_variabili(ls_sql_statement, fs_formula, fs_errore)
		if ls_return=ERRORE_FORMULA then
			ib_ferma_calcolo = true
			return ERRORE_FORMULA
		end if
		
		if ib_lista_variabili then return ls_sql_statement
		
		ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_return, fs_errore)
		if ll_ret<0 then
		
			ib_ferma_calcolo = true
			return ERRORE_FORMULA
		end if
		

		ls_column_type =lds_data.Describe("#1.ColType")

		choose case left(upper(ls_column_type),3)
			case "CHA"
				ls_return = "'" + lds_data.getitemstring(1, 1) + "'"
				
			case "DEC", "INT", "LON", "NUM", "REAL", "ULO"
				if ll_ret > 0 then
				ld_numero = lds_data.getitemdecimal(1, 1)
				if isnull(ld_numero) then ld_numero = 0.0000
			else
				ld_numero = 0.0000
			end if
				ls_return = string(ld_numero, ls_format)
				
			case else
				fs_errore = "Tipo dati non revisto in ritorno select ("+ls_column_type+")"
				ib_ferma_calcolo = true
				return ERRORE_FORMULA
				
		end choose
		
//		if ll_ret > 0 then
//			ld_numero = lds_data.getitemdecimal(1, 1)
//			if isnull(ld_numero) then ld_numero = 0.0000
//		else
//			ld_numero = 0.0000
//		end if
//		ls_return = string(ld_numero, ls_format)
		
		return ls_return
		
	case "C"
		//composta
		
		//verifica se c'è qualche IF
		if uof_if_condition(ls_composta, fs_errore)<0 then
			//in fs_errore il messaggio
			
			return ERRORE_FORMULA
		end if
		
		//estrapola i codici delle altre formule
		ls_return = uof_estrapola(ls_composta, fs_errore)
		
		//se ci sono variabili sostituisco
		ls_return = uof_variabili(ls_return, fs_formula, fs_errore)
		if ls_return = ERRORE_FORMULA then
			ib_ferma_calcolo = true
			return ERRORE_FORMULA
		end if
		
		return ls_return
	
	case "T" //valore colonna di una tabella: gestire la wherestring
		fs_errore = "Tipo calcolo su tabella in fase di sviluppo (formula "+fs_formula +")"
		ib_ferma_calcolo = true
		return ERRORE_FORMULA
		
	case "F"
//		// Funzione
//		if isnull(ls_funzione) or ls_funzione = "" then
//			fs_errore = "La funzione non è impostata correttamente, la dichiarazione risulta vuota"
//			ib_ferma_calcolo = true
//			return ERRORE_FORMULA
//		end if
//		
//		// il risultato viene impostato nella variabile idc_internal_result
//		ls_return = uof_esegui_funzione(ls_funzione, fs_errore)
//		return ls_return
		
	case "E"
		// Excel
		
		
		
	case else
		fs_errore = "Tipo non previsto per formula "+fs_formula
		ib_ferma_calcolo = true
		return ERRORE_FORMULA
		
end choose



end function

private function string uof_remove_quote (string as_valore);
if left(as_valore, 1) = "'" then
	as_valore = mid(as_valore, 2)
end if

if right(as_valore, 1) = "'" then
	as_valore = mid(as_valore, 1, len(as_valore) -1)
end if

return as_valore
end function

public function string uof_get_parametro_funzione (string as_parametro);/**
 * stefanop
 * 17/05/2012
 *
 * Recupera il valore di un parametro nella cache dei valori per usarlo nella funzione.
 * Il parametro protrebbe essere a sua volta una chiave per un parametro.
 **/
 
 
string ls_result, ls_mid
 
ls_result = uof_remove_quote(iuo_cache.get(as_parametro))
 
// controllo che non sia a sua volta una variabile
ls_mid = left(ls_result, 3)
if ls_mid = "#S:" or ls_mid = "#N:" or ls_mid = "#D:" then
 	 ls_result = uof_remove_quote(iuo_cache.get(ls_result))
end if 	
 
 return ls_result
end function

public function integer uof_calcola_formula_testo (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore);// Calcola una formula passata come testo, senza partire da un codice formula
//questa funzione serve ad Enrico cosi com'è quindi non toccarla .....

string					ls_risultato, ls_testo_des_formula, ls_variabili[], ls_colonna, ls_flag_tipo_dato, &
						ls_val_variabile, ls_dsdef, ls_tabella_variabile
						
dec{4}				ld_val_variabile

datetime				ldt_val_variabile
date					ldt_date
						
long					ll_tot_variables, ll_index

uo_espress_reg	luo_espress_reg

boolean				lb_test_mode

datastore			lds_eval



ls_dsdef = 'release 6; datawindow() table(column=(type=char(1000) name=a dbname="a") )'

lb_test_mode = true
if (ii_anno_reg_ord_ven>0 and il_num_reg_ord_ven>0 and il_prog_riga_ord_ven>0) or &
		(is_cod_prodotto_padre<>"" and is_cod_prodotto_figlio<>"" and &
		is_cod_versione_padre<>"" and is_cod_versione_figlio<>"" and il_num_sequenza>0) then
	
	lb_test_mode = false
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratteri che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(fs_testo_formula)
guo_functions.uof_log("uof_calcola_formula_testo(): Risultato PRE-CALCOLO " + guo_functions.uof_format(fs_testo_formula))


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALORE MEDIANTE ESPRESSIONI REGOLARI e 
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

guo_functions.uof_log("uof_calcola_formula_testo(): Ingresso funzione. Formula=" + guo_functions.uof_format(fs_testo_formula))

ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if
guo_functions.uof_log("uof_calcola_formula_testo(): Dopo uof_estrai_stringhe_espress_reg. Formula=" + guo_functions.uof_format(fs_testo_formula))

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
guo_functions.uof_log("uof_calcola_formula_testo(): Inizio Esame variabili")

if ll_tot_variables>0 then
	//verifica che i codici estratti rappresentino effettivamente variabili codificate in tab_variabili_formule
	//se si leggi la colonna relativa e valutane il valore (stringa o numero) e sostituiscilo nella formula
	
	for ll_index=1 to ll_tot_variables
		guo_functions.uof_log("uof_calcola_formula_testo(): Considero Variabile " + guo_functions.uof_format(ls_variabili[ll_index]))
	
		select nome_campo_database,
				flag_tipo_dato
		into 	:ls_colonna,
				:ls_flag_tipo_dato
		from tab_variabili_formule
		where 	cod_azienda=:s_cs_xx_cod_azienda and
					cod_variabile=:ls_variabili[ll_index];
	
		if sqlca.sqlcode<0 then
			fs_errore = "Errore lettura variabile: "+ls_variabili[ll_index]+": "+sqlca.sqlerrtext
			return -1
			
		elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
			//nome colonna estratto
			guo_functions.uof_log("uof_calcola_formula_testo(): Colonna " + guo_functions.uof_format(ls_colonna))
			guo_functions.uof_log("uof_calcola_formula_testo(): Tipo Dato " + guo_functions.uof_format(ls_flag_tipo_dato))
			
			if not lb_test_mode then
				guo_functions.uof_log("uof_calcola_formula_testo(): Test Mode= FALSE" )
				//valuta il valore della variabile e sostituiscila nella formula
				//ls_flag_tipo_dato  può essere S (stringa) oppure N (numerico decimale) oppure D (datetime)
				
				//#####################################################################
				//leggi la tabella della variabile
				uof_get_tabella_variabile(ls_variabili[ll_index], ls_tabella_variabile, fs_errore)
				guo_functions.uof_log("uof_calcola_formula_testo(): Tabella variabile " + guo_functions.uof_format(ls_tabella_variabile))
				//#####################################################################
				
				//###########################################################################
				if uof_calcola_variabile(	ls_tabella_variabile, ls_flag_tipo_dato, ls_variabili[ll_index], ls_colonna, 	ls_val_variabile, ld_val_variabile, ldt_val_variabile, fs_errore) < 0 then
					return -1
				end if
				guo_functions.uof_log("uof_calcola_formula_testo(): Recupero Valore Variabile= " + guo_functions.uof_format(ls_val_variabile))
				//###########################################################################
				
				guo_functions.uof_replace_string(fs_testo_formula, ls_variabili[ll_index], ls_val_variabile)
				
				guo_functions.uof_log("uof_calcola_formula_testo(): Formula risultante= " + guo_functions.uof_format(fs_testo_formula))
			else
				guo_functions.uof_log("uof_calcola_formula_testo(): Test Mode= TRUE" )
				//modifica il nome della variabile mettendo dei caratteri inizio/fine nome variabile e una lettera identificatrice del tipo
				//successivamente sarà chiamata una funzione (uof_variabili) che si occuperà di richiedere i valori per ciascuna variabile
				
				//es: DIM_2   ->   #N:DIM_2#
				guo_functions.uof_replace_string(fs_testo_formula, ls_variabili[ll_index], "#"+ls_flag_tipo_dato+":"+ls_variabili[ll_index]+"#")
				guo_functions.uof_log("uof_calcola_formula_testo(): Risultato testo formula dopo sostituzione= " + guo_functions.uof_format(fs_testo_formula))
						
			end if
			
		else
			continue
		end if
	
	next
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME
if uof_eval_with_datastore(fs_testo_formula, fdc_risultato, fs_errore) < 0 then
	return -1
end if

return 0
end function

public function integer uof_calcola_formula_db (string fs_cod_formula, ref decimal fdc_risultato, ref string fs_errore);string					ls_risultato, ls_testo_formula, ls_testo_des_formula, ls_variabili[], ls_colonna, ls_flag_tipo_dato, &
						ls_val_variabile, ls_testo_formula_2
						
dec{4}				ld_val_variabile

datetime				ldt_val_variabile
date					ldt_date
						
long					ll_tot_variables, ll_index

boolean				lb_test_mode

integer				li_ret



lb_test_mode = true
if (ii_anno_reg_ord_ven>0 and il_num_reg_ord_ven>0 and il_prog_riga_ord_ven>0) or &
		(is_cod_prodotto_padre<>"" and is_cod_prodotto_figlio<>"" and &
		is_cod_versione_padre<>"" and is_cod_versione_figlio<>"" and il_num_sequenza>0) then
		
	lb_test_mode = false
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//LETTURA FORMULA
if fs_cod_formula="" or isnull(fs_cod_formula) then
	fs_errore = "Codice formula non specificato!"
	return -1
end if

select testo_formula,
		testo_des_formula
into 	:ls_testo_formula,
		:ls_testo_des_formula
from tab_formule_db
where cod_azienda=:s_cs_xx_cod_azienda and
		cod_formula=:fs_cod_formula;

if sqlca.sqlcode<0 then
	fs_errore = "Errore lettura formula: "+fs_cod_formula+": "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_testo_formula) or ls_testo_formula="" or isnull(ls_testo_des_formula) or ls_testo_des_formula="" then
	fs_errore = "Formula: "+fs_cod_formula+" non trovata oppure non specificata!"
	return -1
end if

//in ls_testo_formula la formula con le variabili (tab_variabili_formule.cod_variabile)
//in ls_testo_des_formula la formula con le variabili sostituite dai nomi di colonna (tab_variabili_formule.nome_campo_database) della tabella comp_det_ord_ven
ls_testo_formula_2 = ls_testo_formula


//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratterei che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(ls_testo_formula)


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALOREMEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

ll_tot_variables = uof_estrai_stringhe_espress_reg(ls_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if



//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
li_ret = uof_setvariables_formule_db(fs_cod_formula, ls_variabili[], lb_test_mode, ls_testo_formula, ls_testo_formula_2, fs_errore)
if li_ret < 0 then
	return -1
	
elseif li_ret=1 then
	//eri in modalità testing, quindi non server + proseguire per il risultato finale
	return 0
	
end if

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME

if uof_eval_with_datastore(ls_testo_formula, fdc_risultato, fs_errore) < 0 then
	return -1
end if

return 0
end function

public function integer uof_calcola_formula_testo_per_test (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore);string					ls_risultato, ls_variabili[], ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_testo_formula_2
						
dec{4}				ld_val_variabile

datetime				ldt_val_variabile
date					ldt_date
						
long					ll_tot_variables, ll_index

uo_espress_reg	luo_espress_reg

boolean				lb_test_mode

datastore			lds_eval

integer				li_ret



lb_test_mode = true


ls_testo_formula_2 = fs_testo_formula


//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratterei che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(fs_testo_formula)



//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALORE MEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
li_ret = uof_setvariables_formule_db("", ls_variabili[], lb_test_mode, fs_testo_formula, ls_testo_formula_2, fs_errore)
if li_ret < 0 then
	return -1
end if


return 0

end function

public subroutine uof_pre_calcolo_formula_db (ref string fs_testo_formula);//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO
/*
	- ricerca e sostituzione dei seguenti caratteri
			","  con "."   (numeri decimali)
			
			"[" con "("	(compatibilità clausola IF[cond;se_vero;se_falso] con IF(cod,se_ver,se_falso))
			"]" con ")"	(compatibilità clausola IF[cond;se_vero;se_falso] con IF(cod,se_ver,se_falso))
			";" con ","	(compatibilità clausola IF[cond;se_vero;se_falso] con IF(cod,se_ver,se_falso))
			
			"SQR" con "SQRT"  (radice quadrata)
*/

guo_functions.uof_replace_string(fs_testo_formula, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "[", "(")
guo_functions.uof_replace_string(fs_testo_formula, "]", ")")
guo_functions.uof_replace_string(fs_testo_formula, ";", ",")
guo_functions.uof_replace_string(fs_testo_formula, "SQR", "SQRT")
guo_functions.uof_replace_string(fs_testo_formula, "'", "~~'")
guo_functions.uof_replace_string(fs_testo_formula, '"', '~~"')
/* EnMe 30-08-2018 Aggiunto per riuscire a mettere più valori nel case, altrimento la virgola veniva sostituita dal punto
	Quindi il case deve essere fatto così, cioè va usato il carattere backslash come separatore per non creare problemi.
	CASE(TEST WHEN 'COEL0007'\'COEL0009'  THEN 'COEL0116' ELSE 'COEL0132')
*/
guo_functions.uof_replace_string(fs_testo_formula, '\', ',')
end subroutine

public function long uof_estrai_stringhe_espress_reg (string fs_testo_formula, ref string fs_variabili[], ref string fs_errore);//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALORE MEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

long ll_tot_variables
uo_espress_reg luo_espress_reg

luo_espress_reg = create uo_espress_reg

if luo_espress_reg.set_pattern(PATTERN_VARIABLES, fs_errore) < 0 then
	destroy luo_espress_reg
	return -1
end if

ll_tot_variables = luo_espress_reg.cerca_array( fs_testo_formula, fs_variabili[])
destroy luo_espress_reg

return ll_tot_variables
end function

public function integer uof_setvariables_formule_db (string fs_cod_formula, string fs_variabili[], boolean fb_test_mode, ref string fs_testo_formula, ref string fs_testo_formula_2, ref string fs_errore);long				ll_tot_variables, ll_index
string				ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_temp, ls_prog_riga, ls_tabella_variabile
dec{4}			ld_val_variabile, ldc_risultato
datetime			ldt_val_variabile
date				ldt_date


ll_tot_variables = upperbound(fs_variabili[])

// ---------------- EnMe 27-5-2013 (differenza rispetto all'oggetto standard 
fb_test_mode =FALSE
//-----------------

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
for ll_index=1 to ll_tot_variables
	
	ls_temp = upper(fs_variabili[ll_index])
	
	//alcuni casi, già li possiamo escludere, per evitare di fare select inutili
	if isnull(ls_temp) or ls_temp="" or upper(ls_temp)="IF" or ls_temp="'" or pos(ls_temp, "'")>0 or isnumber(ls_temp) or &
			ls_temp="=" or  upper(ls_temp)="LEFT" or  upper(ls_temp)="POS" or upper(ls_temp)="RIGHT" or upper(ls_temp)="AND"	 or upper(ls_temp)="FIND_STRING"	then continue
	
	select nome_campo_database,
			flag_tipo_dato
	into 	:ls_colonna,
			:ls_flag_tipo_dato
	from tab_variabili_formule
	where 	cod_azienda=:s_cs_xx_cod_azienda and
				upper(cod_variabile)=:ls_temp;

	if sqlca.sqlcode<0 then
		fs_errore = "Errore lettura variabile: "+fs_variabili[ll_index]+": "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
		//nome colonna estratto
		
		if not fb_test_mode then
			//valuta il valore della variabile e sostituiscila nella formula
			//ls_flag_tipo_dato  può essere S (stringa) oppure N (numerico decimale) oppure D (datetime)
			
			//#####################################################################
			//leggi la tabella della variabile
			uof_get_tabella_variabile(fs_variabili[ll_index], ls_tabella_variabile, fs_errore)
			//#####################################################################
			
			//###########################################################################
			if uof_calcola_variabile(ls_tabella_variabile, ls_flag_tipo_dato, fs_variabili[ll_index],ls_colonna, ls_val_variabile, ld_val_variabile, ldt_val_variabile, fs_errore) < 0 then
				return -1
			end if
			//###########################################################################
			
			guo_functions.uof_replace_string(fs_testo_formula, fs_variabili[ll_index], ls_val_variabile)
			
		else
			//modifica il nome della variabile mettendo dei caratteri inizio/fine nome variabile e una lettera identificatrice del tipo
			//successivamente sarà chiamata una funzione (uof_variabili) che si occuperà di richiedere i valori per ciascuna variabile
			
			//es: DIM_2   ->   #N:DIM_2#
			guo_functions.uof_replace_string(fs_testo_formula, fs_variabili[ll_index], "#"+ls_flag_tipo_dato+":"+fs_variabili[ll_index]+"#")
					
		end if

	else
		continue
	end if

next

/* ------------ EnMe 27-5-2013 (differenza rispetto all'oggetto standard: questa area va commentata
//SE SIAMO IN MODALITà TEST VUOL DIRE CHE STAI TESTANDO UNA FORMULA
//QUINDI VIENE CHIAMATA UNA FUNZIONE CHE APRE UNA FINESTRA CHE RICHIEDE ALL'UTENTE DI IMMETTERE I VALORIO DELLE VARIABILI EVENTUALI
//VISUALIZZANDO SUBITO IL RELATIVO RISULTATO
if fb_test_mode then
	//chiedi i valori per ogni variabile
	//ls_testo_formula = uof_variabili(ls_testo_formula, fs_cod_formula, fs_errore)
	fs_testo_formula = uof_variabili_cumulativo(fs_testo_formula, fs_testo_formula_2, fs_cod_formula, ldc_risultato, fs_errore)
	if fs_testo_formula = ERRORE_FORMULA then
		ib_ferma_calcolo = true
		
		return -1
	end if
	
	//ora da qui puoi tranquillamente uscire, quando rientrerai nella funzione chiamante ...
	return 1
end if
*/

return 0
end function

public function integer uof_eval_with_datastore (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore);
string			ls_risultato
integer		li_ret


li_ret = uof_eval_with_datastore(fs_testo_formula, ls_risultato, fs_errore)

if li_ret<0 then
	//in fs_errore c'è il messaggio
	return -1
end if

fdc_risultato = dec(ls_risultato)

return 0



//datastore				lds_eval
//string						ls_dsdef, ls_risultato
//
//
//
////---------------------------------------------------------------------------------------------------------------------------------------
////VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME
//
//ls_dsdef = 'release 6; datawindow() table(column=(type=char(255) name=a dbname="a") )'
//
//lds_eval = create datastore
//lds_eval.Create(ls_dsdef, fs_errore)
//lds_eval.InsertRow(0)
//
//if Len(fs_errore) > 0 then
//	destroy lds_eval
//	return -1
//else
//	ls_risultato = lds_eval.describe ( 'evaluate( " ' + fs_testo_formula + ' " ,1)' )
//	destroy lds_eval
//	
//	if ls_risultato="!" then
//		fs_errore = "Errore! Verificare la formula!"
//		return -1
//	end if
//	
//	fdc_risultato = dec(ls_risultato)
//end if
//
//
//
//return 0
end function

public function integer uof_setvariables_auto_formule_db (string fs_variabili[], ref string fs_testo_formula, ref string fs_errore);long				ll_tot_variables, ll_index
string				ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_temp
dec{4}			ld_val_variabile, fdc_risultato
datetime			ldt_val_variabile
date				ldt_date
integer			li_ret


ll_tot_variables = upperbound(fs_variabili[])

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
for ll_index=1 to ll_tot_variables
	
	
	li_ret = uof_set_variabile(fs_variabili[ll_index], fs_testo_formula, fs_errore)
	if li_ret<0 then
		return -1
		
	elseif li_ret=1 then
		continue
		
	end if
	
	
	
//	select nome_campo_database,
//			flag_tipo_dato
//	into 	:ls_colonna,
//			:ls_flag_tipo_dato
//	from tab_variabili_formule
//	where 	cod_azienda=:s_cs_xx_cod_azienda and
//				cod_variabile=:fs_variabili[ll_index];
//
//	ls_temp = upper(fs_variabili[ll_index])
//	
//	//alcuni casi, già li possiamo escludere, per evitare di fare select inutili
//	if isnull(ls_temp) or ls_temp="" or upper(ls_temp)="IF" or ls_temp="'" or pos(ls_temp, "'")>0 or isnumber(ls_temp) then continue
//
//	if sqlca.sqlcode<0 then
//		fs_errore = "Errore lettura variabile: "+fs_variabili[ll_index]+": "+sqlca.sqlerrtext
//		return -1
//		
//	elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
//		
//		choose case ls_flag_tipo_dato
//			case "S"
//				fs_errore = "La funzione uof_setvariables_auto_formule_db NON ammette l'uso di variabili STRINGA!"
//				return -1
//				
//			case "N"
//				ld_val_variabile = 350		//valore fisso
//
//				ls_val_variabile = string(ld_val_variabile, "########0.0000")
//				guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
//				guo_functions.uof_replace_string(fs_testo_formula, fs_variabili[ll_index], ls_val_variabile)
//				
//			case "D"
//				fs_errore = "La funzione uof_setvariables_auto_formule_db NON ammette l'uso di variabili DATETIME!"
//				return -1
//			
//		end choose
//
//		
//	else
//		continue
//	end if

next


return 0
end function

public subroutine uof_setvariables_dtaglio (ref string fs_testo_formula, decimal fd_quan_utilizzo, decimal fd_dim_x, decimal fd_dim_y, decimal fd_dim_z, decimal fd_dim_t, string fs_cod_tessuto, decimal fd_alt_mantovana, decimal fd_quan_ordine, string fs_luce_finita);
//il set di variabili disponibili nelle formule delle distinte di taglio sono un insieme ristretto e costante

guo_functions.uof_replace_string(fs_testo_formula, "quan_utilizzo", 		string(fd_quan_utilizzo, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "x", 						string(fd_dim_x, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "y", 						string(fd_dim_y, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "z", 						string(fd_dim_z, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "dim_t", 				string(fd_dim_t, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "tessuto", 				fs_cod_tessuto)
guo_functions.uof_replace_string(fs_testo_formula, "h_mantovana", 		string(fd_alt_mantovana, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "quan_ordine", 		string(fd_quan_ordine, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "luce_finita", 			fs_luce_finita)

return
end subroutine

public function integer uof_calcola_formula_dtaglio (string fs_testo_formula, boolean fb_test, decimal fd_quan_utilizzo, decimal fd_dim_x, decimal fd_dim_y, decimal fd_dim_z, decimal fd_dim_t, string fs_cod_tessuto, decimal fd_alt_mantovana, decimal fd_quan_ordine, string fs_luce_finita, ref decimal fdc_risultato, ref string fs_errore);string					ls_variabili[], ls_variabili_copy[], ls_testo_formula_originale, ls_flag_tipo_dato, ls_val_variabile, ls_variabili_originali[], ls_temp, ls_testo_formula_2
						
long					ll_tot_variables, ll_index, ll_k

integer				li_ret


ls_testo_formula_originale = fs_testo_formula

if fs_testo_formula='' or isnull(fs_testo_formula) or fs_testo_formula="." then
	fdc_risultato = 0
	return 0
end if

//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratteri che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(fs_testo_formula)


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI MEDIANTE ESPRESSIONI REGOLARI SOLO IN CASO DI TEST
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

if fb_test then

	ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
	if ll_tot_variables<0 then
		return -1
	end if

	ll_k = 0
	//togli quelle che non sono variabili previste
	for ll_index=1 to ll_tot_variables
		choose case lower(ls_variabili[ll_index])
			case "quan_utilizzo", "x", "y","z","dim_t","h_mantovana","quan_ordine","tessuto","luce_finita"
				ll_k += 1
				ls_variabili_copy[ll_k] = ls_variabili[ll_index]
				ls_variabili_originali[ll_k] = ls_variabili[ll_index]
				
			case else
				li_ret = uof_check_variabile(ls_variabili[ll_index], ls_temp)
				if li_ret=0 then
					ll_k += 1
					ls_variabili_copy[ll_k] = ls_temp
					ls_variabili_originali[ll_k] = ls_variabili[ll_index]
				end if

		end choose
	next

	//---------------------------------------------------------------------------------------------------------------------------------------
	//SOSTITUZIONE VARIABILI E RELATIVO VALORE
	if upperbound(ls_variabili_copy)>0 then
		//ci sono variabili da definire ...
		//modifica il testo formula per essere riconosciuto
		//cicla l'array delle variabili
		for ll_index=1 to upperbound(ls_variabili_copy)
			
			choose case lower(ls_variabili_copy[ll_index])
				case "quan_utilizzo", "x", "y","z","dim_t","h_mantovana","quan_ordine"
					ls_flag_tipo_dato = "N"
					//es: h_mantovana   ->   #N:h_mantovana#
					guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_copy[ll_index], "#"+ls_flag_tipo_dato+":"+ls_variabili_copy[ll_index]+"#")
					
				case "tessuto","luce_finita"
					ls_flag_tipo_dato = "S"
					//es: h_mantovana   ->   #S:tessuto#
					guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_copy[ll_index], "#"+ls_flag_tipo_dato+":"+ls_variabili_copy[ll_index]+"#")
					
				case else
					//sostituzione struttura variabile già effettuata, la metto cosi com'è nella formula
					guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_originali[ll_index], ls_variabili_copy[ll_index])
					//guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_originali[ll_k], ls_variabili_copy[ll_index])
					
			end choose
			
		next
	end if

	//IN MODALITA' TEST VUOL DIRE CHE STAI TESTANDO UNA FORMULA
	//QUINDI VIENE CHIAMATA UNA FUNZIONE CHE APRE UNA FINESTRA CHE RICHIEDE ALL'UTENTE DI IMMETTERE I VALORIO DELLE VARIABILI EVENTUALI
	//VISUALIZZANDO SUBITO IL RELATIVO RISULTATO

	//chiedi i valori per ogni variabile
	//ls_testo_formula = uof_variabili(ls_testo_formula, fs_cod_formula, fs_errore)
//	fs_testo_formula = uof_variabili_cumulativo(fs_testo_formula, ls_testo_formula_originale, "", fdc_risultato, fs_errore)
//	if fs_testo_formula = ERRORE_FORMULA then
//		ib_ferma_calcolo = true
//		
//		return -1
//	end if
	
	//ora da qui puoi tranquillamente uscire, quando rientrerai nella funzione chiamante ...
	return 1
end if

//#####################################################################################################
//QUESTA PARTE RIMANE PER COMPATIBILITA'
//#####################################################################################################

//se arrivi fin qui non è un test, quindi esegui sostituzione dei valori
//se numerico fai prima in string e poi metti il punto al posto della virgola !!!!!!

ls_val_variabile = string(fd_quan_utilizzo, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "quan_utilizzo", 		ls_val_variabile)

ls_val_variabile = string(fd_dim_x, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "x", 						ls_val_variabile)

ls_val_variabile = string(fd_dim_y, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "y", 						ls_val_variabile)

ls_val_variabile = string(fd_dim_z, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "z", 						ls_val_variabile)

ls_val_variabile = string(fd_dim_t, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "dim_t", 				ls_val_variabile)

guo_functions.uof_replace_string(fs_testo_formula, "tessuto", 				"~'"+fs_cod_tessuto+"~'")

ls_val_variabile = string(fd_alt_mantovana, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "h_mantovana", 		ls_val_variabile)

ls_val_variabile = string(fd_quan_ordine, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "quan_ordine", 		ls_val_variabile)

guo_functions.uof_replace_string(fs_testo_formula, "luce_finita", 			"~'"+fs_luce_finita+"~'")

//#####################################################################################################


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALOREMEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if


ls_testo_formula_2 = fs_testo_formula
//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
li_ret = uof_setvariables_formule_db(ls_variabili[], fs_testo_formula, fs_errore)
if li_ret < 0 then
	return -1
	
elseif li_ret=1 then
	//eri in modalità testing, quindi non server + proseguire per il risultato finale
	return 0
	
end if




//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME
if uof_eval_with_datastore(fs_testo_formula, fdc_risultato, fs_errore) < 0 then
	return -1
end if

return 0
end function

public function integer uof_elabora_select (string fs_espressione, ref string fs_select, ref decimal fdc_risultato, ref string fs_errore);datastore		lds_data
long				ll_tot
any				lany_valore
string				ls_col_type, ls_risultato

fs_select = "select "+fs_espressione

if ib_lista_variabili then return 1

ll_tot = guo_functions.uof_crea_datastore(lds_data, fs_select, fs_errore)

if ll_tot<0 then
	//in fs_errore il messaggio
	
	//gestisci la possibilità di aver passato una variabile non numerica, es. fs_espressione= pippo005
	//select pippo005 darebbe errore
	//torna -1 e dai la possibilità di continuare, perchè se la chiamata viene da elaborazione if
	//viene interpretato dal sistema come confronto tra stringhe e non numeri
	
	return -1
end if

if lds_data.rowcount() > 0 then
	
	ls_col_type = lds_data.describe("#1.coltype")
	/*
	Char
	Date
	DateTime
	Decima
	Int
	Long
	Number
	Real
	Time
	Timestamp
	ULong
	*/
	
	choose case left(upper(ls_col_type), 3)
		case "DEC","INT","LON","NUM","REA","ULO"
			fdc_risultato = lds_data.getitemnumber(1, 1)
			//setnull(fs_risultato)
			
		case "CHA"
			//fs_risultato = lds_data.getitemstring(1, 1)
			setnull(fdc_risultato)
			
		case else
			destroy lds_data;
			return -1
			
	end choose
	
else
	fdc_risultato = 0.000
	//setnull(fs_risultato)
end if

destroy lds_data;

return 1
end function

public function integer uof_eval_with_datastore (string fs_testo_formula, ref string fs_risultato, ref string fs_errore);datastore				lds_eval
string						ls_dsdef, ls_risultato



//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME

ls_dsdef = 'release 6; datawindow() table(column=(type=char(255) name=a dbname="a") )'

lds_eval = create datastore
lds_eval.Create(ls_dsdef, fs_errore)
lds_eval.InsertRow(0)

if Len(fs_errore) > 0 then
	destroy lds_eval
	return -1
else
	ls_risultato = lds_eval.describe ( 'evaluate( " ' + fs_testo_formula + ' " ,1)' )
	guo_functions.uof_log("Valutazione Formula con Datastore: " + guo_functions.uof_format(fs_testo_formula) + " - RISULTATO=" + guo_functions.uof_format(ls_risultato))
	
	destroy lds_eval
	
	if ls_risultato="!" then
		fs_errore = "Errore! Verificare la formula!"
		return -1
	end if
	
	fs_risultato = ls_risultato
end if



return 0
end function

public function integer uof_calcola_formula_db (string fs_cod_formula, ref decimal fdc_risultato, ref string fs_risultato, ref string fs_errore);string					ls_risultato, ls_testo_formula, ls_testo_des_formula, ls_variabili[], ls_colonna, ls_flag_tipo_dato, &
						ls_val_variabile, ls_testo_formula_2
						
dec{4}				ld_val_variabile

datetime				ldt_val_variabile
date					ldt_date
						
long					ll_tot_variables, ll_index

boolean				lb_test_mode

integer				li_ret



lb_test_mode = true
if (ii_anno_reg_ord_ven>0 and il_num_reg_ord_ven>0 and il_prog_riga_ord_ven>0) or &
		(is_cod_prodotto_padre<>"" and is_cod_prodotto_figlio<>"" and &
		is_cod_versione_padre<>"" and is_cod_versione_figlio<>"" and il_num_sequenza>0) then
		
	lb_test_mode = false
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//LETTURA FORMULA
if fs_cod_formula="" or isnull(fs_cod_formula) then
	fs_errore = "Codice formula non specificato!"
	return -1
end if

select testo_formula,
		testo_des_formula,
		flag_tipo
into 	:ls_testo_formula,
		:ls_testo_des_formula,
		:is_tipo_ritorno
from tab_formule_db
where cod_azienda=:s_cs_xx_cod_azienda and
		cod_formula=:fs_cod_formula;

if isnull(is_tipo_ritorno) or is_tipo_ritorno="" then
	is_tipo_ritorno = "1"		//numerico
end if

if sqlca.sqlcode<0 then
	fs_errore = "Errore lettura formula: "+fs_cod_formula+": "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_testo_formula) or ls_testo_formula="" or isnull(ls_testo_des_formula) or ls_testo_des_formula="" then
	fs_errore = "Formula: "+fs_cod_formula+" non trovata oppure non specificata!"
	return -1
end if

//in ls_testo_formula la formula con le variabili (tab_variabili_formule.cod_variabile)
//in ls_testo_des_formula la formula con le variabili sostituite dai nomi di colonna (tab_variabili_formule.nome_campo_database) della tabella comp_det_ord_ven
ls_testo_formula_2 = ls_testo_formula


//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratterei che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
guo_functions.uof_log("uo_formule_calcolo.uof_pre_calcolo_formula_db(): entrata formula " +ls_testo_formula )
uof_pre_calcolo_formula_db(ls_testo_formula)
guo_functions.uof_log("uo_formule_calcolo.uof_pre_calcolo_formula_db(): uscita formula " +ls_testo_formula )


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALOREMEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

guo_functions.uof_log("verifica posizione ---" )
guo_functions.uof_log("uo_formule_calcolo.uof_estrai_stringhe_espress_reg(): entrata formula " +ls_testo_formula )
ll_tot_variables = uof_estrai_stringhe_espress_reg(ls_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if
guo_functions.uof_log("uo_formule_calcolo.uof_estrai_stringhe_espress_reg(): uscita formula " +ls_testo_formula )


//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
guo_functions.uof_log("uo_formule_calcolo.uof_setvariables_formule_db(): entrata formula " +fs_cod_formula )
li_ret = uof_setvariables_formule_db(fs_cod_formula, ls_variabili[], lb_test_mode, ls_testo_formula, ls_testo_formula_2, fs_errore)
if li_ret < 0 then
	return -1
	
elseif li_ret=1 then
	//eri in modalità testing, quindi non server + proseguire per il risultato finale
	return 0
	
end if

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME

if is_tipo_ritorno="1" then
	//numerico
	guo_functions.uof_log("uo_formule_calcolo.uof_eval_with_datastore(): entrata formula numerca " +ls_testo_formula )
	li_ret = uof_eval_with_datastore(ls_testo_formula, fdc_risultato, fs_errore)
	setnull(fs_risultato)
else
	//stringa
	guo_functions.uof_log("uo_formule_calcolo.uof_eval_with_datastore(): entrata formula stringa " +ls_testo_formula )
	li_ret = uof_eval_with_datastore(ls_testo_formula, fs_risultato, fs_errore)
	setnull(fdc_risultato)
end if

if li_ret < 0 then
	return -1
end if

return 0
end function

public function integer uof_calcola_formula_testo_per_test (string fs_testo_formula, ref string fs_risultato, ref string fs_errore);string					ls_risultato, ls_variabili[], ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_testo_formula_2
						
dec{4}				ld_val_variabile

datetime				ldt_val_variabile
date					ldt_date
						
long					ll_tot_variables, ll_index

uo_espress_reg	luo_espress_reg

boolean				lb_test_mode

datastore			lds_eval

integer				li_ret



lb_test_mode = true


ls_testo_formula_2 = fs_testo_formula


//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratterei che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(fs_testo_formula)



//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALORE MEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
li_ret = uof_setvariables_formule_db("", ls_variabili[], lb_test_mode, fs_testo_formula, ls_testo_formula_2, fs_errore)
if li_ret < 0 then
	return -1
end if


return 0

end function

public function integer uof_set_variabile (string fs_cod_variabile, ref string fs_testo_formula, ref string fs_errore);long				ll_tot_variables, ll_index
string				ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_temp
dec{4}			ld_val_variabile, fdc_risultato
datetime			ldt_val_variabile
date				ldt_date


select nome_campo_database,
		flag_tipo_dato
into 	:ls_colonna,
		:ls_flag_tipo_dato
from tab_variabili_formule
where 	cod_azienda=:s_cs_xx_cod_azienda and
			cod_variabile=:fs_cod_variabile;

ls_temp = upper(fs_cod_variabile)

//alcuni casi, già li possiamo escludere, per evitare di fare select inutili
if isnull(ls_temp) or ls_temp="" or upper(ls_temp)="IF" or ls_temp="'" or pos(ls_temp, "'")>0 or isnumber(ls_temp) then return 1

if sqlca.sqlcode<0 then
	fs_errore = "Errore lettura variabile: "+fs_cod_variabile+": "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
	
	choose case ls_flag_tipo_dato
		case "S"
			fs_errore = "La funzione uof_set_variabile NON ammette l'uso di variabili STRINGA!"
			return -1
			
		case "N"
			ld_val_variabile = 350		//valore fisso

			ls_val_variabile = string(ld_val_variabile, "########0.0000")
			guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
			guo_functions.uof_replace_string(fs_testo_formula, fs_cod_variabile, ls_val_variabile)
			
		case "D"
			fs_errore = "La funzione uof_set_variabile NON ammette l'uso di variabili DATETIME!"
			return -1
		
	end choose

	
else
	return 1
end if



return 0
end function

public function integer uof_check_variabile (string fs_cod_variabile_originale, ref string fs_cod_variabile);long				ll_tot_variables, ll_index
string				ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_temp
dec{4}			ld_val_variabile, fdc_risultato
datetime			ldt_val_variabile
date				ldt_date



ls_temp = upper(fs_cod_variabile_originale)

//alcuni casi, già li possiamo escludere, per evitare di fare select inutili
if isnull(ls_temp) or ls_temp="" or upper(ls_temp)="IF" or ls_temp="'" or pos(ls_temp, "'")>0 or isnumber(ls_temp) then return 1


select nome_campo_database,
		flag_tipo_dato
into 	:ls_colonna,
		:ls_flag_tipo_dato
from tab_variabili_formule
where 	cod_azienda=:s_cs_xx_cod_azienda and
			cod_variabile=:fs_cod_variabile_originale;

if sqlca.sqlcode<0 then
	//me ne infischio dell'errore
	return 1
	
elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
	fs_cod_variabile = "#"+ls_flag_tipo_dato+":" + fs_cod_variabile_originale+ "#"

else
	return 1
end if



return 0
end function

public function integer uof_setvariables_formule_db (string fs_variabili[], ref string fs_testo_formula, ref string fs_errore);long				ll_tot_variables, ll_index
string				ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_temp, ls_prog_riga, ls_tabella_variabile
dec{4}			ld_val_variabile, ldc_risultato
datetime			ldt_val_variabile
date				ldt_date


ll_tot_variables = upperbound(fs_variabili[])

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
for ll_index=1 to ll_tot_variables
	
	ls_temp = upper(fs_variabili[ll_index])
	
	//alcuni casi, già li possiamo escludere, per evitare di fare select inutili
	if isnull(ls_temp) or ls_temp="" or upper(ls_temp)="IF" or ls_temp="'" or pos(ls_temp, "'")>0 or isnumber(ls_temp) or &
			ls_temp="=" or  upper(ls_temp)="LEFT" or  upper(ls_temp)="POS" or upper(ls_temp)="RIGHT" or upper(ls_temp)="AND"	 or upper(ls_temp)="FIND_STRING"	then continue
	
	select nome_campo_database,
			flag_tipo_dato
	into 	:ls_colonna,
			:ls_flag_tipo_dato
	from tab_variabili_formule
	where 	cod_azienda=:s_cs_xx_cod_azienda and
				upper(cod_variabile)=:ls_temp;

	if sqlca.sqlcode<0 then
		fs_errore = "Errore lettura variabile: "+fs_variabili[ll_index]+": "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
		//nome colonna estratto
		
		//#####################################################################
		//leggi la tabella della variabile
		uof_get_tabella_variabile(fs_variabili[ll_index], ls_tabella_variabile, fs_errore)
		//#####################################################################
		
		//valuta il valore della variabile e sostituiscila nella formula
		//ls_flag_tipo_dato  può essere S (stringa) oppure N (numerico decimale) oppure D (datetime)
		
		//###########################################################################
		// ENME 20/05/2014
		if uof_calcola_variabile(ls_tabella_variabile, ls_flag_tipo_dato, fs_variabili[ll_index], ls_colonna, 	ls_val_variabile, ld_val_variabile, ldt_val_variabile, fs_errore) < 0 then
			return -1
		end if
		//###########################################################################
		guo_functions.uof_replace_string(fs_testo_formula, fs_variabili[ll_index], ls_val_variabile)
			
	else
		continue
	end if

next

return 0
end function

public function integer uof_get_tabella_variabile (string as_cod_formula, ref string as_tabella_variabile, ref string as_errore);//in base al contesto, (ORD_VEN oppure OFF_VEN) la variabile viene calcolata sulla comp_det_ord_ven oppure sulla comp_det_off_ven
//questa funzione è necessaria in quanto la tabella dell variabili da la possibilità di mettere solo comp_det_ord_ven e non quella delle offerte
//l'utente quindi definisce una sola variabile, es. DIM_X e specifica la tabella comp_det_ord_ven
//in fase di calcolo poi si controlla il contesto


select nome_tabella_database
into 	:as_tabella_variabile
from tab_variabili_formule
where 	cod_azienda=:s_cs_xx_cod_azienda and
			cod_variabile=:as_cod_formula;

if sqlca.sqlcode<0 then
	as_errore = "Errore lettura tabella variabile: "+as_cod_formula+": "+sqlca.sqlerrtext
	return -1
end if


if left(as_tabella_variabile, 9) = "comp_det_" then
	//dipende dal contesto
	if is_contesto="OFF_VEN" then
		as_tabella_variabile = "comp_det_off_ven"
	else
		as_tabella_variabile = "comp_det_ord_ven"
	end if
else
	//dipende effettivamente dalla tabella della variabile (per ora solo tabella distinta)
	//quindi as_tabella_variabile
end if

return 0
end function

public function integer uof_calcola_variabile1 (string as_tabella_variabile, string as_tipo_ritorno, string as_colonna, ref string as_valore, ref decimal ad_valore, ref datetime adt_valore, ref string as_errore);string				ls_where, ls_where_2, ls_table, ls_sql
date				ldt_date
datastore		lds_data
long				ll_count



if as_tabella_variabile="comp_det_off_ven" or as_tabella_variabile="comp_det_ord_ven" then
	//preparo per l'eventuale errore
	as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+as_tabella_variabile+" richiede "
	//--------------------------------------------------------------------
	if isnull(ii_anno_reg_ord_ven) or ii_anno_reg_ord_ven<=0 then
		as_errore += " l'anno ordine/offerta, che non è stato passato!"
		return -1
	end if
	
	if isnull(il_num_reg_ord_ven) or il_num_reg_ord_ven<=0 then
		as_errore += " il numero ordine/offerta, che non è stato passato!"
		return -1
	end if
	
	if isnull(il_prog_riga_ord_ven) or il_prog_riga_ord_ven<=0 then
		as_errore += " il numero riga ordine/offerta, che non è stato passato!"
		return -1
	end if
	//--------------------------------------------------------------------
	
elseif as_tabella_variabile="distinta" then
	//preparo per l'eventuale errore
	as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+as_tabella_variabile+" richiede "
	//--------------------------------------------------------------------
	if is_cod_prodotto_padre = "" or isnull(is_cod_prodotto_padre) then
		as_errore += " il cod.prodotto padre, che non è stato passato!"
		return -1
	end if
	
	if is_cod_prodotto_figlio = "" or isnull(is_cod_prodotto_figlio) then
		as_errore += " il cod.prodotto figlio, che non è stato passato!"
		return -1
	end if
	
	if is_cod_versione_padre = "" or isnull(is_cod_versione_padre) then
		as_errore += " la versione del padre, che non è stata passata!"
		return -1
	end if
	
	if is_cod_versione_figlio = "" or isnull(is_cod_versione_figlio) then
		as_errore += " la versione del figlio, che non è stata passata!"
		return -1
	end if
	
	if il_num_sequenza<0 or isnull(il_num_sequenza) then
		as_errore += " il num.sequenza, che non è stato passato!"
		return -1
	end if
	//--------------------------------------------------------------------	
end if

as_errore = ""

choose case as_tabella_variabile
	case "comp_det_off_ven"
		ls_where =  "anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
						"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
						"prog_riga_off_ven="+string(il_prog_riga_ord_ven)
		
		//chiesto da Alberto il 04/06/2013
		//se la variabile è su "des_prodotto" e tabella "comp_det_off_ven allora la tabella deve essere "det_off_ven"
		if upper(as_colonna) = "DES_PRODOTTO" then
			as_tabella_variabile = "det_off_ven"
		end if
		//------------------------------------
		
		//chiesto da Alberto il 13/11/2013
		//se la variabile è su "quan_offerta" e tabella "comp_det_off_ven allora la tabella deve essere "det_off_ven" e la colonna quan_offerta
		if upper(as_colonna) = "QUAN_OFFERTA" or upper(as_colonna) = "QUAN_ORDINE" then
			as_tabella_variabile = "det_off_ven"
			as_colonna = "quan_offerta"
		end if
		//------------------------------------
		
	//----------------------------------------------------------------------------------------------------------------------
	case "comp_det_ord_ven"
		ls_where =  "anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
						"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
						"prog_riga_ord_ven="+string(il_prog_riga_ord_ven)
						
		//chiesto da Alberto il 04/06/2013
		//se la variabile è su "des_prodotto" e tabella "comp_det_ord_ven allora la tabella deve essere "det_ord_ven"
		if upper(as_colonna) = "DES_PRODOTTO" then
			as_tabella_variabile = "det_ord_ven"
		end if
		//------------------------------------
		
		//chiesto da Alberto il 13/11/2013
		//se la variabile è su "quan_ordine" e tabella "comp_det_ord_ven allora la tabella deve essere "det_ord_ven" e la colonna quan_ordine
		if upper(as_colonna) = "QUAN_OFFERTA" or upper(as_colonna) = "QUAN_ORDINE" then
			as_tabella_variabile = "det_ord_ven"
			as_colonna = "quan_ordine"
		end if
		//------------------------------------
						
	//----------------------------------------------------------------------------------------------------------------------
	case "distinta"
		//where di base per la tabella distinta
		ls_where =  "cod_prodotto_padre='"+is_cod_prodotto_padre+ "' and " + &
						"cod_versione='"+is_cod_versione_padre+"' and "+&
						"num_sequenza="+string(il_num_sequenza)+" and "+&
						"cod_prodotto_figlio='"+is_cod_prodotto_figlio+"' and "+&
						"cod_versione_figlio='"+is_cod_versione_figlio+"'"
		
		//se esiste una variante (ordine, offerta o commessa, in base al contesto) la variabile va calcolata da questa tabella
		//			infatti, ad es. se una variabile fa riferimento al prodotto figlio, 
		//			questo deve essere il prodotto variante e non il figlio nella distinta base
		choose case is_contesto
			case "OFF_VEN"
				ls_table = "varianti_det_off_ven"
				ls_where_2 = 	"anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
									"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
									"prog_riga_off_ven="+string(il_prog_riga_ord_ven)
				
			case "ORD_VEN"
				ls_table = "varianti_det_ord_ven"
				ls_where_2 =  "anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
									"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
									"prog_riga_ord_ven="+string(il_prog_riga_ord_ven)
			
			case "COMMESSE"
				//in questo caso, però devono essere passate le variabili per il calcolo (li_anno_commessa, ll_num_commessa)
				
				if isnull(ii_anno_commessa) or ii_anno_commessa<=0 then
					as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+ls_table+" richiede "+&
									" l'anno della commessa, che non è stato passato!"
					return -1
				end if
				if isnull(il_num_commessa) or il_num_commessa<=0 then
					as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+ls_table+" richiede "+&
									" l'anno della commessa, che non è stato passato!"
					return -1
				end if
				
				ls_table = "varianti_commesse"
				ls_where_2 =  "anno_commessa="+string(ii_anno_commessa)+ " and " + &
									"num_commessa="+string(il_num_commessa)
				
		end choose
		
		ls_sql = 	"select count(*) from " + ls_table + " " +&
					"where cod_azienda='"+ s_cs_xx_cod_azienda+"' and "+&
							  "cod_prodotto is not null and "+&
							ls_where + " and "+ ls_where_2
		
		if guo_functions.uof_crea_datastore(lds_data, ls_sql)>0 then
		else
			as_errore = "Errore creaz. ds per controllo varianti (uof_calcola_variabile)"
			return -1
		end if
		
		//conteggio varianti presenti
		ll_count = lds_data.getitemnumber(1, 1)
		
		if ll_count>0 then
			//esiste una variante, quindi modifico i termini per fare in modo da leggere dalla tabella delle varianti
			
			as_tabella_variabile = ls_table
			ls_where = ls_where + " and " + ls_where_2
			
			//inoltre se la variabile è relativa alla colonna del prodotto figlio o della versione del prodotto figlio,
			//allora leggo la variante figlio o la versione della variante filgio, come variabile nella formula
			if as_colonna="cod_prodotto_figlio" then
				as_colonna = "cod_prodotto"			//infatti cosi si chiama la colonna nelle 3 tabelle possibili delle vaianti (ordini, offerte, commesse)
				
			elseif as_colonna="cod_versione_figlio" then
				//as_colonna = "cod_versione_figlio"	//infatti cosi si chiama la colonna nelle 3 tabelle possibili delle vaianti (ordini, offerte, commesse)
				
			end if
		end if
		
		//se non hai trovato varianti la variabile ls_where e la variabile  as_tabella_variabile è rimasta la stessa ...
		
	//----------------------------------------------------------------------------------------------------------------------
	case else
		as_errore = "Tabella variabile non specificata oppure non tra quelle previste!"
		return -1
		
end choose


choose case as_tipo_ritorno
	case "S"
		as_valore = guo_des_tabella.f_des_tabella(as_tabella_variabile, ls_where, as_colonna)
		as_valore = "~'"+as_valore+"~'"
		setnull(ad_valore)
		setnull(adt_valore)
		
		
	case "N"
		ad_valore = guo_des_tabella.f_dec_tabella(as_tabella_variabile, ls_where, as_colonna)
																		
		as_valore = string(ad_valore, "########0.0000")
		guo_functions.uof_replace_string(as_valore, ",", ".")
		setnull(adt_valore)
		
	case "D"
		adt_valore = guo_des_tabella.f_datetime_tabella(as_tabella_variabile, ls_where, as_colonna)
		
		ldt_date = date(adt_valore)
		//ls_val_variabile = string(year(ldt_date)) + right("00" + string(month(ldt_date)), 2) + right("00" + string(day(ldt_date)), 2)
		as_valore = "~'"+string(ldt_date, s_cs_xx_db_funzioni_formato_data)+"~'"
		
		//data formattata come '20120630'
		
		setnull(ad_valore)

		//todo
		//verificare SE FUNZIONA ...
	
end choose


return 0
end function

public function integer uof_calcola_variabile (string as_tabella_variabile, string as_tipo_ritorno, string as_cod_variabile, string as_colonna, ref string as_valore, ref decimal ad_valore, ref datetime adt_valore, ref string as_errore);string				ls_where, ls_where_2, ls_table, ls_sql
date				ldt_date
datastore		lds_data
long				ll_count, ll_ret



if as_tabella_variabile="comp_det_off_ven" or as_tabella_variabile="comp_det_ord_ven" then
	//preparo per l'eventuale errore
	as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+as_tabella_variabile+" richiede "
	//--------------------------------------------------------------------
	if isnull(ii_anno_reg_ord_ven) or ii_anno_reg_ord_ven<=0 then
		as_errore += " l'anno ordine/offerta, che non è stato passato!"
		return -1
	end if
	
	if isnull(il_num_reg_ord_ven) or il_num_reg_ord_ven<=0 then
		as_errore += " il numero ordine/offerta, che non è stato passato!"
		return -1
	end if
	
	if isnull(il_prog_riga_ord_ven) or il_prog_riga_ord_ven<=0 then
		as_errore += " il numero riga ordine/offerta, che non è stato passato!"
		return -1
	end if
	//--------------------------------------------------------------------
	
elseif as_tabella_variabile="distinta" then
	//preparo per l'eventuale errore
	as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+as_tabella_variabile+" richiede "
	//--------------------------------------------------------------------
	if is_cod_prodotto_padre = "" or isnull(is_cod_prodotto_padre) then
		as_errore += " il cod.prodotto padre, che non è stato passato!"
		return -1
	end if
	
	if is_cod_prodotto_figlio = "" or isnull(is_cod_prodotto_figlio) then
		as_errore += " il cod.prodotto figlio, che non è stato passato!"
		return -1
	end if
	
	if is_cod_versione_padre = "" or isnull(is_cod_versione_padre) then
		as_errore += " la versione del padre, che non è stata passata!"
		return -1
	end if
	
	if is_cod_versione_figlio = "" or isnull(is_cod_versione_figlio) then
		as_errore += " la versione del figlio, che non è stata passata!"
		return -1
	end if
	
	if il_num_sequenza<0 or isnull(il_num_sequenza) then
		as_errore += " il num.sequenza, che non è stato passato!"
		return -1
	end if
	//--------------------------------------------------------------------	
end if

as_errore = ""

choose case as_tabella_variabile
	case "comp_det_off_ven"
		ls_where =  "anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
						"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
						"prog_riga_off_ven="+string(il_prog_riga_ord_ven)
		
		//chiesto da Alberto il 04/06/2013
		//se la variabile è su "des_prodotto" e tabella "comp_det_off_ven allora la tabella deve essere "det_off_ven"
		if upper(as_colonna) = "DES_PRODOTTO" then
			as_tabella_variabile = "det_off_ven"
		end if
		//------------------------------------
		
		//chiesto da Alberto il 13/11/2013
		//se la variabile è su "quan_offerta" e tabella "comp_det_off_ven allora la tabella deve essere "det_off_ven" e la colonna quan_offerta
		if upper(as_colonna) = "QUAN_OFFERTA" or upper(as_colonna) = "QUAN_ORDINE" then
			as_tabella_variabile = "det_off_ven"
			as_colonna = "quan_offerta"
		end if
		//------------------------------------
		
						
	//----------------------------------------------------------------------------------------------------------------------
	case "comp_det_ord_ven"
		ls_where =  "anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
						"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
						"prog_riga_ord_ven="+string(il_prog_riga_ord_ven)
						
		//chiesto da Alberto il 04/06/2013
		//se la variabile è su "des_prodotto" e tabella "comp_det_ord_ven allora la tabella deve essere "det_ord_ven"
		if upper(as_colonna) = "DES_PRODOTTO" then
			as_tabella_variabile = "det_ord_ven"
		end if
		//------------------------------------
		
		//chiesto da Alberto il 13/11/2013
		//se la variabile è su "quan_ordine" e tabella "comp_det_ord_ven allora la tabella deve essere "det_ord_ven" e la colonna quan_ordine
		if upper(as_colonna) = "QUAN_OFFERTA" or upper(as_colonna) = "QUAN_ORDINE" then
			as_tabella_variabile = "det_ord_ven"
			as_colonna = "quan_ordine"
		end if
		//------------------------------------
						
	//----------------------------------------------------------------------------------------------------------------------
	case "distinta"
		//where di base per la tabella distinta
		ls_where =  "cod_prodotto_padre='"+is_cod_prodotto_padre+ "' and " + &
						"cod_versione='"+is_cod_versione_padre+"' and "+&
						"num_sequenza="+string(il_num_sequenza)+" and "+&
						"cod_prodotto_figlio='"+is_cod_prodotto_figlio+"' and "+&
						"cod_versione_figlio='"+is_cod_versione_figlio+"'"
		
		//se esiste una variante (ordine, offerta o commessa, in base al contesto) la variabile va calcolata da questa tabella
		//			infatti, ad es. se una variabile fa riferimento al prodotto figlio, 
		//			questo deve essere il prodotto variante e non il figlio nella distinta base
		choose case is_contesto
			case "OFF_VEN"
				ls_table = "varianti_det_off_ven"
				ls_where_2 = 	"anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
									"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
									"prog_riga_off_ven="+string(il_prog_riga_ord_ven)
				
			case "ORD_VEN"
				ls_table = "varianti_det_ord_ven"
				ls_where_2 =  "anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
									"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
									"prog_riga_ord_ven="+string(il_prog_riga_ord_ven)
			
			case "COMMESSE"
				//in questo caso, però devono essere passate le variabili per il calcolo (li_anno_commessa, ll_num_commessa)
				
				if isnull(ii_anno_commessa) or ii_anno_commessa<=0 then
					as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+ls_table+" richiede "+&
									" l'anno della commessa, che non è stato passato!"
					return -1
				end if
				if isnull(il_num_commessa) or il_num_commessa<=0 then
					as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+ls_table+" richiede "+&
									" l'anno della commessa, che non è stato passato!"
					return -1
				end if
				
				ls_table = "varianti_commesse"
				ls_where_2 =  "anno_commessa="+string(ii_anno_commessa)+ " and " + &
									"num_commessa="+string(il_num_commessa)
				
		end choose
		
		ls_sql = 	"select count(*) from " + ls_table + " " +&
					"where cod_azienda='"+ s_cs_xx_cod_azienda+"' and "+&
							  "cod_prodotto is not null and "+&
							ls_where + " and "+ ls_where_2
		
		if guo_functions.uof_crea_datastore(lds_data, ls_sql)>0 then
		else
			as_errore = "Errore creaz. ds per controllo varianti (uof_calcola_variabile)"
			return -1
		end if
		
		//conteggio varianti presenti
		ll_count = lds_data.getitemnumber(1, 1)
		
		if ll_count>0 then
			//esiste una variante, quindi modifico i termini per fare in modo da leggere dalla tabella delle varianti
			
			as_tabella_variabile = ls_table
			ls_where = ls_where + " and " + ls_where_2
			
			//inoltre se la variabile è relativa alla colonna del prodotto figlio o della versione del prodotto figlio,
			//allora leggo la variante figlio o la versione della variante filgio, come variabile nella formula
			if as_colonna="cod_prodotto_figlio" then
				as_colonna = "cod_prodotto"			//infatti cosi si chiama la colonna nelle 3 tabelle possibili delle vaianti (ordini, offerte, commesse)
				
			elseif as_colonna="cod_versione_figlio" then
				//as_colonna = "cod_versione_figlio"	//infatti cosi si chiama la colonna nelle 3 tabelle possibili delle vaianti (ordini, offerte, commesse)
				
			end if
		end if
		
		//se non hai trovato varianti la variabile ls_where e la variabile  as_tabella_variabile è rimasta la stessa ...
		
	//----------------------------------------------------------------------------------------------------------------------
	case "dinamica"
		choose case is_contesto
			case "OFF_VEN"
				as_tabella_variabile = "comp_det_off_ven"
			case "ORD_VEN"
				as_tabella_variabile = "comp_det_ord_ven"
		end choose
		
		
	case else
		as_errore = "Tabella variabile non specificata oppure non tra quelle previste!"
		return -1
		
end choose


choose case as_tabella_variabile
	case "comp_det_off_ven", "comp_det_ord_ven"
		
		choose case as_tipo_ritorno
			case "S"
				ls_sql = "select valore_stringa "
			case "N"
				ls_sql = "select valore_numero "
			case "D"
				ls_sql = "select valore_datetime "
		end choose
		
		if as_tabella_variabile = "comp_det_off_ven" then
			ls_sql += " from det_off_ven_conf_variabili "
		else
			ls_sql += " from det_ord_ven_conf_variabili "
		end if
		
		ls_sql += 	" where cod_azienda = '" + s_cs_xx_cod_azienda + "' and " + &
						" anno_registrazione = " + string(ii_anno_reg_ord_ven) + " and " + &
						" num_registrazione = " + string(il_num_reg_ord_ven) + " and "
						
		if as_tabella_variabile = "comp_det_off_ven" then
			ls_sql += " prog_riga_off_ven = " + string(il_prog_riga_ord_ven) + " and "
		else
			ls_sql += " prog_riga_ord_ven = " + string(il_prog_riga_ord_ven) + " and "
		end if
		
		ls_sql += 	" cod_variabile = '" + as_cod_variabile + "' "

		guo_functions.uof_log( "RICERCA VARIABILE ("+as_cod_variabile+"): " + ls_sql )
		
		ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql)

		if ll_ret < 0 then
			as_errore = "Errore in lettura valore variabile da "+as_tabella_variabile+" (uof_calcola_variabile). " + ls_sql
			guo_functions.uof_log( as_errore )
			return -1
		elseif ll_ret = 0 then
			guo_functions.uof_log( "VARIABILE ("+as_cod_variabile+") NON TROVATA in tabella conf_variabili" )	
		else
			guo_functions.uof_log( "VARIABILE ("+as_cod_variabile+") TRIVATA in tabella conf_variabili" )	
		end if

		choose case as_tipo_ritorno
			case "S"
				if lds_data.rowcount() = 0 then							
				// variabile non trovata, vado a vedere con il vecchio sistema
					as_valore = guo_des_tabella.f_des_tabella(as_tabella_variabile, ls_where, as_colonna)
				else
					as_valore = lds_data.getitemstring(1, 1)
				end if
				if isnull(as_valore) then as_valore = ''
				guo_functions.uof_log( "Variabile: " + as_cod_variabile + "=" + as_valore )
				as_valore = "~'"+as_valore+"~'"
				setnull(ad_valore)
				setnull(adt_valore)
			case "N"
				if lds_data.rowcount() = 0 then							
				// variabile non trovata, vado a vedere con il vecchio sistema
					ad_valore = guo_des_tabella.f_dec_tabella(as_tabella_variabile, ls_where, as_colonna)
				else
					ad_valore = lds_data.getitemnumber(1, 1)
				end if
				if isnull(ad_valore) then ad_valore = 0
				as_valore = string(ad_valore, "########0.0000")
				guo_functions.uof_log( "Variabile: " + as_cod_variabile + "=" + as_valore )
				guo_functions.uof_replace_string(as_valore, ",", ".")
				setnull(adt_valore)
			case "D"
				if lds_data.rowcount() = 0 then							
				// variabile non trovata, vado a vedere con il vecchio sistema
					adt_valore = guo_des_tabella.f_datetime_tabella(as_tabella_variabile, ls_where, as_colonna)
				else
					adt_valore = lds_data.getitemdatetime(1, 1)
				end if
				ldt_date = date(adt_valore)
				if isnull(ldt_date) then 
					as_valore = "~'19000101'~'"
				else				
					as_valore = "~'"+string(ldt_date, s_cs_xx_db_funzioni_formato_data)+"~'"
				end if
				guo_functions.uof_log( "Variabile: " + as_cod_variabile + "=" + string(ldt_date) )
				as_valore = "~'"+string(ldt_date, s_cs_xx_db_funzioni_formato_data)+"~'"
				setnull(ad_valore)
		end choose
		
	case else // in tutti gli altri casi
		choose case as_tipo_ritorno
			case "S"
				as_valore = guo_des_tabella.f_des_tabella(as_tabella_variabile, ls_where, as_colonna)
				guo_functions.uof_log( "Variabile=" + as_cod_variabile + "=" + as_valore + "(Tabella=" +  as_tabella_variabile + " Where=" + ls_where + " Colonna=" +  as_colonna)
				as_valore = "~'"+as_valore+"~'"
				if isnull(as_valore) then as_valore = ''
				setnull(ad_valore)
				setnull(adt_valore)
				
			case "N"
				ad_valore = guo_des_tabella.f_dec_tabella(as_tabella_variabile, ls_where, as_colonna)
				as_valore = string(ad_valore, "########0.0000")
				if isnull(ad_valore) then ad_valore = 0
				guo_functions.uof_log( "Variabile=" + as_cod_variabile + "=" + as_valore + "(Tabella=" +  as_tabella_variabile + " Where=" + ls_where + " Colonna=" +  as_colonna)
				guo_functions.uof_replace_string(as_valore, ",", ".")
				setnull(adt_valore)
				
			case "D"
				adt_valore = guo_des_tabella.f_datetime_tabella(as_tabella_variabile, ls_where, as_colonna)
				ldt_date = date(adt_valore)
				guo_functions.uof_log( "Variabile=" + as_cod_variabile + "=" + string(ldt_date) + "(Tabella=" +  as_tabella_variabile + " Where=" + ls_where + " Colonna=" +  as_colonna)
				as_valore = "~'"+string(ldt_date, s_cs_xx_db_funzioni_formato_data)+"~'"
				if isnull(ldt_date) then 
					as_valore = "~'19000101'~'"
				else				
					as_valore = "~'"+string(ldt_date, s_cs_xx_db_funzioni_formato_data)+"~'"
				end if			
				setnull(ad_valore)
			
		end choose
end choose

return 0
end function

public function integer uof_calcola_formula_db_v2 (string as_cod_azienda, string as_cod_formula, long al_riga_documento[], s_chiave_distinta as_distinta, string as_contesto, ref decimal ad_valore, ref string as_valore, ref string as_tipo_ritorno, ref string as_errore);integer							li_anno_doc, li_ret
long								ll_num_doc, ll_riga_doc


if upperbound(al_riga_documento[])<3 then
	as_errore = "Errore calcolo formula "+as_cod_formula+". E' obbligatorio fornire il riferimento riga documento (1)"
	return -1
end if

li_anno_doc = al_riga_documento[1]
ll_num_doc = al_riga_documento[2]
ll_riga_doc = al_riga_documento[3]

if li_anno_doc>0 and ll_num_doc>0 and ll_riga_doc>0 then
else
	as_errore = "Errore calcolo formula "+as_cod_formula+". E' obbligatorio fornire il riferimento riga documento (2)"
	return -1
end if

if as_contesto="ORD_VEN" or as_contesto="OFF_VEN" then
else
	as_errore = "Errore calcolo formula "+as_cod_formula+" al di fuori del contesto ordine o offerta vendita!"
	return -1
end if


this.s_cs_xx_cod_azienda = as_cod_azienda
this.ii_anno_reg_ord_ven = li_anno_doc
this.il_num_reg_ord_ven = ll_num_doc
this.il_prog_riga_ord_ven = ll_riga_doc

this.is_cod_prodotto_padre = as_distinta.cod_prodotto_padre
this.is_cod_versione_padre = as_distinta.cod_versione_padre
this.il_num_sequenza = as_distinta.num_sequenza
this.is_cod_prodotto_figlio = as_distinta.cod_prodotto_figlio
this.is_cod_versione_figlio = as_distinta.cod_versione_figlio

this.is_contesto = as_contesto

guo_functions.uof_log("uo_formule_calcolo.uof_calcola_formula_db_v2(): entrata formula " +as_cod_formula )

li_ret = uof_calcola_formula_db(as_cod_formula, ad_valore, as_valore, as_errore)

guo_functions.uof_log("uo_formule_calcolo.uof_calcola_formula_db_v2(): uscita formula " +as_cod_formula )

as_tipo_ritorno = this.is_tipo_ritorno

if li_ret < 0 then
	as_errore = "Errore calcolo formula "+as_cod_formula+ " : "+as_errore
	return -1
end if

return 0




end function

on uo_formule_calcolo.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_formule_calcolo.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iuo_cache = create uo_array 
end event

event destructor;destroy iuo_cache;
end event

