﻿$PBExportHeader$uo_return_base.sru
forward
global type uo_return_base from nonvisualobject
end type
end forward

global type uo_return_base from nonvisualobject
end type
global uo_return_base uo_return_base

type variables
// oggetto base per ritornare i valori
public: 
boolean ib_error = false
uo_return_error iuo_error
end variables

on uo_return_base.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_return_base.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

