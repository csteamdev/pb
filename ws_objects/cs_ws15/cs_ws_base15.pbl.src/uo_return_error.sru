﻿$PBExportHeader$uo_return_error.sru
forward
global type uo_return_error from nonvisualobject
end type
end forward

global type uo_return_error from nonvisualobject
end type
global uo_return_error uo_return_error

type variables
public:
long errorcode
string errortext
end variables

on uo_return_error.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_return_error.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

