﻿$PBExportHeader$uo_validazione_valori.sru
forward
global type uo_validazione_valori from nonvisualobject
end type
end forward

global type uo_validazione_valori from nonvisualobject
end type
global uo_validazione_valori uo_validazione_valori

type variables
public:
string		cod_prodotto_finito
string 		cod_valore
string 		valore
string		cod_formula
boolean 	valido
end variables

on uo_validazione_valori.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_validazione_valori.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

