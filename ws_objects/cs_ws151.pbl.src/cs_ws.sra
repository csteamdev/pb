﻿$PBExportHeader$cs_ws.sra
$PBExportComments$Generated Application Object
forward
global type cs_ws from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
uo_functions guo_functions
uo_des_tabella guo_des_tabella
string id_sessione=""
boolean gb_log=false
end variables

global type cs_ws from application
string appname = "cs_ws"
end type
global cs_ws cs_ws

on cs_ws.create
appname="cs_ws"
message=create message
sqlca=create transaction
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on cs_ws.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;//guo_functions = create uo_functions
//guo_des_tabella = create uo_des_tabella



end event

