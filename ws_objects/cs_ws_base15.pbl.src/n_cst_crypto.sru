﻿$PBExportHeader$n_cst_crypto.sru
forward
global type n_cst_crypto from nonvisualobject
end type
end forward

global type n_cst_crypto from nonvisualobject
end type
global n_cst_crypto n_cst_crypto

type prototypes

end prototypes

type variables
// Chiavi per la generazione del codice di Attivazione.
string is_private_key = "CONSULTIGXZAB"

// variabili globali per algoritmo crypt/decrypt password
string	gs_crypt_accepted_chars[100]
int		gl_crypt_code_table[100,5]

end variables

forward prototypes
public function integer find_code (string as_char)
public function integer find_char (long al_code, long al_table_y)
public function integer decryptdata (string as_to_decrypt, ref string as_decrypted)
public function string old_decrypt (string data)
public function integer find_codeserial (string as_char)
public function integer encryptdata (string as_to_crypt, ref string as_crypted)
public function string encryptserial (string as_to_crypt)
public function boolean decryptserial (string as_public_key, string as_public_code)
public function string encrypt_data (string as_data)
public function string decrypt_data (string as_data)
end prototypes

public function integer find_code (string as_char);long		ll_code

boolean	lb_found = false


for ll_code = 1 to upperbound(gs_crypt_accepted_chars)
	if gs_crypt_accepted_chars[ll_code] = as_char then
		lb_found = true
		exit
	end if
next

if not lb_found then
	return -1
else
	return ll_code
end if
end function

public function integer find_char (long al_code, long al_table_y);long		ll_table_x

boolean	lb_found = false


for ll_table_x = 1 to upperbound(gl_crypt_code_table,1)
	if gl_crypt_code_table[ll_table_x,al_table_y] = al_code then
		lb_found = true
		exit
	end if
next

if not lb_found then
	return -1
else
	return ll_table_x
end if
end function

public function integer decryptdata (string as_to_decrypt, ref string as_decrypted);string	ls_code, ls_char

long	ll_pos, ll_pos_code, ll_code, ll_table_x, ll_table_y


as_decrypted = ""

ll_pos_code = 0

// si scorre la stringa ricevuta come input processando una coppia di caratteri alla volta
for ll_pos = 1 to len(as_to_decrypt) step +2
	
	// dato che in fase di decrypt dobbiamo considerare una coppia di caratteri alla volta,
	// dobbiamo considerare come posizione il numero di coppia e non la posizione sulla stringa di input
	ll_pos_code ++
	
	ls_code = mid(as_to_decrypt,ll_pos,2)
	
	// se il frammento di stringa in esame non corrisponde ad un numero, significa che la stringa passata come
	// parametro non è stata codificata dall'algoritmo di crypt corrente.
	// sarà necessario gestire tale errore visualizzando una messagebox nell'oggetto chiamante
	if not isnumber(ls_code) then
		return -1
	end if
	
	// si converte in numerico la coppia di caratteri in esame per ottenere il codice di crypt
	ll_code = long(ls_code)
	
	// la tabella dei codici di crypt si ripete ogni 5 posizioni, si procede quindi dividendo le coppie di caratteri
	// nella stringa iniziale in multipli di 5
	ll_table_y = mod(ll_pos_code,5)
	
	if  ll_table_y = 0 then
		ll_table_y = 5
	end if	
	
	// la funzione find_char cerca di individuare la posizione del codice in esame nella relativa matrice
	ll_table_x = find_char(ll_code, ll_table_y)
	
	// se ll_table_x è -1 significa che il codice processato non è fra quelli utilizzati dall'algoritmo di crypt
	// il che significa di conseguenza che la stringa di input non è valida
	// sarà necessario gestire tale errore visualizzando una messagebox nell'oggetto chiamante
	if ll_table_x = -1 then
		return -1
	end if
	
	// a questo punto la posizione del codice nella tabella dei codici di cryp (ll_table_x) rappresenterà
	// l'ndice con cui possiamo andare ad individuare il carattere corrispondente
	// nel relativo array predefinito
	ls_char = gs_crypt_accepted_chars[ll_table_x]
	
	// si costruisce la stringa che rappresenterà il risultato finale dell'algoritmo di decrypt
	as_decrypted += ls_char
	
next

return 0
end function

public function string old_decrypt (string data);string  ls_stringa_1, ls_stringa_2, ls_chiave, ls_appo, ls_appo_2
integer li_i, li_l, li_k, li_v 

//stringa1 è il testo da criptare, stringa2 il testo criptato, chiave la chiave da usare, se k è 1 allora cripta, se k è -1 decripta

ls_stringa_1 = data

ls_chiave = "wlf69ciccio"

li_k = - 1

for li_i = 1 to len(ls_stringa_1)
    li_l = li_l + 1
	 ls_appo = mid( ls_stringa_1, li_i, 1)
	 ls_appo_2 = mid( ls_chiave, li_l, 1)
	 li_v = asca(ls_appo) + li_k * asca(ls_appo_2)
    if li_v > 255 then li_v = li_v - 256
    if li_v < 0 then li_v = li_v + 256
    ls_stringa_2 = ls_stringa_2 + char(li_v)
    if li_l = len(ls_chiave) then li_l=0
next

return ls_stringa_2
end function

public function integer find_codeserial (string as_char);// ritorno l'intero corrispondente alla lettera contenuta nell'array
//long ll_i
//
//for ll_i = 1 to  upperbound(is_array_key)
//	
//		if is_array_key[ll_i] = as_char then
//			
//			return ll_i -1
//			
//		end if
//		
//next

return -1
end function

public function integer encryptdata (string as_to_crypt, ref string as_crypted);string	ls_char

long	ll_pos, ll_table_x, ll_table_y, ll_code


as_crypted = ""

// si scorre la stringa ricevuta come input processando un carattere alla volta
for ll_pos = 1 to len(as_to_crypt)
	
	ls_char = mid(as_to_crypt,ll_pos,1)
	
	// la funzione find_code cerca di individuare la posizione del carattere in esame nella tabella dei caratteri ammessi
	ll_table_x = find_code(ls_char)
	
	// se ll_code è -1 significa che il carattere processato non è fra quelli ammessi
	// sarà necessario gestire tale errore visualizzando una messagebox nell'oggetto chiamante
	if ll_table_x = -1 then
		return -1
	end if
	
	// la tabella dei codici di cypt si ripete ogni 5 posizioni, si procede quindi dividendo la stringa iniziale in multipli di 5
	ll_table_y = mod(ll_pos,5)
	
	if  ll_table_y = 0 then
		ll_table_y = 5
	end if
	
	// a questo punto la posizione del carattere nella tabella caratteri (ll_table_x) e nella stringa di input (ll_table_y),
	// rappresenteranno le coordinate con cui possiamo andare ad individuare il codice numerico corrispondente
	// nella relativa matrice predefinita
	ll_code = gl_crypt_code_table[ll_table_x,ll_table_y]
	
	// convertiamo il codice numerico ottenuto in una stringa a due cifre
	ls_char = string(ll_code,"00")
	
	// si costruisce la stringa che rappresenterà il risultato finale dell'algoritmo di crypt
	as_crypted += ls_char
	
next

return 0
end function

public function string encryptserial (string as_to_crypt);// *** stefanop 11/07/2008: Medoto di criptazione basato su una chiave pubblica e una privata.

string 	ls_pubblic_key, ls_nested_key, ls_complete_key
integer 	li_i, li_pos
long 	  	ll_mod_key

if len(trim(as_to_crypt)) < 1 then return "Error: the  key is not valid"


ls_nested_key = string( abs(integer(as_to_crypt) * 127 + integer(len(is_private_key))) ) + as_to_crypt

// Scansiono la tabella delle chiavi da usare per comporre la chiave
for li_i=1 to len(ls_nested_key)
	
	li_pos = integer(mid(ls_nested_key, li_i, 1))
	
	ls_complete_key += upper( mid(is_private_key, li_pos, 1) )
	
next

return ls_complete_key
end function

public function boolean decryptserial (string as_public_key, string as_public_code);// *** stefano 14/07/2008

string ls_client_code, ls_comunicated_code

ls_client_code = encryptserial( as_public_key )

if len(trim(as_public_code)) < 1 then return false

if ls_client_code = as_public_code then	return true

return false
end function

public function string encrypt_data (string as_data);string ls_date, ls_char
integer li_i, li_n

for li_i = 1 to len(as_data) 

	ls_char = mid(as_data, li_i, 1)
	
	if ls_char = '/' then 
		ls_date += '$'
	else
		li_n = integer(ls_char) + 1
		ls_date += mid(is_private_key, li_n, 1)
	end if
	
next

return ls_date
end function

public function string decrypt_data (string as_data);string ls_date, ls_char
integer li_i, li_n

for li_i = 1 to len(as_data) 

	ls_char = mid(as_data, li_i, 1)
	
	if ls_char = '$' then 
		ls_date += '/'
	else
		li_n = pos(is_private_key, ls_char) -1
		ls_date += string(li_n)
	end if
	
next

return ls_date
end function

on n_cst_crypto.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_cst_crypto.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;// impostazione set caratteri accettati per l'algoritmo di crypt/decrypt
gs_crypt_accepted_chars[1]		= "!"
gs_crypt_accepted_chars[2]		= "#"
gs_crypt_accepted_chars[3]		= "$"
gs_crypt_accepted_chars[4]		= "%"
gs_crypt_accepted_chars[5]		= "&"
gs_crypt_accepted_chars[6]		= "("
gs_crypt_accepted_chars[7]		= ")"
gs_crypt_accepted_chars[8]		= "*"
gs_crypt_accepted_chars[9]		= "+"
gs_crypt_accepted_chars[10]	= ","
gs_crypt_accepted_chars[11]	= "-"
gs_crypt_accepted_chars[12]	= "."
gs_crypt_accepted_chars[13]	= "/"
gs_crypt_accepted_chars[14]	= "0"
gs_crypt_accepted_chars[15]	= "1"
gs_crypt_accepted_chars[16]	= "2"
gs_crypt_accepted_chars[17]	= "3"
gs_crypt_accepted_chars[18]	= "4"
gs_crypt_accepted_chars[19]	= "5"
gs_crypt_accepted_chars[20]	= "6"
gs_crypt_accepted_chars[21]	= "7"
gs_crypt_accepted_chars[22]	= "8"
gs_crypt_accepted_chars[23]	= "9"
gs_crypt_accepted_chars[24]	= ":"
gs_crypt_accepted_chars[25]	= ";"
gs_crypt_accepted_chars[26]	= "<"
gs_crypt_accepted_chars[27]	= "="
gs_crypt_accepted_chars[28]	= ">"
gs_crypt_accepted_chars[29]	= "?"
gs_crypt_accepted_chars[30]	= "@"
gs_crypt_accepted_chars[31]	= "A"
gs_crypt_accepted_chars[32]	= "B"
gs_crypt_accepted_chars[33]	= "C"
gs_crypt_accepted_chars[34]	= "D"
gs_crypt_accepted_chars[35]	= "E"
gs_crypt_accepted_chars[36]	= "F"
gs_crypt_accepted_chars[37]	= "G"
gs_crypt_accepted_chars[38]	= "H"
gs_crypt_accepted_chars[39]	= "I"
gs_crypt_accepted_chars[40]	= "J"
gs_crypt_accepted_chars[41]	= "K"
gs_crypt_accepted_chars[42]	= "L"
gs_crypt_accepted_chars[43]	= "M"
gs_crypt_accepted_chars[44]	= "N"
gs_crypt_accepted_chars[45]	= "O"
gs_crypt_accepted_chars[46]	= "P"
gs_crypt_accepted_chars[47]	= "Q"
gs_crypt_accepted_chars[48]	= "R"
gs_crypt_accepted_chars[49]	= "S"
gs_crypt_accepted_chars[50]	= "T"
gs_crypt_accepted_chars[51]	= "U"
gs_crypt_accepted_chars[52]	= "V"
gs_crypt_accepted_chars[53]	= "W"
gs_crypt_accepted_chars[54]	= "X"
gs_crypt_accepted_chars[55]	= "Y"
gs_crypt_accepted_chars[56]	= "Z"
gs_crypt_accepted_chars[57]	= "["
gs_crypt_accepted_chars[58]	= "\"
gs_crypt_accepted_chars[59]	= "]"
gs_crypt_accepted_chars[60]	= "^"
gs_crypt_accepted_chars[61]	= "_"
gs_crypt_accepted_chars[62]	= "a"
gs_crypt_accepted_chars[63]	= "b"
gs_crypt_accepted_chars[64]	= "c"
gs_crypt_accepted_chars[65]	= "d"
gs_crypt_accepted_chars[66]	= "e"
gs_crypt_accepted_chars[67]	= "f"
gs_crypt_accepted_chars[68]	= "g"
gs_crypt_accepted_chars[69]	= "h"
gs_crypt_accepted_chars[70]	= "i"
gs_crypt_accepted_chars[71]	= "j"
gs_crypt_accepted_chars[72]	= "k"
gs_crypt_accepted_chars[73]	= "l"
gs_crypt_accepted_chars[74]	= "m"
gs_crypt_accepted_chars[75]	= "n"
gs_crypt_accepted_chars[76]	= "o"
gs_crypt_accepted_chars[77]	= "p"
gs_crypt_accepted_chars[78]	= "q"
gs_crypt_accepted_chars[79]	= "r"
gs_crypt_accepted_chars[80]	= "s"
gs_crypt_accepted_chars[81]	= "t"
gs_crypt_accepted_chars[82]	= "u"
gs_crypt_accepted_chars[83]	= "v"
gs_crypt_accepted_chars[84]	= "w"
gs_crypt_accepted_chars[85]	= "x"
gs_crypt_accepted_chars[86]	= "y"
gs_crypt_accepted_chars[87]	= "z"
gs_crypt_accepted_chars[88]	= "{"
gs_crypt_accepted_chars[89]	= "|"
gs_crypt_accepted_chars[90]	= "}"
gs_crypt_accepted_chars[91]	= ""		// non usato
gs_crypt_accepted_chars[92]	= ""		// non usato
gs_crypt_accepted_chars[93]	= ""		// non usato
gs_crypt_accepted_chars[94]	= ""		// non usato
gs_crypt_accepted_chars[95]	= ""		// non usato
gs_crypt_accepted_chars[96]	= ""		// non usato
gs_crypt_accepted_chars[97]	= ""		// non usato
gs_crypt_accepted_chars[98]	= ""		// non usato
gs_crypt_accepted_chars[99]	= ""		// non usato
gs_crypt_accepted_chars[100]	= ""		// non usato

// impostazione codici numerici
// usiamo 5 array distinti in modo che lo stesso carattere assuma codici di crypt diversi a seconda della sua posizione
// nella stringa di origine. questo dovrebbe rendere più difficile individuare l'algoritmo applicato.
long ll_x, ll_y, ll_code

for ll_y = 1 to 5
	
	choose case ll_y
		case 1
			ll_code = 60
		case 2
			ll_code = 20
		case 3
			ll_code = 0
		case 4
			ll_code = 40
		case 5
			ll_code = 80
	end choose
	
	for ll_x = 1 to 100
		
		ll_code++
		
		if ll_code = 100 then
			ll_code = 0
		end if
		
		gl_crypt_code_table[ll_x,ll_y] = ll_code
		
	next
	
next
end event

