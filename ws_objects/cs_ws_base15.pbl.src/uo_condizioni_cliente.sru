﻿$PBExportHeader$uo_condizioni_cliente.sru
$PBExportComments$UserObject Principale per richiamo funzione di ricerca condizioni clienti
forward
global type uo_condizioni_cliente from nonvisualobject
end type
end forward

global type uo_condizioni_cliente from nonvisualobject
end type
global uo_condizioni_cliente uo_condizioni_cliente

type variables
boolean ib_setitem=FALSE, ib_setitem_provvigioni=FALSE
boolean ib_prezzi = TRUE, ib_provvigioni = TRUE
boolean ib_prezzo_speciale=FALSE
boolean ib_provvigione_da_variante_cliente=FALSE
boolean lb_flag_escludi_linee_sconto=FALSE
str_parametri str_parametri
str_output str_output
string is_valuta_lire, is_flag_gruppi_sconto

// ----------- aggiunto per Viropa ---------------  //
string is_prodotti_addizionali[]

// aggiunto per rendere l'oggetto indipendente
string 		s_cs_xx_cod_azienda, s_cs_xx_db_funzioni_formato_data, s_cs_xx_listino_db_tipo_gestione,s_cs_xx_listino_db_flag_calcola, s_cs_xx_listino_db_cod_versione, &
				s_cs_xx_listino_db_cod_tipo_listino_prodotto, s_cs_xx_listino_db_cod_valuta
datetime 	s_cs_xx_listino_db_data_inizio_val
long 			s_cs_xx_listino_db_progressivo,s_cs_xx_listino_db_anno_documento, s_cs_xx_listino_db_num_documento,s_cs_xx_listino_db_prog_riga
dec{4} 		s_cs_xx_listino_db_quan_prodotto_finito


//----------- aggiunto per Zanzar per la gestione delle tabelle dimensionali con variabili -------------------------
str_listini_ven_dim  istr_listini_ven_dim
string		is_cod_prodotto_origine
end variables

forward prototypes
public function integer uof_calcola_sconto_tot (double fd_sconti[], ref double fd_sconto_tot)
public function integer wf_calcola_variazione (string flag_sconto_mag_prezzo, double fd_variazione, double fd_valore_origine, ref double fd_prezzo_finale)
public function integer uof_arrotonda_prezzo_decimal (decimal ad_prezzo_ven, string as_cod_valuta, ref decimal ad_prezzo_arrotondato, ref string as_messaggio)
public function integer uof_arrotonda_prezzo (double ad_prezzo_ven, string as_cod_valuta, ref double ad_prezzo_arrotondato, ref string as_messaggio)
public function integer uof_arrotonda_prezzo (decimal ad_prezzo_ven, string as_cod_valuta, ref decimal ad_prezzo_arrotondato, ref string as_messaggio)
public function integer wf_componi_prezzo (string fs_cod_prodotto, string flag_sconto_mag_prezzo, double fd_variazione, double fd_variazione_precedente, double fd_prezzo_vendita_precedente, string flag_origine_prezzo, string flag_sconto_a_parte, boolean fb_provenienza_condizione_standard, string fs_cod_cliente, ref double fd_sconto, ref double fd_maggiorazione, ref double fd_prezzo, ref string fs_messaggio)
public function integer wf_ricerca_condizioni_prodotto (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_riferimento_val, string fs_cod_prodotto, double fd_dimensione_1, double fd_dimensione_2, double fd_quan_scaglione, double fd_val_scaglione, boolean fb_provenienza_condizione_standard, string fs_cod_cliente, ref double fd_sconto[], ref double fd_maggiorazione[], ref double fd_variazione[], boolean fb_cerca_dimensioni)
public function integer wf_ricerca_condizioni_distinta (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, string fs_cod_prodotto_listino, string fs_cod_versione, long fl_progressivo, s_trova_mp_varianti fs_trova_mp_varianti[], boolean fb_provenienza_condizione_standard, string fs_cod_cliente, ref decimal fd_valore_pf, ref string fs_messaggio)
public function integer wf_componi_prezzo_diba_decimal (string fs_cod_prodotto, string flag_sconto_mag_prezzo, decimal fd_variazione, decimal fd_variazione_precedente, decimal fd_prezzo_vendita_precedente, string flag_origine_prezzo, string flag_sconto_a_parte, ref decimal fd_sconto, ref decimal fd_maggiorazione, ref decimal fd_prezzo, ref string fs_messaggio)
public function integer wf_calcola_variazione_diba_decimal (string flag_sconto_mag_prezzo, decimal fd_variazione, decimal fd_valore_origine, ref decimal fd_prezzo_finale)
public subroutine wf_condizioni_cliente (ref string as_errore)
public function integer wf_ricerca_provv_agenti (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_riferimento_val, string fs_cod_agente, string fs_cod_cliente, string fs_cod_prodotto, double fd_quan_scaglione, double fd_sconto_tot, ref double fd_variazione[], ref string as_errore)
public function integer wf_ricerca_cond_dimensioni (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, long fl_num_scaglione, str_listini_ven_dim astr_listini_ven_dim, ref string fs_flag_sconto_mag_prezzo, ref double fd_variazione)
public function integer wf_ricerca_condizioni_clienti (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_riferimento_val, string fs_cod_cliente, string fs_cod_prodotto, str_listini_ven_dim astr_listini_ven_dim, double fd_quan_scaglione, double fd_val_scaglione, ref double fd_sconto[], ref double fd_maggiorazione[], ref double fd_variazione[], boolean fb_cerca_dimensioni, ref double fd_minimo_fatt_altezza, ref double fd_minimo_fatt_larghezza, ref double fd_minimo_fatt_profondita, ref double fd_minimo_fatt_superficie, ref double fd_minimo_fatt_volume, ref string as_errore)
public function integer wf_listino_log (string as_nota_variazione)
end prototypes

public function integer uof_calcola_sconto_tot (double fd_sconti[], ref double fd_sconto_tot);long ll_i, ll_max
double ld_perc


ll_max=upperbound(fd_sconti)
if ll_max = 0 or isnull(ll_max) then
	fd_sconto_tot = 0
	return 0
end if

ld_perc = 100
for ll_i = 1 to ll_max
	ld_perc = ld_perc - ((ld_perc * fd_sconti[ll_i]) / 100)
next

fd_sconto_tot = 100 - ld_perc

return 0
end function

public function integer wf_calcola_variazione (string flag_sconto_mag_prezzo, double fd_variazione, double fd_valore_origine, ref double fd_prezzo_finale);
choose case flag_sconto_mag_prezzo
	case "S"
		fd_prezzo_finale = fd_valore_origine - ((fd_valore_origine / 100 ) * fd_variazione)
		fd_prezzo_finale = round(fd_prezzo_finale, 4)
	case "M"
		fd_prezzo_finale = fd_valore_origine + ((fd_valore_origine / 100 ) * fd_variazione)
		fd_prezzo_finale = round(fd_prezzo_finale, 4)
	case "D"
		fd_prezzo_finale = fd_valore_origine - fd_variazione
	case "A"
		fd_prezzo_finale = fd_valore_origine + fd_variazione
	case "P"
		fd_prezzo_finale = fd_variazione
	case else
		return -1
end choose

return 0
end function

public function integer uof_arrotonda_prezzo_decimal (decimal ad_prezzo_ven, string as_cod_valuta, ref decimal ad_prezzo_arrotondato, ref string as_messaggio);// funzione che arrotonda il prezzo fd_prezzo_ven secondo quanto indicato nella tabella
// tab_arrotonda_prezzi : restituisce il prezzo arrotondato

decimal ld_scorporo, ld_scaglione_da[], ld_scaglione_a[], ld_variazione[], ld_prezzo_tondo

long    ll_num_scaglioni, ll_i

string  ls_flag_tipo_arrotonda


ad_prezzo_arrotondato = ad_prezzo_ven	// se non trova scaglioni, non arrotonda

select count(scaglione_da)
into   :ll_num_scaglioni
from   tab_arrotonda_prezzi
where  cod_azienda = :s_cs_xx_cod_azienda and 
       cod_valuta = :as_cod_valuta;
if sqlca.sqlcode = 100	then  return 0

if sqlca.sqlcode < 0	then
	as_messaggio = "Errore in lettura tabella tab_arrotonda_prezzi"
	return -1
end if
	
if ll_num_scaglioni > 0 then
	ll_i = 0
	declare cu_scaglioni cursor for 
	select   scaglione_da, scaglione_a, variazione, flag_tipo_arrotondamento
	from tab_arrotonda_prezzi
	where cod_azienda = :s_cs_xx_cod_azienda 
		and cod_valuta = :as_cod_valuta
	order by scaglione_da asc;
	open cu_scaglioni;
	do while 0 = 0
		ll_i = ll_i +1
		fetch cu_scaglioni into :ld_scaglione_da[ll_i], :ld_scaglione_a[ll_i], :ld_variazione[ll_i], :ls_flag_tipo_arrotonda ;
		if (sqlca.sqlcode <> 0 or ll_i = ll_num_scaglioni) then 	exit
		if ((ad_prezzo_ven >= ld_scaglione_da[ll_i]) and (ad_prezzo_ven < ld_scaglione_a[ll_i])) then exit
	loop
	close cu_scaglioni;
	// applica l'ultimo scaglione trovato
	if ld_variazione[ll_i] <> 0 then
		// arrotondamento per difetto
		ld_prezzo_tondo = (truncate((ad_prezzo_ven/ld_variazione[ll_i]), 0) * ld_variazione[ll_i])
		// arrotondamento per eccesso
		if ((ls_flag_tipo_arrotonda = "E" ) and (ad_prezzo_ven - ld_prezzo_tondo <> 0)) or (ls_flag_tipo_arrotonda = "R" and (ad_prezzo_ven - ld_prezzo_tondo > ld_variazione[ll_i]/2)) then
			ld_prezzo_tondo = ld_prezzo_tondo + ld_variazione[ll_i]
		end if	
		ad_prezzo_arrotondato = ld_prezzo_tondo
	end if	
end if

return 0
end function

public function integer uof_arrotonda_prezzo (double ad_prezzo_ven, string as_cod_valuta, ref double ad_prezzo_arrotondato, ref string as_messaggio);// funzione che arrotonda il prezzo fd_prezzo_ven secondo quanto indicato nella tabella
// tab_arrotonda_prezzi : restituisce il prezzo arrotondato

double ld_scorporo, ld_scaglione_da[], ld_scaglione_a[], ld_variazione[], ld_prezzo_tondo, ld_diff
long ll_num_scaglioni, ll_i
string ls_flag_tipo_arrotonda

ad_prezzo_arrotondato = ad_prezzo_ven	// se non trova scaglioni, non arrotonda

select count(scaglione_da)
into   :ll_num_scaglioni
from   tab_arrotonda_prezzi
where  cod_azienda = :s_cs_xx_cod_azienda and 
       cod_valuta = :as_cod_valuta;
if sqlca.sqlcode = 100	then  return 0

if sqlca.sqlcode < 0	then
	as_messaggio = "Errore in lettura tabella tab_arrotonda_prezzi"
	return -1
end if
	
if ll_num_scaglioni > 0 then
	ll_i = 0
	declare cu_scaglioni cursor for 
	select   scaglione_da, scaglione_a, variazione, flag_tipo_arrotondamento
	from tab_arrotonda_prezzi
	where cod_azienda = :s_cs_xx_cod_azienda 
		and cod_valuta = :as_cod_valuta
	order by scaglione_da asc;
	open cu_scaglioni;
	do while 0 = 0
		ll_i = ll_i +1
		fetch cu_scaglioni into :ld_scaglione_da[ll_i], :ld_scaglione_a[ll_i], :ld_variazione[ll_i], :ls_flag_tipo_arrotonda ;
		if (sqlca.sqlcode <> 0 or ll_i = ll_num_scaglioni) then 	exit
		if ((ad_prezzo_ven >= ld_scaglione_da[ll_i]) and (ad_prezzo_ven < ld_scaglione_a[ll_i])) then exit
	loop
	close cu_scaglioni;
	// applica l'ultimo scaglione trovato
	if ld_variazione[ll_i] <> 0 then
		// arrotondamento per difetto
		ld_prezzo_tondo = (truncate((ad_prezzo_ven/ld_variazione[ll_i]), 0) * ld_variazione[ll_i])
		// arrotondamento per eccesso
		ld_diff = ad_prezzo_ven - ld_prezzo_tondo
		ld_diff = round(ld_diff,4)
		if ((ls_flag_tipo_arrotonda = "E" ) and (ld_diff <> 0)) or (ls_flag_tipo_arrotonda = "R" and (ad_prezzo_ven - ld_prezzo_tondo > ld_variazione[ll_i]/2)) then
			ld_prezzo_tondo = ld_prezzo_tondo + ld_variazione[ll_i]
		end if	
		ad_prezzo_arrotondato = ld_prezzo_tondo
	end if	
end if
return 0
end function

public function integer uof_arrotonda_prezzo (decimal ad_prezzo_ven, string as_cod_valuta, ref decimal ad_prezzo_arrotondato, ref string as_messaggio);// funzione che arrotonda il prezzo fd_prezzo_ven secondo quanto indicato nella tabella
// tab_arrotonda_prezzi : restituisce il prezzo arrotondato

dec{4} ld_scorporo, ld_scaglione_da[], ld_scaglione_a[], ld_variazione[], ld_prezzo_tondo, ld_diff
long ll_num_scaglioni, ll_i
string ls_flag_tipo_arrotonda

ad_prezzo_arrotondato = ad_prezzo_ven	// se non trova scaglioni, non arrotonda

select count(scaglione_da)
into   :ll_num_scaglioni
from   tab_arrotonda_prezzi
where  cod_azienda = :s_cs_xx_cod_azienda and 
       cod_valuta = :as_cod_valuta;
if sqlca.sqlcode = 100	then  return 0

if sqlca.sqlcode < 0	then
	as_messaggio = "Errore in lettura tabella tab_arrotonda_prezzi"
	return -1
end if
	
if ll_num_scaglioni > 0 then
	ll_i = 0
	declare cu_scaglioni cursor for 
	select   scaglione_da, scaglione_a, variazione, flag_tipo_arrotondamento
	from tab_arrotonda_prezzi
	where cod_azienda = :s_cs_xx_cod_azienda 
		and cod_valuta = :as_cod_valuta
	order by scaglione_da asc;
	open cu_scaglioni;
	do while 0 = 0
		ll_i = ll_i +1
		fetch cu_scaglioni into :ld_scaglione_da[ll_i], :ld_scaglione_a[ll_i], :ld_variazione[ll_i], :ls_flag_tipo_arrotonda ;
		if (sqlca.sqlcode <> 0 or ll_i = ll_num_scaglioni) then 	exit
		if ((ad_prezzo_ven >= ld_scaglione_da[ll_i]) and (ad_prezzo_ven < ld_scaglione_a[ll_i])) then exit
	loop
	close cu_scaglioni;
	// applica l'ultimo scaglione trovato
	if ld_variazione[ll_i] <> 0 then
		// arrotondamento per difetto
		ld_prezzo_tondo = (truncate((ad_prezzo_ven/ld_variazione[ll_i]), 0) * ld_variazione[ll_i])
		// arrotondamento per eccesso
		ld_diff = ad_prezzo_ven - ld_prezzo_tondo
		ld_diff = round(ld_diff,4)
		if ((ls_flag_tipo_arrotonda = "E" ) and (ld_diff <> 0)) or (ls_flag_tipo_arrotonda = "R" and (ad_prezzo_ven - ld_prezzo_tondo > ld_variazione[ll_i]/2)) then
			ld_prezzo_tondo = ld_prezzo_tondo + ld_variazione[ll_i]
		end if	
		ad_prezzo_arrotondato = ld_prezzo_tondo
	end if	
end if
return 0
end function

public function integer wf_componi_prezzo (string fs_cod_prodotto, string flag_sconto_mag_prezzo, double fd_variazione, double fd_variazione_precedente, double fd_prezzo_vendita_precedente, string flag_origine_prezzo, string flag_sconto_a_parte, boolean fb_provenienza_condizione_standard, string fs_cod_cliente, ref double fd_sconto, ref double fd_maggiorazione, ref double fd_prezzo, ref string fs_messaggio);string ls_cod_materia_prima[], ls_messaggio, ls_cod_versione_prima[], ls_cod_deposito_produzione, ls_null
long	ll_i, ll_cont
double ld_costo_standard, ld_costo_ultimo, ld_prezzo_acquisto, ld_prezzo_vendita, ld_costo_produzione, &
       ld_quantita_utilizzo[], ld_prezzo
dec{4} ld_prezzo_decimal

setnull(ls_null)

select anag_prodotti.costo_standard,   
       	anag_prodotti.costo_ultimo,   
       	anag_prodotti.prezzo_acquisto,   
       	anag_prodotti.prezzo_vendita  
into   :ld_costo_standard,   
       	:ld_costo_ultimo,   
       	:ld_prezzo_acquisto,   
       	:ld_prezzo_vendita  
from   anag_prodotti
where  cod_azienda = :s_cs_xx_cod_azienda and
       	cod_prodotto = :fs_cod_prodotto;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca del prodotto " + fs_cod_prodotto
	return -1
end if

guo_functions.uof_log("Wf_componi_prezzo(). flag_origine_prezzo=" + flag_origine_prezzo)

choose case flag_origine_prezzo
	case "A"												// PREZZO DI ACQUISTO
		if flag_sconto_a_parte = "S" and flag_sconto_mag_prezzo = "S" then
			fd_sconto = fd_variazione
			fd_prezzo = ld_prezzo_acquisto
		else
			wf_calcola_variazione(flag_sconto_mag_prezzo, fd_variazione, ld_prezzo_acquisto, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
			if flag_sconto_mag_prezzo = "M" then fd_maggiorazione = fd_variazione
		end if
	case "S"												// COSTO STANDARD
		if flag_sconto_a_parte = "S" and flag_sconto_mag_prezzo = "S" then
			fd_sconto = fd_variazione
			fd_prezzo = ld_costo_standard
		else
			wf_calcola_variazione(flag_sconto_mag_prezzo, fd_variazione, ld_costo_standard, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
			if flag_sconto_mag_prezzo = "M" then fd_maggiorazione = fd_variazione
		end if
		
	case "P"												// DA DISTINTA BASE
		
		if s_cs_xx_listino_db_flag_calcola = "S" then
			uo_conf_varianti luo_funzioni_1
			s_trova_mp_varianti lstr_trova_mp_varianti[]
			luo_funzioni_1 = create uo_conf_varianti
			
			luo_funzioni_1.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
			luo_funzioni_1.s_cs_xx_db_funzioni_formato_data = s_cs_xx_db_funzioni_formato_data
			
			guo_functions.uof_log("Wf_componi_prezzo(). uof_trova_mat_prime_varianti() START")
			
			guo_functions.uof_log("Wf_componi_prezzo().Prodotto Finito: " +guo_functions.uof_format(is_cod_prodotto_origine) )
			guo_functions.uof_log("Wf_componi_prezzo().Versione Listino: " +guo_functions.uof_format(s_cs_xx_listino_db_cod_versione) )
			guo_functions.uof_log("Wf_componi_prezzo().Tipo Gestione: " +guo_functions.uof_format(s_cs_xx_listino_db_tipo_gestione) )
			guo_functions.uof_log("Wf_componi_prezzo().Anno Documento: " +guo_functions.uof_format(s_cs_xx_listino_db_anno_documento) )
			guo_functions.uof_log("Wf_componi_prezzo().Num Documento: " +guo_functions.uof_format(s_cs_xx_listino_db_num_documento) )
			guo_functions.uof_log("Wf_componi_prezzo().Riga Doc: " +guo_functions.uof_format(s_cs_xx_listino_db_prog_riga) )
			guo_functions.uof_log("Wf_componi_prezzo().Quan Finito: " +guo_functions.uof_format(s_cs_xx_listino_db_quan_prodotto_finito) )

			if luo_funzioni_1.uof_trova_mat_prime_varianti(	is_cod_prodotto_origine, &
																			s_cs_xx_listino_db_cod_versione, &
																			0, &
																			s_cs_xx_listino_db_tipo_gestione, &
																			s_cs_xx_listino_db_anno_documento, &
																			s_cs_xx_listino_db_num_documento, &
																			s_cs_xx_listino_db_prog_riga, &
																			"quan_utilizzo",&
																			s_cs_xx_listino_db_quan_prodotto_finito, &
																			lstr_trova_mp_varianti[], &
																			ls_messaggio) < 0 then
				guo_functions.uof_log("Wf_componi_prezzo(). uof_trova_mat_prime_varianti(). Errore estrazione varianti per quan_utilizzo. (uof_trova_mat_prime_varianti)" + guo_functions.uof_format(ls_messaggio))
			end if																
												 
			guo_functions.uof_log("Wf_componi_prezzo(). uof_trova_mat_prime_varianti() END")

			ld_prezzo = fd_variazione
			ld_prezzo_decimal = dec(fd_variazione)
			
			ll_cont = upperbound(lstr_trova_mp_varianti[])
			
			guo_functions.uof_log("Wf_componi_prezzo(). Trovato " +guo_functions.uof_format(ll_cont) + " Materie Prime")
			
			destroy luo_funzioni_1
			
			wf_ricerca_condizioni_distinta (s_cs_xx_listino_db_cod_tipo_listino_prodotto, &
													 s_cs_xx_listino_db_cod_valuta, &
													 s_cs_xx_listino_db_data_inizio_val, &
													 is_cod_prodotto_origine, &
													 s_cs_xx_listino_db_cod_versione, &
													 s_cs_xx_listino_db_progressivo, &
													 lstr_trova_mp_varianti[], &
													 fb_provenienza_condizione_standard, &
													 fs_cod_cliente, &
													 ld_prezzo_decimal, &
													 ls_messaggio )

			fd_sconto = 0
			fd_maggiorazione = 0
			fd_prezzo = double(ld_prezzo_decimal)
			
			guo_functions.uof_log("Wf_componi_prezzo(). Valore Componente" +guo_functions.uof_format(fd_prezzo) )

		else
			fd_sconto = 0
			fd_maggiorazione = 0
			fd_prezzo = 0
		end if			
	case "U"												// COSTO ULTIMO
		if flag_sconto_a_parte = "S" and flag_sconto_mag_prezzo = "S" then
			fd_sconto = fd_variazione
			fd_prezzo = ld_costo_ultimo
		else
			wf_calcola_variazione(flag_sconto_mag_prezzo, fd_variazione, ld_costo_ultimo, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
			if flag_sconto_mag_prezzo = "M" then fd_maggiorazione = fd_variazione
		end if
	case "V"												// PREZZO DI VENDITA DI MAGAZZINO
		if flag_sconto_a_parte = "S" and flag_sconto_mag_prezzo = "S" then
			fd_sconto = fd_variazione
			fd_prezzo = ld_prezzo_vendita
		else
			wf_calcola_variazione(flag_sconto_mag_prezzo, fd_variazione, ld_prezzo_vendita, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
			if flag_sconto_mag_prezzo = "M" then fd_maggiorazione = fd_variazione
		end if
	case "N"   // variazione rispetto alla precedente condizione ( se esiste altrimenti ciccia!! )
		if flag_sconto_a_parte = "S" and flag_sconto_mag_prezzo = "S" then
			fd_sconto = fd_variazione
			fd_prezzo = fd_prezzo_vendita_precedente
		else
			wf_calcola_variazione(flag_sconto_mag_prezzo, fd_variazione, fd_prezzo_vendita_precedente, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
			if flag_sconto_mag_prezzo = "M" then fd_maggiorazione = fd_variazione
		end if

	case "L"   // variazione rispetto al listino standard del prodotto ( vado alla ricerca delle condizioni standard
		        // del prodotto nella stessa tabella.
		if flag_sconto_a_parte = "S" and flag_sconto_mag_prezzo = "S" then
			fd_sconto = fd_variazione
//			fd_prezzo =  prezzo_di_listino_standard
		else
			wf_calcola_variazione(flag_sconto_mag_prezzo, fd_variazione, fd_prezzo_vendita_precedente, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
			if flag_sconto_mag_prezzo = "M" then fd_maggiorazione = fd_variazione
		end if
		
		
		
end choose
return 0

end function

public function integer wf_ricerca_condizioni_prodotto (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_riferimento_val, string fs_cod_prodotto, double fd_dimensione_1, double fd_dimensione_2, double fd_quan_scaglione, double fd_val_scaglione, boolean fb_provenienza_condizione_standard, string fs_cod_cliente, ref double fd_sconto[], ref double fd_maggiorazione[], ref double fd_variazione[], boolean fb_cerca_dimensioni);long 	 ll_num_righe, ll_cont, ll_livello_criterio, ll_i,  &
     		 ll_cont_sconto, ll_cont_maggiorazione, ll_cont_variazione, ll_progressivo, &
	  	 ll_scaglione_1,ll_scaglione_2, ll_scaglione_3, ll_scaglione_4, ll_scaglione_5
double ld_variazione_precedente, ld_prezzo_vendita_precedente, ld_prezzo, &
       	 ld_minimo_fatt_altezza,ld_minimo_fatt_larghezza,ld_minimo_fatt_profondita, &
		 ld_minimo_fatt_superficie,ld_minimo_fatt_volume, ld_var, &
		 ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5, &
		 ll_sconto ,ll_maggiorazione
string 	 ls_cod_cat_mer, ls_cod_categoria, ls_filtro, ls_messaggio, ls_sql, &
       	 ls_cod_cliente,ls_des_listino_vendite, ls_cod_prodotto, &
		 ls_flag_sconto_a_parte,ls_flag_tipo_scaglioni,ls_flag_sconto_mag_prezzo_1, ls_flag, &
		 ls_flag_sconto_mag_prezzo_2, ls_flag_sconto_mag_prezzo_3, ls_flag_sconto_mag_prezzo_4, &
		 ls_flag_sconto_mag_prezzo_5, ls_flag_origine_prezzo_1,ls_flag_origine_prezzo_2, &
		 ls_flag_origine_prezzo_3, ls_flag_origine_prezzo_4, ls_flag_origine_prezzo_5
datetime ldt_data_inizio_val, ldt_data_inizio_val_precedente

str_listini_ven_dim lstr_listini_ven_dim


lstr_listini_ven_dim.dim_1 = fd_dimensione_1
lstr_listini_ven_dim.dim_2 = fd_dimensione_2

// ------------- valuto esistenza condizioni prodotto ------------------------------------------------

select count(*)
into  :ll_cont
from  listini_vendite
where cod_azienda      = :s_cs_xx_cod_azienda and
		cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
		cod_valuta       = :fs_cod_valuta and
		data_inizio_val <= :fdt_data_riferimento_val and
		cod_cliente      is null and
		cod_cat_mer      is null and 
		cod_prodotto     = :fs_cod_prodotto and
		cod_categoria    is null;
choose case sqlca.sqlcode
	case 0
		if ll_cont > 0 then
			goto ricerca
		else			
        return 0			// non esistono condizioni: successivo criterio
		end if
	case 100
		return 0          // non esistono condizioni: successivo criterio
	case else
		return 0          // errore
end choose

// ------------------------------- ricerca condizioni -----------------------------------
ricerca:

declare cu_condizioni_prodotto dynamic cursor for sqlsa;
ls_sql = "SELECT data_inizio_val, progressivo, cod_cat_mer, " + &
                "cod_prodotto, cod_categoria, cod_cliente, des_listino_vendite, minimo_fatt_altezza, minimo_fatt_larghezza, " + &
		  "minimo_fatt_profondita, minimo_fatt_superficie, minimo_fatt_volume, flag_sconto_a_parte, flag_tipo_scaglioni, " + &
		  "scaglione_1, scaglione_2, scaglione_3, scaglione_4, scaglione_5, flag_sconto_mag_prezzo_1, flag_sconto_mag_prezzo_2, " + &
		  "flag_sconto_mag_prezzo_3, flag_sconto_mag_prezzo_4, flag_sconto_mag_prezzo_5, variazione_1, variazione_2, " + &
		  "variazione_3, variazione_4, variazione_5, flag_origine_prezzo_1, flag_origine_prezzo_2, flag_origine_prezzo_3, " + &
		  "flag_origine_prezzo_4, flag_origine_prezzo_5  " + &
         	  "FROM   listini_vendite " + &
		 "WHERE  cod_azienda = '" + s_cs_xx_cod_azienda + "' and " + &
		 " cod_tipo_listino_prodotto = '" + fs_cod_tipo_listino_prodotto + "' and " + &
               " cod_valuta = '" + fs_cod_valuta + "' and  " + &
               " data_inizio_val <= '" + string(fdt_data_riferimento_val, s_cs_xx_db_funzioni_formato_data) + "' and " + &
               " cod_prodotto = '" + fs_cod_prodotto + "' and " + &
               " cod_cat_mer is null and  " + &
               " cod_categoria is null and "  + &
               " cod_cliente is null    " + &
         	 "order by data_inizio_val DESC,   " + &
               " progressivo ASC   "

prepare sqlsa from :ls_sql;

open dynamic cu_condizioni_prodotto;


ld_variazione_precedente = 0
ld_prezzo_vendita_precedente = 0
ll_cont_sconto = 0
ll_cont_maggiorazione = 0
ll_cont_variazione = 0
ldt_data_inizio_val_precedente = datetime(date("01/01/1900"), 00:00:00)
ll_i = 0

do while 1=1
	fetch cu_condizioni_prodotto into :ldt_data_inizio_val, :ll_progressivo, :ls_cod_cat_mer, :ls_cod_prodotto, :ls_cod_categoria, :ls_cod_cliente, :ls_des_listino_vendite, :ld_minimo_fatt_altezza, :ld_minimo_fatt_larghezza, :ld_minimo_fatt_profondita, :ld_minimo_fatt_superficie, :ld_minimo_fatt_volume, :ls_flag_sconto_a_parte, :ls_flag_tipo_scaglioni, :ll_scaglione_1, :ll_scaglione_2, :ll_scaglione_3, :ll_scaglione_4, :ll_scaglione_5, :ls_flag_sconto_mag_prezzo_1, :ls_flag_sconto_mag_prezzo_2, :ls_flag_sconto_mag_prezzo_3, :ls_flag_sconto_mag_prezzo_4, :ls_flag_sconto_mag_prezzo_5, :ld_variazione_1, :ld_variazione_2, :ld_variazione_3, :ld_variazione_4, :ld_variazione_5, :ls_flag_origine_prezzo_1, :ls_flag_origine_prezzo_2, :ls_flag_origine_prezzo_3, :ls_flag_origine_prezzo_4, :ls_flag_origine_prezzo_5;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	ll_i ++
	if ll_i > 1 then
		if ldt_data_inizio_val_precedente <> ldt_data_inizio_val then exit
	end if	

	if ls_flag_tipo_scaglioni = "Q" then
		choose case fd_quan_scaglione
			case is <= ll_scaglione_1
				if wf_ricerca_cond_dimensioni(fs_cod_tipo_listino_prodotto, &
				                          fs_cod_valuta, &
												  ldt_data_inizio_val, &
												  ll_progressivo, &
												  1, &
												  lstr_listini_ven_dim, &
												  ls_flag, &
												  ld_var) = 0 then
					ls_flag_sconto_mag_prezzo_1 = ls_flag
					ld_variazione_1 = ld_var
				end if
				s_cs_xx_listino_db_data_inizio_val = ldt_data_inizio_val
				s_cs_xx_listino_db_progressivo = ll_progressivo
				wf_componi_prezzo ( fs_cod_prodotto, &
										 ls_flag_sconto_mag_prezzo_1, &
										 ld_variazione_1, &
										 ld_variazione_precedente, &
										 ld_prezzo_vendita_precedente, &
										 ls_flag_origine_prezzo_1, &
										 ls_flag_sconto_a_parte, &
										 fb_provenienza_condizione_standard, &
										 fs_cod_cliente, &
										 ll_sconto, &
										 ll_maggiorazione, &
										 ld_prezzo, &
										 ls_messaggio )
				ld_variazione_precedente = ld_variazione_1
				ld_prezzo_vendita_precedente = ld_prezzo
				if ll_sconto > 0 and not isnull(ll_sconto) then
					ll_cont_sconto = ll_cont_sconto + 1
					fd_sconto[ll_cont_sconto] = ll_sconto
					wf_listino_log("Applicato Sconto " + f_decimal_to_string(ll_sconto) + "%")
				end if
				if ll_maggiorazione > 0 and not isnull(ll_maggiorazione) then
					ll_cont_maggiorazione = ll_cont_maggiorazione + 1
					fd_maggiorazione[ll_cont_maggiorazione] = ll_maggiorazione
					wf_listino_log("Applicato Maggiorazione " + f_decimal_to_string(ll_sconto) + "%")
				end if
				if ld_prezzo > 0 and not isnull(ld_prezzo) then
					ll_cont_variazione = ll_cont_variazione + 1
					fd_variazione[ll_cont_variazione] = ld_prezzo
					wf_listino_log("Applicato Prezzo " + f_decimal_to_string(ld_prezzo))
				end if
				
			case is <= ll_scaglione_2
				if wf_ricerca_cond_dimensioni(fs_cod_tipo_listino_prodotto, &
				                          fs_cod_valuta, &
												  ldt_data_inizio_val, &
												  ll_progressivo, &
												  2, &
												  lstr_listini_ven_dim, &
												  ls_flag, &
												  ld_var) = 0 then
					ls_flag_sconto_mag_prezzo_1 = ls_flag
					ld_variazione_1 = ld_var
				end if
				s_cs_xx_listino_db_data_inizio_val = ldt_data_inizio_val
				s_cs_xx_listino_db_progressivo = ll_progressivo
				wf_componi_prezzo ( fs_cod_prodotto, &
										 ls_flag_sconto_mag_prezzo_2, &
										 ld_variazione_2, &
										 ld_variazione_precedente, &
										 ld_prezzo_vendita_precedente, &
										 ls_flag_origine_prezzo_2, &
										 ls_flag_sconto_a_parte, &
										 fb_provenienza_condizione_standard, &
										 fs_cod_cliente, &
										 ll_sconto, &
										 ll_maggiorazione, &
										 ld_prezzo, &
										 ls_messaggio )
				ld_variazione_precedente = ld_variazione_2
				ld_prezzo_vendita_precedente = ld_prezzo
				if ll_sconto > 0 and not isnull(ll_sconto) then
					ll_cont_sconto = ll_cont_sconto + 1
					fd_sconto[ll_cont_sconto] = ll_sconto
					wf_listino_log("Applicato Sconto " + f_decimal_to_string(ll_sconto) + "%")
				end if
				if ll_maggiorazione > 0 and not isnull(ll_maggiorazione) then
					ll_cont_maggiorazione = ll_cont_maggiorazione + 1
					fd_maggiorazione[ll_cont_maggiorazione] = ll_maggiorazione
					wf_listino_log("Applicato Maggiorazione " + f_decimal_to_string(ll_sconto) + "%")
				end if
				if ld_prezzo > 0 and not isnull(ld_prezzo) then
					ll_cont_variazione = ll_cont_variazione + 1
					fd_variazione[ll_cont_variazione] = ld_prezzo
					wf_listino_log("Applicato Prezzo " + f_decimal_to_string(ld_prezzo))
				end if
			case is <= ll_scaglione_3
				if wf_ricerca_cond_dimensioni(fs_cod_tipo_listino_prodotto, &
				                          fs_cod_valuta, &
												  ldt_data_inizio_val, &
												  ll_progressivo, &
												  3, &
												  lstr_listini_ven_dim, &
												  ls_flag, &
												  ld_var) = 0 then
					ls_flag_sconto_mag_prezzo_1 = ls_flag
					ld_variazione_1 = ld_var
				end if
				s_cs_xx_listino_db_data_inizio_val = ldt_data_inizio_val
				s_cs_xx_listino_db_progressivo = ll_progressivo
				wf_componi_prezzo ( fs_cod_prodotto, &
										 ls_flag_sconto_mag_prezzo_3, &
										 ld_variazione_3, &
										 ld_variazione_precedente, &
										 ld_prezzo_vendita_precedente, &
										 ls_flag_origine_prezzo_3, &
										 ls_flag_sconto_a_parte, &
										 fb_provenienza_condizione_standard, &
										 fs_cod_cliente, &
										 ll_sconto, &
										 ll_maggiorazione, &
										 ld_prezzo, &
										 ls_messaggio )
				ld_variazione_precedente = ld_variazione_3
				ld_prezzo_vendita_precedente = ld_prezzo
				if ll_sconto > 0 and not isnull(ll_sconto) then
					ll_cont_sconto = ll_cont_sconto + 1
					fd_sconto[ll_cont_sconto] = ll_sconto
					wf_listino_log("Applicato Sconto " + f_decimal_to_string(ll_sconto) + "%")
				end if
				if ll_maggiorazione > 0 and not isnull(ll_maggiorazione) then
					ll_cont_maggiorazione = ll_cont_maggiorazione + 1
					fd_maggiorazione[ll_cont_maggiorazione] = ll_maggiorazione
					wf_listino_log("Applicato Maggiorazione " + f_decimal_to_string(ll_sconto) + "%")
				end if
				if ld_prezzo > 0 and not isnull(ld_prezzo) then
					ll_cont_variazione = ll_cont_variazione + 1
					fd_variazione[ll_cont_variazione] = ld_prezzo
					wf_listino_log("Applicato Prezzo " + f_decimal_to_string(ld_prezzo))
				end if
			case is <= ll_scaglione_4
				if wf_ricerca_cond_dimensioni(fs_cod_tipo_listino_prodotto, &
				                          fs_cod_valuta, &
												  ldt_data_inizio_val, &
												  ll_progressivo, &
												  4, &
												  lstr_listini_ven_dim, &
												  ls_flag, &
												  ld_var) = 0 then
					ls_flag_sconto_mag_prezzo_1 = ls_flag
					ld_variazione_1 = ld_var
				end if
				s_cs_xx_listino_db_data_inizio_val = ldt_data_inizio_val
				s_cs_xx_listino_db_progressivo = ll_progressivo
				wf_componi_prezzo ( fs_cod_prodotto, &
										 ls_flag_sconto_mag_prezzo_4, &
										 ld_variazione_4, &
										 ld_variazione_precedente, &
										 ld_prezzo_vendita_precedente, &
										 ls_flag_origine_prezzo_4, &
										 ls_flag_sconto_a_parte, &
										 fb_provenienza_condizione_standard, &
										 fs_cod_cliente, &
										 ll_sconto, &
										 ll_maggiorazione, &
										 ld_prezzo, &
										 ls_messaggio )
				ld_variazione_precedente = ld_variazione_4
				ld_prezzo_vendita_precedente = ld_prezzo
				if ll_sconto > 0 and not isnull(ll_sconto) then
					ll_cont_sconto = ll_cont_sconto + 1
					fd_sconto[ll_cont_sconto] = ll_sconto
					wf_listino_log("Applicato Sconto " + f_decimal_to_string(ll_sconto) + "%")
				end if
				if ll_maggiorazione > 0 and not isnull(ll_maggiorazione) then
					ll_cont_maggiorazione = ll_cont_maggiorazione + 1
					fd_maggiorazione[ll_cont_maggiorazione] = ll_maggiorazione
					wf_listino_log("Applicato Maggiorazione " + f_decimal_to_string(ll_sconto) + "%")
				end if
				if ld_prezzo > 0 and not isnull(ld_prezzo) then
					ll_cont_variazione = ll_cont_variazione + 1
					fd_variazione[ll_cont_variazione] = ld_prezzo
					wf_listino_log("Applicato Prezzo " + f_decimal_to_string(ld_prezzo))
				end if
				
			case else
				if wf_ricerca_cond_dimensioni(fs_cod_tipo_listino_prodotto, &
				                          fs_cod_valuta, &
												  ldt_data_inizio_val, &
												  ll_progressivo, &
												  5, &
												  lstr_listini_ven_dim, &
												  ls_flag, &
												  ld_var) = 0 then
					ls_flag_sconto_mag_prezzo_1 = ls_flag
					ld_variazione_1 = ld_var
				end if
				s_cs_xx_listino_db_data_inizio_val = ldt_data_inizio_val
				s_cs_xx_listino_db_progressivo = ll_progressivo
				wf_componi_prezzo ( fs_cod_prodotto, &
										 ls_flag_sconto_mag_prezzo_5, &
										 ld_variazione_5, &
										 ld_variazione_precedente, &
										 ld_prezzo_vendita_precedente, &
										 ls_flag_origine_prezzo_5, &
										 ls_flag_sconto_a_parte, &
										 fb_provenienza_condizione_standard, &
										 fs_cod_cliente, &
										 ll_sconto, &
										 ll_maggiorazione, &
										 ld_prezzo, &
										 ls_messaggio )
				ld_variazione_precedente = ld_variazione_5
				ld_prezzo_vendita_precedente = ld_prezzo
				if ll_sconto > 0 and not isnull(ll_sconto) then
					ll_cont_sconto = ll_cont_sconto + 1
					fd_sconto[ll_cont_sconto] = ll_sconto
					wf_listino_log("Applicato Sconto " + f_decimal_to_string(ll_sconto) + "%")
				end if
				if ll_maggiorazione > 0 and not isnull(ll_maggiorazione) then
					ll_cont_maggiorazione = ll_cont_maggiorazione + 1
					fd_maggiorazione[ll_cont_maggiorazione] = ll_maggiorazione
					wf_listino_log("Applicato Maggiorazione " + f_decimal_to_string(ll_sconto) + "%")
				end if
				if ld_prezzo > 0 and not isnull(ld_prezzo) then
					ll_cont_variazione = ll_cont_variazione + 1
					fd_variazione[ll_cont_variazione] = ld_prezzo
					wf_listino_log("Applicato Prezzo " + f_decimal_to_string(ld_prezzo))
				end if
		end choose	
	else
		// non previsto scaglione per valore
		
	end if
loop
close cu_condizioni_prodotto;
return 0

end function

public function integer wf_ricerca_condizioni_distinta (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, string fs_cod_prodotto_listino, string fs_cod_versione, long fl_progressivo, s_trova_mp_varianti fs_trova_mp_varianti[], boolean fb_provenienza_condizione_standard, string fs_cod_cliente, ref decimal fd_valore_pf, ref string fs_messaggio);// --------------------------------------------------------------------------------------------------------------------
//
//       Funzione di calcolo prezzo in base ai componenti presenti nella distinta
//       Return = 0     OK prezzo trovato
//       Return = -1    Si è veriricato un errore controllare variabile fs_messaggio
//       Return = 1     Si è veririficato un errore logico ( ad es. non ci sono materie prime nella distinta
//
//			EnMe 16/02/1998
//
//			EnMe 28/08/2007 Modifcato per Viropa.
//
//			EnMe 09/05/2012 Modifcato per Gibus per fare il controllo sul ramo padre-figlio
//
// ---------------------------------------------------------------------------------------------------------------------

boolean lb_listino_prod_comune_var_client=false
string ls_filtro, ls_messaggio, ls_sql, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_flag_sconto_mag_prezzo, &
     ls_flag_origine_prezzo, ls_flag_sconto_a_parte, ls_flag_tipo_scaglioni, ls_des_listino_produzione, &
	  ls_cod_materia_prima, ls_flag_sconto_mag_prezzo_1, ls_flag_sconto_mag_prezzo_2, ls_flag_sconto_mag_prezzo_3, &
	  ls_flag_sconto_mag_prezzo_4, ls_flag_sconto_mag_prezzo_5, ls_flag_origine_prezzo_1, ls_flag_origine_prezzo_2,&
	  ls_flag_origine_prezzo_3, ls_flag_origine_prezzo_4, ls_flag_origine_prezzo_5, ls_filter,ls_flag_prezzo_addizionale_separato, &
	  ls_flag_addizionale, ls_flag_escludi_linee_sconto, ls_flag_escludi_linee_sconto_buffer, ls_cod_padre_mp, ls_flag_ricerca_versione_diba, &
	  ls_des_variazione
long ll_num_righe, ll_cont, ll_livello_criterio, ll_i, ll_num_sequenza, ll_cont_sconto,&
     ll_cont_maggiorazione, ll_cont_variazione, ll_1, ll_progressivo, ll_scaglione_1, ll_scaglione_2, ll_scaglione_3,&
	  ll_scaglione_4, ll_scaglione_5, ll_mp, ll_riga, ll_righe, ll_z, ll_count, ll_count_addizionale, ll_y
dec{4} ld_variazione_precedente, ld_prezzo_vendita_precedente, ld_prezzo, ld_variazione, ld_quan_utilizzo, &
	    ld_minimo_fatt_altezza, ld_minimo_fatt_larghezza, ld_minimo_fatt_profondita, ld_minimo_fatt_superficie, &
		 ld_minimo_fatt_volume, ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5, &
		 ld_valore_variazioni, ld_sconto ,ld_maggiorazione, ld_provvigione_1, ld_provvigione_2
datetime ldt_data_inizio_val, ldt_data_inizio_val_precedente
datastore d_listini_diba



// parametro aggiunto per Viropa che vuole che il valore aggiuntivo delle addizionali
// sia indicato separatamente.
ls_flag_prezzo_addizionale_separato = "N"

select flag
into   :ls_flag_prezzo_addizionale_separato
from	 parametri_azienda
where  cod_azienda = :s_cs_xx_cod_azienda and
		 cod_parametro = 'PAS';
if sqlca.sqlcode <> 0 then
	ls_flag_prezzo_addizionale_separato = "N"
end if

if isnull(ls_flag_prezzo_addizionale_separato) then ls_flag_prezzo_addizionale_separato = "N"

select flag_ricerca_versione_diba
into :ls_flag_ricerca_versione_diba
from con_vendite
where cod_azienda = :s_cs_xx_cod_azienda;

// inizio routine standard
d_listini_diba = CREATE datastore
d_listini_diba.DataObject = 'd_ext_listini_diba'

guo_functions.uof_log("wf_ricerca_condizioni_distinta. Dopo Create Datastore")

ld_valore_variazioni = 0
ll_cont = upperbound(fs_trova_mp_varianti[])
if ll_cont = 0 or isnull(ll_cont) then 
	fs_messaggio = "Nessun prodotto presente nella distinta"
	return 1
end if
declare cu_condizioni_distinta dynamic cursor for sqlsa;

guo_functions.uof_log("wf_ricerca_condizioni_distinta. Prima del CICLO FOR dei prodotti")

guo_functions.uof_log("wf_ricerca_condizioni_distinta; dati fissi. AZIENDA="+guo_functions.uof_format(s_cs_xx_cod_azienda) )
guo_functions.uof_log("wf_ricerca_condizioni_distinta; dati fissi.TIPO LISTINO="+guo_functions.uof_format(fs_cod_tipo_listino_prodotto) )
guo_functions.uof_log("wf_ricerca_condizioni_distinta; dati fissi. VALUTA="+guo_functions.uof_format(fs_cod_valuta) )
guo_functions.uof_log("wf_ricerca_condizioni_distinta; dati fissi. DATA INIZIO VAL="+guo_functions.uof_format(fdt_data_inizio_val) )
guo_functions.uof_log("wf_ricerca_condizioni_distinta; dati fissi. PROGRESSIVO="+guo_functions.uof_format(fl_progressivo) )
guo_functions.uof_log("wf_ricerca_condizioni_distinta; dati fissi. PRODOTTO LISTINO="+guo_functions.uof_format(fs_cod_prodotto_listino) )
guo_functions.uof_log("wf_ricerca_condizioni_distinta; dati fissi. CLIENTE="+guo_functions.uof_format(fs_cod_cliente) )

for ll_mp = 1 to ll_cont
	ls_cod_materia_prima 	= fs_trova_mp_varianti[ll_mp].cod_materia_prima
	ld_quan_utilizzo 			= fs_trova_mp_varianti[ll_mp].quan_utilizzo
	ls_cod_padre_mp 			= fs_trova_mp_varianti[ll_mp].cod_prodotto_padre
	
	ll_count = 0
	lb_listino_prod_comune_var_client = false
	
	if fb_provenienza_condizione_standard then
	
		// verifico se c'è una condizione specificamente caricata sul cliente
		// EnMe: tolto il progressivo 23/02/2021
		//					progressivo          = :fl_progressivo and

		
		select 	count(*)
		into   		:ll_count
		from   	listini_prod_comune_var_client
		where  	cod_azienda          = :s_cs_xx_cod_azienda and
					cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
					cod_valuta           = :fs_cod_valuta and
					data_inizio_val     = :fdt_data_inizio_val and
					cod_prodotto_listino = :fs_cod_prodotto_listino  and
					cod_prodotto_padre  = :ls_cod_padre_mp  and
					cod_prodotto_figlio  = :ls_cod_materia_prima and
					cod_cliente 			= :fs_cod_cliente ;
					
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore in count() listini produzione nella funzione f_ricerca_condizioni_distinta. Dettaglio " + sqlca.sqlerrtext
			return -1
		end if

		guo_functions.uof_log("wf_ricerca_condizioni_distinta (listini_prod_comune_var_client). Padre="+guo_functions.uof_format(ls_cod_padre_mp)+" Figlio MP="+guo_functions.uof_format(ls_cod_materia_prima)+"COUNT= " + guo_functions.uof_format(ll_count) )
		
		
		if ll_count < 1 or isnull(ll_cont) then
			// verifico se esiste una condizione generale nello standard
			// EnMe: tolto il progressivo 23/02/2021
			//					progressivo          = :fl_progressivo and
			
			select 	count(*)
			into   		:ll_count
			from   	listini_produzione
			where  	cod_azienda          = :s_cs_xx_cod_azienda and
						cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
						cod_valuta           = :fs_cod_valuta and
						data_inizio_val     = :fdt_data_inizio_val and
						cod_prodotto_listino = :fs_cod_prodotto_listino  and
						cod_prodotto_padre = :ls_cod_padre_mp  and
						cod_prodotto_figlio  = :ls_cod_materia_prima;
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore in count() listini produzione nella funzione f_ricerca_condizioni_distinta. Dettaglio " + sqlca.sqlerrtext
				return -1
			end if
			
			guo_functions.uof_log("wf_ricerca_condizioni_distinta (listini_produzione1). Padre="+guo_functions.uof_format(ls_cod_padre_mp)+" Figlio MP="+guo_functions.uof_format(ls_cod_materia_prima)+"COUNT= " + guo_functions.uof_format(ll_count) )
			
		else
			lb_listino_prod_comune_var_client = true
		end if
		
		
	else
		// EnMe: tolto il progressivo 23/02/2021
		//					progressivo          = :fl_progressivo and
		
		select 	count(*)
		into   		:ll_count
		from   	listini_produzione
		where  	cod_azienda          = :s_cs_xx_cod_azienda and
					cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
					cod_valuta           = :fs_cod_valuta and
					data_inizio_val     = :fdt_data_inizio_val and
					cod_prodotto_listino = :fs_cod_prodotto_listino  and
					cod_prodotto_padre = :ls_cod_padre_mp  and
					cod_prodotto_figlio  = :ls_cod_materia_prima;
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore in count() listini produzione nella funzione f_ricerca_condizioni_distinta. Dettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
		guo_functions.uof_log("wf_ricerca_condizioni_distinta (listini_produzione2). Padre="+guo_functions.uof_format(ls_cod_padre_mp)+" Figlio MP="+guo_functions.uof_format(ls_cod_materia_prima)+"COUNT= " + guo_functions.uof_format(ll_count) )
	end if
	
	
	
	if ll_count > 0 then
		
		if lb_listino_prod_comune_var_client then
//						EnMe 23/02/2021 tolto il progressivo
//						"  ( progressivo = " + string(fl_progressivo) + ") AND " + &
				
			
				ls_sql ="  SELECT data_inizio_val, progressivo, " + &
						"  cod_prodotto_padre, num_sequenza, cod_prodotto_figlio, des_listino_produzione, minimo_fatt_altezza, " + &
						"  minimo_fatt_larghezza, minimo_fatt_profondita, minimo_fatt_superficie, minimo_fatt_volume, flag_sconto_a_parte, " + &
						"  flag_tipo_scaglioni, scaglione_1, scaglione_2, scaglione_3, scaglione_4, scaglione_5, " + &
						"  flag_sconto_mag_prezzo_1, flag_sconto_mag_prezzo_2, flag_sconto_mag_prezzo_3, flag_sconto_mag_prezzo_4, flag_sconto_mag_prezzo_5, " + &
						"  variazione_1, variazione_2, variazione_3, variazione_4, variazione_5, " + &
						"  flag_origine_prezzo_1, flag_origine_prezzo_2, flag_origine_prezzo_3, flag_origine_prezzo_4, flag_origine_prezzo_5, provvigione_1, provvigione_2 , flag_escludi_linee_sconto " + &
						"  FROM listini_prod_comune_var_client  " + &
						"  WHERE ( cod_azienda = '" + s_cs_xx_cod_azienda + "' ) AND  " + &
						"  ( cod_tipo_listino_prodotto = '" + fs_cod_tipo_listino_prodotto + "') AND  " + &
						"  ( cod_valuta = '" + fs_cod_valuta + "') AND   " + &
						"  ( data_inizio_val = '" + guo_functions.uof_data_db(fdt_data_inizio_val) + "') AND  " + &
						"  ( cod_prodotto_listino = '" +  fs_cod_prodotto_listino  + "') AND " + &
						"  ( cod_prodotto_padre = '" + ls_cod_padre_mp + "') AND " + &
						"  ( cod_prodotto_figlio = '" + ls_cod_materia_prima + "') AND " + &
						"  (cod_cliente = '" + fs_cod_cliente + "' )  " + &
						"  ORDER BY cod_azienda ASC,   " + &  
						"  cod_tipo_listino_prodotto ASC,   " + &  
						"  cod_valuta ASC, " + &
						"  data_inizio_val DESC, " + &
						"  progressivo ASC, "  + &
						"  cod_prodotto_listino ASC, "  + &
						"  cod_versione ASC, "  + &
						"  prog_listino_produzione ASC "

						
		else
//						EnMe 23/02/2021 tolto il progressivo
//						"  ( progressivo = " + string(fl_progressivo) + ") AND " + &
			
			ls_sql = "  SELECT data_inizio_val, progressivo, " + &
						"  cod_prodotto_padre, num_sequenza, cod_prodotto_figlio, des_listino_produzione, minimo_fatt_altezza, " + &
						"  minimo_fatt_larghezza, minimo_fatt_profondita, minimo_fatt_superficie, minimo_fatt_volume, flag_sconto_a_parte, " + &
						"  flag_tipo_scaglioni, scaglione_1, scaglione_2, scaglione_3, scaglione_4, scaglione_5, " + &
						"  flag_sconto_mag_prezzo_1, flag_sconto_mag_prezzo_2, flag_sconto_mag_prezzo_3, flag_sconto_mag_prezzo_4, flag_sconto_mag_prezzo_5, " + &
						"  variazione_1, variazione_2, variazione_3, variazione_4, variazione_5, " + &
						"  flag_origine_prezzo_1, flag_origine_prezzo_2, flag_origine_prezzo_3, flag_origine_prezzo_4, flag_origine_prezzo_5, provvigione_1, provvigione_2, flag_escludi_linee_sconto " + &
						"  FROM listini_produzione  " + &
						"  WHERE ( cod_azienda = '" + s_cs_xx_cod_azienda + "' ) AND  " + &
						"  ( cod_tipo_listino_prodotto = '" + fs_cod_tipo_listino_prodotto + "') AND  " + &
						"  ( cod_valuta = '" + fs_cod_valuta + "') AND   " + &
						"  ( data_inizio_val <= '" + guo_functions.uof_data_db(fdt_data_inizio_val)  + "') AND  " + &
						"  ( cod_prodotto_listino = '" +  fs_cod_prodotto_listino  + "') AND " + &
						"  ( cod_prodotto_padre = '" + ls_cod_padre_mp + "') AND " + &
						"  ( cod_prodotto_figlio = '" + ls_cod_materia_prima + "') " + &
						"  ORDER BY cod_azienda ASC,   " + &  
						"  cod_tipo_listino_prodotto ASC,   " + &  
						"  cod_valuta ASC, " + &
						"  data_inizio_val DESC, " + &
						"  progressivo ASC, "  + &
						"  cod_prodotto_listino ASC, "  + &
						"  cod_versione ASC, "  + &
						"  prog_listino_produzione ASC "
						
		end if
		
		
		guo_functions.uof_log("wf_ricerca_condizioni_distinta. Prepare SQL = " + guo_functions.uof_format(ls_sql))

		prepare sqlsa from :ls_sql;
		
		open dynamic cu_condizioni_distinta;
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore in open cursore su listini produzione nella funzione f_ricerca_condizioni_distinta. Dettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
		ld_variazione_precedente = 0
		ll_cont_sconto = 0
		ll_cont_maggiorazione = 0
		ll_cont_variazione = 0
		ldt_data_inizio_val_precedente = datetime(date("01/01/1900"), 00:00:00)
		ll_i = 0
		ll_riga = 0
		ls_flag_escludi_linee_sconto = "N"
		
		do while true
			
			fetch cu_condizioni_distinta into :ldt_data_inizio_val, :ll_progressivo, :ls_cod_prodotto_padre, :ll_num_sequenza, :ls_cod_prodotto_figlio, :ls_des_listino_produzione, :ld_minimo_fatt_altezza, :ld_minimo_fatt_larghezza, :ld_minimo_fatt_profondita, :ld_minimo_fatt_superficie, :ld_minimo_fatt_volume, :ls_flag_sconto_a_parte, :ls_flag_tipo_scaglioni, :ll_scaglione_1, :ll_scaglione_2, :ll_scaglione_3, :ll_scaglione_4, :ll_scaglione_5, :ls_flag_sconto_mag_prezzo_1, :ls_flag_sconto_mag_prezzo_2, :ls_flag_sconto_mag_prezzo_3, :ls_flag_sconto_mag_prezzo_4, :ls_flag_sconto_mag_prezzo_5, :ld_variazione_1, :ld_variazione_2, :ld_variazione_3, :ld_variazione_4, :ld_variazione_5, :ls_flag_origine_prezzo_1, :ls_flag_origine_prezzo_2, :ls_flag_origine_prezzo_3, :ls_flag_origine_prezzo_4, :ls_flag_origine_prezzo_5, :ld_provvigione_1, :ld_provvigione_2, :ls_flag_escludi_linee_sconto;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode = -1  then
				fs_messaggio = "Errore in open cursore su listini produzione nella funzione f_ricerca_condizioni_distinta. Dettaglio " + sqlca.sqlerrtext
				return -1
			end if
			ll_i ++
			guo_functions.uof_log("wf_ricerca_condizioni_distinta. Valuto condizione " + guo_functions.uof_format(ll_i) + " Figlio="+guo_functions.uof_format(ls_cod_prodotto_figlio))
			if ll_i > 1 then
				if ldt_data_inizio_val_precedente <> ldt_data_inizio_val then exit
			end if	
			ls_flag_escludi_linee_sconto_buffer = ls_flag_escludi_linee_sconto
			ldt_data_inizio_val_precedente = ldt_data_inizio_val
			ls_des_variazione = ""
			
			if ls_flag_tipo_scaglioni = "Q" then
				choose case ld_quan_utilizzo
					case is <= ll_scaglione_1
						ll_riga = d_listini_diba.insertrow(0)
						d_listini_diba.setitem(ll_riga,"cod_prodotto_padre", ls_cod_prodotto_padre)
						d_listini_diba.setitem(ll_riga,"num_sequenza", ll_num_sequenza)
						d_listini_diba.setitem(ll_riga,"cod_prodotto_figlio", ls_cod_prodotto_figlio)
						d_listini_diba.setitem(ll_riga,"flag_sconto_mag_prezzo", ls_flag_sconto_mag_prezzo_1)
						d_listini_diba.setitem(ll_riga,"flag_origine_prezzo", ls_flag_origine_prezzo_1)
						d_listini_diba.setitem(ll_riga,"flag_sconto_a_parte", ls_flag_sconto_a_parte)
						d_listini_diba.setitem(ll_riga,"variazione", ld_variazione_1)
						if ld_provvigione_1 > 0 and not isnull(ld_provvigione_1) then
							d_listini_diba.setitem(ll_riga,"provvigione_1", ld_provvigione_1)
						end if
						if ld_provvigione_2 > 0 and not isnull(ld_provvigione_2) then
							d_listini_diba.setitem(ll_riga,"provvigione_2", ld_provvigione_2)
						end if
						ls_des_variazione = "Applicata Condizione Variante " +ls_cod_prodotto_figlio + " su scaglione 1. "
						choose case ls_flag_sconto_mag_prezzo_1
							case "S" 
								ls_des_variazione += " Sconto " + f_decimal_to_string(ld_variazione_1) + "%"
							case "M" 
								ls_des_variazione += " Maggiorazione " + f_decimal_to_string(ld_variazione_1) + "%"
							case "A" 
								ls_des_variazione += " Aumento " + f_decimal_to_string(ld_variazione_1)
							case "D" 
								ls_des_variazione += " Diminuzione " + f_decimal_to_string(ld_variazione_1)
							case "P" 
								ls_des_variazione += " Sostituzione di prezzo " + f_decimal_to_string(ld_variazione_1)
						end choose	
						wf_listino_log(ls_des_variazione)
						
					case is <= ll_scaglione_2
						ll_riga = d_listini_diba.insertrow(0)
						d_listini_diba.setitem(ll_riga,"cod_prodotto_padre", ls_cod_prodotto_padre)
						d_listini_diba.setitem(ll_riga,"num_sequenza", ll_num_sequenza)
						d_listini_diba.setitem(ll_riga,"cod_prodotto_figlio", ls_cod_prodotto_figlio)
						d_listini_diba.setitem(ll_riga,"flag_sconto_mag_prezzo", ls_flag_sconto_mag_prezzo_2)
						d_listini_diba.setitem(ll_riga,"flag_origine_prezzo", ls_flag_origine_prezzo_2)
						d_listini_diba.setitem(ll_riga,"flag_sconto_a_parte", ls_flag_sconto_a_parte)
						d_listini_diba.setitem(ll_riga,"variazione", ld_variazione_2)
						if ld_provvigione_1 > 0 and not isnull(ld_provvigione_1) then
							d_listini_diba.setitem(ll_riga,"provvigione_1", ld_provvigione_1)
						end if
						if ld_provvigione_2 > 0 and not isnull(ld_provvigione_2) then
							d_listini_diba.setitem(ll_riga,"provvigione_2", ld_provvigione_2)
						end if
						
						ls_des_variazione = "Applicata Condizione Variante " +ls_cod_prodotto_figlio + " su scaglione 2. "
						choose case ls_flag_sconto_mag_prezzo_2
							case "S" 
								ls_des_variazione += " Sconto " + f_decimal_to_string(ld_variazione_2) + "%"
							case "M" 
								ls_des_variazione += " Maggiorazione " + f_decimal_to_string(ld_variazione_2) + "%"
							case "A" 
								ls_des_variazione += " Aumento " + f_decimal_to_string(ld_variazione_2)
							case "D" 
								ls_des_variazione += " Diminuzione " + f_decimal_to_string(ld_variazione_2)
							case "P" 
								ls_des_variazione += " Sostituzione di prezzo " + f_decimal_to_string(ld_variazione_2)
						end choose	
						wf_listino_log(ls_des_variazione)
						
					case is <= ll_scaglione_3
						ll_riga = d_listini_diba.insertrow(0)
						d_listini_diba.setitem(ll_riga,"cod_prodotto_padre", ls_cod_prodotto_padre)
						d_listini_diba.setitem(ll_riga,"num_sequenza", ll_num_sequenza)
						d_listini_diba.setitem(ll_riga,"cod_prodotto_figlio", ls_cod_prodotto_figlio)
						d_listini_diba.setitem(ll_riga,"flag_sconto_mag_prezzo", ls_flag_sconto_mag_prezzo_3)
						d_listini_diba.setitem(ll_riga,"flag_origine_prezzo", ls_flag_origine_prezzo_3)
						d_listini_diba.setitem(ll_riga,"flag_sconto_a_parte", ls_flag_sconto_a_parte)
						d_listini_diba.setitem(ll_riga,"variazione", ld_variazione_3)
						if ld_provvigione_1 > 0 and not isnull(ld_provvigione_1) then
							d_listini_diba.setitem(ll_riga,"provvigione_1", ld_provvigione_1)
						end if
						if ld_provvigione_2 > 0 and not isnull(ld_provvigione_2) then
							d_listini_diba.setitem(ll_riga,"provvigione_2", ld_provvigione_2)
						end if
						
						ls_des_variazione = "Applicata Condizione Variante " +ls_cod_prodotto_figlio + " su scaglione 3. "
						choose case ls_flag_sconto_mag_prezzo_3
							case "S" 
								ls_des_variazione += " Sconto " + f_decimal_to_string(ld_variazione_3) + "%"
							case "M" 
								ls_des_variazione += " Maggiorazione " + f_decimal_to_string(ld_variazione_3) + "%"
							case "A" 
								ls_des_variazione += " Aumento " + f_decimal_to_string(ld_variazione_3)
							case "D" 
								ls_des_variazione += " Diminuzione " + f_decimal_to_string(ld_variazione_3)
							case "P" 
								ls_des_variazione += " Sostituzione di prezzo " + f_decimal_to_string(ld_variazione_3)
						end choose	
						wf_listino_log(ls_des_variazione)
						
					case is <= ll_scaglione_4
						ll_riga = d_listini_diba.insertrow(0)
						d_listini_diba.setitem(ll_riga,"cod_prodotto_padre", ls_cod_prodotto_padre)
						d_listini_diba.setitem(ll_riga,"num_sequenza", ll_num_sequenza)
						d_listini_diba.setitem(ll_riga,"cod_prodotto_figlio", ls_cod_prodotto_figlio)
						d_listini_diba.setitem(ll_riga,"flag_sconto_mag_prezzo", ls_flag_sconto_mag_prezzo_4)
						d_listini_diba.setitem(ll_riga,"flag_origine_prezzo", ls_flag_origine_prezzo_4)
						d_listini_diba.setitem(ll_riga,"flag_sconto_a_parte", ls_flag_sconto_a_parte)
						d_listini_diba.setitem(ll_riga,"variazione", ld_variazione_4)
						if ld_provvigione_1 > 0 and not isnull(ld_provvigione_1) then
							d_listini_diba.setitem(ll_riga,"provvigione_1", ld_provvigione_1)
						end if
						if ld_provvigione_2 > 0 and not isnull(ld_provvigione_2) then
							d_listini_diba.setitem(ll_riga,"provvigione_2", ld_provvigione_2)
						end if
						
						ls_des_variazione = "Applicata Condizione Variante " +ls_cod_prodotto_figlio + " su scaglione 4. "
						choose case ls_flag_sconto_mag_prezzo_4
							case "S" 
								ls_des_variazione += " Sconto " + f_decimal_to_string(ld_variazione_4) + "%"
							case "M" 
								ls_des_variazione += " Maggiorazione " + f_decimal_to_string(ld_variazione_4) + "%"
							case "A" 
								ls_des_variazione += " Aumento " + f_decimal_to_string(ld_variazione_4)
							case "D" 
								ls_des_variazione += " Diminuzione " + f_decimal_to_string(ld_variazione_4)
							case "P" 
								ls_des_variazione += " Sostituzione di prezzo " + f_decimal_to_string(ld_variazione_4)
						end choose	
						wf_listino_log(ls_des_variazione)
						
					case else
						ll_riga = d_listini_diba.insertrow(0)
						d_listini_diba.setitem(ll_riga,"cod_prodotto_padre", ls_cod_prodotto_padre)
						d_listini_diba.setitem(ll_riga,"num_sequenza", ll_num_sequenza)
						d_listini_diba.setitem(ll_riga,"cod_prodotto_figlio", ls_cod_prodotto_figlio)
						d_listini_diba.setitem(ll_riga,"flag_sconto_mag_prezzo", ls_flag_sconto_mag_prezzo_5)
						d_listini_diba.setitem(ll_riga,"flag_origine_prezzo", ls_flag_origine_prezzo_5)
						d_listini_diba.setitem(ll_riga,"flag_sconto_a_parte", ls_flag_sconto_a_parte)
						d_listini_diba.setitem(ll_riga,"variazione", ld_variazione_5)
						if ld_provvigione_1 > 0 and not isnull(ld_provvigione_1) then
							d_listini_diba.setitem(ll_riga,"provvigione_1", ld_provvigione_1)
						end if
						if ld_provvigione_2 > 0 and not isnull(ld_provvigione_2) then
							d_listini_diba.setitem(ll_riga,"provvigione_2", ld_provvigione_2)
						end if
						
						ls_des_variazione = "Applicata Condizione Variante " +ls_cod_prodotto_figlio + " su scaglione 5. "
						choose case ls_flag_sconto_mag_prezzo_5
							case "S" 
								ls_des_variazione += " Sconto " + f_decimal_to_string(ld_variazione_5) + "%"
							case "M" 
								ls_des_variazione += " Maggiorazione " + f_decimal_to_string(ld_variazione_5) + "%"
							case "A" 
								ls_des_variazione += " Aumento " + f_decimal_to_string(ld_variazione_5)
							case "D" 
								ls_des_variazione += " Diminuzione " + f_decimal_to_string(ld_variazione_5)
							case "P" 
								ls_des_variazione += " Sostituzione di prezzo " + f_decimal_to_string(ld_variazione_5)
						end choose	
						wf_listino_log(ls_des_variazione)
						
				end choose	
			end if
		loop
		
		ld_prezzo = 0
		close cu_condizioni_distinta;
	end if
next

// Esclusione linee di sconto in caso di attivazione del flag in condizioni distinta
// Richiesto da Antonella Bellin 23/03/2011
if ls_flag_escludi_linee_sconto_buffer = "S" then lb_flag_escludi_linee_sconto = TRUE

//  -------------------------  imposto filtro per prezzi fissi; leggo prima i prezzi fissi  ----------------------------------------
	
ll_count_addizionale = 0

ld_prezzo_vendita_precedente = fd_valore_pf
	
ls_filter = "flag_sconto_mag_prezzo = 'P' "
d_listini_diba.setfilter(ls_filter)
d_listini_diba.filter()

ll_righe = d_listini_diba.rowcount()
for ll_z = 1 to ll_righe
	ls_flag_sconto_mag_prezzo = d_listini_diba.getitemstring(ll_z, "flag_sconto_mag_prezzo")
	ld_variazione = d_listini_diba.getitemnumber(ll_z, "variazione")
	ls_flag_origine_prezzo = d_listini_diba.getitemstring(ll_z, "flag_origine_prezzo")
	ls_flag_sconto_a_parte = d_listini_diba.getitemstring(ll_z, "flag_sconto_mag_prezzo")
	if d_listini_diba.getitemnumber(ll_z, "provvigione_1") > 0 then
		ib_provvigione_da_variante_cliente = true
		str_output.provvigione_varianti_1 = d_listini_diba.getitemnumber(ll_z, "provvigione_1")
	end if
	if d_listini_diba.getitemnumber(ll_z, "provvigione_2") > 0 then
		ib_provvigione_da_variante_cliente = true
		str_output.provvigione_varianti_2 = d_listini_diba.getitemnumber(ll_z, "provvigione_2")
	end if
	
	wf_componi_prezzo_diba_decimal ( ls_cod_prodotto_figlio, &
							 ls_flag_sconto_mag_prezzo, &
							 ld_variazione, &
							 ld_variazione_precedente, &
							 ld_prezzo_vendita_precedente, &
							 ls_flag_origine_prezzo, &
							 ls_flag_sconto_a_parte, &
							 ld_sconto, &
							 ld_maggiorazione, &
							 ld_prezzo, &
							 ls_messaggio)

	guo_functions.uof_log("wf_ricerca_condizioni_distinta (listini_prod_comune_var_client). Applico sostituzione Prezzo. Prezzo precedente = " + guo_functions.uof_format(ld_prezzo_vendita_precedente) + " - Prezzo nuovo = " + guo_functions.uof_format(ld_prezzo) )
							 
	ld_variazione_precedente = ld_variazione
	ld_prezzo_vendita_precedente = ld_prezzo			// sostituisco il prezzo precedente 
	ld_valore_variazioni = ld_prezzo
next

fd_valore_pf = ld_prezzo_vendita_precedente

//  ------------------------------------  imposto filtro sconti e maggiorazioni  --------------------------------------------

ls_filter = "flag_sconto_mag_prezzo = 'S' or flag_sconto_mag_prezzo = 'D' "
d_listini_diba.setfilter(ls_filter)
d_listini_diba.filter()

ll_righe = d_listini_diba.rowcount()
for ll_z = 1 to ll_righe
	ls_flag_sconto_mag_prezzo = d_listini_diba.getitemstring(ll_z, "flag_sconto_mag_prezzo")
	ld_variazione = d_listini_diba.getitemnumber(ll_z, "variazione")
	ls_flag_origine_prezzo = d_listini_diba.getitemstring(ll_z, "flag_origine_prezzo")
	ls_flag_sconto_a_parte = d_listini_diba.getitemstring(ll_z, "flag_sconto_mag_prezzo")
	if d_listini_diba.getitemnumber(ll_z, "provvigione_1") > 0 then
		ib_provvigione_da_variante_cliente = true
		str_output.provvigione_varianti_1 = d_listini_diba.getitemnumber(ll_z, "provvigione_1")
	end if
	if d_listini_diba.getitemnumber(ll_z, "provvigione_2") > 0 then
		ib_provvigione_da_variante_cliente = true
		str_output.provvigione_varianti_2 = d_listini_diba.getitemnumber(ll_z, "provvigione_2")
	end if
	
	wf_componi_prezzo_diba_decimal ( ls_cod_prodotto_figlio, &
							 ls_flag_sconto_mag_prezzo, &
							 ld_variazione, &
							 ld_variazione_precedente, &
							 fd_valore_pf, &
							 ls_flag_origine_prezzo, &
							 ls_flag_sconto_a_parte, &
							 ld_sconto, &
							 ld_maggiorazione, &
							 ld_prezzo, &
							 ls_messaggio)
							 
	guo_functions.uof_log("wf_ricerca_condizioni_distinta (listini_prod_comune_var_client). Applico Tipo Variazione=" + ls_flag_sconto_mag_prezzo + " - Variazione=" + guo_functions.uof_format(ld_variazione) + " -Prezzo precedente = " + guo_functions.uof_format(ld_prezzo_vendita_precedente) + " -Prezzo nuovo = " + guo_functions.uof_format(ld_prezzo) )

	ld_variazione_precedente = ld_variazione
	ld_prezzo_vendita_precedente = ld_prezzo_vendita_precedente - ld_prezzo
	ld_valore_variazioni = ld_valore_variazioni - ld_prezzo
next
 
ls_filter = "flag_sconto_mag_prezzo = 'M' or flag_sconto_mag_prezzo = 'A' "
d_listini_diba.setfilter(ls_filter)
d_listini_diba.filter()

ll_righe = d_listini_diba.rowcount()
for ll_z = 1 to ll_righe
	ls_cod_prodotto_figlio = d_listini_diba.getitemstring(ll_z, "cod_prodotto_figlio")
	ls_flag_sconto_mag_prezzo = d_listini_diba.getitemstring(ll_z, "flag_sconto_mag_prezzo")
	ld_variazione = d_listini_diba.getitemnumber(ll_z, "variazione")
	ls_flag_origine_prezzo = d_listini_diba.getitemstring(ll_z, "flag_origine_prezzo")
	ls_flag_sconto_a_parte = d_listini_diba.getitemstring(ll_z, "flag_sconto_a_parte")
	if d_listini_diba.getitemnumber(ll_z, "provvigione_1") > 0 then
		ib_provvigione_da_variante_cliente = true
		str_output.provvigione_varianti_1 = d_listini_diba.getitemnumber(ll_z, "provvigione_1")
	end if
	if d_listini_diba.getitemnumber(ll_z, "provvigione_2") > 0 then
		ib_provvigione_da_variante_cliente = true
		str_output.provvigione_varianti_2 = d_listini_diba.getitemnumber(ll_z, "provvigione_2")
	end if
	
	wf_componi_prezzo_diba_decimal ( ls_cod_prodotto_figlio, &
							 ls_flag_sconto_mag_prezzo, &
							 ld_variazione, &
							 ld_variazione_precedente, &
							 fd_valore_pf, &
							 ls_flag_origine_prezzo, &
							 ls_flag_sconto_a_parte, &
							 ld_sconto, &
							 ld_maggiorazione, &
							 ld_prezzo, &
							 ls_messaggio)
							 
	guo_functions.uof_log("wf_ricerca_condizioni_distinta (listini_prod_comune_var_client). Applico Tipo Variazione=" + ls_flag_sconto_mag_prezzo + " - Variazione=" + guo_functions.uof_format(ld_variazione) + " -Prezzo precedente = " + guo_functions.uof_format(ld_prezzo_vendita_precedente) + " -Prezzo nuovo = " + guo_functions.uof_format(ld_prezzo) )
//	ld_variazione_precedente = ld_variazione
//	ld_prezzo_vendita_precedente = ld_prezzo_vendita_precedente + ld_prezzo
//	ld_valore_variazioni = ld_valore_variazioni + ld_prezzo
	ls_flag_addizionale = "N"
	if upperbound(is_prodotti_addizionali) > 0 then
		for ll_y = 1 to upperbound(is_prodotti_addizionali)
			if is_prodotti_addizionali[ll_y] = ls_cod_prodotto_figlio then 
				ls_flag_addizionale = "S"
				exit
			end if
		next
	end if
	
	if ls_flag_prezzo_addizionale_separato = "N" or ls_flag_addizionale = "N" then
		ld_variazione_precedente = ld_variazione
		ld_prezzo_vendita_precedente = ld_prezzo_vendita_precedente + ld_prezzo
		ld_valore_variazioni = ld_valore_variazioni + ld_prezzo
	else
		ll_count_addizionale ++
//		str_output.str_addizionali[ll_count_addizionale].cod_addizionale = d_listini_diba.getitemstring(ll_z, "cod_prodotto_figlio")
//		str_output.str_addizionali[ll_count_addizionale].valore = ld_prezzo
	end if


next

fd_valore_pf = ld_prezzo_vendita_precedente
guo_functions.uof_log("wf_ricerca_condizioni_distinta (listini_prod_comune_var_client). Prezzo finale restituito=" + guo_functions.uof_format(fd_valore_pf) )

return 0

end function

public function integer wf_componi_prezzo_diba_decimal (string fs_cod_prodotto, string flag_sconto_mag_prezzo, decimal fd_variazione, decimal fd_variazione_precedente, decimal fd_prezzo_vendita_precedente, string flag_origine_prezzo, string flag_sconto_a_parte, ref decimal fd_sconto, ref decimal fd_maggiorazione, ref decimal fd_prezzo, ref string fs_messaggio);string ls_cod_materia_prima[], ls_messaggio
dec{4} ld_costo_standard, ld_costo_ultimo, ld_prezzo_acquisto, ld_prezzo_vendita, ld_costo_produzione, &
       ld_quantita_utilizzo[], ld_prezzo

select anag_prodotti.costo_standard,   
       anag_prodotti.costo_ultimo,   
       anag_prodotti.prezzo_acquisto,   
       anag_prodotti.prezzo_vendita  
into   :ld_costo_standard,   
       :ld_costo_ultimo,   
       :ld_prezzo_acquisto,   
       :ld_prezzo_vendita  
from   anag_prodotti
where  cod_azienda = :s_cs_xx_cod_azienda and
       cod_prodotto = :fs_cod_prodotto;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca del prodotto " + fs_cod_prodotto
	return -1
end if


choose case flag_origine_prezzo
	case "A"												// PREZZO DI ACQUISTO
			wf_calcola_variazione_diba_decimal(flag_sconto_mag_prezzo, fd_variazione, ld_prezzo_acquisto, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
	case "S"												// COSTO STANDARD
			wf_calcola_variazione_diba_decimal(flag_sconto_mag_prezzo, fd_variazione, ld_costo_standard, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
	case "U"												// COSTO ULTIMO
			wf_calcola_variazione_diba_decimal(flag_sconto_mag_prezzo, fd_variazione, ld_costo_ultimo, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
	case "V"												// PREZZO DI VENDITA DI MAGAZZINO
			wf_calcola_variazione_diba_decimal(flag_sconto_mag_prezzo, fd_variazione, ld_prezzo_vendita, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
	case "N"   // variazione rispetto alla precedente condizione ( se esiste altrimenti ciccia!! )
			wf_calcola_variazione_diba_decimal(flag_sconto_mag_prezzo, fd_variazione, fd_prezzo_vendita_precedente, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
	case "L"   // variazione rispetto al listino standard del prodotto ( vado alla ricerca delle condizioni standard
		        // del prodotto nella stessa tabella.
			wf_calcola_variazione_diba_decimal(flag_sconto_mag_prezzo, fd_variazione, fd_prezzo_vendita_precedente, ld_prezzo_vendita )
			fd_sconto = 0
			fd_prezzo = ld_prezzo_vendita
end choose
return 0

end function

public function integer wf_calcola_variazione_diba_decimal (string flag_sconto_mag_prezzo, decimal fd_variazione, decimal fd_valore_origine, ref decimal fd_prezzo_finale);
choose case flag_sconto_mag_prezzo
	case "S"
		fd_prezzo_finale = (fd_valore_origine / 100 ) * fd_variazione
		fd_prezzo_finale = round(fd_prezzo_finale, 4)
	case "M"
		fd_prezzo_finale = (fd_valore_origine / 100 ) * fd_variazione
		fd_prezzo_finale = round(fd_prezzo_finale, 4)
	case "D"
		fd_prezzo_finale = fd_variazione
	case "A"
		fd_prezzo_finale = fd_variazione
	case "P"
		fd_prezzo_finale = fd_variazione
	case else
		return -1
end choose

return 0
end function

public subroutine wf_condizioni_cliente (ref string as_errore);uo_gruppi_sconti luo_gruppi_sconti
boolean lb_test
string ls_colonna_sconto, ls_messaggio, ls_cod_valuta, ls_flag_calcolo_spec, ls_flag_gruppi_sconto, ls_flag_usa_listino_euro, ls_cod_valuta_sistema
double ld_quantita, ld_sconti[], ld_maggiorazioni[], ld_variazioni[], ld_sconto_tot, ld_prezzo_da_arrotondare, ld_prezzo_arrotondato, ld_provvigioni_1[], ld_provvigioni_2[], &
			ld_provv_1, ld_provv_2
datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva
long ll_riga_origine, ll_i, ll_y, ll_z, ll_return, ll_colonna

		
select flag_usa_listino_euro
into :ls_flag_usa_listino_euro
from con_vendite
where cod_azienda = :s_cs_xx_cod_azienda;

if isnull(ls_flag_usa_listino_euro) or ls_flag_usa_listino_euro="" or sqlca.sqlcode <> 0 then ls_flag_usa_listino_euro = "N"

if str_parametri.cambio_ven = 0 or isnull(str_parametri.cambio_ven) then str_parametri.cambio_ven = 1

select stringa
into   :ls_cod_valuta_sistema
from   parametri_azienda
where  cod_azienda = :s_cs_xx_cod_azienda and
       cod_parametro = 'LIR';
if sqlca.sqlcode <> 0 then
	as_errore = "Impostare parametro LIR in parametri aziendali"
	return
end if
ld_provv_1 = 0
ld_provv_2 = 0

guo_functions.uof_log("wf_condizioni_cliente. Ingresso Funzione" )

// siccome il cliente non può essere NULL allora lo imposto ad un valore assurdo
// EnMe 05/02/2008
if isnull(str_parametri.cod_cliente) then
	str_parametri.cod_cliente = "XXYYXX"
end if

if ls_flag_usa_listino_euro = "S" then 
	ls_cod_valuta = ls_cod_valuta_sistema
else
	ls_cod_valuta = str_parametri.cod_valuta
end if

if ib_prezzi then
	for ll_i = 1 to 10
		str_output.sconti[ll_i] = 0
	next
	
	istr_listini_ven_dim.dim_1 = str_parametri.dim_1
	istr_listini_ven_dim.dim_2 = str_parametri.dim_2
	
	wf_ricerca_condizioni_clienti(str_parametri.cod_tipo_listino_prodotto, &
										  ls_cod_valuta, &
										  str_parametri.data_riferimento, &
										  str_parametri.cod_cliente, &
										  str_parametri.cod_prodotto, &
										  istr_listini_ven_dim, &
										  str_parametri.quantita, &
										  str_parametri.valore,&
										  ld_sconti[],&
										  ld_maggiorazioni[],&
										  ld_variazioni[], &
										  false, &
										  str_output.min_fat_altezza, &
										  str_output.min_fat_larghezza, &
										  str_output.min_fat_profondita, &
										  str_output.min_fat_superficie, &
										  str_output.min_fat_volume,&
										  ref as_errore)
										  
	ll_y = 0
	str_output.sconti = ld_sconti
	str_output.maggiorazioni = ld_maggiorazioni
	
	for ll_i = 1 to upperbound(ld_variazioni)
		guo_functions.uof_log("wf_condizioni_cliente. Variazione " + string(ll_i) + "=" + guo_functions.uof_format(ld_variazioni[ll_i]) )
	next
	
	// valuta del sistema diversa (EnMe 04-03-2014 x Dek)
	if ls_flag_usa_listino_euro="S" then
		if ls_cod_valuta_sistema <> str_parametri.cod_valuta and str_parametri.cambio_ven <> 0 then
			for ll_i = 1 to upperbound(ld_variazioni)
				ld_variazioni[ll_i] = round(ld_variazioni[ll_i] / str_parametri.cambio_ven, 4)
				guo_functions.uof_log("wf_condizioni_cliente. Applicato cambio " + guo_functions.uof_format(str_parametri.cambio_ven) + " Nuovo Valore:" +  + guo_functions.uof_format(ld_variazioni[ll_i]) )
			next
		end if
	end if
	// -------------------------
	str_output.variazioni = ld_variazioni
	lb_test = true
	
	if lb_test and is_flag_gruppi_sconto = "S" and ib_prezzo_speciale = false and not lb_flag_escludi_linee_sconto then
		
		luo_gruppi_sconti = CREATE uo_gruppi_sconti
		luo_gruppi_sconti.s_cs_xx_cod_azienda = s_cs_xx_cod_azienda
		luo_gruppi_sconti.s_cs_xx_db_funzioni_formato_data = s_cs_xx_db_funzioni_formato_data
		if luo_gruppi_sconti.fuo_ricerca_condizioni(str_parametri.cod_prodotto, str_parametri.cod_cliente, ls_cod_valuta, str_parametri.cod_agente_1, str_parametri.data_riferimento, str_output.gruppi_sconti[], str_output.provvigione_1, str_output.provvigione_2, ls_messaggio) >= 0 then
			
			for ll_i = 1 to 10
				str_output.sconti[ll_i] = str_output.gruppi_sconti[ll_i]
			next
			
		end if
		
	end if
end if	

uof_calcola_sconto_tot(str_output.sconti, ld_sconto_tot)

// nel caso sia attiva la gestione dei gruppi di sconto, la provvigione viene presa dall'archivio delle provvigioni solo se
// esiste una condizione speciale (cioè cliente-prodotto).

if ib_provvigioni = TRUE then
	
   if is_flag_gruppi_sconto = "N" or ( is_flag_gruppi_sconto = "S" and ib_prezzo_speciale) or ( is_flag_gruppi_sconto = "S" and ib_provvigione_da_variante_cliente ) then
		
		ls_flag_calcolo_spec = "N"
		select flag_calcolo_spec
		into   :ls_flag_calcolo_spec
		from   con_vendite
		where  cod_azienda = :s_cs_xx_cod_azienda;
		
		ll_return = 0
		if not isnull(str_parametri.cod_agente_1) and len(str_parametri.cod_agente_1) > 0 then
			if ls_flag_calcolo_spec ="S" then
				if ll_return = 0 and ib_setitem_provvigioni then
					str_parametri.ldw_oggetto.setitem(str_parametri.ldw_oggetto.getrow(), "provvigione_1", str_output.provvigione_1)
				end if
			end if
			if ls_flag_calcolo_spec ="N" or ll_return = 1 then
//				if str_output.provvigione_varianti_1 > 0 and ib_prezzo_speciale = TRUE then
				if str_output.provvigione_varianti_1 > 0 and ( ib_prezzo_speciale OR ib_provvigione_da_variante_cliente ) then
					str_output.provvigione_1 = str_output.provvigione_varianti_1
				else
					if wf_ricerca_provv_agenti(str_parametri.cod_tipo_listino_prodotto, &
												 ls_cod_valuta, &
												 str_parametri.data_riferimento, &
												 str_parametri.cod_agente_1, &
												 str_parametri.cod_cliente, &
												 str_parametri.cod_prodotto, &
												 str_parametri.quantita, &
												 ld_sconto_tot, &
												 ld_provvigioni_1[], &
												 as_errore) = 0 then
						if upperbound(ld_provvigioni_1) = 0 or isnull(upperbound(ld_provvigioni_1)) then
							if ld_provv_1 > 0 then
								str_output.provvigione_1 = ld_provv_1
							else
								str_output.provvigione_1 = 0
							end if
						else		// esiste una provvigione particolare inserita
							str_output.provvigione_1 = ld_provvigioni_1[upperbound(ld_provvigioni_1)]
						end if
						if ib_setitem_provvigioni then
							str_parametri.ldw_oggetto.setitem(str_parametri.ldw_oggetto.getrow(), "provvigione_1", str_output.provvigione_1)
						end if
					end if
				end if
			end if									  
		end if
		
		if not isnull(str_parametri.cod_agente_2) and len(str_parametri.cod_agente_2) > 0 then
			if ls_flag_calcolo_spec ="S" then
				if ll_return = 0 and ib_setitem_provvigioni then
					str_parametri.ldw_oggetto.setitem(str_parametri.ldw_oggetto.getrow(), "provvigione_2", str_output.provvigione_2)
				end if
			end if
			if ls_flag_calcolo_spec ="N" or ll_return = 1 then
//				if str_output.provvigione_varianti_2 > 0 and ib_prezzo_speciale = TRUE then
				if str_output.provvigione_varianti_2 > 0 and ( ib_prezzo_speciale OR ib_provvigione_da_variante_cliente ) then
					str_output.provvigione_2 = str_output.provvigione_varianti_2
				else
					if wf_ricerca_provv_agenti(str_parametri.cod_tipo_listino_prodotto, &
												 str_parametri.cod_valuta, &
												 str_parametri.data_riferimento, &
												 str_parametri.cod_agente_2, &
												 str_parametri.cod_cliente, &
												 str_parametri.cod_prodotto, &
												 str_parametri.quantita, &
												 ld_sconto_tot, &
												 ld_provvigioni_2[], &
												 as_errore) = 0 then
						if upperbound(ld_provvigioni_2) = 0 or isnull(upperbound(ld_provvigioni_2)) then
							if ld_provv_2 > 0 then
								str_output.provvigione_2 = ld_provv_2
							else
								str_output.provvigione_2 = 0
							end if
						else
							str_output.provvigione_2 = ld_provvigioni_2[upperbound(ld_provvigioni_2)]
						end if
						if ib_setitem_provvigioni then
							str_parametri.ldw_oggetto.setitem(str_parametri.ldw_oggetto.getrow(), "provvigione_2", str_output.provvigione_2)
						end if
					end if
				end if
			end if
		end if									  
	end if
end if

// arrotondamenti delle variazioni
for ll_i = 1 to upperbound(str_output.variazioni)
	// arrotondamento sul prezzo in base a tab_arrotonda_prezzi
	ld_prezzo_da_arrotondare = str_output.variazioni[ll_i]
	if uof_arrotonda_prezzo( ld_prezzo_da_arrotondare, str_parametri.cod_valuta, ld_prezzo_arrotondato, ls_messaggio) = -1 then
		as_errore = "Errore arrotondamento prezzo" + ls_messaggio
	else
		str_output.variazioni[ll_i] = ld_prezzo_arrotondato
	end if
// fine arrotondamento sul prezzo in base a tab_arrotonda_prezzi
next 

ib_setitem=TRUE
ib_setitem_provvigioni=TRUE
ib_prezzi = TRUE
ib_provvigioni = TRUE
ib_prezzo_speciale = FALSE
ib_provvigione_da_variante_cliente = FALSE

guo_functions.uof_log("wf_condizioni_cliente. Uscita Funzione" )

return
end subroutine

public function integer wf_ricerca_provv_agenti (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_riferimento_val, string fs_cod_agente, string fs_cod_cliente, string fs_cod_prodotto, double fd_quan_scaglione, double fd_sconto_tot, ref double fd_variazione[], ref string as_errore);// --------------------------------------------------------------------------------------------------------
//	FUNZIONE DI RICERCA PROVVIGIONI IN TABELLA PROVVIGIONI
//
//
//	AUTORE:ENRICO MENEGOTTO   15/02/2000
//		 SUCCESSIVE MODFICHE :
//		 -----------------------------------------------------------
//
//
//
// DESCRIZIONE INPUT/OUTPUT
//	NOME VARIABILE 						TIPO_DATO 			Input/Output
//	---------------------------------------------------------------
//	fs_cod_tipo_listino_prodotto		string				I
//	fs_cod_valuta							string				I
//	fdt_data_riferimento_val			datetime				I
//	fs_cod_cliente							string				I
//	fs_cod_prodotto						string				I
//	fd_quan_scaglione						double				I
// fd_provvigione							double				O
//	
//
// -------------------------------------------------------------------------------------------------------------
boolean lb_test
string ls_cod_cat_mer, ls_cod_categoria, ls_filtro, ls_messaggio, ls_sql, ls_flag_sconto_mag_prezzo, ls_flag_origine_prezzo, ls_flag_sconto_a_parte, &
	  ls_flag_tipo_scaglioni, ls_flag, ls_cod_agente, ls_flag_ric_provv_sconti, ls_cod_prodotto, ls_cod_cliente, &
	  ls_des_listino_vendite, ls_flag_ric_agen_cli_prod, ls_flag_ric_agen_cat_cli_prod, ls_flag_ric_agen_cli_cat_mer, &
	  ls_flag_ric_agen_cliente, ls_flag_ric_agen_cat_cli, ls_flag_ric_agen_cat_mer, ls_flag_ric_agen_prodotto, &
	  ls_flag_ric_provv_cat_cli, ls_flag_ric_provv_agente, ls_flag_ric_provv_cat_mer, ls_flag_ric_provv_prodotto, &
	  ls_flag_ric_provv_cliente
 long ll_num_righe, ll_cont, ll_livello_criterio, ll_i, ll_sconto ,ll_maggiorazione, ll_cont_sconto, &
     ll_cont_maggiorazione, ll_cont_variazione, ll_1, ll_progressivo, ll_ritorno
double ld_variazione_precedente, ld_prezzo_vendita_precedente, ld_prezzo, ld_variazione, ld_minimo_quan_fatturabile, &
	    ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5, ld_var, &
	  ld_scaglione_1, ld_scaglione_2, ld_scaglione_3, ld_scaglione_4, ld_scaglione_5, ld_provvigione
datetime ldt_data_inizio_val, ldt_data_inizio_val_precedente




SELECT flag_ric_agen_cli_prod, 
       flag_ric_agen_cat_cli_prod, 
		 flag_ric_agen_cli_cat_mer, 
		 flag_ric_agen_cat_cli, 
		 flag_ric_agen_cliente, 
		 flag_ric_agen_cat_mer, 
		 flag_ric_agen_prodotto, 
		 flag_ric_provv_cat_cli, 
		 flag_ric_provv_cliente,
		 flag_ric_provv_cat_mer, 
		 flag_ric_provv_prodotto, 
		 flag_ric_provv_agente,
		 flag_ric_provv_sconti
INTO   :ls_flag_ric_agen_cli_prod, 
       :ls_flag_ric_agen_cat_cli_prod, 
		 :ls_flag_ric_agen_cli_cat_mer, 
		 :ls_flag_ric_agen_cat_cli, 
		 :ls_flag_ric_agen_cliente, 
		 :ls_flag_ric_agen_cat_mer, 
		 :ls_flag_ric_agen_prodotto,  
		 :ls_flag_ric_provv_cat_cli, 
		 :ls_flag_ric_provv_cliente,
		 :ls_flag_ric_provv_cat_mer, 
		 :ls_flag_ric_provv_prodotto,
		 :ls_flag_ric_provv_agente,
		 :ls_flag_ric_provv_sconti
FROM   con_vendite  
WHERE  cod_azienda = :s_cs_xx_cod_azienda  ;
if sqlca.sqlcode <> 0 then
	as_errore = "Parametri Vendite: Errore in lettura tabella parametri vendite"
	return -1
end if


// ------------- valuto esistenza provvigioni agente / cliente / prodotto --------------------------------------

if ls_flag_ric_agen_cli_prod = "S" then
	select count(*)
	into  :ll_cont
	from  provvigioni
	where cod_azienda      = :s_cs_xx_cod_azienda and
			cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
			cod_valuta       = :fs_cod_valuta and
			data_inizio_val <= :fdt_data_riferimento_val and
			cod_cliente      = :fs_cod_cliente and
			cod_prodotto     = :fs_cod_prodotto and
			cod_agente       = :fs_cod_agente;
	choose case sqlca.sqlcode
		case 0
			if ll_cont > 0 then
				ll_livello_criterio = 1
				goto ricerca
			else			
			// non esistono condizioni: successivo criterio
			end if
		case 100
			// non esistono condizioni: successivo criterio
		case else
			// errore
	end choose
end if

// ------------- valuto esistenza provvigioni agente/ cat. cliente / prodotto ----------------------------------
if ls_flag_ric_agen_cat_cli_prod = "S" then
	select cod_categoria
	into   :ls_cod_categoria
	from   anag_clienti
	where  cod_azienda = :s_cs_xx_cod_azienda and
			 cod_cliente = :fs_cod_cliente;
	if sqlca.sqlcode = 0 and not(isnull(ls_cod_categoria)) then
		select count(*)
		into  :ll_cont
		from  provvigioni
		where cod_azienda      = :s_cs_xx_cod_azienda and
				cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
				cod_valuta       = :fs_cod_valuta and
				data_inizio_val <= :fdt_data_riferimento_val and
				cod_categoria    = :ls_cod_categoria and
				cod_prodotto     = :fs_cod_prodotto and
				cod_agente       = :fs_cod_agente;
		choose case sqlca.sqlcode
			case 0
				if ll_cont > 0 then
					ll_livello_criterio = 2
					goto ricerca
				else			
					// non esistono condizioni: successivo criterio
				end if
			case 100
				// non esistono condizioni: successivo criterio
			case else
				// errore
		end choose
	end if
end if
// ------------- valuto esistenza provvigioni agente / cliente / categoria prodotto -----------------------------

if ls_flag_ric_agen_cli_cat_mer = "S" then
	select cod_cat_mer
	into   :ls_cod_cat_mer
	from   anag_prodotti
	where  cod_azienda  = :s_cs_xx_cod_azienda and
			 cod_prodotto = :fs_cod_prodotto;
	if sqlca.sqlcode = 0 and not(isnull(ls_cod_cat_mer)) then
		select count(*)
		into  :ll_cont
		from  provvigioni
		where cod_azienda      = :s_cs_xx_cod_azienda and
				cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
				cod_valuta       = :fs_cod_valuta and
				data_inizio_val <= :fdt_data_riferimento_val and
				cod_cliente      = :fs_cod_cliente and
				cod_cat_mer      = :ls_cod_cat_mer and
				cod_agente       = :fs_cod_agente;
		choose case sqlca.sqlcode
			case 0
				if ll_cont > 0 then
					ll_livello_criterio = 3
					goto ricerca
				else			
					// non esistono condizioni: successivo criterio
				end if
			case 100
				// non esistono condizioni: successivo criterio
			case else
				// errore
		end choose
	end if
end if

// ------------- valuto esistenza condizioni agente / categoria cliente -------------------------------------------

if ls_flag_ric_agen_cat_cli = "S" then
	select cod_categoria
	into   :ls_cod_categoria
	from   anag_clienti
	where  cod_azienda = :s_cs_xx_cod_azienda and
			 cod_cliente = :fs_cod_cliente;
	if sqlca.sqlcode = 0 and not(isnull(ls_cod_categoria)) then
		select count(*)
		into  :ll_cont
		from  provvigioni
		where cod_azienda      = :s_cs_xx_cod_azienda and
				cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
				cod_valuta       = :fs_cod_valuta and
				data_inizio_val <= :fdt_data_riferimento_val and
				cod_cliente      is null and
				cod_cat_mer      is null and 
				cod_prodotto     is null and
				cod_categoria    = :ls_cod_categoria and
				cod_agente       = :fs_cod_agente;
		choose case sqlca.sqlcode
			case 0
				if ll_cont > 0 then
					ll_livello_criterio = 4
					goto ricerca
				else			
					// non esistono condizioni: successivo criterio
				end if
			case 100
				// non esistono condizioni: successivo criterio
			case else
				// errore
		end choose
	end if
end if
// ------------- valuto esistenza condizioni agente / cliente ------------------------------------------------

if ls_flag_ric_agen_cliente = "S" then
	select count(*)
	into  :ll_cont
	from  provvigioni
	where cod_azienda      = :s_cs_xx_cod_azienda and
			cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
			cod_valuta       = :fs_cod_valuta and
			data_inizio_val <= :fdt_data_riferimento_val and
			cod_cliente      = :fs_cod_cliente and
			cod_agente       = :fs_cod_agente and
			cod_cat_mer      is null and 
			cod_prodotto     is null and
			cod_categoria    is null;
	choose case sqlca.sqlcode
		case 0
			if ll_cont > 0 then
				ll_livello_criterio = 5
				goto ricerca
			else			
				// non esistono condizioni: successivo criterio
			end if
		case 100
			// non esistono condizioni: successivo criterio
		case else
			// errore
	end choose
end if

// ------------- valuto esistenza condizioni agente / categoria prodotto --------------------------------------
if ls_flag_ric_agen_cat_mer = "S" then
	select cod_cat_mer
	into   :ls_cod_cat_mer
	from   anag_prodotti
	where  cod_azienda  = :s_cs_xx_cod_azienda and
			 cod_prodotto = :fs_cod_prodotto;
	if sqlca.sqlcode = 0 and not(isnull(ls_cod_cat_mer)) then
		select count(*)
		into  :ll_cont
		from  provvigioni
		where cod_azienda      = :s_cs_xx_cod_azienda and
				cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
				cod_valuta       = :fs_cod_valuta and
				data_inizio_val <= :fdt_data_riferimento_val and
				cod_cliente      is null and
				cod_cat_mer      = :ls_cod_cat_mer and 
				cod_agente       = :fs_cod_agente and 
				cod_prodotto     is null and 
				cod_categoria    is null;
		choose case sqlca.sqlcode
			case 0
				if ll_cont > 0 then
					ll_livello_criterio = 6
					goto ricerca
				else			
					// non esistono condizioni: successivo criterio
				end if
			case 100
					// non esistono condizioni: successivo criterio
			case else
				// errore
		end choose
	end if
end if

// ------------- valuto esistenza condizioni agente / prodotto ------------------------------------------------
if ls_flag_ric_agen_prodotto = "S" then
	select count(*)
	into  :ll_cont
	from  provvigioni
	where cod_azienda      = :s_cs_xx_cod_azienda and
			cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
			cod_valuta       = :fs_cod_valuta and
			data_inizio_val <= :fdt_data_riferimento_val and
			cod_cliente      is null and
			cod_cat_mer      is null and 
			cod_prodotto     = :fs_cod_prodotto and
			cod_agente       = :fs_cod_agente and
			cod_categoria    is null;
	choose case sqlca.sqlcode
		case 0
			if ll_cont > 0 then
				ll_livello_criterio = 7
				goto ricerca
			else			
				// non esistono condizioni: successivo criterio
			end if
		case 100
			// non esistono condizioni: successivo criterio
		case else
			// errore
	end choose
end if

// ------------- valuto esistenza provvigioni per categoria cliente -------------------------------------------

if ls_flag_ric_provv_cat_cli = "S" then
	select cod_categoria
	into   :ls_cod_categoria
	from   anag_clienti
	where  cod_azienda = :s_cs_xx_cod_azienda and
			 cod_cliente = :fs_cod_cliente;
	if sqlca.sqlcode = 0 and not(isnull(ls_cod_categoria)) then
		select count(*)
		into  :ll_cont
		from  provvigioni
		where cod_azienda      = :s_cs_xx_cod_azienda and
				cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
				cod_valuta       = :fs_cod_valuta and
				data_inizio_val <= :fdt_data_riferimento_val and
				cod_cliente      is null and
				cod_cat_mer      is null and 
				cod_prodotto     is null and
				cod_agente       is null and
				cod_categoria    = :ls_cod_categoria;
		choose case sqlca.sqlcode
			case 0
				if ll_cont > 0 then
					ll_livello_criterio = 8
					goto ricerca
				else			
					// non esistono condizioni: successivo criterio
				end if
			case 100
				// non esistono condizioni: successivo criterio
			case else
				// errore
		end choose
	end if
end if
// ------------- valuto esistenza provvigioni per cliente ------------------------------------------------

if ls_flag_ric_provv_cliente = "S" then
	select count(*)
	into  :ll_cont
	from  provvigioni
	where cod_azienda      = :s_cs_xx_cod_azienda and
			cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
			cod_valuta       = :fs_cod_valuta and
			data_inizio_val <= :fdt_data_riferimento_val and
			cod_cliente      = :fs_cod_cliente and
			cod_cat_mer      is null and 
			cod_prodotto     is null and
			cod_agente       is null and
			cod_categoria    is null;
	choose case sqlca.sqlcode
		case 0
			if ll_cont > 0 then
				ll_livello_criterio = 9
				goto ricerca
			else			
				// non esistono condizioni: successivo criterio
			end if
		case 100
			// non esistono condizioni: successivo criterio
		case else
			// errore
	end choose
end if

// ------------- valuto esistenza provvigioni per categoria prodotto ---------------------------------------------
if ls_flag_ric_provv_cat_mer = "S" then
	select cod_cat_mer
	into   :ls_cod_cat_mer
	from   anag_prodotti
	where  cod_azienda  = :s_cs_xx_cod_azienda and
			 cod_prodotto = :fs_cod_prodotto;
	if sqlca.sqlcode = 0 and not(isnull(ls_cod_cat_mer)) then
		select count(*)
		into  :ll_cont
		from  provvigioni
		where cod_azienda      = :s_cs_xx_cod_azienda and
				cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
				cod_valuta       = :fs_cod_valuta and
				data_inizio_val <= :fdt_data_riferimento_val and
				cod_cliente      is null and
				cod_cat_mer      = :ls_cod_cat_mer and 
				cod_prodotto     is null and 
				cod_agente       is null and
				cod_categoria    is null;
		choose case sqlca.sqlcode
			case 0
				if ll_cont > 0 then
					ll_livello_criterio = 10
					goto ricerca
				else			
					// non esistono condizioni: successivo criterio
				end if
			case 100
				// non esistono condizioni: successivo criterio
			case else
				// errore
		end choose
	end if
end if

// ------------- valuto esistenza condizioni prodotto ------------------------------------------------
if ls_flag_ric_provv_prodotto = "S" then
	select count(*)
	into  :ll_cont
	from  provvigioni
	where cod_azienda      = :s_cs_xx_cod_azienda and
			cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
			cod_valuta       = :fs_cod_valuta and
			data_inizio_val <= :fdt_data_riferimento_val and
			cod_cliente      is null and
			cod_cat_mer      is null and 
			cod_prodotto     = :fs_cod_prodotto and
			cod_agente       is null and
			cod_categoria    is null;
	choose case sqlca.sqlcode
		case 0
			if ll_cont > 0 then
				ll_livello_criterio = 11
				goto ricerca
			else			
				// non esistono condizioni: successivo criterio
			end if
		case 100
			// non esistono condizioni: successivo criterio
		case else
			// errore
	end choose
end if

// ------------- valuto esistenza condizioni agente ------------------------------------------------
if ls_flag_ric_provv_agente = "S" then
	select count(*)
	into  :ll_cont
	from  provvigioni
	where cod_azienda      = :s_cs_xx_cod_azienda and
			cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
			cod_valuta       = :fs_cod_valuta and
			data_inizio_val <= :fdt_data_riferimento_val and
			cod_cliente      is null and
			cod_cat_mer      is null and 
			cod_prodotto     is null and
			cod_categoria    is null and
			cod_agente       = :fs_cod_agente;
	choose case sqlca.sqlcode
		case 0
			if ll_cont > 0 then
				ll_livello_criterio = 12
				goto ricerca
			else			
				// se a questo punto non c'è nessuna provviogione caricata propongo le condizioni presenti nella
				// anagrafica dell'agente 
				ld_provvigione = 0
				select prov_agente
				into  :ld_provvigione
				from  anag_agenti  
				where cod_azienda = :s_cs_xx_cod_azienda and
						cod_agente = :fs_cod_agente ;
				if sqlca.sqlcode = 0 then
					fd_variazione[1] = ld_provvigione
				end if
				return 0
			end if
		case 100
			return 0
		case else
			// errore
	end choose
end if

// ---- se arrivo qui vuol dire che non ho trovato niente -------------------------------
return 0

// ------------------------------- ricerca condizioni -----------------------------------
ricerca:


declare cu_condizioni_cliente dynamic cursor for sqlsa;
ls_sql = "  SELECT data_inizio_val, progressivo, " + &
         "  cod_cat_mer, cod_prodotto, cod_categoria, cod_cliente, cod_agente, " + &
			"  scaglione_1,  scaglione_2,  scaglione_3,  scaglione_4,  scaglione_5, " + &
			"  variazione_1, variazione_2, variazione_3, variazione_4, variazione_5 " + &
			"  FROM provvigioni  " + &
			"  WHERE ( cod_azienda = '" + s_cs_xx_cod_azienda + "' ) AND  " + &
			"  ( cod_tipo_listino_prodotto = '" + fs_cod_tipo_listino_prodotto + "') AND  " + &
			"  ( cod_valuta = '" + fs_cod_valuta + "') AND   " + &
         "  ( data_inizio_val <= '" + string(fdt_data_riferimento_val, s_cs_xx_db_funzioni_formato_data) + "')  "
			ls_filtro = ""
			choose case ll_livello_criterio
				case 1
					ls_filtro = "cod_agente = '" + fs_cod_agente + "' and cod_cliente = '" + fs_cod_cliente + "' and cod_prodotto = '" + fs_cod_prodotto + "'"
				case 2
					ls_filtro = "cod_agente = '" + fs_cod_agente + "' and cod_categoria = '" + ls_cod_categoria + "' and cod_prodotto = '" + fs_cod_prodotto + "'"
				case 3
					ls_filtro = "cod_agente = '" + fs_cod_agente + "' and cod_cliente = '" + fs_cod_cliente + "' and cod_cat_mer = '" + ls_cod_cat_mer + "'"
				case 4
					ls_filtro = "cod_agente = '" + fs_cod_agente + "' and cod_cliente = '" + fs_cod_cliente + "' and cod_prodotto is null and cod_cat_mer is null and cod_categoria is null"
				case 5
					ls_filtro = "cod_agente = '" + fs_cod_agente + "' and cod_categoria = '" + ls_cod_categoria + "' and cod_prodotto is null and cod_cat_mer is null and cod_cliente is null"
				case 6
					ls_filtro = "cod_agente = '" + fs_cod_agente + "' and cod_cat_mer = '" + ls_cod_cat_mer + "' and cod_categoria is null and cod_prodotto is null and cod_cliente is null"
				case 7
					ls_filtro = "cod_agente = '" + fs_cod_agente + "' and cod_prodotto = '" + fs_cod_prodotto + "' and cod_categoria is null and cod_cat_mer is null and cod_cliente is null"
				case 8
					ls_filtro = "cod_agente is null and cod_categoria = '" + ls_cod_categoria + "' and cod_prodotto is null and cod_cat_mer is null and cod_cliente is null"
				case 9
					ls_filtro = "cod_agente is null and cod_cliente = '" + fs_cod_cliente + "' and cod_prodotto is null and cod_cat_mer is null and cod_categoria is null"
				case 10
					ls_filtro = "cod_agente is null and cod_cat_mer = '" + ls_cod_cat_mer + "' and cod_categoria is null and cod_prodotto is null and cod_cliente is null"
				case 11
					ls_filtro = "cod_agente is null and cod_prodotto = '" + fs_cod_prodotto + "' and cod_categoria is null and cod_cat_mer is null and cod_cliente is null"
				case 12
					ls_filtro = "cod_agente = '" + fs_cod_agente + "' and cod_prodotto is null and cod_categoria is null and cod_cat_mer is null and cod_cliente is null"

			end choose

			if len(ls_filtro) > 0 then
				ls_sql = ls_sql + " and " + ls_filtro
			end if
			
			ls_sql = ls_sql + "  ORDER BY cod_azienda ASC,   " + &  
         "  cod_tipo_listino_prodotto ASC,   " + &  
         "  cod_valuta ASC, " + &
         "  data_inizio_val DESC, " + &
         "  progressivo ASC   "  

prepare sqlsa from :ls_sql;

open dynamic cu_condizioni_cliente;

ld_variazione_precedente = 0
ld_prezzo_vendita_precedente = 0
ll_cont_sconto = 0
ll_cont_maggiorazione = 0
ll_cont_variazione = 0
ldt_data_inizio_val_precedente = datetime(date("01/01/1900"), 00:00:00)
ll_i = 0
do while 1=1
	
	fetch cu_condizioni_cliente into :ldt_data_inizio_val, :ll_progressivo, :ls_cod_cat_mer, :ls_cod_prodotto, :ls_cod_categoria, :ls_cod_cliente, :ls_cod_agente, :ld_scaglione_1, :ld_scaglione_2, :ld_scaglione_3, :ld_scaglione_4, :ld_scaglione_5, :ld_variazione_1, :ld_variazione_2, :ld_variazione_3, :ld_variazione_4, :ld_variazione_5;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i ++
	if ll_i > 1 then
		if ldt_data_inizio_val_precedente <> ldt_data_inizio_val then exit
	end if	

	lb_test = true
	if ls_flag_ric_provv_sconti = "S" and not isnull(fd_sconto_tot) then
		select count(*)
		into   :ll_cont
		from   provvigioni_sconti
		where  cod_azienda = :s_cs_xx_cod_azienda and
				 cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
				 cod_valuta = :fs_cod_valuta and
				 data_inizio_val = :ldt_data_inizio_val and
				 progressivo = :ll_progressivo;
		if ll_cont > 0 and sqlca.sqlcode = 0 then
				if ll_ritorno = 0 then
				lb_test = false
			end if
		end if
	end if
	if lb_test then
		choose case fd_quan_scaglione
			case is <= ld_scaglione_1
				ld_variazione = ld_variazione_1
			case is <= ld_scaglione_2
				ld_variazione = ld_variazione_2
			case is <= ld_scaglione_3
				ld_variazione = ld_variazione_3
			case is <= ld_scaglione_4
				ld_variazione = ld_variazione_4
			case else
				ld_variazione = ld_variazione_5
		end choose	
		fd_variazione[ll_i] = ld_variazione
	end if
	ldt_data_inizio_val_precedente = ldt_data_inizio_val
loop
close cu_condizioni_cliente;
return 0

end function

public function integer wf_ricerca_cond_dimensioni (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, long fl_num_scaglione, str_listini_ven_dim astr_listini_ven_dim, ref string fs_flag_sconto_mag_prezzo, ref double fd_variazione);string ls_sql, ls_flag_sconto_mag_prezzo, ls_where_variabili, ls_errore, ls_des_variazione, ls_des_listino_range
long   ll_errore, ll_range, ll_ret
dec{4} ld_limite_dimensione_1, ld_limite_dimensione_2, ld_variazione, ld_limite_prec
datastore lds_data

ls_where_variabili = ""
ls_des_variazione = ""
ls_des_listino_range = ""
if not isnull(astr_listini_ven_dim.cod_variabile_1) then
	ls_where_variabili += " and cod_variabile_1 ='" + astr_listini_ven_dim.cod_variabile_1 + "' "
	if astr_listini_ven_dim.flag_tipo_dato_1 = "S" then
		if not isnull(astr_listini_ven_dim.valore_var_1_str) then
			ls_where_variabili += " and valore_stringa_1 ='" + astr_listini_ven_dim.valore_var_1_str + "' "
		else
			ls_where_variabili += " and valore_stringa_1 is null "
		end if
	else
		if not isnull(astr_listini_ven_dim.valore_var_1_num) then
			ls_where_variabili += " and valore_numero_1 =" + f_decimal_to_string(astr_listini_ven_dim.valore_var_1_num) + " "
		else
			ls_where_variabili += " and valore_numero_1 is null "
		end if
	end if		
else
	ls_where_variabili += " and cod_variabile_1 is null "
end if

if not isnull(astr_listini_ven_dim.cod_variabile_2) then
	ls_where_variabili += " and cod_variabile_2 ='" + astr_listini_ven_dim.cod_variabile_2 + "' "
	if astr_listini_ven_dim.flag_tipo_dato_2 = "S" then
		if not isnull(astr_listini_ven_dim.valore_var_2_str) then
			ls_where_variabili += " and valore_stringa_2 ='" + astr_listini_ven_dim.valore_var_2_str + "' "
		else
			ls_where_variabili += " and valore_stringa_2 is null "
		end if
	else
		if not isnull(astr_listini_ven_dim.valore_var_2_num) then
			ls_where_variabili += " and valore_numero_2 =" + f_decimal_to_string(astr_listini_ven_dim.valore_var_2_num) + " "
		else
			ls_where_variabili += " and valore_numero_2 is null "
		end if
	end if		
else
	ls_where_variabili += " and cod_variabile_2 is null "
end if

if not isnull(astr_listini_ven_dim.cod_variabile_3) then
	ls_where_variabili += " and cod_variabile_3 ='" + astr_listini_ven_dim.cod_variabile_3 + "' "
	if astr_listini_ven_dim.flag_tipo_dato_3 = "S" then
		if not isnull(astr_listini_ven_dim.valore_var_3_str) then
			ls_where_variabili += " and valore_stringa_3 ='" + astr_listini_ven_dim.valore_var_3_str + "' "
		else
			ls_where_variabili += " and valore_stringa_3 is null "
		end if
	else
		if not isnull(astr_listini_ven_dim.valore_var_3_num) then
			ls_where_variabili += " and valore_numero_3 =" + f_decimal_to_string(astr_listini_ven_dim.valore_var_3_num) + " "
		else
			ls_where_variabili += " and valore_numero_3 is null "
		end if
	end if		
else
	ls_where_variabili += " and cod_variabile_3 is null "
end if

if not isnull(astr_listini_ven_dim.cod_variabile_4) then
	ls_where_variabili += " and cod_variabile_4 ='" + astr_listini_ven_dim.cod_variabile_4 + "' "
	if astr_listini_ven_dim.flag_tipo_dato_4 = "S" then
		if not isnull(astr_listini_ven_dim.valore_var_4_str) then
			ls_where_variabili += " and valore_stringa_4 ='" + astr_listini_ven_dim.valore_var_4_str + "' "
		else
			ls_where_variabili += " and valore_stringa_4 is null "
		end if
	else
		if not isnull(astr_listini_ven_dim.valore_var_4_num) then
			ls_where_variabili += " and valore_numero_4 =" + f_decimal_to_string(astr_listini_ven_dim.valore_var_4_num) + " "
		else
			ls_where_variabili += " and valore_numero_4 is null "
		end if
	end if		
else
	ls_where_variabili += " and cod_variabile_4 is null "
end if

ls_sql = " select distinct prog_range from	listini_vendite_dimensioni " + &
			" where  cod_azienda = '" + s_cs_xx_cod_azienda + "' and  " + &
		 	" cod_tipo_listino_prodotto = '" + fs_cod_tipo_listino_prodotto + "' and    " + &
			" cod_valuta      = '" + fs_cod_valuta + "' and    " + &
		 	" data_inizio_val = '" + string(fdt_data_inizio_val,s_cs_xx_db_funzioni_formato_data) + "' and    " + &
		 	" progressivo     = "+ string(fl_progressivo) +" and    " + &
			" num_scaglione   = "+ string(fl_num_scaglione) + &
			ls_where_variabili

ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
if ll_ret < 0 then
	return -1
elseif ll_ret = 0 then
	// non c'è un range con le variabili, passo allo standard, cioè con tutte le variabili null
	select max(prog_range)
	into   :ll_range
	from  listini_vendite_dimensioni
	where  cod_azienda = :s_cs_xx_cod_azienda and
	cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
	cod_valuta      = :fs_cod_valuta and
	data_inizio_val = :fdt_data_inizio_val and 
	progressivo     = :fl_progressivo and
	num_scaglione   = :fl_num_scaglione and
	cod_variabile_1 is null and 
	cod_variabile_2 is null and 
	cod_variabile_3 is null and 
	cod_variabile_4 is null ;

else
	// trovato, procedo con il range corretto
	ll_range = lds_data.getitemnumber(1,1)
	ls_des_listino_range = ls_where_variabili
end if


select max(limite_dimensione_1)
into   :ld_limite_prec
from  listini_vendite_dimensioni
where  cod_azienda = :s_cs_xx_cod_azienda and
cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
cod_valuta      = :fs_cod_valuta and
data_inizio_val = :fdt_data_inizio_val and 
progressivo     = :fl_progressivo and
num_scaglione   = :fl_num_scaglione and
prog_range = :ll_range;

//ld_limite_prec = 9999999

declare cu_dimensioni dynamic cursor for sqlsa;

ls_sql = " select limite_dimensione_1 " + &
		   " variazione  " + &
         " from  listini_vendite_dimensioni  " + &
			" where  cod_azienda = '" + s_cs_xx_cod_azienda + "' and  " + &
		 	" cod_tipo_listino_prodotto = '" + fs_cod_tipo_listino_prodotto + "' and    " + &
		   " cod_valuta      = '" + fs_cod_valuta + "' and    " + &
		 	" data_inizio_val = '" + string(fdt_data_inizio_val,s_cs_xx_db_funzioni_formato_data) + "' and    " + &
		 	" progressivo     = "+ string(fl_progressivo) +" and    " + &
		 	" prog_range     = "+ string(ll_range) +" and    " + &
		   " num_scaglione   = "+ string(fl_num_scaglione) + &
			" order by limite_dimensione_1 DESC"

prepare sqlsa from :ls_sql;

open dynamic cu_dimensioni;

ll_errore = 0
do while true
   fetch cu_dimensioni into :ld_limite_dimensione_1;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then 
		if sqlca.sqlcode = 100 then
			if ld_limite_dimensione_1 > 0 and not isnull(ld_limite_dimensione_1) then
				exit
			end if
		end if
		ll_errore = -1
		exit
	end if
	if astr_listini_ven_dim.dim_1 > ld_limite_dimensione_1 and astr_listini_ven_dim.dim_1 <= ld_limite_prec then
		ld_limite_dimensione_1 = ld_limite_prec
		exit
	else
		ld_limite_prec = ld_limite_dimensione_1
	end if
loop
close cu_dimensioni;

if ll_errore = -1 then
	return ll_errore
end if

// ----------------------- giro seconda dimensione --------------------------------------

ld_limite_prec = 9999999
ls_sql = " select limite_dimensione_1, limite_dimensione_2, flag_sconto_mag_prezzo, variazione " + &
		   " variazione  " + &
         " from  listini_vendite_dimensioni  " + &
			" where  cod_azienda = '" + s_cs_xx_cod_azienda + "' and  " + &
		 	" cod_tipo_listino_prodotto = '" + fs_cod_tipo_listino_prodotto + "' and    " + &
		   " cod_valuta      = '" + fs_cod_valuta + "' and    " + &
		 	" data_inizio_val = '" + string(fdt_data_inizio_val,s_cs_xx_db_funzioni_formato_data) + "' and " + &
		 	" progressivo     = "+ string(fl_progressivo) +" and    " + &
		 	" prog_range     = "+ string(ll_range) +" and    " + &
		   " num_scaglione   = "+ string(fl_num_scaglione) + " and " + &
		 	" limite_dimensione_1     = " + f_decimal_to_string(ld_limite_dimensione_1) + &
			" order by limite_dimensione_2 ASC"



prepare sqlsa from :ls_sql;

open dynamic cu_dimensioni;

do while true
   fetch cu_dimensioni into :ld_limite_dimensione_1, :ld_limite_dimensione_2, :ls_flag_sconto_mag_prezzo, :ld_variazione;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then 
		ll_errore = -1
		exit
	end if
	
	if astr_listini_ven_dim.dim_2 <= ld_limite_dimensione_2 then
		exit
	else
		ld_limite_prec = ld_limite_dimensione_2
	end if
loop
close cu_dimensioni;

ls_des_variazione = "Applicata Condizione Dimensionale. " +  f_decimal_to_string(ld_limite_dimensione_1) + "x" + f_decimal_to_string(ld_limite_dimensione_2)
if len(ls_des_listino_range) > 0 then ls_des_variazione += " Identificato range dimensionale condizionale " + ls_des_listino_range

choose case ls_flag_sconto_mag_prezzo
	case "S" 
		ls_des_variazione += " Sconto " + f_decimal_to_string(ld_variazione) + "%"
	case "M" 
		ls_des_variazione += " Maggiorazione " + f_decimal_to_string(ld_variazione) + "%"
	case "A" 
		ls_des_variazione += " Aumento " + f_decimal_to_string(ld_variazione)
	case "D" 
		ls_des_variazione += " Diminuzione " + f_decimal_to_string(ld_variazione)
	case "P" 
		ls_des_variazione += " Sostituzione di prezzo " + f_decimal_to_string(ld_variazione)
end choose	
wf_listino_log(ls_des_variazione)



fs_flag_sconto_mag_prezzo = ls_flag_sconto_mag_prezzo
fd_variazione = ld_variazione
return 0
end function

public function integer wf_ricerca_condizioni_clienti (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_riferimento_val, string fs_cod_cliente, string fs_cod_prodotto, str_listini_ven_dim astr_listini_ven_dim, double fd_quan_scaglione, double fd_val_scaglione, ref double fd_sconto[], ref double fd_maggiorazione[], ref double fd_variazione[], boolean fb_cerca_dimensioni, ref double fd_minimo_fatt_altezza, ref double fd_minimo_fatt_larghezza, ref double fd_minimo_fatt_profondita, ref double fd_minimo_fatt_superficie, ref double fd_minimo_fatt_volume, ref string as_errore);// --------------------------------------------------------------------------------------------------------
//	FUNZIONE DI RICERCA PREZZI NEL LISTINI GLOBALI AZIENDA
//
//
//	AUTORE:ENRICO MENEGOTTO   01/01/1999
//		 SUCCESSIVE MODFICHE :
//		 ----------------------
//		 MODIFICATO PER PROGETTOTENDA; aggiunto ricerca per dimensioni prodotti 01/11/1999
//			AGGIUNTO OUTPUT VALORE MINIMI DI FATTURAZIONE 30/11/1999
//
//
//
// DESCRIZIONE INPUT/OUTPUT
//	NOME VARIABILE 						TIPO_DATO 			Input/Output
//	-----------------------------------------------------------
//	fs_cod_tipo_listino_prodotto		string				I
//	fs_cod_valuta							string				I
//	fdt_data_riferimento_val			datetime				I
//	fs_cod_cliente							string				I
//	fs_cod_prodotto						string				I
//	fd_dimensione_1						double				I
//	fd_dimensione_2						double				I
//	fd_quan_scaglione						double				I
//	fd_val_scaglione						double				I
//	fl_sconti[]								double				O
//	fl_maggiorazioni[]					double				O
//	fd_variazioni							double				O
//	fb_cerca_dimensioni					boolean				I
//	fd_minimo_fatt_altezza				double				O
//	fd_minimo_fatt_larghezza			double				O
//	fd_minimo_fatt_profondita			double				O
//	fd_minimo_fatt_superficie			double				O
//	fd_minimo_fatt_volume				double				O
//	
// ------------------------------------------------------------------------------------------------------------
//
// nota importante: il minimo fatturabile per la superficie è anche il minimo fatturabile delle quantità; quindi
// 						 nel caso sia caricato con qualche valore > 0 il suo valore va a sostituire quello passato 
//							 in fd_quan_scaglione; nella nota della maschera di dettaglio dovrebbe comparire una scitta
//							 del tipo "applicato il minimo fatturabile per la quantità".
//
// -------------------------------------------------------------------------------------------------------------

boolean lb_condizione_standard=false
string ls_cod_cat_mer, ls_cod_categoria, ls_filtro, ls_messaggio, ls_sql, &
	  ls_flag_sconto_mag_prezzo, ls_flag_origine_prezzo, ls_flag_sconto_a_parte, &
	  ls_flag_tipo_scaglioni, ls_flag, &
  	  ls_flag_sconto_mag_prezzo_1, ls_flag_sconto_mag_prezzo_2, ls_flag_sconto_mag_prezzo_3, ls_flag_sconto_mag_prezzo_4, ls_flag_sconto_mag_prezzo_5, &
	  ls_flag_origine_prezzo_1, ls_flag_origine_prezzo_2, ls_flag_origine_prezzo_3, ls_flag_origine_prezzo_4, ls_flag_origine_prezzo_5, &
	  ls_cod_prodotto, ls_cod_cliente, ls_des_listino_vendite, ls_flag_ricerca_cli_prod, ls_flag_ricerca_cat_cli_prod, ls_flag_ricerca_cli_cat_mer, &
	  ls_flag_ricerca_cliente, ls_flag_ricerca_cat_cli, ls_flag_ricerca_cat_mer, ls_flag_ricerca_prodotto, ls_flag_ricerca_versione_diba
 long ll_num_righe, ll_cont, ll_livello_criterio, ll_i,  &
    	ll_cont_sconto, ll_cont_maggiorazione, ll_cont_variazione, ll_1, ll_progressivo
double ld_variazione_precedente, ld_prezzo_vendita_precedente, ld_prezzo, ld_variazione, ld_minimo_quan_fatturabile, &
	    ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5, ld_var, ll_sconto, &
		 ll_maggiorazione, ld_minimo_fatt_altezza, ld_minimo_fatt_larghezza, ld_minimo_fatt_profondita, &
		 ld_minimo_fatt_superficie, ld_minimo_fatt_volume
dec{4}  ld_scaglione_1, ld_scaglione_2, ld_scaglione_3, ld_scaglione_4, ld_scaglione_5, ld_quan_scaglione
datetime ldt_data_inizio_val, ldt_data_inizio_val_precedente

ld_quan_scaglione = dec( fd_quan_scaglione)

SELECT flag_ricerca_cli_prod, flag_ricerca_cat_cli_prod, flag_ricerca_cli_cat_mer, flag_ricerca_cliente, flag_ricerca_cat_cli, flag_ricerca_cat_mer, flag_ricerca_prodotto, flag_ricerca_versione_diba
  INTO :ls_flag_ricerca_cli_prod, :ls_flag_ricerca_cat_cli_prod, :ls_flag_ricerca_cli_cat_mer, :ls_flag_ricerca_cliente, :ls_flag_ricerca_cat_cli, :ls_flag_ricerca_cat_mer, :ls_flag_ricerca_prodotto, :ls_flag_ricerca_versione_diba  
  FROM con_vendite  
 WHERE cod_azienda = :s_cs_xx_cod_azienda  ;
if sqlca.sqlcode <> 0 then
	as_errore = "Parametri Vendite: Errore in lettura tabella parametri vendite"
	return -1
end if

// ------------- valuto esistenza condizioni cliente / prodotto --------------------------------------

if ls_flag_ricerca_cli_prod = "S" then
	select count(*)
	into  :ll_cont
	from  listini_vendite
	where cod_azienda      = :s_cs_xx_cod_azienda and
			cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
			cod_valuta       = :fs_cod_valuta and
			data_inizio_val <= :fdt_data_riferimento_val and
			cod_cliente      = :fs_cod_cliente and
			cod_prodotto     = :fs_cod_prodotto;
	choose case sqlca.sqlcode
		case 0
			if ll_cont > 0 then
				ll_livello_criterio = 1
				wf_listino_log( "Applico condizione di listino Cliente/Prodotto" )
				goto ricerca
			else			
			// non esistono condizioni: successivo criterio
			end if
		case 100
			// non esistono condizioni: successivo criterio
		case else
			// errore
	end choose
end if

// ------------- valuto esistenza condizioni cat. cliente / prodotto ----------------------------------
if ls_flag_ricerca_cat_cli_prod = "S" then
	select cod_categoria
	into   :ls_cod_categoria
	from   anag_clienti
	where  cod_azienda = :s_cs_xx_cod_azienda and
			 cod_cliente = :fs_cod_cliente;
	if sqlca.sqlcode = 0 and not(isnull(ls_cod_categoria)) then
		select count(*)
		into  :ll_cont
		from  listini_vendite
		where cod_azienda      = :s_cs_xx_cod_azienda and
				cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
				cod_valuta       = :fs_cod_valuta and
				data_inizio_val <= :fdt_data_riferimento_val and
				cod_categoria    = :ls_cod_categoria and
				cod_prodotto     = :fs_cod_prodotto;
		choose case sqlca.sqlcode
			case 0
				if ll_cont > 0 then
					ll_livello_criterio = 2
					wf_listino_log( "Applico condizione di listino Categoria Cliente/Prodotto" )
					goto ricerca
				else			
					// non esistono condizioni: successivo criterio
				end if
			case 100
				// non esistono condizioni: successivo criterio
			case else
				// errore
		end choose
	end if
end if
// ------------- valuto esistenza condizioni cliente / categoria prodotto -----------------------------

if ls_flag_ricerca_cli_cat_mer = "S" then
	select cod_cat_mer
	into   :ls_cod_cat_mer
	from   anag_prodotti
	where  cod_azienda  = :s_cs_xx_cod_azienda and
			 cod_prodotto = :fs_cod_prodotto;
	if sqlca.sqlcode = 0 and not(isnull(ls_cod_cat_mer)) then
		select count(*)
		into  :ll_cont
		from  listini_vendite
		where cod_azienda      = :s_cs_xx_cod_azienda and
				cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
				cod_valuta       = :fs_cod_valuta and
				data_inizio_val <= :fdt_data_riferimento_val and
				cod_cliente      = :fs_cod_cliente and
				cod_cat_mer      = :ls_cod_cat_mer;
		choose case sqlca.sqlcode
			case 0
				if ll_cont > 0 then
					ll_livello_criterio = 3
					wf_listino_log( "Applico condizione di listino Cliente/Categoria di Prodotto" )
					goto ricerca
				else			
					// non esistono condizioni: successivo criterio
				end if
			case 100
				// non esistono condizioni: successivo criterio
			case else
				// errore
		end choose
	end if
end if

// ------------- valuto esistenza condizioni cliente ------------------------------------------------

if ls_flag_ricerca_cliente = "S" then
	select count(*)
	into  :ll_cont
	from  listini_vendite
	where cod_azienda      = :s_cs_xx_cod_azienda and
			cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
			cod_valuta       = :fs_cod_valuta and
			data_inizio_val <= :fdt_data_riferimento_val and
			cod_cliente      = :fs_cod_cliente and
			cod_cat_mer      is null and 
			cod_prodotto     is null and
			cod_categoria    is null;
	choose case sqlca.sqlcode
		case 0
			if ll_cont > 0 then
				ll_livello_criterio = 4
				wf_listino_log( "Applico condizione di listino Cliente" )
				goto ricerca
			else			
				// non esistono condizioni: successivo criterio
			end if
		case 100
			// non esistono condizioni: successivo criterio
		case else
			// errore
	end choose
end if

// ------------- valuto esistenza condizioni categoria cliente -------------------------------------------

if ls_flag_ricerca_cat_cli = "S" then
	select count(*)
	into  :ll_cont
	from  listini_vendite
	where cod_azienda      = :s_cs_xx_cod_azienda and
			cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
			cod_valuta       = :fs_cod_valuta and
			data_inizio_val <= :fdt_data_riferimento_val and
			cod_cliente      is null and
			cod_cat_mer      is null and 
			cod_prodotto     is null and
			cod_categoria    = :ls_cod_categoria;
	choose case sqlca.sqlcode
		case 0
			if ll_cont > 0 then
				ll_livello_criterio = 5
				wf_listino_log( "Applico condizione di listino Categoria Cliente" )
				goto ricerca
			else			
				// non esistono condizioni: successivo criterio
			end if
		case 100
			// non esistono condizioni: successivo criterio
		case else
			// errore
	end choose
end if

// ------------- valuto esistenza condizioni prodotto ------------------------------------------------
if ls_flag_ricerca_prodotto = "S" then
	select count(*)
	into  :ll_cont
	from  listini_vendite
	where cod_azienda      = :s_cs_xx_cod_azienda and
			cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
			cod_valuta       = :fs_cod_valuta and
			data_inizio_val <= :fdt_data_riferimento_val and
			cod_cliente      is null and
			cod_cat_mer      is null and 
			cod_prodotto     = :fs_cod_prodotto and
			cod_categoria    is null;
	choose case sqlca.sqlcode
		case 0
			if ll_cont > 0 then
				ll_livello_criterio = 7
				wf_listino_log( "Applico condizione di listino sul Prodotto" )
				goto ricerca
			else			
				// se a questo punto non c'è nessun listino caricato propongo le condizioni presenti nella
				// anagrafica del prodotto 
				ld_prezzo = 0
				ll_sconto = 0
				select anag_prodotti.prezzo_vendita,   
						 anag_prodotti.sconto  
				into  :ld_prezzo,   
						:ll_sconto  
				from  anag_prodotti  
				where ( anag_prodotti.cod_azienda = :s_cs_xx_cod_azienda ) AND  
						( anag_prodotti.cod_prodotto = :fs_cod_prodotto )   ;
				if sqlca.sqlcode = 0 then
					fd_variazione[1] = ld_prezzo
					fd_sconto[1] = ll_sconto
				end if
				return 0
			end if
		case 100
			// non esistono condizioni: successivo criterio
		case else
			// errore
	end choose
end if

// ------------- valuto esistenza condizioni categoria prodotto --------------------------------------
if ls_flag_ricerca_cat_mer = "S" then
	select count(*)
	into  :ll_cont
	from  listini_vendite
	where cod_azienda      = :s_cs_xx_cod_azienda and
			cod_tipo_listino_prodotto = :fs_cod_tipo_listino_prodotto and
			cod_valuta       = :fs_cod_valuta and
			data_inizio_val <= :fdt_data_riferimento_val and
			cod_cliente      is null and
			cod_cat_mer      = :ls_cod_cat_mer and 
			cod_prodotto     is null and 
			cod_categoria    is null;
	choose case sqlca.sqlcode
		case 0
			if ll_cont > 0 then
				ll_livello_criterio = 6
				wf_listino_log( "Applico condizione di listino sulla Categoria del Prodotto" )
				goto ricerca
			else			
				// non esistono condizioni: successivo criterio
			end if
		case 100
			return 0
		case else
			// errore
	end choose
end if



// ------------------------------- ricerca condizioni -----------------------------------
ricerca:

// controllo inserito per specifica PTENDA semplificazione listini.
// se la condizione è standard (cioè solo per prodotto) è stata integrata una nuova tabella che permette
// di digitate le condizioni di produzione insieme alle condizioni standard (varianti).

if ll_livello_criterio = 7 then
	lb_condizione_standard = true
end if

declare cu_condizioni_cliente dynamic cursor for sqlsa;
ls_sql = "  SELECT data_inizio_val, progressivo, " + &
         		"  cod_cat_mer, cod_prodotto, cod_categoria, cod_cliente, des_listino_vendite, minimo_fatt_altezza, " + &
			"  minimo_fatt_larghezza, minimo_fatt_profondita, minimo_fatt_superficie, minimo_fatt_volume, flag_sconto_a_parte, " + &
			"  flag_tipo_scaglioni, scaglione_1, scaglione_2, scaglione_3, scaglione_4, scaglione_5, " + &
			"  flag_sconto_mag_prezzo_1, flag_sconto_mag_prezzo_2, flag_sconto_mag_prezzo_3, flag_sconto_mag_prezzo_4, flag_sconto_mag_prezzo_5, " + &
			"  variazione_1, variazione_2, variazione_3, variazione_4, variazione_5, " + &
			"  flag_origine_prezzo_1, flag_origine_prezzo_2, flag_origine_prezzo_3, flag_origine_prezzo_4, flag_origine_prezzo_5  " + &
			"  FROM listini_vendite  " + &
			"  WHERE ( cod_azienda = '" + s_cs_xx_cod_azienda + "' ) AND  " + &
			"  ( cod_tipo_listino_prodotto = '" + fs_cod_tipo_listino_prodotto + "') AND  " + &
			"  ( cod_valuta = '" + fs_cod_valuta + "') AND   " + &
         		"  ( data_inizio_val <= '" + string(fdt_data_riferimento_val, s_cs_xx_db_funzioni_formato_data) + "')  "
			ls_filtro = ""
			choose case ll_livello_criterio
				case 1
					ib_prezzo_speciale = true
					ls_filtro = "cod_cliente = '" + fs_cod_cliente + "' and cod_prodotto = '" + fs_cod_prodotto + "'"
				case 2
					ls_filtro = "cod_categoria = '" + ls_cod_categoria + "' and cod_prodotto = '" + fs_cod_prodotto + "'"
				case 3
					ls_filtro = "cod_cliente = '" + fs_cod_cliente + "' and cod_cat_mer = '" + ls_cod_cat_mer + "'"
				case 4
					ls_filtro = "cod_cliente = '" + fs_cod_cliente + "' and cod_prodotto is null and cod_cat_mer is null and cod_categoria is null"
				case 5
					ls_filtro = "cod_categoria = '" + ls_cod_categoria + "' and cod_prodotto is null and cod_cat_mer is null and cod_cliente is null"
				case 6
					ls_filtro = "cod_cat_mer = '" + ls_cod_cat_mer + "' and cod_categoria is null and cod_prodotto is null and cod_cliente is null"
				case 7
					ls_filtro = "cod_prodotto = '" + fs_cod_prodotto + "' and cod_categoria is null and cod_cat_mer is null and cod_cliente is null"
				case else
					ls_filtro = "cod_prodotto is null and cod_categoria is null and cod_cat_mer is null and cod_cliente is null"
			end choose

			if len(ls_filtro) > 0 then
				ls_sql = ls_sql + " and " + ls_filtro
			end if
			
			ls_sql = ls_sql +	"  ORDER BY cod_azienda ASC,   " + &  
								"  cod_tipo_listino_prodotto ASC,   " + &  
								"  cod_valuta ASC, " + &
								"  data_inizio_val DESC, " + &
								"  progressivo ASC   "  
								
guo_functions.uof_log("wf_ricerca_condizioni_clienti. SQL cursore:" + guo_functions.uof_format(ls_sql))

prepare sqlsa from :ls_sql;

open dynamic cu_condizioni_cliente;

ld_variazione_precedente = 0
ld_prezzo_vendita_precedente = 0
ll_cont_sconto = 0
ll_cont_maggiorazione = 0
ll_cont_variazione = 0
ldt_data_inizio_val_precedente = datetime(date("01/01/1900"), 00:00:00)
ll_i = 0
do while true
	
	fetch cu_condizioni_cliente into :ldt_data_inizio_val, :ll_progressivo, :ls_cod_cat_mer, :ls_cod_prodotto, :ls_cod_categoria, :ls_cod_cliente, :ls_des_listino_vendite, :ld_minimo_fatt_altezza, :ld_minimo_fatt_larghezza, :ld_minimo_fatt_profondita, :ld_minimo_fatt_superficie, :ld_minimo_fatt_volume, :ls_flag_sconto_a_parte, :ls_flag_tipo_scaglioni, :ld_scaglione_1, :ld_scaglione_2, :ld_scaglione_3, :ld_scaglione_4, :ld_scaglione_5, :ls_flag_sconto_mag_prezzo_1, :ls_flag_sconto_mag_prezzo_2, :ls_flag_sconto_mag_prezzo_3, :ls_flag_sconto_mag_prezzo_4, :ls_flag_sconto_mag_prezzo_5, :ld_variazione_1, :ld_variazione_2, :ld_variazione_3, :ld_variazione_4, :ld_variazione_5, :ls_flag_origine_prezzo_1, :ls_flag_origine_prezzo_2, :ls_flag_origine_prezzo_3, :ls_flag_origine_prezzo_4, :ls_flag_origine_prezzo_5;
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
	ll_i ++
	if ll_i > 1 then
		if ldt_data_inizio_val_precedente <> ldt_data_inizio_val then exit
	end if	
	
	fd_minimo_fatt_altezza    = ld_minimo_fatt_altezza
	fd_minimo_fatt_larghezza  = ld_minimo_fatt_larghezza
	fd_minimo_fatt_profondita = ld_minimo_fatt_profondita
	fd_minimo_fatt_superficie = ld_minimo_fatt_superficie
	fd_minimo_fatt_volume     = ld_minimo_fatt_volume
	
	if not isnull(fd_minimo_fatt_superficie) and fd_minimo_fatt_superficie > 0 then ld_minimo_quan_fatturabile = fd_minimo_fatt_superficie

	if ls_flag_tipo_scaglioni = "Q" then
		guo_functions.uof_log("wf_ricerca_condizioni_clienti. Ricerca per scaglioni di q.ta")
		
		choose case ld_quan_scaglione
			case is <= ld_scaglione_1
				guo_functions.uof_log("wf_ricerca_condizioni_clienti. scaglione q.ta 1")
				
				wf_listino_log( "Applico condizione scaglione Qta " + f_decimal_to_string(ld_scaglione_1))
				if ls_flag_origine_prezzo_1 = "L" then
					for ll_1 =	1 to upperbound(fd_sconto)
						fd_sconto[ll_1] = 0
					next
					for ll_1 =	1 to upperbound(fd_maggiorazione)
						fd_maggiorazione[ll_1] = 0
					next
					for ll_1 =	1 to upperbound(fd_variazione)
						fd_variazione[ll_1] = 0
					next

					 wf_ricerca_condizioni_prodotto (fs_cod_tipo_listino_prodotto, &
												fs_cod_valuta, &
												 fdt_data_riferimento_val, &
												 fs_cod_prodotto, &
												 astr_listini_ven_dim.dim_1, &
												 astr_listini_ven_dim.dim_2, &
												 fd_quan_scaglione, &
												 fd_val_scaglione, &
												 lb_condizione_standard, &
												 fs_cod_cliente, &
												 ref fd_sconto, &
												 ref fd_maggiorazione, &
												 ref fd_variazione[], &
												 false)				
					ll_cont_sconto = 0
					for ll_1 = 1 to upperbound(fd_sconto)
						if fd_sconto[ll_1] = 0 then exit 
						ll_cont_sconto = ll_1
					next
					ll_cont_maggiorazione = upperbound(fd_maggiorazione)
					ll_cont_variazione = upperbound(fd_variazione)
					if ll_cont_variazione > 0 then ld_prezzo_vendita_precedente = fd_variazione[ll_cont_variazione]
					
				end if
				// ---------- ricerca per dimensioni ------------------------------
				guo_functions.uof_log("wf_ricerca_condizioni_clienti. Ricerca per dimensioni. ")
				if wf_ricerca_cond_dimensioni(fs_cod_tipo_listino_prodotto, fs_cod_valuta, ldt_data_inizio_val, ll_progressivo, 1, astr_listini_ven_dim , ls_flag, ld_var) = 0 then
					guo_functions.uof_log("wf_ricerca_condizioni_clienti. Trovato condizioni per dimensioni. Flag=" + guo_functions.uof_format(ls_flag) + " Var=" + guo_functions.uof_format(ld_var))
					ls_flag_sconto_mag_prezzo_1 = ls_flag
					ld_variazione_1 = ld_var
				end if
				// -----------------------------------------------------------------
				ls_flag_sconto_mag_prezzo = ls_flag_sconto_mag_prezzo_1
				ld_variazione = ld_variazione_1
				ls_flag_origine_prezzo = ls_flag_origine_prezzo_1
				ls_flag_sconto_a_parte = ls_flag_sconto_a_parte
				s_cs_xx_listino_db_data_inizio_val = ldt_data_inizio_val
				s_cs_xx_listino_db_progressivo = ll_progressivo
				guo_functions.uof_log("wf_ricerca_condizioni_clienti. Composizione della condizione. ")
				wf_componi_prezzo ( fs_cod_prodotto, &
										 ls_flag_sconto_mag_prezzo, &
										 ld_variazione, &
										 ld_variazione_precedente, &
										 ld_prezzo_vendita_precedente, &
										 ls_flag_origine_prezzo, &
										 ls_flag_sconto_a_parte, &
										 lb_condizione_standard, &
										 fs_cod_cliente, &
										 ll_sconto, &
										 ll_maggiorazione, &
										 ld_prezzo, &
										 ls_messaggio )
				ld_variazione_precedente = ld_variazione_1
				ld_prezzo_vendita_precedente = ld_prezzo
				if ll_sconto > 0 and not isnull(ll_sconto) then
					ll_cont_sconto = ll_cont_sconto + 1
					fd_sconto[ll_cont_sconto] = ll_sconto
					guo_functions.uof_log("wf_ricerca_condizioni_clienti. Sconto =" + guo_functions.uof_format(fd_sconto[ll_cont_sconto]))
				end if
				if ll_maggiorazione > 0 and not isnull(ll_maggiorazione) then
					ll_cont_maggiorazione = ll_cont_maggiorazione + 1
					fd_maggiorazione[ll_cont_maggiorazione] = ll_maggiorazione
					guo_functions.uof_log("wf_ricerca_condizioni_clienti. Maggiorazione =" + guo_functions.uof_format(fd_maggiorazione[ll_cont_maggiorazione]))
				end if
				if ld_prezzo > 0 and not isnull(ld_prezzo) then
					ll_cont_variazione = ll_cont_variazione + 1
					fd_variazione[ll_cont_variazione] = ld_prezzo
					guo_functions.uof_log("wf_ricerca_condizioni_clienti. Prezzo =" + guo_functions.uof_format(fd_variazione[ll_cont_variazione]))
				end if
				
			case is <= ld_scaglione_2
				guo_functions.uof_log("wf_ricerca_condizioni_clienti. scaglione q.ta 2")
				
				wf_listino_log( "Applico condizione scaglione Qta " + f_decimal_to_string(ld_scaglione_1))
				if ls_flag_origine_prezzo_2 = "L" then
					for ll_1 =	1 to upperbound(fd_sconto)
						fd_sconto[ll_1] = 0
					next
					for ll_1 =	1 to upperbound(fd_maggiorazione)
						fd_maggiorazione[ll_1] = 0
					next
					for ll_1 =	1 to upperbound(fd_variazione)
						fd_variazione[ll_1] = 0
					next

               wf_ricerca_condizioni_prodotto (fs_cod_tipo_listino_prodotto, &
					                               fs_cod_valuta, &
															 fdt_data_riferimento_val, &
															 fs_cod_prodotto, &
															 astr_listini_ven_dim.dim_1, &
															 astr_listini_ven_dim.dim_2, &
															 fd_quan_scaglione, &
															 fd_val_scaglione, &
															 lb_condizione_standard, &
															 fs_cod_cliente, &
															 ref fd_sconto, &
															 ref fd_maggiorazione, &
															 ref fd_variazione[], &
															 false)				
					ll_cont_sconto = 0
					for ll_1 = 1 to upperbound(fd_sconto)
						if fd_sconto[ll_1] = 0 then exit 
						ll_cont_sconto = ll_1
					next
					ll_cont_maggiorazione = upperbound(fd_maggiorazione)
					ll_cont_variazione = upperbound(fd_variazione)
					if ll_cont_variazione > 0 then ld_prezzo_vendita_precedente = fd_variazione[ll_cont_variazione]
				end if
				ls_flag_sconto_mag_prezzo = ls_flag_sconto_mag_prezzo_2
				ld_variazione = ld_variazione_2
				ls_flag_origine_prezzo = ls_flag_origine_prezzo_2
				ls_flag_sconto_a_parte = ls_flag_sconto_a_parte
				s_cs_xx_listino_db_data_inizio_val = ldt_data_inizio_val
				s_cs_xx_listino_db_progressivo = ll_progressivo
				wf_componi_prezzo ( fs_cod_prodotto, &
										 ls_flag_sconto_mag_prezzo, &
										 ld_variazione, &
										 ld_variazione_precedente, &
										 ld_prezzo_vendita_precedente, &
										 ls_flag_origine_prezzo, &
										 ls_flag_sconto_a_parte, &
										 lb_condizione_standard, &
										 fs_cod_cliente, &
										 ll_sconto, &
										 ll_maggiorazione, &
										 ld_prezzo, &
										 ls_messaggio )
				ld_variazione_precedente = ld_variazione_2
				ld_prezzo_vendita_precedente = ld_prezzo
				if ll_sconto > 0 and not isnull(ll_sconto) then
					ll_cont_sconto = ll_cont_sconto + 1
					fd_sconto[ll_cont_sconto] = ll_sconto
				end if
				if ll_maggiorazione > 0 and not isnull(ll_maggiorazione) then
					ll_cont_maggiorazione = ll_cont_maggiorazione + 1
					fd_maggiorazione[ll_cont_maggiorazione] = ll_maggiorazione
				end if
				if ld_prezzo > 0 and not isnull(ld_prezzo) then
					ll_cont_variazione = ll_cont_variazione + 1
					fd_variazione[ll_cont_variazione] = ld_prezzo
				end if
				
				
			case is <= ld_scaglione_3
				guo_functions.uof_log("wf_ricerca_condizioni_clienti. scaglione q.ta 3")
				
				wf_listino_log( "Applico condizione scaglione Qta " + f_decimal_to_string(ld_scaglione_1))
				if ls_flag_origine_prezzo_3 = "L" then
					for ll_1 =	1 to upperbound(fd_sconto)
						fd_sconto[ll_1] = 0
					next
					for ll_1 =	1 to upperbound(fd_maggiorazione)
						fd_maggiorazione[ll_1] = 0
					next
					for ll_1 =	1 to upperbound(fd_variazione)
						fd_variazione[ll_1] = 0
					next

               wf_ricerca_condizioni_prodotto (fs_cod_tipo_listino_prodotto, &
					                               fs_cod_valuta, &
															 fdt_data_riferimento_val, &
															 fs_cod_prodotto, &
															 astr_listini_ven_dim.dim_1, &
															 astr_listini_ven_dim.dim_2, &
															 fd_quan_scaglione, &
															 fd_val_scaglione, &
															 lb_condizione_standard, &
															 fs_cod_cliente, &
															 ref fd_sconto, &
															 ref fd_maggiorazione, &
															 ref fd_variazione[], &
															 false)				
					ll_cont_sconto = 0
					for ll_1 = 1 to upperbound(fd_sconto)
						if fd_sconto[ll_1] = 0 then exit 
						ll_cont_sconto = ll_1
					next
					ll_cont_maggiorazione = upperbound(fd_maggiorazione)
					ll_cont_variazione = upperbound(fd_variazione)
					if ll_cont_variazione > 0 then ld_prezzo_vendita_precedente = fd_variazione[ll_cont_variazione]
				end if
				ls_flag_sconto_mag_prezzo = ls_flag_sconto_mag_prezzo_3
				ld_variazione = ld_variazione_3
				ls_flag_origine_prezzo = ls_flag_origine_prezzo_3
				ls_flag_sconto_a_parte = ls_flag_sconto_a_parte
				s_cs_xx_listino_db_data_inizio_val = ldt_data_inizio_val
				s_cs_xx_listino_db_progressivo = ll_progressivo
				wf_componi_prezzo ( fs_cod_prodotto, &
										 ls_flag_sconto_mag_prezzo, &
										 ld_variazione, &
										 ld_variazione_precedente, &
										 ld_prezzo_vendita_precedente, &
										 ls_flag_origine_prezzo, &
										 ls_flag_sconto_a_parte, &
										 lb_condizione_standard, &
										 fs_cod_cliente, &
										 ll_sconto, &
										 ll_maggiorazione, &
										 ld_prezzo, &
										 ls_messaggio )
				ld_variazione_precedente = ld_variazione_3
				ld_prezzo_vendita_precedente = ld_prezzo
				if ll_sconto > 0 and not isnull(ll_sconto) then
					ll_cont_sconto = ll_cont_sconto + 1
					fd_sconto[ll_cont_sconto] = ll_sconto
				end if
				if ll_maggiorazione > 0 and not isnull(ll_maggiorazione) then
					ll_cont_maggiorazione = ll_cont_maggiorazione + 1
					fd_maggiorazione[ll_cont_maggiorazione] = ll_maggiorazione
				end if
				if ld_prezzo > 0 and not isnull(ld_prezzo) then
					ll_cont_variazione = ll_cont_variazione + 1
					fd_variazione[ll_cont_variazione] = ld_prezzo
				end if
			case is <= ld_scaglione_4
				guo_functions.uof_log("wf_ricerca_condizioni_clienti. scaglione q.ta 4")
				
				wf_listino_log( "Applico condizione scaglione Qta " + f_decimal_to_string(ld_scaglione_1))
				if ls_flag_origine_prezzo_4 = "L" then
					for ll_1 =	1 to upperbound(fd_sconto)
						fd_sconto[ll_1] = 0
					next
					for ll_1 =	1 to upperbound(fd_maggiorazione)
						fd_maggiorazione[ll_1] = 0
					next
					for ll_1 =	1 to upperbound(fd_variazione)
						fd_variazione[ll_1] = 0
					next

               wf_ricerca_condizioni_prodotto (fs_cod_tipo_listino_prodotto, &
					                               fs_cod_valuta, &
															 fdt_data_riferimento_val, &
															 fs_cod_prodotto, &
															 astr_listini_ven_dim.dim_1, &
															 astr_listini_ven_dim.dim_2, &
															 fd_quan_scaglione, &
															 fd_val_scaglione, &
															 lb_condizione_standard, &
															 fs_cod_cliente, &
															 ref fd_sconto, &
															 ref fd_maggiorazione, &
															 ref fd_variazione[], &
															 false)				
					ll_cont_sconto = 0
					for ll_1 = 1 to upperbound(fd_sconto)
						if fd_sconto[ll_1] = 0 then exit 
						ll_cont_sconto = ll_1
					next
					ll_cont_maggiorazione = upperbound(fd_maggiorazione)
					ll_cont_variazione = upperbound(fd_variazione)
					if ll_cont_variazione > 0 then ld_prezzo_vendita_precedente = fd_variazione[ll_cont_variazione]
				end if
				ls_flag_sconto_mag_prezzo = ls_flag_sconto_mag_prezzo_4
				ld_variazione = ld_variazione_4
				ls_flag_origine_prezzo = ls_flag_origine_prezzo_4
				ls_flag_sconto_a_parte = ls_flag_sconto_a_parte
				s_cs_xx_listino_db_data_inizio_val = ldt_data_inizio_val
				s_cs_xx_listino_db_progressivo = ll_progressivo
				wf_componi_prezzo ( fs_cod_prodotto, &
										 ls_flag_sconto_mag_prezzo, &
										 ld_variazione, &
										 ld_variazione_precedente, &
										 ld_prezzo_vendita_precedente, &
										 ls_flag_origine_prezzo, &
										 ls_flag_sconto_a_parte, &
										 lb_condizione_standard, &
										 fs_cod_cliente, &
										 ll_sconto, &
										 ll_maggiorazione, &
										 ld_prezzo, &
										 ls_messaggio )
				ld_variazione_precedente = ld_variazione_4
				ld_prezzo_vendita_precedente = ld_prezzo
				if ll_sconto > 0 and not isnull(ll_sconto) then
					ll_cont_sconto = ll_cont_sconto + 1
					fd_sconto[ll_cont_sconto] = ll_sconto
				end if
				if ll_maggiorazione > 0 and not isnull(ll_maggiorazione) then
					ll_cont_maggiorazione = ll_cont_maggiorazione + 1
					fd_maggiorazione[ll_cont_maggiorazione] = ll_maggiorazione
				end if
				if ld_prezzo > 0 and not isnull(ld_prezzo) then
					ll_cont_variazione = ll_cont_variazione + 1
					fd_variazione[ll_cont_variazione] = ld_prezzo
				end if
				
				
			case else
				guo_functions.uof_log("wf_ricerca_condizioni_clienti. scaglione q.ta 5")
				
				wf_listino_log( "Applico condizione scaglione Qta " + f_decimal_to_string(ld_scaglione_1))
				if ls_flag_origine_prezzo_5 = "L" then
					for ll_1 =	1 to upperbound(fd_sconto)
						fd_sconto[ll_1] = 0
					next
					for ll_1 =	1 to upperbound(fd_maggiorazione)
						fd_maggiorazione[ll_1] = 0
					next
					for ll_1 =	1 to upperbound(fd_variazione)
						fd_variazione[ll_1] = 0
					next

               wf_ricerca_condizioni_prodotto (fs_cod_tipo_listino_prodotto, &
					                               fs_cod_valuta, &
															 fdt_data_riferimento_val, &
															 fs_cod_prodotto, &
															 astr_listini_ven_dim.dim_1, &
															 astr_listini_ven_dim.dim_2, &
															 fd_quan_scaglione, &
															 fd_val_scaglione, &
															 lb_condizione_standard, &
															 fs_cod_cliente, &
															 ref fd_sconto, &
															 ref fd_maggiorazione, &
															 ref fd_variazione[], &
															 false)				
					ll_cont_sconto = 0
					for ll_1 = 1 to upperbound(fd_sconto)
						if fd_sconto[ll_1] = 0 then exit 
						ll_cont_sconto = ll_1
					next
					ll_cont_maggiorazione = upperbound(fd_maggiorazione)
					ll_cont_variazione = upperbound(fd_variazione)
					if ll_cont_variazione > 0 then ld_prezzo_vendita_precedente = fd_variazione[ll_cont_variazione]
				end if
				ls_flag_sconto_mag_prezzo = ls_flag_sconto_mag_prezzo_5
				ld_variazione = ld_variazione_5
				ls_flag_origine_prezzo = ls_flag_origine_prezzo_5
				ls_flag_sconto_a_parte = ls_flag_sconto_a_parte
				s_cs_xx_listino_db_data_inizio_val = ldt_data_inizio_val
				s_cs_xx_listino_db_progressivo = ll_progressivo
				wf_componi_prezzo ( fs_cod_prodotto, &
										 ls_flag_sconto_mag_prezzo, &
										 ld_variazione, &
										 ld_variazione_precedente, &
										 ld_prezzo_vendita_precedente, &
										 ls_flag_origine_prezzo, &
										 ls_flag_sconto_a_parte, &
										 lb_condizione_standard, &
										 fs_cod_cliente, &
										 ll_sconto, &
										 ll_maggiorazione, &
										 ld_prezzo, &
										 ls_messaggio )
				ld_variazione_precedente = ld_variazione_5
				ld_prezzo_vendita_precedente = ld_prezzo
				if ll_sconto > 0 and not isnull(ll_sconto) then
					ll_cont_sconto = ll_cont_sconto + 1
					fd_sconto[ll_cont_sconto] = ll_sconto
				end if
				if ll_maggiorazione > 0 and not isnull(ll_maggiorazione) then
					ll_cont_maggiorazione = ll_cont_maggiorazione + 1
					fd_maggiorazione[ll_cont_maggiorazione] = ll_maggiorazione
				end if
				if ld_prezzo > 0 and not isnull(ld_prezzo) then
					ll_cont_variazione = ll_cont_variazione + 1
					fd_variazione[ll_cont_variazione] = ld_prezzo
				end if
		end choose	
	else
		guo_functions.uof_log("wf_ricerca_condizioni_clienti. Ricerca per scaglioni di valore disabilitata")
		// scaglione per valore  non previsto
		
	end if
	ldt_data_inizio_val_precedente = ldt_data_inizio_val
loop
close cu_condizioni_cliente;
if not isnull(ld_minimo_quan_fatturabile) and ld_minimo_quan_fatturabile > 0 then fd_quan_scaglione = ld_minimo_quan_fatturabile
return 0

end function

public function integer wf_listino_log (string as_nota_variazione);long ll_progressivo

choose case s_cs_xx_listino_db_tipo_gestione
	case "varianti_det_ord_ven"
		

	case "varianti_det_off_ven"

		select max(progressivo)
		into	:ll_progressivo
		from	det_off_ven_listino
		where  cod_azienda = :s_cs_xx_cod_azienda and
		anno_registrazione = :s_cs_xx_listino_db_anno_documento and
		num_registrazione = :s_cs_xx_listino_db_num_documento and
		prog_riga_off_ven = :s_cs_xx_listino_db_prog_riga ;
		
		if isnull(ll_progressivo) or ll_progressivo <= 0 then
			ll_progressivo = 1
		else
			ll_progressivo ++
		end if
		
		insert into det_off_ven_listino
		(
				cod_azienda,
				anno_registrazione,
				num_registrazione,
				prog_riga_off_ven,
				progressivo,
				nota_variazione )
		values
		(		:s_cs_xx_cod_azienda,
				:s_cs_xx_listino_db_anno_documento,
				:s_cs_xx_listino_db_num_documento,
				:s_cs_xx_listino_db_prog_riga,
				:ll_progressivo,
				:as_nota_variazione);
		
		ll_progressivo = 111	

end choose


return 0
end function

on uo_condizioni_cliente.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_condizioni_cliente.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;select stringa
into   :is_valuta_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx_cod_azienda and
       cod_parametro = 'LIR';
if sqlca.sqlcode <> 0 then
	//g_mb.messagebox("Ricerca condizioni","Impostare parametro LIR in parametri aziendali")
	//sistemare
	return
end if

select flag_gruppi_sconto
into   :is_flag_gruppi_sconto
from   con_vendite
where  cod_azienda = :s_cs_xx_cod_azienda;
if sqlca.sqlcode <> 0 then
	//g_mb.messagebox("Ricerca condizioni","Impostare la tabella parametri vendite")
	//sistemare
	return
end if

end event

