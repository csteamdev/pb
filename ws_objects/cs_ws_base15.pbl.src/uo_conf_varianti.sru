﻿$PBExportHeader$uo_conf_varianti.sru
forward
global type uo_conf_varianti from nonvisualobject
end type
end forward

global type uo_conf_varianti from nonvisualobject
end type
global uo_conf_varianti uo_conf_varianti

type variables
public:
string s_cs_xx_cod_azienda, s_cs_xx_db_funzioni_formato_data
end variables

forward prototypes
public function integer uof_ins_varianti (string as_cod_prodotto_finito, string as_cod_versione, string as_tipo_gestione, str_varianti astr_varianti[], long al_anno_documento, long al_num_documento, long al_prog_riga_documento, ref string as_errore)
public function integer uof_valorizza_formule_figlio_varianti (long al_anno_documento, long al_num_documento, long al_prog_riga_documento, string as_tipo_gestione, string as_padre, string as_versione_padre, long al_num_sequenza, string as_figlio, string as_versione_figlio, ref string as_cod_variante, ref string as_versione_variante, string as_formula_figlio, string as_formula_versione_figlio, ref string as_errore)
public function integer uof_valorizza_formule (string as_cod_prodotto_padre, string as_cod_versione_padre, string as_tipo_gestione, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, ref string as_errore)
public function integer uof_valorizza_formule_prod_figlio (long al_anno_documento, long al_num_documento, long al_prog_riga_documento, string as_tipo_gestione, string as_padre, string as_versione_padre, long al_num_sequenza, ref string as_figlio, ref string as_versione_figlio, ref long al_sequenza_figlio, string as_formula_figlio, string as_formula_versione_figlio, ref string as_errore)
public function integer uof_carica_mp_automatiche (string as_cod_modello_prodotto, decimal ad_dim_1, decimal ad_dim_2, decimal ad_dim_3, decimal ad_dim_4, decimal ad_altezza_asta, string as_cod_vernicitura_1, string as_cod_vernicitura_2, string as_cod_vernicitura_3, string as_cod_gruppo_variante_specifico, string as_cod_mp_automatica_specifica, ref str_varianti atr_mp_automatiche[], ref string as_errore) throws exception
public function integer uof_trova_mat_prime_varianti (string as_cod_prodotto_finito, string as_cod_versione_finito, long al_num_sequenza, string as_tabella_varianti, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, string as_nome_colonna_quan, decimal ad_quan_utilizzo_prec, ref s_trova_mp_varianti fstr_trova_mp_varianti[], ref string as_errore)
public function integer uof_controlla_figlio_formula (string as_codice)
public function integer uof_ricerca_quan_mp_variante (string as_cod_prodotto, string as_cod_versione, decimal ad_quan_prodotto, string as_cod_gruppo_variante, long al_anno_registrazione, long al_num_registrazione, long al_prog_registrazione, string as_tabella_varianti, ref string as_cod_materia_prima, ref decimal ad_quan_materia_prima, ref string as_errore)
end prototypes

public function integer uof_ins_varianti (string as_cod_prodotto_finito, string as_cod_versione, string as_tipo_gestione, str_varianti astr_varianti[], long al_anno_documento, long al_num_documento, long al_prog_riga_documento, ref string as_errore);/*	Funzione che scorre la distinta base e inserisce la variante di tipo codice prodotto (fs_cod_prodotto_var)
		quando incontra un semilavorato o una materia prima di gruppo variante fs_cod_gruppo_variante

 nome: wf_ins_variante
 tipo: integer
  
	le variabili sono tutte passate attraverso la struttura astr_varianti

	messaggio di errore contenuto nella variabile as_errore

	valore della funzione = -1 si è verificato un errore
									 0 tutto ok elaborazione terminata con successo
									 1 c'è una segnalazione, ma si può andare avanti lo stesso


	Creata il 31-08-98 
	Autore ENRICO MENEGOTTO
   modificata il 31/08/1998
   MODIFICATA IL 20/10/2000 PER UNA SOLA PASSATA DELLA DISTINTA.
	modificata il 31/10/2006 per adeguarla alla nuova gestione distinta base con versioni figli diversi dalla versione padre

*/
string						ls_cod_prodotto_figlio[],ls_test, ls_tabella, ls_progressivo, ls_cod_misura, ls_cod_formula_quan_utilizzo, &
							ls_des_estesa, ls_formula_tempo, ls_sql_del, ls_sql_ins, ls_cod_formula_vpp, ls_cod_gruppo_variante, &
							ls_flag_materia_prima, ls_cod_versione_figlio[], ls_tabella_comp, ls_cod_deposito_produzione, ls_cod_reparto_produzione[], &
							ls_errore, ls_vuoto[], ls_tabella_var_rep, ls_flag_materia_prima_distinta, ls_cod_formula_prod_figlio, ls_cod_formula_vers_figlio, &
							ls_cod_formula_quan_tecnica, ls_nuova_variante, ls_nuova_versione_variante, ls_temp, ls_tipo_ritorno, ls_sql_upd
							
dec{4}					ld_quan_tecnica, ld_quan_utilizzo, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_coef_calcolo

long						ll_conteggio,ll_t,ll_num_sequenza, ll_max, ll_i, ll_y, ll_ordine[],  ll_num_sequenza_new

integer					li_risposta, li_count, li_ret


s_chiave_distinta		lstr_distinta
uo_formule_calcolo	luo_formule_calcolo


guo_functions.uof_log("Ingresso funzione uo_conf_varianti.uof_inf_varianti()")

choose case as_tipo_gestione
		
	case "ORD_VEN"
		ls_tabella = "varianti_det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		ls_tabella_comp = "comp_det_ord_ven"
		ls_tabella_var_rep = "varianti_det_ord_ven_prod"
		
	case "OFF_VEN"
		ls_tabella = "varianti_det_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		ls_tabella_comp = "comp_det_off_ven"
		ls_tabella_var_rep = "varianti_det_ord_ven_prod"
		
end choose

ll_conteggio = 1
ll_max = upperbound(astr_varianti)
if ll_max < 1 then
	return 0
end if


declare righe_distinta_3 cursor for 
  SELECT distinta.cod_prodotto_figlio,   
         distinta.cod_versione_figlio,   
         distinta.num_sequenza,   
         distinta.cod_misura,   
         distinta.quan_tecnica,   
         distinta.quan_utilizzo,   
         distinta.dim_x,   
         distinta.dim_y,   
         distinta.dim_z,   
         distinta.dim_t,   
         distinta.coef_calcolo,   
         distinta.des_estesa,   
         distinta.formula_tempo,   
         distinta.cod_formula_quan_utilizzo,   
		distinta.flag_materia_prima,
         distinta_gruppi_varianti.cod_gruppo_variante,
		distinta.cod_formula_prod_figlio,
		distinta.cod_formula_vers_figlio,
		distinta.cod_formula_quan_tecnica
    FROM distinta,   
         distinta_gruppi_varianti  
   WHERE ( distinta_gruppi_varianti.cod_azienda =* distinta.cod_azienda) and  
         ( distinta_gruppi_varianti.cod_prodotto_padre =* distinta.cod_prodotto_padre) and  
         ( distinta_gruppi_varianti.num_sequenza =* distinta.num_sequenza) and  
         ( distinta_gruppi_varianti.cod_prodotto_figlio =* distinta.cod_prodotto_figlio) and  
         ( distinta_gruppi_varianti.cod_versione_figlio =* distinta.cod_versione_figlio) and  
         ( distinta_gruppi_varianti.cod_versione =* distinta.cod_versione) and  
         ( ( distinta.cod_azienda = :s_cs_xx_cod_azienda ) AND  
         ( distinta.cod_prodotto_padre = :as_cod_prodotto_finito ) AND  
         ( distinta.cod_versione = :as_cod_versione) )   ;

open righe_distinta_3;

guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): fetch distinta")


do while true

	fetch righe_distinta_3 
	into  	:ls_cod_prodotto_figlio[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
			:ll_num_sequenza,
			:ls_cod_misura,
			:ld_quan_tecnica,
			:ld_quan_utilizzo,
			:ld_dim_x,
			:ld_dim_y,
			:ld_dim_z,
			:ld_dim_t,
			:ld_coef_calcolo,
			:ls_des_estesa,
			:ls_formula_tempo,
			:ls_cod_formula_quan_utilizzo,
			:ls_flag_materia_prima_distinta,
			:ls_cod_gruppo_variante,
			:ls_cod_formula_prod_figlio,
			:ls_cod_formula_vers_figlio,
			:ls_cod_formula_quan_tecnica;

	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	if sqlca.sqlcode<>0 then
		close righe_distinta_3;
		as_errore = "Configuratore: uof_ins_varianti ~r~nSi è verificato un errore imprevisto nel cursore che scorre la distinta" + SQLCA.SQLErrText
		return -1
	end if
	

	
	if not isnull(ls_cod_gruppo_variante) then

		for ll_i = 1 to ll_max
			li_count = 0
			if ls_cod_gruppo_variante = astr_varianti[ll_i].cod_gruppo_variante then
				
				if as_tipo_gestione = "ORD_VEN" then
					//in questo caso esiste la tabella varianti_det_ord_ven_prod
					
					ls_sql_del ="delete from varianti_det_ord_ven_prod " + &
									" where cod_azienda ='" + s_cs_xx_cod_azienda + "' and " + &
									" anno_registrazione=" + string(al_anno_documento) + " and " + &
									" num_registrazione=" + string(al_num_documento) + " and " + &
									ls_progressivo + "=" + string(al_prog_riga_documento) + " and " + &
									" cod_prodotto_padre='" + as_cod_prodotto_finito + "' and " + &
									" num_sequenza=" + string(ll_num_sequenza) + " and " +&
									" cod_versione='" + as_cod_versione + "' "
					
					ls_sql_del += " and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_conteggio] + "' "
					ls_sql_del += " and cod_versione_figlio='" + ls_cod_versione_figlio[ll_conteggio] + "' "

					execute immediate :ls_sql_del;
					if sqlca.sqlcode = -1 then
						as_errore = "Configuratore uof_ins_varianti~r~nErrore nella Cancellazione della Variante da varianti_det_ord_ven_PROD. Dettaglio " + sqlca.sqlerrtext
						close righe_distinta_3;
						rollback;
						return -1
					end if
				end if
				
				ls_sql_del ="delete from " + ls_tabella + &
								" where cod_azienda='" + s_cs_xx_cod_azienda + "' and " + &
								" anno_registrazione=" + string(al_anno_documento) + " and " + &
								" num_registrazione=" + string(al_num_documento) + " and " + &
								ls_progressivo + "=" + string(al_prog_riga_documento) + " and " + &
								" cod_prodotto_padre='" + as_cod_prodotto_finito + "' and " + &
								" num_sequenza=" + string(ll_num_sequenza) + " and " + &
								" cod_versione='" + as_cod_versione + "' "
				
				ls_sql_del += " and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_conteggio] + "' "
				ls_sql_del += " and cod_versione_figlio='" + ls_cod_versione_figlio[ll_conteggio] + "' "
				
				execute immediate :ls_sql_del;
				if sqlca.sqlcode = -1 then
					as_errore = "Configuratore uof_ins_varianti~r~nErrore nella Cancellazione della Variante da varianti_det_ord_ven. Dettaglio " + sqlca.sqlerrtext
					close righe_distinta_3;
					rollback;
					return -1
				end if
				
				// 	EnMe + Donato 24/02/2014: la versione della variante DEVE restare quella del prodotto figlio del ramo di distinta; altrimenti
				//	bisogna operare con le formule.
				astr_varianti[ll_i].cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
				
				
				//INSERISCO IN TABELLA VARIANTI DOCUMENTO, CON LE RELATIVE QUANTITà PRESENTI IN DISTINTA BASE (O SE <0 DELLA VARIANTE PREVISTA)
				//######################################################################################
				
				// Ptenda 28-9/2011
				// importante che rimangano le prossime 2 righe di codice; infatti se la quantità della materia prima automatica è zero, deve prendere la quantità in distinta.
				if not isnull(astr_varianti[ll_i].quan_tecnica) and astr_varianti[ll_i].quan_tecnica > 0 then ld_quan_tecnica = astr_varianti[ll_i].quan_tecnica
				if not isnull(astr_varianti[ll_i].quan_utilizzo) and astr_varianti[ll_i].quan_utilizzo > 0 then ld_quan_utilizzo = astr_varianti[ll_i].quan_utilizzo

				if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
				if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
				
				if isnull(ls_cod_misura) then ls_cod_misura = ""
				if isnull(ld_dim_x) then ld_dim_x = 0
				if isnull(ld_dim_y) then ld_dim_y = 0
				if isnull(ld_dim_z) then ld_dim_z = 0
				if isnull(ld_dim_t) then ld_dim_t = 0
				if isnull(ld_coef_calcolo) then ld_coef_calcolo = 0
				if isnull(ls_des_estesa) then ls_des_estesa = ""
				if isnull(ls_formula_tempo) then ls_formula_tempo = ""
				
				guo_functions.uof_replace_string(ls_des_estesa, "'", "''")
				
				ld_quan_utilizzo = round(ld_quan_utilizzo, 4)
				ld_quan_tecnica = round(ld_quan_tecnica, 4)

				// EnMe il 12/1/2011 la UM della variante va sempre presa dal magazzino.
				select flag_materia_prima, 
						cod_misura_mag
				into   :ls_flag_materia_prima, 
						:ls_cod_misura
				from   anag_prodotti
				where cod_azienda  = :s_cs_xx_cod_azienda and
						 cod_prodotto = :astr_varianti[ll_i].cod_mp_variante;
						 
				if sqlca.sqlcode <> 0 then
					as_errore = "Configuratore uof_ins_varianti~r~nErrore nella ricerca del flag_materia_prima e dell'unità di misura in anagrafica prodotti " + astr_varianti[ll_i].cod_mp_variante + ". Dettaglio errore " + sqlca.sqlerrtext
					close righe_distinta_3;
					rollback;
					return -1
				end if
				
				if isnull(ls_flag_materia_prima) then ls_flag_materia_prima = "N"

				// EnMe 16/06/2014: solo per costo componenti comanda il flag_mp che è in distinta; quindi il flag_mp della anag_prodotti non conta nulla
				if isnull(ls_flag_materia_prima_distinta) then ls_flag_materia_prima_distinta = "N"
				ls_flag_materia_prima = ls_flag_materia_prima_distinta
				
				// EnMe 07/02/2012: aggiunto per gestione stabilimento e reparto produzione nelle varianti
				ls_cod_reparto_produzione = ls_vuoto
				setnull(ls_cod_deposito_produzione)
				
				if isnull(ls_cod_deposito_produzione) then 
					ls_cod_deposito_produzione = " null "
				else
					ls_cod_deposito_produzione = " '" + ls_cod_deposito_produzione + "' "
				end if
				
				if isnull(ls_cod_misura) then 
					ls_cod_misura = "null"
				else
					ls_cod_misura = "'" + ls_cod_misura + "'"
				end if
				
				ls_sql_ins =" insert into " + ls_tabella + &
								"(cod_azienda," + &
								"anno_registrazione," + &
								"num_registrazione," + &
								ls_progressivo + "," + &
								"cod_prodotto_padre," + &
								"num_sequenza," + &
								"cod_prodotto_figlio," + &
								"cod_versione_figlio," + &
								"cod_versione," + &
								"cod_misura," + &
								"cod_prodotto," + &
								"cod_versione_variante," + &
								"quan_tecnica," + &
								"quan_utilizzo," + &
								"dim_x," + &
								"dim_y," + &
								"dim_z," + &
								"dim_t," + &
								"coef_calcolo," + &
								"flag_esclusione," + &
								"formula_tempo," + &
								"flag_materia_prima," + &
								"des_estesa "
				if as_tipo_gestione = "ORD_VEN" then
					ls_sql_ins += ",cod_deposito_produzione)"
				else
					ls_sql_ins += " ) "
				end if
				
				ls_sql_ins += " VALUES ('" +  s_cs_xx_cod_azienda + "', " + &
								string(al_anno_documento) + ", " + &   
								string(al_num_documento) + ", " + &   
								string(al_prog_riga_documento) + ", '" + &   
								as_cod_prodotto_finito + "', " + &
								string(ll_num_sequenza) + ", '" + &   
								ls_cod_prodotto_figlio[ll_conteggio] + "', '" + &
								ls_cod_versione_figlio[ll_conteggio] + "', '" + &
								as_cod_versione + "'," + &
								ls_cod_misura + ", '" + &
								astr_varianti[ll_i].cod_mp_variante + "', '" + &
								astr_varianti[ll_i].cod_versione_figlio + "', " + &
								f_decimal_to_string(ld_quan_tecnica) + ", " + &   
								f_decimal_to_string(ld_quan_utilizzo) + ", " + &
								f_decimal_to_string(ld_dim_x) + ", " + &   
								f_decimal_to_string(ld_dim_y) + ", " + &   
								f_decimal_to_string(ld_dim_z) + ", " + &   
								f_decimal_to_string(ld_dim_t) + ", " + &   
								f_decimal_to_string(ld_coef_calcolo) + ", '" + &   
								"N', '" + &
								ls_formula_tempo + "', '" + &
								ls_flag_materia_prima + "', '" + &
								ls_des_estesa + "' "

				if as_tipo_gestione = "ORD_VEN" then
					ls_sql_ins += ", " + ls_cod_deposito_produzione + ")" 
				else
					ls_sql_ins += " )"
				end if
				
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. s_cs_xx_cod_azienda " + guo_functions.uof_format(s_cs_xx_cod_azienda) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. al_anno_documento " + guo_functions.uof_format(al_anno_documento) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. al_num_documento " + guo_functions.uof_format(al_num_documento) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. al_prog_riga_documento " + guo_functions.uof_format(al_prog_riga_documento) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. as_cod_prodotto_finito " + guo_functions.uof_format(as_cod_prodotto_finito) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ll_num_sequenza " + guo_functions.uof_format(ll_num_sequenza) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ls_cod_prodotto_figlio[ll_conteggio] " + guo_functions.uof_format(ls_cod_prodotto_figlio[ll_conteggio]) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ls_cod_versione_figlio[ll_conteggio] " + guo_functions.uof_format(ls_cod_versione_figlio[ll_conteggio]) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. as_cod_versione " + guo_functions.uof_format(as_cod_versione) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ls_cod_misura " + guo_functions.uof_format(ls_cod_misura) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. astr_varianti[ll_i].cod_mp_variante " + guo_functions.uof_format(astr_varianti[ll_i].cod_mp_variante) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. astr_varianti[ll_i].cod_versione_figlio " + guo_functions.uof_format(astr_varianti[ll_i].cod_versione_figlio) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ld_quan_tecnica " + guo_functions.uof_format(ld_quan_tecnica) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ld_quan_utilizzo " + guo_functions.uof_format(ld_quan_utilizzo) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ld_dim_x " + guo_functions.uof_format(ld_dim_x) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ld_dim_y " + guo_functions.uof_format(ld_dim_y) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ld_dim_z " + guo_functions.uof_format(ld_dim_z) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ld_dim_t " + guo_functions.uof_format(ld_dim_t) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ld_coef_calcolo " + guo_functions.uof_format(ld_coef_calcolo) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ls_formula_tempo " + guo_functions.uof_format(ls_formula_tempo) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ls_flag_materia_prima " + guo_functions.uof_format(ls_flag_materia_prima) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ls_des_estesa " + guo_functions.uof_format(ls_des_estesa) )
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. ls_cod_deposito_produzione " + guo_functions.uof_format(ls_cod_deposito_produzione) )
				
				guo_functions.uof_log("uo_conf_varianti.uof_inf_varianti(): insert variante. SQL " + guo_functions.uof_format(ls_sql_ins) )


				execute immediate :ls_sql_ins;
				if sqlca.sqlcode = -1 then
					as_errore = "Configuratore uof_ins_varianti~r~nErrore nell'inserimento della Variante nella tabella " + ls_tabella + ". Dettaglio errore " + sqlca.sqlerrtext
					close righe_distinta_3;
					rollback;
					return -1
				end if
				
				//ADESSO, SE CI SONO FORMULE SUL FIGLIO/VERSIONE DEL RAMO VANNO VALUTATE, IN QUANTO, EVENTULAI FORMULE SU QTA UTILIZZO/TECNICA
				//DEVONO FARE RIFERIMENTO A VARIABILI FIGLIO DEI NUOVI VALORI
				//######################################################################################
				
				//carico la variante e relativa versione prevista
				ls_nuova_variante = astr_varianti[ll_i].cod_mp_variante
				ls_nuova_versione_variante = astr_varianti[ll_i].cod_versione_figlio
				
				guo_functions.uof_log("Calcolo formule PRODOTTO FIGLIO ramo distinta variante .... PADRE:" + as_cod_prodotto_finito + "  FIGLIO:" + ls_cod_prodotto_figlio[ll_conteggio] + " VARIANTE:" + astr_varianti[ll_i].cod_mp_variante) 
				guo_functions.uof_log("Calcolo formule PRODOTTO FIGLIO ramo distinta variante .... PADRE:" + as_cod_prodotto_finito + "  FIGLIO:" + ls_cod_prodotto_figlio[ll_conteggio] + " F.QTA util:" + ls_cod_formula_quan_utilizzo) 
				guo_functions.uof_log("Calcolo formule PRODOTTO FIGLIO ramo distinta variante .... PADRE:" + as_cod_prodotto_finito + "  FIGLIO:" + ls_cod_prodotto_figlio[ll_conteggio] + " F.figlio:" + ls_cod_formula_prod_figlio) 
				guo_functions.uof_log("Calcolo formule PRODOTTO FIGLIO ramo distinta variante .... PADRE:" + as_cod_prodotto_finito + "  FIGLIO:" + ls_cod_prodotto_figlio[ll_conteggio] + " F.QTA tec:" + ls_cod_formula_quan_tecnica ) 
				
				if (not isnull(ls_cod_formula_prod_figlio) and ls_cod_formula_prod_figlio<>"") or (not isnull(ls_cod_formula_vers_figlio) and ls_cod_formula_vers_figlio<>"") then
					
					guo_functions.uof_log("Applico formula " + ls_cod_formula_prod_figlio) 
					li_ret = uof_valorizza_formule_figlio_varianti( al_anno_documento, al_num_documento, al_prog_riga_documento, as_tipo_gestione, &
																		 as_cod_prodotto_finito, as_cod_versione, ll_num_sequenza, ls_cod_prodotto_figlio[ll_conteggio], ls_cod_versione_figlio[ll_conteggio], ref ls_nuova_variante, ref ls_nuova_versione_variante, ls_cod_formula_prod_figlio, ls_cod_formula_vers_figlio, ref as_errore)
					
					guo_functions.uof_log("Dopo Applicazione formula figlio "  + ls_cod_formula_prod_figlio  ) 
					
					if li_ret<0 then
						//mettiamo nel messaggio anche l'informazione sul ramo di distinta in cui la formula ha dato errore
						as_errore = 	"Ramo ("+as_cod_prodotto_finito+","+as_cod_versione+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + 	as_errore
						guo_functions.uof_log("Errore formula figlio " + ls_cod_formula_prod_figlio + ": " + as_errore  ) 
						close righe_distinta_3;
						rollback;
						return -1
					end if
				end if
				
				//SE LA VARIANTE O RELATIVA VERSIONE è STATA CAMBIATA, DEVO FARE UPDATE NELLATABELLA VARIANTI DOCUMENTO
				//PRIMA DI PASSARE AL CALCOLO DELLA QUANTITà UTILIZZO O TECNICA MEDIANTE EVENTUALE FORMULA
				//######################################################################################
				
				if ls_nuova_variante <> astr_varianti[ll_i].cod_mp_variante or ls_nuova_versione_variante <> astr_varianti[ll_i].cod_versione_figlio then
					
					//in ls_nuova_variante e ls_nuova_versione_variante ci sono i nuovi valori
					astr_varianti[ll_i].cod_mp_variante = ls_nuova_variante
					astr_varianti[ll_i].cod_versione_figlio = ls_nuova_versione_variante
					
					//faccio update delle quantità calcolate
					ls_sql_upd ="update " + ls_tabella + " "+&
									"set cod_prodotto='"+astr_varianti[ll_i].cod_mp_variante+ "'," +&
										"cod_versione_variante='"+astr_varianti[ll_i].cod_versione_figlio+"' "+ &
									"where cod_azienda='"+s_cs_xx_cod_azienda+"' and "+&
												"anno_registrazione="+string(al_anno_documento)+" and "+&
												"num_registrazione="+string(al_num_documento)+" and "+&
												ls_progressivo+"="+string(al_prog_riga_documento)+" and "+&
												"cod_prodotto_padre='"+as_cod_prodotto_finito+"' and "+&
												"cod_versione='"+as_cod_versione+"' and "+&
												"num_sequenza="+string(ll_num_sequenza)+" and "+&
												"cod_prodotto_figlio='"+ls_cod_prodotto_figlio[ll_conteggio]+"' and "+&
												"cod_versione_figlio='"+ls_cod_versione_figlio[ll_conteggio]+"' "
												
					execute immediate :ls_sql_upd;
					
					if sqlca.sqlcode = -1 then
						as_errore = "Configuratore uof_ins_varianti: Errore in update variante/vers.variante nella tabella " + ls_tabella + ": "+ sqlca.sqlerrtext
						close righe_distinta_3;
						rollback;
						return -1
					end if
					
				end if
				
				//#####################################################################################
				//ADESSO FINALMENTE VALUTO LE FORMULE QUANTITà UTILIZZO E TECNICA
				guo_functions.uof_log("Valuto formule quantità ") 
				
				// ------------------- NUOVA GESTIONE VUOTO PER PIENO -------------------------------
				select cod_formula
				into :ls_cod_formula_vpp
				from tab_formule_legami
				where 	cod_azienda         		= :s_cs_xx_cod_azienda and
							cod_prodotto_finito 	= :as_cod_prodotto_finito and
							cod_versione        	= :as_cod_versione and
							cod_prodotto_padre  	= :as_cod_prodotto_finito and
							cod_versione_padre  	= :as_cod_versione and
							cod_prodotto_figlio 	= :astr_varianti[ll_i].cod_mp_variante and
							cod_versione_figlio 	= :astr_varianti[ll_i].cod_versione_figlio;
		
				if sqlca.sqlcode < 0 then
					as_errore = "Configuratore uof_ins_varianti: Errore in fase di ricerca formula Vuoto per Pieno su prodotto padre " + as_cod_prodotto_finito + " prodotto figlio " + ls_cod_prodotto_figlio[ll_conteggio] + "  DETT:" + sqlca.sqlerrtext
				elseif sqlca.sqlcode = 0 and not isnull(ls_cod_formula_vpp) then
					ls_cod_formula_quan_utilizzo = ls_cod_formula_vpp
				end if
				//-----------------------------------------------------------------------------------------------
				
				// Ptenda 28-9/2011
				// importante che rimangano le prossime 2 righe di codice; infatti se la quantità della materia prima automatica è zero, deve prendere la quantità in distinta.
				if not isnull(astr_varianti[ll_i].quan_tecnica) and astr_varianti[ll_i].quan_tecnica > 0 then ld_quan_tecnica = astr_varianti[ll_i].quan_tecnica
				if not isnull(astr_varianti[ll_i].quan_utilizzo) and astr_varianti[ll_i].quan_utilizzo > 0 then ld_quan_utilizzo = astr_varianti[ll_i].quan_utilizzo

				if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
				if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
				
				//formula q.tà utilizzo ##########################
				guo_functions.uof_log("Valuto formule quantità utilizzo:" + ls_cod_formula_quan_utilizzo) 
				if not isnull(ls_cod_formula_quan_utilizzo) and ls_cod_formula_quan_utilizzo<>"" then
					ll_ordine[1] = al_anno_documento
					ll_ordine[2] = al_num_documento
					ll_ordine[3] = al_prog_riga_documento
					
					lstr_distinta.cod_prodotto_padre = as_cod_prodotto_finito
					lstr_distinta.cod_versione_padre = as_cod_versione
					lstr_distinta.num_sequenza = ll_num_sequenza
					lstr_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio[ll_conteggio]
					lstr_distinta.cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
					
					guo_functions.uof_log("Calcolo formule QUANTITA' UTIL ramo distinta Variante .... PADRE:" + as_cod_prodotto_finito + "  FIGLIO:" + ls_cod_prodotto_figlio[ll_conteggio] + " VARIANTE:" + astr_varianti[ll_i].cod_mp_variante) 

					luo_formule_calcolo = create uo_formule_calcolo
					
					li_ret = luo_formule_calcolo.uof_calcola_formula_db_v2(s_cs_xx_cod_azienda, ls_cod_formula_quan_utilizzo, ll_ordine[],  lstr_distinta, as_tipo_gestione, ld_quan_utilizzo, ls_temp, ls_tipo_ritorno, as_errore)
					if li_ret<0 then
						as_errore = 	"Calcolo Q.ta utilizzo; Ramo ("+as_cod_prodotto_finito+","+as_cod_versione+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
												as_errore			
						guo_functions.uof_log("Errore formula quantità util :" + ls_cod_formula_quan_utilizzo + ": " + as_errore  ) 
						destroy luo_formule_calcolo
						close righe_distinta_3;
						rollback;
						return -1
					end if
					
					destroy luo_formule_calcolo
		
				end if
				
				if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
				//##########################################
				
				//formula su quantità tecnica ##########################
				guo_functions.uof_log("Valuto formule quantità tecnica:" + ls_cod_formula_quan_tecnica) 
				if not isnull(ls_cod_formula_quan_tecnica) and ls_cod_formula_quan_tecnica<>"" then
					ll_ordine[1] = al_anno_documento
					ll_ordine[2] = al_num_documento
					ll_ordine[3] = al_prog_riga_documento
					
					lstr_distinta.cod_prodotto_padre = as_cod_prodotto_finito
					lstr_distinta.cod_versione_padre = as_cod_versione
					lstr_distinta.num_sequenza = ll_num_sequenza
					lstr_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio[ll_conteggio]
					lstr_distinta.cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
					
					guo_functions.uof_log("Calcolo formule QUANTITA' TEC ramo distinta Variante .... PADRE:" + as_cod_prodotto_finito + "  FIGLIO:" + ls_cod_prodotto_figlio[ll_conteggio] + " VARIANTE:" + astr_varianti[ll_i].cod_mp_variante) 

					luo_formule_calcolo = create uo_formule_calcolo
					
					li_ret = luo_formule_calcolo.uof_calcola_formula_db_v2(s_cs_xx_cod_azienda,ls_cod_formula_quan_tecnica, ll_ordine[],  lstr_distinta, as_tipo_gestione, ld_quan_tecnica, ls_temp, ls_tipo_ritorno, as_errore)
					if li_ret<0 then
						as_errore = 	"Calcolo Q.ta tecnica; Ramo ("+as_cod_prodotto_finito+","+as_cod_versione+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
												as_errore			
						guo_functions.uof_log("Errore formula quantità tec :" + ls_cod_formula_quan_utilizzo + ": " + as_errore  ) 
						close righe_distinta_3;
						destroy luo_formule_calcolo
						rollback;
						return -1
					end if
					destroy luo_formule_calcolo
				end if
				
				if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
				//##########################################
				guo_functions.uof_log(" quantità utilizzo:" + string(ld_quan_utilizzo)) 
				guo_functions.uof_log("quantità tecnica:" + string(ld_quan_tecnica)) 
				
				//faccio update delle quantità calcolate
				ls_sql_upd ="update " + ls_tabella + " "+&
								"set quan_tecnica="+f_decimal_to_string(ld_quan_tecnica)+ "," +&
									"quan_utilizzo="+f_decimal_to_string(ld_quan_utilizzo)+" "+ &
								"where cod_azienda='"+s_cs_xx_cod_azienda+"' and "+&
											"anno_registrazione="+string(al_anno_documento)+" and "+&
											"num_registrazione="+string(al_num_documento)+" and "+&
											ls_progressivo+"="+string(al_prog_riga_documento)+" and "+&
											"cod_prodotto_padre='"+as_cod_prodotto_finito+"' and "+&
											"cod_versione='"+as_cod_versione+"' and "+&
											"num_sequenza="+string(ll_num_sequenza)+" and "+&
											"cod_prodotto_figlio='"+ls_cod_prodotto_figlio[ll_conteggio]+"' and "+&
											"cod_versione_figlio='"+ls_cod_versione_figlio[ll_conteggio]+"' "
											
				execute immediate :ls_sql_upd;
				
				if sqlca.sqlcode = -1 then
					as_errore = "Configuratore uof_ins_varianti: Errore in update q.tà utilizzo e tecnica nella tabella " + ls_tabella + ": "+ sqlca.sqlerrtext
					close righe_distinta_3;
					rollback;
					return -1
				end if
				
				// inserisco anche tutti gli eventuali reparti di produzione
				if as_tipo_gestione = "ORD_VEN" then
				
					for ll_y = 1 to upperbound(ls_cod_reparto_produzione)
						
						INSERT INTO varianti_det_ord_ven_prod  
								( cod_azienda,   
								  anno_registrazione,   
								  num_registrazione,   
								  prog_riga_ord_ven,   
								  cod_prodotto_padre,   
								  num_sequenza,   
								  cod_prodotto_figlio,   
								  cod_versione,   
								  cod_versione_figlio,   
								  cod_reparto )  
						VALUES ( :s_cs_xx_cod_azienda,
								  :al_anno_documento,   
								  :al_num_documento,   
								  :al_prog_riga_documento,   
								  :as_cod_prodotto_finito,   
								  :ll_num_sequenza,   
								  :ls_cod_prodotto_figlio[ll_conteggio],   
								  :as_cod_versione,   
								  :ls_cod_versione_figlio[ll_conteggio],   
								  :ls_cod_reparto_produzione[ll_y] )  ;

						if sqlca.sqlcode = -1 then
							as_errore = "Configuratore uof_ins_varianti~r~nErrore nell'inserimento della dei reparti produttivi.Dettaglio errore " + sqlca.sqlerrtext
							close righe_distinta_3;
							rollback;
							return -1
						end if
					next
				end if
				
				//siccome il cod_gruppo_variante è stata trovata, inutile continuare nel ciclo for, quindi esci
				exit
				
			end if	
		next
		
	else
	
	end if
	ll_conteggio++	
	
loop

close righe_distinta_3;


for ll_t = 1 to ll_conteggio -1

   SELECT count(*)
   INTO   :li_count  
   FROM   distinta  
   WHERE  cod_azienda        = :s_cs_xx_cod_azienda
	AND    cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t]
	and    cod_versione       = :ls_cod_versione_figlio[ll_t];

	if sqlca.sqlcode=-1 then // Errore
		as_errore = "Configuratore uof_ins_varianti~r~nErrore in lettura tabella distinta. Dettaglio " + SQLCA.SQLErrText
		rollback;
		return -1
	end if

	if li_count > 0 then
		li_risposta = uof_ins_varianti(ls_cod_prodotto_figlio[ll_t], ls_cod_versione_figlio[ll_t], as_tipo_gestione,  astr_varianti[], al_anno_documento, al_num_documento,al_prog_riga_documento, ref as_errore)
		if li_risposta = -1 then return -1
	end if
next


guo_functions.uof_log("Uscita funzione uo_conf_varianti.uof_inf_varianti()")

return 0
end function

public function integer uof_valorizza_formule_figlio_varianti (long al_anno_documento, long al_num_documento, long al_prog_riga_documento, string as_tipo_gestione, string as_padre, string as_versione_padre, long al_num_sequenza, string as_figlio, string as_versione_figlio, ref string as_cod_variante, ref string as_versione_variante, string as_formula_figlio, string as_formula_versione_figlio, ref string as_errore);long						al_rif_riga_ordine[]
s_chiave_distinta		ls_chiave_distinta
integer					li_ret
dec{4}					ld_valore
string						ls_valore, ls_tipo_ritorno, ls_sql, ls_messaggio
datastore				lds_data
uo_formule_calcolo	luo_formule_calcolo


as_errore = ""

al_rif_riga_ordine[1] = al_anno_documento
al_rif_riga_ordine[2] = al_num_documento
al_rif_riga_ordine[3] = al_prog_riga_documento

ls_chiave_distinta.cod_prodotto_padre = as_padre
ls_chiave_distinta.cod_versione_padre = as_versione_padre
ls_chiave_distinta.num_sequenza = al_num_sequenza
ls_chiave_distinta.cod_prodotto_figlio = as_figlio
ls_chiave_distinta.cod_versione_figlio = as_versione_figlio

setnull(ls_valore)
setnull(ld_valore)

ls_messaggio =""

//###################################################################################
if as_formula_figlio<>"" and not isnull(as_formula_figlio) then
	//valuto la formula sul cod.prodotto.figlio
	
	ls_messaggio += " F.la Figlio "+as_formula_figlio + " "
	
	guo_functions.uof_log("Calcolo formula " +as_formula_figlio )
	luo_formule_calcolo = create uo_formule_calcolo

	li_ret = luo_formule_calcolo.uof_calcola_formula_db_v2 (s_cs_xx_cod_azienda,	as_formula_figlio, al_rif_riga_ordine[], ls_chiave_distinta, as_tipo_gestione, ld_valore, ls_valore, ls_tipo_ritorno, as_errore)
	guo_functions.uof_log("Risultatoo formula " +ls_valore + " Errore="+string(li_ret) )
	destroy luo_formule_calcolo
	
	if li_ret<0 then
		return -1
	end if
	
	//il ritorno deve essere STRINGA
	if ls_tipo_ritorno<>"2" then
		//as_errore="Anomalia NON BLOCCANTE: La formula impostata sul prodotto figlio ha valore ritorno numerico al posto di stringa!"
		//return 1

	else
		//se ritorna VUOTO fai come se non ci fosse niente (Niente sostituzione prodotto figlio...)
		if ls_valore<>"" and not isnull(ls_valore) then
			
			if ls_valore<>as_cod_variante then
				//se è tornato qualcosa di diverso ... imposto il nuovo valore per il prodotto figlio
				if uof_controlla_figlio_formula( ls_valore) = 0 then
					as_cod_variante = ls_valore
				end if
			end if
		end if
		
	end if
end if


setnull(ls_valore)
setnull(ld_valore)

//###################################################################################
if as_formula_versione_figlio<>"" and not isnull(as_formula_versione_figlio) then
	//valuto la formula sul cod.versione.figlio
	
	ls_messaggio += " F.la Vers.Figlio "+as_formula_versione_figlio + " "
	
	luo_formule_calcolo = create uo_formule_calcolo
	li_ret = uo_formule_calcolo.uof_calcola_formula_db_v2 (	s_cs_xx_cod_azienda,as_formula_versione_figlio, al_rif_riga_ordine[], ls_chiave_distinta, as_tipo_gestione, ld_valore, ls_valore, ls_tipo_ritorno, as_errore)
	destroy luo_formule_calcolo	
	if li_ret<0 then
		return -1
	end if
	
	//il ritorno deve essere STRINGA
	if ls_tipo_ritorno<>"2" then
//		as_errore="Anomalia NON BLOCCANTE: La formula impostata sulla versione figlio ha valore ritorno numerico al posto di stringa!"
//		return -1

	else
		//se ritorna VUOTO fai come se non ci fosse niente (Niente sostituzione versione prodotto figlio...)
		if ls_valore<>"" and not isnull(ls_valore) then
			
			if ls_valore<>as_versione_variante then
				//se è tornato qualcosa di diverso ... imposto il nuovo valore per la versione del prodotto figlio
				as_versione_variante = ls_valore
			end if
			
		end if

	end if
end if


return 0

end function

public function integer uof_valorizza_formule (string as_cod_prodotto_padre, string as_cod_versione_padre, string as_tipo_gestione, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, ref string as_errore);/*
 FUNZIONE CHE VALORIZZA TUTTE LE FORMULE PRESENTI DELLA DISTINTA CREANDO LA RELATIVA VARIANTE.

 nome: uof_valirizza_formule
 tipo: integer
  
	Variabili passate: 		nome					 tipo				passaggio per			commento
							
							 as_cod_prodotto_padre	 		string			valore
							 as_cod_versione_padre			string			valore

		Creata il 01/01/2000
		Autore ENRICO MENEGOTTO


27/9/2006 Richiesta di Daniele Riello; impostare il flag_materia_prima allo stesso valore 
 													che ha nella distinta.

31/10/2006 Adeguamento della funzione alla nuova gestione distinta cpn versione del figlio.

21/05/2013 Aggiunto calcolo formule su cod. prodotto figlio e cod. versione figlio
*/

string						ls_cod_prodotto_figlio[],ls_test, ls_tabella, ls_progressivo, ls_cod_misura, ls_cod_formula_quan_utilizzo, &
							ls_des_estesa, ls_formula_tempo, ls_sql_del, ls_sql_ins, ls_flag_materia_prima,ls_cod_versione_figlio[],&
							ls_cod_prodotto_variante, ls_cod_versione_variante, ls_tabella_comp, ls_cod_formula_quan_tecnica, ls_cod_formula_prod_figlio, &
							ls_cod_formula_vers_figlio, ls_nuovo_figlio, ls_nuova_versione_figlio, ls_temp, ls_tipo_ritorno,ls_sql_upd
							
dec{4}					ld_quan_tecnica, ld_quan_utilizzo, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_coef_calcolo

long						ll_conteggio,ll_t,ll_num_sequenza, ll_cont_1, ll_ordine[], ll_num_sequenza_new

integer					li_risposta, li_count

s_chiave_distinta		lstr_distinta
uo_formule_calcolo	luo_formule_calcolo




choose case as_tipo_gestione
		
	case "ORD_VEN"
		ls_tabella = "varianti_det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		ls_tabella_comp = "comp_det_ord_ven"
		
	case "OFF_VEN"
		ls_tabella = "varianti_det_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		ls_tabella_comp = "comp_det_off_ven"
		
end choose

ll_conteggio = 1

guo_functions.uof_log("Entrato in uof_valorizza_formule del padre: " + as_cod_prodotto_padre) 


declare righe_distinta_3 cursor for 
select  cod_prodotto_figlio,
		  cod_versione_figlio,
		  num_sequenza,
		  cod_misura,
		  quan_tecnica,
		  quan_utilizzo,
		  dim_x,
		  dim_y,
		  dim_z,
		  dim_t,
		  coef_calcolo,
		  des_estesa,
		  formula_tempo,
		  cod_formula_quan_utilizzo,
		  flag_materia_prima,
		  cod_formula_quan_tecnica,
		  cod_formula_prod_figlio,
		  cod_formula_vers_figlio
from    distinta 
where   cod_azienda = :s_cs_xx_cod_azienda  and
		  cod_prodotto_padre = :as_cod_prodotto_padre and
		  cod_versione=:as_cod_versione_padre;

open righe_distinta_3;

do while true
	fetch righe_distinta_3 
	into  :ls_cod_prodotto_figlio[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
			:ll_num_sequenza,
			:ls_cod_misura,
			:ld_quan_tecnica,
			:ld_quan_utilizzo,
			:ld_dim_x,
			:ld_dim_y,
			:ld_dim_z,
			:ld_dim_t,
			:ld_coef_calcolo,
			:ls_des_estesa,
			:ls_formula_tempo,
			:ls_cod_formula_quan_utilizzo,
			:ls_flag_materia_prima,
			:ls_cod_formula_quan_tecnica,
			:ls_cod_formula_prod_figlio,
			:ls_cod_formula_vers_figlio;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	if sqlca.sqlcode<>0 then
		as_errore = "Errore in fase di fetch cursore righe_distinta_3 (uo_conf_varianti.uof_valorizza_formule) ~r~n" + SQLCA.SQLErrText
		close righe_distinta_3;
		rollback;
		return -1
	end if

	guo_functions.uof_log("Figlio: " + ls_cod_prodotto_figlio[ll_conteggio])
	
	
	// verifico presenza o meno della variante --------------------------------------------------------------------------------------------------
	choose case as_tipo_gestione
		case "ORD_VEN"
			select count(*)
			into   :ll_cont_1
			from   varianti_det_ord_ven
			where  cod_azienda         = :s_cs_xx_cod_azienda and
					 anno_registrazione  = :al_anno_documento and
					 num_registrazione   = :al_num_documento and
					 prog_riga_ord_ven   = :al_prog_riga_documento and
					 cod_prodotto_padre  = :as_cod_prodotto_padre and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
					 cod_versione        = :as_cod_versione_padre and
					 num_sequenza = :ll_num_sequenza;
					 
		case "OFF_VEN"
			select count(*)
			into   :ll_cont_1
			from   varianti_det_off_ven
			where  cod_azienda         = :s_cs_xx_cod_azienda and
					 anno_registrazione  = :al_anno_documento and
					 num_registrazione   = :al_num_documento and
					 prog_riga_off_ven   = :al_prog_riga_documento and
					 cod_prodotto_padre  = :as_cod_prodotto_padre and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
					 cod_versione        = :as_cod_versione_padre and
					 num_sequenza = :ll_num_sequenza;
					 
	end choose
	
	//-------------------------------------------
	if ll_cont_1 = 0 then						 
		//NON C'è LA VARIANTE (uso la formula presente sul ramo di distinta	) #####################################################
		//######################################################################################
		//qui valuto le formule eventuali su prodotto figlio e versione figlio
		//la funzione modificherà gli argomenti, solo se sono diversi
		ls_nuovo_figlio = ls_cod_prodotto_figlio[ll_conteggio]
		ls_nuova_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
		ll_num_sequenza_new = ll_num_sequenza
		
		guo_functions.uof_log("Calcolo formule PRODOTTO FIGLIO ramo distinta senza variante .... PADRE:" + as_cod_prodotto_padre + "  FIGLIO:" + ls_cod_prodotto_figlio[ll_conteggio] ) 

		
		if (not isnull(ls_cod_formula_prod_figlio) and ls_cod_formula_prod_figlio<>"") or (not isnull(ls_cod_formula_vers_figlio) and ls_cod_formula_vers_figlio<>"") then
			guo_functions.uof_log(" .... applico formula " + ls_cod_formula_prod_figlio ) 
			
			li_risposta = uof_valorizza_formule_prod_figlio(	al_anno_documento, al_num_documento, al_prog_riga_documento, as_tipo_gestione, as_cod_prodotto_padre, as_cod_versione_padre, ll_num_sequenza, ls_nuovo_figlio, ls_nuova_versione_figlio, ll_num_sequenza_new, ls_cod_formula_prod_figlio, ls_cod_formula_vers_figlio, as_errore)
			
			if li_risposta<0 then
				//mettiamo nel messaggio anche l'informazione sul ramo di distinta in cui la formula ha dato errore
				as_errore = 	"Ramo ("+as_cod_prodotto_padre+","+as_cod_versione_padre+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
										as_errore
				guo_functions.uof_log(" ERRORE FORMULA: " + as_errore ) 
				close righe_distinta_3;
				rollback;
				return -1
			end if
			
		end if
		//######################################################################################
		
		//prima l'inserimento veniva fatto solo se c'era una formula quantità utilizzo da calcolare
		//adesso va fatto se c'è una formula q.tà utilizzo, q.tà tecnica o sono stati cambiati prodotto figlio e/o versione figlio
		
		//Prima va fatto l'inserimento del codice prodotto e versione in tabella varianti, e poi valutate le formule sulle quantità,
		//questo per fare in modo che eventuali variabili su prodotto figlio e relativa versione vengano lette dalla tabella varianti con i nuovi valori eventualmente sostituiti
	
	
		//Se non devo inserire, passo al ramo successivo
		if not isnull(ls_cod_formula_quan_utilizzo) or not isnull(ls_cod_formula_quan_tecnica) or &
				ls_nuovo_figlio<>ls_cod_prodotto_figlio[ll_conteggio] or ls_nuova_versione_figlio<>ls_cod_versione_figlio[ll_conteggio] then
			//predispongo per inserimento
			
			if isnull(ls_cod_misura) then 
				ls_cod_misura = "null"
			else
				ls_cod_misura = "'" + ls_cod_misura + "'"
			end if
			
			if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
			if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
			
			ld_quan_tecnica = round(ld_quan_tecnica, 4)
			ld_quan_utilizzo = round(ld_quan_utilizzo, 4)
			
			if isnull(ld_dim_x) then ld_dim_x = 0
			if isnull(ld_dim_y) then ld_dim_y = 0
			if isnull(ld_dim_z) then ld_dim_z = 0
			if isnull(ld_dim_t) then ld_dim_t = 0
			if isnull(ld_coef_calcolo) then ld_coef_calcolo = 0
			if isnull(ls_des_estesa) then ls_des_estesa = ""
			if isnull(ls_formula_tempo) then ls_formula_tempo = ""
			if isnull(ls_flag_materia_prima) then ls_flag_materia_prima = "N"
			
			
			ls_sql_ins =" insert into " + ls_tabella + &
							"(cod_azienda," + &
							"anno_registrazione," + &
							"num_registrazione," + &
							ls_progressivo + "," + &
							"cod_prodotto_padre," + &
							"num_sequenza," + &
							"cod_prodotto_figlio," + &
							"cod_versione_figlio," + &
							"cod_versione," + &
							"cod_misura," + &
							"cod_prodotto," + &
							"cod_versione_variante," + &
							"quan_tecnica," + &
							"quan_utilizzo," + &
							"dim_x," + &
							"dim_y," + &
							"dim_z," + &
							"dim_t," + &
							"coef_calcolo," + &
							"flag_esclusione," + &
							"formula_tempo," + &
							"flag_materia_prima," + &
							"des_estesa)" + &
						" VALUES ('" +  s_cs_xx_cod_azienda + "', " + &
							string(al_anno_documento) + ", " + &   
							string(al_num_documento) + ", " + &   
							string(al_prog_riga_documento) + ", '" + &   
							as_cod_prodotto_padre + "', " + &
							string(ll_num_sequenza) + ", '" + &   
							ls_cod_prodotto_figlio[ll_conteggio] + "', '" + &
							ls_cod_versione_figlio[ll_conteggio] + "', '" + &
							as_cod_versione_padre + "'," + &
							ls_cod_misura + ", '" + &
							ls_nuovo_figlio + "', '" + &
							ls_nuova_versione_figlio + "', " + &
							f_double_to_string(ld_quan_tecnica) + ", " + &   
							f_double_to_string(ld_quan_utilizzo) + ", " + &
							f_double_to_string(ld_dim_x) + ", " + &   
							f_double_to_string(ld_dim_y) + ", " + &   
							f_double_to_string(ld_dim_z) + ", " + &   
							f_double_to_string(ld_dim_t) + ", " + &   
							f_double_to_string(ld_coef_calcolo) + ", '" + &   
							"N', '" + &
							ls_formula_tempo + "', '" + &
							ls_flag_materia_prima + "', '" + &
							ls_des_estesa + "')" 

			execute immediate :ls_sql_ins;
			if sqlca.sqlcode = -1 then
				as_errore = "Errore nell'inserimento della Variante (uo_conf_varianti - uof_valorizza_formule)~r~n" + sqlca.sqlerrtext
				guo_functions.uof_log(as_errore + " SQL:" + ls_sql_ins) 
				close righe_distinta_3;
				return -1
			end if
			
			guo_functions.uof_log(" .... insert variante " + ls_sql_ins ) 
			
			
			//valuto le formule quantità, se previsto
			if not isnull(ls_cod_formula_quan_utilizzo) then
				ll_ordine[1] = al_anno_documento
				ll_ordine[2] = al_num_documento
				ll_ordine[3] = al_prog_riga_documento
			
				lstr_distinta.cod_prodotto_padre = as_cod_prodotto_padre
				lstr_distinta.cod_versione_padre = as_cod_versione_padre
				lstr_distinta.num_sequenza = ll_num_sequenza
				lstr_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio[ll_conteggio]
				lstr_distinta.cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
				
				guo_functions.uof_log("Calcolo formula "+ls_cod_formula_quan_utilizzo+" della Q.TA' UTIL  ramo distinta senza variante .... PADRE:" + as_cod_prodotto_padre + "  FIGLIO:" + ls_cod_prodotto_figlio[ll_conteggio] ) 
				
				luo_formule_calcolo = create uo_formule_calcolo
				li_risposta = luo_formule_calcolo.uof_calcola_formula_db_v2(s_cs_xx_cod_azienda,ls_cod_formula_quan_utilizzo, ll_ordine[],  lstr_distinta, as_tipo_gestione, ld_quan_utilizzo, ls_temp, ls_tipo_ritorno, as_errore)
				
				guo_functions.uof_log("Risultato formula q.tà utilizzo=" + string(ld_quan_utilizzo) ) 
				
				if li_risposta<0 then
					as_errore = 	"F.la Q.tà Utilizzo: Ramo ("+as_cod_prodotto_padre+","+as_cod_versione_padre+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
											as_errore
					guo_functions.uof_log("Errore calcolo formula quantità util : " + as_errore)
					close righe_distinta_3;
					destroy luo_formule_calcolo
					rollback;
					return -1
				end if
				destroy luo_formule_calcolo
			end if
			
			if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
			
			ld_quan_utilizzo = round(ld_quan_utilizzo, 4)
			
			
			if not isnull(ls_cod_formula_quan_tecnica) then
				ll_ordine[1] = al_anno_documento
				ll_ordine[2] = al_num_documento
				ll_ordine[3] = al_prog_riga_documento
			
				lstr_distinta.cod_prodotto_padre = as_cod_prodotto_padre
				lstr_distinta.cod_versione_padre = as_cod_versione_padre
				lstr_distinta.num_sequenza = ll_num_sequenza
				lstr_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio[ll_conteggio]
				lstr_distinta.cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
				
				guo_functions.uof_log("Calcolo formule QUANTITA' TEC ramo distinta senza variante .... PADRE:" + as_cod_prodotto_padre + "  FIGLIO:" + ls_cod_prodotto_figlio[ll_conteggio] ) 
				luo_formule_calcolo = create uo_formule_calcolo

				li_risposta = luo_formule_calcolo.uof_calcola_formula_db_v2(s_cs_xx_cod_azienda,ls_cod_formula_quan_tecnica, ll_ordine[],  lstr_distinta, as_tipo_gestione, ld_quan_tecnica, ls_temp, ls_tipo_ritorno, as_errore)
				guo_functions.uof_log("Risultato formula q.tà tecnica=" + string(ld_quan_tecnica) ) 
				if li_risposta<0 then
					as_errore = 	"F.la Q.tà Tecnica: Ramo ("+as_cod_prodotto_padre+","+as_cod_versione_padre+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
											as_errore
											
					guo_functions.uof_log("Errore calcolo formula tec quantità util : " + as_errore)
					close righe_distinta_3;
					destroy luo_formule_calcolo
					rollback;
					return -1
				end if
				destroy luo_formule_calcolo
			end if
			
			if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
			
			ld_quan_tecnica = round(ld_quan_tecnica, 4)
			
			//ora predispongo per UPDATE
			//faccio update delle quantità calcolate
			ls_sql_upd ="update " + ls_tabella + " " +&
							"set quan_tecnica="+f_double_to_string(ld_quan_tecnica)+ "," +&
								 "quan_utilizzo="+f_double_to_string(ld_quan_utilizzo)+" "+ &
							"where cod_azienda='"+s_cs_xx_cod_azienda+"' and "+&
										"anno_registrazione="+string(al_anno_documento)+" and "+&
										"num_registrazione="+string(al_num_documento)+" and "+&
										ls_progressivo+"="+string(al_prog_riga_documento)+" and "+&
										"cod_prodotto_padre='"+as_cod_prodotto_padre+"' and "+&
										"cod_versione='"+as_cod_versione_padre+"' and "+&
										"num_sequenza="+string(ll_num_sequenza)+" and "+&
										"cod_prodotto_figlio='"+ls_cod_prodotto_figlio[ll_conteggio]+"' and "+&
										"cod_versione_figlio='"+ls_cod_versione_figlio[ll_conteggio]+"' "
										
			execute immediate :ls_sql_upd;
			
			if sqlca.sqlcode < 0 then
				as_errore = "Configuratore uof_ins_varianti: Errore in update quantità post sostituzione figlio/vers.figlio nella tabella " + ls_tabella + ": "+ sqlca.sqlerrtext
				guo_functions.uof_log("Configuratore uof_ins_varianti: Errore in update quantità post sostituzione figlio/vers.figlio nella tabella" + ls_tabella + " - SQL " +ls_sql_upd)
				close righe_distinta_3;
				rollback;
				return -1
			end if
			
			guo_functions.uof_log(" .... update variante " + ls_sql_upd ) 
			
			//Se le formule cod prodotto figlio e/o cod versione figlio hanno cambiato i valori faccio la sostituzione
			if ls_nuovo_figlio<>ls_cod_prodotto_figlio[ll_conteggio] then
				ls_cod_prodotto_figlio[ll_conteggio] = ls_nuovo_figlio
			end if
			
			if ls_nuova_versione_figlio<>ls_cod_versione_figlio[ll_conteggio] then
				ls_cod_versione_figlio[ll_conteggio] = ls_nuova_versione_figlio
			end if
			
		end if
		
	else
		//#################################################################
		// HO TROVATO LA VARIANTE; sicuramente la variante è già stata inserita con le formule valorizzate, ma esso potrebbe essere a sua volta
		// un componente semilavorato che contiene la distinta che quindi devo andare a controllare;
		// infatti potrebbero esserci figli con formula associata
		
		choose case  as_tipo_gestione
			case "ORD_VEN"
				select 	cod_prodotto,
						cod_versione_variante
				into   	:ls_cod_prodotto_variante,
							:ls_cod_versione_variante
				from   varianti_det_ord_ven
				where  cod_azienda         = :s_cs_xx_cod_azienda and
						 anno_registrazione  = :al_anno_documento and
						 num_registrazione   = :al_num_documento and
						 prog_riga_ord_ven   = :al_prog_riga_documento and
						 cod_prodotto_padre  = :as_cod_prodotto_padre and
						 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
						 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
						 cod_versione        = :as_cod_versione_padre and
						 num_sequenza = :ll_num_sequenza;
						 
			case "OFF_VEN"
				select 	cod_prodotto,
						cod_versione_variante
				into   	:ls_cod_prodotto_variante,
						:ls_cod_versione_variante
				from   varianti_det_off_ven
				where  cod_azienda         = :s_cs_xx_cod_azienda and
						 anno_registrazione  = :al_anno_documento and
						 num_registrazione   = :al_num_documento and
						 prog_riga_off_ven   = :al_prog_riga_documento and
						 cod_prodotto_padre  = :as_cod_prodotto_padre and
						 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
						 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
						 cod_versione        = :as_cod_versione_padre and
						 num_sequenza = :ll_num_sequenza;
					 
		end choose

		if sqlca.sqlcode = 100 or sqlca.sqlcode < 0 then
			as_errore = "Errore in ricerca della variante del ramo di distinta:~r~nPadre " + as_cod_prodotto_padre + " vers.:" + as_cod_versione_padre + &
																				"~r~nFiglio " + ls_cod_prodotto_figlio[ll_conteggio] + " vers.:" + ls_cod_versione_figlio[ll_conteggio] + &
																				"~r~n " + sqlca.sqlerrtext
			close righe_distinta_3;
			return -1
			
		end if		
		guo_functions.uof_log("Figlio "+ ls_cod_prodotto_figlio[ll_conteggio]+" con variante: " + ls_cod_prodotto_variante)
		
		ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
		ls_cod_versione_figlio[ll_conteggio] = ls_cod_versione_variante
		
	end if
	
	ll_conteggio++	
	
loop

close righe_distinta_3;


for ll_t = 1 to ll_conteggio -1
   SELECT count(*)
   INTO   :li_count  
   FROM   distinta  
   WHERE  cod_azienda = :s_cs_xx_cod_azienda and
			 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
			 cod_versione=:ls_cod_versione_figlio[ll_t];

	if sqlca.sqlcode=-1 then // Errore
		as_errore = "Errore nel DB: "+SQLCA.SQLErrText	
		rollback;
		return -1
	end if

	if li_count > 0 then
		li_risposta = uof_valorizza_formule(ls_cod_prodotto_figlio[ll_t], ls_cod_versione_figlio[ll_t], as_tipo_gestione, al_anno_documento, al_num_documento, al_prog_riga_documento,	ref as_errore )
		if li_risposta < 0 then
			return -1
		end if
	end if
next

return 0

end function

public function integer uof_valorizza_formule_prod_figlio (long al_anno_documento, long al_num_documento, long al_prog_riga_documento, string as_tipo_gestione, string as_padre, string as_versione_padre, long al_num_sequenza, ref string as_figlio, ref string as_versione_figlio, ref long al_sequenza_figlio, string as_formula_figlio, string as_formula_versione_figlio, ref string as_errore);long						al_rif_riga_ordine[]
s_chiave_distinta	ls_chiave_distinta
integer					li_ret
dec{4}					ld_valore
string					ls_valore, ls_tipo_ritorno, ls_sql, ls_messaggio
boolean					lb_cambiato
datastore				lds_data
uo_formule_calcolo	luo_formule_calcolo



as_errore = ""
lb_cambiato = false

al_rif_riga_ordine[1] = al_anno_documento
al_rif_riga_ordine[2] = al_num_documento
al_rif_riga_ordine[3] = al_prog_riga_documento

ls_chiave_distinta.cod_prodotto_padre = as_padre
ls_chiave_distinta.cod_versione_padre = as_versione_padre
ls_chiave_distinta.num_sequenza = al_num_sequenza
ls_chiave_distinta.cod_prodotto_figlio = as_figlio
ls_chiave_distinta.cod_versione_figlio = as_versione_figlio

setnull(ls_valore)
setnull(ld_valore)

ls_messaggio =""

//###################################################################################
if as_formula_figlio<>"" and not isnull(as_formula_figlio) then
	//valuto la formula sul cod.prodotto.figlio
	
	ls_messaggio += " F.la Figlio "+as_formula_figlio + " "
	
	guo_functions.uof_log("Calcolo formula " +as_formula_figlio )
	luo_formule_calcolo = create uo_formule_calcolo
	li_ret = luo_formule_calcolo.uof_calcola_formula_db_v2 (s_cs_xx_cod_azienda,	as_formula_figlio, al_rif_riga_ordine[], ls_chiave_distinta, as_tipo_gestione, ld_valore, ls_valore, ls_tipo_ritorno, as_errore)
	
	guo_functions.uof_log("Risultato formula " +ls_valore + " Errore="+string(li_ret) )
	destroy luo_formule_calcolo
	if li_ret<0 then
		return -1
	end if
	
	//il ritorno deve essere STRINGA
	if ls_tipo_ritorno<>"2" then
		//as_errore="Anomalia NON BLOCCANTE: La formula impostata sul prodotto figlio ha valore ritorno numerico al posto di stringa!"
		//return 1

	else
		//se ritorna VUOTO fai come se non ci fosse niente (Niente sostituzione prodotto figlio...)
		if ls_valore<>"" and not isnull(ls_valore) then
			
			if ls_valore<>as_figlio then
				//se è tornato qualcosa di diverso ... imposto il nuovo valore per il prodotto figlio
				if uof_controlla_figlio_formula( ls_valore) = 0 then
					as_figlio = ls_valore
					lb_cambiato = true
				end if
			end if
		end if
		
	end if
end if


setnull(ls_valore)
setnull(ld_valore)

//###################################################################################
if as_formula_versione_figlio<>"" and not isnull(as_formula_versione_figlio) then
	//valuto la formula sul cod.versione.figlio
	
	ls_messaggio += " F.la Vers.Figlio "+as_formula_versione_figlio + " "
	luo_formule_calcolo = create uo_formule_calcolo
	
	li_ret = luo_formule_calcolo.uof_calcola_formula_db_v2 (s_cs_xx_cod_azienda,	as_formula_versione_figlio, al_rif_riga_ordine[], ls_chiave_distinta, as_tipo_gestione, ld_valore, ls_valore, ls_tipo_ritorno, as_errore)
	
	guo_functions.uof_log("Risultato formula " +ls_valore + " Errore="+string(li_ret) )
	destroy luo_formule_calcolo
	
	if li_ret<0 then
		return -1
	end if
	
	//il ritorno deve essere STRINGA
	if ls_tipo_ritorno<>"2" then
//		as_errore="Anomalia NON BLOCCANTE: La formula impostata sulla versione figlio ha valore ritorno numerico al posto di stringa!"
//		return -1

	else
		
		//se ritorna VUOTO fai come se non ci fosse niente (Niente sostituzione versione prodotto figlio...)
		if ls_valore<>"" and not isnull(ls_valore) then
			
			if ls_valore<>as_versione_figlio then
				//se è tornato qualcosa di diverso ... imposto il nuovo valore per la versione del prodotto figlio
				as_versione_figlio = ls_valore
				lb_cambiato = true
			end if
			
		end if

	end if
end if


return 0

end function

public function integer uof_carica_mp_automatiche (string as_cod_modello_prodotto, decimal ad_dim_1, decimal ad_dim_2, decimal ad_dim_3, decimal ad_dim_4, decimal ad_altezza_asta, string as_cod_vernicitura_1, string as_cod_vernicitura_2, string as_cod_vernicitura_3, string as_cod_gruppo_variante_specifico, string as_cod_mp_automatica_specifica, ref str_varianti atr_mp_automatiche[], ref string as_errore) throws exception;/*	
EnMe 24-05-13: Funziona ricopiata dall'oggetto del configuratore Gibus uo_conf_varianti.uof_carica_mp_automatiche()

*/
string		ls_str, ls_cod_mp_non_richiesta, ls_cod_verniciatura, ls_flag_verniciatura[3], ls_asta, ls_cod_gruppo_variante, ls_cod_gruppi_scelti, ls_par1, ls_par2, ls_sql
long			ll_prog_mp_non_richiesta, ll_num_mp, ll_i, li_count, ll_ret, ll_row_mp
dec{4}		ld_quan_utilizzo, ld_quan_tecnica
datetime 	ldt_oggi
datastore	lds_data

// Verifico quante verniciature sono previste per il modello corrente.


ls_par1 = ""
guo_functions.uof_get_parametro_azienda("MP1",ls_par1 )
ls_par2 = ""
guo_functions.uof_get_parametro_azienda("MP2",ls_par2 )
if len(ls_par1) > 0 and not isnull(ls_par1) then
	ls_cod_gruppi_scelti += "'" + ls_par1 + "'"
end if
if len(ls_par1) > 0 and not isnull(ls_par1) then
	if len(ls_cod_gruppi_scelti) > 0 then ls_cod_gruppi_scelti += ","
	ls_cod_gruppi_scelti = ls_cod_gruppi_scelti + "'" + ls_par2 + "'"
end if


ls_flag_verniciatura[1] = "N"
ls_flag_verniciatura[2] = "N"
ls_flag_verniciatura[3] = "N"

select 	cod_variabile
into		:ls_str
from 		view_var_conf_prodotto
where	cod_azienda = :s_cs_xx_cod_azienda and
			cod_modello = :as_cod_modello_prodotto and
			nome_campo_database = 'cod_verniciatura';
if sqlca.sqlcode = 0 then
	ls_flag_verniciatura[1] = "S"
end if

select 	cod_variabile
into		:ls_str
from 		view_var_conf_prodotto
where	cod_azienda = :s_cs_xx_cod_azienda and
			cod_modello = :as_cod_modello_prodotto and
			nome_campo_database = 'cod_verniciatura_2';
if sqlca.sqlcode = 0 then
	ls_flag_verniciatura[2] = "S"
end if

select 	cod_variabile
into		:ls_str
from 		view_var_conf_prodotto
where	cod_azienda = :s_cs_xx_cod_azienda and
			cod_modello = :as_cod_modello_prodotto and
			nome_campo_database = 'cod_verniciatura_3';
if sqlca.sqlcode = 0 then
	ls_flag_verniciatura[3] = "S"
end if


ldt_oggi = datetime(today(), 00:00:00)

//**********************
//		ESTRAZIONE MP AUTOMATICHE PER 3 VERNICIATURE
//		EnMe 30-03-2012
// ----------------------------------

for ll_i = 1 to 3
	if  ls_flag_verniciatura[1]="S" or  ls_flag_verniciatura[2]="S" or  ls_flag_verniciatura[3]="S" then
		
		if  ls_flag_verniciatura[ll_i] = "N" then continue
		
		choose case ll_i
			case 1
				ls_cod_verniciatura = as_cod_vernicitura_1
			case 2
				ls_cod_verniciatura = as_cod_vernicitura_2
			case 3
				ls_cod_verniciatura = as_cod_vernicitura_3
		end choose

		if isnull(ls_cod_verniciatura) or len(ls_cod_verniciatura) < 1 then
			select cod_verniciatura  
			into   :ls_cod_verniciatura  
			from   tab_modelli_verniciature  
			where  cod_azienda = :s_cs_xx_cod_azienda and
					 cod_modello = :as_cod_modello_prodotto and  
					 flag_default = 'S' and
					 num_verniciatura = :ll_i and
					 flag_blocco  = 'N' ;
					 
			if sqlca.sqlcode = 100 then
				ls_cod_verniciatura = ""
			end if
			if sqlca.sqlcode = -1 then
				as_errore = "Errore in lettura tabella tab_modelli_verniciature. ~r~nDettaglio errore: " + sqlca.sqlerrtext
				return -1
			end if
			
		end if 
	end if
	
	// se non ci sono verniciature, faccio solo 1 giro
	if ls_flag_verniciatura[ll_i]="N"and ll_i > 1 then continue
	if ls_flag_verniciatura[1] = "N" and ll_i = 1 then setnull(ls_cod_verniciatura)

//		if  len(ls_cod_verniciatura) > 0 and not isnull(ls_cod_verniciatura) then
		
	ls_sql = " select 	tab_mp_non_richieste.cod_mp_non_richiesta, " + &
				" tab_mp_non_richieste.prog_mp_non_richiesta, " + &
				" tab_mp_non_richieste.cod_gruppo_variante, " + &
				" tab_mp_non_richieste.quan_utilizzo, " + &
				" tab_mp_non_richieste.quan_tecnica, " + &
				" tab_mp_non_richieste.quan_commerciale " + &
	 " FROM 	tab_mp_non_richieste "
	 
	 if not isnull(ls_cod_verniciatura) and len( trim(ls_cod_verniciatura)) > 0 then
		ls_sql = ls_sql +  " join		tab_mp_automatiche_mod_vern  on  	tab_mp_automatiche_mod_vern.cod_azienda = tab_mp_non_richieste.cod_azienda and " + &
										 "  tab_mp_automatiche_mod_vern.cod_mp_non_richiesta = tab_mp_non_richieste.cod_mp_non_richiesta and " + &
										 "  tab_mp_automatiche_mod_vern.prog_mp_non_richiesta = tab_mp_non_richieste.prog_mp_non_richiesta "
	end if
	
	ls_sql = ls_sql + 	" WHERE	( " + f_decimal_to_string(ad_dim_1) + " between lim_inf_dim_1 and lim_sup_dim_1  or  lim_sup_dim_1 = 0 or lim_sup_dim_1 is null ) and   " + &
				" ( "   + f_decimal_to_string(ad_dim_2) + " between lim_inf_dim_2 and lim_sup_dim_2   or  lim_sup_dim_2 = 0 or lim_sup_dim_2 is null) and " + &
				" ( "   + f_decimal_to_string(ad_dim_3) + " between lim_inf_dim_3 and lim_sup_dim_3   or  lim_sup_dim_3 = 0 or lim_sup_dim_3 is null) and " + &
				" ( "   + f_decimal_to_string(ad_dim_4) + " between lim_inf_dim_4 and lim_sup_dim_4   or  lim_sup_dim_4 = 0 or lim_sup_dim_4 is null) and " + &
				" ( tab_mp_non_richieste.cod_azienda = '" + s_cs_xx_cod_azienda +"') and " + &
				" ( tab_mp_non_richieste.flag_asta = 'N') and " + &
				" ( tab_mp_non_richieste.flag_blocco = 'N' or  ( tab_mp_non_richieste.flag_blocco = 'S' AND  tab_mp_non_richieste.data_blocco > '" + string(ldt_oggi,s_cs_xx_db_funzioni_formato_data) + "') )  " 
				
	if not isnull(as_cod_gruppo_variante_specifico) and len(trim(as_cod_gruppo_variante_specifico)) > 0 then
		ls_sql = ls_sql + " and tab_mp_non_richieste.cod_gruppo_variante = '" + as_cod_gruppo_variante_specifico + "' "
		
		if not isnull(as_cod_mp_automatica_specifica) and len(trim(as_cod_mp_automatica_specifica)) > 0  then
			ls_sql = ls_sql + " and tab_mp_non_richieste.cod_mp_non_richiesta = '" + as_cod_mp_automatica_specifica + "' "
		end if

	else
		ls_sql = ls_sql + " and tab_mp_non_richieste.cod_gruppo_variante not in (" + ls_cod_gruppi_scelti + ") "
	end if
	
	
	 if not isnull(ls_cod_verniciatura) and len(trim(ls_cod_verniciatura)) > 0 then
		ls_sql = ls_sql + 	" and tab_mp_automatiche_mod_vern.cod_verniciatura = '" + ls_cod_verniciatura +"' and  tab_mp_automatiche_mod_vern.num_verniciatura = " + string(ll_i) + &
								" and tab_mp_automatiche_mod_vern.cod_modello = '" + as_cod_modello_prodotto + "' "
	else
		ls_sql = ls_sql + " and tab_mp_non_richieste.cod_modello_tenda = '" + as_cod_modello_prodotto + "' "
	end if
		
	
	ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql)
	
	if ll_ret < 0 then
		as_errore = "Errore in creazione datastore estrazione materie prime automatiche (uof_carica_mp_automatiche) "
		return -1
	end if

	// aggiunta alle MP che aveva caricato in precedenza
	li_count = upperbound( atr_mp_automatiche )
	
	for ll_row_mp = 1 to lds_data.rowcount()
	
		li_count ++
		atr_mp_automatiche[li_count].cod_mp_variante = lds_data.getitemstring(ll_row_mp, 1)
		atr_mp_automatiche[li_count].cod_gruppo_variante = lds_data.getitemstring(ll_row_mp, 3)
		atr_mp_automatiche[li_count].quan_utilizzo = lds_data.getitemnumber(ll_row_mp, 4)
		atr_mp_automatiche[li_count].quan_tecnica = lds_data.getitemnumber(ll_row_mp, 5)
//		atr_mp_automatiche[li_count].quan_commerciale = lds_data.getitemnumber(ll_row_mp, 6)
		atr_mp_automatiche[li_count].quan_commerciale = 99
	next

	destroy lds_data

next



// ---------------------  selezione dell'asta in funzione dell'altezza comando -------------------------------------

return 0
end function

public function integer uof_trova_mat_prime_varianti (string as_cod_prodotto_finito, string as_cod_versione_finito, long al_num_sequenza, string as_tabella_varianti, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, string as_nome_colonna_quan, decimal ad_quan_utilizzo_prec, ref s_trova_mp_varianti fstr_trova_mp_varianti[], ref string as_errore);/*
 nome: uof_trova_mat_prime_varianti
 tipo: integer
  
	Variabili passate: 		nome					 tipo				passaggio per			commento
							
							 as_cod_prodotto_finito
							 as_cod_versione_finito
							 as_tabella_varianti		
							 al_anno_documento	 	
							 al_num_documento	 	
							 al_prog_riga_documento	
							 as_nome_colonna_quan					-> nome della colonna quantità da estrarre (quan_utilizzo oppure quan_tecnica)
							 ad_quan_utilizzo_prec					-> quantità utilizzo del padre
							 fstr_trova_mp_varianti[]					-> struttura per ritornare i dati
*/

string		ls_cod_prodotto_padre[], ls_cod_versione_padre[], ls_cod_prodotto_figlio[], ls_cod_versione_figlio[], &
			ls_flag_materia_prima[],ls_flag_ramo_descrittivo[]

string		ls_test,ls_cod_prodotto_variante, ls_sql,    ls_flag_materia_prima_variante,ls_errore,  ls_cod_versione_variante, &
			ls_cod_semilavorato, ls_flag_scarico_parziale, ls_cod_deposito_produzione, ls_cod_prodotto_padre_variante, ls_cod_versione_padre_variante

long     ll_conteggio,ll_t,ll_max, ll_cont_figli, ll_cont_figli_integrazioni, ll_num_sequenza[]

integer  li_risposta

decimal  ldd_quantita_utilizzo[], ldd_quan_utilizzo_variante,ld_ordinamento[], ld_ordinamento_variante

declare cu_varianti dynamic cursor for sqlsa;

DynamicStagingArea sqlsb

sqlsb = CREATE DynamicStagingArea

declare righe_distinta_3 dynamic cursor for sqlsb;

guo_functions.uof_log("Uo_conf_varianti.uof_trova_mat_prime_varianti: ingresso FUNCTION" )

if isnull(as_cod_prodotto_finito) or len(as_cod_prodotto_finito) < 1 then
	as_errore = "Manca l'indicazione del prodotto finito."
	return -1
end if

if isnull(as_cod_versione_finito) or len(as_cod_versione_finito) < 1 then
	as_errore = guo_functions.uof_format("Al prodotto finito " + as_cod_prodotto_finito + " manca l'indicazione del prodotto finito.")
	return -1
end if

ll_conteggio = 1

//declare righe_distinta_3 cursor for 


guo_functions.uof_log("Uo_conf_varianti.uof_trova_mat_prime_varianti: dopo declare cursor. SQLCODE="  + guo_functions.uof_format(sqlca.sqlcode) +  "  SQLERRTEXT=" + guo_functions.uof_format(sqlca.sqlerrtext))

ls_sql = "select  cod_prodotto_padre, cod_versione, cod_prodotto_figlio, cod_versione_figlio, "+ as_nome_colonna_quan +", flag_materia_prima, flag_ramo_descrittivo, num_sequenza, lead_time from distinta " + &
			" where   cod_azienda = '" + s_cs_xx_cod_azienda + "' " + &
			" and     cod_prodotto_padre = '" + as_cod_prodotto_finito + "' " + &
			" and     cod_versione= '" + as_cod_versione_finito + "' " 
			

// sono in un documento, quindi potrebbero esserci integrazioni
if not isnull(al_anno_documento) and not isnull(al_num_documento) then
		choose case as_tabella_varianti
				
			case "varianti_commesse"
				
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_padre, cod_versione_padre, cod_prodotto_figlio, cod_versione_figlio, "+as_nome_colonna_quan +", flag_materia_prima, 'N', num_sequenza, lead_time " + &
							 " FROM integrazioni_commessa " + & 
				          " where   cod_azienda = '" + s_cs_xx_cod_azienda + "' " + &
							 " and     anno_commessa = " + string(al_anno_documento)  + &
							 " and     num_commessa = "  + string(al_num_documento) + &
							 " and     cod_prodotto_padre = '" + as_cod_prodotto_finito + "' " + &
							 " and 	  cod_versione_padre = '" + as_cod_versione_finito + "' "
				
			case "varianti_det_off_ven"
				
				guo_functions.uof_log("Uo_conf_varianti.uof_trova_mat_prime_varianti: CURSOR SQL2=" + guo_functions.uof_format(ls_sql) )
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_padre, cod_versione_padre, cod_prodotto_figlio, cod_versione_figlio, "+as_nome_colonna_quan +", flag_materia_prima, 'N', num_sequenza, lead_time  " + &
							 " FROM integrazioni_det_off_ven " + & 
				          " where   cod_azienda = '" + s_cs_xx_cod_azienda + "' " + &
							 " and     anno_registrazione = " + string(al_anno_documento)  + &
							 " and     num_registrazione = "  + string(al_num_documento) + &
							 " and     prog_riga_off_ven = "  + string(al_prog_riga_documento) + &
							 " and     cod_prodotto_padre = '" + as_cod_prodotto_finito + "' " + &
							 " and     cod_versione_padre = '" + as_cod_versione_finito + "' "
				
				
			case "varianti_det_ord_ven"
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_padre, cod_versione_padre, cod_prodotto_figlio, cod_versione_figlio, "+as_nome_colonna_quan +", flag_materia_prima, 'N', num_sequenza, lead_time  " + &
							 " FROM integrazioni_det_ord_ven " + & 
				          " where   cod_azienda = '" + s_cs_xx_cod_azienda + "' " + &
							 " and     anno_registrazione = " + string(al_anno_documento)  + &
							 " and     num_registrazione = "  + string(al_num_documento) + &
							 " and     prog_riga_ord_ven = "  + string(al_prog_riga_documento) + &
							 " and     cod_prodotto_padre = '" + as_cod_prodotto_finito + "' " + &
							 " and     cod_versione_padre = '" + as_cod_versione_finito + "' "
				
			case "varianti_det_trattative"
				
				ls_sql += " union all " + &
				          " SELECT  cod_prodotto_padre, cod_versione_padre, cod_prodotto_figlio, cod_versione_figlio, "+as_nome_colonna_quan +", flag_materia_prima, 'N', num_sequenza, lead_time  " + &
							 " FROM    integrazioni_det_trattative " + & 
				          " where   cod_azienda = '" + s_cs_xx_cod_azienda + "' " + &
							 " and     anno_trattativa = " + string(al_anno_documento)  + &
							 " and     num_trattativa = "  + string(al_num_documento) + &
							 " and     prog_trattativa = "  + string(al_prog_riga_documento) + &
							 " and     cod_prodotto_padre = '" + as_cod_prodotto_finito + "' " + &
							 " and     cod_versione_padre = '" + as_cod_versione_finito + "' "
				
		end choose
end if


prepare sqlsb from :ls_sql;

open dynamic righe_distinta_3;
if sqlca.sqlcode < 0 then
	as_errore =  guo_functions.uof_format("Errore nell'operazione OPEN del cursore righe_distinta_3 (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext)
	close righe_distinta_3;
	return -1
end if

//open righe_distinta_3;
guo_functions.uof_log("Uo_conf_varianti.uof_trova_mat_prime_varianti: CURSOR SQL=" + guo_functions.uof_format(ls_sql) )

do while true

	fetch righe_distinta_3 
	into  	:ls_cod_prodotto_padre[ll_conteggio],
			:ls_cod_versione_padre[ll_conteggio],
			:ls_cod_prodotto_figlio[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
			:ldd_quantita_utilizzo[ll_conteggio],
			:ls_flag_materia_prima[ll_conteggio],
			:ls_flag_ramo_descrittivo[ll_conteggio],
			:ll_num_sequenza[ll_conteggio],
			:ld_ordinamento[ll_conteggio];

   if sqlca.sqlcode = 100 then 
		close righe_distinta_3;	
		exit
	end if

	if sqlca.sqlcode < 0 then
		as_errore =  guo_functions.uof_format("Errore nell'operazione FETCH del cursore righe_distinta_3 (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext)
		close righe_distinta_3;
		return -1
	end if
	
	if ls_flag_ramo_descrittivo[ll_conteggio] = "S" then continue
	
	// verifico se mi trovo in presenza di un documento e quindi di una variante
	if not isnull(al_anno_documento) and not isnull(al_num_documento) then
		
		ls_sql = "select cod_prodotto_padre, cod_versione, "+as_nome_colonna_quan +", cod_prodotto, cod_versione_variante, flag_materia_prima "
					
		if as_tabella_varianti = "varianti_commesse" then
			ls_sql += ", flag_scarico_parziale " 
		else
			ls_sql += ", 'N' "
		end if
			
		ls_sql += " , lead_time from " + as_tabella_varianti + " where  cod_azienda='" + s_cs_xx_cod_azienda + "' "
		
		choose case as_tabella_varianti
			case "varianti_commesse"
					ls_sql = ls_sql + " and anno_commessa=" + string(al_anno_documento) + &
											" and num_commessa=" + string(al_num_documento) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_off_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_documento) + &
											" and num_registrazione=" + string(al_num_documento) + &
											" and prog_riga_off_ven=" + string(al_prog_riga_documento) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_ord_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_documento) + &
											" and num_registrazione=" + string(al_num_documento) + &
											" and prog_riga_ord_ven=" + string(al_prog_riga_documento) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_trattative"
					ls_sql = ls_sql + " and anno_trattativa=" + string(al_anno_documento) + &
											" and num_trattativa=" + string(al_num_documento) + &
											" and prog_trattativa=" + string(al_prog_riga_documento) + &
											" and flag_esclusione<>'S'"
		end choose
		ls_sql = ls_sql + " and cod_prodotto_padre='" + as_cod_prodotto_finito + "'" + &
								" and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_conteggio] + "'" + &
								" and cod_versione='" + as_cod_versione_finito + "'" + &
								" and cod_versione_figlio = '" + ls_cod_versione_figlio[ll_conteggio] + "'" + &
								" and num_sequenza = " + string(ll_num_sequenza[ll_conteggio]) + " "
		
		prepare sqlsa from :ls_sql;
		
		open dynamic cu_varianti;
		
		if sqlca.sqlcode < 0 then
			as_errore =  guo_functions.uof_format("Errore nell'operazione OPEN del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext)
			return -1
		end if

		fetch cu_varianti 
		into 	:ls_cod_prodotto_padre_variante,
				:ls_cod_versione_padre_variante,
				:ldd_quan_utilizzo_variante, 
				:ls_cod_prodotto_variante,
				:ls_cod_versione_variante,
				:ls_flag_materia_prima_variante,
				:ls_flag_scarico_parziale,
				:ld_ordinamento_variante;
				
		if sqlca.sqlcode < 0 then
			as_errore =  guo_functions.uof_format("Errore nell'operazione FETCH del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext)
			close cu_varianti;
			return -1
		end if

		if sqlca.sqlcode <> 100  then
			ls_cod_prodotto_padre[ll_conteggio] = ls_cod_prodotto_padre_variante
			ls_cod_versione_padre[ll_conteggio] = ls_cod_versione_padre_variante
			ldd_quantita_utilizzo[ll_conteggio]=ldd_quan_utilizzo_variante
			ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
			ls_cod_versione_figlio[ll_conteggio] = ls_cod_versione_variante
			ls_flag_materia_prima[ll_conteggio] = ls_flag_materia_prima_variante
			//ld_ordinamento[ll_conteggio] = ld_ordinamento_variante
			if ls_flag_scarico_parziale = "S" then ls_flag_materia_prima[ll_conteggio] = "S"
		end if
	
		close cu_varianti;
	end if

	guo_functions.uof_log("Uo_conf_varianti.uof_trova_mat_prime_varianti: Padre=" + guo_functions.uof_format(ls_cod_prodotto_padre[ll_conteggio]) + " - Figlio  " + guo_functions.uof_format(ls_cod_prodotto_figlio[ll_conteggio]) )

	ldd_quantita_utilizzo[ll_conteggio]=ldd_quantita_utilizzo[ll_conteggio]*ad_quan_utilizzo_prec
	ll_conteggio++	

loop

close righe_distinta_3;

for ll_t = 1 to ll_conteggio -1
	
   if ls_flag_materia_prima[ll_t] = 'N' then
		
		ll_cont_figli = 0
		
		// controllo se esistono dei sotto-rami di distinta
		SELECT count(*)
		into   :ll_cont_figli
		FROM   distinta  
		WHERE  cod_azienda = :s_cs_xx_cod_azienda AND    
				 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t]	and    
				 cod_versione = :ls_cod_versione_figlio[ll_t] ;
		
		if sqlca.sqlcode<0 then
			as_errore =  guo_functions.uof_format("Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext)
			return -1
		end if
		
		// sono in un documento, quindi potrebbero esserci integrazioni
		if not isnull(al_anno_documento) and not isnull(al_num_documento) then
			
				choose case as_tabella_varianti
						
					case "varianti_commesse"
						
						ll_cont_figli_integrazioni = 0
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_commessa
						where  cod_azienda = :s_cs_xx_cod_azienda and
								 anno_commessa = :al_anno_documento and
								 num_commessa = :al_num_documento and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t] and
								 num_sequenza = :ll_num_sequenza[ll_t];
						
						if sqlca.sqlcode < 0 then
							as_errore =  guo_functions.uof_format("Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext)
							return -1
						end if
						
					case "varianti_det_off_ven"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_off_ven
						where  cod_azienda = :s_cs_xx_cod_azienda and
								 anno_registrazione = :al_anno_documento and
								 num_registrazione = :al_num_documento and
								 prog_riga_off_ven = :al_prog_riga_documento and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t] and
								 num_sequenza = :ll_num_sequenza[ll_t];
						
						if sqlca.sqlcode < 0 then
							as_errore =  guo_functions.uof_format("Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext)
							return -1
						end if
					
					case "varianti_det_ord_ven"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_ord_ven
						where  cod_azienda = :s_cs_xx_cod_azienda and
								 anno_registrazione = :al_anno_documento and
								 num_registrazione = :al_num_documento and
								 prog_riga_ord_ven = :al_prog_riga_documento and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t] and
								 num_sequenza = :ll_num_sequenza[ll_t];
						
						if sqlca.sqlcode < 0 then
							as_errore =  guo_functions.uof_format("Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext)
							return -1
						end if
					
					case "varianti_det_trattative"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_trattative
						where  cod_azienda = :s_cs_xx_cod_azienda and
								 anno_trattativa = :al_anno_documento and
								 num_trattativa  = :al_num_documento and
								 prog_trattativa    = :al_prog_riga_documento and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_prodotto_figlio[ll_t] and
								 num_sequenza = :ll_num_sequenza[ll_t];
						
						if sqlca.sqlcode < 0 then
							as_errore =  guo_functions.uof_format("Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext)
							return -1
						end if
					
				end choose
		end if
		
		ll_cont_figli = ll_cont_figli + ll_cont_figli_integrazioni


		// non ci sono figli in distinta, verifico la variante 
		// (quindi non può essere neanche una integrazione, visto che non è ammessa la variante di una integrazione)
		
		if ll_cont_figli < 1 then
			// quindi se non ci sono figli vuol dire che è una materia prima
			
			if not isnull(al_anno_documento) and not isnull(al_num_documento) then
				ls_sql = "select cod_prodotto_padre, cod_versione, "+as_nome_colonna_quan +", cod_prodotto, cod_versione_variante " + &
							" from " + as_tabella_varianti + &
							" where  cod_azienda='" + s_cs_xx_cod_azienda + "'"
				choose case as_tabella_varianti
					case "varianti_commesse"
							ls_sql = ls_sql + " and anno_commessa=" + string(al_anno_documento) + &
													" and num_commessa=" + string(al_num_documento) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_off_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_documento) + &
													" and num_registrazione=" + string(al_num_documento) + &
													" and prog_riga_off_ven=" + string(al_prog_riga_documento) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_ord_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_documento) + &
													" and num_registrazione=" + string(al_num_documento)  + &
													" and prog_riga_ord_ven=" + string(al_prog_riga_documento) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_trattative"
							ls_sql = ls_sql + " and anno_trattativa=" + string(al_anno_documento) + &
													" and num_trattativa=" + string(al_num_documento) + &
													" and prog_trattativa=" + string(al_prog_riga_documento) + &
													" and flag_esclusione<>'S'"
				end choose
				ls_sql = ls_sql + 	" and cod_prodotto_padre='" + as_cod_prodotto_finito + "'" + &
										" and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
										" and cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
										" and cod_versione='" + as_cod_versione_finito + "'"   + &
										" and num_sequenza = " + string(ll_num_sequenza[ll_t]) + " "
				
				prepare sqlsa from :ls_sql;
	
				open dynamic cu_varianti;
				if sqlca.sqlcode < 0 then
					as_errore =  guo_functions.uof_format("Errore nell'operazione OPEN del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext)
					close cu_varianti;
					return -1
				end if
	
				fetch cu_varianti 
				into    :ls_cod_prodotto_padre_variante,
						:ls_cod_versione_padre_variante,
						:ldd_quan_utilizzo_variante, 
						:ls_cod_prodotto_variante,
						:ls_cod_versione_variante;
			
				
				if sqlca.sqlcode < 0 then
					as_errore =  guo_functions.uof_format("Errore nell'operazione FETCH del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext)
					close cu_varianti;
					return -1
				end if
			
				ll_max=upperbound(fstr_trova_mp_varianti)
				ll_max++		
				fstr_trova_mp_varianti[ll_max].cod_prodotto_padre = ls_cod_prodotto_padre[ll_t]
				fstr_trova_mp_varianti[ll_max].cod_versione_padre = ls_cod_versione_padre[ll_t]
				fstr_trova_mp_varianti[ll_max].cod_materia_prima = ls_cod_prodotto_figlio[ll_t]
				fstr_trova_mp_varianti[ll_max].cod_versione_mp = ls_cod_versione_figlio[ll_t]
				if lower(as_nome_colonna_quan) = "quan_utilizzo" then
					fstr_trova_mp_varianti[ll_max].quan_utilizzo = ldd_quantita_utilizzo[ll_t]
				else
					fstr_trova_mp_varianti[ll_max].quan_tecnica = ldd_quantita_utilizzo[ll_t]
				end if
				fstr_trova_mp_varianti[ll_max].ordinamento = ld_ordinamento[ll_t]
				
				
				if sqlca.sqlcode <> 100  then
					ldd_quan_utilizzo_variante = ldd_quan_utilizzo_variante * ad_quan_utilizzo_prec
					ls_cod_prodotto_padre[ll_t] = ls_cod_prodotto_padre_variante
					ls_cod_versione_padre[ll_t] = ls_cod_versione_padre_variante
					ldd_quantita_utilizzo[ll_t]=ldd_quan_utilizzo_variante
					ls_cod_prodotto_figlio[ll_t] = ls_cod_prodotto_variante
					ls_cod_versione_figlio[ll_t] = ls_cod_versione_variante
					
					fstr_trova_mp_varianti[ll_max].cod_prodotto_padre = ls_cod_prodotto_padre_variante
					fstr_trova_mp_varianti[ll_max].cod_versione_padre = ls_cod_versione_padre_variante
					fstr_trova_mp_varianti[ll_max].cod_materia_prima =  ls_cod_prodotto_variante
					fstr_trova_mp_varianti[ll_max].cod_versione_mp = ls_cod_versione_variante
					if lower(as_nome_colonna_quan) = "quan_utilizzo" then
						fstr_trova_mp_varianti[ll_max].quan_utilizzo = ldd_quan_utilizzo_variante
					else
						fstr_trova_mp_varianti[ll_max].quan_tecnica = ldd_quan_utilizzo_variante
					end if
					fstr_trova_mp_varianti[ll_max].ordinamento = ld_ordinamento[ll_t]
					
				end if
	
				close cu_varianti;
			end if
		
		else

			li_risposta = uof_trova_mat_prime_varianti(ls_cod_prodotto_figlio[ll_t], &
																  ls_cod_versione_figlio[ll_t], & 
																  ll_num_sequenza[ll_t], &
																  as_tabella_varianti, &
																  al_anno_documento,al_num_documento,al_prog_riga_documento, &
																  as_nome_colonna_quan, &
																  ldd_quantita_utilizzo[ll_t], &
																  fstr_trova_mp_varianti[], &
																	ls_errore)

			if li_risposta = -1 then 
				as_errore = ls_errore
				return -1
			end if
			
	
		end if
		
	else
		// si tratta di una materia prima forzata (flag_materia_prima=S)
		
		ll_max=upperbound(fstr_trova_mp_varianti[])
		ll_max++		
		
		fstr_trova_mp_varianti[ll_max].cod_prodotto_padre = ls_cod_prodotto_padre[ll_t]
		fstr_trova_mp_varianti[ll_max].cod_versione_padre = ls_cod_versione_padre[ll_t]
		fstr_trova_mp_varianti[ll_max].cod_materia_prima =  ls_cod_prodotto_figlio[ll_t]
		fstr_trova_mp_varianti[ll_max].cod_versione_mp = ls_cod_versione_figlio[ll_t]
		if lower(as_nome_colonna_quan) = "quan_utilizzo" then
			fstr_trova_mp_varianti[ll_max].quan_utilizzo = ldd_quantita_utilizzo[ll_t]
		else
			fstr_trova_mp_varianti[ll_max].quan_tecnica = ldd_quantita_utilizzo[ll_t]
		end if
		fstr_trova_mp_varianti[ll_max].ordinamento = ld_ordinamento[ll_t]
		
		if not isnull(al_anno_documento) and not isnull(al_num_documento) then
			ls_sql = "select  cod_prodotto_padre, cod_versione, "+as_nome_colonna_quan +",cod_prodotto, cod_versione_variante " + &
						" from " + as_tabella_varianti + &
						" where  cod_azienda='" + s_cs_xx_cod_azienda + "'"
			choose case as_tabella_varianti
				case "varianti_commesse"
						ls_sql = ls_sql + " and anno_commessa=" + string(al_anno_documento) + &
												" and num_commessa=" + string(al_num_documento) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_off_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_documento) + &
												" and num_registrazione=" + string(al_num_documento) + &
												" and prog_riga_off_ven=" + string(al_prog_riga_documento) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_ord_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_documento) + &
												" and num_registrazione=" + string(al_num_documento)  + &
												" and prog_riga_ord_ven=" + string(al_prog_riga_documento) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_trattative"
						ls_sql = ls_sql + " and anno_trattativa=" + string(al_anno_documento) + &
												" and num_trattativa=" + string(al_num_documento) + &
												" and prog_trattativa=" + string(al_prog_riga_documento) + &
												" and flag_esclusione<>'S'"
			end choose
			ls_sql = ls_sql + " and    cod_prodotto_padre='" + as_cod_prodotto_finito + "'" + &
									" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
									" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
									" and    cod_versione='" + as_cod_versione_finito + "'"  
			
			prepare sqlsa from :ls_sql;

			open dynamic cu_varianti;
			if sqlca.sqlcode < 0 then
				as_errore =  guo_functions.uof_format("Errore nell'operazione OPEN del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext)
				close cu_varianti;
				return -1
			end if

			fetch cu_varianti 
			into    :ls_cod_prodotto_padre_variante,
					:ls_cod_versione_padre_variante,
					:ldd_quan_utilizzo_variante, 
					:ls_cod_prodotto_variante,
					:ls_cod_versione_variante;
		
			if sqlca.sqlcode < 0 then
				as_errore =  guo_functions.uof_format("Errore nell'operazione FETCH del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext)
				close cu_varianti;
				return -1
			end if
		
			if sqlca.sqlcode <> 100  then
				ldd_quan_utilizzo_variante = ldd_quan_utilizzo_variante * ad_quan_utilizzo_prec
				ldd_quantita_utilizzo[ll_t]=ldd_quan_utilizzo_variante
				ls_cod_prodotto_figlio[ll_t] = ls_cod_prodotto_variante
				ls_cod_versione_figlio[ll_t] = ls_cod_versione_variante
				ls_cod_prodotto_padre[ll_t] = ls_cod_prodotto_padre_variante
				ls_cod_versione_padre[ll_t] = ls_cod_versione_padre_variante
				
				fstr_trova_mp_varianti[ll_max].cod_prodotto_padre = ls_cod_prodotto_padre_variante
				fstr_trova_mp_varianti[ll_max].cod_versione_padre = ls_cod_versione_padre_variante
				fstr_trova_mp_varianti[ll_max].cod_materia_prima =  ls_cod_prodotto_variante
				fstr_trova_mp_varianti[ll_max].cod_versione_mp = ls_cod_versione_variante
				if lower(as_nome_colonna_quan) = "quan_utilizzo" then
					fstr_trova_mp_varianti[ll_max].quan_utilizzo = ldd_quan_utilizzo_variante
				else
					fstr_trova_mp_varianti[ll_max].quan_tecnica = ldd_quan_utilizzo_variante
				end if					
				fstr_trova_mp_varianti[ll_max].ordinamento = ld_ordinamento[ll_t]
			end if

			close cu_varianti;
			
		end if
			
	end if
	
next

return 0

end function

public function integer uof_controlla_figlio_formula (string as_codice);long			ll_count

setnull(ll_count)

select count(*)
into :ll_count
from anag_prodotti
where 	cod_azienda = :s_cs_xx_cod_azienda and
			cod_prodotto = :as_codice;

if ll_count > 0 then
	return 0
else
	return -1
end if

end function

public function integer uof_ricerca_quan_mp_variante (string as_cod_prodotto, string as_cod_versione, decimal ad_quan_prodotto, string as_cod_gruppo_variante, long al_anno_registrazione, long al_num_registrazione, long al_prog_registrazione, string as_tabella_varianti, ref string as_cod_materia_prima, ref decimal ad_quan_materia_prima, ref string as_errore);/* ------------------------------------------------------------------------------------
	Funzione che trova la materia prima e la sua quantità partendo dal gruppo variante
 nella tabella <tabella_varianti>

 nome: uof_ricerca_quantita_mp
 tipo: integer

 return:
 		-1 errore
		 0 trovato materia prima tutto OK
  		 1 trovato nulla
		 2 valido solo all'interno indica l'uscita

	Variabili passate: 		nome					 tipo				passaggio per			commento
							

	Creata il 10-05-2000 Enrico 
	modificata il 31/10/2006 per adeguarla alla nuova gestione distinta base con versioni figli diversi dalla versione padre
 ------------------------------------------------------------------------------------
*/
string  		ls_cod_prodotto_figlio[],ls_cod_prodotto_variante, ls_sql, ls_flag_materia_prima, &
		  		ls_flag_materia_prima_variante,ls_errore, ls_cod_gruppo_variante, ls_cod_versione_figlio[], &
		  		ls_cod_versione_variante, ls_sql_varianti, ls_tabella_varianti
long    		ll_conteggio,ll_t,ll_max, ll_i, ll_cont, ll_num_legami, ll_num_sequenza,ll_test, ll_rows, ll_variante, ll_tot_varianti, ll_y
integer 		li_risposta
dec{4}  		ldd_quantita_utilizzo,ldd_quan_utilizzo_variante
datastore	lds_varianti, lds_det_varianti

ll_conteggio = 1

guo_functions.uof_log("Ingresso Funzione uof_ricerca_quan_mp_variante. Prodotto Finito:" + guo_functions.uof_format(as_cod_prodotto))

guo_functions.uof_log("Ingresso Function uof_ricerca_quan_mp_variante. test 1")


ls_sql_varianti = "select  cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, num_sequenza from distinta where cod_azienda = '" + s_cs_xx_cod_azienda + "' and cod_prodotto_padre = '" + as_cod_prodotto + "' and     cod_versione= '" + as_cod_versione + "' "
guo_functions.uof_log("SQL ricerca distinta base =" + guo_functions.uof_format(ls_sql_varianti))

ll_rows = guo_functions.uof_crea_datastore(lds_varianti, ls_sql_varianti)

if ll_rows < 0 then
	guo_functions.uof_log("uof_ricerca_quan_mp_variante(): Errore nel datastore lds_varianti")
	as_errore = "uof_ricerca_quan_mp_variante(): Errore nel datastore lds_varianti "
	return -1
end if

guo_functions.uof_log("Trovato " + guo_functions.uof_format(ll_rows) + " righe per il padre " + as_cod_prodotto)

choose case upper(as_tabella_varianti)
	case "OFF_VEN"
		ls_tabella_varianti = "varianti_det_off_ven"

	case "ORD_VEN"
		ls_tabella_varianti = "varianti_det_ord_ven"

	case "COMMESSE"
		ls_tabella_varianti = "varianti_commesse"
		
	case else
		as_errore = "uof_ricerca_quan_mp_variante(): il valore dela prametro as_tabella_varianti = "+guo_functions.uof_format(as_tabella_varianti)+" non è valido"
		return -1
end choose

for ll_variante = 1 to ll_rows
	
	ls_cod_prodotto_figlio[ll_conteggio] 	= lds_varianti.getitemstring(ll_variante, 1)
	ls_cod_versione_figlio[ll_conteggio] 	= lds_varianti.getitemstring(ll_variante, 2)
	ldd_quantita_utilizzo 						= lds_varianti.getitemnumber(ll_variante, 3)
	ls_flag_materia_prima 					= lds_varianti.getitemstring(ll_variante, 4)
	ll_num_sequenza 							= lds_varianti.getitemnumber(ll_variante, 5)
	
	guo_functions.uof_log("Prodotto Figlio =" + guo_functions.uof_format(ls_cod_prodotto_figlio[ll_conteggio]))
	guo_functions.uof_log("Versione Figlio =" + guo_functions.uof_format(ls_cod_versione_figlio[ll_conteggio]))
	

	if not isnull(al_anno_registrazione) and not isnull(al_num_registrazione) then
		
		ls_sql = "select quan_utilizzo, " + &
					" cod_prodotto, " + &
					" cod_versione_variante, " + &
					" flag_materia_prima " + &
					" from " + ls_tabella_varianti + &
					" where  cod_azienda='" + s_cs_xx_cod_azienda + "'"
		choose case ls_tabella_varianti
			case "varianti_commesse"
					ls_sql = ls_sql + " and anno_commessa=" + string(al_anno_registrazione) + &
											" and num_commessa=" + string(al_num_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_off_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_registrazione) + &
											" and num_registrazione=" + string(al_num_registrazione) + &
											" and prog_riga_off_ven=" + string(al_prog_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_ord_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_registrazione) + &
											" and num_registrazione=" + string(al_num_registrazione) + &
											" and prog_riga_ord_ven=" + string(al_prog_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_trattative"
					ls_sql = ls_sql + " and anno_trattativa=" + string(al_anno_registrazione) + &
											" and num_trattativa=" + string(al_num_registrazione) + &
											" and prog_trattativa=" + string(al_prog_registrazione) + &
											" and flag_esclusione<>'S'"
		end choose
		ls_sql = ls_sql + " and cod_prodotto_padre='" + as_cod_prodotto + "'" + &
								" and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_conteggio] + "'" + &
								" and cod_versione_figlio='" + ls_cod_versione_figlio[ll_conteggio] + "'" + &
								" and cod_versione='" + as_cod_versione + "'"
		
		guo_functions.uof_log("SQL ricerca varianti del ramo =" + guo_functions.uof_format(ls_sql))
		
		ll_tot_varianti = guo_functions.uof_crea_datastore(lds_det_varianti, ls_sql)
		
		if ll_tot_varianti < 0 then
			guo_functions.uof_log("uof_ricerca_quan_mp_variante(): Errore nel datastore lds_det_varianti")
			as_errore = "uof_ricerca_quan_mp_variante(): Errore nel datastore lds_det_varianti "
			return -1
		end if
		

		if ll_tot_varianti > 0 then
			// può esserci solo 1 record
			ldd_quan_utilizzo_variante = lds_det_varianti.getitemnumber(ll_tot_varianti,1)
			ls_cod_prodotto_variante = lds_det_varianti.getitemstring(ll_tot_varianti,2)
			ls_cod_versione_variante = lds_det_varianti.getitemstring(ll_tot_varianti,3)
			ls_flag_materia_prima_variante = lds_det_varianti.getitemstring(ll_tot_varianti,4)
			
			ldd_quantita_utilizzo = ldd_quan_utilizzo_variante
			ls_flag_materia_prima = ls_flag_materia_prima_variante
			
			guo_functions.uof_log("uof_ricerca_quan_mp_variante(): scorro distinta PADRE=" + as_cod_prodotto + "  FIGLIO="+ls_cod_prodotto_figlio[ll_conteggio]+"; trovato variante=" + ls_cod_prodotto_variante)

			
			select cod_gruppo_variante
			into   :ls_cod_gruppo_variante
			from   distinta_gruppi_varianti
			where  cod_azienda         = :s_cs_xx_cod_azienda and
					 cod_prodotto_padre  = :as_cod_prodotto and
					 num_sequenza        = :ll_num_sequenza and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
					 cod_versione        = :as_cod_versione;
					 
			if sqlca.sqlcode = 0 then
				if ls_cod_gruppo_variante = as_cod_gruppo_variante then
					guo_functions.uof_log("uof_ricerca_quan_mp_variante(): trovato corrispondenza gruppo variante " + guo_functions.uof_format(ls_cod_gruppo_variante) + &
																							 " Padre: " + guo_functions.uof_format(as_cod_prodotto) + &
																							 " Figlio: " + guo_functions.uof_format(ls_cod_prodotto_figlio[ll_conteggio])  )

					//as_cod_materia_prima   = ls_cod_prodotto_figlio[ll_conteggio]
					as_cod_materia_prima   = ls_cod_prodotto_variante
					ad_quan_materia_prima  = ldd_quantita_utilizzo
					destroy lds_det_varianti
					return 0
				end if
			end if
			ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
			
			
		else
			guo_functions.uof_log("uof_ricerca_quan_mp_variante(): scorro distinta PADRE=" + as_cod_prodotto + "  FIGLIO="+ls_cod_prodotto_figlio[ll_conteggio]+ " nessuna variante trovata ")
			// anche nel caso sia una semplice MP senza variante devo verificare se combacia 
			select cod_gruppo_variante
			into   :ls_cod_gruppo_variante
			from   distinta_gruppi_varianti
			where  cod_azienda         = :s_cs_xx_cod_azienda and
					 cod_prodotto_padre  = :as_cod_prodotto and
					 num_sequenza        = :ll_num_sequenza and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
					 cod_versione        = :as_cod_versione;
					 
			if sqlca.sqlcode = 0 then
				if ls_cod_gruppo_variante = as_cod_gruppo_variante then
					guo_functions.uof_log("uof_ricerca_quan_mp_variante(): trovato corrispondenza gruppo variante " + guo_functions.uof_format(ls_cod_gruppo_variante) + &
																							 " Padre: " + guo_functions.uof_format(as_cod_prodotto) + &
																							 " Figlio: " + guo_functions.uof_format(ls_cod_prodotto_figlio[ll_conteggio])  )

					//as_cod_materia_prima   = ls_cod_prodotto_figlio[ll_conteggio]
					as_cod_materia_prima   = ls_cod_prodotto_figlio[ll_conteggio]
					ad_quan_materia_prima  = ldd_quantita_utilizzo
					destroy lds_det_varianti
					return 0
				end if
			end if
			//ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
			
		end if
		
		ldd_quantita_utilizzo = ldd_quantita_utilizzo * ad_quan_prodotto
		ll_conteggio++	
	
		destroy lds_det_varianti
		// ----------------- verifico se gruppo variante corrisponde ------------------
	end if
next


for ll_t = 1 to ll_conteggio -1

   if ls_flag_materia_prima = 'N' then
		
		select count(*)
		into   :ll_test
		from   distinta  
		where  cod_azienda = :s_cs_xx_cod_azienda and
				 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
				 cod_versione = :ls_cod_versione_figlio[ll_t];
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
	
		if ll_test = 0 or isnull(ll_test) then				// è una materia prima
			if not isnull(al_anno_registrazione) and not isnull(al_num_registrazione) then		// verifico se ha varianti
				ls_sql = "select quan_utilizzo, " + &
							" cod_prodotto, " + &
							" cod_versione_variante " + &
							" from " + ls_tabella_varianti + &
							" where  cod_azienda='" + s_cs_xx_cod_azienda + "'"
				choose case ls_tabella_varianti
					case "varianti_commesse"
							ls_sql = ls_sql + " and anno_commessa=" + string(al_anno_registrazione) + &
													" and num_commessa=" + string(al_num_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_off_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_registrazione) + &
													" and num_registrazione=" + string(al_num_registrazione) + &
													" and prog_riga_off_ven=" + string(al_prog_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_ord_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_registrazione) + &
													" and num_registrazione=" + string(al_num_registrazione)  + &
													" and prog_riga_ord_ven=" + string(al_prog_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_trattative"
							ls_sql = ls_sql + " and anno_trattativa=" + string(al_anno_registrazione) + &
													" and num_trattativa=" + string(al_num_registrazione) + &
													" and prog_trattativa=" + string(al_prog_registrazione) + &
													" and flag_esclusione<>'S'"
				end choose
				ls_sql = ls_sql + " and    cod_prodotto_padre='" + as_cod_prodotto + "'" + &
										" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
										" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
										" and    cod_versione='" + as_cod_versione + "'" 
				
				guo_functions.uof_log("SQL ricerca figli varianti =" + guo_functions.uof_format(ls_sql))
				
				ll_tot_varianti = guo_functions.uof_crea_datastore(lds_det_varianti, ls_sql)
				
				if ll_tot_varianti < 0 then
					guo_functions.uof_log("uof_ricerca_quan_mp_variante(): Errore nel datastore lds_det_varianti")
					as_errore = "uof_ricerca_quan_mp_variante(): Errore nel datastore lds_det_varianti "
					return -1
				end if
				
				if ll_tot_varianti > 0 then
					ldd_quan_utilizzo_variante = lds_det_varianti.getitemnumber(ll_tot_varianti, 1)
					ls_cod_prodotto_variante = lds_det_varianti.getitemstring(ll_tot_varianti, 2)
					ls_cod_versione_variante = lds_det_varianti.getitemstring(ll_tot_varianti, 3)
					
					guo_functions.uof_log("uof_ricerca_quan_mp_variante(): esamino ramo MP PADRE=" + as_cod_prodotto + "  FIGLIO="+ls_cod_prodotto_figlio[ll_conteggio]+ " variante trovata=" + ls_cod_prodotto_variante)
					
					ldd_quan_utilizzo_variante    = ldd_quan_utilizzo_variante * ad_quan_prodotto
					ldd_quantita_utilizzo         = ldd_quan_utilizzo_variante
					ls_cod_prodotto_figlio[ll_t]  = ls_cod_prodotto_variante
					ls_cod_versione_figlio[ll_t]  = ls_cod_versione_variante
				else
					guo_functions.uof_log("uof_ricerca_quan_mp_variante(): esamino ramo MP PADRE=" + as_cod_prodotto + "  FIGLIO="+ls_cod_prodotto_figlio[ll_conteggio]+ " nessuna variante trovata=")
				end if
				destroy lds_det_varianti
			end if
			
		else		// è una semilavorato; vado ad esaminare ulteriori livelli
			
			guo_functions.uof_log("Entro in ricorsione nel figlio :  " + guo_functions.uof_format(ls_cod_prodotto_figlio[ll_t]))
			
			li_risposta = uof_ricerca_quan_mp_variante(	ls_cod_prodotto_figlio[ll_t], &
																		ls_cod_versione_figlio[ll_t], &
																		ldd_quantita_utilizzo, &
																		as_cod_gruppo_variante, &
																		al_anno_registrazione, &
																		al_num_registrazione, &
																		al_prog_registrazione, &
																		as_tabella_varianti, &
																		ref as_cod_materia_prima, &
																		ref ad_quan_materia_prima, &
																		ref as_errore)
	
			if li_risposta = -1 then 
				as_errore = ls_errore
				return -1
			end if
			if li_risposta = 0 then
				return 0
			end if
		end if
	else				// flag mp = "S"
		if not isnull(al_anno_registrazione) and not isnull(al_num_registrazione) then
			ls_sql = "select quan_utilizzo, " + &
						" cod_prodotto, " + &
						" cod_versione_variante " + &
						" from " + ls_tabella_varianti + &
						" where  cod_azienda='" + s_cs_xx_cod_azienda + "'"
			choose case ls_tabella_varianti
				case "varianti_commesse"
						ls_sql = ls_sql + " and anno_commessa=" + string(al_anno_registrazione) + &
												" and num_commessa=" + string(al_num_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_off_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_registrazione) + &
												" and num_registrazione=" + string(al_num_registrazione) + &
												" and prog_riga_off_ven=" + string(al_prog_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_ord_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(al_anno_registrazione) + &
												" and num_registrazione=" + string(al_num_registrazione)  + &
												" and prog_riga_ord_ven=" + string(al_prog_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_trattative"
						ls_sql = ls_sql + " and anno_trattativa=" + string(al_anno_registrazione) + &
												" and num_trattativa=" + string(al_num_registrazione) + &
												" and prog_trattativa=" + string(al_prog_registrazione) + &
												" and flag_esclusione<>'S'"
			end choose
			ls_sql = ls_sql + " and    cod_prodotto_padre='" + as_cod_prodotto + "'" + &
									" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
									" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
									" and    cod_versione='" + as_cod_versione + "'" 
			
			guo_functions.uof_log("SQL ricerca figli se flag_mp=S :  " + guo_functions.uof_format(ls_sql))
			
			ll_tot_varianti = guo_functions.uof_crea_datastore(lds_det_varianti, ls_sql)
			
			if ll_tot_varianti < 0 then
				guo_functions.uof_log("uof_ricerca_quan_mp_variante(): Errore nel datastore lds_det_varianti")
				as_errore = "uof_ricerca_quan_mp_variante(): Errore nel datastore lds_det_varianti "
				return -1
			end if
			
			if ll_tot_varianti > 0 then
				ldd_quan_utilizzo_variante = lds_det_varianti.getitemnumber(ll_tot_varianti, 1)
				ls_cod_prodotto_variante = lds_det_varianti.getitemstring(ll_tot_varianti, 2)
				ls_cod_versione_variante = lds_det_varianti.getitemstring(ll_tot_varianti, 3)
				
				guo_functions.uof_log("uof_ricerca_quan_mp_variante(): esamino ramo con flag MP=S PADRE=" + as_cod_prodotto + "  FIGLIO="+ls_cod_prodotto_figlio[ll_conteggio]+ " variante trovata=" + ls_cod_prodotto_variante)
				
				ldd_quan_utilizzo_variante = ldd_quan_utilizzo_variante * ad_quan_prodotto
				ldd_quantita_utilizzo=ldd_quan_utilizzo_variante
				
				// --- verifico corrispondenza del gruppo variante -----------------------
				select cod_gruppo_variante
				into   :ls_cod_gruppo_variante
				from   distinta_gruppi_varianti
				where  cod_azienda = :s_cs_xx_cod_azienda and
						 cod_prodotto_padre = :as_cod_prodotto and
						 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_t] and
						 cod_versione_figlio = :ls_cod_versione_figlio[ll_t] and
						 cod_versione = :as_cod_versione;
				if sqlca.sqlcode = 0 then
					if ls_cod_gruppo_variante = as_cod_gruppo_variante then
						as_cod_materia_prima = ls_cod_prodotto_variante
						ad_quan_prodotto = ldd_quantita_utilizzo
						guo_functions.uof_log("Uscita Function uof_ricerca_quan_mp_variante TROVATO!. Prodotto figlio=" + guo_functions.uof_format(as_cod_materia_prima) + "  Q.ta=" + guo_functions.uof_format(ad_quan_prodotto))
						return 0
					end if
				end if
				
				ls_cod_prodotto_figlio[ll_t]=ls_cod_prodotto_variante
				ls_cod_versione_figlio[ll_t]=ls_cod_versione_variante
			else
				guo_functions.uof_log("uof_ricerca_quan_mp_variante(): esamino ramo con flag MP=S PADRE=" + as_cod_prodotto + "  FIGLIO="+ls_cod_prodotto_figlio[ll_conteggio]+ " nessuna variante trovata=")
			end if
			destroy lds_det_varianti
			
	
		end if
	end if
next

destroy lds_varianti

guo_functions.uof_log("Uscita Function uof_ricerca_quan_mp_variante INSUCCESSO. Prodotto Finito:" + guo_functions.uof_format(as_cod_prodotto))

return 1
end function

on uo_conf_varianti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_conf_varianti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

