﻿$PBExportHeader$uo_espress_reg.sru
forward
global type uo_espress_reg from nonvisualobject
end type
end forward

global type uo_espress_reg from nonvisualobject
end type
global uo_espress_reg uo_espress_reg

type variables


public:
	uo_regex 				iuo_regex
	string						is_pattern

private:
	boolean					ib_not_reg = false
end variables

forward prototypes
public function integer set_pattern (string as_pattern, ref string as_errore)
public function integer cerca_esteso (string as_espressione, ref string as_valori_estesi)
public function integer cerca_array (string as_espressione, ref string as_valori[])
end prototypes

public function integer set_pattern (string as_pattern, ref string as_errore);
is_pattern = as_pattern

if not iuo_regex.initialize( is_pattern, true , not true ) then 
	as_errore = "Inizializzazione non ok :" + "~r~n" + iuo_regex.getlasterror( )
	return -1
end if

return 0
end function

public function integer cerca_esteso (string as_espressione, ref string as_valori_estesi);long ll_count
string ls_matches = ""
long i

ll_count = iuo_regex.search(as_espressione)
as_valori_estesi = ""

for i = 1 to ll_count
	ls_matches+= string(i)+") " + iuo_regex.match(i) + "~r~n"
next

as_valori_estesi = string(ll_count,"[General];(null)") + " match(es)~r~n" + ls_matches

return ll_count

end function

public function integer cerca_array (string as_espressione, ref string as_valori[]);long ll_count
long i, j, k
string ls_temp
boolean lb_trovato

ll_count = iuo_regex.search(as_espressione)

k = 0

for i = 1 to ll_count
	ls_temp = trim(iuo_regex.match(i))
	
	//verifica se la variabile è già presente nell'array
	lb_trovato = false
	for j=1 to upperbound(as_valori[])
		if ls_temp=as_valori[j] then
			lb_trovato = true
			exit
		end if
	next
	
	if not lb_trovato then
		k += 1
		as_valori[k] = ls_temp
	end if
next

return upperbound(as_valori[])

end function

on uo_espress_reg.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_espress_reg.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event destructor;destroy iuo_regex
end event

event constructor;iuo_regex = create uo_regex


if not isvalid(iuo_regex) then
	ib_not_reg = true
	return
end if

iuo_regex.ismultiline( )

//INIZIALIZZAZIONE
iuo_regex.setdotmatchnewline(false)
iuo_regex.setmultiline(false)
iuo_regex.setextendedsyntax(false)
iuo_regex.setungreedy(false)

//st_study.visible = false



end event

