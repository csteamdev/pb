﻿$PBExportHeader$uo_functions.sru
forward
global type uo_functions from nonvisualobject
end type
end forward

global type uo_functions from nonvisualobject
end type
global uo_functions uo_functions

type prototypes
//funzioni di estrazione nomi files da percorso -------------------------
FUNCTION Ulong FindFirstFile(ref String sPath, ref str_win32_find_data lstr_win32_find_data) LIBRARY "KERNEL32.DLL" ALIAS FOR "FindFirstFileW"

FUNCTION boolean FindNextFile(Ulong handle, ref str_win32_find_data lstr_win32_find_data) LIBRARY "KERNEL32.DLL" ALIAS FOR "FindNextFileW"

FUNCTION boolean FindClose(Ulong handle) LIBRARY "KERNEL32.DLL" ALIAS FOR "FindClose"
//----------------------------------------------------------------------------

FUNCTION Long GetTempPath(Long nBufferLength, REF String lpBuffer) LIBRARY "KERNEL32.DLL" ALIAS FOR "GetTempPathA;Ansi"

// stefanop 03/09/2010: apertura documenti per w_ole_v2
FUNCTION long ShellExecuteA(long hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, long nShowCmd) LIBRARY "SHELL32.DLL" ALIAS FOR "ShellExecuteA;ansi"
end prototypes

type variables
/*
*/
string s_cs_xx_cod_azienda
string s_cs_xx_cod_utente
string s_cs_xx_db_funzioni_formato_data
end variables

forward prototypes
public function string uof_get_user_temp_folder ()
public function string uof_get_user_documents_folder ()
public function string uof_get_user_desktop_folder ()
public function long uof_crea_datastore (ref datastore fds_data, string fs_sql, ref string fs_errore)
public function long uof_crea_datastore (ref datastore fds_data, string fs_sql, transaction ft_trans, ref string fs_errore)
public function integer uof_replace_string (ref string fs_stringa, string fs_da_sostituire, string fs_sostitutore)
public function boolean uof_in_array (any aa_search, any aa_array[], ref long al_position)
public function boolean uof_in_array (any aa_search, any aa_array[])
public subroutine uof_explode (string as_string, string as_delimiter, ref string as_array[])
public function string uof_implode (string as_array[], string as_delimiter)
public function long uof_crea_datastore (ref datastore fds_data, string fs_sql)
public function string uof_get_user_info (string as_key)
public function integer uof_get_week_number (date adt_data)
public function integer uof_blob_to_file (ref blob fb_blob, string fs_nome_file)
public function boolean uof_crea_transazione (string as_ini_section, ref transaction at_transaction, ref string as_error)
public subroutine uof_format_rte (ref string fs_value)
public function string uof_implode (string as_array[], string as_delimiter, boolean ab_exclude_empty)
public function integer uof_file_to_blob (string fs_nome_file, ref blob fb_blob)
public function boolean uof_file_write (string as_filepath, string as_message, boolean ab_append, ref string as_error)
public function integer uof_get_stabilimento_utente (ref string fs_cod_deposito, ref string fs_errore)
public function integer uof_get_stabilimento_operatore (string as_cod_operatore, ref string as_cod_deposito, ref string as_errore)
public subroutine uof_merge_arrays (ref string aa_array_to_merge[], string aa_array_to_read[])
public function boolean uof_in_array (string aa_search[], string aa_array[], ref long al_position)
public function boolean uof_in_array (string aa_search[], string aa_array[])
public function string uof_get_operatore_utente (string as_cod_utente)
public function string uof_get_operatore_utente ()
public function string uof_progressivo_alfanumerico (string as_code)
public function boolean uof_dw_has_column (ref datawindow adw_datawindow, string as_column_name)
public function string uof_nome_cella (long fl_riga, long fl_colonna)
public subroutine uof_get_value (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref string fs_tipo, oleobject fole_excel)
public function boolean uof_create_transaction_from (ref transaction at_from, ref transaction at_to, ref string as_error)
public function integer uof_get_stabilimento_operaio (string as_cod_operaio, ref string fs_cod_deposito, ref string fs_errore)
public function integer uof_get_stabilimento_utente (string as_cod_utente, ref string fs_cod_deposito, ref string fs_errore)
public function boolean uof_is_gibus ()
public subroutine uof_get_parametro (string as_cod_parametro, ref long al_numero)
public subroutine uof_get_parametro (string as_cod_parametro, ref date ad_date)
public subroutine uof_get_parametro (string as_cod_parametro, ref string as_stringa)
public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref date ad_date)
public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref string as_stringa)
public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref long al_long)
private function any uof_get_parametri (string as_tipo_tabella, string as_cod_parametro, string as_flag_parametro)
public function integer uof_tipo_tracciato_ord_acq (string fs_cod_fornitore, ref string fs_errore)
public function integer uof_filigrana (string fs_path_file, ref datawindow dw_data, boolean fb_visualizza_subito, ref string fs_errore)
public function string uof_data_db (date adt_date)
public function string uof_data_db (datetime adt_date)
public subroutine uof_get_prossimo_giorno_lavorativo (date adt_date, boolean ab_feste_comandate, ref date adt_working_date)
public function integer uof_get_ultimo_giorno_mese (integer ai_mese, integer ai_anno)
public function integer uof_get_ultimo_giorno_mese (date adt_date)
public function string uof_get_operaio_utente (string as_cod_utente)
public function string uof_get_operaio_utente ()
public function boolean uof_is_supervisore (string as_cod_utente)
public function boolean uof_is_admin (string as_cod_utente)
public function long uof_substr_count (string as_text, string as_find)
public function integer uof_get_file_info (string as_file_source, ref string as_file_dir, ref string as_file_name, ref string as_file_ext)
public function date uof_string_to_date (string as_string_date)
public function integer uof_esporta_clienti_txt (string as_percorso_file, ref string as_errore)
public function string uof_get_file_attribute (string as_file_path, integer ai_attributes)
public function time uof_seconds_to_time (long al_seconds)
public function time uof_time_diff (time at_time_start, time at_time_end)
public function boolean uof_anno_bisestile (integer ai_anno)
public function boolean uof_confronta_arrays (string aa_array_1[], string aa_array_2[])
public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref decimal ad_decimal)
public function string uof_string_to_rte (string as_text)
public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref boolean ab_flag)
public subroutine uof_get_parametro (string as_cod_parametro, ref boolean ab_flag)
public function boolean uof_get_date_from_giornomese (long fl_giorno_mese, integer fi_anno, ref date fdd_date)
public function string uof_get_random_filename ()
public function integer uof_setnull_fatt_proforma (integer fi_anno_ordine, long fl_num_ordine, long fl_riga_ordine, ref string fs_messaggio)
public function integer uof_setnull_fatt_proforma (integer fi_anno_ordine, long fl_num_ordine, ref string fs_messaggio)
public subroutine uof_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, string as_cod_lingua, string as_tabella_iva, ref string fs_cod_iva[], ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[], ref string fs_des_estesa[])
public function integer uof_estrai_files (ref string as_percorso_iniziale, string as_condizione, ref string as_files_estratti[], ref string as_errore)
public function integer as_reverse_array (string as_array_in[], ref string as_array_out[])
public function integer uof_write_log_sistema_not_sqlca (string as_tipo_log, string as_log_id, string as_message)
public function integer uof_log (string as_message)
public function string uof_format (decimal ad_decimal)
public function string uof_format (datetime ad_datetime)
public function string uof_format (string as_string)
end prototypes

public function string uof_get_user_temp_folder ();/**
 * stefanop
 * 04/10/2011
 *
 * Recupero la cartella temporanea dell'utente
 * La cartella avrà sicuramente i permessi di lettura/scrittura
 *
 * return string C:\Users\XXX\AppData\Local\Temp\csteam\
 **/

String ls_temppath

try
	
	ls_temppath = Space(255)
	GetTempPath(255 ,ls_temppath)

	ls_temppath += "csteam\"
	
	if not DirectoryExists(ls_temppath) then
		CreateDirectory(ls_temppath)
	end if
	
catch (RuntimeError ex)
	setnull(ls_temppath)
end try

return ls_temppath
end function

public function string uof_get_user_documents_folder ();/**
 * stefanop
 * 04/10/2011
 *
 * Recupera il percorso dei documenti dell'utente collegato alla macchina
 **/
 
string ls_document_folder

if registryget("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Personal", ls_document_folder) = -1 then
	
	// Provo nella vecchia maniera
	ls_document_folder  = uof_get_user_info("HOMEDRIVE")
	ls_document_folder += uof_get_user_info("HOMEPATH")
	ls_document_folder += "\Documenti\"
	
else 
	ls_document_folder += "\"
end if


return ls_document_folder
end function

public function string uof_get_user_desktop_folder ();/**
 * stefanop
 * 04/10/2011
 *
 * Recupera il percorso del desktop dell'utente collegato alla macchina
 **/
 
string ls_document_folder

if registryget("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Desktop", ls_document_folder) = -1 then
	setnull(ls_document_folder)
else 
	ls_document_folder += "\"
end if

//ls_document_folder  = uof_get_user_info("HOMEDRIVE")
//ls_document_folder += uof_get_user_info("HOMEPATH")
//ls_document_folder += "\Desktop\"

return ls_document_folder
end function

public function long uof_crea_datastore (ref datastore fds_data, string fs_sql, ref string fs_errore);return uof_crea_datastore(fds_data, fs_sql, sqlca, fs_errore)

end function

public function long uof_crea_datastore (ref datastore fds_data, string fs_sql, transaction ft_trans, ref string fs_errore);string ls_error
long ll_ret

fs_sql = ft_trans.syntaxfromsql(fs_sql, "style(type=grid)", ls_error)
if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	fs_errore = "Impossibile convertire stringa SQL per il datastore.~r~n" + ls_error
	return -1
end if

fds_data = create datastore
fds_data.create(fs_sql, ls_error)
if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	destroy fds_data;
	fs_errore = "Impossibile creare datastore.~r~n" + ls_error
	return -1
end if

fds_data.settransobject(ft_trans)

ll_ret = fds_data.retrieve()


return ll_ret

end function

public function integer uof_replace_string (ref string fs_stringa, string fs_da_sostituire, string fs_sostitutore);/**
 * Funzione di sostituzione
 * Rimpiazza tutte le occorenze di fs_da_sostituire in fs_stringa con fs_sostitutore
 **/

integer li_lunghezza_formula,li_t,li_posizione,li

if isnull(fs_stringa) or fs_stringa = "" then return 0

if isnull(fs_sostitutore) then fs_sostitutore=''

li_lunghezza_formula = len(fs_stringa)

if li_lunghezza_formula=0 then return 0

li_posizione = 1

do while 1=1
	li_posizione=pos(fs_stringa,fs_da_sostituire,li_posizione)
	if li_posizione=0 then exit	
	fs_stringa = replace ( fs_stringa, li_posizione, len(fs_da_sostituire), fs_sostitutore ) 
	li_posizione += len(fs_sostitutore)
loop


return 0
end function

public function boolean uof_in_array (any aa_search, any aa_array[], ref long al_position);/**
 * stefanop
 * 18/10/2011
 *
 * Controllo se un determinato valore esiste all'interno di un array e ne ritorno la posizione
 * Ritorna TRUE se l'elemento è presente e FALSE se non è stato trovato
 **/
 
long ll_i

al_position = -1

for ll_i = 1 to upperbound(aa_array)
	
	if aa_array[ll_i] = aa_search then
		al_position = ll_i
		return true
	end if
	
next

return false
end function

public function boolean uof_in_array (any aa_search, any aa_array[]);long ll_position
return uof_in_array(aa_search, aa_array, ll_position)
end function

public subroutine uof_explode (string as_string, string as_delimiter, ref string as_array[]);/**
 * stefanop
 * 18/10/2011
 *
 * Divide una stringa in base al separatore passato
 **/

long ll_pos = 1, ll_delimiter_length

// la stringa deve esistere
if len(as_string) < 1 then return

ll_delimiter_length = len(as_delimiter)

do while true
	
	ll_pos = pos(as_string, as_delimiter, 1)
	if ll_pos = 0 then
		// appendo anche l'ultima occorrenza se valida prima di uscire
		if not isnull(as_string) then as_array[upperbound(as_array)+1] = as_string
		exit
	end if
	
	as_array[upperbound(as_array)+1] = left(as_string, ll_pos - 1)
	as_string = mid(as_string, ll_pos + ll_delimiter_length)
	
loop
end subroutine

public function string uof_implode (string as_array[], string as_delimiter);/**
 * stefanop
 * 18/10/2011
 *
 * Unisce un array di stringe separandole da un delimitatore
 **/

return uof_implode(as_array, as_delimiter, false)
end function

public function long uof_crea_datastore (ref datastore fds_data, string fs_sql);string ls_error

return uof_crea_datastore(fds_data, fs_sql, sqlca, ls_error)
end function

public function string uof_get_user_info (string as_key);/**
 * stefanop
 * 04/10/2011
 *
 * Recupera le informazioni dalle variabili di ambiente del sistema corrente
 **/
 
 
// Ritorna le variabili d'ambiente del sistema
string ls_Path
string ls_values[]
ContextKeyword lcxk_base

GetContextService("Keyword", lcxk_base)

lcxk_base.GetContextKeywords(as_key, ls_values)
IF Upperbound(ls_values) > 0 THEN
   ls_Path = ls_values[1]
ELSE
   ls_Path = "*UNDEFINED*"
END IF

return ls_Path
end function

public function integer uof_get_week_number (date adt_data);Integer li_year, li_prev_year
Integer li_day, li_week

li_day = DayNumber(adt_data) - 1 // giorno italiano
if li_day = 0 then li_day = 7 // domenica


DO WHILE li_day <> 4 
IF li_day < 4 THEN 
   // Mon - Wed. Go to next Thur.
   adt_data = RelativeDate(adt_data, 1)
ELSE
   // Thur - Sun. Go to prev Thur.
   adt_data = RelativeDate(adt_data, -1)
END IF
	li_day = DayNumber(adt_data) - 1
	if li_day = 0 then li_day = 7 // domenica
LOOP

li_year = Year(adt_data)


// Count weeks backwards until the
// previous year
DO
   li_week++
   adt_data = RelativeDate(adt_data, -7)
   li_prev_year = Year(adt_data)
LOOP UNTIL li_year <> li_prev_year

Return li_week
end function

public function integer uof_blob_to_file (ref blob fb_blob, string fs_nome_file);/**
 * Salva il blob all'interno di un file
 **/
 
integer li_numero_file, li_count, i

long ll_lunghezza_blob, ll_pos, ll_quantita
blob lb_blob

ll_lunghezza_blob = Len(fb_blob)
li_numero_file = FileOpen(fs_nome_file, StreamMode!, Write!, LockWrite!, Replace!)
if li_numero_file = -1 then return -1

if ll_lunghezza_blob > 32765 then
	if Mod(ll_lunghezza_blob, 32765) = 0 then
		li_count = ll_lunghezza_blob/32765
	else
		li_count = (ll_lunghezza_blob/32765) + 1
	end if
else
	li_count = 1
end if

for i = 1 to li_count
	ll_pos = (i - 1) * 32765 + 1
	if i = li_count then
		ll_quantita = mod(ll_lunghezza_blob, 32765)
	else
		ll_quantita = 32765
	end if
	lb_blob = BlobMid(fb_blob, ll_pos, ll_quantita)
	FileWrite(li_numero_file, lb_blob)
next
FileClose(li_numero_file)
return 0
 
return 0
end function

public function boolean uof_crea_transazione (string as_ini_section, ref transaction at_transaction, ref string as_error);/**
 * Stefano Pulze
 * 25/11/2011
 *
 * Leggo i paramentri dal registro e imposto la transazione,
 * il codice è copiato da quello usato dal client/server
 */

string ls_logpass, ls_option, ls_reg_chiave_root = "HKEY_LOCAL_MACHINE\SOFTWARE\Consulting&Software\"
int li_risposta, ll_ret

at_transaction = create transaction

// -- Leggo i dati dal registro
li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "servername", at_transaction.ServerName)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: servername."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "dbms", at_transaction.DBMS)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: dbms."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "database", at_transaction.Database)
if li_risposta = -1 then
	as_error = "Mancano le impostazione del database sul registro: database."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "logid", at_transaction.LogId)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: logid."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "logpass", ls_logpass)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: logpass."
	return false
end if

li_risposta = Registryset(ls_reg_chiave_root + "profilocorrente", "appo", "1.1")		 

if at_transaction.DBMS <> "ODBC" then
	
	if isnull(ls_logpass) or ls_logpass="" then		
		
		as_error = "Manca la password per l'accesso al database è necessario impostarla ora altrimenti non è possibile accedere al sistema."
		return false
		
	end if	
		
	n_cst_crypto luo_crypto
	luo_crypto = create n_cst_crypto
	ll_ret = luo_crypto.decryptdata( ls_logpass, ls_logpass)  
	destroy luo_crypto;
	if ll_ret < 0 then
		as_error = "La password contiene caratteri non consentiti.I caratteri consentiti comprendono:" + &
					"- tutte le cifre numeriche 0,1,2,...,9  tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z" + &
					"- alcuni simboli.Modificare la password e riprovare"
		return false
	end if
	
	at_transaction.LogPass = ls_logpass
	
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "userid", at_transaction.UserId)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: userid."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "dbpass", at_transaction.DBPass)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: dbpass."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "dbparm", at_transaction.DBParm)
if li_risposta = -1 then
	as_error = "Mancano le impostazione del database sul registro: dbparm."
	return false
end if

// QUANDO VA USATO???
//li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "option", ls_option)
//if li_risposta = -1 then
//	as_error = "Mancano le impostazione del database sul registro: option."
//	return false
//end if
//
//at_transaction.dbparm = at_transaction.dbparm + ls_option

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "lock", at_transaction.Lock)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: lock."
	return false
end if

li_risposta = Registryset(ls_reg_chiave_root + "profilocorrente", "appo", "1.2")		 

disconnect using at_transaction;
connect using at_transaction;
if at_transaction.sqlcode <> 0 then
	as_error = "Errore durante la connessione al database~r~n" + at_transaction.sqlerrtext
	return false
end if

return true
end function

public subroutine uof_format_rte (ref string fs_value);string ls_rte_format

if isnull(fs_value) then fs_value = ""

ls_rte_format = &
"{\rtf1\ansi\ansicpg1252\uc1\deff0{\fonttbl&
{\f0\fnil\fcharset0\fprq2 Arial;}&
{\f1\fswiss\fcharset0\fprq2 Tahoma;}&
{\f2\froman\fcharset2\fprq2 Symbol;}}&
{\colortbl;\red0\green0\blue0;\red255\green255\blue255;}&
{\stylesheet{\s0\itap0\nowidctlpar\f0\fs24 [Normal];}{\*\cs10\additive Default Paragraph Font;}}&
{\*\generator TX_RTF32 15.0.530.503;}&
\deftab1134\paperw11905\paperh16838\margl0\margt0\margr0\margb0\widowctrl\formshade&
{\*\background{\shp{\*\shpinst\shpleft0\shptop0\shpright0\shpbottom0\shpfhdr0\shpbxmargin\shpbxignore\shpbymargin\shpbyignore\shpwr0\shpwrk0\shpfblwtxt1\shplid1025{\sp{\sn shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn fillColor}{\sv 16777215}}{\sp{\sn fFilled}{\sv 1}}{\sp{\sn lineWidth}{\sv 0}}{\sp{\sn fLine}{\sv 0}}{\sp{\sn fBackground}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 1}}}}}\sectd&
\headery720\footery720\pgwsxn6750\pghsxn16838\marglsxn0\margtsxn0\margrsxn0\margbsxn0\pard\itap0\nowidctlpar\plain\f1\fs20 "

fs_value = ls_rte_format + fs_value + "}"

return
end subroutine

public function string uof_implode (string as_array[], string as_delimiter, boolean ab_exclude_empty);/**
 * stefanop
 * 18/10/2011
 *
 * Unisce un array di stringe separandole da un delimitatore
 *
 * Parametri:
 *		as_array[] = array delle stringhe da unire
 *		as_delimiter = delimitatore per l'unione
 *		ab_exclude_empty = se TRUE non prende in considerazione le stringhe vuote
 **/

string ls_result
long ll_i, ll_count

ll_count = upperbound(as_array)

// se maggiore di due allora necessita del delimitatore
if ll_count > 1 then
	
	for ll_i = 1 to ll_count -1
		if isnull(as_array[ll_i]) or as_array[ll_i] = "" then
			if ab_exclude_empty then
				continue
			else
				as_array[ll_i] = ""
			end if
		end if
		
		ls_result += as_array[ll_i] + as_delimiter
	next
	
	if isnull(as_array[ll_i]) or as_array[ll_i] = "" then as_array[ll_count] = ""

	if ab_exclude_empty and len(as_array[ll_count]) < 1 then
	else
		ls_result += as_array[ll_count]
	end if
	
	return ls_result

// se un elemento solo allora non serve il delimitatore e nemmeno il ciclo
elseif ll_count = 1 then
	
	if ab_exclude_empty and len(as_array[1]) < 1 then
		return ""
	else
		return as_array[1]
	end if
	
// se nessun elemento allora non serve niente
elseif ll_count < 1 then
	return ""
end if
end function

public function integer uof_file_to_blob (string fs_nome_file, ref blob fb_blob);//Funzione che legge un file e lo mette in una variabile di tipo blob passata per referenza
//(Tiene conto delle dimensioni)

integer li_numero_file, li_count, i

long ll_lunghezza_file, ll_byte_letti
blob lb_blob, lb_tot_blob

if not fileexists(fs_nome_file) then return -1
ll_lunghezza_file = FileLength(fs_nome_file)
li_numero_file = FileOpen(fs_nome_file, StreamMode!, Read!, LockRead!)
if li_numero_file = -1 then return -1

if ll_lunghezza_file > 32765 then
	if Mod(ll_lunghezza_file, 32765) = 0 then
		li_count = ll_lunghezza_file/32765
	else
		li_count = (ll_lunghezza_file/32765) + 1
	end if
else
	li_count = 1
end if

for i = 1 to li_count
	ll_byte_letti = FileRead(li_numero_file, lb_blob)
	if ll_byte_letti = -1 then return -1
	lb_tot_blob = lb_tot_blob + lb_blob
next
FileClose(li_numero_file)
fb_blob = lb_tot_blob
return 0
end function

public function boolean uof_file_write (string as_filepath, string as_message, boolean ab_append, ref string as_error);/**
 * stefanop
 * 29/11/2011
 *
 * Funzione che scrive all'interno di un file la stringa passata per parametro
 **/
 
int li_handle, li_risposta
 
if ab_append then
	li_handle = fileopen(as_filepath, linemode!, Write!, LockWrite!, Append!)
else
	li_handle = fileopen(as_filepath, linemode!, Write!, LockWrite!, Replace!)
end if

if li_handle = -1 then
	as_error = "Errore durante l'apertura del file " + as_filepath + ".~r~nVerificare le connessioni delle unità di rete"
	return false
end if

li_risposta = filewrite(li_handle, as_message)
fileclose(li_handle)

return li_risposta >= 0
end function

public function integer uof_get_stabilimento_utente (ref string fs_cod_deposito, ref string fs_errore);/**
 * stefanop
 * 04/10/2011
 *
 * Recupera lo stabilimento di default di un utente-operatore (tabella tab_operatori_utenti)
 		se per caso ce nè più di uno di default torna il primo che becca
 **/
 
return uof_get_stabilimento_utente(s_cs_xx_cod_utente, fs_cod_deposito, fs_errore)
end function

public function integer uof_get_stabilimento_operatore (string as_cod_operatore, ref string as_cod_deposito, ref string as_errore);/**
 * stefanop
 * 21/12/2011
 *
 * Recupera lo stabilimento di default di un utente-operatore (tabella tab_operatori_utenti)
 *	se per caso ce nè più di uno di default torna il primo che becca
 **/
 
string			ls_sql
datastore	lds_data
long			ll_ret

setnull(as_cod_deposito)

//se sei CS_SYSTEM torna vuoto
if s_cs_xx_cod_utente = "CS_SYSTEM" then return 0

ls_sql = "select cod_deposito "+&
			"from tab_operatori_utenti "+&
			"where cod_azienda='"+s_cs_xx_cod_azienda+"' and "+&
						"cod_operatore='"+as_cod_operatore+"' and "+&
						"flag_default='S' "

ll_ret = uof_crea_datastore(lds_data, ls_sql, as_errore)

choose case ll_ret
	case is < 0
		//in fs_errore il messaggio
		return -1
		
	case is > 0
		//prendo la prima riga
		as_cod_deposito = lds_data.getitemstring(1, "cod_deposito")
		if trim(as_cod_deposito)="" then setnull( as_cod_deposito)
		
		return 0
		
	case else
		//deposito non specificato (torna NULL)
		return 0
		
end choose

end function

public subroutine uof_merge_arrays (ref string aa_array_to_merge[], string aa_array_to_read[]);long ll_position
any la_value

for ll_position = 1 to upperbound(aa_array_to_read[])
	la_value = aa_array_to_read[ll_position]
	
	if not uof_in_array(la_value, aa_array_to_merge[]) then
		//non esiste, inserisci
		aa_array_to_merge[upperbound(aa_array_to_merge[]) + 1] = la_value
	end if
next
end subroutine

public function boolean uof_in_array (string aa_search[], string aa_array[], ref long al_position);/**
 * stefanop
 * 18/10/2011
 *
 * Controllo se un determinato valore esiste all'interno di un array e ne ritorno la posizione
 * Ritorna TRUE se l'elemento è presente e FALSE se non è stato trovato
 **/
 
long ll_i, ll_y

al_position = -1

for ll_i = 1 to upperbound(aa_search)
	
	for ll_y = 1 to upperbound(aa_array)
	
		if aa_search[ll_i] = aa_array[ll_y] then
			al_position = ll_y
			return true
		end if
		
	next
	
next

return false
end function

public function boolean uof_in_array (string aa_search[], string aa_array[]);long ll_pos

return uof_in_array(aa_search[], aa_array[], ll_pos)
end function

public function string uof_get_operatore_utente (string as_cod_utente);/**
 * stefanop
 * 25/01/2012
 *
 * Recupero l'operatore associato all'utentepassato per parametro
 **/
 
string ls_sql, ls_cod_operatore
long ll_row
datastore lds_store

ls_sql = "SELECT cod_operatore FROM tab_operatori_utenti WHERE cod_azienda='" + s_cs_xx_cod_azienda +"' AND cod_utente='" + as_cod_utente + "'"
ls_sql += " ORDER BY flag_default"

ll_row = uof_crea_datastore(lds_store, ls_sql)

if ll_row < 0 then
	setnull(ls_cod_operatore)
else
	
	ls_cod_operatore = lds_store.getitemstring(1,1)
	
	if ls_cod_operatore = "" then setnull(ls_cod_operatore)
	
end if

return ls_cod_operatore
end function

public function string uof_get_operatore_utente ();/**
 * stefanop
 * 25/01/2012
 *
 * Recupero l'operatore associato all'utentepassato per parametro
 **/
 
return uof_get_operatore_utente(s_cs_xx_cod_utente)
end function

public function string uof_progressivo_alfanumerico (string as_code);/**
 * stefanop
 * 20/04/2009
 *
 * La funzione crea un progressivo alfanumerico a partire dalla stringa passata
 * Esempio di successione: 0,1,2,3,4,5,6,7,8,9,A,B,C,D,...,96,97,98,99,9A,9B,9C
 */
 
integer ll_i, ll_max
string ls_return, ls_char, ls_appo[]
	
ll_max = len(as_code)
if ll_max <=0 then return ""

ls_return = as_code

//carica un array con i caratteri della stringa
for ll_i = 1 to ll_max
	ls_appo[ll_i] = mid(ls_return, ll_i, 1)
next

for ll_i = ll_max to 1 step -1
	//preleva i caratteri a partire da destra
	ls_char = ls_appo[ll_i]
	
	choose case ls_char
		case "9"
			//fallo diventare "A"
			ls_appo[ll_i] = "A"
			exit
			
		case "Z"
			//metti il carattere corrente a "0"
			ls_appo[ll_i] = "0"
			
			//vai al successivo carattere spostandoti verso sinistra			
			
		case else
			//incrementa il suo codice ascii
			ls_appo[ll_i] = char(asc(ls_char) + 1)
			exit
			
	end choose
next

ls_return = ""
for ll_i = 1 to ll_max
	ls_return +=  ls_appo[ll_i]
next

return ls_return
end function

public function boolean uof_dw_has_column (ref datawindow adw_datawindow, string as_column_name);/**
 * stefanop
 * 30/01/2012
 *
 * Controllo che la colonna esista all'interno della datawindow passata
 **/
 
string ls_describe

ls_describe = adw_datawindow.describe(as_column_name + ".ColType")

return (ls_describe <> "!")
end function

public function string uof_nome_cella (long fl_riga, long fl_colonna);/*Questa funzione restituisce il nome di una cella di un foglio excel se gli passi il numero riga ed il numero colonna
lettere dell'alfabeto completo

Viene usata dalla funzione get value di questo stesso oggetto
*/

string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if fl_colonna > 26 then
	ll_resto = mod(fl_colonna, 26)
	ll_i = long(fl_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	fl_colonna = ll_resto 
end if

if fl_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[fl_colonna] + string(fl_riga)
end if	

return ls_char
end function

public subroutine uof_get_value (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref string fs_tipo, oleobject fole_excel);OLEObject		lole_foglio
any				lany_ret
string ls_1

//legge dalla cella (fl_riga, fl_colonna) del foglio di lavoro fs_foglio
/*
prova prima con valore decimale: 	se ok torna il valore in fd_valore e mette fs_tipo="D"
poi prova con valore string:				se ok torna il valore in fs_valore e mette fs_tipo="S"

se proprio non riesce a leggere mette a fs_tipo=NULL e ritorna
*/

fs_tipo = ""
setnull(fd_valore)
setnull(fs_valore)

lole_foglio = fole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)
ls_1 = uof_nome_cella(fl_riga, fl_colonna)

//lany_ret = lole_foglio.range(wf_nome_cella(fl_riga, fl_colonna) + ":" + wf_nome_cella(fl_riga , fl_colonna) ).value
lany_ret = lole_foglio.cells[fl_riga,fl_colonna].value

if isnull(lany_ret) then 
	fs_tipo = "S"
	setnull(fs_valore)
	return
end if

try
	//prova con valore stringa
	fs_valore = lany_ret
	fs_tipo = "S"
catch (RuntimeError rte2)
	setnull(fs_valore)
	setnull(fs_tipo)
end try


//try
//	//prova con valore decimale
//	fd_valore = lany_ret
//	fs_tipo = "D"
//catch (RuntimeError rte1)
//	
//	setnull(fd_valore)
//	setnull(fs_tipo)
//	try
//		//prova con valore stringa
//		fs_valore = lany_ret
//		fs_tipo = "S"
//	catch (RuntimeError rte2)
//		setnull(fs_valore)
//		setnull(fs_tipo)
//	end try
//	
//end try
end subroutine

public function boolean uof_create_transaction_from (ref transaction at_from, ref transaction at_to, ref string as_error);/**
 * stefanop
 * 10/02/2012
 *
 * Imposta i parametri di una nuova conessione recuperandoli dalla transazione passata
 **/


string ls_db,ls_logpass
integer li_risposta

at_to = create transaction

at_to.ServerName = at_from.ServerName
at_to.LogId = at_from.LogId
at_to.DBMS = at_from.DBMS
at_to.Database = at_from.Database
at_to.UserId = at_from.UserId
at_to.LogPass = at_from.LogPass
at_to.DBPass = at_from.DBPass
at_to.dbparm = at_from.dbparm
at_to.Lock = at_from.Lock

connect using at_to;

if at_to.sqlcode = 0 then
	return true
else
	as_error = at_to.sqlerrtext
	return false
end if
end function

public function integer uof_get_stabilimento_operaio (string as_cod_operaio, ref string fs_cod_deposito, ref string fs_errore);/**
 * stefanop
 * 14/02/2012
 **/
 
string			ls_cod_utente


setnull(fs_cod_deposito)

select cod_utente
into :ls_cod_utente
from anag_operai
where
	cod_azienda = :s_cs_xx_cod_azienda and
	cod_operaio = :as_cod_operaio;
	
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in ricerca del codice utente dall'anagrafica Operai.~r~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_cod_utente) or ls_cod_utente = "" then
	fs_errore = "L'operaio non ha nessun utente collegato."
	return 0
end if

return uof_get_stabilimento_utente(ls_cod_utente, fs_cod_deposito, fs_errore)
end function

public function integer uof_get_stabilimento_utente (string as_cod_utente, ref string fs_cod_deposito, ref string fs_errore);/**
 * stefanop
 * 04/10/2011
 *
 * Recupera lo stabilimento di default di un utente-operatore (tabella tab_operatori_utenti)
 		se per caso ce nè più di uno di default torna il primo che becca
 **/
 
string			ls_sql
datastore	lds_data
long			ll_ret

setnull(fs_cod_deposito)

//se sei CS_SYSTEM torna vuoto
if s_cs_xx_cod_utente = "CS_SYSTEM" then return 0

ls_sql = "select cod_deposito "+&
			"from tab_operatori_utenti "+&
			"where cod_azienda='"+s_cs_xx_cod_azienda+"' and "+&
						"cod_utente='"+ as_cod_utente+"' and "+&
						"flag_default='S' "

ll_ret = uof_crea_datastore(lds_data, ls_sql, fs_errore)

choose case ll_ret
	case is < 0
		//in fs_errore il messaggio
		return -1
		
	case is > 0
		//prendo la prima riga
		fs_cod_deposito = lds_data.getitemstring(1, "cod_deposito")
		if trim(fs_cod_deposito)="" then setnull( fs_cod_deposito)
		
		return 0
		
	case else
		//deposito non specificato (torna NULL)
		return 0
		
end choose

end function

public function boolean uof_is_gibus ();/**
 * stefanop
 * 14/02/2012
 *
 * Controllo se sono nell'azienda GIBUS
 **/
 
 
string ls_flag

select flag
into :ls_flag
from parametri_azienda
where
	cod_azienda = :s_cs_xx_cod_azienda and
	cod_parametro = 'GIB' and
	flag_parametro = 'F';
	
if ls_flag = "S"  then
	return true
else
	return false
end if
	

end function

public subroutine uof_get_parametro (string as_cod_parametro, ref long al_numero);al_numero = long(uof_get_parametri("M", as_cod_parametro, "N"))
end subroutine

public subroutine uof_get_parametro (string as_cod_parametro, ref date ad_date);ad_date = date(uof_get_parametri("M", as_cod_parametro, "D"))
end subroutine

public subroutine uof_get_parametro (string as_cod_parametro, ref string as_stringa);as_stringa = string(uof_get_parametri("M", as_cod_parametro, "S"))
end subroutine

public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref date ad_date);ad_date = date(uof_get_parametri("A", as_cod_parametro, "D"))
end subroutine

public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref string as_stringa);as_stringa = string(uof_get_parametri("A", as_cod_parametro, "S"))
end subroutine

public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref long al_long);al_long = long(uof_get_parametri("A", as_cod_parametro, "N"))
end subroutine

private function any uof_get_parametri (string as_tipo_tabella, string as_cod_parametro, string as_flag_parametro);/**
 * stefanop
 * 17/02/2012
 *
 * Funzione generica che recupera i parametri dallle tabelle di configurazione
 * Parametri:
 * 		as_tipo_tabella = (M)ultiaziendale, (A)ziendale, (O)mnia, (U)tente
 *		as_flag_parametro = (S)tringa, (N)umero, (D)ata
 **/
 
string ls_sql, ls_where
any la_any
datastore lds_store

// Compongo SQL
ls_sql = "SELECT "

choose case as_flag_parametro
	case "S"
		ls_sql += " stringa "
		
	case "N", "DEC"
		ls_sql += " numero "
		
	case "D"
		ls_sql += " data "
	
	case "F"
		ls_sql += " flag "
end choose

// stefanop: 20/09/2012: fix
if as_flag_parametro = "DEC" then as_flag_parametro = "N"

ls_sql += " FROM "
ls_where = " WHERE cod_parametro='" + as_cod_parametro + "' AND flag_parametro='" + as_flag_parametro + "' "

choose case as_tipo_tabella
	case "M"
		ls_sql += " parametri "
		
	case "A"
		ls_sql += " parametri_azienda "
		ls_where += " AND cod_azienda='" + s_cs_xx_cod_azienda + "'"
		
	case "U"
		ls_sql += " parametri_azienda_utente "
		ls_where += " AND cod_azienda='" + s_cs_xx_cod_azienda + "' AND cod_utente='" + s_cs_xx_cod_utente + "'"
		
	case "O"
		ls_sql += " parametri_omnia "
		ls_where += " AND cod_azienda='" + s_cs_xx_cod_azienda + "'"
end choose

ls_sql += ls_where
// ----

setnull(la_any)

if uof_crea_datastore(lds_store, ls_sql) > 0 then
	
	choose case as_flag_parametro
		case "S", "F"
			la_any = lds_store.getitemstring(1,1)
			
		case "N"
			la_any = lds_store.getitemdecimal(1,1)
			
		case "D"
			la_any = lds_store.getitemdate(1,1)

	end choose
	
end if

return la_any

end function

public function integer uof_tipo_tracciato_ord_acq (string fs_cod_fornitore, ref string fs_errore);string	ls_EO1, ls_EO2, ls_EO3

//funzione che legge a quale tipo di tracciato risponde il fornitore
//EO1		tracciato VIV								excel
//EO2		tracciato ANODALL-EXTRUSION		txt
//EO3		tracciato FONDERIA BUSTREO			csv

//leggo nei vari parametri che contengono i codici fornitore tra singoli apici, separati da virgola.
select stringa
into :ls_EO1
from parametri_azienda
where 	cod_azienda=:s_cs_xx_cod_azienda and
			cod_parametro='EO1' and
			flag_parametro='S';

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro aziendale 'EO1' (codici fornitore del tracciato 1 VIV): "+sqlca.sqlerrtext
	return -1
end if
if isnull(ls_EO1) then ls_EO1 = ""

//-------
select stringa
into :ls_EO2
from parametri_azienda
where 	cod_azienda=:s_cs_xx_cod_azienda and
			cod_parametro='EO2' and
			flag_parametro='S';

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro aziendale 'EO2' (codici fornitore del tracciato 2 ANODALL): "+sqlca.sqlerrtext
	return -1
end if
if isnull(ls_EO2) then ls_EO2 = ""

//-------
select stringa
into :ls_EO3
from parametri_azienda
where 	cod_azienda=:s_cs_xx_cod_azienda and
			cod_parametro='EO3' and
			flag_parametro='S';

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro aziendale 'EO3' (codici fornitore del tracciato 3 BUSTREO): "+sqlca.sqlerrtext
	return -1
end if
if isnull(ls_EO3) then ls_EO3 = ""


//leggo a quale tipo di tracciato risponde il fornitore
if pos(ls_EO1, "'"+fs_cod_fornitore+"'")>0 then
	//VIV
	return 1

elseif pos(ls_EO2, "'"+fs_cod_fornitore+"'")>0 then
	//ANODALL-EXTRUSION
	return 2

elseif pos(ls_EO3, "'"+fs_cod_fornitore+"'")>0 then
	//BUSTREO
	return 3

else
	//il fornitore non risponde a nessuno dei tracciati: esporto secondo lo standard
	return 0
	
end if
end function

public function integer uof_filigrana (string fs_path_file, ref datawindow dw_data, boolean fb_visualizza_subito, ref string fs_errore);// imposta Filigrana su datawindow:  non la rende visibile se l'argomento fb_visualizza_subito

if isnull(fs_path_file) or fs_path_file="" then
	//fs_errore = "Non è stato specificato il path del file immagine della filigrana!"
	return 0
end if

fs_errore = dw_data.modify("DataWindow.Picture.File='" + fs_path_file + "'")
if not isnull(fs_errore) and fs_errore <> "" then
	fs_errore = "File: " + fs_errore
	return -1
end if
//----------------------------------
fs_errore = dw_data.modify("DataWindow.Picture.Mode=1")
if not isnull(fs_errore) and fs_errore <> "" then
	fs_errore = "Mode: " + fs_errore
	return -1
end if

//fs_errore = dw_data.modify("DataWindow.Picture.Transparency=80")
//if not isnull(fs_errore) and fs_errore <> "" then
//	fs_errore = "Transparency: " + fs_errore
//	return -1
//end if

fs_errore = dw_data.modify("DataWindow.Print.Background='Yes'")
if not isnull(fs_errore) and fs_errore <> "" then
	fs_errore = "Background: " + fs_errore
	return -1
end if

if fb_visualizza_subito then
	fs_errore = dw_data.modify("DataWindow.brushmode=6")
else
	fs_errore = dw_data.modify("DataWindow.brushmode=0")
end if

if not isnull(fs_errore) and fs_errore <> "" then
	fs_errore = "Brushmode: " + fs_errore
	return -1
end if

return 0


end function

public function string uof_data_db (date adt_date);return string(adt_date, s_cs_xx_db_funzioni_formato_data)
end function

public function string uof_data_db (datetime adt_date);return string(adt_date, s_cs_xx_db_funzioni_formato_data)
end function

public subroutine uof_get_prossimo_giorno_lavorativo (date adt_date, boolean ab_feste_comandate, ref date adt_working_date);/**
 * stefanop + donatoc
 * 04/02/2012
 *
 * Calcolo il primo giorno lavorativo valido a partire da una data passata
 * La funzione tiene conto dei sabati e domeniche + eventuali festivita comandate
 **/

int li_day_number

adt_working_date = relativedate(adt_date, 1)

do while true
	li_day_number = daynumber(adt_working_date)
	
	if li_day_number > 1 and li_day_number < 7 then 
		// TODO è una festività comandata? se si NON esco
		exit
	end if

	
	adt_working_date = relativedate(adt_working_date, 1)
loop
end subroutine

public function integer uof_get_ultimo_giorno_mese (integer ai_mese, integer ai_anno);/**
 * stefanop + donatoc
 * 04/02/2012
 *
 * Calcolo l'ultimo giorno del mese valido
 **/
 
long ll_ultimo_giorno

choose case ai_mese
	case 11,4,6,9
		ll_ultimo_giorno = 30
		
	case 2
		//se bisestile sono 29, altrimenti 28
		//sono bisestili gli anni divisibili per quattro, tranne quelli di inizio secolo (cioè divisibili per 100) non divisibili per 400
		if mod(ai_anno, 4) = 0 then
			
			if mod(ai_anno, 100) = 0 then
				
				if mod(ai_anno, 400) = 0 then
					//bisestile
					ll_ultimo_giorno = 28
				else
					//non bisestile
					ll_ultimo_giorno = 29
				end if
				
			else
				//sicuramente bisestile
				ll_ultimo_giorno = 29
			end if
			
		else
			//sicuramente non bisestile
			ll_ultimo_giorno = 28
		end if
		
	case else
		ll_ultimo_giorno = 31
		
end choose

return ll_ultimo_giorno
end function

public function integer uof_get_ultimo_giorno_mese (date adt_date);/**
 * stefanop + donatoc
 * 04/02/2012
 *
 * Calcolo l'ultimo giorno del mese valido
 **/
 
return uof_get_ultimo_giorno_mese(month(adt_date), year(adt_date))
end function

public function string uof_get_operaio_utente (string as_cod_utente);/**
 * stefanop
 * 25/01/2012
 *
 * Recupero l'operatore associato all'utentepassato per parametro
 **/
 
string ls_sql, ls_cod_operatore
long ll_row
datastore lds_store

ls_sql = "SELECT cod_operaio FROM anag_operai WHERE cod_azienda='" + s_cs_xx_cod_azienda +"' AND cod_utente='" + as_cod_utente + "'"

ll_row = uof_crea_datastore(lds_store, ls_sql)

if ll_row < 0 then
	setnull(ls_cod_operatore)
else
	
	ls_cod_operatore = lds_store.getitemstring(1,1)
	
	if ls_cod_operatore = "" then setnull(ls_cod_operatore)
	
end if

return ls_cod_operatore
end function

public function string uof_get_operaio_utente ();/**
 * stefanop
 * 25/01/2012
 *
 * Recupero l'operatore associato all'utentepassato per parametro
 **/
 
return uof_get_operaio_utente(s_cs_xx_cod_utente)
end function

public function boolean uof_is_supervisore (string as_cod_utente);/**
 * stefanop
 * 10/04/2012
 *
 * Indica se l'utente ha il flag_supervisore attivato
 **/
 
string ls_flag

if as_cod_utente = "CS_SYSTEM" then return true

select flag_supervisore
into :ls_flag
from utenti
where cod_utente = :as_cod_utente;

if sqlca.sqlcode = 0 and ls_flag = 'S' then
	return true
else
	return false
end if


end function

public function boolean uof_is_admin (string as_cod_utente);/**
 * stefanop
 * 10/04/2012
 *
 * Indica se l'utente ha il flag_collegato attivato
 **/
 
string ls_flag

if as_cod_utente = "CS_SYSTEM" then return true

select flag_collegato
into :ls_flag
from utenti
where cod_utente = :as_cod_utente;

if sqlca.sqlcode = 0 and ls_flag = 'S' then
	return true
else
	return false
end if


end function

public function long uof_substr_count (string as_text, string as_find);/**
  * stefanop
  * 24/04/2012
  *
  * Conta le occorrenze di una stringa all'interno di un testo
  **/
  
  
long ll_pos, ll_length, ll_count

ll_count = 0

ll_length = len(as_find)
ll_pos = pos(as_text, as_find)

do while ll_pos > 0
	
	ll_count++
	
	ll_pos = pos(as_text, as_find, ll_pos + ll_length)
	
loop

return ll_count
end function

public function integer uof_get_file_info (string as_file_source, ref string as_file_dir, ref string as_file_name, ref string as_file_ext);/**
 * stefanop
 * 03/02/2012
 *
 * Dato un percorso di un file recupero le informazioni della cartella, il nome e l'estensione
 **/
 
long ll_last_pos, ll_last_pos_point

setnull(as_file_dir)
setnull(as_file_name)
setnull(as_file_ext)

if isnull(as_file_source) or as_file_source = "" then return -1

ll_last_pos = lastpos(as_file_source, "\")
ll_last_pos_point = lastpos(as_file_source, ".")

as_file_dir = left(as_file_source, ll_last_pos)
as_file_name = mid(as_file_source, ll_last_pos + 1, ll_last_pos_point - ll_last_pos - 1)
as_file_ext = mid(as_file_source, ll_last_pos_point + 1)
end function

public function date uof_string_to_date (string as_string_date);
return date(left(as_string_date, 4) + "-" + mid(as_string_date, 5,2) + "-" + right(as_string_date, 2))
end function

public function integer uof_esporta_clienti_txt (string as_percorso_file, ref string as_errore);/*
----Funzione che legge i dati dalla tabella anagrafica clienti e scrive alcuni campi su un file di testo
*/

String ls_cod_cliente,ls_rag_soc_1,ls_indirizzo,ls_cap,ls_localita,ls_provincia,ls_cod_nazione,ls_partita_iva,ls_cod_fiscale,ls_flag_blocco,ls_valore
int li_num_file


declare cu_cursore cursor for  

	select cod_cliente,rag_soc_1,indirizzo,cap,localita,provincia,cod_nazione,partita_iva,cod_fiscale,flag_blocco
	from anag_clienti
	where cod_azienda=:s_cs_xx_cod_azienda
	order by cod_cliente;

open cu_cursore;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore 'cu_cursore'~r~n" + sqlca.sqlerrtext
	return -1
end if

//Apertura del file

li_num_file=FileOpen(as_percorso_file, linemode!, write!, shared!, replace!)

FileWriteEx(li_num_file,"codice_cliente;ragione_sociale;indirizzo;cap;località,provincia,nazione;partita_iva;codice_fiscale;legale;bloccati") //Intestazione file

do while true
	
	fetch cu_cursore into :ls_cod_cliente,:ls_rag_soc_1,:ls_indirizzo,:ls_cap,:ls_localita,:ls_provincia,:ls_cod_nazione,:ls_partita_iva,:ls_cod_fiscale,:ls_flag_blocco;

	
	if sqlca.sqlcode < 0 then
		close cu_cursore;
		as_errore = "Errore in FETCH cursore 'cu_cursore'~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	//Controllo che le variabili non siano nulle,se sono nulle ("") altrimenti sostituisco il ; con il -
	
	uof_replace_string(ls_cod_cliente,";","-")	//Sostituisce il ; con il -
	
	if isnull(ls_rag_soc_1) then
		ls_rag_soc_1=""
	else 
		uof_replace_string(ls_rag_soc_1,";","-")	
	end if
	
	if isnull(ls_indirizzo) then
		ls_indirizzo=""
	else 
		uof_replace_string(ls_indirizzo,";","-")
	end if
	
	if isnull(ls_cap) then
		ls_cap=""
	else 
		uof_replace_string(ls_cap,";","-")
	end if
	
	if isnull(ls_localita) then
		ls_localita=""
	else 
		uof_replace_string(ls_localita,";","-")	
	end if
	
	if isnull(ls_provincia) then
		ls_provincia=""
	else
		uof_replace_string(ls_provincia,";","-")
	end if
	
	if isnull(ls_cod_nazione) then
		ls_cod_nazione=""
	else
		uof_replace_string(ls_cod_nazione,";","-")
	end if
	
	if isnull(ls_partita_iva) then
		ls_partita_iva=""
	else
		uof_replace_string(ls_partita_iva,";","-")
	end if
	
	if isnull(ls_cod_fiscale) then
		ls_cod_fiscale=""
	else
		uof_replace_string(ls_cod_fiscale,";","-")	
	end if	
	
	if isnull(ls_flag_blocco) then
		ls_flag_blocco=""
	else
		
		if ls_flag_blocco="S" then
			ls_flag_blocco="B"
		else
			ls_flag_blocco=""
		end if
		
	end if
	
	ls_valore=ls_cod_cliente+";"+ls_rag_soc_1+";"+ls_indirizzo+";"+ls_cap+";"+ls_localita+";"+ls_provincia+";"+ls_cod_nazione+";"+ls_partita_iva+";"+ls_cod_fiscale+";"+""+";"+ls_flag_blocco+";"
	
	FileWriteEx(li_num_file,ls_valore)
	

loop

FileClose(li_num_file)

close cu_cursore;

return 0	//Se non ci sono errori


end function

public function string uof_get_file_attribute (string as_file_path, integer ai_attributes);/**
 * stefanop
 * 02/07/2012
 *
 * Funzione trovata da Matteo Casarin
 *
 * Recupera gli attributi di un file.
 * Attributi possibili:
 * 0 = Filename
 * 1 = Size
 * 2 = Type
 * 3 = Modified Date
 * 4 = Created Date
 * 5 = Unknown
 * 6 = Attributes
 * 7 = ???
 * 8 = Domene
 **/
 

String ls_path, ls_file,ls_ret=""
OLEObject obj_shell, obj_folder, obj_item

obj_shell = CREATE OLEObject
obj_shell.ConnectToNewObject( 'shell.application' )

ls_path = Left( as_file_path, LastPos( as_file_path, "\" ) )
ls_file = Mid( as_file_path, LastPos( as_file_path, "\" ) + 1 )

IF FileExists( as_file_path ) THEN
	
	obj_folder = obj_shell.NameSpace( ls_path )    
   
	IF IsValid( obj_folder ) THEN 
		
		obj_item = obj_folder.ParseName( ls_file )  
		
		IF IsValid( obj_item ) THEN 
			ls_ret = obj_folder.GetDetailsOf( obj_item, ai_attributes)       
		END IF
	END IF
END IF

DESTROY obj_shell
DESTROY obj_folder
DESTROY obj_item

RETURN ls_ret
end function

public function time uof_seconds_to_time (long al_seconds);/**
 * stefanop
 * 03/07/2012
 *
 * Converte un numero di secondi in una variabile time
 **/
 
 
long ll_second, ll_minute, ll_hour

ll_second = mod(al_seconds, 60)
ll_minute = al_seconds / 60
ll_hour = 0

if ll_minute >= 60 then
	
	ll_hour = ll_minute / 60
	ll_minute = mod(ll_minute, 60)
	
end if

return time(string(ll_hour) + ":" +  string(ll_minute) + ":" + string(ll_second))
end function

public function time uof_time_diff (time at_time_start, time at_time_end);/**
 * stefanop
 * 03/07/2012
 *
 * Converte un numero di secondi in una variabile time
 **/
 
 
return uof_seconds_to_time(secondsafter(at_time_start, at_time_end))
end function

public function boolean uof_anno_bisestile (integer ai_anno);//Donato 19/07/2012
//torna TRUE se l'anno della variabile "ai_anno" è bisestile, altrimenti torna FALSE

//algoritmo anno bisestile
//se è 
//		( DIVISIBILE per 4 AND NON divisibile per 100) OR (DIVISIBILE per 400)

if (mod(ai_anno, 4) = 0 and mod(ai_anno, 100) <> 0) or mod(ai_anno, 400) = 0 then
	//BISESTILE
	return true
else
	//NON BISESTILE
	return false
end if
end function

public function boolean uof_confronta_arrays (string aa_array_1[], string aa_array_2[]);long ll_i, ll_dim1
any la_value1, la_value2

ll_dim1 = upperbound(aa_array_1[])

if ll_dim1=upperbound(aa_array_2[]) then
	if ll_dim1=0 then
		//di sicuro sono uguali, in quanto entrambi vuoti
		return true
	end if
else
	//di sicuro non sono uguali
	return false
end if


//se arrivi fin qui devi leggere le componenti dei due array (che di sicuro hanno la stessa dimensione)
for ll_i = 1 to ll_dim1
	la_value1 = aa_array_1[ll_i]
	la_value2 = aa_array_2[ll_i]
	
	if la_value1=la_value2 then
	else
		return false
	end if
	
next

return true
end function

public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref decimal ad_decimal);ad_decimal = dec(uof_get_parametri("A", as_cod_parametro, "DEC"))
end subroutine

public function string uof_string_to_rte (string as_text);/**
 * stefanop
 * 26/09/2012
 *
 * Converte un plain-text in formato RTE con font Arial 9
 **/
 
if isnull(as_text) or as_text = "" then
	return ""
elseif left(as_text, 12) ="{\rtf1\ansi\" then
	return as_text
else
	return "{\rtf1\ansi\ansicpg1252\uc1\deff0{\fonttbl" + &
			"{\f0\fnil\fcharset0\fprq2 Arial;}" + &
			"{\f1\fswiss\fcharset0\fprq2 Arial;}" + &
			"{\f2\froman\fcharset2\fprq2 Symbol;}}" + &
			"{\colortbl;\red0\green0\blue0;\red255\green255\blue255;}" + &
			"{\stylesheet{\s0\itap0\nowidctlpar\f0\fs24 [Normal];}{\*\cs10\additive Default Paragraph Font;}}" + &
			"{\*\generator TX_RTF32 15.0.530.503;}" + &
			"\deftab1134\paperw11905\paperh16838\margl0\margt0\margr0\margb0\widowctrl\formshade" + &
			"{\*\background{\shp{\*\shpinst\shpleft0\shptop0\shpright0\shpbottom0\shpfhdr0\shpbxmargin\shpbxignore\shpbymargin\shpbyignore\shpwr0\shpwrk0\shpfblwtxt1\shplid1025{\sp{\sn shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn fillColor}{\sv 16777215}}{\sp{\sn fFilled}{\sv 1}}{\sp{\sn lineWidth}{\sv 0}}{\sp{\sn fLine}{\sv 0}}{\sp{\sn fBackground}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 1}}}}}\sectd" + &
			"\headery720\footery720\pgwsxn6750\pghsxn16838\marglsxn0\margtsxn0\margrsxn0\margbsxn0\pard\itap0\nowidctlpar\plain\f1\fs18 " + as_text + "}"
end if
end function

public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref boolean ab_flag);string	ls_flag

ls_flag = string(uof_get_parametri("A", as_cod_parametro, "F"))

ab_flag = ls_flag="S"
end subroutine

public subroutine uof_get_parametro (string as_cod_parametro, ref boolean ab_flag);string ls_flag

ls_flag = string(uof_get_parametri("M", as_cod_parametro, "F"))

ab_flag = ls_flag = "S"
end subroutine

public function boolean uof_get_date_from_giornomese (long fl_giorno_mese, integer fi_anno, ref date fdd_date);/*
Questa funzione costruisce una data a partire da un numero formattato come (d)dmm
esempi

  803  	->  	08/03/anno
1012		->		10/12/anno

dove anno è la variabile fi_anno, o se vuota considera l'anno corrente

Torna TRUE se tutto a posto, FALSE in caso di valori errati
*/

integer	li_mese, li_giorno, li_max

//gli ultimi due numeri sono sempre di un mese, che deve trovarsi tra 01 e 12
li_mese = integer(right(string(fl_giorno_mese), 2))

if li_mese>0 and li_mese <= 12 then
else
	return false
end if

//estreaggo i giorni come divisione intera per 100
//infatti ad esempio 				int( 803/100 ) = int( 8.03 ) = 8
//										int( 1012/100 ) = int( 10.12 ) = 10
li_giorno = int(fl_giorno_mese / 100)

choose case li_mese
	case 2		//febbraio
		li_max = 28
		
	case 4, 6, 9, 11
		li_max = 30
		
	case else
		li_max = 31
		
end choose

if li_giorno>0 and li_giorno <= li_max then
else
	return false
end if

if fi_anno<=0 or isnull(fi_anno) then fi_anno=year(today())

fdd_date = date(fi_anno, li_mese, li_giorno)

return true

end function

public function string uof_get_random_filename ();return 		string( year(today()),"0000") 	+ string( month(today()),"00")		+ string( day(today()),"00") + &
				string( hour(now()),"00") 		+ string( minute(now()),"00") 		+ string( second(now()),"00") 

end function

public function integer uof_setnull_fatt_proforma (integer fi_anno_ordine, long fl_num_ordine, long fl_riga_ordine, ref string fs_messaggio);//28/01/2013 Donato
//su richiesta di Beatrice
//chiamata prima di cancellare una riga di ordine di vendita
//se esiste una riga di fattura proforma riferita alla riga di ordine, mette a NULL i 3 campi in det_fat_ven
//questo per evitare l'errore di violazione di FK tra det_ord_ven e det_fat_ven

update det_fat_ven
set			fd.anno_reg_ord_ven=null,
    			fd.num_reg_ord_ven=null,
    			fd.prog_riga_ord_ven=null
from det_fat_ven as fd
join tes_fat_ven as ft on 	ft.cod_azienda=fd.cod_azienda and
                        			ft.anno_registrazione=fd.anno_registrazione and
                        			ft.num_registrazione=fd.num_registrazione
join det_ord_ven as od on 	od.cod_azienda=fd.cod_azienda and
                        				od.anno_registrazione=fd.anno_reg_ord_ven and
                        				od.num_registrazione=fd.num_reg_ord_ven and
                        				od.prog_riga_ord_ven=fd.prog_riga_ord_ven
where od.cod_azienda=:s_cs_xx_cod_azienda and 
		od.anno_registrazione=:fi_anno_ordine and 
		od.num_registrazione=:fl_num_ordine and 
		od.prog_riga_ord_ven=:fl_riga_ordine and
		ft.cod_tipo_fat_ven in (		select cod_tipo_fat_ven
                           					from tab_tipi_fat_ven
                            					where 	cod_azienda=:s_cs_xx_cod_azienda and 
														flag_tipo_fat_ven in ('P','F'));

if sqlca.sqlcode<0 then
	fs_messaggio = "Errore in scollegamento righe fatture PRO-forma da righe ordine di vendita: "+sqlca.sqlerrtext
	return -1
else
	return 0
end if
end function

public function integer uof_setnull_fatt_proforma (integer fi_anno_ordine, long fl_num_ordine, ref string fs_messaggio);//28/01/2013 Donato
//su richiesta di Beatrice
//chiamata prima di cancellare un ordine di vendita (testata)
//se esistono righe di fattura proforma riferite alle righe di ordine, mette a NULL i 3 campi in det_fat_ven
//questo per evitare l'errore di violazione di FK tra det_ord_ven e det_fat_ven

update det_fat_ven
set			fd.anno_reg_ord_ven=null,
			fd.num_reg_ord_ven=null,
			fd.prog_riga_ord_ven=null
from det_fat_ven as fd
join tes_fat_ven as ft on 	ft.cod_azienda=fd.cod_azienda and
								ft.anno_registrazione=fd.anno_registrazione and
                    			    ft.num_registrazione=fd.num_registrazione
join det_ord_ven as od on 	od.cod_azienda=fd.cod_azienda and
                        				od.anno_registrazione=fd.anno_reg_ord_ven and
                        				od.num_registrazione=fd.num_reg_ord_ven and
                        				od.prog_riga_ord_ven=fd.prog_riga_ord_ven
where 	od.cod_azienda=:s_cs_xx_cod_azienda and 
			od.anno_registrazione=:fi_anno_ordine and 
			od.num_registrazione=:fl_num_ordine and
			ft.cod_tipo_fat_ven in (		select cod_tipo_fat_ven
                           						from tab_tipi_fat_ven
                            						where 	cod_azienda=:s_cs_xx_cod_azienda and
                                   								flag_tipo_fat_ven in ('P','F'));

if sqlca.sqlcode<0 then
	fs_messaggio = "Errore in scollegamento righe fatture (T) PRO-forma da righe ordine di vendita: "+sqlca.sqlerrtext
	return -1
else
	return 0
end if
end function

public subroutine uof_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, string as_cod_lingua, string as_tabella_iva, ref string fs_cod_iva[], ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[], ref string fs_des_estesa[]);string ls_colonna_anno, ls_colonna_numero, ls_sql
long ll_i
boolean lb_leggi_italiano

//prepara l'array degli imponibili, imposte e aliquote nel documento (totali raggruppati per codice iva)
choose case upper(as_tabella_iva)
	case "IVA_BOL_ACQ"
		ls_colonna_anno = "anno_bolla_acq"
		ls_colonna_numero = "num_bolla_acq"
		
	case else
		ls_colonna_anno = "anno_registrazione"
		ls_colonna_numero = "num_registrazione"
		
end choose

ls_sql = 	"select		cod_iva,"+&
						"imponibile_iva,"+&
						"importo_iva,"+&
						"perc_iva "+&
				"from  " + as_tabella_iva + " " + & 
				"where cod_azienda = '"+s_cs_xx_cod_azienda +"' and "+&
						ls_colonna_anno + " = " + string(fl_anno_registrazione) +" and "+&
						ls_colonna_numero + " = " + string(fl_num_registrazione)

declare cu_iva dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_iva;

if sqlca.sqlcode <> 0 then return

ll_i = 0
do while true
	ll_i = ll_i + 1
	fetch cu_iva into 	:fs_cod_iva[ll_i], 
							:fd_imponibile_iva_valuta[ll_i], 
							:fd_imposta_iva_valuta[ll_i],
							:fd_aliquote_iva[ll_i];

	if sqlca.sqlcode <> 0 then exit
	
	lb_leggi_italiano = true
	
	if fd_aliquote_iva[ll_i] = 0 then
		
		if as_cod_lingua <> "IT" and not isnull(as_cod_lingua) and as_cod_lingua<>"" then
			//leggo la traduzione
			select 	des_iva, 
						des_estesa
			into   		:fs_des_esenzione[ll_i], 
						:fs_des_estesa[ll_i]
			from   	tab_ive_lingue
			where  	cod_azienda = :s_cs_xx_cod_azienda and
						cod_iva = :fs_cod_iva[ll_i] and
						cod_lingua=:as_cod_lingua;
						
			if sqlca.sqlcode = 0 then
				//traduzione presente
				lb_leggi_italiano = false
			else
				//altrimenti leggi quelle in italiano
				lb_leggi_italiano = true
			end if
		end if
		
		if lb_leggi_italiano then
			// o la lingua è IT oppure la traduzione cercata non è presente in tabella tab_ive_lingue, quindi recupera dall'italiano
			
			select 	des_iva, 
						des_estesa
			into   		:fs_des_esenzione[ll_i], 
						:fs_des_estesa[ll_i]
			from   	tab_ive
			where  	cod_azienda = :s_cs_xx_cod_azienda and
							cod_iva = :fs_cod_iva[ll_i];
		end if
		
	end if
	
loop

close cu_iva;

return

end subroutine

public function integer uof_estrai_files (ref string as_percorso_iniziale, string as_condizione, ref string as_files_estratti[], ref string as_errore);string							ls_nome_elemento, ls_percorso_files
ulong							ll_handle
long							ll_index, ll_attributo_cartella
boolean						lb_ret
str_win32_find_data		lstr_win32_find_data


//mi accerto che il percorso finisca con il backslash, in caso contrario glielo aggiungo
if right(as_percorso_iniziale, 1) <> "\" then as_percorso_iniziale += "\"

//se non hai specificato alcuna condizione di estrazione, metti *.*
if isnull(as_condizione) or as_condizione="" then as_condizione = "*.*"

//accodo la richiesta dei files con la condizione
ls_percorso_files = as_percorso_iniziale + as_condizione

//assegno l'attributo dato dal S.O. alle cartelle, per escluderle dalla lettura (caso di sottocartelle presenti nel percorso)
ll_attributo_cartella = 16
//-------------------------------------------------------------------------------------------------------------------------------------

//INIZIO ELABORAZIONE ####################################################
ll_handle = FindFirstFile(ls_percorso_files, ref lstr_win32_find_data)

if ll_handle>0 then
	do		
		try
			ls_nome_elemento = string(lstr_win32_find_data.cfilename)
			if not isnull(ls_nome_elemento) and ls_nome_elemento<>"" and ls_nome_elemento<>"." and ls_nome_elemento<>".." then
				
				//solo se non si tratta di una cartella, aggiungo nell'array
				if lstr_win32_find_data.dwfileattributes <> ll_attributo_cartella then
					ll_index += 1
					as_files_estratti[ll_index] = lstr_win32_find_data.cfilename
				end if
				
			end if
			lb_ret = FindNextFile(ll_handle, ref lstr_win32_find_data)
			
		catch (runtimeerror error)
			as_errore = error.getmessage()
			FindClose(ll_handle)
			return -1
		end try
		
	loop while lb_ret	//il ciclo continua per tutto il tempo in cui questa condizione è VERA
	
	try
		FindClose(ll_handle)		
	catch (runtimeerror error2)
		as_errore = error2.getmessage()
	end try
	
end if
//FINE ELABORAZIONE #########################################################


return upperbound(as_files_estratti[])
end function

public function integer as_reverse_array (string as_array_in[], ref string as_array_out[]);// 8-4-13 EnMe
// funzione che dato un array di strighe in input inverte l'ordine degli elementi.
string ls_null[]
long ll_i, ll_count, ll_ind

as_array_out = ls_null
ll_count = upperbound( as_array_in[])

if ll_count <= 0 then return 0
ll_ind = 0
for ll_i = ll_count to 1 step -1
	ll_ind ++
	as_array_out[ll_ind] = as_array_in[ll_i]
next

return 0
end function

public function integer uof_write_log_sistema_not_sqlca (string as_tipo_log, string as_log_id, string as_message);/**
 * stefanop
 * 27/09/2010
 *
 * Permette di scrivere le informazioni di log nella tabella interna log_sistema.
 **/
 
string				ls_sqlerrtext = ""
int						li_sqlcode = 0
datetime			ldt_today, ldt_now
transaction			ltran_transaction


if not uof_create_transaction_from(sqlca, ltran_transaction, ls_sqlerrtext) then
	return -1
end if


//ll_id_sistema++
ldt_today = datetime(today(), 00:00:00)
ldt_now = datetime(date("01/01/1900"), now())
if len(as_tipo_log) > 20 then left(as_tipo_log, 20)

as_log_id = as_log_id
//la tabella è autoincrementale
insert into log_sistema (
	data_registrazione,
	ora_registrazione,
	utente,
	flag_tipo_log,
	db_sqlcode,
	db_sqlerrtext,
	messaggio)
values (
	:ldt_today,
	:ldt_now,
	:as_log_id,
	:as_tipo_log,
	:li_sqlcode,
	:ls_sqlerrtext,
	:as_message)
using ltran_transaction;

if ltran_transaction.sqlcode <> 0 then
	rollback using ltran_transaction;
	return -1
end if

commit using ltran_transaction;

disconnect using ltran_transaction;

destroy ltran_transaction

return 0
end function

public function integer uof_log (string as_message);/**
 * Stefano Pulze
 * 09/09/09
 *
 * Funzione che permette di creare un file di log.
 * La funziona creerà un file chiamato log.txt all'interno della cartella principale dell'applicazione
 * e si preoccuperà di controllare se il programma è avviato in modalità client/server o intranet.
 *
 * @param string as_message
 * @return boolean
 */
 
string 	ls_path, ls_message, ls_log_name
integer	li_handle
boolean	lb_return

if gb_log then uof_write_log_sistema_not_sqlca('WS', id_sessione, as_message)

return 0

end function

public function string uof_format (decimal ad_decimal);if isnull(ad_decimal) then
	return " Null Value "
else
	return string(ad_decimal,"###,###,##0.0000")
end if
end function

public function string uof_format (datetime ad_datetime);if isnull(ad_datetime) then
	return " Null Value "
else
	return string(ad_datetime,"dd/mm/yyyy")
end if
end function

public function string uof_format (string as_string);if isnull(as_string) then
	return " Null Value "
else
	return as_string
end if
end function

on uo_functions.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_functions.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

