﻿$PBExportHeader$uo_gruppi_sconti.sru
$PBExportComments$UserObject Funzioni per gestione gruppi di sconto
forward
global type uo_gruppi_sconti from nonvisualobject
end type
end forward

global type uo_gruppi_sconti from nonvisualobject
end type
global uo_gruppi_sconti uo_gruppi_sconti

type variables
public:
string s_cs_xx_cod_azienda,s_cs_xx_db_funzioni_formato_data
end variables

forward prototypes
public function integer fuo_crea_gruppo_cliente (string fs_cod_cliente, string fs_cod_gruppo, datetime fdt_data_inizio_val, boolean fb_elimina, ref string fs_messaggio)
public function integer fuo_ricerca_condizioni (string fs_cod_prodotto, string fs_cod_cliente, string fs_cod_valuta, string fs_cod_agente, datetime fdt_data_inizio_val, ref double fd_sconti[], ref double fd_provv_1, ref double fd_provv_2, ref string fs_messaggio)
public function integer fuo_ricerca_livello (string fs_cod_gruppo_sconto, string fs_livello_prod_1, string fs_livello_prod_2, string fs_livello_prod_3, string fs_livello_prod_4, string fs_livello_prod_5, string fs_cod_prodotto, string fs_cod_cliente, string fs_cod_valuta, string fs_cod_agente, datetime fdt_data_inizio_val, ref double fd_sconti[], ref double fd_provv_1, ref double fd_provv_2, ref string fs_messaggio)
public function integer fuo_duplica_gruppo_cliente (string fs_cod_cliente_origine, string fs_cod_cliente_destinazione, datetime fdt_data_inizio_val, ref string fs_messaggio)
end prototypes

public function integer fuo_crea_gruppo_cliente (string fs_cod_cliente, string fs_cod_gruppo, datetime fdt_data_inizio_val, boolean fb_elimina, ref string fs_messaggio);string ls_cod_valuta, ls_sql, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5
long ll_progressivo, ll_cont, ll_max
datetime ldt_data_inizio_val
double ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_provvigione_1, ld_provvigione_2

if fb_elimina and not isnull(fs_cod_cliente) then
	delete from gruppi_sconto
	where  cod_azienda = :s_cs_xx_cod_azienda and
			 cod_cliente = :fs_cod_cliente;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante cancellazione precedenti linee di sconto. ~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
end if

select cod_valuta
into   :ls_cod_valuta
from   anag_clienti
where  cod_azienda = :s_cs_xx_cod_azienda and
       cod_cliente = :fs_cod_cliente;
if sqlca.sqlcode = 100 then
	fs_messaggio = "Cliente codice " + fs_cod_cliente + " non trovato !"
	return -1
elseif sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca cliente" + fs_cod_cliente + "~r~n Dettaglio Errore: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_valuta) then
	fs_messaggio = "Il cliente " + fs_cod_cliente + " non ha l'indicazione della valuta nella sua anagrafica "
	return -1
end if

declare cu_gruppi dynamic cursor for sqlsa;
ls_sql = " select data_inizio_val, cod_livello_prod_1, cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4, cod_livello_prod_5, sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2  " + &
			" from gruppi_sconto " + &
			" where  cod_azienda = '" + s_cs_xx_cod_azienda + "' and cod_valuta = '" + ls_cod_valuta + "' and cod_gruppo_sconto = '" + fs_cod_gruppo + "' and cod_cliente is null and cod_agente is null " + &
			" group by data_inizio_val, cod_livello_prod_1, cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4, cod_livello_prod_5, sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2 " + &
			" having data_inizio_val = max(data_inizio_val) "
prepare sqlsa from :ls_sql;
open dynamic cu_gruppi;


//" group by cod_livello_prod_1, cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4, cod_livello_prod_5 " + &



ll_cont = 0
do while 1=1
   fetch cu_gruppi into :ldt_data_inizio_val, :ls_cod_livello_prod_1, :ls_cod_livello_prod_2, :ls_cod_livello_prod_3, :ls_cod_livello_prod_4, :ls_cod_livello_prod_5, :ld_sconto_1, :ld_sconto_2, :ld_sconto_3, :ld_sconto_4, :ld_sconto_5, :ld_sconto_6, :ld_sconto_7, :ld_sconto_8, :ld_sconto_9, :ld_sconto_10, :ld_provvigione_1, :ld_provvigione_2;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_cont ++
	ll_max = 0
	select max(progressivo)
	into   :ll_max
	from   gruppi_sconto
	where  cod_azienda = :s_cs_xx_cod_azienda and
	       cod_gruppo_sconto = :fs_cod_gruppo and
			 cod_valuta = :ls_cod_valuta and
			 data_inizio_val = :fdt_data_inizio_val;
	if ll_max = 0 or isnull(ll_max) then
		ll_max = 1
	else
		ll_max ++
	end if
	
	INSERT INTO gruppi_sconto  
			( cod_azienda,   
			  cod_gruppo_sconto,   
			  cod_valuta,   
			  data_inizio_val,   
			  progressivo,   
			  cod_agente,   
			  cod_cliente,   
			  cod_livello_prod_1,   
			  cod_livello_prod_2,   
			  cod_livello_prod_3,   
			  cod_livello_prod_4,   
			  cod_livello_prod_5,   
			  sconto_1,   
			  sconto_2,   
			  sconto_3,   
			  sconto_4,   
			  sconto_5,   
			  sconto_6,   
			  sconto_7,   
			  sconto_8,   
			  sconto_9,   
			  sconto_10,   
			  provvigione_1,   
			  provvigione_2 )  
	VALUES ( :s_cs_xx_cod_azienda,   
			  :fs_cod_gruppo,   
			  :ls_cod_valuta,   
			  :fdt_data_inizio_val,   
			  :ll_max,   
			  null,   
			  :fs_cod_cliente,   
			  :ls_cod_livello_prod_1,   
			  :ls_cod_livello_prod_2,   
			  :ls_cod_livello_prod_3,   
			  :ls_cod_livello_prod_4,   
			  :ls_cod_livello_prod_5,   
			  :ld_sconto_1,   
			  :ld_sconto_2,   
			  :ld_sconto_3,   
			  :ld_sconto_4,   
			  :ld_sconto_5,   
			  :ld_sconto_6,   
			  :ld_sconto_7,   
			  :ld_sconto_8,   
			  :ld_sconto_9,   
			  :ld_sconto_10,   
			  :ld_provvigione_1,   
			  :ld_provvigione_2 )  ;
	 if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante inserimento condizioni cliente " + fs_cod_cliente + "~r~nDettaglio errore:" + sqlca.sqlerrtext
		close cu_gruppi;
		return -1
	end if
loop

close cu_gruppi;
if ll_cont = 0 then
	fs_messaggio = "Nessun record inserito per il GRUPPO " + fs_cod_gruppo + " CLIENTE " + fs_cod_cliente + " VALUTA " + ls_cod_valuta
	return -1
end if

return 0
end function

public function integer fuo_ricerca_condizioni (string fs_cod_prodotto, string fs_cod_cliente, string fs_cod_valuta, string fs_cod_agente, datetime fdt_data_inizio_val, ref double fd_sconti[], ref double fd_provv_1, ref double fd_provv_2, ref string fs_messaggio);// --------------------------------------------------------------------------------------------------------
//		FUNZIONE DI RICERCA SCONTI OPER RICLASSIFICA PRODOTTI NELLA TABELLA GRUPPI SCONTO
//
//
//		ENTRATA: PRODOTTO
//					CLIENTE
//					AGENTE
//		USCITA	LD_SCONTI[]			SCALA SCONTI   1..10
//					LD_PROVV_1			PROVVIGIONE 1 
//					LD_PROVV_2			PROVVIGIONE 2
//		RETURN   -1 = ERRORE
//					0	= OK HO TROVATO DEGLI SCONTI
//					1  = ESECUZIONE OK, MA NON HO TROVATO NULLA PERCHE' IL PRODOTTO NON HA UNA RICLASSIFICA
//
//		REALIZZATO IL :    09/09/1999 DA ENRICO MENEGOTTO
//		ULTIMA MODIFICA :             DA                 
//
// ---------------------------------------------------------------------------------------------------------
string ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5, ls_null, &
       ls_cod_gruppo_sconto
long   ll_cont, ll_scelta,ll_i

setnull(ls_null)
ll_scelta = 0
ll_cont = 0

select cod_gruppo_sconto
into   :ls_cod_gruppo_sconto
from   anag_clienti
where  cod_azienda = :s_cs_xx_cod_azienda and
       cod_cliente = :fs_cod_cliente;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca gruppo di sconto nella anagrafica del cliente. Dettaglio errore " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_gruppo_sconto) then
	fs_messaggio = "Nella anagrafica del cliente non è specificato alcun gruppo di sconto"
	return -1
end if

select cod_livello_prod_1,   
		 cod_livello_prod_2,   
		 cod_livello_prod_3,   
		 cod_livello_prod_4,   
		 cod_livello_prod_5  
into   :ls_cod_livello_prod_1,   
		 :ls_cod_livello_prod_2,   
		 :ls_cod_livello_prod_3,   
		 :ls_cod_livello_prod_4,   
		 :ls_cod_livello_prod_5  
from   anag_prodotti  
where  cod_azienda = :s_cs_xx_cod_azienda and  
		 cod_prodotto = :fs_cod_prodotto;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca riclassifica del prodotto. Dettaglio errore " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_livello_prod_1) and isnull(ls_cod_livello_prod_2) and isnull(ls_cod_livello_prod_3) and isnull(ls_cod_livello_prod_4) and isnull(ls_cod_livello_prod_5) then return 1

// --------------  esiste un gruppo di sconto per il cliente ??? -------------------------------------------

//Donato 08/04/2010 ricercare l'esistenza di gruppo dis conto per il cliente su data inferiore o uguale a quella di riferimento
select count(*)
into   :ll_cont
from   gruppi_sconto
where  cod_azienda = :s_cs_xx_cod_azienda and
       cod_gruppo_sconto = :ls_cod_gruppo_sconto and
       cod_cliente = :fs_cod_cliente and data_inizio_val<=:fdt_data_inizio_val;
/*
select count(*)
into   :ll_cont
from   gruppi_sconto
where  cod_azienda = :s_cs_xx_cod_azienda and
       cod_gruppo_sconto = :ls_cod_gruppo_sconto and
       cod_cliente = :fs_cod_cliente;
*/
//fine modifica -----------------------------------------------------------------------------
		 
if ll_cont = 0 or isnull(ll_cont) then		// non esiste un gruppo di sconto personalizzato per il cliente
	fuo_ricerca_livello(ls_cod_gruppo_sconto, &
	                   ls_cod_livello_prod_1, &
							 ls_cod_livello_prod_2, &
							 ls_cod_livello_prod_3, &
							 ls_cod_livello_prod_4, &
							 ls_cod_livello_prod_5, &
							 fs_cod_prodotto, &
							 ls_null, &
							 fs_cod_valuta, &
							 fs_cod_agente, &
							 fdt_data_inizio_val, &
							 fd_sconti[], &
							 fd_provv_1, &
							 fd_provv_2, &
							 fs_messaggio)
else  ///////////  linea di sconto per cliente
	fuo_ricerca_livello(ls_cod_gruppo_sconto, &
	                   ls_cod_livello_prod_1, &
							 ls_cod_livello_prod_2, &
							 ls_cod_livello_prod_3, &
							 ls_cod_livello_prod_4, &
							 ls_cod_livello_prod_5, &
							 fs_cod_prodotto, &
							 fs_cod_cliente, &
							 fs_cod_valuta, &
							 fs_cod_agente, &
							 fdt_data_inizio_val, &
							 fd_sconti[], &
							 fd_provv_1, &
							 fd_provv_2, &
							 fs_messaggio)
end if	
return 0

end function

public function integer fuo_ricerca_livello (string fs_cod_gruppo_sconto, string fs_livello_prod_1, string fs_livello_prod_2, string fs_livello_prod_3, string fs_livello_prod_4, string fs_livello_prod_5, string fs_cod_prodotto, string fs_cod_cliente, string fs_cod_valuta, string fs_cod_agente, datetime fdt_data_inizio_val, ref double fd_sconti[], ref double fd_provv_1, ref double fd_provv_2, ref string fs_messaggio);// --------------------------------------------------------------------------------------------------------
//		FUNZIONE DI RICERCA SCONTI OPER RICLASSIFICA PRODOTTI NELLA TABELLA GRUPPI SCONTO
//
//
//		ENTRATA: PRODOTTO
//					CLIENTE
//					AGENTE
//		USCITA	LD_SCONTI[]			SCALA SCONTI   1..10
//					LD_PROVV_1			PROVVIGIONE 1 
//					LD_PROVV_2			PROVVIGIONE 2
//		RETURN   -1 = ERRORE
//					0	= OK HO TROVATO DEGLI SCONTI
//					1  = ESECUZIONE OK, MA NON HO TROVATO NULLA PERCHE' IL PRODOTTO NON HA UNA RICLASSIFICA
//
//		REALIZZATO IL :    09/09/1999 DA ENRICO MENEGOTTO
//		ULTIMA MODIFICA :             DA                 
//
// ---------------------------------------------------------------------------------------------------------
boolean lb_trovato
long   ll_cont, ll_scelta,ll_i
string ls_sql
datastore lds_data
uo_functions luo_functions 

luo_functions = create uo_functions

lb_trovato = false
ll_scelta = 0
ll_cont = 0

if isnull(fs_livello_prod_1) and isnull(fs_livello_prod_2) and isnull(fs_livello_prod_3) and isnull(fs_livello_prod_4) and isnull(fs_livello_prod_5) then return 1

if not isnull(fs_livello_prod_5) and not isnull(fs_livello_prod_4) and not isnull(fs_livello_prod_3) and not isnull(fs_livello_prod_2) and not isnull(fs_livello_prod_1) then
	
	ls_sql = "select sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2 "+&
				"from gruppi_sconto "+&
				"where  cod_azienda='"+s_cs_xx_cod_azienda+"' and cod_agente is null and 	cod_valuta='"+fs_cod_valuta+"' and "+&
					"cod_livello_prod_1='"+fs_livello_prod_1+"' and cod_livello_prod_2='"+fs_livello_prod_2+"' and cod_livello_prod_3='"+fs_livello_prod_3+"' and "+&
					"cod_livello_prod_4='"+fs_livello_prod_4+"' and cod_livello_prod_5='"+fs_livello_prod_5+"'  and cod_gruppo_sconto='"+fs_cod_gruppo_sconto+"' and "+&
					"data_inizio_val<='"+string(fdt_data_inizio_val,s_cs_xx_db_funzioni_formato_data)+"' "
						
	if not isnull(fs_cod_cliente) and fs_cod_cliente<> "" then
		ls_sql += " and cod cliente='"+fs_cod_cliente+"' "
	else
		ls_sql += " and cod cliente is null "
	end if
	
	ls_sql += " order by data_inizio_val desc"
	
	if luo_functions.uof_crea_datastore(lds_data, ls_sql) < 0 then
		fs_messaggio = "Errore in creazione datastore"
		return -1
	end if
	
	lds_data.retrieve()
	if lds_data.rowcount()>0 then
		fd_sconti[1]=lds_data.getitemnumber(1, 1)
		fd_sconti[2]=lds_data.getitemnumber(1, 2)
		fd_sconti[3]=lds_data.getitemnumber(1, 3)
		fd_sconti[4]=lds_data.getitemnumber(1, 4)
		fd_sconti[5]=lds_data.getitemnumber(1, 5)
		fd_sconti[6]=lds_data.getitemnumber(1, 6)
		fd_sconti[7]=lds_data.getitemnumber(1, 7)
		fd_sconti[8]=lds_data.getitemnumber(1, 8)
		fd_sconti[9]=lds_data.getitemnumber(1, 9)
		fd_sconti[10]=lds_data.getitemnumber(1, 10)
		fd_provv_1=lds_data.getitemnumber(1, 11)
		fd_provv_2=lds_data.getitemnumber(1, 12)
		
		return 0
	else
		setnull(fs_livello_prod_5)
	end if
	
	/*
	select sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2
	into   :fd_sconti[1], :fd_sconti[2], :fd_sconti[3], :fd_sconti[4], :fd_sconti[5], :fd_sconti[6], :fd_sconti[7], :fd_sconti[8], :fd_sconti[9], :fd_sconti[10], :fd_provv_1, :fd_provv_2
	from gruppi_sconto
	where  cod_azienda = :s_cs_xx_cod_azienda and
			cod_agente is null and
			cod_valuta = :fs_cod_valuta and
			cod_livello_prod_1 = :fs_livello_prod_1 and
			cod_livello_prod_2 = :fs_livello_prod_2 and
			cod_livello_prod_3 = :fs_livello_prod_3 and
			cod_livello_prod_4 = :fs_livello_prod_4 and
			cod_livello_prod_5 = :fs_livello_prod_5 and
			cod_cliente        = :fs_cod_cliente and
			cod_gruppo_sconto  = :fs_cod_gruppo_sconto
	having cod_livello_prod_1 = :fs_livello_prod_1 and
			cod_livello_prod_2 = :fs_livello_prod_2 and
			cod_livello_prod_3 = :fs_livello_prod_3 and
			cod_livello_prod_4 = :fs_livello_prod_4 and
			cod_livello_prod_5 = :fs_livello_prod_5 and
			cod_cliente        = :fs_cod_cliente and
         cod_agente         is null and
			cod_gruppo_sconto  = :fs_cod_gruppo_sconto and
			cod_valuta = :fs_cod_valuta and
			data_inizio_val = max(data_inizio_val) and 
			data_inizio_val <= :fdt_data_inizio_val;
	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore in ricerca linee di sconto (5).   Codice errore: " + string(sqlca.sqlcode) + "    Dettaglio errore:" + sqlca.sqlerrtext
		return -1
	end if
	if sqlca.sqlcode = 0 then return 0
	setnull(fs_livello_prod_5)
	*/
end if	
	
if isnull(fs_livello_prod_5) and not isnull(fs_livello_prod_4) and not isnull(fs_livello_prod_3) and not isnull(fs_livello_prod_2) and not isnull(fs_livello_prod_1) then
	ls_sql="select sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2 "+&
				"from gruppi_sconto "+&
				"where  cod_azienda = '"+s_cs_xx_cod_azienda+"' and cod_agente is null and cod_valuta = '"+fs_cod_valuta+"' and "+&
					"cod_livello_prod_1 = '"+fs_livello_prod_1+"' and cod_livello_prod_2 = '"+fs_livello_prod_2+"' and "+&
					"cod_livello_prod_3 = '"+fs_livello_prod_3+"' and cod_livello_prod_4 = '"+fs_livello_prod_4+"' and "+&
					"cod_livello_prod_5 is null and cod_gruppo_sconto  = '"+fs_cod_gruppo_sconto+"' and "+&
					"data_inizio_val<='"+string(fdt_data_inizio_val,s_cs_xx_db_funzioni_formato_data)+"' "
					
	if not isnull(fs_cod_cliente) and fs_cod_cliente<>"" then
		ls_sql += "and cod_cliente='"+fs_cod_cliente+"' "
	else
		ls_sql += "and cod_cliente is null "
	end if
	ls_sql += " order by data_inizio_val desc"
	
	if luo_functions.uof_crea_datastore(lds_data, ls_sql) <0 then
		fs_messaggio = "Errore in creazione datastore"
		return -1
	end if
	
	lds_data.retrieve()
	if lds_data.rowcount()>0 then
		fd_sconti[1]=lds_data.getitemnumber(1, 1)
		fd_sconti[2]=lds_data.getitemnumber(1, 2)
		fd_sconti[3]=lds_data.getitemnumber(1, 3)
		fd_sconti[4]=lds_data.getitemnumber(1, 4)
		fd_sconti[5]=lds_data.getitemnumber(1, 5)
		fd_sconti[6]=lds_data.getitemnumber(1, 6)
		fd_sconti[7]=lds_data.getitemnumber(1, 7)
		fd_sconti[8]=lds_data.getitemnumber(1, 8)
		fd_sconti[9]=lds_data.getitemnumber(1, 9)
		fd_sconti[10]=lds_data.getitemnumber(1, 10)
		fd_provv_1=lds_data.getitemnumber(1, 11)
		fd_provv_2=lds_data.getitemnumber(1, 12)
		
		return 0
	else
		setnull(fs_livello_prod_4)
	end if
	/*
	select sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2
	into   :fd_sconti[1], :fd_sconti[2], :fd_sconti[3], :fd_sconti[4], :fd_sconti[5], :fd_sconti[6], :fd_sconti[7], :fd_sconti[8], :fd_sconti[9], :fd_sconti[10], :fd_provv_1, :fd_provv_2
	from gruppi_sconto
	where  cod_azienda = :s_cs_xx_cod_azienda and
			cod_agente is null and
			cod_valuta = :fs_cod_valuta and
			cod_livello_prod_1 = :fs_livello_prod_1 and
			cod_livello_prod_2 = :fs_livello_prod_2 and
			cod_livello_prod_3 = :fs_livello_prod_3 and
			cod_livello_prod_4 = :fs_livello_prod_4 and
			cod_livello_prod_5 is null and
			cod_cliente        = :fs_cod_cliente and
			cod_gruppo_sconto  = :fs_cod_gruppo_sconto
	having cod_livello_prod_1 = :fs_livello_prod_1 and
			cod_livello_prod_2 = :fs_livello_prod_2 and
			cod_livello_prod_3 = :fs_livello_prod_3 and
			cod_livello_prod_4 = :fs_livello_prod_4 and
			cod_livello_prod_5 is null and
			cod_valuta = :fs_cod_valuta and
			cod_cliente        = :fs_cod_cliente and
         cod_agente         is null and
			cod_gruppo_sconto  = :fs_cod_gruppo_sconto and
			data_inizio_val = max(data_inizio_val) and 
			data_inizio_val <= :fdt_data_inizio_val;
	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore in ricerca linee di sconto (4).   Codice errore: " + string(sqlca.sqlcode) + "    Dettaglio errore:" + sqlca.sqlerrtext
		return -1
	end if
	if sqlca.sqlcode = 0 then return 0
	setnull(fs_livello_prod_4)
	*/
end if

if isnull(fs_livello_prod_5) and isnull(fs_livello_prod_4) and not isnull(fs_livello_prod_3) and not isnull(fs_livello_prod_2) and not isnull(fs_livello_prod_1) then
	
	ls_sql = "select sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2 "+&
				"from gruppi_sconto "+&
				"where  cod_azienda='"+s_cs_xx_cod_azienda+"' and cod_agente is null and 	cod_valuta='"+fs_cod_valuta+"' and "+&
					"cod_livello_prod_1 = '"+fs_livello_prod_1+"' and cod_livello_prod_2 = '"+fs_livello_prod_2+"' and "+&
					"cod_livello_prod_3 = '"+fs_livello_prod_3+"' and cod_livello_prod_4 is null and cod_livello_prod_5 is null and "+&
					"cod_gruppo_sconto='"+fs_cod_gruppo_sconto+"' and "+&
					"data_inizio_val<='"+string(fdt_data_inizio_val,s_cs_xx_db_funzioni_formato_data)+"' "
	
	if not isnull(fs_cod_cliente) and fs_cod_cliente<>"" then
		ls_sql += "and cod_cliente='"+fs_cod_cliente+"' "
	else
		ls_sql += "and cod_cliente is null "
	end if
	ls_sql += " order by data_inizio_val desc"
	
	if luo_functions.uof_crea_datastore(lds_data, ls_sql) < 0 then
		fs_messaggio = "Errore in creazione datastore"
		return -1
	end if
	
	lds_data.retrieve()
	if lds_data.rowcount()>0 then
		fd_sconti[1]=lds_data.getitemnumber(1, 1)
		fd_sconti[2]=lds_data.getitemnumber(1, 2)
		fd_sconti[3]=lds_data.getitemnumber(1, 3)
		fd_sconti[4]=lds_data.getitemnumber(1, 4)
		fd_sconti[5]=lds_data.getitemnumber(1, 5)
		fd_sconti[6]=lds_data.getitemnumber(1, 6)
		fd_sconti[7]=lds_data.getitemnumber(1, 7)
		fd_sconti[8]=lds_data.getitemnumber(1, 8)
		fd_sconti[9]=lds_data.getitemnumber(1, 9)
		fd_sconti[10]=lds_data.getitemnumber(1, 10)
		fd_provv_1=lds_data.getitemnumber(1, 11)
		fd_provv_2=lds_data.getitemnumber(1, 12)
		
		return 0
	else
		setnull(fs_livello_prod_3)
	end if
	
	/*
	select sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2
	into   :fd_sconti[1], :fd_sconti[2], :fd_sconti[3], :fd_sconti[4], :fd_sconti[5], :fd_sconti[6], :fd_sconti[7], :fd_sconti[8], :fd_sconti[9], :fd_sconti[10], :fd_provv_1, :fd_provv_2
	from gruppi_sconto
	where  cod_azienda = :s_cs_xx_cod_azienda and
			cod_agente is null and
			cod_valuta = :fs_cod_valuta and
			cod_livello_prod_1 = :fs_livello_prod_1 and
			cod_livello_prod_2 = :fs_livello_prod_2 and
			cod_livello_prod_3 = :fs_livello_prod_3 and
			cod_livello_prod_4 is null and
			cod_livello_prod_5 is null and
			cod_cliente        = :fs_cod_cliente and
			cod_gruppo_sconto  = :fs_cod_gruppo_sconto
	having cod_livello_prod_1 = :fs_livello_prod_1 and
			cod_livello_prod_2 = :fs_livello_prod_2 and
			cod_livello_prod_3 = :fs_livello_prod_3 and
			cod_livello_prod_4 is null and
			cod_livello_prod_5 is null and
			cod_cliente         = :fs_cod_cliente and
         cod_agente         is null and
			cod_gruppo_sconto  = :fs_cod_gruppo_sconto and
			cod_valuta = :fs_cod_valuta and
			data_inizio_val = max(data_inizio_val) and 
			data_inizio_val <= :fdt_data_inizio_val;
	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore in ricerca linee di sconto (3).   Codice errore: " + string(sqlca.sqlcode) + "    Dettaglio errore:" + sqlca.sqlerrtext
		return -1
	end if
	if sqlca.sqlcode = 0 then return 0
	setnull(fs_livello_prod_3)
	*/
end if

if isnull(fs_livello_prod_5) and isnull(fs_livello_prod_4) and isnull(fs_livello_prod_3) and not isnull(fs_livello_prod_2) and not isnull(fs_livello_prod_1) then
	
	ls_sql = "select sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2 "+&
				"from gruppi_sconto "+&
				"where  cod_azienda='"+s_cs_xx_cod_azienda+"' and cod_agente is null and 	cod_valuta='"+fs_cod_valuta+"' and "+&
					"cod_livello_prod_1 = '"+fs_livello_prod_1+"' and cod_livello_prod_2 = '"+fs_livello_prod_2+"' and "+&
					"cod_livello_prod_3 is null and cod_livello_prod_4 is null and cod_livello_prod_5 is null and "+&
					"cod_gruppo_sconto='"+fs_cod_gruppo_sconto+"' and "+&
					"data_inizio_val<='"+string(fdt_data_inizio_val,s_cs_xx_db_funzioni_formato_data)+"' "
	
	if not isnull(fs_cod_cliente) and fs_cod_cliente<>"" then
		ls_sql += "and cod_cliente='"+fs_cod_cliente+"' "
	else
		ls_sql += "and cod_cliente is null "
	end if
	ls_sql += " order by data_inizio_val desc"
	
	if luo_functions.uof_crea_datastore(lds_data, ls_sql) < 0 then
		fs_messaggio = "Errore in creazione datastore"
		return -1
	end if
	
	lds_data.retrieve()
	if lds_data.rowcount()>0 then
		fd_sconti[1]=lds_data.getitemnumber(1, 1)
		fd_sconti[2]=lds_data.getitemnumber(1, 2)
		fd_sconti[3]=lds_data.getitemnumber(1, 3)
		fd_sconti[4]=lds_data.getitemnumber(1, 4)
		fd_sconti[5]=lds_data.getitemnumber(1, 5)
		fd_sconti[6]=lds_data.getitemnumber(1, 6)
		fd_sconti[7]=lds_data.getitemnumber(1, 7)
		fd_sconti[8]=lds_data.getitemnumber(1, 8)
		fd_sconti[9]=lds_data.getitemnumber(1, 9)
		fd_sconti[10]=lds_data.getitemnumber(1, 10)
		fd_provv_1=lds_data.getitemnumber(1, 11)
		fd_provv_2=lds_data.getitemnumber(1, 12)
		
		return 0
	else
		setnull(fs_livello_prod_2)
	end if
	
	/*
	select sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2
	into   :fd_sconti[1], :fd_sconti[2], :fd_sconti[3], :fd_sconti[4], :fd_sconti[5], :fd_sconti[6], :fd_sconti[7], :fd_sconti[8], :fd_sconti[9], :fd_sconti[10], :fd_provv_1, :fd_provv_2
	from gruppi_sconto
	where  cod_azienda = :s_cs_xx_cod_azienda and
			cod_agente is null and
			cod_valuta = :fs_cod_valuta and
			cod_livello_prod_1 = :fs_livello_prod_1 and
			cod_livello_prod_2 = :fs_livello_prod_2 and
			cod_livello_prod_3 is null and
			cod_livello_prod_4 is null and
			cod_livello_prod_5 is null and
			cod_cliente         = :fs_cod_cliente and
			cod_gruppo_sconto  = :fs_cod_gruppo_sconto
	having cod_livello_prod_1 = :fs_livello_prod_1 and
			cod_livello_prod_2 = :fs_livello_prod_2 and
			cod_livello_prod_3 is null and
			cod_livello_prod_4 is null and
			cod_livello_prod_5 is null and
			cod_cliente        = :fs_cod_cliente and
         cod_agente         is null and
			cod_gruppo_sconto  = :fs_cod_gruppo_sconto and
			cod_valuta = :fs_cod_valuta and
			data_inizio_val = max(data_inizio_val) and 
			data_inizio_val <= :fdt_data_inizio_val;
	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore in ricerca linee di sconto (2).   Codice errore: " + string(sqlca.sqlcode) + "    Dettaglio errore:" + sqlca.sqlerrtext
		return -1
	end if
	if sqlca.sqlcode = 0 then return 0
	setnull(fs_livello_prod_2)
	*/
end if

if  isnull(fs_livello_prod_5) and isnull(fs_livello_prod_4) and isnull(fs_livello_prod_3) and isnull(fs_livello_prod_2) and not isnull(fs_livello_prod_1) then
	
	ls_sql = "select sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2 "+&
				"from gruppi_sconto "+&
				"where  cod_azienda='"+s_cs_xx_cod_azienda+"' and cod_agente is null and 	cod_valuta='"+fs_cod_valuta+"' and "+&
					"cod_livello_prod_1='"+fs_livello_prod_1+"' and cod_livello_prod_2 is null and cod_livello_prod_3 is null and "+&
					"cod_livello_prod_4 is null and cod_livello_prod_5 is null and cod_gruppo_sconto='"+fs_cod_gruppo_sconto+"' and "+&
					"data_inizio_val<='"+string(fdt_data_inizio_val,s_cs_xx_db_funzioni_formato_data)+"' "

	if not isnull(fs_cod_cliente) and fs_cod_cliente<>"" then
		ls_sql += " and cod_cliente='"+fs_cod_cliente+"' "
	else
		ls_sql += " and cod_cliente is null "
	end if
	
	ls_sql += " order by data_inizio_val desc"
	
	if luo_functions.uof_crea_datastore(lds_data, ls_sql) < 0 then
		fs_messaggio = "Errore in creazione datastore"
		return -1
	end if
	
	lds_data.retrieve()
	if lds_data.rowcount()>0 then
		fd_sconti[1]=lds_data.getitemnumber(1, 1)
		fd_sconti[2]=lds_data.getitemnumber(1, 2)
		fd_sconti[3]=lds_data.getitemnumber(1, 3)
		fd_sconti[4]=lds_data.getitemnumber(1, 4)
		fd_sconti[5]=lds_data.getitemnumber(1, 5)
		fd_sconti[6]=lds_data.getitemnumber(1, 6)
		fd_sconti[7]=lds_data.getitemnumber(1, 7)
		fd_sconti[8]=lds_data.getitemnumber(1, 8)
		fd_sconti[9]=lds_data.getitemnumber(1, 9)
		fd_sconti[10]=lds_data.getitemnumber(1, 10)
		fd_provv_1=lds_data.getitemnumber(1, 11)
		fd_provv_2=lds_data.getitemnumber(1, 12)
		
		return 0
	else
		setnull(fs_livello_prod_1)
	end if
	
	/*
	select sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2
	into   :fd_sconti[1], :fd_sconti[2], :fd_sconti[3], :fd_sconti[4], :fd_sconti[5], :fd_sconti[6], :fd_sconti[7], :fd_sconti[8], :fd_sconti[9], :fd_sconti[10], :fd_provv_1, :fd_provv_2
	from gruppi_sconto
	where  cod_azienda = :s_cs_xx_cod_azienda and
			cod_agente is null and
			cod_valuta = :fs_cod_valuta and
			cod_livello_prod_1 = :fs_livello_prod_1 and
			cod_livello_prod_2 is null and
			cod_livello_prod_3 is null and
			cod_livello_prod_4 is null and
			cod_livello_prod_5 is null and
			cod_cliente        = :fs_cod_cliente and
			cod_gruppo_sconto  = :fs_cod_gruppo_sconto
	having cod_livello_prod_1 = :fs_livello_prod_1 and
			cod_livello_prod_2 is null and
			cod_livello_prod_3 is null and
			cod_livello_prod_4 is null and
			cod_livello_prod_5 is null and
			cod_cliente        = :fs_cod_cliente and
         cod_agente         is null and
			cod_gruppo_sconto  = :fs_cod_gruppo_sconto and
			cod_valuta = :fs_cod_valuta and
			data_inizio_val = max(data_inizio_val) and 
			data_inizio_val <= :fdt_data_inizio_val;
	
	
	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore in ricerca linee di sconto (1).   Codice errore: " + string(sqlca.sqlcode) + "    Dettaglio errore:" + sqlca.sqlerrtext
		return -1
	end if
	if sqlca.sqlcode = 0 then return 0
	setnull(fs_livello_prod_1)
	*/
end if

for ll_i = 1 to 10
	fd_sconti[ll_i ] = 0
next
return 0


end function

public function integer fuo_duplica_gruppo_cliente (string fs_cod_cliente_origine, string fs_cod_cliente_destinazione, datetime fdt_data_inizio_val, ref string fs_messaggio);string ls_cod_valuta, ls_sql, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5,&
       ls_cod_gruppo,ls_flag_sconto_particolare
long ll_progressivo, ll_cont, ll_max
datetime ldt_data_inizio_val
double ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_provvigione_1, ld_provvigione_2

if not isnull(fs_cod_cliente_destinazione) then
	delete gruppi_sconto
	where  cod_azienda = :s_cs_xx_cod_azienda and
			 cod_cliente = :fs_cod_cliente_destinazione;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante cancellazione precedenti linee di sconto. ~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
end if

select cod_valuta
into   :ls_cod_valuta
from   anag_clienti
where  cod_azienda = :s_cs_xx_cod_azienda and
       cod_cliente = :fs_cod_cliente_destinazione;
if sqlca.sqlcode = 100 then
	fs_messaggio = "Cliente codice " + fs_cod_cliente_destinazione + " non trovato !"
	return -1
elseif sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca cliente" + fs_cod_cliente_destinazione + "~r~n Dettaglio Errore: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_valuta) then
	fs_messaggio = "Il cliente " + fs_cod_cliente_destinazione + " non ha l'indicazione della valuta nella sua anagrafica "
	return -1
end if

declare cu_gruppi dynamic cursor for sqlsa;
ls_sql = " select cod_gruppo_sconto, data_inizio_val, cod_livello_prod_1, cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4, cod_livello_prod_5, sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2, flag_sconto_particolare  " + &
			" from gruppi_sconto " + &
			" where  cod_azienda = '" + s_cs_xx_cod_azienda + "' and cod_valuta = '" + ls_cod_valuta + "' and cod_cliente = '" + fs_cod_cliente_origine + "' and cod_cliente is not null and cod_agente is null " + &
			" group by cod_gruppo_sconto, data_inizio_val, cod_livello_prod_1, cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4, cod_livello_prod_5, sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2, flag_sconto_particolare " + &
			" having data_inizio_val = max(data_inizio_val) "
prepare sqlsa from :ls_sql;
open dynamic cu_gruppi;

ll_cont = 0

do while true
   fetch cu_gruppi into :ls_cod_gruppo, :ldt_data_inizio_val, :ls_cod_livello_prod_1, :ls_cod_livello_prod_2, :ls_cod_livello_prod_3, :ls_cod_livello_prod_4, :ls_cod_livello_prod_5, :ld_sconto_1, :ld_sconto_2, :ld_sconto_3, :ld_sconto_4, :ld_sconto_5, :ld_sconto_6, :ld_sconto_7, :ld_sconto_8, :ld_sconto_9, :ld_sconto_10, :ld_provvigione_1, :ld_provvigione_2, :ls_flag_sconto_particolare;
	
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_cont ++
	ll_max = 0
	
	select max(progressivo)
	into   :ll_max
	from   gruppi_sconto
	where  cod_azienda = :s_cs_xx_cod_azienda and
			 cod_valuta = :ls_cod_valuta and
			 data_inizio_val = :fdt_data_inizio_val;
			 
	if ll_max = 0 or isnull(ll_max) then
		ll_max = 1
	else
		ll_max ++
	end if
	
	INSERT INTO gruppi_sconto  
			( cod_azienda,   
			  cod_gruppo_sconto,   
			  cod_valuta,   
			  data_inizio_val,   
			  progressivo,   
			  cod_agente,   
			  cod_cliente,   
			  cod_livello_prod_1,   
			  cod_livello_prod_2,   
			  cod_livello_prod_3,   
			  cod_livello_prod_4,   
			  cod_livello_prod_5,   
			  sconto_1,   
			  sconto_2,   
			  sconto_3,   
			  sconto_4,   
			  sconto_5,   
			  sconto_6,   
			  sconto_7,   
			  sconto_8,   
			  sconto_9,   
			  sconto_10,   
			  provvigione_1,   
			  provvigione_2 ,
			  flag_sconto_particolare)  
	VALUES ( :s_cs_xx_cod_azienda,   
			  :ls_cod_gruppo,   
			  :ls_cod_valuta,   
			  :fdt_data_inizio_val,   
			  :ll_max,   
			  null,   
			  :fs_cod_cliente_destinazione,   
			  :ls_cod_livello_prod_1,   
			  :ls_cod_livello_prod_2,   
			  :ls_cod_livello_prod_3,   
			  :ls_cod_livello_prod_4,   
			  :ls_cod_livello_prod_5,   
			  :ld_sconto_1,   
			  :ld_sconto_2,   
			  :ld_sconto_3,   
			  :ld_sconto_4,   
			  :ld_sconto_5,   
			  :ld_sconto_6,   
			  :ld_sconto_7,   
			  :ld_sconto_8,   
			  :ld_sconto_9,   
			  :ld_sconto_10,   
			  :ld_provvigione_1,   
			  :ld_provvigione_2,
			  :ls_flag_sconto_particolare)  ;
	 if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante inserimento gruppo sconto cliente " + fs_cod_cliente_destinazione + "~r~nDettaglio errore:" + sqlca.sqlerrtext
		close cu_gruppi;
		return -1
	end if
loop

close cu_gruppi;

if ll_cont = 0 then
	fs_messaggio = "Nessuna condizione inserita per il CLIENTE "+ fs_cod_cliente_destinazione + " VALUTA " + ls_cod_valuta
	return -1
end if

return 0
end function

on uo_gruppi_sconti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_gruppi_sconti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

