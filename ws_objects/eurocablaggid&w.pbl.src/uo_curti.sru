﻿$PBExportHeader$uo_curti.sru
forward
global type uo_curti from nonvisualobject
end type
end forward

global type uo_curti from nonvisualobject
end type
global uo_curti uo_curti

type variables
private:
	string is_cod_parametro_path_curti="CU1"
	string is_cod_parametro_path_backup="CU2"
	long	il_filenumber=0
end variables

forward prototypes
public function integer uof_get_path_ricetta (ref string as_path)
public function integer uof_leggi_ricetta (ref string as_message)
public function integer uof_scrivi_ricetta (ref string as_errore)
public function integer uof_open_file_ricette (ref string as_message)
public function integer uof_close_file_ricette ()
public function integer uof_copy_file_ricette (ref string as_message)
end prototypes

public function integer uof_get_path_ricetta (ref string as_path);string  ls_path

guo_functions.uof_get_parametro(is_cod_parametro_path_curti, as_path)
if as_path = "" or isnull(as_path) then return -1

if not fileexists(as_path) then return -1

return 0


end function

public function integer uof_leggi_ricetta (ref string as_message);// Lettura del file ricetta e sua scrittura nel Database per una ricerca più efficiente
string		ls_prodotto_finito[], ls_des_prodotto_finito[], ls_sequenza[], ls_str, ls_explode_vars[], ls_formato_var,ls_variabile_ricetta, ls_vuoto[], ls_path_file_ricetta, ls_decs, ls_des_prodotto_finito_410[]
long		ll_ret, ll_i, ll_decs, ll_sequenza, ll_num_riga_variabile, ll_rows, ll_id, ll_num_decs
dec{4}	ld_valore_variabile
uo_log	luo_log

luo_log = create uo_log
// leggo riga 1 (non mi serve)
ll_ret = filereadEx(il_filenumber, ls_str)
if ll_ret < 0 then
	as_message = "Errore in lettura file ricette"
	return -1
end if
// leggo riga 2 (non mi serve)
ll_ret = filereadEx(il_filenumber, ls_str)
if ll_ret < 0 then
	as_message = "Errore in lettura file ricette"
	return -1
end if

// raso la tabella
delete from curti_ricette;
if sqlca.sqlcode < 0 then			
//	uof_close_file_ricette()
	as_message=g_str.format("Errore in insert variabile ricetta. $1", sqlca.sqlerrtext)
	luo_log.error( as_message )
	rollback;
	return -1
else
	select max(id_curti_ricette) 
	into	:ll_id
	from curti_ricette;
	
	if ll_id > 100000 then
		execute immediate "CALL sa_reset_identity( 'curti_ricette', 'dba', 0 )" using sqlca;
		if sqlca.sqlcode < 0 then			
//			uof_close_file_ricette()
			as_message=g_str.format("Errore in reseed ID tabella curti_ricette. $1", sqlca.sqlerrtext)
			luo_log.error( as_message )
			rollback;
			return -1
		end if	
	end if
end if	



// da qui in poi leggo tutte le variabili fino alla prima riga libera o fino alla fine del file)
ll_num_riga_variabile = 2
do while true

	ll_ret = filereadEx(il_filenumber, ls_str)
	// raggiunta la fiune del file
	if ll_ret = -100 then exit
	
	// riga vuota
	if ll_ret = 0 then exit
	
	ls_formato_var = ""
	ll_num_riga_variabile ++
	
	choose case  ll_num_riga_variabile
		case 3
			// Langid_410
			ls_formato_var = ""
			g_str.explode( ls_str,";", ls_des_prodotto_finito[] )
			ls_variabile_ricetta = ls_des_prodotto_finito[1]
			ls_prodotto_finito[] = ls_vuoto[]
			ls_prodotto_finito[ upperbound(ls_des_prodotto_finito) ] =""
			ll_rows = upperbound(ls_des_prodotto_finito)
			ls_des_prodotto_finito_410 = ls_des_prodotto_finito
		case 4
			//langid_809
			ls_formato_var = ""
			g_str.explode( ls_str,";", ls_prodotto_finito[] )
			ls_variabile_ricetta = ls_prodotto_finito[1]
			ls_des_prodotto_finito[ upperbound(ls_prodotto_finito) ] =""
			ll_rows = upperbound(ls_prodotto_finito)
		case 5
			ls_formato_var = "#####0"
			g_str.explode( ls_str,";",  ls_sequenza[] )
			ls_explode_vars = ls_sequenza
			ls_variabile_ricetta = "1"
			ll_rows = upperbound(ls_sequenza)
		case else
			g_str.explode( ls_str,";",ls_explode_vars[])
			ls_variabile_ricetta = ls_explode_vars[1]
			ll_rows = upperbound(ls_explode_vars)
	end choose
			
	for ll_i = 2 to ll_rows
		
		if ll_num_riga_variabile = 3 then exit
		
		if ll_num_riga_variabile >= 5 then
				if pos(ls_explode_vars[ll_i],",") > 0 then
					ls_decs = mid(ls_explode_vars[ll_i], pos(ls_explode_vars[ll_i],",") + 1)
					ll_num_decs = len(ls_decs)
					ls_formato_var = "#####0.0" + fill("0",ll_num_decs - 1)
				else
					ls_formato_var = "#####0"
			end if		
			if upperbound(ls_sequenza) < ll_i then
				g_mb.error(g_str.format("ERRORE SEQUENZA UPPERBOUND=$1 LL_I=$2",upperbound(ls_sequenza),ll_i))
			end if
			ll_sequenza = long(ls_sequenza[ll_i])
			ld_valore_variabile = dec(ls_explode_vars[ll_i])
		else
			ll_sequenza = ll_i - 1
		end if
		
		if ll_num_riga_variabile = 4 then
			insert into curti_ricette
				(cod_prodotto_finito,
				cod_variabile_ricetta,
				valore_variabile,
				formato,
				sequenza,
				des_prodotto_finito,
				num_riga_variabile)
			values
				(:ls_prodotto_finito[ll_i],
				'LANGID_410',
				:ld_valore_variabile,
				:ls_formato_var,
				:ll_sequenza -1,
				:ls_des_prodotto_finito_410[ll_i],
				:ll_num_riga_variabile -1) ;
			if sqlca.sqlcode < 0 then				
				as_message=g_str.format("Errore in insert variabile ricetta. $1", sqlca.sqlerrtext)
				luo_log.error( as_message)
				rollback;
				return -1
			end if	
		end if		
		
		insert into curti_ricette
			(cod_prodotto_finito,
			cod_variabile_ricetta,
			valore_variabile,
			formato,
			sequenza,
			des_prodotto_finito,
			num_riga_variabile)
		values
			(:ls_prodotto_finito[ll_i],
			:ls_variabile_ricetta,
			:ld_valore_variabile,
			:ls_formato_var,
			:ll_sequenza,
			null,
			:ll_num_riga_variabile) ;
		if sqlca.sqlcode < 0 then				
			as_message=g_str.format("Errore in insert variabile ricetta. $1", sqlca.sqlerrtext)
			luo_log.error( as_message)
			rollback;
			return -1
		end if	
	next	
	
loop

//uof_close_file_ricette()

commit;

return 0
end function

public function integer uof_scrivi_ricetta (ref string as_errore);string	ls_sql, ls_errore, ls_str1, ls_cod_variabile_prec, ls_str, ls_path_file_ricetta
long ll_ret, ll_max_sequenza, ll_rows, ll_i, ll_num_var_prec, ll_file
uo_log	luo_log
datastore lds_data

if uof_copy_file_ricette( as_errore ) < 0 then
	return -1
end if

luo_log = create uo_log
 
ll_ret = uof_get_path_ricetta( ls_path_file_ricetta)
if ll_ret < 0 then
	if len(ls_path_file_ricetta) > 0 then
		as_errore = g_str.format("Il file ricetta $1 non esiste", ls_path_file_ricetta)
		luo_log.error( as_errore )
		return -1
	else
		as_errore = g_str.format("File ricetta non caricato; verificare il parametro multiaziendale CU1", ls_path_file_ricetta)
		luo_log.error( as_errore )
		return -1
	end if
end if

ll_file = fileopen(ls_path_file_ricetta, linemode!, write!, LockReadWrite!, Replace!)
if ll_file = -1 then
	as_errore = g_str.format("Errore in apertura file $1 in sola lettura", ls_path_file_ricetta)
	luo_log.error( as_errore )
	return -1
end if

select max(sequenza)
into	:ll_max_sequenza
from 	curti_ricette;


ls_str = "List separator=;Decimal symbol=," + fill(";",ll_max_sequenza -1)
ll_ret = filewriteex(il_filenumber,ls_str)
if ll_ret < 0 then
	as_errore = "Errore in scrittura file ricette"
	return -1
end if

ls_str = "Recipe Ricetta_1 " + fill(";",ll_max_sequenza)
ll_ret = filewriteex(il_filenumber,ls_str)
if ll_ret < 0 then
	as_errore = "Errore in scrittura file ricette"
	return -1
end if

ls_sql = "select des_prodotto_finito, cod_prodotto_finito, sequenza, cod_variabile_ricetta, valore_variabile, formato, num_riga_variabile from curti_ricette order by num_riga_variabile, sequenza"
ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
if ll_rows < 0 then
	as_errore = g_str.format("Errore in caricamento datastore ricette. $1", ls_errore)
	luo_log.error( as_errore )
	return -1
end if
if ll_rows = 0 then
	luo_log.warn( "Tabella curti_ricetta vuota")
	return -1
end if

ls_cod_variabile_prec = ""
ls_str1 = ""
ll_num_var_prec = 0
for ll_i = 1 to ll_rows
	if ll_num_var_prec <> lds_data.getitemnumber(ll_i,7) then
		ll_num_var_prec= lds_data.getitemnumber(ll_i,7)
	end if
	if ls_cod_variabile_prec <> lds_data.getitemstring(ll_i,4) then
		 // scrivo nel file al cambio variabile
		if len(ls_str1) > 0 then
			// non prendo l'ultimo carattere ";"
			if right(ls_str1,1) = ";" then ls_str1=left(ls_str1, len(ls_str1) - 1)
			ll_ret = filewriteex(il_filenumber,ls_str1)
		end if
		// imposto nuova riga variabile
		ls_str1 = lds_data.getitemstring(ll_i,4) + ";"
		ls_cod_variabile_prec = lds_data.getitemstring(ll_i,4)
	end if
	// aggiungo valori in riga variabile
	choose case lds_data.getitemnumber(ll_i,7) // num_riga_variabile
		case 3
			ls_str1 += lds_data.getitemstring(ll_i,1) + ";"
		case 4
			ls_str1 += lds_data.getitemstring(ll_i,2) + ";"
		case else
			ls_str1 += string( lds_data.getitemnumber(ll_i,5), lds_data.getitemstring(ll_i,6) ) + ";"
	end choose
next
// scrivo ultima riga
if right(ls_str1,1) = ";" then ls_str1=left(ls_str1, len(ls_str1) - 1)
ll_ret = filewriteex(il_filenumber,ls_str1)


fileclose(ll_file)

destroy lds_data
return 0


end function

public function integer uof_open_file_ricette (ref string as_message);string		ls_path_file_ricetta
long 		ll_ret
uo_log	luo_log

luo_log = create uo_log

ll_ret = uof_get_path_ricetta( ls_path_file_ricetta)
if ll_ret < 0 then
	if len(ls_path_file_ricetta) > 0 then
		as_message = g_str.format("Il file ricetta $1 non esiste", ls_path_file_ricetta)
		luo_log.error( as_message )
		destroy luo_log
		return -1
	else
		as_message = g_str.format("File ricetta non caricato; verificare il parametro multiaziendale CU1", ls_path_file_ricetta)
		luo_log.error( as_message )
		destroy luo_log
		return -1
	end if
end if

il_filenumber = fileopen(ls_path_file_ricetta, linemode!, read!, lockwrite!)
if il_filenumber = -1 then
	as_message = g_str.format("Errore in apertura file $1 in sola lettura", ls_path_file_ricetta)
	luo_log.error( as_message )
	destroy luo_log
	return -1
end if

destroy luo_log


return 0
end function

public function integer uof_close_file_ricette ();long ll_ret

ll_ret = fileclose(il_filenumber)

return ll_ret
end function

public function integer uof_copy_file_ricette (ref string as_message);string		ls_path_file_ricetta, ls_target_file, ls_path_target
long 		ll_ret
uo_log	luo_log

luo_log = create uo_log

ll_ret = uof_get_path_ricetta( ls_path_file_ricetta)
if ll_ret < 0 then
	if len(ls_path_file_ricetta) > 0 then
		as_message = g_str.format("Il file ricetta $1 non esiste", ls_path_file_ricetta)
		luo_log.error( as_message )
		destroy luo_log
		return -1
	else
		as_message = g_str.format("File ricetta non caricato; verificare il parametro multiaziendale CU1", ls_path_file_ricetta)
		luo_log.error( as_message )
		destroy luo_log
		return -1
	end if
end if

ls_target_file = guo_functions.uof_get_random_filename( )
ls_target_file = "record_" + ls_target_file  + ".csv"

guo_functions.uof_get_parametro(is_cod_parametro_path_backup, ls_path_target)
if ls_path_target = "" or isnull(ls_path_target) then 
	as_message = g_str.format("Attenzione, manca il path della cartella backup delle ricette", ls_path_file_ricetta)
	luo_log.error( as_message )
	destroy luo_log
	return -1
end if

ll_ret = filecopy(ls_path_file_ricetta, ls_path_target + ls_target_file, true)
if ll_ret < 0 then
	as_message = g_str.format("Errore in backup del file ricette su file $1 ", ls_path_target + ls_target_file)
	luo_log.error( as_message )
	destroy luo_log
	return -1
end if

destroy luo_log

return 0
end function

on uo_curti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_curti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

