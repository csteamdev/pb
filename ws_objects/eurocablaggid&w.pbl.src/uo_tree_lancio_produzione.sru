﻿$PBExportHeader$uo_tree_lancio_produzione.sru
forward
global type uo_tree_lancio_produzione from treeview
end type
end forward

global type uo_tree_lancio_produzione from treeview
integer width = 997
integer height = 1696
string dragicon = "Exclamation!"
boolean dragauto = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type
global uo_tree_lancio_produzione uo_tree_lancio_produzione

type variables
boolean ib_menu = true
long il_picture[], il_handle
end variables

forward prototypes
private function integer uof_add_picture (string as_image_filename)
private subroutine uof_reset ()
public subroutine uof_add_ramo_nuovo_lancio ()
public function integer uof_carica_tv ()
end prototypes

private function integer uof_add_picture (string as_image_filename);return addpicture(s_cs_xx.volume + s_cs_xx.risorse + as_image_filename) 


end function

private subroutine uof_reset ();setredraw(false)
deletepictures()
deleteitem(0)
setredraw(true)
end subroutine

public subroutine uof_add_ramo_nuovo_lancio ();long	 ll_item
s_tree_lancio_produzione	lstr_record
treeviewitem	ltvi_campo


lstr_record.anno_commessa = 0
lstr_record.num_num_commessa = 0
ltvi_campo.data = lstr_record
ltvi_campo.label = "Elenco Ricette in Produzione"
ltvi_campo.pictureindex = il_picture[1]
ltvi_campo.selectedpictureindex =  il_picture[1]
ltvi_campo.overlaypictureindex =  il_picture[1]
ltvi_campo.children = true
ltvi_campo.selected = false
ll_item = insertitemlast(0, ltvi_campo)

end subroutine

public function integer uof_carica_tv ();string ls_sql
long ll_livello = 0, ll_ret, ll_i, ll_current_handle, ll_item, ll_progr_old, ll_cont
datastore lds_data1
treeviewitem ltv_item
s_tree_lancio_produzione ls_record, ls_record_null

uof_reset()

il_picture[1] = uof_add_picture("12.5\Lancio_prod_aperta.ico")
il_picture[2] = uof_add_picture("12.5\tv_lancio_prod_ottimizzato.png")
il_picture[3] = uof_add_picture("12.5\tv_ordine_cliente.png")
il_picture[4] = uof_add_picture("12.5\tv_righe_ordine.png")
il_picture[5] = uof_add_picture("12.5\Lancio_prod_chiuso.ico")
il_picture[6] = uof_add_picture("12.5\Lancio_prod_parziale.ico")


// inserisco ramo iniziale
uof_add_ramo_nuovo_lancio()
/*
if isnull(handle) or handle <= 0 then
	return 0
end if

ll_current_handle = handle

getitem(handle,ltv_item)
*/


setpointer(HourGlass!)

ls_sql = "select A.progr_produzione, A.anno_commessa, A.num_commessa, A.cod_reparto, A.cod_lavorazione, A.quan_in_produzione, A.quan_prodotta, A.flag_fine_fase, &
			B.cod_cat_attrezzature, A.flag_esterna, C.des_cat_attrezzature, R.des_reparto, COM.cod_prodotto, COM.cod_versione, A.ordinamento &
			from avan_produzione_com A &
			left outer join tes_fasi_lavorazione B on A.cod_azienda=B.cod_azienda and A.cod_prodotto=B.cod_prodotto and A.cod_reparto=B.cod_reparto and A.cod_lavorazione=B.cod_lavorazione and A.cod_versione=B.cod_versione &
			left outer join tab_cat_attrezzature C on B.cod_azienda=C.cod_azienda and B.cod_cat_attrezzature=C.cod_cat_attrezzature &
			left outer join anag_reparti R on A.cod_azienda=R.cod_azienda and A.cod_reparto=R.cod_reparto &
			left outer join anag_commesse COM on A.cod_azienda=COM.cod_azienda and A.anno_commessa=COM.anno_commessa and A.num_commessa=COM.num_commessa "

ls_sql = g_str.format( "$1 where A.cod_azienda = '$2' and A.progr_produzione > 0 and A.progr_produzione is not null and A.flag_fine_fase='N' order by 1 asc, 15 asc",ls_sql, s_cs_xx.cod_azienda)
			
ll_ret = guo_functions.uof_crea_datastore(lds_data1, ls_sql)
ll_progr_old = 0

for ll_i = 1 to ll_ret
	if ll_progr_old =  lds_data1.getitemnumber(ll_i,1) then continue
	ll_progr_old = lds_data1.getitemnumber(ll_i,1)
	ls_record.progr_produzione = lds_data1.getitemnumber(ll_i,1)
	ls_record.anno_commessa = lds_data1.getitemnumber(ll_i,2)
	ls_record.num_num_commessa = lds_data1.getitemnumber(ll_i,3)
	ltv_item.data = ls_record
	ltv_item.label = g_str.format("[$1] $2 - $3/$4", lds_data1.getitemnumber(ll_i,1), lds_data1.getitemstring(ll_i,13), lds_data1.getitemnumber(ll_i,2), lds_data1.getitemnumber(ll_i,3))
	
	select 	count(*) 
	into		:ll_cont
	from 		det_orari_produzione
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :ls_record.anno_commessa and
				num_commessa = :ls_record.num_num_commessa;

	if not isnull(ll_cont) and ll_cont > 0 then
		// ci sono degli avanzamenti
		ltv_item.pictureindex = il_picture[2]
		ltv_item.selectedpictureindex =  il_picture[2]
		ltv_item.overlaypictureindex =  il_picture[2]
	else	
		ltv_item.pictureindex = il_picture[4]
		ltv_item.selectedpictureindex =  il_picture[4]
		ltv_item.overlaypictureindex =  il_picture[4]
	end if
	ltv_item.children = true
	ltv_item.selected = false
	
	ll_item = insertitemlast(ll_current_handle, ltv_item)

next

destroy lds_data1

setpointer(Arrow!)
return 0
end function

on uo_tree_lancio_produzione.create
end on

on uo_tree_lancio_produzione.destroy
end on

event rightclicked;
/*if ib_menu then
	long  ll_livello
	treeviewitem ltv_item
	s_tree_lancio_produzione ls_record
	m_pop_tree_lancio_produzione lm_menu
	
	il_handle = handle
	

	if handle > 0 then
	
		getitem(handle,ltv_item)
		ls_record = ltv_item.data
		ll_livello = ls_record.livello
		
		if ll_livello = 0 then
			lm_menu = create m_pop_tree_lancio_produzione
			lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
		end if
		
		// verifico se è stato eseguito il lancio di produzione; se non è stato fatto inibisco la stampa produzione e lista preparazione
		//lm_menu.m_stampalistapreparazione.enabled=false
		
	else
		lm_menu = create m_pop_tree_lancio_produzione
		lm_menu.m_ottimizza.enabled=false
		lm_menu.m_stampa_ordini.enabled=false
		lm_menu.m_elimina_lancio.enabled=false
		lm_menu.m_lancio_produzione.enabled=false
		lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
		
	end if
	
	destroy lm_menu
	
end if	
*/
end event

event selectionchanged;il_handle=newhandle
end event

