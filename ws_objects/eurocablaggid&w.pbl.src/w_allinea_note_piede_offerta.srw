﻿$PBExportHeader$w_allinea_note_piede_offerta.srw
forward
global type w_allinea_note_piede_offerta from window
end type
type st_2 from statictext within w_allinea_note_piede_offerta
end type
type cb_1 from commandbutton within w_allinea_note_piede_offerta
end type
type st_1 from statictext within w_allinea_note_piede_offerta
end type
type cb_connect from commandbutton within w_allinea_note_piede_offerta
end type
end forward

global type w_allinea_note_piede_offerta from window
integer width = 2469
integer height = 788
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
st_2 st_2
cb_1 cb_1
st_1 st_1
cb_connect cb_connect
end type
global w_allinea_note_piede_offerta w_allinea_note_piede_offerta

type variables
transaction it_euro
end variables
on w_allinea_note_piede_offerta.create
this.st_2=create st_2
this.cb_1=create cb_1
this.st_1=create st_1
this.cb_connect=create cb_connect
this.Control[]={this.st_2,&
this.cb_1,&
this.st_1,&
this.cb_connect}
end on

on w_allinea_note_piede_offerta.destroy
destroy(this.st_2)
destroy(this.cb_1)
destroy(this.st_1)
destroy(this.cb_connect)
end on

type st_2 from statictext within w_allinea_note_piede_offerta
integer x = 800
integer y = 340
integer width = 1326
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_allinea_note_piede_offerta
integer x = 137
integer y = 320
integer width = 631
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "NOTE"
end type

event clicked;long ll_i, ll_anno_registrazione, ll_num_registrazione
string ls_sql, ls_errore, ls_note
datastore lds_data

ls_sql = "select anno_registrazione, num_registrazione, nota_piede from tes_off_ven where cod_azienda = 'A01' order by anno_registrazione, num_registrazione "

if guo_functions.uof_crea_datastore( lds_data, ls_sql, it_euro, ls_errore) < 0 then
	g_mb.error(ls_errore)
	rollback;
	return 
end if


for ll_i = 1 to lds_data.rowcount()
	ll_anno_registrazione = lds_data.getitemnumber(ll_i,1)
	ll_num_registrazione = lds_data.getitemnumber(ll_i,2)
	
	st_2.text = g_str.format("$1-$2", ll_anno_registrazione, ll_num_registrazione )
	Yield()
	
	ls_note = lds_data.getitemstring(ll_i,3)
	
	update tes_off_ven
	set nota_piede = :ls_note
	where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;
	if sqlca.sqlcode <> 0 then
		rollback;
		if g_mb.confirm("ERRORE UPDATE. PROSEGUO?",sqlca.sqlerrtext ) = false then return
	end if
	commit;
	
next


end event

type st_1 from statictext within w_allinea_note_piede_offerta
integer x = 800
integer y = 100
integer width = 1257
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_connect from commandbutton within w_allinea_note_piede_offerta
integer x = 137
integer y = 60
integer width = 631
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Connetti al vecchio DB"
end type

event clicked;it_euro = create transaction
if f_po_getconnectinfo("", "database_01", it_euro) <> 0 then
   return
end if

connect using it_euro;
if it_euro.sqlcode = 0 then
	st_1.text = "Connessione eseguita correttamente al DB"
else
	g_mb.error(it_euro.sqlerrtext)
	st_1.text = "Errore connessione al DB"
end if
end event

