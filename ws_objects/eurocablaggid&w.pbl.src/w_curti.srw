﻿$PBExportHeader$w_curti.srw
forward
global type w_curti from w_cs_xx_principale
end type
type cb_3 from commandbutton within w_curti
end type
type st_2 from statictext within w_curti
end type
type cb_2 from commandbutton within w_curti
end type
type dw_ricetta_prodotto from datawindow within w_curti
end type
type st_1 from statictext within w_curti
end type
type cb_1 from commandbutton within w_curti
end type
end forward

global type w_curti from w_cs_xx_principale
integer width = 4398
integer height = 2020
string title = "Curti"
cb_3 cb_3
st_2 st_2
cb_2 cb_2
dw_ricetta_prodotto dw_ricetta_prodotto
st_1 st_1
cb_1 cb_1
end type
global w_curti w_curti

on w_curti.create
int iCurrent
call super::create
this.cb_3=create cb_3
this.st_2=create st_2
this.cb_2=create cb_2
this.dw_ricetta_prodotto=create dw_ricetta_prodotto
this.st_1=create st_1
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_3
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.dw_ricetta_prodotto
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.cb_1
end on

on w_curti.destroy
call super::destroy
destroy(this.cb_3)
destroy(this.st_2)
destroy(this.cb_2)
destroy(this.dw_ricetta_prodotto)
destroy(this.st_1)
destroy(this.cb_1)
end on

type cb_3 from commandbutton within w_curti
integer x = 23
integer y = 360
integer width = 430
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Carica Tabella"
end type

event clicked;dw_ricetta_prodotto.settransobject(sqlca)
dw_ricetta_prodotto.retrieve()
end event

type st_2 from statictext within w_curti
integer x = 2057
integer y = 360
integer width = 1851
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 16777215
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_curti
integer x = 3931
integer y = 340
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "WRITE FILE"
end type

event clicked;string	ls_errore
long	ll_ret
time		lt_start, lt_end
uo_curti luo_curti

lt_start = now()
luo_curti = create uo_curti
ll_ret = luo_curti.uof_scrivi_ricetta( ls_errore)

if ll_ret < 0 then
	g_mb.error(ls_errore)
	return
end if
destroy luo_curti
lt_end=now()

st_2.text = g_str.format( "Elaborazione eseguita in $1 secondi", SecondsAfter ( lt_start, lt_end ) )


return 
end event

type dw_ricetta_prodotto from datawindow within w_curti
integer x = 23
integer y = 480
integer width = 4320
integer height = 1420
integer taborder = 20
string title = "none"
string dataobject = "d_curti_read_ricetta"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_curti
integer x = 434
integer y = 60
integer width = 1851
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 16777215
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_curti
integer x = 23
integer y = 40
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "READ FILE"
end type

event clicked;string	ls_errore
long	ll_ret
time		lt_start, lt_end
uo_curti luo_curti

lt_start = now()
luo_curti = create uo_curti
ll_ret = luo_curti.uof_leggi_ricetta(  ref ls_errore)
if ll_ret < 0 then
	g_mb.error(ls_errore)
	return
end if
destroy luo_curti
lt_end=now()

st_1.text = g_str.format( "Elaborazione eseguita in $1 secondi", SecondsAfter ( lt_start, lt_end ) )


return 
end event

