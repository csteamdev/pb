﻿$PBExportHeader$w_curti_ricerca_ricette.srw
forward
global type w_curti_ricerca_ricette from w_cs_xx_principale
end type
type tv_1 from uo_tree_lancio_produzione within w_curti_ricerca_ricette
end type
type dw_edit_ricetta from datawindow within w_curti_ricerca_ricette
end type
type dw_elenco_prodotti_stat_prod from uo_std_dw within w_curti_ricerca_ricette
end type
type dw_folder from u_folder within w_curti_ricerca_ricette
end type
type dw_avanzamento from uo_std_dw within w_curti_ricerca_ricette
end type
type dw_ricette_trovate from datawindow within w_curti_ricerca_ricette
end type
type dw_avanzamento_sel from datawindow within w_curti_ricerca_ricette
end type
type dw_elenco_prodotti_stat from uo_cs_xx_dw within w_curti_ricerca_ricette
end type
type dw_selezione from uo_std_dw within w_curti_ricerca_ricette
end type
type dw_dddw_lista_reparti from uo_dddw_checkbox within w_curti_ricerca_ricette
end type
type dw_ricerca_ricette from datawindow within w_curti_ricerca_ricette
end type
end forward

global type w_curti_ricerca_ricette from w_cs_xx_principale
integer width = 6702
integer height = 2780
string title = "Ricette Produzione"
tv_1 tv_1
dw_edit_ricetta dw_edit_ricetta
dw_elenco_prodotti_stat_prod dw_elenco_prodotti_stat_prod
dw_folder dw_folder
dw_avanzamento dw_avanzamento
dw_ricette_trovate dw_ricette_trovate
dw_avanzamento_sel dw_avanzamento_sel
dw_elenco_prodotti_stat dw_elenco_prodotti_stat
dw_selezione dw_selezione
dw_dddw_lista_reparti dw_dddw_lista_reparti
dw_ricerca_ricette dw_ricerca_ricette
end type
global w_curti_ricerca_ricette w_curti_ricerca_ricette

type variables
boolean 	ib_retrieve=true

decimal	id_tot_minuti_attrezzaggio, id_tot_minuti_attrezzaggio_commessa,id_tot_minuti_lavorazione, & 
			id_tot_minuti_risorsa_umana 
			
			
datetime idt_da_data, idt_a_data

string   is_1, is_2, is_3, is_4, is_5, is_6, is_7, is_8, is_9, is_10

uo_curti 	iuo_curti
end variables

forward prototypes
public function integer wf_calcola_tempi_fase (string fs_cod_reparto, string fs_cod_lavorazione, string fs_cod_prodotto, string fs_cod_operaio, datetime fdt_da_data, datetime fdt_a_data, ref decimal fd_minuti_attrezzaggio, ref decimal fd_minuti_attrezzaggio_commessa, ref decimal fd_minuti_lavorazione, ref decimal fd_minuti_risorsa_umana)
public function boolean wf_is_edit_modified ()
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
end prototypes

public function integer wf_calcola_tempi_fase (string fs_cod_reparto, string fs_cod_lavorazione, string fs_cod_prodotto, string fs_cod_operaio, datetime fdt_da_data, datetime fdt_a_data, ref decimal fd_minuti_attrezzaggio, ref decimal fd_minuti_attrezzaggio_commessa, ref decimal fd_minuti_lavorazione, ref decimal fd_minuti_risorsa_umana);string   ls_giorno,ls_giorno_it,ls_problemi_produzione, & 
			ls_cognome,ls_errore,ls_cod_prodotto_test,ls_cod_operaio
datetime ldt_data_corrente,lt_orario_attrezzaggio,lt_orario_attrezzaggio_commessa, &
			lt_orario_lavorazione,lt_orario_risorsa_umana,lt_orario_attrezzaggio_fine,lt_orario_attrezzaggio_commessa_fine, &
			lt_orario_lavorazione_fine,lt_orario_risorsa_umana_fine
decimal  ld_secondi,ld_minuti_risorsa_umana,ld_ore,ld_quan_prodotta,ld_minuti_lavorazione,ld_minuti_attrezzaggio, & 
			ld_minuti_attrezzaggio_commessa,ld_ore_lavorazione,ld_ore_attrezzaggio,ld_ore_attrezzaggio_commessa, & 
			ld_somma_tempo_attrezzaggio,ld_somma_tempo_attrezzaggio_commessa,ld_somma_tempo_lavorazione
integer  li_risposta
long     ll_num_anno_commessa,ll_num_num_commessa,ll_num_commesse,ll_num_orari

select count(*)
into   :ll_num_orari
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if
		
if ll_num_orari = 0 then return 1

		
ldt_data_corrente = fdt_da_data

do while ldt_data_corrente <= fdt_a_data
	
	declare r_det_orari cursor for 
	select orario_attrezzaggio,
			 orario_attrezzaggio_commessa,
			 orario_lavorazione,
			 orario_risorsa_umana,
			 cod_operaio
	from   det_orari_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    data_giorno = :ldt_data_corrente
	and    flag_inizio = 'S'
	and    cod_reparto = :fs_cod_reparto
	and    cod_lavorazione=:fs_cod_lavorazione
	and    cod_prodotto=:fs_cod_prodotto
	and    cod_operaio like :fs_cod_operaio
	order by prog_orari;
	
	open r_det_orari;
	
	do while 1 = 1 
	
		fetch r_det_orari
		into  :lt_orario_attrezzaggio,
				:lt_orario_attrezzaggio_commessa,
				:lt_orario_lavorazione,
				:lt_orario_risorsa_umana,
				:ls_cod_operaio;

		
		if sqlca.sqlcode = 100 then exit

		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_orari;
			return -1
		end if
	
		if time(lt_orario_attrezzaggio) <> 00:00:00 then

			declare r_d1 cursor for 
			select orario_attrezzaggio
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio like :ls_cod_operaio
			and    orario_attrezzaggio is not null
			and    orario_attrezzaggio > :lt_orario_attrezzaggio
			and    cod_prodotto=:fs_cod_prodotto
			and    cod_lavorazione=:fs_cod_lavorazione
			and    cod_reparto=:fs_cod_reparto
			order by orario_attrezzaggio;
			
			open r_d1;
			
			fetch r_d1
			into  :lt_orario_attrezzaggio_fine;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d1;
				close r_det_orari;
				return -1
			end if
			
			close r_d1;
			ld_secondi = SecondsAfter ( time(lt_orario_attrezzaggio),time(lt_orario_attrezzaggio_fine))
			ld_minuti_attrezzaggio = ld_minuti_attrezzaggio + ld_secondi/60
			ld_secondi=0
		end if	

		if time(lt_orario_attrezzaggio_commessa) <> 00:00:00 then
			declare r_d2 cursor for 
			select orario_attrezzaggio_commessa
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio like :ls_cod_operaio
			and    orario_attrezzaggio_commessa is not null
			and    orario_attrezzaggio_commessa > :lt_orario_attrezzaggio_commessa
			and    cod_prodotto=:fs_cod_prodotto
			and    cod_lavorazione=:fs_cod_lavorazione
			and    cod_reparto=:fs_cod_reparto

			order by orario_attrezzaggio_commessa;
			
			open r_d2;
			
			fetch r_d2
			into  :lt_orario_attrezzaggio_commessa_fine;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d2;
				close r_det_orari;
				return -1
			end if
			
			close r_d2;
			ld_secondi = SecondsAfter ( time(lt_orario_attrezzaggio_commessa),time(lt_orario_attrezzaggio_commessa_fine))
			ld_minuti_attrezzaggio_commessa = ld_minuti_attrezzaggio_commessa + ld_secondi/60
			ld_secondi = 0
		end if	

		if time(lt_orario_lavorazione) <> 00:00:00  then
			declare r_d3 cursor for 
			select orario_lavorazione,
					 problemi_produzione
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio like :ls_cod_operaio
			and    orario_lavorazione is not null
			and    orario_lavorazione > :lt_orario_lavorazione
			and    cod_prodotto=:fs_cod_prodotto
			and    cod_lavorazione=:fs_cod_lavorazione
			and    cod_reparto=:fs_cod_reparto

			order by orario_lavorazione;
			
			open r_d3;
			
			fetch r_d3
			into  :lt_orario_lavorazione_fine,
					:ls_problemi_produzione;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d3;
				close r_det_orari;
				return -1
			end if 
			
			close r_d3;
			if ls_problemi_produzione ='N' or isnull(ls_problemi_produzione) then
				ld_secondi = SecondsAfter ( time(lt_orario_lavorazione),time(lt_orario_lavorazione_fine))
				ld_minuti_lavorazione = ld_minuti_lavorazione + ld_secondi/60			
			end if
		end if	

		if time(lt_orario_risorsa_umana) <> 00:00:00 then
			declare r_d4 cursor for 
			select orario_risorsa_umana
			from   det_orari_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda
			and    data_giorno = :ldt_data_corrente
			and    flag_inizio = 'N'
			and    cod_operaio = :ls_cod_operaio
			and    orario_risorsa_umana is not null
			and    orario_risorsa_umana >= :lt_orario_risorsa_umana
			order by orario_risorsa_umana;
			
			open r_d4;
			
			fetch r_d4
			into  :lt_orario_risorsa_umana_fine;
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_d4;
				close r_det_orari;
				return -1
			end if
			
			close r_d4;
			ld_secondi = SecondsAfter ( time(lt_orario_risorsa_umana),time(lt_orario_risorsa_umana_fine))
			ld_minuti_risorsa_umana = ld_minuti_risorsa_umana + ld_secondi/60			
		
		end if	

	loop
	
	
	close r_det_orari;


	ldt_data_corrente = datetime(RelativeDate ( date(ldt_data_corrente) , 1 ),00:00:00)
		
loop

select sum(quan_prodotta)
into   :ld_quan_prodotta
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select count(distinct anno_commessa)
into   :ll_num_anno_commessa
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select count(distinct num_commessa)
into   :ll_num_num_commessa
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda
and    data_giorno between :fdt_da_data and :fdt_a_data
and    cod_reparto = :fs_cod_reparto
and    cod_lavorazione=:fs_cod_lavorazione
and    cod_prodotto=:fs_cod_prodotto
and    cod_operaio like :fs_cod_operaio;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if ll_num_num_commessa > ll_num_anno_commessa then
	ll_num_commesse = ll_num_num_commessa
else
	ll_num_commesse = ll_num_anno_commessa
end if

if ll_num_commesse>0 then
	fd_minuti_attrezzaggio = round(ld_minuti_attrezzaggio/ll_num_commesse,4)
	fd_minuti_attrezzaggio_commessa = round(ld_minuti_attrezzaggio_commessa/ll_num_commesse,4)
end if

if ld_quan_prodotta > 0 then
	fd_minuti_lavorazione = round(ld_minuti_lavorazione/ld_quan_prodotta,4)
	fd_minuti_risorsa_umana = round(ld_minuti_risorsa_umana/ld_quan_prodotta,4)
end if



return 0
end function

public function boolean wf_is_edit_modified ();long ll_i
for ll_i = 1 to dw_edit_ricetta.rowcount()
	if dw_edit_ricetta.getitemstatus(ll_i,0,Primary!) = DataModified! then
		return true
	end if
next
return false

end function

public subroutine wf_memorizza_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero,ll_cont
datetime ldt_data

if g_mb.messagebox("Omnia","Memorizza l'attuale impostazione dei filtro come predefinito?",Question!,YesNo!,2) = 2 then return

ls_colcount = dw_avanzamento_sel.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""
for ll_i = 1 to ll_colonne
	ls_nome_colonna = dw_avanzamento_sel.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_avanzamento_sel.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		ldt_data = dw_avanzamento_sel.getitemdatetime(dw_avanzamento_sel.getrow(), ls_nome_colonna)
		if isnull(ldt_data) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
		end if
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		ls_stringa = dw_avanzamento_sel.getitemstring(dw_avanzamento_sel.getrow(), ls_nome_colonna)
		if isnull(ls_stringa) then
			ls_memo += "NULL~t"
		else
			ls_memo += ls_stringa + "~t"
		end if
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		ll_numero = dw_avanzamento_sel.getitemnumber(dw_avanzamento_sel.getrow(), ls_nome_colonna)
		if isnull(ll_numero) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ll_numero) + "~t"
		end if
	end if

next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'CURTI';
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente =  :ls_cod_utente and
		   tipo_filtro = 'CURTI';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
else
	insert into filtri_manutenzioni
		(cod_azienda,
		 cod_utente,
		 filtri,
		 tipo_filtro)
	 values
		 (:s_cs_xx.cod_azienda,
		  :ls_cod_utente,
		  :ls_memo,
		  'CURTI');
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

commit;

return
end subroutine

public subroutine wf_leggi_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero
datetime ldt_data

ls_colcount = dw_avanzamento_sel.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'CURTI';
if sqlca.sqlcode = 100 then
	//g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_avanzamento_sel.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_avanzamento_sel.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_avanzamento_sel.setitem(dw_avanzamento_sel.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_avanzamento_sel.setitem(dw_avanzamento_sel.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_avanzamento_sel.setitem(dw_avanzamento_sel.getrow(),ls_nome_colonna, ll_numero)
	end if

next
dw_avanzamento_sel.accepttext()

return
end subroutine

on w_curti_ricerca_ricette.create
int iCurrent
call super::create
this.tv_1=create tv_1
this.dw_edit_ricetta=create dw_edit_ricetta
this.dw_elenco_prodotti_stat_prod=create dw_elenco_prodotti_stat_prod
this.dw_folder=create dw_folder
this.dw_avanzamento=create dw_avanzamento
this.dw_ricette_trovate=create dw_ricette_trovate
this.dw_avanzamento_sel=create dw_avanzamento_sel
this.dw_elenco_prodotti_stat=create dw_elenco_prodotti_stat
this.dw_selezione=create dw_selezione
this.dw_dddw_lista_reparti=create dw_dddw_lista_reparti
this.dw_ricerca_ricette=create dw_ricerca_ricette
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tv_1
this.Control[iCurrent+2]=this.dw_edit_ricetta
this.Control[iCurrent+3]=this.dw_elenco_prodotti_stat_prod
this.Control[iCurrent+4]=this.dw_folder
this.Control[iCurrent+5]=this.dw_avanzamento
this.Control[iCurrent+6]=this.dw_ricette_trovate
this.Control[iCurrent+7]=this.dw_avanzamento_sel
this.Control[iCurrent+8]=this.dw_elenco_prodotti_stat
this.Control[iCurrent+9]=this.dw_selezione
this.Control[iCurrent+10]=this.dw_dddw_lista_reparti
this.Control[iCurrent+11]=this.dw_ricerca_ricette
end on

on w_curti_ricerca_ricette.destroy
call super::destroy
destroy(this.tv_1)
destroy(this.dw_edit_ricetta)
destroy(this.dw_elenco_prodotti_stat_prod)
destroy(this.dw_folder)
destroy(this.dw_avanzamento)
destroy(this.dw_ricette_trovate)
destroy(this.dw_avanzamento_sel)
destroy(this.dw_elenco_prodotti_stat)
destroy(this.dw_selezione)
destroy(this.dw_dddw_lista_reparti)
destroy(this.dw_ricerca_ricette)
end on

event pc_setwindow;call super::pc_setwindow;string				ls_message, ls_sql, ls_memo
windowobject	lw_oggetti[]


dw_elenco_prodotti_stat.ib_dw_report = true
dw_elenco_prodotti_stat_prod.ib_dw_report = true
dw_avanzamento.ib_dw_report=true

ls_sql = 	g_str.format("SELECT cod_reparto, des_reparto from anag_reparti where cod_azienda = '$1' and flag_blocco='N' order by cod_reparto",s_cs_xx.cod_azienda)
dw_dddw_lista_reparti.uof_set_column_by_sql(ls_sql)
dw_dddw_lista_reparti.uof_set_parent_dw(dw_avanzamento_sel, "cod_reparto", "cod_reparto")

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	select filtri 
	into   :ls_memo
	from   filtri_manutenzioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_utente =  :s_cs_xx.cod_utente and
			 tipo_filtro = 'CURTI';
	if sqlca.sqlcode = 0 then
		dw_dddw_lista_reparti.uof_set_select_on_filtered_rows( "cod_reparto", ls_memo )
		dw_avanzamento_sel.setitem(1,1,trim(ls_memo))
	else
		dw_dddw_lista_reparti.uof_select_all_rows( )
	end if
else
	dw_dddw_lista_reparti.uof_select_all_rows( )
end if

lw_oggetti[1] = dw_elenco_prodotti_stat_prod
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])
lw_oggetti[1] = dw_avanzamento
lw_oggetti[2] = dw_avanzamento_sel
dw_folder.fu_assigntab(4, "Avanzamento", lw_oggetti[])
lw_oggetti[1] = dw_ricette_trovate
lw_oggetti[2] = dw_edit_ricetta
dw_folder.fu_assigntab(3, "Ricette Corrisp.", lw_oggetti[])
lw_oggetti[1] = dw_ricerca_ricette
lw_oggetti[2] = dw_selezione
lw_oggetti[3] = dw_elenco_prodotti_stat
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
dw_folder.fu_foldercreate(4, 4)
dw_folder.fu_selecttab(1)

dw_elenco_prodotti_stat.SetTrans(sqlca)
dw_elenco_prodotti_stat_prod.setTrans(sqlca)
dw_edit_ricetta.setTrans(sqlca)

dw_selezione.insertrow(0)
dw_ricerca_ricette.insertrow(0)
dw_avanzamento_sel.insertrow(0)

dw_ricerca_ricette.setitem(1,"data_inizio", datetime(today(),00:00:00))
dw_ricerca_ricette.setitem(1,"data_fine", datetime(relativedate(today(),30),00:00:00))
dw_ricerca_ricette.object.b_pdf.enabled='0'
dw_ricerca_ricette.object.b_stampa.enabled='0'

dw_edit_ricetta.Modify("b_save.Filename='" + s_cs_xx.volume + s_cs_xx.risorse + "12.5\curti_save32x32.png'")
dw_edit_ricetta.Modify("b_saveas.Filename='" + s_cs_xx.volume + s_cs_xx.risorse + "12.5\Curti_copy32x32.png'")
dw_edit_ricetta.Modify("b_read.Filename='" + s_cs_xx.volume + s_cs_xx.risorse + "12.5\Curti_Refresh32x32.png'")
dw_edit_ricetta.Modify("b_distinta.Filename='" + s_cs_xx.volume + s_cs_xx.risorse + "12.5\Curti_distinta32x32.png'")
dw_avanzamento_sel.Modify("b_refresh.Filename='" + s_cs_xx.volume + s_cs_xx.risorse + "12.5\Curti_Refresh32x32.png'")

tv_1.uof_carica_tv( )


// Apro il file ricette con LOCK di scrittura
iuo_curti = create uo_curti
if iuo_curti.uof_open_file_ricette( ls_message) < 0 then
	g_mb.error(ls_message)
	event post close()
end if

end event

event pc_setddlb;call super::pc_setddlb;//f_po_loadddlb(ddlb_cod_tipo_analisi, &
//                 sqlca, &
//                 "tab_tipi_analisi", &
//                 "cod_tipo_analisi", &
//                 "des_tipo_analisi", &
//                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
					  
f_po_loaddddw_dw ( dw_ricerca_ricette, "cod_tipo_analisi", sqlca, "tab_tipi_analisi", "cod_tipo_analisi", "des_tipo_analisi", "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )
					

//f_po_loaddddw_dw(dw_tab_des_variabili_lista, &
//                 "cod_variabile", &
//                 sqlca, &
//                 "tab_variabili_formule", &
//                 "cod_variabile", &
//                 "isnull(nome_campo_database, cod_variabile) ", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_po_loaddddw_dw ( dw_selezione, "as_reparto_1", sqlca, "anag_reparti", "cod_reparto", "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )

f_po_loaddddw_dw ( dw_selezione, "as_reparto_2", sqlca, "anag_reparti", "cod_reparto", "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )

f_po_loaddddw_dw ( dw_selezione, "as_reparto_3", sqlca, "anag_reparti", "cod_reparto", "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )

f_po_loaddddw_dw ( dw_selezione, "as_reparto_4", sqlca, "anag_reparti", "cod_reparto", "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )

f_po_loaddddw_dw ( dw_selezione, "as_reparto_5", sqlca, "anag_reparti", "cod_reparto", "des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )

end event

event close;call super::close;if iuo_curti.uof_close_file_ricette( ) < 0 then
	g_mb.error("ERRORE IN FASE DI CHIUSURA FILE RICETTE")
end if
destroy iuo_curti
end event

type tv_1 from uo_tree_lancio_produzione within w_curti_ricerca_ricette
integer x = 5097
integer y = 20
integer width = 1531
integer height = 2620
integer taborder = 260
string dragicon = "DisplayCurrentLibrary!"
boolean disabledragdrop = false
end type

event dragwithin;call super::dragwithin;string ls_immagini

ls_immagini = s_cs_xx.volume + s_cs_xx.risorse

if handle = 0 or isnull(handle) then
	setdrophighlight(0)
	dragicon = ls_immagini + "10.5\MENU_dragstop.ico"
	return -1
end if
	
if typeof(source) = treeview! and source.classname() = "tv_1" then
	setdrophighlight(handle)
	dragicon = ls_immagini + "10.5\MENU_dragdrop.ico"
else
	setdrophighlight(0)
	dragicon = ls_immagini + "10.5\MENU_dragstop.ico"
end if



end event

event rightclicked;call super::rightclicked;//long ll_drop, ll_drag
//
//
//if handle = 0 or isnull(handle) then
//	return -1
//end if
//
//ll_drop = handle
//
//ll_drag = tv_1.finditem(currenttreeitem!,0)
//
//tv_1.selectitem(handle)
//
//il_handle = handle
//
//m_tree_produzione_drag_drop menu
//menu = create m_tree_produzione_drag_drop
//m_tree_produzione_drag_drop.itv_tree = this
//m_tree_produzione_drag_drop.il_drop = ll_drop
//m_tree_produzione_drag_drop.il_drag = ll_drag
//m_tree_produzione_drag_drop.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
//
end event

event dragdrop;call super::dragdrop;string	ls_sql
long ll_drop, ll_drag, ll_rows, ll_prog_produzione, ll_i, ll_prog_produzione_insert, ll_anno_commessa, ll_num_commessa, ll_ret
treeviewitem ltv_item_source, ltv_item_target
s_tree_lancio_produzione lstr_origine, lstr_destinazione
datastore lds_data

if isnull(handle) then return
ll_drop = handle

ll_drag = tv_1.finditem(currenttreeitem!,0)

ll_ret=getitem(ll_drag,ltv_item_source)
if ll_ret < 0 then return
lstr_origine = ltv_item_source.data
ll_ret=getitem(ll_drop, ltv_item_target)
if ll_ret < 0 then return
lstr_destinazione = ltv_item_target.data

if lstr_destinazione.anno_commessa = 0 and lstr_destinazione.num_num_commessa=0 then
	ll_prog_produzione_insert = 10
else
	ll_prog_produzione_insert = lstr_destinazione.progr_produzione + 10
end if

ll_anno_commessa = lstr_origine.anno_commessa
ll_num_commessa = lstr_origine.num_num_commessa

update avan_produzione_com
set 	progr_produzione = 0
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_commessa = :ll_anno_commessa and
		num_commessa = :ll_num_commessa ;
if sqlca.sqlcode < 0 then
	g_mb.error ("Errore SQL in aggiornamento ordinamento commesse " + sqlca.sqlerrtext)
	rollback;
	return
end if

commit;


ll_prog_produzione = lstr_destinazione.progr_produzione

ls_sql = g_str.format("select distinct anno_commessa, num_commessa, progr_produzione from avan_produzione_com where progr_produzione >= $1 order by progr_produzione asc",ll_prog_produzione+ 1)

ll_rows = guo_functions.uof_crea_datastore(lds_data,ls_sql)

for ll_i = 1 to ll_rows
	
	ll_anno_commessa = lds_data.getitemnumber(ll_i,1)
	ll_num_commessa = lds_data.getitemnumber(ll_i,2)
	
	update avan_produzione_com
	set progr_produzione = :ll_prog_produzione + 20
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa and
			num_commessa = :ll_num_commessa;
	
	ll_prog_produzione = ll_prog_produzione + 10
	
next
commit;

update avan_produzione_com
set progr_produzione = :ll_prog_produzione_insert
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_commessa = :lstr_origine.anno_commessa and
		num_commessa = :lstr_origine.num_num_commessa;
		
commit;

uof_carica_tv( )

end event

event key;call super::key;if key = KeyDelete! then
	
	long ll_handle, ll_anno_commessa, ll_num_commessa
	treeviewitem ltv_item
	s_tree_lancio_produzione lstr_origine
	datastore lds_data
	
	ll_handle = tv_1.finditem(currenttreeitem!,0)
	
	getitem(ll_handle, ltv_item)
	lstr_origine = ltv_item.data
	
	ll_anno_commessa = lstr_origine.anno_commessa
	ll_num_commessa = lstr_origine.num_num_commessa
	
	update avan_produzione_com
	set progr_produzione = 0
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa and
			num_commessa = :ll_num_commessa;
	
	commit;
	
	tv_1.deleteitem(ll_handle)
	
	tv_1.uof_carica_tv( )
	
end if
end event

type dw_edit_ricetta from datawindow within w_curti_ricerca_ricette
event ue_retrieve ( string as_cod_prodotto )
integer x = 2240
integer y = 160
integer width = 2811
integer height = 2460
integer taborder = 80
boolean bringtotop = true
string title = "none"
string dataobject = "d_curti_ricetta_edit"
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_retrieve(string as_cod_prodotto);string ls_ret,ls_des_prodotto_finito, ls_message
long	ll_ret

accepttext()
if wf_is_edit_modified( ) then
	choose case g_mb.messagebox("CURTI","Ci sono modifiche non salvate alla ricetta: SALVO?", Question!, YesNoCancel!,2)
		case 1
			// salvo la ricetta
			dw_edit_ricetta.update(true,true )
			commit using sqlca;
			
			if iuo_curti.uof_close_file_ricette( ) < 0 then
				g_mb.error("Scrittura ricette: errore in chiusura del file")
				return 
			end if
	
			parent.title = "Ricette Produzione - ATTENDERE, scrittura ricetta in corso ..."
			setpointer(hourglass!)
			
			ll_ret = iuo_curti.uof_scrivi_ricetta(  ref ls_message )
			parent.title = "Ricette Produzione"
			setpointer(arrow!)
			if ll_ret < 0 then
				g_mb.error(ls_message)
			end if
			
			if iuo_curti.uof_open_file_ricette( ls_message) < 0 then
				g_mb.error(ls_message)
			end if
			
		case 3
			return 
	end choose
end if

ll_ret=retrieve(as_cod_prodotto)

ls_ret=dw_edit_ricetta.Modify("cod_variabile_ricetta.Color='0~tIf(cod_variabile_ricetta=~~'Lancio_Cavo_Rec_LenghtCavo~~' or &
cod_variabile_ricetta=~~'Sguaina_Tiro_REC_lungh_sguaina_coda~~' or &
cod_variabile_ricetta=~~'Tiro_cavi_MPP_REC_Lenght_cavo_DX~~' or &
cod_variabile_ricetta=~~'Tiro_cavi_MPP_REC_Lenght_cavo_SX~~' ,255,0)'")

// cambiato colore su richiesta di  Arianna; prima era 16711680

if ls_ret <> "" then
	g_mb.error("Errore durante applicazione colori. $1",ls_ret)
end if

select des_prodotto_finito
into	:ls_des_prodotto_finito
from	curti_ricette
where cod_prodotto_finito=:as_cod_prodotto and cod_variabile_ricetta='LANGID_410';
if sqlca.sqlcode = 100 then
	ls_des_prodotto_finito = "<descrizione non trovata>"
end if

ls_ret = dw_edit_ricetta.Modify("t_titolo.text='" + g_str.format("$1 - $2", as_cod_prodotto,ls_des_prodotto_finito) + "'")
if ls_ret <> "" then
	g_mb.error("Errore durante applicazione titolo. $1",ls_ret)
end if

resetupdate()

end event

event buttonclicked;string ls_message, ls_cod_prodotto, ls_cod_prodotto_new, ls_cod_versione, ls_des_prodotto
long ll_ret, ll_cont
//uo_curti luo_curti
s_distinta_open_parm s_distinta

choose case dwo.name
	case "b_distinta"
		s_distinta.cod_prodotto_padre=dw_ricette_trovate.getitemstring(dw_ricette_trovate.getrow(),1)
		
		select cod_versione
		into	:ls_cod_versione
		from	distinta_padri
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :s_distinta.cod_prodotto_padre and
				flag_predefinita = 'S';
		if sqlca.sqlcode = 100 then
			g_mb.warning("Non trovo la distinta base predefinita del prodotto " + s_distinta.cod_prodotto_padre)
			return
		end if
		
		s_distinta.cod_versione = ls_cod_versione
		window_open_parm(w_distinta_base, -1, s_distinta)
		
	case "b_read"
		// leggo il file ricetta
		dw_edit_ricetta.accepttext()
		if wf_is_edit_modified( ) then
			if g_mb.messagebox("CURTI","Ci sono modifiche non salvate alla ricetta: SALVO?", Question!, YesNo!,2) = 1 then
				// salvo la ricetta
				dw_edit_ricetta.update(true,true )
				commit using sqlca;
				
				if iuo_curti.uof_close_file_ricette( ) < 0 then
					g_mb.error("Scrittura ricette: errore in chiusura del file")
					return 
				end if
		
				parent.title = "Ricette Produzione - ATTENDERE, scrittura ricetta in corso ..."
				setpointer(hourglass!)
				
				ll_ret = iuo_curti.uof_scrivi_ricetta(  ref ls_message )
				parent.title = "Ricette Produzione"
				setpointer(arrow!)
				if ll_ret < 0 then
					g_mb.error(ls_message)
				end if
				
				if iuo_curti.uof_open_file_ricette( ls_message) < 0 then
					g_mb.error(ls_message)
				end if
			end if
		end if
		
		parent.title = "Ricette Produzione - ATTENDERE, caricamento ricetta in corso ..."
		setpointer(hourglass!)
		ll_ret = iuo_curti.uof_leggi_ricetta(  ref ls_message )
		setpointer(Arrow!)
		if ll_ret < 0 then
			g_mb.error(ls_message)
		end if
		
		// ricarico i dati nella DW.
		dw_ricette_trovate.event ue_retrieve( )
		parent.title = "Ricette Produzione - Pronto"

	case "b_save"
		// salvo i dati del prodotto corrente e poi salvo il file
		dw_edit_ricetta.update(true,true )
		commit using sqlca;
		
		if iuo_curti.uof_close_file_ricette( ) < 0 then
			g_mb.error("Scrittura ricette: errore in chiusura del file")
			return 
		end if

		parent.title = "Ricette Produzione - ATTENDERE, scrittura ricetta in corso ..."
		setpointer(hourglass!)
		ll_ret = iuo_curti.uof_scrivi_ricetta(  ref ls_message )
		parent.title = "Ricette Produzione"
		setpointer(arrow!)
		if ll_ret < 0 then
			g_mb.error(ls_message)
		end if
		
		if iuo_curti.uof_open_file_ricette( ls_message) < 0 then
			g_mb.error(ls_message)
		end if

	case "b_saveas"
		// duplica un prodotto finito su un altro.
		// chiedo il codice prodotto di destinazione
		open(w_curti_ricerca_ricette_ricerca_prod)
		if message.stringparm = "Annullato" or len(message.stringparm) < 1 then
			return
		else
			ls_cod_prodotto_new = message.stringparm
		end if
		
		ls_cod_prodotto = dw_ricette_trovate.getitemstring(dw_ricette_trovate.getrow(),1)
		
		if not g_mb.confirm( g_str.format("Sei sicuro di vole duplicare la ricetta del prodotto $1, creando il nuovo prodotto $2 ?", ls_cod_prodotto, ls_cod_prodotto_new), 2) then return

		// Per prima cosa verifico che il prodotto di destinazione non sia già presente e se si chiedo conferma
		select 	count(*)
		into		:ll_cont
		from		curti_ricette
		where	cod_prodotto_finito = :ls_cod_prodotto_new;
		
		if ll_cont > 0 then
			// se il prodotto esiste già chiedo se lo devo sovrascrivere
			if not g_mb.confirm( g_str.format("ATTENZIONE: il prodotto di destinazione $1 esiste già: lo sovrascrivo?", ls_cod_prodotto), 2) then return
			
			// cancello vecchio prodotto
			delete from	curti_ricette
			where	cod_prodotto_finito = :ls_cod_prodotto_new;
			if sqlca.sqlcode <> 0 then
				g_mb.error( g_str.format("Errore in cancellazione ricetta del prodotto $1.~r~n$2", ls_cod_prodotto_new, sqlca.sqlerrtext ))
				rollback;
				return
			end if
		end if
		
		// a questo punto posso procedere con INSERT nuova ricetta
		
		select max(sequenza)
		into	:ll_cont
		from	curti_ricette
		where num_riga_variabile = 5;
		
		if ll_cont = 0 then 
			ll_cont = 1
		else
			ll_cont++
		end if
		
		INSERT INTO curti_ricette  
				( cod_prodotto_finito,   
				  cod_variabile_ricetta,   
				  valore_variabile,   
				  formato,   
				  sequenza,   
				  des_prodotto_finito,   
				  num_riga_variabile)  
		  SELECT :ls_cod_prodotto_new,   
					cod_variabile_ricetta,   
					valore_variabile,   
					formato,   
					:ll_cont,   
					des_prodotto_finito,   
					num_riga_variabile
			 FROM curti_ricette  
			WHERE curti_ricette.cod_prodotto_finito = :ls_cod_prodotto   ;
			if sqlca.sqlcode <> 0 then
				g_mb.error( g_str.format("Errore in inserimento nuova ricetta del prodotto $1.~r~n$2", ls_cod_prodotto_new, sqlca.sqlerrtext ))
				rollback;
				return
			end if
			
			update curti_ricette
			set valore_variabile = :ll_cont
			where num_riga_variabile = 5 and cod_prodotto_finito = :ls_cod_prodotto_new;
			if sqlca.sqlcode <> 0 then
				g_mb.error( g_str.format("Errore in inserimento nuova ricetta del prodotto $1.~r~n$2", ls_cod_prodotto_new, sqlca.sqlerrtext ))
				rollback;
				return
			end if
			
			ls_des_prodotto = ls_cod_prodotto_new + " -->>> NUOVO"
		
			update curti_ricette
			set des_prodotto_finito = :ls_des_prodotto
			where num_riga_variabile = 3 and cod_prodotto_finito = :ls_cod_prodotto_new;
			if sqlca.sqlcode <> 0 then
				g_mb.error( g_str.format("Errore in inserimento nuova ricetta del prodotto $1.~r~n$2", ls_cod_prodotto_new, sqlca.sqlerrtext ))
				rollback;
				return
			end if
			
			commit;
			
			// scrivo il file ricette
			if iuo_curti.uof_close_file_ricette( ) < 0 then
				g_mb.error("Scrittura ricette: errore in chiusura del file")
				return 
			end if
	
			parent.title = "Ricette Produzione - ATTENDERE, scrittura ricetta in corso ..."
			setpointer(hourglass!)
			
			ll_ret = iuo_curti.uof_scrivi_ricetta(  ref ls_message )
			parent.title = "Ricette Produzione"
			setpointer(arrow!)
			if ll_ret < 0 then
				g_mb.error(ls_message)
			end if
			
			if iuo_curti.uof_open_file_ricette( ls_message) < 0 then
				g_mb.error(ls_message)
			end if

			// refresh datawindow
			dw_ricette_trovate.event ue_retrieve()

end choose
end event

type dw_elenco_prodotti_stat_prod from uo_std_dw within w_curti_ricerca_ricette
event ue_retrieve_report ( )
event ue_genera_pdf ( )
event ue_1 ( )
integer x = 46
integer y = 160
integer width = 5006
integer height = 2480
integer taborder = 240
string dataobject = "d_curti_prodotti_stat_prod_st"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_retrieve_report();integer li_risposta,li_stato_fase, li_i, li_ret
long 		ll_riga,ll_i,ll_num_prodotti, l_error, ll_anno_commessa, ll_num_commessa, ll_righe, ll_y, ll_anno_ord_ven, ll_num_ord_ven, ll_prog_riga_ord_ven, ll_pos, ll_cont_var, ll_progr_produzione, ll_sum_produzione
string 	ls_cod_operaio, ls_cod_reparto_scelto,ls_cod_lavorazione,ls_cod_prodotto,ls_cod_prodotto_finito,ls_cod_versione, &
		 	ls_cod_reparto,ls_cod_versione_scelta,ls_sintassi, ls_errore, ls_sintax, & 
		 	ls_tipo_analisi, ls_stat_1, ls_stat_2, ls_stat_3, ls_stat_4, ls_stat_5, ls_stat_6, ls_stat_7, ls_stat_8, ls_stat_9, ls_stat_10, &
		 	ls_stat_1_1, ls_stat_2_2, ls_stat_3_3, ls_stat_4_4, ls_stat_5_5, ls_stat_6_6, ls_stat_7_7, ls_stat_8_8, ls_stat_9_9, ls_stat_10_10, &
		 	ls_flag_evasione, ls_sql, ls_c_1, ls_c_2, ls_c_3, ls_c_4, ls_c_5, ls_c_6, ls_c_7, ls_c_8, ls_c_9, ls_c_10, ls_d_1,ls_d_2,ls_d_3, &
		 	ls_d_4,ls_d_5,ls_d_6,ls_d_7,ls_d_8,ls_d_9,ls_d_10, ls_t_an, ls_sel_reparto[5], ls_sezione_cavo, ls_tipo_cavo, ls_sql1,&
			ls_flag_attiva_ricerca_cod_prodotto, ls_flag_attiva_ricerca_ricette,ls_cod_mp, ls_path_risorse, ls_flag_tipo_avanz_commessa
dec{4} 	ld_quan_ordine, ld_quan_evasa, ld_quan_prodotta_reparto, ld_quan_mp, ld_lunghezza_cavo_inizio,ld_lunghezza_cavo_fine,ld_sguainatura_inizio, &
			ld_sguainatura_fine,ld_tiro_cavi_dx_inizio,ld_tiro_cavi_dx_fine,ld_tiro_cavi_sx_inizio,ld_tiro_cavi_sx_fine

datetime ldt_da_data,ldt_a_data, ldt_data_consegna
datastore lds_ricerca, lds_ricette
uo_funzioni_1 luo_funz1


setpointer (hourglass!)

// intervallo data di consegna obbligatoria!!
dw_ricerca_ricette.accepttext()
luo_funz1 = create uo_funzioni_1 

ls_path_risorse = s_cs_xx.volume + s_cs_xx.risorse
ldt_da_data = dw_ricerca_ricette.getitemdatetime(1,"data_inizio")
ldt_a_data = dw_ricerca_ricette.getitemdatetime(1,"data_fine")

if isnull(ldt_da_data) or isnull(ldt_a_data) or ldt_da_data = datetime(date("01/01/1900"),00:00:00) or ldt_a_data = datetime(date("01/01/1900"),00:00:00) then
	g_mb.messagebox("Sep","Attenzione! Selezionare L'intervallo della data di consegna!",stopsign!)
	return
end if

ls_tipo_analisi = dw_ricerca_ricette.getitemstring(1,"cod_tipo_analisi")

if isnull(ls_tipo_analisi) or ls_tipo_analisi = "" then
	g_mb.messagebox("Sep","Attenzione! Selezionare il tipo di analisi!",stopsign!)
	return	
end if

ls_stat_1 = dw_ricerca_ricette.getitemstring(1,"cod_stat_1")
ls_stat_2 = dw_ricerca_ricette.getitemstring(1,"cod_stat_2")
ls_stat_3 = dw_ricerca_ricette.getitemstring(1,"cod_stat_3")
ls_stat_4 = dw_ricerca_ricette.getitemstring(1,"cod_stat_4")
ls_stat_5 = dw_ricerca_ricette.getitemstring(1,"cod_stat_5")
ls_stat_6 = dw_ricerca_ricette.getitemstring(1,"cod_stat_6")
ls_stat_7 = dw_ricerca_ricette.getitemstring(1,"cod_stat_7")
ls_stat_8 = dw_ricerca_ricette.getitemstring(1,"cod_stat_8")
ls_stat_9 = dw_ricerca_ricette.getitemstring(1,"cod_stat_9")
ls_stat_10 = dw_ricerca_ricette.getitemstring(1,"cod_stat_10")

ls_stat_1_1 = dw_ricerca_ricette.getitemstring(1,"cod_stat_11")
ls_stat_2_2 = dw_ricerca_ricette.getitemstring(1,"cod_stat_22")
ls_stat_3_3 = dw_ricerca_ricette.getitemstring(1,"cod_stat_33")
ls_stat_4_4 = dw_ricerca_ricette.getitemstring(1,"cod_stat_44")
ls_stat_5_5 = dw_ricerca_ricette.getitemstring(1,"cod_stat_55")
ls_stat_6_6 = dw_ricerca_ricette.getitemstring(1,"cod_stat_66")
ls_stat_7_7 = dw_ricerca_ricette.getitemstring(1,"cod_stat_77")
ls_stat_8_8 = dw_ricerca_ricette.getitemstring(1,"cod_stat_88")
ls_stat_9_9 = dw_ricerca_ricette.getitemstring(1,"cod_stat_99")
ls_stat_10_10 = dw_ricerca_ricette.getitemstring(1,"cod_stat_1010")

ls_sel_reparto[1] = dw_selezione.getitemstring(1,"as_reparto_1")
ls_sel_reparto[2] = dw_selezione.getitemstring(1,"as_reparto_2")
ls_sel_reparto[3] = dw_selezione.getitemstring(1,"as_reparto_3")
ls_sel_reparto[4] = dw_selezione.getitemstring(1,"as_reparto_4")
ls_sel_reparto[5] = dw_selezione.getitemstring(1,"as_reparto_5")

ls_flag_attiva_ricerca_cod_prodotto = dw_ricerca_ricette.getitemstring(1,"flag_attiva_ricerca_cod_prodotto")
if ls_flag_attiva_ricerca_cod_prodotto = "S" then
	ls_sezione_cavo = dw_ricerca_ricette.getitemstring(1,"al_sezione_cavo")
	ls_tipo_cavo = dw_ricerca_ricette.getitemstring(1,"al_tipo_cavo")
else
	setnull(ls_sezione_cavo)
	setnull(ls_tipo_cavo)
end if

ls_flag_attiva_ricerca_ricette = dw_ricerca_ricette.getitemstring(1,"flag_attiva_ricerca_ricette")
if ls_flag_attiva_ricerca_ricette = "S" then
	ld_lunghezza_cavo_inizio = dw_ricerca_ricette.getitemnumber(1,"lunghezza_cavo_inizio") * 10
	ld_lunghezza_cavo_fine = dw_ricerca_ricette.getitemnumber(1,"lunghezza_cavo_fine") * 10
	ld_sguainatura_inizio = dw_ricerca_ricette.getitemnumber(1,"sguainatura_inizio") * 10
	ld_sguainatura_fine = dw_ricerca_ricette.getitemnumber(1,"sguainatura_fine") *10
	ld_tiro_cavi_dx_inizio = dw_ricerca_ricette.getitemnumber(1,"tiro_cavi_dx_inizio") * 10
	ld_tiro_cavi_dx_fine = dw_ricerca_ricette.getitemnumber(1,"tiro_cavi_dx_fine") *10
	ld_tiro_cavi_sx_inizio = dw_ricerca_ricette.getitemnumber(1,"tiro_cavi_sx_inizio") *10
	ld_tiro_cavi_sx_fine = dw_ricerca_ricette.getitemnumber(1,"tiro_cavi_sx_fine") *10
else
	ld_lunghezza_cavo_inizio = 0
	ld_lunghezza_cavo_fine = 0
	ld_sguainatura_inizio = 0
	ld_sguainatura_fine = 0
	ld_tiro_cavi_dx_inizio = 0
	ld_tiro_cavi_dx_fine = 0
	ld_tiro_cavi_sx_inizio = 0
	ld_tiro_cavi_sx_fine = 0
end if

idt_da_data = ldt_da_data
idt_a_data = ldt_a_data
dw_elenco_prodotti_stat.triggerevent("pcd_retrieve")

// carico la seconda dw!!
// ***** carico gli ordini in base alla azienda e all'intervallo della consegna
dw_elenco_prodotti_stat_prod.reset()


dw_elenco_prodotti_stat_prod.Object.des_statistico_1_t.Text = is_1
dw_elenco_prodotti_stat_prod.Object.des_statistico_2_t.Text = is_2
dw_elenco_prodotti_stat_prod.Object.des_statistico_3_t.Text = is_3
dw_elenco_prodotti_stat_prod.Object.des_statistico_4_t.Text = is_4
dw_elenco_prodotti_stat_prod.Object.des_statistico_5_t.Text = is_5
dw_elenco_prodotti_stat_prod.Object.des_statistico_6_t.Text = is_6
dw_elenco_prodotti_stat_prod.Object.reparto_1.Text = ""
dw_elenco_prodotti_stat_prod.Object.reparto_2.Text = ""
dw_elenco_prodotti_stat_prod.Object.reparto_3.Text = ""
dw_elenco_prodotti_stat_prod.Object.reparto_4.Text = ""
dw_elenco_prodotti_stat_prod.Object.reparto_5.Text = ""

for ll_y = 1 to 5
	if not isnull(ls_sel_reparto[ll_y]) then 
		dw_elenco_prodotti_stat_prod.modify("reparto_"+string(ll_y)+".Text = '" + ls_sel_reparto[ll_y] + "'")
	else
		dw_elenco_prodotti_stat_prod.modify("reparto_"+string(ll_y)+".Text = ''")
	end if
next
DECLARE cu_prodotti_stat DYNAMIC CURSOR FOR SQLSA ;

//Donato 13/10/2008 ------------------------------------------------------------------
//Modifica per filtrare in base allo stato dell'ordine

ls_sintax = 	"SELECT det_ord_ven.anno_registrazione," + &
			"det_ord_ven.num_registrazione," + &
			"det_ord_ven.cod_prodotto," + &
			"det_ord_ven.quan_ordine," + &
			"det_ord_ven.data_consegna," + &
			"det_ord_ven.quan_evasa," + &
			"det_ord_ven.flag_evasione," + &
			"det_ord_ven.num_commessa," + &
			"anag_clienti.rag_soc_1, " + &
			"det_ord_ven.anno_commessa," + &
			"det_ord_ven.cod_versione," + &
			"det_ord_ven.prog_riga_ord_ven," + &
			"anag_commesse.flag_tipo_avanzamento" + &
	" FROM det_ord_ven " + &
	" LEFT OUTER JOIN tes_ord_ven ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda "+&
				"AND det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione "+&
				"AND det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione "+&
	" LEFT OUTER JOIN anag_clienti ON anag_clienti.cod_azienda = tes_ord_ven.cod_azienda "+&
				"AND anag_clienti.cod_cliente = tes_ord_ven.cod_cliente " + &
	" LEFT OUTER JOIN anag_commesse ON det_ord_ven.cod_azienda = anag_commesse.cod_azienda AND det_ord_ven.anno_commessa = anag_commesse.anno_commessa AND det_ord_ven.num_commessa = anag_commesse.num_commessa " + &
	" WHERE det_ord_ven.cod_azienda = '"+ s_cs_xx.cod_azienda+ "' " + &
			" AND det_ord_ven.data_consegna >= '"+string(ldt_da_data, s_cs_xx.db_funzioni.formato_data)+"' " + &
			" AND det_ord_ven.data_consegna <= '"+string(ldt_a_data, s_cs_xx.db_funzioni.formato_data)+"' "
			
ls_flag_evasione = dw_ricerca_ricette.getitemstring(1,"flag_evasione")
choose case ls_flag_evasione
	case "A"
		ls_sintax += " AND det_ord_ven.flag_evasione='A'  "
		
	case "P"
		ls_sintax += " AND det_ord_ven.flag_evasione in ('P','A') "
		
	case "EVASO"
		ls_sintax += " AND det_ord_ven.flag_evasione='E'  "
		
end choose

li_risposta = guo_functions.uof_crea_datastore( lds_ricerca, ls_sintax, ls_errore)

if li_risposta = -1 then
	destroy lds_ricerca;
	g_mb.messagebox("SEP",g_str.format("Errore nel caricamento dei dati! $1", ls_errore) )
	return		
end if

if li_risposta > 0 then
	
	ll_i = 0
	ll_righe = 0
	for ll_i = 1 to lds_ricerca.rowcount()
		
		ls_cod_prodotto 		= lds_ricerca.getitemstring(ll_i,3)
		ll_anno_commessa 	= lds_ricerca.getitemnumber(ll_i,10)
		ll_num_commessa 	= lds_ricerca.getitemnumber(ll_i,8)

		ls_cod_versione 		= lds_ricerca.getitemstring(ll_i,11)
		ll_anno_ord_ven		= lds_ricerca.getitemnumber(ll_i,1)
		ll_num_ord_ven		= lds_ricerca.getitemnumber(ll_i,2)
		ll_prog_riga_ord_ven = lds_ricerca.getitemnumber(ll_i,12)
		ld_quan_ordine			= lds_ricerca.getitemnumber(ll_i,4)
		ls_flag_evasione		= lds_ricerca.getitemstring(ll_i,7)
		ls_flag_tipo_avanz_commessa = lds_ricerca.getitemstring(ll_i,13)
		
		// riga evasa
		if ls_flag_evasione = "E" then continue
		
		// commessa chiusa
		if ls_flag_tipo_avanz_commessa = "7" or ls_flag_tipo_avanz_commessa = "8" or ls_flag_tipo_avanz_commessa = "9" then continue
		

		if ls_flag_attiva_ricerca_cod_prodotto = "S" then
			// cerco in disinta la variante del cavo
			ls_cod_mp = ""
			li_ret = luo_funz1.uof_ricerca_quan_mp(ls_cod_prodotto, ls_cod_versione, ld_quan_ordine, "9CA", ll_anno_ord_ven, ll_num_ord_ven, ll_prog_riga_ord_ven, "varianti_det_ord_ven", ls_cod_mp, ld_quan_mp, ls_errore)
	
			if li_ret < 0 then
				g_mb.error(g_str.format("Errore nella funzione uof_ricerca_quan_mp. $1", sqlca.sqlerrtext))
				rollback;
				exit
			end if		
			
			if li_ret =1 then continue  // salta riga ordine
			
			if li_ret = 0 then  /// ho trovato qualcosa
				// verifico se ci siamo con la sezione del cavo
				if not isnull(ls_sezione_cavo) and len(ls_sezione_cavo) > 0 then
					if upper(ls_sezione_cavo) <> upper(mid(ls_cod_mp,4,4)) then continue		// salta riga ordine
				end if
				if not isnull(ls_tipo_cavo) and len(ls_tipo_cavo) > 0 then
					ll_pos = pos(ls_cod_mp,"/")
					if ll_pos = 0 then  // non ho trovato il carattere "/" quindi provo a vedere se c'è il tipo cavo
						ll_pos = pos( upper(ls_cod_mp), upper(ls_tipo_cavo))
						if ll_pos = 0 then continue 		// salto perchè non trovato il tipo cavo richiesto
					else	
						if upper(ls_tipo_cavo) <> mid(ls_cod_mp,8, ll_pos - 8) then continue // salto perchè non trovato il tipo cavo richiesto
					end if
				end if
			end if
		end if
		
		if ls_flag_attiva_ricerca_ricette = "S" then
			
			ls_sql1 = ""
			ll_cont_var = 0
			if ld_lunghezza_cavo_inizio > 0 or ld_lunghezza_cavo_fine > 0 then
				ll_cont_var++
				ls_sql1 += g_str.format("  (cod_variabile_ricetta = '$1' ","Lancio_Cavo_Rec_LenghtCavo")
				if ld_lunghezza_cavo_inizio > 0 then ls_sql1 += g_str.format(" and valore_variabile >= '$1' ",long(ld_lunghezza_cavo_inizio))
				if ld_lunghezza_cavo_fine > 0 then ls_sql1 += g_str.format(" and valore_variabile <= '$1' ",long(ld_lunghezza_cavo_fine))
				ls_sql1 += " ) "
			end if
			
			if ld_sguainatura_inizio > 0 or ld_sguainatura_fine > 0 then
				ll_cont_var++
				if len(ls_sql1) > 0 then ls_sql1 += " or "
				ls_sql1 += g_str.format(" (cod_variabile_ricetta = '$1' ","Sguaina_Tiro_REC_lungh_sguaina_coda")
				if ld_sguainatura_inizio > 0 then ls_sql1 += g_str.format(" and valore_variabile >= '$1' ",long(ld_sguainatura_inizio))
				if ld_sguainatura_fine > 0 then ls_sql1 += g_str.format(" and valore_variabile <= '$1' ",long(ld_sguainatura_fine))
				ls_sql1 += " ) "
			end if
			
			if ld_tiro_cavi_dx_inizio > 0 or ld_tiro_cavi_dx_fine > 0 then
				ll_cont_var++
				if len(ls_sql1) > 0 then ls_sql1 += " or "
				ls_sql1 += g_str.format(" (cod_variabile_ricetta = '$1' ","Tiro_cavi_MPP_REC_Lenght_cavo_DX")
				if ld_tiro_cavi_dx_inizio > 0 then ls_sql1 += g_str.format(" and valore_variabile >= '$1' ",long(ld_tiro_cavi_dx_inizio))
				if ld_tiro_cavi_dx_fine > 0 then ls_sql1 += g_str.format(" and valore_variabile <= '$1' ",long(ld_tiro_cavi_dx_fine))
				ls_sql1 += " ) "
			end if
			
			if ld_tiro_cavi_sx_inizio > 0 or ld_tiro_cavi_sx_fine > 0 then
				ll_cont_var++
				if len(ls_sql1) > 0 then ls_sql1 += " or "
				ls_sql1 += g_str.format(" (cod_variabile_ricetta = '$1' ","Tiro_cavi_MPP_REC_Lenght_cavo_SX")
				if ld_tiro_cavi_sx_inizio > 0 then ls_sql1 += g_str.format(" and valore_variabile >= '$1' ",long(ld_tiro_cavi_sx_inizio))
				if ld_tiro_cavi_sx_fine > 0 then ls_sql1 += g_str.format(" and valore_variabile <= '$1' ",long(ld_tiro_cavi_sx_fine))
				ls_sql1 += " ) "
			end if
			
			if len(ls_sql1) > 0 then	// è stata caricata qualche variabile per la ricerca nelle ricette
				ls_sql1 = g_str.format("select cod_prodotto_finito, cod_variabile_ricetta, valore_variabile, sequenza, num_riga_variabile from curti_ricette where cod_prodotto_finito = '$1' and ( $2 )", ls_cod_prodotto, ls_sql1)
				li_ret = guo_functions.uof_crea_datastore( lds_ricette, ls_sql1, ls_errore)
				if li_ret = 0 then continue 		// nessun risultato, salto
				if li_ret < 0 then					// errore
					g_mb.error(g_str.format("Errore nella creazione del datastore per ricerca variabili ricette. $1",ls_errore))
					rollback;
					exit
				end if		
				
				// se ho controllato 4 variabili, mi spetto 4 risultati; quindi testo la variabile ll_cont_var con il risultato della query
				if li_ret <> ll_cont_var then continue
			end if
		end if
			
		dw_ricerca_ricette.object.t_message.text = g_str.format("Commessa $1-$2 Prodotto $3",ll_anno_commessa,ll_num_commessa,ls_cod_prodotto)
		Yield()
		
		if ls_cod_prodotto <> "" and not isnull(ls_cod_prodotto) then
			
			ls_sql = "select cod_tipo_analisi,cod_statistico_1,cod_statistico_2, cod_statistico_3, " + &
			          " cod_statistico_4, cod_statistico_5, cod_statistico_6, cod_statistico_7, " + &
						 " cod_statistico_8, cod_statistico_9, cod_statistico_10 " + &
						 " from tab_stat_prodotti "  + &
						 " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + ls_cod_prodotto + "' "
		
			if ls_tipo_analisi <> "" then
				ls_sql = ls_sql + " and cod_tipo_analisi = '" + ls_tipo_analisi + "' "
				if ls_stat_1_1 <> "" and ls_stat_1 <> "" then
					ls_sql = ls_sql + " and cod_statistico_1 between '" + ls_stat_1 + "' and '" + ls_stat_1_1 + "' "
				end if
				
				if ls_stat_2_2 <> "" and ls_stat_2 <> "" then
					ls_sql = ls_sql + " and cod_statistico_2 between '" + ls_stat_2 + "' and '" + ls_stat_2_2 + "' "
				end if
				
				if ls_stat_3_3 <> "" and ls_stat_3 <> "" then
					ls_sql = ls_sql + " and cod_statistico_3 between '" + ls_stat_3 + "' and '" + ls_stat_3_3 + "' "
				end if
				
				if ls_stat_4_4 <> "" and ls_stat_4 <> "" then
					ls_sql = ls_sql + " and cod_statistico_4 between '" + ls_stat_4 + "' and '" + ls_stat_4_4 + "' "
				end if
				
				if ls_stat_5_5 <> "" and ls_stat_5 <> "" then
					ls_sql = ls_sql + " and cod_statistico_5 between '" + ls_stat_5 + "' and '" + ls_stat_5_5 + "' "
				end if
				
				if ls_stat_6_6 <> "" and ls_stat_6 <> "" then
					ls_sql = ls_sql + " and cod_statistico_6 between '" + ls_stat_6 + "' and '" + ls_stat_6_6 + "' "
				end if
			
				if ls_stat_7_7 <> "" and ls_stat_7 <> "" then
					ls_sql = ls_sql + " and cod_statistico_7 between '" + ls_stat_7 + "' and '" + ls_stat_7_7 + "' "
				end if
				
				if ls_stat_8_8 <> "" and ls_stat_8 <> "" then
					ls_sql = ls_sql + " and cod_statistico_8 between '" + ls_stat_8 + "' and '" + ls_stat_8_8 + "' "
				end if
				
				if ls_stat_9_9 <> "" and ls_stat_9 <> "" then
					ls_sql = ls_sql + " and cod_statistico_9 between '" + ls_stat_9 + "' and '" + ls_stat_9_9 + "' "
				end if
				
				if ls_stat_10_10 <> "" and ls_stat_10 <> "" then
					ls_sql = ls_sql + " and cod_statistico_10 between '" + ls_stat_10 + "' and '" + ls_stat_10_10 + "' "
				end if		
		
			end if		
			ls_sql = ls_sql + " order by cod_statistico_1, cod_statistico_2, cod_statistico_3 "
			
		
			PREPARE SQLSA FROM :ls_sql;
			OPEN DYNAMIC cu_prodotti_stat;
			
			do while true
				FETCH cu_prodotti_stat INTO 	:ls_t_an,
													 	:ls_c_1,
				                            				:ls_c_2,
													 	:ls_c_3,
													 	:ls_c_4,
													 	:ls_c_5,
													 	:ls_c_6,
													 	:ls_c_7,
													 	:ls_c_8,
													 	:ls_c_9,
													 	:ls_c_10;
				if sqlca.sqlcode < 0 then
					g_mb.error(g_str.format("Errore cursore cu_prodotti_stat. $1", sqlca.sqlerrtext))
					close cu_prodotti_stat;
					rollback;
					exit
				end if
				if sqlca.sqlcode = 100 then exit
				
				Yield()
				
				ll_righe = dw_elenco_prodotti_stat_prod.insertrow(ll_righe)
				dw_elenco_prodotti_stat_prod.SetItem(ll_righe,"cod_prodotto",ls_cod_prodotto)
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"rag_soc_1",lds_ricerca.getitemstring(ll_i,9))
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"quan_ordine",lds_ricerca.getitemdecimal(ll_i,4))
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"quan_evasa",lds_ricerca.getitemdecimal(ll_i,6))
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"flag_evasione",lds_ricerca.getitemstring(ll_i,7))
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"num_commessa",ll_num_commessa)
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"anno_commessa",ll_anno_commessa)
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"path_risorse",ls_path_risorse)
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_1",ls_c_1)
	
				select des_statistico_2
				into   :ls_d_2
				from   tab_statistica_2
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_2 = :ls_c_2;
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_2",ls_d_2)
				
				select des_statistico_3
				into   :ls_d_3
				from   tab_statistica_3
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_3 = :ls_c_3;
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_3",ls_d_3)
				
				select des_statistico_4
				into   :ls_d_4
				from   tab_statistica_4
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_4 = :ls_c_4;
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_4",ls_d_4)
				
				select des_statistico_5
				into   :ls_d_5
				from   tab_statistica_5
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_5 = :ls_c_5;
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_5",ls_d_5)
				
				select des_statistico_6
				into   :ls_d_6
				from   tab_statistica_6
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_6 = :ls_c_6;
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"des_statistico_6",ls_d_6)
				
				select des_statistico_7
				into   :ls_d_7
				from   tab_statistica_7
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_tipo_analisi = :ls_t_an
				and    cod_statistico_7 = :ls_c_7;
				
				
				dw_elenco_prodotti_stat_prod.setitem(ll_righe,"data_consegna",lds_ricerca.getitemdatetime(ll_i,5))			
				ll_sum_produzione = 0
				for ll_y = 1 to 5
					if isnull(ls_sel_reparto[ll_y]) or len(ls_sel_reparto[ll_y]) < 1 then exit
					
					select sum(quan_prodotta), sum(progr_produzione)
					into	:ld_quan_prodotta_reparto, :ll_progr_produzione
					from	avan_produzione_com
					where cod_azienda = :s_cs_xx.cod_azienda and
							anno_commessa = :ll_anno_commessa and
							num_commessa = :ll_num_commessa and
							cod_reparto = :ls_sel_reparto[ll_y];
							
					if isnull(ld_quan_prodotta_reparto) then ld_quan_prodotta_reparto = 0
					ll_sum_produzione += ll_progr_produzione
					
					dw_elenco_prodotti_stat_prod.setitem(ll_righe,"quan_reparto_" + string(ll_y),ld_quan_prodotta_reparto)
					if ll_sum_produzione > 0 then dw_elenco_prodotti_stat_prod.setitem(ll_righe,"flag_programmata","S")
					dw_ricerca_ricette.object.t_message.text = g_str.format("$1  Reparto $2  Quan-prodotta $3)",dw_ricerca_ricette.object.t_message.text, ls_sel_reparto[ll_y], ld_quan_prodotta_reparto)
					Yield()
				next
				
				ll_righe = ll_righe + 1
			loop

			CLOSE cu_prodotti_stat;
		
		end if		
	next
	
end if
dw_elenco_prodotti_stat_prod.setfocus()
dw_elenco_prodotti_stat_prod.SetSort("des_statistico_1 A ,des_statistico_2 A, des_statistico_3 A, data_consegna A ")

dw_elenco_prodotti_stat_prod.Sort()
setpointer (arrow!)

dw_ricerca_ricette.object.t_message.text = "Pronto!"
dw_folder.fu_selecttab( 2 )
dw_ricerca_ricette.object.b_pdf.enabled='1'
dw_ricerca_ricette.object.b_stampa.enabled='1'


return 
end event

event ue_genera_pdf();string ls_path
long ll_ret
uo_shellexecute luo_exec

ls_path = guo_functions.uof_get_user_documents_folder( ) + guo_functions.uof_get_random_filename( ) + ".pdf"
dw_elenco_prodotti_stat_prod.saveas(  ls_path , PDF!, true)
g_mb.show( "Il file è stato salvato nella cartella DOCUMENTI dell'utente.~r~n" + ls_path)

end event

event doubleclicked;if dwo.name="cod_prodotto" then
	if row > 0 then
		dw_ricette_trovate.event ue_retrieve_one(getitemstring(row,"cod_prodotto"))
	end if
	return 
end if

if right(dwo.name,2) = "_t" then
	// ordinamento
	string ls_str
	long ll_len
	ls_str = dwo.name
	ll_len=len(ls_str) 
	ls_str = left(ls_str, ll_len - 2)
	setsort( ls_str + " A")
	sort()
	return
end if		

call super :: doubleclicked


end event

event buttonclicked;call super::buttonclicked;if dwo.name="b_sposta" then
	string ls_cod_prodotto, ls_errore
	long 	ll_rows, ll_i, ll_anno_commessa, ll_num_commessa, ll_prog
	
	
	ls_cod_prodotto = dw_elenco_prodotti_stat_prod.getitemstring(row, "cod_prodotto")
	ll_anno_commessa = dw_elenco_prodotti_stat_prod.getitemnumber(row,"anno_commessa")
	ll_num_commessa = dw_elenco_prodotti_stat_prod.getitemnumber(row, "num_commessa")
	

	select count(progr_produzione)
	into	:ll_prog
	from avan_produzione_com
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa and
			num_commessa = :ll_num_commessa and 
			progr_produzione > 0;
	if ll_prog > 0 and not isnull(ll_prog) then
		g_mb.show("Commessa già inserita nel programma di produzione")
		return 
	end if
	
	select max(progr_produzione)
	into	:ll_prog
	from	avan_produzione_com;
	
	if isnull(ll_prog) then ll_prog = 10
	ll_prog += 10
	
	update avan_produzione_com
	set progr_produzione = :ll_prog
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :ll_anno_commessa and
			num_commessa = :ll_num_commessa;
	if sqlca.sqlcode < 0 then
		g_mb.error(g_str.format("Errore in impostazione sequenza produzione. ~r~n$1", sqlca.sqlerrtext))
		rollback;
		return
	end if
	
	commit;
	
	tv_1.uof_carica_tv(  )
	
	dw_elenco_prodotti_stat_prod.setredraw(false)
	dw_elenco_prodotti_stat_prod.setitem(row, "flag_programmata","S")
	dw_elenco_prodotti_stat_prod.resetupdate( )
	dw_elenco_prodotti_stat_prod.setredraw(true)
	
end if	
tv_1.uof_carica_tv( )
end event

type dw_folder from u_folder within w_curti_ricerca_ricette
integer x = 23
integer y = 20
integer width = 5051
integer height = 2640
integer taborder = 250
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Ricette Corrisp."
      dw_ricette_trovate.event ue_retrieve()
   CASE "Avanzamento"
      dw_avanzamento.event ue_retrieve()
END CHOOSE



end event

type dw_avanzamento from uo_std_dw within w_curti_ricerca_ricette
event ue_retrieve ( )
integer x = 46
integer y = 320
integer width = 4983
integer height = 2300
integer taborder = 80
string dataobject = "d_curti_monitor_avanzamento"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_retrieve();string		ls_reparti, ls_sql, ls_cod_reparto
long		ll_ret

settransobject(sqlca)

ls_reparti = dw_dddw_lista_reparti.uof_get_selected_column( "cod_reparto",true)
ls_cod_reparto = dw_avanzamento_sel.getitemstring(1,"cod_reparto")

//dw_dddw_lista_reparti.uof_get_selected_column_as_array( "cod_reparto", ls_cod_reparto[], true)
if isnull(ls_reparti) and isnull(ls_cod_reparto) then return

ls_sql = "select distinct A.cod_azienda , A.progr_produzione, A.anno_commessa, A.num_commessa, A.flag_fine_fase, A.cod_reparto, C.cod_prodotto, P.des_prodotto, D.anno_registrazione, D.num_registrazione, D.prog_riga_ord_ven, D.data_consegna, CL.rag_soc_1 &
from avan_produzione_com A &
join anag_commesse C on A.cod_azienda = C.cod_azienda and A.Anno_commessa=C.anno_commessa and A.num_commessa=C.num_commessa &
join anag_prodotti P on C.cod_azienda=P.cod_azienda and C.cod_prodotto=P.cod_prodotto &
left join det_ord_ven D on D.cod_azienda=C.cod_azienda and D.anno_commessa=C.anno_commessa and D.num_commessa=C.num_commessa &
left join tes_ord_ven T on T.cod_azienda=D.cod_azienda and T.anno_registrazione=D.anno_registrazione and T.num_registrazione=D.num_registrazione &
left join anag_clienti CL on CL.cod_azienda=T.cod_azienda and CL.cod_cliente = T.cod_cliente &
where A.progr_produzione > 0 &
group by A.cod_azienda, A.progr_produzione, A.anno_commessa, A.num_commessa, A.flag_fine_fase, A.cod_reparto,C.cod_prodotto,P.des_prodotto, D.anno_registrazione, D.num_registrazione, D.prog_riga_ord_ven, D.data_consegna, CL.rag_soc_1 &
having A.cod_azienda='$1' and A.flag_fine_fase='N' $2 &
order by A.progr_produzione, A.anno_commessa, A.num_commessa, A.cod_reparto "


//ls_reparti = "'RAGGAB','RTC','RCOL','RCOL','RTS.PR','RSTAMP','RAGLB'"

if not isnull(ls_reparti) then
	// ho selezionato tutto
	ls_reparti = "and A.cod_reparto in " + mid( ls_reparti, pos(ls_reparti,"(") )
	ls_sql = g_str.format(ls_sql, s_cs_xx.cod_azienda, ls_reparti)
else
	ls_sql = g_str.format(ls_sql, s_cs_xx.cod_azienda, "")
end if

ll_ret = setsqlselect(ls_sql)

ll_ret = retrieve(s_cs_xx.cod_azienda, ls_reparti)
end event

type dw_ricette_trovate from datawindow within w_curti_ricerca_ricette
event ue_retrieve ( )
event ue_retrieve_one ( string as_cod_prodotto )
integer x = 46
integer y = 160
integer width = 2194
integer height = 2460
integer taborder = 70
boolean bringtotop = true
string title = "none"
string dataobject = "d_curti_ricette_find"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_retrieve();string		ls_sql, ls_sql1, ls_errore,ls_flag_attiva_ricerca_ricette
long		ll_cont_var, ll_ret, ll_i
dec{4} 	ld_lunghezza_cavo_inizio, ld_lunghezza_cavo_fine, ld_sguainatura_inizio,ld_sguainatura_fine, ld_tiro_cavi_dx_inizio, ld_tiro_cavi_dx_fine, &
			ld_tiro_cavi_sx_inizio, ld_tiro_cavi_sx_fine
datastore lds_ricette


if not ib_retrieve then return
dw_ricerca_ricette.accepttext()

ls_flag_attiva_ricerca_ricette = dw_ricerca_ricette.getitemstring(1,"flag_attiva_ricerca_ricette")
if ls_flag_attiva_ricerca_ricette = "S" then
	ld_lunghezza_cavo_inizio = dw_ricerca_ricette.getitemnumber(1,"lunghezza_cavo_inizio") * 10
	ld_lunghezza_cavo_fine = dw_ricerca_ricette.getitemnumber(1,"lunghezza_cavo_fine") * 10
	ld_sguainatura_inizio = dw_ricerca_ricette.getitemnumber(1,"sguainatura_inizio") * 10
	ld_sguainatura_fine = dw_ricerca_ricette.getitemnumber(1,"sguainatura_fine") *10
	ld_tiro_cavi_dx_inizio = dw_ricerca_ricette.getitemnumber(1,"tiro_cavi_dx_inizio") * 10
	ld_tiro_cavi_dx_fine = dw_ricerca_ricette.getitemnumber(1,"tiro_cavi_dx_fine") *10
	ld_tiro_cavi_sx_inizio = dw_ricerca_ricette.getitemnumber(1,"tiro_cavi_sx_inizio") *10
	ld_tiro_cavi_sx_fine = dw_ricerca_ricette.getitemnumber(1,"tiro_cavi_sx_fine") *10

	
	ls_sql1 = ""
	ll_cont_var = 0
	if ld_lunghezza_cavo_inizio > 0 or ld_lunghezza_cavo_fine > 0 then
		ll_cont_var++
		ls_sql1 += g_str.format("  (A.cod_variabile_ricetta = '$1' ","Lancio_Cavo_Rec_LenghtCavo")
		if ld_lunghezza_cavo_inizio > 0 then ls_sql1 += g_str.format(" and A.valore_variabile >= '$1' ",long(ld_lunghezza_cavo_inizio))
		if ld_lunghezza_cavo_fine > 0 then ls_sql1 += g_str.format(" and A.valore_variabile <= '$1' ",long(ld_lunghezza_cavo_fine))
		ls_sql1 += " ) "
	end if
	
	if ld_sguainatura_inizio > 0 or ld_sguainatura_fine > 0 then
		ll_cont_var++
		if len(ls_sql1) > 0 then ls_sql1 += " or "
		ls_sql1 += g_str.format(" (A.cod_variabile_ricetta = '$1' ","Sguaina_Tiro_REC_lungh_sguaina_coda")
		if ld_sguainatura_inizio > 0 then ls_sql1 += g_str.format(" and A.valore_variabile >= '$1' ",long(ld_sguainatura_inizio))
		if ld_sguainatura_fine > 0 then ls_sql1 += g_str.format(" and A.valore_variabile <= '$1' ",long(ld_sguainatura_fine))
		ls_sql1 += " ) "
	end if
	
	if ld_tiro_cavi_dx_inizio > 0 or ld_tiro_cavi_dx_fine > 0 then
		ll_cont_var++
		if len(ls_sql1) > 0 then ls_sql1 += " or "
		ls_sql1 += g_str.format(" (A.cod_variabile_ricetta = '$1' ","Tiro_cavi_MPP_REC_Lenght_cavo_DX")
		if ld_tiro_cavi_dx_inizio > 0 then ls_sql1 += g_str.format(" and A.valore_variabile >= '$1' ",long(ld_tiro_cavi_dx_inizio))
		if ld_tiro_cavi_dx_fine > 0 then ls_sql1 += g_str.format(" and A.valore_variabile <= '$1' ",long(ld_tiro_cavi_dx_fine))
		ls_sql1 += " ) "
	end if
	
	if ld_tiro_cavi_sx_inizio > 0 or ld_tiro_cavi_sx_fine > 0 then
		ll_cont_var++
		if len(ls_sql1) > 0 then ls_sql1 += " or "
		ls_sql1 += g_str.format(" (A.cod_variabile_ricetta = '$1' ","Tiro_cavi_MPP_REC_Lenght_cavo_SX")
		if ld_tiro_cavi_sx_inizio > 0 then ls_sql1 += g_str.format(" and A.valore_variabile >= '$1' ",long(ld_tiro_cavi_sx_inizio))
		if ld_tiro_cavi_sx_fine > 0 then ls_sql1 += g_str.format(" and A.valore_variabile <= '$1' ",long(ld_tiro_cavi_sx_fine))
		ls_sql1 += " ) "
	end if
end if

ls_sql = "select distinct A.cod_prodotto_finito, C.des_prodotto_finito, B.des_prodotto from curti_ricette A left outer join anag_prodotti B on A.cod_prodotto_finito=B.cod_prodotto left outer join curti_ricette C on A.cod_prodotto_finito=C.cod_prodotto_finito and C.cod_variabile_ricetta='LANGID_410'  "

if len(ls_sql1) > 0 then	// è stata caricata qualche variabile per la ricerca nelle ricette
	ls_sql = g_str.format(ls_sql + " where $1 ", ls_sql1)
end if

ls_sql +=  " order by 1 "
	
ll_ret = guo_functions.uof_crea_datastore( lds_ricette, ls_sql, ls_errore)
if ll_ret < 0 then					// errore
	g_mb.error(g_str.format("Errore nella creazione del datastore per ricerca variabili ricette. $1",ls_errore))
	return
end if		

this.reset()
setredraw(false)

for ll_i = 1 to ll_ret
	insertrow(0)
	setitem(ll_i, 1, lds_ricette.getitemstring(ll_i,1) )
	setitem(ll_i, 3, lds_ricette.getitemstring(ll_i,2) )
	setitem(ll_i, 2, lds_ricette.getitemstring(ll_i,3) )
next

destroy lds_ricette
setredraw(true)

return
end event

event ue_retrieve_one(string as_cod_prodotto);string		ls_sql, ls_errore
long		ll_ret, ll_i
datastore lds_ricette

ib_retrieve=false
dw_folder.fu_selecttab(3)
ib_retrieve=true

ls_sql = g_str.format("select distinct A.cod_prodotto_finito, C.des_prodotto_finito, B.des_prodotto from curti_ricette A left outer join anag_prodotti B on A.cod_prodotto_finito=B.cod_prodotto left outer join curti_ricette C on A.cod_prodotto_finito=C.cod_prodotto_finito and C.cod_variabile_ricetta='LANGID_410'  where A.cod_prodotto_finito = '$1' order by 1 ", as_cod_prodotto)
ll_ret = guo_functions.uof_crea_datastore( lds_ricette, ls_sql, ls_errore)
if ll_ret < 0 then					// errore
	g_mb.error(g_str.format("Errore nella creazione del datastore per ricerca variabili ricette. $1",ls_errore))
	return
end if		
	
this.reset()
dw_edit_ricetta.reset()

if ll_ret = 0 then return
	
for ll_i = 1 to ll_ret
	insertrow(0)
	setitem(ll_i, 1, lds_ricette.getitemstring(ll_i,1) )
	setitem(ll_i, 3, lds_ricette.getitemstring(ll_i,2) )
	setitem(ll_i, 2, lds_ricette.getitemstring(ll_i,3) )
next

destroy lds_ricette

this.scrolltorow(1)

dw_edit_ricetta.event ue_retrieve(as_cod_prodotto)


return
end event

event clicked;if row > 0 then
	string ls_cod_prodotto
	
	ls_cod_prodotto = getitemstring(row, 1)
	dw_edit_ricetta.event ue_retrieve(ls_cod_prodotto)
end if
end event

type dw_avanzamento_sel from datawindow within w_curti_ricerca_ricette
integer x = 46
integer y = 140
integer width = 5006
integer height = 160
integer taborder = 270
boolean bringtotop = true
string title = "none"
string dataobject = "d_curti_monitor_avanzamento_sel"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;choose  case dwo.name
	case "b_refresh" 
		dw_avanzamento.reset()
		dw_avanzamento.event ue_retrieve()
	case "b_memo_filtro"
		dw_avanzamento_sel.accepttext()
		wf_memorizza_filtro()
end choose
end event

event clicked;choose case dwo.name
	case "cod_reparto"
		dw_dddw_lista_reparti.show()
		
end choose
end event

type dw_elenco_prodotti_stat from uo_cs_xx_dw within w_curti_ricerca_ricette
event pcd_retrieve pbm_custom60
integer x = 2217
integer y = 1860
integer width = 1966
integer height = 680
boolean bringtotop = true
string title = "Prodotti Privi di Codici Statistici"
string dataobject = "d_prodotti_stat"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve();long l_error


l_error = dw_elenco_prodotti_stat.retrieve(s_cs_xx.cod_azienda,idt_da_data,idt_a_data)


IF l_error < 0 THEN
   PCCA.Error = c_Fatal
	return
END IF
end event

type dw_selezione from uo_std_dw within w_curti_ricerca_ricette
integer x = 46
integer y = 1860
integer width = 2171
integer height = 680
integer taborder = 240
boolean bringtotop = true
string dataobject = "d_report_stat_produzione_selezione"
boolean border = false
borderstyle borderstyle = stylebox!
end type

type dw_dddw_lista_reparti from uo_dddw_checkbox within w_curti_ricerca_ricette
integer x = 2537
integer y = 200
integer taborder = 90
boolean bringtotop = true
end type

type dw_ricerca_ricette from datawindow within w_curti_ricerca_ricette
event ue_set_dddw ( )
integer x = 46
integer y = 160
integer width = 4137
integer height = 1700
integer taborder = 60
boolean bringtotop = true
string title = "none"
string dataobject = "d_curti_ricerca_ricette"
boolean border = false
boolean livescroll = true
end type

event ue_set_dddw();string ls_cod_tipo_analisi, ls_label[]
long ll_i 

ls_cod_tipo_analisi = getitemstring(1,"cod_tipo_analisi")

select label_livello_1,
       label_livello_2,
   	 label_livello_3,
		 label_livello_4,
		 label_livello_5,
		 label_livello_6,
		 label_livello_7,
		 label_livello_8,
		 label_livello_9,
		 label_livello_10
into   :ls_label[1],
		 :ls_label[2],
		 :ls_label[3],
		 :ls_label[4],
		 :ls_label[5],
		 :ls_label[6],
		 :ls_label[7],
		 :ls_label[8],
		 :ls_label[9],
		 :ls_label[10]
from   tab_tipi_analisi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_analisi=:ls_cod_tipo_analisi;

is_1 = ls_label[1]
is_2 = ls_label[2]
is_3 = ls_label[3]
is_4 = ls_label[4]
is_5 = ls_label[5]
is_6 = ls_label[6]
is_7 = ls_label[7]
is_8 = ls_label[8]
is_9 = ls_label[9]
is_10 = ls_label[10]

dw_elenco_prodotti_stat_prod.reset()
dw_elenco_prodotti_stat_prod.Modify("des_statistico_1_t.Text='" + is_1 + "'")
dw_elenco_prodotti_stat_prod.Object.des_statistico_2_t.Text = is_2
dw_elenco_prodotti_stat_prod.Object.des_statistico_3_t.Text = is_3
dw_elenco_prodotti_stat_prod.Object.des_statistico_4_t.Text = is_4
dw_elenco_prodotti_stat_prod.Object.des_statistico_5_t.Text = is_5
dw_elenco_prodotti_stat_prod.Object.des_statistico_6_t.Text = is_6

for ll_i = 1 to 10
	modify(g_str.format("cod_stat_$1_t.text='$2'",ll_i,ls_label[ll_i]))
	
	f_po_loaddddw_dw(	this, &
								g_str.format("cod_stat_$1",ll_i),&
								sqlca, &
								g_str.format("tab_statistica_$1",ll_i), &
								g_str.format("cod_statistico_$1",ll_i), &
								g_str.format("des_statistico_$1",ll_i), &
								"cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'")
	
	f_po_loaddddw_dw(this, &
				g_str.format("cod_stat_$1$1",ll_i),&
				sqlca, &
				g_str.format("tab_statistica_$1",ll_i), &
				g_str.format("cod_statistico_$1",ll_i), &
				g_str.format("des_statistico_$1",ll_i), &
				"cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_analisi ='" + ls_cod_tipo_analisi +"'")
	
next

parent.triggerevent("pc_retrieve")
end event

event itemchanged;choose case dwo.name
	case  "cod_tipo_analisi"
		event post ue_set_dddw( )
	case "al_sezione_cavo"
		if len(data) <> 0 and len(data) <> 4 then
			g_mb.error("La sezione del cavo DEVE essere di 4 caratteri")
			return 1
		end if
	case else
		parent.triggerevent("pc_retrieve")		
end choose

end event

event buttonclicked;choose case dwo.name
	case "b_annulla"
		reset()
		insertrow(0)
		dw_ricerca_ricette.object.b_pdf.enabled='0'
		dw_ricerca_ricette.object.b_stampa.enabled='0'		
	case "b_report"
		dw_elenco_prodotti_stat_prod.event ue_retrieve_report()
	case "b_stampa"
		dw_elenco_prodotti_stat_prod.print()
	case "b_pdf"
		dw_elenco_prodotti_stat_prod.event ue_genera_pdf()
end choose
			
end event

