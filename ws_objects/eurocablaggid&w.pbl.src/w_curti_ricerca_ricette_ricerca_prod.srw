﻿$PBExportHeader$w_curti_ricerca_ricette_ricerca_prod.srw
forward
global type w_curti_ricerca_ricette_ricerca_prod from w_std_principale
end type
type dw_1 from uo_std_dw within w_curti_ricerca_ricette_ricerca_prod
end type
end forward

global type w_curti_ricerca_ricette_ricerca_prod from w_std_principale
integer width = 2482
integer height = 412
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
dw_1 dw_1
end type
global w_curti_ricerca_ricette_ricerca_prod w_curti_ricerca_ricette_ricerca_prod

on w_curti_ricerca_ricette_ricerca_prod.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_curti_ricerca_ricette_ricerca_prod.destroy
call super::destroy
destroy(this.dw_1)
end on

event open;call super::open;dw_1.insertrow(0)
end event

type dw_1 from uo_std_dw within w_curti_ricerca_ricette_ricerca_prod
integer x = 23
integer y = 20
integer width = 2446
integer height = 320
integer taborder = 10
string dataobject = "d_curti_ricetta_edit_prodotto"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_1,"cod_prodotto")
		
	case "b_conferma"
		string		ls_cod_prodotto, ls_str
		accepttext()
		
		ls_cod_prodotto = dw_1.getitemstring(1,"cod_prodotto")
		
		select cod_prodotto
		into	:ls_str
		from	anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.error("Il prodotto non esiste!")
			return
		end if
		
		CloseWithReturn ( parent, dw_1.getitemstring(1,"cod_prodotto") )
		
	case "b_annulla"
		CloseWithReturn ( parent, "Annullato" )
		close(parent)
		
end choose
end event

