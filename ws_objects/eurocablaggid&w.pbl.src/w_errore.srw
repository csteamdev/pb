﻿$PBExportHeader$w_errore.srw
forward
global type w_errore from w_cs_xx_principale
end type
type tab_1 from tab within w_errore
end type
type tabpage_attuale from userobject within tab_1
end type
type dw_5 from datawindow within tabpage_attuale
end type
type cb_update from commandbutton within tabpage_attuale
end type
type dw_2 from datawindow within tabpage_attuale
end type
type dw_1 from datawindow within tabpage_attuale
end type
type tabpage_attuale from userobject within tab_1
dw_5 dw_5
cb_update cb_update
dw_2 dw_2
dw_1 dw_1
end type
type tabpage_old from userobject within tab_1
end type
type dw_6 from datawindow within tabpage_old
end type
type dw_4 from datawindow within tabpage_old
end type
type dw_3 from datawindow within tabpage_old
end type
type tabpage_old from userobject within tab_1
dw_6 dw_6
dw_4 dw_4
dw_3 dw_3
end type
type tab_1 from tab within w_errore
tabpage_attuale tabpage_attuale
tabpage_old tabpage_old
end type
type cb_2 from commandbutton within w_errore
end type
type em_numero from editmask within w_errore
end type
type st_2 from statictext within w_errore
end type
type st_1 from statictext within w_errore
end type
type cb_1 from commandbutton within w_errore
end type
type em_anno from editmask within w_errore
end type
end forward

global type w_errore from w_cs_xx_principale
integer width = 5093
integer height = 2848
tab_1 tab_1
cb_2 cb_2
em_numero em_numero
st_2 st_2
st_1 st_1
cb_1 cb_1
em_anno em_anno
end type
global w_errore w_errore

type variables
transaction sqlcb
end variables

on w_errore.create
int iCurrent
call super::create
this.tab_1=create tab_1
this.cb_2=create cb_2
this.em_numero=create em_numero
this.st_2=create st_2
this.st_1=create st_1
this.cb_1=create cb_1
this.em_anno=create em_anno
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.em_numero
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.cb_1
this.Control[iCurrent+7]=this.em_anno
end on

on w_errore.destroy
call super::destroy
destroy(this.tab_1)
destroy(this.cb_2)
destroy(this.em_numero)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.em_anno)
end on

type tab_1 from tab within w_errore
integer y = 180
integer width = 5029
integer height = 2500
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_attuale tabpage_attuale
tabpage_old tabpage_old
end type

on tab_1.create
this.tabpage_attuale=create tabpage_attuale
this.tabpage_old=create tabpage_old
this.Control[]={this.tabpage_attuale,&
this.tabpage_old}
end on

on tab_1.destroy
destroy(this.tabpage_attuale)
destroy(this.tabpage_old)
end on

type tabpage_attuale from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4992
integer height = 2372
long backcolor = 12632256
string text = "Attuale"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_5 dw_5
cb_update cb_update
dw_2 dw_2
dw_1 dw_1
end type

on tabpage_attuale.create
this.dw_5=create dw_5
this.cb_update=create cb_update
this.dw_2=create dw_2
this.dw_1=create dw_1
this.Control[]={this.dw_5,&
this.cb_update,&
this.dw_2,&
this.dw_1}
end on

on tabpage_attuale.destroy
destroy(this.dw_5)
destroy(this.cb_update)
destroy(this.dw_2)
destroy(this.dw_1)
end on

type dw_5 from datawindow within tabpage_attuale
integer x = 5
integer y = 8
integer width = 4983
integer height = 360
integer taborder = 50
string title = "none"
string dataobject = "d_errore_0"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_update from commandbutton within tabpage_attuale
integer x = 1787
integer y = 2248
integer width = 1417
integer height = 100
integer taborder = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "AGGIORNA ATTUALE DAL PRECEDENTE"
end type

event clicked;long ll_anno_commessa, ll_num_commessa

ll_anno_commessa = long(em_anno.text)
ll_num_commessa = long(em_numero.text)

delete avan_produzione_com
where cod_azienda='A01' and anno_commessa=:ll_anno_commessa and num_commessa=:ll_num_commessa;
if sqlca.sqlcode < 0 then
	g_mb.error(sqlca.sqlerrtext)
	rollback;
	return
end if

delete det_orari_produzione
where cod_azienda='A01' and anno_commessa=:ll_anno_commessa and num_commessa=:ll_num_commessa;
if sqlca.sqlcode < 0 then
	g_mb.error(sqlca.sqlerrtext)
	rollback;
	return
end if

commit;

tab_1.tabpage_old.dw_3.RowsCopy(1,tab_1.tabpage_old.dw_3.RowCount(), Primary!, tab_1.tabpage_attuale.dw_1, 1, Primary!)
tab_1.tabpage_old.dw_4.RowsCopy(1,tab_1.tabpage_old.dw_4.RowCount(), Primary!, tab_1.tabpage_attuale.dw_2, 1, Primary!)
tab_1.tabpage_old.dw_6.RowsCopy(1,tab_1.tabpage_old.dw_6.RowCount(), Primary!, tab_1.tabpage_attuale.dw_5, 1, Primary!)

if dw_5.update() = -1 then 
	rollback;
	return
end if
if dw_1.update() = -1 then 
	rollback;
	return
end if
if dw_2.update() = -1 then 
	rollback;
	return
end if

commit using sqlca;


end event

type dw_2 from datawindow within tabpage_attuale
integer x = 5
integer y = 928
integer width = 4983
integer height = 1300
integer taborder = 60
string title = "none"
string dataobject = "d_errore_2"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from datawindow within tabpage_attuale
integer x = 5
integer y = 388
integer width = 4983
integer height = 540
integer taborder = 50
string title = "none"
string dataobject = "d_errore_1"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_old from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4992
integer height = 2372
long backcolor = 12632256
string text = "Precedente"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_6 dw_6
dw_4 dw_4
dw_3 dw_3
end type

on tabpage_old.create
this.dw_6=create dw_6
this.dw_4=create dw_4
this.dw_3=create dw_3
this.Control[]={this.dw_6,&
this.dw_4,&
this.dw_3}
end on

on tabpage_old.destroy
destroy(this.dw_6)
destroy(this.dw_4)
destroy(this.dw_3)
end on

type dw_6 from datawindow within tabpage_old
integer x = 27
integer y = 8
integer width = 4937
integer height = 360
integer taborder = 60
string title = "none"
string dataobject = "d_errore_0"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_4 from datawindow within tabpage_old
integer x = 27
integer y = 1028
integer width = 4937
integer height = 1320
integer taborder = 70
string title = "none"
string dataobject = "d_errore_2"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_3 from datawindow within tabpage_old
integer x = 27
integer y = 388
integer width = 4937
integer height = 620
integer taborder = 50
string title = "none"
string dataobject = "d_errore_1"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_2 from commandbutton within w_errore
integer x = 2469
integer y = 20
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "carica dati"
end type

event clicked;tab_1.tabpage_attuale.dw_1.settransobject(sqlca)
tab_1.tabpage_attuale.dw_2.settransobject(sqlca)
tab_1.tabpage_attuale.dw_5.settransobject(sqlca)
tab_1.tabpage_attuale.dw_1.retrieve( long(em_anno.text),long(em_numero.text)  )
tab_1.tabpage_attuale.dw_2.retrieve( long(em_anno.text),long(em_numero.text)  )
tab_1.tabpage_attuale.dw_5.retrieve( long(em_anno.text),long(em_numero.text)  )

tab_1.tabpage_old.dw_3.settransobject(sqlcb)
tab_1.tabpage_old.dw_4.settransobject(sqlcb)
tab_1.tabpage_old.dw_6.settransobject(sqlcb)
tab_1.tabpage_old.dw_3.retrieve( long(em_anno.text),long(em_numero.text)  )
tab_1.tabpage_old.dw_4.retrieve( long(em_anno.text),long(em_numero.text)  )
tab_1.tabpage_old.dw_6.retrieve( long(em_anno.text),long(em_numero.text)  )


end event

type em_numero from editmask within w_errore
integer x = 1920
integer y = 40
integer width = 343
integer height = 80
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "2018"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "######"
end type

type st_2 from statictext within w_errore
integer x = 1623
integer y = 60
integer width = 274
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "NUMERO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_errore
integer x = 1097
integer y = 60
integer width = 229
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "ANNO"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_errore
integer x = 23
integer y = 20
integer width = 617
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "connetti al db vecchio"
end type

event clicked;string ls_logpass, ls_option, ls_reg_chiave_root
int li_risposta, ll_ret

sqlcb = create transaction

sqlcb.dbms="ODBC"
sqlcb.dbparm="Connectstring='DSN=cs_euro_old'"

connect using sqlcb;
if sqlcb.sqlcode=0 then
	g_mb.show("Connesso al DB OLD")
else
	g_mb.show(sqlcb.sqlerrtext)
end if

return 0
end event

type em_anno from editmask within w_errore
integer x = 1349
integer y = 40
integer width = 206
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "2018"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "####"
end type

