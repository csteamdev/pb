﻿$PBExportHeader$w_etichette_non_conformita.srw
$PBExportComments$Composizione + Stampa Etichette Merce Non Conforme
forward
global type w_etichette_non_conformita from w_cs_xx_risposta
end type
type dw_etichette_non_conformita from uo_cs_xx_dw within w_etichette_non_conformita
end type
type st_3 from statictext within w_etichette_non_conformita
end type
type st_5 from statictext within w_etichette_non_conformita
end type
type st_prodotto from statictext within w_etichette_non_conformita
end type
type st_fornitore from statictext within w_etichette_non_conformita
end type
type st_bolla_acq from statictext within w_etichette_non_conformita
end type
type st_data_bolla from statictext within w_etichette_non_conformita
end type
type st_15 from statictext within w_etichette_non_conformita
end type
type st_20 from statictext within w_etichette_non_conformita
end type
type st_quantita from statictext within w_etichette_non_conformita
end type
type st_difettosa from statictext within w_etichette_non_conformita
end type
type st_rapporto from statictext within w_etichette_non_conformita
end type
type st_difformita from statictext within w_etichette_non_conformita
end type
type sle_titolo_1 from singlelineedit within w_etichette_non_conformita
end type
type st_1 from statictext within w_etichette_non_conformita
end type
type gb_1 from groupbox within w_etichette_non_conformita
end type
type st_nrcopie from statictext within w_etichette_non_conformita
end type
type cb_stampa from commandbutton within w_etichette_non_conformita
end type
type cb_aggiorna from commandbutton within w_etichette_non_conformita
end type
type sle_nrcopie from singlelineedit within w_etichette_non_conformita
end type
type gb_3 from groupbox within w_etichette_non_conformita
end type
type sle_logo_csteam from singlelineedit within w_etichette_non_conformita
end type
type sle_anno_commessa from singlelineedit within w_etichette_non_conformita
end type
type sle_desc_prodotto from singlelineedit within w_etichette_non_conformita
end type
type sle_fornitore from singlelineedit within w_etichette_non_conformita
end type
type sle_anno_bolla_acq from singlelineedit within w_etichette_non_conformita
end type
type sle_num_bolla_acq from singlelineedit within w_etichette_non_conformita
end type
type sle_num_commessa from singlelineedit within w_etichette_non_conformita
end type
type sle_prodotto from singlelineedit within w_etichette_non_conformita
end type
type sle_quantita from singlelineedit within w_etichette_non_conformita
end type
type sle_difettosa from singlelineedit within w_etichette_non_conformita
end type
type sle_rapporto from singlelineedit within w_etichette_non_conformita
end type
type sle_difformita from singlelineedit within w_etichette_non_conformita
end type
type sle_logo_azienda from singlelineedit within w_etichette_non_conformita
end type
type st_ret_1 from statictext within w_etichette_non_conformita
end type
type st_ret_2 from statictext within w_etichette_non_conformita
end type
type st_ret_3 from statictext within w_etichette_non_conformita
end type
type st_ret_4 from statictext within w_etichette_non_conformita
end type
type sle_ret_2 from singlelineedit within w_etichette_non_conformita
end type
type sle_ret_3 from singlelineedit within w_etichette_non_conformita
end type
type sle_ret_1 from singlelineedit within w_etichette_non_conformita
end type
type sle_ret_4 from singlelineedit within w_etichette_non_conformita
end type
type r_1 from rectangle within w_etichette_non_conformita
end type
end forward

global type w_etichette_non_conformita from w_cs_xx_risposta
integer width = 2578
integer height = 1464
string title = "Stampa Etichette Materiale Non Conforme"
boolean resizable = false
dw_etichette_non_conformita dw_etichette_non_conformita
st_3 st_3
st_5 st_5
st_prodotto st_prodotto
st_fornitore st_fornitore
st_bolla_acq st_bolla_acq
st_data_bolla st_data_bolla
st_15 st_15
st_20 st_20
st_quantita st_quantita
st_difettosa st_difettosa
st_rapporto st_rapporto
st_difformita st_difformita
sle_titolo_1 sle_titolo_1
st_1 st_1
gb_1 gb_1
st_nrcopie st_nrcopie
cb_stampa cb_stampa
cb_aggiorna cb_aggiorna
sle_nrcopie sle_nrcopie
gb_3 gb_3
sle_logo_csteam sle_logo_csteam
sle_anno_commessa sle_anno_commessa
sle_desc_prodotto sle_desc_prodotto
sle_fornitore sle_fornitore
sle_anno_bolla_acq sle_anno_bolla_acq
sle_num_bolla_acq sle_num_bolla_acq
sle_num_commessa sle_num_commessa
sle_prodotto sle_prodotto
sle_quantita sle_quantita
sle_difettosa sle_difettosa
sle_rapporto sle_rapporto
sle_difformita sle_difformita
sle_logo_azienda sle_logo_azienda
st_ret_1 st_ret_1
st_ret_2 st_ret_2
st_ret_3 st_ret_3
st_ret_4 st_ret_4
sle_ret_2 sle_ret_2
sle_ret_3 sle_ret_3
sle_ret_1 sle_ret_1
sle_ret_4 sle_ret_4
r_1 r_1
end type
global w_etichette_non_conformita w_etichette_non_conformita

type variables

end variables

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_etichette_non_conformita.set_dw_key("cod_azienda")
dw_etichette_non_conformita.set_dw_options(sqlca,pcca.null_object,c_nonew+c_nomodify+c_nodelete,c_default)

sle_nrcopie.text = "0001"

end on

on w_etichette_non_conformita.create
int iCurrent
call super::create
this.dw_etichette_non_conformita=create dw_etichette_non_conformita
this.st_3=create st_3
this.st_5=create st_5
this.st_prodotto=create st_prodotto
this.st_fornitore=create st_fornitore
this.st_bolla_acq=create st_bolla_acq
this.st_data_bolla=create st_data_bolla
this.st_15=create st_15
this.st_20=create st_20
this.st_quantita=create st_quantita
this.st_difettosa=create st_difettosa
this.st_rapporto=create st_rapporto
this.st_difformita=create st_difformita
this.sle_titolo_1=create sle_titolo_1
this.st_1=create st_1
this.gb_1=create gb_1
this.st_nrcopie=create st_nrcopie
this.cb_stampa=create cb_stampa
this.cb_aggiorna=create cb_aggiorna
this.sle_nrcopie=create sle_nrcopie
this.gb_3=create gb_3
this.sle_logo_csteam=create sle_logo_csteam
this.sle_anno_commessa=create sle_anno_commessa
this.sle_desc_prodotto=create sle_desc_prodotto
this.sle_fornitore=create sle_fornitore
this.sle_anno_bolla_acq=create sle_anno_bolla_acq
this.sle_num_bolla_acq=create sle_num_bolla_acq
this.sle_num_commessa=create sle_num_commessa
this.sle_prodotto=create sle_prodotto
this.sle_quantita=create sle_quantita
this.sle_difettosa=create sle_difettosa
this.sle_rapporto=create sle_rapporto
this.sle_difformita=create sle_difformita
this.sle_logo_azienda=create sle_logo_azienda
this.st_ret_1=create st_ret_1
this.st_ret_2=create st_ret_2
this.st_ret_3=create st_ret_3
this.st_ret_4=create st_ret_4
this.sle_ret_2=create sle_ret_2
this.sle_ret_3=create sle_ret_3
this.sle_ret_1=create sle_ret_1
this.sle_ret_4=create sle_ret_4
this.r_1=create r_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_etichette_non_conformita
this.Control[iCurrent+2]=this.st_3
this.Control[iCurrent+3]=this.st_5
this.Control[iCurrent+4]=this.st_prodotto
this.Control[iCurrent+5]=this.st_fornitore
this.Control[iCurrent+6]=this.st_bolla_acq
this.Control[iCurrent+7]=this.st_data_bolla
this.Control[iCurrent+8]=this.st_15
this.Control[iCurrent+9]=this.st_20
this.Control[iCurrent+10]=this.st_quantita
this.Control[iCurrent+11]=this.st_difettosa
this.Control[iCurrent+12]=this.st_rapporto
this.Control[iCurrent+13]=this.st_difformita
this.Control[iCurrent+14]=this.sle_titolo_1
this.Control[iCurrent+15]=this.st_1
this.Control[iCurrent+16]=this.gb_1
this.Control[iCurrent+17]=this.st_nrcopie
this.Control[iCurrent+18]=this.cb_stampa
this.Control[iCurrent+19]=this.cb_aggiorna
this.Control[iCurrent+20]=this.sle_nrcopie
this.Control[iCurrent+21]=this.gb_3
this.Control[iCurrent+22]=this.sle_logo_csteam
this.Control[iCurrent+23]=this.sle_anno_commessa
this.Control[iCurrent+24]=this.sle_desc_prodotto
this.Control[iCurrent+25]=this.sle_fornitore
this.Control[iCurrent+26]=this.sle_anno_bolla_acq
this.Control[iCurrent+27]=this.sle_num_bolla_acq
this.Control[iCurrent+28]=this.sle_num_commessa
this.Control[iCurrent+29]=this.sle_prodotto
this.Control[iCurrent+30]=this.sle_quantita
this.Control[iCurrent+31]=this.sle_difettosa
this.Control[iCurrent+32]=this.sle_rapporto
this.Control[iCurrent+33]=this.sle_difformita
this.Control[iCurrent+34]=this.sle_logo_azienda
this.Control[iCurrent+35]=this.st_ret_1
this.Control[iCurrent+36]=this.st_ret_2
this.Control[iCurrent+37]=this.st_ret_3
this.Control[iCurrent+38]=this.st_ret_4
this.Control[iCurrent+39]=this.sle_ret_2
this.Control[iCurrent+40]=this.sle_ret_3
this.Control[iCurrent+41]=this.sle_ret_1
this.Control[iCurrent+42]=this.sle_ret_4
this.Control[iCurrent+43]=this.r_1
end on

on w_etichette_non_conformita.destroy
call super::destroy
destroy(this.dw_etichette_non_conformita)
destroy(this.st_3)
destroy(this.st_5)
destroy(this.st_prodotto)
destroy(this.st_fornitore)
destroy(this.st_bolla_acq)
destroy(this.st_data_bolla)
destroy(this.st_15)
destroy(this.st_20)
destroy(this.st_quantita)
destroy(this.st_difettosa)
destroy(this.st_rapporto)
destroy(this.st_difformita)
destroy(this.sle_titolo_1)
destroy(this.st_1)
destroy(this.gb_1)
destroy(this.st_nrcopie)
destroy(this.cb_stampa)
destroy(this.cb_aggiorna)
destroy(this.sle_nrcopie)
destroy(this.gb_3)
destroy(this.sle_logo_csteam)
destroy(this.sle_anno_commessa)
destroy(this.sle_desc_prodotto)
destroy(this.sle_fornitore)
destroy(this.sle_anno_bolla_acq)
destroy(this.sle_num_bolla_acq)
destroy(this.sle_num_commessa)
destroy(this.sle_prodotto)
destroy(this.sle_quantita)
destroy(this.sle_difettosa)
destroy(this.sle_rapporto)
destroy(this.sle_difformita)
destroy(this.sle_logo_azienda)
destroy(this.st_ret_1)
destroy(this.st_ret_2)
destroy(this.st_ret_3)
destroy(this.st_ret_4)
destroy(this.sle_ret_2)
destroy(this.sle_ret_3)
destroy(this.sle_ret_1)
destroy(this.sle_ret_4)
destroy(this.r_1)
end on

type dw_etichette_non_conformita from uo_cs_xx_dw within w_etichette_non_conformita
integer x = 1303
integer y = 100
integer width = 1143
integer height = 196
integer taborder = 0
string dataobject = "d_etichette_non_conformita"
boolean border = false
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_d_1, s_cs_xx.parametri.parametro_d_2)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
   

END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type st_3 from statictext within w_etichette_non_conformita
integer x = 46
integer y = 140
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Titolo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_5 from statictext within w_etichette_non_conformita
integer x = 46
integer y = 240
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Anno Commessa:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_prodotto from statictext within w_etichette_non_conformita
integer x = 46
integer y = 640
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Fornitore:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_fornitore from statictext within w_etichette_non_conformita
integer x = 46
integer y = 540
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Descr.Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_bolla_acq from statictext within w_etichette_non_conformita
integer x = 46
integer y = 740
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Anno Bolla:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_data_bolla from statictext within w_etichette_non_conformita
integer x = 46
integer y = 840
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Numero Bolla:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_15 from statictext within w_etichette_non_conformita
integer x = 46
integer y = 340
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nr Commessa:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_20 from statictext within w_etichette_non_conformita
integer x = 46
integer y = 440
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_quantita from statictext within w_etichette_non_conformita
integer x = 46
integer y = 940
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Quantità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_difettosa from statictext within w_etichette_non_conformita
integer x = 46
integer y = 1040
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Difettosa:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_rapporto from statictext within w_etichette_non_conformita
integer x = 46
integer y = 1140
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Rapporto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_difformita from statictext within w_etichette_non_conformita
integer x = 46
integer y = 1240
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Difformità:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_titolo_1 from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 140
integer width = 709
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_etichette_non_conformita
integer x = 46
integer y = 40
integer width = 480
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Azienda:"
alignment alignment = right!
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_etichette_non_conformita
integer x = 1280
integer y = 40
integer width = 1189
integer height = 260
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Riferimento Registrazione Accettazione"
end type

type st_nrcopie from statictext within w_etichette_non_conformita
integer x = 1417
integer y = 400
integer width = 498
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nr Copie Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_stampa from commandbutton within w_etichette_non_conformita
integer x = 1943
integer y = 500
integer width = 503
integer height = 100
integer taborder = 190
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa Etichetta"
end type

event clicked;string stringa, ls_errore,ls_com,ls_default
integer li_ritorno,li_risposta

setpointer(hourglass!)
li_ritorno = g_mb.messagebox("Stampa Etichette","Conferma Stampa Etichette",question!,YesNo!)

if li_ritorno <> 1 then RETURN

li_risposta = Registryget(s_cs_xx.chiave_root + "profilocorrente", "numero", ls_default)

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + ls_default, "com", ls_com)


w_cs_xx_mdi.setmicrohelp("Stampa Etichette su Stampante Termica in Corso ..... ")

// ------------------------------ STAMPA SU TERMICA ---------------------------------------

stringa = char(2) + "L" + char(13)

stringa = stringa + "111100000100200" + sle_logo_csteam.text + char(13)

stringa = stringa + "221100005600395" + sle_logo_azienda.text + char(13)
stringa = stringa + "231100005700355" + sle_titolo_1.text + char(13)

stringa = stringa + "221100005500330" + "CODICE " + sle_prodotto.text  + char(13)
stringa = stringa + "221100002300330" + "COMM " + sle_anno_commessa.text + "-" + sle_num_commessa.text  + char(13)

stringa = stringa + "221100005400300" + sle_desc_prodotto.text + char(13)

stringa = stringa + "221100005500270" + "FORNITORE " + sle_fornitore.text + char(13)
stringa = stringa + "221100002800270" + "BOLLA " + sle_anno_bolla_acq.text + "-" + sle_num_bolla_acq.text  + char(13)

stringa = stringa + "221100005500240" + "QUANTITA'" + sle_quantita.text + "  %DIFETT " + sle_difettosa.text + "%   LOTTO:" + char(13)

stringa = stringa + "221100005500210" + "RAPPORTO N.C." + sle_rapporto.text + char(13)

stringa = stringa + "221100005500175" + "MOTIVO :" + sle_difformita.text + char(13)

if len(sle_ret_1.text) > 0 then stringa = stringa + "211200005700130" + sle_ret_1.text + char(13)
if len(sle_ret_2.text) > 0 then stringa = stringa + "211200005700100" + sle_ret_2.text + char(13)
if len(sle_ret_3.text) > 0 then stringa = stringa + "211200005700060" + sle_ret_3.text + char(13)
if len(sle_ret_4.text) > 0 then stringa = stringa + "211200005700030" + sle_ret_4.text + char(13)

stringa = stringa + "Q" + left(sle_nrcopie.text,4) + char(13)
stringa = stringa + "E" + char(13)

uo_serial_communication luo_serial_communication

luo_serial_communication = create uo_serial_communication

if luo_serial_communication.uof_write_com ( stringa, ls_com, 9600,ls_errore) = -1 then
	g_mb.messagebox("OMNIA",ls_errore)
	return
end if

destroy luo_serial_communication


w_cs_xx_mdi.setmicrohelp("Pronto !")

g_mb.messagebox("Stampa Etichette","Dati trasferiti a Stampante Termica",information!)

setpointer(arrow!)
end event

type cb_aggiorna from commandbutton within w_etichette_non_conformita
integer x = 1303
integer y = 500
integer width = 503
integer height = 100
integer taborder = 200
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna Campi"
end type

on clicked;string ls_str_1, ls_str_2
long   ll_num, ll_con, ll_quan, ll_perc
// ---------------------  imposto campi etichetta ------------------------------------------

sle_logo_csteam.text = "Consulting&Software"

select aziende.rag_soc_1
into   :ls_str_1
from aziende
where aziende.cod_azienda = :s_cs_xx.cod_azienda ;

sle_logo_azienda.text = ls_str_1

sle_titolo_1.text ="MATERIALE NON CONFORME"
sle_rapporto.text= string(dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "anno_non_conf") ) + "-" + string( dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "num_non_conf" ) )

sle_prodotto.text = dw_etichette_non_conformita.getitemstring(dw_etichette_non_conformita.getrow(), "cod_prodotto") 
ls_str_2 = sle_prodotto.text

SELECT anag_prodotti.des_prodotto
INTO :ls_str_1
FROM anag_prodotti
WHERE (anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda) and (anag_prodotti.cod_prodotto = :ls_str_2) ;
sle_desc_prodotto.text = ls_str_1

sle_fornitore.text = dw_etichette_non_conformita.getitemstring(dw_etichette_non_conformita.getrow(), "cod_fornitore")
sle_anno_bolla_acq.text= string(dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "anno_bolla_acq"))
sle_num_bolla_acq.text = string(dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "num_bolla_acq" ) )

sle_fornitore.text = dw_etichette_non_conformita.getitemstring(dw_etichette_non_conformita.getrow(), "cod_fornitore")

sle_quantita.text = string(dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "quan_non_conformita"))

ll_quan =dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "quan_non_conformita")
ll_con = 0
ll_num = dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "quan_derogata")
ll_con = ll_con + ll_num
ll_num = dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "quan_recuperata")
ll_con = ll_con + ll_num
ll_num = dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "quan_accettabile")
ll_con = ll_con + ll_num
ll_num = dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "quan_resa")
ll_con = ll_con + ll_num
ll_num = dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "quan_rottamata")
ll_con = ll_con + ll_num

if ll_quan <> 0 then
   ll_num             = ll_quan - ll_con
   ll_perc  = (ll_num * 100) / ll_quan
   sle_difettosa.text = string(ll_perc)
else
   sle_difettosa.text = "??"   
end if

sle_anno_commessa.text = string( dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "anno_commessa") )
sle_num_commessa.text  = string( dw_etichette_non_conformita.getitemnumber(dw_etichette_non_conformita.getrow(), "num_commessa" ) )

ls_str_2 = dw_etichette_non_conformita.getitemstring(dw_etichette_non_conformita.getrow(), "cod_errore" )

SELECT tab_difformita.des_difformita
INTO :ls_str_1
FROM tab_difformita
WHERE tab_difformita.cod_errore = :ls_str_2 ;


sle_difformita.text = ls_str_2 + " " + ls_str_1


sle_ret_1.text = "VIETATO"
sle_ret_2.text = "UTILIZZARE          QUESTA TARGA DEVE ESSERE RIMOSSA"
sle_ret_3.text = "QUESTO             SOLO DA PERSONALE CONTROLLO QUALITA'"
sle_ret_4.text = "MATERIALE"


end on

type sle_nrcopie from singlelineedit within w_etichette_non_conformita
integer x = 1920
integer y = 400
integer width = 526
integer height = 80
integer taborder = 210
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "0001"
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

on losefocus;if len(sle_nrcopie.text) <> 4 then
   sle_nrcopie.setfocus()
end if
	
end on

type gb_3 from groupbox within w_etichette_non_conformita
integer x = 1280
integer y = 320
integer width = 1189
integer height = 300
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Stampa Etichette"
end type

type sle_logo_csteam from singlelineedit within w_etichette_non_conformita
boolean visible = false
integer x = 503
integer y = 1080
integer width = 709
integer height = 60
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_anno_commessa from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 240
integer width = 709
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_desc_prodotto from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 540
integer width = 709
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_fornitore from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 640
integer width = 709
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_anno_bolla_acq from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 740
integer width = 709
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_num_bolla_acq from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 840
integer width = 709
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_num_commessa from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 340
integer width = 709
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_prodotto from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 440
integer width = 709
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_quantita from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 940
integer width = 709
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_difettosa from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 1040
integer width = 709
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_rapporto from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 1140
integer width = 709
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_difformita from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 1240
integer width = 709
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_logo_azienda from singlelineedit within w_etichette_non_conformita
integer x = 526
integer y = 40
integer width = 709
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_ret_1 from statictext within w_etichette_non_conformita
integer x = 1280
integer y = 640
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 1:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_2 from statictext within w_etichette_non_conformita
integer x = 1280
integer y = 740
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 2:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_3 from statictext within w_etichette_non_conformita
integer x = 1280
integer y = 840
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 3:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_4 from statictext within w_etichette_non_conformita
integer x = 1280
integer y = 940
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 4:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_ret_2 from singlelineedit within w_etichette_non_conformita
integer x = 1646
integer y = 740
integer width = 823
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_3 from singlelineedit within w_etichette_non_conformita
integer x = 1646
integer y = 840
integer width = 823
integer height = 80
integer taborder = 170
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_1 from singlelineedit within w_etichette_non_conformita
integer x = 1646
integer y = 640
integer width = 823
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_4 from singlelineedit within w_etichette_non_conformita
integer x = 1646
integer y = 940
integer width = 823
integer height = 80
integer taborder = 180
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type r_1 from rectangle within w_etichette_non_conformita
integer linethickness = 5
long fillcolor = 79741120
integer x = 23
integer y = 20
integer width = 2514
integer height = 1340
end type

