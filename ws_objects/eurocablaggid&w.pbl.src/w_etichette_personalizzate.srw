﻿$PBExportHeader$w_etichette_personalizzate.srw
$PBExportComments$Composizione + Stampa Etichette Personalizzate
forward
global type w_etichette_personalizzate from w_cs_xx_risposta
end type
type st_printer from statictext within w_etichette_personalizzate
end type
type rb_zebra from radiobutton within w_etichette_personalizzate
end type
type rb_default from radiobutton within w_etichette_personalizzate
end type
type cb_1 from commandbutton within w_etichette_personalizzate
end type
type cb_stampa_classico from commandbutton within w_etichette_personalizzate
end type
type dw_report from datawindow within w_etichette_personalizzate
end type
type sle_barcode from singlelineedit within w_etichette_personalizzate
end type
type st_5 from statictext within w_etichette_personalizzate
end type
type cb_stampa_moderno from commandbutton within w_etichette_personalizzate
end type
type st_4 from statictext within w_etichette_personalizzate
end type
type st_2 from statictext within w_etichette_personalizzate
end type
type em_num_commessa from editmask within w_etichette_personalizzate
end type
type em_anno_commessa from editmask within w_etichette_personalizzate
end type
type st_1 from statictext within w_etichette_personalizzate
end type
type st_3 from statictext within w_etichette_personalizzate
end type
type st_prodotto from statictext within w_etichette_personalizzate
end type
type st_fornitore from statictext within w_etichette_personalizzate
end type
type st_20 from statictext within w_etichette_personalizzate
end type
type st_quantita from statictext within w_etichette_personalizzate
end type
type st_rapporto from statictext within w_etichette_personalizzate
end type
type st_difformita from statictext within w_etichette_personalizzate
end type
type st_nrcopie from statictext within w_etichette_personalizzate
end type
type cb_stampa from commandbutton within w_etichette_personalizzate
end type
type sle_nrcopie from singlelineedit within w_etichette_personalizzate
end type
type sle_logo_csteam from singlelineedit within w_etichette_personalizzate
end type
type sle_titolo_1 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_2 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_3 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_1 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_4 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_5 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_6 from singlelineedit within w_etichette_personalizzate
end type
type sle_logo_azienda from singlelineedit within w_etichette_personalizzate
end type
type st_ret_1 from statictext within w_etichette_personalizzate
end type
type st_ret_2 from statictext within w_etichette_personalizzate
end type
type st_ret_3 from statictext within w_etichette_personalizzate
end type
type st_ret_4 from statictext within w_etichette_personalizzate
end type
type sle_ret_2 from singlelineedit within w_etichette_personalizzate
end type
type sle_ret_3 from singlelineedit within w_etichette_personalizzate
end type
type sle_ret_1 from singlelineedit within w_etichette_personalizzate
end type
type sle_ret_4 from singlelineedit within w_etichette_personalizzate
end type
type sle_avv_1 from singlelineedit within w_etichette_personalizzate
end type
type sle_avv_2 from singlelineedit within w_etichette_personalizzate
end type
type st_ret_20 from statictext within w_etichette_personalizzate
end type
type st_ret_21 from statictext within w_etichette_personalizzate
end type
type cb_2 from commandbutton within w_etichette_personalizzate
end type
type gb_printer from groupbox within w_etichette_personalizzate
end type
end forward

global type w_etichette_personalizzate from w_cs_xx_risposta
integer width = 3813
integer height = 2740
string title = "Etichette Personalizzate"
boolean resizable = false
st_printer st_printer
rb_zebra rb_zebra
rb_default rb_default
cb_1 cb_1
cb_stampa_classico cb_stampa_classico
dw_report dw_report
sle_barcode sle_barcode
st_5 st_5
cb_stampa_moderno cb_stampa_moderno
st_4 st_4
st_2 st_2
em_num_commessa em_num_commessa
em_anno_commessa em_anno_commessa
st_1 st_1
st_3 st_3
st_prodotto st_prodotto
st_fornitore st_fornitore
st_20 st_20
st_quantita st_quantita
st_rapporto st_rapporto
st_difformita st_difformita
st_nrcopie st_nrcopie
cb_stampa cb_stampa
sle_nrcopie sle_nrcopie
sle_logo_csteam sle_logo_csteam
sle_titolo_1 sle_titolo_1
sle_riga_2 sle_riga_2
sle_riga_3 sle_riga_3
sle_riga_1 sle_riga_1
sle_riga_4 sle_riga_4
sle_riga_5 sle_riga_5
sle_riga_6 sle_riga_6
sle_logo_azienda sle_logo_azienda
st_ret_1 st_ret_1
st_ret_2 st_ret_2
st_ret_3 st_ret_3
st_ret_4 st_ret_4
sle_ret_2 sle_ret_2
sle_ret_3 sle_ret_3
sle_ret_1 sle_ret_1
sle_ret_4 sle_ret_4
sle_avv_1 sle_avv_1
sle_avv_2 sle_avv_2
st_ret_20 st_ret_20
st_ret_21 st_ret_21
cb_2 cb_2
gb_printer gb_printer
end type
global w_etichette_personalizzate w_etichette_personalizzate

type variables
string is_current_printer
string is_zebra_printer
boolean ib_printer_changed = false
end variables

event pc_setwindow;call super::pc_setwindow;string ls_str_1

sle_nrcopie.text = "0001"
em_anno_commessa.text=string(f_anno_esercizio ( ))

// Stampante Zebra
if guo_functions.uof_get_zebra_printer(is_current_printer, is_zebra_printer) then
	
	if guo_functions.uof_imposta_stampante(is_zebra_printer) then
		st_printer.text = "Stampante: " + is_zebra_printer
		ib_printer_changed = true
	else
		ib_printer_changed = false
		st_printer.text = "Stampante: " + is_current_printer
		g_mb.error("Errore durante l'impostazione della stampante Zebra '" + g_str.safe(is_zebra_printer) + "'.~r~nVerificare che il nome sia corretto e riprovare (parametro utente ZPN).") 
		rb_zebra.checked=false
		rb_default.checked=false
	end if
	
end if
end event

on w_etichette_personalizzate.create
int iCurrent
call super::create
this.st_printer=create st_printer
this.rb_zebra=create rb_zebra
this.rb_default=create rb_default
this.cb_1=create cb_1
this.cb_stampa_classico=create cb_stampa_classico
this.dw_report=create dw_report
this.sle_barcode=create sle_barcode
this.st_5=create st_5
this.cb_stampa_moderno=create cb_stampa_moderno
this.st_4=create st_4
this.st_2=create st_2
this.em_num_commessa=create em_num_commessa
this.em_anno_commessa=create em_anno_commessa
this.st_1=create st_1
this.st_3=create st_3
this.st_prodotto=create st_prodotto
this.st_fornitore=create st_fornitore
this.st_20=create st_20
this.st_quantita=create st_quantita
this.st_rapporto=create st_rapporto
this.st_difformita=create st_difformita
this.st_nrcopie=create st_nrcopie
this.cb_stampa=create cb_stampa
this.sle_nrcopie=create sle_nrcopie
this.sle_logo_csteam=create sle_logo_csteam
this.sle_titolo_1=create sle_titolo_1
this.sle_riga_2=create sle_riga_2
this.sle_riga_3=create sle_riga_3
this.sle_riga_1=create sle_riga_1
this.sle_riga_4=create sle_riga_4
this.sle_riga_5=create sle_riga_5
this.sle_riga_6=create sle_riga_6
this.sle_logo_azienda=create sle_logo_azienda
this.st_ret_1=create st_ret_1
this.st_ret_2=create st_ret_2
this.st_ret_3=create st_ret_3
this.st_ret_4=create st_ret_4
this.sle_ret_2=create sle_ret_2
this.sle_ret_3=create sle_ret_3
this.sle_ret_1=create sle_ret_1
this.sle_ret_4=create sle_ret_4
this.sle_avv_1=create sle_avv_1
this.sle_avv_2=create sle_avv_2
this.st_ret_20=create st_ret_20
this.st_ret_21=create st_ret_21
this.cb_2=create cb_2
this.gb_printer=create gb_printer
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_printer
this.Control[iCurrent+2]=this.rb_zebra
this.Control[iCurrent+3]=this.rb_default
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.cb_stampa_classico
this.Control[iCurrent+6]=this.dw_report
this.Control[iCurrent+7]=this.sle_barcode
this.Control[iCurrent+8]=this.st_5
this.Control[iCurrent+9]=this.cb_stampa_moderno
this.Control[iCurrent+10]=this.st_4
this.Control[iCurrent+11]=this.st_2
this.Control[iCurrent+12]=this.em_num_commessa
this.Control[iCurrent+13]=this.em_anno_commessa
this.Control[iCurrent+14]=this.st_1
this.Control[iCurrent+15]=this.st_3
this.Control[iCurrent+16]=this.st_prodotto
this.Control[iCurrent+17]=this.st_fornitore
this.Control[iCurrent+18]=this.st_20
this.Control[iCurrent+19]=this.st_quantita
this.Control[iCurrent+20]=this.st_rapporto
this.Control[iCurrent+21]=this.st_difformita
this.Control[iCurrent+22]=this.st_nrcopie
this.Control[iCurrent+23]=this.cb_stampa
this.Control[iCurrent+24]=this.sle_nrcopie
this.Control[iCurrent+25]=this.sle_logo_csteam
this.Control[iCurrent+26]=this.sle_titolo_1
this.Control[iCurrent+27]=this.sle_riga_2
this.Control[iCurrent+28]=this.sle_riga_3
this.Control[iCurrent+29]=this.sle_riga_1
this.Control[iCurrent+30]=this.sle_riga_4
this.Control[iCurrent+31]=this.sle_riga_5
this.Control[iCurrent+32]=this.sle_riga_6
this.Control[iCurrent+33]=this.sle_logo_azienda
this.Control[iCurrent+34]=this.st_ret_1
this.Control[iCurrent+35]=this.st_ret_2
this.Control[iCurrent+36]=this.st_ret_3
this.Control[iCurrent+37]=this.st_ret_4
this.Control[iCurrent+38]=this.sle_ret_2
this.Control[iCurrent+39]=this.sle_ret_3
this.Control[iCurrent+40]=this.sle_ret_1
this.Control[iCurrent+41]=this.sle_ret_4
this.Control[iCurrent+42]=this.sle_avv_1
this.Control[iCurrent+43]=this.sle_avv_2
this.Control[iCurrent+44]=this.st_ret_20
this.Control[iCurrent+45]=this.st_ret_21
this.Control[iCurrent+46]=this.cb_2
this.Control[iCurrent+47]=this.gb_printer
end on

on w_etichette_personalizzate.destroy
call super::destroy
destroy(this.st_printer)
destroy(this.rb_zebra)
destroy(this.rb_default)
destroy(this.cb_1)
destroy(this.cb_stampa_classico)
destroy(this.dw_report)
destroy(this.sle_barcode)
destroy(this.st_5)
destroy(this.cb_stampa_moderno)
destroy(this.st_4)
destroy(this.st_2)
destroy(this.em_num_commessa)
destroy(this.em_anno_commessa)
destroy(this.st_1)
destroy(this.st_3)
destroy(this.st_prodotto)
destroy(this.st_fornitore)
destroy(this.st_20)
destroy(this.st_quantita)
destroy(this.st_rapporto)
destroy(this.st_difformita)
destroy(this.st_nrcopie)
destroy(this.cb_stampa)
destroy(this.sle_nrcopie)
destroy(this.sle_logo_csteam)
destroy(this.sle_titolo_1)
destroy(this.sle_riga_2)
destroy(this.sle_riga_3)
destroy(this.sle_riga_1)
destroy(this.sle_riga_4)
destroy(this.sle_riga_5)
destroy(this.sle_riga_6)
destroy(this.sle_logo_azienda)
destroy(this.st_ret_1)
destroy(this.st_ret_2)
destroy(this.st_ret_3)
destroy(this.st_ret_4)
destroy(this.sle_ret_2)
destroy(this.sle_ret_3)
destroy(this.sle_ret_1)
destroy(this.sle_ret_4)
destroy(this.sle_avv_1)
destroy(this.sle_avv_2)
destroy(this.st_ret_20)
destroy(this.st_ret_21)
destroy(this.cb_2)
destroy(this.gb_printer)
end on

event close;call super::close;long ll_job

if ib_printer_changed then
	guo_functions.uof_imposta_stampante(is_current_printer)
	
	// Esegui un fake job per reipostare la stampante corrente su Windows
	// altrimenti PB la cambia solo al primo processo di stampa
	ll_job = printopen()
	printclose(ll_job)
end if
end event

type st_printer from statictext within w_etichette_personalizzate
integer x = 2354
integer y = 300
integer width = 1417
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stampante:"
boolean focusrectangle = false
end type

type rb_zebra from radiobutton within w_etichette_personalizzate
integer x = 2400
integer y = 120
integer width = 402
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "ZEBRA"
boolean checked = true
end type

event clicked;long ll_job
// Stampante Zebra
if guo_functions.uof_get_zebra_printer(is_current_printer, is_zebra_printer) then
	
	if guo_functions.uof_imposta_stampante(is_zebra_printer) then
		st_printer.text = "Stampante: " + is_zebra_printer
		ib_printer_changed = true
	else
		ib_printer_changed = false
		st_printer.text = "Stampante: " + is_current_printer
		g_mb.error("Errore durante l'impostazione della stampante Zebra '" + g_str.safe(is_zebra_printer) + "'.~r~nVerificare che il nome sia corretto e riprovare (parametro utente ZPN).") 
	end if
	ll_job = printopen()
	printclose(ll_job)
	
end if
end event

type rb_default from radiobutton within w_etichette_personalizzate
integer x = 2811
integer y = 120
integer width = 402
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "DEFAULT"
end type

event clicked;long ll_job

if ib_printer_changed then
	guo_functions.uof_imposta_stampante(is_current_printer)
	st_printer.text = "Stampante: " + is_current_printer
	// Esegui un fake job per reipostare la stampante corrente su Windows
	// altrimenti PB la cambia solo al primo processo di stampa
	ll_job = printopen()
	printclose(ll_job)
end if
end event

type cb_1 from commandbutton within w_etichette_personalizzate
integer x = 2354
integer y = 2280
integer width = 1394
integer height = 180
integer taborder = 60
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi Commessa"
end type

event clicked;string		ls_errore
long		ll_anno_commessa, ll_num_commessa
uo_funzioni_1 luo_f1

ll_anno_commessa = long(em_anno_commessa.text)
ll_num_commessa = long(em_num_commessa.text)

if g_mb.confirm( g_str.format("Sei sicuro di voler CHIUDERE la commessa $1/$2", ll_anno_commessa, ll_num_commessa), 2) then

	luo_f1 = create uo_funzioni_1 
	
	if luo_f1.uof_chiudi_commessa_fasi( ll_anno_commessa, ll_num_commessa, "RTC", true, guo_functions.uof_get_user_documents_folder( ) +  guo_functions.uof_get_random_filename( ) + ".log",  ls_errore) < 0 then
		rollback;
		destroy luo_f1
		g_mb.error(ls_errore)
		return
	else
		destroy luo_f1
		commit;
		g_mb.show(g_str.format("Commessa $1-$2 chiusa!",ll_anno_commessa, ll_num_commessa))
	end if
end if

end event

type cb_stampa_classico from commandbutton within w_etichette_personalizzate
integer x = 2354
integer y = 1840
integer width = 1394
integer height = 168
integer taborder = 100
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA classico"
end type

event clicked;long ll_job, ll_i

if g_mb.messagebox("Stampa Etichette","Conferma Stampa Etichette",question!,YesNo!) <> 1 then return

setpointer(hourglass!)
w_cs_xx_mdi.setmicrohelp("Stampa Etichette in Corso ..... ")

// ------------------------------ STAMPA SU TERMICA ---------------------------------------


dw_report.reset()
dw_report.dataobject="d_etichetta_personalizzata"
dw_report.insertrow(0)

dw_report.setitem(1,"logo",sle_logo_azienda.text)
dw_report.setitem(1,"titolo",sle_titolo_1.text)

dw_report.setitem(1,"riga_1",sle_riga_1.text)
dw_report.setitem(1,"riga_2",sle_riga_2.text)
dw_report.setitem(1,"riga_3",sle_riga_3.text)
dw_report.setitem(1,"riga_4",sle_riga_4.text)
dw_report.setitem(1,"riga_5",sle_riga_5.text)
dw_report.setitem(1,"riga_6",sle_riga_6.text)


dw_report.setitem(1,"ret_1",sle_ret_1.text)
dw_report.setitem(1,"ret_2",sle_ret_2.text)
dw_report.setitem(1,"ret_3",sle_ret_3.text)
dw_report.setitem(1,"ret_4",sle_ret_4.text)

dw_report.setitem(1,"avv_1",sle_avv_1.text)
dw_report.setitem(1,"avv_2",sle_avv_2.text)

dw_report.setitem(1,"barcode","*" + sle_barcode.text + "*")

ll_job = printopen()

for ll_i = 1 to long(sle_nrcopie.text)
	printdatawindow(ll_job, dw_report)
next

printclose(ll_job)

w_cs_xx_mdi.setmicrohelp("Pronto !")

setpointer(arrow!)


end event

type dw_report from datawindow within w_etichette_personalizzate
boolean visible = false
integer x = 2181
integer y = 348
integer width = 686
integer height = 400
integer taborder = 90
string title = "none"
string dataobject = "d_etichetta_personalizzata"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type sle_barcode from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 2488
integer width = 1847
integer height = 152
integer taborder = 160
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_5 from statictext within w_etichette_personalizzate
integer x = 41
integer y = 2500
integer width = 357
integer height = 136
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "BARCODE LOTTO"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_stampa_moderno from commandbutton within w_etichette_personalizzate
integer x = 2354
integer y = 1640
integer width = 1394
integer height = 160
integer taborder = 160
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA moderno"
end type

event clicked;string 	ls_str_1, ls_path_logo, ls_modify, ls_pos, ls_rag_soc_azienda, ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_telefono, ls_cod_prodotto, ls_cod_cliente, &
			ls_num_ord_cliente, ls_des_prodotto, ls_rag_soc_1
long		ll_i, ll_pos, ll_copie, ll_anno_commessa, ll_num_commessa, ll_anno_ordine, ll_num_ordine, ll_job
dec{4} 	ld_quan_prodotta

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
       flag_parametro = 'S' and &
       cod_parametro = 'LO3';

ls_path_logo = s_cs_xx.volume + ls_path_logo

dw_report.reset()
dw_report.dataobject = "d_etichetta_personalizzata_moderno"
dw_report.insertrow(0)
ls_modify = "intestazione.filename='" + ls_path_logo + "'"
ls_pos = dw_report.modify(ls_modify)


select rag_soc_1, indirizzo, cap, localita, provincia, telefono
into   :ls_rag_soc_azienda, :ls_indirizzo, :ls_cap, :ls_localita, :ls_provincia, :ls_telefono
from   aziende
where  cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ls_indirizzo) then ls_indirizzo = ""
if not isnull(ls_localita) then 	ls_indirizzo += " - " + ls_localita
if not isnull(ls_provincia) then 	ls_indirizzo += " (" + ls_provincia + ")"
if not isnull(ls_telefono) then 	ls_indirizzo += " Tel." + ls_telefono

sle_logo_azienda.text = ls_rag_soc_azienda


////////////////////////////////////

dw_report.setitem(1,"commessa", em_num_commessa.text)
dw_report.setitem(1,"anno_commessa", em_anno_commessa.text)

ll_anno_commessa = integer(em_anno_commessa.text)
ll_num_commessa= long(em_num_commessa.text)

select rag_soc_1
into   :ls_rag_soc_azienda
from   aziende
where  cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

dw_report.setitem(1,"indirizzo_azienda", ls_rag_soc_azienda)


select 	quan_prodotta
into   		:ld_quan_prodotta
from   	anag_commesse
where  	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:ll_anno_commessa and
			num_commessa=:ll_num_commessa;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

dw_report.setitem(1,"quantita", string(ld_quan_prodotta,"###,##0"))


select 	anno_registrazione,
		 	num_registrazione,
		 	cod_prodotto
into   		:ll_anno_ordine,
		 	:ll_num_ordine,
		 	:ls_cod_prodotto
from   	det_ord_ven
where  	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:ll_anno_commessa and
			num_commessa=:ll_num_commessa;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 

if sqlca.sqlcode = 100 then
	g_mb.messagebox("Apice","Attenzione, questa commessa non corrisponde ad alcun ordine CLIENTE")
	ll_anno_ordine = 0
	ll_num_ordine = 0
end if	 

dw_report.setitem(1,"cod_prodotto", ls_cod_prodotto)


select 	cod_cliente,
		 	num_ord_cliente
into   		:ls_cod_cliente,
		 	:ls_num_ord_cliente
from   	tes_ord_ven
where  	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_ordine and
			num_registrazione=:ll_num_ordine;
if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 

dw_report.setitem(1,"vs_ordine", ls_num_ord_cliente)
dw_report.setitem(1,"vs_codice",sle_riga_4.text)

select 	des_prodotto
into   		:ls_des_prodotto
from   	anag_prodotti
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:ls_cod_prodotto;
if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 

dw_report.setitem(1,"des_prodotto", ls_des_prodotto)


select 	rag_soc_1
into   		:ls_rag_soc_1
from   	anag_clienti
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_cliente=:ls_cod_cliente;
if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 

dw_report.setitem(1,"rag_soc", ls_rag_soc_1 + "(" + ls_cod_cliente +")" )
dw_report.setitem(1,"titolo", sle_titolo_1.text)

dw_report.setitem(1,"testo_qualita", sle_ret_1.text + "~r~n" + sle_ret_2.text + "~r~n" + sle_ret_3.text + "~r~n" + sle_ret_4.text)

ll_job = PrintOpen( ) 
for ll_i = 1 to long(sle_nrcopie.text)
		PrintDataWindow(ll_job, dw_report)
next
PrintClose(ll_job)

end event

type st_4 from statictext within w_etichette_personalizzate
integer x = 2354
integer y = 640
integer width = 841
integer height = 124
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nr. Commessa"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_etichette_personalizzate
integer x = 2354
integer y = 480
integer width = 841
integer height = 124
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Anno Commessa"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_num_commessa from editmask within w_etichette_personalizzate
integer x = 3223
integer y = 640
integer width = 544
integer height = 140
integer taborder = 190
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
string mask = "######"
end type

type em_anno_commessa from editmask within w_etichette_personalizzate
integer x = 3223
integer y = 480
integer width = 544
integer height = 140
integer taborder = 180
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
string mask = "####"
boolean spin = true
end type

type st_1 from statictext within w_etichette_personalizzate
integer x = 46
integer y = 40
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Azienda:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_etichette_personalizzate
integer x = 41
integer y = 200
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Titolo Alto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_prodotto from statictext within w_etichette_personalizzate
integer x = 37
integer y = 740
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 3:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_fornitore from statictext within w_etichette_personalizzate
integer x = 37
integer y = 564
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 2:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_20 from statictext within w_etichette_personalizzate
integer x = 32
integer y = 388
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 1:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_quantita from statictext within w_etichette_personalizzate
integer x = 37
integer y = 908
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 4:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_rapporto from statictext within w_etichette_personalizzate
integer x = 27
integer y = 1088
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 5:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_difformita from statictext within w_etichette_personalizzate
integer x = 27
integer y = 1256
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 6:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_nrcopie from statictext within w_etichette_personalizzate
integer x = 2606
integer y = 1440
integer width = 681
integer height = 180
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Nr Copie Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_stampa from commandbutton within w_etichette_personalizzate
integer x = 2971
integer y = 2500
integer width = 777
integer height = 100
integer taborder = 170
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa Vecchia Stampante"
end type

event clicked;string stringa, ls_errore,ls_com,ls_default
integer li_ritorno,li_risposta

setpointer(hourglass!)
li_ritorno = g_mb.messagebox("Stampa Etichette","Conferma Stampa Etichette",question!,YesNo!)

if li_ritorno <> 1 then RETURN

li_risposta = Registryget(s_cs_xx.chiave_root + "profilocorrente", "numero", ls_default)

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + ls_default, "com", ls_com)

w_cs_xx_mdi.setmicrohelp("Stampa Etichette su Stampante Termica in Corso ..... ")

// ------------------------------ STAMPA SU TERMICA ---------------------------------------

stringa = char(2) + "L" + char(13)

stringa = stringa + "111100000100200" + sle_logo_csteam.text + char(13)

stringa = stringa + "221100005600395" + sle_logo_azienda.text + char(13)
stringa = stringa + "222100005900360" + sle_titolo_1.text + char(13)

stringa = stringa + "221100005700330" + sle_riga_1.text  + char(13)

stringa = stringa + "221100005700300" + sle_riga_2.text + char(13)

stringa = stringa + "221100005700270" + sle_riga_3.text + char(13)

stringa = stringa + "221100005700240" + sle_riga_4.text + char(13)

stringa = stringa + "221100005700210" + sle_riga_5.text + char(13)

stringa = stringa + "221100005700175" + sle_riga_6.text + char(13)

stringa = stringa + "211200005700130" + sle_ret_1.text + char(13)
stringa = stringa + "211200005700100" + sle_ret_2.text + char(13)
stringa = stringa + "211200005700070" + sle_ret_3.text + char(13)
stringa = stringa + "211200005700040" + sle_ret_4.text + char(13)

stringa = stringa + "252100004200080" + sle_avv_1.text + char(13)
stringa = stringa + "252100004200010" + sle_avv_2.text + char(13)

stringa = stringa + "Q" + left(sle_nrcopie.text,4) + char(13)
stringa = stringa + "E" + char(13)

uo_serial_communication luo_serial_communication

luo_serial_communication = create uo_serial_communication

if luo_serial_communication.uof_write_com ( stringa, ls_com, 9600,ls_errore) = -1 then
	g_mb.messagebox("OMNIA",ls_errore)
	return
end if

destroy luo_serial_communication

w_cs_xx_mdi.setmicrohelp("Pronto !")

g_mb.messagebox("Stampa Etichette","Dati trasferiti a Stampante Termica",information!)

setpointer(arrow!)


end event

type sle_nrcopie from singlelineedit within w_etichette_personalizzate
integer x = 3314
integer y = 1440
integer width = 439
integer height = 168
integer taborder = 160
integer textsize = -20
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "0001"
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

on losefocus;if len(sle_nrcopie.text) <> 4 then
   sle_nrcopie.setfocus()
end if
	
end on

type sle_logo_csteam from singlelineedit within w_etichette_personalizzate
boolean visible = false
integer x = 503
integer y = 1080
integer width = 709
integer height = 60
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_titolo_1 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 200
integer width = 1847
integer height = 152
integer taborder = 20
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_2 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 380
integer width = 1847
integer height = 152
integer taborder = 50
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_3 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 556
integer width = 1847
integer height = 152
integer taborder = 60
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_1 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 732
integer width = 1847
integer height = 152
integer taborder = 40
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_4 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 908
integer width = 1847
integer height = 152
integer taborder = 70
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_5 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 1084
integer width = 1847
integer height = 152
integer taborder = 80
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_6 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 1260
integer width = 1847
integer height = 152
integer taborder = 90
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_logo_azienda from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 28
integer width = 1847
integer height = 152
integer taborder = 10
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_ret_1 from statictext within w_etichette_personalizzate
integer x = 32
integer y = 1416
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 1:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_2 from statictext within w_etichette_personalizzate
integer x = 27
integer y = 1616
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 2:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_3 from statictext within w_etichette_personalizzate
integer x = 37
integer y = 1784
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 3:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_4 from statictext within w_etichette_personalizzate
integer x = 23
integer y = 1956
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 4:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_ret_2 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 1612
integer width = 1847
integer height = 152
integer taborder = 110
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_3 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 1788
integer width = 1847
integer height = 152
integer taborder = 120
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_1 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 1436
integer width = 1847
integer height = 152
integer taborder = 100
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_4 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 1964
integer width = 1847
integer height = 152
integer taborder = 130
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_avv_1 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 2140
integer width = 1847
integer height = 152
integer taborder = 140
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_avv_2 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 2312
integer width = 1847
integer height = 152
integer taborder = 150
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_ret_20 from statictext within w_etichette_personalizzate
integer x = 32
integer y = 2148
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Avvertenza 1:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_21 from statictext within w_etichette_personalizzate
integer x = 41
integer y = 2324
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Avvertenza 2:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_etichette_personalizzate
integer x = 2354
integer y = 800
integer width = 1394
integer height = 200
integer taborder = 200
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Dati"
end type

event clicked;string ls_rag_soc_azienda,ls_num_ord_cliente,ls_cod_prodotto,ls_cod_cliente,ls_des_prodotto,ls_rag_soc_1
integer li_anno_commessa,li_anno_registrazione
long  ll_num_commessa,ll_num_registrazione,ll_prog_riga_ord_ven
dec{4} ld_quan_prodotta

li_anno_commessa = integer(em_anno_commessa.text)
ll_num_commessa= long(em_num_commessa.text)

select rag_soc_1
into   :ls_rag_soc_azienda
from   aziende
where  cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

select quan_prodotta
into   :ld_quan_prodotta
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

select anno_registrazione,
		 num_registrazione,
		 cod_prodotto
into   :li_anno_registrazione,
		 :ll_num_registrazione,
		 :ls_cod_prodotto
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 

select cod_cliente,
		 num_ord_cliente
into   :ls_cod_cliente,
		 :ls_num_ord_cliente
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 
		 
select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 

select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 

sle_logo_azienda.text = ls_rag_soc_azienda
sle_titolo_1.text     = "IDENTIFICAZIONE PRODOTTO"
sle_riga_1.text       = "Cliente: " + ls_rag_soc_1
sle_riga_2.text       = "Commessa: " + string(ll_num_commessa) + "   Quantità: " + string(ld_quan_prodotta)
sle_riga_3.text       = "Vs.Ordine: " + ls_num_ord_cliente
sle_riga_4.text       = "Vs.Codice: "
sle_riga_5.text       = "Descriz.: " + ls_des_prodotto
sle_riga_6.text       = "Ns.Codice: " + ls_cod_prodotto
sle_ret_1.text        = "CONTROLLO"
sle_ret_2.text        = "QUALITA'"
sle_ret_3.text        = "MATERIALE"
sle_ret_4.text        = "CONFORME"
sle_avv_1.text        = "PALETTA"
sle_avv_2.text        = ""




end event

type gb_printer from groupbox within w_etichette_personalizzate
integer x = 2354
integer y = 20
integer width = 1417
integer height = 260
integer taborder = 210
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "Stampante"
end type

