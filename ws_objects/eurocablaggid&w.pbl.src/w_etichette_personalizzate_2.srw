﻿$PBExportHeader$w_etichette_personalizzate_2.srw
$PBExportComments$Composizione + Stampa Etichette Personalizzate
forward
global type w_etichette_personalizzate_2 from w_cs_xx_risposta
end type
type cb_1 from commandbutton within w_etichette_personalizzate_2
end type
type rb_default from radiobutton within w_etichette_personalizzate_2
end type
type rb_zebra from radiobutton within w_etichette_personalizzate_2
end type
type st_printer from statictext within w_etichette_personalizzate_2
end type
type cbx_solo_indirizzo from checkbox within w_etichette_personalizzate_2
end type
type st_9 from statictext within w_etichette_personalizzate_2
end type
type sle_paletta from singlelineedit within w_etichette_personalizzate_2
end type
type cbx_stampa_paletta from checkbox within w_etichette_personalizzate_2
end type
type st_8 from statictext within w_etichette_personalizzate_2
end type
type sle_cod_prod_cliente from singlelineedit within w_etichette_personalizzate_2
end type
type st_7 from statictext within w_etichette_personalizzate_2
end type
type sle_barcode from singlelineedit within w_etichette_personalizzate_2
end type
type em_quantita from editmask within w_etichette_personalizzate_2
end type
type dw_destinazioni from datawindow within w_etichette_personalizzate_2
end type
type st_6 from statictext within w_etichette_personalizzate_2
end type
type mle_indirizzo_azienda from multilineedit within w_etichette_personalizzate_2
end type
type st_5 from statictext within w_etichette_personalizzate_2
end type
type em_qta_etichette from editmask within w_etichette_personalizzate_2
end type
type cbx_etic_destinazione from checkbox within w_etichette_personalizzate_2
end type
type dw_report from datawindow within w_etichette_personalizzate_2
end type
type cb_stampa_etichetta from commandbutton within w_etichette_personalizzate_2
end type
type r_1 from rectangle within w_etichette_personalizzate_2
end type
type st_4 from statictext within w_etichette_personalizzate_2
end type
type st_2 from statictext within w_etichette_personalizzate_2
end type
type em_num_commessa from editmask within w_etichette_personalizzate_2
end type
type em_anno_commessa from editmask within w_etichette_personalizzate_2
end type
type st_1 from statictext within w_etichette_personalizzate_2
end type
type st_3 from statictext within w_etichette_personalizzate_2
end type
type st_prodotto from statictext within w_etichette_personalizzate_2
end type
type st_20 from statictext within w_etichette_personalizzate_2
end type
type st_quantita from statictext within w_etichette_personalizzate_2
end type
type st_rapporto from statictext within w_etichette_personalizzate_2
end type
type st_difformita from statictext within w_etichette_personalizzate_2
end type
type st_nrcopie from statictext within w_etichette_personalizzate_2
end type
type sle_titolo_1 from singlelineedit within w_etichette_personalizzate_2
end type
type sle_riga_3 from singlelineedit within w_etichette_personalizzate_2
end type
type sle_riga_1 from singlelineedit within w_etichette_personalizzate_2
end type
type sle_riga_4 from singlelineedit within w_etichette_personalizzate_2
end type
type sle_riga_5 from singlelineedit within w_etichette_personalizzate_2
end type
type sle_riga_6 from singlelineedit within w_etichette_personalizzate_2
end type
type sle_logo_azienda from singlelineedit within w_etichette_personalizzate_2
end type
type cb_carica from commandbutton within w_etichette_personalizzate_2
end type
type gb_printer from groupbox within w_etichette_personalizzate_2
end type
end forward

global type w_etichette_personalizzate_2 from w_cs_xx_risposta
integer width = 3799
integer height = 2564
string title = "Etichette Commessa"
boolean resizable = false
cb_1 cb_1
rb_default rb_default
rb_zebra rb_zebra
st_printer st_printer
cbx_solo_indirizzo cbx_solo_indirizzo
st_9 st_9
sle_paletta sle_paletta
cbx_stampa_paletta cbx_stampa_paletta
st_8 st_8
sle_cod_prod_cliente sle_cod_prod_cliente
st_7 st_7
sle_barcode sle_barcode
em_quantita em_quantita
dw_destinazioni dw_destinazioni
st_6 st_6
mle_indirizzo_azienda mle_indirizzo_azienda
st_5 st_5
em_qta_etichette em_qta_etichette
cbx_etic_destinazione cbx_etic_destinazione
dw_report dw_report
cb_stampa_etichetta cb_stampa_etichetta
r_1 r_1
st_4 st_4
st_2 st_2
em_num_commessa em_num_commessa
em_anno_commessa em_anno_commessa
st_1 st_1
st_3 st_3
st_prodotto st_prodotto
st_20 st_20
st_quantita st_quantita
st_rapporto st_rapporto
st_difformita st_difformita
st_nrcopie st_nrcopie
sle_titolo_1 sle_titolo_1
sle_riga_3 sle_riga_3
sle_riga_1 sle_riga_1
sle_riga_4 sle_riga_4
sle_riga_5 sle_riga_5
sle_riga_6 sle_riga_6
sle_logo_azienda sle_logo_azienda
cb_carica cb_carica
gb_printer gb_printer
end type
global w_etichette_personalizzate_2 w_etichette_personalizzate_2

type variables
string is_current_printer
string is_zebra_printer
boolean ib_printer_changed = false
end variables

forward prototypes
public subroutine wf_popola_dw ()
public function integer wf_barcode_commessa (string as_barcode, ref integer ai_anno_commessa, ref long al_num_commessa, ref string as_errore)
public subroutine wf_carica_dati ()
end prototypes

public subroutine wf_popola_dw ();long			ll_new, ll_num_commessa, ll_num_ordine
string			ls_cod_des_cliente, ls_rag_soc, ls_indirizzo, ls_localita, ls_provincia, ls_cod_cliente, ls_cod_prodotto_cliente
integer		li_anno_commessa, li_anno_ordine


//################################################
//popolo la dw etichetta commessa
dw_report.reset()

ll_new = dw_report.insertrow(0)

//titolo ---------------------------------------------------------------------
dw_report.setitem(ll_new, "titolo", sle_titolo_1.text)
dw_report.setitem(ll_new, "indirizzo_azienda", mle_indirizzo_azienda.text)

//cliente --------------------------------------------------------------------
dw_report.setitem(ll_new, "rag_soc", sle_riga_1.text)

//prodotto cliente ---------------------------------------------------------
ls_cod_prodotto_cliente = sle_cod_prod_cliente.text
if ls_cod_prodotto_cliente<>"" and not isnull(ls_cod_prodotto_cliente) then
	dw_report.setitem(ll_new, "cod_prodotto_cliente", ls_cod_prodotto_cliente)
	dw_report.modify("cod_prod_cliente_t.text='Vs Cod:'")
else
	dw_report.setitem(ll_new, "cod_prodotto_cliente", ls_cod_prodotto_cliente)
	dw_report.modify("cod_prod_cliente_t.text=''")
end if

//commessa e quantità ----------------------------------------------------
//dw_report.setitem(ll_new, "commessa", em_num_commessa.text +"/" + Right(em_anno_commessa.text,2))
dw_report.setitem(ll_new, "commessa", em_num_commessa.text)
dw_report.setitem(ll_new, "anno_commessa", Right(em_anno_commessa.text,2) + "-")

dw_report.setitem(ll_new, "quantita", em_quantita.text)

//vs ordine e vs codice ----------------------------------------------------
dw_report.setitem(ll_new, "vs_ordine", sle_riga_3.text)
dw_report.setitem(ll_new, "vs_codice", sle_riga_4.text)

//prodotto -------------------------------------------------------------------
dw_report.setitem(ll_new, "cod_prodotto", sle_riga_6.text)
dw_report.setitem(ll_new, "des_prodotto", sle_riga_5.text)

//paletta --------------------------------------------------------------------
dw_report.setitem(ll_new, "paletta", "P.1")


//################################################
//popolo la dw etichetta commessa

li_anno_commessa = integer(em_anno_commessa.text)
ll_num_commessa= long(em_num_commessa.text)

select anno_registrazione, num_registrazione
into   :li_anno_ordine, :ll_num_ordine
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:li_anno_commessa and
			num_commessa=:ll_num_commessa;

select cod_cliente, cod_des_cliente
into   :ls_cod_cliente, :ls_cod_des_cliente
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno_ordine and
			num_registrazione=:ll_num_ordine;


if ls_cod_des_cliente="" or isnull(ls_cod_des_cliente) then
	//prendi i dati da angrafica clienti
	select rag_soc_1, indirizzo, localita, provincia
	into   :ls_rag_soc, :ls_indirizzo, :ls_localita, :ls_provincia
	from   anag_clienti
	where  cod_azienda=:s_cs_xx.cod_azienda and
				cod_cliente=:ls_cod_cliente;
	
else
	//prendi i dati da angrafica destinazione cliente
	select rag_soc_1, indirizzo, localita, provincia
	into   :ls_rag_soc, :ls_indirizzo, :ls_localita, :ls_provincia
	from   anag_des_clienti
	where  cod_azienda=:s_cs_xx.cod_azienda and
				cod_cliente=:ls_cod_cliente and
				cod_des_cliente=:ls_cod_des_cliente;
end if

if isnull(ls_rag_soc) then ls_rag_soc = ""
if not isnull(ls_indirizzo) then ls_rag_soc += "~r~n" + ls_indirizzo
if not isnull(ls_localita) then 	ls_rag_soc += "~r~n" + ls_localita
if not isnull(ls_provincia) then 	ls_rag_soc += " (" + ls_provincia + ")"

dw_destinazioni.reset()

ll_new = dw_destinazioni.insertrow(0)

dw_destinazioni.setitem(ll_new, "indirizzo_azienda", mle_indirizzo_azienda.text)
dw_destinazioni.setitem(ll_new, "destinazione", ls_rag_soc)
dw_destinazioni.setitem(ll_new, "paletta", "P.1")







end subroutine

public function integer wf_barcode_commessa (string as_barcode, ref integer ai_anno_commessa, ref long al_num_commessa, ref string as_errore);string				ls_temp

//formato barcode
//				   		   1 1 1 1 1 1 1
//   1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
//	* |anno |  numero  |  prog  |*
//
//cioè 
//		all'inizio in posizione 1 c'è il carattere asterisco '*'
//		dalla posizione 2 alla posizione 5 (len 4) si trova l'anno della commessa
//		dalla posizione 6 alla osizione 11 (len 6) si trova il numero commessa  (il campo viene riempito con spazi a destra, es: '123   ', aggiunti 3 spazi a desta per fare lunghezza 6)
//		dalla posizione 12 alla osizione 15 (len 4) si trova il progressivo riga della det  anag_commessa (il campo viene riempito con spazi a destra, es: '1   ', aggiunti 3 spazi a desta per fare lunghezza 4)
//		alla fine in posizione 16 c'è il carattere asterisco '*'

//i due caratteri di controllo '*', all'inizio e alla fine non vengono letti dalla procedura

//anno registrazione commessa: a partire dalla posizione 1 per una lunghezza pari a 4
ls_temp = mid(as_barcode, 1, 4)
if isnumber(ls_temp) then
	ai_anno_commessa = integer(ls_temp)
else
	as_errore = "E' stato estratto dal codice a barre della commessa un ANNO REGISTRAZIONE non numerico!"
	return -1
end if

//numero registrazione commessa: a partire dalla posizione 5 per una lunghezza pari a 6
ls_temp = trim(mid(as_barcode, 5, 6))
if isnumber(ls_temp) then
	al_num_commessa = integer(ls_temp)
else
	as_errore = "E' stato estratto dal codice a barre della commessa un NUMERO REGISTRAZIONE non numerico!"
	return -1
end if


return 0
end function

public subroutine wf_carica_dati ();string				ls_rag_soc_azienda,ls_num_ord_cliente,ls_cod_prodotto,ls_cod_cliente,ls_des_prodotto,ls_rag_soc_1,&
					ls_vs_codice, ls_cod_prod_cliente

integer			li_anno_commessa,li_anno_registrazione

long				ll_num_commessa,ll_num_registrazione,ll_prog_riga_ord_ven, ll_pos, ll_new

dec{4}			ld_quan_prodotta



li_anno_commessa = integer(em_anno_commessa.text)
ll_num_commessa= long(em_num_commessa.text)

select quan_prodotta
into   :ld_quan_prodotta
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<0 then
	g_mb.error("Apice","Errore sul DB: " + sqlca.sqlerrtext)
	return
	
elseif sqlca.sqlcode=100 then
	g_mb.warning("Apice","Commessa inesistente!")
	return
	
end if

select anno_registrazione,
		 num_registrazione,
		 cod_prodotto
into   :li_anno_registrazione,
		 :ll_num_registrazione,
		 :ls_cod_prodotto
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 

select cod_cliente,
		 num_ord_cliente
into   :ls_cod_cliente,
		 :ls_num_ord_cliente
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 
		 
select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	

//estrazione vs codice da des_prodotto
ll_pos = LastPos(ls_des_prodotto, "*" )
if ll_pos>0 then
	ls_vs_codice = right (ls_des_prodotto, len(ls_des_prodotto) - ll_pos)
	
	//poi considero come descrizione tutto quello che c'è prima di "*"
	ls_des_prodotto = left(ls_des_prodotto, ll_pos - 1)
end if


select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

select cod_prod_cliente
into :ls_cod_prod_cliente
from tab_prod_clienti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto and
			cod_cliente = :ls_cod_cliente;

if isnull(ls_cod_prod_cliente) then ls_cod_prod_cliente = ""


sle_titolo_1.text				= "IDENTIFICAZIONE PRODOTTO"
sle_riga_1.text					= ls_rag_soc_1
em_quantita.text				= string(ld_quan_prodotta)
sle_riga_3.text					= ls_num_ord_cliente
sle_riga_4.text					= ls_vs_codice
sle_riga_5.text					= ls_des_prodotto
sle_riga_6.text					= ls_cod_prodotto
sle_cod_prod_cliente.text 	= ls_cod_prod_cliente


//posiziona il cursore sulla casella quantità
em_quantita.setfocus()
em_quantita.SelectText(1, Len(em_quantita.text))

end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_str_1, ls_path_logo, ls_modify, ls_pos, ls_rag_soc_azienda, ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_telefono
long	ll_pos

em_qta_etichette.text = "1"
em_anno_commessa.text=string(f_anno_esercizio ( ))

ls_path_logo = s_cs_xx.volume + s_cs_xx.risorse + "logo_etichetta.JPG"

ls_modify = "intestazione.filename='" + ls_path_logo + "'"
ls_pos = dw_report.modify(ls_modify)
ls_pos = dw_destinazioni.modify(ls_modify)

select rag_soc_1, indirizzo, cap, localita, provincia, telefono
into   :ls_rag_soc_azienda, :ls_indirizzo, :ls_cap, :ls_localita, :ls_provincia, :ls_telefono
from   aziende
where  cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ls_indirizzo) then ls_indirizzo = ""
if not isnull(ls_localita) then 	ls_indirizzo += " - " + ls_localita
if not isnull(ls_provincia) then 	ls_indirizzo += " (" + ls_provincia + ")"
if not isnull(ls_telefono) then 	ls_indirizzo += " Tel." + ls_telefono

sle_logo_azienda.text = ls_rag_soc_azienda
mle_indirizzo_azienda.text = ls_indirizzo

// Stampante Zebra
if guo_functions.uof_get_zebra_printer(is_current_printer, is_zebra_printer) then
	
	if guo_functions.uof_imposta_stampante(is_zebra_printer) then
		st_printer.text = "Stampante: " + is_zebra_printer
		ib_printer_changed = true
	else
		ib_printer_changed = false
		st_printer.text = "Stampante: " + is_current_printer
		g_mb.error("Errore durante l'impostazione della stampante Zebra '" + g_str.safe(is_zebra_printer) + "'.~r~nVerificare che il nome sia corretto e riprovare (parametro utente ZPN).") 
		rb_zebra.checked=false
		rb_default.checked=false
	end if
	
end if



end event

on w_etichette_personalizzate_2.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.rb_default=create rb_default
this.rb_zebra=create rb_zebra
this.st_printer=create st_printer
this.cbx_solo_indirizzo=create cbx_solo_indirizzo
this.st_9=create st_9
this.sle_paletta=create sle_paletta
this.cbx_stampa_paletta=create cbx_stampa_paletta
this.st_8=create st_8
this.sle_cod_prod_cliente=create sle_cod_prod_cliente
this.st_7=create st_7
this.sle_barcode=create sle_barcode
this.em_quantita=create em_quantita
this.dw_destinazioni=create dw_destinazioni
this.st_6=create st_6
this.mle_indirizzo_azienda=create mle_indirizzo_azienda
this.st_5=create st_5
this.em_qta_etichette=create em_qta_etichette
this.cbx_etic_destinazione=create cbx_etic_destinazione
this.dw_report=create dw_report
this.cb_stampa_etichetta=create cb_stampa_etichetta
this.r_1=create r_1
this.st_4=create st_4
this.st_2=create st_2
this.em_num_commessa=create em_num_commessa
this.em_anno_commessa=create em_anno_commessa
this.st_1=create st_1
this.st_3=create st_3
this.st_prodotto=create st_prodotto
this.st_20=create st_20
this.st_quantita=create st_quantita
this.st_rapporto=create st_rapporto
this.st_difformita=create st_difformita
this.st_nrcopie=create st_nrcopie
this.sle_titolo_1=create sle_titolo_1
this.sle_riga_3=create sle_riga_3
this.sle_riga_1=create sle_riga_1
this.sle_riga_4=create sle_riga_4
this.sle_riga_5=create sle_riga_5
this.sle_riga_6=create sle_riga_6
this.sle_logo_azienda=create sle_logo_azienda
this.cb_carica=create cb_carica
this.gb_printer=create gb_printer
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.rb_default
this.Control[iCurrent+3]=this.rb_zebra
this.Control[iCurrent+4]=this.st_printer
this.Control[iCurrent+5]=this.cbx_solo_indirizzo
this.Control[iCurrent+6]=this.st_9
this.Control[iCurrent+7]=this.sle_paletta
this.Control[iCurrent+8]=this.cbx_stampa_paletta
this.Control[iCurrent+9]=this.st_8
this.Control[iCurrent+10]=this.sle_cod_prod_cliente
this.Control[iCurrent+11]=this.st_7
this.Control[iCurrent+12]=this.sle_barcode
this.Control[iCurrent+13]=this.em_quantita
this.Control[iCurrent+14]=this.dw_destinazioni
this.Control[iCurrent+15]=this.st_6
this.Control[iCurrent+16]=this.mle_indirizzo_azienda
this.Control[iCurrent+17]=this.st_5
this.Control[iCurrent+18]=this.em_qta_etichette
this.Control[iCurrent+19]=this.cbx_etic_destinazione
this.Control[iCurrent+20]=this.dw_report
this.Control[iCurrent+21]=this.cb_stampa_etichetta
this.Control[iCurrent+22]=this.r_1
this.Control[iCurrent+23]=this.st_4
this.Control[iCurrent+24]=this.st_2
this.Control[iCurrent+25]=this.em_num_commessa
this.Control[iCurrent+26]=this.em_anno_commessa
this.Control[iCurrent+27]=this.st_1
this.Control[iCurrent+28]=this.st_3
this.Control[iCurrent+29]=this.st_prodotto
this.Control[iCurrent+30]=this.st_20
this.Control[iCurrent+31]=this.st_quantita
this.Control[iCurrent+32]=this.st_rapporto
this.Control[iCurrent+33]=this.st_difformita
this.Control[iCurrent+34]=this.st_nrcopie
this.Control[iCurrent+35]=this.sle_titolo_1
this.Control[iCurrent+36]=this.sle_riga_3
this.Control[iCurrent+37]=this.sle_riga_1
this.Control[iCurrent+38]=this.sle_riga_4
this.Control[iCurrent+39]=this.sle_riga_5
this.Control[iCurrent+40]=this.sle_riga_6
this.Control[iCurrent+41]=this.sle_logo_azienda
this.Control[iCurrent+42]=this.cb_carica
this.Control[iCurrent+43]=this.gb_printer
end on

on w_etichette_personalizzate_2.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.rb_default)
destroy(this.rb_zebra)
destroy(this.st_printer)
destroy(this.cbx_solo_indirizzo)
destroy(this.st_9)
destroy(this.sle_paletta)
destroy(this.cbx_stampa_paletta)
destroy(this.st_8)
destroy(this.sle_cod_prod_cliente)
destroy(this.st_7)
destroy(this.sle_barcode)
destroy(this.em_quantita)
destroy(this.dw_destinazioni)
destroy(this.st_6)
destroy(this.mle_indirizzo_azienda)
destroy(this.st_5)
destroy(this.em_qta_etichette)
destroy(this.cbx_etic_destinazione)
destroy(this.dw_report)
destroy(this.cb_stampa_etichetta)
destroy(this.r_1)
destroy(this.st_4)
destroy(this.st_2)
destroy(this.em_num_commessa)
destroy(this.em_anno_commessa)
destroy(this.st_1)
destroy(this.st_3)
destroy(this.st_prodotto)
destroy(this.st_20)
destroy(this.st_quantita)
destroy(this.st_rapporto)
destroy(this.st_difformita)
destroy(this.st_nrcopie)
destroy(this.sle_titolo_1)
destroy(this.sle_riga_3)
destroy(this.sle_riga_1)
destroy(this.sle_riga_4)
destroy(this.sle_riga_5)
destroy(this.sle_riga_6)
destroy(this.sle_logo_azienda)
destroy(this.cb_carica)
destroy(this.gb_printer)
end on

event close;call super::close;long ll_job

if ib_printer_changed then
	guo_functions.uof_imposta_stampante(is_current_printer)
	
	// Esegui un fake job per reipostare la stampante corrente su Windows
	// altrimenti PB la cambia solo al primo processo di stampa
	ll_job = printopen()
	printclose(ll_job)
end if
end event

type cb_1 from commandbutton within w_etichette_personalizzate_2
integer x = 2075
integer y = 1880
integer width = 1646
integer height = 180
integer taborder = 210
boolean bringtotop = true
integer textsize = -22
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Dati"
boolean default = true
end type

event clicked;

wf_carica_dati()
end event

type rb_default from radiobutton within w_etichette_personalizzate_2
integer x = 2560
integer y = 120
integer width = 402
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "DEFAULT"
end type

event clicked;long ll_job

if ib_printer_changed then
	guo_functions.uof_imposta_stampante(is_current_printer)
	st_printer.text = "Stampante: " + is_current_printer
	// Esegui un fake job per reipostare la stampante corrente su Windows
	// altrimenti PB la cambia solo al primo processo di stampa
	ll_job = printopen()
	printclose(ll_job)
end if
end event

type rb_zebra from radiobutton within w_etichette_personalizzate_2
integer x = 2149
integer y = 120
integer width = 402
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "ZEBRA"
boolean checked = true
end type

event clicked;long ll_job
// Stampante Zebra
if guo_functions.uof_get_zebra_printer(is_current_printer, is_zebra_printer) then
	
	if guo_functions.uof_imposta_stampante(is_zebra_printer) then
		st_printer.text = "Stampante: " + is_zebra_printer
		ib_printer_changed = true
	else
		ib_printer_changed = false
		st_printer.text = "Stampante: " + is_current_printer
		g_mb.error("Errore durante l'impostazione della stampante Zebra '" + g_str.safe(is_zebra_printer) + "'.~r~nVerificare che il nome sia corretto e riprovare (parametro utente ZPN).") 
	end if
	ll_job = printopen()
	printclose(ll_job)
	
end if
end event

type st_printer from statictext within w_etichette_personalizzate_2
integer x = 2103
integer y = 240
integer width = 1600
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stampante:"
boolean focusrectangle = false
end type

type cbx_solo_indirizzo from checkbox within w_etichette_personalizzate_2
integer x = 2194
integer y = 800
integer width = 1518
integer height = 156
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Stampa SOLO Destinaz."
boolean lefttext = true
end type

event clicked;

if cbx_solo_indirizzo.checked then
	cbx_etic_destinazione.checked = true
end if
end event

type st_9 from statictext within w_etichette_personalizzate_2
integer x = 2021
integer y = 1004
integer width = 910
integer height = 128
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Forza Paletta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_paletta from singlelineedit within w_etichette_personalizzate_2
integer x = 2958
integer y = 988
integer width = 763
integer height = 164
integer taborder = 30
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cbx_stampa_paletta from checkbox within w_etichette_personalizzate_2
integer x = 2194
integer y = 640
integer width = 1518
integer height = 156
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Stampa n° Paletta."
boolean lefttext = true
end type

type st_8 from statictext within w_etichette_personalizzate_2
integer x = 1966
integer y = 2100
integer width = 1577
integer height = 108
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Codice a barre Commessa:"
boolean focusrectangle = false
end type

type sle_cod_prod_cliente from singlelineedit within w_etichette_personalizzate_2
integer x = 430
integer y = 1792
integer width = 1467
integer height = 164
integer taborder = 100
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_7 from statictext within w_etichette_personalizzate_2
integer x = 64
integer y = 1832
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Prod.Cli:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_barcode from singlelineedit within w_etichette_personalizzate_2
integer x = 1979
integer y = 2240
integer width = 1737
integer height = 164
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

event modified;integer			li_anno_commessa, li_ret

long				ll_num_commessa

string				ls_errore



if sle_barcode.text <> "" and not isnull(sle_barcode.text) then
	
	li_ret = wf_barcode_commessa(sle_barcode.text, li_anno_commessa, ll_num_commessa, ls_errore)
	if li_ret<0 then
		g_mb.error(ls_errore)
		return
		
	end if

	em_anno_commessa.text = string(li_anno_commessa)
	em_num_commessa.text = string(ll_num_commessa)
	
	cb_carica.postevent(clicked!)
	
	sle_barcode.text = ""
	
end if
end event

type em_quantita from editmask within w_etichette_personalizzate_2
integer x = 430
integer y = 904
integer width = 1467
integer height = 164
integer taborder = 190
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###,##0"
end type

type dw_destinazioni from datawindow within w_etichette_personalizzate_2
boolean visible = false
integer x = 2405
integer y = 1744
integer width = 347
integer height = 96
integer taborder = 140
string title = "none"
string dataobject = "d_etichetta_destinazioni"
boolean border = false
boolean livescroll = true
end type

type st_6 from statictext within w_etichette_personalizzate_2
integer x = 59
integer y = 944
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Q.tà:"
alignment alignment = right!
boolean focusrectangle = false
end type

type mle_indirizzo_azienda from multilineedit within w_etichette_personalizzate_2
integer x = 430
integer y = 212
integer width = 1463
integer height = 328
integer taborder = 150
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type st_5 from statictext within w_etichette_personalizzate_2
integer x = 46
integer y = 244
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ind. Azienda:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_qta_etichette from editmask within w_etichette_personalizzate_2
integer x = 3154
integer y = 320
integer width = 553
integer height = 164
integer taborder = 140
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "####0"
boolean spin = true
double increment = 1
string minmax = "1~~99999"
end type

type cbx_etic_destinazione from checkbox within w_etichette_personalizzate_2
integer x = 2194
integer y = 500
integer width = 1518
integer height = 156
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Stampa etic. Destinaz."
boolean checked = true
boolean lefttext = true
end type

type dw_report from datawindow within w_etichette_personalizzate_2
boolean visible = false
integer x = 2002
integer y = 1652
integer width = 384
integer height = 96
integer taborder = 130
string title = "none"
string dataobject = "d_etichetta_commessa"
boolean border = false
boolean livescroll = true
end type

type cb_stampa_etichetta from commandbutton within w_etichette_personalizzate_2
integer x = 2958
integer y = 1184
integer width = 763
integer height = 192
integer taborder = 50
integer textsize = -22
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long			job, ll_index, ll_num_copie


wf_popola_dw()

ll_num_copie = long(em_qta_etichette.text)

job = PrintOpen( ) 

for ll_index = 1 to ll_num_copie
	
	if cbx_stampa_paletta.checked then
		if sle_paletta.text<>"" and not isnull(sle_paletta.text) then
			dw_report.setitem(1, "paletta", sle_paletta.text)
		else
			dw_report.setitem(1, "paletta", "P."+string(ll_index))
		end if
	else
		dw_report.setitem(1, "paletta", "")
	end if
	
	//se hai deciso di stampare solo l'etichetta destinazioni, non stampare l'etichetta principale
	if not cbx_solo_indirizzo.checked then
		PrintDataWindow(job, dw_report)
	end if
	
	if cbx_etic_destinazione.checked or cbx_solo_indirizzo.checked then
		PrintDataWindow(job, dw_destinazioni)
	end if
	
next

PrintClose(job)

em_qta_etichette.text = "1"


end event

type r_1 from rectangle within w_etichette_personalizzate_2
integer linethickness = 5
long fillcolor = 12632256
integer y = 20
integer width = 3771
integer height = 2440
end type

type st_4 from statictext within w_etichette_personalizzate_2
integer x = 1938
integer y = 1680
integer width = 987
integer height = 148
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nr. Commessa"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_etichette_personalizzate_2
integer x = 1938
integer y = 1516
integer width = 987
integer height = 140
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Anno Commessa"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_num_commessa from editmask within w_etichette_personalizzate_2
integer x = 2962
integer y = 1688
integer width = 736
integer height = 164
integer taborder = 190
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "######"
boolean spin = true
end type

type em_anno_commessa from editmask within w_etichette_personalizzate_2
integer x = 2962
integer y = 1504
integer width = 507
integer height = 164
integer taborder = 180
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
string mask = "####"
boolean spin = true
end type

type st_1 from statictext within w_etichette_personalizzate_2
integer x = 46
integer y = 92
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Azienda:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_etichette_personalizzate_2
integer x = 50
integer y = 588
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Titolo Alto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_prodotto from statictext within w_etichette_personalizzate_2
integer x = 50
integer y = 1120
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Vs ordine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_20 from statictext within w_etichette_personalizzate_2
integer x = 32
integer y = 764
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Cliente:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_quantita from statictext within w_etichette_personalizzate_2
integer x = 55
integer y = 1300
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Vs Codice:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_rapporto from statictext within w_etichette_personalizzate_2
integer x = 59
integer y = 1476
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Descriz.:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_difformita from statictext within w_etichette_personalizzate_2
integer x = 59
integer y = 1652
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ns Codice:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_nrcopie from statictext within w_etichette_personalizzate_2
integer x = 2194
integer y = 340
integer width = 933
integer height = 132
integer textsize = -15
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nr Copie Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_titolo_1 from singlelineedit within w_etichette_personalizzate_2
integer x = 430
integer y = 548
integer width = 1467
integer height = 164
integer taborder = 20
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_3 from singlelineedit within w_etichette_personalizzate_2
integer x = 430
integer y = 1080
integer width = 1467
integer height = 164
integer taborder = 60
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_1 from singlelineedit within w_etichette_personalizzate_2
integer x = 430
integer y = 724
integer width = 1467
integer height = 164
integer taborder = 40
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_4 from singlelineedit within w_etichette_personalizzate_2
integer x = 430
integer y = 1260
integer width = 1467
integer height = 164
integer taborder = 70
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_5 from singlelineedit within w_etichette_personalizzate_2
integer x = 430
integer y = 1436
integer width = 1467
integer height = 164
integer taborder = 80
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_6 from singlelineedit within w_etichette_personalizzate_2
integer x = 430
integer y = 1612
integer width = 1467
integer height = 164
integer taborder = 90
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_logo_azienda from singlelineedit within w_etichette_personalizzate_2
integer x = 430
integer y = 40
integer width = 1467
integer height = 164
integer taborder = 10
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type cb_carica from commandbutton within w_etichette_personalizzate_2
integer x = 229
integer y = 2080
integer width = 1646
integer height = 180
integer taborder = 200
boolean bringtotop = true
integer textsize = -22
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi Commessa"
boolean default = true
end type

event clicked;string		ls_errore
long		ll_anno_commessa, ll_num_commessa
uo_funzioni_1 luo_f1

ll_anno_commessa = long(em_anno_commessa.text)
ll_num_commessa = long(em_num_commessa.text)

if g_mb.confirm( g_str.format("Sei sicuro di voler CHIUDERE la commessa $1/$2", ll_anno_commessa, ll_num_commessa), 2) then
	luo_f1 = create uo_funzioni_1 
	
	if luo_f1.uof_chiudi_commessa_fasi( ll_anno_commessa, ll_num_commessa, "RTC", true, guo_functions.uof_get_user_documents_folder( ) +  guo_functions.uof_get_random_filename( ) + ".log", ls_errore) < 0 then
		rollback;
		destroy luo_f1
		g_mb.error(ls_errore)
		return
	else
		destroy luo_f1
		commit;
		g_mb.show(g_str.format("Commessa $1-$2 chiusa!",ll_anno_commessa, ll_num_commessa))
	end if
end if

end event

type gb_printer from groupbox within w_etichette_personalizzate_2
integer x = 2103
integer y = 40
integer width = 1623
integer height = 180
integer taborder = 220
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "Stampante"
end type

