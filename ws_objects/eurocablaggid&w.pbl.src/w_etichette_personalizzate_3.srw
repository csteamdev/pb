﻿$PBExportHeader$w_etichette_personalizzate_3.srw
$PBExportComments$Composizione + Stampa Etichette Personalizzate
forward
global type w_etichette_personalizzate_3 from w_cs_xx_risposta
end type
type st_printer from statictext within w_etichette_personalizzate_3
end type
type rb_zebra from radiobutton within w_etichette_personalizzate_3
end type
type rb_default from radiobutton within w_etichette_personalizzate_3
end type
type cbx_solo_indirizzo from checkbox within w_etichette_personalizzate_3
end type
type sle_cod_prod_cliente from singlelineedit within w_etichette_personalizzate_3
end type
type st_8 from statictext within w_etichette_personalizzate_3
end type
type st_7 from statictext within w_etichette_personalizzate_3
end type
type cb_sel_prodotto from commandbutton within w_etichette_personalizzate_3
end type
type sle_prodotto from singlelineedit within w_etichette_personalizzate_3
end type
type em_quantita from editmask within w_etichette_personalizzate_3
end type
type dw_destinazioni from datawindow within w_etichette_personalizzate_3
end type
type st_6 from statictext within w_etichette_personalizzate_3
end type
type mle_indirizzo_azienda from multilineedit within w_etichette_personalizzate_3
end type
type st_5 from statictext within w_etichette_personalizzate_3
end type
type em_qta_etichette from editmask within w_etichette_personalizzate_3
end type
type cbx_etic_destinazione from checkbox within w_etichette_personalizzate_3
end type
type dw_report from datawindow within w_etichette_personalizzate_3
end type
type cb_stampa_etichetta from commandbutton within w_etichette_personalizzate_3
end type
type r_1 from rectangle within w_etichette_personalizzate_3
end type
type st_4 from statictext within w_etichette_personalizzate_3
end type
type st_2 from statictext within w_etichette_personalizzate_3
end type
type em_num_ordine from editmask within w_etichette_personalizzate_3
end type
type em_anno_ordine from editmask within w_etichette_personalizzate_3
end type
type st_1 from statictext within w_etichette_personalizzate_3
end type
type st_3 from statictext within w_etichette_personalizzate_3
end type
type st_prodotto from statictext within w_etichette_personalizzate_3
end type
type st_20 from statictext within w_etichette_personalizzate_3
end type
type st_quantita from statictext within w_etichette_personalizzate_3
end type
type st_rapporto from statictext within w_etichette_personalizzate_3
end type
type st_difformita from statictext within w_etichette_personalizzate_3
end type
type st_nrcopie from statictext within w_etichette_personalizzate_3
end type
type gb_3 from groupbox within w_etichette_personalizzate_3
end type
type sle_titolo_1 from singlelineedit within w_etichette_personalizzate_3
end type
type sle_riga_3 from singlelineedit within w_etichette_personalizzate_3
end type
type sle_riga_1 from singlelineedit within w_etichette_personalizzate_3
end type
type sle_riga_4 from singlelineedit within w_etichette_personalizzate_3
end type
type sle_riga_5 from singlelineedit within w_etichette_personalizzate_3
end type
type sle_riga_6 from singlelineedit within w_etichette_personalizzate_3
end type
type sle_logo_azienda from singlelineedit within w_etichette_personalizzate_3
end type
type cb_carica from commandbutton within w_etichette_personalizzate_3
end type
type gb_printer from groupbox within w_etichette_personalizzate_3
end type
end forward

global type w_etichette_personalizzate_3 from w_cs_xx_risposta
integer width = 4119
integer height = 2300
string title = "Etichette Manuali"
boolean resizable = false
st_printer st_printer
rb_zebra rb_zebra
rb_default rb_default
cbx_solo_indirizzo cbx_solo_indirizzo
sle_cod_prod_cliente sle_cod_prod_cliente
st_8 st_8
st_7 st_7
cb_sel_prodotto cb_sel_prodotto
sle_prodotto sle_prodotto
em_quantita em_quantita
dw_destinazioni dw_destinazioni
st_6 st_6
mle_indirizzo_azienda mle_indirizzo_azienda
st_5 st_5
em_qta_etichette em_qta_etichette
cbx_etic_destinazione cbx_etic_destinazione
dw_report dw_report
cb_stampa_etichetta cb_stampa_etichetta
r_1 r_1
st_4 st_4
st_2 st_2
em_num_ordine em_num_ordine
em_anno_ordine em_anno_ordine
st_1 st_1
st_3 st_3
st_prodotto st_prodotto
st_20 st_20
st_quantita st_quantita
st_rapporto st_rapporto
st_difformita st_difformita
st_nrcopie st_nrcopie
gb_3 gb_3
sle_titolo_1 sle_titolo_1
sle_riga_3 sle_riga_3
sle_riga_1 sle_riga_1
sle_riga_4 sle_riga_4
sle_riga_5 sle_riga_5
sle_riga_6 sle_riga_6
sle_logo_azienda sle_logo_azienda
cb_carica cb_carica
gb_printer gb_printer
end type
global w_etichette_personalizzate_3 w_etichette_personalizzate_3

type variables
string is_current_printer
string is_zebra_printer
boolean ib_printer_changed = false
end variables

forward prototypes
public subroutine wf_popola_dw ()
public subroutine wf_carica_dati ()
end prototypes

public subroutine wf_popola_dw ();long			ll_new, ll_num_commessa, ll_num_ordine
string			ls_cod_des_cliente, ls_rag_soc, ls_indirizzo, ls_localita, ls_provincia, ls_cod_cliente, ls_cod_prodotto_cliente, ls_ordine
integer		li_anno_commessa, li_anno_ordine


//################################################
//popolo la dw etichetta commessa
dw_report.reset()

li_anno_ordine = integer(em_anno_ordine.text)
ll_num_ordine= long(em_num_ordine.text)

ll_new = dw_report.insertrow(0)

//titolo ---------------------------------------------------------------------
dw_report.setitem(ll_new, "titolo", sle_titolo_1.text)
dw_report.setitem(ll_new, "indirizzo_azienda", mle_indirizzo_azienda.text)

//cliente --------------------------------------------------------------------
dw_report.setitem(ll_new, "rag_soc", sle_riga_1.text)

//prodotto cliente ---------------------------------------------------------
ls_cod_prodotto_cliente = sle_cod_prod_cliente.text
if ls_cod_prodotto_cliente<>"" and not isnull(ls_cod_prodotto_cliente) then
	dw_report.setitem(ll_new, "cod_prodotto_cliente", ls_cod_prodotto_cliente)
	dw_report.modify("cod_prod_cliente_t.text='Vs Cod:'")
else
	dw_report.setitem(ll_new, "cod_prodotto_cliente", ls_cod_prodotto_cliente)
	dw_report.modify("cod_prod_cliente_t.text=''")
end if

//ordine e quantità ----------------------------------------------------
dw_report.modify("anno_numero_t.text='Ord.'")

//ls_ordine = string(ll_num_ordine) + "/" +  Right(string(li_anno_ordine), 2)
ls_ordine = string(ll_num_ordine)
dw_report.setitem(ll_new, "commessa", ls_ordine)
dw_report.setitem(ll_new, "anno_commessa", Right(string(li_anno_ordine), 2) + "-")


dw_report.setitem(ll_new, "quantita", em_quantita.text)

//vs ordine e vs codice ----------------------------------------------------
dw_report.setitem(ll_new, "vs_ordine", sle_riga_3.text)
dw_report.setitem(ll_new, "vs_codice", sle_riga_4.text)

//prodotto -------------------------------------------------------------------
dw_report.setitem(ll_new, "cod_prodotto", sle_riga_6.text)
dw_report.setitem(ll_new, "des_prodotto", sle_riga_5.text)

//paletta --------------------------------------------------------------------
dw_report.setitem(ll_new, "paletta", "P.1")


//################################################
//popolo la dw etichetta commessa

select cod_cliente, cod_des_cliente
into   :ls_cod_cliente, :ls_cod_des_cliente
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno_ordine and
			num_registrazione=:ll_num_ordine;


if ls_cod_des_cliente="" or isnull(ls_cod_des_cliente) then
	//prendi i dati da angrafica clienti
	select rag_soc_1, indirizzo, localita, provincia
	into   :ls_rag_soc, :ls_indirizzo, :ls_localita, :ls_provincia
	from   anag_clienti
	where  cod_azienda=:s_cs_xx.cod_azienda and
				cod_cliente=:ls_cod_cliente;
	
else
	//prendi i dati da angrafica destinazione cliente
	select rag_soc_1, indirizzo, localita, provincia
	into   :ls_rag_soc, :ls_indirizzo, :ls_localita, :ls_provincia
	from   anag_des_clienti
	where  cod_azienda=:s_cs_xx.cod_azienda and
				cod_cliente=:ls_cod_cliente and
				cod_des_cliente=:ls_cod_des_cliente;
end if

if isnull(ls_rag_soc) then ls_rag_soc = ""
if not isnull(ls_indirizzo) then ls_rag_soc += "~r~n" + ls_indirizzo
if not isnull(ls_localita) then 	ls_rag_soc += "~r~n" + ls_localita
if not isnull(ls_provincia) then 	ls_rag_soc += " (" + ls_provincia + ")"

dw_destinazioni.reset()

ll_new = dw_destinazioni.insertrow(0)

dw_destinazioni.setitem(ll_new, "indirizzo_azienda", mle_indirizzo_azienda.text)
dw_destinazioni.setitem(ll_new, "destinazione", ls_rag_soc)
dw_destinazioni.setitem(ll_new, "paletta", "P.1")







end subroutine

public subroutine wf_carica_dati ();string				ls_rag_soc_azienda,ls_num_ord_cliente,ls_cod_prodotto, ls_cod_cliente,ls_des_prodotto,ls_rag_soc_1,&
					ls_vs_codice, ls_sql, ls_errore, ls_cod_prod_cliente
					
integer			li_anno_ordine

long				ll_num_ordine, ll_prog_riga_ord_ven, ll_pos, ll_new

dec{4}			ld_quan_prodotta

datastore		lds_data


li_anno_ordine = integer(em_anno_ordine.text)
ll_num_ordine= long(em_num_ordine.text)
ls_cod_prodotto = sle_prodotto.text

select cod_cliente, num_ord_cliente
into   :ls_cod_cliente, :ls_num_ord_cliente
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno_ordine and
			num_registrazione=:ll_num_ordine;

if sqlca.sqlcode<0 then
	g_mb.error("Apice","Errore sul DB: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode=100 then
	g_mb.warning("Apice","ordine inesistente!")
	return
end if	 

ls_sql = "select quan_ordine "+&
			"from   det_ord_ven  "+&
			"where  cod_azienda='"+s_cs_xx.cod_azienda +"'and  "+&
						"anno_registrazione="+string(li_anno_ordine)+" and "+&
						"num_registrazione="+string(ll_num_ordine)+" and "+&
						"cod_prodotto='"+ls_cod_prodotto+"'"

ll_new = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
if ll_new>0 then
	ld_quan_prodotta = lds_data.getitemnumber(1, 1)
else
	ld_quan_prodotta = 0
end if

ls_des_prodotto = f_des_tabella("anag_prodotti","cod_prodotto = '" +  ls_cod_prodotto +"'", "des_prodotto")

//estrazione vs codice da des_prodotto
ll_pos = LastPos(ls_des_prodotto, "*" )
if ll_pos>0 then
	ls_vs_codice = right (ls_des_prodotto, len(ls_des_prodotto) - ll_pos)
	
	//poi considero come descrizione tutto quello che c'è prima di "*"
	ls_des_prodotto = left(ls_des_prodotto, ll_pos - 1)
end if


select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode<0 then
	g_mb.error("Apice","Errore sul DB: " + sqlca.sqlerrtext)
	return
end if

select cod_prod_cliente
into :ls_cod_prod_cliente
from tab_prod_clienti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto and
			cod_cliente = :ls_cod_cliente;

if isnull(ls_cod_prod_cliente) then ls_cod_prod_cliente = ""


sle_titolo_1.text				= "IDENTIFICAZIONE PRODOTTO"
sle_riga_1.text					= ls_rag_soc_1
em_quantita.text				= string(ld_quan_prodotta)
sle_riga_3.text					= ls_num_ord_cliente
sle_riga_4.text					= ls_vs_codice
sle_riga_5.text					= ls_des_prodotto
sle_riga_6.text					= ls_cod_prodotto
sle_cod_prod_cliente.text 	= ls_cod_prod_cliente


//posiziona il cursore sulla casella quantità
em_quantita.setfocus()
em_quantita.SelectText(1, Len(em_quantita.text))

end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_str_1, ls_path_logo, ls_modify, ls_pos, ls_rag_soc_azienda, ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_telefono
long	ll_pos

em_qta_etichette.text = "1"
em_anno_ordine.text=string(f_anno_esercizio ( ))


ls_path_logo = guo_functions.uof_risorse("logo_etichetta.JPG")

ls_modify = "intestazione.filename='" + ls_path_logo + "'"
ls_pos = dw_report.modify(ls_modify)
ls_pos = dw_destinazioni.modify(ls_modify)



select rag_soc_1, indirizzo, cap, localita, provincia, telefono
into   :ls_rag_soc_azienda, :ls_indirizzo, :ls_cap, :ls_localita, :ls_provincia, :ls_telefono
from   aziende
where  cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ls_indirizzo) then ls_indirizzo = ""
if not isnull(ls_localita) then 	ls_indirizzo += " - " + ls_localita
if not isnull(ls_provincia) then 	ls_indirizzo += " (" + ls_provincia + ")"
if not isnull(ls_telefono) then 	ls_indirizzo += " Tel." + ls_telefono

sle_logo_azienda.text = ls_rag_soc_azienda
mle_indirizzo_azienda.text = ls_indirizzo


// Stampante Zebra
if guo_functions.uof_get_zebra_printer(is_current_printer, is_zebra_printer) then
	
	if guo_functions.uof_imposta_stampante(is_zebra_printer) then
		st_printer.text = "Stampante: " + is_zebra_printer
		ib_printer_changed = true
	else
		ib_printer_changed = false
		st_printer.text = "Stampante: " + is_current_printer
		g_mb.error("Errore durante l'impostazione della stampante Zebra '" + g_str.safe(is_zebra_printer) + "'.~r~nVerificare che il nome sia corretto e riprovare (parametro utente ZPN).") 
		rb_zebra.checked=false
		rb_default.checked=false
	end if
	
end if
end event

on w_etichette_personalizzate_3.create
int iCurrent
call super::create
this.st_printer=create st_printer
this.rb_zebra=create rb_zebra
this.rb_default=create rb_default
this.cbx_solo_indirizzo=create cbx_solo_indirizzo
this.sle_cod_prod_cliente=create sle_cod_prod_cliente
this.st_8=create st_8
this.st_7=create st_7
this.cb_sel_prodotto=create cb_sel_prodotto
this.sle_prodotto=create sle_prodotto
this.em_quantita=create em_quantita
this.dw_destinazioni=create dw_destinazioni
this.st_6=create st_6
this.mle_indirizzo_azienda=create mle_indirizzo_azienda
this.st_5=create st_5
this.em_qta_etichette=create em_qta_etichette
this.cbx_etic_destinazione=create cbx_etic_destinazione
this.dw_report=create dw_report
this.cb_stampa_etichetta=create cb_stampa_etichetta
this.r_1=create r_1
this.st_4=create st_4
this.st_2=create st_2
this.em_num_ordine=create em_num_ordine
this.em_anno_ordine=create em_anno_ordine
this.st_1=create st_1
this.st_3=create st_3
this.st_prodotto=create st_prodotto
this.st_20=create st_20
this.st_quantita=create st_quantita
this.st_rapporto=create st_rapporto
this.st_difformita=create st_difformita
this.st_nrcopie=create st_nrcopie
this.gb_3=create gb_3
this.sle_titolo_1=create sle_titolo_1
this.sle_riga_3=create sle_riga_3
this.sle_riga_1=create sle_riga_1
this.sle_riga_4=create sle_riga_4
this.sle_riga_5=create sle_riga_5
this.sle_riga_6=create sle_riga_6
this.sle_logo_azienda=create sle_logo_azienda
this.cb_carica=create cb_carica
this.gb_printer=create gb_printer
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_printer
this.Control[iCurrent+2]=this.rb_zebra
this.Control[iCurrent+3]=this.rb_default
this.Control[iCurrent+4]=this.cbx_solo_indirizzo
this.Control[iCurrent+5]=this.sle_cod_prod_cliente
this.Control[iCurrent+6]=this.st_8
this.Control[iCurrent+7]=this.st_7
this.Control[iCurrent+8]=this.cb_sel_prodotto
this.Control[iCurrent+9]=this.sle_prodotto
this.Control[iCurrent+10]=this.em_quantita
this.Control[iCurrent+11]=this.dw_destinazioni
this.Control[iCurrent+12]=this.st_6
this.Control[iCurrent+13]=this.mle_indirizzo_azienda
this.Control[iCurrent+14]=this.st_5
this.Control[iCurrent+15]=this.em_qta_etichette
this.Control[iCurrent+16]=this.cbx_etic_destinazione
this.Control[iCurrent+17]=this.dw_report
this.Control[iCurrent+18]=this.cb_stampa_etichetta
this.Control[iCurrent+19]=this.r_1
this.Control[iCurrent+20]=this.st_4
this.Control[iCurrent+21]=this.st_2
this.Control[iCurrent+22]=this.em_num_ordine
this.Control[iCurrent+23]=this.em_anno_ordine
this.Control[iCurrent+24]=this.st_1
this.Control[iCurrent+25]=this.st_3
this.Control[iCurrent+26]=this.st_prodotto
this.Control[iCurrent+27]=this.st_20
this.Control[iCurrent+28]=this.st_quantita
this.Control[iCurrent+29]=this.st_rapporto
this.Control[iCurrent+30]=this.st_difformita
this.Control[iCurrent+31]=this.st_nrcopie
this.Control[iCurrent+32]=this.gb_3
this.Control[iCurrent+33]=this.sle_titolo_1
this.Control[iCurrent+34]=this.sle_riga_3
this.Control[iCurrent+35]=this.sle_riga_1
this.Control[iCurrent+36]=this.sle_riga_4
this.Control[iCurrent+37]=this.sle_riga_5
this.Control[iCurrent+38]=this.sle_riga_6
this.Control[iCurrent+39]=this.sle_logo_azienda
this.Control[iCurrent+40]=this.cb_carica
this.Control[iCurrent+41]=this.gb_printer
end on

on w_etichette_personalizzate_3.destroy
call super::destroy
destroy(this.st_printer)
destroy(this.rb_zebra)
destroy(this.rb_default)
destroy(this.cbx_solo_indirizzo)
destroy(this.sle_cod_prod_cliente)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.cb_sel_prodotto)
destroy(this.sle_prodotto)
destroy(this.em_quantita)
destroy(this.dw_destinazioni)
destroy(this.st_6)
destroy(this.mle_indirizzo_azienda)
destroy(this.st_5)
destroy(this.em_qta_etichette)
destroy(this.cbx_etic_destinazione)
destroy(this.dw_report)
destroy(this.cb_stampa_etichetta)
destroy(this.r_1)
destroy(this.st_4)
destroy(this.st_2)
destroy(this.em_num_ordine)
destroy(this.em_anno_ordine)
destroy(this.st_1)
destroy(this.st_3)
destroy(this.st_prodotto)
destroy(this.st_20)
destroy(this.st_quantita)
destroy(this.st_rapporto)
destroy(this.st_difformita)
destroy(this.st_nrcopie)
destroy(this.gb_3)
destroy(this.sle_titolo_1)
destroy(this.sle_riga_3)
destroy(this.sle_riga_1)
destroy(this.sle_riga_4)
destroy(this.sle_riga_5)
destroy(this.sle_riga_6)
destroy(this.sle_logo_azienda)
destroy(this.cb_carica)
destroy(this.gb_printer)
end on

event close;call super::close;long ll_job

if ib_printer_changed then
	guo_functions.uof_imposta_stampante(is_current_printer)
	
	// Esegui un fake job per reipostare la stampante corrente su Windows
	// altrimenti PB la cambia solo al primo processo di stampa
	ll_job = printopen()
	printclose(ll_job)
end if
end event

type st_printer from statictext within w_etichette_personalizzate_3
integer x = 1943
integer y = 240
integer width = 2057
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stampante:"
boolean focusrectangle = false
end type

type rb_zebra from radiobutton within w_etichette_personalizzate_3
integer x = 2149
integer y = 120
integer width = 402
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "ZEBRA"
boolean checked = true
end type

event clicked;long ll_job
// Stampante Zebra
if guo_functions.uof_get_zebra_printer(is_current_printer, is_zebra_printer) then
	
	if guo_functions.uof_imposta_stampante(is_zebra_printer) then
		st_printer.text = "Stampante: " + is_zebra_printer
		ib_printer_changed = true
	else
		ib_printer_changed = false
		st_printer.text = "Stampante: " + is_current_printer
		g_mb.error("Errore durante l'impostazione della stampante Zebra '" + g_str.safe(is_zebra_printer) + "'.~r~nVerificare che il nome sia corretto e riprovare (parametro utente ZPN).") 
	end if
	ll_job = printopen()
	printclose(ll_job)
	
end if
end event

type rb_default from radiobutton within w_etichette_personalizzate_3
integer x = 2651
integer y = 120
integer width = 402
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "DEFAULT"
end type

event clicked;long ll_job

if ib_printer_changed then
	guo_functions.uof_imposta_stampante(is_current_printer)
	st_printer.text = "Stampante: " + is_current_printer
	// Esegui un fake job per reipostare la stampante corrente su Windows
	// altrimenti PB la cambia solo al primo processo di stampa
	ll_job = printopen()
	printclose(ll_job)
end if
end event

type cbx_solo_indirizzo from checkbox within w_etichette_personalizzate_3
integer x = 2057
integer y = 840
integer width = 1394
integer height = 128
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Stampa SOLO Destinaz."
boolean lefttext = true
end type

event clicked;

if cbx_solo_indirizzo.checked then
	cbx_etic_destinazione.checked = true
end if
end event

type sle_cod_prod_cliente from singlelineedit within w_etichette_personalizzate_3
integer x = 430
integer y = 1828
integer width = 1467
integer height = 164
integer taborder = 100
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_8 from statictext within w_etichette_personalizzate_3
integer x = 46
integer y = 1876
integer width = 361
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Prod.Cli:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_7 from statictext within w_etichette_personalizzate_3
integer x = 2080
integer y = 1800
integer width = 553
integer height = 132
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_sel_prodotto from commandbutton within w_etichette_personalizzate_3
integer x = 3909
integer y = 1780
integer width = 128
integer height = 148
integer taborder = 70
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;datawindow			ldw_null
any					la_cod_prodotto[]
string					ls_cod_prodotto


setnull(ldw_null)
guo_ricerca.uof_set_response( )
guo_ricerca.uof_ricerca_prodotto(ldw_null	, "")
guo_ricerca.uof_get_results( la_cod_prodotto[] )

if upperbound(la_cod_prodotto[]) > 0 then
	ls_cod_prodotto = string(la_cod_prodotto[1])
	sle_prodotto.text = ls_cod_prodotto
else
	//probabilmente hai fatto annulla dalla ricerca
	return
end if
end event

type sle_prodotto from singlelineedit within w_etichette_personalizzate_3
integer x = 2697
integer y = 1780
integer width = 1184
integer height = 164
integer taborder = 70
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type em_quantita from editmask within w_etichette_personalizzate_3
integer x = 430
integer y = 932
integer width = 1467
integer height = 164
integer taborder = 190
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###,##0"
end type

type dw_destinazioni from datawindow within w_etichette_personalizzate_3
boolean visible = false
integer x = 2153
integer y = 1832
integer width = 521
integer height = 168
integer taborder = 140
string title = "none"
string dataobject = "d_etichetta_destinazioni"
boolean border = false
boolean livescroll = true
end type

type st_6 from statictext within w_etichette_personalizzate_3
integer x = 46
integer y = 972
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Q.tà:"
alignment alignment = right!
boolean focusrectangle = false
end type

type mle_indirizzo_azienda from multilineedit within w_etichette_personalizzate_3
integer x = 430
integer y = 232
integer width = 1463
integer height = 328
integer taborder = 150
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type st_5 from statictext within w_etichette_personalizzate_3
integer x = 46
integer y = 244
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ind. Azienda:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_qta_etichette from editmask within w_etichette_personalizzate_3
integer x = 3337
integer y = 500
integer width = 553
integer height = 164
integer taborder = 140
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "####0"
boolean spin = true
double increment = 1
string minmax = "1~~99999"
end type

type cbx_etic_destinazione from checkbox within w_etichette_personalizzate_3
integer x = 2057
integer y = 700
integer width = 1385
integer height = 128
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Stampa etic. Destinaz."
boolean checked = true
boolean lefttext = true
end type

type dw_report from datawindow within w_etichette_personalizzate_3
boolean visible = false
integer x = 2802
integer y = 1848
integer width = 521
integer height = 132
integer taborder = 130
string title = "none"
string dataobject = "d_etichetta_commessa"
boolean border = false
boolean livescroll = true
end type

type cb_stampa_etichetta from commandbutton within w_etichette_personalizzate_3
integer x = 3131
integer y = 1080
integer width = 763
integer height = 192
integer taborder = 50
integer textsize = -22
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long			job, ll_index, ll_num_copie


wf_popola_dw()

ll_num_copie = long(em_qta_etichette.text)

job = PrintOpen( ) 

for ll_index = 1 to ll_num_copie
	
	//per le copie successive alla prima impostare il campo paletta (es. P.2, P.3, ecc...)
	if ll_index>1 then
		dw_report.setitem(1, "paletta", "P."+string(ll_index))
	end if
	
	//se hai deciso di stampare solo l'etichetta destinazioni, non stampare l'etichetta principale
	if not cbx_solo_indirizzo.checked then
		PrintDataWindow(job, dw_report)
	end if
	
	if cbx_etic_destinazione.checked or cbx_solo_indirizzo.checked then
		PrintDataWindow(job, dw_destinazioni)
	end if
	
next

PrintClose(job)

em_qta_etichette.text = "1"
end event

type r_1 from rectangle within w_etichette_personalizzate_3
integer linethickness = 5
long fillcolor = 12632256
integer width = 4091
integer height = 2200
end type

type st_4 from statictext within w_etichette_personalizzate_3
integer x = 2171
integer y = 1580
integer width = 896
integer height = 132
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nr. Ordine"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_etichette_personalizzate_3
integer x = 2400
integer y = 1380
integer width = 896
integer height = 132
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Anno Ordine"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_num_ordine from editmask within w_etichette_personalizzate_3
integer x = 3131
integer y = 1560
integer width = 736
integer height = 164
integer taborder = 190
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
string mask = "######"
boolean spin = true
end type

type em_anno_ordine from editmask within w_etichette_personalizzate_3
integer x = 3360
integer y = 1360
integer width = 507
integer height = 164
integer taborder = 180
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
string mask = "####"
boolean spin = true
end type

type st_1 from statictext within w_etichette_personalizzate_3
integer x = 46
integer y = 96
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Azienda:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_etichette_personalizzate_3
integer x = 46
integer y = 612
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Titolo Alto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_prodotto from statictext within w_etichette_personalizzate_3
integer x = 46
integer y = 1152
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Vs ordine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_20 from statictext within w_etichette_personalizzate_3
integer x = 46
integer y = 792
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Cliente:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_quantita from statictext within w_etichette_personalizzate_3
integer x = 46
integer y = 1336
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Vs Codice:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_rapporto from statictext within w_etichette_personalizzate_3
integer x = 46
integer y = 1512
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Descriz.:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_difformita from statictext within w_etichette_personalizzate_3
integer x = 46
integer y = 1688
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ns Codice:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_nrcopie from statictext within w_etichette_personalizzate_3
integer x = 2446
integer y = 520
integer width = 837
integer height = 132
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nr Copie Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type gb_3 from groupbox within w_etichette_personalizzate_3
integer x = 1943
integer y = 420
integer width = 2098
integer height = 892
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 12632256
string text = "Stampa Etichette Nuova"
end type

type sle_titolo_1 from singlelineedit within w_etichette_personalizzate_3
integer x = 430
integer y = 572
integer width = 1467
integer height = 164
integer taborder = 20
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_3 from singlelineedit within w_etichette_personalizzate_3
integer x = 430
integer y = 1112
integer width = 1467
integer height = 164
integer taborder = 60
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_1 from singlelineedit within w_etichette_personalizzate_3
integer x = 430
integer y = 752
integer width = 1467
integer height = 164
integer taborder = 40
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_4 from singlelineedit within w_etichette_personalizzate_3
integer x = 430
integer y = 1296
integer width = 1467
integer height = 164
integer taborder = 70
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_5 from singlelineedit within w_etichette_personalizzate_3
integer x = 430
integer y = 1472
integer width = 1467
integer height = 164
integer taborder = 80
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_6 from singlelineedit within w_etichette_personalizzate_3
integer x = 430
integer y = 1648
integer width = 1467
integer height = 164
integer taborder = 90
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_logo_azienda from singlelineedit within w_etichette_personalizzate_3
integer x = 430
integer y = 56
integer width = 1467
integer height = 164
integer taborder = 10
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type cb_carica from commandbutton within w_etichette_personalizzate_3
integer x = 3131
integer y = 1980
integer width = 763
integer height = 192
integer taborder = 200
integer textsize = -22
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Dati"
boolean default = true
end type

event clicked;

wf_carica_dati()
end event

type gb_printer from groupbox within w_etichette_personalizzate_3
integer x = 1943
integer y = 40
integer width = 2080
integer height = 180
integer taborder = 230
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "Stampante"
end type

