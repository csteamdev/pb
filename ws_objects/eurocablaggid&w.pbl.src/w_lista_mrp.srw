﻿$PBExportHeader$w_lista_mrp.srw
$PBExportComments$Window lista MRP
forward
global type w_lista_mrp from w_cs_xx_principale
end type
type dw_lista_mrp from uo_cs_xx_dw within w_lista_mrp
end type
type st_1 from statictext within w_lista_mrp
end type
type cb_calcola from commandbutton within w_lista_mrp
end type
type em_da_data from editmask within w_lista_mrp
end type
type em_a_data from editmask within w_lista_mrp
end type
type st_2 from statictext within w_lista_mrp
end type
type st_3 from statictext within w_lista_mrp
end type
end forward

global type w_lista_mrp from w_cs_xx_principale
int Width=3466
int Height=1501
boolean TitleBar=true
string Title="Material Resource Planning"
dw_lista_mrp dw_lista_mrp
st_1 st_1
cb_calcola cb_calcola
em_da_data em_da_data
em_a_data em_a_data
st_2 st_2
st_3 st_3
end type
global w_lista_mrp w_lista_mrp

forward prototypes
public function integer f_elimina_array (ref string fs_array[])
end prototypes

public function integer f_elimina_array (ref string fs_array[]);string ls_array[]

fs_array[]=ls_array

return 0
end function

on w_lista_mrp.create
int iCurrent
call w_cs_xx_principale::create
this.dw_lista_mrp=create dw_lista_mrp
this.st_1=create st_1
this.cb_calcola=create cb_calcola
this.em_da_data=create em_da_data
this.em_a_data=create em_a_data
this.st_2=create st_2
this.st_3=create st_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_mrp
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=cb_calcola
this.Control[iCurrent+4]=em_da_data
this.Control[iCurrent+5]=em_a_data
this.Control[iCurrent+6]=st_2
this.Control[iCurrent+7]=st_3
end on

on w_lista_mrp.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_lista_mrp)
destroy(this.st_1)
destroy(this.cb_calcola)
destroy(this.em_da_data)
destroy(this.em_a_data)
destroy(this.st_2)
destroy(this.st_3)
end on

event pc_setwindow;call super::pc_setwindow;dw_lista_mrp.set_dw_options(sqlca, &
                        	 pcca.null_object, &
                            c_multiselect + &
									 c_nonew + &
									 c_nomodify + &
									 c_nodelete + &
									 c_disablecc + &
									 c_noretrieveonopen + &
									 c_disableccinsert , &
                            c_viewmodeblack)
													
													
em_da_data.text = string(today())
em_a_data.text=string(RelativeDate(today(), 30))
iuo_dw_main=dw_lista_mrp
end event

type dw_lista_mrp from uo_cs_xx_dw within w_lista_mrp
int X=23
int Y=101
int Width=3383
int Height=1181
int TabOrder=10
string DataObject="d_lista_mrp"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

type st_1 from statictext within w_lista_mrp
int X=23
int Y=21
int Width=801
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Elenco Prodotti da Ordinare"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_calcola from commandbutton within w_lista_mrp
int X=3041
int Y=1301
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Calcola"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string   ls_cod_prodotto[],ls_cod_prodotto_ven,ls_cod_prodotto_acq,ls_cod_prodotto_com, &
		   ls_materie_prime[],ls_test,ls_des_prodotto,ls_flag_sotto_scorta,ls_cod_versione
double   ldd_disponibilita[],ldd_quan_ordine_ven[],ldd_quan_ordine_acq[],ldd_quan_commesse[], & 
		   ldd_quan_ordine_v,ldd_quan_ordine_a,ldd_quan_c,ldd_disp,ldd_quan_utilizzo[], &
		   ldd_quan_ordinare[],ldd_scorta_minima,ldd_quan_ripristino, &
			ldd_quan_necessaria[],ldd_giacenza_stock,ldd_giacenza[]
long     ll_i,ll_t,ll_mp,ll_num_commessa,ll_num_commessa_ordine
boolean  lb_flag_trovato
integer  li_risposta,li_anno_commessa,li_anno_commessa_ordine
datetime ldt_oggi,ldt_da_data,ldt_a_data

ldt_a_data = datetime(date(em_a_data.text))
ldt_da_data = datetime(date(em_da_data.text))

setnull(li_anno_commessa)
setnull(li_anno_commessa_ordine)
setnull(ll_num_commessa)
setnull(ll_num_commessa_ordine)

setpointer(hourglass!)
dw_lista_mrp.reset()

ldt_oggi = datetime(today())

declare righe_det_ord_ven_1 cursor for
select  cod_prodotto,
		  quan_ordine - quan_in_evasione - quan_evasa,
		  anno_commessa,
		  num_commessa,
		  cod_versione
from    det_ord_ven
where   cod_azienda =: s_cs_xx.cod_azienda
and     flag_blocco = 'N'
and     flag_evasione = 'A'
and     data_consegna between :ldt_da_data and :ldt_a_data;

open righe_det_ord_ven_1;

do while 1=1
	fetch righe_det_ord_ven_1 
	into  :ls_cod_prodotto_ven,
			:ldd_quan_ordine_v,	
			:li_anno_commessa_ordine,
			:ll_num_commessa_ordine,
			:ls_cod_versione;
			
	if sqlca.sqlcode<>0 then 
		choose case sqlca.sqlcode
			case 100
				exit

			case is <0
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
				exit
		end choose
	end if
	
//	select cod_azienda
//	into   :ls_test
//	from   det_anag_commesse
//	where  cod_azienda=:s_cs_xx.cod_azienda
//	and    anno_commessa=:li_anno_commessa_ordine
//	and    num_commessa=:ll_num_commessa_ordine;
//
//	if sqlca.sqlcode = 0 then continue //possibilita di leggere la quan_assegnata nella commessa ed elaborarla
//
//	if isnull(ls_cod_prodotto_ven) then continue
//	
//	if isnull(ldd_quan_ordine_v) then ldd_quan_ordine_v = 0
//	
//	lb_flag_trovato = false
//	
//	for ll_t = 1 to upperbound(ls_cod_prodotto[])
//		 if ls_cod_prodotto[ll_t] = ls_cod_prodotto_ven then
//		    lb_flag_trovato = true
//			 ldd_quan_ordine_ven[ll_t] = ldd_quan_ordine_ven[ll_t] + ldd_quan_ordine_v
//			 exit
//		 end if
//	next
//	
//	if lb_flag_trovato=false then
//		ll_i=upperbound(ls_cod_prodotto[])
//		ll_i++
//		
//		select saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita & 
//				 - quan_impegnata - quan_assegnata - quan_anticipi - quan_in_spedizione
//		into   :ldd_disp
//		from   anag_prodotti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_prodotto=:ls_cod_prodotto_ven;
//		
//		if isnull(ldd_disp) then ldd_disp = 0
//		ls_cod_prodotto[ll_i]=ls_cod_prodotto_ven
//		ldd_disponibilita[ll_i]=ldd_disp
//		ldd_quan_ordine_ven[ll_i]=ldd_quan_ordine_v
//		ldd_quan_ordine_acq[ll_i]=0
//		ldd_quan_commesse[ll_i]=0
//		ldd_quan_ordinare[ll_i]=0
//	end if
//	
	f_elimina_array(ls_materie_prime[])

	li_risposta = f_trova_mat_prima(ls_cod_prodotto_ven, ls_cod_versione,ls_materie_prime[], & 
											  ldd_quan_utilizzo[],ldd_quan_ordine_v,li_anno_commessa,ll_num_commessa)
		
	for ll_mp = 1 to upperbound(ls_materie_prime[])
		lb_flag_trovato = false
	
		for ll_t = 1 to upperbound(ls_cod_prodotto[])
			 if ls_cod_prodotto[ll_t] = ls_materie_prime[ll_mp] then
			    lb_flag_trovato = true
				 ldd_quan_necessaria[ll_t]=ldd_quan_necessaria[ll_t] + ldd_quan_utilizzo[ll_mp]
				 exit
			 end if
		next
	
		if lb_flag_trovato=false then
			ll_i=upperbound(ls_cod_prodotto[])
			ll_i++
		
			select saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita & 
					 - quan_assegnata - quan_anticipi - quan_in_spedizione
			into   :ldd_disp
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_materie_prime[ll_mp];

			if isnull(ldd_disp) then ldd_disp = 0
			
			select sum(giacenza_stock)
			into   :ldd_giacenza_stock
			from   stock
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_materie_prime[ll_mp];
			
			ls_cod_prodotto[ll_i]=ls_materie_prime[ll_mp]
			ldd_disponibilita[ll_i]=ldd_disp
			ldd_giacenza[ll_i]=ldd_giacenza_stock
			ldd_quan_necessaria[ll_i]=ldd_quan_utilizzo[ll_mp]	
			ldd_quan_ordine_acq[ll_i] = 0
			ldd_quan_commesse[ll_i] = 0
			
		end if
	next
loop

close righe_det_ord_ven_1;

declare righe_det_ord_acq_1 cursor for
select  cod_prodotto,
		  quan_ordinata - quan_arrivata
from    det_ord_acq
where   cod_azienda=:s_cs_xx.cod_azienda
and     flag_blocco='N'
and     flag_saldo='N'
and     data_consegna_fornitore between :ldt_da_data and :ldt_a_data;

open righe_det_ord_acq_1;

do while 1=1
	fetch righe_det_ord_acq_1 
	into  :ls_cod_prodotto_acq,
			:ldd_quan_ordine_a;
			
	if sqlca.sqlcode<>0 then 
		choose case sqlca.sqlcode
			case 100
				exit

			case is <0
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
				exit
				
		end choose
	end if
	
	lb_flag_trovato = false
	if isnull(ldd_quan_ordine_a) then ldd_quan_ordine_a = 0
	
	for ll_t = 1 to upperbound(ls_cod_prodotto[])
		 if ls_cod_prodotto[ll_t] = ls_cod_prodotto_acq then
		    lb_flag_trovato = true
			 ldd_quan_ordine_acq[ll_t] = ldd_quan_ordine_acq[ll_t] + ldd_quan_ordine_a
			 exit
		 end if
	next
	
	if lb_flag_trovato=false then
		ll_i=upperbound(ls_cod_prodotto[])
		ll_i++
		
		select saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita & 
				 quan_assegnata - quan_anticipi - quan_in_spedizione
		into   :ldd_disp
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto_acq;

		if isnull(ldd_disp) then ldd_disp = 0

		select sum(giacenza_stock)
		into   :ldd_giacenza_stock
		from   stock
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto_acq;
				
   	ldd_giacenza[ll_i]=ldd_giacenza_stock
		ls_cod_prodotto[ll_i] = ls_cod_prodotto_acq
		ldd_disponibilita[ll_i]=ldd_disp
		ldd_quan_ordine_acq[ll_i]=ldd_quan_ordine_a
		ldd_quan_commesse[ll_i]=0
		ldd_quan_necessaria[ll_i]=0

		
	end if
	
loop

close righe_det_ord_acq_1;

declare righe_com_1 cursor for
select  cod_prodotto,
		  cod_versione,
		  quan_ordine - quan_in_produzione - quan_assegnata - quan_prodotta,
		  anno_commessa,
		  num_commessa
from    anag_commesse
where   cod_azienda=:s_cs_xx.cod_azienda
and     quan_ordine - quan_in_produzione - quan_assegnata - quan_prodotta > 0
and     data_consegna between :ldt_da_data and :ldt_a_data;

open righe_com_1;

do while 1=1
	fetch righe_com_1 
	into  :ls_cod_prodotto_com,
			:ls_cod_versione,
			:ldd_quan_c,
			:li_anno_commessa,
			:ll_num_commessa;
			
	if sqlca.sqlcode<>0 then 
		choose case sqlca.sqlcode
			case 100
				exit

			case is <0
				g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
				exit
				
		end choose
	end if
	
	if isnull(ldd_quan_c) then ldd_quan_c = 0
	
	select cod_azienda
	into   :ls_test
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa
	and    num_commessa=:ll_num_commessa;
	
	if sqlca.sqlcode = 0 then continue
		
	f_elimina_array(ls_materie_prime[])
	
	li_risposta = f_trova_mat_prima(ls_cod_prodotto_com, ls_cod_versione,ls_materie_prime[], & 
											  ldd_quan_utilizzo[],ldd_quan_c,li_anno_commessa,ll_num_commessa)
		
	for ll_mp = 1 to upperbound(ls_materie_prime[])
		lb_flag_trovato = false
	
		for ll_t = 1 to upperbound(ls_cod_prodotto[])
			 if ls_cod_prodotto[ll_t] = ls_materie_prime[ll_mp] then
			    lb_flag_trovato = true
				 ldd_quan_necessaria[ll_t]=ldd_quan_necessaria[ll_t]+ldd_quan_utilizzo[ll_mp]
				 exit
			 end if
		next
	
		if lb_flag_trovato=false then
			ll_i=upperbound(ls_cod_prodotto[])
			ll_i++
		
			select saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita & 
					 quan_assegnata - quan_anticipi - quan_in_spedizione
			into   :ldd_disp
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_materie_prime[ll_mp];

			if isnull(ldd_disp) then ldd_disp=0

		select sum(giacenza_stock)
		into   :ldd_giacenza_stock
		from   stock
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_materie_prime[ll_mp];
			
		
		
   	ldd_giacenza[ll_i]=ldd_giacenza_stock			
		ls_cod_prodotto[ll_i]=ls_materie_prime[ll_mp]
		ldd_disponibilita[ll_i]=ldd_disp
		ldd_quan_necessaria[ll_i]=ldd_quan_utilizzo[ll_mp]
		ldd_quan_ordine_acq[ll_i] = 0
		ldd_quan_ordinare[ll_i] = 0

		end if
	next
loop

close righe_com_1;

ll_i = 1

for ll_t = 1 to upperbound(ls_cod_prodotto[])

//	ldd_quan_ordinare[ll_t] = 
//	ldd_disponibilita[ll_t] + 
//	ldd_quan_ordine_ven[ll_t] - ldd_quan_ordine_acq[ll_t] + ldd_quan_commesse[ll_t]
//	
//	if cbx_maggiore_zero.checked = true  and ldd_quan_ordinare[ll_t] <= 0 then continue
	
	dw_lista_mrp.insertrow(0)
	dw_lista_mrp.setitem(ll_i,"cod_prodotto",ls_cod_prodotto[ll_t])

	select des_prodotto,
			 flag_sotto_scorta,
			 scorta_minima
	into   :ls_des_prodotto,
			 :ls_flag_sotto_scorta,
			 :ldd_scorta_minima
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto[ll_t];
	
//	if ls_flag_sotto_scorta = "S" then
//		ldd_quan_ripristino = ldd_quan_ordinare[ll_t] + ldd_scorta_minima
//	else
//		ldd_quan_ripristino = ldd_quan_ordinare[ll_t]
//	end if
	
	dw_lista_mrp.setitem(ll_i,"des_prodotto",ls_des_prodotto)
	dw_lista_mrp.setitem(ll_i,"quan_necessaria",ldd_quan_necessaria[ll_t])
	dw_lista_mrp.setitem(ll_i,"quan_disponibile",ldd_disponibilita[ll_t])
	dw_lista_mrp.setitem(ll_i,"quan_ordine_fornitore",ldd_quan_ordine_acq[ll_t])
	dw_lista_mrp.setitem(ll_i,"giacenza",ldd_giacenza[ll_t])
	ll_i ++
next

dw_lista_mrp.resetupdate()			
dw_lista_mrp.accepttext()
parent.triggerevent("pc_retrieve")
setpointer(arrow!)
end event

type em_da_data from editmask within w_lista_mrp
int X=298
int Y=1301
int Width=389
int Height=81
int TabOrder=40
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_a_data from editmask within w_lista_mrp
int X=1029
int Y=1301
int Width=412
int Height=81
int TabOrder=30
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData="À"
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_lista_mrp
int X=23
int Y=1301
int Width=247
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Da data:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_lista_mrp
int X=755
int Y=1301
int Width=247
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="A data:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

