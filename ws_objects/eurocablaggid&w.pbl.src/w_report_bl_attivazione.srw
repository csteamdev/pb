﻿$PBExportHeader$w_report_bl_attivazione.srw
$PBExportComments$Window report bolla di lavoro personalizzata
forward
global type w_report_bl_attivazione from w_cs_xx_principale
end type
type dw_report_bolle_lavoro from uo_cs_xx_dw within w_report_bl_attivazione
end type
end forward

global type w_report_bl_attivazione from w_cs_xx_principale
integer width = 3735
integer height = 4416
string title = "Bolla di Lavorazione"
boolean hscrollbar = true
boolean vscrollbar = true
dw_report_bolle_lavoro dw_report_bolle_lavoro
end type
global w_report_bl_attivazione w_report_bl_attivazione

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();long    ll_num_commessa,ll_prog_riga,ll_num_registrazione,ll_i,ll_t,ll_prog_stock
integer li_anno_commessa,li_anno_registrazione
string  ls_anno_commessa,ls_num_commessa,ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione, &
		  ls_prog_riga,ls_buffer,ls_note_fase,ls_quan_in_produzione,ls_cod_prodotto_finito, & 
		  ls_des_prodotto_finito,ls_cod_materia_prima,ls_des_materia_prima,ls_cod_prodotto_1, &
		  ls_cod_cliente,ls_rag_soc_cliente,ls_cod_barre_1,ls_cod_barre_2,ls_cod_barre_3,ls_cod_mezzo, & 
		  ls_des_mezzo,ls_des_prodotto,ls_mat_prime,ls_cod_deposito,ls_des_deposito,ls_cod_lotto, & 
		  ls_cod_ubicazione,ls_cod_prodotto_variante,ldd_quan_utilizzo_variante,ls_num_ord_cliente,ls_cod_versione, &
		  ls_cod_vettore, ls_des_vettore, ls_localita
double  ldd_quan_in_produzione,ldd_quan_in_produzione_mp,ldd_tempo_lavorazione,ldd_tempo_attrezzaggio,ldd_quan_in_produzione_sl
datetime ldt_data_ord_cliente,ldt_data_consegna,ldt_data_stock

//li_anno_commessa = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(), "anno_commessa")
//ll_num_commessa = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(), "num_commessa")
//ll_prog_riga = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(), "prog_riga")

li_anno_commessa = s_cs_xx.parametri.parametro_i_1
ll_num_commessa = s_cs_xx.parametri.parametro_dec4_1
ll_prog_riga = s_cs_xx.parametri.parametro_i_2

setnull(s_cs_xx.parametri.parametro_i_1)
setnull(s_cs_xx.parametri.parametro_dec4_1)
setnull(s_cs_xx.parametri.parametro_i_2)


setnull(ls_cod_cliente)
setnull(li_anno_registrazione)
dw_report_bolle_lavoro.reset()

select anno_registrazione,
		 num_registrazione
into   :li_anno_registrazione,
		 :ll_num_registrazione
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode=0 then
	select cod_cliente,
			 num_ord_cliente,
			 data_ord_cliente,	 
			 cod_mezzo,
			 cod_vettore,
			 localita
   into   :ls_cod_cliente,
			 :ls_num_ord_cliente,
			 :ldt_data_ord_cliente,
			 :ls_cod_mezzo,
			 :ls_cod_vettore,
			 :ls_localita
	from   tes_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;

	select rag_soc_1
	into   :ls_rag_soc_cliente
	from   anag_clienti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cliente=:ls_cod_cliente;

	select des_mezzo
	into   :ls_des_mezzo
	from   tab_mezzi
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_mezzo=:ls_cod_mezzo;

	if isnull(ls_cod_vettore) then
		 setnull(ls_des_vettore)
	else
		select rag_soc_1
		into   :ls_des_vettore
		from   anag_vettori
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_vettore=:ls_cod_vettore;
		
		if sqlca.sqlcode <> 0 then setnull(ls_des_vettore)
	end if
	
end if
	
select cod_prodotto,
		 quan_in_produzione,
		 data_consegna,
		 cod_versione
into   :ls_cod_prodotto_finito,
		 :ldd_quan_in_produzione,
		 :ldt_data_consegna,	
		 :ls_cod_versione
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

select des_prodotto
into   :ls_des_prodotto_finito
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto_finito;
	
ls_anno_commessa = string(li_anno_commessa)
ls_num_commessa = string(ll_num_commessa)
ls_prog_riga = string(ll_prog_riga)
ls_quan_in_produzione= string(ldd_quan_in_produzione)

//if len(ls_num_commessa) < 6 then
//	ls_num_commessa = ls_num_commessa + fill("_", 6 - len(ls_num_commessa))
//end if

declare  righe_avanzamento_pc_1 cursor for
select   cod_prodotto,
		   cod_reparto,
		   cod_lavorazione,
			quan_in_produzione
from     avan_produzione_com
where    cod_azienda=:s_cs_xx.cod_azienda
and      anno_commessa=:li_anno_commessa
and      num_commessa=:ll_num_commessa
and      prog_riga=:ll_prog_riga
order by cod_prodotto;

open righe_avanzamento_pc_1;
	
do while 1=1
	fetch righe_avanzamento_pc_1
	into  :ls_cod_prodotto,	
  		   :ls_cod_reparto,
			:ls_cod_lavorazione,
			:ldd_quan_in_produzione_sl;

	if sqlca.sqlcode = 100 then exit
	
	dw_report_bolle_lavoro.insertrow(0)
	ll_i = dw_report_bolle_lavoro.rowcount()

	//if ll_i = 1 then
		dw_report_bolle_lavoro.setitem(ll_i,"anno_commessa",ls_anno_commessa)
		dw_report_bolle_lavoro.setitem(ll_i,"num_commessa",ls_num_commessa)
		dw_report_bolle_lavoro.setitem(ll_i,"prog_riga",string(ll_prog_riga))
		dw_report_bolle_lavoro.setitem(ll_i,"cod_prodotto",ls_cod_prodotto_finito)
		dw_report_bolle_lavoro.setitem(ll_i,"des_prodotto",ls_des_prodotto_finito)
		dw_report_bolle_lavoro.setitem(ll_i,"data_consegna",left(string(ldt_data_consegna),10))
		dw_report_bolle_lavoro.setitem(ll_i,"mezzo",ls_des_mezzo)
		dw_report_bolle_lavoro.setitem(ll_i,"vettore",ls_des_vettore)
		dw_report_bolle_lavoro.setitem(ll_i, "destinazione", ls_localita)
		dw_report_bolle_lavoro.setitem(ll_i,"quan_in_produzione",ls_quan_in_produzione)
		dw_report_bolle_lavoro.setitem(ll_i,"anno_ordine",string(li_anno_registrazione))
		dw_report_bolle_lavoro.setitem(ll_i,"num_ordine",string(ll_num_registrazione))
		dw_report_bolle_lavoro.setitem(ll_i,"cod_cliente",ls_cod_cliente)
		dw_report_bolle_lavoro.setitem(ll_i,"rag_sociale",ls_rag_soc_cliente)
		dw_report_bolle_lavoro.setitem(ll_i,"num_ord_cliente",string(ls_num_ord_cliente))
		dw_report_bolle_lavoro.setitem(ll_i,"data_ord_cliente",left(string(ldt_data_ord_cliente),10))
	//end if

	ls_mat_prime = ""
	ls_des_materia_prima = ""

	select des_prodotto 
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;

	dw_report_bolle_lavoro.setitem(ll_i,"cod_semilavorato",ls_cod_prodotto)
	dw_report_bolle_lavoro.setitem(ll_i,"des_semilavorato",ls_des_prodotto)

	ls_cod_prodotto_1 = trim(ls_cod_prodotto)

	declare righe_db cursor for
	select  cod_prodotto_figlio
	from    distinta
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     cod_prodotto_padre=:ls_cod_prodotto_1
	and     cod_versione=:ls_cod_versione;

	open righe_db;
	
	do while 1=1
		fetch righe_db
		into  :ls_cod_materia_prima;

		if sqlca.sqlcode = 100 or sqlca.sqlcode <0 then exit

		select cod_prodotto
		into   :ls_cod_prodotto_variante
		from   varianti_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    cod_prodotto_padre=:ls_cod_prodotto_1
		and    cod_prodotto_figlio =:ls_cod_materia_prima;

		if sqlca.sqlcode = 0 and not isnull(ls_cod_prodotto_variante) then
			ls_cod_materia_prima = ls_cod_prodotto_variante
		end if

		select des_prodotto
		into   :ls_des_materia_prima
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_materia_prima;

		select quan_in_produzione
		into   :ldd_quan_in_produzione_mp
		from   mat_prime_commessa
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa 
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga;
		
		ls_mat_prime = ls_mat_prime + "Materia Prima:" + ls_cod_materia_prima + " - " + ls_des_materia_prima + char(13)	

	loop

	close righe_db;

	dw_report_bolle_lavoro.setitem(ll_i,"mat_prime",ls_mat_prime)

	select des_estesa_prodotto,
			 tempo_lavorazione,
			 tempo_attrezzaggio
	into   :ls_note_fase,
			 :ldd_tempo_lavorazione,
			 :ldd_tempo_attrezzaggio
	from   tes_fasi_lavorazione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto
	and    cod_reparto=:ls_cod_reparto
	and    cod_lavorazione=:ls_cod_lavorazione;


//	if len(ls_prog_riga) < 4 then
//		ls_prog_riga=ls_prog_riga + fill("_", 4 - len(ls_prog_riga))
//	end if
//			
//	if len(ls_cod_prodotto) < 15 then
//		ls_cod_prodotto=ls_cod_prodotto + fill("_", 15 - len(ls_cod_prodotto))
//	end if
//
//	if len(ls_cod_reparto) < 6 then
//		ls_cod_reparto=ls_cod_reparto + fill("_", 6 - len(ls_cod_reparto))
//	end if
//	
//	if len(ls_cod_lavorazione) < 6 then
//		ls_cod_lavorazione=ls_cod_lavorazione + fill("_", 6 - len(ls_cod_lavorazione))
//	end if

	ls_cod_barre_1 = "*" + ls_anno_commessa + ls_num_commessa + ls_prog_riga + "*"
	ls_cod_barre_2 = "*" + ls_cod_prodotto + "*"
	ls_cod_barre_3 = "*" + ls_cod_reparto + ls_cod_lavorazione + "*"

	dw_report_bolle_lavoro.setitem(ll_i,"des_fase",ls_note_fase)
	dw_report_bolle_lavoro.setitem(ll_i,"quan_in_produzione_sl",string(ldd_quan_in_produzione_sl))
	dw_report_bolle_lavoro.setitem(ll_i,"tempo",string(ldd_tempo_lavorazione*ldd_quan_in_produzione_sl))
	dw_report_bolle_lavoro.setitem(ll_i,"tempo_1",string(ldd_tempo_attrezzaggio))
	dw_report_bolle_lavoro.setitem(ll_i,"cod_barre_1",ls_cod_barre_1)
	dw_report_bolle_lavoro.setitem(ll_i,"cod_barre_2",ls_cod_barre_2)
	dw_report_bolle_lavoro.setitem(ll_i,"cod_barre_3",ls_cod_barre_3)

loop

close righe_avanzamento_pc_1;

dw_report_bolle_lavoro.resetupdate()

return 0
end function

on w_report_bl_attivazione.create
int iCurrent
call super::create
this.dw_report_bolle_lavoro=create dw_report_bolle_lavoro
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_bolle_lavoro
end on

on w_report_bl_attivazione.destroy
call super::destroy
destroy(this.dw_report_bolle_lavoro)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noresizewin)

dw_report_bolle_lavoro.ib_dw_report=true

dw_report_bolle_lavoro.set_dw_options(sqlca, &
                                 c_nulldw, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
					c_disablecc, 	&
					c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)

iuo_dw_main = dw_report_bolle_lavoro

save_on_close(c_socnosave)
end event

type dw_report_bolle_lavoro from uo_cs_xx_dw within w_report_bl_attivazione
integer x = 23
integer y = 20
integer width = 3520
integer height = 4160
string dataobject = "r_report_bl_attivazione"
end type

event pcd_last;call super::pcd_last;wf_report()
parent.triggerevent("pc_retrieve")
end event

event pcd_first;call super::pcd_first;wf_report()
parent.triggerevent("pc_retrieve")
end event

event pcd_next;call super::pcd_next;wf_report()
parent.triggerevent("pc_retrieve")
end event

