﻿$PBExportHeader$w_report_bol_ven.srw
$PBExportComments$Finestra Report Bolla di Vendita
forward
global type w_report_bol_ven from w_cs_xx_principale
end type
type dw_report_bol_ven from uo_cs_xx_dw within w_report_bol_ven
end type
type ddlb_1 from dropdownlistbox within w_report_bol_ven
end type
type cb_1 from commandbutton within w_report_bol_ven
end type
end forward

global type w_report_bol_ven from w_cs_xx_principale
integer width = 4338
integer height = 4772
string title = "Stampa Bolle Uscita"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
dw_report_bol_ven dw_report_bol_ven
ddlb_1 ddlb_1
cb_1 cb_1
end type
global w_report_bol_ven w_report_bol_ven

type variables
boolean ib_modifica=false, ib_nuovo=false
long il_anno_registrazione, il_num_registrazione
end variables

forward prototypes
public subroutine wf_report ()
public function integer wf_imposta_filigrana ()
end prototypes

public subroutine wf_report ();boolean lb_prima_riga, lb_flag_prodotto_cliente, lb_flag_nota_piede, lb_flag_nota_dettaglio
string ls_cod_tipo_bol_ven, ls_cod_cliente, ls_cod_porto, ls_cod_resa, &
		 ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		 ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, &
		 ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
		 ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, ls_des_causale,&
		 ls_des_tipo_bol_ven, ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_cod_causale,&
		 ls_des_resa_lingua, ls_des_prodotto_anag, ls_flag_stampa_bolla, ls_cod_tipo_det_ven, &
		 ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_cod_documento, ls_flag_tipo_det_ven, &
		 ls_numeratore_documento, ls_cod_vettore, ls_cod_inoltro, ls_causale_trasporto, &
		 ls_aspetto_beni, ls_rag_soc_1_vet, ls_rag_soc_2_vet, ls_indirizzo_vet, &
		 ls_localita_vet, ls_frazione_vet, ls_cap_vet, ls_provincia_vet, ls_des_tipo_det_ven, &
		 ls_rag_soc_1_ino, ls_rag_soc_2_ino, ls_indirizzo_ino, ls_cod_mezzo, ls_des_mezzo, &
		 ls_localita_ino, ls_frazione_ino, ls_cap_ino, ls_provincia_ino, ls_cod_fornitore, &
		 ls_cod_deposito_tras, ls_cod_fil_fornitore, ls_flag_tipo_bol_ven, ls_des_deposito, ls_stato_cli, ls_num_civico
		 
long ll_errore, ll_anno_documento, &
	  ll_num_documento, ll_num_colli
double ld_quan_consegnata, ld_fat_conversione_ven, ld_peso_lordo
datetime ldt_data_bolla, ldt_data_inizio_trasporto, ldt_ora_inizio_trasporto


dw_report_bol_ven.reset()

select tes_bol_ven.cod_tipo_bol_ven,   
		 tes_bol_ven.cod_cliente,   
		 tes_bol_ven.cod_porto,   
		 tes_bol_ven.cod_resa,   
		 tes_bol_ven.nota_testata,   
		 tes_bol_ven.nota_piede,   
		 tes_bol_ven.rag_soc_1,   
		 tes_bol_ven.rag_soc_2,   
		 tes_bol_ven.indirizzo,   
		 tes_bol_ven.localita,   
		 tes_bol_ven.frazione,   
		 tes_bol_ven.cap,   
		 tes_bol_ven.provincia, 
		 tes_bol_ven.cod_documento,
		 tes_bol_ven.numeratore_documento,
		 tes_bol_ven.anno_documento,
		 tes_bol_ven.num_documento,
		 tes_bol_ven.data_bolla,
		 tes_bol_ven.cod_vettore,
		 tes_bol_ven.cod_inoltro,
		 tes_bol_ven.causale_trasporto,
		 tes_bol_ven.aspetto_beni,
		 tes_bol_ven.num_colli,
		 tes_bol_ven.cod_fornitore,
		 tes_bol_ven.cod_deposito_tras,
		 tes_bol_ven.cod_fil_fornitore,
		 tes_bol_ven.cod_mezzo,
		 tes_bol_ven.peso_lordo,
		 tes_bol_ven.cod_causale,
		 tes_bol_ven.data_inizio_trasporto,
		 tes_bol_ven.ora_inizio_trasporto
into   :ls_cod_tipo_bol_ven,   
	 	 :ls_cod_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ls_cod_documento,
		 :ls_numeratore_documento,
		 :ll_anno_documento,
		 :ll_num_documento,
		 :ldt_data_bolla,
		 :ls_cod_vettore,
		 :ls_cod_inoltro,
		 :ls_causale_trasporto,
		 :ls_aspetto_beni,
		 :ll_num_colli,
		 :ls_cod_fornitore,
		 :ls_cod_deposito_tras,
		 :ls_cod_fil_fornitore,
		 :ls_cod_mezzo,
		 :ld_peso_lordo,
		 :ls_cod_causale,
 		 :ldt_data_inizio_trasporto,
		 :ldt_ora_inizio_trasporto
from   tes_bol_ven  
where  tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_bol_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_bol_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_bol_ven)
	setnull(ls_cod_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	setnull(ls_cod_documento)
	setnull(ls_numeratore_documento)
	setnull(ll_anno_documento)
	setnull(ll_num_documento)
	setnull(ldt_data_bolla)
	setnull(ls_cod_vettore)
	setnull(ls_cod_inoltro)
	setnull(ls_causale_trasporto)
	setnull(ls_aspetto_beni)
	setnull(ll_num_colli)
	setnull(ls_cod_fornitore)
	setnull(ls_cod_deposito_tras)
	setnull(ls_cod_mezzo)
	setnull(ls_cod_causale)
	setnull(ldt_data_inizio_trasporto)
	setnull(ldt_ora_inizio_trasporto)
end if

select	anag_clienti.rag_soc_1,   
		anag_clienti.rag_soc_2,   
		anag_clienti.indirizzo,   
		anag_clienti.localita,   
		anag_clienti.frazione,   
		anag_clienti.cap,   
		anag_clienti.provincia,   
		anag_clienti.partita_iva,   
		anag_clienti.cod_fiscale,   
		anag_clienti.cod_lingua,   
		anag_clienti.flag_tipo_cliente,
		anag_clienti.stato,
		anag_clienti.fatel_num_civico
into	:ls_rag_soc_1_cli,   
		:ls_rag_soc_2_cli,   
		:ls_indirizzo_cli,   
		:ls_localita_cli,   
		:ls_frazione_cli,   
		:ls_cap_cli,   
		:ls_provincia_cli,   
		:ls_partita_iva,   
		:ls_cod_fiscale,   
		:ls_cod_lingua,   
		:ls_flag_tipo_cliente,
		:ls_stato_cli,
		:ls_num_civico
from   anag_clienti  
where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
       anag_clienti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_cli)
	setnull(ls_rag_soc_2_cli)
	setnull(ls_indirizzo_cli)
	setnull(ls_localita_cli)
	setnull(ls_frazione_cli)
	setnull(ls_cap_cli)
	setnull(ls_provincia_cli)
	setnull(ls_partita_iva)
	setnull(ls_cod_fiscale)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_cliente)
	setnull(ls_stato_cli)
	setnull(ls_num_civico)
else
	if ls_flag_tipo_cliente = 'E' or ls_flag_tipo_cliente = 'C' or ls_flag_tipo_cliente = 'P' then
		ls_partita_iva = ls_cod_fiscale
		if ls_flag_tipo_cliente = 'P' then 
			dw_report_bol_ven.object.t_partita_iva.text = "C.F."
		else
			dw_report_bol_ven.object.t_partita_iva.text = "P.IVA / VAT"
		end if
	end if
	if not isnull(ls_num_civico) and len(ls_num_civico) > 0 then
		ls_indirizzo_cli = g_str.format("$1 $2",ls_indirizzo_cli, ls_num_civico)
	end if
end if

if not isnull(ls_cod_fornitore) then
	
	select anag_fornitori.rag_soc_1,   
			 anag_fornitori.rag_soc_2,   
			 anag_fornitori.indirizzo,   
			 anag_fornitori.localita,   
			 anag_fornitori.frazione,   
			 anag_fornitori.cap,   
			 anag_fornitori.provincia,   
			 anag_fornitori.partita_iva,   
			 anag_fornitori.cod_fiscale,   
			 anag_fornitori.cod_lingua,   
			 anag_fornitori.flag_tipo_fornitore,
			 anag_fornitori.stato,
			 anag_fornitori.fatel_num_civico
	into   :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,   
			 :ls_flag_tipo_cliente,
			 :ls_stato_cli,
			 :ls_num_civico
	from   anag_fornitori
	where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
			 anag_fornitori.cod_fornitore = :ls_cod_fornitore;
	
	if sqlca.sqlcode <> 0 then
		setnull(ls_rag_soc_1_cli)
		setnull(ls_rag_soc_2_cli)
		setnull(ls_indirizzo_cli)
		setnull(ls_localita_cli)
		setnull(ls_frazione_cli)
		setnull(ls_cap_cli)
		setnull(ls_provincia_cli)
		setnull(ls_partita_iva)
		setnull(ls_cod_fiscale)
		setnull(ls_cod_lingua)
		setnull(ls_flag_tipo_cliente)
		setnull(ls_stato_cli)
		setnull(ls_num_civico)
	else
		if ls_flag_tipo_cliente = 'E' then
			ls_partita_iva = ls_cod_fiscale
		end if
		if not isnull(ls_num_civico) and len(ls_num_civico) > 0 then
			ls_indirizzo_cli = g_str.format("$1 $2",ls_indirizzo_cli, ls_num_civico)
		end if
	end if
	
	if not isnull(ls_cod_fil_fornitore) then
		setnull(ls_stato_cli)
		
		select anag_fil_fornitori.rag_soc_1,   
				 anag_fil_fornitori.rag_soc_2,   
				 anag_fil_fornitori.indirizzo,   
				 anag_fil_fornitori.localita,   
				 anag_fil_fornitori.frazione,   
				 anag_fil_fornitori.cap,   
				 anag_fil_fornitori.provincia
		into   :ls_rag_soc_1_cli,   
				 :ls_rag_soc_2_cli,   
				 :ls_indirizzo_cli,   
				 :ls_localita_cli,   
				 :ls_frazione_cli,   
				 :ls_cap_cli,   
				 :ls_provincia_cli
		from   anag_fil_fornitori
		where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
				 anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and
				 anag_fil_fornitori.cod_fil_fornitore = :ls_cod_fil_fornitore;
		
		if sqlca.sqlcode <> 0 then
			setnull(ls_rag_soc_1_cli)
			setnull(ls_rag_soc_2_cli)
			setnull(ls_indirizzo_cli)
			setnull(ls_localita_cli)
			setnull(ls_frazione_cli)
			setnull(ls_cap_cli)
			setnull(ls_provincia_cli)
		end if
	end if
end if


if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if


select tab_tipi_bol_ven.des_tipo_bol_ven,
		 tab_tipi_bol_ven.flag_tipo_bol_ven
into   :ls_des_tipo_bol_ven, 
		 :ls_flag_tipo_bol_ven
from   tab_tipi_bol_ven  
where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_bol_ven)
	setnull(ls_flag_tipo_bol_ven)
end if

if ls_flag_tipo_bol_ven = "T" then
   dw_report_bol_ven.modify("st_cliente_fornitore_dep.text='Deposito:'")

	ls_rag_soc_1_cli = ls_rag_soc_1
	ls_rag_soc_2_cli = ls_rag_soc_2   
	ls_indirizzo_cli = ls_indirizzo
	ls_localita_cli = ls_localita
	ls_frazione_cli = ls_frazione
	ls_cap_cli = ls_cap
	ls_provincia_cli = ls_provincia
elseif ls_flag_tipo_bol_ven = "V" then
   dw_report_bol_ven.modify("st_cliente_fornitore_dep.text='Cliente:'")
elseif ls_flag_tipo_bol_ven = "R" or &
		 ls_flag_tipo_bol_ven = "C" then
   dw_report_bol_ven.modify("st_cliente_fornitore_dep.text='Fornitore:'")
end if

select tab_mezzi.des_mezzo  
into   :ls_des_mezzo  
from   tab_mezzi 
where  tab_mezzi.cod_azienda = :s_cs_xx.cod_azienda and
       tab_mezzi.cod_mezzo = :ls_cod_mezzo;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo)
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

select anag_vettori.rag_soc_1,   
		 anag_vettori.rag_soc_2,   
		 anag_vettori.indirizzo,   
		 anag_vettori.localita,   
		 anag_vettori.frazione,   
		 anag_vettori.cap,   
		 anag_vettori.provincia
into   :ls_rag_soc_1_vet,   
		 :ls_rag_soc_2_vet,   
		 :ls_indirizzo_vet,   
		 :ls_localita_vet,   
		 :ls_frazione_vet,   
		 :ls_cap_vet,   
		 :ls_provincia_vet  
from   anag_vettori  
where  anag_vettori.cod_azienda = :s_cs_xx.cod_azienda and
		 anag_vettori.cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_vet)
	setnull(ls_rag_soc_2_vet)
	setnull(ls_indirizzo_vet)
	setnull(ls_localita_vet)
	setnull(ls_frazione_vet)
	setnull(ls_cap_vet)
	setnull(ls_provincia_vet)
end if

select anag_vettori.rag_soc_1,   
		 anag_vettori.rag_soc_2,   
		 anag_vettori.indirizzo,   
		 anag_vettori.localita,   
		 anag_vettori.frazione,   
		 anag_vettori.cap,   
		 anag_vettori.provincia
into   :ls_rag_soc_1_ino,   
		 :ls_rag_soc_2_ino,   
		 :ls_indirizzo_ino,   
		 :ls_localita_ino,   
		 :ls_frazione_ino,   
		 :ls_cap_ino,   
		 :ls_provincia_ino  
from   anag_vettori  
where  anag_vettori.cod_azienda = :s_cs_xx.cod_azienda and
		 anag_vettori.cod_vettore = :ls_cod_inoltro;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_ino)
	setnull(ls_rag_soc_2_ino)
	setnull(ls_indirizzo_ino)
	setnull(ls_localita_ino)
	setnull(ls_frazione_ino)
	setnull(ls_cap_ino)
	setnull(ls_provincia_ino)
end if

declare cu_dettagli cursor for 
	select   det_bol_ven.cod_tipo_det_ven, 
				det_bol_ven.cod_misura, 
				det_bol_ven.quan_consegnata, 
				det_bol_ven.nota_dettaglio, 
				det_bol_ven.cod_prodotto, 
				det_bol_ven.des_prodotto,
				det_bol_ven.fat_conversione_ven
	from     det_bol_ven 
	where    det_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				det_bol_ven.anno_registrazione = :il_anno_registrazione and 
				det_bol_ven.num_registrazione = :il_num_registrazione
	order by det_bol_ven.cod_azienda, 
				det_bol_ven.anno_registrazione, 
				det_bol_ven.num_registrazione,
				det_bol_ven.prog_riga_bol_ven;

open cu_dettagli;


if not isnull(ls_nota_testata) and len(trim(ls_nota_testata)) > 0 then lb_prima_riga = true
lb_flag_prodotto_cliente = false
lb_flag_nota_dettaglio = false
lb_flag_nota_piede = false
do while 0 = 0
		if not(lb_prima_riga) and not(lb_flag_prodotto_cliente) and not(lb_flag_nota_dettaglio) then
			fetch cu_dettagli into :ls_cod_tipo_det_ven, 
										  :ls_cod_misura, 
										  :ld_quan_consegnata, 
										  :ls_nota_dettaglio, 
										  :ls_cod_prodotto, 
										  :ls_des_prodotto,
										  :ld_fat_conversione_ven;
		
			if sqlca.sqlcode <> 0 then 
				if not isnull(ls_nota_piede) and len(trim(ls_nota_piede)) > 0 then
					lb_flag_nota_piede = true
				else
					exit
				end if
			end if
		end if
		if len(ls_cod_tipo_det_ven) < 1 then setnull(ls_cod_tipo_det_ven)
		if len(ls_cod_misura) < 1 then setnull(ls_cod_misura)
		if len(ls_nota_dettaglio) < 1 then setnull(ls_nota_dettaglio)
		if len(ls_cod_prodotto) < 1 then setnull(ls_cod_prodotto)
		if len(ls_des_prodotto) < 1 then setnull(ls_des_prodotto)
	
		
		select tab_tipi_det_ven.flag_stampa_bolla,
		       tab_tipi_det_ven.des_tipo_det_ven,
				 tab_tipi_det_ven.flag_tipo_det_ven
		into   :ls_flag_stampa_bolla,
		       :ls_des_tipo_det_ven,
				 :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag_stampa_bolla)
		end if
	
		if lb_prima_riga then
			dw_report_bol_ven.insertrow(0)
			dw_report_bol_ven.setrow(dw_report_bol_ven.rowcount())
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_nota_testata)
		elseif lb_flag_prodotto_cliente then
			dw_report_bol_ven.insertrow(0)
			dw_report_bol_ven.setrow(dw_report_bol_ven.rowcount())
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_cod_prodotto", "Vs Codice")
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_cod_prod_cliente)
			lb_flag_prodotto_cliente = false
		elseif (lb_flag_nota_dettaglio) then
			dw_report_bol_ven.insertrow(0)
			dw_report_bol_ven.setrow(dw_report_bol_ven.rowcount())
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_nota_dettaglio)
			lb_flag_nota_dettaglio = false
		elseif lb_flag_nota_piede then
			dw_report_bol_ven.insertrow(0)
			dw_report_bol_ven.setrow(dw_report_bol_ven.rowcount())
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_nota_piede)
		else
			if ls_flag_stampa_bolla = 'S' then
				dw_report_bol_ven.insertrow(0)
				dw_report_bol_ven.setrow(dw_report_bol_ven.rowcount())
				if not isnull(ls_cod_prodotto) then
					select anag_prodotti.des_prodotto  
					into   :ls_des_prodotto_anag  
					from   anag_prodotti  
					where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
					
					if sqlca.sqlcode <> 0 then
						setnull(ls_des_prodotto_anag)
					end if
				end if
		
				if not isnull(ls_cod_lingua) then
					select anag_prodotti_lingue.des_prodotto  
					into   :ls_des_prodotto_lingua  
					from   anag_prodotti_lingue  
					where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
							 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
					
					if sqlca.sqlcode <> 0 then
						setnull(ls_des_prodotto_lingua)
					end if
				end if
		
				select tab_prod_clienti.cod_prod_cliente  
				into   :ls_cod_prod_cliente  
				from   tab_prod_clienti  
				where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
						 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
						 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
		
				if sqlca.sqlcode <> 0 then
					setnull(ls_cod_prod_cliente)
				end if
		
				if not isnull(ls_cod_fornitore) then
					select tab_prod_fornitori.cod_prod_fornitore  
					into   :ls_cod_prod_cliente
					from   tab_prod_fornitori  
					where  tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda and  
							 tab_prod_fornitori.cod_prodotto = :ls_cod_prodotto and   
							 tab_prod_fornitori.cod_fornitore = :ls_cod_fornitore;
			
					if sqlca.sqlcode <> 0 then
						setnull(ls_cod_prod_cliente)
					end if
				end if
				
				dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_cod_misura", ls_cod_misura)
				dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_quan_consegnata", ld_quan_consegnata * ld_fat_conversione_ven)
				dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_cod_prodotto", ls_cod_prodotto)

				if ((ls_flag_tipo_det_ven = "M") or (ls_flag_tipo_det_ven = "C")) and (lb_flag_nota_dettaglio) then
					dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_nota_dettaglio)
				elseif not isnull(ls_cod_lingua) and not isnull(ls_des_prodotto_lingua) then
					dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
				elseif not isnull(ls_des_prodotto) then
					dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_des_prodotto)
				elseif not isnull(ls_des_prodotto_anag) then
					dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
				else
					ls_des_prodotto_anag = ls_des_tipo_det_ven
					dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
				end if

				if not isnull(ls_cod_prod_cliente) and len(trim(ls_cod_prod_cliente)) > 0 then
					lb_flag_prodotto_cliente = true
				end if
				if not isnull(ls_nota_dettaglio)  and len(trim(ls_nota_dettaglio)) > 0 then
					lb_flag_nota_dettaglio = true
				end if
			end if
		end if		

		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_documento", ls_cod_documento)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_numeratore_documento", ls_numeratore_documento)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_anno_documento", ll_anno_documento)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_num_documento", ll_num_documento)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_tipo_bol_ven", ls_cod_tipo_bol_ven)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_data_bolla", ldt_data_bolla)
		if ls_flag_tipo_bol_ven = "V" then
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_cliente", ls_cod_cliente)
		elseif ls_flag_tipo_bol_ven = "R" or &
				 ls_flag_tipo_bol_ven = "C" then
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_cliente", ls_cod_fornitore)
		elseif ls_flag_tipo_bol_ven = "T" then
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_cliente", ls_cod_deposito_tras)
		end if
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_rag_soc_1", ls_rag_soc_1)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_rag_soc_2", ls_rag_soc_2)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_indirizzo", ls_indirizzo)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_localita", ls_localita)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_frazione", ls_frazione)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cap", ls_cap)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_provincia", ls_provincia)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_nota_testata", ls_nota_testata)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_porto", ls_cod_porto)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_resa", ls_cod_resa)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "data_inizio_trasporto", ldt_data_inizio_trasporto)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "ora_inizio_trasporto", ldt_ora_inizio_trasporto)
		if not isnull(ls_cod_causale) then
			select des_causale
			into   :ls_des_causale
			from   tab_causali_trasp
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_causale = :ls_cod_causale;
			if sqlca.sqlcode = 0 then
				dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_causale_trasporto", ls_des_causale)
			end if
		else
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_causale_trasporto", ls_causale_trasporto)
		end if
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_aspetto_beni", ls_aspetto_beni)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_num_colli", ll_num_colli)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_peso_lordo", ld_peso_lordo)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_des_mezzo", ls_des_mezzo)
		
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_localita", ls_localita_cli)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_frazione", ls_frazione_cli)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_cap", ls_cap_cli)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_provincia", ls_provincia_cli)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_tipi_bol_ven_des_tipo_bol_ven", ls_des_tipo_bol_ven)

		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_porti_des_porto", ls_des_porto)
		
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
		
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_rese_des_resa", ls_des_resa)
		
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
	
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_rag_soc_1", ls_rag_soc_1_vet)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_rag_soc_2", ls_rag_soc_2_vet)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_indirizzo", ls_indirizzo_vet)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_localita", ls_localita_vet)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_frazione", ls_frazione_vet)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_cap", ls_cap_vet)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_provincia", ls_provincia_vet)
	
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_rag_soc_1", ls_rag_soc_1_ino)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_rag_soc_2", ls_rag_soc_2_ino)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_indirizzo", ls_indirizzo_ino)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_localita", ls_localita_ino)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_frazione", ls_frazione_ino)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_cap", ls_cap_ino)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_provincia", ls_provincia_ino)
		
		lb_prima_riga = false
		if lb_flag_nota_piede then exit
		
		setnull(ls_cod_tipo_det_ven)
	   setnull(ls_cod_misura)
	   setnull(ld_quan_consegnata)
	   setnull(ls_cod_prodotto)
	   setnull(ls_des_prodotto)
	   setnull(ld_fat_conversione_ven)
		setnull(ls_des_prodotto_anag)
		
loop
close cu_dettagli;
dw_report_bol_ven.reset_dw_modified(c_resetchildren)

end subroutine

public function integer wf_imposta_filigrana ();string ls_path, ls_error

guo_functions.uof_get_parametro_azienda("FL1", ls_path)

if isnull(ls_path) or ls_path = "" then return -1

if guo_functions.uof_filigrana(s_cs_xx.volume + ls_path, dw_report_bol_ven, true, ls_error) < 0 then
	g_mb.error(ls_error)
	return -1
else
	return 0
end if
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_path_logo_2, ls_modify, ls_tipo_modulo

dw_report_bol_ven.ib_dw_report = true

select parametri_azienda.stringa
into   :ls_tipo_modulo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'SBM';
if sqlca.sqlcode <>0 then
	g_mb.messagebox("Apice","Non trovato il parametro az. SBM, indicante il tipo di modulo da utilizzare; ~r~n verrà utilizzato quello per stampante di pagina")
	ls_tipo_modulo ="PAGINA"
end if

if ls_tipo_modulo ="PAGINA" then
	dw_report_bol_ven.DataObject = 'd_report_bol_ven'
else
	dw_report_bol_ven.DataObject = 'd_report_bol_ven_ascii'
end if


set_w_options(c_noresizewin)

il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2

dw_report_bol_ven.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
												

if ls_tipo_modulo ="PAGINA" then
	// da aggiungere solo su modulo per stampante di pagina
	select parametri_azienda.stringa
	into   :ls_path_logo_1
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LO1';
	
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
	dw_report_bol_ven.modify(ls_modify)
end if

// stefanop: 24/03/2014
wf_imposta_filigrana()

save_on_close(c_socnosave)

end event

on w_report_bol_ven.create
int iCurrent
call super::create
this.dw_report_bol_ven=create dw_report_bol_ven
this.ddlb_1=create ddlb_1
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_bol_ven
this.Control[iCurrent+2]=this.ddlb_1
this.Control[iCurrent+3]=this.cb_1
end on

on w_report_bol_ven.destroy
call super::destroy
destroy(this.dw_report_bol_ven)
destroy(this.ddlb_1)
destroy(this.cb_1)
end on

type dw_report_bol_ven from uo_cs_xx_dw within w_report_bol_ven
integer x = 23
integer y = 20
integer width = 3657
integer height = 4520
integer taborder = 10
string dataobject = "d_report_bol_ven"
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

event pcd_first;call super::pcd_first;wf_report()
end event

event pcd_last;call super::pcd_last;wf_report()
end event

event pcd_next;call super::pcd_next;wf_report()
end event

event pcd_previous;call super::pcd_previous;wf_report()
end event

type ddlb_1 from dropdownlistbox within w_report_bol_ven
integer x = 3703
integer y = 40
integer width = 480
integer height = 240
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
string item[] = {"AGHI","PAGINA"}
end type

event selectionchanged;
//STRING ls_path_logo_1, ls_modify
//
//dw_report_bol_ven.reset()
//
//if index = 1 then	// PAGINA
//	dw_report_bol_ven.DataObject = 'd_report_bol_ven'
//else	// AGHI
//	dw_report_bol_ven.DataObject = 'd_report_bol_ven_ascii'
//end if
//
//
//set_w_options(c_noresizewin)
//
//dw_report_bol_ven.set_dw_options(sqlca, &
//                                 i_openparm, &
//                                 c_nonew + &
//                                 c_nomodify + &
//                                 c_nodelete + &
//                                 c_noenablenewonopen + &
//                                 c_noenablemodifyonopen + &
//                                 c_scrollparent + &
//											c_disablecc, &
//											c_noresizedw + &
//                                 c_nohighlightselected + &
//                                 c_nocursorrowfocusrect + &
//                                 c_nocursorrowpointer)
//												
//
//if index = 1 then
//	// da aggiungere solo su modulo per stampante di pagina
//	select parametri_azienda.stringa
//	into   :ls_path_logo_1
//	from   parametri_azienda
//	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
//			 parametri_azienda.flag_parametro = 'S' and &
//			 parametri_azienda.cod_parametro = 'LO1';
//	
//	ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
//	dw_report_bol_ven.modify(ls_modify)
//end if
//
//w_report_bol_ven.postevent("pc_retrieve")
//
//
end event

event modified;STRING ls_path_logo_1, ls_modify, ls_tipo_modulo

ls_tipo_modulo = THIS.text

dw_report_bol_ven.reset()

if ls_tipo_modulo = "PAGINA" then	// PAGINA
	dw_report_bol_ven.DataObject = 'd_report_bol_ven'
	cb_1.enabled =true
else	// AGHI
	dw_report_bol_ven.DataObject = 'd_report_bol_ven_ascii'
	cb_1.enabled =false
end if


set_w_options(c_noresizewin)

dw_report_bol_ven.set_dw_options(sqlca, &
                                 i_openparm, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
												

if ls_tipo_modulo = "PAGINA" then
	// da aggiungere solo su modulo per stampante di pagina
	select parametri_azienda.stringa
	into   :ls_path_logo_1
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LO1';
	
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
	dw_report_bol_ven.modify(ls_modify)
end if

w_report_bol_ven.postevent("pc_retrieve")


end event

type cb_1 from commandbutton within w_report_bol_ven
integer x = 3726
integer y = 300
integer width = 457
integer height = 80
integer taborder = 22
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa 3 copie"
end type

event clicked;dw_report_bol_ven.triggerevent("pcd_print")
dw_report_bol_ven.triggerevent("pcd_print")
dw_report_bol_ven.triggerevent("pcd_print")
end event

