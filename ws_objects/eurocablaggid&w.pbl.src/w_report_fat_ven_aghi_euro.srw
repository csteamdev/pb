﻿$PBExportHeader$w_report_fat_ven_aghi_euro.srw
$PBExportComments$Finestra Report Fattura ad aghi Eurocablaggi
forward
global type w_report_fat_ven_aghi_euro from w_cs_xx_principale
end type
type dw_report_fat_ven from uo_cs_xx_dw within w_report_fat_ven_aghi_euro
end type
end forward

global type w_report_fat_ven_aghi_euro from w_cs_xx_principale
integer width = 3872
integer height = 4772
string title = "Stampa Fatture Attive"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
dw_report_fat_ven dw_report_fat_ven
end type
global w_report_fat_ven_aghi_euro w_report_fat_ven_aghi_euro

type variables
boolean ib_modifica=false, ib_nuovo=false
end variables

forward prototypes
public subroutine wf_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[])
public subroutine wf_leggi_scadenze (long fl_anno_registrazione, long fl_num_registrazione, ref datetime fdt_data_scadenze[], ref decimal fd_importo_rata[])
public subroutine wf_report ()
end prototypes

public subroutine wf_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[]);string ls_cod_iva
long ll_i

declare cu_iva cursor for 
	select   iva_fat_ven.cod_iva, 
				iva_fat_ven.imponibile_valuta_iva, 
				iva_fat_ven.importo_valuta_iva,
				iva_fat_ven.perc_iva
	from     iva_fat_ven 
	where    iva_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				iva_fat_ven.anno_registrazione = :fl_anno_registrazione and 
				iva_fat_ven.num_registrazione = :fl_num_registrazione;

open cu_iva;

ll_i = 0
do while 0 = 0
	ll_i = ll_i + 1
   fetch cu_iva into :ls_cod_iva, 
							:fd_imponibile_iva_valuta[ll_i], 
							:fd_imposta_iva_valuta[ll_i],
							:fd_aliquote_iva[ll_i];

   if sqlca.sqlcode <> 0 then exit
	if fd_aliquote_iva[ll_i] = 0 then
		select tab_ive.des_iva
		into   :fs_des_esenzione[ll_i]
		from   tab_ive
		where  tab_ive.cod_azienda = :s_cs_xx.cod_azienda and
		       tab_ive.cod_iva = :ls_cod_iva;
	end if
loop

close cu_iva;

return

end subroutine

public subroutine wf_leggi_scadenze (long fl_anno_registrazione, long fl_num_registrazione, ref datetime fdt_data_scadenze[], ref decimal fd_importo_rata[]);long ll_i

declare cu_scadenze cursor for 
	select   scad_fat_ven.data_scadenza, 
				scad_fat_ven.imp_rata_valuta
	from     scad_fat_ven 
	where    scad_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				scad_fat_ven.anno_registrazione = :fl_anno_registrazione and 
				scad_fat_ven.num_registrazione = :fl_num_registrazione;

open cu_scadenze;

ll_i = 0
do while 0 = 0
	ll_i = ll_i + 1
   fetch cu_scadenze into :fdt_data_scadenze[ll_i], 
							     :fd_importo_rata[ll_i];

   if sqlca.sqlcode <> 0 then exit
loop

close cu_scadenze;
return

end subroutine

public subroutine wf_report ();boolean 	lb_prima_riga, lb_flag_prodotto_cliente, lb_flag_nota_dettaglio, lb_flag_nota_piede

string 	ls_stringa, ls_cod_tipo_fat_ven, ls_cod_banca_clien_for, ls_cod_cliente, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, &
		 	ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		 	ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 	ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, &
		 	ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
		 	ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, &
		 	ls_des_tipo_fat_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, &
  		 	ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , ls_flag_tipo_det_ven, &
		 	ls_des_prodotto_anag, ls_flag_stampa_fattura, ls_cod_tipo_det_ven, ls_des_esenzione_iva[],&
		 	ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_cod_iva, ls_des_iva, ls_causale_trasporto, &
		 	ls_cod_vettore, ls_vettore_rag_soc_1, ls_vettore_rag_soc_2, ls_vettore_indirizzo, &
		 	ls_vettore_cap, ls_vettore_localita, ls_vettore_provincia, ls_cod_mezzo, ls_des_mezzo, &
		 	ls_des_mezzo_lingua, ls_flag_tipo_fat_ven, ls_des_tipo_det_ven, ls_cod_agente,&
		 	ls_cod_documento, ls_numeratore_doc, ls_formato
		 
long 		ll_errore, ll_anno_registrazione, ll_num_registrazione, ll_num_colli, ll_num_aliquote, &
     		ll_anno_documento, ll_num_documento, ll_prog_mimytype
	  
dec{4} 	ld_tot_fattura, ld_sconto, ld_quan_fatturata, ld_prezzo_vendita, ld_sconto_1, &
	    	ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7,&
		 	ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_tot, ld_imponibile_riga, &
		 	ld_val_riga, ld_sconto_pagamento, ld_cambio_ven, &
		 	ld_imponibili_valuta[], ld_iva_valuta[],ld_aliquote_iva[] ,ld_rate_scadenze[],&
		 	ld_imponibile_iva_valuta, ld_importo_iva_valuta, ld_perc_iva, ld_tot_merci, &
		 	ld_imponibile_iva, ld_tot_spese_trasporto, ld_tot_spese_varie
			 
dec{5}   ld_fat_conversione_ven
		 
datetime ldt_data_ord_cliente, ldt_data_registrazione, ldt_scadenze[], ldt_data_fattura

blob     l_blob

dw_report_fat_ven.reset()

ll_anno_registrazione = dw_report_fat_ven.i_parentdw.getitemnumber(dw_report_fat_ven.i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = dw_report_fat_ven.i_parentdw.getitemnumber(dw_report_fat_ven.i_parentdw.i_selectedrows[1], "num_registrazione")

select parametri_azienda.stringa  
into  :ls_stringa  
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select tes_fat_ven.cod_tipo_fat_ven,   
		 tes_fat_ven.data_registrazione,   
		 tes_fat_ven.cod_cliente,   
		 tes_fat_ven.cod_valuta,   
		 tes_fat_ven.cod_pagamento,   
		 tes_fat_ven.sconto,   
		 tes_fat_ven.cod_banca_clien_for,   
		 tes_fat_ven.num_ord_cliente,   
		 tes_fat_ven.data_ord_cliente,   
		 tes_fat_ven.cod_porto,   
		 tes_fat_ven.cod_resa,   
		 tes_fat_ven.nota_testata,   
		 tes_fat_ven.nota_piede,   
		 tes_fat_ven.imponibile_iva,
		 tes_fat_ven.importo_iva,
		 tes_fat_ven.tot_fattura,   
		 tes_fat_ven.rag_soc_1,   
		 tes_fat_ven.rag_soc_2,   
		 tes_fat_ven.indirizzo,   
		 tes_fat_ven.localita,   
		 tes_fat_ven.frazione,   
		 tes_fat_ven.cap,   
		 tes_fat_ven.provincia,
		 tes_fat_ven.cambio_ven,
		 tes_fat_ven.num_colli,
		 tes_fat_ven.cod_vettore,
		 tes_fat_ven.causale_trasporto,
		 tes_fat_ven.tot_merci,
		 tes_fat_ven.imponibile_iva,
		 tes_fat_ven.tot_spese_trasporto,
		 tes_fat_ven.cod_mezzo,
		 tes_fat_ven.cod_agente_1,
		 tes_fat_ven.tot_spese_varie,
		 tes_fat_ven.cod_documento,
		 tes_fat_ven.numeratore_documento,
		 tes_fat_ven.anno_documento,
		 tes_fat_ven.num_documento,
		 tes_fat_ven.data_fattura
into   :ls_cod_tipo_fat_ven,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente,   
		 :ls_cod_valuta,   
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_imponibile_iva_valuta,
		 :ld_importo_iva_valuta,
		 :ld_tot_fattura,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ld_cambio_ven,
		 :ll_num_colli,
		 :ls_cod_vettore,
		 :ls_causale_trasporto,
		 :ld_tot_merci,
		 :ld_imponibile_iva,
		 :ld_tot_spese_trasporto,
		 :ls_cod_mezzo,
		 :ls_cod_agente,
		 :ld_tot_spese_varie,
	    :ls_cod_documento,
		 :ls_numeratore_doc,
		 :ll_anno_documento,
		 :ll_num_documento,
		 :ldt_data_fattura
from   tes_fat_ven  
where  tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_fat_ven.anno_registrazione = :ll_anno_registrazione and 
		 tes_fat_ven.num_registrazione = :ll_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_fat_ven)
	setnull(ldt_data_registrazione)
	setnull(ls_cod_cliente)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ord_cliente)
	setnull(ldt_data_ord_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_fattura)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	ld_cambio_ven = 1
	setnull(ll_num_colli)
	setnull(ls_cod_vettore)
	setnull(ls_causale_trasporto)
	setnull(ls_cod_documento)
	setnull(ls_numeratore_doc)
	setnull(ll_anno_documento)
	setnull(ll_num_documento)
	setnull(ldt_data_fattura)
end if
if not isnull(ls_num_ord_cliente) and (len(ls_num_ord_cliente) > 0) and not isnull(ldt_data_ord_cliente) and (ldt_data_ord_cliente > datetime("01/01/1900")) then ls_num_ord_cliente = ls_num_ord_cliente + "  Del: "

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return
end if	

if isnull(ls_cod_documento) then
	setnull(ls_numeratore_doc)
	setnull(ll_anno_documento)
	setnull(ll_num_documento)
	setnull(ldt_data_fattura)
end if

select anag_clienti.rag_soc_1,   
       anag_clienti.rag_soc_2,   
       anag_clienti.indirizzo,   
       anag_clienti.localita,   
       anag_clienti.frazione,   
       anag_clienti.cap,   
       anag_clienti.provincia,   
       anag_clienti.partita_iva,   
       anag_clienti.cod_fiscale,   
       anag_clienti.cod_lingua,   
       anag_clienti.flag_tipo_cliente  
into   :ls_rag_soc_1_cli,   
       :ls_rag_soc_2_cli,   
       :ls_indirizzo_cli,   
       :ls_localita_cli,   
       :ls_frazione_cli,   
       :ls_cap_cli,   
       :ls_provincia_cli,   
       :ls_partita_iva,   
       :ls_cod_fiscale,   
       :ls_cod_lingua,   
       :ls_flag_tipo_cliente  
from   anag_clienti  
where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
       anag_clienti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_cli)
	setnull(ls_rag_soc_2_cli)
	setnull(ls_indirizzo_cli)
	setnull(ls_localita_cli)
	setnull(ls_frazione_cli)
	setnull(ls_cap_cli)
	setnull(ls_provincia_cli)
	setnull(ls_partita_iva)
	setnull(ls_cod_fiscale)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_cliente)
end if


select rag_soc_1,
       indirizzo,   
       localita,   
       cap,   
       provincia  
into   :ls_vettore_rag_soc_1,   
       :ls_vettore_indirizzo,   
       :ls_vettore_localita,   
       :ls_vettore_cap,   
       :ls_vettore_provincia
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
	    cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
   setnull(ls_vettore_rag_soc_1)   
   setnull(ls_vettore_indirizzo)   
   setnull(ls_vettore_localita)   
   setnull(ls_vettore_cap)   
   setnull(ls_vettore_provincia)
end if



if ls_flag_tipo_cliente = 'E' then
	ls_partita_iva = ls_cod_fiscale
end if

select tab_tipi_fat_ven.des_tipo_fat_ven,
       tab_tipi_fat_ven.flag_tipo_fat_ven 
into   :ls_des_tipo_fat_ven,
       :ls_flag_tipo_fat_ven
from   tab_tipi_fat_ven  
where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_fat_ven)
end if

select tab_pagamenti.des_pagamento,   
       tab_pagamenti.sconto  
into   :ls_des_pagamento,   
       :ld_sconto_pagamento  
from   tab_pagamenti  
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select tab_pagamenti_lingue.des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  tab_pagamenti_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_pagamenti_lingue.cod_pagamento = :ls_cod_pagamento and
       tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

select tab_mezzi.des_mezzo
into   :ls_des_mezzo
from   tab_mezzi
where  tab_mezzi.cod_azienda = :s_cs_xx.cod_azienda and
       tab_mezzi.cod_mezzo = :ls_cod_mezzo;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo)
end if

select tab_mezzi_lingue.des_mezzo
into   :ls_des_mezzo_lingua  
from   tab_mezzi_lingue
where  tab_mezzi_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_mezzi_lingue.cod_mezzo = :ls_cod_mezzo and
       tab_mezzi_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo_lingua)
end if

select anag_banche_clien_for.des_banca  
into   :ls_des_banca  
from   anag_banche_clien_for  
where  anag_banche_clien_for.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_banche_clien_for.cod_banca_clien_for = :ls_cod_banca_clien_for;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

wf_leggi_iva(ll_anno_registrazione, &
             ll_num_registrazione, &
				 ld_aliquote_iva[], &
				 ld_imponibili_valuta[], &
				 ld_iva_valuta[],&
				 ls_des_esenzione_iva[])

wf_leggi_scadenze(ll_anno_registrazione, &
                  ll_num_registrazione, &
					   ldt_scadenze[], &
					   ld_rate_scadenze[])

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

declare cu_dettagli cursor for 
	select   det_fat_ven.cod_tipo_det_ven, 
				det_fat_ven.cod_misura, 
				det_fat_ven.quan_fatturata, 
				det_fat_ven.prezzo_vendita, 
				det_fat_ven.sconto_1, 
				det_fat_ven.sconto_2, 
				det_fat_ven.sconto_3, 
				det_fat_ven.sconto_4, 
				det_fat_ven.sconto_5, 
				det_fat_ven.sconto_6, 
				det_fat_ven.sconto_7, 
				det_fat_ven.sconto_8, 
				det_fat_ven.sconto_9, 
				det_fat_ven.sconto_10, 
				det_fat_ven.nota_dettaglio, 
				det_fat_ven.cod_prodotto, 
				det_fat_ven.des_prodotto,
				det_fat_ven.fat_conversione_ven,
				det_fat_ven.cod_iva
	from     det_fat_ven 
	where    det_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				det_fat_ven.anno_registrazione = :ll_anno_registrazione and 
				det_fat_ven.num_registrazione = :ll_num_registrazione
	order by det_fat_ven.cod_azienda, 
				det_fat_ven.anno_registrazione, 
				det_fat_ven.num_registrazione,
				det_fat_ven.prog_riga_fat_ven;

open cu_dettagli;

if not isnull(ls_nota_testata) and len(trim(ls_nota_testata)) > 0 then lb_prima_riga = true
lb_flag_prodotto_cliente = false
lb_flag_nota_dettaglio = false
lb_flag_nota_piede = false
do while 0 = 0
	if not(lb_prima_riga) and not(lb_flag_prodotto_cliente) and not(lb_flag_nota_dettaglio) then
		fetch cu_dettagli into :ls_cod_tipo_det_ven, 
									  :ls_cod_misura, 
									  :ld_quan_fatturata, 
									  :ld_prezzo_vendita,   
									  :ld_sconto_1, 
									  :ld_sconto_2, 
									  :ld_sconto_3, 
									  :ld_sconto_4, 
									  :ld_sconto_5, 
									  :ld_sconto_6, 
									  :ld_sconto_7, 
									  :ld_sconto_8, 
									  :ld_sconto_9, 
									  :ld_sconto_10, 
									  :ls_nota_dettaglio, 
									  :ls_cod_prodotto, 
									  :ls_des_prodotto,
									  :ld_fat_conversione_ven,
									  :ls_cod_iva;
	
		if sqlca.sqlcode <> 0 then
			if not isnull(ls_nota_piede) and len(trim(ls_nota_piede)) > 0 then
				lb_flag_nota_piede = true
			else
				exit
			end if
		end if
	end if

	ld_sconto_tot = 0
   if ld_sconto_1 <> 0 and not isnull(ld_sconto_1) then ld_sconto_tot = 1 *             ( 1 - (ld_sconto_1 / 100))
   if ld_sconto_2 <> 0 and not isnull(ld_sconto_2) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_2 / 100))
   if ld_sconto_3 <> 0 and not isnull(ld_sconto_3) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_3 / 100))
   if ld_sconto_4 <> 0 and not isnull(ld_sconto_4) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_4 / 100))
   if ld_sconto_5 <> 0 and not isnull(ld_sconto_5) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_5 / 100))
   if ld_sconto_6 <> 0 and not isnull(ld_sconto_6) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_6 / 100))
   if ld_sconto_7 <> 0 and not isnull(ld_sconto_7) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_7 / 100))
   if ld_sconto_8 <> 0 and not isnull(ld_sconto_8) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_8 / 100))
   if ld_sconto_9 <> 0 and not isnull(ld_sconto_9) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_9 / 100))
   if ld_sconto_10<> 0 and not isnull(ld_sconto_10) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_10 / 100))

	ld_imponibile_riga = (ld_quan_fatturata * ld_fat_conversione_ven) * (ld_prezzo_vendita / ld_cambio_ven)
	if ld_sconto_tot <> 0 then
		ld_sconto_tot = (1 - ld_sconto_tot) * 100
		ld_imponibile_riga = ld_imponibile_riga - ( (ld_imponibile_riga/100) * ld_sconto_tot)
	end if
	
	select tab_tipi_det_ven.flag_stampa_fattura,
	       tab_tipi_det_ven.des_tipo_det_ven,
	       tab_tipi_det_ven.flag_tipo_det_ven
	into   :ls_flag_stampa_fattura,
	       :ls_des_tipo_det_ven,
	       :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	if sqlca.sqlcode <> 0 then
		setnull(ls_flag_stampa_fattura)
	end if

	if lb_prima_riga then
		dw_report_fat_ven.insertrow(0)
		dw_report_fat_ven.setrow(dw_report_fat_ven.rowcount())
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_testata)
	elseif lb_flag_prodotto_cliente then
		dw_report_fat_ven.insertrow(0)
		dw_report_fat_ven.setrow(dw_report_fat_ven.rowcount())
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_prodotto", "Vs Codice")
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_cod_prod_cliente)
		lb_flag_prodotto_cliente = false
	elseif (lb_flag_nota_dettaglio) and not ((ls_flag_tipo_det_ven = "C") or (ls_flag_tipo_det_ven = "M")) then
		dw_report_fat_ven.insertrow(0)
		dw_report_fat_ven.setrow(dw_report_fat_ven.rowcount())
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_dettaglio)
		lb_flag_nota_dettaglio = false
	elseif lb_flag_nota_piede then
		dw_report_fat_ven.insertrow(0)
		dw_report_fat_ven.setrow(dw_report_fat_ven.rowcount())
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_piede)
//		lb_flag_nota_piede = false
	else	
		if ls_flag_stampa_fattura = 'S' then
			dw_report_fat_ven.insertrow(0)
			dw_report_fat_ven.setrow(dw_report_fat_ven.rowcount())
			select anag_prodotti.des_prodotto  
			into   :ls_des_prodotto_anag  
			from   anag_prodotti  
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
	
			select anag_prodotti_lingue.des_prodotto  
			into   :ls_des_prodotto_lingua  
			from   anag_prodotti_lingue  
			where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
					 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_lingua)
			end if
	
			select tab_prod_clienti.cod_prod_cliente  
			into   :ls_cod_prod_cliente  
			from   tab_prod_clienti  
			where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
					 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if
			
			select tab_ive.aliquota
			into   :ld_perc_iva
			from   tab_ive
			where  tab_ive.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_ive.cod_iva = :ls_cod_iva;
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_iva)
			else
				ls_des_iva = string(ld_perc_iva,"###")
			end if
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_prodotto", ls_cod_prodotto)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_misura", ls_cod_misura)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_quan_ordine", ld_quan_fatturata * ld_fat_conversione_ven)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_prezzo_vendita", ld_prezzo_vendita / ld_cambio_ven)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_sconto_tot", ld_sconto_tot)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_val_riga", ld_imponibile_riga)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_iva", ls_des_iva)

			if ((ls_flag_tipo_det_ven = "M") or (ls_flag_tipo_det_ven = "C")) and (not isnull(ls_nota_dettaglio)) then
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_dettaglio)
			elseif not isnull(ls_cod_lingua) and not isnull(ls_des_prodotto_lingua) then
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
			elseif not isnull(ls_des_prodotto) then
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_des_prodotto)
			elseif not isnull(ls_des_prodotto_anag) then
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
			else
				ls_des_prodotto_anag = ls_des_tipo_det_ven
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
			end if

			if not isnull(ls_cod_prod_cliente) and len(trim(ls_cod_prod_cliente)) > 0 then
				lb_flag_prodotto_cliente = true
			end if
			if not isnull(ls_nota_dettaglio) and len(trim(ls_nota_dettaglio)) > 0 and &
			   not((ls_flag_tipo_det_ven = "C") or (ls_flag_tipo_det_ven = "M")) then
				lb_flag_nota_dettaglio = true
			end if
		end if
	end if	
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "parametri_azienda_stringa", ls_stringa)
	if not isnull(ls_cod_documento) then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_documento", ls_cod_documento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_numeratore_documento", ls_numeratore_doc)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_anno_documento", ll_anno_documento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_documento", ll_num_documento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_data_fattura", ldt_data_fattura)
	else
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_anno_registrazione", ll_anno_registrazione)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_registrazione", ll_num_registrazione)
	end if
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_tipo_fat_ven", ls_cod_tipo_fat_ven)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_data_registrazione", ldt_data_registrazione)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_cliente", ls_cod_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_valuta", ls_cod_valuta)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_pagamento", ls_cod_pagamento)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_sconto", ld_sconto)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_ord_cliente", ls_num_ord_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_data_ord_cliente", ldt_data_ord_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_nota_testata", ls_nota_testata)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_nota_piede", ls_nota_piede)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_porto", ls_cod_porto)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_mezzo", ls_cod_mezzo)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_agente", ls_cod_agente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_tot_fattura_valuta", ld_tot_fattura)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_imponibile_iva_valuta", 0)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_tot_merce", ld_tot_merci)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_tot_imponibile", ld_imponibile_iva - ld_tot_spese_trasporto - ld_tot_spese_varie)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_tot_spese_trasporto", ld_tot_spese_trasporto + ld_tot_spese_varie)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_iva_valuta", 0)
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_localita", ls_localita_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_frazione", ls_frazione_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_cap", ls_cap_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_provincia", ls_provincia_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_tipi_fat_ven_des_tipo_fat_ven", ls_des_tipo_fat_ven)
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)

	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)

	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_porti_des_porto", ls_des_porto)
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_mezzi_des_mezzo", ls_des_mezzo)
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_mezzi_lingue_des_mezzo", ls_des_mezzo_lingua)

	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_rag_soc_1", ls_vettore_rag_soc_1)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_indirizzo", ls_vettore_indirizzo)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_cap", ls_vettore_cap)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_localita", ls_vettore_localita)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_provincia", ls_vettore_provincia)

	ll_num_aliquote = upperbound(ld_aliquote_iva) - 1
	if ll_num_aliquote > 0 then
		if ld_aliquote_iva[1] > 0 and not isnull(ld_aliquote_iva[1]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_1", ld_aliquote_iva[1])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_1", ld_imponibili_valuta[1])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_1", ld_iva_valuta[1])
		else
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_1", ld_imponibili_valuta[1])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_1", ls_des_esenzione_iva[1])
		end if
	end if
	if ll_num_aliquote > 1 then
		if ld_aliquote_iva[2] > 0 and not isnull(ld_aliquote_iva[2]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_2", ld_aliquote_iva[2])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_2", ld_imponibili_valuta[2])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_2", ld_iva_valuta[2])
		else
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_2", ld_imponibili_valuta[2])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_2", ls_des_esenzione_iva[2])
		end if
	end if
	if ll_num_aliquote > 2 then
		if ld_aliquote_iva[3] > 0 and not isnull(ld_aliquote_iva[3]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_3", ld_aliquote_iva[3])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_3", ld_imponibili_valuta[3])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_3", ld_iva_valuta[3])
		else
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_3", ld_imponibili_valuta[3])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_3", ls_des_esenzione_iva[3])
		end if
	end if
	if ll_num_aliquote > 3 then
		if ld_aliquote_iva[4] > 0 and not isnull(ld_aliquote_iva[4]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_4", ld_aliquote_iva[4])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_4", ld_imponibili_valuta[4])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_4", ld_iva_valuta[4])
		else
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_4", ld_imponibili_valuta[4])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_4", ls_des_esenzione_iva[4])
		end if
	end if
	if ll_num_aliquote > 4 then
		if ld_aliquote_iva[5] > 0 and not isnull(ld_aliquote_iva[5]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_5", ld_aliquote_iva[5])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_5", ld_imponibili_valuta[5])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_5", ld_iva_valuta[5])
		else
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_5", ld_imponibili_valuta[5])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_5", ls_des_esenzione_iva[5])
		end if
	end if
	if ll_num_aliquote > 5 then
		if ld_aliquote_iva[6] > 0 and not isnull(ld_aliquote_iva[6]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_6", ld_aliquote_iva[6])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_6", ld_imponibili_valuta[6])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_6", ld_iva_valuta[6])
		else
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_6", ld_imponibili_valuta[6])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_6", ls_des_esenzione_iva[6])
		end if
	end if

	if upperbound(ldt_scadenze) > 0 then
		if date(ldt_scadenze[1]) > date("01/01/1900") and not isnull(ldt_scadenze[1]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_1", ldt_scadenze[1])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_1", ld_rate_scadenze[1])
		end if
	end if
	if upperbound(ldt_scadenze) > 1 then
		if date(ldt_scadenze[2]) > date("01/01/1900") and not isnull(ldt_scadenze[2]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_2", ldt_scadenze[2])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_2", ld_rate_scadenze[2])
		end if
	end if
	if upperbound(ldt_scadenze) > 2 then
		if date(ldt_scadenze[3]) > date("01/01/1900") and not isnull(ldt_scadenze[3]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_3", ldt_scadenze[3])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_3", ld_rate_scadenze[3])
		end if
	end if
	if upperbound(ldt_scadenze) > 3 then
		if date(ldt_scadenze[4]) > date("01/01/1900") and not isnull(ldt_scadenze[4]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_4", ldt_scadenze[4])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_4", ld_rate_scadenze[4])
		end if
	end if
	if upperbound(ldt_scadenze) > 4 then
		if date(ldt_scadenze[5]) > date("01/01/1900") and not isnull(ldt_scadenze[5]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_5", ldt_scadenze[5])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_5", ld_rate_scadenze[5])
		end if
	end if
	if upperbound(ldt_scadenze) > 5 then
		if date(ldt_scadenze[6]) > date("01/01/1900") and not isnull(ldt_scadenze[6]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_6", ldt_scadenze[6])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_6", ld_rate_scadenze[6])
		end if
	end if

	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_imponibile_iva_valuta", ld_imponibile_iva_valuta)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_iva_valuta", ld_importo_iva_valuta)

	lb_prima_riga = false
	if lb_flag_nota_piede then exit
	
loop
close cu_dettagli;
dw_report_fat_ven.reset_dw_modified(c_resetchildren)

// ----------- Michela: inserisco la possibilità di stampare il pdf
if dw_report_fat_ven.RowCount() < 1 then
	g_mb.messagebox("APICE","La fattura non possiede nessun dettaglio: impossibile creare il pdf", Stopsign!)
	return
end if

// creo il file PDF della fattura

if ll_anno_documento > 0 and ll_num_documento > 0 and not isnull(ll_anno_documento) and not isnull(ll_num_documento) then

	uo_archivia_pdf luo_pdf
	luo_pdf = create uo_archivia_pdf
	luo_pdf.uof_crea_pdf(dw_report_fat_ven, ref l_blob)
	
	SELECT prog_mimetype  
	INTO   :ll_prog_mimytype  
	FROM   tab_mimetype  
	WHERE  cod_azienda = :s_cs_xx.cod_azienda and
			 estensione = 'PDF'   ;
	if sqlca.sqlcode = 100 then
		g_mb.messagebox("APICE","Impossibile archiviare il documento: impostare il mimetype")
		return
	end if
	
	delete from tes_fat_ven_note
	where       cod_azienda = :s_cs_xx.cod_azienda and
	            anno_registrazione = :ll_anno_registrazione and
				   num_registrazione = :ll_num_registrazione and
				   cod_nota = 'FAT';
	
	
	ls_stringa = "Fattura nr " + string(ll_anno_documento) + "/" + string(ll_num_documento) + " del " + string(ldt_data_fattura,"dd/mm/yyyy")
	INSERT INTO tes_fat_ven_note  
			(     cod_azienda,   
			      anno_registrazione,   
			      num_registrazione,   
			      cod_nota,   
			      des_nota,   
			      prog_mimetype)  
	VALUES (    :s_cs_xx.cod_azienda,   
			      :ll_anno_registrazione,   
			      :ll_num_registrazione,   
			      'FAT',   
			      :ls_stringa,   
			      :ll_prog_mimytype )  ;
	
	updateblob tes_fat_ven_note
	set        note_esterne = :l_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_registrazione = :ll_anno_registrazione and
				  num_registrazione = :ll_num_registrazione and
				  cod_nota = 'FAT';
	
	destroy luo_pdf
	commit;
	
end if
end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_path_logo_2, ls_modify


set_w_options(c_noresizewin)

dw_report_fat_ven.set_dw_options(sqlca, &
                                 i_openparm, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
												
select parametri_azienda.stringa
into   :ls_path_logo_1
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO1';

select parametri_azienda.stringa
into   :ls_path_logo_2
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO2';

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
dw_report_fat_ven.modify(ls_modify)

ls_modify = "piede.filename='" + s_cs_xx.volume + ls_path_logo_2 + "'~t"
dw_report_fat_ven.modify(ls_modify)

save_on_close(c_socnosave)

// *** metto il focus
dw_report_fat_ven.change_dw_focus(dw_report_fat_ven)
// ***

end event

on w_report_fat_ven_aghi_euro.create
int iCurrent
call super::create
this.dw_report_fat_ven=create dw_report_fat_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_fat_ven
end on

on w_report_fat_ven_aghi_euro.destroy
call super::destroy
destroy(this.dw_report_fat_ven)
end on

type dw_report_fat_ven from uo_cs_xx_dw within w_report_fat_ven_aghi_euro
integer x = 23
integer y = 20
integer width = 3657
integer height = 4520
integer taborder = 20
string dataobject = "d_report_fat_ven_aghi_euro"
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

event pcd_first;call super::pcd_first;wf_report()
end event

event pcd_last;call super::pcd_last;wf_report()
end event

event pcd_next;call super::pcd_next;wf_report()
end event

event pcd_previous;call super::pcd_previous;wf_report()
end event

