﻿$PBExportHeader$w_report_fat_ven_euro.srw
$PBExportComments$Finestra Report Fattura di Vendita
forward
global type w_report_fat_ven_euro from w_cs_xx_principale
end type
type cb_archivia from commandbutton within w_report_fat_ven_euro
end type
type cb_singola from picturebutton within w_report_fat_ven_euro
end type
type cb_3 from picturebutton within w_report_fat_ven_euro
end type
type cb_2 from picturebutton within w_report_fat_ven_euro
end type
type cb_4 from picturebutton within w_report_fat_ven_euro
end type
type cb_1 from picturebutton within w_report_fat_ven_euro
end type
type st_copie from statictext within w_report_fat_ven_euro
end type
type st_1 from statictext within w_report_fat_ven_euro
end type
type dw_report_fat_ven from uo_cs_xx_dw within w_report_fat_ven_euro
end type
end forward

global type w_report_fat_ven_euro from w_cs_xx_principale
integer width = 3872
integer height = 4772
string title = "Stampa Fatture"
boolean minbox = false
boolean maxbox = false
boolean hscrollbar = true
boolean vscrollbar = true
event ue_close ( )
event printed pbm_dwnprintend
cb_archivia cb_archivia
cb_singola cb_singola
cb_3 cb_3
cb_2 cb_2
cb_4 cb_4
cb_1 cb_1
st_copie st_copie
st_1 st_1
dw_report_fat_ven dw_report_fat_ven
end type
global w_report_fat_ven_euro w_report_fat_ven_euro

type variables
boolean ib_modifica=false, ib_nuovo=false, ib_nr_nr, ib_stampando = false, ib_email=false
boolean ib_estero
long il_anno_registrazione, il_num_registrazione, il_anno[], il_num[], il_corrente = 0
long il_num_copie

// ------------- dichiarate per stampa da pagina a pagina -----------
boolean ib_stampa = true
string is_flag_email[], is_email_amministrazione[], is_email_utente[]
long    il_totale_pagine, il_pagina_corrente

// ------------- discalimer per invio fatture tramite email ----------
constant string is_disclaimer=""


//serve per l'invio mail mediante ole redemption
oleobject		iole_outlook, iole_redemption, iole_item, iole_redemption_attach, iole_attach
end variables

forward prototypes
public subroutine wf_leggi_scadenze (long fl_anno_registrazione, long fl_num_registrazione, ref datetime fdt_data_scadenze[], ref decimal fd_importo_rata[])
public function integer wf_impostazioni ()
public subroutine wf_report ()
public function integer wf_memorizza_blob (long fl_anno_registrazione, long fl_num_registrazione, string fs_path)
public subroutine wf_stampa (long copie)
public subroutine _wf_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[])
public function integer wf_imposta_filigrana ()
public function integer wf_sendmail (string as_oggetto, string as_testo, string as_destinatari[], string as_allegati[], ref string as_errore)
public function integer wf_testo_mail (string fs_rag_soc_1, long fl_anno, long fl_numero, datetime fdt_data, ref string fs_testo)
public function integer wf_email ()
public function integer wf_replace_marker (ref string as_source, string as_find, string as_replace, boolean ab_case_sensitive)
end prototypes

event ue_close();close(this)
end event

event printed;ib_stampando = true
end event

public subroutine wf_leggi_scadenze (long fl_anno_registrazione, long fl_num_registrazione, ref datetime fdt_data_scadenze[], ref decimal fd_importo_rata[]);long ll_i

declare cu_scadenze cursor for 
	select   scad_fat_ven.data_scadenza, 
				scad_fat_ven.imp_rata_valuta
	from     scad_fat_ven 
	where    scad_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				scad_fat_ven.anno_registrazione = :fl_anno_registrazione and 
				scad_fat_ven.num_registrazione = :fl_num_registrazione;

open cu_scadenze;

ll_i = 0
do while 0 = 0
	ll_i = ll_i + 1
   fetch cu_scadenze into :fdt_data_scadenze[ll_i], 
							     :fd_importo_rata[ll_i];

   if sqlca.sqlcode <> 0 then exit
loop

close cu_scadenze;
return

end subroutine

public function integer wf_impostazioni ();string ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_cod_cliente, ls_flag_tipo_cliente, ls_path_logo_1, &
       ls_path_logo_2, ls_modify, ls_dataobject

long   ll_copie, ll_copie_cee, ll_copie_extra_cee



save_on_close(c_socnosave)

ib_estero = false
il_num_copie = 3

select cod_cliente,
       cod_tipo_fat_ven
into   :ls_cod_cliente,
		 :ls_cod_tipo_fat_ven
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione;
		 
if sqlca.sqlcode = 0 and not isnull(ls_cod_cliente) then
	
	select flag_tipo_cliente
	into   :ls_flag_tipo_cliente
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode = 0 then
		
		// carico il dataobject corretto
		select dataobject
		into   :ls_dataobject
		from   tab_tipi_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
		
		if isnull(ls_dataobject) or len(ls_dataobject) < 1 then
			g_mb.messagebox("APICE","Manca il dataobject nella tabella TIPI FATTURE~r~nViene applicato il report di default",stopsign!)
		else
			dw_report_fat_ven.dataobject = ls_dataobject
		end if
		
		// determino quante copie stampare della fattura
		select num_copie,
				 num_copie_cee,
				 num_copie_extra_cee
		into   :ll_copie,
				 :ll_copie_cee,
				 :ll_copie_extra_cee
		from   tab_tipi_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
		if sqlca.sqlcode = 0 then
			choose case ls_flag_tipo_cliente
				case "C"
					ib_estero = true
					il_num_copie = ll_copie_cee
				case "E"
					ib_estero = true
					il_num_copie = ll_copie_extra_cee
				case else
					ib_estero = false
					il_num_copie = ll_copie
			end choose
		end if
	end if
	
end if

select parametri_azienda.stringa
into   :ls_path_logo_1
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO1';

select parametri_azienda.stringa
into   :ls_path_logo_2
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO2';

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
dw_report_fat_ven.modify(ls_modify)

ls_modify = "piede.filename='" + s_cs_xx.volume + ls_path_logo_2 + "'~t"
dw_report_fat_ven.modify(ls_modify)

wf_imposta_filigrana()

return 0
end function

public subroutine wf_report ();boolean  lb_prima_riga, lb_flag_prodotto_cliente, lb_flag_nota_dettaglio, lb_flag_nota_piede

string   ls_stringa, ls_stringa_euro, ls_cod_tipo_fat_ven, ls_cod_banca_clien_for, ls_cod_cliente, &
		   ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, &
		   ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		   ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		   ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, &
		   ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
		   ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, &
		   ls_des_tipo_fat_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, &
  		   ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua, ls_des_tipo_det_ven,&
		   ls_des_prodotto_anag, ls_flag_stampa_fattura, ls_cod_tipo_det_ven, ls_flag_tipo_fat_ven, &
		   ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_cod_iva, ls_des_iva, ls_causale_trasporto, &
		   ls_cod_vettore, ls_vettore_rag_soc_1, ls_vettore_rag_soc_2, ls_vettore_indirizzo, &
		   ls_vettore_cap, ls_vettore_localita, ls_vettore_provincia, ls_des_esenzione_iva[], &
		   ls_cod_documento, ls_numeratore_doc, ls_cod_banca, ls_flag_tipo_pagamento, ls_cod_abi, ls_cod_cab, &
		   ls_cod_des_cliente, ls_telefono_cliente, ls_fax_cliente, ls_telefono_des, ls_fax_des, &
		   ls_des_valuta, ls_des_valuta_lingua, ls_dicitura_std, ls_cod_agente_1, ls_cod_agente_2,&
		   ls_anag_agenti_rag_soc_1, ls_anag_agenti_rag_soc_2, ls_des_mezzo, ls_des_mezzo_lingua, &
		   ls_cod_mezzo, ls_formato, ls_cod_contatto, ls_des_estesa[], ls_des_aliquota, ls_codici_iva[], ls_stato_cli, ls_num_civico
		 
long     ll_errore, ll_num_colli, ll_num_aliquote, &
         ll_anno_documento, ll_num_documento, ll_prog_mimytype
	  
dec{4}   ld_tot_fattura, ld_sconto, ld_quan_fatturata, ld_prezzo_vendita, ld_sconto_1, &
	      ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7,&
		   ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_tot, ld_imponibile_riga, &
		   ld_val_riga, ld_sconto_pagamento, ld_cambio_ven, &
		   ld_imponibili_valuta[], ld_iva_valuta[],ld_aliquote_iva[] ,ld_rate_scadenze[],&
		   ld_imponibile_iva_valuta, ld_importo_iva_valuta, ld_perc_iva, ld_tot_fattura_euro, &
		   ld_tot_fattura_lire, ld_tot_merci, tot_sconti_commerciali, tot_sconto_cassa
			
dec{5}   ld_fat_conversione_ven
		 
datetime ldt_data_ord_cliente, ldt_data_registrazione, ldt_scadenze[], ldt_data_fattura

blob l_blob

COMMIT;

dw_report_fat_ven.reset()


select parametri_azienda.stringa  
into  :ls_stringa  
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select parametri_azienda.stringa  
into  :ls_stringa_euro
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if
select tes_fat_ven.cod_tipo_fat_ven,   
		 tes_fat_ven.data_registrazione,   
		 tes_fat_ven.cod_cliente,   
		 tes_fat_ven.cod_contatto,   
		 tes_fat_ven.cod_valuta,   
		 tes_fat_ven.cod_pagamento,   
		 tes_fat_ven.sconto,   
		 tes_fat_ven.cod_banca_clien_for,   
		 tes_fat_ven.cod_banca,   
		 tes_fat_ven.num_ord_cliente,   
		 tes_fat_ven.data_ord_cliente,   
		 tes_fat_ven.cod_porto,   
		 tes_fat_ven.cod_resa,   
		 tes_fat_ven.nota_testata,   
		 tes_fat_ven.nota_piede,   
		 tes_fat_ven.imponibile_iva_valuta,
		 tes_fat_ven.importo_iva_valuta,
		 tes_fat_ven.tot_fattura_valuta,
		 tes_fat_ven.tot_fattura,
		 tes_fat_ven.rag_soc_1,   
		 tes_fat_ven.rag_soc_2,   
		 tes_fat_ven.indirizzo,   
		 tes_fat_ven.localita,   
		 tes_fat_ven.frazione,   
		 tes_fat_ven.cap,   
		 tes_fat_ven.provincia,
		 tes_fat_ven.cambio_ven,
		 tes_fat_ven.num_colli,
		 tes_fat_ven.cod_vettore,
		 tes_fat_ven.causale_trasporto,
		 tes_fat_ven.cod_documento,
		 tes_fat_ven.numeratore_documento,
		 tes_fat_ven.anno_documento,
		 tes_fat_ven.num_documento,
		 tes_fat_ven.data_fattura,
		 tes_fat_ven.cod_agente_1,
		 tes_fat_ven.cod_agente_2,
		 tes_fat_ven.cod_des_cliente,
		 tes_fat_ven.tot_merci,
		 tes_fat_ven.tot_sconto_cassa,
		 tes_fat_ven.tot_sconti_commerciali,
		 tes_fat_ven.cod_mezzo
into   :ls_cod_tipo_fat_ven,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente, 
		 :ls_cod_contatto,
		 :ls_cod_valuta,   
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_cod_banca,
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_imponibile_iva_valuta,
		 :ld_importo_iva_valuta,
		 :ld_tot_fattura_lire,
		 :ld_tot_fattura,
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ld_cambio_ven,
		 :ll_num_colli,
		 :ls_cod_vettore,
		 :ls_causale_trasporto,
		 :ls_cod_documento,
		 :ls_numeratore_doc,
		 :ll_anno_documento,
		 :ll_num_documento,
		 :ldt_data_fattura,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_des_cliente,
		 :ld_tot_merci,
		 :tot_sconto_cassa,
		 :tot_sconti_commerciali,
		 :ls_cod_mezzo
from   tes_fat_ven  
where  tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_fat_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_fat_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_fat_ven)
	setnull(ldt_data_registrazione)
	setnull(ls_cod_cliente)
	setnull(ls_cod_contatto)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ord_cliente)
	setnull(ldt_data_ord_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_fattura)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	ld_cambio_ven = 1
	setnull(ll_num_colli)
	setnull(ls_cod_vettore)
	setnull(ls_causale_trasporto)
	setnull(ls_cod_des_cliente)
	setnull(ld_tot_merci)
	setnull(tot_sconto_cassa)
	setnull(tot_sconti_commerciali)
	setnull(ls_cod_mezzo)
end if

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return
end if	

if not isnull(ls_cod_cliente) then

	select rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 partita_iva,   
			 cod_fiscale,   
			 cod_lingua,   
			 telefono,   
			 fax,   
			 flag_tipo_cliente,
			 stato,
			 fatel_num_civico
	into     :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,  
			 :ls_telefono_cliente,
			 :ls_fax_cliente,
			 :ls_flag_tipo_cliente,
			 :ls_stato_cli,
			 :ls_num_civico
	from   anag_clienti  
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :ls_cod_cliente;
			 
elseif not isnull(ls_cod_contatto) then
	setnull(ls_stato_cli)
	
	select  rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 partita_iva,   
			 cod_fiscale,   
			 cod_lingua,   
			 telefono,   
			 fax,   
			 flag_tipo_cliente  
	into     :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,  
			 :ls_telefono_cliente,
			 :ls_fax_cliente,
			 :ls_flag_tipo_cliente  
	from   anag_contatti 
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_contatto = :ls_cod_contatto;
	
end if
			 

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_cli)
	setnull(ls_rag_soc_2_cli)
	setnull(ls_indirizzo_cli)
	setnull(ls_localita_cli)
	setnull(ls_frazione_cli)
	setnull(ls_cap_cli)
	setnull(ls_provincia_cli)
	setnull(ls_partita_iva)
	setnull(ls_cod_fiscale)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_cliente)
	setnull(ls_telefono_cliente)
	setnull(ls_fax_cliente)
	setnull(ls_stato_cli)
end if

if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if


select rag_soc_1,
       indirizzo,   
       localita,   
       cap,   
       provincia  
into   :ls_vettore_rag_soc_1,   
       :ls_vettore_indirizzo,   
       :ls_vettore_localita,   
       :ls_vettore_cap,   
       :ls_vettore_provincia
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
	    cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
   setnull(ls_vettore_rag_soc_1)   
   setnull(ls_vettore_indirizzo)   
   setnull(ls_vettore_localita)   
   setnull(ls_vettore_cap)   
   setnull(ls_vettore_provincia)
end if



if ls_flag_tipo_cliente = 'E' or ls_flag_tipo_cliente = 'C' or ls_flag_tipo_cliente = 'P' then
	ls_partita_iva = ls_cod_fiscale
	if ls_flag_tipo_cliente = 'P' then 
		dw_report_fat_ven.object.t_partita_iva.text = "C.F."
	else
		dw_report_fat_ven.object.t_partita_iva.text = "P.IVA / VAT"
	end if
end if

if not isnull(ls_num_civico) and len(ls_num_civico) > 0 then
	ls_indirizzo_cli = g_str.format("$1 $2",ls_indirizzo_cli, ls_num_civico)
end if

select des_tipo_fat_ven,
       flag_tipo_fat_ven
into   :ls_des_tipo_fat_ven,
       :ls_flag_tipo_fat_ven
from   tab_tipi_fat_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_fat_ven)
end if

select des_pagamento,   
       sconto,
       flag_tipo_pagamento
into   :ls_des_pagamento,   
       :ld_sconto_pagamento,
       :ls_flag_tipo_pagamento
from   tab_pagamenti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
   ls_flag_tipo_pagamento = "D"
end if

select des_pagamento
into   :ls_des_pagamento_lingua
from   tab_pagamenti_lingue  
where  cod_azienda   = :s_cs_xx.cod_azienda and 
       cod_pagamento = :ls_cod_pagamento and
       cod_lingua    = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

if ls_flag_tipo_pagamento = "R"	then			// se ricevuta bancaria
	select des_banca,
			 cod_abi,
			 cod_cab
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab
	from   anag_banche_clien_for  
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_banca_clien_for = :ls_cod_banca_clien_for;
else
	select des_banca,
	       cod_abi,
			 cod_cab
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab
	from   anag_banche
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_banca = :ls_cod_banca;
end if			
if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
	setnull(ls_cod_abi)
	setnull(ls_cod_cab)
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto and
       cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_resa = :ls_cod_resa and  
       cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

select des_mezzo
into   :ls_des_mezzo
from   tab_mezzi  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo = :ls_cod_mezzo;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo)
end if

select des_mezzo  
into   :ls_des_mezzo_lingua  
from   tab_mezzi_lingue 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo = :ls_cod_mezzo and
       cod_lingua = :ls_cod_lingua;
		 
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo_lingua)
end if


select des_valuta
into   :ls_des_valuta  
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if

select tab_valute_lingue.des_valuta
into   :ls_des_valuta_lingua  
from   tab_valute_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :ls_cod_valuta and  
       cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta_lingua)
end if

if not isnull(ls_cod_agente_1) then
	select rag_soc_1
	into   :ls_anag_agenti_rag_soc_1
	from   anag_agenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_agente = :ls_cod_agente_1;
			 
	if sqlca.sqlcode <> 0 then setnull(ls_anag_agenti_rag_soc_1)
end if

if not isnull(ls_cod_agente_2) then
	select rag_soc_1
	into   :ls_anag_agenti_rag_soc_2
	from   anag_agenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_agente = :ls_cod_agente_2;
			 
	if sqlca.sqlcode <> 0 then setnull(ls_anag_agenti_rag_soc_2)
end if


if not isnull(ls_cod_des_cliente) then
  select telefono,   
         fax  
  into   :ls_telefono_des,   
         :ls_fax_des  
  from   anag_des_clienti
  where  cod_azienda = :s_cs_xx.cod_azienda and
         cod_cliente = :ls_cod_cliente and
         cod_des_cliente = :ls_cod_des_cliente ;
	if sqlca.sqlcode <> 0 then
		setnull(ls_telefono_des)
		setnull(ls_fax_des)
	end if
end if

//Donato 15/02/2013
//creata la funzione leggi iva centraliozzata in uo_functions, in base alla gestione ed alla lingua del cliente/fornitore
//se la lingua non è gestita passare stringa vuota nel relativo argomento
guo_functions.uof_leggi_iva(il_anno_registrazione, il_num_registrazione, ls_cod_lingua, "iva_fat_ven", 	ls_codici_iva[], &
																																	ld_aliquote_iva[], &
																																	ld_imponibili_valuta[], &
																																	ld_iva_valuta[], &
																																	ls_des_esenzione_iva[], &
																																	ls_des_estesa[])
//--------------------------------------------------------------------------------------------------------------------------------

//wf_leggi_iva(il_anno_registrazione, &
//             il_num_registrazione, &
//				 ld_aliquote_iva[], &
//				 ld_imponibili_valuta[], &
//				 ld_iva_valuta[], &
//				 ls_des_esenzione_iva[])

wf_leggi_scadenze(il_anno_registrazione, &
                  il_num_registrazione, &
					   ldt_scadenze[], &
					   ld_rate_scadenze[])

declare cu_dettagli cursor for 
	select   det_fat_ven.cod_tipo_det_ven, 
				det_fat_ven.cod_misura, 
				det_fat_ven.quan_fatturata, 
				det_fat_ven.prezzo_vendita, 
				det_fat_ven.sconto_1, 
				det_fat_ven.sconto_2, 
				det_fat_ven.sconto_3, 
				det_fat_ven.sconto_4, 
				det_fat_ven.sconto_5, 
				det_fat_ven.sconto_6, 
				det_fat_ven.sconto_7, 
				det_fat_ven.sconto_8, 
				det_fat_ven.sconto_9, 
				det_fat_ven.sconto_10, 
				det_fat_ven.nota_dettaglio, 
				det_fat_ven.cod_prodotto, 
				det_fat_ven.des_prodotto,
				det_fat_ven.fat_conversione_ven,
				det_fat_ven.cod_iva,
				imponibile_iva_valuta
	from     det_fat_ven 
	where    det_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				det_fat_ven.anno_registrazione = :il_anno_registrazione and 
				det_fat_ven.num_registrazione = :il_num_registrazione
	order by det_fat_ven.cod_azienda, 
				det_fat_ven.anno_registrazione, 
				det_fat_ven.num_registrazione,
				det_fat_ven.prog_riga_fat_ven;

open cu_dettagli;

if not isnull(ls_nota_testata) and len(trim(ls_nota_testata)) > 0 then lb_prima_riga = true

lb_flag_prodotto_cliente = false
lb_flag_nota_dettaglio = false
lb_flag_nota_piede = false

do while 0 = 0
	if not(lb_prima_riga) and not(lb_flag_prodotto_cliente) and not(lb_flag_nota_dettaglio) then
		fetch cu_dettagli into :ls_cod_tipo_det_ven, 
									  :ls_cod_misura, 
									  :ld_quan_fatturata, 
									  :ld_prezzo_vendita,   
									  :ld_sconto_1, 
									  :ld_sconto_2, 
									  :ld_sconto_3, 
									  :ld_sconto_4, 
									  :ld_sconto_5, 
									  :ld_sconto_6, 
									  :ld_sconto_7, 
									  :ld_sconto_8, 
									  :ld_sconto_9, 
									  :ld_sconto_10, 
									  :ls_nota_dettaglio, 
									  :ls_cod_prodotto, 
									  :ls_des_prodotto,
									  :ld_fat_conversione_ven,
									  :ls_cod_iva,
									  :ld_imponibile_riga;
	
		if sqlca.sqlcode <> 0 then
			if not isnull(ls_nota_piede) and len(trim(ls_nota_piede)) > 0 then
				lb_flag_nota_piede = true
			else
				exit
			end if
		end if
	end if
	
	ld_sconto_tot = 0
   if ld_sconto_1 <> 0 and not isnull(ld_sconto_1) then ld_sconto_tot = 1 *             ( 1 - (ld_sconto_1 / 100))
   if ld_sconto_2 <> 0 and not isnull(ld_sconto_2) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_2 / 100))
   if ld_sconto_3 <> 0 and not isnull(ld_sconto_3) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_3 / 100))
   if ld_sconto_4 <> 0 and not isnull(ld_sconto_4) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_4 / 100))
   if ld_sconto_5 <> 0 and not isnull(ld_sconto_5) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_5 / 100))
   if ld_sconto_6 <> 0 and not isnull(ld_sconto_6) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_6 / 100))
   if ld_sconto_7 <> 0 and not isnull(ld_sconto_7) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_7 / 100))
   if ld_sconto_8 <> 0 and not isnull(ld_sconto_8) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_8 / 100))
   if ld_sconto_9 <> 0 and not isnull(ld_sconto_9) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_9 / 100))
   if ld_sconto_10<> 0 and not isnull(ld_sconto_10) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_10 / 100))
	
	if ld_sconto_tot <> 0 then ld_sconto_tot = (1 - ld_sconto_tot) * 100

// ---------- 
	dw_report_fat_ven.insertrow(0)
	dw_report_fat_ven.setrow(dw_report_fat_ven.rowcount())
		
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "formato", ls_formato)

	select tab_tipi_det_ven.flag_stampa_fattura,
	       tab_tipi_det_ven.des_tipo_det_ven
	into   :ls_flag_stampa_fattura,
	       :ls_des_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode <> 0 then
		setnull(ls_flag_stampa_fattura)
		setnull(ls_des_tipo_det_ven)
	end if

	if lb_prima_riga then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_testata)
	elseif lb_flag_prodotto_cliente then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_prodotto", "Vs Codice")
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_cod_prod_cliente)
		lb_flag_prodotto_cliente = false
	elseif lb_flag_nota_dettaglio then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_dettaglio)
		lb_flag_nota_dettaglio = false		
	elseif lb_flag_nota_piede then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_piede)
	else		
		
		if ls_flag_stampa_fattura = 'S' then
			select anag_prodotti.des_prodotto  
			into   :ls_des_prodotto_anag  
			from   anag_prodotti  
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
	
			select anag_prodotti_lingue.des_prodotto  
			into   :ls_des_prodotto_lingua  
			from   anag_prodotti_lingue  
			where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
					 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_lingua)
			end if
	
			select tab_prod_clienti.cod_prod_cliente  
			into   :ls_cod_prod_cliente  
			from   tab_prod_clienti  
			where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
					 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if
			
			select tab_ive.aliquota
			into   :ld_perc_iva
			from   tab_ive
			where  tab_ive.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_ive.cod_iva = :ls_cod_iva;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_iva)
			else
				ls_des_iva = string(ld_perc_iva,"###")
			end if
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_misura", ls_cod_misura)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_quan_ordine", ld_quan_fatturata * ld_fat_conversione_ven)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_prezzo_vendita", ld_prezzo_vendita / ld_fat_conversione_ven)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_sconto_tot", ld_sconto_tot)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_val_riga", ld_imponibile_riga)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_iva", ls_des_iva)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_nota_dettaglio", ls_nota_dettaglio)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_prodotto", ls_cod_prodotto)
	
			if not isnull(ls_cod_lingua) and not isnull(ls_des_prodotto_lingua) then
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
			elseif not isnull(ls_des_prodotto) then
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_des_prodotto)
			elseif not isnull(ls_des_prodotto_anag) then
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
			else		
				ls_des_prodotto_anag = ls_des_tipo_det_ven
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
			end if
			
			if not isnull(ls_cod_prod_cliente) and len(trim(ls_cod_prod_cliente)) > 0 then
				lb_flag_prodotto_cliente = true
			end if
			if not isnull(ls_nota_dettaglio) and len(trim(ls_nota_dettaglio)) > 0 then
				lb_flag_nota_dettaglio = true
			end if
		end if
	end if	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "parametri_azienda_stringa", ls_stringa)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "parametri_azienda_stringa_euro", ls_stringa_euro)
	if isnull(ls_cod_documento) then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_documento", ls_cod_documento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_anno_registrazione", il_anno_registrazione)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_registrazione", il_num_registrazione)
	else
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_documento", ls_cod_documento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_numeratore_documento", ls_numeratore_doc)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_anno_documento", ll_anno_documento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_documento", ll_num_documento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_data_fattura", ldt_data_fattura)
	end if
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_tipo_fat_ven", ls_cod_tipo_fat_ven)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_data_registrazione", ldt_data_registrazione)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_cliente", ls_cod_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_valuta", ls_cod_valuta)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_pagamento", ls_cod_pagamento)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_sconto", ld_sconto)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_ord_cliente", ls_num_ord_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_data_ord_cliente", ldt_data_ord_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_rag_soc_1", ls_rag_soc_1)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_rag_soc_2", ls_rag_soc_2)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_indirizzo", ls_indirizzo)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_localita", ls_localita)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_frazione", ls_frazione)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cap", ls_cap)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_provincia", ls_provincia)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_nota_testata", ls_nota_testata)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_nota_piede", ls_nota_piede)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_porto", ls_cod_porto)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_resa", ls_cod_resa)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_agente_1", ls_cod_agente_1)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_agenti_rag_soc_1", ls_anag_agenti_rag_soc_1)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_agente_2", ls_cod_agente_2)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_agenti_rag_soc_2", ls_anag_agenti_rag_soc_2)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_tot_merce", ld_tot_merci)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_sconti", tot_sconti_commerciali + tot_sconto_cassa)
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_tot_fattura_euro", ld_tot_fattura)	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_tot_fattura_lire", ld_tot_fattura_lire)	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_vettore", ls_cod_vettore)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_colli", ll_num_colli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_causale_trasporto", ls_causale_trasporto)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_telefono", ls_telefono_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_fax", ls_fax_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cambio_ven", ld_cambio_ven)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_localita", ls_localita_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_frazione", ls_frazione_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_cap", ls_cap_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_provincia", ls_provincia_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_telefono", ls_telefono_des)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_fax", ls_fax_des)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_tipi_fat_ven_des_tipo_fat_ven", ls_des_tipo_fat_ven)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
	if ls_flag_tipo_pagamento = "R" then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_cod_abi", ls_cod_abi)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_cod_cab", ls_cod_cab)
	else
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_des_banca", ls_des_banca)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_cod_abi", ls_cod_abi)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_cod_cab", ls_cod_cab)
	end if
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_valute_des_valuta", ls_des_valuta)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_valute_lingue_des_valuta", ls_des_valuta_lingua)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_porti_des_porto", ls_des_porto)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_rese_des_resa", ls_des_resa)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_mezzi_lingue_des_mezzo", ls_des_mezzo_lingua)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_mezzi_des_mezzo", ls_des_mezzo)
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_rag_soc_1", ls_vettore_rag_soc_1)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_indirizzo", ls_vettore_indirizzo)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_cap", ls_vettore_cap)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_localita", ls_vettore_localita)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_provincia", ls_vettore_provincia)	

	//##################################################################################################
	ll_num_aliquote = upperbound(ld_aliquote_iva) - 1
	if ll_num_aliquote > 0 then
		if ld_aliquote_iva[1] > 0 and not isnull(ld_aliquote_iva[1]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[1],"#0"), string(ld_imponibili_valuta[1],ls_formato), string(ld_iva_valuta[1], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_1", ld_aliquote_iva[1])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_1", ld_imponibili_valuta[1])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_1", ld_iva_valuta[1])
		else
			if len(ls_des_estesa[1]) > 0 then  ls_des_esenzione_iva[1]  = ls_des_estesa[1]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[1],ls_formato), ls_des_esenzione_iva[1] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_1", ld_imponibili_valuta[1])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_1", ls_des_esenzione_iva[1])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_1", ls_des_aliquota)
	end if
	//------------------------------------------------------------------------------------------------
	if ll_num_aliquote > 1 then
		if ld_aliquote_iva[2] > 0 and not isnull(ld_aliquote_iva[2]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[2],"#0"), string(ld_imponibili_valuta[2],ls_formato), string(ld_iva_valuta[2], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_2", ld_aliquote_iva[2])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_2", ld_imponibili_valuta[2])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_2", ld_iva_valuta[2])
		else
			if len(ls_des_estesa[2]) > 0 then  ls_des_esenzione_iva[2]  = ls_des_estesa[2]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[2],ls_formato), ls_des_esenzione_iva[2] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_2", ld_imponibili_valuta[2])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_2", ls_des_esenzione_iva[2])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_2", ls_des_aliquota)
	end if
	//------------------------------------------------------------------------------------------------
	if ll_num_aliquote > 2 then
		if ld_aliquote_iva[3] > 0 and not isnull(ld_aliquote_iva[3]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[3],"#0"), string(ld_imponibili_valuta[3],ls_formato), string(ld_iva_valuta[3], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_3", ld_aliquote_iva[3])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_3", ld_imponibili_valuta[3])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_3", ld_iva_valuta[3])
		else
			if len(ls_des_estesa[3]) > 0 then  ls_des_esenzione_iva[3]  = ls_des_estesa[3]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[3],ls_formato), ls_des_esenzione_iva[3] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_3", ld_imponibili_valuta[3])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_3", ls_des_esenzione_iva[3])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_3", ls_des_aliquota)
	end if
	//------------------------------------------------------------------------------------------------
	if ll_num_aliquote > 3 then
		if ld_aliquote_iva[4] > 0 and not isnull(ld_aliquote_iva[4]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[4],"#0"), string(ld_imponibili_valuta[4],ls_formato), string(ld_iva_valuta[4], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_4", ld_aliquote_iva[4])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_4", ld_imponibili_valuta[4])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_4", ld_iva_valuta[4])
		else
			if len(ls_des_estesa[4]) > 0 then  ls_des_esenzione_iva[4]  = ls_des_estesa[4]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[4],ls_formato), ls_des_esenzione_iva[4] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_4", ld_imponibili_valuta[4])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_4", ls_des_esenzione_iva[4])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_4", ls_des_aliquota)
	end if
	//------------------------------------------------------------------------------------------------
	if ll_num_aliquote > 4 then
		if ld_aliquote_iva[5] > 0 and not isnull(ld_aliquote_iva[5]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[5],"#0"), string(ld_imponibili_valuta[5],ls_formato), string(ld_iva_valuta[5], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_5", ld_aliquote_iva[5])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_5", ld_imponibili_valuta[5])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_5", ld_iva_valuta[5])
		else
			if len(ls_des_estesa[5]) > 0 then  ls_des_esenzione_iva[5]  = ls_des_estesa[5]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[5],ls_formato), ls_des_esenzione_iva[5] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_5", ld_imponibili_valuta[5])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_5", ls_des_esenzione_iva[5])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_5", ls_des_aliquota)
	end if
	//------------------------------------------------------------------------------------------------
	if ll_num_aliquote > 5 then
		if ld_aliquote_iva[6] > 0 and not isnull(ld_aliquote_iva[6]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[6],"#0"), string(ld_imponibili_valuta[6],ls_formato), string(ld_iva_valuta[6], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_6", ld_aliquote_iva[6])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_6", ld_imponibili_valuta[6])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_6", ld_iva_valuta[6])
		else
			if len(ls_des_estesa[6]) > 0 then  ls_des_esenzione_iva[6]  = ls_des_estesa[6]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[6],ls_formato), ls_des_esenzione_iva[6] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_6", ld_imponibili_valuta[6])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_6", ls_des_esenzione_iva[6])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_6", ls_des_aliquota)
	end if
	//##################################################################################################

	if upperbound(ldt_scadenze) > 0 then
		if date(ldt_scadenze[1]) > date("01/01/1900") and not isnull(ldt_scadenze[1]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_1", ldt_scadenze[1])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_1", ld_rate_scadenze[1])
		end if
	end if
	if upperbound(ldt_scadenze) > 1 then
		if date(ldt_scadenze[2]) > date("01/01/1900") and not isnull(ldt_scadenze[2]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_2", ldt_scadenze[2])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_2", ld_rate_scadenze[2])
		end if
	end if
	if upperbound(ldt_scadenze) > 2 then
		if date(ldt_scadenze[3]) > date("01/01/1900") and not isnull(ldt_scadenze[3]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_3", ldt_scadenze[3])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_3", ld_rate_scadenze[3])
		end if
	end if
	if upperbound(ldt_scadenze) > 3 then
		if date(ldt_scadenze[4]) > date("01/01/1900") and not isnull(ldt_scadenze[4]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_4", ldt_scadenze[4])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_4", ld_rate_scadenze[4])
		end if
	end if
	if upperbound(ldt_scadenze) > 4 then
		if date(ldt_scadenze[5]) > date("01/01/1900") and not isnull(ldt_scadenze[5]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_5", ldt_scadenze[5])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_5", ld_rate_scadenze[5])
		end if
	end if
	if upperbound(ldt_scadenze) > 5 then
		if date(ldt_scadenze[6]) > date("01/01/1900") and not isnull(ldt_scadenze[6]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_6", ldt_scadenze[6])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_6", ld_rate_scadenze[6])
		end if
	end if
	
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_imponibile_iva_valuta", ld_imponibile_iva_valuta)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_iva_valuta", ld_importo_iva_valuta)
	lb_prima_riga = false
	if lb_flag_nota_piede then exit
loop
close cu_dettagli;
dw_report_fat_ven.reset_dw_modified(c_resetchildren)
dw_report_fat_ven.change_dw_current()
COMMIT;


// ----------- Michela: inserisco la possibilità di stampare il pdf
if dw_report_fat_ven.RowCount() < 1 then
	g_mb.messagebox("APICE","La fattura non possiede nessun dettaglio: impossibile creare il pdf", Stopsign!)
	return
end if

// creo il file PDF della fattura

//if ll_anno_documento > 0 and ll_num_documento > 0 and not isnull(ll_anno_documento) and not isnull(ll_num_documento) then
//
//	uo_archivia_pdf luo_pdf
//	luo_pdf = create uo_archivia_pdf
//	luo_pdf.uof_crea_pdf(dw_report_fat_ven, ref l_blob)
//	
//	SELECT prog_mimetype  
//	INTO   :ll_prog_mimytype  
//	FROM   tab_mimetype  
//	WHERE  cod_azienda = :s_cs_xx.cod_azienda and
//			 estensione = 'PDF'   ;
//	if sqlca.sqlcode = 100 then
//		g_mb.messagebox("APICE","Impossibile archiviare il documento: impostare il mimetype")
//		return
//	end if
//	
//	delete from tes_fat_ven_note
//	where       cod_azienda = :s_cs_xx.cod_azienda and
//	            anno_registrazione = :il_anno_registrazione and
//				   num_registrazione = :il_num_registrazione and
//				   cod_nota = 'FAT';
//	
//	
//	ls_stringa = "Fattura nr " + string(ll_anno_documento) + "/" + string(ll_num_documento) + " del " + string(ldt_data_fattura,"dd/mm/yyyy")
//	INSERT INTO tes_fat_ven_note  
//			(     cod_azienda,   
//			      anno_registrazione,   
//			      num_registrazione,   
//			      cod_nota,   
//			      des_nota,   
//			      prog_mimetype)  
//	VALUES (    :s_cs_xx.cod_azienda,   
//			      :il_anno_registrazione,   
//			      :il_num_registrazione,   
//			      'FAT',   
//			      :ls_stringa,   
//			      :ll_prog_mimytype )  ;
//	
//	updateblob tes_fat_ven_note
//	set        note_esterne = :l_blob
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           anno_registrazione = :il_anno_registrazione and
//				  num_registrazione = :il_num_registrazione and
//				  cod_nota = 'FAT';
//	
//	destroy luo_pdf
//	commit;
//	
//end if
//
//





end subroutine

public function integer wf_memorizza_blob (long fl_anno_registrazione, long fl_num_registrazione, string fs_path);long ll_prog_mimytype,ll_ret
blob l_blob
string ls_note, ls_nota, ls_cod_nota



ll_ret = f_file_to_blob(fs_path, l_blob)
if ll_ret < 0 then return -1

ll_ret = len(l_blob)


SELECT prog_mimetype  
INTO   :ll_prog_mimytype  
FROM   tab_mimetype  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 estensione = 'PDF'   ;
if sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE","Impossibile archiviare il documento: impostare il mimetype")
	return 0
end if

setnull(ls_cod_nota)

select max(cod_nota)
into   :ls_cod_nota
from   tes_fat_ven_note
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 cod_nota like 'F%';
		 
if isnull(ls_cod_nota) or len(ls_cod_nota) < 1 then
	ls_cod_nota = "F01"
else
	ls_cod_nota = right(ls_cod_nota, 2)
	ll_ret = long(ls_cod_nota)
	ll_ret ++
	ls_cod_nota = "F" + string(ll_ret, "00")
end if

ls_nota = "Invio fattura PDF tramite e-mail"
ls_note = "Fattura Inviata dall'utente " + s_cs_xx.cod_utente + "~r~n" + &
          "Data invio:" + string(today(), "dd/mm/yyyy") + "  Ora invio:" + string(now(), "hh:mm:ss")

INSERT INTO tes_fat_ven_note  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  cod_nota,   
		  des_nota, 
		  note,
		  prog_mimetype)  
VALUES ( :s_cs_xx.cod_azienda,   
		  :fl_anno_registrazione,   
		  :fl_num_registrazione,   
		  :ls_cod_nota,   
		  :ls_nota,   
		  :ls_note,
		  :ll_prog_mimytype )  ;

updateblob tes_fat_ven_note
set        note_esterne = :l_blob
where      cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fl_anno_registrazione and
			  num_registrazione = :fl_num_registrazione and
			  cod_nota = :ls_cod_nota;


end function

public subroutine wf_stampa (long copie);string ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_cod_vettore, ls_cod_cliente, ls_flag_tipo_cliente
long ll_num_copie, ll_num_copie_cee, ll_num_copie_extra_cee, ll_num_stampe, ll_i, ll_anno_registrazione, ll_num_registrazione, ll_zoom


select numero
into   :ll_zoom
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'ZFV';
		 
if sqlca.sqlcode <> 0 then ll_zoom = 100

if isnull(ll_zoom) then ll_zoom = 100


il_totale_pagine = 1

dw_report_fat_ven.object.datawindow.zoom = ll_zoom

if ib_email then
	// processo di invio tramite EMAIL

	// invio la mail solo della prima copia
	if is_flag_email[il_corrente] = "S" then 
		wf_email()
	end if

else

	ll_anno_registrazione = il_anno_registrazione
	ll_num_registrazione = il_num_registrazione
	
	select cod_tipo_fat_ven,
			 cod_vettore,
			 cod_cliente
	  into :ls_cod_tipo_fat_ven,
			 :ls_cod_vettore,
			 :ls_cod_cliente	
	  from tes_fat_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and anno_registrazione = :il_anno_registrazione
		and num_registrazione = :il_num_registrazione;
		
	if sqlca.sqlcode <> 0 then ll_num_stampe = 1
	
	select flag_tipo_fat_ven,
			 num_copie,
			 num_copie_cee,
			 num_copie_extra_cee
	  into :ls_flag_tipo_fat_ven,
			 :ll_num_copie,
			 :ll_num_copie_cee,
			 :ll_num_copie_extra_cee
	  from tab_tipi_fat_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <> 0 then ll_num_stampe = 1
	
	select flag_tipo_cliente 
	  into :ls_flag_tipo_cliente
	  from anag_clienti
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_cliente = :ls_cod_cliente;
	
	if sqlca.sqlcode <> 0 then ll_num_stampe = 1
	
	if isnull(ll_num_stampe) or ll_num_stampe = 0 then
		if ls_flag_tipo_cliente = 'C' then
			for ll_i = 1 to (ll_num_copie_cee)
				dw_report_fat_ven.Print( )
			next
		end if
		
		if ls_flag_tipo_cliente = 'E' then
			for ll_i = 1 to (ll_num_copie_extra_cee)
				dw_report_fat_ven.Print( )
			next
		end if	
		
		if ls_flag_tipo_cliente <> 'E' and ls_flag_tipo_cliente <> 'C' then
			if isnull(ls_cod_vettore) or ls_flag_tipo_fat_ven <> "I" then
				for ll_i = 1 to (ll_num_copie)
					dw_report_fat_ven.Print( )
				next
			elseif not isnull(ls_cod_vettore) and ls_flag_tipo_fat_ven = "I" then
				for ll_i = 1 to ll_num_copie
					dw_report_fat_ven.Print( )
				next			
			end if				
		end if		
	end if
end if
end subroutine

public subroutine _wf_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[]);string ls_cod_iva
long ll_i

declare cu_iva cursor for 
	select   iva_fat_ven.cod_iva, 
				iva_fat_ven.imponibile_iva, 
				iva_fat_ven.importo_iva,
				iva_fat_ven.perc_iva
	from     iva_fat_ven 
	where    iva_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				iva_fat_ven.anno_registrazione = :fl_anno_registrazione and 
				iva_fat_ven.num_registrazione = :fl_num_registrazione;

open cu_iva;

ll_i = 0
do while 0 = 0
	ll_i = ll_i + 1
   fetch cu_iva into :ls_cod_iva, 
							:fd_imponibile_iva_valuta[ll_i], 
							:fd_imposta_iva_valuta[ll_i],
							:fd_aliquote_iva[ll_i];

   if sqlca.sqlcode <> 0 then exit
	if fd_aliquote_iva[ll_i] = 0 then
		select tab_ive.des_iva
		into   :fs_des_esenzione[ll_i]
		from   tab_ive
		where  tab_ive.cod_azienda = :s_cs_xx.cod_azienda and
		       tab_ive.cod_iva = :ls_cod_iva;
	end if
loop

close cu_iva;

return

end subroutine

public function integer wf_imposta_filigrana ();string ls_path, ls_error

guo_functions.uof_get_parametro_azienda("FL1", ls_path)

if isnull(ls_path) or ls_path = "" then return -1

if guo_functions.uof_filigrana(s_cs_xx.volume + ls_path, dw_report_fat_ven, true, ls_error) < 0 then
	g_mb.error(ls_error)
	return -1
else
	return 0
end if
end function

public function integer wf_sendmail (string as_oggetto, string as_testo, string as_destinatari[], string as_allegati[], ref string as_errore);

uo_outlook			luo_outlook
string					ls_cc[], ls_mail
boolean				lb_ret


select e_mail
into :ls_mail
from utenti
where cod_utente=:s_cs_xx.cod_utente;

if not isnull(ls_mail) and ls_mail<> "" then ls_cc[1] = ls_mail


luo_outlook = create uo_outlook
lb_ret = luo_outlook.uof_invio_sendmail(as_destinatari[], ls_cc[], as_oggetto, as_testo, as_allegati[], as_errore)
destroy luo_outlook

if not lb_ret then return -1

return 0
end function

public function integer wf_testo_mail (string fs_rag_soc_1, long fl_anno, long fl_numero, datetime fdt_data, ref string fs_testo);string 			ls_default, ls_path, ls_nome_file, ls_testo, ls_mark_ragsoc, ls_mark_numero
string			ls_mark_data, ls_rag_soc_azienda, ls_tel, ls_fax

integer			li_FileNum

long				ll_pos_ragsoc, ll_pos_numero, ll_pos_data

//parametri
ls_nome_file = "testo_mail.txt"
ls_mark_ragsoc = "[RAGIONESOCIALE]"
ls_mark_numero = "[NUMERO]"
ls_mark_data = "[DATA]"

//il percorso contiene anche il backslash finale
ls_path = s_cs_xx.volume + s_cs_xx.risorse +"11.5\" + ls_nome_file

ls_default = "Gentile Cliente " + fs_rag_soc_1 + ", " + &
					"con la presente Le trasmettiamo in allegato la nostra fattura N°" +  string(fl_anno) + "/" + &
					string(fl_numero)
if not isnull(fdt_data) then ls_default+=  " del " + string(fdt_data,"dd/mm/yyyy") + "."
	
ls_default += "~r~n"
ls_default += "Per qualunque chiarimento siamo a Vostra disposizione.~r~n~r~n" + &
					"Cordiali Saluti~r~n"

select rag_soc_1, telefono, fax
into :ls_rag_soc_azienda, :ls_tel, :ls_fax
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

if ls_rag_soc_azienda<>"" and not isnull(ls_rag_soc_azienda) then ls_default += ls_rag_soc_azienda + "~r~n"
if ls_tel<>"" and not isnull(ls_tel) then ls_default += "Tel. "+ls_tel + "~r~n"
if ls_fax<>"" and not isnull(ls_fax) then ls_default += "Fax. "+ls_fax + "~r~n"

ls_default += is_disclaimer

if not FileExists(ls_path) then
	
	//questo alert lo commentiamo, perchè altrimenti potrebbe scocciare l'utente...
	//se il cliente chiede una personalizzazione del testo basterà creargli questo file
	
	/*
	g_mb.messagebox("APICE","Il file di configurazione del testo del messaggio e-mail '"+ls_path+&
												"' non esiste oppure è stato spostato!~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	*/
	//-------------------------------------------------------------------------------------------------------
	
	fs_testo = ls_default
	return 1
end if

//leggo dal file il testo personalizzato
li_FileNum = FileOpen(ls_path, TextMode!)
FileReadEx(li_FileNum, ls_testo)
FileClose(li_FileNum)


//sostituire i marcatori con i valori opportuni
//[RAGIONESOCIALE]
//[NUMERO]
//[DATA]

ll_pos_ragsoc = pos(ls_testo, ls_mark_ragsoc)
ll_pos_numero = pos(ls_testo, ls_mark_numero)
ll_pos_data = pos(ls_testo, ls_mark_data)

if ll_pos_ragsoc>0 then
else
	g_mb.messagebox("APICE","Marcatore "+ls_mark_ragsoc+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

if ll_pos_numero>0 then
else
	g_mb.messagebox("APICE","Marcatore "+ls_mark_numero+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

if ll_pos_data>0 then
else
	g_mb.messagebox("APICE","Marcatore "+ls_mark_data+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

fs_testo = ls_testo

//ragione sociale
if wf_replace_marker(fs_testo, ls_mark_ragsoc, fs_rag_soc_1, false)=1 then
else
	g_mb.messagebox("APICE","Errore in sostituzione marcatore "+ls_mark_ragsoc+" nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

//  anno/numero
if wf_replace_marker(fs_testo, ls_mark_numero, string(fl_anno)+"/"+string(fl_numero), false)=1 then
else
	g_mb.messagebox("APICE","Errore in sostituzione marcatore "+ls_mark_numero+" nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

//data
if wf_replace_marker(fs_testo, ls_mark_data, string(fdt_data,"dd/mm/yyyy"), false)=1 then
else
	g_mb.messagebox("APICE","Errore in sostituzione marcatore "+ls_mark_numero+" nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

return 1
end function

public function integer wf_email ();// funzione che provvedere all'invio MAIL della fattura corrente

integer 	li_ret
long    	ll_anno_documento, ll_num_documento, ll_prog_mimytype
string 	ls_path, ls_messaggio, ls_destinatari[], ls_allegati[], ls_subject, ls_message, &
			ls_rag_soc_1,ls_cod_cliente, ls_nota, ls_email_utente, ls_cc[], ls_errore
datetime ldt_data_fattura
uo_archivia_pdf luo_archivia_pdf
uo_outlook      luo_outlook
uo_archivia_pdf luo_pdf

luo_outlook = CREATE uo_outlook
luo_pdf = create uo_archivia_pdf

if ib_nr_nr then
	luo_outlook.ib_silent_mode = true
end if

ls_path = luo_pdf.uof_crea_pdf_path(dw_report_fat_ven)

if ls_path = "errore" then
	g_mb.messagebox ("Errore","Errore nella creazione del file pdf.")
	return -1
end if

ls_destinatari[1] = is_email_amministrazione[il_corrente]
if not isnull( is_email_utente[il_corrente] ) then
	ls_cc[1] = is_email_utente[il_corrente]
end if
ls_allegati[1]    = ls_path

select anno_documento,
       num_documento,
		 data_fattura, 
		 cod_cliente
into   :ll_anno_documento,
		 :ll_num_documento,
		 :ldt_data_fattura,
	    :ls_cod_cliente
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and	
       anno_registrazione = :il_anno[il_corrente] and
		 num_registrazione = :il_num[il_corrente];

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca dati fattura (wf_email)~r~n" + sqlca.sqlerrtext)
end if


select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca dati cliente (wf_email)~r~n" + sqlca.sqlerrtext)
end if

ls_subject = ls_rag_soc_1+" - Invio Fattura nr. " + string(ll_anno_documento) + "/" + string(ll_num_documento)

if not isnull(ldt_data_fattura) then ls_subject += " del " + string(ldt_data_fattura,"dd/mm/yyyy")


wf_testo_mail(ls_rag_soc_1, il_anno[il_corrente], il_num[il_corrente], ldt_data_fattura, ls_message)

// stefanop 16/02/2012: sostituito metodo con l'invio sendmail 
// ------------------------------------------------------------------------------------------------------
if not luo_outlook.uof_invio_sendmail( ls_destinatari[], ls_cc[], ls_subject, ls_message, ls_allegati[], ref ls_messaggio) then
	g_mb.messagebox("APICE", "Errore in fase di invio fattura~r~n" + ls_messaggio)
else
	// tutto bene; memorizzo pure in tes_fat_ven l'invio
	update tes_fat_ven
	set    flag_email_inviata = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and	
			 anno_registrazione = :il_anno[il_corrente] and
			 num_registrazione = :il_num[il_corrente];
	if wf_memorizza_blob(il_anno[il_corrente], il_num[il_corrente], ls_path) < 0 then
		g_mb.messagebox("APICE", "Errore in fase di invio fattura~r~n" + ls_errore)
		rollback;
	else
		commit;
	end if
end if
// ------------------------------------------------------------------------------------------------------

destroy luo_archivia_pdf
destroy luo_outlook

filedelete(ls_path)

return 0
end function

public function integer wf_replace_marker (ref string as_source, string as_find, string as_replace, boolean ab_case_sensitive);/*
Function fof_replace_text
Created by: Michele
Creation Date: 13/12/2007
Comment: Given a string <as_source>, replaces all occurencies of <as_find> with <as_replace>

as_source                                                          Reference to the string to process
as_find                                                               The text we wish to change
as_replace                                                        The new text to be put in place of <as_find>

Return Values
Value                                                                   Comments
 1                                                                                           Everything OK
-1                                                                                          Some error occured
*/
long		ll_pos

 if isnull(as_source) or as_source = "" then
	return -1
end if

if isnull(as_find) or as_find = "" then
	return -1
end if

 ll_pos = 1

do
	 if ab_case_sensitive then
		ll_pos = pos(as_source,as_find,ll_pos)
	 else
		ll_pos = pos(lower(as_source),lower(as_find),ll_pos)
	 end if

	 if ll_pos = 0 then
		continue
	 end if
	 
	 as_source = replace(as_source,ll_pos,len(as_find),as_replace)	 
	 ll_pos += len(as_replace)

loop while ll_pos > 0

return 1


end function

event pc_setwindow;call super::pc_setwindow;boolean lb_invio_singolo_mail=false

long    ll_i, ll_anno, ll_num, ll_num_documento, ll_ret

string  ls_cod_documento,ls_cod_tipo_fat_ven,ls_dataobject, ls_flag_email, ls_email_amministrazione, &
		  ls_cod_cliente, ls_flag_email_inviata, ls_email_utente


dw_report_fat_ven.ib_dw_report=true

set_w_options(c_noresizewin)
il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2

if il_corrente = 0 then 
	ib_nr_nr = s_cs_xx.parametri.parametro_b_1
	ib_email = s_cs_xx.parametri.parametro_b_2
end if

// carico il dataobject a seconda che la fattura sia immediata o differita

setnull(ls_dataobject)

if not ib_nr_nr  then

	select cod_tipo_fat_ven,
			 cod_cliente,
			 flag_email_inviata
	into   :ls_cod_tipo_fat_ven,
			 :ls_cod_cliente,
			 :ls_flag_email_inviata
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Fattura richiesta non trovata~r~n" + sqlca.sqlerrtext)
		return
	end if
	
	if not isnull(ls_cod_cliente) then
	
		select flag_accetta_mail,
				 email_amministrazione
		into   :ls_flag_email,
				 :ls_email_amministrazione
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Cliente " + ls_cod_cliente + " inesistente in anagrafica~r~n" + sqlca.sqlerrtext)
			return
		end if
	
	else
		ls_flag_email = "N"
		ls_email_amministrazione = "N"
	end if

	select dataobject
	into   :ls_dataobject
	from   tab_tipi_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if isnull(ls_dataobject) or len(ls_dataobject) < 1 then
		g_mb.messagebox("APICE","Manca il dataobject nella tabella TIPI FATTURE~r~nViene applicato il report di default",stopsign!)
	else
		dw_report_fat_ven.dataobject = ls_dataobject
	end if
	
end if

// ------------------------------ fine modifica 1/10/2007  --------------------------

dw_report_fat_ven.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
							  c_disablecc, &
							 c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
												
save_on_close(c_socnosave)


// *** metto il focus
dw_report_fat_ven.change_dw_focus(dw_report_fat_ven)
// ***

if il_corrente = 0 then
	
	ib_nr_nr = s_cs_xx.parametri.parametro_b_1
	s_cs_xx.parametri.parametro_b_1 = false
	
	if ib_nr_nr then
		
		declare stampa cursor for
		select   anno_registrazione,
					num_registrazione,
					flag_email,
					email_amministrazione,
					email_utente
		from     stampa_fat_ven
		where    cod_azienda = :s_cs_xx.cod_azienda and
					cod_utente = :s_cs_xx.cod_utente
		order by anno_registrazione ASC,
					num_registrazione ASC;
					
		open stampa;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore nella open del cursore stampa: " + sqlca.sqlerrtext)
			return
		end if
		
		ll_i = 1
		
		do while true
			
			fetch stampa
			into  :ll_anno,
					:ll_num,
					:ls_flag_email,
					:ls_email_amministrazione,
					:ls_email_utente;
					
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore nella fetch del cursore stampa: " + sqlca.sqlerrtext)
				close stampa;
				return
			elseif sqlca.sqlcode = 100 then
				close stampa;
				exit
			end if
			
			il_anno[ll_i] = ll_anno
			il_num[ll_i] = ll_num
			
			//################################################################
			is_flag_email[ll_i] = ls_flag_email
			is_email_amministrazione[ll_i] = ls_email_amministrazione
			is_email_utente[ll_i] = ls_email_utente
			//################################################################
			
			ll_i++
				
		loop
		
	else
		
		//################################################################
		ib_email = false
		is_flag_email[1] = "N"
		is_email_amministrazione[1] = ""
		//################################################################
		
		il_anno[1] = s_cs_xx.parametri.parametro_d_1
		il_num[1]  = s_cs_xx.parametri.parametro_d_2
		
	end if
	
end if

for il_corrente = 1 to upperbound(il_num[])
	
	il_anno_registrazione = il_anno[il_corrente]
	il_num_registrazione = il_num[il_corrente]
	
	wf_impostazioni()
	
	wf_report()
	
	if ib_nr_nr then
		triggerevent("pc_print")
		dw_report_fat_ven.reset()
		
	//###############################################################	
	else
		// l'attuale cliente prevede l'invio della fattura tramite mail
		if ls_flag_email = "S" and not isnull(ls_flag_email) then
			
			ll_ret = g_mb.messagebox("APICE","Il cliente selezionato prevede l'invio fattura tramite E-Mail: procedo con l'invio?",Question!, YesNo!,2) 
			
			if ll_ret = 1 then
			
				is_flag_email[1] = "S"
				is_email_amministrazione[1] = ls_email_amministrazione
				
				update tes_fat_ven
				set flag_email_inviata = 'S'
				where cod_azienda = :s_cs_xx.cod_azienda and
				      anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
						num_registrazione = :s_cs_xx.parametri.parametro_d_2; 
						
				cb_singola.event clicked()
				
				// il commit avviene dentro al pulsante cb_singola perchè la funzione provvede alla 
				// memorizzazione del documento nel database.
				
			else
				
				update tes_fat_ven
				set flag_email_inviata = 'N'
				where cod_azienda = :s_cs_xx.cod_azienda and
				      anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
						num_registrazione = :s_cs_xx.parametri.parametro_d_2; 
						
				ib_email = false
				
			end if
			
		end if
		
	end if
	
	
next

il_corrente --

// **************  metto la DW in anteprima  **********************
dw_report_fat_ven.object.datawindow.print.preview = 'Yes'
dw_report_fat_ven.object.datawindow.print.preview.rulers = 'Yes'


// stefnaop: 24/03/2014
wf_imposta_filigrana()

end event

on w_report_fat_ven_euro.create
int iCurrent
call super::create
this.cb_archivia=create cb_archivia
this.cb_singola=create cb_singola
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_4=create cb_4
this.cb_1=create cb_1
this.st_copie=create st_copie
this.st_1=create st_1
this.dw_report_fat_ven=create dw_report_fat_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_archivia
this.Control[iCurrent+2]=this.cb_singola
this.Control[iCurrent+3]=this.cb_3
this.Control[iCurrent+4]=this.cb_2
this.Control[iCurrent+5]=this.cb_4
this.Control[iCurrent+6]=this.cb_1
this.Control[iCurrent+7]=this.st_copie
this.Control[iCurrent+8]=this.st_1
this.Control[iCurrent+9]=this.dw_report_fat_ven
end on

on w_report_fat_ven_euro.destroy
call super::destroy
destroy(this.cb_archivia)
destroy(this.cb_singola)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_4)
destroy(this.cb_1)
destroy(this.st_copie)
destroy(this.st_1)
destroy(this.dw_report_fat_ven)
end on

event pc_print;//IN QUESTO EVENTO IL FLAG EXTEND ANCESTOR DEVE ESSERE DISATTIVATO
cb_1.triggerevent("clicked")


end event

event close;call super::close;if ib_nr_nr then
	
	delete
	from   stampa_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_utente = :s_cs_xx.cod_utente;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella delete di stampa_fat_ven: " + sqlca.sqlerrtext)
		rollback;
	else
		commit;
	end if
	
	g_mb.messagebox("APICE","Processo di stampa completato!")
	
end if
end event

event open;call super::open;if ib_nr_nr then
	postevent("ue_close")
end if
end event

type cb_archivia from commandbutton within w_report_fat_ven_euro
integer x = 2761
integer y = 8
integer width = 375
integer height = 136
integer taborder = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "&Archivia"
end type

event clicked;string ls_path
uo_archivia_pdf luo_pdf
uo_upload_documenti luo_upload

luo_pdf = create uo_archivia_pdf

ls_path = luo_pdf.uof_crea_pdf_path(dw_report_fat_ven)

if ls_path = "errore" then
	g_mb.messagebox ("Errore","Errore nella creazione del file pdf.")
	return -1
end if


try 
	luo_upload = create uo_upload_documenti
	luo_upload.uof_archivio_fattura_vendita(il_anno_registrazione, il_num_registrazione, ls_path)
	g_mb.success("Documento archiviato con successo.")

	return 0
catch (uo_upload_exception e)
	g_mb.error(e.getMessage())
end try

end event

type cb_singola from picturebutton within w_report_fat_ven_euro
integer x = 2359
integer y = 8
integer width = 357
integer height = 136
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "E-mail"
string picturename = "C:\cs_115\framework\RISORSE\11.5\ft_email.png"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;string ls_cod_cliente, ls_flag_accetta_mail,ls_email_amministrazione,ls_email_utente

il_corrente = 1

select cod_cliente
into   :ls_cod_cliente
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione and
		 num_registrazione =  :il_num_registrazione;
if sqlca.sqlcode <> 0 or len(ls_cod_cliente) = 0 or isnull(ls_cod_cliente) then
	g_mb.warning(g_str.format("Impossibile trovare il cliente per la fattura $1/$2 (numero registrazione interno)",il_anno_registrazione,il_num_registrazione))
	return
end if
		 
select flag_accetta_mail,
       email_amministrazione
into   :ls_flag_accetta_mail,
       :ls_email_amministrazione
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 or len(ls_email_amministrazione) = 0 or isnull(ls_email_amministrazione) then
	g_mb.warning(g_str.format("Attenzione, il cliente $1 non è stato trovat oppure non è presente alcuna mail a cui inviare la fattura!",ls_cod_cliente))
	return
end if

select e_mail
into	:ls_email_utente
from  utenti
where cod_utente = :s_cs_xx.cod_utente;
if sqlca.sqlcode <> 0 or len(ls_email_utente) = 0 or isnull(ls_email_utente) then
	g_mb.warning("Attenzione, verificare la mail dell'utente connesso!")
	return
end if

if ls_flag_accetta_mail = "S" then 
	ib_email = true
else
	return
end if

if isnull(ls_email_amministrazione) or len(ls_email_amministrazione) < 1 or pos(ls_email_amministrazione, "@")< 1 then
	g_mb.messagebox("APICE","Impossibile inviare la mail: verificare le impostazione in anagrafica cliente")
	return
end if

is_flag_email[1] = "S"
is_email_amministrazione[1] = ls_email_amministrazione
is_email_utente[1] = ls_email_utente

wf_stampa(1)

ib_email = false
end event

type cb_3 from picturebutton within w_report_fat_ven_euro
integer x = 1957
integer y = 8
integer width = 357
integer height = 136
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Succ."
string picturename = "C:\cs_115\framework\RISORSE\11.5\ft_pgsucc.png"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;dw_report_fat_ven.scrollnextpage()
end event

type cb_2 from picturebutton within w_report_fat_ven_euro
integer x = 1554
integer y = 8
integer width = 357
integer height = 136
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Prec."
string picturename = "C:\cs_115\framework\RISORSE\11.5\ft_pgprec.png"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;dw_report_fat_ven.scrollpriorpage()
end event

type cb_4 from picturebutton within w_report_fat_ven_euro
integer x = 1152
integer y = 8
integer width = 357
integer height = 136
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "1 Copia"
string picturename = "C:\cs_115\framework\RISORSE\11.5\Ft_singola.png"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;wf_stampa(1)
end event

type cb_1 from picturebutton within w_report_fat_ven_euro
integer x = 750
integer y = 8
integer width = 357
integer height = 136
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Stampa"
boolean originalsize = true
string picturename = "C:\cs_115\framework\RISORSE\11.5\Print.png"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;ib_stampa = true
wf_stampa(il_num_copie)		

end event

type st_copie from statictext within w_report_fat_ven_euro
integer x = 251
integer y = 20
integer width = 210
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_1 from statictext within w_report_fat_ven_euro
integer x = 18
integer y = 20
integer width = 215
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "COPIE:"
boolean focusrectangle = false
end type

type dw_report_fat_ven from uo_cs_xx_dw within w_report_fat_ven_euro
event printed pbm_dwnprintend
integer x = 14
integer y = 160
integer width = 3657
integer height = 4336
integer taborder = 20
string dataobject = "d_report_fat_ven_euro"
boolean livescroll = true
end type

event printed;ib_stampando = true
end event

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

event pcd_first;call super::pcd_first;wf_report()
end event

event pcd_last;call super::pcd_last;wf_report()
end event

event pcd_next;call super::pcd_next;wf_report()
end event

event pcd_previous;call super::pcd_previous;wf_report()
end event

event printpage;call super::printpage;//if ib_stampa and pagenumber <> il_pagina_corrente then
//	return 1
//end if

//if ib_stampa then
//	return 1
//end if
end event

event printstart;call super::printstart;il_totale_pagine = pagesmax
end event

