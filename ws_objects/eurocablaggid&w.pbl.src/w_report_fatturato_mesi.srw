﻿$PBExportHeader$w_report_fatturato_mesi.srw
$PBExportComments$Report Statistiche: fatturato per agente ultimi 12 mesi
forward
global type w_report_fatturato_mesi from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_fatturato_mesi
end type
type cb_report from commandbutton within w_report_fatturato_mesi
end type
type cb_selezione from commandbutton within w_report_fatturato_mesi
end type
type dw_report from uo_cs_xx_dw within w_report_fatturato_mesi
end type
type sle_1 from singlelineedit within w_report_fatturato_mesi
end type
type dw_selezione from uo_cs_xx_dw within w_report_fatturato_mesi
end type
end forward

global type w_report_fatturato_mesi from w_cs_xx_principale
integer width = 3589
integer height = 1988
string title = "Stampa statistiche: fatturato per agente"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_report dw_report
sle_1 sle_1
dw_selezione dw_selezione
end type
global w_report_fatturato_mesi w_report_fatturato_mesi

forward prototypes
public function integer wf_scrivi_riga (long fl_riga_cli, string fs_cod_cliente, string fs_rag_soc_1, double fd_imponib[], string fs_cod_agente, ref string fs_error)
end prototypes

public function integer wf_scrivi_riga (long fl_riga_cli, string fs_cod_cliente, string fs_rag_soc_1, double fd_imponib[], string fs_cod_agente, ref string fs_error);// 	li_risp = wf_scrivi_riga(ll_riga_cli, ls_cliente_corrente, ls_rag_soc_1, imponib, fs_cod_agente, ls_error)
// scrive i campi di una riga del report
string ls_rag_soc_1_ag, ls_rag_soc_1, ls_cod_agente_1, ls_cod_agente_2

if fl_riga_cli = 0 then return 0		// deve ancora leggere il primo cliente

select rag_soc_1, cod_agente_1, cod_agente_2
into :ls_rag_soc_1, :ls_cod_agente_1, :ls_cod_agente_2
from anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_cliente = :fs_cod_cliente
using sqlca;


if (not isnull(ls_cod_agente_1) or ls_cod_agente_1 <> "") and ls_cod_agente_1 = fs_cod_agente then 
	select rag_soc_1
	into :ls_rag_soc_1_ag
	from anag_agenti
	where cod_azienda = :s_cs_xx.cod_azienda
		and cod_agente = :ls_cod_agente_1
	using sqlca;
else 
	ls_rag_soc_1_ag = " "
end if

//if (not isnull(ls_cod_agente_2) or ls_cod_agente_2 <> "") and ls_cod_agente_2 = fs_cod_agente and ls_rag_soc_1_ag = " " then 
//	select rag_soc_1
//	into :ls_rag_soc_1_ag
//	from anag_agenti
//	where cod_azienda = :s_cs_xx.cod_azienda
//		and cod_agente = :ls_cod_agente_2
//	using sqlca;
//elseif ls_rag_soc_1_ag = " " then
//	ls_rag_soc_1_ag = " "
//end if

// ------------ modificato da Alessio il 19/06/2000: 
// ------------ scrive sempre rag. soc. in anag. clienti, perchè intestatario della fattura
//if  not isnull(fs_rag_soc_1) then 
//	ls_rag_soc_1 = fs_rag_soc_1
//end if
//




dw_report.setitem(fl_riga_cli, "rs_cod_cliente", fs_cod_cliente)
dw_report.setitem(fl_riga_cli, "rs_rag_soc_1", ls_rag_soc_1)
dw_report.setitem(fl_riga_cli, "rs_cod_agente_1", fs_cod_agente)
dw_report.setitem(fl_riga_cli, "rs_rag_soc_1_ag", ls_rag_soc_1_ag)
dw_report.setitem(fl_riga_cli, "rd_imponib_1", fd_imponib[1])
dw_report.setitem(fl_riga_cli, "rd_imponib_2", fd_imponib[2])
dw_report.setitem(fl_riga_cli, "rd_imponib_3", fd_imponib[3])
dw_report.setitem(fl_riga_cli, "rd_imponib_4", fd_imponib[4])
dw_report.setitem(fl_riga_cli, "rd_imponib_5", fd_imponib[5])
dw_report.setitem(fl_riga_cli, "rd_imponib_6", fd_imponib[6])
dw_report.setitem(fl_riga_cli, "rd_imponib_7", fd_imponib[7])
dw_report.setitem(fl_riga_cli, "rd_imponib_8", fd_imponib[8])
dw_report.setitem(fl_riga_cli, "rd_imponib_9", fd_imponib[9])
dw_report.setitem(fl_riga_cli, "rd_imponib_10", fd_imponib[10])
dw_report.setitem(fl_riga_cli, "rd_imponib_11", fd_imponib[11])
dw_report.setitem(fl_riga_cli, "rd_imponib_12", fd_imponib[12])


return 0
end function

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
								 c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report




this.x =673
this.y = 265
this.width = 1950
this.height = 490

end event

on w_report_fatturato_mesi.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_report=create dw_report
this.sle_1=create sle_1
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.sle_1
this.Control[iCurrent+6]=this.dw_selezione
end on

on w_report_fatturato_mesi.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_report)
destroy(this.sle_1)
destroy(this.dw_selezione)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_agente_1", &
							sqlca, &
							"anag_agenti", &
							"cod_agente", &
                     "rag_soc_1", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "'and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

// and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")



end event

type cb_annulla from commandbutton within w_report_fatturato_mesi
integer x = 1097
integer y = 280
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esci"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_fatturato_mesi
integer x = 1486
integer y = 280
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_flag_iva, ls_cod_cliente, ls_rag_soc_1_ag, ls_rag_soc_1, ls_rag_soc_1_corr, &
		 ls_cliente_corrente , ls_error, ls_cod_agente_1, ls_sqlstatement, ls_clausola, ls_mese, ls_cod_cliente_ricerca, &
		 ls_ag_1, ls_ag_2
date ldd_data_riferimento
datetime ldt_data_rif_da, ldt_data_rif_a, ldt_data_fattura
integer li_mese,li_anno, li_i, li_risp, li_mese_1, li_cont
dec{4} ld_imponibile_iva, ld_importo_iva, imponib[], ld_tot_1_riga
long ll_riga_cli
string ls_mesi[], ls_cod_notacred, ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven

dw_selezione.accepttext()
ls_cod_agente_1 = dw_selezione.getitemstring(1,"cod_agente_1")
ls_flag_iva = dw_selezione.getitemstring(1,"flag_iva")
ldd_data_riferimento = dw_selezione.getitemdate(1,"data_riferimento")
ls_cod_cliente_ricerca = dw_selezione.getitemstring(1,"cliente")

//if isnull(ls_cod_agente_1) then ls_cod_agente_1 = "%"
if isnull(ls_flag_iva) then  ls_flag_iva  = "N"

li_mese = month(ldd_data_riferimento)
li_mese_1 = li_mese
li_anno = year(ldd_data_riferimento)
if li_mese = 12 then 
	ldt_data_rif_da = datetime(date("01/01/" + string(li_anno)))
	ldt_data_rif_a = datetime(date("01/01/" + string(li_anno + 1)))
else
	ldt_data_rif_da = datetime(date("01/" +string(li_mese + 1) +"/"+string(li_anno - 1)))
	ldt_data_rif_a = datetime(date("01/" +string(li_mese + 1) +"/"+string(li_anno)))
end if
// messagebox("debug", string(ldt_data_rif_da) + " -> " + string(ldt_data_rif_a))

//select stringa
//into :ls_cod_notacred
//from parametri_azienda
//where cod_azienda =:s_cs_xx.cod_azienda
//   and cod_parametro = 'NC'
//	using sqlca;


ls_sqlstatement = "select tes_fat_ven.data_fattura, " + &
					"tes_fat_ven.cod_tipo_fat_ven, " + &
					"tes_fat_ven.cod_cliente, " + &
					"tes_fat_ven.rag_soc_1, " + &
					"tes_fat_ven.imponibile_iva, " + &
					"tes_fat_ven.importo_iva " + &
					"from tes_fat_ven , " + &
				  	"anag_clienti " + &
					"where  tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda+"' " + &
					"and tes_fat_ven.cod_azienda = anag_clienti.cod_azienda " + &
					"and tes_fat_ven.cod_cliente = anag_clienti.cod_cliente " + &
					"and tes_fat_ven.flag_calcolo = 'S' " + &
					"and tes_fat_ven.data_fattura < '" + string(ldt_data_rif_a, "yyyy-mm-dd") + "' " + &
					"and tes_fat_ven.data_fattura >= '" + string(ldt_data_rif_da, "yyyy-mm-dd") + "' " + &
					"and tes_fat_ven.flag_blocco = 'N' " 


// Nota:	al posto di tes_fat_ven.flag_calcolo = 'S' andrebbe tes_fat_ven.flag_movimenti = 'S'
if isnull(ls_cod_cliente_ricerca) then
	if isnull(ls_cod_agente_1) then
		ls_clausola = " order by  tes_fat_ven.cod_cliente"
	else
		ls_clausola = " and (tes_fat_ven.cod_agente_1 = '" + ls_cod_agente_1 + "' " + &
						  " or tes_fat_ven.cod_agente_2 = '" + ls_cod_agente_1 + "') " + &
						  "order by  tes_fat_ven.cod_cliente"
	end if
else
	if isnull(ls_cod_agente_1) then
		ls_clausola = " and (tes_fat_ven.cod_cliente = '" + ls_cod_cliente_ricerca + "' "+ &
							")order by  tes_fat_ven.cod_cliente"
	else
		ls_clausola = " and (tes_fat_ven.cod_cliente = '" + ls_cod_cliente_ricerca + "') "+ &
							"and (tes_fat_ven.cod_agente_1 = '" + ls_cod_agente_1 + "' " + &
						  " or tes_fat_ven.cod_agente_2 = '" + ls_cod_agente_1 + "') " + &
						  "order by  tes_fat_ven.cod_cliente"
	end if
//	ls_clausola = " and tes_fat_ven.cod_agente_1 = '" + ls_cod_agente_1 + "' " + &
//					  "order by  tes_fat_ven.cod_cliente"
//
end if
	
ls_sqlstatement = ls_sqlstatement + ls_clausola

// messagebox("debug", ls_sqlstatement)

DECLARE cur_fat DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sqlstatement;
OPEN DYNAMIC cur_fat;

ls_cliente_corrente = " "
ll_riga_cli = 0

dw_report.reset()

DO while true
	
	FETCH cur_fat
	INTO  :ldt_data_fattura,
	      :ls_cod_tipo_fat_ven,
			:ls_cod_cliente,
			:ls_rag_soc_1, 
	      :ld_imponibile_iva,
			:ld_importo_iva;
			
	
   if (sqlca.sqlcode = 100) then
		if  ls_cliente_corrente <> " " then  // memorizza l'ultimo cliente prima di uscire dal loop
			li_risp = wf_scrivi_riga(ll_riga_cli, ls_cliente_corrente, ls_rag_soc_1_corr, imponib, ls_cod_agente_1, ls_error)
		end if
		exit
	elseif sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore nel DB " , SQLCA.SQLErrText)
		return -1
	end if
	
	sle_1.text = "Cod. cliente: " + ls_cod_cliente
	
	if ls_cod_cliente <> ls_cliente_corrente  then
		li_risp = wf_scrivi_riga(ll_riga_cli, ls_cliente_corrente, ls_rag_soc_1_corr, imponib, ls_cod_agente_1, ls_error)
		ls_cliente_corrente = ls_cod_cliente
		// oss. rag_soc_1 è memorizzata in tes_fat_ven solo se destinazione è diversa da quella in anag_clienti
		ls_rag_soc_1_corr = ls_rag_soc_1		
		for li_i = 1 to 12
			imponib[li_i] = 0
		next
		
		ll_riga_cli = dw_report.insertrow(0)
		
	end if	

	li_mese = month(date(string(ldt_data_fattura,"dd-mm-yyyy"))) // mese della fattura corrente
	li_mese = li_mese - li_mese_1
	if li_mese <= 0 then li_mese = li_mese + 12
	
	select flag_tipo_fat_ven
	into :ls_flag_tipo_fat_ven
	from tab_tipi_fat_ven
	where cod_azienda = :s_cs_xx.cod_azienda 
		and cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <>0 then
		g_mb.messagebox("Apice", "Errore lettura tipo fatture vendita " + sqlca.sqlerrtext)
		return 	
	end if
	
	if ls_flag_tipo_fat_ven = "N" then	//Nota di credito
		ld_imponibile_iva = ld_imponibile_iva * (-1)
		ld_importo_iva = ld_importo_iva * (-1)
	end if
	
	if (ls_flag_tipo_fat_ven = "D" or ls_flag_tipo_fat_ven = "I" or ls_flag_tipo_fat_ven = "N") then 	//non contare fatture pro forma
		if ls_flag_iva = 'N' then
			imponib[li_mese] = imponib[li_mese] + ld_imponibile_iva
		else
			imponib[li_mese] = imponib[li_mese] + ld_imponibile_iva + ld_importo_iva
		end if	
	end if
	
LOOP

CLOSE cur_fat ;


dw_report.setsort( "rs_cod_agente_1 A, rs_rag_soc_1 A")
dw_report.sort()

ls_mesi[1] = "Gennaio "
ls_mesi[2] = "Febbraio "
ls_mesi[3] = "Marzo "
ls_mesi[4] = "Aprile "
ls_mesi[5] = "Maggio "
ls_mesi[6] = "Giugno "
ls_mesi[7] = "Luglio "
ls_mesi[8] = "Agosto "
ls_mesi[9] = "Settembre "
ls_mesi[10] = "Ottobre "
ls_mesi[11] = "Novembre "
ls_mesi[12] = "Dicembre "

if (1 + li_mese_1) > 12 then 
	dw_report.object.rd_imponib_1_t.text = ls_mesi[1]
else
	dw_report.object.rd_imponib_1_t.text = ls_mesi[1 + li_mese_1]
end if
if (2 + li_mese_1) > 12 then 
	dw_report.object.rd_imponib_2_t.text = ls_mesi[2 +li_mese_1 -12 ]
else
	dw_report.object.rd_imponib_2_t.text = ls_mesi[2 + li_mese_1]
end if
if (3 + li_mese_1) > 12 then 
	dw_report.object.rd_imponib_3_t.text = ls_mesi[3 +li_mese_1 -12 ]
else
	dw_report.object.rd_imponib_3_t.text = ls_mesi[3 + li_mese_1]
end if
if (4 + li_mese_1) > 12 then 
	dw_report.object.rd_imponib_4_t.text = ls_mesi[4 +li_mese_1 -12 ]
else
	dw_report.object.rd_imponib_4_t.text = ls_mesi[4 + li_mese_1]
end if
if (5 + li_mese_1) > 12 then 
	dw_report.object.rd_imponib_5_t.text = ls_mesi[5 +li_mese_1 -12 ]
else
	dw_report.object.rd_imponib_5_t.text = ls_mesi[5 + li_mese_1]
end if
if (6 + li_mese_1) > 12 then 
	dw_report.object.rd_imponib_6_t.text = ls_mesi[6 +li_mese_1 -12 ]
else
	dw_report.object.rd_imponib_6_t.text = ls_mesi[6 + li_mese_1]
end if
if (7 + li_mese_1) > 12 then 
	dw_report.object.rd_imponib_7_t.text = ls_mesi[7 +li_mese_1 -12 ]
else
	dw_report.object.rd_imponib_7_t.text = ls_mesi[7 + li_mese_1]
end if
if (8 + li_mese_1) > 12 then 
	dw_report.object.rd_imponib_8_t.text = ls_mesi[8 +li_mese_1 -12 ]
else
	dw_report.object.rd_imponib_8_t.text = ls_mesi[8 + li_mese_1]
end if
if (9 + li_mese_1) > 12 then 
	dw_report.object.rd_imponib_9_t.text = ls_mesi[9 +li_mese_1 -12 ]
else
	dw_report.object.rd_imponib_9_t.text = ls_mesi[9 + li_mese_1]
end if
if (10 + li_mese_1) > 12 then 
	dw_report.object.rd_imponib_10_t.text = ls_mesi[10 +li_mese_1 -12 ]
else
	dw_report.object.rd_imponib_10_t.text = ls_mesi[10 + li_mese_1]
end if
if (11 + li_mese_1) > 12 then 
	dw_report.object.rd_imponib_11_t.text = ls_mesi[11 +li_mese_1 -12 ]
else
	dw_report.object.rd_imponib_11_t.text = ls_mesi[11 + li_mese_1]
end if

dw_report.object.rd_imponib_12_t.text = ls_mesi[li_mese_1]

ls_mese = ls_mesi[li_mese_1] + string(li_anno)
dw_report.object.data_riferimento.text = "Ultimi 12 mesi, fino a " + ls_mese

// elimino eventuali righe vuote all'inizio del report ( ci sono, ci sono)
do while dw_report.rowcount() <> 0

	ld_tot_1_riga = dw_report.getitemnumber(1,"rd_imponib_1") + dw_report.getitemnumber(1,"rd_imponib_2") + &
	                dw_report.getitemnumber(1,"rd_imponib_3") + dw_report.getitemnumber(1,"rd_imponib_4") + &
						 dw_report.getitemnumber(1,"rd_imponib_5") + dw_report.getitemnumber(1,"rd_imponib_6") + &
						 dw_report.getitemnumber(1,"rd_imponib_7") + dw_report.getitemnumber(1,"rd_imponib_8") + &
						 dw_report.getitemnumber(1,"rd_imponib_9") + dw_report.getitemnumber(1,"rd_imponib_10") + &
						 dw_report.getitemnumber(1,"rd_imponib_11") + dw_report.getitemnumber(1,"rd_imponib_12")

	if isnull(ld_tot_1_riga)  then
		dw_report.deleterow(1)
		dw_report.sort()		
	else
		exit
	end if
	
loop

dw_selezione.hide()

parent.x = 10
parent.y = 100
parent.width = 3552
parent.height = 1884

dw_report.show()
cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_fatturato_mesi
integer x = 3109
integer y = 1680
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;
long ll_num_righe

ll_num_righe= dw_report.rowcount()
dw_report.RowsDiscard (1, ll_num_righe, primary!)

dw_report.object.rd_imponib_1_t.text = "Gennaio"
dw_report.object.rd_imponib_2_t.text = "Febbraio"
dw_report.object.rd_imponib_3_t.text = "Marzo"
dw_report.object.rd_imponib_4_t.text = "Aprile"
dw_report.object.rd_imponib_5_t.text = "Maggio"
dw_report.object.rd_imponib_6_t.text = "Giugno"
dw_report.object.rd_imponib_7_t.text = "Luglio"
dw_report.object.rd_imponib_8_t.text = "Agosto"
dw_report.object.rd_imponib_9_t.text = "Settembre"
dw_report.object.rd_imponib_10_t.text = "Ottobre"
dw_report.object.rd_imponib_11_t.text = "Novembre"
dw_report.object.rd_imponib_12_t.text = "Dicembre"



dw_selezione.show()
//cb_cerca_cliente.show()
parent.x = 673
parent.y = 265
parent.width = 1920
parent.height = 470

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_report from uo_cs_xx_dw within w_report_fatturato_mesi
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1620
integer taborder = 10
string dataobject = "d_report_fatturato_mesi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type sle_1 from singlelineedit within w_report_fatturato_mesi
integer x = 23
integer y = 280
integer width = 1001
integer height = 92
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = false
boolean autohscroll = false
end type

type dw_selezione from uo_cs_xx_dw within w_report_fatturato_mesi
integer x = 9
integer y = 16
integer width = 1883
integer height = 248
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_report_fatturato_mesi_sel"
end type

event pcd_new;call super::pcd_new;dw_selezione.setitem(1, "data_riferimento", today())
end event

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = dw_selezione
s_cs_xx.parametri.parametro_s_1 = "cliente"
end event

event buttonclicked;call super::buttonclicked;

choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cliente")
		
end choose
end event

