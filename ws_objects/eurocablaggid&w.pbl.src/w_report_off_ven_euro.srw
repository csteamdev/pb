﻿$PBExportHeader$w_report_off_ven_euro.srw
$PBExportComments$Stampa Offerta Vendita
forward
global type w_report_off_ven_euro from w_cs_xx_principale
end type
type st_1 from statictext within w_report_off_ven_euro
end type
type cb_stampa from commandbutton within w_report_off_ven_euro
end type
type cbx_allegato from checkbox within w_report_off_ven_euro
end type
type dw_1 from uo_cs_xx_dw within w_report_off_ven_euro
end type
type dw_report_off_ven from uo_cs_xx_dw within w_report_off_ven_euro
end type
type dw_allegato_off_ven from uo_cs_xx_dw within w_report_off_ven_euro
end type
end forward

global type w_report_off_ven_euro from w_cs_xx_principale
integer width = 3808
integer height = 4772
string title = "Stampa Offerte Clienti"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
st_1 st_1
cb_stampa cb_stampa
cbx_allegato cbx_allegato
dw_1 dw_1
dw_report_off_ven dw_report_off_ven
dw_allegato_off_ven dw_allegato_off_ven
end type
global w_report_off_ven_euro w_report_off_ven_euro

type variables
long il_anno_registrazione, il_num_registrazione
end variables

forward prototypes
public subroutine wf_report ()
public function integer wf_allegato ()
public function integer wf_imposta_filigrana ()
end prototypes

public subroutine wf_report ();// Inizio --- 11/06/79 ---
// Gestione del numero di telefono, e fax
// Fine --- 11/06/79 ---


string 	ls_stringa, ls_cod_tipo_off_ven, ls_cod_banca_clien_for, ls_cod_cliente, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_ric_cliente, ls_cod_porto, ls_cod_resa, &
		 	ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		 	ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 	ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, &
		 	ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
		 	ls_partita_iva_cli, ls_cod_fiscale_cli, ls_cod_lingua, ls_flag_tipo_cliente, &
		 	ls_des_tipo_off_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, &
  		 	ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , &
		 	ls_des_prodotto_anag, ls_flag_stampa_offerta, ls_cod_tipo_det_ven, &
		 	ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_cod_contatto, &
		 	ls_rag_soc_1_con, ls_rag_soc_2_con, ls_indirizzo_con, ls_localita_con, &
		 	ls_frazione_con, ls_cap_con, ls_provincia_con, ls_partita_iva_con, &
		 	ls_cod_fiscale_con, ls_cod_lingua_con, ls_flag_tipo_cliente_con, &
		 	ls_telefono_cli, ls_fax_cli, ls_telefono_con, ls_fax_con, ls_formato, ls_stato_cli
		 
long     ll_errore

dec{4}   ld_tot_val_offerta, ld_sconto, ld_quan_offerta, ld_prezzo_vendita, ld_sconto_1, &
	      ld_sconto_2, ld_val_riga, ld_sconto_pagamento, ld_tes_off_ven_imponibile_iva, ld_tes_off_ven_importo_iva
			
dec{5}   ld_fat_conversione_ven
			
datetime ld_data_ric_cliente, ld_data_registrazione, ld_data_consegna, ld_data_scadenza


dw_report_off_ven.reset()

select parametri_azienda.stringa  
into  :ls_stringa  
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select cod_tipo_off_ven,   
		 data_registrazione,   
		 data_scadenza,   
		 cod_cliente,   
		 cod_contatto,
		 cod_valuta,   
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 num_ric_cliente,   
		 data_ric_cliente,   
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,   
		 tot_val_offerta,
		 importo_iva,
		 imponibile_iva
into   :ls_cod_tipo_off_ven,   
	 	 :ld_data_registrazione,   
	 	 :ld_data_scadenza,   
	 	 :ls_cod_cliente,   
	 	 :ls_cod_contatto,   
		 :ls_cod_valuta,   
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_num_ric_cliente,   
		 :ld_data_ric_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_tot_val_offerta,
		 :ld_tes_off_ven_importo_iva,
		 :ld_tes_off_ven_imponibile_iva
from   tes_off_ven  
where  tes_off_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_off_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_off_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_off_ven)
	setnull(ld_data_registrazione)
	setnull(ld_data_scadenza)
	setnull(ls_cod_cliente)
	setnull(ls_cod_contatto)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ric_cliente)
	setnull(ld_data_ric_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_val_offerta)
	setnull(ld_tes_off_ven_importo_iva)
	setnull(ld_tes_off_ven_imponibile_iva)
end if

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return
end if	

select		anag_clienti.rag_soc_1,   
			anag_clienti.rag_soc_2,   
			anag_clienti.indirizzo,   
			anag_clienti.localita,   
			anag_clienti.frazione,   
			anag_clienti.cap,   
			anag_clienti.provincia,   
			anag_clienti.partita_iva,   
			anag_clienti.cod_fiscale,   
			anag_clienti.cod_lingua,   
			anag_clienti.flag_tipo_cliente,
			anag_clienti.telefono,
			anag_clienti.fax,
			anag_clienti.stato
into	:ls_rag_soc_1_cli,   
		:ls_rag_soc_2_cli,   
		:ls_indirizzo_cli,   
		:ls_localita_cli,   
		:ls_frazione_cli,   
		:ls_cap_cli,   
		:ls_provincia_cli,   
		:ls_partita_iva_cli,   
		:ls_cod_fiscale_cli,   
		:ls_cod_lingua,   
		:ls_flag_tipo_cliente,
		:ls_telefono_cli,
		:ls_fax_cli,
		:ls_stato_cli
from   anag_clienti  
where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
       anag_clienti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_cli)
	setnull(ls_rag_soc_2_cli)
	setnull(ls_indirizzo_cli)
	setnull(ls_localita_cli)
	setnull(ls_frazione_cli)
	setnull(ls_cap_cli)
	setnull(ls_provincia_cli)
	setnull(ls_partita_iva_cli)
	setnull(ls_cod_fiscale_cli)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_cliente)
	setnull(ls_telefono_cli)
	setnull(ls_fax_cli)
	setnull(ls_stato_cli)
end if

if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if

if ls_flag_tipo_cliente = 'E' then
	ls_partita_iva_cli = ls_cod_fiscale_cli
end if

select anag_contatti.rag_soc_1,   
       anag_contatti.rag_soc_2,   
       anag_contatti.indirizzo,   
       anag_contatti.localita,   
       anag_contatti.frazione,   
       anag_contatti.cap,   
       anag_contatti.provincia,   
       anag_contatti.partita_iva,   
       anag_contatti.cod_fiscale,   
       anag_contatti.cod_lingua,   
       anag_contatti.flag_tipo_cliente,
		 anag_contatti.telefono,
		 anag_contatti.fax
into   :ls_rag_soc_1_con,   
       :ls_rag_soc_2_con,   
       :ls_indirizzo_con,   
       :ls_localita_con,   
       :ls_frazione_con,   
       :ls_cap_con,   
       :ls_provincia_con,   
       :ls_partita_iva_con,   
       :ls_cod_fiscale_con,   
       :ls_cod_lingua_con,   
       :ls_flag_tipo_cliente_con,
		 :ls_telefono_con,
		 :ls_fax_con
from   anag_contatti  
where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and
       anag_contatti.cod_contatto = :ls_cod_contatto;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_con)
	setnull(ls_rag_soc_2_con)
	setnull(ls_indirizzo_con)
	setnull(ls_localita_con)
	setnull(ls_frazione_con)
	setnull(ls_cap_con)
	setnull(ls_provincia_con)
	setnull(ls_partita_iva_con)
	setnull(ls_cod_fiscale_con)
	setnull(ls_cod_lingua_con)
	setnull(ls_flag_tipo_cliente_con)
	setnull(ls_telefono_con)
	setnull(ls_fax_con)
end if

if ls_flag_tipo_cliente_con = 'E' then
	ls_partita_iva_con = ls_cod_fiscale_con
end if

select tab_tipi_off_ven.des_tipo_off_ven  
into   :ls_des_tipo_off_ven  
from   tab_tipi_off_ven  
where  tab_tipi_off_ven.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_off_ven.cod_tipo_off_ven = :ls_cod_tipo_off_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_off_ven)
end if

select tab_pagamenti.des_pagamento,   
       tab_pagamenti.sconto  
into   :ls_des_pagamento,   
       :ld_sconto_pagamento  
from   tab_pagamenti  
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select tab_pagamenti_lingue.des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  tab_pagamenti_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_pagamenti_lingue.cod_pagamento = :ls_cod_pagamento and
       (tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua or
		  tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua_con);

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

select anag_banche_clien_for.des_banca  
into   :ls_des_banca  
from   anag_banche_clien_for  
where  anag_banche_clien_for.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_banche_clien_for.cod_banca_clien_for = :ls_cod_banca_clien_for;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       (tab_porti_lingue.cod_lingua = :ls_cod_lingua or
		  tab_porti_lingue.cod_lingua = :ls_cod_lingua_con);

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       (tab_rese_lingue.cod_lingua = :ls_cod_lingua or 
        tab_rese_lingue.cod_lingua = :ls_cod_lingua_con);

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

declare cu_dettagli cursor for 
	select   det_off_ven.cod_tipo_det_ven, 
				det_off_ven.cod_misura, 
				det_off_ven.quan_offerta, 
				det_off_ven.prezzo_vendita, 
				det_off_ven.sconto_1, 
				det_off_ven.sconto_2, 
				det_off_ven.imponibile_iva_valuta, 
				det_off_ven.data_consegna, 
				det_off_ven.nota_dettaglio, 
				det_off_ven.cod_prodotto, 
				det_off_ven.des_prodotto,
				det_off_ven.fat_conversione_ven
	from     det_off_ven 
	where    det_off_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				det_off_ven.anno_registrazione = :il_anno_registrazione and 
				det_off_ven.num_registrazione = :il_num_registrazione
	order by det_off_ven.cod_azienda, 
				det_off_ven.anno_registrazione, 
				det_off_ven.num_registrazione,
				det_off_ven.prog_riga_off_ven;

open cu_dettagli;

do while 0 = 0
   fetch cu_dettagli into :ls_cod_tipo_det_ven, 
								  :ls_cod_misura, 
								  :ld_quan_offerta, 
								  :ld_prezzo_vendita,   
								  :ld_sconto_1, 
								  :ld_sconto_2, 
								  :ld_val_riga, 
								  :ld_data_consegna, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ld_fat_conversione_ven;

   if sqlca.sqlcode <> 0 then exit

	dw_report_off_ven.insertrow(0)
	dw_report_off_ven.setrow(dw_report_off_ven.rowcount())

	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "parametri_azienda_stringa", ls_stringa)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_anno_registrazione", il_anno_registrazione)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_num_registrazione", il_num_registrazione)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_tipo_off_ven", ls_cod_tipo_off_ven)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_data_registrazione", ld_data_registrazione)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_data_scadenza", ld_data_scadenza)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_cliente", ls_cod_cliente)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_contatto", ls_cod_contatto)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_valuta", ls_cod_valuta)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_pagamento", ls_cod_pagamento)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_sconto", ld_sconto)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_num_ric_cliente", ls_num_ric_cliente)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_data_ric_cliente", ld_data_ric_cliente)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_nota_testata", ls_nota_testata)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_nota_piede", ls_nota_piede)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_porto", ls_cod_porto)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_resa", ls_cod_resa)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_tot_val_offerta", ld_tot_val_offerta)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_imponibile_iva", ld_tes_off_ven_imponibile_iva)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_importo_iva", ld_tes_off_ven_importo_iva)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_localita", ls_localita_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_frazione", ls_frazione_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_cap", ls_cap_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_provincia", ls_provincia_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_partita_iva", ls_partita_iva_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_telefono", ls_telefono_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_fax", ls_fax_cli)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_contatti_rag_soc_1", ls_rag_soc_1_con)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_contatti_rag_soc_2", ls_rag_soc_2_con)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_contatti_indirizzo", ls_indirizzo_con)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_contatti_localita", ls_localita_con)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_contatti_frazione", ls_frazione_con)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_contatti_cap", ls_cap_con)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_contatti_provincia", ls_provincia_con)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_contatti_partita_iva", ls_partita_iva_con)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_contatti_telefono", ls_telefono_con)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_contatti_fax", ls_fax_con)

	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_tipi_off_ven_des_tipo_off_ven", ls_des_tipo_off_ven)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "formato", ls_formato)
	
	select tab_tipi_det_ven.flag_stampa_offerta  
	into   :ls_flag_stampa_offerta  
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode <> 0 then
		setnull(ls_flag_stampa_offerta)
	end if

	if ls_flag_stampa_offerta = 'S' then
		select anag_prodotti.des_prodotto  
		into   :ls_des_prodotto_anag  
		from   anag_prodotti  
		where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode <> 0 then
			setnull(ls_des_prodotto_anag)
		end if

		select anag_prodotti_lingue.des_prodotto  
		into   :ls_des_prodotto_lingua  
		from   anag_prodotti_lingue  
		where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
				 (anag_prodotti_lingue.cod_lingua = :ls_cod_lingua or 
				  anag_prodotti_lingue.cod_lingua = :ls_cod_lingua_con);
		
		if sqlca.sqlcode <> 0 then
			setnull(ls_des_prodotto_lingua)
		end if

		select tab_prod_clienti.cod_prod_cliente  
		into   :ls_cod_prod_cliente  
		from   tab_prod_clienti  
		where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
				 tab_prod_clienti.cod_cliente = :ls_cod_cliente;

		if sqlca.sqlcode <> 0 then
			setnull(ls_cod_prod_cliente)
		end if

		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_cod_misura", ls_cod_misura)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_quan_offerta", ld_quan_offerta * ld_fat_conversione_ven)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_prezzo_vendita", ld_prezzo_vendita / ld_fat_conversione_ven)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_sconto_1", ld_sconto_1)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_sconto_2", ld_sconto_2)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_val_riga", ld_val_riga)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_data_consegna", ld_data_consegna)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_nota_dettaglio", ls_nota_dettaglio)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_cod_prodotto", ls_cod_prodotto)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_des_prodotto", ls_des_prodotto)

		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
		
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
		
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_prod_clienti_cod_prod_cliente", ls_cod_prod_cliente)
	end if
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_porti_des_porto", ls_des_porto)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_rese_des_resa", ls_des_resa)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)

loop
close cu_dettagli;
dw_report_off_ven.reset_dw_modified(c_resetchildren)
end subroutine

public function integer wf_allegato ();long     ll_riga, ll_pos, ll_count

datetime ldt_data_ric_cliente, ldt_data_registrazione

string 	ls_num_ric_cliente, ls_flag_tipo_cliente, ls_flag_tipo_contatto, &
			ls_cod_cliente, ls_rag_soc_1_cli, ls_rag_soc_2_cli, ls_partita_iva_cli, ls_cod_fiscale_cli, &
			ls_cod_contatto, ls_rag_soc_1_con, ls_rag_soc_2_con, ls_partita_iva_con, ls_cod_fiscale_con, &
			ls_cliente_telefono, ls_cliente_fax, ls_contatto_telefono, ls_contatto_fax, &
			ls_testo_1, ls_testo_2, ls_testo_3, ls_testo_4, ls_stringa, ls_riga


dw_allegato_off_ven.reset()

select data_registrazione,
		 cod_cliente,
		 cod_contatto,
		 num_ric_cliente,
		 data_ric_cliente
into   :ldt_data_registrazione,
	 	 :ls_cod_cliente,
	 	 :ls_cod_contatto,
		 :ls_num_ric_cliente,
		 :ldt_data_ric_cliente
from   tes_off_ven
where  tes_off_ven.cod_azienda = :s_cs_xx.cod_azienda and
		 tes_off_ven.anno_registrazione = :il_anno_registrazione and
		 tes_off_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ldt_data_registrazione)
	setnull(ls_cod_cliente)
	setnull(ls_cod_contatto)
	setnull(ls_num_ric_cliente)
	setnull(ldt_data_ric_cliente)
end if

select rag_soc_1,
       rag_soc_2,
       partita_iva,
       cod_fiscale,
       flag_tipo_cliente,
		 telefono,
		 fax
into   :ls_rag_soc_1_cli,
       :ls_rag_soc_2_cli,
       :ls_partita_iva_cli,
       :ls_cod_fiscale_cli,
       :ls_flag_tipo_cliente,
		 :ls_cliente_telefono,
		 :ls_cliente_fax
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_cli)
	setnull(ls_rag_soc_2_cli)
	setnull(ls_partita_iva_cli)
	setnull(ls_cod_fiscale_cli)
	setnull(ls_flag_tipo_cliente)
	setnull(ls_cliente_telefono)
	setnull(ls_cliente_fax)
end if

if ls_flag_tipo_cliente = 'E' then
	ls_partita_iva_cli = ls_cod_fiscale_cli
end if

select rag_soc_1,
       rag_soc_2,
       partita_iva,
       cod_fiscale,
       flag_tipo_cliente,
		 telefono,
		 fax
into   :ls_rag_soc_1_con,
       :ls_rag_soc_2_con,
       :ls_partita_iva_con,
       :ls_cod_fiscale_con,
       :ls_flag_tipo_contatto,
		 :ls_contatto_telefono,
		 :ls_contatto_fax
from   anag_contatti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_contatto = :ls_cod_contatto;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_con)
	setnull(ls_rag_soc_2_con)
	setnull(ls_partita_iva_con)
	setnull(ls_cod_fiscale_con)
	setnull(ls_flag_tipo_contatto)
	setnull(ls_contatto_telefono)
	setnull(ls_contatto_fax)
end if

if ls_flag_tipo_contatto = 'E' then
	ls_partita_iva_con = ls_cod_fiscale_con
end if

select testo_1,
		 testo_2,
		 testo_3,
		 testo_4
into	 :ls_testo_1,
		 :ls_testo_2,
		 :ls_testo_3,
		 :ls_testo_4
from	 allegato_doc_ven
where	 cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura testo allegato: " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 100 then
	return -1
end if

ll_count = 0

ll_pos = 1

do while true
	
	if isnull(ls_stringa) or ls_stringa = "" then
		
		ll_count ++
		
		choose case ll_count
			case 1
				ls_stringa = ls_testo_1
			case 2
				ls_stringa = ls_testo_2
			case 3
				ls_stringa = ls_testo_3
			case 4
				ls_stringa = ls_testo_4
			case else
				exit
		end choose
		
	end if
	
	ll_pos = pos(ls_stringa,char(13),1)
	
	if not isnull(ll_pos) and ll_pos > 0 then
		ls_riga = left(ls_stringa,ll_pos - 1)
		ls_stringa = right(ls_stringa,len(ls_stringa) - ll_pos - 1)
	else
		ls_riga = ls_stringa
		ls_stringa = ""
	end if
	
	ll_riga = dw_allegato_off_ven.insertrow(0)
	dw_allegato_off_ven.setrow(ll_riga)
	
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "tes_off_ven_anno_registrazione", il_anno_registrazione)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "tes_off_ven_num_registrazione", il_num_registrazione)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "tes_off_ven_data_registrazione", ldt_data_registrazione)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "tes_off_ven_cod_cliente", ls_cod_cliente)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "tes_off_ven_cod_contatto", ls_cod_contatto)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "tes_off_ven_num_ric_cliente", ls_num_ric_cliente)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "tes_off_ven_data_ric_cliente", ldt_data_ric_cliente)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "anag_clienti_partita_iva", ls_partita_iva_cli)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "anag_clienti_telefono", ls_cliente_telefono)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "anag_clienti_fax", ls_cliente_fax)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "anag_contatti_rag_soc_1", ls_rag_soc_1_con)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "anag_contatti_rag_soc_2", ls_rag_soc_2_con)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "anag_contatti_partita_iva", ls_partita_iva_con)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "anag_contatti_telefono", ls_contatto_telefono)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "anag_contatti_fax", ls_contatto_fax)
	dw_allegato_off_ven.setitem(dw_allegato_off_ven.getrow(), "testo", ls_riga)
	
loop

dw_allegato_off_ven.resetupdate()
dw_allegato_off_ven.change_dw_current()

return 0
end function

public function integer wf_imposta_filigrana ();string ls_path, ls_error

guo_functions.uof_get_parametro_azienda("FL1", ls_path)

if isnull(ls_path) or ls_path = "" then return -1

if guo_functions.uof_filigrana(s_cs_xx.volume + ls_path, dw_report_off_ven, true, ls_error) < 0 then
	g_mb.error(ls_error)
	return -1
else
	return 0
end if
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_path_logo_2, ls_modify

dw_report_off_ven.ib_dw_report = true
dw_allegato_off_ven.ib_dw_report = true
dw_1.ib_dw_report = true

set_w_options(c_noresizewin)

il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione = s_cs_xx.parametri.parametro_d_2

dw_report_off_ven.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
											
pcca.window_currentdw = dw_report_off_ven
												
select parametri_azienda.stringa
into   :ls_path_logo_1
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO1';

select parametri_azienda.stringa
into   :ls_path_logo_2
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO2';

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
dw_report_off_ven.modify(ls_modify)
dw_allegato_off_ven.modify(ls_modify)

ls_modify = "piede.filename='" + s_cs_xx.volume + ls_path_logo_2 + "'~t"
dw_report_off_ven.modify(ls_modify)
dw_allegato_off_ven.modify(ls_modify)

// stefanop 24/03/2014
wf_imposta_filigrana()

// --------------  FIRME ELETTRONICHE SU DOCUMENTI -------------------------------------------------
//                      AGGIUNTO IL 14/11/2002
string ls_cod_oggetto, ls_path_firma

select stringa
into   :ls_cod_oggetto
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
       flag_parametro = 'S' and &
       cod_parametro = 'FR1';

if sqlca.sqlcode = 0 then
	select path_oggetto
	into   :ls_path_firma
	from   utenti_oggetti
	where  cod_azienda = :s_cs_xx.cod_azienda and
   	    cod_utente = :s_cs_xx.cod_utente and 
      	 cod_oggetto = :ls_cod_oggetto;

	ls_modify = "firma.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_path_firma + "'~t"
	dw_report_off_ven.modify(ls_modify)
end if


end event

on w_report_off_ven_euro.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_stampa=create cb_stampa
this.cbx_allegato=create cbx_allegato
this.dw_1=create dw_1
this.dw_report_off_ven=create dw_report_off_ven
this.dw_allegato_off_ven=create dw_allegato_off_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_stampa
this.Control[iCurrent+3]=this.cbx_allegato
this.Control[iCurrent+4]=this.dw_1
this.Control[iCurrent+5]=this.dw_report_off_ven
this.Control[iCurrent+6]=this.dw_allegato_off_ven
end on

on w_report_off_ven_euro.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_stampa)
destroy(this.cbx_allegato)
destroy(this.dw_1)
destroy(this.dw_report_off_ven)
destroy(this.dw_allegato_off_ven)
end on

event pc_print;return
end event

type st_1 from statictext within w_report_off_ven_euro
integer x = 2373
integer y = 28
integer width = 914
integer height = 128
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Per stampare l~'offerta di vendita usare questo pulsante >>>"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_stampa from commandbutton within w_report_off_ven_euro
integer x = 3328
integer y = 52
integer width = 352
integer height = 76
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA"
end type

event clicked;// In questo evento il flag EXTEND ANCESTOR deve essere DISATTIVATO

string ls_cod_tipo_off_ven, ls_cod_vettore, ls_cod_cliente, ls_flag_tipo_cliente
long ll_num_copie, ll_num_copie_cee, ll_num_copie_extra_cee, ll_num_stampe, ll_i

if cbx_allegato.checked then
	
	///// modifica Michela 16/11/2005: in questo modo la stampa viene su un unico documento (vedi fax epannelli del 15/11/2005)
	/////                              se si vuole l'allegato procedo con la stampa della dw unificata, altrimenti proseguo
	/////                              normalmente
	/////dw_allegato_ord_ven.print()
	DataWindowChild f1, f2 
	integer rtncode 
	
	dw_1.reset()
	dw_1.dataobject = "d_composta_2"
	dw_1.settransobject(sqlca)
	
	dw_1.insertRow(0) 
	
	rtncode = dw_1.GetChild('dw_1', f1) 
	IF rtncode = -1 THEN g_mb.messagebox( "Error", "Not a DataWindowChild") 
	f1.SetTransObject(SQLCA) 
	
	rtncode = dw_1.GetChild('dw_2', f2) 
	IF rtncode = -1 THEN g_mb.messagebox( "Error", "Not a DataWindowChild") 
	f2.SetTransObject(SQLCA) 
	
	dw_allegato_off_ven.ROWScopy( 1, dw_allegato_off_ven.ROWCOUNT(), Primary!, F1, 1, Primary!)
	dw_report_off_ven.ROWScopy( 1, dw_report_off_ven.ROWCOUNT(), Primary!, F2, 1, Primary!)		
	dw_1.print()
else
	dw_report_off_ven.Print()
end if
end event

type cbx_allegato from checkbox within w_report_off_ven_euro
integer x = 46
integer y = 56
integer width = 1883
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Includi allegato ai documenti di vendita in fase di stampa dell~'offerta"
end type

type dw_1 from uo_cs_xx_dw within w_report_off_ven_euro
boolean visible = false
integer x = 3840
integer y = 420
integer width = 2491
integer height = 960
integer taborder = 30
string dataobject = "d_composta_2"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

type dw_report_off_ven from uo_cs_xx_dw within w_report_off_ven_euro
integer x = 23
integer y = 176
integer width = 3657
integer height = 4376
integer taborder = 20
string dataobject = "d_report_off_ven_euro"
boolean livescroll = true
end type

event pcd_first;call super::pcd_first;wf_report()
wf_allegato()
end event

event pcd_last;call super::pcd_last;wf_report()
wf_allegato()
end event

event pcd_next;call super::pcd_next;wf_report()
wf_allegato()
end event

event pcd_previous;call super::pcd_previous;wf_report()
end event

event pcd_retrieve;call super::pcd_retrieve;wf_report()

wf_allegato()
end event

type dw_allegato_off_ven from uo_cs_xx_dw within w_report_off_ven_euro
boolean visible = false
integer x = 23
integer y = 176
integer width = 3657
integer height = 4380
integer taborder = 30
string dataobject = "d_allegato_off_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

