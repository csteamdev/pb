﻿$PBExportHeader$w_report_ord_acq_euro.srw
$PBExportComments$Report Ordine Acquisto
forward
global type w_report_ord_acq_euro from w_cs_xx_principale
end type
type dw_report_ord_acq from uo_cs_xx_dw within w_report_ord_acq_euro
end type
end forward

global type w_report_ord_acq_euro from w_cs_xx_principale
integer width = 3872
integer height = 4772
string title = "Stampa Ordini Acquisto"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
dw_report_ord_acq dw_report_ord_acq
end type
global w_report_ord_acq_euro w_report_ord_acq_euro

type variables
boolean ib_modifica=false, ib_nuovo=false,ib_stampa_residuo
long il_anno_registrazione, il_num_registrazione
end variables

forward prototypes
public subroutine wf_report ()
public function integer wf_imposta_filigrana ()
end prototypes

public subroutine wf_report ();// Inizio --- 1/09/79 ---
// Gestione del numero di telefono, e fax
// Fine --- 1/09/79 ---

string 	ls_stringa, ls_cod_tipo_ord_acq, ls_cod_banca_clien_for, ls_cod_fornitore, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_ord_fornitore, ls_cod_porto, ls_cod_resa, &
		 	ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		 	ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 	ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_for, ls_rag_soc_2_for, &
		 	ls_indirizzo_for, ls_localita_for, ls_frazione_for, ls_cap_for, ls_provincia_for, &
		 	ls_partita_iva_for, ls_cod_fiscale_for, ls_cod_lingua, ls_flag_tipo_fornitore, &
		 	ls_des_tipo_ord_acq, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, &
  		 	ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , &
		 	ls_des_prodotto_anag, ls_flag_stampa_ordine, ls_cod_tipo_det_acq, &
		 	ls_des_prodotto_lingua, ls_cod_prod_fornitore, ls_cod_fil_fornitore, &
		 	ls_rag_soc_1_fil, ls_rag_soc_2_fil, ls_indirizzo_fil, &
		 	ls_localita_fil, ls_frazione_fil, ls_cap_fil, ls_provincia_fil, &
		 	ls_telefono_for, ls_fax_for, ls_telefono_fil_for, ls_fax_fil_for, ls_formato
		 
long   	ll_errore

dec{4}   ld_tot_val_ordine, ld_sconto, ld_quan_ordinata, ld_prezzo_acquisto, ld_sconto_1, &
	      ld_sconto_2, ld_val_riga, ld_sconto_pagamento
			
dec{5}   ld_fat_conversione
		 
datetime ld_data_ord_fornitore, ld_data_registrazione, ld_data_consegna

dw_report_ord_acq.reset()

//ll_anno_registrazione = dw_report_ord_acq.i_parentdw.getitemnumber(dw_report_ord_acq.i_parentdw.i_selectedrows[1], "anno_registrazione")
//ll_num_registrazione = dw_report_ord_acq.i_parentdw.getitemnumber(dw_report_ord_acq.i_parentdw.i_selectedrows[1], "num_registrazione")

select parametri_azienda.stringa  
into  :ls_stringa  
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select tes_ord_acq.cod_tipo_ord_acq,   
		 tes_ord_acq.data_registrazione,   
		 tes_ord_acq.cod_fornitore,   
		 tes_ord_acq.cod_fil_fornitore,
		 tes_ord_acq.cod_valuta,   
		 tes_ord_acq.cod_pagamento,   
		 tes_ord_acq.sconto,   
		 tes_ord_acq.cod_banca_clien_for,   
		 tes_ord_acq.num_ord_fornitore,   
		 tes_ord_acq.data_ord_fornitore,   
		 tes_ord_acq.cod_porto,   
		 tes_ord_acq.cod_resa,   
		 tes_ord_acq.nota_testata,   
		 tes_ord_acq.nota_piede,   
		 tes_ord_acq.tot_val_ordine,
		 tes_ord_acq.rag_soc_1,
		 tes_ord_acq.rag_soc_2,
		 tes_ord_acq.indirizzo,
		 tes_ord_acq.localita,
		 tes_ord_acq.frazione,
		 tes_ord_acq.cap,
		 tes_ord_acq.provincia
into   :ls_cod_tipo_ord_acq,   
	 	 :ld_data_registrazione,   
	 	 :ls_cod_fornitore,   
		 :ls_cod_fil_fornitore,
		 :ls_cod_valuta,   
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_num_ord_fornitore,   
		 :ld_data_ord_fornitore,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_tot_val_ordine,
		 :ls_rag_soc_1,
		 :ls_rag_soc_2,
		 :ls_indirizzo,
		 :ls_localita,
		 :ls_frazione,
		 :ls_cap,
		 :ls_provincia
from   tes_ord_acq  
where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_ord_acq.anno_registrazione = :il_anno_registrazione and 
		 tes_ord_acq.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_ord_acq)
	setnull(ld_data_registrazione)
	setnull(ls_cod_fornitore)
	setnull(ls_cod_fil_fornitore)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ord_fornitore)
	setnull(ld_data_ord_fornitore)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_val_ordine)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
end if

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return
end if	

select anag_fornitori.rag_soc_1,   
       anag_fornitori.rag_soc_2,   
       anag_fornitori.indirizzo,   
       anag_fornitori.localita,   
       anag_fornitori.frazione,   
       anag_fornitori.cap,   
       anag_fornitori.provincia,   
       anag_fornitori.partita_iva,   
       anag_fornitori.cod_fiscale,   
       anag_fornitori.cod_lingua,   
       anag_fornitori.flag_tipo_fornitore,
		 anag_fornitori.telefono,
		 anag_fornitori.fax
into   :ls_rag_soc_1_for,   
       :ls_rag_soc_2_for,   
       :ls_indirizzo_for,   
       :ls_localita_for,   
       :ls_frazione_for,   
       :ls_cap_for,   
       :ls_provincia_for,   
       :ls_partita_iva_for,   
       :ls_cod_fiscale_for,   
       :ls_cod_lingua,   
       :ls_flag_tipo_fornitore,
		 :ls_telefono_for,
		 :ls_fax_for
from   anag_fornitori  
where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
       anag_fornitori.cod_fornitore = :ls_cod_fornitore;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_for)
	setnull(ls_rag_soc_2_for)
	setnull(ls_indirizzo_for)
	setnull(ls_localita_for)
	setnull(ls_frazione_for)
	setnull(ls_cap_for)
	setnull(ls_provincia_for)
	setnull(ls_partita_iva_for)
	setnull(ls_cod_fiscale_for)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_fornitore)
	setnull(ls_telefono_for)
	setnull(ls_fax_for)
end if

if ls_flag_tipo_fornitore = 'E' then
	ls_partita_iva_for = ls_cod_fiscale_for
end if

select anag_fil_fornitori.rag_soc_1,   
       anag_fil_fornitori.rag_soc_2,   
       anag_fil_fornitori.indirizzo,   
       anag_fil_fornitori.localita,   
       anag_fil_fornitori.frazione,   
       anag_fil_fornitori.cap,   
       anag_fil_fornitori.provincia,
		 anag_fil_fornitori.telefono,
		 anag_fil_fornitori.fax
into   :ls_rag_soc_1_fil,   
       :ls_rag_soc_2_fil,   
       :ls_indirizzo_fil,   
       :ls_localita_fil,   
       :ls_frazione_fil,   
       :ls_cap_fil,   
       :ls_provincia_fil,
		 :ls_telefono_fil_for,
		 :ls_fax_fil_for
from   anag_fil_fornitori
where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
       anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and
		 anag_fil_fornitori.cod_fil_fornitore = :ls_cod_fil_fornitore;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_fil)
	setnull(ls_rag_soc_2_fil)
	setnull(ls_indirizzo_fil)
	setnull(ls_localita_fil)
	setnull(ls_frazione_fil)
	setnull(ls_cap_fil)
	setnull(ls_provincia_fil)
	setnull(ls_telefono_fil_for)
	setnull(ls_fax_fil_for)
end if

select tab_tipi_ord_acq.des_tipo_ord_acq  
into   :ls_des_tipo_ord_acq  
from   tab_tipi_ord_acq  
where  tab_tipi_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_ord_acq.cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_ord_acq)
end if

select tab_pagamenti.des_pagamento,   
       tab_pagamenti.sconto  
into   :ls_des_pagamento,   
       :ld_sconto_pagamento  
from   tab_pagamenti  
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select tab_pagamenti_lingue.des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  tab_pagamenti_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_pagamenti_lingue.cod_pagamento = :ls_cod_pagamento and
       tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

select anag_banche_clien_for.des_banca  
into   :ls_des_banca  
from   anag_banche_clien_for  
where  anag_banche_clien_for.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_banche_clien_for.cod_banca_clien_for = :ls_cod_banca_clien_for;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

declare cu_dettagli cursor for 
	select   det_ord_acq.cod_tipo_det_acq, 
				det_ord_acq.cod_misura, 
				det_ord_acq.quan_ordinata, 
				det_ord_acq.prezzo_acquisto, 
				det_ord_acq.sconto_1, 
				det_ord_acq.sconto_2, 
				det_ord_acq.imponibile_iva_valuta, 
				det_ord_acq.data_consegna, 
				det_ord_acq.nota_dettaglio, 
				det_ord_acq.cod_prodotto, 
				det_ord_acq.des_prodotto,
				det_ord_acq.cod_prod_fornitore,
				det_ord_acq.fat_conversione
	from     det_ord_acq 
	where    det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and 
				det_ord_acq.anno_registrazione = :il_anno_registrazione and 
				det_ord_acq.num_registrazione = :il_num_registrazione
	order by det_ord_acq.cod_azienda, 
				det_ord_acq.anno_registrazione, 
				det_ord_acq.num_registrazione,
				det_ord_acq.prog_riga_ordine_acq;

open cu_dettagli;

do while 0 = 0
   fetch cu_dettagli into :ls_cod_tipo_det_acq, 
								  :ls_cod_misura, 
								  :ld_quan_ordinata, 
								  :ld_prezzo_acquisto,   
								  :ld_sconto_1, 
								  :ld_sconto_2, 
								  :ld_val_riga, 
								  :ld_data_consegna, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ls_cod_prod_fornitore,
								  :ld_fat_conversione;

   if sqlca.sqlcode <> 0 then exit

	dw_report_ord_acq.insertrow(0)
	dw_report_ord_acq.setrow(dw_report_ord_acq.rowcount())

	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "parametri_azienda_stringa", ls_stringa)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_anno_registrazione", il_anno_registrazione)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_num_registrazione", il_num_registrazione)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_tipo_ord_acq", ls_cod_tipo_ord_acq)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_data_registrazione", ld_data_registrazione)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_fornitore", ls_cod_fornitore)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_fil_fornitore", ls_cod_fil_fornitore)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_valuta", ls_cod_valuta)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_pagamento", ls_cod_pagamento)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_sconto", ld_sconto)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_banca_clien_for", ls_cod_banca_clien_for)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_num_ord_fornitore", ls_num_ord_fornitore)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_data_ord_fornitore", ld_data_ord_fornitore)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_nota_testata", ls_nota_testata)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_nota_piede", ls_nota_piede)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_porto", ls_cod_porto)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cod_resa", ls_cod_resa)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_tot_val_ordine", ld_tot_val_ordine)	
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_rag_soc_1", ls_rag_soc_1)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_rag_soc_2", ls_rag_soc_2)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_indirizzo", ls_indirizzo)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_localita", ls_localita)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_frazione", ls_frazione)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_cap", ls_cap)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tes_ord_acq_provincia", ls_provincia)

	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_rag_soc_1", ls_rag_soc_1_for)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_rag_soc_2", ls_rag_soc_2_for)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_indirizzo", ls_indirizzo_for)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_localita", ls_localita_for)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_frazione", ls_frazione_for)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_cap", ls_cap_for)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_provincia", ls_provincia_for)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_partita_iva", ls_partita_iva_for)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_telefono", ls_telefono_for)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fornitori_fax", ls_fax_for)
	
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_rag_soc_1", ls_rag_soc_1_fil)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_rag_soc_2", ls_rag_soc_2_fil)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_indirizzo", ls_indirizzo_fil)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_localita", ls_localita_fil)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_frazione", ls_frazione_fil)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_cap", ls_cap_fil)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_provincia", ls_provincia_fil)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_telefono", ls_telefono_fil_for)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_fil_fornitori_fax", ls_fax_fil_for)

	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_tipi_ord_acq_des_tipo_ord_acq", ls_des_tipo_ord_acq)
	
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
	
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
	
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
	
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "formato", ls_formato)

	select tab_tipi_det_acq.flag_stampa_ordine
	into   :ls_flag_stampa_ordine  
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode <> 0 then
		setnull(ls_flag_stampa_ordine)
	end if

	if ls_flag_stampa_ordine = 'S' then
		select anag_prodotti.des_prodotto  
		into   :ls_des_prodotto_anag  
		from   anag_prodotti  
		where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode <> 0 then
			setnull(ls_des_prodotto_anag)
		end if

		select anag_prodotti_lingue.des_prodotto  
		into   :ls_des_prodotto_lingua  
		from   anag_prodotti_lingue  
		where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
				 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
		
		if sqlca.sqlcode <> 0 then
			setnull(ls_des_prodotto_lingua)
		end if

		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_cod_misura", ls_cod_misura)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_quan_ordinata", ld_quan_ordinata * ld_fat_conversione)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_prezzo_acquisto", ld_prezzo_acquisto / ld_fat_conversione)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_sconto_1", ld_sconto_1)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_sconto_2", ld_sconto_2)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_val_riga", ld_val_riga)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_data_consegna", ld_data_consegna)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_nota_dettaglio", ls_nota_dettaglio)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_cod_prodotto", ls_cod_prodotto)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_des_prodotto", ls_des_prodotto)
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "det_ord_acq_cod_prod_fornitore", ls_cod_prod_fornitore)

		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
		
		dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
	end if
	
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_porti_des_porto", ls_des_porto)
	
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
	
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_rese_des_resa", ls_des_resa)
	
	dw_report_ord_acq.setitem(dw_report_ord_acq.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)

loop
close cu_dettagli;
dw_report_ord_acq.reset_dw_modified(c_resetchildren)
dw_report_ord_acq.change_dw_current()
end subroutine

public function integer wf_imposta_filigrana ();string ls_path, ls_error

guo_functions.uof_get_parametro_azienda("FL1", ls_path)

if isnull(ls_path) or ls_path = "" then return -1

if guo_functions.uof_filigrana(s_cs_xx.volume + ls_path, dw_report_ord_acq, true, ls_error) < 0 then
	g_mb.error(ls_error)
	return -1
else
	return 0
end if
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_path_logo_2, ls_modify
s_report_ordine ls_report_ordine

dw_report_ord_acq.ib_dw_report = true

ls_report_ordine = message.powerobjectparm
ib_stampa_residuo = ls_report_ordine.ab_flag_residuo
il_anno_registrazione = ls_report_ordine.al_anno_registrazione
il_num_registrazione = ls_report_ordine.al_num_registrazione

set_w_options(c_noresizewin)

dw_report_ord_acq.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_disablecc, &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
												
select parametri_azienda.stringa
into   :ls_path_logo_1
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO1';

select parametri_azienda.stringa
into   :ls_path_logo_2
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO2';

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
dw_report_ord_acq.modify(ls_modify)

ls_modify = "piede.filename='" + s_cs_xx.volume + ls_path_logo_2 + "'~t"

dw_report_ord_acq.modify(ls_modify)

// stefanop: 24/03/2014
wf_imposta_filigrana()

// --------------  FIRME ELETTRONICHE SU DOCUMENTI -------------------------------------------------
//                      AGGIUNTO IL 14/11/2002
string ls_cod_oggetto, ls_path_firma

select stringa
into   :ls_cod_oggetto
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
       flag_parametro = 'S' and &
       cod_parametro = 'FR1';

if sqlca.sqlcode = 0 then
	select path_oggetto
	into   :ls_path_firma
	from   utenti_oggetti
	where  cod_azienda = :s_cs_xx.cod_azienda and
   	    cod_utente = :s_cs_xx.cod_utente and 
      	 cod_oggetto = :ls_cod_oggetto;

	ls_modify = "firma.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_path_firma + "'~t"
	dw_report_ord_acq.modify(ls_modify)
end if


end event

on w_report_ord_acq_euro.create
int iCurrent
call super::create
this.dw_report_ord_acq=create dw_report_ord_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_ord_acq
end on

on w_report_ord_acq_euro.destroy
call super::destroy
destroy(this.dw_report_ord_acq)
end on

type dw_report_ord_acq from uo_cs_xx_dw within w_report_ord_acq_euro
integer x = 23
integer y = 20
integer width = 3657
integer height = 4520
integer taborder = 20
string dataobject = "d_report_ord_acq_euro"
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

event pcd_first;call super::pcd_first;wf_report()
end event

event pcd_last;call super::pcd_last;wf_report()
end event

event pcd_next;call super::pcd_next;wf_report()
end event

event pcd_previous;call super::pcd_previous;wf_report()
end event

