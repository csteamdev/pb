﻿$PBExportHeader$w_ricerca_eurocablaggi_response.srw
forward
global type w_ricerca_eurocablaggi_response from w_ricerca_response
end type
type cb_report_distinta_base from commandbutton within w_ricerca_eurocablaggi_response
end type
end forward

global type w_ricerca_eurocablaggi_response from w_ricerca_response
cb_report_distinta_base cb_report_distinta_base
end type
global w_ricerca_eurocablaggi_response w_ricerca_eurocablaggi_response

on w_ricerca_eurocablaggi_response.create
int iCurrent
call super::create
this.cb_report_distinta_base=create cb_report_distinta_base
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_report_distinta_base
end on

on w_ricerca_eurocablaggi_response.destroy
call super::destroy
destroy(this.cb_report_distinta_base)
end on

event open;call super::open;

if istr_data.table_name = "anag_prodotti" then cb_report_distinta_base.visible = true
end event

event resize;call super::resize;
cb_report_distinta_base.move(newwidth - 40 - cbx_salva_ricerca.width - cb_report_distinta_base.width - 40, 30)
end event

type cbx_salva_ricerca from w_ricerca_response`cbx_salva_ricerca within w_ricerca_eurocablaggi_response
end type

type tab_1 from w_ricerca_response`tab_1 within w_ricerca_eurocablaggi_response
end type

type ricerca from w_ricerca_response`ricerca within tab_1
end type

type dw_ricerca from w_ricerca_response`dw_ricerca within ricerca
end type

type dw_selezione from w_ricerca_response`dw_selezione within ricerca
end type

type personalizza from w_ricerca_response`personalizza within tab_1
end type

type dw_colonne from w_ricerca_response`dw_colonne within personalizza
end type

type cb_report_distinta_base from commandbutton within w_ricerca_eurocablaggi_response
boolean visible = false
integer x = 2075
integer y = 20
integer width = 562
integer height = 92
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report Dist. Base"
end type

event clicked;long ll_row, ll_count
string ls_cod_prodotto, ls_cod_versione
window lw_window, lw_parent

ll_row = tab_1.ricerca.dw_selezione.getrow()

if ll_row>0 then
else
	g_mb.warning("Selezionare un codice prodotto!")
	return
end if

ls_cod_prodotto = tab_1.ricerca.dw_selezione.getitemstring(ll_row, "cod_prodotto")

if ls_cod_prodotto="" or isnull(ls_cod_prodotto) then
	g_mb.warning("Selezionare un codice prodotto!")
	return
end if

select count(*)
into :ll_count
from distinta_padri
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_prodotto=:ls_cod_prodotto;

if isnull(ll_count) or ll_count=0 then
	g_mb.warning("Il codice prodotto "+ls_cod_prodotto+" non sembra avere nessuna versione in tabella distinta base!")
	return
end if

choose case ll_count
	case is > 1 		
		g_mb.show("Il codice prodotto "+ls_cod_prodotto+" ha più versioni in tabella distinta base, Verrà mostrato il report relativo alla versione più alta!")
		
		select max(cod_versione)
		into :ls_cod_versione
		from distinta_padri
		where cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:ls_cod_prodotto;
		
	case 1
		select cod_versione
		into :ls_cod_versione
		from distinta_padri
		where cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:ls_cod_prodotto;
		
end choose

s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
s_cs_xx.parametri.parametro_s_2 = ls_cod_versione
s_cs_xx.parametri.parametro_d_10 = 999.999


lw_parent = parent
open(lw_window, "w_report_distinta", lw_parent)

end event

