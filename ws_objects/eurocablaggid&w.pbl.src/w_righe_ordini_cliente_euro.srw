﻿$PBExportHeader$w_righe_ordini_cliente_euro.srw
$PBExportComments$Report righe da ordini cliente
forward
global type w_righe_ordini_cliente_euro from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_righe_ordini_cliente_euro
end type
type cb_stampa from commandbutton within w_righe_ordini_cliente_euro
end type
type cb_default from commandbutton within w_righe_ordini_cliente_euro
end type
type cb_esci from commandbutton within w_righe_ordini_cliente_euro
end type
type dw_report from uo_cs_xx_dw within w_righe_ordini_cliente_euro
end type
type cb_1 from commandbutton within w_righe_ordini_cliente_euro
end type
type dw_selezione from uo_cs_xx_dw within w_righe_ordini_cliente_euro
end type
type cb_reset from commandbutton within w_righe_ordini_cliente_euro
end type
end forward

global type w_righe_ordini_cliente_euro from w_cs_xx_principale
integer x = 5
integer y = 4
integer width = 5554
integer height = 3172
string title = "Report Righe Ordini Clienti"
boolean maxbox = false
boolean resizable = false
cb_2 cb_2
cb_stampa cb_stampa
cb_default cb_default
cb_esci cb_esci
dw_report dw_report
cb_1 cb_1
dw_selezione dw_selezione
cb_reset cb_reset
end type
global w_righe_ordini_cliente_euro w_righe_ordini_cliente_euro

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

save_on_close(c_socnosave)

iuo_dw_main = dw_report

dw_report.ib_dw_report=true

dw_report.object.datawindow.print.preview = "Yes"
dw_report.object.datawindow.print.preview.rulers = "Yes"

cb_default.postevent("clicked")



end event

on w_righe_ordini_cliente_euro.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_stampa=create cb_stampa
this.cb_default=create cb_default
this.cb_esci=create cb_esci
this.dw_report=create dw_report
this.cb_1=create cb_1
this.dw_selezione=create dw_selezione
this.cb_reset=create cb_reset
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_stampa
this.Control[iCurrent+3]=this.cb_default
this.Control[iCurrent+4]=this.cb_esci
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.cb_1
this.Control[iCurrent+7]=this.dw_selezione
this.Control[iCurrent+8]=this.cb_reset
end on

on w_righe_ordini_cliente_euro.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_stampa)
destroy(this.cb_default)
destroy(this.cb_esci)
destroy(this.dw_report)
destroy(this.cb_1)
destroy(this.dw_selezione)
destroy(this.cb_reset)
end on

type cb_2 from commandbutton within w_righe_ordini_cliente_euro
integer x = 5234
integer y = 400
integer width = 302
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Anteprima"
end type

event clicked;dw_report.triggerevent("ue_anteprima")
end event

type cb_stampa from commandbutton within w_righe_ordini_cliente_euro
integer x = 4937
integer y = 400
integer width = 297
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;dw_report.print()
end event

type cb_default from commandbutton within w_righe_ordini_cliente_euro
integer x = 5234
integer y = 220
integer width = 297
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Default"
end type

event clicked;string ls_null


setnull(ls_null)

dw_selezione.setitem(dw_selezione.getrow(), "cod_cliente", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_destinazione", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_prodotto", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "d_data_da",relativedate(today(),-60))
dw_selezione.setitem(dw_selezione.getrow(), "d_data_a",relativedate(today(),180))
dw_selezione.setitem(dw_selezione.getrow(), "n_anno",f_anno_esercizio())
dw_selezione.setitem(dw_selezione.getrow(), "n_ord_da", 1)
dw_selezione.setitem(dw_selezione.getrow(), "n_ord_a", 999999)
dw_selezione.setitem(dw_selezione.getrow(), "flag_evaso", "I")
dw_selezione.setitem(dw_selezione.getrow(), "flag_blocco", "N")
dw_selezione.setitem(dw_selezione.getrow(), "flag_residuo", "N")
dw_selezione.resetupdate()
end event

type cb_esci from commandbutton within w_righe_ordini_cliente_euro
integer x = 5234
integer y = 20
integer width = 297
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esci"
end type

event clicked;close(parent)
end event

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type dw_report from uo_cs_xx_dw within w_righe_ordini_cliente_euro
integer x = 23
integer y = 500
integer width = 5509
integer height = 2560
integer taborder = 30
string dataobject = "d_righe_ordini_clienti_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_cliente, ls_flag_evaso, ls_flag_evasione_1, ls_flag_evasione_2, ls_flag_evasione_3, &
		 ls_flag_blocco, ls_cod_prodotto, ls_cod_destinazione, ls_flag_residuo, ls_modify

long   ll_ordine_inizio, ll_ordine_fine, ll_errore, ll_anno_ordine

date   ld_data_inizio, ld_data_fine


dw_selezione.accepttext()

dw_report.change_dw_current()

iuo_dw_main = dw_report

ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")

if isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then
	ls_cod_cliente = "%"
end if

ls_cod_destinazione = dw_selezione.getitemstring(1,"cod_destinazione")

if trim(ls_cod_destinazione) = "" then
	setnull(ls_cod_destinazione)
end if

ls_cod_prodotto = dw_selezione.getitemstring(1,"cod_prodotto")

if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) < 1 then
	ls_cod_prodotto = "%"
end if

ls_flag_residuo = dw_selezione.getitemstring(1,"flag_residuo")
if isnull(ls_flag_residuo) then
	ls_flag_residuo = 'N'
end if

ls_flag_evaso = dw_selezione.getitemstring(1,"flag_evaso")

choose case ls_flag_evaso
	case "A"
		ls_flag_evasione_1="A"
		ls_flag_evasione_2="A"
		ls_flag_evasione_3="A"
	case "P"
		ls_flag_evasione_1="P"
		ls_flag_evasione_2="P"
		ls_flag_evasione_3="P"	
	case "E"
		ls_flag_evasione_1="E"
		ls_flag_evasione_2="E"
		ls_flag_evasione_3="E"
		if ls_flag_residuo = 'N' then	ls_flag_residuo = 'N'
	case "I"
		ls_flag_evasione_1="A"
		ls_flag_evasione_2="P"
		ls_flag_evasione_3="P"
	case "T"
		ls_flag_evasione_1="A"
		ls_flag_evasione_2="P"
		ls_flag_evasione_3="E"
		
end choose
//visualizzo un campo o l'altro....
//dw_report.object
//det_ord_ven_imponibile_iva

if ls_flag_residuo = 'S' then
	ls_modify = 'det_ord_ven_imponibile_iva.visible="0"'
	dw_report.modify(ls_modify)
	ls_modify = 'compute_6.visible="0"'
	dw_report.modify(ls_modify)
	ls_modify = 'compute_5.visible="0"'
	dw_report.modify(ls_modify)
	ls_modify = 'compute_7.visible="1"'
	dw_report.modify(ls_modify)
	ls_modify = 'compute_8.visible="1"'
	dw_report.modify(ls_modify)
	ls_modify = 'compute_9.visible="1"'
	dw_report.modify(ls_modify)
else
	ls_modify = 'compute_7.visible="0"'
	dw_report.modify(ls_modify)
	ls_modify = 'compute_8.visible="0"'
	dw_report.modify(ls_modify)
	ls_modify = 'compute_9.visible="0"'
	dw_report.modify(ls_modify)
	ls_modify = 'det_ord_ven_imponibile_iva.visible="1"'
	dw_report.modify(ls_modify)
	ls_modify = 'compute_6.visible="1"'
	dw_report.modify(ls_modify)
	ls_modify = 'compute_5.visible="1"'
	dw_report.modify(ls_modify)
end if
//ls_modify = 'compute_7.visible="0"'

//ls_modify = '	det_ord_ven_imponibile_iva.visible="0"'		

ld_data_inizio = dw_selezione.getitemdate(1,"d_data_da")

ld_data_fine   = dw_selezione.getitemdate(1,"d_data_a")

ll_anno_ordine = dw_selezione.getitemnumber(1,"n_anno")

if ll_anno_ordine = 0 then
	setnull(ll_anno_ordine)
end if

ll_ordine_inizio = dw_selezione.getitemnumber(1,"n_ord_da")

if isnull(ll_ordine_inizio) or ll_ordine_inizio = 0 then
	ll_ordine_inizio = 1
end if

ll_ordine_fine   = dw_selezione.getitemnumber(1,"n_ord_a")

if isnull(ll_ordine_fine) or ll_ordine_fine = 0 then
	ll_ordine_fine = 999999
end if	

ls_flag_blocco = dw_selezione.getitemstring(1,"flag_blocco")

if isnull(ls_flag_blocco) then
	ls_flag_blocco = 'N'
end if	

ll_errore = dw_report.retrieve(s_cs_xx.cod_azienda, ld_data_inizio, ld_data_fine, ll_ordine_inizio, ll_ordine_fine, &
										ls_flag_evasione_1, ls_flag_evasione_2, ls_flag_evasione_3, ls_cod_cliente, &
										ls_cod_prodotto, ll_anno_ordine, ls_flag_blocco, ls_cod_destinazione)
										
dw_report.modify("p_report_ol.filename='" + s_cs_xx.volume + s_cs_xx.risorse + "12.5\produzione_bl.png'~t")									
dw_report.modify("p_commessa_pronta.filename='" + s_cs_xx.volume + s_cs_xx.risorse + "12.5\commessa_pronta.png'~t")									

end event

event getfocus;call super::getfocus;//dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = dw_selezione
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

event doubleclicked;
if not isnull(dwo) then
	if dwo.name="cf_num_commessa" then
	
		long	ll_anno_commessa, ll_num_commessa
		s_cs_xx_parametri				lstr_param
		
		ll_anno_commessa = getitemnumber(row, 25)
		ll_num_commessa = getitemnumber(row, 26)
		
		lstr_param.parametro_ul_1=ll_anno_commessa
		lstr_param.parametro_ul_2=ll_num_commessa
		
		window_open_parm(w_commesse_produzione_tv, -1, lstr_param)
	elseif dwo.name="p_report_ol" then
		
			s_cs_xx.parametri.parametro_i_1 = getitemnumber(row, 25)
			s_cs_xx.parametri.parametro_dec4_1 = getitemnumber(row, 26)
			s_cs_xx.parametri.parametro_i_2 = 1
			
			window_open(w_report_bl_attivazione,-1)
		
	end if
end if

end event

type cb_1 from commandbutton within w_righe_ordini_cliente_euro
integer x = 5234
integer y = 300
integer width = 297
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;dw_report.triggerevent("pcd_retrieve")
end event

type dw_selezione from uo_cs_xx_dw within w_righe_ordini_cliente_euro
integer y = 20
integer width = 3291
integer height = 456
integer taborder = 20
string dataobject = "d_selezione_righe_ordini_cliente"
end type

event itemchanged;call super::itemchanged;if i_colname = "cod_cliente" then
	
	string ls_null
	
	setnull(ls_null)
	
	setitem(row,"cod_destinazione",ls_null)
	
	f_po_loaddddw_dw(this,"cod_destinazione",sqlca,"anag_des_clienti","cod_des_cliente","rag_soc_1", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + i_coltext + "'")
	
end if
end event

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
		
end choose
end event

type cb_reset from commandbutton within w_righe_ordini_cliente_euro
integer x = 5234
integer y = 120
integer width = 297
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Azzera"
end type

event clicked;string ls_null
date ld_null
long ll_null

setnull(ld_null)
setnull(ll_null)
setnull(ls_null)

dw_selezione.setitem(dw_selezione.getrow(), "cod_cliente", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_destinazione", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_prodotto", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "d_data_da", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "d_data_a", ld_null)
dw_selezione.setitem(dw_selezione.getrow(),"n_anno", ll_null)
dw_selezione.setitem(dw_selezione.getrow(), "n_ord_da", ll_null)
dw_selezione.setitem(dw_selezione.getrow(), "n_ord_a", ll_null)
dw_selezione.setitem(dw_selezione.getrow(), "flag_evaso", "T")
dw_selezione.setitem(dw_selezione.getrow(), "flag_blocco", "N")
dw_selezione.setitem(dw_selezione.getrow(), "flag_residuo", "N")
dw_selezione.resetupdate()
end event

