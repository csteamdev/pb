﻿$PBExportHeader$w_stampa_commesse_attive.srw
$PBExportComments$Window per stampa commesse attive
forward
global type w_stampa_commesse_attive from w_cs_xx_principale
end type
type cbx_bolla_lavoro_commessa from checkbox within w_stampa_commesse_attive
end type
type cbx_etichette_produzione from checkbox within w_stampa_commesse_attive
end type
type dw_report_etichette_produzione from datawindow within w_stampa_commesse_attive
end type
type cb_retrieve from commandbutton within w_stampa_commesse_attive
end type
type cb_stampa from commandbutton within w_stampa_commesse_attive
end type
type cbx_num_a_num from checkbox within w_stampa_commesse_attive
end type
type sle_a_numero from singlelineedit within w_stampa_commesse_attive
end type
type st_2 from statictext within w_stampa_commesse_attive
end type
type sle_da_numero from singlelineedit within w_stampa_commesse_attive
end type
type st_1 from statictext within w_stampa_commesse_attive
end type
type dw_report_bolle_lavoro from datawindow within w_stampa_commesse_attive
end type
type dw_commesse_lista from uo_cs_xx_dw within w_stampa_commesse_attive
end type
type em_anno_commessa from editmask within w_stampa_commesse_attive
end type
type st_4 from statictext within w_stampa_commesse_attive
end type
type st_num_commesse from statictext within w_stampa_commesse_attive
end type
end forward

global type w_stampa_commesse_attive from w_cs_xx_principale
integer width = 1984
integer height = 1680
string title = "Stampa complessiva bolle di lavoro"
cbx_bolla_lavoro_commessa cbx_bolla_lavoro_commessa
cbx_etichette_produzione cbx_etichette_produzione
dw_report_etichette_produzione dw_report_etichette_produzione
cb_retrieve cb_retrieve
cb_stampa cb_stampa
cbx_num_a_num cbx_num_a_num
sle_a_numero sle_a_numero
st_2 st_2
sle_da_numero sle_da_numero
st_1 st_1
dw_report_bolle_lavoro dw_report_bolle_lavoro
dw_commesse_lista dw_commesse_lista
em_anno_commessa em_anno_commessa
st_4 st_4
st_num_commesse st_num_commesse
end type
global w_stampa_commesse_attive w_stampa_commesse_attive

type variables
boolean ib_in_new=FALSE
boolean ib_attivata=false
end variables

forward prototypes
public function integer wf_report (integer fi_anno, long fl_numero, long fl_prog)
public function integer wf_stampa (integer fi_anno_commessa, long fl_num_commessa)
public function integer wf_report_1 (integer fi_anno, long fl_numero, long fl_prog)
end prototypes

public function integer wf_report (integer fi_anno, long fl_numero, long fl_prog);long    ll_num_commessa,ll_prog_riga,ll_num_registrazione,ll_i,ll_t,ll_prog_stock
integer li_anno_commessa,li_anno_registrazione
string  ls_anno_commessa,ls_num_commessa,ls_cod_prodotto,ls_cod_reparto,ls_cod_lavorazione, &
		  ls_prog_riga,ls_buffer,ls_note_fase,ls_quan_in_produzione,ls_cod_prodotto_finito, & 
		  ls_des_prodotto_finito,ls_cod_materia_prima,ls_des_materia_prima,ls_cod_prodotto_1, &
		  ls_cod_cliente,ls_rag_soc_cliente,ls_cod_barre_1,ls_cod_barre_2,ls_cod_barre_3,ls_cod_mezzo, & 
		  ls_des_mezzo,ls_des_prodotto,ls_mat_prime,ls_cod_deposito,ls_des_deposito,ls_cod_lotto, & 
		  ls_cod_ubicazione,ls_cod_prodotto_variante,ldd_quan_utilizzo_variante,ls_num_ord_cliente,ls_cod_versione, &
		  ls_cod_vettore, ls_des_vettore, ls_destinazione
double  ldd_quan_in_produzione,ldd_quan_in_produzione_mp,ldd_tempo_lavorazione,ldd_tempo_attrezzaggio,ldd_quan_in_produzione_sl
datetime ldt_data_ord_cliente,ldt_data_consegna,ldt_data_stock

li_anno_commessa = fi_anno
ll_num_commessa = fl_numero
ll_prog_riga = fl_prog

setnull(ls_cod_cliente)
setnull(li_anno_registrazione)
dw_report_bolle_lavoro.reset()

select anno_registrazione,
		 num_registrazione
into   :li_anno_registrazione,
		 :ll_num_registrazione
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode=0 then
	select cod_cliente,
			 num_ord_cliente,
			 data_ord_cliente,	 
			 cod_mezzo,
			 cod_vettore,
			 localita
   into   :ls_cod_cliente,
			 :ls_num_ord_cliente,
			 :ldt_data_ord_cliente,
			 :ls_cod_mezzo,
			 :ls_cod_vettore,
			 :ls_destinazione
	from   tes_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;

	select rag_soc_1
	into   :ls_rag_soc_cliente
	from   anag_clienti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cliente=:ls_cod_cliente;

	select des_mezzo
	into   :ls_des_mezzo
	from   tab_mezzi
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_mezzo=:ls_cod_mezzo;

	if isnull(ls_cod_vettore) then
		 setnull(ls_des_vettore)
	else
		select rag_soc_1
		into   :ls_des_vettore
		from   anag_vettori
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_vettore=:ls_cod_vettore;
		
		if sqlca.sqlcode <> 0 then setnull(ls_des_vettore)
	end if
end if
	
select cod_prodotto,
		 quan_in_produzione,
		 data_consegna,
		 cod_versione
into   :ls_cod_prodotto_finito,
		 :ldd_quan_in_produzione,
		 :ldt_data_consegna,	
		 :ls_cod_versione
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

select des_prodotto
into   :ls_des_prodotto_finito
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto_finito;
	
ls_anno_commessa = string(li_anno_commessa)
ls_num_commessa = string(ll_num_commessa)
ls_prog_riga = string(ll_prog_riga)
ls_quan_in_produzione= string(ldd_quan_in_produzione)

//if len(ls_num_commessa) < 6 then
//	ls_num_commessa = ls_num_commessa + fill("_", 6 - len(ls_num_commessa))
//end if

declare  righe_avanzamento_pc_1 cursor for
select   cod_prodotto,
		   cod_reparto,
		   cod_lavorazione,
			quan_in_produzione
from     avan_produzione_com
where    cod_azienda=:s_cs_xx.cod_azienda
and      anno_commessa=:li_anno_commessa
and      num_commessa=:ll_num_commessa
and      prog_riga=:ll_prog_riga
order by cod_prodotto;

open righe_avanzamento_pc_1;
	
do while 1=1
	fetch righe_avanzamento_pc_1
	into  :ls_cod_prodotto,	
  		   :ls_cod_reparto,
			:ls_cod_lavorazione,
			:ldd_quan_in_produzione_sl;

	if sqlca.sqlcode = 100 then exit
	
	dw_report_bolle_lavoro.insertrow(0)
	ll_i = dw_report_bolle_lavoro.rowcount()

	//if ll_i = 1 then
		dw_report_bolle_lavoro.setitem(ll_i,"anno_commessa",ls_anno_commessa)
		dw_report_bolle_lavoro.setitem(ll_i,"num_commessa",ls_num_commessa)
		dw_report_bolle_lavoro.setitem(ll_i,"prog_riga",string(ll_prog_riga))
		dw_report_bolle_lavoro.setitem(ll_i,"cod_prodotto",ls_cod_prodotto_finito)
		dw_report_bolle_lavoro.setitem(ll_i,"des_prodotto",ls_des_prodotto_finito)
		dw_report_bolle_lavoro.setitem(ll_i,"data_consegna",left(string(ldt_data_consegna),10))
		dw_report_bolle_lavoro.setitem(ll_i,"mezzo",ls_des_mezzo)
		dw_report_bolle_lavoro.setitem(ll_i,"vettore",ls_des_vettore)
		dw_report_bolle_lavoro.setitem(ll_i,"destinazione", ls_destinazione)
		dw_report_bolle_lavoro.setitem(ll_i,"quan_in_produzione",ls_quan_in_produzione)
		dw_report_bolle_lavoro.setitem(ll_i,"anno_ordine",string(li_anno_registrazione))
		dw_report_bolle_lavoro.setitem(ll_i,"num_ordine",string(ll_num_registrazione))
		dw_report_bolle_lavoro.setitem(ll_i,"cod_cliente",ls_cod_cliente)
		dw_report_bolle_lavoro.setitem(ll_i,"rag_sociale",ls_rag_soc_cliente)
		dw_report_bolle_lavoro.setitem(ll_i,"num_ord_cliente",string(ls_num_ord_cliente))
		dw_report_bolle_lavoro.setitem(ll_i,"data_ord_cliente",left(string(ldt_data_ord_cliente),10))
	//end if

	ls_mat_prime = ""
	ls_des_materia_prima = ""

	select des_prodotto 
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;

	dw_report_bolle_lavoro.setitem(ll_i,"cod_semilavorato",ls_cod_prodotto)
	dw_report_bolle_lavoro.setitem(ll_i,"des_semilavorato",ls_des_prodotto)

	ls_cod_prodotto_1 = trim(ls_cod_prodotto)

	declare righe_db cursor for
	select  cod_prodotto_figlio
	from    distinta
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     cod_prodotto_padre=:ls_cod_prodotto_1
	and     cod_versione=:ls_cod_versione;

	open righe_db;
	
	do while 1=1
		fetch righe_db
		into  :ls_cod_materia_prima;

		if sqlca.sqlcode = 100 or sqlca.sqlcode <0 then exit

		select cod_prodotto
		into   :ls_cod_prodotto_variante
		from   varianti_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    cod_prodotto_padre=:ls_cod_prodotto_1
		and    cod_prodotto_figlio =:ls_cod_materia_prima;

		if sqlca.sqlcode = 0 and not isnull(ls_cod_prodotto_variante) then
			ls_cod_materia_prima = ls_cod_prodotto_variante
		end if

		select des_prodotto
		into   :ls_des_materia_prima
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_materia_prima;

		select quan_in_produzione
		into   :ldd_quan_in_produzione_mp
		from   mat_prime_commessa
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa 
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga;
		
		ls_mat_prime = ls_mat_prime + "Materia Prima:" + ls_cod_materia_prima + " - " + ls_des_materia_prima + char(13)	

	loop

	close righe_db;

	dw_report_bolle_lavoro.setitem(ll_i,"mat_prime",ls_mat_prime)

	select des_estesa_prodotto,
			 tempo_lavorazione,
			 tempo_attrezzaggio
	into   :ls_note_fase,
			 :ldd_tempo_lavorazione,
			 :ldd_tempo_attrezzaggio
	from   tes_fasi_lavorazione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto
	and    cod_reparto=:ls_cod_reparto
	and    cod_lavorazione=:ls_cod_lavorazione;

	ls_cod_barre_1 = "*" + ls_anno_commessa + ls_num_commessa + string(fl_prog) + "*"
	ls_cod_barre_2 = "*" + ls_cod_prodotto + "*"
	ls_cod_barre_3 = "*" + ls_cod_reparto + ls_cod_lavorazione + "*"

	dw_report_bolle_lavoro.setitem(ll_i,"des_fase",ls_note_fase)
	dw_report_bolle_lavoro.setitem(ll_i,"quan_in_produzione_sl",string(ldd_quan_in_produzione_sl))
	dw_report_bolle_lavoro.setitem(ll_i,"tempo",string(ldd_tempo_lavorazione*ldd_quan_in_produzione_sl))
	dw_report_bolle_lavoro.setitem(ll_i,"tempo_1",string(ldd_tempo_attrezzaggio))
	dw_report_bolle_lavoro.setitem(ll_i,"cod_barre_1",ls_cod_barre_1)
	dw_report_bolle_lavoro.setitem(ll_i,"cod_barre_2",ls_cod_barre_2)
	dw_report_bolle_lavoro.setitem(ll_i,"cod_barre_3",ls_cod_barre_3)

loop

close righe_avanzamento_pc_1;

return 0
end function

public function integer wf_stampa (integer fi_anno_commessa, long fl_num_commessa);long ll_progr,job
integer li_risposta 

declare r_com cursor for
select prog_riga
from   det_anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:fi_anno_commessa
and    num_commessa=:fl_num_commessa;

open r_com;

do while 1=1
	fetch r_com
	into  :ll_progr;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	li_risposta = wf_report(fi_anno_commessa,fl_num_commessa,ll_progr)
	li_risposta = wf_report_1(fi_anno_commessa,fl_num_commessa,ll_progr)
	
//	job = PrintOpen( ) 
//	if cbx_bolla_lavoro_commessa.checked = true then PrintDataWindow(job, dw_report_bolle_lavoro) 
//	if cbx_etichette_produzione.checked = true then PrintDataWindow(job, dw_report_etichette_produzione) 
//	PrintClose(job)

	if cbx_bolla_lavoro_commessa.checked = true then dw_report_bolle_lavoro.print(false, false)
	if cbx_etichette_produzione.checked = true then dw_report_etichette_produzione.print(false, false)

	
	dw_report_bolle_lavoro.reset()
	dw_report_etichette_produzione.reset()

	
loop

close r_com;

return 0
end function

public function integer wf_report_1 (integer fi_anno, long fl_numero, long fl_prog);long    ll_num_commessa,ll_prog_riga,ll_num_registrazione,ll_i,ll_t
integer li_anno_commessa,li_anno_registrazione
string  ls_anno_commessa,ls_num_commessa,ls_cod_prodotto, &
		  ls_prog_riga,ls_quan_in_produzione,ls_cod_prodotto_finito, & 
		  ls_des_prodotto_finito,ls_cod_materia_prima,ls_des_materia_prima, &
		  ls_cod_cliente,ls_cod_prodotto_1,ls_cod_lavorazione,ls_cod_reparto,& 
		  ls_des_prodotto,ls_mat_prime, ls_note_fase,& 
		  ls_cod_prodotto_variante,ldd_quan_utilizzo_variante,ls_cod_versione, ls_destinazione
double  ldd_quan_in_produzione,ldd_quan_in_produzione_mp,ldd_quan_in_produzione_sl
datetime ldt_data_consegna


li_anno_commessa = fi_anno
ll_num_commessa = fl_numero
ll_prog_riga = fl_prog

setnull(ls_cod_cliente)
setnull(li_anno_registrazione)
dw_report_etichette_produzione.reset()

select anno_registrazione,
		 num_registrazione
into   :li_anno_registrazione,
		 :ll_num_registrazione
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode=0 then
	select cod_cliente, localita
   into   :ls_cod_cliente, :ls_destinazione
	from   tes_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;	

end if
	
select cod_prodotto,
		 quan_in_produzione,
		 data_consegna,
		 cod_versione
into   :ls_cod_prodotto_finito,
		 :ldd_quan_in_produzione,
		 :ldt_data_consegna,
		 :ls_cod_versione
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

select des_prodotto
into   :ls_des_prodotto_finito
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto_finito;
	
ls_anno_commessa = string(li_anno_commessa)
ls_num_commessa = string(ll_num_commessa)
ls_prog_riga = string(ll_prog_riga)
ls_quan_in_produzione= string(ldd_quan_in_produzione)


declare  righe_avanzamento_pc_1 cursor for
select   cod_prodotto,
			quan_in_produzione,
			cod_lavorazione,
			cod_reparto
from     avan_produzione_com
where    cod_azienda=:s_cs_xx.cod_azienda
and      anno_commessa=:li_anno_commessa
and      num_commessa=:ll_num_commessa
and      prog_riga=:ll_prog_riga
order by cod_prodotto;

open righe_avanzamento_pc_1;
	
do while 1=1
	fetch righe_avanzamento_pc_1
	into  :ls_cod_prodotto,	
			:ldd_quan_in_produzione_sl,
			:ls_cod_lavorazione,
			:ls_cod_reparto;

	if sqlca.sqlcode = 100 then exit
	
	dw_report_etichette_produzione.insertrow(0)
	ll_i = dw_report_etichette_produzione.rowcount()

	dw_report_etichette_produzione.setitem(ll_i,"anno_commessa",ls_anno_commessa)
	dw_report_etichette_produzione.setitem(ll_i,"num_commessa",ls_num_commessa)
	dw_report_etichette_produzione.setitem(ll_i,"prog_riga", ls_prog_riga)
	dw_report_etichette_produzione.setitem(ll_i,"cod_prodotto",ls_cod_prodotto_finito)
	dw_report_etichette_produzione.setitem(ll_i,"cod_cliente",ls_cod_cliente)
	dw_report_etichette_produzione.setitem(ll_i,"quan_in_produzione",ls_quan_in_produzione)
	dw_report_etichette_produzione.setitem(ll_i,"data_consegna",left(string(ldt_data_consegna),10))
	dw_report_etichette_produzione.setitem(ll_i, "destinazione", ls_destinazione)

	ls_mat_prime = ""
	ls_des_materia_prima = ""

	select des_prodotto 
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;

	dw_report_etichette_produzione.setitem(ll_i,"cod_semilavorato",ls_cod_prodotto)
	dw_report_etichette_produzione.setitem(ll_i,"des_semilavorato",ls_des_prodotto)


	ls_cod_prodotto_1 = trim(ls_cod_prodotto)

	declare righe_db cursor for
	select  cod_prodotto_figlio
	from    distinta
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     cod_prodotto_padre=:ls_cod_prodotto_1
	and     cod_versione=:ls_cod_versione;

	open righe_db;
	
	do while 1=1
		fetch righe_db
		into  :ls_cod_materia_prima;

		if sqlca.sqlcode = 100 or sqlca.sqlcode <0 then exit

		select cod_prodotto
		into   :ls_cod_prodotto_variante
		from   varianti_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa
		and    cod_prodotto_padre=:ls_cod_prodotto_1
		and    cod_prodotto_figlio =:ls_cod_materia_prima;

		if sqlca.sqlcode = 0 and not isnull(ls_cod_prodotto_variante) then
			ls_cod_materia_prima = ls_cod_prodotto_variante
		end if

		select des_prodotto
		into   :ls_des_materia_prima
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_materia_prima;

		select quan_in_produzione
		into   :ldd_quan_in_produzione_mp
		from   mat_prime_commessa
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa 
		and    num_commessa=:ll_num_commessa
		and    prog_riga=:ll_prog_riga;
		
		ls_mat_prime = ls_mat_prime + "Materia Prima:" + ls_cod_materia_prima + " - " + ls_des_materia_prima + char(13)	

	loop

	close righe_db;

	dw_report_etichette_produzione.setitem(ll_i,"mat_prime",ls_mat_prime)

	select des_estesa_prodotto
	into   :ls_note_fase
	from   tes_fasi_lavorazione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto
	and    cod_reparto=:ls_cod_reparto
	and    cod_lavorazione=:ls_cod_lavorazione;
			

	dw_report_etichette_produzione.setitem(ll_i,"des_fase",ls_note_fase)

loop

close righe_avanzamento_pc_1;

return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_commesse_lista.ib_dw_report=true
dw_commesse_lista.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nodelete + c_nomodify + c_multiselect,c_default)
iuo_dw_main = dw_commesse_lista


em_anno_commessa.text=string(f_anno_esercizio ( ))

end event

on w_stampa_commesse_attive.create
int iCurrent
call super::create
this.cbx_bolla_lavoro_commessa=create cbx_bolla_lavoro_commessa
this.cbx_etichette_produzione=create cbx_etichette_produzione
this.dw_report_etichette_produzione=create dw_report_etichette_produzione
this.cb_retrieve=create cb_retrieve
this.cb_stampa=create cb_stampa
this.cbx_num_a_num=create cbx_num_a_num
this.sle_a_numero=create sle_a_numero
this.st_2=create st_2
this.sle_da_numero=create sle_da_numero
this.st_1=create st_1
this.dw_report_bolle_lavoro=create dw_report_bolle_lavoro
this.dw_commesse_lista=create dw_commesse_lista
this.em_anno_commessa=create em_anno_commessa
this.st_4=create st_4
this.st_num_commesse=create st_num_commesse
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_bolla_lavoro_commessa
this.Control[iCurrent+2]=this.cbx_etichette_produzione
this.Control[iCurrent+3]=this.dw_report_etichette_produzione
this.Control[iCurrent+4]=this.cb_retrieve
this.Control[iCurrent+5]=this.cb_stampa
this.Control[iCurrent+6]=this.cbx_num_a_num
this.Control[iCurrent+7]=this.sle_a_numero
this.Control[iCurrent+8]=this.st_2
this.Control[iCurrent+9]=this.sle_da_numero
this.Control[iCurrent+10]=this.st_1
this.Control[iCurrent+11]=this.dw_report_bolle_lavoro
this.Control[iCurrent+12]=this.dw_commesse_lista
this.Control[iCurrent+13]=this.em_anno_commessa
this.Control[iCurrent+14]=this.st_4
this.Control[iCurrent+15]=this.st_num_commesse
end on

on w_stampa_commesse_attive.destroy
call super::destroy
destroy(this.cbx_bolla_lavoro_commessa)
destroy(this.cbx_etichette_produzione)
destroy(this.dw_report_etichette_produzione)
destroy(this.cb_retrieve)
destroy(this.cb_stampa)
destroy(this.cbx_num_a_num)
destroy(this.sle_a_numero)
destroy(this.st_2)
destroy(this.sle_da_numero)
destroy(this.st_1)
destroy(this.dw_report_bolle_lavoro)
destroy(this.dw_commesse_lista)
destroy(this.em_anno_commessa)
destroy(this.st_4)
destroy(this.st_num_commesse)
end on

type cbx_bolla_lavoro_commessa from checkbox within w_stampa_commesse_attive
integer x = 855
integer y = 820
integer width = 1042
integer height = 76
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stampa bolla lavoro commessa"
boolean checked = true
end type

type cbx_etichette_produzione from checkbox within w_stampa_commesse_attive
integer x = 855
integer y = 920
integer width = 1042
integer height = 76
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stampa scheda lavorazioni"
boolean checked = true
end type

type dw_report_etichette_produzione from datawindow within w_stampa_commesse_attive
boolean visible = false
integer x = 823
integer y = 940
integer width = 891
integer height = 300
integer taborder = 60
string title = "none"
string dataobject = "r_report_etichette_produzione"
boolean border = false
boolean livescroll = true
end type

type cb_retrieve from commandbutton within w_stampa_commesse_attive
integer x = 1806
integer y = 136
integer width = 119
integer height = 128
integer taborder = 20
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">"
end type

event clicked;parent.triggerevent("pc_retrieve")
end event

type cb_stampa from commandbutton within w_stampa_commesse_attive
integer x = 1563
integer y = 1480
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long ll_row[],ll_righe,ll_num_commessa,ll_selected[],job,ll_da_num, ll_a_num,ll_progr
integer li_anno_commessa,li_risposta
string ls_flag_tipo_avanzamento

setpointer (hourglass!)

if cbx_num_a_num.checked = true then
	ll_da_num = long(sle_da_numero.text)
	ll_a_num = long(sle_a_numero.text)
	li_anno_commessa=integer(em_anno_commessa.text)
	
	for ll_num_commessa = ll_da_num to ll_a_num
		
		select flag_tipo_avanzamento
		into   :ls_flag_tipo_avanzamento
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
		if ls_flag_tipo_avanzamento = "2" then
			li_risposta = wf_stampa(li_anno_commessa,ll_num_commessa)			
		end if
		
	next
	
	
else
	dw_commesse_lista.get_selected_rows(ll_selected[])
	
	for ll_righe = 1 to upperbound(ll_selected)
		li_anno_commessa = dw_commesse_lista.getitemnumber(ll_selected[ll_righe], "anno_commessa")
		ll_num_commessa = dw_commesse_lista.getitemnumber(ll_selected[ll_righe], "num_commessa")
		
		li_risposta = wf_stampa(li_anno_commessa,ll_num_commessa)	
		
	next
end if

setpointer (arrow!)
end event

type cbx_num_a_num from checkbox within w_stampa_commesse_attive
integer x = 855
integer y = 360
integer width = 1051
integer height = 76
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stampa da numero a numero"
end type

event clicked;if sle_da_numero.enabled = false then
	sle_da_numero.enabled = true
	sle_a_numero.enabled=true
else
	sle_da_numero.enabled = false
	sle_a_numero.enabled=false
end if
end event

type sle_a_numero from singlelineedit within w_stampa_commesse_attive
integer x = 1408
integer y = 632
integer width = 430
integer height = 120
integer taborder = 40
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
boolean border = false
end type

type st_2 from statictext within w_stampa_commesse_attive
integer x = 882
integer y = 624
integer width = 507
integer height = 124
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "a numero:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_da_numero from singlelineedit within w_stampa_commesse_attive
integer x = 1408
integer y = 468
integer width = 430
integer height = 120
integer taborder = 30
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
boolean border = false
end type

type st_1 from statictext within w_stampa_commesse_attive
integer x = 855
integer y = 468
integer width = 535
integer height = 120
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "da numero:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_report_bolle_lavoro from datawindow within w_stampa_commesse_attive
boolean visible = false
integer x = 823
integer y = 1260
integer width = 891
integer height = 300
integer taborder = 50
string title = "none"
string dataobject = "r_report_bl_attivazione"
boolean border = false
boolean livescroll = true
end type

type dw_commesse_lista from uo_cs_xx_dw within w_stampa_commesse_attive
integer x = 23
integer y = 20
integer width = 777
integer height = 1540
integer taborder = 10
string dataobject = "d_commesse_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_anno_commessa
string ls_tipo_avanzamento

ls_tipo_avanzamento =  "2" // COMMESSE ATTIVATE E ANCORA IN PRODUZIONE

ll_anno_commessa=long(em_anno_commessa.text)
l_Error = Retrieve(s_cs_xx.cod_azienda, ll_anno_commessa, ls_tipo_avanzamento)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
else
	st_num_commesse.text = "Nr. Commesse: " + string(l_error)
END IF
end event

type em_anno_commessa from editmask within w_stampa_commesse_attive
integer x = 1408
integer y = 136
integer width = 379
integer height = 120
integer taborder = 20
boolean bringtotop = true
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean border = false
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
boolean autoskip = true
boolean spin = true
string displaydata = ""
double increment = 1
string minmax = "1900~~2999"
end type

event modified;dw_commesse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type st_4 from statictext within w_stampa_commesse_attive
integer x = 1093
integer y = 140
integer width = 297
integer height = 108
boolean bringtotop = true
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Anno:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_num_commesse from statictext within w_stampa_commesse_attive
integer x = 837
integer y = 20
integer width = 800
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nr.Commesse trovate: 0"
boolean focusrectangle = false
end type

