﻿$PBExportHeader$w_test_email.srw
forward
global type w_test_email from window
end type
type st_4 from statictext within w_test_email
end type
type st_3 from statictext within w_test_email
end type
type em_password from editmask within w_test_email
end type
type em_username from editmask within w_test_email
end type
type cbx_auth from checkbox within w_test_email
end type
type st_2 from statictext within w_test_email
end type
type st_1 from statictext within w_test_email
end type
type em_text from editmask within w_test_email
end type
type em_subject from editmask within w_test_email
end type
type em_send_email from editmask within w_test_email
end type
type em_send_name from editmask within w_test_email
end type
type em_server from editmask within w_test_email
end type
type em_from_name from editmask within w_test_email
end type
type em_from_email from editmask within w_test_email
end type
type cb_1 from commandbutton within w_test_email
end type
end forward

global type w_test_email from window
integer width = 2592
integer height = 1200
boolean border = false
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
st_4 st_4
st_3 st_3
em_password em_password
em_username em_username
cbx_auth cbx_auth
st_2 st_2
st_1 st_1
em_text em_text
em_subject em_subject
em_send_email em_send_email
em_send_name em_send_name
em_server em_server
em_from_name em_from_name
em_from_email em_from_email
cb_1 cb_1
end type
global w_test_email w_test_email

on w_test_email.create
this.st_4=create st_4
this.st_3=create st_3
this.em_password=create em_password
this.em_username=create em_username
this.cbx_auth=create cbx_auth
this.st_2=create st_2
this.st_1=create st_1
this.em_text=create em_text
this.em_subject=create em_subject
this.em_send_email=create em_send_email
this.em_send_name=create em_send_name
this.em_server=create em_server
this.em_from_name=create em_from_name
this.em_from_email=create em_from_email
this.cb_1=create cb_1
this.Control[]={this.st_4,&
this.st_3,&
this.em_password,&
this.em_username,&
this.cbx_auth,&
this.st_2,&
this.st_1,&
this.em_text,&
this.em_subject,&
this.em_send_email,&
this.em_send_name,&
this.em_server,&
this.em_from_name,&
this.em_from_email,&
this.cb_1}
end on

on w_test_email.destroy
destroy(this.st_4)
destroy(this.st_3)
destroy(this.em_password)
destroy(this.em_username)
destroy(this.cbx_auth)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_text)
destroy(this.em_subject)
destroy(this.em_send_email)
destroy(this.em_send_name)
destroy(this.em_server)
destroy(this.em_from_name)
destroy(this.em_from_email)
destroy(this.cb_1)
end on

type st_4 from statictext within w_test_email
integer x = 878
integer y = 252
integer width = 343
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Password"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_test_email
integer x = 878
integer y = 144
integer width = 343
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Username"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_password from editmask within w_test_email
integer x = 1253
integer y = 236
integer width = 901
integer height = 88
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_username from editmask within w_test_email
integer x = 1253
integer y = 132
integer width = 901
integer height = 88
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type cbx_auth from checkbox within w_test_email
integer x = 1248
integer y = 24
integer width = 494
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Autenticazione"
end type

type st_2 from statictext within w_test_email
integer x = 1257
integer y = 444
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "TO"
boolean focusrectangle = false
end type

type st_1 from statictext within w_test_email
integer x = 41
integer y = 448
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "FROM"
boolean focusrectangle = false
end type

type em_text from editmask within w_test_email
integer x = 27
integer y = 984
integer width = 901
integer height = 88
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "message"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_subject from editmask within w_test_email
integer x = 27
integer y = 864
integer width = 901
integer height = 88
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "subject"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_send_email from editmask within w_test_email
integer x = 1253
integer y = 628
integer width = 901
integer height = 88
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "enrico.menegotto@csteam.com"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_send_name from editmask within w_test_email
integer x = 1253
integer y = 520
integer width = 901
integer height = 88
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "enrico menegotto[recipient]"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_server from editmask within w_test_email
integer x = 37
integer y = 20
integer width = 901
integer height = 88
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "mail.csteam.com"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_from_name from editmask within w_test_email
integer x = 46
integer y = 520
integer width = 901
integer height = 88
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "enrico menegotto[sender]"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_from_email from editmask within w_test_email
integer x = 46
integer y = 628
integer width = 901
integer height = 88
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "enrico.menegotto@csteam.com"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type cb_1 from commandbutton within w_test_email
integer x = 1760
integer y = 960
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "SEND"
end type

event clicked;n_smtp ln_mail
String ls_server, ls_from_email, ls_from_name
String ls_subject, ls_body, ls_send_email, ls_send_name

ls_server = em_server.text    		// name of smtp server
ls_from_email = em_from_email.text  // from email address
ls_from_name = em_from_name.text    // name of the from person
ls_subject = em_subject.text        // subject
ls_body = em_text.text              // the message text to be sent
ls_send_email = em_send_email.text  // send to email address
ls_send_name = em_send_name.text    // name of the send to person

ln_mail.of_SetServer(ls_server)
ln_mail.of_SetFrom(ls_from_email, ls_from_name)
ln_mail.of_SetSubject(ls_subject)
ln_mail.of_SetBody(ls_body, False)
ln_mail.of_AddTo(ls_send_email, ls_send_name)
//ln_mail.of_SendMail()

If cbx_auth.checked Then
	ln_mail.of_SetLogin(em_username.text, em_password.text)
End If


If Not ln_mail.of_SendMail() Then
	MessageBox("SendMail Error", ln_mail.is_msgtext  )
Else
	MessageBox("SendMail Error", "Mail Successfully Sent" )
end if

end event

