﻿$PBExportHeader$uo_service_base.sru
forward
global type uo_service_base from nonvisualobject
end type
end forward

global type uo_service_base from nonvisualobject
end type
global uo_service_base uo_service_base

type variables
protected:
	uo_log iuo_log
end variables

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
public subroutine uof_init (string as_service_name, integer ai_log_level)
public subroutine uof_set_s_cs_xx (string as_cod_azienda)
public subroutine uof_set_cod_utente (string as_cod_utente)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);/**
 *
 **/
 
return 1
end function

public subroutine uof_init (string as_service_name, integer ai_log_level);/**
 * stefanop
 * 12/11/2012
 *
 * Imposto dei parametri base per il servizio utili per la creazione
 * degli oggetti di istanza come ad esempio il log
 **/
 
iuo_log.set_file(s_cs_xx.volume + s_cs_xx.risorse + "logs/" + as_service_name + ".log")
iuo_log.set_log_level(ai_log_level)
end subroutine

public subroutine uof_set_s_cs_xx (string as_cod_azienda);s_cs_xx.cod_azienda = as_cod_azienda


end subroutine

public subroutine uof_set_cod_utente (string as_cod_utente);s_cs_xx.cod_utente = as_cod_utente


end subroutine

on uo_service_base.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_service_base.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;/**
 * stefanop
 * 12/11/2012
 *
 * Creazione degli oggetti base.
 * Le impostazioni base vengono eseguire da uo_service_manager tramite il
 * metodo uof_init
 **/

iuo_log = create uo_log
end event

