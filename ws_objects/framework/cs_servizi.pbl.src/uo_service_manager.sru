﻿$PBExportHeader$uo_service_manager.sru
forward
global type uo_service_manager from nonvisualobject
end type
end forward

global type uo_service_manager from nonvisualobject
end type
global uo_service_manager uo_service_manager

type variables
private:
	uo_commandparm_service iuo_option
	uo_log iuo_log
end variables

forward prototypes
public function integer wf_uo_test ()
public function integer dispatch (ref uo_commandparm auo_commandparm, uo_commandparm_service auo_option)
end prototypes

public function integer wf_uo_test ();string ls_string, ls_string_1, ls_string_2
int li_int, li_int_2, li_int_3
decimal ld_decimal
long ll_long
date ldt_date

ls_string = iuo_option.uof_get_string(1)
li_int = iuo_option.uof_get_int(-1, 5)
ld_decimal = iuo_option.uof_get_decimal(2)
ldt_date =  iuo_option.uof_get_date(3)
li_int_2 = iuo_option.uof_get_int(4)
ls_string_1 = iuo_option.uof_get_string(5)
ls_string_2 = iuo_option.uof_get_string(6, "cazzo")

return 1
end function

public function integer dispatch (ref uo_commandparm auo_commandparm, uo_commandparm_service auo_option);/**
 * stefanop
 * 08/11/2012
 *
 * Entry point per il lancio dei serivizi
 **/

string ls_service_name
int li_ret, li_minimal_log_level
uo_service_base luo_service_base
uo_commandparm_option luo_log_option

iuo_option = auo_option
li_minimal_log_level = uo_log.WARN_LEVEL // visualizzo sempre i messaggi di errore e warning

// Applico regole per i servizi
iuo_option.uof_init()
ls_service_name= "uo_service_" + iuo_option.uof_get_service_name()

try
	luo_service_base = create using ls_service_name
	
	// E' indicato -L come command parm?
	if auo_commandparm.uof_have_option("L") then
		luo_log_option = auo_commandparm.uof_get_option("L")
		li_minimal_log_level = luo_log_option.uof_get_int(1, li_minimal_log_level)
	end if
	// -----
	
	luo_service_base.uof_init(iuo_option.uof_get_service_name(), li_minimal_log_level)
	luo_service_base.dispatch(auo_option)
		
catch(RuntimeError ex)
	// Intercetto qualsiasi errore dei servizi e lo loggo
	// TODO: Aggiungere log
	iuo_log.error(ex.getmessage())
	return -1
	
finally
	destroy luo_service_base
	
end try
end function

on uo_service_manager.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_service_manager.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iuo_log = create uo_log

iuo_log.set_file(s_cs_xx.volume + s_cs_xx.risorse + "logs\service_manager.log")
end event

event destructor;destroy iuo_log
end event

