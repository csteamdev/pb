﻿$PBExportHeader$jsonexception.sru
forward
global type jsonexception from exception
end type
end forward

global type jsonexception from exception
end type
global jsonexception jsonexception

type variables

end variables

on jsonexception.create
call super::create
TriggerEvent( this, "constructor" )
end on

on jsonexception.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

