﻿$PBExportHeader$n_cst_crypto.sru
forward
global type n_cst_crypto from nonvisualobject
end type
end forward

global type n_cst_crypto from nonvisualobject
end type
global n_cst_crypto n_cst_crypto

type prototypes

end prototypes

type variables
// Chiavi per la generazione del codice di Attivazione.
PRIVATE:
string is_private_key = "CONSULTIGXZAB"

string	is_car_numeri = "1234567890"
string	is_car_maiuscoli = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
string	is_car_minuscoli = "abcdefghijklmnopqrstuvwxyz"
string is_car_simboli = "!#$%&()*+,-./:;<>=?@[]\_|{}"

end variables

forward prototypes
public function integer find_code (string as_char)
public function integer find_char (long al_code, long al_table_y)
public function integer decryptdata (string as_to_decrypt, ref string as_decrypted)
public function string old_decrypt (string data)
public function integer find_codeserial (string as_char)
public function integer encryptdata (string as_to_crypt, ref string as_crypted)
public function string encryptserial (string as_to_crypt)
public function boolean decryptserial (string as_public_key, string as_public_code)
public function string encrypt_data (string as_data)
public function string decrypt_data (string as_data)
public function integer uof_pwd_strenght (string as_password)
end prototypes

public function integer find_code (string as_char);long		ll_code

boolean	lb_found = false


for ll_code = 1 to upperbound(gs_crypt_accepted_chars)
	if gs_crypt_accepted_chars[ll_code] = as_char then
		lb_found = true
		exit
	end if
next

if not lb_found then
	return -1
else
	return ll_code
end if
end function

public function integer find_char (long al_code, long al_table_y);long		ll_table_x

boolean	lb_found = false


for ll_table_x = 1 to upperbound(gl_crypt_code_table,1)
	if gl_crypt_code_table[ll_table_x,al_table_y] = al_code then
		lb_found = true
		exit
	end if
next

if not lb_found then
	return -1
else
	return ll_table_x
end if
end function

public function integer decryptdata (string as_to_decrypt, ref string as_decrypted);string	ls_code, ls_char

long	ll_pos, ll_pos_code, ll_code, ll_table_x, ll_table_y


as_decrypted = ""

ll_pos_code = 0

// si scorre la stringa ricevuta come input processando una coppia di caratteri alla volta
for ll_pos = 1 to len(as_to_decrypt) step +2
	
	// dato che in fase di decrypt dobbiamo considerare una coppia di caratteri alla volta,
	// dobbiamo considerare come posizione il numero di coppia e non la posizione sulla stringa di input
	ll_pos_code ++
	
	ls_code = mid(as_to_decrypt,ll_pos,2)
	
	// se il frammento di stringa in esame non corrisponde ad un numero, significa che la stringa passata come
	// parametro non è stata codificata dall'algoritmo di crypt corrente.
	// sarà necessario gestire tale errore visualizzando una messagebox nell'oggetto chiamante
	if not isnumber(ls_code) then
		return -1
	end if
	
	// si converte in numerico la coppia di caratteri in esame per ottenere il codice di crypt
	ll_code = long(ls_code)
	
	// la tabella dei codici di crypt si ripete ogni 5 posizioni, si procede quindi dividendo le coppie di caratteri
	// nella stringa iniziale in multipli di 5
	ll_table_y = mod(ll_pos_code,5)
	
	if  ll_table_y = 0 then
		ll_table_y = 5
	end if	
	
	// la funzione find_char cerca di individuare la posizione del codice in esame nella relativa matrice
	ll_table_x = find_char(ll_code, ll_table_y)
	
	// se ll_table_x è -1 significa che il codice processato non è fra quelli utilizzati dall'algoritmo di crypt
	// il che significa di conseguenza che la stringa di input non è valida
	// sarà necessario gestire tale errore visualizzando una messagebox nell'oggetto chiamante
	if ll_table_x = -1 then
		return -1
	end if
	
	// a questo punto la posizione del codice nella tabella dei codici di cryp (ll_table_x) rappresenterà
	// l'ndice con cui possiamo andare ad individuare il carattere corrispondente
	// nel relativo array predefinito
	ls_char = gs_crypt_accepted_chars[ll_table_x]
	
	// si costruisce la stringa che rappresenterà il risultato finale dell'algoritmo di decrypt
	as_decrypted += ls_char
	
next

return 0
end function

public function string old_decrypt (string data);string  ls_stringa_1, ls_stringa_2, ls_chiave, ls_appo, ls_appo_2
integer li_i, li_l, li_k, li_v 

//stringa1 è il testo da criptare, stringa2 il testo criptato, chiave la chiave da usare, se k è 1 allora cripta, se k è -1 decripta

ls_stringa_1 = data

ls_chiave = "wlf69ciccio"

li_k = - 1

for li_i = 1 to len(ls_stringa_1)
    li_l = li_l + 1
	 ls_appo = mid( ls_stringa_1, li_i, 1)
	 ls_appo_2 = mid( ls_chiave, li_l, 1)
	 li_v = asca(ls_appo) + li_k * asca(ls_appo_2)
    if li_v > 255 then li_v = li_v - 256
    if li_v < 0 then li_v = li_v + 256
    ls_stringa_2 = ls_stringa_2 + char(li_v)
    if li_l = len(ls_chiave) then li_l=0
next

return ls_stringa_2
end function

public function integer find_codeserial (string as_char);// ritorno l'intero corrispondente alla lettera contenuta nell'array
//long ll_i
//
//for ll_i = 1 to  upperbound(is_array_key)
//	
//		if is_array_key[ll_i] = as_char then
//			
//			return ll_i -1
//			
//		end if
//		
//next

return -1
end function

public function integer encryptdata (string as_to_crypt, ref string as_crypted);string	ls_char

long	ll_pos, ll_table_x, ll_table_y, ll_code


as_crypted = ""

// si scorre la stringa ricevuta come input processando un carattere alla volta
for ll_pos = 1 to len(as_to_crypt)
	
	ls_char = mid(as_to_crypt,ll_pos,1)
	
	// la funzione find_code cerca di individuare la posizione del carattere in esame nella tabella dei caratteri ammessi
	ll_table_x = find_code(ls_char)
	
	// se ll_code è -1 significa che il carattere processato non è fra quelli ammessi
	// sarà necessario gestire tale errore visualizzando una messagebox nell'oggetto chiamante
	if ll_table_x = -1 then
		return -1
	end if
	
	// la tabella dei codici di cypt si ripete ogni 5 posizioni, si procede quindi dividendo la stringa iniziale in multipli di 5
	ll_table_y = mod(ll_pos,5)
	
	if  ll_table_y = 0 then
		ll_table_y = 5
	end if
	
	// a questo punto la posizione del carattere nella tabella caratteri (ll_table_x) e nella stringa di input (ll_table_y),
	// rappresenteranno le coordinate con cui possiamo andare ad individuare il codice numerico corrispondente
	// nella relativa matrice predefinita
	ll_code = gl_crypt_code_table[ll_table_x,ll_table_y]
	
	// convertiamo il codice numerico ottenuto in una stringa a due cifre
	ls_char = string(ll_code,"00")
	
	// si costruisce la stringa che rappresenterà il risultato finale dell'algoritmo di crypt
	as_crypted += ls_char
	
next

return 0
end function

public function string encryptserial (string as_to_crypt);// *** stefanop 11/07/2008: Medoto di criptazione basato su una chiave pubblica e una privata.

string 	ls_pubblic_key, ls_nested_key, ls_complete_key
integer 	li_i, li_pos
long 	  	ll_mod_key

if len(trim(as_to_crypt)) < 1 then return "Error: the  key is not valid"


ls_nested_key = string( abs(integer(as_to_crypt) * 127 + integer(len(is_private_key))) ) + as_to_crypt

// Scansiono la tabella delle chiavi da usare per comporre la chiave
for li_i=1 to len(ls_nested_key)
	
	li_pos = integer(mid(ls_nested_key, li_i, 1))
	
	ls_complete_key += upper( mid(is_private_key, li_pos, 1) )
	
next

return ls_complete_key
end function

public function boolean decryptserial (string as_public_key, string as_public_code);// *** stefano 14/07/2008

string ls_client_code, ls_comunicated_code

ls_client_code = encryptserial( as_public_key )

if len(trim(as_public_code)) < 1 then return false

if ls_client_code = as_public_code then	return true

return false
end function

public function string encrypt_data (string as_data);string ls_date, ls_char
integer li_i, li_n

for li_i = 1 to len(as_data) 

	ls_char = mid(as_data, li_i, 1)
	
	if ls_char = '/' then 
		ls_date += '$'
	else
		li_n = integer(ls_char) + 1
		ls_date += mid(is_private_key, li_n, 1)
	end if
	
next

return ls_date
end function

public function string decrypt_data (string as_data);string ls_date, ls_char
integer li_i, li_n

for li_i = 1 to len(as_data) 

	ls_char = mid(as_data, li_i, 1)
	
	if ls_char = '$' then 
		ls_date += '/'
	else
		li_n = pos(is_private_key, ls_char) -1
		ls_date += string(li_n)
	end if
	
next

return ls_date
end function

public function integer uof_pwd_strenght (string as_password);/* EnMe 19-07-2018 
Questa funzione impone le regole di sicurezza per la password.
Ritorna:
0 = validazione non superata
1 = validazione OK
*/
boolean lb_maiuscolo= false, lb_minuscolo=false, lb_numero=false, lb_simbolo=false
string ls_car
long 	ll_i

// per prima cosa verifico la lunghezza della password
if len(as_password) < 8 then return 0

for ll_i = 1 to len(as_password) 
	ls_car=mid(as_password, ll_i, 1)
	if not lb_maiuscolo then
		if pos(is_car_maiuscoli, ls_car) > 0 then lb_maiuscolo = true
	end if
	
	if not lb_minuscolo then
		if pos(is_car_minuscoli, ls_car) > 0 then lb_minuscolo = true
	end if
	
	if not lb_numero then
		if pos(is_car_numeri, ls_car) > 0 then lb_numero = true
	end if
	
	if not lb_simbolo then
		if pos(is_car_simboli, ls_car) > 0 then lb_simbolo = true
	end if

next

if lb_maiuscolo and lb_minuscolo and lb_numero and lb_simbolo then
	return 1
else
	return 0
end if

end function

on n_cst_crypto.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_cst_crypto.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

