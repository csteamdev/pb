﻿$PBExportHeader$n_pc_mb_std.sru
$PBExportComments$MessageBox Defaults Powerclass (copiata da Powerclass)
forward
global type n_pc_mb_std from n_pc_mb_main
end type
end forward

shared variables
STRING	s_ConstMsg[]

end variables

global type n_pc_mb_std from n_pc_mb_main
end type
global n_pc_mb_std n_pc_mb_std

type variables

end variables

on constructor;call n_pc_mb_main::constructor;unsignedlong lul_i


//	set upper element of array to allocate it only once
s_ConstMsg[c_MBI_DW_ReSelectedNone] = ""

//	c_MBC_ValidationError
s_ConstMsg[c_MBI_DOMValError] = "Valore non valido come giorno del mese (%3s) [%7s]."
s_ConstMsg[c_MBI_DOWValError] = "Valore non valido come giorno della settimana (%3s) [%7s]."
s_ConstMsg[c_MBI_DateValError] = "Valore non valido come data (%3s) [%7s]."
s_ConstMsg[c_MBI_DecValError] = "Valore non valido come numero decimale (%3s) [%7s]."
s_ConstMsg[c_MBI_FMTValError] = "Valore non valido rispetto al formato ~"%4s~" [%7s]."
s_ConstMsg[c_MBI_GEValError] = "Valore inferiore a %1d [%7s]."
s_ConstMsg[c_MBI_GTValError] = "Valore inferiore o uguale a %1d [%7s]."
s_ConstMsg[c_MBI_IntValError] = "Valore non valido come intero (%3s) [%7s]."
s_ConstMsg[c_MBI_LEValError] = "Valore superiore a %1d [%7s]."
s_ConstMsg[c_MBI_LTValError] = "Valore superiore o uguale a %1d [%7s]."
s_ConstMsg[c_MBI_LengthValError] = "Il valore deve essere di %1d caratteri [%7s]."
s_ConstMsg[c_MBI_MaxLenValError] = "Il valore deve essere più corto di %1d caratteri [%7s]."
s_ConstMsg[c_MBI_MinLenValError] = "Il valore deve essere di almeno %1d caratteri [%7s]."
s_ConstMsg[c_MBI_MonValError] = "Valore non valido come mese (%3s) [%7s]."
s_ConstMsg[c_MBI_PhoneValError] = "Valore non valido come numero telefonico (%3s) [%7s]."
s_ConstMsg[c_MBI_TimeValError] = "Valore non valido come ora (%3s) [%7s]."
s_ConstMsg[c_MBI_YearValError] = "Valore non valido come anno (%3s) [%7s]."
s_ConstMsg[c_MBI_ZipValError] = "Valore non valido come CAP (%3s) [%7s]."
s_ConstMsg[c_MBI_RequiredField] = "Valore obbligatorio [%7s]."
s_ConstMsg[c_MBI_NonVisRequiredField] = "Un valore non visibile è obbligatorio [%7s]."
s_ConstMsg[c_MBI_OKDevValError] = "%1s [%7s]"
s_ConstMsg[c_MBI_YesNoDevValError] = "%1s [%7s]"
s_ConstMsg[c_MBI_OKFMTDevValError] = "%1s [%7s]"
s_ConstMsg[c_MBI_YesNoFMTDevValError] = "%1s [%7s]"

//	c_MBC_DW_DBError
s_ConstMsg[c_MBI_DW_RetrieveError] = "Si è verificato un errore durante la lettura dei dati."
s_ConstMsg[c_MBI_DW_UpdateError] = "Si è verificato un errore durante il salvataggio dei records modificati.~nProva a correggere l'errore e a ripetere il salvataggio."
s_ConstMsg[c_MBI_DW_CommitError] = "Si è verificato un errore durante il COMMIT delle modifiche apportate.~nProva a correggere l'errore e a ripetere il salvataggio."
s_ConstMsg[c_MBI_DW_LockError] = "Si è verificato un errore durante il LOCK dei records richiesti.~nI records sono disponibili in modalità di Visualizzazione."
s_ConstMsg[c_MBI_DW_RollBackError] = "Si è verificato un errore durante il ROLLBACK delle modifiche apportate."
s_ConstMsg[c_MBI_DW_SaveAfterError] = "Si è verificato un errore dopo il salvataggio dei records modificati.~nProva a correggere l'errore e a ripetere il salvataggio."
s_ConstMsg[c_MBI_DW_SetKeyError] = "Si è verificato un errore durante l'impostazione delle chiavi dei nuovi record.~nProva a correggere l'errore e a ripetere il salvataggio."
s_ConstMsg[c_MBI_DW_CC_ExceededRetry] = "Si è verificato un errore durante l'impostazione delle chiavi dei nuovi record.~nProva a correggere l'errore e a ripetere il salvataggio."

//	c_MBC_DW_OpenError
s_ConstMsg[c_MBI_DW_PickedRowError] = "Si è verificato un errore durante l'apertura di una finestra."

//	c_MBC_DW_ValidationError
s_ConstMsg[c_MBI_DW_DOMValError] = s_ConstMsg[c_MBI_DOMValError]
s_ConstMsg[c_MBI_DW_DOWValError] = s_ConstMsg[c_MBI_DOWValError]
s_ConstMsg[c_MBI_DW_DateValError] = s_ConstMsg[c_MBI_DateValError]
s_ConstMsg[c_MBI_DW_DecValError] = s_ConstMsg[c_MBI_DecValError]
s_ConstMsg[c_MBI_DW_FMTValError] = s_ConstMsg[c_MBI_FMTValError]
s_ConstMsg[c_MBI_DW_GEValError] = s_ConstMsg[c_MBI_GEValError]
s_ConstMsg[c_MBI_DW_GTValError] = s_ConstMsg[c_MBI_GTValError]
s_ConstMsg[c_MBI_DW_IntValError] = s_ConstMsg[c_MBI_IntValError]
s_ConstMsg[c_MBI_DW_LEValError] = s_ConstMsg[c_MBI_LEValError]
s_ConstMsg[c_MBI_DW_LTValError] = s_ConstMsg[c_MBI_LTValError]
s_ConstMsg[c_MBI_DW_LengthValError] = s_ConstMsg[c_MBI_LengthValError]
s_ConstMsg[c_MBI_DW_MaxLenValError] = s_ConstMsg[c_MBI_MaxLenValError]
s_ConstMsg[c_MBI_DW_MinLenValError] = s_ConstMsg[c_MBI_MinLenValError]
s_ConstMsg[c_MBI_DW_MonValError] = s_ConstMsg[c_MBI_MonValError]
s_ConstMsg[c_MBI_DW_PhoneValError] = s_ConstMsg[c_MBI_PhoneValError]
s_ConstMsg[c_MBI_DW_TimeValError] = s_ConstMsg[c_MBI_TimeValError]
s_ConstMsg[c_MBI_DW_YearValError] = s_ConstMsg[c_MBI_YearValError]
s_ConstMsg[c_MBI_DW_ZipValError] = s_ConstMsg[c_MBI_ZipValError]
s_ConstMsg[c_MBI_DW_RequiredField] = s_ConstMsg[c_MBI_RequiredField]
s_ConstMsg[c_MBI_DW_NonVisRequiredField] = s_ConstMsg[c_MBI_NonVisRequiredField]
s_ConstMsg[c_MBI_DW_DWValError] = "%8s [%7s]"
s_ConstMsg[c_MBI_DW_OKDevValError] = "%8s [%7s]"
s_ConstMsg[c_MBI_DW_YesNoDevValError] = "%8s [%7s]"
s_ConstMsg[c_MBI_DW_OKFMTDevValError] = "%8s [%7s]"
s_ConstMsg[c_MBI_DW_YesNoFMTDevValError] = "%8s [%7s]"
s_ConstMsg[c_MBI_DW_PBValidation] = "Errore di validazione [%7s]"

//	c_MBC_DeveloperError
s_ConstMsg[c_MBI_DW_SameParent] = "%1s:~nLa DATAWINDOW '%3s' ha indicato se stessa come parent DATAWINDOW."
s_ConstMsg[c_MBI_DW_BadTransObject] = "%1s:~nImpossibile impostare il TRANSACTION OBJECT per la DATAWINDOW '%3s'."
s_ConstMsg[c_MBI_WindowOpenError] = "Si è verificato un errore durante l'evento '%5s' della WINDOW '%3s'."

//	c_MBC_DW_UserInput
s_ConstMsg[c_MBI_DW_OneHighlightAnyChanges] = "Sono state apportate modifiche nella finestra evidenziata.~nVuoi salvare le modifiche?"
s_ConstMsg[c_MBI_DW_HighlightAnyChanges] = "Sono state apportate modifiche nelle %1d finestre evidenziate.~nVuoi salvare le modifiche?"
s_ConstMsg[c_MBI_DW_OneAnyChanges] = "Sono state apportate modifiche in una finestra.~nVuoi salvare le modifiche?"
s_ConstMsg[c_MBI_DW_AnyChanges] = "Sono state apportate modifiche in %1d finestre.~nVuoi salvare le modifiche?"
s_ConstMsg[c_MBI_DW_OneAskDeleteOk] = "Un record è selezionato per la cancellazione.~nVuoi cancellarlo?"
s_ConstMsg[c_MBI_DW_AskDeleteOk] = "%1d records sono selezionati per la cancellazione.~nVuoi cancellarli?"

//	c_MBC_DW_UserError
s_ConstMsg[c_MBI_DW_DeleteNotAllowed] = "La cancellazione non è consentita in questa finestra."
s_ConstMsg[c_MBI_DW_ZeroToDelete] = "Non vi sono records selezionati per la cancellazione."
s_ConstMsg[c_MBI_DW_QueryNotAllowed] = "La ricerca non è consentita in questa finestra."
s_ConstMsg[c_MBI_DW_OnlyOneNew] = "Le modifiche devono essere salvate prima di inserire un nuovo record."

//	c_MBC_DW_UserInformation
s_ConstMsg[c_MBI_DW_ZeroToDeleteAfterSave] = "Dopo l'aggiornamento non vi sono records da cancellare."
s_ConstMsg[c_MBI_DW_ZeroSearchRows] = "Nessun record soddisfa le condizioni di ricerca."
s_ConstMsg[c_MBI_DW_ReSelectedChanged] = "%1d record(s) erano selezionati prima della lettura.~n%2d record(s) sono selezionati dopo la lettura."
s_ConstMsg[c_MBI_DW_ReSelectedNone] = "Non vi sono records selezionati dopo la lettura.~nSelezionare un record."

for lul_i = c_MBI_DOMValError to c_MBI_DW_ReSelectedNone
	c_MessageText[lul_i] = s_ConstMsg[lul_i]
next

end on

on n_pc_mb_std.create
TriggerEvent( this, "constructor" )
end on

on n_pc_mb_std.destroy
TriggerEvent( this, "destructor" )
end on

