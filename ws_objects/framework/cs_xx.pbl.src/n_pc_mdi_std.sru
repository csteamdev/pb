﻿$PBExportHeader$n_pc_mdi_std.sru
$PBExportComments$Microhelp Defaults Powerclass (copiata da Powerclass)
forward
global type n_pc_mdi_std from n_pc_mdi_main
end type
end forward

shared variables
//	24 == c_MDI_MaxBaseID
STRING	s_ConstMsg[24]

end variables

global type n_pc_mdi_std from n_pc_mdi_main
end type
global n_pc_mdi_std n_pc_mdi_std

on constructor;call n_pc_mdi_main::constructor;long li_i, li_num


PCCA.Application_Obj.MicroHelpDefault = "Pronto."
fu_Push(PCCA.Application_Obj.MicroHelpDefault, c_ShowMark)

s_ConstMsg[c_MDI_Undefined] = "MicroHelp ID non definito (%1d)."
s_ConstMsg[c_MDI_DB_EnterDBMS] = "Fornire le informazioni di connessione al database..."
s_ConstMsg[c_MDI_DB_Connect] = "Connessione al database %1s..."
s_ConstMsg[c_MDI_DW_Append] = "Aggiunta di una nuova riga..."
s_ConstMsg[c_MDI_DW_Delete] = "Cancellazione delle righe selezionate..."
s_ConstMsg[c_MDI_DW_Insert] = "Inserimento di una nuova riga..."
s_ConstMsg[c_MDI_DW_Modify] = "Righe in modalità di Modifica..."
s_ConstMsg[c_MDI_DW_Query] = "Modalità di ricerca..."
s_ConstMsg[c_MDI_DW_RefreshRows] = "Presentazione delle righe..."
s_ConstMsg[c_MDI_DW_Retrieve] = "Lettura dei dati..."
s_ConstMsg[c_MDI_DW_ReselectRows] = "Riselezione delle righe..."
s_ConstMsg[c_MDI_DW_ScrollFirst] = "Posizionamento sulla prima riga..."
s_ConstMsg[c_MDI_DW_ScrollLast] = "Posizionamento sull'ultima riga..."
s_ConstMsg[c_MDI_DW_ScrollNext] = "Posizionamento sulla prossima riga..."
s_ConstMsg[c_MDI_DW_ScrollPrevious] = "Posizionamento sulla riga precedente..."
s_ConstMsg[c_MDI_DW_Search] = "Ricerca dei dati..."
s_ConstMsg[c_MDI_DW_Filter] = "Filtraggio dei dati..."
s_ConstMsg[c_MDI_DW_Validate] = "Controllo dei dati..."
s_ConstMsg[c_MDI_DW_View] = "Righe in modalità di Visualizzazione..."
s_ConstMsg[c_MDI_Close] = "Chiusura..."
s_ConstMsg[c_MDI_Open] = "Apertura di una nuova finestra..."
s_ConstMsg[c_MDI_Print] = "Stampa..."
s_ConstMsg[c_MDI_Save] = "Salvataggio delle modifiche..."
s_ConstMsg[c_MDI_SaveRowsAs] = "Esportazione delle righe..."

li_num = UpperBound(s_ConstMsg)
for li_i = 1 to li_num
	c_MHPromptText[li_i] = s_ConstMsg[li_i]
next
end on

on n_pc_mdi_std.create
TriggerEvent( this, "constructor" )
end on

on n_pc_mdi_std.destroy
TriggerEvent( this, "destructor" )
end on

