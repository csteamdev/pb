﻿$PBExportHeader$nvo_messagebox.sru
forward
global type nvo_messagebox from nonvisualobject
end type
end forward

global type nvo_messagebox from nonvisualobject
end type
global nvo_messagebox nvo_messagebox

type variables
CONSTANT int NONE = -1
CONSTANT int INFORMATION_ICON = 1
CONSTANT int STOP_ICON = 2
CONSTANT int EXCLAMATION_ICON = 3
CONSTANT int QUESTION_ICON  = 4
CONSTANT int SUCCESS_ICON = 5
CONSTANT int WARNING_ICON = 6


private:
	powerobject ipo_null
	int ii_default_button = 1
end variables

forward prototypes
public function integer messagebox (string as_title, string as_text)
public function integer messagebox (string as_title, string as_text, icon aicon_icon)
public function integer messagebox (string as_title, string as_text, icon aicon_icon, button abutt_buttons)
public function integer messagebox (string as_title, string as_text, icon aicon_icon, button abutt_buttons, integer ai_default_button)
public function boolean confirm (string as_title, string as_text)
public subroutine error (string as_title, string as_text)
public function integer messagebox (string as_title, string as_text, icon aicon_icon, button abutt_buttons, boolean ab_tran_info, n_tran atran_tran_object, boolean ab_pb_object_info, powerobject apo_pb_object, boolean ab_systemerror_info, boolean ab_automail)
public function integer show (string as_title, string as_text)
public function integer show (string as_text)
public function integer system_error (string as_title, string as_text)
private function long uf_log (string as_error_message, string as_tipo_log)
public subroutine error (string as_title, string as_text, boolean ab_log)
public subroutine error (string as_text)
public function boolean confirm (string as_title, string as_text, integer ai_default_button)
public function boolean confirm (string as_text)
public function boolean prompt (ref string as_string)
public function boolean prompt (ref string as_string, string as_title, integer ai_limit_string)
public subroutine error (string as_text, ref transaction at_transaction)
public subroutine error (string as_title, string as_text, ref transaction at_transaction)
public subroutine error (string as_title, string as_text, boolean ab_log, ref transaction at_transaction)
public function boolean confirm (string as_text, integer ai_button_default)
public subroutine success (string as_title, string as_text)
public function integer get_icon_index (icon aic_icon)
public function integer messagebox (string as_title, string as_text, integer aicon_icon, button abutt_buttons, boolean ab_tran_info, n_tran atran_tran_object, boolean ab_pb_object_info, powerobject apo_pb_object, boolean ab_systemerror_info, boolean ab_automail)
public subroutine success (string as_text)
public subroutine warning (string as_title, string as_text)
public subroutine warning (string as_text)
end prototypes

public function integer messagebox (string as_title, string as_text);/** DEPRECATE 13/09/2010 **/
return this.messagebox(as_title, as_text, information!, ok!, false, sqlca, false, ipo_null, false, false)

end function

public function integer messagebox (string as_title, string as_text, icon aicon_icon);/** DEPRECATE 13/09/2010 **/
return this.messagebox(as_title,as_text,aicon_icon,ok!,false,sqlca,false,ipo_null,false,false)

end function

public function integer messagebox (string as_title, string as_text, icon aicon_icon, button abutt_buttons);/** DEPRECATE 13/09/2010 **/
return this.messagebox(as_title,as_text,aicon_icon,abutt_buttons,false,sqlca,false,ipo_null,false,false)

end function

public function integer messagebox (string as_title, string as_text, icon aicon_icon, button abutt_buttons, integer ai_default_button);/** DEPRECATE 13/09/2010 **/
return this.messagebox(as_title,as_text,aicon_icon,abutt_buttons,false,sqlca,false,ipo_null,false,false)

end function

public function boolean confirm (string as_title, string as_text);return this.confirm(as_title, as_text, 2)
end function

public subroutine error (string as_title, string as_text);this.error(as_title, as_text, false)
end subroutine

public function integer messagebox (string as_title, string as_text, icon aicon_icon, button abutt_buttons, boolean ab_tran_info, n_tran atran_tran_object, boolean ab_pb_object_info, powerobject apo_pb_object, boolean ab_systemerror_info, boolean ab_automail);long ll_appo

/* --- VERSIONE PRINCIPALE CON TUTTI I PARAMETRI --- */

str_messagebox lstr_messagebox

if isnull(as_title) then	as_title = ""
if isnull(as_text) then	as_text = ""
if isnull(aicon_icon) then	aicon_icon = information!
if isnull(abutt_buttons) then abutt_buttons = ok!
if isnull(atran_tran_object) then atran_tran_object = sqlca
if isnull(ab_systemerror_info) then	ab_systemerror_info = false
if isnull(ab_automail) then ab_automail = false

lstr_messagebox.title = as_title
lstr_messagebox.text = as_text
lstr_messagebox.icon = get_icon_index(aicon_icon)
lstr_messagebox.buttons = abutt_buttons
lstr_messagebox.tran_info = ab_tran_info
lstr_messagebox.tran_object = atran_tran_object
lstr_messagebox.pb_object_info = ab_pb_object_info
lstr_messagebox.pb_object = apo_pb_object
lstr_messagebox.systemerror_info = ab_systemerror_info
lstr_messagebox.automail = ab_automail
lstr_messagebox.default_button = ii_default_button

lstr_messagebox.return_value = -1

//openwithparm(w_messagebox,lstr_messagebox)
openwithparm(w_messagebox_v2, lstr_messagebox)

lstr_messagebox = message.powerobjectparm
setnull(message.powerobjectparm)

ii_default_button = 1

try
	if not isvalid(lstr_messagebox) or isnull(lstr_messagebox) then return 1
	
	return lstr_messagebox.return_value
catch (throwable err)
	f_log_sistema(s_cs_xx.cod_utente,"MSG")
end try

return 1
end function

public function integer show (string as_title, string as_text);return this.messagebox(as_title, as_text, Information!, Ok!, false,sqlca,false,ipo_null,false,false)
end function

public function integer show (string as_text);return this.messagebox("", as_text, None!, Ok!, false,sqlca,false,ipo_null,false,false)
end function

public function integer system_error (string as_title, string as_text);string ls_error_message

// devo raccogliere prima questi dari perchè poi verrebbero sporcati con la query di selezione
ls_error_message = Error.text + " | Object: " +  Error.object + " | Script: " + Error.ObjectEvent + "(" + string(Error.line) + ")"
this.uf_log(ls_error_message, "SE")

return this.messagebox(as_title, as_text, Information!, Ok!, false,sqlca,false,ipo_null,true,true)
end function

private function long uf_log (string as_error_message, string as_tipo_log);string ls_sqlerrtext
//long ll_id_sistema
long ll_sqlcode
datetime ldt_today

// devo raccogliere prima questi dari perchè poi verrebbero sporcati con la query di selezione
ls_sqlerrtext = sqlca.sqlerrtext
ll_sqlcode = sqlca.sqlcode
ldt_today = datetime(today(), now())

//select max(id_log_sistema)
//into :ll_id_sistema
//from log_sistema;
//
//if sqlca.sqlcode < 0 then
//	return -1
//elseif sqlca.sqlcode = 100 or isnull(ll_id_sistema) or ll_id_sistema < 1 then 
//	ll_id_sistema = 0
//end if
//
//ll_id_sistema++

//insert into log_sistema (
//	id_log_sistema,
//	data_registrazione,
//	ora_registrazione,
//	utente,
//	flag_tipo_log,
//	db_sqlcode,
//	db_sqlerrtext,
//	messaggio)
//values (
//	:ll_id_sistema,
//	:ldt_today,
//	:ldt_today,
//	:s_cs_xx.cod_utente,
//	:as_tipo_log,
//	:ll_sqlcode,
//	:ls_sqlerrtext,
//	:as_error_message);
	
//la tabella ha un autoincrement
insert into log_sistema (
	data_registrazione,
	ora_registrazione,
	utente,
	flag_tipo_log,
	db_sqlcode,
	db_sqlerrtext,
	messaggio)
values (
	:ldt_today,
	:ldt_today,
	:s_cs_xx.cod_utente,
	:as_tipo_log,
	:ll_sqlcode,
	:ls_sqlerrtext,
	:as_error_message);

if sqlca.sqlcode <> 0 then
	return -1
else
	return 1 //ll_id_sistema
end if
end function

public subroutine error (string as_title, string as_text, boolean ab_log);/**
 * stefanop
 * 13/09/2010
 *
 * Funzione che salva il messaggio di errore nella tabella e mostra la messagebox
 **/
 
transaction lt_empty_transaction

setnull(lt_empty_transaction)

this.error(as_title, as_text, ab_log, lt_empty_transaction)
end subroutine

public subroutine error (string as_text);this.error("", as_text, false)
end subroutine

public function boolean confirm (string as_title, string as_text, integer ai_default_button);ii_default_button = ai_default_button
if 1 = this.messagebox(as_title, as_text, Question!, YesNo!, false,sqlca,false,ipo_null,false,false) then
	return true
else
	return false
end if
end function

public function boolean confirm (string as_text);return this.confirm("", as_text, 2)
end function

public function boolean prompt (ref string as_string);/**
 * stefanop
 * 11/05/11
 *
 * Visualizza una maschera modale dove l'utente può inserire del testo
 * Ritorna TRUE se l'utente ha premuto su OK; FALSE il contrario
 **/
 
 
return prompt(as_string, "Inserisci testo", 2000)
end function

public function boolean prompt (ref string as_string, string as_title, integer ai_limit_string);/**
 * stefanop
 * 11/05/11
 *
 * Visualizza una maschera modale dove l'utente può inserire del testo
 * Ritorna TRUE se l'utente ha premuto su OK; FALSE il contrario
 *
 * Parametri
 * as_string stringa inserita dall'utente
 * as_title stringa titolo della finestra
 * ai_limit_string integer limite della stringa che l'utente può inserire
 **/
 
 
s_cs_xx_parametri lstr_params

lstr_params.parametro_s_1 = as_string
lstr_params.parametro_s_2 = as_title
lstr_params.parametro_ul_1 = ai_limit_string

openWithParm(w_prompt_mle, lstr_params)

lstr_params = message.powerobjectparm
setnull(message.powerobjectparm)

as_string = lstr_params.parametro_s_1

return lstr_params.parametro_b_1
end function

public subroutine error (string as_text, ref transaction at_transaction);this.error("", as_text, false, at_transaction)
end subroutine

public subroutine error (string as_title, string as_text, ref transaction at_transaction);this.error(as_title, as_text, false, at_transaction)
end subroutine

public subroutine error (string as_title, string as_text, boolean ab_log, ref transaction at_transaction);/**
 * stefanop
 * 13/09/2010
 *
 * Funzione che salva il messaggio di errore nella tabella e mostra la messagebox
 **/
 
if ab_log then
	this.uf_log(as_text, "E")	
end if

// aggiungo in automatico la descrizione in caso di errore
if not isnull(at_transaction) then
	if at_transaction.sqlcode <> 0 and not isnull(at_transaction.sqlerrtext) and at_transaction.sqlerrtext <> "" then
		as_text += "~r~n" + at_transaction.sqlerrtext
	end if
end if
// ----

this.messagebox(as_title, as_text, StopSign!, Ok!, false,sqlca,false,ipo_null,false,false)
end subroutine

public function boolean confirm (string as_text, integer ai_button_default);return this.confirm("", as_text, ai_button_default)
end function

public subroutine success (string as_title, string as_text);this.messagebox(as_title, as_text, SUCCESS_ICON, Ok!, false,sqlca,false,ipo_null,false,false)
end subroutine

public function integer get_icon_index (icon aic_icon);choose case aic_icon
	case Information! 
		return 1
		
	case StopSign!
		return 2
	
	case Exclamation!
		return 3
		
	case Question!
		return 4
		
	case else
		return -1
end choose
end function

public function integer messagebox (string as_title, string as_text, integer aicon_icon, button abutt_buttons, boolean ab_tran_info, n_tran atran_tran_object, boolean ab_pb_object_info, powerobject apo_pb_object, boolean ab_systemerror_info, boolean ab_automail);long ll_appo

/* --- VERSIONE PRINCIPALE CON TUTTI I PARAMETRI --- */

str_messagebox lstr_messagebox

if isnull(as_title) then	as_title = ""
if isnull(as_text) then	as_text = ""
if isnull(aicon_icon) then	aicon_icon = 1
if isnull(abutt_buttons) then abutt_buttons = ok!
if isnull(atran_tran_object) then atran_tran_object = sqlca
if isnull(ab_systemerror_info) then	ab_systemerror_info = false
if isnull(ab_automail) then ab_automail = false

lstr_messagebox.title = as_title
lstr_messagebox.text = as_text
lstr_messagebox.icon = aicon_icon
lstr_messagebox.buttons = abutt_buttons
lstr_messagebox.tran_info = ab_tran_info
lstr_messagebox.tran_object = atran_tran_object
lstr_messagebox.pb_object_info = ab_pb_object_info
lstr_messagebox.pb_object = apo_pb_object
lstr_messagebox.systemerror_info = ab_systemerror_info
lstr_messagebox.automail = ab_automail
lstr_messagebox.default_button = ii_default_button

lstr_messagebox.return_value = -1

//openwithparm(w_messagebox,lstr_messagebox)
openwithparm(w_messagebox_v2, lstr_messagebox)

lstr_messagebox = message.powerobjectparm
setnull(message.powerobjectparm)

ii_default_button = 1

try
	if not isvalid(lstr_messagebox) or isnull(lstr_messagebox) then return 1
	
	return lstr_messagebox.return_value
catch (throwable err)
	f_log_sistema(s_cs_xx.cod_utente,"MSG")
end try

return 1
end function

public subroutine success (string as_text);success("", as_text)
end subroutine

public subroutine warning (string as_title, string as_text);this.messagebox(as_title, as_text, WARNING_ICON, Ok!, false,sqlca,false,ipo_null,false,false)
end subroutine

public subroutine warning (string as_text);warning("", as_text)
end subroutine

on nvo_messagebox.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_messagebox.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;setnull(ipo_null)
end event

