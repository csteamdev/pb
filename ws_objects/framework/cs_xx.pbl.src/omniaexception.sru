﻿$PBExportHeader$omniaexception.sru
forward
global type omniaexception from exception
end type
end forward

global type omniaexception from exception
end type
global omniaexception omniaexception

type variables

end variables

on omniaexception.create
call super::create
TriggerEvent( this, "constructor" )
end on

on omniaexception.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

