﻿$PBExportHeader$sqlexception.sru
forward
global type sqlexception from omniaexception
end type
end forward

global type sqlexception from omniaexception
end type
global sqlexception sqlexception

type variables
private:
	transaction it_transaction
end variables

forward prototypes
public function integer getsqlcode ()
public subroutine initialize (transaction at_transaction)
end prototypes

public function integer getsqlcode ();return it_transaction.sqlcode
end function

public subroutine initialize (transaction at_transaction);it_transaction = at_transaction

setMessage(it_transaction.SQLErrText)
end subroutine

on sqlexception.create
call super::create
end on

on sqlexception.destroy
call super::destroy
end on

