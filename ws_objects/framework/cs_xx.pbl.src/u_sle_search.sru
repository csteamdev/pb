﻿$PBExportHeader$u_sle_search.sru
$PBExportComments$User object that uses a single line edit as a search object
forward
global type u_sle_search from singlelineedit
end type
end forward

global type u_sle_search from singlelineedit
integer width = 800
integer height = 96
integer taborder = 1
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
event po_valempty pbm_custom74
event po_validate pbm_custom75
end type
global u_sle_search u_sle_search

type variables
DATAWINDOW	i_SearchDW

TRANSACTION	i_SearchTransObj
STRING		i_SearchTable
STRING		i_SearchColumn
STRING		i_SearchValue
STRING		i_SearchOriginal

INTEGER		i_SearchError

INTEGER		c_ValOK		= 0
INTEGER		c_ValFailed	= 1

end variables

forward prototypes
public function integer fu_setcode (string default_code)
public function integer fu_wiredw (datawindow search_dw, string search_table, string search_column, transaction search_transobj)
public subroutine fu_unwiredw ()
public function integer fu_buildsearch (boolean search_reset)
end prototypes

on po_valempty;////******************************************************************
////  PO Module     : u_SLE_Search
////  Event         : po_ValEmpty
////  Description   : Provides the opportunity for the developer to
////                  generate errors when the user has not made a
////                  selection in the SLE object.
////
////  Return Value  : c_ValOK     = OK
////                  c_ValFailed = Error, criteria is required.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1994-1995.  All Rights Reserved.
////******************************************************************
//
////-------------------------------------------------------------------
////  Set the i_SearchError variable to c_ValFailed to indicate that
////  this search object is required.
////-------------------------------------------------------------------
//
////SetFocus()
////MessageBox("Search Object Error", &
////           "This is a required search field", &
////           Exclamation!, Ok!)
////i_SearchError = c_ValFailed
end on

on po_validate;////******************************************************************
////  PO Module     : u_SLE_Search
////  Event         : po_Validate
////  Description   : Provides the opportunity for the developer to
////                  write code to validate the fields in this
////                  object.
////
////  Return Value  : INTEGER i_SearchError 
////
////                  If the developer codes the validation 
////                  testing, then the developer should set 
////                  this variable to one of the following
////                  validation return codes:
////
////                  c_ValOK     = The validation test passed and 
////                                the data is to be accepted.
////                  c_ValFailed = The validation test failed and 
////                                the data is to be rejected.  
////                                Do not allow the user to move 
////                                off of this field.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1992-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  The following values are set for the developer before this
////  event is triggered:
////
////      STRING i_SearchValue -
////            The input the user has made.
////------------------------------------------------------------------
//
////i_SearchError = f_PO_ValMaxLen(i_SearchValue, 10, TRUE)

end on

public function integer fu_setcode (string default_code);//******************************************************************
//  PO Module     : u_SLE_Search
//  Subroutine    : fu_SetCode
//  Description   : Sets a default value for this object..
//
//  Parameters    : STRING Default_Code -
//                     The value to set for this object.
//
//  Return Value  : 0 = set OK   -1 = set failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

Text = default_code

RETURN 0
end function

public function integer fu_wiredw (datawindow search_dw, string search_table, string search_column, transaction search_transobj);//******************************************************************
//  PO Module     : u_SLE_Search
//  Subroutine    : fu_WireDW
//  Description   : Wires a column in a DataWindow to this object.
//
//  Parameters    : DATAWINDOW Search_DW -
//                     The DataWindow that is to be wired to
//                     this search object.
//                  STRING Search_Table -
//                     The table in the database that the search
//                     object should build the WHERE clause for.
//                  STRING Search_Column -
//                     The column in the database that the search
//                     object should build the WHERE clause for.
//                  TRANSACTION Search_TransObj - 
//                     Transaction object associated with the
//                     search DataWindow.
//
//  Return Value  : 0 = valid DataWindow   -1 = invalid DataWindow
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Return = -1

//------------------------------------------------------------------
//  Make sure that we were passed a valid DataWindow to be wired.
//------------------------------------------------------------------

IF IsValid(search_dw) THEN

   //---------------------------------------------------------------
   //  Remember the DataWindow tabe and column for searching.
   //---------------------------------------------------------------

   i_SearchDW       = search_dw
   i_SearchTransObj = search_transobj
   i_SearchTable    = search_table
   i_SearchColumn   = search_column
   i_SearchOriginal = i_SearchDW.Describe("datawindow.table.select")
   Enabled          = TRUE
   l_Return         = 0

END IF

RETURN l_Return
end function

public subroutine fu_unwiredw ();//******************************************************************
//  PO Module     : u_SLE_Search
//  Subroutine    : fu_UnwireDW
//  Description   : Un-wires a DataWindow from the search object.
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

SetNull(i_SearchDW)
Enabled = FALSE
end subroutine

public function integer fu_buildsearch (boolean search_reset);//******************************************************************
//  PO Module     : u_SLE_Search
//  Subroutine    : fu_BuildSearch
//  Description   : Extends the WHERE clause of a SQL Select 
//                  with the values in this search object.
//
//  Parameters    : BOOLEAN Search_Reset -
//                     Should the SQL Select be reset to its
//                     original state before the building begins.
//
//  Return Value  : 0 = build OK   -1 = validation error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

N_PO_DATE_STD l_DateObj
BOOLEAN  l_Date, l_First
INTEGER  l_GroupByPos, l_OrderByPos, l_Idx, l_Pos
STRING   l_Operator, l_Text, l_ColumnType, l_Concat, l_NewSelect
STRING   l_GroupBy, l_OrderBy, l_Quotes, l_Other, l_Block, l_Not
STRING   l_Lower, l_Upper, l_Value, l_CC, l_Comma, l_DateFormat

//------------------------------------------------------------------
//  See if the DataWindow search criteria needs to be reset to its
//  original syntax.
//------------------------------------------------------------------

IF search_reset THEN
   i_SearchDW.Modify('datawindow.table.select="' + &
                     i_SearchOriginal + '"')
END IF

l_NewSelect = i_SearchDW.Describe("datawindow.table.select")
l_Text      = TRIM(Text)

//------------------------------------------------------------------
//  If this object has been un-wired, don't include in build.
//------------------------------------------------------------------

IF IsNull(i_SearchDW) THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  If the SQL statement for this DataWindow does not currently
//  have a WHERE clause, then we need to add one.  If it already
//  does have a WHERE clause, we will just concatenate additional
//  criteria to it.
//------------------------------------------------------------------

IF Pos(Upper(l_NewSelect), "WHERE") = 0 THEN
   l_Concat = " WHERE "
ELSE
   l_Concat = " AND "
END IF

//------------------------------------------------------------------
//  See if we have an "GROUP BY" clause and remove it from the
//  select statement for the time being.
//------------------------------------------------------------------

l_GroupBy    = ""
l_GroupByPos = Pos(Upper(l_NewSelect), "GROUP BY")
IF l_GroupByPos > 0 THEN
   l_GroupBy   = " " + Mid(l_NewSelect, l_GroupByPos)
   l_NewSelect = Mid(l_NewSelect, 1, l_GroupByPos - 1)
ELSE

   //---------------------------------------------------------------
   //  See if we have an "ORDER BY" clause and remove it from the
   //  select statement for the time being.
   //---------------------------------------------------------------

   l_OrderBy    = ""
   l_OrderByPos = Pos(Upper(l_NewSelect), "ORDER BY")
   IF l_OrderByPos > 0 THEN
      l_OrderBy   = " " + Mid(l_NewSelect, l_OrderByPos)
      l_NewSelect = Mid(l_NewSelect, 1, l_OrderByPos - 1)
   END IF
END IF

//------------------------------------------------------------------
//  Check to see if the developer made this a required field for
//  search criteria.
//------------------------------------------------------------------

IF l_Text = "" THEN
   i_SearchError = c_ValOK
   i_SearchValue = l_Text
   TriggerEvent("po_ValEmpty")

   //---------------------------------------------------------------
   //  If we get a validation error, abort the search.
   //---------------------------------------------------------------

   IF i_SearchError = c_ValFailed THEN
      SetFocus()
      RETURN -1
   ELSE
      RETURN 0
   END IF
END IF

//------------------------------------------------------------------
//  If the user has typed something into the object, we need to 
//  validate it and add it to our new select statement
//------------------------------------------------------------------

l_Date   = FALSE
l_Quotes = ""

//------------------------------------------------------------------
//  See what type of data this search object contains.
//------------------------------------------------------------------

l_ColumnType = Upper(i_SearchDW.Describe(i_SearchColumn + ".ColType"))
IF Left(l_ColumnType, 3) = "DEC" THEN
   l_ColumnType = "NUMBER"
END IF

CHOOSE CASE l_ColumnType
   CASE "NUMBER"

      //------------------------------------------------------------
      //  If we have a DATE type and the format is not
      //  specified in the column, then use the short format.
      //------------------------------------------------------------

   CASE "DATE", "DATETIME"
      l_DateObj = CREATE n_po_date_std
      l_Date = TRUE
      l_DateFormat = i_SearchDW.Describe(i_SearchColumn + ".Edit.Format")
      IF l_DateFormat = "" OR l_DateFormat = "?" THEN
         l_DateFormat = "d/m/yy"
      END IF
 
   CASE "TIME"

      //------------------------------------------------------------
      //  We have a data type that needs to be quoted 
      //  (e.g. STRING).
      //------------------------------------------------------------

   CASE ELSE
      l_Quotes = "'"
END CHOOSE

//------------------------------------------------------------------
//  Parse the value that the user entered and pass each unrecognized
//  token through validation.
//------------------------------------------------------------------

l_Other = ""
l_First = TRUE

DO WHILE l_Text <> ""

   //---------------------------------------------------------------
   //  Look for an "OR" command and strip it off.
   //---------------------------------------------------------------

   IF Pos(l_Text, "|") > 0 AND &
      (Pos(l_Text, "~~|") = 0 OR &
      Pos(l_Text, "~~|") > Pos(l_Text, "|")) THEN
      l_Block = Trim(Mid(l_Text, 1, Pos(l_Text, "|") - 1))
      l_Text  = Trim(Mid(l_Text, Pos(l_Text, "|") + 1))
      l_CC    = " OR  "

   ELSEIF Pos(Upper(l_Text), " OR ") > 0 THEN
      l_Block = Trim(Mid(l_Text, 1, Pos(Upper(l_Text), " OR ") - 1))
      l_Text  = Trim(Mid(l_Text, Pos(Upper(l_Text), " OR ") + 4))
      l_CC    = " OR  "

   //---------------------------------------------------------------
   //  Look for an "AND" command and strip it off.
   //---------------------------------------------------------------

   ELSEIF Pos(l_Text, "&") > 0 AND &
      (Pos(l_Text, "~~&") = 0 OR &
      Pos(l_Text, "~~&") > Pos(l_Text, "&")) THEN
      l_Block = Trim(Mid(l_Text, 1, Pos(l_Text, "&") - 1))
      l_Text  = Trim(Mid(l_Text, Pos(l_Text, "&") + 1))
      l_CC    = " OR "

   ELSEIF Pos(Upper(l_Text), " AND ") > 0 THEN
      l_Block = Trim(Mid(l_Text, 1, Pos(Upper(l_Text), " AND ") - 1))
      l_Text  = Trim(Mid(l_Text, Pos(Upper(l_Text), " AND ") + 5))
      l_CC    = " OR "

   //---------------------------------------------------------------
   //  Look for a "," command and strip it off.
   //---------------------------------------------------------------

   ELSEIF Pos(l_Text, ",") > 0 AND &
      (Pos(l_Text, "~~,") = 0 OR &
      Pos(l_Text, "~~,") > Pos(l_Text, ",")) THEN
      l_Block = Trim(Mid(l_Text, 1, Pos(l_Text, ",") - 1))
      l_Text  = Trim(Mid(l_Text, Pos(l_Text, ",") + 1))
      l_CC    = " OR "
      
   ELSE

      //------------------------------------------------------------
      //  We're not sure what it is.  Could be a "NOT" or a "TO" 
      //  or a...?
      //------------------------------------------------------------

      l_Block = Trim(l_Text)
      l_Text  = ""
      IF Pos(Lower(l_Block), "not ") = 1 THEN
         l_CC = " AND "
      ELSE
         l_CC = " OR  "
      END IF
   END IF

   //---------------------------------------------------------------
   //  Look for any escape characters and strip it off.  Typically
   //  they would be around the keywords &, and, |, or.
   //---------------------------------------------------------------

   DO
      l_Pos = Pos(l_Block, "~~")
      IF l_Pos > 0 THEN
         l_Block = Replace(l_Block, l_Pos, 1, "")
		END IF
   LOOP UNTIL l_Pos = 0

   //---------------------------------------------------------------
   //  Look for an "NOT" command and strip it off.
   //---------------------------------------------------------------

   l_Not = ""
   IF Pos(Lower(l_Block), "not ") = 1 THEN
      l_Not = "NOT"
      l_Block = Mid(Trim(l_Block), 5)
      IF l_Block = "" THEN EXIT
   END IF

   //---------------------------------------------------------------
   //  If this is the first token then clear our conditional 
   //  operator.
   //---------------------------------------------------------------

   IF l_First THEN
      l_CC    = ""
      l_First = FALSE
   END IF

   //---------------------------------------------------------------
   //  Look for an "TO" command.
   //---------------------------------------------------------------

   IF Pos(Lower(l_Block), " to ") > 0 THEN

      //------------------------------------------------------------
      //  We found a "TO" command.  Grab the token preceeding the 
      //  "TO" and validate it.
      //------------------------------------------------------------

      l_Lower = Mid(l_Block, 1, Pos(Lower(l_Block), " to ") - 1)

      //------------------------------------------------------------
      //  Stuff the current token in and perform valdiation.
      //------------------------------------------------------------

      i_SearchValue = l_Lower
      i_SearchError = c_ValOK
      TriggerEvent("po_Validate")

      //------------------------------------------------------------
      //  If we get a validation error, abort the search.
      //------------------------------------------------------------

      IF i_SearchError = c_ValFailed THEN
         SetFocus()
         RETURN -1
      END IF

      //------------------------------------------------------------
      //  If we got to here, validation must have been successful. 
      //  Validation may have modified the token.  Grab it back.
      //------------------------------------------------------------

      l_Lower = i_SearchValue

      //------------------------------------------------------------
      //  Grab the token that comes after the "TO" and validate it.
      //------------------------------------------------------------

      l_Upper = Trim(Mid(l_Block, Pos(Lower(l_Block), " to ") + 4))

      //------------------------------------------------------------
      //  Stuff the current token in and perform valdiation.
      //------------------------------------------------------------

      i_SearchValue = l_Upper
      i_SearchError = c_ValOK
      TriggerEvent("po_Validate")

      //------------------------------------------------------------
      //  If we get a validation error, abort the search.
      //------------------------------------------------------------

      IF i_SearchError = c_ValFailed THEN
         SetFocus()
         RETURN -1
      END IF

      //------------------------------------------------------------
      //  If we got to here, validation must have been successful. 
      //  Validation may have modified the token.  Grab it back.
      //------------------------------------------------------------

      l_Upper = i_SearchValue

      //------------------------------------------------------------
      //  If the data is of type DATE, then we need to convert the 
      //  DATE formats to a format that will be accepted by the 
      //  database that we are connected to.
      //------------------------------------------------------------

      IF l_Date THEN
         l_Lower = l_DateObj.fu_SetDateDB(i_SearchTable + "." + &
                                          i_SearchColumn,       &
                                          l_Lower,              &
                                          l_DateFormat,         &
                                          " >= ",               &
                                          i_SearchTransObj)
         l_Upper = l_DateObj.fu_SetDateDB(i_SearchTable + "." + &
                                          i_SearchColumn,       &
                                          l_Upper,              &
                                          l_DateFormat,         &
                                          " <= ",               &
                                          i_SearchTransObj)

         //---------------------------------------------------------
         //  Convert the "TO" command to a recognizable SQL 
         //  statement.
         //---------------------------------------------------------

         l_Other = l_Other + l_CC + l_Not + "(" + &
                             l_Lower + " AND " + l_Upper + ")"
      ELSE

         //---------------------------------------------------------
         //  Convert the "TO" command to a recognizable SQL 
         //  statement.
         //---------------------------------------------------------

         l_Other = l_Other + l_CC + i_SearchTable + "." + &
                             i_SearchColumn +  " " + l_Not + &
                             " BETWEEN " + l_Quotes + &
                             l_Lower + l_Quotes + " AND " + &
                             l_Quotes + l_Upper + l_Quotes
      END IF
 
   ELSE

      //------------------------------------------------------------
      //  Well, we don't have a "TO" command.  See if we have some 
      //  other sort of operator.
      //------------------------------------------------------------

      IF Pos(l_Block, ">=") = 1 THEN
         l_Value = Trim(Mid(l_Block, 3))
         IF l_Not = "NOT" THEN
            l_Operator = " < "
         ELSE
            l_Operator = " >= "
         END IF
      ELSEIF Pos(l_Block, "<=") = 1 THEN
         l_Value = Trim(Mid(l_Block, 3))
         IF l_Not = "NOT" THEN
            l_Operator = " > "
         ELSE
            l_Operator = " <= "
         END IF
      ELSEIF Pos(l_Block, ">") = 1 THEN
         l_Value = Trim(Mid(l_Block, 2))
         IF l_Not = "NOT" THEN
            l_Operator = " <= "
         ELSE
            l_Operator = " > "
         END IF
      ELSEIF Pos(l_Block, "<") = 1 THEN
         l_Value = Trim(Mid(l_Block, 2))
         IF l_Not = "NOT" THEN
            l_Operator = " >= "
         ELSE
            l_Operator = " < "
         END IF
      ELSE
         l_Value = l_Block
         IF l_Not = "NOT" THEN
            IF Pos(l_Value, "*") > 0 OR &
               Pos(l_Value, "%") > 0 THEN
               l_Operator = " NOT LIKE "
            ELSE
               l_Operator = " <> "
            END IF
         ELSE
            IF Pos(l_Value, "*") > 0 OR &
               Pos(l_Value, "%") > 0 THEN
               l_Operator = " LIKE "
            ELSE
               l_Operator = " = "
            END IF
         END IF
      END IF

      //------------------------------------------------------------
      //  Stuff the current token in and perform valdiation.
      //------------------------------------------------------------

      i_SearchValue = l_Value
      i_SearchError = c_ValOK
      TriggerEvent("po_Validate")

      //------------------------------------------------------------
      //  If we get a validation error, abort the search.
      //------------------------------------------------------------

      IF i_SearchError = c_ValFailed THEN
         SetFocus()
         RETURN -1
      END IF

      //------------------------------------------------------------
      //  If we got to here, validation must have been successful. 
      //  Validation may have modified the token.  Grab it back.
      //------------------------------------------------------------

      l_Value = i_SearchValue

      //------------------------------------------------------------
      //  If the data is of type DATE, then we need to convert the 
      //  DATE formats to a format that will be accepted by the 
      //  database that we are connected to.
      //------------------------------------------------------------

      IF l_Date THEN
         l_Value = l_DateObj.fu_SetDateDB(i_SearchTable + "." + &
                                          i_SearchColumn,       &
                                          l_Value,              &
                                          l_DateFormat,         &
                                          l_Operator,           &
                                          i_SearchTransObj)

         //---------------------------------------------------------
         //  Convert the operator command to a recognizable SQL 
         //  statement.
         //---------------------------------------------------------

         l_Other = l_Other + l_CC + l_Value

      ELSE

         //---------------------------------------------------------
         //  Convert the operator command to a recognizable SQL 
         //  statement.
         //---------------------------------------------------------

         l_Other = l_Other + l_CC + i_SearchTable + "." + &
                             i_SearchColumn + &
                             l_Operator + l_Quotes + &
                             l_Value    + l_Quotes
      END IF
   END IF
LOOP

//------------------------------------------------------------------
//  If we found a valid where clause, concatenate it onto the new 
//  SQL statement that we are building.
//------------------------------------------------------------------

IF Len(l_Other) > 0 THEN
   l_NewSelect = l_NewSelect + l_Concat + "(" + l_Other + ")"
END IF

//------------------------------------------------------------------
//  Stuff the parameter with the completed SQL statement.
//------------------------------------------------------------------

IF IsValid(l_DateObj) THEN
   DESTROY l_DateObj
END IF

l_NewSelect = "datawindow.table.select='" + &
              w_POManager_STD.MB.fu_QuoteChar(l_NewSelect, "'") + &
              w_POManager_STD.MB.fu_QuoteChar(l_GroupBy, "'")   + &
              w_POManager_STD.MB.fu_QuoteChar(l_OrderBy, "'")   + "'"
i_SearchDW.Modify(l_NewSelect)

RETURN 0
end function

on constructor;//******************************************************************
//  PO Module     : u_SLE_Search
//  Event         : Constructor
//  Description   : Initializes the Search object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Identify this object as a PowerObject Search.
//------------------------------------------------------------------

Tag = "PowerObject Search SLE"
Enabled  = FALSE
SetNull(i_SearchDW)
end on

on u_sle_search.create
end on

on u_sle_search.destroy
end on

