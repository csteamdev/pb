﻿$PBExportHeader$u_style_bar.sru
$PBExportComments$Oggetto Barra Stili (Font, tipo di carattere, altezza, allineamento)
forward
global type u_style_bar from UserObject
end type
type ddlb_font from dropdownlistbox within u_style_bar
end type
type ddlb_size from dropdownlistbox within u_style_bar
end type
type p_underline from picture within u_style_bar
end type
type p_italic from picture within u_style_bar
end type
type p_bold from picture within u_style_bar
end type
type p_right from picture within u_style_bar
end type
type p_center from picture within u_style_bar
end type
type p_left from picture within u_style_bar
end type
type ddlb_border from dropdownlistbox within u_style_bar
end type
type sle_text from singlelineedit within u_style_bar
end type
end forward

global type u_style_bar from UserObject
int Width=2652
int Height=133
boolean Border=true
long BackColor=78682240
long PictureMaskColor=25166016
long TabTextColor=33554432
long TabBackColor=67108864
event ue_style_changed pbm_custom01
ddlb_font ddlb_font
ddlb_size ddlb_size
p_underline p_underline
p_italic p_italic
p_bold p_bold
p_right p_right
p_center p_center
p_left p_left
ddlb_border ddlb_border
sle_text sle_text
end type
global u_style_bar u_style_bar

type variables
string is_alignment
string is_type_change
end variables

forward prototypes
public subroutine uf_load_true_type_size ()
public function string uf_get_style (ref object_style fos_style)
public subroutine uf_set_style (object_style fos_style)
end prototypes

public subroutine uf_load_true_type_size ();int li_i

ddlb_size.AddItem (String(li_i," 8"))
ddlb_size.AddItem (String(li_i," 9"))

For li_i = 10 to 12
	ddlb_size.AddItem (String(li_i,"##"))
Next
For li_i = 14 to 72 step 2
	ddlb_size.AddItem (String(li_i,"##"))
Next

end subroutine

public function string uf_get_style (ref object_style fos_style);fos_style.text = sle_text.text

choose case ddlb_border.text
	case "None"
		fos_style.border = "0"
	case "Shadow Box"
		fos_style.border = "1"
	case "Box"
		fos_style.border = "2"
	case "Underline"
		fos_style.border = "4"
	case "3D Lowered"
		fos_style.border = "5"
	case "3D Raised"
		fos_style.border = "6"
	case "Resize"
		fos_style.border = "3"
end choose

fos_style.font_face = ddlb_font.text
fos_style.font_height = ddlb_size.text

if p_bold.picturename = s_cs_xx.volume + s_cs_xx.risorse+"pb_b_dn.bmp" then
	fos_style.font_weight = "700"
else
	fos_style.font_weight = "400"
end if

if p_italic.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_i_dn.bmp" then
	fos_style.italic = "1"
else
	fos_style.italic = "0"
end if

if p_underline.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_u_dn.bmp" then
	fos_style.underline = "1"
else
	fos_style.underline = "0"
end if

fos_style.alignment = is_alignment
return is_type_change
end function

public subroutine uf_set_style (object_style fos_style);string ls_text

if fos_style.text = "" then
	hide (sle_text)
else
	sle_text.text = fos_style.text
	show (sle_text)
end if

choose case fos_style.border
	case "0"
		ls_text = "None"
	case "1"
		ls_text = "Shadow Box"
	case "2"
		ls_text = "Box"
	case "4"
		ls_text = "Underline"
	case "5"
		ls_text = "3D Lowered"
	case "6"
		ls_text = "3D Raised"
end choose
if ddlb_border.text <> ls_text then ddlb_border.text = ls_text

if ddlb_font.text <> fos_style.font_face then
	ddlb_font.text = fos_style.font_face
	Triggerevent (ddlb_font, selectionchanged!)
end if

if fos_style.font_height <> "0" then
	if ddlb_size.text <> fos_style.font_height then ddlb_size.text = fos_style.font_height
end if

if fos_style.font_weight = "400" then
	ls_text = s_cs_xx.volume + s_cs_xx.risorse + "pb_b_up.bmp"
else
	ls_text = s_cs_xx.volume + s_cs_xx.risorse + "pb_b_dn.bmp"
end if
if p_bold.picturename <> ls_text then p_bold.picturename = ls_text

if fos_style.italic = "1" then
	ls_text = s_cs_xx.volume + s_cs_xx.risorse + "pb_i_dn.bmp"
else
	ls_text = s_cs_xx.volume + s_cs_xx.risorse + "pb_i_up.bmp"
end if
if p_italic.picturename <> ls_text then p_italic.picturename = ls_text

if fos_style.underline = "1" then
	ls_text = s_cs_xx.volume + s_cs_xx.risorse + "pb_u_dn.bmp"
else
	ls_text = s_cs_xx.volume + s_cs_xx.risorse + "pb_u_up.bmp"
end if
if p_underline.picturename <> ls_text then p_underline.picturename = ls_text

choose case fos_style.alignment
	case "0"
		if p_left.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_l_up.bmp" then
			p_left.picturename  = s_cs_xx.volume + s_cs_xx.risorse + "pb_l_dn.bmp"
			p_center.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_c_up.bmp"
			p_right.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_r_up.bmp"
		end if
	case "1"
		if p_right.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_r_up.bmp" then
			p_left.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_l_up.bmp"
			p_center.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_c_up.bmp"
			p_right.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_r_dn.bmp"
		end if
	case "2"
		if p_center.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_c_up.bmp" then
			p_left.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_l_up.bmp"
			p_center.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_c_dn.bmp"
			p_right.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_r_up.bmp"
		end if
end choose

is_alignment = fos_style.alignment

end subroutine

on u_style_bar.create
this.ddlb_font=create ddlb_font
this.ddlb_size=create ddlb_size
this.p_underline=create p_underline
this.p_italic=create p_italic
this.p_bold=create p_bold
this.p_right=create p_right
this.p_center=create p_center
this.p_left=create p_left
this.ddlb_border=create ddlb_border
this.sle_text=create sle_text
this.Control[]={ this.ddlb_font,&
this.ddlb_size,&
this.p_underline,&
this.p_italic,&
this.p_bold,&
this.p_right,&
this.p_center,&
this.p_left,&
this.ddlb_border,&
this.sle_text}
end on

on u_style_bar.destroy
destroy(this.ddlb_font)
destroy(this.ddlb_size)
destroy(this.p_underline)
destroy(this.p_italic)
destroy(this.p_bold)
destroy(this.p_right)
destroy(this.p_center)
destroy(this.p_left)
destroy(this.ddlb_border)
destroy(this.sle_text)
end on

type ddlb_font from dropdownlistbox within u_style_bar
int X=1102
int Y=21
int Width=622
int Height=325
int TabOrder=30
string Text="System"
boolean Sorted=false
boolean VScrollBar=true
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
string Item[]={"Arial",&
"Courier New",&
"System",&
"Times New Roman"}
end type

on selectionchanged;//Selectionchanged script for ddlb_font

string ls_hold 

ls_hold = ddlb_size.text

ddlb_size.Reset( ) 

choose case this.text
	case "Arial"
		uf_load_true_type_size()
	case "Courier New"
		uf_load_true_type_size()		
	case "System"
		ddlb_size.AddItem(" 9")
		ddlb_size.AddItem("10")
	case "Times New Roman"
		uf_load_true_type_size()
end choose

ddlb_size.text = ls_hold

is_type_change = "Font"
parent.triggerevent("ue_style_changed")
end on

type ddlb_size from dropdownlistbox within u_style_bar
int X=1738
int Y=21
int Width=238
int Height=353
int TabOrder=40
string Text="15"
boolean VScrollBar=true
boolean AllowEdit=true
long TextColor=33554432
long BackColor=78682240
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on modified;is_type_change = "Size"
parent.triggerevent("ue_style_changed")
end on

type p_underline from picture within u_style_bar
int X=2199
int Y=21
int Width=92
int Height=81
int TabOrder=70
end type

event clicked;//Clicked script for p_underline

if this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_u_up.bmp" then
	this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_u_dn.bmp"
else
	this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_u_up.bmp"
end if

is_type_change = "underline"
parent.triggerevent("ue_style_changed")
end event

event constructor;this.picturename=s_cs_xx.volume + s_cs_xx.risorse + "pb_u_up.bmp"
end event

type p_italic from picture within u_style_bar
int X=2108
int Y=21
int Width=92
int Height=81
int TabOrder=60
end type

event clicked;//Clicked script for p_italic

if this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_i_up.bmp" then
	this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_i_dn.bmp"
else
	this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_i_up.bmp"
end if

is_type_change = "italic"
parent.triggerevent("ue_style_changed")
end event

event constructor;this.picturename=s_cs_xx.volume + s_cs_xx.risorse + "pb_i_up.bmp"
end event

type p_bold from picture within u_style_bar
int X=2021
int Y=21
int Width=92
int Height=81
int TabOrder=50
end type

event clicked;//Clicked script for p_bold

if this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_b_up.bmp" then
	this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_b_dn.bmp"
else
	this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_b_up.bmp"
end if

is_type_change = "Bold"
parent.triggerevent("ue_style_changed")
end event

event constructor;this.picturename=s_cs_xx.volume + s_cs_xx.risorse + "pb_b_up.bmp"
end event

type p_right from picture within u_style_bar
int X=2519
int Y=21
int Width=92
int Height=81
int TabOrder=100
end type

event clicked;//Clicked script for p_right

is_alignment = "1"
this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_r_dn.bmp"
p_center.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_c_up.bmp"
p_left.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_l_up.bmp"

is_type_change = "right"
parent.triggerevent("ue_style_changed")

end event

event constructor;this.picturename=s_cs_xx.volume + s_cs_xx.risorse + "pb_r_up.bmp"
end event

type p_center from picture within u_style_bar
int X=2433
int Y=21
int Width=92
int Height=81
int TabOrder=90
end type

event clicked;//Clicked script for p_center

is_alignment = "2"
this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_c_dn.bmp"
p_left.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_l_up.bmp"
p_right.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_r_up.bmp"

is_type_change = "center"
parent.triggerevent("ue_style_changed")

end event

event constructor;this.picturename=s_cs_xx.volume + s_cs_xx.risorse + "pb_c_up.bmp"
end event

type p_left from picture within u_style_bar
int X=2346
int Y=21
int Width=92
int Height=81
int TabOrder=80
end type

event clicked;//Clicked script for p_left

is_alignment = "0"
this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_l_dn.bmp"
p_center.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_c_up.bmp"
p_right.picturename = s_cs_xx.volume + s_cs_xx.risorse + "pb_r_up.bmp"


is_type_change = "left"
parent.triggerevent("ue_style_changed")
end event

event constructor;this.picturename=s_cs_xx.volume + s_cs_xx.risorse + "pb_l_up.bmp"
end event

type ddlb_border from dropdownlistbox within u_style_bar
int X=618
int Y=21
int Width=467
int Height=325
int TabOrder=20
string Text="None"
boolean VScrollBar=true
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
string Item[]={"None",&
"Shadow Box",&
"Box",&
"3D Lowered",&
"3D Raised",&
"Resize"}
end type

on selectionchanged;is_type_change = "border"

parent.triggerevent("ue_style_changed")
end on

type sle_text from singlelineedit within u_style_bar
int X=19
int Y=21
int Width=577
int Height=81
int TabOrder=10
boolean AutoHScroll=false
string Pointer="arrow!"
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on modified;is_type_change = "text"
parent.triggerevent("ue_style_changed")
end on

