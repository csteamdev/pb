﻿$PBExportHeader$uo_7z.sru
forward
global type uo_7z from nonvisualobject
end type
end forward

global type uo_7z from nonvisualobject
end type
global uo_7z uo_7z

type variables
constant int RETURN_SUCCES = 0
constant int RETURN_WARNING = 1
constant int RETURN_FATAL_ERROR = 2
constant int RETURN_COMMAND_LINE_ERROR = 7
constant int RETURN_MEMORY_ERROR = 8
constant int RETURN_EMPTY_FILE_LIST = 9
constant int RETURN_USER_STOP = 255

private:
	// indica se è presente l'.exe da lanciare per zippare o dezippare
	boolean ib_7z = false
	
	// percorso per l'eseguibile
	string is_7z_exe
	
	// l'ultimo messaggio di errore
	string is_error
	
	// lista dei file da zippare in uno unico
	string is_file_unzipped[]
end variables

forward prototypes
public function string uof_get_error ()
public function boolean uof_add_file (string as_filename)
public function boolean uof_has_unzipped_file (string as_file_name)
public function boolean uof_remove_file (string as_filename)
private function boolean uof_has_unzipped_file (string as_file_name, ref integer ai_position)
public function boolean uof_remove_all_file (string as_filename)
public function boolean uof_set_7z_path (string as_7z_path)
public function boolean uof_add_folder (string as_folder)
public function boolean uof_add_folder (string as_folder, boolean ab_include_subfolder)
public function boolean uof_zip (string as_zipped_file)
public function boolean uof_unzip (string as_zipped_file, string as_folder)
private function boolean uof_execute (string as_command)
public function boolean uof_zip (string as_zipped_file, string as_files_folders[])
end prototypes

public function string uof_get_error ();/**
 * stefanop
 * 24/11/2011
 *
 * Ritorno il messaggio di errore
 **/

return is_error
end function

public function boolean uof_add_file (string as_filename);/**
 * stefanop
 * 24/11/2011
 *
 * Aggiungo un file alla lista dei file da zippare
 **/
 
if fileexists(as_filename) then
	
	if not uof_has_unzipped_file(as_filename) then 
		is_file_unzipped[upperbound(is_file_unzipped) + 1] = as_filename
	end if
	
	setnull(is_error)
	return true
else
	
	is_error = "Il file " + as_filename + " non esiste o non è accessibile."
	return false
end if


end function

public function boolean uof_has_unzipped_file (string as_file_name);/**
 * stefanop
 * 24/11/2011
 *
 * Controllo se il file è incluso nell'array dei file unzippati
 **/
 
int li_i

return uof_has_unzipped_file(as_file_name, li_i)
end function

public function boolean uof_remove_file (string as_filename);/**
 * stefanop
 * 24/11/2011
 *
 * Rimuovo un file alla lista dei file da zippare
 **/
 
int li_position

if uof_has_unzipped_file(as_filename, li_position) then 
	is_file_unzipped[li_position] = ""
end if

return true


end function

private function boolean uof_has_unzipped_file (string as_file_name, ref integer ai_position);/**
 * stefanop
 * 24/11/2011
 *
 * Controllo se il file è incluso nell'array dei file unzippati
 **/
 
int li_i

for li_i = 1 to upperbound(is_file_unzipped)
	
	if is_file_unzipped[li_i] = as_file_name then 
		ai_position = li_i
		return true
	end if
	
next

return false
end function

public function boolean uof_remove_all_file (string as_filename);/**
 * stefanop
 * 24/11/2011
 *
 * Rimuovo tutti file alla lista dei file da zippare
 **/
 
string ls_empty[]

is_file_unzipped = ls_empty

return true
end function

public function boolean uof_set_7z_path (string as_7z_path);/**
* stefanop
* 25/11/2011
*
* Imposto il percorso dell'eseguibile di 7z.
* L'oggetto imposta automaticamente la directory con il percorso delle risorse durante 
* la creazione (constructor)
**/


if not fileexists(as_7z_path) then
	is_error = "Il file " + as_7z_path + " non è stato trovato!"
	ib_7z = false
else
	is_7z_exe = as_7z_path
	ib_7z = true
end if

return ib_7z
end function

public function boolean uof_add_folder (string as_folder);/**
 * stefanop
 * 24/11/2011
 *
 * Aggiungo un file alla lista dei file da zippare
 **/
 
return uof_add_folder(as_folder, true)


end function

public function boolean uof_add_folder (string as_folder, boolean ab_include_subfolder);/**
 * stefanop
 * 24/11/2011
 *
 * Aggiungo un file alla lista dei file da zippare
 **/

if directoryexists(as_folder) then
	
	if not uof_has_unzipped_file(as_folder) then 
		is_file_unzipped[upperbound(is_file_unzipped) + 1] = as_folder
	end if
	
	setnull(is_error)
	return true
else
	
	is_error = "La cartella " + as_folder + " non esiste o non è accessibile."
	return false
end if


end function

public function boolean uof_zip (string as_zipped_file);/**
 * stefanop
 * 24/11/2011
 *
 * Comprimo tutta la lista dei file in uno file zip unico
 **/

return uof_zip(as_zipped_file, is_file_unzipped)
end function

public function boolean uof_unzip (string as_zipped_file, string as_folder);/**
 * stefanop
 * 24/11/2011
 *
 * Decomprimo il file zip all'interno di una cartella che passo per parametro
 **/

string ls_run, ls_files
long ll_return
uo_shellexecute luo_shell

if not ib_7z then return false

setnull(is_error)

// controllo esistenza file zip
if not fileexists(as_zipped_file) then
	is_error = "Il file " + as_zipped_file + " non esiste!"
	return false
end if

// controllo esistenza cartella e la creo in caso non esista
if not directoryexists(as_folder) then
	if createdirectory(as_folder) = -1 then
		is_error = "Errore durante la creazione della cartella " + as_folder + ".~r~nVerificare i permessi di scrittura."
		return false
	end if
end if

return uof_execute(' x -y "' + as_zipped_file + '" -o"' + as_folder + '"')
end function

private function boolean uof_execute (string as_command);/**
 * stefanop
 * 25/11/2011
 *
 * Eseguo il comando con 7z.
 **/
 
uo_shellexecute luo_shell
luo_shell = create uo_shellexecute

setnull(is_error)

choose case luo_shell.uof_run(is_7z_exe + as_command)
		
	case RETURN_WARNING
		is_error = "Warning (Non fatal error(s)). For example, one or more files were locked by some other application, so they were not compressed."
		
	case RETURN_FATAL_ERROR
		is_error = "Fatal error"
		
	case RETURN_COMMAND_LINE_ERROR
		is_error = "Command line error"
		
	case RETURN_MEMORY_ERROR
		is_error = "Not enough memory for operation"
		
	case RETURN_USER_STOP
		is_error = "User stopped the process"
		
end choose

destroy luo_shell

return len(is_error) = 0
end function

public function boolean uof_zip (string as_zipped_file, string as_files_folders[]);/**
 * stefanop
 * 24/11/2011
 *
 * Comprimo tutta la lista dei file/folder in uno file zip unico
 **/

string ls_files

if not ib_7z then return false

setnull(is_error)

if upperbound(as_files_folders) < 1 then
	is_error = "Per creare un archivio serve specificare almeno un file."
	return false
end if

ls_files = '"' + guo_functions.uof_implode(as_files_folders, '" "', true) + '"'

return uof_execute(' a -y "' + as_zipped_file + '" ' + ls_files)
end function

on uo_7z.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_7z.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;
// imposto il percorso per l'exe di 7z
uof_set_7z_path(s_cs_xx.volume + s_cs_xx.risorse + "7z\7za.exe")
end event

