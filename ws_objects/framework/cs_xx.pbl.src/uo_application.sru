﻿$PBExportHeader$uo_application.sru
forward
global type uo_application from nonvisualobject
end type
end forward

global type uo_application from nonvisualobject
end type
global uo_application uo_application

type variables

string CS_PWD = "CS.$e95"

end variables

forward prototypes
public subroutine uof_inizio (application aa_applicazione, string as_app_nome, string as_app_versione, string as_app_ini, string as_app_bmp, boolean ab_placca, string as_utente, ref w_cs_xx_mdi aw_w_mdi)
public subroutine uof_fine ()
private subroutine uof_set_themes ()
public subroutine uof_set_current_theme (integer ai_theme_number)
public function boolean uof_login_sso (ref string as_username, ref string as_password)
public function boolean uof_uid_pwd_commandparm (ref string as_username, ref string as_password)
public function boolean uof_try_login (string as_username, string as_password, boolean ab_password_crypted)
public function string uof_versione ()
public subroutine uof_set_crypt_params ()
public subroutine uof_set_crypt_params_db ()
end prototypes

public subroutine uof_inizio (application aa_applicazione, string as_app_nome, string as_app_versione, string as_app_ini, string as_app_bmp, boolean ab_placca, string as_utente, ref w_cs_xx_mdi aw_w_mdi);/**
 * stefanop
 * 14/10/2010
 *
 * Porto la funzione f_cs_xx_inizio dentro a questo user object
 **/
 
boolean						lb_LRD
 
integer						li_errore, li_risposta, li_pos_start, li_pos_end, ll_tema, li_ret

string							ls_cod_individuale, ls_parametri, ls_cod_utente, ls_password, ls_pass, ls_cod_azienda, ls_valore, &
								ls_placca, ls_cod_lingua_applicazione, ls_error, ls_tema, ls_path_exe,ls_messaggio, ls_piva, ls_crypt_data, ls_numero
						
m_cs_xx						lm_cs_xx

uo_log_sistema				luo_log_sistema
uo_licenze					luo_licenze
uo_service_manager		luo_service_manager
uo_commandparm			 luo_commandparm
uo_commandparm_option	luo_option

luo_licenze = create uo_licenze

luo_commandparm = create uo_commandparm

setpointer(hourglass!)

// EnMe 18/08/2008: impostazione dei colori per le colonne importanti e obbligatorie
s_cs_xx.colore_colonna_importante = 10223615
s_cs_xx.colore_colonna_obbligatorio = 7319039
// fine modifica EnMe 18/08/2008

// Aggiunto da EnMe 5/12/2008 per SMTS tramite PBX
s_cs_xx.systemditectory = getcurrentdirectory()

/*
	GESTIONE PARAMETRI REGISTRY PER AVVIO APPLICAZIONE 
	24/04/2007 - Enrico & Michele
	Si è deciso di rivedere la gestione nel registry dei	profili di connessione al
	database e dei parametri all'applicazione (database_x e applicazione_x).
	Le modifiche implementate saranno principalmente:
	 - PROFILO CORRENTE: spostato dal registry (LOCAL_MACHINE) alle variabili
	 								globali (s_cs_xx.profilocorrente)
	 - PROFILO DEFAULT: aggiunto anche in LOCAL_USER
	I vari profili resteranno codificati in LOCAL_MACHINE, verrà modificata solo il
	metodo di lettura del numero profilo default.
	Una volta ottenuto il numero, i dettagli del profilo saranno comunque letti dalla
	chiave in LOCAL_MACHINE.
	Per maggiori dettagli sulla gestione di questi parametri vedere il codice a seguire
*/

// configurazione delle chiavi root di registro per LOCAL_MACHINE e LOCAL_USER
s_cs_xx.chiave_root = "HKEY_LOCAL_MACHINE\Software\Consulting&Software\"
s_cs_xx.chiave_root_user = "HKEY_CURRENT_USER\Software\Consulting&Software\"

// stefanop 14/10/2010: impostazioni
this.uof_set_themes()

if Registryget(s_cs_xx.chiave_root_user + "profilodefault", "tema", ls_tema) < 0 then
	ls_tema = "1"	//tema default classico
	Registryset(s_cs_xx.chiave_root_user + "profilodefault", "tema", regstring!, ls_tema)
end if
ll_tema = long(ls_tema)

if isnull(ll_tema) or ll_tema<0 or not isnumber(ls_tema) or ll_tema>upperbound(s_themes.theme)  then
	ll_tema=1
	Registryset(s_cs_xx.chiave_root_user + "profilodefault", "tema", regstring!, "1")
end if

this.uof_set_current_theme(ll_tema)

this.uof_set_crypt_params()
// ----

// l'eventuale profilo specificato all'avvio dell'applicazione ha sempre massima priorità
s_cs_xx.livello_profilo = "OPEN_PARM"

// stefanop 09/11/2012
if luo_commandparm.uof_have_option("P") then

	luo_option = luo_commandparm.uof_get_option("P")
	s_cs_xx.profilodefault = luo_option.uof_get_string(1)

	// se il profilo è stato specificato nei parametri di avvio, si riconduce alla gestione per utente.
	// Per questo si simula che il profilo default sia stato letto da questa posizione.
	// Inoltre SOLO SE NON ESISTE si va anche ad inserire la chiave profilo default per utente (LOCAL_USER)
	// utilizzando il profilo passato nei parametri di avvio
	s_cs_xx.livello_profilo = "LOCAL_USER"
	
	string ls_dummy
	
	if Registryget(s_cs_xx.chiave_root_user + "profilodefault", "numero", ls_dummy) < 0 then
		Registryset(s_cs_xx.chiave_root_user + "profilodefault", "numero", regstring!, s_cs_xx.profilodefault)
	end if
	
// se nessun profilo è specificato nel collegamento di avvio, allora si cerca il profilo default nel registro
else
	
	// si cerca prima un'impostazione per singolo utente (LOCAL_USER)
	s_cs_xx.livello_profilo = "LOCAL_USER"
	
	li_risposta = Registryget(s_cs_xx.chiave_root_user + "profilodefault", "numero", s_cs_xx.profilodefault)
	
	if li_risposta = -1 then 
		
		// se il profilo default non è specificato per utente, si cerca a livello di macchina (LOCAL_MACHINE)
		s_cs_xx.livello_profilo = "LOCAL_MACHINE"
		
		li_risposta = Registryget(s_cs_xx.chiave_root + "profilodefault", "numero", s_cs_xx.profilodefault)
				
		if li_risposta = -1 then
			
			// se il profilo default non è specificato nel registro a nessun livello, si chiede all'utente di selezionare uno dei profili
			// configurati a livello di macchina (LOCAL_MACHINE)
			// La maschera di selezione profili si aprirà col check "imposta default" bloccato, in modo che alla conferma la chiave di registro
			// del profilo default per utente (LOCAL_USER) venga impostata
			s_cs_xx.livello_profilo = "LOCAL_USER"
	
			s_cs_xx.parametri.parametro_b_1 = true
			
			open(w_scelta_profili)
			
			// se anche dopo la richiesta di selezione all'utente non si è otteuto un numero profilo, allora dobbiamo interrompere
			// l'esecuzione dell'applicazione in quanto non abbiamo i dati necessari a continuare
			if s_cs_xx.parametri.parametro_i_1 < 0 then
				messagebox("Consulting & Software","Impossibile continuare senza alcun profilo di connessione impostato o selezionato.~nApplicazione interrotta",stopsign!,ok!)
				halt close
			else
				s_cs_xx.profilodefault = string(s_cs_xx.parametri.parametro_i_1)
				s_cs_xx.livello_profilo = "LOCAL_USER"
				registryset(s_cs_xx.chiave_root_user + "profilodefault", "numero", regstring!, s_cs_xx.profilodefault)
			end if
			
		else
			
			s_cs_xx.livello_profilo = "LOCAL_USER"
			registryset(s_cs_xx.chiave_root_user + "profilodefault", "numero", regstring!, s_cs_xx.profilodefault)
			
		end if
		
	end if
	
end if
// -- fine scelta profilo --------------------------

// si imposta il profilo corrente e si aggiorna il relativo parametro nel registry per utente (LOCAL_USER)
s_cs_xx.profilocorrente = s_cs_xx.profilodefault

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "vol", s_cs_xx.volume)
//li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "vhl", s_cs_xx.s_help.volume)
li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "ver", as_app_versione)
li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "nom", as_app_nome)

if li_risposta = - 1 then
	as_app_nome = "APICE"
	li_risposta = RegistrySet(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "nom", "APICE")
end if

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "pic", as_app_bmp)
if li_risposta = - 1 then
	as_app_bmp = "\cs_xx_50\cs_team\cs_team.bmp"
	li_risposta = RegistrySet(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "pic", "\cs_xx_50\cs_team\cs_team.bmp")
end if

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "plq", ls_placca)
if li_risposta = - 1 then
	ls_placca = "true"
	li_risposta = RegistrySet(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "plq", "true")
end if

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "ris", s_cs_xx.risorse)
if li_risposta = - 1 then
	s_cs_xx.risorse = "\cs_90\framework\risorse\"
	li_risposta = RegistrySet(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "ris", "\cs_90\framework\risorse\")
end if

if ls_placca = "true" then 
	ab_placca = true
else
	ab_placca = false
end if
	
as_app_versione = "Versione " + as_app_versione 
li_errore = init_pcca(aa_applicazione, as_app_nome, as_app_versione, s_cs_xx.volume, as_utente, s_cs_xx.volume + as_app_bmp, ab_placca)
pcca_error(li_errore)


if f_po_getconnectinfo(as_app_ini, "database_" + s_cs_xx.profilocorrente, sqlca) <> 0 then
   halt close
end if

if upper( left(sqlca.DBMS,2) ) = "OR" then
	li_risposta = registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "oracle_dateformat", sqlca.oracle_dateformat)
end if

if f_po_connect(sqlca, true) <> 0 then
   halt close
end if

if f_db_funzioni() <> 0 then
   halt close
end if

// Stefanop: 08/11/2012
// Aggiunta modalita esequzione servizio
// Attenzione che qui c'è un super trick per fare il cast degli oggetti in PB
if luo_commandparm.uof_have_option("S") then
	
	uo_commandparm_service luo_option_service
	luo_service_manager = create uo_service_manager
	luo_option = create uo_commandparm_service // CAST
	
	luo_option =  luo_commandparm.uof_get_option_with_cast("S", luo_option)
	luo_option_service = luo_option
		
	// Eseguo il servizio richiesto
	luo_service_manager.dispatch(ref luo_commandparm, luo_option_service)
	
	// Termino il programma, tutti i processi sono finiti
	halt close
	return
end if
// -----

open(aw_w_mdi)

m_cs_xx.m_help.m_filtriautomatici.visible = true
m_cs_xx.m_help.m_labelwindow.visible = true

lm_cs_xx = create m_cs_xx
pcca.mdi.c_mdi_menulabelfile = lm_cs_xx.m_file.text
pcca.mdi.c_mdi_menulabeledit = lm_cs_xx.m_edit.text
pcca.mdi.c_mdi_menulabelsave = lm_cs_xx.m_edit.m_save.text
pcca.mdi.c_mdi_menulabelsaverowsas = lm_cs_xx.m_file.m_esporta.text
pcca.mdi.c_mdi_menulabelprint = lm_cs_xx.m_file.m_stampa.text
pcca.mdi.c_mdi_menulabelnew = lm_cs_xx.m_edit.m_new.text
pcca.mdi.c_mdi_menulabelview = lm_cs_xx.m_edit.m_view.text
pcca.mdi.c_mdi_menulabelmodify = lm_cs_xx.m_edit.m_modify.text
pcca.mdi.c_mdi_menulabeldelete = lm_cs_xx.m_edit.m_delete.text
pcca.mdi.c_mdi_menulabelfirst = lm_cs_xx.m_edit.m_first.text
pcca.mdi.c_mdi_menulabelprev = lm_cs_xx.m_edit.m_previous.text
pcca.mdi.c_mdi_menulabelnext = lm_cs_xx.m_edit.m_next.text
pcca.mdi.c_mdi_menulabellast = lm_cs_xx.m_edit.m_last.text
pcca.mdi.c_mdi_menulabelquery = lm_cs_xx.m_edit.m_query.text
pcca.mdi.c_mdi_menulabelsearch = lm_cs_xx.m_edit.m_search.text
pcca.mdi.c_mdi_menulabelseperator = "-"
destroy lm_cs_xx

close(w_plaque)

// Carico il paramentro del tipo di invio mail supportato
li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "mai", ls_valore)
s_cs_xx.num_livello_mail = integer(ls_valore)
s_cs_xx.tipo_ordinamento = true

	
//Donato 27-10-2008
//modifica per caricare eventuali parametri del commandparm UID e PWD per login automatica
string ls_username

if not uof_login_sso(ls_username, ls_password) then
	// Controllo la commandarm UID e PWD
	s_cs_xx.num_livello_sic = integer(Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "sic", ls_valore)	)
	
	if s_cs_xx.num_livello_sic >= 1 then
		if not uof_uid_pwd_commandparm(ls_username, ls_password) then
			window_open(w_login, 0)
		else
			if not uof_try_login(ls_username, ls_password, false) then
				window_open(w_login, 0)
			else
				// se ci sei riuscito registra l'accesso
				luo_log_sistema = create uo_log_sistema
				luo_log_sistema.uof_write_log_sistema( "LOGIN_WIN", g_str.format("LOGIN - SUCCESS: Accesso Utente=$1, ComputerName=$2, IP Address=$3",s_cs_xx.cod_utente, guo_functions.uof_get_computer_name( ),guo_functions.uof_get_ip()) )
				destroy luo_log_sistema
			end if
		end if
	end if
	
else
	// Cerco di entrare con SSO del Dominio.
	if not uof_try_login(ls_username, ls_password, true) then
		window_open(w_login, 0)
	else
		// se ci sei riuscito registra l'utente
		luo_log_sistema = create uo_log_sistema
		luo_log_sistema.uof_write_log_sistema( "LOGIN_WIN", g_str.format("LOGIN - SUCCESS: Accesso Utente=$1, ComputerName=$2, IP Address=$3",s_cs_xx.cod_utente, guo_functions.uof_get_computer_name( ),guo_functions.uof_get_ip()) )
		destroy luo_log_sistema
	end if
end if


commit;


//####################################################################
// aggiunto da EnMe per gestione multilingue 30/1/2008
select cod_lingua_applicazione
into :ls_cod_lingua_applicazione
from aziende_utenti
where 
	cod_azienda = :s_cs_xx.cod_azienda and 
	cod_utente = :s_cs_xx.cod_utente;

if sqlca.sqlcode <> 0 then setnull(ls_cod_lingua_applicazione)
s_cs_xx.cod_lingua_applicazione = ls_cod_lingua_applicazione
// fine modifica 30/1/2008


// ------------------- GESTIONE NUOVO CONTROLLO LICENZE   ------------------------------------

/* 			 nuova gestione con controllo CLOUD
Attenzione: nell'azienda è obbligatorio che ci sia la partita iva e che essa sia attivata nel nostro database 

// Leggo partita iva azienda che deve essere presente obbligatoriamente
select partita_iva 
into	:ls_piva
from	aziende
where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	messagebox("Framework", "Impossibile eseguire il controllo licenza")
	disconnect;
	halt close;
end if

li_ret = luo_licenze.uof_cloud_license( ls_piva)

choose case li_ret
	//(false) licenza disattivata
	case 0		
		luo_licenze.uof_lock_client()
		
	case -1		
		// vado avanti normalmente
		// verifico da quanti giorni la licenza è in grace time e se ho superato i 30 giorni chiudo!
		if luo_licenze.uof_grace_time() = -1 then
			// Blocco
			luo_licenze.uof_lock_client()
		else
			// vado avanti
			luo_licenze.uof_set_acl("A1")
		end if
		
 	// (true) tutto bene, licenza attiva!
	case 1
		luo_licenze.uof_set_acl("A1")
		
		// memorizzo l'ultima volta che il controllo licenza è andato a buon fine
		ls_crypt_data = "Verify" + string( year(today()),"0000") + string(month(today()),"00") + string(day(today()),"00"  ) + "CS"
		
		guo_functions.uof_crypt(ls_crypt_data, ref ls_crypt_data) 
		
		update sistema 
		set pwd_manuale = :ls_crypt_data;
		
		luo_licenze.uof_unlock_client()
		
		commit;
	case -2		
		// Risposta Json non compatibile con i valori attesi
		messagebox("Framework", "Errore di comunicazione con il License Server Csteam.")
		disconnect;
		halt close;
		
	case -3		
		// Risposta Json non compatibile con i valori attesi
		luo_licenze.uof_lock_client()
		messagebox("Framework", "La licenza del software non è valida.")
		disconnect;
		halt close;
		
end choose


// -----------  Se in precedenza avevo bloccato, rimane bloccato finchè non viene eliminata la chiave di registro NUMERO da HkeyLocalUser\profiloCorrente
if RegistryGet( s_cs_xx.chiave_root_user +  "profilocorrente", "numero", RegString!, ls_numero)	 = 1 then
	// applicazione bloccata
	messagebox("Framework", "La licenza non ha superato il processo di verifica.")
	disconnect;
	halt close
end if

// ----------- Controllo licenze in caso di installazioni per user -----------------------
destroy luo_licenze

select stringa
into :gs_licenze
from parametri
where
 cod_parametro = 'ACL' and
 flag_parametro = 'S';

if sqlca.sqlcode <> 0 or isnull(gs_licenze) or gs_licenze = "" then gs_licenze = "A1"

if gs_licenze = "A3" and s_cs_xx.cod_utente <> "CS_SYSTEM" then

	li_ret = luo_licenze.uof_accesso_v2(ref ls_messaggio)
	
	if li_ret <> 0 then
		
		gs_licenze = "A1"
		if not isnull(ls_messaggio) and len(ls_messaggio) > 0 then
			g_mb.error("Framework", ls_messaggio)
			destroy luo_licenze
		end if
		halt close
		
	end if

end if	
*/


// ------------------------------  fine modifica licenze -----------------------------------

// -- creazione della transazione SQLCI verso la contabilità di impresa
string ls_stringa, ls_impresa

ls_impresa = "FALSE"
li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "imp", ls_impresa)

ls_impresa = upper(ls_impresa)

select stringa
into   :ls_stringa
from   parametri
where  cod_parametro = 'OIM';
if sqlca.sqlcode < 0 then
	g_mb.error("Apice - Impresa", "Connessione verso la contabilità non effettuata a causa di un errore. L'applicazione verrà terminata. Dettaglio " + sqlca.sqlerrtext)
	halt close
end if

// ENRICO 28/02/2001 AGGIUNTO PER GESTIONE DI PIU' AZIENDE -------------------------------------
if sqlca.sqlcode = 100 then
	select stringa
	into   :ls_stringa
	from   parametri_azienda
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_parametro = 'OIM';
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Apice - Impresa", "Connessione verso la contabilità non effettuata a causa di un errore. L'applicazione verrà terminata. Dettaglio " + sqlca.sqlerrtext)
		halt close
	end if
end if
/* ---------------------------------------------------------------------------------------------
EnMe 18-12-2013: ora esiste una tabella nella quale inserire i parametri per i collegamenti ai DB ESTERNI.
il collegamento al DB di Impresa deve avere obbligatoriamente il parametro "impresa_snc" in minuscolo 
Inoltre il parametro OIM deve essere contenere il valore stringa "impresa_snc"
*/

choose case ls_stringa
	case "impresa_snc"
		
		if sqlca.sqlcode = 0 and ls_impresa = "TRUE" then // esiste il parametro e allora mi connetto alla contabilità IMPRESA.
		
			s_cs_xx.parametri.impresa = FALSE
			sqlci = CREATE n_tran

			select 	db_esterni.par_database,   
						db_esterni.par_dbms,   
						db_esterni.par_dbparm,   
						db_esterni.par_dbpass,   
						db_esterni.par_lock,   
						db_esterni.par_logid,   
						db_esterni.par_logpass,   
						db_esterni.par_servername,   
						db_esterni.par_userid  
			 into 		:sqlci.database,
			 			:sqlci.dbms,
						:sqlci.dbparm,   
						:sqlci.dbpass,   
						:sqlci.lock,   
						:sqlci.logid,   
						:sqlci.logpass,   
						:sqlci.servername,   
						:sqlci.userid  
			 from 	db_esterni   
			 where 	db_esterni.cod_database = :ls_stringa;
			 
			 if sqlca.sqlcode = 0 then
				if f_po_connect(sqlci, TRUE) <> 0 then
					g_mb.error("Apice - Impresa", "Errore durante il collegamento alla fonte dati della contabilità Impresa")
					halt close
				end if
				s_cs_xx.parametri.impresa = TRUE
			else
				g_mb.error("Attenzione: mancano dei parametri per il collegamento ai dati contabili: contattare il servizio di assistenza")
				halt close
			end if
			
		end if
		
	case else
		
		if sqlca.sqlcode = 0 and ls_impresa = "TRUE" then // esiste il parametro e allora mi connetto alla contabilità IMPRESA.
			sqlci = CREATE n_tran
			sqlci.servername = "CS_DB"
			sqlci.logid = ""
			sqlci.logpass = ""
			sqlci.dbms = "Odbc"
			sqlci.database = ""
			sqlci.userid = "DBA"
			sqlci.dbpass = "nonlaso"
			sqlci.dbparm = "Connectstring='DSN=" + ls_stringa + "'"
			if f_po_connect(sqlci, TRUE) <> 0 then
				g_mb.error("Apice - Impresa", "Errore durante il collegamento alla fonte dati della contabilità Impresa")
				halt close
			end if
			s_cs_xx.parametri.impresa = TRUE
		else
			s_cs_xx.parametri.impresa = FALSE
		end if
		
end choose


/* ---------------  Enrico 07/01/2014   ---------------------------------
NUOVA GESTIONE ACCESSO MANUALE ON-LINE
select pwd_manuale
into :s_cs_xx.pwd_manuale
from sistema;

if sqlca.sqlcode <> 0 then
	g_mb.error("Attenzione, è necessario inizializzare la tabella SISTEMA")
	setnull(s_cs_xx.pwd_manuale)
else
	if guo_functions.uof_decrypt(s_cs_xx.pwd_manuale, ref s_cs_xx.pwd_manuale) < 0 then
		setnull(s_cs_xx.pwd_manuale)
		g_mb.error("Errore in decodifica password guida utente on-line.")
	end if
end if
//------------------------------------------------------------------------------*/



/* ---------------  Enrico 23/09/2008   ---------------------------------
	Aggiunto parametro aziendale per la gestione dei report distribuiti; 
	in pratica viene aggiunta una PBL in alto nella librarylist; al cliente
	vengono consegnate le DW in formato sorgente e se le può modificare
	con infomaker
*/
string							ls_library, ls_lrd, ls_librarylist
long							ll_ret, ll_pos
uo_log_sistema				luo_log
s_cs_xx_parametri			lstr_parametri
w_splash_screen			lw_window

select stringa
into :ls_lrd
from   parametri_azienda 
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_parametro = 'LRD';

if sqlca.sqlcode = 0 then
	if not isnull(ls_lrd) and len(ls_lrd) > 0 then
		//è stato abilitato il parametro libreria report personalizzati
		lb_LRD = true
		
		//prova prima nel percorso dell'eseguibile
		//estrazione percorso dell'exe (es. c:\cs_team\exe\)
		ls_path_exe = GetLibraryList()
		ls_path_exe = left(ls_path_exe, lastpos(left(ls_path_exe, pos(ls_path_exe, ",") -1), "\"))
		
		//concateno il nome del file .pbl
		ls_library = ls_path_exe + ls_lrd
		
		if not fileexists(ls_library) then
			//libreria report personalizzati non presente nel percorso dell'eseguibile
			//prova allora nel percorso delle risorse
			ls_library = s_cs_xx.volume + s_cs_xx.risorse + ls_lrd
		
			if not fileexists(ls_library) then
				//nè nelle risorse nè nel percorso dell'eseguibile
				lb_LRD = false
				//g_mb.error("Attenzione", "La libreria di report prevista " + ls_lrd + " non è stata trovata (nè nel percorso dell'eseguibile nè nelle risorse): "+&
				//									"i report distribuiti non saranno disponibili nell'applicazione!")
				try
					lstr_parametri.parametro_s_1 = "E"
					lstr_parametri.parametro_s_2 = "La libreria di report prevista " + ls_lrd + " non è stata trovata (nè nel percorso dell'eseguibile nè nelle risorse): "+&
																"i report distribuiti non saranno disponibili nell'applicazione!"
					opensheetwithparm(lw_window, lstr_parametri, PCCA.MDI_Frame, 6,  Original!)
				catch (runtimeerror e1)
				end try
				
			end if
		end if
		
		if lb_LRD then
			ls_librarylist = GetLibraryList ( )
			ls_librarylist = ls_library + ", " + ls_librarylist
			ll_ret = SetLibraryList (ls_librarylist)
			
			choose case ll_ret
				case -1
					//The application is being run from PowerBuilder, rather than from a standalone executable
					//g_mb.warning("Attenzione", "(-1) Impossibile aggiungere la libreria ("+ls_library+") se hai avviato il programma dall'ambiente di sviluppo: "+&
					//										"i report distribuiti non saranno disponibili nell'applicazione!")
					try
						lstr_parametri.parametro_s_1 = "W"
						lstr_parametri.parametro_s_2 = "(-1) Impossibile aggiungere la libreria ("+ls_library+") se hai avviato il programma dall'ambiente di sviluppo: "+&
																	"i report distribuiti non saranno disponibili nell'applicazione!"
						opensheetwithparm(lw_window, lstr_parametri, PCCA.MDI_Frame, 6,  Original!)
					catch (runtimeerror e2)
					end try
					
				case -2
					//A currently instantiated object is in a library that is not on the new list. If any argument's value is null, SetLibraryList returns null.
					//g_mb.error("Attenzione", "(-2) Errore in funzione SetlibraryList ("+ls_library+"): "+&
					//									"i report distribuiti non saranno disponibili nell'applicazione!")
					try
						lstr_parametri.parametro_s_1 = "E"
						lstr_parametri.parametro_s_2 =  "(-2) Errore in funzione SetlibraryList ("+ls_library+"): "+&
																	"i report distribuiti non saranno disponibili nell'applicazione!"
						opensheetwithparm(lw_window, lstr_parametri, PCCA.MDI_Frame, 6,  Original!)
					catch (runtimeerror e3)
					end try

				case is < 0
					//g_mb.error("Attenzione", "(<0) Errore in funzione SetlibraryList ("+ls_library+"): "+&
					//									"i report distribuiti non saranno disponibili nell'applicazione!")
					try
						lstr_parametri.parametro_s_1 = "E"
						lstr_parametri.parametro_s_2 =  "(<0) Errore in funzione SetlibraryList ("+ls_library+"): "+&
																	"i report distribuiti non saranno disponibili nell'applicazione!"
						opensheetwithparm(lw_window, lstr_parametri, PCCA.MDI_Frame, 6,  Original!)
					catch (runtimeerror e4)
					end try

				case else
					//OK
					luo_log = create uo_log_sistema
					luo_log.uof_write_log_sistema_not_sqlca("LDR", "Libreria report personali "+ls_library+" aggiunta alla lista!")
					destroy luo_log
			end choose
			
//			if ll_ret < 0 then
//				g_mb.error("FRAMEWORK", "Libreria Report Distribuiti non trovata.~r~nI report distribuiti non saranno disponibili nell'applicazione")
//			end if

		end if
	end if
end if
//  ---------------  fine modifica EnMe 23/09/2008
end subroutine

public subroutine uof_fine ();/**
 * stefanop
 * 14/10/2010
 *
 * Funzione copiata e modificata dalla f_cs_xx_fine
 * Slogga l'utente dalla sessione se abilitato
 **/

if gs_licenze = "A3" and s_cs_xx.cod_utente <> "CS_SYSTEM" then

	delete from sessioni
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente and
		start_at = :gdt_session_start_at;
		
	commit using sqlca;
	
end if

if s_cs_xx.parametri.impresa then
	
	commit using sqlci;
	disconnect using sqlci;
	
end if

disconnect;

term_pcca()
end subroutine

private subroutine uof_set_themes ();/**
 * stefanop
 * 14/10/2010
 *
 * Imposto lo stile dei temi
 **/
 
s_themes.theme[1].theme_name = "Classic"

s_themes.theme[1].mdi_backcolor = 285212671
s_themes.theme[1].w_backcolor = 12632256
s_themes.theme[1].dw_backcolor = 12632256

s_themes.theme[1].dw_column_backcolor = 16777215
s_themes.theme[1].dw_column_viewmode_backcolor = 12632256
s_themes.theme[1].dw_column_textcolor = 0
s_themes.theme[1].dw_column_font = "Tahoma"
s_themes.theme[1].dw_column_fontsize = "-8"
s_themes.theme[1].dw_column_fontbold = "400"
s_themes.theme[1].dw_column_fontitalic = "0"

s_themes.theme[1].dw_text_backcolor = 12632256
s_themes.theme[1].dw_text_textcolor = 0
s_themes.theme[1].dw_text_font = "Tahoma"
s_themes.theme[1].dw_text_fontsize = "-8"
s_themes.theme[1].dw_text_fontbold = "400"
s_themes.theme[1].dw_text_fontitalic = "0"

// --- impostazioni del folder ----------------------
// Colore della cartella disabilitata
s_themes.theme[1].f_folder_color = 12632256

//Colore dell'effetto 3D (Shade)
s_themes.theme[1].f_shade_color =  8421504

// Colore generale della cartella
s_themes.theme[1].f_tab_color = 12632256

// Colore del testo del folder su cui c'è il focus e su cui non c'è
s_themes.theme[1].f_tab_disabled_textcolor = 0
s_themes.theme[1].f_tab_enabled_textcolor = 0


// --  impostazioni dedicate al tooltip  ----------------------

s_themes.theme[1].tooltip_backcolor = 134217752
s_themes.theme[1].tooltip_textcolor = 0
s_themes.theme[1].tooltip_isbubble = 1




// TEMA 2 - Soft ##########################################
s_themes.theme[2].theme_name = "SOFT"

s_themes.theme[2].mdi_backcolor = 14672615
s_themes.theme[2].w_backcolor = 12699603
s_themes.theme[2].dw_backcolor = 22699603

s_themes.theme[2].dw_column_backcolor = 16777215
s_themes.theme[2].dw_column_viewmode_backcolor = 0
s_themes.theme[2].dw_column_textcolor = 0
s_themes.theme[2].dw_column_font = "Tahoma"
s_themes.theme[2].dw_column_fontsize = "-8"
s_themes.theme[2].dw_column_fontbold = "400"
s_themes.theme[2].dw_column_fontitalic = "0"

s_themes.theme[2].dw_text_backcolor = 16777215
s_themes.theme[2].dw_text_textcolor = 0
s_themes.theme[2].dw_text_font = "Tahoma"
s_themes.theme[2].dw_text_fontsize = "-8"
s_themes.theme[2].dw_text_fontbold = "400"
s_themes.theme[2].dw_text_fontitalic = "0"

// --- impostazioni del folder ----------------------
s_themes.theme[2].f_tab_color = 14672615 		// current
s_themes.theme[2].f_shade_color =  11515591		//shadow
s_themes.theme[2].f_folder_color = 13357273		// disable

// Colore del testo del folder su cui c'è il focus e su cui non c'è
s_themes.theme[2].f_tab_disabled_textcolor = 0
s_themes.theme[2].f_tab_enabled_textcolor = 0


// --  impostazioni dedicate al tooltip  ----------------------
s_themes.theme[2].tooltip_backcolor = 134217752
s_themes.theme[2].tooltip_textcolor = 0
s_themes.theme[2].tooltip_isbubble = 1
// ##########################################################



// TEMA 3 - FRESH  (OK) ########################################
s_themes.theme[3].theme_name = "Fresh"

s_themes.theme[3].mdi_backcolor = 15261901
s_themes.theme[3].w_backcolor = 15261901
s_themes.theme[3].dw_backcolor = 9878990

s_themes.theme[3].dw_column_backcolor = 16777215
s_themes.theme[3].dw_column_viewmode_backcolor = 16777215
s_themes.theme[3].dw_column_textcolor = 0
s_themes.theme[3].dw_column_font = "Tahoma"
s_themes.theme[3].dw_column_fontsize = "-8"
s_themes.theme[3].dw_column_fontbold = "400"
s_themes.theme[3].dw_column_fontitalic = "0"

s_themes.theme[3].dw_text_backcolor = 12632256
s_themes.theme[3].dw_text_textcolor = 0
s_themes.theme[3].dw_text_font = "Tahoma"
s_themes.theme[3].dw_text_fontsize = "-8"
s_themes.theme[3].dw_text_fontbold = "400"
s_themes.theme[3].dw_text_fontitalic = "0"

// --- impostazioni del folder ----------------------
s_themes.theme[3].f_folder_color	= 12836071	// disable
s_themes.theme[3].f_shade_color =  3441059	// shadow
s_themes.theme[3].f_tab_color 	= 9878990	// current

// Colore del testo del folder su cui c'è il focus e su cui non c'è
s_themes.theme[3].f_tab_disabled_textcolor = 0
s_themes.theme[3].f_tab_enabled_textcolor = 0
// ##########################################################


// TEMA 4 - BOSSISTYLE (OK) ########################################
s_themes.theme[4].theme_name = "BossiStyle"

s_themes.theme[4].mdi_backcolor = 11711395
s_themes.theme[4].w_backcolor 	= 14737887
s_themes.theme[4].dw_backcolor = 11711395

s_themes.theme[4].dw_column_backcolor = 16777215
s_themes.theme[4].dw_column_viewmode_backcolor = 0
s_themes.theme[4].dw_column_textcolor = 0
s_themes.theme[4].dw_column_font = "Verdana"
s_themes.theme[4].dw_column_fontsize = "-8"
s_themes.theme[4].dw_column_fontbold = "400"
s_themes.theme[4].dw_column_fontitalic = "0"

s_themes.theme[4].dw_text_backcolor = 12632256
s_themes.theme[4].dw_text_textcolor = 0
s_themes.theme[4].dw_text_font = "Tahoma"
s_themes.theme[4].dw_text_fontsize = "-8"
s_themes.theme[4].dw_text_fontbold = "400"
s_themes.theme[4].dw_text_fontitalic = "0"

// --- impostazioni del folder ----------------------
s_themes.theme[4].f_tab_color 	= 11711395	// current
s_themes.theme[4].f_folder_color	= 12370066	// disable
s_themes.theme[4].f_shade_color =  7106385	// shadow

// Colore del testo del folder su cui c'è il focus e su cui non c'è
s_themes.theme[4].f_tab_disabled_textcolor = 0
s_themes.theme[4].f_tab_enabled_textcolor = 0
// ##########################################################


// TEMA 4 - SOFT (OK) ########################################
s_themes.theme[5].theme_name = "WinColor"

s_themes.theme[5].mdi_backcolor = 14211288
s_themes.theme[5].w_backcolor 	= 15790320
s_themes.theme[5].dw_backcolor = 15790320

s_themes.theme[5].dw_column_backcolor = 16777215
s_themes.theme[5].dw_column_viewmode_backcolor = 14211288
s_themes.theme[5].dw_column_textcolor = 0
s_themes.theme[5].dw_column_font = "Tahoma"
s_themes.theme[5].dw_column_fontsize = "-8"
s_themes.theme[5].dw_column_fontbold = "400"
s_themes.theme[5].dw_column_fontitalic = "0"

s_themes.theme[5].dw_text_backcolor = 255
s_themes.theme[5].dw_text_textcolor = 0
s_themes.theme[5].dw_text_font = "Tahoma"
s_themes.theme[5].dw_text_fontsize = "-8"
s_themes.theme[5].dw_text_fontbold = "400"
s_themes.theme[5].dw_text_fontitalic = "0"

// --- impostazioni del folder ----------------------
s_themes.theme[5].f_tab_color 	= 15790320 // current
s_themes.theme[5].f_folder_color	= 13948116	// disable
s_themes.theme[5].f_shade_color =  116777215 // shadow

// Colore del testo del folder su cui c'è il focus e su cui non c'è
s_themes.theme[5].f_tab_disabled_textcolor = 8421504 // 0
s_themes.theme[5].f_tab_enabled_textcolor = 0
// ##########################################################
end subroutine

public subroutine uof_set_current_theme (integer ai_theme_number);/**
 * stefanop
 * 14/10/2010
 *
 * Imposto il numero del tema corrente
 **/

s_themes.current_theme = ai_theme_number
end subroutine

public function boolean uof_login_sso (ref string as_username, ref string as_password);/**
 * stefanop
 * 28/10/2011
 *
 * Login via Single Sing On
 **/
 
string ls_username, ls_cod_utente

ls_username = guo_functions.uof_get_user_info("USERNAME")


if not isnull(ls_username) and ls_username <> "" then
	
	select cod_utente
	into :ls_cod_utente
	from utenti_dominio
	where utente_dominio = :ls_username;
		
	if sqlca.sqlcode = 0 and not isnull(ls_cod_utente) and ls_cod_utente <> "" then
			
		select username, password
		into :as_username, :as_password
		from utenti
		where cod_utente = :ls_cod_utente;
		
		if sqlca.sqlcode = 0 and not isnull(as_username) and as_username <> "" then
			return true
		end if
		
	end if
end if

return false

end function

public function boolean uof_uid_pwd_commandparm (ref string as_username, ref string as_password);string ls_commandparm,  ls_cmdp_upper
long ll_pos, ll_pos_end

ls_commandparm = Commandparm()
ls_cmdp_upper = upper(ls_commandparm)

if len(ls_commandparm) < 1 then
	return false
else
	ll_pos = pos(ls_cmdp_upper, "UID=")
	
	if ll_pos > 0 then
		//parametro UID trovato
		ll_pos_end = pos(ls_cmdp_upper, ",",ll_pos)
		if ll_pos_end > 0 then
			as_username = mid(ls_commandparm, ll_pos + 4, ll_pos_end - (ll_pos + 4) )
		else
			as_username = mid(ls_commandparm, ll_pos + 4)
		end if
	else
		return false
	end if
	
	ll_pos = pos(ls_cmdp_upper, "PWD=")
	if ll_pos > 0 then
		//parametro PWD trovato
		ll_pos_end = pos(ls_cmdp_upper, ",", ll_pos)
		if ll_pos_end > 0 then
			as_password = mid(ls_commandparm, ll_pos + 4, ll_pos_end - (ll_pos + 4) )
		else
			as_password = mid(ls_commandparm, ll_pos + 4)
		end if
	else
		return false
	end if
end if 

return true

end function

public function boolean uof_try_login (string as_username, string as_password, boolean ab_password_crypted);string ls_cod_utente, ls_pass, ls_cod_azienda, ls_flag_collegato, ls_flag_blocco, ls_flag_supervisore
datetime ldt_data_blocco, ldt_data_modifica_pwd
n_cst_crypto lnv_crypt

lnv_crypt = create n_cst_crypto

select
	cod_utente,
	password,
	flag_collegato,
	flag_blocco,
	data_blocco,
	cod_azienda,
	data_modifica_pwd,
	flag_supervisore
into		
	:ls_cod_utente,
	:ls_pass,
	:ls_flag_collegato,
	:ls_flag_blocco,
	:ldt_data_blocco,
	:ls_cod_azienda,
	:ldt_data_modifica_pwd,
	:ls_flag_supervisore
from utenti
where username = :as_username;

// Bloccato ?
if ls_flag_blocco = "S" and today() >= date(ldt_data_blocco) then
	g_mb.messagebox("Attenzione", "Utente bloccato dal " + string(date(ldt_data_blocco), "dd/mm/yyyy") + ".", exclamation!, ok!)
	return false
end if

if not ab_password_crypted then
	//Donato: occorre cryptare la password parametrizzata per confrontarla con quella memorizzata					
	if as_password <> "" then
		if lnv_crypt.EncryptData(as_password, as_password) < 0 then as_password = "prova tanto non funziona"
	end if
end if

destroy lnv_crypt

if as_password = ls_pass then
	s_cs_xx.cod_utente = ls_cod_utente
else
	g_mb.error("Login", "Password Errata")
	return false
end if

// AZIENDA
if isnull(ls_cod_azienda) then
	window_open(w_aziende_scelta, 0)
else
	s_cs_xx.cod_azienda = ls_cod_azienda
end if

// ADMIN?
s_cs_xx.admin = (ls_flag_collegato = "S")

//SUPERVISORE
s_cs_xx.supervisore=(ls_flag_supervisore = "S")

open(w_menu_button,w_cs_xx_mdi)
return true
end function

public function string uof_versione ();/**
 * stefanop
 * 02/07/2012
 *
 * Compongo la versione del software che è data dalla concatenazione della versione_sw + versione_db della tabella sistema
 * La versione sarà: 12.1.103
 * - Versione software di powerbuilder
 * - Versione script database
 **/
 
string ls_versione_sw, ls_versione_db

select versione_sw, versione_db
into :ls_versione_sw, :ls_versione_db
from sistema
where id = 1;

if sqlca.sqlcode = 0 then
	
	if isnull(ls_versione_sw) or ls_versione_sw = "" then ls_versione_sw = "0.0"
	if isnull(ls_versione_db) or ls_versione_db = "" then ls_versione_db = "0"
	
	return ls_versione_sw + "." + ls_versione_db
	
else
	
	return "0.0.0"
	
end if
end function

public subroutine uof_set_crypt_params ();// impostazione set caratteri accettati per l'algoritmo di crypt/decrypt
gs_crypt_accepted_chars[1]		= "!"
gs_crypt_accepted_chars[2]		= "#"
gs_crypt_accepted_chars[3]		= "$"
gs_crypt_accepted_chars[4]		= "%"
gs_crypt_accepted_chars[5]		= "&"
gs_crypt_accepted_chars[6]		= "("
gs_crypt_accepted_chars[7]		= ")"
gs_crypt_accepted_chars[8]		= "*"
gs_crypt_accepted_chars[9]		= "+"
gs_crypt_accepted_chars[10]	= ","
gs_crypt_accepted_chars[11]	= "-"
gs_crypt_accepted_chars[12]	= "."
gs_crypt_accepted_chars[13]	= "/"
gs_crypt_accepted_chars[14]	= "0"
gs_crypt_accepted_chars[15]	= "1"
gs_crypt_accepted_chars[16]	= "2"
gs_crypt_accepted_chars[17]	= "3"
gs_crypt_accepted_chars[18]	= "4"
gs_crypt_accepted_chars[19]	= "5"
gs_crypt_accepted_chars[20]	= "6"
gs_crypt_accepted_chars[21]	= "7"
gs_crypt_accepted_chars[22]	= "8"
gs_crypt_accepted_chars[23]	= "9"
gs_crypt_accepted_chars[24]	= ":"
gs_crypt_accepted_chars[25]	= ";"
gs_crypt_accepted_chars[26]	= "<"
gs_crypt_accepted_chars[27]	= "="
gs_crypt_accepted_chars[28]	= ">"
gs_crypt_accepted_chars[29]	= "?"
gs_crypt_accepted_chars[30]	= "@"
gs_crypt_accepted_chars[31]	= "A"
gs_crypt_accepted_chars[32]	= "B"
gs_crypt_accepted_chars[33]	= "C"
gs_crypt_accepted_chars[34]	= "D"
gs_crypt_accepted_chars[35]	= "E"
gs_crypt_accepted_chars[36]	= "F"
gs_crypt_accepted_chars[37]	= "G"
gs_crypt_accepted_chars[38]	= "H"
gs_crypt_accepted_chars[39]	= "I"
gs_crypt_accepted_chars[40]	= "J"
gs_crypt_accepted_chars[41]	= "K"
gs_crypt_accepted_chars[42]	= "L"
gs_crypt_accepted_chars[43]	= "M"
gs_crypt_accepted_chars[44]	= "N"
gs_crypt_accepted_chars[45]	= "O"
gs_crypt_accepted_chars[46]	= "P"
gs_crypt_accepted_chars[47]	= "Q"
gs_crypt_accepted_chars[48]	= "R"
gs_crypt_accepted_chars[49]	= "S"
gs_crypt_accepted_chars[50]	= "T"
gs_crypt_accepted_chars[51]	= "U"
gs_crypt_accepted_chars[52]	= "V"
gs_crypt_accepted_chars[53]	= "W"
gs_crypt_accepted_chars[54]	= "X"
gs_crypt_accepted_chars[55]	= "Y"
gs_crypt_accepted_chars[56]	= "Z"
gs_crypt_accepted_chars[57]	= "["
gs_crypt_accepted_chars[58]	= "\"
gs_crypt_accepted_chars[59]	= "]"
gs_crypt_accepted_chars[60]	= "^"
gs_crypt_accepted_chars[61]	= "_"
gs_crypt_accepted_chars[62]	= "a"
gs_crypt_accepted_chars[63]	= "b"
gs_crypt_accepted_chars[64]	= "c"
gs_crypt_accepted_chars[65]	= "d"
gs_crypt_accepted_chars[66]	= "e"
gs_crypt_accepted_chars[67]	= "f"
gs_crypt_accepted_chars[68]	= "g"
gs_crypt_accepted_chars[69]	= "h"
gs_crypt_accepted_chars[70]	= "i"
gs_crypt_accepted_chars[71]	= "j"
gs_crypt_accepted_chars[72]	= "k"
gs_crypt_accepted_chars[73]	= "l"
gs_crypt_accepted_chars[74]	= "m"
gs_crypt_accepted_chars[75]	= "n"
gs_crypt_accepted_chars[76]	= "o"
gs_crypt_accepted_chars[77]	= "p"
gs_crypt_accepted_chars[78]	= "q"
gs_crypt_accepted_chars[79]	= "r"
gs_crypt_accepted_chars[80]	= "s"
gs_crypt_accepted_chars[81]	= "t"
gs_crypt_accepted_chars[82]	= "u"
gs_crypt_accepted_chars[83]	= "v"
gs_crypt_accepted_chars[84]	= "w"
gs_crypt_accepted_chars[85]	= "x"
gs_crypt_accepted_chars[86]	= "y"
gs_crypt_accepted_chars[87]	= "z"
gs_crypt_accepted_chars[88]	= "{"
gs_crypt_accepted_chars[89]	= "|"
gs_crypt_accepted_chars[90]	= "}"
gs_crypt_accepted_chars[91]	= ""		// non usato
gs_crypt_accepted_chars[92]	= ""		// non usato
gs_crypt_accepted_chars[93]	= ""		// non usato
gs_crypt_accepted_chars[94]	= ""		// non usato
gs_crypt_accepted_chars[95]	= ""		// non usato
gs_crypt_accepted_chars[96]	= ""		// non usato
gs_crypt_accepted_chars[97]	= ""		// non usato
gs_crypt_accepted_chars[98]	= ""		// non usato
gs_crypt_accepted_chars[99]	= ""		// non usato
gs_crypt_accepted_chars[100]	= ""		// non usato

// impostazione codici numerici
// usiamo 5 array distinti in modo che lo stesso carattere assuma codici di crypt diversi a seconda della sua posizione
// nella stringa di origine. questo dovrebbe rendere più difficile individuare l'algoritmo applicato.
long ll_x, ll_y, ll_code

for ll_y = 1 to 5
	
	choose case ll_y
		case 1
			ll_code = 60
		case 2
			ll_code = 20
		case 3
			ll_code = 0
		case 4
			ll_code = 40
		case 5
			ll_code = 80
	end choose
	
	for ll_x = 1 to 100
		
		ll_code++
		
		if ll_code = 100 then
			ll_code = 0
		end if
		
		gl_crypt_code_table[ll_x,ll_y] = ll_code
		
	next
	
next
end subroutine

public subroutine uof_set_crypt_params_db ();// impostazione set caratteri accettati per l'algoritmo di crypt/decrypt
gs_crypt_accepted_chars[1]		= "!"
gs_crypt_accepted_chars[2]		= "#"
gs_crypt_accepted_chars[3]		= "$"
gs_crypt_accepted_chars[4]		= "%"
gs_crypt_accepted_chars[5]		= "&"
gs_crypt_accepted_chars[6]		= "("
gs_crypt_accepted_chars[7]		= ")"
gs_crypt_accepted_chars[8]		= "*"
gs_crypt_accepted_chars[9]		= "+"
gs_crypt_accepted_chars[10]	= ","
gs_crypt_accepted_chars[11]	= "-"
gs_crypt_accepted_chars[12]	= "."
gs_crypt_accepted_chars[13]	= "/"
gs_crypt_accepted_chars[14]	= "0"
gs_crypt_accepted_chars[15]	= "1"
gs_crypt_accepted_chars[16]	= "2"
gs_crypt_accepted_chars[17]	= "3"
gs_crypt_accepted_chars[18]	= "4"
gs_crypt_accepted_chars[19]	= "5"
gs_crypt_accepted_chars[20]	= "6"
gs_crypt_accepted_chars[21]	= "7"
gs_crypt_accepted_chars[22]	= "8"
gs_crypt_accepted_chars[23]	= "9"
gs_crypt_accepted_chars[24]	= ":"
gs_crypt_accepted_chars[25]	= ";"
gs_crypt_accepted_chars[26]	= "<"
gs_crypt_accepted_chars[27]	= "="
gs_crypt_accepted_chars[28]	= ">"
gs_crypt_accepted_chars[29]	= "?"
gs_crypt_accepted_chars[30]	= "@"
gs_crypt_accepted_chars[31]	= "A"
gs_crypt_accepted_chars[32]	= "B"
gs_crypt_accepted_chars[33]	= "C"
gs_crypt_accepted_chars[34]	= "D"
gs_crypt_accepted_chars[35]	= "E"
gs_crypt_accepted_chars[36]	= "F"
gs_crypt_accepted_chars[37]	= "G"
gs_crypt_accepted_chars[38]	= "H"
gs_crypt_accepted_chars[39]	= "I"
gs_crypt_accepted_chars[40]	= "J"
gs_crypt_accepted_chars[41]	= "K"
gs_crypt_accepted_chars[42]	= "L"
gs_crypt_accepted_chars[43]	= "M"
gs_crypt_accepted_chars[44]	= "N"
gs_crypt_accepted_chars[45]	= "O"
gs_crypt_accepted_chars[46]	= "P"
gs_crypt_accepted_chars[47]	= "Q"
gs_crypt_accepted_chars[48]	= "R"
gs_crypt_accepted_chars[49]	= "S"
gs_crypt_accepted_chars[50]	= "T"
gs_crypt_accepted_chars[51]	= "U"
gs_crypt_accepted_chars[52]	= "V"
gs_crypt_accepted_chars[53]	= "W"
gs_crypt_accepted_chars[54]	= "X"
gs_crypt_accepted_chars[55]	= "Y"
gs_crypt_accepted_chars[56]	= "Z"
gs_crypt_accepted_chars[57]	= "["
gs_crypt_accepted_chars[58]	= "\"
gs_crypt_accepted_chars[59]	= "]"
gs_crypt_accepted_chars[60]	= "^"
gs_crypt_accepted_chars[61]	= "_"
gs_crypt_accepted_chars[62]	= "a"
gs_crypt_accepted_chars[63]	= "b"
gs_crypt_accepted_chars[64]	= "c"
gs_crypt_accepted_chars[65]	= "d"
gs_crypt_accepted_chars[66]	= "e"
gs_crypt_accepted_chars[67]	= "f"
gs_crypt_accepted_chars[68]	= "g"
gs_crypt_accepted_chars[69]	= "h"
gs_crypt_accepted_chars[70]	= "i"
gs_crypt_accepted_chars[71]	= "j"
gs_crypt_accepted_chars[72]	= "k"
gs_crypt_accepted_chars[73]	= "l"
gs_crypt_accepted_chars[74]	= "m"
gs_crypt_accepted_chars[75]	= "n"
gs_crypt_accepted_chars[76]	= "o"
gs_crypt_accepted_chars[77]	= "p"
gs_crypt_accepted_chars[78]	= "q"
gs_crypt_accepted_chars[79]	= "r"
gs_crypt_accepted_chars[80]	= "s"
gs_crypt_accepted_chars[81]	= "t"
gs_crypt_accepted_chars[82]	= "u"
gs_crypt_accepted_chars[83]	= "v"
gs_crypt_accepted_chars[84]	= "w"
gs_crypt_accepted_chars[85]	= "x"
gs_crypt_accepted_chars[86]	= "y"
gs_crypt_accepted_chars[87]	= "z"
gs_crypt_accepted_chars[88]	= "{"
gs_crypt_accepted_chars[89]	= "|"
gs_crypt_accepted_chars[90]	= "}"
gs_crypt_accepted_chars[91]	= ""		// non usato
gs_crypt_accepted_chars[92]	= ""		// non usato
gs_crypt_accepted_chars[93]	= ""		// non usato
gs_crypt_accepted_chars[94]	= ""		// non usato
gs_crypt_accepted_chars[95]	= ""		// non usato
gs_crypt_accepted_chars[96]	= ""		// non usato
gs_crypt_accepted_chars[97]	= ""		// non usato
gs_crypt_accepted_chars[98]	= ""		// non usato
gs_crypt_accepted_chars[99]	= ""		// non usato
gs_crypt_accepted_chars[100]	= ""		// non usato

// impostazione codici numerici
// usiamo 5 array distinti in modo che lo stesso carattere assuma codici di crypt diversi a seconda della sua posizione
// nella stringa di origine. questo dovrebbe rendere più difficile individuare l'algoritmo applicato.
long ll_x, ll_y, ll_code

for ll_y = 1 to 5
	
	choose case ll_y
		case 1
			ll_code = 60
		case 2
			ll_code = 20
		case 3
			ll_code = 0
		case 4
			ll_code = 40
		case 5
			ll_code = 80
	end choose
	
	for ll_x = 1 to 100
		
		ll_code++
		
		if ll_code = 100 then
			ll_code = 0
		end if
		
		gl_crypt_code_table[ll_x,ll_y] = ll_code
		
	next
	
next
end subroutine

on uo_application.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_application.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

