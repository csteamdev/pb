﻿$PBExportHeader$uo_archivia_pdf.sru
forward
global type uo_archivia_pdf from nonvisualobject
end type
end forward

global type uo_archivia_pdf from nonvisualobject
end type
global uo_archivia_pdf uo_archivia_pdf

type variables
STRING IS_PATH
end variables

forward prototypes
public function integer uof_crea_pdf (uo_cs_xx_dw fdw_oggetto, ref blob f_blob)
public subroutine uof_imposta_dir ()
public function string uof_crea_pdf_path (uo_cs_xx_dw fdw_oggetto)
public function string uof_crea_pdf_path (datawindow fdw_oggetto)
public function integer uof_crea_pdf (datawindow fdw_oggetto, ref blob f_blob)
public function string uof_crea_pdf_path (uo_cs_xx_dw fdw_oggetto, string as_filename)
public function string uof_crea_pdf_path (datawindow fdw_oggetto, string fs_filename)
public function string uof_crea_pdf_path (uo_cs_xx_dw fdw_oggetto, boolean fb_hhmmss, string as_filename)
private function string uof_genera_nome_file (datawindow adw_dw)
end prototypes

public function integer uof_crea_pdf (uo_cs_xx_dw fdw_oggetto, ref blob f_blob);string ls_str, ls_p, ls_logo
long ll_ret
//datastore lds_datastore
//
//
//lds_datastore = create datastore
//
//lds_datastore.dataobject = fdw_oggetto.dataobject
//lds_datastore.object.data = fdw_oggetto.object.data
//
//lds_datastore.object.intestazione.filename = fdw_oggetto.object.intestazione.filename
//lds_datastore.object.piede.filename = fdw_oggetto.object.piede.filename

uof_imposta_dir()

ls_p = is_path + "cs" + string(Hour(Now())) + string(Minute(Now())) + string(Second(Now())) + ".pdf"

//ll_ret = lds_datastore.saveas(ls_p, pdf!, true)
ll_ret = fdw_oggetto.saveas(ls_p, pdf!, true)
if ll_ret < 0 then return -1

f_blob = f_carica_blob(ls_p)

filedelete(ls_p)

return 0
end function

public subroutine uof_imposta_dir ();string  ls_dir, ls_path, ls_default, ls_valore, ls_str
integer li_risposta


li_risposta = f_getuserpath(is_path) 

if li_risposta = 1 then return

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "VQL", ls_dir)	  // parametro registro VQL

setnull(is_path)

if isnull(ls_dir) then ls_dir = ""

select stringa
into   :ls_valore
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_parametro = 'TDD';

if sqlca.sqlcode = -1 then
	
	g_mb.messagebox("OMNIA","Attenzione: Errore nella select del parametro TDD! Dettaglio: " + sqlca.sqlerrtext)
	
	return
	
end if

if isnull(ls_valore) then ls_valore = ""

ls_valore = ls_dir + "\" + ls_valore

if ls_valore = "" then setnull(ls_valore)

if isnull(ls_valore) or ls_valore = "" then
	
	g_mb.messagebox("OMNIA","Attenzione: impostare il parametro aziendale TDD o il parametro del registro ris. ")
	
	return
	
end if

setnull(ls_str)

ls_str = right(ls_valore, 1)

if ls_str <> "\" then ls_valore = ls_valore + "\"

is_path = ls_valore

return
end subroutine

public function string uof_crea_pdf_path (uo_cs_xx_dw fdw_oggetto);string ls_p
long ll_ret

uof_imposta_dir()

//ls_p = is_path + "cs" + string(Hour(Now())) + string(Minute(Now())) + string(Second(Now())) + ".pdf"
ls_p = is_path + uof_genera_nome_file(fdw_oggetto) + ".pdf"

//ll_ret = lds_datastore.saveas(ls_p, pdf!, true)
ll_ret = fdw_oggetto.saveas(ls_p, pdf!, true)

if ll_ret < 0 then return "errore"

return ls_p
end function

public function string uof_crea_pdf_path (datawindow fdw_oggetto);string ls_p
long ll_ret

//datastore lds_datastore
//
//
//lds_datastore = create datastore
//
//lds_datastore.dataobject = fdw_oggetto.dataobject
//lds_datastore.object.data = fdw_oggetto.object.data
//TRY
//	lds_datastore.object.intestazione.filename = fdw_oggetto.object.intestazione.filename
//	lds_datastore.object.piede.filename = fdw_oggetto.object.piede.filename
//CATCH ( throwable e)
//END TRY

uof_imposta_dir()

ls_p = is_path + "cs" + string(Hour(Now())) + string(Minute(Now())) + string(Second(Now())) + ".pdf"

//ll_ret = lds_datastore.saveas(ls_p, pdf!, true)
ll_ret = fdw_oggetto.saveas(ls_p, pdf!, true)

if ll_ret < 0 then return "errore"

return ls_p
end function

public function integer uof_crea_pdf (datawindow fdw_oggetto, ref blob f_blob);string ls_str, ls_p, ls_logo
long ll_ret


//datastore lds_datastore
//
//
//lds_datastore = create datastore
//
//lds_datastore.dataobject = fdw_oggetto.dataobject
//lds_datastore.object.data = fdw_oggetto.object.data
//
//lds_datastore.object.intestazione.filename = fdw_oggetto.object.intestazione.filename
//lds_datastore.object.piede.filename = fdw_oggetto.object.piede.filename

uof_imposta_dir()

ls_p = is_path + "cs" + string(Hour(Now())) + string(Minute(Now())) + string(Second(Now())) + ".pdf"

//ll_ret = lds_datastore.saveas(ls_p, pdf!, true)
ll_ret = fdw_oggetto.saveas(ls_p, pdf!, true)
if ll_ret < 0 then return -1

f_blob = f_carica_blob(ls_p)

filedelete(ls_p)

return 0
end function

public function string uof_crea_pdf_path (uo_cs_xx_dw fdw_oggetto, string as_filename);// stefanop 26/07/2010: richiesto da BEATRICE: ticket 2010/213
string ls_p
long ll_ret

//datastore lds_datastore
//
//
//lds_datastore = create datastore
//
//lds_datastore.dataobject = fdw_oggetto.dataobject
//lds_datastore.object.data = fdw_oggetto.object.data
//TRY
//	lds_datastore.object.intestazione.filename = fdw_oggetto.object.intestazione.filename
//	lds_datastore.object.piede.filename = fdw_oggetto.object.piede.filename
//CATCH ( throwable e)
//END TRY
uof_imposta_dir()

ls_p = is_path + as_filename + ".pdf"

//ll_ret = lds_datastore.saveas(ls_p, pdf!, true)
ll_ret = fdw_oggetto.saveas(ls_p, pdf!, true)

if ll_ret < 0 then return "errore"

return ls_p
end function

public function string uof_crea_pdf_path (datawindow fdw_oggetto, string fs_filename);string ls_p
long ll_ret

//datastore lds_datastore
//
//
//lds_datastore = create datastore
//
//lds_datastore.dataobject = fdw_oggetto.dataobject
//lds_datastore.object.data = fdw_oggetto.object.data
//TRY
//	lds_datastore.object.intestazione.filename = fdw_oggetto.object.intestazione.filename
//	lds_datastore.object.piede.filename = fdw_oggetto.object.piede.filename
//CATCH ( throwable e)
//END TRY

uof_imposta_dir()

ls_p = is_path + fs_filename + "_" + string(Hour(Now())) + string(Minute(Now())) + string(Second(Now())) + ".pdf"

//ll_ret = lds_datastore.saveas(ls_p, pdf!, true)
ll_ret = fdw_oggetto.saveas(ls_p, pdf!, true)

if ll_ret < 0 then return "errore"

return ls_p
end function

public function string uof_crea_pdf_path (uo_cs_xx_dw fdw_oggetto, boolean fb_hhmmss, string as_filename);// stefanop 26/07/2010: richiesto da BEATRICE: ticket 2010/213
string ls_p
long ll_ret

//datastore lds_datastore
//
//
//lds_datastore = create datastore
//
//lds_datastore.dataobject = fdw_oggetto.dataobject
//lds_datastore.object.data = fdw_oggetto.object.data
//TRY
//	lds_datastore.object.intestazione.filename = fdw_oggetto.object.intestazione.filename
//	lds_datastore.object.piede.filename = fdw_oggetto.object.piede.filename
//CATCH ( throwable e)
//END TRY
uof_imposta_dir()

ls_p = is_path + as_filename

if fb_hhmmss then
	ls_p += "_" + string(Hour(Now())) + string(Minute(Now())) + string(Second(Now()))
end if

ls_p += ".pdf"

//ll_ret = lds_datastore.saveas(ls_p, pdf!, true)
ll_ret = fdw_oggetto.saveas(ls_p, pdf!, true)

if ll_ret < 0 then return "errore"

return ls_p
end function

private function string uof_genera_nome_file (datawindow adw_dw);/**
 * stefanop
 * 04/07/2014
 *
 * Recupero il nome del file da salvare controllando prima il nome della stampa
 * della DW e nel caso sia vuoto genero un nome causale
 **/
 
string ls_value

ls_value = adw_dw.Object.DataWindow.Print.DocumentName

if g_str.isempty(ls_value) then
	ls_value = "cs" + string(Hour(Now())) + string(Minute(Now())) + string(Second(Now()))
else
	ls_value =  guo_functions.uof_sanatize_filename(ls_value)
end if

return ls_value
end function

on uo_archivia_pdf.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_archivia_pdf.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

