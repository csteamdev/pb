﻿$PBExportHeader$uo_ascii.sru
forward
global type uo_ascii from uo_file
end type
end forward

global type uo_ascii from uo_file
end type
global uo_ascii uo_ascii

type variables
private:
	string is_filepath
	long ai_handle
end variables

forward prototypes
public function boolean open (string as_filepath)
private function integer write (string as_text)
public function uo_ascii append (string as_value, integer ai_empty_space)
public function uo_ascii new_line ()
public function uo_ascii append (string as_value)
end prototypes

public function boolean open (string as_filepath);/**
 * stefanop
 * 06/10/2014
 *
 * Crea il file
 **/
int li_file

if FileExists(as_filepath) then
	FileDelete(as_filepath)
end if

if not FileExists(as_filepath) then
	li_file = fileopen(as_filepath, LineMode!, write!)
	FileClose(li_file)
end if

is_filepath = as_filepath
 
return true
end function

private function integer write (string as_text);int ll_handle, li_ret

try 
	ll_handle = fileopen(is_filepath, TextMode!, Write!, LockWrite!, Append!)
	li_ret = FileWriteEx (ll_handle, as_text)
	li_ret = fileclose(ll_handle)
catch(RuntimeError e)
end try

return 0
end function

public function uo_ascii append (string as_value, integer ai_empty_space);string ls_full_value

ls_full_value = space(ai_empty_space)

if g_str.isnotempty(as_value) then
	ls_full_value = left(as_value + ls_full_value, ai_empty_space)
end if

write(ls_full_value)

return this
end function

public function uo_ascii new_line ();
write("~r~n")

return this
end function

public function uo_ascii append (string as_value);write(as_value)

return this
end function

on uo_ascii.create
call super::create
end on

on uo_ascii.destroy
call super::destroy
end on

