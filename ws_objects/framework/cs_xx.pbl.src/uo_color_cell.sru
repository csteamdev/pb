﻿$PBExportHeader$uo_color_cell.sru
forward
global type uo_color_cell from userobject
end type
type r_color from statictext within uo_color_cell
end type
type r_border from rectangle within uo_color_cell
end type
end forward

global type uo_color_cell from userobject
integer width = 96
integer height = 84
long backcolor = 67108864
string text = "none"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
event ue_click pbm_lbaddstring
event clicked ( long ai_color )
r_color r_color
r_border r_border
end type
global uo_color_cell uo_color_cell

type variables
public:
	long il_color
	boolean ib_fix_size = true
	
private:
	long il_border_color_focused = 33023
	long il_border_color = 10526880
end variables

forward prototypes
public subroutine uof_set_color (long ai_color)
public function long uof_get_color ()
end prototypes

event clicked(long ai_color);/**
 * stefanop
 * 01/10/2010
 *
 * Ritorna il colore impostato nel controllo
 **/
end event

public subroutine uof_set_color (long ai_color);il_color = ai_color
r_color.backcolor = il_color
end subroutine

public function long uof_get_color ();return il_color
end function

on uo_color_cell.create
this.r_color=create r_color
this.r_border=create r_border
this.Control[]={this.r_color,&
this.r_border}
end on

on uo_color_cell.destroy
destroy(this.r_color)
destroy(this.r_border)
end on

event constructor;
if ib_fix_size then
	// imposto la dimensione fissa a 20x20 pixel
	width = 96
	height = 84
else
	r_border.move(0,0)
	r_border.resize(width, height)
	r_color.move(9, 8)
	r_color.resize(r_border.width + -18, r_border.height - 16)
end if



r_color.backcolor = il_color
end event

type r_color from statictext within uo_color_cell
integer x = 9
integer y = 8
integer width = 78
integer height = 68
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 134217741
long bordercolor = 134217741
boolean focusrectangle = false
end type

event clicked;/**
 * stefanop
 * 01/10/2010
 *
 * Ritorna il colore impostato nel controllo
 **/
 
event clicked(il_color)
end event

event getfocus;r_border.linecolor = il_border_color_focused
end event

event losefocus;r_border.linecolor = il_border_color
end event

type r_border from rectangle within uo_color_cell
long linecolor = 8421504
integer linethickness = 2
long fillcolor = 16777215
integer width = 96
integer height = 84
end type

