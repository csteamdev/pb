﻿$PBExportHeader$uo_color_tab.sru
forward
global type uo_color_tab from userobject
end type
type cb_other from commandbutton within uo_color_tab
end type
type uo_20 from uo_color_cell within uo_color_tab
end type
type uo_19 from uo_color_cell within uo_color_tab
end type
type uo_18 from uo_color_cell within uo_color_tab
end type
type uo_17 from uo_color_cell within uo_color_tab
end type
type uo_16 from uo_color_cell within uo_color_tab
end type
type uo_15 from uo_color_cell within uo_color_tab
end type
type uo_14 from uo_color_cell within uo_color_tab
end type
type uo_13 from uo_color_cell within uo_color_tab
end type
type uo_12 from uo_color_cell within uo_color_tab
end type
type uo_11 from uo_color_cell within uo_color_tab
end type
type uo_10 from uo_color_cell within uo_color_tab
end type
type uo_9 from uo_color_cell within uo_color_tab
end type
type uo_8 from uo_color_cell within uo_color_tab
end type
type uo_7 from uo_color_cell within uo_color_tab
end type
type uo_6 from uo_color_cell within uo_color_tab
end type
type uo_5 from uo_color_cell within uo_color_tab
end type
type uo_4 from uo_color_cell within uo_color_tab
end type
type uo_3 from uo_color_cell within uo_color_tab
end type
type uo_2 from uo_color_cell within uo_color_tab
end type
type uo_1 from uo_color_cell within uo_color_tab
end type
end forward

global type uo_color_tab from userobject
integer width = 1193
integer height = 184
long backcolor = 67108864
string text = "none"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
event ue_selected_color ( long ai_color )
cb_other cb_other
uo_20 uo_20
uo_19 uo_19
uo_18 uo_18
uo_17 uo_17
uo_16 uo_16
uo_15 uo_15
uo_14 uo_14
uo_13 uo_13
uo_12 uo_12
uo_11 uo_11
uo_10 uo_10
uo_9 uo_9
uo_8 uo_8
uo_7 uo_7
uo_6 uo_6
uo_5 uo_5
uo_4 uo_4
uo_3 uo_3
uo_2 uo_2
uo_1 uo_1
end type
global uo_color_tab uo_color_tab

type variables
private:
	long il_color
	
	//il_color = uof_get_color()
end variables

event ue_selected_color(long ai_color);/**
 * stefanop
 * 01/01/2010
 *
 * Evento generato al click su di un colore
 **/
end event

on uo_color_tab.create
this.cb_other=create cb_other
this.uo_20=create uo_20
this.uo_19=create uo_19
this.uo_18=create uo_18
this.uo_17=create uo_17
this.uo_16=create uo_16
this.uo_15=create uo_15
this.uo_14=create uo_14
this.uo_13=create uo_13
this.uo_12=create uo_12
this.uo_11=create uo_11
this.uo_10=create uo_10
this.uo_9=create uo_9
this.uo_8=create uo_8
this.uo_7=create uo_7
this.uo_6=create uo_6
this.uo_5=create uo_5
this.uo_4=create uo_4
this.uo_3=create uo_3
this.uo_2=create uo_2
this.uo_1=create uo_1
this.Control[]={this.cb_other,&
this.uo_20,&
this.uo_19,&
this.uo_18,&
this.uo_17,&
this.uo_16,&
this.uo_15,&
this.uo_14,&
this.uo_13,&
this.uo_12,&
this.uo_11,&
this.uo_10,&
this.uo_9,&
this.uo_8,&
this.uo_7,&
this.uo_6,&
this.uo_5,&
this.uo_4,&
this.uo_3,&
this.uo_2,&
this.uo_1}
end on

on uo_color_tab.destroy
destroy(this.cb_other)
destroy(this.uo_20)
destroy(this.uo_19)
destroy(this.uo_18)
destroy(this.uo_17)
destroy(this.uo_16)
destroy(this.uo_15)
destroy(this.uo_14)
destroy(this.uo_13)
destroy(this.uo_12)
destroy(this.uo_11)
destroy(this.uo_10)
destroy(this.uo_9)
destroy(this.uo_8)
destroy(this.uo_7)
destroy(this.uo_6)
destroy(this.uo_5)
destroy(this.uo_4)
destroy(this.uo_3)
destroy(this.uo_2)
destroy(this.uo_1)
end on

type cb_other from commandbutton within uo_color_tab
integer x = 1056
integer width = 137
integer height = 180
integer taborder = 210
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "altri"
end type

event clicked;event ue_selected_color(f_scegli_colori(0))
end event

type uo_20 from uo_color_cell within uo_color_tab
integer x = 210
integer y = 92
integer taborder = 130
long il_color = 5733049
end type

on uo_20.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_19 from uo_color_cell within uo_color_tab
integer x = 315
integer y = 92
integer taborder = 140
long il_color = 13217535
end type

on uo_19.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_18 from uo_color_cell within uo_color_tab
integer x = 421
integer y = 92
integer taborder = 150
long il_color = 969215
end type

on uo_18.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_17 from uo_color_cell within uo_color_tab
integer x = 526
integer y = 92
integer taborder = 160
long il_color = 11592943
end type

on uo_17.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_16 from uo_color_cell within uo_color_tab
integer x = 946
integer y = 92
integer taborder = 200
long il_color = 15187912
end type

on uo_16.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_15 from uo_color_cell within uo_color_tab
integer x = 736
integer y = 92
integer taborder = 180
long il_color = 15391129
end type

on uo_15.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_14 from uo_color_cell within uo_color_tab
integer x = 631
integer y = 92
integer taborder = 170
long il_color = 1959605
end type

on uo_14.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_13 from uo_color_cell within uo_color_tab
integer x = 841
integer y = 92
integer taborder = 190
long il_color = 12489328
end type

on uo_13.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_12 from uo_color_cell within uo_color_tab
integer x = 841
integer taborder = 90
long il_color = 13387839
end type

on uo_12.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_11 from uo_color_cell within uo_color_tab
integer x = 631
integer taborder = 70
long il_color = 5026082
end type

on uo_11.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_10 from uo_color_cell within uo_color_tab
integer x = 736
integer taborder = 80
long il_color = 15245824
end type

on uo_10.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_9 from uo_color_cell within uo_color_tab
integer x = 946
integer taborder = 100
long il_color = 10766755
end type

on uo_9.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_8 from uo_color_cell within uo_color_tab
integer x = 526
integer taborder = 60
long il_color = 62207
end type

on uo_8.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_7 from uo_color_cell within uo_color_tab
integer x = 421
integer taborder = 50
long il_color = 2588671
end type

on uo_7.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_6 from uo_color_cell within uo_color_tab
integer x = 315
integer taborder = 40
long il_color = 2366701
end type

on uo_6.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_5 from uo_color_cell within uo_color_tab
integer x = 210
integer taborder = 30
long il_color = 1376392
end type

on uo_5.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_4 from uo_color_cell within uo_color_tab
integer x = 105
integer y = 92
integer taborder = 120
long il_color = 12829635
end type

on uo_4.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_3 from uo_color_cell within uo_color_tab
integer x = 105
integer taborder = 20
long il_color = 8355711
end type

on uo_3.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_2 from uo_color_cell within uo_color_tab
integer y = 92
integer taborder = 110
long il_color = 16777215
end type

on uo_2.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

type uo_1 from uo_color_cell within uo_color_tab
integer taborder = 10
end type

on uo_1.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;event ue_selected_color(ai_color)
end event

