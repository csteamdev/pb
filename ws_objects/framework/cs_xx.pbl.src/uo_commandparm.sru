﻿$PBExportHeader$uo_commandparm.sru
forward
global type uo_commandparm from nonvisualobject
end type
end forward

global type uo_commandparm from nonvisualobject
end type
global uo_commandparm uo_commandparm

type variables
/**
 * Divide l'intera commandparm in stringe per ogni -X= trovato
 *
 * Esempio: csteam.exe P=10 U=stefanop S=file.txt,2,10/10/2012 Z
 * Ritornina: [1] = P=10
 *				 [2] = U=stefanop
 *				 [3] = S=file.txt,2,10/10/2012
 *				 [4] = Z
 **/
constant string REGEX_OPTIONS = "(\w=?)((([\'[\w\s.]*\')|(\~"[\w\s.]*\~")|([\/\w-_,.]+))*)"

/**
 * Divide l'intera stringa dell'opzione in singoli parametri
 *
 * Esempio
 * U=file.txt,'mario rossi',09/11/2012
 * Ritorna: [1] = U
 *			   [2] = file.txt
 *			   [3] = mario rossi
 *			   [4] = 09/11/2012
 **/
constant string REGEX_PARAM = "([\/a-z0-9-_\s.])+"


private:
	uo_regex iuo_regex
	
	string is_command_lines[]




end variables

forward prototypes
public function boolean uof_have_option (string as_option)
public function uo_commandparm_option uof_get_option (string as_option)
public function boolean uof_have_option (string as_option, ref integer ai_index)
public function uo_commandparm_option uof_get_option_with_cast (string as_option, uo_commandparm_option ab_parent)
end prototypes

public function boolean uof_have_option (string as_option);/**
 * stefanop
 * 09/11/2012
 *
 * Controlloa se nella command parm è presente l'opzione passata.
 * Ad esempio: uof_have_option("A") ritorna true se cs_team.exe -A
 **/
 
int li_i

return uof_have_option(as_option, ref li_i)
end function

public function uo_commandparm_option uof_get_option (string as_option);/**
 * stefanop
 * 09/11/2012
 *
 * Ritorna l'opzione con tutti i suoi parametri
 **/

int li_option_count, li_i
uo_commandparm_option luo_option

luo_option = create uo_commandparm_option

return uof_get_option_with_cast(as_option, luo_option)
end function

public function boolean uof_have_option (string as_option, ref integer ai_index);/**
 * stefanop
 * 09/11/2012
 *
 * Controlloa se nella command parm è presente l'opzione passata.
 * Ad esempio: uof_have_option("A") ritorna true se cs_team.exe -A
 **/
 
int li_i

for li_i = 1 to upperbound(is_command_lines)
	
	if g_str.start_with(is_command_lines[li_i], as_option) then
		ai_index = li_i
		return true
	end if
	
next

return false
end function

public function uo_commandparm_option uof_get_option_with_cast (string as_option, uo_commandparm_option ab_parent);/**
 * stefanop
 * 09/11/2012
 *
 * Ritorna l'opzione con tutti i suoi parametri.
 * Viene passato l'oggetto padre per effettuare cast futuri su altri oggetti.
 * Vedi uo_service_manager
 **/

int li_option_count, li_i

if uof_have_option(as_option, li_i) then
	
	if iuo_regex.initialize(REGEX_PARAM, true , false) then
		
		li_option_count = iuo_regex.search(is_command_lines[li_i])
		
		if li_option_count > 0 then
			
			ab_parent.uof_set_option_name(iuo_regex.match(1))
			
			for li_i=2 to li_option_count
				
				ab_parent.uo_add_param(iuo_regex.match(li_i))
				
			next
			
		end if
		
	else
		g_mb.error(iuo_regex.getlasterror( ))
	end if
	
end if

return ab_parent
end function

on uo_commandparm.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_commandparm.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;string ls_commandparm
int li_command_line_count, li_i

ls_commandparm = commandparm()
// DEBUG
//ls_commandparm  ="P=999 S=test'mario rossi',2.12,09/11/2012,-1,file.txt X Z L=99"
//ls_commandparm = "S=avan_prod,A01,60,2016,1564"

// Per Nova Facility, esegue i report richiesti dalla APP
//ls_commandparm = "S=report_man,A01,N,5"

// Per calendario produzione
//ls_commandparm = "S=cal_prod,A01"

if not isnull(ls_commandparm) and ls_commandparm <> "" then
	
	iuo_regex = create uo_regex
	
	// Estraggo tutte le string X=XXXX
	if iuo_regex.initialize(REGEX_OPTIONS, true , false) then
		
		li_command_line_count = iuo_regex.search(ls_commandparm)
		
		for li_i=1 to li_command_line_count
			
			is_command_lines[li_i] = iuo_regex.match(li_i)
			
		next
		
	else
		g_mb.error(iuo_regex.getlasterror( ))
	end if
end if
end event

event destructor;destroy iuo_regex
end event

