﻿$PBExportHeader$uo_commandparm_option.sru
forward
global type uo_commandparm_option from nonvisualobject
end type
end forward

global type uo_commandparm_option from nonvisualobject
end type
global uo_commandparm_option uo_commandparm_option

type variables
protected:
 	string is_option_name
 	string is_params[]
	int ii_param_count = 0
	
end variables

forward prototypes
public function string uof_get_option_name ()
public subroutine uof_set_option_name (string as_option_name)
public subroutine uo_add_param (string as_value)
public function string uof_get_string (integer ai_position, string as_default)
public function string uof_get_string (integer ai_position)
private function any uof_get_param (integer ai_position, any aa_default)
public function date uof_get_date (integer ai_position, date as_default)
public function integer uof_get_int (integer ai_position, integer as_default)
public function long uof_get_long (integer ai_position, long as_default)
public function date uof_get_date (integer ai_position)
public function integer uof_get_int (integer ai_position)
public function long uof_get_long (integer ai_position)
public function decimal uof_get_decimal (integer ai_position)
public function decimal uof_get_decimal (integer ai_position, decimal ad_default)
end prototypes

public function string uof_get_option_name ();return is_option_name
end function

public subroutine uof_set_option_name (string as_option_name); is_option_name = as_option_name
end subroutine

public subroutine uo_add_param (string as_value);is_params[upperbound(is_params) + 1] = as_value
end subroutine

public function string uof_get_string (integer ai_position, string as_default);/**
 * stefanop
 * 09/11/2012
 *
 * Recupera il parmetro in formato stringa se valido oppure usa quello di default
 **/
 
 return  string(uof_get_param(ai_position, as_default))
end function

public function string uof_get_string (integer ai_position);/**
 * stefanop
 * 09/11/2012
 *
 * Recupera il parmetro in formato stringa se valido oppure usa quello di default
 **/
 
string ls_empty

setnull(ls_empty)

return uof_get_string(ai_position, ls_empty)
end function

private function any uof_get_param (integer ai_position, any aa_default);/**
 * stefanop
 * 09/11/2012
 *
 * Recupera il parmetro in formato stringa se valido oppure usa quello di default
 **/
 
if ai_position > 0 and ai_position <= upperbound(is_params) then
	
	if isnull(is_params[ai_position]) or is_params[ai_position] = "" then
		return aa_default
	else
		return is_params[ai_position]
	end if
	
else
	return aa_default
end if

end function

public function date uof_get_date (integer ai_position, date as_default);/**
 * stefanop
 * 09/11/2012
 *
 * Recupera il parmetro in formato stringa se valido oppure usa quello di default
 **/
 
 return  date(uof_get_param(ai_position, as_default))
end function

public function integer uof_get_int (integer ai_position, integer as_default);/**
 * stefanop
 * 09/11/2012
 *
 * Recupera il parmetro in formato stringa se valido oppure usa quello di default
 **/
 
 return  integer(uof_get_param(ai_position, as_default))
end function

public function long uof_get_long (integer ai_position, long as_default);/**
 * stefanop
 * 09/11/2012
 *
 * Recupera il parmetro in formato stringa se valido oppure usa quello di default
 **/
 
 return  long(uof_get_param(ai_position, as_default))
end function

public function date uof_get_date (integer ai_position);/**
 * stefanop
 * 09/11/2012
 *
 * Recupera il parmetro in formato stringa se valido oppure usa quello di default
 **/
 
date ls_empty

setnull(ls_empty)

return uof_get_date(ai_position, ls_empty)
end function

public function integer uof_get_int (integer ai_position);/**
 * stefanop
 * 09/11/2012
 *
 * Recupera il parmetro in formato stringa se valido oppure usa quello di default
 **/
 
integer ls_empty

setnull(ls_empty)

return uof_get_int(ai_position, ls_empty)
end function

public function long uof_get_long (integer ai_position);/**
 * stefanop
 * 09/11/2012
 *
 * Recupera il parmetro in formato stringa se valido oppure usa quello di default
 **/
 
long ls_empty

setnull(ls_empty)

return uof_get_long(ai_position, ls_empty)
end function

public function decimal uof_get_decimal (integer ai_position);/**
 * stefanop
 * 09/11/2012
 *
 * Recupera il parmetro in formato stringa se valido oppure usa quello di default
 **/
 
decimal ls_empty

setnull(ls_empty)

return uof_get_decimal(ai_position, ls_empty)
end function

public function decimal uof_get_decimal (integer ai_position, decimal ad_default);/**
 * stefanop
 * 09/11/2012
 *
 * Recupero parametro decimal
 * Caso particolare perchè devo verificare il regional settings per fare 
 * la conversione corretta
 **/
 
string ls_value, ls_empty
 
setnull(ls_empty)
ls_value =  string(uof_get_param(ai_position, ls_empty))
 
 if isnull(ls_value) or ls_value = "" then
	return ad_default
else
	
	if 2.12 <> dec("2.12") then
		ls_value = g_str.replace(ls_value, ".", ",")
	end if
	
	return dec(ls_value)
	
end if
end function

on uo_commandparm_option.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_commandparm_option.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

