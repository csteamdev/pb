﻿$PBExportHeader$uo_commandparm_service.sru
forward
global type uo_commandparm_service from uo_commandparm_option
end type
end forward

global type uo_commandparm_service from uo_commandparm_option
end type
global uo_commandparm_service uo_commandparm_service

type variables
/**
 * Estense l'oggeto uo_commandparm_option
 * per semplicare l'utilizzo della riga di comando
 * nel caso sia un servizio da eseguire e chiudere
 **/
 
private:
	string is_service_name
end variables

forward prototypes
public function string uof_get_service_name ()
public subroutine uof_init ()
end prototypes

public function string uof_get_service_name ();return is_service_name
end function

public subroutine uof_init ();/**
 * stefanop
 * 09/11/2012
 *
 * Questo oggetto semplifica l'utilizzo della commandparm option se servizio
 * La commandparm di servizio ha il primo parametro che identifica il nome del servizio
 * e quindi l'array dei parametri viene traslato di 1
 **/

string is_temp_param[]
int li_upperbound, li_i
 
is_service_name = uof_get_string(1)
li_upperbound = upperbound(is_params)

if li_upperbound > 1 then
	
	// Shifto i parametri
	for li_i=2 to li_upperbound
		
		is_temp_param[li_i - 1] = is_params[li_i]
		
	next
	
end if

is_params = is_temp_param


end subroutine

on uo_commandparm_service.create
call super::create
end on

on uo_commandparm_service.destroy
call super::destroy
end on

