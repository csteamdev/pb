﻿$PBExportHeader$uo_crypto.sru
forward
global type uo_crypto from nonvisualobject
end type
end forward

global type uo_crypto from nonvisualobject
end type
global uo_crypto uo_crypto

type prototypes
// GENERAL
Function Boolean CryptAcquireContext (ref ulong phProv, string pszContainer, string pszProvider, ulong dwProvType, ulong dwFlags) Library "advapi32.dll" Alias For "CryptAcquireContextW"
Function Boolean CryptReleaseContext (ulong hProv, ulong dwFlags) Library "advapi32.dll" Alias For "CryptReleaseContext"

// HASH
Function Boolean CryptCreateHash (ulong hProv, ulong Algid, ulong hKey, ulong dwFlags, ref ulong phHash) Library "advapi32.dll" Alias For "CryptCreateHash;Ansi"
Function Boolean CryptHashData (ulong hHash, ref string pbData, ulong dwDataLen, ulong dwFlags) Library "advapi32.dll" Alias For "CryptHashData;Ansi"
Function Boolean CryptGetHashParam (uLong hHash, uLong dwParam, ref int pbData, ref uLong dwDataLength, uLong dwFlags) Library "advapi32.dll" Alias for "CryptGetHashParam;Ansi"
Function Boolean CryptGetHashParam (uLong hHash, uLong dwParam, ref byte pbData[], ref uLong dwDataLength, uLong dwFlags) Library "advapi32.dll" Alias for "CryptGetHashParam;Ansi"
Function Boolean CryptDestroyHash (ulong hHash) Library "advapi32.dll" Alias For "CryptDestroyHash"

// CRYPT
Function Boolean CryptDeriveKey (ulong hProv, ulong Algid, ulong hBaseData, ulong dwFlags, ref ulong phKey) Library "advapi32.dll" Alias For "CryptDeriveKey"
Function Boolean CryptDestroyKey (ulong hKey) Library "advapi32.dll" Alias For "CryptDestroyKey"
Function Boolean CryptEncrypt (ulong hKey, ulong hHash, boolean Final, ulong dwFlags, ref blob pbData, ref ulong pdwDataLen, ulong dwBufLen) Library "advapi32.dll" Alias For "CryptEncrypt"
Function Boolean CryptDecrypt (ulong hKey, ulong hHash, boolean Final, ulong dwFlags,ref blob pbData, ref ulong pdwDataLen) Library "advapi32.dll" Alias For "CryptDecrypt"
Function Boolean CryptBinaryToString (Blob pbBinary, long cbBinary, ulong dwFlags, Ref string pszString, Ref ulong pcchString) Library "crypt32.dll" Alias For "CryptBinaryToStringW"
Function Boolean CryptStringToBinary (string pszString, ulong cchString, ulong dwFlags, Ref blob pbBinary, Ref ulong pcbBinary, Ref ulong pdwSkip, Ref ulong pdwFlags) Library "crypt32.dll" Alias For "CryptStringToBinaryW"

// PROVIDER
Function Boolean CryptGetDefaultProvider (ulong dwProvType, ulong pdwReserved, ulong dwFlags, ref string pszProvName, ref ulong pcbProvName) Library "advapi32.dll" Alias For "CryptGetDefaultProviderW"
Function Boolean CryptEnumProviders (ulong dwIndex, ulong pdwReserved, ulong dwFlags, Ref ulong pdwProvType, Ref string pszProvName, Ref ulong pcbProvName) Library "advapi32.dll" Alias For "CryptEnumProvidersW"

// ERROR
Function long GetLastError( ) Library "kernel32.dll"
Function long FormatMessage(long dwFlags, long lpSource, long dwMessageId, long dwLanguageId, Ref string lpBuffer, long nSize, long Arguments) Library "kernel32.dll" Alias For "FormatMessageW"



end prototypes

type variables
// http://msdn.microsoft.com/en-us/library/ms867086.aspx

private:
	string is_last_error = ""
	boolean ib_have_error = false
	
	string SERVICE_PROVIDER = "Microsoft Base Cryptographic Provider v1.0"
	constant string KEY_CONTAINER = "12xx36zz"
//	constant ULong PROV_RSA_FULL = 1
	constant ULong PROV_RSA_FULL = 24
	constant ULong CRYPT_NEWKEYSET = 8
	constant ULong CRYPT_USER_DEFAULT = 2
	constant ULong CRYPT_STRING_BASE64	= 1
	
	constant ulong HP_HASHVAL = 2
	constant ulong HP_HASHSIZE = 4
	
	char HexDigits[0 TO 15] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'}
		
	// Encryption Algorithms
	// see http://msdn.microsoft.com/en-us/library/aa375549%28v=vs.85%29.aspx for more details
	Constant ULong CALG_3DES				= 26115
	Constant ULong CALG_3DES_112		= 26121
	Constant ULong CALG_AES				= 26129
	Constant ULong CALG_AES_128		= 26126
	Constant ULong CALG_AES_192		= 26127
	Constant ULong CALG_AES_256		= 26128
	Constant ULong CALG_CYLINK_MEK	= 26124
	Constant ULong CALG_DES				= 26113
	Constant ULong CALG_DESX				= 26116
	Constant ULong CALG_DH_EPHEM		= 43522
	Constant ULong CALG_DH_SF			= 43521
	Constant ULong CALG_DSS_SIGN		= 8704
	Constant ULong CALG_RC2				= 26114
	Constant ULong CALG_RC4				= 26625
	Constant ULong CALG_RC5				= 26125
	Constant ULong CALG_RSA_KEYX		= 41984
	Constant ULong CALG_RSA_SIGN		= 9216
	
	// Hashing Algorithms
	// see http://msdn.microsoft.com/en-us/library/aa375549%28v=vs.85%29.aspx for more details
	Constant ULong CALG_HUGHES_MD5	= 40963
	Constant ULong CALG_HMAC			= 32777
	Constant ULong CALG_MAC				= 32773
	Constant ULong CALG_MD2				= 32769
	Constant ULong CALG_MD5				= 32771
	Constant ULong CALG_SHA				= 32772
	Constant ULong CALG_SHA1				= 32772
	Constant ULong CALG_SHA_256		= 32780
	Constant ULong CALG_SHA_384		= 32781
	Constant ULong CALG_SHA_512		= 32782
	
end variables

forward prototypes
private function string uof_to_hex (byte ai_data[])
public function string uof_sha1 (string as_data)
public function string uof_md5 (string as_data)
private function string uof_to_hex (string as_data)
public function string uof_encode_64 (string as_data)
public function string uof_encrypt (string as_data, string as_password)
private function blob uof_encrypt_decrypt (string as_data, string as_password, boolean ab_encrypt)
public function string uof_decrypt (string as_data, string as_password)
public function string uof_encode_64 (blob ablb_data)
public function blob uof_decode_64 (string as_data_encoded)
private function string uof_hash (string as_data, unsignedlong ai_alg_id)
public function string uof_decrypt (blob ablb_data, string as_password)
private subroutine uof_get_lasterror (ref long al_error, ref string as_msgtext)
private function string uof_get_defaultprovider ()
public function boolean uof_error (ref string as_error_message)
public function string uof_sha_256 (string as_data)
public function string uof_sha_512 (string as_data)
public function string uof_encode_64 (string as_data, integer ai_encode_type)
end prototypes

private function string uof_to_hex (byte ai_data[]);/**
 * stefanop
 * 28/09/2010
 *
 * Converte un sequenza di byte in caratteri esadecimali
 **/
 
string ls_hash
int li_i, li_n, li_r, li_l

for li_i = 1 TO upperbound(ai_data)
	li_n = ai_data[li_i]
	li_r = Mod (li_n, 16) // right 4 bits
	li_l = li_n / 16 // left 4 bits
	ls_hash += HexDigits [li_l] + HexDigits [li_r]
next

return ls_hash
end function

public function string uof_sha1 (string as_data);/**
 * stefanop
 * 28/09/2010
 *
 * Ritorna l'hash (SHA1) di una determinata stringa
 **/
 
return this.uof_hash(as_data, CALG_SHA1)
end function

public function string uof_md5 (string as_data);/**
 * stefanop
 * 28/09/2010
 *
 * Ritorna l'hash (MD5) di una determinata stringa
 **/
 
return this.uof_hash(as_data, CALG_MD5)
end function

private function string uof_to_hex (string as_data);/**
 * stefanop
 * 28/09/2010
 *
 * Converte un sequenza di byte in caratteri esadecimali
 **/
 
string ls_hash
int li_i, li_n, li_r, li_l

for li_i = 1 TO len(as_data)
	li_n = asc(mid(as_data, li_i, 1))
	li_r = Mod (li_n, 16) // right 4 bits
	li_l = li_n / 16 // left 4 bits
	ls_hash += HexDigits [li_l] + HexDigits [li_r]
next

return ls_hash
end function

public function string uof_encode_64 (string as_data);String ls_encoded
ULong lul_len, lul_buflen
Boolean lb_rtn

blob lblb_data

return uof_encode_64(as_data, 1)

end function

public function string uof_encrypt (string as_data, string as_password);/** 
 * stefanop
 * 29/09/2010
 *
 * Funzione che crypta una stringa con la password passata
 **/

return this.uof_encode_64(this.uof_encrypt_decrypt(as_data, as_password, true))
end function

private function blob uof_encrypt_decrypt (string as_data, string as_password, boolean ab_encrypt);string ls_null, ls_datatemp
ulong hProv, hHash, hKey, lul_dataLength, lul_bufferLength
boolean bResult
blob lblb_data
	
setnull(ls_null)
ib_have_error = false

// Attempt to acquire a handle to the default key container.
if not CryptAcquireContext(ref hProv, ls_null, SERVICE_PROVIDER, PROV_RSA_FULL, 0) then	
	if not CryptAcquireContext(ref hProv, ls_null, SERVICE_PROVIDER, PROV_RSA_FULL, CRYPT_NEWKEYSET) then
		is_last_error = "Error during CryptAcquireContext for a new key container." + "~r~nA container with this name probably already exists."
		ib_have_error = true
	end if
end if

// Obtain handle to hash object.
if CryptCreateHash(hProv, CALG_MD5, 0, 0, ref hHash) then
	
	// Hash data.
	if CryptHashData(hHash, as_password, len(as_password), 0) then
		
		// Derive a session key from the hash object.
		if CryptDeriveKey(hProv, CALG_3DES, hHash, 1, hKey) then
			
			// Alloc buffer
			lblb_data = blob(as_data, EncodingAnsi!)
			lul_dataLength = len(lblb_data)
			lblb_data = blob(as_data, EncodingAnsi!) + blob(space(8))
			lul_bufferLength = len(lblb_data)
			
			if ab_encrypt then
				// Encrypt data.
				if not CryptEncrypt(hKey, 0, true, 0, lblb_data, lul_datalength, lul_bufferLength) then
					ib_have_error = true
					is_last_error = "Error during CryptEncrypt."
				end if
			else
				// Decrypt data.
				if not CryptDecrypt(hKey, 0, true, 0, lblb_data, lul_datalength) then
					ib_have_error = true
					is_last_error = "Error during CryptDecrypt."
				end if
			end if
		else
			ib_have_error = true
			is_last_error = "Error during CryptDeriveKey!"
		end if
	else
		ib_have_error = true
		is_last_error = "Error during CryptCreateHash!"
	end if
else
	ib_have_error = true
	is_last_error = "Error during CryptCreateHash!"
end if

// Destroy session key.
if hKey <> 0 then CryptDestroyKey(hKey)

// Destroy hash object.
if hHash <> 0 Then CryptDestroyHash(hHash)

// Release provider handle.
if hProv <> 0 then CryptReleaseContext(hProv, 0)

if ib_have_error then 
	setnull(lblb_data)
else
	// This is what we return.
	lblb_data = blobmid(lblb_data, 1, lul_datalength)
end if

return lblb_data
end function

public function string uof_decrypt (string as_data, string as_password);/** 
 * stefanop
 * 29/09/2010
 *
 * Funzione che decrypta una stringa con l'algoritmo selezionato
 **/

string ls_encode

ls_encode = string(this.uof_decode_64(as_data), EncodingAnsi!)

return string(this.uof_encrypt_decrypt(ls_encode, as_password, false), EncodingAnsi!)
end function

public function string uof_encode_64 (blob ablb_data);String ls_encoded
ULong lul_len, lul_buflen
Boolean lb_rtn

lul_len = Len(ablb_data)
lul_buflen = lul_len * 2
ls_encoded = Space(lul_buflen)

lb_rtn = CryptBinaryToString(ablb_data, lul_len, CRYPT_STRING_BASE64, ls_encoded, lul_buflen)

If lb_rtn Then
	//ls_encoded = of_ReplaceAll(ls_encoded, "~r~n", "")
Else
	ls_encoded = ""
End If

Return ls_encoded
end function

public function blob uof_decode_64 (string as_data_encoded);/**
 * stefanop
 * 29/09/2010
 *
 * Decodifica un stringa base64
 * Per recuperare la stringa dal blob di ritorno usare string(blob_data, EncodingAnsi!)
 **/
ulong lul_len, lul_buflen, lul_skip, lul_pflags
boolean lb_rtn
blob lblob_data

lul_len = Len(as_data_encoded)
lul_buflen = lul_len
lblob_data = blob(Space(lul_len))

lb_rtn = CryptStringToBinary(as_data_encoded, lul_len, CRYPT_STRING_BASE64, lblob_data, lul_buflen, lul_skip, lul_pflags)

Return BlobMid(lblob_data, 1, lul_buflen)
end function

private function string uof_hash (string as_data, unsignedlong ai_alg_id);string ls_null, ls_hash
boolean bResult
ulong hProv,  hHash, dwBufferlen, dwBufferSize
byte dwData[]

int li_count, li_buffer[], li_hash_size

setnull(ls_null)

// Attempt to acquire a handle to the default key container.
if not CryptAcquireContext(ref hProv, ls_null, SERVICE_PROVIDER, PROV_RSA_FULL, 0) then	
	if not CryptAcquireContext(ref hProv, ls_null, SERVICE_PROVIDER, PROV_RSA_FULL, CRYPT_NEWKEYSET) then
		is_last_error = "Error during CryptAcquireContext for a new key container." + "~r~nA container with this name probably already exists."
		ib_have_error = true
	end if
end if

// Obtain handle to hash object.
if CryptCreateHash(hProv, ai_alg_id, 0, 0, ref hHash) then
	
	// Hash data.
	if CryptHashData(hHash, as_data, len(as_data), 0) then
		
		// Get size of hash value.
		dwBufferSize = 4
		if CryptGetHashParam(hHash, HP_HASHSIZE, ref li_hash_size, ref dwBufferSize, 0) then
			
			// Get hash value.
			dwBufferSize = li_hash_size
			dwData[dwBufferSize] = 0
			if not CryptGetHashParam(hHash, HP_HASHVAL, dwData, dwBufferSize, 0) then
				ib_have_error = true
				is_last_error = "Error during CryptGetHashParam!"
			end if
		else
			ib_have_error = true
			is_last_error = "Error during CryptGetHashParam!"
		end if
	else
		ib_have_error = true
		is_last_error = "Error during CryptHashData!"
	end if
else
	ib_have_error = true
	is_last_error = "Error during CryptCreateHash!"
end if

// Release hash object.
bResult = CryptDestroyHash(hHash)
 
// Release handle to container.
bResult = CryptReleaseContext(hProv, 0);

if not  ib_have_error then
	ls_hash = this.uof_to_hex(dwData)
end if

return ls_hash
end function

public function string uof_decrypt (blob ablb_data, string as_password);/** 
 * stefanop
 * 29/09/2010
 *
 * Funzione che decrypta una stringa con l'algoritmo selezionato
 **/

return uof_decrypt(string(ablb_data, EncodingAnsi!), as_password)
end function

private subroutine uof_get_lasterror (ref long al_error, ref string as_msgtext);Constant Long FORMAT_MESSAGE_FROM_SYSTEM = 4096
Constant Long LANG_NEUTRAL = 0
Long ll_rtn

al_error = GetLastError()

as_msgtext = Space(200)

ll_rtn = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, al_error, LANG_NEUTRAL, as_msgtext, 200, 0)

end subroutine

private function string uof_get_defaultprovider ();/**
 * stefanop
 * 28/09/2010
 *
 * Recupera il provider di default in uso per cifrare le password
 **/

string ls_provider, ls_msgtext
ulong lul_datalen = 256
long ll_error

ls_provider = Space(lul_datalen)

if not CryptGetDefaultProvider(PROV_RSA_FULL, 0, CRYPT_USER_DEFAULT, ls_provider, lul_datalen) Then
	messagebox("", "CryptGetDefaultProvide")
end if
	
return ls_provider
end function

public function boolean uof_error (ref string as_error_message);/**
 * stefanop
 * 30/09/2010
 *
 * Consente di sapere se è avvenuto un errore durante il processo di cifratura e ne indica il motivo
 **/
 
 if ib_have_error then
	as_error_message = is_last_error
else
	as_error_message = ""
end if

return ib_have_error
end function

public function string uof_sha_256 (string as_data);/**
 * stefanop
 * 28/09/2010
 *
 * Ritorna l'hash (SHA1) di una determinata stringa
 **/
 
return this.uof_hash(as_data, CALG_SHA_256)
end function

public function string uof_sha_512 (string as_data);/**
 * stefanop
 * 28/09/2010
 *
 * Ritorna l'hash (SHA1) di una determinata stringa
 **/
 
return this.uof_hash(as_data, CALG_SHA_512)
end function

public function string uof_encode_64 (string as_data, integer ai_encode_type);String ls_encoded
ULong lul_len, lul_buflen
Boolean lb_rtn

blob lblb_data

choose case ai_encode_type
	case 1
		lblb_data = blob(as_data, EncodingANSI!)
	case 2
		lblb_data = blob(as_data, EncodingUTF8!)
	case 3
		lblb_data = blob(as_data, EncodingUTF16LE!)
	case 4
		lblb_data = blob(as_data, EncodingUTF16BE!)
end choose		
		
//lblb_data = blob(as_data, EncodingAnsi!)
lul_len = Len(lblb_data)
lul_buflen = lul_len * 2
ls_encoded = Space(lul_buflen)

lb_rtn = CryptBinaryToString(lblb_data, lul_len, CRYPT_STRING_BASE64, ls_encoded, lul_buflen)

If lb_rtn Then
	//ls_encoded = of_ReplaceAll(ls_encoded, "~r~n", "")
Else
	ls_encoded = ""
End If

Return ls_encoded
end function

on uo_crypto.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_crypto.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;// Get default provider
SERVICE_PROVIDER = this.uof_get_defaultprovider()
end event

