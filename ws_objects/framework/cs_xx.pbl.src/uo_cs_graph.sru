﻿$PBExportHeader$uo_cs_graph.sru
forward
global type uo_cs_graph from datawindow
end type
end forward

global type uo_cs_graph from datawindow
integer width = 1559
integer height = 1044
string title = "none"
string dataobject = "d_cs_graph"
boolean border = false
boolean livescroll = true
event ue_sfondo ( )
end type
global uo_cs_graph uo_cs_graph

type variables
//datawindow o datastore dei dati
datawindow			idw_data
datastore				ids_data

//indica se deve usare la variabile datastore (default) o quella datawindow
boolean				ib_datastore = true

//se le colonne del datastore sono da referenziarsi per numero e non per nome
boolean				ib_col_by_number = false

//indica se si tratta di un grafico a torta e quindi valori percentuali
boolean				ib_pie = false

//espressione della colonna valore
string					is_exp_valore = "num_1"	//es. sum(num_1 for graph)
																	//count(num_1 for graph)
																	//ecc...

//colonna della dw o ds che alimenta il grafico per la categoria (serve per l'order by)
string					is_source_categoria_col

//colonna della dw del grafico adibita a categoria
string 					is_categoria_col

//colonne della dw del grafico adibita a serie
string					is_serie_col = ""

string					is_str[], is_num[], is_dtm[]

//titolo principale
string					is_titolo = ""

//etichetta asse categoria
string					is_label_categoria = "ascissa"

//etichetta asse valori
string					is_label_valori = "valori"

//scala dei valori di default --------------------------------------------------------------
integer					ii_scala = 1 //1=lineare  2=logaritmica in base 10
//scala dei valori
integer					ii_scala_run = -1
//-------------------------------------------------------------------------------------------------

//tipo grafico di default --------------------------------------------------------------------
//LEGENDA:
/*
 1 Area					 6 Barre a  Stack 3D Obj		11 Colonne a Stack 3D Obj			16 Linee 3D
 2 Barre					 7 Colonne						12 Linee //default						17 Torta 3D
 3 Barre 3D			 8 Colonne 3D					13 Torta
 4 Barre 3D Obj		 9  Colonne 3D Obj				14 Scatter
 5 Barre a Stack		10 Colonne a Stack			15 Area3D
 */
integer					ii_tipo_grafico = 12 // Linee (DEFAULT)
//tipo grafico (se -1 allora parte quello di default)
integer					ii_tipo_grafico_run = -1
//-------------------------------------------------------------------------------------------------

//colore di sfondo di default --------------------------------------------------------------
//RGB -> R + G * 256 + B * 65536
//per il bianco (DEFAULT)	: 16777215
//per il silver						: 12632256
long il_backcolor = 16777215 //bianco (DEFAULT) 

//colore di sfondo in runtime (se -1 allora parte quello di default)
long il_backcolor_run = -1
//-------------------------------------------------------------------------------------------------
end variables

forward prototypes
public function integer uof_retrieve ()
end prototypes

event ue_sfondo();//g_mb.messagebox("OMNIA", as_tag, Information!)
//g_mb.messagebox("OMNIA", "pippopippo", Information!)
//
//return
end event

public function integer uof_retrieve ();long				ll_rows, ll_row, ll_index, ll_i, ll_J, ll_col_number
string			ls_category_type, ls_value_type, ls_series_type, ls_orderby
string			ls_appo
double			ld_appo
datetime		ldt_appo
datastore	     lds_cs_graph_data



this.reset( )

ls_orderby = is_source_categoria_col + " ASC "
if ib_col_by_number then 	
	ls_orderby = "#" + ls_orderby
end if

if ib_datastore then
	//dati su datastore (default)
	
	//numero righe dei dati da rappresentare
	ll_rows = ids_data.rowcount( )
	
	//sort in base alla colonna categoria
	ids_data.setsort( ls_orderby )
	ids_data.sort( )
	
	//ciclo su ciascuna riga
	for ll_i = 1 to ll_rows
		
		//inserisco la riga da rappresentare
		ll_row = this.insertrow(0)
		
		if not ib_col_by_number then
			//trasferisco le righe negli eventuali campi stringa (max 5)
			for ll_J= 1 to upperbound( is_str ) //max 5
				ls_appo = ids_data.getitemstring( ll_i, is_str[ ll_J ] )
				this.setitem(ll_row, "str_" + string( ll_J ),  ls_appo)
			next
			//---------------------------------------------------------------------------------
			//trasferisco le righe negli eventuali campi number (max 5)
			for ll_J= 1 to upperbound( is_num ) //max 5
				ld_appo = ids_data.getitemnumber( ll_i, is_num[ ll_J ] )
				this.setitem(ll_row, "num_" + string( ll_J ),  ld_appo)
			next
			//---------------------------------------------------------------------------------
			//trasferisco le righe negli eventuali campi datetime (max 5)
			for ll_J= 1 to upperbound( is_dtm ) //max 5
				ldt_appo = ids_data.getitemdatetime( ll_i, is_dtm[ ll_J ] )
				this.setitem(ll_row, "dtm_" + string( ll_J ),  ldt_appo)
			next
			//---------------------------------------------------------------------------------
		else			
			//trasferisco le righe negli eventuali campi stringa (max 5)
			for ll_J= 1 to upperbound( is_str ) //max 5
				ll_col_number = long(is_str[ ll_J ])
				ls_appo = ids_data.getitemstring( ll_i, ll_col_number )
				this.setitem(ll_row, "str_" + string( ll_J ),  ls_appo)
			next
			//---------------------------------------------------------------------------------
			//trasferisco le righe negli eventuali campi number (max 5)
			for ll_J= 1 to upperbound( is_num ) //max 5
				ll_col_number = long(is_num[ ll_J ])
				ld_appo = ids_data.getitemnumber( ll_i, ll_col_number )
				this.setitem(ll_row, "num_" + string( ll_J ),  ld_appo)
			next
			//---------------------------------------------------------------------------------
			//trasferisco le righe negli eventuali campi datetime (max 5)
			for ll_J= 1 to upperbound( is_dtm ) //max 5
				ll_col_number = long(is_dtm[ ll_J ])
				ldt_appo = ids_data.getitemdatetime( ll_i, ll_col_number )
				this.setitem(ll_row, "dtm_" + string( ll_J ),  ldt_appo)
			next
			//---------------------------------------------------------------------------------
		end if
	next
else
	//dati su datawindow
	
	//numero righe dei dati da rappresentare
	ll_rows = idw_data.rowcount( )
	
	//sort in base alla colonna categoria
	idw_data.setsort( ls_orderby )
	idw_data.sort( )
	
	//ciclo su ciascuna riga
	for ll_i = 1 to ll_rows
		
		//inserisco la riga da rappresentare
		ll_row = this.insertrow(0)
		
		if not ib_col_by_number then
			//trasferisco le righe negli eventuali campi stringa (max 5)
			for ll_J= 1 to upperbound( is_str ) //max 5
				ls_appo = idw_data.getitemstring( ll_i, is_str[ ll_J ] )
				this.setitem(ll_row, "str_" + string( ll_J ),  ls_appo)
			next
			//---------------------------------------------------------------------------------
			//trasferisco le righe negli eventuali campi number (max 5)
			for ll_J= 1 to upperbound( is_num ) //max 5
				ld_appo = idw_data.getitemnumber( ll_i, is_num[ ll_J ] )
				this.setitem(ll_row, "num_" + string( ll_J ),  ld_appo)
			next
			//---------------------------------------------------------------------------------
			//trasferisco le righe negli eventuali campi datetime (max 5)
			for ll_J= 1 to upperbound( is_dtm ) //max 5
				ldt_appo = idw_data.getitemdatetime( ll_i, is_dtm[ ll_J ] )
				this.setitem(ll_row, "dtm_" + string( ll_J ),  ldt_appo)
			next
			//---------------------------------------------------------------------------------
		else
			//trasferisco le righe negli eventuali campi stringa (max 5)
			for ll_J= 1 to upperbound( is_str ) //max 5
				ll_col_number = long(is_str[ ll_J ])
				ls_appo = idw_data.getitemstring( ll_i, ll_col_number )
				this.setitem(ll_row, "str_" + string( ll_J ),  ls_appo)
			next
			//---------------------------------------------------------------------------------
			//trasferisco le righe negli eventuali campi number (max 5)
			for ll_J= 1 to upperbound( is_num ) //max 5
				ll_col_number = long(is_num[ ll_J ])
				ld_appo = idw_data.getitemnumber( ll_i, ll_col_number )
				this.setitem(ll_row, "num_" + string( ll_J ),  ld_appo)
			next
			//---------------------------------------------------------------------------------
			//trasferisco le righe negli eventuali campi datetime (max 5)
			for ll_J= 1 to upperbound( is_dtm ) //max 5
				ll_col_number = long(is_dtm[ ll_J ])
				ldt_appo = idw_data.getitemdatetime( ll_i, ll_col_number )
				this.setitem(ll_row, "dtm_" + string( ll_J ),  ldt_appo)
			next
			//---------------------------------------------------------------------------------
		end if
	next
end if

//proprietà grafico -------------------------------------------
this.object.gr_cs_graph.category = is_categoria_col
this.object.gr_cs_graph.values = is_exp_valore

if is_serie_col <> "" then
	this.Modify("gr_cs_graph.Series='" + is_serie_col + "'")
end if

this.object.gr_cs_graph.title = is_titolo
this.object.gr_cs_graph.category.label = is_label_categoria
this.object.gr_cs_graph.values.label = is_label_valori

if ii_scala_run = -1 then
	this.object.gr_cs_graph.Values.ScaleType = ii_scala
else
	this.object.gr_cs_graph.Values.ScaleType = ii_scala_run
end if

if ib_pie then
	if ii_tipo_grafico_run = -1 then
		
		if ii_tipo_grafico <> 13 and ii_tipo_grafico <> 17 then
			ii_tipo_grafico = 13
			this.object.gr_cs_graph.GraphType = 13
		else
			this.object.gr_cs_graph.GraphType = ii_tipo_grafico
		end if
		
	else
		
		if ii_tipo_grafico_run <> 13 and ii_tipo_grafico_run <> 17 then
			ii_tipo_grafico_run = 13
			this.object.gr_cs_graph.GraphType = 13
		else
			this.object.gr_cs_graph.GraphType = ii_tipo_grafico_run
		end if
		
	end if
else
	if ii_tipo_grafico_run = -1 then
		this.object.gr_cs_graph.GraphType = ii_tipo_grafico
	else	
		this.object.gr_cs_graph.GraphType = ii_tipo_grafico_run
	end if
end if

if il_backcolor_run = -1 then
	this.object.gr_cs_graph.backcolor = il_backcolor
	this.object.DataWindow.Color = il_backcolor
else
	this.object.gr_cs_graph.backcolor = il_backcolor_run
	this.object.DataWindow.Color = il_backcolor_run
end if
//----------------------------------------------------------------

return 1
end function

event resize;this.object.gr_cs_graph.width = this.width - 100
this.object.gr_cs_graph.height = this.height - 400
end event

on uo_cs_graph.create
end on

on uo_cs_graph.destroy
end on

event buttonclicked;//s_cs_graph_settings ls_cs_graph_settings
//long ll_value, ll_backcolor
//
//choose case dwo.name
//	case "b_impostazioni"
//		ll_value = long( this.object.gr_cs_graph.Values.ScaleType )
//		ls_cs_graph_settings.i_scala = ll_value
//		
//		ll_value = long( this.object.gr_cs_graph.GraphType )
//		ls_cs_graph_settings.i_tipo_graph = ll_value
//		
//		ll_backcolor = long(this.object.datawindow.color)
//		ls_cs_graph_settings.i_backcolor = ll_backcolor
//		
//		openwithparm ( w_cs_graph_impostazioni, ls_cs_graph_settings )
//		ls_cs_graph_settings = message.powerobjectparm
//		
//		if ls_cs_graph_settings.b_conferma then
//			ii_scala_run = ls_cs_graph_settings.i_scala
//			ii_tipo_grafico_run = ls_cs_graph_settings.i_tipo_graph
//			il_backcolor_run = ls_cs_graph_settings.i_backcolor
//			
//			this.object.gr_cs_graph.Values.ScaleType = ls_cs_graph_settings.i_scala
//			this.object.gr_cs_graph.GraphType = ls_cs_graph_settings.i_tipo_graph
//			this.object.datawindow.color = ls_cs_graph_settings.i_backcolor
//			this.object.gr_cs_graph.backcolor = ls_cs_graph_settings.i_backcolor
//		end if
//end choose
end event

event constructor;this.object.gr_cs_graph.width = this.width - 100
this.object.gr_cs_graph.height = this.height - 400

if il_backcolor_run = -1 then
	//quello di default
	this.object.gr_cs_graph.backcolor = il_backcolor
	this.object.DataWindow.Color = il_backcolor
else
	this.object.gr_cs_graph.backcolor = il_backcolor_run
	this.object.DataWindow.Color = il_backcolor_run
end if
	


end event

event rbuttondown;s_cs_graph_settings ls_cs_graph_settings
long ll_value, ll_backcolor

ll_value = long( this.object.gr_cs_graph.Values.ScaleType )
ls_cs_graph_settings.i_scala = ll_value

ll_value = long( this.object.gr_cs_graph.GraphType )
ls_cs_graph_settings.i_tipo_graph = ll_value

ll_backcolor = long(this.object.datawindow.color)
ls_cs_graph_settings.i_backcolor = ll_backcolor

ls_cs_graph_settings.b_pie = ib_pie

openwithparm ( w_cs_graph_impostazioni, ls_cs_graph_settings )
ls_cs_graph_settings = message.powerobjectparm

if ls_cs_graph_settings.b_conferma then
	ii_scala_run = ls_cs_graph_settings.i_scala
	ii_tipo_grafico_run = ls_cs_graph_settings.i_tipo_graph
	il_backcolor_run = ls_cs_graph_settings.i_backcolor
	
	this.object.gr_cs_graph.Values.ScaleType = ls_cs_graph_settings.i_scala
	this.object.gr_cs_graph.GraphType = ls_cs_graph_settings.i_tipo_graph
	this.object.datawindow.color = ls_cs_graph_settings.i_backcolor
	this.object.gr_cs_graph.backcolor = ls_cs_graph_settings.i_backcolor
end if





//string 										ls_img[], ls_path_origine
//m_cs_graph_impostazioni 		lm_popup
//
//lm_popup = CREATE m_cs_graph_impostazioni
//
//
//ls_path_origine = s_cs_xx.volume + s_cs_xx.risorse + "img_grafici\"
//
////Tipo Grafico
//ls_img[1] = ls_path_origine + "Area.bmp"
//ls_img[2] = ls_path_origine +  "Area3D.bmp"
////-------------------------------------------------------------
//ls_img[3] = ls_path_origine +  "Bar.bmp"
//ls_img[4] = ls_path_origine +  "Bar3D.bmp"
//ls_img[5] = ls_path_origine +  "Bar3DObj.bmp"
//ls_img[6] = ls_path_origine +  "BarStacked.bmp"
//ls_img[7] = ls_path_origine +  "BarStacked3DObj.bmp"
////-------------------------------------------------------------
//ls_img[8] = ls_path_origine +  "Col.bmp"
//ls_img[9] = ls_path_origine +  "Col3D.bmp"
//ls_img[10] = ls_path_origine +  "Col3DObj.bmp"
//ls_img[11] = ls_path_origine +  "ColStacked.bmp"
//ls_img[12] = ls_path_origine +  "ColStacked3DObj.bmp"
////-------------------------------------------------------------
//ls_img[13] = ls_path_origine +  "Line.bmp" //default
//ls_img[14] = ls_path_origine +  "Line3D.bmp"
////-------------------------------------------------------------
//ls_img[15] = ls_path_origine +  "Pie.bmp"
//ls_img[16] = ls_path_origine +  "Pie3D.bmp"
////-------------------------------------------------------------
//ls_img[17] = ls_path_origine +  "Scatter.bmp"
//
////Scala Grafico
//ls_img[18] = ls_path_origine +  "lineare.bmp"
//ls_img[19] = ls_path_origine +  "log10.bmp"
//
////Sfondo Grafico
//ls_img[20] = ls_path_origine +  "bianco.bmp"
//ls_img[21] = ls_path_origine +  "argento.bmp"
//
//
//
////Tipo Grafico -------------------------------------------------
//lm_popup.m_tipografico.m_area.menuimage = ls_img[1]
//lm_popup.m_tipografico.m_area3d.menuimage = ls_img[2]
////-------------------------------------------------------------
//lm_popup.m_tipografico.m_barre.menuimage = ls_img[3]
//lm_popup.m_tipografico.m_barre3d.menuimage = ls_img[4]
//lm_popup.m_tipografico.m_barre3dobj.menuimage = ls_img[5]
//lm_popup.m_tipografico.m_barreastack.menuimage = ls_img[6]
//lm_popup.m_tipografico.m_barreastack3dobj.menuimage = ls_img[7]
////-------------------------------------------------------------
//lm_popup.m_tipografico.m_colonne.menuimage = ls_img[8]
//lm_popup.m_tipografico.m_colonne3d.menuimage = ls_img[9]
//lm_popup.m_tipografico.m_colonne3dobj.menuimage = ls_img[10]
//lm_popup.m_tipografico.m_colonneastack.menuimage = ls_img[11]
//lm_popup.m_tipografico.m_colonneastack3dobj.menuimage = ls_img[12]
////-------------------------------------------------------------
//lm_popup.m_tipografico.m_linee.menuimage = ls_img[13]
//lm_popup.m_tipografico.m_linee3d.menuimage = ls_img[14]
////-------------------------------------------------------------
//lm_popup.m_tipografico.m_torta.menuimage = ls_img[15]
//lm_popup.m_tipografico.m_torta3d.menuimage = ls_img[16]
////-------------------------------------------------------------
//lm_popup.m_tipografico.m_scatter.menuimage = ls_img[17]
//
////Scala Grafico -------------------------------------------------
//lm_popup.m_scala.m_lineare.menuimage = ls_img[18]
//lm_popup.m_scala.m_logaritmicabase10.menuimage = ls_img[19]
//
////Sfondo Grafico ----------------------------------------------
//lm_popup.m_sfondo.m_bianco.menuimage = ls_img[20]
//lm_popup.m_sfondo.m_argento.menuimage = ls_img[21]
//
//
//lm_popup.popmenu(PCCA.MDI_Frame.PointerX(), PCCA.MDI_Frame.PointerY())
//
end event

