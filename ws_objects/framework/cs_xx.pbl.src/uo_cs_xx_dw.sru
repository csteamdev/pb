﻿$PBExportHeader$uo_cs_xx_dw.sru
$PBExportComments$Modificata con SELECTTEXT sulle editmask
forward
global type uo_cs_xx_dw from uo_dw_main
end type
end forward

global type uo_cs_xx_dw from uo_dw_main
event ue_trova pbm_custom01
event ue_anteprima pbm_custom02
event ue_imp_pagina pbm_custom03
event ue_key pbm_dwnkey
event ue_selecttext ( )
event ue_aggiorna_text ( )
event ue_proteggi_campi_insert ( )
event ue_imposta_campi_note ( )
event ue_anteprima_pdf pbm_custom02
event type long ue_check_prodotto_bloccato ( datetime adt_data,  long al_row,  dwobject adw_dwo,  string as_data )
end type
global uo_cs_xx_dw uo_cs_xx_dw

type variables
// see http://infocenter.sybase.com/archive/index.jsp?topic=/com.sybase.help.pb_10.5.dwref/html/dwref/CAIBEIJA.htm
constant	string STYLE_GRID = "1"

public:
	boolean ib_proteggi_chiavi = true
	boolean ib_chiavi_protette = false
	boolean ib_dettaglio = false
	boolean ib_stato_nuovo, ib_stato_modifica, ib_stato_cancellazione, ib_stato_visualizzazione
	string  is_sql_origine = ""
	long    il_limit_campo_note = 0
	
	// stefanop: 09/11/2011 indico se la DW è di tipo detail (dove NON serve fare il sort delle colonne al doppio click)
	boolean ib_dw_detail = false
	
	// stefanop: 18/06/2014: il messaggio della mail in caso di "Invia Pdf" dal menu
	string is_mail_message 
	string	is_mail_destinatario

	string is_cod_parametro_blocco_prodotto=""
//----
//long il_riga_noscroll
//----

/*
ITEMFOCUSCHANGED
ROWFOCUSCHANGED
SCROLLVERTICAL
UE_KEY
RBUTTONDOWN
*/
end variables

forward prototypes
public subroutine uof_aggiorna_text ()
public subroutine set_dw_options (transaction dbca, uo_dw_main parent_dw, unsignedlong control_world, unsignedlong visual_world)
public subroutine uf_proteggi_chiavi ()
public subroutine uof_proteggi_campi_insert ()
public function string uof_inserisci_valore_insert (string as_sql, string as_colonna_db, string as_valore)
public subroutine uof_imposta_campi_note ()
public function string uof_controlla_obbligatori (string nome_datawindow, long row_number)
public subroutine uof_imposta_limite_colonne_note (long al_num_caratteri)
end prototypes

on ue_trova;call uo_dw_main::ue_trova;if not i_inuse then
	pcca.error = c_fatal
	return
end if

if i_dwstate = c_dwstatequery then
	change_dw_focus(this)
	pcca.error = c_fatal
	return
end if

setpointer(hourglass!)

pcca.error = c_success

openwithparm(w_trova, this)

end on

event ue_anteprima;setpointer(hourglass!)

s_cs_xx.parametri.parametro_dw_1 = this
s_cs_xx.parametri.parametro_s_1 = pcca.window_current.title

open(w_anteprima)
end event

on ue_imp_pagina;call uo_dw_main::ue_imp_pagina;setpointer(hourglass!)

s_cs_xx.parametri.parametro_dw_1 = this

open(w_imp_pagina)
end on

event ue_selecttext;this.selecttext(1, len(this.gettext()))
end event

event ue_aggiorna_text();uof_aggiorna_text()
end event

event ue_proteggi_campi_insert();uof_proteggi_campi_insert()
end event

event ue_imposta_campi_note();uof_imposta_campi_note()
end event

event ue_anteprima_pdf;string ls_messaggio, ls_path, ls_to[], ls_subject, ls_message, ls_error
long ll_return, ll_elemento
boolean lb_invio_con_outlook
uo_outlook luo_outlook

ll_elemento = 0

setpointer(hourglass!)

s_cs_xx.parametri.parametro_dw_1 = this
s_cs_xx.parametri.parametro_s_1 = pcca.window_current.title

uo_archivia_pdf luo_pdf
luo_pdf = create uo_archivia_pdf

ls_path = luo_pdf.uof_crea_pdf_path(this)

if ls_path = "errore" then
	g_mb.messagebox ("Errore","Errore nella creazione del file pdf.")
	return -1
end if

try
	luo_outlook = create uo_outlook
	
	// verifico se era stato impostato un destinatario
	if not isnull(is_mail_destinatario) and len(is_mail_destinatario) > 0 then
		ls_to[1] = is_mail_destinatario
	end if
	
	// Il nome dell'oggetto lo recupero dal nome della DW
	ls_subject = Object.DataWindow.Print.DocumentName
	ls_message = is_mail_message
	
	// Messaggio forzato o preso dall'oggetto
	if isnull(ls_message) or ls_message = "" then
		ls_message = ls_subject
	end if
	
	guo_functions.uof_get_parametro_azienda("IPO", lb_invio_con_outlook)
	
	if lb_invio_con_outlook then
		// Invio con outlook
		if luo_outlook.uof_apri_outlook(ls_to, ls_subject, ls_message, {ls_path}, ls_error) < 0 then
			g_mb.error(ls_error)
		end if
	else
		// Invio con il send mail
		luo_outlook.ib_force_open = true
		
		if luo_outlook.uof_invio_sendmail(ls_to[], ls_subject, ls_message, {ls_path}, ls_error) then
			ll_return = 1
			g_mb.success("Email inviata con successo")
		else
			ll_return = -1
			g_mb.warning(ls_error)
		end if
	end if
	
catch(RuntimeError e)
	g_mb.error("Errore durante l'invio della mail.")
finally
	destroy luo_outlook
end try

filedelete(ls_path)


// stefanop 18/06/2014: Commento codice vecchio a favore di quello nuovo per l'invio della mail
//oleobject iole_outlook, iole_item, iole_attach
//invio pdf
//iole_outlook = create oleobject
//
//ll_return = iole_outlook.connecttonewobject("outlook.application")
//
//if ll_return <> 0 THEN
//	ls_messaggio = "Impossibile connettersi ad Outlook. Codice errore: " + string(ll_return)
//	g_mb.messagebox ("Errore",ls_messaggio)
//	
//	destroy iole_outlook
//	return -1
//end if
//
//// Viene creato un nuovo elemento del tipo specificato
//
//iole_item = iole_outlook.createitem(ll_elemento)
////visualizza la mail da inviare
//iole_item.display
//
//iole_attach = iole_item.attachments
//	
//// Il file specificato viene allegato solo se esiste al percorso indicato
//		
//if fileexists(ls_path) then
//	iole_attach.add(ls_path)
//else
//	ls_messaggio = "allegato inesistente" + string(ll_return)
//	g_mb.messagebox ("Errore",ls_messaggio)
//	destroy iole_attach
//	destroy iole_outlook
//	return -1
//end if
//		
//destroy iole_item		
////destroy iole_attach
//destroy iole_attach
//filedelete(ls_path)
end event

event type long ue_check_prodotto_bloccato(datetime adt_data, long al_row, dwobject adw_dwo, string as_data);string ls_messaggio, ls_null
long li_ret
uo_fido_cliente luo_fido_cliente

setnull(ls_null)

luo_fido_cliente = create uo_fido_cliente
li_ret = luo_fido_cliente.uof_get_blocco_prodotto( as_data, adt_data, is_cod_parametro_blocco_prodotto, ls_messaggio)
if li_ret=2 then
	//blocco
	g_mb.error(ls_messaggio)
	this.setitem(al_row, adw_dwo.name, ls_null)
	this.SetColumn ( string(adw_dwo.name) )
	destroy luo_fido_cliente
	return 2
elseif li_ret=1 then
	//solo warning
	g_mb.warning(ls_messaggio)
end if
destroy luo_fido_cliente
return 0
end event

public subroutine uof_aggiorna_text ();string ls_modify, ls_nome_dw, ls_nome_oggetto, ls_testo, ls_tooltip, ls_sql, ls_ret

ls_nome_dw = dataobject

if s_cs_xx.cod_lingua_applicazione = "" then setnull(s_cs_xx.cod_lingua_applicazione)
if s_cs_xx.cod_lingua_window 		 = "" then setnull(s_cs_xx.cod_lingua_window)

declare cu_dtext dynamic cursor for SQLSA;
ls_sql = " select nome_oggetto, trim(testo), tooltip from tab_dw_text_repository " + &
         " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and" + &
         " nome_dw = '" + ls_nome_dw + "' and " 
			
if isnull(s_cs_xx.cod_lingua_window) then
	if isnull(s_cs_xx.cod_lingua_applicazione) then
		ls_sql += " cod_lingua is null "
	else
		ls_sql += " cod_lingua = '" + s_cs_xx.cod_lingua_applicazione + "' "
	end if
else
	ls_sql += " cod_lingua = '" + s_cs_xx.cod_lingua_window + "' "
end if

PREPARE SQLSA FROM :ls_sql ;

open dynamic cu_dtext;

do while true
	fetch cu_dtext into :ls_nome_oggetto, :ls_testo, :ls_tooltip;
	if sqlca.sqlcode <> 0 then exit
	
	// stefanop 27/07/2011: imposto il valore Zoom
	if ls_nome_oggetto = "Zoom" then
		ls_modify += "DataWindow.Zoom=" + ls_testo + "'~t"
		continue
	end if
	// ----
	
	if not isnull(ls_testo) and len(ls_testo) > 0 then  
		ls_testo = f_parse_modify( ls_testo )
		ls_modify += ls_nome_oggetto + ".text='"+ ls_testo + "'~t"
	end if
	
	if not isnull(ls_tooltip) and len(ls_tooltip) > 0 then  
		
		ls_tooltip = f_parse_modify( ls_tooltip )
		
		ls_modify += ls_nome_oggetto + ".tooltip.enabled='1'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.tip='"+ ls_tooltip + "'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.title='Help'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.icon='1'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.isbubble='" + string(s_themes.theme[1].tooltip_isbubble) + "'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.textcolor='" + string(s_themes.theme[1].tooltip_textcolor) + "'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.backcolor='" + string(s_themes.theme[1].tooltip_backcolor) + "'~t"
		
	end if
	
loop

ls_ret = modify(ls_modify)

close cu_dtext;

return
end subroutine

public subroutine set_dw_options (transaction dbca, uo_dw_main parent_dw, unsignedlong control_world, unsignedlong visual_world);integer li_i
string ls_modify, ls_colonna


for li_i = 1 to integer(describe("datawindow.column.count"))
	
   ls_colonna = describe("#" + string(li_i) + ".name")
	
   if describe(ls_colonna + ".visible") = "1" and (describe(ls_colonna + ".edit.style") = "edit" or describe(ls_colonna + ".edit.style") = "editmask") then
      ls_modify = ls_modify + ls_colonna + ".pointer='beam!'~t"
   end if
	
next

if not isnull(ls_modify) and ls_modify <> "" then
   modify(ls_modify)
end if

if not s_cs_xx.admin then
	control_world = control_world + s_cs_xx.parametri.parametro_ul_1
end if

super::set_dw_options(dbca, parent_dw, control_world, visual_world)

postevent("ue_aggiorna_text")

// CAMPI NOTE: aggiunto da Enrico il 27/6/2005
// lo scopo è quello di bloccare l'input nei campi note in base al tipo di database.
postevent("ue_imposta_campi_note")

end subroutine

public subroutine uf_proteggi_chiavi ();long ll_i, ll_j
string ls_num_col, ls_modify

get_col_info()

for ll_i = 1 to i_xnumkeycolumns
   ll_j = i_xkeycolumns[ll_i]
   if i_colvisible[ll_j] then
      ls_num_col = "#" + string(ll_j)
      ls_modify = ls_modify + &
                  ls_num_col + &
                  ".background.color='" + &
                  i_modifymodecolor + &
					   "~tif(isrownew()," + &
                  i_modifymodecolor + &
                  "," + &
					   i_viewmodecolor + &
                  ")'~t"
      if not ib_chiavi_protette then
			ls_modify = ls_modify + &
                     ls_num_col + &
                     ".protect='0~tif(isrownew(),0,1)'~t"
      end if
   end if
next

if ls_modify <> "" then this.modify(ls_modify)

ib_chiavi_protette = true

end subroutine

public subroutine uof_proteggi_campi_insert ();string ls_colonna_dw, ls_cod_utente, ls_valore_insert, ls_nome_dw, ls_nome_parent_dw, ls_modify, &
		 ls_flag_tipo_valore, ls_flag_visualizza_subito, ls_string
decimal ld_decimal
datetime ldt_datetime

if s_cs_xx.admin then return 

ls_nome_dw = dataobject

if isvalid(i_parentdw) then
	ls_nome_parent_dw = i_parentdw.dataobject
else
	ls_nome_parent_dw = ls_nome_dw
end if

DECLARE cu_dw_filtri CURSOR FOR  
SELECT colonna_dw,   
		cod_utente,   
		valore_insert,
		flag_tipo_valore,
		flag_visualizza_subito
 FROM tab_dw_filtri  
WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
		((nome_dw = :ls_nome_dw ) or (nome_dw = :ls_nome_parent_dw)) AND  
		cod_utente = :s_cs_xx.cod_utente and
		flag_ambiente = 'C';

open cu_dw_filtri;

ls_modify = ""

do while true
	fetch cu_dw_filtri into :ls_colonna_dw, :ls_cod_utente, :ls_valore_insert, :ls_flag_tipo_valore, :ls_flag_visualizza_subito;
	if sqlca.sqlcode <> 0 then exit
	
	ls_modify = ls_modify + ls_colonna_dw + ".background.color='" + i_viewmodecolor + "'~t"
	ls_modify = ls_modify + ls_colonna_dw + ".protect=1" + "'~t"
	
	if ls_flag_visualizza_subito = "S" and not isnull(ls_flag_visualizza_subito) then
		choose case ls_flag_tipo_valore
			case "S"  // stringa
				try
					setitem(getrow(),ls_colonna_dw, ls_valore_insert)
				catch (throwable err1)
					// non faccio nulla
				end try
				
			case "N" 	// numero
				try
					ld_decimal = dec(ls_valore_insert)
					setitem(getrow(),ls_colonna_dw, ld_decimal)
				catch (throwable err2)
					// non faccio nulla
				end try
					
			case "D"		// datetime
				try
					ldt_datetime = datetime(ls_valore_insert)
					setitem(getrow(),ls_colonna_dw, ldt_datetime)
				catch (throwable err3)
					// non faccio nulla
				end try
		end choose
	end if
	
loop

close cu_dw_filtri;

if len(ls_modify) > 1 then this.modify(ls_modify)
end subroutine

public function string uof_inserisci_valore_insert (string as_sql, string as_colonna_db, string as_valore);string ls_str, ls_insert_colonne, ls_insert_values, ls_colonne[], ls_values[], ls_sql_inizio, ls_sql
long ll_pos, ll_pos_inizio_col, ll_pos_fine_col, ll_pos_inizio_val, ll_pos_fine_val, ll_i, ll_cont

ll_pos = pos(as_sql, "VALUES")
if ll_pos < 1 then return ""

ls_insert_colonne = left(as_sql, ll_pos - 1)
ls_insert_values = mid(as_sql, ll_pos)

ll_pos = pos(ls_insert_colonne, "(")
ls_sql_inizio = left(ls_insert_colonne, ll_pos)
ll_pos_inizio_col = ll_pos + 1

ll_pos = pos(ls_insert_values, "(")
ll_pos_inizio_val = ll_pos + 1

ll_i = 0
do while true
	ll_pos_fine_col = pos(ls_insert_colonne, ",",ll_pos_inizio_col)
	if ll_pos_fine_col < 1 then 
		// controllo ultima colonna
		ll_pos_fine_col = pos(ls_insert_colonne, ")",ll_pos_inizio_col)
		if ll_pos_fine_col < 1 then 
			exit
		end if
	end if
	
	ll_i ++
	
	ls_str = mid(ls_insert_colonne, ll_pos_inizio_col, ll_pos_fine_col - ll_pos_inizio_col)
	ls_colonne[ll_i] = ls_str
	ll_pos_inizio_col = ll_pos_fine_col + 1
	
	ll_pos_fine_val = pos(ls_insert_values, ",",ll_pos_inizio_val)
	
	if ll_pos_fine_val < 1 then 
		// controllo ultima colonna
		ll_pos_fine_val = pos(ls_insert_values, ")",ll_pos_inizio_val)
		if ll_pos_fine_val < 1 then 
			exit
		end if
	end if

	ls_str = mid(ls_insert_values, ll_pos_inizio_val, ll_pos_fine_val - ll_pos_inizio_val)
	ls_values[ll_i] = ls_str
	ll_pos_inizio_val = ll_pos_fine_val + 1
loop

// ripulisco il nome della colonna
if pos(as_colonna_db,".") > 0 then
	as_colonna_db = mid(as_colonna_db, pos(as_colonna_db,".") + 1)
end if

do while pos(as_colonna_db,char(34)) > 0
	as_colonna_db = replace(as_colonna_db,pos(as_colonna_db,char(34)),1,"")
loop

// aggiungo il valore desiderato
ll_cont = upperbound(ls_colonne)

for ll_i = 1 to ll_cont
	if pos(ls_colonne[ll_i], as_colonna_db) > 0 then
		ls_values[ll_i] = as_valore
	elseif ll_i = ll_cont then
		// vuol dire che sono arrivato alla fine e 
		// non lo ho trovato e quindi lo aggiungo d'ufficio
		ls_colonne[ll_i+1] = as_colonna_db
		ls_values[ll_i +1] = as_valore
	end if
next

// ricompongo la sintassi INSERT
ls_sql = ls_sql_inizio
ll_cont = upperbound(ls_colonne)
for ll_i = 1 to ll_cont
	if ll_i > 1 then ls_sql += ", "
	ls_sql += ls_colonne[ll_i]
next

ls_sql += " ) VALUES ("

ll_cont = upperbound(ls_values)
for ll_i = 1 to ll_cont
	if ll_i > 1 then ls_sql += ", "
	ls_sql += ls_values[ll_i]
next

ls_sql += " ) "

return ls_sql
end function

public subroutine uof_imposta_campi_note ();/*	ENRICO 27/6/2005

QUESTA FUNZIONE PROVVEDE A BLOCCARE L'IMPUTAZIONE DI UN NUMERO DI CARATTERI NEI CAMPI NOTE
MAGGIORE DI QUELLO CHE ACCETTA IL TIPO DI DATO DEL DATABASE.
SE IN QUALCHE GESTIONE PERSONALIZZATA I CAMPI NOTE POSSO ACCETTARE PIU' CARATTERI
E' POSSIBILE IMPOSTARE LA VARIABILE DI ISTANZA IL_LIMITE_CAMPO_NOTE PRIMA DI CHIAMARE
LA SET_DW_OPTION.

*/

integer li_i, li_char
string ls_modify, ls_colonna, ls_str

for li_i = 1 to integer(describe("datawindow.column.count"))
	
   ls_colonna = describe("#" + string(li_i) + ".name")
	
   if describe(ls_colonna + ".visible") = "1" and (describe(ls_colonna + ".edit.style") = "edit" or describe(ls_colonna + ".edit.style") = "editmask") then
		ls_str = describe(ls_colonna + ".coltype")
		
      if lower( left(ls_str, 4) ) = "char" then
			li_char = integer(mid(ls_str, 6, len(ls_str) - 6))
			
			// se il tipo di dato prevede più di 100 caratteri.
			if li_char > 100 then
				
				if il_limit_campo_note = 0 then
					
					choose case f_db()
						case "SYBASE_ASA"
							ls_modify = ls_modify + ls_colonna + ".Edit.limit='32000'~t"
						case "SYBASE_ASE"
							ls_modify = ls_modify + ls_colonna + ".Edit.limit='255'~t"
						case "ORACLE"
							ls_modify = ls_modify + ls_colonna + ".Edit.limit='1999'~t"
						case "MSSQL"
							//ls_modify = ls_modify + ls_colonna + ".Edit.limit='255'~t"
							if li_char > 8000 then li_char = 8000
							ls_modify = ls_modify + ls_colonna + ".Edit.limit='" + string(li_char) + "'~t"
					end choose
				else
					ls_modify = ls_modify + ls_colonna + ".Edit.limit='"+string(il_limit_campo_note)+"'~t"
				end if
				
			end if
		end if
   end if
next

if not isnull(ls_modify) and ls_modify <> "" then
   modify(ls_modify)
end if
end subroutine

public function string uof_controlla_obbligatori (string nome_datawindow, long row_number);string ls_nome_oggetto, ls_flag_obbligatorio, ls_coltype, ls_string, ls_return
date ld_date
datetime ld_datetime
long ll_long
decimal ld_decimal

declare cu_colobb cursor for
select nome_oggetto, 
       flag_obbligatorio
from   tab_dw_text_repository
where  cod_lingua is null and
       nome_dw = :nome_datawindow;

ls_return = ""

open   cu_colobb;

do while true
	fetch  cu_colobb into :ls_nome_oggetto, :ls_flag_obbligatorio;
	if sqlca.sqlcode <> 0 then exit
	
	if ls_flag_obbligatorio = "N" or isnull(ls_flag_obbligatorio) then continue
	
	ls_coltype = this.Describe(ls_nome_oggetto + ".ColType")
	
	choose case lower(left(ls_coltype,5))
			
		case "char("
			ls_string = getitemstring(row_number, ls_nome_oggetto)
			if isnull(ls_string) or len(ls_string) < 1 then
				ls_return += '"' + ls_nome_oggetto + '"' + " Informazione Obbligatoria~r~n"
			end if
			
		case "date"
			ld_date = getitemdate(row_number, ls_nome_oggetto)
			if isnull(ld_date) or ld_date <= date("01/01/1900") then
				ls_return += '"' + ls_nome_oggetto + '"' + " Informazione Obbligatoria~r~n"
			end if
			
		case "datet", "times"
			ld_datetime = getitemdatetime(row_number, ls_nome_oggetto)
			if isnull(ld_datetime) or ld_datetime <= datetime(date("01/01/1900"),00:00:00) then
				ls_return += '"' + ls_nome_oggetto + '"' + " Informazione Obbligatoria~r~n"
			end if
			
		case "int", "integ", "long"
			ll_long =  getitemnumber(row_number, ls_nome_oggetto)
			if isnull(ll_long) or ll_long = 0 then
				ls_return += '"' + ls_nome_oggetto + '"' + " Informazione Obbligatoria~r~n"
			end if
			
		case "decim","number", "real"
			ld_decimal =  getitemnumber(row_number, ls_nome_oggetto)
			if isnull(ld_decimal) or ld_decimal = 0 then
				ls_return += '"' + ls_nome_oggetto + '"' + " Informazione Obbligatoria~r~n"
			end if

	end choose

loop

close cu_colobb;

return ls_return 
end function

public subroutine uof_imposta_limite_colonne_note (long al_num_caratteri);if al_num_caratteri > 0 and not isnull(al_num_caratteri) then
	il_limit_campo_note = al_num_caratteri
end if
end subroutine

event clicked;call super::clicked;if i_extendmode then
   string ls_nome_window, ls_nome_datawindow, ls_nome_colonna
   integer li_punto, li_num_colonna


   li_punto = pos(i_classname, ".")
   ls_nome_window = mid(i_classname, 1, (li_punto - 1))
   ls_nome_datawindow = dataobject
   li_num_colonna = getclickedcolumn()
   if li_num_colonna > 0 then
      ls_nome_colonna = describe("#" + string(li_num_colonna) + ".dbname")
      ls_nome_colonna = mid(ls_nome_colonna, pos(ls_nome_colonna, ".") + 1)
   else
      ls_nome_colonna = " "
   end if
/*
   if not f_help(ls_nome_window, ls_nome_datawindow, ls_nome_colonna) then
      li_punto = pos(i_classname, ".")
      ls_nome_window = mid(i_classname, 1, (li_punto - 1))
      ls_nome_datawindow = dataobject
      ls_nome_colonna = " "
      if not f_help(ls_nome_window, ls_nome_datawindow, ls_nome_colonna) then
         li_punto = pos(i_classname, ".")
         ls_nome_window = mid(i_classname, 1, (li_punto - 1))
         ls_nome_datawindow = " "
         ls_nome_colonna = " "
         f_help(ls_nome_window, ls_nome_datawindow, ls_nome_colonna)
      end if
   end if
*/
end if
end event

event doubleclicked;call super::doubleclicked;integer li_i, li_num_colonna, li_num_colonne
long ll_num_riga
string ls_nome_colonna, ls_nome_datawindow, ls_nome_window, ls_nome_tabella, ls_nome_codice, &
       ls_nome_descrizione, ls_selezione, ls_str, ls_newsort, ls_zk, ls_risultato
window lw_window, lw_window_1
w_cs_xx_principale lw_cs_xx_principale

// stefanop 27/07/2011: al doppio click, se dw_report, apro l'anteprima di stampa
if ib_dw_report then
	if isvalid(pcca.window_current) then
		triggerevent(pcca.window_current, "ue_anteprima")
	end if
end if
// ----

// stefanop 05/12/2012: aumento il controllo e faccio il sort solo
// se la DW è di tipo grid
/*
 * F. Baschirotto - 16/01/2013.
 * Tolto il controllo, in quanto il Processing restituisce un valore per n tipologie e non permette di filtrare come si vorrebbe lo stile della DataWindow.
 * Provato con un test, sembra non dar problemi neanche nelle DataWindow di tipo FreeForm.
*/
//if string(Object.DataWindow.Processing) <> STYLE_GRID then
//	return 0
//end if
// ----

// stefanop 09/11/2011: se dw di dettaglio no devo ordinare una mazza
if ib_dw_detail then return
// ----


if dwo.type = "text" and row<1 then
   ls_str = dwo.name
	ls_str = mid(ls_str, 1, len(ls_str) - 2 )
	if s_cs_xx.tipo_ordinamento then
		setsort(ls_str + " A")
	else
		setsort(ls_str + " D")
	end if
	
	sort()
	s_cs_xx.tipo_ordinamento = not s_cs_xx.tipo_ordinamento
	
	return
end if

// stefanop 23/02/2012: aggiunto supporto ai TAB
// Il controllo andrebbe fatto sulla base del tipo oggetto, ma il metodo TypeOf di PB,
// non funziona (STRANO!!) e quindi son costretto a controllare la nomeclatura della classe
// per capire se è una tab. La nomeclatura dei tab inzia sempre per det_X (confenzione)
if left(classname(getparent()), 4) = "det_" then
	lw_window_1 = parent.getParent().getParent()
else
	lw_window_1 = parent
end if

// lw_window_1 = parent

// ----

if lw_window_1.windowtype = Response! then
	return
end if

ls_risultato = upper(this.describe(dwo.name + ".EditMask.Required"))

if ls_risultato = "?" or ls_risultato = "!" then
	
	ls_risultato = upper(this.describe(dwo.name + ".Edit.Required"))
	
	if ls_risultato = "?" or ls_risultato = "!" then
		
		ls_risultato = upper(this.describe(dwo.name + ".dddw.case"))
		
		if ls_risultato = "?" or ls_risultato = "!" then
			
			return
			
		end if
		
	end if
	
end if

ll_num_riga = row
li_num_colonna = row

if ll_num_riga > 0 and li_num_colonna > 0 then
	if dwo.edit.style = "dddw" then
		
		// TODO - 21/10/2011 : controllo che la parent sia solo w_cs_xx_principale, altrimenti esco!
		
		lw_cs_xx_principale = parent
		for li_i = 1 to upperbound(lw_cs_xx_principale.is_nome_colonna)
			if isnull(lw_cs_xx_principale.is_nome_colonna[li_i]) then 
				li_num_colonne = li_i
				exit
			end if
		next
		if li_num_colonne = 0 then li_num_colonne = 1
	
		ls_nome_colonna = dwo.dbname
		ls_nome_colonna = mid(ls_nome_colonna, pos(ls_nome_colonna, ".") + 1)
		ls_nome_datawindow = dataobject
		
		select voci_repository.collegamento, colonne.nome_tabella, colonne.nome_codice, 
				 colonne.nome_descrizione, colonne.selezione
		into   :ls_nome_window, :ls_nome_tabella, :ls_nome_codice, :ls_nome_descrizione, 
				 :ls_selezione
		from   colonne, voci_repository
		where  colonne.nome_colonna = :ls_nome_colonna and
				 colonne.nome_datawindow = :ls_nome_datawindow and
				 colonne.id_voce = voci_repository.id_voce;
	
		if isnull(ls_nome_window) or ls_nome_window = "" then
			select voci_repository.collegamento, colonne.nome_tabella, colonne.nome_codice, 
					 colonne.nome_descrizione, colonne.selezione
			into   :ls_nome_window, :ls_nome_tabella, :ls_nome_codice, :ls_nome_descrizione, 
					 :ls_selezione
			from   colonne, voci_repository
			where  colonne.nome_colonna = :ls_nome_colonna and
					 colonne.nome_datawindow = 'TUTTI' and
					 colonne.id_voce = voci_repository.id_voce;
		end if
			
		if not isnull(ls_nome_window) and ls_nome_window <> "" then
			lw_cs_xx_principale.is_nome_colonna[li_num_colonne] = ls_nome_colonna
			lw_cs_xx_principale.is_nome_datawindow[li_num_colonne] = this
			lw_cs_xx_principale.is_nome_tabella[li_num_colonne] = ls_nome_tabella
			lw_cs_xx_principale.is_nome_codice[li_num_colonne] = ls_nome_codice
			lw_cs_xx_principale.is_nome_descrizione[li_num_colonne] = ls_nome_descrizione
			lw_cs_xx_principale.is_selezione[li_num_colonne] = ls_selezione
			window_type_open(lw_window, ls_nome_window, -1)
		end if
	end if
end if

//aggiunto per gestione multilingua Enme 8/2/2008

//if row < 1 then return

if s_cs_xx.admin then
	string ls_nome_pk_1, ls_nome_pk_2, ls_nome_colonna_descrizione
	str_des_multilingua lstr_des_multilingua
	
	ls_nome_tabella = this.Object.DataWindow.Table.UpdateTable
	
	select nome_pk_1, nome_pk_2, nome_colonna_descrizione
	into   :ls_nome_pk_1, :ls_nome_pk_2, :ls_nome_colonna_descrizione
	from   tab_tabelle_lingue
	where  nome_tabella = :ls_nome_tabella;
	
	if sqlca.sqlcode = 0 then
	
		lstr_des_multilingua.nome_tabella = ls_nome_tabella
		lstr_des_multilingua.descrizione_origine = getitemstring(row,ls_nome_colonna_descrizione)
		lstr_des_multilingua.chiave_str_1 = getitemstring(row,ls_nome_pk_1)
		if not isnull(ls_nome_pk_2) and len(ls_nome_pk_2) > 0 then
			lstr_des_multilingua.chiave_str_2 = getitemstring(row,ls_nome_pk_2)
		else
			setnull(lstr_des_multilingua.chiave_str_2)
		end if
		
		if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
		
		openwithparm(w_des_multilingua, lstr_des_multilingua)
	end if
end if
end event

event pcd_new;call super::pcd_new;long ll_i
uo_cs_xx_dw luo_cs_xx_dw

if not i_extendmode then return

if ib_proteggi_chiavi then uf_proteggi_chiavi()

for ll_i = 1 to i_numchildren
   if i_childdw[ll_i].i_currentmode <> c_modenew     and &
      i_childdw[ll_i].i_currentmode <> c_modemodify  and &
      i_childdw[ll_i].i_allownew                    then
      luo_cs_xx_dw = i_childdw[ll_i]
      if luo_cs_xx_dw.i_sharedata        or &
         (not luo_cs_xx_dw.i_sharedata  and &
          luo_cs_xx_dw.ib_dettaglio)   then
         luo_cs_xx_dw.set_dw_modify()
         if luo_cs_xx_dw.ib_proteggi_chiavi then luo_cs_xx_dw.uf_proteggi_chiavi()
      end if
   end if
next

ib_stato_nuovo = true

postevent("ue_proteggi_campi_insert")
end event

event pcd_modify;call super::pcd_modify;long ll_i
uo_cs_xx_dw luo_cs_xx_dw

if not i_extendmode then return

if ib_proteggi_chiavi then uf_proteggi_chiavi()

for ll_i = 1 to i_numchildren
   if i_childdw[ll_i].i_currentmode <> c_modenew     and &
      i_childdw[ll_i].i_currentmode <> c_modemodify  and &
      i_childdw[ll_i].i_allowmodify                 then
      luo_cs_xx_dw = i_childdw[ll_i]
      if luo_cs_xx_dw.i_sharedata        or &
         (not luo_cs_xx_dw.i_sharedata  and &
          luo_cs_xx_dw.ib_dettaglio)   then
         luo_cs_xx_dw.set_dw_modify()
         if luo_cs_xx_dw.ib_proteggi_chiavi then luo_cs_xx_dw.uf_proteggi_chiavi()
      end if
   end if
next

ib_stato_modifica = true

postevent("ue_proteggi_campi_insert")
end event

event pcd_delete;call super::pcd_delete;ib_stato_cancellazione = true
end event

event pcd_view;call super::pcd_view;ib_stato_nuovo = false
ib_stato_modifica = false
ib_stato_cancellazione = false
ib_stato_visualizzazione = true
end event

event itemfocuschanged;call super::itemfocuschanged;string ls_risultato

ls_risultato = upper(this.describe(dwo.name + ".EditMask.Required"))
if ls_risultato <> "?" then
	postevent("ue_selecttext")
end if
end event

on uo_cs_xx_dw.create
call super::create
end on

on uo_cs_xx_dw.destroy
call super::destroy
end on

event pcd_saverowsas;// In questo script il flag EXTEND ANCESTOR deve essere disattivato

saveas()
end event

event sqlpreview;call super::sqlpreview;// ---------------------  Enrico 27/4/2004 ----------------------------------
//   Nuova gestione filtri automatici
boolean lb_variazioni=false
string ls_nome_dw, ls_sql, ls_flag_tipo_valore, ls_sql_select, ls_sql_where, ls_valore, ls_sql_order,ls_filter
long ll_row, ll_i, ll_pos, ll_ret
datastore lds_filtri

if sqltype = PreviewDelete! then return

ls_sql = ""
lds_filtri = CREATE datastore
lds_filtri.dataobject = 'd_ds_tab_dw_filtri'
lds_filtri.settransobject(sqlca)
ls_nome_dw = this.dataobject
ll_row = lds_filtri.retrieve(s_cs_xx.cod_azienda, ls_nome_dw)
if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_filter = "cod_utente = '" + s_cs_xx.cod_utente + "' or isnull(cod_utente)"
	lds_filtri.setfilter(ls_filter)
	lds_filtri.filter()
	ll_row = lds_filtri.rowcount()
end if
if ll_row > 0 then
	
	choose case sqltype
		case PreviewInsert! 			// è il caso la sintassi SQL della DW è  INSERT
			
			// modificato da EnMe 6/2/08: se visualizza subito allora non impongo in insert, perchè 
			// è già stato imposto a livello di DW
			ls_filter=""
			if s_cs_xx.cod_utente <> "CS_SYSTEM" then
				ls_filter = "(cod_utente = '" + s_cs_xx.cod_utente + "' or isnull(cod_utente)) and "
			end if
			ls_filter = ls_filter + " flag_visualizza_subito = 'N' "
			lds_filtri.setfilter(ls_filter)
			lds_filtri.filter()
			ll_row = lds_filtri.rowcount()
			
			ls_sql = sqlsyntax
			
			for ll_i = 1 to ll_row
				
				ls_flag_tipo_valore = lds_filtri.getitemstring(ll_i, "flag_tipo_valore") 
				
				choose case ls_flag_tipo_valore
					case "S"		// stringa
						ls_valore = "'" + lds_filtri.getitemstring(ll_i, "valore_select") + "'"
						if isnull(ls_valore) then ls_valore = ""
						
					case "N"		// numero
						ls_valore = lds_filtri.getitemstring(ll_i, "valore_select")
						if isnull(ls_valore) then ls_valore = ""
						
					case "D"		// data
						ls_valore = "'" + lds_filtri.getitemstring(ll_i, "valore_select") + "'"
						if isnull(ls_valore) then ls_valore = ""
				end choose
				
				ls_sql = uof_inserisci_valore_insert(ls_sql, lds_filtri.getitemstring(ll_i, "colonna_db"), ls_valore)

			next
			
			ll_ret = setsqlpreview(ls_sql)
			
			if ll_ret < 1 then
				g_mb.messagebox("FrameWork", "Errore nella impostazione filtri automatici (UO_CS_XX_DW.SQLPREVIEW)")
			end if
			
			
		case PreviewUpdate!	
			// inquesto caso non faccio nulla
			
		case PreviewDelete!	
			
			// in questo caso non devo fare niente perchè vado a cancellare record che ho già visualizzato nella DW
			
		case PreviewSelect!	
			
			if s_cs_xx.cod_utente <> "CS_SYSTEM" then
				
				for ll_i = 1 to ll_row
					if len(ls_sql) > 0 then
						ls_sql += " and "
					end if
					
					ls_sql += lds_filtri.getitemstring(ll_i, "colonna_db")
					ls_sql += " "
					ls_sql += lds_filtri.getitemstring(ll_i, "operatore") 
					ls_sql += " "
					ls_flag_tipo_valore = lds_filtri.getitemstring(ll_i, "flag_tipo_valore") 
					choose case ls_flag_tipo_valore
						case "S"		// stringa
							ls_sql += "'" + lds_filtri.getitemstring(ll_i, "valore_select") + "'"
							
						case "N"		// numero
							ls_sql += lds_filtri.getitemstring(ll_i, "valore_select")
							
						case "D"		// data
							ls_sql += lds_filtri.getitemstring(ll_i, "valore_select")
							
					end choose
				next
			
				// modifica della sintassi SQL a seconda dell'azione da compiere
				if ls_sql <> "" then
					
					ll_pos = pos(upper(sqlsyntax), "WHERE")
					
					if ll_pos > 0 then
						
						ls_sql_select = left(sqlsyntax, ll_pos - 1)
						ls_sql_where = mid(sqlsyntax, ll_pos)
						
						ls_sql_where = " where ( " + ls_sql + ") and " + mid(ls_sql_where, 6)
						
					else
						// non c'è la WHERE controllo se c'è un ORDER BY					
						ll_pos = pos(upper(sqlsyntax), "ORDER BY")
	
						if ll_pos > 0 then
							
							ls_sql_select = left(sqlsyntax, ll_pos - 1)
							ls_sql_order = mid(sqlsyntax, ll_pos)
							
							ls_sql_where = " where ( " + ls_sql + ") " + ls_sql_order
						
						else		// ne where ne order by			
						
							ls_sql_select = sqlsyntax
							ls_sql_where = " where ( " + ls_sql + ") "
							
						end if
					end if
					
					ls_sql = ls_sql_select + ls_sql_where
					
					ll_ret = setsqlpreview(ls_sql)
					if ll_ret < 1 then
						g_mb.messagebox("FrameWork", "Errore nella impostazione filtri automatici (UO_CS_XX_DW.SQLPREVIEW)")
					end if
					
				end if
				
			end if
			
	end choose
end if

destroy lds_filtri

end event

event updateend;call super::updateend;postevent("ue_imposta_campi_note")
end event

event pcd_validaterow;call super::pcd_validaterow;// EnMe 18/08/2008
// Gestione colonne obbligatorie
string ls_return

ls_return = uof_controlla_obbligatori(dataobject, i_rownbr  )

if len(ls_return) > 0 then 
	g_mb.messagebox("CAMPI OBBLIGATORI", ls_return)
	PCCA.Error = c_Fatal
end if

end event

