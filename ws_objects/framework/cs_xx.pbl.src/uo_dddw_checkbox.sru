﻿$PBExportHeader$uo_dddw_checkbox.sru
forward
global type uo_dddw_checkbox from datawindow
end type
end forward

global type uo_dddw_checkbox from datawindow
integer width = 1019
integer height = 620
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
event ue_keypress pbm_dwnkey
event ue_post_focus ( )
end type
global uo_dddw_checkbox uo_dddw_checkbox

type variables
constant string DETAIL_STRING = "detail(height=80)"
constant string CHECK_COLUMN = "checked"

public:
	long il_color_selected_row = 16768443
	long il_color_unselected_row = 16777215
	
	// Indica la grandezza in percentuale
	// Se posto a 0 viene utilizzata quella impostata dal programmatore
	// in design-time
	int ii_width_percent = 100
	
private:
	// Indica lo stato di selezione:
	// S = Tutte le righe selezionate
	// N = Tutte le righe deselezionate
	// X = Misto
	string is_selected_status = "N"
	
	string is_select = "N"
	
	// Reference con padre
	datawindow idw_parent
	string is_parent_column
	string is_show_column
	
	
	// Test
	string is_all_column_selected = "TUTTI"
end variables

forward prototypes
public function boolean uof_set_column (string as_columns, string as_table, string as_where, string as_order, ref transaction at_transaction)
private subroutine uof_apply_style ()
private subroutine uof_sort_column (string as_column_name)
public subroutine uof_set_select_on_all_rows ()
private function string uof_getitemdata (integer ai_row, string as_column)
public function string uof_get_selected_column (string as_column)
private subroutine uof_position ()
public function integer show ()
private subroutine uof_toggle_selection (integer ai_row)
private function string uof_inject_checkbox (string as_syntax)
public subroutine uof_set_parent_dw (datawindow adw_parent, string as_parent_column)
public subroutine uof_set_parent_dw (datawindow adw_parent, string as_parent_column, string as_show_column)
public function boolean uof_set_column (string as_columns, string as_table)
public function boolean uof_set_column (string as_columns, string as_table, string as_where)
public function boolean uof_set_column (string as_columns, string as_table, string as_where, string as_order)
public subroutine uof_render_selection_on_parent ()
private function string uof_getitemdata (integer ai_row, string as_column, boolean ab_add_quote_on_string)
public subroutine uof_set_width (integer ai_width_percent)
public function integer uof_get_selected_column_as_array (string as_column, ref string as_keys[], boolean ab_prepare_for_in)
public subroutine uof_select_all_rows ()
public subroutine uof_deselect_all_rows ()
public function integer uof_get_selected_column_as_array (string as_column, ref string as_keys[])
public subroutine uof_set_column_width (integer ai_position, integer ai_width)
public subroutine uof_set_column_width (string as_column, integer ai_width)
public function boolean uof_set_column_by_sql (string as_sql, ref transaction at_transaction)
public function boolean uof_set_column_by_sql (string as_sql)
public subroutine uof_set_column_property (integer ai_position, string as_property, string as_value)
public subroutine uof_set_column_name (integer ai_position, string as_title)
public subroutine uof_set_column_align (integer ai_position, string as_align)
public function string uof_get_selected_column (string as_column, boolean ab_prepare_for_in)
public function string uof_get_selected_column ()
public subroutine uof_set_select_on_filtered_rows (string as_column_filter, string as_filter)
end prototypes

event ue_keypress;if key = KeyEscape! then
	
	hide()
	
elseif key = KeyEnter! then
	
	uof_toggle_selection(getrow())
	
end if
end event

event ue_post_focus();setfocus()
end event

public function boolean uof_set_column (string as_columns, string as_table, string as_where, string as_order, ref transaction at_transaction);string ls_sql, ls_error, ls_syntax, ls_presentation

// force visible false
visible = false

ls_sql = "SELECT " + as_columns + " FROM " + as_table

if not isnull(as_where) and as_where <> "" then
	ls_sql +=	 " WHERE " + as_where
end if
if not isnull(as_order) and as_order <> "" then
	ls_sql +=	 " ORDER BY " + as_order
end if

return uof_set_column_by_sql(ls_sql, at_transaction)
end function

private subroutine uof_apply_style ();/**
 * stefanop
 * 10/12/2012
 *
 * Applicato lo stile alla dw.
 **/
 
string ls_error
int li_i, li_column

// Colore delle righe selezionate
modify("DataWindow.Detail.Color=~"" + string(il_color_unselected_row) + "~tif(" + CHECK_COLUMN + " ='S', " + string(il_color_selected_row) + ","  + string(il_color_unselected_row) + ")~"")

// Blocco lo spostamento delle colonne
modify("Datawindow.Grid.Columnmove=no")

// Blocco la selezione del mouse
modify("Datawindow.Selected.Mouse=no")

// Blocco resize
modify("DataWindow.Row.Resize=0")

// Progetto le colonne, tranne la prima
li_column = integer(describe("Datawindow.Column.Count"))
for li_i = 2 to li_column
	modify("#"+ string(li_i) + ".Protect=1")
next

end subroutine

private subroutine uof_sort_column (string as_column_name);/**
 * stefanop
 * 10/12/2012
 *
 * Ordino i valori della colonna selezionata
 **/
 
 
string ls_tag

if g_str.end_with(as_column_name, "_t") then
	as_column_name = mid(as_column_name, 1,  len(as_column_name) - 2)
end if

// colonna "Sel." ?
if as_column_name = CHECK_COLUMN then
	uof_set_select_on_all_rows()
	return
end if

// altre colonne, procedo con il sort
ls_tag = describe(as_column_name + ".Tag")

if ls_tag = "ASC" then	
	ls_tag = "DESC"
else
	ls_tag = "ASC"
end if

modify(as_column_name + ".Tag='" + ls_tag + "'")
SetSort(as_column_name + " " + ls_tag)
sort()
end subroutine

public subroutine uof_set_select_on_all_rows ();/**
 * stefanop
 * 28/06/2012
 *
 * Imposta lo stato a tutte le righe con quello passato per valore
 **/

string ls_status
long ll_i

setredraw(false)

if is_select = "N" then
	is_select = "S"
else
	is_select = "N"
end if

for ll_i = 1 to rowcount()
	setitem(ll_i, CHECK_COLUMN, is_select)
next

// Se ho filtrato la lista, allora non so se ho selezionato tutto oppure no
if filteredcount() > 0 then
	is_selected_status = "X"
end if

setredraw(true)
end subroutine

private function string uof_getitemdata (integer ai_row, string as_column);return uof_getitemdata(ai_row, as_column, true)
end function

public function string uof_get_selected_column (string as_column);return uof_get_selected_column(as_column, true)
end function

private subroutine uof_position ();/**
 * stefanop
 * 06/06/2012
 *
 * Calcolo le posizione di dove far apparire la dw.
 * Le posizioni vengono calcolate in base all'offset x della colonna nella dw e della dw nella finestra
 **/
 
long ll_y, ll_width

ll_y = idw_parent.y + integer(idw_parent.describe(is_parent_column + ".y"))
ll_width = integer(idw_parent.describe(is_parent_column + ".width"))

//if ib_add_offset_height then
ll_y += integer(idw_parent.describe(is_parent_column + ".height"))
//end if

this.x = idw_parent.x + integer(idw_parent.describe(is_parent_column + ".x"))
this.y = ll_y
end subroutine

public function integer show ();
visible = true
setcolumn(1)
postevent("ue_post_focus")

return 1
end function

private subroutine uof_toggle_selection (integer ai_row);
if getitemstring(ai_row, CHECK_COLUMN) = "S" then
	setitem(ai_row, CHECK_COLUMN, "N")
else
	setitem(ai_row, CHECK_COLUMN, "S")
end if
end subroutine

private function string uof_inject_checkbox (string as_syntax);/**
 * stefanop
 * 10/12/2012
 *
 * Sostituisco la prima colonna "finta"
 * con la colonna e il testo della colonna con "Sel."
 **/
 
string ls_check, ls_left, ls_right
int li_pos_1, li_length

ls_check = "~r~ncolumn(band=detail id=1 alignment=~"2~" tabsequence=10 border=~"0~" color=~"33554432~" x=~"14~" y=~"8~" height=~"64~" width=~"110~" format=~"[general]~" html.valueishtml=~"0~"  name=" + CHECK_COLUMN +" visible=~"1~" checkbox.text=~"~" checkbox.on=~"S~" checkbox.off=~"N~" checkbox.scale=no checkbox.threed=yes  font.face=~"Tahoma~" font.height=~"-10~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"1~" background.color=~"536870912~" background.transparency=~"0~" background.gradient.color=~"8421504~" background.gradient.transparency=~"0~" background.gradient.angle=~"0~" background.brushmode=~"0~" background.gradient.repetition.mode=~"0~" background.gradient.repetition.count=~"0~" background.gradient.repetition.length=~"100~" background.gradient.focus=~"0~" background.gradient.scale=~"100~" background.gradient.spread=~"100~" tooltip.backcolor=~"134217752~" tooltip.delay.initial=~"0~" tooltip.delay.visible=~"32000~" tooltip.enabled=~"0~" tooltip.hasclosebutton=~"0~" tooltip.icon=~"0~" tooltip.isbubble=~"0~" tooltip.maxwidth=~"0~" tooltip.textcolor=~"134217751~" tooltip.transparency=~"0~" transparency=~"0~" )~r~n"
ls_check += "~r~ntext(band=header text=~"Sel.~" x=~"14~" y=~"8~" height=~"52~" width=~"110~" font.face=~"Tahoma~" font.height=~"-8~" font.weight=~"400~" font.charset=~"0~" font.pitch=~"2~" font.family=~"2~" font.underline=~"0~" font.italic=~"0~" font.strikethrough=~"0~" border=~"0~" color=~"0~" background.mode=~"1~" background.color=~"536870912~" background.brushmode=~"0~" background.gradient.color=~"8421504~" background.gradient.scale=~"100~" background.gradient.spread=~"100~" background.gradient.repetition.mode=~"0~" background.gradient.repetition.count=~"0~" background.gradient.repetition.length=~"100~"  alignment=~"2~"  name=" + CHECK_COLUMN + "_t )~r~n"

li_length = len(DETAIL_STRING)
li_pos_1 = pos(as_syntax, DETAIL_STRING)
ls_left = left(as_syntax, li_pos_1 + li_length)
ls_right = mid(as_syntax, pos(as_syntax, "column(band=detail id=2",  li_pos_1 + 1))

return ls_left + ls_check + ls_right
end function

public subroutine uof_set_parent_dw (datawindow adw_parent, string as_parent_column);/**
 * stefanop
 * 10/12/2012
 *
 * Imposto il padre della DW
 **/
 
 
uof_set_parent_dw(adw_parent, as_parent_column, as_parent_column)
end subroutine

public subroutine uof_set_parent_dw (datawindow adw_parent, string as_parent_column, string as_show_column);/**
 * stefanop
 * 10/12/2012
 *
 * Imposto il padre della DW
 **/
 
 
string ls_error
 
idw_parent = adw_parent
is_parent_column = as_parent_column
is_show_column = as_show_column

idw_parent.setitem(idw_parent.getrow(), is_parent_column, is_all_column_selected)
idw_parent.modify(is_parent_column + ".Edit.Limit=0")

// posiziono DW
uof_position()
uof_set_width(ii_width_percent)
end subroutine

public function boolean uof_set_column (string as_columns, string as_table);return uof_set_column(as_columns, as_table, "", "", sqlca)
end function

public function boolean uof_set_column (string as_columns, string as_table, string as_where);return uof_set_column(as_columns, as_table, as_where, "", sqlca)
end function

public function boolean uof_set_column (string as_columns, string as_table, string as_where, string as_order);return uof_set_column(as_columns, as_table, as_where, as_order, sqlca)
end function

public subroutine uof_render_selection_on_parent ();/**
 * stefanop
 * 10/12/2012
 *
 * Visualizzo le colonne selezionate nel padre
 **/
 
string ls_selected[], ls_partial
int li_i, li_rowcount, li_row_selected, li_j, li_filteredcount
 
if is_selected_status <> "X" then
	
	idw_parent.setitem(idw_parent.getrow(), is_parent_column, is_all_column_selected)
	return
	
end if

li_rowcount = rowcount()
li_filteredcount = filteredcount()

for li_i = 1 to li_rowcount
	
	if getitemstring(li_i, CHECK_COLUMN) <> "S" then continue
	
	li_row_selected++
	ls_selected[li_row_selected] = uof_getitemdata(li_i, is_show_column, false)
				
next

if li_row_selected = (li_rowcount + li_filteredcount) then
	idw_parent.setitem(idw_parent.getrow(), is_parent_column, is_all_column_selected)
else
	idw_parent.setitem(idw_parent.getrow(), is_parent_column, g_str.implode(ls_selected, ", "))
end if
end subroutine

private function string uof_getitemdata (integer ai_row, string as_column, boolean ab_add_quote_on_string);/**
 * stefanop
 * 10/12/2012
 *
 * Recupero il valore della DW in base al tipo colonna
 **/
 
string ls_value, ls_coltype

ls_coltype = lower(describe(as_column + ".ColType"))

choose case left(ls_coltype, 4)
		
	case "char"
		ls_value = getitemstring(ai_row, as_column)
		
		if ab_add_quote_on_string then ls_value = "'" + ls_value + "'"
		
	case "date"
		ls_value = string(getitemdate(ai_row, as_column), s_cs_xx.db_funzioni.formato_data)
		
		if ab_add_quote_on_string then ls_value = "'" + ls_value + "'"
		
	case "deci", "int", "long", "numb", "real", "ulon"
		ls_value = string(getitemnumber(ai_row, as_column))
		
	case "time"
		ls_value = string(getitemtime(ai_row, as_column))
		
end choose

return ls_value
end function

public subroutine uof_set_width (integer ai_width_percent);ii_width_percent = ai_width_percent

if isvalid(idw_parent) and not isnull(idw_parent) and ii_width_percent > 0 then
	
	// Calcolo la larghezza in percentuale
	width = integer(idw_parent.describe(is_parent_column + ".Width")) * (ii_width_percent / 100)
	
end if
end subroutine

public function integer uof_get_selected_column_as_array (string as_column, ref string as_keys[], boolean ab_prepare_for_in);/**
 * stefanop
 * 10/12/2012
 *
 * Ritorna in array tutte le chiavi selezionate
 * as_columna = nome della colonna della dw da recuperare
 * (ref) as_keys[] = array delle chiavi trovate
 * ab_prepare_for_in = aggiunte ' alle stringhe, cosi da essere pronte da usare nell'SQL
 *
 **/
 
string ls_empty[], ls_pk_selected[]
int li_i, li_rowcount, li_row_selected, li_filteredcount

as_keys = ls_empty
li_row_selected = 0

// Controllo che non siano selezionate o deselezionate tutte le righe
if is_selected_status <> "X" then return 0

li_rowcount = rowcount()
li_filteredcount = filteredCount()

for li_i = 1 to li_rowcount
	
	if getitemstring(li_i, CHECK_COLUMN) <> "S" then continue
	
	li_row_selected++
	
	ls_pk_selected[li_row_selected] = uof_getitemdata(li_i, as_column, ab_prepare_for_in)
			
next

// controllo che il numero di righe selezionate non sia uguale a quelle della DW,
// altrimenti vuol dire che ho selezionato tutto a mano.
if li_row_selected = (li_rowcount + li_filteredcount) then return 0

as_keys = ls_pk_selected
return li_row_selected
end function

public subroutine uof_select_all_rows ();/**
 * stefanop
 * 11/12/2012
 *
 * Imposta tutte le righe selezionate
 **/

long ll_i, ll_count, li_filteredcount

ll_count = rowcount()
li_filteredcount = filteredcount()

setredraw(false)

for ll_i = 1 to ll_count
	setitem(ll_i, CHECK_COLUMN, "S")
next

if ll_count = (ll_count + li_filteredcount) then
	is_selected_status = "S"
else
	is_selected_status = "X"
end if

setredraw(true)
end subroutine

public subroutine uof_deselect_all_rows ();/**
 * stefanop
 * 11/12/2012
 *
 * Deseleziona tutte le righe
 **/

long ll_i, ll_count

ll_count = rowcount()
setredraw(false)

for ll_i = 1 to ll_count
	setitem(ll_i, CHECK_COLUMN, "N")
next

is_selected_status = "N"
setredraw(true)
end subroutine

public function integer uof_get_selected_column_as_array (string as_column, ref string as_keys[]);return uof_get_selected_column_as_array(as_column, as_keys, false)
end function

public subroutine uof_set_column_width (integer ai_position, integer ai_width);
//if ai_position > 0 and ai_position + 1 > integer(Object.Colums.Count)  then return

uof_set_column_property(ai_position, "Width", string(ai_width))
end subroutine

public subroutine uof_set_column_width (string as_column, integer ai_width);modify(as_column + ".Width=" + string(ai_width))
end subroutine

public function boolean uof_set_column_by_sql (string as_sql, ref transaction at_transaction);string ls_sql, ls_error, ls_syntax, ls_presentation

// force visible false
visible = false

ls_presentation = "style( type=Grid )"
//ls_presentation += " text( Font.Face='Arial' Font.Height=-9 )"
//ls_presentation += " column( Font.Face='Arial' Font.Height=-9 )"

as_sql = "SELECT 'N' as checked, " + mid(as_sql, 7)

ls_syntax = at_transaction.SyntaxFromSQL(as_sql, ls_presentation, ls_error)
ls_syntax = uof_inject_checkbox(ls_syntax)

if len(ls_error) > 0 then
	g_mb.error("Errore creazione sintassi SQL dw_selezione!~r~n" + ls_error)
	return false
else
	create(ls_syntax, ls_error)
	if len(ls_error) > 0 then
		g_mb.error("Errore creazione datawindow dw_selezione!~r~n" + ls_error)
		return false
	end if
end if

uof_apply_style()

settransobject(at_transaction)
retrieve()

return true
end function

public function boolean uof_set_column_by_sql (string as_sql);return uof_set_column_by_sql(as_sql, sqlca)
end function

public subroutine uof_set_column_property (integer ai_position, string as_property, string as_value);
//if ai_position > 0 and ai_position + 1 > integer(Object.Colums.Count)  then return

modify("#" + string(ai_position + 1) + "." + as_property + "=" + as_value)
end subroutine

public subroutine uof_set_column_name (integer ai_position, string as_title);string ls_column_name, ls_error
//if ai_position > 0 and ai_position + 1 > integer(Object.Colums.Count)  then return

ls_column_name = describe("#" + string(ai_position + 1) + ".Name")
ls_error = modify(ls_column_name + "_t.Text='" + as_title + "'")

if len(ls_error) > 0 then
	g_mb.error(ls_error)
end if
end subroutine

public subroutine uof_set_column_align (integer ai_position, string as_align);string ls_column_name, ls_error
int li_align

choose case lower(as_align)
	case "left"
		li_align = 1
		
	case "right"
		li_align = 3
		
	case "center"
		li_align = 2
		
end choose
//if ai_position > 0 and ai_position + 1 > integer(Object.Colums.Count)  then return

ls_column_name = describe("#" + string(ai_position + 1) + ".Name")
ls_error = modify(ls_column_name + "_t.Alignment=" + string(li_align))
ls_error = modify("#" + string(ai_position + 1) + ".Alignment=" + string(li_align))

if len(ls_error) > 0 then
	g_mb.error(ls_error)
end if
end subroutine

public function string uof_get_selected_column (string as_column, boolean ab_prepare_for_in);/**
 * stefanop
 * 10/12/2012
 *
 * Recupero le righe selezionate
 * Ritorna NULL se non ho selezionato nulla o se ho selezionato tutto
 **/
 
string ls_empty, ls_pks[], ls_partial_pk
int li_count

li_count = uof_get_selected_column_as_array(as_column, ls_pks, ab_prepare_for_in)

if li_count > 0 then
	
	if ab_prepare_for_in then
		// colonna IN (val_1, val_2, ...)
		return " " + as_column + " IN (" + g_str.implode(ls_pks, ",") + ") "
	else
		return g_str.implode(ls_pks, ",")
	end if
	
else
	
	setnull(ls_empty)
	return ls_empty
	
end if

end function

public function string uof_get_selected_column ();return uof_get_selected_column(is_show_column, true)
end function

public subroutine uof_set_select_on_filtered_rows (string as_column_filter, string as_filter);/**
 * stefanop
 * 28/06/2012
 *
 * Imposta lo stato a tutte le righe con quello passato per valore
 **/

string ls_status
long ll_i

setredraw(false)

for ll_i = 1 to rowcount()
	if pos(as_filter, getitemstring(ll_i,as_column_filter)) > 0 then
		setitem(ll_i, CHECK_COLUMN, "S")
	else
		setitem(ll_i, CHECK_COLUMN, "N")
	end if		
next

// Se ho filtrato la lista, allora non so se ho selezionato tutto oppure no
if filteredcount() > 0 then
	is_selected_status = "X"
end if

setredraw(true)
end subroutine

on uo_dddw_checkbox.create
end on

on uo_dddw_checkbox.destroy
end on

event rowfocuschanged;selectrow(0, false)
selectrow(currentrow, true)

end event

event clicked;if row > 0 then
	// Riga
	is_selected_status = "X"
	setrow(row)
	
end if
end event

event doubleclicked;if row < 1 then
	
	// Intestazione
	uof_sort_column(string(dwo.name))
	
else
	
	uof_toggle_selection(row)
	
end if
end event

event losefocus;hide()
uof_render_selection_on_parent()
end event

