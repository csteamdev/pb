﻿$PBExportHeader$uo_ds.sru
forward
global type uo_ds from datastore
end type
end forward

global type uo_ds from datastore
end type
global uo_ds uo_ds

forward prototypes
public function long uof_get_column_count ()
public function any getitemany (long al_row, long al_column)
public function string coltype (integer ai_column)
public function string uof_row_to_string (long al_row, string as_separator)
end prototypes

public function long uof_get_column_count ();return Long(Object.DataWindow.Column.Count)
end function

public function any getitemany (long al_row, long al_column);/**
 * stefanop
 * 08/09/2014
 *
 **/
 
string ls_coltype
any la_value

ls_coltype = coltype(al_column)

choose case left(upper(ls_coltype),3)
	case "CHA"
		la_value = getitemstring(al_row, al_column)
		
	case "DEC", "INT", "LON", "NUM", "REAL", "ULO"
		la_value = getitemdecimal(al_row, al_column)
		
	case else
		setnull(la_value)
		
end choose


return la_value
end function

public function string coltype (integer ai_column);/**
 * stefanop
 * 08/09/2014
 *
 * ritorna il tipo di colonna
 **/
 
return describe("#" + string(ai_column) + ".ColType")
end function

public function string uof_row_to_string (long al_row, string as_separator);/**
 * stefanop
 * 10/09/2014
 *
 * Converto la riga del datastore in stringa
 **/
 
string ls_separator
long ll_columns, ll_i
uo_string_builder sb

if al_row <= rowcount() then
	ll_columns = uof_get_column_count()
	ls_separator = ""
	sb = g_str.append(ls_separator)
	
	for ll_i = 1 to ll_columns
		sb.append(ls_separator).append(getitemany(al_row, ll_i))
		
		ls_separator = as_separator
	next
	
	return sb.tostring()
else
	return ""
end if
end function

on uo_ds.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ds.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

