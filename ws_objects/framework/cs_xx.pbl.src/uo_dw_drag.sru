﻿$PBExportHeader$uo_dw_drag.sru
forward
global type uo_dw_drag from datawindow
end type
end forward

global type uo_dw_drag from datawindow
integer width = 814
integer height = 512
string title = "none"
boolean border = false
boolean livescroll = true
event ue_drop_files pbm_dropfiles
event type boolean ue_end_drop ( boolean ab_status,  string as_message )
event type boolean ue_drop_file ( string as_filename,  ref string as_message )
event ue_start_drop ( integer ai_count,  ref boolean ab_return,  ref string as_message )
event type boolean ue_drop_dw_data ( dragobject aobj_drag,  ref string as_message )
end type
global uo_dw_drag uo_dw_drag

type prototypes
// aggiunte per abilitare drag&drog sulla DW dei Blob
function ulong DragQueryFileW( ulong hDrop, ulong iFile, ref string LPTSTR, ulong cb ) library 'shell32.dll'
subroutine DragAcceptFiles(ulong h, boolean b ) library 'shell32.dll'

end prototypes

type variables
protected:
	boolean		ib_drag_from_dw = false
end variables

forward prototypes
public subroutine uof_center_dw_text ()
public subroutine uof_center_dw_text (string as_message)
end prototypes

event ue_drop_files;integer			li_count, li_index
string				ls_file_name, ls_message
boolean			lb_ret

setpointer(Hourglass!)


li_count = DragQueryFileW(Message.WordParm, -1, ls_file_name, 0)

event trigger ue_start_drop(li_count, lb_ret, ls_message)

if lb_ret then
	
	ls_file_name = space(255)
	
	lb_ret = true
	
	for li_index = 1 to li_count
		
		DragQueryFileW(Message.WordParm, li_index - 1, ls_file_name, 255)
		
		lb_ret = event trigger ue_drop_file(ls_file_name,ls_message)
		if not lb_ret then
			exit
		end if
		
	next

end if


event trigger ue_end_drop(lb_ret, ls_message)

setpointer(Arrow!)




end event

event type boolean ue_end_drop(boolean ab_status, string as_message);

return true
end event

event type boolean ue_drop_file(string as_filename, ref string as_message);

return true
end event

event ue_start_drop(integer ai_count, ref boolean ab_return, ref string as_message);
ab_return = true

return


end event

event type boolean ue_drop_dw_data(dragobject aobj_drag, ref string as_message);

return true

end event

public subroutine uof_center_dw_text ();/**
 * stefanop
 * 04/08/2014
 *
 * Questa funzione imposta le dimensioni corrette alla dw e centra il testo della d_drag.
 **/
 
uof_center_dw_text("Trascina qui il file per allegarlo")

end subroutine

public subroutine uof_center_dw_text (string as_message);/**
 * stefanop
 * 04/08/2014
 *
 * Questa funzione imposta le dimensioni corrette alla dw e centra il testo della d_drag.
 **/
 
long ll_mid_height

ll_mid_height = integer(height / 2)

object.message_t.x = 0
object.message_t.width = width
object.message_t.y = ll_mid_height - 32
object.message_t.text = as_message
end subroutine

on uo_dw_drag.create
end on

on uo_dw_drag.destroy
end on

event constructor;
DragAcceptFiles(handle(this), true)
end event

event destructor;
DragAcceptFiles(handle(this), false)
end event

event dragdrop;long					ll_row
string					ls_errore
integer				li_ret
boolean				lb_ret

//SOLO se è abilitato il trascinamento dati da altra dw (post-it)
if ib_drag_from_dw then
	//commit o rollback (con messaggio di errore) fatto dalla seguente funzione, sovrascritta nell'oggetto ereditato
	lb_ret = event trigger ue_drop_dw_data(source, ls_errore)

end if

end event

