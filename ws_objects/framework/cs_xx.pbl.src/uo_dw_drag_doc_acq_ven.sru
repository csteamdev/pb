﻿$PBExportHeader$uo_dw_drag_doc_acq_ven.sru
forward
global type uo_dw_drag_doc_acq_ven from uo_dw_drag
end type
end forward

global type uo_dw_drag_doc_acq_ven from uo_dw_drag
boolean border = true
borderstyle borderstyle = styleraised!
end type
global uo_dw_drag_doc_acq_ven uo_dw_drag_doc_acq_ven

type variables
protected:
	string					is_gestione = "", is_tabella_padre= "", is_tabella_gestione = "", is_colonne_pk[], is_gestione_prec="", is_colonne_dw[]
	uo_cs_xx_dw		idw_data
	boolean				ib_enable_delete_blob = false, ib_testata_docs = false
	transaction			itran_note
end variables

forward prototypes
public subroutine uof_set_management (string as_gestione, uo_cs_xx_dw adw_data)
public subroutine uof_retrieve_blob (long al_row)
public subroutine uof_enabled_delete_blob ()
public function integer uof_get_blob (string as_collegato, integer ai_anno, long al_numero, long al_prog_riga, string as_cod_nota, ref blob ablb_documento, ref string as_errore)
public function boolean uof_inserisci_documento (integer ai_anno, long al_numero, long al_prog_riga, string as_cod_nota, string as_file, long al_prog_mimetype, blob ab_note_esterne, ref string as_errore)
public function boolean uof_inserisci_post_it (long al_row, datawindow adw_data, ref string as_errore)
public function integer uof_modifica_post_it (integer ai_anno, long al_numero, string as_cod_nota, string as_note, ref string as_errore)
public function integer uof_conferma_post_it (ref string as_valore)
public function boolean wf_controlla_operatore (ref string as_messaggio)
public function integer wf_abilita_disabilita (integer ai_anno, long al_numero, long al_riga, string as_cod_nota, string as_flag_collegato, ref integer ai_disabilitato, ref string as_errore)
end prototypes

public subroutine uof_set_management (string as_gestione, uo_cs_xx_dw adw_data);
ib_testata_docs = false

choose case UPPER(as_gestione)
	case "ORDACQ"
		//---------------------------------------------------------------------
		//nome tabella dettaglio gestione
		is_tabella_padre= "det_ord_acq"
		
		is_gestione_prec=""
		
		//nome tabella documenti
		is_tabella_gestione = "det_ord_acq_note"
		
		//PK tabella documenti (cod_azienda esclusa)
		is_colonne_pk[1] = "anno_registrazione"
		is_colonne_pk[2] = "num_registrazione"
		is_colonne_pk[3] = "prog_riga_ordine_acq"
		is_colonne_pk[4] = "cod_nota"
		
		is_colonne_dw[1] = "anno_registrazione"
		is_colonne_dw[2] = "num_registrazione"
		is_colonne_dw[3] = "prog_riga_ordine_acq"
		is_colonne_dw[4] = "cod_nota"
		
	//---------------------------------------------------------------------
	case "OFFVEN"
		//nome tabella dettaglio gestione
		is_tabella_padre= "det_off_ven"
		
		is_gestione_prec=""
		
		//nome tabella documenti
		is_tabella_gestione = "det_off_ven_note"
		
		//PK tabella documenti (cod_azienda esclusa)
		is_colonne_pk[1] = "anno_registrazione"
		is_colonne_pk[2] = "num_registrazione"
		is_colonne_pk[3] = "prog_riga_off_ven"
		is_colonne_pk[4] = "cod_nota"
		
		is_colonne_dw[1] = "anno_registrazione"
		is_colonne_dw[2] = "num_registrazione"
		is_colonne_dw[3] = "prog_riga_off_ven"	
		is_colonne_dw[4] = "cod_nota"
	
	//---------------------------------------------------------------------
	case "ORDVEN"
		//nome tabella dettaglio gestione
		is_tabella_padre= "det_ord_ven"
		
		is_gestione_prec="OFFVEN"
		
		//nome tabella documenti
		is_tabella_gestione = "det_ord_ven_note"
		
		//PK tabella documenti (cod_azienda esclusa)
		is_colonne_pk[1] = "anno_registrazione"
		is_colonne_pk[2] = "num_registrazione"
		is_colonne_pk[3] = "prog_riga_ord_ven"
		is_colonne_pk[4] = "cod_nota"
		
		is_colonne_dw[1] = "anno_registrazione"
		is_colonne_dw[2] = "num_registrazione"
		is_colonne_dw[3] = "prog_riga"				//gestione comune perchè c'è una UNION (ordini vendita - offerte vendita)
		is_colonne_dw[4] = "cod_nota"
		
	//---------------------------------------------------------------------
	case "BOLVEN"
		//nome tabella dettaglio gestione
		is_tabella_padre= "det_bol_ven"
		
		is_gestione_prec="ORDVEN"
		
		//nome tabella documenti
		is_tabella_gestione = "det_bol_ven_note"
		
		//PK tabella documenti (cod_azienda esclusa)
		is_colonne_pk[1] = "anno_registrazione"
		is_colonne_pk[2] = "num_registrazione"
		is_colonne_pk[3] = "prog_riga_bol_ven"
		is_colonne_pk[4] = "cod_nota"
		
		is_colonne_dw[1] = "anno_registrazione"
		is_colonne_dw[2] = "num_registrazione"
		is_colonne_dw[3] = "prog_riga"				//gestione comune perchè c'è una UNION (bolle vendita - ordini vendita)
		is_colonne_dw[4] = "cod_nota"
		
	//---------------------------------------------------------------------
	case "FATVEN"
		//nome tabella dettaglio gestione
		is_tabella_padre= "det_fat_ven"
		
		is_gestione_prec="ORDVEN"
		
		//nome tabella documenti
		is_tabella_gestione = "det_fat_ven_note"
		
		//PK tabella documenti (cod_azienda esclusa)
		is_colonne_pk[1] = "anno_registrazione"
		is_colonne_pk[2] = "num_registrazione"
		is_colonne_pk[3] = "prog_riga_fat_ven"
		is_colonne_pk[4] = "cod_nota"
		
		is_colonne_dw[1] = "anno_registrazione"
		is_colonne_dw[2] = "num_registrazione"
		is_colonne_dw[3] = "prog_riga"				//gestione comune perchè c'è una UNION (fatture vendita - ordini vendita)
		is_colonne_dw[4] = "cod_nota"
		
	case "TESORDVEN"
		//nome tabella gestione
		is_tabella_padre= "tes_ord_ven"
		
		is_gestione_prec=""
		
		//nome tabella documenti
		is_tabella_gestione = "tes_ord_ven_note"
		
		//PK tabella documenti (cod_azienda esclusa)
		is_colonne_pk[1] = "anno_registrazione"
		is_colonne_pk[2] = "num_registrazione"
		is_colonne_pk[3] = "cod_nota"
		
		is_colonne_dw[1] = "anno_registrazione"
		is_colonne_dw[2] = "num_registrazione"
		is_colonne_dw[3] = "cod_nota"	
	
		ib_testata_docs = true
	
	case "TESORDACQ"
		//nome tabella gestione
		is_tabella_padre= "tes_ord_acq"
		
		is_gestione_prec=""
		
		//nome tabella documenti
		is_tabella_gestione = "tes_ord_acq_note"
		
		//PK tabella documenti (cod_azienda esclusa)
		is_colonne_pk[1] = "anno_registrazione"
		is_colonne_pk[2] = "num_registrazione"
		is_colonne_pk[3] = "cod_nota"
		
		is_colonne_dw[1] = "anno_registrazione"
		is_colonne_dw[2] = "num_registrazione"
		is_colonne_dw[3] = "cod_nota"	
	
		ib_testata_docs = true
	
end choose

idw_data = adw_data
is_gestione = UPPER(as_gestione)

return
end subroutine

public subroutine uof_retrieve_blob (long al_row);

integer			li_anno
long				ll_numero, ll_prog_riga, ll_count

if not isvalid(idw_data) then return

this.reset()

ll_count = 0

if al_row > 0 then
	li_anno = idw_data.getitemnumber(al_row, is_colonne_pk[1])
	ll_numero = idw_data.getitemnumber(al_row, is_colonne_pk[2])
	
	if not isnull(li_anno) and li_anno > 0 then
	
		if ib_testata_docs then
			//gestione da tabelle di testata
			ll_count = this.retrieve(s_cs_xx.cod_azienda, li_anno, ll_numero  )
		else
			//gestione da tabelle di dettaglio righe
			ll_prog_riga = idw_data.getitemnumber(al_row, is_colonne_pk[3])
			ll_count = this.retrieve(s_cs_xx.cod_azienda, li_anno, ll_numero, ll_prog_riga  )
		end if
	end if
	
end if

end subroutine

public subroutine uof_enabled_delete_blob ();string			ls_flag_supervisore

ib_enable_delete_blob = false

if s_cs_xx.cod_utente<>"CS_SYSTEM" then
	select flag_supervisore
	into :ls_flag_supervisore
	from utenti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
	
	if ls_flag_supervisore="S" then ib_enable_delete_blob = true
else
	//sei CS_SYSTEM
	ib_enable_delete_blob = true
end if
end subroutine

public function integer uof_get_blob (string as_collegato, integer ai_anno, long al_numero, long al_prog_riga, string as_cod_nota, ref blob ablb_documento, ref string as_errore);string ls_gestione

if as_collegato="S" then
	ls_gestione = is_gestione_prec
else
	ls_gestione = is_gestione
end if


choose case UPPER(ls_gestione)
	case "ORDACQ"
		//---------------------------------------------------------------------
		selectblob 	note_esterne
		into 			:ablb_documento
		from det_ord_acq_note
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno and
				num_registrazione = :al_numero and
				prog_riga_ordine_acq = :al_prog_riga and
				cod_nota = :as_cod_nota;
		
	//---------------------------------------------------------------------
	case "OFFVEN"
		selectblob 	note_esterne
		into 			:ablb_documento
		from det_off_ven_note
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno and
				num_registrazione = :al_numero and
				prog_riga_off_ven = :al_prog_riga and
				cod_nota = :as_cod_nota;
	
	//---------------------------------------------------------------------
	case "ORDVEN"
		selectblob 	note_esterne
		into 			:ablb_documento
		from det_ord_ven_note
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno and
				num_registrazione = :al_numero and
				prog_riga_ord_ven = :al_prog_riga and
				cod_nota = :as_cod_nota;
	
	//---------------------------------------------------------------------
	case "BOLVEN"
		selectblob 	note_esterne
		into 			:ablb_documento
		from det_bol_ven_note
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno and
				num_registrazione = :al_numero and
				prog_riga_bol_ven = :al_prog_riga and
				cod_nota = :as_cod_nota;
				
	//---------------------------------------------------------------------
	case "FATVEN"
		selectblob 	note_esterne
		into 			:ablb_documento
		from det_fat_ven_note
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno and
				num_registrazione = :al_numero and
				prog_riga_fat_ven = :al_prog_riga and
				cod_nota = :as_cod_nota;
	
	case "TESORDVEN"
		selectblob 	note_esterne
		into 			:ablb_documento
		from tes_ord_ven_note
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno and
				num_registrazione = :al_numero and
				cod_nota = :as_cod_nota;
	
	case "TESORDACQ"
		selectblob 	note_esterne
		into 			:ablb_documento
		from tes_ord_acq_note
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno and
				num_registrazione = :al_numero and
				cod_nota = :as_cod_nota;
	
end choose

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura documento dalla tabella (sqlcode -1): "+sqlca.sqlerrtext
	return -1

elseif sqlca.sqlcode = 100 then
	as_errore = "Documento non trovato! Potrebbe essere stato cancellato da un altro utente!"
	return -1
	
elseif sqlca.sqlcode <> 0 then
	as_errore = "Errore in lettura documento dalla tabella (sqlcode <>-1,<>0 e <>100)"
	return -1
	
end if

return 0
end function

public function boolean uof_inserisci_documento (integer ai_anno, long al_numero, long al_prog_riga, string as_cod_nota, string as_file, long al_prog_mimetype, blob ab_note_esterne, ref string as_errore);string		ls_sql


guo_functions.uof_replace_string(as_file, "'", "")
guo_functions.uof_replace_string(as_file, '"', "")

ls_sql = "insert into " + is_tabella_gestione + " "+&  
				"( 	   cod_azienda,"+&
					  is_colonne_pk[1]+","+&
					  is_colonne_pk[2]+","+&
					  is_colonne_pk[3]+","
					  
if not ib_testata_docs then
	ls_sql += is_colonne_pk[4]+","
end if
					
ls_sql +=			  "des_nota,"+&
					  "note,"+&
					  "prog_mimetype) "+&  
		"values ('" + s_cs_xx.cod_azienda + "', "+&
				  string(ai_anno)+","+&
				  string(al_numero)+","
				  
if not ib_testata_docs then				  
	ls_sql +=  string(al_prog_riga)+","
end if

ls_sql += 	  "'"+as_cod_nota+"',"+&
				  "'"+as_file+"',"+&
				  "null,"+&
				  string(al_prog_mimetype) +")"

execute immediate :ls_sql using itran_note;

if itran_note.sqlcode = 0 then
	
	choose case is_gestione
		case "ORDACQ"
			//------------------------------------------------------------------
			updateblob det_ord_acq_note
			set note_esterne = :ab_note_esterne
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno and
					num_registrazione = :al_numero and
					prog_riga_ordine_acq = :al_prog_riga and
					cod_nota = :as_cod_nota
			using itran_note;
			//------------------------------------------------------------------
			
		case "OFFVEN"
			//------------------------------------------------------------------
			updateblob det_off_ven_note
			set note_esterne = :ab_note_esterne
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno and
					num_registrazione = :al_numero and
					prog_riga_off_ven = :al_prog_riga and
					cod_nota = :as_cod_nota
			using itran_note;
			//------------------------------------------------------------------
			
		case "ORDVEN"
			//------------------------------------------------------------------
			updateblob det_ord_ven_note
			set note_esterne = :ab_note_esterne
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno and
					num_registrazione = :al_numero and
					prog_riga_ord_ven = :al_prog_riga and
					cod_nota = :as_cod_nota
			using itran_note;
			//------------------------------------------------------------------
			
		case "BOLVEN"
			//------------------------------------------------------------------
			updateblob det_bol_ven_note
			set note_esterne = :ab_note_esterne
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno and
					num_registrazione = :al_numero and
					prog_riga_bol_ven = :al_prog_riga and
					cod_nota = :as_cod_nota
			using itran_note;
			//------------------------------------------------------------------
			
		case "FATVEN"
			//------------------------------------------------------------------
			updateblob det_fat_ven_note
			set note_esterne = :ab_note_esterne
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno and
					num_registrazione = :al_numero and
					prog_riga_fat_ven = :al_prog_riga and
					cod_nota = :as_cod_nota
			using itran_note;
			//------------------------------------------------------------------
			
		case "TESORDVEN"
			//------------------------------------------------------------------
			updateblob tes_ord_ven_note
			set note_esterne = :ab_note_esterne
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno and
					num_registrazione = :al_numero and
					cod_nota = :as_cod_nota
			using itran_note;
			//------------------------------------------------------------------
			
		case "TESORDACQ"
			//------------------------------------------------------------------
			updateblob tes_ord_acq_note
			set note_esterne = :ab_note_esterne
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno and
					num_registrazione = :al_numero and
					cod_nota = :as_cod_nota
			using itran_note;
			//------------------------------------------------------------------
			
	end choose
	
	if itran_note.sqlcode <> 0 then
		as_errore = "Errore nell'inserimento del documento digitale.~r~n" + itran_note.sqlerrtext
		return false
	end if
	
else
	if itran_note.sqlcode <> 0 then
		as_errore = "Errore nell'inserimento del documento digitale.~r~n" + itran_note.sqlerrtext
		return false
	end if
end if

return true
end function

public function boolean uof_inserisci_post_it (long al_row, datawindow adw_data, ref string as_errore);string				ls_sql, ls_valore, ls_codice, ls_errore, ls_cont, ls_cod_nota
long				ll_row, ll_numero, ll_prog_riga, ll_cont, ll_riga_main
integer			li_anno, li_cont
datastore		lds_data




ll_riga_main = idw_data.getrow()
if isnull(ll_riga_main) or ll_riga_main < 1 then
	as_errore = "Non è stata inserita alcuna riga principale oppure è necessario selezionare una riga principale!"
	return false
end if

li_anno = idw_data.getitemnumber(ll_riga_main, is_colonne_pk[1])
ll_numero = idw_data.getitemnumber(ll_riga_main, is_colonne_pk[2])

ls_sql = "select cod_azienda from " + is_tabella_padre + " " + &
			"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " +&
						is_colonne_pk[1] + " = " + string(li_anno) + " and " + &
						is_colonne_pk[2] + " = " + string(ll_numero) + " "

if not ib_testata_docs then
	ll_prog_riga = idw_data.getitemnumber(ll_row, is_colonne_pk[3])
	ls_sql += " and " + is_colonne_pk[3] + " = " + string(ll_prog_riga)
end if

li_cont = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)
destroy lds_data

if li_cont>0 then
else
	as_errore = "Prima di associare documenti è necessario salvare il dettaglio!"
	return false
end if



//creo la transazione (di istanza) che sarà usata per il salvataggio dei documenti
if not guo_functions.uof_create_transaction_from( sqlca, itran_note, ls_errore)  then
	as_errore = "Errore in creazione transazione: " + ls_errore
	return false
end if


ll_row = idw_data.getrow()

//RECUPERO MAX COD_NOTA ------------------------------------------------------------------------------------
//di sicuro una riga selezionata c'è (controllato da ue_start_drop)
li_anno = idw_data.getitemnumber(ll_row, is_colonne_pk[1])
ll_numero = idw_data.getitemnumber(ll_row, is_colonne_pk[2])

ls_sql = "select max(cod_nota) from " + is_tabella_gestione + " " + &
			"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " +&
						is_colonne_pk[1] + " = " + string(li_anno) + " and " + &
						is_colonne_pk[2] + " = " + string(ll_numero) + " and " + &
						"isnumeric(cod_nota)=1"

if not ib_testata_docs then
	ll_prog_riga = idw_data.getitemnumber(ll_row, is_colonne_pk[3])
	ls_sql += " and " + is_colonne_pk[3] + " = " + string(ll_prog_riga) + " "
else
	ll_prog_riga = 0
end if

ll_cont = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)

if ll_cont< 0 then
	destroy lds_data
	as_errore = "Errore creazione datastore conteggio: " + ls_errore
	
	if isvalid(itran_note) then
		rollback using itran_note;
		disconnect using itran_note;
		destroy itran_note;
	end if
	
	return false
	
elseif ll_cont = 0 or isnull(ll_cont) then
	ll_cont = 1
	
else
	ls_cont = lds_data.getitemstring(1, 1)
	if isnull(ls_cont) or ls_cont="" then ls_cont = "0"
	ll_cont = long(ls_cont)
	ll_cont += 1
	
end if

destroy lds_data

ls_cod_nota = string(ll_cont, "000")


//LEGGO I VALORI DEL POST-IT DA INSERIRE ---------------------------------------------------------------------------
//ls_codice = adw_data.getitemstring(al_row, "cod_post_it")
ls_valore = adw_data.getitemstring(al_row, "des_post_it")

//qui apri la finestra di conferma/modifica contenuto post-it
uof_conferma_post_it(ls_valore)

ls_codice = left(ls_valore, 40)
ls_valore = g_str.replace(ls_valore, "'", "''")

//qua glieli tolgo tutti
ls_codice = g_str.replace(ls_codice, "'", "")


//PREDISPONGO INSERIMENTO --------------------------------------------------------------------------------------------
ls_sql = "insert into " + is_tabella_gestione + " "+&  
				"( 	   cod_azienda,"+&
					  is_colonne_pk[1]+","+&
					  is_colonne_pk[2]+","+&
					  is_colonne_pk[3]+","
					  
if not ib_testata_docs then
	ls_sql += is_colonne_pk[4]+","
end if
					
ls_sql +=			  "des_nota,"+&
					  "note,"+&
					  "prog_mimetype,"+&
					  "flag_postit) "+&  
		"values ('" + s_cs_xx.cod_azienda + "', "+&
				  string(li_anno)+","+&
				  string(ll_numero)+","
				  
if not ib_testata_docs then				  
	ls_sql +=  string(ll_prog_riga)+","
end if

//lascio il prog_mimetype nullo in quanto non si tratta di file ...
ls_sql += 	  "'"+ls_cod_nota+"',"+&
				  "'"+ls_codice+"',"+&
				  "'"+ls_valore+"',"+&
				  "NULL,"+&
				  "'S')"

execute immediate :ls_sql using itran_note;

if itran_note.sqlcode <> 0 then
	as_errore = "Errore nell'inserimento del post-it:" + itran_note.sqlerrtext
	
	if isvalid(itran_note) then
		rollback using itran_note;
		disconnect using itran_note;
		destroy itran_note;
	end if
	
	return false
end if

//se arrivi fin qui fai commit
commit using itran_note;
disconnect using itran_note;
destroy itran_note;

return true

end function

public function integer uof_modifica_post_it (integer ai_anno, long al_numero, string as_cod_nota, string as_note, ref string as_errore);string			ls_des_nota
long			ll_row
integer		li_ret


//qui apri la finestra di conferma/modifica contenuto post-it
li_ret = uof_conferma_post_it(as_note)

if li_ret=1 then return 0

//creo la transazione (di istanza) che sarà usata per il salvataggio
if not guo_functions.uof_create_transaction_from( sqlca, itran_note, as_errore)  then
	as_errore = "Errore in creazione transazione: " + as_errore
	return -1
end if

ls_des_nota = left(as_note, 40)

choose case UPPER(is_gestione)
		
	case "TESORDVEN"
		update tes_ord_ven_note  
		set	     des_nota = :ls_des_nota,   
					note = :as_note,   
					prog_mimetype = null,   
					flag_postit = 'S'   
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ai_anno and
					num_registrazione=:al_numero and
					cod_nota=:as_cod_nota
		using itran_note;

	case "TESORDACQ"
		update tes_ord_avq_note  
		set	     des_nota = :ls_des_nota,   
					note = :as_note,   
					prog_mimetype = null,   
					flag_postit = 'S'   
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ai_anno and
					num_registrazione=:al_numero and
					cod_nota=:as_cod_nota
		using itran_note;
		
end choose

if itran_note.sqlcode<0 then
	as_errore = "Errore in aggiornamento testo Post-it: " + itran_note.sqlerrtext
	rollback using itran_note;
	disconnect using itran_note;
	destroy itran_note;
	return -1
end if


//se arrivi qui commit
commit using itran_note;
disconnect using itran_note;
destroy itran_note;

//quindi fai il refresh della dw
ll_row = idw_data.getrow()
uof_retrieve_blob(ll_row)

return 0
end function

public function integer uof_conferma_post_it (ref string as_valore);string				ls_hold


//imposta il limit a 255
s_cs_xx.parametri.parametro_d_10 = 9998

//ridimensiona la finestrella e togli la proprietà ACCEPT al tasto OK
s_cs_xx.parametri.parametro_d_11 = 6666

ls_hold = as_valore
openwithparm(w_inserisci_altro_valore, as_valore)

as_valore = message.stringparm

if as_valore="cs_team_esc_pressed" then
	as_valore = ls_hold
	return 1
else
	
	if as_valore="" or isnull(as_valore) then
		//non puoi pulire il contenuto del post-it
		as_valore = ls_hold
	end if
	
	//in as_valore il nuovo valore
	return 0
end if
end function

public function boolean wf_controlla_operatore (ref string as_messaggio);//long				ll_riga_main, ll_numero
//integer			li_anno
//string				ls_cod_utente
//
//
//as_messaggio = ""
//
//if UPPER(is_gestione) = "TESORDVEN" and ib_testata_docs then
//
//	ll_riga_main = idw_data.getrow()
//
//	if ll_riga_main>0 then
//
//		li_anno = idw_data.getitemnumber(ll_riga_main, is_colonne_pk[1])
//		ll_numero = idw_data.getitemnumber(ll_riga_main, is_colonne_pk[2])
//
//		select tab_operatori_utenti.cod_utente
//		into :ls_cod_utente
//		from tes_ord_ven
//		join tab_operatori_utenti on 	tab_operatori_utenti.cod_azienda=tes_ord_ven.cod_azienda and
//												tab_operatori_utenti.cod_operatore=tes_ord_ven.cod_operatore
//		where 	tes_ord_ven.cod_azienda=:s_cs_xx.cod_azienda and
//					tes_ord_ven.anno_registrazione=:li_anno and
//					tes_ord_ven.num_registrazione=:ll_numero;
//
//		if ls_cod_utente=s_cs_xx.cod_utente or ls_cod_utente="" or isnull(ls_cod_utente) then
//			return true
//		else
//			as_messaggio = "Impossibile proseguire. L'utente associato all'operatore ("+ls_cod_utente+") dell'ordine "+&
//									"è diverso dall'utente collegato ("+s_cs_xx.cod_utente+")!"
//			
//			return false
//		end if
//
//	end if
//
//end if

string				ls_flag_supervisore

if s_cs_xx.cod_utente<>"CS_SYSTEM" then
	select flag_supervisore
	into :ls_flag_supervisore
	from utenti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
			
	if ls_flag_supervisore="S" then return true
else
	//sei CS_SYSTEM, puoi fare tutto
	return true
end if


return false
end function

public function integer wf_abilita_disabilita (integer ai_anno, long al_numero, long al_riga, string as_cod_nota, string as_flag_collegato, ref integer ai_disabilitato, ref string as_errore);string					ls_sql, ls_gestione, ls_tabella, ls_where

datastore			lds_data

integer				li_ret


//se si tratta di documento collegato cambia tabella e chiave primaria per il controllo

if as_flag_collegato="S" then
	ls_gestione = is_gestione_prec
else
	ls_gestione = is_gestione
end if


choose case UPPER(ls_gestione)
	case "ORDACQ"
		//---------------------------------------------------------------------
		ls_tabella = "det_ord_acq_note"
		ls_where = "anno_registrazione="+string(ai_anno)+" and num_registrazione="+string(al_numero)+" and "+&
						"prog_riga_ordine_acq="+string(al_riga)+" and cod_nota='"+as_cod_nota+"' "
		
	//---------------------------------------------------------------------
	case "OFFVEN"
		ls_tabella = "det_off_ven_note"
		ls_where = "anno_registrazione="+string(ai_anno)+" and num_registrazione="+string(al_numero)+" and "+&
						"prog_riga_off_ven="+string(al_riga)+" and cod_nota='"+as_cod_nota+"' "
	
	//---------------------------------------------------------------------
	case "ORDVEN"
		ls_tabella = "det_ord_ven_note"
		ls_where = "anno_registrazione="+string(ai_anno)+" and num_registrazione="+string(al_numero)+" and "+&
						"prog_riga_ord_ven="+string(al_riga)+" and cod_nota='"+as_cod_nota+"' "
	
	//---------------------------------------------------------------------
	case "BOLVEN"
		ls_tabella = "det_bol_ven_note"
		ls_where = "anno_registrazione="+string(ai_anno)+" and num_registrazione="+string(al_numero)+" and "+&
						"prog_riga_bol_ven="+string(al_riga)+" and cod_nota='"+as_cod_nota+"' "
				
	//---------------------------------------------------------------------
	case "FATVEN"
		ls_tabella = "det_fat_ven_note"
		ls_where = "anno_registrazione="+string(ai_anno)+" and num_registrazione="+string(al_numero)+" and "+&
						"prog_riga_fat_ven="+string(al_riga)+" and cod_nota='"+as_cod_nota+"' "
	
	//---------------------------------------------------------------------
	case "TESORDVEN"
		ls_tabella = "tes_ord_ven_note"
		ls_where = "anno_registrazione="+string(ai_anno)+" and num_registrazione="+string(al_numero)+" and "+&
						"cod_nota='"+as_cod_nota+"' "

	//---------------------------------------------------------------------
	case "TESORDACQ"
		ls_tabella = "tes_ord_acq_note"
		ls_where = "anno_registrazione="+string(ai_anno)+" and num_registrazione="+string(al_numero)+" and "+&
						"cod_nota='"+as_cod_nota+"' "
	
end choose

ls_sql = 	"select disabilitato from " + ls_tabella + " " + &
			"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					ls_where

li_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)


if li_ret<0 then
	as_errore = "Errore in lettura documento dalla tabella (sqlcode -1): "+sqlca.sqlerrtext
	return -1
elseif li_ret = 0 then
	as_errore = "Documento non trovato! Potrebbe essere stato cancellato da un altro utente!"
	return -1
end if

ai_disabilitato = lds_data.getitemnumber(1, 1)
if isnull(ai_disabilitato) then ai_disabilitato = 0


as_errore=""

if ai_disabilitato=0 then
	ai_disabilitato = 1
else
	ai_disabilitato = 0
end if

return 0
end function

on uo_dw_drag_doc_acq_ven.create
call super::create
end on

on uo_dw_drag_doc_acq_ven.destroy
call super::destroy
end on

event buttonclicked;call super::buttonclicked;string							ls_messaggio, ls_flag_postit, ls_cod_nota, ls_des_nota,ls_errore, ls_collegato, ls_describe, ls_sql, ls_key, ls_test

long							ll_anno, ll_numero, ll_prog_riga, ll_count

transaction					sqlc_blob

integer						li_ret, li_disabilita

uo_log_sistema				luo_log




if isvalid(dwo) then
	
	choose case dwo.name
		case "b_anag_post_it"
			window_open(w_post_it, -1)
	
	
		case "b_disable"
			if row > 0 then
				//devi abilitare o disabilitare?
			
				ll_anno = getitemnumber(row, is_colonne_dw[1])
				ll_numero = getitemnumber(row, is_colonne_dw[2])
				
				if not ib_testata_docs then
					ll_prog_riga = getitemnumber(row, is_colonne_dw[3])
					ls_cod_nota = getitemstring(row, is_colonne_dw[4])
				else
					setnull(ll_prog_riga)
					ls_cod_nota = getitemstring(row, is_colonne_dw[3])
					ls_flag_postit = getitemstring(row, "flag_postit")
				end if
				
				ls_des_nota = getitemstring(row, "des_nota")
				
				//se collegato non puoi disabilitare
				ls_describe = this.Describe("collegato.visible")
				choose case ls_describe
					case "1","0"		//vuol dire che la colonna c'è
						ls_collegato = getitemstring(row, "collegato")
						
					case else	
						ls_collegato = "N"
				end choose
				
				//dovrebbe essere invisibile il pulsante, ma per sicurezza faccio questo
				if ls_collegato <> "N" then
					return
				end if
				
				if ls_flag_postit="S" then
					return
				else
					
					li_ret = wf_abilita_disabilita(ll_anno, ll_numero, ll_prog_riga, ls_cod_nota, ls_collegato, li_disabilita, ls_errore)
					if li_ret<0 then
						g_mb.error(ls_errore)
						return
					elseif li_ret>0 then
						g_mb.warning(ls_errore)
						return
					else
						if li_disabilita=1 then
							ls_messaggio = "Procedo con la disabilitazione del documento " + ls_cod_nota + " - " + ls_des_nota + "?"
						else
							ls_messaggio = "Procedo con la riabilitazione del documento " + ls_cod_nota + " - " + ls_des_nota + "?"
						end if
					end if
					
					//ls_messaggio = "Procedo con la disabilitazione del documento " + ls_cod_nota + " - " + ls_des_nota + "?"
				end if
				
				//se hai richiesto di ri-abilitare e non sei supervisore non permetterlo
				if li_disabilita=1 then
				else
					if not wf_controlla_operatore(ls_test) then
						g_mb.warning("Non sei autorizzato ad effettuare questa operazione!")
						return
					end if
				end if
				
				
				if not g_mb.confirm(ls_messaggio) then return 
				
				// creo transazione separata
				if not guo_functions.uof_create_transaction_from( sqlca, sqlc_blob, ls_errore)  then
					g_mb.error("Errore in creazione transazione~r~n" + ls_errore)
					return
				end if
				
				ls_sql = 	"update " + is_tabella_gestione + " " + &
							"set disabilitato="+string(li_disabilita) + " " + &
							"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
										is_colonne_pk[1] + " = " + string(ll_anno) + " and " + &
										is_colonne_pk[2] + " = " + string(ll_numero) + " and "
				
				ls_key = is_colonne_pk[1] + " = " + string(ll_anno) + " and " + &
							is_colonne_pk[2] + " = " + string(ll_numero) + " and "
				
				if not ib_testata_docs then
					ls_sql += 	is_colonne_pk[3] + " = " + string(ll_prog_riga) + " and " + &
									is_colonne_pk[4] + " = '" + ls_cod_nota + "' "
									
					ls_key += is_colonne_pk[3] + " = " + string(ll_prog_riga) + " and " + &
									is_colonne_pk[4] + " = '" + ls_cod_nota + "' "
				else
					ls_sql += 	is_colonne_pk[3] + " = '" + ls_cod_nota + "' "
					
					ls_key += is_colonne_pk[3] + " = '" + ls_cod_nota + "' "
				end if
				
				
				execute immediate :ls_sql using sqlc_blob;
				
				if sqlc_blob.sqlcode <> 0 then
					
					//if ls_flag_postit="S" then
						//ls_messaggio = "Errore in cancellazione post-it:  " + sqlc_blob.sqlerrtext
					//else
						ls_messaggio = "Errore in abilitazione/disabilitazione documento: " + sqlc_blob.sqlerrtext
					//end if
					
					g_mb.error(ls_messaggio)
					rollback using sqlc_blob;
					disconnect using sqlc_blob;
					destroy sqlc_blob
				else
					commit using sqlc_blob;
					disconnect using sqlc_blob;
					destroy sqlc_blob
					
					//######################################################
					//scrivo in log sistema
					if li_disabilita = 1 then
						//hai disabilitato
						ls_messaggio = "DISABILITAZIONE documento"
					else
						//hai abilitato
						ls_messaggio = "ABILITAZIONE documento"
					end if
					
					ls_messaggio += " in tabella " + is_tabella_gestione + " ("+ls_key+")"
					
					luo_log = create uo_log_sistema
					luo_log.uof_write_log_sistema_not_sqlca("DOCS", ls_messaggio)
					destroy luo_log
					//######################################################
					
				end if		
				
				if not ib_testata_docs then
					ll_count = retrieve(s_cs_xx.cod_azienda, ll_anno, ll_numero, ll_prog_riga)
				else
					ll_count = retrieve(s_cs_xx.cod_azienda, ll_anno, ll_numero)
				end if
			end if


		case "b_del"
			//cancellazione definitiva (ma solo se abilitato, a me no che si tratta di post-it)
			if row > 0 then
				
				ll_anno = getitemnumber(row, is_colonne_dw[1])
				ll_numero = getitemnumber(row, is_colonne_dw[2])
				
				if not ib_testata_docs then
					ll_prog_riga = getitemnumber(row, is_colonne_dw[3])
					ls_cod_nota = getitemstring(row, is_colonne_dw[4])
				else
					setnull(ll_prog_riga)
					ls_cod_nota = getitemstring(row, is_colonne_dw[3])
					ls_flag_postit = getitemstring(row, "flag_postit")
				end if
				
				
				//se collegato non puoi disabilitare
				ls_describe = this.Describe("collegato.visible")
				choose case ls_describe
					case "1","0"		//vuol dire che la colonna c'è
						ls_collegato = getitemstring(row, "collegato")
						
					case else	
						ls_collegato = "N"
				end choose
				
				//dovrebbe essere invisibile il pulsante, ma per sicurezza faccio questo
				if ls_collegato <> "N" then
					return
				end if
				
				if ls_flag_postit="S" then
					//se si tratta di un post-it non controllare se abilitato o meno
				else
					//se è disabilitato non permettere di andare avanti
					li_ret = wf_abilita_disabilita(ll_anno, ll_numero, ll_prog_riga, ls_cod_nota, ls_collegato, li_disabilita, ls_errore)
					if li_ret<0 then
						g_mb.error(ls_errore)
						return
					elseif li_ret>0 then
						g_mb.warning(ls_errore)
						return
					else
						if li_disabilita=1 then
							//il documento è abilitato, prosegui
						else
							//il documento è DISABILITATO: impossibile proseguire
							g_mb.warning("Il documento "+ ls_cod_nota + " è disabilitato!")
							return
						end if
					end if
					
					
					//inoltre controlla se sei abilitato a cancellare (devi essere supervisore),
					//a meno che si tratta di post-it, in tal caso puoi cancellare
					if not wf_controlla_operatore(ls_messaggio) then
						//if ls_messaggio<>"" then g_mb.warning(ls_messaggio)
						g_mb.warning("Non sei autorizzato ad effettuare questa operazione!")
						return
					end if
				end if
				
				
				
				ls_des_nota = getitemstring(row, "des_nota")
				
				ls_describe = this.Describe("collegato.visible")
				choose case ls_describe
					case "1","0"		//vuol dire che la colonna c'è
						ls_collegato = getitemstring(row, "collegato")
						
					case else	
						ls_collegato = "N"
				end choose
				
				//dovrebbe essere invisibile il pulsante, ma per sicurezza faccio questo
				if ls_collegato <> "N" then
					return
				end if
				
				if ls_flag_postit="S" then
					ls_messaggio = "Procedo con la eliminazione del post-it " + ls_cod_nota + " - " + left(ls_des_nota, 10) + "... " + "?"
				else
					ls_messaggio = "Procedo con la cancellazione del documento " + ls_cod_nota + " - " + ls_des_nota + "?"
				end if
				
				if not g_mb.confirm(ls_messaggio) then return 
					
				// creo transazione separata
				if not guo_functions.uof_create_transaction_from( sqlca, sqlc_blob, ls_errore)  then
					g_mb.error("Errore in creazione transazione~r~n" + ls_errore)
					return
				end if
				
				ls_sql = 	"delete from " + is_tabella_gestione + " " + &
							"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
										is_colonne_pk[1] + " = " + string(ll_anno) + " and " + &
										is_colonne_pk[2] + " = " + string(ll_numero) + " and "
				
				if not ib_testata_docs then
					ls_sql += 	is_colonne_pk[3] + " = " + string(ll_prog_riga) + " and " + &
									is_colonne_pk[4] + " = '" + ls_cod_nota + "' "
				else
					ls_sql += 	is_colonne_pk[3] + " = '" + ls_cod_nota + "' "
				end if
				
				execute immediate :ls_sql using sqlc_blob;
				
				if sqlc_blob.sqlcode <> 0 then
					
					if ls_flag_postit="S" then
						ls_messaggio = "Errore in cancellazione post-it:  " + sqlc_blob.sqlerrtext
					else
						ls_messaggio = "Errore in cancellazione documento: " + sqlc_blob.sqlerrtext
					end if
					
					g_mb.error(ls_messaggio)
					rollback using sqlc_blob;
					disconnect using sqlc_blob;
					destroy sqlc_blob
				else
					commit using sqlc_blob;
					disconnect using sqlc_blob;
					destroy sqlc_blob
				end if		
				
				if not ib_testata_docs then
					ll_count = retrieve(s_cs_xx.cod_azienda, ll_anno, ll_numero, ll_prog_riga)
				else
					ll_count = retrieve(s_cs_xx.cod_azienda, ll_anno, ll_numero)
				end if
				
			end if
			
	end choose
end if
end event

event doubleclicked;call super::doubleclicked;string						ls_temp_dir, ls_rnd,ls_estensione,ls_file_name, ls_cod_nota, ls_collegato, ls_temp, ls_tabella_prec, &
							ls_temp1[], ls_pk_prec[], ls_errore, ls_describe, ls_flag_postit, ls_note
							
long						ll_anno, ll_numero, ll_prog_riga, ll_cont, ll_prog_mimetype, ll_len
blob						lb_blob
uo_shellexecute 		luo_run
integer					li_ret, li_disabilita

if row > 0 then
	
	ll_anno = getitemnumber(row, is_colonne_dw[1])
	ll_numero = getitemnumber(row, is_colonne_dw[2])
	
	if not ib_testata_docs then
		ll_prog_riga = getitemnumber(row, is_colonne_dw[3])
		ls_cod_nota = getitemstring(row, is_colonne_dw[4])
	else
		//gestione docs su tabella testata
		ls_cod_nota = getitemstring(row, is_colonne_dw[3])
		
		ls_flag_postit = getitemstring(row, "flag_postit")
		
		if ls_flag_postit ="N" then
		else
			//gestione modifica contenuto post-it
			ls_note = getitemstring(row, "note")
			
			//commit o rollback fatto all'interno della seguente funzione
			if uof_modifica_post_it(ll_anno, ll_numero, ls_cod_nota, ls_note, ls_errore) < 0 then
				g_mb.error(ls_errore)
				return
			end if
			
			//esci dall'evento ...
			return
		end if
	end if
	
	
	ls_describe = this.Describe("collegato.visible")
	choose case ls_describe
		case "1","0"
			ls_collegato = getitemstring(row, "collegato")
			
		case else
			ls_collegato = "N"
	end choose
	

	//se è disabilitato non permettere di visualizzare il documento
	li_ret = wf_abilita_disabilita(ll_anno, ll_numero, ll_prog_riga, ls_cod_nota, ls_collegato, li_disabilita, ls_errore)
	if li_ret<0 then
		g_mb.error(ls_errore)
		return
	elseif li_ret>0 then
		g_mb.warning(ls_errore)
		return
	else
		if li_disabilita=1 then
			//il documento è abilitato, prosegui
		else
			//il documento è DISABILITATO: impossibile proseguire
			g_mb.warning("Il documento "+ ls_cod_nota + " è disabilitato!")
			return
		end if
	end if

	ll_prog_mimetype = getitemnumber(row, "prog_mimetype")
	
	li_ret = uof_get_blob(ls_collegato, ll_anno, ll_numero, ll_prog_riga, ls_cod_nota, lb_blob, ls_errore)
	
	if li_ret < 0 then
		g_mb.error(ls_errore)
		return
	end if
	
	ll_len = lenA(lb_blob)
	
	select estensione
	into :ls_estensione
	from tab_mimetype
	where cod_azienda = :s_cs_xx.cod_azienda and
			prog_mimetype = :ll_prog_mimetype;
	
	ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
	
	ls_rnd = string( hour(now()),"00") +"_" + string( minute(now()),"00") +"_" + string( second(now()),"00") 
	ls_file_name =  ls_temp_dir + ls_rnd + "." + ls_estensione
	guo_functions.uof_blob_to_file( lb_blob, ls_file_name)
	
	
	luo_run = create uo_shellexecute
	luo_run.uof_run( handle(parent), ls_file_name )
	destroy(luo_run)
		
end if
end event

event ue_drop_file;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

integer			li_anno
long				ll_numero, ll_prog_riga, ll_row, ll_cont, ll_len, ll_prog_mimetype, ll_maxKB
string				ls_cod_nota, ls_dir, ls_file, ls_ext,ls_errore, ls_sql, ls_cont
blob				lb_note_esterne
datastore		lds_data


ll_row = idw_data.getrow()

//di sicuro una riga selezionata c'è (controllato da ue_start_drop)
li_anno = idw_data.getitemnumber(ll_row, is_colonne_pk[1])
ll_numero = idw_data.getitemnumber(ll_row, is_colonne_pk[2])

ls_sql = "select max(cod_nota) from " + is_tabella_gestione + " " + &
			"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " +&
						is_colonne_pk[1] + " = " + string(li_anno) + " and " + &
						is_colonne_pk[2] + " = " + string(ll_numero) + " and " + &
						"isnumeric(cod_nota)=1"

if not ib_testata_docs then
	ll_prog_riga = idw_data.getitemnumber(ll_row, is_colonne_pk[3])
	ls_sql += " and " + is_colonne_pk[3] + " = " + string(ll_prog_riga) + " "
else
	ll_prog_riga = 0
end if


ll_cont = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)

if ll_cont< 0 then
	destroy lds_data
	as_message = "Errore creazione datastore conteggio: " + ls_errore
	return false
	
elseif ll_cont = 0 or isnull(ll_cont) then
	ll_cont = 1
	
else
	ls_cont = lds_data.getitemstring(1, 1)
	if isnull(ls_cont) or ls_cont="" then ls_cont = "0"
	ll_cont = long(ls_cont)
	ll_cont += 1
	
end if

destroy lds_data

ls_cod_nota = string(ll_cont, "000")
guo_functions.uof_file_to_blob(as_filename, lb_note_esterne)

//lunghezza in BYTE del documento trascinato
ll_len = lenA(lb_note_esterne)


guo_functions.uof_get_parametro("MLD", ll_maxKB)
if not isnull(ll_maxKB) and ll_maxKB>0 then
	if ll_len > ll_maxKB * 1024 then
		//documento oltre la grandezza massima pre-stabilita
		as_message = "Attenzione: grandezza file (" + string(ll_len) + " BYTE) superiore a quella prestabilita ("+string(ll_maxKB * 1024)+" KB) come da parametro multiazienda MLD"
		return false
	end if
end if

guo_functions.uof_get_file_info( as_filename, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;
if sqlca.sqlcode <> 0 then
	as_message = "Attenzione: estensione file " + ls_ext + " non prevista da sistema."
	return false
end if

return uof_inserisci_documento(li_anno, ll_numero, ll_prog_riga, ls_cod_nota, ls_file, ll_prog_mimetype, lb_note_esterne, as_message)
end event

event ue_end_drop;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

long					ll_row

if ab_status then
	//commit
	commit using itran_note;
	disconnect using itran_note;
	destroy itran_note;
else
	//rollback
	if isvalid(itran_note) then
		rollback using itran_note;
		disconnect using itran_note;
		destroy itran_note;
	end if
	
	if as_message<>"" and not isnull(as_message) then g_mb.error(as_message)
	
end if

ll_row = idw_data.getrow()

uof_retrieve_blob(ll_row)

return true
end event

event ue_start_drop;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

integer		li_anno, li_cont
long			ll_row, ll_numero, ll_prog_riga, ll_ret
string			ls_errore, ls_sql, ls_databasedocs, ls_servernamedocs
datastore	lds_data

ll_row = idw_data.getrow()


if isnull(ll_row) or ll_row < 1 then
	as_message = "Non è stata inserita alcuna riga oppure è necessario selezionare una riga nella lista!"
	ab_return = false
	return
end if

li_anno = idw_data.getitemnumber(ll_row, is_colonne_pk[1])
ll_numero = idw_data.getitemnumber(ll_row, is_colonne_pk[2])


ls_sql = "select cod_azienda from " + is_tabella_padre + " " + &
			"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " +&
						is_colonne_pk[1] + " = " + string(li_anno) + " and " + &
						is_colonne_pk[2] + " = " + string(ll_numero) + " "

if not ib_testata_docs then
	ll_prog_riga = idw_data.getitemnumber(ll_row, is_colonne_pk[3])
	ls_sql += " and " + is_colonne_pk[3] + " = " + string(ll_prog_riga)
end if

li_cont = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)
destroy lds_data

if li_cont>0 then
else
	as_message = "Prima di associare documenti è necessario salvare il dettaglio!"
	ab_return = false
	return
end if

//creo la transazione (di istanza) che sarà usata per il salvataggio dei documenti

ls_databasedocs = ""
ls_servernamedocs = ""
Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "databasedocs",ls_databasedocs)
Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "servernamedocs",ls_servernamedocs)


if (is_gestione = "TESORDVEN" or is_gestione = "ORDVEN") and not isnull(ls_servernamedocs) and len(ls_servernamedocs) > 0 and not isnull(ls_databasedocs) and len(ls_databasedocs) > 0 then
	if not guo_functions.uof_create_transaction_from( sqlca, itran_note, ls_servernamedocs, ls_databasedocs, ls_errore)  then
		as_message = "Errore in creazione transazione~r~n" + ls_errore
		ab_return = false
		return
	end if
else
	if not guo_functions.uof_create_transaction_from( sqlca, itran_note, ls_errore)  then
		as_message = "Errore in creazione transazione~r~n" + ls_errore
		ab_return = false
		return
	end if
end if
ab_return = true
return
end event

event ue_drop_dw_data;//*********************************************************
//ANCESTOR SCRIPT DISATTIVATO
//********************************************************


datawindow			ldw_data
boolean				lb_ret
long					ll_row
	
	
choose case aobj_drag.classname()
		//-------------------------------------------------------------------------------
		case "dw_post_it"
			//datacontrol su finestra anagrafica post-it (w_post_it)
			ldw_data = aobj_drag
			
			if ldw_data.dataobject = "d_post_it" then
				ll_row = ldw_data.getrow()
				//ELENCO CAMPI DATAWINDOW PROVENIENZA
				//	1. cod_azienda
				//	2. cod_post_it
				//	3. des_post_it
				if ll_row > 0 then
					//inserisci post-it, previa approvazione (nella funzione successiva viene creata la transazione)
					lb_ret = uof_inserisci_post_it(ll_row, ldw_data, as_message)
					
					if lb_ret then
						//commit già fatto e transazione già distrutta
					else
						//rollback già fatto e transazione già distrutta
						if as_message<>"" and not isnull(as_message) then g_mb.error(as_message)
					end if
					
					ll_row = idw_data.getrow()
					uof_retrieve_blob(ll_row)
					
				end if
			end if
			
	end choose

return true
end event

