﻿$PBExportHeader$uo_dw_grid.sru
forward
global type uo_dw_grid from datawindow
end type
end forward

global type uo_dw_grid from datawindow
integer width = 686
integer height = 400
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
event ue_left_mouse_clicked pbm_dwnlbuttonclk
event ue_left_mouse_clicked_up pbm_dwnlbuttonup
event ue_column_resize_end ( )
event ue_check_column_width_changed ( )
end type
global uo_dw_grid uo_dw_grid

type variables
CONSTANT int MODE_HEADER_EDITABLE = 1
CONSTANT int MODE_VALUE_EDITABLE = 2
CONSTANT int MODE_VIEW_ONLY = 3

CONSTANT int ALIGN_LEFT = 0
CONSTANT int ALIGN_RIGHT = 1
CONSTANT int ALIGN_CENTER = 2

private:
	int ii_num_rows, ii_num_cols
	string is_error
	int ii_mode = 2
	
	int ii_column_height = 80
	
	// colori
	long il_header_color = 12632256
	
	long il_row_start_clicked, il_row_end_clicked
	long il_column_start_clicked, il_column_end_clicked
	
	long il_default_text_align = 0
	
	// stefanop: 15/07/2014: indico se la dw ha fatto il render delle intestazioni
	boolean ib_col_header = true
	boolean ib_row_header = true
	
	// stefanop: 22/09/2014: sto ridimensionando una colonna?
	boolean ib_column_resize = false
	
	long il_cache_column_width[]
	
public:
	int ii_column_width = 300
end variables

forward prototypes
private subroutine uof_create_dw ()
private function string uof_create_db_column (string as_name, string as_type, integer ai_limit)
public subroutine ouf_draw ()
public function integer uof_get_rowcount ()
public function integer uof_get_columncount ()
public function integer setitem (long r, readonly integer c, any v)
public function string getitemstring (long r, readonly integer c)
private subroutine wf_create_column_syntax (ref string as_columns)
public subroutine uof_init (integer ai_mode, integer ai_rows, integer ai_cols)
public function long uof_get_col_number (string as_dwo_name)
public function integer uof_add_columns (integer ai_num_columns, boolean ab_trigger_refresh)
public function integer uof_add_rows (integer ai_num_rows, boolean ab_trigger_refresh)
public function integer uof_delete_rows (integer ai_num_rows, boolean ab_trigger_refresh)
public function integer uof_delete_columns (integer ai_num_columns, boolean ab_trigger_refresh)
public function integer uof_get_selected_row (ref integer ai_rows[])
public function string uof_set_column_width (integer ai_column_number, integer ai_column_width)
public subroutine uof_get_selected_column (ref integer ai_columns[])
public subroutine uof_set_default_text_align (integer ai_text_align)
public subroutine uof_set_column_text_align (integer ai_column, integer ai_text_align)
public function integer uof_set_default_column_width (integer ai_column_width)
public function integer uof_get_default_column_width ()
public subroutine uof_clearallrows ()
public subroutine uof_draw ()
public subroutine uof_init (integer ai_mode, integer ai_rows, integer ai_cols, boolean ab_row_header, boolean ab_col_header)
public subroutine uof_init_only_header (integer ai_cols)
private subroutine uof_create_dw (boolean ab_header)
private function string uof_text_header_column (string as_name, string as_type, integer ai_limit)
private subroutine wf_create_text_syntax (ref string as_columns)
public function long uof_get_column_width (integer ai_column_number)
public function integer uof_get_column_clicked ()
public subroutine uof_deserialize_column_width (long al_columns[])
public subroutine uof_serialize_column_width (ref long al_columns[])
public subroutine uof_sync_column_width (uo_dw_grid adw_grid)
end prototypes

event ue_left_mouse_clicked;il_row_start_clicked = row -1
il_column_start_clicked = uof_get_col_number(string(dwo.name))

if row = 0 and dwo.name = "datawindow" then
	ib_column_resize = true
	uof_serialize_column_width(il_cache_column_width)
end if

end event

event ue_left_mouse_clicked_up;int li_i
long ll_column_width[]

il_row_end_clicked = row - 1
il_column_end_clicked = uof_get_col_number(string(dwo.name))

if ib_column_resize then
	ib_column_resize = false
	
	event post ue_check_column_width_changed()	
end if
end event

event ue_column_resize_end();/**
 * stefanop
 * 22/09/2014
 *
 * L'evento scatta quando l'utente ha terminato di ridimensionare la colonna
 **/
end event

event ue_check_column_width_changed();/**
 * stefanop
 * 22/09/2014
 *
 * Controllo prima di eseguire l'evento ue_column_resize_end
 *
 **/
 
int li_i
long ll_column_width[]
 
uof_serialize_column_width(ll_column_width)
	
for li_i = 1 to upperbound(ll_column_width)
	if ll_column_width[li_i] <> il_cache_column_width[li_i] then
		// fire event
		event post ue_column_resize_end()
		exit
	end if
next	
end event

private subroutine uof_create_dw ();/**
 * stefanop
 * 27/02/2012
 *
 * Creo la sintassi dinamica per creare la DW
 **/
 
uof_create_dw(false)
end subroutine

private function string uof_create_db_column (string as_name, string as_type, integer ai_limit);/**
 * stefanop
 * 27/02/2012
 *
 * La funzione crea la sintassi per una singola colonna e ne ritorna la sintassi corretta
 * da inserire nella DW
 **/
 
string ls_syntax

ls_syntax = " column=(type="

choose case as_type
		
	case "string"
		ls_syntax += "char(" + string(ai_limit) + ")"
		
	// TODO: Aggiungere altri formati
		
end choose
 
ls_syntax += " updatewhereclause=no name=" + as_name +" dbname=~"" + as_name + "~") "

return ls_syntax
end function

public subroutine ouf_draw ();/* DEPRECATO */
uof_create_dw()
end subroutine

public function integer uof_get_rowcount ();/**
 * stefanop
 * 27/02/2012
 *
 * Ritorno il numero di righe della DW.
 * Prima controllo il parametro di istanza per vedere se è stata creata con un numero prefissato di righe,
 * se < 1 allora controllo il rowcount() della DW in quanto sono in fase di creazione.
 **/
 
if ii_num_rows < 1 then
	return rowcount()
else
	return ii_num_rows
end if
end function

public function integer uof_get_columncount ();/**
 * stefanop
 * 27/02/2012
 **/
 
return ii_num_cols
end function

public function integer setitem (long r, readonly integer c, any v);/**
 * stefanop
 * 27/02/2012
 *
 * Imposto il valore nella DW
 **/
 
long ll_row, ll_col
setnull(is_error)
 
 if r > uof_get_rowcount() then
	is_error = "Il numero di riga impostato eccedere il limite massimo."
	return -1
end if

if c > uof_get_columncount() then
	is_error = "Il numero di colonna impostato eccedere il limite massimo."
	return -1
end if

ll_row = r
ll_col = c

if ib_col_header then ll_row++
if ib_row_header then ll_col++
 
return super::setitem(ll_row, ll_col, v)
end function

public function string getitemstring (long r, readonly integer c);/**
 * stefanop
 * 27/02/2012
 **/
 
string ls_empty
long ll_row, ll_col

setnull(ls_empty)
setnull(is_error)
 
if r > uof_get_rowcount() then
	is_error = "Il numero di riga impostato eccedere il limite massimo."
	return ls_empty
end if

if c > uof_get_columncount() then
	is_error = "Il numero di colonna impostato eccedere il limite massimo."
	return ls_empty
end if

ll_row = r
ll_col = c

if ib_col_header then ll_row++
if ib_row_header then ll_col++

return super::getitemstring(ll_row, ll_col)
end function

private subroutine wf_create_column_syntax (ref string as_columns);string ls_text
int li_i, li_x, li_num_cols

li_num_cols = ii_num_cols
if ib_row_header then li_num_cols++

for li_i = 1 to li_num_cols
	
	li_x = (li_i - 1) * ii_column_width
	
	as_columns += "column(band=detail id=" + string(li_i) + " alignment=~"" + string(il_default_text_align) + "~" tabsequence=10 color=~"33554432~" border=~"0~" "
	as_columns += "x=~"" + string(li_x) + "~" y=~"0~" height=~"" + string(ii_column_height - 5) + "~" width=~"" + string(ii_column_width) + "~" format=~"[general]~" html.valueishtml=~"0~" name=col_" + string(li_i) + " visible=~"1~" "
	as_columns += "edit.limit=0 edit.case=any edit.focusrectangle=no edit.autoselect=yes edit.autohscroll=yes  "
	
	if ii_mode = MODE_VALUE_EDITABLE then
		if ib_col_header then
			if li_i > 1 then
				as_columns += " protect='1~tIf(getrow() > 1, 0, 1)' "
			else
				as_columns += " protect='1' "
			end if
		else
			as_columns += " protect='1~tIf(getrow() > 1, 0, 1)' "
		end if
	elseif ii_mode = MODE_HEADER_EDITABLE then
		// Solo le colonne > 1 devono avere i campi bloccati, la prima colonna rappresenta un HEADER
		if li_i  > 1 then as_columns += " protect='1~tIf(getrow() = 1, 0, 1)' "
	elseif ii_mode = MODE_VIEW_ONLY then
		as_columns += " protect='1' "
	end if
	
	as_columns += "font.face=~"Tahoma~" font.height=~"-9~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"2~" "
	
	if li_i > 1 then
		as_columns += "background.color=~"536870912~" "
	elseif ib_col_header then
		as_columns += "background.color=~"" + string(il_header_color) + "~" "
	else
		as_columns += "background.color=~"536870912~" "
	end if
	
	as_columns += " background.transparency=~"0~" background.gradient.transparency=~"0~" background.gradient.angle=~"0~" background.brushmode=~"0~" background.gradient.repetition.mode=~"0~" background.gradient.repetition.count=~"0~" background.gradient.repetition.length=~"100~" background.gradient.focus=~"0~" background.gradient.scale=~"100~" background.gradient.spread=~"100~" tooltip.backcolor=~"134217752~" tooltip.delay.initial=~"0~" tooltip.delay.visible=~"32000~" tooltip.enabled=~"0~" tooltip.hasclosebutton=~"0~" tooltip.icon=~"0~" tooltip.isbubble=~"0~" tooltip.maxwidth=~"0~" tooltip.textcolor=~"134217751~" tooltip.transparency=~"0~" transparency=~"0~" ) "

	
next
end subroutine

public subroutine uof_init (integer ai_mode, integer ai_rows, integer ai_cols);/**
 * stefanop
 * 27/02/2012
 *
 * Inizializzo la DW
 **/
 
uof_init(ai_mode, ai_rows, ai_cols, true, true)
end subroutine

public function long uof_get_col_number (string as_dwo_name);/**
 * stefanop
 * 10/05/2012
 *
 * Recupera il numero di colonna a partire da una stringa, solitamente il dwo.name
 *
 * Ritorna: il numero della colonna o -1 in caso di errore
 **/
 
if len(as_dwo_name) < 5 or left(as_dwo_name, 4) <> "col_" then return -1

return long(mid(as_dwo_name, 5))
end function

public function integer uof_add_columns (integer ai_num_columns, boolean ab_trigger_refresh);/**
 * stefanop
 * 11/05/2012
 *
 * Aggiunge N colonne a quelle già presenti e se il parametro ab_trigger_refresh è a true allora scatena
 * il refresh della DW.
 * !! ATTENZIONE !! il refresh provoca la perdita di tutti i dati inseriti
 **/
 
this.ii_num_cols += ai_num_columns

if ab_trigger_refresh then
	ouf_draw()
end if

return this.ii_num_cols
end function

public function integer uof_add_rows (integer ai_num_rows, boolean ab_trigger_refresh);/**
 * stefanop
 * 11/05/2012
 *
 * Aggiunge N righe a quelle già presenti e se il parametro ab_trigger_refresh è a true allora scatena
 * il refresh della DW.
 * !! ATTENZIONE !! il refresh provoca la perdita di tutti i dati inseriti
 **/
 
long li_i

this.ii_num_rows += ai_num_rows

if ab_trigger_refresh then
	ouf_draw()
	
	for li_i = 1 to ai_num_rows
		insertrow(0)
	next
	
end if

return this.ii_num_rows
end function

public function integer uof_delete_rows (integer ai_num_rows, boolean ab_trigger_refresh);/**
 * stefanop
 * 11/05/2012
 *
 * Rimuove N righe a quelle già presenti e se il parametro ab_trigger_refresh è a true allora scatena
 * il refresh della DW.
 * !! ATTENZIONE !! il refresh provoca la perdita di tutti i dati inseriti
 **/
 
if ai_num_rows > this.ii_num_rows then
	this.ii_num_rows = 0
else
 	this.ii_num_rows -= ai_num_rows
end if

if ab_trigger_refresh then
	ouf_draw()
end if

return this.ii_num_rows
end function

public function integer uof_delete_columns (integer ai_num_columns, boolean ab_trigger_refresh);/**
 * stefanop
 * 11/05/2012
 *
 * Rimuove N colonne a quelle già presenti e se il parametro ab_trigger_refresh è a true allora scatena
 * il refresh della DW.
 * !! ATTENZIONE !! il refresh provoca la perdita di tutti i dati inseriti
 **/
 
if ai_num_columns > this.ii_num_cols then
	this.ii_num_cols = 0
else
	this.ii_num_cols -= ai_num_columns
end if

if ab_trigger_refresh then
	ouf_draw()
end if

return this.ii_num_cols
end function

public function integer uof_get_selected_row (ref integer ai_rows[]);/**
 **/
int li_min, li_max, li_i, li_row

if il_row_start_clicked = il_row_end_clicked then
	ai_rows[1] = il_row_start_clicked
	
else

	li_min = min( il_row_start_clicked, il_row_end_clicked)
	li_max = max( il_row_start_clicked, il_row_end_clicked)
	
	for li_i = 0 to (li_max - li_min)
		ai_rows[li_i + 1] = li_i + li_min		
	next

end if

return 1
end function

public function string uof_set_column_width (integer ai_column_number, integer ai_column_width);/**
 * stefanop
 * 14/05/2012
 *
 * Imposto la grandezza della colonna in base al parametro passato
 **/
 
return modify("#" + string(ai_column_number) + ".width=" + string(ai_column_width))
end function

public subroutine uof_get_selected_column (ref integer ai_columns[]);/**
 * stefanop
 * 14/05/2012
 *
 * Ritorno un array con i numeri di colonna selezionati
 **/

int li_min, li_max, li_i, li_col

if il_column_start_clicked = il_column_end_clicked then
	ai_columns[1] = il_column_start_clicked
	
else

	li_min = min( il_column_start_clicked, il_column_end_clicked)
	li_max = max( il_column_start_clicked, il_column_end_clicked)
	li_col = li_min
	
	for li_i = 0 to (li_max - li_min)
		ai_columns[li_i + 1] = li_i + li_min		
	next

end if
end subroutine

public subroutine uof_set_default_text_align (integer ai_text_align);/**
 * stefanop
 * 15/05/2012
 *
 * Imposta l'allineamento del testo nelle celle di default.
 * I valori da passare sono: dw_grid.ALIGN_LEFT, ALIGN_RIGHT o ALIGN_CENTER
 **/
 
if ai_text_align >= 0 and ai_text_align <= 2 then
	il_default_text_align = ai_text_align
else
	il_default_text_align = this.ALIGN_LEFT
end if
end subroutine

public subroutine uof_set_column_text_align (integer ai_column, integer ai_text_align);/**
 * stefanop
 * 15/05/2012
 *
 * Imposta l'allineamento del testo nelle celle di default.
 * I valori da passare sono: dw_grid.ALIGN_LEFT, ALIGN_RIGHT o ALIGN_CENTER
 **/

if ai_text_align >= 0 and ai_text_align <= 2 then
	il_default_text_align = ai_text_align
else
	il_default_text_align = this.ALIGN_LEFT
end if

if ai_column < 0 or ai_column > uof_get_columncount() then
	return
end if

is_error = modify("#" + string(ai_column) + ".align=" + string(ai_text_align))
end subroutine

public function integer uof_set_default_column_width (integer ai_column_width);/**
 * stefanop
 *
 * Imposoto la grandezza di default delle colonne
 **/

if ai_column_width < 0 then ai_column_width = 0

this.ii_column_width = ai_column_width

return ai_column_width
end function

public function integer uof_get_default_column_width ();/**
 * stefanop
 *
 * Ritorna la larghezza di default associata alle colonne
 **/
 

return this.ii_column_width
end function

public subroutine uof_clearallrows ();rowsmove(1, rowcount(), Primary!, this, 1, Delete!)
resetupdate()
end subroutine

public subroutine uof_draw ();uof_create_dw()
end subroutine

public subroutine uof_init (integer ai_mode, integer ai_rows, integer ai_cols, boolean ab_row_header, boolean ab_col_header);/**
 * stefanop
 * 27/02/2012
 *
 * Inizializzo la DW
 **/
 
ii_mode = ai_mode
ii_num_rows = ai_rows
ii_num_cols = ai_cols
ib_col_header = ab_col_header
ib_row_header = ab_row_header

uof_create_dw()
end subroutine

public subroutine uof_init_only_header (integer ai_cols);/**
 * stefanop
 * 27/02/2012
 *
 * Funzione che crea solo la parte di header !!!
 **/
 
ii_mode = MODE_VIEW_ONLY
ii_num_rows = 1
ii_num_cols = ai_cols
ib_col_header = false
ib_row_header = false

uof_create_dw(true)
end subroutine

private subroutine uof_create_dw (boolean ab_header);/**
 * stefanop
 * 27/02/2012
 *
 * Creo la sintassi dinamica per creare la DW
 **/
 
string ls_syntax, ls_cols_syntax, ls_error
int li_i, li_num_cols, li_num_rows, ll_header_height, ll_detail_height

reset()
setredraw(false)
ls_cols_syntax = ""

if ab_header then
	ll_header_height = ii_column_height
	ll_detail_height = 0
else
	ll_header_height = 0
	ll_detail_height = ii_column_height
end if
 
ls_syntax = "release 12; "+ &
"datawindow(units=0 timer_interval=0 color=1073741824 brushmode=0 transparency=0 gradient.angle=0 gradient.color=8421504 gradient.focus=0 gradient.repetition.count=0 gradient.repetition.length=100 gradient.repetition.mode=0 gradient.scale=100 gradient.spread=100 gradient.transparency=0 picture.blur=0 picture.clip.bottom=0 picture.clip.left=0 picture.clip.right=0 picture.clip.top=0 picture.mode=0 picture.scale.x=100 picture.scale.y=100 picture.transparency=0 processing=1 HTMLDW=no print.printername=~"~" print.documentname=~"~" print.orientation = 0 print.margin.left = 110 print.margin.right = 110 print.margin.top = 96 print.margin.bottom = 96 print.paper.source = 0 print.paper.size = 0 print.canusedefaultprinter=yes print.prompt=no print.buttons=no print.preview.buttons=no print.cliptext=no print.overrideprintjob=no print.collate=yes print.background=no print.preview.background=no print.preview.outline=yes hidegrayline=no showbackcoloronxp=no picture.file=~"~" grid.lines=0 grid.columnmove=no) "+ &
"header(height=" + string(ll_header_height) + " color=~"" + string(il_header_color) + "~" transparency=~"0~" gradient.color=~"8421504~" gradient.transparency=~"0~" gradient.angle=~"0~" brushmode=~"0~" gradient.repetition.mode=~"0~" gradient.repetition.count=~"0~" gradient.repetition.length=~"~100~" gradient.focus=~"0~" gradient.scale=~"100~" gradient.spread=~"100~" ) "+ &
"summary(height=0 color=~"536870912~" transparency=~"0~" gradient.color=~"8421504~" gradient.transparency=~"0~" gradient.angle=~"0~" brushmode=~"0~" gradient.repetition.mode=~"0~" gradient.repetition.count=~"0~" gradient.repetition.length=~"100~" gradient.focus=~"0~" gradient.scale=~"100~" gradient.spread=~"100~" ) "+ &
"footer(height=0 color=~"536870912~" transparency=~"0~" gradient.color=~"8421504~" gradient.transparency=~"0~" gradient.angle=~"0~" brushmode=~"0~" gradient.repetition.mode=~"0~" gradient.repetition.count=~"0~" gradient.repetition.length=~"100~" gradient.focus=~"0~" gradient.scale=~"100~" gradient.spread=~"100~" ) "+ &
"detail(height=" + string(ll_detail_height)

if ib_col_header then
	ls_syntax += " color=~"536870912~tif(getrow() > 1, 536870912, " + string(il_header_color) + ")~" "
else
	ls_syntax += " color=~"536870912~" "
end if

ls_syntax += "transparency=~"0~" gradient.color=~"8421504~" gradient.transparency=~"0~" gradient.angle=~"0~" brushmode=~"0~" gradient.repetition.mode=~"0~" gradient.repetition.count=~"0~" gradient.repetition.length=~"100~" gradient.focus=~"0~" gradient.scale=~"100~" gradient.spread=~"100~" ) "+ &
"table("

li_num_cols = ii_num_cols
if ib_row_header then li_num_cols++

// Creo le colonne
for li_i = 1 to li_num_cols 
	ls_syntax += uof_create_db_column("x_" + string(li_i), "string", 20)
next


if ab_header then
	wf_create_text_syntax(ref ls_cols_syntax)
end if

// Creo Labels e colonne
wf_create_column_syntax(ref ls_cols_syntax)


ls_syntax += " ) " + ls_cols_syntax + &
"htmltable(border=~"1~" ) "+ &
"htmlgen(clientevents=~"1~" clientvalidation=~"1~" clientcomputedfields=~"1~" clientformatting=~"0~" clientscriptable=~"0~" generatejavascript=~"1~" encodeselflinkargs=~"1~" netscapelayers=~"0~" pagingmethod=0 generatedddwframes=~"1~" ) "+ &
"xhtmlgen() cssgen(sessionspecific=~"0~" ) "+ &
"xmlgen(inline=~"0~" ) "+ &
"xsltgen() "+ &
"jsgen() "+ &
"export.xml(headgroups=~"1~" includewhitespace=~"0~" metadatatype=0 savemetadata=0 ) "+ &
"import.xml() "+ &
"export.pdf(method=0 distill.custompostscript=~"0~" xslfop.print=~"0~" ) "+ &
"export.xhtml()"

if create(ls_syntax, ref ls_error) < 0 then
	g_mb.error("uo_dw_grid::uof_create_dw", ls_error)
else
	
	li_num_rows = ii_num_rows
	if ib_col_header then li_num_rows++
	
	for li_i = 1 to li_num_rows
		
		insertrow(0)
		
	next
		
end if

setredraw(true)
end subroutine

private function string uof_text_header_column (string as_name, string as_type, integer ai_limit);/**
 * stefanop
 * 27/02/2012
 *
 * La funzione crea la sintassi per una singola colonna e ne ritorna la sintassi corretta
 * da inserire nella DW
 **/
 
string ls_syntax

//text(band=header alignment="2" text="Anno Acc Materiali" border="0" color="33554432" x="352" y="8" height="64" width="485" html.valueishtml="0"  name=anno_acc_materiali_t visible="1"  font.face="Tahoma" font.height="-10" font.weight="400"  font.family="2" font.pitch="2" font.charset="0" background.mode="1" background.color="536870912" background.transparency="0" background.gradient.color="8421504" background.gradient.transparency="0" background.gradient.angle="0" background.brushmode="0" background.gradient.repetition.mode="0" background.gradient.repetition.count="0" background.gradient.repetition.length="100" background.gradient.focus="0" background.gradient.scale="100" background.gradient.spread="100" tooltip.backcolor="134217752" tooltip.delay.initial="0" tooltip.delay.visible="32000" tooltip.enabled="0" tooltip.hasclosebutton="0" tooltip.icon="0" tooltip.isbubble="0" tooltip.maxwidth="0" tooltip.textcolor="134217751" tooltip.transparency="0" transparency="0" )


ls_syntax = " column=(type="

choose case as_type
		
	case "string"
		ls_syntax += "char(" + string(ai_limit) + ")"
		
	// TODO: Aggiungere altri formati
		
end choose
 
ls_syntax += " updatewhereclause=no name=" + as_name +" dbname=~"" + as_name + "~") "

return ls_syntax
end function

private subroutine wf_create_text_syntax (ref string as_columns);string ls_text
int li_i, li_x, li_num_cols

li_num_cols = ii_num_cols
if ib_row_header then li_num_cols++

for li_i = 1 to li_num_cols
	
	li_x = (li_i - 1) * ii_column_width
	
	as_columns += "text(band=header alignment=~"2~" border=~"0~" color=~"33554432~" html.valueishtml=~"0~"  visible=~"1~"  font.face=~"Tahoma~" font.height=~"-9~" "
	as_columns += "font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"2~" background.color=~"536870912~" "
	as_columns += "background.transparency=~"0~" background.gradient.color=~"8421504~" background.gradient.transparency=~"0~" background.gradient.angle=~"0~" " 
	as_columns += "background.brushmode=~"0~" background.gradient.repetition.mode=~"0~" background.gradient.repetition.count=~"0~" "
	as_columns += "background.gradient.repetition.length=~"100~" background.gradient.focus=~"0~" background.gradient.scale=~"100~" background.gradient.spread=~"100~" "
	as_columns += "tooltip.backcolor=~"134217752~" tooltip.delay.initial=~"0~" tooltip.delay.visible=~"32000~" tooltip.enabled=~"0~" tooltip.hasclosebutton=~"0~" tooltip.icon=~"0~" "
	as_columns += "tooltip.isbubble=~"0~" tooltip.maxwidth=~"0~" tooltip.textcolor=~"134217751~" tooltip.transparency=~"0~" transparency=~"0~" "
	as_columns += "x=~"" + string(li_x) + "~" y=~"0~" height=~"" + string(ii_column_height - 5) + "~" width=~"" + string(ii_column_width) + "~" name=col_" + string(li_i) + "_t  "
	as_columns += "text=~"" + string(li_i) + "~" )"	

next
end subroutine

public function long uof_get_column_width (integer ai_column_number);/**
 * stefanop
 * 14/05/2012
 *
 * Imposto la grandezza della colonna in base al parametro passato
 **/
 
return Long(describe("#" + string(ai_column_number) + ".width"))
end function

public function integer uof_get_column_clicked ();return il_column_start_clicked
end function

public subroutine uof_deserialize_column_width (long al_columns[]);/**
 * stefanop
 * 22/09/2014
 *
 * Imposta la larghezza delle colonne
 **/
 
int li_i, li_count
long ll_empty[]

li_count = uof_get_columncount()

if li_count > 0 and upperbound(al_columns) <= li_count then
	for li_i = 1 to upperbound(al_columns)
		uof_set_column_width(li_i, al_columns[li_i])
	next
end if
end subroutine

public subroutine uof_serialize_column_width (ref long al_columns[]);/**
 * stefanop
 * 22/09/2014
 *
 * Memorizza la larghezza delle colonne
 **/
 
int li_i, li_count
long ll_empty[]

li_count = uof_get_columncount()
al_columns = ll_empty

if li_count > 0 then
	for li_i = 1 to li_count
		al_columns[li_i] = uof_get_column_width(li_i)
	next
end if

end subroutine

public subroutine uof_sync_column_width (uo_dw_grid adw_grid);/**
 * stefanop
 * 22/09/2014
 *
 * Imposta la larghezza delle colonne
 **/
 
int li_i, li_count
long ll_columns[]

uof_serialize_column_width(ll_columns)
li_count = adw_grid.uof_get_columncount()

if li_count > 0 and upperbound(ll_columns) <= li_count then
	for li_i = 1 to upperbound(ll_columns)
		adw_grid.uof_set_column_width(li_i, ll_columns[li_i])
	next
end if
end subroutine

on uo_dw_grid.create
end on

on uo_dw_grid.destroy
end on

