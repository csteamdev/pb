﻿$PBExportHeader$uo_dw_main.sru
$PBExportComments$Ancestor main DataWindow to inherit (copiata da Powerclass)
forward
global type uo_dw_main from datawindow
end type
end forward

global type uo_dw_main from datawindow
integer width = 1349
integer height = 636
integer taborder = 1
string dataobject = "d_main"
event pcd_active pbm_custom37
event pcd_anychanges pbm_custom38
event pcd_close pbm_custom39
event pcd_commit pbm_custom40
event pcd_concurrencyerror pbm_custom41
event pcd_delete pbm_custom42
event pcd_disable pbm_custom43
event pcd_enable pbm_custom44
event pcd_filter pbm_custom45
event pcd_first pbm_custom46
event pcd_inactive pbm_custom47
event pcd_insert pbm_custom48
event pcd_last pbm_custom49
event pcd_lock pbm_custom50
event pcd_modify pbm_custom51
event pcd_new pbm_custom52
event pcd_next pbm_custom53
event pcd_pcccerror pbm_custom54
event pcd_pickedrow pbm_custom55
event pcd_previous pbm_custom56
event pcd_print pbm_custom57
event pcd_query pbm_custom58
event pcd_refresh pbm_custom59
event pcd_retrieve pbm_custom60
event pcd_rollback pbm_custom61
event pcd_save pbm_custom62
event pcd_saveafter pbm_custom63
event pcd_savebefore pbm_custom64
event pcd_saverowsas pbm_custom65
event pcd_search pbm_custom66
event pcd_setcontrol pbm_custom67
event pcd_setkey pbm_custom68
event pcd_setrowindicator pbm_custom69
event pcd_update pbm_custom70
event pcd_validate pbm_custom71
event pcd_validateafter pbm_custom72
event pcd_validatecol pbm_custom73
event pcd_validaterow pbm_custom74
event pcd_view pbm_custom75
event pcd_wm_hscroll pbm_hscroll
event pcd_wm_vscroll pbm_vscroll
end type
global uo_dw_main uo_dw_main

type variables
//-------------------------------------------------------------------------------
//   Instance Variables
//-------------------------------------------------------------------------------

BOOLEAN		i_Active
STRING			i_ActiveEmptyColors 
INTEGER			i_ActiveLength 
STRING			i_ActiveNonEmptyColors 
BOOLEAN		i_AddedEmpty 
BOOLEAN		i_AllowControlAccept		= TRUE
BOOLEAN		i_AllowControlCancel		= TRUE
BOOLEAN		i_AllowControlClose		= TRUE
BOOLEAN		i_AllowControlDelete		= TRUE
BOOLEAN		i_AllowControlFilter		= TRUE
BOOLEAN		i_AllowControlFirst		= TRUE
BOOLEAN		i_AllowControlInsert		= TRUE
BOOLEAN		i_AllowControlLast		= TRUE
BOOLEAN		i_AllowControlMain		= TRUE
BOOLEAN		i_AllowControlModify		= TRUE
BOOLEAN		i_AllowControlNew		= TRUE
BOOLEAN		i_AllowControlNext		= TRUE
BOOLEAN		i_AllowControlPrevious		= TRUE
BOOLEAN		i_AllowControlPrint		= TRUE
BOOLEAN		i_AllowControlQuery		= TRUE
BOOLEAN		i_AllowControlResetQuery	= TRUE
BOOLEAN		i_AllowControlRetrieve		= TRUE
BOOLEAN		i_AllowControlSave		= TRUE
BOOLEAN		i_AllowControlSaveRowsAs	= TRUE
BOOLEAN		i_AllowControlSearch		= TRUE
BOOLEAN		i_AllowControlView		= TRUE
BOOLEAN		i_AllowDelete 
BOOLEAN		i_AllowDrillDown
BOOLEAN		i_AllowModify
BOOLEAN		i_AllowNew
BOOLEAN		i_AllowQuery
BOOLEAN		i_AlwaysCheckRequired
LONG			i_AnchorRow
INTEGER			i_AttribLength
BOOLEAN		i_AutoConfigMenus
BOOLEAN		i_AutoFocus

BOOLEAN		i_BatchAttrib
STRING			i_BatchString
BOOLEAN		i_BecameCurrent

BOOLEAN		i_CalcHighlightSelected
LONG			i_CCDBDuplicateID[]
BOOLEAN		i_CCDBError
LONG			i_CCDBErrorID[]
UNSIGNEDLONG		i_CCDBMSType
UNSIGNEDLONG		i_CCErrorCode
UNSIGNEDLONG		i_CCErrorColorIdx
S_CCINFO		i_CCInfo[]
BOOLEAN		i_CCIsODBC
LONG			i_CCLastBadRow[]
INTEGER			i_CCMaxNumUpdate
LONG			i_CCNumColumns
LONG			i_CCNumErrors
LONG			i_CCNumRetry[]
LONG			i_CCNumRows
UNSIGNEDLONG		i_CCProcCode
LONG			i_CCSkipDeleteRows[]
LONG			i_CCSkipFilterRows[]
LONG			i_CCSkipPrimaryRows[]
S_CCSTATUS		i_CCStatus[]
STRING			i_CCUser
UNSIGNEDLONG		i_CCUserCode
LONG			i_CCUserRow
UNSIGNEDLONG		i_CCUserStatus
UNSIGNEDLONG		i_CCValidate
INTEGER			i_CCWhichUpdate
UO_DW_MAIN		i_ChangedDW[]
UO_DW_MAIN		i_ChildDW[]
BOOLEAN		i_ChildrenAreModified
STRING			i_ClassName
LONG			i_ColBColors[]
LONG			i_ColBorders[]
LONG			i_ColColors[]
REAL			i_ColFormatNumber
STRING			i_ColFormatString
STRING			i_ColName
INTEGER			i_ColNbr
BOOLEAN		i_ColRequiredError
STRING			i_ColText
STRING			i_ColValMsg
BOOLEAN		i_ColVisible[]
UO_CONTAINER_MAIN	i_Container
INTEGER			i_CurInstance
BOOLEAN		i_Current
UNSIGNEDLONG		i_CurrentMode
LONG			i_CursorRow
BOOLEAN		i_CursorRowFocusRect
BOOLEAN		i_CursorRowPointer

TRANSACTION		i_DBCA
DWBUFFER		i_DBErrorBuffer
LONG			i_DBErrorCode
STRING			i_DBErrorMessage
LONG			i_DBErrorRow
MENU			i_DefaultEditMenu
MENU			i_DefaultFileMenu
BOOLEAN		i_DefaultMenusDefined
MENU			i_DefaultPopupMenu
STRING			i_DevDBErrorMessage
BOOLEAN		i_DidPickedRow
BOOLEAN		i_DidRollback
STRING			i_DisableTabs
BOOLEAN		i_DisplayError
BOOLEAN		i_DisplayRequired
BOOLEAN		i_DoEmpty
BOOLEAN		i_DoSetKey
BOOLEAN		i_DrillingDown
UNSIGNEDLONG		i_DW_ControlWord
LONG			i_DW_DevNumber[]
STRING			i_DW_DevString[]
UNSIGNEDLONG		i_DW_VisualWord
INTEGER			i_DWState
LONG			i_DWTopRow

MENU			i_EditMenu
MENU			i_EditMenuDelete
UNSIGNEDLONG		i_EditMenuDeleteNum
MENU			i_EditMenuFilter
UNSIGNEDLONG		i_EditMenuFilterNum
MENU			i_EditMenuFirst
UNSIGNEDLONG		i_EditMenuFirstNum
BOOLEAN		i_EditMenuInit
MENU			i_EditMenuInsert
UNSIGNEDLONG		i_EditMenuInsertNum
BOOLEAN		i_EditMenuIsPopup
BOOLEAN		i_EditMenuIsValid
MENU			i_EditMenuLast
UNSIGNEDLONG		i_EditMenuLastNum
MENU			i_EditMenuModeSep
UNSIGNEDLONG		i_EditMenuModeSepNum
MENU			i_EditMenuModify
UNSIGNEDLONG		i_EditMenuModifyNum
MENU			i_EditMenuNew
UNSIGNEDLONG		i_EditMenuNewNum
MENU			i_EditMenuNext
UNSIGNEDLONG		i_EditMenuNextNum
MENU			i_EditMenuPrev
UNSIGNEDLONG		i_EditMenuPrevNum
MENU			i_EditMenuPrint
UNSIGNEDLONG		i_EditMenuPrintNum
MENU			i_EditMenuPrintSep
UNSIGNEDLONG		i_EditMenuPrintSepNum
MENU			i_EditMenuQuery
UNSIGNEDLONG		i_EditMenuQueryNum
MENU			i_EditMenuSave
UNSIGNEDLONG		i_EditMenuSaveNum
MENU			i_EditMenuSaveRowsAs
UNSIGNEDLONG		i_EditMenuSaveRowsAsNum
MENU			i_EditMenuSaveSep
UNSIGNEDLONG		i_EditMenuSaveSepNum
MENU			i_EditMenuSearch
UNSIGNEDLONG		i_EditMenuSearchNum
MENU			i_EditMenuSearchSep
UNSIGNEDLONG		i_EditMenuSearchSepNum
MENU			i_EditMenuView
UNSIGNEDLONG		i_EditMenuViewNum
UNSIGNEDLONG		i_EM_EndAddr
UNSIGNEDLONG		i_EM_StartAddr
ROWFOCUSIND		i_EmptyRowInd
BOOLEAN		i_EmptyRowInit
BOOLEAN		i_EmptyRowIsPicture
PICTURE			i_EmptyRowPicture
INTEGER			i_EmptyRowXPosition
INTEGER			i_EmptyRowYPosition
STRING			i_EmptyTextColors
BOOLEAN		i_EnableCC
BOOLEAN		i_EnableCCInsert
BOOLEAN		i_EnableModifyOnOpen
BOOLEAN		i_EnableNewOnOpen
STRING			i_EnableTabs
BOOLEAN		i_ExecuteFilter
BOOLEAN		i_ExtendMode

MENU			i_FileMenu
MENU			i_FileMenuAccept
UNSIGNEDLONG		i_FileMenuAcceptNum
MENU			i_FileMenuCancel
UNSIGNEDLONG		i_FileMenuCancelNum
MENU			i_FileMenuClose
UNSIGNEDLONG		i_FileMenuCloseNum
MENU			i_FileMenuExit
UNSIGNEDLONG		i_FileMenuExitNum
MENU			i_FileMenuExitSep
UNSIGNEDLONG		i_FileMenuExitSepNum
MENU			i_FileMenuFileSep
UNSIGNEDLONG		i_FileMenuFileSepNum
BOOLEAN		i_FileMenuInit
BOOLEAN		i_FileMenuIsValid
MENU			i_FileMenuNew
UNSIGNEDLONG		i_FileMenuNewNum
MENU			i_FileMenuOpen
UNSIGNEDLONG		i_FileMenuOpenNum
MENU			i_FileMenuPrint
UNSIGNEDLONG		i_FileMenuPrintNum
MENU			i_FileMenuPrintSep
UNSIGNEDLONG		i_FileMenuPrintSepNum
MENU			i_FileMenuPrintSetup
UNSIGNEDLONG		i_FileMenuPrintSetupNum
MENU			i_FileMenuSave
UNSIGNEDLONG		i_FileMenuSaveNum
MENU			i_FileMenuSaveRowsAs
UNSIGNEDLONG		i_FileMenuSaveRowsAsNum
MENU			i_FileMenuSaveSep
UNSIGNEDLONG		i_FileMenuSaveSepNum
GRAPHICOBJECT		i_FilterObjects[]
UO_DW_FIND		i_FindObjects[]
LONG			i_FirstTabColumn
LONG			i_FirstVisColumn
UO_DW_MAIN		i_FocusDW
ROWFOCUSIND		i_FocusRowInd
BOOLEAN		i_FocusRowInit
BOOLEAN		i_FocusRowIsPicture
PICTURE			i_FocusRowPicture
INTEGER			i_FocusRowXPosition
INTEGER			i_FocusRowYPosition
BOOLEAN		i_FreeFormStyle

BOOLEAN		i_GotColInfo
BOOLEAN		i_GotFocus
BOOLEAN		i_GotKeyMap
BOOLEAN		i_GotObjects
BOOLEAN		i_GotValInfo[]

BOOLEAN		i_HaveInstances
INTEGER			i_Height
BOOLEAN		i_HighlightSelected

BOOLEAN		i_IgnoreNewRows
BOOLEAN		i_IgnoreRFC
INTEGER			i_IgnoreRFCCount
BOOLEAN		i_IgnoreRFCStack[]
BOOLEAN		i_IgnoreVal
INTEGER			i_IgnoreValAction
INTEGER			i_IgnoreValCount
BOOLEAN		i_IgnoreValStack[]
INTEGER			i_IgnoreVSCount
BOOLEAN		i_IgnoreVScroll
BOOLEAN		i_IgnoreVSStack[]
BOOLEAN		i_InactiveCol
UNSIGNEDLONG		i_InactiveDWColorIdx
STRING			i_InactiveEmptyColors
BOOLEAN		i_InactiveLine
STRING			i_InactiveNonEmptyColors
BOOLEAN		i_InactiveText
INTEGER			i_InFilter
INTEGER			i_InitCount
INTEGER			i_InItemError
BOOLEAN		i_Initialized
BOOLEAN		i_InQuery
INTEGER			i_InReselect
INTEGER			i_InRFC
INTEGER			i_InSave
INTEGER			i_InSearch
UO_DW_MAIN		i_InstanceDW[]
INTEGER			i_InUpdate
BOOLEAN		i_InUse
BOOLEAN		i_InView
BOOLEAN		i_IsCascading
BOOLEAN		i_IsEmpty
BOOLEAN		i_IsFinal
BOOLEAN		i_IsInstance
BOOLEAN		i_IsRoot
BOOLEAN		i_ItemValidated

BOOLEAN		i_KeyMapValid
STRING			i_KeysInParent[]

BOOLEAN		i_LostCurrent

INTEGER			i_MaxChildren
UO_CB_MESSAGE		i_Messages[]
UNSIGNEDLONG		i_ModeOnOpen
UNSIGNEDLONG		i_ModeOnSelect
BOOLEAN		i_Modified
STRING			i_ModifyBorders
STRING			i_ModifyColors
STRING			i_ModifyModeColor
STRING			i_ModifyTextColor
LONG			i_MoveRow
MENU			i_mPopup
BOOLEAN		i_MultiRowDisplay
BOOLEAN		i_MultiSelect
BOOLEAN		i_MultiSelectionChanged

BOOLEAN		i_NewModeOnEmpty
STRING			i_NonEmptyTextColors
ROWFOCUSIND		i_NotFocusRowInd
BOOLEAN		i_NotFocusRowInit
BOOLEAN		i_NotFocusRowIsPicture
PICTURE			i_NotFocusRowPicture
INTEGER			i_NotFocusRowXPosition
INTEGER			i_NotFocusRowYPosition
INTEGER			i_NumChangedDW
INTEGER			i_NumChildren
LONG			i_NumColumns
INTEGER			i_NumFilterObjects
INTEGER			i_NumFindObjects
INTEGER			i_NumInstance
INTEGER			i_NumMessages
LONG			i_NumObjects
INTEGER			i_NumPCControls
LONG			i_NumReselectRows
INTEGER			i_NumSearchObjects
LONG			i_NumSelected
INTEGER			i_NumShareSecondary
INTEGER			i_NumWindowDWs

LONG			i_ObjectColors[]
STRING			i_ObjectNames[]
STRING			i_ObjectType			= "uo_DW_Main"
LONG			i_ObjectTypes[]
LONG			i_OldClickedRow
LONG			i_OldCursorRow
BOOLEAN		i_OnlyOneNewRow
UNSIGNEDLONG		i_OpenChildOption
BOOLEAN		i_OpenModeSet

UO_DW_MAIN		i_ParentDW
BOOLEAN		i_ParentIsValid
UO_CB_MAIN		i_PCControls[]
BOOLEAN		i_PLDeleteOK
BOOLEAN		i_PLDrillDownOK
BOOLEAN		i_PLModifyOK
BOOLEAN		i_PLNewOK
UNSIGNEDLONG		i_PM_EndAddr
UNSIGNEDLONG		i_PM_StartAddr
MENU			i_PopupMenu
MENU			i_PopupMenuDelete
UNSIGNEDLONG		i_PopupMenuDeleteNum
MENU			i_PopupMenuFilter
UNSIGNEDLONG		i_PopupMenuFilterNum
MENU			i_PopupMenuFirst
UNSIGNEDLONG		i_PopupMenuFirstNum
BOOLEAN		i_PopupMenuInit
MENU			i_PopupMenuInsert
UNSIGNEDLONG		i_PopupMenuInsertNum
BOOLEAN		i_PopupMenuIsEdit
BOOLEAN		i_PopupMenuIsValid
MENU			i_PopupMenuLast
UNSIGNEDLONG		i_PopupMenuLastNum
MENU			i_PopupMenuModeSep
UNSIGNEDLONG		i_PopupMenuModeSepNum
MENU			i_PopupMenuModify
UNSIGNEDLONG		i_PopupMenuModifyNum
MENU			i_PopupMenuNew
UNSIGNEDLONG		i_PopupMenuNewNum
MENU			i_PopupMenuNext
UNSIGNEDLONG		i_PopupMenuNextNum
MENU			i_PopupMenuPrev
UNSIGNEDLONG		i_PopupMenuPrevNum
MENU			i_PopupMenuPrint
UNSIGNEDLONG		i_PopupMenuPrintNum
MENU			i_PopupMenuPrintSep
UNSIGNEDLONG		i_PopupMenuPrintSepNum
MENU			i_PopupMenuQuery
UNSIGNEDLONG		i_PopupMenuQueryNum
MENU			i_PopupMenuSave
UNSIGNEDLONG		i_PopupMenuSaveNum
MENU			i_PopupMenuSaveRowsAs
UNSIGNEDLONG		i_PopupMenuSaveRowsAsNum
MENU			i_PopupMenuSaveSep
UNSIGNEDLONG		i_PopupMenuSaveSepNum
MENU			i_PopupMenuSearch
UNSIGNEDLONG		i_PopupMenuSearchNum
MENU			i_PopupMenuSearchSep
UNSIGNEDLONG		i_PopupMenuSearchSepNum
MENU			i_PopupMenuView
UNSIGNEDLONG		i_PopupMenuViewNum
BOOLEAN		i_ProcessMode

ROWFOCUSIND		i_QueryRowInd
BOOLEAN		i_QueryRowInit
BOOLEAN		i_QueryRowIsPicture
PICTURE			i_QueryRowPicture
INTEGER			i_QueryRowXPosition
INTEGER			i_QueryRowYPosition
STRING			i_QueryTabs

INTEGER			i_RedrawCount
BOOLEAN		i_RedrawStack[]
BOOLEAN		i_RefreshChild
INTEGER			i_RefreshMode
UNSIGNEDLONG		i_RefreshOpenOption
BOOLEAN		i_RefreshParent
BOOLEAN		i_RefreshPosted
UNSIGNEDLONG		i_RefreshWhen
UNSIGNEDLONG		i_RequestedMode
STRING			i_ReselectKeys[]
STRING			i_ResetDBUpdate
BOOLEAN		i_ResizeDW
BOOLEAN		i_RetrieveAsNeeded
BOOLEAN		i_RetrieveChildren
BOOLEAN		i_RetrieveMySelf
BOOLEAN		i_RetrieveOnOpen
BOOLEAN		i_RetrySave
UNSIGNEDLONG		i_RFCEvent
UNSIGNEDLONG		i_RFCType
UO_DW_MAIN		i_RootDW
LONG			i_RowNbr

UO_DW_MAIN		i_ScrollDW
BOOLEAN		i_ScrollParent
GRAPHICOBJECT		i_SearchObjects[]
LONG			i_SelectedRows[]
BOOLEAN		i_SelectionChanged
TRIGEVENT		i_SelectionType
UNSIGNEDLONG		i_SetControlMode
BOOLEAN		i_SetDBStrings
BOOLEAN		i_SetEmptyColors
BOOLEAN		i_ShareData
BOOLEAN		i_ShareDoSetKey
BOOLEAN		i_ShareEvent
BOOLEAN		i_ShareModified
UO_DW_MAIN		i_ShareParent
UO_DW_MAIN		i_SharePrimary
UO_DW_MAIN		i_ShareSecondary[]
UNSIGNEDLONG		i_ShareType
BOOLEAN		i_ShowEmpty
LONG			i_ShowRow
BOOLEAN		i_SkipFirstRefresh
STRING			i_SQLSelect
STRING			i_StoredProc
BOOLEAN		i_SyncShare

LONG			i_TabLength
LONG			i_TabOrder[]
BOOLEAN		i_TabularFormStyle

STRING			i_UpdateTable

BOOLEAN		i_ViewAfterSave
STRING			i_ViewBorders
STRING			i_ViewColors
UNSIGNEDLONG		i_ViewModeBorderIdx
STRING			i_ViewModeColor
UNSIGNEDLONG		i_ViewModeColorIdx

INTEGER			i_Width
WINDOW			i_Window
UO_DW_MAIN		i_WindowDWs[]
UNSIGNEDLONG		i_WindowID

STRING			i_xColumnNames[]
BOOLEAN		i_xColumnNilIsNull[]
BOOLEAN		i_xColumnRequired[]
INTEGER			i_xColumnType[]
BOOLEAN		i_xColumnUpdate[]
STRING			i_xColumnValMsg[]
STRING			i_xDBNames[]
INTEGER			i_xKeyColumns[]
INTEGER			i_xKeyMap[]
INTEGER			i_xNumKeyColumns
INTEGER			i_XPos
UNSIGNEDLONG		i_xRefreshMode
UNSIGNEDLONG		i_xReselectMode

INTEGER			i_YPos

S_DEVCONTROL		is_DevControl
S_EVENTCONTROL	is_EventControl
S_EVENTINFO		is_EventInfo
S_DEVCONTROL		is_PostDControl[]
S_EVENTCONTROL	is_PostEControl[]
S_EVENTCONTROL	is_ResetControl


// EnMe 28-07-2009
// Nuova variabile che serve per impostazioni personalizzate dei report
Boolean ib_dw_report = false

//-------------------------------------------------------------------------------
//   Constants
//-------------------------------------------------------------------------------

INTEGER		c_AcceptAndMove		= 2
INTEGER		c_AcceptCB		= 1
UNSIGNEDLONG	c_AcceptIgnoreErrors	= 1201
UNSIGNEDLONG	c_AcceptProcessErrors	= 1200
BOOLEAN	c_AcceptText		= TRUE
UNSIGNEDLONG	c_AllowControlAccept	= 1
UNSIGNEDLONG	c_AllowControlAll		= 0
UNSIGNEDLONG	c_AllowControlCancel	= 2
UNSIGNEDLONG	c_AllowControlClose	= 4
UNSIGNEDLONG	c_AllowControlDelete	= 8
UNSIGNEDLONG	c_AllowControlDisable	= 3801
UNSIGNEDLONG	c_AllowControlEnable	= 3800
UNSIGNEDLONG	c_AllowControlFilter	= 32
UNSIGNEDLONG	c_AllowControlFirst		= 16
UNSIGNEDLONG	c_AllowControlInsert	= 64
UNSIGNEDLONG	c_AllowControlLast		= 128
UNSIGNEDLONG	c_AllowControlMain		= 256
UNSIGNEDLONG	c_AllowControlModify	= 512
UNSIGNEDLONG	c_AllowControlNew		= 1024
UNSIGNEDLONG	c_AllowControlNext		= 2048
UNSIGNEDLONG	c_AllowControlPrevious	= 4096
UNSIGNEDLONG	c_AllowControlPrint		= 8192
UNSIGNEDLONG	c_AllowControlQuery	= 16384
UNSIGNEDLONG	c_AllowControlResetQuery	= 32768
UNSIGNEDLONG	c_AllowControlRetrieve	= 65536
UNSIGNEDLONG	c_AllowControlSave	= 131072
UNSIGNEDLONG	c_AllowControlSaveRowsAs	= 262144
UNSIGNEDLONG	c_AllowControlSearch	= 524288
UNSIGNEDLONG	c_AllowControlView		= 1048576
UNSIGNEDLONG	c_AutoRefreshOnOpen	= 700

BOOLEAN	c_BatchAttrib		= TRUE
UNSIGNEDLONG	c_Black			= 1
UNSIGNEDLONG	c_Blue			= 9
UNSIGNEDLONG	c_Brown			= 16
BOOLEAN	c_BufferDraw		= FALSE

INTEGER		c_CancelCB		= 2
UNSIGNEDLONG	c_CapabilityAll		= 0
UNSIGNEDLONG	c_CapabilityDelete		= 4
UNSIGNEDLONG	c_CapabilityDisable		= 3701
UNSIGNEDLONG	c_CapabilityEnable		= 3700
UNSIGNEDLONG	c_CapabilityModify		= 2
UNSIGNEDLONG	c_CapabilityNew		= 1
UNSIGNEDLONG	c_CapabilityQuery		= 8
UNSIGNEDLONG	c_CascadeFirst		= 1400
UNSIGNEDLONG	c_CascadeLast		= 1401
UNSIGNEDLONG	c_CascadePost		= 1500
UNSIGNEDLONG	c_CascadeTrigger		= 1501
UNSIGNEDLONG	c_CCErrorDeleteModified	= 2905
UNSIGNEDLONG	c_CCErrorDeleteNonExistent	= 2906
UNSIGNEDLONG	c_CCErrorKeyConflict	= 2907
UNSIGNEDLONG	c_CCErrorNone		= 2900
UNSIGNEDLONG	c_CCErrorNonExistent	= 2904
UNSIGNEDLONG	c_CCErrorNonOverlap	= 2901
UNSIGNEDLONG	c_CCErrorOverlap		= 2902
UNSIGNEDLONG	c_CCErrorOverlapMatch	= 2903
LONG		c_CCMaxNumRetry		= 5
UNSIGNEDLONG	c_CCProcUseDWValue	= 3102
UNSIGNEDLONG	c_CCProcUseNewDB	= 3101
UNSIGNEDLONG	c_CCProcUseOldDB	= 3100
UNSIGNEDLONG	c_CCProcUseSpecial	= 3103
UNSIGNEDLONG	c_CCSetForce		= 3401
UNSIGNEDLONG	c_CCSetIfModified		= 3400
UNSIGNEDLONG	c_CCUserCancel		= 3301
UNSIGNEDLONG	c_CCUserNone		= 3000
UNSIGNEDLONG	c_CCUserOK		= 3300
UNSIGNEDLONG	c_CCUserSpecify		= 3001
UNSIGNEDLONG	c_CCValidateNone		= 3200
UNSIGNEDLONG	c_CCValidateRow		= 3201
UNSIGNEDLONG	c_CheckChildren		= 1601
UNSIGNEDLONG	c_CheckForChanges	= 2400
UNSIGNEDLONG	c_CheckThisDW		= 1602
UNSIGNEDLONG	c_CheckTree		= 1600
INTEGER		c_CloseCB		= 3
UNSIGNEDLONG	c_ColorFirst		= 1
UNSIGNEDLONG	c_ColorLast		= 16
UNSIGNEDLONG	c_ColorUndefined		= 0
UNSIGNEDLONG	c_ColTypeBlob		= 407
UNSIGNEDLONG	c_ColTypeDate		= 401
UNSIGNEDLONG	c_ColTypeDateTime	= 402
UNSIGNEDLONG	c_ColTypeDecimal		= 403
UNSIGNEDLONG	c_ColTypeNumber		= 404
UNSIGNEDLONG	c_ColTypeString		= 400
UNSIGNEDLONG	c_ColTypeTime		= 405
UNSIGNEDLONG	c_ColTypeTimeStamp	= 406
UNSIGNEDLONG	c_ColTypeUndefined	= 0
UNSIGNEDLONG	c_ControlActiveDW		= 1000
UNSIGNEDLONG	c_ControlMainMenu	= 1002
UNSIGNEDLONG	c_ControlPopupMenu	= 1003
UNSIGNEDLONG	c_ControlRows		= 1001
UNSIGNEDLONG	c_ControlUndefined	= 0
UNSIGNEDLONG	c_Cyan			= 13

UNSIGNEDLONG	c_DarkBlue		= 10
UNSIGNEDLONG	c_DarkCyan		= 14
UNSIGNEDLONG	c_DarkGray		= 4
UNSIGNEDLONG	c_DarkGreen		= 8
UNSIGNEDLONG	c_DarkMagenta		= 12
UNSIGNEDLONG	c_DarkRed		= 6
UNSIGNEDLONG	c_Default			= 0
INTEGER		c_DeleteCB		= 4
INTEGER		c_DisplayError		= 0
LONG		c_DoNew			= 1100
UNSIGNEDLONG	c_DoRefresh		= 2500
BOOLEAN	c_DrawNow		= TRUE
UNSIGNEDLONG	c_DW_BorderBox		= 3
UNSIGNEDLONG	c_DW_BorderFirst		= 1
UNSIGNEDLONG	c_DW_BorderLast		= 7
UNSIGNEDLONG	c_DW_BorderLowered	= 6
UNSIGNEDLONG	c_DW_BorderNone		= 1
UNSIGNEDLONG	c_DW_BorderRaised	= 7
UNSIGNEDLONG	c_DW_BorderResize	= 4
UNSIGNEDLONG	c_DW_BorderShadowBox	= 2
UNSIGNEDLONG	c_DW_BorderUndefined	= 0
UNSIGNEDLONG	c_DW_BorderUnderLine	= 5
UNSIGNEDLONG	c_DWStateDisabled	= 201
UNSIGNEDLONG	c_DWStateEnabled		= 200
UNSIGNEDLONG	c_DWStateQuery		= 202
UNSIGNEDLONG	c_DWStateUndefined	= 0

UNSIGNEDLONG	c_EventGotoRoot		= 1700
UNSIGNEDLONG	c_EventNoTriggerUp	= 1701

INTEGER		c_Fatal			= -1
INTEGER		c_FilterCB		= 5
UNSIGNEDLONG	c_FindRootDW		= 1800
UNSIGNEDLONG	c_FindScrollDW		= 1801
INTEGER		c_FirstCB			= 6
STRING		c_FormatDate		= "mm/dd/yyyy"
STRING		c_FormatDateTime		= "mm/dd/yyyy hh:mm:ss.ffffff"
STRING		c_FormatTime		= "hh:mm:ss.ffffff"

UNSIGNEDLONG	c_GetCurrentValues	= 1210
UNSIGNEDLONG	c_GetOriginalValues	= 1201
UNSIGNEDLONG	c_Gray			= 3
UNSIGNEDLONG	c_Green			= 7

STRING		c_HeightKey		= "Height"

UNSIGNEDLONG	c_IgnoreChanges		= 2401
BOOLEAN	c_IgnoreRFC		= FALSE
BOOLEAN	c_IgnoreVal		= FALSE
BOOLEAN	c_IgnoreVScroll		= FALSE
STRING		c_INIUndefined		= "[INI_Undefined]"
INTEGER		c_InsertCB		= 7
UNSIGNEDLONG	c_Invisible		= 0
INTEGER		c_InvisibleCol		= -2

INTEGER		c_LastCB			= 8

UNSIGNEDLONG	c_Magenta		= 11
INTEGER		c_MainCB		= 9
UNSIGNEDLONG	c_Mark			= 2
INTEGER		c_MaxNonColumnObjects	= 20
INTEGER		c_MaxObjectNameLen	= 40
INTEGER		c_MinimumOverlap		= 25
UNSIGNEDLONG	c_ModeModify		= 101
UNSIGNEDLONG	c_ModeNew		= 102
UNSIGNEDLONG	c_ModeUndefined		= 0
UNSIGNEDLONG	c_ModeView		= 100
INTEGER		c_ModifyCB		= 10

UNSIGNEDINT	c_NewAppend		= 1101
INTEGER		c_NewCB			= 11
UNSIGNEDINT	c_NewInsert		= 1102
INTEGER		c_NextCB		= 12
UNSIGNEDLONG	c_No			= 900
BOOLEAN	c_NoAcceptText		= FALSE
INTEGER		c_NoDisplayError		= 1
INTEGER		c_NoProcessClicked	= 1
UNSIGNEDLONG	c_NoPromptUser		= 2600
UNSIGNEDLONG	c_NoRefresh		= 2501
UNSIGNEDLONG	c_NoRefreshChildren	= 2501
UNSIGNEDLONG	c_NoReselectRows		= 2301
UNSIGNEDLONG	c_NoResetChildren		= 2701
BOOLEAN	c_NoResetFlags		= FALSE
UNSIGNEDLONG	c_NoSyncShare		= 0
UO_CB_MAIN	c_NullCB
UO_DW_MAIN	c_NullDW
MENU		c_NullMenu
PICTURE		c_NullPicture

UNSIGNEDLONG	c_ObjTypeColumn		= 300
UNSIGNEDLONG	c_ObjTypeLine		= 302
UNSIGNEDLONG	c_ObjTypeText		= 301
UNSIGNEDLONG	c_ObjTypeUndefined	= 0
UNSIGNEDLONG	c_OnlyIfModified		= 2200
UNSIGNEDLONG	c_OpenChildAlwaysForce	= 2002
UNSIGNEDLONG	c_OpenChildAlwaysPrevent	= 2004
UNSIGNEDLONG	c_OpenChildAuto		= 2000
UNSIGNEDLONG	c_OpenChildForce		= 2001
UNSIGNEDLONG	c_OpenChildPrevent	= 2003

UNSIGNEDLONG	c_PostRefreshOnOpen	= 703
INTEGER		c_PreviousCB		= 13
INTEGER		c_PrintCB		= 14
BOOLEAN	c_ProcessAttrib		= FALSE
INTEGER		c_ProcessClicked		= 0
UNSIGNEDLONG	c_ProcessFilter		= 2802
UNSIGNEDLONG	c_ProcessGroupCalc	= 2801
BOOLEAN	c_ProcessRFC		= TRUE
UNSIGNEDLONG	c_ProcessSort		= 2800
UNSIGNEDLONG	c_ProcessSync		= 2803
BOOLEAN	c_ProcessVal		= TRUE
BOOLEAN	c_ProcessVScroll		= TRUE
UNSIGNEDLONG	c_PromptUser		= 2601

INTEGER		c_QueryCB		= 15

UNSIGNEDLONG	c_Red			= 5
UNSIGNEDLONG	c_RefreshChildren		= 2500
UNSIGNEDLONG	c_RefreshDrillDown	= 2104
UNSIGNEDLONG	c_RefreshModify		= 2101
UNSIGNEDLONG	c_RefreshNew		= 2102
UNSIGNEDLONG	c_RefreshRFC		= 2103
UNSIGNEDLONG	c_RefreshSame		= 2105
UNSIGNEDLONG	c_RefreshSave		= 2106
UNSIGNEDLONG	c_RefreshUndefined	= 0
UNSIGNEDLONG	c_RefreshView		= 2100
INTEGER		c_RejectAndMove		= 3
INTEGER		c_RejectAndShowPBError	= 0
INTEGER		c_RejectAndStay		= 1
UNSIGNEDLONG	c_ReselectRows		= 2300
UNSIGNEDLONG	c_ResetChildren		= 2700
BOOLEAN	c_ResetFlags		= TRUE
INTEGER		c_ResetQueryCB		= 16
UNSIGNEDLONG	c_RetrieveAllDWs		= 2201
INTEGER		c_RetrieveCB		= 17
UNSIGNEDLONG	c_RFC_Clicked		= 502
UNSIGNEDLONG	c_RFC_DoubleClicked	= 501
UNSIGNEDLONG	c_RFC_NoAutoRefresh	= 601
UNSIGNEDLONG	c_RFC_NoSelect		= 500
UNSIGNEDLONG	c_RFC_RefreshOnMultiSelect= 602
UNSIGNEDLONG	c_RFC_RefreshOnSelect	= 600
UNSIGNEDLONG	c_RFC_RowFocusChanged	= 503
UNSIGNEDLONG	c_RFC_Undefined		= 0

LONG		c_SameBColor		= -1
INTEGER		c_SameBorder		= -1
LONG		c_SameColor		= -1
INTEGER		c_SameTab		= -1
INTEGER		c_SaveCB		= 18
UNSIGNEDLONG	c_SaveChanges		= 2402
INTEGER		c_SaveRowsAsCB		= 19
INTEGER		c_SearchCB		= 20
UNSIGNEDLONG	c_SetRowIndicatorEmpty	= 3603
UNSIGNEDLONG	c_SetRowIndicatorFocus	= 3600
UNSIGNEDLONG	c_SetRowIndicatorNormal	= 3602
UNSIGNEDLONG	c_SetRowIndicatorNotFocus	= 3601
UNSIGNEDLONG	c_SetRowIndicatorQuery	= 3604
UNSIGNEDLONG	c_ShareAllDoSetKey	= 4
UNSIGNEDLONG	c_ShareAllModified		= 2
UNSIGNEDLONG	c_ShareEmpty		= 1901
UNSIGNEDLONG	c_ShareEnabled		= 801
UNSIGNEDLONG	c_ShareModified		= 1
UNSIGNEDLONG	c_ShareNone		= 800
UNSIGNEDLONG	c_ShareNotEmpty		= 1900
UNSIGNEDLONG	c_SharePopRedraw	= 1903
UNSIGNEDLONG	c_SharePopRFC		= 1905
UNSIGNEDLONG	c_SharePopValidate	= 1907
UNSIGNEDLONG	c_SharePopVScroll		= 1909
UNSIGNEDLONG	c_SharePushRedraw	= 1902
UNSIGNEDLONG	c_SharePushRFC		= 1904
UNSIGNEDLONG	c_SharePushValidate	= 1906
UNSIGNEDLONG	c_SharePushVScroll	= 1908
UNSIGNEDLONG	c_Show			= 1
UNSIGNEDLONG	c_ShowMark		= 3
UNSIGNEDLONG	c_SkipRefreshOnOpen	= 701
UNSIGNEDLONG	c_SOCNoSave		= 103
UNSIGNEDLONG	c_SOCPromptUser		= 100
UNSIGNEDLONG	c_SOCPromptUserOnce	= 101
UNSIGNEDLONG	c_SOCSave		= 102
INTEGER		c_SQLAbort		= 1
INTEGER		c_SQLContinue		= 0
INTEGER		c_SQLSkipRequest		= 2
BOOLEAN	c_StateUnused		= FALSE
INTEGER		c_Success		= 0
UNSIGNEDLONG	c_SyncShare		= 1

UNSIGNEDLONG	c_TriggerRefreshOnOpen	= 702

INTEGER		c_ValFailed		= 1
INTEGER		c_ValFixed		= 2
UNSIGNEDLONG	c_ValidateAllCols		= 3500
UNSIGNEDLONG	c_ValidateCurCol		= 3501
INTEGER		c_ValNotProcessed		= -9
INTEGER		c_ValOk			= 0
INTEGER		c_ViewCB		= 21
INTEGER		c_VisibleCol		= -3

UNSIGNEDLONG	c_White			= 2
STRING		c_WidthKey		= "Width"
STRING		c_WinStateKey		= "WindowState"

INTEGER		c_XHandPosition		= -90
STRING		c_XKey			= "X"
UNSIGNEDLONG	c_Yellow			= 15
UNSIGNEDLONG	c_Yes			= 901
INTEGER		c_YHandPosition		= -15
STRING		c_YKey			= "Y"
end variables

forward prototypes
public subroutine batch_attrib (boolean on_or_off)
public subroutine cascade_event (string event_name, unsignedlong children_order, unsignedlong post_or_trigger)
public subroutine change_dw_current ()
public subroutine change_dw_focus (uo_dw_main to_dw)
public function boolean check_dw_modified (unsignedlong check_option)
public function integer column_nbr (string column_name)
public function integer dddw_load_code (string dddw_column, string table_name, string column_code, string column_desc, string where_clause)
public function integer ddlb_load_code (string ddlb_column, string table_name, string column_code, string column_desc, string where_clause)
public subroutine delete_dw_row (long delete_row, unsignedlong do_any_changes, unsignedlong do_refresh)
public subroutine delete_dw_rows (long num_to_delete, long delete_rows[], unsignedlong do_any_changes, unsignedlong do_refresh)
public function integer display_dw_val_error (unsignedlong mb_id, string error_message)
public function boolean dw_accepttext (unsignedlong error_option)
public subroutine dw_allowcontrol (unsignedlong which_capabilities, unsignedlong enable_or_disable)
public subroutine dw_capability (unsignedlong which_capabilities, unsignedlong enable_or_disable)
public function long dw_rowcount ()
public function integer em_load_code (string em_column, string table_name, string column_code, string column_desc, string where_clause)
public function integer filter_dw (unsignedlong do_any_changes, unsignedlong reselect_rows, unsignedlong refresh_mode)
public function long get_retrieve_rows (ref long selected_rows[])
public function long get_selected_rows (ref long selected_rows[])
public function integer insert_dw_rows (long start_row_nbr, long num_to_insert)
public subroutine modify_dw_attrib (integer column_nbr, integer new_tab_order, integer new_border, long new_color, long new_bcolor)
public subroutine pop_dw_redraw ()
public subroutine pop_dw_rfc ()
public subroutine pop_dw_validate ()
public subroutine pop_dw_vscroll ()
public function boolean postevent (string event_name)
public subroutine push_dw_redraw (boolean on_or_off)
public subroutine push_dw_rfc (boolean on_or_off)
public subroutine push_dw_validate (boolean on_or_off)
public subroutine push_dw_vscroll (boolean on_or_off)
public subroutine reset_dev_control ()
public subroutine reset_dw_modified (unsignedlong reset_children)
public subroutine retrieve_dw (unsignedlong only_if_modified, unsignedlong reselect_rows, unsignedlong refresh_mode)
public function integer save_dw (unsignedlong prompt_user, unsignedlong do_refresh)
public function integer search_dw (unsignedlong do_any_changes, unsignedlong reselect_rows, unsignedlong refresh_mode)
public subroutine set_dw_emenu (menu edit_menu)
public subroutine set_dw_fmenu (menu file_menu)
public function integer set_dw_insert ()
public subroutine set_dw_key (string column_name)
public function integer set_dw_modify ()
public function integer set_dw_new ()
public subroutine set_dw_options (transaction dbca, uo_dw_main parent_dw, unsignedlong control_word, unsignedlong visual_word)
public subroutine set_dw_pmenu (menu popup_menu)
public subroutine set_dw_popup (menu popup_menu)
public function integer set_dw_query (unsignedlong do_any_changes)
public function integer set_dw_view (unsignedlong do_any_changes)
public subroutine set_open_child (unsignedlong open_child_option)
public subroutine set_preferred_focus (uo_dw_main to_dw)
public subroutine set_row_indicator (unsignedlong which_focus, rowfocusind which_indicator, picture indicator_bitmap, integer x_position, integer y_position)
public function integer set_selected_rows (long num_to_set, long selected_rows[], unsignedlong do_any_changes, unsignedlong do_refresh, unsignedlong refresh_mode)
public subroutine swap_dw (string dataobject_name, unsignedlong control_word, unsignedlong visual_word)
public function integer validate_date (ref string value, string format)
public function integer validate_dec (ref string value, string format)
public function integer validate_dom (ref string value, string format)
public function integer validate_dow (ref string value, string format)
public function integer validate_dw ()
public function integer validate_dw_row (long validate_row)
public function integer validate_fmt (ref string value, string pattern)
public function integer validate_ge (ref string value, real value_ge)
public function integer validate_gt (ref string value, real value_gt)
public function integer validate_int (ref string value, string format)
public function integer validate_le (ref string value, real value_le)
public function integer validate_length (ref string value, long length)
public function integer validate_lt (ref string value, real value_lt)
public function integer validate_maxlen (ref string value, long max_len)
public function integer validate_minlen (ref string value, long min_len)
public function integer validate_mon (ref string value, string format)
public function integer validate_phone (ref string value)
public function integer validate_time (ref string value, string format)
public function integer validate_year (ref string value, string format)
public function integer validate_zip (ref string value)
public subroutine wire_button (uo_cb_main command_button)
public subroutine wire_filter (graphicobject filter_obj, string filter_column)
public subroutine wire_filter_dw (uo_dw_filter filter_obj, integer num_filter_column, string filter_dw_column[], string filter_column[])
public subroutine wire_find (uo_dw_find find_dw, string find_column)
public subroutine wire_message (uo_cb_message messagecb, string message_column, string message_title)
public subroutine wire_search (graphicobject search_obj, string search_table, string search_column)
public subroutine wire_search_dw (uo_dw_search search_obj, integer num_search_column, string search_dw_column[], string search_table[], string search_column[])
public subroutine wire_share (uo_dw_main secondary_dw, unsignedlong share_type)
public subroutine add_reselection (string reselect_keys[])
public function integer ask_user (integer num_modified_dws, uo_dw_main modified_dws[])
public subroutine build_changed_list (ref integer num_modified_dws, ref uo_dw_main modified_dws[])
public function integer check_dw_save (unsignedlong do_refresh)
public function integer check_modifications (unsignedlong do_any_changes)
public subroutine check_save (ref unsignedlong refresh_cmd)
public subroutine config_menus ()
public subroutine do_cc_rollback ()
public function uo_dw_main find_dw_current ()
public function uo_dw_main find_dw_root (unsignedlong qualification)
public subroutine find_instance (long selected_row)
public function long find_key_row (long which_to_find, string key_array[], long start_row, long stop_row)
public function string get_col_data (long row_nbr, integer column_nbr)
public function string get_col_data_ex (long row_nbr, integer column_nbr, dwbuffer which_buffer, unsignedlong which_values)
public subroutine get_col_info ()
public subroutine get_event_info (unsignedlong trigger_level)
public subroutine get_val_info (integer column_nbr)
public function integer handle_retrieve (ref unsignedlong next_mode, ref integer level)
public function integer load_rows (uo_dw_main skip_dw, uo_dw_main skip_dw_children)
public subroutine map_keys ()
public subroutine pass_minimize ()
public subroutine pass_normal ()
public function integer process_modes (ref unsignedlong next_mode, long insert_row_nbr)
public subroutine remove_dw_rows (long num_to_remove, long remove_rows[], unsignedlong do_refresh)
public subroutine restore_selected_rows (long num_selected, ref long selected_rows[], ref string selected_keys[])
public function long save_selected_rows (ref long selected_rows[], ref string selected_keys[])
public subroutine secure_controls ()
public subroutine set_auto_focus (uo_dw_main to_dw)
public subroutine set_cb_message (uo_cb_message messagecb, string message_column, string message_title)
public subroutine set_cb_scroll (uo_cb_next scrollcb_next, uo_cb_previous scrollcb_prev, uo_cb_first scrollcb_first, uo_cb_last scrollcb_last)
public subroutine set_cc_row (long data_row, unsignedlong set_cc_option)
public subroutine set_col_data (long row_nbr, integer column_nbr, string value)
public subroutine set_ddlb_search (uo_ddlb_search ddlb_name, string search_table, string search_column)
public subroutine set_dw_default_menus (menu file_menu, menu edit_menu, menu popup_menu)
public subroutine set_dw_find (uo_dw_find find_dw, string find_column)
public subroutine set_em_search (uo_em_search em_name, string search_table, string search_column)
public subroutine set_lb_search (uo_lb_search lb_name, string search_table, string search_column)
public subroutine set_sle_search (uo_sle_search sle_name, string search_table, string search_column)
public subroutine setup_dw ()
public subroutine share_flags (unsignedlong which_flags, boolean new_state)
public subroutine share_insert (long insert_row_nbr)
public subroutine share_remove (long remove_row_nbr)
public subroutine share_state (unsignedlong which_state, boolean new_state)
public subroutine unset_query ()
public subroutine unwire_button (uo_cb_main command_button)
public subroutine unwire_filter (graphicobject filter_obj)
public subroutine unwire_find (uo_dw_find find_obj)
public subroutine unwire_message (uo_cb_message messagecb)
public subroutine unwire_search (graphicobject search_obj)
public subroutine unwire_share (uo_dw_main secondary_dw)
public subroutine update_modified ()
public subroutine validate_columns (unsignedlong which_columns, long except_column)
public subroutine debug (string event_name, string prefix, integer debug_level)
public function boolean dw_buffer_drawing (boolean new_val)
public function long dw_debug_count (long new_val)
public subroutine enter_dw_filter ()
public subroutine enter_dw_itemerror ()
public subroutine enter_dw_refresh ()
public subroutine enter_dw_reselect ()
public subroutine enter_dw_rfc ()
public subroutine enter_dw_save ()
public subroutine enter_dw_search ()
public subroutine enter_dw_update ()
public subroutine exit_dw_filter ()
public subroutine exit_dw_itemerror ()
public subroutine exit_dw_refresh ()
public subroutine exit_dw_reselect ()
public subroutine exit_dw_rfc ()
public subroutine exit_dw_save ()
public subroutine exit_dw_search ()
public subroutine exit_dw_update ()
public subroutine proc_entry (string proc_name, long debug_dump_level)
public subroutine proc_exit ()
public subroutine remove_obj (powerobject a_object, ref integer num_objects, ref powerobject objects[])
public subroutine set_document_name (string as_document_name)
end prototypes

on pcd_active;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Active
//  Description   : Indicate that this DataWindow is active.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_HighlightOnly
INTEGER  l_Idx

//----------
//  Assume that we are not becoming the current DataWindow.
//----------

i_BecameCurrent = FALSE

//----------
//  Find out if we are being called only to highlight this
//  DataWindow and reset the Event Control structure.
//----------

l_HighlightOnly = is_EventControl.Highlight_Only
is_EventControl = is_ResetControl

//----------
//  If this window is already active and we are not performing
//  highlighting, do nothing.
//----------

IF NOT i_InUse THEN
   GOTO Finished
END IF

IF NOT l_HighlightOnly THEN

   //----------
   //  Since the current DataWindow is being set, let the other
   //  events know that it has been done.
   //----------

   PCCA.PCMGR.i_DW_FocusDone = TRUE

   IF i_Active THEN
      GOTO Finished
   END IF
END IF

//----------
//  If we get to here, we are either not the current DataWindow or
//  we are just being triggered for the highlight effect.  If we
//  are not just doing highlighting (e.g. for pcd_AnyChanges),
//  then we are becoming the current DataWindow
//----------

IF NOT l_HighlightOnly THEN

   //----------
   //  If there is already a current DataWindow, tell it that it
   //  just lost its status as the current DataWindow.
   //----------

   IF IsValid(PCCA.Window_CurrentDW) THEN
      PCCA.Window_CurrentDW.TriggerEvent("pcd_Inactive")
   END IF

   //----------
   //  This DataWindow is now the current DataWindow.
   //----------

   PCCA.Window_CurrentDW = THIS

   //----------
   //  If this DataWindow is not marked as being the current
   //  DataWindow in the window, then make sure all of the other
   //  DataWindows in this window are marked as not being the
   //  current DataWindow and that this DataWindow is.
   //----------

   IF NOT i_Current THEN
      FOR l_Idx = 1 TO i_NumWindowDWs
         i_WindowDWs[l_Idx].i_Current = FALSE
      NEXT
      i_Current = TRUE
   END IF

   //----------
   //  If an activate is being processed, make sure that it knows
   //  that the active DataWindow has changed.
   //----------

   IF PCCA.PCMGR.i_ProcessingActivate THEN
      PCCA.PCMGR.i_ActivateDW = THIS
   END IF
END IF

//----------
//  Return the color of all text in the DataWindow to its
//  original color to indicate that this DataWindow is active.
//----------

IF i_InactiveDWColorIdx <> c_ColorUndefined THEN
   SetRedraw(c_BufferDraw)
   i_RedrawCount = i_RedrawCount + 1
   i_RedrawStack[i_RedrawCount] = c_BufferDraw

   IF i_IsEmpty AND i_DWState <> c_DWStateQuery THEN
      Modify(i_ActiveEmptyColors)
   ELSE
      Modify(i_ActiveNonEmptyColors)
   END IF

   IF i_RedrawCount > 1 THEN
      i_RedrawCount = i_RedrawCount - 1
      SetRedraw(i_RedrawStack[i_RedrawCount])
   ELSE
      i_RedrawCount = 0
      SetRedraw(TRUE)
   END IF
END IF

//----------
//  If this event was not triggered for its highlight effect, then
//  mark this DataWindow as current DataWindow.
//----------

IF NOT l_HighlightOnly THEN
   i_Active        = TRUE
   i_BecameCurrent = TRUE

   //----------
   //  Since we now are active, set controls.
   //----------

   is_EventControl.Control_Mode = c_ControlActiveDW
   TriggerEvent("pcd_SetControl")
END IF

Finished:

i_ExtendMode = i_BecameCurrent
end on

on pcd_anychanges;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_AnyChanges
//  Description   : Checks the DataWindow(s) for changes.
//
//  Return Value  : c_Success - No modifications
//                  c_Yes     - Save the changes.
//                  c_No      - Forget the changes.
//                  c_Fatal   - Cancel the command in progress.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_OnlyDoChildren, l_CheckQuiet, l_CheckCurInstance
BOOLEAN       l_CheckAccept,    l_ClearOk,    l_ClearCancel
INTEGER       l_Answer,         l_Start,      l_End,        l_Idx
LONG          l_RowCount
DWITEMSTATUS  l_ItemStatus
UO_DW_MAIN    l_ChildDW

//----------
//  pcd_AnyChanges can be triggered to perform several different
//  functions.  It is told what function to perform by flags set
//  in the Event Control structure.  Some of the operations that
//  pcd_AnyChanges provides are:
//     - Check_Accept:
//          Check for validation errors in the field in which
//          the user is currently entering data.
//     - Clear_Ok:
//          The data has been successfully saved to the database.
//          Clear all of the modified flags to indicate that
//          the DataWindow does not contain any changes.
//     - Clear_Cancel:
//          The user has indicated that they want to abort
//          changes.  pcd_AnyChanges needs clear all of the
//          modified flags to indicate that the DataWindow should
//          not be saved and sets the i_RetrieveMySelf flag to
//          TRUE to indicate that the this DataWindow needs to be
//          re-retrieved from the database.
//     - None of the above:
//          Check for changes.  If there are changes, ask the
//          user if they want to save the changes, abort the
//          changes, or cancel the command in progress.
//
//  Grab all of the options to this event out of the Event
//  Control structure.
//----------

l_OnlyDoChildren   = is_EventControl.Only_Do_Children
l_CheckAccept      = is_EventControl.Check_Accept
l_CheckQuiet       = is_EventControl.Check_Quiet
l_CheckCurInstance = is_EventControl.Check_Cur_Instance
l_ClearOk          = is_EventControl.Clear_Ok
l_ClearCancel      = is_EventControl.Clear_Cancel

//----------
//  Get_Event_Info() returns status about what stage of processing
//  this event is in (e.g. cascading).
//----------

Get_Event_Info(c_EventNoTriggerUp)

//----------
//  Make sure that this DataWindow is in use.
//----------

IF NOT i_InUse THEN
   is_EventInfo.Event_Recipient = FALSE
   is_EventInfo.Event_Root      = FALSE
   is_EventInfo.Cascading       = FALSE
   PCCA.Error                  = c_Fatal
   GOTO Finished
END IF

//----------
//  If this is the root DataWindow of the event, then we are just
//  starting to look for DataWindows that have been modified.
//  Reset PCCA.Error and i_RootDW.i_NumChangedDW.
//----------

IF is_EventInfo.Event_Root THEN
   PCCA.Error              = c_Success
   i_RootDW.i_NumChangedDW = 0
END IF

//----------
//  If the event is cascading, we should be checking to see if the
//  DataWindow has been modified or row(s) deleted.
//----------

IF is_EventInfo.Cascading THEN

   //----------
   //  Use Update_Modified() to insure that the i_Modified flag
   //  is current.
   //----------

   Update_Modified()

   //----------
   //  Check to see if our children had changes.
   //----------

   FOR l_Idx = 1 TO i_NumChildren
      l_ChildDW = i_ChildDW[l_Idx]

      //----------
      //  Set up the Event Control structure to be passed to the
      //  child DataWindow.  Set cascading to TRUE to tell the
      //  child DataWindow that it is being triggered by its
      //  parent (i.e. this DataWindow).
      //----------

      l_ChildDW.is_EventControl.Cascading = TRUE

      //----------
      //  The options in the Event Control structure are set up
      //  with basically the same options that were specified for
      //  root DataWindow of this event.
      //----------

      l_ChildDW.is_EventControl.Check_Accept = l_CheckAccept
      l_ChildDW.is_EventControl.Clear_Ok     = l_ClearOk
      l_ChildDW.is_EventControl.Clear_Cancel = l_ClearCancel

      //----------
      //  Tell the child DataWindow to check itself and its
      //  children DataWindows for changes.
      //----------

      l_ChildDW.TriggerEvent("pcd_AnyChanges")
   NEXT

   //----------
   //  We checked the "normal" children DataWindows.  Now we must
   //  check the Instance children DataWindows.  This event can
   //  either be told to check only the current Instance
   //  DataWindow child or all Instance DataWindow children.
   //----------

   IF NOT l_CheckCurInstance THEN

      //----------
      //  We have been told to check all Instance DataWindow
      //  children.  Set up the FOR loop parameters to check all
      //  of the Instance DataWindow children.
      //----------

      l_Start = 1
      l_End   = i_NumInstance
   ELSE

      //----------
      //  We have been told to only check the current Instance
      //  DataWindow child.  Set up the FOR loop parameters to
      //  check only the current Instance DataWindow child.
      //----------

      IF i_CurInstance > 0 THEN
         l_Start = i_CurInstance
         l_End   = i_CurInstance
      ELSE

         //----------
         //  There is not a current Instance DataWindow (i.e.
         //  there is not an Instance DataWindow associated with
         //  the currently selected row.  Set the FOR loop
         //  parameters to bogus values so that the loop will
         //  never execute.
         //----------

         l_Start = 1
         l_End   = 0
      END IF
   END IF

   //----------
   //  Cycle through the specified Instance DataWindow children
   //  looking for changes.
   //----------

   IF l_End > 0 THEN
   FOR l_Idx = l_Start TO l_End
      l_ChildDW = i_InstanceDW[l_Idx]

      //----------
      //  Set up the Event Control structure to be passed to the
      //  child DataWindow.  Set cascading to TRUE to tell the
      //  child DataWindow that it is being triggered by its
      //  parent (i.e. this DataWindow).
      //----------

      l_ChildDW.is_EventControl.Cascading = TRUE

      //----------
      //  The options in the Event Control structure are set up
      //  with basically the same options that were specified for
      //  root DataWindow of this event.
      //----------

      l_ChildDW.is_EventControl.Check_Accept = l_CheckAccept
      l_ChildDW.is_EventControl.Clear_Ok     = l_ClearOk
      l_ChildDW.is_EventControl.Clear_Cancel = l_ClearCancel

      //----------
      //  Tell the child DataWindow to check itself and its
      //  children DataWindows for changes.
      //----------

      l_ChildDW.TriggerEvent("pcd_AnyChanges")
   NEXT
   END IF

   //----------
   //  If this DataWindow is not the root DataWindow of the event
   //  (e.g. it is a child of the root DataWindow) or if
   //  only-do-children was NOT specified (i.e. the caller
   //  specified that checks for changes was to occur in all
   //  DataWindows, including the root DataWindow), then we must
   //  check this DataWindow for changes.
   //----------

   IF (NOT is_EventInfo.Event_Root OR NOT l_OnlyDoChildren) THEN

      //----------
      //  The l_CheckAccept option indicates that the caller wants
      //  to know if the current field in each of the DataWindows
      //  contain valid data.  To find the answer, this event must
      //  perform an AcceptText() and check for validation errors.
      //----------

      IF l_CheckAccept THEN

         //----------
         //  We only need to check for accept errors if we haven't
         //  found any yet.  If we have found validation error,
         //  there is not any reason to check for more - one is
         //  enough.
         //----------

         IF PCCA.Error = c_Success THEN

            //----------
            //  Accept the user's input into the column.
            //----------

            IF NOT dw_AcceptText(c_AcceptProcessErrors) THEN
               Change_DW_Focus(THIS)
               PCCA.Error = c_Fatal
            END IF
         END IF
      ELSE

         //----------
         //  If the DataWindow specified c_RefreshChild and its
         //  parent is either going to be retrieved or was
         //  modified, then we need to retrieve this DataWindow.
         //----------

         IF l_ClearOk THEN
            IF i_RefreshChild  THEN
            IF i_ParentIsValid THEN
               IF i_ParentDW.i_RetrieveMySelf OR &
                  i_ParentDW.i_Modified       THEN
                  i_RetrieveMySelf = TRUE
               END IF
            END IF
            END IF
         END IF

         //----------
         //  If we get here, we are only concerned about whether
         //  the DataWindow has been modified.
         //----------

         //----------
         //  If this DataWindow has been modified, we must do a
         //  few things.
         //----------

         IF i_Modified THEN

            //----------
            //  If pcd_AnyChanges is being triggered to check for
            //  changes (i.e. the "clear" options were not
            //  specified), then add this DataWindow to the list
            //  of modified DataWindows.  pcd_AnyChanges will use
            //  this list later for highlighting.
            //----------

            IF NOT l_ClearOk AND NOT l_ClearCancel THEN
               i_RootDW.i_NumChangedDW = i_RootDW.i_NumChangedDW + 1
               i_RootDW.i_ChangedDW[i_RootDW.i_NumChangedDW] = THIS
            ELSE

               //----------
               //  Clear the i_Modified flag.
               //----------

               Share_Flags(c_ShareModified, FALSE)

               //----------
               //  We need to clear the modified flags to indicate
               //  that there are not any changes in this
               //  DataWindow.  Use RESETUPDATE() to tell
               //  PowerBuilder that there are not any changes.
               //----------

               IF NOT i_SharePrimary.i_ShareModified THEN
                  i_SharePrimary.ResetUpdate()
               END IF

               //----------
               //  If a save happened and the data was
               //  successfully saved to the database, then the
               //  data in this DataWindow matches what is in the
               //  database - no need to do a retrieve.  However,
               //  if changes were aborted or the save failed, we
               //  need to retrieve the original data from the
               //  database and overwrite the user's changes.
               //----------

               IF l_ClearCancel THEN
                  i_SharePrimary.i_RetrieveMySelf = TRUE
                  Share_Flags(c_ShareAllDoSetKey, FALSE)
               ELSE

                  //----------
                  //  If the data is not going to be re-retrieved
                  //  from the database, then we need to deal with
                  //  the i_DoSetKey flag.  The i_DoSetKey flag
                  //  tells us if new rows have been added to the
                  //  DataWindow.  If we are ignoring new rows,
                  //  then there may still be some New! rows in
                  //  the DataWindow after the database update.
                  //  If there are still New! rows, we need to
                  //  leave the i_DoSetKey flag set to TRUE.  If
                  //  we are NOT ignoring new rows, then they
                  //  should have had their keys set by the
                  //  developer and been updated to the database.
                  //----------

                  IF i_SharePrimary.i_ShareDoSetKey THEN

                     //----------
                     //  Since there may have been New! rows in
                     //  the DataWindow that have now been saved,
                     //  we need to set up the selection arrays.
                     //----------

                     i_NumSelected = Get_Retrieve_Rows(i_SelectedRows[])

                     IF i_IgnoreNewRows THEN

                        //----------
                        //  Assume there are not any New! rows
                        //  until proven otherwise.
                        //----------

                        i_DoSetKey = FALSE

                        //----------
                        //  Look for a New! row in the DataWindow.
                        //----------

                        l_RowCount = RowCount()
                        FOR l_Idx = 1 TO l_RowCount
                           l_ItemStatus = &
                              GetItemStatus(l_Idx, 0, Primary!)
                           IF l_ItemStatus = New! THEN
                              i_DoSetKey = TRUE
                              EXIT
                           END IF
                        NEXT
                     ELSE
                        i_DoSetKey = FALSE
                     END IF
                  END IF
                  IF NOT i_DoSetKey THEN
                     Share_Flags(c_ShareAllDoSetKey, FALSE)
                  END IF
               END IF
            END IF
         END IF
      END IF
   END IF
END IF

//----------
//  If this is the root DataWindow of the event, by the time we
//  get to here all of the children DataWindows have been checked
//  for changes.  If i_RootDW.i_NumChangedDW is greater than 0,
//  then are DataWindows that have changes and we need to check
//  with the user to see what they want to do about them.
//----------

IF is_EventInfo.Event_Root      THEN
IF i_RootDW.i_NumChangedDW > 0 THEN

   //----------
   //  If the caller specified to check quietly, then the caller
   //  only wants to know if there ARE changes and doesn't need to
   //  know the user wants to save them or not.
   //----------

   IF l_CheckQuiet THEN

      //----------
      //  If we got to here, there are DataWindows with changes.
      //  Set PCCA.Error to indicate this fact.
      //----------

      PCCA.Error = c_Fatal
   ELSE

      l_Answer = Ask_User(i_RootDW.i_NumChangedDW, &
                          i_RootDW.i_ChangedDW[])

      //----------
      //  Finally, let's see how the user responded.  Map the
      //  return from fu_MessageBox() to PowerClass enumerated
      //  constants.
      //----------

      CHOOSE CASE l_Answer
         CASE 2
            PCCA.Error = c_No
         CASE 3
            PCCA.Error = c_Fatal
         CASE ELSE
            PCCA.Error = c_Yes
      END CHOOSE

      //----------
      //  If the user said to abort changes, we immediately
      //  trigger ourselves, specifing that changes are to be
      //  cleared and that modified DataWindows should be marked
      //  for retrieval.
      //----------

      IF PCCA.Error = c_No THEN
         is_EventControl.Only_Do_Children = l_OnlyDoChildren
         is_EventControl.Cascading        = TRUE
         is_EventControl.Clear_Cancel     = TRUE
         TriggerEvent("pcd_AnyChanges")

         //----------
         //  Make sure the PCCA.Error reflects the user's answer.
         //----------

         PCCA.Error = c_No
      END IF
   END IF
END IF
END IF

//----------
//  For the developer, we set the instance variables to reflect
//  the state of processing by this event.
//----------

i_IsFinal     = is_EventInfo.Event_Recipient
i_IsRoot      = is_EventInfo.Event_Root
i_IsCascading = is_EventInfo.Cascading
i_ExtendMode  = is_EventInfo.Cascading

Finished:
end on

on pcd_close;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Close
//  Description   : Closes a DataWindow and its children.  DO NOT 
//                  EXTEND THIS EVENT!  The window may be gone by 
//                  the time your extended code begins execution.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN              l_NoClose, l_DoClose, l_Found
INTEGER              l_Idx
STRING               l_Select
UNSIGNEDLONG         l_RefreshCmd
GRAPHICOBJECT        l_SearchObj
GRAPHICOBJECT        l_FilterObj
UO_CB_MAIN           l_PCControl
UO_DDDW_SEARCH_MAIN  l_SDDDWName
UO_DW_SEARCH         l_SDWName
UO_DDLB_SEARCH       l_SDDLBName
UO_EM_SEARCH         l_SEMName
UO_LB_SEARCH         l_SLBName
UO_SLE_SEARCH        l_SSLEName
UO_DDDW_FILTER_MAIN  l_FDDDWName
UO_DW_FILTER         l_FDWName
UO_DDLB_FILTER       l_FDDLBName
UO_EM_FILTER         l_FEMName
UO_LB_FILTER         l_FLBName
UO_SLE_FILTER        l_FSLEName
UO_DW_MAIN           l_ToDW, l_ChildDW

//----------
//  Grab the No_Close flag out of the Event Control structure.
//  If No_Close is TRUE, then this event is being told to NOT to
//  try to close the window using CLOSE().
//----------

l_NoClose = is_EventControl.No_Close

//----------
//  Get_Event_Info() returns status about what stage of processing
//  this event is in (e.g. cascading).
//----------

Get_Event_Info(c_EventNoTriggerUp)

//----------
//  Assume that the DataWindow cannot close (e.g. this DataWindow
//  has changes and the user chooses cancel).
//----------

l_DoClose = FALSE

//----------
//  If we are not initialized, do nothing.
//----------

IF NOT i_Initialized THEN
   GOTO Finished
END IF

//----------
//  If this DataWindow is the root of the event, push the close
//  prompt and check to see if any changes have been made to this
//  DataWindow or its children.
//----------

IF is_EventInfo.Event_Root THEN
   SetPointer(HourGlass!)
   PCCA.Error = c_Success

   PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_Close, c_Show)

   Check_Save(l_RefreshCmd)

   //----------
   //  If PCCA.Error does not indicate success, then the user
   //  hit cancel or pcd_Save failed (e.g. validation error).
   //----------

   IF PCCA.Error <> c_Success THEN

      //----------
      //  Pop the close prompt off and exit the event.
      //----------

      PCCA.MDI.fu_Pop()
      PCCA.Error = c_Fatal
      GOTO Finished
   END IF

   //----------
   //  If a save or abort changes happened, the DataWindow
   //  hierarchy may need refreshing.
   //----------

   IF i_RootDW     <> THIS               THEN
   IF l_RefreshCmd <> c_RefreshUndefined THEN
      i_RootDW.is_EventControl.Skip_DW_Valid = TRUE
      i_RootDW.is_EventControl.Skip_DW       = THIS
      i_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
      i_RootDW.TriggerEvent("pcd_Refresh")

      //----------
      //  If an error occurred during the refresh, don't allow
      //  the close to happen.
      //----------

      IF PCCA.Error <> c_Success THEN
         PCCA.MDI.fu_Pop()
         PCCA.Error = c_Fatal
         GOTO Finished
      END IF
   END IF
   END IF
END IF

IF is_EventInfo.Cascading THEN

   //----------
   //  Trigger pcd_Close on all the children DataWindows.
   //----------

   DO WHILE i_NumChildren <> 0
      l_ChildDW = i_ChildDW[i_NumChildren]

      //----------
      //  If our child is on the same window as us, tell it not to
      //  trigger try to close.  Since we know the window can't
      //  close until this DataWindow is closed (i.e. marked as
      //  not-in-use), there is no use for the children
      //  DataWindows to try to close the window.
      //----------

      IF i_Window = l_ChildDW.i_Window THEN
         l_ChildDW.is_EventControl.No_Close = TRUE
      END IF

      //----------
      //  Tell the child DataWindow that the event is being
      //  cascaded from its parent.
      //----------

      l_ChildDW.is_EventControl.Cascading = TRUE

      //----------
      //  Tell the child DataWindow to close.
      //----------

      l_ChildDW.TriggerEvent("pcd_Close")
   LOOP

   //----------
   //  Trigger pcd_Close on all the instance DataWindows.
   //----------

   DO WHILE i_NumInstance <> 0
      l_ChildDW = i_InstanceDW[i_NumInstance]

      //----------
      //  If our child is on the same window as us, tell it not to
      //  trigger try to close.  Since we know the window can't
      //  close until this DataWindow is closed (i.e. marked as
      //  not-in-use), there is no use for the children
      //  DataWindows to try to close the window.
      //----------

      IF i_Window = l_ChildDW.i_Window THEN
         l_ChildDW.is_EventControl.No_Close = TRUE
      END IF

      //----------
      //  Tell the child DataWindow that the event is being
      //  cascaded from its parent.
      //----------

      l_ChildDW.is_EventControl.Cascading = TRUE

      //----------
      //  Tell the child Instance DataWindow to close.
      //----------

      l_ChildDW.TriggerEvent("pcd_Close")
   LOOP

   //----------
   //  If this DataWindow is sharing result sets, make sure they
   //  get shut down.
   //----------

   IF i_ShareType <> c_ShareNone THEN
      i_SharePrimary.Unwire_Share(THIS)
   ELSE
      IF i_NumShareSecondary > 0 THEN
         FOR l_Idx = i_NumShareSecondary TO 1 STEP -1
            Unwire_Share(i_ShareSecondary[l_Idx])
         NEXT
      END IF
   END IF

   //----------
   //  If this DataWindow has a parent, then remove this DataWindow
   //  from the parent's list of children.
   //----------

   IF i_ParentIsValid THEN
      IF i_IsInstance THEN
         Remove_Obj(THIS,                     &
                    i_ParentDW.i_NumInstance, &
                    i_ParentDW.i_InstanceDW[])

         //----------
         //  Tell the parent to find a new current Instance
         //  DataWindow since this DataWindow is closing.
         //----------

         i_ParentDW.Find_Instance(i_ParentDW.i_ShowRow)
      ELSE

         //----------
         //  For normal DataWindow, just remove it from the parent
         //  DataWindow's child array.
         //----------

         Remove_Obj(THIS,                     &
                    i_ParentDW.i_NumChildren, &
                    i_ParentDW.i_ChildDW[])
      END IF

      i_ParentIsValid = FALSE
   END IF

   //----------
   //  If we were in use, then take ourselves out of use.
   //----------

   IF i_InUse THEN

      //----------
      //  Make sure we are not PCCA.PCMGR.i_DW_NeedRetrieveDW.
      //----------

      IF PCCA.PCMGR.i_DW_NeedRetrieve          THEN
      IF PCCA.PCMGR.i_DW_NeedRetrieveDW = THIS THEN
         PCCA.PCMGR.i_DW_NeedRetrieve = FALSE
      END IF
      END IF

      //----------
      //  Reset basic DataWindow relationships.
      //----------

      i_RootDW   = THIS
      i_ScrollDW = THIS

      //----------
      //  Since the DataWindow is closed, it can't be modified.
      //----------

      i_Modified = FALSE
      i_DoSetKey = FALSE
   END IF

   //----------
   //  l_NoClose tells if we should avoid calling CLOSE()
   //  because we are being called by CloseQuery.  If it is Ok
   //  to close, then tell CloseQuery not to do anything by
   //  setting PCCA.Do_Event and close the window.
   //----------

   IF NOT l_NoClose THEN

      //----------
      //  Set i_InUse to FALSE while we try to find another
      //  DataWindow.
      //----------

      i_InUse = FALSE

      //----------
      //  It is Ok if we close the window.  But before we do,
      //  we need to see if there are any other DataWindows
      //  in use on this window.  If there are, then set the current
      //  DataWindow to the first in-use DataWindow in the list.
      //  If there are not any DataWindows in use, then the window
      //  can be closed.
      //----------

      l_ToDW  = Find_DW_Current()

      //----------
      //  If there are is a DataWindow still in use, then see if
      //  if we should make it the current DataWindow.
      //----------

      IF IsValid(l_ToDW) THEN

         //----------
         //  If there is already a current DataWindow, then we
         //  should not change it.  However, if there is not, then
         //  we may want to make l_ToDW the current DataWindow.
         //----------

         IF IsValid(PCCA.Window_CurrentDW) THEN
         ELSE
            IF PCCA.Window_Current = i_Window THEN
               Change_DW_Focus(l_ToDW)
            END IF
         END IF
      ELSE

         //----------
         //  We couldn't find another DataWindow to make current.
         //  However, all the other DataWindows may just be
         //  invisible.  Look for DataWindows that are in use.
         //----------

         l_Found = FALSE
         FOR l_Idx = 1 TO i_NumWindowDWs
            IF i_WindowDWs[l_Idx].i_InUse THEN
               l_Found = TRUE
               EXIT
            END IF
         NEXT

         //----------
         //  Set the l_DoClose flag to TRUE so that this window
         //  will be closed.
         //----------

         IF NOT l_Found THEN
            l_DoClose = TRUE
         END IF
      END IF

      //----------
      //  Restore the value of i_InUse to TRUE.
      //----------

      i_InUse = TRUE
   END IF
END IF

//----------
//  If this is the root DataWindow of the event, then pop
//  the prompt.
//----------

IF is_EventInfo.Event_Root THEN
   PCCA.MDI.fu_Pop()
END IF

Finished:

//----------
//  We may need to do clean up of the DataWindows.  We only need
//  to do this clean up if this DataWindow is in use, there are
//  not any errors and if the window is NOT going to close.  We
//  know that the DataWindow is going to close when:
//     a) l_DoClose is TRUE OR
//     b) 1) l_NoClose is TRUE (i.e. we were triggered by
//            CloseQuery) AND
//        2) There is only one DataWindow on the window AND
//----------

IF i_InUse                THEN
IF PCCA.Error = c_Success THEN
   IF NOT (l_DoClose OR (l_NoClose AND i_NumWindowDWs = 1)) THEN

      //----------
      //  Reset this DataWindow since the window on which this
      //  DataWindow resides is not ready to close.
      //----------

      SetRedraw(c_BufferDraw)
      i_RedrawCount = i_RedrawCount + 1
      i_RedrawStack[i_RedrawCount] = c_BufferDraw

      i_IgnoreRFC      = (NOT c_IgnoreRFC)
      i_IgnoreRFCCount = i_IgnoreRFCCount + 1
      i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

      i_IgnoreVScroll = (NOT c_IgnoreVScroll)
      i_IgnoreVSCount = i_IgnoreVSCount + 1
      i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

      IF NOT i_IsEmpty THEN
         Reset()
         i_DWState = c_DWStateUndefined
         TriggerEvent("pcd_Disable")
      END IF

      IF i_Active THEN
         TriggerEvent("pcd_Inactive")
      END IF

      //----------
      //  Set the current row indicator now that we are closed.
      //----------

      TriggerEvent("pcd_SetRowIndicator")

      //----------
      //  Restore states.
      //----------

      IF i_IgnoreVSCount > 1 THEN
         i_IgnoreVSCount = i_IgnoreVSCount - 1
         i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
      ELSE
         i_IgnoreVSCount = 0
         i_IgnoreVScroll = FALSE
      END IF
      IF NOT i_IgnoreVScroll THEN
         i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
      END IF

      IF i_IgnoreRFCCount > 1 THEN
         i_IgnoreRFCCount = i_IgnoreRFCCount - 1
         i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
      ELSE
         i_IgnoreRFCCount = 0
         i_IgnoreRFC      = FALSE
      END IF

      IF i_RedrawCount > 1 THEN
         i_RedrawCount = i_RedrawCount - 1
         SetRedraw(i_RedrawStack[i_RedrawCount])
      ELSE
         i_RedrawCount = 0
         SetRedraw(TRUE)
      END IF

      //----------
      //  Disable all buttons that are wired to this DataWindow
      //  since they can not be used anymore.
      //----------

      FOR l_Idx = 1 TO i_NumPCControls
         l_PCControl = i_PCControls[l_Idx]
         IF l_PCControl.i_TrigObject = THIS THEN
            l_PCControl.Enabled = FALSE
         END IF
      NEXT

      //----------
      //  Disable all message buttons that are wired to this
      //  DataWindow since they can not be used anymore.
      //----------

      FOR l_Idx = 1 TO i_NumMessages
         i_Messages[l_Idx].Enabled = FALSE
      NEXT

      //----------
      //  Disable all find objects that are wired to this
      //  DataWindow since they can not be used anymore.
      //----------

      FOR l_Idx = 1 TO i_NumFindObjects
         i_FindObjects[l_Idx].Enabled = FALSE
      NEXT

      //----------
      //  Disable all search objects that are wired to this
      //  DataWindow since they can not be used anymore.
      //----------

      FOR l_Idx = 1 TO i_NumSearchObjects
         l_SearchObj = i_SearchObjects[l_Idx]
         CHOOSE CASE l_SearchObj.Tag
            CASE "PowerClass Search DDDW"
               l_SDDDWName         = l_SearchObj
               l_SDDDWName.Enabled = FALSE
            CASE "PowerClass Search DW"
               l_SDWName           = l_SearchObj
               l_SDWName.Enabled   = FALSE
            CASE "PowerClass Search DDLB"
               l_SDDLBName         = l_SearchObj
               l_SDDLBName.Enabled = FALSE
            CASE "PowerClass Search EM"
               l_SEMName           = l_SearchObj
               l_SEMName.Enabled   = FALSE
            CASE "PowerClass Search LB"
               l_SLBName           = l_SearchObj
               l_SLBName.Enabled   = FALSE
            CASE "PowerClass Search SLE"
               l_SSLEName          = l_SearchObj
               l_SSLEName.Enabled  = FALSE
            CASE ELSE
         END CHOOSE
      NEXT

      //----------
      //  Disable all filter objects that are wired to this
      //  DataWindow since they can not be used anymore.
      //----------

      FOR l_Idx = 1 TO i_NumFilterObjects
         l_FilterObj = i_FilterObjects[l_Idx]
         CHOOSE CASE l_FilterObj.Tag
            CASE "PowerClass Filter DDDW"
               l_FDDDWName         = l_FilterObj
               l_FDDDWName.Enabled = FALSE
            CASE "PowerClass Filter DW"
               l_FDWName           = l_FilterObj
               l_FDWName.Enabled   = FALSE
            CASE "PowerClass Filter DDLB"
               l_FDDLBName         = l_FilterObj
               l_FDDLBName.Enabled = FALSE
            CASE "PowerClass Filter EM"
               l_FEMName           = l_FilterObj
               l_FEMName.Enabled   = FALSE
            CASE "PowerClass Filter LB"
               l_FLBName           = l_FilterObj
               l_FLBName.Enabled   = FALSE
            CASE "PowerClass Filter SLE"
               l_FSLEName          = l_FilterObj
               l_FSLEName.Enabled  = FALSE
            CASE ELSE
         END CHOOSE
      NEXT

      //----------
      //  Restore the SQL statement to the original value as it
      //  may have been modified by pcd_Search.
      //----------

      IF Len(i_StoredProc) = 0 THEN
         l_Select = "DataWindow.Table.Select='"          + &
                     PCCA.MB.fu_QuoteChar(i_SQLSelect, "'") + "'"
         Modify(l_Select)
      END IF

      //----------
      //  Since the DataWindow is closed, it can't be current.
      //----------

      i_Current = FALSE
   ELSE

      //----------
      //  If we are the current DataWindow, we won't be after we
      //  have been closed.  Setting i_InactiveDWColorIdx prevents
      //  pcd_Inactive from changing colors, which we do not care
      //  about since we are closing.
      //----------

      IF i_Active THEN
         i_InactiveDWColorIdx = PCCA.MDI.c_ColorUndefined
         TriggerEvent("pcd_Inactive")
      END IF
   END IF

   //----------
   //  The DataWindow is no longer usable.
   //----------

   i_InUse = FALSE

   //----------
   //  Make sure that Window_CurrentDW does not point to this
   //  DataWindow.
   //----------

   IF PCCA.Window_CurrentDW = THIS THEN
      PCCA.Window_CurrentDW = PCCA.Null_Object
   END IF
END IF
END IF

//----------
//  If l_DoClose is TRUE, then this event has figured out that it
//  needs to close the window that this DataWindow resides on
//----------

i_ExtendMode = FALSE

IF l_DoClose THEN

   //----------
   //  Set PCCA.Do_Event to TRUE so CloseQuery will just close.
   //----------

   PCCA.Do_Event = TRUE
   Close(i_Window)
   RETURN
END IF

//******************************************************************
//  DO NOT EXTEND THIS EVENT!  The window may be gone by the
//  time your extended code begins execution.
//******************************************************************
end on

event pcd_commit;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Commit
//  Description   : Commit the changes to the database.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  If this is an external DataWindow, bypass this operation.
//----------

IF IsNull(i_DBCA) THEN
   GOTO Finished
END IF

//----------
//  Commit the updates made to the database.
//----------

COMMIT USING i_DBCA;

//----------
//  Check for errors that occurred during the COMMIT.
//----------

IF i_DBCA.SQLCode <> 0 THEN
   PCCA.Error = c_Fatal
END IF

Finished:

i_ExtendMode = i_InUse
end event

event pcd_concurrencyerror;g_mb.messagebox("Apice","Un altro utente ha modificato contemporaneamente i dati o la chiave del record è già esistente: eseguire una rilettura dei dati per " + & 
			  "vedere le nuove modifiche, oppure, ripetere il salvataggio per sovrascriverle. ",information!)
PCCA.ERROR = c_valfailed
return -1
end event

on pcd_delete;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Delete
//  Description   : Deletes one or more rows from this DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_DeleteOk,    l_AskOk
INTEGER       l_Answer,      l_Idx
UNSIGNEDLONG  l_MBICode,     l_RefreshCmd
LONG          l_NumSelected, l_SelectedRows[]
DWITEMSTATUS  l_ItemStatus

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Delete, c_Show)

//----------
//  If this DataWindow is not use or is in "QUERY" mode, don't
//  allow deletes.
//----------

IF NOT i_InUse OR i_DWState = c_DWStateQuery THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Assume that we will have to ask the user if it is Ok to
//  delete the row.
//----------

l_AskOk = TRUE

//----------
//  Find the rows that are to be deleted.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  Make sure that deleting is allowed in this window.
//----------

l_DeleteOk = i_AllowDelete

//----------
//  PowerClass allows the user to delete new records that they
//  have inserted even if the DataWindow does not support delete.
//  We check for this case by:
//     a) Making sure that the user is allowed to add rows AND
//     b) Making sure there are New! rows in this DataWindow
//        (i.e. i_DoSetKey is TRUE) AND
//     c) Making sure there is at least one row selected AND
//     d) Making sure that there are not any retrievable
//        rows (i.e. if the rows are retrievable, they are
//        not New!).
//----------

IF i_AllowNew                     THEN
IF i_SharePrimary.i_ShareDoSetKey THEN
IF i_ShowRow > 0                  THEN
IF i_NumSelected = 0              THEN

   l_DeleteOk = TRUE

   //----------
   //  If the New! rows have not been modified, we won't ask the
   //  user about them.
   //----------

   IF Len(GetText()) = 0 OR Describe(GetColumnName() + ".Initial") = GetText() THEN
      l_AskOk = FALSE

      FOR l_Idx = 1 TO l_NumSelected
         l_ItemStatus = &
            GetItemStatus(l_SelectedRows[l_Idx], 0, Primary!)
         IF l_ItemStatus <> New! THEN
            l_AskOk = TRUE
            EXIT
         END IF
      NEXT
   END IF
END IF
END IF
END IF
END IF

//----------
//  If this DataWindow does not allow delete, tell the user and
//  exit the event.
//----------

IF NOT l_DeleteOk THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_DeleteNotAllowed, &
                      0, PCCA.MB.i_MB_Numbers[],         &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDelete, &
                      0, PCCA.MB.i_MB_Numbers[],     &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Make sure that changes have been saved to the database.
//----------

is_EventControl.Check_Cur_Instance = TRUE
is_EventControl.Only_Do_Children   = TRUE
Check_Save(l_RefreshCmd)

//----------
//  If the user canceled or there was an error during the save
//  process, we do not allow the delete to happen.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  If there were changes, Check_Save() may have indicated that
//  the hierarchy of DataWindows need to be refreshed.  Refresh
//  the entire hierarchy except for the children of this
//  DataWindow.  The rows in the children of this DataWindow are
//  going to be deleted so there is not need to refresh them.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd            = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Children_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW_Children       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")
END IF

//----------
//  If there was an error during the refresh, exit the event.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  pcd_Refresh may have retrieved new rows.  Make sure there are
//  still rows to delete.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDeleteAfterSave, &
                      0, PCCA.MB.i_MB_Numbers[],              &
                      5, PCCA.MB.i_MB_Strings[])

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Only_Do_Children = TRUE
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Verify with the user that it is Ok to delete.
//----------

IF l_AskOk THEN
   IF l_NumSelected = 1 THEN
      l_MBICode = PCCA.MB.c_MBI_DW_OneAskDeleteOk
   ELSE
      l_MBICode = PCCA.MB.c_MBI_DW_AskDeleteOk
   END IF

   PCCA.MB.i_MB_Numbers[1] = l_NumSelected
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   l_Answer             = PCCA.MB.fu_MessageBox          &
                             (l_MBICode,              &
                              1, PCCA.MB.i_MB_Numbers[], &
                              5, PCCA.MB.i_MB_Strings[])
ELSE
   l_Answer = 0
END IF

//----------
//  If l_Answer is 0, we have a pure New! row.  Otherwise, if
//  l_Answer is not 1, the user indicated that they do not want
//  to delete the rows.
//----------

IF l_Answer <> 0 AND l_Answer <> 1 THEN

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      is_EventControl.Only_Do_Children = TRUE
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If we get to here, we know that we can delete the rows.  Let
//  Delete_DW_Rows() take care of the work.
//----------

Delete_DW_Rows(l_NumSelected,   l_SelectedRows[], &
               c_IgnoreChanges, c_RefreshChildren)

Finished:

//----------
//  Remove the delete prompt.
//----------

PCCA.MDI.fu_Pop()

i_ExtendMode = i_InUse
end on

on pcd_disable;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Disable
//  Description   : Puts the DataWindow into "VIEW" mode by
//                  disabling all of the columns.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

LONG  l_SaveRow, l_InsertRowNbr

//----------
//  If this window is already disabled, do nothing.
//----------

IF i_DWState = c_DWStateDisabled THEN
   GOTO Finished
END IF

//----------
//  What we are about to do may trigger RFC or validation events.
//  Turn off processing of RFC, validation, VScroll, and redraw.
//----------

i_IgnoreRFC      = (NOT c_IgnoreRFC)
i_IgnoreRFCCount = i_IgnoreRFCCount + 1
i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

i_IgnoreVal      = (NOT c_IgnoreVal)
i_IgnoreValCount = i_IgnoreValCount + 1
i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

i_IgnoreVScroll = (NOT c_IgnoreVScroll)
i_IgnoreVSCount = i_IgnoreVSCount + 1
i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

SetRedraw(c_BufferDraw)
i_RedrawCount = i_RedrawCount + 1
i_RedrawStack[i_RedrawCount] = c_BufferDraw

//----------
//  If we are in "QUERY" mode, make sure that we get out of it.
//----------

IF i_DWState = c_DWStateQuery THEN
   Unset_Query()
END IF

//----------
//  Mark the current DataWindow as disabled.
//----------

i_DWState = c_DWStateDisabled

//----------
//  The following code may cause the row to get reset, so save
//  so that we can restore it.
//----------

l_SaveRow = GetRow()

//----------
//  We need to make sure a row is inserted, otherwise the DDLB
//  arrows will be left.
//----------

IF RowCount() = 0 THEN
   l_InsertRowNbr = InsertRow(0)
END IF

//----------
//  If borders are to be changed in "VIEW" mode, use MODIFY()
//  to blast a string containing the "VIEW" border modes to the
//  DataWindow.
//----------

IF i_ViewModeBorderIdx <> c_DW_BorderUndefined THEN
   Modify(i_ViewBorders)
END IF

//----------
//  If colors are to be changed in "VIEW" mode, use MODIFY()
//  to blast a string containing the "VIEW" colors to the
//  DataWindow.
//----------

IF i_ViewModeColorIdx <> PCCA.MDI.c_ColorUndefined THEN
   Modify(i_ViewColors)
END IF

//----------
//  Use MODIFY() to blast a string to the DataWindow that will
//  set the tab order of all modifiable fields to 0.  The effect
//  of this is to prevent the user from tabbing to or typing into
//  any of the fields.
//----------

Modify(i_DisableTabs)

//----------
//  If we had to insert an empty row, the DataWindow is empty.  Do
//  EMPTY processing.
//----------

IF l_InsertRowNbr > 0 THEN

   //----------
   //  If this is a free-form DataWindow, then leave the
   //  record that was inserted.  This prevents PowerBuilder
   //  from removing the column labels and borders.
   //----------

   IF i_DoEmpty THEN
      i_SharePrimary.i_AddedEmpty = TRUE
   END IF

   i_SharePrimary.Share_State(c_ShareEmpty, i_DoEmpty)

   //----------
   //  We left the dummy row in while we synced the other share
   //  DataWindows.  If the developer does not want it, remove
   //  it now.
   //----------

   IF NOT i_DoEmpty THEN
      DeleteRow(l_InsertRowNbr)
   END IF

   IF i_SharePrimary.i_ShareDoSetKey THEN
      Share_Flags(c_ShareAllDoSetKey, FALSE)
   END IF
END IF

//----------
//  If there was a current row before we blasted changes to the
//  DataWindow, restore it.
//----------

IF l_SaveRow > 0 THEN
   IF l_SaveRow <> GetRow() THEN
      SetRow(l_SaveRow)
   END IF
END IF

//----------
//  Restore processing of RFC, validation, VScroll, and redraw to
//  their previous states.
//----------

IF i_RedrawCount > 1 THEN
   i_RedrawCount = i_RedrawCount - 1
   SetRedraw(i_RedrawStack[i_RedrawCount])
ELSE
   i_RedrawCount = 0
   SetRedraw(TRUE)
END IF

IF i_IgnoreValCount > 1 THEN
   i_IgnoreValCount = i_IgnoreValCount - 1
   i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
ELSE
   i_IgnoreValCount = 0
   i_IgnoreVal      = FALSE
END IF

IF i_IgnoreVSCount > 1 THEN
   i_IgnoreVSCount = i_IgnoreVSCount - 1
   i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
ELSE
   i_IgnoreVSCount = 0
   i_IgnoreVScroll = FALSE
END IF
IF NOT i_IgnoreVScroll THEN
   i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
END IF

IF i_IgnoreRFCCount > 1 THEN
   i_IgnoreRFCCount = i_IgnoreRFCCount - 1
   i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
ELSE
   i_IgnoreRFCCount = 0
   i_IgnoreRFC      = FALSE
END IF

Finished:

i_ExtendMode = i_InUse
end on

on pcd_enable;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Enable
//  Description   : Enable the DataWindow for "NEW" or "MODIFY"
//                  mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

LONG  l_SaveRow

//----------
//  If this DataWindow is already enabled, do nothing.
//----------

IF i_DWState = c_DWStateEnabled THEN
   GOTO Finished
END IF

//----------
//  What we are about to do may trigger RFC or validation events.
//  Turn off processing of RFC, validation, VScroll, and redraw.
//----------

i_IgnoreRFC      = (NOT c_IgnoreRFC)
i_IgnoreRFCCount = i_IgnoreRFCCount + 1
i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

i_IgnoreVScroll = (NOT c_IgnoreVScroll)
i_IgnoreVSCount = i_IgnoreVSCount + 1
i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

i_IgnoreVal      = (NOT c_IgnoreVal)
i_IgnoreValCount = i_IgnoreValCount + 1
i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

SetRedraw(c_BufferDraw)
i_RedrawCount = i_RedrawCount + 1
i_RedrawStack[i_RedrawCount] = c_BufferDraw

//----------
//  If we are in "QUERY" mode, make sure that we get out of it.
//----------

IF i_DWState = c_DWStateQuery THEN
   Unset_Query()
END IF

//----------
//  The following code may cause the row to get reset, so save
//  so that we can restore it.
//----------

l_SaveRow = GetRow()

//----------
//  If borders are to be changed in "VIEW" mode, use MODIFY()
//  to blast a string containing the "MODIFY" border modes to the
//  DataWindow.
//----------

IF i_ViewModeBorderIdx <> c_DW_BorderUndefined THEN
   Modify(i_ModifyBorders)
END IF

//----------
//  If colors are to be changed in "VIEW" mode, use MODIFY()
//  to blast a string containing the "MODIFY" colors to the
//  DataWindow.
//----------

IF i_ViewModeColorIdx <> c_ColorUndefined THEN
   Modify(i_ModifyColors)
END IF

//----------
//  Use MODIFY() to blast a string to the DataWindow that will
//  set the tab order of all modifiable fields back to their
//  original values (i.e. they may have been set to 0 by
//  pcd_Disable).
//----------

Modify(i_EnableTabs)

//----------
//  If there was a current row before we blasted changes to the
//  DataWindow, restore it.
//----------

IF l_SaveRow > 0 THEN
   IF l_SaveRow <> GetRow() THEN
      SetRow(l_SaveRow)
   END IF
END IF

//----------
//  Restore processing of RFC, validation, VScroll, and redraw to
//  their previous states.
//----------

IF i_RedrawCount > 1 THEN
   i_RedrawCount = i_RedrawCount - 1
   SetRedraw(i_RedrawStack[i_RedrawCount])
ELSE
   i_RedrawCount = 0
   SetRedraw(TRUE)
END IF

IF i_IgnoreValCount > 1 THEN
   i_IgnoreValCount = i_IgnoreValCount - 1
   i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
ELSE
   i_IgnoreValCount = 0
   i_IgnoreVal      = FALSE
END IF

IF i_IgnoreVSCount > 1 THEN
   i_IgnoreVSCount = i_IgnoreVSCount - 1
   i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
ELSE
   i_IgnoreVSCount = 0
   i_IgnoreVScroll = FALSE
END IF
IF NOT i_IgnoreVScroll THEN
   i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
END IF

IF i_IgnoreRFCCount > 1 THEN
   i_IgnoreRFCCount = i_IgnoreRFCCount - 1
   i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
ELSE
   i_IgnoreRFCCount = 0
   i_IgnoreRFC      = FALSE
END IF

//----------
//  Mark this DataWindow as enabled.
//----------

i_DWState = c_DWStateEnabled

Finished:

i_ExtendMode = i_InUse
end on

on pcd_filter;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Filter
//  Description   : Tells the current DataWindow to build up a
//                  filter based on non-DataWindow fields and
//                  execute it.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN              l_First,  l_SetFocus
INTEGER              l_Answer, l_Idx
UNSIGNEDLONG         l_RefreshCmd
STRING               l_OldFilter
GRAPHICOBJECT        l_TmpObj
UO_DDDW_FILTER_MAIN  l_DDDWName
UO_DW_FILTER         l_DWName
UO_DDLB_FILTER       l_DDLBName
UO_EM_FILTER         l_EMName
UO_LB_FILTER         l_LBName
UO_SLE_FILTER        l_SLEName
UO_DW_MAIN           l_ToDW, l_ChildDW

//----------
//  Indicate that we are in the pcd_Filter event.
//----------

i_InFilter = i_InFilter + 1

//----------
//  Make sure that this DataWindow is in use.
//----------

IF NOT i_InUse THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If the DataWindow is in "QUERY" mode, then we don't process
//  filter events.
//----------

IF i_DWState = c_DWStateQuery THEN
   Change_DW_Focus(THIS)
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Display the filter prompt.
//----------

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Filter, c_ShowMark)

//----------
//  Make sure changes have been taken care of.  Filter will
//  cause new rows to be loaded and any changes will be clobbered.
//----------

Check_Save(l_RefreshCmd)
IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  Call Load_Rows() to load selections that have been made
//  by the user, but not yet loaded into the children
//  DataWindows.
//----------

IF PCCA.PCMGR.i_DW_NeedRetrieve THEN
   IF Load_Rows(PCCA.Null_Object, PCCA.Null_Object) <> &
      c_Success THEN
      PCCA.Error = c_Fatal
      GOTO Finished
   END IF
END IF

//----------
//  Save the old filter in case we get a validation error or
//  something.
//----------

l_OldFilter = Describe("DataWindow.Table.Filter")

//----------
//  Cycle through all of the filter objects that are wired to
//  this DataWindow to gather additional filter criteria.
//----------

l_First = TRUE
FOR l_Idx = 1 TO i_NumFilterObjects

   //----------
   //  PowerClass supports several different types of filter
   //  objects.  Find out what we have and handle it.
   //----------

   l_TmpObj = i_FilterObjects[l_Idx]
   CHOOSE CASE l_TmpObj.Tag

      CASE "PowerObject Filter DDDW"
         l_DDDWName = l_TmpObj
         PCCA.Error = l_DDDWName.fu_BuildFilter(l_First)
         IF PCCA.Error <> c_Success THEN
            l_DDDWName.SetFocus()
            GOTO Finished
         END IF

      CASE "PowerObject Filter DW"
         l_DWName = l_TmpObj
         PCCA.Error = l_DWName.fu_BuildFilter(l_First)
         IF PCCA.Error <> c_Success THEN
            l_DWName.SetFocus()
            GOTO Finished
         END IF

      CASE "PowerObject Filter DDLB"
         l_DDLBName = l_TmpObj
         PCCA.Error = l_DDLBName.fu_BuildFilter(l_First)
         IF PCCA.Error <> c_Success THEN
            l_DDLBName.SetFocus()
            GOTO Finished
         END IF

      CASE "PowerObject Filter EM"
         l_EMName   = l_TmpObj
         PCCA.Error = l_EMName.fu_BuildFilter(l_First)
         IF PCCA.Error <> c_Success THEN
            l_EMName.SetFocus()
            GOTO Finished
         END IF

      CASE "PowerObject Filter LB"
         l_LBName   = l_TmpObj
         PCCA.Error = l_LBName.fu_BuildFilter(l_First)
         IF PCCA.Error <> c_Success THEN
            l_LBName.SetFocus()
            GOTO Finished
         END IF

      CASE "PowerObject Filter SLE"
         l_SLEName  = l_TmpObj
         PCCA.Error = l_SLEName.fu_BuildFilter(l_First)
         IF PCCA.Error <> c_Success THEN
            l_SLEName.SetFocus()
            GOTO Finished
         END IF
   END CHOOSE

   IF l_First THEN
      l_First = FALSE
   END IF
NEXT

//----------
//  At the start of this event, we checked for changes.  We now
//  see if there is any refreshing that needs to be done because
//  of changes being saved or aborted.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")
END IF

//----------
//  Because we may have just came out of "QUERY" mode, we want
//  current mode to be recalculated.
//----------

i_CurrentMode = c_ModeUndefined

//----------
//  Because our filter has changed, we need to be retrieved.
//----------

PCCA.MDI.fu_SetRedraw(c_BufferDraw)
i_ExecuteFilter = TRUE
Retrieve_DW(c_RetrieveAllDWs, i_xReselectMode, i_xRefreshMode)
i_ExecuteFilter = FALSE
PCCA.MDI.fu_SetRedraw(c_DrawNow)

//----------
//  If the DataWindow is empty after pcd_Refresh and was not put
//  in "NEW" mode by the developer, then no rows were retrieved.
//  Tell the user that they are being too picky.
//----------

IF i_IsEmpty THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Filter"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   l_Answer             = PCCA.MB.fu_MessageBox                &
                             (PCCA.MB.c_MBI_DW_ZeroFilterRows, &
                              0, PCCA.MB.i_MB_Numbers[],       &
                              5, PCCA.MB.i_MB_Strings[])
ELSE
   l_Answer = 0
END IF

//----------
//  If the developer has not set focus, then see if we should
//  try to set it.
//----------

IF NOT PCCA.PCMGR.i_DW_FocusDone THEN

   //----------
   //  If l_Answer is 0, then set focus to the preferred DataWindow
   //  (if any), a child DataWindow, or this DataWindow.  l_Answer
   //  being 0 indicates that we got data.
   //----------

   l_SetFocus = FALSE

   IF l_Answer = 0 THEN

      //----------
      //  If the developer specified a preferred-focus DataWindow,
      //  we want to set focus to it.  There should either be rows
      //  selected or this DataWindow should been requested to go
      //  to "NEW" mode.
      //----------

      IF IsValid(i_FocusDW) THEN
         IF i_FocusDW.i_InUse THEN
            CHOOSE CASE i_RequestedMode
               CASE c_ModeModify
                  IF i_FocusDW.i_AllowModify THEN
                  IF i_ShowRow > 0           THEN
                     l_SetFocus = TRUE
                     l_ToDW     = i_FocusDW
                  END IF
                  END IF
               CASE c_ModeNew
                  IF i_FocusDW.i_AllowNew THEN
                     l_SetFocus = TRUE
                     l_ToDW     = i_FocusDW
                  END IF
               CASE c_ModeView
                  IF i_ShowRow > 0 THEN
                     l_SetFocus = TRUE
                     l_ToDW     = i_FocusDW
                  END IF
            END CHOOSE
         END IF
      END IF

      //----------
      //  If the DataWindow that is to get focus was not set to a
      //  preferred-focus DataWindow, then see if it should be set
      //  to an instance DataWindow.
      //----------

      IF NOT l_SetFocus THEN

         //----------
         //  If there are Instance DataWindows, we will set focus
         //  to the Instance DataWindow that corresponds to the
         //  currently selected or New! row.
         //----------

         IF i_CurInstance > 0 THEN
            l_ChildDW = i_InstanceDW[i_CurInstance]
            CHOOSE CASE i_RequestedMode
               CASE c_ModeModify
                  IF NOT i_AllowModify THEN
                     IF l_ChildDW.i_AllowModify THEN
                        l_SetFocus = TRUE
                        l_ToDW     = l_ChildDW
                     END IF
                  ELSE
                     l_SetFocus = TRUE
                     l_ToDW     = THIS
                  END IF
               CASE c_ModeNew
                  IF NOT i_AllowNew THEN
                     IF l_ChildDW.i_AllowNew THEN
                        l_SetFocus = TRUE
                        l_ToDW     = l_ChildDW
                     END IF
                  ELSE
                     l_SetFocus = TRUE
                     l_ToDW     = THIS
                  END IF
               CASE c_ModeView
                  l_SetFocus = TRUE
                  l_ToDW     = l_ChildDW
            END CHOOSE
         END IF
      END IF

      //----------
      //  If the DataWindow that is to get focus has still not
      //  set, then see if it should be set to one of the normal
      //  children DataWindows.
      //----------

      IF NOT l_SetFocus THEN

         //----------
         //  We search backwards through the child DataWindow
         //  array so that focus will get set to the most
         //  recently created child DataWindow.
         //----------

         FOR l_Idx = i_NumChildren TO 1 STEP -1
            l_ChildDW = i_ChildDW[l_Idx]
            CHOOSE CASE i_RequestedMode
               CASE c_ModeModify
                  IF NOT i_AllowModify THEN
                     IF i_ShowRow > 0           THEN
                     IF l_ChildDW.i_AllowModify THEN
                        l_SetFocus = TRUE
                        l_ToDW     = l_ChildDW
                     END IF
                     END IF
                  ELSE
                     l_SetFocus = TRUE
                     l_ToDW     = THIS
                  END IF
               CASE c_ModeNew
                  IF NOT i_AllowNew THEN
                     IF l_ChildDW.i_AllowNew THEN
                        l_SetFocus = TRUE
                        l_ToDW     = l_ChildDW
                     END IF
                  ELSE
                     l_SetFocus = TRUE
                     l_ToDW     = THIS
                  END IF
               CASE c_ModeView
                  IF i_ShowRow > 0 THEN
                     IF l_ChildDW.i_Window <> i_Window THEN
                        l_SetFocus = TRUE
                        l_ToDW     = l_ChildDW
                     ELSE
                        l_SetFocus = TRUE
                        l_ToDW     = THIS
                     END IF
                  ELSE
                     l_SetFocus = TRUE
                     l_ToDW     = THIS
                  END IF
            END CHOOSE

            IF l_SetFocus THEN
               EXIT
            END IF
         NEXT
      END IF
   END IF

   //----------
   //  If we couldn't find a better choice for the DataWindow to
   //  set focus to, just set it to the DataWindow that received
   //  the pcd_Filter event (i.e. this DataWindow).
   //----------

   IF NOT l_SetFocus THEN
      l_ToDW = THIS
   END IF

   //----------
   //  Make the DataWindow that was our best choice the current
   //  DataWindow.
   //----------

   Set_Auto_Focus(l_ToDW)
END IF

Finished:

//----------
//  If there was an error along the way, make sure the any
//  refresh requested by Check_Save() gets taken care of.
//----------

IF PCCA.Error <> c_Success THEN

   SetFilter(l_OldFilter)

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      i_RootDW.is_EventControl.Refresh_Cmd = l_RefreshCmd
      i_RootDW.TriggerEvent("pcd_Refresh")

      //----------
      //  Make sure that PCCA.Error indicates failure.
      //----------

      PCCA.Error = c_Fatal
   END IF
END IF

//----------
//  Remove the filtering prompt.
//----------

PCCA.MDI.fu_Pop()

//----------
//  Indicate that we are no longer in the pcd_Filter event.
//----------

IF i_InFilter > 1 THEN
   i_InFilter = i_InFilter - 1
ELSE
   i_InFilter = 0
END IF

i_ExtendMode = i_InUse
end on

on pcd_first;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_First
//  Description   : Scroll to the first row in the DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN     l_SetFocus
LONG        l_RowCount
UO_DW_MAIN  l_ToDW

//----------
//  Make sure that this DataWindow is in use.
//----------

IF NOT i_InUse THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If the DataWindow is in "QUERY" mode, then we don't process
//  scroll events.
//----------

IF i_DWState = c_DWStateQuery THEN
   Change_DW_Focus(THIS)
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Indicate to the user that we are processing the sroll.
//----------

SetPointer(HourGlass!)
PCCA.Error = c_Success

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_ScrollFirst, c_Show)

//----------
//  We need to scroll to the first record.  However, we can only
//  do this if the DataWindow to be scrolled has rows in it.
//----------

l_SetFocus = FALSE
l_RowCount = i_ScrollDW.RowCount()
IF l_RowCount > 0 THEN

   //----------
   //  We use the i_MoveRow instance variable to tell the
   //  RowFocusChanged! event what row we want to scroll to.
   //  By setting i_MoveRow to a negative row number, we are
   //  telling RowFocusChanged! to scroll to that row AND
   //  select it.
   //----------

   i_ScrollDW.i_MoveRow = - 1
   i_ScrollDW.TriggerEvent(RowFocusChanged!)

   //----------
   //  If the scroll event was successful, set the focus to the
   //  preferred-focus DataWindow.  If there is not a preferred-
   //  focus DataWindow, then set focus to this DataWindow (i.e.
   //  the DataWindow that received the scroll event).
   //----------

   IF PCCA.Error = c_Success THEN
      IF IsValid(i_FocusDW) THEN
         IF i_FocusDW.i_InUse THEN
            l_SetFocus = TRUE
            l_ToDW     = i_FocusDW
         END IF
      END IF
   END IF
END IF

//----------
//  If focus has not been set by the developer, set it.
//----------

IF NOT PCCA.PCMGR.i_DW_FocusDone THEN

   //----------
   //  If we couldn't find a better choice for the DataWindow to
   //  set focus to, just set it to the DataWindow that received
   //  the scroll event (i.e. this DataWindow).
   //----------

   IF NOT l_SetFocus THEN
      l_ToDW = THIS
   END IF

   //----------
   //  Make the DataWindow that was our best choice the
   //  current DataWindow.
   //----------

   Set_Auto_Focus(l_ToDW)
END IF

PCCA.MDI.fu_Pop()

Finished:

i_ExtendMode = i_InUse
end on

on pcd_inactive;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Inactive
//  Description   : Indicate that this DataWindow is inactive.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_HighlightOnly

//----------
//  Assume that we are not losing our status as the current
//  DataWindow.
//----------

i_LostCurrent = FALSE

IF NOT i_InUse THEN
   GOTO Finished
END IF

//----------
//  Find out if we are being called only to un-highlight this
//  DataWindow and reset the Event Control structure.
//----------

l_HighlightOnly = is_EventControl.Highlight_Only
is_EventControl = is_ResetControl

//----------
//  If this window is already inactive and we are not performing
//  un-highlighting, do nothing.
//----------

IF (NOT i_Active AND NOT l_HighlightOnly) THEN
   GOTO Finished
END IF

//----------
//  If we get to here, we are either the current DataWindow or
//  we are just being triggered for the highlight effect.  If we
//  are not just doing highlighting (e.g. for pcd_AnyChanges),
//  then we are losing our status as the current DataWindow
//----------

IF NOT l_HighlightOnly THEN

  //----------
  //  Make sure that we are the current DataWindow as
  //  indicated by PCCA.Window_CurrentDW.
  //----------

   IF PCCA.Window_CurrentDW = THIS THEN
      PCCA.Window_CurrentDW = PCCA.Null_Object
   END IF
END IF

//----------
//  Change the colors in the DataWindow to the specified
//  inactive color.
//----------

IF i_InactiveDWColorIdx <> c_ColorUndefined THEN
   SetRedraw(c_BufferDraw)
   i_RedrawCount = i_RedrawCount + 1
   i_RedrawStack[i_RedrawCount] = c_BufferDraw

   IF i_IsEmpty AND i_DWState <> c_DWStateQuery THEN
      Modify(i_InactiveEmptyColors)
   ELSE
      Modify(i_InactiveNonEmptyColors)
   END IF

   IF i_RedrawCount > 1 THEN
      i_RedrawCount = i_RedrawCount - 1
      SetRedraw(i_RedrawStack[i_RedrawCount])
   ELSE
      i_RedrawCount = 0
      SetRedraw(TRUE)
   END IF
END IF

//----------
//  If this event was not triggered for its un-highlight effect,
//  then indicate that this DataWindow just lost its status as
//  the current DataWindow.
//----------

IF NOT l_HighlightOnly THEN
   i_Active      = FALSE
   i_LostCurrent = TRUE
END IF

Finished:

i_ExtendMode = i_LostCurrent
end on

on pcd_insert;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Insert
//  Description   : Change to "NEW" mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Trigger the pcd_New event telling it to do an insert.  By
//  default it will do an append.
//----------

TriggerEvent("pcd_New", c_NewInsert, c_DoNew)
end on

on pcd_last;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Last
//  Description   : Scroll to the last row in the DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN     l_SetFocus
LONG        l_RowCount
UO_DW_MAIN  l_ToDW

//----------
//  Make sure that this DataWindow is in use.
//----------

IF NOT i_InUse THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If the DataWindow is in "QUERY" mode, then we don't process
//  scroll events.
//----------

IF i_DWState = c_DWStateQuery THEN
   Change_DW_Focus(THIS)
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Indicate to the user that we are processing the sroll.
//----------

SetPointer(HourGlass!)
PCCA.Error = c_Success

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_ScrollLast, c_Show)

//----------
//  We need to scroll to the last record.  However, we can only
//  do this if the DataWindow to be scrolled has rows in it.
//----------

l_SetFocus = FALSE
l_RowCount = i_ScrollDW.RowCount()
IF l_RowCount > 0 THEN

   //----------
   //  We use the i_MoveRow instance variable to tell the
   //  RowFocusChanged! event what row we want to scroll to.
   //  By setting i_MoveRow to a negative row number, we are
   //  telling RowFocusChanged! to scroll to that row AND
   //  select it.
   //----------

   i_ScrollDW.i_MoveRow = - l_RowCount
   i_ScrollDW.TriggerEvent(RowFocusChanged!)

   //----------
   //  If the scroll event was successful, set the focus to the
   //  preferred-focus DataWindow.  If there is not a preferred-
   //  focus DataWindow, then set focus to this DataWindow (i.e.
   //  the DataWindow that received the scroll event).
   //----------

   IF PCCA.Error = c_Success THEN
      IF IsValid(i_FocusDW) THEN
         IF i_FocusDW.i_InUse THEN
            l_SetFocus = TRUE
            l_ToDW     = i_FocusDW
         END IF
      END IF
   END IF
END IF

//----------
//  If focus has not been set by the developer, set it.
//----------

IF NOT PCCA.PCMGR.i_DW_FocusDone THEN

   //----------
   //  If we couldn't find a better choice for the DataWindow to
   //  set focus to, just set it to the DataWindow that received
   //  the scroll event (i.e. this DataWindow).
   //----------

   IF NOT l_SetFocus THEN
      l_ToDW = THIS
   END IF

   //----------
   //  Make the DataWindow that was our best choice the
   //  current DataWindow.
   //----------

   Set_Auto_Focus(l_ToDW)
END IF

PCCA.MDI.fu_Pop()

Finished:

i_ExtendMode = i_InUse
end on

on pcd_modify;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Modify
//  Description   : Change to "MODIFY" mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN     l_SetFocus
INTEGER     l_ColNbr,   l_Idx
UO_DW_MAIN  l_ToDW,     l_ChildDW

//----------
//  If we were not triggered by pcd_Refresh, do normal processing.
//----------

IF NOT i_ProcessMode THEN

   //----------
   //  Make sure that this DataWindow is in use.
   //----------

   IF NOT i_InUse THEN
      PCCA.Error = c_Fatal
      GOTO Finished
   END IF

   //----------
   //  If the DataWindow is in "QUERY" mode, then we don't process
   //  mode-change events.
   //----------

   IF i_DWState = c_DWStateQuery THEN
      Change_DW_Focus(THIS)
      PCCA.Error = c_Fatal
      GOTO Finished
   END IF

   //----------
   //  Indicate to the user that we are processing the mode change.
   //----------

   SetPointer(HourGlass!)
   PCCA.Error = c_Success

   PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Modify, c_Show)

   //----------
   //  Call Load_Rows() to load selections that have been made
   //  by the user, but not yet loaded into the children
   //  DataWindows.
   //----------

   IF PCCA.PCMGR.i_DW_NeedRetrieve THEN
      IF Load_Rows(PCCA.Null_Object, THIS) <> c_Success THEN
         PCCA.MDI.fu_Pop()
         PCCA.Error = c_Fatal
         GOTO Finished
      END IF
   END IF

   //----------
   //  We need to insure that what the user has typed (if anything)
   //  into the current field is valid input.  Set up the Event
   //  Control structure to tell pcd_AnyChanges to perform this
   //  checking for us (note: pcd_AnyChanges accomplishes this by
   //  doing an AcceptText()).
   //----------

   is_EventControl.Check_Accept = TRUE

   //----------
   //  If this DataWindow does not allow "MODIFY" mode, then we
   //  only need to check the children DataWindows for invalid
   //  input.
   //----------

   IF NOT i_AllowModify THEN
      is_EventControl.Only_Do_Children = TRUE
   END IF

   //----------
   //  Trigger pcd_AnyChanges to do the validation checking.
   //----------

   TriggerEvent("pcd_AnyChanges")

   //----------
   //  If there are not any validation errors, go to "MODIFY" mode.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  Set up the Event Control structure to tell pcd_Refresh
      //  that it needs to change this DataWindow and its children
      //  DataWindows to "MODIFY" mode.
      //----------

      is_EventControl.Refresh_Cmd = c_RefreshModify
      TriggerEvent("pcd_Refresh")

      //----------
      //  If pcd_Refresh successfully changed this DataWindow and
      //  its children DataWindows to "MODIFY" mode, then we want
      //  to set focus.  PCCA.PCMGR.i_DW_FocusDone tells us if
      //  the developer already has taken care of focus.
      //----------

      IF NOT PCCA.PCMGR.i_DW_FocusDone THEN
      IF PCCA.Error = c_Success        THEN

         //----------
         //  If the developer specified a preferred-focus
         //  DataWindow, and there are rows to modify, then
         //  we want to set focus to it.
         //----------

         l_SetFocus = FALSE

         IF i_ShowRow > 0 THEN
            IF IsValid(i_FocusDW) THEN
               IF i_FocusDW.i_AllowModify THEN
               IF i_FocusDW.i_InUse       THEN
                  l_SetFocus = TRUE
                  l_ToDW     = i_FocusDW
               END IF
               END IF
            END IF
         END IF

         IF NOT l_SetFocus THEN

            //----------
            //  If this DataWindow does not allow "MODIFY" mode,
            //  then we want to set focus to one of the children
            //  DataWindows that do allow "MODIFY".
            //----------

            IF NOT i_AllowModify THEN

               //----------
               //  If there are Instance DataWindows, we will
               //  set focus to the Instance DataWindow that
               //  corresponds to the currently selected row.
               //----------

               IF i_CurInstance > 0 THEN
                  l_ChildDW = i_InstanceDW[i_CurInstance]
                  IF l_ChildDW.i_AllowModify THEN
                     l_SetFocus = TRUE
                     l_ToDW     = l_ChildDW
                  END IF
               END IF

               //----------
               //  If the DataWindow that is to get focus was not
               //  set to one of the Instance DataWindows, then
               //  see if it should be set to one of the normal
               //  children DataWindows.
               //----------

               IF NOT l_SetFocus THEN

                  //----------
                  //  If there are normal children DataWindows
                  //  and there is a row selected (i.e. the
                  //  children DataWindows contain data), then
                  //  try to find a DataWindow that allows
                  //  "MODIFY" mode.  We search backwards
                  //  through the child DataWindow array so that
                  //  focus will get set to the most recently
                  //  created child DataWindow.
                  //----------

                  IF i_ShowRow > 0 THEN
                     FOR l_Idx = i_NumChildren TO 1 STEP -1
                        l_ChildDW = i_ChildDW[l_Idx]
                        IF l_ChildDW.i_AllowModify THEN
                           l_SetFocus = TRUE
                           l_ToDW     = l_ChildDW
                           EXIT
                        END IF
                     NEXT
                  END IF
               END IF
            END IF
         END IF

         //----------
         //  If we couldn't find a better choice for the DataWindow
         //  to set focus to, just set it to the DataWindow that
         //  received the pcd_Modify event (i.e. this DataWindow).
         //----------

         IF NOT l_SetFocus THEN
            l_ToDW = THIS
         END IF

         //----------
         //  Make the DataWindow that was our best choice the
         //  current DataWindow.
         //----------

         Set_Auto_Focus(l_ToDW)
      END IF
      END IF
   END IF

   //----------
   //  Remove the "MODIFY" mode prompt.
   //----------

   PCCA.MDI.fu_Pop()
END IF

Finished:

//----------
//  Set the flag to let the developer know if they should execute
//  their extended code at this time.
//----------

i_ExtendMode = i_ProcessMode
end on

on pcd_new;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_New
//  Description   : Change to "NEW" mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  pcd_New can insert a new row or append a new row, depending
//  on what option it is triggered with.  Grab this option from
//  the message structure before it gets lost.
//----------

UNSIGNEDINT  l_wParam
LONG         l_lParam

l_wParam = Message.WordParm
l_lParam = Message.LongParm

//----------

BOOLEAN       l_SetFocus
INTEGER       l_Idx
UNSIGNEDLONG  l_RefreshCmd
LONG          l_InsertRowNbr
UO_DW_MAIN    l_ToDW, l_ChildDW

//----------
//  If we were not triggered by pcd_Refresh, do normal processing.
//----------

IF NOT i_ProcessMode THEN

   //----------
   //  Make sure that this DataWindow is in use.
   //----------

   IF NOT i_InUse THEN
      PCCA.Error = c_Fatal
      GOTO Finished
   END IF

   //----------
   //  If this is an instance then trigger the same event on the
   //  parent so a new window is opened.
   //----------

   IF i_IsInstance THEN
      i_ParentDW.TriggerEvent("pcd_New")
      GOTO Finished
   END IF

   //----------
   //  If the DataWindow is in "QUERY" mode, then we don't process
   //  mode-change events.
   //----------

   IF i_DWState = c_DWStateQuery THEN
      Change_DW_Focus(THIS)
      PCCA.Error = c_Fatal
      GOTO Finished
   END IF

   //----------
   //  If the options passed to this event are not valid, then
   //  assume that we are doing an append.
   //----------

   IF (l_wParam <> c_NewAppend  AND l_wParam <> c_NewInsert) OR  &
      l_lParam <> c_DoNew THEN
      l_wParam = c_NewAppend
   END IF

   //----------
   //  Indicate to the user that we are processing the mode change.
   //----------

   SetPointer(HourGlass!)
   PCCA.Error = c_Success

   //----------
   //  Initialize l_RefreshCmd to an undefined refresh mode.
   //  Check_Save() will set it to a valid mode if a refresh needs
   //  to be done because of changes the user has made in the
   //  DataWindow hierarchy.
   //----------

   l_RefreshCmd = c_RefreshUndefined

   //----------
   //  Depending on how this event was triggered, we will either
   //  insert a new record or append a new record.
   //----------

   IF l_wParam = c_NewAppend THEN
      PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Append, c_Show)
   ELSE
      PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Insert, c_Show)
   END IF

   //----------
   //  If we were told to insert a new row, set the insert postion
   //  to the cursor row.  Otherwise set it to 0 to indicate that
   //  the new row should be appended.  The insert postion is
   //  is used by the pcd_Refresh event when it inserts the row.
   //----------

   IF l_wParam = c_NewInsert AND i_AllowNew THEN
      l_InsertRowNbr = i_CursorRow
   ELSE
      l_InsertRowNbr = 0
   END IF

   //----------
   //  Turn refresh off for this DataWindow while the new happens.
   //----------

   IF i_AllowNew THEN
      SetRedraw(c_BufferDraw)
      i_RedrawCount = i_RedrawCount + 1
      i_RedrawStack[i_RedrawCount] = c_BufferDraw
   END IF

   //----------
   //  Call Load_Rows() to load selections that have been made
   //  by the user, but not yet loaded into the children
   //  DataWindows.
   //----------

   IF PCCA.PCMGR.i_DW_NeedRetrieve THEN
      IF Load_Rows(PCCA.Null_Object, PCCA.Null_Object) <> &
         c_Success THEN

         //----------
         //  Restore redraw if we changed it.
         //----------

         IF i_AllowNew THEN
            IF i_RedrawCount > 1 THEN
               i_RedrawCount = i_RedrawCount - 1
               SetRedraw(i_RedrawStack[i_RedrawCount])
            ELSE
               i_RedrawCount = 0
               SetRedraw(TRUE)
            END IF
         END IF

         PCCA.MDI.fu_Pop()

         PCCA.Error = c_Fatal
         GOTO Finished
      END IF
   END IF

   //----------
   //  Changing a DataWindow to "NEW" mode will cause all of
   //  its children DataWindows to cleared.  Therefore, we must
   //  check for changes before we go to "NEW" mode.  If the
   //  DataWindow that receives this event does not allow
   //  "NEW", then the DataWindow will place its children
   //  DataWindows into "NEW" mode.  If this DataWindow allows
   //  "NEW" mode, we need to check its children DataWindows
   //  for changes.  However, if it does not allow "NEW" mode,
   //  it is going to tell its children DataWindows to go into
   //  "NEW" mode.  In that case, we need to check the
   //  children's children DataWindows for changes.
   //----------

   IF NOT i_AllowNew THEN

      //----------
      //  This DataWindow does not allow "NEW" mode so it is
      //  going to put its children DataWindows into "NEW" mode.
      //  Therefore, we must cycle through all of the children
      //  DataWindows to see if their children DataWindows have
      //  changes.
      //----------

      FOR l_Idx = 1 TO i_NumChildren
         l_ChildDW = i_ChildDW[l_Idx]
         l_ChildDW.i_CurInstance                      = 0
         l_ChildDW.is_EventControl.Check_Cur_Instance = TRUE
         l_ChildDW.is_EventControl.Only_Do_Children   = TRUE
         l_ChildDW.Check_Save(l_RefreshCmd)
         IF PCCA.Error <> c_Success THEN
            EXIT
         END IF
      NEXT
   ELSE

      //----------
      //  This is the easy case.  This DataWindow does allow
      //  "NEW" mode so we only have to check the children
      //  DataWindows for changes.
      //----------

      i_CurInstance                      = 0
      is_EventControl.Check_Cur_Instance = TRUE
      is_EventControl.Only_Do_Children   = TRUE
      Check_Save(l_RefreshCmd)
   END IF

   //----------
   //  If we did not encounter any errors, keep trying to go
   //  "NEW" mode.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  If there already New! rows, make sure that it is Ok
      //  to insert another one.
      //----------

      IF NOT i_AllowNew THEN
         FOR l_Idx = 1 TO i_NumChildren
            l_ChildDW = i_ChildDW[l_Idx]
            IF l_ChildDW.i_DoSetKey AND &
               l_ChildDW.i_OnlyOneNewRow THEN
               PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_New"
               PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
               PCCA.MB.i_MB_Strings[3] = i_ClassName
               PCCA.MB.i_MB_Strings[4] = i_Window.Title
               PCCA.MB.i_MB_Strings[5] = l_ChildDW.DataObject
               PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_OnlyOneNew, &
                                  0, PCCA.MB.i_MB_Numbers[],   &
                                  5, PCCA.MB.i_MB_Strings[])
               PCCA.Error = c_Fatal
            END IF
            IF PCCA.Error <> c_Success THEN
               EXIT
            END IF
         NEXT
      ELSE
         IF i_DoSetKey AND i_OnlyOneNewRow THEN
            PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_New"
            PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
            PCCA.MB.i_MB_Strings[3] = i_ClassName
            PCCA.MB.i_MB_Strings[4] = i_Window.Title
            PCCA.MB.i_MB_Strings[5] = DataObject
            PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_OnlyOneNew, &
                               0, PCCA.MB.i_MB_Numbers[],   &
                               5, PCCA.MB.i_MB_Strings[])
            PCCA.Error = c_Fatal
         END IF
      END IF
   END IF

   //----------
   //  If it is Ok to add a New! row, keep trying to go to "NEW"
   //  mode.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  We need to insure that what the user has typed (if
      //  anything) into the current field is valid input.  Set
      //  up the Event Control structure to tell pcd_AnyChanges
      //  to perform this checking for us (note: pcd_AnyChanges
      //  accomplishes this by doing an AcceptText()).
      //----------

      is_EventControl.Check_Accept = TRUE

      //----------
      //  If this DataWindow does not allow "NEW" mode, then we
      //  only need to check the children DataWindows for invalid
      //  input.
      //----------

      IF NOT i_AllowNew THEN
         is_EventControl.Only_Do_Children = TRUE
      END IF

      //----------
      //  Trigger pcd_AnyChanges to do the validation checking.
      //----------

      TriggerEvent("pcd_AnyChanges")
   END IF

   //----------
   //  If the validation test was successful, we can now go to
   //  "NEW" mode.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  If l_RefreshCmd is a valid refresh mode, then the
      //  DataWindow hierarchy needs to be refreshed because a
      //  save or an abort changes happened.  Depending on which
      //  DataWindow(s) are going to be placed into "NEW" mode, we
      //  tell pcd_Refresh to skip either the branch starting with
      //  this DataWindow or all of the branches starting with this
      //  DataWindow's children DataWindows.
      //----------

      IF l_RefreshCmd <> c_RefreshUndefined THEN

         //----------
         //  Set up the Event Control structure for pcd_Refresh.
         //  Tell pcd_Refresh to use the refresh mode that was
         //  specified by Check_Save().
         //----------

         i_RootDW.is_EventControl.Refresh_Cmd = l_RefreshCmd

         //----------
         //  If this DataWindow does not allow "NEW", the tell
         //  pcd_Refresh to refresh the DataWindow hierarchy up to
         //  and including this DataWindow, but not its children
         //  DataWindows.  They will be refreshed later.
         //----------

         IF NOT i_AllowNew THEN
            i_RootDW.is_EventControl.Skip_DW_Children_Valid = TRUE
            i_RootDW.is_EventControl.Skip_DW_Children       = THIS
         ELSE

            //----------
            //  This DataWindow does allow "NEW".  Tell pcd_Refresh
            //  to refresh the DataWindow hierarchy, except for
            //  this branch.  It will be refreshed later.
            //----------

            i_RootDW.is_EventControl.Skip_DW_Valid = TRUE
            i_RootDW.is_EventControl.Skip_DW       = THIS
         END IF

         //----------
         //  Trigger the refresh of the DataWindow hierarchy.
         //----------

         i_RootDW.TriggerEvent("pcd_Refresh")
      END IF

      //----------
      //  If the refresh was successful, we can place the
      //  DataWindow(s) into "NEW" mode.
      //----------

      IF PCCA.Error = c_Success THEN

         //----------
         //  Set up the Event Control structure for pcd_Refresh.
         //  Tell pcd_Refresh to use the "NEW" refresh mode.
         //----------

         is_EventControl.Refresh_Cmd    = c_RefreshNew
         is_EventControl.Insert_Row_Nbr = l_InsertRowNbr

         //----------
         //  We tell pcd_Refresh where in this DataWindow's branch
         //  to start changing DataWindow(s) to "NEW" mode.
         //  pcd_Refresh will only change DataWindows at level 1 to
         //  "NEW" mode.  Therefore, if this DataWindow does not
         //  allow "NEW", set its level to 0, so that its children
         //  DataWindows will be at level 1.
         //----------

         IF NOT i_AllowNew THEN
            is_EventControl.Level = 0
         ELSE
            is_EventControl.Level = 1
         END IF

         //----------
         //  Trigger pcd_Refresh to put the DataWindow(s) into
         //  "NEW" mode.
         //----------

         TriggerEvent("pcd_Refresh")
      END IF
   ELSE

      //----------
      //  We must have encountered an error along the way.  If
      //  Check_Save() indicated that the DataWindow hierarchy
      //  needed refreshing, we take care of it here.
      //----------

      IF l_RefreshCmd <> c_RefreshUndefined THEN
         i_RootDW.is_EventControl.Refresh_Cmd = l_RefreshCmd
         i_RootDW.TriggerEvent("pcd_Refresh")
      END IF

      //----------
      //  Make sure that PCCA.Error indicates that we failed.
      //----------

      PCCA.Error = c_Fatal
   END IF

   //----------
   //  Restore redraw.
   //----------

   IF i_AllowNew THEN
      IF i_RedrawCount > 1 THEN
         i_RedrawCount = i_RedrawCount - 1
         SetRedraw(i_RedrawStack[i_RedrawCount])
      ELSE
         i_RedrawCount = 0
         SetRedraw(TRUE)
      END IF
   END IF

   //----------
   //  If pcd_Refresh successfully changed this DataWindow and
   //  its children DataWindows to "NEW" mode, then we want
   //  to set focus.  PCCA.PCMGR.i_DW_FocusDone tells us if
   //  the developer already has taken care of focus.
   //----------

   IF NOT PCCA.PCMGR.i_DW_FocusDone THEN
   IF PCCA.Error = c_Success        THEN

      //----------
      //  If the developer specified a preferred-focus DataWindow,
      //  we want to set focus to it.
      //----------

      l_SetFocus = FALSE

      IF IsValid(i_FocusDW) THEN
         IF i_FocusDW.i_AllowNew THEN
         IF i_FocusDW.i_InUse    THEN
            l_SetFocus = TRUE
            l_ToDW     = i_FocusDW
         END IF
         END IF
      END IF

      IF NOT l_SetFocus THEN

         //----------
         //  If this DataWindow does not allow "NEW" mode, then
         //  we want to set focus to one of the children
         //  DataWindows that do allow "NEW".
         //----------

         IF NOT i_AllowNew THEN

            //----------
            //  If there are Instance DataWindows, we will set
            //  focus to the Instance DataWindow that corresponds
            //  to the new row.
            //----------

            IF i_CurInstance > 0 THEN
               l_ChildDW = i_InstanceDW[i_CurInstance]
               IF l_ChildDW.i_AllowNew THEN
                  l_SetFocus = TRUE
                  l_ToDW     = l_ChildDW
               END IF
            END IF

            //----------
            //  If the DataWindow that is to get focus was not set
            //  to one of the Instance DataWindows, then see if it
            //  should be set to one of the normal children
            //  DataWindows.
            //----------

            IF NOT l_SetFocus THEN

               //----------
               //  If there are normal children DataWindows, then
               //  try to find a DataWindow that allows "NEW"
               //  mode.  We search backwards through the child
               //  DataWindow array so that focus will get set
               //  to the most recently created child DataWindow.
               //----------

               FOR l_Idx = i_NumChildren TO 1 STEP -1
                  l_ChildDW = i_ChildDW[l_Idx]
                  IF l_ChildDW.i_AllowNew THEN
                     l_SetFocus = TRUE
                     l_ToDW     = l_ChildDW
                     EXIT
                  END IF
               NEXT
            END IF
         END IF
      END IF

      //----------
      //  If we couldn't find a better choice for the DataWindow
      //  to set focus to, just set it to the DataWindow that
      //  received the pcd_New event (i.e. this DataWindow).
      //----------

      IF NOT l_SetFocus THEN
         l_ToDW = THIS
      END IF

      //----------
      //  Make the DataWindow that was our best choice the
      //  current DataWindow.
      //----------

      Set_Auto_Focus(l_ToDW)
   END IF
   END IF

   //----------
   //  Remove the "NEW" mode prompt.
   //----------

   PCCA.MDI.fu_Pop()
END IF

Finished:

//----------
//  Set the flag to let the developer know if they should execute
//  their extended code at this time.
//----------

i_ExtendMode = i_ProcessMode
end on

on pcd_next;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Next
//  Description   : Scroll to the next row in the DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN     l_SetFocus
LONG        l_RowCount, l_NextRow
UO_DW_MAIN  l_ToDW

//----------
//  Make sure that this DataWindow is in use.
//----------

IF NOT i_InUse THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If the DataWindow is in "QUERY" mode, then we don't process
//  scroll events.
//----------

IF i_DWState = c_DWStateQuery THEN
   Change_DW_Focus(THIS)
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Indicate to the user that we are processing the sroll.
//----------

SetPointer(HourGlass!)
PCCA.Error = c_Success

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_ScrollNext, c_Show)

//----------
//  We need to scroll to the next record.  However, we can only
//  do this if the DataWindow to be scrolled has rows in it.
//----------

l_SetFocus = FALSE
l_RowCount = i_ScrollDW.RowCount()
IF l_RowCount > 0 THEN

   //----------
   //  Determine the current cursor row and get ready to scroll
   //  to the next row.
   //----------

   l_NextRow = i_ScrollDW.GetRow() + 1
   IF l_NextRow > l_RowCount THEN
      l_NextRow = l_RowCount
   END IF

   //----------
   //  We use the i_MoveRow instance variable to tell the
   //  RowFocusChanged! event what row we want to scroll to.
   //  By setting i_MoveRow to a negative row number, we are
   //  telling RowFocusChanged! to scroll to that row AND
   //  select it.
   //----------

   i_ScrollDW.i_MoveRow = - l_NextRow
   i_ScrollDW.TriggerEvent(RowFocusChanged!)

   //----------
   //  If the scroll event was successful, set the focus to the
   //  preferred-focus DataWindow.  If there is not a preferred-
   //  focus DataWindow, then set focus to this DataWindow (i.e.
   //  the DataWindow that received the scroll event).
   //----------

   IF PCCA.Error = c_Success THEN
      IF IsValid(i_FocusDW) THEN
         IF i_FocusDW.i_InUse THEN
            l_SetFocus = TRUE
            l_ToDW     = i_FocusDW
         END IF
      END IF
   END IF
END IF

//----------
//  If focus has not been set by the developer, set it.
//----------

IF NOT PCCA.PCMGR.i_DW_FocusDone THEN

   //----------
   //  If we couldn't find a better choice for the DataWindow to
   //  set focus to, just set it to the DataWindow that received
   //  the scroll event (i.e. this DataWindow).
   //----------

   IF NOT l_SetFocus THEN
      l_ToDW = THIS
   END IF

   //----------
   //  Make the DataWindow that was our best choice the current
   //  DataWindow.
   //----------

   Set_Auto_Focus(l_ToDW)
END IF

PCCA.MDI.fu_Pop()

Finished:

i_ExtendMode = i_InUse
end on

on pcd_pcccerror;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_PCCCError
//  Description   : Process a database concurrency error and
//                  sets up information for the developer and/or
//                  user.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_Found,          l_IsSame,     l_CCDBError
BOOLEAN       l_RetrySave,      l_Rollback,   l_DisplayError
INTEGER       l_SaveColumn,     l_Error,      l_Check
LONG          l_SaveRow,        l_FixRow,     l_MBID
LONG          l_TmpRow,         l_Idx
LONG          l_DBErrorCode,    l_DBErrorRow
LONG          l_NumOldSelected, l_OldSelected[]
REAL          l_MBNumbers[]
STRING        l_CreateLine,     l_Describe,   l_Modify, l_Result
STRING        l_DBErrorMessage, l_DevDBErrorMessage
STRING        l_KeyArray[],     l_MBStrings[]
DWBUFFER      l_DBErrorBuffer
DWITEMSTATUS  l_RowStatus
WINDOWSTATE   l_WinState
UO_DW_MAIN    l_CurDW

//----------
//  PowerBuilder can't skip statements with SQL bind variables.
//  Therefore, we always have to rollback the entire transaction
//  and start again.
//----------

Do_CC_Rollback()

//----------
//  If there was an error, make sure that our local error reflects
//  it.
//----------

IF PCCA.Error <> c_Success THEN
   RETURN
END IF

//----------
//  Since we rolled everything back, we will need to retry the
//  save in its entirety.
//----------

l_RetrySave = TRUE

//----------
//  pcd_Save will call Get_Col_Info() to make sure that we have
//  the column information.  No need to do it again.
//----------

//----------
//  Bump the concurrency error counts.  These are for
//  informational purposes only.
//----------

IF i_CCLastBadRow[i_CCWhichUpdate] = 0 THEN
   i_CCNumErrors = i_CCNumErrors + 1
END IF
IF i_CCLastBadRow[i_CCWhichUpdate] <> i_DBErrorRow THEN
   i_CCNumRows = i_CCNumRows + 1
END IF

//----------
//  Initialize l_RetrySave to indicate that this DataWindow
//  is not forcing a rollback and retrying the save of the
//  entire hierarchy of DataWindows.
//----------

l_RetrySave = FALSE

//----------
//  Make sure redraw is off while we mess with the fields on the
//  DataWindow.
//----------

SetRedraw(c_BufferDraw)
i_RedrawCount = i_RedrawCount + 1
i_RedrawStack[i_RedrawCount] = c_BufferDraw

//----------
//  Remember the currently selected rows because things may get
//  a little shuffled.
//----------

l_NumOldSelected = Save_Selected_Rows(l_OldSelected[], l_KeyArray[])

//----------
//  Save the current row and column.  Turn off RFC and scroll
//  processing because we may be moving rows between buffers
//  and/or inserting and deleting rows.
//----------

l_SaveRow    = i_CursorRow
l_SaveColumn = GetColumn()

i_IgnoreRFC      = (NOT c_IgnoreRFC)
i_IgnoreRFCCount = i_IgnoreRFCCount + 1
i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

i_IgnoreVScroll = (NOT c_IgnoreVScroll)
i_IgnoreVSCount = i_IgnoreVSCount + 1
i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

//----------
//  Turn off validation processing because we may be stuffing
//  data values into the DataWindow.
//----------

i_IgnoreVal      = (NOT c_IgnoreVal)
i_IgnoreValCount = i_IgnoreValCount + 1
i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

//----------
//  Get the status of the row with the update error.
//----------

l_RowStatus = GetItemStatus(i_DBErrorRow, 0, i_DBErrorBuffer)

//----------
//  Stuff the i_CCInfo structure with the necessary information
//  for the developer to deal with the concurrency error.  Also,
//  set up default actions in i_CCStatus[] structure in case
//  the developer does not code the pcd_ConcurrencyError event.
//----------

FOR l_Idx = 1 TO i_NumColumns
   Get_Val_Info(l_Idx)

   i_CCInfo[l_Idx].Column_Name    = i_xColumnNames[l_Idx]

   l_Describe                     = "#" + String(l_Idx) + ".Update"
   l_Result                       = Upper(Describe(l_Describe))
   i_CCInfo[l_Idx].Is_Updatable   = (l_Result = "YES")

   i_CCInfo[l_Idx].Error_Code     = c_CCErrorNone

   i_CCInfo[l_Idx].Row_Status     = l_RowStatus

   i_CCInfo[l_Idx].Column_Status  = &
      GetItemStatus(i_DBErrorRow, l_Idx, i_DBErrorBuffer)

   i_CCInfo[l_Idx].Old_DB_Value   =           &
      Get_Col_Data_Ex(i_DBErrorRow,    l_Idx, &
                      i_DBErrorBuffer, c_GetOriginalValues)

   i_CCInfo[l_Idx].DW_Value       =           &
      Get_Col_Data_Ex(i_DBErrorRow,    l_Idx, &
                      i_DBErrorBuffer, c_GetCurrentValues)

   i_CCStatus[l_Idx].User_Code    = c_CCUserNone

   i_CCStatus[l_Idx].Process_Code = c_CCProcUseDWValue

   SetNull(i_CCStatus[l_Idx].Special_Value)
NEXT

//----------
//  Initialize i_CCUser to NULL.  If the developer sets it, we
//  will use it in the error messages.
//----------

SetNull(i_CCUser)

//----------
//  If the record is NewModified!, then it does not exist in the
//  database.  We must have gotten the concurrency error because
//  the keys are not unique.  Identify the concurrency error as
//  such.
//----------

IF l_RowStatus = NewModified! THEN

   //----------
   //  Insert a row to replace the row that we can't reselect
   //  from the database.
   //----------

   l_FixRow = InsertRow(0)

   //----------
   //  Set the error to indicate that we had a key conflict.
   //----------

   i_CCErrorCode = c_CCErrorKeyConflict

   //----------
   //  By default, PowerClass does not ask for user intervention.
   //----------

   i_CCUserCode = c_CCUserNone

   //----------
   //  The default PowerClass method of handling this error will
   //  be to try to reinsert the row into the database.
   //----------

   i_CCProcCode = c_CCProcUseDWValue

   //----------
   //  Since we may try to reinsert the record, perform validation.
   //----------

   i_CCValidate = c_CCValidateRow

   //----------
   //  Finish filling in the i_CCInfo[] structure.
   //----------

   FOR l_Idx = 1 TO i_NumColumns
      i_CCInfo[l_Idx].Error_Code = c_CCErrorKeyConflict

      SetNull(i_CCInfo[l_Idx].New_DB_Value)
   NEXT
ELSE

   //----------
   //  Now copy the broken row so that we keep it as the original
   //  row.  We need the original row and a copy for:
   //     1) In case the user cancels AND
   //     2) have a row to do a RESELECTROW() on.
   //----------

   l_FixRow = RowCount() + 1
   l_Check  = RowsCopy(i_DBErrorRow,    i_DBErrorRow, &
                       i_DBErrorBuffer, THIS,         &
                       l_FixRow,        Primary!)

   //----------
   //  Get the record as it currently exists in the database.
   //----------

   i_InReselect = i_InReselect + 1
   l_Check = ReselectRow(l_FixRow)
   IF i_InReselect > 1 THEN
      i_InReselect = i_InReselect - 1
   ELSE
      i_InReselect = 0
   END IF

   IF l_Check <> 1 THEN

      //----------
      //  Failure -- we did not get a row back from RESELECTROW().
      //----------

      //----------
      //  Since the record has already been deleted from the
      //  database, if we are trying to delete it, then there
      //  is not an error.
      //----------

      IF i_DBErrorBuffer = Delete! THEN

         //----------
         //  Since we didn't get back the row we wanted, get rid
         //  of the l_FixRow.
         //----------

         l_Check = RowsDiscard(l_FixRow, l_FixRow, Primary!)
         l_FixRow = 0

         //----------
         //  Since we don't have an error, the "bad" row should
         //  be non-existent.
         //----------

         l_Check = RowsDiscard(i_DBErrorRow, i_DBErrorRow, &
                               i_DBErrorBuffer)

         //----------
         //  Since we discarded a deleted row, we need to shove
         //  the skip count back.
         //----------

         i_CCSkipDeleteRows[i_CCWhichUpdate] = i_DBErrorRow - 1

         //----------
         //  Clear the error condition since we are trying to
         //  delete a row that has already been deleted.
         //----------

         i_CCErrorCode = c_CCErrorNone

         //----------
         //  If the developer is updating more than one table,
         //  then we need to re-do the entire transaction in
         //  case something got modified in the other table(s).
         //----------

         IF i_CCWhichUpdate > 1 THEN

            Do_CC_Rollback()

            //----------
            //  If there was an error, make sure that our local
            //  error reflects it.
            //----------

            IF PCCA.Error <> c_Success THEN
               l_Error = PCCA.Error
            END IF

            //----------
            //  Make sure that everything is still Ok after the
            //  rollback.
            //----------

            IF l_Error = c_Success THEN

               //----------
               //  Since we rolled everything back, we will need
               //  to retry the save in its entirety.
               //----------

               l_RetrySave = TRUE
            END IF
         END IF
      ELSE

         //----------
         //  Since RESELECTROW() was unable to retrieve the row
         //  from the database, l_FixRow contains the original
         //  data in the DataWindow.  That is OK since we will
         //  overwrite it later.
         //----------

         //----------
         //  The record no longer exists in the database.
         //----------

         i_CCErrorCode = c_CCErrorNonExistent

         //----------
         //  By default, PowerClass does not ask for user
         //  intervention.
         //----------

         i_CCUserCode = c_CCUserNone

         //----------
         //  If c_CCEnableInsert is TRUE, the developer has told
         //  us that it this DataWindow has all of the columns
         //  need to insert rows into the database.  Therefore,
         //  the default PowerClass method of handling this error
         //  will be to re-insert the row into the database.
         //  If the developer has told use that the DataWindow
         //  does not contain the columns neccessary to insert
         //  into the database, our only choice is to drop the
         //  the user's changes and delete the row.
         //----------

         IF i_EnableCCInsert THEN
            i_CCProcCode = c_CCProcUseDWValue
         ELSE
            i_CCProcCode = c_CCProcUseNewDB
         END IF

         //----------
         //  If we are going to turn this into a NewModified!
         //  record, we will do validation.
         //----------

         IF i_CCProcCode = c_CCProcUseDWValue THEN
            i_CCValidate = c_CCValidateRow
         ELSE
            i_CCValidate = c_CCValidateNone
         END IF

         //----------
         //  Finish filling in the i_CCInfo[] structure.
         //----------

         FOR l_Idx = 1 TO i_NumColumns
            i_CCInfo[l_Idx].Error_Code = c_CCErrorNonExistent

            SetNull(i_CCInfo[l_Idx].New_DB_Value)
         NEXT
      END IF
   ELSE

      //----------
      //  Success -- got a row back from RESELECTROW().
      //----------

      //----------
      //  If a column is not updatable, it does not get retrieved
      //  by RESELECTROW().  We have to patch these values in.
      //----------

      FOR l_Idx = 1 TO i_NumColumns
         IF NOT i_CCInfo[l_Idx].Is_Updatable THEN
            Set_Col_Data(l_FixRow, l_Idx, &
                         i_CCInfo[l_Idx].Old_DB_Value)
         END IF
      NEXT

      //----------
      //  We have the current values for the row from the database.
      //  Mark the row as not being modified for the time being.
      //  In PowerBuilder, we have to call SETITEMSTATUS() twice
      //  to get the status back to NotModified!
      //
      //     - Convert NewModified!  to DataModified!.
      //     - Convert DataModified! to NotModified!.
      //----------

      SetItemStatus(l_FixRow, 0, Primary!, DataModified!)
      SetItemStatus(l_FixRow, 0, Primary!, NotModified!)

      //----------
      //  Fill in the i_CCInfo[].New_DB_Value element.
      //----------

      FOR l_Idx = 1 TO i_NumColumns
         i_CCInfo[l_Idx].New_DB_Value =      &
            Get_Col_Data_Ex(l_FixRow, l_Idx, &
                            Primary!, c_GetCurrentValues)
      NEXT

      //----------
      //  See if this record has been deleted from the DataWindow,
      //  but has been updated in the database.
      //----------

      IF i_DBErrorBuffer = Delete! THEN

         //----------
         //  The record no longer exists in the DataWindow.
         //----------

         i_CCErrorCode = c_CCErrorDeleteModified

         //----------
         //  By default, PowerClass does not ask for user
         //  intervention.
         //----------

         i_CCUserCode = c_CCUserNone

         //----------
         //  The default PowerClass method of handling this error
         //  will be to delete the row from the database.
         //----------

         i_CCProcCode = c_CCProcUseDWValue

         //----------
         //  Since we are either going to delete the record or
         //  change it to be NotModified!, there should not be
         //  a need to perform validation.
         //----------

         i_CCValidate = c_CCValidateNone

         //----------
         //  Finish filling in the i_CCInfo[] structure.
         //----------

         FOR l_Idx = 1 TO i_NumColumns
            i_CCStatus[l_Idx].Process_Code = c_CCProcUseNewDB

            i_CCInfo[l_Idx].Error_Code     = c_CCErrorDeleteModified
         NEXT
      ELSE

         //----------
         //  If we get to here, the record exists in the database,
         //  but we have some sort of overlap error.  The following
         //  code determines the overlap errors.
         //----------

         //----------
         //  Initialize i_CCErrorCode to indicate that there is
         //  not an error on the row.  This will be changed if
         //  and when we actually find the error.
         //----------

         i_CCErrorCode = c_CCErrorNone

         //----------
         //  By default, PowerClass does not ask for user
         //  intervention.
         //----------

         i_CCUserCode = c_CCUserNone

         //----------
         //  The default PowerClass method of handling this error
         //  will be to merge the columns to the database.  For
         //  now, assume that the DataWindow value will be used.
         //  If a newer value is found in the database, it will
         //  be used as the default value.
         //----------

         i_CCProcCode = c_CCProcUseDWValue

         //----------
         //  Since we may be updating columns, we should perform
         //  validation.
         //----------

         i_CCValidate = c_CCValidateRow

         //----------
         //  Finish filling in the i_CCInfo[] structure.
         //----------

         FOR l_Idx = 1 TO i_NumColumns

            //----------
            //  If the old DB and the new DB values are different,
            //  then there was a collision on this column.
            //----------

            l_IsSame = FALSE
            IF i_CCInfo[l_Idx].Old_DB_Value = &
               i_CCInfo[l_Idx].New_DB_Value THEN
               l_IsSame = TRUE
            ELSE
               IF IsNull(i_CCInfo[l_Idx].Old_DB_Value) THEN
               IF IsNull(i_CCInfo[l_Idx].New_DB_Value) THEN
                  l_IsSame = TRUE
               END IF
               END IF
            END IF

            //----------
            //  Are the old DB and new DB values the same?
            //----------

            IF NOT l_IsSame THEN

               //----------
               //  If the DataWindow value has not changed then
               //  this is a non-overlap error.  If it has changed,
               //  then we have an overlap error.
               //----------

               l_IsSame = FALSE
               IF i_CCInfo[l_Idx].Old_DB_Value = &
                  i_CCInfo[l_Idx].DW_Value THEN
                  l_IsSame = TRUE
               ELSE
                  IF IsNull(i_CCInfo[l_Idx].Old_DB_Value) THEN
                  IF IsNull(i_CCInfo[l_Idx].DW_Value) THEN
                     l_IsSame = TRUE
                  END IF
                  END IF
               END IF

               IF l_IsSame THEN
                  IF i_CCErrorCode = c_CCErrorNone THEN
                     i_CCErrorCode = c_CCErrorNonOverlap
                  END IF

                  i_CCInfo[l_Idx].Error_Code     = &
                     c_CCErrorNonOverlap

                  i_CCStatus[l_Idx].Process_Code = &
                     c_CCProcUseNewDB
               ELSE
                  i_CCErrorCode = c_CCErrorOverlap

                  //----------
                  //  If the current DataWindow value is the same
                  //  as the new DB value, then we have a "match"
                  //  error because everybody agrees as to what
                  //  the value in the database should be.
                  //----------

                  l_IsSame = FALSE
                  IF i_CCInfo[l_Idx].New_DB_Value = &
                     i_CCInfo[l_Idx].DW_Value THEN
                     l_IsSame = TRUE
                  ELSE
                     IF IsNull(i_CCInfo[l_Idx].New_DB_Value) THEN
                     IF IsNull(i_CCInfo[l_Idx].DW_Value) THEN
                        l_IsSame = TRUE
                     END IF
                     END IF
                  END IF
                  IF l_IsSame THEN
                     i_CCInfo[l_Idx].Error_Code = &
                        c_CCErrorOverlapMatch
                  ELSE
                     i_CCInfo[l_Idx].Error_Code = &
                        c_CCErrorOverlap
                  END IF
               END IF
            END IF

            //----------
            //  If we got an error on this column, then bump the
            //  number of column errors (informational purposes
            //  only).
            //----------

            IF i_CCInfo[l_Idx].Error_Code <> c_CCErrorNone THEN
               i_CCNumColumns = i_CCNumColumns + 1
            END IF
         NEXT
      END IF
   END IF
END IF

//----------
//  Reset PCCA.Error and l_Error.
//----------

PCCA.Error = c_Success
l_Error    = c_Success

//----------
//  See if we still have an error.
//----------

IF i_CCErrorCode <> c_CCErrorNone THEN

   //----------
   //  Remember if the transaction has been rolled back yet.
   //----------

   l_Rollback = i_RootDW.i_DidRollback

   //----------
   //  This event provides the developer the opportunity to fix
   //  concurrency errors.  If the event is used, it must be
   //  coded by the developer.
   //----------

   TriggerEvent("pcd_ConcurrencyError")

   //----------
   //  Make sure that the developer did not abort the update
   //  process because of the concurrency error.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  If the transaction had not been rolled back before the
      //  developer's concurrency error event and it is now, then
      //  the developer must have performed a rollback.  We must
      //  indicate that the entire save process must be performed
      //  again.
      //----------

      IF i_RootDW.i_DidRollback THEN
         IF l_Rollback <> i_RootDW.i_DidRollback THEN
            l_RetrySave = TRUE
         END IF
      END IF

      //----------
      //  Validate the developer's return values.
      //----------

      STRING  l_TmpStr

      l_TmpStr = ""
      IF i_CCUserCode < c_CCUserNone OR &
         i_CCUserCode > c_CCUserSpecify THEN
         l_TmpStr = l_TmpStr              + &
                    "      i_CCUserCode=" + &
                    String(i_CCUserCode)  + "~n"
      END IF
      IF i_CCProcCode < c_CCProcUseOldDB OR &
         i_CCProcCode > c_CCProcUseSpecial THEN
         l_TmpStr = l_TmpStr                + &
                    "      i_CDevProcCode=" + &
                    String(i_CCProcCode)    + "~n"
      END IF
      IF i_CCValidate < c_CCValidateNone OR &
         i_CCValidate > c_CCValidateRow  THEN
         l_TmpStr = l_TmpStr              + &
                    "      i_CCValidate=" + &
                    String(i_CCValidate)  + "~n"
      END IF
      FOR l_Idx = 1 TO i_NumColumns
         IF i_CCStatus[l_Idx].User_Code < c_CCUserNone OR &
            i_CCStatus[l_Idx].User_Code > c_CCUserSpecify THEN
            l_TmpStr = &
               l_TmpStr                            + &
               "      i_CCStatus["                 + &
               String(l_Idx)                       + &
               "].User_Code="                      + &
               String(i_CCStatus[l_Idx].User_Code) + "~n"
         END IF

         IF i_CCStatus[l_Idx].Process_Code < &
            c_CCProcUseOldDB OR              &
            i_CCStatus[l_Idx].Process_Code > &
            c_CCProcUseSpecial THEN
            l_TmpStr = &
               l_TmpStr                               + &
               "      i_CCStatus["                    + &
               String(l_Idx)                          + &
               "].Process_Code="                      + &
               String(i_CCStatus[l_Idx].Process_Code) + "~n"
         END IF
      NEXT

      IF Len(l_TmpStr) > 0 THEN
         PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_PCCCError"
         PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
         PCCA.MB.i_MB_Strings[3] = i_ClassName
         PCCA.MB.i_MB_Strings[4] = i_Window.Title
         PCCA.MB.i_MB_Strings[5] = DataObject
         PCCA.MB.i_MB_Strings[6] = l_TmpStr
         PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_BadCCValue, &
                            0, PCCA.MB.i_MB_Numbers[],   &
                            6, PCCA.MB.i_MB_Strings[])
      END IF

      //----------
      //  See if we are to open the window to allow user
      //  intervention for concurrency errors.
      //----------

      l_Found = FALSE
      IF i_CCErrorCode = c_CCErrorNonOverLap  OR &
         i_CCErrorCode = c_CCErrorOverLap     OR &
         i_CCErrorCode = c_CCErrorKeyConflict THEN

         //----------
         //  Make sure that the user can actually modify the
         //  columns before we open the window.
         //----------

         i_CCUserCode = c_CCUserNone

         FOR l_Idx = 1 TO i_NumColumns
            IF i_CCStatus[l_Idx].User_Code = c_CCUserSpecify THEN
               IF i_TabOrder[l_Idx] > 0 AND i_ColVisible[l_Idx] THEN
                  l_Found = TRUE

                  //----------
                  //  Need to do the whole loop to check all
                  //  columns for tab order and visiblity.
                  //
                  //  EXIT
                  //----------
               ELSE

                  //----------
                  //  If the user can't see or modify this column,
                  //  then mark the column as not requiring user
                  //  intervention.
                  //----------

                  i_CCStatus[l_Idx].User_Code = c_CCUserNone
               END IF
            END IF
         NEXT
      END IF

      //----------
      //  If requested, open the window to allow user intervention
      //  for errors.
      //----------

      IF i_CCUserCode = c_CCUserSpecify OR l_Found THEN

         //----------
         //  Since we are going interactive, rollback the
         //  transaction and free the database.  We don't
         //  want the database tied up while the user is
         //  making their descision.
         //----------

         Do_CC_Rollback()

         //----------
         //  If there was an error, make sure that our local error
         //  reflects it.
         //----------

         IF PCCA.Error <> c_Success THEN
            l_Error = PCCA.Error
         END IF

         //----------
         //  Make sure that everything is still Ok after the
         //  rollback.
         //----------

         IF l_Error = c_Success THEN

            //----------
            //  Since we rolled everything back, we will need to
            //  retry the save in its entirety.
            //----------

            l_RetrySave = TRUE

            //----------
            //  Move the copy of the broken row to the Delete!
            //  buffer so that it will not be visible while the
            //  user works with the DataWindow.
            //----------

            l_TmpRow = DeletedCount() + 1
            l_Check  = RowsMove(l_FixRow, l_FixRow, Primary!, &
                                THIS,     l_TmpRow, Delete!)

            //----------
            //  Set up i_CCUserRow with the default values.  This
            //  row will be deleted by the concurrency error
            //  window.
            //----------

            i_CCUserRow = InsertRow(0)

            //----------
            //  Set the values recommended by PowerClass and/or
            //  the developer into the new row and scroll to it.
            //----------

            Set_CC_Row(i_CCUserRow, c_CCSetForce)
            IF i_DBErrorBuffer = Primary! THEN
               ScrollToRow(i_DBErrorRow)
            END IF

            //----------
            //  Make sure that this DataWindow is on top.
            //----------

            l_WinState = i_Window.WindowState
            l_CurDW    = PCCA.Window_CurrentDW
            Change_DW_Focus(THIS)

            //----------
            //  Make sure that the user can see the data in the
            //  DataWindow.
            //----------

            IF i_IsEmpty THEN
               Modify(i_ActiveNonEmptyColors)
            END IF

            //----------
            //  Open the error window for user intervention.
            //----------

            Window_Open_Parm(w_CC_Error_Std, 0, THIS)

            //----------
            //  The concurrency error window turned redraw on.
            //  Restore it to its original state.
            //----------

            IF i_RedrawCount > 1 THEN
               i_RedrawCount = i_RedrawCount - 1
               SetRedraw(i_RedrawStack[i_RedrawCount])
            ELSE
               i_RedrawCount = 0
               SetRedraw(TRUE)
            END IF

            //----------
            //  If the DataWindow was empty, restore the empty
            //  colors.
            //----------

            IF i_IsEmpty THEN
               Modify(i_ActiveEmptyColors)
            END IF

            //----------
            //  Restore the window state and current DataWindow.
            //----------

            IF i_CCUserStatus <> c_CCUserCancel THEN
               IF l_WinState = Minimized! THEN
                  i_Window.WindowState = Minimized!
               END IF
               IF IsNull(l_CurDW) THEN
               ELSE
                  IF l_CurDW <> THIS THEN
                     Change_DW_Focus(l_CurDW)
                  END IF
               END IF
            END IF

            //----------
            //  Reset the focus done flag.
            //----------

            PCCA.PCMGR.i_DW_FocusDone = FALSE

            //----------
            //  Restore the copy of the broken row to the
            //  Primary! buffer.
            //----------

            l_TmpRow = DeletedCount()
            l_Check  = RowsMove(l_TmpRow, l_TmpRow, Delete!, &
                                THIS,     l_FixRow, Primary!)

            //----------
            //  See if the user canceled.  If they did, we will
            //  leave the original row in its current state with
            //  the concurrency errors.
            //----------

            IF i_CCUserStatus = c_CCUserCancel THEN
               l_Error = c_Fatal
            END IF
         END IF
      END IF
   ELSE

      //----------
      //  The developer generated an error in the
      //  pcd_ConcurrencyError event.  Store the error
      //  into our local so that it will not get reset.
      //----------

      l_Error = PCCA.Error
   END IF

   //----------
   //  Check l_Error as either the developer or the user may have
   //  aborted the save process at this point.
   //----------

   IF l_Error = c_Success THEN

      //----------
      //  Based on the error code, process the concurrency error
      //  according to the developer's and/or user's instructions.
      //----------

      CHOOSE CASE i_CCErrorCode

         //----------
         //  For the columns that the developer and/or user
         //  processed, stuff the values into the DataWindow
         //  so that UPDATE() will get them on the next pass.
         //----------

         CASE c_CCErrorNonOverlap, &
              c_CCErrorOverlap,    &
              c_CCErrorOverlapMatch

            //----------
            //  We need to patch the requested values into the
            //  DataWindow.
            //----------

            Set_CC_Row(l_FixRow, c_CCSetIfModified)

         //----------
         //  The row does not exist in the database.  Should we
         //  reinsert it?  Make sure that the row status is New!.
         //  It will become NewModified! after the Set_CC_Row()
         //  routine.
         //----------

         CASE c_CCErrorNonExistent
            IF i_CCProcCode = c_CCProcUseNewDB THEN

               //----------
               //  We are not going to reinsert the record into
               //  the database to replace the row deleted by
               //  another user.  Discard the fixed row.
               //----------

               l_Check = RowsDiscard(l_FixRow, l_FixRow, Primary!)

               //----------
               //  The fixed row does not exist anymore.
               //----------

               l_FixRow = 0

               //----------
               //  If the developer is updating more than one
               //  table, then we need to re-do the entire
               //  transaction in case something got modified in
               //  the other table(s).
               //----------

               IF i_CCWhichUpdate > 1 THEN

                  Do_CC_Rollback()

                  //----------
                  //  If there was an error, make sure that our
                  //  local error reflects it.
                  //----------

                  IF PCCA.Error <> c_Success THEN
                     l_Error = PCCA.Error
                  END IF

                  //----------
                  //  Make sure that everything is still Ok after
                  //  the rollback.
                  //----------

                  IF l_Error = c_Success THEN

                     //----------
                     //  Since we rolled everything back, we will
                     //  need to retry the save in its entirety.
                     //----------

                     l_RetrySave = TRUE
                  END IF
               END IF
            ELSE

               //----------
               //  We are going to reinsert the record into the
               //  database to replace the row deleted by another
               //  user.  Set the row values as specified by the
               //  developer.
               //----------

               FOR l_Idx = 1 TO i_NumColumns
                  i_CCStatus[l_Idx].Process_Code = &
                     i_CCProcCode
               NEXT

               SetItemStatus(l_FixRow, 0, Primary!, New!)
               Set_CC_Row(l_FixRow, c_CCSetForce)
            END IF

         //----------
         //  The row was deleted from the DataWindow, but was
         //  updated in the database.  Should we still delete it?
         //----------

         CASE c_CCErrorDeleteModified
            IF i_CCProcCode = c_CCProcUseNewDB THEN

               //----------
               //  We are not going to delete the record that was
               //  updated in the database by another user.
               //----------

               FOR l_Idx = 1 TO i_NumColumns
                  i_CCStatus[l_Idx].Process_Code = &
                     i_CCProcCode
               NEXT
               Set_CC_Row(l_FixRow, c_CCSetIfModified)

               //----------
               //  This row came from the Delete! buffer, but
               //  since we are not deleting it, just leave it
               //  at the end of the Primary! buffer.  Since we
               //  have taken care of all the processing that
               //  needs to be done, set l_FixRow to 0 so that
               //  no more processing takes place.
               //----------

               l_FixRow = 0
            ELSE

               //----------
               //  Move the fixed row to the Delete! buffer
               //  so that it will be deleted.  Put it right
               //  after the broken row in the Delete! buffer
               //  so that when the broken row gets discarded
               //  the fixed row will be in the correct position.
               //----------

               l_TmpRow = i_DBErrorRow + 1
               l_Check  = RowsMove(l_FixRow, l_FixRow, Primary!, &
                                   THIS,     l_TmpRow, Delete!)

               //----------
               //  The fixed row does not "exist" anymore.
               //----------

               l_FixRow = 0
            END IF

         //----------
         //  For key conflicts, trigger pcd_SetKey again to try
         //  to set unique keys.
         //----------

         CASE c_CCErrorKeyConflict

            Set_CC_Row(l_FixRow, c_CCSetIfModified)

            //----------
            //  Initialize variables for use by the developer.
            //----------

            PCCA.Error     = c_Success
            i_DisplayError = TRUE

            //----------
            //  Save the DB error information.
            //----------

            l_CCDBError         = i_CCDBError
            l_DBErrorCode       = i_DBErrorCode
            l_DBErrorMessage    = i_DBErrorMessage
            l_DBErrorRow        = i_DBErrorRow
            l_DBErrorBuffer     = i_DBErrorBuffer
            l_DevDBErrorMessage = i_DevDBErrorMessage

            //----------
            //  Reset the DB Error codes.
            //----------

            i_CCDBError         = FALSE
            i_DBErrorCode       = 0
            i_DBErrorMessage    = ""
            i_DBErrorRow        = 0
            i_DevDBErrorMessage = ""

            //----------
            //  This event provides the developer the opportunity
            //  to set values for the key columns in the
            //  DataWindow.  If this event is used, it must be
            //  coded by the developer.
            //----------

            TriggerEvent("pcd_SetKey")

            //----------
            //  If there was an error while setting the keys,
            //  then we may need to rollback the transaction.
            //----------

            IF PCCA.Error <> c_Success THEN

               //----------
               //  Save the flag which indicates if the developer
               //  wanted us to display the error.
               //----------

               l_DisplayError = i_DisplayError

               //----------
               //  Save the fu_MessageBox() parms as the
               //  information may get cleared by the
               //  rollback process.
               //----------

               IF l_DisplayError THEN
                  l_MBID         = PCCA.MB.c_MBI_DW_SetKeyError
                  l_MBNumbers[1] = i_DBErrorCode
                  l_MBStrings[1] = "pcd_SetKey"
                  l_MBStrings[2] = PCCA.Application_Name
                  l_MBStrings[3] = i_ClassName
                  l_MBStrings[4] = i_Window.Title
                  l_MBStrings[5] = DataObject
                  l_MBStrings[6] = i_DBErrorMessage
                  l_MBStrings[7] = i_DevDBErrorMessage
               END IF

               //----------
               //  Do the rollback.
               //----------

               Do_CC_Rollback()

               //----------
               //  There was an error earlier but we did not
               //  display it because we didn't want to hold
               //  up the ROLLBACK process.  Display the error
               //  now if the developer requested it.
               //----------

               IF l_DisplayError THEN
                  PCCA.MB.fu_MessageBox(l_MBID,           &
                                     1, l_MBNumbers[], &
                                     7, l_MBStrings[])
               END IF

               //----------
               //  Make sure that the error variables still
               //  indicate failure.
               //----------

               PCCA.Error = c_Fatal
               l_Error    = c_Fatal
            END IF

            //----------
            //  Restore the DB error information.
            //----------

            i_CCDBError         = l_CCDBError
            i_DBErrorCode       = l_DBErrorCode
            i_DBErrorMessage    = l_DBErrorMessage
            i_DBErrorRow        = l_DBErrorRow
            i_DBErrorBuffer     = l_DBErrorBuffer
            i_DevDBErrorMessage = l_DevDBErrorMessage

         //----------
         //  No errors.
         //----------

         //CASE c_CCErrorNone
         CASE ELSE
      END CHOOSE

      //----------
      //  Process validation if there is still a concurrency
      //  error.
      //----------

      IF i_CCErrorCode <> c_CCErrorNone THEN
      IF l_Error       =  c_Success     THEN

         //----------
         //  See if validation was requested.
         //----------

         IF i_CCValidate = c_CCValidateRow THEN

            //----------
            //  Make sure that there is a row to validate.
            //----------

            IF l_FixRow > 0 THEN

               //----------
               //  Set up i_RowNbr for the developer to use during
               //  validation.
               //----------

               i_RowNbr = l_FixRow

               //----------
               //  Make sure that validation is enabled while we
               //  do validation checking.
               //----------

               i_IgnoreVal      = (NOT c_ProcessVal)
               i_IgnoreValCount = i_IgnoreValCount + 1
               i_IgnoreValStack[i_IgnoreValCount] = c_ProcessVal

               TriggerEvent("pcd_ValidateRow")

               IF i_IgnoreValCount > 1 THEN
                  i_IgnoreValCount = i_IgnoreValCount - 1
                  i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
               ELSE
                  i_IgnoreValCount = 0
                  i_IgnoreVal      = FALSE
               END IF

               //----------
               //  If validation failed, then an error message was
               //  already displayed.  Set i_DisplayError to FALSE
               //  so that the error message from pcd_Update is not
               //  displayed.
               //----------

               IF PCCA.Error <> c_Success THEN
                  l_Error        = PCCA.Error
                  i_DisplayError = FALSE
               END IF
            END IF
         END IF
      END IF
      END IF
   END IF

   IF l_Error = c_Success THEN

      //----------
      //  Since we had a concurrency error, we want to update
      //  this row again.  Set i_CCSkip*Rows[] to skip the rows
      //  that have already been successfully updated and get
      //  ready to try this row again.
      //----------

      CHOOSE CASE i_DBErrorBuffer
         CASE Delete!
            i_CCSkipDeleteRows[i_CCWhichUpdate]  = i_DBErrorRow - 1
         CASE Filter!
            i_CCSkipFilterRows[i_CCWhichUpdate]  = i_DBErrorRow - 1
         CASE Primary!
            i_CCSkipPrimaryRows[i_CCWhichUpdate] = i_DBErrorRow - 1
         CASE ELSE
      END CHOOSE
   END IF
END IF

//----------
//  If something bad happened, clean up.  We need to discard the
//  fixed row and restore the original broken row.
//----------

IF l_Error <> c_Success THEN

   //----------
   //  Discard the fixed row.
   //----------

   IF l_FixRow > 0 THEN
      l_Check = RowsDiscard(l_FixRow, l_FixRow, Primary!)
   END IF

   //----------
   //  We will leave the original "bad" row since the save
   //  operation was canceled.
   //----------

ELSE

   //----------
   //  Make sure that we really had a concurrency error and that
   //  this was really not a false alarm (e.g. deleting a row
   //  that has already been deleted in the database).
   //----------

   IF i_CCErrorCode <> c_CCErrorNone THEN

      //----------
      //  If we get to here, there was a concurrency error but it
      //  has gotten corrected (hopefully).  Make sure that the
      //  fixed row is in the correct buffer at the correct
      //  position.  Then discard the original broken row.
      //----------

      IF l_FixRow > 0 THEN

         //----------
         //  Get ready to move the fixed row to the correct buffer.
         //  Put it right after the broken row so that when the
         //  broken row gets discarded the fixed row will be in
         //  the correct position.
         //----------

         l_TmpRow = i_DBErrorRow + 1
         l_Check = RowsMove(l_FixRow, l_FixRow, Primary!, &
                            THIS,     l_TmpRow, i_DBErrorBuffer)
      END IF

      //----------
      //  Now discard the original broken row.
      //----------

      l_TmpRow = i_DBErrorRow
      l_Check  = RowsDiscard(l_TmpRow, l_TmpRow, i_DBErrorBuffer)
   END IF
END IF

//----------
//  Depending on how the concurrency error was handled, the current
//  selections might have changed.  We need to make sure that the
//  correct rows are selected.
//----------

//----------
//  Check EMPTY processing.
//----------

IF i_IsEmpty THEN
   IF RowCount() > 1 OR NOT i_AddedEmpty THEN
      IF i_AddedEmpty THEN
         DeleteRow(1)
      END IF
      Share_State(c_ShareNotEmpty, c_StateUnused)
   END IF
END IF

IF RowCount() > 0 THEN

   //----------
   //  Since the order of the rows may have gotten shuffled,
   //  clear all of the selected rows.
   //----------

   SelectRow(0, FALSE)

   //----------
   //  Attempt to reselect the rows.
   //----------

   Restore_Selected_Rows &
      (l_NumOldSelected, l_OldSelected[], l_KeyArray[])
   i_NumSelected = Get_Selected_Rows(i_SelectedRows[])

   //----------
   //  If we were unable to find the old i_CursorRow, then set
   //  the cursor position to the same row position it was
   //  before the retrieve.
   //----------

   IF i_CursorRow = 0 THEN
      i_CursorRow = l_SaveRow
   END IF

   //----------
   //  Make sure that the cursor row is legal.
   //----------

   IF i_CursorRow < 1 THEN
      i_CursorRow = 1
   END IF

   IF i_CursorRow > RowCount() THEN
      i_CursorRow = RowCount()
   END IF

   //----------
   //  If we were able to re-select some rows, we need to do
   //  some processing.
   //----------

   IF i_NumSelected > 0 THEN

      //----------
      //  If we are unable to find a i_ShowRow, just set
      //  i_ShowRow to the first selected row.
      //----------

      IF i_ShowRow = 0 THEN
         i_ShowRow = GetSelectedRow(0)
      END IF

      //----------
      //  If we are not a multi-select DataWindow, make sure
      //  that we did not select more than one row.
      //----------

      IF NOT i_MultiSelect AND i_NumSelected > 1 THEN
         SelectRow(0, FALSE)

         IF i_HighlightSelected THEN
            SelectRow(i_ShowRow, TRUE)
         END IF
      ELSE

         //----------
         //  If i_HighlightSelected is FALSE, then we don't use
         //  SelectRow().
         //----------

         IF NOT i_HighlightSelected THEN
            SelectRow(0, FALSE)
         END IF
      END IF
   END IF

   //----------
   //  Now set up i_SelectedRows for retrieval.
   //----------

   i_NumSelected = Get_Retrieve_Rows(i_SelectedRows[])

   //----------
   //  Restore the cursor row in case it got changed during the
   //  processing of the concurrency error.
   //----------

   ScrollToRow(i_CursorRow)

   //----------
   //  Move the cursor to the original column.
   //----------

   IF l_SaveColumn > 0 THEN
      IF l_SaveColumn <> GetColumn() THEN
         SetColumn(l_SaveColumn)
      END IF
   END IF
ELSE

   //----------
   //  No rows are left after processing the concurrency error.
   //  Make the DataWindow empty and reset the status variables.
   //----------

   i_CursorRow   = 0
   i_ShowRow     = 0
   i_AnchorRow   = 0
   i_DWTopRow    = 0
   i_CurrentMode = c_ModeView
   i_DWState     = c_DWStateUndefined
   TriggerEvent("pcd_Disable")
END IF

//----------
//  Restore redraw, RFC, VScroll, and validation processing.
//----------

IF i_RedrawCount > 1 THEN
   i_RedrawCount = i_RedrawCount - 1
   SetRedraw(i_RedrawStack[i_RedrawCount])
ELSE
   i_RedrawCount = 0
   SetRedraw(TRUE)
END IF

IF i_IgnoreRFCCount > 1 THEN
   i_IgnoreRFCCount = i_IgnoreRFCCount - 1
   i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
ELSE
   i_IgnoreRFCCount = 0
   i_IgnoreRFC      = FALSE
END IF

IF i_IgnoreVSCount > 1 THEN
   i_IgnoreVSCount = i_IgnoreVSCount - 1
   i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
ELSE
   i_IgnoreVSCount = 0
   i_IgnoreVScroll = FALSE
END IF
IF NOT i_IgnoreVScroll THEN
   i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
END IF

IF i_IgnoreValCount > 1 THEN
   i_IgnoreValCount = i_IgnoreValCount - 1
   i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
ELSE
   i_IgnoreValCount = 0
   i_IgnoreVal      = FALSE
END IF

//----------
//  If this row was the same row we got a concurrency error on the
//  last time we tried to update, bump the retry count.  If there
//  are too many retries, abort the update.
//----------

IF i_DBErrorRow > 0 THEN
   IF i_CCLastBadRow[i_CCWhichUpdate] = i_DBErrorRow THEN

      //----------
      //  Bump the retry count.
      //----------

      i_CCNumRetry[i_CCWhichUpdate] = &
         i_CCNumRetry[i_CCWhichUpdate] + 1

      //----------
      //  If c_CCMaxNumRetry is 0, we will keep retrying forever.
      //----------

      IF c_CCMaxNumRetry <> 0 THEN
         IF i_CCNumRetry[i_CCWhichUpdate] >= c_CCMaxNumRetry THEN
            PCCA.MB.i_MB_Numbers[1] = i_DBErrorCode
            PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_PCCCError"
            PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
            PCCA.MB.i_MB_Strings[3] = i_ClassName
            PCCA.MB.i_MB_Strings[4] = i_Window.Title
            PCCA.MB.i_MB_Strings[5] = DataObject
            PCCA.MB.i_MB_Strings[6] = i_DBErrorMessage
            PCCA.MB.i_MB_Strings[7] = i_DevDBErrorMessage
            PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_CC_ExceededRetry, &
                               1, PCCA.MB.i_MB_Numbers[],         &
                               7, PCCA.MB.i_MB_Strings[])

            l_Error        = c_Fatal
            i_DisplayError = FALSE
         END IF
      END IF
   ELSE
      i_CCLastBadRow[i_CCWhichUpdate] = i_DBErrorRow
      i_CCNumRetry[i_CCWhichUpdate]   = 1
   END IF
END IF

PCCA.Error = l_Error

//----------
//  If this event was successful, we have to see if the transaction
//  was rolled back.  If it was, we need to stop the save process
//  and restart it, so that all updates get sent to the database
//  again.
//----------

IF PCCA.Error = c_Success THEN
   IF l_RetrySave THEN
      i_RootDW.i_RetrySave = TRUE
      PCCA.Error           = c_Fatal
      i_DisplayError       = FALSE
   END IF
END IF

i_ExtendMode = i_InUse
end on

on pcd_previous;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Previous
//  Description   : Scroll to the previous row in the DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN     l_SetFocus
LONG        l_RowCount, l_NextRow
UO_DW_MAIN  l_ToDW

//----------
//  Make sure that this DataWindow is in use.
//----------

IF NOT i_InUse THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If the DataWindow is in "QUERY" mode, then we don't process
//  scroll events.
//----------

IF i_DWState = c_DWStateQuery THEN
   Change_DW_Focus(THIS)
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Indicate to the user that we are processing the sroll.
//----------

SetPointer(HourGlass!)
PCCA.Error = c_Success

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_ScrollPrevious, c_Show)

//----------
//  We need to scroll to the previous record.  However, we can only
//  do this if the DataWindow to be scrolled has rows in it.
//----------

l_SetFocus = FALSE
l_RowCount = i_ScrollDW.RowCount()
IF l_RowCount > 0 THEN

   //----------
   //  Determine the current cursor row and get ready to scroll
   //  to the previous row.
   //----------

   l_NextRow = i_ScrollDW.GetRow() - 1
   IF l_NextRow < 1 THEN
      l_NextRow = 1
   END IF

   //----------
   //  We use the i_MoveRow instance variable to tell the
   //  RowFocusChanged! event what row we want to scroll to.
   //  By setting i_MoveRow to a negative row number, we are
   //  telling RowFocusChanged! to scroll to that row AND
   //  select it.
   //----------

   i_ScrollDW.i_MoveRow = - l_NextRow
   i_ScrollDW.TriggerEvent(RowFocusChanged!)

   //----------
   //  If the scroll event was successful, set the focus to the
   //  preferred-focus DataWindow.  If there is not a preferred-
   //  focus DataWindow, then set focus to this DataWindow (i.e.
   //  the DataWindow that received the scroll event).
   //----------

   IF PCCA.Error = c_Success THEN
      IF IsValid(i_FocusDW) THEN
         IF i_FocusDW.i_InUse THEN
            l_SetFocus = TRUE
            l_ToDW     = i_FocusDW
         END IF
      END IF
   END IF
END IF

//----------
//  If focus has not been set by the developer, set it.
//----------

IF NOT PCCA.PCMGR.i_DW_FocusDone THEN

   //----------
   //  If we couldn't find a better choice for the DataWindow to
   //  set focus to, just set it to the DataWindow that received
   //  the scroll event (i.e. this DataWindow).
   //----------

   IF NOT l_SetFocus THEN
      l_ToDW = THIS
   END IF

   //----------
   //  Make the DataWindow that was our best choice the current
   //  DataWindow.
   //----------

   Set_Auto_Focus(l_ToDW)
END IF

PCCA.MDI.fu_Pop()

Finished:

i_ExtendMode = i_InUse
end on

on pcd_print;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Print
//  Description   : Print the contents of the DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Make sure that there are actually rows in DataWindow that
//  can be printed.
//----------

IF NOT i_InUse OR i_IsEmpty THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Indicate to the user that we are processing the print request.
//----------

SetPointer(HourGlass!)
PCCA.Error = c_Success

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_Print, c_ShowMark)

//----------
//  Make sure that there is valid data before we print.
//----------

IF dw_AcceptText(c_AcceptProcessErrors) THEN
   Print()
ELSE
   PCCA.Error = c_Fatal
END IF

//----------
//  Remove the print prompt.
//----------

PCCA.MDI.fu_Pop()

Finished:

i_ExtendMode = i_InUse
end on

on pcd_query;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Query
//  Description   : Put the DataWindow into "QUERY" mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_ColNbr
UNSIGNEDLONG  l_RefreshCmd
STRING        l_Select

//----------
//  Make sure that this DataWindow is in use.
//----------

IF NOT i_InUse THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Send the query request to the root-level DataWindow, if this
//  DataWindow is not the root-level DataWindow
//----------

IF THIS <> i_RootDW THEN
   i_RootDW.TriggerEvent("pcd_Query")
   GOTO Finished
END IF

//----------
//  If this window is already in "QUERY" mode, just return.
//----------

PCCA.Error = c_Success

IF i_DWState = c_DWStateQuery THEN
   GOTO Finished
END IF

//----------
//  Indicate to the user that we are setting "QUERY" mode.
//----------

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Query, c_ShowMark)

//----------
//  Make sure that this DataWindow allows "QUERY" mode.  If it
//  does not, tell the user and exit this event.
//----------

IF NOT i_AllowQuery THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Query"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_QueryNotAllowed, &
                      0, PCCA.MB.i_MB_Numbers[],        &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   PCCA.MDI.fu_Pop()
   GOTO Finished
END IF

//----------
//  When we come out of "QUERY" mode, this DataWindow and the
//  children DataWindows may get loaded with new records.  Make
//  sure that changes are taken care of.
//----------

Check_Save(l_RefreshCmd)

//----------
//  If we did not get a successful return from Check_Save(), then
//  we will not go into "QUERY" mode (e.g. the user hit cancel).
//----------

IF PCCA.Error <> c_Success THEN
   PCCA.MDI.fu_Pop()
   GOTO Finished
END IF

//----------
//  If l_RefreshCmd is a valid refresh mode, then Check_Save()
//  specified that the DataWindow hierarchy needs to be refreshed.
//  Refresh the entire hierarchy, except for this branch since it
//  is going to be cleared and placed into "QUERY" mode.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")

   IF PCCA.Error <> c_Success THEN
      PCCA.MDI.fu_Pop()
      GOTO Finished
   END IF
END IF

//----------
//  Turn off RFC, validation, VScroll, and redraw while we set
//  up the DataWindow for "QUERY" mode.
//----------

SetRedraw(c_BufferDraw)
i_RedrawCount = i_RedrawCount + 1
i_RedrawStack[i_RedrawCount] = c_BufferDraw

i_IgnoreRFC      = (NOT c_IgnoreRFC)
i_IgnoreRFCCount = i_IgnoreRFCCount + 1
i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

i_IgnoreVScroll = (NOT c_IgnoreVScroll)
i_IgnoreVSCount = i_IgnoreVSCount + 1
i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

i_IgnoreVal      = (NOT c_IgnoreVal)
i_IgnoreValCount = i_IgnoreValCount + 1
i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

//----------
//  Clear the DataWindow and all of its children DataWindows.
//  pcd_Search will re-populate them when the user exits "QUERY"
//  mode.
//----------

is_EventControl.Refresh_Cmd = c_RefreshSame
is_EventControl.Reset       = TRUE
TriggerEvent("pcd_Refresh")

//----------
//  Enable the DataWindow so that the user can type into it.
//----------

TriggerEvent("pcd_Enable")

//----------
//  All of the columns on the DataWindow may not be modifiable.
//  However, the user may want to enter a query clause for non-
//  modifiable columns, so we need to make sure that all fields
//  on the DataWindow have a tab order so that user can type into
//  them.
//----------

Modify(i_QueryTabs)

//----------
//  Make sure that there is a visible column on the DataWindow
//  (note that it would pretty unlikely that there is not).
//----------

IF i_FirstVisColumn > 0 THEN

   //----------
   //  We want to set the cursor to the first visible column on
   //  the DataWindow.  If the cursor is not already there, then
   //  move it there.
   //----------

   l_ColNbr = GetColumn()
   IF i_FirstVisColumn <> l_ColNbr THEN
      SetColumn(i_FirstVisColumn)
   END IF
END IF

//----------
//  Reset the SQL statement to the original SQL statement
//  specified in the DataWindow.  When the user hits "SEARCH",
//  pcd_Search will check the search objects (if any) and rebuild
//  the appropriate SQL statement.
//----------

IF Len(i_StoredProc) = 0 THEN
   l_Select = "DataWindow.Table.Select='"          + &
               PCCA.MB.fu_QuoteChar(i_SQLSelect, "'") + "'"
   Modify(l_Select)
END IF

//----------
//  Place the DataWindow into "QUERY" mode.
//----------

Modify("DataWindow.QueryMode=Yes")

//----------
//  Need to set i_DWState before changing focus so pcd_SetControl
//  sees the new value.
//----------

i_DWState = c_DWStateQuery

//----------
//  Force i_Active to FALSE so that the active colors get set for
//  the query DataWindow.
//----------

i_Active = FALSE

//----------
//  Change focus to this DataWindow since it just got put into
//  "QUERY" mode.
//----------

Change_DW_Focus(THIS)

//----------
//  Make sure that the text color is visible.
//----------

IF i_SetEmptyColors THEN
   Modify(i_NonEmptyTextColors)
END IF

//----------
//  Set the current row indicator.
//----------

TriggerEvent("pcd_SetRowIndicator")

//----------
//  Restore redraw to its previous state.
//
//  NOTE: RFC, VScroll, and validation processing are left off
//        while the DataWindow is in "QUERY" mode.  When the user
//        exists "QUERY" mode, they will be restored to their
//        previous values.
//----------

IF i_RedrawCount > 1 THEN
   i_RedrawCount = i_RedrawCount - 1
   SetRedraw(i_RedrawStack[i_RedrawCount])
ELSE
   i_RedrawCount = 0
   SetRedraw(TRUE)
END IF

//----------
//  Remove the query prompt.
//----------

PCCA.MDI.fu_Pop()

Finished:

i_ExtendMode = i_InUse
end on

on pcd_refresh;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Refresh
//  Description   : Puts the window into the specified mode and
//                  retrieves rows if necessary.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_Reset,        l_SaveRaise,    l_PopPrompt
BOOLEAN       l_Modified,     l_Overwritten,  l_DoPickedRow
BOOLEAN       l_IsEmpty,      l_AllInstances, l_DoReset
BOOLEAN       l_SkipChildren, l_OnlyDoChildren
BOOLEAN       l_SkipDWValid,  l_SkipDWChildrenValid
INTEGER       l_Error,        l_Level
UNSIGNEDLONG  l_ModeOnOpen,   l_NextMode
LONG          l_RowCount,     l_InsRowNbr
LONG          l_Start,        l_End, l_Selected[]
LONG          l_Idx,          l_Jdx, l_Kdx
UO_DW_MAIN    l_SkipDW,       l_SkipDWChildren, l_ChildDW

//----------
//  If pcd_Refresh is not already running, then set
//  PCCA.PCMGR.i_DW_FocusDone to FALSE to indicate
//  that focus has not set.  If it is FALSE when
//  pcd_Refresh finishes, then whoever triggered
//  pcd_Refresh may set focus (e.g. pcd_View).
//----------

IF PCCA.PCMGR.i_DW_InRefresh = 0 THEN
   PCCA.PCMGR.i_DW_FocusDone = FALSE
END IF

//----------
//  Increment the refresh variable to indicate to other PowerClass
//  events that pcd_Refresh is running.
//----------

PCCA.PCMGR.i_DW_InRefresh = PCCA.PCMGR.i_DW_InRefresh + 1

//----------
//  pcd_Refresh is used to refresh DataWindows in various modes
//  (e.g. "NEW").  pcd_Refresh is also responsible for determining
//  if a retrieve needs to be done and triggering the developer's
//  pcd_Retrieve event.  The Event Control structure specifies
//  how pcd_Refresh is to do its job.  Save all of these options
//  into local variables before the Event Control structure gets
//  reset by Get_Event_Info().
//----------

i_RefreshMode         = is_EventControl.Refresh_Cmd
l_OnlyDoChildren      = is_EventControl.Only_Do_Children
l_Reset               = is_EventControl.Reset
l_Level               = is_EventControl.Level
l_InsRowNbr           = is_EventControl.Insert_Row_Nbr
l_SkipDWValid         = is_EventControl.Skip_DW_Valid
l_SkipDW              = is_EventControl.Skip_DW
l_SkipDWChildrenValid = is_EventControl.Skip_DW_Children_Valid
l_SkipDWChildren      = is_EventControl.Skip_DW_Children

//----------
//  Get_Event_Info() returns status about what stage of processing
//  this event is in (e.g. cascading).
//----------

Get_Event_Info(c_EventNoTriggerUp)

//----------
//  Make sure that this DataWindow is in use.
//----------

IF NOT i_InUse THEN
   is_EventInfo.Event_Recipient = FALSE
   is_EventInfo.Event_Root      = FALSE
   is_EventInfo.Cascading       = FALSE
   l_Error                     = c_Fatal
   GOTO Finished
END IF

//----------
//  See if this is the first refresh of the DataWindow.
//----------

i_RefreshPosted = FALSE

IF NOT i_OpenModeSet THEN
   i_OpenModeSet = TRUE
   l_ModeOnOpen  = i_ModeOnOpen

   IF i_ParentIsValid THEN
      IF l_ModeOnOpen = c_ModeUndefined THEN
         l_ModeOnOpen = i_ParentDW.i_RequestedMode

         //----------
         //  The developer may have specified a mode to go
         //  to when a row was selected by RowFocusChanged!.
         //  Honor their request.
         //----------

         IF i_ParentDW.i_RefreshMode = c_RefreshRFC THEN
            IF i_ModeOnSelect <> c_ModeUndefined THEN
               l_ModeOnOpen = i_ModeOnSelect
            ELSE
               IF l_ModeOnOpen = c_ModeNew THEN
                  l_ModeOnOpen = c_ModeModify
               END IF
            END IF
         END IF
      END IF
   END IF

   CHOOSE CASE l_ModeOnOpen
      CASE c_ModeModify
         i_RefreshMode  = c_RefreshModify
      CASE c_ModeNew
         i_RefreshMode  = c_RefreshNew
         l_InsRowNbr    = 0
      CASE ELSE
         i_RefreshMode  = c_RefreshView
   END CHOOSE

   l_Level               = 1
END IF

//----------
//  If this DataWindow is the first recipient of the pcd_Refresh
//  event, display the refresh prompt.
//----------

IF is_EventInfo.Event_Recipient THEN
   IF i_RefreshMode <> c_RefreshDrillDown THEN
      l_PopPrompt = TRUE
      PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_RefreshRows, c_ShowMark)
   ELSE
      i_RefreshMode = c_RefreshRFC
      l_PopPrompt   = FALSE
   END IF
END IF

//----------
//  Save the current value of i_IsEmpty as it may be reset by
//  Handle_Retrieve().
//----------

l_IsEmpty = i_IsEmpty

//----------
//  If we have been told to skip this branch of the DataWindow
//  hierarchy, then just exit the event.
//----------

IF l_SkipDWValid   THEN
IF l_SkipDW = THIS THEN
   GOTO Finished
END IF
END IF

//----------
//  If this is the root of the pcd_Refresh event, then turn on
//  the hour glass.
//----------

IF is_EventInfo.Event_Root THEN
   SetPointer(HourGlass!)
END IF

//----------
//  Assume that this event will be successful.
//----------

l_Error = c_Success

//----------
//  If the event is cascading and if this DataWindow is not
//  the root DataWindow of the event (e.g. it is a child
//  of the root DataWindow) or if only-do-children was NOT
//  specified (i.e. the caller specified that refresh was to
//  occur in all DataWindows, including the root DataWindow),
//  then we must do processing.
//----------

IF is_EventInfo.Cascading                              THEN
IF NOT is_EventInfo.Event_Root OR NOT l_OnlyDoChildren THEN

   //----------
   //  Turn off redraw processing.
   //----------

   SetRedraw(c_BufferDraw)
   i_RedrawCount = i_RedrawCount + 1
   i_RedrawStack[i_RedrawCount] = c_BufferDraw

   //----------
   //  Save the modified status of this DataWindow.  This is
   //  used for sanity checking later because we don't want
   //  to be clobbering DataWindows that contain changes.
   //  Update_Modified() insures that the i_Modified flag is
   //  current.
   //----------

   Update_Modified()
   l_Modified = i_SharePrimary.i_ShareModified

   //----------
   //  Indicate that we have not clobbered this DataWindow.
   //----------

   l_OverWritten = FALSE

   //----------
   //  If the caller specified that we were to reset ourselves,
   //  then do it.
   //----------

   IF l_Reset THEN

      //----------
      //  If this DataWindow is not sharing a result set with its
      //  parent DataWindow, then make the DataWindow empty if it
      //  is not already empty.
      //----------

      IF i_ShareType = c_ShareNone THEN
         IF NOT i_IsEmpty THEN

            //----------
            //  The DataWindow was not empty, so we have to make
            //  it empty.  Indicate that we clobbered the
            //  DataWindow's contents.  Also, indicate that we
            //  have done a "retrieve" for this DataWindow. The
            //  EMPTY processing will happen later.
            //----------

            l_OverWritten    = TRUE
            i_RetrieveMySelf = FALSE

            i_IgnoreVScroll = (NOT c_IgnoreVScroll)
            i_IgnoreVSCount = i_IgnoreVSCount + 1
            i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

            Reset()

            IF i_IgnoreVSCount > 1 THEN
               i_IgnoreVSCount = i_IgnoreVSCount - 1
               i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
            ELSE
               i_IgnoreVSCount = 0
               i_IgnoreVScroll = FALSE
            END IF
            IF NOT i_IgnoreVScroll THEN
               i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
            END IF
         END IF
      ELSE

         //----------
         //  Make sure that the DataWindow is in "VIEW" mode.
         //----------

         TriggerEvent("pcd_Disable")

         //----------
         //  We are sharing a result set with our parent so we
         //  can't clear this DataWindow because it would clear
         //  our parent also.  Just make sure that we are
         //  displaying the same row that our parent is on.
         //----------

         IF i_ParentDW.i_CursorRow > 0 THEN

            //----------
            //  Turn RFC processing on for a moment and move to
            //  the same row as our parent.
            //----------

            IF NOT i_MultiRowDisplay THEN
               i_IgnoreRFC      = (NOT c_ProcessRFC)
               i_IgnoreRFCCount = i_IgnoreRFCCount + 1
               i_IgnoreRFCStack[i_IgnoreRFCCount] = c_ProcessRFC

               i_RFCEvent = c_RFC_RowFocusChanged
               i_MoveRow  = i_ShareParent.i_ShowRow
               IF i_MoveRow < 1 THEN
                  i_RFCEvent = c_RFC_NoSelect
                  i_MoveRow  = i_ShareParent.i_CursorRow
               END IF
               TriggerEvent(RowFocusChanged!)

               IF i_IgnoreRFCCount > 1 THEN
                  i_IgnoreRFCCount = i_IgnoreRFCCount - 1
                  i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
               ELSE
                  i_IgnoreRFCCount = 0
                  i_IgnoreRFC      = FALSE
               END IF
            END IF
         END IF
      END IF
   ELSE

      //----------
      //  We do not have to reset the DataWindow, then we have to
      //  do normal processing to make sure that it contains the
      //  correct data.  PCCA.PCMGR.i_DW_NeedRetrieveDW indicates
      //  a DataWindow in which rows have been selected by the
      //  user, but not loaded into its children DataWindows.  We
      //  are about to load children DataWindows now so we can
      //  clear the PCCA.PCMGR.FixNeedRetrieve flag.
      //----------

      IF PCCA.PCMGR.i_DW_NeedRetrieve          THEN
      IF PCCA.PCMGR.i_DW_NeedRetrieveDW = THIS THEN
         PCCA.PCMGR.i_DW_NeedRetrieve = FALSE
      END IF
      END IF

      //----------
      //  Based on the refresh mode, determine on how to proceed.
      //----------

      CHOOSE CASE i_RefreshMode

         //----------
         //  This DataWindow is being changed to "VIEW" mode.
         //----------

         CASE c_RefreshView
            i_RequestedMode = c_ModeView
            l_NextMode      = i_RequestedMode

         //----------
         //  This DataWindow is being changed to "MODIFY" mode.
         //----------

         CASE c_RefreshModify
            i_RequestedMode = c_ModeModify
            l_NextMode      = i_RequestedMode

         //----------
         //  This DataWindow is being changed to "NEW" mode.
         //----------

         CASE c_RefreshNew
            i_RequestedMode = c_ModeNew
            l_NextMode      = i_RequestedMode

         //----------
         //  This DataWindow is being refreshed because the user
         //  selected a new row (i.e. a RowFocusChanged! event
         //  occurred).
         //----------

         CASE c_RefreshRFC

            //----------
            //  RFC mode only applies to the children (and
            //  children's children, etc) of the root DataWindow
            //  of the event (i.e. the DataWindow in which the
            //  user selected a row does not change modes).
            //----------

            IF NOT is_EventInfo.Event_Root THEN

               //----------
               //  The developer may have specified a mode to go
               //  to when a row was selected by RowFocusChanged!.
               //  Honor their request.
               //----------

               IF i_ModeOnSelect <> c_ModeUndefined THEN
                  i_RequestedMode = i_ModeOnSelect
               ELSE
                  IF i_RequestedMode = c_ModeNew THEN
                     i_RequestedMode = c_ModeModify
                  END IF
               END IF
            END IF

            //----------
            //  Tell the DataWindow to go to the requested mode.
            //  Note that the requested mode may be different
            //  than the current mode.  For example, the requested
            //  mode may be "MODIFY", but the current mode could
            //  be "VIEW" because the DataWindow did not contain
            //  any rows.
            //----------

            l_NextMode = i_RequestedMode

         //----------
         //  This DataWindow should be left in the same mode.
         //  Handle_Retrieve() will try to re-select the
         //  currently selected rows.
         //----------

         CASE c_RefreshSame
            l_NextMode = i_RequestedMode

         //----------
         //  This DataWindow is being refreshed because a save
         //  operation has occurred.  Handle_Retrieve() will try
         //  to re-select the currently selected rows.
         //----------

         CASE c_RefreshSave

            //----------
            //  The developer may have specified that we are to
            //  go to "VIEW" mode after a save has occurred.
            //  Honor their request.
            //----------

            IF i_ViewAfterSave THEN
               i_RequestedMode = c_ModeView
            END IF

            l_NextMode = i_RequestedMode
         CASE ELSE
      END CHOOSE

      //----------
      //  If the next mode did not get set, then assume that we
      //  will go to "VIEW" mode.
      //----------

      IF l_NextMode = c_ModeUndefined THEN
         l_NextMode = c_ModeView
      ELSE

         //----------
         //  Unless the refresh mode is c_RefreshNew, we can't
         //  go to "NEW" mode.  If l_NextMode is c_ModeNew, then
         //  change it to c_ModeModify.
         //----------

         IF i_RequestedMode =  c_ModeNew    THEN
         IF i_RefreshMode   <> c_RefreshNew THEN
            i_RequestedMode = c_ModeModify
            l_NextMode      = i_RequestedMode
         END IF
         END IF

         //----------
         //  If we were told to go to "NEW" mode but the
         //  DataWindow does not allow "NEW" mode, then just go
         //  to "MODIFY" mode, if possible.
         //----------

         IF l_NextMode = c_ModeNew THEN
            IF NOT i_AllowNew THEN
               IF NOT i_AllowModify THEN
                  l_NextMode = c_ModeView
               ELSE
                  l_NextMode = c_ModeModify
               END IF
            END IF
         ELSE

            //----------
            //  If we were told to go to "MODIFY" mode but the
            //  DataWindow does not allow "MODIFY" mode, then just
            //  go to "VIEW" mode.
            //----------

            IF l_NextMode = c_ModeModify THEN
            IF NOT i_AllowModify         THEN
               l_NextMode = c_ModeView
            END IF
            END IF
         END IF
      END IF

      //----------
      //  Now we must determine if we need to do a retrieve for
      //  this DataWindow.
      //----------

      IF i_NumReselectRows > 0 OR i_RetrieveMySelf THEN

         //----------
         //  Since we are going to perform a retrieve, we'll
         //  overwrite any modifications in this DataWindow.
         //  Indicate that we're going to clobber the data in
         //  this DataWindow.  Note that if this is a shared
         //  DataWindow, the data really won't get clobbered
         //  (i.e. retrieved).
         //----------

         IF i_ShareType = c_ShareNone THEN
         IF i_RetrieveMySelf          THEN
            l_OverWritten = TRUE
         END IF
         END IF

         //----------
         //  Perform the retrieve.
         //----------

         IF Handle_Retrieve(l_NextMode, l_Level) <> c_Success THEN
            l_Error = c_Fatal
         END IF
      END IF

      //----------
      //  Process_Mode() makes sure the DataWindow is in the
      //  requested mode.
      //----------

      IF l_Error = c_Success THEN
         IF Process_Modes(l_NextMode, l_InsRowNbr) <> c_Success THEN
            l_Error = c_Fatal
         END IF
      END IF
   END IF

   //----------
   //  Get the current number of rows in the DataWindow.
   //----------

   l_RowCount = RowCount()

   //----------
   //  Clear any rows in i_ReselectKeys[].
   //----------

   i_NumReselectRows = 0

   //----------
   //  If a retrieve has happened, then we need to recalculate
   //  the EMPTY processing.
   //----------

   IF i_RetrieveMySelf          THEN
   IF NOT l_Reset               THEN
   IF i_ShareType = c_ShareNone THEN

      //----------
      //  Since a retrieve happened, force reprocessing of
      //  EMPTY.
      //----------

      IF l_RowCount = 0 THEN
         i_IsEmpty = FALSE
      ELSE

         //----------
         //  If we were empty before, we are not empty any
         //  longer: we need to get the non-empty colors back.
         //----------

         IF l_IsEmpty THEN
            Share_State(c_ShareNotEmpty, c_StateUnused)
         END IF
      END IF
   END IF
   END IF
   END IF

   //----------
   //  If this DataWindow does not have any rows, then it is
   //  empty.  If it has not already been marked as empty, we
   //  need to mark it as empty.
   //----------

   IF NOT i_IsEmpty THEN
      IF l_RowCount = 0 THEN

         //----------
         //  pcd_Disable will do EMPTY processing and disable the
         //  DataWindow for modification and turn on the "VIEW"
         //  mode indicators for each field.
         //----------

         l_NextMode = c_ModeView
         i_DWState  = c_DWStateUndefined
         TriggerEvent("pcd_Disable")
      END IF
   ELSE

      //----------
      //  Since the DataWindow is empty, make sure that it gets
      //  placed into "VIEW" mode.
      //----------

      l_NextMode = c_ModeView
   END IF

   //----------
   //  Save our mode.
   //----------

   i_CurrentMode = l_NextMode

   //----------
   //  Return redraw processing to its previous state.
   //----------

   IF i_RedrawCount > 1 THEN
      i_RedrawCount = i_RedrawCount - 1
      SetRedraw(i_RedrawStack[i_RedrawCount])
   ELSE
      i_RedrawCount = 0
      SetRedraw(TRUE)
   END IF

   //----------
   //  Time to see if we are insane.  If the DataWindow contained
   //  changes and we clobbered them, tell the user that we are
   //  losing it.
   //----------

   IF l_Modified    THEN
   IF l_OverWritten THEN
      PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Refresh"
      PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
      PCCA.MB.i_MB_Strings[3] = i_ClassName
      PCCA.MB.i_MB_Strings[4] = i_Window.Title
      PCCA.MB.i_MB_Strings[5] = DataObject
      PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ChangesClobbered, &
                         0, PCCA.MB.i_MB_Numbers[],         &
                         5, PCCA.MB.i_MB_Strings[])
   END IF
   END IF
END IF
END IF

//----------
//  If the event is cascading and has not encounter any errors,
//  we need to continue cascading the event to our children
//  DataWindows.
//----------

IF is_EventInfo.Cascading THEN
IF l_Error = c_Success   THEN

   //----------
   //  Now, we have figure out if we should call the developer's
   //  event to open children DataWindows (i.e. the mis-named
   //  pcd_PickedRow event).  See if the PowerLock security system
   //  says if it is Okay to run picked row for this DataWindow.
   //----------

   IF i_PLDrillDownOk THEN

      //----------
      //  See if PowerClass should figure it out or if the
      //  developer has specified what they want to happen.
      //----------

      IF i_OpenChildOption = c_OpenChildAuto THEN

         //----------
         //  The developer wants PowerClass to figure out if the
         //  pcd_PickedRow event should be triggered.  We should
         //  only trigger the "open windows" event if:
         //     a) l_Level is 0 AND
         //        1) There are rows selected OR
         //        2) We are going to "NEW" mode.
         //----------

         l_DoPickedRow = FALSE

         IF l_Level = 0 THEN

            //----------
            //  If we are going to "NEW" mode, then we don't want
            //  to open children DataWindows for the DataWindows
            //  that are going into "NEW" mode.  However, if
            //  l_Level is 0, then this DataWindow received a
            //  pcd_New event but it does not allow "NEW" mode.
            //  In that case, it wants to pass the "NEW" to its
            //  children and they need to be open for that to
            //  happen.
            //----------

            IF i_RefreshMode = c_RefreshNew THEN
               l_DoPickedRow = TRUE
               IF i_NumInstance > 0 THEN
                  i_ShowRow = 0
                  i_AnchorRow = 0
                  SelectRow(0, FALSE)
                  i_NumSelected = 0
               END IF
            ELSE

               //----------
               //  We check for l_Level to be 0 because we don't
               //  want our children DataWindows (and their
               //  children DataWindows...) to open their children.
               //  If there are rows selected, we still can't be
               //  sure if we should trigger the developer's "open
               //  windows" event.  It depends on what mode we are
               //  refreshing in (e.g. if the refresh mode is
               //  "SAME", we shouldn't start opening windows).
               //----------

               IF i_NumSelected > 0 THEN

                  //----------
                  //  If we told to refresh the DataWindows and
                  //  leave them in the same mode or we are just
                  //  cleaning up after a save, we don't want to
                  //  be opening windows.
                  //----------

                  IF i_RefreshMode <> c_RefreshSame THEN
                  IF i_RefreshMode <> c_RefreshSave THEN

                     //----------
                     //  If we have Instance DataWindow children,
                     //  figure out if there is already a
                     //  DataWindow associated with the currently
                     //  selected row.  If there is, then there is
                     //  no need to tell the developer to open
                     //  another window.
                     //----------

                     l_DoPickedRow = TRUE
                     IF i_NumInstance > 0 THEN
                        Find_Instance(i_ShowRow)
                        IF i_CurInstance > 0 THEN
                           l_DoPickedRow = FALSE
                        END IF
                     END IF
                  END IF
                  END IF
               END IF
            END IF
         END IF
      ELSE
         CHOOSE CASE i_OpenChildOption
            CASE c_OpenChildForce
               l_DoPickedRow     = TRUE
               i_OpenChildOption = c_OpenChildAuto
            CASE c_OpenChildAlwaysForce
               l_DoPickedRow     = TRUE
            CASE c_OpenChildPrevent
               l_DoPickedRow     = FALSE
               i_OpenChildOption = c_OpenChildAuto
            CASE c_OpenChildAlwaysPrevent
               l_DoPickedRow     = FALSE
         END CHOOSE
      END IF

      //----------
      //  If we have figured out that this DataWindow needs to
      //  trigger the "open windows" event and this DataWindow is
      //  not being reset, do it.
      //----------

      IF l_DoPickedRow THEN
      IF NOT l_Reset   THEN

         //----------
         //  Initialize variables for use by the developer.
         //----------

         l_SaveRaise            = PCCA.Raise_Open_Window
         PCCA.Raise_Open_Window = FALSE
         PCCA.Error             = c_Success
         i_DisplayError         = TRUE

         //----------
         //  Despite the misnomer, this event provides the
         //  developer the opportunity to open new windows
         //  (most likely containing children DataWindows of
         //  the current DataWindow).  This event must be coded
         //  by the developer.
         //----------

         TriggerEvent("pcd_PickedRow")

         //----------
         //  Restore PCCA.Raise_Open_Window to its previous value.
         //----------

         PCCA.Raise_Open_Window = l_SaveRaise

         //----------
         //  If there was an error during the "open windows" event
         //  and the developer did not tell us to prevent the an
         //  error display, then display the error.
         //----------

         IF PCCA.Error <> c_Success THEN
            IF i_DisplayError          THEN
               PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Refresh"
               PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
               PCCA.MB.i_MB_Strings[3] = i_ClassName
               PCCA.MB.i_MB_Strings[4] = i_Window.Title
               PCCA.MB.i_MB_Strings[5] = DataObject
               PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_PickedRowError, &
                                  0, PCCA.MB.i_MB_Numbers[],       &
                                  5, PCCA.MB.i_MB_Strings[])
            END IF
         ELSE

            //----------
            //  Since we successfully did the picked row, set the
            //  i_DidPickedRow flag.  This is used to tell the
            //  Clicked! and DoubleClicked! events whether they
            //  need to drill down when the user is clicking
            //  around on the same row.
            //----------

            i_DidPickedRow = TRUE
         END IF
         IF NOT i_HaveInstances THEN
            i_HaveInstances = (i_NumInstance > 0)
         END IF
         IF i_MaxChildren < i_NumChildren THEN
            i_MaxChildren = i_NumChildren
         END IF
      END IF
      END IF
   END IF

   //----------
   //  If we are not just doing a straight reset of the
   //  DataWindows, then we need to do a little processing.
   //----------

   IF NOT l_Reset THEN

      //----------
      //  Bump the level.  Level gets bigger as we move from the
      //  DataWindow that is the root of the event towards the
      //  leaf-level DataWindows.  l_Level is used to coordinate
      //  changing modes.
      //----------

      l_Level = l_Level + 1

      //----------
      //  If we are going to "NEW" mode and level is greater than
      //  1, then we just want to reset the children DataWindows.
      //  Only DataWindows at level are placed into "NEW" mode.
      //----------

      IF i_RefreshMode = c_RefreshNew THEN
         IF l_Level > 1 THEN
            i_RetrieveChildren = FALSE  //  Added - 940704.
            l_Reset            = TRUE
         END IF
      ELSE

         //----------
         //  For all refresh modes except "NEW", if there are not
         //  any rows selected, the only thing to do is reset the
         //  children DataWindows.
         //----------

         IF i_NumSelected = 0 THEN

            //----------
            //  Added - 940704.
            //  If we are being refreshed because of RFC and the
            //  DataWindow was not retrieved (i.e. the user
            //  clicked the same row), we don't want to reset
            //  the children DataWindows.
            //----------

            IF i_RefreshMode <> c_RefreshRFC OR &
               i_RetrieveMySelf THEN
               i_RetrieveChildren = FALSE
               l_Reset            = TRUE
            END IF
         END IF
      END IF
   END IF

   //----------
   //  We have finished processing this DataWindow.  Now we must
   //  cascade the event to our children DataWindows.  However,
   //  before we do that, we need to see if we were told to skip
   //  the children DataWindows of this DataWindow (i.e. the
   //  event that originally triggered pcd_Refresh will take care
   //  of refreshing them later).
   //----------

   l_SkipChildren = FALSE
   IF l_SkipDWChildrenValid   THEN
   IF l_SkipDWChildren = THIS THEN
      l_SkipChildren = TRUE
   END IF
   END IF

   //----------
   //  We have to refresh both "normal" children DataWindows and
   //  Instance children DataWindows.  On the first pass of the
   //  loop we deal with the "normal" children and the Instance
   //  children on the second loop.
   //----------

   FOR l_Kdx = 1 TO 2

      //----------
      //  Abort processing if there are errors.
      //----------

      IF l_Error <> c_Success THEN
         EXIT
      END IF

      //----------
      //  On the first loop, set up the FOR indices to loop
      //  through all of the normal children DataWindows.
      //----------

      IF l_Kdx = 1 THEN
         l_Start = 1
         l_End   = i_NumChildren
      ELSE

         //----------
         //  Assume that we just need to refresh the current
         //  Instance DataWindow (i.e. the Instance DataWindow
         //  associated with the currently selected row), as
         //  opposed to all of the Instance children DataWindows.
         //----------

         l_AllInstances = FALSE

         //----------
         //  If we are refreshing in the same mode or cleaning up
         //  after a save we have to refresh all of the children
         //  Instance Windows.  Set up the FOR indices to reflect
         //  this and set l_AllInstances to TRUE.
         //----------

         IF i_RefreshMode = c_RefreshSame OR &
            i_RefreshMode = c_RefreshSave THEN
            l_Start = 1
            l_End   = i_NumInstance
            IF i_NumInstance > 0 THEN
               l_AllInstances = TRUE
            END IF
         ELSE

            //----------
            //  We only have to refresh the current Instance
            //  DataWindow (if any).
            //----------

            IF i_CurInstance > 0 THEN
               l_Start = i_CurInstance
               l_End   = i_CurInstance
            ELSE

               //----------
               //  There is not an Instance DataWindow associated
               //  with the currently selected row.  Set the FOR
               //  indices to blow out immediately.
               //----------

               l_Start = 1
               l_End   = 0
            END IF
         END IF
      END IF

      //----------
      //  The FOR indices have been set up to cycle through
      //  the necessary children DataWindows.
      //----------

      FOR l_Idx = l_Start TO l_End

         //----------
         //  Abort processing if there are errors.
         //----------

         IF l_Error <> c_Success THEN
            EXIT
         END IF

         //----------
         //  The first pass of the children DataWindows are the
         //  "normal" ones.
         //----------

         l_DoReset = l_Reset
         IF l_Kdx = 1 THEN
            l_ChildDW = i_ChildDW[l_Idx]
         ELSE

            //----------
            //  The second pass of the children DataWindows are
            //  the Instance DataWindows.  If we have to refresh
            //  all of the Instance DataWindows, we need to set
            //  the i_SelectedRows[] array up so that each
            //  Instance DataWindow "sees" the row that is
            //  associated with is selected.
            //----------

            IF l_AllInstances THEN
               l_ChildDW = i_InstanceDW[l_Idx]

               //----------
               //  Find the row that is associated with this
               //  Instance DataWindow.
               //----------

               l_Jdx = Find_Key_Row(1,                          &
                                    l_ChildDW.i_KeysInParent[], &
                                    1,                          &
                                    RowCount())

               //----------
               //  If we found a valid row, then place it in the
               //  i_SelectedRows[] array so that the Instance
               //  DataWindow will see it and retrieve itself
               //  correctly.  If we didn't find it...oh well.
               //----------

               l_DoReset         = FALSE
               IF l_Jdx > 0 THEN
                  i_NumSelected     = 1
                  i_SelectedRows[1] = l_Jdx
               ELSE
                  IF i_NumSelected = 0 THEN
                     l_DoReset     = TRUE
                  END IF
               END IF
            ELSE

               //----------
               //  We only have to retrieve the Instance
               //  DataWindow that is associated with the
               //  currently selected row, which implies that
               //  i_SelectedRows[] is already set up correctly.
               //----------

               l_ChildDW = i_InstanceDW[l_Idx]
            END IF
         END IF

         //----------
         //  We don't want to force retrieval if this is the
         //  first refresh - the initial refresh should do it.
         //----------

         IF NOT l_ChildDW.i_SkipFirstRefresh THEN

            //----------
            //  Mark the child as being in need of a retrieve if
            //  it is not doing its initial pcd_Refresh AND:
            //     a) This DataWindow has specified that its
            //        children should be retrieved OR
            //     b) The children are to be reset.
            //----------

            IF i_RetrieveChildren THEN
               IF l_Kdx = 1 THEN
                  l_ChildDW.i_RetrieveMySelf = TRUE
               END IF
            ELSE
               IF l_DoReset THEN
                  l_ChildDW.i_RetrieveMySelf = TRUE
               END IF
            END IF

            //----------
            //  Make sure that it is Ok to refresh the children of
            //  this DataWindow.
            //----------

            IF NOT l_SkipChildren THEN

               //----------
               //  Set up the Event Control structure for the child
               //  to tell pcd_Refresh how to operate.
               //----------

               l_ChildDW.is_EventControl.Refresh_Cmd            = &
                  i_RefreshMode
               l_ChildDW.is_EventControl.Reset                  = &
                  l_DoReset
               l_ChildDW.is_EventControl.Level                  = &
                  l_Level
               l_ChildDW.is_EventControl.Insert_Row_Nbr         = &
                  l_InsRowNbr
               l_ChildDW.is_EventControl.Cascading              = &
                  TRUE
               l_ChildDW.is_EventControl.Skip_DW_Valid          = &
                  l_SkipDWValid
               l_ChildDW.is_EventControl.Skip_DW                = &
                  l_SkipDW
               l_ChildDW.is_EventControl.Skip_DW_Children_Valid = &
                  l_SkipDWChildrenValid
               l_ChildDW.is_EventControl.Skip_DW_Children       = &
                  l_SkipDWChildren

               //----------
               //  Cascade the pcd_Refresh event to the children.
               //----------

               l_ChildDW.TriggerEvent("pcd_Refresh")

               //----------
               //  If an error occurred in the child, remember it
               //  into our local.
               //----------

               IF PCCA.Error <> c_Success THEN
                  l_Error = c_Fatal
               END IF
            END IF
         ELSE
            l_ChildDW.i_SkipFirstRefresh = FALSE
         END IF
      NEXT
   NEXT

   //----------
   //  If we refreshed all of the instance DataWindows, we need
   //  to restore i_SelectedRows[].
   //----------

   IF l_AllInstances THEN
      i_NumSelected = Get_Retrieve_Rows(i_SelectedRows[])
   END IF
END IF
END IF

//----------
//  If the event is cascading and if this DataWindow is not
//  the root DataWindow of the event (e.g. it is a child
//  of the root DataWindow) or if only-do-children was NOT
//  specified (i.e. the caller specified that refresh was to occur
//  in all DataWindows, including the root DataWindow), then we
//  need to trigger the pcd_<mode> event for the developer..
//----------

IF is_EventInfo.Cascading                                THEN
IF l_Error = c_Success                                  THEN
IF (NOT is_EventInfo.Event_Root OR NOT l_OnlyDoChildren) THEN

   //----------
   //  For the developer, we set the instance variables to reflect
   //  the state of processing by this event.
   //----------

   i_IsFinal     = is_EventInfo.Event_Recipient
   i_IsRoot      = is_EventInfo.Event_Root
   i_IsCascading = is_EventInfo.Cascading

   //----------
   //  Initialize variables for use by the developer.
   //----------

   PCCA.Error    = c_Success
   i_ProcessMode = TRUE

   //----------
   //  Trigger the pcd_Modify, pcd_New, or pcd_View event
   //  depending on our current mode to allow the developer
   //  to do any processing they want to do.
   //----------

   CHOOSE CASE i_CurrentMode
      CASE c_ModeModify
         TriggerEvent("pcd_Modify")
      CASE c_ModeNew
         TriggerEvent("pcd_New")
      CASE c_ModeView
         TriggerEvent("pcd_View")
   END CHOOSE

   //----------
   //  Restore variables after the developer is done.
   //----------

   i_ProcessMode = FALSE

   //----------
   //  Check for errors from the developer.
   //----------

   IF PCCA.Error <> c_Success THEN
      l_Error = c_Fatal
   END IF
END IF
END IF
END IF

IF is_EventInfo.Cascading THEN

   //----------
   //  If the pcd_Refresh went without error, we can clean some
   //  things up.
   //----------

   IF l_Error = c_Success THEN

      //----------
      //  Since the event was successful, we have been
      //  successfully retrieved if necessary.  Indicate that
      //  this DataWindow no longer needs to be retrieved.
      //----------

      i_RetrieveMySelf = FALSE

      //----------
      //  If we told our children to retrieve themselves, then we
      //  can clear this DataWindow's flag indicating that its
      //  children need to be retrieved.
      //----------

      IF NOT l_Reset        THEN
      IF NOT l_SkipChildren THEN
         i_RetrieveChildren = FALSE
      END IF
      END IF
   ELSE

      //----------
      //  Since there was an error, remind ourselves that we need
      //  retrieve our children the next time pcd_Refresh is
      //  attempted.
      //----------

      i_RetrieveChildren = TRUE
   END IF
END IF

//----------
//  If this DataWindow is the root of the event, there was an
//  error, and we did not just reset our children, then trigger
//  pcd_Refresh on this DataWindow to reset all of the children.
//  For example, if a retrieve generates an error, we want to
//  reset all of our children.
//----------

IF is_EventInfo.Event_Root  THEN
IF l_Error <> c_Success    THEN
IF NOT l_Reset             THEN
   PCCA.Do_Event                    = TRUE
   is_EventControl.Cascading        = TRUE
   is_EventControl.Refresh_Cmd      = c_RefreshSame
   is_EventControl.Reset            = TRUE
   is_EventControl.Only_Do_Children = TRUE
   TriggerEvent("pcd_Refresh")
   Change_DW_Focus(THIS)
END IF
END IF
END IF

Finished:

//----------
//  If this DataWindow is in use, then there is a little cleanup
//  that we need to do.
//----------

IF i_InUse THEN

   //----------
   //  If this was the recipient of the event, then we need to
   //  set controls so that they reflect any rows that have been
   //  loaded or reset.
   //----------

   IF is_EventInfo.Event_Recipient THEN
      is_EventControl.Control_Mode = c_ControlRows
      TriggerEvent("pcd_SetControl")

      IF l_PopPrompt THEN
         PCCA.MDI.fu_Pop()
      END IF
   END IF
END IF

//----------
//  l_Error contains failure if there was a failure at any
//  point during the pcd_Refresh.  Store it into PCCA.Error
//  so that the outside world can see the result.
//----------

PCCA.Error = l_Error
PCCA.MDI.fu_Pop()

//----------
//  For the developer, we set the instance variables to reflect
//  the state of processing by this event.
//----------

i_IsFinal     = is_EventInfo.Event_Recipient
i_IsRoot      = is_EventInfo.Event_Root
i_IsCascading = is_EventInfo.Cascading
i_ExtendMode  = is_EventInfo.Cascading

//----------
//  Decrement the refresh count.  When all the cascaded
//  pcd_Refreshs are done, this will tell the RowFocusChanged!
//  event that it is Ok to trigger pcd_Refresh again.
//----------

IF PCCA.PCMGR.i_DW_InRefresh > 1 THEN
   PCCA.PCMGR.i_DW_InRefresh = PCCA.PCMGR.i_DW_InRefresh - 1
ELSE
   PCCA.PCMGR.i_DW_InRefresh = 0
END IF
end on

on pcd_rollback;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Rollback
//  Description   : Rollback the changes made to the database since
//                  the last commit.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Indicate that the rollback has been done.
//----------

i_DidRollback = TRUE

//----------
//  If this an external DataWindow, bypass this event.
//----------

IF IsNull(i_DBCA) THEN
   GOTO Finished
END IF

//----------
//  Rollback the changes made to the database since the last
//  COMMIT.
//----------

ROLLBACK USING i_DBCA;

//----------
//  Check for errors that occurred during the ROLLBACK.
//----------

IF i_DBCA.SQLCode <> 0 THEN
   PCCA.Error = c_Fatal
END IF

Finished:

i_ExtendMode = i_InUse
end on

on pcd_save;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Save
//  Description   : Saves the changes for this DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_NoRefreshAfterSave, l_AddedPrimaryReselect
BOOLEAN       l_First,              l_DisplayError
INTEGER       l_Error,              l_ColNbr
LONG          l_RowCount,           l_Idx,    l_Jdx
UNSIGNEDLONG  l_MBID
REAL          l_MBNumbers[]
STRING        l_Modify,             l_Result, l_Col
STRING        l_ReselectKeys[],     l_MBStrings[]
DWITEMSTATUS  l_ItemStatus
UO_DW_MAIN    l_ChildDW

//----------
//  Indicate that we are in save.
//----------

i_InSave = i_InSave + 1

//----------
//  Save the options passed to us in the Event Control structure.
//----------

l_NoRefreshAfterSave = is_EventControl.No_Refresh_After_Save

//----------
//  Get_Event_Info() returns status about what stage of processing
//  this event is in (e.g. cascading).
//----------

Get_Event_Info(c_EventGotoRoot)

//----------
//  Make sure that this DataWindow is in use.
//----------

IF NOT i_InUse THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If we are not the root-level DataWindow, then we need to pass
//  the event to the root-level DataWindow.
//----------

IF is_EventInfo.Trigger_Up THEN
   PCCA.Do_Event                                  = TRUE
   i_RootDW.is_EventControl.Cascading             = TRUE
   i_RootDW.is_EventControl.No_Refresh_After_Save =  &
      l_NoRefreshAfterSave
   i_RootDW.TriggerEvent("pcd_Save")
   GOTO Finished
END IF

//----------
//  If we are the root of the event, then get ready to do the
//  save process.
//----------

IF is_EventInfo.Event_Root THEN

   //----------
   //  Set the hour glass and clear PCCA.Error.
   //----------

   SetPointer(HourGlass!)
   PCCA.Error = c_Success

   //----------
   //  Tell the MDI object to add the save prompt to the stack,
   //  but don't display it.  The validation checking message will
   //  be displayed and when it gets poped, the save prompt will
   //  ready and waiting.
   //----------

   PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_Save, c_Invisible)

   //----------
   //  Validate the data before saving.  If there is a validation
   //  error, blow out of here.
   //----------

   TriggerEvent("pcd_Validate")

   //----------
   //  Check for a validation error and exit if there is one.
   //----------

   IF PCCA.Error <> c_Success THEN
      PCCA.MDI.fu_Pop()
      GOTO Finished
   END IF

   //----------
   //  Make sure the save prompt is displayed.
   //----------

   PCCA.MDI.fu_Visible(c_ShowMark)

   //----------
   //  Set PCCA.PCMGR.i_DW_FocusDone to FALSE to indicate
   //  that focus has not set.  If there is an error and
   //  focus has not been set, pcd_Save will set focus.
   //----------

   PCCA.PCMGR.i_DW_FocusDone = FALSE
END IF

IF is_EventInfo.Cascading THEN

   //----------
   //  If a ROLLBACK occurs in a child DataWindow due to
   //  concurrency errors, we may have to attempt to save the
   //  DataWindow multiple times.  Set i_RetrySave to TRUE so
   //  that we make it through the loop at least once.
   //----------

   i_RetrySave = TRUE

   DO WHILE i_RetrySave

      //----------
      //  Reset i_RetrySave
      //----------

      i_RetrySave = FALSE

      //----------
      //  Clear the i_Rollback flag to indicate that a ROLLBACK
      //  has not occurred.
      //----------

      IF is_EventInfo.Event_Root THEN
         i_DidRollback = FALSE

         //----------
         //  PCCA.Error may have been set by concurrecny error
         //  processing during an prior attempt to update.  Make
         //  sure that it is cleared.
         //----------

         PCCA.Error = c_Success
      END IF

      //----------
      //  The pcd_SaveBefore event provides the developer with a
      //  hook to do things before the database has been updated.
      //  This event is only triggered in the root-level
      //  DataWindow.
      //----------

      IF is_EventInfo.Event_Root THEN

         //----------
         //  Initialize variables for use by the developer.
         //----------

         i_DisplayError = TRUE

         //----------
         //  Reset the DB Error codes.
         //----------

         i_CCDBError         = FALSE
         i_DBErrorCode       = 0
         i_DBErrorMessage    = ""
         i_DBErrorRow        = 0
         i_DevDBErrorMessage = ""

         //----------
         //  Trigger the developer's event to do processing before
         //  any of the DataWindows are updated.
         //----------

         TriggerEvent("pcd_SaveBefore")

         //----------
         //  If there was an error, the developer is responsible
         //  for rolling back the transaction.  Just display a
         //  message, if requested.
         //----------

         IF PCCA.Error <> c_Success THEN
            IF i_DisplayError THEN
               l_MBID         = PCCA.MB.c_MBI_DW_SaveBeforeError
               l_MBNumbers[1] = i_DBErrorCode
               l_MBStrings[1] = "pcd_SaveBefore"
               l_MBStrings[2] = PCCA.Application_Name
               l_MBStrings[3] = i_ClassName
               l_MBStrings[4] = i_Window.Title
               l_MBStrings[5] = DataObject
               l_MBStrings[6] = i_DBErrorMessage
               l_MBStrings[7] = i_DevDBErrorMessage
               PCCA.MB.fu_MessageBox(l_MBID,           &
                                  1, l_MBNumbers[], &
                                  7, l_MBStrings[])
            END IF

            //----------
            //  Make sure that PCCA.Error still indicates
            //  failure.
            //----------

            PCCA.Error = c_Fatal
         END IF
      END IF

      //----------
      //  If we are doing a share, then the primary share
      //  DataWindow will take care of setting keys and
      //  updating to the database.  No need to do it again.
      //----------

      IF PCCA.Error  = c_Success   THEN
      IF i_ShareType = c_ShareNone THEN

         //----------
         //  Determine if this DataWindow has been modified.
         //  Update_Modified() insures that the i_Modified flag
         //  is current.
         //----------

         Update_Modified()

         //----------
         //  See if we need to trigger the developer's event to
         //  set keys for new rows.  We need to trigger it if the
         //  DataWindow has been modified and new rows have been
         //  added.
         //----------

         IF i_SharePrimary.i_ShareModified THEN
         IF i_SharePrimary.i_ShareDoSetKey THEN
         IF PCCA.Error = c_Success         THEN

            //----------
            //  Initialize variables for use by the developer.
            //----------

            i_DisplayError = TRUE

            //----------
            //  Reset the DB Error codes.
            //----------

            i_CCDBError         = FALSE
            i_DBErrorCode       = 0
            i_DBErrorMessage    = ""
            i_DBErrorRow        = 0
            i_DevDBErrorMessage = ""

            //----------
            //  This event provides the developer the opportunity
            //  to set values for the key columns in the
            //  DataWindow.  If this event is used, it must be
            //  coded by the developer.
            //----------

            TriggerEvent("pcd_SetKey")

            //----------
            //  If there was an error while setting the keys,
            //  then we may need to rollback the transaction.
            //----------

            IF PCCA.Error <> c_Success THEN

               //----------
               //  Save the flag which indicates if the developer
               //  wanted us to display the error.
               //----------

               l_DisplayError = i_DisplayError

               //----------
               //  Save the fu_MessageBox() parms as the
               //  information may get cleared by the
               //  rollback process.
               //----------

               IF l_DisplayError THEN
                  l_MBID         = PCCA.MB.c_MBI_DW_SetKeyError
                  l_MBNumbers[1] = i_DBErrorCode
                  l_MBStrings[1] = "pcd_SetKey"
                  l_MBStrings[2] = PCCA.Application_Name
                  l_MBStrings[3] = i_ClassName
                  l_MBStrings[4] = i_Window.Title
                  l_MBStrings[5] = DataObject
                  l_MBStrings[6] = i_DBErrorMessage
                  l_MBStrings[7] = i_DevDBErrorMessage
               END IF

               //----------
               //  Initialize variables for use by the developer.
               //----------

               PCCA.Error              = c_Success
               i_RootDW.i_DisplayError = TRUE

               //----------
               //  Make sure that the rollback has not already
               //  been done.
               //----------

               IF NOT i_RootDW.i_DidRollback THEN

                  //----------
                  //  Reset the DB Error codes.
                  //----------

                  i_RootDW.i_CCDBError         = FALSE
                  i_RootDW.i_DBErrorCode       = 0
                  i_RootDW.i_DBErrorMessage    = ""
                  i_RootDW.i_DBErrorRow        = 0
                  i_RootDW.i_DevDBErrorMessage = ""

                  //----------
                  //  Trigger the event to rollback the database.
                  //----------

                  i_RootDW.i_DidRollback = TRUE
                  i_RootDW.TriggerEvent("pcd_Rollback")
               END IF

               //----------
               //  Save the error from the rollback.
               //----------

               l_Error = PCCA.Error

               //----------
               //  There was an error earlier but we did not
               //  display it because we didn't want to hold
               //  up the ROLLBACK process.  Display the error
               //  now if the developer requested it.
               //----------

               IF l_DisplayError THEN
                  PCCA.MB.fu_MessageBox(l_MBID,           &
                                     1, l_MBNumbers[], &
                                     7, l_MBStrings[])
               END IF

               //----------
               //  If there was an error while rolling back the
               //  database and we were not told to prevent the
               //  error display, then display the error.
               //----------

               IF l_Error <> c_Success THEN
               IF i_DisplayError       THEN
                  l_MBID         = &
                     PCCA.MB.c_MBI_DW_RollbackError

                  l_MBNumbers[1] = i_RootDW.i_DBErrorCode
                  l_MBStrings[1] = "pcd_Rollback"
                  l_MBStrings[2] = PCCA.Application_Name
                  l_MBStrings[3] = i_RootDW.i_ClassName
                  l_MBStrings[4] = i_RootDW.i_Window.Title
                  l_MBStrings[5] = i_RootDW.DataObject
                  l_MBStrings[6] = i_RootDW.i_DBErrorMessage
                  l_MBStrings[7] = i_RootDW.i_DevDBErrorMessage
                  PCCA.MB.fu_MessageBox(l_MBID,           &
                                              1, l_MBNumbers[], &
                                              7, l_MBStrings[])
               END IF
               END IF

               //----------
               //  Make sure that PCCA.Error still indicates
               //  failure.
               //----------

               PCCA.Error = c_Fatal
            ELSE
               //----------
               //  If this is a new instance we need to set the keys of the row
               //  because it could not be done in Set_DW_Options.
               //----------

               IF i_IsInstance THEN

                  //----------
                  //  For every key column, get the keys for
                  //  the new row.
                  //----------

                  IF NOT i_GotColInfo THEN
                     Get_Col_Info()
                  END IF

                  FOR l_Idx = 1 TO i_xNumKeyColumns
                     l_ColNbr              = i_xKeyColumns[l_Idx]
                     i_KeysInParent[l_Idx] = Get_Col_Data_Ex &
                                             (1, l_ColNbr, &
                                             Primary!, c_GetCurrentValues)
                  NEXT
               END IF
            END IF
         END IF
         END IF
         END IF

         //----------
         //  If this DataWindow has been modified and there have
         //  not been any errors, we need to trigger the event
         //  to update the database.
         //----------

         IF i_SharePrimary.i_ShareModified THEN
         IF PCCA.Error = c_Success         THEN

            //----------
            //  Since the developer may modify the column
            //  information and we will need it later, make
            //  sure that we have it now.
            //----------

            IF NOT i_GotColInfo THEN
               Get_Col_Info()
            END IF

            //----------
            //  We now need to update the modified rows to the
            //  database.  During this process, we check for
            //  concurrency errors.  We reset the number of
            //  UPDATES() that have happened since we are just
            //  starting.
            //----------

            i_CCMaxNumUpdate = 0

            //----------
            //  We set i_CCDBError to TRUE to force us through
            //  the UPDATE() loop at least once.
            //----------

            i_CCDBError = TRUE

            //----------
            //  Attempt the update.
            //----------

            DO WHILE PCCA.Error = c_Success AND i_CCDBError

               //----------
               //  Initialize variables for use by the developer.
               //----------

               i_DisplayError = TRUE

               //----------
               //  Reset the DB Error codes.
               //----------

               i_CCDBError         = FALSE
               i_DBErrorCode       = 0
               i_DBErrorMessage    = ""
               i_DBErrorRow        = 0
               i_DevDBErrorMessage = ""

               //----------
               //  Reset the number of UPDATE()'s we have done.
               //----------

               i_CCWhichUpdate  = 0

               //----------
               //  Indicate that we are in the process of updating.
               //----------

               i_InUpdate = i_InUpdate + 1

               //----------
               //  Trigger the event to update the rows to the
               //  database.  By default, this event uses the
               //  PowerBuilder UPDATE() routine to save changes
               //  to the database.  If stored procedures are
               //  used or the developer wants to do their own
               //  updating, they will need to code this event.
               //
               //  If the updating fails, the pcd_Rollback event
               //  will be triggered to rollback any changes to
               //  the database since the last commit.  If the
               //  developer wants to do their own rollback
               //  procedure, they must code the pcd_Rollback
               //  event.
               //----------

               TriggerEvent("pcd_Update")

               //----------
               //  Indicate that we are done updating.
               //----------

               IF i_InUpdate > 1 THEN
                  i_InUpdate = i_InUpdate - 1
               ELSE
                  i_InUpdate = 0
               END IF

               //----------
               //  If errors are due to collisions, get ready to
               //  throw them to the developer for processing.
               //----------

               IF i_CCDBError THEN
                  TriggerEvent("pcd_PCCCError")
               END IF

               //----------
               //  If UPDATE() was called more than once, then
               //  restore the table and key and update columns
               //  because they may have been changed by the
               //  developer in the pcd_Update event.
               //----------

               IF i_CCWhichUpdate > 1 THEN

                  //----------
                  //  Restore the update table.
                  //----------

                  IF i_UpdateTable <> "" THEN
                     l_Modify = "DataWindow.Table.UpdateTable='" + &
                                i_UpdateTable + "'"
                     l_Result = Modify(l_Modify)
                  END IF

                  IF NOT i_SetDBStrings THEN
                     i_SetDBStrings  = TRUE
                     i_ResetDBUpdate = ""
                     l_Jdx           = 1
                     l_First         = TRUE

                     FOR l_Idx = 1 TO i_NumColumns
                        IF l_First THEN
                           l_Col = "#" + String(l_Idx)
                        ELSE
                           l_Col = "~t#" + String(l_Idx)
                        END IF

                        //----------
                        //  Restore key columns.
                        //----------

                        IF l_Jdx <= i_xNumKeyColumns THEN
                           IF i_xKeyColumns[l_Jdx] = l_Idx THEN
                              l_Jdx           = l_Jdx + 1
                              i_ResetDBUpdate = i_ResetDBUpdate + &
                                                l_Col + ".Key=Yes"
                           ELSE
                              i_ResetDBUpdate = i_ResetDBUpdate + &
                                                l_Col + ".Key=No"
                           END IF
                        ELSE
                           i_ResetDBUpdate = i_ResetDBUpdate + &
                                             l_Col + ".Key=No"
                        END IF

                        IF l_First THEN
                           l_First = FALSE
                           l_Col   = "~t" + l_Col
                        END IF

                        //----------
                        //  Restore update columns.
                        //----------

                        IF i_xColumnUpdate[l_Idx] THEN
                           i_ResetDBUpdate = i_ResetDBUpdate + &
                                             l_Col + ".Update=Yes"
                        ELSE
                           i_ResetDBUpdate = i_ResetDBUpdate + &
                                             l_Col + ".Update=No"
                        END IF
                     NEXT
                  END IF

                  l_Result = Modify(i_ResetDBUpdate)
               END IF
            LOOP

            //----------
            //  If we not going to retry the save, then reset the
            //  instance variables used to limit the maximum number
            //  of retries for concurrency errors.
            //----------

            IF NOT i_RootDW.i_RetrySave THEN
            IF i_CCWhichUpdate > 0      THEN
               i_CCLastBadRow[i_CCWhichUpdate] = 0
               i_CCNumRetry[i_CCWhichUpdate]   = 0
            END IF
            END IF

            //----------
            //  If there was an error while updating to the
            //  database, then we may need to rollback the
            //  transaction.
            //----------

            IF PCCA.Error <> c_Success THEN

               //----------
               //  Save the flag which indicates if the developer
               //  wanted us to display the error.
               //----------

               l_DisplayError = i_DisplayError

               //----------
               //  Save the fu_MessageBox() parms as the
               //  information may get cleared by the
               //  rollback process.
               //----------

               IF l_DisplayError THEN
                  l_MBID         = PCCA.MB.c_MBI_DW_UpdateError
                  l_MBNumbers[1] = i_DBErrorCode
                  l_MBStrings[1] = "pcd_Update"
                  l_MBStrings[2] = PCCA.Application_Name
                  l_MBStrings[3] = i_ClassName
                  l_MBStrings[4] = i_Window.Title
                  l_MBStrings[5] = DataObject
                  l_MBStrings[6] = i_DBErrorMessage
                  l_MBStrings[7] = i_DevDBErrorMessage
               END IF

               //----------
               //  Initialize variables for use by the developer.
               //----------

               PCCA.Error              = c_Success
               i_RootDW.i_DisplayError = TRUE

               //----------
               //  Make sure that the rollback has not already
               //  been done.
               //----------

               IF NOT i_RootDW.i_DidRollback THEN

                  //----------
                  //  Added - 950205.
                  //  Reset the DB Error codes.
                  //----------

                  i_RootDW.i_CCDBError         = FALSE
                  i_RootDW.i_DBErrorCode       = 0
                  i_RootDW.i_DBErrorMessage    = ""
                  i_RootDW.i_DBErrorRow        = 0
                  i_RootDW.i_DevDBErrorMessage = ""

                  //----------
                  //  Trigger the event to rollback the database.
                  //----------

                  i_RootDW.i_DidRollback = TRUE
                  i_RootDW.TriggerEvent("pcd_Rollback")
               END IF

               //----------
               //  Save the error from the rollback.
               //----------

               l_Error = PCCA.Error

               //----------
               //  There was an error earlier but we did not
               //  display it because we didn't want to hold
               //  up the ROLLBACK process.  Display the error
               //  now if the developer requested it.
               //----------

               IF l_DisplayError THEN
                  PCCA.MB.fu_MessageBox(l_MBID,           &
                                     1, l_MBNumbers[], &
                                     7, l_MBStrings[])
               END IF

               //----------
               //  If there was an error while rolling back the
               //  database and we were not told to prevent the
               //  error display, then display the error.
               //----------

               IF l_Error <> c_Success THEN
               IF i_DisplayError       THEN
                  l_MBID         = &
                     PCCA.MB.c_MBI_DW_RollbackError
                  l_MBNumbers[1] = i_RootDW.i_DBErrorCode
                  l_MBStrings[1] = "pcd_Rollback"
                  l_MBStrings[2] = PCCA.Application_Name
                  l_MBStrings[3] = i_RootDW.i_ClassName
                  l_MBStrings[4] = i_RootDW.i_Window.Title
                  l_MBStrings[5] = i_RootDW.DataObject
                  l_MBStrings[6] = i_RootDW.i_DBErrorMessage
                  l_MBStrings[7] = i_RootDW.i_DevDBErrorMessage
                  PCCA.MB.fu_MessageBox(l_MBID,           &
                                              1, l_MBNumbers[], &
                                              7, l_MBStrings[])
               END IF
               END IF

               //----------
               //  Make sure that PCCA.Error still indicates
               //  failure.
               //----------

               PCCA.Error = c_Fatal
            END IF
         END IF
         END IF

         //----------
         //  Change focus to ourselves if we have an error.  Note
         //  that Change_DW_Focus() will raise the window if
         //  necessary.
         //----------

         IF NOT PCCA.PCMGR.i_DW_FocusDone THEN
         IF PCCA.Error <> c_Success       THEN
            Change_DW_Focus(THIS)
         END IF
         END IF
      END IF
      END IF

      //----------
      //  Tell the children to save their data.
      //----------

      FOR l_Idx = 1 TO i_NumChildren
         IF PCCA.Error <> c_Success THEN
            EXIT
         END IF
         l_ChildDW = i_ChildDW[l_Idx]
         l_ChildDW.is_EventControl.Cascading = TRUE
         l_ChildDW.TriggerEvent("pcd_Save")
      NEXT

      //----------
      //  Tell the Instance children to save their data.
      //----------

      FOR l_Idx = 1 TO i_NumInstance
         IF PCCA.Error <> c_Success THEN
            EXIT
         END IF
         l_ChildDW = i_InstanceDW[l_Idx]
         l_ChildDW.is_EventControl.Cascading = TRUE
         l_ChildDW.TriggerEvent("pcd_Save")
      NEXT
   LOOP

   //----------
   //  See if we were modified or got marked for retrieval (e.g.
   //  one of our children has refresh parent specified).
   //----------

   IF (i_SharePrimary.i_ShareModified OR i_RetrieveMySelf) THEN
   IF PCCA.Error = c_Success                               THEN

      //----------
      //  If we are supposed to refresh our parent, then mark our
      //  parent for retrieval.
      //----------

      IF i_RefreshParent THEN
         IF i_ShareParent <> i_ParentDW THEN
            i_ParentDW.i_RetrieveMySelf = TRUE
         END IF

         //----------
         //  If there were NewModified! records, then we want them
         //  to be reselected in our parent DataWindow when it
         //  retrieves itself.  FInd the NewModified! records and
         //  add their keys to the reselection list.
         //----------

         IF i_DoSetKey THEN

            //----------
            //  Make sure the keys in this DataWindow have been
            //  mapped to those in the parent DataWindow.
            //----------

            Map_Keys()

            //----------
            //  If we are able to map the keys in this DataWindow
            //  to the keys in the parent, then we can tell the
            //  parent what rows we want to be re-selected after
            //  it does its retrieve.
            //----------

            IF i_KeyMapValid THEN

               //----------
               //  In case our parent does not do multi-selects, we
               //  need to tell it what row to re-select if nobody
               //  else has already told it.
               //----------

               IF NOT i_ParentDW.i_MultiSelect THEN
               IF i_NumReselectRows = 0        THEN
                  l_AddedPrimaryReselect = TRUE
                  l_Idx                  = i_CursorRow

                  FOR l_Jdx = 1 TO i_ParentDW.i_xNumKeyColumns
                     l_ReselectKeys[l_Jdx] = &
                          Get_Col_Data_Ex    &
                             (l_Idx,    i_xKeyMap[l_Jdx], &
                              Primary!, c_GetCurrentValues)
                  NEXT
                  i_ParentDW.Add_Reselection(l_ReselectKeys[])
               END IF
               END IF

               //----------
               //  Find all of the rows that have been added
               //  in this DataWindow and tell the parent that
               //  it needs to reselect them after it does its
               //  retrieve.
               //----------

               l_RowCount = RowCount()
               FOR l_Idx = 1 TO l_RowCount
                  l_ItemStatus = GetItemStatus(l_Idx, 0, Primary!)
                  IF l_ItemStatus = NewModified! THEN
                     FOR l_Jdx = 1 TO i_ParentDW.i_xNumKeyColumns
                        l_ReselectKeys[l_Jdx] = &
                             Get_Col_Data_Ex    &
                                (l_Idx,    i_xKeyMap[l_Jdx], &
                                 Primary!, c_GetCurrentValues)
                     NEXT
                     i_ParentDW.Add_Reselection(l_ReselectKeys[])
                  END IF
               NEXT

               //----------
               //  If we added the primary reselection key, but
               //  didn't added any new rows, then kill the
               //  primary selection key.
               //----------

               IF l_AddedPrimaryReselect THEN
               IF i_ParentDW.i_NumReselectRows = 1 THEN
                  i_ParentDW.i_NumReselectRows = 0
               END IF
               END IF
            END IF
         END IF
      END IF
   END IF
   END IF
END IF

//----------
//  If this is the root of the event, we need to wrap up a few
//  details (like committing the changes to the database).
//----------

IF is_EventInfo.Event_Root THEN

   //----------
   //  The pcd_SaveAfter event provides the developer with a hook
   //  to do things after the database has been updated, but
   //  before it has been committed.  This event is only
   //  triggered in the root-level DataWindow.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  Initialize variables for use by the developer.
      //----------

      i_DisplayError = TRUE

      //----------
      //  Added - 950205.
      //  Reset the DB Error codes.
      //----------

      i_CCDBError         = FALSE
      i_DBErrorCode       = 0
      i_DBErrorMessage    = ""
      i_DBErrorRow        = 0
      i_DevDBErrorMessage = ""

      //----------
      //  Trigger the developer's event to do processing after
      //  all of the DataWindows have been updated.
      //----------

      TriggerEvent("pcd_SaveAfter")

      //----------
      //  If there was an error during processing after updates,
      //  then we may need to rollback the transaction.
      //----------

      IF PCCA.Error <> c_Success THEN

         //----------
         //  Save the flag which indicates if the developer
         //  wanted us to display the error.
         //----------

         l_DisplayError = i_DisplayError

         //----------
         //  Save the fu_MessageBox() parms as the
         //  information may get cleared by the
         //  rollback process.
         //----------

         IF l_DisplayError THEN
            l_MBID         = PCCA.MB.c_MBI_DW_SaveAfterError
            l_MBNumbers[1] = i_DBErrorCode
            l_MBStrings[1] = "pcd_SaveAfter"
            l_MBStrings[2] = PCCA.Application_Name
            l_MBStrings[3] = i_ClassName
            l_MBStrings[4] = i_Window.Title
            l_MBStrings[5] = DataObject
            l_MBStrings[6] = i_DBErrorMessage
            l_MBStrings[7] = i_DevDBErrorMessage
         END IF

         //----------
         //  Initialize variables for use by the developer.
         //----------

         PCCA.Error              = c_Success
         i_RootDW.i_DisplayError = TRUE

         //----------
         //  Make sure that the rollback has not already
         //  been done.
         //----------

         IF NOT i_RootDW.i_DidRollback THEN

            //----------
            //  Reset the DB Error codes.
            //----------

            i_RootDW.i_CCDBError         = FALSE
            i_RootDW.i_DBErrorCode       = 0
            i_RootDW.i_DBErrorMessage    = ""
            i_RootDW.i_DBErrorRow        = 0
            i_RootDW.i_DevDBErrorMessage = ""

            //----------
            //  Trigger the event to rollback the database.
            //----------

            i_RootDW.i_DidRollback = TRUE
            i_RootDW.TriggerEvent("pcd_Rollback")
         END IF

         //----------
         //  Save the error from the rollback.
         //----------

         l_Error = PCCA.Error

         //----------
         //  There was an error earlier but we did not
         //  display it because we didn't want to hold
         //  up the ROLLBACK process.  Display the error
         //  now if the developer requested it.
         //----------

         IF l_DisplayError THEN
            PCCA.MB.fu_MessageBox(l_MBID,           &
                               1, l_MBNumbers[], &
                               7, l_MBStrings[])
         END IF

         //----------
         //  If there was an error while rolling back the
         //  database and we were not told to prevent the
         //  error display, then display the error.
         //----------

         IF l_Error <> c_Success THEN
         IF i_DisplayError       THEN
            l_MBID         = &
               PCCA.MB.c_MBI_DW_RollbackError
            l_MBNumbers[1] = i_RootDW.i_DBErrorCode
            l_MBStrings[1] = "pcd_Rollback"
            l_MBStrings[2] = PCCA.Application_Name
            l_MBStrings[3] = i_RootDW.i_ClassName
            l_MBStrings[4] = i_RootDW.i_Window.Title
            l_MBStrings[5] = i_RootDW.DataObject
            l_MBStrings[6] = i_RootDW.i_DBErrorMessage
            l_MBStrings[7] = i_RootDW.i_DevDBErrorMessage
            PCCA.MB.fu_MessageBox(l_MBID,           &
                                        1, l_MBNumbers[], &
                                        7, l_MBStrings[])
         END IF
         END IF

         //----------
         //  Make sure that PCCA.Error still indicates
         //  failure.
         //----------

         PCCA.Error = c_Fatal
      END IF
   END IF

   //----------
   //  If there were not any errors, trigger the event to commit
   //  the changes.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  Initialize variables for use by the developer.
      //----------

      PCCA.Error     = c_Success
      i_DisplayError = TRUE

      //----------
      //  Reset the DB Error codes.
      //----------

      i_CCDBError         = FALSE
      i_DBErrorCode       = 0
      i_DBErrorMessage    = ""
      i_DBErrorRow        = 0
      i_DevDBErrorMessage = ""

      //----------
      //  Trigger the event to commit to the database.
      //----------

      TriggerEvent("pcd_Commit")

      //----------
      //  If there was an error while commiting to the database,
      //  then we may need to rollback the transaction.
      //----------

      IF PCCA.Error <> c_Success THEN

         //----------
         //  Save the flag which indicates if the developer
         //  wanted us to display the error.
         //----------

         l_DisplayError = i_DisplayError

         //----------
         //  Save the fu_MessageBox() parms as the
         //  information may get cleared by the
         //  rollback process.
         //----------

         IF l_DisplayError THEN
            l_MBID         = PCCA.MB.c_MBI_DW_CommitError
            l_MBNumbers[1] = i_DBErrorCode
            l_MBStrings[1] = "pcd_Commit"
            l_MBStrings[2] = PCCA.Application_Name
            l_MBStrings[3] = i_ClassName
            l_MBStrings[4] = i_Window.Title
            l_MBStrings[5] = DataObject
            l_MBStrings[6] = i_DBErrorMessage
            l_MBStrings[7] = i_DevDBErrorMessage
         END IF

         //----------
         //  Initialize variables for use by the developer.
         //----------

         PCCA.Error              = c_Success
         i_RootDW.i_DisplayError = TRUE

         //----------
         //  Make sure that the rollback has not already
         //  been done.
         //----------

         IF NOT i_RootDW.i_DidRollback THEN

            //----------
            //  Reset the DB Error codes.
            //----------

            i_RootDW.i_CCDBError         = FALSE
            i_RootDW.i_DBErrorCode       = 0
            i_RootDW.i_DBErrorMessage    = ""
            i_RootDW.i_DBErrorRow        = 0
            i_RootDW.i_DevDBErrorMessage = ""

            //----------
            //  Trigger the event to rollback the database.
            //----------

            i_RootDW.i_DidRollback = TRUE
            i_RootDW.TriggerEvent("pcd_Rollback")
         END IF

         //----------
         //  Save the error from the rollback.
         //----------

         l_Error = PCCA.Error

         //----------
         //  There was an error earlier but we did not
         //  display it because we didn't want to hold
         //  up the ROLLBACK process.  Display the error
         //  now if the developer requested it.
         //----------

         IF l_DisplayError THEN
            PCCA.MB.fu_MessageBox(l_MBID,           &
                               1, l_MBNumbers[], &
                               7, l_MBStrings[])
         END IF

         //----------
         //  If there was an error while rolling back the
         //  database and we were not told to prevent the
         //  error display, then display the error.
         //----------

         IF l_Error <> c_Success THEN
         IF i_DisplayError       THEN
            l_MBID         = &
               PCCA.MB.c_MBI_DW_RollbackError
            l_MBNumbers[1] = i_RootDW.i_DBErrorCode
            l_MBStrings[1] = "pcd_Rollback"
            l_MBStrings[2] = PCCA.Application_Name
            l_MBStrings[3] = i_RootDW.i_ClassName
            l_MBStrings[4] = i_RootDW.i_Window.Title
            l_MBStrings[5] = i_RootDW.DataObject
            l_MBStrings[6] = i_RootDW.i_DBErrorMessage
            l_MBStrings[7] = i_RootDW.i_DevDBErrorMessage
            PCCA.MB.fu_MessageBox(l_MBID,           &
                                        1, l_MBNumbers[], &
                                        7, l_MBStrings[])
         END IF
         END IF

         //----------
         //  Make sure that PCCA.Error still indicates
         //  failure.
         //----------

         PCCA.Error = c_Fatal
      END IF
   END IF

   //----------
   //  If there are no errors, then clear changes and refresh
   //  the DataWindow and its children, unless specified
   //  otherwise.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  Changes have been successfully saved to the database.
      //  Therefore, clear all of the modified flags in the
      //  saved DataWindows.
      //----------

      is_EventControl.Clear_Ok = TRUE
      TriggerEvent("pcd_AnyChanges")

      //----------
      //  Unless whoever triggered us specified that they would
      //  take care of the refresh, we'll take care of it here.
      //----------

      IF NOT l_NoRefreshAfterSave THEN
         is_EventControl.Refresh_Cmd = c_RefreshSave
         TriggerEvent("pcd_Refresh")
      END IF
   ELSE

      //----------
      //  There are errors.  Rollback the changes if it has not
      //  already been done.
      //----------

      IF NOT i_DidRollback THEN

         //----------
         //  Initialize variables for use by the developer.
         //----------

         PCCA.Error     = c_Success
         i_DisplayError = TRUE

         //----------
         //  Reset the DB Error codes.
         //----------

         i_CCDBError         = FALSE
         i_DBErrorCode       = 0
         i_DBErrorMessage    = ""
         i_DBErrorRow        = 0
         i_DevDBErrorMessage = ""

         //----------
         //  Trigger the event to rollback the database.
         //----------

         i_DidRollback = TRUE
         TriggerEvent("pcd_Rollback")

         //----------
         //  If there was an error while rolling back the database
         //  and we were not told to prevent the error display,
         //  then display the error.
         //----------

         IF PCCA.Error <> c_Success THEN
         IF i_DisplayError          THEN
            PCCA.MB.i_MB_Numbers[1] = i_DBErrorCode
            PCCA.MB.i_MB_Strings[1] = "pcd_Rollback"
            PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
            PCCA.MB.i_MB_Strings[3] = i_ClassName
            PCCA.MB.i_MB_Strings[4] = i_Window.Title
            PCCA.MB.i_MB_Strings[5] = DataObject
            PCCA.MB.i_MB_Strings[6] = i_DBErrorMessage
            PCCA.MB.i_MB_Strings[7] = i_DevDBErrorMessage
            PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_RollbackError, &
                               1, PCCA.MB.i_MB_Numbers[],      &
                               7, PCCA.MB.i_MB_Strings[])
         END IF
         END IF
      END IF

      //----------
      //  Make sure that PCCA.Error still indicates failure.
      //----------

      PCCA.Error = c_Fatal
   END IF

   //----------
   //  Remove the saving prompt.
   //----------

   PCCA.MDI.fu_Pop()
END IF

Finished:

//----------
//  For the developer, we set the instance variables to reflect
//  the state of processing by this event.
//----------

i_IsFinal     = is_EventInfo.Event_Recipient
i_IsRoot      = is_EventInfo.Event_Root
i_IsCascading = is_EventInfo.Cascading
i_ExtendMode  = is_EventInfo.Cascading

//----------
//  Indicate that we exiting save.
//----------

IF i_InSave > 1 THEN
   i_InSave = i_InSave - 1
ELSE
   i_InSave = 0
END IF
end on

on pcd_saverowsas;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_SaveRowsAs
//  Description   : Save the DataWindow contents in a specific file
//                  format.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//----------
//  Make sure that:
//     a) The DataWindow is in use and
//     b) The DataWindow is not in "QUERY" mode and
//     c) There are actually rows in DataWindow that can be
//        saved.
//----------

IF NOT i_InUse OR i_DWState = c_DWStateQuery OR i_IsEmpty THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Indicate to the user that we are processing the SaveRowsAs
//  command.
//----------

PCCA.Error = c_Success
PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_SaveRowsAs, c_ShowMark)

//----------
//  Make sure that there is valid data before we save the rows.
//----------

IF dw_AcceptText(c_AcceptProcessErrors) THEN
   l_Error = f_PO_FileSaveAs(THIS, PCCA.Current_Directory)
   IF l_Error <> c_ValOk THEN
      PCCA.Error = c_Fatal
   ELSE
      PCCA.Error = c_Success
   END IF
ELSE
   PCCA.Error = c_Fatal
END IF

//----------
//  Remove the SaveRowsAs prompt.
//----------

PCCA.MDI.fu_Pop()

Finished:

i_ExtendMode = i_InUse
end on

on pcd_search;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Search
//  Description   : Get search criteria from non-DataWindow fields
//                  and build up the SQL statement for a retrieve.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN              l_First,  l_SetFocus
INTEGER              l_Answer, l_Idx
UNSIGNEDLONG         l_RefreshCmd
STRING               l_OldSelect
GRAPHICOBJECT        l_TmpObj
UO_DDDW_SEARCH_MAIN  l_DDDWName
UO_DW_SEARCH         l_DWName
UO_DDLB_SEARCH       l_DDLBName
UO_EM_SEARCH         l_EMName
UO_LB_SEARCH         l_LBName
UO_SLE_SEARCH        l_SLEName
UO_DW_MAIN           l_ToDW, l_ChildDW

//----------
//  Indicate that we are in the pcd_Search event.
//----------

i_InSearch = i_InSearch + 1

//----------
//  Make sure that this DataWindow is in use.
//----------

IF NOT i_InUse THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Display the search prompt.
//----------

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Search, c_ShowMark)

//----------
//  Make sure changes have been taken care of.  Search will
//  cause new rows to be loaded and any changes will be clobbered.
//----------

Check_Save(l_RefreshCmd)
IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  Call Load_Rows() to load selections that have been made
//  by the user, but not yet loaded into the children
//  DataWindows.
//----------

IF PCCA.PCMGR.i_DW_NeedRetrieve THEN
   IF Load_Rows(PCCA.Null_Object, PCCA.Null_Object) <> &
      c_Success THEN
      PCCA.Error = c_Fatal
      GOTO Finished
   END IF
END IF

//----------
//  Save the old select in case we get a validation error or
//  something.
//----------

l_OldSelect = Describe("DataWindow.Table.Select")

//----------
//  Cycle through all of the search objects that are wired to
//  this DataWindow to gather additional search criteria.
//----------

l_First = TRUE
FOR l_Idx = 1 TO i_NumSearchObjects

   //----------
   //  PowerClass supports several different types of search
   //  objects.  Find out what we have and handle it.
   //----------

   l_TmpObj = i_SearchObjects[l_Idx]
   CHOOSE CASE l_TmpObj.Tag

      CASE "PowerObject Search DDDW"
         l_DDDWName = l_TmpObj
         PCCA.Error = l_DDDWName.fu_BuildSearch(l_First)
         IF PCCA.Error <> c_Success THEN
            l_DDDWName.SetFocus()
            GOTO Finished
         END IF

      CASE "PowerObject Search DW"
         l_DWName = l_TmpObj
         PCCA.Error = l_DWName.fu_BuildSearch(l_First)
         IF PCCA.Error <> c_Success THEN
            l_DWName.SetFocus()
            GOTO Finished
         END IF

      CASE "PowerObject Search DDLB"
         l_DDLBName = l_TmpObj
         PCCA.Error = l_DDLBName.fu_BuildSearch(l_First)
         IF PCCA.Error <> c_Success THEN
            l_DDLBName.SetFocus()
            GOTO Finished
         END IF

      CASE "PowerObject Search EM"
         l_EMName   = l_TmpObj
         PCCA.Error = l_EMName.fu_BuildSearch(l_First)
         IF PCCA.Error <> c_Success THEN
            l_EMName.SetFocus()
            GOTO Finished
         END IF

      CASE "PowerObject Search LB"
         l_LBName   = l_TmpObj
         PCCA.Error = l_LBName.fu_BuildSearch(l_First)
         IF PCCA.Error <> c_Success THEN
            l_LBName.SetFocus()
            GOTO Finished
         END IF

      CASE "PowerObject Search SLE"
         l_SLEName  = l_TmpObj
         PCCA.Error = l_SLEName.fu_BuildSearch(l_First)
         IF PCCA.Error <> c_Success THEN
            l_SLEName.SetFocus()
            GOTO Finished
         END IF
   END CHOOSE

   IF l_First THEN
      l_First = FALSE
   END IF
NEXT

//----------
//  We may have been in "QUERY" mode.  Make sure that we get out
//  of it.
//----------

Unset_Query()

//----------
//  At the start of this event, we checked for changes.  We now
//  see if there is any refreshing that needs to be done because
//  of changes being saved or aborted.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")
END IF

//----------
//  Because we may have just came out of "QUERY" mode, we want
//  current mode to be recalculated.
//----------

i_CurrentMode = c_ModeUndefined

//----------
//  Because our SQL has changed, we need to be retrieved.
//----------

Retrieve_DW(c_RetrieveAllDWs, i_xReselectMode, i_xRefreshMode)

//----------
//  If the DataWindow is empty after pcd_Refresh and was not put
//  in "NEW" mode by the developer, then no rows were retrieved.
//  Tell the user that they are being too picky.
//----------

IF i_IsEmpty THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Search"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   l_Answer             = PCCA.MB.fu_MessageBox                &
                             (PCCA.MB.c_MBI_DW_ZeroSearchRows, &
                              0, PCCA.MB.i_MB_Numbers[],       &
                              5, PCCA.MB.i_MB_Strings[])
ELSE
   l_Answer = 0
END IF

//----------
//  If the developer has not set focus, then see if we should
//  try to set it.
//----------

IF NOT PCCA.PCMGR.i_DW_FocusDone THEN

   //----------
   //  If l_Answer is 0, then set focus to the preferred DataWindow
   //  (if any), a child DataWindow, or this DataWindow.  l_Answer
   //  being 0 indicates that we got data.
   //----------

   l_SetFocus = FALSE

   IF l_Answer = 0 THEN

      //----------
      //  If the developer specified a preferred-focus DataWindow,
      //  we want to set focus to it.  There should either be rows
      //  selected or this DataWindow should been requested to go
      //  to "NEW" mode.
      //----------

      IF IsValid(i_FocusDW) THEN
         IF i_FocusDW.i_InUse THEN
            CHOOSE CASE i_RequestedMode
               CASE c_ModeModify
                  IF i_FocusDW.i_AllowModify THEN
                  IF i_ShowRow > 0           THEN
                     l_SetFocus = TRUE
                     l_ToDW     = i_FocusDW
                  END IF
                  END IF
               CASE c_ModeNew
                  IF i_FocusDW.i_AllowNew THEN
                     l_SetFocus = TRUE
                     l_ToDW     = i_FocusDW
                  END IF
               CASE c_ModeView
                  IF i_ShowRow > 0 THEN
                     l_SetFocus = TRUE
                     l_ToDW     = i_FocusDW
                  END IF
            END CHOOSE
         END IF
      END IF

      //----------
      //  If the DataWindow that is to get focus was not set to a
      //  preferred-focus DataWindow, then see if it should be set
      //  to an instance DataWindow.
      //----------

      IF NOT l_SetFocus THEN

         //----------
         //  If there are Instance DataWindows, we will set focus
         //  to the Instance DataWindow that corresponds to the
         //  currently selected or New! row.
         //----------

         IF i_CurInstance > 0 THEN
            l_ChildDW = i_InstanceDW[i_CurInstance]
            CHOOSE CASE i_RequestedMode
               CASE c_ModeModify
                  IF NOT i_AllowModify THEN
                     IF l_ChildDW.i_AllowModify THEN
                        l_SetFocus = TRUE
                        l_ToDW     = l_ChildDW
                     END IF
                  ELSE
                     l_SetFocus = TRUE
                     l_ToDW     = THIS
                  END IF
               CASE c_ModeNew
                  IF NOT i_AllowNew THEN
                     IF l_ChildDW.i_AllowNew THEN
                        l_SetFocus = TRUE
                        l_ToDW     = l_ChildDW
                     END IF
                  ELSE
                     l_SetFocus = TRUE
                     l_ToDW     = THIS
                  END IF
               CASE c_ModeView
                  l_SetFocus = TRUE
                  l_ToDW     = l_ChildDW
            END CHOOSE
         END IF
      END IF

      //----------
      //  If the DataWindow that is to get focus has still not
      //  set, then see if it should be set to one of the normal
      //  children DataWindows.
      //----------

      IF NOT l_SetFocus THEN

         //----------
         //  We search backwards through the child DataWindow
         //  array so that focus will get set to the most
         //  recently created child DataWindow.
         //----------

         FOR l_Idx = i_NumChildren TO 1 STEP -1
            l_ChildDW = i_ChildDW[l_Idx]
            CHOOSE CASE i_RequestedMode
               CASE c_ModeModify
                  IF NOT i_AllowModify THEN
                     IF i_ShowRow > 0           THEN
                     IF l_ChildDW.i_AllowModify THEN
                        l_SetFocus = TRUE
                        l_ToDW     = l_ChildDW
                     END IF
                     END IF
                  ELSE
                     l_SetFocus = TRUE
                     l_ToDW     = THIS
                  END IF
               CASE c_ModeNew
                  IF NOT i_AllowNew THEN
                     IF l_ChildDW.i_AllowNew THEN
                        l_SetFocus = TRUE
                        l_ToDW     = l_ChildDW
                     END IF
                  ELSE
                     l_SetFocus = TRUE
                     l_ToDW     = THIS
                  END IF
               CASE c_ModeView
                  IF i_ShowRow > 0 THEN
                     IF l_ChildDW.i_Window <> i_Window THEN
                        l_SetFocus = TRUE
                        l_ToDW     = l_ChildDW
                     ELSE
                        l_SetFocus = TRUE
                        l_ToDW     = THIS
                     END IF
                  ELSE
                     l_SetFocus = TRUE
                     l_ToDW     = THIS
                  END IF
            END CHOOSE

            IF l_SetFocus THEN
               EXIT
            END IF
         NEXT
      END IF
   END IF

   //----------
   //  If we couldn't find a better choice for the DataWindow to
   //  set focus to, just set it to the DataWindow that received
   //  the pcd_Search event (i.e. this DataWindow).
   //----------

   IF NOT l_SetFocus THEN
      l_ToDW = THIS
   END IF

   //----------
   //  Make the DataWindow that was our best choice the current
   //  DataWindow.
   //----------

   Set_Auto_Focus(l_ToDW)
END IF

Finished:

//----------
//  If there was an error along the way, make sure the any
//  refresh requested by Check_Save() gets taken care of.
//----------

IF PCCA.Error <> c_Success THEN

   l_OldSelect = "DataWindow.Table.Select='"         + &
                 PCCA.MB.fu_QuoteChar(l_OldSelect, "'") + "'"
   Modify(l_OldSelect)

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      i_RootDW.is_EventControl.Refresh_Cmd = l_RefreshCmd
      i_RootDW.TriggerEvent("pcd_Refresh")

      //----------
      //  Make sure that PCCA.Error indicates failure.
      //----------

      PCCA.Error = c_Fatal
   END IF
END IF

//----------
//  Remove the searching prompt.
//----------

PCCA.MDI.fu_Pop()

//----------
//  Indicate that we are no longer in the pcd_Search event.
//----------

IF i_InSearch > 1 THEN
   i_InSearch = i_InSearch - 1
ELSE
   i_InSearch = 0
END IF

i_ExtendMode = i_InUse
end on

on pcd_setcontrol;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_SetControl
//  Description   : Enables/disables menu items and buttons.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_Set,         l_QMode
BOOLEAN       l_ViewOk,      l_ModifyOk, l_NewOk,    l_InsertOk
BOOLEAN       l_DeleteOk,    l_QueryOk,  l_ResetOk,  l_SearchOk
BOOLEAN       l_FirstOk,     l_LastOk,   l_PrevOk,   l_NextOk
BOOLEAN       l_FilterOk,    l_SaveOk,   l_PrintOk,  l_SaveRowsAsOk
BOOLEAN       l_AcceptOk,    l_CancelOk, l_CloseOk,  l_RetrieveOk
INTEGER       l_Idx
UNSIGNEDLONG  l_ControlMode
UO_CB_MAIN    l_PCControl
UO_DW_MAIN    l_ChildDW

//----------
//  Save the options passed to us in the Event Control structure.
//----------

l_ControlMode = is_EventControl.Control_Mode

//----------
//  Reset all of the event control variables.
//----------

PCCA.Do_Event   = FALSE
is_EventControl = is_ResetControl

//----------
//  If we are not in use, then we shouldn't be enabling controls.
//----------

IF NOT i_InUse THEN
   l_ControlMode = c_ControlUndefined
   GOTO Finished
END IF

//----------
//  Determine if we are in "QUERY" mode.
//----------

l_QMode = (i_DWState = c_DWStateQuery)

//----------
//  Figure out if this DataWindow accepts Modify or New
//  commands.  Even if this DataWindow does not allow "MODIFY"
//  and "NEW" mode itself, it can pass those modes to the
//  children DataWindows for processing.  Therefore, if the
//  children Datawindows do allow these modes, we want to
//  enable these commands in the parent so that the user can
//  use them.  We are going to set initial values for
//  l_ModifyOk and l_NewOk here.  As we cascade through the
//  children DataWindows, they will set these variables in
//  this DataWindow to TRUE if they allow those modes (i.e.
//  the capabilties "bubble" up from below).
//----------

//----------
//  If the developer told us that our children DataWindows
//  would have "MODIFY" capability, we may want to enable
//  the Modify menu for this DataWindow so that the user can
//  open the child DataWindow in "MODIFY" mode.
//----------

l_ModifyOk = i_EnableModifyOnOpen

//----------
//  If the developer told us that our children DataWindows
//  would have "NEW" capability, we want to enable the New
//  menu for this DataWindow so that the user can open the
//  child DataWindow in "NEW" mode.
//----------

l_NewOk = i_EnableNewOnOpen

//----------
//  Do the "normal" children DataWindows.
//----------

FOR l_Idx = 1 TO i_NumChildren
   l_ChildDW = i_ChildDW[l_Idx]

   //----------
   //  If the child DataWindow allows "MODIFY" and "NEW", then
   //  this DataWindow should also allow them.
   //----------

   IF l_ChildDW.i_AllowModify AND l_ModifyOK THEN
      l_ModifyOk = TRUE
   END IF
   IF l_ChildDW.i_AllowNew AND l_NewOK THEN
   IF (NOT l_ChildDW.i_DoSetKey OR &
       NOT l_ChildDW.i_OnlyOneNewRow) THEN
      l_NewOk = TRUE
   END IF
   END IF

   //----------
   //  If we are only setting menus, then there is no need to do
   //  cascade to the children.
   //----------

   IF l_ControlMode <> c_ControlMainMenu  THEN
   IF l_ControlMode <> c_ControlPopupMenu THEN
      l_ChildDW.is_EventControl.Control_Mode = l_ControlMode
      l_ChildDW.TriggerEvent("pcd_SetControl")
   END IF
   END IF
NEXT

//----------
//  Do the Instance children DataWindows.
//----------

IF i_CurInstance > 0 THEN
   l_ChildDW = i_InstanceDW[i_CurInstance]

   //----------
   //  If the child DataWindow allows "MODIFY" and "NEW", then
   //  this DataWindow should also allow them.
   //----------

   IF l_ChildDW.i_AllowModify THEN
      l_ModifyOk = TRUE
   END IF
   IF l_ChildDW.i_AllowNew THEN
   IF (NOT l_ChildDW.i_DoSetKey OR &
       NOT l_ChildDW.i_OnlyOneNewRow) THEN
      l_NewOk = TRUE
   END IF
   END IF

   //----------
   //  If we are only setting menus, then there is no need to do
   //  cascade to the children.
   //----------

   IF l_ControlMode <> c_ControlMainMenu  THEN
   IF l_ControlMode <> c_ControlPopupMenu THEN
      l_ChildDW.is_EventControl.Control_Mode = l_ControlMode
      l_ChildDW.TriggerEvent("pcd_SetControl")
   END IF
   END IF
END IF

//----------
//  Calculcate the enabled/disabled state for each of the menus
//  and buttons.
//----------

//----------
//  None of these are allowed in "QUERY" mode.
//----------

IF l_QMode THEN
   l_DeleteOk     = FALSE
   l_ModifyOk     = FALSE
   l_NewOk        = FALSE
   l_InsertOk     = FALSE
   l_FirstOk      = FALSE
   l_LastOk       = FALSE
   l_PrevOk       = FALSE
   l_NextOk       = FALSE
   l_InsertOk     = FALSE
   l_ViewOk       = FALSE
   l_QueryOk      = FALSE
   l_FilterOk     = FALSE
   l_PrintOk      = FALSE
   l_SaveRowsAsOk = FALSE
ELSE

   //----------
   //  If this DataWindow does allow delete, make sure there is
   //  at least one row selected.
   //----------

   IF i_AllowDelete THEN
      l_DeleteOk = (i_ShowRow > 0)
   ELSE

      //----------
      //  The DataWindow does not have delete capability.
      //  However, we do allow the user to delete new records
      //  which they have inserted.
      //----------

      l_DeleteOk = FALSE

      //----------
      //  See if the currently selected record is a New! record
      //  that the user has inserted.  We check for the
      //  following conditions:
      //     a) Making sure that the user is allowed to add
      //        rows AND
      //     b) Making sure there are New! rows in this
      //        DataWindow (i.e. i_DoSetKey is TRUE) AND
      //     c) Making sure there is at least one row selected
      //        AND
      //     d) Making sure that there are not any retrievable
      //        rows (i.e. if the rows are retrievable, they are
      //        not New!).
      //----------

      IF i_AllowNew                     THEN
      IF i_SharePrimary.i_ShareDoSetKey THEN
      IF i_ShowRow > 0                  THEN
      IF i_NumSelected = 0              THEN
         l_DeleteOk = TRUE
      END IF
      END IF
      END IF
      END IF
   END IF

   IF i_IsEmpty THEN
      l_ModifyOk = FALSE
   ELSE
      IF i_AllowModify THEN
         l_ModifyOk = TRUE
      ELSE
         IF l_ModifyOk THEN
            l_ModifyOk = (i_ShowRow > 0)
         END IF
      END IF
   END IF

   IF i_AllowNew THEN
      l_NewOk = (NOT i_DoSetKey OR NOT i_OnlyOneNewRow)
   END IF

   l_InsertOk = (l_NewOk AND i_AllowNew)

   //----------
   //  Figure out how the scroll buttons should be set.
   //  If the scroll DataWindow is empty, scrolling should
   //  not be allowed.
   //----------

   IF i_ScrollDW.i_IsEmpty THEN
      l_FirstOk = FALSE
      l_LastOk  = FALSE
      l_PrevOk  = FALSE
      l_NextOk  = FALSE
   ELSE

      //----------
      //  First and Previous are allowed except when we are
      //  on the first row and Last and Next are allowed
      //  except when we are on the last row.
      //----------

      l_FirstOk = (i_ScrollDW.i_CursorRow > 1)
      l_PrevOk  = l_FirstOk
      l_LastOk  = (i_ScrollDW.i_CursorRow < &
                   i_ScrollDW.RowCount())
      l_NextOk  = l_LastOk
   END IF

   l_QueryOk      = i_AllowQuery
   l_FilterOk     = (i_NumFilterObjects > 0)
   l_ViewOk       = (NOT i_IsEmpty)
   l_PrintOk      = (NOT i_IsEmpty)
   l_SaveRowsAsOk = (NOT i_IsEmpty)
END IF

//----------
//  Set the enabled/disabled status for all of the other
//  command buttons.
//----------

l_SearchOk   = (i_NumSearchObjects > 0 OR i_AllowQuery)
l_ResetOk    = i_AllowQuery

l_SaveOk     = TRUE
l_AcceptOk   = TRUE
l_CancelOk   = TRUE
l_CloseOk    = TRUE
l_RetrieveOk = TRUE

//----------
//  For each PowerClass command button that is either on this
//  window or wired to this DataWindow, enable or disable the
//  button.  If we are only setting menus, then there is no
//  need to do buttons.
//----------

IF l_ControlMode <> c_ControlMainMenu  THEN
IF l_ControlMode <> c_ControlPopupMenu THEN

   FOR l_Idx = 1 TO i_NumPCControls
      l_PCControl = i_PCControls[l_Idx]

      //----------
      //  If this button is wired to a DataWindow, then see if it
      //  is wired to this DataWindow.  If the command button is
      //  not wired to any DataWindow, then we want to enable or
      //  disable it only if this is the current DataWindow (or
      //  would be the current DataWindow if the window was
      //  activiated).
      //----------

      IF IsValid(l_PCControl.i_TrigObject) THEN
         l_Set = (l_PCControl.i_TrigObject = THIS)
      ELSE
         l_Set = i_Current
      END IF

      //----------
      //  If this button needs to be set, do it.
      //----------

      IF l_Set THEN

         //----------
         //  Based on the type of the event the button triggers,
         //  we may need to enable or disable it.
         //----------

         CHOOSE CASE l_PCControl.i_ButtonType

            CASE c_FirstCB
               IF i_AllowControlFirst THEN
                  IF l_PCControl.Enabled <> l_FirstOk THEN
                     l_PCControl.Enabled =  l_FirstOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_LastCB
               IF i_AllowControlLast THEN
                  IF l_PCControl.Enabled <> l_LastOk THEN
                     l_PCControl.Enabled =  l_LastOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_NextCB
               IF i_AllowControlNext THEN
                  IF l_PCControl.Enabled <> l_NextOk THEN
                     l_PCControl.Enabled =  l_NextOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_PreviousCB
               IF i_AllowControlPrevious THEN
                  IF l_PCControl.Enabled <> l_PrevOk THEN
                     l_PCControl.Enabled =  l_PrevOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_DeleteCB
               IF i_AllowControlDelete THEN
                  IF l_PCControl.Enabled <> l_DeleteOk THEN
                     l_PCControl.Enabled =  l_DeleteOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_ViewCB
               IF i_AllowControlView THEN
                  IF l_PCControl.Enabled <> l_ViewOk THEN
                     l_PCControl.Enabled =  l_ViewOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_ModifyCB
               IF i_AllowControlModify THEN
                  IF l_PCControl.Enabled <> l_ModifyOk THEN
                     l_PCControl.Enabled =  l_ModifyOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_NewCB
               IF i_AllowControlNew THEN
                  IF l_PCControl.Enabled <> l_NewOk THEN
                     l_PCControl.Enabled =  l_NewOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_InsertCB
               IF i_AllowControlInsert THEN
                  IF l_PCControl.Enabled <> l_InsertOk THEN
                     l_PCControl.Enabled =  l_InsertOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_QueryCB
               IF i_AllowControlQuery THEN
                  IF l_PCControl.Enabled <> l_QueryOk THEN
                     l_PCControl.Enabled =  l_QueryOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_ResetQueryCB
               IF i_AllowControlResetQuery THEN
                  IF l_PCControl.Enabled <> l_ResetOk THEN
                     l_PCControl.Enabled =  l_ResetOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_SearchCB
               IF i_AllowControlSearch THEN
                  IF l_PCControl.Enabled <> l_SearchOk THEN
                     l_PCControl.Enabled =  l_SearchOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_FilterCB
               IF i_AllowControlFilter THEN
                  IF l_PCControl.Enabled <> l_FilterOk THEN
                     l_PCControl.Enabled =  l_FilterOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_SaveCB
               IF i_AllowControlSave THEN
                  IF l_PCControl.Enabled <> l_SaveOk THEN
                     l_PCControl.Enabled =  l_SaveOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_PrintCB
               IF i_AllowControlPrint THEN
                  IF l_PCControl.Enabled <> l_PrintOk THEN
                     l_PCControl.Enabled =  l_PrintOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_SaveRowsAsCB
               IF i_AllowControlSaveRowsAs THEN
                  IF l_PCControl.Enabled <> l_SaveRowsAsOk THEN
                     l_PCControl.Enabled =  l_SaveRowsAsOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_AcceptCB
               IF i_AllowControlAccept THEN
                  IF l_PCControl.Enabled <> l_AcceptOk THEN
                     l_PCControl.Enabled =  l_AcceptOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_CancelCB
               IF i_AllowControlCancel THEN
                  IF l_PCControl.Enabled <> l_CancelOk THEN
                     l_PCControl.Enabled =  l_CancelOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_CloseCB
               IF i_AllowControlClose THEN
                  IF l_PCControl.Enabled <> l_CloseOk THEN
                     l_PCControl.Enabled =  l_CloseOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            CASE c_RetrieveCB
               IF i_AllowControlRetrieve THEN
                  IF l_PCControl.Enabled <> l_RetrieveOk THEN
                     l_PCControl.Enabled =  l_RetrieveOk
                  END IF
               ELSE
                  l_PCControl.Enabled = FALSE
               END IF

            //----------
            //  We don't know what kind of button this is.
            //  Just leave it alone.
            //----------

            CASE ELSE
         END CHOOSE
      END IF
   NEXT
END IF
END IF

//----------
//  If we are setting menus, this is where we do the work.
//----------

IF i_Active                          THEN
IF i_Window.ToolBarVisible           OR &
   l_ControlMode = c_ControlMainMenu THEN

   //----------
   //  If there is a FILE menu, then we may need to change
   //  the menu items in it.
   //----------

   IF NOT i_FileMenuInit THEN
       Set_DW_FMenu(i_FileMenu)
   END IF

   IF i_FileMenuIsValid THEN

      //----------
      //  The "Save" menu item.
      //----------

      IF i_FileMenuSaveNum > 0 THEN
         IF i_AllowControlSave THEN
            IF i_FileMenuSave.Enabled <> l_SaveOk THEN
               i_FileMenuSave.Enabled =  l_SaveOk
            END IF
         ELSE
            i_FileMenuSave.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Save Rows As" menu item.
      //----------

      IF i_FileMenuSaveRowsAsNum > 0 THEN
         IF i_AllowControlSaveRowsAs THEN
            IF i_FileMenuSaveRowsAs.Enabled <> l_SaveRowsAsOk THEN
               i_FileMenuSaveRowsAs.Enabled =  l_SaveRowsAsOk
            END IF
         ELSE
            i_FileMenuSaveRowsAs.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Print" menu item.
      //----------

      IF i_FileMenuPrintNum > 0 THEN
         IF i_AllowControlPrint THEN
            IF i_FileMenuPrint.Enabled <> l_PrintOk THEN
               i_FileMenuPrint.Enabled =  l_PrintOk
            END IF
         ELSE
            i_FileMenuPrint.Enabled = FALSE
         END IF
      END IF
   END IF

   //----------
   //  If there is a EDIT menu, then we may need to change
   //  the menu items in it.
   //----------

   IF NOT i_EditMenuInit THEN
       Set_DW_EMenu(i_EditMenu)
   END IF

   IF i_EditMenuIsValid THEN

      //----------
      //  The "First" menu item.
      //----------

      IF i_EditMenuFirstNum > 0 THEN
         IF i_AllowControlFirst THEN
            IF i_EditMenuFirst.Enabled <> l_FirstOk THEN
               i_EditMenuFirst.Enabled =  l_FirstOk
            END IF
         ELSE
            i_EditMenuFirst.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Previous" menu item.
      //----------

      IF i_EditMenuPrevNum > 0  THEN
         IF i_AllowControlPrevious THEN
            IF i_EditMenuPrev.Enabled <> l_PrevOk THEN
               i_EditMenuPrev.Enabled =  l_PrevOk
            END IF
         ELSE
            i_EditMenuPrev.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Next" menu item.
      //----------

      IF i_EditMenuNextNum > 0 THEN
         IF i_AllowControlNext THEN
            IF i_EditMenuNext.Enabled <> l_NextOk THEN
               i_EditMenuNext.Enabled =  l_NextOk
            END IF
         ELSE
            i_EditMenuNext.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Last" menu item.
      //----------

      IF i_EditMenuLastNum > 0 THEN
         IF i_AllowControlLast THEN
            IF i_EditMenuLast.Enabled <> l_LastOk THEN
               i_EditMenuLast.Enabled =  l_LastOk
            END IF
         ELSE
            i_EditMenuLast.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Delete" menu item.
      //----------

      IF i_EditMenuDeleteNum > 0 THEN
         IF i_AllowControlDelete THEN
            IF i_EditMenuDelete.Enabled <> l_DeleteOk THEN
               i_EditMenuDelete.Enabled =  l_DeleteOk
            END IF
         ELSE
            i_EditMenuDelete.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "New" menu item.
      //----------

      IF i_EditMenuNewNum > 0 THEN
         IF i_AllowControlNew THEN
            IF i_EditMenuNew.Enabled <> l_NewOk THEN
               i_EditMenuNew.Enabled =  l_NewOk
            END IF
         ELSE
            i_EditMenuNew.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "View" menu item.
      //----------

      IF i_EditMenuViewNum > 0 THEN
         IF i_AllowControlView THEN
            IF i_EditMenuView.Enabled <> l_ViewOk THEN
               i_EditMenuView.Enabled =  l_ViewOk
            END IF
         ELSE
            i_EditMenuView.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Modify" menu item.
      //----------

      IF i_EditMenuModifyNum > 0 THEN
         IF i_AllowControlModify THEN
            IF i_EditMenuModify.Enabled <> l_ModifyOk THEN
               i_EditMenuModify.Enabled =  l_ModifyOk
            END IF
         ELSE
            i_EditMenuModify.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Insert" menu item.
      //----------

      IF i_EditMenuInsertNum > 0 THEN
         IF i_AllowControlInsert THEN
            IF i_EditMenuInsert.Enabled <> l_InsertOk THEN
               i_EditMenuInsert.Enabled =  l_InsertOk
            END IF
         ELSE
            i_EditMenuInsert.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Query" menu item.
      //----------

      IF i_EditMenuQueryNum > 0 THEN
         IF i_AllowControlQuery THEN
            IF i_EditMenuQuery.Enabled <> l_QueryOk THEN
               i_EditMenuQuery.Enabled =  l_QueryOk
            END IF
         ELSE
            i_EditMenuQuery.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Search" menu item.
      //----------

      IF i_EditMenuSearchNum > 0 THEN
         IF i_AllowControlSearch THEN
            IF i_EditMenuSearch.Enabled <> l_SearchOk THEN
               i_EditMenuSearch.Enabled =  l_SearchOk
            END IF
         ELSE
            i_EditMenuSearch.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Filter" menu item.
      //----------

      IF i_EditMenuFilterNum > 0 THEN
         IF i_AllowControlFilter THEN
            IF i_EditMenuFilter.Enabled <> l_FilterOk THEN
               i_EditMenuFilter.Enabled =  l_FilterOk
            END IF
         ELSE
            i_EditMenuFilter.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Save" menu item.
      //----------

      IF i_EditMenuSaveNum > 0 THEN
         IF i_AllowControlSave THEN
            IF i_EditMenuSave.Enabled <> l_SaveOk THEN
               i_EditMenuSave.Enabled = l_SaveOk
            END IF
         ELSE
            i_EditMenuSave.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Save Rows As" menu item.
      //----------

      IF i_EditMenuSaveRowsAsNum > 0 THEN
         IF i_AllowControlSaveRowsAs THEN
            IF i_EditMenuSaveRowsAs.Enabled <> l_SaveRowsAsOk THEN
               i_EditMenuSaveRowsAs.Enabled =  l_SaveRowsAsOk
            END IF
         ELSE
            i_EditMenuSaveRowsAs.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Print" menu item.
      //----------

      IF i_EditMenuPrintNum > 0 THEN
         IF i_AllowControlPrint THEN
            IF i_EditMenuPrint.Enabled <> l_PrintOk THEN
               i_EditMenuPrint.Enabled =  l_PrintOk
            END IF
         ELSE
            i_EditMenuPrint.Enabled = FALSE
         END IF
      END IF
   END IF
END IF
END IF

//----------
//  If there is a POPUP menu, then we may need to change the
//  menu items in it.
//----------

IF l_ControlMode = c_ControlPopupMenu THEN
   IF NOT i_PopupMenuInit THEN
       Set_DW_PMenu(i_PopupMenu)
   END IF

   IF i_PopupMenuIsValid THEN

      //----------
      //  The "First" menu item.
      //----------

      IF i_PopupMenuFirstNum <> 0 THEN
         IF i_AllowControlFirst THEN
            IF i_PopupMenuFirst.Enabled <> l_FirstOk THEN
               i_PopupMenuFirst.Enabled =  l_FirstOk
            END IF
         ELSE
            i_PopupMenuFirst.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Previous" menu item.
      //----------

      IF i_PopupMenuPrevNum > 0 THEN
         IF i_AllowControlPrevious THEN
            IF i_PopupMenuPrev.Enabled <> l_PrevOk THEN
               i_PopupMenuPrev.Enabled =  l_PrevOk
            END IF
         ELSE
            i_PopupMenuPrev.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Next" menu item.
      //----------

      IF i_PopupMenuNextNum > 0 THEN
         IF i_AllowControlNext THEN
            IF i_PopupMenuNext.Enabled <> l_NextOk THEN
               i_PopupMenuNext.Enabled =  l_NextOk
            END IF
         ELSE
            i_PopupMenuNext.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Last" menu item.
      //----------

      IF i_PopupMenuLastNum > 0 THEN
         IF i_AllowControlLast THEN
            IF i_PopupMenuLast.Enabled <> l_LastOk THEN
               i_PopupMenuLast.Enabled =  l_LastOk
            END IF
         ELSE
            i_PopupMenuLast.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Delete" menu item.
      //----------

      IF i_PopupMenuDeleteNum > 0 THEN
         IF i_AllowControlDelete     THEN
            IF i_PopupMenuDelete.Enabled <> l_DeleteOk THEN
               i_PopupMenuDelete.Enabled =  l_DeleteOk
            END IF
         ELSE
            i_PopupMenuDelete.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "New" menu item.
      //----------

      IF i_AllowControlNew THEN
         IF i_PopupMenuNewNum > 0 THEN
            IF i_PopupMenuNew.Enabled <> l_NewOk THEN
               i_PopupMenuNew.Enabled =  l_NewOk
            END IF
         END IF
      ELSE
         i_PopupMenuNew.Enabled = FALSE
      END IF

      //----------
      //  The "View" menu item.
      //----------

      IF i_PopupMenuViewNum > 0 THEN
         IF i_AllowControlView THEN
            IF i_PopupMenuView.Enabled <> l_ViewOk THEN
               i_PopupMenuView.Enabled =  l_ViewOk
            END IF
         ELSE
            i_PopupMenuView.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Modify" menu item.
      //----------

      IF i_PopupMenuModifyNum > 0 THEN
         IF i_AllowControlModify THEN
            IF i_PopupMenuModify.Enabled <> l_ModifyOk THEN
               i_PopupMenuModify.Enabled =  l_ModifyOk
            END IF
         ELSE
            i_PopupMenuModify.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Insert" menu item.
      //----------

      IF i_PopupMenuInsertNum > 0 THEN
         IF i_AllowControlInsert THEN
            IF i_PopupMenuInsert.Enabled <> l_InsertOk THEN
               i_PopupMenuInsert.Enabled =  l_InsertOk
            END IF
         ELSE
            i_PopupMenuInsert.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Query" menu item.
      //----------

      IF i_PopupMenuQueryNum > 0 THEN
         IF i_AllowControlQuery THEN
            IF i_PopupMenuQuery.Enabled <> l_QueryOk THEN
               i_PopupMenuQuery.Enabled =  l_QueryOk
            END IF
         ELSE
            i_PopupMenuQuery.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Search" menu item.
      //----------

      IF i_PopupMenuSearchNum > 0 THEN
         IF i_AllowControlSearch THEN
            IF i_PopupMenuSearch.Enabled <> l_SearchOk THEN
               i_PopupMenuSearch.Enabled =  l_SearchOk
            END IF
         ELSE
            i_PopupMenuSearch.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Filter" menu item.
      //----------

      IF i_PopupMenuFilterNum > 0 THEN
         IF i_AllowControlFilter THEN
            IF i_PopupMenuFilter.Enabled <> l_FilterOk THEN
               i_PopupMenuFilter.Enabled =  l_FilterOk
            END IF
         ELSE
            i_PopupMenuFilter.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Save" menu item.
      //----------

      IF i_PopupMenuSaveNum > 0 THEN
         IF i_AllowControlSave THEN
            IF i_PopupMenuSave.Enabled <> l_SaveOk THEN
               i_PopupMenuSave.Enabled =  l_SaveOk
            END IF
         ELSE
            i_PopupMenuSave.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Save Rows As" menu item.
      //----------

      IF i_PopupMenuSaveRowsAsNum > 0 THEN
         IF i_AllowControlSaveRowsAs THEN
            IF i_PopupMenuSaveRowsAs.Enabled <> l_SaveRowsAsOk THEN
               i_PopupMenuSaveRowsAs.Enabled =  l_SaveRowsAsOk
            END IF
         ELSE
            i_PopupMenuSaveRowsAs.Enabled = FALSE
         END IF
      END IF

      //----------
      //  The "Print" menu item.
      //----------

      IF i_PopupMenuPrintNum > 0 THEN
         IF i_AllowControlPrint THEN
            IF i_PopupMenuPrint.Enabled <> l_PrintOk THEN
               i_PopupMenuPrint.Enabled =  l_PrintOk
            END IF
         ELSE
            i_PopupMenuPrint.Enabled = FALSE
         END IF
      END IF
   END IF
END IF

Finished:

//----------
//  We set the i_SetControlMode to let the developer know why
//  this event was triggered.
//----------

i_SetControlMode = l_ControlMode
i_ExtendMode     = i_InUse
end on

on pcd_setrowindicator;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_SetRowIndicator
//  Description   : Sets up the current row indicator as the
//                  DataWindow changes states.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_HaveFocus

//----------
//  See if we have focus.
//----------

l_HaveFocus = (GetFocus() = THIS)

//----------
//  If the DataWindow is not in use, then we don't want a row
//  focus indicator.
//----------

IF NOT i_InUse OR i_IsEmpty THEN
   IF (l_HaveFocus     AND i_CursorRowFocusRect) OR &
      (NOT l_HaveFocus AND i_CursorRowPointer) THEN
      IF i_EmptyRowIsPicture THEN
         SetRowFocusIndicator(i_EmptyRowPicture,   &
                              i_EmptyRowXPosition, &
                              i_EmptyRowYPosition)
      ELSE
         SetRowFocusIndicator(i_EmptyRowInd,       &
                              i_EmptyRowXPosition, &
                              i_EmptyRowYPosition)
      END IF
   END IF
   GOTO Finished
END IF

//----------
//  Turn the current row indicator off while we are in "QUERY"
//  mode since it does not look very good.
//----------

IF i_DWState = c_DWStateQuery THEN
   IF (l_HaveFocus     AND i_CursorRowFocusRect) OR &
      (NOT l_HaveFocus AND i_CursorRowPointer) THEN
      IF i_QueryRowIsPicture THEN
         SetRowFocusIndicator(i_QueryRowPicture,   &
                              i_QueryRowXPosition, &
                              i_QueryRowYPosition)
      ELSE
         SetRowFocusIndicator(i_QueryRowInd,       &
                              i_QueryRowXPosition, &
                              i_QueryRowYPosition)
      END IF
   END IF
   GOTO Finished
END IF

//----------
//  Set the current row indicator.
//----------

IF l_HaveFocus THEN

   //----------
   //  See if we are to handle the current row indicator when the
   //  DataWindow has focus.
   //----------

   IF i_CursorRowFocusRect THEN
      IF i_FocusRowIsPicture THEN
         SetRowFocusIndicator(i_FocusRowPicture,   &
                              i_FocusRowXPosition, &
                              i_FocusRowYPosition)
      ELSE
         SetRowFocusIndicator(i_FocusRowInd,       &
                              i_FocusRowXPosition, &
                              i_FocusRowYPosition)
      END IF
   END IF
ELSE

   //----------
   //  See if we are to handle the current row indicator when the
   //  DataWindow does not have focus.
   //----------

   IF i_CursorRowPointer THEN
      IF i_NotFocusRowIsPicture THEN
         SetRowFocusIndicator(i_NotFocusRowPicture,   &
                              i_NotFocusRowXPosition, &
                              i_NotFocusRowYPosition)
      ELSE
         SetRowFocusIndicator(i_NotFocusRowInd,       &
                              i_NotFocusRowXPosition, &
                              i_NotFocusRowYPosition)
      END IF
   END IF
END IF

Finished:

i_ExtendMode = i_InUse
end on

on pcd_update;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Update
//  Description   : Update the DataWindow rows to the database.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//----------
//  If this is an external DataWindow, bypass this event.
//----------

IF IsNull(i_DBCA) THEN
   GOTO Finished
END IF

//----------
//  Use the PowerBuilder UPDATE() function to update the rows to
//  the database.
//----------

l_Error = Update(c_NoAcceptText, c_NoResetFlags)

//----------
//  Check for errors that occurred during the UPDATE().
//----------

IF l_Error <> 1 THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

Finished:

i_ExtendMode = i_InUse
end on

on pcd_validate;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Validate
//  Description   : Cycles through each row and column for
//                  validation checking in preparation for updating
//                  the database.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN        l_CheckCurrentRow, l_Modified
INTEGER        l_SaveCol,         l_InSave
LONG           l_ModifiedCnt, l_SaveRow, l_NumSelected, l_Idx
LONG           l_SelectedRows[]
DWITEMSTATUS   l_ItemStatus
UO_DW_MAIN     l_ChildDW

//----------
//  If the root is in save, then this DataWindow is in save.
//----------

l_InSave = i_InSave
i_InSave = i_RootDW.i_InSave

//----------
//  Get_Event_Info() returns status about what stage of processing
//  this event is in (e.g. cascading).
//----------

Get_Event_Info(c_EventNoTriggerUp)

//----------
//  See if this DataWindow is the root of the event.
//----------

IF is_EventInfo.Event_Root THEN

   //----------
   //  Set the hour glass and clear PCCA.Error.
   //----------

   SetPointer(HourGlass!)
   PCCA.Error = c_Success

   //----------
   //  Display the validation prompt.
   //----------

   PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Validate, c_ShowMark)
END IF

//----------
//  If we are cascading, then cascade the event to the children
//  DataWindows.
//----------

IF is_EventInfo.Cascading THEN

   //----------
   //  Assume that none of our children DataWindows have
   //  modications.
   //----------

   i_ChildrenAreModified = FALSE

   //----------
   //  Validate the children DataWindows.
   //----------

   FOR l_Idx = 1 TO i_NumChildren
      l_ChildDW                           = i_ChildDW[l_Idx]
      l_ChildDW.is_EventControl.Cascading = TRUE
      l_ChildDW.TriggerEvent("pcd_Validate")

      //----------
      //  If we found a validation error, then we don't have to
      //  do any more validating - one is enough.
      //----------

      IF PCCA.Error <> c_Success THEN
         EXIT
      END IF
   NEXT

   //----------
   //  Make sure that there were not any valdiation errors in any
   //  of the normal children DataWindows.  If there were not,
   //  then proceed with validating the Instance DataWindows.
   //----------

   IF PCCA.Error = c_Success THEN
      FOR l_Idx = 1 TO i_NumInstance
         l_ChildDW = i_InstanceDW[l_Idx]
         l_ChildDW.is_EventControl.Cascading = TRUE
         l_ChildDW.TriggerEvent("pcd_Validate")

         //----------
         //  If we found a validation error, then we don't have to
         //  do any more validating - one is enough.
         //----------

         IF PCCA.Error <> c_Success THEN
            EXIT
         END IF
      NEXT
   END IF

   //----------
   //  Because the cursor flashes as we move it from field to
   //  field during validation, turn redraw off.
   //----------

   SetRedraw(c_BufferDraw)
   i_RedrawCount = i_RedrawCount + 1
   i_RedrawStack[i_RedrawCount] = c_BufferDraw

   //----------
   //  If validation has been successful up to this point, go
   //  ahead and validate this DataWindow.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  Save the current row and column so that they can get
      //  restored after the validation procedure is finished.
      //----------

      l_SaveRow = GetRow()
      l_SaveCol = GetColumn()

      //----------
      //  Do an AcceptText() to get any text the user has typed
      //  into the DataWindow.  Check for errors on the
      //  AcceptText().  If the AcceptText() fails, then we have
      //  a validation error.  It will already have been reported
      //  to the user because the ItemChanged! and/or ItemError!
      //  events get triggered during the AcceptText().
      //----------

      IF AcceptText() = -1 THEN

         //----------
         //  PCCA.Error contains the validation-style error code.
         //  Change it to a PowerClass error code.
         //----------

         PCCA.Error = c_Fatal
      ELSE

         //----------
         //  PCCA.Error contains the validation-style success code.
         //  Change it to a PowerClass success code.
         //----------

         PCCA.Error = c_Success

         //----------
         //  If the developer specified that required field
         //  checking was not be done all of the time, then it
         //  may not have been trapped by the AcceptText.  Do
         //  it now before the save happens.
         //----------

         IF NOT i_AlwaysCheckRequired THEN
         IF l_SaveRow > 0             THEN
         IF l_SaveCol > 0             THEN

            l_ItemStatus = GetItemStatus(l_SaveRow, 0, Primary!)
            IF l_ItemStatus <> New! OR NOT i_IgnoreNewRows THEN

               i_RowNbr = l_SaveRow
               i_ColNbr = l_SaveCol

               //----------
               //  Update i_ColRequiredError variable.
               //----------

               Get_Val_Info(i_ColNbr)

               //----------
               //  If it will generate an error, call
               //  Validate_Columns() to trap and output
               //  the error.
               //----------

               IF i_ColRequiredError THEN
                  Validate_Columns(c_ValidateCurCol, 0)
               END IF
            END IF
         END IF
         END IF
         END IF
      END IF

      //----------
      //  ItemChanged! and/or ItemError! may have been triggered
      //  by the AcceptText() call.  Reset i_ItemValidated to
      //  indicate that validation is ready to go again.
      //----------

      i_ItemValidated = FALSE
   END IF

   //----------
   //  If validation is still successful, keep going.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  Turn RFC, VScroll, and validation processing off while
      //  we cycle through the columns to do validation.
      //----------

      i_IgnoreRFC      = (NOT c_IgnoreRFC)
      i_IgnoreRFCCount = i_IgnoreRFCCount + 1
      i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

      i_IgnoreVScroll = (NOT c_IgnoreVScroll)
      i_IgnoreVSCount = i_IgnoreVSCount + 1
      i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

      i_IgnoreVal      = (NOT c_IgnoreVal)
      i_IgnoreValCount = i_IgnoreValCount + 1
      i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

      //----------
      //  We need to continue validation checking if this
      //  DataWindow:
      //     a) has modifications OR
      //     b) has New! rows that are not to be ignored OR
      //     c) has New! rows and have children with modifications.
      //----------

      l_ModifiedCnt = i_SharePrimary.ModifiedCount()
      l_Modified    = (l_ModifiedCnt > 0)

      IF NOT l_Modified                 THEN
      IF i_SharePrimary.i_ShareDoSetKey THEN

         l_Modified = (NOT i_IgnoreNewRows)

         IF NOT l_Modified        THEN
         IF i_ChildrenAreModified THEN

            l_NumSelected = Get_Selected_Rows(l_SelectedRows[])
            FOR l_Idx = 1 TO l_NumSelected
               l_ItemStatus = &
                  GetItemStatus(l_SelectedRows[l_Idx], 0, Primary!)
               IF l_ItemStatus = New! THEN
                  l_Modified = TRUE
                  EXIT
               END IF
            NEXT
         END IF
         END IF
      END IF
      END IF

      IF l_Modified THEN

         //----------
         //  There are changes in this DataWindow.  Set this
         //  flag so that it will get proprogates to the parent
         //  DataWindow.
         //----------

         i_ChildrenAreModified = TRUE

         //----------
         //  We want to validate the row that the user is currently
         //  on first.  If there are not any errors, then we will
         //  cycle through the entire DataWindow looking for
         //  modified rows.  See if there is a current row.
         //----------

         l_CheckCurrentRow = FALSE

         IF l_SaveRow > 0 THEN
            l_ItemStatus = GetItemStatus(l_SaveRow, 0, Primary!)
            IF l_ItemStatus <> NotModified!                THEN
            IF l_ItemStatus <> New! OR NOT i_IgnoreNewRows THEN
               l_CheckCurrentRow = TRUE
            END IF
            END IF
         END IF

         //----------
         //  If there is a current row, set up i_RowNbr to check
         //  the current row first.  If there is not a current row,
         //  then just look for the first modified row in this
         //  DataWindow.
         //----------

         IF l_CheckCurrentRow THEN
            i_RowNbr = l_SaveRow
         ELSE
            i_RowNbr = GetNextModified(0, Primary!)
         END IF

         //----------
         //  Do validation checking for each row and column in the
         //  DataWindow that has been changed.
         //----------

         DO WHILE i_RowNbr > 0 AND PCCA.Error = c_Success

            //----------
            //  Since we start with the row that the user is
            //  currently on and then look through the entire
            //  DataWindow for modified rows, we could possibly
            //  validate the current row twice.  The following IF
            //  just prevents that from happening.
            //----------

            IF l_CheckCurrentRow OR i_RowNbr <> l_SaveRow THEN

               //----------
               //  Validate the columns on this row.
               //----------

             IF l_CheckCurrentRow THEN
                Validate_Columns(c_ValidateAllCols, l_SaveCol)
             ELSE
                Validate_Columns(c_ValidateAllCols, 0)
             END IF

               //----------
               //  Do any columns that need to be cross-validated
               //  (e.g. state with country)?  This event must be
               //  coded by the developer.
               //----------
               //  Validation routines are expected to do their own
               //  error display.  Therefore, i_DisplayError is not
               //  checked for developer-coded validation routines.
               //----------

               IF PCCA.Error = c_Success THEN
                  TriggerEvent("pcd_ValidateRow")
               END IF
            END IF

            //----------
            //  As long as no errors have occurred, keep getting
            //  the next modified row.
            //----------

            IF PCCA.Error = c_Success THEN

               //----------
               //  The first time through this validation loop, we
               //  check the row that the user is currently on.
               //  When we are done checking that row, set i_RowNbr
               //  to 0 so that we will start searching for
               //  modified rows from the beginning of the
               //  DataWindow.
               //----------

               IF l_CheckCurrentRow THEN
                  l_CheckCurrentRow = FALSE
                  i_RowNbr          = 0
               END IF

               //----------
               //  Find the next modified row.
               //----------

               i_RowNbr = GetNextModified(i_RowNbr, Primary!)
            END IF
         LOOP

         //----------
         //  This is semi-redundant.  Validate_Columns() also does
         //  required field checking, but this is more robust.
         //  Validate_Columns() only checks columns that are on the
         //  screen.
         //----------

         IF PCCA.Error = c_Success THEN
            i_RowNbr = 1
            i_ColNbr = 1
         END IF

         DO WHILE PCCA.Error = c_Success AND i_RowNbr > 0
            IF FindRequired(Primary!,  i_RowNbr, i_ColNbr, &
                            i_ColName, FALSE) = 1 THEN
               IF i_RowNbr > 0 THEN
               IF i_ColNbr > 0 THEN

                  //----------
                  //  Get the status of the row which contains
                  //  required fields.
                  //----------

                  l_ItemStatus = &
                     GetItemStatus(i_RowNbr, 0, Primary!)

                  //----------
                  //  If the row is not a New! row or if the
                  //  developer specified that New! rows were to
                  //  be processed, then flag the required field.
                  //----------

                  IF l_ItemStatus <> New! OR &
                     NOT i_IgnoreNewRows THEN

                     //----------
                     //  Move to the field with the error.
                     //----------

                     SetRow(i_RowNbr)
                     SetColumn(i_ColNbr)

                     //----------
                     //  We found a required field that was not
                     //  caught.  We'll give the developer the
                     //  first crack at reporting the error.  Get
                     //  the variables that the developer uses for
                     //  validation ready.
                     //----------

                     Get_Val_Info(i_ColNbr)
                     i_ColRequiredError = TRUE
                     i_ColText          = ""

                     //----------
                     //  Initialize PCCA.Error to indicate that
                     //  validation processing has not been done.
                     //  Initialize i_DisplayRequired to indicate
                     //  that required field errors should be
                     //  reported.  If the developer does not want
                     //  PowerClass to do required field reporting,
                     //  they can set this flag to FALSE.
                     //----------

                     PCCA.Error        = c_ValNotProcessed
                     i_DisplayRequired = TRUE

                     //----------
                     //  Performs validation on column by column
                     //  basis.  This event must be coded by the
                     //  developer.
                     //----------
                     //  Validation routines are expected to do
                     //  their own error display.  Therefore,
                     //  i_DisplayError is not checked for
                     //  developer-coded validation routines.
                     //----------

                     TriggerEvent("pcd_ValidateCol")

                     //----------
                     //  We need to display the required field
                     //  error if:
                     //     a) The developer's routine did not
                     //        catch or process it AND
                     //     b) The developer said it was Ok to
                     //        display it.
                     //----------

                     IF (PCCA.Error = c_ValOk            OR  &
                         PCCA.Error = c_ValNotProcessed) AND &
                        i_DisplayRequired                THEN

                        IF i_ColVisible[i_ColNbr] THEN
                           Display_DW_Val_Error &
                              (PCCA.MB.c_MBI_DW_RequiredField, "")
                        ELSE
                           Display_DW_Val_Error &
                              (PCCA.MB.c_MBI_DW_NonVisRequiredField, &
                               "")
                        END IF

                        //----------
                        //  Make sure that PCCA.Error indicates
                        //  failure.
                        //----------

                        PCCA.Error = c_Fatal
                     ELSE

                        //----------
                        //  The developer handled the required
                        //  field error.  However, we need to
                        //  convert from validation error codes
                        //  to PowerClass error codes.
                        //----------

                        IF PCCA.Error = c_ValFailed THEN
                           PCCA.Error = c_Fatal
                        ELSE
                           PCCA.Error = c_Success
                        END IF
                     END IF
                  ELSE

                     //----------
                     //  If we get to here we have a New! record,
                     //  but the developer specified that New!
                     //  records were to be ignored.  Try to find
                     //  the next row that has a requried field
                     //  error.
                     //----------

                     i_RowNbr = i_RowNbr + 1
                  END IF
               END IF
               END IF
            END IF
         LOOP

         //----------
         //  If there were not any errors, then restore the
         //  original row and column.
         //----------

         IF PCCA.Error = c_Success THEN
            IF l_SaveRow > 0 THEN
               IF l_SaveRow <> GetRow() THEN
                  SetRow(l_SaveRow)
               END IF
            END IF
            IF l_SaveCol > 0 THEN
               IF l_SaveCol <> GetColumn() THEN
                  SetColumn(l_SaveCol)
               END IF
            END IF
         END IF
      END IF

      //----------
      //  Restore RFC, VScroll, and validation processing to their
      //  prior state.
      //----------

      IF i_IgnoreValCount > 1 THEN
         i_IgnoreValCount = i_IgnoreValCount - 1
         i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
      ELSE
         i_IgnoreValCount = 0
         i_IgnoreVal      = FALSE
      END IF

      IF i_IgnoreVSCount > 1 THEN
         i_IgnoreVSCount = i_IgnoreVSCount - 1
         i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
      ELSE
         i_IgnoreVSCount = 0
         i_IgnoreVScroll = FALSE
      END IF
      IF NOT i_IgnoreVScroll THEN
         i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
      END IF

      IF i_IgnoreRFCCount > 1 THEN
         i_IgnoreRFCCount = i_IgnoreRFCCount - 1
         i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
      ELSE
         i_IgnoreRFCCount = 0
         i_IgnoreRFC      = FALSE
      END IF
   END IF

   //----------
   //  Restore redraw processing to the prior state.
   //----------

   IF i_RedrawCount > 1 THEN
      i_RedrawCount = i_RedrawCount - 1
      SetRedraw(i_RedrawStack[i_RedrawCount])
   ELSE
      i_RedrawCount = 0
      SetRedraw(TRUE)
   END IF

   //----------
   //  If there are changes in this DataWindow (or its children),
   //  then tell the parent DataWindow about it.
   //----------

   IF i_ChildrenAreModified THEN
      IF i_ParentIsValid THEN
         i_ParentDW.i_ChildrenAreModified = TRUE
      END IF
   END IF
END IF

//----------
//  If this DataWindow is the root of the event, then we need to
//  take care of a couple more details before we are done with
//  validation.
//----------

IF is_EventInfo.Event_Root THEN

   //----------
   //  Is there an other validation that must be done after all the
   //  rows and columns have been validated?  This event must be
   //  coded by the developer.
   //----------
   //  Validation routines are expected to do their own error
   //  display.  Therefore, i_DisplayError is not checked for
   //  developer-coded validation routines.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  Trigger the developer's event to do cross validation.
      //----------

      TriggerEvent("pcd_ValidateAfter")
   END IF

   //----------
   //  Remove the validation prompt.
   //----------

   PCCA.MDI.fu_Pop()
END IF

//----------
//  For the developer, we set the instance variables to reflect
//  the state of processing by this event.
//----------

i_IsFinal     = is_EventInfo.Event_Recipient
i_IsRoot      = is_EventInfo.Event_Root
i_IsCascading = is_EventInfo.Cascading
i_ExtendMode  = is_EventInfo.Cascading

//----------
//  Restore the original value of i_InSave.
//----------

i_InSave = l_InSave
end on

on pcd_view;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_View
//  Description   : Change to "VIEW" mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_SetFocus
UNSIGNEDLONG  l_RefreshCmd
UO_DW_MAIN    l_ToDW, l_ChildDW

//----------
//  If we were not triggered by pcd_Refresh, do normal processing.
//----------

IF NOT i_ProcessMode THEN

   //----------
   //  Make sure that this DataWindow is in use.
   //----------

   IF NOT i_InUse THEN
      PCCA.Error = c_Fatal
      GOTO Finished
   END IF

   //----------
   //  If the DataWindow is in "QUERY" mode, then we don't process
   //  mode-change events.
   //----------

   IF i_DWState = c_DWStateQuery THEN
      Change_DW_Focus(THIS)
      PCCA.Error = c_Fatal
      GOTO Finished
   END IF

   //----------
   //  Indicate to the user that we are processing the mode change.
   //----------

   SetPointer(HourGlass!)
   PCCA.Error = c_Success

   PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_View, c_Show)

   //----------
   //  Call Load_Rows() to load selections that have been made
   //  by the user, but not yet loaded into the children
   //  DataWindows.
   //----------

   IF PCCA.PCMGR.i_DW_NeedRetrieve THEN
      IF Load_Rows(PCCA.Null_Object, THIS) <> c_Success THEN
         PCCA.Error = c_Fatal
         PCCA.MDI.fu_Pop()
         GOTO Finished
      END IF
   END IF

   //----------
   //  Check for changes.
   //----------

   Check_Save(l_RefreshCmd)

   //----------
   //  As long as there were not errors (e.g. validation error),
   //  then go to "VIEW" mode.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  If l_RefreshCmd is a valid refresh mode, then the
      //  DataWindow hierarchy needs to be refreshed because a
      //  save or an abort changes happened.  However, we tell
      //  pcd_Refresh to skip this DataWindow's branch as we will
      //  refresh it ourselves using c_RefreshView.
      //----------

      IF l_RefreshCmd <> c_RefreshUndefined THEN
         i_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
         i_RootDW.is_EventControl.Skip_DW_Valid = TRUE
         i_RootDW.is_EventControl.Skip_DW       = THIS
         i_RootDW.TriggerEvent("pcd_Refresh")
      END IF

      //----------
      //  Put this DataWindow and its children into "VIEW" mode.
      //----------

      IF PCCA.Error = c_Success THEN
         is_EventControl.Refresh_Cmd = c_RefreshView
         TriggerEvent("pcd_Refresh")
      END IF
   END IF

   //----------
   //  If pcd_Refresh successfully changed this DataWindow and
   //  its children DataWindows to "VIEW" mode, then we want
   //  to set focus.  PCCA.PCMGR.i_DW_FocusDone tells us if
   //  the developer already has taken care of focus.
   //----------

   IF NOT PCCA.PCMGR.i_DW_FocusDone THEN
   IF PCCA.Error = c_Success        THEN

      //----------
      //  If the developer specified a preferred-focus DataWindow,
      //  we want to set focus to it.
      //----------

      l_SetFocus = FALSE

      IF i_ShowRow > 0 THEN
         IF IsValid(i_FocusDW) THEN
            IF i_FocusDW.i_InUse THEN
               l_SetFocus = TRUE
               l_ToDW     = i_FocusDW
            END IF
         END IF
      END IF

      //----------
      //  If this DataWindow allows "MODIFY" or "NEW" modes, then
      //  just leave the focus here.
      //----------

      IF i_AllowModify THEN
         l_SetFocus = TRUE
         l_ToDW     = THIS
      ELSE
         IF i_AllowNew THEN
            l_SetFocus = TRUE
            l_ToDW     = THIS
         END IF
      END IF

      IF NOT l_SetFocus THEN

         //----------
         //  If there are Instance DataWindows, we will set
         //  focus to the Instance DataWindow that corresponds
         //  to the currently selected row.
         //----------

         IF i_CurInstance > 0 THEN
            l_ChildDW  = i_InstanceDW[i_CurInstance]
            l_SetFocus = TRUE
            l_ToDW     = l_ChildDW
         END IF

         //----------
         //  If the DataWindow that is to get focus was not set
         //  to one of the Instance DataWindows, then see if it
         //  should be set to one of the normal children
         //  DataWindows.
         //----------

         IF NOT l_SetFocus THEN

            //----------
            //  If there are normal children DataWindows and
            //  there is a row selected (i.e. the children
            //  DataWindows contain data), then try to find a
            //  DataWindow that is in use and is not on the same
            //  window.  We search backwards through the child
            //  DataWindow array so that focus will get set to
            //  the most recently created child DataWindow.
            //----------

            IF i_ShowRow > 0 THEN
               IF i_NumChildren > 0 THEN
                  l_ChildDW  = i_ChildDW[i_NumChildren]
                  l_SetFocus = TRUE
                  l_ToDW     = l_ChildDW
               END IF
            END IF
         END IF
      END IF

      //----------
      //  If we couldn't find a better choice for the DataWindow
      //  to set focus to, just set it to the DataWindow that
      //  received the pcd_View event (i.e. this DataWindow).
      //----------

      IF NOT l_SetFocus THEN
         l_ToDW = THIS
      END IF

      //----------
      //  Make the DataWindow that was our best choice the
      //  current DataWindow.
      //----------

      Set_Auto_Focus(l_ToDW)
   END IF
   END IF

   //----------
   //  Remove the "VIEW" mode prompt.
   //----------

   PCCA.MDI.fu_Pop()
END IF

Finished:

//----------
//  Set the flag to let the developer know if they should execute
//  their extended code at this time.
//----------

i_ExtendMode = i_ProcessMode
end on

on pcd_wm_hscroll;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_WM_HScroll
//  Description   : This is event is triggered when the user
//                  scrolls the DataWindow using the horizontal
//                  scroll bar.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Scrolling is to be ignored when it is generated by pcd_Refresh.
//----------

IF PCCA.PCMGR.i_DW_InRefresh = 0 THEN

   //----------
   //  If PCCA.PCMGR.i_ProcessingActivate is TRUE, then the
   //  current DataWindow may not have been set to the correct
   //  DataWindow on this window.  However, since a ScrollBar
   //  event changes the current DataWindow, we can handle it
   //  here.  All we have to do is de-fuse activate processing
   //  and make sure that the GetFocus event is run on this
   //  DataWindow.
   //----------

   IF PCCA.PCMGR.i_ProcessingActivate THEN
      IF NOT i_IgnoreVScroll THEN
         PCCA.PCMGR.Set_DW_Deactivate()
         TriggerEvent("GetFocus")
      ELSE
         PCCA.PCMGR.Repost_Events()
      END IF
   END IF
END IF

i_ExtendMode = i_InUse
end on

on pcd_wm_vscroll;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_WM_VScroll
//  Description   : This is event is triggered when the user
//                  scrolls the DataWindow using the vertical
//                  scroll bar.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//----------
//  If an event triggers this event and you want to bypass the
//  ScrollVertical! code, then do the following:
//
//      Push_DW_VScroll(c_IgnoreVScroll)
//      <code the generates the ScrollVertical! Event>
//      Pop_DW_VScroll()
//
//  Do not modify the i_IgnoreVScroll instance variable!
//----------

IF NOT i_IgnoreVScroll THEN

   //----------
   //  If PCCA.PCMGR.i_ProcessingActivate is TRUE, then the
   //  current DataWindow may not have been set to the correct
   //  DataWindow on this window.  However, since a ScrollBar
   //  event changes the current DataWindow, we can handle it
   //  here.  All we have to do is de-fuse activate processing
   //  and make sure that the GetFocus event is run on this
   //  DataWindow.
   //----------

   IF PCCA.PCMGR.i_ProcessingActivate THEN
      PCCA.PCMGR.Set_DW_Deactivate()
      TriggerEvent("GetFocus")
   END IF
END IF

//----------
//  Set the flag to let the developer know if they should execute
//  their extended code at this time.
//----------

i_ExtendMode = (NOT i_IgnoreVScroll AND i_InUse)
end on

public subroutine batch_attrib (boolean on_or_off);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Batch_Attrib
//  Description   : Tells PowerClass not to process the
//                  developer's new attributes on each
//                  call to Modify_DW_Attrib()
//
//  Parameters    : BOOLEAN On_Or_Off -
//                        Specifies if developer calls to
//                        Modify_DW_Attrib() are to be
//                        batched until Batch_Attrib() is
//                        turned off.  Valid options are:
//
//                           - c_BatchAttrib
//                           - c_ProcessAttrib
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

LONG    l_SaveRow,        l_InsertRowNbr
STRING  l_ViewModeBorder, l_ViewModeColor, l_InActiveDWColor

i_BatchAttrib = On_Or_Off

IF i_BatchAttrib THEN
   i_BatchString = ""
ELSE

   //----------
   //  What we are about to do may trigger RFC or validation
   //  events.  Turn off processing of RFC, validation, VScroll,
   //  and redraw.
   //----------

   i_IgnoreRFC      = (NOT c_IgnoreRFC)
   i_IgnoreRFCCount = i_IgnoreRFCCount + 1
   i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

   i_IgnoreVal      = (NOT c_IgnoreVal)
   i_IgnoreValCount = i_IgnoreValCount + 1
   i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

   i_IgnoreVScroll = (NOT c_IgnoreVScroll)
   i_IgnoreVSCount = i_IgnoreVSCount + 1
   i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

   SetRedraw(c_BufferDraw)
   i_RedrawCount = i_RedrawCount + 1
   i_RedrawStack[i_RedrawCount] = c_BufferDraw

   //----------
   //  The following code may cause the row to get reset, so save
   //  so that we can restore it.
   //----------

   l_SaveRow = GetRow()

   //----------
   //  We need to make sure a row is inserted, otherwise the DDLB
   //  arrows will be left.
   //----------

   IF RowCount() = 0 THEN
      l_InsertRowNbr = InsertRow(0)
   END IF

   //----------

   IF i_ViewModeBorderIdx <> c_DW_BorderUndefined THEN
      l_ViewModeBorder = PCCA.MDI.c_DW_Borders[i_ViewModeBorderIdx]
   ELSE
      l_ViewModeBorder = ""
   END IF

   IF i_ViewModeColorIdx <> c_ColorUndefined THEN
      l_ViewModeColor = PCCA.MDI.c_RGBStrings[i_ViewModeColorIdx]
   ELSE
      l_ViewModeColor = ""
   END IF

   PCCA.DLL.fu_BuildTabStrings          &
      (THIS,             i_NumColumns,     &
       i_TabOrder[],     i_ColVisible[],   &
       i_FirstTabColumn, i_FirstVisColumn, &
       i_TabLength,      i_EnableTabs,     &
       i_DisableTabs,    i_QueryTabs)

   PCCA.DLL.fu_BuildAttribStrings           &
      (THIS,                 i_NumColumns,     &
       i_TabOrder[],         i_ColVisible[],   &
       i_ColBorders[],       i_ColColors[],    &
       i_ColBColors[],       l_ViewModeBorder, &
       l_ViewModeColor,      i_AttribLength,   &
       i_ModifyBorders,      i_ViewBorders,    &
       i_ModifyColors,       i_ViewColors,     &
       i_NonEmptyTextColors, i_EmptyTextColors)

   IF i_InactiveDWColorIdx <> c_ColorUndefined THEN
      l_InactiveDWColor = &
         PCCA.MDI.c_RGBStrings[i_InactiveDWColorIdx]

      PCCA.DLL.fu_BuildActiveStrings                  &
         (THIS,                                          &
          i_NumObjects, i_ObjectNames[],     &
          i_ObjectTypes[],          i_ObjectColors[],    &
          i_InactiveCol,            i_InactiveText,      &
          i_InactiveLine,           l_InactiveDWColor,   &
          l_ViewModeColor,          i_ActiveLength,      &
          i_ActiveNonEmptyColors,   i_ActiveEmptyColors, &
          i_InactiveNonEmptyColors, i_InactiveEmptyColors)
   END IF

   //----------
   //  i_BatchString modifies the visiblity and attributes of
   //  columns not tracked by PowerClass.
   //----------

   IF i_BatchString <> "" THEN
      i_BatchString = Modify(i_BatchString)
      i_BatchString = ""
   END IF

   //----------
   //  If we are in "QUERY" mode, we always want to update the
   //  borders, colors, and tabs.
   //----------

   IF i_DWState = c_DWStateQuery THEN
      Modify(i_QueryTabs)

      IF i_ViewModeBorderIdx <> c_DW_BorderUndefined THEN
         Modify(i_ModifyBorders)
      END IF

      IF i_ViewModeColorIdx <> c_ColorUndefined THEN
         Modify(i_ModifyColors)
      END IF
   ELSE

      //----------
      //  Make sure the new tabs are set (this will also do colors
      //  and borders for non-"VIEW" modes).
      //----------

      IF i_CurrentMode <> c_ModeView THEN
         i_DWState = c_DWStateUndefined
         TriggerEvent("pcd_Enable")
      END IF
   END IF

   //----------
   //  Handle inactive colors.
   //----------

   IF i_InactiveDWColorIdx <> c_ColorUndefined THEN
      IF i_Active THEN
         IF i_SetEmptyColors AND i_DWState <> c_DWStateQuery THEN
            Modify(i_ActiveEmptyColors)
         ELSE
            Modify(i_ActiveNonEmptyColors)
         END IF
      ELSE
         IF i_SetEmptyColors AND i_DWState <> c_DWStateQuery THEN
            Modify(i_InactiveEmptyColors)
         ELSE
            Modify(i_InactiveNonEmptyColors)
         END IF
      END IF
   END IF

   IF NOT i_InactiveCol THEN

      //----------
      //  If the DataWindow is empty make sure that the colors are
      //  set to their correct state.
      //----------

      IF i_SetEmptyColors AND i_DWState <> c_DWStateQuery THEN
         Modify(i_EmptyTextColors)
      ELSE
         Modify(i_NonEmptyTextColors)
      END IF
   END IF

   //----------
   //  If we had to insert an empty row, the DataWindow is empty.
   //  Do EMPTY processing.
   //----------

   IF l_InsertRowNbr > 0 THEN

      //----------
      //  If this is a free-form DataWindow, then leave the
      //  record that was inserted.  This prevents PowerBuilder
      //  from removing the column labels and borders.
      //----------

      IF i_DoEmpty THEN
         i_SharePrimary.i_AddedEmpty = TRUE
      END IF

      i_SharePrimary.Share_State(c_ShareEmpty, i_DoEmpty)

      //----------
      //  We left the dummy row in while we synced the other share
      //  DataWindows.  If the developer does not want it, remove
      //  it now.
      //----------

      IF NOT i_DoEmpty THEN
         DeleteRow(l_InsertRowNbr)
      END IF

      IF i_SharePrimary.i_ShareDoSetKey THEN
         Share_Flags(c_ShareAllDoSetKey, FALSE)
      END IF
   END IF

   //----------
   //  If there was a current row before we blasted changes to the
   //  DataWindow, restore it.
   //----------

   IF l_SaveRow > 0 THEN
      IF l_SaveRow <> GetRow() THEN
         SetRow(l_SaveRow)
      END IF
   END IF

   //----------
   //  Restore processing of RFC, validation, VScroll, and redraw
   //  to their previous states.
   //----------

   IF i_RedrawCount > 1 THEN
      i_RedrawCount = i_RedrawCount - 1
      SetRedraw(i_RedrawStack[i_RedrawCount])
   ELSE
      i_RedrawCount = 0
      SetRedraw(TRUE)
   END IF

   IF i_IgnoreValCount > 1 THEN
      i_IgnoreValCount = i_IgnoreValCount - 1
      i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
   ELSE
      i_IgnoreValCount = 0
      i_IgnoreVal      = FALSE
   END IF

   IF i_IgnoreVSCount > 1 THEN
      i_IgnoreVSCount = i_IgnoreVSCount - 1
      i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
   ELSE
      i_IgnoreVSCount = 0
      i_IgnoreVScroll = FALSE
   END IF
   IF NOT i_IgnoreVScroll THEN
      i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
   END IF

   IF i_IgnoreRFCCount > 1 THEN
      i_IgnoreRFCCount = i_IgnoreRFCCount - 1
      i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
   ELSE
      i_IgnoreRFCCount = 0
      i_IgnoreRFC      = FALSE
   END IF
END IF

RETURN
end subroutine

public subroutine cascade_event (string event_name, unsignedlong children_order, unsignedlong post_or_trigger);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Cascade_Event
//  Description   : Cascades the specified event to all of this
//                  DataWindow's children.  Only events that
//                  are not already cascaded should be specifed
//                  with this routine (e.g. pcd_ValidateAfter and
//                  pcd_SaveAfter).
//
//  Parameters    : STRING Event_Name -
//                        The name of the event to be cascaded.
//
//                  UNSIGNEDLONG Children_Order -
//                        Indicates if the children are to be
//                        traversed first or last.  Valid options
//                        are:
//
//                           - c_CascadeFirst
//                           - c_CascadeLast
//
//                  UNSIGNEDLONG Post_Or_Trigger -
//                        Indicates whether the specified event
//                        event should be posted or triggered.
//                        Valid options are:
//
//                           - c_CascadePost
//                           - c_CascadeTrigger
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN      l_InCascadeEvent
INTEGER      l_Idx
UO_DW_MAIN   l_ChildDW

//----------
//  Get_Event_Info() returns status about what stage of processing
//  this event is in (e.g. cascading).
//----------

l_InCascadeEvent = is_EventControl.In_Cascade_Event
Get_Event_Info(c_EventNoTriggerUp)

//----------
//  The In_Cascade_Event field is reset manually, so restore it
//  to its previous value until somebody resets it.
//----------

is_EventControl.In_Cascade_Event = l_InCascadeEvent

//----------
//  See if this routine is the root of the routine.
//----------

IF is_EventInfo.Event_Root THEN

   //----------
   //  If Cascade_Event() is already running, ignore this call
   //  to it.
   //----------

   IF l_InCascadeEvent THEN
      GOTO Finished
   END IF

   //----------
   //  Initialize PCCA.Error to indicate that there have not been
   //  any errors.
   //----------

   PCCA.Error = c_Success
ELSE

  //----------
  //  Since we are not the root-level of the routine, see if the
  //  this DataWindow or children are to be done first.  If this
  //  DataWindow is to be done first, do it now and then cascade
  //  the event to the children.
  //----------

   IF Children_Order = c_CascadeLast THEN
      IF PCCA.Error = c_Success THEN
         is_EventControl.In_Cascade_Event = TRUE
         IF Post_Or_Trigger = c_CascadePost THEN
            PostEvent(Event_Name)
         ELSE
            TriggerEvent(Event_Name)
         END IF

         //----------
         //  The In_Cascade_Event variable is reset manually.
         //----------

         is_EventControl.In_Cascade_Event = FALSE
      END IF
   END IF
END IF

//----------
//  While there have not been any errors, keep cascading the
//  event to the children DataWindows.
//----------

FOR l_Idx = 1 TO i_NumChildren
   IF PCCA.Error <> c_Success THEN
      EXIT
   END IF
   l_ChildDW                           = i_ChildDW[l_Idx]
   l_ChildDW.is_EventControl.Cascading = TRUE
   l_ChildDW.Cascade_Event(Event_Name, Children_Order, Post_Or_Trigger)
NEXT

FOR l_Idx = 1 TO i_NumInstance
   IF PCCA.Error <> c_Success THEN
      EXIT
   END IF
   l_ChildDW                           = i_InstanceDW[l_Idx]
   l_ChildDW.is_EventControl.Cascading = TRUE
   l_ChildDW.Cascade_Event(Event_Name, Children_Order, Post_Or_Trigger)
NEXT

//----------
//  We only want to trigger the specified event on DataWindows
//  which are not the root.
//----------

IF NOT is_EventInfo.Event_Root THEN

   //----------
   //  If children are to be done first, they are now done.
   //  Trigger the specified event on ourselves.
   //----------

   IF Children_Order = c_CascadeFirst THEN
      IF PCCA.Error = c_Success THEN
         is_EventControl.In_Cascade_Event = TRUE
         IF Post_Or_Trigger = c_CascadePost THEN
            PostEvent(Event_Name)
         ELSE
            TriggerEvent(Event_Name)
         END IF

         //----------
         //  The In_Cascade_Event is variable reset manually.
         //----------

         is_EventControl.In_Cascade_Event = FALSE
      END IF
   END IF
END IF

Finished:

RETURN
end subroutine

public subroutine change_dw_current ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Change_DW_Current
//  Description   : Changes the current DataWindow.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Use the pcd_Active event to make the DataWindow current.
//----------

TriggerEvent("pcd_Active")

RETURN
end subroutine

public subroutine change_dw_focus (uo_dw_main to_dw);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Change_DW_Focus
//  Description   : Changes focus to a new DataWindow.
//
//  Parameters    : UO_DW_MAIN To_DW -
//                        The DataWindow that should recieve
//                        focus.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

//----------
//  Make sure that focus is going to a viable DataWindow.
//----------

IF NOT To_DW.i_InUse THEN
   GOTO Finished
END IF

//----------
//  Since the current DataWindow is being set, let the other
//  events know that it has been done.
//----------

PCCA.PCMGR.i_DW_FocusDone = TRUE

//----------
//  If we do not have focus or processing for an Activate event
//  is not progress, we need make sure the window is raised and
//  that the requested DataWindow becomes the current DataWindow.
//----------

IF GetFocus() <> To_DW OR PCCA.PCMGR.i_ProcessingActivate THEN

   //----------
   //  If the requested DataWindow was not the last DataWindow
   //  that was current on this DataWindow, then we need to clear
   //  the DataWindow that used to be and mark the requested
   //  DataWindow as being the DataWindow that should become
   //  current when this window is activated.
   //----------

   IF NOT To_DW.i_Current THEN

      //----------
      //  Cycle through all of the DataWindows on this window and
      //  clear i_Current.
      //----------

      FOR l_Idx = 1 TO To_DW.i_NumWindowDWs
         To_DW.i_WindowDWs[l_Idx].i_Current = FALSE
      NEXT

      //----------
      //  Mark To_DW as the DataWindow that should become the
      //  current DataWindow when this window is activated.
      //----------

      To_DW.i_Current = TRUE
   END IF

   //----------
   //  If the window that the requested DataWindow is on is
   //  iconizied, then bring it up.
   //----------

   IF To_DW.i_Window.WindowState = Minimized! THEN
      PCCA.Do_Event              = TRUE
      To_DW.i_Window.WindowState = Normal!
      PCCA.Do_Event              = FALSE
   ELSE

      //----------
      //  Cause the window that the requested DataWindow is on to
      //  be raised.  We can't tell if it already is, so we always
      //  have to do this.
      //----------

      To_DW.i_Window.SetFocus()
   END IF

   //----------
   //  If there an activate event is being processed, then we need
   //  need to de-fuse it so that it will not try to change the
   //  current DataWindow after we are done.
   //----------

   IF PCCA.PCMGR.i_ProcessingActivate THEN
      PCCA.PCMGR.Set_DW_Deactivate()
   END IF

   //----------
   //  If the requested DataWindow already has focus, we can't
   //  call SetFocus on it again - the call will be ignored.  In
   //  this case just trigger the GetFocus event directly to get
   //  the code to run.
   //----------

   IF GetFocus() <> To_DW THEN
      To_DW.SetFocus()
   ELSE
      To_DW.TriggerEvent("GetFocus")
   END IF
ELSE

   //----------
   //  The requested DataWindow already had focus, so all we need
   //  to do is to make sure that it is the current DataWindow.
   //----------

   To_DW.TriggerEvent("pcd_Active")
END IF

Finished:

RETURN
end subroutine

public function boolean check_dw_modified (unsignedlong check_option);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Check_DW_Modified
//  Description   : Returns the modified status of the
//                  DataWindow and its children.  PCCA.Error
//                  is always reset to c_Success by this
//                  routine.
//
//  Parameters    : UNSIGNEDLONG Check_Option -
//                        Indicates which DataWindows should be
//                        checked for modifications.  Valid
//                        options are:
//
//                           c_CheckTree -
//                              Check the entire tree that this
//                              DataWindow is a member of.
//
//                           c_CheckChildren -
//                              Check this DataWindow and its
//                              children for changes.
//
//                           c_CheckThisDW -
//                              Check only this DataWindow.
//
//  Return Value  : BOOLEAN -
//                        If TRUE, indicates that the specified
//                        DataWindows contain modifications.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Return

//----------
//  See who we're doing the modifications check for.
//----------

CHOOSE CASE Check_Option
   CASE c_CheckTree

      //----------
      //  Check for changes in the tree that this DataWindow
      //  belongs to.
      //----------

      i_RootDW.is_EventControl.Check_Quiet = TRUE
      i_RootDW.TriggerEvent("pcd_AnyChanges")

      //----------
      //  If PCCA.Error is not c_Success, then there are changes.
      //----------

      l_Return = (PCCA.Error <> c_Success)

   CASE c_CheckChildren

      //----------
      //  Check for changes in this DataWindow and its children.
      //  There may be changes in the DataWindows above us, but
      //  we don't care at this point.  We are only interested
      //  in this DataWindow and its children DataWindows.
      //----------

      is_EventControl.Check_Quiet = TRUE
      TriggerEvent("pcd_AnyChanges")

      //----------
      //  If PCCA.Error is not c_Success, then there are changes.
      //----------

      l_Return = (PCCA.Error <> c_Success)

   CASE ELSE

      //----------
      //  Call Update_Modified() to make sure that the i_Modified
      //  flag is up to date.
      //----------

      Update_Modified()
      l_Return = i_SharePrimary.i_ShareModified
END CHOOSE

//----------
//  Reset PCCA.Error.
//----------

PCCA.Error = c_Success

//----------
//  Return the modified status.
//----------

RETURN l_Return
end function

public function integer column_nbr (string column_name);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Column_Nbr
//  Description   : Returns the column number for the requested
//                  column name.
//
//  Parameters    : STRING Column_Name
//                        The name of the column for which
//                        the column number is requested.
//
//  Return Value  : INTEGER -
//                        The number of the column corresponding
//                        to the column name.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_ColNbr
STRING   l_Describe, l_Result

l_Describe = Column_Name + ".ID"
l_Result   = Describe(l_Describe)
l_ColNbr   = Integer(l_Result)

RETURN l_ColNbr
end function

public function integer dddw_load_code (string dddw_column, string table_name, string column_code, string column_desc, string where_clause);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : DDDW_Load_Code
//  Description   : Load a DataWindow drop down DataWindow
//                  using codes and descriptions from the
//                  database.
//
//  Parameters    : STRING DDDW_Column -
//                        Column name with the DDDW to assign
//                        and load.
//
//                  STRING Table_Name -
//                        Table from where the column with the
//                        code values resides.
//
//                  STRING Column_Code -
//                        Column name with the code code values.
//
//                  STRING Column_Desc -
//                        Column name with the code descriptions.
//
//                  STRING Where_Clause -
//                        WHERE clause statement to restrict the
//                        code values.
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER          l_ColNbr,     l_Idx,      l_Jdx
INTEGER          l_ColPos,     l_Start,    l_Count
LONG             l_Error
STRING           l_SQLString,  l_ErrStr
STRING           l_dwDescribe, l_dwModify, l_Attrib, l_ColStr
DATAWINDOWCHILD  l_DWC

STRING           c_ColAttrib[]  =            &
                    { ".BackGround.Color",   &
                      ".Background.Mode",    &
                      ".Border",             &
                      ".Color",              &
                      ".Font.CharSet",       &
                      ".Font.Face",          &
                      ".Font.Family",        &
                      ".Font.Height",        &
                      ".Font.Italic",        &
                      ".Font.Pitch",         &
                      ".Font.Strikethrough", &
                      ".Font.Underline",     &
                      ".Font.Weight",        &
                      ".Font.Width",         &
                      ".Height",             &
                      ".Width" }

//----------
//  Set the number and name of the DDDW column.
//     Supports "#<Column_Number>" syntax.
//----------

l_dwDescribe = DDDW_Column + ".ID"
l_ColNbr     = Integer(Describe(l_dwDescribe))
l_dwDescribe = "#" + String(l_ColNbr) + ".Name"
l_ColStr     = Describe(l_dwDescribe)

//----------
//  Build the SQL SELECT statement.
//----------

l_SQLString = "SELECT " + Column_Desc + ", " + &
              Column_Code + " FROM " + Table_Name

IF Trim(Where_Clause) <> "" THEN
   l_SQLString = l_SQLString + " WHERE " + Where_Clause
END IF

//----------
//  Get the child DataWindow.  If column is not a DDDW column,
//  then return with error.  Otherwise, we need to set the
//  Select statement in the drop down DataWindow.
//----------

l_Error = GetChild(l_ColStr, l_DWC)

IF l_Error <> 1 THEN
   PCCA.POMGR.MB.fu_MessageBox                      &
      (PCCA.POMGR.MB.c_MBI_DDDWChildFindError, &
       0, PCCA.POMGR.MB.i_MB_Numbers[],  &
       0, PCCA.POMGR.MB.i_MB_Strings[])
   l_Error = -1
   GOTO Finished
END IF

//----------
//  Set the transaction object.
//----------

l_Error = l_DWC.SetTransObject(i_DBCA)
IF l_Error <> 1 THEN
   PCCA.POMGR.MB.fu_MessageBox           &
      (PCCA.POMGR.MB.c_MBI_DDDWTransactionError, &
       0, PCCA.POMGR.MB.i_MB_Numbers[],  &
       0, PCCA.POMGR.MB.i_MB_Strings[])
   l_Error = -1
   GOTO Finished
END IF

//----------
//  Assign the Select statement to the drop down DataWindow.
//----------

l_dwModify = "DataWindow.Table.Select='" + &
             PCCA.MB.fu_QuoteChar(l_SQLString, "'")  + "'"
l_ErrStr   = l_DWC.Modify(l_dwModify)

IF l_ErrStr <> "" THEN
   PCCA.POMGR.MB.fu_MessageBox           &
      (PCCA.POMGR.MB.c_MBI_DDDWModifySQLError, &
       0, PCCA.POMGR.MB.i_MB_Numbers[],  &
       0, PCCA.POMGR.MB.i_MB_Strings[])
   l_Error = -1
   GOTO Finished
END IF

//----------
//  Retrieve the data for the child DataWindow.
//----------

l_Error = l_DWC.Retrieve()
IF l_Error < 0 THEN
   PCCA.POMGR.MB.fu_MessageBox           &
      (PCCA.POMGR.MB.c_MBI_DDDWRetrieveError, &
       0, PCCA.POMGR.MB.i_MB_Numbers[],  &
       0, PCCA.POMGR.MB.i_MB_Strings[])
   l_Error = -1
   GOTO Finished
END IF

//----------
//  Set the attributes of the column to be the same as the
//  parent column.
//----------

l_Jdx = UpperBound(c_ColAttrib[])
FOR l_Idx = 1 TO l_Jdx

   l_dwDescribe = l_ColStr + c_ColAttrib[l_Idx]
   l_Attrib     = Describe(l_dwDescribe)

   IF c_ColAttrib[l_Idx] = ".Font.Face" THEN
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "System"
      END IF
      l_Attrib = "'" + l_Attrib + "'"
   ELSE
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "0"
      END IF
   END IF

   l_dwModify = "#1" + c_ColAttrib[l_Idx] + "=" + l_Attrib
   l_ErrStr   = l_DWC.Modify(l_dwModify)

NEXT

//----------
//  Make sure the border for the child column is off.
//----------

l_dwModify = "#1" + ".Border=0"
l_ErrStr   = l_DWC.Modify(l_dwModify)

//----------
//  Since the DataWindow has been initialized, the column colors
//  may have been modified by PowerClass.
//----------

l_Attrib   = String(i_ColColors[l_ColNbr])
l_dwModify = "#1.Color=" + l_Attrib
l_ErrStr   = l_DWC.Modify(l_dwModify)

l_Attrib   = String(i_ColBColors[l_ColNbr])
l_dwModify = "#1.Background.Color=" + l_Attrib
l_ErrStr   = l_DWC.Modify(l_dwModify)

//----------
//  Indicate that there not any errors.
//----------

l_Error = 0

Finished:

RETURN l_Error
end function

public function integer ddlb_load_code (string ddlb_column, string table_name, string column_code, string column_desc, string where_clause);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : DDLB_Load_Code
//  Description   : Load a DataWindow drop down list box using
//                  codes and descriptions from the database.
//
//  Parameters    : STRING  DDLB_Column -
//                        Column name with the DDLB to load.
//
//                  STRING  Table_Name -
//                        Table from where the column with the
//                        code values resides.
//
//                  STRING  Column_Code -
//                        Column name with the code code
//                        values.
//
//                  STRING  Column_Desc -
//                        Column name with the code
//                        descriptions.
//
//                  STRING  Where_Clause -
//                        WHERE clause statement to restrict
//                        the code values.
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

l_Error =  f_po_LoadDDLB_DW(THIS,        &
                            DDLB_Column, &
                            i_DBCA,      &
                            Table_Name,  &
                            Column_Code, &
                            Column_Desc, &
                            Where_Clause)

RETURN l_Error
end function

public subroutine delete_dw_row (long delete_row, unsignedlong do_any_changes, unsignedlong do_refresh);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Delete_DW_Row
//  Description   : This routine provides a PowerClass
//                  interface to the DELETEROW() function.
//                  This routine will delete the specified
//                  row from the DataWindow and perform the
//                  necessary clean up (e.g.  cursor row
//                  management and refreshing of children
//                  DataWindows).
//
//  Parameters    : LONG  Delete_Row -
//                        The number of the row to be deleted.
//
//                  UNSIGNEDLONG Do_Any_Changes -
//                        Specifies if the children DataWindows
//                        will be checked for changes before the
//                        rows are deleted.  If there are changes
//                        that the user wishes to save, they will
//                        be saved and then the new rows will be
//                        selected.
//
//                           - c_CheckForChanges
//                           - c_IgnoreChanges
//                           - c_SaveChanges
//
//                  UNSIGNEDLONG Do_Refresh -
//                        Specifies if  the children DataWindows
//                        are to refreshed after the delete.
//                        Valid options are:
//
//                           - c_RefreshChildren
//                           - c_NoRefreshChildren
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

LONG  l_TmpRows[]

//----------
//  Use the Delete_DW_Rows[] function to delete the specified row.
//----------

l_TmpRows[1] = Delete_Row
Delete_DW_Rows(1, l_TmpRows[], Do_Any_Changes, Do_Refresh)

RETURN
end subroutine

public subroutine delete_dw_rows (long num_to_delete, long delete_rows[], unsignedlong do_any_changes, unsignedlong do_refresh);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Delete_DW_Rows
//  Description   : This routine provides a PowerClass
//                  interface to the DELETEROW() function.
//                  This routine will delete the specified
//                  set of rows from the DataWindow and
//                  perform the necessary clean up (e.g.
//                  cursor row management and refreshing
//                  of children DataWindows).
//
//  Parameters    : LONG Num_To_Delete -
//                        The number of rows that have been
//                        passed in the Delete_Rows[] array.
//
//                  LONG Delete_Rows[] -
//                        An array containing the rows numbers
//                        corresponding to the rows which are
//                        to be deleted.
//
//                        *** THIS MUST BE A SORTED ARRAY ***
//
//                  UNSIGNEDLONG Do_Any_Changes -
//                        Specifies if the children DataWindows
//                        will be checked for changes before the
//                        rows are deleted.  If there are changes
//                        that the user wishes to save, they will
//                        be saved and then the new rows will be
//                        selected.
//
//                           - c_CheckForChanges
//                           - c_IgnoreChanges
//                           - c_SaveChanges
//
//                  UNSIGNEDLONG Do_Refresh -
//                        Specifies if  the children DataWindows
//                        are to refreshed after the delete.
//                        Valid options are:
//
//                           - c_RefreshChildren
//                           - c_NoRefreshChildren
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_Modified, l_NewRows,      l_RefreshDone
UNSIGNEDLONG  l_SaveOpenChild
LONG          l_RowCount, l_DeleteRowNbr, l_Delete[], l_Idx
DWITEMSTATUS  l_ItemStatus

//----------
//  Make sure there are some rows to be deleted.
//----------

IF Num_To_Delete < 1 THEN
   GOTO Finished
END IF

//----------
//  Check for changes.
//----------

PCCA.Error = Check_Modifications(Do_Any_Changes)

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  Turn RFC, validation, VScroll, and redraw off while we delete
//  the rows.
//----------

Share_State(c_SharePushRedraw,   c_BufferDraw)
Share_State(c_SharePushRFC,      c_IgnoreRFC)
Share_State(c_SharePushValidate, c_IgnoreVal)
Share_State(c_SharePushVScroll,  c_IgnoreVScroll)

//----------
//  Found out how many rows there are currently in the DataWindow.
//----------

l_RowCount = RowCount()

//----------
//  Delete the rows.
//----------

FOR l_Idx = Num_To_Delete TO 1 STEP -1

   //----------
   //  Make sure we were told to delete a valid row.
   //----------

   l_DeleteRowNbr  = Delete_Rows[l_Idx]
   l_Delete[l_Idx] = 0

   IF l_DeleteRowNbr >  0          THEN
   IF l_DeleteRowNbr <= l_RowCount THEN

      l_Delete[l_Idx] = l_DeleteRowNbr

      //----------
      //  Since a row will be deleted, we need to update
      //  PowerClass's row variables (e.g. i_CursorRow).
      //----------

      Share_Remove(l_DeleteRowNbr)

      //----------
      //  If this is not a New! row, then indicate that the
      //  DataWindow has been modified.
      //----------

      l_ItemStatus = GetItemStatus(l_DeleteRowNbr, 0, Primary!)
      IF l_ItemStatus <> New! AND l_ItemStatus <> NewModified! THEN
         l_Modified = TRUE
      END IF

      //----------
      //  Finally, we can delete the row.
      //----------

      DeleteRow(l_DeleteRowNbr)
   END IF
   END IF
NEXT

//----------
//  If the delete modified the DataWindow, then update the
//  i_Modified flag.
//----------

IF i_Modified <> l_Modified THEN
   Share_Flags(c_ShareModified, TRUE)
END IF

//----------
//  See how many rows are left in the DataWindow.
//----------

l_RowCount = RowCount()

//----------
//  If i_DoSetKey was set, there were New! rows in the DataWindow.
//  However, after deleting rows, there may not be.  Assume that
//  all New! rows have been deleted and if we find one, we won't
//  modify i_DoSetKey.
//----------

IF i_SharePrimary.i_ShareDoSetKey THEN
   FOR l_Idx = 1 TO l_RowCount

      //----------
      //  If this is a New! row, then indicate that that DataWindow
      //  contains New! rows.
      //----------

      l_ItemStatus = GetItemStatus(l_Idx, 0, Primary!)
      IF l_ItemStatus = New! OR l_ItemStatus = NewModified! THEN
         l_NewRows = TRUE
         EXIT
      END IF
   NEXT

   IF NOT l_NewRows THEN
      Share_Flags(c_ShareAllDoSetKey, FALSE)
   END IF
END IF

//----------
//  Update the DataWindow after the deletes are done.
//----------

Share_Remove(0)

//----------
//  If we do have a valid cursor row, trigger RowFocusChanged! to
//  scroll the DataWindow to that row.
//----------

l_RefreshDone = FALSE
IF i_CursorRow > 0 THEN

   //----------
   //  Make sure that RFC processing is on.
   //----------

   i_IgnoreRFC      = (NOT c_ProcessRFC)
   i_IgnoreRFCCount = i_IgnoreRFCCount + 1
   i_IgnoreRFCStack[i_IgnoreRFCCount] = c_ProcessRFC

   //----------
   //  Set up the instance variables to tell the RFC event how
   //  and where we want to move.  If the developer said not to
   //  refresh, make sure that RowFocusChanged! will not do
   //  it by using the c_RFC_NoSelect option.
   //----------

   IF Do_Refresh = c_RefreshChildren OR &
      (i_NumChildren = 0 AND i_NumInstance = 0) THEN
      IF i_RFCType = c_RFC_RowFocusChanged THEN
         l_RefreshDone = TRUE
      END IF
      i_RFCEvent = c_RFC_RowFocusChanged
   ELSE
     i_RFCEvent = c_RFC_NoSelect
   END IF

   //----------
   //  We don't want any children DataWindows getting popped open,
   //  so use Set_Open_Child()
   //----------

   l_SaveOpenChild   = i_OpenChildOption
   i_OpenChildOption = c_OpenChildPrevent

   //----------
   //  Move to the cursor row.
   //----------

   i_MoveRow = i_CursorRow
   TriggerEvent(RowFocusChanged!)

   //----------
   //  Restore the open child option to its original state.
   //----------

   i_OpenChildOption = l_SaveOpenChild

   //----------
   //  Restore RFC processing to its original state.
   //----------

   IF i_IgnoreRFCCount > 1 THEN
      i_IgnoreRFCCount = i_IgnoreRFCCount - 1
      i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
   ELSE
      i_IgnoreRFCCount = 0
      i_IgnoreRFC      = FALSE
   END IF
END IF

//----------
//  If the developer specified to load the currently selected rows
//  into the DataWindows and there have not been errors, trigger
//  pcd_Refresh to do it.
//----------

IF NOT l_RefreshDone              THEN
IF Do_Refresh = c_RefreshChildren THEN
   is_EventControl.Refresh_Cmd = c_RefreshSame
   TriggerEvent("pcd_Refresh")
END IF
END IF

//----------
//  Restore the processing states of RFC, validation, VScroll.
//----------

Share_State(c_SharePopRedraw,   c_StateUnused)
Share_State(c_SharePopRFC,      c_StateUnused)
Share_State(c_SharePopValidate, c_StateUnused)
Share_State(c_SharePopVScroll,  c_StateUnused)

Finished:

RETURN
end subroutine

public function integer display_dw_val_error (unsignedlong mb_id, string error_message);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Display_DW_Val_Error
//  Description   : Handles the display of validation errors.
//                  The error message can be specified by the
//                  developer in the Error_Message parameter.
//                  See the parameters section below for
//                  instructions on how to do this.  If an
//                  Error_Message string is not provided, then
//                  PowerClass will attempt to get the validation
//                  message from the DataWindow.  Failing that,
//                  it will provide a default PowerClass
//                  validation message specific to the error.
//
//  Parameters    : UNSIGNEDINTEGER MB_ID -
//                        A code that specifies the error message
//                        to displayed by the message box object.
//                        If the developer is going to provide
//                        the validation message in the
//                        Error_Message parameter, then the
//                        value PCCA.MB.c_MBI_DW_OKDevValError
//                        should be passed if the message box
//                        is to use the Ok! button.
//                        PCCA.MB.c_MBI_DW_YesNoDevValError should
//                        passed if the message box is to use the
//                        YesNo! buttons.
//
//                  STRING Error_Message -
//                        An error message provided by the
//                        developer.  Pass MB_ID as either
//                        PCCA.MB.c_MBI_DW_OKDevValError or
//                        PCCA.MB.c_MBI_DW_YesNoDevValError.
//
//  Return Value  : INTEGER -
//                        The button pressed by the user.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_InSingleQuote,   l_InDoubleQuote,   l_Quoted
BOOLEAN       l_NextSingleQuote, l_NextDoubleQuote, l_TermToken
BOOLEAN       l_AddToken,        l_Processed,       l_GetInfo
BOOLEAN       l_SaveRedraw
INTEGER       l_LenErrMsg,       l_LenColErr,       l_Idx
INTEGER       l_Answer,          l_SaveErr,         l_ColNbr
UNSIGNEDLONG  l_MBICode,         l_SaveOpenChild
LONG          l_RowNbr
STRING        l_ValidationMsg,   l_Token,           l_Char

//----------
//  Initialize the return value.
//----------

l_Answer = 0

//----------
//  Save PCCA.Error while we display the error.
//----------

l_SaveErr  = PCCA.Error
PCCA.Error = c_Success

//----------
//  Turn validation processing off in case we have to move to a
//  the row or column that had the error.
//----------

i_IgnoreVal      = (NOT c_IgnoreVal)
i_IgnoreValCount = i_IgnoreValCount + 1
i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

//----------
//  Move to the row that had the error.  We only want to move if
//  we have to, as RFC will foul up PowerBuilder's internal
//  validation/modified state.
//----------

l_RowNbr = GetRow()
IF i_RowNbr = 0 THEN
   i_RowNbr = l_RowNbr
END IF

IF i_RowNbr <> l_RowNbr OR i_RowNbr <> i_CursorRow THEN
   l_GetInfo = TRUE

   //----------
   //  We have to move.  Force RowFocusChanged! processing on so
   //  that the move will actually happen.
   //----------

   i_IgnoreRFC      = (NOT c_ProcessRFC)
   i_IgnoreRFCCount = i_IgnoreRFCCount + 1
   i_IgnoreRFCStack[i_IgnoreRFCCount] = c_ProcessRFC

   //----------
   //  We don't want any children DataWindows getting popped
   //  open, so use Set_Open_Child()
   //----------

   l_SaveOpenChild   = i_OpenChildOption
   i_OpenChildOption = c_OpenChildPrevent

   //----------
   //  If there are not children, we generate a
   //  c_RFC_RowFocusChanged type of event.  If
   //  there are children, we must use c_RFC_NoSelect
   //  because we do not want rows loaded into the
   //  children.
   //----------

   IF i_NumChildren = 0 AND i_NumInstance = 0 THEN
      i_RFCEvent = c_RFC_RowFocusChanged
   ELSE
      i_RFCEvent = c_RFC_NoSelect
   END IF

   //----------
   //  i_MoveRow tells the RowFocusChanged! event what row we
   //  want to move to.
   //----------

   l_SaveRedraw      = PCCA.MDI.i_Redraw
   PCCA.MDI.i_Redraw = FALSE
   i_MoveRow         = i_RowNbr
   TriggerEvent(RowFocusChanged!)
   PCCA.MDI.i_Redraw = l_SaveRedraw

   //----------
   //  If we are in RFC, we don't want RFC to move back to the
   //  old row
   //----------

   IF i_InRFC > 0 THEN
      i_OldCursorRow  = i_CursorRow
      i_OldClickedRow = i_CursorRow
   END IF

   //----------
   //  Restore the open child option to its original state.
   //----------

   i_OpenChildOption = l_SaveOpenChild

   //----------
   //  Restore the prior state of RFC processing.
   //----------

   IF i_IgnoreRFCCount > 1 THEN
      i_IgnoreRFCCount = i_IgnoreRFCCount - 1
      i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
   ELSE
      i_IgnoreRFCCount = 0
      i_IgnoreRFC      = FALSE
   END IF
END IF

//----------
//  Move to the column that had the error.  We only want to
//  move if we have to, as SetColumn() will foul up
//  PowerBuilder's internal validation/modified state.
//----------

IF i_ColNbr > 0 THEN
   l_ColNbr = GetColumn()
   IF i_ColNbr <> l_ColNbr THEN
      l_GetInfo = TRUE
      IF i_ColNbr >  0 THEN
         SetColumn(i_ColNbr)
      ELSE
         i_ColNbr = l_ColNbr
      END IF
   END IF
END IF

//----------
//  Make sure that we have the current validation info.
//----------

IF l_GetInfo THEN
   Get_Val_Info(i_ColNbr)
END IF

//----------
//  Make sure that the cursor row has been updated.
//----------

IF i_CursorRow <> i_RowNbr THEN
   i_CursorRow = i_RowNbr
   IF i_RFCType     = c_RFC_RowFocusChanged THEN
   IF i_NumChildren = 0                     THEN
      i_ShowRow = i_CursorRow
      SelectRow(0, FALSE)
      SelectRow(i_ShowRow, TRUE)
   END IF
   END IF
   Find_Instance(i_ShowRow)
END IF

//----------
//  Restore the prior state of validation processing.
//----------

IF i_IgnoreValCount > 1 THEN
   i_IgnoreValCount = i_IgnoreValCount - 1
   i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
ELSE
   i_IgnoreValCount = 0
   i_IgnoreVal      = FALSE
END IF

//----------
//  Change focus to ourselves since we have an error.  Note
//  that Change_DW_Focus() will raise the window if necessary.
//----------

Change_DW_Focus(THIS)

//----------
//  Make sure that it is Ok to display the validation error.
//----------

IF NOT PCCA.No_Validation_Error_Display THEN

   //----------
   //  Figure out the validation error message to display.  The
   //  priority of error messages is:
   //     1) If a error message was passed as a parameter, use
   //        it as the developer specified.
   //     2) If there is an error in i_ColValMsg, then process
   //        and use it.
   //     3) Use the fu_MessageBox() routine with the message ID
   //        that was passed.
   //----------

   l_LenErrMsg = Len(Trim(Error_Message))
   l_LenColErr = Len(Trim(i_ColValMsg))
   IF l_LenErrMsg = 0 AND l_LenColErr > 0 THEN

      l_ValidationMsg   = ""
      l_Token           = ""

      l_InSingleQuote   = FALSE
      l_InDoubleQuote   = FALSE
      l_NextSingleQuote = FALSE
      l_NextDoubleQuote = FALSE
      l_Quoted          = FALSE
      l_TermToken       = FALSE
      l_AddToken        = FALSE

      //----------
      //  We have a validation error message from the DataWindow.
      //  We need to parse and process it.
      //----------

      FOR l_Idx = 1 TO l_LenColErr + 1

         //----------
         //  Indicate that we have not processed this character.
         //----------

         l_Processed = FALSE

         IF l_Idx > l_LenColErr THEN
            l_Char = ""
         ELSE
            l_Char = Mid(i_ColValMsg, l_Idx, 1)
         END IF

         //----------
         //  If this is not a quoted character and it is a tilde
         //  (~), then the next character in the string is to be
         //  quoted.
         //----------

         IF NOT l_Quoted AND l_Char = "~~" THEN
            l_Processed = TRUE
            l_Quoted    = TRUE
         END IF

         //----------
         //  If the character has not been processed and it is not
         //  quoted, see if it is a single quote (') or double
         //  quote (").
         //----------

         IF NOT l_Processed AND NOT l_Quoted THEN
            IF l_Char = "'" THEN

               //----------
               //  The character is a single quote.  If we are not
               //  inside of a double quoted string, then this
               //  single quote begins or ends a single quoted
               //  string.  In either case, the token we were
               //  building is complete.  Not that we do switch
               //  the state of quoting until the end of this loop.
               //  The state of quoting needs to stay the same
               //  until after the token has been added to the
               //  error message buffer.
               //----------

               IF NOT l_InDoubleQuote THEN
                  l_NextSingleQuote = (NOT l_InSingleQuote)
                  l_Processed       = TRUE
                  l_AddToken        = TRUE
               END IF
            END IF
            IF l_Char = "~"" THEN

               //----------
               //  The character is a double quote.  If we are not
               //  inside of a single quoted string, then this
               //  double quote begins or ends a double quoted
               //  string.  In either case, the token we were
               //  building is complete.  Not that we do switch
               //  the state of quoting until the end of this loop.
               //  The state of quoting needs to stay the same
               //  until after the token has been added to the
               //  error message buffer.
               //----------

               IF NOT l_InSingleQuote THEN
                  l_NextDoubleQuote = (NOT l_InDoubleQuote)
                  l_Processed       = TRUE
                  l_AddToken        = TRUE
               END IF
            END IF
         END IF

         //----------
         //  If the characters still has not been processed, keep
         //  trying.
         //----------

         IF NOT l_Processed THEN

            //----------
            //  If the character has been quoted, check to see if
            //  it matches one of the PowerBuilder special
            //  characters.  If it does, then the current token we
            //  are building is complete.  If the character is not
            //----------

            IF l_Quoted THEN
               l_TermToken = (l_Char = "n"  OR &
                              l_Char = "t"  OR &
                              l_Char = "v"  OR &
                              l_Char = "r"  OR &
                              l_Char = "f"  OR &
                              l_Char = "b"  OR &
                              l_Char = "'"  OR &
                              l_Char = "~"")
            ELSE

               //----------
               //  The character is not quoted.  Look for special
               //  characters which terminate tokens.
               //----------

               l_TermToken = (l_Char = " "  OR &
                              l_Char = "+"  OR &
                              l_Char = ""  OR &
                              l_Char = ""  OR &
                              l_Char = "~"" OR &
                              l_Char = "~n" OR &
                              l_Char = "~t" OR &
                              l_Char = "~r")
            END IF

            //----------
            //  Since we have found a charater that says to start
            //  a new token, indicate that the current token is
            //  done.
            //----------

            IF l_TermToken THEN
               l_AddToken = TRUE
            END IF
         END IF

         //----------
         //  If the current token is done or if we are at the end
         //  of the validation message, add the token to the
         //  processed validation error message.
         //----------

         IF l_AddToken OR l_Idx > l_LenColErr THEN
            l_AddToken = FALSE

            //----------
            //  If there is not a token, then we have nothing to
            //  add.
            //----------

            IF Len(l_Token) > 0 THEN

               //----------
               //  While we are inside of a quoted string, the
               //  only thing will substitue in for the developer
               //  is the column name.
               //----------

               IF l_InSingleQuote OR l_InDoubleQuote THEN
                  CHOOSE CASE Upper(l_Token)
                  CASE "@COLUMNNAME", "@COLUMN", "@COL"
                        l_ValidationMsg = l_ValidationMsg + &
                                          i_ColName
                     CASE ELSE
                        l_ValidationMsg = l_ValidationMsg + &
                                          l_Token
                  END CHOOSE
               ELSE

                  //----------
                  //  If we are not inside of quoted string, then
                  //  look for tokens that we can substitute for
                  //  the developer.
                  //----------

                  CHOOSE CASE Upper(l_Token)
                     CASE "GETTEXT()"
                        l_ValidationMsg = l_ValidationMsg + &
                                          i_ColText
                     CASE "GETROW()"
                        l_ValidationMsg = l_ValidationMsg + &
                                          String(GetRow())
                     CASE "ROWCOUNT()"
                        l_ValidationMsg = l_ValidationMsg + &
                                          String(RowCount())
                     CASE ELSE
                  END CHOOSE
               END IF

               //----------
               //  Reset the token.
               //----------

               l_Token = ""
            END IF
         END IF

         //----------
         //  If the character still has not been processed, keep
         //  trying.
         //----------

         IF NOT l_Processed THEN
            IF l_Quoted THEN

               //----------
               //  We have a quoted character since there was a
               //  tilde (~) preceeding this character (e.g.
               //  "~n").  Look for PowerBuilder special
               //  characters and add them if they are found.
               //  Otherwise, just add the raw character.
               //----------

               l_Quoted = FALSE

               CHOOSE CASE l_Char
                  CASE "n"
                     l_Token = l_Token + "~n"
                  CASE "t"
                     l_Token = l_Token + "~t"
                  CASE "v"
                     l_Token = l_Token + "~v"
                  CASE "r"
                     l_Token = l_Token + "~r"
                  CASE "f"
                     l_Token = l_Token + "~f"
                  CASE "b"
                     l_Token = l_Token + "~b"
                  CASE ELSE
                     l_Token = l_Token + l_Char
               END CHOOSE
            ELSE

               //----------
               //  We just found a normal character.  Tack it on
               //  to the current token.
               //----------

               l_Token = l_Token + l_Char
            END IF
         END IF

         //----------
         //  l_TermToken is TRUE when the character that just got
         //  added was one of the PowerBuilder special characters.
         //  Because it is special, it is a complete token in and
         //  of itself.  Indicate that the token is complete and
         //  should be added to the error message buffer.
         //----------

         IF l_TermToken THEN
            l_TermToken = FALSE
            l_AddToken  = TRUE
         END IF

         //----------
         //  The state of the quoted string may have been changed
         //  at the begining of this loop.  Set the state so that
         //  it is ready for the next time through the loop.
         //----------

         l_InSingleQuote = l_NextSingleQuote
         l_InDoubleQuote = l_NextDoubleQuote
      NEXT

      //----------
      //  Set up the parameters for fu_MessageBox().
      //----------

      l_MBICode            = PCCA.MB.c_MBI_DW_DWValError
      PCCA.MB.i_MB_Numbers[1] = i_ColFormatNumber
      PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Display_DW_Val_Error"
      PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
      PCCA.MB.i_MB_Strings[3] = i_ClassName
      PCCA.MB.i_MB_Strings[4] = i_Window.Title
      PCCA.MB.i_MB_Strings[5] = DataObject
      PCCA.MB.i_MB_Strings[6] = i_ColText
      PCCA.MB.i_MB_Strings[7] = i_ColName
      PCCA.MB.i_MB_Strings[8] = l_ValidationMsg
      PCCA.MB.i_MB_Strings[9] = i_ColFormatString
   ELSE

      //----------
      //  There is not a validation error message in the
      //  DataWindow.  Use the Message ID to display the message.
      //----------

      l_MBICode            = MB_ID
      PCCA.MB.i_MB_Numbers[1] = i_ColFormatNumber
      PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Display_DW_Val_Error"
      PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
      PCCA.MB.i_MB_Strings[3] = i_ClassName
      PCCA.MB.i_MB_Strings[4] = i_Window.Title
      PCCA.MB.i_MB_Strings[5] = DataObject
      PCCA.MB.i_MB_Strings[6] = i_ColText
      PCCA.MB.i_MB_Strings[7] = i_ColName
      PCCA.MB.i_MB_Strings[8] = Error_Message
      PCCA.MB.i_MB_Strings[9] = i_ColFormatString
   END IF

   //----------
   //  Make sure that redraw is on when we display the message
   //  box containing the validation error.  If it were not, the
   //  message box would "eat" the DataWindow if the user were to
   //  move the message box.
   //----------

   SetRedraw(c_DrawNow)
   i_RedrawCount = i_RedrawCount + 1
   i_RedrawStack[i_RedrawCount] = c_DrawNow

   //----------
   //  Popup the message box with the validaton error.
   //----------

   l_Answer = PCCA.MB.fu_MessageBox(l_MBICode,              &
                                 1, PCCA.MB.i_MB_Numbers[], &
                                 9, PCCA.MB.i_MB_Strings[])

   //----------
   //  Restore the prior state of redraw.
   //----------

   IF i_RedrawCount > 1 THEN
      i_RedrawCount = i_RedrawCount - 1
      SetRedraw(i_RedrawStack[i_RedrawCount])
   ELSE
      i_RedrawCount = 0
      SetRedraw(TRUE)
   END IF
END IF

//----------
//  Restore PCCA.Error.
//----------

PCCA.Error = l_SaveErr

RETURN l_Answer
end function

public function boolean dw_accepttext (unsignedlong error_option);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : DW_AcceptText
//  Description   : Provides the developer with an method for
//                  calling AcceptText().  This routine accepts
//                  the user's input into the column via
//                  AcceptText() and does necessary clean up.
//                  PCCA.Error is always reset to c_Success by
//                  this routine.
//
//
//  Parameters    : UNSIGNEDLONG Error_Option -
//                        Specifies if the validation should
//                        be performed and errors reported.
//                        Valid options are:
//
//                           - c_AcceptProcessErrors
//                           - c_AcceptIgnoreErrors
//
//  Return Value  : BOOLEAN -
//                        TRUE indicates that the user's input
//                        was accepted.  FALSE indicates a
//                        validation error.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Success
INTEGER  l_SaveIVA

//----------
//  If the developer said to ignore errors, then turn validation
//  processing off.
//----------

IF Error_Option = c_AcceptIgnoreErrors THEN
   i_IgnoreVal      = (NOT c_IgnoreVal)
   i_IgnoreValCount = i_IgnoreValCount + 1
   i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

   l_SaveIVA         = i_IgnoreValAction
   i_IgnoreValAction = c_AcceptAndMove
END IF

//----------
//  Call AcceptText() to accept the user's input into the column.
//  Check for errors on the AcceptText().  If the AcceptText()
//  fails, then we have a validation error.  It will already have
//  been reported to the user because the ItemChanged! and/or
//  ItemError! events get triggered during the AcceptText().
//----------

l_Success = (AcceptText() = 1)

//----------
//  ItemChanged! and/or ItemError! may have been triggered
//  by the AcceptText() call.  Reset i_ItemValidated to
//  indicate that validation is ready to go again.
//----------

i_ItemValidated = FALSE

//----------
//  If the developer said to ignore errors, then restore
//  validation processing.
//----------

IF Error_Option = c_AcceptIgnoreErrors THEN
   l_Success         = TRUE
   i_IgnoreValAction = l_SaveIVA
   IF i_IgnoreValCount > 1 THEN
      i_IgnoreValCount = i_IgnoreValCount - 1
      i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
   ELSE
      i_IgnoreValCount = 0
      i_IgnoreVal      = FALSE
   END IF
END IF

//----------
//  Make sure that PCCA.Error is reset.
//----------

PCCA.Error = c_Success

RETURN l_Success
end function

public subroutine dw_allowcontrol (unsignedlong which_capabilities, unsignedlong enable_or_disable);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : dw_AllowControl
//  Description   : Allows the developer to  which menus
//                  and buttons are controlled by the DataWindow.
//
//  Parameters    : UNSIGNEDLONG  Which_Capabilities -
//                        Indicates which capabilites are to be
//                        changed.  Valid options are:
//
//                           - c_AllowControlAll
//                           - c_AllowControlAccept
//                           - c_AllowControlCancel
//                           - c_AllowControlClose
//                           - c_AllowControlDelete
//                           - c_AllowControlFirst
//                           - c_AllowControlFilter
//                           - c_AllowControlInsert
//                           - c_AllowControlLast
//                           - c_AllowControlMain
//                           - c_AllowControlModify
//                           - c_AllowControlNew
//                           - c_AllowControlNext
//                           - c_AllowControlPrevious
//                           - c_AllowControlPrint
//                           - c_AllowControlQuery
//                           - c_AllowControlResetQuery
//                           - c_AllowControlRetrieve
//                           - c_AllowControlSave
//                           - c_AllowControlSaveRowsAs
//                           - c_AllowControlSearch
//                           - c_AllowControlView
//
//                  UNSIGNEDLONG  Enable_Or_Disable -
//                        Specifies if the indicated capabilities
//                        are to be enabled or disabled.  Valid
//                        options are:
//
//                           - c_AllowControlEnable
//                           - c_AllowControlDisable
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Enabled

l_Enabled = (Enable_Or_Disable = c_AllowControlEnable)

IF Which_Capabilities = c_AllowControlAll THEN
   i_AllowControlAccept     = l_Enabled
   i_AllowControlCancel     = l_Enabled
   i_AllowControlClose      = l_Enabled
   i_AllowControlDelete     = l_Enabled
   i_AllowControlFirst      = l_Enabled
   i_AllowControlFilter     = l_Enabled
   i_AllowControlInsert     = l_Enabled
   i_AllowControlLast       = l_Enabled
   i_AllowControlMain       = l_Enabled
   i_AllowControlModify     = l_Enabled
   i_AllowControlNew        = l_Enabled
   i_AllowControlNext       = l_Enabled
   i_AllowControlPrevious   = l_Enabled
   i_AllowControlPrint      = l_Enabled
   i_AllowControlQuery      = l_Enabled
   i_AllowControlResetQuery = l_Enabled
   i_AllowControlRetrieve   = l_Enabled
   i_AllowControlSave       = l_Enabled
   i_AllowControlSaveRowsAs = l_Enabled
   i_AllowControlSearch     = l_Enabled
   i_AllowControlView       = l_Enabled
ELSE
   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlAccept) > 0 THEN
      i_AllowControlAccept = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlCancel) > 0 THEN
      i_AllowControlCancel = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlClose) > 0 THEN
      i_AllowControlClose = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlDelete) > 0 THEN
      i_AllowControlDelete = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlFirst) > 0 THEN
      i_AllowControlFirst = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlFilter) > 0 THEN
      i_AllowControlFilter = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlInsert) > 0 THEN
      i_AllowControlInsert = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlLast) > 0 THEN
      i_AllowControlLast = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlMain) > 0 THEN
      i_AllowControlMain = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlModify) > 0 THEN
      i_AllowControlModify = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlNew) > 0 THEN
      i_AllowControlNew = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlNext) > 0 THEN
      i_AllowControlNext = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlPrevious) > 0 THEN
      i_AllowControlPrevious = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlPrint) > 0 THEN
      i_AllowControlPrint = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlQuery) > 0 THEN
      i_AllowControlQuery = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlResetQuery) > 0 THEN
      i_AllowControlResetQuery = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlRetrieve) > 0 THEN
      i_AllowControlRetrieve = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlSave) > 0 THEN
      i_AllowControlSave = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlSaveRowsAs) > 0 THEN
      i_AllowControlSaveRowsAs = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlSearch) > 0 THEN
      i_AllowControlSearch = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_AllowControlView) > 0 THEN
      i_AllowControlView = l_Enabled
   END IF
END IF

//----------
//  If the developer enabled capabilities, run pcd_SetControl to
//  allow them to take effect.
//----------

is_EventControl.Control_Mode = c_ControlActiveDW
TriggerEvent("pcd_SetControl")

RETURN
end subroutine

public subroutine dw_capability (unsignedlong which_capabilities, unsignedlong enable_or_disable);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : dw_Capability
//  Description   : Allows the developer to change the
//                  capabilties that the user has on the
//                  DataWindow (e.g. delete capability).
//
//  Parameters    : UNSIGNEDLONG  Which_Capabilities -
//                        Indicates which capabilites are to be
//                        changed.  Valid options are:
//
//                           - c_CapabilityAll
//                           - c_CapabilityNew
//                           - c_CapabilityModify
//                           - c_CapabilityDelete
//                           - c_CapabilityQuery
//
//                  UNSIGNEDLONG  Enable_Or_Disable -
//                        Specifies if the indicated capabilities
//                        are to be enabled or disabled.  Valid
//                        options are:
//
//                           - c_CapabilityEnable
//                           - c_CapabilityDisable
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Enabled

l_Enabled = (Enable_Or_Disable = c_CapabilityEnable)

IF Which_Capabilities = c_CapabilityAll THEN
   i_AllowNew    = l_Enabled
   i_AllowModify = l_Enabled
   i_AllowDelete = l_Enabled
   i_AllowQuery  = l_Enabled
ELSE
   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_CapabilityNew) > 0 THEN
      i_AllowNew = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_CapabilityModify) > 0 THEN
      i_AllowModify = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_CapabilityDelete) > 0 THEN
      i_AllowDelete = l_Enabled
   END IF

   IF PCCA.EXT.fu_BitANDMask(Which_Capabilities, &
                             c_CapabilityQuery) > 0 THEN
      i_AllowQuery = l_Enabled
   END IF
END IF

//----------
//  Run pcd_SetControl to allow capabilties to take effect.
//----------

is_EventControl.Control_Mode = c_ControlActiveDW
TriggerEvent("pcd_SetControl")

RETURN
end subroutine

public function long dw_rowcount ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : DW_RowCount
//  Description   : Duplicates the DataWindow ROWCOUNT() function
//                  except that it takes into account the empty
//                  row that the PowerClass may have added to
//                  prevent freeform columns and text from
//                  disappearing.
//
//  Parameters    : (None)
//  Return Value  : LONG -
//                        The number of rows contained in the
//                        DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

LONG  l_Return

IF i_IsEmpty THEN
   l_Return = 0
ELSE
   l_Return = RowCount()
END IF

RETURN l_Return
end function

public function integer em_load_code (string em_column, string table_name, string column_code, string column_desc, string where_clause);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : EM_Load_Code
//  Description   : Load a datawindow edit mask spin box using codes
//                  and descriptions from the database.
//
//  Parameters    : STRING  EM_Column -
//                        Column name with the edit mask spin box
//                        to load.
//
//                  STRING  Table_Name -
//                        Table from where the column with the
//                        code values resides.
//
//                  STRING  Column_Code -
//                        Column name with the code values.
//
//                  STRING  Column_Desc -
//                        Column name with the code descriptions.
//
//                  STRING  Where_Clause -
//                        WHERE clause statement to restrict
//                        the code values.
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

l_Error = f_po_LoadEM_DW(THIS,        &
                         EM_Column,   &
                         i_DBCA,      &
                         Table_Name,  &
                         Column_Code, &
                         Column_Desc, &
                         Where_Clause)

RETURN l_Error
end function

public function integer filter_dw (unsignedlong do_any_changes, unsignedlong reselect_rows, unsignedlong refresh_mode);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Filter_DW
//  Description   : Executes a filter on the DataWindow.
//
//  Parameters    : UNSIGNEDLONG Do_Any_Changes -
//                        Specifies if the children DataWindows
//                        will be checked for changes before the
//                        rows are loaded.  If there are changes
//                        that the user wishes to save, they will
//                        be saved and then the new rows will be
//                        selected.
//
//                           - c_CheckForChanges
//                           - c_IgnoreChanges
//                           - c_SaveChanges
//
//                  UNSIGNEDLONG Reselect_Rows -
//                        Tells this routine whether to attempt
//                        to reselect the currently selected rows
//                        after the retrieve happens.  Valid
//                        options are:
//
//                           - c_ReselectRows
//                           - c_NoReselectRows
//
//                  UNSIGNEDLONG Refresh_Mode -
//                        Specifies what mode the children
//                        DataWindows should be placed in
//                        after the filter.  Valid options
//                        are:
//
//                           - c_RefreshView
//                           - c_RefreshModify
//                           - c_RefreshNew
//                           - c_RefreshSame
//                           - c_RefreshRFC
//
//  Return Value  : INTEGER -
//                        Error return - c_Success or c_Fatal.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Error
UNSIGNEDLONG  l_xReselectMode, l_xRefreshMode

//----------
//  Use the pcd_Filter event to put the DataWindow into filter
//  mode.
//----------

PCCA.Error = Check_Modifications(Do_Any_Changes)

IF PCCA.Error = c_Success THEN
   l_xReselectMode = i_xReselectMode
   l_xRefreshMode  = i_xRefreshMode
   i_xReselectMode = Reselect_Rows
   i_xRefreshMode  = Refresh_Mode
   TriggerEvent("pcd_Filter")
   i_xReselectMode = l_xReselectMode
   i_xRefreshMode  = l_xRefreshMode
END IF

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public function long get_retrieve_rows (ref long selected_rows[]);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Get_Retrieve_Rows
//  Description   : Builds an array containing the rows that
//                  are currently selected in the DataWindow
//                  and can be retrieved (i.e. non-New!)
//
//  Parameters    : ref LONG Selected_Rows[] -
//                         An array that will be filled with
//                         the row numbers that are selected
//                         in this DataWindow.  All New! rows
//                         created by the user will be excluded
//                         from this array so that it may be
//                         used for keys by RETRIEVE() in the
//                         children DataWindow(s).
//
//  Return Value  : LONG -
//                         The number of rows returned in the
//                         Selected_Rows[] array.  Note that
//                         zero (0) rows is a valid return.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

LONG          l_NumRows
LONG          l_NextRow
DWITEMSTATUS  l_ItemStatus

//----------
//  Assume there are no rows selected.
//----------

l_NumRows = 0

//----------
//  If i_ShowRow is 0, then there really are no rows selected.
//  Just return.
//----------

IF i_ShowRow = 0 THEN
   GOTO Finished
END IF

//----------
//  See if we are using PowerBuilder SelectRow() function.  If
//  we are, we can ask PowerBuilder for the selected rows.
//----------

IF i_HighlightSelected THEN

   //----------
   //  Find the first selected row.
   //----------

   l_NextRow = GetSelectedRow(0)

   //----------
   //  As long as we find selected rows, keep looping for other
   //  selected rows.
   //----------

   DO WHILE (l_NextRow > 0)

      //----------
      //  Look at the status of this row.  If it is New! or
      //  NewModified! this row should not be used to select
      //  rows from the database because the database knows
      //  nothing about this record.  The biggest problem with
      //  using New! rows is that their keys may not have been
      //  set, since they are usually set during the save process.
      //  PowerBuilder gets big-time indigestion when it tries
      //  to use a NULL key.
      //----------

      l_ItemStatus = GetItemStatus(l_NextRow, 0, Primary!)
      IF l_ItemStatus <> New! AND l_ItemStatus <> NewModified! THEN
         l_NumRows                = l_NumRows + 1
         Selected_Rows[l_NumRows] = l_NextRow
      END IF

      //----------
      //  Find the next selected row.
      //----------

      l_NextRow = GetSelectedRow(l_NextRow)
   LOOP
ELSE

   //----------
   //  If we are not using PowerBuilder's SelectRow() function,
   //  then there can only be one selected row, which is
   //  identified by i_ShowRow.  Free-form style DataWindows
   //  are an example of a DataWindow which does not use
   //  PowerBuilder's SelectRow() function.
   //----------

   l_ItemStatus = GetItemStatus(i_ShowRow, 0, Primary!)
   IF l_ItemStatus <> New! AND l_ItemStatus <> NewModified! THEN
      l_NumRows                = l_NumRows + 1
      Selected_Rows[l_NumRows] = i_ShowRow
   END IF
END IF

Finished:

RETURN l_NumRows
end function

public function long get_selected_rows (ref long selected_rows[]);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Get_Selected_Rows
//  Description   : Builds an array containing the rows that
//                  are currently selected in the DataWindow.
//
//  Parameters    : ref LONG Selected_Rows[] -
//                         An array that will be filled with
//                         the row numbers that are selected
//                         in this DataWindow.
//
//  Return Value  : LONG -
//                         The number of rows returned in the
//                         Selected_Rows[] array.  Note that
//                         zero (0) rows is a valid return.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

LONG  l_NumRows
LONG  l_NextRow

//----------
//  Assume there are no rows selected.
//----------

l_NumRows = 0

//----------
//  If i_ShowRow is 0, then there really are no rows selected.
//  Just return.
//----------

IF i_ShowRow = 0 THEN
   GOTO Finished
END IF

//----------
//  See if we are using PowerBuilder SelectRow() function.  If
//  we are, we can ask PowerBuilder for the selected rows.
//----------

IF i_HighlightSelected THEN

   //----------
   //  Find the first selected row.
   //----------

   l_NextRow = GetSelectedRow(0)

   //----------
   //  As long as we find selected rows, keep looping for other
   //  selected rows.
   //----------

   DO WHILE (l_NextRow > 0)
      l_NumRows                = l_NumRows + 1
      Selected_Rows[l_NumRows] = l_NextRow

      //----------
      //  Find the next selected row.
      //----------

      l_NextRow = GetSelectedRow(l_NextRow)
   LOOP
ELSE

   //----------
   //  If we are not using PowerBuilder's SelectRow() function,
   //  then there can only be one selected row, which is
   //  identified by i_ShowRow.  Free-form style DataWindows
   //  are an example of a DataWindow which does not use
   //  PowerBuilder's SelectRow() function.
   //----------

   l_NumRows                = l_NumRows + 1
   Selected_Rows[l_NumRows] = i_ShowRow
END IF

Finished:

RETURN l_NumRows
end function

public function integer insert_dw_rows (long start_row_nbr, long num_to_insert);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Insert_DW_Rows
//  Description   : Provides an PowerClass interface to the
//                  PowerBuilder INSERTROW() function.
//
//  Parameters    : LONG Start_Row_Nbr -
//                        The row before which to insert new
//                        rows.
//
//                  LONG Num_To_Insert -
//                        The number of rows to be inserted.
//
//  Return Value  : INTEGER -
//                       Error return - c_Success or c_Fatal
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error, l_Idx

//----------
//  Turn refresh off for this DataWindow while the new happens.
//----------

SetRedraw(c_BufferDraw)
i_RedrawCount = i_RedrawCount + 1
i_RedrawStack[i_RedrawCount] = c_BufferDraw

//----------
//  Depending on the starting row number, we will either
//  insert the new records or append the new records.
//----------

IF Start_Row_Nbr = 0 THEN
   PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Append, c_Show)
ELSE
   PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Insert, c_Show)
END IF
PCCA.MDI.fu_SetRedraw(PCCA.MDI.c_BufferDraw)

//----------
//  Use pcd_New to insert the first row.  pcd_New will check for
//  errors, perform validation, get rid of empty rows, etc.
//----------

i_CursorRow = Start_Row_Nbr
TriggerEvent("pcd_New", c_NewInsert, c_DoNew)

//----------
//  If pcd_New was successful, we can continue adding new rows.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  See if we still have rows to do.
//----------

IF Num_To_Insert > 0 THEN

   //----------
   //  If this DataWindow allows "NEW" mode, we can just use
   //  InsertRow() to keep adding rows since the first pcd_New
   //  did all of the overhead checking.  If this DataWindow
   //  does not allow "NEW", we will continue to use pcd_New
   //  to insert rows into the hierarchy.
   //----------

   IF i_AllowNew THEN

      //----------
      //  If there already New! rows, make sure that it is Ok
      //  to insert another one.
      //----------

      IF i_DoSetKey AND i_OnlyOneNewRow THEN
         PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Insert_DW_Rows()"
         PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
         PCCA.MB.i_MB_Strings[3] = i_ClassName
         PCCA.MB.i_MB_Strings[4] = i_Window.Title
         PCCA.MB.i_MB_Strings[5] = DataObject
         PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_OnlyOneNew, &
                            0, PCCA.MB.i_MB_Numbers[],   &
                            5, PCCA.MB.i_MB_Strings[])
         PCCA.Error = c_Fatal
         GOTO Finished
      END IF

      i_ProcessMode = TRUE
      FOR l_Idx = 2 TO Num_To_Insert - 1
         IF PCCA.Error <> c_Success THEN
            GOTO Finished
         END IF
         InsertRow(Start_Row_Nbr)
         i_CursorRow = Start_Row_Nbr
         TriggerEvent("pcd_New")
      NEXT
      i_ProcessMode = FALSE

      //----------
      //  Use pcd_New to do the last row so that the cursor
      //  gets position correctly.
      //----------

      IF Num_To_Insert > 1 THEN
         i_CursorRow = Start_Row_Nbr
         TriggerEvent("pcd_New", c_NewInsert, c_DoNew)
      END IF
   ELSE

      //----------
      //  This DataWindow does not allow "NEW" mode.  Trigger
      //  pcd_New to handle the "NEW" mode for this DataWindow's
      //  children.
      //----------

      FOR l_Idx = 2 TO Num_To_Insert
         IF PCCA.Error <> c_Success THEN
            GOTO Finished
         END IF
         TriggerEvent("pcd_New")
      NEXT
   END IF
END IF

Finished:

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

//----------
//  Restore refresh mode.
//----------

SetRedraw(c_DrawNow)
i_RedrawCount = i_RedrawCount + 1
i_RedrawStack[i_RedrawCount] = c_DrawNow

//----------
//  Remove the "NEW" mode prompt.
//----------

PCCA.MDI.fu_SetRedraw(PCCA.MDI.c_DrawNow)
PCCA.MDI.fu_Pop()

RETURN l_Error
end function

public subroutine modify_dw_attrib (integer column_nbr, integer new_tab_order, integer new_border, long new_color, long new_bcolor);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Modify_DW_Attrib
//  Description   : Provides a method for the developer to
//                  alter DataWindow tabs and colors.
//
//  Parameters    : INTEGER  Column_Nbr -
//                        The column to be modified.
//
//                  INTEGER  New_Tab_Order -
//                        The new tab specification for the
//                        column.  Use c_SameTab to retain the
//                        current tab order for this column.
//
//                  INTEGER  New_Border -
//                        The new border for the column.
//                        column.  Use c_SameBorder to retain
//                        the current border for this column.
//
//                  LONG New_Color -
//                        The new foreground color (i.e. text
//                        color) for the column.  Use c_SameColor
//                        to retain the current color for this
//                        column.
//
//                  LONG New_BColor -
//                        The new background color for the column.
//                        Use c_SameBColor to retain the current
//                        color for this column.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Batch
INTEGER  l_Idx
STRING   l_Col

IF Column_Nbr < 1 OR Column_Nbr > i_NumColumns THEN
   GOTO Finished
END IF

IF NOT i_BatchAttrib THEN
   l_Batch = TRUE
   Batch_Attrib(c_BatchAttrib)
END IF

CHOOSE CASE New_Tab_Order
   CASE c_SameTab

   CASE c_VisibleCol
      i_ColVisible[Column_Nbr] = TRUE
      IF i_BatchString <> "" THEN
         i_BatchString = i_BatchString + "~t"
      END IF
      l_Col         = "#" + String(Column_Nbr)
      i_BatchString = i_BatchString + l_Col + ".Visible=1"

   CASE c_InvisibleCol
      i_ColVisible[Column_Nbr] = FALSE
      IF i_BatchString <> "" THEN
         i_BatchString = i_BatchString + "~t"
      END IF
      l_Col         = "#" + String(Column_Nbr)
      i_BatchString = i_BatchString + l_Col + &
                      ".TabSequence=0"      + &
                      "~t"          + l_Col + &
                      ".Visible=0"

   CASE ELSE
      IF i_TabOrder[Column_Nbr] >= 0 AND &
         New_Tab_Order          >= 0 THEN
         i_TabOrder[Column_Nbr] = New_Tab_Order

         IF New_Tab_Order = 0 THEN
            IF i_BatchString <> "" THEN
               i_BatchString = i_BatchString + "~t"
            END IF
            l_Col         = "#" + String(Column_Nbr)
            i_BatchString = i_BatchString + l_Col + &
                            ".TabSequence=0"
         END IF
      END IF
END CHOOSE

New_Tab_Order = i_TabOrder[Column_Nbr]

IF New_Border <> c_SameBorder THEN
   IF New_Border < c_DW_BorderFirst OR &
      New_Border > c_DW_BorderLast  THEN
      New_Border = c_DW_BorderNone
   END IF

   i_ColBorders[Column_Nbr] = &
      Integer(PCCA.MDI.c_DW_Borders [New_Border])
END IF

New_Border = i_ColBorders[Column_Nbr]

IF i_ViewModeBorderIdx = c_DW_BorderUndefined THEN
   IF i_BatchString <> "" THEN
      i_BatchString = i_BatchString + "~t"
   END IF
   l_Col         = "#" + String(Column_Nbr)
   i_BatchString = i_BatchString + l_Col + &
                   ".Border="    +         &
                    String(New_Border)
ELSE
   IF New_Tab_Order < 1 THEN
      IF i_BatchString <> "" THEN
         i_BatchString = i_BatchString + "~t"
      END IF
      l_Col         = "#" + String(Column_Nbr)
      i_BatchString = i_BatchString + l_Col + &
                      ".Border="    +         &
                       String(New_Border)
   ELSE
      IF i_CurrentMode =  c_ModeView     THEN
      IF i_DWState     <> c_DWStateQuery THEN
         IF i_BatchString <> "" THEN
            i_BatchString = i_BatchString + "~t"
         END IF
         l_Col         = "#" + String(Column_Nbr)
         i_BatchString = i_BatchString + l_Col + &
                         ".Border="    + &
                         PCCA.MDI.c_DW_Borders[i_ViewModeBorderIdx]
      END IF
      END IF
   END IF
END IF

IF New_Color <> c_SameColor THEN
   i_ColColors[Column_Nbr] = New_Color

   //----------
   //  Need to update the new color into the object colors.
   //----------

   Get_Val_Info(Column_Nbr)
   FOR l_Idx = 1 TO i_NumObjects
      IF i_xColumnNames[Column_Nbr] = i_ObjectNames[l_Idx] THEN
         i_ObjectColors[l_Idx] = New_Color
         EXIT
      END IF
   NEXT
ELSE
   New_Color = i_ColColors[Column_Nbr]
END IF

//----------

IF New_BColor <> c_SameBColor THEN
   i_ColBColors[Column_Nbr] = New_BColor
ELSE
   New_BColor               = i_ColBColors[Column_Nbr]
END IF

IF i_ViewModeColorIdx = c_ColorUndefined THEN
   IF i_BatchString <> "" THEN
      i_BatchString = i_BatchString + "~t"
   END IF
   l_Col         = "#" + String(Column_Nbr)
   i_BatchString = i_BatchString + l_Col + &
                   ".BackGround.Color="  + String(New_BColor)
ELSE
   IF New_Tab_Order < 1 THEN
      IF i_BatchString <> "" THEN
         i_BatchString = i_BatchString + "~t"
      END IF
      l_Col         = "#" + String(Column_Nbr)
      i_BatchString = i_BatchString + l_Col + &
                      ".BackGround.Color="  + String(New_BColor)
   ELSE
      IF i_CurrentMode =  c_ModeView     THEN
      IF i_DWState     <> c_DWStateQuery THEN
         IF i_BatchString <> "" THEN
            i_BatchString = i_BatchString + "~t"
         END IF
         l_Col         = "#" + String(Column_Nbr)
         i_BatchString = i_BatchString + l_Col + &
                         ".BackGround.Color="  + &
                         PCCA.MDI.c_RGBStrings[i_ViewModeColorIdx]
      END IF
      END IF
   END IF
END IF

IF l_Batch THEN
   Batch_Attrib(c_ProcessAttrib)
END IF

Finished:

RETURN
end subroutine

public subroutine pop_dw_redraw ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Pop_DW_Redraw
//  Description   : Pops the last redraw state from
//                  the stack and sets it.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//----------
//  If i_RedrawCount is greater than one, then we need to
//  restore the previous state of redrawing.  Otherwise, just
//  force redraw to be TRUE.
//----------

IF i_RedrawCount > 1 THEN
   i_RedrawCount = i_RedrawCount - 1
   SetRedraw(i_RedrawStack[i_RedrawCount])
ELSE
   i_RedrawCount = 0
   SetRedraw(TRUE)
END IF

RETURN
end subroutine

public subroutine pop_dw_rfc ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Pop_DW_RFC
//  Description   : Pops the last Ignore-RFC state from
//                  the stack and sets it.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//----------
//  If i_IgnoreRFCCount is greater than one, then we need to
//  restore the previous state of RFC processing.  Otherwise,
//  just set the flag turn on RFC processing.
//----------

IF i_IgnoreRFCCount > 1 THEN
   i_IgnoreRFCCount = i_IgnoreRFCCount - 1
   i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
ELSE
   i_IgnoreRFCCount = 0
   i_IgnoreRFC      = FALSE
END IF

RETURN
end subroutine

public subroutine pop_dw_validate ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Pop_DW_Validate
//  Description   : Pops the last Ignore-Validation state from
//                  the stack and sets it.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//----------
//  If i_IgnoreValCount is greater than one, then we need to
//  restore the previous state of validation processing.
//  Otherwise, just set the flag to turn on validation processing.
//----------

IF i_IgnoreValCount > 1 THEN
   i_IgnoreValCount = i_IgnoreValCount - 1
   i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
ELSE
   i_IgnoreValCount = 0
   i_IgnoreVal      = FALSE
END IF

RETURN
end subroutine

public subroutine pop_dw_vscroll ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Pop_DW_VScroll
//  Description   : Pops the last Ignore-VScroll state from
//                  the stack and sets it.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//----------
//  If i_IgnoreVSCount is greater than one, then we need to
//  restore the previous state of VScroll processing.
//  Otherwise, just set the flag so that VScroll processing
//  is done.
//----------

IF i_IgnoreVSCount > 1 THEN
   i_IgnoreVSCount = i_IgnoreVSCount - 1
   i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
ELSE
   i_IgnoreVSCount = 0
   i_IgnoreVScroll = FALSE
END IF

//----------
//  If processing of vertical scrolling is on, then find the
//  top row so we know where we are.
//----------

IF NOT i_IgnoreVScroll THEN
   i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
END IF

RETURN
end subroutine

public function boolean postevent (string event_name);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : PostEvent
//  Description   : Overrides the DataWindow's POSTEVENT().
//                  Redirects the posted event to PCCA.PCMGR for
//                  processing.  Many of the DataWindow events
//                  require the S_EVENTCONTROL structure to
//                  inform them of the function that is being
//                  requested.  For example, the pcd_Refresh
//                  event needs to be told how to refresh the
//                  DataWindows (e.g. change them to "VIEW" mode).
//                  If another DataWindow event that uses the
//                  event control struture runs before the posted
//                  event, the event control structure will have
//                  changed by the time the posted event executes.
//                  By allowing PCCA.PCMGR to manage the posted
//                  events, the event control structure can be
//                  set up correctly before each posted DataWindow
//                  event begins execution.
//
//  Parameters    : STRING  Event_Name -
//                        The event to be posted.
//
//  Return Value  : BOOLEAN -
//                        The return value of the POSTEVENT().
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Return

l_Return = PCCA.PCMGR.Post_DW_Event(THIS, Event_Name)

RETURN l_Return
end function

public subroutine push_dw_redraw (boolean on_or_off);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Push_DW_Redraw
//  Description   : Pushs Redraw requests onto a stack and
//                  sets the requested state.
//
//  Parameters    : BOOLEAN On_Or_Off -
//                        Turn DataWindow redraw on or off.
//                         Valid options are:
//
//                            - c_DrawNow
//                            - c_BufferDraw
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Set redraw to the specified state and push it onto the
//  stack.
//----------

SetRedraw(On_Or_Off)
i_RedrawCount = i_RedrawCount + 1
i_RedrawStack[i_RedrawCount] = On_Or_Off

RETURN
end subroutine

public subroutine push_dw_rfc (boolean on_or_off);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Push_DW_RFC
//  Description   : Pushs Ignore-RFC requests onto a stack and
//                  sets the requested state.
//
//  Parameters    : BOOLEAN On_Or_Off -
//                        Turn RFC processing on or off.  Valid
//                        options are:
//
//                           - c_ProcessRFC
//                           - c_IgnoreRFC
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Set RFC processing to the specified state and push it onto the
//  stack.
//----------

i_IgnoreRFC      = (NOT On_Or_Off)
i_IgnoreRFCCount = i_IgnoreRFCCount + 1
i_IgnoreRFCStack[i_IgnoreRFCCount] = On_Or_Off

RETURN
end subroutine

public subroutine push_dw_validate (boolean on_or_off);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Push_DW_Validate
//  Description   : Pushs Ignore-Validation requests onto a stack
//                  and sets the requested state.
//
//  Parameters    : BOOLEAN On_Or_Off -
//                        Turn validation processing on or off.
//                        Valid options are:
//
//                           - c_ProcessVal
//                           - c_IgnoreVal
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Set validation processing to the specified state and push it
//  onto the stack.
//----------

i_IgnoreVal      = (NOT On_Or_Off)
i_IgnoreValCount = i_IgnoreValCount + 1
i_IgnoreValStack[i_IgnoreValCount] = On_Or_Off

RETURN
end subroutine

public subroutine push_dw_vscroll (boolean on_or_off);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Push_DW_VScroll
//  Description   : Pushs Ignore-VScroll requests onto a stack
//                  and sets the requested state.
//
//  Parameters    : BOOLEAN On_Or_Off -
//                        Turn VScroll processing on or off.
//                        Valid options are:
//
//                           - c_ProcessVScroll
//                           - c_IgnoreVScroll
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Set Vscroll processing to the specified state and push it
//  onto the stack.
//----------

i_IgnoreVScroll = (NOT On_Or_Off)
i_IgnoreVSCount = i_IgnoreVSCount + 1
i_IgnoreVSStack[i_IgnoreVSCount] = On_Or_Off

RETURN
end subroutine

public subroutine reset_dev_control ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Reset_Dev_Control
//  Description   : The S_EVENTCONTROL structure is used with
//                  many DataWindow events to specify control
//                  or function information (e.g. the pcd_Refresh
//                  event is told how to refresh the DataWindows
//                  via the event control strucutre).  The event
//                  control structure is very similar in concept
//                  to parameters to a normal subroutine.
//                  PowerClass provides a structure variable
//                  called is_DevControl for the developer so that
//                  the developer may also "pass parameters" to
//                  developer coded events.  The developer is
//                  responsible for setting up this structure and
//                  calling this routine to reset its contents
//                  after the event has fetched the required
//                  information from it.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

is_DevControl.Option = 0

RETURN
end subroutine

public subroutine reset_dw_modified (unsignedlong reset_children);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Reset_DW_Modified
//  Description   : Resets the DataWindow's and, optionally,
//                  its children DataWindows' modified flags.
//
//  Parameters    : UNSIGNEDLONG Reset_Children -
//                       Indicates if only this DataWindow's
//                       modified flags are to be reset or
//                       all of its children, too.  Valid
//                       options are:
//
//                          - c_ResetChildren
//                          - c_NoResetChildren
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

//----------
//  See if we're doing it for everybody or just this DataWindow.
//----------

IF Reset_Children = c_ResetChildren THEN

   //----------
   //  Normally, you would use Clear_Cancel instead of Clear_Ok,
   //  but Clear_Cancel marks DataWindows for retrieval.
   //----------

   is_EventControl.Clear_Ok = TRUE
   TriggerEvent("pcd_AnyChanges")
ELSE

   //----------
   //  Only clear the modified flags for this DataWindow.
   //----------

   Update_Modified()

   //----------
   //  We need to clear the modified flags to indicate that
   //  there are not any changes in this DataWindow.  Use
   //  RESETUPDATE() to tell PowerBuilder that there are not
   //  any changes.
   //----------

   IF i_SharePrimary.i_ShareModified THEN
      i_SharePrimary.ResetUpdate()
   END IF

   //----------
   //  Clear the i_Modified and i_DoSetKey flags.
   //----------

   Share_Flags(c_ShareAllModified + c_ShareAllDoSetKey, FALSE)
END IF

RETURN
end subroutine

public subroutine retrieve_dw (unsignedlong only_if_modified, unsignedlong reselect_rows, unsignedlong refresh_mode);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Retrieve_DW
//  Description   : This routine provides the developer with
//                  a method to force a DataWindow to retrieve
//                  itself and refresh its children DataWindows.
//
//  Parameters    : UNSIGNEDLONG Only_If_Modified -
//                        Tells this routine to only retrieve
//                        DataWindows which contain modifications.
//                        In essence, the developer is wiping out
//                        all of the user's changes.  Valid options
//                        are:
//
//                           - c_OnlyIfModified
//                           - c_RetrieveAllDWs
//
//                  UNSIGNEDLONG Reselect_Rows -
//                        Tells this routine whether to attempt
//                        to reselect the currently selected rows
//                        after the retrieve happens.  Valid
//                        options are:
//
//                           - c_ReselectRows
//                           - c_NoReselectRows
//
//                  UNSIGNEDLONG Refresh_Mode -
//                        Specifies what mode this DataWindow
//                        and its children DataWindows should
//                        be placed in.  Valid options are:
//
//                           - c_RefreshView
//                           - c_RefreshModify
//                           - c_RefreshNew
//                           - c_RefreshSame
//                           - c_RefreshRFC
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  This may take a little while...
//----------

SetPointer(HourGlass!)

//----------
//  Clear the modified flags in this DataWindow and its children.
//----------

is_EventControl.Clear_Cancel = TRUE
TriggerEvent("pcd_AnyChanges")

//----------
//  See if the developer only wants the modified DataWindows
//  retrieved or the whole hierarchy.
//----------

IF Only_If_Modified = c_OnlyIfModified THEN

   //----------
   //  If we're only retrieving DataWindows that have been
   //  modified, then we do want the current rows reselected.
   //----------

   Reselect_Rows = c_ReselectRows
ELSE

   //----------
   //  Setting i_RetrieveMySelf will cause this DataWindow and
   //  all of its children DataWindows to be retrieved.
   //----------

   i_RetrieveMySelf = TRUE
END IF

//----------
//  If rows are not to be re-selected, then unselect all rows
//  before pcd_Refresh (and the retrieve) happens.  If there
//  are not any rows selected, then pcd_Refresh will not be able
//  to re-select any rows.
//----------

IF Reselect_Rows = c_NoReselectRows THEN

   //----------
   //  Clear any rows in i_ReselectKeys[].
   //----------

   i_NumReselectRows = 0

   SelectRow(0, FALSE)
   i_NumSelected = 0
   i_CursorRow   = 0
   i_ShowRow     = 0
   i_AnchorRow   = 0
   i_CurInstance = 0
END IF

//----------
//  Cause the retrieve to happen.
//----------

is_EventControl.Refresh_Cmd = Refresh_Mode
TriggerEvent("pcd_Refresh")

RETURN
end subroutine

public function integer save_dw (unsignedlong prompt_user, unsignedlong do_refresh);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Save_DW
//  Description   : Saves the DataWindow.
//
//  Parameters    : UNSIGNEDLONG Prompt_User -
//                        Indicates if the developer wants the
//                        user to be prompted for changes, or
//                        if the changes will automatically saved.
//                        Valid Options are:
//
//                           - c_NoPromptUser
//                           - c_PromptUser
//
//                  UNSIGNEDLONG Do_Refresh -
//                        Specifies if  the children DataWindows
//                        are to refreshed after the save.
//                        Valid options are:
//
//                           - c_DoRefresh
//                           - c_NoRefresh
//
//  Return Value  : INTEGER -
//                        Error return - c_Success or c_Fatal.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Error
UNSIGNEDLONG  l_RefreshCmd

//----------
//  Reset PCCA.Error to indicate that there are no errors.
//----------

PCCA.Error = c_Success

//----------
//  Call Load_Rows() to load selections that have been made
//  by the user, but not yet loaded into the children
//  DataWindows.
//----------

IF PCCA.PCMGR.i_DW_NeedRetrieve THEN
   IF Load_Rows(PCCA.Null_Object, PCCA.Null_Object) <> &
      c_Success THEN
      PCCA.Error = c_Fatal
      GOTO Finished
   END IF
END IF

//----------
//  Set l_RefreshCmd to indicate that refreshing is not needed.
//  If there are changes, l_RefreshCmd will tell us how to
//  refresh after the changes have been processed (e.g. if the
//  user aborts changes, then the modified DataWindows need to
//  have rows re-loaded from the database).
//----------

l_RefreshCmd = c_RefreshUndefined

//----------
//  Check for changes and ask the user if necessary.  If there
//  are changes to be saved, Check_Save() will save them.  Save
//  the error return from Check_Save().  It will tells us if
//  there were validation errors, the user canceled, or there
//  was a problem saving.
//----------

IF Prompt_User = c_PromptUser THEN
   i_RootDW.Check_Save(l_RefreshCmd)

   //----------
   //  If l_RefreshCmd is not undefined, then DataWindows need
   //  to be refreshed, starting at the root-level DataWindow.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
   IF Do_Refresh    = c_DoRefresh        THEN

      //----------
      //  Set up the Event Control structure to tell pcd_Refresh
      //  how the refresh needs to be done.
      //----------

      i_RootDW.is_EventControl.Refresh_Cmd = l_RefreshCmd
      i_RootDW.TriggerEvent("pcd_Refresh")
   END IF
   END IF
ELSE

   //----------
   //  Use the pcd_Save event to save the DataWindow.
   //----------

   IF Do_Refresh = c_NoRefresh THEN
       i_RootDW.is_EventControl.No_Refresh_After_Save = TRUE
   END IF
   i_RootDW.TriggerEvent("pcd_Save")
END IF

Finished:

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public function integer search_dw (unsignedlong do_any_changes, unsignedlong reselect_rows, unsignedlong refresh_mode);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Search_DW
//  Description   : Executes a search on the DataWindow.
//
//  Parameters    : UNSIGNEDLONG Do_Any_Changes -
//                        Specifies if the children DataWindows
//                        will be checked for changes before the
//                        rows are loaded.  If there are changes
//                        that the user wishes to save, they will
//                        be saved and then the new rows will be
//                        selected.
//
//                           - c_CheckForChanges
//                           - c_IgnoreChanges
//                           - c_SaveChanges
//
//                  UNSIGNEDLONG Reselect_Rows -
//                        Tells this routine whether to attempt
//                        to reselect the currently selected rows
//                        after the retrieve happens.  Valid
//                        options are:
//
//                           - c_ReselectRows
//                           - c_NoReselectRows
//
//                  UNSIGNEDLONG Refresh_Mode -
//                        Specifies what mode the children
//                        DataWindows should be placed in
//                        after the search.  Valid options
//                        are:
//
//                           - c_RefreshView
//                           - c_RefreshModify
//                           - c_RefreshNew
//                           - c_RefreshSame
//                           - c_RefreshRFC
//
//  Return Value  : INTEGER -
//                        Error return - c_Success or c_Fatal.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Error
UNSIGNEDLONG  l_xReselectMode, l_xRefreshMode

//----------
//  Use the pcd_Search event to put the DataWindow into search
//  mode.
//----------

PCCA.Error = Check_Modifications(Do_Any_Changes)

IF PCCA.Error = c_Success THEN
   l_xReselectMode = i_xReselectMode
   l_xRefreshMode  = i_xRefreshMode
   i_xReselectMode = Reselect_Rows
   i_xRefreshMode  = Refresh_Mode
   TriggerEvent("pcd_Search")
   i_xReselectMode = l_xReselectMode
   i_xRefreshMode  = l_xRefreshMode
END IF

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public subroutine set_dw_emenu (menu edit_menu);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_DW_EMenu
//  Description   : Initializes the EDIT menu for uo_DW_Main.
//
//  Parameters    : MENU Edit_Menu -
//                        The menu that is to be used
//                        as the EDIT menu by PowerClass.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx
MENU     l_TmpMenu

//----------
//  We indicate that this routine has been called by setting
//  i_EditMenuInit to TRUE.  If this routine is called by the
//  developer, this flag will be TRUE and PowerClass will know
//  not to not call it again.
//----------

i_EditMenuInit = TRUE

//----------
//  Set the our menu to the specified menu.
//----------

i_EditMenu = Edit_Menu

//----------
//  See if the EDIT Menu is valid.  If it is, save the individual
//  menu items into instance variables for speedy access.
//----------

i_EditMenuIsValid       = IsValid(i_EditMenu)
i_EditMenuNewNum        = 0
i_EditMenuViewNum       = 0
i_EditMenuModifyNum     = 0
i_EditMenuInsertNum     = 0
i_EditMenuDeleteNum     = 0
i_EditMenuModeSepNum    = 0
i_EditMenuFirstNum      = 0
i_EditMenuPrevNum       = 0
i_EditMenuNextNum       = 0
i_EditMenuLastNum       = 0
i_EditMenuSearchSepNum  = 0
i_EditMenuQueryNum      = 0
i_EditMenuSearchNum     = 0
i_EditMenuFilterNum     = 0
i_EditMenuSaveSepNum    = 0
i_EditMenuSaveNum       = 0
i_EditMenuSaveRowsAsNum = 0
i_EditMenuPrintSepNum   = 0
i_EditMenuPrintNum      = 0

IF i_EditMenuIsValid THEN
   l_Jdx = UpperBound(i_EditMenu.Item[])
   FOR l_Idx = 1 TO l_Jdx
      l_TmpMenu = i_EditMenu.Item[l_Idx]
      CHOOSE CASE l_TmpMenu.Text
         CASE PCCA.MDI.c_MDI_MenuLabelNew
            i_EditMenuNewNum        = l_Idx
            i_EditMenuNew           = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelView
            i_EditMenuViewNum       = l_Idx
            i_EditMenuView          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelModify
            i_EditMenuModifyNum     = l_Idx
            i_EditMenuModify        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelInsert
            i_EditMenuInsertNum     = l_Idx
            i_EditMenuInsert        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelDelete
            i_EditMenuDeleteNum     = l_Idx
            i_EditMenuDelete        = l_TmpMenu
            IF l_Idx < l_Jdx THEN
               IF i_EditMenu.Item[l_Idx + 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuModeSepNum = l_Idx + 1
                  i_EditMenuModeSep    = &
                     i_EditMenu.Item[l_Idx + 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelFirst
            i_EditMenuFirstNum      = l_Idx
            i_EditMenuFirst         = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelPrev
            i_EditMenuPrevNum       = l_Idx
            i_EditMenuPrev          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelNext
            i_EditMenuNextNum       = l_Idx
            i_EditMenuNext          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelLast
            i_EditMenuLastNum       = l_Idx
            i_EditMenuLast          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelQuery
            i_EditMenuQueryNum      = l_Idx
            i_EditMenuQuery         = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_EditMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuSearchSepNum = l_Idx - 1
                  i_EditMenuSearchSep    = &
                     i_EditMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelSearch
            i_EditMenuSearchNum     = l_Idx
            i_EditMenuSearch        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelFilter
            i_EditMenuFilterNum     = l_Idx
            i_EditMenuFilter        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelSave
            i_EditMenuSaveNum       = l_Idx
            i_EditMenuSave          = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_EditMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuSaveSepNum = l_Idx - 1
                  i_EditMenuSaveSep    = &
                     i_EditMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelSaveRowsAs
            i_EditMenuSaveRowsAsNum = l_Idx
            i_EditMenuSaveRowsAs    = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelPrint
            i_EditMenuPrintNum      = l_Idx
            i_EditMenuPrint         = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_EditMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuPrintSepNum = l_Idx - 1
                  i_EditMenuPrintSep    = &
                     i_EditMenu.Item[l_Idx - 1]
               END IF
            END IF
      END CHOOSE
   NEXT
END IF

IF i_EditMenuIsPopup THEN
   PCCA.DLL.fu_SetDWPMenu (THIS)
END IF

RETURN
end subroutine

public subroutine set_dw_fmenu (menu file_menu);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_DW_FMenu
//  Description   : Initializes the FILE menu for uo_DW_Main.
//
//  Parameters    : MENU File_Menu -
//                        The menu that is to be used
//                        as the FILE menu by PowerClass.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx
MENU     l_TmpMenu

//----------
//  We indicate that this routine has been called by setting
//  i_FileMenuInit to TRUE.  If this routine is called by the
//  developer, this flag will be TRUE and PowerClass will know
//  not to not call it again.
//----------

i_FileMenuInit = TRUE

//----------
//  Set the our menu to the specified menu.
//----------

i_FileMenu = File_Menu

//----------
//  See if the FILE Menu is valid.  If it is, save the individual
//  menu items into instance variables for speedy access.
//----------

i_FileMenuIsValid       = IsValid(i_FileMenu)
i_FileMenuNewNum        = 0
i_FileMenuOpenNum       = 0
i_FileMenuCloseNum      = 0
i_FileMenuFileSepNum    = 0
i_FileMenuCancelNum     = 0
i_FileMenuAcceptNum     = 0
i_FileMenuSaveSepNum    = 0
i_FileMenuSaveNum       = 0
i_FileMenuSaveRowsAsNum = 0
i_FileMenuPrintSepNum   = 0
i_FileMenuPrintNum      = 0
i_FileMenuPrintSetupNum = 0
i_FileMenuExitSepNum    = 0
i_FileMenuExitNum       = 0

IF i_FileMenuIsValid THEN
   l_Jdx = UpperBound(i_FileMenu.Item[])
   FOR l_Idx = 1 TO l_Jdx
      l_TmpMenu = i_FileMenu.Item[l_Idx]
      CHOOSE CASE l_TmpMenu.Text
         CASE PCCA.MDI.c_MDI_MenuLabelNew
            i_FileMenuNewNum        = l_Idx
            i_FileMenuNew           = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelOpen
            i_FileMenuOpenNum       = l_Idx
            i_FileMenuOpen          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelClose
            i_FileMenuCloseNum      = l_Idx
            i_FileMenuClose         = l_TmpMenu
            IF l_Idx < l_Jdx THEN
               IF i_FileMenu.Item[l_Idx + 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuFileSepNum = l_Idx + 1
                  i_FileMenuFileSep    = &
                     i_FileMenu.Item[l_Idx + 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelCancel
            i_FileMenuCancelNum     = l_Idx
            i_FileMenuCancel        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelAccept
            i_FileMenuAcceptNum     = l_Idx
            i_FileMenuAccept        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelSave
            i_FileMenuSaveNum       = l_Idx
            i_FileMenuSave          = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_FileMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuSaveSepNum = l_Idx - 1
                  i_FileMenuSaveSep    = &
                     i_FileMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelSaveRowsAs
            i_FileMenuSaveRowsAsNum = l_Idx
            i_FileMenuSaveRowsAs    = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelPrint
            i_FileMenuPrintNum      = l_Idx
            i_FileMenuPrint         = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_FileMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuPrintSepNum = l_Idx - 1
                  i_FileMenuPrintSep    = &
                     i_FileMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelPrintSetup
            i_FileMenuPrintSetupNum = l_Idx
            i_FileMenuPrintSetup    = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelExit
            i_FileMenuExitNum       = l_Idx
            i_FileMenuExit          = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_FileMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuExitSepNum = l_Idx - 1
                  i_FileMenuExitSep    = &
                     i_FileMenu.Item[l_Idx - 1]
               END IF
            END IF
      END CHOOSE
   NEXT
END IF

RETURN
end subroutine

public function integer set_dw_insert ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Set_DW_Insert
//  Description   : Changes the DataWindow to "NEW" mode.
//
//  Parameters    : (None)
//
//  Return Value  : INTEGER -
//                       Error return - c_Success or c_Fatal
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//----------
//  Use the pcd_Insert event to put the DataWindow into "NEW"
//  mode.
//----------

TriggerEvent("pcd_Insert")

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public subroutine set_dw_key (string column_name);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_DW_Key
//  Description   : Sets the specified column as a key column.
//
//  Parameters    : STRING Column_Name
//                        The name of the column that is to
//                        marked as a key column.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

Modify(Column_Name + ".Key=Yes")

RETURN
end subroutine

public function integer set_dw_modify ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Set_DW_Modify
//  Description   : Changes the DataWindow to "MODIFY" mode.
//
//  Parameters    : (None)
//
//  Return Value  : INTEGER -
//                       Error return - c_Success or c_Fatal
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//----------
//  Use the pcd_Modify event to put the DataWindow into "MODIFY"
//  mode.
//----------

TriggerEvent("pcd_Modify")

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public function integer set_dw_new ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Set_DW_New
//  Description   : Changes the DataWindow to "NEW" mode.
//
//  Parameters    : (None)
//
//  Return Value  : INTEGER -
//                       Error return - c_Success or c_Fatal
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//----------
//  Use the pcd_New event to put the DataWindow into "NEW"
//  mode.
//----------

TriggerEvent("pcd_New")

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public subroutine set_dw_options (transaction dbca, uo_dw_main parent_dw, unsignedlong control_word, unsignedlong visual_word);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_DW_Options
//  Description   : Specifies how the DataWindow is controlled
//                  and displayed.
//
//  Parameters    : TRANSACTION DBCA -
//                        A transaction object that will be
//                        used for this DataWindow.
//
//                  UO_DW_MAIN Parent_DW -
//                        The parent DataWindow for this
//                        DataWindow.
//
//                  UNSIGNEDLONG Control_Word -
//                        Specifies options for how this
//                        DataWindow is to behave.
//
//                  UNSIGNEDLONG Visual_Word -
//                        Specifies options for how this
//                        DataWindow is to be displayed.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN              l_Found
INTEGER              l_Error, l_Idx,  l_Jdx,       l_Kdx
UNSIGNEDLONG         l_RefreshOpenOption
STRING               l_Describe,      l_Result
GRAPHICOBJECT        l_Control,       l_SearchObj, l_FilterObj
UO_CB_MAIN           l_PCControl
UO_DDDW_SEARCH_MAIN  l_SDDDWName
UO_DW_SEARCH         l_SDWName
UO_DDLB_SEARCH       l_SDDLBName
UO_EM_SEARCH         l_SEMName
UO_LB_SEARCH         l_SLBName
UO_SLE_SEARCH        l_SSLEName
UO_DDDW_FILTER_MAIN  l_FDDDWName
UO_DW_FILTER         l_FDWName
UO_DDLB_FILTER       l_FDDLBName
UO_EM_FILTER         l_FEMName
UO_LB_FILTER         l_FLBName
UO_SLE_FILTER        l_FSLEName

//----------
//  If we are sharing, unwire ourselves until we get through this
//  routine.  This routine may change the sharing option.
//----------

IF i_ShareType <> c_ShareNone THEN
   i_SharePrimary.Unwire_Share(THIS)
END IF

//----------
//  Turn off processing of redraw, RFC, VScroll, and validation
//  while we setup the DataWindow.
//----------

SetRedraw(c_BufferDraw)
i_RedrawCount = i_RedrawCount + 1
i_RedrawStack[i_RedrawCount] = c_BufferDraw

i_IgnoreRFC      = (NOT c_IgnoreRFC)
i_IgnoreRFCCount = i_IgnoreRFCCount + 1
i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

i_IgnoreVScroll = (NOT c_IgnoreVScroll)
i_IgnoreVSCount = i_IgnoreVSCount + 1
i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

i_IgnoreVal      = (NOT c_IgnoreVal)
i_IgnoreValCount = i_IgnoreValCount + 1
i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

//----------
//  Get PowerLock Security information.
//----------

i_PLNewOK       = f_PL_CheckAdd(i_Window, THIS)
i_PLModifyOK    = f_PL_CheckEnable(i_Window, THIS)
i_PLDeleteOK    = f_PL_CheckDelete(i_Window, THIS)
i_PLDrillDownOK = f_PL_CheckDrillDown(i_Window, THIS)

//----------
//  Remember the developer's specifications.
//----------

i_DW_ControlWord = Control_Word
i_DW_VisualWord  = Visual_Word

PCCA.DLL.fu_SetDWOptions (THIS, Control_Word, Visual_Word)

//----------
//  Set restrictions based on the security of this user.
//----------

IF NOT i_PLNewOk THEN
   i_AllowNew           = FALSE
   i_EnableNewOnOpen    = FALSE
   i_AllowControlNew    = FALSE
END IF

IF NOT i_PLModifyOk THEN
   i_AllowModify        = FALSE
   i_AllowNew           = FALSE
   i_AllowDelete        = FALSE
   i_EnableNewOnOpen    = FALSE
   i_EnableModifyOnOpen = FALSE
   i_AllowControlModify = FALSE
   i_AllowControlNew    = FALSE
   i_AllowControlDelete = FALSE
END IF

IF NOT i_PLDeleteOk THEN
   i_AllowDelete        = FALSE
   i_AllowControlDelete = FALSE
END IF

IF NOT i_PLDrillDownOK THEN
   i_EnableModifyOnOpen = FALSE
   i_EnableNewOnOpen    = FALSE
END IF

//----------
//  Initialize the DataWindow if it has not been done.
//----------

IF NOT i_Initialized THEN

   //----------
   //  Initialize DataWindow instance variables.
   //----------

   //----------
   //  DataWindow Column and Validation Information.
   //----------

   i_ItemValidated = FALSE

   //----------
   //  Initialize the DataWindow states.
   //----------

   i_DWState     = c_DWStateUndefined
   i_CurrentMode = c_ModeUndefined

   //----------
   //  Basic DataWindow relationships.
   //----------

   i_RootDW       = THIS
   i_ScrollDW     = THIS
   i_SharePrimary = THIS
   i_ShareParent  = THIS

   //----------
   //  RowFocusChanged and Selection Information.
   //----------

   i_RFCEvent           = c_RFC_Undefined
   i_OpenModeSet        = FALSE
   i_RefreshPosted      = FALSE
   i_SkipFirstRefresh   = TRUE
   i_RetrieveMySelf     = FALSE
   i_RetrieveChildren   = FALSE
   i_IsEmpty            = FALSE
   i_AddedEmpty         = FALSE
   i_SetEmptyColors     = FALSE
   i_MoveRow            = 0
   i_ShowRow            = 0
   i_AnchorRow          = 0
   i_CursorRow          = 0
   i_OldCursorRow       = 0
   i_OldClickedRow      = 0
   i_DWTopRow           = 0
   i_NumSelected        = 0
   i_CurInstance        = 0

   //----------
   //  Reset the Event Control information.
   //----------

   is_EventControl = is_ResetControl

   //----------
   //  Reset the Developer's Control information.
   //----------

   Reset_Dev_Control()

   //----------
   //  See if the parent DataWindow specified is a valid
   //  DataWindow.  If it is, then remember who the parent is and
   //  set a flag indicating that the parent is valid.
   //----------

   IF IsValid(Parent_DW) THEN

      //----------
      //  See if the developer did a dumb thing.
      //----------

      IF i_ParentDW = THIS THEN
         PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Set_DW_Options()"
         PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
         PCCA.MB.i_MB_Strings[3] = i_ClassName
         PCCA.MB.i_MB_Strings[4] = i_Window.Title
         PCCA.MB.i_MB_Strings[5] = DataObject
         PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_SameParent, &
                            0, PCCA.MB.i_MB_Numbers[],   &
                            5, PCCA.MB.i_MB_Strings[])
         PCCA.Error = c_Fatal
         GOTO Finished
      END IF

      i_ParentDW      = Parent_DW
      i_ParentIsValid = TRUE
   ELSE

      //----------
      //  There is not a parent for this DataWindow.
      //----------

      i_ParentDW      = PCCA.Null_Object
      i_ParentIsValid = FALSE
   END IF

   //----------
   //  Save the tranaction object passed by the developer and set
   //  the transaction object for the DataWindow.
   //----------

   i_DBCA = DBCA
   IF NOT IsNull(DBCA) THEN
      l_Error = SetTransObject(i_DBCA)
      IF l_Error <> 1 THEN
         PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Set_DW_Options()"
         PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
         PCCA.MB.i_MB_Strings[3] = i_ClassName
         PCCA.MB.i_MB_Strings[4] = i_Window.Title
         PCCA.MB.i_MB_Strings[5] = DataObject
         PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_BadTransObject, &
                            0, PCCA.MB.i_MB_Numbers[],       &
                            5, PCCA.MB.i_MB_Strings[])
         PCCA.Error = c_Fatal
         GOTO Finished
      END IF

      //----------
      //  Set PowerLock Row Security information.
      //----------

      f_PL_RowSecurity(i_Window)

   END IF

   //----------
   //  The first DataWindow on this window that was initialized by
   //  the developer using Set_DW_Options() may contain default
   //  menus.  If this DataWindow has not had its menus set yet,
   //  use the defaults if they are valid.
   //----------

   IF i_WindowDWs[1].i_DefaultMenusDefined THEN
      IF NOT i_FileMenuIsValid THEN
         i_FileMenu        = i_WindowDWs[1].i_DefaultFileMenu
         i_FileMenuIsValid = IsValid(i_FileMenu)
      END IF

      IF NOT i_EditMenuIsValid THEN
         i_EditMenu        = i_WindowDWs[1].i_DefaultEditMenu
         i_EditMenuIsValid = IsValid(i_EditMenu)
      END IF

      IF NOT i_PopupMenuIsValid THEN
         i_PopupMenu        = i_WindowDWs[1].i_DefaultPopupMenu
         i_PopupMenuIsValid = IsValid(i_PopupMenu)
      END IF

      Config_Menus()
   END IF

   //----------
   //  Save the original SQL Select statement or Stored
   //  Procedure from the DataWindow.  If PowerBuilder returns
   //  an "not known" answer, just set the answer to empty.
   //----------

   i_SQLSelect = Describe("DataWindow.Table.Select")
   IF i_SQLSelect = "?" OR i_SQLSelect = "!" THEN
      i_SQLSelect = ""
   END IF

   i_StoredProc = Describe("DataWindow.Table.Procedure")
   IF i_StoredProc = "?" OR i_StoredProc = "!" THEN
      i_StoredProc = ""
   END IF

   //----------
   //  If this is the first time we are being initialized, we need
   //  to load i_PCControls with the list of PowerClass controls.
   //  This is done by looking through all of the controls on
   //  the window and container (if any).  If a control has a tag
   //  that is equal to "PowerClass Control", we assume that it is
   //  a PowerClass command button.
   //----------

   IF i_InitCount = 0 THEN

      l_Kdx = UpperBound(i_Window.Control[])
      FOR l_Idx = 1 TO l_Kdx
         l_Control = i_Window.Control[l_Idx]

         //----------
         //  See if the tag specifies that this is a PowerClass
         //  control.
         //----------

         IF l_Control.Tag = "PowerClass Control" THEN

            //----------
            //  We now have to look though our control list to make
            //  sure that this control is not already in our list.
            //  For example, if the developer has called
            //  Wire_Button(), then the command button has already
            //  been added to our control list.
            //----------

            l_Found = FALSE
            FOR l_Jdx = 1 TO i_NumPCControls
               IF i_PCControls[l_Jdx] = l_Control THEN
                  l_Found = TRUE
                  EXIT
               END IF
            NEXT

            //----------
            //  If the command button was not already found in the
            //  control list, then we need to add it in.
            //----------

            IF NOT l_Found THEN
               i_NumPCControls               = i_NumPCControls + 1
               i_PCControls[i_NumPCControls] = l_Control
            END IF
         END IF
      NEXT

      IF IsNull(i_Container) THEN
      ELSE
         l_Kdx = UpperBound(i_Container.Control[])
         FOR l_Idx = 1 TO l_Kdx
            l_Control = i_Container.Control[l_Idx]

            //----------
            //  See if the tag specifies that this is a PowerClass
            //  control.
            //----------

            IF l_Control.Tag = "PowerClass Control" THEN

               //----------
               //  We now have to look though our control list to
               //  make sure that this control is not already in
               //  our list.  For example, if the developer has
               //  called Wire_Button(), then the command button
               //  has already been added to our control list.
               //----------

               l_Found = FALSE
               FOR l_Jdx = 1 TO i_NumPCControls
                  IF i_PCControls[l_Jdx] = l_Control THEN
                     l_Found = TRUE
                     EXIT
                  END IF
               NEXT

               //----------
               //  If the command button was not already found in
               //  the control list, then we need to add it in.
               //----------

               IF NOT l_Found THEN
                  i_NumPCControls               = i_NumPCControls + 1
                  i_PCControls[i_NumPCControls] = l_Control
               END IF
            END IF
         NEXT
      END IF
   END IF

   //----------
   //  Determine if any standard menu items or buttons have been
   //  secured so that PowerClass will not touch them.
   //----------

   Secure_Controls()

   //----------
   //  Insert ourselves into the parent, if one was specified.
   //----------

   IF i_ParentIsValid THEN

      //----------
      //  We need to insert ourselves into our parent's list of
      //  DataWindows.  Normal DataWindows and Instance DataWindows
      //  go into different lists.  Check to see which list this
      //  DataWindow should go into.
      //----------

      IF i_IsInstance THEN

         //----------
         //  Append this DataWindow to the parent's Instance
         //  DataWindow list.
         //----------

         i_ParentDW.i_NumInstance = i_ParentDW.i_NumInstance + 1
         i_ParentDW.i_CurInstance = i_ParentDW.i_NumInstance
         i_ParentDW.i_InstanceDW[i_ParentDW.i_NumInstance] = THIS

         //----------
         //  Since this is an Instance DataWindow, we need to get
         //  the keys of the row that caused us to open.  Note that
         //  this row is in our parent.  The Get_Col_Info() makes
         //  sure that the parent has gotten all of the necessary
         //  information about its columns (e.g. what columns are
         //  the key columns.
         //----------

         IF NOT i_ParentDW.i_GotColInfo THEN
            i_ParentDW.Get_Col_Info()
         END IF

         //----------
         //  Sanity check.  The parent must have keys if instance
         //  DataWindows are going to work.
         //----------

         IF i_ParentDW.i_xNumKeyColumns = 0 THEN
            PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Set_DW_Options()"
            PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
            PCCA.MB.i_MB_Strings[3] = i_ClassName
            PCCA.MB.i_MB_Strings[4] = i_Window.Title
            PCCA.MB.i_MB_Strings[5] = DataObject
            PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_NoKeysInParent, &
                               0, PCCA.MB.i_MB_Numbers[],       &
                               5, PCCA.MB.i_MB_Strings[])
            PCCA.Error = c_Fatal
            GOTO Finished
         END IF

         //----------
         //  For every key column in our parent, get the keys for
         //  the currently selected row.
         //----------

         FOR l_Idx = 1 TO i_ParentDW.i_xNumKeyColumns

            //----------
            //  i_xKeyColumns[] contains the numbers of the columns
            //  that are the key columns.
            //----------

            l_Jdx = i_ParentDW.i_xKeyColumns[l_Idx]

            //----------
            //  For each key column, get the data the the selected
            //  row.
            //----------

            i_KeysInParent[l_Idx] = i_ParentDW.Get_Col_Data_Ex &
                (i_ParentDW.i_ShowRow, l_Jdx, Primary!, c_GetCurrentValues)
         NEXT
      ELSE

         //----------
         //  Append this DataWindow to the parent's normal
         //  DataWindow list.
         //----------

         IF NOT l_Found THEN
            i_ParentDW.i_NumChildren = i_ParentDW.i_NumChildren + 1
            i_ParentDW.i_ChildDW[i_ParentDW.i_NumChildren] = THIS
         END IF

         //----------
         //  Sanity check.  The parent must have keys if refresh
         //  parent is going to work.
         //----------

         IF i_RefreshParent THEN

            IF NOT i_ParentDW.i_GotColInfo THEN
               i_ParentDW.Get_Col_Info()
            END IF

            IF i_ParentDW.i_xNumKeyColumns = 0 THEN
               PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Set_DW_Options()"
               PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
               PCCA.MB.i_MB_Strings[3] = i_ClassName
               PCCA.MB.i_MB_Strings[4] = i_Window.Title
               PCCA.MB.i_MB_Strings[5] = DataObject
               PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_NoKeysInParent, &
                                  0, PCCA.MB.i_MB_Numbers[],       &
                                  5, PCCA.MB.i_MB_Strings[])
               PCCA.Error = c_Fatal
               GOTO Finished
            END IF
         END IF
      END IF
   END IF

   //----------
   //  If concurrency checking was requested, make sure that the
   //  DataWindow is configured for it.
   //----------

   IF i_EnableCC THEN
      PCCA.MB.fu_GetDatabaseType(i_DBCA, i_CCDBMSType, i_CCIsODBC)
      IF i_CCDBMSType <> PCCA.MB.c_DB_SQLServer AND &
         i_CCDBMSType <> PCCA.MB.c_DB_Sybase    THEN
         l_Describe = "DataWindow.Table.UpdateWhere"
         l_Result   = Describe(l_Describe)
         IF l_Result = "0" THEN
            PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Set_DW_Options()"
            PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
            PCCA.MB.i_MB_Strings[3] = i_ClassName
            PCCA.MB.i_MB_Strings[4] = i_Window.Title
            PCCA.MB.i_MB_Strings[5] = DataObject
            PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_BadUpdateWhere, &
                               0, PCCA.MB.i_MB_Numbers[],       &
                               5, PCCA.MB.i_MB_Strings[])
         END IF
      END IF
   END IF
END IF

//----------
//  Call Setup_DW() to initialize visual options of the
//  DataWindow using the specified bits.
//----------

Setup_DW()

//----------
//  See if this DataWindow is already in use.  We need to do
//  this because this routine may be called multiple times and
//  we only need to do this if DataWindow is not in use.
//----------

IF NOT i_InUse THEN

   //----------
   //  If we have a parent and they exist, then we may be in
   //  use.  We can only be in use if our parent is in use.
   //----------

   IF i_ParentIsValid THEN
      i_InUse = i_ParentDW.i_InUse
   ELSE
      i_InUse = TRUE
   END IF

   IF i_InUse THEN

      //----------
      //  Depending if we have a valid parent or not, there are
      //  some modes that we have to avoid.
      //----------

      IF i_ParentIsValid THEN
         IF i_IsInstance THEN
            i_ParentDW.i_MultiSelect = FALSE
         END IF
      ELSE
         i_RefreshParent = FALSE
         i_RefreshChild  = FALSE
         i_ScrollParent  = FALSE
         i_ShareData     = FALSE
         i_IsInstance    = FALSE
      END IF

      //----------
      //  Since this DataWindow is now in use, we can enable all
      //  of the objects that have been wired to this DataWindow.
      //  Note that buttons will be enabled by pcd_SetControl.
      //----------

      IF i_NumMessages > 0 THEN
      FOR l_Idx = 1 TO i_NumMessages
         i_Messages[l_Idx].Enabled = TRUE
      NEXT
      END IF

      IF i_NumFindObjects > 0 THEN
      FOR l_Idx = 1 TO i_NumFindObjects
         i_FindObjects[l_Idx].Enabled = TRUE
      NEXT
      END IF

      IF i_NumSearchObjects > 0 THEN
      FOR l_Idx = 1 TO i_NumSearchObjects
         l_SearchObj = i_SearchObjects[l_Idx]
         CHOOSE CASE l_SearchObj.Tag
            CASE "PowerObject Search DDDW"
               l_SDDDWName         = l_SearchObj
               l_SDDDWName.Enabled = TRUE
            CASE "PowerObject Search DW"
               l_SDWName           = l_SearchObj
               l_SDWName.Enabled   = TRUE
            CASE "PowerObject Search DDLB"
               l_SDDLBName         = l_SearchObj
               l_SDDLBName.Enabled = TRUE
            CASE "PowerObject Search EM"
               l_SEMName           = l_SearchObj
               l_SEMName.Enabled   = TRUE
            CASE "PowerObject Search LB"
               l_SLBName           = l_SearchObj
               l_SLBName.Enabled   = TRUE
            CASE "PowerObject Search SLE"
               l_SSLEName          = l_SearchObj
               l_SSLEName.Enabled  = TRUE
            CASE ELSE
         END CHOOSE
      NEXT
      END IF

      IF i_NumFilterObjects > 0 THEN
      FOR l_Idx = 1 TO i_NumFilterObjects
         l_FilterObj = i_FilterObjects[l_Idx]
         CHOOSE CASE l_FilterObj.Tag
            CASE "PowerObject Filter DDDW"
               l_FDDDWName         = l_FilterObj
               l_FDDDWName.Enabled = TRUE
            CASE "PowerObject Filter DW"
               l_FDWName           = l_FilterObj
               l_FDWName.Enabled   = TRUE
            CASE "PowerObject Filter DDLB"
               l_FDDLBName         = l_FilterObj
               l_FDDLBName.Enabled = TRUE
            CASE "PowerObject Filter EM"
               l_FEMName           = l_FilterObj
               l_FEMName.Enabled   = TRUE
            CASE "PowerObject Filter LB"
               l_FLBName           = l_FilterObj
               l_FLBName.Enabled   = TRUE
            CASE "PowerObject Filter SLE"
               l_FSLEName          = l_FilterObj
               l_FSLEName.Enabled  = TRUE
            CASE ELSE
         END CHOOSE
      NEXT
      END IF

      //----------
      //  Find the root-level DataWindow.
      //----------

      i_RootDW = Find_DW_Root(c_FindRootDW)

      //----------
      //  Unless the developer said not to, we will always try to
      //  do a retrieve when we are initialized.
      //----------

      i_RetrieveMySelf = i_RetrieveOnOpen

      //----------
      //  Determine whether the DataWindow should be refreshed
      //  during initialization.
      //----------

      l_RefreshOpenOption = i_RefreshOpenOption

      IF l_RefreshOpenOption = c_AutoRefreshOnOpen THEN
         l_RefreshOpenOption = c_SkipRefreshOnOpen

         IF NOT i_ParentIsValid THEN
            l_RefreshOpenOption = c_PostRefreshOnOpen
         ELSE
            IF PCCA.PCMGR.i_DW_InRefresh = 0 THEN
            IF NOT i_ParentDW.i_RefreshPosted THEN
               l_RefreshOpenOption = c_PostRefreshOnOpen
            END IF
            END IF
         END IF
      END IF

      //----------
      //  If we are not being triggered by pcd_Refresh, then
      //  we don't have to worry about optimizing out the
      //  second retrieve.
      //----------

      IF l_RefreshOpenOption <> c_SkipRefreshOnOpen THEN

         i_SkipFirstRefresh = FALSE
         IF i_ParentIsValid THEN
            IF PCCA.PCMGR.i_DW_InRefresh > 0 OR &
               i_ParentDW.i_RefreshPosted    THEN
               i_SkipFirstRefresh = TRUE
            END IF
         END IF

         //----------
         //  Send the pcd_Refresh.
         //----------

         IF PCCA.POMGR.i_TimerOn OR &
            l_RefreshOpenOption = c_TriggerRefreshOnOpen THEN

            TriggerEvent("pcd_Refresh")
         ELSE
            PCCA.PCMGR.Post_DW_Event(THIS, "pcd_Refresh")
            i_RefreshPosted = TRUE
         END IF
      ELSE
         i_SkipFirstRefresh = FALSE
         IF i_RefreshOpenOption = c_SkipRefreshOnOpen OR &
            i_ModeOnOpen        = c_ModeUndefined     THEN
            i_OpenModeSet = TRUE
         END IF
      END IF

      //----------
      //  If this DataWindow has normal or instance children
      //  DataWindows, then call Set_DW_Options() for each one.
      //  This will allow them to become usable since this
      //  DataWindow is now usable.
      //----------

      IF i_NumChildren > 0 THEN
      FOR l_Idx = 1 TO i_NumChildren
         i_ChildDW[l_Idx].Set_DW_Options         &
            (PCCA.Null_Object, PCCA.Null_Object, &
             c_Default,        c_Default)
      NEXT
      END IF

      IF i_NumInstance > 0 THEN
      FOR l_Idx = 1 TO i_NumInstance
         i_InstanceDW[l_Idx].Set_DW_Options        &
            (PCCA.Null_Object,   PCCA.Null_Object, &
             c_Default, c_Default)
      NEXT
      END IF
   END IF
END IF

//----------
//  If we are in use, set up the last few things.
//----------

IF i_InUse THEN

   //----------
   //  Find the scroll DataWindow.
   //----------

   i_ScrollDW = Find_DW_Root(c_FindScrollDW)

   //----------
   //  Set the RetrieveAsNeeded and Share attributes.  These
   //  must be done after we have a transaction object and we
   //  have initialized the child (i.e. we don't want to be
   //  doing Reset() when we are sharing data with our parent).
   //----------

   IF i_RetrieveAsNeeded THEN
      Modify("DataWindow.Retrieve.AsNeeded=Yes")
   ELSE
      Modify("DataWindow.Retrieve.AsNeeded=No")
   END IF

   //----------
   //  Set up sharing, if requested.
   //----------

   IF i_ShareData THEN

      //----------
      //  Find the share DataWindow.
      //----------

      i_ParentDW.Wire_Share(THIS, c_ShareEnabled)
   END IF
ELSE

   //----------
   //  If the DataWindow is still not is use, make sure that it is
   //  empty and in "VIEW" mode.
   //----------

   IF NOT i_IsEmpty THEN
      Reset()
      i_DWState = c_DWStateUndefined
      TriggerEvent("pcd_Disable")
   END IF
END IF

//----------
//  The DataWindow is finally initialized.
//----------

i_Initialized = TRUE
i_InitCount   = i_InitCount + 1

//----------
//  Restore states as we found them.
//----------

IF i_IgnoreValCount > 1 THEN
   i_IgnoreValCount = i_IgnoreValCount - 1
   i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
ELSE
   i_IgnoreValCount = 0
   i_IgnoreVal      = FALSE
END IF

IF i_IgnoreVSCount > 1 THEN
   i_IgnoreVSCount = i_IgnoreVSCount - 1
   i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
ELSE
   i_IgnoreVSCount = 0
   i_IgnoreVScroll = FALSE
END IF
IF NOT i_IgnoreVScroll THEN
   i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
END IF

IF i_IgnoreRFCCount > 1 THEN
   i_IgnoreRFCCount = i_IgnoreRFCCount - 1
   i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
ELSE
   i_IgnoreRFCCount = 0
   i_IgnoreRFC      = FALSE
END IF

IF i_RedrawCount > 1 THEN
   i_RedrawCount = i_RedrawCount - 1
   SetRedraw(i_RedrawStack[i_RedrawCount])
ELSE
   i_RedrawCount = 0
   SetRedraw(TRUE)
END IF

Finished:

RETURN
end subroutine

public subroutine set_dw_pmenu (menu popup_menu);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_DW_PMenu
//  Description   : Initializes the POPUP menu for uo_DW_Main.
//
//  Parameters    : MENU Popup_Menu -
//                       The POPUP menu that is to be used
//                       by this DataWindow.  If it is
//                       specified as c_NullMenu, then a
//                       POPUP menu will be created from
//                       M_POPUP.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx
MENU     l_TmpMenu

//----------
//  We indicate that this routine has been called by setting
//  i_PopupMenuInit to TRUE.  If this routine is called by the
//  developer, this flag will be TRUE and PowerClass will know
//  not to not call it again.
//----------

i_PopupMenuInit = TRUE

//----------
//  Set our menu to the specified menu.
//----------

i_PopupMenu = Popup_Menu

//----------
//  See if the POPUP Menu is valid.  If it is, save the individual
//  menu items into instance variables for speedy access.
//----------

i_PopupMenuIsValid       = IsValid(i_PopupMenu)
i_PopupMenuNewNum        = 0
i_PopupMenuViewNum       = 0
i_PopupMenuModifyNum     = 0
i_PopupMenuInsertNum     = 0
i_PopupMenuDeleteNum     = 0
i_PopupMenuModeSepNum    = 0
i_PopupMenuFirstNum      = 0
i_PopupMenuPrevNum       = 0
i_PopupMenuNextNum       = 0
i_PopupMenuLastNum       = 0
i_PopupMenuSearchSepNum  = 0
i_PopupMenuQueryNum      = 0
i_PopupMenuSearchNum     = 0
i_PopupMenuFilterNum     = 0
i_PopupMenuSaveSepNum    = 0
i_PopupMenuSaveNum       = 0
i_PopupMenuSaveRowsAsNum = 0
i_PopupMenuPrintSepNum   = 0
i_PopupMenuPrintNum      = 0

//----------
//  Determine if the POPUP menu is the same menu as the EDIT Menu.
//----------

IF i_PopupMenuIsValid THEN
    IF i_EditMenuIsValid THEN
       i_PopupMenuIsEdit = (i_EditMenu = i_PopupMenu)
    ELSE
       i_PopupMenuIsEdit = FALSE
   END IF
ELSE

   //----------
   //  The POPUP Menu is not valid.  Indicate that the EDIT Menu
   //  should be used when the POPUP Menu is needed.
   //----------

   i_PopupMenuIsEdit = TRUE
END IF

i_EditMenuIsPopup = i_PopupMenuIsEdit

//----------
//  If the POPUP Menu is not the same as the EDIT Menu, then
//  save the individual menu items into instance variables for
//  speedy access.
//----------

IF i_PopupMenuIsValid THEN
   IF NOT i_PopupMenuIsEdit THEN
      l_Jdx = UpperBound(i_PopupMenu.Item[])
      FOR l_Idx = 1 TO l_Jdx
         l_TmpMenu = i_PopupMenu.Item[l_Idx]
         CHOOSE CASE l_TmpMenu.Text
            CASE PCCA.MDI.c_MDI_MenuLabelNew
               i_PopupMenuNewNum        = l_Idx
               i_PopupMenuNew           = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelView
               i_PopupMenuViewNum       = l_Idx
               i_PopupMenuView          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelModify
               i_PopupMenuModifyNum     = l_Idx
               i_PopupMenuModify        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelInsert
               i_PopupMenuInsertNum     = l_Idx
               i_PopupMenuInsert        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelDelete
               i_PopupMenuDeleteNum     = l_Idx
               i_PopupMenuDelete        = l_TmpMenu
               IF l_Idx < l_Jdx THEN
                  IF i_PopupMenu.Item[l_Idx + 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuModeSepNum = l_Idx + 1
                     i_PopupMenuModeSep    = &
                        i_PopupMenu.Item[l_Idx + 1]
                  END IF
               END IF
            CASE PCCA.MDI.c_MDI_MenuLabelFirst
               i_PopupMenuFirstNum      = l_Idx
               i_PopupMenuFirst         = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelPrev
               i_PopupMenuPrevNum       = l_Idx
               i_PopupMenuPrev          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelNext
               i_PopupMenuNextNum       = l_Idx
               i_PopupMenuNext          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelLast
               i_PopupMenuLastNum       = l_Idx
               i_PopupMenuLast          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelQuery
               i_PopupMenuQueryNum      = l_Idx
               i_PopupMenuQuery         = l_TmpMenu
               IF l_Idx > 1 THEN
                  IF i_PopupMenu.Item[l_Idx - 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuSearchSepNum = l_Idx - 1
                     i_PopupMenuSearchSep    = &
                        i_PopupMenu.Item[l_Idx - 1]
                  END IF
               END IF
            CASE PCCA.MDI.c_MDI_MenuLabelSearch
               i_PopupMenuSearchNum     = l_Idx
               i_PopupMenuSearch        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelFilter
               i_PopupMenuFilterNum     = l_Idx
               i_PopupMenuFilter        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelSave
               i_PopupMenuSaveNum       = l_Idx
               i_PopupMenuSave          = l_TmpMenu
               IF l_Idx > 1 THEN
                  IF i_PopupMenu.Item[l_Idx - 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuSaveSepNum = l_Idx - 1
                     i_PopupMenuSaveSep    = &
                        i_PopupMenu.Item[l_Idx - 1]
                  END IF
               END IF
            CASE PCCA.MDI.c_MDI_MenuLabelSaveRowsAs
               i_PopupMenuSaveRowsAsNum = l_Idx
               i_PopupMenuSaveRowsAs    = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelPrint
               i_PopupMenuPrintNum      = l_Idx
               i_PopupMenuPrint         = l_TmpMenu
               IF l_Idx > 1 THEN
                  IF i_PopupMenu.Item[l_Idx - 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuPrintSepNum = l_Idx - 1
                     i_PopupMenuPrintSep    = &
                        i_PopupMenu.Item[l_Idx - 1]
                  END IF
               END IF
         END CHOOSE
      NEXT
   ELSE
      PCCA.DLL.fu_SetDWPMenu (THIS)
   END IF
END IF

RETURN
end subroutine

public subroutine set_dw_popup (menu popup_menu);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_DW_Popup
//  Description   : Sets the POPUP menu to be used by the
//                  DataWindow.
//
//  Parameters    : MENU Popup_Menu -
//                       The POPUP menu that is to be used
//                       by this DataWindow.  If it is
//                       specified as c_NullMenu, then a
//                       POPUP menu will be created from
//                       M_POPUP.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx

//----------
//  If the passed POPUP menu is not valid, then create a popup
//  of type M_POPUP.
//----------

IF IsValid(Popup_Menu) THEN
   i_PopupMenu = Popup_Menu.Item[1]
ELSE
   IF IsValid(i_mPopup) THEN
   ELSE
      i_PopupMenu = c_NullMenu
      i_mPopup    = CREATE M_POPUP
   END IF

   IF IsValid(i_mPopup) THEN
      i_PopupMenu         = i_mPopup.Item[1]
      i_PopupMenu.Visible = TRUE

      l_Jdx = UpperBound(i_PopupMenu.Item[])
      FOR l_Idx = 1 TO l_Jdx
          i_PopupMenu.Item[l_Idx].Visible = TRUE
          i_PopupMenu.Item[l_Idx].Enabled = FALSE
      NEXT
   END IF
END IF

IF IsValid(i_PopupMenu) THEN
   i_PopupMenuIsValid = TRUE
ELSE
   i_PopupMenuIsValid = FALSE
END IF

RETURN
end subroutine

public function integer set_dw_query (unsignedlong do_any_changes);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Set_DW_Query
//  Description   : Changes the DataWindow to "QUERY" mode.
//
//  Parameters    : UNSIGNEDLONG Do_Any_Changes -
//                        Specifies if the children DataWindows
//                        will be checked for changes before the
//                        mode is changed.  If there are changes
//                        that the user wishes to save, they will
//                        be saved and then the new rows will be
//                        selected.
//
//                           - c_CheckForChanges
//                           - c_IgnoreChanges
//                           - c_SaveChanges
//
//  Return Value  : INTEGER -
//                       Error return - c_Success or c_Fatal
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//----------
//  Use the pcd_Query event to put the DataWindow into "QUERY"
//  mode.
//----------

i_InQuery = TRUE
PCCA.Error = Check_Modifications(Do_Any_Changes)
i_InQuery = FALSE

IF PCCA.Error = c_Success THEN
   TriggerEvent("pcd_Query")
END IF

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public function integer set_dw_view (unsignedlong do_any_changes);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Set_DW_View
//  Description   : Changes the DataWindow to "VIEW" mode.
//
//  Parameters    : UNSIGNEDLONG Do_Any_Changes -
//                        Specifies if the children DataWindows
//                        will be checked for changes before the
//                        mode is changed.  If there are changes
//                        that the user wishes to save, they will
//                        be saved and then the new rows will be
//                        selected.
//
//                           - c_CheckForChanges
//                           - c_IgnoreChanges
//                           - c_SaveChanges
//
//  Return Value  : INTEGER -
//                       Error return - c_Success or c_Fatal
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//----------
//  Use the pcd_View event to put the DataWindow into "VIEW"
//  mode.
//----------

i_InView = TRUE
PCCA.Error = Check_Modifications(Do_Any_Changes)
i_InView = FALSE

IF PCCA.Error = c_Success THEN
   TriggerEvent("pcd_View")
END IF

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public subroutine set_open_child (unsignedlong open_child_option);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_Open_Child
//  Description   : This routine provides the developer with
//                  a method to force or prevent the
//                  pcd_PickedRow event from executing during
//                  pcd_Refresh.  It will generally be called
//                  in the developer's pcd_Retrieve event.
//
//  Parameters    : UNSIGNEDLONG Open_Child_Option -
//                       Should be specified as one of the
//                       following:
//
//                          - c_OpenChildAuto
//                              Indicates that PowerClass is to
//                              call the pcd_PickedRow event
//                              as it deems appropriate.
//
//                          - c_OpenChildForce
//                              Indicates that the developer
//                              wants the pcd_PickedRow event to
//                              be triggered this one time and
//                              revert back to c_OpenChildAuto.
//
//                          - c_OpenChildAlwaysForce
//                              Indicates that the developer
//                              wants the pcd_PickedRow event to
//                              be triggered every time the
//                              pcd_Refresh event executes.
//
//                          - c_OpenChildPrevent
//                              Indicates that the developer
//                              wants the pcd_PickedRow event to
//                              be prevented this one time and
//                              revert back to c_OpenChildAuto.
//
//                          - c_OpenChildAlwaysPrevent
//                              Indicates that the developer
//                              does not want the pcd_PickedRow
//                              event to be triggered again.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_OpenChildOption = Open_Child_Option

RETURN
end subroutine

public subroutine set_preferred_focus (uo_dw_main to_dw);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_Preferred_Focus
//  Description   : Used by the developer to tell PowerClass
//                  the DataWindow that should receive focus
//                  after a user operation (e.g.
//                  RowFocusChanged!).  PowerClass will not
//                  change the current DataWindow if the
//                  developer has specified c_NoAutoFocus
//                  in the Set_DW_Options() call.
//
//  Parameters    : UO_DW_MAIN To_DW -
//                     The DataWindow that focus should be set
//                     to when rows are selected, etc.  If it
//                     is never specified or specified as
//                     c_NullDW, PowerClass will give focus to
//                     the last child DataWindow
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

i_FocusDW = To_DW

RETURN
end subroutine

public subroutine set_row_indicator (unsignedlong which_focus, rowfocusind which_indicator, picture indicator_bitmap, integer x_position, integer y_position);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_Row_Indicator
//  Description   : Allows the developer to specify the current
//                  row indicator for both focus and not-focus
//                  DataWindows.  This routine is somewhat
//                  similar in function to the PowerBuilder
//                  SETROWFOCUSINDICATOR() routine.
//
//  Parameters    : UNSIGNEDLONG Which_Focus -
//                        Indicates if the current row indicator
//                        is being set for while the DataWindow
//                        has focus, does not have focus, both
//                        focus and not focus (i.e.
//                        c_SetRowIndicatorNormal), is empty, or
//                        is in "QUERY" mode.  Valid options are:
//
//                           c_SetRowIndicatorFocus
//                           c_SetRowIndicatorNotFocus
//                           c_SetRowIndicatorNormal
//                           c_SetRowIndicatorEmpty
//                           c_SetRowIndicatorQuery
//
//                  ROWFOCUSIND Which_Indicator -
//                        Designates which PowerBuilder row
//                        indicator is to be used.  This is
//                        only used if the parameter
//                        INDICATOR_BITMAP is NULL.
//
//                  PICTURE Indicator_Bitmap -
//                        Designates a picture bitmap to be used
//                        as the current row indicator.
//                        c_NullPicture can be used if a bitmap
//                        is not desired.
//
//                  INTEGER X_Position -
//                        The X Position of the current row
//                        indicator.
//
//                  INTEGER Y_Position -
//                        The Y Position of the current row
//                        indicator.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF Which_Focus = c_SetRowIndicatorFocus OR &
   Which_Focus = c_SetRowIndicatorNormal THEN
   i_FocusRowInit = TRUE

   IF IsNull(Indicator_Bitmap) THEN
      i_FocusRowIsPicture = FALSE
   ELSE
      i_FocusRowIsPicture = TRUE
   END IF

   i_FocusRowInd       = Which_Indicator
   i_FocusRowPicture   = Indicator_Bitmap
   i_FocusRowXPosition = X_Position
   i_FocusRowYPosition = Y_Position
END IF

IF Which_Focus = c_SetRowIndicatorNotFocus OR &
   Which_Focus = c_SetRowIndicatorNormal THEN
   i_NotFocusRowInit = TRUE

   IF IsNull(Indicator_Bitmap) THEN
      i_NotFocusRowIsPicture = FALSE
   ELSE
      i_NotFocusRowIsPicture = TRUE
   END IF

   i_NotFocusRowInd       = Which_Indicator
   i_NotFocusRowPicture   = Indicator_Bitmap
   i_NotFocusRowXPosition = X_Position
   i_NotFocusRowYPosition = Y_Position
END IF

IF Which_Focus = c_SetRowIndicatorEmpty THEN
   i_EmptyRowInit = TRUE

   IF IsNull(Indicator_Bitmap) THEN
      i_EmptyRowIsPicture = FALSE
   ELSE
      i_EmptyRowIsPicture = TRUE
   END IF

   i_EmptyRowInd       = Which_Indicator
   i_EmptyRowPicture   = Indicator_Bitmap
   i_EmptyRowXPosition = X_Position
   i_EmptyRowYPosition = Y_Position
END IF

IF Which_Focus = c_SetRowIndicatorQuery THEN
   i_QueryRowInit = TRUE

   IF IsNull(Indicator_Bitmap) THEN
      i_QueryRowIsPicture = FALSE
   ELSE
      i_QueryRowIsPicture = TRUE
   END IF

   i_QueryRowInd       = Which_Indicator
   i_QueryRowPicture   = Indicator_Bitmap
   i_QueryRowXPosition = X_Position
   i_QueryRowYPosition = Y_Position
END IF

TriggerEvent("pcd_SetRowIndicator")

RETURN
end subroutine

public function integer set_selected_rows (long num_to_set, long selected_rows[], unsignedlong do_any_changes, unsignedlong do_refresh, unsignedlong refresh_mode);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Set_Selected_Rows
//  Description   : Using the array passed, selects the rows in the
//                  DataWindow and optionally checks and loads
//                  children.
//
//  Parameters    : LONG Num_To_Set -
//                        The number of rows that have been
//                        passed in the Selected_Rows[] array.
//
//                  LONG Selected_Rows[] -
//                        An array containing the row numbers
//                        corresponding to the rows which are
//                        to be marked as selected.
//
//                  UNSIGNEDLONG Do_Any_Changes -
//                        Specifies if the children DataWindows
//                        will be checked for changes before the
//                        new rows are selected.  If there are
//                        changes that the user wishes to save,
//                        they will be saved and then the new rows
//                        will be selected.
//
//                           - c_CheckForChanges
//                           - c_IgnoreChanges
//                           - c_SaveChanges
//
//                  UNSIGNEDLONG Do_Refresh -
//                        Specifies if the children DataWindows
//                        are to retrieved now based on the new
//                        selections.
//
//                           - c_RefreshChildren
//                           - c_NoRefreshChildren
//
//                  UNSIGNEDLONG Refresh_Mode -
//                        Specifies what mode the children
//                        DataWindows should be placed in if
//                        c_RefreshChildren is specified for
//                        Do_Refresh.  Valid options are:
//
//                           - c_RefreshView
//                           - c_RefreshModify
//                           - c_RefreshNew
//                           - c_RefreshSame
//                           - c_RefreshRFC
//
//  Return Value  : INTEGER -
//                        Error return - c_Success or c_Fatal.
//
//******************************************************************
//  If the developer wants to have these rows loaded when the
//  user "takes action" (i.e. clicks on another window), these
//  instance variables should be set after the call to
//  Set_Selected_Rows().
//******************************************************************
//
//   PCCA.PCMGR.i_DW_NeedRetrieve   = TRUE
//   PCCA.PCMGR.i_DW_NeedRetrieveDW = <dw>
//
//******************************************************************
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Error
UNSIGNEDLONG  l_RefreshCmd
LONG          l_RowCount,  l_Idx
STRING        l_HorizScroll

PCCA.Error = Check_Modifications(Do_Any_Changes)

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  Turn redraw off - we don't want the screen flashing as we
//  un-select and select rows.
//----------

SetRedraw(c_BufferDraw)
i_RedrawCount = i_RedrawCount + 1
i_RedrawStack[i_RedrawCount] = c_BufferDraw

//----------
//  Un-select all currently selected rows.
//----------

SelectRow(0, FALSE)

//----------
//  Clear any rows in i_ReselectKeys[].
//----------

i_NumReselectRows = 0

//----------
//  Indicate that there is not a "current" row and that the
//  cursor gets moved to the first row.
//----------

i_RetrieveChildren = TRUE
i_ShowRow          = 0
i_CursorRow        = 1
i_CurInstance      = 0

//----------
//  If the developer has specified rows to be selected, then
//  select them.
//----------

IF Num_To_Set > 0 THEN

   //----------
   //  Find the current number of rows in the DataWindow.
   //----------

   IF i_IsEmpty THEN
      l_RowCount = 0
   ELSE
      l_RowCount = RowCount()
   END IF

   //----------
   //  Step through the developer's array, selecting the
   //  specified rows.
   //----------

   FOR l_Idx = 1 TO Num_To_Set

      //----------
      //  As long as the developer has specified a legal row,
      //  add it to the current selections.
      //----------

      IF Selected_Rows[l_Idx] >  0 AND &
         Selected_Rows[l_Idx] <= l_RowCount THEN

         IF i_HighlightSelected THEN
            SelectRow(Selected_Rows[l_Idx], TRUE)
         END IF

         //----------
         //  We will set the "current" and cursor rows to the
         //  first valid row specified by the developer.  See if
         //  there is already an Instance DataWindow associated
         //  with this row.
         //----------

         IF i_ShowRow = 0 THEN
            i_ShowRow   = Selected_Rows[1]
            i_CursorRow = Selected_Rows[1]
            i_AnchorRow = Selected_Rows[1]
            Find_Instance(i_ShowRow)
         END IF
      END IF
   NEXT
END IF

//----------
//  The cursor row may have changed.  Scroll to the new cursor
//  row.  Make sure that RFC, validation, and VScroll processing
//  are off before we do the scroll and restored after we are done.
//----------

i_IgnoreRFC      = (NOT c_IgnoreRFC)
i_IgnoreRFCCount = i_IgnoreRFCCount + 1
i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

i_IgnoreVal      = (NOT c_IgnoreVal)
i_IgnoreValCount = i_IgnoreValCount + 1
i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

i_IgnoreVScroll = (NOT c_IgnoreVScroll)
i_IgnoreVSCount = i_IgnoreVSCount + 1
i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

l_HorizScroll = Describe("DataWindow.HorizontalScrollPosition")
ScrollToRow(i_CursorRow)
Modify("DataWindow.HorizontalScrollPosition=" + l_HorizScroll)

IF i_IgnoreVSCount > 1 THEN
   i_IgnoreVSCount = i_IgnoreVSCount - 1
   i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
ELSE
   i_IgnoreVSCount = 0
   i_IgnoreVScroll = FALSE
END IF
IF NOT i_IgnoreVScroll THEN
   i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
END IF

IF i_IgnoreValCount > 1 THEN
   i_IgnoreValCount = i_IgnoreValCount - 1
   i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
ELSE
   i_IgnoreValCount = 0
   i_IgnoreVal      = FALSE
END IF

IF i_IgnoreRFCCount > 1 THEN
   i_IgnoreRFCCount = i_IgnoreRFCCount - 1
   i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
ELSE
   i_IgnoreRFCCount = 0
   i_IgnoreRFC      = FALSE
END IF

//----------
//  We turned redraw off while we were selecting rows.  Restore
//  the previous state.
//----------

IF i_RedrawCount > 1 THEN
   i_RedrawCount = i_RedrawCount - 1
   SetRedraw(i_RedrawStack[i_RedrawCount])
ELSE
   i_RedrawCount = 0
   SetRedraw(TRUE)
END IF

//----------
//  Set up i_SelectedRows[].
//----------

i_NumSelected = Get_Retrieve_Rows(i_SelectedRows[])

//----------
//  If a save happened, we may need refreshing.
//----------

IF PCCA.PCMGR.i_DW_InRefresh = 0 THEN
   is_EventControl.Refresh_Cmd            = c_RefreshSame
   is_EventControl.Skip_DW_Children_Valid = TRUE
   is_EventControl.Skip_DW_Children       = THIS
   TriggerEvent("pcd_Refresh")
END IF

//----------
//  If the developer specified to load the newly selected rows
//  into the children DataWindows and there have not been errors,
//  trigger pcd_Refresh to do it.
//----------

IF Do_Refresh = c_RefreshChildren THEN
   is_EventControl.Refresh_Cmd      = Refresh_Mode
   is_EventControl.Only_Do_Children = TRUE
   TriggerEvent("pcd_Refresh")
END IF

Finished:

//----------
//  Get the error we are going to return out of PCCA.Error and
//  reset PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public subroutine swap_dw (string dataobject_name, unsignedlong control_word, unsignedlong visual_word);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Swap_DW
//  Description   : Swaps the DataWindow object.
//
//  Parameters    : STRING DataObject_Name
//                        Specifies the new DataWindow object
//                        for this DataWindow control.
//
//                  UNSIGNEDLONG Control_Word -
//                        Specifies options for how this
//                        DataWindow is to behave.
//
//                  UNSIGNEDLONG Visual_Word -
//                        Specifies options for how this
//                        DataWindow is to be displayed.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

UO_DW_MAIN  l_SaveParent

is_EventControl.Only_Do_Children = TRUE
is_EventControl.Reset            = TRUE
is_EventControl.Refresh_Cmd      = c_RefreshSame
TriggerEvent("pcd_Refresh")

IF i_ShareType <> c_ShareNone THEN
   i_SharePrimary.Unwire_Share(THIS)
END IF

//----------
//  If this DataWindow has a parent, then remove this DataWindow
//  from the parent's list of children.
//----------

IF i_ParentIsValid THEN
   l_SaveParent = i_ParentDW

   IF i_IsInstance THEN
      Remove_Obj(THIS,                     &
                 i_ParentDW.i_NumInstance, &
                 i_ParentDW.i_InstanceDW[])

      //----------
      //  Tell the parent to find a new current Instance
      //  DataWindow since this DataWindow is closing.
      //----------

      i_ParentDW.Find_Instance(i_ParentDW.i_ShowRow)
   ELSE

      //----------
      //  For normal DataWindow, just remove it from the parent
      //  DataWindow's child array.
      //----------

      Remove_Obj(THIS,                     &
                 i_ParentDW.i_NumChildren, &
                 i_ParentDW.i_ChildDW[])
   END IF

   i_ParentIsValid = FALSE
ELSE
   l_SaveParent = c_NullDW
END IF

SetRedraw(c_BufferDraw)
i_RedrawCount = i_RedrawCount + 1
i_RedrawStack[i_RedrawCount] = c_BufferDraw

DataObject    = DataObject_Name
i_Initialized = FALSE
i_InUse       = FALSE

Set_DW_Options(i_DBCA, l_SaveParent, Control_Word, Visual_Word)

IF i_RedrawCount > 1 THEN
   i_RedrawCount = i_RedrawCount - 1
   SetRedraw(i_RedrawStack[i_RedrawCount])
ELSE
   i_RedrawCount = 0
   SetRedraw(TRUE)
END IF

RETURN
end subroutine

public function integer validate_date (ref string value, string format);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_Date
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  STRING Format -
//                        The format that is to be used.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValDate(Value, Format, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_DateValError, "")
   END IF
END IF

RETURN l_Error
end function

public function integer validate_dec (ref string value, string format);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_Dec
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  STRING Format -
//                        The format that is to be used.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValDec(Value, Format, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_DecValError, "")
   END IF
END IF

RETURN l_Error
end function

public function integer validate_dom (ref string value, string format);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_DOM
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  STRING Format -
//                        The format that is to be used.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValDOM(Value, Format, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_DOMValError, "")
   END IF
END IF

RETURN l_Error
end function

public function integer validate_dow (ref string value, string format);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_DOW
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  STRING Format -
//                        The format that is to be used.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValDOW(Value, Format, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_DOWValError, "")
   END IF
END IF

RETURN l_Error
end function

public function integer validate_dw ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_DW
//  Description   : Validates the DataWindow.
//
//  Parameters    : (None)
//
//  Return Value  : INTEGER -
//                        Error return - c_Success or c_Fatal.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//----------
//  Use the pcd_Validate event to validate the DataWindow.
//----------

TriggerEvent("pcd_Validate")

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public function integer validate_dw_row (long validate_row);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_DW_Row
//  Description   : Validates the specified row.
//
//  Parameters    : LONG  Validate_Row -
//                        The row to be validated.
//
//  Return Value  : INTEGER -
//                        Error return - c_Success or c_Fatal.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//----------
//  Use the pcd_ValidateRow event to validate the specified row
//  in the DataWindow.
//----------

i_RowNbr = Validate_Row
TriggerEvent("pcd_ValidateRow")

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public function integer validate_fmt (ref string value, string pattern);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_FMT
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  STRING Pattern -
//                        The pattern that must be matched.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValFMT(Value, Pattern, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      i_ColFormatString = Pattern
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_FMTValError, "")
      i_ColFormatString = ""
   END IF
END IF

RETURN l_Error
end function

public function integer validate_ge (ref string value, real value_ge);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_GE
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  REAL Value_GE -
//                        The value that is to be used for
//                        comparision.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValGE(Value, Value_GE, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      i_ColFormatNumber = Value_GE
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_GEValError, "")
      i_ColFormatNumber = 0.0
   END IF
END IF

RETURN l_Error
end function

public function integer validate_gt (ref string value, real value_gt);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_GT
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  REAL Value_GT -
//                        The value that is to be used for
//                        comparision.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValGT(Value, Value_GT, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      i_ColFormatNumber = Value_GT
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_GTValError, "")
      i_ColFormatNumber = 0.0
   END IF
END IF

RETURN l_Error
end function

public function integer validate_int (ref string value, string format);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_Int
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  STRING Format -
//                        The format that is to be used.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValInt(Value, Format, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_IntValError, "")
   END IF
END IF

RETURN l_Error
end function

public function integer validate_le (ref string value, real value_le);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_LE
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  REAL Value_LE -
//                        The value that is to be used for
//                        comparision.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValLE(Value, Value_LE, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      i_ColFormatNumber = Value_LE
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_LEValError, "")
      i_ColFormatNumber = 0.0
   END IF
END IF

RETURN l_Error
end function

public function integer validate_length (ref string value, long length);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_Length
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  LONG Length -
//                        The value that is to be used for
//                        the length comparision.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValLength(Value, Length, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      i_ColFormatNumber = Length
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_LengthValError, "")
      i_ColFormatNumber = 0.0
   END IF
END IF

RETURN l_Error
end function

public function integer validate_lt (ref string value, real value_lt);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_LT
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  REAL Value_LT -
//                        The value that is to be used for
//                        comparision.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValLT(Value, Value_LT, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      i_ColFormatNumber = Value_LT
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_LTValError, "")
      i_ColFormatNumber = 0.0
   END IF
END IF

RETURN l_Error
end function

public function integer validate_maxlen (ref string value, long max_len);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_MaxLen
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  LONG Max_Len -
//                        The value that is to be used for
//                        the length comparision.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValMaxLen(Value, Max_Len, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      i_ColFormatNumber = Max_Len
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_MaxLenValError, "")
      i_ColFormatNumber = 0.0
   END IF
END IF

RETURN l_Error
end function

public function integer validate_minlen (ref string value, long min_len);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_MinLen
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  LONG Min_Len -
//                        The value that is to be used for
//                        the length comparision.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValMinLen(Value, Min_Len, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      i_ColFormatNumber = Min_Len
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_MinLenValError, "")
      i_ColFormatNumber = 0.0
   END IF
END IF

RETURN l_Error
end function

public function integer validate_mon (ref string value, string format);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_Mon
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  STRING Format -
//                        The format that is to be used.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValMon(Value, Format, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_MonValError, "")
   END IF
END IF

RETURN l_Error
end function

public function integer validate_phone (ref string value);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_Phone
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValPhone(Value, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_PhoneValError, "")
   END IF
END IF

RETURN l_Error
end function

public function integer validate_time (ref string value, string format);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_Time
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  STRING Format -
//                        The format that is to be used.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValTime(Value, Format, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_TimeValError, "")
   END IF
END IF

RETURN l_Error
end function

public function integer validate_year (ref string value, string format);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_Year
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//                  STRING Format -
//                        The format that is to be used.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValYear(Value, Format, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_YearValError, "")
   END IF
END IF

RETURN l_Error
end function

public function integer validate_zip (ref string value);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Validate_Zip
//  Description   : DataWindow validation wrapper.
//
//  Parameters    : ref STRING Value -
//                        The value to be validated.
//
//  Return Value  : INTEGER -
//                        Validation error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error
STRING   l_Value

//----------
//  Turn off the validation messages while we call the global
//  validation routine.
//----------

l_Value = Value
l_Error = f_PO_ValZip(Value, FALSE)

//----------
//  If there are no errors and the value was modified by the
//  validation routine, then set it back into the column.
//----------

IF l_Error = c_ValOk THEN
   IF l_Value <> Value THEN
      Set_Col_Data(i_RowNbr, i_ColNbr, Value)
      i_ColText = Value
      l_Error   = c_ValFixed
   END IF
ELSE

   //----------
   //  If there was a validation error, then use
   //  Display_DW_Val_Error() to display the validation
   //  error message.
   //----------

   IF l_Error = c_ValFailed THEN
      Display_DW_Val_Error(PCCA.MB.c_MBI_DW_ZipValError, "")
   END IF
END IF

RETURN l_Error
end function

public subroutine wire_button (uo_cb_main command_button);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Wire_Button
//  Description   : Wires a command button to this DataWindow.
//
//  Parameters    : UO_CB_MAIN Command_Button -
//                        The command button that it is to be
//                        wired to this DataWindow..
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Found
INTEGER  l_Idx

//----------
//  Make sure that we were passed a valid button to be wired.
//----------

IF IsValid(Command_Button) THEN

   //----------
   //  Make sure that this button is not already wired to another
   //  DataWindow.
   //----------

   l_Found = FALSE
   IF IsValid(Command_Button.i_TrigObject) THEN
      IF Command_Button.i_TrigObject = THIS THEN
         l_Found = TRUE
      ELSE
         Unwire_Button(Command_Button)
      END IF
   END IF

   //----------
   //  Look through our list of PowerClass controls to see if
   //  this new command button is already in the list.
   //----------

   IF NOT l_Found THEN
      FOR l_Idx = 1 TO i_NumPCControls
         IF i_PCControls[l_Idx] = Command_Button THEN
            l_Found = TRUE
            EXIT
         END IF
      NEXT

      //----------
      //  If we did not find the new command button in the list
      //  of PowerClass controls for this window, then append it.
      //----------

      IF NOT l_Found THEN
         i_NumPCControls               = i_NumPCControls + 1
         i_PCControls[i_NumPCControls] = Command_Button
      END IF

      //----------
      //  Set the command button's trigger DataWindow to this
      //  DataWindow.
      //----------

      Command_Button.i_TrigObject = THIS
   END IF

   //----------
   //  Enable the button if this DataWindow is in use.
   //----------

   Command_Button.Enabled = i_InUse
END IF

RETURN
end subroutine

public subroutine wire_filter (graphicobject filter_obj, string filter_column);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Wire_Filter
//  Description   : Wires a filter object to this DataWindow.
//
//  Parameters    : GRAPHICOBJECT Filter_Obj -
//                        The filter object that it is to be
//                        wired to this DataWindow.
//                  STRING Filter_Column -
//                        The column in the DataWindow that
//                        should be used for the filter.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN              l_Found
UO_DDDW_FILTER_MAIN  l_DDDWName
UO_DDLB_FILTER       l_DDLBName
UO_EM_FILTER         l_EMName
UO_LB_FILTER         l_LBName
UO_SLE_FILTER        l_SLEName

//----------
//  Make sure that we were passed a valid object to be wired.
//----------

IF IsValid(Filter_Obj) THEN

   //----------
   //  Filter objects can be any one of five different types:
   //  DDDW, DDLB, EM, LB, or SLE.  Figure out which type this
   //  filter object is and get and set the appropriate
   //  information.
   //----------

   l_Found = FALSE

   CHOOSE CASE Filter_Obj.Tag
      CASE "PowerObject Filter DDDW"
         l_DDDWName = Filter_Obj

         //----------
         //  Make sure that this object is not already wired to
         //  another DataWindow.
         //----------

         IF IsValid(l_DDDWName.i_PCFilterDW) THEN
            IF l_DDDWName.i_PCFilterDW = THIS THEN
               l_Found = TRUE
            ELSE
               Unwire_Filter(l_DDDWName)
            END IF
         END IF

         //----------
         //  Set up the filter object to point to the DataWindow,
         //  remember the database information, and enable the
         //  object if the DataWindow is in use.
         //----------

         l_DDDWName.fu_WireDW(THIS, Filter_Column, i_DBCA)
         l_DDDWName.i_PCFilterDW = THIS
         l_DDDWName.Enabled      = i_InUse
      CASE "PowerObject Filter DDLB"
         l_DDLBName = Filter_Obj

         //----------
         //  Make sure that this object is not already wired to
         //  another DataWindow.
         //----------

         IF IsValid(l_DDLBName.i_PCFilterDW) THEN
            IF l_DDLBName.i_PCFilterDW = THIS THEN
               l_Found = TRUE
            ELSE
               Unwire_Filter(l_DDLBName)
            END IF
         END IF

         //----------
         //  Set up the filter object to point to the DataWindow,
         //  remember the database information, and enable the
         //  object if the DataWindow is in use.
         //----------

         l_DDLBName.fu_WireDW(THIS, Filter_Column, i_DBCA)
         l_DDLBName.i_PCFilterDW = THIS
         l_DDLBName.Enabled      = i_InUse
      CASE "PowerObject Filter EM"
         l_EMName = Filter_Obj

         //----------
         //  Make sure that this object is not already wired to
         //  another DataWindow.
         //----------

         IF IsValid(l_EMName.i_PCFilterDW) THEN
            IF l_EMName.i_PCFilterDW = THIS THEN
               l_Found = TRUE
            ELSE
               Unwire_Filter(l_EMName)
            END IF
         END IF

         //----------
         //  Set up the filter object to point to the DataWindow,
         //  remember the database information, and enable the
         //  object if the DataWindow is in use.
         //----------

         l_EMName.fu_WireDW(THIS, Filter_Column, i_DBCA)
         l_EMName.i_PCFilterDW = THIS
         l_EMName.Enabled      = i_InUse
      CASE "PowerObject Filter LB"
         l_LBName = Filter_Obj

         //----------
         //  Make sure that this object is not already wired to
         //  another DataWindow.
         //----------

         IF IsValid(l_LBName.i_PCFilterDW) THEN
            IF l_LBName.i_PCFilterDW = THIS THEN
               l_Found = TRUE
            ELSE
               Unwire_Filter(l_LBName)
            END IF
         END IF

         //----------
         //  Set up the filter object to point to the DataWindow,
         //  remember the database information, and enable the
         //  object if the DataWindow is in use.
         //----------

         l_LBName.fu_WireDW(THIS, Filter_Column, i_DBCA)
         l_LBName.i_PCFilterDW = THIS
         l_LBName.Enabled      = i_InUse
      CASE "PowerObject Filter SLE"
         l_SLEName = Filter_Obj

         //----------
         //  Make sure that this object is not already wired to
         //  another DataWindow.
         //----------

         IF IsValid(l_SLEName.i_PCFilterDW) THEN
            IF l_SLEName.i_PCFilterDW = THIS THEN
               l_Found = TRUE
            ELSE
               Unwire_Filter(l_SLEName)
            END IF
         END IF

         //----------
         //  Set up the filter object to point to the DataWindow,
         //  remember the database information, and enable the
         //  object if the DataWindow is in use.
         //----------

         l_SLEName.fu_WireDW(THIS, Filter_Column, i_DBCA)
         l_SLEName.i_PCFilterDW = THIS
         l_SLEName.Enabled      = i_InUse
      CASE ELSE
         GOTO Finished
   END CHOOSE

   //----------
   //  Append the new object to the array of filter objects in
   //  this DataWindow.
   //----------

   IF NOT l_Found THEN
      i_NumFilterObjects = i_NumFilterObjects + 1
      i_FilterObjects[i_NumFilterObjects] = Filter_Obj
   END IF
END IF

Finished:

RETURN
end subroutine

public subroutine wire_filter_dw (uo_dw_filter filter_obj, integer num_filter_column, string filter_dw_column[], string filter_column[]);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Wire_Filter_DW
//  Description   : Wires a filter DW object to this DataWindow.
//
//  Parameters    : UO_DW_FILTER Filter_Obj -
//                        The filter object that it is to be
//                        wired to this DataWindow.
//
//                  INTEGER Num_Filter_Column -
//                        The number of filter columns that will
//                        be wired to this DataWindow.
//
//                  STRING Filter_DW_Column -
//                        The column in the filter-object
//                        DataWindow that should be used for the
//                        filter.
//
//                  STRING Filter_Column -
//                        The column in the filter DataWindow that
//                        should be used for the filter.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Found
INTEGER  l_Idx
STRING   l_DW_Col[], l_Col[]

//----------
//  Make sure that we were passed a valid object to be wiredF.
//----------

IF IsValid(Filter_Obj) THEN

   //----------
   //  Make sure that this object is not already wired to
   //  another DataWindow.
   //----------

   IF IsValid(Filter_Obj.i_PCFilterDW) THEN
      IF Filter_Obj.i_PCFilterDW = THIS THEN
         l_Found = TRUE
      ELSE
         Unwire_Filter(Filter_Obj)
      END IF
   END IF

   //----------
   //  Set up the filter object to point to the DataWindow,
   //  remember the database information, and enable the
   //  object if the DataWindow is in use.
   //----------

   FOR l_Idx = 1 TO Num_Filter_Column
      l_DW_Col[l_Idx] = Filter_DW_Column[l_Idx]
      l_Col[l_Idx]    = Filter_Column[l_Idx]
   NEXT

   Filter_Obj.fu_WireDW(l_DW_Col[], THIS, l_Col[], i_DBCA)
   Filter_Obj.i_PCFilterDW = THIS
   Filter_Obj.Enabled      = i_InUse

   //----------
   //  Append the new object to the array of filter objects in
   //  this DataWindow.
   //----------

   IF NOT l_Found THEN
      i_NumFilterObjects = i_NumFilterObjects + 1
      i_FilterObjects[i_NumFilterObjects] = Filter_Obj
   END IF
END IF

RETURN
end subroutine

public subroutine wire_find (uo_dw_find find_dw, string find_column);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Wire_Find
//  Description   : Wires a find object to this DataWindow.
//
//  Parameters    : UO_DW_FIND Find_DW -
//                        The find object that it is to be
//                        wired to this DataWindow.
//                  STRING Find_Column -
//                        The column in the DataWindow that
//                        the find object should search through.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Found

//----------
//  Make sure that we were passed a valid object to be wired.
//----------

IF IsValid(Find_DW) THEN

   //----------
   //  Make sure that this object is not already wired to another
   //  DataWindow.
   //----------

   l_Found = FALSE
   IF IsValid(Find_DW.i_FindDW) THEN
      IF Find_DW.i_FindDW = THIS THEN
         l_Found = TRUE
      ELSE
         Unwire_Find(Find_DW)
      END IF
   END IF

   //----------
   //  Append the new object to the array of find objects in this
   //  DataWindow and set the find object to point at this
   //  DataWindow.
   //----------

   IF NOT l_Found THEN
      i_NumFindObjects                = i_NumFindObjects + 1
      i_FindObjects[i_NumFindObjects] = Find_DW
      Find_DW.i_FindDW                = THIS
   END IF

   //----------
   //  Remember the DataWindow column for searching.
   //----------

   Find_DW.i_FindColumn = Find_Column
   Find_DW.i_GotOrder   = FALSE

   //----------
   //  Enable the find object if this DataWindow is in use.
   //----------

   Find_DW.Enabled = i_InUse

   //----------
   //  Insert a empty row into the find object so that the user
   //  has a row to type in.  Also, set the focus indicator off.
   //----------

   IF Find_DW.RowCount() = 0 THEN
      Find_DW.InsertRow(0)
      Find_DW.SetRowFocusIndicator(Off!)
   END IF
END IF

RETURN
end subroutine

public subroutine wire_message (uo_cb_message messagecb, string message_column, string message_title);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Wire_Message
//  Description   : Wires a message button to this DataWindow.
//
//  Parameters    : UO_CB_MESSAGE MessageCB -
//                        The message button that it is to be
//                        wired to this DataWindow.
//                  STRING Message_Column -
//                        The column in the DataWindow that
//                        the message button should edit.
//                  STRING Message_Title -
//                        The title for the window when the
//                        column is being edited.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Found

//----------
//  Make sure that we were passed a valid object to be wired.
//----------

IF IsValid(MessageCB) THEN

   //----------
   //  Make sure that this object is not already wired to another
   //  DataWindow.
   //----------

   l_Found = FALSE
   IF IsValid(MessageCB.i_MessageDW) THEN
      IF MessageCB.i_MessageDW = THIS THEN
         l_Found = TRUE
      ELSE
         MessageCB.i_MessageDW.Unwire_Message(MessageCB)
      END IF
   END IF

   //----------
   //  Append the new object to the array of message buttons in
   //  this DataWindow and set the message button to point at
   //  this DataWindow.
   //----------

   IF NOT l_Found THEN
      i_NumMessages             = i_NumMessages + 1
      i_Messages[i_NumMessages] = MessageCB
      MessageCB.i_MessageDW     = THIS
   END IF

   //----------
   //  Remember the DataWindow column for editing and remember
   //  the title to be used during editing.
   //----------

   MessageCB.i_MessageTitle  = Message_Title
   MessageCB.i_MessageColumn = Message_Column

   //----------
   //  Enable the message button if this DataWindow is in use.
   //----------

   MessageCB.Enabled = i_InUse
END IF

RETURN
end subroutine

public subroutine wire_search (graphicobject search_obj, string search_table, string search_column);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Wire_Search
//  Description   : Wires a search object to this DataWindow.
//
//  Parameters    : GRAPHICOBJECT Search_Obj -
//                        The search object that it is to be
//                        wired to this DataWindow.
//                  STRING Search_Table -
//                        The table of the column in the
//                        database that should be used for
//                        the search.
//                  STRING Search_Column -
//                        The column in the database that should
//                        be used for the search.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN              l_Found
UO_DDDW_SEARCH_MAIN  l_DDDWName
UO_DDLB_SEARCH       l_DDLBName
UO_EM_SEARCH         l_EMName
UO_LB_SEARCH         l_LBName
UO_SLE_SEARCH        l_SLEName

//----------
//  Make sure that we were passed a valid object to be wired.
//----------

IF IsValid(Search_Obj) THEN

   //----------
   //  Search objects can be any one of five different types:
   //  DDDW, DDLB, EM, LB, or SLE.  Figure out which type this
   //  search object is and get and set the appropriate
   //  information.
   //----------

   l_Found = FALSE

   CHOOSE CASE Search_Obj.Tag
      CASE "PowerObject Search DDDW"
         l_DDDWName = Search_Obj

         //----------
         //  Make sure that this object is not already wired to
         //  another DataWindow.
         //----------

         IF IsValid(l_DDDWName.i_PCSearchDW) THEN
            IF l_DDDWName.i_PCSearchDW = THIS THEN
               l_Found = TRUE
            ELSE
               Unwire_Search(l_DDDWName)
            END IF
         END IF

         //----------
         //  Set up the search object to point to the DataWindow,
         //  remember the database information, and enable the
         //  object if the DataWindow is in use.
         //----------

         l_DDDWName.fu_WireDW(THIS,          Search_Table, &
                              Search_Column, i_DBCA)
         l_DDDWName.i_PCSearchDW = THIS
         l_DDDWName.Enabled      = i_InUse
      CASE "PowerObject Search DDLB"
         l_DDLBName = Search_Obj

         //----------
         //  Make sure that this object is not already wired to
         //  another DataWindow.
         //----------

         IF IsValid(l_DDLBName.i_PCSearchDW) THEN
            IF l_DDLBName.i_PCSearchDW = THIS THEN
               l_Found = TRUE
            ELSE
               Unwire_Search(l_DDLBName)
            END IF
         END IF

         //----------
         //  Set up the search object to point to the DataWindow,
         //  remember the database information, and enable the
         //  object if the DataWindow is in use.
         //----------

         l_DDLBName.fu_WireDW(THIS,          Search_Table, &
                              Search_Column, i_DBCA)
         l_DDLBName.i_PCSearchDW = THIS
         l_DDLBName.Enabled      = i_InUse
      CASE "PowerObject Search EM"
         l_EMName = Search_Obj

         //----------
         //  Make sure that this object is not already wired to
         //  another DataWindow.
         //----------

         IF IsValid(l_EMName.i_PCSearchDW) THEN
            IF l_EMName.i_PCSearchDW = THIS THEN
               l_Found = TRUE
            ELSE
               Unwire_Search(l_EMName)
            END IF
         END IF

         //----------
         //  Set up the search object to point to the DataWindow,
         //  remember the database information, and enable the
         //  object if the DataWindow is in use.
         //----------

         l_EMName.fu_WireDW(THIS,          Search_Table, &
                            Search_Column, i_DBCA)
         l_EMName.i_PCSearchDW = THIS
         l_EMName.Enabled      = i_InUse
      CASE "PowerObject Search LB"
         l_LBName = Search_Obj

         //----------
         //  Make sure that this object is not already wired to
         //  another DataWindow.
         //----------

         IF IsValid(l_LBName.i_PCSearchDW) THEN
            IF l_LBName.i_PCSearchDW = THIS THEN
               l_Found = TRUE
            ELSE
               Unwire_Search(l_LBName)
            END IF
         END IF

         //----------
         //  Set up the search object to point to the DataWindow,
         //  remember the database information, and enable the
         //  object if the DataWindow is in use.
         //----------

         l_LBName.fu_WireDW(THIS,          Search_Table, &
                            Search_Column, i_DBCA)
         l_LBName.i_PCSearchDW = THIS
         l_LBName.Enabled      = i_InUse
      CASE "PowerObject Search SLE"
         l_SLEName = Search_Obj

         //----------
         //  Make sure that this object is not already wired to
         //  another DataWindow.
         //----------

         IF IsValid(l_SLEName.i_PCSearchDW) THEN
            IF l_SLEName.i_PCSearchDW = THIS THEN
               l_Found = TRUE
            ELSE
               Unwire_Search(l_SLEName)
            END IF
         END IF

         //----------
         //  Set up the search object to point to the DataWindow,
         //  remember the database information, and enable the
         //  object if the DataWindow is in use.
         //----------

         l_SLEName.fu_WireDW(THIS,          Search_Table, &
                             Search_Column, i_DBCA)
         l_SLEName.i_PCSearchDW = THIS
         l_SLEName.Enabled      = i_InUse
      CASE ELSE
         GOTO Finished
   END CHOOSE

   //----------
   //  Append the new object to the array of search objects in
   //  this DataWindow.
   //----------

   IF NOT l_Found THEN
      i_NumSearchObjects = i_NumSearchObjects + 1
      i_SearchObjects[i_NumSearchObjects] = Search_Obj
   END IF
END IF

Finished:

RETURN
end subroutine

public subroutine wire_search_dw (uo_dw_search search_obj, integer num_search_column, string search_dw_column[], string search_table[], string search_column[]);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Wire_Search_DW
//  Description   : Wires a search object to this DataWindow.
//
//  Parameters    : UO_DW_SEARCH Search_Obj -
//                        The search object that it is to be
//                        wired to this DataWindow.
//
//                  INTEGER Num_Search_Column -
//                        The number of search columns that will
//                        be wired to this DataWindow.
//
//                  STRING Search_DW_Column -
//                        The column in the search-object
//                        DataWindow that should be used for the
//                        search.
//
//                  STRING Search_Table -
//                        The table of the column in the
//                        database that should be used for
//                        the search.
//
//                  STRING Search_Column -
//                        The column in the database that should
//                        be used for the search.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Found
INTEGER  l_Idx
STRING   l_DW_Col[], l_Tbl[], l_Col[]

//----------
//  Make sure that we were passed a valid object to be wired.
//----------

IF IsValid(Search_Obj) THEN

   l_Found = FALSE

   //----------
   //  Make sure that this object is not already wired to
   //  another DataWindow.
   //----------

   IF IsValid(Search_Obj.i_PCSearchDW) THEN
      IF Search_Obj.i_PCSearchDW = THIS THEN
         l_Found = TRUE
      ELSE
         Unwire_Search(Search_Obj)
      END IF
   END IF

   //----------
   //  Set up the search object to point to the DataWindow,
   //  remember the database information, and enable the
   //  object if the DataWindow is in use.
   //----------

   FOR l_Idx = 1 TO Num_Search_Column
      l_DW_Col[l_Idx] = Search_DW_Column[l_Idx]
      l_Tbl[l_Idx]    = Search_Table[l_Idx]
      l_Col[l_Idx]    = Search_Column[l_Idx]
   NEXT

   Search_Obj.fu_WireDW(l_DW_Col[], THIS, l_Tbl[], l_Col[], i_DBCA)
   Search_Obj.i_PCSearchDW = THIS
   Search_Obj.Enabled      = i_InUse

   //----------
   //  Append the new object to the array of search objects in
   //  this DataWindow.
   //----------

   IF NOT l_Found THEN
      i_NumSearchObjects = i_NumSearchObjects + 1
      i_SearchObjects[i_NumSearchObjects] = Search_Obj
   END IF
END IF

RETURN
end subroutine

public subroutine wire_share (uo_dw_main secondary_dw, unsignedlong share_type);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Wire_Share
//  Description   : Wires a shared DataWindow to this DataWindow.
//
//  Parameters    : UO_DW_MAIN Secondary_DW -
//                        The DataWindow who wants to set up a
//                        share with this DataWindow.
//
//                  UNSIGNEDLONG Share_Type -
//                        The type of share to be setup.  Valid
//                        options are:
//
//                           - c_ShareEnabled
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Error
LONG          l_Selected[]
UNSIGNEDLONG  l_SaveOpenChild

IF i_ShareType <> c_ShareNone THEN
   i_SharePrimary.Wire_Share(Secondary_DW, Share_Type)
   Secondary_DW.i_ShareParent = THIS
ELSE

   //----------
   //  We may need to set ourselves up if this is the first share.
   //----------

   IF i_NumShareSecondary = 0 THEN
      i_DoEmpty = i_ShowEmpty

      IF i_IsEmpty        THEN
      IF NOT i_AddedEmpty THEN
      IF i_DoEmpty        THEN
         i_DWState = c_DWStateUndefined
         TriggerEvent("pcd_Disable")
      END IF
      END IF
      END IF
   END IF

   Secondary_DW.Push_DW_Redraw(c_BufferDraw)

   //----------
   //  Before we set up the secondary DataWindow, make sure that it
   //  is not already doing a share with this DataWindow.
   //----------

   IF Secondary_DW.i_SharePrimary <> THIS THEN

      IF Secondary_DW.i_ShareType <> c_ShareNone THEN
         Secondary_DW.i_SharePrimary.Unwire_Share(Secondary_DW)
      END IF

      i_NumShareSecondary = i_NumShareSecondary + 1
      i_ShareSecondary[i_NumShareSecondary] = Secondary_DW

      //----------
      //  Initialize values for the Secondary_DW.
      //----------

      Secondary_DW.i_SharePrimary = THIS
      Secondary_DW.i_ShareType    = Share_Type
      Secondary_DW.i_ShareParent  = THIS

      //----------
      //  Setting up the share may cause an RFC to get triggered.
      //----------

      Share_State(c_SharePushRFC,     c_IgnoreRFC)
      Share_State(c_SharePushVScroll, c_IgnoreVScroll)
      l_Error = ShareData(Secondary_DW)
      IF l_Error <> 1 THEN
         PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Wire_Share()"
         PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
         PCCA.MB.i_MB_Strings[3] = i_ClassName
         PCCA.MB.i_MB_Strings[4] = i_Window.Title
         PCCA.MB.i_MB_Strings[5] = DataObject
         PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_BadShare, &
                            0, PCCA.MB.i_MB_Numbers[], &
                            5, PCCA.MB.i_MB_Strings[])
      END IF
      Share_State(c_SharePopVScroll, c_StateUnused)
      Share_State(c_SharePopRFC,     c_StateUnused)

      //----------
      //  Now that Secondary_DW has its share, we need to tell it
      //  about some states.
      //----------

      Secondary_DW.i_IsEmpty    = i_IsEmpty
      Secondary_DW.i_AddedEmpty = i_AddedEmpty
      Secondary_DW.TriggerEvent("pcd_SetRowIndicator")

      IF Secondary_DW.i_SetEmptyColors <> i_IsEmpty THEN
         Secondary_DW.i_SetEmptyColors = i_IsEmpty
         IF Secondary_DW.i_SetEmptyColors THEN
            Secondary_DW.Modify(Secondary_DW.i_EmptyTextColors)
         ELSE
            Secondary_DW.Modify(Secondary_DW.i_NonEmptyTextColors)
         END IF
      END IF

      Secondary_DW.Set_Selected_Rows(0, l_Selected[],     &
                                     c_IgnoreChanges,     &
                                     c_NoRefreshChildren, &
                                     c_RefreshView)

      IF NOT Secondary_DW.i_IsEmpty THEN

         //----------
         //  We don't want any children DataWindows getting
         //  popped open, so use Set_Open_Child()
         //----------

         l_SaveOpenChild   = i_OpenChildOption
         i_OpenChildOption = c_OpenChildPrevent

         //----------
         //  Move to the first row.
         //----------

         Secondary_DW.i_RFCEvent = c_RFC_RowFocusChanged
         Secondary_DW.i_MoveRow  = 1
         Secondary_DW.TriggerEvent(RowFocusChanged!)

         //----------
         //  Restore the open child option to its original state.
         //----------

         i_OpenChildOption = l_SaveOpenChild
      END IF

      Secondary_DW.is_EventControl.Control_Mode = c_ControlRows
      Secondary_DW.TriggerEvent("pcd_SetControl")
   END IF

   Secondary_DW.Pop_DW_Redraw()
END IF

RETURN
end subroutine

public subroutine add_reselection (string reselect_keys[]);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Add_Reselection
//  Description   : Provides the developer the opportunity
//                  to have rows re-selected when a parent
//                  DataWindow is refreshed by its children.
//                  The number of keys passed must match the
//                  number of key columns in the DataWindow.
//
//  Parameters    : STRING Reselect_Keys[] -
//                        The set of keys that  the
//                        row be selected.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

LONG  l_NumReselectKeys, l_Idx

l_NumReselectKeys = i_NumReselectRows * i_xNumKeyColumns
i_NumReselectRows = i_NumReselectRows + 1

FOR l_Idx = 1 TO i_xNumKeyColumns
   l_NumReselectKeys = l_NumReselectKeys + 1
   i_ReselectKeys[l_NumReselectKeys] = Reselect_Keys[l_Idx]
NEXT

RETURN
end subroutine

public function integer ask_user (integer num_modified_dws, uo_dw_main modified_dws[]);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Ask_User
//  Description   : Asks the user if they want to save the
//                  changes in the specified DataWindows.
//
//  Parameters    : INTEGER Num_Modified_DWS -
//                        The number of DataWindows that contain
//                        changes.
//
//                  UO_DW_MAIN Modified_DWS[] -
//                        The DataWindows that contain changes.
//
//  Return Value  : INTEGER -
//                        The return value from the
//                        fu_MessageBox() function.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_PCCAValid, l_Raised
INTEGER       l_Return,    l_Idx
UNSIGNEDLONG  l_MBICode
WINDOWSTATE   l_WindowState[]
WINDOW        l_CurWin
UO_DW_MAIN    l_CurDW,     l_TmpDW

//----------
//  There are changes and we need to find out the user's response
//  (save, abort, or cancel).  To do this, we highlight all of the
//  DataWindows with changes and then display a message box asking
//  for the user's response.  Start by setting up some local
//  variables.  l_Raised tells us if any window had to be
//  normalized.  l_CurWin and l_CurDW save the current window and
//  DataWindows because they may be modified when windows are
//  normalized.
//----------

l_Raised    = FALSE
l_CurWin    = PCCA.Window_Current
l_CurDW     = PCCA.Window_CurrentDW
l_PCCAValid = IsValid(l_CurDW)

//----------
//  Tell the current DataWindow to turn off its highlight.
//----------

IF l_PCCAValid THEN
   l_CurDW.is_EventControl.Highlight_Only = TRUE
   l_CurDW.TriggerEvent("pcd_Inactive")
END IF

//----------
//  Cycle through every DataWindow that contains modifications.
//----------

FOR l_Idx = 1 TO Num_Modified_DWs
   l_TmpDW = Modified_DWs[l_Idx]

   //----------
   //  Remember WindowState of every window that contains a
   //  modified DataWindow.
   //----------

   l_WindowState[l_Idx] = l_TmpDW.i_Window.WindowState

   //----------
   //  If the window's state is not Normal!, we need to make it
   //  Normal! so that the user can see which DataWindows have been
   //  modified.  If we do change the WindowState, set the l_Raised
   //  flag to TRUE to indicate that we will have to undo the
   //  WindowState! changes.
   //----------

   IF l_TmpDW.i_Window.WindowState <> Normal! THEN
      IF Num_Modified_DWs             >  1 OR &
         l_TmpDW.i_Window.WindowState <> Maximized! THEN
         PCCA.Do_Event                = TRUE
         l_TmpDW.i_Window.WindowState = Normal!
         PCCA.Do_Event                = FALSE
         l_Raised                     = TRUE
      END IF
   END IF

   //----------
   //  Highlight the DataWindow to indicate that it contains
   //  changes.
   //----------

   l_TmpDW.is_EventControl.Highlight_Only = TRUE
   l_TmpDW.TriggerEvent("pcd_Active")
NEXT

//----------
//  All of the DataWindows that contain changes have been raised
//  and highlighted.  Ask the user what they want to do.
//----------

IF i_InactiveDWColorIdx <> c_ColorUndefined THEN
   IF Num_Modified_DWs = 1 THEN
      l_MBICode = PCCA.MB.c_MBI_DW_OneHighlightAnyChanges
   ELSE
      l_MBICode = PCCA.MB.c_MBI_DW_HighlightAnyChanges
   END IF
ELSE
   IF Num_Modified_DWs = 1 THEN
      l_MBICode = PCCA.MB.c_MBI_DW_OneAnyChanges
   ELSE
      l_MBICode = PCCA.MB.c_MBI_DW_AnyChanges
   END IF
END IF

PCCA.MB.i_MB_Numbers[1] = Num_Modified_DWs
PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Ask_User()"
PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
PCCA.MB.i_MB_Strings[3] = i_ClassName
PCCA.MB.i_MB_Strings[4] = i_RootDW.i_Window.Title
PCCA.MB.i_MB_Strings[5] = i_RootDW.DataObject
l_Return = PCCA.MB.fu_MessageBox(l_MBICode,              &
                              1, PCCA.MB.i_MB_Numbers[], &
                              5, PCCA.MB.i_MB_Strings[])

//----------
//  If there is a current DataWindow, tell it to buffer its
//  redraws.
//----------

IF l_PCCAValid THEN
   l_CurDW.Push_DW_Redraw(c_BufferDraw)
END IF

//----------
//  Cycle through each of the modified DataWindows.
//----------

FOR l_Idx = 1 TO Num_Modified_DWs
   l_TmpDW = Modified_DWs[l_Idx]

   //----------
   //  If we modified the WindowState, we need to put it back the
   //  way it was when we found it.
   //----------

   IF l_WindowState[l_Idx] <> Normal! THEN
      PCCA.Do_Event                = TRUE
      l_TmpDW.i_Window.WindowState = l_WindowState[l_Idx]
      PCCA.Do_Event                = FALSE
   END IF

   //----------
   //  Turn off the highlight in all of the modified DataWindows.
   //----------

   l_TmpDW.is_EventControl.Highlight_Only = TRUE
   l_TmpDW.TriggerEvent("pcd_Inactive")
NEXT

//----------
//  If we had to change the Window State for any of the modified
//  windows, focus may have been shifted to the wrong one.  We
//  need to set it back to the proper DataWindow.
//----------

IF l_Raised THEN

   //----------
   //  Tell the DataWindow which was current when we entered
   //  pcd_AnyChanges to make itself current.
   //----------

   IF l_PCCAValid THEN
      l_CurDW.TriggerEvent("pcd_Active")
   END IF

   //----------
   //  See if there was a current window when we entered this
   //  event.
   //----------

   IF IsValid(l_CurWin) THEN

      //----------
      //  If the current window is not iconizied, then we need to
      //  change focus to the current DataWindow (if any).
      //----------

      IF l_CurWin.WindowState <> Minimized! THEN

         //----------
         //  See if there was a current DataWindow before we
         //  entered this event.
         //----------

         IF l_PCCAValid THEN

            //----------
            //  If the current DataWindow is on the same window,
            //  then use Change_DW_Focus() to raise the window
            //  and set the current DataWindow.  If the current
            //  DataWindow was on a different window, just set
            //  focus to the window.
            //----------

            IF l_CurWin = l_CurDW.i_Window THEN
               Change_DW_Focus(l_CurDW)
            ELSE
               l_CurWin.SetFocus()
            END IF
         ELSE

            //----------
            //  There is not a current DataWindow - just set
            //  focus to the current window.
            //----------

            l_CurWin.SetFocus()
         END IF
      END IF
   ELSE

      //----------
      //  There was not a current window when we entered this
      //  event.  If the current DataWindow is not on a window
      //  that is iconized, then raise the window and set focus
      //  to the current DataWindow.
      //----------

      IF l_PCCAValid THEN
      IF l_CurDW.i_Window.WindowState <> Minimized! THEN
         Change_DW_Focus(l_CurDW)
      END IF
      END IF
   END IF
  END IF

//----------
//  Restore the highlight of the current DataWindow and restore
//  redraw processing to its previous state.
//----------

IF l_PCCAValid THEN
   l_CurDW.is_EventControl.Highlight_Only = TRUE
   l_CurDW.TriggerEvent("pcd_Active")
   l_CurDW.Pop_DW_Redraw()
END IF

RETURN l_Return
end function

public subroutine build_changed_list (ref integer num_modified_dws, ref uo_dw_main modified_dws[]);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Build_Changed_List
//  Description   : Internal routine.  Used to gather all
//                  DataWindows that have been changed in
//                  multiple hierarchies.
//
//  Parameters    : ref INTEGER  Num_Modified_DWs -
//                        The number of DataWindows which have
//                        been found that contain modifications
//                        Used as both and input and output
//                        parameter.
//
//                  ref UO_DW_MAIN  Modified_DWs[] -
//                        The DataWindows which have been found
//                        that contain modifications Used as both
//                        and input and output parameter.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN     l_Found
INTEGER     l_Idx
UO_DW_MAIN  l_ChildDW

//----------
//  Make sure that the i_Modified flag is up to date.
//----------

Update_Modified()

//----------
//  Do the normal children.
//----------

FOR l_Idx = 1 TO i_NumChildren
   l_ChildDW = i_ChildDW[l_Idx]
   l_ChildDW.Build_Changed_List(Num_Modified_DWs, Modified_DWs[])
NEXT

FOR l_Idx = 1 TO i_NumInstance
   l_ChildDW = i_ChildDW[l_Idx]
   l_ChildDW.Build_Changed_List(Num_Modified_DWs, Modified_DWs[])
NEXT

IF i_Modified THEN

   //----------
   //  Make sure that this DataWindow is not already in the list.
   //----------

   l_Found = FALSE
   FOR l_Idx = 1 TO Num_Modified_DWs
      IF THIS = Modified_DWs[l_Idx] THEN
         l_Found = TRUE
         EXIT
      END IF
   NEXT

   //----------
   //  Not already in the list.
   //----------

   IF NOT l_Found THEN
      Num_Modified_DWs               = Num_Modified_DWs + 1
      Modified_DWs[Num_Modified_DWs] = THIS
   END IF
END IF

end subroutine

public function integer check_dw_save (unsignedlong do_refresh);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Check_DW_Save
//  Description   : Checks the DataWindows for modifications.  If
//                  there are modifications, the user will be
//                  asked if they want save or drop the changes,
//                  or cancel the operation.  If the user requests
//                  that the modifications be saved, pcd_Save will
//                  be triggered.  Do_Refresh specifies if the
//                  DataWindows are to be refreshed after the save.
//                  PCCA.Error is always reset to c_Success by this
//                  routine.
//
//  Parameters    : UNSIGNEDLONG Do_Refresh -
//                        Specifies if the DataWindows are to
//                        retrieved after the save.
//
//                           - c_DoRefresh
//                           - c_NoRefresh
//
//  Return Value  : INTEGER -
//                        Error return - c_Success or c_Fatal.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

l_Error = Save_DW(c_PromptUser, Do_Refresh)

RETURN l_Error
end function

public function integer check_modifications (unsignedlong do_any_changes);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Check_Modifications
//  Description   : Prepares the DataWindow for mode switchs or
//                  loading of new rows.
//
//  Parameters    : UNSIGNEDLONG Do_Any_Changes -
//                        Specifies if the children DataWindows
//                        will be checked for changes before the
//                        rows are loaded.  If there are changes
//                        that the user or developer wishes to
//                        save, they will be saved and then the
//                        new rows will be loaded.
//
//                           - c_CheckForChanges
//                           - c_IgnoreChanges
//                           - c_SaveChanges
//
//  Return Value  : INTEGER -
//                       Error return - c_Success or c_Fatal
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Error, l_IsModified
UNSIGNEDLONG  l_RefreshCmd

//----------
//  This may take a little while...
//----------

SetPointer(HourGlass!)

//----------
//  Clear PCCA.Error to indicate that there are no errors.
//----------

PCCA.Error = c_Success

//----------
//  Load_Rows() will load any selections that user has made that
//  have not yet been loaded (e.g. multiple selections are
//  generally not loaded everytime the user adds to their
//  multi-select list).
//----------

IF PCCA.PCMGR.i_DW_NeedRetrieve THEN
   IF Load_Rows(PCCA.Null_Object, PCCA.Null_Object) <> &
      c_Success THEN
      PCCA.Error = c_Fatal
      GOTO Finished
   END IF
END IF

//----------
//  Set l_RefreshCmd to indicate that refreshing is not needed.
//  If there are changes, l_RefreshCmd will tell us how to
//  refresh after the changes have been processed (e.g. if the
//  user aborts changes, then the modified DataWindows need to
//  have rows re-loaded from the database).
//----------

l_RefreshCmd = c_RefreshUndefined

//----------
//  See if there are modifications in this DataWindow or its
//  children.
//----------

IF Check_DW_Modified(c_CheckChildren) THEN
   CHOOSE CASE Do_Any_Changes

      //----------
      //  The developer wanted the user to be prompted for changes
      //  before new rows are loaded.
      //----------

      CASE c_CheckForChanges

         //----------
         //  Check with the user for changes.
         //----------

         Check_Save(l_RefreshCmd)

         //----------
         //  If PCCA.Error is not c_Success, then something went
         //  wrong in Check_Save() (e.g. the user canceled or
         //  there was a validation error).
         //----------

         IF PCCA.Error <> c_Success THEN

            //----------
            //  See if there was a refresh mode specified by
            //  Check_Save().  If there was, we need to use
            //  the mode to to refresh from the root-level
            //  DataWindow.
            //----------

            IF l_RefreshCmd <> c_RefreshUndefined THEN
               i_RootDW.is_EventControl.Refresh_Cmd = l_RefreshCmd
               i_RootDW.TriggerEvent("pcd_Refresh")
               PCCA.Error = c_Fatal
            END IF

            GOTO Finished
         END IF

      //----------
      //  The developer wanted any changes to be dropped and the
      //  new rows loaded.
      //----------

      CASE c_IgnoreChanges

//         l_RefreshCmd = c_RefreshSame

         is_EventControl.Clear_Cancel     = TRUE
         IF NOT i_InQuery AND NOT i_InView THEN
            is_EventControl.Only_Do_Children = TRUE
         END IF
         TriggerEvent("pcd_AnyChanges")

      //----------
      //  The developer wanted any changes to be saved.
      //----------

      CASE c_SaveChanges

         l_RefreshCmd = c_RefreshSave

         i_RootDW.is_EventControl.No_Refresh_After_Save = TRUE
         i_RootDW.TriggerEvent("pcd_Save")

         //----------
         //  If PCCA.Error is not c_Success, then something went
         //  wrong during the save process.
         //----------

         IF PCCA.Error <> c_Success THEN
            GOTO Finished
         END IF

      CASE ELSE
   END CHOOSE
END IF

//----------
//  Changes (if any) have been saved.  If l_RefreshCmd contains a
//  refresh mode, then refresh from the root-level DataWindow down
//  to this DataWindow using that specified refresh mode.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")
END IF

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

Finished:

//----------
//  Set the return value and clear PCCA.Error.
//----------

l_Error    = PCCA.Error
PCCA.Error = c_Success

RETURN l_Error
end function

public subroutine check_save (ref unsignedlong refresh_cmd);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Check_Save
//  Description   : Check for changes and saves if neccessary.
//
//  Parameters    : ref UNSIGNEDLONG Refresh_Cmd -
//                         Return value indicating if and how
//                         refreshing needs to be done after
//                         changes have been saved or aborted.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Initially, nothing needs to be refreshed.
//----------

Refresh_Cmd = c_RefreshUndefined

//----------
//  Check for changes from this DataWindow down.  There may be
//  changes in the DataWindows above us, but we don't care at
//  this point.  We are only interested in children DataWindows
//  because they may get overwritten by pcd_Refresh (i.e.
//  retrieve).  Note that whoever called may have set the
//  is_EventControl.Only_Do_Children.
//----------

is_EventControl.Check_Quiet = TRUE
TriggerEvent("pcd_AnyChanges")

//----------
//  If PCCA.Error is not c_Success, then there are changes.  We
//  need to trigger pcd_AnyChanges again on the root-level
//  DataWindow.  This will find all DataWindows which contain
//  changes and prompt the user.
//----------

IF PCCA.Error <> c_Success THEN
   i_RootDW.TriggerEvent("pcd_AnyChanges")
END IF

//----------
//  Take appropriate action based on the user's response (if there
//  are changes).
//----------

CHOOSE CASE PCCA.Error

   //----------
   //  Save the changes to all the DataWindows in this group.
   //----------

   CASE c_Yes

      //----------
      //  The DataWindow tree will need to be refreshed using the
      //  "SAVE" refresh mode.
      //----------

      Refresh_Cmd = c_RefreshSave

      //----------
      //  The caller of this routine always has responsibility for
      //  refreshing - tell pcd_Save that it is should not refresh.
      //----------

      i_RootDW.is_EventControl.No_Refresh_After_Save = TRUE
      i_RootDW.TriggerEvent("pcd_Save")

      //----------
      //  Check for errors.  If the error was due to a validation
      //  problem, the refresh should not be done.  However, if it
      //  was due to a problem while saving, then set the refresh
      //  mode to "SAME".
      //----------

      IF PCCA.Error <> c_Success THEN
         Refresh_Cmd = c_RefreshUndefined
      END IF

   //----------
   //  Don't save changes made to the DataWindows.
   //----------

   CASE c_No

      //----------
      //  The DataWindow tree will need to be refreshed using the
      //  "SAME" refresh mode.  Reset PCCA.Error as everything is
      //  Ok.
      //----------

      Refresh_Cmd = c_RefreshSame
      PCCA.Error  = c_Success

   //----------
   //  Cancel the command in progress.
   //----------

   CASE c_Fatal

   //----------
   //  No changes were made.
   //----------

   CASE c_Success
   CASE ELSE
END CHOOSE

RETURN
end subroutine

public subroutine config_menus ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Config_Menus
//  Description   : Enables menu items based on the capabilities
//                  of the DataWindow.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_ModeSep, l_SearchSep, l_DoView, l_DoDelete

IF i_AutoConfigMenus THEN

   IF NOT i_FileMenuInit THEN
       Set_DW_FMenu(i_FileMenu)
   END IF
   IF NOT i_EditMenuInit THEN
       Set_DW_EMenu(i_EditMenu)
   END IF
   IF NOT i_PopupMenuInit THEN
       Set_DW_PMenu(i_PopupMenu)
   END IF

   IF i_FileMenuCloseNum > 0 THEN
      IF NOT i_FileMenuClose.Visible THEN
         i_FileMenuClose.Visible = TRUE
      END IF
      IF i_Window.ToolBarVisible THEN
         IF NOT i_FileMenuClose.ToolBarItemVisible THEN
            i_FileMenuClose.ToolBarItemVisible = TRUE
         END IF
      END IF
   END IF
   
   IF i_FileMenuFileSepNum > 0 THEN
      IF NOT i_FileMenuFileSep.Visible THEN
         i_FileMenuFileSep.Visible = TRUE
      END IF
   END IF

   IF i_FileMenuSaveNum > 0 THEN
      IF NOT i_FileMenuSave.Visible THEN
         i_FileMenuSave.Visible = TRUE
      END IF
      IF i_Window.ToolBarVisible THEN
         IF NOT i_FileMenuSave.ToolBarItemVisible THEN
            i_FileMenuSave.ToolBarItemVisible = TRUE
         END IF
      END IF
   END IF

   IF i_FileMenuSaveRowsAsNum > 0 THEN
      IF NOT i_FileMenuSaveRowsAs.Visible THEN
         i_FileMenuSaveRowsAs.Visible = TRUE
      END IF
   END IF

   IF i_FileMenuPrintSepNum > 0 THEN
      IF NOT i_FileMenuPrintSep.Visible THEN
         i_FileMenuPrintSep.Visible = TRUE
      END IF
   END IF

   IF i_FileMenuPrintNum > 0 THEN
      IF NOT i_FileMenuPrint.Visible THEN
         i_FileMenuPrint.Visible = TRUE
      END IF
   END IF

   IF i_EditMenuIsValid THEN
      i_EditMenu.Enabled = TRUE
   END IF

   IF i_EditMenuFirstNum > 0 THEN
      IF NOT i_EditMenuFirst.Visible THEN
         i_EditMenuFirst.Visible = TRUE
      END IF
      IF i_Window.ToolBarVisible THEN
         IF NOT i_EditMenuFirst.ToolBarItemVisible THEN
            i_EditMenuFirst.ToolBarItemVisible = TRUE
         END IF
      END IF
   END IF

   IF i_EditMenuPrevNum > 0 THEN
      IF NOT i_EditMenuPrev.Visible THEN
         i_EditMenuPrev.Visible = TRUE
      END IF
      IF i_Window.ToolBarVisible THEN
         IF NOT i_EditMenuPrev.ToolBarItemVisible THEN
            i_EditMenuPrev.ToolBarItemVisible = TRUE
         END IF
      END IF
   END IF

   IF i_EditMenuNextNum > 0 THEN
      IF NOT i_EditMenuNext.Visible THEN
         i_EditMenuNext.Visible = TRUE
      END IF
      IF i_Window.ToolBarVisible THEN
         IF NOT i_EditMenuNext.ToolBarItemVisible THEN
            i_EditMenuNext.ToolBarItemVisible = TRUE
         END IF
      END IF
   END IF

   IF i_EditMenuLastNum > 0 THEN
      IF NOT i_EditMenuLast.Visible THEN
         i_EditMenuLast.Visible = TRUE
      END IF
      IF i_Window.ToolBarVisible THEN
         IF NOT i_EditMenuLast.ToolBarItemVisible THEN
            i_EditMenuLast.ToolBarItemVisible = TRUE
         END IF
      END IF
   END IF

   IF NOT i_PopupMenuIsEdit THEN
      IF i_PopupMenuFirstNum > 0 THEN
         IF NOT i_PopupMenuFirst.Visible THEN
            i_PopupMenuFirst.Visible = TRUE
         END IF
      END IF

      IF i_PopupMenuPrevNum > 0 THEN
         IF NOT i_PopupMenuPrev.Visible THEN
            i_PopupMenuPrev.Visible = TRUE
         END IF
      END IF

      IF i_PopupMenuNextNum > 0 THEN
         IF NOT i_PopupMenuNext.Visible THEN
            i_PopupMenuNext.Visible = TRUE
         END IF
      END IF

      IF i_PopupMenuLastNum > 0 THEN
         IF NOT i_PopupMenuLast.Visible THEN
            i_PopupMenuLast.Visible = TRUE
         END IF
      END IF
   END IF

   IF i_AllowNew OR i_EnableNewOnOpen THEN
      l_ModeSep  = TRUE
      l_DoView   = TRUE

      IF i_EditMenuNewNum > 0 THEN
         IF NOT i_EditMenuNew.Visible THEN
            i_EditMenuNew.Visible = TRUE
         END IF
         IF i_Window.ToolBarVisible THEN
            IF NOT i_EditMenuNew.ToolBarItemVisible THEN
               i_EditMenuNew.ToolBarItemVisible = TRUE
            END IF
         END IF
      END IF

      IF NOT i_PopupMenuIsEdit THEN
         IF i_PopupMenuNewNum > 0 THEN
            IF NOT i_PopupMenuNew.Visible THEN
               i_PopupMenuNew.Visible = TRUE
            END IF
         END IF
      END IF
   END IF

   IF i_AllowNew THEN
      l_DoDelete = TRUE

      IF i_EditMenuInsertNum > 0 THEN
         IF NOT i_EditMenuInsert.Visible THEN
            i_EditMenuInsert.Visible = TRUE
         END IF
         IF i_Window.ToolBarVisible THEN
            IF NOT i_EditMenuInsert.ToolBarItemVisible THEN
               i_EditMenuInsert.ToolBarItemVisible = TRUE
            END IF
         END IF
      END IF

      IF NOT i_PopupMenuIsEdit THEN
         IF i_PopupMenuInsertNum > 0 THEN
          IF NOT i_PopupMenuInsert.Visible THEN
             i_PopupMenuInsert.Visible = TRUE
          END IF
         END IF
      END IF
   END IF

   IF i_AllowModify OR i_EnableModifyOnOpen THEN
      l_ModeSep = TRUE
      l_DoView  = TRUE

      IF i_EditMenuModifyNum > 0 THEN
         IF NOT i_EditMenuModify.Visible THEN
            i_EditMenuModify.Visible = TRUE
         END IF
         IF i_Window.ToolBarVisible THEN
            IF NOT i_EditMenuModify.ToolBarItemVisible THEN
               i_EditMenuModify.ToolBarItemVisible = TRUE
            END IF
         END IF
      END IF

      IF NOT i_PopupMenuIsEdit THEN
         IF i_PopupMenuModifyNum > 0 THEN
            IF NOT i_PopupMenuModify.Visible THEN
               i_PopupMenuModify.Visible = TRUE
            END IF
         END IF
      END IF
   END IF

   IF l_DoView THEN
      IF i_EditMenuViewNum > 0 THEN
         IF NOT i_EditMenuView.Visible THEN
            i_EditMenuView.Visible = TRUE
         END IF
         IF i_Window.ToolBarVisible THEN
            IF NOT i_EditMenuView.ToolBarItemVisible THEN
               i_EditMenuView.ToolBarItemVisible = TRUE
            END IF
         END IF
      END IF

      IF NOT i_PopupMenuIsEdit THEN
         IF i_PopupMenuViewNum > 0 THEN
            IF NOT i_PopupMenuView.Visible THEN
               i_PopupMenuView.Visible = TRUE
            END IF
         END IF
      END IF
   END IF

   IF i_AllowDelete OR l_DoDelete THEN
      l_ModeSep = TRUE

      IF i_EditMenuDeleteNum > 0 THEN
         IF NOT i_EditMenuDelete.Visible THEN
            i_EditMenuDelete.Visible = TRUE
         END IF
         IF i_Window.ToolBarVisible THEN
            IF NOT i_EditMenuDelete.ToolBarItemVisible THEN
               i_EditMenuDelete.ToolBarItemVisible = TRUE
            END IF
         END IF
      END IF

      IF NOT i_PopupMenuIsEdit THEN
         IF i_PopupMenuDeleteNum > 0 THEN
            IF NOT i_PopupMenuDelete.Visible THEN
               i_PopupMenuDelete.Visible = TRUE
            END IF
         END IF
      END IF
   END IF

   IF l_ModeSep THEN
      IF i_EditMenuModeSepNum > 0 THEN
         IF NOT i_EditMenuModeSep.Visible THEN
            i_EditMenuModeSep.Visible = TRUE
         END IF
      END IF

      IF NOT i_PopupMenuIsEdit THEN
         IF i_PopupMenuModeSepNum > 0 THEN
            IF NOT i_PopupMenuModeSep.Visible THEN
               i_PopupMenuModeSep.Visible = TRUE
            END IF
         END IF
      END IF
   END IF

   IF i_AllowQuery THEN
      l_SearchSep = TRUE

      IF i_EditMenuQueryNum > 0 THEN
         IF NOT i_EditMenuQuery.Visible THEN
            i_EditMenuQuery.Visible = TRUE
         END IF
         IF i_Window.ToolBarVisible THEN
            IF NOT i_EditMenuQuery.ToolBarItemVisible THEN
               i_EditMenuQuery.ToolBarItemVisible = TRUE
            END IF
         END IF
      END IF

      IF NOT i_PopupMenuIsEdit THEN
         IF i_PopupMenuQueryNum > 0 THEN
            IF NOT i_PopupMenuQuery.Visible THEN
               i_PopupMenuQuery.Visible = TRUE
            END IF
         END IF
      END IF
   END IF

   IF i_AllowQuery           OR &
      i_NumSearchObjects > 0 OR &
      i_NumFilterObjects > 0 THEN
      l_SearchSep = TRUE

      IF i_NumSearchObjects > 0 THEN
         IF i_EditMenuSearchNum > 0 THEN
            IF NOT i_EditMenuSearch.Visible THEN
               i_EditMenuSearch.Visible = TRUE
            END IF
            IF i_Window.ToolBarVisible THEN
               IF NOT i_EditMenuSearch.ToolBarItemVisible THEN
                  i_EditMenuSearch.ToolBarItemVisible = TRUE
               END IF
            END IF
         END IF

         IF NOT i_PopupMenuIsEdit THEN
            IF i_PopupMenuSearchNum > 0 THEN
               IF NOT i_PopupMenuSearch.Visible THEN
                  i_PopupMenuSearch.Visible = TRUE
               END IF
            END IF
         END IF
      END IF

      IF i_NumFilterObjects > 0 THEN
         IF i_EditMenuFilterNum > 0 THEN
            IF NOT i_EditMenuFilter.Visible THEN
               i_EditMenuFilter.Visible = TRUE
            END IF
            IF i_Window.ToolBarVisible THEN
               IF NOT i_EditMenuFilter.ToolBarItemVisible THEN
                  i_EditMenuFilter.ToolBarItemVisible = TRUE
               END IF
            END IF
         END IF

         IF NOT i_PopupMenuIsEdit THEN
            IF i_PopupMenuFilterNum > 0 THEN
               IF NOT i_PopupMenuFilter.Visible THEN
                  i_PopupMenuFilter.Visible = TRUE
               END IF
            END IF
         END IF
      END IF
   END IF

   IF l_SearchSep THEN
      IF i_EditMenuSearchSepNum > 0 THEN
         IF NOT i_EditMenuSearchSep.Visible THEN
            i_EditMenuSearchSep.Visible = TRUE
         END IF
      END IF

      IF NOT i_PopupMenuIsEdit THEN
         IF i_PopupMenuSearchSepNum > 0 THEN
            IF NOT i_PopupMenuSearchSep.Visible THEN
               i_PopupMenuSearchSep.Visible = TRUE
            END IF
         END IF
      END IF
   END IF
END IF

RETURN
end subroutine

public subroutine do_cc_rollback ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Do_CC_Rollback
//  Description   : Internal routine to perform rollbacks for
//                  concurrency errors.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_CCDBError
LONG          l_DBErrorCode,    l_DBErrorRow
UNSIGNEDLONG  l_MBID
REAL          l_MBNumbers[]
STRING        l_MBStrings[]
STRING        l_DBErrorMessage, l_DevDBErrorMessage
DWBUFFER      l_DBErrorBuffer

//----------
//  Initialize variables for use by the developer.
//----------

PCCA.Error                       = c_Success
i_RootDW.i_DisplayError = TRUE

//----------
//  Make sure that the rollback has not already been done.
//----------

IF NOT i_RootDW.i_DidRollback THEN

   //----------
   //  Save the DB error information.
   //----------

   l_CCDBError         = i_CCDBError
   l_DBErrorCode       = i_DBErrorCode
   l_DBErrorMessage    = i_DBErrorMessage
   l_DBErrorRow        = i_DBErrorRow
   l_DBErrorBuffer     = i_DBErrorBuffer
   l_DevDBErrorMessage = i_DevDBErrorMessage

   //----------
   //  Reset the DB Error codes.
   //----------

   i_RootDW.i_CCDBError  = FALSE
   i_RootDW.i_DBErrorCode             = 0
   i_RootDW.i_DBErrorMessage          = ""
   i_RootDW.i_DBErrorRow              = 0
   i_RootDW.i_DevDBErrorMessage       = ""

   //----------
   //  Trigger the event to rollback the database.
   //----------

   i_RootDW.i_DidRollback = TRUE
   i_RootDW.TriggerEvent("pcd_Rollback")

   //----------
   //  If there was an error while rolling back the database and
   //  we were not told to prevent the error display, then display
   //  the error.
   //----------

   IF PCCA.Error <> c_Success THEN
   IF i_RootDW.i_DisplayError THEN
      l_MBID         = PCCA.MB.c_MBI_DW_RollbackError
      l_MBNumbers[1] = i_RootDW.i_DBErrorCode
      l_MBStrings[1] = "pcd_Rollback"
      l_MBStrings[2] = PCCA.Application_Name
      l_MBStrings[3] = i_RootDW.i_ClassName
      l_MBStrings[4] = i_RootDW.i_Window.Title
      l_MBStrings[5] = i_RootDW.DataObject
      l_MBStrings[6] = i_RootDW.i_DBErrorMessage
      l_MBStrings[7] = i_RootDW.i_DevDBErrorMessage
      PCCA.MB.fu_MessageBox(l_MBID,           &
                            1, l_MBNumbers[], &
                            7, l_MBStrings[])

      //----------
      //  Make sure that PCCA.Error still indicates
      //  failure.
      //----------

      PCCA.Error = c_Fatal
   END IF
   END IF

   //----------
   //  Restore the DB error information.
   //----------

   i_CCDBError                  = l_CCDBError
   i_DBErrorCode       = l_DBErrorCode
   i_DBErrorMessage    = l_DBErrorMessage
   i_DBErrorRow        = l_DBErrorRow
   i_DBErrorBuffer     = l_DBErrorBuffer
   i_DevDBErrorMessage = l_DevDBErrorMessage
END IF

end subroutine

public function uo_dw_main find_dw_current ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Find_DW_Current
//  Description   : Finds the DataWindow that is current in
//                  this window.
//
//  Parameters    : (None)
//  Return Value  : UO_DW_MAIN -
//                        The DataWindow that should become the
//                        current DataWindow when the window is
//                        activated.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER     l_Idx
UO_DW_MAIN  l_CurrentDW, l_TmpDW

//----------
//  Assume that there is not a valid DataWindow.
//----------

l_CurrentDW = PCCA.Null_Object

//----------
//  Go looking to see if our assumption is correct.
//----------

FOR l_Idx = 1 TO i_NumWindowDWs
   l_TmpDW = i_WindowDWs[l_Idx]

   //----------
   //  See if this DataWindow was the current DataWindow the
   //  last time this window was active.  If it is, then we have
   //  found the DataWindow we were looking for and we are done.
   //----------

   IF l_TmpDW.i_Current THEN
   IF l_TmpDW.i_InUse   THEN
      l_CurrentDW = l_TmpDW
      EXIT
   END IF
   END IF

   //----------
   //  If we get to here, then this DataWindow was not the
   //  DataWindow that was current that last time the window was
   //  active.  However, if this is the first valid DataWindow that
   //  we find, remember it in case we can't find the DataWindow
   //  that was current that last time the window was active.
   //----------

   IF IsNull(l_CurrentDW) THEN
   IF l_TmpDW.i_InUse THEN
   IF l_TmpDW.Visible THEN
      l_CurrentDW = l_TmpDW
   END IF
   END IF
   END IF
NEXT

//----------
//  If l_CurrentDW is non-NULL, we have found the candidate to
//  become the current DataWindow.
//----------

IF IsNull(l_CurrentDW) THEN
ELSE
   l_CurrentDW.i_Current = TRUE
END IF

RETURN l_CurrentDW
end function

public function uo_dw_main find_dw_root (unsignedlong qualification);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Find_DW_Root
//  Description   : Looks up the hierarchy of DataWindows to
//                  find the DataWindow who meets the
//                  specified qualifications.
//
//  Parameters    : UNSIGNEDLONG Qualification -
//                        An enumerated constant for the root
//                        window that is desired.  Valid values
//                        are:
//
//                           - c_FindRootDW
//                           - c_FindScrollDW
//
//  Return Value  : UO_DW_MAIN -
//                        The DataWindow that fills the
//                        requirements specified by Qualification.
//                        The value that is returned may be
//                        the same DataWindow that requested
//                        the information (i.e. THIS may be
//                        returned).
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN     l_LessQualified
UO_DW_MAIN  l_ReturnDW

//----------
//  Assume that this is the DataWindow that fills the requirements.
//  We will then look up the tree to find if another DataWindow
//  fits the requirements better.
//----------

l_ReturnDW = THIS

//----------
//  Based on what we are looking for, set the l_LessQualified
//  flag.  This indicates that our parent is less qualified than
//  us for the request.
//----------

CHOOSE CASE Qualification
   CASE c_FindRootDW
      l_LessQualified = (NOT l_ReturnDW.i_ParentIsValid)
   CASE c_FindScrollDW
      l_LessQualified = (NOT l_ReturnDW.i_ScrollParent OR &
                         NOT l_ReturnDW.i_ParentIsValid)
   CASE ELSE
      GOTO Finished
END CHOOSE

//----------
//  As long as the parent is more qualified, keep looking up the
//  tree for the DataWindow that meets the requirements.
//----------

DO WHILE NOT l_LessQualified

   //----------
   //  Since we know the parent is more qualified, set the return
   //  value to the parent.
   //----------

   l_ReturnDW = l_ReturnDW.i_ParentDW

   //----------
   //  Check again to see if the current return value's parent
   //  meets the requirements.
   //----------

   CHOOSE CASE Qualification
      CASE c_FindRootDW
         l_LessQualified = (NOT l_ReturnDW.i_ParentIsValid)
      CASE c_FindScrollDW
         l_LessQualified = (NOT l_ReturnDW.i_ScrollParent OR &
                            NOT l_ReturnDW.i_ParentIsValid)
      CASE ELSE
         GOTO Finished
   END CHOOSE
LOOP

Finished:

RETURN l_ReturnDW
end function

public subroutine find_instance (long selected_row);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Find_Instance
//  Description   : Finds the current instance DataWindow
//                  (if any) associated with the selected row.
//
//  Parameters    : LONG Selected_Row -
//                        The row for which to see if there
//                        is a corresponding DataWindow.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN     l_Found
INTEGER     l_ColNbr, l_Idx
STRING      l_KeyArray[]
UO_DW_MAIN  l_ChildDW

//----------
//  Assume that there is not an instance DataWindow associated
//  with the requested row.
//----------

i_CurInstance = 0

//----------
//  If there are not any instance DataWindows or if Selected_Row
//  is not valid, then nothing needs to be done.
//----------

IF i_NumInstance > 0 THEN
IF Selected_Row  > 0 THEN

   //----------
   //  InstanceDW.Set_DW_Options() will have called Get_Col_Info().
   //  No need to do it again.
   //----------

   //----------
   //  For each key column in this DataWindow, get the data for
   //  Selected_Row.  We will use this data to look through all
   //  of the instance DataWindows looking for the instance
   //  DataWindow that contains the matching data.
   //----------

   FOR l_Idx = 1 TO i_xNumKeyColumns
      l_ColNbr          = i_xKeyColumns[l_Idx]
      l_KeyArray[l_Idx] = Get_Col_Data_Ex            &
                             (Selected_Row, l_ColNbr,&
                              Primary!,     c_GetCurrentValues)
   NEXT

   //----------
   //  Search through the instance DataWindows looking for an
   //  instance DataWindow which corresponds to the requested row.
   //  When the instance DataWindow was initialized, it saved the
   //  keys of the row that caused it to open.  Compare the
   //  requested row's key data against this data.  If it is a
   //  match, then we know that this instance DataWindow was
   //  opened when the user selected the requested row.
   //----------

   FOR l_Idx = 1 TO i_NumInstance
      l_ChildDW = i_InstanceDW[l_Idx]

      //----------
      //  Assume the keys match until proven otherwise.
      //----------

      l_Found = TRUE

      FOR l_ColNbr = 1 TO i_xNumKeyColumns

         //----------
         //  If the keys don't match, then this is the not the
         //  instance DataWindow associated with the requested
         //  row.
         //----------

         IF l_KeyArray[l_ColNbr] <> &
            l_ChildDW.i_KeysInParent[l_ColNbr] THEN
            l_Found = FALSE
            EXIT
         END IF
      NEXT

      //----------
      //  If all of the keys matched, then we have found the
      //  instance DataWindow that we were looking for.  No need
      //  to do any more searching.
      //----------

      IF l_Found THEN
         EXIT
      END IF
   NEXT

   //----------
   //  If we found a match, then set i_CurInstance to the index
   //  of the instance DataWindow that is associated with the
   //  requested row.
   //----------

   IF l_Found THEN
      i_CurInstance = l_Idx
   END IF
END IF
END IF

RETURN
end subroutine

public function long find_key_row (long which_to_find, string key_array[], long start_row, long stop_row);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Find_Key_Row
//  Description   : Given the index into the key array, finds
//                  the row that matchs those keys.
//
//  Parameters    : LONG Which_To_Find -
//                        An index into Key_Array[] that identifies
//                        the set of keys that are to be used to
//                        search for the row.
//
//                  STRING Key_Array[] -
//                        An array of key sets.
//
//                  LONG Start_Row -
//                        The row at which to start looking
//                        for a match to the key set.  Provided for
//                        optimization purposes.
//
//                  LONG Stop_Row -
//                        The row at which to stop looking
//                        for a match to the key set.  Provided for
//                        optimization purposes.
//
//  Return Value  : LONG -
//                        The row that corresponds to the
//                        specified key set.  It will be zero (0),
//                        if a row corresponding to the key
//                        set could not be found.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_KeyStart, l_ColNbr
UNSIGNEDLONG  l_ColType
LONG          l_FoundRow
STRING        l_Left, l_Find, l_Add

//----------
//  Assume that a row cannot be found that matches the key set.
//----------

l_FoundRow = 0

//----------
//  Find the starting index of the key set in Key_Array[].
//  If there are 3 key columns in the DataWindow, Key_Array[]
//  contains the key sets in the following format:
//     Key_Array[1] = Key #1 for Record #1
//     Key_Array[2] = Key #2 for Record #1
//     Key_Array[3] = Key #3 for Record #1
//     Key_Array[4] = Key #1 for Record #2
//     Key_Array[5] = Key #2 for Record #2
//     Key_Array[6] = Key #3 for Record #2
//     Key_Array[7] = Key #1 for Record #3
//     Key_Array[8] = Key #2 for Record #3
//     Key_Array[9] = Key #3 for Record #3
//----------

l_KeyStart = (Which_To_Find - 1) * i_xNumKeyColumns

//----------
//  Whoever is calling this should have called Get_Col_Info().
//  No need to do it again.
//----------

//----------
//  We need to build a search string that will be passed to FIND().
//  For each key in the DataWindow, add the key data for the row
//  for which we are searching to this string.  The format of the
//  search string will be:
//     String(#3) = "123" AND String(#5) = "456"
//----------

l_Find = ""
FOR l_ColNbr = 1 TO i_xNumKeyColumns

   //----------
   //  Get the enumerated value for the type of data that the
   //  column contains.
   //----------

   l_ColType = i_xColumnType[i_xKeyColumns[l_ColNbr]]

   //----------
   //  Based on the column data type, set up the left hand side
   //  of the FIND() specification.
   //----------

   CHOOSE CASE l_ColType
      CASE c_ColTypeDate
         l_Left = "String(#" +                      &
                  String(i_xKeyColumns[l_ColNbr]) + &
                  ", ~"" + c_FormatDate + "~") = '"

      CASE c_ColTypeDateTime
         l_Left = "String(#" + &
                  String(i_xKeyColumns[l_ColNbr]) + &
                  ", ~"" + c_FormatDateTime + "~") = '"

      CASE c_ColTypeTime
         l_Left = "String(#" + &
                  String(i_xKeyColumns[l_ColNbr]) + &
                  ", ~"" + c_FormatTime + "~") = '"

      CASE ELSE
         l_Left = "String(#" +                      &
                  String(i_xKeyColumns[l_ColNbr]) + &
                  ") = '"
   END CHOOSE

   l_Add = l_Left +                                     &
           PCCA.MB.fu_QuoteChar                            &
              (Key_Array[l_KeyStart + l_ColNbr], "'") + &
           "'"
   IF l_Find = "" THEN
      l_Find = l_Add
   ELSE
      l_Find = l_Find + " AND " + l_Add
   END IF
NEXT

//----------
//  Using the search string just built, call FIND() to see if
//  there is a row that matches the key set.  Start_Row and
//  Stop_Row are passed by the caller and can be used to cut
//  down the range and time of the search.  They can be used
//  by the caller if the caller has an idea of the range where
//  the desired row might be found.
//----------

l_FoundRow = Find(l_Find, Start_Row, Stop_Row)

RETURN l_FoundRow
end function

public function string get_col_data (long row_nbr, integer column_nbr);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Get_Col_Data
//  Description   : Returns the current data from the specified
//                  field as a string.  If the field is NULL,
//                  the return value will be NULL.  Note that an
//                  ACCEPTTEXT() is not done.  The developer
//                  should call dw_AcceptText() if they wish the
//                  user's input to be accepted into the field.
//
//  Parameters    : LONG Row_Nbr -
//                        The row number for the field from
//                        which to get the data.
//
//                  INTEGER Column_Nbr -
//                        The column number for the field from
//                        which to get the data.
//
//  Return Value  : STRING -
//                        The data at the specifed row and
//                        column.  May be NULL.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

STRING  l_Return

l_Return = Get_Col_Data_Ex(Row_Nbr,  Column_Nbr, &
                           Primary!, c_GetCurrentValues)

RETURN l_Return
end function

public function string get_col_data_ex (long row_nbr, integer column_nbr, dwbuffer which_buffer, unsignedlong which_values);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Get_Col_Data_Ex
//  Description   : Returns the current data from the specified
//                  field as a string.  If the field is NULL,
//                  the return value will be NULL.  Note that an
//                  ACCEPTTEXT() is not done.  The developer
//                  should call dw_AcceptText() if they wish the
//                  user's input to be accepted into the field.
//
//  Parameters    : LONG Row_Nbr -
//                        The row number for the field from
//                        which to get the data.
//
//                  INTEGER Column_Nbr -
//                        The column number for the field from
//                        which to get the data.
//
//                  DWBUFFER Which_Buffer -
//                        Specified which DataWindow buffer to
//                        retrieve the data from (e.g. Primary!).
//
//                  BOOLEAN Which_Values -
//                        Specifies if the current values in
//                        the DataWindow or the orignal values
//                        from the database are to be returned.
//                        Valid options are:
//
//                           - c_GetCurrentValues
//                           - c_GetOriginalValues
//
//  Return Value  : STRING -
//                        The data at the specifed row and
//                        column.  May be NULL.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_GetOriginalValues
UNSIGNEDLONG  l_ColType
LONG          l_NumRows
STRING        l_Return
DECIMAL       l_Decimal
DATE          l_Date
DATETIME      l_DateTime
TIME          l_Time

//----------
//  Initialize the return to indicate that no data was found.
//----------

SetNull(l_Return)

//----------
//  Make sure we have a valid column and row.
//----------

IF Which_Buffer = Primary! THEN
   l_NumRows = RowCount()
ELSE
   IF Which_Buffer = Delete! THEN
      l_NumRows = DeletedCount()
   ELSE
      IF Which_Buffer = Filter! THEN
         l_NumRows = FilteredCount()
      END IF
   END IF
END IF

IF Column_Nbr < 1 OR Column_Nbr > i_NumColumns OR &
   Row_Nbr    < 1 OR Row_Nbr    > l_NumRows    THEN
   GOTO Finished
END IF

//----------
//  Column information is not saved when the DataWindow
//  is initialized to cut down on open time.  Therefore,
//  Get_Col_Info() may need to be called to get this
//  information loaded.
//----------

IF NOT i_GotColInfo THEN
   Get_Col_Info()
END IF

//----------
//  Get the enumerated value for the type of data that the column
//  contains.
//----------

l_ColType = i_xColumnType[Column_Nbr]

//----------
//  Determine what values we are getting from the DataWindow.
//----------

l_GetOriginalValues = (Which_Values = c_GetOriginalValues)

//----------
//  Based on the column data type, get the data from the specified
//  cell and and convert it to string.
//----------

CHOOSE CASE l_ColType

   //----------
   //  Get DATE data from the cell.
   //----------

   CASE c_ColTypeDate

      l_Date = GetItemDate(Row_Nbr,      &
                           Column_Nbr,   &
                           Which_Buffer, &
                           l_GetOriginalValues)
      IF IsNull(l_Date) THEN
      ELSE
         l_Return = String(l_Date, c_FormatDate)
      END IF

   //----------
   //  Get DATETIME data from the cell.
   //----------

   CASE c_ColTypeDateTime

      l_DateTime = GetItemDateTime(Row_Nbr,      &
                                   Column_Nbr,   &
                                   Which_Buffer, &
                                   l_GetOriginalValues)
      IF IsNull(l_DateTime) THEN
      ELSE
         l_Return = String(l_DateTime, c_FormatDateTime)
      END IF

   //----------
   //  Get DECIMAL data from the cell.
   //----------

   CASE c_ColTypeDecimal

      l_Decimal = GetItemDecimal(Row_Nbr,      &
                                 Column_Nbr,   &
                                 Which_Buffer, &
                                 l_GetOriginalValues)
      IF IsNull(l_Decimal) THEN
      ELSE
         l_Return = String(l_Decimal)
      END IF

   //----------
   //  Get NUMBER data from the cell.
   //----------

   CASE c_ColTypeNumber

      IF IsNull(GetItemNumber(Row_Nbr,      &
                              Column_Nbr,   &
                              Which_Buffer, &
                              l_GetOriginalValues)) THEN
      ELSE
         l_Return = String(GetItemNumber(Row_Nbr,      &
                                         Column_Nbr,   &
                                         Which_Buffer, &
                                         l_GetOriginalValues))
      END IF

   //----------
   //  Get TIME data from the cell.
   //----------

   CASE c_ColTypeTime

      l_Time = GetItemTime(Row_Nbr,      &
                           Column_Nbr,   &
                           Which_Buffer, &
                           l_GetOriginalValues)
      IF IsNull(l_Time) THEN
      ELSE
         l_Return = String(l_Time, c_FormatTime)
      END IF

   //----------
   //  Can't get Blob data - return NULL.
   //----------

   CASE c_ColTypeBlob

   //----------
   //  Get STRING data from the cell.
   //----------

   //CASE c_ColTypeTimeStamp
   //CASE c_ColTypeString
   CASE ELSE

      l_Return = GetItemString(Row_Nbr,      &
                               Column_Nbr,   &
                               Which_Buffer, &
                               l_GetOriginalValues)
END CHOOSE

Finished:

RETURN l_Return
end function

public subroutine get_col_info ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Get_Col_Info
//  Description   : This routine loads information about the
//                  DataWindow's columns into instance variables.
//                  The column information is not queried when
//                  the DataWindow is initialized.  Therefore,
//                  this routine must be called when the column
//                  information is needed for processing.  The
//                  following instance variables are initialized
//                  with column information by this routine:
//
//                     - i_GotColInfo
//                     - i_xNumKeyColumns
//                     - i_xKeyColumns[]
//                     - i_xColumnUpdate[]
//                     - i_xColumnType[]
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_IsUpdate
INTEGER       l_Pos,     l_Idx
UNSIGNEDLONG  l_Type
STRING        l_Col,     l_Describe, l_Result

//----------
//  We only need to do this once.  If it has already been done,
//  there is no reason to do it again.
//----------

IF NOT i_GotColInfo THEN
   i_GotColInfo     = TRUE
   i_xNumKeyColumns = 0

   //----------
   //  For each column in the DataWindow, we need to get the
   //  following column information:
   //     a) The type of data the column contains (e.g. DATETIME)
   //     b) Is the column a key column?
   //----------

   FOR l_Idx = 1 TO i_NumColumns
      l_Col     = "#" + String(l_Idx)

      l_Describe = l_Col + ".ColType"
      l_Result   = Describe(l_Describe)

      //----------
      //  Some of the strings describing the data come back with
      //  parentheses to indicating the length of the data (e.g.
      //  "CHAR(10)").  Strip this information off.
      //----------

      l_Pos = Pos(l_Result, "(")
      IF l_Pos > 0 THEN
         l_Result = Mid(l_Result, 1, l_Pos - 1)
      END IF

      //----------
      //  Convert PowerBuilder's character description of the data
      //  to our enumerated constants.
      //----------

      CHOOSE CASE Upper(l_Result)
         CASE "CHAR"
            l_Type = c_ColTypeString
         CASE "DATE"
            l_Type = c_ColTypeDate
         CASE "DATETIME"
            l_Type = c_ColTypeDateTime
         CASE "DECIMAL"
            l_Type = c_ColTypeDecimal
         CASE "NUMBER"
            l_Type = c_ColTypeNumber
         CASE "TIME"
            l_Type = c_ColTypeTime
         CASE "TIMESTAMP"
            l_Type = c_ColTypeTimeStamp
         CASE "BLOB"
         CASE "TABLEBLOB"
            l_Type = c_ColTypeBlob
         CASE ELSE
            l_Type = c_ColTypeUndefined
      END CHOOSE

      //----------
      //  Save the enumerated constant describing the type of the
      //  column's data.
      //----------

      i_xColumnType[l_Idx] = l_Type

      //----------
      //  Ask the DataWindow if this column is a key column.
      //----------

      l_Describe = l_Col + ".Key"
      l_Result   = Upper(Describe(l_Describe))
      IF l_Result = "YES" THEN

          //----------
          //  This is a key column.  Increment the number of key
          //  columns that this DataWindow contains and save the
          //  column number into the list of columns that are
          //  keys (Note: l_Idx contains the column number that
          //  we are processing).
          //----------

          i_xNumKeyColumns = i_xNumKeyColumns + 1
          i_xKeyColumns[i_xNumKeyColumns] = l_Idx
      END IF

      //----------
      //  Ask the DataWindow if this column is updated to the
      //  database.
      //----------

      l_Describe             = l_Col + ".Update"
      l_Result               = Upper(Describe(l_Describe))
      i_xColumnUpdate[l_Idx] = (l_Result = "YES")
      IF i_xColumnUpdate[l_Idx] THEN
         l_IsUpdate = TRUE
      END IF
   NEXT

   //----------
   //  Ask the DataWindow which table it is updating.
   //----------

   IF l_IsUpdate THEN
      l_Describe    = "DataWindow.Table.UpdateTable"
      i_UpdateTable = Describe(l_Describe)
   ELSE
      i_UpdateTable = ""
   END IF
END IF

RETURN
end subroutine

public subroutine get_event_info (unsignedlong trigger_level);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Get_Event_Info
//  Description   : Calculates event control information.
//
//  Parameters    : UNSIGNEDLONG Trigger_Level -
//                        Indicates if the event needs to go
//                        to the root-level DataWindow.  Valid
//                        options are:
//
//                           - c_EventGotoRoot
//                           - c_EventNoTriggerUp
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//----------
//  We can only be a recipient if the event is not cascading.  If
//  it is cascading, then obviously some other DataWindow was the
//  recipient.
//----------

is_EventInfo.Event_Recipient = (NOT is_EventControl.Cascading)

//----------
//  PCCA.Do_Event can be set TRUE to force a DataWindow to become
//  the root DataWindow for the event.
//----------

is_EventInfo.Event_Root = PCCA.Do_Event

//----------
//  Trigger_Up indicates if the event needs to be triggered upwards
//  in the hierarchy of DataWindows.  For now, we will assume that
//  this event does not need to be triggered up.
//----------

is_EventInfo.Trigger_Up = FALSE

//----------
//  If we are the original recipient of the event, we need to
//  figure out if we should deal with the event or pass it off
//  to some other DataWindow.
//----------

IF is_EventInfo.Event_Recipient THEN

   //----------
   //  If we aren't sure if we are the root of the event, we
   //  had better figure it out.  Earlier in this routine we
   //  checked to see if we forced into being the root by checking
   //  PCCA.Do_Event.
   //----------

   IF NOT is_EventInfo.Event_Root THEN

      //----------
      //  If Trigger_Level is c_EventGotoRoot, and we are the
      //  root-level DataWindow, then there is no need to trigger
      //  up.  However, if we are not the root-level DataWindow,
      //  then will need to trigger up.
      //----------

      IF Trigger_Level = c_EventGotoRoot THEN
         IF i_RootDW = THIS THEN
            is_EventInfo.Event_Root = TRUE
         ELSE
            is_EventInfo.Trigger_Up = TRUE
         END IF
      ELSE

         //----------
         //  We are the original recipient of the event and we
         //  have been told that there is no need to trigger up.
         //  Therefore, we are the root of the event.
         //----------

         is_EventInfo.Event_Root = TRUE
      END IF
   END IF
END IF

//----------
//  If this event was already cascading, then it should continue
//  cascading.  If we are the root of the event, then we need to
//  start the event cascading.
//----------

is_EventInfo.Cascading = (is_EventControl.Cascading OR &
                          is_EventInfo.Event_Root)

//----------
//  Reset all of the event control variables.
//----------

PCCA.Do_Event   = FALSE
is_EventControl = is_ResetControl

RETURN
end subroutine

public subroutine get_val_info (integer column_nbr);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Get_Val_Info
//  Description   : Prepares column validation information for
//                  a call to the developer's validation
//                  event.  If necessary, this routine will
//                  get any column information from the
//                  DataWindow if it has not already been
//                  gotten.  The following instance variables
//                  are initialized by this routine:
//
//                     i_GotValInfo[Column_Nbr]
//                     i_xColumnNames[Column_Nbr]
//                     i_xDBNames[Column_Nbr]
//                     i_xColumnValMsg[Column_Nbr]
//                     i_xColumnRequired[Column_Nbr]
//                     i_xColumnNilIsNull[Column_Nbr]
//
//                  Additionally, the following instance
//                  variables are set in preparation of
//                  triggering the developer's pcd_ValidateCol
//                  event:
//
//                     i_ColText
//                     i_ColName
//                     i_ColValMsg
//                     i_ColFormatNumber
//                     i_ColFormatString
//                     i_ColRequiredError
//
//  Parameters    : INTEGER Column_Nbr -
//                        The column number for which to get
//                        validation information.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_NumStyles, l_Idx
STRING        l_Col,       l_EditStyles[], l_Describe
STRING        l_Required,  l_NilIsNull
DWITEMSTATUS  l_ItemStatus

//----------
//  Make sure we have a valid column.
//----------

IF Column_Nbr < 1 OR Column_Nbr > i_NumColumns THEN
   GOTO Finished
END IF

//----------
//  We only need to do this once for each column.  If it has
//  already been done, there is no reason to do it again.
//----------

IF NOT i_GotValInfo[Column_Nbr] THEN
   i_GotValInfo[Column_Nbr] = TRUE

   //----------
   //  For each column in the DataWindow, we need to get the
   //  following validation information:
   //     a) The column name.
   //     b) The name of the column in the database.
   //     c) The validation message (if any) for this column.
   //     d) Whether this is a required column and if an empty
   //        string is considered a NULL value.
   //----------

   l_Col                       = "#" + String(Column_Nbr)

   l_Describe                  = l_Col + ".Name"
   i_xColumnNames[Column_Nbr]  = Trim(Lower(Describe(l_Describe)))

   l_Describe                  = l_Col + ".DBName"
   i_xDBNames[Column_Nbr]      = Trim(Lower(Describe(l_Describe)))

   l_Describe                  = l_Col + ".ValidationMsg"
   i_xColumnValMsg[Column_Nbr] = Describe(l_Describe)
   IF i_xColumnValMsg[Column_Nbr] = "?" THEN
      i_xColumnValMsg[Column_Nbr] = ""
   END IF

   //----------
   //  We can not find out what edit style the column is from
   //  PowerBuilder.  Therefore, we have to cycle through all the
   //  possible types seeing if we get a valid response.  Set up
   //  an array containing all known styles.
   //----------

   l_EditStyles[1] = ".Edit"
   l_EditStyles[2] = ".EditMask"
   l_EditStyles[3] = ".DDLB"
   l_EditStyles[4] = ".DDDW"
   l_NumStyles     = UpperBound(l_EditStyles[])

   //----------
   //  For each edit style that we know about, see if this column
   //  will respond to a DESCRIBE().
   //----------

   FOR l_Idx = 1 TO l_NumStyles
      l_Describe = l_Col + l_EditStyles[l_Idx] + ".Required"
      l_Required = Upper(Describe(l_Describe))
      IF l_Required <> "?" THEN

         //----------
         //  The column responded.  Now we can see if empty
         //  strings are to be treated as NULL.
         //----------

         l_Describe  = l_Col + l_EditStyles[l_Idx] + ".NilIsNull"
         l_NilIsNull = Upper(Describe(l_Describe))

         //----------
         //  Remember what we just found out.
         //----------

         i_xColumnRequired[Column_Nbr]  = (l_Required  = "YES")
         i_xColumnNilIsNull[Column_Nbr] = (l_NilIsNull = "YES")
         EXIT
      END IF
   NEXT

   //----------
   //  If l_Required is "?", then this must be a CheckBox or
   //  RadioButton column.
   //----------

   IF l_Required = "?" THEN
      i_xColumnRequired[Column_Nbr]  = FALSE
      i_xColumnNilIsNull[Column_Nbr] = FALSE
   END IF
END IF

//----------
//  We can only fill out the validation information if we are on
//  the row and column specified by i_ColNbr and i_RowNbr.
//----------

IF i_ColNbr = GetColumn() THEN
IF i_RowNbr = GetRow()    THEN

   //----------
   //  Copy the validation information that the developer
   //  needs into the instance variables that are provided
   //  for developer-coded validation events.
   //----------

  i_ColText   = GetText()
  i_ColName   = i_xColumnNames[Column_Nbr]
  i_ColValMsg = i_xColumnValMsg[Column_Nbr]

   //----------
   //  We will have a required field error if:
   //     a) The field contains a zero-length value AND
   //     b) During creation of the DataWindow, the field was
   //        specified as being a required field AND
   //           1) The field was specified as NIL is NULL (i.e.
   //              an empty string is to be treated as NULL) OR
   //           2) The field has truly not been modified.
   //----------

   //----------
   //  Initially, assume that this field will not generate a
   //  required field error.
   //----------

   i_ColRequiredError = FALSE

   //----------
   //  See if the user typed in anything.
   //----------

   IF Len(i_ColText) = 0 THEN

      //----------
      //  See if this is a required column.
      //----------

      IF i_xColumnRequired[Column_Nbr] THEN

         //----------
         //  If NilIsNull is specified, then we have a required
         //  field error because a blank string is considered a
         //  NULL value and this is a required field.
         //----------

         i_ColRequiredError = TRUE
      END IF
   END IF
END IF
END IF

Finished:

//----------
//  Initialize i_ColFormatNumber to 0 and i_ColFormatString to an
//  empty string.  These will be filled in by any PowerClass
//  validation routines that get called.
//----------

i_ColFormatNumber = 0.0
i_ColFormatString = ""

RETURN
end subroutine

public function integer handle_retrieve (ref unsignedlong next_mode, ref integer level);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Handle_Retrieve
//  Description   : Handles the retrieve processing for
//                  pcd_Refresh.
//
//  Parameters    : ref UNSIGNEDLONG Next_Mode -
//                        The mode that the DataWindow is going
//                        to.  This routine may modify it.
//
//                  ref INTEGER Level -
//                        The level that the DataWindow is at.
//                        This routine may modify it.
//
//  Return Value  : INTEGER -
//                        An error status of c_Success or c_Fatal.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_SaveSelected,   l_First
BOOLEAN  l_ReselectPrompt
INTEGER  l_Error
LONG     l_NumInParent,    l_RowNbr,         l_RowCount
LONG     l_SaveCursorRow,  l_NumOldSelected, l_Idx
LONG     l_OldSelected[],  l_ResetRows[]
STRING   l_KeyArray[]

//----------
//  Initialize the return error.
//----------

l_Error = c_Success

//----------
//  When a DataWindow is refreshed, there may be rows already
//  selected by the user.  If possible, we want to try to
//  re-select the same rows.  l_SaveSelected tells us if we
//  should try to re-select these rows.  We only want to
//  re-select rows if the refresh mode is not RFC.
//----------

l_SaveSelected = (i_RefreshMode <> c_RefreshRFC)

//----------
//  Remember the current cursor row.
//----------

l_SaveCursorRow = i_CursorRow

//----------
//  If we are not sharing a result set with another DataWindow,
//  we need to do the retrieve and reselect rows.
//----------

IF i_ShareType = c_ShareNone THEN
IF i_RetrieveMySelf          THEN

   //----------
   //  Set i_NumSelected to 0 to indicate that we have not
   //  tried to re-select any of the currently selected rows.
   //----------

   i_NumSelected = 0

   //----------
   //  Determine if there are any rows selected in the parent
   //  DataWindow.
   //----------

   IF i_ParentIsValid THEN
      l_NumInParent = i_ParentDW.i_NumSelected
   ELSE
      l_NumInParent = 0
   END IF

   //----------
   //  If we need to re-select the currently selected rows, see if
   //  there are any selections to remember.
   //----------

   IF l_SaveSelected THEN

      //----------
      //  See if there is at least one row are currently selected
      //  in this DataWindow.
      //----------

      l_RowNbr = GetSelectedRow(0)

      //----------
      //  If there are not any selected rows and the cursor
      //  is on the first row, then we do not have to
      //  remember anything or try to re-select anything.
      //----------

      IF l_RowNbr          = 0 AND &
         i_NumReselectRows = 0 AND &
         i_CursorRow       < 2 THEN
         l_SaveSelected = FALSE
      END IF
   END IF

   //----------
   //  If we need to re-select the currently selected rows, we have
   //  remember what they are.
   //----------

   IF l_SaveSelected THEN
      l_NumOldSelected = &
         Save_Selected_Rows(l_OldSelected[], l_KeyArray[])
   END IF

   //----------
   //  All of these variables will soon be invalid because the data
   //  in the DataWindow will have been retrieved or reset.
   //----------

   i_ShowRow   = 0
   i_CursorRow = 0
   i_AnchorRow = 0
   i_DWTopRow  = 0

   //----------
   //  Since we are filling the DataWindow with data (well,
   //  maybe), indicate that it is not empty.  EMPTY processing
   //  will take place later.
   //----------

   IF i_ExecuteFilter THEN
      IF i_AddedEmpty THEN
         DeleteRow(1)
      END IF
   END IF

   i_IsEmpty    = FALSE
   i_AddedEmpty = FALSE

   //----------
   //  If there are any New! rows in the DataWindow now, they
   //  won't be there after a retrieve.
   //----------

   IF i_SharePrimary.i_ShareDoSetKey THEN
      Share_Flags(c_ShareAllDoSetKey, FALSE)
   END IF

   IF i_NumShareSecondary > 0 THEN
      Share_State(c_SharePushRedraw,   c_BufferDraw)
      Share_State(c_SharePushRFC,      c_IgnoreRFC)
      Share_State(c_SharePushValidate, c_IgnoreVal)
      Share_State(c_SharePushVScroll,  c_IgnoreVScroll)
   ELSE
      i_IgnoreRFC      = (NOT c_IgnoreRFC)
      i_IgnoreRFCCount = i_IgnoreRFCCount + 1
      i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

      i_IgnoreVal      = (NOT c_IgnoreVal)
      i_IgnoreValCount = i_IgnoreValCount + 1
      i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

      i_IgnoreVScroll = (NOT c_IgnoreVScroll)
      i_IgnoreVSCount = i_IgnoreVSCount + 1
      i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll
   END IF

   //----------
   //  See if there are selected rows in our parent or if we are
   //  root-level DataWindow.  If there are not any rows selected
   //  in our parent or this is an instance DataWindow, then we 
   //  don't need to do a retrieve.
   //----------

   IF l_NumInParent > 0 OR NOT i_ParentIsValid THEN
   IF NOT i_IsInstance OR (i_IsInstance AND next_mode <> c_ModeNew) THEN

      //----------
      //  Tell the user what is going on.
      //----------

      IF NOT i_ExecuteFilter THEN

         //----------
         //  We tell the MDI object that we do not want the refresh
         //  prompt restored after the retrieve prompt is popped.
         //----------

         PCCA.MDI.fu_Visible(c_Invisible)

         PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Retrieve, c_ShowMark)
      END IF

      //----------
      //  Initialize variables for use by the developer.
      //----------

      PCCA.Error     = c_Success
      i_DisplayError = TRUE

      //----------
      //  Reset the DB Error codes.
      //----------

      i_CCDBError                  = FALSE
      i_DBErrorCode       = 0
      i_DBErrorMessage    = ""
      i_DBErrorRow        = 0
      i_DevDBErrorMessage = ""

      //----------
      //  Trigger the developer's event that does the retrieve
      //  for the DataWindow.  This event must be coded by the
      //  developer.
      //----------

      f_PO_TimerSubMark()

      IF i_ExecuteFilter THEN
         Filter()
      ELSE
         TriggerEvent("pcd_Retrieve")
      END IF

      f_PO_TimerMark(i_ClassName + ".Retrieve")

      //----------
      //  If there was an error on the retrieve and the developer
      //  did not tell us to prevent the an error display, then
      //  display the error.
      //----------

      IF PCCA.Error <> c_Success THEN
      IF i_DisplayError          THEN
         PCCA.MB.i_MB_Numbers[1] = i_DBErrorCode
         PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Handle_Retrieve()"
         PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
         PCCA.MB.i_MB_Strings[3] = i_ClassName
         PCCA.MB.i_MB_Strings[4] = i_Window.Title
         PCCA.MB.i_MB_Strings[5] = DataObject
         PCCA.MB.i_MB_Strings[6] = i_DBErrorMessage
         PCCA.MB.i_MB_Strings[7] = i_DevDBErrorMessage
         PCCA.MB.fu_MessageBox               &
            (PCCA.MB.c_MBI_DW_RetrieveError, &
             1, PCCA.MB.i_MB_Numbers[],      &
             7, PCCA.MB.i_MB_Strings[])

         //----------
         //  Make sure that PCCA.Error still indicates failure.
         //----------

         PCCA.Error = c_Fatal
      END IF
      END IF

      //----------
      //  See if we had a failure.
      //----------

      IF PCCA.Error <> c_Success THEN

         //----------
         //  We now know that we have a failure.  Set l_Error to
         //  remember the failure because PCCA.Error may get set
         //  back to c_Success later.
         //----------

         l_Error = c_Fatal

         //----------
         //  Retrieve failed - reset the DataWindow.
         //----------

         Reset()
         Next_Mode = c_ModeView
      ELSE

         //----------
         //  If i_ShowRow is not 0, then the developer selected
         //  his own rows in pcd_Retrieve.  Mark l_SaveSelected
         //  as FALSE so that we will not reselect rows.
         //----------

         IF i_ShowRow > 0 THEN
            l_SaveSelected = FALSE

            //----------
            //  Because the selected rows may have changed, we
            //  have to retrieve the newly selected rows into
            //  our children.
            //----------

            i_RetrieveChildren = TRUE
         END IF

         //----------
         //  If we have share children, we need to tell them to
         //  to "retrieve" themselves, since their cursor rows
         //  and selections were wiped out by this retrieve.
         //----------

         FOR l_Idx = 1 TO i_NumShareSecondary
            i_ShareSecondary[l_Idx].i_RetrieveMySelf = TRUE
         NEXT

         //----------
         //  If the developer said that we were to go to "NEW"
         //  mode if there were not any rows retrieved, then set
         //  ourselves up to do it.
         //----------

         IF NOT i_ExecuteFilter THEN
            l_RowCount = RowCount()
            IF l_RowCount = 0   THEN
            IF i_NewModeOnEmpty THEN
               i_RefreshMode   = c_RefreshNew
               i_RequestedMode = c_ModeNew

               IF NOT i_AllowNew THEN
                  Level = 0

                  IF NOT i_AllowModify THEN
                     Next_Mode = c_ModeView
                  ELSE
                     Next_Mode = c_ModeModify
                  END IF
               ELSE
                  Level     = 1
                  Next_Mode = c_ModeNew
               END IF
            END IF
            END IF
         END IF

         //----------
         //  Remove the retrieving prompt.
         //----------

         PCCA.MDI.fu_Pop()
      END IF
   ELSE

      //----------
      //  Reset the DataWindow if there are not any rows selected
      //  in our parent.
      //----------

      Reset()

      //----------
      //  If this is an instance DataWindow and it has a new record
      //  then deselect any parent records.
      //----------
      
      IF i_ParentIsValid AND i_IsInstance AND next_mode = c_ModeNew THEN
         i_ParentDW.SelectRow(0, FALSE)
         i_ParentDW.i_AnchorRow       = 0
         i_ParentDW.i_NumSelected     = 0
         i_ParentDW.i_NumReselectRows = 0
         i_ParentDW.i_ShowRow         = 0
      END IF

   END IF
	// MS
	ELSE

      //----------
      //  Reset the DataWindow if there are not any rows selected
      //  in our parent.
      //----------

      Reset()

   END IF

   IF i_NumShareSecondary > 0 THEN
      Share_State(c_SharePopRedraw,   c_StateUnused)
      Share_State(c_SharePopRFC,      c_StateUnused)
      Share_State(c_SharePopValidate, c_StateUnused)
      Share_State(c_SharePopVScroll,  c_StateUnused)
   ELSE
      IF i_IgnoreVSCount > 1 THEN
         i_IgnoreVSCount = i_IgnoreVSCount - 1
         i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
      ELSE
         i_IgnoreVSCount = 0
         i_IgnoreVScroll = FALSE
      END IF
      IF NOT i_IgnoreVScroll THEN
         i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
      END IF

      IF i_IgnoreValCount > 1 THEN
         i_IgnoreValCount = i_IgnoreValCount - 1
         i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
      ELSE
         i_IgnoreValCount = 0
         i_IgnoreVal      = FALSE
      END IF

      IF i_IgnoreRFCCount > 1 THEN
         i_IgnoreRFCCount = i_IgnoreRFCCount - 1
         i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
      ELSE
         i_IgnoreRFCCount = 0
         i_IgnoreRFC      = FALSE
      END IF
   END IF

   //----------
   //  See how many rows we have now.
   //----------

   l_RowCount = RowCount()

   IF l_RowCount = 0 THEN
      i_IsEmpty = TRUE
   END IF

   //----------
   //  If we are supposed to re-select the rows that used to be
   //  selected and there are actually some rows that could be
   //  selected, give it a try.
   //----------

   IF l_SaveSelected THEN
   IF NOT i_IsEmpty  THEN

      //----------
      //  Tell the user what is going on.
      //----------

      l_ReselectPrompt = TRUE
      PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_ReselectRows, c_ShowMark)

      //----------
      //  Reselect the rows.
      //----------

      Restore_Selected_Rows &
         (l_NumOldSelected, l_OldSelected[], l_KeyArray[])
   END IF
   END IF
END IF
END IF

//----------
//  See how many rows are in there are.
//----------

l_RowCount = RowCount()

//----------
//  Now we have to find rows that are in the reselection list.
//  A good example of these are New! rows which have been
//  entered in one of the children DataWindows.
//----------

IF l_SaveSelected THEN
IF NOT i_IsEmpty  THEN

   l_First = TRUE
   FOR l_Idx = 1 TO i_NumReselectRows

      //----------
      //  We have to use the brute force method because we have
      //  no idea where the record might be.
      //----------

      l_RowNbr = Find_Key_Row(l_Idx, i_ReselectKeys[], &
                              1,     l_RowCount)

      //----------
      //  If l_RowNbr is greater than 0, then we have found the
      //  reselection row in the DataWindow's new data.
      //----------

      IF l_RowNbr > 0 THEN

         //----------
         //  If this is the first re-selected row that we have
         //  found, then make it the show row and move the cursor
         //  to it.
         //----------

         IF l_First THEN
            l_First     = FALSE
            i_ShowRow   = l_RowNbr
            i_CursorRow = l_RowNbr
         END IF

         //----------
         //  Re-select it and bump the number of rows that we
         //  have been able to re-select.
         //----------

         IF NOT IsSelected(l_RowNbr) THEN
            i_NumSelected = i_NumSelected + 1
            SelectRow(l_RowNbr, TRUE)
         END IF
      END IF
   NEXT

END IF
END IF

//----------
//  If we were unable to find the old i_CursorRow, then set the
//  cursor position to the same row position it was before the
//  retrieve.  Legality checking will be done later.
//----------

IF i_CursorRow = 0 THEN
   IF l_SaveSelected THEN
      i_CursorRow = l_SaveCursorRow
   ELSE
      i_CursorRow = 1
   END IF
END IF

//----------
//  If we were able to re-select some rows, we need to do some
//  processing.
//----------

IF i_NumSelected > 0 THEN

   //----------
   //  If we are unable to find a i_ShowRow, just set i_ShowRow
   //  to the first selected row.
   //----------

   IF i_ShowRow = 0 THEN
      i_ShowRow = GetSelectedRow(0)
   END IF

   //----------
   //  If we are not a multi-select DataWindow, make sure that
   //  we did not select more than one row.
   //----------

   IF NOT i_MultiSelect AND i_NumSelected > 1 THEN
      SelectRow(0, FALSE)

      IF i_HighlightSelected THEN
         SelectRow(i_ShowRow, TRUE)
      END IF
   ELSE

      //----------
      //  If i_HighlightSelected is FALSE, then we don't use
      //  SelectRow().
      //----------

      IF NOT i_HighlightSelected THEN
         SelectRow(0, FALSE)
      END IF
   END IF
END IF

//----------
//  If we are doing a share, we did not have to worry about
//  re-selecting the previously selected rows.  However, we
//  want to move our cursor row so that it is on the same row
//  as our primary share DataWindow.
//----------

IF i_ShareType <> c_ShareNone THEN
   IF NOT i_MultiRowDisplay THEN
      i_CursorRow = i_ShareParent.i_ShowRow
      IF i_CursorRow < 1 THEN
         i_CursorRow = i_ShareParent.i_CursorRow
      END IF
   END IF
END IF

//----------
//  If we automatically re-selected rows and the number of
//  selected rows changed, we have some work to do.
//----------

IF l_SaveSelected THEN

   //----------
   //  We're all done displaying retrieve and re-select prompts.
   //----------

   IF l_ReselectPrompt THEN
      PCCA.MDI.fu_Pop()
   END IF

   IF (i_NumSelected <> l_NumOldSelected OR i_NumReselectRows > 0) THEN

      //----------
      //  Because the number of selected rows changed, we have to
      //  retrieve the newly selected rows into our children.
      //----------

      i_RetrieveChildren = TRUE

      //----------
      //  If this is a tabular-form style DataWindow, tell the
      //  user that the number of selected rows changed since
      //  they may not be able to see which rows used to be
      //  selected and what rows are now selected.
      //----------

      //************************************************************
      //  Developer Note:
      //    Most developers did not want these messages, so by
      //    default they have been disabled and will not display.
      //    If you want them, you can enable them by modifying the
      //    constructor event of the uo_MB object or executing the
      //    following statements after Init_PCCA():
      //
      //       PCCA.MB.i_MessageEnabled                  &
      //          [PCCA.MB.c_MBI_DW_ReselectedChanged] = &
      //              PCCA.MB.c_MB_Enabled
      //
      //       PCCA.MB.i_MessageEnabled               &
      //          [PCCA.MB.c_MBI_DW_ReselectedNone] = &
      //              PCCA.MB.c_MB_Enabled
      //************************************************************

      IF i_MultiRowDisplay THEN
         PCCA.MB.i_MB_Numbers[1] = l_NumOldSelected
         PCCA.MB.i_MB_Numbers[2] = i_NumSelected
         PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Handle_Retrieve()"
         PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
         PCCA.MB.i_MB_Strings[3] = i_ClassName
         PCCA.MB.i_MB_Strings[4] = i_Window.Title
         PCCA.MB.i_MB_Strings[5] = DataObject

         IF i_NumSelected >  0 THEN
            PCCA.MB.fu_MessageBox                   &
               (PCCA.MB.c_MBI_DW_ReSelectedChanged, &
                2, PCCA.MB.i_MB_Numbers[],          &
                5, PCCA.MB.i_MB_Strings[])
         ELSE
            PCCA.MB.fu_MessageBox                &
               (PCCA.MB.c_MBI_DW_ReSelectedNone, &
                0, PCCA.MB.i_MB_Numbers[],       &
                5, PCCA.MB.i_MB_Strings[])
         END IF
      END IF
   END IF
END IF

//----------
//  Make sure that the cursor row is legal.
//----------

IF i_CursorRow < 1 THEN
   i_CursorRow = 1
END IF

IF i_CursorRow > l_RowCount THEN
   i_CursorRow = l_RowCount
END IF

//----------
//  If we have a valid cursor row (i.e. the DataWindow is not
//  empty), trigger RowFocusChanged! to move us there.
//----------

IF i_CursorRow > 0 THEN

   //----------
   //  Make sure that RFC processing is on while we move to the
   //  cursor row.
   //----------

   i_IgnoreRFC      = (NOT c_ProcessRFC)
   i_IgnoreRFCCount = i_IgnoreRFCCount + 1
   i_IgnoreRFCStack[i_IgnoreRFCCount] = c_ProcessRFC

   //----------
   //  If we automatically re-selected rows, then we do not want
   //  RFC to select rows.  Set i_RFCEvent to c_RFC_NoSelect to
   //  make sure that the RowFocusChanged! event will not perform
   //  row selections.
   //----------

   IF l_SaveSelected THEN
      i_RFCEvent = c_RFC_NoSelect

      //----------
      //  PowerBuilder has a scroll bug that occurs in
      //  the following situation:
      //     1) Turn redraw off.
      //     2) Do a Retrieve().
      //     3) Do a ScrollToRow().
      //     4) Turn redraw on.
      //     5) PowerBuilder warps the cursor to some
      //        random row.
      //
      //  Turn redraw on now, so that PowerBuilder can
      //  regain its bearings.
      //----------

      IF i_MultiRowDisplay THEN
         SetRedraw(c_DrawNow)
         i_RedrawCount = i_RedrawCount + 1
         i_RedrawStack[i_RedrawCount] = c_DrawNow

         IF i_RedrawCount > 1 THEN
            i_RedrawCount = i_RedrawCount - 1
            SetRedraw(i_RedrawStack[i_RedrawCount])
         ELSE
            i_RedrawCount = 0
            SetRedraw(TRUE)
         END IF
      END IF
   ELSE

      //----------
      //  Set the event type to c_RFC_RowFocusChanged and let
      //  the developer's mode of selection determine if a row
      //  is selected.
      //----------

      i_RFCEvent = c_RFC_RowFocusChanged
   END IF

   //----------
   //  Set i_MoveRow to the row we want to move to and set
   //  i_CursorRow back to its orginal value so the RFC will
   //  not be optimized.  Let the RowFocusChanged! event take
   //  care of positioning to the new row.
   //----------

   i_MoveRow   = i_CursorRow
   i_CursorRow = l_SaveCursorRow
   TriggerEvent(RowFocusChanged!)

   //----------
   //  Restore RFC processing to its previous state.
   //----------

   IF i_IgnoreRFCCount > 1 THEN
      i_IgnoreRFCCount = i_IgnoreRFCCount - 1
      i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
   ELSE
      i_IgnoreRFCCount = 0
      i_IgnoreRFC      = FALSE
   END IF
END IF

//----------
//  If there are Instance DataWindows, find which one (if any)
//  corresponds to the currently selected row.
//----------

IF i_NumInstance > 0 THEN
   Find_Instance(i_ShowRow)
END IF

//----------
//  Set up i_SelectedRows[].
//----------

i_NumSelected = Get_Retrieve_Rows(i_SelectedRows[])

//----------
//  Tell the MDI object to re-display the refresh prompt if
//  it needs to.
//----------

PCCA.MDI.fu_Visible(c_Mark)

//----------
//  This completes the retrieval process for this
//  DataWindow.
//----------

RETURN l_Error
end function

public function integer load_rows (uo_dw_main skip_dw, uo_dw_main skip_dw_children);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Load_Rows
//  Description   : If rows have been selected/multi-selected but
//                  have not been retrieved into the children
//                  DataWindows, this routine will retrieve them.
//
//  Parameters    : UO_DW_MAIN Skip_DW -
//                        During refresh, tells pcd_Refresh to
//                        skip this branch and all of its
//                        children DataWindows.
//
//                  UO_DW_MAIN Skip_DW_Children -
//                        During refresh, tells pcd_Refresh to
//                        skip all of this branch's children
//                        DataWindows.
//
//  Return Value  : INTEGER -
//                        Error return - c_Success or c_Fatal.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER     l_SaveError, l_Error
UO_DW_MAIN  l_NeedRetrieveDW

//----------
//  Assume that no errors will occur.
//----------

l_Error = c_Success

//----------
//  See if there is a DataWindow which has not had its selections
//  loaded into its children DataWindows.
//----------

IF PCCA.PCMGR.i_DW_NeedRetrieve AND &
   PCCA.PCMGR.i_DW_NeedRetrieveDW <> THIS THEN
   PCCA.PCMGR.i_DW_NeedRetrieve = FALSE

   //----------
   //  Save PCCA.Error into a local so that it can be restored.
   //----------

   l_SaveError = PCCA.Error
   PCCA.Error  = c_Success

   //----------
   //  Make sure that the DataWindow's children actually need to
   //  be retrieved.
   //----------

   IF PCCA.PCMGR.i_DW_NeedRetrieveDW.i_RetrieveChildren THEN

      //----------
      //  This may take a while...
      //----------

      SetPointer(HourGlass!)

      //----------
      //  Save the DataWindow that needs refreshing into a
      //  local because PCCA.PCMGR.i_DW_NeedRetrieveDW may
      //  be reset before we are done with it.
      //----------

      l_NeedRetrieveDW = PCCA.PCMGR.i_DW_NeedRetrieveDW

      //----------
      //  Set up the event control information for pcd_Refresh.
      //----------

      l_NeedRetrieveDW.is_EventControl.Refresh_Cmd = &
         c_RefreshRFC
      l_NeedRetrieveDW.is_EventControl.Only_Do_Children = TRUE

      IF IsValid(Skip_DW) THEN
         l_NeedRetrieveDW.is_EventControl.Skip_DW_Valid = TRUE
         l_NeedRetrieveDW.is_EventControl.Skip_DW       = Skip_DW
      END IF
      IF IsValid(Skip_DW_Children) THEN
         l_NeedRetrieveDW.is_EventControl.Skip_DW_Children_Valid =&
            TRUE
         l_NeedRetrieveDW.is_EventControl.Skip_DW_Children       =&
            Skip_DW_Children
      END IF

      //----------
      //  Call pcd_Refresh to retrieve the new rows into the
      //  children DataWindows.
      //----------

      l_NeedRetrieveDW.TriggerEvent("pcd_Refresh")

      //----------
      //  Make sure the data actually got retrieved.  If there
      //  was an error, set the focus to the DataWindow where
      //  the user did the multiselections.
      //----------

      IF PCCA.Error <> c_Success THEN
         Change_DW_Focus(l_NeedRetrieveDW)
         PCCA.Error = c_Fatal
      END IF
   END IF

   //----------
   //  Put the error to be returned in l_Error and restore the
   //  original value of PCCA.Error.
   //----------

   l_Error    = PCCA.Error
   PCCA.Error = l_SaveError
END IF

RETURN l_Error
end function

public subroutine map_keys ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Map_Keys
//  Description   : This routine maps the keys from a child
//                  DataWindow to the keys in the parent.  If
//                  PowerClass can not compute it correctly, the
//                  developer should set the i_KeyMapValid and
//                  i_GotKeyMap instance variables to TRUE and
//                  set up i_xKeyMap[] array manually.  This
//                  should be done in the pcd_SetWindow event of
//                  w_Main after Set_DW_Options() has been called.
//                  To set up the key map, PowerClass cycles
//                  through the key columns in the child
//                  DataWindow.  For each key column, it tries
//                  to find a column with the corresponding name
//                  in the parent (e.g. "emp_id").  If PowerClass
//                  is unable to find a match, it then tries to
//                  find a column in the parent which has the
//                  same table and column name
//                  (e.g. "employee.employee_id").  As a last try,
//                  it will try to find a column in the parent
//                  whose database column matchs (e.g.
//                  "employee_id").  The following example
//                  demonstrates how to manually set up the
//                  i_xKeyMap[] array:
//
//                     Keys in child:       Keys in parent:
//                      Col #4 - "emp_id"    Col #6 - "dept"
//                      Col #5 - "dept_id"   Col #8 - "emp"
//
//                    i_xKeyMap[1] = 8
//                    i_xKeyMap[2] = 6
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Found,  l_Mapped[]
INTEGER  l_KeyCol, l_PKeyCol,   l_Period, l_Idx, l_Jdx
STRING   l_TmpCol, l_ColName[], l_PColName[]

//----------
//  If we have already tried to get the map information, there
//  is no need to try again.
//----------

IF NOT i_GotKeyMap THEN
   i_GotKeyMap   = TRUE
   i_KeyMapValid = FALSE

   //----------
   //  We can only map keys if there is a parent.
   //----------

   IF i_ParentIsValid THEN

      //----------
      //  Make sure we have the column information for both this
      //  DataWindow and the parent DataWindow.
      //----------

      IF NOT i_GotColInfo THEN
         Get_Col_Info()
      END IF
      IF NOT i_ParentDW.i_GotColInfo THEN
         i_ParentDW.Get_Col_Info()
      END IF

      //----------
      //  If the number of key columns don't match between this
      //  DataWindow and the parent DataWindow, we can't map the
      //  keys.
      //----------

      IF i_xNumKeyColumns = i_ParentDW.i_xNumKeyColumns THEN

         //----------
         //  For each of the key columns, get the column name, etc.
         //----------

         FOR l_Idx = 1 TO i_xNumKeyColumns
            l_Mapped[l_Idx] = FALSE
            Get_Val_Info(i_xKeyColumns[l_Idx])
            i_ParentDW.Get_Val_Info &
               (i_ParentDW.i_xKeyColumns[l_Idx])
         NEXT

         //----------
         //  Try to find key columns whose names match in both this
         //  DataWindow and the parent DataWindow.
         //----------

         l_Found = TRUE
         FOR l_Idx = 1 TO i_xNumKeyColumns
            l_PKeyCol = i_ParentDW.i_xKeyColumns[l_Idx]

            FOR l_Jdx = 1 TO i_xNumKeyColumns
               l_KeyCol = i_xKeyColumns[l_Jdx]

               IF i_xColumnNames[l_KeyCol] = &
                  i_ParentDW.i_xColumnNames[l_PKeyCol] THEN
                  i_xKeyMap[l_Idx] = l_KeyCol
                  l_Mapped[l_Idx]  = TRUE
                  EXIT
               END IF
            NEXT
            IF NOT l_Mapped[l_Idx] THEN
               l_Found = FALSE
            END IF
         NEXT

         //----------
         //  If we couldn't map all of the key columns, then we
         //  have to keep trying.  Try i_xDBName[] this time.
         //  i_xDBName[] is the "table.column" name in the
         //  database.
         //----------

         IF NOT l_Found THEN

            //----------
            //  Get the "table.column" names for the key columns
            //  into local arrays.
            //----------

            FOR l_Idx = 1 TO i_xNumKeyColumns
               IF NOT l_Mapped[l_Idx] THEN
                  l_KeyCol          = i_xKeyColumns[l_Idx]
                  l_TmpCol          = i_xDBNames[l_KeyCol]
                  l_ColName[l_Idx]  = Lower(l_TmpCol)

                  l_PKeyCol         = &
                     i_ParentDW.i_xKeyColumns[l_Idx]
                  l_TmpCol          = &
                     i_ParentDW.i_xDBNames[l_PKeyCol]
                  l_PColName[l_Idx] = Lower(l_TmpCol)
               END IF
            NEXT

            //----------
            //  Now try to map the keys using the DB
            //  "table.column" names.
            //----------

            l_Found = TRUE
            FOR l_Idx = 1 TO i_xNumKeyColumns
               IF NOT l_Mapped[l_Idx] THEN
                  FOR l_Jdx = 1 TO i_xNumKeyColumns
                     IF l_ColName[l_Idx] = l_PColName[l_Idx] THEN
                        l_KeyCol         = i_xKeyColumns[l_Jdx]
                        i_xKeyMap[l_Idx] = l_KeyCol
                        l_Mapped[l_Idx]  = TRUE
                        EXIT
                     END IF
                  NEXT
                  IF NOT l_Mapped[l_Idx] THEN
                     l_Found = FALSE
                  END IF
               END IF
            NEXT
         END IF

         //----------
         //  If we still couldn't map all of the key columns, then
         //  we have to keep trying.  Try using only the "column"
         //  name in the database.
         //----------

         IF NOT l_Found THEN

            //----------
            //  Get the "column" names for the key columns into
            //  local arrays.
            //----------

            FOR l_Idx = 1 TO i_xNumKeyColumns
               IF NOT l_Mapped[l_Idx] THEN
                  l_KeyCol  = i_xKeyColumns[l_Idx]
                  l_TmpCol  = i_xDBNames[l_KeyCol]
                  l_Period  = Pos(l_TmpCol, ".")
                  IF l_Period > 0 THEN
                     l_TmpCol = Mid(l_TmpCol, l_Period + 1)
                  END IF
                  l_ColName[l_Idx] = Lower(l_TmpCol)

                  l_PKeyCol = i_ParentDW.i_xKeyColumns[l_Idx]
                  l_TmpCol  = i_ParentDW.i_xDBNames[l_PKeyCol]
                  l_Period  = Pos(l_TmpCol, ".")
                  IF l_Period > 0 THEN
                     l_TmpCol = Mid(l_TmpCol, l_Period + 1)
                     END IF
                  l_PColName[l_Idx] = Lower(l_TmpCol)
               END IF
            NEXT

            //----------
            //  Now try to map the keys using the DB "column"
            //  names.
            //----------

            l_Found = TRUE
            FOR l_Idx = 1 TO i_xNumKeyColumns
               IF NOT l_Mapped[l_Idx] THEN
                  FOR l_Jdx = 1 TO i_xNumKeyColumns
                     IF l_ColName[l_Idx] = l_PColName[l_Idx] THEN
                        l_KeyCol         = i_xKeyColumns[l_Jdx]
                        i_xKeyMap[l_Idx] = l_KeyCol
                        l_Mapped[l_Idx]  = TRUE
                        EXIT
                     END IF
                  NEXT
                  IF NOT l_Mapped[l_Idx] THEN
                     l_Found = FALSE
                  END IF
               END IF
            NEXT
         END IF
         i_KeyMapValid = l_Found
      END IF
   END IF
END IF

RETURN
end subroutine

public subroutine pass_minimize ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Pass_Minimize
//  Description   : When the DataWindow is minimized,
//                  this routine will pass it to
//                  its children DataWindows.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

//----------
//  PCCA.Do_Event will tell the window's Resize event not to
//  call this routine again.
//----------

IF i_Window.WindowState <> Minimized! THEN
   PCCA.Do_Event        = TRUE
   i_Window.WindowState = Minimized!
   PCCA.Do_Event        = FALSE
END IF

//----------
//  Cascade the window state to the children DataWindows.
//----------

FOR l_Idx = 1 TO i_NumChildren
   i_ChildDW[l_Idx].Pass_Minimize()
NEXT

FOR l_Idx = 1 TO i_NumInstance
   i_InstanceDW[l_Idx].Pass_Minimize()
NEXT

RETURN
end subroutine

public subroutine pass_normal ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Pass_Normal
//  Description   : When the DataWindow is normalized,
//                  this routine will pass it to
//                  its children DataWindows.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

//----------
//  PCCA.Do_Event will tell the window's Resize event not to
//  call this routine again.
//----------

IF i_Window.WindowState <> Normal! THEN
   PCCA.Do_Event        = TRUE
   i_Window.WindowState = Normal!
   PCCA.Do_Event        = FALSE
END IF

//----------
//  Cascade the window state to the children DataWindows.
//----------

FOR l_Idx = 1 TO i_NumChildren
   i_ChildDW[l_Idx].Pass_Normal()
NEXT

FOR l_Idx = 1 TO i_NumInstance
   i_InstanceDW[l_Idx].Pass_Normal()
NEXT

RETURN
end subroutine

public function integer process_modes (ref unsignedlong next_mode, long insert_row_nbr);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Process_Modes
//  Description   : Changes modes for pcd_Refresh.
//
//  Parameters    : ref UNSIGNEDLONG Next_Mode -
//                        The mode that the DataWindow is going
//                        to.  This routine may modify it.
//
//                  LONG Insert_Row_Nbr -
//                        If the DataWindow is going to "NEW"
//                        mode, Insert_Row_Nbr tells the
//                        DataWindow where to insert the row.
//
//  Return Value  : INTEGER -
//                        An error status of c_Success or c_Fatal.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Error,    l_ColNbr
LONG          l_RowCount, l_NumNewRows, l_Idx
LONG          l_NewRows[]
DWITEMSTATUS  l_ItemStatus

//----------
//  Found out how many rows there are currently in the DataWindow.
//----------

l_RowCount = RowCount()

//----------
//  If we were requested to go to "NEW" mode (i.e.  i_RefreshMode
//  is c_RefreshNew) and we are actually doing it (i.e. Next_Mode
//  is c_ModeNew), then we need to set the DataWindow up for "NEW"
//  mode
//----------

IF i_RefreshMode = c_RefreshNew AND &
   Next_Mode = c_ModeNew THEN

   //----------
   //  If there already New! rows, make sure that the developer
   //  said that it is Ok to insert another one.
   //----------

   IF i_DoSetKey AND i_OnlyOneNewRow THEN
      PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Process_Modes()"
      PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
      PCCA.MB.i_MB_Strings[3] = i_ClassName
      PCCA.MB.i_MB_Strings[4] = i_Window.Title
      PCCA.MB.i_MB_Strings[5] = DataObject
      PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_OnlyOneNew, &
                         0, PCCA.MB.i_MB_Numbers[],   &
                         5, PCCA.MB.i_MB_Strings[])
      Next_Mode = c_ModeModify
   ELSE

      //----------
      //  If the developer said that new rows were important,
      //  mark the DataWindow as modified.
      //----------

      IF NOT i_IgnoreNewRows THEN
         Share_Flags(c_ShareModified, TRUE)
      END IF

      //----------
      //  Indicate that we have a new record and that keys
      //  will need to be set by the developer.
      //----------

      Share_Flags(c_ShareAllDoSetKey, TRUE)

      //----------
      //  Insert_Row_Nbr specifies where the new row is to be
      //  inserted: insure that it is a legal value.
      //----------

      IF Insert_Row_Nbr < 0 THEN
         Insert_Row_Nbr = 0
      END IF
      IF Insert_Row_Nbr > l_RowCount THEN
         Insert_Row_Nbr = l_RowCount
      END IF

      //----------
      //  If the DataWindow was empty, it isn't going to be
      //  any more since we are inserting a new record.
      //----------

      Share_State(c_SharePushRedraw,   c_BufferDraw)
      Share_State(c_SharePushRFC,      c_IgnoreRFC)
      Share_State(c_SharePushValidate, c_IgnoreVal)
      Share_State(c_SharePushVScroll,  c_IgnoreVScroll)

      IF i_IsEmpty THEN
         IF NOT i_AddedEmpty THEN
            Insert_Row_Nbr = InsertRow(Insert_Row_Nbr)
         ELSE
            Insert_Row_Nbr = 1
         END IF

         Share_State(c_ShareNotEmpty, c_StateUnused)
      ELSE

         //----------
         //  This DataWindow already has data in it.  Just
         //  insert the new record at the requested position.
         //----------

         Insert_Row_Nbr = InsertRow(Insert_Row_Nbr)
      END IF

      //----------
      //  If Insert_Row_Nbr is greater than 0, then we were able
      //  to insert a new record.
      //----------

      IF Insert_Row_Nbr > 0 THEN

         //----------
         //  Since a row has been inserted, we need to update
         //  PowerClass's row variables (e.g. i_CursorRow).
         //----------

         Share_Insert(Insert_Row_Nbr)

         //----------
         //  We want the cursor positioned at the new
         //  record.  Turn RowFocusChanged! processing
         //  on while we move to and select the record
         //  which was just inserted.
         //----------

         i_IgnoreRFC      = (NOT c_ProcessRFC)
         i_IgnoreRFCCount = i_IgnoreRFCCount + 1
         i_IgnoreRFCStack[i_IgnoreRFCCount] = c_ProcessRFC

         i_MoveRow = - Insert_Row_Nbr
         TriggerEvent(RowFocusChanged!)

         IF i_IgnoreRFCCount > 1 THEN
            i_IgnoreRFCCount = i_IgnoreRFCCount - 1
            i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
         ELSE
            i_IgnoreRFCCount = 0
            i_IgnoreRFC      = FALSE
         END IF

         //----------
         //  Enable the DataWindow for modification and turn
         //  on the "MODIFY" mode borders for each field.
         //----------

         IF i_DWState <> c_DWStateEnabled THEN
            TriggerEvent("pcd_Enable")
         END IF

         //----------
         //  Put the cursor to the first field, if it is not
         //  already there.
         //----------

         IF i_FirstTabColumn > 0 THEN
            l_ColNbr = GetColumn()
            IF i_FirstTabColumn <> l_ColNbr THEN
               SetColumn(i_FirstTabColumn)
            END IF
         END IF
      ELSE

         //----------
         //  Since we were unable to insert a new record, we
         //  can't be in "NEW" mode.
         //----------

         IF i_CurrentMode <> c_ModeNew THEN
            Next_Mode = i_CurrentMode
         ELSE
            Next_Mode = c_ModeModify
         END IF
      END IF

      Share_State(c_SharePopRedraw,   c_StateUnused)
      Share_State(c_SharePopRFC,      c_StateUnused)
      Share_State(c_SharePopValidate, c_StateUnused)
      Share_State(c_SharePopVScroll,  c_StateUnused)
   END IF
ELSE

   //----------
   //  We can't be either "MODIFY" or "VIEW" mode if the DataWindow
   //  is empty.
   //----------

   IF l_RowCount > 0 THEN
   IF NOT i_IsEmpty  THEN

      //----------
      //  Check for "MODIFY" mode.
      //----------

      IF Next_Mode = c_ModeModify THEN

         //----------
         //  If the mode of the DataWindow is, or is going to be,
         //  "MODIFY", we may need to lock rows.  If new rows have
         //  been retrieved and the DataWindow is not empty or we
         //  are being told to go to "MODIFY" mode, then we need
         //  to trigger the developer's event to lock rows.
         //----------

         IF i_RetrieveMySelf OR &
            i_RefreshMode = c_RefreshModify THEN

            //----------
            //  Initialize variables for use by the developer.
            //----------

            PCCA.Error     = c_Success
            i_DisplayError = TRUE

            //----------
            //  Reset the DB Error codes.
            //----------

            i_CCDBError                  = FALSE
            i_DBErrorCode       = 0
            i_DBErrorMessage    = ""
            i_DBErrorRow        = 0
            i_DevDBErrorMessage = ""

            //----------
            //  Trigger the developer's event to do the row
            //  locking.  This event must be coded by the
            //  developer if row locking is desired.
            //----------

            TriggerEvent("pcd_Lock")

            //----------
            //  If there was an error during the row locking and
            //  the developer did not tell us to prevent the an
            //  error display, then display the error.
            //----------

            IF PCCA.Error <> c_Success THEN
               IF i_DisplayError          THEN
                  PCCA.MB.i_MB_Numbers[1] = i_DBErrorCode
                  PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Process_Modes()"
                  PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
                  PCCA.MB.i_MB_Strings[3] = i_ClassName
                  PCCA.MB.i_MB_Strings[4] = i_Window.Title
                  PCCA.MB.i_MB_Strings[5] = DataObject
                  PCCA.MB.i_MB_Strings[6] = i_DBErrorMessage
                  PCCA.MB.i_MB_Strings[7] = i_DevDBErrorMessage
                  PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_LockError, &
                                     1, PCCA.MB.i_MB_Numbers[],  &
                                     7, PCCA.MB.i_MB_Strings[])
               END IF

               //----------
               //  Since we could not lock rows, we can not go to
               //  "MODIFY" mode: go to "VIEW" mode.
               //----------

               l_Error   = c_Fatal
               Next_Mode = c_ModeView
            END IF
         END IF
         END IF

         //----------
         //  If we are going to "MODIFY" mode and we are not
         //  currently in "MODIFY" mode, then we have work to do.
         //----------

         IF Next_Mode     =  c_ModeModify THEN
         IF i_CurrentMode <> c_ModeModify THEN

            //----------
            //  Enable the DataWindow for modification and turn
            //  on the "MODIFY" mode borders for each field.
            //----------

            IF i_DWState <> c_DWStateEnabled THEN
               TriggerEvent("pcd_Enable")
            END IF

            //----------
            //  Put the cursor to the first field, if it is not
            //  already there.
            //----------

            IF i_FirstTabColumn > 0 THEN
               l_ColNbr = GetColumn()
               IF i_FirstTabColumn <> l_ColNbr THEN
                  SetColumn(i_FirstTabColumn)
               END IF
            END IF
         END IF
         END IF
      END IF

      //----------
      //  Check for "VIEW" mode.
      //----------

      IF Next_Mode     =  c_ModeView THEN
      IF i_CurrentMode <> c_ModeView THEN

         //----------
         //  If this DataWindow contains New! rows, we want to
         //  remove them before we go to "VIEW" mode.
         //----------

         IF i_SharePrimary.i_ShareDoSetKey THEN
            l_NumNewRows = 0
            FOR l_Idx = 1 TO l_RowCount
               l_ItemStatus = GetItemStatus(l_Idx, 0, Primary!)
               IF l_ItemStatus = New! THEN
                  l_NumNewRows            = l_NumNewRows + 1
                  l_NewRows[l_NumNewRows] = l_Idx
               END IF
            NEXT
            IF l_NumNewRows > 0 THEN
               Delete_DW_Rows(l_NumNewRows,    l_NewRows[], &
                                       c_IgnoreChanges, &
                                       c_RefreshChildren)
               l_RowCount = l_RowCount - l_NumNewRows
            END IF

            //----------
            //  No more New! rows - reset i_DoSetKey.
            //----------

            Share_Flags(c_ShareAllDoSetKey, FALSE)
         END IF

         //----------
         //  Disable the DataWindow for modification and turn on
         //  the "VIEW" mode borders for each field.
         //----------

         IF i_DWState <> c_DWStateDisabled THEN
            TriggerEvent("pcd_Disable")
         END IF
      END IF
   END IF
   END IF
END IF

RETURN l_Error
end function

public subroutine remove_dw_rows (long num_to_remove, long remove_rows[], unsignedlong do_refresh);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Remove_DW_Rows
//  Description   : This routine provides a PowerClass
//                  interface to the DELETEROW() function.
//                  This routine will remove the specified
//                  set of rows from the DataWindow and
//                  performs the necessary clean up (e.g.
//                  cursor row management and refreshing
//                  of children DataWindows).
//                  Note: Delete_DW_Rows() is now the preferred
//                        routine to do this and it takes the
//                        same parameters.
//
//  Parameters    : LONG Num_To_Remove -
//                        The number of rows that have been
//                        passed in the Remove_Rows[] array.
//
//                  LONG Remove_Rows[] -
//                        An array containing the rows numbers
//                        corresponding to the rows which are
//                        to be deleted.
//
//                        *** THIS MUST BE A SORTED ARRAY ***
//
//                  UNSIGNEDLONG Do_Refresh -
//                        Specifies if  the children DataWindows
//                        are to refreshed after the delete.
//                        Valid options are:
//
//                           - c_RefreshChildren
//                           - c_NoRefreshChildren
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Delete_DW_Rows() is now the preferred method for doing this.
//  This routine exists only for backward compatibility.
//----------

Delete_DW_Rows(Num_To_Remove,   Remove_Rows[], &
                        c_IgnoreChanges, Do_Refresh)

RETURN
end subroutine

public subroutine restore_selected_rows (long num_selected, ref long selected_rows[], ref string selected_keys[]);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Restore_Selected_Rows
//  Description   : Utility routine used by PowerClass to
//                  reselect rows specified by the Selected_Keys[]
//                  parameter.
//
//  Parameters    : INTEGER Num_Selected -
//                        The number of rows in the
//                        Selected_Rows[] array.
//
//                  ref LONG  Selected_Rows[] -
//                        The list of rows that were previously
//                        selected.  (Declared as "ref" to avoid
//                        copy-by-value overhead).
//
//                  ref STRING  Selected_Keys[] -
//                        The keys for the rows that are to be
//                        re-selected.  (Declared as "ref" to
//                        avoid copy-by-value overhead).
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_BeSmart,  l_FoundAll
LONG     l_RowCount, l_MinRow, l_MaxRow, l_RowNbr
LONG     l_Idx,      l_Jdx,    l_Kdx

//----------
//  We make two passes through the list of rows that need to be
//  re-selected.  The first time through, we just look for the
//  rows in the general vicinity of where they used to be.  If
//  that doesn't find them, then we look through the entire
//  DataWindow for them.  Assume that we will find them all the
//  first time through.
//----------

l_FoundAll = TRUE
l_RowCount = RowCount()

FOR l_Kdx = 1 TO 2

   //----------
   //  On the first time through, just look in the vicinity where
   //  the row used to be.
   //----------

   l_BeSmart = (l_Kdx = 1)

   //----------
   //  Cycle through all of the rows that we must find.
   //----------

   FOR l_Idx = 1 TO Num_Selected + 3

      //----------
      //  Find the offset into Selected_Keys[] where the keys for
      //  this row begin.
      //----------

      l_Jdx = (l_Idx - 1) * i_xNumKeyColumns

      //----------
      //  If the first key for this row is not NULL, then this row
      //  has not yet been found in the DataWindow's new data.
      //----------

      IF IsNull(Selected_Keys[l_Jdx + 1]) THEN
      ELSE

         //----------
         //  Set l_RowNbr to 0 to indicate that we have not yet
         //  found this row.
         //----------

         l_RowNbr = 0

         //----------
         //  See if we can find it fast.
         //----------

         IF l_BeSmart THEN

            //----------
            //  Set l_RowNbr to the row number where this row used
            //  to be.
            //----------

            l_RowNbr = Selected_Rows[l_Idx]

            //----------
            //  Since we now have new data, the row numbers have
            //  changed and our old row number may not be valid.
            //  Make sure that it is and then find a range of rows
            //  in the vicinity.
            //----------

            IF l_RowNbr >  0 AND &
               l_RowNbr <= l_RowCount THEN
               l_MinRow = l_RowNbr - 10
               l_MaxRow = l_RowNbr + 10
               IF l_MinRow < 1 THEN
                  l_MinRow = 1
               END IF
               IF l_MaxRow > l_RowCount THEN
                  l_MaxRow = l_RowCount
               END IF

               //----------
               //  Tell Find_Key_Row to only look for this row in
               //  the vicinity of where it used to be.
               //----------

               l_RowNbr = Find_Key_Row(l_Idx,           &
                                       Selected_Keys[], &
                                       l_MinRow,        &
                                       l_MaxRow)
            ELSE

               //----------
               //  Our old row number is just not in the ballpark.
               //  We're going to have to look for this row using
               //  the brute force method in the next pass of this
               //  loop.
               //----------


               l_RowNbr = 0
            END IF
         ELSE

            //----------
            //  We couldn't find the row in the vicinity of where
            //  it used to be.  Now we'll just brute force our way
            //  through the entire DataWindow looking for this row.
            //----------

            l_RowNbr = Find_Key_Row(l_Idx,           &
                                    Selected_Keys[], &
                                    1,               &
                                    l_RowCount)
         END IF

         //----------
         //  If l_RowNbr is greater than 0, then we have found the
         //  old row in the DataWindow's new data.
         //----------

         IF l_RowNbr > 0 THEN

            //----------
            //  Set the first key item to NULL to indicate that
            //  this row has been found and that no more searching
            //  is required.
            //----------

            SetNull(Selected_Keys[l_Jdx + 1])

            //----------
            //  If the row we just found was a row that used to
            //  selected, re-select it and bump the number of rows
            //  that we have been able to re-select.
            //----------

            IF l_Idx <= Num_Selected THEN
               i_NumSelected = i_NumSelected + 1
               SelectRow(l_RowNbr, TRUE)
            END IF

            //----------
            //  If the row we just found corresponds to the old
            //  i_ShowRow, set it.
            //----------

            IF l_Idx = Num_Selected + 1 THEN
               i_ShowRow = l_RowNbr
            END IF

            //----------
            //  If the row we just found corresponds to the old
            //  i_CursorRow, set it.
            //----------

            IF l_Idx = Num_Selected + 2 THEN
               i_CursorRow = l_RowNbr
            END IF

            //----------
            //  If the row we just found corresponds to the old
            //  i_AnchorRow, set it.
            //----------

            IF l_Idx = Num_Selected + 3 THEN
               i_AnchorRow = l_RowNbr
            END IF
         ELSE

            //----------
            //  We were unable to find this row.  Set l_FoundAll
            //  to FALSE to indicate that we were unable to find
            //  all of the rows.
            //----------

            l_FoundAll = FALSE
         END IF
      END IF
   NEXT

   //----------
   //  If we found all of the rows, then we are done.  Otherwise,
   //  we may have to cycle through the key list again.
   //----------

   IF l_FoundAll THEN
      EXIT
   END IF
NEXT

RETURN
end subroutine

public function long save_selected_rows (ref long selected_rows[], ref string selected_keys[]);//******************************************************************
//  PC Module     : uo_DW_Main
//  Function      : Save_Selected_Rows
//  Description   : Utility routine used by PowerClass to
//                  save the keys for the currently selected
//                  rows.
//
//  Parameters    : ref LONG  Selected_Rows[] -
//                        The list of the rows that are
//                        currently selected are returned
//                        in this array.
//
//                  ref STRING  Selected_Keys[] -
//                        The keys for the rows that are
//                        currently selected are returned
//                        in this array.
//
//  Return Value  : LONG -
//                        The number of ROWS (not KEYS) that
//                        are currently selected.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_NumSelected, l_ColNbr
LONG     l_RowCount,    l_RowNbr
LONG     l_Idx,         l_Jdx,   l_Kdx

//----------
//  Get the current number of rows in the DataWindow.
//----------

l_RowCount = RowCount()

//----------
//  Find out how many rows are currently selected in this
//  DataWindow.
//----------

l_NumSelected = Get_Selected_Rows(Selected_Rows[])

//----------
//  We need to save the currently selected rows.  We also save
//  the current show row, cursor row, and anchor row.  Append
//  these rows to selected rows array so that we can also restore
//  them when the restore of selected rows happens.
//----------

Selected_Rows[l_NumSelected + 1] = i_ShowRow
Selected_Rows[l_NumSelected + 2] = i_CursorRow
Selected_Rows[l_NumSelected + 3] = i_AnchorRow

//----------
//  Call Get_Col_Info() to make sure we have all the information
//  about keys rows, etc. for this DataWindow.
//----------

IF NOT i_GotColInfo THEN
   Get_Col_Info()
END IF

//----------
//  For every row that we need to restore, go through the
//  DataWindow and collect the key values for that row.  We
//  save the key values in a one-dimensional array, ordered
//  as follows:
//
//     Selected_Keys[1] = Key #1 for selected row #1
//     Selected_Keys[2] = Key #2 for selected row #1
//     Selected_Keys[3] = Key #3 for selected row #1
//     Selected_Keys[4] = Key #1 for selected row #2
//     Selected_Keys[5] = Key #2 for selected row #2
//     Selected_Keys[6] = Key #3 for selected row #2
//     .
//     .
//     .
//     Selected_Keys[x + 1] = Key #1 for i_ShowRow
//     Selected_Keys[x + 2] = Key #2 for i_ShowRow
//     Selected_Keys[x + 3] = Key #3 for i_ShowRow
//     Selected_Keys[x + 4] = Key #1 for i_CursorRow
//     Selected_Keys[x + 5] = Key #2 for i_CursorRow
//     Selected_Keys[x + 6] = Key #3 for i_CursorRow
//     Selected_Keys[x + 7] = Key #1 for i_AnchorRow
//     Selected_Keys[x + 8] = Key #2 for i_AnchorRow
//     Selected_Keys[x + 9] = Key #3 for i_AnchorRow
//----------

FOR l_Idx = 1 TO l_NumSelected + 3

   //----------
   //  Get the actual row number for the key column or special
   //  row (e.g. cursor row) that we are trying to process.
   //----------

   l_RowNbr = Selected_Rows[l_Idx]

   //----------
   //  Set l_Jdx to an offset into the array at which the key
   //  values for this row will start.
   //----------

   l_Jdx = (l_Idx - 1) * i_xNumKeyColumns

   //----------
   //  As long as this is a valid row, get all of the key
   //  values for it.
   //----------

   IF l_RowNbr > 0 AND l_RowNbr <= l_RowCount THEN
      FOR l_Kdx = 1 TO i_xNumKeyColumns

         //----------
         //  Get the column number of the key column.
         //----------

         l_ColNbr = i_xKeyColumns[l_Kdx]

         //----------
         //  Save the key value in this column for this row.
         //----------

         Selected_Keys[l_Jdx + l_Kdx] = &
            Get_Col_Data_Ex(l_RowNbr, l_ColNbr, &
                            Primary!, c_GetCurrentValues)
      NEXT
   ELSE

      //----------
      //  The row that we are processing is not valid.  Set
      //  its first key value to NULL to indicate that we do
      //  not have to process this row.
      //----------

      SetNull(Selected_Keys[l_Jdx + 1])
   END IF
NEXT

RETURN l_NumSelected
end function

public subroutine secure_controls ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Secure_Controls
//  Description   : If PowerLock is used, this function is used to
//                  remove the standard menu items and buttons from
//                  being controlled by PowerClass.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN l_ObjVisible[], l_ObjEnable[], l_ObjAdd[], l_ObjDelete[]
BOOLEAN l_ObjDrill[]
STRING  l_ObjName[], l_ObjType[], l_ObjDesc[]
INTEGER l_Idx, l_Jdx, l_NumObjects

//----------
//  Get the objects that are to be secured for this window.
//----------

l_NumObjects = f_PL_GetObjInfo(i_Window, l_ObjName[], l_ObjDesc[], &
                               l_ObjType[], l_ObjVisible[], &
                               l_ObjEnable[], l_ObjAdd[], &
                               l_ObjDelete[], l_ObjDrill[])

//----------
//  If no objects are to be secured for this window, return.
//----------

IF l_NumObjects = -1 THEN
   RETURN
END IF

//----------
//  For each secured object check to see if it is a PowerClass
//  standard menu item or command button.  If it is, set the
//  allow control variable to FALSE.
//----------

FOR l_Idx = 1 TO l_NumObjects
   IF l_ObjType[l_Idx] = "CommandButton" THEN
      FOR l_Jdx = 1 TO i_NumPCControls
         IF l_ObjName[l_Idx] = ClassName(i_PCControls[l_Jdx]) THEN
            IF NOT l_ObjEnable[l_Idx] OR NOT l_ObjVisible[l_Idx] THEN
               CHOOSE CASE i_PCControls[l_Jdx].i_ButtonType
                  CASE c_AcceptCB
                     i_AllowControlAccept     = FALSE
                  CASE c_CancelCB
                     i_AllowControlCancel     = FALSE
                  CASE c_CloseCB
                     i_AllowControlClose      = FALSE
                  CASE c_DeleteCB
                     i_AllowControlDelete     = FALSE
                  CASE c_FilterCB
                     i_AllowControlFilter     = FALSE
                  CASE c_FirstCB
                     i_AllowControlFirst      = FALSE
                  CASE c_InsertCB
                     i_AllowControlInsert     = FALSE
                  CASE c_LastCB
                     i_AllowControlLast       = FALSE
                  CASE c_MainCB
                     i_AllowControlMain       = FALSE
                  CASE c_ModifyCB
                     i_AllowControlModify     = FALSE
                  CASE c_NewCB
                     i_AllowControlNew        = FALSE
                  CASE c_NextCB
                     i_AllowControlNext       = FALSE
                  CASE c_PreviousCB
                     i_AllowControlPrevious   = FALSE
                  CASE c_PrintCB
                     i_AllowControlPrint      = FALSE
                  CASE c_QueryCB
                     i_AllowControlQuery      = FALSE
                  CASE c_ResetQueryCB
                     i_AllowControlResetQuery = FALSE
                  CASE c_RetrieveCB
                     i_AllowControlRetrieve   = FALSE
                  CASE c_SaveCB
                     i_AllowControlSave       = FALSE
                  CASE c_SaveRowsAsCB
                     i_AllowControlSaveRowsAs = FALSE
                  CASE c_SearchCB
                     i_AllowControlSearch     = FALSE
                  CASE c_ViewCB
                     i_AllowControlView       = FALSE
               END CHOOSE
            END IF
         END IF
      NEXT
   ELSEIF l_ObjType[l_Idx] = "Menu Item" THEN
      CHOOSE CASE l_ObjDesc[l_Idx]
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelClose, &
                      POS(PCCA.MDI.c_MDI_MenuLabelClose, "&"), 1, "")
            i_AllowControlClose = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelCancel, &
                      POS(PCCA.MDI.c_MDI_MenuLabelCancel, "&"), 1, "")
            i_AllowControlCancel = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelAccept, &
                      POS(PCCA.MDI.c_MDI_MenuLabelAccept, "&"), 1, "")
            i_AllowControlAccept = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelSave, &
                      POS(PCCA.MDI.c_MDI_MenuLabelSave, "&"), 1, "")
            i_AllowControlSave = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelSaveRowsAs, &
                      POS(PCCA.MDI.c_MDI_MenuLabelSaveRowsAs, "&"), 1, "")
            i_AllowControlSaveRowsAs = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelPrint, &
                      POS(PCCA.MDI.c_MDI_MenuLabelPrint, "&"), 1, "")
            i_AllowControlPrint = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelNew, &
                      POS(PCCA.MDI.c_MDI_MenuLabelNew, "&"), 1, "")
            i_AllowControlNew = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelView, &
                      POS(PCCA.MDI.c_MDI_MenuLabelView, "&"), 1, "")
            i_AllowControlView = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelModify, &
                      POS(PCCA.MDI.c_MDI_MenuLabelModify, "&"), 1, "")
            i_AllowControlModify = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelInsert, &
                      POS(PCCA.MDI.c_MDI_MenuLabelInsert, "&"), 1, "")
            i_AllowControlInsert = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelDelete, &
                      POS(PCCA.MDI.c_MDI_MenuLabelDelete, "&"), 1, "")
            i_AllowControlDelete = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelFirst, &
                      POS(PCCA.MDI.c_MDI_MenuLabelFirst, "&"), 1, "")
            i_AllowControlFirst = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelPrev, &
                      POS(PCCA.MDI.c_MDI_MenuLabelPrev, "&"), 1, "")
            i_AllowControlPrevious = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelNext, &
                      POS(PCCA.MDI.c_MDI_MenuLabelNext, "&"), 1, "")
            i_AllowControlNext = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelLast, &
                      POS(PCCA.MDI.c_MDI_MenuLabelLast, "&"), 1, "")
            i_AllowControlLast = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelQuery, &
                      POS(PCCA.MDI.c_MDI_MenuLabelQuery, "&"), 1, "")
            i_AllowControlQuery = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelSearch, &
                      POS(PCCA.MDI.c_MDI_MenuLabelSearch, "&"), 1, "")
            i_AllowControlSearch = FALSE
         CASE Replace(PCCA.MDI.c_MDI_MenuLabelFilter, &
                      POS(PCCA.MDI.c_MDI_MenuLabelFilter, "&"), 1, "")
            i_AllowControlFilter = FALSE
      END CHOOSE
   END IF
NEXT
end subroutine

public subroutine set_auto_focus (uo_dw_main to_dw);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_Auto_Focus
//  Description   : Utility routine used by PowerClass when
//                  it is setting auto-focus.  This routine
//                  will not change the current DataWindow if
//                  the developer specifed that the DataWindow
//                  should not perform auto-focus.  This routine
//                  attempts to make the specified DataWindow
//                  current without changing focus to it.  The
//                  PCCA.PCMGR.i_DW_FocusDone flag will be set
//                  to TRUE if this routine performed the
//                  auto-focus request.
//
//  Parameters    : UO_DW_MAIN  To_DW -
//                        DataWindow to become current.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN        l_SetFocus
GRAPHICOBJECT  l_FocusObj

//----------
//  See if this DataWindow allows auto-focus.
//----------

IF i_AutoFocus THEN

   //----------
   //  We need to change the current DataWindow.  We would like
   //  to do it without disturbing the current focus, if possible.
   //  Start by assuming that we will not have to set focus.
   //----------

   l_SetFocus = FALSE

   //----------
   //  If the current window does not match this DataWindow's
   //  window, then we want to set focus so that the window will
   //  get raised.
   //----------

   IF To_DW.i_Window <> PCCA.Window_Current THEN
      l_SetFocus = TRUE
   ELSE

      //----------
      //  If this DataWindow is already active, then we don't have
      //  worry about changing focus.
      //----------

      IF NOT To_DW.i_Active THEN

         //----------
         //  See if anybody has focus.  If nobody has focus, then
         //  we don't have worry about setting focus, because we
         //  only have to worry when another DataWindow has focus.
         //----------

         l_FocusObj = GetFocus()

         IF IsValid(l_FocusObj) THEN

            //----------
            //  Somebody has focus.  See if it is a DataWindow.
            //----------

            IF l_FocusObj.TypeOf() = DataWindow! THEN
               l_SetFocus = TRUE
            END IF
         END IF
      END IF
   END IF

   IF l_SetFocus THEN
      Change_DW_Focus(To_DW)
   ELSE
      IF NOT To_DW.i_Active THEN
         To_DW.TriggerEvent("pcd_Active")
      END IF
   END IF

   //----------
   //  Indicate that focus was taken care of.
   //----------

   PCCA.PCMGR.i_DW_FocusDone = TRUE
END IF

RETURN
end subroutine

public subroutine set_cb_message (uo_cb_message messagecb, string message_column, string message_title);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_CB_Message
//  Description   : Initialize parameters for the Message window.
//                  Note: Wire_Message() is now the preferred
//                        routine to do this and it takes the
//                        same parameters.
//
//  Parameters    : UO_CB_MESSAGE MessageCB -
//                        Message button to wire.
//                  STRING Message_Column -
//                        Column in the DataWindow with the message
//                        to display
//                  STRING Message_Title -
//                        Title to go on the message window.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Wire_Message() is now the preferred method for doing this.
//  This routine exists only for backward compatibility.
//----------

Wire_Message(MessageCB, Message_Column, Message_Title)

RETURN
end subroutine

public subroutine set_cb_scroll (uo_cb_next scrollcb_next, uo_cb_previous scrollcb_prev, uo_cb_first scrollcb_first, uo_cb_last scrollcb_last);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_CB_Scroll
//  Description   : Initialize the DataWindow scrolling buttons.
//
//  Parameters    : UO_CB_NEXT ScrollCB_Next -
//                        A button that will trigger next.
//                  UO_CB_PREV ScrollCB_Prev -
//                        A button that will trigger previous.
//                  UO_CB_FIRST ScrollCB_First -
//                        A button that will trigger first.
//                  UO_CB_LAST ScrollCB_Last -
//                        A button that will trigger last.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Wire_Button() will take care of all the details.
//----------

Wire_Button(ScrollCB_Next)
Wire_Button(ScrollCB_Prev)
Wire_Button(ScrollCB_First)
Wire_Button(ScrollCB_Last)

RETURN
end subroutine

public subroutine set_cc_row (long data_row, unsignedlong set_cc_option);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_CC_Row
//  Description   : Fills in the column values for a row in
//                  the DataWindow based on the state of
//                  concurrency error processing.
//
//  Parameters    : LONG Data_Row -
//                        The DataWindow that is to be updated
//                        with concurrency recovery values.
//
//                  UNSIGNEDLONG Set_CC_Option -
//                        Indicates if every column is to be
//        F                filled in or only those which have
//                        been changed.  Valid options are:
//
//                     - c_CCSetForce
//                     - c_CCSetIfModified
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Force,    l_DisableKey, l_KeyColumns[], l_Doit
INTEGER  l_Idx
STRING   l_NewValue, l_Describe,   l_Result

//----------
//  Get the key columns from the DataWindow.  We can't use
//  i_xKeyColumn[] because the developer may have modified
//  which columns are the keys for UPDATE().
//----------

//----------
//  Ask the DataWindow if this column is a key column.
//----------

FOR l_Idx = 1 TO i_NumColumns
   l_Describe          = "#" + String(l_Idx) + ".Key"
   l_Result            = Upper(Describe(l_Describe))
   l_KeyColumns[l_Idx] = (l_Result = "YES")
NEXT

//----------
//  For the columns that the developer processed, stuff the
//  values into the DataWindow so that UPDATE() will get
//  them on the next pass.
//----------

l_Force = (Set_CC_Option = c_CCSetForce)

FOR l_Idx = 1 TO i_NumColumns
   CHOOSE CASE i_CCStatus[l_Idx].Process_Code

      //----------
      //  We are to use the original value from the database.
      //----------

      CASE c_CCProcUseOldDB
         l_NewValue = i_CCInfo[l_Idx].Old_DB_Value

      //----------
      //  We are to use the new updated value in the database.
      //----------

      CASE c_CCProcUseNewDB
         l_NewValue = i_CCInfo[l_Idx].New_DB_Value

      //----------
      //  We are to use the value entered by the user into the
      //  DataWindow.
      //----------

      CASE c_CCProcUseDWValue
         l_NewValue = i_CCInfo[l_Idx].DW_Value

      //----------
      //  We are to use the value generated by the developer.
      //----------

      CASE c_CCProcUseSpecial
         l_NewValue = i_CCStatus[l_Idx].Special_Value

      //----------
      //  Shouldn't get to here...
      //----------

      CASE ELSE
         SetNull(l_NewValue)
   END CHOOSE

   //----------
   //  If the new requested value always needs to be placed into
   //  the column (i.e. l_Force is TRUE) or doesn't match the
   //  current value in the database, we use Set_Col_Data() to
   //  stuff the new value into the column.  Using Set_Col_Data()
   //  will mark the row and column as being updated.
   //----------

   l_Doit = FALSE
   IF l_Force THEN
      l_Doit = TRUE
   ELSE
      l_Doit = TRUE
      IF i_CCInfo[l_Idx].New_DB_Value = l_NewValue THEN
         l_Doit = FALSE
      ELSE
         IF IsNull(i_CCInfo[l_Idx].New_DB_Value) THEN
         IF IsNull(l_NewValue) THEN

            //----------
            //  Both are NULL and therefore equal.  Don't
            //  stuff the value
            //----------

            l_Doit = FALSE
         END IF
         END IF
      END IF
   END IF

   IF l_Doit THEN

      //----------
      //  If this is a key column and the key values have not
      //  changed, we need to turn the key off.  If we do not
      //  and the DataWindow has been requested to do DELETE/INSERT
      //  for key columns (instead of UPDATE), PowerBuilder will
      //  attempt to delete the row and reinsert, even though
      //  the key value has not changed.
      //----------

      l_DisableKey = FALSE
      IF l_KeyColumns[l_Idx] THEN
         IF i_CCInfo[l_Idx].New_DB_Value = l_NewValue THEN
            l_DisableKey = TRUE
            Modify("#" + String(l_Idx) + ".Key=No")
         END IF
      END IF

      //----------
      //  Set the new value into the column
      //----------

      Set_Col_Data(Data_Row, l_Idx, l_NewValue)

      //----------
      //  If we disabled a key column, then we need to reenable it.
      //----------

      IF l_DisableKey THEN
         Modify("#" + String(l_Idx) + ".Key=Yes")
      END IF
   END IF
NEXT

RETURN
end subroutine

public subroutine set_col_data (long row_nbr, integer column_nbr, string value);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_Col_Data
//  Description   : Sets the data of the specified field
//                  with Value.
//
//  Parameters    : LONG Row_Nbr -
//                        The row number of the field into
//                        which the data is to be placed.
//
//                  INTEGER Column_Nbr -
//                        The column number of the field into
//                        which the data is to be placed.
//
//                  STRING Value -
//                        The value of the data to be placed
//                        into the cell.  It will be converted
//                        automatically to the appropriate
//                        data type.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Space
UNSIGNEDLONG  l_ColType
STRING        l_Date, l_Time

//----------
//  Make sure we have a valid column and row.
//----------

IF Column_Nbr < 1 OR Column_Nbr > i_NumColumns OR &
   Row_Nbr    < 1 OR Row_Nbr    > RowCount()   THEN
   GOTO Finished
END IF

//----------
//  Column information is not saved when the DataWindow
//  is initialized to cut down on open time.  Therefore,
//  Get_Col_Info() may need to be called to get this
//  information loaded.
//----------

IF NOT i_GotColInfo THEN
   Get_Col_Info()
END IF

//----------
//  Get the enumerated value for the type of data that the column
//  contains.
//----------

l_ColType = i_xColumnType[Column_Nbr]

//----------
//  Based on the column type convert the data and set it into
//  the specified cell.
//----------

CHOOSE CASE l_ColType
   CASE c_ColTypeDate

      //----------
      //  Set DATE data into the cell.
      //----------

      SetItem(Row_Nbr, Column_Nbr, Date(Value))
   CASE c_ColTypeDateTime

      //----------
      //  Set DATETIME data into the cell.  If we may have to
      //  split the string apart into a DATE string and a TIME
      //  string.
      //----------

      l_Space = Pos(Value, " ")
      IF l_Space > 0 THEN
         l_Date = Mid(Value, 1, l_Space - 1)
         l_Time = Mid(Value, l_Space + 1)
         SetItem(Row_Nbr, Column_Nbr, &
                 DateTime(Date(l_Date), Time(l_Time)))
      ELSE
         SetItem(Row_Nbr, Column_Nbr, DateTime(Date(Value)))
      END IF
   CASE c_ColTypeDecimal

      //----------
      //  Set DECIMAL data into the cell.
      //----------

      SetItem(Row_Nbr, Column_Nbr, Dec(Value))
   CASE c_ColTypeNumber

      //----------
      //  Set NUMBER data into the cell.
      //----------

      SetItem(Row_Nbr, Column_Nbr, Real(Value))
   CASE c_ColTypeTime

      //----------
      //  Set TIME data into the cell.
      //----------

      SetItem(Row_Nbr, Column_Nbr, Time(Value))
   CASE c_ColTypeTimeStamp

      //----------
      //  Set TIME data into the cell (handled the same
      //  as TIME data).
      //----------

      SetItem(Row_Nbr, Column_Nbr, Time(Value))
   //CASE c_ColTypeString
   //CASE c_ColTypeBlob
   CASE ELSE

      //----------
      //  Set STRING data into the cell.
      //----------

      SetItem(Row_Nbr, Column_Nbr, Value)
END CHOOSE

Finished:

RETURN
end subroutine

public subroutine set_ddlb_search (uo_ddlb_search ddlb_name, string search_table, string search_column);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_DDLB_Search
//  Description   : Initialize parameters for the Search drop
//                  down list box field.
//
//                  Note: Wire_Search() is now the preferred
//                        routine to do this and it takes the
//                        same parameters.
//
//  Parameters    : UO_DDLB_SEARCH DDLB_Name -
//                        The DDLB object that is to be wired
//                        to this DataWindow.
//                  STRING Search_Table -
//                        Table in the database that the search
//                        is to performed upon.
//                  STRING Search_Column -
//                        Column in the database that the search
//                        is to performed upon.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Wire_Search() is now the preferred method for doing this.
//  This routine exists only for backward compatibility.
//----------

Wire_Search(DDLB_Name, Search_Table, Search_Column)

RETURN
end subroutine

public subroutine set_dw_default_menus (menu file_menu, menu edit_menu, menu popup_menu);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_DW_Default_Menus
//  Description   : Used by the window to pass the default menus
//                  to the DataWindows it contains.  This routine
//                  is called only for the first DataWindow
//                  on the window.
//
//  Parameters    : MENU File_Menu -
//                        The menu that is to be used
//                        as the FILE menu by PowerClass.
//
//                  MENU Edit_Menu -
//                        The menu that is to be used
//                        as the EDIT menu by PowerClass.
//
//                  MENU Popup_Menu -
//                        The menu that is to be used
//                        as the POPUP menu by PowerClass.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_DefaultMenusDefined = TRUE
i_DefaultFileMenu     = File_Menu
i_DefaultEditMenu     = Edit_Menu
i_DefaultPopupMenu    = Popup_Menu

RETURN
end subroutine

public subroutine set_dw_find (uo_dw_find find_dw, string find_column);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_DW_Find
//  Description   : Initialize parameters for the Find DataWindow.
//
//                  Note: Wire_Find() is now the preferred
//                        routine to do this and it takes the
//                        same parameters.
//
//  Parameters    : UO_DW_FIND Find_DW -
//                        The find object that is to be wired
//                        to this DataWindow.
//                  STRING Find_Column -
//                        Column name in the DataWindow that is
//                        to searched by the Find DataWindow.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Wire_Find() is now the preferred method for doing this.
//  This routine exists only for backward compatibility.
//----------

Wire_Find(Find_DW, Find_Column)

RETURN
end subroutine

public subroutine set_em_search (uo_em_search em_name, string search_table, string search_column);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_EM_Search
//  Description   : Initialize parameters for the Search edit mask
//                  field.
//
//                  Note: Wire_Search() is now the preferred
//                        routine to do this and it takes the
//                        same parameters.
//
//  Parameters    : UO_EM_SEARCH EM_Name -
//                       The EM object that is to be wired
//                       to this DataWindow.
//                  STRING Search_Table -
//                       Table in the database that the search
//                       is to performed upon.
//                  STRING Search_Column -
//                       Column in the database that the search
//                       is to performed upon.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Wire_Search() is now the preferred method for doing this.
//  This routine exists only for backward compatibility.
//----------

Wire_Search(EM_Name, Search_Table, Search_Column)

RETURN
end subroutine

public subroutine set_lb_search (uo_lb_search lb_name, string search_table, string search_column);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_LB_Search
//  Description   : Initialize parameters for the Search list box
//                  field.
//
//                  Note: Wire_Search() is now the preferred
//                        routine to do this and it takes the
//                        same parameters.
//
//  Parameters    : UO_LB_SEARCH LB_Name -
//                       The LB object that is to be wired
//                       to this DataWindow.
//                  STRING Search_Table -
//                       Table in the database that the search
//                       is to performed upon.
//                  STRING Search_Column -
//                       Column in the database that the search
//                       is to performed upon.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Wire_Search() is now the preferred method for doing this.
//  This routine exists only for backward compatibility.
//----------

Wire_Search(LB_Name, Search_Table, Search_Column)

RETURN
end subroutine

public subroutine set_sle_search (uo_sle_search sle_name, string search_table, string search_column);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Set_SLE_Search
//  Description   : Initialize parameters for the Search single line
//                  edit field.
//                  Note: Wire_Search() is now the preferred
//                        routine to do this and it takes the
//                        same parameters.
//
//  Parameters    : UO_SLE_SEARCH SLE_Name -
//                        The SLE object that is to be wired
//                        to this DataWindow.
//                  STRING Search_Table -
//                        Table in the database that the search
//                        is to performed upon.
//                  STRING Search_Column -
//                        Column in the database that the search
//                        is to performed upon.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Wire_Search() is now the preferred method for doing this.
//  This routine exists only for backward compatibility.
//----------

Wire_Search(SLE_Name, Search_Table, Search_Column)

RETURN
end subroutine

public subroutine setup_dw ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Setup_DW
//  Description   : Helper in initialization of the DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

LONG     l_DescribeLen
INTEGER  l_Idx
LONG     l_FirstRow,       l_LastRow
STRING   l_ObjNames,       l_Describe,      l_Result
STRING   l_ViewModeBorder, l_ViewModeColor, l_InactiveDWColor

// EnMe 17-02-2009; imposto il colore di sfondo della DW
if NOT ib_dw_report then this.object.datawindow.color = s_themes.theme[s_themes.current_theme].dw_backcolor

//----------
//  See if the developer specified whether the DataWindow is
//  a free-form style or a tabular-form style.
//----------

IF i_FreeFormStyle THEN
   i_MultiRowDisplay = FALSE
ELSE
   IF i_TabularFormStyle THEN
      i_MultiRowDisplay = TRUE
   ELSE

      //----------
      //  If the developer did not specify whether the DataWindow
      //  was a free-form style or a tabular-form style, we have
      //  to figure it out.
      //----------

      IF NOT i_Initialized  THEN

         //----------
         //  Determine whether the DataWindow is a tabular-form
         //  or free-form style of DataWindow.  We test this by
         //  inserting two rows and then checking the first and
         //  last lines on the display.  If it is a tabular-form
         //  DataWindow, the first line will be 1 and the last
         //  line will be 2.  If it is a free-form with a
         //  single record displayed, then the first line will
         //  be 1 and the last line will also be 1.
         //----------

         //----------
         //  Insert the 2 rows and get the numbers of the first
         //  and last rows displayed.
         //----------

         InsertRow(0)
         InsertRow(0)

         l_FirstRow = Long(Describe("DataWindow.FirstRowOnPage"))
         l_LastRow  = Long(Describe("DataWindow.LastRowOnPage"))

         //----------
         //  We can now determine if we have a multi-row (i.e.
         //  tabular-form style) DataWindow.
         //----------

         i_MultiRowDisplay = (l_FirstRow <> l_LastRow)

         //----------
         //  Snuff out the bogus rows we just added.
         //----------

         Reset()
      END IF
   END IF
END IF

//----------
//  Determine if we should do EMPTY processing for this
//  DataWindow.
//----------

i_DoEmpty = FALSE
IF NOT i_MultiRowDisplay THEN
IF i_ShowEmpty           THEN
   i_DoEmpty = TRUE
END IF
END IF

//----------
//  See if the developer specified whether the DataWindow was to
//  use PowerBuilder's SelectRow() function to highlight selected
//  rows.  If the developer specified nothing, then calculate it
//  automatically based on the DataWindow style.
//----------

IF i_CalcHighlightSelected THEN
   i_CalcHighlightSelected = FALSE

   IF i_MultiRowDisplay THEN
      i_HighlightSelected = TRUE
   ELSE
      i_HighlightSelected = FALSE
   END IF
END IF

//----------
//  Get ready to inquire column information.  Note that we
//  only get the minimum necessary to get us through
//  initialization.  If other routines require other column
//  information, they will have to get it on a as-needed basis.
//----------

//----------
//  If this DataWindow is just being initialized, then get tab
//  information about the columns.
//----------

IF NOT i_Initialized THEN

   i_NumColumns = Integer(Describe("DataWindow.Column.Count"))

   i_GotColInfo  = FALSE
   i_GotKeyMap   = FALSE
   i_KeyMapValid = FALSE
   i_GotObjects  = FALSE

   FOR l_Idx = 1 TO i_NumColumns
      i_GotValInfo[l_Idx] = FALSE
   NEXT

   i_TabLength                = 30 * i_NumColumns
   l_DescribeLen              = 1  * (25 + i_TabLength)
   l_Describe                 = Fill(" ", l_DescribeLen)
   i_EnableTabs               = Fill(" ", i_TabLength)
   i_DisableTabs              = Fill(" ", i_TabLength)
   i_QueryTabs                = Fill(" ", i_TabLength)
   i_TabOrder[i_NumColumns]   = i_NumColumns
   i_ColVisible[i_NumColumns] = TRUE

   PCCA.DLL.fu_BuildGetTabString (THIS, i_NumColumns, l_DescribeLen, l_Describe)

   l_Result = Describe(l_Describe)

   PCCA.DLL.fu_ParseTabString (THIS, i_NumColumns, l_Result, i_TabOrder[], i_ColVisible[])

   //----------
   //  Now, get color information about the columns.
   //----------

   i_AttribLength             = 40 * i_NumColumns
   l_DescribeLen              = 3  * i_AttribLength
   l_Describe                 = Fill(" ", l_DescribeLen)
   i_ModifyBorders            = Fill(" ", i_AttribLength)
   i_ViewBorders              = Fill(" ", i_AttribLength)
   i_ModifyColors             = Fill(" ", i_AttribLength)
   i_ViewColors               = Fill(" ", i_AttribLength)
   i_NonEmptyTextColors       = Fill(" ", i_AttribLength)
   i_EmptyTextColors          = Fill(" ", i_AttribLength)
   i_ColBorders[i_NumColumns] = i_NumColumns
   i_ColColors[i_NumColumns]  = i_NumColumns
   i_ColBColors[i_NumColumns] = i_NumColumns

   PCCA.DLL.fu_BuildGetAttribString &
      (THIS,                           &
       i_NumColumns,  i_ColVisible[],  &
       l_DescribeLen, l_Describe)

   l_Result = Describe(l_Describe)

   PCCA.DLL.fu_ParseAttribString              &
      (THIS,                                 &
       i_NumColumns,   i_ColVisible[], l_Result, &
       i_ColBorders[], i_ColColors[],  i_ColBColors[])

END IF

//----------
//  Build the tab strings for MODIFY().
//----------

PCCA.DLL.fu_BuildTabStrings          &
   (THIS,         i_NumColumns,     &
    i_TabOrder[],     i_ColVisible[],   &
    i_FirstTabColumn, i_FirstVisColumn, &
    i_TabLength,      i_EnableTabs,     &
    i_DisableTabs,    i_QueryTabs)
//----------
//  Build the attribute strings for MODIFY().
//----------

IF i_ViewModeBorderIdx <> c_DW_BorderUndefined THEN
   l_ViewModeBorder = PCCA.MDI.c_DW_Borders[i_ViewModeBorderIdx]
ELSE
   l_ViewModeBorder = ""
END IF

IF i_ViewModeColorIdx <> c_ColorUndefined THEN
	// stefanop 20/09/2010: colore errato, prelevo il codice dai parametri del tema
	//l_ViewModeColor = PCCA.MDI.c_RGBStrings[i_ViewModeColorIdx]
	l_ViewModeColor = string(s_themes.theme[s_themes.current_theme].dw_column_viewmode_backcolor)
ELSE
   l_ViewModeColor = ""
END IF

if not ib_dw_report then
//-------------
//		EnMe 18/08/2008 ... ma guarda come mi tocca fare in agosto.
//		In questo punto modifico il colore di background della colonna per evidenziare le colonne obbligatorie
//-------------
	long   ll_i
	string ls_column_name, ls_flag_importante, ls_flag_obbligatorio
	// variabili aggiunte da EnMe (17-02-2009) per gestione del theme
	string ls_modifyfont
	
	for ll_i = 1 to i_NumColumns
		
		ls_column_name = describe("#" + string(ll_i) + ".name")
		
		setnull(ls_flag_importante)
		setnull(ls_flag_obbligatorio)
		
		select flag_importante,
				flag_obbligatorio
		into   :ls_flag_importante,
				:ls_flag_obbligatorio
		from   tab_dw_text_repository
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_lingua is null and
				 nome_dw = :dataobject and
				 nome_oggetto = :ls_column_name;
				 
		if ls_flag_obbligatorio = "S" then
			i_ColBColors[ll_i] = s_cs_xx.colore_colonna_obbligatorio
			//modify("#" + string(ll_i) + ".Background.Color='" + string(s_cs_xx.colore_colonna_obbligatorio) + "'")
		else
		
			if ls_flag_importante = "S" then
				i_ColBColors[ll_i] = s_cs_xx.colore_colonna_importante
				//modify("#" + string(ll_i) + ".Background.Color='" + string(s_cs_xx.colore_colonna_importante) + "'")
			else
				i_ColBColors[ll_i] = s_themes.theme[s_themes.current_theme].dw_column_backcolor
				//modify("#" + string(ll_i) + ".Background.Color='" + string(s_themes.theme[s_themes.current_theme].dw_column_backcolor) + "'")
			end if
		
		end if
		
		// altre impostazioni che prescindono dalla questione colonne importanti / obbligatorie
		
		// colore del testo
		if i_TabOrder[ll_i] >= 0 then
			i_ColColors[ll_i] 	= s_themes.theme[s_themes.current_theme].dw_column_textcolor
			ls_modifyfont		= ls_modifyfont + "~t#" + string(ll_i) + ".font.face='" + s_themes.theme[s_themes.current_theme].dw_column_font +"'"
			ls_modifyfont		= ls_modifyfont + "~t#" + string(ll_i) + ".font.height='" + s_themes.theme[s_themes.current_theme].dw_column_fontsize +"'"
			ls_modifyfont		= ls_modifyfont + "~t#" + string(ll_i) + ".font.weight='" + s_themes.theme[s_themes.current_theme].dw_column_fontbold +"'"
			ls_modifyfont		= ls_modifyfont + "~t#" + string(ll_i) + ".font.italic='" + s_themes.theme[s_themes.current_theme].dw_column_fontitalic +"'"
		end if
		
	next
	
	// Fine modifica EnMe 18/08/2008
	//-------------
end if

PCCA.DLL.fu_BuildAttribStrings           &
   (THIS,             i_NumColumns,     &
    i_TabOrder[],         i_ColVisible[],   &
    i_ColBorders[],       i_ColColors[],    &
    i_ColBColors[],       l_ViewModeBorder, &
    l_ViewModeColor,   i_AttribLength,   &
    i_ModifyBorders,     i_ViewBorders,    &
    i_ModifyColors,       i_ViewColors,     &
    i_NonEmptyTextColors, i_EmptyTextColors)

/* SPIEGAZIONE DELLE VARIABILI
i_ModifyColors 	= colore di sfondo delle colonne in corso di modifica
i_ViewColors 	= colore di sfondo delle colonne quando sono in modalità visualizzazione
i_EmptyTextColors	= Colore del testo delle colonne quando non ci sono righe (in pratica è il colore de ltesto della riga finta)
i_NonEmptyTextColors = Colore del testo delle colonne quando ci sono dati nelle righe
i_ModifyBorders = tipologia del bordo delle colonne quando sono in modifica / nuovo
i_ViewBorders   = tipologia del bordo delle colonne quando sono in visualizzazione
l_ViewModeColor = colore della colonna in fase di visualizzazione
l_ViewModeBorder = tipo bordo  colonna in fase di visualizzazione
*/

//i_EmptyTextColors += ls_modifyfont
i_NonEmptyTextColors += ls_modifyfont
i_EmptyTextColors     = i_NonEmptyTextColors


//----------
//  If the developer has already defined an Inactive color, then
//  change the DataWindow to the orignal colors while we setup
//  the new Inactive colors.
//----------

IF i_GotObjects THEN
   IF i_IsEmpty THEN
      Modify(i_ActiveEmptyColors)
   ELSE
      Modify(i_ActiveNonEmptyColors)
   END IF
END IF


/*	==================================================
		NUOVA GESTIONE DEI TEMI GRAFICI   EnMe 17-02-2009
	================================================== */

if i_InactiveDWColorIdx <> c_ColorUndefined then


end if


/*	==================================================
		FINE MODIFICA NUOVA GESTIONE DEI TEMI GRAFICI   EnMe 17-02-2009
	================================================== */


//----------
//  If the developer specified an inactive DataWindow color,
//  we have to build strings for MODIFY().
//----------

/*  ==================================================
// EnMe 17-02-2009 ELIMINATA la funzione del cambio colore della DW inattiva
___________________________________________________________________

IF i_InactiveDWColorIdx <> c_ColorUndefined THEN

   //----------
   //  If we have not done so, get a list of all objects on the
   //  DataWindow.
   //----------

   IF NOT i_GotObjects THEN
      i_GotObjects = TRUE

      l_ObjNames = Describe("DataWindow.Objects")

      l_DescribeLen = (10 + c_MaxObjectNameLen) * &
                      (i_NumColumns + c_MaxNonColumnObjects)
      l_Describe    = Fill(" ", l_DescribeLen)

      PCCA.DLL.fu_BuildGetObjTypeString &
         (THIS, l_ObjNames, l_DescribeLen, l_Describe, i_NumObjects)

      l_Result = Describe(l_Describe)

      l_DescribeLen = (15 + c_MaxObjectNameLen) * i_NumObjects
      l_Describe    = Fill(" ", l_DescribeLen)

      FOR l_Idx = 1 TO i_NumObjects
         i_ObjectNames[l_Idx] = Fill(" ", c_MaxObjectNameLen)
      NEXT
      i_ObjectTypes[i_NumObjects]  = i_NumObjects
      i_ObjectColors[i_NumObjects] = i_NumObjects

      PCCA.DLL.fu_ParseObjTypeString  &
         (THIS,        i_NumObjects, &
          l_ObjNames,      l_Result,     &
          l_DescribeLen,   l_Describe,   &
          i_ObjectNames[], i_ObjectTypes[])

      l_Result = Describe(l_Describe)

      PCCA.DLL.fu_ParseObjColorString &
         (THIS,                      &
          i_NumObjects, i_ObjectTypes[], &
          l_Result,     i_ObjectColors[])

      i_ActiveLength           = (26 + c_MaxObjectNameLen) * &
                                 i_NumObjects
      i_ActiveNonEmptyColors   = Fill(" ", i_ActiveLength)
      i_ActiveEmptyColors      = Fill(" ", i_ActiveLength)
      i_InactiveNonEmptyColors = Fill(" ", i_ActiveLength)
      i_InactiveEmptyColors    = Fill(" ", i_ActiveLength)
   END IF

   l_InactiveDWColor = &
      PCCA.MDI.c_RGBStrings[i_InactiveDWColorIdx]

   PCCA.DLL.fu_BuildActiveStrings                      &
      (THIS,                                          &
       i_NumObjects,             i_ObjectNames[],         &
       i_ObjectTypes[],          i_ObjectColors[],        &
       i_InactiveCol,   i_InactiveText, &
       i_InactiveLine,  l_InactiveDWColor,       &
       l_ViewModeColor,          i_ActiveLength,          &
       i_ActiveNonEmptyColors,   i_ActiveEmptyColors,     &
       i_InactiveNonEmptyColors, i_InactiveEmptyColors)

END IF
//    ===================  FINE MODIFICA DEL 17-02-2009
_________________________________________________________________________________   */



//----------
//  If we are already initialized, then we need to make sure
//  that the DataWindow is updated with the new settings.
//----------

IF i_Initialized THEN

   //----------
   //  Make sure that the borders are set to their new values.
   //----------

//   IF i_ViewModeBorderIdx = c_DW_BorderUndefined THEN
//      Modify(i_ModifyBorders)
//   ELSE
//      IF i_CurrentMode = c_ModeView THEN
//         Modify(i_ViewBorders)
//      ELSE
//         Modify(i_ModifyBorders)
//      END IF
//   END IF

   //----------
   //  Make sure that the colors are set to their new values.
   //----------
//   IF i_ViewModeColorIdx = c_ColorUndefined THEN
//      Modify(i_ModifyColors)
//   ELSE
//      IF i_CurrentMode = c_ModeView THEN
//         Modify(i_ViewColors)
//      ELSE
//         Modify(i_ModifyColors)
//      END IF
//   END IF
	
END IF

//----------
//  Make sure that the active colors are set to their new values.
//----------

i_SetEmptyColors = i_IsEmpty

IF i_InactiveCol THEN

   //----------
   //  If the DataWindow is empty make sure that the colors are
   //  set to their correct state.
   //----------

   IF i_IsEmpty THEN
      Modify(i_EmptyTextColors)
   ELSE
      Modify(i_NonEmptyTextColors)
   END IF
END IF

//----------
//  i_ViewModeColor, i_ModifyTextColor, and i_ModifyModeColor are
//  used by uo_CB_Message.
//----------

IF i_ViewModeColorIdx <> c_ColorUndefined THEN
   i_ViewModeColor = l_ViewModeColor
ELSE
   i_ViewModeColor = PCCA.MDI.c_RGBStrings[c_Gray]
END IF

IF i_FirstTabColumn > 0 THEN
   i_ModifyModeColor = String(i_ColBColors[i_FirstTabColumn])
   i_ModifyTextColor = String(i_ColColors[i_FirstTabColumn])
ELSE
   IF i_FirstVisColumn > 0 THEN
      i_ModifyModeColor = String(i_ColBColors[i_FirstVisColumn])
      i_ModifyTextColor = String(i_ColColors[i_FirstVisColumn])
   ELSE
      i_ModifyModeColor = l_ViewModeColor
      i_ModifyTextColor = "0"
   END IF
END IF

IF NOT i_FocusRowInit THEN
   i_FocusRowInit      = TRUE
   i_FocusRowIsPicture = FALSE
   i_FocusRowPicture   = c_NullPicture
   i_FocusRowXPosition = 0
   i_FocusRowYPosition = 0

   //----------
   //  If this a single row DataWindow (i.e. free-form style) or
   //  if the DataWindow is empty, then don't display the row
   //  focus indicator.
   //----------

   IF i_MultiRowDisplay THEN
      i_FocusRowInd = FocusRect!
   ELSE
      i_FocusRowInd = Off!
   END IF
END IF

IF NOT i_NotFocusRowInit THEN
   i_NotFocusRowInit      = TRUE
   i_NotFocusRowIsPicture = FALSE
   i_NotFocusRowPicture   = c_NullPicture

   //----------
   //  If this a single row DataWindow (i.e. free-form style) or
   //  if the DataWindow is empty, then don't display the row
   //  focus indicator.
   //----------

   IF i_MultiRowDisplay THEN
      i_NotFocusRowInd       = Hand!
      i_NotFocusRowXPosition = c_XHandPosition
      i_NotFocusRowYPosition = c_YHandPosition
   ELSE
      i_NotFocusRowInd       = Off!
      i_NotFocusRowXPosition = 0
      i_NotFocusRowYPosition = 0
   END IF
END IF

IF NOT i_EmptyRowInit THEN
   i_EmptyRowInit      = TRUE
   i_EmptyRowInd       = Off!
   i_EmptyRowIsPicture = FALSE
   i_EmptyRowPicture   = c_NullPicture
   i_EmptyRowXPosition = 0
   i_EmptyRowYPosition = 0
END IF

IF NOT i_QueryRowInit THEN
   i_QueryRowInit      = TRUE
   i_QueryRowInd       = Off!
   i_QueryRowIsPicture = FALSE
   i_QueryRowPicture   = c_NullPicture
   i_QueryRowXPosition = 0
   i_QueryRowYPosition = 0
END IF

TriggerEvent("pcd_SetRowIndicator")

RETURN
end subroutine

public subroutine share_flags (unsignedlong which_flags, boolean new_state);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Share_Flags
//  Description   : Modifies flags that are used by DataWindows
//                  using share result sets.
//
//  Parameters    : UNSIGNEDLONG  Which_Flags -
//                        Indicates which flags in the
//                        DataWindows should be modified.
//                        Valid options are:
//
//                           - c_ShareModified
//                           - c_ShareAllModified
//                           - c_ShareAllDoSetKey
//
//                  UNSIGNEDLONG  New_State -
//                        The new value of the flags.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_ShareFlag
INTEGER       l_Idx
UNSIGNEDLONG  l_BitMask
UO_DW_MAIN    l_TmpDW

//----------
//  Set the i_Modified flags.
//----------

l_BitMask = PCCA.EXT.fu_BitANDMask(Which_Flags, c_ShareModified)
IF l_BitMask = c_ShareModified THEN
   i_Modified  = New_State

   l_ShareFlag = New_State
   IF NOT New_State THEN
      IF i_SharePrimary.i_Modified THEN
         l_ShareFlag = TRUE
      ELSE
         FOR l_Idx = 1 TO i_SharePrimary.i_NumShareSecondary
            l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
            IF l_TmpDW.i_Modified THEN
               l_ShareFlag = TRUE
               EXIT
            END IF
         NEXT
      END IF
   END IF

   i_SharePrimary.i_ShareModified = l_ShareFlag
ELSE
 l_BitMask = PCCA.EXT.fu_BitANDMask(Which_Flags, &
                                      c_ShareAllModified)
   IF l_BitMask = c_ShareAllModified THEN
      i_SharePrimary.i_ShareModified = New_State

      i_SharePrimary.i_Modified      = New_State
      FOR l_Idx = 1 TO i_SharePrimary.i_NumShareSecondary
         l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
         l_TmpDW.i_Modified = New_State
      NEXT
   END IF
END IF

//----------
//  Set the i_DoSetKey flags.
//----------

l_BitMask = PCCA.EXT.fu_BitANDMask(Which_Flags, c_ShareAllDoSetKey)
IF l_BitMask = c_ShareAllDoSetKey THEN
   i_SharePrimary.i_ShareDoSetKey = New_State

   FOR l_Idx = 0 TO i_SharePrimary.i_NumShareSecondary
      IF l_Idx = 0 THEN
         l_TmpDW = i_SharePrimary
      ELSE
         l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
      END IF
      IF l_TmpDW.i_DoSetKey <> New_State THEN
         l_TmpDW.i_DoSetKey                   = New_State
         l_TmpDW.is_EventControl.Control_Mode = &
            c_ControlRows
         l_TmpDW.TriggerEvent("pcd_SetControl")
      END IF
   NEXT
END IF

RETURN
end subroutine

public subroutine share_insert (long insert_row_nbr);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Share_Insert
//  Description   : Internal Routine.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER     l_Idx
UO_DW_MAIN  l_TmpDW
STRING      l_HorizScroll

FOR l_Idx = 0 TO i_SharePrimary.i_NumShareSecondary
   IF l_Idx = 0 THEN
      l_TmpDW = i_SharePrimary
   ELSE
      l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
   END IF

// MS - INIT PATCH
////   IF l_TmpDW.i_CursorRow >= Insert_Row_Nbr THEN
////      l_TmpDW.i_CursorRow = l_TmpDW.i_CursorRow + 1
////   END IF
////
////   IF l_TmpDW.i_AnchorRow >= Insert_Row_Nbr THEN
////      l_TmpDW.i_AnchorRow = l_TmpDW.i_AnchorRow + 1
////   END IF
////
////   IF l_TmpDW.i_ShowRow >= Insert_Row_Nbr THEN
////      l_TmpDW.i_ShowRow = l_TmpDW.i_ShowRow + 1
////   END IF
//
//	l_TmpDW.i_CursorRow = Insert_Row_Nbr
//	l_TmpDW.i_AnchorRow = Insert_Row_Nbr
//	l_TmpDW.i_ShowRow = Insert_Row_Nbr


IF i_ShareData THEN
   l_TmpDW.i_CursorRow = Insert_Row_Nbr
   l_TmpDW.i_ShowRow   = Insert_Row_Nbr
   l_TmpDW.i_AnchorRow = Insert_Row_Nbr
ELSE
   IF l_TmpDW.i_CursorRow >= Insert_Row_Nbr THEN
      l_TmpDW.i_CursorRow = l_TmpDW.i_CursorRow + 1
   END IF
   IF l_TmpDW.i_AnchorRow >= Insert_Row_Nbr THEN
      l_TmpDW.i_AnchorRow = l_TmpDW.i_AnchorRow + 1
   END IF
   IF l_TmpDW.i_ShowRow >= Insert_Row_Nbr THEN
      l_TmpDW.i_ShowRow = l_TmpDW.i_ShowRow + 1
   END IF
END IF

// END PATCH
   
	IF l_Idx > 0 THEN
      l_TmpDW.Push_DW_Redraw(c_DrawNow)
   END IF

   l_HorizScroll = l_TmpDW.Describe("DataWindow.HorizontalScrollPosition")
   l_TmpDW.ScrollToRow(l_TmpDW.i_CursorRow)
   l_TmpDW.Modify("DataWindow.HorizontalScrollPosition=" + l_HorizScroll)

   IF l_Idx > 0 THEN
      l_TmpDW.Pop_DW_Redraw()
   END IF
NEXT

RETURN
end subroutine

public subroutine share_remove (long remove_row_nbr);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Share_Remove
//  Description   : Internal Routine.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER     l_Idx
LONG        l_RowCount
UO_DW_MAIN  l_TmpDW
STRING      l_HorizScroll

FOR l_Idx = 0 TO i_SharePrimary.i_NumShareSecondary
   IF l_Idx = 0 THEN
      l_TmpDW = i_SharePrimary
   ELSE
      l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
   END IF

   IF Remove_Row_Nbr > 0 THEN
      IF l_TmpDW.i_CursorRow > Remove_Row_Nbr THEN
         l_TmpDW.i_CursorRow = l_TmpDW.i_CursorRow - 1
      END IF

      IF l_TmpDW.i_AnchorRow > Remove_Row_Nbr THEN
         l_TmpDW.i_AnchorRow = l_TmpDW.i_AnchorRow - 1
      ELSE
         IF l_TmpDW.i_AnchorRow = Remove_Row_Nbr THEN
            l_TmpDW.i_AnchorRow = 0
         END IF
      END IF

      IF l_TmpDW.i_ShowRow > Remove_Row_Nbr THEN
         l_TmpDW.i_ShowRow = l_TmpDW.i_ShowRow - 1
      ELSE
         IF l_TmpDW.i_ShowRow = Remove_Row_Nbr THEN
            l_TmpDW.i_RetrieveChildren = TRUE
            l_TmpDW.i_ShowRow          = 0
         END IF
      END IF

      IF l_TmpDW.IsSelected(Remove_Row_Nbr) THEN
         l_TmpDW.i_RetrieveChildren = TRUE
      END IF

      l_TmpDW.Find_Instance(Remove_Row_Nbr)
      IF l_TmpDW.i_CurInstance > 0 THEN
         l_TmpDW.i_InstanceDW[i_CurInstance].TriggerEvent &
            ("pcd_Close")
      END IF
   ELSE
      IF NOT l_TmpDW.i_IsEmpty THEN
         l_RowCount = l_TmpDW.RowCount()

         IF l_TmpDW.i_ShowRow = 0 THEN
            l_TmpDW.i_ShowRow = l_TmpDW.GetSelectedRow(0)
            IF l_TmpDW.i_ShowRow > 0 THEN
               l_TmpDW.Find_Instance(l_TmpDW.i_ShowRow)
            END IF
         END IF

         l_TmpDW.i_NumSelected = &
            l_TmpDW.Get_Retrieve_Rows(l_TmpDW.i_SelectedRows[])

         IF l_TmpDW.i_CursorRow > l_RowCount THEN
            l_TmpDW.i_CursorRow = l_RowCount
         END IF

         IF l_Idx > 0 THEN
            l_TmpDW.Push_DW_Redraw(c_DrawNow)
         END IF

         l_HorizScroll = l_TmpDW.Describe &
                            ("DataWindow.HorizontalScrollPosition")
         l_TmpDW.ScrollToRow(l_TmpDW.i_CursorRow)
         l_TmpDW.Modify &
            ("DataWindow.HorizontalScrollPosition=" + l_HorizScroll)

         IF l_Idx > 0 THEN
            l_TmpDW.Pop_DW_Redraw()
         END IF
      END IF
   END IF
NEXT

RETURN
end subroutine

public subroutine share_state (unsignedlong which_state, boolean new_state);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Share_State
//  Description   : Sets various DataWindow states in DataWindows
//                  which are using share result sets.
//
//  Parameters    : UNSIGNEDLONG  Which_State -
//                        Indicates which state in the
//                        DataWindows should be modified.
//                        Valid options are:
//
//                           - c_SharePushRedraw
//                           - c_SharePopRedraw
//                           - c_ShareEmpty
//                           - c_ShareNotEmpty
//
//                  BOOLEAN  New_State -
//                        The new value of the state.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER     l_Idx
UO_DW_MAIN  l_TmpDW

CHOOSE CASE Which_State
   CASE c_ShareEmpty
      FOR l_Idx = 0 TO i_SharePrimary.i_NumShareSecondary
         IF l_Idx = 0 THEN
            l_TmpDW = i_SharePrimary
         ELSE
            l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
         END IF

         l_TmpDW.i_IsEmpty    = TRUE
         l_TmpDW.i_AddedEmpty = New_State
         l_TmpDW.TriggerEvent("pcd_SetRowIndicator")

         IF NOT l_TmpDW.i_SetEmptyColors THEN
            l_TmpDW.i_SetEmptyColors = TRUE

            IF l_TmpDW.i_InactiveCol THEN
               IF l_TmpDW.i_Active THEN
                  l_TmpDW.Modify(l_TmpDW.i_ActiveEmptyColors)
               ELSE
                  l_TmpDW.Modify(l_TmpDW.i_InactiveEmptyColors)
               END IF
            ELSE
               l_TmpDW.Modify(l_TmpDW.i_EmptyTextColors)
            END IF
         END IF

         l_TmpDW.i_NumSelected = 0
         l_TmpDW.i_CursorRow   = 0
         l_TmpDW.i_ShowRow     = 0
         l_TmpDW.i_AnchorRow   = 0

         l_TmpDW.TriggerEvent("pcd_Disable")
      NEXT

   CASE c_ShareNotEmpty
      FOR l_Idx = 0 TO i_SharePrimary.i_NumShareSecondary
         IF l_Idx = 0 THEN
            l_TmpDW = i_SharePrimary
         ELSE
            l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
         END IF

         l_TmpDW.i_IsEmpty    = FALSE
         l_TmpDW.i_AddedEmpty = FALSE
         l_TmpDW.TriggerEvent("pcd_SetRowIndicator")

         IF l_TmpDW.i_SetEmptyColors THEN
            l_TmpDW.i_SetEmptyColors = FALSE

            IF l_TmpDW.i_InactiveCol THEN
               IF l_TmpDW.i_Active THEN
                  l_TmpDW.Modify(l_TmpDW.i_ActiveNonEmptyColors)
               ELSE
                  l_TmpDW.Modify(l_TmpDW.i_InactiveNonEmptyColors)
               END IF
            ELSE
               l_TmpDW.Modify(l_TmpDW.i_NonEmptyTextColors)
            END IF
         END IF
      NEXT

   CASE c_SharePushRedraw
      i_SharePrimary.Push_DW_Redraw(New_State)
      FOR l_Idx = 1 TO i_SharePrimary.i_NumShareSecondary
         l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
         l_TmpDW.Push_DW_Redraw(New_State)
      NEXT

   CASE c_SharePopRedraw
      i_SharePrimary.Pop_DW_Redraw()
      FOR l_Idx = 1 TO i_SharePrimary.i_NumShareSecondary
         l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
         l_TmpDW.Pop_DW_Redraw()
      NEXT

   CASE c_SharePushRFC
      i_SharePrimary.Push_DW_RFC(New_State)
      FOR l_Idx = 1 TO i_SharePrimary.i_NumShareSecondary
         l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
         l_TmpDW.Push_DW_RFC(New_State)
      NEXT

   CASE c_SharePopRFC
      i_SharePrimary.Pop_DW_RFC()
      FOR l_Idx = 1 TO i_SharePrimary.i_NumShareSecondary
         l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
         l_TmpDW.Pop_DW_RFC()
      NEXT

   CASE c_SharePushValidate
      i_SharePrimary.Push_DW_Validate(New_State)
      FOR l_Idx = 1 TO i_SharePrimary.i_NumShareSecondary
         l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
         l_TmpDW.Push_DW_Validate(New_State)
      NEXT

   CASE c_SharePopValidate
      i_SharePrimary.Pop_DW_Validate()
      FOR l_Idx = 1 TO i_SharePrimary.i_NumShareSecondary
         l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
         l_TmpDW.Pop_DW_Validate()
      NEXT

   CASE c_SharePushVScroll
      i_SharePrimary.Push_DW_VScroll(New_State)
      FOR l_Idx = 1 TO i_SharePrimary.i_NumShareSecondary
         l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
         l_TmpDW.Push_DW_VScroll(New_State)
      NEXT

   CASE c_SharePopVScroll
      i_SharePrimary.Pop_DW_VScroll()
      FOR l_Idx = 1 TO i_SharePrimary.i_NumShareSecondary
         l_TmpDW = i_SharePrimary.i_ShareSecondary[l_Idx]
         l_TmpDW.Pop_DW_VScroll()
      NEXT

   CASE ELSE
END CHOOSE

RETURN
end subroutine

public subroutine unset_query ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Unset_Query
//  Description   : Takes a DataWindow out of "QUERY" mode.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Make sure the DataWindow is in "QUERY" mode before we try to
//  take it out.
//----------

IF i_DWState = c_DWStateQuery THEN

   //----------
   //  Turn off redraw processing.
   //----------

   SetRedraw(c_BufferDraw)
   i_RedrawCount = i_RedrawCount + 1
   i_RedrawStack[i_RedrawCount] = c_BufferDraw

   //----------
   //  Tell the DataWindow to go out of "QUERY" mode.
   //----------

   Modify("DataWindow.QueryMode=No")

   //----------
   //  Make the DataWindow state as unknown since we don't know
   //  what mode we are going to.
   //----------

   i_DWState = c_DWStateUndefined

   //----------
   //  While the DataWindow is in "QUERY" mode, we have turned off
   //  the display of the focus rectangle/row pointer.  If the
   //  developer specified that they are to be on, we need to
   //  turn them on.
   //----------

   TriggerEvent("pcd_SetRowIndicator")

   //----------
   //  When we put the DataWindow was put into "QUERY" mode, we
   //  disabled processing of RFC, VScroll, and validation.  Need
   //  to restore the previous states.
   //----------

   IF i_IgnoreValCount > 1 THEN
      i_IgnoreValCount = i_IgnoreValCount - 1
      i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
   ELSE
      i_IgnoreValCount = 0
      i_IgnoreVal      = FALSE
   END IF

   IF i_IgnoreVSCount > 1 THEN
      i_IgnoreVSCount = i_IgnoreVSCount - 1
      i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
   ELSE
      i_IgnoreVSCount = 0
      i_IgnoreVScroll = FALSE
   END IF
   IF NOT i_IgnoreVScroll THEN
      i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
   END IF

   IF i_IgnoreRFCCount > 1 THEN
      i_IgnoreRFCCount = i_IgnoreRFCCount - 1
      i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
   ELSE
      i_IgnoreRFCCount = 0
      i_IgnoreRFC      = FALSE
   END IF

   //----------
   //  We need to re-enable any menus and/or buttons that we
   //  disabled when we went to "QUERY" mode.
   //----------

   is_EventControl.Control_Mode = c_ControlActiveDW
   TriggerEvent("pcd_SetControl")

   //----------
   //  Restore redraw processing.
   //----------

   IF i_RedrawCount > 1 THEN
      i_RedrawCount = i_RedrawCount - 1
      SetRedraw(i_RedrawStack[i_RedrawCount])
   ELSE
      i_RedrawCount = 0
      SetRedraw(TRUE)
   END IF
END IF

RETURN
end subroutine

public subroutine unwire_button (uo_cb_main command_button);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Unwire_Button
//  Description   : Un-wires a command button from the DataWindow.
//
//  Parameters    : UO_CB_MAIN Command_Button -
//                        The command button to be unwired from
//                        a DataWindow.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN     l_Found
INTEGER     l_Idx, l_Jdx
UO_DW_MAIN  l_UnwireDW

//----------
//  Make sure that we were passed a valid button to be unwired.
//----------

IF IsValid(Command_Button) THEN

   //----------
   //  Make sure that the button points to a valid DataWindow.
   //----------

   IF IsValid(Command_Button.i_TrigObject) THEN
      l_UnwireDW = Command_Button.i_TrigObject

      //----------
      //  We need to remove this command button from this
      //  DataWindow's array of PowerClass controls.
      //----------

      l_Found = FALSE
      l_Jdx   = UpperBound(i_Window.Control[])
      FOR l_Idx = 1 TO l_Jdx
         IF Command_Button = i_Window.Control[l_Idx] THEN
            l_Found = TRUE
            EXIT
         END IF
      NEXT
      IF NOT l_Found THEN
         Remove_Obj(Command_Button,             &
                    l_UnwireDW.i_NumPCControls, &
                    l_UnwireDW.i_PCControls[])
      END IF
   END IF

   //----------
   //  Clear the variables the contains the reference to the
   //  DataWindow we were wired to.
   //----------

   Command_Button.i_TrigObject = PCCA.Null_Object
END IF

RETURN
end subroutine

public subroutine unwire_filter (graphicobject filter_obj);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Unwire_Filter
//  Description   : Un-wires a filter object from the DataWindow.
//
//  Parameters    : GRAPHICOBJECT FilterObj -
//                        The filter object to be unwired from
//                        a DataWindow.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

UO_DDDW_FILTER_MAIN  l_DDDWName
UO_DW_FILTER         l_DWName
UO_DDLB_FILTER       l_DDLBName
UO_EM_FILTER         l_EMName
UO_LB_FILTER         l_LBName
UO_SLE_FILTER        l_SLEName
UO_DW_MAIN           l_TmpDW

//----------
//  Make sure we have a valid object to be unwired.
//----------

IF IsValid(Filter_Obj) THEN

   //----------
   //  Filter objects can be any one of five different types:
   //  DDDW, DDLB, EM, LB, or SLE.  Figure out which type this
   //  filter object is and get and set the appropriate
   //  information.
   //----------

   CHOOSE CASE Filter_Obj.Tag
      CASE "PowerObject Filter DDDW"
         l_DDDWName              = Filter_Obj
         l_TmpDW                 = l_DDDWName.i_PCFilterDW
         l_DDDWName.i_PCFilterDW = PCCA.Null_Object
         l_DDDWName.Enabled      = FALSE
         l_DDDWName.fu_UnwireDW()
      CASE "PowerObject Filter DW"
         l_DWName                = Filter_Obj
         l_TmpDW                 = l_DWName.i_PCFilterDW
         l_DWName.i_PCFilterDW   = PCCA.Null_Object
         l_DWName.Enabled        = FALSE
         l_DWName.fu_UnwireDW()
      CASE "PowerObject Filter DDLB"
         l_DDLBName              = Filter_Obj
         l_TmpDW                 = l_DDLBName.i_PCFilterDW
         l_DDLBName.i_PCFilterDW = PCCA.Null_Object
         l_DDLBName.Enabled      = FALSE
         l_DDLBName.fu_UnwireDW()
      CASE "PowerObject Filter EM"
         l_EMName                = Filter_Obj
         l_TmpDW                 = l_EMName.i_PCFilterDW
         l_EMName.i_PCFilterDW   = PCCA.Null_Object
         l_EMName.Enabled        = FALSE
         l_EMName.fu_UnwireDW()
      CASE "PowerObject Filter LB"
         l_LBName                = Filter_Obj
         l_TmpDW                 = l_LBName.i_PCFilterDW
         l_LBName.i_PCFilterDW   = PCCA.Null_Object
         l_LBName.Enabled        = FALSE
         l_LBName.fu_UnwireDW()
      CASE "PowerObject Filter SLE"
         l_SLEName               = Filter_Obj
         l_TmpDW                 = l_SLEName.i_PCFilterDW
         l_SLEName.i_PCFilterDW  = PCCA.Null_Object
         l_SLEName.Enabled       = FALSE
         l_SLEName.fu_UnwireDW()
      CASE ELSE
         l_TmpDW = PCCA.Null_Object
   END CHOOSE

   //----------
   //  Make sure that this filter object is wired to a valid
   //  DataWindow.  If it is, remove it from the array of filter
   //  objects in that DataWindow.
   //----------

   IF IsValid(l_TmpDW) THEN
      Remove_Obj(Filter_Obj,                 &
                 l_TmpDW.i_NumFilterObjects, &
                 l_TmpDW.i_FilterObjects[])
   END IF
END IF

RETURN
end subroutine

public subroutine unwire_find (uo_dw_find find_obj);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Unwire_Find
//  Description   : Un-wires a find object from the DataWindow.
//
//  Parameters    : UO_DW_FIND Find_Obj -
//                        The find object to be unwired from
//                        a DataWindow.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

UO_DW_MAIN  l_TmpDW

//----------
//  Make sure we have a valid object to be unwired.
//----------

IF IsValid(Find_Obj) THEN

   //----------
   //  Make sure that this find object is wired to a valid
   //  DataWindow.  If it is, remove it from the array of find
   //  objects in that DataWindow.
   //----------

   IF IsValid(Find_Obj.i_FindDW) THEN
       l_TmpDW = Find_Obj.i_FindDW
       Remove_Obj(Find_Obj,                 &
                  l_TmpDW.i_NumFindObjects, &
                  l_TmpDW.i_FindObjects[])
   END IF

   //----------
   //  Indicate that this find object is not wired to any
   //  DataWindows.  Disable the find object since it is not
   //  wired to a DataWindow.
   //----------

   Find_Obj.i_FindDW = PCCA.Null_Object
   Find_Obj.Enabled  = FALSE
END IF

RETURN
end subroutine

public subroutine unwire_message (uo_cb_message messagecb);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Unwire_Message
//  Description   : Un-wires a message button from the DataWindow.
//
//  Parameters    : UO_CB_MESSAGE MessageCB -
//                        The message button to be unwired from
//                        a DataWindow.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

UO_DW_MAIN  l_TmpDW

//----------
//  Make sure we have a valid object to be unwired.
//----------

IF IsValid(MessageCB) THEN

   //----------
   //  Make sure that this message button is wired to a valid
   //  DataWindow.  If it is, remove it from the array of message
   //  buttons in that DataWindow.
   //----------

   IF IsValid(MessageCB.i_MessageDW) THEN
      l_TmpDW = MessageCB.i_MessageDW
      Remove_Obj(MessageCB,             &
                 l_TmpDW.i_NumMessages, &
                 l_TmpDW.i_Messages[])

   END IF

   //----------
   //  Indicate that this message button is not wired to any
   //  DataWindows.  Disable the message button since it is not
   //  wired to a DataWindow.
   //----------

   MessageCB.i_MessageDW = PCCA.Null_Object
   MessageCB.Enabled     = FALSE
END IF

RETURN
end subroutine

public subroutine unwire_search (graphicobject search_obj);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Unwire_Search
//  Description   : Un-wires a search object from the DataWindow.
//
//  Parameters    : GRAPHICOBJECT SearchObj -
//                        The search object to be unwired from
//                        a DataWindow.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

UO_DDDW_SEARCH_MAIN  l_DDDWName
UO_DW_SEARCH         l_DWName
UO_DDLB_SEARCH       l_DDLBName
UO_EM_SEARCH         l_EMName
UO_LB_SEARCH         l_LBName
UO_SLE_SEARCH        l_SLEName
UO_DW_MAIN           l_TmpDW

//----------
//  Make sure we have a valid object to be unwired.
//----------

IF IsValid(Search_Obj) THEN

   //----------
   //  Search objects can be any one of five different types:
   //  DDDW, DDLB, EM, LB, or SLE.  Figure out which type this
   //  search object is and get and set the appropriate
   //  information.
   //----------

   CHOOSE CASE Search_Obj.Tag
      CASE "PowerObject Search DDDW"
         l_DDDWName              = Search_Obj
         l_TmpDW                 = l_DDDWName.i_PCSearchDW
         l_DDDWName.i_PCSearchDW = PCCA.Null_Object
         l_DDDWName.Enabled      = FALSE
         l_DDDWName.fu_UnwireDW()
      CASE "PowerObject Search DW"
         l_DWName                = Search_Obj
         l_TmpDW                 = l_DWName.i_PCSearchDW
         l_DWName.i_PCSearchDW   = PCCA.Null_Object
         l_DWName.Enabled        = FALSE
         l_DWName.fu_UnwireDW()
      CASE "PowerObject Search DDLB"
         l_DDLBName              = Search_Obj
         l_TmpDW                 = l_DDLBName.i_PCSearchDW
         l_DDLBName.i_PCSearchDW = PCCA.Null_Object
         l_DDLBName.Enabled      = FALSE
         l_DDLBName.fu_UnwireDW()
      CASE "PowerObject Search EM"
         l_EMName                = Search_Obj
         l_TmpDW                 = l_EMName.i_PCSearchDW
         l_EMName.i_PCSearchDW   = PCCA.Null_Object
         l_EMName.Enabled        = FALSE
         l_EMName.fu_UnwireDW()
      CASE "PowerObject Search LB"
         l_LBName                = Search_Obj
         l_TmpDW                 = l_LBName.i_PCSearchDW
         l_LBName.i_PCSearchDW   = PCCA.Null_Object
         l_LBName.Enabled        = FALSE
         l_LBName.fu_UnwireDW()
      CASE "PowerObject Search SLE"
         l_SLEName               = Search_Obj
         l_TmpDW                 = l_SLEName.i_PCSearchDW
         l_SLEName.i_PCSearchDW  = PCCA.Null_Object
         l_SLEName.Enabled       = FALSE
         l_SLEName.fu_UnwireDW()
      CASE ELSE
         l_TmpDW = PCCA.Null_Object
   END CHOOSE

   //----------
   //  Make sure that this search object is wired to a valid
   //  DataWindow.  If it is, remove it from the array of search
   //  objects in that DataWindow.
   //----------

   IF IsValid(l_TmpDW) THEN
      Remove_Obj(Search_Obj,                 &
                 l_TmpDW.i_NumSearchObjects, &
                 l_TmpDW.i_SearchObjects[])
   END IF
END IF

RETURN
end subroutine

public subroutine unwire_share (uo_dw_main secondary_dw);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Unwire_Share
//  Description   : Un-wires a secondary shared DataWindow from
//                  the primary share DataWindow.
//
//  Parameters    : UO_DW_MAIN Secondary_DW -
//                        The DataWindow who wants to quit doing
//                        a share with this DataWindow.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//----------
//  Turn sharing off.
//----------

Share_State(c_SharePushRFC,     c_IgnoreRFC)
Share_State(c_SharePushVScroll, c_IgnoreVScroll)
Secondary_DW.ShareDataOff()

//----------
//  Now that Secondary_DW has lost its share, we need to reset
//  some states.
//----------

Secondary_DW.i_ShareType    = c_ShareNone
Secondary_DW.i_SharePrimary = Secondary_DW
Secondary_DW.i_ShareParent  = Secondary_DW
Secondary_DW.i_IsEmpty      = FALSE
Secondary_DW.i_AddedEmpty   = FALSE

IF Secondary_DW.i_InUse THEN
   Secondary_DW.i_Modified = FALSE
   Secondary_DW.i_DoSetKey = FALSE

   Secondary_DW.Reset()

   is_EventControl.Refresh_Cmd  = c_RefreshSame
   is_EventControl.Reset        = TRUE
   Secondary_DW.TriggerEvent("pcd_Refresh")

   is_EventControl.Control_Mode = c_ControlRows
   Secondary_DW.TriggerEvent("pcd_SetControl")
END IF

Share_State(c_SharePopVScroll, c_StateUnused)
Share_State(c_SharePopRFC,     c_StateUnused)

//----------
//  Since Secondary_DW is not longer doing a share, remove it
//  from the list of shared DataWindows.
//----------

Remove_Obj(Secondary_DW, i_NumShareSecondary, i_ShareSecondary[])

RETURN
end subroutine

public subroutine update_modified ();//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Update_Modified
//  Description   : Insures that i_Modified flag is up to date.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//----------
//  If the DataWindow is in "QUERY" mode, we don't care
//  about its modified state.
//----------

IF i_RootDW.i_DWState = c_DWStateQuery THEN
   IF i_SharePrimary.i_ShareModified OR &
      i_SharePrimary.i_ShareDoSetKey THEN
      Share_Flags(c_ShareAllModified + c_ShareAllDoSetKey, FALSE)
   END IF
ELSE

   //----------
   //  i_Modified gives a quick indication if the DataWindow has
   //  been modified because it will be set to TRUE anytime the
   //  user enters data into a field.  However, if it is not
   //  TRUE, we must do a little more extensive checking.
   //----------

   IF NOT i_SharePrimary.i_ShareModified THEN

      //----------
      //  Check with PowerBuilder to see if it thinks the
      //  DataWindow has been modified.  If it does not, then
      //  see if New! records have been added.
      //----------

      IF i_SharePrimary.ModifiedCount() > 0 THEN
         i_Modified = TRUE
      ELSE
         IF i_SharePrimary.DeletedCount() > 0 THEN
            i_Modified = TRUE
         ELSE
            i_Modified = (i_DoSetKey AND NOT i_IgnoreNewRows)
         END IF
      END IF

      IF i_Modified THEN
         Share_Flags(c_ShareModified, TRUE)
      END IF
   END IF
END IF

RETURN
end subroutine

public subroutine validate_columns (unsignedlong which_columns, long except_column);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Validate_Columns
//  Description   : Validates the i_RowNbr row.
//
//  Parameters    : UNSIGNEDLONG  Which_Columns -
//                       Indicates which columns should be
//                       validated.  Valid options are:
//
//                          - c_ValidateAllCols
//                               All columns should be validated.
//
//                          - c_ValidateCurCol
//                               Only the column specified by
//                               i_ColNbr will be validated.
//
//                  LONG Except_Column -
//                        This column will be skipped during the
//                        validate process preformed by
//                        Validate_Col().  Pass a value of 0 for
//                        this parameter to have all of the
//                        columns on the row validated.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Start, l_End
DWITEMSTATUS  l_ItemStatus

//----------
//  Move to the row to be validated.
//----------

SetRow(i_RowNbr)

//----------
//  See if all columns or just the current column is to be
//  validated.
//----------

IF Which_Columns = c_ValidateAllCols THEN
   l_Start = 1
   l_End   = i_NumColumns
ELSE
   l_Start = i_ColNbr
   l_End   = i_ColNbr
END IF

//----------
//  Cycle through all of the columns on the DataWindow looking
//  for columns that need to be verified.
//----------

FOR i_ColNbr = l_Start TO l_End

   //----------
   //  Make sure that the column is not to be skipped.
   //----------

   IF Except_Column <> i_ColNbr THEN

      //----------
      //  Is the column on the DataWindow?
      //----------

      IF SetColumn(i_ColNbr) <> -1 THEN

         //----------
         //  Get the variables that are used for validation ready.
         //----------

         Get_Val_Info(i_ColNbr)

         //----------
         //  Find out if this column been modified.
         //----------

         l_ItemStatus = GetItemStatus(i_RowNbr, &
                                               i_ColNbr, &
                                               Primary!)

         //----------
         //  Initialize PCCA.Error to indicate that validation
         //  processing has not been done.  Initialize
         //  i_DisplayRequired to indicate that required field
         //  errors should be reported.  If the developer does
         //  not want PowerClass to do required field reporting,
         //  they can set this flag to FALSE.
         //----------

         PCCA.Error        = c_ValNotProcessed
         i_DisplayRequired = TRUE

         //----------
         //  If the item has been modified, then we need to trigger
         //  the developer's validation event.  Also, if this is a
         //  column that is going to generate a required field
         //  error, we will let the developer's validation event
         //  have the first crack at displaying a validation error
         //  message.
         //----------

         IF l_ItemStatus <> NotModified! OR i_ColRequiredError THEN

            //----------
            //  pcd_ValidateCol performs validation on column by
            //  column basis.  This event must be coded by the
            //  developer.
            //----------
            //  Validation routines are expected to do their own
            //  error display.  Therefore, i_DisplayError is not
            //  checked for developer-coded validation routines.
            //----------

            TriggerEvent("pcd_ValidateCol")
         END IF

         //----------
         //  We need to display the required field error if:
         //     a) If we have a required field error AND
         //     b) The developer's routine did not catch or
         //        process it AND
         //     c) The developer said it was Ok to display it.
         //----------

         IF i_ColRequiredError               AND &
            (PCCA.Error = c_ValOk            OR  &
             PCCA.Error = c_ValNotProcessed) AND &
            i_DisplayRequired                THEN

            //----------
            //  Use Display_DW_Val_Error() to display the required
            //  field error.
            //----------

            Display_DW_Val_Error(PCCA.MB.c_MBI_DW_RequiredField, "")

            //----------
            //  Since this is a required field, we have an error.
            //----------

            PCCA.Error = c_Fatal
         ELSE

            //----------
            //  If the developer's validation event returned
            //  an error, then set PCCA.Error to return the
            //  PowerClass fatal error.  Otherwise, indicate
            //  that this function was successful.
            //----------

            IF PCCA.Error = c_ValFailed THEN
               PCCA.Error = c_Fatal
            ELSE
               PCCA.Error = c_Success
            END IF
         END IF
      END IF

      //----------
      //  If we had an error, then we don't need to check any more
      //  columns.
      //----------

      IF PCCA.Error <> c_Success THEN
         EXIT
      END IF
   END IF
NEXT

RETURN
end subroutine

public subroutine debug (string event_name, string prefix, integer debug_level);
end subroutine

public function boolean dw_buffer_drawing (boolean new_val);RETURN TRUE
end function

public function long dw_debug_count (long new_val);RETURN 0
end function

public subroutine enter_dw_filter ();
end subroutine

public subroutine enter_dw_itemerror ();
end subroutine

public subroutine enter_dw_refresh ();
end subroutine

public subroutine enter_dw_reselect ();
end subroutine

public subroutine enter_dw_rfc ();
end subroutine

public subroutine enter_dw_save ();
end subroutine

public subroutine enter_dw_search ();
end subroutine

public subroutine enter_dw_update ();
end subroutine

public subroutine exit_dw_filter ();
end subroutine

public subroutine exit_dw_itemerror ();
end subroutine

public subroutine exit_dw_refresh ();
end subroutine

public subroutine exit_dw_reselect ();
end subroutine

public subroutine exit_dw_rfc ();
end subroutine

public subroutine exit_dw_save ();
end subroutine

public subroutine exit_dw_search ();
end subroutine

public subroutine exit_dw_update ();
end subroutine

public subroutine proc_entry (string proc_name, long debug_dump_level);
end subroutine

public subroutine proc_exit ();
end subroutine

public subroutine remove_obj (powerobject a_object, ref integer num_objects, ref powerobject objects[]);//******************************************************************
//  PC Module     : uo_DW_Main
//  Subroutine    : Remove_Obj
//  Description   : Removes the specified object from the array
//                  of objects.
//
//  Parameters    : POWEROBJECT a_Object -
//                        The object to be removed from the
//                        Objects[] array.
//
//                  ref INTEGER Num_Objects -
//                        The number of objects contained in
//                        the Objects[] array.  Modified with
//                        the new count of objects if an object
//                        is removed from the array.
//
//                  ref POWEROBJECT Objects[] -
//                        The array of objects.  If Object is
//                        found in the array, it will be
//                        removed.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER      l_NumLeft, l_Idx
POWEROBJECT  l_TmpObj

//----------
//  Search through the Objects[] array, looking for the specifed
//  Object.
//----------

l_NumLeft = 0
FOR l_Idx = 1 TO Num_Objects
   l_TmpObj = Objects[l_Idx]

   //----------
   //  If this object is not the same as the specified Object,
   //  leave it in the Objects[] array.
   //----------

   IF l_TmpObj <> A_Object THEN
      l_NumLeft = l_NumLeft + 1

      //----------
      //  If l_Idx is equal to l_NumLeft, then no objects have
      //  been deleted and the objects in the array do not have
      //  to be shifted down.  If l_Idx is not equal to l_NumLeft,
      //  then an element has been deleted and the objects need
      //  to be shuffled down in the array.
      //----------

      IF l_Idx <> l_NumLeft THEN
         Objects[l_NumLeft] = l_TmpObj
      END IF
   END IF
NEXT

//----------
//  Return back the number of objects left in the Objects[] array.
//----------

Num_Objects = l_NumLeft

RETURN
end subroutine

public subroutine set_document_name (string as_document_name);/**
 * stefanop
 * 09/06/2014
 *
 * Alias per impostare il nome del documento durante il processo di stampa.
 * Il nome verrà visualizzato nello spooler di stampa
 **/
 
if trim(as_document_name) = "" then setnull(as_document_name)
	
Object.DataWindow.Print.DocumentName = as_document_name
end subroutine

event itemerror;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : ItemError
//  Description   : This event is used to disable the display
//                  of the generic PowerBuilder validation error
//                  message.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_NullError,     l_Enabled
INTEGER  l_SetActionCode, l_DefaultButton
STRING   l_Title,         l_Text
BUTTON   l_ButtonSet
ICON     l_Icon

i_InItemError = i_InItemError + 1

//----------
//  Occasionally you might initialize fields with values.  Doing
//  this may cause this event to trigger.  If you don't want to do
//  validation checking, then do the following:
//
//      Push_DW_Validate(c_IgnoreVal)
//      <code the generates the ItemError! Event>
//      Pop_DW_Validate()
//
//  Do not modify the i_IgnoreVal instance variable!
//----------

IF i_IgnoreVal THEN

   //----------
   //  Don't display the error message from Powerbuilder, but
   //  allow focus to change.
   //----------

   l_SetActionCode = i_IgnoreValAction

   GOTO Finished
END IF

//----------
//  Trigger the ItemChanged! event to initiate the validation
//  process only if it has not already run (i_ItemValidated tells
//  if it has already been run).  ItemChanged! can be triggered
//  before this event by ACCEPTTEXT().
//----------

IF NOT i_ItemValidated THEN

   //----------
   //  See if we are worried about required field checking every
   //  time the user moves.  If not, then see if ItemError was
   //  triggered because of a required field error.  If so, then
   //  we don't need to do any more processing for ItemError.
   //----------

   IF NOT i_AlwaysCheckRequired THEN
      i_RowNbr = GetRow()
      i_ColNbr = GetColumn()

      IF i_RowNbr > 0 AND i_ColNbr > 0 THEN
         Get_Val_Info(i_ColNbr)

         IF i_ColRequiredError THEN
            l_SetActionCode = c_AcceptAndMove
            GOTO Finished
         END IF
      END IF
   END IF

   TriggerEvent(ItemChanged!)

   //----------
   //  Unless the developer specified that validation error
   //  messages were not to be displayed, set the action code
   //  appropriately.
   //----------

   IF NOT PCCA.No_Validation_Error_Display THEN

      //----------
      //  Convert ItemChanged! action codes to ItemError! action
      //  codes.
      //----------

      CHOOSE CASE PCCA.Error
         CASE c_ValOk

            //----------
            //  The developer told us that the value is good.
            //  Tell PowerBuilder to accept it and move.
            //----------

            l_SetActionCode = c_AcceptAndMove

         CASE c_ValFixed

            //----------
            //  The value was bad, but it was fixed using
            //  SETITEM().  Therefore, we don't want to accept
            //  the user's text over the correction.  Just
            //  reject the user's value and move.
            //----------

            l_SetActionCode = c_RejectAndMove

         CASE c_ValFailed

            //----------
            //  The value failed validation and the developer was
            //  unable to fix it.  Reject the value and stay put.
            //----------

            l_SetActionCode = c_RejectAndStay

         CASE c_ValNotProcessed

            //----------
            //  The developer did not do any processing in the
            //  pcd_ValidateCol event.  See if ItemError! was
            //  triggered because of a required field error.  If
            //  it was, we don't want PowerBuilder to display an
            //  error message.
            //----------

            IF i_ColRequiredError THEN
               PCCA.Error      = c_ValOk
               l_SetActionCode = c_RejectAndMove
            ELSE

               //----------
               //  PowerBuilder thinks something is wrong and we
               //  now know that it is not a required field error.
               //  If there is a validation error message in the
               //  DataWindow call Display_DW_Val_Error() to
               //  display it.
               //----------

               IF Len(Trim(i_ColValMsg)) > 0 THEN
                  Display_DW_Val_Error(PCCA.MB.c_MBI_DW_DWValError, "")
                  PCCA.Error      = c_ValFailed
                  l_SetActionCode = c_RejectAndStay
               ELSE

                  //----------
                  //  Nothing else to do but let PowerBuilder
                  //  display the message.
                  //----------

                  PCCA.Error      = c_ValFailed
                  l_SetActionCode = c_RejectAndShowPBError
               END IF
            END IF
         CASE ELSE

            //----------
            //  If we get to here, then the developer gave us back
            //  a bad validation return.
            //----------

            PCCA.Error      = c_ValFailed
            l_SetActionCode = c_RejectAndShowPBError
      END CHOOSE

      //----------
      //  If we're going to let PowerBuilder display the error,
      //  then build the title for PowerBuilder.
      //----------

      IF l_SetActionCode = c_RejectAndShowPBError THEN
         PCCA.MB.i_MB_Numbers[1] = i_ColFormatNumber
         PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::ItemError"
         PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
         PCCA.MB.i_MB_Strings[3] = i_ClassName
         PCCA.MB.i_MB_Strings[4] = i_Window.Title
         PCCA.MB.i_MB_Strings[5] = DataObject
         PCCA.MB.i_MB_Strings[6] = i_ColText
         PCCA.MB.i_MB_Strings[7] = i_ColName
         PCCA.MB.i_MB_Strings[8] = ""
         PCCA.MB.i_MB_Strings[9] = i_ColFormatString
         PCCA.MB.fu_GetInfo(PCCA.MB.c_MBI_DW_PBValidation,           &
                         1, PCCA.MB.i_MB_Numbers[],               &
                         9, PCCA.MB.i_MB_Strings[],               &
                         l_Title,     l_Text,          l_Icon, &
                         l_ButtonSet, l_DefaultButton, l_Enabled)

         IF l_Enabled THEN
            l_Title = "DataWindow.Message.Title='"    + &
                      PCCA.MB.fu_QuoteChar(l_Title, "'") + "'"
            Modify(l_Title)
         END IF
      END IF
   ELSE

      //----------
      //  The developer said that validation errors were not to
      //  be displayed, but there is an error with the data.
      //  Reject the value and don't move.
      //----------

      l_SetActionCode = c_RejectAndStay
   END IF
ELSE

   //----------
   //  If we get here, ItemChanged! has run, but Powerbuilder
   //  thinks there is is an error with the field.  Don't display
   //  the error message from Powerbuilder, but reject the value.
   //----------

   l_SetActionCode = c_RejectAndStay
END IF

//----------
//  Clear the validation state flag indicating that it is Ok to
//  do validation checking.
//----------

i_ItemValidated = FALSE

Finished:

//----------
//  Set the flag to let the developer know if they should execute
//  their extended code at this time.
//----------

i_ExtendMode = (NOT i_IgnoreVal AND i_InUse)

//----------
//  The action code is interpreted as follows:
//     c_RejectAndShowPBError (0):
//        The data is to be rejected and focus should be returned
//        to the field in error.  PowerBuilder will display a
//        DataWindow error message.
//     c_RejectAndStay (1):
//        The data is to be rejected and focus should be returned
//        to the field in error.  PowerBuilder will not display
//        a DataWindow error message.
//     c_AcceptAndMove (2):
//        The data is to be accepted and focus should be allowed
//        to change.  PowerBuilder will not display a DataWindow
//        error message.
//     c_RejectAndMove (3):
//        The data is to be rejected but focus should be allowed
//        to change.  PowerBuilder will not display a DataWindow
//        error message.
//----------

IF i_InItemError > 1 THEN
   i_InItemError = i_InItemError - 1
ELSE
   i_InItemError = 0
END IF

// EnMe 17-02-2009 Nuovo sistema con Return
//SetActionCode(l_SetActionCode)
return l_SetActionCode
end event

on rowfocuschanged;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : RowFocusChanged
//  Description   : Maintains selected rows, cursor row, and anchor
//                  row.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_UnSelect,    l_IsEmpty,       l_SetFocus
BOOLEAN       l_SaveDrill,   l_IsShifted,     l_IsControl
BOOLEAN       l_MoveFocus,   l_Tab,           l_ForceSelect
BOOLEAN       l_SaveSelect,  l_SaveMultiSelect
UNSIGNEDLONG  l_RFCEvent,    l_SaveOpenChild, l_RefreshCmd
LONG          l_RowCount,    l_BeginRow,   l_EndRow
LONG          l_NewRow,      l_Idx
STRING        l_HorizScroll
TRIGEVENT     l_SaveRFC
UO_DW_MAIN    l_ToDW,        l_ChildDW

//----------
//  Let the developer know that we are in the RFC event.
//----------

i_InRFC = i_InRFC + 1

//----------
//  Initially assume that a pcd_Refresh to take of modifications
//  does not need to happen.
//----------

l_RefreshCmd = c_RefreshUndefined

//----------
//  If an event triggers this event and you want to bypass the
//  RowFocusChanged! code, then do the following:
//
//      Push_DW_RFC(c_IgnoreRFC)
//      <code the generates the RowFocusChanged! Event>
//      Pop_DW_RFC()
//
//  Do not modify the i_IgnoreRFC instance variable!
//----------

IF i_IgnoreRFC THEN
   GOTO Finished
END IF

//----------
//  Initialize PCCA.Error and remember the current row number.
//----------

PCCA.Error     = c_Success
i_OldCursorRow = i_CursorRow

//----------
//  Save the event type that we are processing into a local
//  variable and reset the instance variable.
//----------

l_RFCEvent = i_RFCEvent
i_RFCEvent = c_RFC_Undefined

//----------
//  i_IsEmpty may get reset during this event.  Remember the
//  original value, so that we will exit this event if it was
//  empty when the user originally did the RFC.
//----------

l_IsEmpty = i_IsEmpty

//----------
//  Assume no selections.
//----------

i_SelectionChanged      = FALSE
i_MultiSelectionChanged = FALSE
i_DrillingDown          = FALSE

//----------
//  The following section of code only needs to be done if we
//  were not triggered by pcd_Refresh.
//----------

IF PCCA.PCMGR.i_DW_InRefresh = 0 THEN

   i_DrillingDown = (i_AllowDrillDown AND &
                     i_ShowRow  > 0   AND &
                     l_RFCEvent = c_RFC_DoubleClicked)

   //----------
   //  Figure out if we are doing a multiple selection.  Mulitple
   //  selections are only possible if:
   //     a) We are not drilling down AND
   //     b) This DataWindow has been enabled for multiple
   //        selections AND
   //     c) The CONTROL or SHIFT keys are pressed.
   //----------

   IF NOT i_DrillingDown OR i_RFCType = c_RFC_DoubleClicked THEN

      //----------
      //  We need l_IsControl for unselecting rows, so always save
      //  it regardless of whether or not this DataWindow supports
      //  multiple selections.
      //----------

      l_IsControl = KeyDown(keyControl!)

      IF i_MultiSelect THEN

         //----------
         //  If the user is pressing "Shift-Tab" to move backwards,
         //  don't count it as a multiple selection.
         //----------

         l_Tab = KeyDown(keyTab!)
         IF NOT l_Tab THEN
            l_IsShifted = KeyDown(keyShift!)
            i_MultiSelectionChanged = (l_IsControl OR l_IsShifted)
         END IF
      END IF
   END IF

   //----------
   //  See if the arrow keys generated this event.
   //----------

   IF l_RFCEvent = c_RFC_Undefined THEN

      //----------
      //  If we were triggered by the arrow keys, PowerBuilder has
      //  already changed the row underneath us.  Update where we
      //  think the cursor row is.
      //----------

      l_RFCEvent  = c_RFC_RowFocusChanged
      i_CursorRow = GetRow()
   END IF

   //----------
   //  We need to see if we should call Load_Rows().  It should
   //  only be done if:
   //     a) There is a DataWindow that needs to have rows loaded
   //        into its children and
   //     b) This DataWindow is not the DataWindow that needs to
   //        be loaded.  If it is, then the user may be adding to
   //        their multi-selection and we don't want to load it
   //        until they are done.
   //----------

   IF PCCA.PCMGR.i_DW_NeedRetrieve           THEN
   IF PCCA.PCMGR.i_DW_NeedRetrieveDW <> THIS THEN

      //----------
      //  There is a DataWindow that needs to have its rows loaded
      //  into the children DataWindows.  Call Load_Rows() to do
      //  it.  If there is an error from load rows (e.g. retrieve
      //  error), then clean up and exit this event.
      //----------

      IF Load_Rows(PCCA.Null_Object, PCCA.Null_Object) <> &
         c_Success THEN
         PCCA.Error = c_Fatal
         GOTO Finished
      END IF
   END IF
   END IF
END IF

//----------
//  Save the event type for the developer.
//----------

CHOOSE CASE l_RFCEvent
   CASE c_RFC_RowFocusChanged
      i_SelectionType = RowFocusChanged!
   CASE c_RFC_Clicked
      i_SelectionType = Clicked!
   CASE c_RFC_DoubleClicked
      i_SelectionType = DoubleClicked!
   CASE ELSE
      i_SelectionType = RowFocusChanged!
END CHOOSE

//----------
//  We need to force l_RFCEvent to c_RFC_RowFocusChanged if the
//  requested select type is c_RFC_RowFocusChanged and l_RFCEvent
//  is not c_RFC_NoSelect (if l_RFCEvent is c_RFC_NoSelect, then
//  no selections should be done).
//----------

IF i_RFCType  =  c_RFC_RowFocusChanged THEN
IF l_RFCEvent <> c_RFC_NoSelect        THEN

   //----------
   //  Selections are to happen on RowFocusChanged!.  If we
   //  were triggered by a Clicked! or DoubleClicked! event,
   //  we need to convert the event type to RFC so that when
   //  we test later, we will determine that there is a new
   //  selection.
   //----------

   l_RFCEvent = c_RFC_RowFocusChanged
END IF
END IF

//----------
//  Other PowerClass event can force a selection by setting
//  i_MoveRow to a negative row number.  For now, we assume that
//  another event is not forcing us to do a new selection.
//----------

l_ForceSelect = FALSE

//----------
//  Determine the "clicked" row.  We will bail out if no new row
//  is selected.
//----------

IF i_MoveRow < 0 THEN

   //----------
   //  i_MoveRow is negative - we are being told to do new
   //  selection by another PowerClass event.  Set l_NewRow to
   //  the row number of the new selected row.
   //----------

   l_ForceSelect = TRUE
   l_NewRow      = - i_MoveRow
ELSE

   //----------
   //  If i_MoveRow is positive, we are being told by another
   //  PowerClass event to move to that row, but to not make it
   //  the current selected row.
   //----------

   IF i_MoveRow > 0 THEN
      l_NewRow  = i_MoveRow
   ELSE

      //----------
      //  i_MoveRow is 0, so we will just ask PowerBuilder what
      //  row we should move to.
      //----------

      l_NewRow = GetRow()
   END IF
END IF

//----------
//  If there is not a new row (i.e. l_NewRow is 0), or this
//  DataWindow is empty or not in use, then exit this event.
//----------

IF l_NewRow = 0 OR l_IsEmpty OR NOT i_InUse THEN
   GOTO Finished
END IF

//----------
//  We only need to do the following things if:
//     a) We are not drilling down OR
//     b) The SelectOn type is DoubleClicked!.
//----------

IF NOT i_DrillingDown OR i_RFCType = c_RFC_DoubleClicked THEN

   //----------
   //  We only need to do validation if we are not triggered by
   //  pcd_Refresh.
   //----------

   IF PCCA.PCMGR.i_DW_InRefresh = 0 THEN

      //----------
      //  Check to see if the user is unselecting the current row.
      //----------

      IF l_IsControl          THEN
      IF NOT i_MultiSelect    THEN
         i_SelectionChanged = TRUE
         l_UnSelect         = TRUE
      END IF
      END IF

      //----------
      //  Check validation only if:
      //     a) It is possible for the user to modify the data AND
      //     b) Validation is being processed.
      //----------

      IF i_CurrentMode <> c_ModeView THEN
      IF NOT i_IgnoreVal             THEN

         //----------
         //  Check column validation only if the row has not
         //  already been changed (i.e. the up arrow key will
         //  call SetRow(), which will have already generated
         //  validation checking).
         //----------
         //  Only do validate processing if the row is changing.
         //  If the column is changing, ItemChanged will be
         //  triggered.
         //----------

         IF i_CursorRow =  i_OldCursorRow THEN
         IF l_NewRow    <> i_OldCursorRow THEN

            //----------
            //  dw_AcceptText() is used to generate validation
            //  checking.
            //----------

            IF NOT dw_AcceptText(c_AcceptProcessErrors) THEN

               //----------
               //  Since there was a validation error, make sure
               //  that this DataWindow becomes the current
               //  DataWindow. Set PCCA.Error to indicate that
               //  an error has occurred and exit this event.
               //----------

               PCCA.Error = c_Fatal
               GOTO Finished
            END IF
         END IF
         END IF

         //----------
         //  If the row is going to change, trigger row validation
         //  processing.
         //----------

         IF i_OldCursorRow <> l_NewRow THEN

            //----------
            //  Set up i_RowNbr for the developer to use in
            //  pcd_ValidateRow.
            //----------

            i_RowNbr = i_OldCursorRow

            //----------
            //  Do any columns that need to be cross-validated
            //  (e.g. state with country)?  This event must be
            //  coded by the developer.
            //----------
            //  Validation routines are expected to do their own
            //  error display.  Therefore, i_DisplayError is not
            //  checked for developer-coded validation routines.
            //----------

            TriggerEvent("pcd_ValidateRow")

            IF PCCA.Error <> c_Success THEN
               GOTO Finished
            END IF
         END IF
      END IF
      END IF
   END IF

   //----------
   //  We need to change the selected row if:
   //     a) This is NOT a multiple selection AND
   //     b) 1) The l_RFCEvent matches the i_RFCType OR
   //        2) We are being forced to do a new selection.
   //----------

   IF NOT i_MultiSelectionChanged THEN
      IF i_RFCType = l_RFCEvent THEN
         i_SelectionChanged = TRUE
      ELSE
         IF l_ForceSelect THEN
            i_SelectionChanged = TRUE
         END IF
      END IF
   END IF

   //----------
   //  We don't need to update the screen if:
   //     a) We don't have a new selection AND
   //     b) This is not a multiple selection AND
   //----------

   IF NOT i_SelectionChanged      THEN
   IF NOT i_MultiSelectionChanged THEN

      //----------
      //  Move the cursor to the new row.
      //----------

      SetRedraw(c_BufferDraw)
      i_RedrawCount = i_RedrawCount + 1
      i_RedrawStack[i_RedrawCount] = c_BufferDraw

      i_IgnoreRFC      = (NOT c_IgnoreRFC)
      i_IgnoreRFCCount = i_IgnoreRFCCount + 1
      i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

      i_IgnoreVal      = (NOT c_IgnoreVal)
      i_IgnoreValCount = i_IgnoreValCount + 1
      i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

      i_IgnoreVScroll = (NOT c_IgnoreVScroll)
      i_IgnoreVSCount = i_IgnoreVSCount + 1
      i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

      l_HorizScroll = Describe("DataWindow.HorizontalScrollPosition")
      ScrollToRow(l_NewRow)
      Modify("DataWindow.HorizontalScrollPosition="+l_HorizScroll)

      IF i_IgnoreVSCount > 1 THEN
         i_IgnoreVSCount = i_IgnoreVSCount - 1
         i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
      ELSE
         i_IgnoreVSCount = 0
         i_IgnoreVScroll = FALSE
      END IF
      IF NOT i_IgnoreVScroll THEN
         i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
      END IF

      IF i_IgnoreValCount > 1 THEN
         i_IgnoreValCount = i_IgnoreValCount - 1
         i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
      ELSE
         i_IgnoreValCount = 0
         i_IgnoreVal      = FALSE
      END IF

      IF i_IgnoreRFCCount > 1 THEN
         i_IgnoreRFCCount = i_IgnoreRFCCount - 1
         i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
      ELSE
         i_IgnoreRFCCount = 0
         i_IgnoreRFC      = FALSE
      END IF

      IF i_RedrawCount > 1 THEN
         i_RedrawCount = i_RedrawCount - 1
         SetRedraw(i_RedrawStack[i_RedrawCount])
      ELSE
         i_RedrawCount = 0
         SetRedraw(TRUE)
      END IF

      //----------
      //  If we were not triggered by pcd_Refresh, we made need
      //  to update the scroll command buttons.  The scroll
      //  command buttons only need to be changed when the user
      //  moves to or from the first and last rows in the
      //  DataWindow.  For example, if the user moves to the row
      //  #1, then the previous command buttons needs to be
      //  disabled.  If the user moves from row #2 to row #3,
      //  there would be no change in the status of the scroll
      //  command buttons.
      //----------

      IF PCCA.PCMGR.i_DW_InRefresh = 0 THEN

         //----------
         //  Find the row count for this DataWindow and see if we
         //  are moving to or from the first or last rows.
         //----------

         l_RowCount = RowCount()
         IF i_OldCursorRow <= 1          OR &
            l_NewRow       <= 1          OR &
            i_OldCursorRow >= l_RowCount OR &
            l_NewRow       >= l_RowCount THEN

            //----------
            //  Update the display with the new cursor position
            //  since it looks funny if the scroll buttons change
            //  before the cursor has moved.
            //----------

            SetRedraw(c_DrawNow)
            i_RedrawCount = i_RedrawCount + 1
            i_RedrawStack[i_RedrawCount] = c_DrawNow

            IF i_RedrawCount > 1 THEN
               i_RedrawCount = i_RedrawCount - 1
               SetRedraw(i_RedrawStack[i_RedrawCount])
            ELSE
               i_RedrawCount = 0
               SetRedraw(TRUE)
            END IF

            //----------
            //  Make sure i_CursorRow is updated with the new row
            //  and trigger pcd_SetControl to take care of enabling
            //  and disabling the command buttons.
            //----------

            i_CursorRow                  = l_NewRow
            is_EventControl.Control_Mode = c_ControlRows
            TriggerEvent("pcd_SetControl")
         END IF
      END IF

      //----------
      //  Remember the current cursor row and exit the event as we
      //  are done.
      //----------

      i_CursorRow = l_NewRow
      GOTO Finished
   END IF
   END IF

   //----------
   //  See if the user selected the same row.  If they did, we
   //  won't consider it a new selection.
   //----------

   IF i_ShowRow = l_NewRow THEN
   IF i_SelectionChanged   THEN
   IF NOT l_Unselect AND i_CursorRow <> i_ShowRow THEN
      i_SelectionChanged = TRUE
   ELSE
      i_SelectionChanged = FALSE
   END IF
   END IF
   END IF

   //----------
   //  If this DataWindow has children that are Instance
   //  DataWindows and there is a new selection, then find
   //  the Instance DataWindow (if any) that is associated
   //  with the new row.  
   //----------

   IF i_NumInstance > 0  THEN
   IF i_SelectionChanged THEN
   IF NOT l_UnSelect     THEN
      Find_Instance(l_NewRow)

      //----------
      //  If there is already an instance DataWindow for this row
      //  and there are not any normal children, then indicate
      //  that selections are not changing.
      //----------

      IF i_CurInstance > 0 THEN
      IF i_NumChildren = 0 THEN
         i_SelectionChanged = FALSE
         i_AnchorRow        = l_NewRow
         i_ShowRow          = l_NewRow

         SelectRow(0, FALSE)

         //----------
         //  Before we select the new row, we need to make sure
         //  that there is actually a new row.  Also, we don't
         //  want to select the row if the developer said not
         //  to use PowerBuilder's SelectRow() function.  For
         //  example, in a free-form style DataWindow, only one
         //  row can be displayed and the selected row
         //  highlighting is ugly.  However, if this DataWindow
         //  allows multiple selections, we have to tell
         //  PowerBuilder to select the rows so the user can
         //  see which rows have or have not been selected.
         //----------

         IF i_ShowRow > 0 AND &
            (i_HighlightSelected OR i_MultiSelect) THEN
            SelectRow(i_ShowRow, TRUE)
         END IF
      END IF
      END IF
   END IF
   END IF
   END IF

   //----------
   //  If we have a tabular-form DataWindow, the focus rectangle
   //  will move to indicate the cursor row.  If we have a free-
   //  form DataWindow, the records in the DataWindow will scroll
   //  to the new cursor row.  Until we know for sure that we will
   //  be changing the cursor row, we don't want a scroll to happen
   //  in the free-form DataWindow.  Therefore, only move to the
   //  cursor if this is a tabular-form DataWindow.  We will move
   //  to the cursor row in a free-form DataWindow later in this
   //  event.
   //----------

   IF i_MultiRowDisplay THEN
      SetRedraw(c_BufferDraw)
      i_RedrawCount = i_RedrawCount + 1
      i_RedrawStack[i_RedrawCount] = c_BufferDraw

      i_IgnoreRFC      = (NOT c_IgnoreRFC)
      i_IgnoreRFCCount = i_IgnoreRFCCount + 1
      i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

      i_IgnoreVal      = (NOT c_IgnoreVal)
      i_IgnoreValCount = i_IgnoreValCount + 1
      i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

      i_IgnoreVScroll = (NOT c_IgnoreVScroll)
      i_IgnoreVSCount = i_IgnoreVSCount + 1
      i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

      l_HorizScroll = Describe("DataWindow.HorizontalScrollPosition")
      ScrollToRow(l_NewRow)
      Modify("DataWindow.HorizontalScrollPosition="+l_HorizScroll)

      IF i_IgnoreVSCount > 1 THEN
         i_IgnoreVSCount = i_IgnoreVSCount - 1
         i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
      ELSE
         i_IgnoreVSCount = 0
         i_IgnoreVScroll = FALSE
      END IF
      IF NOT i_IgnoreVScroll THEN
         i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
      END IF

      IF i_IgnoreValCount > 1 THEN
         i_IgnoreValCount = i_IgnoreValCount - 1
         i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
      ELSE
         i_IgnoreValCount = 0
         i_IgnoreVal      = FALSE
      END IF

      IF i_IgnoreRFCCount > 1 THEN
         i_IgnoreRFCCount = i_IgnoreRFCCount - 1
         i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
      ELSE
         i_IgnoreRFCCount = 0
         i_IgnoreRFC      = FALSE
      END IF

      IF i_RedrawCount > 1 THEN
         i_RedrawCount = i_RedrawCount - 1
         SetRedraw(i_RedrawStack[i_RedrawCount])
      ELSE
         i_RedrawCount = 0
         SetRedraw(TRUE)
      END IF
   END IF

   //----------
   //  If we have gotten this far in the event and we were not
   //  triggered by pcd_Refresh, then we know that a retrieve will
   //  happen if the selections are changing.  We must check for
   //  changes if this DW is not part of a share.
   //----------

   IF PCCA.PCMGR.i_DW_InRefresh = 0                 THEN
   IF i_SelectionChanged OR i_MultiSelectionChanged THEN
   IF i_NumShareSecondary = 0                       THEN

      //----------
      //  i_CurInstance has been set to point at the Instance
      //  DataWindow (if any) associated with the new cursor row.
      //  Tell Check_Save() to check that Instance DataWindow for
      //  changes as we will re-load the new cursor row into it.
      //----------

      is_EventControl.Check_Cur_Instance = TRUE

      //----------
      //  The RowFocusChanged! event only causes retrieves to
      //  happen in the children, so only they need to be checked
      //  for changes.
      //----------

      is_EventControl.Only_Do_Children = TRUE

      //----------
      //  Call Check_Save() to actually do the check for changes.
      //----------

      Check_Save(l_RefreshCmd)

      //----------
      //  If PCCA.Error does not indicate success, then the user
      //  canceled this operation or there was an error in saving.
      //  Restore everything to its original state and exit this
      //  event.
      //----------

      IF PCCA.Error <> c_Success THEN
         Goto Finished
      END IF
   END IF
   END IF
   END IF

   //----------
   //  We need to update.  Turn redraws off while we update the
   //  screen.
   //----------

   SetRedraw(c_BufferDraw)
   i_RedrawCount = i_RedrawCount + 1
   i_RedrawStack[i_RedrawCount] = c_BufferDraw

   //----------
   //  If this is a multi-select, we need to process it.
   //----------

   IF i_MultiSelectionChanged THEN

      //----------
      //  Since the user is changing their multi-selection,
      //  indicate that the children DataWindows will need
      //  to be retrieved.
      //----------

      i_RetrieveChildren = TRUE

      //----------
      //  See if the user did the multi-selection using the CONTROL
      //  key.
      //----------

      IF l_IsControl THEN

         //----------
         //  The CONTROL key toggles the cursor row as being
         //  selected or not selected.  If the new row is
         //  already selected, then we need to unselect it.
         //----------

         IF IsSelected(l_NewRow) THEN

            //----------
            //  Tell the DataWindow to unselect the row and clear
            //  the anchor position.
            //----------

            SelectRow(l_NewRow, FALSE)
            i_AnchorRow = 0

            //----------
            //  If the user unselected the current row, try to
            //  find another row.
            //----------

            IF i_ShowRow = l_NewRow THEN
               i_ShowRow = GetSelectedRow(0)
            END IF
         ELSE

            //----------
            //  The user selected another row: tell the DataWindow
            //  to add it to the list.
            //----------

            SelectRow(l_NewRow, TRUE)
            i_AnchorRow = l_NewRow

            //----------
            //  If there is not a current row, then this is a new
            //  selection.  Marking i_SelectionChanged as TRUE,
            //  will cause the anchor row to get set to this new
            //  row.
            //----------

            IF i_ShowRow = 0 THEN
               i_SelectionChanged = TRUE
            END IF
         END IF
      END IF

      //----------
      //  See if the user did the multi-selection using the SHIFT
      //  key.
      //----------

      IF l_IsShifted THEN

         //----------
         //  Make sure that there is an anchor row.
         //----------

         IF i_AnchorRow > 0 THEN

            //----------
            //  The new row may be before or after the anchor row.
            //  Find which one is first and which one is last.
            //----------

            IF l_NewRow >= i_AnchorRow THEN
               l_BeginRow = i_AnchorRow
               l_EndRow   = l_NewRow
            ELSE
               l_EndRow   = i_AnchorRow
               l_BeginRow = l_NewRow
            END IF

            //----------
            //  Generally, we don't set the hourglass in this event.
            //  However, if there are a lot of rows, we better tell
            //  the user that it is going to be a while.
            //----------

            IF l_EndRow - l_BeginRow > 25 THEN
               SetPointer(HourGlass!)
            END IF

            //----------
            //  Clear all of the current selections.
            //----------

            SelectRow(0, FALSE)

            //----------
            //  Set through all rows between the anchor row and the
            //  new row and add them to the selected list.
            //----------

            FOR l_Idx = l_BeginRow TO l_EndRow
               SelectRow(l_Idx, TRUE)
            NEXT
         ELSE

            //----------
            //  If we got here, we did not have an anchor row.
            //  Just clear all of the selections, select the
            //  new row.  Marking i_SelectionChanged as TRUE,
            //  will cause the anchor row to get set to this
            //  new row.
            //----------

            SelectRow(0,        FALSE)
            SelectRow(l_NewRow, TRUE)
            i_SelectionChanged = TRUE
         END IF
      END IF
   END IF

   //----------
   //  If this is a new selection, update the current row and
   //  anchor row and display.  Note that i_CurInstance has
   //  already been updated.
   //----------

   IF i_SelectionChanged THEN

      i_AnchorRow = l_NewRow

      IF l_UnSelect THEN
         i_ShowRow = 0
      ELSE
         i_ShowRow = l_NewRow
      END IF

      //----------
      //  Since we have a new selection, it will need to be
      //  retrieved into the children DataWindows.
      //----------

      i_RetrieveChildren = TRUE

      //----------
      //  If this is a multi-selection, then PowerBuilder has
      //  already been told what rows are selected.  If it this
      //  is a single selection, then clear all of the current
      //  selections and select the new row.
      //----------

      IF NOT i_MultiSelectionChanged THEN
         SelectRow(0, FALSE)

         //----------
         //  Before we select the new row, we need to make sure
         //  that there is actually a new row.  Also, we don't
         //  want to select the row if the developer said not
         //  to use PowerBuilder's SelectRow() function.  For
         //  example, in a free-form style DataWindow, only one
         //  row can be displayed and the selected row
         //  highlighting is ugly.  However, if this DataWindow
         //  allows multiple selections, we have to tell
         //  PowerBuilder to select the rows so the user can
         //  see which rows have or have not been selected.
         //----------

         IF i_ShowRow > 0 AND &
            (i_HighlightSelected OR i_MultiSelect) THEN
            SelectRow(i_ShowRow, TRUE)
         END IF
      ELSE

         //----------
         //  If i_MultiSelection is TRUE, then return
         //  i_SelectionChanged to FALSE.
         //----------

         i_SelectionChanged = FALSE
      END IF
   END IF

   //----------
   //  Set up i_SelectedRows[].
   //----------

   i_NumSelected = Get_Retrieve_Rows(i_SelectedRows[])

   //----------
   //  If this is not a tabular-form DataWindow, we have not yet
   //  moved the cursor (i.e. scrolled to the cursor row).  Do it
   //  now.
   //----------

   IF NOT i_MultiRowDisplay THEN
      i_IgnoreRFC      = (NOT c_IgnoreRFC)
      i_IgnoreRFCCount = i_IgnoreRFCCount + 1
      i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

      i_IgnoreVal      = (NOT c_IgnoreVal)
      i_IgnoreValCount = i_IgnoreValCount + 1
      i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

      i_IgnoreVScroll = (NOT c_IgnoreVScroll)
      i_IgnoreVSCount = i_IgnoreVSCount + 1
      i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

      l_HorizScroll = Describe("DataWindow.HorizontalScrollPosition")
      ScrollToRow(l_NewRow)
      Modify("DataWindow.HorizontalScrollPosition="+l_HorizScroll)

      IF i_IgnoreVSCount > 1 THEN
         i_IgnoreVSCount = i_IgnoreVSCount - 1
         i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
      ELSE
         i_IgnoreVSCount = 0
         i_IgnoreVScroll = FALSE
      END IF
      IF NOT i_IgnoreVScroll THEN
         i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
      END IF

      IF i_IgnoreValCount > 1 THEN
         i_IgnoreValCount = i_IgnoreValCount - 1
         i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
      ELSE
         i_IgnoreValCount = 0
         i_IgnoreVal      = FALSE
      END IF

      IF i_IgnoreRFCCount > 1 THEN
         i_IgnoreRFCCount = i_IgnoreRFCCount - 1
         i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
      ELSE
         i_IgnoreRFCCount = 0
         i_IgnoreRFC      = FALSE
      END IF
   END IF

   //----------
   //  Remember the new cursor row.
   //----------

   i_CursorRow = l_NewRow

   //----------
   //  Restore redraw to the previous state.
   //----------

   IF i_RedrawCount > 1 THEN
      i_RedrawCount = i_RedrawCount - 1
      SetRedraw(i_RedrawStack[i_RedrawCount])
   ELSE
      i_RedrawCount = 0
      SetRedraw(TRUE)
   END IF
END IF

//----------
//  If pcd_Refresh is already running, we are done.
//----------

IF PCCA.PCMGR.i_DW_InRefresh > 0 THEN
   GOTO Finished
END IF

//----------
//  Since this event may be re-entered (e.g. this event
//  triggers pcd_Refresh, which in turn trigger this event),
//  we must save the developer's return variables so they
//  don't get clobbered if and when this event gets re-entered.
//----------

l_SaveRFC         = i_SelectionType
l_SaveSelect      = i_SelectionChanged
l_SaveMultiSelect = i_MultiSelectionChanged
l_SaveDrill       = i_DrillingDown

//----------
//  We don't want any rows being reselected during the refresh.
//----------

i_NumReselectRows = 0

//----------
//  If l_RefreshCmd contains a valid refresh mode, then
//  we must refresh the DataWindow hierarchy.  For example,
//  this occurs when the user hits "No" when asked if they
//  want to save changes.  The modified DataWindow(s) must be
//  retrieved to clear the changes the user has made.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN

   //----------
   //  Set up the Event Control structure for pcd_Refresh.
   //  Refresh_Cmd tells pcd_Refresh how to refresh the
   //  DataWindow hierarchies and it set to the refresh mode
   //  determined by Check_Save().  We also tell pcd_Refresh
   //  to skip the children DataWindow of this DataWindow
   //  we will handle them ourselves in a little bit.
   //----------

   i_RootDW.is_EventControl.Refresh_Cmd            = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Children_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW_Children       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")

   //----------
   //  Restore the values of the developer's return variables
   //  that may have gotten clobbered.
   //----------

   i_SelectionType         = l_SaveRFC
   i_SelectionChanged      = l_SaveSelect
   i_MultiSelectionChanged = l_SaveMultiSelect
   i_DrillingDown          = l_SaveDrill
END IF

IF PCCA.Error = c_Success THEN

   //----------
   //  We have a new single-selection or multi-selection.  We
   //  need to trigger pcd_Refresh to either refresh or clear
   //  the children DataWindows for us.  We need to set up the
   //  Event Control structure to tell pcd_Refresh how to do
   //  it.  First, we tell it that we need the refresh done in
   //  RFC mode and that only children of this DataWindow need
   //  to be refreshed.
   //----------

   IF NOT i_DrillingDown OR i_RFCType = c_RFC_DoubleClicked THEN
      is_EventControl.Refresh_Cmd = c_RefreshRFC
   ELSE
      is_EventControl.Refresh_Cmd = c_RefreshDrillDown
   END IF
   is_EventControl.Only_Do_Children = TRUE

   //----------
   //  If DrillDown is specified, make sure that the child
   //  DataWindows don't open.
   //----------

   l_SaveOpenChild = i_OpenChildOption

   IF i_AllowDrillDown AND NOT i_DrillingDown THEN
      i_OpenChildOption = c_OpenChildPrevent
   END IF

   //----------
   //  We now have to tell pcd_Refresh to actually retrieve the
   //  children DataWindows or just reset them until the user
   //  indicates that they want the rows loaded.  i_RefreshWhen
   //  tells us if the retrieve should happen after every
   //  selection, multi-selection, or only when the user
   //  selects a different DataWindow.
   //----------

   IF NOT i_AllowDrillDown OR NOT i_DrillingDown THEN
      IF i_SelectionChanged THEN
         is_EventControl.Reset = &
            (i_RefreshWhen = c_RFC_NoAutoRefresh)
      END IF

      IF i_MultiSelectionChanged THEN
         is_EventControl.Reset = &
            (i_RefreshWhen <> c_RFC_RefreshOnMultiSelect)
      END IF
   END IF

   //----------
   //  If we are telling pcd_Refresh to NOT load the rows now,
   //  then we need to remember that this DataWindow's
   //  selections have not been retrieved into its children
   //  DataWindows.
   //----------

   IF is_EventControl.Reset AND i_ShowRow > 0 THEN
      PCCA.PCMGR.i_DW_NeedRetrieve   = TRUE
      PCCA.PCMGR.i_DW_NeedRetrieveDW = THIS
   END IF

   //----------
   //  Trigger pcd_Refresh to handle the new selection(s).
   //----------

   TriggerEvent("pcd_Refresh")

   //----------
   //  Restore the values of the developer's return variables
   //  that may have gotten clobbered.
   //----------

   i_OpenChildOption = l_SaveOpenChild
   i_SelectionType         = l_SaveRFC
   i_SelectionChanged      = l_SaveSelect
   i_MultiSelectionChanged = l_SaveMultiSelect
   i_DrillingDown          = l_SaveDrill
END IF

//----------
//  Determine if we should change focus.  Don't change focus
//  if:
//     a) There are no rows selected.
//     b) If focus has already been taken care of (i.e.
//        PCCA.PCMGR.i_DW_FocusDone is TRUE).
//     c) We're being triggered by another event (i.e.
//        l_ForceSelect is TRUE, then let the triggering
//        event handle the focus (e.g. pcd_First)).
//     d) There was an error on the refresh.
//     e) We are DrillDown and the event type is not
//        DoubleClicked!
//     f) The children were not really retrieved because
//        i_RefreshWhen did not allow the retrieval.
//        (Note: i_RetrieveChildren will be reset to FALSE
//        if pcd_Refresh actually retrieved data into the
//        children.  If pcd_Refresh did not retrieve the data
//        into the children then i_RetrieveChildren will
//        not be reset.
//     g) If we are not DrillDown and selections are be
//        changed by RowFocusChanged! (i.e. if the user is
//        using the arrow keys, they don't expect the focus
//        to jump to another window).
//     h) If we are not DrillDown and the child we are going
//        to change focus to is on the same window as us, don't
//        do it.  If it is on a different window, that it is Ok
//        because the window will be raised and it will be
//        (more) obvious that focus has changed.
//----------

l_MoveFocus = FALSE

IF i_ShowRow > 0                 THEN
IF NOT PCCA.PCMGR.i_DW_FocusDone THEN
IF NOT l_ForceSelect             THEN
IF PCCA.Error = c_Success        THEN
   IF i_AllowDrillDown THEN
      l_MoveFocus = i_DrillingDown
   ELSE
      IF NOT i_RetrieveChildren             THEN
      IF i_RFCType <> c_RFC_RowFocusChanged THEN
         l_MoveFocus = TRUE
      END IF
      END IF
   END IF
END IF
END IF
END IF
END IF

IF l_MoveFocus THEN

   //----------
   //  If the developer specified a preferred-focus DataWindow,
   //  we want to set focus to it.
   //----------

   l_SetFocus = FALSE
   IF IsValid(i_FocusDW) THEN
      IF i_FocusDW.i_InUse THEN
         l_SetFocus = TRUE
         l_ToDW     = i_FocusDW
      END IF
   END IF

   IF NOT l_SetFocus THEN

      //----------
      //  If Instance DataWindows are being used, we want to
      //  set focus to the Instance DataWindow that is
      //  associated with with this row.  Otherwise, just set
      //  focus to our last child.
      //----------

      IF i_CurInstance > 0 THEN
         l_ToDW     = i_InstanceDW[i_CurInstance]
         l_SetFocus = TRUE
      END IF

      IF NOT l_SetFocus THEN

         //----------
         //  Find the last child DataWindow created that is
         //  in use.  If it is not on the same window, then
         //  set focus to it.
         //----------

         IF i_NumChildren > 0 THEN
            l_ChildDW = i_ChildDW[i_NumChildren]
            IF l_ChildDW.i_Window <> i_Window THEN
               l_SetFocus = TRUE
               l_ToDW     = l_ChildDW
            END IF
         END IF
      END IF
   END IF

   //----------
   //  If we found a DataWindow to make current, do it.
   //----------

   IF l_SetFocus THEN
      Set_Auto_Focus(l_ToDW)
   END IF
END IF

Finished:

//----------
//  If PCCA.Error does not indicate success, then there has been
//  an error.  If it was in this event, try to restore everything
//  to its original state.  However, we only want to do this only
//  if we are not ignoring RFC and if we were not triggered by
//  pcd_Refresh.
//----------

IF PCCA.Error <> c_Success       THEN
IF NOT i_IgnoreRFC               THEN
IF PCCA.PCMGR.i_DW_InRefresh = 0 THEN

   //----------
   //  If we were triggered by the DoubleClicked! event, we
   //  want to restore the cursor row to the row that was
   //  current before the Clicked! event.  If this event was
   //  triggered by a RowFocusChanged! or Clicked! event,
   //  then just restore the cursor to what it was before
   //  this event was entered.
   //----------

   IF l_RFCEvent = c_RFC_DoubleClicked THEN
      l_NewRow = i_OldClickedRow
   ELSE
      l_NewRow = i_OldCursorRow
   END IF

   //----------
   //  Turn RFC processing off and scroll to the prior cursor
   //  row.
   //----------

   SetRedraw(c_BufferDraw)
   i_RedrawCount = i_RedrawCount + 1
   i_RedrawStack[i_RedrawCount] = c_BufferDraw

   i_IgnoreRFC      = (NOT c_IgnoreRFC)
   i_IgnoreRFCCount = i_IgnoreRFCCount + 1
   i_IgnoreRFCStack[i_IgnoreRFCCount] = c_IgnoreRFC

   i_IgnoreVScroll = (NOT c_IgnoreVScroll)
   i_IgnoreVSCount = i_IgnoreVSCount + 1
   i_IgnoreVSStack[i_IgnoreVSCount] = c_IgnoreVScroll

   i_IgnoreVal      = (NOT c_IgnoreVal)
   i_IgnoreValCount = i_IgnoreValCount + 1
   i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

   l_HorizScroll = Describe("DataWindow.HorizontalScrollPosition")
   ScrollToRow(l_NewRow)
   l_HorizScroll = Describe("DataWindow.HorizontalScrollPosition")

   IF i_IgnoreValCount > 1 THEN
      i_IgnoreValCount = i_IgnoreValCount - 1
      i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
   ELSE
      i_IgnoreValCount = 0
      i_IgnoreVal      = FALSE
   END IF

   IF i_IgnoreVSCount > 1 THEN
      i_IgnoreVSCount = i_IgnoreVSCount - 1
      i_IgnoreVScroll = (NOT i_IgnoreVSStack[i_IgnoreVSCount])
   ELSE
      i_IgnoreVSCount = 0
      i_IgnoreVScroll = FALSE
   END IF
   IF NOT i_IgnoreVScroll THEN
      i_DWTopRow = Long(Describe("DataWindow.FirstRowOnPage"))
   END IF

   IF i_IgnoreRFCCount > 1 THEN
      i_IgnoreRFCCount = i_IgnoreRFCCount - 1
      i_IgnoreRFC      = (NOT i_IgnoreRFCStack[i_IgnoreRFCCount])
   ELSE
      i_IgnoreRFCCount = 0
      i_IgnoreRFC      = FALSE
   END IF

   IF i_RedrawCount > 1 THEN
      i_RedrawCount = i_RedrawCount - 1
      SetRedraw(i_RedrawStack[i_RedrawCount])
   ELSE
      i_RedrawCount = 0
      SetRedraw(TRUE)
   END IF

   i_CursorRow = l_NewRow

   //----------
   //  Restore the pointer the Instance DataWindow.
   //----------

   Find_Instance(i_CursorRow)

   //----------
   //  If l_RefreshCmd has been set to a valid refresh mode,
   //  we need to refresh the DataWindow hierarchy.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      i_RootDW.is_EventControl.Refresh_Cmd = l_RefreshCmd
      i_RootDW.TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Indicate that there are not any new selections and
   //  that an error has occurred.
   //----------

   i_SelectionChanged      = FALSE
   i_MultiSelectionChanged = FALSE
   i_DrillingDown          = FALSE

   //----------
   //  Make sure PCCA.Error is set to fatal.
   //----------

   PCCA.Error = c_Fatal
END IF
END IF
END IF

//----------
//  Make sure i_MoveRow is reset.
//----------

i_MoveRow = 0

//----------
//  All done in the RFC event.
//----------

IF i_InRFC > 1 THEN
   i_InRFC = i_InRFC - 1
ELSE
   i_InRFC = 0
END IF

//----------
//  Set the flag to let the developer know if they should execute
//  their extended code at this time.
//----------

i_ExtendMode = (NOT i_IgnoreRFC AND i_InUse)
end on

event itemchanged;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : ItemChanged
//  Description   : This event is used to trigger validation
//                  checking when a field has changed.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_SetActionCode

//----------
//  Occasionally you might initialize fields with values.  This
//  may cause this event to trigger.  If you don't want to do
//  validation checking, then do the following:
//
//      Push_DW_Validate(c_IgnoreVal)
//      <code the generates the ItemChanged! Event>
//      Pop_DW_Validate()
//
//  Do not modify the i_IgnoreVal instance variable!
//----------

IF i_IgnoreVal THEN
   GOTO Finished
END IF

//----------
//  If validation is to be done, find the current row and column
//  that needs to be validated.
//----------

i_RowNbr = GetRow()
i_ColNbr = GetColumn()

//----------
//  This event may be triggered by child DataWindows, in which case
//  column number may be 0.  We also check row number just to be
//  safe.  Note that we are talking about child DataWindows in the
//  PowerBuilder sense, not the PowerClass sense.
//----------

IF i_RowNbr = 0 OR i_ColNbr = 0 THEN
   GOTO Finished
END IF

//----------
//  Get the validation information ready that can be used by
//  the developer to code the pcd_ValidateCol event.
//----------

Get_Val_Info(i_ColNbr)

//----------
//  Initialize PCCA.Error to indicate that validation processing
//  has not been done.  Initialize i_DisplayRequired to indicate
//  that required field errors should be reported.  If the
//  developer does not want PowerClass to do required field
//  reporting, they can set this flag to FALSE.
//----------

PCCA.Error        = c_ValNotProcessed
i_DisplayRequired = TRUE

//----------
//  pcd_ValidateCol performs validation on column by column basis.
//  This event must be coded by the developer.
//----------
//  Validation routines are expected to do their own error
//  display.  Therefore, i_DisplayError is not checked for
//  developer-coded validation routines.
//----------

TriggerEvent("pcd_ValidateCol")

//----------
//  We need to display the required field error if:
//     a) If the developer wanted required field checking
//        all of the time (versus just during update) AND
//     b) If we have a required field error AND
//     c) The developer's routine did not catch or process
//        it AND
//     d) The developer said it was Ok to display it.
//----------

IF i_AlwaysCheckRequired            THEN
IF i_ColRequiredError               THEN
IF (PCCA.Error = c_ValOk            OR &
    PCCA.Error = c_ValNotProcessed) THEN
IF i_DisplayRequired                THEN

   //----------
   //  Use Display_DW_Val_Error() to display the required
   //  field error.
   //----------

   Display_DW_Val_Error(PCCA.MB.c_MBI_DW_RequiredField, "")

   //----------
   //  Since this is a required field, we have an error.
   //----------

   PCCA.Error = c_ValFailed
END IF
END IF
END IF
END IF

//----------
//  Indictate that the DataWindow has been through validation.
//  Since ItemError! may be called immediately, i_ItemValidated
//  tells PowerClass not to re-try the validation process (which
//  would display another validation error message box).
//----------

i_ItemValidated = TRUE

Finished:

//----------
//  If we were not triggered by ItemError and the developer did
//  not process the error, then indicate success.
//----------

IF i_InItemError = 0                 THEN
IF PCCA.Error    = c_ValNotProcessed THEN
   PCCA.Error = c_ValOk
END IF
END IF

//----------
//  The validation return code is interpreted as follows:
//     c_ValOk (0):
//        The the data was accepted.
//     c_ValFailed (1):
//        The data should be rejected and focus should be returned
//        to the field in error.
//     c_ValFixed (2):
//        The text was reformatted and placed back into the field
//        using SetItem().  The text now passes validation.
//     c_ValNotProcessed (-9):
//        The field was not validated by the developer's
//        pcd_ValidateCol event.  Let PowerBuilder handle the
//        validation.
//----------

l_SetActionCode = PCCA.Error
IF l_SetActionCode = c_ValNotProcessed THEN
   l_SetActionCode = c_ValOk
END IF

//----------
//  Set the flag to let the developer know if they should execute
//  their extended code at this time.
//----------

i_ExtendMode = (NOT i_IgnoreVal AND i_InUse)

//EnMe 17-02-2009
//SetActionCode(l_SetActionCode)
return l_SetActionCode
end event

on editchanged;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : EditChanged
//  Description   : This event is used to indicate when a change
//                  has been made to a field in the DataWindow and
//                  validation needs to be performed.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  This event may be triggered by child DataWindows, in which
//  case we need to ignore this event.  Note that child
//  DataWindow is used in the PowerBuilder sense, not the
//  PowerClass sense.
//----------

IF i_IgnoreVal THEN
   GOTO Finished
END IF

//----------
//  This flag indicates that the DataWindow has been modified.
//----------

IF i_CurrentMode <> c_ModeView THEN
   IF NOT i_Modified THEN
      Share_Flags(c_ShareModified, TRUE)
   END IF
END IF

Finished:

//----------
//  Set the flag to let the developer know if they should execute
//  their extended code at this time.
//----------

i_ExtendMode = (NOT i_IgnoreVal AND i_InUse)
end on

on itemfocuschanged;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : ItemFocusChanged
//  Description   : This event is used to indicate when a focus
//                  changes between items in a DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  If validation processing is off, we do not want to do
//  anything.
//----------

IF i_IgnoreVal THEN
   GOTO Finished
END IF

//----------
//  Clear the validation state flag indicating
//  the validation needs to be run.
//----------

i_ItemValidated = FALSE

Finished:

//----------
//  Set the flag to let the developer know if they should execute
//  their extended code at this time.
//----------

i_ExtendMode = (NOT i_IgnoreVal AND i_InUse)
end on

event doubleclicked;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : DoubleClicked
//  Description   : Select the doubleclicked on row and trigger
//                  the RowFocusChanged! event for row processing.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_IsShifted,  l_IsControl, l_NeedRFC
INTEGER  l_ClickedCol
LONG     l_ClickedRow
STRING   l_ComputeName, l_ColType

//----------
//  PCCA.Error will have been set by the Clicked! event.  If the
//  Clicked! event failed, then the DoubleClicked! event will fail.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  Remember if the control or  key is depressed.
//----------

l_IsControl = KeyDown(keyControl!)
l_IsShifted = KeyDown(keyShift!)

//----------
//  Get the row that was just double-clicked on.
//----------

l_ClickedRow = GetClickedRow()

//----------
//  Make sure that the user double-clicked on a valid row.  For
//  example, if they double-click on the border or scroll bar, the
//  double-clicked row would be invalid (i.e. 0).
//----------

IF l_ClickedRow > 0 THEN

   //----------
   //  Try to avoid an RFC event when the user is just
   //  double-clicking on a field.
   //----------

   l_ClickedCol = GetClickedColumn()

   //----------
   //  If clicked column is 0, check to see if a computed column
   //  was clicked on.
   //----------

   IF l_ClickedCol = 0 THEN
      l_ComputeName = GetObjectAtPointer()
      l_ComputeName = Left(l_ComputeName, Pos(l_ComputeName, "~t") - 1)
      l_ColType = Upper(Describe(l_ComputeName + ".type"))
   ELSE
      l_ColType = ""
   END IF

   IF l_ClickedCol > 0 OR l_ColType = "COMPUTE" THEN

      //----------
      //  Assume that we don't need the RFC until proven otherwise.
      //----------

      l_NeedRFC = FALSE

      //----------
      //  If this row is not selected and the clicked event
      //  will cause it to become selected, then we need the
      //  RFC event.
      //----------

      IF l_ClickedRow <> i_ShowRow OR i_AllowDrillDown THEN
         l_NeedRFC = TRUE
      END IF

      //----------
      //  If we still haven't proven that we need to RFC, we need
      //  to see if clicking might cause children DataWindows to
      //  open.  If so, we need to trigger the RFC event so that
      //  pcd_Refresh event (and the pcd_PickedRow event) will
      //  get triggered to open the children.
      //----------

      IF NOT l_NeedRFC THEN

         //----------
         //  If the user has the  or control key down, always
         //  do the RFC.
         //----------

         IF l_IsControl OR l_IsShifted THEN
            l_NeedRFC = TRUE
         ELSE

            //----------
            //  If the developer told us that there were not
            //  children, then we don't have to guess about it.
            //----------

            IF i_OpenChildOption <> c_OpenChildAlwaysPrevent THEN
            IF i_OpenChildOption <> c_OpenChildPrevent       THEN

               //----------
               //  If the developer said he always wanted the
               //  children to be opened, do it.
               //----------

               IF i_OpenChildOption = c_OpenChildAlwaysForce THEN
                  l_NeedRFC = TRUE
               ELSE
                  IF i_OpenChildOption = c_OpenChildForce THEN
                     l_NeedRFC = TRUE
                  ELSE

                     //----------
                     //  Now, we guess whether there are
                     //  children.  If we haven't done a
                     //  picked row yet, then we have no
                     //  idea if there are children or not.
                     //----------

                     IF NOT i_DidPickedRow THEN
                        IF l_ClickedRow <> i_CursorRow THEN
                           l_NeedRFC = TRUE
                        END IF
                     ELSE
                        IF i_NumChildren < i_MaxChildren THEN
                           l_NeedRFC = TRUE
                        ELSE
                           IF i_HaveInstances THEN
                              l_NeedRFC = TRUE
                           END IF
                        END IF
                     END IF
                  END IF
               END IF
            END IF
            END IF
         END IF
      END IF
   ELSE

      //----------
      //  If the user is clicking somewhere on the DataWindow
      //  other than a column, we always do the RFC event.
      //----------

      IF l_ClickedRow <> i_CursorRow THEN
         l_NeedRFC = TRUE
      END IF
   END IF

   IF l_NeedRFC THEN

      //----------
      //  The RowFocusChanged! event handles scrolling,
      //  validation, saving, and refreshing of the children
      //  DataWindows.  All we have to do is set i_MoveRow
      //  to tell the RowFocusChanged! event what row we want
      //  to move to.
      //----------

      i_RFCEvent = c_RFC_DoubleClicked
      i_MoveRow  = l_ClickedRow
      TriggerEvent(RowFocusChanged!)
   END IF
END IF

Finished:

//----------
//  If there was an error during the DoubleClicked! event, we do
//  not want the row and/or column changing.  Tell PowerBuilder
//  to discontinue processing.
//----------

i_ExtendMode = (PCCA.Error = c_Success AND i_InUse)

IF NOT i_ExtendMode THEN
	//EnMe 17-02-2009
	//SetActionCode(c_NoProcessClicked)
	RETURN c_NoProcessClicked
END IF
end event

event clicked;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : Clicked
//  Description   : Get the row that was clicked on and trigger the
//                  RowFocusChanged! event for processing.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_IsShifted,  l_IsControl, l_NeedRFC, l_WillSelect
INTEGER  l_ClickedCol, l_ColNbr
LONG     l_ClickedRow
STRING   l_ComputeName, l_ColType

//----------
//  Assume that we will not have any errors.
//----------

PCCA.Error = c_Success

//----------
//  Remember if the control or  key is depressed.
//----------

l_IsControl = KeyDown(keyControl!)
l_IsShifted = KeyDown(keyShift!)

//----------
//  If PCCA.PCMGR.i_ProcessingActivate is TRUE, then the
//  current DataWindow may not have been set to the correct
//  DataWindow on this window.  However, since the Clicked!
//  event changes the current DataWindow, we can handle it
//  here.  All we have to do is de-fuse activate processing
//  and make sure that the GetFocus event is run on this
//  DataWindow.
//----------

IF PCCA.PCMGR.i_ProcessingActivate THEN
   PCCA.PCMGR.Set_DW_Deactivate()
   TriggerEvent("GetFocus")
END IF

//----------
//  Get the row that was just clicked on.
//----------

l_ClickedRow = GetClickedRow()

//----------
//  Make sure that the user clicked on a valid row.  For example,
//  if they click on the border or scroll bar, the clicked row
//  would be invalid (i.e. 0).
//----------

IF l_ClickedRow > 0 THEN

   //----------
   //  Get the new column that has been clicked.
   //----------

   l_ClickedCol = GetClickedColumn()

   //----------
   //  If clicked column is 0, check to see if a computed column
   //  was clicked on.
   //----------

   IF l_ClickedCol = 0 THEN
      l_ComputeName = GetObjectAtPointer()
      l_ComputeName = Left(l_ComputeName, Pos(l_ComputeName, "~t") - 1)
      l_ColType = Upper(Describe(l_ComputeName + ".type"))
   ELSE
      l_ColType = ""
   END IF

   IF l_ClickedCol > 0 OR l_ColType = "COMPUTE" THEN

      l_ColNbr  = GetColumn()

      IF l_ClickedCol <> l_ColNbr THEN

         //----------
         //  dw_AcceptText() is used to generate validation
         //  checking.
         //----------

         IF NOT dw_AcceptText(c_AcceptProcessErrors) THEN

            //----------
            //  Since there was a validation error, make sure
            //  that this DataWindow becomes the current
            //  DataWindow. Set PCCA.Error to indicate that
            //  an error has occurred and exit this event.
            //----------

            PCCA.Error = c_Fatal
            GOTO Finished
         END IF
      END IF

      //----------
      //  Assume that we don't need the RFC until proven otherwise.
      //----------

      l_NeedRFC = FALSE

      //----------
      //  If the user clicked to a different row, we definitely
      //  need the RFC event.
      //----------

      IF l_ClickedRow <> i_CursorRow THEN
         l_NeedRFC = TRUE
      ELSE

         //----------
         //  See if clicking on a row will cause it to become
         //  selected.
         //----------

         l_WillSelect = (i_RFCType <> c_RFC_DoubleClicked)

         //----------
         //  If this row is not selected and the clicked event
         //  will cause it to become selected, then we need the
         //  RFC event.
         //----------

         IF l_ClickedRow <> i_ShowRow THEN
            l_NeedRFC = l_WillSelect
         END IF
      END IF

      //----------
      //  If we still haven't proven that we need to RFC, we need
      //  to see if clicking might cause children DataWindows to
      //  open.  If so, we need to trigger the RFC event so that
      //  pcd_Refresh event (and the pcd_PickedRow event) will
      //  get triggered to open the children.
      //----------

      IF NOT l_NeedRFC THEN

         //----------
         //  If the user has the shift or control key down, always
         //  do the RFC.
         //----------

         IF l_IsControl OR l_IsShifted THEN
            l_NeedRFC = TRUE
         ELSE

            //----------
            //  If clicking will do a select, then we have to worry
            //  about children being opened.
            //----------

            IF l_WillSelect THEN

               //----------
               //  If the developer told us that there were not
               //  children, then we don't have to guess about it.
               //----------

               IF i_OpenChildOption <> c_OpenChildAlwaysPrevent THEN
               IF i_OpenChildOption <> c_OpenChildPrevent       THEN

                  //----------
                  //  If the developer said he always wanted the
                  //  children to be opened, do it.
                  //----------

                  IF i_OpenChildOption = c_OpenChildAlwaysForce THEN
                     l_NeedRFC = TRUE
                  ELSE
                     IF i_OpenChildOption = c_OpenChildForce THEN
                        l_NeedRFC = TRUE
                     ELSE

                        //----------
                        //  Now, we guess whether there are
                        //  children.  If we haven't done a
                        //  picked row yet, then we have no
                        //  idea if there are children or not.
                        //----------

                        IF NOT i_DidPickedRow THEN
                           IF l_ClickedRow <> i_CursorRow THEN
                              l_NeedRFC = TRUE
                           END IF
                        ELSE
                           IF i_NumChildren < i_MaxChildren THEN
                              l_NeedRFC = TRUE
                           ELSE
                              IF i_HaveInstances THEN
                                 l_NeedRFC = TRUE
                              END IF
                           END IF
                        END IF
                     END IF
                  END IF
               END IF
               END IF
            END IF
         END IF
      END IF
   ELSE

      //----------
      //  If the user is clicking somewhere on the DataWindow
      //  other than a column, we always do the RFC event.
      //----------

      IF l_ClickedRow <> i_CursorRow THEN
         l_NeedRFC = TRUE
      END IF
   END IF

   //----------
   //  Remember the cursor row.  This is used when the user
   //  cancels a DoubleClicked! event - the cursor can be set
   //  back to the orignal row before they started clicking.
   //----------

   i_OldClickedRow = i_CursorRow

   IF l_NeedRFC THEN

      //----------
      //  The RowFocusChanged! event handles scrolling,
      //  validation, saving, and refreshing of the children
      //  DataWindows.  All we have to do is set i_MoveRow to
      //  tell the RowFocusChanged! event what row we want to
      //  move to.
      //----------

      SetRedraw(c_BufferDraw)
      i_RedrawCount = i_RedrawCount + 1
      i_RedrawStack[i_RedrawCount] = c_BufferDraw

      i_RFCEvent = c_RFC_Clicked
      i_MoveRow  = l_ClickedRow
      TriggerEvent(RowFocusChanged!)
   END IF

   //----------
   //  Check for errors/cancel.
   //----------

   IF PCCA.Error = c_Success THEN

      //----------
      //  If we have a valid column and it is different than the
      //  current column, then make sure the cursor gets moved
      //  to the new column.  Allow validation to run again
      //  because if pcd_Validate is not coded, PowerBuilder may
      //  be able to detect and generate a validation error when
      //  the column is changed.
      //----------

      IF l_ClickedCol > 0 THEN
         l_ColNbr = GetColumn()
         IF l_ClickedCol <> l_ColNbr THEN
            SetColumn(l_ClickedCol)

            IF PCCA.Error <> c_Success THEN
               i_IgnoreVal      = (NOT c_IgnoreVal)
               i_IgnoreValCount = i_IgnoreValCount + 1
               i_IgnoreValStack[i_IgnoreValCount] = c_IgnoreVal

               SetColumn(l_ColNbr)
               IF i_IgnoreValCount > 1 THEN
                  i_IgnoreValCount = i_IgnoreValCount - 1
                  i_IgnoreVal      = (NOT i_IgnoreValStack[i_IgnoreValCount])
               ELSE
                  i_IgnoreValCount = 0
                  i_IgnoreVal      = FALSE
               END IF
               PCCA.Error = c_Fatal
            END IF
         END IF
      END IF
   END IF

   IF l_NeedRFC THEN
      IF i_RedrawCount > 1 THEN
         i_RedrawCount = i_RedrawCount - 1
         SetRedraw(i_RedrawStack[i_RedrawCount])
      ELSE
         i_RedrawCount = 0
         SetRedraw(TRUE)
      END IF
   END IF
END IF

//----------
//  If there was an error during the Clicked! event, we do not
//  want the row and/or column changing.  Tell PowerBuilder to
//  discontinue processing.
//----------

Finished:

i_ExtendMode = (PCCA.Error = c_Success AND i_InUse)

IF NOT i_ExtendMode THEN
	//	EnMe 17-02-2009
	//	SetActionCode(c_NoProcessClicked)
	 RETURN c_NoProcessClicked
END IF
end event

event constructor;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : Constructor
//  Description   : Save the original size and position
//                  of the DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx

//----------
//  Determine the window the DataWindow is on.
//----------

IF PARENT.TypeOf() = UserObject! THEN
	//i_Container = PARENT
	//i_Window    = i_Container.i_Window
	
	// controllo che non sia un tab
	userobject luo_obj
	luo_obj = PARENT
	if luo_obj.getparent().TypeOf() = Tab! then
		i_Container =  PCCA.Null_Object
		i_Window    = luo_obj.getparent().getparent()
	else
		i_Container = PARENT
		i_Window    = i_Container.i_Window
	end if
	
	string ls_test
	ls_test =  parent.classname()
	
ELSE
   i_Container = PCCA.Null_Object
   i_Window    = PARENT
END IF

i_ClassName = i_Window.ClassName() + "." + ClassName()

//----------
//  Save the original position and size of the DataWindow.
//----------

i_XPos   = X
i_YPos   = Y
i_Width  = Width
i_Height = Height

//----------
//  Initialize NULL objects for use by the developer.
//----------

c_NullDW      = PCCA.Null_Object
c_NullCB      = PCCA.Null_Object
c_NullMenu    = PCCA.Null_Object
c_NullPicture = PCCA.Null_Object

//----------
//  Initialize the preferred-focus DW.
//----------

i_FocusDW = PCCA.Null_Object

//----------
//  Initialize the Open-Child option.
//----------

i_OpenChildOption = c_OpenChildAuto

//----------
//  Initialize the share type.
//----------

i_ShareType = c_ShareNone

//----------
//  Default Retrieve_DW() options for pcd_Search and pcd_Filter
//----------

i_xReselectMode = c_NoReselectRows
i_xRefreshMode  = c_RefreshSame

//----------
//  Action to be taken when validation is being ignored.
//----------

i_IgnoreValAction = c_RejectAndMove

//----------
//  Set up a structure to reset DataWindow Event Control
//  information.
//----------

is_ResetControl.Cascading              = FALSE
is_ResetControl.In_Cascade_Event       = FALSE
is_ResetControl.Only_Do_Children       = FALSE
is_ResetControl.Highlight_Only         = FALSE
is_ResetControl.No_Close               = FALSE
is_ResetControl.No_Refresh_After_Save  = FALSE
is_ResetControl.Check_Accept           = FALSE
is_ResetControl.Check_Quiet            = FALSE
is_ResetControl.Check_Cur_Instance     = FALSE
is_ResetControl.Clear_Ok               = FALSE
is_ResetControl.Clear_Cancel           = FALSE
is_ResetControl.Control_Mode           = c_ControlUndefined
is_ResetControl.Refresh_Cmd            = c_RefreshUndefined
is_ResetControl.Reset                  = FALSE
is_ResetControl.Level                  = 0
is_ResetControl.Insert_Row_Nbr         = 0
is_ResetControl.Skip_DW_Valid          = FALSE
is_ResetControl.Skip_DW                = PCCA.Null_Object
is_ResetControl.Skip_DW_Children_Valid = FALSE
is_ResetControl.Skip_DW_Children       = PCCA.Null_Object

//----------
//  Add this DataWindow the list of all DataWindows in
//  existance.
//----------

PCCA.PCMGR.i_DW_NumGlobalDW = PCCA.PCMGR.i_DW_NumGlobalDW + 1
PCCA.PCMGR.i_DW_GlobalDWList[PCCA.PCMGR.i_DW_NumGlobalDW] = THIS
i_WindowID = PCCA.PCMGR.i_WindowID

//----------
//  If the last DataWindow created has a different window ID
//  than this DataWindow, we know that it is on a different
//  window.
//----------

l_Idx = PCCA.PCMGR.i_DW_NumGlobalDW - 1

IF l_Idx > 0 THEN
   IF PCCA.PCMGR.i_DW_GlobalDWList[l_Idx].i_WindowID <> &
      i_WindowID THEN
      l_Idx = 0
   END IF
END IF

//----------
//  If l_Idx = 0, then we didn't find any other DataWindows that
//  exist on the same window as this DataWindow.  Just set up
//  this DataWindow.
//----------

IF l_Idx = 0 THEN
   l_Idx = PCCA.PCMGR.i_DW_NumGlobalDW
END IF

//----------
//  We will at least find this DataWindow, so don't need to check
//  for l_Idx = 0.  All DataWindows maintain a list of all other
//  DataWindows that reside on the same window as themselves.
//  The list of DataWindows residing on this window need to be
//  copied into this DataWindow and this DataWindow needs to be
//  added to all of the other co-resident DataWindow's lists.
//----------

i_NumWindowDWs = PCCA.PCMGR.i_DW_GlobalDWList[l_Idx].i_NumWindowDWs + 1
PCCA.PCMGR.i_DW_GlobalDWList[l_Idx].i_WindowDWs[i_NumWindowDWs] = THIS

//----------
//  Cycle through all of the DataWindows that reside on the
//  same window as this DataWindow.
//----------

FOR l_Jdx = 1 TO i_NumWindowDWs

   //----------
   //  Copy the list of DataWindows on this window into this
   //  DataWindow.
   //----------

   i_WindowDWs[l_Jdx] = PCCA.PCMGR.i_DW_GlobalDWList[l_Idx].i_WindowDWs[l_Jdx]

   //----------
   //  For all of the DataWindows on this window, we need to
   //  change the number of DataWindows on the window.
   //----------

   i_WindowDWs[l_Jdx].i_NumWindowDWs = i_NumWindowDWs

   //----------
   //  For all of the DataWindows on this window, we need to
   //  append this DataWindow to the list of DataWindows on
   //  the window.
   //----------

   i_WindowDWs[l_Jdx].i_WindowDWs[i_NumWindowDWs] = THIS
NEXT

i_ExtendMode = TRUE
end event

event dberror;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : DBError
//  Description   : Save the database error message and
//                  database specific error code.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

//EnMe 17-02-2009
//i_DBErrorCode    = DBErrorCode()
i_DBErrorCode = sqldbcode

//EnMe 17-02-2009
//i_DBErrorMessage = DBErrorMessage()
i_DBErrorMessage = sqlerrtext

//----------
//  Initialize the i_ExtendMode flag.  This flag is for the
//  developer to use while extending this event.
//----------

i_ExtendMode = TRUE

//----------
//  See if the developer requested that PowerClass check for
//  concurrency errors.
//----------

IF i_EnableCC THEN

  //----------
  //  Make sure that we are updating.
  //----------

  IF i_InUpdate > 0 THEN

      //----------
      //  Check for a concurrency error.  i_CCDBErrorID is the
      //  database-dependent code for a concurrency error and
      //  i_CCDBDuplicateID is the database-dependent code for
      //  duplicate values in a unqiue column.  If i_CCDBErrorID
      //  has not been set yet, then we need to figure out what
      //  database we are connected to and set it.
      //----------

      IF UpperBound(i_CCDBErrorID) = 0 THEN

         //----------
         //  Now that we know the database that we are connected
         //  to, we can now set i_CCDBErrorID to the database-
         //  dependent error codes for concurrency errors and
         //  i_CCDBDuplicateID  to the database-dependent error
         //  codes for duplicate values in unique columns.
         //----------

         CHOOSE CASE i_CCDBMSType

            //----------
            //  HP Allbase/SQL.
            //----------

            CASE PCCA.MB.c_DB_AllBase
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSAllBase
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSAllBase

            //----------
            //  Dec RDB.
            //----------

            CASE PCCA.MB.c_DB_DecRdb
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSDecRdb
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSDecRdb

            //----------
            //  Gupta SQLBase.
            //----------

            CASE PCCA.MB.c_DB_Gupta
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSGupta
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSGupta

            //----------
            //  IBM Distributed Relational Database Architecture.
            //----------

            CASE PCCA.MB.c_DB_IBMDRDA
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSIBMDRDA
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSIBMDRDA

            //----------
            //  Informix 4.
            //----------

            CASE PCCA.MB.c_DB_Informix4
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSInformix4
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSInformix4

            //----------
            //  Informix 5.
            //----------

            CASE PCCA.MB.c_DB_Informix5
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSInformix5
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSInformix5

            //----------
            //  Micro Decisionware Gateway for DB2.
            //----------

            CASE PCCA.MB.c_DB_MDG_DB2
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSMDGDB2
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSMDGDB2

            //----------
            //  Oracle 6.
            //----------

            CASE PCCA.MB.c_DB_Oracle6
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSOracle6
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSOracle6

            //----------
            //  Oracle 7.
            //----------

            CASE PCCA.MB.c_DB_Oracle7
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSOracle7
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSOracle7

            //----------
            //  SQL Server.
            //----------

            CASE PCCA.MB.c_DB_SQLServer
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSSQLServer_1
               i_CCDBErrorID[2]     = PCCA.PCMGR.c_CCErrorDBMSSQLServer_2
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSSQLServer

            //----------
            //  Sybase SQL Server System 10.
            //----------

            CASE PCCA.MB.c_DB_Sybase, PCCA.MB.c_DB_Sybase49, PCCA.MB.c_DB_SybaseNet
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSSybase_1
               i_CCDBErrorID[2]     = PCCA.PCMGR.c_CCErrorDBMSSybase_2
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSSybase

            //----------
            //  XDB.
            //----------

            CASE PCCA.MB.c_DB_XDB
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSXDB
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSXDB

            //----------
            //  ODBC/Watcom.
            //----------

            CASE PCCA.MB.c_DB_Watcom
               i_CCDBErrorID[1]     = PCCA.PCMGR.c_CCErrorDBMSWatcom
               i_CCDBDuplicateID[1] = PCCA.PCMGR.c_CCDuplicateDBMSWatcom

            //CASE PCCA.MB.c_DB_Unknown
            CASE ELSE
               PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::DBError"
               PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
               PCCA.MB.i_MB_Strings[3] = i_ClassName
               PCCA.MB.i_MB_Strings[4] = i_Window.Title
               PCCA.MB.i_MB_Strings[5] = DataObject
               PCCA.MB.i_MB_Strings[6] = &
               PCCA.MB.fu_GetDatabaseDescrip(i_DBCA)
               PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ChangesClobbered, &
                                  0, PCCA.MB.i_MB_Numbers[],         &
                                  6, PCCA.MB.i_MB_Strings[])
               i_EnableCC = FALSE
         END CHOOSE
      END IF

      //----------
      //  If the error reported by the database matches the
      //  database-dependent error code for concurrency errors,
      //  then we have a concurrency error.  Set i_CCDBError to
      //  TRUE to indicate that we do have a concurrency error
      //  and use SetActionCode to indicate that we do not want
      //  the error reported.
      //----------

      FOR l_Idx = 1 TO UpperBound(i_CCDBErrorID[])
         IF i_DBErrorCode = i_CCDBErrorID[l_Idx] THEN
            i_CCDBError  = TRUE
            i_ExtendMode = FALSE
            EXIT
            END IF
      NEXT
      IF NOT i_CCDBError THEN
         FOR l_Idx = 1 TO UpperBound(i_CCDBDuplicateID[])
            IF i_DBErrorCode = i_CCDBDuplicateID[l_Idx] THEN
               i_CCDBError  = TRUE
               i_ExtendMode = FALSE
            END IF
         NEXT
      END IF
   ELSE

      //----------
      //  If PowerClass is doing a RESELECTROW() during concurrency
      //  error processing, we don't want PowerBuilder displaying
      //  an error message.
      //----------

      IF i_InReselect > 0 THEN
         i_ExtendMode = FALSE
      END IF
   END IF
END IF

IF NOT i_ExtendMode THEN
	//EnMe 17-02-2009
   //SetActionCode(c_NoDisplayError)
   RETURN c_NoDisplayError
END IF
end event

event rbuttondown;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : RButtonDown
//  Description   : Processes the Right-button-down event.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

IF NOT i_InUse THEN
   i_Window.TriggerEvent("RButtonDown")
   GOTO Finished
END IF

//----------
//  Make sure that this window is active before we process
//  the right mouse button event.
//----------

IF PCCA.Window_Current <> i_Window THEN
   i_Window.SetFocus()
   Change_DW_Focus(THIS)
END IF

// stefanop: 12/10/2011 Fix per il problema della perdita di fuoco della DW dopo aver aperto la finestra di
// ricerca !!! Maledetto Framework !!!
if isnull(PCCA.Window_Current) then
	PCCA.Window_Current = i_Window
end if
//----------


//----------
//  Make sure that the popup menu has been initialized.
//----------

IF NOT i_PopupMenuInit THEN
   Set_DW_PMenu(i_PopupMenu)
END IF

//----------
//  Make sure that we have a right-button Popup Menu.
//----------

IF i_PopupMenuIsValid THEN

   //----------
   //  Make sure the POPUP menus are loaded correctly.
   //----------

   is_EventControl.Control_Mode = c_ControlPopupMenu
   TriggerEvent("pcd_SetControl")

   //----------
   //  We need to specify the position of the right-button Popup
   //  Menu.  If the window that contains this DataWindow is
   //  within an MDI frame, we need to specify the Popup Menu
   //  postion relative to the MDI Frame.  If the window is not
   //  inside the MDI frame, then we specify the Popup Menu's
   //  position relative to the window.  Note that a Response!-
   //  style window is not inside the MDI frame, even for a MDI
   //  application.
   //----------

   IF PCCA.MDI_Frame_Valid AND &
      i_Window.WindowType <> Response! THEN
      i_PopupMenu.PopMenu(PCCA.MDI_Frame.PointerX(), &
                          PCCA.MDI_Frame.PointerY())
   ELSE
      i_PopupMenu.PopMenu(i_Window.PointerX(), &
                          i_Window.PointerY())
   END IF
END IF

Finished:

i_ExtendMode = i_InUse
end event

on destructor;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : Destructor
//  Description   : When a window closes, this routine will
//                  remove the DataWindows from the list of
//                  living DataWindows and unwire them, if
//                  necessary.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER              l_Idx
GRAPHICOBJECT        l_SearchObj
GRAPHICOBJECT        l_FilterObj
UO_CB_MAIN           l_PCControl
UO_CB_MESSAGE        l_MessageCB
UO_DW_FIND           l_FindObj

//----------
//  Destroy the POPUP menu if we created it.
//----------

IF IsValid(i_mPopup) THEN
   DESTROY i_mPopup
END IF

//----------
//  If PowerBuilder is closing windows, PCCA.PCMGR may not be
//  there any more.
//----------

IF IsValid(PCCA.PCMGR) THEN

   //----------
   //  Remove this DataWindow from the global list of all
   //  DataWindows.
   //----------

   Remove_Obj(THIS,                        &
              PCCA.PCMGR.i_DW_NumGlobalDW, &
              PCCA.PCMGR.i_DW_GlobalDWList[])
END IF

//----------
//  For this DataWindow, we should unwire all buttons that have
//  been wired to this DataWindow.
//----------

FOR l_Idx = i_NumPCControls TO 1 STEP -1
   l_PCControl = i_PCControls[l_Idx]
   IF IsValid(l_PCControl) THEN
      IF l_PCControl.i_TrigObject = THIS THEN
         Unwire_Button(l_PCControl)
      END IF
   END IF
NEXT

//----------
//  As with buttons, unwire any message buttons.
//----------

FOR l_Idx = i_NumMessages TO 1 STEP -1
   l_MessageCB = i_Messages[l_Idx]
   IF IsValid(l_MessageCB) THEN
      Unwire_Message(l_MessageCB)
   END IF
NEXT

//----------
//  As with buttons, unwire any find objects.
//----------

FOR l_Idx = i_NumFindObjects TO 1 STEP -1
   l_FindObj = i_FindObjects[l_Idx]
   IF IsValid(l_FindObj) THEN
      Unwire_Find(l_FindObj)
   END IF
NEXT

//----------
//  As with buttons, unwire any search objects.
//----------

FOR l_Idx = i_NumSearchObjects TO 1 STEP -1
   l_SearchObj = i_SearchObjects[l_Idx]
   IF IsValid(l_SearchObj) THEN
      Unwire_Search(l_SearchObj)
   END IF
NEXT

//----------
//  As with buttons, unwire any filter objects.
//----------

FOR l_Idx = i_NumFilterObjects TO 1 STEP -1
   l_FilterObj = i_FilterObjects[l_Idx]
   IF IsValid(l_FilterObj) THEN
      Unwire_Filter(l_FilterObj)
   END IF
NEXT

i_ExtendMode = TRUE

end on

event sqlpreview;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : SQLPreview
//  Description   : Saves the row number and buffer for the
//                  row currently being updated to the database.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

// EnMe 17-02-2009 Corretto con nuovo sistema.
//GetUpdateStatus(i_DBErrorRow, i_DBErrorBuffer)

i_DBErrorRow = row
i_DBErrorBuffer = buffer

i_ExtendMode = TRUE
end event

on getfocus;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : GetFocus
//  Description   : Set the current DataWindow global variable in
//                  the PCCA structure to this DataWindow.  The
//                  PCCA.Window_CurrentDW variable is checked
//                  before most user events to determine which
//                  DataWindow is current.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

LONG  l_RowNbr

//----------
//  Initially assume that this is not the DataWindow that should
//  be getting focus and becoming the current DataWindow.
//----------

i_GotFocus = FALSE

//----------
//  If PCCA.PCMGR.i_ProcessingActivate is TRUE, then this window
//  has been activated, but the correct current DataWindow has
//  not been determined as of yet.
//----------

IF PCCA.PCMGR.i_ProcessingActivate THEN

   //----------
   //  If this window has just been activated and the
   //  current DataWindow has not been set, the variable
   //  PCCA.PCMGR.i_JustActivated will be TRUE.  In this
   //  case, this DataWindow may or may not be the DataWindow
   //  which should get focus and become the current DataWindow.
   //----------

   IF PCCA.PCMGR.i_JustActivated THEN

      //----------
      //  Indicate that the DataWindow that received focus has
      //  been set.
      //----------

      PCCA.PCMGR.i_JustActivated = FALSE

      //----------
      //  Remember that this is the DataWindow that originally
      //  got focus when the window was activated.
      //----------

      PCCA.PCMGR.i_ActivateFocusDW = THIS
   END IF

   //----------
   //  Even though the pc_Activate may have already been posted
   //  to the queue, keep posting it because PowerBuilder loses
   //  posted events in certain situtations.  PostEvent() just
   //  adds the event to the message queue and returns.  The
   //  pc_Activate event will not run until some time in the
   //  future when it is pulled from the message queue.
   //----------

   PCCA.PCMGR.PostEvent("pc_Activate")

   //----------
   //  Since we do not know at this time if this is the
   //  DataWindow that should become current, just jump out.
   //  The pc_Activate event will take care of setting the
   //  correct current DataWindow at a later time.
   //----------

   GOTO Finished
END IF

//----------
//  If we are not in use, then we can not be the current
//  DataWindow.
//----------

IF NOT i_InUse THEN
   GOTO Finished
END IF

//----------
//  If we got this far, then this DataWindow should become the
//  current DataWindow.  Set the i_GotFocus flag so that the
//  developer knows that this DataWindow really got focus.
//----------

i_GotFocus = TRUE

//----------
//  If this is an Instance DataWindow, then we need to find the
//  record in the parent DataWindow which caused this DataWindow
//  to open.
//----------

IF i_IsInstance THEN

   //----------
   //  If we are in RFC, then RFC is changing focus to this
   //  instance DataWindow.  No need to loop back into RFC again.
   //----------

   IF i_InRFC = 0 THEN

      //----------
      //  Using the keys that were saved when this Instance
      //  DataWindow was opened, search for the corresponding row
      //  in the parent DataWindow.
      //----------

      l_RowNbr = i_ParentDW.Find_Key_Row(1, i_KeysInParent[], &
                                         1, i_ParentDW.RowCount())

      //----------
      //  If l_RowNbr is greater than 0, then we found the row in
      //  our parent DataWindow the opened this DataWindow.  Tell
      //  the parent DataWindow to move its cursor to that row, but
      //  not to select it.  If we told the parent DataWindow to
      //  select the row, it would try to check for changes and
      //  re-load the row back into this DataWindow.
      //----------

      IF l_RowNbr >  0                      THEN
      IF l_RowNbr <> i_ParentDW.i_CursorRow THEN
         IF i_ParentDW.i_NumChildren = 0 THEN
            i_ParentDW.i_MoveRow  = - l_RowNbr
         ELSE
            i_ParentDW.i_RFCEvent = c_RFC_NoSelect
            i_ParentDW.i_MoveRow  = l_RowNbr
         END IF
         i_ParentDW.TriggerEvent(RowFocusChanged!)
      END IF
      END IF
   END IF
END IF

//----------
//  Tell the DataWindow to buffer draws.
//----------

SetRedraw(c_BufferDraw)
i_RedrawCount = i_RedrawCount + 1
i_RedrawStack[i_RedrawCount] = c_BufferDraw

//----------
//  Make this DataWindow the current DataWindow.  pcd_Active
//  will also take care of setting up controls and menus.
//----------

TriggerEvent("pcd_Active")

//----------
//  Just because the event was triggered, we may not be the
//  control with focus (e.g. when a window is activated it may
//  set focus to a command button, which is the first tab object.
//  Another PowerClass event (e.g. "pc_Activate")  may have
//  triggered this event to make it the current DataWindow.
//----------

IF GetFocus() = THIS THEN

   //----------
   //  This DataWindow does have focus.  Set the current row
   //  indicator.
   //----------

   TriggerEvent("pcd_SetRowIndicator")
END IF

//----------
//  Restore redraw mode.
//----------

IF i_RedrawCount > 1 THEN
   i_RedrawCount = i_RedrawCount - 1
   SetRedraw(i_RedrawStack[i_RedrawCount])
ELSE
   i_RedrawCount = 0
   SetRedraw(TRUE)
END IF

Finished:

i_ExtendMode = i_GotFocus
end on

on losefocus;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : LoseFocus
//  Description   : Changes the RowFocusIndicator to the hand when
//                  the DataWindow loses focus.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//----------
//  If the DataWindow is not in use, jump out.
//----------

IF NOT i_InUse THEN
   GOTO Finished
END IF

//----------
//  Set the current row indicator.
//----------

TriggerEvent("pcd_SetRowIndicator")

Finished:

i_ExtendMode = i_InUse
end on

on updatestart;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : UpdateStart
//  Description   : Used by concurrency checking to initialize
//                  the i_CCSkip*Rows[] variables.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CCWhichUpdate = i_CCWhichUpdate + 1

//----------
//  See if we are starting a new UPDATE() or if this is the
//  second (or greater) attempt to perform the update.
//----------

IF i_CCMaxNumUpdate < i_CCWhichUpdate THEN

   //----------
   //  New UPDATE().  Initialize the appropriate instance
   //  variables.
   //----------

   i_CCMaxNumUpdate                     = i_CCWhichUpdate
   i_CCSkipDeleteRows[i_CCWhichUpdate]  = 0
   i_CCSkipFilterRows[i_CCWhichUpdate]  = 0
   i_CCSkipPrimaryRows[i_CCWhichUpdate] = 0

   IF i_CCWhichUpdate > 1 THEN

      //----------
      //  Since we successfully made it through the previous
      //  UPDATE() for the DataWindow, reset the instance
      //  variables used to limit the maximum number of retries
      //  for concurrency errors.
      //----------

      i_CCLastBadRow[i_CCWhichUpdate - 1] = 0
      i_CCNumRetry[i_CCWhichUpdate - 1]   = 0
   END IF

   //----------
   //  If i_CCLastBadRow[] and i_CCNumRetry[] have not been
   //  initialized, initialize them now.
   //----------

   IF UpperBound(i_CCLastBadRow[]) < i_CCWhichUpdate THEN
      i_CCLastBadRow[i_CCWhichUpdate] = 0
      i_CCNumRetry[i_CCWhichUpdate]   = 0
   END IF
END IF

i_ExtendMode = i_InUse
end on

on uo_dw_main.create
end on

on uo_dw_main.destroy
end on

