﻿$PBExportHeader$uo_editor.sru
forward
global type uo_editor from datawindow
end type
end forward

global type uo_editor from datawindow
integer width = 686
integer height = 400
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
event uoe_selection_changed ( integer ai_index,  string as_name )
event uoe_mousemove pbm_dwnmousemove
event uoe_mousebuttonup pbm_dwnlbuttonup
event uoe_dragend ( integer ai_x,  integer ai_y,  integer ai_width,  integer ai_height,  string as_name )
event uoe_check_position ( )
event uoe_zoom_changed ( decimal ai_zoom_scale )
event type boolean uoe_selection_changing ( integer ai_old_id,  integer ai_new_id,  string ai_old_name,  string ai_new_name )
end type
global uo_editor uo_editor

type variables
// -- Variabili utente
protected:
	string is_user_temporaly = ""
	string is_user_documents = ""
	string is_image_path = ""

// -- Varibili relative ai rettangoli
protected:
	string is_font_size = "10"
	string is_color_normal = ""
	string is_color_selected = ""
	boolean ib_editor = true

// -- Variabili messagebox
protected:
	string is_messagebox_title = "uo_editor"
	
// -- Variabili mappa
protected:
	long il_dw_max_width
	long il_dw_max_height
	long il_image_width_original
	long il_image_height_original
	long il_image_width
	long il_image_height
	long il_image_pixel_width
	long il_image_pixel_height
	dec{2} id_zoom_scale = 1
	
// -- Variabili rettangoli
protected:
	string is_t_active
	int		il_t_active
	
// -- Varibili Drag&Drop rettangoli
private:
	string is_rectangle_dragging = ""
	boolean ib_leftclick_pressed = false
	boolean ib_rectangle_drag = false
end variables

forward prototypes
public subroutine uof_draw ()
protected subroutine uof_reset ()
protected function string uof_get_labels ()
protected subroutine msg (string as_message)
protected function string uof_getuserinfo (string as_key)
protected subroutine uof_set_directory (string as_directory)
protected subroutine uof_fix_tilde (ref string as_string)
public subroutine uof_clear ()
public subroutine uof_set_dimension (long al_max_width, long al_max_height)
public subroutine uof_resize ()
public subroutine uof_set_editormode (boolean ab_editor_mode)
public subroutine uof_set_zoom (integer ai_zoom_scale)
end prototypes

event uoe_selection_changed(integer ai_index, string as_name);/*
*/
end event

event uoe_mousemove;if ib_leftclick_pressed and left(dwo.name, 2) = "t_" then
	ib_rectangle_drag = true
	is_rectangle_dragging = string(dwo.name)
end if
end event

event uoe_mousebuttonup;if ib_rectangle_drag then
	
	event post uoe_check_position()
	ib_rectangle_drag = false
end if

ib_leftclick_pressed = false
ib_rectangle_drag = false
end event

event uoe_dragend(integer ai_x, integer ai_y, integer ai_width, integer ai_height, string as_name);/*
*/
end event

event uoe_check_position();int li_x, li_y, li_width, li_height

li_x = integer(describe(is_rectangle_dragging + ".x"))
li_y = integer(describe(is_rectangle_dragging + ".y"))
li_width = integer(describe(is_rectangle_dragging + ".width"))
li_height = integer(describe(is_rectangle_dragging + ".height"))

event uoe_dragend(li_x, li_y, li_width, li_height, is_rectangle_dragging)
end event

event uoe_zoom_changed(decimal ai_zoom_scale);/*
*/
end event

event type boolean uoe_selection_changing(integer ai_old_id, integer ai_new_id, string ai_old_name, string ai_new_name);/* */

return true
end event

public subroutine uof_draw ();string ls_dw, ls_error
boolean lb_return

SetPointer(HourGlass!)

ls_dw = 'release 11.5; &
datawindow(units=0 timer_interval=0 color=1073741824 brushmode=0 transparency=0 gradient.angle=0 gradient.color=8421504 gradient.focus=0 gradient.repetition.count=0 gradient.repetition.length=1 gradient.repetition.mode=1 gradient.scale=100 gradient.spread=100 gradient.transparency=0 picture.blur=0 picture.clip.bottom=0 picture.clip.left=0 picture.clip.right=0 picture.clip.top=0 picture.mode=0 picture.scale.x=0 picture.scale.y=0 picture.transparency=0 processing=0 HTMLDW=no print.printername="" print.documentname="" print.orientation = 0 print.margin.left = 110 print.margin.right = 110 print.margin.top = 96 print.margin.bottom = 96 print.paper.source = 0 print.paper.size = 0 print.canusedefaultprinter=yes print.prompt=no print.buttons=no print.preview.buttons=no print.cliptext=no print.overrideprintjob=no print.collate=yes print.background=no print.preview.background=no print.preview.outline=yes hidegrayline=no showbackcoloronxp=no picture.file="" ) &
header(height=0 color="536870912" transparency="0" gradient.color="8421504" gradient.transparency="0" gradient.angle="0" brushmode="0" gradient.repetition.mode="0" gradient.repetition.count="0" gradient.repetition.length="100" gradient.focus="0" gradient.scale="100" gradient.spread="100" ) &
summary(height=0 color="536870912" transparency="0" gradient.color="8421504" gradient.transparency="0" gradient.angle="0" brushmode="0" gradient.repetition.mode="0" gradient.repetition.count="0" gradient.repetition.length="100" gradient.focus="0" gradient.scale="100" gradient.spread="100" ) &
footer(height=0 color="536870912" transparency="0" gradient.color="8421504" gradient.transparency="0" gradient.angle="0" brushmode="0" gradient.repetition.mode="0" gradient.repetition.count="0" gradient.repetition.length="100" gradient.focus="0" gradient.scale="100" gradient.spread="100" ) &
detail(height=' + string(this.height) +' color="536870912" transparency="0" gradient.color="8421504" gradient.transparency="0" gradient.angle="0" brushmode="0" gradient.repetition.mode="0" gradient.repetition.count="0" gradient.repetition.length="100" gradient.focus="0" gradient.scale="100" gradient.spread="100" height.autosize=yes ) &
table(column=(type=char(10) updatewhereclause=yes name=a dbname="a" ) &
 )  &
bitmap(band=detail filename="' + this.is_image_path + '" x="0" y="0" height="' + string(il_image_height) + '" width="' + string(il_image_width) + '" border="0" tooltip.backcolor="134217752" tooltip.delay.initial="0" tooltip.delay.visible="32000" tooltip.enabled="0" tooltip.hasclosebutton="0" tooltip.icon="0" tooltip.isbubble="0" tooltip.maxwidth="0" tooltip.textcolor="134217751" tooltip.transparency="0"  name=p_1 visible="1" transparency="0" ) &
' + uof_get_labels() + ' &
htmltable(border="1" ) &
htmlgen(clientevents="1" clientvalidation="1" clientcomputedfields="1" clientformatting="0" clientscriptable="0" generatejavascript="1" encodeselflinkargs="1" netscapelayers="0" pagingmethod=0 generatedddwframes="1" ) &
xhtmlgen() cssgen(sessionspecific="0" ) &
xmlgen(inline="0" ) &
xsltgen() &
jsgen() &
export.xml(headgroups="1" includewhitespace="0" metadatatype=0 savemetadata=0 ) &
import.xml() &
export.pdf(method=0 distill.custompostscript="0" xslfop.print="0" ) &
export.xhtml()'

setredraw(false)
if create(ls_dw, ref ls_error) < 0 then
	messagebox("uo_editor::uof_redraw", ls_error)
else
	this.insertrow(0)
end if

bringtotop = true
setredraw(true)

SetPointer(Arrow!)

end subroutine

protected subroutine uof_reset ();/**
 * Stefano Pulze
 * 09-11-2009
 *
 * Pulisco variabili
 */
 

end subroutine

protected function string uof_get_labels ();string ls_modify
string ls_mode

if this.ib_editor then
	ls_mode = "1"
else
	ls_mode = "0"
end if

ls_modify = "text(band=detail alignment=~"0~" text=~"asdasd~" border=~"0~" resizeable="+ls_mode+"  moveable="+ls_mode+" color=~"33554432~" x=~"754~" y=~"544~" height=~"360~" width=~"1120~" html.valueishtml=~"0~"  name=t_1 visible=~"1~"  font.face=~"Tahoma~" font.height=~"-"+is_font_size+"~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"2~" background.color=~"32889792~" background.transparency=~"0~" background.gradient.color=~"8421504~" background.gradient.transparency=~"0~" background.gradient.angle=~"0~" background.brushmode=~"0~" background.gradient.repetition.mode=~"0~" background.gradient.repetition.count=~"0~" background.gradient.repetition.length=~"100~" background.gradient.focus=~"0~" background.gradient.scale=~"100~" background.gradient.spread=~"100~" tooltip.backcolor=~"134217752~" tooltip.delay.initial=~"0~" tooltip.delay.visible=~"32000~" tooltip.enabled=~"0~" tooltip.hasclosebutton=~"0~" tooltip.icon=~"0~" tooltip.isbubble=~"0~" tooltip.maxwidth=~"0~" tooltip.textcolor=~"134217751~" tooltip.transparency=~"0~" transparency=~"0~" )"

return ls_modify
end function

protected subroutine msg (string as_message);g_mb.messagebox(is_messagebox_title, as_message)
end subroutine

protected function string uof_getuserinfo (string as_key);// Ritorna le variabili d'ambiente del sistema
string ls_Path
string ls_values[]
ContextKeyword lcxk_base

this.GetContextService("Keyword", lcxk_base)
lcxk_base.GetContextKeywords(as_key, ls_values)
IF Upperbound(ls_values) > 0 THEN
   ls_Path = ls_values[1]
ELSE
   ls_Path = "*UNDEFINED*"
END IF

return ls_Path
end function

protected subroutine uof_set_directory (string as_directory);/**
 * Stefano Pulze
 * 09 - 11 - 2009
 *
 * Consente di impostare la directory dove verranno salvati i file
 * temporanei usati dalla finestra. La cartella sarà salvata all'interno
 * user_temp_folder/omnia/as_directory
 *
 * @param string as_directory
 */
 
if as_directory <> "" and not isnull(as_directory) then
	
	if not DirectoryExists(is_user_temporaly + as_directory) then
		CreateDirectory(is_user_temporaly + as_directory)
	end if
	
end if

// -- controllo che sia presente la / finale di separazione directory
if right(as_directory, 1) <> "\" then
	as_directory += "\"
end if
// ----

is_user_temporaly += as_directory
end subroutine

protected subroutine uof_fix_tilde (ref string as_string);string	ls_tilde
integer li_lunghezza_formula,li_t,li_posizione,li

ls_tilde = "~~"

li_lunghezza_formula = len(as_string)

if li_lunghezza_formula=0 then return

li_posizione = 1

do while 1=1
	li_posizione = pos(as_string, ls_tilde, li_posizione)
	if li_posizione = 0 then exit	
	as_string = replace ( as_string, li_posizione, len(ls_tilde), ls_tilde + ls_tilde ) 
	li_posizione += len(ls_tilde + ls_tilde)
loop
end subroutine

public subroutine uof_clear ();/**
 * Stefano Pulze
 * 09-11-2009
 *
 * Funzione pubblica che permette di ripulire tutte le variabili e fare
 * il redraw della datawindow
 * Se si deve pulire solo le variabili via codice usare la funzione privata
 * uof_reset()
 */
 
uof_reset()
uof_draw()
end subroutine

public subroutine uof_set_dimension (long al_max_width, long al_max_height);/**
 * Stefano Pulze
 * 10-11-2009
 *
 * Consente di impostare i margini massimi di ridimensionamento della
 * datawindow; se l'immagine supera quest'ultimi allora saranno visulizzate
 * le barre di scorrimento appropriate
 *
 * @param long al_max_width
 * @param long al_max_height
 */
 
il_dw_max_width = al_max_width
il_dw_max_height = al_max_height
end subroutine

public subroutine uof_resize ();if il_image_width > this.il_dw_max_width then
	this.width = il_dw_max_width
	this.Hscrollbar = true
else
	this.width = il_image_width
	this.Hscrollbar = false
end if

if il_image_height > this.il_dw_max_height then
	this.height = il_dw_max_height
	this.Vscrollbar = true
else
	this.height = il_image_height
	this.Vscrollbar = false
end if
end subroutine

public subroutine uof_set_editormode (boolean ab_editor_mode);/**
 * Stefano Pulze
 * 19-12-2009
 *
 * Indica se l'editor è in modalita di sola visualizzazione (navigatore)
 * o se è in modalità di costruzione (editor)
 *
 * @param boolean ab_editor_mode true se editor, false se navigatore
 */
this.ib_editor = ab_editor_mode
end subroutine

public subroutine uof_set_zoom (integer ai_zoom_scale);id_zoom_scale = ai_zoom_scale / 100

// ricalcolo dimensioni
il_image_width = il_image_width_original * id_zoom_scale
il_image_height = il_image_height_original * id_zoom_scale
this.uof_resize()
// ----

event uoe_zoom_changed(id_zoom_scale)

uof_draw()
end subroutine

on uo_editor.create
end on

on uo_editor.destroy
end on

event constructor;// Calcolo percorsi utente
try
	f_getuserpath(is_user_temporaly)
	is_user_temporaly += "omnia\"
	
	if not DirectoryExists(is_user_temporaly) then
		CreateDirectory(is_user_temporaly)
	end if
	
	is_user_documents  = uof_getuserinfo("HOMEDRIVE")
	is_user_documents += uof_getuserinfo("HOMEPATH")
	is_user_documents += "\Documenti\"
catch (RuntimeError ex)
	setnull(is_user_temporaly)
	setnull(is_user_documents)
end try
// ---

this.uof_set_dimension(this.width, this.height)

end event

event clicked;string ls_old_name, ls_new_name
long ll_old_id, ll_new_id

if left(dwo.name, 2) = "t_" then
	ls_old_name = is_t_active
	ll_old_id = il_t_active
	
	ls_new_name = string(dwo.name)
	ll_new_id = integer(mid(dwo.name, 3))
	
	if event uoe_selection_changing(ll_old_id, ll_new_id, ls_old_name, ls_new_name) then
		
		is_t_active = ls_new_name
		il_t_active = ll_new_id
		
		ib_leftclick_pressed = true
		
		event post uoe_selection_changed(ll_new_id, ls_new_name)
	end if
else
	ib_leftclick_pressed = false
end if
end event

event rbuttondown;trigger event clicked(xpos, ypos, row, dwo)
end event

