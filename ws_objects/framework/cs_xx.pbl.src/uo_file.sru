﻿$PBExportHeader$uo_file.sru
forward
global type uo_file from nonvisualobject
end type
end forward

global type uo_file from nonvisualobject
end type
global uo_file uo_file

type variables
constant string FILE_NAME_PATTERN = "([\w\d\-\]+.[A-Za-z]{2,})$"


protected:
	int ii_handle = -1
	string is_filepath
end variables

forward prototypes
public function integer sanatize_filename (string as_file_name)
public function integer copy_to_folder (string as_files[], string as_remote_folder, ref string as_error)
public function string get_file_name (string as_file_path)
public subroutine close ()
public function boolean open (string as_filepath, boolean ab_append)
end prototypes

public function integer sanatize_filename (string as_file_name);/*
 */
 
 return 0
end function

public function integer copy_to_folder (string as_files[], string as_remote_folder, ref string as_error);/**
 * stefanop
 * 25/09/2014
 *
 * Copia il file indicati all'interno di una cartella
 * 
 * Ritorna: -1 error, > 0 numero di file copiati
 **/

string ls_file_name
int li_i, li_count

// Controllo esistenza cartella remota
if DirectoryExists(as_remote_folder) then
	
	li_count = upperbound(as_files)
	
	if not g_str.end_with(as_remote_folder, "/") then as_remote_folder += "/"
	
	for li_i = 1 to li_count
		
		ls_file_name = get_file_name(as_files[li_i])
		
		if FileCopy(as_files[li_i], as_remote_folder + ls_file_name, true) < 0 then
			as_error = "Errore durante la copia del file " + g_str.safe(as_files[li_i]) + " nella cartella " + g_str.safe(as_remote_folder)
			return -1
			
		end if
		
	next
	
	return li_count
	
else
	as_error = "La cartella di destinazione " + g_str.safe(as_remote_folder) + " non esiste o non si ha accesso per la scrittura."
	return -1
	
end if

end function

public function string get_file_name (string as_file_path);/**
 * stefanop
 * 25/09/2014
 *
 * Estrae il nome del file (compresa di estensione) dalla stringa con il percorso
 * completo.
 *
 **/
 
string ls_name
uo_regex luo_regex

setnull(ls_name)

if g_str.isnotempty(as_file_path) then

	luo_regex = create uo_regex

	luo_regex.initialize(FILE_NAME_PATTERN, true, false)
	luo_regex.search(as_file_path)
	ls_name = luo_regex.match(1)
	
	destroy luo_regex

end if

return ls_name
end function

public subroutine close ();if ii_handle > 0 then
	fileclose(ii_handle)
end if
end subroutine

public function boolean open (string as_filepath, boolean ab_append);/**
 * stefanop
 * 07/10/2014
 *
 * Apre il file
 **/
 
if ab_append = false and fileexists(as_filepath) then
	filedelete(as_filepath)
end if

is_filepath = as_filepath
ii_handle = fileopen(as_filepath, TextMode!, Write!, Shared!, Append!)

return true
end function

on uo_file.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_file.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event destructor;close()
end event

