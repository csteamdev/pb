﻿$PBExportHeader$uo_file_text.sru
forward
global type uo_file_text from uo_file
end type
end forward

global type uo_file_text from uo_file
end type
global uo_file_text uo_file_text

forward prototypes
public function uo_file_text new_line ()
public function uo_file_text write (string as_value)
end prototypes

public function uo_file_text new_line ();write("~r~n")
return this
end function

public function uo_file_text write (string as_value);try
	
	filewriteex(ii_handle, as_value)
	
catch(RuntimeError e)
end try

return this
end function

on uo_file_text.create
call super::create
end on

on uo_file_text.destroy
call super::destroy
end on

