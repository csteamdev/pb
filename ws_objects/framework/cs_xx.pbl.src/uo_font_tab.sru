﻿$PBExportHeader$uo_font_tab.sru
forward
global type uo_font_tab from userobject
end type
type pb_right from picturebutton within uo_font_tab
end type
type pb_center from picturebutton within uo_font_tab
end type
type pb_left from picturebutton within uo_font_tab
end type
type pb_underline from picturebutton within uo_font_tab
end type
type pb_italic from picturebutton within uo_font_tab
end type
type pb_bold from picturebutton within uo_font_tab
end type
type em_font_size from editmask within uo_font_tab
end type
type ddlb_fonts from dropdownpicturelistbox within uo_font_tab
end type
end forward

global type uo_font_tab from userobject
integer width = 992
integer height = 228
long backcolor = 67108864
string text = "none"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
event ue_load_fonts ( )
event ue_style_changed ( string as_font_name,  integer ai_font_size )
event ue_font_variant_changed ( boolean ab_bold,  boolean ab_italic,  boolean ab_underline )
event ue_font_align_changed ( integer ai_align )
event ue_font_type_changed ( string as_font_name )
event ue_font_size_changed ( integer ai_font_size )
pb_right pb_right
pb_center pb_center
pb_left pb_left
pb_underline pb_underline
pb_italic pb_italic
pb_bold pb_bold
em_font_size em_font_size
ddlb_fonts ddlb_fonts
end type
global uo_font_tab uo_font_tab

type variables
public:
	// Indica che è possibile aver premuto sia su Bold che su Italic che su Underline allo stesso tempo
	boolean ib_multiple_variant = true
	
	constant int ALIGN_LEFT = 0
	constant int ALIGN_RIGHT = 1
	constant int ALIGN_CENTER = 2
	constant int ALIGN_JUSTIFIED = 3
	
private:
	boolean ib_bold = false
	boolean ib_italic = false
	boolean ib_underline = false
end variables

forward prototypes
public subroutine uof_set_fontstyle (object_style aos_style)
end prototypes

event ue_load_fonts();/** 
 * stefanop
 * 01/01/2010
 *
 * Carico i font
 **/
 
ddlb_fonts.additem("Arial", 1)
ddlb_fonts.additem("Courier New", 1)
ddlb_fonts.additem("System", 1)
ddlb_fonts.additem("Times New Roman", 1)
end event

event ue_style_changed(string as_font_name, integer ai_font_size);/** 
 * stefanop
 * 01/10/2010
 *
 * Evento generato quando viene cambiato un valore
 **/
end event

event ue_font_variant_changed(boolean ab_bold, boolean ab_italic, boolean ab_underline);/**
 * stefanop
 * 01/10/2010
 *
 * Evento generato alla pressione dei pulsanti Bold, Italic e Underline
 * A TRUE quello premuto
 **/
end event

event ue_font_align_changed(integer ai_align);/**
 * stefanop
 * 01/10/2010
 *
 * Evento generato quando viene cambiata l'allineamento del testo
 * Usare le costanti pubbliche per capire da che parte si sta allineando
 **/
end event

event ue_font_type_changed(string as_font_name);/**
 * stefanop
 * 04/10/2010
 *
 * Evento generato quando il tipo di carattere viene cambiato
 **/
end event

event ue_font_size_changed(integer ai_font_size);/**
 * stefanop
 * 04/10/2010
 *
 * Generato quanto la dimensione del font viene cambiata
 **/
end event

public subroutine uof_set_fontstyle (object_style aos_style);/**
 * stefanop
 * 04/10/2010
 *
 * Imposto i parametri con quelli passati
 **/
 
em_font_size.text = aos_style.font_height
ddlb_fonts.text = aos_style.font_face

if aos_style.font_weight = "400" then
	ib_bold = false
else
	ib_bold = true
end if

if aos_style.italic = "0" then
	ib_italic = false
else
	ib_italic = true
end if

if aos_style.underline = "0" then
	ib_underline = false
else
	ib_underline = true
end if
end subroutine

on uo_font_tab.create
this.pb_right=create pb_right
this.pb_center=create pb_center
this.pb_left=create pb_left
this.pb_underline=create pb_underline
this.pb_italic=create pb_italic
this.pb_bold=create pb_bold
this.em_font_size=create em_font_size
this.ddlb_fonts=create ddlb_fonts
this.Control[]={this.pb_right,&
this.pb_center,&
this.pb_left,&
this.pb_underline,&
this.pb_italic,&
this.pb_bold,&
this.em_font_size,&
this.ddlb_fonts}
end on

on uo_font_tab.destroy
destroy(this.pb_right)
destroy(this.pb_center)
destroy(this.pb_left)
destroy(this.pb_underline)
destroy(this.pb_italic)
destroy(this.pb_bold)
destroy(this.em_font_size)
destroy(this.ddlb_fonts)
end on

event constructor;// Imposto immagini
string ls_base

ls_base = s_cs_xx.volume + s_cs_xx.risorse + "11.5\"

pb_bold.picturename = ls_base + "text_bold.png"
pb_italic.picturename = ls_base + "text_italic.png"
pb_underline.picturename = ls_base + "text_underline.png"
pb_left.picturename = ls_base + "shape_align_left.png"
pb_center.picturename = ls_base + "shape_align_center.png"
pb_right.picturename = ls_base + "shape_align_right.png"

ddlb_fonts.deletepictures()
ddlb_fonts.addpicture(ls_base + "font.png")

event ue_load_fonts()

ddlb_fonts.selectitem(1)
end event

type pb_right from picturebutton within uo_font_tab
integer x = 869
integer y = 120
integer width = 110
integer height = 96
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\shape_align_right.png"
alignment htextalign = left!
end type

event clicked;event ue_font_align_changed(ALIGN_RIGHT)
end event

type pb_center from picturebutton within uo_font_tab
integer x = 754
integer y = 120
integer width = 110
integer height = 96
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\shape_align_center.png"
alignment htextalign = left!
end type

event clicked;event ue_font_align_changed(ALIGN_CENTER)
end event

type pb_left from picturebutton within uo_font_tab
integer x = 640
integer y = 120
integer width = 110
integer height = 96
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\shape_align_left.png"
alignment htextalign = left!
end type

event clicked;event ue_font_align_changed(ALIGN_LEFT)
end event

type pb_underline from picturebutton within uo_font_tab
integer x = 480
integer y = 120
integer width = 110
integer height = 96
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\text_underline.png"
alignment htextalign = left!
end type

event clicked;ib_underline = not ib_underline

if not ib_multiple_variant then
	ib_bold = false
	ib_italic = false
end if

event ue_font_variant_changed(ib_bold, ib_italic, ib_underline)
end event

type pb_italic from picturebutton within uo_font_tab
integer x = 366
integer y = 120
integer width = 110
integer height = 96
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\text_italic.png"
alignment htextalign = left!
end type

event clicked;ib_italic = not ib_italic

if not ib_multiple_variant then
	ib_bold = false
	ib_underline = false
end if

event ue_font_variant_changed(ib_bold, ib_italic, ib_underline)
end event

type pb_bold from picturebutton within uo_font_tab
integer x = 251
integer y = 120
integer width = 110
integer height = 96
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean originalsize = true
string picturename = "C:\cs_115\framework\risorse\11.5\text_bold.png"
alignment htextalign = left!
end type

event clicked;ib_bold = not ib_bold

if not ib_multiple_variant then
	ib_italic = false
	ib_underline = false
end if

event ue_font_variant_changed(ib_bold, ib_italic, ib_underline)
end event

type em_font_size from editmask within uo_font_tab
integer y = 120
integer width = 201
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "9"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##"
string minmax = "7~~99"
end type

event modified;event ue_font_size_changed(integer(text))
end event

type ddlb_fonts from dropdownpicturelistbox within uo_font_tab
integer width = 983
integer height = 380
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
string picturename[] = {"DefaultFonts!"}
integer picturewidth = 16
integer pictureheight = 16
long picturemaskcolor = 536870912
end type

event selectionchanged;event ue_font_type_changed(text)
end event

