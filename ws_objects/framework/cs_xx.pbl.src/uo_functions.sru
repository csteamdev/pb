﻿$PBExportHeader$uo_functions.sru
forward
global type uo_functions from nonvisualobject
end type
end forward

global type uo_functions from nonvisualobject
end type
global uo_functions uo_functions

type prototypes
//funzioni di estrazione nomi files da percorso -------------------------
FUNCTION Ulong FindFirstFile(ref String sPath, ref str_win32_find_data lstr_win32_find_data) LIBRARY "KERNEL32.DLL" ALIAS FOR "FindFirstFileW"

FUNCTION boolean FindNextFile(Ulong handle, ref str_win32_find_data lstr_win32_find_data) LIBRARY "KERNEL32.DLL" ALIAS FOR "FindNextFileW"

FUNCTION boolean FindClose(Ulong handle) LIBRARY "KERNEL32.DLL" ALIAS FOR "FindClose"
//----------------------------------------------------------------------------


// stefanop: 17/06/2014
FUNCTION long GetComputerName(ref string ComputerName, ref ulong BufferLength) LIBRARY "KERNEL32.DLL" ALIAS FOR "GetComputerNameW"
FUNCTION long GetUserName(ref string UserName, ref ulong BufferLength) LIBRARY "ADVAPI32.DLL" ALIAS FOR "GetUserNameW"
FUNCTION ulong GetAdaptersInfo (ref blob pAdapterInfo, ref ulong pOutBufLen) Library "iphlpapi.dll" Alias For "GetAdaptersInfo"

end prototypes

type variables

// stefanop: 04/07/2014
// Contiene la regex dei caratteri non validi per il nome file
constant string ILLEGAL_FILE_PATTERN  = "[\\/?:*'<>]"
end variables

forward prototypes
public function string uof_get_user_temp_folder ()
public function string uof_get_user_documents_folder ()
public function string uof_get_user_desktop_folder ()
public function long uof_crea_datastore (ref datastore fds_data, string fs_sql, ref string fs_errore)
public function long uof_crea_datastore (ref datastore fds_data, string fs_sql, transaction ft_trans, ref string fs_errore)
public function integer uof_replace_string (ref string fs_stringa, string fs_da_sostituire, string fs_sostitutore)
public function boolean uof_in_array (any aa_search, any aa_array[], ref long al_position)
public function boolean uof_in_array (any aa_search, any aa_array[])
public subroutine uof_explode (string as_string, string as_delimiter, ref string as_array[])
public function string uof_implode (string as_array[], string as_delimiter)
public function boolean uof_memorizza_filtro (ref datawindow adw_datawindow, string as_tipo_filtro)
public function boolean uof_leggi_filtro (ref datawindow adw_datawindow, string as_tipo_filtro)
public function long uof_crea_datastore (ref datastore fds_data, string fs_sql)
public function string uof_get_user_info (string as_key)
public function long uof_gen_num_ordine (date adt_date)
public function integer uof_get_week_number (date adt_data)
public function integer uof_blob_to_file (ref blob fb_blob, string fs_nome_file)
public function boolean uof_crea_transazione (string as_ini_section, ref transaction at_transaction, ref string as_error)
public subroutine uof_format_rte (ref string fs_value)
public function string uof_implode (string as_array[], string as_delimiter, boolean ab_exclude_empty)
public function integer uof_file_to_blob (string fs_nome_file, ref blob fb_blob)
public function boolean uof_file_write (string as_filepath, string as_message, boolean ab_append, ref string as_error)
public function integer uof_get_stabilimento_utente (ref string fs_cod_deposito, ref string fs_errore)
public function integer uof_get_stabilimento_operatore (string as_cod_operatore, ref string as_cod_deposito, ref string as_errore)
public subroutine uof_merge_arrays (ref string aa_array_to_merge[], string aa_array_to_read[])
public function boolean uof_in_array (string aa_search[], string aa_array[], ref long al_position)
public function boolean uof_in_array (string aa_search[], string aa_array[])
public function string uof_get_operatore_utente (string as_cod_utente)
public function string uof_get_operatore_utente ()
public function string uof_progressivo_alfanumerico (string as_code)
public function boolean uof_dw_has_column (ref datawindow adw_datawindow, string as_column_name)
public function string uof_nome_cella (long fl_riga, long fl_colonna)
public subroutine uof_get_value (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref string fs_tipo, oleobject fole_excel)
public function boolean uof_create_transaction_from (ref transaction at_from, ref transaction at_to, ref string as_error)
public function integer uof_get_stabilimento_operaio (string as_cod_operaio, ref string fs_cod_deposito, ref string fs_errore)
public function integer uof_get_stabilimento_utente (string as_cod_utente, ref string fs_cod_deposito, ref string fs_errore)
public function boolean uof_is_gibus ()
public subroutine uof_get_parametro (string as_cod_parametro, ref long al_numero)
public subroutine uof_get_parametro (string as_cod_parametro, ref date ad_date)
public subroutine uof_get_parametro (string as_cod_parametro, ref string as_stringa)
public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref date ad_date)
public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref string as_stringa)
public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref long al_long)
private function any uof_get_parametri (string as_tipo_tabella, string as_cod_parametro, string as_flag_parametro)
public function integer uof_tipo_tracciato_ord_acq (string fs_cod_fornitore, ref string fs_errore)
public function boolean uof_reset_filtro (string as_tipo_filtro)
public function integer uof_filigrana (string fs_path_file, ref datawindow dw_data, boolean fb_visualizza_subito, ref string fs_errore)
public function string uof_data_db (date adt_date)
public function string uof_data_db (datetime adt_date)
public subroutine uof_get_prossimo_giorno_lavorativo (date adt_date, boolean ab_feste_comandate, ref date adt_working_date)
public function integer uof_get_ultimo_giorno_mese (integer ai_mese, integer ai_anno)
public function integer uof_get_ultimo_giorno_mese (date adt_date)
public function string uof_get_operaio_utente (string as_cod_utente)
public function string uof_get_operaio_utente ()
public function boolean uof_is_supervisore (string as_cod_utente)
public function boolean uof_is_admin (string as_cod_utente)
public function long uof_substr_count (string as_text, string as_find)
public function integer uof_get_file_info (string as_file_source, ref string as_file_dir, ref string as_file_name, ref string as_file_ext)
public function date uof_string_to_date (string as_string_date)
public function integer uof_esporta_clienti_txt (string as_percorso_file, ref string as_errore)
public function integer uof_get_qrcode (ref string fs_path, string fs_valore, long fl_dim_x, long fl_dim_y, string fs_filename, ref string fs_errore)
public function string uof_get_file_attribute (string as_file_path, integer ai_attributes)
public function time uof_seconds_to_time (long al_seconds)
public function time uof_time_diff (time at_time_start, time at_time_end)
public subroutine uof_resize_max_mdi (window a_window, integer ai_resize)
public function time uof_time_diff (long al_cpu_start, long al_cpu_end)
public function boolean uof_anno_bisestile (integer ai_anno)
public function boolean uof_confronta_arrays (string aa_array_1[], string aa_array_2[])
public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref decimal ad_decimal)
public function string uof_string_to_rte (string as_text)
public function boolean uof_cancella_filtro (ref datawindow adw_datawindow, string as_tipo_filtro)
public function string uof_risorse (string as_path)
public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref boolean ab_flag)
public subroutine uof_get_parametro (string as_cod_parametro, ref boolean ab_flag)
public function boolean uof_get_date_from_giornomese (long fl_giorno_mese, integer fi_anno, ref date fdd_date)
public function string uof_get_random_filename ()
public function integer uof_setnull_fatt_proforma (integer fi_anno_ordine, long fl_num_ordine, long fl_riga_ordine, ref string fs_messaggio)
public function integer uof_setnull_fatt_proforma (integer fi_anno_ordine, long fl_num_ordine, ref string fs_messaggio)
public subroutine uof_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, string as_cod_lingua, string as_tabella_iva, ref string fs_cod_iva[], ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[], ref string fs_des_estesa[])
public function integer uof_estrai_files (ref string as_percorso_iniziale, string as_condizione, ref string as_files_estratti[], ref string as_errore)
public function integer as_reverse_array (string as_array_in[], ref string as_array_out[])
public function integer uof_loaddddw_dw (datawindow dddw_dw, string dddw_column, transaction trans_object, string table_name, string column_code, string column_desc, string join_clause, string where_clause, string groupby_clause, string orderby_clause)
public function string uof_concat_op (string as_profilo)
public function string uof_concat_op ()
public function integer uof_decrypt (string as_string, ref string as_decrypted)
public function integer uof_crypt (string as_string, ref string as_decrypted)
public function long uof_get_identity (ref n_tran at_transaction, ref long al_identity)
public function integer uof_get_cod_iva (string as_ambiente, datetime adt_data_documento, string as_cod_prodotto, string as_cod_anagrafica, string as_cod_tipo_det, ref string as_cod_iva, ref string as_message)
public function boolean uof_create_transaction_from (ref transaction at_from, ref transaction at_to, string as_servername, string as_database, ref string as_error)
public function string uof_get_computer_name ()
public function string uof_get_username ()
public function string uof_get_system_info ()
public function string uof_sanatize_filename (string as_filename)
public function integer uof_valida_iban (string as_iban, ref string as_error)
public function integer uof_load_dddw (datawindow adw_dw, string as_column_name, string as_sql)
public function long uof_crea_ds (ref datastore fds_data, string fs_sql, transaction ft_trans, ref string fs_errore)
public function boolean uof_get_zebra_printer (ref string as_current_printer, ref string as_zebra_printer)
public function boolean uof_imposta_stampante (string as_printer_name)
public function integer uof_get_offerte_web (ref string as_cod_tipo_offven[])
public function integer uof_git_test ()
public function string uof_data_db_tran (datetime adt_date, n_tran atr_transaction)
public function integer uof_get_note_fisse (string fs_cod_cliente, string fs_cod_fornitore, string fs_cod_prodotto, string fs_tipo_gestione, string fs_cod_tipo_doc, datetime fdt_data_registrazione, ref string fs_nota_testata, ref string fs_nota_piede, ref string fs_nota_video)
public function string uof_get_day_name_ita (integer as_day_number_ita)
public function integer uof_get_day_number_ita (date ad_date)
public function string uof_get_ip ()
public function long uof_get_file_prog_mimetype (string as_file_ext)
public function boolean uof_file_write_encoding (string as_filepath, string as_message, boolean ab_append, encoding a_encoding, ref string as_error)
public function long uof_string_to_date_epoc (string as_text)
public function string uof_get_file_mimetype (string as_file_ext)
end prototypes

public function string uof_get_user_temp_folder ();/**
 * stefanop
 * 04/10/2011
 *
 * Recupero la cartella temporanea dell'utente
 * La cartella avrà sicuramente i permessi di lettura/scrittura
 *
 * return string C:\Users\XXX\AppData\Local\Temp\csteam\
 **/

String ls_temppath

try
	
	ls_temppath = Space(255)
	GetTempPath(255 ,ls_temppath)

	ls_temppath += "csteam\"
	
	if not DirectoryExists(ls_temppath) then
		CreateDirectory(ls_temppath)
	end if
	
catch (RuntimeError ex)
	setnull(ls_temppath)
end try

return ls_temppath
end function

public function string uof_get_user_documents_folder ();/**
 * stefanop
 * 04/10/2011
 *
 * Recupera il percorso dei documenti dell'utente collegato alla macchina
 **/
 
string ls_document_folder

if registryget("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Personal", ls_document_folder) = -1 then
	
	// Provo nella vecchia maniera
	ls_document_folder  = uof_get_user_info("HOMEDRIVE")
	ls_document_folder += uof_get_user_info("HOMEPATH")
	ls_document_folder += "\Documenti\"
	
else 
	ls_document_folder += "\"
end if


return ls_document_folder
end function

public function string uof_get_user_desktop_folder ();/**
 * stefanop
 * 04/10/2011
 *
 * Recupera il percorso del desktop dell'utente collegato alla macchina
 **/
 
string ls_document_folder

if registryget("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Desktop", ls_document_folder) = -1 then
	setnull(ls_document_folder)
else 
	ls_document_folder += "\"
end if

//ls_document_folder  = uof_get_user_info("HOMEDRIVE")
//ls_document_folder += uof_get_user_info("HOMEPATH")
//ls_document_folder += "\Desktop\"

return ls_document_folder
end function

public function long uof_crea_datastore (ref datastore fds_data, string fs_sql, ref string fs_errore);return uof_crea_datastore(fds_data, fs_sql, sqlca, fs_errore)

end function

public function long uof_crea_datastore (ref datastore fds_data, string fs_sql, transaction ft_trans, ref string fs_errore);string ls_error
long ll_ret

fs_sql = ft_trans.syntaxfromsql(fs_sql, "style(type=grid)", ls_error)
if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	fs_errore = "Impossibile convertire stringa SQL per il datastore.~r~n" + ls_error
	return -1
end if

fds_data = create datastore
fds_data.create(fs_sql, ls_error)
if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	destroy fds_data;
	fs_errore = "Impossibile creare datastore.~r~n" + ls_error
	return -1
end if

fds_data.settransobject(ft_trans)

ll_ret = fds_data.retrieve()

if ll_ret < 0 then
	fs_errore = "Errore durante la retrieve del datastore.~r~n" + fs_sql
end if

return ll_ret

end function

public function integer uof_replace_string (ref string fs_stringa, string fs_da_sostituire, string fs_sostitutore);/**
 * Funzione di sostituzione
 * Rimpiazza tutte le occorenze di fs_da_sostituire in fs_stringa con fs_sostitutore
 **/

integer li_lunghezza_formula,li_t,li_posizione,li

if isnull(fs_stringa) or fs_stringa = "" then return 0

if isnull(fs_sostitutore) then fs_sostitutore = ''

li_lunghezza_formula = len(fs_stringa)

if li_lunghezza_formula=0 then return 0

li_posizione = 1

do while 1=1
	li_posizione=pos(fs_stringa,fs_da_sostituire,li_posizione)
	if li_posizione=0 then exit	
	fs_stringa = replace ( fs_stringa, li_posizione, len(fs_da_sostituire), fs_sostitutore ) 
	li_posizione += len(fs_sostitutore)
loop


return 0
end function

public function boolean uof_in_array (any aa_search, any aa_array[], ref long al_position);/**
 * stefanop
 * 18/10/2011
 *
 * Controllo se un determinato valore esiste all'interno di un array e ne ritorno la posizione
 * Ritorna TRUE se l'elemento è presente e FALSE se non è stato trovato
 **/
 
long ll_i

al_position = -1

for ll_i = 1 to upperbound(aa_array)
	
	if aa_array[ll_i] = aa_search then
		al_position = ll_i
		return true
	end if
	
next

return false
end function

public function boolean uof_in_array (any aa_search, any aa_array[]);long ll_position
return uof_in_array(aa_search, aa_array, ll_position)
end function

public subroutine uof_explode (string as_string, string as_delimiter, ref string as_array[]);/**
 * stefanop
 * 18/10/2011
 *
 * Divide una stringa in base al separatore passato
 **/

long ll_pos = 1, ll_delimiter_length

// la stringa deve esistere
if len(as_string) < 1 then return

ll_delimiter_length = len(as_delimiter)

do while true
	
	ll_pos = pos(as_string, as_delimiter, 1)
	if ll_pos = 0 then
		// appendo anche l'ultima occorrenza se valida prima di uscire
		if not isnull(as_string) then as_array[upperbound(as_array)+1] = as_string
		exit
	end if
	
	as_array[upperbound(as_array)+1] = left(as_string, ll_pos - 1)
	as_string = mid(as_string, ll_pos + ll_delimiter_length)
	
loop
end subroutine

public function string uof_implode (string as_array[], string as_delimiter);/**
 * stefanop
 * 18/10/2011
 *
 * Unisce un array di stringe separandole da un delimitatore
 **/

return uof_implode(as_array, as_delimiter, false)
end function

public function boolean uof_memorizza_filtro (ref datawindow adw_datawindow, string as_tipo_filtro);/**
 * stefanop
 * 28/10/2011
 *
 * Centrallizzata la funzione che permette di salvare i valori impostati nella datawindow
 * all'interno del database
 **/
 
string ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero,ll_cont
date ldt_data
datetime ldtt_data

if not g_mb.confirm("Memorizza l'attuale impostazione dei filtro come predefinito?")  then 
	return true
end if

adw_datawindow.accepttext()
ll_colonne = long(adw_datawindow.Object.DataWindow.Column.Count)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then ls_cod_utente = "SYSTEM"


ls_memo = ""
for ll_i = 1 to ll_colonne
	ls_nome_colonna = adw_datawindow.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = adw_datawindow.describe("#"+string(ll_i)+".coltype")

	choose case lower(left(ls_tipo_colonna, 5))
			
		case "datet"
			ldtt_data = adw_datawindow.getitemdatetime(1, ls_nome_colonna)
			if isnull(ldtt_data) then
				ls_memo += "NULL~t"
			else
				ls_memo += string(ldtt_data, "dd/mm/yyyy") + "~t"
			end if
			
		case "date"
			ldt_data = adw_datawindow.getitemdate(1, ls_nome_colonna)
			if isnull(ldt_data) then
				ls_memo += "NULL~t"
			else
				ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
			end if
			
		case "char("
			ls_stringa = adw_datawindow.getitemstring(1, ls_nome_colonna)
			if isnull(ls_stringa) then
				ls_memo += "NULL~t"
			else
				ls_memo += ls_stringa + "~t"
			end if
			
		case "numbe", "long", "decim", "int"
			ll_numero = adw_datawindow.getitemnumber(1, ls_nome_colonna)
			if isnull(ll_numero) then
				ls_memo += "NULL~t"
			else
				ls_memo += string(ll_numero) + "~t"
			end if
			
	end choose
	
next

select count(*)
into :ll_cont
from filtri_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_utente =  :ls_cod_utente and
	tipo_filtro = :as_tipo_filtro;
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente =  :ls_cod_utente and
		tipo_filtro = :as_tipo_filtro;
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore in memorizzazione impostazioni predefinite filtri di ricerca.", sqlca)
		rollback;
		return false
	end if
else
	insert into filtri_manutenzioni (
		cod_azienda,
		cod_utente,
		filtri,
		tipo_filtro
	) values (
		:s_cs_xx.cod_azienda,
		:ls_cod_utente,
		:ls_memo,
		:as_tipo_filtro);
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore in memorizzazione impostazioni predefinite filtri di ricerca.", sqlca)
		rollback;
		return false
	end if
end if

commit; 
return true
end function

public function boolean uof_leggi_filtro (ref datawindow adw_datawindow, string as_tipo_filtro);/**
 * stefanop
 * 28/10/2011
 *
 * Centrallizzata la funzione che permette di leggere i valori impostati nella datawindow
 * all'interno del database
 **/
 
string ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero
date ldt_data
datetime ldtt_data

ll_colonne = long(adw_datawindow.Object.DataWindow.Column.Count)
ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then ls_cod_utente = "SYSTEM"

ls_memo = ""

select filtri 
into :ls_memo
from filtri_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_utente =  :ls_cod_utente and
	tipo_filtro = :as_tipo_filtro;

if sqlca.sqlcode = 100 then
	//g_mb.show("OMNIA", "Nessun filtro memorizzato.")
	return true
elseif sqlca.sqlcode < 0 then
	g_mb.error("Errore in lettura impostazioni predefinite filtri di ricerca.", sqlca)
	return false
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = adw_datawindow.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = adw_datawindow.describe("#"+string(ll_i)+".coltype")
	
	choose case lower(left(ls_tipo_colonna, 5))
			
		case "datet"
			if ls_stringa = "NULL" then
				setnull(ldtt_data)
			else
				ldtt_data = datetime(date(ls_stringa), 00:00:00)
			end if
			adw_datawindow.setitem(1,ls_nome_colonna, ldtt_data)
					
		case "date"
			if ls_stringa = "NULL" then
				setnull(ldt_data)
			else
				ldt_data = date(ls_stringa)
			end if
			adw_datawindow.setitem(1,ls_nome_colonna, ldt_data)
						
		case "char("
			if ls_stringa = "NULL" then
				setnull(ls_stringa)
			end if
			adw_datawindow.setitem(1,ls_nome_colonna, ls_stringa)
			
		case "numbe", "long", "decim", "int"
			if ls_stringa = "NULL" then
				setnull(ll_numero)
			else
				ll_numero = long(ls_stringa)
			end if
			adw_datawindow.setitem(1,ls_nome_colonna, ll_numero)
					
	end choose
next

return true
end function

public function long uof_crea_datastore (ref datastore fds_data, string fs_sql);string ls_error

return uof_crea_datastore(fds_data, fs_sql, sqlca, ls_error)
end function

public function string uof_get_user_info (string as_key);/**
 * stefanop
 * 04/10/2011
 *
 * Recupera le informazioni dalle variabili di ambiente del sistema corrente
 **/
 
 
// Ritorna le variabili d'ambiente del sistema
string ls_Path
string ls_values[]
ContextKeyword lcxk_base

GetContextService("Keyword", lcxk_base)

lcxk_base.GetContextKeywords(as_key, ls_values)
IF Upperbound(ls_values) > 0 THEN
   ls_Path = ls_values[1]
ELSE
   ls_Path = "*UNDEFINED*"
END IF

return ls_Path
end function

public function long uof_gen_num_ordine (date adt_date);/**
 * stefanop
 * 10/11/2011
 *
 * Calcolo il progressivo in base alla data passata
 * Il progressivo sarà composto da
 * 2 cifre che indicato la settimana dell'anno
 * 1 cifra il giorno della settimana
 * 3 cifre il progressivo di quel giorno
 **/
 
 /*
 EnMe 12/03/2018 aggiunto il parametro flag_tipo_numerazione in tabella con_ord_ven
 Valore PArametro:
1 = allora numerazione progressiva normale progressiva
2 = numerazione pgoressiva con recupero buchi
3 = numerazione tipo Settimana/Giorno/Progressivo (stile Gibus)
 */
 
int 			li_week, li_week_day, li_anno_esercizio
long 			ll_min, ll_max, ll_num_registrazione, ll_cont, ll_i, ll_num_reg_prec, ll_num_reg_buco
string			ls_flag_tipo_numerazione, ls_sql
date 			ld_januaryfirst
datastore 	lds_prog

select 	flag_tipo_numerazione
into	 	:ls_flag_tipo_numerazione
from 		con_ord_ven
where 	cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode < 0 or isnull(ls_flag_tipo_numerazione) then ls_flag_tipo_numerazione = '3'

li_anno_esercizio = f_anno_esercizio()

choose case ls_flag_tipo_numerazione
		
	case "1"
		select max(num_registrazione)
		into	:ll_num_registrazione
		from	tes_ord_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :li_anno_esercizio;
		if sqlca.sqlcode < 0 or ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
			ll_num_registrazione = 1
		else
			ll_num_registrazione ++
		end if
		return ll_num_registrazione
		
	case "2"
		ll_num_reg_prec = 0
		ll_num_reg_buco = 0
		ls_sql = g_str.format("select num_registrazione from tes_ord_ven where cod_azienda = '$1' and anno_registrazione = $2 order by num_registrazione DESC ",s_cs_xx.cod_azienda, li_anno_esercizio)
		ll_cont =uof_crea_datastore(lds_prog, ls_sql)
		for ll_i = 1 to ll_cont
			if ll_i = 1 then
				ll_num_reg_prec = lds_prog.getitemnumber(ll_i,1)
				continue
			else
				ll_num_registrazione = lds_prog.getitemnumber(ll_i,1)
				if ll_num_reg_prec - ll_num_registrazione > 1 then
					// trovato buco
					ll_num_reg_buco = ll_num_reg_prec - 1
					exit
				else
					ll_num_reg_prec = ll_num_registrazione
				end if
			end if
		next		
		
		if ll_num_reg_buco = 0 then // nessun buco trovato, vado avanti con progressivo
			select max(num_registrazione)
			into	:ll_num_registrazione
			from	tes_ord_ven
			where	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :li_anno_esercizio;
			if sqlca.sqlcode < 0 or ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
				ll_num_registrazione = 1
			else
				ll_num_registrazione ++
			end if
			return ll_num_registrazione
		else
			return ll_num_reg_buco
		end if
		
	case "3"
		li_week_day = daynumber(adt_date) - 1
		if li_week_day = 0 then li_week_day = 7
		li_week = uof_get_week_number(adt_date)
		
		 ll_min =  long( string(li_week) + string(li_week_day) + "000")
		 ll_max = long( string(li_week) + string(li_week_day) + "999")
		 
		do while true
			 select max(num_registrazione)
			 into :ll_num_registrazione
			 from tes_ord_ven
			 where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_esercizio and
				num_registrazione > :ll_min and
				num_registrazione < :ll_max ;
				
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore durante il calcolo del progressivo degli ordini", sqlca)
				return -1
			elseif isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
				ll_num_registrazione = long( string(li_week) + string(li_week_day) + "001")
			else
				ll_num_registrazione++
			end if
			
			select	count(*)
			into 	:ll_cont
			from 	tes_ord_ven
			where
					cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :li_anno_esercizio and
					num_registrazione = :ll_num_registrazione;
			
			if ll_cont = 0 then
				exit
			else
				ll_max --
				if ll_max = ll_min then 
					// non ci sono buchi liberi
					g_mb.messagebox("Apice", "Attenzione ... non ci sono numerazioni libere nella settimana / giorno ")
					exit
				end if
			end if
		
		loop
		return ll_num_registrazione
end choose
end function

public function integer uof_get_week_number (date adt_data);Integer li_year, li_prev_year
Integer li_day, li_week

li_day = DayNumber(adt_data) - 1 // giorno italiano
if li_day = 0 then li_day = 7 // domenica


DO WHILE li_day <> 4 
IF li_day < 4 THEN 
   // Mon - Wed. Go to next Thur.
   adt_data = RelativeDate(adt_data, 1)
ELSE
   // Thur - Sun. Go to prev Thur.
   adt_data = RelativeDate(adt_data, -1)
END IF
	li_day = DayNumber(adt_data) - 1
	if li_day = 0 then li_day = 7 // domenica
LOOP

li_year = Year(adt_data)


// Count weeks backwards until the
// previous year
DO
   li_week++
   adt_data = RelativeDate(adt_data, -7)
   li_prev_year = Year(adt_data)
LOOP UNTIL li_year <> li_prev_year

Return li_week
end function

public function integer uof_blob_to_file (ref blob fb_blob, string fs_nome_file);/**
 * Salva il blob all'interno di un file
 **/
 
integer li_numero_file, li_count, i

long ll_lunghezza_blob, ll_pos, ll_quantita
blob lb_blob

ll_lunghezza_blob = Len(fb_blob)
li_numero_file = FileOpen(fs_nome_file, StreamMode!, Write!, LockWrite!, Replace!)
if li_numero_file = -1 then return -1

if ll_lunghezza_blob > 32765 then
	if Mod(ll_lunghezza_blob, 32765) = 0 then
		li_count = ll_lunghezza_blob/32765
	else
		li_count = (ll_lunghezza_blob/32765) + 1
	end if
else
	li_count = 1
end if

for i = 1 to li_count
	ll_pos = (i - 1) * 32765 + 1
	if i = li_count then
		ll_quantita = mod(ll_lunghezza_blob, 32765)
	else
		ll_quantita = 32765
	end if
	lb_blob = BlobMid(fb_blob, ll_pos, ll_quantita)
	FileWrite(li_numero_file, lb_blob)
next
FileClose(li_numero_file)
return 0
 
return 0
end function

public function boolean uof_crea_transazione (string as_ini_section, ref transaction at_transaction, ref string as_error);/**
 * Stefano Pulze
 * 25/11/2011
 *
 * Leggo i paramentri dal registro e imposto la transazione,
 * il codice è copiato da quello usato dal client/server
 */

string ls_logpass, ls_option, ls_reg_chiave_root = "HKEY_LOCAL_MACHINE\SOFTWARE\Consulting&Software\"
int li_risposta, ll_ret

at_transaction = create transaction

// -- Leggo i dati dal registro
li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "servername", at_transaction.ServerName)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: servername."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "dbms", at_transaction.DBMS)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: dbms."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "database", at_transaction.Database)
if li_risposta = -1 then
	as_error = "Mancano le impostazione del database sul registro: database."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "logid", at_transaction.LogId)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: logid."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "logpass", ls_logpass)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: logpass."
	return false
end if

li_risposta = Registryset(ls_reg_chiave_root + "profilocorrente", "appo", "1.1")		 

if at_transaction.DBMS <> "ODBC" then
	
	if g_str.isnotempty(ls_logpass) then
//	if isnull(ls_logpass) or ls_logpass="" then		
//		
//		as_error = "Manca la password per l'accesso al database è necessario impostarla ora altrimenti non è possibile accedere al sistema."
//		return false
//		
//	end if	
		
		n_cst_crypto luo_crypto
		luo_crypto = create n_cst_crypto
		ll_ret = luo_crypto.decryptdata( ls_logpass, ls_logpass)  
		destroy luo_crypto;
		if ll_ret < 0 then
			as_error = "La password contiene caratteri non consentiti.I caratteri consentiti comprendono:" + &
						"- tutte le cifre numeriche 0,1,2,...,9  tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z" + &
						"- alcuni simboli.Modificare la password e riprovare"
			return false
		end if
	
		at_transaction.LogPass = ls_logpass
	end if
	
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "userid", at_transaction.UserId)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: userid."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "dbpass", at_transaction.DBPass)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: dbpass."
	return false
end if

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "dbparm", at_transaction.DBParm)
if li_risposta = -1 then
	as_error = "Mancano le impostazione del database sul registro: dbparm."
	return false
end if

// QUANDO VA USATO???
//li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "option", ls_option)
//if li_risposta = -1 then
//	as_error = "Mancano le impostazione del database sul registro: option."
//	return false
//end if
//
//at_transaction.dbparm = at_transaction.dbparm + ls_option

li_risposta = registryget(ls_reg_chiave_root + as_ini_section, "lock", at_transaction.Lock)
if li_risposta = -1 then
	as_error = "Mancano le impostazioni del database sul registro: lock."
	return false
end if

li_risposta = Registryset(ls_reg_chiave_root + "profilocorrente", "appo", "1.2")		 

disconnect using at_transaction;
connect using at_transaction;
if at_transaction.sqlcode <> 0 then
	as_error = "Errore durante la connessione al database~r~n" + at_transaction.sqlerrtext
	return false
end if

return true
end function

public subroutine uof_format_rte (ref string fs_value);string ls_rte_format

if isnull(fs_value) then fs_value = ""

ls_rte_format = &
"{\rtf1\ansi\ansicpg1252\uc1\deff0{\fonttbl&
{\f0\fnil\fcharset0\fprq2 Arial;}&
{\f1\fswiss\fcharset0\fprq2 Tahoma;}&
{\f2\froman\fcharset2\fprq2 Symbol;}}&
{\colortbl;\red0\green0\blue0;\red255\green255\blue255;}&
{\stylesheet{\s0\itap0\nowidctlpar\f0\fs24 [Normal];}{\*\cs10\additive Default Paragraph Font;}}&
{\*\generator TX_RTF32 15.0.530.503;}&
\deftab1134\paperw11905\paperh16838\margl0\margt0\margr0\margb0\widowctrl\formshade&
{\*\background{\shp{\*\shpinst\shpleft0\shptop0\shpright0\shpbottom0\shpfhdr0\shpbxmargin\shpbxignore\shpbymargin\shpbyignore\shpwr0\shpwrk0\shpfblwtxt1\shplid1025{\sp{\sn shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn fillColor}{\sv 16777215}}{\sp{\sn fFilled}{\sv 1}}{\sp{\sn lineWidth}{\sv 0}}{\sp{\sn fLine}{\sv 0}}{\sp{\sn fBackground}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 1}}}}}\sectd&
\headery720\footery720\pgwsxn6750\pghsxn16838\marglsxn0\margtsxn0\margrsxn0\margbsxn0\pard\itap0\nowidctlpar\plain\f1\fs20 "

fs_value = ls_rte_format + fs_value + "}"

return
end subroutine

public function string uof_implode (string as_array[], string as_delimiter, boolean ab_exclude_empty);/**
 * stefanop
 * 18/10/2011
 *
 * Unisce un array di stringe separandole da un delimitatore
 *
 * Parametri:
 *		as_array[] = array delle stringhe da unire
 *		as_delimiter = delimitatore per l'unione
 *		ab_exclude_empty = se TRUE non prende in considerazione le stringhe vuote
 **/

string ls_result
long ll_i, ll_count

ll_count = upperbound(as_array)

// se maggiore di due allora necessita del delimitatore
if ll_count > 1 then
	
	for ll_i = 1 to ll_count -1
		if isnull(as_array[ll_i]) or as_array[ll_i] = "" then
			if ab_exclude_empty then
				continue
			else
				as_array[ll_i] = ""
			end if
		end if
		
		ls_result += as_array[ll_i] + as_delimiter
	next
	
	if isnull(as_array[ll_i]) or as_array[ll_i] = "" then as_array[ll_count] = ""

	if ab_exclude_empty and len(as_array[ll_count]) < 1 then
	else
		ls_result += as_array[ll_count]
	end if
	
	return ls_result

// se un elemento solo allora non serve il delimitatore e nemmeno il ciclo
elseif ll_count = 1 then
	
	if ab_exclude_empty and len(as_array[1]) < 1 then
		return ""
	else
		return as_array[1]
	end if
	
// se nessun elemento allora non serve niente
elseif ll_count < 1 then
	return ""
end if
end function

public function integer uof_file_to_blob (string fs_nome_file, ref blob fb_blob);//Funzione che legge un file e lo mette in una variabile di tipo blob passata per referenza
//(Tiene conto delle dimensioni)

integer li_numero_file, li_count, i

long ll_lunghezza_file, ll_byte_letti
blob lb_blob, lb_tot_blob

if not fileexists(fs_nome_file) then return -1
ll_lunghezza_file = FileLength(fs_nome_file)
li_numero_file = FileOpen(fs_nome_file, StreamMode!, Read!, LockRead!)
if li_numero_file = -1 then return -1

if ll_lunghezza_file > 32765 then
	if Mod(ll_lunghezza_file, 32765) = 0 then
		li_count = ll_lunghezza_file/32765
	else
		li_count = (ll_lunghezza_file/32765) + 1
	end if
else
	li_count = 1
end if

for i = 1 to li_count
	ll_byte_letti = FileRead(li_numero_file, lb_blob)
	if ll_byte_letti = -1 then return -1
	lb_tot_blob = lb_tot_blob + lb_blob
next
FileClose(li_numero_file)
fb_blob = lb_tot_blob
return 0
end function

public function boolean uof_file_write (string as_filepath, string as_message, boolean ab_append, ref string as_error);return uof_file_write_encoding( as_filepath, as_message, ab_append,  EncodingANSI!,as_error)


/**
 * stefanop
 * 29/11/2011
 *
 * Funzione che scrive all'interno di un file la stringa passata per parametro
 
int li_handle, li_risposta
 
if ab_append then
	li_handle = fileopen(as_filepath, linemode!, Write!, LockWrite!, Append!)
else
	li_handle = fileopen(as_filepath, linemode!, Write!, LockWrite!, Replace!)
end if

if li_handle = -1 then
	as_error = "Errore durante l'apertura del file " + as_filepath + ".~r~nVerificare le connessioni delle unità di rete"
	return false
end if

li_risposta = filewrite(li_handle, as_message)
fileclose(li_handle)

return li_risposta >= 0
*/
end function

public function integer uof_get_stabilimento_utente (ref string fs_cod_deposito, ref string fs_errore);/**
 * stefanop
 * 04/10/2011
 *
 * Recupera lo stabilimento di default di un utente-operatore (tabella tab_operatori_utenti)
 		se per caso ce nè più di uno di default torna il primo che becca
 **/
 
return uof_get_stabilimento_utente(s_cs_xx.cod_utente, fs_cod_deposito, fs_errore)
end function

public function integer uof_get_stabilimento_operatore (string as_cod_operatore, ref string as_cod_deposito, ref string as_errore);/**
 * stefanop
 * 21/12/2011
 *
 * Recupera lo stabilimento di default di un utente-operatore (tabella tab_operatori_utenti)
 *	se per caso ce nè più di uno di default torna il primo che becca
 **/
 
string			ls_sql
datastore	lds_data
long			ll_ret

setnull(as_cod_deposito)

//se sei CS_SYSTEM torna vuoto
if s_cs_xx.cod_utente = "CS_SYSTEM" then return 0

ls_sql = "select cod_deposito "+&
			"from tab_operatori_utenti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_operatore='"+as_cod_operatore+"' and "+&
						"flag_default='S' "

ll_ret = uof_crea_datastore(lds_data, ls_sql, as_errore)

choose case ll_ret
	case is < 0
		//in fs_errore il messaggio
		return -1
		
	case is > 0
		//prendo la prima riga
		as_cod_deposito = lds_data.getitemstring(1, "cod_deposito")
		if trim(as_cod_deposito)="" then setnull( as_cod_deposito)
		
		return 0
		
	case else
		//deposito non specificato (torna NULL)
		return 0
		
end choose

end function

public subroutine uof_merge_arrays (ref string aa_array_to_merge[], string aa_array_to_read[]);long ll_position
any la_value

for ll_position = 1 to upperbound(aa_array_to_read[])
	la_value = aa_array_to_read[ll_position]
	
	if not uof_in_array(la_value, aa_array_to_merge[]) then
		//non esiste, inserisci
		aa_array_to_merge[upperbound(aa_array_to_merge[]) + 1] = la_value
	end if
next
end subroutine

public function boolean uof_in_array (string aa_search[], string aa_array[], ref long al_position);/**
 * stefanop
 * 18/10/2011
 *
 * Controllo se un determinato valore esiste all'interno di un array e ne ritorno la posizione
 * Ritorna TRUE se l'elemento è presente e FALSE se non è stato trovato
 **/
 
long ll_i, ll_y

al_position = -1

for ll_i = 1 to upperbound(aa_search)
	
	for ll_y = 1 to upperbound(aa_array)
	
		if aa_search[ll_i] = aa_array[ll_y] then
			al_position = ll_y
			return true
		end if
		
	next
	
next

return false
end function

public function boolean uof_in_array (string aa_search[], string aa_array[]);long ll_pos

return uof_in_array(aa_search[], aa_array[], ll_pos)
end function

public function string uof_get_operatore_utente (string as_cod_utente);/**
 * stefanop
 * 25/01/2012
 *
 * Recupero l'operatore associato all'utentepassato per parametro
 **/
 
string ls_sql, ls_cod_operatore
long ll_row
datastore lds_store

ls_sql = "SELECT cod_operatore FROM tab_operatori_utenti WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' AND cod_utente='" + as_cod_utente + "'"
ls_sql += " ORDER BY flag_default"

ll_row = uof_crea_datastore(lds_store, ls_sql)

if ll_row < 0 then
	setnull(ls_cod_operatore)
else
	
	ls_cod_operatore = lds_store.getitemstring(1,1)
	
	if ls_cod_operatore = "" then setnull(ls_cod_operatore)
	
end if

return ls_cod_operatore
end function

public function string uof_get_operatore_utente ();/**
 * stefanop
 * 25/01/2012
 *
 * Recupero l'operatore associato all'utentepassato per parametro
 **/
 
return uof_get_operatore_utente(s_cs_xx.cod_utente)
end function

public function string uof_progressivo_alfanumerico (string as_code);/**
 * stefanop
 * 20/04/2009
 *
 * La funzione crea un progressivo alfanumerico a partire dalla stringa passata
 * Esempio di successione: 0,1,2,3,4,5,6,7,8,9,A,B,C,D,...,96,97,98,99,9A,9B,9C
 */
 
integer ll_i, ll_max
string ls_return, ls_char, ls_appo[]
	
ll_max = len(as_code)
if ll_max <=0 then return ""

ls_return = as_code

//carica un array con i caratteri della stringa
for ll_i = 1 to ll_max
	ls_appo[ll_i] = mid(ls_return, ll_i, 1)
next

for ll_i = ll_max to 1 step -1
	//preleva i caratteri a partire da destra
	ls_char = ls_appo[ll_i]
	
	choose case ls_char
		case "9"
			//fallo diventare "A"
			ls_appo[ll_i] = "A"
			exit
			
		case "Z"
			//metti il carattere corrente a "0"
			ls_appo[ll_i] = "0"
			
			//vai al successivo carattere spostandoti verso sinistra			
			
		case else
			//incrementa il suo codice ascii
			ls_appo[ll_i] = char(asc(ls_char) + 1)
			exit
			
	end choose
next

ls_return = ""
for ll_i = 1 to ll_max
	ls_return +=  ls_appo[ll_i]
next

return ls_return
end function

public function boolean uof_dw_has_column (ref datawindow adw_datawindow, string as_column_name);/**
 * stefanop
 * 30/01/2012
 *
 * Controllo che la colonna esista all'interno della datawindow passata
 **/
 
string ls_describe

ls_describe = adw_datawindow.describe(as_column_name + ".ColType")

return (ls_describe <> "!")
end function

public function string uof_nome_cella (long fl_riga, long fl_colonna);/*Questa funzione restituisce il nome di una cella di un foglio excel se gli passi il numero riga ed il numero colonna
lettere dell'alfabeto completo

Viene usata dalla funzione get value di questo stesso oggetto
*/

string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if fl_colonna > 26 then
	ll_resto = mod(fl_colonna, 26)
	ll_i = long(fl_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	fl_colonna = ll_resto 
end if

if fl_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[fl_colonna] + string(fl_riga)
end if	

return ls_char
end function

public subroutine uof_get_value (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref string fs_tipo, oleobject fole_excel);OLEObject		lole_foglio
any				lany_ret
string ls_1

//legge dalla cella (fl_riga, fl_colonna) del foglio di lavoro fs_foglio
/*
prova prima con valore decimale: 	se ok torna il valore in fd_valore e mette fs_tipo="D"
poi prova con valore string:				se ok torna il valore in fs_valore e mette fs_tipo="S"

se proprio non riesce a leggere mette a fs_tipo=NULL e ritorna
*/

fs_tipo = ""
setnull(fd_valore)
setnull(fs_valore)

lole_foglio = fole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)
ls_1 = uof_nome_cella(fl_riga, fl_colonna)

//lany_ret = lole_foglio.range(wf_nome_cella(fl_riga, fl_colonna) + ":" + wf_nome_cella(fl_riga , fl_colonna) ).value
lany_ret = lole_foglio.cells[fl_riga,fl_colonna].value

if isnull(lany_ret) then 
	fs_tipo = "S"
	setnull(fs_valore)
	return
end if

try
	//prova con valore stringa
	fs_valore = lany_ret
	fs_tipo = "S"
catch (RuntimeError rte2)
	setnull(fs_valore)
	setnull(fs_tipo)
end try


//try
//	//prova con valore decimale
//	fd_valore = lany_ret
//	fs_tipo = "D"
//catch (RuntimeError rte1)
//	
//	setnull(fd_valore)
//	setnull(fs_tipo)
//	try
//		//prova con valore stringa
//		fs_valore = lany_ret
//		fs_tipo = "S"
//	catch (RuntimeError rte2)
//		setnull(fs_valore)
//		setnull(fs_tipo)
//	end try
//	
//end try
end subroutine

public function boolean uof_create_transaction_from (ref transaction at_from, ref transaction at_to, ref string as_error);/**
 * stefanop
 * 10/02/2012
 *
 * Imposta i parametri di una nuova conessione recuperandoli dalla transazione passata
 **/


string ls_db,ls_logpass
integer li_risposta

at_to = create transaction

at_to.ServerName = at_from.ServerName
at_to.LogId = at_from.LogId
at_to.DBMS = at_from.DBMS
at_to.Database = at_from.Database
at_to.UserId = at_from.UserId
at_to.LogPass = at_from.LogPass
at_to.DBPass = at_from.DBPass
at_to.dbparm = at_from.dbparm
at_to.Lock = at_from.Lock

connect using at_to;

if at_to.sqlcode = 0 then
	return true
else
	as_error = at_to.sqlerrtext
	return false
end if
end function

public function integer uof_get_stabilimento_operaio (string as_cod_operaio, ref string fs_cod_deposito, ref string fs_errore);/**
 * stefanop
 * 14/02/2012
 **/
 
string			ls_cod_utente


setnull(fs_cod_deposito)

select cod_utente
into :ls_cod_utente
from anag_operai
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_operaio = :as_cod_operaio;
	
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in ricerca del codice utente dall'anagrafica Operai.~r~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_cod_utente) or ls_cod_utente = "" then
	fs_errore = "L'operaio non ha nessun utente collegato."
	return 0
end if

return uof_get_stabilimento_utente(ls_cod_utente, fs_cod_deposito, fs_errore)
end function

public function integer uof_get_stabilimento_utente (string as_cod_utente, ref string fs_cod_deposito, ref string fs_errore);/**
 * stefanop
 * 04/10/2011
 *
 * Recupera lo stabilimento di default di un utente-operatore (tabella tab_operatori_utenti)
 		se per caso ce nè più di uno di default torna il primo che becca
 **/
 
string			ls_sql
datastore	lds_data
long			ll_ret

setnull(fs_cod_deposito)

//se sei CS_SYSTEM torna vuoto
if s_cs_xx.cod_utente = "CS_SYSTEM" then return 0

ls_sql = "select cod_deposito "+&
			"from tab_operatori_utenti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_utente='"+ as_cod_utente+"' and "+&
						"flag_default='S' "

ll_ret = uof_crea_datastore(lds_data, ls_sql, fs_errore)

choose case ll_ret
	case is < 0
		//in fs_errore il messaggio
		return -1
		
	case is > 0
		//prendo la prima riga
		fs_cod_deposito = lds_data.getitemstring(1, "cod_deposito")
		if trim(fs_cod_deposito)="" then setnull( fs_cod_deposito)
		
		return 0
		
	case else
		//deposito non specificato (torna NULL)
		return 0
		
end choose

end function

public function boolean uof_is_gibus ();/**
 * stefanop
 * 14/02/2012
 *
 * Controllo se sono nell'azienda GIBUS
 **/
 
 
string ls_flag

select flag
into :ls_flag
from parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_parametro = 'GIB' and
	flag_parametro = 'F';
	
if ls_flag = "S"  then
	return true
else
	return false
end if
	

end function

public subroutine uof_get_parametro (string as_cod_parametro, ref long al_numero);al_numero = long(uof_get_parametri("M", as_cod_parametro, "N"))
end subroutine

public subroutine uof_get_parametro (string as_cod_parametro, ref date ad_date);ad_date = date(uof_get_parametri("M", as_cod_parametro, "D"))
end subroutine

public subroutine uof_get_parametro (string as_cod_parametro, ref string as_stringa);as_stringa = string(uof_get_parametri("M", as_cod_parametro, "S"))
end subroutine

public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref date ad_date);ad_date = date(uof_get_parametri("A", as_cod_parametro, "D"))
end subroutine

public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref string as_stringa);as_stringa = string(uof_get_parametri("A", as_cod_parametro, "S"))
end subroutine

public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref long al_long);al_long = long(uof_get_parametri("A", as_cod_parametro, "N"))
end subroutine

private function any uof_get_parametri (string as_tipo_tabella, string as_cod_parametro, string as_flag_parametro);/**
 * stefanop
 * 17/02/2012
 *
 * Funzione generica che recupera i parametri dallle tabelle di configurazione
 * Parametri:
 * 		as_tipo_tabella = (M)ultiaziendale, (A)ziendale, (O)mnia, (U)tente
 *		as_flag_parametro = (S)tringa, (N)umero, (D)ata
 **/
 
string ls_sql, ls_where
any la_any
datastore lds_store

// Compongo SQL
ls_sql = "SELECT "

choose case as_flag_parametro
	case "S"
		ls_sql += " stringa "
		
	case "N", "DEC"
		ls_sql += " numero "
		
	case "D"
		ls_sql += " data "
	
	case "F"
		ls_sql += " flag "
end choose

// stefanop: 20/09/2012: fix
if as_flag_parametro = "DEC" then as_flag_parametro = "N"

ls_sql += " FROM "
ls_where = " WHERE cod_parametro='" + as_cod_parametro + "' AND flag_parametro='" + as_flag_parametro + "' "

choose case as_tipo_tabella
	case "M"
		ls_sql += " parametri "
		
	case "A"
		ls_sql += " parametri_azienda "
		ls_where += " AND cod_azienda='" + s_cs_xx.cod_azienda + "'"
		
	case "U"
		ls_sql += " parametri_azienda_utente "
		ls_where += " AND cod_azienda='" + s_cs_xx.cod_azienda + "' AND cod_utente='" + s_cs_xx.cod_utente + "'"
		
	case "O"
		ls_sql += " parametri_omnia "
		ls_where += " AND cod_azienda='" + s_cs_xx.cod_azienda + "'"
end choose

ls_sql += ls_where
// ----

setnull(la_any)

if uof_crea_datastore(lds_store, ls_sql) > 0 then
	
	choose case as_flag_parametro
		case "S", "F"
			la_any = lds_store.getitemstring(1,1)
			
		case "N"
			la_any = lds_store.getitemdecimal(1,1)
			
		case "D"
			la_any = lds_store.getitemdate(1,1)

	end choose
	
end if

return la_any

end function

public function integer uof_tipo_tracciato_ord_acq (string fs_cod_fornitore, ref string fs_errore);string	ls_EO1, ls_EO2, ls_EO3

//funzione che legge a quale tipo di tracciato risponde il fornitore
//EO1		tracciato VIV								excel
//EO2		tracciato ANODALL-EXTRUSION		txt
//EO3		tracciato FONDERIA BUSTREO			csv

//leggo nei vari parametri che contengono i codici fornitore tra singoli apici, separati da virgola.
select stringa
into :ls_EO1
from parametri_azienda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro='EO1' and
			flag_parametro='S';

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro aziendale 'EO1' (codici fornitore del tracciato 1 VIV): "+sqlca.sqlerrtext
	return -1
end if
if isnull(ls_EO1) then ls_EO1 = ""

//-------
select stringa
into :ls_EO2
from parametri_azienda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro='EO2' and
			flag_parametro='S';

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro aziendale 'EO2' (codici fornitore del tracciato 2 ANODALL): "+sqlca.sqlerrtext
	return -1
end if
if isnull(ls_EO2) then ls_EO2 = ""

//-------
select stringa
into :ls_EO3
from parametri_azienda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro='EO3' and
			flag_parametro='S';

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro aziendale 'EO3' (codici fornitore del tracciato 3 BUSTREO): "+sqlca.sqlerrtext
	return -1
end if
if isnull(ls_EO3) then ls_EO3 = ""


//leggo a quale tipo di tracciato risponde il fornitore
if pos(ls_EO1, "'"+fs_cod_fornitore+"'")>0 then
	//VIV
	return 1

elseif pos(ls_EO2, "'"+fs_cod_fornitore+"'")>0 then
	//ANODALL-EXTRUSION
	return 2

elseif pos(ls_EO3, "'"+fs_cod_fornitore+"'")>0 then
	//BUSTREO
	return 3

else
	//il fornitore non risponde a nessuno dei tracciati: esporto secondo lo standard
	return 0
	
end if
end function

public function boolean uof_reset_filtro (string as_tipo_filtro);/**
 * stefanop
 * 28/10/2011
 *
 * Centrallizzata la funzione che permette di leggere i valori impostati nella datawindow
 * all'interno del database
 **/
 
string ls_cod_utente

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then ls_cod_utente = "SYSTEM"

delete 
from filtri_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_utente =  :ls_cod_utente and
	tipo_filtro = :as_tipo_filtro;

if sqlca.sqlcode = 0 then
	commit;
	return true
elseif sqlca.sqlcode < 0 then
	rollback;
	g_mb.error("Errore in lettura impostazioni predefinite filtri di ricerca.", sqlca)
	return false
end if

return true
end function

public function integer uof_filigrana (string fs_path_file, ref datawindow dw_data, boolean fb_visualizza_subito, ref string fs_errore);// imposta Filigrana su datawindow:  non la rende visibile se l'argomento fb_visualizza_subito

if isnull(fs_path_file) or fs_path_file="" then
	//fs_errore = "Non è stato specificato il path del file immagine della filigrana!"
	return 0
end if

fs_errore = dw_data.modify("DataWindow.Picture.File='" + fs_path_file + "'")
if not isnull(fs_errore) and fs_errore <> "" then
	fs_errore = "File: " + fs_errore
	return -1
end if
//----------------------------------
fs_errore = dw_data.modify("DataWindow.Picture.Mode=1")
if not isnull(fs_errore) and fs_errore <> "" then
	fs_errore = "Mode: " + fs_errore
	return -1
end if

//fs_errore = dw_data.modify("DataWindow.Picture.Transparency=80")
//if not isnull(fs_errore) and fs_errore <> "" then
//	fs_errore = "Transparency: " + fs_errore
//	return -1
//end if

fs_errore = dw_data.modify("DataWindow.Print.Background='Yes'")
if not isnull(fs_errore) and fs_errore <> "" then
	fs_errore = "Background: " + fs_errore
	return -1
end if

if fb_visualizza_subito then
	fs_errore = dw_data.modify("DataWindow.brushmode=6")
else
	fs_errore = dw_data.modify("DataWindow.brushmode=0")
end if

if not isnull(fs_errore) and fs_errore <> "" then
	fs_errore = "Brushmode: " + fs_errore
	return -1
end if

return 0


end function

public function string uof_data_db (date adt_date);// se ORACLE devo usare il suo formato data
choose case upper(left(sqlca.dbms,2)) 
	case "OR","O9","O10","ORA"
		if len(sqlca.oracle_dateformat) > 0 then 
			return string(adt_date, sqlca.oracle_dateformat)
		end if
	case else
	return string(adt_date, s_cs_xx.db_funzioni.formato_data)
end choose
end function

public function string uof_data_db (datetime adt_date);// se ORACLE devo usare il suo formato data
choose case upper(left(sqlca.dbms,2)) 
	case "OR","O9","O10","ORA"
		if len(sqlca.oracle_dateformat) > 0 then 
			return string(adt_date, sqlca.oracle_dateformat)
		end if
	case else
		return string(adt_date, s_cs_xx.db_funzioni.formato_data)
end choose
end function

public subroutine uof_get_prossimo_giorno_lavorativo (date adt_date, boolean ab_feste_comandate, ref date adt_working_date);/**
 * stefanop + donatoc
 * 04/02/2012
 *
 * Calcolo il primo giorno lavorativo valido a partire da una data passata
 * La funzione tiene conto dei sabati e domeniche + eventuali festivita comandate
 **/

int li_day_number

adt_working_date = relativedate(adt_date, 1)

do while true
	li_day_number = daynumber(adt_working_date)
	
	if li_day_number > 1 and li_day_number < 7 then 
		// TODO è una festività comandata? se si NON esco
		exit
	end if

	
	adt_working_date = relativedate(adt_working_date, 1)
loop
end subroutine

public function integer uof_get_ultimo_giorno_mese (integer ai_mese, integer ai_anno);/**
 * stefanop + donatoc
 * 04/02/2012
 *
 * Calcolo l'ultimo giorno del mese valido
 **/
 
long ll_ultimo_giorno

choose case ai_mese
	case 11,4,6,9
		ll_ultimo_giorno = 30
		
	case 2
		//se bisestile sono 29, altrimenti 28
		//sono bisestili gli anni divisibili per quattro, tranne quelli di inizio secolo (cioè divisibili per 100) non divisibili per 400
		if mod(ai_anno, 4) = 0 then
			
			if mod(ai_anno, 100) = 0 then
				
				if mod(ai_anno, 400) = 0 then
					//bisestile
					ll_ultimo_giorno = 28
				else
					//non bisestile
					ll_ultimo_giorno = 29
				end if
				
			else
				//sicuramente bisestile
				ll_ultimo_giorno = 29
			end if
			
		else
			//sicuramente non bisestile
			ll_ultimo_giorno = 28
		end if
		
	case else
		ll_ultimo_giorno = 31
		
end choose

return ll_ultimo_giorno
end function

public function integer uof_get_ultimo_giorno_mese (date adt_date);/**
 * stefanop + donatoc
 * 04/02/2012
 *
 * Calcolo l'ultimo giorno del mese valido
 **/
 
return uof_get_ultimo_giorno_mese(month(adt_date), year(adt_date))
end function

public function string uof_get_operaio_utente (string as_cod_utente);/**
 * stefanop
 * 25/01/2012
 *
 * Recupero l'operatore associato all'utentepassato per parametro
 **/
 
string ls_sql, ls_cod_operatore
long ll_row
datastore lds_store

ls_sql = "SELECT cod_operaio FROM anag_operai WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' AND cod_utente='" + as_cod_utente + "'"

ll_row = uof_crea_datastore(lds_store, ls_sql)

if ll_row < 0 then
	setnull(ls_cod_operatore)
else
	
	ls_cod_operatore = lds_store.getitemstring(1,1)
	
	if ls_cod_operatore = "" then setnull(ls_cod_operatore)
	
end if

return ls_cod_operatore
end function

public function string uof_get_operaio_utente ();/**
 * stefanop
 * 25/01/2012
 *
 * Recupero l'operatore associato all'utentepassato per parametro
 **/
 
return uof_get_operaio_utente(s_cs_xx.cod_utente)
end function

public function boolean uof_is_supervisore (string as_cod_utente);/**
 * stefanop
 * 10/04/2012
 *
 * Indica se l'utente ha il flag_supervisore attivato
 **/
 
string ls_flag

if as_cod_utente = "CS_SYSTEM" then return true

select flag_supervisore
into :ls_flag
from utenti
where cod_utente = :as_cod_utente;

if sqlca.sqlcode = 0 and ls_flag = 'S' then
	return true
else
	return false
end if


end function

public function boolean uof_is_admin (string as_cod_utente);/**
 * stefanop
 * 10/04/2012
 *
 * Indica se l'utente ha il flag_collegato attivato
 **/
 
string ls_flag

if as_cod_utente = "CS_SYSTEM" then return true

select flag_collegato
into :ls_flag
from utenti
where cod_utente = :as_cod_utente;

if sqlca.sqlcode = 0 and ls_flag = 'S' then
	return true
else
	return false
end if


end function

public function long uof_substr_count (string as_text, string as_find);/**
  * stefanop
  * 24/04/2012
  *
  * Conta le occorrenze di una stringa all'interno di un testo
  **/
  
  
long ll_pos, ll_length, ll_count

ll_count = 0

ll_length = len(as_find)
ll_pos = pos(as_text, as_find)

do while ll_pos > 0
	
	ll_count++
	
	ll_pos = pos(as_text, as_find, ll_pos + ll_length)
	
loop

return ll_count
end function

public function integer uof_get_file_info (string as_file_source, ref string as_file_dir, ref string as_file_name, ref string as_file_ext);/**
 * stefanop
 * 03/02/2012
 *
 * Dato un percorso di un file recupero le informazioni della cartella, il nome e l'estensione
 **/
 
long ll_last_pos, ll_last_pos_point

setnull(as_file_dir)
setnull(as_file_name)
setnull(as_file_ext)

if isnull(as_file_source) or as_file_source = "" then return -1

ll_last_pos = lastpos(as_file_source, "\")
ll_last_pos_point = lastpos(as_file_source, ".")

as_file_dir = left(as_file_source, ll_last_pos)
as_file_name = mid(as_file_source, ll_last_pos + 1, ll_last_pos_point - ll_last_pos - 1)
as_file_ext = mid(as_file_source, ll_last_pos_point + 1)
end function

public function date uof_string_to_date (string as_string_date);
return date(left(as_string_date, 4) + "-" + mid(as_string_date, 5,2) + "-" + right(as_string_date, 2))
end function

public function integer uof_esporta_clienti_txt (string as_percorso_file, ref string as_errore);/*
----Funzione che legge i dati dalla tabella anagrafica clienti e scrive alcuni campi su un file di testo
*/

String ls_cod_cliente,ls_rag_soc_1,ls_indirizzo,ls_cap,ls_localita,ls_provincia,ls_cod_nazione,ls_partita_iva,ls_cod_fiscale,ls_flag_blocco,ls_valore
int li_num_file


declare cu_cursore cursor for  

	select cod_cliente,rag_soc_1,indirizzo,cap,localita,provincia,cod_nazione,partita_iva,cod_fiscale,flag_blocco
	from anag_clienti
	where cod_azienda=:s_cs_xx.cod_azienda
	order by cod_cliente;

open cu_cursore;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore 'cu_cursore'~r~n" + sqlca.sqlerrtext
	return -1
end if

//Apertura del file

li_num_file=FileOpen(as_percorso_file, linemode!, write!, shared!, replace!)

FileWriteEx(li_num_file,"codice_cliente;ragione_sociale;indirizzo;cap;località,provincia,nazione;partita_iva;codice_fiscale;legale;bloccati") //Intestazione file

do while true
	
	fetch cu_cursore into :ls_cod_cliente,:ls_rag_soc_1,:ls_indirizzo,:ls_cap,:ls_localita,:ls_provincia,:ls_cod_nazione,:ls_partita_iva,:ls_cod_fiscale,:ls_flag_blocco;

	
	if sqlca.sqlcode < 0 then
		close cu_cursore;
		as_errore = "Errore in FETCH cursore 'cu_cursore'~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	//Controllo che le variabili non siano nulle,se sono nulle ("") altrimenti sostituisco il ; con il -
	
	uof_replace_string(ls_cod_cliente,";","-")	//Sostituisce il ; con il -
	
	if isnull(ls_rag_soc_1) then
		ls_rag_soc_1=""
	else 
		uof_replace_string(ls_rag_soc_1,";","-")	
	end if
	
	if isnull(ls_indirizzo) then
		ls_indirizzo=""
	else 
		uof_replace_string(ls_indirizzo,";","-")
	end if
	
	if isnull(ls_cap) then
		ls_cap=""
	else 
		uof_replace_string(ls_cap,";","-")
	end if
	
	if isnull(ls_localita) then
		ls_localita=""
	else 
		uof_replace_string(ls_localita,";","-")	
	end if
	
	if isnull(ls_provincia) then
		ls_provincia=""
	else
		uof_replace_string(ls_provincia,";","-")
	end if
	
	if isnull(ls_cod_nazione) then
		ls_cod_nazione=""
	else
		uof_replace_string(ls_cod_nazione,";","-")
	end if
	
	if isnull(ls_partita_iva) then
		ls_partita_iva=""
	else
		uof_replace_string(ls_partita_iva,";","-")
	end if
	
	if isnull(ls_cod_fiscale) then
		ls_cod_fiscale=""
	else
		uof_replace_string(ls_cod_fiscale,";","-")	
	end if	
	
	if isnull(ls_flag_blocco) then
		ls_flag_blocco=""
	else
		
		if ls_flag_blocco="S" then
			ls_flag_blocco="B"
		else
			ls_flag_blocco=""
		end if
		
	end if
	
	ls_valore=ls_cod_cliente+";"+ls_rag_soc_1+";"+ls_indirizzo+";"+ls_cap+";"+ls_localita+";"+ls_provincia+";"+ls_cod_nazione+";"+ls_partita_iva+";"+ls_cod_fiscale+";"+""+";"+ls_flag_blocco+";"
	
	FileWriteEx(li_num_file,ls_valore)
	

loop

FileClose(li_num_file)

close cu_cursore;

return 0	//Se non ci sono errori


end function

public function integer uof_get_qrcode (ref string fs_path, string fs_valore, long fl_dim_x, long fl_dim_y, string fs_filename, ref string fs_errore);//funzione che crea un file immagine png nel formato qr-code
//
//Argomenti
//					ref			string			fs_path			-> se non valorizzato prende la temp dell'utente: BYREF, TORNA IL PATH COMPLETO DEL FILE GENERATO!!!!
//					value		string			fs_valore			-> è la stringa da rappresenatre nel qr-code
//					value		long			fl_dim_x			-> dimensione x dell'immagine in pixel
//					value		long			fl_dim_y			-> dimensione y dell'immagine in pixel
//					value		string			fs_filename		-> nome (senza estensione) da dare al file da produrre (es: my_file)
//					ref			string			fs_errore			-> se la funzione torna -1 contiene il messaggio di errore
//Valore ritorno
//					0		se tutto OK
//					-1		in caso di errore   (da legegre nella variabile fs_errore)


uo_google_qrcode_result luo_gchart
inet linet
int li_success
string ls_url, ls_path


if GetContextService("Internet", linet) = -1 then
	  fs_errore = "Errore  in accesso Internet service (GetContextService)"
	  return -1
end if
	
luo_gchart = create uo_google_qrcode_result

if fs_path="" or isnull(fs_path) then
	//imposta il percorso la cartella temporanea dell'utente
	fs_path = uof_get_user_temp_folder()
end if

ls_url = "https://chart.googleapis.com/chart?cht=qr&chl="+fs_valore+"&chs="+string(fl_dim_x)+"x"+string(fl_dim_y)

li_success = linet.GetURL(ls_url, luo_gchart)
if  li_success<>  1 then
	fs_errore = "Errore " + string(li_success) + ": url recupero qrcode non valido o problemi connessione internet ..."
	destroy luo_gchart
	return -1
end if

fs_path = fs_path + fs_filename + ".png"		//"qrcode_"+string(fl_pointer) + ".png"

 if Len(luo_gchart.ib_result)>  0 then
	guo_functions.uof_blob_to_file( luo_gchart.ib_result, fs_path)
 end if

destroy luo_gchart

return 1
end function

public function string uof_get_file_attribute (string as_file_path, integer ai_attributes);/**
 * stefanop
 * 02/07/2012
 *
 * Funzione trovata da Matteo Casarin
 *
 * Recupera gli attributi di un file.
 * Attributi possibili:
 * 0 = Filename
 * 1 = Size
 * 2 = Type
 * 3 = Modified Date
 * 4 = Created Date
 * 5 = Unknown
 * 6 = Attributes
 * 7 = ???
 * 8 = Domene
 **/
 

String ls_path, ls_file,ls_ret=""
OLEObject obj_shell, obj_folder, obj_item

obj_shell = CREATE OLEObject
obj_shell.ConnectToNewObject( 'shell.application' )

ls_path = Left( as_file_path, LastPos( as_file_path, "\" ) )
ls_file = Mid( as_file_path, LastPos( as_file_path, "\" ) + 1 )

IF FileExists( as_file_path ) THEN
	
	obj_folder = obj_shell.NameSpace( ls_path )    
   
	IF IsValid( obj_folder ) THEN 
		
		obj_item = obj_folder.ParseName( ls_file )  
		
		IF IsValid( obj_item ) THEN 
			ls_ret = obj_folder.GetDetailsOf( obj_item, ai_attributes)       
		END IF
	END IF
END IF

DESTROY obj_shell
DESTROY obj_folder
DESTROY obj_item

RETURN ls_ret
end function

public function time uof_seconds_to_time (long al_seconds);/**
 * stefanop
 * 03/07/2012
 *
 * Converte un numero di secondi in una variabile time
 **/
 
 
long ll_second, ll_minute, ll_hour

ll_second = mod(al_seconds, 60)
ll_minute = al_seconds / 60
ll_hour = 0

if ll_minute >= 60 then
	
	ll_hour = ll_minute / 60
	ll_minute = mod(ll_minute, 60)
	
end if

return time(string(ll_hour) + ":" +  string(ll_minute) + ":" + string(ll_second))
end function

public function time uof_time_diff (time at_time_start, time at_time_end);/**
 * stefanop
 * 03/07/2012
 *
 * Converte un numero di secondi in una variabile time
 **/
 
 
return uof_seconds_to_time(secondsafter(at_time_start, at_time_end))
end function

public subroutine uof_resize_max_mdi (window a_window, integer ai_resize);/**
 * stefanop
 * 10/07/2012
 *
 * Ridimensiona la finestra passa per argomento sulle dimensioni dell MDI
 * E' possibile ridimensionare per altezza, larghezza o entrambe
 *
 * Parametri possibili:
 * 1 = resize altezza
 * 2 = resize larghezza
 * 3 = resize altezza e larghezza
 *
 * @param window   a_window
 * @param ai_resize ai_resize
 * @return void
 **/
 
choose case ai_resize
		
	case 1
		a_window.x = 0
		a_window.height = w_cs_xx_mdi.mdi_1.height - 70
		
	case 2
		a_window.y = 0
		a_window.width = w_cs_xx_mdi.mdi_1.width - 70
		
	case 3
		a_window.move(0,0)
		a_window.height = w_cs_xx_mdi.mdi_1.height - 70
		a_window.width  = w_cs_xx_mdi.mdi_1.width - 70
		
end choose
end subroutine

public function time uof_time_diff (long al_cpu_start, long al_cpu_end);/**
 * stefanop
 * 03/07/2012
 *
 * Converte un numero di secondi in una variabile time
 **/

long ll_microseconds, ll_milliseconds, ll_seconds, ll_second, ll_minutes, ll_hours
time lt_diff

ll_microseconds = al_cpu_end - al_cpu_start

ll_seconds = ll_microseconds / 1000
ll_minutes = ll_seconds / 60
ll_hours = ll_minutes / 60

ll_microseconds = mod(ll_microseconds, 1000)
ll_seconds = mod(ll_seconds, 60)
ll_minutes = mod(ll_minutes, 60)


//if ll_minutes >= 60 then
//	
//	ll_hours = ll_minutes / 60
//	ll_minutes = mod(ll_minutes, 60)
//	
//end if

return time(ll_hours, ll_minutes, ll_seconds, ll_microseconds)
end function

public function boolean uof_anno_bisestile (integer ai_anno);//Donato 19/07/2012
//torna TRUE se l'anno della variabile "ai_anno" è bisestile, altrimenti torna FALSE

//algoritmo anno bisestile
//se è 
//		( DIVISIBILE per 4 AND NON divisibile per 100) OR (DIVISIBILE per 400)

if (mod(ai_anno, 4) = 0 and mod(ai_anno, 100) <> 0) or mod(ai_anno, 400) = 0 then
	//BISESTILE
	return true
else
	//NON BISESTILE
	return false
end if
end function

public function boolean uof_confronta_arrays (string aa_array_1[], string aa_array_2[]);long ll_i, ll_dim1
any la_value1, la_value2

ll_dim1 = upperbound(aa_array_1[])

if ll_dim1=upperbound(aa_array_2[]) then
	if ll_dim1=0 then
		//di sicuro sono uguali, in quanto entrambi vuoti
		return true
	end if
else
	//di sicuro non sono uguali
	return false
end if


//se arrivi fin qui devi leggere le componenti dei due array (che di sicuro hanno la stessa dimensione)
for ll_i = 1 to ll_dim1
	la_value1 = aa_array_1[ll_i]
	la_value2 = aa_array_2[ll_i]
	
	if la_value1=la_value2 then
	else
		return false
	end if
	
next

return true
end function

public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref decimal ad_decimal);ad_decimal = dec(uof_get_parametri("A", as_cod_parametro, "DEC"))
end subroutine

public function string uof_string_to_rte (string as_text);/**
 * stefanop
 * 26/09/2012
 *
 * Converte un plain-text in formato RTE con font Arial 9
 **/
 
if isnull(as_text) or as_text = "" then
	return ""
elseif left(as_text, 12) ="{\rtf1\ansi\" then
	return as_text
else
	return "{\rtf1\ansi\ansicpg1252\uc1\deff0{\fonttbl" + &
			"{\f0\fnil\fcharset0\fprq2 Arial;}" + &
			"{\f1\fswiss\fcharset0\fprq2 Arial;}" + &
			"{\f2\froman\fcharset2\fprq2 Symbol;}}" + &
			"{\colortbl;\red0\green0\blue0;\red255\green255\blue255;}" + &
			"{\stylesheet{\s0\itap0\nowidctlpar\f0\fs24 [Normal];}{\*\cs10\additive Default Paragraph Font;}}" + &
			"{\*\generator TX_RTF32 15.0.530.503;}" + &
			"\deftab1134\paperw11905\paperh16838\margl0\margt0\margr0\margb0\widowctrl\formshade" + &
			"{\*\background{\shp{\*\shpinst\shpleft0\shptop0\shpright0\shpbottom0\shpfhdr0\shpbxmargin\shpbxignore\shpbymargin\shpbyignore\shpwr0\shpwrk0\shpfblwtxt1\shplid1025{\sp{\sn shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn fillColor}{\sv 16777215}}{\sp{\sn fFilled}{\sv 1}}{\sp{\sn lineWidth}{\sv 0}}{\sp{\sn fLine}{\sv 0}}{\sp{\sn fBackground}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 1}}}}}\sectd" + &
			"\headery720\footery720\pgwsxn6750\pghsxn16838\marglsxn0\margtsxn0\margrsxn0\margbsxn0\pard\itap0\nowidctlpar\plain\f1\fs18 " + as_text + "}"
end if
end function

public function boolean uof_cancella_filtro (ref datawindow adw_datawindow, string as_tipo_filtro);/**
 * stefanop
 * 22/10/2011
 *
 * Centrallizzata la funzione che permette di cancellare i valori impostati nella datawindow
 **/
 
string ls_cod_utente

if not g_mb.confirm("Cancellare i valori del filtro predefinito?")  then 
	return true
end if

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then ls_cod_utente = "SYSTEM"

delete from filtri_manutenzioni 
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_utente =  :ls_cod_utente and
	tipo_filtro = :as_tipo_filtro;

if sqlca.sqlcode = 100 then
	return true
elseif sqlca.sqlcode < 0 then
	g_mb.error(g_str.format("Errore durante la cancellazione del filtro $1", as_tipo_filtro), sqlca)
	rollback;
	return false
end if

commit;
adw_datawindow.reset()
adw_datawindow.insertrow(0)
return true
end function

public function string uof_risorse (string as_path);/**
 * stefanop
 * 22/10/2012
 *
 * Aggiungo il suffisso delle risorse nella stringa
 **/
 
return s_cs_xx.volume + s_cs_xx.risorse + as_path
end function

public subroutine uof_get_parametro_azienda (string as_cod_parametro, ref boolean ab_flag);string	ls_flag

ls_flag = string(uof_get_parametri("A", as_cod_parametro, "F"))

choose case ls_flag
	case "S"
		ab_flag=true
	case else
		ab_flag=false
end choose
end subroutine

public subroutine uof_get_parametro (string as_cod_parametro, ref boolean ab_flag);string ls_flag

ls_flag = string(uof_get_parametri("M", as_cod_parametro, "F"))

//ab_flag = ls_flag = "S"


choose case ls_flag
	case "S"
		ab_flag=true
	case else
		ab_flag=false
end choose
end subroutine

public function boolean uof_get_date_from_giornomese (long fl_giorno_mese, integer fi_anno, ref date fdd_date);/*
Questa funzione costruisce una data a partire da un numero formattato come (d)dmm
esempi

  803  	->  	08/03/anno
1012		->		10/12/anno

dove anno è la variabile fi_anno, o se vuota considera l'anno corrente

Torna TRUE se tutto a posto, FALSE in caso di valori errati
*/

integer	li_mese, li_giorno, li_max

//gli ultimi due numeri sono sempre di un mese, che deve trovarsi tra 01 e 12
li_mese = integer(right(string(fl_giorno_mese), 2))

if li_mese>0 and li_mese <= 12 then
else
	return false
end if

//estreaggo i giorni come divisione intera per 100
//infatti ad esempio 				int( 803/100 ) = int( 8.03 ) = 8
//										int( 1012/100 ) = int( 10.12 ) = 10
li_giorno = int(fl_giorno_mese / 100)

choose case li_mese
	case 2		//febbraio
		li_max = 28
		
	case 4, 6, 9, 11
		li_max = 30
		
	case else
		li_max = 31
		
end choose

if li_giorno>0 and li_giorno <= li_max then
else
	return false
end if

if fi_anno<=0 or isnull(fi_anno) then fi_anno=year(today())

fdd_date = date(fi_anno, li_mese, li_giorno)

return true

end function

public function string uof_get_random_filename ();return 		string( year(today()),"0000") 	+ string( month(today()),"00")		+ string( day(today()),"00") + &
				string( hour(now()),"00") 		+ string( minute(now()),"00") 		+ string( second(now()),"00") 

end function

public function integer uof_setnull_fatt_proforma (integer fi_anno_ordine, long fl_num_ordine, long fl_riga_ordine, ref string fs_messaggio);//28/01/2013 Donato
//su richiesta di Beatrice
//chiamata prima di cancellare una riga di ordine di vendita
//se esiste una riga di fattura proforma riferita alla riga di ordine, mette a NULL i 3 campi in det_fat_ven
//questo per evitare l'errore di violazione di FK tra det_ord_ven e det_fat_ven


long			ll_tot, ll_index, ll_anno_proforma, ll_num_proforma, ll_riga_proforma
string			ls_sql
datastore	lds_data


ls_sql = 	"select fd.anno_registrazione, fd.num_registrazione, fd.prog_riga_fat_ven "+&
			"from det_fat_ven as fd "+&
			"join tes_fat_ven as ft on ft.cod_azienda=fd.cod_azienda and  "+&
											"ft.anno_registrazione=fd.anno_registrazione and  "+&
                    			    				"ft.num_registrazione=fd.num_registrazione  "+&
			"join det_ord_ven as od on 	od.cod_azienda=fd.cod_azienda and  "+&
                        				"od.anno_registrazione=fd.anno_reg_ord_ven and  "+&
                        				"od.num_registrazione=fd.num_reg_ord_ven and  "+&
                        				"od.prog_riga_ord_ven=fd.prog_riga_ord_ven "+&
			"where od.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"od.anno_registrazione="+string(fi_anno_ordine)+" and "+&
					"od.num_registrazione="+string(fl_num_ordine)+" and "+&
					"od.prog_riga_ord_ven="+string(fl_riga_ordine)+" and "+&
					"ft.cod_tipo_fat_ven in (select cod_tipo_fat_ven "+&
												"from tab_tipi_fat_ven "+&
												"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
														"flag_tipo_fat_ven in ('P','F')) "

ll_tot = uof_crea_datastore(lds_data, ls_sql, fs_messaggio)

for ll_index=1 to ll_tot
	ll_anno_proforma	= lds_data.getitemnumber(ll_index, 1)
	ll_num_proforma	= lds_data.getitemnumber(ll_index, 2)
	ll_riga_proforma	= lds_data.getitemnumber(ll_index, 3)
	
	
	//scollegamento ------
	update det_fat_ven
	set 	anno_reg_ord_ven=null,
			num_reg_ord_ven=null,
			prog_riga_ord_ven=null
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_proforma and
				num_registrazione = :ll_num_proforma and
				prog_riga_fat_ven = :ll_riga_proforma;
	
	if sqlca.sqlcode<0 then
		fs_messaggio = "(D) Errore in scollegamento riga fattura proforma ("+string(ll_anno_proforma)+"/"+string(ll_num_proforma)+"/"+string(ll_riga_proforma)+") "+&
								"da riga ordine vendita ("+string(fi_anno_ordine)+"/"+string(fl_num_ordine)+"/"+string(fl_riga_ordine)+") :"+sqlca.sqlerrtext
		return -1
	end if
	
next

return 0
end function

public function integer uof_setnull_fatt_proforma (integer fi_anno_ordine, long fl_num_ordine, ref string fs_messaggio);//28/01/2013 Donato
//su richiesta di Beatrice
//chiamata prima di cancellare un ordine di vendita (testata)
//se esistono righe di fattura proforma riferite alle righe di ordine, mette a NULL i 3 campi in det_fat_ven
//questo per evitare l'errore di violazione di FK tra det_ord_ven e det_fat_ven


string			ls_sql
datastore	lds_data
long			ll_tot, ll_index, ll_anno_proforma, ll_num_proforma, ll_riga_proforma, ll_prog_riga_ord_ven


ls_sql = 	"select fd.anno_registrazione, fd.num_registrazione, fd.prog_riga_fat_ven, fd.prog_riga_ord_ven "+&
			"from det_fat_ven as fd "+&
			"join tes_fat_ven as ft on ft.cod_azienda=fd.cod_azienda and  "+&
											"ft.anno_registrazione=fd.anno_registrazione and  "+&
                    			    				"ft.num_registrazione=fd.num_registrazione  "+&
			"join det_ord_ven as od on 	od.cod_azienda=fd.cod_azienda and  "+&
                        				"od.anno_registrazione=fd.anno_reg_ord_ven and  "+&
                        				"od.num_registrazione=fd.num_reg_ord_ven and  "+&
                        				"od.prog_riga_ord_ven=fd.prog_riga_ord_ven "+&
			"where od.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"od.anno_registrazione="+string(fi_anno_ordine)+" and "+&
					"od.num_registrazione="+string(fl_num_ordine)+" and "+&
					"ft.cod_tipo_fat_ven in (select cod_tipo_fat_ven "+&
												"from tab_tipi_fat_ven "+&
												"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
														"flag_tipo_fat_ven in ('P','F')) "

ll_tot = uof_crea_datastore(lds_data, ls_sql, fs_messaggio)

for ll_index=1 to ll_tot
	ll_anno_proforma	= lds_data.getitemnumber(ll_index, 1)
	ll_num_proforma	= lds_data.getitemnumber(ll_index, 2)
	ll_riga_proforma	= lds_data.getitemnumber(ll_index, 3)
	
	ll_prog_riga_ord_ven = lds_data.getitemnumber(ll_index, 4)
	
	//scollegamento ------
	update det_fat_ven
	set 	anno_reg_ord_ven=null,
			num_reg_ord_ven=null,
			prog_riga_ord_ven=null
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_proforma and
				num_registrazione = :ll_num_proforma and
				prog_riga_fat_ven = :ll_riga_proforma;
	
	if sqlca.sqlcode<0 then
		fs_messaggio = "(T) Errore in scollegamento riga fattura proforma ("+string(ll_anno_proforma)+"/"+string(ll_num_proforma)+"/"+string(ll_riga_proforma)+") "+&
								"da riga ordine vendita ("+string(fi_anno_ordine)+"/"+string(fl_num_ordine)+"/"+string(ll_prog_riga_ord_ven)+") :"+sqlca.sqlerrtext
		return -1
	end if
	
next

return 0
end function

public subroutine uof_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, string as_cod_lingua, string as_tabella_iva, ref string fs_cod_iva[], ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[], ref string fs_des_estesa[]);string ls_colonna_anno, ls_colonna_numero, ls_sql
long ll_i
boolean lb_leggi_italiano

//prepara l'array degli imponibili, imposte e aliquote nel documento (totali raggruppati per codice iva)
choose case upper(as_tabella_iva)
	case "IVA_BOL_ACQ"
		ls_colonna_anno = "anno_bolla_acq"
		ls_colonna_numero = "num_bolla_acq"
		
	case else
		ls_colonna_anno = "anno_registrazione"
		ls_colonna_numero = "num_registrazione"
		
end choose

ls_sql = 	"select		cod_iva,"+&
						"imponibile_iva,"+&
						"importo_iva,"+&
						"perc_iva "+&
				"from  " + as_tabella_iva + " " + & 
				"where cod_azienda = '"+s_cs_xx.cod_azienda +"' and "+&
						ls_colonna_anno + " = " + string(fl_anno_registrazione) +" and "+&
						ls_colonna_numero + " = " + string(fl_num_registrazione)

declare cu_iva dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_iva;

if sqlca.sqlcode <> 0 then return

ll_i = 0
do while true
	ll_i = ll_i + 1
	fetch cu_iva into 	:fs_cod_iva[ll_i], 
							:fd_imponibile_iva_valuta[ll_i], 
							:fd_imposta_iva_valuta[ll_i],
							:fd_aliquote_iva[ll_i];

	if sqlca.sqlcode <> 0 then exit
	
	lb_leggi_italiano = true
	
	if fd_aliquote_iva[ll_i] = 0 then
		
		if as_cod_lingua <> "IT" and not isnull(as_cod_lingua) and as_cod_lingua<>"" then
			//leggo la traduzione
			select 	des_iva, 
						des_estesa
			into   		:fs_des_esenzione[ll_i], 
						:fs_des_estesa[ll_i]
			from   	tab_ive_lingue
			where  	cod_azienda = :s_cs_xx.cod_azienda and
						cod_iva = :fs_cod_iva[ll_i] and
						cod_lingua=:as_cod_lingua;
						
			if sqlca.sqlcode = 0 then
				//traduzione presente
				lb_leggi_italiano = false
			else
				//altrimenti leggi quelle in italiano
				lb_leggi_italiano = true
			end if
		end if
		
		if lb_leggi_italiano then
			// o la lingua è IT oppure la traduzione cercata non è presente in tabella tab_ive_lingue, quindi recupera dall'italiano
			
			select 	des_iva, 
						des_estesa
			into   		:fs_des_esenzione[ll_i], 
						:fs_des_estesa[ll_i]
			from   	tab_ive
			where  	cod_azienda = :s_cs_xx.cod_azienda and
							cod_iva = :fs_cod_iva[ll_i];
		end if
		
		// stefanop: 13/06/2014: se non è impostata nessuna des estesa metto la des_iva
		if isnull(fs_des_estesa[ll_i]) or fs_des_estesa[ll_i] = "" then
			fs_des_estesa[ll_i] = fs_des_esenzione[ll_i]
		end if
		
	end if
	
loop

close cu_iva;

return

end subroutine

public function integer uof_estrai_files (ref string as_percorso_iniziale, string as_condizione, ref string as_files_estratti[], ref string as_errore);string							ls_nome_elemento, ls_percorso_files
ulong							ll_handle
long							ll_index, ll_attributo_cartella
boolean						lb_ret
str_win32_find_data		lstr_win32_find_data


//mi accerto che il percorso finisca con il backslash, in caso contrario glielo aggiungo
if right(as_percorso_iniziale, 1) <> "\" then as_percorso_iniziale += "\"

//se non hai specificato alcuna condizione di estrazione, metti *.*
if isnull(as_condizione) or as_condizione="" then as_condizione = "*.*"

//accodo la richiesta dei files con la condizione
ls_percorso_files = as_percorso_iniziale + as_condizione

//assegno l'attributo dato dal S.O. alle cartelle, per escluderle dalla lettura (caso di sottocartelle presenti nel percorso)
ll_attributo_cartella = 16
//-------------------------------------------------------------------------------------------------------------------------------------

//INIZIO ELABORAZIONE ####################################################
ll_handle = FindFirstFile(ls_percorso_files, ref lstr_win32_find_data)

if ll_handle>0 then
	do		
		try
			ls_nome_elemento = string(lstr_win32_find_data.cfilename)
			if not isnull(ls_nome_elemento) and ls_nome_elemento<>"" and ls_nome_elemento<>"." and ls_nome_elemento<>".." then
				
				//solo se non si tratta di una cartella, aggiungo nell'array
				if lstr_win32_find_data.dwfileattributes <> ll_attributo_cartella then
					ll_index += 1
					as_files_estratti[ll_index] = lstr_win32_find_data.cfilename
				end if
				
			end if
			lb_ret = FindNextFile(ll_handle, ref lstr_win32_find_data)
			
		catch (runtimeerror error)
			as_errore = error.getmessage()
			FindClose(ll_handle)
			return -1
		end try
		
	loop while lb_ret	//il ciclo continua per tutto il tempo in cui questa condizione è VERA
	
	try
		FindClose(ll_handle)		
	catch (runtimeerror error2)
		as_errore = error2.getmessage()
	end try
	
end if
//FINE ELABORAZIONE #########################################################


return upperbound(as_files_estratti[])
end function

public function integer as_reverse_array (string as_array_in[], ref string as_array_out[]);// 8-4-13 EnMe
// funzione che dato un array di strighe in input inverte l'ordine degli elementi.
string ls_null[]
long ll_i, ll_count, ll_ind

as_array_out = ls_null
ll_count = upperbound( as_array_in[])

if ll_count <= 0 then return 0
ll_ind = 0
for ll_i = ll_count to 1 step -1
	ll_ind ++
	as_array_out[ll_ind] = as_array_in[ll_i]
next

return 0
end function

public function integer uof_loaddddw_dw (datawindow dddw_dw, string dddw_column, transaction trans_object, string table_name, string column_code, string column_desc, string join_clause, string where_clause, string groupby_clause, string orderby_clause);INTEGER          l_ColNbr,     l_Idx,      l_Jdx
INTEGER          l_ColPos,     l_Start,    l_Count
LONG             l_Error
STRING           l_SQLString,  l_ErrStr
STRING           l_dwDescribe, l_dwModify, l_Attrib, l_ColStr
DATAWINDOWCHILD  l_DWC

STRING           c_ColAttrib[]  =            &
                    { ".BackGround.Color",   & 
                      ".Background.Mode",    &
                      ".Border",             &
                      ".Color",              &
                      ".Font.CharSet",       &
                      ".Font.Face",          &
                      ".Font.Family",        &
                      ".Font.Height",        &
                      ".Font.Italic",        &
                      ".Font.Pitch",         &
                      ".Font.Strikethrough", &
                      ".Font.Underline",     &
                      ".Font.Weight",        &
                      ".Font.Width",         &
                      ".Height",             &
                      ".Width" }

//------------------------------------------------------------------
//  Get the number and name of the DDDW column.
//------------------------------------------------------------------

l_dwDescribe = DDDW_Column + ".ID"
l_ColNbr     = Integer(DDDW_DW.Describe(l_dwDescribe))
l_dwDescribe = "#" + String(l_ColNbr) + ".Name"
l_ColStr     = DDDW_DW.Describe(l_dwDescribe)

//****** Modifica Michele per far funzionare la select di cognome e nome anche su Oracle - 21/09/2001 *******

long   ll_return, ll_pos
string ls_db

if ll_return = -1 then
	g_mb.messagebox("uo_functions.uof_loaddddw_dw","Errore nella lettura del profilo corrente dal registro")
	return -1
end if

ll_return = Registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "enginetype", ls_db)

if ll_return = -1 then
	g_mb.messagebox("uo_functions.uof_loaddddw_dw","Errore nella lettura del motore database dal registro")
	return -1
end if

if ls_db = "ORACLE" then
	
	do
		
		ll_pos = pos(Column_Desc,"+",1)
		
		if ll_pos > 0 then
			Column_Desc = replace(Column_Desc,ll_pos,1,"||")
		end if	
	
	loop while ll_pos > 0
	
end if

//******************************************** Fine modifica ************************************************

	
//------------------------------------------------------------------
//  Build the SQL SELECT statement.
//------------------------------------------------------------------

//l_SQLString = "SELECT DISTINCT " + Column_Code + " , " + &
//				  Column_Desc + " FROM " + Table_Name
l_SQLString = "SELECT " + Column_Code + ", " + &
				  Column_Desc + " FROM " + Table_Name
				  
if Trim(join_clause) <> "" then
	l_SQLString = l_SQLString + " " + join_clause
end if

IF Trim(Where_Clause) <> "" THEN
	l_SQLString = l_SQLString + " WHERE " + Where_Clause
END IF

IF Trim(groupby_clause) <> "" THEN
	l_SQLString = l_SQLString + " GROUP BY " + groupby_clause
END IF

IF Trim(orderby_clause) <> "" THEN
	l_SQLString = l_SQLString + " ORDER BY " + orderby_clause
END IF

	
//------------------------------------------------------------------
//  Get the child DataWindow.  If column is not a DDDW column,
//  then return with error.  Otherwise, we need to set the
//  Select statement in the drop down DataWindow.
//------------------------------------------------------------------

l_Error = DDDW_DW.GetChild(l_ColStr, l_DWC)

IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWChildFindError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Set the transaction object.
//------------------------------------------------------------------

l_Error = l_DWC.SetTransObject(trans_object)

IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWTransactionError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Assign the Select statement to the drop down DataWindow.
//------------------------------------------------------------------

l_dwModify = "DataWindow.Table.Select='" + &
             w_POManager_STD.MB.fu_QuoteChar(l_SQLString, "'")  + "'"
l_ErrStr   = l_DWC.Modify(l_dwModify)

IF l_ErrStr <> "" THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWModifySQLError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Retrieve the data for the child DataWindow.
//------------------------------------------------------------------

l_Error = l_DWC.Retrieve()
IF l_Error < 0 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWRetrieveError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//---------- Modifica Michele per togliere spazi dalla lista 04/03/2003 ------------
long ll_i

for ll_i = 1 to l_DWC.rowcount()
	l_DWC.setitem(ll_i,"display_column",trim(l_DWC.getitemstring(ll_i,"display_column")))
next
//------------- Fine Modifica Michele per togliere spazi dalla lista ---------------

//------------------------------------------------------------------
//  Set the attributes of the column to be the same as the
//  parent column.
//------------------------------------------------------------------

l_Jdx = UpperBound(c_ColAttrib[])
FOR l_Idx = 1 TO l_Jdx

   l_dwDescribe = l_ColStr + c_ColAttrib[l_Idx]
   l_Attrib     = DDDW_DW.Describe(l_dwDescribe)

   IF c_ColAttrib[l_Idx] = ".Font.Face" THEN
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "Arial"
      END IF
      l_Attrib = "'" + l_Attrib + "'"
   ELSE
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "0"
      END IF
   END IF

   l_dwModify = "#1" + c_ColAttrib[l_Idx] + "=" + l_Attrib
   l_ErrStr   = l_DWC.Modify(l_dwModify)

NEXT

//------------------------------------------------------------------
//  Make sure the border for the child column is off.
//------------------------------------------------------------------

l_dwModify = "#1" + ".Border=0"
l_ErrStr   = l_DWC.Modify(l_dwModify)

//------------------------------------------------------------------
//  Indicate that there not any errors.
//------------------------------------------------------------------

string ls_Required

ls_Required = dddw_dw.Describe(dddw_column + ".DDDW.Required")

IF ls_Required = "no" THEN
   l_DWC.InsertRow(1)
END IF

RETURN 0
end function

public function string uof_concat_op (string as_profilo);//operatore di concatenazione nel caso ORACLE è ||, che va bene anche su tutti gli altri, ma non su sqlserver
//al contrario l'oeratore standard della concatenazione tra stringhe, +, non viene recepito da ORACLE
//Quindi questa funzione restituisce l'operatore concatenazione opportuno, in base alla chiave di registro che identifica l'operazione

//Argomento della funzione è il numero del profilo da considerare, in formato stringa

string				ls_db

long				ll_return


ll_return = Registryget(s_cs_xx.chiave_root + "database_" +  as_profilo, "enginetype", ls_db)

if ll_return = -1 then
	//torna l'operatore di concatenazione standard, che è il +
	return "+"
end if

if Upper(left(ls_db, 3)) = "ORA" then
	return "||"
else
	return "+"
end if
end function

public function string uof_concat_op ();//operatore di concatenazione nel caso ORACLE è ||, che va bene anche su tutti gli altri, ma non su sqlserver
//al contrario l'oeratore standard della concatenazione tra stringhe, +, non viene recepito da ORACLE
//Quindi questa funzione restituisce l'operatore concatenazione opportuno, in base alla chiave di registro che identifica l'operazione

return uof_concat_op(s_cs_xx.profilocorrente)


end function

public function integer uof_decrypt (string as_string, ref string as_decrypted);n_cst_crypto inv_crypt
inv_crypt = create n_cst_crypto


if isnull(as_string) or as_string = "" then
	as_decrypted=""
	return 0
end if

if inv_crypt.decryptdata(as_string, as_decrypted) < 0 then
	return -1
end if

return 0

end function

public function integer uof_crypt (string as_string, ref string as_decrypted);n_cst_crypto inv_crypt
inv_crypt = create n_cst_crypto


if isnull(as_string) or as_string = "" then
	as_decrypted=""
	return 0
end if

if inv_crypt.encryptdata(as_string, as_decrypted) < 0 then
	return -1
end if

return 0

end function

public function long uof_get_identity (ref n_tran at_transaction, ref long al_identity);string ls_db
long li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "enginetype_impresa", ls_db)
if li_risposta < 0 or trim(ls_db)="" or isnull(ls_db) then
	li_risposta = Registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "enginetype", ls_db)
	if li_risposta < 0 then return -1
end if


choose case ls_db
	case "SYBASE_ASA"
		select @@identity into :al_identity from sys.dummy using at_transaction;
		
	case "ORACLE"
		// caso non previsto dal momento che impresa non gira su ORACLE
		
	case "SYBASE_ASE", "MSSQL"
		DECLARE my_cursor DYNAMIC CURSOR FOR SQLSA;
		PREPARE SQLSA FROM "SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]"  using at_transaction ;
		OPEN DYNAMIC my_cursor ;
		if sqlca.sqlcode = -1 then return -1
		FETCH my_cursor INTO :al_identity ;
		CLOSE my_cursor ;	

end  choose

return 0

end function

public function integer uof_get_cod_iva (string as_ambiente, datetime adt_data_documento, string as_cod_prodotto, string as_cod_anagrafica, string as_cod_tipo_det, ref string as_cod_iva, ref string as_message);/**
 * stefanop
 * 11/04/2014
 *
 * Centrallizato il calcolo per l'iva.
 **/
 
string ls_cod_iva
datetime ldt_data_esenzione_iva
 
setnull(as_cod_iva)
setnull(as_message)

if as_cod_prodotto = "" then setnull(as_cod_prodotto)
if as_cod_anagrafica = "" then setnull(as_cod_anagrafica)
if as_cod_tipo_det = "" then setnull(as_cod_tipo_det)

choose case as_ambiente
		
	case "VEN"
		if not isnull(as_cod_tipo_det) and as_cod_tipo_det <> "" then
			select cod_iva  
			into :ls_cod_iva  
			from tab_tipi_det_ven  
			where cod_azienda = :s_cs_xx.cod_azienda and  
			 		 cod_tipo_det_ven = :as_cod_tipo_det;
					  
			if sqlca.sqlcode = 0 then
				as_cod_iva = ls_cod_iva
			else
				as_message = g_str.format("Estrazione aliquota iva: Tipo dettaglio $1 non trovato",  as_cod_tipo_det)
				return -1
			end if			
		end if
		
	case "ACQ"
		if not isnull(as_cod_tipo_det) and as_cod_tipo_det <> "" then
			select cod_iva  
			into :ls_cod_iva  
			from tab_tipi_det_acq  
			where cod_azienda = :s_cs_xx.cod_azienda and  
			 		 cod_tipo_det_acq = :as_cod_tipo_det;
					  
			if sqlca.sqlcode = 0 then
				as_cod_iva = ls_cod_iva
			else
				as_message = g_str.format("Estrazione aliquota iva: Tipo dettaglio $1 non trovato",  as_cod_tipo_det)
				return -1
			end if	
		end if

end choose

if not isnull(as_cod_tipo_det) and not isnull(as_cod_prodotto) then
	select cod_iva
	into :ls_cod_iva
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_prodotto = :as_cod_prodotto;
			 
	if sqlca.sqlcode = 0  then
		if not isnull(ls_cod_iva) and ls_cod_iva <> "" then
			as_cod_iva = ls_cod_iva
		end if
	else
		as_message = g_str.format("Estrazione aliquota iva: Prodotto $1 non trovato",  as_cod_prodotto)
		return -1
	end if
end if

choose case as_ambiente
		
	case "VEN"
		if not isnull(as_cod_anagrafica) then
			select 	cod_iva,
					 	data_esenzione_iva
			into 		:ls_cod_iva, 
				  		:ldt_data_esenzione_iva
			from 		anag_clienti
			where	cod_azienda = :s_cs_xx.cod_azienda and 
						cod_cliente = :as_cod_anagrafica;

			if sqlca.sqlcode = 0 then
				if ls_cod_iva <> "" and not isnull(ls_cod_iva) and (ldt_data_esenzione_iva <= adt_data_documento or isnull(ldt_data_esenzione_iva)) then
					as_cod_iva = ls_cod_iva
				end if
			else
				as_message = g_str.format("Estrazione aliquota iva: Cliente $1 non trovato",  as_cod_anagrafica)
				return -1
			end if
		end if
		
	case "ACQ"
		if not isnull(as_cod_anagrafica) then
			select 	cod_iva,
					 	data_esenzione_iva
			into 		:ls_cod_iva, 
				  		:ldt_data_esenzione_iva
			from 		anag_fornitori
			where	cod_azienda = :s_cs_xx.cod_azienda and 
						cod_fornitore = :as_cod_anagrafica;

			if sqlca.sqlcode = 0 then
				if ls_cod_iva <> "" and not isnull(ls_cod_iva) and (ldt_data_esenzione_iva <= adt_data_documento or isnull(ldt_data_esenzione_iva)) then
					as_cod_iva = ls_cod_iva
				end if
			else
				as_message = g_str.format("Estrazione aliquota iva: Cliente $1 non trovato",  as_cod_anagrafica)
				return -1
			end if
		end if
		
end choose

return 0


end function

public function boolean uof_create_transaction_from (ref transaction at_from, ref transaction at_to, string as_servername, string as_database, ref string as_error);/**
 * stefanop
 * 10/02/2012
 *
 * Imposta i parametri di una nuova conessione recuperandoli dalla transazione passata
 **/


string ls_db,ls_logpass
integer li_risposta

at_to = create transaction

at_to.ServerName = as_servername
at_to.LogId = at_from.LogId
at_to.DBMS = at_from.DBMS
at_to.Database = as_database
at_to.UserId = at_from.UserId
at_to.LogPass = at_from.LogPass
at_to.DBPass = at_from.DBPass
at_to.dbparm = at_from.dbparm
at_to.Lock = at_from.Lock

connect using at_to;

if at_to.sqlcode = 0 then
	return true
else
	as_error = at_to.sqlerrtext
	return false
end if
end function

public function string uof_get_computer_name ();/**
 * stefanop
 * 17/06/2014
 *
 * Ritorna il nome del computer corrente
 **/
 
long ll_ret
string ls_computername
ulong ul_buffer = 250

ls_computername = Space(ul_buffer)
GetComputerName(ls_computername, ul_buffer)

return trim(ls_computername)
end function

public function string uof_get_username ();/**
 * stefanop
 * 17/06/2014
 *
 * Ritorna il nome dell'utente collegato
 **/
 
long ll_ret
string ls_username
ulong ul_buffer = 100

ls_username = Space(ul_buffer)
GetUserName(ls_username, ul_buffer)

return trim(ls_username)
end function

public function string uof_get_system_info ();/**
 * stefanop
 * 17/06/2014
 *
 * Ritorna una lista con le informazioni del sistema
 **/
 
string ls_nl = "~r~n"
string ls_message
environment le_env

ls_message = ""

if GetEnvironment(le_env) = 1 then
	ls_message += "PB version: "+ string(le_env.pbmajorrevision)
	ls_message += "." + string(le_env.pbminorrevision)
	ls_message += "." + string(le_env.pbfixesrevision)
	ls_message += " Build: " + string(le_env.pbbuildnumber)
	ls_message += ls_nl
end if

ls_message += "Window OS: "
ls_message += ls_nl + "Architettura: " + uof_get_user_info("PROCESSOR_ARCHITECTURE")
ls_message += ls_nl + "Nome PC: " + uof_get_computer_name()
ls_message += ls_nl + "Username: " + uof_get_username()

return ls_message
end function

public function string uof_sanatize_filename (string as_filename);/**
 * stefanop
 * 04/07/2014
 *
 * Elimina i caratteri non validi nei nomi file
 **/
 
string ls_value
uo_regex luo_regex
 
luo_regex = create uo_regex

luo_regex.initialize(ILLEGAL_FILE_PATTERN, true, false)
ls_value = luo_regex.replace(as_filename, "_")

destroy luo_regex

return ls_value
end function

public function integer uof_valida_iban (string as_iban, ref string as_error);/**
 * stefanop
 * 22/07/2014
 *
 * Validazione IBAN tramite webservice
 *
 * Return: -1 error, 0 iban vuoto, 1 valido
 **/
 
string ls_response, ls_token, ls_ws, ls_id_tes_lancio, ls_messaggio
long 	ll_response_error
int li_return, li_response_status
//uo_xhr luo_xhr
uo_json luo_json
oleobject luo_response

setnull(as_error)

as_error = "Per questa funzione è richiesta la sottoscrizione di un abbonamento."
return -1


/*
if g_str.isempty(as_iban) then
	return 0
end if

//try 
	
	uo_xhr2 luo_xhr
	oleobject lole_json
	luo_json = create uo_json
	
	luo_xhr = create uo_xhr2
	
//	luo_xhr.setrequestheader( "Token", ls_token)
	ls_ws = "http://openiban.com/validate/"
	as_iban = trim(as_iban)
	luo_xhr.getJson(ls_ws + as_iban )
	
	ll_response_error = luo_xhr.send()
	
	if ll_response_error <> 200 then
		try 
		ls_response = luo_xhr.text
		luo_json.uof_get_json(ls_response, luo_response, as_error)

			if luo_response.valid then
				as_error = "codice IBAN valido"
				li_return = 1
			else
				as_error = "codice IBAN non valido"
				li_return = -1
			end if
				
			catch (Exception e)
				as_error = e.getmessage()
				li_return = -1
		end try
	
	else
		as_error = "Impossibile contattare il webservice. Sicuro di essere connesso?"
		li_return = -1
		
	end if
*/
return li_return
end function

public function integer uof_load_dddw (datawindow adw_dw, string as_column_name, string as_sql);/**
 * stefanop
 **/
 
string ls_modify
int li_error
datawindowchild ldw_child

li_error = adw_dw.GetChild(as_column_name, ldw_child)
if li_error <> 1 then
	g_mb.error("Errore durante il recupero della dwchild per la colonna " + g_str.safe(as_column_name))
	return -1
end if 

li_error = ldw_child.SetTransObject(sqlca)
if li_error <> 1 then
	g_mb.error("Errore durante l'impostazione della transazione nella dwchild per la colonna " + g_str.safe(as_column_name))
	return -1
end if 

as_sql = g_str.quote(as_sql)
ls_modify = "DataWindow.Table.Select='" + as_sql  + "'"
ls_modify = ldw_child.Modify(ls_modify)
if ls_modify <> "" then
	g_mb.error("Errore durante l'impostazione del'SQL nella dwchild per la colonna " + g_str.safe(as_column_name))
	return -1
end if

li_error = ldw_child.Retrieve()
if li_error < 0 then
	g_mb.error("Errore durante la retrieve della dwchld per la colonna " + g_str.safe(as_column_name), sqlca)
	return -1
end if

return li_error
end function

public function long uof_crea_ds (ref datastore fds_data, string fs_sql, transaction ft_trans, ref string fs_errore);string ls_error
long ll_ret

fs_sql = ft_trans.syntaxfromsql(fs_sql, "style(type=grid)", ls_error)
if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	fs_errore = "Impossibile convertire stringa SQL per il datastore.~r~n" + ls_error
	return -1
end if

fds_data = create uo_ds
fds_data.create(fs_sql, ls_error)
if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	destroy fds_data;
	fs_errore = "Impossibile creare datastore.~r~n" + ls_error
	return -1
end if

fds_data.settransobject(ft_trans)

ll_ret = fds_data.retrieve()


return ll_ret

end function

public function boolean uof_get_zebra_printer (ref string as_current_printer, ref string as_zebra_printer);/**
 * stefanop
 * 28/01/2016
 *
 * Recupero il nome della stampante corrente e quella Zebra impostata nei parametri
 * utenti PZN
 **/
 
as_current_printer = PrintGetPrinter()
as_current_printer = left(as_current_printer, pos(as_current_printer, "~t") - 1)

select stringa
into :as_zebra_printer
from parametri_azienda_utente
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 cod_parametro = 'ZPN';
		 
if sqlca.sqlcode <> 0 then
	as_zebra_printer = as_current_printer
	return false
else
	 return true
end if
end function

public function boolean uof_imposta_stampante (string as_printer_name);/**
 * stefanop
 * 26/01/2016
 *
 * Imposto la stampante corrente e la visualizzo nel menu
 **/
 
if PrintSetPrinter(as_printer_name) = 1 then
	w_cs_xx_mdi.im_menu_cs.m_file.m_stampa.text =  "&Stampa su "  + as_printer_name
	return true
else
	return false
end if
end function

public function integer uof_get_offerte_web (ref string as_cod_tipo_offven[]);/**
 * stefnaop
 * 2016/04/01
 *
 * Recupero i codici delle offerte di vendita in formato array
 **/
 
string ls_tow

select cod_tipo_off_ven_sfuso_ita, cod_tipo_off_ven_sfuso_cee, cod_tipo_off_ven_sfuso_excee,
		cod_tipo_off_ven_finito_ita, cod_tipo_off_ven_finito_cee, cod_tipo_off_ven_finito_excee
into 	:as_cod_tipo_offven[1], :as_cod_tipo_offven[2], :as_cod_tipo_offven[3],
		:as_cod_tipo_offven[4], :as_cod_tipo_offven[5], :as_cod_tipo_offven[6]
from con_vendite;


uof_get_parametro_azienda( "TOW", ls_tow)
if g_str.isnotempty(ls_tow) then
	as_cod_tipo_offven[7] = ls_tow
end if

return upperbound(as_cod_tipo_offven)
end function

public function integer uof_git_test ();/**
 * prova
 **/
 
 return 1
end function

public function string uof_data_db_tran (datetime adt_date, n_tran atr_transaction);// se ORACLE devo usare il suo formato data
choose case upper(left(atr_transaction.dbms,2)) 
	case "OR","O9","O10","ORA"
		if len(atr_transaction.oracle_dateformat) > 0 then 
			return string(adt_date, atr_transaction.oracle_dateformat)
		end if
	case else
		return string(adt_date, s_cs_xx.db_funzioni.formato_data)
end choose
end function

public function integer uof_get_note_fisse (string fs_cod_cliente, string fs_cod_fornitore, string fs_cod_prodotto, string fs_tipo_gestione, string fs_cod_tipo_doc, datetime fdt_data_registrazione, ref string fs_nota_testata, ref string fs_nota_piede, ref string fs_nota_video);/* EnMe 01-08-2017
Gestione unificata delle note fisse per cliente, fornitore, prodotto e per tipo documento.
L'errore non viene più mostrato con messagebox, ma viene portato e gestito fuori.
L'eventuale messaggio di ERRORE viene passato nel parametro FS_NOTA_TESTATA
*/

string ls_sql, ls_nota_fissa, ls_flag_piede_testata, ls_cod_nota_fissa, ls_cod_lingua, ls_descrizione_lingua, ls_flag_tipo_fat_ven, ls_errore, &
		ls_flag_note_fisse_prodotto
long ll_rows, ll_i
datastore lds_data

fs_nota_testata = ""
fs_nota_piede = ""
fs_nota_video = ""

if pos(fs_tipo_gestione,"_ACQ") > 0 then
	select flag_note_fisse_prodotto 
	into	:ls_flag_note_fisse_prodotto 
	from	con_acquisti
	where cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode = 0 and ls_flag_note_fisse_prodotto = "N" then return 0
else
	select flag_note_fisse_prodotto 
	into	:ls_flag_note_fisse_prodotto 
	from	con_vendite
	where cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode = 0 and ls_flag_note_fisse_prodotto = "N" then return 0
end if

ls_sql = "select nota_fissa , flag_piede_testata, cod_nota_fissa from tab_note_fisse  where cod_azienda = '" + s_cs_xx.cod_azienda + "' and ('" + string(fdt_data_registrazione, s_cs_xx.db_funzioni.formato_data) + "' between data_inizio and data_fine ) and  flag_blocco = 'N' and "

//----------------------------------------------------------------------------------------------------------------------------------------------------------------
//se sono vuoti SIA il cliente CHE il fornitore (es. DDT verso Contatto) deve caricare solo le note senza cliente & senza fornitore impostato
//con lo script che c'era prima (che è stato commentato più sotto) praticamente ignorava la calusola e caricava tutte le note fisse
if len(fs_cod_cliente) < 1 and len(fs_cod_fornitore) < 1 and len(fs_cod_prodotto) < 1 then
	ls_sql += " cod_cliente is null and cod_fornitore is null and cod_prodotto is null and"
else
	//almeno uno tra cliente, fornitore e prodotto è impostato
	if not isnull(fs_cod_cliente) then ls_sql = ls_sql + "( cod_cliente = '" + fs_cod_cliente + "' or (cod_cliente is null and cod_fornitore is null and cod_prodotto is null) ) and "
	if not isnull(fs_cod_fornitore) then ls_sql = ls_sql + "( cod_fornitore = '" + fs_cod_fornitore + "' or (cod_fornitore is null and cod_cliente is null and cod_prodotto is null) ) and "
	if not isnull(fs_cod_prodotto) then ls_sql = ls_sql + "( cod_prodotto = '" + fs_cod_prodotto + "' ) and "
end if


choose case fs_tipo_gestione
	case "OFF_ACQ"
		ls_sql = ls_sql + "	flag_offerta_acq = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'N' "

	case "ORD_ACQ"
		ls_sql = ls_sql + "	flag_ordine_acq = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'N' "
		
	case "BOL_ACQ"
		ls_sql = ls_sql + "	flag_bolla_acq = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'N' "
		
	case "TRAT_VEN"
		ls_sql = ls_sql + "	flag_trattativa_ven = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'N' "
		
	case "OFF_VEN"
		ls_sql = ls_sql + "	flag_offerta_ven = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'N' "
		
	case "ORD_VEN"
		ls_sql = ls_sql + "	flag_ordine_ven = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'N' "
		
	case "BOL_VEN"
		ls_sql = ls_sql + "	flag_bolla_ven = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'N' "
		
	case "FAT_VEN"
		if len(fs_cod_tipo_doc) > 0 then
			select flag_tipo_fat_ven
			into   :ls_flag_tipo_fat_ven
			from   tab_tipi_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_fat_ven = :fs_cod_tipo_doc;
			if sqlca.sqlcode < 0 then
				fs_nota_testata = g_str.format("Errore SQL in controllo tipo fattura di vendita: $1",sqlca.sqlerrtext)
				return -1
			elseif sqlca.sqlcode = 100 then
				fs_nota_testata = g_str.format("Il tipo fattura $1 richiesto per la ricerca note fisse non esiste",fs_cod_tipo_doc)
				return -1
			else
				if ls_flag_tipo_fat_ven = "P" then
					ls_sql = ls_sql + "	flag_fattura_proforma = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'N' "
				else
					ls_sql = ls_sql + "	flag_fattura_ven = 'S' and flag_uso_gen_doc = 'N'  and flag_usa_stampa_doc = 'N' and ( cod_tipo_fat_ven = '" + fs_cod_tipo_doc + "' or cod_tipo_fat_ven is null ) "
				end if
			end if
		else			
			ls_sql = ls_sql + "	flag_fattura_ven = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'N' "
		end if
	case "FAT_PRO"
		ls_sql = ls_sql + "	flag_fattura_proforma = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'N' "

	case "GEN_OFF_ACQ"
		ls_sql = ls_sql + "	flag_offerta_acq = 'S' and flag_uso_gen_doc = 'S' and flag_usa_stampa_doc = 'N' "
		
	case "GEN_ORD_ACQ"
		ls_sql = ls_sql + "	flag_ordine_acq = 'S' and flag_uso_gen_doc = 'S' and flag_usa_stampa_doc = 'N' "
		
	case "GEN_BOL_ACQ"
		
		ls_sql = ls_sql + "	flag_bolla_acq = 'S' and flag_uso_gen_doc = 'S' and flag_usa_stampa_doc = 'N' "
	case "GEN_OFF_VEN"
		ls_sql = ls_sql + "	flag_offerta_ven = 'S' and flag_uso_gen_doc = 'S' and flag_usa_stampa_doc = 'N' "
		
	case "GEN_ORD_VEN"
		ls_sql = ls_sql + "	flag_ordine_ven = 'S' and flag_uso_gen_doc = 'S' and flag_usa_stampa_doc = 'N' "
		
	case "GEN_BOL_VEN"
		ls_sql = ls_sql + "	flag_bolla_ven = 'S' and flag_uso_gen_doc = 'S' and flag_usa_stampa_doc = 'N' "
		
	case "GEN_FAT_VEN"
		if len(fs_cod_tipo_doc) > 0 then
			select flag_tipo_fat_ven
			into   :ls_flag_tipo_fat_ven
			from   tab_tipi_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_fat_ven = :fs_cod_tipo_doc;
			if sqlca.sqlcode < 0 then
				fs_nota_testata = g_str.format("Errore SQL in controllo tipo fattura di vendita: $1",sqlca.sqlerrtext)
				return -1
			elseif sqlca.sqlcode = 100 then
				fs_nota_testata =  g_str.format("Il tipo fattura $1 richiesto per la ricerca note fisse non esiste",fs_cod_tipo_doc)
				return -1
			else
				if ls_flag_tipo_fat_ven = "P" then
					ls_sql = ls_sql + "	flag_fattura_proforma = 'S' and flag_uso_gen_doc = 'S' and flag_usa_stampa_doc = 'N' "
				else
					ls_sql = ls_sql + "	flag_fattura_ven = 'S' and flag_uso_gen_doc = 'S'  and flag_usa_stampa_doc = 'N' and ( cod_tipo_fat_ven = '" + fs_cod_tipo_doc + "' or cod_tipo_fat_ven is null ) "
				end if
			end if
		else			
			ls_sql = ls_sql + "	flag_fattura_ven = 'S' and flag_uso_gen_doc = 'S' and flag_usa_stampa_doc = 'N' "
		end if		
	case "GEN_FAT_PRO"
		ls_sql = ls_sql + "	flag_fattura_proforma = 'S' and flag_uso_gen_doc = 'S' and flag_usa_stampa_doc = 'N' "
		
	case "STAMPA_OFF_ACQ"
		ls_sql = ls_sql + "	flag_offerta_acq = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
		
	case "STAMPA_ORD_ACQ"
		ls_sql = ls_sql + "	flag_ordine_acq = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
		
	case "STAMPA_BOL_ACQ"
		ls_sql = ls_sql + "	flag_bolla_acq = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
		
	case "STAMPA_TRAT_VEN"
		ls_sql = ls_sql + "	flag_trattativa_ven = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
		
	case "STAMPA_OFF_VEN"
		ls_sql = ls_sql + "	flag_offerta_ven = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
		
	case "STAMPA_ORD_VEN"
		ls_sql = ls_sql + "	flag_ordine_ven = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
		
	case "STAMPA_CONF_ORD_VEN"
		ls_sql = ls_sql + "	flag_conferma_ord_ven = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
		
	case "STAMPA_BOL_VEN"
		ls_sql = ls_sql + "	flag_bolla_ven = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
		
	case "STAMPA_FAT_VEN"
		if len(fs_cod_tipo_doc) > 0 then
			select flag_tipo_fat_ven
			into   :ls_flag_tipo_fat_ven
			from   tab_tipi_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_fat_ven = :fs_cod_tipo_doc;
			if sqlca.sqlcode < 0 then
				fs_nota_testata = g_str.format("Errore SQL in controllo tipo fattura di vendita: $1",sqlca.sqlerrtext)
				return -1
			elseif sqlca.sqlcode = 100 then
				fs_nota_testata =  g_str.format("Il tipo fattura $1 richiesto per la ricerca note fisse non esiste",fs_cod_tipo_doc)
				return -1
			else
				if ls_flag_tipo_fat_ven = "P" then
					ls_sql = ls_sql + "	flag_fattura_proforma = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
				else
					ls_sql = ls_sql + "	flag_fattura_ven = 'S' and flag_uso_gen_doc = 'N'  and flag_usa_stampa_doc = 'S' and ( cod_tipo_fat_ven = '" + fs_cod_tipo_doc + "' or cod_tipo_fat_ven is null ) "
				end if
			end if
		else			
			ls_sql = ls_sql + "	flag_fattura_ven = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
		end if
	case "STAMPA_FAT_PRO"
		ls_sql = ls_sql + "	flag_fattura_proforma = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
		
	case "STAMPA_ORD_PROD"
		ls_sql = ls_sql + "	flag_ordine_prod = 'S' and flag_uso_gen_doc = 'N' and flag_usa_stampa_doc = 'S' "
end choose

ll_rows = uof_crea_datastore( lds_data, ls_sql, ref ls_errore)
if ll_rows < 0 then
	fs_nota_testata = "Errore in creazione datastore note fisse.~r~n" + ls_errore
	return -1
end if

for ll_i = 1 to ll_rows
	
	ls_nota_fissa = lds_data.getitemstring(ll_i,1)
	ls_flag_piede_testata = lds_data.getitemstring(ll_i,2)
	ls_cod_nota_fissa = lds_data.getitemstring(ll_i,3)
	
	// EnMe 15-07-2015 per gestione note fisse in lingua
	setnull(ls_cod_lingua)
	if not isnull(fs_cod_cliente) and len(fs_cod_cliente) > 0 then
		// verifico se esiste una nota in lingua per questo cliente
		select 	cod_lingua
		into		:ls_cod_lingua
		from		anag_clienti
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_cliente = :fs_cod_cliente;
	elseif	not isnull(fs_cod_fornitore) and len(fs_cod_fornitore) > 0 then	
		// verifico se esiste una nota in lingua per questo cliente
		select 	cod_lingua
		into		:ls_cod_lingua
		from		anag_fornitori
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_fornitore = :fs_cod_fornitore;
	end if
	
	if not isnull(ls_cod_lingua) then
		select 	descrizione_lingua
		into		:ls_descrizione_lingua
		from		tab_note_fisse_lingue
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_nota_fissa = :ls_cod_nota_fissa and
					cod_lingua = :ls_cod_lingua;
		if sqlca.sqlcode = 0 and len(ls_descrizione_lingua) > 0 then
			ls_nota_fissa = ls_descrizione_lingua
		end if
	end if
	// EnMe 15-07-2015: fine.
	
	if not isnull(ls_nota_fissa) then
		if ls_flag_piede_testata = "T" then // Testata
			if isnull(fs_nota_testata) or len(fs_nota_testata) < 1 then
				fs_nota_testata = ""
				fs_nota_testata = ls_nota_fissa
			else
				fs_nota_testata = fs_nota_testata + "~r~n" + ls_nota_fissa
			end if
		elseif ls_flag_piede_testata = "P" then // Piede
			if isnull(fs_nota_piede)  or len(fs_nota_piede) < 1 then
				fs_nota_piede = ""
				fs_nota_piede = ls_nota_fissa
			else
				fs_nota_piede = fs_nota_piede + "~r~n" + ls_nota_fissa
			end if
		elseif ls_flag_piede_testata = "V" then // Video
			if isnull(fs_nota_video) or len(fs_nota_video) < 1 then
				fs_nota_video = ""
				fs_nota_video = ls_nota_fissa
			else
				fs_nota_video = fs_nota_video + "~r~n" + ls_nota_fissa
			end if
		end if
	end if
	
next

destroy lds_data

return 0
end function

public function string uof_get_day_name_ita (integer as_day_number_ita);choose case as_day_number_ita
	case 1
		return "Lunedì"
	case 2
		return "Martedì"
	case 3
		return "Mercoledì"
	case 4
		return "Giovedì"
	case 5
		return "Venerdì"
	case 6
		return "Sabato"
	case 7
		return "Domenica"
	case else
		return "ERRORE"
end choose
return "ERRORE"
end function

public function integer uof_get_day_number_ita (date ad_date);// traforma il daynumber americano (1= domenica) in daynumber italiano
long ll_day

ll_day = daynumber(ad_date) - 1
if ll_day = 0 then ll_day = 7

return ll_day

		
end function

public function string uof_get_ip ();/* stefanop
 * 27/09/2010
 *
 * Recupera l'indirizzo IP della macchina
 * GetAdaptersInfo: http://msdn.microsoft.com/en-us/library/aa365917%28VS.85%29.aspx
 **/
 
string ls_description, ls_adapter, ls_ip
int li_adaptmax, li_adaptcnt, li_offset
ulong lul_rtn, lul_bufferLen
blob lblb_AdapterInfo, lblb_data

// call function to get buffer size
lul_rtn = GetAdaptersInfo(lblb_AdapterInfo, lul_bufferLen)
If lul_rtn = 111 Then
	
	// allocate buffer
	lblb_AdapterInfo = Blob(Space(lul_bufferLen/2))
	
	// call function to get data
	 lul_rtn = GetAdaptersInfo(lblb_AdapterInfo, lul_bufferLen)
	
	If lul_rtn = 0 Then
		// how many adapters?
		li_adaptmax = lul_bufferLen / 640
		
		For li_adaptcnt = 1 To li_adaptmax
			// calculate offset
			li_offset = (640 * (li_adaptcnt - 1))
						
			// get description
			// lblb_data = BlobMid(lblb_AdapterInfo, li_offset + 269, 132)
			// ls_description = String(lblb_data, EncodingAnsi!)
			
			// get adaptername
			// lblb_data = BlobMid(lblb_AdapterInfo, li_offset + 9, 260)
			// ls_adapter = String(lblb_data, EncodingAnsi!)
			
			// get ip address
			lblb_data = BlobMid(lblb_AdapterInfo, li_offset + 433, 16)
			ls_ip = String(lblb_data, EncodingAnsi!)
			
			if not isnull(ls_ip) or ls_ip = "" then return ls_ip
		Next
	end if
End If
 
return ""
end function

public function long uof_get_file_prog_mimetype (string as_file_ext);/**
  EnMe
  21/01/2019
 
  Data l'estensione di un file, mi torma il prog_mimetype preso dalla tabella
  
Return	  0: tutto ok; trovato
  			-1: Errore
 **/
 
string ls_file_ext
long 	ll_prog_mimetype

ls_file_ext = upper(as_file_ext)

SELECT prog_mimetype  
INTO   :ll_prog_mimetype  
FROM   tab_mimetype  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and estensione = :ls_file_ext  ;
if sqlca.sqlcode <> 0 then
	return -1
end if

return ll_prog_mimetype
end function

public function boolean uof_file_write_encoding (string as_filepath, string as_message, boolean ab_append, encoding a_encoding, ref string as_error);/**
 * stefanop
 * 29/11/2011
 *
 * Funzione che scrive all'interno di un file la stringa passata per parametro
 **/
int li_handle, li_risposta
 
if ab_append then
	li_handle = fileopen(as_filepath, linemode!, Write!, LockWrite!, Append!,a_encoding)
else
	li_handle = fileopen(as_filepath, linemode!, Write!, LockWrite!, Replace!,a_encoding)
end if

if li_handle = -1 then
	as_error = "Errore durante l'apertura del file " + as_filepath + ".~r~nVerificare le connessioni delle unità di rete"
	return false
end if

li_risposta = filewrite(li_handle, as_message)
fileclose(li_handle)

return li_risposta >= 0
end function

public function long uof_string_to_date_epoc (string as_text);long ll_seconds
datetime  ldt_datetime
date		ld_date

ldt_datetime = datetime(date(as_text), 00:00:00)
ld_date = date(as_text)

choose case f_db()
	case "ORACLE"
//		SELECT UTC_TO_DATE (1463533832) FROM DUAL
//		SELECT date_to_utc(ld_date) 
//		FROM DUAL
//		using sqlca;
	case else		
		select DATEDIFF(SECOND, '19700101', :ldt_datetime)
		into	:ll_seconds
		from	aziende;
end choose

return ll_seconds
end function

public function string uof_get_file_mimetype (string as_file_ext);/**
  EnMe
  21/01/2019
 
  Data l'estensione di un file, mi torma il mimetype preso dalla tabella
  
Return	  0: tutto ok; trovato
  			-1: Errore
 **/
 
string ls_file_ext, ls_mimetype

// provo con maiuscolo
ls_file_ext = upper(as_file_ext)

SELECT mimetype  
INTO   :ls_mimetype  
FROM   tab_mimetype  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and estensione = :ls_file_ext  ;
choose case  sqlca.sqlcode
	case 0
		return ls_mimetype
	case 100
		ls_file_ext = lower(as_file_ext)
		
		SELECT mimetype  
		INTO   :ls_mimetype  
		FROM   tab_mimetype  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda and estensione = :ls_file_ext  ;
		
		if sqlca.sqlcode = 0 then
			return ls_mimetype
		else
			return ""
		end if
	case else
		return ""
end choose

return ""
end function

on uo_functions.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_functions.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

