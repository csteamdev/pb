﻿$PBExportHeader$uo_google_qrcode_result.sru
forward
global type uo_google_qrcode_result from internetresult
end type
end forward

global type uo_google_qrcode_result from internetresult
end type
global uo_google_qrcode_result uo_google_qrcode_result

type variables
blob	ib_result
end variables

forward prototypes
public function integer internetdata (blob data)
end prototypes

public function integer internetdata (blob data);ib_result = data

return 1
end function

on uo_google_qrcode_result.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_google_qrcode_result.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

