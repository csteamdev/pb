﻿$PBExportHeader$uo_json.sru
forward
global type uo_json from nonvisualobject
end type
end forward

global type uo_json from nonvisualobject
end type
global uo_json uo_json

forward prototypes
public function integer uof_get_json (string as_text, ref oleobject ao_json, ref string as_error) throws exception
public function integer uof_get_json_2 (string as_text, ref oleobject ao_json, ref string as_error) throws exception
public subroutine parse (string as_text, ref oleobject ao_json) throws jsonexception
end prototypes

public function integer uof_get_json (string as_text, ref oleobject ao_json, ref string as_error) throws exception;oleobject wsh
string ls_value
int li_result

wsh = CREATE oleobject
wsh.ConnectToNewObject( "MSScriptControl.ScriptControl" )
wsh.Language = "JScript"
wsh.AllowUI = false

// TODO aggiungere controlli aggiuntivi
//as_text = left(as_text, len(as_text) - 1)

ao_json = wsh.Eval('(' + as_text + ')')

if isnull(ao_json) or not isvalid(ao_json) then
	li_result = -1
else
	li_result = 1
end if

wsh.disconnectobject( )
destroy wsh

return li_result
end function

public function integer uof_get_json_2 (string as_text, ref oleobject ao_json, ref string as_error) throws exception;oleobject wsh
string ls_value

wsh = CREATE oleobject
wsh.ConnectToNewObject( "MSScriptControl.ScriptControl" )
wsh.Language = "JScript"
wsh.AllowUI = false

// TODO aggiungere controlli aggiuntivi
//as_text = left(as_text, len(as_text) - 1)

ao_json = wsh.Eval('(JSON.parse(' + as_text + '))')

if isnull(ao_json) or not isvalid(ao_json) then
	return -1
end if

return 1
end function

public subroutine parse (string as_text, ref oleobject ao_json) throws jsonexception;string ls_value
int li_result
oleobject wsh
jsonException le_exception

if g_str.isempty(as_text) then
	setnull(ao_json)
end if

try
	wsh = CREATE oleobject
	wsh.ConnectToNewObject( "MSScriptControl.ScriptControl" )
	wsh.Language = "JScript"
	wsh.AllowUI = false
	
	ao_json = wsh.Eval('(' + as_text + ')')
		
	wsh.disconnectobject( )
catch(Exception e)
	le_exception = create jsonException
	le_exception.setMessage("Errore durante la decodifica JSON")
	throw le_exception
catch(RuntimeError er)
	le_exception = create jsonException
	le_exception.setMessage("Errore durante la decodifica JSON")
	throw le_exception
finally
	destroy wsh
end try
	


end subroutine

on uo_json.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_json.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

