﻿$PBExportHeader$uo_licenze.sru
forward
global type uo_licenze from nonvisualobject
end type
end forward

global type uo_licenze from nonvisualobject
end type
global uo_licenze uo_licenze

type prototypes
Function ulong GetAdaptersInfo (ref blob pAdapterInfo, ref ulong pOutBufLen) Library "iphlpapi.dll" Alias For "GetAdaptersInfo"
end prototypes

type variables
private:
	// indirizzo email da avvisare in caso di licenze esaurite
	string is_email_alert = "assistenza@csteam.com"
	string is_client_adapter = ""
	string is_client_adapter_encoded = ""
	string is_codindividuale_encoded = ""
	uo_crypto iuo_crypto
	
	// variabili di istanza
	int ii_license = -1
	date idt_last_login_date
	date idt_end_license_date
	
	constant string SECURE_KEY = "csteamBest"
	constant int MAX_RETRY = 3
	
	// Tipi licenze
	constant int NONE = 1 // nessun controllo di licenze
	constant int TRIAL = 2 // periodo di prova
	constant int LICENSE = 3 // numero di licenze attivo
	

	
end variables

forward prototypes
public function integer uof_accesso_v2 (ref string fs_messaggio)
public function string uof_get_ip ()
public function boolean uof_process_activation (blob ablb_encoded_activation_string)
private function boolean uof_set_params (integer ai_license_type, integer ai_license_number)
public function string uof_gen_seriale (string as_adapter, date adt_end_date, integer ai_license, integer ai_n_license, ref string as_error)
public function integer uof_cloud_license (string as_license_code)
public subroutine uof_set_acl (string as_value)
public function integer uof_grace_time ()
public subroutine uof_lock_client ()
public subroutine uof_unlock_client ()
end prototypes

public function integer uof_accesso_v2 (ref string fs_messaggio);/**
 * stefanop
 * 27/09/2010
 *
 * Nuova funzione di controllo.
 * Viene usata la tabella sessioni per memorizzare IP e cod utente per verificare il numero corretto di licenze in uso
 **/

string ls_ip_address, ls_sql, ls_attachment[], ls_azienda, ls_license, ls_error
int li_num_max_license, li_license_active
datetime ldt_now
datastore lds_store

// leggo il numero di licenze disponibili
select numero
into :li_num_max_license
from parametri
where cod_parametro = 'LIC';

if sqlca.sqlcode <> 0 or isnull(li_num_max_license) or li_num_max_license = 0 then
	fs_messaggio =  "Numero licenze disponibili non valido."
	return -1
end if

// sessioni aperte
ls_sql = "SELECT distinct cod_utente, ip_address FROM sessioni WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "'"
if not f_crea_datastore(ref lds_store, ls_sql) then
	g_mb.error("", "Errore durante la creazione del datastore per il controllo licenze")
	return -1
end if

// variabili utente
ls_ip_address = this.uof_get_ip()
gdt_session_start_at = datetime(today(), now())

if lds_store.retrieve() >= li_num_max_license then
	f_log_sistema("Superato il numero massimo di licenze", "LIC")
	
	if g_mb.confirm("", "E' stato superato il numero di licenze disponibili.~r~nInviare una mail di segnalazione e proseguire con l'avvio dell'applicazione?") then
		uo_outlook luo_mail
		luo_mail = create uo_outlook
		
		select rag_soc_1
		into :ls_azienda
		from aziende
		where
			cod_azienda = :s_cs_xx.cod_azienda;
			
		if sqlca.sqlcode <> 0 or isnull(ls_azienda) or ls_azienda = "" then ls_azienda = ""
		
		luo_mail.uof_invio_sendmail({is_email_alert}, "Superato limite licenze - " + ls_azienda, "Attenzione superato il limite massimo delle licenze acquistate", ls_attachment)
		
	else
		setnull(fs_messaggio)
		return -1
	end if
end if

// Scrivo accesso
insert into sessioni	(
	cod_azienda,
	cod_utente,
	ip_address,
	start_at)
values (
	:s_cs_xx.cod_azienda,
	:s_cs_xx.cod_utente,
	:ls_ip_address,
	:gdt_session_start_at
);

// aggiorno tabella licenza
ls_license = string(today(), "dd/MM/yyyy") + string(idt_end_license_date, "dd/MM/yyyy")
ls_license = this.iuo_crypto.uof_encrypt(ls_license, SECURE_KEY)
if this.iuo_crypto.uof_error(ref ls_error) then
	g_mb.error("Licenze", "Errore durante la codifica della licenza in uso")
	return -1
end if

update registrazione
set cod_individuale = :ls_license
where
	num_serie = :is_client_adapter_encoded;

commit;

return 0
end function

public function string uof_get_ip ();/**
 * stefanop
 * 27/09/2010
 *
 * Recupera l'indirizzo IP della macchina
 * GetAdaptersInfo: http://msdn.microsoft.com/en-us/library/aa365917%28VS.85%29.aspx
 **/
 
string ls_description, ls_adapter, ls_ip
int li_adaptmax, li_adaptcnt, li_offset
ulong lul_rtn, lul_bufferLen
blob lblb_AdapterInfo, lblb_data

// call function to get buffer size
lul_rtn = GetAdaptersInfo(lblb_AdapterInfo, lul_bufferLen)
If lul_rtn = 111 Then
	
	// allocate buffer
	lblb_AdapterInfo = Blob(Space(lul_bufferLen/2))
	
	// call function to get data
	lul_rtn = GetAdaptersInfo(lblb_AdapterInfo, lul_bufferLen)
	
	If lul_rtn = 0 Then
		// how many adapters?
		li_adaptmax = lul_bufferLen / 640
		
		For li_adaptcnt = 1 To li_adaptmax
			// calculate offset
			li_offset = (640 * (li_adaptcnt - 1))
						
			// get description
			// lblb_data = BlobMid(lblb_AdapterInfo, li_offset + 269, 132)
			// ls_description = String(lblb_data, EncodingAnsi!)
			
			// get adaptername
			// lblb_data = BlobMid(lblb_AdapterInfo, li_offset + 9, 260)
			// ls_adapter = String(lblb_data, EncodingAnsi!)
			
			// get ip address
			lblb_data = BlobMid(lblb_AdapterInfo, li_offset + 433, 16)
			ls_ip = String(lblb_data, EncodingAnsi!)
			
			if not isnull(ls_ip) or ls_ip = "" then return ls_ip
		Next
	end if
End If
 
return ""
end function

public function boolean uof_process_activation (blob ablb_encoded_activation_string);/**
 * stefanop
 * 29/09/2010
 *
 * Controllo che il codice inviato sia corretto e elaboro le informazioni inviate
 * La stringa dove contenere
 * 1. Adapter valido non cifrato
 * 2. Data ultimo login nell'applicativo
 * 3. Data termine validità applicativo
 * 4. Tipo di licenza acquistata
 * 5. Numero licenze acquistate
 **/

string ls_data, ls_actived_adapter, ls_client_adapter_encoded, ls_test, ls_error
int li_pos, li_license_type, li_license_number
date ldt_data_1, ldt_data_2

if len(ablb_encoded_activation_string) < 1 then return false

ls_data = this.iuo_crypto.uof_decrypt(ablb_encoded_activation_string, SECURE_KEY)
if len(ls_data) < 1 or this.iuo_crypto.uof_error(ref ls_error) then
	g_mb.error("Licenze", "Errore durante il controllo del codice seriale")
	return false
end if

ls_actived_adapter = left(ls_data, len(is_client_adapter))
if ls_actived_adapter <> is_client_adapter then
	// l'adatper validato è diverso da quello installato nel sistema quindi codice non valido
	return false
end if

ls_data = mid(ls_data, len(is_client_adapter) + 1)
 
// recupero data ultimo login
ldt_data_1 = date(left(ls_data, 10))

// recuper data termine licenze
ldt_data_2 = date(mid(ls_data, 11, 10))

// Tipo licenza
li_license_type = integer(mid(ls_data, 21, 1))

// Numero licenze
li_license_number = integer(mid(ls_data, 22, 1))

if li_license_type = LICENSE then
	// TODO: impostare parametri licenza
	this.uof_set_params(li_license_type, li_license_number)
	
elseif li_license_type = TRIAL then
	if ldt_data_2 < today() then
		// periodo di prova terminato
		return false
	end if
end if

// salvo solo le date di login e fine
ls_data = mid(ls_data, 1, 20)
ls_client_adapter_encoded = this.iuo_crypto.uof_encrypt(is_client_adapter, SECURE_KEY)
is_codindividuale_encoded = this.iuo_crypto.uof_encrypt(ls_data, SECURE_KEY)

insert into registrazione (
	nome,
	societa,
	num_serie,
	cod_individuale )
values (
	:s_cs_xx.parametri.parametro_s_1,
	:s_cs_xx.parametri.parametro_s_2,
	:ls_client_adapter_encoded,
	:is_codindividuale_encoded);
	
if sqlca.sqlcode <> 0 then
	rollback;
	return false
else
	commit;
	return true
end if
end function

private function boolean uof_set_params (integer ai_license_type, integer ai_license_number);/**
 * stefanop
 * 30/09/2010
 *
 * Imposto i parametri delle licenze all'interno del software
 **/
 
string ls_test, ls_license
int li_test

ls_license = "A" + string(ai_license_type)

// controllo se esiste il parametro ACL
select stringa
into :ls_test
from parametri
where
	cod_parametro = 'ACL' and
	flag_parametro = 'S';
	
if sqlca.sqlcode = 100 then
	// non esiste, quindi inserisco
	insert into parametri (
		flag_parametro,
		cod_parametro,
		stringa )
	values (
		'S',
		'ACL',
		:ls_license);
else
	// aggiorno
	update parametri
	set stringa = :ls_license
	where
		cod_parametro = 'S' and
		flag_parametro = 'ACL';
end if

// controllo esistenza parametro LIC
select numero
into :li_test
from parametri
where
	cod_parametro = 'LIC' and
	flag_parametro = 'N';
	
if sqlca.sqlcode = 100 then
	// non esiste, quindi inserisco
	insert into parametri (
		flag_parametro,
		cod_parametro,
		stringa )
	values (
		'N',
		'LIC',
		:ai_license_number);
else
	// aggiorno
	update parametri
	set numero = :ai_license_number
	where
		cod_parametro = 'N' and
		flag_parametro = 'LIC';
end if

commit;

return true
end function

public function string uof_gen_seriale (string as_adapter, date adt_end_date, integer ai_license, integer ai_n_license, ref string as_error);/**
 * stefnaop
 * 04/10/2010
 *
 * Genera un seriale di attivazione per il prodotto
 * Stringa che forma il seriale
 * 1. codice adattatore client (racchiuso tra { })
 * 2. data ultimo login (10 caratteri)
 * 3. data di validità software (10 caratteri)
 * 4. tipo licenza (1 carattere)
 * 5. numero licenze (restanti caratteri)
 *
 * Es: {AB3EC8A1-41C3-4311-9E82-0C9F7E20DC91}01/01/190010/10/2010325
 *
 * ATTENZIONE: la secure key NON deve essere cambiata MAI!!!!
 **/
 
string ls_key

// controllo
as_error = ""
if len(as_adapter) < 5 then
	as_error = "Il codice di attivazione non è valido"
	return ""
end if

if today() >= adt_end_date then
	as_error = "La data di validità deve essere maggiore della data odierna"
	return ""
end if

if isnull(ai_license) or ai_license < 1 or ai_license > 3 then ai_license = 1
if isnull(ai_n_license) or ai_n_license < 1 or ai_n_license > 9999 then ai_license = 1

// punto 1
ls_key = "{" + as_adapter + "}" 

// punto 2
ls_key += "01/01/1900"

// punto 3
ls_key += string(adt_end_date, "dd/MM/yyyy")

// punto 4
ls_key += string(ai_license)

// punto 5
ls_key += string(ai_n_license)

return iuo_crypto.uof_encrypt(ls_key, SECURE_KEY)
end function

public function integer uof_cloud_license (string as_license_code);// EnMe 17-07-2018 Controllo Licenza Cloud
/*
string		as_license_code = partita iva cliente

RETURN		Integer	1 (true) 	= Licenza OK
							-1(error)	= Licenza Scaduta
							-2(errore tecnico= La risposta Json non è un valore fra quelli ammessi
							-3 licenza mancante o manomissione
							0 (false)	= Impossibile Controllare la licenza
*/

n_winhttp ln_http
String ls_URL, ls_title, ls_pathname, ls_filename, ls_token, ls_license_active, ls_license_status, ls_response
String ls_filter, ls_mimetype, ls_data, ls_license_crtypted, ls_validation_code
Integer li_rc, li_fnum
ULong lul_length
Any	la_temp
sailjson luo_parse, luo_sendjson
uo_crypto luo_crypto
uo_secure_id luo_secure

if isnull(as_license_code) then as_license_code = " "

luo_crypto = create uo_crypto
ls_license_crtypted = luo_crypto.uof_sha_256( as_license_code)
ls_license_crtypted = upper(ls_license_crtypted)

ls_URL  = "https://license.csteam.com/authentication/login?username=csteam&password=CS.$e95"

ln_http.Open("POST", ls_URL)

lul_length = ln_http.Send(ls_data)

If lul_length > 0 Then
	
	luo_parse=create sailjson
	luo_sendjson=create sailjson
	
	luo_parse.parse( ln_http.ResponseText)

	// Chiedo la licenza
	try
		la_temp = luo_parse.getattribute( "token")
		ls_token = "Bearer " + string(la_temp)
	catch(Exception e)
		return -2
	end try
	
	ln_http.Open("POST", "https://license.csteam.com/license/status")
	
	ln_http.SetRequestHeader("Authorization", ls_token)
	ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")

	ls_data = '[{"code":"' + as_license_code + '","validationCode":"' + ls_license_crtypted + '"}]'

	lul_length = ln_http.Send(ls_data)
	If lul_length > 0 Then
		
		if left(ln_http.ResponseText,1) <> "[" then 
			// risposta con errore
			return -2
		else
			
			ls_response = mid(ln_http.ResponseText, 2, len(ln_http.ResponseText) -2)
		
			luo_parse.parse(ls_response)
			ls_license_status = string(luo_parse.getattribute( "status"))
			
			// La risposta non è valida. Probabilmente ci sono dati errati nel database delle licenze
			// Questo previene qualcuno che vuole metterisi n mezzo
			choose case ls_license_status
				case "NOT_FOUND","BREAD_FOX"
					return -3
			end choose
			ls_license_active = lower(string(luo_parse.getattribute( "actived")))
			ls_validation_code = string(luo_parse.getattribute( "validationCode"))
			
			
			ls_license_crtypted = luo_crypto.uof_sha_256( ls_license_crtypted + as_license_code)
			
			if ls_license_active = "true" and ls_validation_code <> ls_license_crtypted then 
				// licenza non verificata; probabilmente qualcuno sta cercando di fregarci!
				ls_license_active = "false"
			end if
			
			destroy luo_parse
			destroy luo_sendjson
			choose case ls_license_active
				case "true"
					return 1
				case "false"
					return 0
			end choose
		end if
	else
		// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
		return -1
	end if
	
	destroy luo_parse
	destroy luo_sendjson
	
Else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	return -1
End If

end function

public subroutine uof_set_acl (string as_value);string	ls_acl

select stringa
into	:ls_acl
from	parametri
where cod_parametro='ACL';

if sqlca.sqlcode <> 0 then
	insert into parametri (
		flag_parametro,
		cod_parametro,
		des_parametro,
		stringa,
		flag_ini)
	values
	('S',
	'ACL',
	'Default Formato Stampa',
	:as_value,
	'S');
	if sqlca.sqlcode <> 0 then
		halt close
	end if
else
	update parametri
	set stringa= :as_value
	where cod_parametro ='ACL';
end if

commit;

return 
end subroutine

public function integer uof_grace_time ();string	ls_crypt_data, ls_str, ls_data
date ldd_date_start, ldd_date_end
datetime ldt_datetime

DECLARE my_cursor DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM "SELECT getdate()" ;
OPEN DYNAMIC my_cursor ;
if sqlca.sqlcode < 0 then 
	ldd_date_end = today()
else
	FETCH my_cursor INTO :ldt_datetime ;
	CLOSE my_cursor ;
	ldd_date_end = date(ldt_datetime)
end if

select pwd_manuale
into	:ls_crypt_data
from sistema;

guo_functions.uof_decrypt(ls_crypt_data, ref ls_str) 

ls_data = mid(ls_str, 7, 8)

ls_data = right(ls_data,2) + "/" + mid(ls_data,5,2) + "/" + left(ls_data,4)

ldd_date_start = date( ls_data  )

if DaysAfter(ldd_date_start, ldd_date_end) > 7 then return -1

return 0

end function

public subroutine uof_lock_client ();string ls_numero

uof_set_acl("A3")
RegistrySet(s_cs_xx.chiave_root +  "database_" + s_cs_xx.profilocorrente, "logpass", "")
if RegistryGet( s_cs_xx.chiave_root_user +  "profilodefault", "numero", RegString!, ls_numero)	 = 1 and RegistryGet( s_cs_xx.chiave_root_user +  "profilocorrente", "numero", RegString!, ls_numero) = -1 then
	RegistrySet( s_cs_xx.chiave_root_user +  "profilocorrente", "numero", RegString!, ls_numero)		
end if

end subroutine

public subroutine uof_unlock_client ();delete parametri where cod_parametro = 'ACL';
commit;
RegistryDelete( s_cs_xx.chiave_root_user +  "profilocorrente", "numero")		

end subroutine

on uo_licenze.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_licenze.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iuo_crypto = create uo_crypto
end event

event destructor;destroy iuo_crypto
end event

