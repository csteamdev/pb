﻿$PBExportHeader$uo_log.sru
forward
global type uo_log from nonvisualobject
end type
end forward

global type uo_log from nonvisualobject
end type
global uo_log uo_log

type variables
constant int ERROR_LEVEL = 1
constant int WARN_LEVEL = 2
constant int LOG_LEVEL = 3
constant int INFO_LEVEL = 4
constant int DEBUG_LEVEL = 5


private:

	string is_file_path
	
	/*
	 * Formato da utilizzare per salvare l'ora nel file di log
	 * Utilizzare la funzione set_date_time_format()
	 * Esempio delle possibili formati:
	 * - hh:mm:ss => 18:54:29
	 * - dd/mm/yyyy => 03/08/212
	 * - dd/mm/yyyy hh:mm:ss => 03/08/212 18:54:29
	 **/
	string is_date_time_format = "hh:mm:ss"
	
	string is_separator = "~t"
	
	string is_sigla[] = {"ERROR", "WARNING", "LOG", "INFO", "DEBUG"}
	
	/*
	 * Di default loggo sempre tutto
	 * Se si vuole visualizzare solo determinati messaggi
	 * usare la funzione set_log_level
	 **/
	int ii_log_level = 5
	
	/**
	 * stefanop: 12/09/2014
	 * indica il livello di identazione del testo
	 **/
	 int ii_ident_level = 0
end variables

forward prototypes
public function integer log (string as_message)
public function integer debug (string as_message)
public function integer error (string as_message)
public function integer warning (string as_message)
public subroutine set_separator (string as_separator)
public subroutine open (string as_file_path)
public function integer info (string as_message)
public subroutine open (string as_file_path, boolean ab_clear)
public subroutine set_date_time_format (string as_date_time_format)
public subroutine set_file (string as_file_path, boolean ab_clear)
public subroutine set_file (string as_file_path)
public subroutine set_log_level (integer ai_log_level)
public function integer get_log_level ()
private function integer uof_write (string as_message, integer ai_level)
public function integer write (string as_message, integer ai_level)
private function integer uof_get_safe_level (integer ai_level)
private subroutine uof_fix_sigla ()
public function integer warn (string as_message)
public subroutine create_to_desktop (string as_file_name)
public function uo_log ident ()
public function uo_log outdent ()
end prototypes

public function integer log (string as_message);return uof_write(as_message, LOG_LEVEL)
end function

public function integer debug (string as_message);return uof_write(as_message, DEBUG_LEVEL)
end function

public function integer error (string as_message);return uof_write(as_message, ERROR_LEVEL)
end function

public function integer warning (string as_message);return uof_write(as_message, WARN_LEVEL)
end function

public subroutine set_separator (string as_separator);/**
 * stefanop
 * 03/08/2012
 *
 * Imposta il carattere di separazione tra l'ora, il livello ed il messaggio nel file di log.
 * DEFAULT = tab separetor
 **/
 
is_separator = as_separator
end subroutine

public subroutine open (string as_file_path);/**
 * stefanop
 * 03/08/2012
 *
 * Imposto il percorso del file dove salvare il log
 * DEPRECATED
 **/
 
 
open(as_file_path, false)
end subroutine

public function integer info (string as_message);return uof_write(as_message, INFO_LEVEL)
end function

public subroutine open (string as_file_path, boolean ab_clear);/**
 * stefanop
 * 03/08/2012
 *
 * Imposto il percorso del file dove salvare il log.
 *
 * @param as_file_path	Percorso del file di log
 * @param ab_clear		Indica se pulire il file prima di iniziare a scrivere
 *
 * DEPRECATED
 **/

int li_handle

is_file_path = as_file_path

if ab_clear then
	
	if  fileexists(is_file_path) then
		// Il file esiste quindi devo pulirlo.
		li_handle = fileopen(is_file_path, linemode!, Write!, LockWrite!, Replace!)
		
		if li_handle = -1 then return
		
		filewrite(li_handle, "")
		fileclose(li_handle)
		
	end if
	
end if
end subroutine

public subroutine set_date_time_format (string as_date_time_format);/**
 * stefanop
 * 03/08/2012
 **/
 
is_date_time_format = as_date_time_format
end subroutine

public subroutine set_file (string as_file_path, boolean ab_clear);/**
 * stefanop
 * 03/08/2012
 *
 * Imposto il percorso del file dove salvare il log.
 *
 * @param as_file_path	Percorso del file di log
 * @param ab_clear		Indica se pulire il file prima di iniziare a scrivere
 **/

string ls_dir
int li_handle

is_file_path = as_file_path
ls_dir = mid(as_file_path,1, LastPos(as_file_path, "\"))

if not directoryexists(ls_dir) then
	createdirectory(ls_dir)
end if

if ab_clear then
	
	if  fileexists(is_file_path) then
		// Il file esiste quindi devo pulirlo.
		li_handle = fileopen(is_file_path, linemode!, Write!, LockWrite!, Replace!)
		
		if li_handle = -1 then return
		
		filewrite(li_handle, "")
		fileclose(li_handle)
		
	end if
	
end if
end subroutine

public subroutine set_file (string as_file_path);/**
 * stefanop
 * 03/08/2012
 *
 * Imposto il percorso del file dove salvare il log
 **/
 
 
set_file(as_file_path, false)
end subroutine

public subroutine set_log_level (integer ai_log_level);/**
 * stefanop
 * 12/11/2012
 *
 * Imposta il livello di log da scrivere sul file,
 * deve essere compreso tra 1 (ERROR) e 5 (DEBUG)
 **/

ii_log_level = uof_get_safe_level(ai_log_level)
end subroutine

public function integer get_log_level ();/**
 * stefanop
 * 12/11/2012
 *
 * Ritorna il livello di log impostato.
 * Deve essere compreso tra 1 (ERROR) e 5 (DEBUG)
 **/
 
return ii_log_level
end function

private function integer uof_write (string as_message, integer ai_level);/**
 * stefanop
 * 03/08/2012
 *
 * Scrivo il contenuto nel file di log
 **/
 
string ls_ident_separator
int li_handle, ll_i

if ai_level <= ii_log_level then
 
	li_handle = fileopen(is_file_path, linemode!, Write!, LockWrite!, Append!)
	
	if li_handle = -1 then return -1
	
	if ii_ident_level < 1 then
		ls_ident_separator = ""
	else
		for ll_i = 1 to ii_ident_level
			ls_ident_separator += "~t"
		next
	end if
	
	as_message = string(datetime(today(), now()), is_date_time_format) + is_separator + is_sigla[ai_level] + is_separator + is_separator + ls_ident_separator + as_message
	
	filewrite(li_handle, as_message)
	fileclose(li_handle)

	return 0
else
	// Il log che si vuole scrivere non è compreso nel livello impostato
	return 1
end if


end function

public function integer write (string as_message, integer ai_level);
return uof_write(as_message, uof_get_safe_level(ai_level))
end function

private function integer uof_get_safe_level (integer ai_level);/**
 * stefanop
 * 12/11/2012
 *
 * Ritorna il numero di livello controllando che sia valido
 **/
 
if ai_level < 1 then
	ai_level = 1
elseif ai_level > upperbound(is_sigla) then
	ai_level =  upperbound(is_sigla)
end if

return ai_level
end function

private subroutine uof_fix_sigla ();/**
 * stefanop
 * 12/11/2012
 *
 * Controllo le sigle impostate e aggiungo gli spazi per fare in modo che siano all'ineate a destra
 * Ad esempio:
 * 	WARNING
 *	    ERROR
 *         INFO
 **/
 
 
int li_i, li_count, li_length, li_len

li_length = 0
li_count = upperbound(is_sigla)

// trovo lunghezza massima
for li_i = 1 to li_count
	
	li_len = len(is_sigla[li_i])
	if li_length < li_len then li_length = li_len
	
next

// Aggiunto spazi
for li_i = 1 to li_count
	
	is_sigla[li_i] = right("                    " + is_sigla[li_i], li_length)
	
next
end subroutine

public function integer warn (string as_message);return uof_write(as_message, WARN_LEVEL)
end function

public subroutine create_to_desktop (string as_file_name);/**
 * stefanop
 * 12/09/2014
 *
 * Crea il file di log nel desktop dell'utente collegato
 **/
 
string ls_desktop

ls_desktop = guo_functions.uof_get_user_documents_folder( )

set_file(ls_desktop + as_file_name, false)
end subroutine

public function uo_log ident ();/**
 * stefanop
 * 12/09/2014
 *
 * Aggiunge un livello di identazione (un tab) ai successivi log
 **/
 
ii_ident_level ++
return this
end function

public function uo_log outdent ();/**
 * stefanop
 * 12/09/2014
 *
 * Rimuove un livello di identazione (un tab) ai successivi log
 **/
 
ii_ident_level --

if ii_ident_level < 0 then
	ii_ident_level = 0
end if

return this
end function

on uo_log.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_log.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;uof_fix_sigla()
end event

