﻿$PBExportHeader$uo_log_sistema.sru
$PBExportComments$Oggetto Ereditato dalla uo_log.
forward
global type uo_log_sistema from uo_log
end type
end forward

global type uo_log_sistema from uo_log
end type
global uo_log_sistema uo_log_sistema

forward prototypes
public function integer uof_write_log_sistema (string as_tipo_log, string as_message)
public function integer uof_verify_process (string as_process_code_start, string as_process_code_end, ref string as_message)
public function integer uof_write_log_sistema_not_sqlca (string as_tipo_log, string as_message)
end prototypes

public function integer uof_write_log_sistema (string as_tipo_log, string as_message);/**
 * stefanop
 * 27/09/2010
 *
 * Permette di scrivere le informazioni di log nella tabella interna log_sistema.
 **/
 
string ls_sqlerrtext = ""
int li_sqlcode = 0
datetime ldt_today, ldt_now

li_sqlcode = sqlca.sqlcode
if li_sqlcode <> 0 then ls_sqlerrtext = sqlca.sqlerrtext


//ll_id_sistema++
ldt_today = datetime(today(), 00:00:00)
ldt_now = datetime(date("01/01/1900"), now())
if len(as_tipo_log) > 20 then left(as_tipo_log, 20)

//la tabella è autoincrementale
insert into log_sistema (
	data_registrazione,
	ora_registrazione,
	utente,
	flag_tipo_log,
	db_sqlcode,
	db_sqlerrtext,
	messaggio)
values (
	:ldt_today,
	:ldt_now,
	:s_cs_xx.cod_utente,
	:as_tipo_log,
	:li_sqlcode,
	:ls_sqlerrtext,
	:as_message);

if sqlca.sqlcode <> 0 then
	return -1
else
	return 0
end if
end function

public function integer uof_verify_process (string as_process_code_start, string as_process_code_end, ref string as_message);string	ls_utente, ls_des_utente
long		ll_cont
datetime ldt_oggi
ldt_oggi = datetime(today(),00:00:00)

select 	max(id_log_sistema)
into 		:ll_cont
from 		log_sistema
where 	flag_tipo_log = :as_process_code_end and data_registrazione >= :ldt_oggi ;

setnull(ls_utente)

if ll_cont > 0 and not isnull(ll_cont) then
	select 	max(utente)
	into		:ls_utente
	from 		log_sistema
	where	flag_tipo_log = :as_process_code_start and id_log_sistema > :ll_cont  ;
else
	select 	max(utente)
	into		:ls_utente
	from 		log_sistema
	where	flag_tipo_log = :as_process_code_start and data_registrazione >= :ldt_oggi  ;
end if

if not isnull(ls_utente) and len(ls_utente) > 0 and ls_utente <> s_cs_xx.cod_utente and upper(s_cs_xx.cod_utente) <> "CS_SYSTEM" then
	
	select 	nome_cognome
	into		:ls_des_utente
	from		utenti
	where 	cod_utente = :ls_utente;
	
	if isnull(ls_des_utente) then ls_des_utente = ls_utente + " <Sconosciuto>"
	
	as_message = g_str.format("Attenzione: l'utente $1 $2 ha già iniziato la procedura.~r~nL'esecuzione contemporanea può provocare errori.~r~nProcedo ugualmente?",ls_utente, ls_des_utente)
	return 1
	
end if

return 0
end function

public function integer uof_write_log_sistema_not_sqlca (string as_tipo_log, string as_message);/**
 * stefanop
 * 27/09/2010
 *
 * Permette di scrivere le informazioni di log nella tabella interna log_sistema.
 **/
 
string					ls_sqlerrtext = ""
int						li_sqlcode = 0
datetime				ldt_today, ldt_now
transaction			ltran_transaction


if not guo_functions.uof_create_transaction_from(sqlca, ltran_transaction, ls_sqlerrtext) then
	return -1
end if


//ll_id_sistema++
ldt_today = datetime(today(), 00:00:00)
ldt_now = datetime(date("01/01/1900"), now())
if len(as_tipo_log) > 20 then left(as_tipo_log, 20)

//la tabella è autoincrementale
insert into log_sistema (
	data_registrazione,
	ora_registrazione,
	utente,
	flag_tipo_log,
	db_sqlcode,
	db_sqlerrtext,
	messaggio)
values (
	:ldt_today,
	:ldt_now,
	:s_cs_xx.cod_utente,
	:as_tipo_log,
	:li_sqlcode,
	:ls_sqlerrtext,
	:as_message)
using ltran_transaction;

if ltran_transaction.sqlcode <> 0 then
	rollback using ltran_transaction;
	return -1
end if

commit using ltran_transaction;

disconnect using ltran_transaction;

destroy ltran_transaction

return 0
end function

on uo_log_sistema.create
call super::create
end on

on uo_log_sistema.destroy
call super::destroy
end on

