﻿$PBExportHeader$uo_mail.sru
$PBExportComments$Mail
forward
global type uo_mail from UserObject
end type
end forward

global type uo_mail from UserObject
int Width=1998
int Height=1213
boolean Border=true
long PictureMaskColor=25166016
long TabTextColor=33554432
long TabBackColor=67108864
end type
global uo_mail uo_mail

type variables
mailsession ims_sessione
mailreturncode imr_ritorno
mailfiledescription imf_allegato
mailmessage imm_messaggio
end variables

forward prototypes
public function string uf_decodifica (mailreturncode pmrc_ritorno)
public subroutine uf_termina ()
public function boolean uf_inizializza ()
public function boolean uf_invia (string ps_destinatario[], string ps_oggetto, string ps_nota, string ps_allegato[], boolean pb_lettura)
public function boolean uf_leggi (boolean pb_non_letti)
public function boolean uf_elenco_indirizzi ()
end prototypes

public function string uf_decodifica (mailreturncode pmrc_ritorno);string ls_ritorno


choose case pmrc_ritorno
   case MailReturnAccessDenied!
      ls_ritorno = "Accesso negato."
   case MailReturnAttachmentNotFound!
      ls_ritorno = "Allegato non trovato."
   case MailReturnAttachmentOpenFailure!
      ls_ritorno = "Impossibile aprire l'allegato"
   case MailReturnAttachmentWriteFailure!
      ls_ritorno = "Impossibile scrivere l'allegato"
   case MailReturnDiskFull!
      ls_ritorno = "Disco pieno."
   case MailReturnFailure!
      ls_ritorno = "Errore."
   case MailReturnInsufficientMemory!
      ls_ritorno = "Memoria insufficiente"
   case MailReturnInvalidMessage!
      ls_ritorno = "Messaggio invalido."
   case MailReturnLoginFailure!
      ls_ritorno = "Impossibile collegarsi."
   case MailReturnMessageInUse!
      ls_ritorno = "Messaggio già in uso."
   case MailReturnNoMessages!
      ls_ritorno = "Nessun messaggio."
   case MailReturnSuccess!
      ls_ritorno = "Successo."
   case MailReturnTextTooLarge!
      ls_ritorno = "Testo troppo lungo."
   case MailReturnTooManyFiles!
      ls_ritorno = "Troppi files."
   case MailReturnTooManyRecipients!
      ls_ritorno = "Troppi destinatari."
   case MailReturnTooManySessions!
      ls_ritorno = "Troppe sessioni"
   case MailReturnUnknownRecipient!
      ls_ritorno = "Destinatario sconosciuto."
   case MailReturnUserAbort!
      ls_ritorno = "Abbandono da parte dell'utente."
   case else
      ls_ritorno = "Altro."
end choose


return ls_ritorno
end function

public subroutine uf_termina ();setpointer(hourglass!)


if s_cs_xx.num_livello_mail = 0 then
   g_mb.messagebox("Attenzione", "Gestione mail non abilitata.", exclamation!)
   return
end if


imr_ritorno = ims_sessione.maillogoff()
if imr_ritorno <> MailReturnSuccess! then
   g_mb.messagebox("Errore", "Si è verificato un errore durante la disconnessione alla centrale di Mail.~n" + &
              uf_decodifica(imr_ritorno), stopsign!)
end if

destroy ims_sessione
end subroutine

public function boolean uf_inizializza ();setpointer(hourglass!)


if s_cs_xx.num_livello_mail = 0 then
   g_mb.messagebox("Attenzione", "Gestione mail non abilitata.", exclamation!)
   return false
end if


ims_sessione = create mailsession

imr_ritorno = ims_sessione.maillogon(mailnewsession!) // utente e password ???
if imr_ritorno <> MailReturnSuccess! then
   g_mb.messagebox("Errore", "Si è verificato un errore durante la connessione alla centrale di Mail.~n" + &
              uf_decodifica(imr_ritorno), stopsign!)
   uf_termina()
   return false
end if


return true
end function

public function boolean uf_invia (string ps_destinatario[], string ps_oggetto, string ps_nota, string ps_allegato[], boolean pb_lettura);integer li_i, li_num_destinatari


setpointer(hourglass!)


if s_cs_xx.num_livello_mail = 0 then
   g_mb.messagebox("Attenzione", "Gestione mail non abilitata.", exclamation!)
   return false
end if


li_num_destinatari = upperbound(ps_destinatario)

if li_num_destinatari < 1 then
   g_mb.messagebox("Attenzione", "Inserire almeno un destinatario per il messaggio.", exclamation!)
   return false
end if

for li_i = 1 to li_num_destinatari
   imm_messaggio.recipient[li_i].name = ps_destinatario[li_i]
   imr_ritorno = ims_sessione.mailresolverecipient(imm_messaggio.recipient[li_i])
   if imr_ritorno <> MailReturnSuccess! then
      g_mb.messagebox("Attenzione", "Impossibile trovare l'indirizzo per il destinatario: " + &
                 ps_destinatario[li_i] + ".", exclamation!)
      return false
   end if
next

imm_messaggio.subject = ps_oggetto
imm_messaggio.notetext = ps_nota

for li_i = 1 to upperbound(ps_allegato)
   if not isnull(ps_allegato[li_i]) and trim(ps_allegato[li_i]) <> "" then
      if not fileexists(ps_allegato[li_i]) then
         g_mb.messagebox("Attenzione", "Impossibile trovare l'allegato: " + &
                    ps_allegato[li_i] + ".", exclamation!)
         return false
      end if
      imf_allegato.filetype = mailattach!
      imf_allegato.pathname = ps_allegato[li_i]
      imf_allegato.filename = ps_allegato[li_i]
      imf_allegato.position = -1
      imm_messaggio.attachmentfile[li_i] = imf_allegato
   end if
next

imm_messaggio.receiptrequested = pb_lettura


imr_ritorno = ims_sessione.mailsend(imm_messaggio)

//messagebox("Informazione", "Messaggio spedito con il seguente esito:~n" + uf_decodifica(imr_ritorno), information!)


return true
end function

public function boolean uf_leggi (boolean pb_non_letti);integer li_i, li_j
string ls_stringa


setpointer(hourglass!)


if s_cs_xx.num_livello_mail = 0 then
   g_mb.messagebox("Attenzione", "Gestione mail non abilitata.", exclamation!)
   return false
end if


imr_ritorno = ims_sessione.mailgetmessages(pb_non_letti)
if imr_ritorno <> MailReturnSuccess! then
   g_mb.messagebox("Attenzione", "Non è stato possibile leggere l'elenco dei messaggi a causa di:~n" + uf_decodifica(imr_ritorno), exclamation!)
   return false
end if


s_cs_xx.s_mail.num_messaggi = 0

for li_i = 1 to upperbound(ims_sessione.messageid)
   imr_ritorno = ims_sessione.mailreadmessage(ims_sessione.messageid[li_i], imm_messaggio, &
                                              mailentiremessage!, false)
   if imr_ritorno = MailReturnSuccess! then
      s_cs_xx.s_mail.num_messaggi ++
      s_cs_xx.s_mail.id_messaggio[li_i] = ims_sessione.messageid[li_i]
      ls_stringa = ""
      for li_j = 1 to (upperbound(imm_messaggio.recipient) - 1)
         ls_stringa = ls_stringa + imm_messaggio.recipient[li_j].name + ";"
      next
      s_cs_xx.s_mail.destinatari[li_i] = mid(ls_stringa, 1, len(ls_stringa) - 1)
      s_cs_xx.s_mail.oggetto[li_i] = imm_messaggio.subject
      s_cs_xx.s_mail.data[li_i] = imm_messaggio.datereceived
      s_cs_xx.s_mail.nota[li_i] = imm_messaggio.notetext
      ls_stringa = ""
      for li_j = 1 to upperbound(imm_messaggio.attachmentfile)
         ls_stringa = ls_stringa + imm_messaggio.attachmentfile[li_j].pathname + ";"
      next
      s_cs_xx.s_mail.allegati[li_i] = mid(ls_stringa, 1, len(ls_stringa) - 1)
   end if
next


return true
end function

public function boolean uf_elenco_indirizzi ();setpointer(hourglass!)

if s_cs_xx.num_livello_mail = 0 then
   g_mb.messagebox("Attenzione", "Gestione mail non abilitata.", exclamation!)
   return false
end if


imr_ritorno=ims_sessione.mailAddress(imm_messaggio)


return true
end function

on uo_mail.create
end on

on uo_mail.destroy
end on

