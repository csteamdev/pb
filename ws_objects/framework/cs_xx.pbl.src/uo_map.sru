﻿$PBExportHeader$uo_map.sru
forward
global type uo_map from nonvisualobject
end type
end forward

global type uo_map from nonvisualobject
end type
global uo_map uo_map

type variables
private:
	string is_key[]
	any is_value[]
end variables

forward prototypes
public function uo_map add (string as_key, any as_value)
public function integer size ()
public function uo_map remove (string as_key)
public function boolean has_key (string as_key)
public function boolean has_key (string as_key, ref integer ai_position)
public function any get (string as_key)
public function string get_as_string (string as_key)
public function string get_key (integer ai_index)
public function any get (integer ai_index)
public function uo_map put (string as_key, any as_value)
public function uo_map remove_all ()
public function uo_map clear ()
end prototypes

public function uo_map add (string as_key, any as_value);int li_index

li_index = upperbound(is_key) + 1

is_key[li_index] = as_key
is_value[li_index] = as_value

return this
end function

public function integer size ();return upperbound(is_key)
end function

public function uo_map remove (string as_key);/** 
 * stefanop
 * 23/07/2014
 *
 * Rimuove un elemento
 **/

string ls_new_key[]
any	la_new_value[]
int li_position, li_current_size, li_new_size, li_i, li_k


if has_key(as_key, li_position) then
	
	li_current_size = size()
	li_k = 1

	for li_i = 1 to li_current_size
		
		if li_i = li_position then
			// salto la posizione per spostare indietro gli elementi
			li_i++
		end if
		
		
		ls_new_key[li_k] = is_key[li_i]
		la_new_value[li_k] = is_value[li_i]
		li_k++
		
	next
		
	is_key = ls_new_key
	is_value = la_new_value
	
end if
 
return this
end function

public function boolean has_key (string as_key);/**
 * stefanop
 * 23/07/2014
 *
 * Controlla se esiste una key
 **/
 
int li_position

return has_key(as_key, li_position)
end function

public function boolean has_key (string as_key, ref integer ai_position);/**
 * stefanop
 * 23/07/2014
 *
 * Controlla se esiste una key
 **/
 
int li_count

li_count = size()

for ai_position = 1 to li_count
	
	if is_key[ai_position] = as_key then
		return true
	end if
	
next

ai_position = -1
return false
end function

public function any get (string as_key);/**
 * stefanop
 * 23/07/2014
 *
 * Ottiene il valore della chiave
 **/
 
any la_null
int li_position

if has_key(as_key, li_position) then
	return is_value[li_position]
else
	setnull(la_null)
	return la_null
end if
end function

public function string get_as_string (string as_key);/**
 * stefanop
 * 23/07/2014
 *
 * Ottiene il valore della chiave in formato string
 **/
 
any la_null
int li_position

if has_key(as_key, li_position) then
	return string(is_value[li_position])
else
	setnull(la_null)
	return la_null
end if
end function

public function string get_key (integer ai_index);/**
 * stefanop
 * 23/07/2014
 *
 * Ottiene il valore della chiave
 **/
 
string ls_null

if ai_index <= size() then
	return is_key[ai_index]
else
	setnull(ls_null)
	return ls_null
end if
end function

public function any get (integer ai_index);/**
 * stefanop
 * 23/07/2014
 *
 * Ottiene il valore della chiave
 **/
 
any ls_null

if ai_index <= size() then
	return is_value[ai_index]
else
	setnull(ls_null)
	return ls_null
end if
end function

public function uo_map put (string as_key, any as_value);int li_index

if not has_key(as_key, li_index) then
	li_index = upperbound(is_key) + 1
end if

is_key[li_index] = as_key
is_value[li_index] = as_value

return this
end function

public function uo_map remove_all ();string ls_empty[]
any la_empty[]

is_key = ls_empty
is_value = la_empty

return this
end function

public function uo_map clear ();string ls_empty[]
any la_empty[]

is_key = ls_empty
is_value = la_empty

return this
end function

on uo_map.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_map.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

