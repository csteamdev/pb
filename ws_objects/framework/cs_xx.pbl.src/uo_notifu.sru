﻿$PBExportHeader$uo_notifu.sru
forward
global type uo_notifu from nonvisualobject
end type
end forward

global type uo_notifu from nonvisualobject
end type
global uo_notifu uo_notifu

type variables

private:
	string is_nofitu_path
	string is_error
end variables

forward prototypes
public function boolean uof_set_path (string as_path)
public function string uof_get_error ()
private function integer uof_execute (string as_command)
public function boolean uof_show (string as_title, string as_message)
public function boolean uof_warning (string as_title, string as_message)
public function boolean uof_info (string as_title, string as_message)
public function boolean uof_error (string as_title, string as_message)
public function boolean uof_show (string as_title, string as_message, string as_type, long al_milliseconds)
public function boolean uof_show (string as_title, string as_message, string as_type)
end prototypes

public function boolean uof_set_path (string as_path);/**
 * stefanop
 * 24/04/2012
 *
 * Imposto il percorso del file .exe
 **/
 
if right(as_path, 1) <> "\" then as_path += "\"
	
as_path += "notifu.exe"
	
if fileexists(as_path) then
	this.is_nofitu_path = as_path
	return true
else
	setnull(this.is_nofitu_path)
	is_error = "File non trovato nel percorso: " + as_path
	return false
end if
end function

public function string uof_get_error ();return is_error
end function

private function integer uof_execute (string as_command);long ll_return

uo_shellexecute luo_shell
luo_shell = create uo_shellexecute

setnull(is_error)

luo_shell.uof_run_async(is_nofitu_path + as_command)

destroy luo_shell

return 1
end function

public function boolean uof_show (string as_title, string as_message);/**
 * stefanop
 * 24/04/20120
 *
 * Visualizzo notifica
 **/
 
uof_show(as_title, as_message, "")
 
return true
end function

public function boolean uof_warning (string as_title, string as_message);/**
 * stefanop
 * 24/04/20120
 *
 * Visualizzo notifica
 **/
 
uof_show(as_title, as_message, "warn")
 
return true
end function

public function boolean uof_info (string as_title, string as_message);/**
 * stefanop
 * 24/04/20120
 *
 * Visualizzo notifica
 **/
 
uof_show(as_title, as_message, "info")
 
return true
end function

public function boolean uof_error (string as_title, string as_message);/**
 * stefanop
 * 24/04/20120
 *
 * Visualizzo notifica
 **/
 
uof_show(as_title, as_message, "error")
 
return true
end function

public function boolean uof_show (string as_title, string as_message, string as_type, long al_milliseconds);/**
 * stefanop
 * 24/04/20120
 *
 * Visualizzo notifica
 **/
 
string ls_command

ls_command = ' /p "' + as_title + '" /m "' + as_message + '"'

// altri parametri
if not isnull(as_type) and as_type <> "" then ls_command += " /t " + as_type
if al_milliseconds > 0 then ls_command += " /d " + string(al_milliseconds)

uof_execute(ls_command)
 
return true
end function

public function boolean uof_show (string as_title, string as_message, string as_type);/**
 * stefanop
 * 24/04/20120
 *
 * Visualizzo notifica
 **/
 
uof_show(as_title, as_message, as_type, 0)
 
return true
end function

on uo_notifu.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_notifu.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;
// imposto il percorso di default
this.uof_set_path(s_cs_xx.volume + s_cs_xx.risorse + "notifu\")
end event

