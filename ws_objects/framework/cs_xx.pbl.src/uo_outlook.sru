﻿$PBExportHeader$uo_outlook.sru
forward
global type uo_outlook from nonvisualobject
end type
end forward

global type uo_outlook from nonvisualobject
end type
global uo_outlook uo_outlook

type variables
constant string LOG_NAME = "INOLTRMAIL"

oleobject iole_outlook, iole_item, iole_attach, iole_redemption, iole_redemption_attach

public:
	string is_tipo_invio
	
	/**
	 * Stefanop
	 * 16/05/2014
	 *
	 * Se a true NON mostra la finestra di modifica
	 **/
	boolean ib_silent_mode = false

	/**
	* forza apertura
	**/
	boolean ib_force_open = false
	
	boolean ib_html = true

end variables

forward prototypes
public function integer uof_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_destinatari[], string as_allegati[], boolean ab_conferma, ref string as_messaggio)
public function integer uof_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_cod_tipo_lista_dist, string as_cod_lista_dist, string as_allegati[], boolean ab_conferma, ref string as_messaggio)
public function integer uof_controllo_tipo_invio (ref string as_messaggio)
public function integer uof_invio_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_destinatari[], string as_allegati[], boolean ab_conferma, ref string as_messaggio)
public function integer uof_invio_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_cod_tipo_lista_dist, string as_cod_lista_dist, string as_allegati[], boolean ab_conferma, ref string as_messaggio)
public function integer uof_controllo (ref string as_messaggio, boolean ab_conferma)
private function string uof_join (string as_array[], string as_sep)
public function boolean uof_invio_sendmail (string as_from, string as_to[], string as_cc[], string as_bcc[], string as_subject, string as_message, string as_attachment[], string as_smtp_server, string as_smtp_usd, string as_smtp_pwd, ref string as_error)
public function boolean uof_invio_sendmail (string as_from, string as_to[], string as_subject, string as_message, string as_attachment[], string as_smtp_server, string as_smtp_usd, string as_smtp_pwd, ref string as_error)
public function boolean uof_invio_sendmail (string as_to[], string as_subject, string as_message, string as_attachment[], ref string as_error)
public function boolean uof_invio_sendmail (string as_to[], string as_cc[], string as_bcc[], string as_subject, string as_message, string as_attachment[], string as_error)
public function integer uof_invio_sendmail (string as_tipo_invio, string as_oggetto, string as_testo, string as_cod_tipo_lista_dist, string as_cod_lista_dist, string as_allegati[], ref string as_messaggio)
public function boolean uof_invio_sendmail (string as_to[], string as_subject, string as_message, string as_attachment[])
private function boolean uof_open_window (ref s_invio_email astr_email)
private function boolean uof_anteprima_abilitata ()
public function boolean uof_invio_sendmail (string as_to[], string as_cc[], string as_subject, string as_message, string as_attachment[], ref string as_error)
public function boolean uof_inoltro_cs (ref string as_destinatari[])
public function integer uof_apri_outlook (string as_to[], string as_subject, string as_message, string as_allegati[], ref string as_error)
private function string uof_get_outlook_version ()
public function boolean uof_is_outlook_installed ()
end prototypes

public function integer uof_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_destinatari[], string as_allegati[], boolean ab_conferma, ref string as_messaggio);long    ll_return, ll_i, ll_count

string  ls_destinatari, ls_indirizzo

boolean lb_trovato

// Chiamata alla funzione di controllo
ll_return = uof_controllo(as_messaggio,ab_conferma)

if ll_return = -1 then
	// Si è verificato un errore nel controllo del tipo invio posta	
	return -1
elseif ll_return = 100 then
	// Il parametro MAI è impostato in modo da disabilitare l'invio di messaggi
	as_messaggio = "Parametro MAI disattivato nel registro di sistema"
	return 100
elseif ll_return = 50 then
	// L'utente, dopo la richiesta di conferma, ha deciso di interrompere l'invio del messaggio
	as_messaggio = "Operazione interrotta dall'utente"
	return 100
end if

choose case is_tipo_invio
	case 'O'
		ll_return = uof_invio_outlook(al_elemento, as_tipo_invio, as_oggetto, as_testo, as_destinatari[], as_allegati[], ab_conferma, as_messaggio)

	case 'S'
	case 'W'
	case 'R'
		as_messaggio = "Metodo di invio email NON è supportato."
		ll_return = -1
		
	case 'E'
		if uof_invio_sendmail(as_destinatari[], as_oggetto, as_testo, as_allegati[], as_messaggio) then
			ll_return = 0
		else
			ll_return = -1
		end if

end choose

if ll_return  = 0 then

	as_messaggio = "Messaggio inviato con successo"
	return 0

else
	
	return -1
end if
end function

public function integer uof_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_cod_tipo_lista_dist, string as_cod_lista_dist, string as_allegati[], boolean ab_conferma, ref string as_messaggio);long    ll_return, ll_i, ll_count

string  ls_destinatari, ls_indirizzo

boolean lb_trovato


// Chiamata alla funzione di controllo

ll_return = uof_controllo(as_messaggio,ab_conferma)

if ll_return = -1 then
	// Si è verificato un errore nel controllo del tipo invio posta	
	return -1
elseif ll_return = 100 then
	// Il parametro MAI è impostato in modo da disabilitare l'invio di messaggi
	as_messaggio = "Parametro MAI disattivato nel registro di sistema"
	return 100
elseif ll_return = 50 then
	// L'utente, dopo la richiesta di conferma, ha deciso di interrompere l'invio del messaggio
	as_messaggio = "Operazione interrotta dall'utente"
	return 100
end if


choose case is_tipo_invio
	case 'O'
		ll_return = uof_invio_outlook(al_elemento, as_tipo_invio, as_oggetto, as_testo, as_cod_tipo_lista_dist, as_cod_lista_dist, as_allegati[], ab_conferma, as_messaggio)

	case 'S'
	case 'W'
	case 'R'
		as_messaggio = "Metodo di invio email NON è supportato."
		ll_return = -1
		
	case 'E'
		ll_return = uof_invio_sendmail(as_tipo_invio, as_oggetto, as_testo, as_cod_tipo_lista_dist, as_cod_lista_dist, as_allegati[], as_messaggio)

end choose

if ll_return  = 0 then
	as_messaggio = "Messaggio inviato con successo"
	return 0
else
	return -1
end if
end function

public function integer uof_controllo_tipo_invio (ref string as_messaggio);string ls_flag_invia_mail, ls_tipo_invio

long   ll_return


// controllo se l'utente è abilitato a inviare la mail

select flag_invia_mail
into   :ls_flag_invia_mail
from   utenti 
where  cod_utente = :s_cs_xx.cod_utente;
		 
if sqlca.sqlcode < 0 then
	as_messaggio = "Selezione parametro invio posta: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Utente non valido per invio Mail. Cod. utente: " + s_cs_xx.cod_utente
	return -1
elseif sqlca.sqlcode = 0 then
	if ls_flag_invia_mail <> 'S' then
		as_messaggio = "Attenzione: l'utente non è abilitato all'invio di E-Mail!"
		return -1
	end if
end if

// se non esiste il parametro nel registro, controllo nei parametri aziendali.
// il parametro si chiama in entrambi i casi 'TIM' (tipo invio mail)

ll_return = registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "tim", ls_tipo_invio)

if ll_return = -1 then
	
	ls_tipo_invio = "" 
	
	select stringa
	into   :ls_tipo_invio
	from   parametri_azienda
	where  cod_azienda=:s_cs_xx.cod_azienda and
				cod_parametro = 'TIM';
			 
	if sqlca.sqlcode < 0 then		
		as_messaggio = "Errore durante la ricerca del parametro invio E-Mail: " + sqlca.sqlerrtext
		return -1		
	elseif sqlca.sqlcode = 100 then		
		as_messaggio = "Attenzione (100): impossibile identificare il tipo di invio E-Mail!"
		return -1		
	end if
	
end if

choose case ls_tipo_invio
	case 'O' // Outlook
		is_tipo_invio = 'O'

	case 'S' // SMTP
	case 'W' // winsock
		as_messaggio = "Attenzione: Metodo di invio email NON supportato!"
		return -1

	case 'E' // sendmail
		is_tipo_invio = 'E'	
	
	case else
		as_messaggio = "Attenzione: impossibile identificare il tipo di invio E-Mail!"
		return -1
end choose

return 0

end function

public function integer uof_invio_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_destinatari[], string as_allegati[], boolean ab_conferma, ref string as_messaggio);long    ll_return, ll_i
string  ls_destinatari, ls_cs_destinatari[]
boolean lb_trovato
uo_sendmail luo_sendmail
uo_log_sistema luo_log_sistema

// La funzione gestisce la creazione di 4 tipi di elementi di OUTLOOK:
//		0 = messaggio email
//		1 = appuntamento
//		2 = contatto
//		3 = attività

if isnull(al_elemento) or al_elemento < 0 or al_elemento > 3 then
	as_messaggio = "Tipo elemento deve essere uno dei seguenti valori: 0 (mail), 1 (appuntamento), 2 (contatto), 3 (attività)"
	return -1
end if

// Controlli da eseguire nel caso in cui l'elemento da creare è un messaggio email
if al_elemento = 0 then
	
	// La funzione può inviare il messaggio in due diversi modi:
	//		(M)anuale = il messaggio viene creato, compilato e visualizzato; l'utente deve cliccare INVIA in OUTLOOK
	//		(A)automatico = il messaggio viene creato e spedito senza essere visualizzato e senza interazione dell'utente
	if isnull(as_tipo_invio) or (as_tipo_invio <> "M" and as_tipo_invio <> "A") then
		as_messaggio = "Tipo invio deve essere uno dei seguenti valori: M (manuale), A (automatico)"
		return -1
	end if
	
	// Se è stato selezionato l'invio automatico deve essere presente almeno un destinatari
	if as_tipo_invio = "A" then
		
		// Si controlla se nella chiamata alla funzione è stato specificato almeno un destinatario
		lb_trovato = false
		
		for ll_i = 1 to upperbound(as_destinatari)
			if isnull(as_destinatari[ll_i]) then
				as_destinatari[ll_i] = ""
			elseif as_destinatari[ll_i] <> "" then
				lb_trovato = true
			end if
		next
		
		if upperbound(as_destinatari) < 1  or lb_trovato = false then
			as_messaggio = "Per l'invio automatico è necessario specificare almeno un destinatario"
			return -1
		end if
				
	end if
	
	// Si eliminano eventuali valori NULL dall'intestazione e dal corpo del messaggio
	
	if isnull(as_oggetto) then
		as_oggetto = ""
	end if
	
	if isnull(as_testo) then
		as_testo = ""
	end if
	
end if

// L'oggetto iole_outlook viene creato e collegato all'applicazione OUTLOOK

iole_outlook = create oleobject

ll_return = iole_outlook.connecttonewobject("outlook.application")

if ll_return <> 0 THEN
	as_messaggio = "Impossibile connettersi ad Outlook. Codice errore: " + string(ll_return)
	destroy iole_outlook
	return -1
end if

// Viene creato un nuovo elemento del tipo specificato

iole_item = iole_outlook.createitem(al_elemento)

// Se l'elemento è un messaggio email questo viene compilato e inviato, altrimenti viene solo visualizzato
if al_elemento = 0 then
	
	// Impostazione dell'oggetto del messaggio
	iole_item.subject = as_oggetto
	
	//stefanop 05/05/11: forzo l'uso HTML
	try 
		iole_item.HTMLBody  = as_testo
	catch(RuntimeError ex)
		// Impostazione del testo del messaggio
		iole_item.body = as_testo
	end try
	
	// Impostazione dei destinatari del messaggio
	
	ls_destinatari = ""
	
	for ll_i = 1 to upperbound(as_destinatari)
		
		if not isnull(as_destinatari[ll_i]) and as_destinatari[ll_i] <> "" then
			
			// I destinatari multipli vanno separati da punto e virgola
			if ll_i > 1 then
				ls_destinatari = ls_destinatari + ";"
			end if
			
			ls_destinatari = ls_destinatari + as_destinatari[ll_i]
			
		end if
		
	next
	
	iole_item.to = ls_destinatari
	
	// Gestione degli allegati
	
	iole_attach = iole_item.attachments
	
	for ll_i = 1 to upperbound(as_allegati)
		
		// Il file specificato viene allegato solo se esiste al percorso indicato
		
		if fileexists(as_allegati[ll_i]) then
			iole_attach.add(as_allegati[ll_i])
		end if
		
	next
	
	destroy iole_attach
	
	// Gestione del tipo invio (Automatico / Manuale)

	choose case as_tipo_invio
			
		case "M"
			
			iole_item.display
			
		case "A"
			
			iole_item.send
			
	end choose
	
else
	
	// L'elemento non è un messaggio email e quindi viene solo visualizzato
	
	iole_item.display
	
end if

// Gli oggetti istanziati vengono distrutti dopo essere stati scollegati dall'applicazione OUTLOOK

destroy iole_item

iole_outlook.disconnectobject()
destroy iole_outlook

// Inoltro mai la CSTEAM
try
	if uof_inoltro_cs(ls_cs_destinatari) then
		luo_sendmail = create uo_sendmail
		luo_sendmail.uof_set_from("assistenza@csteam.com")
		luo_sendmail.uof_set_to(ls_cs_destinatari)
		luo_sendmail.uof_set_subject(as_oggetto)
		luo_sendmail.uof_set_message(guo_functions.uof_get_system_info())
		luo_sendmail.uof_add_attachments(as_allegati)		
		luo_sendmail.uof_set_html( false )
		luo_sendmail.uof_send()
	end if
catch(RuntimeError e) 
	// non deve dare nessun errore! MAI!
	luo_log_sistema = create uo_log_sistema
	luo_log_sistema.uof_write_log_sistema_not_sqlca(LOG_NAME, "Errore durante l'inoltro della mail. " + ex.getMessage())
	destroy luo_log_sistema
end try
// -------------

as_messaggio = "Messaggio inviato con successo"

return 0
end function

public function integer uof_invio_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_cod_tipo_lista_dist, string as_cod_lista_dist, string as_allegati[], boolean ab_conferma, ref string as_messaggio);string  ls_destinatari, ls_indirizzo, ls_cs_destinatari[]
long    ll_return, ll_i, ll_count
boolean lb_trovato
uo_sendmail luo_sendmail
uo_log_sistema luo_log_sistema

if isnull(al_elemento) or al_elemento < 0 or al_elemento > 3 then
	as_messaggio = "Tipo elemento deve essere uno dei seguenti valori: 0 (mail), 1 (appuntamento), 2 (contatto), 3 (attività)"
	return -1
end if

// Controlli da eseguire nel caso in cui l'elemento da creare è un messaggio email

if al_elemento = 0 then
	
	// La funzione può inviare il messaggio in due diversi modi:
	//		(M)anuale = il messaggio viene creato, compilato e visualizzato; l'utente deve cliccare INVIA in OUTLOOK
	//		(A)automatico = il messaggio viene creato e spedito senza essere visualizzato e senza interazione dell'utente
	
	if isnull(as_tipo_invio) or (as_tipo_invio <> "M" and as_tipo_invio <> "A") then
		as_messaggio = "Tipo invio deve essere uno dei seguenti valori: M (manuale), A (automatico)"
		return -1
	end if
	
	// Se è stato selezionato l'invio automatico deve essere specificata una lista di distribuzione valida
	
	if as_tipo_invio = "A" then
		
		// Si controlla se nella chiamata alla funzione è stata specificata una lista di distribuzione
	
		if isnull(as_cod_tipo_lista_dist) or isnull(as_cod_lista_dist) then
			
			as_messaggio = "Per l'invio automatico è necessario specificare una lista di distribuzione"
			return -1
			
		else
			
			// Si controlla se la lista di distribuzione specificata è valida e se contiene destinatari
			
			select count(*)
			into   :ll_count
			from	 tab_ind_dest
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       email = 'S' and
			       cod_destinatario in ( select cod_destinatario
												  from   det_liste_dist
												  where  cod_azienda = :s_cs_xx.cod_azienda and
			                                    cod_tipo_lista_dist = :as_cod_tipo_lista_dist and
														 	cod_lista_dist = :as_cod_lista_dist);
			
			if sqlca.sqlcode < 0 then
				as_messaggio = "Errore in lettura destinatari lista di distribuzione da tabella utenti: " + sqlca.sqlerrtext
				return -1
			elseif sqlca.sqlcode = 100 or isnull(ll_count) or ll_count = 0 then
				as_messaggio = "Per l'invio automatico è necessario specificare una lista di distribuzione valida"
				return -1
			end if
			
		end if
				
	end if
	
	// Si eliminano eventuali valori NULL dall'intestazione e dal corpo del messaggio
	
	if isnull(as_oggetto) then
		as_oggetto = ""
	end if
	
	if isnull(as_testo) then
		as_testo = ""
	end if
	
end if

// L'oggetto iole_outlook viene creato e collegato all'applicazione OUTLOOK

iole_outlook = create oleobject

ll_return = iole_outlook.connecttonewobject("outlook.application")

if ll_return <> 0 THEN
	as_messaggio = "Impossibile connettersi ad Outlook. Codice errore: " + string(ll_return)
	destroy iole_outlook
	return -1
end if

// Viene creato un nuovo elemento del tipo specificato

iole_item = iole_outlook.createitem(al_elemento)

// Se l'elemento è un messaggio email questo viene compilato e inviato, altrimenti viene solo visualizzato

if al_elemento = 0 then
	
	// Impostazione dell'oggetto del messaggio

	iole_item.subject = as_oggetto
	
	// Impostazione del testo del messaggio
	
	iole_item.body = as_testo
	
	// Impostazione dei destinatari del messaggio
	
	ls_destinatari = ""
	
	declare destinatari cursor for
	select indirizzo
	from	 tab_ind_dest
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 email = 'S' and
			 cod_destinatario in ( select cod_destinatario
										  from   det_liste_dist
										  where  cod_azienda = :s_cs_xx.cod_azienda and
													cod_tipo_lista_dist = :as_cod_tipo_lista_dist and
													cod_lista_dist = :as_cod_lista_dist);
													
	open destinatari;
	
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore in costruzione elenco destinatari~nErrore nella open del cursore destinatari: " + sqlca.sqlerrtext
		return -1
	end if
	
	do while true
		
		fetch destinatari
		into  :ls_indirizzo;
		
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore in costruzione elenco destinatari~nErrore nella fetch del cursore destinatari: " + sqlca.sqlerrtext
			close destinatari;
			return -1
		elseif sqlca.sqlcode = 100 then
			close destinatari;
			exit
		end if
		
		if ls_destinatari <> "" then
			ls_destinatari = ls_destinatari + ";"
		end if
		
		ls_destinatari = ls_destinatari + ls_indirizzo
		
	loop
	
	iole_item.to = ls_destinatari
	
	// Gestione degli allegati
	
	iole_attach = iole_item.attachments
	
	for ll_i = 1 to upperbound(as_allegati)
		
		// Il file specificato viene allegato solo se esiste al percorso indicato
		
		if fileexists(as_allegati[ll_i]) then
			iole_attach.add(as_allegati[ll_i])
		end if
		
	next
	
	destroy iole_attach
	
	// Gestione del tipo invio (Automatico / Manuale)

	choose case as_tipo_invio
			
		case "M"
			iole_item.display
			
		case "A"
			iole_item.send
			
	end choose
	
else
	
	// L'elemento non è un messaggio email e quindi viene solo visualizzato
	
	iole_item.display
	
end if

// Gli oggetti istanziati vengono distrutti dopo essere stati scollegati dall'applicazione OUTLOOK

destroy iole_item

iole_outlook.disconnectobject()

destroy iole_outlook

// Inoltro mai la CSTEAM
try
	if uof_inoltro_cs(ls_cs_destinatari) then
		luo_sendmail = create uo_sendmail
		luo_sendmail.uof_set_from("assistenza@csteam.com")
		luo_sendmail.uof_set_to(ls_cs_destinatari)
		luo_sendmail.uof_set_subject(as_oggetto)
		luo_sendmail.uof_set_message(guo_functions.uof_get_system_info())
		luo_sendmail.uof_add_attachments(as_allegati)		
		luo_sendmail.uof_set_html( false )
		luo_sendmail.uof_send()
	end if
catch(RuntimeError ex) 
	// non deve dare nessun errore! MAI!
	luo_log_sistema = create uo_log_sistema
	luo_log_sistema.uof_write_log_sistema_not_sqlca(LOG_NAME, "Errore durante l'inoltro della mail. " + ex.getMessage())
	destroy luo_log_sistema
end try
// -------------

as_messaggio = "Messaggio inviato con successo"

return 0
end function

public function integer uof_controllo (ref string as_messaggio, boolean ab_conferma);string ls_valore

long   ll_return


// controllo se l'utente può inviare mail. se si, controllo il tipo di invio

ll_return = uof_controllo_tipo_invio(as_messaggio)

if ll_return = -1 then
	return -1
end if

choose case is_tipo_invio 
	case 'O'
		ll_return = registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "mai", ls_valore)
		
		if ll_return = -1 then
			as_messaggio = "Errore durante la ricerca del parametro mai!"
			return -1
		end if
		
		if long(ls_valore) <= 0 then
			
			// parametro MAI disabilitato
			
			return 100
			
		end if
		
end choose

// se arrivo qui, l'eventuale parametro MAI è abilitato
			
if ab_conferma then
				
	// è stato specificato di richiedere la conferma dell'utente				
	if as_messaggio = "" or isnull(as_messaggio) then
		// nessun testo specificato per il messaggio di conferma, viene usato un testo di default
		as_messaggio = "Procedere con l'invio del messaggio?"
	end if
	
//	if g_mb.messagebox("Invio Messaggio",as_messaggio,question!,yesno!,2) = 2 then
	if messagebox("Invio Messaggio",as_messaggio,question!,yesno!,2) = 2 then
		// l'utente sceglie di non continuare
		return 50
	else
		// l'utente sceglie di continuare
		return 0	
	end if
	
end if

return 0
				
end function

private function string uof_join (string as_array[], string as_sep);/**
 * stefanop
 * 27/09/2010
 *
 * Dato un array di stringhe ed un separatore la funzione restituisce una stringa unica concatenata con il separatore
 **/
 
string ls_join
int li_count, li_i

li_count = upperbound(as_array)
if li_count < 1 then
	return ""
elseif li_count = 1 then
	return as_array[1]
else 
	ls_join = ""
	for li_i = 1 to li_count - 1
		ls_join += as_array[li_i] + as_sep
	next
	
	ls_join += as_array[li_count]
	
	return ls_join
end if
end function

public function boolean uof_invio_sendmail (string as_from, string as_to[], string as_cc[], string as_bcc[], string as_subject, string as_message, string as_attachment[], string as_smtp_server, string as_smtp_usd, string as_smtp_pwd, ref string as_error);/**
 * stefanop
 * 27/09/2010
 *
 * Consente di inviare una mail con il programma esterno sendmail.
 * Il programma deve essere inserito all'interno della cartella risorse/sendmail/sendmail.exe
 **/

string ls_cs_destinatari[]
boolean lb_result
s_invio_email lstr_email
uo_log_sistema luo_log_sistema
uo_sendmail luo_sendmail

luo_sendmail = create uo_sendmail

// stefanop 24/05/2014
if uof_anteprima_abilitata() then
	lstr_email.as_a = as_to
	lstr_email.as_cc = as_cc
	lstr_email.as_subject = as_subject
	lstr_email.as_message = as_message
	lstr_email.as_allegati = as_attachment
	
	if uof_open_window(lstr_email) then
		as_error = "Operazione annullata dall'utente"
		return false
	elseif (lstr_email.ab_inviato_ad_outlook) then
		// L'utente ha scelto di aprire outlook per inviare
		// la mail, quindi termino il processo.
		setnull(as_error)
		return true
	else
		as_to = lstr_email.as_a
		as_cc = lstr_email.as_cc
		as_subject = lstr_email.as_subject
		as_message = lstr_email.as_message
		as_attachment = lstr_email.as_allegati
	end if
end if

luo_sendmail.uof_set_from(as_from)
luo_sendmail.uof_set_to(as_to)
luo_sendmail.uof_set_cc(as_cc)
luo_sendmail.uof_set_bcc(as_bcc)
luo_sendmail.uof_set_attachments(as_attachment)
luo_sendmail.uof_set_subject(as_subject)
luo_sendmail.uof_set_message(as_message)
luo_sendmail.uof_set_smtp(as_smtp_server, as_smtp_usd, as_smtp_pwd)
luo_sendmail.uof_set_log(s_cs_xx.volume + s_cs_xx.risorse + "logs\sendmail.log")
luo_sendmail.uof_set_html( ib_html )

lb_result = luo_sendmail.uof_send()
as_error = luo_sendmail.uof_get_error()

// Inoltro mai la CSTEAM
try
	if lb_result and uof_inoltro_cs(ls_cs_destinatari) then
		luo_sendmail.uof_clear()
		luo_sendmail.uof_set_to(ls_cs_destinatari)
		luo_sendmail.uof_set_message(guo_functions.uof_get_system_info())
		luo_sendmail.uof_set_html( false )
		luo_sendmail.uof_send()
	end if
catch(RuntimeError ex) 
	// non deve dare nessun errore! MAI!
	luo_log_sistema = create uo_log_sistema
	luo_log_sistema.uof_write_log_sistema_not_sqlca(LOG_NAME, "Errore durante l'inoltro della mail. " + ex.getMessage())
	destroy luo_log_sistema
end try
// -------------

return lb_result
end function

public function boolean uof_invio_sendmail (string as_from, string as_to[], string as_subject, string as_message, string as_attachment[], string as_smtp_server, string as_smtp_usd, string as_smtp_pwd, ref string as_error);/**
 * stefanop
 * 27/09/2010
 *
 * Consente di inviare una mail con il programma esterno sendmail.
 * Il programma deve essere inserito all'interno della cartella risorse/sendmail/sendmail.exe
 **/

string ls_empty[]
 
setnull(ls_empty)
 
return uof_invio_sendmail(as_from, as_to, ls_empty, ls_empty, as_subject, as_message, as_attachment, as_smtp_server, as_smtp_usd, as_smtp_pwd, as_error)
end function

public function boolean uof_invio_sendmail (string as_to[], string as_subject, string as_message, string as_attachment[], ref string as_error);/**
 * stefanop
 * 27/09/2010
 *
 * Consente di inviare una mail con il programma esterno sendmail.
 * Il programma deve essere inserito all'interno della cartella risorse/sendmail/sendmail.exe
 **/
 
string ls_smtp_server, ls_smtp_usd, ls_smtp_pwd, ls_from

select 
	e_mail,
	smtp_server,
	smtp_usr,
	smtp_pwd
into
	:ls_from,
	:ls_smtp_server,
	:ls_smtp_usd,
	:ls_smtp_pwd
from utenti
where
	cod_utente  = :s_cs_xx.cod_utente;
	
if sqlca.sqlcode <> 0 then
	as_error = "Impossibile recuperare le informazioni di invio per l'utente collegato"
	return false
else
	return uof_invio_sendmail(ls_from, as_to, as_subject, as_message, as_attachment, ls_smtp_server, ls_smtp_usd, ls_smtp_pwd, as_error)
end if
end function

public function boolean uof_invio_sendmail (string as_to[], string as_cc[], string as_bcc[], string as_subject, string as_message, string as_attachment[], string as_error);/**
 * stefanop
 * 27/09/2010
 *
 * Consente di inviare una mail con il programma esterno sendmail.
 * Il programma deve essere inserito all'interno della cartella risorse/sendmail/sendmail.exe
 **/
 
string ls_smtp_server, ls_smtp_usd, ls_smtp_pwd, ls_from

select 
	e_mail,
	smtp_server,
	smtp_usr,
	smtp_pwd
into
	:ls_from,
	:ls_smtp_server,
	:ls_smtp_usd,
	:ls_smtp_pwd
from utenti
where cod_utente  = :s_cs_xx.cod_utente;
	
if sqlca.sqlcode <> 0 then
	as_error = "Impossibile recuperare le informazioni di invio per l'utente collegato"
	return false
else
	return uof_invio_sendmail(ls_from, as_to, as_cc, as_bcc, as_subject, as_message, as_attachment, ls_smtp_server, ls_smtp_usd, ls_smtp_pwd,as_error)
end if
end function

public function integer uof_invio_sendmail (string as_tipo_invio, string as_oggetto, string as_testo, string as_cod_tipo_lista_dist, string as_cod_lista_dist, string as_allegati[], ref string as_messaggio);string ls_destinatari, ls_email, ls_sender_name, ls_server, ls_usr, ls_pwd, ls_indirizzo
long ll_count
uo_sendmail luo_sendmail

// La funzione può inviare il messaggio in due diversi modi:
//		(M)anuale = il messaggio viene creato, compilato e visualizzato; l'utente deve cliccare INVIA in OUTLOOK
//		(A)automatico = il messaggio viene creato e spedito senza essere visualizzato e senza interazione dell'utente
if isnull(as_tipo_invio) or (as_tipo_invio <> "M" and as_tipo_invio <> "A") then
	as_messaggio = "Tipo invio deve essere uno dei seguenti valori: M (manuale), A (automatico)"
	return -1
end if
	
// Se è stato selezionato l'invio automatico deve essere specificata una lista di distribuzione valida
if as_tipo_invio = "A" then
		
	// Si controlla se nella chiamata alla funzione è stata specificata una lista di distribuzione
	if isnull(as_cod_tipo_lista_dist) or isnull(as_cod_lista_dist) then
			
		as_messaggio = "Per l'invio automatico è necessario specificare una lista di distribuzione"
		return -1
			
	else
			
		// Si controlla se la lista di distribuzione specificata è valida e se contiene destinatari
		select count(*)
		into :ll_count
		from tab_ind_dest
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			email = 'S' and
			cod_destinatario in ( select cod_destinatario
											  from   det_liste_dist
											  where  cod_azienda = :s_cs_xx.cod_azienda and
		                                    cod_tipo_lista_dist = :as_cod_tipo_lista_dist and
													 	cod_lista_dist = :as_cod_lista_dist);
		
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore in lettura destinatari lista di distribuzione da tabella utenti: " + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ll_count) or ll_count = 0 then
			as_messaggio = "Per l'invio automatico è necessario specificare una lista di distribuzione valida"
			return -1
		end if
			
	end if
				
end if
	
// Si eliminano eventuali valori NULL dall'intestazione e dal corpo del messaggio
if isnull(as_oggetto) then
	as_oggetto = ""
end if

if isnull(as_testo) then
	as_testo = ""
end if

// Impostazione dei destinatari del messaggio
	
ls_destinatari = ""
	
declare destinatari cursor for
select indirizzo
from	 tab_ind_dest
where  cod_azienda = :s_cs_xx.cod_azienda and
		 email = 'S' and
		 cod_destinatario in ( select cod_destinatario
									  from   det_liste_dist
									  where  cod_azienda = :s_cs_xx.cod_azienda and
												cod_tipo_lista_dist = :as_cod_tipo_lista_dist and
												cod_lista_dist = :as_cod_lista_dist);
												
open destinatari;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in costruzione elenco destinatari~nErrore nella open del cursore destinatari: " + sqlca.sqlerrtext
	return -1
end if

// leggo i dati dall'utente

select 
	e_mail,
	sender_name,
	smtp_server,
	smtp_usr, 
	smtp_pwd
into
	:ls_email,
	:ls_sender_name,
	:ls_server,
	:ls_usr,
	:ls_pwd
from utenti
where cod_utente  = :s_cs_xx.cod_utente;

if isnull(ls_email) then ls_email = ""

luo_sendmail = create uo_sendmail

luo_sendmail.uof_set_smtp(ls_server, ls_usr,ls_pwd)
luo_sendmail.uof_set_from(ls_email)
luo_sendmail.uof_set_subject(as_oggetto)
luo_sendmail.uof_set_message(as_testo)
luo_sendmail.uof_set_attachments(as_allegati)

do while true
	
	fetch destinatari	
	into :ls_indirizzo;
	
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in costruzione elenco destinatari~nErrore nella fetch del cursore destinatari: " + sqlca.sqlerrtext
		close destinatari;
		return -1
		
	elseif sqlca.sqlcode = 100 then
		close destinatari;	
		exit
	end if
		
	luo_sendmail.uof_clear_to()
	luo_sendmail.uof_add_to(ls_indirizzo)
	
	if not luo_sendmail.uof_send() then
		as_messaggio = luo_sendmail.uof_get_error()
		destroy luo_sendmail
		close destinatari;
		return -1
	end if
	
loop


as_messaggio = "Messaggio inviato con successo"

return 0
end function

public function boolean uof_invio_sendmail (string as_to[], string as_subject, string as_message, string as_attachment[]);/**
 * stefanop
 * 27/09/2010
 *
 * Consente di inviare una mail con il programma esterno sendmail.
 * Il programma deve essere inserito all'interno della cartella risorse/sendmail/sendmail.exe
 **/
 
string ls_smtp_server, ls_smtp_usd, ls_smtp_pwd, ls_from, ls_error

select 
	e_mail,
	smtp_server,
	smtp_usr,
	smtp_pwd
into
	:ls_from,
	:ls_smtp_server,
	:ls_smtp_usd,
	:ls_smtp_pwd
from utenti
where
	cod_utente  = :s_cs_xx.cod_utente;
	
if sqlca.sqlcode <> 0 then
	return false
else
	return uof_invio_sendmail(ls_from, as_to, as_subject, as_message, as_attachment, ls_smtp_server, ls_smtp_usd, ls_smtp_pwd, ls_error)
end if
end function

private function boolean uof_open_window (ref s_invio_email astr_email);/**
 * stefanop
 * 16/05/2014
 *
 * Apro la finestra di modifica dei parametri per invio email
 **/
 
window_open_parm(w_invio_email, 0, astr_email)

astr_email = message.powerobjectparm

return isnull(astr_email) or astr_email.ab_invio_annullato
end function

private function boolean uof_anteprima_abilitata ();/**
 * stefanop
 * 17/06/2014
 *
 * Controllo se devo far visualizzare la finestra di anteprima o meno.
 * Ha priorità il la variabile ib_silent_mode (impostata dal programmatore) e successivamente
 * il parametro aziendale FIM
 **/
 
boolean lb_fim

// Deve essere a true sia la variabile che il flag, altrimenti non visualizzo niente
if ib_silent_mode then return false

if ib_force_open then return true

guo_functions.uof_get_parametro_azienda("FIM", lb_fim)

return lb_fim
end function

public function boolean uof_invio_sendmail (string as_to[], string as_cc[], string as_subject, string as_message, string as_attachment[], ref string as_error);/**
 * stefanop
 * 27/09/2010
 *
 * Consente di inviare una mail con il programma esterno sendmail.
 * Il programma deve essere inserito all'interno della cartella risorse/sendmail/sendmail.exe
 **/
 
string ls_smtp_server, ls_smtp_usd, ls_smtp_pwd, ls_from, ls_empty[]

select 
	e_mail,
	smtp_server,
	smtp_usr,
	smtp_pwd
into
	:ls_from,
	:ls_smtp_server,
	:ls_smtp_usd,
	:ls_smtp_pwd
from utenti
where
	cod_utente  = :s_cs_xx.cod_utente;
	
if sqlca.sqlcode <> 0 then
	as_error = "Impossibile recuperare le informazioni di invio per l'utente collegato"
	return false
else
	return uof_invio_sendmail(ls_from, as_to, as_cc, ls_empty, as_subject, as_message, as_attachment, ls_smtp_server, ls_smtp_usd, ls_smtp_pwd, as_error)
end if
end function

public function boolean uof_inoltro_cs (ref string as_destinatari[]);/** 
 * stefanop
 * 17/06/2014
 *
 * L'inoltro alla CS è abilitato?
 **/
 
string ls_destinatari
boolean lb_abilitato

guo_functions.uof_get_parametro_azienda("AIC", ls_destinatari)

if isnull(ls_destinatari) or ls_destinatari = "" then
	return false
else
	g_str.explode(ls_destinatari, ";", as_destinatari)
	return true
end if
end function

public function integer uof_apri_outlook (string as_to[], string as_subject, string as_message, string as_allegati[], ref string as_error);/**
 * stefanop
 * 04/07/2014
 *
 * Funzione recuperata dalla vecchia gestione "Invia PDF"
 * Consente di aprire outlook con una serie di allegati
 *
 * Ritorna: 1 tuttok, -1 error
 **/
 
long ll_return, ll_file 
oleobject lole_outlook, lole_item, lole_attach, lole_recipients

try 
	lole_outlook = create oleobject
	
	ll_return = lole_outlook.connecttonewobject(uof_get_outlook_version())
	
	if ll_return <> 0 THEN
		as_error = "Impossibile connettersi ad Outlook. Codice errore: " + string(ll_return)		
		destroy iole_outlook
		return -1
	end if
	
	//  creato un nuovo elemento del tipo specificato
	lole_outlook = lole_outlook.createitem(0)
	lole_outlook.subject = as_subject
	
	if upperbound(as_to) > 0 then
		lole_recipients = lole_outlook.recipients
		
		for ll_file = 1 to upperbound(as_to)
			lole_recipients.add(as_to[ll_file])
		next
	end if
		
	//visualizza la mail da inviare
	lole_outlook.display
	
	lole_attach = lole_outlook.attachments
		
	// Il file specificato viene allegato solo se esiste al percorso indicato
	if upperbound(as_allegati) > 0 then
		for ll_file = 1 to upperbound(as_allegati)
			if fileexists(as_allegati[ll_file]) then
				lole_attach.add(as_allegati[ll_file])
			end if
		next
	end if
	
	lole_outlook.Display
	
	// tolto perchè mangia le firme
	if not isnull(as_message) and as_message <> "" then
		lole_outlook.body = as_message + lole_outlook.body					
	end if
	
	destroy lole_attach
	destroy lole_outlook
	
	return 1
catch (RuntimeError e)
	as_error = "Errore di runtime.~r~nProbabilmente Outlook non è installato nella postazione.~r~n" + g_str.tostring(e)
end try

return -1
end function

private function string uof_get_outlook_version ();/**
 * stefanop
 * 04/07/2014
 *
 * Creo l'oggetto Outllok tramite Ole con la versione corretta
 **/
 
string ls_outlook_version

if RegistryGet("HKEY_CLASSES_ROOT\Outlook.Application\CurVer", "", RegString!, ls_outlook_version) < 0 then
	ls_outlook_version = "Outlook.Application"
end if

return ls_outlook_version
end function

public function boolean uof_is_outlook_installed ();/**
 * stefanop
 * 14/07/2014
 *
 * Controllo se c'è outlook installato.
 **/
 
boolean lb_installed
oleobject lole_outlook

try
	lole_outlook = create oleobject
	
	if lole_outlook.connecttonewobject(uof_get_outlook_version()) = 0 then
		lb_installed = true
	else
		lb_installed = false
	end if
	
	destroy lole_outlook
catch (Exception e)
	lb_installed = false
end try

return lb_installed
end function

on uo_outlook.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_outlook.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

