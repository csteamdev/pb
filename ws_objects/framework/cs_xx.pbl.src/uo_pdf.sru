﻿$PBExportHeader$uo_pdf.sru
forward
global type uo_pdf from nonvisualobject
end type
end forward

global type uo_pdf from nonvisualobject
end type
global uo_pdf uo_pdf

forward prototypes
public function integer uof_merge (string as_pdf[], string as_merge_pdf, ref string as_error)
end prototypes

public function integer uof_merge (string as_pdf[], string as_merge_pdf, ref string as_error);/**
 * stefanop
 * 29/01/2012
 *
 * Unisce tutti i pdf passati come array in un unico pdf
 **/
 
string ls_pdftk, ls_command
int li_i
uo_shellexecute luo_shell

setnull(as_error)
ls_pdftk = s_cs_xx.volume + s_cs_xx.risorse + "libs\pdftk.exe"

if not fileexists(ls_pdftk) then
	as_error = "Il file " + ls_pdftk + " NON è stato trovato"
	return -1
end if

if isnull(as_merge_pdf) or as_merge_pdf = "" then
	as_error = "Il percorso del file pdf di destinazione risulta vuoto o non valido."
	return -1
end if

for li_i = 1 to upperbound(as_pdf)
	
	if not fileexists(as_pdf[li_i]) then
		
		if isnull(as_pdf[li_i]) or as_pdf[li_i] = "" then
			as_error = "Uno dei file da unire risulta vuoto o non inizializzato."
		else
			as_error = "Un file NON è stato trovato.~r~nFile: " +  as_pdf[li_i]
		end if
		
		return -1
	end if
	
next

ls_command = '"' + g_str.implode( as_pdf, '" "') + '"'
ls_command += ' output "' + as_merge_pdf + '"'

ls_pdftk += " " + ls_command

luo_shell = create uo_shellexecute

try
	luo_shell.uof_run(ls_pdftk)
catch(RuntimeError ex)
	as_error = ex.getMessage()
	return -1
end try

destroy luo_shell

// Se il file esiste allora il processo è concluso correttamente
if fileexists(as_merge_pdf) then
	return 0
else
	as_error = "Il file " + as_merge_pdf + " non è stato generato. Motivo sconosciuto"
	return -1
end if
end function

on uo_pdf.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_pdf.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

