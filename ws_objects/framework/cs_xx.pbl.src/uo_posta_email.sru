﻿$PBExportHeader$uo_posta_email.sru
$PBExportComments$oggetto che sostituisce uo_outlook
forward
global type uo_posta_email from nonvisualobject
end type
end forward

global type uo_posta_email from nonvisualobject
end type
global uo_posta_email uo_posta_email

type variables
oleobject iole_outlook, iole_item, iole_attach
end variables

forward prototypes
public function integer uof_controllo (string as_messaggio, boolean ab_conferma)
public function integer uof_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_destinatari[], string as_allegati[], boolean ab_conferma, ref string as_messaggio)
public function integer uof_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_cod_tipo_lista_dist, string as_cod_lista_dist, string as_allegati[], boolean ab_conferma, ref string as_messaggio)
end prototypes

public function integer uof_controllo (string as_messaggio, boolean ab_conferma);string ls_valore

long   ll_return


// Lettura del parametro MAI dal registro di sistema, il parametro MAI abilita o meno l'invio di messaggi
// Qualsiasi errore di lettura dal registro (-1) interrompe l'operazione

ll_return = registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "mai", ls_valore)

if ll_return = -1 then 
	return -1
end if

// Anche con il parametro abilitato è ancora possibile richiedere la conferma dell'utente prima di continuare

if long(ls_valore) > 0 then
	
	// parametro MAI abilitato
	
	if ab_conferma then
		
		// è stato specificato di richiedere la conferma dell'utente
		
		if as_messaggio = "" or isnull(as_messaggio) then
			// nessun testo specificato per il messaggio di conferma, viene usato un testo di default
			as_messaggio = "Procedere con l'invio del messaggio?"
		end if
		
		if g_mb.messagebox("Invio Messaggio",as_messaggio,question!,yesno!,2) = 2 then
			// l'utente sceglie di non continuare
			return 50
		else
			// l'utente sceglie di continuare
			return 0
		end if
		
	else
		
		// non è richiesta la conferma dell'utente
		
		return 0
		
	end if
	
else
	
	// parametro MAI disabilitato
	
	return 100
	
end if
end function

public function integer uof_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_destinatari[], string as_allegati[], boolean ab_conferma, ref string as_messaggio);long    ll_return, ll_i

string  ls_destinatari

boolean lb_trovato


// Chiamata alla funzione di controllo

ll_return = uof_controllo(as_messaggio,ab_conferma)

if ll_return = -1 then
	// Si è verificato un errore nel controllo del registro di sistema
	as_messaggio = "Errore in controllo parametro MAI dal registro di sistema"
	return -1
elseif ll_return = 100 then
	// Il parametro MAI è impostato in modo da disabilitare l'invio di messaggi
	as_messaggio = "Parametro MAI disattivato nel registro di sistema"
	return 100
elseif ll_return = 50 then
	// L'utente, dopo la richiesta di conferma, ha deciso di interrompere l'invio del messaggio
	as_messaggio = "Operazione interrotta dall'utente"
	return 100
end if

// La funzione gestisce la creazione di 4 tipi di elementi di OUTLOOK:
//		0 = messaggio email
//		1 = appuntamento
//		2 = contatto
//		3 = attività

if isnull(al_elemento) or al_elemento < 0 or al_elemento > 3 then
	as_messaggio = "Tipo elemento deve essere uno dei seguenti valori: 0 (mail), 1 (appuntamento), 2 (contatto), 3 (attività)"
	return -1
end if

// Controlli da eseguire nel caso in cui l'elemento da creare è un messaggio email

if al_elemento = 0 then
	
	// La funzione può inviare il messaggio in due diversi modi:
	//		(M)anuale = il messaggio viene creato, compilato e visualizzato; l'utente deve cliccare INVIA in OUTLOOK
	//		(A)automatico = il messaggio viene creato e spedito senza essere visualizzato e senza interazione dell'utente
	
	if isnull(as_tipo_invio) or (as_tipo_invio <> "M" and as_tipo_invio <> "A") then
		as_messaggio = "Tipo invio deve essere uno dei seguenti valori: M (manuale), A (automatico)"
		return -1
	end if
	
	// Se è stato selezionato l'invio automatico deve essere presente almeno un destinatario
	
	if as_tipo_invio = "A" then
		
		// Si controlla se nella chiamata alla funzione è stato specificato almeno un destinatario
	
		lb_trovato = false
		
		for ll_i = 1 to upperbound(as_destinatari)
			if isnull(as_destinatari[ll_i]) then
				as_destinatari[ll_i] = ""
			elseif as_destinatari[ll_i] <> "" then
				lb_trovato = true
			end if
		next
		
		if upperbound(as_destinatari) < 1  or lb_trovato = false then
			as_messaggio = "Per l'invio automatico è necessario specificare almeno un destinatario"
			return -1
		end if
				
	end if
	
	// Si eliminano eventuali valori NULL dall'intestazione e dal corpo del messaggio
	
	if isnull(as_oggetto) then
		as_oggetto = ""
	end if
	
	if isnull(as_testo) then
		as_testo = ""
	end if
	
end if

// L'oggetto iole_outlook viene creato e collegato all'applicazione OUTLOOK

iole_outlook = create oleobject

ll_return = iole_outlook.connecttonewobject("outlook.application")

if ll_return <> 0 THEN
	as_messaggio = "Impossibile connettersi ad Outlook. Codice errore: " + string(ll_return)
	destroy iole_outlook
	return -1
end if

// Viene creato un nuovo elemento del tipo specificato

iole_item = iole_outlook.createitem(al_elemento)

// Se l'elemento è un messaggio email questo viene compilato e inviato, altrimenti viene solo visualizzato

if al_elemento = 0 then
	
	// Impostazione dell'oggetto del messaggio

	iole_item.subject = as_oggetto
	
	// Impostazione del testo del messaggio
	
	iole_item.body = as_testo
	
	// Impostazione dei destinatari del messaggio
	
	ls_destinatari = ""
	
	for ll_i = 1 to upperbound(as_destinatari)
		
		if not isnull(as_destinatari[ll_i]) and as_destinatari[ll_i] <> "" then
			
			// I destinatari multipli vanno separati da punto e virgola
		
			if ll_i > 1 then
				ls_destinatari = ls_destinatari + ";"
			end if
			
			ls_destinatari = ls_destinatari + as_destinatari[ll_i]
			
		end if
		
	next
	
	iole_item.to = ls_destinatari
	
	// Gestione degli allegati
	
	iole_attach = iole_item.attachments
	
	for ll_i = 1 to upperbound(as_allegati)
		
		// Il file specificato viene allegato solo se esiste al percorso indicato
		
		if fileexists(as_allegati[ll_i]) then
			iole_attach.add(as_allegati[ll_i])
		end if
		
	next
	
	destroy iole_attach
	
	// Gestione del tipo invio (Automatico / Manuale)

	choose case as_tipo_invio
			
		case "M"
			
			iole_item.display
			
		case "A"
			
			iole_item.send
			
	end choose
	
else
	
	// L'elemento non è un messaggio email e quindi viene solo visualizzato
	
	iole_item.display
	
end if

// Gli oggetti istanziati vengono distrutti dopo essere stati scollegati dall'applicazione OUTLOOK

destroy iole_item

iole_outlook.disconnectobject()

destroy iole_outlook

as_messaggio = "Messaggio inviato con successo"

return 0
end function

public function integer uof_outlook (long al_elemento, string as_tipo_invio, string as_oggetto, string as_testo, string as_cod_tipo_lista_dist, string as_cod_lista_dist, string as_allegati[], boolean ab_conferma, ref string as_messaggio);long    ll_return, ll_i, ll_count

string  ls_destinatari, ls_indirizzo

boolean lb_trovato


// Chiamata alla funzione di controllo

ll_return = uof_controllo(as_messaggio,ab_conferma)

if ll_return = -1 then
	// Si è verificato un errore nel controllo del registro di sistema
	as_messaggio = "Errore in controllo parametro MAI dal registro di sistema"
	return -1
elseif ll_return = 100 then
	// Il parametro MAI è impostato in modo da disabilitare l'invio di messaggi
	as_messaggio = "Parametro MAI disattivato nel registro di sistema"
	return 100
elseif ll_return = 50 then
	// L'utente, dopo la richiesta di conferma, ha deciso di interrompere l'invio del messaggio
	as_messaggio = "Operazione interrotta dall'utente"
	return 100
end if

// La funzione gestisce la creazione di 4 tipi di elementi di OUTLOOK:
//		0 = messaggio email
//		1 = appuntamento
//		2 = contatto
//		3 = attività

if isnull(al_elemento) or al_elemento < 0 or al_elemento > 3 then
	as_messaggio = "Tipo elemento deve essere uno dei seguenti valori: 0 (mail), 1 (appuntamento), 2 (contatto), 3 (attività)"
	return -1
end if

// Controlli da eseguire nel caso in cui l'elemento da creare è un messaggio email

if al_elemento = 0 then
	
	// La funzione può inviare il messaggio in due diversi modi:
	//		(M)anuale = il messaggio viene creato, compilato e visualizzato; l'utente deve cliccare INVIA in OUTLOOK
	//		(A)automatico = il messaggio viene creato e spedito senza essere visualizzato e senza interazione dell'utente
	
	if isnull(as_tipo_invio) or (as_tipo_invio <> "M" and as_tipo_invio <> "A") then
		as_messaggio = "Tipo invio deve essere uno dei seguenti valori: M (manuale), A (automatico)"
		return -1
	end if
	
	// Se è stato selezionato l'invio automatico deve essere specificata una lista di distribuzione valida
	
	if as_tipo_invio = "A" then
		
		// Si controlla se nella chiamata alla funzione è stata specificata una lista di distribuzione
	
		if isnull(as_cod_tipo_lista_dist) or isnull(as_cod_lista_dist) then
			
			as_messaggio = "Per l'invio automatico è necessario specificare una lista di distribuzione"
			return -1
			
		else
			
			// Si controlla se la lista di distribuzione specificata è valida e se contiene destinatari
			
			select count(*)
			into   :ll_count
			from	 tab_ind_dest
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       email = 'S' and
			       cod_destinatario in ( select cod_destinatario
												  from   det_liste_dist
												  where  cod_azienda = :s_cs_xx.cod_azienda and
			                                    cod_tipo_lista_dist = :as_cod_tipo_lista_dist and
														 	cod_lista_dist = :as_cod_lista_dist);
			
			if sqlca.sqlcode < 0 then
				as_messaggio = "Errore in lettura destinatari lista di distribuzione da tabella utenti: " + sqlca.sqlerrtext
				return -1
			elseif sqlca.sqlcode = 100 or isnull(ll_count) or ll_count = 0 then
				as_messaggio = "Per l'invio automatico è necessario specificare una lista di distribuzione valida"
				return -1
			end if
			
		end if
				
	end if
	
	// Si eliminano eventuali valori NULL dall'intestazione e dal corpo del messaggio
	
	if isnull(as_oggetto) then
		as_oggetto = ""
	end if
	
	if isnull(as_testo) then
		as_testo = ""
	end if
	
end if

// L'oggetto iole_outlook viene creato e collegato all'applicazione OUTLOOK

iole_outlook = create oleobject

ll_return = iole_outlook.connecttonewobject("outlook.application")

if ll_return <> 0 THEN
	as_messaggio = "Impossibile connettersi ad Outlook. Codice errore: " + string(ll_return)
	destroy iole_outlook
	return -1
end if

// Viene creato un nuovo elemento del tipo specificato

iole_item = iole_outlook.createitem(al_elemento)

// Se l'elemento è un messaggio email questo viene compilato e inviato, altrimenti viene solo visualizzato

if al_elemento = 0 then
	
	// Impostazione dell'oggetto del messaggio

	iole_item.subject = as_oggetto
	
	// Impostazione del testo del messaggio
	
	iole_item.body = as_testo
	
	// Impostazione dei destinatari del messaggio
	
	ls_destinatari = ""
	
	declare destinatari cursor for
	select indirizzo
	from	 tab_ind_dest
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 email = 'S' and
			 cod_destinatario in ( select cod_destinatario
										  from   det_liste_dist
										  where  cod_azienda = :s_cs_xx.cod_azienda and
													cod_tipo_lista_dist = :as_cod_tipo_lista_dist and
													cod_lista_dist = :as_cod_lista_dist);
													
	open destinatari;
	
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore in costruzione elenco destinatari~nErrore nella open del cursore destinatari: " + sqlca.sqlerrtext
		return -1
	end if
	
	do while true
		
		fetch destinatari
		into  :ls_indirizzo;
		
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore in costruzione elenco destinatari~nErrore nella fetch del cursore destinatari: " + sqlca.sqlerrtext
			close destinatari;
			return -1
		elseif sqlca.sqlcode = 100 then
			close destinatari;
			exit
		end if
		
		if ls_destinatari <> "" then
			ls_destinatari = ls_destinatari + ";"
		end if
		
		ls_destinatari = ls_destinatari + ls_indirizzo
		
	loop
	
	iole_item.to = ls_destinatari
	
	// Gestione degli allegati
	
	iole_attach = iole_item.attachments
	
	for ll_i = 1 to upperbound(as_allegati)
		
		// Il file specificato viene allegato solo se esiste al percorso indicato
		
		if fileexists(as_allegati[ll_i]) then
			iole_attach.add(as_allegati[ll_i])
		end if
		
	next
	
	destroy iole_attach
	
	// Gestione del tipo invio (Automatico / Manuale)

	choose case as_tipo_invio
			
		case "M"
			
			iole_item.display
			
		case "A"
			
			iole_item.send
			
	end choose
	
else
	
	// L'elemento non è un messaggio email e quindi viene solo visualizzato
	
	iole_item.display
	
end if

// Gli oggetti istanziati vengono distrutti dopo essere stati scollegati dall'applicazione OUTLOOK

destroy iole_item

iole_outlook.disconnectobject()

destroy iole_outlook

as_messaggio = "Messaggio inviato con successo"

return 0
end function

on uo_posta_email.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_posta_email.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

