﻿$PBExportHeader$uo_regex.sru
forward
global type uo_regex from nonvisualobject
end type
end forward

global type uo_regex from nonvisualobject native "PbniRegex120.pbx"
public function  string of_hello()
public function  string pcreversion()
public function  boolean initialize(string as_pattern, boolean ab_globalscope, boolean ab_casesensitive)
public function  boolean test(string teststring)
public subroutine  setutf8(boolean isutf)
public function  long search(string searchstring)
public function  long matchcount()
public function  long matchposition(long al_index)
public function  long matchlength(long al_index)
public function  long groupcount(long al_matchindex)
public function  string match(long al_index)
public function  string group(long al_matchindex, long al_groupindex)
public function  string replace(string as_searchstring, string as_replacestring)
public function  long groupposition(long al_matchindex, long al_groupindex)
public function  long grouplength(long al_matchindex, long al_groupindex)
public subroutine  setmultiline(boolean ismulti)
public function  boolean ismultiline()
public function  boolean isutf8()
public function  boolean study()
public function  boolean getdotmatchnewline()
public subroutine  setdotmatchnewline(boolean match)
public function  boolean getextendedsyntax()
public subroutine  setextendedsyntax(boolean extended)
public function  boolean getungreedy()
public subroutine  setungreedy(boolean greedy)
public function  string getpattern()
public function  string getlasterror()
public function  string stringtest(string str)
end type
global uo_regex uo_regex

on uo_regex.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_regex.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

