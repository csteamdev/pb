﻿$PBExportHeader$uo_secure_id.sru
forward
global type uo_secure_id from nonvisualobject
end type
end forward

global type uo_secure_id from nonvisualobject
end type
global uo_secure_id uo_secure_id

type variables
// Chiave di sicurezza da aggiungere per incrementare la complessità del recuper della
// chiave.
// Attenzione che modificando questa parola vanno modificate tutte le app che ne fanno uso
// ad esempio l'applicazione omniaweb.

constant String SECURE_SALT = "Cs@2015"

private:
	uo_crypto iuo_crypto
	
	boolean ib_initialized = false
end variables

forward prototypes
private function uo_crypto get_crypto ()
public subroutine uof_secure_cliente (string as_cod_azienda, string as_cod_cliente) throws sqlexception
public function integer uof_secure_clienti () throws sqlexception
public function string uof_secure_piva (string as_piva, string as_key)
end prototypes

private function uo_crypto get_crypto ();/**
 * stefanop
 * 21/09/2015
 *
 * Lazy interface per il caricamento, solo quando necessario, dell'oggeto uo_crypto
 **/
 
if not ib_initialized then
	iuo_crypto = create uo_crypto
	ib_initialized = true
end if

return iuo_crypto
end function

public subroutine uof_secure_cliente (string as_cod_azienda, string as_cod_cliente) throws sqlexception;/**
 * stefanop
 * 21/09/2015
 *
 * Aggiorna il cliente con il secure id calcolato
 **/

string ls_secure_id
SqlException luo_ex

ls_secure_id = get_crypto().uof_sha1(SECURE_SALT + as_cod_cliente)

update anag_clienti
set secure_id = :ls_secure_id
where cod_azienda = :as_cod_azienda and
		 cod_cliente = :as_cod_cliente;
		 
if sqlca.sqlcode < 0 then
	luo_ex = create SqlException
	luo_ex.initialize(sqlca)
	
	rollback;
	
	throw luo_ex
end if

commit;
end subroutine

public function integer uof_secure_clienti () throws sqlexception;/**
 * stefanop
 * 21/09/2015
 *
 * Processa tutti i clienti, di tutte le aziende, e aggiorna/calcola il campo secure_id
 **/

string ls_cod_azienda, ls_cod_cliente, ls_secure_id
long ll_rows, ll_i
datastore lds_store

ll_rows = guo_functions.uof_crea_datastore(lds_store, "select cod_azienda, cod_cliente from anag_clienti")

for ll_i = 1 to ll_rows
	ls_cod_azienda = lds_store.getitemstring(ll_i, "cod_azienda")
	ls_cod_cliente = lds_store.getitemstring(ll_i, "cod_cliente")
	
	uof_secure_cliente(ls_cod_azienda, ls_cod_cliente)
	
next
 
return 1
end function

public function string uof_secure_piva (string as_piva, string as_key);/**
 * stefanop
 * 21/09/2015
 *
 * Aggiorna il cliente con il secure id calcolato
 **/

string ls_secure_id

//ls_secure_id = get_crypto().uof_sha1(as_key + as_piva)
ls_secure_id = get_crypto().uof_sha_256(as_key + as_piva)

return ls_secure_id
end function

on uo_secure_id.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_secure_id.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

