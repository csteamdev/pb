﻿$PBExportHeader$uo_sendmail.sru
forward
global type uo_sendmail from nonvisualobject
end type
end forward

global type uo_sendmail from nonvisualobject
end type
global uo_sendmail uo_sendmail

type variables
constant int SEND_OK = 0
constant int SEND_FAILED = 1


private:
	// indica che il file sendmail.exe esiste
	boolean ib_sendmail = false

	// messaggio dell'ultimo errore
	string is_error
	
	// percorso di sendmail
	string is_sendmail_exe
	string is_temp_folder
	string is_temp_file
	
	boolean ib_html = false
	
	string is_from
	string is_to[], is_cc[], is_bcc[]
	string is_subject
	string is_message
	string is_attachments[]
	
	string is_smtp_host, is_smtp_usd, is_smtp_pwd
	
	string is_log_file
end variables

forward prototypes
public function boolean uof_set_sendmail_path (string as_filepath)
public subroutine uof_set_from (string as_email)
public function string uof_get_from ()
public subroutine uof_set_to (string as_emails[])
public subroutine uof_get_to (ref string as_emails[])
public subroutine uof_add_to (string as_emails)
public subroutine uof_add_to (string as_emails[])
public function boolean uof_has_to (string as_email)
public subroutine uof_clear_to ()
public subroutine uof_set_subject (string as_subject)
public subroutine uof_set_message (string as_message)
public subroutine uof_set_smtp (string as_host, string as_username, string as_password)
public function boolean uof_send ()
public function string uof_get_error ()
private function boolean uof_execute (string as_command)
public subroutine uof_set_html (boolean ab_email_html)
private function boolean uof_create_message_file (string as_message)
private function boolean uof_delete_message_file ()
public subroutine uof_set_bcc (string as_emails[])
public subroutine uof_set_cc (string as_emails[])
public subroutine uof_add_bcc (string as_emails[])
public function boolean uof_has_bcc (string as_email)
public function boolean uof_has_cc (string as_email)
public subroutine uof_add_cc (string as_emails[])
public subroutine uof_add_bcc (string as_emails)
public subroutine uof_add_cc (string as_emails)
public subroutine uof_clear_bcc ()
public subroutine uof_clear_cc ()
public subroutine uof_clear ()
public subroutine uof_set_attachments (string as_attachments[])
public subroutine uof_clear_attachments ()
public subroutine uof_add_attachments (string as_attachments[])
public subroutine uof_add_attachment (string as_attachment)
public subroutine uof_set_log (string as_path)
public subroutine uof_sleep ()
end prototypes

public function boolean uof_set_sendmail_path (string as_filepath);/**
 * stefanop
 * 25/11/2011
 *
 * Imposto il percorso per l'exe di sendmail.
 * L'oggetto imposta in automatico il percorso delle risose, ma è possibile forzarlo
 * con un percorso a piacere
 **/
 
setnull(is_error)

if not fileexists(as_filepath) then
	is_error = "Il file " + as_filepath + " non esiste."
	ib_sendmail = false
end if
	
is_sendmail_exe = as_filepath
ib_sendmail = true

return ib_sendmail
end function

public subroutine uof_set_from (string as_email);/**
 * stefanop
 * 28/11/2011
 *
 * Mittente della email
 **/
 
this.is_from = as_email
end subroutine

public function string uof_get_from ();/**
 * stefanop
 * 28/11/2011
 *
 * Mittente della email
 **/
 
return this.is_from
end function

public subroutine uof_set_to (string as_emails[]);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/
 	
this.is_to = as_emails
end subroutine

public subroutine uof_get_to (ref string as_emails[]);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/
 	
as_emails = this.is_to
end subroutine

public subroutine uof_add_to (string as_emails);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/

uof_add_to({as_emails})
end subroutine

public subroutine uof_add_to (string as_emails[]);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/

int li_i

for li_i = 1 to upperbound(as_emails)
	
	if len(as_emails[li_i]) < 1 or uof_has_to(as_emails[li_i]) then continue
	
	this.is_to[upperbound(is_to) + 1] = as_emails[li_i]
	
next
end subroutine

public function boolean uof_has_to (string as_email);/**
 * stefanop
 * 28/11/2011
 *
 * Controllo se l'indirizzo email è presente tra i destinatari
 **/
 
int li_i

for li_i = 1 to upperbound(is_to)
	
	if is_to[li_i] = as_email then return true
	
next

return false
end function

public subroutine uof_clear_to ();string ls_empty[]

is_to = ls_empty
end subroutine

public subroutine uof_set_subject (string as_subject);is_subject = as_subject
end subroutine

public subroutine uof_set_message (string as_message);is_message = as_message
end subroutine

public subroutine uof_set_smtp (string as_host, string as_username, string as_password);is_smtp_host = as_host
is_smtp_usd = as_username
is_smtp_pwd = as_password
end subroutine

public function boolean uof_send ();/**
 * stefanop
 * 28/11/2011
 *
 * Invio email
 **/
 
string ls_command, ls_valid_attachments[]
int li_i
boolean lb_send_result

// errori nell'impostazione del percorso sendmail.exe
if not ib_sendmail then return false

setnull(is_error)

// mittente
if len(is_from) < 1 then
	is_error = "Per inviare una mail occorre specificare il mittente."
	return false
end if
ls_command = " -f " + is_from

// destinatari
if upperbound(is_to) < 1 then
	is_error = "Per inviare una mail occorre specificare almeno un mittente."
	return false
end if
ls_command += " -t " + guo_functions.uof_implode(is_to, " ", true)

// CC - BCC
if upperbound(is_cc) > 0 then ls_command += " -cc " + guo_functions.uof_implode(is_cc, " ", true)
if upperbound(is_bcc) > 0 then ls_command += " -bcc " + guo_functions.uof_implode(is_bcc, " ", true)

// subject
ls_command += " -u ~"" + is_subject + "~""

// message
if not uof_create_message_file(is_message) then
	return false
end if
ls_command += " -o message-file=~"" + is_temp_file + "~""

// attachments
if upperbound(is_attachments) > 0 then
	// ricontrollo l'esistenza effettiva del file
	for li_i = 1 to upperbound(is_attachments)
		if len(is_attachments[li_i]) < 1 or not fileexists(is_attachments[li_i]) then continue
		ls_valid_attachments[upperbound(ls_valid_attachments) + 1] = is_attachments[li_i]
	next
	
	// se i file son prenseti li allego alla mail
	if upperbound(ls_valid_attachments) > 0 then  ls_command += " -a ~"" + guo_functions.uof_implode(ls_valid_attachments, "~" ~"", true) + "~""
end if

// server
if len(is_smtp_host) < 1 then
	is_error = "Impostare un indirizzo server SMTP valido e riprovare."
	return false
end if
ls_command += " -s " + is_smtp_host

// html o plain-text
if ib_html then ls_command += " -o message-content-type=html"

if len(is_smtp_usd) > 0 then ls_command += " -xu " + is_smtp_usd
if len(is_smtp_pwd) > 0 then ls_command += " -xp " + is_smtp_pwd

if not isnull(is_log_file) and is_log_file <> "" then
	ls_command += " -l " + is_log_file
end if

lb_send_result = uof_execute(ls_command)
uof_delete_message_file()

return lb_send_result
end function

public function string uof_get_error ();return is_error
end function

private function boolean uof_execute (string as_command);long ll_return

uo_shellexecute luo_shell
luo_shell = create uo_shellexecute

setnull(is_error)

// EnMe 02-05-2017 introdotto tempo attesa basato su parametro aziendale WBS (Waint Before Send)
uof_sleep()

ll_return = luo_shell.uof_run(is_sendmail_exe + as_command)

if ll_return <> 0 then is_error = "Errore durante l'invio email."

destroy luo_shell

return isnull(is_error)
end function

public subroutine uof_set_html (boolean ab_email_html);/**
 * Stefanop
 * 29/11/2011
 *
 * Indica se il messaggio è in formato HTML o plain-text
 **/
 
 
this.ib_html = ab_email_html
end subroutine

private function boolean uof_create_message_file (string as_message);/**
 * stefanop
 * 29/11/2011
 * 
 * Creo il file con il messaggio in quanto la riga di comando non accetta i caratteri a capo
 **/
 
// stefanop: 17/0/2017: fix a capo
if ib_html then
	as_message = g_str.replaceAll(as_message, "~r~n", "<br>", true)
end if
 

is_temp_file = is_temp_folder + string(datetime(today(), now()), "yyyymmddffff") + ".txt"

return guo_functions.uof_file_write(is_temp_file, as_message, false, is_error)
end function

private function boolean uof_delete_message_file ();/**
 * stefanop
 * 29/11/2011
 * 
 * Creo il file con il messaggio in quanto la riga di comando non accetta i caratteri a capo
 **/
 

return filedelete(is_temp_file)
end function

public subroutine uof_set_bcc (string as_emails[]);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/
 	
this.is_bcc = as_emails
end subroutine

public subroutine uof_set_cc (string as_emails[]);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/
 	
this.is_cc = as_emails
end subroutine

public subroutine uof_add_bcc (string as_emails[]);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/

int li_i

for li_i = 1 to upperbound(as_emails)
	
	if len(as_emails[li_i]) < 1 or uof_has_bcc(as_emails[li_i]) then continue
	
	this.is_bcc[upperbound(is_bcc) + 1] = as_emails[li_i]
	
next
end subroutine

public function boolean uof_has_bcc (string as_email);/**
 * stefanop
 * 28/11/2011
 *
 * Controllo se l'indirizzo email è presente tra i destinatari
 **/
 
int li_i

for li_i = 1 to upperbound(is_bcc)
	
	if is_bcc[li_i] = as_email then return true
	
next

return false
end function

public function boolean uof_has_cc (string as_email);/**
 * stefanop
 * 28/11/2011
 *
 * Controllo se l'indirizzo email è presente tra i destinatari
 **/
 
int li_i

for li_i = 1 to upperbound(is_cc)
	
	if is_cc[li_i] = as_email then return true
	
next

return false
end function

public subroutine uof_add_cc (string as_emails[]);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/

int li_i

for li_i = 1 to upperbound(as_emails)
	
	if len(as_emails[li_i]) < 1 or uof_has_cc(as_emails[li_i]) then continue
	
	this.is_cc[upperbound(is_cc) + 1] = as_emails[li_i]
	
next
end subroutine

public subroutine uof_add_bcc (string as_emails);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/

uof_add_bcc({as_emails})
end subroutine

public subroutine uof_add_cc (string as_emails);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/

uof_add_cc({as_emails})
end subroutine

public subroutine uof_clear_bcc ();string ls_empty[]

is_bcc = ls_empty
end subroutine

public subroutine uof_clear_cc ();string ls_empty[]

is_cc = ls_empty
end subroutine

public subroutine uof_clear ();string ls_empty[]

is_to = ls_empty
is_cc = ls_empty
is_bcc = ls_empty
end subroutine

public subroutine uof_set_attachments (string as_attachments[]);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/
 	
this.is_attachments = as_attachments
end subroutine

public subroutine uof_clear_attachments ();string ls_empty[]

is_attachments = ls_empty
end subroutine

public subroutine uof_add_attachments (string as_attachments[]);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/

int li_i

for li_i = 1 to upperbound(as_attachments)
	
	if len(as_attachments[li_i]) < 1 or not fileexists(as_attachments[li_i]) then continue
	
	this.is_attachments[upperbound(is_attachments) + 1] = as_attachments[li_i]
	
next
end subroutine

public subroutine uof_add_attachment (string as_attachment);/**
 * stefanop
 * 28/11/2011
 *
 * Imposto i destinatari
 **/

uof_add_attachments({as_attachment})
end subroutine

public subroutine uof_set_log (string as_path);this.is_log_file = as_path
end subroutine

public subroutine uof_sleep ();long	ll_number

guo_functions.uof_get_parametro_azienda("WBS", ll_number)

if isnull(ll_number) then ll_number = 0

sleep(ll_number)

return

end subroutine

on uo_sendmail.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_sendmail.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;
// imposto come percorso automatico quello delle risorse di PB
uof_set_sendmail_path(s_cs_xx.volume + s_cs_xx.risorse + "sendmail\sendmail.exe")

is_temp_folder = guo_functions.uof_get_user_temp_folder()
end event

