﻿$PBExportHeader$uo_serial_communication.sru
$PBExportComments$User Object Per la comunicazione su porta seriale
forward
global type uo_serial_communication from nonvisualobject
end type
end forward

global type uo_serial_communication from nonvisualobject
end type
global uo_serial_communication uo_serial_communication

type prototypes
Function long GetLastError()  Library "kernel32.dll" ALIAS FOR "GetLastError;ansi"
Function long CloseHandle(long hObject) Library "kernel32.dll" ALIAS FOR "CloseHandle;ansi"
Function long FlushFileBuffers(long hFile) Library "kernel32.dll" ALIAS FOR "FlushFileBuffers;ansi"
Function long WriteFile(long hFile, ref string lpBuffer, long nNumberOfBytesToWrite, ref long lpNumberOfBytesWritten, st_overlapped  lpOverlapped) Library "kernel32.dll" alias for "WriteFile;Ansi"
Function long CreateFileA(ref string lpFileName, long dwDesiredAccess, long dwShareMode, long lpSecurityAttributes, long dwCreationDisposition, long dwFlagsAndAttributes, long hTemplateFile) Library "kernel32.dll" alias for "CreateFileA;Ansi"
Function long ReadFile(long hFile, ref string lpBuffer, long nNumberOfBytesToRead, ref long lpNumberOfBytesRead, st_overlapped  lpOverlapped) Library "kernel32.dll" alias for "ReadFile;Ansi"
Function long GetCommState(long hCommDev, ref st_dcb  lpdcb) Library "kernel32.dll" alias for "GetCommState;Ansi"
Function long SetCommState(long hCommDev, ref st_dcb  lpdcb) Library "kernel32.dll" alias for "SetCommState;Ansi"
end prototypes

forward prototypes
public function integer uof_read_com (ref string fs_buffer, string fs_nome_porta, ref string fs_errore)
public function integer uof_write_com (string fs_comando, string fs_nome_porta, long fl_velocita, ref string fs_errore)
end prototypes

public function integer uof_read_com (ref string fs_buffer, string fs_nome_porta, ref string fs_errore);// Funzione che scrive sulla porta seriale assegnata
// tipo: integer
//       0 passed
//      -1 failed
// argomenti:
//           fs_comando tipo stringa per valore
//           fs_nome_porta tipo stringa per valore ("COM1", "COM2", ecc.)
//
// Valori per CreateFileA:
//
// CreateFile(lpszName,	fdwAccess,fdwShareMode,	lpsa, fdwCreate, 
//				  fdwAttrsAndFlags, hTemplateFile);
//
// lpszName:nome pipe,COM1,COM2,\\.\A:,\\.\C: ecc.
//
// fdwAccess: 0
LONG          GENERIC_READ = 2147483648 
LONG			  GENERIC_WRITE = 1073741824 
LONG			  GENERIC_READ_WRITE = 3221225472
//
// fdwShareMode: 
LONG				  SHARE_MODE = 0 
LONG				  FILE_SHARE_READ = 1
LONG				  FILE_SHARE_WRITE = 2
//
// lpsa ?????
//
// fdwCreate: 
LONG			  CREATE_NEW = 1 
LONG			  CREATE_ALWAYS = 2
LONG		     OPEN_EXISTING =3 
LONG			  OPEN_ALWAYS =4
LONG			  TRUNCATE_EXISTING =5               
//
// fdwAttrsAndFlags 
LONG 					  FILE_ATTRIBUTE_READONLY = 1 
LONG					  FILE_ATTRIBUTE_HIDDEN = 2 
LONG					  FILE_ATTRIBUTE_SYSTEM = 4 
LONG					  FILE_ATTRIBUTE_DIRECTORY = 10 
LONG					  FILE_ATTRIBUTE_ARCHIVE = 32 
LONG					  FILE_ATTRIBUTE_NORMAL = 128
LONG					  FILE_ATTRIBUTE_TEMPORARY = 256
LONG					  FILE_ATTRIBUTE_ATOMIC_WRITE = 512
LONG				     FILE_ATTRIBUTE_XACTION_WRITE = 1024
LONG					  FILE_ATTRIBUTE_COMPRESSED = 2048  
LONG					  FILE_FLAG_WRITE_THROUGH = 2147483648
LONG					  FILE_FLAG_OVERLAPPED = 1073741824
LONG					  FILE_FLAG_NO_BUFFERING = 536870912
LONG					  FILE_FLAG_RANDOM_ACCESS = 268435456
LONG					  FILE_FLAG_SEQUENTIAL_SCAN = 134217728
LONG					  FILE_FLAG_DELETE_ON_CLOSE = 67108864
LONG					  FILE_FLAG_BACKUP_SEMANTICS = 33554432
LONG					  FILE_FLAG_POSIX_SEMANTICS = 16777216

long   ll_openport,ll_retvalue,ll_retbytes,ll_null
string ls_buffer, ls_port
st_overlapped lst_overlapped

ll_openport=0
ll_retvalue=0
ll_retbytes=0
ls_buffer=space(100)
ls_port = fs_nome_porta
setnull(ll_null)

ll_openport=CreateFileA(ls_port, GENERIC_WRITE,SHARE_MODE,ll_null,OPEN_EXISTING, & 
								FILE_FLAG_OVERLAPPED,ll_null)

if ll_openport = -1 then
	fs_errore="Non è possibile inizializzare la porta " + fs_nome_porta + ". Verificare " + &
				 "che non vi siano già altre periferiche che la utilizzano."
	ll_retvalue =CloseHandle(ll_openport)
	return -1
end if

ll_retvalue = ReadFile(ll_openport,ls_buffer,len(ls_buffer),ll_retbytes,lst_overlapped)

if ll_retvalue = 0 then
	fs_errore = "Errore in fase di lettura della porta " + fs_nome_porta + "."
	ll_retvalue = CloseHandle(ll_openport)
	return -1
end if

ll_retvalue = CloseHandle(ll_openport)
fs_buffer = ls_buffer

return 0
end function

public function integer uof_write_com (string fs_comando, string fs_nome_porta, long fl_velocita, ref string fs_errore);// Function that write into serial com
// tipo: integer
//       0 passed
//      -1 failed
// arguments:
//           fs_comando    type string pass by value
//           fs_nome_porta type string pass by value ("COM1", "COM2", ecc.)
//				 fl_velocita	type long 	pass by value
//				 fs_errore		type string pass by reference
// Valori per CreateFileA:
//
// CreateFile(lpszName,	fdwAccess,fdwShareMode,	lpsa, fdwCreate, 
//				  fdwAttrsAndFlags, hTemplateFile);
//
// lpszName:nome pipe,COM1,COM2,\\.\A:,\\.\C: ecc.
//
// fdwAccess: 0
LONG          GENERIC_READ = 2147483648 
LONG			  GENERIC_WRITE = 1073741824 
LONG			  GENERIC_READ_WRITE = 3221225472
//
// fdwShareMode: 
LONG				  SHARE_MODE = 0 
LONG				  FILE_SHARE_READ = 1
LONG				  FILE_SHARE_WRITE = 2
//
// lpsa ?????
//
// fdwCreate: 
LONG			  CREATE_NEW = 1 
LONG			  CREATE_ALWAYS = 2
LONG		     OPEN_EXISTING =3 
LONG			  OPEN_ALWAYS =4
LONG			  TRUNCATE_EXISTING =5               
//
// fdwAttrsAndFlags 
LONG 					  FILE_ATTRIBUTE_READONLY = 1 
LONG					  FILE_ATTRIBUTE_HIDDEN = 2 
LONG					  FILE_ATTRIBUTE_SYSTEM = 4 
LONG					  FILE_ATTRIBUTE_DIRECTORY = 10 
LONG					  FILE_ATTRIBUTE_ARCHIVE = 32 
LONG					  FILE_ATTRIBUTE_NORMAL = 128
LONG					  FILE_ATTRIBUTE_TEMPORARY = 256
LONG					  FILE_ATTRIBUTE_ATOMIC_WRITE = 512
LONG				     FILE_ATTRIBUTE_XACTION_WRITE = 1024
LONG					  FILE_ATTRIBUTE_COMPRESSED = 2048  
LONG					  FILE_FLAG_WRITE_THROUGH = 2147483648
LONG					  FILE_FLAG_OVERLAPPED = 1073741824
LONG					  FILE_FLAG_NO_BUFFERING = 536870912
LONG					  FILE_FLAG_RANDOM_ACCESS = 268435456
LONG					  FILE_FLAG_SEQUENTIAL_SCAN = 134217728
LONG					  FILE_FLAG_DELETE_ON_CLOSE = 67108864
LONG					  FILE_FLAG_BACKUP_SEMANTICS = 33554432
LONG					  FILE_FLAG_POSIX_SEMANTICS = 16777216


long   ll_openport,ll_retvalue,ll_retbytes,ll_risposta,ll_null
string ls_command, ls_port
st_overlapped lst_overlapped
st_dcb l_dcb

ll_openport=0
ll_retvalue=0
ll_retbytes=0
setnull(ll_null)
ls_port = fs_nome_porta
ls_command = fs_comando

ll_openport = CreateFileA(ls_port, GENERIC_WRITE,SHARE_MODE,ll_null,OPEN_EXISTING, & 
								  0,ll_null)

if ll_openport = -1 then
	fs_errore="Non è possibile inizializzare la porta " + fs_nome_porta + ". Verificare " + &
				 "che non vi siano già altre periferiche che la utilizzano."
	ll_retvalue = closehandle (ll_openport)
	return -1
end if

//DWORD DCBlength;      /* sizeof(DCB)                     */
//DWORD BaudRate;       /* Baudrate at which running       */
//DWORD fBinary: 1;     /* Binary Mode (skip EOF check)    */
//DWORD fParity: 1;     /* Enable parity checking          */
//DWORD fOutxCtsFlow:1; /* CTS handshaking on output       */
//DWORD fOutxDsrFlow:1; /* DSR handshaking on output       */
//DWORD fDtrControl:2;  /* DTR Flow control                */
//DWORD fDsrSensitivity:1; /* DSR Sensitivity              */
//DWORD fTXContinueOnXoff: 1; /* Continue TX when Xoff sent */
//DWORD fOutX: 1;       /* Enable output X-ON/X-OFF        */
//DWORD fInX: 1;        /* Enable input X-ON/X-OFF         */
//DWORD fErrorChar: 1;  /* Enable Err Replacement          */
//DWORD fNull: 1;       /* Enable Null stripping           */
//DWORD fRtsControl:2;  /* Rts Flow control                */
//DWORD fAbortOnError:1; /* Abort all reads and writes on Error */
//DWORD fDummy2:17;     /* Reserved                        */
//WORD wReserved;       /* Not currently used              */
//WORD XonLim;          /* Transmit X-ON threshold         */
//WORD XoffLim;         /* Transmit X-OFF threshold        */
//BYTE ByteSize;        /* Number of bits/byte, 4-8        */
//BYTE Parity;          /* 0-4=None,Odd,Even,Mark,Space    */
//BYTE StopBits;        /* 0,1,2 = 1, 1.5, 2               */
//char XonChar;         /* Tx and Rx X-ON character        */
//char XoffChar;        /* Tx and Rx X-OFF character       */
//char ErrorChar;       /* Error replacement char          */
//char EofChar;         /* End of Input character          */
//char EvtChar;         /* Received Event character        */
//WORD wReserved1;      /* Fill for no

l_dcb.BaudRate = fl_velocita
l_dcb.ByteSize = 8
l_dcb.Parity = 0
l_dcb.StopBits = 1
l_dcb.fbinary = 1
l_dcb.fParity = 33554432
l_dcb.fOutxCtsFlow = 524416
l_dcb.fOutxDsrFlow = -15527680
l_dcb.fDtrControl = 26

ll_retvalue = SetCommState(ll_openport, l_dcb)

if ll_retvalue=0 then
	fs_errore="Errore sulla porta " + fs_nome_porta
	ll_retvalue = closehandle (ll_openport)
	return -1
end if

ll_retvalue = GetCommState(ll_openport, l_dcb)

if l_dcb.baudrate <> fl_velocita or l_dcb.bytesize <> 8 or l_dcb.parity <>0 or &
	l_dcb.stopbits <> 1 or l_dcb.fbinary <> 1 or l_dcb.fparity <> 33554432 or &
	l_dcb.fOutxCtsFlow <> 524416 or l_dcb.fOutxDsrFlow <> -15527680 or l_dcb.fDtrControl <> 26 then

	fs_errore="Impossibile impostare i parametri sulla porta " + fs_nome_porta
	ll_retvalue = closehandle (ll_openport)
	return -1

end if

ll_retvalue = WriteFile(ll_openport,ls_command,len(ls_command),ll_retbytes,lst_overlapped)

if ll_retvalue = 0 then
	fs_errore = "Errore in fase di scrittura sulla porta " + fs_nome_porta + "."
	ll_retvalue = closehandle (ll_openport)
	return -1
end if

ll_retvalue = closehandle (ll_openport)

return 0
end function

on uo_serial_communication.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_serial_communication.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;// User Object per la comunicazione con le porte seriali
//
// Creato il 24/06/1997 by Diego Ferrari
//
// Copyright (c) 1997 by Consulting & Software
end event

