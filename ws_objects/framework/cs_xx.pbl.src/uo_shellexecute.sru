﻿$PBExportHeader$uo_shellexecute.sru
forward
global type uo_shellexecute from nonvisualobject
end type
type startupinfo from structure within uo_shellexecute
end type
type shellexecuteinfo from structure within uo_shellexecute
end type
type process_information from structure within uo_shellexecute
end type
end forward

type startupinfo from structure
	long		cb
	string		lpreserved
	string		lpdesktop
	string		lptitle
	long		dwx
	long		dwy
	long		dwxsize
	long		dwysize
	long		dwxcountechars
	long		dwycountchars
	long		dwfillattribute
	long		dwflags
	long		wshowwindow
	long		cbreserverd2
	long		lpreserved2
	long		hstdinput
	long		hstdoutput
	long		hstderror
end type

type shellexecuteinfo from structure
	long		cbsize
	long		fmask
	long		hwnd
	string		lpverb
	string		lpfile
	string		lpparameters
	string		lpdirectory
	long		nshow
	long		hinstapp
	long		lpidlist
	string		lpclass
	long		hkeyclass
	long		hicon
	long		hmonitor
	long		hprocess
end type

type process_information from structure
	long		hprocess
	long		hthread
	long		dwprocessid
	long		dwthreadid
end type

global type uo_shellexecute from nonvisualobject
end type
global uo_shellexecute uo_shellexecute

type prototypes
private Function Boolean CloseHandle (long hObject) Library "kernel32.dll"

private Function Boolean TerminateProcess (long hProcess,long uExitCode) Library "kernel32.dll"

Function boolean ShellExecuteEx (Ref shellexecuteinfo lpExecInfo) Library "shell32.dll" Alias For "ShellExecuteExW"

Function long ShellExecute( long hwnd,  string lpOperation, string lpFile, string lpParameters,  string lpDirectory,  integer nShowCmd ) Library "shell32.dll" alias for "ShellExecuteW"


Function long ShellExecuteA( long hwnd,  string lpOperation, string lpFile, string lpParameters,  string lpDirectory,  integer nShowCmd ) Library "shell32.dll" alias for "ShellExecuteA"

Function long WaitForSingleObject (long hHandle, long dwMilliseconds) Library "kernel32.dll"

Function Boolean GetExitCodeProcess (long hProcess, Ref long lpExitCode) Library "kernel32.dll"

private Function Boolean CreateProcess ( &
	String lpApplicationName, &
	String lpCommandLine, &
	long lpProcessAttributes, &
	long lpThreadAttributes, &
	Boolean bInheritHandles, &
	long dwCreationFlags, &
	long lpEnvironment, &
	String lpCurrentDirectory, &
	STARTUPINFO lpStartupInfo, &
	Ref PROCESS_INFORMATION lpProcessInformation &
	) Library "kernel32.dll" Alias For "CreateProcessW"
end prototypes

type variables
/* http://www.topwizprogramming.com/freecode_runandwait.html */

CONSTANT long INFINITE = -1
CONSTANT long WAIT_ABANDONED = 128
CONSTANT long WAIT_COMPLETE = 0
CONSTANT long WAIT_OBJECT_0 = 0
CONSTANT long WAIT_TIMEOUT = 258

CONSTANT long STARTF_USESHOWWINDOW	= 1
CONSTANT long CREATE_NEW_CONSOLE		= 16

CONSTANT long HIGH_PRIORITY_CLASS		= 128
CONSTANT long IDLE_PRIORITY_CLASS		= 64
CONSTANT long NORMAL_PRIORITY_CLASS		= 32
CONSTANT long REALTIME_PRIORITY_CLASS	= 256

private:
	process_information ipf_process

end variables

forward prototypes
public function boolean uof_createprocess (string as_fullpath)
public function long uof_get_processid ()
public subroutine uof_terminateproccess ()
public function long uof_run (string as_filename)
public function long uof_run (string as_filename, long al_millisec_timeout)
private function boolean uof_create_process (string as_filename)
public function boolean uof_run_async (string as_filename)
public function integer uof_run (long al_window_handle, string as_path)
public function integer uof_print (long al_window_handle, string as_path, string as_printername)
end prototypes

public function boolean uof_createprocess (string as_fullpath);/** DEPRECATED **/
string 		 ls_null
boolean		 lb_return
long 			 ll_creation_flags, ll_null
startupinfo	 si

SetNull(ls_null)
SetNull(ll_null)

//						No_Window + Normal Priority
ll_creation_flags = 134217728 + 32
si.cb = 72
si.dwFlags = 1
si.wShowWindow = 0


return CreateProcess(ls_null, as_fullpath, ll_null, ll_null, False, ll_creation_flags, ll_null, ls_null, si, ipf_process)

end function

public function long uof_get_processid ();return ipf_process.dwprocessid
end function

public subroutine uof_terminateproccess ();TerminateProcess(ipf_process.hprocess, -1)


end subroutine

public function long uof_run (string as_filename);return uof_run(as_filename, 0)
end function

public function long uof_run (string as_filename, long al_millisec_timeout);/**
 * stefanop
 * 25/11/2011
 *
 * Eseguo un programma in background e attendo che abbia finito.
 * Il programma viene eseguito in background e viene valorizzato la struttura
 * di istanza ipf_process con tutte le informazioni del processo;
 * Al termine mi viene restituito il valore di ritorno del processo
 *
 * Ritorno:
 *		valore di ritorno del processo
 **/
 
long ll_exitcode

// create process/thread and execute the passed program
If uof_create_process(as_filename) Then
	// wait for the process to complete
	If al_millisec_timeout > 0 Then
		// wait until process ends or timeout period expires
		ll_exitcode = WaitForSingleObject(ipf_process.hProcess, al_millisec_timeout)
		// terminate process if not finished
		If ll_exitcode = WAIT_TIMEOUT Then
			TerminateProcess(ipf_process.hProcess, -1)
			ll_exitcode = WAIT_TIMEOUT
		Else
			// check for exit code
			GetExitCodeProcess(ipf_process.hProcess, ll_exitcode)
		End If
	Else
		// wait until process ends
		WaitForSingleObject(ipf_process.hProcess, INFINITE)
		// check for exit code
		GetExitCodeProcess(ipf_process.hProcess, ll_exitcode)
	End If
	// close process and thread handles
	CloseHandle(ipf_process.hProcess)
	CloseHandle(ipf_process.hThread)
Else
	// return failure
	ll_exitcode = -1
End If

Return ll_exitcode

end function

private function boolean uof_create_process (string as_filename);STARTUPINFO lstr_si
long ll_null, ll_CreationFlags, ll_ExitCode, ll_msecs
String ls_null

// initialize arguments
SetNull(ll_null)
SetNull(ls_null)
lstr_si.cb = 72
lstr_si.dwFlags = STARTF_USESHOWWINDOW
lstr_si.wShowWindow = 0
ll_CreationFlags = CREATE_NEW_CONSOLE + NORMAL_PRIORITY_CLASS

// create process/thread and execute the passed program
return CreateProcess(ls_null, as_filename, ll_null, ll_null, False, ll_CreationFlags, ll_null, ls_null, lstr_si, ipf_process)
end function

public function boolean uof_run_async (string as_filename);/**
 * stefanop
 * 25/11/2011
 *
 * Eseguo un programma in maniera asincrona.
 * Il programma viene eseguito in background e viene valorizzato la struttura
 * di istanza ipf_process con tutte le informazioni del processo
 *
 * Ritorno:
 *		TRUE se il processo è partito
 *		FALSE se il processo da errore
 **/
 

// create process/thread and execute the passed program
return uof_create_process(as_filename)
end function

public function integer uof_run (long al_window_handle, string as_path);// al_window_handle = handle(parent)

//return shellexecuteA(al_window_handle, 'open', as_path, '', '', 1)

integer        li_return_code
string     ls_null
long       ll_null

SetNull(ls_null)
SetNull(ll_null)

li_return_code = ShellExecute(ll_null, "open", as_path, ls_null, ls_null,1)

return li_return_code
end function

public function integer uof_print (long al_window_handle, string as_path, string as_printername);integer   li_return_code
string     ls_null
long       ll_null, ll_pos

SetNull(ls_null)
SetNull(ll_null)

if isnull(as_printername) or len(as_printername) < 1 then
	as_printername = PrintGetPrinter()
end if

//ll_pos = pos(as_printername, "~t")
//as_printername=left(as_printername, ll_pos -1)
as_printername = '"' + g_str.replace(as_printername, "~t", '" "') + '"'

li_return_code = ShellExecute(ll_null, "printto", as_path, as_printername, ls_null,0)

return li_return_code





end function

on uo_shellexecute.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_shellexecute.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

