﻿$PBExportHeader$uo_splitbar.sru
forward
global type uo_splitbar from statictext
end type
end forward

global type uo_splitbar from statictext
integer width = 50
integer height = 1208
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = center!
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
event mousemove pbm_mousemove
event lbuttonup pbm_lbuttonup
event uoe_enddrag ( integer ai_delta_x,  integer ai_delta_y )
end type
global uo_splitbar uo_splitbar

type variables
// Orientamento della barra
public constant integer VERTICAL = 1
public constant integer HORIZONTAL = 2
private string	is_verticalpointer ='SizeWE!'
private string	is_horizontalpointer ='SizeNS!'


// Tipi di aggoncio con gli oggetti
public constant integer LEFT = 1
public constant integer RIGHT = 2
public constant integer ABOVE = 3
public constant integer BELOW = 4

//-- Define the "Extreme points" constants. --
private constant integer LEFTMOST=1
private constant integer RIGHTMOST=2
private constant integer TOPMOST=3
private constant integer BOTTOMMOST=4
private integer ii_minobjectsize = 40

private integer ii_style
private integer ii_barwidth

// Oggetto padre
private window iw_parent
private userobject iuo_parent
private tab itab_parent

private integer ii_maxwidth = -1
private integer ii_maxheight = -1

private integer ii_prevpositionx = -1
private integer ii_prevpositiony = -1
private dragobject idrg_lefttop[]
private dragobject idrg_rightbottom[] 

// Colori barra
private long il_barcolor = 12632256
private long il_barmovecolor = 8421504

end variables

forward prototypes
private function integer uof_mousemove (unsignedlong aul_flags, integer ai_xpos, integer ai_ypos)
public function boolean uof_register (dragobject adrg_object, integer ai_position)
private function integer uof_lbuttonup (unsignedlong aul_flags, integer ai_xpos, integer ai_ypos)
public subroutine uof_set_barcolor (integer ai_color)
public subroutine uof_set_movebarcolor (integer ai_color)
end prototypes

event mousemove;call super::mousemove;

if not KeyDown(keyLeftButton!) then
	return
end if

uof_MouseMove(flags, xpos, ypos)
end event

event lbuttonup;call super::lbuttonup;

uof_lbuttonup(flags, xpos, ypos)
end event

private function integer uof_mousemove (unsignedlong aul_flags, integer ai_xpos, integer ai_ypos);integer li_begin_x, li_begin_y, li_begin_width, li_begin_height
integer li_pointerx, li_pointery, li_minx, li_miny, li_maxx, li_maxy

this.SetPosition(ToTop!)
this.BackColor = il_barmovecolor

li_begin_x = this.x
li_begin_y = this.y
li_begin_width = this.width
li_begin_height = this.height

If ii_prevpositionx < 0 or ii_prevpositiony < 0 Then
	ii_prevpositionx = This.X
	ii_prevpositiony = This.Y
End If

// Get the new position.
If IsValid(iuo_parent) Then	
	li_pointerx = iuo_parent.PointerX()
	li_pointery = iuo_parent.PointerY()	
ElseIf IsValid(itab_parent) Then
	li_pointerx = itab_parent.PointerX()
	li_pointery = itab_parent.PointerY()
ElseIf IsValid(iw_parent) Then
	li_pointerx = iw_parent.PointerX()
	li_pointery = iw_parent.PointerY()
Else
	Return -1
End If
// ---------------


if ii_style = HORIZONTAL then
	this.y = li_pointery
else
	this.x = li_pointerx
end if

return 0
end function

public function boolean uof_register (dragobject adrg_object, integer ai_position);/**
 * Stefano Pulze
 * 21/09/2009
 *
 * La funzione ancora gli oggetti con la splitbar.
 * I parametri passati sono l'oggetto da ancorare e la sua posizione rispetto alla splitbar
 * che può essere LEFT, RIGHT, ABOVE, BELOW
 *
 * @param dragobject oggetto da ancorare
 * @param integer costante relativa alla posizione
 * @return boolean
 */
 
integer li_left_bound, li_right_bound

// Validazioni
if ii_style < 0 or ii_barwidth < 0 then return false
if isnull(adrg_object) or not isvalid(adrg_object) then return false
if isnull(ai_position) or ai_position < 0 or ai_position > 4 then 
	return false
else
	if (ii_style = HORIZONTAL) and (ai_position = LEFT or ai_position = RIGHT) then
		return false
	elseif (ii_style = VERTICAL) and (ai_position = ABOVE or ai_position = BELOW) then
		return false
	end if
end if
// ----------------


// Registro il nuovo oggetto
if ai_position = LEFT or ai_position = ABOVE then
	li_left_bound = UpperBound (idrg_lefttop) + 1
	idrg_lefttop[li_left_bound] = adrg_object
else
	li_right_bound = UpperBound (idrg_rightbottom) + 1
	idrg_rightbottom[li_right_bound] = adrg_object			
end if
// ----------------

return true
end function

private function integer uof_lbuttonup (unsignedlong aul_flags, integer ai_xpos, integer ai_ypos);integer li_pointerx, li_pointery, li_cnt
integer li_deltax, li_deltay

this.BackColor = il_barcolor
this.SetPosition(ToTop!)

// Validazione dei parametri
if ii_style < 0 or ii_barwidth < 0 then return -1
if UpperBound(idrg_lefttop) = 0 or UpperBound(idrg_rightbottom) = 0 then return -1
if ii_prevpositionx < 0 or ii_prevpositiony < 0 then return 0
// -----------------------------

// Ottengo la posizione attuale del mouse
If IsValid(iuo_parent) Then	
	li_pointerx = iuo_parent.PointerX()
	li_pointery = iuo_parent.PointerY()	
ElseIf IsValid(itab_parent) Then
	li_pointerx = itab_parent.PointerX()
	li_pointery = itab_parent.PointerY()
ElseIf IsValid(iw_parent) Then
	li_pointerx = iw_parent.PointerX()
	li_pointery = iw_parent.PointerY()
Else
	Return -1
End If
// -----------------------------

li_deltax = li_pointerx - ii_prevpositionx
li_deltay = li_pointery - ii_prevpositiony

If ii_style = HORIZONTAL Then
	// Ridimensiono gli elementi SOPRA la barra
	For li_cnt = 1 to UpperBound(idrg_lefttop)
		If IsValid(idrg_lefttop[li_cnt]) Then
			idrg_lefttop[li_cnt].Resize(idrg_lefttop[li_cnt].width, idrg_lefttop[li_cnt].height + li_deltay)
		End If
	Next
	
	// Ridimensiono gli elementi SOTTO la barra
	For li_cnt = 1 to UpperBound(idrg_rightbottom)
		If IsValid(idrg_rightbottom[li_cnt]) Then
			idrg_rightbottom[li_cnt].Move (idrg_rightbottom[li_cnt].x, idrg_rightbottom[li_cnt].y + li_deltay)
			idrg_rightbottom[li_cnt].Resize (idrg_rightbottom[li_cnt].width, idrg_rightbottom[li_cnt].height - li_deltay)
		End If
		
	Next	
else
	// Ridimensiono gli elementi a SINISTRA della barra
	For li_cnt = 1 to UpperBound(idrg_lefttop)
		If IsValid(idrg_lefttop[li_cnt]) Then
			idrg_lefttop[li_cnt].Resize(idrg_lefttop[li_cnt].width + li_deltax, idrg_lefttop[li_cnt].height)
		End If
	Next
	
	// Ridimensiono gli elementi a DESTRA della barra
	For li_cnt = 1 to UpperBound(idrg_rightbottom)
		If IsValid(idrg_rightbottom[li_cnt]) Then
			idrg_rightbottom[li_cnt].Move (idrg_rightbottom[li_cnt].x + li_deltax, idrg_rightbottom[li_cnt].y)
			idrg_rightbottom[li_cnt].Resize(idrg_rightbottom[li_cnt].width - li_deltax, idrg_rightbottom[li_cnt].height)
		End If
	Next
	
end if

ii_prevpositionx = -1
ii_prevpositiony = -1

event post uoe_enddrag(li_deltax, li_deltay)
return 0
end function

public subroutine uof_set_barcolor (integer ai_color);/**
 * Stefano Pulze
 * 21/09/2009
 *
 * Imposta il colore della splitbar quando è in stato normale.
 *
 * @param int ai_color numero colore
 */
 
if not isnull(ai_color) and ai_color >= 0 then
	il_barcolor = ai_color
end if
end subroutine

public subroutine uof_set_movebarcolor (integer ai_color);/**
 * Stefano Pulze
 * 21/09/2009
 *
 * Imposta il colore della splitbar quando è in modalità trascinamento.
 *
 * @param int ai_color numero colore
 */
 
if not isnull(ai_color) and ai_color >= 0 then
	il_barmovecolor = ai_color
end if
end subroutine

on uo_splitbar.create
end on

on uo_splitbar.destroy
end on

event constructor;/**
 * Stefano Pulze
 * 21/09/2009
 *
 * uo_splitbar permette di creare un divisiore mobile tra due o più oggetti.
 * Il componente una volta spostato a run-time nella posizione desiderata riposizionerà
 * in maniera automatica i componenti ad esso ancorati.
 */

call super::constructor;

powerobject lpo_parent
SetPosition(ToTop!)

// Ottengo l'oggetto padre
lpo_parent = this.GetParent()
choose case lpo_parent.TypeOf()
	case Window!
		iw_parent = lpo_parent
	case UserObject!
		iuo_parent = lpo_parent
	case Tab!
		itab_parent = lpo_parent
end choose
// ------------------------------

if this.Height >= this.Width then
	ii_style = VERTICAL
	this.Pointer = is_verticalpointer
	ii_barwidth = this.Width	
else
	ii_style = HORIZONTAL
	this.Pointer = is_horizontalpointer
	ii_barwidth = this.Height
end if
end event

