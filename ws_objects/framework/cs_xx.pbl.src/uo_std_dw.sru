﻿$PBExportHeader$uo_std_dw.sru
forward
global type uo_std_dw from datawindow
end type
end forward

global type uo_std_dw from datawindow
integer width = 686
integer height = 400
boolean livescroll = true
borderstyle borderstyle = stylelowered!
event ue_aggiorna_text ( )
event ue_setup_dw ( )
event ue_proteggi_campi_insert ( )
event ue_anteprima ( )
event type long ue_anteprima_pdf ( )
end type
global uo_std_dw uo_std_dw

type variables
public: 
	boolean ib_dw_report = false
	
	// indica che la DW si colori in base al tema
	boolean ib_colora = true
	
	// stefanop: 18/06/2014: il messaggio della mail in caso di "Invia Pdf" dal menu
	string is_mail_message 
	string	is_mail_destinatario
	
end variables

forward prototypes
public subroutine uof_aggiorna_text ()
public function string uof_inserisci_valore_insert (string as_sql, string as_colonna_db, string as_valore)
public subroutine uof_setup_dw ()
public subroutine uof_proteggi_campi_insert ()
public subroutine uof_aggiorna_text (string as_cod_lingua)
end prototypes

event ue_aggiorna_text();uof_aggiorna_text()
end event

event ue_setup_dw();uof_setup_dw()
end event

event ue_proteggi_campi_insert();uof_proteggi_campi_insert()
end event

event ue_anteprima();setpointer(hourglass!)

s_cs_xx.parametri.parametro_dw_1 = this
s_cs_xx.parametri.parametro_s_1 = pcca.window_current.title

open(w_anteprima)
end event

event type long ue_anteprima_pdf();string ls_messaggio, ls_path, ls_to[], ls_subject, ls_message, ls_error
long ll_return, ll_elemento
boolean lb_invio_con_outlook
uo_outlook luo_outlook

ll_elemento = 0

setpointer(hourglass!)

s_cs_xx.parametri.parametro_dw_1 = this
s_cs_xx.parametri.parametro_s_1 = pcca.window_current.title

uo_archivia_pdf luo_pdf
luo_pdf = create uo_archivia_pdf

ls_path = luo_pdf.uof_crea_pdf_path(this)

if ls_path = "errore" then
	g_mb.messagebox ("Errore","Errore nella creazione del file pdf.")
	return -1
end if

try
	luo_outlook = create uo_outlook
	
	// verifico se era stato impostato un destinatario
	if not isnull(is_mail_destinatario) and len(is_mail_destinatario) > 0 then
		ls_to[1] = is_mail_destinatario
	end if
	
	// Il nome dell'oggetto lo recupero dal nome della DW
	ls_subject = Object.DataWindow.Print.DocumentName
	ls_message = is_mail_message
	
	// Messaggio forzato o preso dall'oggetto
	if isnull(ls_message) or ls_message = "" then
		ls_message = ls_subject
	end if
	
	guo_functions.uof_get_parametro_azienda("IPO", lb_invio_con_outlook)
	
	if lb_invio_con_outlook then
		// Invio con outlook
		if luo_outlook.uof_apri_outlook(ls_to, ls_subject, ls_message, {ls_path}, ls_error) < 0 then
			g_mb.error(ls_error)
		end if
	else
		// Invio con il send mail
		luo_outlook.ib_force_open = true
		
		if luo_outlook.uof_invio_sendmail(ls_to[], ls_subject, ls_message, {ls_path}, ls_error) then
			ll_return = 1
			g_mb.success("Email inviata con successo")
		else
			ll_return = -1
			g_mb.warning(ls_error)
		end if
	end if
	
catch(RuntimeError e)
	g_mb.error("Errore durante l'invio della mail.")
finally
	destroy luo_outlook
end try

filedelete(ls_path)

end event

public subroutine uof_aggiorna_text ();if s_cs_xx.cod_lingua_applicazione = "" then setnull(s_cs_xx.cod_lingua_applicazione)
if s_cs_xx.cod_lingua_window 		 = "" then setnull(s_cs_xx.cod_lingua_window)

if not isnull(s_cs_xx.cod_lingua_window) then
	uof_aggiorna_text(s_cs_xx.cod_lingua_window)
else
	uof_aggiorna_text(s_cs_xx.cod_lingua_applicazione)
end if	

return
end subroutine

public function string uof_inserisci_valore_insert (string as_sql, string as_colonna_db, string as_valore);string ls_str, ls_insert_colonne, ls_insert_values, ls_colonne[], ls_values[], ls_sql_inizio, ls_sql
long ll_pos, ll_pos_inizio_col, ll_pos_fine_col, ll_pos_inizio_val, ll_pos_fine_val, ll_i, ll_cont

ll_pos = pos(as_sql, "VALUES")
if ll_pos < 1 then return ""

ls_insert_colonne = left(as_sql, ll_pos - 1)
ls_insert_values = mid(as_sql, ll_pos)

ll_pos = pos(ls_insert_colonne, "(")
ls_sql_inizio = left(ls_insert_colonne, ll_pos)
ll_pos_inizio_col = ll_pos + 1

ll_pos = pos(ls_insert_values, "(")
ll_pos_inizio_val = ll_pos + 1

ll_i = 0
do while true
	ll_pos_fine_col = pos(ls_insert_colonne, ",",ll_pos_inizio_col)
	if ll_pos_fine_col < 1 then 
		// controllo ultima colonna
		ll_pos_fine_col = pos(ls_insert_colonne, ")",ll_pos_inizio_col)
		if ll_pos_fine_col < 1 then 
			exit
		end if
	end if
	
	ll_i ++
	
	ls_str = mid(ls_insert_colonne, ll_pos_inizio_col, ll_pos_fine_col - ll_pos_inizio_col)
	ls_colonne[ll_i] = ls_str
	ll_pos_inizio_col = ll_pos_fine_col + 1
	
	ll_pos_fine_val = pos(ls_insert_values, ",",ll_pos_inizio_val)
	
	if ll_pos_fine_val < 1 then 
		// controllo ultima colonna
		ll_pos_fine_val = pos(ls_insert_values, ")",ll_pos_inizio_val)
		if ll_pos_fine_val < 1 then 
			exit
		end if
	end if

	ls_str = mid(ls_insert_values, ll_pos_inizio_val, ll_pos_fine_val - ll_pos_inizio_val)
	ls_values[ll_i] = ls_str
	ll_pos_inizio_val = ll_pos_fine_val + 1
loop

// ripulisco il nome della colonna
if pos(as_colonna_db,".") > 0 then
	as_colonna_db = mid(as_colonna_db, pos(as_colonna_db,".") + 1)
end if

do while pos(as_colonna_db,char(34)) > 0
	as_colonna_db = replace(as_colonna_db,pos(as_colonna_db,char(34)),1,"")
loop

// aggiungo il valore desiderato
ll_cont = upperbound(ls_colonne)

for ll_i = 1 to ll_cont
	if pos(ls_colonne[ll_i], as_colonna_db) > 0 then
		ls_values[ll_i] = as_valore
	elseif ll_i = ll_cont then
		// vuol dire che sono arrivato alla fine e 
		// non lo ho trovato e quindi lo aggiungo d'ufficio
		ls_colonne[ll_i+1] = as_colonna_db
		ls_values[ll_i +1] = as_valore
	end if
next

// ricompongo la sintassi INSERT
ls_sql = ls_sql_inizio
ll_cont = upperbound(ls_colonne)
for ll_i = 1 to ll_cont
	if ll_i > 1 then ls_sql += ", "
	ls_sql += ls_colonne[ll_i]
next

ls_sql += " ) VALUES ("

ll_cont = upperbound(ls_values)
for ll_i = 1 to ll_cont
	if ll_i > 1 then ls_sql += ", "
	ls_sql += ls_values[ll_i]
next

ls_sql += " ) "

return ls_sql
end function

public subroutine uof_setup_dw ();/**
 * Stefanop
 * 20/09/2010
 *
 * Imposto le proprietà dei colori della DW
 **/
 
string ls_column_name, ls_flag_importante, ls_flag_obbligatorio, ls_modifyfont
int li_num_columns, li_i

// Se la DW è di tipo report non devo modificare nulla, quindi esco
if ib_dw_report or not ib_colora then return 

li_num_columns = Integer(Describe("DataWindow.Column.Count"))

this.object.datawindow.color = s_themes.theme[s_themes.current_theme].dw_backcolor

for li_i = 1 to li_num_columns
		
	ls_column_name = describe("#" + string(li_i) + ".name")
	
	setnull(ls_flag_importante)
	setnull(ls_flag_obbligatorio)
		
	select flag_importante, flag_obbligatorio
	into   :ls_flag_importante, :ls_flag_obbligatorio
	from   tab_dw_text_repository
	where 
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_lingua is null and
		nome_dw = :dataobject and
		nome_oggetto = :ls_column_name;
				 
	if ls_flag_obbligatorio = "S" then
		//i_ColBColors[ll_i] = s_cs_xx.colore_colonna_obbligatorio
		modify("#" + string(li_i) + ".Background.Color='" + string(s_cs_xx.colore_colonna_obbligatorio) + "'")
	else
	
		if ls_flag_importante = "S" then
			//i_ColBColors[ll_i] = s_cs_xx.colore_colonna_importante
			modify("#" + string(li_i) + ".Background.Color='" + string(s_cs_xx.colore_colonna_importante) + "'")
		else
			//i_ColBColors[ll_i] = s_themes.theme[s_themes.current_theme].dw_column_backcolor
			modify("#" + string(li_i) + ".Background.Color='" + string(s_themes.theme[s_themes.current_theme].dw_column_backcolor) + "'")
		end if
	
	end if
		
	// colore del testo
	//if i_TabOrder[ll_i] >= 0 then
		//i_ColColors[ll_i] 	= s_themes.theme[s_themes.current_theme].dw_column_textcolor
		ls_modifyfont = "#" + string(li_i) + ".font.textcolor='" + s_themes.theme[s_themes.current_theme].dw_column_font +"'"
		ls_modifyfont += "~t#" + string(li_i) + ".font.face='" + s_themes.theme[s_themes.current_theme].dw_column_font +"'"
		ls_modifyfont += "~t#" + string(li_i) + ".font.height='" + s_themes.theme[s_themes.current_theme].dw_column_fontsize +"'"
		ls_modifyfont += "~t#" + string(li_i) + ".font.weight='" + s_themes.theme[s_themes.current_theme].dw_column_fontbold +"'"
		ls_modifyfont += "~t#" + string(li_i) + ".font.italic='" + s_themes.theme[s_themes.current_theme].dw_column_fontitalic +"'"
	//end if
	
		modify(ls_modifyfont)
next
end subroutine

public subroutine uof_proteggi_campi_insert ();string ls_colonna_dw, ls_cod_utente, ls_valore_insert, ls_nome_dw, ls_nome_parent_dw, ls_modify, &
		 ls_flag_tipo_valore, ls_flag_visualizza_subito, ls_string
decimal ld_decimal
datetime ldt_datetime

if s_cs_xx.admin then return 

ls_nome_dw = dataobject

//if isvalid(i_parentdw) then
//	ls_nome_parent_dw = i_parentdw.dataobject
//else
//	ls_nome_parent_dw = ls_nome_dw
//end if

DECLARE cu_dw_filtri CURSOR FOR  
	SELECT
		colonna_dw,   
		cod_utente,   
		valore_insert,
		flag_tipo_valore,
		flag_visualizza_subito
	FROM tab_dw_filtri  
	WHERE
		cod_azienda = :s_cs_xx.cod_azienda AND  
		nome_dw = :ls_nome_dw AND  
		cod_utente = :s_cs_xx.cod_utente and
		flag_ambiente = 'C';

open cu_dw_filtri;

ls_modify = ""

do while true
	fetch cu_dw_filtri into :ls_colonna_dw, :ls_cod_utente, :ls_valore_insert, :ls_flag_tipo_valore, :ls_flag_visualizza_subito;
	if sqlca.sqlcode <> 0 then exit
	
	ls_modify = ls_modify + ls_colonna_dw + ".protect=1" + "'~t"
	
	// stefanop: 10/11/2011 se la DW è stat modificata da errore
	try
	
		if ls_flag_visualizza_subito = "S" and not isnull(ls_flag_visualizza_subito) then
			choose case ls_flag_tipo_valore
				case "S"  // stringa
					setitem(getrow(),ls_colonna_dw, ls_valore_insert)
				case "N" 	// numero
					ld_decimal = dec(ls_valore_insert)
					setitem(getrow(),ls_colonna_dw, ld_decimal)
				case "D"		// datetime
					ldt_datetime = datetime(ls_valore_insert)
					setitem(getrow(),ls_colonna_dw, ldt_datetime)
			end choose
		end if
		
	catch(RuntimeError ex)
		f_log_sistema(ex.GetMessage(), "DW")
	end try
	
loop

close cu_dw_filtri;

if len(ls_modify) > 1 then this.modify(ls_modify)
end subroutine

public subroutine uof_aggiorna_text (string as_cod_lingua);string ls_modify, ls_nome_dw, ls_nome_oggetto, ls_testo, ls_sql, ls_tooltip

ls_nome_dw = dataobject

declare cu_dtext dynamic cursor for SQLSA;
ls_sql = 	" select nome_oggetto, trim(testo), tooltip from tab_dw_text_repository " + &
         	" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and" + &
			" flag_importante ='N' and flag_obbligatorio = 'N' and " + &
         	" nome_dw = '" + ls_nome_dw + "' and " 

if isnull(s_cs_xx.cod_lingua_window) then
	if isnull(s_cs_xx.cod_lingua_applicazione) then
		ls_sql += " cod_lingua is null "
	else
		ls_sql += " cod_lingua = '" + s_cs_xx.cod_lingua_applicazione + "' "
	end if
else
	ls_sql += " cod_lingua = '" + s_cs_xx.cod_lingua_window + "' "
end if

PREPARE SQLSA FROM :ls_sql ;

open dynamic cu_dtext;

do while true
	fetch cu_dtext into :ls_nome_oggetto, :ls_testo, :ls_tooltip;
	if sqlca.sqlcode <> 0 then exit
	
	// stefanop 27/07/2011: imposto il valore Zoom
	if ls_nome_oggetto = "Zoom" then
		ls_modify += "DataWindow.Zoom=" + ls_testo + "'~t"
		continue
	end if
	// ----
	
	if not isnull(ls_testo) and len(ls_testo) > 0 then  
		ls_modify += ls_nome_oggetto + ".text='"+ ls_testo + "'~t"
	end if
	
	if not isnull(ls_tooltip) and len(ls_tooltip) > 0 then  
		ls_modify += ls_nome_oggetto + ".tooltip.enabled='1'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.tip='"+ ls_tooltip + "'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.title='Help'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.icon='1'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.isbubble='" + string(s_themes.theme[1].tooltip_isbubble) + "'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.textcolor='" + string(s_themes.theme[1].tooltip_textcolor) + "'~t"
		ls_modify += ls_nome_oggetto + ".tooltip.backcolor='" + string(s_themes.theme[1].tooltip_backcolor) + "'~t"
	end if
	
loop

modify(ls_modify)

close cu_dtext;

return
end subroutine

on uo_std_dw.create
end on

on uo_std_dw.destroy
end on

event constructor;//stefnaop 20/09/2010:
postevent("ue_setup_dw")

postevent("ue_aggiorna_text")

postevent("ue_proteggi_campi_insert")
end event

event sqlpreview;// ---------------------  Enrico 27/4/2004 ----------------------------------
//   Nuova gestione filtri automatici
boolean lb_variazioni=false
string ls_nome_dw, ls_sql, ls_flag_tipo_valore, ls_sql_select, ls_sql_where, ls_valore, ls_sql_order,ls_filter
long ll_row, ll_i, ll_pos, ll_ret
datastore lds_filtri

if sqltype = PreviewDelete! then return

ls_sql = ""
lds_filtri = CREATE datastore
lds_filtri.dataobject = 'd_ds_tab_dw_filtri'
lds_filtri.settransobject(sqlca)
ls_nome_dw = this.dataobject
ll_row = lds_filtri.retrieve(s_cs_xx.cod_azienda, ls_nome_dw)
if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_filter = "cod_utente = '" + s_cs_xx.cod_utente + "' or isnull(cod_utente)"
	lds_filtri.setfilter(ls_filter)
	lds_filtri.filter()
	ll_row = lds_filtri.rowcount()
end if
if ll_row > 0 then
	
	choose case sqltype
		case PreviewInsert! 			// è il caso la sintassi SQL della DW è  INSERT
			
			// modificato da EnMe 6/2/08: se visualizza subito allora non impongo in insert, perchè 
			// è già stato imposto a livello di DW
			ls_filter=""
			if s_cs_xx.cod_utente <> "CS_SYSTEM" then
				ls_filter = "(cod_utente = '" + s_cs_xx.cod_utente + "' or isnull(cod_utente)) and "
			end if
			ls_filter = ls_filter + " flag_visualizza_subito = 'N' "
			lds_filtri.setfilter(ls_filter)
			lds_filtri.filter()
			ll_row = lds_filtri.rowcount()
			
			ls_sql = sqlsyntax
			
			for ll_i = 1 to ll_row
				
				ls_flag_tipo_valore = lds_filtri.getitemstring(ll_i, "flag_tipo_valore") 
				
				choose case ls_flag_tipo_valore
					case "S"		// stringa
						ls_valore = "'" + lds_filtri.getitemstring(ll_i, "valore_select") + "'"
						if isnull(ls_valore) then ls_valore = ""
						
					case "N"		// numero
						ls_valore = lds_filtri.getitemstring(ll_i, "valore_select")
						if isnull(ls_valore) then ls_valore = ""
						
					case "D"		// data
						ls_valore = "'" + lds_filtri.getitemstring(ll_i, "valore_select") + "'"
						if isnull(ls_valore) then ls_valore = ""
				end choose
				
				ls_sql = uof_inserisci_valore_insert(ls_sql, lds_filtri.getitemstring(ll_i, "colonna_db"), ls_valore)

			next
			
			ll_ret = setsqlpreview(ls_sql)
			
			if ll_ret < 1 then
				g_mb.messagebox("FrameWork", "Errore nella impostazione filtri automatici (UO_CS_XX_DW.SQLPREVIEW)")
			end if
			
			
		case PreviewUpdate!	
			// inquesto caso non faccio nulla
			
		case PreviewDelete!	
			
			// in questo caso non devo fare niente perchè vado a cancellare record che ho già visualizzato nella DW
			
		case PreviewSelect!	
			
			if s_cs_xx.cod_utente <> "CS_SYSTEM" then
				
				for ll_i = 1 to ll_row
					if len(ls_sql) > 0 then
						ls_sql += " and "
					end if
					
					ls_sql += lds_filtri.getitemstring(ll_i, "colonna_db")
					ls_sql += " "
					ls_sql += lds_filtri.getitemstring(ll_i, "operatore") 
					ls_sql += " "
					ls_flag_tipo_valore = lds_filtri.getitemstring(ll_i, "flag_tipo_valore") 
					choose case ls_flag_tipo_valore
						case "S"		// stringa
							ls_sql += "'" + lds_filtri.getitemstring(ll_i, "valore_select") + "'"
							
						case "N"		// numero
							ls_sql += lds_filtri.getitemstring(ll_i, "valore_select")
							
						case "D"		// data
							ls_sql += lds_filtri.getitemstring(ll_i, "valore_select")
							
					end choose
				next
			
				// modifica della sintassi SQL a seconda dell'azione da compiere
				if ls_sql <> "" then
					
					ll_pos = pos(upper(sqlsyntax), "WHERE")
					
					if ll_pos > 0 then
						
						ls_sql_select = left(sqlsyntax, ll_pos - 1)
						ls_sql_where = mid(sqlsyntax, ll_pos)
						
						ls_sql_where = " where ( " + ls_sql + ") and " + mid(ls_sql_where, 6)
						
					else
						// non c'è la WHERE controllo se c'è un ORDER BY					
						ll_pos = pos(upper(sqlsyntax), "ORDER BY")
	
						if ll_pos > 0 then
							
							ls_sql_select = left(sqlsyntax, ll_pos - 1)
							ls_sql_order = mid(sqlsyntax, ll_pos)
							
							ls_sql_where = " where ( " + ls_sql + ") " + ls_sql_order
						
						else		// ne where ne order by			
						
							ls_sql_select = sqlsyntax
							ls_sql_where = " where ( " + ls_sql + ") "
							
						end if
					end if
					
					ls_sql = ls_sql_select + ls_sql_where
					
					ll_ret = setsqlpreview(ls_sql)
					if ll_ret < 1 then
						g_mb.messagebox("FrameWork", "Errore nella impostazione filtri automatici (UO_CS_XX_DW.SQLPREVIEW)")
					end if
					
				end if
				
			end if
			
	end choose
end if

destroy lds_filtri

end event

event doubleclicked;// stefanop 27/07/2011: al doppio click, se dw_report, apro l'anteprima di stampa
if ib_dw_report then
	event ue_anteprima()
end if
// ----
end event

