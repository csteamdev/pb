﻿$PBExportHeader$uo_str.sru
forward
global type uo_str from nonvisualobject
end type
end forward

global type uo_str from nonvisualobject
end type
global uo_str uo_str

type variables
//
end variables

forward prototypes
public function string format (string as_testo, any aa_array[])
public function string format (string as_testo, any aa_1)
public function string format (string as_testo, any aa_1, any aa_2)
public function string format (string as_testo, any aa_1, any aa_2, any aa_3)
public function string format (string as_testo, any aa_1, any aa_2, any aa_3, any aa_4)
public function string format (string as_testo, any aa_1, any aa_2, any aa_3, any aa_4, any aa_5)
public function string format (string as_testo, any aa_1, any aa_2, any aa_3, any aa_4, any aa_5, any aa_6)
public function string replace (string as_stringa, string as_to_find, string as_to_replace)
public function string implode (string as_array[], string as_delimiter, boolean ab_exclude_empty)
public function string implode (string as_array[], string as_delimiter)
public function integer explode (string as_string, string as_delimiter, ref string as_array[])
public function string limit (string as_string, integer ai_limit, string as_ending_with)
public function string limit (string as_string, integer ai_limit)
public function string trim (string as_string, string as_char)
private function string uof_trim (string as_string, string as_char, string as_method)
public function string ltrim (string as_string, string as_char)
public function string rtrim (string as_string, string as_char)
public function boolean contains (string as_string, string as_find)
public function boolean start_with (string as_string, string as_start_string)
public function boolean end_with (string as_string, string as_end_string)
public subroutine bubblesort (ref string as_vettore[])
public function integer split (string as_value, string as_pattern, ref string as_array[])
public function boolean isempty (string as_value)
public function boolean isnotempty (string as_value)
public function string append (any aa_array[])
public function string tostring (runtimeerror ae_exception)
public function string quote (string as_string)
public function string safe (any aa_value)
public function uo_string_builder append (string as_value)
public function boolean contains (string as_string, string as_find, boolean ab_ignorecase)
public function string printf (string as_template, any aa_values[])
public function string printf (string as_template, datastore ads_store, integer ai_row)
public function string replaceall (string as_stringa, string as_to_find, string as_to_replace, boolean ab_replace_all)
public function string sanatize_for_sql (string as_value)
end prototypes

public function string format (string as_testo, any aa_array[]);/*
Funzione che estrae i dati dal vettore all'indice trovato all'interno della stringa di testo (Parametrio as_testo)
*/

String ls_dato
int li_i
long ll_pos,ll_lunghezza

for li_i=1 to upperbound(aa_array)	//Contiunuo fino a quando ci sono elementi all'interno dell'array
	
	// TODO: in futuro si potrebbe aggiungere il formato per i numeri e date
	
	as_testo = replace(as_testo, "$" + string(li_i), string(aa_array[li_i]))
	
next

return as_testo
end function

public function string format (string as_testo, any aa_1);/*
CASO 1: Funzione con 1 parametro
*/

return format(as_testo,{aa_1})


	
end function

public function string format (string as_testo, any aa_1, any aa_2);/*
CASO 2: Funzione con 2 parametri
*/

Any la_v[]

//Inserisco sul vettore i parametri passati
la_v[1]=aa_1
la_v[2]=aa_2

return format(as_testo,la_v[])
end function

public function string format (string as_testo, any aa_1, any aa_2, any aa_3);/*
CASO 3: Funzione con tre parametri 			
*/

Any la_v[]

//Inserisco sul vettore i parametri passati

la_v[1]=aa_1
la_v[2]=aa_2
la_v[3]=aa_3

return format(as_testo,la_v[])
end function

public function string format (string as_testo, any aa_1, any aa_2, any aa_3, any aa_4);/*
CASO 4: Funzione con 4 parametri 			
*/

Any la_v[]

//Inserisco sul vettore i parametri passati

la_v[1]=aa_1
la_v[2]=aa_2
la_v[3]=aa_3
la_v[4]=aa_4

return format(as_testo,la_v[])
end function

public function string format (string as_testo, any aa_1, any aa_2, any aa_3, any aa_4, any aa_5);/*
CASO 5: Funzione con 5 parametri 			
*/

Any la_v[]

//Inserisco sul vettore i parametri passati

la_v[1]=aa_1
la_v[2]=aa_2
la_v[3]=aa_3
la_v[4]=aa_4
la_v[5]=aa_5

return format(as_testo,la_v[])
end function

public function string format (string as_testo, any aa_1, any aa_2, any aa_3, any aa_4, any aa_5, any aa_6);/*
CASO 6 : Funzione con 6 parametri 			
*/

Any la_v[]

//Inserisco sul vettore i parametri passati

la_v[1]=aa_1
la_v[2]=aa_2
la_v[3]=aa_3
la_v[4]=aa_4
la_v[5]=aa_5
la_v[6]=aa_6

return format(as_testo,la_v[])
end function

public function string replace (string as_stringa, string as_to_find, string as_to_replace);return replaceAll(as_stringa, as_to_find, as_to_replace, true)
end function

public function string implode (string as_array[], string as_delimiter, boolean ab_exclude_empty);/**
 * stefanop
 * 18/10/2011
 *
 * Unisce un array di stringe separandole da un delimitatore
 *
 * Parametri:
 *		as_array[] = array delle stringhe da unire
 *		as_delimiter = delimitatore per l'unione
 *		ab_exclude_empty = se TRUE non prende in considerazione le stringhe vuote
 **/

string ls_result
long ll_i, ll_count

ll_count = upperbound(as_array)

// se maggiore di due allora necessita del delimitatore
if ll_count > 1 then
	
	for ll_i = 1 to ll_count -1
		if isnull(as_array[ll_i]) or as_array[ll_i] = "" then
			if ab_exclude_empty then
				continue
			else
				as_array[ll_i] = ""
			end if
		end if
		
		ls_result += as_array[ll_i] + as_delimiter
	next
	
	if isnull(as_array[ll_i]) or as_array[ll_i] = "" then as_array[ll_count] = ""

	if ab_exclude_empty and len(as_array[ll_count]) < 1 then
	else
		ls_result += as_array[ll_count]
	end if
	
	return ls_result

// se un elemento solo allora non serve il delimitatore e nemmeno il ciclo
elseif ll_count = 1 then
	
	if ab_exclude_empty and (isnull(as_array[1]) or as_array[1] = "") then
		return ""
	else
		return as_array[1]
	end if
	
// se nessun elemento allora non serve niente
elseif ll_count < 1 then
	return ""
end if
end function

public function string implode (string as_array[], string as_delimiter);return implode(as_array, as_delimiter, true)
end function

public function integer explode (string as_string, string as_delimiter, ref string as_array[]);/**
 * stefanop
 * 18/10/2011
 *
 * Divide una stringa in base al separatore passato
 **/

int li_count
long ll_pos = 1, ll_delimiter_length

// la stringa deve esistere
if isnull(as_string) or len(as_string) < 1 then return 0

li_count = 0
ll_delimiter_length = len(as_delimiter)

do while true
	
	ll_pos = pos(as_string, as_delimiter, 1)
	if ll_pos = 0 then
		// appendo anche l'ultima occorrenza se valida prima di uscire
		li_count++
		if not isnull(as_string) then as_array[li_count] = as_string
		exit
	end if
	
	li_count++
	as_array[li_count] = left(as_string, ll_pos - 1)
	as_string = mid(as_string, ll_pos + ll_delimiter_length)
	
loop

return li_count
end function

public function string limit (string as_string, integer ai_limit, string as_ending_with);/**
 * stefanop
 * 03/08/2012
 *
 * Tronca una stringa se supera il numero massimo di caratteri e se passatto il separatore
 * lo mette alla fine della stringa
 **/
 
if isnull(as_string) or as_string = "" then
	// Stringa nulla o vuota
	return ""
end if

as_string = left(as_string, ai_limit)

if not isnull(as_ending_with) and as_ending_with <> "" then
	
	as_string = left(as_string, len(as_string) - len(as_ending_with)) + as_ending_with
	
end if
	
return as_string


end function

public function string limit (string as_string, integer ai_limit);/**
 * stefanop
 * 03/08/2012
 *
 * Tronca una stringa se supera il numero massimo di caratteri e se passatto il separatore
 * lo mette alla fine della stringa
 **/
 
return limit(as_string, ai_limit, "...")
end function

public function string trim (string as_string, string as_char);/**
 * stefanop
 * 03/08/2012
 *
 * Taglia nella stringa il delimitatore passato
 **/
 
return uof_trim(as_string, as_char, "A")
end function

private function string uof_trim (string as_string, string as_char, string as_method);/**
 * stefanop
 * 03/08/2012
 *
 * Taglia la stringa e ne elimina i caratteri inizio e fine se corrisponde al delimitatore
 * Ad esempio:
 * g_str.trim("la smell", "l")  => "a smel"
 * g_str.ltrim("la smell", "l") => "a smell"
 * g_str.rtrim("la smell", "l") => "la smel"
 * g_str.trim("la smell", "c") => "la smell"
 **/
 
as_string = trim(as_string, true)

// Controllo a sinistra
if as_method = "L" or as_method = "A" then
	if left(as_string, 1) = as_char then
		as_string = mid(as_string, 2)
	end if
end if

// Controllo a destra
if as_method = "R" or as_method = "A" then
	if right(as_string, 1) = as_char then
		as_string = mid(as_string, 1, len(as_string) - 1)
	end if
end if
 
return as_string
end function

public function string ltrim (string as_string, string as_char);/**
 * stefanop
 * 03/08/2012
 *
 * Taglia nella stringa il delimitatore passato
 **/
 
return uof_trim(as_string, as_char, "L")
end function

public function string rtrim (string as_string, string as_char);/**
 * stefanop
 * 03/08/2012
 *
 * Taglia nella stringa il delimitatore passato
 **/
 
return uof_trim(as_string, as_char, "R")
end function

public function boolean contains (string as_string, string as_find);/**
 * stefanop
 * 08/11/2012
 *
 * Controlla se nella stringa è presente la stringa da cercare
 **/
 
return contains(as_string, as_find, false)
end function

public function boolean start_with (string as_string, string as_start_string);/**
 * stefanop
 * 09/11/2012
 *
 * Controlla se la stringa inzia con una determinata sequenza di caratteri
 **/
  
if left(as_string,  len(as_start_string)) = as_start_string then
	return true
else
	return false
end if
end function

public function boolean end_with (string as_string, string as_end_string);/**
 * stefanop
 * 09/11/2012
 *
 * Controlla se la stringa termina con una determinata sequenza di caratteri
 **/
  
if right(as_string,  len(as_end_string)) = as_end_string then
	return true
else
	return false
end if
end function

public subroutine bubblesort (ref string as_vettore[]);/*
 * F. Baschirotto - 15/01/2013
 * BubbleSort su vettore di stringhe. Alla fine il vettore è ordinato in ordine alfabetico.
 * La funzione è Case Insensitive.
*/

Integer li_i, li_j
String ls_provvisorio

li_i=UpperBound(as_vettore)

Do While(li_i>0)
	for li_j=1 To li_i - 1
		if(Upper(as_vettore[li_j])>Upper(as_vettore[li_j+1])) Then
			ls_provvisorio=as_vettore[li_j]
			as_vettore[li_j]=as_vettore[li_j+1]
			as_vettore[li_j+1]=ls_provvisorio
		end if
	Next
	li_i=li_i - 1
Loop
end subroutine

public function integer split (string as_value, string as_pattern, ref string as_array[]);/**
 * stefanop
 * 16/05/2014
 *
 * La funzione divide la stringa in base ad un pattern regex
 * Inoltre viene ritornato il numero di elementi trovati
 **/
 
string ls_error, ls_empty[]
int li_count
uo_espress_reg luo_espress_reg

as_array = ls_empty

// Pre check
if isnull(as_value) or as_value = "" then	
	return 0
end if

luo_espress_reg = create uo_espress_reg

if luo_espress_reg.set_pattern(as_pattern, ls_error) < 0 then
	destroy luo_espress_reg
	return -1
end if

li_count = luo_espress_reg.cerca_array( as_value, ref as_array[])
destroy luo_espress_reg

return li_count
end function

public function boolean isempty (string as_value);/**
 * stefanop
 * 03/07/2014
 *
 * Ritorna true le la stringa è NULL oppure ""
 **/
 
return isnull(as_value) or as_value = "" or len(as_value) = 0
end function

public function boolean isnotempty (string as_value);/**
 * stefanop
 * 03/07/2014
 *
 * Ritorna true solo le la stringa ha almeno un valore
 **/
 
return not isempty(as_value)
end function

public function string append (any aa_array[]);/**
 * stefanop
 * 03/07/2014
 *
 * Consente di unire un array in stringa
 **/
 
string ls_result
uint li_upperbound, li_i

if isnull(aa_array) then return ""

li_upperbound = upperbound(aa_array)

if li_upperbound > 1 then
	ls_result = ""
	
	for li_i = 1 to li_upperbound
		if isnotempty(aa_array[li_i]) then 
			ls_result += string(aa_array[li_i])
		end if
	next
	
	return ls_result
	
elseif li_upperbound = 1 then
	if isempty(aa_array[1]) then return ""
	return string(aa_array[1])
else
	return ""
end if
end function

public function string tostring (runtimeerror ae_exception);/**
 * stefanop
 * 04/07/2014
 *
 * Stampo l'eccezione
 **/
 
if isvalid(ae_exception) and not isnull(ae_exception) then
	if g_str.isnotempty(ae_exception.getmessage()) then
		return ae_exception.getmessage()
	else
		return ""
	end if
	
	// TODO controllare
//	return "Error Number: " + string( ae_exception.number ) + "~r~n" +&
//      "~tObject: " + string( ae_exception.class ) + "~r~n" +&
//      "~tFunction: " + string( ae_exception.routinename ) + "~r~n" +&
//      "~tLine: " + string( ae_exception.line ) + "~r~n" +&
//      "~tDescription: " + string( ae_exception.text )
else
	return ""
end if

end function

public function string quote (string as_string);/**
 * stefanop
 * 01/09/2014
 *
 * Quota il carattere ' con ~'
 **/
 
 
return replace(as_string, "'", "~~'")
end function

public function string safe (any aa_value);/**
 * stefanop
 * 22/10/2012
 *
 * Ritorna il valore della string se validata oppure stringa vuota
 * Da usare quando si usa l'operatore + con le stringhe.
 **/
 
if isnull(aa_value) then
	return ""
end if

return string(aa_value)
end function

public function uo_string_builder append (string as_value);uo_string_builder luo_sb
luo_sb = create uo_string_builder

return luo_sb.append(as_value)
end function

public function boolean contains (string as_string, string as_find, boolean ab_ignorecase);/**
 * stefanop
 * 03/06/2015
 *
 * Controlla se nella stringa è presente la stringa da cercare
 **/
 
if ab_ignorecase then
	as_string = upper(as_string)
	as_find = upper(as_find)
end if
 
return pos(as_string, as_find) > 0
end function

public function string printf (string as_template, any aa_values[]);/**
 * stefanop
 * 21/03/2016
 *
 * Prova a simulare il printf degli altri linguaggi.
 * TODO aggiungere descrizione
 **/
 
return as_template
end function

public function string printf (string as_template, datastore ads_store, integer ai_row);/**
 * stefanop
 * 21/03/2016
 *
 * Prova a simulare il printf degli altri linguaggi.
 * TODO aggiungere descrizione
 **/



 
return as_template
end function

public function string replaceall (string as_stringa, string as_to_find, string as_to_replace, boolean ab_replace_all);/**
 * Funzione di sostituzione
 * Rimpiazza tutte le occorenze di fs_da_sostituire in fs_stringa con fs_sostitutore
 **/

integer li_lunghezza_formula,li_t,li_posizione,li, li_replace, li_tofind

if isnull(as_stringa) or as_stringa = "" then return ""
if isnull(as_to_replace) then as_to_replace = "<null_value>"

li_lunghezza_formula = len(as_stringa)
li_replace = len(as_to_replace)
li_tofind = len(as_to_find)

li_posizione = 1

do while true
	li_posizione = pos(as_stringa, as_to_find, li_posizione)
	if li_posizione=0 or isnull(li_posizione) then exit	
	
	as_stringa = replace (as_stringa, li_posizione, li_tofind, as_to_replace) 
	li_posizione += li_replace
	
	if not ab_replace_all then
		// Sostituisco solo la prima occorrenza
		exit
	end if
loop


return as_stringa
end function

public function string sanatize_for_sql (string as_value);/**
 * stefanop
 * 29/06/2016
 *
 * Pulisco la stringa da eventuali caratteri che danno problemi con sql e/o metto i caratteri di escape
 **/
 
as_value = replaceall(as_value, "*", "~*", true)

return as_value
end function

on uo_str.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_str.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

