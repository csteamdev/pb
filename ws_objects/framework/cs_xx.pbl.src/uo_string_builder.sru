﻿$PBExportHeader$uo_string_builder.sru
forward
global type uo_string_builder from nonvisualobject
end type
end forward

global type uo_string_builder from nonvisualobject
end type
global uo_string_builder uo_string_builder

type variables
constant string LF = "~r~n"
constant string BR = "<br />"

private:
	string is_value
end variables

forward prototypes
private subroutine help ()
public function string tostring ()
public function uo_string_builder append (any aa_value)
public function uo_string_builder space ()
public function uo_string_builder newline ()
public function uo_string_builder lf ()
public function uo_string_builder br ()
end prototypes

private subroutine help ();/**
 * stefanop
 * 02/09/2014
 *
 * Oggetto per semplificare la concatenazione delle stringhe all'interno di PB
 *
 * E' possibile usare all'infinito il metodo .append per concatenare i valori ad una 
 * stringa; l'oggetto si preoccupa di non concatenare in caso di stringa vuota o nulla.
 *
 * E' possibile ritornare il valore tramite il metodo .tostring()
 *
 * Esempio:
 * string ls_pippo = g_str.append("Donato è ").append("un ricchione ").append(2).tostring()
 *
 * ls_pippo sarà "Donato è un ricchione 2"
 **/
end subroutine

public function string tostring ();/**
 * stefanop
 * 02/09/2014
 *
 * Ritorna la stringa
 **/
 
return is_value
end function

public function uo_string_builder append (any aa_value);/**
 * stefanop
 * 02/09/2014
 *
 **/
 
if not isnull(aa_value) then

	is_value += string(aa_value)

end if
	

return this
end function

public function uo_string_builder space ();/**
 * stefanop
 * 02/09/2014
 *
 **/
 
 is_value += " "
	
return this
end function

public function uo_string_builder newline ();/**
 * stefanop
 * 02/09/2014
 *
 **/
 
is_value += LF

return this
end function

public function uo_string_builder lf ();/**
 * stefanop
 * 02/09/2014
 *
 **/
 
is_value += LF

return this
end function

public function uo_string_builder br ();/**
 * stefanop
 * 02/09/2014
 *
 **/
 
is_value += BR

return this
end function

on uo_string_builder.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_string_builder.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;is_value = ""
end event

