﻿$PBExportHeader$uo_webfax.sru
forward
global type uo_webfax from nonvisualobject
end type
end forward

global type uo_webfax from nonvisualobject
end type
global uo_webfax uo_webfax

forward prototypes
public function integer uof_fax_destinatario (string fs_fax_dest, ref string fs_valore, ref string fs_msg)
public function integer uof_oggetto_fax (string fs_num_doc_fiscale, ref string fs_valore, ref string fs_msg)
public function integer uof_invia_webfax (string fs_cod_webfax, string fs_testo, string fs_allegati[], string fs_fax_destinatario, string fs_num_doc_fiscale, ref string fs_msg)
end prototypes

public function integer uof_fax_destinatario (string fs_fax_dest, ref string fs_valore, ref string fs_msg);//nella variabile fs_valore sostituisce [NUM] con fs_fax_dest, che contiene l'effettivo numero del fax

string ls_stringa_da_sostituire
long ll_pos = 1

ls_stringa_da_sostituire = "[NUM]"
ll_pos = Pos(fs_valore, ls_stringa_da_sostituire, ll_pos)

if ll_pos=0 then
	fs_msg = "Stringa Marcatore " + ls_stringa_da_sostituire + " non trovata nel campo destinatario!"
	return -1
end if

do while ll_pos > 0
	 fs_valore = Replace(fs_valore, ll_pos, len(ls_stringa_da_sostituire), fs_fax_dest)
	 
	 ll_pos = Pos(fs_valore, ls_stringa_da_sostituire, ll_pos + len(fs_fax_dest))
loop	


return 1
end function

public function integer uof_oggetto_fax (string fs_num_doc_fiscale, ref string fs_valore, ref string fs_msg);//nella variabile fs_valore sostituisce [DOC] con fs_num_doc_fiscale, che contiene la numerazione fiscale del documento
//se [DOC] non viene trovato allora non fare niente

string ls_stringa_da_sostituire
long ll_pos = 1

ls_stringa_da_sostituire = "[DOC]"
ll_pos = Pos(fs_valore, ls_stringa_da_sostituire, ll_pos)

if ll_pos=0 then
	//non fa niente....
	return 1
end if

do while ll_pos > 0
	 fs_valore = Replace(fs_valore, ll_pos, len(ls_stringa_da_sostituire), fs_num_doc_fiscale)
	 
	 ll_pos = Pos(fs_valore, ls_stringa_da_sostituire, ll_pos + len(fs_num_doc_fiscale))
loop	

fs_num_doc_fiscale = "FAX " + fs_num_doc_fiscale

return 1
end function

public function integer uof_invia_webfax (string fs_cod_webfax, string fs_testo, string fs_allegati[], string fs_fax_destinatario, string fs_num_doc_fiscale, ref string fs_msg);//usa la utility sendemail (presente in risorse nella sottocartella SENDEMAIL)

string ls_cmd, ls_indirizzo_mittente, ls_indirizzo_dest, ls_stringa_invio, ls_stringa_allegati
string ls_smtp_server, ls_smtp_usr, ls_smtp_pwd, ls_path_log
long ll_index

ls_cmd = s_cs_xx.volume + s_cs_xx.risorse + 'SENDEMAIL\sendemail.exe'
ls_path_log =  s_cs_xx.volume + s_cs_xx.risorse + "SENDEMAIL\LOG\"+s_cs_xx.cod_utente+".log"

if not FileExists(ls_cmd) then
	fs_msg = "Utility SendEmail non trovata!"
	return -1
end if

//racchiudi tutto tra doppi apici
ls_cmd = '"'+ls_cmd+'"'

//lettura parametri dal webfax
select indirizzo_mittente,   
		indirizzo_dest,   
		stringa_invio 
 into	:ls_indirizzo_mittente, 
 		:ls_indirizzo_dest, 
		:ls_stringa_invio
 from tab_webfax
 where 	cod_azienda=:s_cs_xx.cod_azienda and
 			cod_fax=:fs_cod_webfax;
			 
if sqlca.sqlcode <> 0 then
	fs_msg = "Errore in lettura parametri webfax. "
	if not isnull(sqlca.sqlerrtext) and sqlca.sqlerrtext<>"" then fs_msg+="~r~n"+sqlca.sqlerrtext
	
	return -1
end if

//check dei campi
if ls_indirizzo_mittente="" or isnull(ls_indirizzo_mittente) then
	fs_msg = "Non è stato specificato il campo mittente nel webfax selezionato!"
	return -1
end if
if ls_indirizzo_dest="" or isnull(ls_indirizzo_dest) then
	fs_msg = "Non è stato specificato il campo destinatario nel webfax selezionato!"
	return -1
end if
if ls_stringa_invio="" or isnull(ls_stringa_invio) then
	fs_msg = "Non è stato specificato il campo stringa invio nel webfax selezionato!"
	return -1
end if

//elabora il campo destinatario sostituendo [NUM] con il fax del destinatario
if uof_fax_destinatario(fs_fax_destinatario, ls_indirizzo_dest, fs_msg) < 0 then
	//in fs_msg il messaggio di errore
	return -1
end if

//elabora il campo stringa invio sostituendo [DOC] (se presente) con la numerazione fiscale del documento bolla o ddt
if uof_oggetto_fax(fs_num_doc_fiscale, ls_stringa_invio, fs_msg) < 0 then
	//in fs_msg il messaggio di errore
	return -1
end if

if isnull(fs_testo) or fs_testo="" then fs_testo = " "

//racchiudi tutto tra doppi apici
fs_testo = '"' + fs_testo + '"'
 ls_stringa_invio = '"' + ls_stringa_invio + '"'
 
ls_cmd += " -f "+ls_indirizzo_mittente
ls_cmd += " -t "+ls_indirizzo_dest
ls_cmd += " -u "+ls_stringa_invio
ls_cmd += " -m "+fs_testo

ls_stringa_allegati = ""
for ll_index = 1 to upperbound(fs_allegati[])
	//doppio apice in apertura
	ls_stringa_allegati += '"'
	
	ls_stringa_allegati +=  fs_allegati[ll_index]
	
	//doppio apice in chiusura + spazio
	ls_stringa_allegati += '" '
next
ls_cmd += " -a "+ls_stringa_allegati


//parametri smtp
select smtp_server,   
		smtp_usr,   
		smtp_pwd  
 into 		:ls_smtp_server, 
 			:ls_smtp_usr, 
			:ls_smtp_pwd
 from utenti
 where cod_utente =:s_cs_xx.cod_utente;
 
 
 //check parametri smtp dell'utente
 if sqlca.sqlcode<0 then
	fs_msg = "Errore in lettura parametri SMTP dell'utente! "+ sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_msg = "Impossibile leggere i parametri SMTP dell'utente perchè l'utente non è presente in tabella!"
	return -1
	
elseif ls_smtp_server="" or isnull(ls_smtp_server) then
	fs_msg = "Manca l'indirizzo server SMTP associato all'utente!"
	return -1
	
end if

ls_cmd += " -s "+ls_smtp_server

//utente e password smtp possono essere anche vuoti
if not isnull(ls_smtp_usr) and ls_smtp_usr<>"" then
	ls_cmd += " -xu "+ls_smtp_usr
end if
if not isnull(ls_smtp_pwd) and ls_smtp_pwd<>"" then
	ls_cmd += " -xp "+ls_smtp_pwd
end if

//percorso ls_path_logorso log invio per utente APICE-OMNIA
ls_cmd += " -l "+ls_path_log

run(ls_cmd)

return 1
end function

on uo_webfax.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_webfax.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

