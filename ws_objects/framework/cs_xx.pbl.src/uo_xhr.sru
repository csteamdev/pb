﻿$PBExportHeader$uo_xhr.sru
forward
global type uo_xhr from nonvisualobject
end type
end forward

global type uo_xhr from nonvisualobject
end type
global uo_xhr uo_xhr

type variables
// Content-Type
constant string XML = "text/xml"
constant string TEXT = "text/html"
constant string JSON = "application/json"
constant string FORM = "application/x-www-form-urlencoded"

// Method
constant string METHOD_GET = "GET"
constant string METHOD_POST = "POST"
constant string METHOD_DELETE = "DELETE"
constant string METHOD_PUT = "PUT"

private:
	boolean supported = false
	
	// versioni XMLHTTP, dalla più recente alla più vecchia
	// http://www.microsoft.com/en-us/download/details.aspx?id=19662#filelist
	string is_versions[] = { &
		"Msxml2.XMLHTTP.4.0", &
		"Msxml2.XMLHTTP.3.0", &
		"Msxml2.XMLHTTP.2.0" &
	}
	
	oleobject iole_xhr
	
	string is_method = METHOD_GET
	
	uo_map iuo_empty_param
end variables

forward prototypes
private subroutine create_xhr ()
public subroutine set_request_header (string as_header_key, string as_value)
public function boolean is_supported ()
public function integer get (string as_url)
public function integer post (string as_url)
public subroutine set_content_type (string as_content_type)
public function integer get (string as_url, uo_map auo_map)
public function integer post (string as_url, uo_map auo_map)
private function string build_query (string as_url, string as_method, uo_map auo_map)
public function integer get_response_code ()
public function string get_response_status_text ()
public function string get_response_text ()
private subroutine help ()
private function integer send_request (string as_method, string as_url, uo_map auo_map)
end prototypes

private subroutine create_xhr ();/**
 * stefanop
 * 22/07/2014
 *
 * Provo a crere l'ole per la connessione.
 **/
 
int li_i, li_errore

iole_xhr = CREATE oleobject

supported = false

// Ciclo tutte le chiamate conosciuti, alla prima valida istanzio ed esco
for li_i = 1 to upperbound(is_versions)
	
	li_errore = iole_xhr.ConnectToNewObject(is_versions[li_i])
	
	if li_errore = 0 then 
		supported = true
		exit
	end if
	
next

end subroutine

public subroutine set_request_header (string as_header_key, string as_value);/**
 * stefanop
 * 22/07/2014
 *
 * Imposta le intestazioni HTTP
 **/
 
if g_str.isnotempty(as_header_key) and g_str.isnotempty(as_value) then
	iole_xhr.setRequestHeader(as_header_key, as_value)
end if
end subroutine

public function boolean is_supported ();return supported
end function

public function integer get (string as_url);/**
 * stefanop
 * 22/07/2014
 *
 * Invia una richiesta di GET e non recupera il risultato
 **/
 
return get(as_url, iuo_empty_param)
end function

public function integer post (string as_url);
return post(as_url, iuo_empty_param)
end function

public subroutine set_content_type (string as_content_type);/**
 * stefanop
 * 22/07/2014
 *
 * Imposta il contenuto della richiesta
 **/
 
set_request_header("Content-Type", as_content_type)
end subroutine

public function integer get (string as_url, uo_map auo_map);/**
 * stefanop
 * 22/07/2014
 *
 * Invia una richiesta di GET e non recupera il risultato
 **/
 
return send_request(METHOD_GET, as_url, auo_map)
end function

public function integer post (string as_url, uo_map auo_map);set_content_type(FORM)

return send_request(METHOD_POST, as_url, auo_map)
end function

private function string build_query (string as_url, string as_method, uo_map auo_map);/**
 * stefanop
 * 22/07/2014
 *
 * Preparazione della query in base al metodo
 **/
 
string ls_query = "", ls_separator
int li_count, li_i

if as_method = METHOD_GET then
	if g_str.contains(as_url, "?") then
		ls_separator = "&"
	else
		ls_separator = "?"
	end if
else
	ls_separator = ""
end if

for li_i = 1 to auo_map.size()
	
	ls_query += ls_separator
	ls_query += auo_map.get_key(li_i)
	ls_query += "=" + auo_map.get(li_i)
	
	ls_separator = "&"
	
next

return ls_query
end function

public function integer get_response_code ();/**
 * stefanop
 * 11/01/2013
 *
 * Ritorna il codice di risposta dell'ultima chiamata.
 *
 * 200 OK
 * 400 Errore
 * 404 Non trovato
 * 500 Errore del server
 **/
 
 
return integer(iole_xhr.Status)
end function

public function string get_response_status_text ();/**
 * stefanop
 * 11/01/2013
 *
 * Ritorna il codice di risposta dell'ultima chiamata.
 *
 * OK
 * BAD REQUEST
 **/
 

return string(iole_xhr.StatusText)
end function

public function string get_response_text ();/**
 * stefanop
 * 11/01/2013
 *
 * Ritorna il corpo di risposta della chiamata
 **/
 

return string(iole_xhr.ResponseText)
end function

private subroutine help ();/**
 * stefanop
 * 23/07/2014
 *
 * Questo oggetto permette di eseguire richieste XHR verso delle pagine.
 * Le richieste possono essere di tipo GET, POST, PUT, DELETE
 *
 * Utilizzo:
 * 1. Creare l'oggeto uo_xhr
 * 2. controllare che uo_xhr.is_supported() = true, altrimenti verificare l'errore
 * 3. Impostare l'url uo_xhr.get(URL) oopure uo_xhr.post(URL)
 * 6. Eseguire la chiamata uo_xhr.send()
 **/
end subroutine

private function integer send_request (string as_method, string as_url, uo_map auo_map);/**
 * stefanop
 * 22/07/2014
 *
 * Invia la richiesta HTTP
 **/
 
string ls_query
int li_status_code 

if not isvalid(auo_map) or isnull(auo_map) or auo_map.size() < 1 then
	setnull(ls_query)
else
	ls_query = build_query(as_url, as_method, auo_map)
end if

try 
	if not isnull(ls_query) then as_url +=  ls_query
	
	iole_xhr.open(as_method, as_url, false)  // Sincrone
	
	/*
	Per chiamate Asyncrone; bisogna però vedere
	come recuperare i valori di ritorno con i callback
	iole_xhr.open(as_method, as_url, true)  
	*/
	
	if as_method = METHOD_GET then
		iole_xhr.send()
	else
		iole_xhr.send(ls_query)	
	end if
	
	li_status_code = get_response_code()
	
catch(Exception e)
	li_status_code = -1
end try
 
return li_status_code
end function

on uo_xhr.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_xhr.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iuo_empty_param = create uo_map

create_xhr()
end event

event destructor;destroy iole_xhr
destroy iuo_empty_param
end event

