﻿$PBExportHeader$uo_xhr2.sru
forward
global type uo_xhr2 from nonvisualobject
end type
end forward

global type uo_xhr2 from nonvisualobject
end type
global uo_xhr2 uo_xhr2

type variables
// Content-Type
constant string XML = "text/xml"
constant string TEXT = "text/html"
constant string JSON = "application/json"
constant string FORM = "application/x-www-form-urlencoded"
constant string MULTIPART = "multipart/form-data"

// Method
constant string METHOD_GET = "GET"
constant string METHOD_POST = "POST"
constant string METHOD_DELETE = "DELETE"
constant string METHOD_PUT = "PUT"

private:
	string version = "0.0.1-SNAPSHOT"

	boolean supported = false
	
	// versioni XMLHTTP, dalla più recente alla più vecchia
	// http://www.microsoft.com/en-us/download/details.aspx?id=19662#filelist
	string is_versions[] = { &
		"msxml2.XMLHTTP.6.0", &
		"msxml2.XMLHTTP.4.0", &
		"msxml2.XMLHTTP.3.0", &
		"msxml2.XMLHTTP.2.0" &
	}
	
	oleobject iole_xhr
	
	uo_map iuo_params
	uo_map iuo_request_header_params
	
	string is_method
	
	string is_url
	
	// username
	string is_username
	string is_password
	
	// Access token
	string is_header_access_token_name = "Token"
	string is_query_access_token_name = "access_token"
	string is_access_token

end variables

forward prototypes
private subroutine create_xhr_ole ()
public function uo_xhr2 method (string as_method)
public function integer send ()
public function uo_xhr2 get (string as_url)
public function uo_xhr2 post (string as_url)
public function uo_xhr2 put (string as_url)
private function string build_query_string ()
public function uo_xhr2 clear ()
public function uo_xhr2 addparam (string as_key, any aa_value)
public function uo_xhr2 contenttype (string as_content_type)
public function uo_xhr2 getjson (string as_url)
public function uo_xhr2 postjson (string as_url)
public function uo_xhr2 removeparam (string as_key)
public function integer responseCode ()
public function string responsestatustext ()
public function uo_xhr2 setrequestheader (string as_header_key, string as_header_value)
public function boolean issupported ()
public function string responsetext ()
public subroutine responsejson (ref oleobject aole_json) throws xhrexception
public function integer sendblob (blob a_blob)
public function uo_xhr2 authorizationtoken (string as_access_token)
public function uo_xhr2 username (string as_username)
public function uo_xhr2 password (string as_password)
public function uo_xhr2 credentials (string as_username, string as_password)
public function integer send (any as_boby)
public function string getresponseheader (string as_header)
public subroutine responsebody (ref blob ab_blob)
end prototypes

private subroutine create_xhr_ole ();/**
 * stefanop
 * 23/12/2014
 *
 * Crea l'ole per la connessione
 **/
 
int li_i, li_errore

iole_xhr = CREATE oleobject

supported = false

iuo_params = create uo_map
iuo_request_header_params = create uo_map

// Ciclo tutte le chiamate conosciuti, alla prima valida istanzio ed esco
for li_i = 1 to upperbound(is_versions)
	
	li_errore = iole_xhr.ConnectToNewObject(is_versions[li_i])
	
	if li_errore = 0 then
		supported = true
		exit
	end if
	
next

end subroutine

public function uo_xhr2 method (string as_method);/**
 * stefanop
 * 23/12/2014
 *
 * Imposta il metodo della richiesta
 **/
 
is_method = as_method

return this
end function

public function integer send ();/**
 * stefanop
 * 23/12/2014
 *
 *  Invia la richiesta HTTP
 **/
 
string ls_query
int li_status_code, li_i

if isnull(iuo_params) or iuo_params.size() < 1 then
	setnull(ls_query)
else
	ls_query = build_query_string()
end if
 
try 
	if is_method = METHOD_GET then
		if not isnull(ls_query) then is_url +=  ls_query
	end if
	
	if g_str.isnotempty(is_username) then
		iole_xhr.open(is_method, is_url, false, is_username, is_password)  // Sincrone
	else
		iole_xhr.open(is_method, is_url, false)  // Sincrone
	end if

	if not isnull(iuo_request_header_params) then
		if iuo_request_header_params.size( ) > 0 then
			for li_i = 1 to iuo_request_header_params.size( )
				iole_xhr.setRequestHeader(iuo_request_header_params.get_key(li_i) , iuo_request_header_params.get(li_i))
			next
		end if
	end if
	
	if is_method = METHOD_GET then
		iole_xhr.send()
	else
		iole_xhr.send(ls_query)	
	end if
		
	li_status_code = responseCode()

catch(Exception e)
	li_status_code = -1
catch(RuntimeError er)
	li_status_code = -1
end try
 
return li_status_code
end function

public function uo_xhr2 get (string as_url);/**
 * stefanop
 * 23/12/2014
 *
 * Imposta il metodo della richiesta
 **/
 
is_url = as_url

return method(METHOD_GET)
end function

public function uo_xhr2 post (string as_url);/**
 * stefanop
 * 23/12/2014
 *
 * Imposta il metodo della richiesta
 **/
 
is_url = as_url

return method(METHOD_POST)
end function

public function uo_xhr2 put (string as_url);/**
 * stefanop
 * 23/12/2014
 *
 * Imposta il metodo della richiesta
 **/
 
is_url = as_url

return method(METHOD_PUT)
end function

private function string build_query_string ();/**
 * stefanop
 * 23/12/2014
 *
 * Preparazione della query in base al metodo
 **/
 
string ls_query = "", ls_separator
int li_count, li_i

if is_method = METHOD_GET then
	if g_str.contains(is_url, "?") then
		ls_separator = "&"
	else
		ls_separator = "?"
	end if
else
	ls_separator = ""
end if

if not isnull(iuo_params) then
	for li_i = 1 to iuo_params.size()
		
		ls_query += ls_separator
		ls_query += iuo_params.get_key(li_i)
		ls_query += "=" + iuo_params.get(li_i)
		
		ls_separator = "&"
		
	next
end if

return ls_query
end function

public function uo_xhr2 clear ();/**
 * stefanop
 * 23/12/2014
 *
 * Pulisce tutte le variabili prima di eseguire una nuova 
 * richiesta web
 **/

iuo_params.clear()
iuo_request_header_params.clear()
setnull(is_username)
setnull(is_password)

return this
end function

public function uo_xhr2 addparam (string as_key, any aa_value);/**
 * stefanop
 * 23/12/2014
 *
 * Aggiunge un parametro alla richiesta.
 * Il metodo crea in maniera Lazy la mappa dei
 * parametri.
 *
 **/
 
if isnull(iuo_params) then
	iuo_params = create uo_map
end if

iuo_params.put(as_key, aa_value)

return this
end function

public function uo_xhr2 contenttype (string as_content_type);/**
 * stefanop
 * 23/12/2014
 *
 * Imposta il contenuto della richiesta
 **/
 
setRequestHeader("Content-Type", as_content_type)

return this
end function

public function uo_xhr2 getjson (string as_url);/**
 * stefanop
 * 23/12/2014
 *
 * Imposta il metodo della richiesta
 **/
 
is_url = as_url

method(METHOD_GET)
contentType(JSON)

return this
end function

public function uo_xhr2 postjson (string as_url);/**
 * stefanop
 * 23/12/2014
 *
 * Imposta il metodo della richiesta
 **/
 
is_url = as_url

method(METHOD_POST)
contentType(JSON)

return this
end function

public function uo_xhr2 removeparam (string as_key);/**
 * stefanop
 * 23/12/2014
 *
 * Aggiunge un parametro alla richiesta.
 * Il metodo crea in maniera Lazy la mappa dei
 * parametri.
 *
 **/
 
if not isnull(iuo_params) then
	iuo_params.remove(as_key)
end if

return this
end function

public function integer responseCode ();/**
 * stefanop
 * 11/01/2013
 *
 * Ritorna il codice di risposta dell'ultima chiamata.
 *
 * 200 OK
 * 400 Errore
 * 404 Non trovato
 * 500 Errore del server
 **/
 
 
return integer(iole_xhr.Status)
end function

public function string responsestatustext ();/**
 * stefanop
 * 11/01/2013
 *
 * Ritorna il codice di risposta dell'ultima chiamata.
 *
 * OK
 * BAD REQUEST
 **/
 

return string(iole_xhr.StatusText)
end function

public function uo_xhr2 setrequestheader (string as_header_key, string as_header_value);/**
 * stefanop
 * 23/12/2014
 *
 * Imposto le intestazioni della richiesta http
 **/
 
if g_str.isnotempty(as_header_key) and g_str.isnotempty(as_header_value) then
	if isnull(iuo_request_header_params) then
		iuo_request_header_params = create uo_map
	end if
	
	iuo_request_header_params.put(as_header_key, as_header_value)
end if
 
return this
end function

public function boolean issupported ();return supported
end function

public function string responsetext ();/**
 * stefanop
 * 11/01/2013
 *
 * Ritorna il corpo di risposta della chiamata
 **/
 

return string(iole_xhr.ResponseText)
end function

public subroutine responsejson (ref oleobject aole_json) throws xhrexception;/**
 * stefanop
 * 23/12/2014
 *
 * Legge il testo della risposta in formato JSON.
 **/
 
uo_json luo_json_parser
XhrException xhr_eception

try
	luo_json_parser = create uo_json
	
	luo_json_parser.parse(responseText(), aole_json)
	
catch(Exception ex)
	xhr_eception = create XhrException
	xhr_eception.setmessage(ex.getmessage())
	throw xhr_eception
	
catch(RuntimeError ere)
	xhr_eception = create XhrException
	xhr_eception.setmessage(ere.getmessage())
	throw xhr_eception
	
finally
	destroy luo_json_parser
end try

end subroutine

public function integer sendblob (blob a_blob);/**
 * stefanop
 * 23/12/2014
 *
 *  Invia la richiesta HTTP
 **/
 
string CRLF
string ls_query, ls_body, ls_boundary
int li_status_code, li_i

try 
	CRLF = "~r~n"
	ls_boundary = "83ff53821b7c"
	if not isnull(ls_query) then is_url +=  ls_query
	
	iole_xhr.open(is_method, is_url, false) 
	
	if not isnull(iuo_request_header_params) then
		if iuo_request_header_params.size( ) > 0 then
			for li_i = 1 to iuo_request_header_params.size( )
				iole_xhr.setRequestHeader(iuo_request_header_params.get_key(li_i) , iuo_request_header_params.get(li_i))
			next
		end if
	end if
	
	iole_xhr.setRequestHeader("Content-Type", TEXT) // + "; boundary=" + ls_boundary)
	
	ls_body = ""
	ls_body += "--" + ls_boundary + CRLF;
	ls_body += 'Content-Disposition: attachment; name="Mario"; filename="mario.jpg"' + CRLF;
	ls_body += "Content-Transfer-Encoding: binary" + CRLF + CRLF;
	ls_body += string(a_blob) + CRLF;
	ls_body += "--" + ls_boundary + "--" + CRLF + CRLF;
		
	iole_xhr.setRequestHeader("Content-Length", len(ls_body))
	
	iole_xhr.send(a_blob)	
	
	li_status_code = responseCode()
	
catch(Exception e)
	li_status_code = -1
end try
 
return li_status_code
end function

public function uo_xhr2 authorizationtoken (string as_access_token);
setRequestHeader("Authorization", is_header_access_token_name + " " + as_access_token)
return this
end function

public function uo_xhr2 username (string as_username);/**
 * stefanop
 * 26/12/2014
 *
 * Imposta l'username da inviare alla richiesta
 **/
 
is_username = as_username
return this
end function

public function uo_xhr2 password (string as_password);/**
 * stefanop
 * 26/12/2014
 *
 * Imposta la password da inviare alla richiesta
 **/
 
is_password = as_password
return this
end function

public function uo_xhr2 credentials (string as_username, string as_password);/**
 * stefanop
 * 26/12/2014
 *
 * Imposta le credenziali da inviare alla richiesta
 **/

is_username = as_username
is_password = as_password
return this
end function

public function integer send (any as_boby);/**
 * stefanop
 * 23/12/2014
 *
 *  Invia la richiesta HTTP
 **/
 
string ls_query
int li_status_code, li_i

if not isvalid(iuo_params) or isnull(iuo_params) or iuo_params.size() < 1 then
	setnull(ls_query)
else
	ls_query = build_query_string()
end if
 
try 
	if is_method = METHOD_GET then
		if not isnull(ls_query) then is_url +=  ls_query
	end if
	
	iole_xhr.open(is_method, is_url, false)  // Sincrone
	
	if not isnull(iuo_request_header_params) then
		if iuo_request_header_params.size( ) > 0 then
			for li_i = 1 to iuo_request_header_params.size( )
				iole_xhr.setRequestHeader(iuo_request_header_params.get_key(li_i) , iuo_request_header_params.get(li_i))
			next
		end if
	end if
	
	if is_method = METHOD_GET then
		iole_xhr.send()
	else
		iole_xhr.send(as_boby)	
	end if
	
	li_status_code = responseCode()
	
catch(Exception e)
	li_status_code = -1
end try
 
return li_status_code
end function

public function string getresponseheader (string as_header);return iole_xhr.getResponseHeader(as_header)
end function

public subroutine responsebody (ref blob ab_blob);ab_blob = blob(iole_xhr.ResponseBody)
end subroutine

on uo_xhr2.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_xhr2.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;setnull(iuo_params)
setnull(iuo_request_header_params)

create_xhr_ole()
end event

