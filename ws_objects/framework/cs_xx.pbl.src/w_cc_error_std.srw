﻿$PBExportHeader$w_cc_error_std.srw
$PBExportComments$Finestra Errore Concorrenza Powerclass (copiata da Powerclass)
forward
global type w_cc_error_std from w_cc_error_main
end type
end forward

global type w_cc_error_std from w_cc_error_main
int Width=2204
int Height=1665
end type
global w_cc_error_std w_cc_error_std

type variables

end variables

on w_cc_error_std.create
call w_cc_error_main::create
end on

on w_cc_error_std.destroy
call w_cc_error_main::destroy
end on

type p_information from w_cc_error_main`p_information within w_cc_error_std
int X=92
int Y=141
end type

type st_errortype from w_cc_error_main`st_errortype within w_cc_error_std
int X=298
int Y=141
end type

type gb_errortype from w_cc_error_main`gb_errortype within w_cc_error_std
int Y=21
string Text="Errore di Concorrenza"
end type

type gb_movement from w_cc_error_main`gb_movement within w_cc_error_std
int Y=381
int Height=261
string Text="Selezione Colonna"
end type

type rb_saveolddb from w_cc_error_main`rb_saveolddb within w_cc_error_std
int X=823
int Y=481
int Width=755
int Height=61
string Text="Valore Originale Database"
end type

type rb_savenewdb from w_cc_error_main`rb_savenewdb within w_cc_error_std
int X=823
int Y=561
int Width=755
int Height=61
string Text="Valore Nuovo Database"
end type

type rb_saveentered from w_cc_error_main`rb_saveentered within w_cc_error_std
int Y=481
string Text="Valore Inserito"
end type

type rb_savespecial from w_cc_error_main`rb_savespecial within w_cc_error_std
int Y=561
int Width=503
int Height=61
string Text="Valore Alternativo"
end type

type gb_proccode from w_cc_error_main`gb_proccode within w_cc_error_std
int Y=381
int Width=1349
int Height=261
string Text="Valori da Salvare"
end type

type cb_ok from w_cc_error_main`cb_ok within w_cc_error_std
int X=1761
int Y=41
int Width=366
int Height=81
end type

type cb_cancel_cl from w_cc_error_main`cb_cancel_cl within w_cc_error_std
int X=1761
int Y=141
int Width=366
int Height=81
string Text="Annulla"
end type

type cb_help from w_cc_error_main`cb_help within w_cc_error_std
int X=1761
int Y=241
int Width=366
int Height=81
end type

type cb_yes from w_cc_error_main`cb_yes within w_cc_error_std
int X=1761
int Y=41
int Width=366
int Height=81
string Text="&Si"
end type

type cb_no from w_cc_error_main`cb_no within w_cc_error_std
int X=1761
int Y=141
int Width=366
int Height=81
end type

type cb_cancel_rl from w_cc_error_main`cb_cancel_rl within w_cc_error_std
int X=1761
int Y=241
int Width=366
int Height=81
string Text="Annulla"
end type

type dw_1 from w_cc_error_main`dw_1 within w_cc_error_std
int Y=681
int Width=2081
int Height=841
end type

