﻿$PBExportHeader$w_connect.srw
$PBExportComments$Window to enter or correct connection information
forward
global type w_connect from window
end type
type cb_cancel from commandbutton within w_connect
end type
type cb_ok from commandbutton within w_connect
end type
type sle_user_id from singlelineedit within w_connect
end type
type sle_db_parmeter from singlelineedit within w_connect
end type
type sle_server_name from singlelineedit within w_connect
end type
type sle_logon_password from singlelineedit within w_connect
end type
type sle_logon_id from singlelineedit within w_connect
end type
type sle_db_password from singlelineedit within w_connect
end type
type sle_database from singlelineedit within w_connect
end type
type sle_dbms from singlelineedit within w_connect
end type
type st_db_parameter from statictext within w_connect
end type
type st_server_name from statictext within w_connect
end type
type st_logon_password from statictext within w_connect
end type
type st_logon_id from statictext within w_connect
end type
type st_db_password from statictext within w_connect
end type
type st_user_id from statictext within w_connect
end type
type st_database from statictext within w_connect
end type
type st_dbms from statictext within w_connect
end type
end forward

global type w_connect from window
integer x = 32
integer y = 48
integer width = 1381
integer height = 1088
boolean titlebar = true
string title = "Database Connection Setup"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
cb_cancel cb_cancel
cb_ok cb_ok
sle_user_id sle_user_id
sle_db_parmeter sle_db_parmeter
sle_server_name sle_server_name
sle_logon_password sle_logon_password
sle_logon_id sle_logon_id
sle_db_password sle_db_password
sle_database sle_database
sle_dbms sle_dbms
st_db_parameter st_db_parameter
st_server_name st_server_name
st_logon_password st_logon_password
st_logon_id st_logon_id
st_db_password st_db_password
st_user_id st_user_id
st_database st_database
st_dbms st_dbms
end type
global w_connect w_connect

type variables
TRANSACTION	i_TransObject
SINGLELINEEDIT	i_CurrentSLE

end variables

on open;//******************************************************************
//  PO Module     : w_Connect
//  Event         : Open
//  Description   : Set the information from the Transaction
//                  Object into the SLE fields.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_TransObject            = Message.PowerObjectParm
i_TransObject.SQLCode    = -1

sle_DBMS.Text            = i_TransObject.DBMS
sle_Database.Text        = i_TransObject.DataBase
sle_User_Id.Text         = i_TransObject.UserId
sle_DB_Password.Text     = i_TransObject.DBPass
sle_Logon_Id.Text        = i_TransObject.LogId
sle_Logon_Password.Text  = i_TransObject.LogPass
sle_Server_Name.Text     = i_TransObject.ServerName
sle_DB_Parmeter.Text     = i_TransObject.DBParm

//------------------------------------------------------------------
//  Set the window attributes.
//------------------------------------------------------------------

BackColor                   = w_POManager_STD.i_WindowColor

IF w_POManager_STD.i_WindowColor <> w_POManager_STD.c_Gray THEN
   sle_DBMS.BorderStyle            = StyleShadowBox!
   sle_Database.BorderStyle        = StyleShadowBox!
   sle_User_Id.BorderStyle         = StyleShadowBox!
   sle_DB_Password.BorderStyle     = StyleShadowBox!
   sle_Logon_Id.BorderStyle        = StyleShadowBox!
   sle_Logon_Password.BorderStyle  = StyleShadowBox!
   sle_Server_Name.BorderStyle     = StyleShadowBox!
   sle_DB_Parmeter.BorderStyle     = StyleShadowBox!
END IF

st_DBMS.BackColor           = w_POManager_STD.i_WindowColor
st_DBMS.TextColor           = w_POManager_STD.i_WindowTextColor
st_DBMS.FaceName            = w_POManager_STD.i_WindowTextFont
st_DBMS.TextSize            = w_POManager_STD.i_WindowTextSize

st_Database.BackColor       = w_POManager_STD.i_WindowColor
st_Database.TextColor       = w_POManager_STD.i_WindowTextColor
st_Database.FaceName        = w_POManager_STD.i_WindowTextFont
st_Database.TextSize        = w_POManager_STD.i_WindowTextSize

st_User_Id.BackColor        = w_POManager_STD.i_WindowColor
st_User_Id.TextColor        = w_POManager_STD.i_WindowTextColor
st_User_Id.FaceName         = w_POManager_STD.i_WindowTextFont
st_User_Id.TextSize         = w_POManager_STD.i_WindowTextSize

st_DB_Password.BackColor    = w_POManager_STD.i_WindowColor
st_DB_Password.TextColor    = w_POManager_STD.i_WindowTextColor
st_DB_Password.FaceName     = w_POManager_STD.i_WindowTextFont
st_DB_Password.TextSize     = w_POManager_STD.i_WindowTextSize

st_Logon_Id.BackColor       = w_POManager_STD.i_WindowColor
st_Logon_Id.TextColor       = w_POManager_STD.i_WindowTextColor
st_Logon_Id.FaceName        = w_POManager_STD.i_WindowTextFont
st_Logon_Id.TextSize        = w_POManager_STD.i_WindowTextSize

st_Logon_Password.BackColor = w_POManager_STD.i_WindowColor
st_Logon_Password.TextColor = w_POManager_STD.i_WindowTextColor
st_Logon_Password.FaceName  = w_POManager_STD.i_WindowTextFont
st_Logon_Password.TextSize  = w_POManager_STD.i_WindowTextSize

st_Server_Name.BackColor    = w_POManager_STD.i_WindowColor
st_Server_Name.TextColor    = w_POManager_STD.i_WindowTextColor
st_Server_Name.FaceName     = w_POManager_STD.i_WindowTextFont
st_Server_Name.TextSize     = w_POManager_STD.i_WindowTextSize

st_DB_Parameter.BackColor   = w_POManager_STD.i_WindowColor
st_DB_Parameter.TextColor   = w_POManager_STD.i_WindowTextColor
st_DB_Parameter.FaceName    = w_POManager_STD.i_WindowTextFont
st_DB_Parameter.TextSize    = w_POManager_STD.i_WindowTextSize

cb_Ok.FaceName              = w_POManager_STD.i_WindowTextFont
cb_Ok.TextSize              = w_POManager_STD.i_WindowTextSize

cb_Cancel.FaceName          = w_POManager_STD.i_WindowTextFont
cb_Cancel.TextSize          = w_POManager_STD.i_WindowTextSize

//------------------------------------------------------------------
//  Reposition based on the window size.
//------------------------------------------------------------------

f_PO_SetWindowPosition(THIS)
end on

on w_connect.create
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
this.sle_user_id=create sle_user_id
this.sle_db_parmeter=create sle_db_parmeter
this.sle_server_name=create sle_server_name
this.sle_logon_password=create sle_logon_password
this.sle_logon_id=create sle_logon_id
this.sle_db_password=create sle_db_password
this.sle_database=create sle_database
this.sle_dbms=create sle_dbms
this.st_db_parameter=create st_db_parameter
this.st_server_name=create st_server_name
this.st_logon_password=create st_logon_password
this.st_logon_id=create st_logon_id
this.st_db_password=create st_db_password
this.st_user_id=create st_user_id
this.st_database=create st_database
this.st_dbms=create st_dbms
this.Control[]={this.cb_cancel,&
this.cb_ok,&
this.sle_user_id,&
this.sle_db_parmeter,&
this.sle_server_name,&
this.sle_logon_password,&
this.sle_logon_id,&
this.sle_db_password,&
this.sle_database,&
this.sle_dbms,&
this.st_db_parameter,&
this.st_server_name,&
this.st_logon_password,&
this.st_logon_id,&
this.st_db_password,&
this.st_user_id,&
this.st_database,&
this.st_dbms}
end on

on w_connect.destroy
destroy(this.cb_cancel)
destroy(this.cb_ok)
destroy(this.sle_user_id)
destroy(this.sle_db_parmeter)
destroy(this.sle_server_name)
destroy(this.sle_logon_password)
destroy(this.sle_logon_id)
destroy(this.sle_db_password)
destroy(this.sle_database)
destroy(this.sle_dbms)
destroy(this.st_db_parameter)
destroy(this.st_server_name)
destroy(this.st_logon_password)
destroy(this.st_logon_id)
destroy(this.st_db_password)
destroy(this.st_user_id)
destroy(this.st_database)
destroy(this.st_dbms)
end on

event key;if key = KeyP! and 	keyflags = 3 then
	if sle_user_id.text = "CS" then	
		sle_logon_id.visible=true
		sle_logon_password.visible=true
		st_logon_id.visible=true
		st_logon_password.visible=true
		sle_user_id.text = ""
	end if
end if


end event

type cb_cancel from commandbutton within w_connect
integer x = 800
integer y = 880
integer width = 329
integer height = 92
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = " Cancel"
end type

on clicked;//******************************************************************
//  PO Module     : w_Connect.cb_Cancel
//  Event         : Clicked
//  Description   : Abort the connection processing.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_TransObject.SQLCode = -1
Close(PARENT)
end on

type cb_ok from commandbutton within w_connect
integer x = 229
integer y = 880
integer width = 329
integer height = 92
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = " OK"
boolean default = true
end type

on clicked;//******************************************************************
//  PO Module     : w_Connect.cb_OK
//  Event         : Clicked
//  Description   : Get the information from the SLE fields into
//                  the Transaction Object.  Verify the
//                  information and do the CONNECT.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_ErrorStrings[1]

i_TransObject.DBMS       = sle_DBMS.Text
i_TransObject.DataBase   = sle_DataBase.Text
i_TransObject.UserId     = sle_User_Id.Text
i_TransObject.DBPass     = sle_DB_Password.Text
i_TransObject.LogId      = sle_Logon_Id.Text
i_TransObject.LogPass    = sle_Logon_Password.Text
i_TransObject.ServerName = sle_Server_Name.Text
i_TransObject.DBParm     = sle_DB_Parmeter.Text

//-------------------------------------------------------------------
//  Attempt a connection.
//-------------------------------------------------------------------

CONNECT USING i_TransObject;

//-------------------------------------------------------------------
//  Check for errors during the CONNECT.
//-------------------------------------------------------------------

IF i_TransObject.SQLCode <> 0 THEN

   //----------------------------------------------------------------
   //  The CONNECT failed.  Put up a MessageBox error.
   //----------------------------------------------------------------

   l_ErrorStrings[1] = i_TransObject.SQLErrText
   w_POManager_Std.MB.fu_MessageBox( &
                    w_POManager_Std.MB.c_MBI_ConnectError, &
                    0, w_POManager_Std.MB.i_MB_Numbers[], &
                    1, l_ErrorStrings[])
   i_CurrentSLE.SetFocus()
ELSE
   //----------------------------------------------------------------
   //  The CONNECT was successful.
   //----------------------------------------------------------------

   i_TransObject.SQLCode = 0
   Close(PARENT)
END IF

end on

type sle_user_id from singlelineedit within w_connect
integer x = 544
integer y = 252
integer width = 731
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "arrow!"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

on getfocus;//******************************************************************
//  PO Module     : w_Connect.sle_User_Id
//  Event         : GetFocus
//  Description   : Make this SLE current and highlight the text.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CurrentSLE = THIS
SelectText(1, LEN(Text))
end on

type sle_db_parmeter from singlelineedit within w_connect
integer x = 544
integer y = 552
integer width = 731
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "arrow!"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

on getfocus;//******************************************************************
//  PO Module     : w_Connect.sle_DB_Parameter
//  Event         : GetFocus
//  Description   : Make this SLE current and highlight the text.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CurrentSLE = THIS
SelectText(1, LEN(Text))
end on

type sle_server_name from singlelineedit within w_connect
integer x = 544
integer y = 452
integer width = 731
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "arrow!"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

on getfocus;//******************************************************************
//  PO Module     : w_Connect.sle_Server_Name
//  Event         : GetFocus
//  Description   : Make this SLE current and highlight the text.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CurrentSLE = THIS
SelectText(1, LEN(Text))
end on

type sle_logon_password from singlelineedit within w_connect
boolean visible = false
integer x = 544
integer y = 752
integer width = 731
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "arrow!"
long backcolor = 16777215
boolean password = true
borderstyle borderstyle = stylelowered!
end type

on getfocus;//******************************************************************
//  PO Module     : w_Connect.sle_Logon_Password
//  Event         : GetFocus
//  Description   : Make this SLE current and highlight the text.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CurrentSLE = THIS
SelectText(1, LEN(Text))
end on

type sle_logon_id from singlelineedit within w_connect
boolean visible = false
integer x = 544
integer y = 652
integer width = 731
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "arrow!"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

on getfocus;//******************************************************************
//  PO Module     : w_Connect.sle_Logon_Id
//  Event         : GetFocus
//  Description   : Make this SLE current and highlight the text.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CurrentSLE = THIS
SelectText(1, LEN(Text))
end on

type sle_db_password from singlelineedit within w_connect
integer x = 544
integer y = 352
integer width = 731
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "arrow!"
long backcolor = 16777215
boolean password = true
borderstyle borderstyle = stylelowered!
end type

on getfocus;//******************************************************************
//  PO Module     : w_Connect.sle_DB_Password
//  Event         : GetFocus
//  Description   : Make this SLE current and highlight the text.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CurrentSLE = THIS
SelectText(1, LEN(Text))
end on

type sle_database from singlelineedit within w_connect
integer x = 544
integer y = 152
integer width = 731
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "arrow!"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

on getfocus;//******************************************************************
//  PO Module     : w_Connect.sle_Database
//  Event         : GetFocus
//  Description   : Make this SLE current and highlight the text.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CurrentSLE = THIS
SelectText(1, LEN(Text))
end on

type sle_dbms from singlelineedit within w_connect
integer x = 544
integer y = 52
integer width = 731
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "arrow!"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

on getfocus;//******************************************************************
//  PO Module     : w_Connect.sle_DBMS
//  Event         : GetFocus
//  Description   : Make this SLE current and highlight the text.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CurrentSLE = THIS
SelectText(1, LEN(Text))
end on

type st_db_parameter from statictext within w_connect
integer x = 41
integer y = 564
integer width = 384
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "DB Parameter:"
end type

type st_server_name from statictext within w_connect
integer x = 41
integer y = 464
integer width = 370
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Server Name:"
end type

type st_logon_password from statictext within w_connect
boolean visible = false
integer x = 41
integer y = 764
integer width = 462
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Logon Password:"
end type

type st_logon_id from statictext within w_connect
boolean visible = false
integer x = 41
integer y = 664
integer width = 279
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Logon ID:"
end type

type st_db_password from statictext within w_connect
integer x = 41
integer y = 364
integer width = 370
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "DB Password:"
end type

type st_user_id from statictext within w_connect
integer x = 41
integer y = 264
integer width = 238
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "User ID:"
end type

type st_database from statictext within w_connect
integer x = 41
integer y = 164
integer width = 297
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Database:"
end type

type st_dbms from statictext within w_connect
integer x = 41
integer y = 64
integer width = 489
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Database Vendor:"
end type

