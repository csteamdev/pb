﻿$PBExportHeader$w_cs_graph_impostazioni.srw
forward
global type w_cs_graph_impostazioni from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_cs_graph_impostazioni
end type
type cb_conferma from commandbutton within w_cs_graph_impostazioni
end type
type dw_impostazioni from datawindow within w_cs_graph_impostazioni
end type
end forward

global type w_cs_graph_impostazioni from w_cs_xx_risposta
integer width = 1024
integer height = 2008
boolean titlebar = false
string title = ""
boolean controlmenu = false
boolean resizable = false
boolean center = false
windowanimationstyle openanimation = fadeanimation!
windowanimationstyle closeanimation = fadeanimation!
integer animationtime = 500
cb_annulla cb_annulla
cb_conferma cb_conferma
dw_impostazioni dw_impostazioni
end type
global w_cs_graph_impostazioni w_cs_graph_impostazioni

on w_cs_graph_impostazioni.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.dw_impostazioni=create dw_impostazioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_conferma
this.Control[iCurrent+3]=this.dw_impostazioni
end on

on w_cs_graph_impostazioni.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.dw_impostazioni)
end on

event pc_setwindow;call super::pc_setwindow;string 							ls_img[], ls_path_origine,ls_expression, ls_bitmap
long 								ll_row, ll_row2
s_cs_graph_settings 	ls_cs_graph_settings
DataWindowChild			ldwc_child

ls_cs_graph_settings = message.powerobjectparm

if not ls_cs_graph_settings.b_pie then
	dw_impostazioni.dataobject = "d_cs_graph_settings"
else
	dw_impostazioni.dataobject = "d_cs_graph_pie_settings"
end if

ll_row = dw_impostazioni.insertrow(0)
ls_path_origine = s_cs_xx.volume + s_cs_xx.risorse + "\img_grafici\"

if not ls_cs_graph_settings.b_pie then
	//imposto il vettore con i percorsi delle immagini
	ls_img[1] = ls_path_origine + "Area.png"
	ls_img[2] = ls_path_origine +  "Area3D.png"	
	ls_img[3] = ls_path_origine +  "Bar.png"
	ls_img[4] = ls_path_origine +  "Bar3D.png"
	ls_img[5] = ls_path_origine +  "Bar3DObj.png"
	ls_img[6] = ls_path_origine +  "BarStacked.png"
	ls_img[7] = ls_path_origine +  "BarStacked3DObj.png"
	ls_img[8] = ls_path_origine +  "Col.png"
	ls_img[9] = ls_path_origine +  "Col3D.png"
	ls_img[10] = ls_path_origine +  "Col3DObj.png"
	ls_img[11] = ls_path_origine +  "ColStacked.png"
	ls_img[12] = ls_path_origine +  "ColStacked3DObj.png"
	ls_img[13] = ls_path_origine +  "Line.png" //default
	ls_img[14] = ls_path_origine +  "Line3D.png"
	ls_img[15] = ls_path_origine +  "Pie.png"
	ls_img[16] = ls_path_origine +  "Pie3D.png"
	ls_img[17] = ls_path_origine +  "Scatter.png"
	ls_img[18] = ls_path_origine +  "bianco.png"
	ls_img[19] = ls_path_origine +  "argento.png"
	ls_img[20] = ls_path_origine +  "lineare.png"
	ls_img[21] = ls_path_origine +  "log10.png"
	//--------------------------------------------------------------------------------------
	
	//imposto le immagini nei relativi contenitori
	ls_expression = ls_img[1]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_area.expression = ls_bitmap
	//-------
	ls_expression = ls_img[2]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_area3d.expression = ls_bitmap
	//-------
	ls_expression = ls_img[3]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_barre.expression = ls_bitmap
	//-------
	ls_expression = ls_img[4]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_barre3d.expression = ls_bitmap
	//-------
	ls_expression = ls_img[5]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_barre3dobj.expression = ls_bitmap
	//-------
	ls_expression = ls_img[6]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_barreastack.expression = ls_bitmap
	//-------
	ls_expression = ls_img[7]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_barreastack3dobj.expression = ls_bitmap
	//-------
	ls_expression = ls_img[8]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_colonne.expression = ls_bitmap
	//-------
	ls_expression = ls_img[9]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_colonne3d.expression = ls_bitmap
	//-------
	ls_expression = ls_img[10]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_colonne3dobj.expression = ls_bitmap
	//-------
	ls_expression = ls_img[11]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_colonneastack.expression = ls_bitmap
	//-------
	ls_expression = ls_img[12]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_colonneastack3dobj.expression = ls_bitmap
	//-------
	ls_expression = ls_img[13]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_linee.expression = ls_bitmap
	//-------
	ls_expression = ls_img[14]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_linee3d.expression = ls_bitmap
	//-------
	ls_expression = ls_img[15]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_torta.expression = ls_bitmap
	//-------
	ls_expression = ls_img[16]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_torta3d.expression = ls_bitmap
	//-------
	ls_expression = ls_img[17]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_scatter.expression = ls_bitmap
	//-------
	ls_expression = ls_img[18]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_bianco.expression = ls_bitmap
	//-------
	ls_expression = ls_img[19]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_argento.expression = ls_bitmap
	//------
	ls_expression = ls_img[20]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_lineare.expression = ls_bitmap
	//-------
	ls_expression = ls_img[21]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_log10.expression = ls_bitmap
	//-------
else
	//imposto il vettore con i percorsi delle immagini	
	ls_img[1] = ls_path_origine +  "Pie.png"
	ls_img[2] = ls_path_origine +  "Pie3D.png"
	//--------------------------------------------------------------------------------------
	
	//imposto le immagini nei relativi contenitori
	ls_expression = ls_img[1]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_torta.expression = ls_bitmap
	//-------
	ls_expression = ls_img[2]
	ls_bitmap = "bitmap('"+ls_expression+"')"
	dw_impostazioni.object.cf_torta3d.expression = ls_bitmap
	//-------
end if

//this.set_w_options(c_noautoposition)

//set dei valori pre-impostati
dw_impostazioni.setitem( ll_row, "tipo_scala", ls_cs_graph_settings.i_scala )
dw_impostazioni.setitem( ll_row, "tipo_grafico", ls_cs_graph_settings.i_tipo_graph )
dw_impostazioni.setitem( ll_row, "backcolor", ls_cs_graph_settings.i_backcolor )


end event

event open;call super::open;//integer li_larghezza,li_altezza
//
//li_larghezza = w_cs_xx_mdi.WorkSpaceWidth ( )
//li_altezza = w_cs_xx_mdi.WorkSpaceheight ( )
//
//this.move( li_larghezza - this.width, 0)
//
end event

type cb_annulla from commandbutton within w_cs_graph_impostazioni
integer x = 544
integer y = 1884
integer width = 347
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;s_cs_graph_settings		l_s_cs_graph_settings

l_s_cs_graph_settings.b_conferma = false

closewithreturn( parent, l_s_cs_graph_settings )
end event

type cb_conferma from commandbutton within w_cs_graph_impostazioni
integer x = 142
integer y = 1884
integer width = 347
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
boolean default = true
end type

event clicked;s_cs_graph_settings		l_s_cs_graph_settings

dw_impostazioni.accepttext()

if dw_impostazioni.getitemnumber(1, "backcolor" ) >= 0 then
	
	l_s_cs_graph_settings.b_conferma = true
	l_s_cs_graph_settings.i_scala = dw_impostazioni.getitemnumber(1, "tipo_scala" )
	l_s_cs_graph_settings.i_tipo_graph = dw_impostazioni.getitemnumber(1, "tipo_grafico" )
	l_s_cs_graph_settings.i_backcolor = dw_impostazioni.getitemnumber(1, "backcolor" )
	
	closewithreturn( parent, l_s_cs_graph_settings )
	
else
	g_mb.messagebox("CS_TEAM", "E' necessario specificare un numero per il colore di sfondo!", Exclamation!)
end if
end event

type dw_impostazioni from datawindow within w_cs_graph_impostazioni
integer x = 14
integer y = 16
integer width = 983
integer height = 1868
integer taborder = 10
string title = "none"
string dataobject = "d_cs_graph_settings"
boolean border = false
boolean livescroll = true
end type

