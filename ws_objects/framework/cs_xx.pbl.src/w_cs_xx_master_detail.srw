﻿$PBExportHeader$w_cs_xx_master_detail.srw
forward
global type w_cs_xx_master_detail from w_cs_xx_principale
end type
type tab_1 from tab within w_cs_xx_master_detail
end type
type lista from userobject within tab_1
end type
type dw_lista from uo_cs_xx_dw within lista
end type
type lista from userobject within tab_1
dw_lista dw_lista
end type
type ricerca from userobject within tab_1
end type
type dw_ricerca from datawindow within ricerca
end type
type ricerca from userobject within tab_1
dw_ricerca dw_ricerca
end type
type tab_1 from tab within w_cs_xx_master_detail
lista lista
ricerca ricerca
end type
end forward

global type w_cs_xx_master_detail from w_cs_xx_principale
integer width = 2789
integer height = 1808
tab_1 tab_1
end type
global w_cs_xx_master_detail w_cs_xx_master_detail

type variables
protected:
	// Sql base della lista, sarà aggiunto la where in un secondo momento
	string is_sql_base
end variables

forward prototypes
public subroutine wf_esegui_ricerca ()
public function boolean wf_imposta_where_ricerca (string as_sql, ref string as_where)
end prototypes

public subroutine wf_esegui_ricerca ();/**
 * stefanop
 * 06/06/2014
 *
 * Funzione che viene chiamata quando viene premuto il pulsante di ricerca
 * dalla datawindow di ricerca
 **/
 
tab_1.ricerca.dw_ricerca.accepttext()
tab_1.lista.dw_lista.change_dw_current()
postevent("pc_retrieve")
end subroutine

public function boolean wf_imposta_where_ricerca (string as_sql, ref string as_where);/** 
 * stefanop
 * 06/06/2014
 *
 * Crea la where da utilizzare nella ricerca della lista.
 *
 * Il ritorno può essere true per continuare oppure false se si vuole bloccare tutto.
 **/
 
return true
end function

on w_cs_xx_master_detail.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_cs_xx_master_detail.destroy
call super::destroy
destroy(this.tab_1)
end on

event resize;/**
 * stefanop
 * 06/06/2014
 *
 * Rimosso resize standard
 **/
 
tab_1.width = newwidth - 60

tab_1.event ue_resize()
end event

event pc_setwindow;call super::pc_setwindow;

// Tab
tab_1.selectedtab = 2

// Tab Ricerca
tab_1.ricerca.dw_ricerca.insertrow(0)

// Tab Lista
tab_1.lista.dw_lista.set_dw_options(sqlca, pcca.null_object, c_noretrieveonopen, c_default)
is_sql_base = tab_1.lista.dw_lista.getsqlselect()
end event

type tab_1 from tab within w_cs_xx_master_detail
event ue_resize ( )
integer x = 23
integer y = 20
integer width = 2697
integer height = 740
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
tabposition tabposition = tabsonleft!
integer selectedtab = 1
lista lista
ricerca ricerca
end type

event ue_resize();//lista.resize(newwidth, newheight)

lista.dw_lista.resize(lista.width, lista.height)
ricerca.dw_ricerca.resize(ricerca.width, ricerca.height)

end event

on tab_1.create
this.lista=create lista
this.ricerca=create ricerca
this.Control[]={this.lista,&
this.ricerca}
end on

on tab_1.destroy
destroy(this.lista)
destroy(this.ricerca)
end on

type lista from userobject within tab_1
integer x = 133
integer y = 16
integer width = 2546
integer height = 708
long backcolor = 12632256
string text = "Lista"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_lista dw_lista
end type

on lista.create
this.dw_lista=create dw_lista
this.Control[]={this.dw_lista}
end on

on lista.destroy
destroy(this.dw_lista)
end on

type dw_lista from uo_cs_xx_dw within lista
integer x = 27
integer y = 24
integer width = 2469
integer height = 660
integer taborder = 30
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;/**
 * stefanop
 * 06/06/2014
 *
 * Eseguo ricerca
 **/
 
string ls_where

setnull(ls_where)

if wf_imposta_where_ricerca(is_sql_base, ref ls_where)  then
	
	if isnull(ls_where) then
		ls_where = ""
	end if
	
	setsqlselect(is_sql_base + ls_where)
	
	if retrieve() < 0 then
		pcca.error = c_fatal
	else
		tab_1.selectedtab = 1
	end if
	
end if
end event

type ricerca from userobject within tab_1
integer x = 133
integer y = 16
integer width = 2546
integer height = 708
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_ricerca dw_ricerca
end type

on ricerca.create
this.dw_ricerca=create dw_ricerca
this.Control[]={this.dw_ricerca}
end on

on ricerca.destroy
destroy(this.dw_ricerca)
end on

type dw_ricerca from datawindow within ricerca
integer x = 27
integer y = 24
integer width = 2491
integer height = 660
integer taborder = 20
string title = "none"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;choose case dwo.name
		
	case "b_cerca", "b_ricerca"
		wf_esegui_ricerca()
		
end choose
end event

