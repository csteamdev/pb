﻿$PBExportHeader$w_cs_xx_mdi.srw
$PBExportComments$Finestra MDI Standard (da ereditare)
forward
global type w_cs_xx_mdi from w_mdi_frame
end type
end forward

global type w_cs_xx_mdi from w_mdi_frame
integer width = 3323
integer height = 2048
boolean hscrollbar = true
boolean vscrollbar = true
long backcolor = 268435456
toolbaralignment toolbaralignment = alignatbottom!
windowanimationstyle openanimation = centeranimation!
windowanimationstyle closeanimation = centeranimation!
event refresh ( )
end type
global w_cs_xx_mdi w_cs_xx_mdi

type variables
public:
	m_cs_xx im_menu_cs
end variables

forward prototypes
public subroutine wf_imposta_menu ()
end prototypes

event refresh();/**
 * stefanop
 * 24/12/2010
 *
 * Rinfresco la finestra
 **/
 
yield()
end event

public subroutine wf_imposta_menu ();/// stefanop: 21/09/2010: creo il menu solo se serve non mille volte, chissà che memory leak faceva!!!
if not isvalid(im_menu_cs) then
	im_menu_cs = menuid
	im_menu_cs.mf_imposta_menu_mdi()
end if
end subroutine

event pc_setwindow;call super::pc_setwindow;wf_imposta_menu()
end event

on w_cs_xx_mdi.create
call super::create
end on

on w_cs_xx_mdi.destroy
call super::destroy
if IsValid(MenuID) then destroy(MenuID)
end on

event pc_mdiactive;call super::pc_mdiactive;wf_imposta_menu()
end event

event timer;call super::timer;if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	string ls_sessione
	
	select sessione
	into   :ls_sessione
	from   utenti
	where  cod_utente = :s_cs_xx.cod_utente;
	
	if gs_licenze = "A2" and (isnull(ls_sessione) or ls_sessione <> gs_sessione) then
		
		g_mb.messagebox("Framework","Sessione Annullata: contattare l'amministratore del sistema.",StopSign!)
		gs_licenze = "A0"
		close(this)
		
	end if
end if
end event

event resize;call super::resize;// stefanp 10/09/2010: sposto il pulsante CS
if isvalid(w_menu_button) then
	w_menu_button.postevent("ue_resize")
end if

// stefanp 10/09/2010: sposto il pulsante CS
if isvalid(w_extra_windows) then
	w_extra_windows.postevent("ue_resize")
end if

if isvalid(w_menu_80) then
	w_menu_80.postevent("ue_imposta_window")
end if
end event

event open;call super::open;w_cs_xx_mdi.mdi_1.backcolor = s_themes.theme[s_themes.current_theme].mdi_backcolor
end event

event activate;call super::activate;// stefanop: 24/08/2011, se la login è aperta allora la porto sempre in primo piano.
if isvalid(w_login) then
	w_login.show()
end if
end event

