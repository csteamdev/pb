﻿$PBExportHeader$w_cs_xx_principale.srw
$PBExportComments$Finestra Principale Standard (da ereditare)
forward
global type w_cs_xx_principale from w_main
end type
end forward

global type w_cs_xx_principale from w_main
integer width = 2030
integer height = 1444
event ue_trova pbm_custom01
event ue_anteprima pbm_custom02
event ue_imp_pagina pbm_custom03
event ue_commit ( )
event ue_imposta_text ( )
event ue_anteprima_pdf pbm_custom02
event ue_layout_params ( )
event ue_custom_color ( )
end type
global w_cs_xx_principale w_cs_xx_principale

type variables
boolean ib_prima_riga = true
string is_nuovo, is_modifica, is_cancella, is_visualizza
string is_nome_colonna[], &
         is_nome_tabella[], is_nome_codice[], &
         is_nome_descrizione[], is_selezione[]
datawindow is_nome_datawindow[]
m_cs_xx im_cs_xx
uo_dw_main iuo_dw_main
end variables

forward prototypes
public subroutine wf_imposta_menu ()
public subroutine wf_centra ()
public subroutine wf_imposta_text ()
public subroutine wf_imposta_text (string fs_cod_lingua)
public function integer wf_layout_params ()
public function integer wf_layout_params_dw (ref datawindow fdw_objects)
end prototypes

on ue_trova;call w_main::ue_trova;if isvalid(pcca.window_currentdw) then
   pcca.window_currentdw.triggerevent("ue_trova")
end if

end on

on ue_anteprima;call w_main::ue_anteprima;if isvalid(pcca.window_currentdw) then
   pcca.window_currentdw.triggerevent("ue_anteprima")
end if

end on

on ue_imp_pagina;call w_main::ue_imp_pagina;if isvalid(pcca.window_currentdw) then
   pcca.window_currentdw.triggerevent("ue_imp_pagina")
end if

end on

event ue_commit;call super::ue_commit;commit using sqlca;
end event

event ue_imposta_text();wf_imposta_text()
end event

event ue_anteprima_pdf;if isvalid(pcca.window_currentdw) then
   pcca.window_currentdw.triggerevent("ue_anteprima_pdf")
end if

end event

event ue_layout_params();/*
EnMe 8/7/2008
Aggiunto per parametrizzare alcune opzioni di natura visuale.
*/

wf_layout_params( )
end event

event ue_custom_color();/**
* stefanop
* 04/10/2010
*
* Evento da codificare nel caso ci siano oggetti che richiedono un colore specifico non derivato dal tema corrente
**/

end event

public subroutine wf_imposta_menu ();im_cs_xx.mf_imposta_menu_principale()

if not s_cs_xx.admin then
	
	if not s_cs_xx.flag_nuovo then
		im_cs_xx.m_edit.m_new.enabled = false
	end if
	
	if not s_cs_xx.flag_modifica then
		im_cs_xx.m_edit.m_modify.enabled = false
	end if
	
	if not s_cs_xx.flag_cancella then
		im_cs_xx.m_edit.m_delete.enabled = false
	end if
	
end if


end subroutine

public subroutine wf_centra ();long ll_larghezza, ll_altezza, ll_x, ll_y

// cambiato perchè col nuovo menu le dimensioni del workspace MDI (w_cs_xx_mdi.mdi_1) potrebbero non
// corrispondere più alle dimensioni dell'area client della maschera base MDI (w_cs_xx_mdi.workspace)

//------------------
	//ll_larghezza = w_cs_xx_mdi.workspacewidth()  - w_cs_xx_mdi.workspacex()
	//ll_altezza   = w_cs_xx_mdi.workspaceheight() - w_cs_xx_mdi.workspacey() - w_cs_xx_mdi.mdi_1.microhelpheight
//------------------
ll_larghezza = w_cs_xx_mdi.mdi_1.width//  - w_cs_xx_mdi.mdi_1.x
ll_altezza   = w_cs_xx_mdi.mdi_1.height// - w_cs_xx_mdi.mdi_1.y - w_cs_xx_mdi.mdi_1.microhelpheight

ll_x = (ll_larghezza - this.width) / 2
ll_y = (ll_altezza - this.height) / 2

if ll_x < 0 then ll_x = 0
if ll_y < 0 then ll_y = 0

this.move(ll_x,ll_y)
end subroutine

public subroutine wf_imposta_text ();string ls_cod_lingua

if s_cs_xx.cod_lingua_applicazione = "" then setnull(s_cs_xx.cod_lingua_applicazione)
if s_cs_xx.cod_lingua_window = "" then setnull(s_cs_xx.cod_lingua_window)

if not isnull(s_cs_xx.cod_lingua_window) then
	wf_imposta_text(s_cs_xx.cod_lingua_window)
else
	wf_imposta_text(s_cs_xx.cod_lingua_applicazione)
end if	
end subroutine

public subroutine wf_imposta_text (string fs_cod_lingua);string ls_modify, ls_nome_window, ls_nome_oggetto, ls_testo, ls_sql
long   ll_i, ll_control
checkbox lch_1
commandbutton lcb_1
radiobutton lrb_1
statictext lst_1
groupbox lgb_1

ls_nome_window = this.classname()

ll_control = upperbound(control)

declare cu_wtext dynamic cursor for SQLSA;
ls_sql = 	" select nome_oggetto, testo from tab_w_text_repository " + &
         	" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and" + &
         	" nome_window = '" + ls_nome_window + "' and " + &
			" testo is not null and "
if isnull(fs_cod_lingua) then
	ls_sql += " cod_lingua is null "
else
	ls_sql += " cod_lingua = '" + fs_cod_lingua + "' "
end if

PREPARE SQLSA FROM :ls_sql ;

open dynamic cu_wtext;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Framework", "Errore nell'applicazione delle impostazioni internazionali~r~n"+sqlca.sqlerrtext)
	return
end if

do while true
	fetch cu_wtext into :ls_nome_oggetto, :ls_testo;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Framework", "Errore nell'applicazione delle impostazioni internazionali~r~n"+sqlca.sqlerrtext)
		exit
	end if
	if sqlca.sqlcode = 100 then exit
	
	for ll_i = 1 to ll_control
		
		if ls_nome_oggetto = "this.title" then
			this.title = ls_testo
			continue
		end if
		
		if ls_nome_oggetto = control[ll_i].classname() then
			choose case control[ll_i].typeof()
				case checkbox! 
					lch_1 = control[ll_i]
					lch_1.text = ls_testo
					
				case commandbutton!
					lcb_1 = control[ll_i]
					lcb_1.text = ls_testo
					
				case radiobutton!   
					lrb_1 = control[ll_i]
					lrb_1.text = ls_testo

				case statictext! 
					lst_1 = control[ll_i]
					lst_1.text = ls_testo
					
				case groupbox!
					lgb_1 = control[ll_i]
					lgb_1.text = ls_testo
					
				case else
					continue
						
			end choose
			
		end if
		
	next
	
loop

close cu_wtext;

return

end subroutine

public function integer wf_layout_params ();int li_j
long 		ll_controls,ll_i

commandbutton l_commandbutton
checkbox      l_checkbox
radiobutton   l_radiobutton
statictext    l_statictext
groupbox      l_groupbox
statichyperlink l_statichyperlink
tab l_tab


ll_controls = upperbound(this.control)

for ll_i = 1 to ll_controls
				
	choose case this.control[ll_i].typeof()
			
		case checkbox! 
			l_checkbox = this.control[ll_i]
			l_checkbox.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
		case commandbutton!
			l_commandbutton = this.control[ll_i]
			l_commandbutton.text = upper(l_commandbutton.text)
			l_commandbutton.facename = 'Arial'
			l_commandbutton.textsize = -8
			l_commandbutton.weight = 400
			
		case radiobutton!   
			l_radiobutton = this.control[ll_i]
			l_radiobutton.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
		case statictext! 
			l_statictext = this.control[ll_i]
			l_statictext.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
		case groupbox!
			l_groupbox = this.control[ll_i]
			l_groupbox.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
		case statichyperlink!
			l_statichyperlink = this.control[ll_i]
			l_statichyperlink.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
		case tab!
			l_tab =  this.control[ll_i]
			l_tab.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
			// imposto colore di sfondo delle pagine
			for li_j = 1 to upperbound(l_tab.control)
				l_tab.control[li_j].backcolor =  s_themes.theme[s_themes.current_theme].f_tab_color
			next
			
		case else
			
			continue
				
	end choose

next

return 0

end function

public function integer wf_layout_params_dw (ref datawindow fdw_objects);string	ls_objects, ls_oggetto,ls_type, ls_modify
long 		ll_pos, ll_color

ls_objects = fdw_objects.Object.DataWindow.Objects
ls_modify = ""

do while true
	ll_pos = pos(ls_objects, "~t", 1)
	
	if ll_pos = 0 then 
		ls_oggetto = ls_objects
	else
		ls_oggetto = left(ls_objects, ll_pos - 1)
	end if

	ls_type = fdw_objects.Describe(ls_oggetto + ".type")
	
	choose case ls_type
		case "column"
			ll_color = rgb(255,255,0)
			ls_modify += ls_oggetto + ".Background.Color='"+string(ll_color)+"'~t"
		
		case "text"
		
			ls_modify += ls_oggetto + ".font.weight='400'~t"
			ls_modify += ls_oggetto + ".font.Height='-12'~t"
			
			ll_color = rgb(255,255,0)
			ls_modify += ls_oggetto + ".Color='"+string(ll_color)+"'~t"

	end choose

	ls_objects = mid(ls_objects, ll_pos + 1)
	
	if len(trim(ls_objects)) < 1 or ll_pos = 0 then exit
loop

fdw_objects.modify(ls_modify)

return 0
end function

event open;call super::open;wf_centra()

if isvalid(iuo_dw_main) and ib_prima_riga then
   iuo_dw_main.change_dw_current()
   iuo_dw_main.i_ScrollDW = iuo_dw_main
   iuo_dw_main.postevent("pcd_first")
end if
end event

event deactivate;call super::deactivate;//f_azzera_topic()


end event

event activate;call super::activate;string ls_nome_window, ls_nome_datawindow, ls_nome_colonna
integer li_i

wf_imposta_menu()

ls_nome_window = i_classname
ls_nome_datawindow = " "
ls_nome_colonna = " "
//f_help(ls_nome_window, ls_nome_datawindow, ls_nome_colonna)

for li_i = 1 to upperbound(is_nome_colonna)
   if not isnull(is_nome_colonna[li_i]) and &
      not isnull(is_nome_tabella[li_i]) and &
      trim(is_nome_tabella[li_i]) <> "" and &
      not isnull(is_nome_codice[li_i]) and &
      trim(is_nome_codice[li_i]) <> "" and &
      not isnull(is_nome_descrizione[li_i]) and &
      trim(is_nome_descrizione[li_i]) <> "" then
		
		//--------- 15/04/2004 Michele X problemi con gestioni multiaziendali ------------
		if isnull(is_selezione[li_i]) then
			is_selezione[li_i] = "cod_azienda = '" + s_cs_xx.cod_azienda + "'"
		else
			is_selezione[li_i] = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + is_selezione[li_i]
		end if
		//--------------------------------------------------------------------------------
		
      f_po_loaddddw_dw(is_nome_datawindow[li_i], &
                       is_nome_colonna[li_i], &
                       sqlca, &
                       is_nome_tabella[li_i], &
                       is_nome_codice[li_i], &
                       is_nome_descrizione[li_i], &
                       is_selezione[li_i])
							  
      setnull(is_nome_colonna[li_i])
      setnull(is_nome_datawindow[li_i])
      setnull(is_nome_tabella[li_i])
      setnull(is_nome_codice[li_i])
      setnull(is_nome_descrizione[li_i])
      setnull(is_selezione[li_i])
		
   end if
next
end event

on pc_previous;if isvalid(iuo_dw_main) then
   iuo_dw_main.change_dw_current()
end if

call super::pc_previous
end on

on pc_new;if isvalid(iuo_dw_main) then
   iuo_dw_main.change_dw_current()
end if

call super::pc_new
end on

event pc_setwindow;call super::pc_setwindow;unsignedlong lul_control_world
string ls_nome_window, ls_cod_voce


im_cs_xx = menuid
if not isvalid(im_cs_xx) then im_cs_xx = pcca.mdi_frame.menuid

if not s_cs_xx.admin then
	
	lul_control_world = 0
	
	if not s_cs_xx.flag_nuovo then
		lul_control_world = lul_control_world + c_noenablenewonopen + c_nonew
	end if
	
	if not s_cs_xx.flag_modifica then
		lul_control_world = lul_control_world + c_noenablemodifyonopen + c_nomodify
	end if
	
	if not s_cs_xx.flag_cancella then
		lul_control_world = lul_control_world + c_nodelete
	end if
	
	s_cs_xx.parametri.parametro_ul_1 = lul_control_world
	
end if
	
wf_imposta_menu()

postevent("ue_imposta_text")

postevent("ue_layout_params")

postevent("ue_commit")

postevent("ue_custom_color")

end event

on w_cs_xx_principale.create
call super::create
end on

on w_cs_xx_principale.destroy
call super::destroy
end on

on pc_modify;if isvalid(iuo_dw_main) then
   iuo_dw_main.change_dw_current()
end if

call super::pc_modify
end on

on pc_view;if isvalid(iuo_dw_main) then
   iuo_dw_main.change_dw_current()
end if

call super::pc_view
end on

on pc_first;if isvalid(iuo_dw_main) then
   iuo_dw_main.change_dw_current()
end if

call super::pc_first
end on

on pc_last;if isvalid(iuo_dw_main) then
   iuo_dw_main.change_dw_current()
end if

call super::pc_last
end on

on pc_next;if isvalid(iuo_dw_main) then
   iuo_dw_main.change_dw_current()
end if

call super::pc_next
end on

event close;call super::close;postevent("ue_commit")
end event

event pc_print;/*
	in questo evento il flag EXTEND ANCESTOR SCRIPT va lasciato DISATTIVATO
*/

boolean lb_procedi = false

if isvalid(PCCA.Window_CurrentDW) then
	
	s_cs_xx.parametri.parametro_uo_dw_1 = PCCA.Window_CurrentDW
	
	window_open(w_stampa_dw,0)
	
	lb_procedi = s_cs_xx.parametri.parametro_b_1
	
	setnull(s_cs_xx.parametri.parametro_b_1)
	
	if lb_procedi then
		call w_main::pc_print
	end if
	
end if
end event

event systemkey;call super::systemkey;if key=keyF1! then
	uo_web luo_web
	luo_web.openwebpage("www.gmail.com")
end if


end event

