﻿$PBExportHeader$w_cs_xx_risposta.srw
$PBExportComments$Finestra Risposta Standard (da ereditare)
forward
global type w_cs_xx_risposta from w_response
end type
end forward

global type w_cs_xx_risposta from w_response
integer width = 2167
integer height = 1388
boolean center = true
event ue_imposta_text ( )
event ue_layout_params ( )
event ue_custom_color ( )
end type
global w_cs_xx_risposta w_cs_xx_risposta

forward prototypes
public subroutine wf_imposta_text (string fs_cod_lingua)
public subroutine wf_imposta_text ()
public function integer wf_layout_params ()
end prototypes

event ue_imposta_text();wf_imposta_text()
end event

event ue_layout_params();/*
EnMe 8/7/2008
Aggiunto per parametrizzare alcune opzioni di natura visuale.
*/

wf_layout_params( )
end event

event ue_custom_color();/**
 * stefanop
 * 04/10/2010
 *
 * Evento da codificare in caso di siano componenti che devono avere un determinato colore che non dipende dal tema corrente
 **/
end event

public subroutine wf_imposta_text (string fs_cod_lingua);string ls_modify, ls_nome_window, ls_nome_oggetto, ls_testo, ls_sql
long   ll_i, ll_control
checkbox lch_1
commandbutton lcb_1
radiobutton lrb_1
statictext lst_1
groupbox lgb_1

ls_nome_window = this.classname()

ll_control = upperbound(control)

declare cu_wtext dynamic cursor for SQLSA;
ls_sql = " select nome_oggetto, testo from tab_w_text_repository " + &
         " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and" + &
         " nome_window = '" + ls_nome_window + "' and " + &
			" testo is not null and "
if isnull(fs_cod_lingua) then
	ls_sql += " cod_lingua is null "
else
	ls_sql += " cod_lingua = '" + fs_cod_lingua + "' "
end if

PREPARE SQLSA FROM :ls_sql ;

open dynamic cu_wtext;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Framework", "Errore nell'applicazione delle impostazioni internazionali~r~n"+sqlca.sqlerrtext)
	return
end if

do while true
	fetch cu_wtext into :ls_nome_oggetto, :ls_testo;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Framework", "Errore nell'applicazione delle impostazioni internazionali~r~n"+sqlca.sqlerrtext)
		exit
	end if
	if sqlca.sqlcode = 100 then exit
	
	for ll_i = 1 to ll_control
		
		if ls_nome_oggetto = "this.title" then
			this.title = ls_testo
			continue
		end if
		
		if ls_nome_oggetto = control[ll_i].classname() then
			choose case control[ll_i].typeof()
				case checkbox! 
					lch_1 = control[ll_i]
					lch_1.text = ls_testo
					
				case commandbutton!
					lcb_1 = control[ll_i]
					lcb_1.text = ls_testo
					
				case radiobutton!   
					lrb_1 = control[ll_i]
					lrb_1.text = ls_testo

				case statictext! 
					lst_1 = control[ll_i]
					lst_1.text = ls_testo
					
				case groupbox!
					lgb_1 = control[ll_i]
					lgb_1.text = ls_testo
					
				case else
					continue
						
			end choose
			
		end if
		
	next
	
loop

close cu_wtext;

return

end subroutine

public subroutine wf_imposta_text ();string ls_cod_lingua

if s_cs_xx.cod_lingua_applicazione = "" then setnull(s_cs_xx.cod_lingua_applicazione)
if s_cs_xx.cod_lingua_window = "" then setnull(s_cs_xx.cod_lingua_window)

if not isnull(s_cs_xx.cod_lingua_window) then
	wf_imposta_text(s_cs_xx.cod_lingua_window)
else
	wf_imposta_text(s_cs_xx.cod_lingua_applicazione)
end if
end subroutine

public function integer wf_layout_params ();int li_j
long 		ll_controls,ll_i

commandbutton l_commandbutton
checkbox      l_checkbox
radiobutton   l_radiobutton
statictext    l_statictext
groupbox      l_groupbox
statichyperlink l_statichyperlink
tab l_tab

ll_controls = upperbound(this.control)

for ll_i = 1 to ll_controls
		
	choose case this.control[ll_i].typeof()
			
		case checkbox! 
			l_checkbox = this.control[ll_i]
			l_checkbox.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
		case commandbutton!
			boolean lb_default, lb_cancel
			
			l_commandbutton = this.control[ll_i]
			lb_default = l_commandbutton.default
			lb_cancel  = l_commandbutton.cancel
			l_commandbutton.text = upper(l_commandbutton.text)
			l_commandbutton.facename = 'Arial'
			l_commandbutton.textsize = -8
			l_commandbutton.weight = 400
			//l_commandbutton.FlatStyle = TRUE	
			l_commandbutton.default = lb_default				
			
		case radiobutton!   
			l_radiobutton = this.control[ll_i]
			l_radiobutton.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
		case statictext! 
			l_statictext = this.control[ll_i]
			l_statictext.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
		case groupbox!
			l_groupbox = this.control[ll_i]
			l_groupbox.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
		case statichyperlink!
			l_statichyperlink = this.control[ll_i]
			l_statichyperlink.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor
		
		case tab!
			l_tab =  this.control[ll_i]
			l_tab.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
			// imposto colore di sfondo delle pagine
			for li_j = 1 to upperbound(l_tab.control)
				l_tab.control[li_j].backcolor =  s_themes.theme[s_themes.current_theme].f_tab_color
			next
			
		case else
			
			continue
				
	end choose
next

return 0

end function

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup + c_nosaveposition)

postevent("ue_imposta_text")

postevent("ue_layout_params")

postevent("ue_custom_color")

setredraw( true )
end event

on w_cs_xx_risposta.create
call super::create
end on

on w_cs_xx_risposta.destroy
call super::destroy
end on

event open;call super::open;integer li_larghezza,li_altezza

li_larghezza = w_cs_xx_mdi.WorkSpaceWidth ( )
li_altezza = w_cs_xx_mdi.WorkSpaceheight ( )

this.move((li_larghezza - this.width)/2,(li_altezza - this.height)/2)

// modifica per la centratura automatica senza PClass 
// Autore: Diego Ferrari
// data: 29/07/1999
end event

event key;call super::key;choose case key
	case KeyF12!
		if  keyflags = 3 then
			s_cs_xx.parametri.tipo_oggetto = "datawindow"
			s_cs_xx.parametri.nome_oggetto = pcca.window_currentdw.dataobject
			open(w_imposta_etichette_lingue)
		else
			string ls_nome_window
			ls_nome_window = pcca.window_current.classname()
			g_mb.messagebox("Framework","Il nome della window è: " + ls_nome_window,information!)
		end if
	case KeyF11!
		if  keyflags = 3 then
			s_cs_xx.parametri.tipo_oggetto = "window"
			s_cs_xx.parametri.nome_oggetto = pcca.window_current.classname()
			s_cs_xx.parametri.parametro_w_cs_xx_1 = pcca.window_current
			open(w_imposta_etichette_lingue)
		end if
end choose

end event

