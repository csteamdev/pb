﻿$PBExportHeader$w_cs_xx_treeview.srw
forward
global type w_cs_xx_treeview from w_cs_xx_principale
end type
type tab_dettaglio from tab within w_cs_xx_treeview
end type
type det_1 from userobject within tab_dettaglio
end type
type det_1 from userobject within tab_dettaglio
end type
type tab_dettaglio from tab within w_cs_xx_treeview
det_1 det_1
end type
type tab_ricerca from tab within w_cs_xx_treeview
end type
type ricerca from userobject within tab_ricerca
end type
type dw_ricerca from uo_std_dw within ricerca
end type
type ricerca from userobject within tab_ricerca
dw_ricerca dw_ricerca
end type
type selezione from userobject within tab_ricerca
end type
type tv_selezione from treeview within selezione
end type
type selezione from userobject within tab_ricerca
tv_selezione tv_selezione
end type
type tab_ricerca from tab within w_cs_xx_treeview
ricerca ricerca
selezione selezione
end type
end forward

global type w_cs_xx_treeview from w_cs_xx_principale
integer width = 3872
integer height = 1948
boolean center = true
event we_set_dw_position ( )
event ue_valori_ricerca_predefiniti ( )
event ue_imposta_valori_livelli ( )
tab_dettaglio tab_dettaglio
tab_ricerca tab_ricerca
end type
global w_cs_xx_treeview w_cs_xx_treeview

type variables
protected:
	constant int ii_tab_width_offset = 40
	
	// condizioni di filtro impostati nella dw_ricerca da usare in tutte le query
	string is_sql_filtro
	
	// indica l'ultimo handle selezionato nell'albero
	long il_current_handle = -1
	
	str_treeview istr_data
	
	// Indica il numero massimo di elementi da visulizzare nell'albero
	// !!! deve essere codificato dal programmatore il controllo !!!
	long il_max_treeview_results = 500
	
	// Indica se il selection changed del treeview deve essere effettuato o meno
	// Da codificare nell'evento updateend della DW main per permettere l'inserimento
	// del nuovo item nell'albero e la sua selezione senza far scattare la retrieve della DW
	//
	// Ricordarsi di valorizzare la struttura istr_data con la struttura locale usata per 
	// l'inserimento del singolo nodo (cioè quando si fa new)
	// esempio: w_tes_bol_ven::wf_inserisci_bolla_vendita
	boolean ib_fire_selection_changed = true
	
private:
	boolean ib_clear_tree = false
	
	// indica il numero di colonne di tipo livello_ presente nella dw di ricerca
	int ii_numero_livelli_dw = 0
	
	string is_valori_livelli[]
	
	int ICON_DELETED_ITEM
	
public:
	// codice del filtro per il salvataggio dei valori di ricerca
	string is_codice_filtro = 'XXX'

end variables

forward prototypes
public subroutine wf_treeview_icons ()
public subroutine wf_imposta_ricerca ()
public function long wf_leggi_livello (long al_handle, integer ai_livello)
private subroutine wf_imposta_valori_livelli ()
public function treeviewitem wf_new_item (boolean ab_children, integer ai_icon)
public function string wf_get_valore_livello (integer ai_livello)
public function boolean wf_is_clear ()
private subroutine wf_valore_livelli_taborder (string as_column, string as_data)
private subroutine wf_treeview_clear ()
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function powerobject getwindow ()
protected function integer wf_treeview_add_icon (string as_file)
public subroutine wf_add_valore_livello (string as_des_livello, string as_flag_livelllo)
public subroutine wf_valori_livelli ()
public function boolean wf_has_livello (string as_flag_livello)
private subroutine wf_valore_livelli_taborder ()
public function boolean wf_is_valid (long new_handle, long old_handle)
public function boolean wf_is_valid (long new_handle)
public function long wf_get_current_handle ()
public subroutine wf_set_deleted_item_status ()
public function long wf_delete_treeview_children (long al_handle)
private subroutine wf_imposta_icone_ricerca ()
public function treeviewitem wf_new_item (boolean ab_children, integer ai_icon, integer ai_icon_selected)
public subroutine wf_check_limit (ref long ll_find_rows)
public subroutine wf_popola_item (long al_handle)
protected subroutine wf_treeview_search ()
end prototypes

event we_set_dw_position();/**
 * stefanop
 * 28/10/2011
 *
 * Controllo se ci sono datawindow all'interno dei tab (99% dei casi SI) e le posiziono alle coordinate 0,0
 **/
 
 
int li_i, li_j

// scorro i tab
for li_i = 1 to upperbound(tab_dettaglio.control)

	// scorro gli elementi del tabpage
	for li_j = 1 to upperbound(tab_dettaglio.control[li_i].control)
		if tab_dettaglio.control[li_i].control[li_j].typeof() = DataWindow! then
			 tab_dettaglio.control[li_i].control[li_j].move(0,0)
		end if
	next 	
	
next
end event

event ue_valori_ricerca_predefiniti();// imposto valori filtro predefinito
guo_functions.uof_leggi_filtro(tab_ricerca.ricerca.dw_ricerca, is_codice_filtro)
wf_valore_livelli_taborder()
end event

event ue_imposta_valori_livelli();wf_imposta_valori_livelli()
end event

public subroutine wf_treeview_icons ();/**
 * stefanop
 * 28/10/2011
 *
 * Codificare le icone all'interno di questo evento.
 * Creare sempre una variabile di istanza intera che contiene il numero dell'icona
 * da usare nell'albero!
 * NON passare mai alla funzione il percorso delle risorse perchè lo fa da sola
 **/
 
 
 /**
ESEMPIO


//1 tipo offerta
TIPO_OFF_ICON = wf_treeview_add_icon("treeview\tipo_documento.png")
 **/
end subroutine

public subroutine wf_imposta_ricerca ();/**
 * stefanop
 * 28/10/2011
 *
 * Imposto il filtro di ricerca globale, pulisco l'albero e recupero i dati
 **/
 
 
end subroutine

public function long wf_leggi_livello (long al_handle, integer ai_livello);/**
 * stefanop
 * 28/10/2011
 *
 * Leggeggere il valore del livello corrente ed impostare in maniera corretta la ricerca
 **/
 
/*
il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "T"
		return wf_inserisci_tipo_nc(al_handle)
				
	case else
		return wf_inserisci_ordini_acquisti(al_handle)
		
end choose
*/

return 0
end function

private subroutine wf_imposta_valori_livelli ();/**
 * stefanop
 * 28/10/2011
 *
 * Imposto il valore dei livelli all'interno della dw di ricerca.
 * La funzione controlla se son preseni colonne del tipo
 * livello_[ID] e in quel caso inietta in maniera automatica i 
 * valori.
 **/
 
string ls_colname, ls_valore_livelli, ls_empty[]
int li_i

// pulisco array per sicurezza
is_valori_livelli = ls_empty

// carico i valori inseriri dall'utente
wf_valori_livelli()

ls_valore_livelli = guo_functions.uof_implode(is_valori_livelli, "/")
ii_numero_livelli_dw = 0

for li_i = 1 to integer(tab_ricerca.ricerca.dw_ricerca.describe("DataWindow.Column.Count"))
	
	ls_colname = tab_ricerca.ricerca.dw_ricerca.describe("#" + string(li_i) + ".Name")
	
	if left(ls_colname, 8) = "livello_" then
		
		tab_ricerca.ricerca.dw_ricerca.modify(ls_colname + ".Values='" + ls_valore_livelli + "'")
		tab_ricerca.ricerca.dw_ricerca.modify(ls_colname + ".ddlb.VScrollBar='Yes'")
		//tab_ricerca.ricerca.dw_ricerca.modify(ls_colname + ".ddlb.sorted=yes")
		
		ii_numero_livelli_dw++
		
	end if
	
next
end subroutine

public function treeviewitem wf_new_item (boolean ab_children, integer ai_icon);/**
 * stefanop
 * 28/10/2011
 * 
 * centralizzo la creazione dei nuovi treeviewitem
 **/

return wf_new_item(ab_children, ai_icon, ai_icon)
end function

public function string wf_get_valore_livello (integer ai_livello);/**
 * Recupera il valore del livello passato
 * Se il livello non esiste ritorna N
 **/
 
 
if ai_livello <= ii_numero_livelli_dw then
	return tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "livello_" + string(ai_livello))
else
	return "N"
end if
end function

public function boolean wf_is_clear ();/**
 * !!! DEPRECATED !!! 
 * usare wf_is_valid() 
 **/

return ib_clear_tree
end function

private subroutine wf_valore_livelli_taborder (string as_column, string as_data);/**
 * stefanop
 * 28/10/2011
 *
 * Centralizzo la gestione dei livelli e ne gestisco il taborder
 **/
 
int li_i, li_livello

li_livello = integer(mid(as_column, 9)) + 1


for li_i = li_livello to ii_numero_livelli_dw
	
	tab_ricerca.ricerca.dw_ricerca.setitem(1, "livello_" + string(li_i), "N")
	tab_ricerca.ricerca.dw_ricerca.settaborder("livello_" + string(li_i), 0)
	
next

if li_livello=1 then 
	tab_ricerca.ricerca.dw_ricerca.settaborder("livello_" + string(li_livello), 5000 + li_livello)

elseif not isnull(as_data) and as_data <> "N" and li_livello <= ii_numero_livelli_dw then 
	
	tab_ricerca.ricerca.dw_ricerca.settaborder("livello_" + string(li_livello), 5000 + li_livello)
	
end if
end subroutine

private subroutine wf_treeview_clear ();/**
 * stefanop
 * 28/10/11
 *
 * Pulisco l'albero
 **/
 
ib_clear_tree = true
tab_ricerca.selezione.tv_selezione.setredraw(false)

tab_ricerca.selezione.tv_selezione.deleteitem(0)

tab_ricerca.selezione.tv_selezione.setredraw(true)
ib_clear_tree = false
end subroutine

public function integer wf_leggi_parent (long al_handle, ref string as_sql);/**
 * stefanop
 * 31/10/2011
 *
 * Leggo il filtro dei nodi padri per poter filtrare i dati solo per quel che serve
 **/
 
 
/*
long	ll_item
treeviewitem ltv_item


if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lss_record = ltv_item.data
	
	choose case lss_record.tipo_livello		
			
		case "T"
			if isnull(lss_record.codice) or lss_record.codice = "" then
				as_sql += " AND tes_off_ven.cod_tipo_off_ven is null "
			else
				as_sql += " AND tes_off_ven.cod_tipo_off_ven = '" + lss_record.codice + "' "
			end if
			
						
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
*/

return 0
end function

public function powerobject getwindow ();/**
 * stefanop
 * 08/11/11
 *
 * Ritorna la finestra principale, utile da chiamare all'interno dei tab
 **/
 
return this
end function

protected function integer wf_treeview_add_icon (string as_file);/**
 * stefanop
 * 08/11/111
 *
 * Aggiunge un'icona al treeview e ne ritorna l'indice associato.
 * La funziona appende in automatico il percorso delle risorse
 **/
 
 return tab_ricerca.selezione.tv_selezione.addpicture(s_cs_xx.volume + s_cs_xx.risorse + as_file)
end function

public subroutine wf_add_valore_livello (string as_des_livello, string as_flag_livelllo);/**
 * stefanop
 * 11/11/11
 *
 * Aggiunge un valore a livelli della datawindow di ricerca.
 * Questa funzione va chiamata all'interno della funzione wf_valori_livelli
 **/
 
 
is_valori_livelli[upperbound(is_valori_livelli) + 1] = as_des_livello + "~t" + as_flag_livelllo
end subroutine

public subroutine wf_valori_livelli ();/**
 * stefanop
 * 28/10/2011
 *
 * Codificare qui i valori per i livelli all'interno della dw di ricerca
 **/
 
wf_add_valore_livello("Non Specificato", "N")
end subroutine

public function boolean wf_has_livello (string as_flag_livello);/**
 * stefanop
 * 11/11/11
 * 
 * Ritorna TRUE se nei livelli è impostato il valore passato per riferimento
 **/
 
int li_i

for li_i = 1 to ii_numero_livelli_dw
	if tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "livello_" + string(li_i)) = as_flag_livello then return true
next

return false
end function

private subroutine wf_valore_livelli_taborder ();/**
 * stefanop
 * 28/10/2011
 *
 * Centralizzo la gestione dei livelli e ne gestisco il taborder
 **/
 
string ls_data
int li_i

for li_i = 1 to ii_numero_livelli_dw
	//il taborder della colonna "livello_1" non deve essere mai impostato a ZERO a prescindere
	
	if li_i>1 then
		//si tratta di livello_2, livello_3, ecc ...
		//se il valore del livello precedente è uguale a "N" metti il taborderl di questo livello a ZERO
		if tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "livello_" + string(li_i - 1)) = "N" then
			//disabilita
			tab_ricerca.ricerca.dw_ricerca.settaborder("livello_" + string(li_i), 0)
		end if
	end if
next
end subroutine

public function boolean wf_is_valid (long new_handle, long old_handle);/**
 * stefnaop
 * 06/12/2011
 *
 * Controllo se tutti i valori son corretti per poter spostare il fuoco sul nuovo nodo
 * dell'albero, se non sono in cancellazione.
 **/
 

if ib_clear_tree then return false
if new_handle < 1 then return false
if new_handle = old_handle then return false
 
return true
end function

public function boolean wf_is_valid (long new_handle);/**
 * stefnaop
 * 06/12/2011
 *
 * Controllo se tutti i valori son corretti per poter spostare il fuoco sul nuovo nodo
 * dell'albero, se non sono in cancellazione.
 **/
 

return wf_is_valid(new_handle, -1)
end function

public function long wf_get_current_handle ();/**
 * giulio
 * 23/12/2011
 *
 * Ritorno l'ultimo handle selezionato nell'albero
 **/
 
return il_current_handle
end function

public subroutine wf_set_deleted_item_status ();/**
 * Giulio
 * 29/12/2011
 *
 * Azzero la struttura.
 * Inserisco l'icona di cancellazione
 * Nel treeview viene visualizzata nello stesso posto di prima l'icona più  la label "Cancellato"
 **/

treeviewitem ltvi_campo
str_treeview lstr_treeview

ltvi_campo.data = lstr_treeview

ltvi_campo.label = "Cancellato"

ltvi_campo.pictureindex = ICON_DELETED_ITEM		
ltvi_campo.selectedpictureindex = ICON_DELETED_ITEM

tab_ricerca.selezione.tv_selezione.setitem(wf_get_current_handle(), ltvi_campo)

/**
 *Codice da incorporare nell'updatestart:
 *
 if DeletedCount() > 0 then
 	wf_set_deleted_item_status()
end if
 *
 **/
end subroutine

public function long wf_delete_treeview_children (long al_handle);/**
 * stefanop
 * 27/04/2012
 *
 * Cancello tutti i figli collegati all'handle passato.
 * Ritorna il numero di figli cancellati.
 *
 * ATTENZIONE: NON usare questo metodo per pulire la treeview, usare invece wf_treeview_clear()
 **/
 
int li_deleted
long ll_children_handle

li_deleted = 0

if not isnull(al_handle) and al_handle >= 0 then
 
	ib_clear_tree = true
	tab_ricerca.selezione.tv_selezione.setredraw(false)
	
	ll_children_handle = tab_ricerca.selezione.tv_selezione.finditem(ChildTreeItem!, al_handle)
	
	do while ll_children_handle > 0 
		tab_ricerca.selezione.tv_selezione.deleteitem(ll_children_handle)
		ll_children_handle = tab_ricerca.selezione.tv_selezione.finditem(ChildTreeItem!, al_handle)
	loop
	
	tab_ricerca.selezione.tv_selezione.setredraw(true)
	ib_clear_tree = false
	
end if

return li_deleted
end function

private subroutine wf_imposta_icone_ricerca ();/**
 * stefanop
 * 22/10/2012
 *
 * Imposto le icone nella ricerca.
 * Viene usato il try catch perchè potrebbero esserci delle DW che non hanno ancora il pulsante
 * Tra qualche mese /anno è possibili rimuove i try catch e fare l'assegnazione diretta.
 **/
 
try
	tab_ricerca.ricerca.dw_ricerca.object.b_leggi_filtro.Filename  = guo_functions.uof_risorse("treeview/filter.png")
	tab_ricerca.ricerca.dw_ricerca.object.b_memorizza_filtro.Filename  = guo_functions.uof_risorse("treeview/filter-add.png")
	tab_ricerca.ricerca.dw_ricerca.object.b_cancella_filtro.Filename  = guo_functions.uof_risorse("treeview/filter-delete.png")
	
	tab_ricerca.ricerca.dw_ricerca.object.b_leggi_filtro.OriginalSize = false
	tab_ricerca.ricerca.dw_ricerca.object.b_leggi_filtro.width = 73
	tab_ricerca.ricerca.dw_ricerca.object.b_leggi_filtro.height = 64
	tab_ricerca.ricerca.dw_ricerca.object.b_memorizza_filtro.OriginalSize = false
	tab_ricerca.ricerca.dw_ricerca.object.b_memorizza_filtro.width = 73
	tab_ricerca.ricerca.dw_ricerca.object.b_memorizza_filtro.height = 64
	tab_ricerca.ricerca.dw_ricerca.object.b_cancella_filtro.OriginalSize = false
	tab_ricerca.ricerca.dw_ricerca.object.b_cancella_filtro.width = 73
	tab_ricerca.ricerca.dw_ricerca.object.b_cancella_filtro.height = 64
	
	// Imposto tooltip
	tab_ricerca.ricerca.dw_ricerca.object.b_leggi_filtro.Tooltip.Enabled = true
	tab_ricerca.ricerca.dw_ricerca.object.b_memorizza_filtro.Tooltip.Enabled = true
	tab_ricerca.ricerca.dw_ricerca.object.b_cancella_filtro.Tooltip.Enabled = true
	
	tab_ricerca.ricerca.dw_ricerca.object.b_leggi_filtro.Tooltip.Tip  = "Leggi Filtro"
	tab_ricerca.ricerca.dw_ricerca.object.b_memorizza_filtro.Tooltip.Tip = "Memorizza Filtro"
	tab_ricerca.ricerca.dw_ricerca.object.b_cancella_filtro.Tooltip.Tip = "Cancella Filtro"

	
catch (RuntimeError ex)
end try
end subroutine

public function treeviewitem wf_new_item (boolean ab_children, integer ai_icon, integer ai_icon_selected);/**
 * stefanop
 * 28/10/2011
 * 
 * centralizzo la creazione dei nuovi treeviewitem
 **/
 
treeviewitem ltvi_campo

ltvi_campo.expanded = false
ltvi_campo.selected = false
ltvi_campo.children = ab_children	//altrimenti mostrava il segno di + anche accanto agli elementi senza sottoelementi

ltvi_campo.pictureindex = ai_icon		
ltvi_campo.selectedpictureindex = ai_icon_selected		
//ltvi_campo.overlaypictureindex = 4

return ltvi_campo
end function

public subroutine wf_check_limit (ref long ll_find_rows);/**
 * stefanop
 * 10/07/2014
 *
 * Controllo se le righe recuperate dal datastore non sia 
 * superiore a quello impostato come limite.
 * La funzione da un messaggio di conferma per limitare le righe
 * da visualizzare
 **/
 
if ll_find_rows > il_max_treeview_results then
	
	if g_mb.confirm("Sono stati recupertati più di " + string(il_max_treeview_results) + ".~r~nLimitare la visualizzazione?") then
		ll_find_rows = il_max_treeview_results
	end if
	
end if
end subroutine

public subroutine wf_popola_item (long al_handle);/**
 * stefanop
 * 11/07/2014
 *
 * Pulisce tutti i figli dell'item della treeview selezionato e lo ripopola.
 * Utile per aggiornare i figli durante un cambiamento, come cancellazione e inserimento.
 **/
 
long ll_children_handle

if not isnull(al_handle) and al_handle >= 0 then
 
	ib_clear_tree = true
	tab_ricerca.selezione.tv_selezione.setredraw(false)
	
	ll_children_handle = tab_ricerca.selezione.tv_selezione.finditem(ChildTreeItem!, al_handle)
	
	do while ll_children_handle > 0 
		tab_ricerca.selezione.tv_selezione.deleteitem(ll_children_handle)
		ll_children_handle = tab_ricerca.selezione.tv_selezione.finditem(ChildTreeItem!, al_handle)
	loop
	
	ib_clear_tree = false
	
	// Ripopolo item
	tab_ricerca.selezione.tv_selezione.post event ItemPopulate(al_handle)
	tab_ricerca.selezione.tv_selezione.setredraw(true)
	
end if


end subroutine

protected subroutine wf_treeview_search ();/**
 * stefanop
 * 28/10/2011
 *
 * Pulisco l'albero, recupero la query con i filtri impostati e lancio la ricerca
 **/

setpointer(HourGlass!)
 
wf_treeview_clear()

tab_ricerca.ricerca.dw_ricerca.accepttext()
wf_imposta_ricerca()
wf_leggi_livello(0, 1)

if wf_get_valore_livello(1) <> "N" then
	ib_clear_tree = true
end if

tab_ricerca.selecttab(2)
tab_ricerca.selezione.tv_selezione.setredraw(true)
tab_ricerca.selezione.tv_selezione.setfocus()

ib_clear_tree = false

setpointer(Arrow!)
end subroutine

on w_cs_xx_treeview.create
int iCurrent
call super::create
this.tab_dettaglio=create tab_dettaglio
this.tab_ricerca=create tab_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_dettaglio
this.Control[iCurrent+2]=this.tab_ricerca
end on

on w_cs_xx_treeview.destroy
call super::destroy
destroy(this.tab_dettaglio)
destroy(this.tab_ricerca)
end on

event resize;/** TOLTO ANCESTOR **/

tab_ricerca.height = newheight - 30
tab_dettaglio.resize(newwidth - tab_ricerca.x - tab_ricerca.width - ii_tab_width_offset - 20, newheight - 30)

tab_ricerca.event ue_resize()
tab_dettaglio.event ue_resize()
end event

event pc_setwindow;call super::pc_setwindow;/**
VALORI DA CODIFICARE A MANO NELLE FINESTRE EREDITATE
 
is_codice_filtro = "codice_filtro"
 
 **/
 
// posiziono elementi
tab_ricerca.move(20,20)
tab_ricerca.ricerca.dw_ricerca.move(0,0)
tab_ricerca.selezione.tv_selezione.move(0,0)
tab_ricerca.ricerca.dw_ricerca.insertrow(0)

tab_dettaglio.move(tab_ricerca.x + tab_ricerca.width + ii_tab_width_offset, 20)

// Icone ricerca
wf_imposta_icone_ricerca()

// altre impostazioni di base
tab_ricerca.selezione.tv_selezione.deletepictures()
ICON_DELETED_ITEM = wf_treeview_add_icon("treeview\nodo_eliminato.png")
wf_treeview_icons()

// Imposta i livello
postevent("ue_imposta_valori_livelli")

postevent("ue_valori_ricerca_predefiniti")


end event

event pc_new;call super::pc_new;////tolto ancestor script, ma viene richiamato dopo mediante call super
////serve a fare in modo che se vai con il tasto su/giù o rotellina non seleziona il record precedente, in quanto viene rimosso grazie al reset
////succede se fai retrieve di un dato esistente, poi fai nuovo, inserendo nuovo record (quindi in totale 2 record) e facendo lo scroll ri rischiava di selezionare altro record
////invece cosi rimuoviamo il record precedente ed abbiamo solo il nuovo record
//
////questo vale per le maschere tree che devono avere nelle dw un record per volta ...
//
//if isvalid(iuo_dw_main) then
//   iuo_dw_main.reset()
//end if
//
//call super::pc_new
end event

type tab_dettaglio from tab within w_cs_xx_treeview
event ue_resize ( )
integer x = 1371
integer y = 20
integer width = 2446
integer height = 1800
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
alignment alignment = center!
integer selectedtab = 1
det_1 det_1
end type

event ue_resize();//int li_i, li_j
//
//// scorro i tab
//for li_i = 1 to upperbound(control)
//
//	// scorro gli elementi del tabpage
//	for li_j = 1 to upperbound(control[li_i].control)
//		if control[li_i].control[li_j].typeof() = DataWindow! then
//			control[li_i].control[li_j].resize(control[li_i].width, control[li_i].height)
//		end if
//	next 	
//	
//next
end event

on tab_dettaglio.create
this.det_1=create det_1
this.Control[]={this.det_1}
end on

on tab_dettaglio.destroy
destroy(this.det_1)
end on

type det_1 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2409
integer height = 1676
long backcolor = 12632256
string text = "none"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tab_ricerca from tab within w_cs_xx_treeview
event ue_resize ( )
integer x = 23
integer y = 20
integer width = 1257
integer height = 1800
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
alignment alignment = center!
integer selectedtab = 1
ricerca ricerca
selezione selezione
end type

event ue_resize();// ridimensiono DW di ricerca
ricerca.dw_ricerca.resize(ricerca.width, ricerca.height)

// ridimensiono TV
selezione.tv_selezione.resize(ricerca.width, ricerca.height)

end event

on tab_ricerca.create
this.ricerca=create ricerca
this.selezione=create selezione
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
destroy(this.ricerca)
destroy(this.selezione)
end on

type ricerca from userobject within tab_ricerca
integer x = 18
integer y = 108
integer width = 1221
integer height = 1676
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_ricerca dw_ricerca
end type

on ricerca.create
this.dw_ricerca=create dw_ricerca
this.Control[]={this.dw_ricerca}
end on

on ricerca.destroy
destroy(this.dw_ricerca)
end on

type dw_ricerca from uo_std_dw within ricerca
event ue_key pbm_dwnkey
integer x = 5
integer y = 12
integer width = 1189
integer height = 1640
integer taborder = 30
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event ue_key;if key = keyenter! then
	wf_treeview_search()
end if

end event

event itemchanged;call super::itemchanged;if left(dwo.name, 8) = "livello_" then
	wf_valore_livelli_taborder(string(dwo.name), data)
end if
end event

event clicked;call super::clicked;choose case dwo.name
		
	case "b_leggi_filtro"
		guo_functions.uof_leggi_filtro(dw_ricerca, is_codice_filtro)
		
	case "b_memorizza_filtro"
		guo_functions.uof_memorizza_filtro(dw_ricerca, is_codice_filtro)
		
	case "b_cancella_filtro"
		guo_functions.uof_cancella_filtro(dw_ricerca, is_codice_filtro)
		
	case "b_cerca", "b_ricerca"
		wf_treeview_search()
		
end choose
end event

type selezione from userobject within tab_ricerca
integer x = 18
integer y = 108
integer width = 1221
integer height = 1676
long backcolor = 12632256
string text = "Selezione"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
tv_selezione tv_selezione
end type

on selezione.create
this.tv_selezione=create tv_selezione
this.Control[]={this.tv_selezione}
end on

on selezione.destroy
destroy(this.tv_selezione)
end on

type tv_selezione from treeview within selezione
integer x = 5
integer y = 12
integer width = 1211
integer height = 1660
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
boolean linesatroot = true
boolean hideselection = false
boolean tooltips = false
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event selectionchanged;/**
 * stefanop
 * 06/11/2011
 *
 * Nel figlio bisogna testare la variabile del padre per eseguire le istruzioni con la variabile
 * AncestorReturnValue = 0 tutto ok
 * AncestorReturnValue < 0 errore
 * code: if AncestorReturnValue < 0 then return
 **/
 
if not wf_is_valid(newhandle, oldhandle) or not ib_fire_selection_changed then return -1

il_current_handle = newhandle

// ** DA COPIARE NEL FIGLIO **/
//treeviewitem ltvi_item
//
//if AncestorReturnValue < 0 then return
//
//tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)
//
//istr_data = ltvi_item.data
//	
//tab_dettaglio.det_1.dw_1.change_dw_current()
//getwindow().postevent("pc_retrieve")
////pcca.window_current = getwindow()

end event

event rightclicked;/**
 * stefanop
 * 06/11/2011
 *
 * In alcune finestre, ad esempio il configuratore, è necessario aggiugnere questo controllo al figlio
 * code: pcca.window_current = getwindow()
 *
 * Nel figlio bisogna testare la variabile del padre per eseguire le istruzioni con la variabile
 * AncestorReturnValue = 0 tutto ok
 * AncestorReturnValue < 0 errore
 * code: if AncestorReturnValue < 0 then return
 **/

if not wf_is_valid(handle) then return -1

// seleziono il nodo
tab_ricerca.selezione.tv_selezione.selectitem(handle)

/** DA COPIARE NELLA FINESTRA **/
//treeviewitem ltvi_item
//str_non_conformita lstr_data
//m_non_conformita lm_menu
//
//if AncestorReturnValue < 0 then return
//
//tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)
//
//lstr_data = ltvi_item.data
//
//if lstr_data.tipo_livello = "N" then
//
//	lm_menu = create m_non_conformita
//	
//	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
//	
//	destroy lm_menu
//	
//end if
/** END **/
end event

event itempopulate;/**
 * stefanop
 * 06/11/2011
 *
 * Nel figlio bisogna testare la variabile del padre per eseguire le istruzioni con la variabile
 * AncestorReturnValue = 0 tutto ok
 * AncestorReturnValue < 0 errore
 * code: if AncestorReturnValue < 0 then return
 **/
 
// controllo
if not wf_is_valid(handle) then return -1


//treeviewitem ltvi_item
//s_cs_xx_treeview lstr_data
//
//if AncestorReturnValue < 0 then return
//
//getitem(handle, ltvi_item)
//
//lstr_data = ltvi_item.data
//
//if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
//	ltvi_item.children = false
//	setitem(handle, ltvi_item)
//end if
//
end event

