﻿$PBExportHeader$w_invio_email.srw
forward
global type w_invio_email from w_cs_xx_risposta
end type
type tab_1 from tab within w_invio_email
end type
type page_1 from userobject within tab_1
end type
type dw_messaggio from datawindow within page_1
end type
type cb_outlook from commandbutton within page_1
end type
type p_allegati from picture within page_1
end type
type cbx_copia from checkbox within page_1
end type
type p_add_allegati from picture within page_1
end type
type p_add_cc from picture within page_1
end type
type p_add_a from picture within page_1
end type
type st_3 from statictext within page_1
end type
type dw_allegati from uo_dw_drag within page_1
end type
type dw_cc from datawindow within page_1
end type
type dw_a from datawindow within page_1
end type
type st_2 from statictext within page_1
end type
type st_1 from statictext within page_1
end type
type cb_invia from commandbutton within page_1
end type
type cb_2 from commandbutton within page_1
end type
type page_1 from userobject within tab_1
dw_messaggio dw_messaggio
cb_outlook cb_outlook
p_allegati p_allegati
cbx_copia cbx_copia
p_add_allegati p_add_allegati
p_add_cc p_add_cc
p_add_a p_add_a
st_3 st_3
dw_allegati dw_allegati
dw_cc dw_cc
dw_a dw_a
st_2 st_2
st_1 st_1
cb_invia cb_invia
cb_2 cb_2
end type
type page_2 from userobject within tab_1
end type
type cb_add from commandbutton within page_2
end type
type dw_destinatari from datawindow within page_2
end type
type page_2 from userobject within tab_1
cb_add cb_add
dw_destinatari dw_destinatari
end type
type tab_1 from tab within w_invio_email
page_1 page_1
page_2 page_2
end type
end forward

global type w_invio_email from w_cs_xx_risposta
integer width = 3415
integer height = 2184
string title = "Invio Email"
boolean controlmenu = false
tab_1 tab_1
end type
global w_invio_email w_invio_email

type variables
constant string MAIL_PATTERN = "\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}\b"
constant ulong SUCCESS_COLOR = 14024661
constant ulong WHITE_COLOR = 16777215
constant ulong WARNING_COLOR = 11192063

private:
	s_invio_email istr_email
	uo_regex iuo_regex
	
	// Lazy load destinatari
	boolean ib_lazy_load_destinatari = true
	
	// Indica dove aggiungere i nuovi destinatari (A, CC)
	string is_add_to = "A"
	
	int ii_selected_count = 0
	
	string is_email_utente
	
	uo_outlook iuo_outlook
end variables

forward prototypes
public subroutine wf_prepare_window ()
public function boolean wf_valida (ref string as_message)
private subroutine wf_add_emails_to_dw (ref datawindow adw_dw, string as_campo, string as_array[])
private function string wf_nome_file (string as_path)
public function integer wf_get_emails_from_dw (datawindow adw_dw, ref string as_emails[])
public function integer wf_valida_dw (datawindow adw_dw, boolean ab_almeno_uno)
public subroutine wf_check_outlook ()
end prototypes

public subroutine wf_prepare_window ();/**
 * Partendo dalla struttura inizializzo la UI
 **/
 
int li_i, li_r

wf_add_emails_to_dw(tab_1.page_1.dw_a, "email", istr_email.as_a)
wf_add_emails_to_dw(tab_1.page_1.dw_cc, "email", istr_email.as_cc)

tab_1.page_1.dw_messaggio.insertrow(0)
tab_1.page_1.dw_messaggio.setitem(1, "oggetto", istr_email.as_subject)
tab_1.page_1.dw_messaggio.setitem(1, "messaggio", istr_email.as_message)

for li_i = 1 to upperbound(istr_email.as_allegati)
	
	li_r = tab_1.page_1.dw_allegati.insertrow(0)
	
	tab_1.page_1.dw_allegati.setitem(li_r, "nome_file", wf_nome_file(istr_email.as_allegati[li_i]))
	tab_1.page_1.dw_allegati.setitem(li_r, "path", istr_email.as_allegati[li_i])
	
next
end subroutine

public function boolean wf_valida (ref string as_message);/**
 * stefanop
 * 16/05/2014
 *
 * Valida i dati della UI
 *
 * @return true se è tutto OK
 **/

string ls_empty[], ls_value, ls_valid
int li_i, li_error_count

setnull(as_message)
li_error_count = 0

// A
tab_1.page_1.dw_a.accepttext()
if tab_1.page_1.dw_a.rowcount() > 0 then
	li_error_count = wf_valida_dw( tab_1.page_1.dw_a, true )
else
	as_message = "Selezionare almeno un DESTINATARIO"
	return false
end if

// CC
tab_1.page_1.dw_cc.accepttext()
tab_1.page_1.dw_messaggio.accepttext()
li_error_count += wf_valida_dw( tab_1.page_1.dw_cc, false )

// Subject
ls_value = tab_1.page_1.dw_messaggio.getitemstring(1,"oggetto")
if isnull(ls_value) or ls_value = "" then
	as_message = "Il campo OGGETTO non può essere vuoto"
	tab_1.page_1.dw_messaggio.object.oggetto.background.color = long(WARNING_COLOR)
	tab_1.page_1.dw_messaggio.setcolumn("oggetto")
	li_error_count++
else
	tab_1.page_1.dw_messaggio.object.oggetto.background.color = long(WHITE_COLOR)
end if

// Message
ls_value = tab_1.page_1.dw_messaggio.getitemstring(1,"messaggio")
if isnull(ls_value) or ls_value = "" then
	as_message = "Il campo MESSAGGIO non può essere vuoto"
	li_error_count++
	tab_1.page_1.dw_messaggio.object.messaggio.background.color = long(WARNING_COLOR)
	tab_1.page_1.dw_messaggio.setcolumn("messaggio")
else
	tab_1.page_1.dw_messaggio.object.messaggio.background.color = long(WHITE_COLOR)
end if

// Attachment
tab_1.page_1.dw_cc.accepttext()

return li_error_count = 0
end function

private subroutine wf_add_emails_to_dw (ref datawindow adw_dw, string as_campo, string as_array[]);/**
 * stefanop
 * 16/05/2014
 *
 * Aggiungo lista alla dw e cancello campi vuoti
 **/
 
string ls_value
int li_i, li_r

for li_i = 1 to upperbound(as_array)

	li_r = adw_dw.insertrow(0)
	adw_dw.setitem(li_r, as_campo, as_array[li_i])
	
next

// clear empty row
for li_i = 1 to adw_dw.rowcount()

	ls_value = adw_dw.getitemstring(li_i, as_campo)
	
	if isnull(ls_value) or ls_value = "" then
		adw_dw.deleterow(li_i)
		li_i --
	end if
	
next

// add empty row
li_r = adw_dw.insertrow(0)
end subroutine

private function string wf_nome_file (string as_path);string ls_path[]

g_str.split( as_path, "([\w.-]+)$", ls_path)

if upperbound(ls_path) > 0 then
	return ls_path[1]
else
	return "<n.d.>"
end if
end function

public function integer wf_get_emails_from_dw (datawindow adw_dw, ref string as_emails[]);/**
 * stefanop
 * 17/06/2014
 *
 * Recupero le mail valide
 **/
 
string ls_empty[], ls_email
int li_i, li_count

as_emails = ls_empty

li_count = adw_dw.rowcount()

for li_i = 1 to li_count
	ls_email = adw_dw.getitemstring(li_i, "email")
	
	// In teoria non serve a na mazza perchè se arrivo qui
	// sono già passato per la wf_valid() che ha fatto tutti
	// i controlli
	if adw_dw.getitemstring(li_i, "valid") = "S" and not isnull(ls_email) and ls_email <> "" then
		as_emails[upperbound(as_emails) + 1 ] = ls_email
	end if
	
next

return upperbound(as_emails)
end function

public function integer wf_valida_dw (datawindow adw_dw, boolean ab_almeno_uno);/**
 * stefanop
 * 17/06/2014
 *
 * Valido le colonne email di una datawindow
 * Return: il numero di colonne con errore
 **/
 
string ls_email, ls_valid
int li_i, li_err, li_count

li_err = 0
li_count = adw_dw.rowcount()

// C'è solo una riga e devo verifica se è valida altrimenti la devo segnare come errore
if li_count = 1 and ab_almeno_uno then
	ls_email = adw_dw.getitemstring(1, "email")
	if isnull(ls_email) or ls_email = "" or not iuo_regex.test(ls_email) then
		li_err++
		adw_dw.setitem(1, "valid", "N")
	else
		adw_dw.setitem(1, "valid", "S")
	end if
else 
	// controllo tutte le righe
	for li_i = 1 to li_count
		ls_valid = "N"
		ls_email = adw_dw.getitemstring(li_i, "email")
		
		if li_i = li_count and isnull(ls_email) then
			// se sono nell'ultima riga e la mail è vuota è corretto!
			ls_valid = "S"
			
		elseif isnull(ls_email) or ls_email = "" or not iuo_regex.test(ls_email) then
			// altrimenti NON è corretto
			li_err++
		else
			ls_valid = "S"
		end if
		
		adw_dw.setitem(li_i, "valid", ls_valid)
	next
end if

return li_err
end function

public subroutine wf_check_outlook ();/**
 * stefanop
 * 14/07/2014
 *
 * Controllo se nel pc è presente outlook e abilito il pulsante
 * Richiesto da Gibus
 **/

tab_1.page_1.cb_outlook.visible = iuo_outlook.uof_is_outlook_installed( )
end subroutine

on w_invio_email.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_invio_email.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;
iuo_outlook = create uo_outlook

// stefanop 16/05/2014
iuo_regex = create uo_regex
iuo_regex.initialize( MAIL_PATTERN, true, false)
	
// recupero informazioni
istr_email = message.powerObjectParm

tab_1.page_2.dw_destinatari.settransobject(sqlca)

// Disegno UI
wf_prepare_window()

// Seleziono primo tab
tab_1.selectedtab = 1

// Recupero email utente collegato
select e_mail
into :is_email_utente
from utenti
where cod_utente = :s_cs_xx.cod_utente;

if isnull(is_email_utente) or is_email_utente = "" or not iuo_regex.test(is_email_utente) then
	
	tab_1.page_1.cbx_copia.checked = false
	tab_1.page_1.cbx_copia.visible = false
	
else
	tab_1.page_1.cbx_copia.text = "Invia una copia a me (" + is_email_utente + ")"

end if

wf_check_outlook()
istr_email.ab_inviato_ad_outlook = false

// Icone
tab_1.page_1.p_add_a.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\add.png"
tab_1.page_1.p_add_cc.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\add.png"
tab_1.page_1.p_add_allegati.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\add.png"
tab_1.page_1.p_allegati.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_non_eseguita.png" // documento


end event

event close;call super::close;destroy iuo_outlook
destroy iuo_regex
end event

type tab_1 from tab within w_invio_email
integer x = 23
integer y = 20
integer width = 3337
integer height = 2040
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
page_1 page_1
page_2 page_2
end type

on tab_1.create
this.page_1=create page_1
this.page_2=create page_2
this.Control[]={this.page_1,&
this.page_2}
end on

on tab_1.destroy
destroy(this.page_1)
destroy(this.page_2)
end on

event selectionchanged;long ll_i, ll_count

// stefanop 16/05/2014
if newindex = 2 and ib_lazy_load_destinatari then
	
	page_2.dw_destinatari.retrieve(s_cs_xx.cod_azienda)
	ib_lazy_load_destinatari = false
	
elseif newindex = 2 then
	
	// deseleziono righe precedenti
	ll_count = page_2.dw_destinatari.rowcount()
	ll_i = 1
	
	do while true
		ll_i = page_2.dw_destinatari.find("checked='S'", ll_i, ll_count)
		
		// exit loop
		if ll_i <= 0 or ll_i = ll_count then exit
		
		 page_2.dw_destinatari.setitem(ll_i, "checked", "N")
		 ll_i ++
	loop
			
end if

ii_selected_count = 0

page_2.enabled = newindex = 2
page_2.cb_add.text = "Seleziona destinatari"
page_2.cb_add.enabled = false
end event

type page_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3301
integer height = 1916
long backcolor = 12632256
string text = "Email"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_messaggio dw_messaggio
cb_outlook cb_outlook
p_allegati p_allegati
cbx_copia cbx_copia
p_add_allegati p_add_allegati
p_add_cc p_add_cc
p_add_a p_add_a
st_3 st_3
dw_allegati dw_allegati
dw_cc dw_cc
dw_a dw_a
st_2 st_2
st_1 st_1
cb_invia cb_invia
cb_2 cb_2
end type

on page_1.create
this.dw_messaggio=create dw_messaggio
this.cb_outlook=create cb_outlook
this.p_allegati=create p_allegati
this.cbx_copia=create cbx_copia
this.p_add_allegati=create p_add_allegati
this.p_add_cc=create p_add_cc
this.p_add_a=create p_add_a
this.st_3=create st_3
this.dw_allegati=create dw_allegati
this.dw_cc=create dw_cc
this.dw_a=create dw_a
this.st_2=create st_2
this.st_1=create st_1
this.cb_invia=create cb_invia
this.cb_2=create cb_2
this.Control[]={this.dw_messaggio,&
this.cb_outlook,&
this.p_allegati,&
this.cbx_copia,&
this.p_add_allegati,&
this.p_add_cc,&
this.p_add_a,&
this.st_3,&
this.dw_allegati,&
this.dw_cc,&
this.dw_a,&
this.st_2,&
this.st_1,&
this.cb_invia,&
this.cb_2}
end on

on page_1.destroy
destroy(this.dw_messaggio)
destroy(this.cb_outlook)
destroy(this.p_allegati)
destroy(this.cbx_copia)
destroy(this.p_add_allegati)
destroy(this.p_add_cc)
destroy(this.p_add_a)
destroy(this.st_3)
destroy(this.dw_allegati)
destroy(this.dw_cc)
destroy(this.dw_a)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_invia)
destroy(this.cb_2)
end on

type dw_messaggio from datawindow within page_1
integer x = 27
integer y = 872
integer width = 3269
integer height = 880
integer taborder = 80
string title = "none"
string dataobject = "d_invio_email_messaggio"
boolean border = false
boolean livescroll = true
end type

type cb_outlook from commandbutton within page_1
integer x = 1902
integer y = 1772
integer width = 549
integer height = 104
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Apri con Outlook"
end type

event clicked;string  ls_empty[], ls_error
int li_i

istr_email.as_subject = tab_1.page_1.dw_messaggio.getitemstring(1,"oggetto")
istr_email.as_message =  tab_1.page_1.dw_messaggio.getitemstring(1,"messaggio")

wf_get_emails_from_dw(dw_a, istr_email.as_a)
wf_get_emails_from_dw(dw_cc, istr_email.as_cc)

// Aggiungo una copia per me stesso
if cbx_copia.checked then
	istr_email.as_cc[ upperbound(istr_email.as_cc) + 1] = is_email_utente
end if

// allegati
istr_email.as_allegati = ls_empty
for li_i = 1 to dw_allegati.rowcount()
	istr_email.as_allegati[li_i] = dw_allegati.getitemstring(li_i, "path")
next

iuo_outlook.uof_apri_outlook( &
	istr_email.as_a, &
	istr_email.as_subject, &
	istr_email.as_message, &
	istr_email.as_allegati, &
	ref ls_error)
	
istr_email.ab_invio_annullato = false
istr_email.ab_inviato_ad_outlook = true
closewithreturn(w_invio_email, istr_email)
end event

type p_allegati from picture within page_1
integer x = 2245
integer y = 32
integer width = 73
integer height = 64
boolean originalsize = true
string picturename = "C:\cs_125\framework\RISORSE\11.5\man_reg_non_eseguita.png"
boolean focusrectangle = false
end type

type cbx_copia from checkbox within page_1
integer x = 27
integer y = 1792
integer width = 1829
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Invia una copia a me"
boolean checked = true
end type

type p_add_allegati from picture within page_1
integer x = 3090
integer y = 32
integer width = 73
integer height = 64
boolean originalsize = true
string picturename = "C:\cs_125\RISORSE\11.5\add.png"
boolean focusrectangle = false
end type

event clicked;string docpath, docname[], ls_desktop
integer li_r, li_i, li_count

ls_desktop = guo_functions.uof_get_user_desktop_folder( )

if GetFileOpenName( "Seleziona File da Allegare", docpath, docname[], ls_desktop) > 0 then
	
	li_count = upperbound(docname)
	
	if li_count = 1 then
		li_r = dw_allegati.insertrow(0)
		dw_allegati.setitem(li_r, "nome_file", wf_nome_file(docpath))
		dw_allegati.setitem(li_r, "path", docpath)
	else
		for li_i = 1 to li_count
			li_r = dw_allegati.insertrow(0)
			dw_allegati.setitem(li_r, "nome_file", docname[li_i])
			dw_allegati.setitem(li_r, "path", docpath + "\" + docname[li_i]  )	
		next
		
	end if
	
end if


end event

type p_add_cc from picture within page_1
integer x = 1993
integer y = 32
integer width = 73
integer height = 64
boolean originalsize = true
string picturename = "C:\cs_125\RISORSE\11.5\add.png"
boolean focusrectangle = false
end type

event clicked;is_add_to = "CC"
tab_1.page_2.enabled = true
tab_1.selecttab(2)
end event

type p_add_a from picture within page_1
integer x = 873
integer y = 32
integer width = 73
integer height = 64
boolean originalsize = true
string picturename = "C:\cs_125\RISORSE\11.5\add.png"
boolean focusrectangle = false
end type

event clicked;is_add_to = "A"
tab_1.page_2.enabled = true
tab_1.selecttab(2)
end event

type st_3 from statictext within page_1
integer x = 2336
integer y = 32
integer width = 389
integer height = 64
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Allegati:"
boolean focusrectangle = false
end type

type dw_allegati from uo_dw_drag within page_1
integer x = 2245
integer y = 112
integer width = 1029
integer height = 740
integer taborder = 70
string dataobject = "d_invio_email_allegati"
end type

event ue_drop_file;call super::ue_drop_file;int li_i

li_i = insertrow(0)
setitem(li_i, "nome_file", wf_nome_file(as_filename))
setitem(li_i, "path", as_filename)

return true
end event

event buttonclicked;call super::buttonclicked;if string(dwo.name) = "b_elimina" then
	deleterow(row)
end if
end event

event doubleclicked;call super::doubleclicked;string							ls_path
uo_shellexecute			luo_run


if row>0 then
	ls_path = getitemstring(row, "path")
	
	if ls_path<>"" and not isnull(ls_path) then
		if fileexists(ls_path) then
			
			luo_run = create uo_shellexecute
			luo_run.uof_run( handle(parent), ls_path )
			destroy(luo_run)
			
		else
			g_mb.warning("Il file "+ls_path+" sembra non sia presente nel percorso indicato!")
			return
		end if
	end if
	
end if
end event

type dw_cc from datawindow within page_1
integer x = 1147
integer y = 112
integer width = 1029
integer height = 740
integer taborder = 60
string title = "none"
string dataobject = "d_invio_email_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event buttonclicked;if string(dwo.name) = "b_elimina" then
	deleterow(row)
end if
end event

type dw_a from datawindow within page_1
integer x = 27
integer y = 112
integer width = 1029
integer height = 740
integer taborder = 60
string title = "none"
string dataobject = "d_invio_email_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event buttonclicked;if string(dwo.name) = "b_elimina" then
	deleterow(row)
end if
end event

event itemchanged;string ls_value

if string(dwo.name) = "email" then
	
	if iuo_regex.test(data) then
		setitem(row, "valid", "S")
	else
		setitem(row, "valid", "N")
	end if
	
	// controllo se esiste un'ultima riga vuota, altrimenti la aggiungo
	if row = rowcount() then
		insertrow(0)
	end if
	
end if
end event

type st_2 from statictext within page_1
integer x = 1147
integer y = 32
integer width = 389
integer height = 64
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "CC:"
boolean focusrectangle = false
end type

type st_1 from statictext within page_1
integer x = 27
integer y = 32
integer width = 402
integer height = 64
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "A:"
boolean focusrectangle = false
end type

type cb_invia from commandbutton within page_1
integer x = 2930
integer y = 1772
integer width = 343
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Invia"
end type

event clicked;string ls_message, ls_empty[], ls_email, ls_error
int li_i

enabled = false

if not wf_valida(ls_message) then
	
	if not isnull(ls_message) then
		g_mb.warning(ls_message)
	end if
	
	enabled = true
	
else
	
	istr_email.as_subject =  tab_1.page_1.dw_messaggio.getitemstring(1,"oggetto")
	istr_email.as_message =  tab_1.page_1.dw_messaggio.getitemstring(1,"messaggio")
	
	wf_get_emails_from_dw(dw_a, istr_email.as_a)
	wf_get_emails_from_dw(dw_cc, istr_email.as_cc)
	
	// Aggiungo una copia per me stesso
	if cbx_copia.checked then
		istr_email.as_cc[ upperbound(istr_email.as_cc) + 1] = is_email_utente
	end if
	
	// allegati
	istr_email.as_allegati = ls_empty
	for li_i = 1 to dw_allegati.rowcount()
		istr_email.as_allegati[li_i] = dw_allegati.getitemstring(li_i, "path")
	next
	
	destroy iuo_regex
	
	istr_email.ab_invio_annullato = false
	closewithreturn(w_invio_email, istr_email)
	
end if
end event

type cb_2 from commandbutton within page_1
integer x = 2565
integer y = 1772
integer width = 343
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;istr_email.ab_invio_annullato = true
closewithreturn(w_invio_email, istr_email)

end event

type page_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3301
integer height = 1916
boolean enabled = false
long backcolor = 12632256
string text = "Altri Destinatari"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_add cb_add
dw_destinatari dw_destinatari
end type

on page_2.create
this.cb_add=create cb_add
this.dw_destinatari=create dw_destinatari
this.Control[]={this.cb_add,&
this.dw_destinatari}
end on

on page_2.destroy
destroy(this.cb_add)
destroy(this.dw_destinatari)
end on

type cb_add from commandbutton within page_2
integer x = 1719
integer y = 1772
integer width = 777
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi"
end type

event clicked;string ls_mail[], ls_emails, ls_value
long ll_rowcount, ll_find

ll_rowcount = dw_destinatari.rowcount()
ll_find = dw_destinatari.find("checked='S'", 1, ll_rowcount)

DO WHILE ll_find > 0
	
	ls_mail[upperbound(ls_mail) + 1] = dw_destinatari.getitemstring(ll_find, "email")
	dw_destinatari.setitem(ll_find, "checked", "N")
	
	// Prevent endless loop
	ll_find++
	if ll_find > ll_rowcount then exit
	ll_find = dw_destinatari.find("checked='S'", ll_find, ll_rowcount)
loop

// Aggiorno UI
if is_add_to = "A" then
	wf_add_emails_to_dw(tab_1.page_1.dw_a, "email", ls_mail)
else
	wf_add_emails_to_dw(tab_1.page_1.dw_cc, "email", ls_mail)
end if

tab_1.selectedtab = 1
end event

type dw_destinatari from datawindow within page_2
integer x = 27
integer y = 32
integer width = 2469
integer height = 1680
integer taborder = 30
string title = "none"
string dataobject = "d_invio_email_destinatari"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;
if string(dwo.name) = "checked" then
	
	if data = "S" then
		ii_selected_count ++
	else
		ii_selected_count --
	end if
	
	cb_add.text = "Aggiungi " + string(ii_selected_count) + " destinatari"
	cb_add.enabled = ii_selected_count > 0
	
end if
end event

