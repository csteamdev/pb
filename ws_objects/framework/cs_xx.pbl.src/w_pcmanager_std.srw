﻿$PBExportHeader$w_pcmanager_std.srw
$PBExportComments$Finestra Defaults Powerclass (copiata da Powerclass)
forward
global type w_pcmanager_std from w_pcmanager_main
end type
end forward

global type w_pcmanager_std from w_pcmanager_main
end type
global w_pcmanager_std w_pcmanager_std

event pc_setwindow;call super::pc_setwindow;set_mdi_defaults(c_mdi_noallowredraw + &
                 c_mdi_autoconfigmenus + &
                 c_mdi_toolbartop + &
                 c_mdi_toolbarhidetext + &
                 c_mdi_showresources + &
                 c_mdi_showclock + &
                 c_mdi_nosaveposition)

set_w_defaults(c_enablepopup + &
               c_toolbarnone + &
               c_toolbarhidetext + &
               c_noautoposition + &
               c_autosize + &
               c_zoomwin + &
               c_noautominimize + &
               c_NoSavePosition + &
               c_loadcodetable + &               
               c_closepromptuser)

set_dw_defaults(c_nosharedata + &	
                c_isnotinstance + &
                c_noenablenewonopen + &
					 c_noenablemodifyonopen + &
					 c_retrieveonopen + &
					 c_nonewmodeonempty + &
					 c_parentmodeonopen + &
					 c_newok + &
					 c_onlyonenewrow + &
					 c_modifyok + &
					 c_deleteok + &
					 c_queryok + &
					 c_retrieveall + &
					 c_selectonrowfocuschange + &
					 c_nomultiselect + &
                c_samemodeonselect + &
 					 c_scrollself + &
					 c_refreshonselect + &
					 c_refreshparent + &
					 c_refreshchild + &
					 c_autorefresh + &
					 c_nodrilldown + &
					 c_alwayscheckrequired + &	
					 c_noignorenewrows + &
					 c_viewaftersave + &
					 c_disablecc + &
					 c_disableccinsert, &
					 c_viewmodelowered + &
					 c_viewmodelightgray + &
					 c_inactivedwblack + &
 					 c_cursorrowfocusrect + &
					 c_cursorrowpointer + &
					 c_calcdwstyle + &
					 c_autocalchighlightselected + &
					 c_resizedw + &
					 c_showempty + &
					 c_noautofocus + &
					 c_ccerrorred) 
//					 c_inactivetextlinecol + &
// enablecc modificato in disablecc il 03/08/1999 da Diego

set_ct_defaults(c_ct_loadcodetable + &
					 c_ct_zoomcont + &
					 c_ct_bringtotop + &
					 c_ct_closepromptuser)
						
end event

on w_pcmanager_std.create
call w_pcmanager_main::create
end on

on w_pcmanager_std.destroy
call w_pcmanager_main::destroy
end on

