﻿$PBExportHeader$w_pomanager_std.srw
$PBExportComments$Finestra Defaults Powerclass (copiata da Powerclass)
forward
global type w_pomanager_std from w_pomanager_main
end type
end forward

global type w_pomanager_std from w_pomanager_main
end type
global w_pomanager_std w_pomanager_std

on open;call w_pomanager_main::open;fw_windowdefaults(c_defaultfont, &
                  c_defaultsize, &
                  c_windowgray + &
                  c_windowtextblack)

fw_progressdefaults(c_progressborderlowered + &
                    c_progressdirectionhoriz + &
                    c_progresspercentshow + &
                    c_progressdarkblue + &
                    c_progressbggray + &
                    c_progresstextwhite + &
                    c_progressmetercyan)

fw_sliderdefaults(c_sliderframeraised + &
                  c_sliderborderlowered + &
                  c_sliderdirectionhoriz + &
                  c_sliderindicatorout + &
                  c_slidercenterlineshow + &
                  c_sliderbggray + &
                  c_sliderframegray + &
                  c_sliderbardarkblue + &
                  c_slidercenterlinewhite)

fw_caldefaults(c_calstyle3d + &
               c_calsortasc + &
               c_calgray + &
               c_calwegray)
fw_calheadingdefaults(c_calheadingauto + &
                      c_calheadingbold + &
                      c_calheadingcenter + &
                      c_calheadingdarkblue)
fw_caldaydefaults(c_caldayregular + &
                  c_caldaycenter + &
                  c_caldaygray + &
                  c_calwedaygray + &
                  c_calwedayregular)
fw_calmonthdefaults(c_calmonthbold + &
                    c_calmonthdarkblue)
fw_calyeardefaults(c_calyearbold + &
                   c_calyearshow + &
                   c_calyeardarkblue)
fw_caldisabledefaults(c_caldisableregular + &
                      c_caldisableblack + &
                      c_caldisablebggray)
fw_calselectdefaults(c_calselectregular + &
                     c_calselectdarkblue + &
                     c_calselectbgwhite)

fw_folderdefaults(c_foldertabtop + &
                  c_folder3d + &
                  c_foldergray + &
                  c_folderbggray + &
                  c_folderborderon + &
                  c_folderresizeon)
fw_tabdefaults(c_textbold + &
               c_textcenter + &
               c_textblack + &
               c_textrotationon)
fw_tabdisabledefaults(c_textdisablebold + &
                      c_textdisableblack)
fw_tabcurrentdefaults(c_textcurrentbold + &
                      c_textcurrentdarkblue + &
                      c_tabcurrentgray + &
                      c_tabcurrentcolorfolder)

fw_hldefaults(c_hlwhite + &
              c_showlines + &
              c_showboxes + &
              c_showbmp + &
              c_lineblack + &
              c_retrieveall + &
              c_retainoncollapse + &
              c_collapseonopen + &
              c_highlightwhite + &
              c_rowfocusindicatorblack + &
              c_showrowfocusindicator + &
              c_hidehighlight + &
              c_drilldownondoubleclick + &
              c_singleselect + &
              c_noallowdragdrop + &
              c_dragcopyrows)
fw_hltextdefaults(c_textregular + &
                  c_textblack + &
                  c_texthighlightdarkblue)
end on

on w_pomanager_std.create
call w_pomanager_main::create
end on

on w_pomanager_std.destroy
call w_pomanager_main::destroy
end on

