﻿$PBExportHeader$w_prompt_mle.srw
forward
global type w_prompt_mle from window
end type
type cb_ok from commandbutton within w_prompt_mle
end type
type mle_1 from multilineedit within w_prompt_mle
end type
end forward

global type w_prompt_mle from window
integer width = 1728
integer height = 948
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_ok cb_ok
mle_1 mle_1
end type
global w_prompt_mle w_prompt_mle

type variables
private:
	s_cs_xx_parametri istr_params
end variables

on w_prompt_mle.create
this.cb_ok=create cb_ok
this.mle_1=create mle_1
this.Control[]={this.cb_ok,&
this.mle_1}
end on

on w_prompt_mle.destroy
destroy(this.cb_ok)
destroy(this.mle_1)
end on

event open;istr_params = message.powerobjectparm
setnull( message.powerobjectparm)

istr_params.parametro_b_1 = false
title = istr_params.parametro_s_2
mle_1.limit = istr_params.parametro_ul_1

if not isnull(istr_params.parametro_s_1) and istr_params.parametro_s_1 <> "" then
	mle_1.text = istr_params.parametro_s_1
else
	mle_1.text = ""
end if
end event

event close;message.powerobjectparm = istr_params
end event

type cb_ok from commandbutton within w_prompt_mle
integer x = 1280
integer y = 760
integer width = 411
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;istr_params.parametro_s_1 = mle_1.text
istr_params.parametro_b_1 = true

close(parent)
end event

type mle_1 from multilineedit within w_prompt_mle
integer x = 23
integer y = 20
integer width = 1669
integer height = 720
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
end type

