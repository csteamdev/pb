﻿$PBExportHeader$w_seleziona_tema.srw
forward
global type w_seleziona_tema from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_seleziona_tema
end type
type cb_ok from commandbutton within w_seleziona_tema
end type
type ddlb_profili from dropdownlistbox within w_seleziona_tema
end type
type st_1 from statictext within w_seleziona_tema
end type
end forward

global type w_seleziona_tema from w_cs_xx_principale
integer width = 997
integer height = 444
string title = "Scegli Tema"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
cb_annulla cb_annulla
cb_ok cb_ok
ddlb_profili ddlb_profili
st_1 st_1
end type
global w_seleziona_tema w_seleziona_tema

on w_seleziona_tema.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.ddlb_profili=create ddlb_profili
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.ddlb_profili
this.Control[iCurrent+4]=this.st_1
end on

on w_seleziona_tema.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.ddlb_profili)
destroy(this.st_1)
end on

event open;call super::open;string ls_tema
long ll_tema

if Registryget(s_cs_xx.chiave_root_user + "profilodefault", "tema", ls_tema) < 0 then
	ls_tema = "1"	//tema default classico
	Registryset(s_cs_xx.chiave_root_user + "profilodefault", "tema", regstring!, ls_tema)
end if
ll_tema = long(ls_tema)

if isnull(ll_tema) or ll_tema<0 or not isnumber(ls_tema) or ll_tema>upperbound(s_themes.theme)  then
	ll_tema=1
	Registryset(s_cs_xx.chiave_root_user + "profilodefault", "tema", regstring!, "1")
end if

choose case ll_tema
	case 1
		ddlb_profili.text = "Classico"
	case 5
		ddlb_profili.text = "Moderno"
	case else
		ddlb_profili.text = "Classico"
end choose
end event

type cb_annulla from commandbutton within w_seleziona_tema
integer x = 512
integer y = 220
integer width = 366
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;close(parent)
end event

type cb_ok from commandbutton within w_seleziona_tema
integer x = 114
integer y = 220
integer width = 366
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
boolean default = true
end type

event clicked;string ls_tema

choose case upper(ddlb_profili.text)
	case "CLASSICO"
		ls_tema="1"
		
	case "MODERNO"
		ls_tema="5"
		
	case else
		ls_tema="1"
end choose

Registryset(s_cs_xx.chiave_root_user + "profilodefault", "tema", regstring!, ls_tema)

g_mb.show("Riavviare il programma affinchè le modifiche siano rese effettive!")

close(parent)
end event

type ddlb_profili from dropdownlistbox within w_seleziona_tema
integer x = 297
integer y = 40
integer width = 635
integer height = 376
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean vscrollbar = true
string item[] = {"Classico","Moderno"}
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_seleziona_tema
integer x = 27
integer y = 44
integer width = 229
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tema:"
alignment alignment = right!
boolean focusrectangle = false
end type

