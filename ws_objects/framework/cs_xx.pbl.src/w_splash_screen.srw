﻿$PBExportHeader$w_splash_screen.srw
forward
global type w_splash_screen from window
end type
type mle_messaggio from multilineedit within w_splash_screen
end type
type p_immagine from picture within w_splash_screen
end type
type st_countdown from statictext within w_splash_screen
end type
type cb_chiudi from commandbutton within w_splash_screen
end type
end forward

global type w_splash_screen from window
integer width = 2990
integer height = 1308
windowtype windowtype = child!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
windowanimationstyle openanimation = centeranimation!
windowanimationstyle closeanimation = centeranimation!
mle_messaggio mle_messaggio
p_immagine p_immagine
st_countdown st_countdown
cb_chiudi cb_chiudi
end type
global w_splash_screen w_splash_screen

type variables



time				itm_apertura

integer			ii_delay = 5, ii_conter
end variables

on w_splash_screen.create
this.mle_messaggio=create mle_messaggio
this.p_immagine=create p_immagine
this.st_countdown=create st_countdown
this.cb_chiudi=create cb_chiudi
this.Control[]={this.mle_messaggio,&
this.p_immagine,&
this.st_countdown,&
this.cb_chiudi}
end on

on w_splash_screen.destroy
destroy(this.mle_messaggio)
destroy(this.p_immagine)
destroy(this.st_countdown)
destroy(this.cb_chiudi)
end on

event open;s_cs_xx_parametri				lstr_parametri

lstr_parametri = message.powerobjectparm

this.setredraw(false)
choose case lstr_parametri.parametro_s_1
	case "E"
		p_immagine.picturename = s_cs_xx.volume + s_cs_xx.risorse + "MB_icon_error.JPG"
		
	case "W"
		p_immagine.picturename = s_cs_xx.volume + s_cs_xx.risorse + "MB_icon_warning.JPG"
		
	case else
		p_immagine.picturename = s_cs_xx.volume + s_cs_xx.risorse + "MB_icon_info.JPG"
		
end choose

mle_messaggio.text = "~r~n~r~n" + lstr_parametri.parametro_s_2

this.setredraw(true)


itm_apertura = now()
st_countdown.text = "Questa finestra si chiuderà da sola entro "+string(ii_delay) + " secondi ..."
ii_conter = ii_delay + 1

timer(1)

end event

event timer;


time				ldt_adesso

ldt_adesso = now()
ii_conter -= 1
st_countdown.text = "Questa finestra si chiuderà da sola entro "+string(ii_conter) + " secondi ..."

if secondsafter(itm_apertura, ldt_adesso) > ii_delay then 
	cb_chiudi.postevent(clicked!)
end if
end event

type mle_messaggio from multilineedit within w_splash_screen
integer x = 672
integer y = 20
integer width = 2286
integer height = 1148
integer taborder = 10
integer textsize = -14
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean border = false
boolean autovscroll = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type p_immagine from picture within w_splash_screen
integer x = 46
integer y = 20
integer width = 590
integer height = 528
boolean focusrectangle = false
end type

type st_countdown from statictext within w_splash_screen
integer x = 59
integer y = 1188
integer width = 2894
integer height = 84
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_chiudi from commandbutton within w_splash_screen
boolean visible = false
integer x = 114
integer y = 700
integer width = 494
integer height = 136
integer taborder = 20
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "to exit"
end type

event clicked;close(parent)
end event

