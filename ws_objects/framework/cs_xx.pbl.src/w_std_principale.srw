﻿$PBExportHeader$w_std_principale.srw
$PBExportComments$Window di Base da usare SEMPRE ereditandola.
forward
global type w_std_principale from window
end type
end forward

global type w_std_principale from window
integer width = 3566
integer height = 1648
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
event pc_setwindow ( )
event ue_imposta_text ( )
event ue_layout_params ( )
event ue_commit ( )
end type
global w_std_principale w_std_principale

forward prototypes
private subroutine wf_centra ()
private subroutine wf_imposta_text ()
private subroutine wf_imposta_text (string fs_cod_lingua)
private function integer wf_layout_params ()
end prototypes

event pc_setwindow();postevent("ue_imposta_text")

postevent("ue_layout_params")


end event

event ue_imposta_text();wf_imposta_text()
end event

event ue_layout_params();/*
EnMe 8/7/2008
Aggiunto per parametrizzare alcune opzioni di natura visuale.
*/

wf_layout_params( )
end event

event ue_commit();commit using sqlca;

end event

private subroutine wf_centra ();long ll_larghezza, ll_altezza, ll_x, ll_y

// cambiato perchè col nuovo menu le dimensioni del workspace MDI (w_cs_xx_mdi.mdi_1) potrebbero non
// corrispondere più alle dimensioni dell'area client della maschera base MDI (w_cs_xx_mdi.workspace)

//------------------
	//ll_larghezza = w_cs_xx_mdi.workspacewidth()  - w_cs_xx_mdi.workspacex()
	//ll_altezza   = w_cs_xx_mdi.workspaceheight() - w_cs_xx_mdi.workspacey() - w_cs_xx_mdi.mdi_1.microhelpheight
//------------------
ll_larghezza = w_cs_xx_mdi.mdi_1.width//  - w_cs_xx_mdi.mdi_1.x
ll_altezza   = w_cs_xx_mdi.mdi_1.height// - w_cs_xx_mdi.mdi_1.y - w_cs_xx_mdi.mdi_1.microhelpheight

ll_x = (ll_larghezza - this.width) / 2
ll_y = (ll_altezza - this.height) / 2

if ll_x < 0 then ll_x = 0
if ll_y < 0 then ll_y = 0

this.move(ll_x,ll_y)
end subroutine

private subroutine wf_imposta_text ();string ls_cod_lingua

if s_cs_xx.cod_lingua_applicazione = "" then setnull(s_cs_xx.cod_lingua_applicazione)
if s_cs_xx.cod_lingua_window = "" then setnull(s_cs_xx.cod_lingua_window)

if not isnull(s_cs_xx.cod_lingua_window) then
	wf_imposta_text(s_cs_xx.cod_lingua_window)
else
	wf_imposta_text(s_cs_xx.cod_lingua_applicazione)
end if
end subroutine

private subroutine wf_imposta_text (string fs_cod_lingua);string ls_modify, ls_nome_window, ls_nome_oggetto, ls_testo, ls_sql
long   ll_i, ll_control
checkbox lch_1
commandbutton lcb_1
radiobutton lrb_1
statictext lst_1
groupbox lgb_1

ls_nome_window = this.classname()

ll_control = upperbound(control)

declare cu_wtext dynamic cursor for SQLSA;
ls_sql = 	" select nome_oggetto, testo from tab_w_text_repository " + &
         	" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and" + &
         	" nome_window = '" + ls_nome_window + "' and " + &
			" testo is not null and "
if isnull(fs_cod_lingua) then
	ls_sql += " cod_lingua is null "
else
	ls_sql += " cod_lingua = '" + fs_cod_lingua + "' "
end if

PREPARE SQLSA FROM :ls_sql ;

open dynamic cu_wtext;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Framework", "Errore nell'applicazione delle impostazioni internazionali~r~n"+sqlca.sqlerrtext)
	return
end if

do while true
	fetch cu_wtext into :ls_nome_oggetto, :ls_testo;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Framework", "Errore nell'applicazione delle impostazioni internazionali~r~n"+sqlca.sqlerrtext)
		exit
	end if
	if sqlca.sqlcode = 100 then exit
	
	for ll_i = 1 to ll_control
		
		if ls_nome_oggetto = "this.title" then
			this.title = ls_testo
			continue
		end if
		
		if ls_nome_oggetto = control[ll_i].classname() then
			choose case control[ll_i].typeof()
				case checkbox! 
					lch_1 = control[ll_i]
					lch_1.text = ls_testo
					
				case commandbutton!
					lcb_1 = control[ll_i]
					lcb_1.text = ls_testo
					
				case radiobutton!   
					lrb_1 = control[ll_i]
					lrb_1.text = ls_testo

				case statictext! 
					lst_1 = control[ll_i]
					lst_1.text = ls_testo
					
				case groupbox!
					lgb_1 = control[ll_i]
					lgb_1.text = ls_testo
					
				case else
					continue
						
			end choose
			
		end if
		
	next
	
loop

close cu_wtext;

return

end subroutine

private function integer wf_layout_params ();int li_i, li_j
long ll_controls
commandbutton l_commandbutton
tab l_tab
datawindow	  l_datawindow

ll_controls = upperbound(this.control)

for li_i = 1 to ll_controls
	
	choose case this.control[li_i].typeof()
							
		case commandbutton!
			l_commandbutton = this.control[li_i]
			l_commandbutton.text = upper(l_commandbutton.text)
			l_commandbutton.facename = 'Arial'
			l_commandbutton.textsize = -8
			l_commandbutton.weight = 400
			//l_commandbutton.FlatStyle = TRUE
			
		case tab!
			l_tab =  this.control[li_i]
			l_tab.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor 
			
			// imposto colore di sfondo delle pagine
			for li_j = 1 to upperbound(l_tab.control)
				l_tab.control[li_j].backcolor =  s_themes.theme[s_themes.current_theme].f_tab_color
			next
								
		case else
			continue
				
	end choose

next

return 0

end function

on w_std_principale.create
end on

on w_std_principale.destroy
end on

event open;this.backcolor = s_themes.theme[ s_themes.current_theme].w_backcolor

postevent("pc_setwindow")
end event

