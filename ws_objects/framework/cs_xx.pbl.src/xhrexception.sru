﻿$PBExportHeader$xhrexception.sru
forward
global type xhrexception from exception
end type
end forward

global type xhrexception from exception
end type
global xhrexception xhrexception

type variables
private:
	int responseCode
	
	string responseText
end variables

forward prototypes
public function integer getresponsecode ()
public function string getresponsetext ()
public subroutine setresponsecode (integer ai_response_code)
public subroutine setresponsetext (string as_response_text)
public subroutine setexceptionvalue (uo_xhr2 auo_xhr)
end prototypes

public function integer getresponsecode ();return responseCode
end function

public function string getresponsetext ();return responseText
end function

public subroutine setresponsecode (integer ai_response_code);responseCode = ai_response_code
end subroutine

public subroutine setresponsetext (string as_response_text);responseText = as_response_text
end subroutine

public subroutine setexceptionvalue (uo_xhr2 auo_xhr);responseCode = auo_xhr.responseCode()
responseText = auo_xhr.responseText()
end subroutine

on xhrexception.create
call super::create
TriggerEvent( this, "constructor" )
end on

on xhrexception.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

