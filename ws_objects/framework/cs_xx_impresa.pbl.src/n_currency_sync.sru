﻿$PBExportHeader$n_currency_sync.sru
$PBExportComments$[PRO] Oggetto di cambio con il sistema Europeo.
forward
global type n_currency_sync from nonvisualobject
end type
type ss_valuta from structure within n_currency_sync
end type
end forward

type ss_valuta from structure
	long		l_idval
	string		s_codice
	string		s_simbolo
	decimal {8}	dc_cambio
	decimal {8}	dc_parita
	string		s_tipoarr
	integer		i_cifrearr
	string		s_classe
end type

global type n_currency_sync from nonvisualobject
end type
global n_currency_sync n_currency_sync

type variables
Protected:

string		is_valaz
integer		ii_euroarr = 5
ss_valuta		istr_curr_in, istr_curr_out[]
ss_valuta		istr_curr_lire, istr_curr_euro
boolean		ib_valido = false
n_tran      it_tran
end variables

forward prototypes
public function integer nf_set_valuta_base (long al_id_esercizio)
public function integer nf_set_valuta_base (date ad_data)
public function boolean nf_check_valid ()
protected function decimal nf_conv_kernel (ref ss_valuta astr_in, ref ss_valuta astr_out, decimal adc_in, integer ai_tipo)
protected function decimal nf_round (decimal adc_in, string as_tipo, integer ai_cifre)
public function integer nf_set_valuta_in (long al_id_valuta, decimal adc_cambio)
public function integer nf_set_valuta_out (long al_id_valuta, decimal adc_cambio, integer ai_posiz)
public function integer nf_converti_valore (long al_idval, decimal adc_cambio, decimal adc_valore, long al_idvalout, decimal adc_chgout, ref decimal adc_out[])
public function string nf_check_valute ()
public function string nf_string (any aa_1)
public function character nf_get_valuta_base (long al_id_esercizio)
public function character nf_get_valuta_base ()
public function integer nf_cambia_valuta_in (long al_id_valuta, decimal adc_cambio)
public function integer nf_cambia_valuta_out (long al_id_valuta, decimal adc_cambio, integer ai_posiz)
public function integer nf_converti_valore (decimal adc_in, ref decimal adc_out[])
public function decimal nf_round_valuta_in (decimal adc_in)
protected function decimal nf_conv_kernel_nornd (ref ss_valuta astr_in, ref ss_valuta astr_out, decimal adc_in, integer ai_tipo)
public function integer nf_converti_valore_nornd (long al_idval, decimal adc_cambio, decimal adc_valore, long al_idvalout, decimal adc_chgout, ref decimal adc_out[])
public function integer nf_converti_valore_nornd (decimal adc_in, ref decimal adc_out[])
public function boolean nf_valute_uguali (long al_idv1, decimal adc_c1, long al_idv2, decimal adc_c2)
public function integer nf_tipo_valuta (string as_tipo)
public function integer nf_accoda (long al_code)
public function integer nf_accoda (long al_code, string as_mex[])
public subroutine uf_set_transaction (transaction af_tran)
end prototypes

public function integer nf_set_valuta_base (long al_id_esercizio);// --------------------------------------------------------------------------------
//	Funzione				nf_set_valuta_base
//	Accesso				public
//	Argomenti			long		al_id_esercizio 		esercizio di riferimento
//
//	Ritorno				integer		0=Ok -1=Errore
//
//	Descrizione:
//	Fornito un esercizio, mi calcola la valuta base tramite una select
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		21-12-1998		Prima stesura
// 1.1      [ANT]    04-02-1999     Sostituite le chiamate alle funzioni di sistema
//                                  MessageBox con le chiamate in post alla funzione
//                                  nf_MessageBox.
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

string	ls_mex, ls_valbase, ls_msg[]
integer	li_ret

if isNull (al_id_esercizio) then
	//("Errore","Parametri errati in ricerca valuta base")
	post function nf_accoda(53701)
	return -1
end if

li_ret = 0
//select	esercizio.valuta_base
select	valuta_base
	into	:ls_valbase
	from	dba.esercizio
	where	id_esercizio = :al_id_esercizio
	using it_tran;
if it_tran.sqlcode=0 then
	is_valaz = ls_valbase
else
	//ls_mex = "Impossibile caricare valuta base per l'esercizio scelto:~r~r"+it_tran.sqlerrtext
	//("Database Error",ls_mex)
	ls_msg[1] = it_tran.sqlerrtext
	post function nf_accoda(53702,ls_msg)
	li_ret = -1
end if

return li_ret

end function

public function integer nf_set_valuta_base (date ad_data);// --------------------------------------------------------------------------------
//	Funzione				nf_set_valuta_base
//	Accesso				public
//	Argomenti			date		ad_data		data di riferimento
//
//	Ritorno				integer		0=Ok -1=Errore
//
//	Descrizione:
//	Fornita una data, troverà l'esercizio per sapere poi quale valuta è base in
// quell'esercizio;
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		21-12-1998		Prima stesura
// 1.1      [ANT]    04-02-1999		Sostituite le chiamate alle funzioni di sistema
//                                  MessageBox con le chiamate in post alla funzione
//                                  nf_MessageBox.
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

long		ll_mese, ll_anno, ll_ideser, ll_idaz

if isNull (ad_data) then
	//("Errore","Parametri errati in ricerca valuta base")
	post function nf_accoda(53701)
	return -1
end if

ll_mese = month(ad_data)
ll_anno = year(ad_data)
ll_idaz = 1
it_tran.nf_trova_esercizio(ll_anno, ll_mese, ll_idaz, ll_ideser)
if (not isNull(ll_ideser)) and (ll_ideser>0) then
	return nf_set_valuta_base(ll_ideser)
else
	return -1
end if

end function

public function boolean nf_check_valid ();// --------------------------------------------------------------------------------
//	Funzione				nf_check_valid
//	Accesso				public
//	Argomenti			<nessuno>
//
//	Ritorno				boolean		true=oggetto valido
//
//	Descrizione:
//	dice se i parametri basilari dell'oggetto sono validi o no. Dovrebbe essere lan-
// ciata dopo la costruzione dell'oggetto, ma in post oppure in differita perché
// potrebbe mostrare messaggeboxes, e dentro un evento OPEN sdono pericolosi.
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		21-12-1998		Prima stesura
// 1.1      [ANT]    04-02-1999     Sostituite le chiamate alle funzioni di sistema
//                                  MessageBox con le chiamate in post alla funzione
//                                  nf_MessageBox.
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------
string		ls_mex, ls_field, ls_msg[]


ib_valido = true

ls_field = ""
// id valuta dell'Euro deve essere valida
if (isNull(istr_curr_euro.l_idval)) or (istr_curr_euro.l_idval<=0) then
	ls_field += "id valuta Euro, "
end if
// il Cambio Euro dell'Euro deve essere 1
if (isNull(istr_curr_euro.dc_cambio)) or (istr_curr_euro.dc_cambio<>1) then
	ls_field += "cambio Euro, "
	istr_curr_euro.dc_cambio = 1
end if
// la parità euro dell'Euro deve essere 1
if (isNull(istr_curr_euro.dc_parita)) or (istr_curr_euro.dc_parita<>1) then
	ls_field += "parità euro Euro, "
	istr_curr_euro.dc_parita = 1
end if
// il numero di cifre di arrotondamento dell'euro deve essere 2
if (isNull(istr_curr_euro.i_cifrearr)) or (istr_curr_euro.i_cifrearr<>2) then
	ls_field += "cifre arr. Euro, "
	istr_curr_euro.i_cifrearr = 2
end if
// il tipo di arrotondamento dell'euro deve essere 'M'edio
if (isNull(istr_curr_euro.s_tipoarr)) or (istr_curr_euro.s_tipoarr<>'M') then
	ls_field += "tipo arr. Euro, "
	istr_curr_euro.s_tipoarr = 'M'
end if
// la Lira deve esistere come valuta
if (isNull(istr_curr_lire.l_idval)) or (istr_curr_lire.l_idval<=0) then
	ls_field += "id valuta Lire, "
end if
// la parità euro della lira deve essere positiva. Apposta viene definita "male" a
// 2000 per far vedere quando si sta sballando
if (isNull(istr_curr_lire.dc_parita)) or (istr_curr_euro.dc_parita<=0) then
	ls_field += "parità euro Lire, "
	istr_curr_euro.dc_parita = 2000
end if
// il tipo di arrotondamento della lira deve essere 'M'edio
if (isNull(istr_curr_lire.s_tipoarr)) then
	ls_field += "tipo arr. Lire, "
	istr_curr_lire.s_tipoarr = 'M'
end if
// il numero di cifre di arrotondamento della lira deve essere 0
if (isNull(istr_curr_lire.i_cifrearr)) or (istr_curr_lire.i_cifrearr<>0) then
	ls_field += "cifre arr. Lire, "
	istr_curr_lire.i_cifrearr = 0
end if

if ls_field>"" then
	ib_valido = false
	ls_mex = "L'oggetto non è valido perché manca la valorizzazione di~r"
	ls_mex += "alcuni dei suoi attributi più importanti.~r"
	ls_mex += ls_field
	ls_mex += "~r"
	ls_mex += "I valori sono stati impostati a condizioni di default"
	ls_msg[1] = ls_mex
	post function nf_accoda(53703,ls_msg)
end if

return ib_valido

end function

protected function decimal nf_conv_kernel (ref ss_valuta astr_in, ref ss_valuta astr_out, decimal adc_in, integer ai_tipo);// --------------------------------------------------------------------------------
//	Funzione				nf_conv_kernel
//	Accesso				protected
//	Argomenti			ss_valuta	astr_in		struttura valuta ingresso
//							ss_valuta	astr_out		struttura valuta uscita
//							decimal		adc_in		valore in ingresso
//							integer		ai_tipo		tipo di algoritmo (codificato)
//
//	Ritorno				decimal		valore convertito
//
//	Descrizione:
// Attraverso la chiamata a nf_conv_kernel_nornd questa funzione realizza la conversione
// Poi il risultato viene arrotondato
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		28-07-1999		Fattorizzazione con la _nornd
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------


decimal		ldc_ret


ldc_ret = nf_conv_kernel_nornd (astr_in, astr_out, adc_in, ai_tipo)

if not isnull(ldc_ret) then
	ldc_ret = nf_round(ldc_ret, astr_out.s_tipoarr, astr_out.i_cifrearr)
end if

return ldc_ret

end function

protected function decimal nf_round (decimal adc_in, string as_tipo, integer ai_cifre);// --------------------------------------------------------------------------------
//	Funzione				nf_round
//	Accesso				protected
//	Argomenti			decimal		adc_in		valore in ingresso
//							string		as_tipo		tipo di arrotondamento I|M|S
//							integer		ai_cifre		numero di cifre decimali
//
//	Ritorno				decimal		valore convertito
//
//	Descrizione:
//	questa funzione arrotonda secondo i parametri della valuta passati
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		22-12-1998		Prima stesura
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

decimal		ldc_ret, ldc_prec
decimal		ldc_segno, ldc_modulo

// --------------------------------------------------------------------------------
// normalizzazione in modulo e segno
// --------------------------------------------------------------------------------
if (adc_in<>0) then
	ldc_segno = adc_in/abs(adc_in)
	ldc_modulo = abs(adc_in)
else
	ldc_segno = 1
	ldc_modulo = 0
end if

// --------------------------------------------------------------------------------
// calcolo del fattore di shift
// --------------------------------------------------------------------------------
if ai_cifre>0 then
	ldc_prec = 10^ai_cifre
else
	ai_cifre = - ai_cifre
	ldc_prec = 1/(10^ai_cifre)
end if

// --------------------------------------------------------------------------------
// calcolo del risultato
// --------------------------------------------------------------------------------
choose case as_tipo
	case 'I'
		ldc_ret = ldc_modulo * ldc_prec
		ldc_ret = truncate(ldc_ret,0)
		ldc_ret = ldc_ret / ldc_prec
	case 'M'
		ldc_ret = ldc_modulo * ldc_prec
		ldc_ret = round(ldc_ret,0)
		ldc_ret = ldc_ret / ldc_prec
	case 'S'
		ldc_ret = ldc_modulo * ldc_prec
		ldc_ret = ceiling(ldc_ret)
		ldc_ret = ldc_ret / ldc_prec
	case else
		ldc_ret = ldc_modulo
end choose

// --------------------------------------------------------------------------------
// riconversione con il segno
// --------------------------------------------------------------------------------
ldc_ret = ldc_ret * ldc_segno

return ldc_ret

end function

public function integer nf_set_valuta_in (long al_id_valuta, decimal adc_cambio);// --------------------------------------------------------------------------------
//	Funzione				nf_set_valuta_in
//	Accesso				public
//	Argomenti			long		al_id_valuta		id valuta da sistemare
//							decimal	adc_cambio			cambio eventalmente diverso
//
//	Ritorno				integer		0=Ok -1=Errore
//
//	Descrizione:
//	Permette di impostare una valuta come "valuta di ingresso". Se nelle funzioni di
// conversione non viene specificata, userà quella interna, impostata con questa
// funzione; se non è mai stata impostata neanche questa, pazienza.
//
//	Nota:
//	il cambio ha senso solamente per le valute extracomunitarie
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		21-12-1998		Prima stesura
// 1.1      [ANT]    04-02-1999     Sostituite le chiamate alle funzioni di sistema
//                                  MessageBox con le chiamate in post alla funzione
//                                  nf_MessageBox.
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------


integer		li_ret
string		ls_msg[]


li_ret = 0
select	id_valuta,				codice,
			simbolo,					cambio_euro,
			parita_euro,			cifra_arrotondamento,
			tipo_arrotondamento,	liraeuro
	into	:istr_curr_in.l_idval,			:istr_curr_in.s_codice,
			:istr_curr_in.s_simbolo,		:istr_curr_in.dc_cambio,
			:istr_curr_in.dc_parita,		:istr_curr_in.i_cifrearr,
			:istr_curr_in.s_tipoarr,		:istr_curr_in.s_classe
	from	dba.valuta
	where	id_valuta = :al_id_valuta
	using it_tran;

if it_tran.SQLCode<>0 then
	//Post Function nf_MessageBox("Errore","Impossibile caricare valuta IN:~r"+it_tran.sqlerrtext)
	ls_msg[1] = it_tran.SQLErrText
	post function nf_accoda(53704)
	li_ret = -1
end if

if (not isNull(adc_cambio)) and (adc_cambio>0) then
	istr_curr_in.dc_cambio = adc_cambio
end if

return li_ret


end function

public function integer nf_set_valuta_out (long al_id_valuta, decimal adc_cambio, integer ai_posiz);// --------------------------------------------------------------------------------
//	Funzione				nf_set_valuta_out
//	Accesso				public
//	Argomenti			long		al_id_valuta	valuta da cercare
//							integer	ai_posiz			posizione nel vettore di valute
//
//	Ritorno				integer		0=Ok -1=Errore
//
//	Descrizione:
//	Permette di impostare una valuta come "valuta di uscita numero xx". Per il resto
// si veda nf_set_valuta_in
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		22-12-1998		Prima stesura
// 1.1      [ANT]    04-02-1999     Sostituite le chiamate alle funzioni di sistema
//                                  MessageBox con le chiamate in post alla funzione
//                                  nf_MessageBox.
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------


integer		li_ret
string		ls_msg[]


li_ret = 0
select	id_valuta, 				codice,
			simbolo,					cambio_euro,
			parita_euro,			cifra_arrotondamento,
			tipo_arrotondamento,	liraeuro
	into	:istr_curr_out[ai_posiz].l_idval,		:istr_curr_out[ai_posiz].s_codice,
			:istr_curr_out[ai_posiz].s_simbolo,		:istr_curr_out[ai_posiz].dc_cambio,
			:istr_curr_out[ai_posiz].dc_parita,		:istr_curr_out[ai_posiz].i_cifrearr,
			:istr_curr_out[ai_posiz].s_tipoarr,		:istr_curr_out[ai_posiz].s_classe
	from	dba.valuta
	where	id_valuta = :al_id_valuta
	using it_tran;

if it_tran.SQLCode<>0 then
	//("Errore","Impossibile caricare valuta OUT:~r"+ls_mex)
	ls_msg[1] = it_tran.SQLErrText
	post function nf_accoda(53705,ls_msg)
	li_ret = -1
end if

if (not isnull(adc_cambio)) and (adc_cambio>0) then
	istr_curr_out[ai_posiz].dc_cambio = adc_cambio
end if

return li_ret

end function

public function integer nf_converti_valore (long al_idval, decimal adc_cambio, decimal adc_valore, long al_idvalout, decimal adc_chgout, ref decimal adc_out[]);// --------------------------------------------------------------------------------
//	Funzione				nf_converti_valore
//	Accesso				public
//	Argomenti			long			al_idval			id valuta ingresso
//							decimal		adc_cambio		cambio valuta ingresso (se diverso)
//							decimal		adc_valore		valore da convertire
//							long			al_idvalout		id valuta di uscita
//							decimal		adc_chgout		cambio valuta uscita (se diverso)
//							decimal		adc_out[]		valori convertiti
//
//	Ritorno				integer		0=Ok -1=Errore
//
//	Descrizione:
//	Converte il valore di ingresso in quelli di uscita secondo le valute impostate.
// le valute possono essere reimpostate al momento secondo degli ID. Se si reimpo-
// sta la valuta lo si fa solo per il valore istr_curr_out[1], che è il terzo che
// viene ritornato (i primi due sono sempre Euro e Lire)
//
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		21-12-1998		Prima stesura
// 1.1      [ANT]    04-02-1999     Sostituite le chiamate alle funzioni di sistema
//                                  MessageBox con le chiamate in post alla funzione
//                                  nf_MessageBox.
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------
boolean		lb_valid
integer		li_ret, li_size, li_i, li_digits
integer		li_tipo_val_in, li_tipo_val_out, li_comb
decimal		ldc_par, ldc_chg
string		ls_mex, ls_clas, ls_msg[]

// --------------------------------------------------------------------------------
// 1. prima fase di controlli interni
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
// ci deve essere il record di descrizione di Euro e Lire messo bene
lb_valid = nf_check_valid()
if not lb_valid then
	ls_msg[1] = "Le valute Lira ed Euro non sono caricate correttamente nel database,"
	ls_msg[2] = "oppure vi sono stati errori nel loro caricamento. Si continuerà con dei"
	ls_msg[3] = "valori di default ma non necessariamente validi!"
	post function nf_accoda (53706,ls_msg)
end if


// --------------------------------------------------------------------------------
// 2. seconda fase di controllo e condizionamento parametri
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
// un Null viene preso come zero 
if isNull (adc_valore) then
	adc_valore = 0
end if
// --------------------------------------------------------------------------------
// in caso di ingresso a zero si ritornano tutti zeri e si esce
if adc_valore=0 then
	li_size = upperBound(istr_curr_out)
	adc_out[1] = 0		// euro
	adc_out[2] = 0		// lire
	for li_i=1 to li_size
		adc_out[2+li_i] = 0	// gli altri
	next
	return 0
end if


li_ret = 0
// --------------------------------------------------------------------------------
// se è passata una valuta in ingresso la si imposta
if (not isNull(al_idval)) and (al_idval>0) then
	if al_idval<>istr_curr_in.l_idval then
		nf_set_valuta_in (al_idval, adc_cambio)
	end if
	if adc_cambio<>istr_curr_in.dc_cambio then
		nf_set_valuta_in (al_idval, adc_cambio)
	end if
end if


// --------------------------------------------------------------------------------
// se è passata una valuta in uscita la si imposta per la prima struttura
if (not isNull(al_idvalout)) and (al_idvalout>0) then
	if upperbound(istr_curr_out)<1 then
		nf_set_valuta_out (al_idvalout,adc_chgout,1)
	end if
	if al_idvalout<>istr_curr_out[1].l_idval then
		nf_set_valuta_out (al_idvalout,adc_chgout,1)
	end if
	if adc_chgout<>istr_curr_out[1].dc_cambio then
		nf_set_valuta_out (al_idvalout,adc_chgout,1)
	end if
end if


// --------------------------------------------------------------------------------
// riconoscimento del tipo di valuta in ingresso: N, I, E
// --------------------------------------------------------------------------------
ldc_par = istr_curr_in.dc_parita
ldc_chg = istr_curr_in.dc_cambio
ls_clas = istr_curr_in.s_classe
li_tipo_val_in = nf_tipo_valuta(ls_clas)

// --------------------------------------------------------------------------------
// tutte le conversioni avvengono solamente se le valute sono diverse, tranne il
// caso di valuta extracomunitaria con due cambi diversi. In ogni caso, le conver-
// sioni sono a carico della funzione nf_conv_kernel, che si occupa di tutto
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
// conversione in Euro
// --------------------------------------------------------------------------------
if istr_curr_in.l_idval <> istr_curr_euro.l_idval then
	li_tipo_val_out = 2		// 2 = Euro
	li_comb = 10 * li_tipo_val_in + li_tipo_val_out
	adc_out[1] = nf_conv_kernel (istr_curr_in, istr_curr_euro, adc_valore, li_comb)
else
	adc_out[1] = adc_valore
end if

// --------------------------------------------------------------------------------
// conversione in Lire. La conversione in lire viene sempre fatta, tranne che quando
// si parte dalle lire, passando per l'euro. Di questo si occupa la nf_conv_kernel
// --------------------------------------------------------------------------------
if istr_curr_in.l_idval <> istr_curr_lire.l_idval then
	li_tipo_val_out = 3		// Lire
	li_comb = 10 * li_tipo_val_in + li_tipo_val_out
	adc_out[2] = nf_conv_kernel (istr_curr_in, istr_curr_lire, adc_valore, li_comb)
else
	adc_out[2] = adc_valore
end if

// --------------------------------------------------------------------------------
// conversione per le altre eventuali divise
// combinazioni valide: 11, 12, 13,    21, 22, 23,     31, 32, 33
// vengono ritornate da nf_tipo_valuta per in e out e combinate in li_comb
// --------------------------------------------------------------------------------
li_size = upperBound(istr_curr_out)
for li_i=1 to li_size
	// -----------------------------------------------------------------------------
	// [PRO] 21-07-1999 Se le valute sono uguali, ma i cambi diversi, devo lo stesso
	// convertire. Di questo controllo si occupa ora la nf_conv_kernel. Da qui si
	// invoca e basta (anche se c'è un leggero calo di prestazioni)
	// -----------------------------------------------------------------------------
	ldc_par = istr_curr_out[li_i].dc_parita
	ldc_chg = istr_curr_out[li_i].dc_cambio
	ls_clas = istr_curr_out[li_i].s_classe
	li_tipo_val_out = nf_tipo_valuta(ls_clas)
	li_comb = 10 * li_tipo_val_in + li_tipo_val_out
	adc_out[2+li_i] = nf_conv_kernel (istr_curr_in, istr_curr_out[li_i], adc_valore, li_comb)
next


return li_ret

end function

public function string nf_check_valute ();// --------------------------------------------------------------------------------
//	Funzione				nf_check_valute
//	Accesso				public
//	Argomenti			<nessuno>
//
//	Ritorno				string		descrittore delle valute
//
//	Descrizione:
//	Test. Serve a visualizzare i dati valutari e di sistema
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		22-12-1998		Prima stesura
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

string		ls_1
integer		li_i, li_size

ls_1 = ""
ls_1 += "Validità oggetto = "+string(ib_valido)+"~r~n"
ls_1 += "Arrotondamento Euro intermedio = "+string(ii_euroarr)+"~r~n"
ls_1 += "Parametri interni Euro:~r~n"
ls_1 += "   l_idval: "+nf_string(istr_curr_euro.l_idval)+"~r~n"
ls_1 += "   s_codice: "+nf_string(istr_curr_euro.s_codice)+"~r~n"
ls_1 += "   s_simbolo: "+nf_string(istr_curr_euro.s_simbolo)+"~r~n"
ls_1 += "   dc_cambio: "+nf_string(istr_curr_euro.dc_cambio)+"~r~n"
ls_1 += "   dc_parita: "+nf_string(istr_curr_euro.dc_parita)+"~r~n"
ls_1 += "   s_tipoarr: "+nf_string(istr_curr_euro.s_tipoarr)+"~r~n"
ls_1 += "   i_cifrearr: "+nf_string(istr_curr_euro.i_cifrearr)+"~r~n~r~n"
ls_1 += "Parametri interni Lira:~r~n"
ls_1 += "   l_idval: "+nf_string(istr_curr_lire.l_idval)+"~r~n"
ls_1 += "   s_codice: "+nf_string(istr_curr_lire.s_codice)+"~r~n"
ls_1 += "   s_simbolo: "+nf_string(istr_curr_lire.s_simbolo)+"~r~n"
ls_1 += "   dc_cambio: "+nf_string(istr_curr_lire.dc_cambio)+"~r~n"
ls_1 += "   dc_parita: "+nf_string(istr_curr_lire.dc_parita)+"~r~n"
ls_1 += "   s_tipoarr: "+nf_string(istr_curr_lire.s_tipoarr)+"~r~n"
ls_1 += "   i_cifrearr: "+nf_string(istr_curr_lire.i_cifrearr)+"~r~n~r~n"

li_size = upperbound(istr_curr_out)
for li_i = 1 to li_size
	ls_1 += "Parametri valuta out "+string(li_i)+":~r~n"
	ls_1 += "   l_idval: "+nf_string(istr_curr_out[li_i].l_idval)+"~r~n"
	ls_1 += "   s_codice: "+nf_string(istr_curr_out[li_i].s_codice)+"~r~n"
	ls_1 += "   s_simbolo: "+nf_string(istr_curr_out[li_i].s_simbolo)+"~r~n"
	ls_1 += "   dc_cambio: "+nf_string(istr_curr_out[li_i].dc_cambio)+"~r~n"
	ls_1 += "   dc_parita: "+nf_string(istr_curr_out[li_i].dc_parita)+"~r~n"
	ls_1 += "   s_tipoarr: "+nf_string(istr_curr_out[li_i].s_tipoarr)+"~r~n"
	ls_1 += "   i_cifrearr: "+nf_string(istr_curr_out[li_i].i_cifrearr)+"~r~n~r~n"
next

return ls_1

end function

public function string nf_string (any aa_1);if isnull(aa_1) then
	return "-NULL-"
else
	return string(aa_1)
end if

end function

public function character nf_get_valuta_base (long al_id_esercizio);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  05/02/99  [ANT]    Accetta un id_esercizio e restituisce un Char contenente il campo
//                          esercizio.valuta_base.
//
// *****************************************************************************************
string	ls_msg[]
char		lc_retval = ""

if (al_id_esercizio>0) then
	select	valuta_base
		into	:lc_retval
		from	dba.esercizio
		where	id_esercizio = :al_id_esercizio
		using	it_tran;
		
	choose case it_tran.SQLCode
		case 100
			ls_msg[1] = string(al_id_esercizio)
         //n_err_mgr.nf_accoda(53707,ls_msg)
         g_mb.messagebox("n_currency_sync / nf_get_valuta_base", "Attenzione: non esiste un esercizio con ID " + string(al_id_esercizio))
			return lc_retval
		case -1
			ls_msg[1] = it_tran.sqlerrtext
         //n_err_mgr.nf_accoda(53708,ls_msg)
         g_mb.messagebox("n_currency_sync / nf_get_valuta_base", "Errore durante la lettura della valuta base dell'esercizio.", Exclamation!)
			return lc_retval
	end choose
	
	if IsNull(lc_retval) OR (lc_retval = "") then
      g_mb.messagebox("n_currency_sync / nf_get_valuta_base", "Attenzione: Non è stata impostata la valuta base per l'esercizio corrente.", Exclamation!)
      //n_err_mgr.nf_accoda(53709)
	end if
end if

return lc_retval

end function

public function character nf_get_valuta_base ();// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  05/02/99  [ANT]    OverLoad della funzione nf_get_valuta_base.
//
// *****************************************************************************************
string	ls_msg[]
long 		ll_id_esercizio_proposto
char 		lc_retval = ""

select	id_esercizio_proposto
	into	:ll_id_esercizio_proposto
	from	dba.azienda
	where id_azienda = 1
	using it_tran;
	
CHOOSE CASE it_tran.SQLCode
	CASE 0
		lc_retval = nf_get_valuta_base(ll_id_esercizio_proposto)
	CASE 100
		//("n_currency_sync / nf_get_valuta_base", "Attenzione: non esiste una azienda con id_azienda = 1.", Exclamation!)
		RETURN lc_retval
	CASE -1
		ls_msg[1] = it_tran.sqlerrtext
      //n_err_mgr.nf_accoda(53710,ls_msg)
      g_mb.messagebox("n_currency_sync / nf_get_valuta_base", "Errore durante la lettura dell'esercizio proposto dell'azienda.", Exclamation!)
		RETURN lc_retval
END CHOOSE

RETURN lc_retval
end function

public function integer nf_cambia_valuta_in (long al_id_valuta, decimal adc_cambio);// --------------------------------------------------------------------------------
//	Funzione				nf_cambia_valuta_in
//	Accesso				public
//	Argomenti			long		al_id_valuta		id valuta da sistemare
//							decimal	adc_cambio			cambio eventalmente diverso
//
//	Ritorno				integer		0=Ok -1=Errore
//
//	Descrizione:
//	Funziona come set_valuta_in, ma solo che si attiva in caso di DIVERSITA'. Se
// si deve cambiare la valuta in se stessa, bypassa l'operazione e non legge dal DB
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		22-06-1999		Prima stesura
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

integer		li_ret

if (isnull(al_id_valuta)) or (al_id_valuta<0) then
	return -1
end if

if (al_id_valuta<>istr_curr_in.l_idval) or (isnull (istr_curr_in.l_idval)) then
	li_ret = nf_set_valuta_in (al_id_valuta, adc_cambio)
else
	li_ret = 0
end if

if (not isNull(adc_cambio)) then
	if (adc_cambio<>istr_curr_in.dc_cambio) or (isnull(istr_curr_in.dc_cambio)) then
		istr_curr_in.dc_cambio = adc_cambio
	end if
end if

return li_ret


end function

public function integer nf_cambia_valuta_out (long al_id_valuta, decimal adc_cambio, integer ai_posiz);// --------------------------------------------------------------------------------
//	Funzione				nf_cambia_valuta_out
//	Accesso				public
//	Argomenti			long		al_id_valuta		id valuta da sistemare
//							decimal	adc_cambio			cambio eventalmente diverso
//							integer	ai_posiz				posizione nel vettore di valute
//
//	Ritorno				integer		0=Ok -1=Errore
//
//	Descrizione:
//	Funziona come set_valuta_out, ma solo che si attiva in caso di DIVERSITA'. Se
// si deve cambiare la valuta in se stessa, bypassa l'operazione e non legge dal DB
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		22-06-1999		Prima stesura
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

integer		li_ret, li_size

if (isnull(al_id_valuta)) or (al_id_valuta<0) then
	return -1
end if

li_size = upperbound(istr_curr_out)
if li_size<ai_posiz then
	li_ret = nf_set_valuta_out (al_id_valuta, adc_cambio, ai_posiz)
else
	if (al_id_valuta<>istr_curr_out[ai_posiz].l_idval) or (isnull(istr_curr_out[ai_posiz].l_idval)) then
		li_ret = nf_set_valuta_out (al_id_valuta, adc_cambio, ai_posiz)
	else
		li_ret = 0
	end if
end if


if (not isNull(adc_cambio)) then
	if (adc_cambio<>istr_curr_out[ai_posiz].dc_cambio) or (isnull(istr_curr_out[ai_posiz].dc_cambio)) then
		istr_curr_out[ai_posiz].dc_cambio = adc_cambio
	end if
end if

return li_ret

end function

public function integer nf_converti_valore (decimal adc_in, ref decimal adc_out[]);// --------------------------------------------------------------------------------
//	Funzione				nf_converti_valore
//	Accesso				public
//	Argomenti			decimal		adc_in			valore da convertire
//							decimal		adc_out[]		valori convertiti
//
//	Ritorno				integer		0=Ok -1=Errore
//
//	Descrizione:
// Versione in overload che chiama quella standard. Permette di usare la conver-
// sione in modo più facile, sapendo di avere già impostato le valute
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		22-06-1999		Prima stesura
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------
integer		li_ret
long			ll_null
decimal		ldc_null

setnull (ldc_null)
setnull(ll_null)

li_ret = nf_converti_valore (ll_null, ldc_null, adc_in, ll_null, ldc_null, adc_out[])

return li_ret

end function

public function decimal nf_round_valuta_in (decimal adc_in);// --------------------------------------------------------------------------------
//	Funzione				nf_round_valuta_in
//	Accesso				public
//	Argomenti			decimal		adc_in		valore in ingresso
//
//	Ritorno				decimal		valore convertito
//
//	Descrizione:
//	questa funzione arrotonda la valuta in ingresso. Può servire
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		22-06-1999		Prima stesura
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

decimal		ldc_ret
string		ls_tipo
integer		li_cifre

ls_tipo = istr_curr_in.s_tipoarr
li_cifre = istr_curr_in.i_cifrearr

ldc_ret = nf_round(adc_in,ls_tipo,li_cifre)

return ldc_ret

end function

protected function decimal nf_conv_kernel_nornd (ref ss_valuta astr_in, ref ss_valuta astr_out, decimal adc_in, integer ai_tipo);// --------------------------------------------------------------------------------
//	Funzione				nf_conv_kernel_nornd
//	Accesso				protected
//	Argomenti			ss_valuta	astr_in		struttura valuta ingresso
//							ss_valuta	astr_out		struttura valuta uscita
//							decimal		adc_in		valore in ingresso
//							integer		ai_tipo		tipo di algoritmo (codificato)
//
//	Ritorno				decimal		valore convertito
//
//	Descrizione:
//	Questa funzione realizza il nucleo vero e proprio della conversione; viene pas-
// sato, oltre che il valore di ingresso, anche un tipo di conversione, secondo lo
// schema riportato nel seguito. Inoltre vengono passati anche i parametri necessari
// al calcolo, direttamente come puntatore a struttura.
//
// QUESTA FUNZIONE NON ESEGUE IL ROUND DEI RISULTATI alle cifre della valuta di uscita
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		21-12-1998		Prima stesura
// 1.1		[ANT]		05/02/1999     Modificate le modalità di conversione 1, 2, 3, 4, 7
//                                  alla luce del fatto che tutti i cambi ufficali sono
//                                  espressi con il rapporto 1 Euro = X Unità Valutarie.
//	1.2		[PRO]		21-07-1999		Attuata la conversione anche per valute uguali, extra
//												comunitarie ma con cambi diversi
//												Tolto anche il codice errato corretto al punto 1.1
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

decimal		ldc_ret



// --------------------------------------------------------------------------------
// condizionamento e controllo valori di ingresso necessari
// il problema è che in diverse condizioni mi servono cose diverse. Pertanto i
// controlli possono essere effettuati solo in loco (tranne alcuni :-) )
// --------------------------------------------------------------------------------
if isNull(adc_in) then adc_in=0
if adc_in=0 then
	return 0
end if

// --------------------------------------------------------------------------------
// devono esserci tutte e due le valute!
// --------------------------------------------------------------------------------
if isNull (astr_in.l_idval) or (astr_in.l_idval<=0) or &
	isNull (astr_out.l_idval) or (astr_out.l_idval<=0) then
	setNull (ldc_ret)
	return ldc_ret
end if

// --------------------------------------------------------------------------------
// se le valute sono identiche non si fa nulla
// [PRO] 21-07-1999 Se però sono extracomunitarie guardo anche i cambi
// --------------------------------------------------------------------------------
if astr_in.l_idval = astr_out.l_idval then
	if astr_in.s_classe = 'X' then
		if astr_in.dc_cambio=astr_out.dc_cambio then
			ldc_ret = adc_in
			return (ldc_ret)
		end if
	else
		ldc_ret = adc_in
		return (ldc_ret)
	end if
end if


// --------------------------------------------------------------------------------
//	Tipi di valuta
//
//	E = Euro
//	I = Intracomunitaria
//	N = Niente
//	
//	VLx = Valore x
//	Cx = Cambio x (i cambi sono sempre sull'Euro)
//	PEx = Parità Euro x
//	Ax = Arrotondamento x (tipo e cifre)
//	
//	modalità di conversione:
//	
//	1. N -> N			2. N -> E			3. N -> I
//	4. E -> N			5. E -> E			6. E -> I
//	7. I -> N			8. I -> E			9. I -> I
//	
//	
//	1. Da valuta non comunitaria ad altra non comunitaria (es.: Dollari-Yen)
//		Se V1=V2 allora VL2 = VL1
//		Se V1<>V2 allora VL2 = VL1*C1/C2 --> A2
//		
//	2. Da valuta non comunitaria ad EURO
//		VL2 = Vl1*C1 --> A2 (2 cifre, matematico)
//	
//	3.	Da valuta non comunitaria ad una valuta Nazionale (es.: Dollari-Franchi)
//		TEMP = VL1*C1;
//		VL2 = TEMP*PE2 --> A2
//		
//	4. Da EURO a valuta non comunitaria
//		VL2 = VL1/C2 --> A2
//	
//	5. Da Euro a Euro
//		VL2 = VL1
//	
//	6. Da Euro a valuta Nazionale
//		VL2 = VL1*PE2 --> A2		(Nota: PE1 = 1)
//	
//	7. Da valuta Nazionale ad altra Non comunitaria
//		TEMP = VL1/PE1
//		VL2 = TEMP/C2 --> A2
//		
//	8. Da valuta Nazionale ad EURO
//		VL2 = VL1/PE1 --> A2		(Nota: PE2 = 1)
//	
//	9. Da una valuta nazionale ad altra nazionale
//		TEMP = VL1/PE1
//		TEMP=TEMP arrotondato a N cifre decimali (minimo 3)
//		VL2 = TEMP*PE2 --> A2
//
// --------------------------------------------------------------------------------


choose case ai_tipo
		
	case 11		// N->N supposte diverse
		if (isNull(astr_in.dc_cambio)) or (astr_in.dc_cambio<=0) or isNull (astr_out.dc_cambio) or (astr_out.dc_cambio<=0) then
			setnull(ldc_ret)
		else
			ldc_ret = adc_in/astr_in.dc_cambio*astr_out.dc_cambio
		end if
		
	case 12		// N->E
		if (isNull(astr_in.dc_cambio)) or (astr_in.dc_cambio<=0) then
			setnull(ldc_ret)
		else
			ldc_ret = adc_in/astr_in.dc_cambio
		end if
		
	case 13		// N->I
		if (isNull(astr_in.dc_cambio)) or (astr_in.dc_cambio<=0) or isNull (astr_out.dc_parita) or (astr_out.dc_parita<=0) then
			setnull (ldc_ret)
		else
			ldc_ret = adc_in/astr_in.dc_cambio
			ldc_ret = ldc_ret*astr_out.dc_parita
		end if
		
	case 21		// E->N
		if (isNull(astr_out.dc_cambio)) or (astr_out.dc_cambio<=0) then
			setNull (ldc_ret)
		else
			ldc_ret = adc_in*astr_out.dc_cambio
		end if

	case 22		// E->E
		ldc_ret = adc_in
		
	case 23		// E->I
		if (isNull(astr_out.dc_parita)) or (astr_out.dc_parita<=0) then
			setNull (ldc_ret)
		else
			ldc_ret = adc_in*astr_out.dc_parita
		end if
		
	case 31		// I->N
		if (isNull(astr_out.dc_cambio)) or (astr_out.dc_cambio<=0) or (isNull(astr_in.dc_parita)) or (astr_in.dc_parita<=0) then
			setNull(ldc_ret)
		else
			ldc_ret = adc_in/astr_in.dc_parita
			ldc_ret = ldc_ret*astr_out.dc_cambio
		end if
		
	case 32		// I->E
		if (isNull(astr_in.dc_parita)) or (astr_in.dc_parita<=0) then
			setNull(ldc_ret)
		else
			ldc_ret = adc_in/astr_in.dc_parita
		end if
		
	case 33		// I->I supposte diverse
		if (isNull(astr_in.dc_parita)) or (astr_in.dc_parita<=0) or (isNull(astr_out.dc_parita)) or (astr_out.dc_parita<=0) then
			setnull(ldc_ret)
		else
			ldc_ret = adc_in/astr_in.dc_parita
			ldc_ret = nf_round(ldc_ret,'I',ii_euroarr)
			ldc_ret = ldc_ret*astr_out.dc_parita
		end if
		
	case else
		setNull (ldc_ret)
end choose

return ldc_ret

end function

public function integer nf_converti_valore_nornd (long al_idval, decimal adc_cambio, decimal adc_valore, long al_idvalout, decimal adc_chgout, ref decimal adc_out[]);// --------------------------------------------------------------------------------
//	Funzione				nf_converti_valore_nornd
//	Accesso				public
//	Argomenti			long			al_idval			id valuta ingresso
//							decimal		adc_cambio		cambio valuta ingresso (se diverso)
//							decimal		adc_valore		valore da convertire
//							long			al_idvalout		id valuta di uscita
//							decimal		adc_chgout		cambio valuta uscita (se diverso)
//							decimal		adc_out[]		valori convertiti
//
//	Ritorno				integer		0=Ok -1=Errore
//
//	Descrizione:
//	Funzione identica alla nf_converti_valore, tranne per il fatto che NON ESEGUE
// conversioni, poiché chiama nf_conv_kernel_NORND
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		28-07-1999		Prima stesura (copiando da nf_converti_valore)
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------
boolean		lb_valid
integer		li_ret, li_size, li_i, li_digits
integer		li_tipo_val_in, li_tipo_val_out, li_comb
decimal		ldc_par, ldc_chg
string		ls_mex, ls_clas, ls_msg[]

// --------------------------------------------------------------------------------
// 1. prima fase di controlli interni
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
// ci deve essere il record di descrizione di Euro e Lire messo bene
lb_valid = nf_check_valid()
if not lb_valid then
	ls_msg[1] = "Le valute Lira ed Euro non sono caricate correttamente nel database,"
	ls_msg[2] = "oppure vi sono stati errori nel loro caricamento. Si continuerà con dei"
	ls_msg[3] = "valori di default ma non necessariamente validi!"
	post function nf_accoda (53706,ls_msg)
end if


// --------------------------------------------------------------------------------
// 2. seconda fase di controllo e condizionamento parametri
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
// un Null viene preso come zero 
if isNull (adc_valore) then
	adc_valore = 0
end if
// --------------------------------------------------------------------------------
// in caso di ingresso a zero si ritornano tutti zeri e si esce
if adc_valore=0 then
	li_size = upperBound(istr_curr_out)
	adc_out[1] = 0		// euro
	adc_out[2] = 0		// lire
	for li_i=1 to li_size
		adc_out[2+li_i] = 0	// gli altri
	next
	return 0
end if


li_ret = 0
// --------------------------------------------------------------------------------
// se è passata una valuta in ingresso la si imposta
if (not isNull(al_idval)) and (al_idval>0) then
	if al_idval<>istr_curr_in.l_idval then
		nf_set_valuta_in (al_idval, adc_cambio)
	end if
	if adc_cambio<>istr_curr_in.dc_cambio then
		nf_set_valuta_in (al_idval, adc_cambio)
	end if
end if


// --------------------------------------------------------------------------------
// se è passata una valuta in uscita la si imposta per la prima struttura
if (not isNull(al_idvalout)) and (al_idvalout>0) then
	if upperbound(istr_curr_out)<1 then
		nf_set_valuta_out (al_idvalout,adc_chgout,1)
	end if
	if al_idvalout<>istr_curr_out[1].l_idval then
		nf_set_valuta_out (al_idvalout,adc_chgout,1)
	end if
	if adc_chgout<>istr_curr_out[1].dc_cambio then
		nf_set_valuta_out (al_idvalout,adc_chgout,1)
	end if
end if


// --------------------------------------------------------------------------------
// riconoscimento del tipo di valuta in ingresso: N, I, E
// --------------------------------------------------------------------------------
ldc_par = istr_curr_in.dc_parita
ldc_chg = istr_curr_in.dc_cambio
ls_clas = istr_curr_in.s_classe
li_tipo_val_in = nf_tipo_valuta(ls_clas)

// --------------------------------------------------------------------------------
// tutte le conversioni avvengono solamente se le valute sono diverse, tranne il
// caso di valuta extracomunitaria con due cambi diversi. In ogni caso, le conver-
// sioni sono a carico della funzione nf_conv_kernel, che si occupa di tutto
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
// conversione in Euro
// --------------------------------------------------------------------------------
if istr_curr_in.l_idval <> istr_curr_euro.l_idval then
	li_tipo_val_out = 2		// 2 = Euro
	li_comb = 10 * li_tipo_val_in + li_tipo_val_out
	adc_out[1] = nf_conv_kernel_nornd (istr_curr_in, istr_curr_euro, adc_valore, li_comb)
else
	adc_out[1] = adc_valore
end if

// --------------------------------------------------------------------------------
// conversione in Lire. La conversione in lire viene sempre fatta, tranne che quando
// si parte dalle lire, passando per l'euro. Di questo si occupa la nf_conv_kernel
// --------------------------------------------------------------------------------
if istr_curr_in.l_idval <> istr_curr_lire.l_idval then
	li_tipo_val_out = 3		// Lire
	li_comb = 10 * li_tipo_val_in + li_tipo_val_out
	adc_out[2] = nf_conv_kernel_nornd (istr_curr_in, istr_curr_lire, adc_valore, li_comb)
else
	adc_out[2] = adc_valore
end if

// --------------------------------------------------------------------------------
// conversione per le altre eventuali divise
// combinazioni valide: 11, 12, 13,    21, 22, 23,     31, 32, 33
// vengono ritornate da nf_tipo_valuta per in e out e combinate in li_comb
// --------------------------------------------------------------------------------
li_size = upperBound(istr_curr_out)
for li_i=1 to li_size
	// -----------------------------------------------------------------------------
	// [PRO] 21-07-1999 Se le valute sono uguali, ma i cambi diversi, devo lo stesso
	// convertire. Di questo controllo si occupa ora la nf_conv_kernel. Da qui si
	// invoca e basta (anche se c'è un leggero calo di prestazioni)
	// -----------------------------------------------------------------------------
	ldc_par = istr_curr_out[li_i].dc_parita
	ldc_chg = istr_curr_out[li_i].dc_cambio
	ls_clas = istr_curr_out[li_i].s_classe
	li_tipo_val_out = nf_tipo_valuta(ls_clas)
	li_comb = 10 * li_tipo_val_in + li_tipo_val_out
	adc_out[2+li_i] = nf_conv_kernel_nornd (istr_curr_in, istr_curr_out[li_i], adc_valore, li_comb)
next


return li_ret

end function

public function integer nf_converti_valore_nornd (decimal adc_in, ref decimal adc_out[]);// --------------------------------------------------------------------------------
//	Funzione				nf_converti_valore_nornd
//	Accesso				public
//	Argomenti			decimal		adc_in			valore da convertire
//							decimal		adc_out[]		valori convertiti
//
//	Ritorno				integer		0=Ok -1=Errore
//
//	Descrizione:
// Versione in overload che chiama quella standard. Permette di usare la conver-
// sione in modo più facile, sapendo di avere già impostato le valute
// Questa funzione agisce come la sorella ma SENZA ESEGUIRE GLI ARROTONDAMENTI
// ALLA VALUTA FINALE
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		28-07-1999		Prima stesura
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

integer		li_ret
long			ll_null
decimal		ldc_null

setnull (ldc_null)
setnull(ll_null)

li_ret = nf_converti_valore_nornd (ll_null, ldc_null, adc_in, ll_null, ldc_null, adc_out[])

return li_ret

end function

public function boolean nf_valute_uguali (long al_idv1, decimal adc_c1, long al_idv2, decimal adc_c2);// --------------------------------------------------------------------------------
//	Funzione				nf_valute_uguali
//	Accesso				public
//	Argomenti			long		al_idv1			id valuta 1 del confronto
//							decimal	adc_c1			cambio 1 del confronto
//							long		al_idv2			id valuta 2 del confronto
//							decimal	adc_c2			cambio 2 del confronto
//
//	Ritorno				boolean						true = valute uguali
//
//	Descrizione:
//	Indica se le valute (con cambi eventualmente imposti) sono "uguali" o "diverse"
//
//	Nota:
//	il cambio ha senso solamente per le valute extracomunitarie e non viene preso
// in considerazione in altri casi
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		29-07-1999		Prima stesura
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------


boolean		lb_ret
string		ls_mex[], ls_tipo
decimal		ldc_c1, ldc_c2, ldc_p1, ldc_p2


// --------------------------------------------------------------------------------
// valuta numero 1
// --------------------------------------------------------------------------------
select	cambio_euro,	parita_euro,	liraeuro
	into	:ldc_c1, :ldc_p1, :ls_tipo
	from	dba.valuta
	where	id_valuta = :al_idv1
	using it_tran;

if it_tran.SQLCode<>0 then
	ls_mex[1] = it_tran.SQLErrText
	post function nf_accoda(53712,ls_mex)
	setnull(lb_ret)
end if

if ls_tipo="X" then
	if (not isnull(adc_c1)) and (adc_c1>0) and (adc_c1<>ldc_c1) then
		ldc_c1=adc_c1
	end if
end if

// --------------------------------------------------------------------------------
// valuta numero 2
// --------------------------------------------------------------------------------
select	cambio_euro,	parita_euro,	liraeuro
	into	:ldc_c2, :ldc_p2, :ls_tipo
	from	dba.valuta
	where	id_valuta = :al_idv2
	using it_tran;

if it_tran.SQLCode<>0 then
	ls_mex[1] = it_tran.SQLErrText
	post function nf_accoda(53712,ls_mex)
	setnull(lb_ret)
end if

if ls_tipo="X" then
	if (not isnull(adc_c2)) and (adc_c2>0) and (adc_c2<>ldc_c2) then
		ldc_c2=adc_c2
	end if
end if

// --------------------------------------------------------------------------------
// confronto
// --------------------------------------------------------------------------------
if (al_idv1=al_idv2) and (ldc_c1=ldc_c2) then
	lb_ret = true
else
	lb_ret = false
end if

return lb_ret

end function

public function integer nf_tipo_valuta (string as_tipo);// --------------------------------------------------------------------------------
//	Funzione				nf_tipo_valuta
//	Accesso				protected
//	Argomenti			string		as_classe			classe della valuta
//
//	Ritorno				integer		1=Non comun., 2=Euro, 3=Intracom., -1=Errore
//
//	Descrizione:
//	Ritorna un numero da 1 a 3 oppure -1 se si tratta di errore.Fornisce indicazioni
// sul tipo di conversione da usare
//
// Note:
// Nella revisione 2, di fatto, non sarebbe quasi necessaria la funzione, tuttavia
// la si tiene per mascherare il modo con cui si calcola il tutto
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		21-12-1998		Prima stesura
// 2.0		[PRO]		04-02-1999		Modifica per riconoscimento diverso delle valute
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

string		ls_1, ls_dot
integer		li_digits, li_ret

// --------------------------------------------------------------------------------
// riconoscimento del tipo di valuta in ingresso: N, I, E
//
// - Una valuta è intracomunitaria se ha parità Euro definita e positiva;
//   questa parità Euro dovrebbe avere sei cifre significative;
//   il controllo delle cifre viene soppresso e lasciato alla gestione delle valute
// - Una valuta è Euro se ha la 'E', nel qual caso deve vere parità e cambio pari a 1
// - Una valuta è extracomunitaria quando la sua classe è 'X'; in tal caso deve avere
//   un cambio
//
// --------------------------------------------------------------------------------
choose case as_tipo
	case 'I','C'
		li_ret = 3
	case 'E'
		li_ret = 2
	case 'X'
		li_ret = 1
	case else
		li_ret = -1
end choose

return li_ret

end function

public function integer nf_accoda (long al_code);string ls_msg

CHOOSE CASE al_code
   CASE 53701 
      ls_msg = "Parametri errati in ricerca valuta base."
   CASE 53709 
      ls_msg = "Attenzione: Non è stata impostata la valuta base per l'esercizio corrente."
END CHOOSE

RETURN g_mb.messagebox("n_currency_sync", ls_msg, StopSign!, Ok!)

//return n_err_mgr.nf_accoda(al_code)

end function

public function integer nf_accoda (long al_code, string as_mex[]);string ls_msg

CHOOSE CASE al_code
   CASE 53702 
      ls_msg = "Impossibile caricare valuta base per l'esercizio scelto: " + as_mex[1]
   CASE 53703 
      ls_msg = "Errore : " + as_mex[1]
   CASE 53704 
      ls_msg = "Impossibile caricare valuta IN: " + as_mex[1]
   CASE 53705 
      ls_msg = "Impossibile caricare valuta OUT: " + as_mex[1]
   CASE 53706 
      ls_msg = "Attenzione : "  + as_mex[1] + " "  + as_mex[2] + " "  + as_mex[3]
   CASE 53707 
      ls_msg = "Attenzione: non esiste un esercizio con ID " +  + as_mex[1]
   CASE 53708 
      ls_msg = "Errore durante la lettura della valuta base dell'esercizio. : " + as_mex[1]
   CASE 53710 
      ls_msg = "Errore durante la lettura dell'esercizio proposto dell'azienda. : " + as_mex[1]
   CASE 53711 
      ls_msg = "Errore nel costruttore di n_currency_sync : "  + as_mex[1]
   CASE 53712 
      ls_msg = "Impossibile caricare la valuta 1~r "  + as_mex[1]
END CHOOSE

RETURN g_mb.messagebox("n_currency_sync", ls_msg, StopSign!, Ok!)

//return n_err_mgr.nf_accoda(al_code,as_mex)

end function

public subroutine uf_set_transaction (transaction af_tran);it_tran = af_tran
end subroutine

on n_currency_sync.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_currency_sync.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;call super::constructor;// --------------------------------------------------------------------------------
//	Oggetto				n_currency_sync
//
//	Descrizione:
//	Questo oggetto serve a sincronizzare delle valute; si tratta di un convertitore
// integrato simultaneo di valute; è programmato per ottemperare alle direttive
// della regolamentazione dell'Articolo 235.
// --------------------------------------------------------------------------------
//
//	Revisioni
//
//	1.0		[PRO]		21-12-1998		Prima stesura
// 1.1      [ANT]    04-02-1999     Sostituite le chiamate alle funzioni di sistema
//                                  MessageBox con le chiamate in post alla funzione
//                                  nf_MessageBox.
// --------------------------------------------------------------------------------
//	Copyright © 1998 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------



// --------------------------------------------------------------------------------
// variabili di istanza
//
// ib_valido		boolean		l'oggetto è valido e funzionante
//	is_valaz			string		[I|E] indica se la valuta aziendale è Lire o Euro
//	ii_euroarr		integer		numero di cifre per gli arrotondamenti intermedi
//
// istr_curr_in	ss_valuta	caratteristiche della valuta "in ingresso"
//	istr_curr_out	ss_valuta[]	caratteristiche delle valute "in uscita"
// istr_curr_lire	ss_valuta	caratteristiche della lira come valuta
// istr_curr_euro	ss_valuta	caratteristiche dell'euro come valuta
//
// --------------------------------------------------------------------------------
long		ll_id_valuta
string	ls_mex[]

is_valaz = 'E'
ib_valido = true
ii_euroarr = 5

it_tran = sqlci

istr_curr_euro.s_codice = "EURO!"
istr_curr_euro.s_simbolo = "EUR"
istr_curr_euro.dc_cambio = 1
istr_curr_euro.dc_parita = 1
istr_curr_euro.s_tipoarr = "M"
istr_curr_euro.i_cifrearr = 2
istr_curr_euro.s_classe = 'E'


// --------------------------------------------------------------------------------
// Impostazione valori per la valuta "LIRE" che deve sempre esistere nel sistema
// --------------------------------------------------------------------------------
select	id_valuta,					codice,
			simbolo,						cambio_euro,
			parita_euro,				tipo_arrotondamento,
			cifra_arrotondamento,	liraeuro
	into	:istr_curr_lire.l_idval, 		:istr_curr_lire.s_codice,
			:istr_curr_lire.s_simbolo,		:istr_curr_lire.dc_cambio,
			:istr_curr_lire.dc_parita,		:istr_curr_lire.s_tipoarr,
			:istr_curr_lire.i_cifrearr,	:istr_curr_lire.s_classe
	from	dba.valuta
	where	liraeuro = 'I'
	using it_tran;
	
if (it_tran.sqlcode=0) and (not isnull(istr_curr_lire.l_idval)) and (istr_curr_lire.l_idval>0) then
	// do nothing, all ok
else
	//"Errore in costruttore currency_sync:~r~r"+it_tran.SQLErrText
	ls_mex[1] = it_tran.SQLErrText
	post function nf_accoda(53711,ls_mex)
	ib_valido = false
end if


// --------------------------------------------------------------------------------
// Impostazione valori per la valuta "EURO" che deve sempre esistere nel sistema
// --------------------------------------------------------------------------------
select	id_valuta,					codice,
			simbolo,						cambio_euro,
			parita_euro,				tipo_arrotondamento,
			cifra_arrotondamento,	liraeuro
	into	:istr_curr_euro.l_idval, 		:istr_curr_euro.s_codice,
			:istr_curr_euro.s_simbolo,		:istr_curr_euro.dc_cambio,
			:istr_curr_euro.dc_parita,		:istr_curr_euro.s_tipoarr,
			:istr_curr_euro.i_cifrearr,	:istr_curr_euro.s_classe
	from	dba.valuta
	where	liraeuro = 'E'
	using it_tran;
	
if (it_tran.sqlcode=0) and (not isnull(istr_curr_euro.l_idval)) and (istr_curr_euro.l_idval>0) then
	// do nothing, all ok
else
	//"Errore in costruttore currency_sync:~r~r"+it_tran.SQLErrText
	ls_mex[1] = it_tran.SQLErrText
	post function nf_accoda(53711,ls_mex)
	ib_valido = false
end if

if nf_set_valuta_base(today()) <0 then
	ib_valido = false
end if

// --------------------------------------------------------------------------------
// controlli di consistenza
// --------------------------------------------------------------------------------
nf_check_valid()


return 0


end event

