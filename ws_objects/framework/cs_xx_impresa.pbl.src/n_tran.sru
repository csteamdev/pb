﻿$PBExportHeader$n_tran.sru
forward
global type n_tran from transaction
end type
end forward

global type n_tran from transaction
end type
global n_tran n_tran

type prototypes
subroutine sp_trova_esercizio(decimal ai_anno,decimal ai_mese,long ai_id_azienda,ref long ai_id_esercizio) RPCFUNC ALIAS FOR "~"dba~".~"sp_trova_esercizio~""
subroutine sp_elimina_reg_pd(long ai_id_reg_pd,ref long ai_retval) RPCFUNC ALIAS FOR "~"dba~".~"sp_elimina_reg_pd~""

end prototypes

type variables
string oracle_dateformat=""
end variables

forward prototypes
public function long nf_trova_esercizio (decimal ai_anno, decimal ai_mese, long ai_id_azienda, ref long ai_id_esercizio)
public function long nf_elimina_reg_pd (long ai_id_reg_pd, ref long ai_retval)
end prototypes

public function long nf_trova_esercizio (decimal ai_anno, decimal ai_mese, long ai_id_azienda, ref long ai_id_esercizio);// -----------------------------------------------------------------------------------------
// [GIO] 09/08/00 PER SQL7 BISOGNA INIZIALIZZARE LE VARIABILI DI RITORNO
// -----------------------------------------------------------------------------------------
if isnull(ai_id_esercizio) then
	ai_id_esercizio	= 0
end if

this.sp_trova_esercizio(ai_anno,ai_mese,ai_id_azienda,ai_id_esercizio)

if ai_id_esercizio = 0 then
	setnull(ai_id_esercizio)
end if

return this.sqlcode

end function

public function long nf_elimina_reg_pd (long ai_id_reg_pd, ref long ai_retval);// -----------------------------------------------------------------------------------------
// [GIO] 09/08/00 PER SQL7 BISOGNA INIZIALIZZARE LE VARIABILI DI RITORNO
// -----------------------------------------------------------------------------------------
if isnull(ai_retval) then ai_retval=0

this.sp_elimina_reg_pd(ai_id_reg_pd,ai_RetVal)
return this.sqlcode


end function

on n_tran.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_tran.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

