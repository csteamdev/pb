﻿$PBExportHeader$u_reg_pd_base.sru
$PBExportComments$[ANT] - Oggetto per la creazione di registrazioni contabili (da ereditare).
forward
global type u_reg_pd_base from nonvisualobject
end type
end forward

global type u_reg_pd_base from nonvisualobject
end type
global u_reg_pd_base u_reg_pd_base

type variables
// ----------------- Variabili varie -----------------
integer     ii_DisplayMode             = 2
long        il_esercizio_id_valuta
boolean     ib_AutoCommit           	= TRUE
boolean     ib_crea_mov_scadenza       = FALSE
boolean     ib_crea_cc_mov_contabile   = FALSE
boolean     ib_elimina_esistente       = FALSE
boolean     ib_zero_mov_scadenza       = FALSE
boolean     ib_zero_mov_contabile      = FALSE
boolean     ib_no_mc                   = FALSE
boolean     ib_modifydescontabile      = FALSE
boolean     ib_show_sbilanciata        = TRUE
boolean     ib_allow_negativi          = FALSE // Indica se sono consentiti importi negativi.
boolean     ib_attiva_wizard           = FALSE
boolean     ib_notify_existing         = TRUE
boolean     ib_display_error           = true
boolean     ib_calcola_cc_mov_contabile= false
boolean    	ib_elab_des_contabile      = TRUE
boolean     ib_importa_scadenze        = FALSE
string      is_info 							= ""
string      is_descr_registro 			= ""
string      is_ges_par_aperte_new 		= "N"
string      is_ges_cc_nuova 				= "N"
string      is_ges_ritenute_new
char			ic_rel_num_prog_iva 			= "E"
char        ic_sottomastro_uso 			= ""
decimal     idc_esercizio_cambio


// ----------------- Testata -----------------
long        il_id_reg_pd
boolean     ib_num_righe_no_Scad[]
long        il_id_prof_registrazione= 0
long        il_id_esercizio       	= 0
long        il_id_registro       	= 0
long        il_num_progressivo      = 0
date        id_data_registrazione
date        id_data_dec_pagamento
date        id_data_competenza
date        id_periodo_liquidazione
date        id_periodo_reg_iva
date        id_data_calcolo_scad
string      is_descrizione       	= ""
long        il_id_valuta       		= 0
long        il_id_operatore   		= 0
long        il_id_capo_commessa   	= 0
decimal     idc_importo       		= 0
decimal     idc_imp_valuta       	= 0
decimal     idc_cambio       			= 0
long        il_id_conto
string      is_num_documento       	= ""
string      is_tipo_registro
date        id_data_documento
char        ic_ges_oper_intracomunitarie    = 'N'
long        il_id_reg_pd_padre
long        il_id_azienda       		= 1
long        il_id_documento
long        il_id_mod_registro


// ----------------- Movimenti IVA -----------------
long        il_id_mov_iva[]
decimal     idc_imponibile[] 
long        il_id_tab_iva[]
decimal     idc_imposta[]
decimal     idc_imponibile_omaggi[]
string      is_ges_art74[]

// ----------------- Movimenti Contabili -----------------
boolean     ib_mc_iva[]
boolean     ib_mc_iva_alt[]
char        ic_rettifica
char        ic_segno_mc[]
char        ic_tipo_fattura       = 'N'
date        id_data_fine_competenza[]
date        id_data_inizio_competenza[]
date        id_data_val_banca[]
decimal {6} idc_cambio_mc[]
decimal {6} idc_importo_mc[]
decimal {6} idc_imp_valuta_mc[]
integer     ii_segno_reg_iva       = 1
long        il_id_conto_mc[]
long        il_id_mov_contabile[]
long        il_id_par_aperta[]
long        il_id_scadenza[]
long        il_id_valuta_mc[]
long        il_id_cantiere_mc[]
long        il_id_materiale_mc[]
long        il_id_ubicazione_mc[]
long        il_id_risorsa_mc[]
long        il_id_cc_chiave[]
long        il_prog_stampato[]
s_reg_pd_scadenza            is_reg_pd_scadenza[]
s_reg_pd_mov_scadenza       is_reg_pd_mov_scadenza[]
s_reg_pd_cc_mov_contabile    is_reg_pd_cc_mov_contabile[]
string      is_des_contabile[]
string      is_riferimenti_mc[]
string      is_rif_partita[]
date        id_data_partita[]
decimal     idc_quantita[]

/* ----------------------------------------------------------------------------- */
/* Scadenze                                                                      */
/* ----------------------------------------------------------------------------- */
long        il_id_con_pagamento
boolean     ib_crea_scadenze = FALSE
boolean     ib_up_scadenze = False


// ----------------- Movimenti di Scadenza -----------------
char			ic_esito_pagamento 	= 'P'
string		is_note_mov_scadenza 	= ''

/* ----------------------------------------------------------------------------- */
/* [GIU] 14/02/2007 Gestione Intra                                                */
/* ----------------------------------------------------------------------------- */
string      is_doc_intra = "N"
//u_ds_main   ids_intra
//u_ds_main   ids_mov_intra


// ----------------- Oggetto per conversioni tra valute. -----------------
n_currency_sync	in_currency_sync

// ----------------- Funzioni di utilità -----------------
u_reg_pd_funct	iu_reg_pd_funct

// ----------------- Transazione --------------------------
n_tran it_tran

// ---------  EnMe 26/09/2008 (con Giorgio) ---------------

// ---------  EnMe 26/06/2014 (EnMe) ----------------------
long il_id_wizard_ivacee

// --------	EnMe 25/9/2015 -----
// per ratei risconti
string	is_rateo_risconto[]
boolean ib_disabilita_incasso_automatico


end variables

forward prototypes
public subroutine uf_id_prof_registrazione (long al_id_prof_registrazione)
public subroutine uf_id_esercizio (long al_id_esercizio)
public subroutine uf_data_competenza (date ad_data_competenza)
public subroutine uf_descrizione (string as_descrizione)
public subroutine uf_id_valuta (long al_id_valuta)
public subroutine uf_cambio (decimal adc_cambio)
public subroutine uf_id_conto (long al_id_conto)
public subroutine uf_importo (decimal adc_importo)
public subroutine uf_num_documento (string as_num_documento)
public subroutine uf_data_documento (date ad_data_documento)
public subroutine uf_id_azienda (long al_id_azienda)
public subroutine uf_imponibile (decimal adc_imponibile[])
public subroutine uf_id_tab_iva (long al_id_tab_iva[])
public subroutine uf_imposta (decimal adc_imposta[])
public subroutine uf_id_conto_mc (long al_id_conto_mc[])
public subroutine uf_segno_mc (character ac_segno_mc[])
public subroutine uf_importo_mc (decimal adc_importo_mc[])
public subroutine uf_mc_iva (boolean ab_mc_iva[])
public subroutine uf_id_registro (long al_id_registro)
public subroutine uf_num_progressivo (long al_num_progressivo)
public subroutine uf_data_registrazione (date ad_data_registrazione)
public subroutine uf_displaymode (integer ai_displaymode)
public subroutine uf_rettifica (character ac_rettifica)
public subroutine uf_tipo_fattura (character ac_tipo_fattura)
public subroutine uf_des_contabile (string as_des_contabile[])
public subroutine uf_data_val_banca (date ad_data_val_banca[])
public subroutine uf_data_inizio_competenza (date ad_data_inizio_competenza[])
public subroutine uf_data_fine_competenza (date ad_data_fine_competenza[])
public subroutine uf_id_valuta_mc (long al_id_valuta_mc[])
public subroutine uf_cambio_mc (decimal adc_cambio_mc[])
public subroutine uf_ges_oper_intracomunitarie (character ac_ges_oper_intracomunitarie)
public subroutine uf_id_reg_pd_padre (long al_id_reg_pd_padre)
public subroutine uf_imp_valuta (decimal adc_imp_valuta)
public subroutine uf_core_controlla_array (ref any aa_array[], any aa_value, long al_upperbound)
public subroutine uf_prog_stampato (long al_prog_stampato[])
public subroutine uf_id_scadenza (long al_id_scadenza[])
public subroutine uf_crea_mov_scadenza (boolean ab_crea_mov_scadenza)
public function integer uf_mc_bilanciati ()
public subroutine uf_get_id_mov_contabile (ref long al_id_mov_contabile[])
public subroutine uf_esito_pagamento (character ac_esito_pagamento)
public subroutine uf_zero_mov_scadenza (boolean ab_flag)
public subroutine uf_note_mov_scadenza (string as_note)
public subroutine uf_periodo_liquidazione (date ad_periodo_liquidazione)
public function boolean uf_controlla_array ()
public subroutine uf_zero_mov_contabile (boolean ab_flag)
public subroutine uf_id_documento (long al_id_documento)
public subroutine uf_no_mc (boolean ab_flag)
public subroutine uf_id_scadenza (s_reg_pd_mov_scadenza as_reg_pd_mov_scadenza[])
public subroutine uf_mc_iva_alt (boolean ab_mc_iva_alt[])
public function string uf_get_info ()
public subroutine uf_build_info (integer ai_value)
public subroutine uf_id_con_pagamento (long al_id_con_pagamento)
public function long uf_get_id_reg_pd ()
public subroutine uf_riferimenti_mc (string as_riferimenti[])
public subroutine uf_imp_valuta_mc (decimal adc_imp_valuta_mc[])
public function boolean uf_reg_pd_esistente (long al_id_registro, long al_num_progressivo, date ad_data_registrazione)
public subroutine uf_autocommit (boolean ab_autocommit)
public function long uf_proponi_esercizio ()
public subroutine uf_elimina_esistente (boolean ab_elimina_esistente)
public subroutine uf_visualizza_reg_pd (long al_id_reg_pd)
public subroutine uf_visualizza_reg_pd (long al_id_reg_pd[])
public function long uf_elimina (long al_id_reg_pd)
public function long uf_crea ()
public function long uf_aggiorna (long al_id_reg_pd)
public subroutine uf_reset ()
public subroutine uf_id_operatore (long al_id_operatore)
public subroutine uf_imponibile_omaggi (decimal adc_imponibile_omaggi[])
public subroutine uf_importa_scadenze (boolean ab_flag)
public function integer uf_materiale_mc (long al_id_materiale_mc[])
public subroutine uf_modify_des_contabile (boolean ab_value)
public subroutine uf_quantita (decimal adc_quantita[])
public function integer uf_reg_pd_scadenza (s_reg_pd_scadenza as_reg_pd_scadenza[])
public function integer uf_risorsa_mc (long al_id_risorsa_mc[])
public function integer uf_set_data_calcolo_scad (date ad_data)
public function integer uf_set_display_error (boolean ab_value)
public subroutine uf_set_info (long al_val)
public subroutine uf_set_info (long al_val, string as_msg)
public subroutine uf_set_notify_existing (boolean ab_notify_existing)
public subroutine uf_show_sbilanciata (boolean ab_flag)
public function integer uf_sincronizza_banca_azienda (long al_id)
public function integer uf_ubicazione_mc (long al_id_ubicazione_mc[])
public function integer uf_aggiorna_scadenza ()
public function integer uf_aggiungi_fornitore_a_costo (long al_id_conto, ref string as_des_contabile, long al_id_conto_testa)
public subroutine uf_allow_negativi (boolean ab_flag)
public subroutine uf_attiva_wizard (boolean ab_attiva_wizard)
public function integer uf_attiva_wizard (long al_id_reg_pd)
public function long uf_calc_id_con_pagamento ()
public function integer uf_calcola_cc_mov_cont (long al_id_reg_pd)
public function integer uf_calcola_cc_mov_contabile (boolean ab_value)
public function integer uf_cantiere_mc (long al_id_cantiere_mc[])
public function string uf_controlla_conto (long al_id_reg_pd)
public subroutine uf_crea_cc_mov_contabile (boolean ab_crea_cc_mov_contabile)
public function string uf_crea_partite_da_prof ()
public subroutine uf_crea_scadenze (boolean ab_flag)
public function integer uf_crea_scadenze (long al_id_con_pagamento)
public function integer uf_data_dec_pagamento (date ad_data)
public function integer uf_data_partita_mc (date ad_data_partita[])
public subroutine uf_doc_intra (string as_doc_intra)
public function boolean uf_elab_des_contabile ()
public subroutine uf_ges_art74 (string as_ges_art74[])
public function decimal uf_get_tot_imponibile ()
public function decimal uf_get_tot_imponibile_valuta ()
public function decimal uf_get_tot_imposta ()
public function decimal uf_get_tot_imposta_valuta ()
public subroutine uf_id_capo_commessa (long al_id_capo_commessa)
public function integer uf_id_cc_chiave_mc (long al_id_cc_chiave[])
public subroutine uf_id_cc_mov_contabile (s_reg_pd_cc_mov_contabile as_reg_pd_cc_mov_contabile[])
public function boolean uf_isivacee ()
public function integer uf_rif_partita_mc (string as_rif_partita[])
public function integer uf_righe_no_crea_scad (boolean al_righe[])
public subroutine uf_set_transaction (ref transaction at_tran)
public function integer uf_check_rateo_risconto (long al_row, datetime adt_data_inizio, datetime adt_data_fine, long al_id_esercizio)
public function integer uf_crea_incasso_automatico (long al_id_reg_pd)
end prototypes

public subroutine uf_id_prof_registrazione (long al_id_prof_registrazione);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta l'ID del profilo registrazione (il_id_prof_registrazione).
//
// *****************************************************************************************

il_id_prof_registrazione = al_id_prof_registrazione
end subroutine

public subroutine uf_id_esercizio (long al_id_esercizio);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta l'ID dell'esercizio (il_id_esercizio). Se il valore
//                          passato è zero o NULL, imposta l'ID dell'esercizio a quello
//                          dell'azienda.
//
// *****************************************************************************************

il_id_esercizio = al_id_esercizio

SELECT 
   dba.valuta.id_valuta,
   dba.valuta.parita_euro
INTO
   :il_esercizio_id_valuta,
   :idc_esercizio_cambio
FROM
   dba.valuta
   JOIN dba.esercizio
      ON dba.valuta.liraeuro = dba.esercizio.valuta_base
WHERE
   dba.esercizio.id_esercizio = :al_id_esercizio
USING
   it_tran;

end subroutine

public subroutine uf_data_competenza (date ad_data_competenza);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta la data competenza (id_data_competenza).
//
// *****************************************************************************************

id_data_competenza = ad_data_competenza
end subroutine

public subroutine uf_descrizione (string as_descrizione);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta la descrizione (is_descrizione).
//
// *****************************************************************************************

is_descrizione = as_descrizione
end subroutine

public subroutine uf_id_valuta (long al_id_valuta);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta l'ID della valuta (il_id_valuta).
//
// *****************************************************************************************

il_id_valuta = al_id_valuta
end subroutine

public subroutine uf_cambio (decimal adc_cambio);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta il cambio (idc_cambio).
//
// *****************************************************************************************

idc_cambio = adc_cambio
end subroutine

public subroutine uf_id_conto (long al_id_conto);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta l'ID del conto cliente/fornitore (il_id_conto).
//
// *****************************************************************************************

il_id_conto = al_id_conto

SELECT dba.sottomastro.uso
   INTO :ic_sottomastro_uso
   FROM dba.conto
      JOIN dba.sottomastro ON dba.conto.id_sottomastro = dba.sottomastro.id_sottomastro
      JOIN dba.mastro ON dba.sottomastro.id_mastro = dba.mastro.id_mastro
   WHERE dba.conto.id_conto = :al_id_conto
   USING it_tran;
   

end subroutine

public subroutine uf_importo (decimal adc_importo);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta l'importo del documento (idc_importo).
//
// *****************************************************************************************

idc_importo = adc_importo
end subroutine

public subroutine uf_num_documento (string as_num_documento);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta il numero del documento (is_num_documento).
//
// *****************************************************************************************

is_num_documento = as_num_documento
end subroutine

public subroutine uf_data_documento (date ad_data_documento);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta la data del documento (id_data_documento).
//
// *****************************************************************************************

id_data_documento = ad_data_documento
end subroutine

public subroutine uf_id_azienda (long al_id_azienda);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta l'id_azienda della registrazione.
//
// *****************************************************************************************

il_id_azienda = al_id_azienda
end subroutine

public subroutine uf_imponibile (decimal adc_imponibile[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/04/98  [ANT]    Assegna gli imponibili (idc_imposta[]).
//
// *****************************************************************************************

idc_imponibile = adc_imponibile
end subroutine

public subroutine uf_id_tab_iva (long al_id_tab_iva[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/04/98  [ANT]    Assegna gli ID della tabella iva (il_id_tab_iva[]).
//
// *****************************************************************************************

il_id_tab_iva = al_id_tab_iva
end subroutine

public subroutine uf_imposta (decimal adc_imposta[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/04/98  [ANT]    Assegna gli importi delle imposte per le righe dei movimenti
//                          IVA (idc_imposta[]).
//
// *****************************************************************************************

idc_imposta = adc_imposta
end subroutine

public subroutine uf_id_conto_mc (long al_id_conto_mc[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/04/98  [ANT]    Assegna gli ID dei conti dei movimenti contabili (il_id_conto_mc[]).
//
// *****************************************************************************************

il_id_conto_mc = al_id_conto_mc
end subroutine

public subroutine uf_segno_mc (character ac_segno_mc[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/04/98  [ANT]    Imposta il segno degli importi dei movimenti contabili.
//
// *****************************************************************************************

ic_segno_mc = ac_segno_mc
end subroutine

public subroutine uf_importo_mc (decimal adc_importo_mc[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/04/98  [ANT]    Assegna gli importi dei movimenti contabili (idc_importo_mc[]).
//
// *****************************************************************************************

idc_importo_mc = adc_importo_mc
end subroutine

public subroutine uf_mc_iva (boolean ab_mc_iva[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/04/98  [ANT]    Assegna il flag che indica se una riga di movimento contabile
//                          è la riga dell'IVA (ib_mc_iva[]).
//
// *****************************************************************************************

ib_mc_iva = ab_mc_iva
end subroutine

public subroutine uf_id_registro (long al_id_registro);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta l'ID del registro (il_id_registro).
//
// *****************************************************************************************

il_id_registro = al_id_registro


SELECT dba.registro.id_esercizio
   INTO :il_id_esercizio
   FROM dba.registro
   WHERE dba.registro.id_registro = :al_id_registro
   USING it_tran;
   
CHOOSE CASE it_tran.SQLCode
   CASE 0
      uf_id_esercizio(il_id_esercizio)
END CHOOSE


end subroutine

public subroutine uf_num_progressivo (long al_num_progressivo);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta il numero progressivo.
//
// *****************************************************************************************

il_num_progressivo = al_num_progressivo

end subroutine

public subroutine uf_data_registrazione (date ad_data_registrazione);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Imposta la data della registrazione (id_data_registrazione).
//
// *****************************************************************************************

id_data_registrazione = ad_data_registrazione

end subroutine

public subroutine uf_displaymode (integer ai_displaymode);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  16/04/98  [ANT]    Imposta la modalità di visualizzazione della registrazione
//                          generata: 0 - Non visualizza.
//                                    1 - Visualizza automaticamente la registrazione senza
//                                        chiederlo.
//                                    2 - Attiva la MessageBox che chiede se visualizzare la
//                                        registrazione.
//                          Il valore default è 2.
//
// *****************************************************************************************

IF (ai_displaymode >= 0) AND (ai_displaymode <= 2) THEN
	ii_DisplayMode = ai_DisplayMode
ELSE
	ii_DisplayMode = 2
END IF
end subroutine

public subroutine uf_rettifica (character ac_rettifica);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  16/04/98  [ANT]    Imposta il flag che indica se la registrazione che si sta
//                          generando è una rilevazione di un rateo o di un risconto.
//
// *****************************************************************************************

ic_rettifica = ac_rettifica
end subroutine

public subroutine uf_tipo_fattura (character ac_tipo_fattura);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  12/05/98  [ANT]    Imposta il campo Tipo_Fattura.
//
// *****************************************************************************************

ic_tipo_fattura = ac_tipo_fattura

CHOOSE CASE ac_tipo_fattura
	CASE "N","D","S"
		ii_segno_reg_iva = 1
	CASE "C", "A"
		ii_segno_reg_iva = -1
END CHOOSE

end subroutine

public subroutine uf_des_contabile (string as_des_contabile[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  26/05/98  [ANT]    Imposta l'array di tipo stringa di istanza che memorizza le
//                          descrizioni dei movimenti contabili.
//
// *****************************************************************************************

is_des_contabile = as_des_contabile
end subroutine

public subroutine uf_data_val_banca (date ad_data_val_banca[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/05/98  [ANT]    Imposta l'array della data valuta banca dei movimenti contabili.
//
// *****************************************************************************************

id_data_val_banca = ad_data_val_banca
end subroutine

public subroutine uf_data_inizio_competenza (date ad_data_inizio_competenza[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/05/98  [ANT]    Imposta l'array della data inizio competenza dei movimenti
//                          contabili.
//
// *****************************************************************************************

id_data_inizio_competenza = ad_data_inizio_competenza
end subroutine

public subroutine uf_data_fine_competenza (date ad_data_fine_competenza[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/05/98  [ANT]    Imposta l'array della data fine competenza dei movimenti
//                          contabili.
//
// *****************************************************************************************

id_data_fine_competenza = ad_data_fine_competenza
end subroutine

public subroutine uf_id_valuta_mc (long al_id_valuta_mc[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/05/98  [ANT]    Assegna l'array contenente l'id_valuta dei movimenti contabili.
//
// *****************************************************************************************

il_id_valuta_mc = al_id_valuta_mc
end subroutine

public subroutine uf_cambio_mc (decimal adc_cambio_mc[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/05/98  [ANT]    Assegna l'array contenente l'id_valuta dei movimenti contabili.
//
// *****************************************************************************************

idc_cambio_mc = adc_cambio_mc
end subroutine

public subroutine uf_ges_oper_intracomunitarie (character ac_ges_oper_intracomunitarie);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/05/98  [ANT]    Per impostare il flag ges_oper_intracomunitarie
//
// *****************************************************************************************

ic_ges_oper_intracomunitarie = ac_ges_oper_intracomunitarie
end subroutine

public subroutine uf_id_reg_pd_padre (long al_id_reg_pd_padre);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/05/98  [ANT]    Per impostare l'id_reg_pd_padre.
//
// *****************************************************************************************

il_id_reg_pd_padre = al_id_reg_pd_padre
end subroutine

public subroutine uf_imp_valuta (decimal adc_imp_valuta);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/05/98  [ANT]    Per impostare l'importo in valuta da memorizzare in testata.
//
// *****************************************************************************************

idc_imp_valuta = adc_imp_valuta
end subroutine

public subroutine uf_core_controlla_array (ref any aa_array[], any aa_value, long al_upperbound);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/05/98  [ANT]    Cfr. funzione uf_controlla_array.
//
// *****************************************************************************************

long ll_i

IF UpperBound(aa_array) < al_UpperBound THEN
	FOR ll_i = UpperBound(aa_array) + 1 TO al_UpperBound
		aa_array[ll_i] = aa_value
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(aa_array)
	IF IsNull(aa_array[ll_i]) THEN
		aa_array[ll_i] = aa_value
	END IF
NEXT

end subroutine

public subroutine uf_prog_stampato (long al_prog_stampato[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/05/98  [ANT]    Imposta l'array del campo prog_stampato dei movimenti contabili.
//
// *****************************************************************************************

il_prog_stampato = al_prog_stampato
end subroutine

public subroutine uf_id_scadenza (long al_id_scadenza[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  01/06/98  [ANT]    Per impostare l'array degli ID delle scadenze.
//
// *****************************************************************************************

il_id_scadenza = al_id_scadenza
end subroutine

public subroutine uf_crea_mov_scadenza (boolean ab_crea_mov_scadenza);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  01/06/98  [ANT]
//
// *****************************************************************************************

ib_crea_mov_scadenza = ab_crea_mov_scadenza

end subroutine

public function integer uf_mc_bilanciati ();// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/06/98  [ANT]    Controlla che i movimenti contabili siano bilanciati.
//   02  05/02/99  [ANT]    Modificato il controllo sui totali: questi possono essere a zero
//                          (ma non sbilanciati).
//
// *****************************************************************************************

long ll_i
decimal ldc_tot_dare, ldc_tot_avere, ldc_saldo
boolean lb_dare_non_zero = FALSE, lb_avere_non_zero = FALSE // Indicano che c'è almeno un
// importo (in dare e in avere rispettivamente) diverso da zero.
CONSTANT CHAR LCC_DARE = "D"
CONSTANT CHAR LCC_AVERE = "A"

IF (UpperBound(il_id_conto_mc) = UpperBound(ic_segno_mc)) AND &
   (UpperBound(ic_segno_mc) = UpperBound(idc_importo_mc)) THEN
   FOR ll_i = 1 TO UpperBound(il_id_conto_mc)

      CHOOSE CASE ic_segno_mc[ll_i]
         CASE LCC_DARE
				IF idc_importo_mc[ll_i] <> 0 THEN
					lb_dare_non_zero = TRUE
				END IF
            ldc_tot_dare = ldc_tot_dare + idc_importo_mc[ll_i]
         CASE LCC_AVERE
				IF idc_importo_mc[ll_i] <> 0 THEN
					lb_avere_non_zero = TRUE
				END IF
            ldc_tot_avere = ldc_tot_avere + idc_importo_mc[ll_i]
      END CHOOSE

   NEXT
   ldc_saldo = ldc_tot_dare - ldc_tot_avere
//	IF ((lb_dare_non_zero = FALSE) AND (lb_avere_non_zero = FALSE)) OR (ldc_saldo <> 0) THEN
//		/* I movimenti contabili non sono bilanciati o il loro totale è zero. */
//		RETURN -1
//   END IF
	IF (ldc_saldo <> 0) THEN
		/* I movimenti contabili non sono bilanciati. */
		RETURN -1
	END IF

	IF ((lb_dare_non_zero = FALSE) AND (lb_avere_non_zero = FALSE)) THEN
		IF ib_zero_mov_contabile = FALSE THEN
			/* Il totale dei movimenti contabili è zero. */
			RETURN -1
		END IF
   END IF
ELSE
   /* Il numero degli elementi non è uguale per tutti gli array. */
   RETURN -2
END IF

RETURN 0
end function

public subroutine uf_get_id_mov_contabile (ref long al_id_mov_contabile[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  08/10/98  [ANT]    Restituisce l'array contenente gli ID dei movimenti contabili
//                          creati.
//
// *****************************************************************************************

al_id_mov_contabile = il_id_mov_contabile
end subroutine

public subroutine uf_esito_pagamento (character ac_esito_pagamento);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  05/12/98  [ANT]    Imposta la variabile ic_esito_pagamento che memorizza l'esito_pagamento
//                          del movimento di scadenza.
//
// *****************************************************************************************

IF Upper(ac_esito_pagamento) = 'C' OR &
	Upper(ac_esito_pagamento) = 'T' OR &
	Upper(ac_esito_pagamento) = 'E' OR &
	Upper(ac_esito_pagamento) = 'D' OR &
	Upper(ac_esito_pagamento) = 'I' OR &
	Upper(ac_esito_pagamento) = 'P' OR &
	Upper(ac_esito_pagamento) = 'B' THEN

	ic_esito_pagamento = Upper(ac_esito_pagamento)
END IF
end subroutine

public subroutine uf_zero_mov_scadenza (boolean ab_flag);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  09/12/98  [ANT]    Imposta la variabile che indica se i movimenti di scadenza devono
//                          essere creati con importo a zero.
//
// *****************************************************************************************

ib_zero_mov_scadenza = ab_flag
end subroutine

public subroutine uf_note_mov_scadenza (string as_note);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  11/12/98  [ANT]    Imposta la variabile di istanza che memorizza le note dei movimenti
//                          di scadenza.
//
// *****************************************************************************************

is_note_mov_scadenza = as_note
end subroutine

public subroutine uf_periodo_liquidazione (date ad_periodo_liquidazione);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  25/01/99  [ANT]    Assegna la data del periodo liquidazione.
//
// *****************************************************************************************

id_periodo_liquidazione = ad_periodo_liquidazione
end subroutine

public function boolean uf_controlla_array ();// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/05/98  [ANT]    Alcuni degli array dei campi dei movimenti contabili possono
//                          anche non essere passati. Questa funzione ha lo scopo di assegnare
//                          valori a tutti gli array che memorizzano i campi dei movimenti
//                          contabili.
//   02  17/05/99  [ANT]    Aggiunto il controllo dell'array che memorizza i flag per l'assegnazione
//                          del numero 9998 al numero di riga dell'IVA.
//
// *****************************************************************************************

long ll_i, ll_UpperBound

ll_UpperBound = UpperBound(il_id_conto_mc)

IF ib_no_mc = TRUE THEN
	RETURN TRUE
END IF

IF ll_UpperBound = 0 THEN
	RETURN FALSE
END IF

/* Controllo dell'array delle descrizioni dei movimenti contabili. */
//uf_core_controlla_array(is_des_contabile, is_descrizione, ll_UpperBound)
IF UpperBound(is_des_contabile) < ll_UpperBound THEN
	FOR ll_i = UpperBound(is_des_contabile) + 1 TO ll_UpperBound
		is_des_contabile[ll_i] = is_descrizione
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(is_des_contabile)
	IF IsNull(is_des_contabile[ll_i]) THEN
		is_des_contabile[ll_i] = is_descrizione
	END IF
NEXT

/* Controllo dell'array della data valuta banca dei movimenti contabili. */
//uf_core_controlla_array(id_data_val_banca, id_data_registrazione, ll_UpperBound)
IF UpperBound(id_data_val_banca) < ll_UpperBound THEN
	FOR ll_i = UpperBound(id_data_val_banca) + 1 TO ll_UpperBound
		id_data_val_banca[ll_i] = id_data_registrazione
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(id_data_val_banca)
	IF IsNull(id_data_val_banca[ll_i]) THEN
		id_data_val_banca[ll_i] = id_data_registrazione
	END IF
NEXT

/* Controllo dell'array della data inizio competenza dei movimenti contabili. */
//uf_core_controlla_array(id_data_inizio_competenza, id_data_registrazione, ll_UpperBound)
IF UpperBound(id_data_inizio_competenza) < ll_UpperBound THEN
	FOR ll_i = UpperBound(id_data_inizio_competenza) + 1 TO ll_UpperBound
		id_data_inizio_competenza[ll_i] = id_data_registrazione
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(id_data_inizio_competenza)
	IF IsNull(id_data_inizio_competenza[ll_i]) OR (id_data_inizio_competenza[ll_i] = Date("01/01/1900")) THEN
		id_data_inizio_competenza[ll_i] = id_data_registrazione
	END IF
NEXT

/* Controllo dell'array della data fine competenza dei movimenti contabili. */
//uf_core_controlla_array(id_data_fine_competenza, id_data_registrazione, ll_UpperBound)
IF UpperBound(id_data_fine_competenza) < ll_UpperBound THEN
	FOR ll_i = UpperBound(id_data_fine_competenza) + 1 TO ll_UpperBound
		id_data_fine_competenza[ll_i] = id_data_registrazione
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(id_data_fine_competenza)
	IF IsNull(id_data_fine_competenza[ll_i]) OR (id_data_fine_competenza[ll_i] = Date("01/01/1900"))THEN
		id_data_fine_competenza[ll_i] = id_data_registrazione
	END IF
NEXT

/* Controllo dell'array degli id_valuta dei movimenti contabili. */
//uf_core_controlla_array(il_id_valuta_mc, il_id_valuta, ll_UpperBound)
IF UpperBound(il_id_valuta_mc) < ll_UpperBound THEN
	FOR ll_i = UpperBound(il_id_valuta_mc) + 1 TO ll_UpperBound
		il_id_valuta_mc[ll_i] = il_id_valuta
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(il_id_valuta_mc)
	IF IsNull(il_id_valuta_mc[ll_i]) THEN
		il_id_valuta_mc[ll_i] = il_id_valuta
	END IF
NEXT

/* Controllo dell'array del cambio dei movimenti contabili. */
//uf_core_controlla_array(idc_cambio_mc, idc_cambio, ll_UpperBound)
IF UpperBound(idc_cambio_mc) < ll_UpperBound THEN
	FOR ll_i = UpperBound(idc_cambio_mc) + 1 TO ll_UpperBound
		idc_cambio_mc[ll_i] = idc_cambio
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(idc_cambio_mc)
	IF IsNull(idc_cambio_mc[ll_i]) THEN
		idc_cambio_mc[ll_i] = idc_cambio
	END IF
NEXT

/* Controllo dell'array del campo prog_stampato dei movimenti contabili. */
IF UpperBound(il_prog_stampato) < ll_UpperBound THEN
	FOR ll_i = UpperBound(il_prog_stampato) + 1 TO ll_UpperBound
		SetNull(il_prog_stampato[ll_i])
	NEXT
END IF

/* Controllo dell'array che indica quale dei movimenti contabili è la riga dell'IVA. */
IF UpperBound(ib_mc_iva) < ll_UpperBound THEN
	FOR ll_i = UpperBound(ib_mc_iva) + 1 TO ll_UpperBound
		ib_mc_iva[ll_i] = FALSE
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(ib_mc_iva)
	IF IsNull(ib_mc_iva[ll_i]) THEN
		ib_mc_iva[ll_i] = FALSE
	END IF
NEXT

/* Controllo dell'array che indica quale dei movimenti contabili è la riga dell'IVA con
numero 9998. */
IF UpperBound(ib_mc_iva_alt) < ll_UpperBound THEN
	FOR ll_i = UpperBound(ib_mc_iva_alt) + 1 TO ll_UpperBound
		ib_mc_iva_alt[ll_i] = FALSE
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(ib_mc_iva_alt)
	IF IsNull(ib_mc_iva_alt[ll_i]) THEN
		ib_mc_iva_alt[ll_i] = FALSE
	END IF
NEXT

/* Controllo degli array degli ID delle partite aperte. */
IF UpperBound(il_id_par_aperta) < ll_UpperBound THEN
	FOR ll_i = UpperBound(il_id_par_aperta) + 1 TO ll_UpperBound
		SetNull(il_id_par_aperta[ll_i])
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(il_id_par_aperta)
	IF IsNull(il_id_par_aperta[ll_i]) THEN
		SetNull(il_id_par_aperta[ll_i])
	END IF
NEXT

/* Controllo degli array degli ID delle scadenze. */
IF UpperBound(il_id_scadenza) < ll_UpperBound THEN
	FOR ll_i = UpperBound(il_id_scadenza) + 1 TO ll_UpperBound
		SetNull(il_id_scadenza[ll_i])
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(il_id_scadenza)
	IF IsNull(il_id_scadenza[ll_i]) THEN
		SetNull(il_id_scadenza[ll_i])
	END IF
NEXT

/* Controllo dell'array degli ID dei movimenti IVA. */
IF UpperBound(il_id_mov_iva) < ll_UpperBound THEN
	FOR ll_i = UpperBound(il_id_mov_iva) + 1 TO ll_UpperBound
		SetNull(il_id_mov_iva[ll_i])
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(il_id_mov_iva)
	IF IsNull(il_id_mov_iva[ll_i]) THEN
		SetNull(il_id_mov_iva[ll_i])
	END IF
NEXT

/* Controllo dell'array degli ID dei movimenti contabili. */
IF UpperBound(il_id_mov_contabile) < ll_UpperBound THEN
	FOR ll_i = UpperBound(il_id_mov_contabile) + 1 TO ll_UpperBound
		SetNull(il_id_mov_contabile[ll_i])
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(il_id_mov_contabile)
	IF IsNull(il_id_mov_contabile[ll_i]) THEN
		SetNull(il_id_mov_contabile[ll_i])
	END IF
NEXT

/* Controllo dell'array dei riferimenti dei movimenti contabili. */
IF UpperBound(is_riferimenti_mc) < ll_UpperBound THEN
	FOR ll_i = UpperBound(is_riferimenti_mc) + 1 TO ll_UpperBound
		SetNull(is_riferimenti_mc[ll_i])
	NEXT
END IF

/* Controllo dell'array degli importi in valuta dei movimenti contabili. */
IF UpperBound(idc_imp_valuta_mc) < ll_UpperBound THEN
	FOR ll_i = UpperBound(idc_imp_valuta_mc) + 1 TO ll_UpperBound
		SetNull(idc_imp_valuta_mc[ll_i])
	NEXT
END IF


/* Controllo dell'array dei riferimenti partita . */
IF UpperBound(is_rif_partita) < ll_UpperBound THEN
   FOR ll_i = UpperBound(is_rif_partita) + 1 TO ll_UpperBound
      SetNull(is_rif_partita[ll_i])
   NEXT
END IF

/* Controllo dell'array dei riferimenti partita . */
IF UpperBound(id_data_partita) < ll_UpperBound THEN
   FOR ll_i = UpperBound(id_data_partita) + 1 TO ll_UpperBound
      SetNull(id_data_partita[ll_i])
   NEXT
END IF


IF UpperBound(il_id_cantiere_mc) < ll_UpperBound THEN
   FOR ll_i = UpperBound(il_id_cantiere_mc) + 1 TO ll_UpperBound
      SetNull(il_id_cantiere_mc[ll_i])
   NEXT
END IF

IF UpperBound(il_id_risorsa_mc) < ll_UpperBound THEN
   FOR ll_i = UpperBound(il_id_risorsa_mc) + 1 TO ll_UpperBound
      SetNull(il_id_risorsa_mc[ll_i])
   NEXT
END IF

IF UpperBound(il_id_ubicazione_mc) < ll_UpperBound THEN
   FOR ll_i = UpperBound(il_id_ubicazione_mc) + 1 TO ll_UpperBound
      SetNull(il_id_ubicazione_mc[ll_i])
   NEXT
END IF

IF UpperBound(il_id_materiale_mc) < ll_UpperBound THEN
   FOR ll_i = UpperBound(il_id_materiale_mc) + 1 TO ll_UpperBound
      SetNull(il_id_materiale_mc[ll_i])
   NEXT
END IF

IF UpperBound(il_id_cc_chiave) < ll_UpperBound THEN
   FOR ll_i = UpperBound(il_id_cc_chiave) + 1 TO ll_UpperBound
      SetNull(il_id_cc_chiave[ll_i])
   NEXT
END IF

IF UpperBound(is_rateo_risconto) < ll_UpperBound THEN
   FOR ll_i = UpperBound(is_rateo_risconto) + 1 TO ll_UpperBound
      SetNull(is_rateo_risconto[ll_i])
   NEXT
END IF

/*

TEMPLATE

IF UpperBound(aa_array) < ll_UpperBound THEN
	FOR ll_i = UpperBound(aa_array) + 1 TO ll_UpperBound
		aa_array[ll_i] = aa_value
	NEXT
END IF

FOR ll_i = 1 TO UpperBound(aa_array)
	IF IsNull(aa_array[ll_i]) THEN
		aa_array[ll_i] = aa_value
	END IF
NEXT

*/

RETURN TRUE
end function

public subroutine uf_zero_mov_contabile (boolean ab_flag);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  11/02/99  [ANT]    Imposta la variabile di istanza che indica se è consentito creare
//                          movimenti contabili con importo a zero.
//
// *****************************************************************************************

ib_zero_mov_contabile = ab_flag
end subroutine

public subroutine uf_id_documento (long al_id_documento);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  16/02/99  [ANT]    Imposta la variabile di istanza che memorizza l'id_documento di
//                          reg_pd.
//
// *****************************************************************************************

il_id_documento = al_id_documento
end subroutine

public subroutine uf_no_mc (boolean ab_flag);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  08/03/99  [ANT]    Imposta la variabile di istanza che indica se generare o meno i
//                          movimenti contabili.
//
// *****************************************************************************************

ib_no_mc = ab_flag
end subroutine

public subroutine uf_id_scadenza (s_reg_pd_mov_scadenza as_reg_pd_mov_scadenza[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  26/03/99  [ANT]    Popola l'array di strutture is_reg_pd_mov_scadenza per la creazione
//                          di più movimenti di scadenza collegati allo stesso movimento di
//                          partita.
//
// *****************************************************************************************

is_reg_pd_mov_scadenza = as_reg_pd_mov_scadenza
end subroutine

public subroutine uf_mc_iva_alt (boolean ab_mc_iva_alt[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  17/05/99  [ANT]    Assegna il flag che indica se una riga di movimento contabile
//                          è la seconda riga dell'IVA, cioè quella con numero 9998 (ib_mc_iva_alt[]).
//
// *****************************************************************************************

ib_mc_iva_alt = ab_mc_iva_alt
end subroutine

public function string uf_get_info ();// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  26/07/99  [ANT]    Restituisce la variabile is_info che contiene l'esito della creazione
//
// *****************************************************************************************

RETURN is_info
end function

public subroutine uf_build_info (integer ai_value);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  26/07/99  [ANT]
//
// *****************************************************************************************

//ls_RifDoc = "Registro: '" + ls_des_registro + "' Progressivo: " + string(ll_num_progressivo) + " Data: " + string(ld_data_registrazione, "dd/mm/yyyy") + " (" + string(ll_id_documento) + ") ==> "
//
//CHOOSE CASE ai_value
//	CASE IS > 0
//		ls_ErrText = "OK"
//	CASE -1
//		ls_ErrText = "Movimenti contabili non bilanciati o con totali a zero."
//	CASE -2
//		ls_ErrText = "Il numero degli elementi non è uguale per tutti gli array dei movimenti contabili."
//	CASE -3
//		ls_ErrText = "L'ID del profilo registrazione non è valido."
//	CASE -4
//		ls_ErrText = "L'ID dell'esercizio passato è zero o nullo e l'azienda corrente non ha un esercizio impostato."
//	CASE -5
//		ls_ErrText = "Non esiste un profilo con l'ID passato."
//	CASE -6
//		ls_ErrText = "Le righe del profilo registrazione con contengono i conti."
//	CASE -7
//		ls_ErrText = "Le righe del profilo registrazione con contengono i segni."
//	CASE -8
//		ls_ErrText = "Registrazione già esistente."
//	CASE -9
//		ls_ErrText = "Impossibile generare la registrazione: immettere la valuta nei Parametri di Sistema."
//	CASE -10
//		ls_ErrText = "Il tipo del registro è di tipo PN o MN ma il totale imposta non è zero."
//	CASE -11
//		ls_ErrText = "Non ci sono informazioni sufficienti per creare i movimenti contabili."
//	CASE ELSE
//		ls_ErrText = "Errore generico durante la contabilizzazione."
//END CHOOSE
//
//
end subroutine

public subroutine uf_id_con_pagamento (long al_id_con_pagamento);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  16/11/99  [ANT]    Imposta la variabile di istanza che memorizza la condizione di 
//                          pagamento della testata.
//
// *****************************************************************************************

il_id_con_pagamento = al_id_con_pagamento
end subroutine

public function long uf_get_id_reg_pd ();RETURN il_id_reg_pd
end function

public subroutine uf_riferimenti_mc (string as_riferimenti[]);is_riferimenti_mc = as_riferimenti
end subroutine

public subroutine uf_imp_valuta_mc (decimal adc_imp_valuta_mc[]);idc_imp_valuta_mc = adc_imp_valuta_mc
end subroutine

public function boolean uf_reg_pd_esistente (long al_id_registro, long al_num_progressivo, date ad_data_registrazione);//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  18/05/98  [ANT]    Controlla se esiste già una registrazione con id_registro,
//                          numero progressivo e data registrazione passati. Ritorna TRUE
//                          se esiste, altrimenti FALSE.
//   02  28/09/00  [ANT]    Eliminata la data_registrazione dalla WHERE della SELECT.
//
// *****************************************************************************************

boolean lb_RetVal = FALSE
integer li_anno

IF (is_tipo_registro = 'IA') OR (is_tipo_registro = 'IV') THEN
	IF ic_rel_num_prog_iva = 'A' THEN
		/* Selezione del mod_registro del registro. */
		SELECT dba.registro.id_mod_registro
			INTO :il_id_mod_registro
			FROM dba.registro
			WHERE dba.registro.id_registro = :il_id_registro
			USING it_tran;

		li_anno = Year(id_data_registrazione)
		
		/* Se il registro è di tipo IVA, il numero progressivo deve essere relativo
		all'anno solare (a parità di mod_registro). */
		SELECT dba.reg_pd.id_reg_pd
			INTO :il_id_reg_pd
			FROM dba.reg_pd
				JOIN dba.registro 
					ON dba.reg_pd.id_registro = dba.registro.id_registro
			WHERE (dba.reg_pd.num_progressivo = :il_num_progressivo) AND
					(dba.registro.id_mod_registro = :il_id_mod_registro) AND
					(Year(dba.reg_pd.data_registrazione) = :li_anno)
			USING it_tran;
	ELSE
		SELECT dba.reg_pd.id_reg_pd
			INTO :il_id_reg_pd
			FROM dba.reg_pd
			WHERE (dba.reg_pd.num_progressivo = :il_num_progressivo) AND
					(dba.reg_pd.id_registro = :il_id_registro)
			USING it_tran;
	END IF
ELSE
	/* Se il registro non è di tipo IVA, il numero progressivo deve essere relativo
	al registro. */
	SELECT dba.reg_pd.id_reg_pd
		INTO :il_id_reg_pd
		FROM dba.reg_pd
		WHERE (dba.reg_pd.num_progressivo = :il_num_progressivo) AND
				(dba.reg_pd.id_registro = :il_id_registro)
		USING it_tran;
END IF

CHOOSE CASE it_tran.SQLCode
	CASE 0
		IF il_id_reg_pd > 0 THEN
			lb_RetVal = TRUE
		END IF
	CASE ELSE
		lb_RetVal = FALSE
END CHOOSE

RETURN lb_RetVal
end function

public subroutine uf_autocommit (boolean ab_autocommit);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/04/98  [ANT]    Imposta il flag che indica se eseguire il commit alla fine della
//                          creazione.
//
// *****************************************************************************************

ib_autocommit = ab_AutoCommit
end subroutine

public function long uf_proponi_esercizio ();// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/04/98  [ANT]    Restituisce l'ID dell'esercizio proposto dell'azienda corrente.
//
// *****************************************************************************************

long ll_RetVal = 0

SELECT dba.azienda.id_esercizio_proposto
	INTO :ll_RetVal
   FROM dba.azienda
   WHERE dba.azienda.id_azienda = :il_id_azienda
	USING it_tran;

CHOOSE CASE it_tran.SQLCode
	CASE -1
      //g_mb.messagebox("Errore sul DataBase", "u_reg_pd / uf_proponi_esercizio / 1")
      //n_err_mgr.nf_mess_immediato(55522)
END CHOOSE

RETURN ll_RetVal
end function

public subroutine uf_elimina_esistente (boolean ab_elimina_esistente);ib_elimina_esistente = ab_elimina_esistente
end subroutine

public subroutine uf_visualizza_reg_pd (long al_id_reg_pd);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  17/11/97  [ANT]    Accetta l'ID di una registrazione e attiva w_reg_pd_cx
//                          visualizzando la registrazione con l'ID passato.
//
// *****************************************************************************************

string ls_args[], ls_tipo_registro
long li_retval
DateTime ldt_data_registrazione
boolean lb_ok = FALSE
//s_list_main lstr_list
//
//IF (ii_DisplayMode = 1) OR (ii_DisplayMode = 2) THEN
//	CHOOSE CASE ii_DisplayMode
//		CASE 1
//			lb_ok = TRUE
//		CASE 2
//			//li_RetVal = MessageBox("Registrazioni Contabili","Generata la registrazione numero " + string(il_num_progressivo) + " con data " + string(id_data_registrazione) + "~rnel registro " + is_descr_registro + ".~rVisualizzare la registrazione?", Information!, YesNo!, 2)
//			ls_args[1] = string(il_num_progressivo)
//			ls_args[2] = string(id_data_registrazione, "dd/mm/yyyy")
//			ls_args[3] = is_descr_registro
//			li_RetVal = n_err_mgr.nf_mess_immediato(55521, ls_args)
//
//			IF li_RetVal = gci_btn_si THEN
//				lb_ok = TRUE
//			END IF
//	END CHOOSE
//END IF
//
//IF lb_ok = TRUE THEN
//	lstr_list.s_modo = modo_modifica
//	lstr_list.s_identifica[1] = "id_reg_pd"
//	lstr_list.l_identifica[1] = al_id_reg_pd
//	
//	SELECT dba.registro.tipo_registro
//		INTO :ls_tipo_registro
//		FROM dba.reg_pd,
//			  dba.registro
//		WHERE (dba.reg_pd.id_registro = dba.registro.id_registro) AND
//				(dba.reg_pd.id_reg_pd = :al_id_reg_pd)
//		USING it_tran;
//	
//	CHOOSE CASE it_tran.SQLCode
//		CASE 0
//			lstr_list.s_win_parametri = ls_tipo_registro
//	END CHOOSE
//	
//	OpenWithParm(w_reg_pd_cx, lstr_list)
//	
//	/* Una volta attivata la finestra w_reg_pd_cx per la visualizzazione della
//	registrazione, l'utente potrebbe anche cancellare la registrazione. Per questo motivo,
//	verifico se, dopo l'attivazione della finestra, la registrazione esite ancora. */
//	il_id_reg_pd = 0
//	ldt_data_registrazione = DateTime(id_data_registrazione)
//	SELECT dba.reg_pd.id_reg_pd
//		INTO :il_id_reg_pd
//		FROM dba.reg_pd
//		WHERE (dba.reg_pd.num_progressivo    = :il_num_progressivo)     AND
//				(dba.reg_pd.data_registrazione = :ldt_data_registrazione) AND
//				(dba.reg_pd.id_registro        = :il_id_registro)
//		USING it_tran;
//END IF
end subroutine

public subroutine uf_visualizza_reg_pd (long al_id_reg_pd[]);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  04/03/99  [ANT]    Attiva w_reg_pd_lt sulle registrazioni i cui ID sono contenuti
//                          nell'array al_id_reg_pd[].
//
// *****************************************************************************************

long ll_i
string ls_sql_cond
//w_reg_pd_visualizza_lt lw_reg_pd_lt
//s_list_main ls_list
//
//IF UpperBound(al_id_reg_pd) > 0 THEN
//	ls_sql_cond = "reg_pd.id_reg_pd in ("
//	FOR ll_i = 1 TO UpperBound(al_id_reg_pd)
//		ls_sql_cond = ls_sql_cond + string(al_id_reg_pd[ll_i])
//		IF ll_i < UpperBound(al_id_reg_pd) THEN
//			ls_sql_cond = ls_sql_cond + ", "
//		ELSE
//			ls_sql_cond = ls_sql_cond + ")"
//		END IF
//	NEXT
//
//	ls_list.s_win_parametri = ls_sql_cond
//
//	OpenWithParm(lw_reg_pd_lt, ls_list, "w_reg_pd_visualizza_lt")
//END IF
end subroutine

public function long uf_elimina (long al_id_reg_pd);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  12/06/98  [ANT]    Elimina una registrazione.
//
// *****************************************************************************************

long ll_RetVal = -1

//it_tran.nf_BeginTran()

it_tran.nf_Elimina_Reg_PD(al_id_reg_pd, ll_RetVal)

CHOOSE CASE ll_RetVal
	CASE 0
		//it_tran.nf_Commit()
		COMMIT;
	CASE ELSE
		//it_tran.nf_Rollback()
		ROLLBACK;
END CHOOSE


RETURN ll_RetVal
end function

public function long uf_crea ();// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  14/04/98  [ANT]    Crea la registrazione contabile.
//   02  12/05/98  [ANT]    Aggiunta la memorizzazione dei campi segno_reg_iva e tipo_fattura.
//   03  18/05/98  [ANT]    Aggiunta la chiamata alla funzione uf_reg_pd_esistente per
//                          controllare se esiste già una registrazione con registro, numero
//                          progressivo e data passati.
//   04  27/05/98  [ANT]    Aggiunta la chiamata alla funzione per il completamento degli
//                          array dei movimenti contabili.
//   05  28/05/98  [ANT]    Modificato il controllo sul totale dei movimenti contabili: i
//                          totali del dare e dell'avere possono anche essere in negativo,
//                          l'importante è che il saldo (dare - avere) sia zero.
//   06  09/12/98  [ANT]    Aggiunta la possibilità di creare movimenti di partita e di scadenza
//                          con importi a zero. Ciò viene controllato da ib_zero_mov_partita
//                          e ib_zero_mov_scadenza.
//   07  07/01/99  [ANT]    Aggiunto il controllo per il quale non vengono generate registrazioni
//                          di tipo PN o MN se l'importo dell'imposta (IVA) è maggiore di 0.
//                          Tale modifica si è resa necessaria per la contabilizzazione di
//                          documenti Ismea di tipo PN o MN.
//   08  25/01/99  [ANT]    Aggiunta l'impostazione del campo reg_pd.periodo_liquidazione.
//   09  27/01/99  [ANT]    Modificati i calcoli degli importi in valuta mediante l'oggetto
//                          n_currency_sync.
//   10  17/02/99  [ANT]    Aggiunta l'assegnazione del campo reg_pd.id_documento.
//   11  17/05/99  [ANT]    Aggiunta l'assegnazione del numero 9998 alla eventuale seconda
//                          riga dell'IVA.
//   12  19/07/99  [ANT]    Nel caso in cui un movimento contabile abbia importo negativo,
//                          questo viene reso positivo e i segni dare e avere vengono invertiti.
//   13  08/09/99  [ANT]    Aggiunta la gestione delle transazioni: l'apertura della transazione
//                          avviene solo se l'ib_AutoCommit è TRUE. In caso contrario, infatti,
//                          si presume che la commit (e quindi anche l'apertura della transazione)
//                          venga fatta da chi usa u_reg_pd.
//   14  19/07/00  [ANT]    Ampliato il comportamento in caso di registrazione esistente:
//                          viene controllata la variabile ib_elimina_esistente per valultare se
//                          eliminare o menola registrazione esistente.
//   15  24/10/00  [ANT]    Aggiunta la creazione dei record in wiz_reg_pd.
//   16  19/12/01  [ANT]    Aggiunta la creazione dei movimenti di centri di costo.
//   17  08/10/02  [ANT]    Aggiunta l'attivazione dei wizard se la creazione della registrazione
//                          va a buon fine.
//   18  30/04/04  [STF]    Aggiunto il salvataggio in reg_pd del capo commessa
//                           (id_capo_commessa) e dell'operatore (id_operatore), non
//                           gestiti per ora nello standard, ma solo da Ecoter
//     19  26/02/09  [PIE]   Inserito controllo che verifica se il conto gestisce i cc o meno e quindi prosegue l'impostazione della chiave.
//
//   20  15/01/10  [RIC]   Gestione della variabile ib_display_error per tutti i possibili
//                           errori. aggiunto dove necessario una funzione uf_set_info con
//                         un paramentro string aggiuntivo con un dettaglio dell'errore (tipicamente sqlca.sqlerrtext)
//     21  19/01/2010 [Piero] Inserito flag dba.wiz_profilo.utilizzabile = 'S' per operare solo sui wizard che sono utilizzabili.
// *****************************************************************************************

long       ll_id_prof_esiste, ll_NULL, ll_i, ll_numero, ll_id_mov_contabile,ll_scad_doc
long       ll_num_riga, ll_id_mov_partita, ll_SQL_reg_pd = 0, ll_SQL_mov_contabile = 0,ll_sql_mov_scadenza=0
long       ll_SQL_wiz = 0, ll_SQL_cc_mov_contabile = 0,ll_sql_scadenza = 0
long       ll_SQL_mov_iva = 0, ll_j, ll_id_scadenza, ll_id_con_pagamento, ll_id_conto,ll_id_Scad_pag
long      ll_scad_id_valuta,ll_scad_id_tipo_pag,ll_scad_anno_partita,ll_scad_id_banca_az,ll_scad_id_banca_sc
long      ll_scad_id_conto,ll_scad_id_reg_pd,ll_scad_id_mov_cont,ll_scad_num_mov, ll_id_tipo_pagamento_def
long      ll_id_cc_chiave,ll_id_tipo_pag_scad, ll_id_dich_intento, ll_conta_insoluti
decimal    ldc_importo_dare, ldc_importo_avere, ldc_NULL, ldc_tot_dare = 0, ldc_tot_avere = 0
decimal    ldc_saldo, ldc_imp_mov_scadenza, ldc_importo_dare_valuta, ldc_importo_avere_valuta
decimal    ldc_imp_mov_partita, ldc_tot_imposta, ldc_imp_valuta[]

decimal   ldc_scad_cambio,ldc_scad_dare,ldc_scad_avere,ldc_scad_dare_v,ldc_scad_avere_v
integer    li_invert, li_RetVal, li_mc_bilanciati, li_retval_scadenze
date       ld_NULL
string    ls_NULL, ls_prof_registrazione_codice, ls_tipo_registro, ls_args[]
string   ls_msg[], ls_rel_num_prog_iva, las_NULL[]
string   ls_bloccato,ls_rag_soc,ls_tipo,ls_esercizio_bloccato,ls_tipo_pag
string    ls_conto_ges_par_aperte,ls_rif_partita,ls_sottomastro_uso
string   ls_note_mov_scadenza,ls_esito_mov_Scadenza
string   ls_scad_da_raggr,ls_scad_num_doc,ls_scad_num_partita,ls_scad_tipo_scad
string    ls_conto_ges_cc = 'N',ls_esito_prec
boolean    lb_stop
boolean    lb_crea_mov_contabile
boolean    lb_elab_des_contabile
datetime ldt_data_competenza, ldt_data_registrazione, ldt_periodo_liquidazione, ldt_periodo_reg_iva
datetime ldt_data_val_banca, ldt_data_inizio_competenza, ldt_data_fine_competenza

datetime   ldt_blocca_registro_al,ldt_data_mov_Scadenza
datetime   ldt_scad_data_scad,ldt_scad_data_doc,ldt_scad_data_valuta,ldt_data_temp,ldt_data_dec_pag

CONSTANT CHAR LCC_DARE = 'D'
CONSTANT CHAR LCC_AVERE = 'A'
CONSTANT CHAR LC_RISCONTO = "S"
CONSTANT CHAR LC_RATEO = "T"

SetNull(ll_NULL)
SetNull(ldc_NULL)
SetNull(ls_NULL)
SetNull(ld_NULL)

setnull(ldt_blocca_registro_al)

select id_tipo_pagamento
   into :ll_id_tipo_pagamento_def
   from dba.par_sistema
   using it_tran;

lb_stop                  = false
ls_esercizio_bloccato   = "N"

datastore lds_wiz_source, lds_wiz_target
string ls_sql_wiz_source, ls_dwsyntax_wiz_source

string ls_errors, ls_descrizione, ls_style
long ll_id_reg_pd, ll_id_wizard, ll_id_prof_registrazione, ll_num_ordine


in_currency_sync.uf_set_transaction(it_tran)
iu_reg_pd_funct.uf_set_transaction(it_tran)


IF il_id_prof_registrazione > 0 THEN
   SELECT Count(*)
      INTO :ll_id_prof_esiste
      FROM dba.prof_registrazione
      WHERE dba.prof_registrazione.id_prof_registrazione = :il_id_prof_registrazione
		USING it_tran;
ELSE
   /* L'ID del profilo registrazione non è valido. */
	uf_set_info(-3)
   RETURN -3
END IF

/* Controllo che i movimenti contabili siano bilanciati. */
li_mc_bilanciati = uf_mc_bilanciati()
IF (li_mc_bilanciati <> 0) AND (ib_no_mc =FALSE) THEN
	uf_set_info(li_mc_bilanciati)
	RETURN li_mc_bilanciati
END IF

/* Se non sono stati passati alcuni ID dei conti o alcuni segni dei movimenti contabili,
questi vengono attribuiti dal profilo. */
FOR ll_i = 1 TO UpperBound(il_id_conto_mc)
	IF IsNull(ic_rettifica) THEN
		ll_num_riga = ll_i
	ELSE
		CHOOSE CASE ic_rettifica
			CASE LC_RISCONTO
				ll_num_riga = 2
			CASE LC_RATEO
				ll_num_riga = 1
			CASE ELSE
				ll_num_riga = ll_i
		END CHOOSE
	END IF

   IF il_id_conto_mc[ll_i] = 0 or IsNull(il_id_conto_mc[ll_i]) THEN
		SELECT dba.prof_mov_contabile.id_conto
			INTO :il_id_conto_mc[ll_i]
			FROM dba.prof_mov_contabile
			WHERE (dba.prof_mov_contabile.id_prof_registrazione = :il_id_prof_registrazione) AND
			      (dba.prof_mov_contabile.num_riga = :ll_num_riga)
			USING it_tran;

		IF IsNull(il_id_conto_mc[ll_i]) OR (il_id_conto_mc[ll_i] = 0) THEN
			SELECT dba.prof_registrazione.codice
				INTO :ls_prof_registrazione_codice
				FROM dba.prof_registrazione
				WHERE dba.prof_registrazione.id_prof_registrazione = :il_id_prof_registrazione
				USING it_tran;

         //g_mb.messagebox("Generazioni Automatiche", "Impossibile generare la registrazione: immettere un conto~rsulla riga " + string(ll_num_riga) + " del Profilo Registrazione con codice " + ls_prof_registrazione_codice + ".", StopSign!)
         if ib_display_error then
            ls_args[1] = string(ll_num_riga)
            ls_args[2] = ls_prof_registrazione_codice
            //n_err_mgr.nf_mess_immediato(55519, ls_args)
            uf_set_info(-6)
         else
            uf_set_info(-6," Riga n° " + string(ll_num_riga) + " del Profilo Registrazione " + ls_prof_registrazione_codice)
         end if

		END IF
	END IF

   IF ic_segno_mc[ll_i] = "" or isnull(ic_segno_mc[ll_i])  THEN
		SELECT dba.prof_mov_contabile.segno
			INTO :ic_segno_mc[ll_i]
			FROM dba.prof_mov_contabile
			WHERE (dba.prof_mov_contabile.id_prof_registrazione = :il_id_prof_registrazione) AND
			      (dba.prof_mov_contabile.num_riga = :ll_num_riga)
			USING it_tran;

		IF ic_segno_mc[ll_i] = "" THEN 
			uf_set_info(-7)
			RETURN -7
		END IF
	END IF
NEXT

IF ll_id_prof_esiste = 1 THEN
	
   SELECT rel_num_prog_iva
      INTO :ls_rel_num_prog_iva
      FROM dba.par_sistema
      WHERE dba.par_sistema.id_azienda = 1
      USING it_tran;

   IF IsNull(ls_rel_num_prog_iva) THEN ls_rel_num_prog_iva = "E"
	
	/* Se non è stato passato l'esercizio ma è stata passata la data registrazione, l'esercizio
	viene calcolato in base alla data registrazione. */
	ldt_data_registrazione   = DateTime(id_data_registrazione)
	IF (IsNull(il_id_esercizio) OR (il_id_esercizio = 0)) AND NOT IsNull(ldt_data_registrazione) THEN
		SELECT dba.esercizio.id_esercizio
			INTO :il_id_esercizio
			FROM dba.esercizio
			WHERE dba.esercizio.data_inizio <= :ldt_data_registrazione AND
					dba.esercizio.data_fine >= :ldt_data_registrazione
			USING it_tran;
			
		CHOOSE CASE it_tran.SQLCode
			CASE -1
				//g_mb.messagebox("Errore", "u_reg_pd_base / uf_crea / 1~r~n" + it_tran.SQLErrText)
				ls_args[1] = it_tran.SQLErrText
				//n_err_mgr.nf_mess_immediato(55567, ls_args)
		END CHOOSE
	END IF

   /* Se l'ID dell'esercizio passato è 0 o nullo, memorizza l'esercizio dell'azienda corrente. */
   IF IsNull(il_id_esercizio) OR (il_id_esercizio = 0) THEN
		il_id_esercizio = uf_proponi_esercizio()

      IF IsNull(il_id_esercizio) OR (il_id_esercizio = 0) THEN
         /* L'ID dell'esercizio passato è zero o nullo e l'azienda corrente non ha un
         esercizio impostato. */
			uf_set_info(-4)
         RETURN -4
      END IF
   END IF

   /* Chiama la funzione per l'impostazione delle variabili di istanza che memorizzano
   la valuta dell'esercizio e il relativo cambio. */
   uf_id_esercizio(il_id_esercizio)
   
   SELECT dba.esercizio.bloccato
      INTO :ls_esercizio_bloccato
      FROM dba.esercizio
      WHERE dba.esercizio.id_esercizio = :il_id_esercizio 
      USING it_tran;
   
   if ls_esercizio_bloccato = "S" then
      uf_set_info( -22 )
      RETURN -22
   end if

   /* Se l'ID del registro passato è 0 o nullo, viene calcolato. */
   IF IsNull(il_id_registro) OR (il_id_registro = 0) THEN
      il_id_registro = f_proponi_registro(il_id_prof_registrazione, il_id_esercizio)
		uf_id_registro(il_id_registro)
   END IF

   SELECT dba.registro.tipo_registro,
          dba.registro.id_mod_registro,
          dba.registro.blocca_al
      INTO :is_tipo_registro,
           :il_id_mod_registro,
           :ldt_blocca_registro_al           
      FROM dba.registro
      WHERE dba.registro.id_registro = :il_id_registro
      USING it_tran;
      
   /* Impostazione della variabile che memorizza il tipo fattura. */
   IF ic_tipo_fattura = "" OR IsNull(ic_tipo_fattura) THEN
      SELECT dba.prof_registrazione.tipo_fattura
         INTO :ic_tipo_fattura
         FROM dba.prof_registrazione
         WHERE dba.prof_registrazione.id_prof_registrazione = :il_id_prof_registrazione
         USING it_tran;
         
      IF ic_tipo_fattura = "" OR IsNull(ic_tipo_fattura) THEN
         ic_tipo_fattura = "N"
      END IF
      
   END IF
   
   CHOOSE CASE ic_tipo_fattura
      CASE "C", "A", "T"
         ii_segno_reg_iva = -1
      CASE ELSE
         ii_segno_reg_iva = 1
   END CHOOSE

   /* Non vengono generate registrazioni di tipo PN o MN con imposta > 0. */
   SELECT dba.registro.descrizione,
          dba.registro.tipo_registro
      INTO :is_descr_registro,
           :ls_tipo_registro           
      FROM dba.registro
      WHERE dba.registro.id_registro = :il_id_registro
      USING it_tran;

   IF ((ls_tipo_registro = "PN") OR (ls_tipo_registro = "MN")) AND (UpperBound(idc_imposta) > 0) THEN
      ldc_tot_imposta = 0
      FOR ll_i = 1 TO UpperBound(idc_imposta)
         ldc_tot_imposta = ldc_tot_imposta + idc_imposta[ll_i]
      NEXT
      IF ldc_tot_imposta > 0 THEN
         uf_set_info(-10)
         RETURN -10
      END IF
   END IF

   IF ls_rel_num_prog_iva = "E" THEN
      /* Se il numero progressivo è zero o nullo, viene calcolato. */
      IF IsNull(il_num_progressivo) OR (il_num_progressivo = 0) THEN
         il_num_progressivo = f_proponi_num_reg_pd(il_id_registro)
      END IF
   END IF

   /* Se la data registrazione è nulla, viene calcolata. */
   IF IsNull(id_data_registrazione) OR (String(id_data_registrazione, "dd/mm/yyyy") = "01/01/1900") THEN
      id_data_registrazione = f_proponi_data_reg_pd(il_id_registro)
   END IF

   if not isnull(ldt_blocca_registro_al) and ldt_blocca_registro_al >= datetime(id_data_registrazione) then
      uf_set_info( -23 )
      RETURN -23   
   end if

   IF ls_rel_num_prog_iva = "A" THEN
      /* Se il numero progressivo è zero o nullo, viene calcolato. */
      IF IsNull(il_num_progressivo) OR (il_num_progressivo = 0) THEN
         il_num_progressivo = f_proponi_num_reg_pd_anno(il_id_registro, Year(id_data_registrazione))
      END IF
   END IF

   /* Se la data competenza è nulla, viene impostata alla data registrazione. */
   IF IsNull(id_data_competenza) THEN id_data_competenza = id_data_registrazione

   /* Se la descrizione contabile è vuota o nulla, viene assegnata quella del profilo. */
   IF IsNull(is_descrizione) OR is_descrizione = "" THEN
      is_descrizione = f_proponi_descr_reg_pd(il_id_prof_registrazione)
   END IF

   /* Se la valuta è zero o nulla, viene calcolata. */
   IF IsNull(il_id_valuta) OR (il_id_valuta = 0) THEN
      il_id_valuta = f_proponi_valuta(ll_NULL, il_id_prof_registrazione)
   END IF

   /* Se il cambio è zero o nullo viene calcolato in base alla valuta. */
   IF IsNull(idc_cambio) OR (idc_cambio = 0) THEN
      idc_cambio = f_proponi_cambio(il_id_valuta)
   END IF

   /* Se viene passato un conto, calcolo l'importo in valuta, il numero documento e la data
   documento. */
   IF il_id_conto > 0 THEN
		IF IsNull(idc_imp_valuta) THEN
	      //idc_imp_valuta = idc_importo / idc_cambio

			in_currency_sync.nf_converti_valore(il_esercizio_id_valuta, &
															idc_esercizio_cambio,   &
															idc_importo,              &
															il_id_valuta,       &
															idc_cambio,          &
															ldc_imp_valuta)

			IF UpperBound(ldc_imp_valuta) >= 3 THEN
				idc_imp_valuta = ldc_imp_valuta[3]
			ELSE
				SetNull(idc_imp_valuta)
			END IF
		END IF

      /* Se il numero del documento è zero o nullo, viene calcolato. */
      IF IsNull(is_num_documento) OR (is_num_documento = "") THEN
         is_num_documento = f_proponi_num_documento(il_id_registro)
      END IF

      /* Se la data del documento è nulla, viene calcolata. */
      IF IsNull(id_data_documento) THEN
         id_data_documento = f_proponi_data_documento(il_id_registro)
      END IF
   END IF

	/* Chiama la funzione per il completamento degli array dei movimenti contabili. */
	IF uf_controlla_array() = FALSE THEN
		uf_set_info(-11)
		RETURN -11
	END IF

	/* Controlla che non esista già una registrazione con registro, numero progressivo e
	data registrazione passati. */
	IF uf_reg_pd_esistente(il_id_registro, il_num_progressivo, id_data_registrazione) = TRUE THEN
		IF ib_elimina_esistente = TRUE THEN
			/* La registrazione esiste già ma bisogna cancellarla. */
			CHOOSE CASE uf_elimina(il_id_reg_pd)
				CASE 0
				CASE -1
					/* Errore in fase di cancellazione della registrazione esistente. */
					uf_set_info(-21)
					RETURN -21
				CASE -20
					/* La registrazione esistente è stata già stampata, quindi non può essere
					cancellata. */
					uf_set_info(-20)
					RETURN -21
			END CHOOSE
		ELSE
         IF ib_notify_existing = TRUE THEN
            if ib_display_error then
               ls_args = las_NULL
               ls_args[1] = String(il_num_progressivo)
               ls_args[2] = String(id_data_registrazione, "dd/mm/yyyy")
               ls_args[3] = is_descr_registro
               //n_err_mgr.nf_mess_immediato(55606, ls_args)
            end if
         END IF
			
			uf_set_info(-8)
			RETURN -8
		END IF
	END IF

	/* Se non è stata impostata la data periodo liquidazione, assegna la data_registrazione
	aumentata dei mesi di posticipo liquidazione del registro. */
	IF IsNull(id_periodo_liquidazione) THEN
		id_periodo_liquidazione = iu_reg_pd_funct.uf_MonthRelativeDate(id_data_registrazione, iu_reg_pd_funct.uf_get_mesi_post_liquidazione(il_id_registro))
	END IF

	/* Se non è stata impostata la data periodo registro IVA, assegna la data_registrazione
	aumentata dei mesi di posticipo registro IVA del registro. */
	IF IsNull(id_periodo_reg_iva) THEN
		id_periodo_reg_iva = iu_reg_pd_funct.uf_MonthRelativeDate(id_data_registrazione, iu_reg_pd_funct.uf_get_mesi_post_reg_iva(il_id_registro))
	END IF

   /* Calcolo della condizione di pagamento. */
   ll_id_con_pagamento = uf_calc_id_con_pagamento()

   IF ib_AutoCommit = TRUE THEN	
      //it_tran.nf_BeginTran()
   END IF

   ldt_data_competenza      = DateTime(id_data_competenza)
   ldt_data_registrazione   = DateTime(id_data_registrazione)
   ldt_periodo_liquidazione = DateTime(id_periodo_liquidazione)
   ldt_periodo_reg_iva      = DateTime(id_periodo_reg_iva)

   ldt_data_dec_pag          = DateTime(id_data_dec_pagamento)

   if il_id_operatore <= 0 then setnull(il_id_operatore)
   if il_id_capo_commessa <= 0 then setnull(il_id_capo_commessa)
   
   //[GIO] 15/04/2009
   Setnull(il_id_reg_pd)

	//[ENRICO] 25/9/2015
	// aggiunto in insert anche id_dich_intento che richiama la dichiarazione d'intento del cliente dal soggetto commerciale
	// aggiiungere richerca eventuale dichiarazione intento nella sog_commerciale
	setnull(ll_id_dich_intento)

   INSERT INTO dba.reg_pd
          (id_registro,
           num_progressivo,
           data_competenza,
           id_prof_registrazione,
           data_registrazione,
           id_valuta,
           cambio,
           importo,
           imp_valuta,
           descrizione,
           num_documento,
           data_documento,
           id_conto,
			  segno_reg_iva,
			  tipo_fattura,
			  ges_oper_intracomunitarie,
			  id_reg_pd_padre,
			  periodo_liquidazione,
			  periodo_reg_iva,
			  id_documento,
			  id_con_pagamento,
           id_capo_commessa,
           id_operatore,
           id_azienda,
           data_dec_pagamento,
			automatica,
			importata,
			id_dichiarazione_intento)
   VALUES (:il_id_registro,
           :il_num_progressivo,
           :ldt_data_competenza,
           :il_id_prof_registrazione,
           :ldt_data_registrazione,
           :il_id_valuta,
           :idc_cambio,
           :idc_importo,
           :idc_imp_valuta,
           :is_descrizione,
           :is_num_documento,
           :id_data_documento,
           :il_id_conto,
			  :ii_segno_reg_iva,
			  :ic_tipo_fattura,
			  :ic_ges_oper_intracomunitarie,
			  :il_id_reg_pd_padre,
           :ldt_periodo_liquidazione,
           :ldt_periodo_reg_iva,
			  :il_id_documento,
           :ll_id_con_pagamento,
           :il_id_capo_commessa,
           :il_id_operatore,
           :il_id_azienda,
           :ldt_data_dec_pag,
			  'S',
			  'N',
			  :ll_id_dich_intento)
		USING it_tran;

	ll_SQL_reg_pd = it_tran.SQLCode

   if ll_SQL_reg_pd <> 0 then
      lb_stop   = true
      if ib_display_error = true then
         ls_msg[1]   = it_tran.sqlerrtext
         //n_err_mgr.nf_accoda( 55583,ls_msg )
      else
         uf_set_info(-100,it_tran.sqlerrtext)
      end if
   end if
   
   if lb_stop = false then      


		SELECT dba.reg_pd.id_reg_pd
			INTO :il_id_reg_pd
			FROM dba.reg_pd
			WHERE (dba.reg_pd.num_progressivo    = :il_num_progressivo)    AND
               (dba.reg_pd.data_registrazione = :ldt_data_registrazione) AND
			      (dba.reg_pd.id_registro        = :il_id_registro)
			USING it_tran;

		IF il_id_reg_pd > 0 THEN
			/* Controlla che siano stati passati movimenti IVA. */
			IF UpperBound(idc_imponibile) > 0 THEN
				/* Creazione dei movimenti IVA. */
            //[GIU] 09/06/2005 Aggiunta degli imponibile omaggi            
            FOR ll_i = 1 TO UpperBound(idc_imponibile)
                 INSERT INTO dba.mov_iva
                           ( id_reg_pd,
                             imponibile,
                             id_tab_iva,
                             imposta,
                             id_azienda,
                             imponibile_omaggi,
									  num_riga)
                    VALUES ( :il_id_reg_pd,
                             :idc_imponibile[ll_i],
                             :il_id_tab_iva[ll_i],
                             :idc_imposta[ll_i],
                             :il_id_azienda,
                             :idc_imponibile_omaggi[ll_i],
							:ll_i)
							  USING it_tran;
					IF it_tran.SQLCode <> 0 THEN
						ll_SQL_mov_iva = it_tran.SQLCode
                  if ib_display_error = true then
                     ls_msg[1]   = it_tran.sqlerrtext
                     //n_err_mgr.nf_accoda( 55585,ls_msg )
                  else
                     uf_set_info(-100,it_tran.sqlerrtext)
                  end if						
					END IF
				NEXT
			END IF
	
			/* Creazione dei movimenti contabili. */
			IF ib_no_mc = FALSE THEN
				FOR ll_i = 1 TO UpperBound(il_id_conto_mc)
					/*  */
					IF idc_importo_mc[ll_i] < 0 THEN
						idc_importo_mc[ll_i] = idc_importo_mc[ll_i] * -1
                  idc_imp_valuta_mc[ll_i] = idc_imp_valuta_mc[ll_i] * -1
						IF ic_segno_mc[ll_i] = "D" THEN
							ic_segno_mc[ll_i] = "A"
						ELSE
							ic_segno_mc[ll_i] = "D"
						END IF
					END IF
	
					IF ib_zero_mov_contabile = FALSE THEN
						IF idc_importo_mc[ll_i] <> 0 THEN
							lb_crea_mov_contabile = TRUE
						ELSE
							lb_crea_mov_contabile = FALSE
						END IF
					ELSE
						lb_crea_mov_contabile = TRUE
					END IF
	
					IF lb_crea_mov_contabile = TRUE THEN
						CHOOSE CASE ic_segno_mc[ll_i]
							CASE "D"
								ldc_importo_dare = idc_importo_mc[ll_i]
								ldc_importo_avere = ldc_NULL
	
								in_currency_sync.nf_converti_valore(il_esercizio_id_valuta, &
																				idc_esercizio_cambio,   &
																				idc_importo_mc[ll_i],              &
																				il_id_valuta_mc[ll_i],       &
																				idc_cambio_mc[ll_i],          &
																				ldc_imp_valuta)
	
								IF UpperBound(ldc_imp_valuta) >= 3 THEN
									/* Assegno il valore calcolato solo se non è stato passato mediante
									l'array apposito idc_imp_valuta_mc*/
									IF IsNull(idc_imp_valuta_mc[ll_i]) THEN
										ldc_importo_dare_valuta = ldc_imp_valuta[3]
									ELSE
										ldc_importo_dare_valuta = idc_imp_valuta_mc[ll_i]
									END IF
								ELSE
									SetNull(ldc_importo_dare_valuta)
								END IF
								ldc_importo_avere_valuta = ldc_NULL
							CASE "A"
								ldc_importo_dare = ldc_NULL
								ldc_importo_avere =idc_importo_mc[ll_i]
	
								ldc_importo_dare_valuta = ldc_NULL
	
								in_currency_sync.nf_converti_valore(il_esercizio_id_valuta, &
																				idc_esercizio_cambio,   &
																				idc_importo_mc[ll_i],              &
																				il_id_valuta_mc[ll_i],       &
																				idc_cambio_mc[ll_i],          &
																				ldc_imp_valuta)
	
								IF UpperBound(ldc_imp_valuta) >= 3 THEN
									/* Assegno il valore calcolato solo se non è stato passato mediante
									l'array apposito idc_imp_valuta_mc*/
									IF IsNull(idc_imp_valuta_mc[ll_i]) THEN
										ldc_importo_avere_valuta = ldc_imp_valuta[3]
									ELSE
										ldc_importo_avere_valuta = idc_imp_valuta_mc[ll_i]
									END IF
								ELSE
									SetNull(ldc_importo_avere_valuta)
								END IF
						END CHOOSE
	
						//--------------------------------------------------------------
                  // [GIO] 26/09/03 Sbagliato perchè così ci non poteva mai mettere
                  //                  riga = 9998 perchè poi ci rimetteva ll_numero = ll_i
                  //--------------------------------------------------------------
                  /*
                  IF ib_mc_iva_alt[ll_i] = TRUE THEN
                     ll_numero = 9998
                  ELSE
                     ll_numero = ll_i
                  END IF
   
                  IF ib_mc_iva[ll_i] = TRUE THEN
                     ll_numero = 9999
                  ELSE
                     ll_numero = ll_i
                  END IF
                  */
                  IF ib_mc_iva[ll_i] = TRUE THEN
                     ll_numero = 9999
                  ELSE
                     IF ib_mc_iva_alt[ll_i] = TRUE THEN
                        ll_numero = 9998
                     ELSE
                        ll_numero = ll_i
                     END IF
                  END IF

						ldt_data_val_banca         = DateTime(id_data_val_banca[ll_i])
						ldt_data_inizio_competenza = DateTime(id_data_inizio_competenza[ll_i])
						ldt_data_fine_competenza   = DateTime(id_data_fine_competenza[ll_i])
						
						is_des_contabile[ll_i] = Left(is_des_contabile[ll_i], 80)
	
                  // -----------------------------------------------------------------------------
                  // [GIO] 02/05/05 Se il conto è costo o ricavo e la registrazione è iva acquisti/vendite
                  //                  allora riporto sulla descvrizione contabile la ragione sociale del conto
                  // -----------------------------------------------------------------------------
                  // [GIU] 24/10/2006 Se il profilo registrazione ha il falg di elabora des contabile selezionato
                  // chiamo la funzione che aggiunge la ragione sociale alla descrizione contabile del conto
                  //----------------------------------------------------------------------------------------
                  IF il_id_conto_mc[ll_i] > 0 AND (ls_tipo_registro = "IA" OR ls_tipo_registro = "IV") THEN
   
                     lb_elab_des_contabile = uf_elab_des_contabile()
                     if uf_elab_des_contabile() = true then 
                        uf_aggiungi_fornitore_a_costo(il_id_conto_mc[ll_i],is_des_contabile[ll_i],il_id_conto)
                     end if
                  END IF      
                  
                  //-----------------------------------------------------------------------------------
                  // [GIU] 08/01/2007 Aggiunto il riferimento partita (num_documento) in mov_contabile
                  //-----------------------------------------------------------------------------------         
                  setNull(ls_rif_partita)
                  if is_ges_par_aperte_new = 'S' and uf_crea_partite_da_prof() = 'S'   then
                     select dba.conto.ges_par_aperte
                        into :ls_conto_ges_par_aperte
                     from dba.conto
                     where dba.conto.id_conto = :il_id_conto_mc[ll_i]
                     using it_tran;
                     if ls_conto_ges_par_aperte = 'S'  then                      
                        choose case  ls_tipo_registro 
                           case 'IA' 
                              ls_rif_partita =  is_num_documento
                           case  'IV'
                              ls_rif_partita =  string(il_num_progressivo)
                           case else
                              ls_rif_partita =string(il_num_progressivo)
                        end choose                           
   
                     else
                        setNull(ls_rif_partita)
                     end if
                  else
                     
                     if is_ges_par_aperte_new = "S" then
                        
                        IF   (ib_crea_mov_scadenza = TRUE) AND UpperBound(il_id_scadenza) >= ll_i then
                           
                           if il_id_scadenza[ll_i] > 0 THEN                     
                           // [GIU] 14/09/2007
                           //   select dba.scadenza.num_documento   
                           select dba.scadenza.num_partita   
                                 into :ls_rif_partita
                                 from dba.scadenza
                                 where id_scadenza = :il_id_scadenza[ll_i] 
                                 using it_tran;                     
                                 
                           end if
                              
                        else
                           if ll_i <= UpperBound(is_reg_pd_mov_scadenza) then
                              
                              if UpperBound(is_reg_pd_mov_scadenza[ll_i].l_id_scadenza) > 0 THEN
                  
                                 ll_id_Scad_pag = is_reg_pd_mov_scadenza[ll_i].l_id_scadenza[1]                     
                                 
                                 if not isnull(ll_id_scad_pag) then
                                    
                                    select dba.scadenza.num_partita   
                                       into :ls_rif_partita
                                       from dba.scadenza
                                       where id_scadenza = :ll_id_scad_pag 
                                       using it_tran;
                                       
                                 end if
                                 
                              end if
                              
                           end if
                        end if
                     end if
                     
                  end if
                  
                  //---------------------------------------------------
                  // Nella nuova gestione se non c'à chiave me la calcolo 
                  //---------------------------------------------------
                  // [Piero, 26/02/09]: se il conto è legato ad un sottomastro con ges_centri_costo seleziono la chiave legata al conto o quella di default. altrimenti non faccio nulla.
                  
                  select dba.sottomastro.ges_centri_costo
                  into :ls_conto_ges_cc
                  from dba.conto
                     join dba.sottomastro on dba.sottomastro.id_sottomastro = dba.conto.id_sottomastro
                  where dba.conto.id_conto = :il_id_conto_mc[ll_i]
                  using it_tran;
                  if ls_conto_ges_cc = '' or isnull(ls_conto_ges_cc) then ls_conto_ges_cc = 'N'

                  if is_ges_cc_nuova = "S" and ls_conto_ges_cc = 'S' then // [Piero, 26/02/09]: aggiunto controllo
                     if isnull(il_id_cc_chiave[ll_i]) then
                        
                        setnull(ll_id_cc_chiave)
                        
                        select id_cc_chiave
                           into :ll_id_cc_chiave
                           from dba.cc_chiave_conto
                           where id_conto = :il_id_conto_mc[ll_i] and
                                 predefinita = 'S'
                           using it_tran;
                           
                        if not isnull(ll_id_cc_chiave) and ll_id_cc_chiave > 0 then
                           il_id_cc_chiave[ll_i]   = ll_id_cc_chiave
                        else
                           
                           select id_cc_chiave
                              into :ll_id_cc_chiave
                              from dba.par_sistema
                              using it_tran;
                           
                           if not isnull(ll_id_cc_chiave) and ll_id_cc_chiave > 0 then
                              il_id_cc_chiave[ll_i]   = ll_id_cc_chiave
                           end if
                        end if
                     end if
                  end if
                  
                  //------------------------------------------------------------------------
                  // [GIO] 15/04/2009
                  
                  if isnull(is_des_contabile[ll_i]) or is_des_contabile[ll_i] = "" then
                     is_des_contabile[ll_i] = f_proponi_descr_reg_pd(il_id_prof_registrazione)
                  end if
						
				if isnull(is_rateo_risconto[ll_i]) then
					is_rateo_risconto[ll_i] = 'N'
				end if
				
				uf_check_rateo_risconto(ll_i, ldt_data_inizio_competenza, ldt_data_fine_competenza, il_id_esercizio)		
						
	
						  INSERT INTO dba.mov_contabile
										( id_reg_pd,
										  numero,
										  id_conto,
										  id_valuta,
										  cambio,
										  des_contabile,
										  dare,
										  avere,
										  dare_valuta,
										  avere_valuta,
										  data_val_banca,
										  data_inizio_competenza,
										  data_fine_competenza,
										  riferimenti,
										  id_azienda,
                                quantita,
                                num_documento,
                                id_cantiere,
                                id_ubicazione,
                                id_materiale,
                                id_risorsa,
                                id_cc_chiave,
							rateo_risconto)
							  VALUES ( :il_id_reg_pd,
										  :ll_numero,
										  :il_id_conto_mc[ll_i],
										  :il_id_valuta_mc[ll_i],
										  :idc_cambio_mc[ll_i],
										  :is_des_contabile[ll_i],
										  :ldc_importo_dare,
										  :ldc_importo_avere,
										  :ldc_importo_dare_valuta,
										  :ldc_importo_avere_valuta,
										  :ldt_data_val_banca,
										  :ldt_data_inizio_competenza,
										  :ldt_data_fine_competenza,
										  :is_riferimenti_mc[ll_i],
										  :il_id_azienda,
                                :idc_quantita[ll_i],
                                :ls_rif_partita,
                                :il_id_cantiere_mc[ll_i],
                                :il_id_ubicazione_mc[ll_i],
                                :il_id_materiale_mc[ll_i],
                                :il_id_risorsa_mc[ll_i],
                                :il_id_cc_chiave[ll_i],
                                :is_rateo_risconto[ll_i] )										  
									USING it_tran;
	
						IF it_tran.SQLCode <> 0 THEN
							ll_SQL_mov_contabile = it_tran.SQLCode
                     if ib_display_error = true then
                        ls_msg[1]   = it_tran.sqlerrtext
                        //n_err_mgr.nf_accoda( 55584,ls_msg )
                     else
                        uf_set_info(-100,it_tran.sqlerrtext)
                     end if
							
						END IF
	
							SELECT dba.mov_contabile.id_mov_contabile
								INTO :ll_id_mov_contabile
								FROM dba.mov_contabile
								WHERE (dba.mov_contabile.id_reg_pd = :il_id_reg_pd) AND
										(dba.mov_contabile.numero = :ll_numero)
								USING it_tran;
	
						il_id_mov_contabile[ll_i] = ll_id_mov_contabile
	
	               /* Creazione delle scadenze */
                  if not isnull(is_reg_pd_scadenza) and (ll_i <= UpperBound(is_reg_pd_scadenza)) AND &
                     (UpperBound(is_reg_pd_scadenza) > 0) THEN
                     
                     if isvalid(is_reg_pd_scadenza[ll_i]) then
                        for ll_j = 1 to UpperBound(is_reg_pd_scadenza[ll_i].dt_data_scadenza)
                           
                           ll_scad_id_conto      = il_id_conto_mc[ll_i]
                           ll_scad_id_reg_pd      = il_id_reg_pd
                           ll_scad_num_mov      = ll_numero
                           ll_scad_id_mov_cont   = ll_id_mov_contabile
                           
                           ldt_scad_data_scad   = is_reg_pd_scadenza[ll_i].dt_data_scadenza[ll_j]
                           if UpperBound(is_reg_pd_scadenza[ll_i].l_id_valuta) >= ll_j then
                              ll_scad_id_valuta      = is_reg_pd_scadenza[ll_i].l_id_valuta[ll_j]
                           else
                              ll_scad_id_valuta      = il_id_valuta_mc[ll_i]
                           end if
                           
                           if UpperBound(is_reg_pd_scadenza[ll_i].dc_cambio) >= ll_j then
                              ldc_scad_cambio      = is_reg_pd_scadenza[ll_i].dc_cambio[ll_j]
                           else
                              ldc_scad_cambio      = idc_cambio_mc[ll_i]
                           end if
                           
                           ldc_scad_dare            = is_reg_pd_scadenza[ll_i].dc_dare[ll_j]
                           ldc_scad_avere            = is_reg_pd_scadenza[ll_i].dc_avere[ll_j]
                           
                           if UpperBound(is_reg_pd_scadenza[ll_i].dc_dare_valuta) >= ll_j then
                              ldc_scad_dare_v      = is_reg_pd_scadenza[ll_i].dc_dare_valuta[ll_j]
                           else
                              ldc_scad_dare_v      = ldc_scad_dare
                           end if
                           
                           if UpperBound(is_reg_pd_scadenza[ll_i].dc_avere_valuta) >= ll_j then
                              ldc_scad_avere_v      = is_reg_pd_scadenza[ll_i].dc_avere_valuta[ll_j]
                           else
                              ldc_scad_avere_v      = ldc_scad_avere
                           end if
                           
                           if UpperBound(is_reg_pd_scadenza[ll_i].l_id_tipo_pagamento) >= ll_j then
                              ll_scad_id_tipo_pag   = is_reg_pd_scadenza[ll_i].l_id_tipo_pagamento[ll_j]
                           else
                              ll_scad_id_tipo_pag   = ll_id_tipo_pagamento_def
                           end if
                           
                           if UpperBound(is_reg_pd_scadenza[ll_i].s_da_raggruppare) >= ll_j then
                              ls_scad_da_raggr      = is_reg_pd_scadenza[ll_i].s_da_raggruppare[ll_j]
                           else
                              ls_Scad_da_raggr      = "N"
                           end if

                           if UpperBound(is_reg_pd_scadenza[ll_i].s_num_documento) >= ll_j then
                              ls_Scad_num_doc      = is_reg_pd_scadenza[ll_i].s_num_documento[ll_j]
                           else
                              if not isnull(is_num_documento) and is_num_documento > "" then
                                 ls_Scad_num_doc      = is_num_documento
                              else
                                 ls_Scad_num_doc      = string(il_num_progressivo)
                              end if
                           end if
                           
                           if UpperBound(is_reg_pd_scadenza[ll_i].dt_data_documento) >= ll_j then
                              ldt_scad_data_doc      = is_reg_pd_scadenza[ll_i].dt_data_documento[ll_j]
                           else
                              if not isnull(id_data_documento) then
                                 ldt_scad_data_doc      = datetime(id_data_documento)
                              else
                                 ldt_scad_data_doc      = datetime(id_data_registrazione)
                              end if
                           end if

                           if UpperBound(is_reg_pd_scadenza[ll_i].s_num_partita) >= ll_j then
                              ls_scad_num_partita      = is_reg_pd_scadenza[ll_i].s_num_partita[ll_j]
                           else
                              if not isnull(is_num_documento) and is_num_documento > "" then
                                 ls_scad_num_partita      = is_num_documento
                              else
                                 ls_scad_num_partita      = ls_Scad_num_doc
                              end if
                           end if

                           if UpperBound(is_reg_pd_scadenza[ll_i].l_anno_partita) >= ll_j then
                              ll_scad_anno_partita      = is_reg_pd_scadenza[ll_i].l_anno_partita[ll_j]
                           else
                              ll_scad_anno_partita      = Year(id_data_registrazione)
                           end if

                           if UpperBound(is_reg_pd_scadenza[ll_i].dt_data_valuta) >= ll_j then
                              ldt_scad_data_valuta   = is_reg_pd_scadenza[ll_i].dt_data_valuta[ll_j]
                           else
                              ldt_scad_data_valuta   = ldt_scad_data_scad
                           end if
                           
                           if UpperBound(is_reg_pd_scadenza[ll_i].s_tipo_scadenza) >= ll_j then
                              ls_scad_tipo_scad      = is_reg_pd_scadenza[ll_i].s_tipo_scadenza[ll_j]
                           else
                              
                              select dba.sottomastro.uso
                                 into :ls_sottomastro_uso
                                 from dba.conto
                                       join dba.sottomastro
                                          on dba.conto.id_sottomastro = dba.sottomastro.id_sottomastro
                                 where dba.conto.id_conto = :ll_scad_id_conto
                                 using it_tran;
                              
                              choose case ls_sottomastro_uso
                                 case "C"
                                    ls_scad_tipo_scad      = "A"
                                 case "F"
                                    ls_scad_tipo_scad      = "P"
                                 case else
                                    ls_scad_tipo_scad      = "A"      
                              end choose
                              
                           end if

                           if UpperBound(is_reg_pd_scadenza[ll_i].l_id_banca_azienda) >= ll_j then
                              ll_scad_id_banca_az      = is_reg_pd_scadenza[ll_i].l_id_banca_azienda[ll_j]
                              if ll_scad_id_banca_az = 0 then setnull(ll_scad_id_banca_az)                                 
                           else
                              
                              SELECT dba.banca_sog_commerciale.id_banca_azienda
                                 INTO :ll_scad_id_banca_az
                                 FROM dba.sog_commerciale
                                      LEFT OUTER JOIN dba.banca_sog_commerciale 
                                       ON dba.banca_sog_commerciale.id_banca_sog_commerciale = dba.sog_commerciale.id_banca_sog_commerciale
                                 WHERE dba.sog_commerciale.id_conto = :ll_scad_id_conto
                                 USING it_tran;                              
                              
                           end if
                           
                           if UpperBound(is_reg_pd_scadenza[ll_i].l_id_banca_sog_commerciale) >= ll_j then
                              ll_scad_id_banca_sc      = is_reg_pd_scadenza[ll_i].l_id_banca_sog_commerciale[ll_j]
                           else
                              
                              SELECT dba.sog_commerciale.id_banca_sog_commerciale
                                 INTO :ll_scad_id_banca_az
                                 FROM dba.sog_commerciale
                                 WHERE dba.sog_commerciale.id_conto = :ll_scad_id_conto
                                 USING it_tran;
                              
                           end if
                           
                           if ll_scad_id_banca_az = 0 then setnull(ll_scad_id_banca_az)                                                         
                           if ll_scad_id_banca_sc = 0 then setnull(ll_scad_id_banca_sc)                              
                              
                           insert into dba.scadenza
                              (
                                 id_conto,
                                 id_reg_pd,
                                 num_mov_contabile,
                                 id_mov_contabile,
                                 data_scadenza,
                                 id_valuta,
                                 cambio,
                                 dare,
                                 avere,
                                 dare_valuta,
                                 avere_valuta,
                                 id_tipo_pagamento,
                                 scadenza_da_raggruppare,
                                 num_documento,
                                 data_documento,
                                 num_partita,
                                 anno_partita,
                                 data_valuta,
                                 tipo_scadenza,
                                 id_banca_azienda,
                                 id_banca_sog_commerciale
                              )
                              values
                              (
                                 :ll_scad_id_conto,
                                 :ll_scad_id_reg_pd,
                                 :ll_scad_num_mov,
                                 :ll_scad_id_mov_cont,
                                 :ldt_scad_data_scad,
                                 :ll_scad_id_valuta,
                                 :ldc_scad_cambio,
                                 :ldc_scad_dare,
                                 :ldc_scad_avere,
                                 :ldc_scad_dare_v,
                                 :ldc_scad_avere_v,
                                 :ll_scad_id_tipo_pag,
                                 :ls_scad_da_raggr,
                                 :ls_Scad_num_doc,
                                 :ldt_scad_data_doc,
                                 :ls_scad_num_partita,
                                 :ll_scad_anno_partita,
                                 :ldt_scad_data_valuta,
                                 :ls_scad_tipo_scad,
                                 :ll_scad_id_banca_az,
                                 :ll_scad_id_banca_sc
                              )
                              using it_tran;
                           
                           ll_sql_scadenza   = it_tran.sqlcode 
                           if ll_sql_scadenza <> 0 then
                              if ib_display_error = true then
                                 ls_msg[1]   = it_tran.sqlerrtext
                                 //n_err_mgr.nf_accoda( 55586,ls_msg )
                              else
                                 uf_set_info(-100,it_tran.sqlerrtext)
                              end if
                           end if
                              
                           
                        next
                     end if                     
                  end if

						/* Creazione del singolo movimento di scadenza legato al movimento contabile. */
						IF	(ib_crea_mov_scadenza = TRUE) AND &
							(il_id_scadenza[ll_i] > 0) THEN
	
							IF ib_zero_mov_scadenza = TRUE THEN
								IF IsNull(ldc_importo_dare) THEN
									ldc_importo_avere        = 0
									ldc_importo_avere_valuta = 0
								ELSE
									ldc_importo_dare        = 0
									ldc_importo_dare_valuta = 0
								END IF
							END IF
							
                     choose case ic_esito_pagamento
                        case "I" 
                        
                           setnull(ldt_data_temp)
                           
                           select data_scadenza
                              into :ldt_data_temp
                              from dba.scadenza
                              where id_scadenza = :il_id_scadenza[ll_i]
                              using it_tran;
                           
                           ldt_data_mov_Scadenza   = ldt_data_temp
                        case "P"
                           //----------------------------------------------------------
                           // [GIO] 02/03/2009 In base al tipo pagamento uso data scadenza
                           //                    oppure data registrazione   
                           //       29/06/2009 Se sto pagando un insoluto devo riportare la 
                           //                    data della registrazione
                           //----------------------------------------------------------
                           setnull(ldt_data_temp)                        
                           setnull(ll_id_tipo_pag_scad)
                           setnull(ls_tipo_pag)
                           setnull(ls_esito_prec)
                           
					  //-------------------------------------------------------------------
					// [GIO] 30/10/12 Se ho insoluti storici i successivi pagamenti vanno 
					//                                                                                          fatti con data registrazione.Questo risolve il problema
					//                                                                                          dei pagamenti parziali su insoluti senza riemissione
					//-------------------------------------------------------------------
						ll_conta_insoluti             = 0
													
						select count(*)
						 into :ll_conta_insoluti
						 from dba.mov_scadenza
						 where id_scadenza = :il_id_scadenza[ll_i] and
								  esito_pagamento = 'I'
						 using sqlca;
																					 
						if isnull(ll_conta_insoluti) then ll_conta_insoluti = 0
																																																							
                           select id_tipo_pagamento,data_scadenza,esito_pagamento
                              into :ll_id_tipo_pag_scad,:ldt_data_temp,:ls_esito_prec
                              from dba.scadenza
                              where id_scadenza = :il_id_scadenza[ll_i]
                              using sqlca;
                              
                           select tipo
                              into :ls_tipo_pag
                              from dba.tipo_pagamento
                              where id_tipo_pagamento = :ll_id_tipo_pag_scad
                              using sqlca;
                           
                           if isnull(ls_tipo_pag) then ls_tipo_pag = ""
                           if isnull(ls_esito_prec) then ls_esito_prec = ""
                           
                           if ( ls_tipo_pag = "RIBA" or ls_tipo_pag = "RID" ) and ls_esito_prec <> "I" and ll_conta_insoluti = 0  then
                              ldt_data_mov_Scadenza   = ldt_data_temp
                           else
                              ldt_data_mov_Scadenza   = ldt_data_registrazione
                           end if
      

                           
                        case else
                           
                           ldt_data_mov_Scadenza   = ldt_data_registrazione
                        
                     end choose
							
					INSERT INTO dba.mov_scadenza
						(id_mov_contabile,
						id_scadenza,
						data,
						dare,
						avere,
						dare_valuta,
						avere_valuta,
						id_azienda,
						note,
						esito_pagamento)
					VALUES
						(:ll_id_mov_contabile,
						:il_id_scadenza[ll_i],
						:ldt_data_mov_scadenza,
						:ldc_importo_dare,
						:ldc_importo_avere,
						:ldc_importo_dare_valuta,
						:ldc_importo_avere_valuta,
						:il_id_azienda,
						:is_note_mov_scadenza,
						:ic_esito_pagamento)
					USING it_tran;
					
					ll_sql_mov_scadenza=it_tran.sqlcode
						if ll_sql_mov_scadenza <> 0 then
                        if ib_display_error = true then
                           ls_msg[1]   = it_tran.sqlerrtext
                           //n_err_mgr.nf_accoda( 55586,ls_msg )
                        else
                           uf_set_info(-100,it_tran.sqlerrtext)
                        end if
                     end if

						END IF
	
						/* Creazione dei vari movimenti di scadenza legati al movimento contabile. */
						IF	(ib_crea_mov_scadenza = TRUE) AND &
							(ll_i <= UpperBound(is_reg_pd_mov_scadenza)) AND &
							(UpperBound(is_reg_pd_mov_scadenza) > 0) THEN
	
							FOR ll_j = 1 TO UpperBound(is_reg_pd_mov_scadenza[ll_i].l_id_scadenza)
								
                        setnull(ls_note_mov_scadenza)
                        setnull(ls_esito_mov_Scadenza)
                        setnull(ldt_data_mov_Scadenza)
								
								ldc_importo_dare         = is_reg_pd_mov_scadenza[ll_i].dc_dare[ll_j]
								ldc_importo_avere        = is_reg_pd_mov_scadenza[ll_i].dc_avere[ll_j]
								ldc_importo_dare_valuta  = is_reg_pd_mov_scadenza[ll_i].dc_dare_valuta[ll_j]
								ldc_importo_avere_valuta = is_reg_pd_mov_scadenza[ll_i].dc_avere_valuta[ll_j]
								ll_id_scadenza           = is_reg_pd_mov_scadenza[ll_i].l_id_scadenza[ll_j]
								
                        if UpperBound(is_reg_pd_mov_scadenza[ll_i].s_note) >= ll_j then
                           ls_note_mov_scadenza       = is_reg_pd_mov_scadenza[ll_i].s_note[ll_j]
                        end if
                        
                        if UpperBound(is_reg_pd_mov_scadenza[ll_i].s_esito_pagamento) >= ll_j then
                           ls_esito_mov_Scadenza    = is_reg_pd_mov_scadenza[ll_i].s_esito_pagamento[ll_j]
                        end if
                        
                        if UpperBound(is_reg_pd_mov_scadenza[ll_i].dt_data_movimento) >= ll_j then
                           ldt_data_mov_Scadenza    = is_reg_pd_mov_scadenza[ll_i].dt_data_movimento[ll_j]
                        end if
                        
                        if isnull(ls_note_mov_scadenza) or ls_note_mov_scadenza = "" then
                           ls_note_mov_scadenza   = is_note_mov_scadenza
                        end if
                        
                        if isnull(ls_esito_mov_Scadenza) or ls_esito_mov_Scadenza = "" then
                           ls_esito_mov_Scadenza   = ic_esito_pagamento
                        end if
                        
                        choose case ls_esito_mov_scadenza
                           case "I"   
                              setnull(ldt_data_temp)
                              
                              select data_scadenza
                                 into :ldt_data_temp
                                 from dba.scadenza
                                 where id_scadenza = :ll_id_scadenza
                                 using it_tran;
                           
                              ldt_data_mov_Scadenza   = ldt_data_temp
                           case "P"
                              //-----------------------------------------------------------
                              // [GIO] 24/02/09 Con questa modifica la contabilizzazione delle distinte
                              //                  passa la data corretta ( data scadenza ) e viene tenuta quella
                              //                  eventuali altri pagamenti o tipi di movimenti che non passano la
                              //                  data prendono data registrazione
                              //-----------------------------------------------------------
                              if isnull(ldt_data_mov_Scadenza) or ldt_data_mov_Scadenza = Datetime(date(1900,01,01)) then
                                 ldt_data_mov_Scadenza   = ldt_data_registrazione
                              else
                                 
                                 //----------------------------------------------------------
                                 // [GIO] 02/03/2009 In base al tipo pagamento uso data scadenza
                                 //                    oppure data registrazione   
                                 //       29/06/2009 Se sto pagando un insoluto devo riportare la 
                                 //                    data della registrazione
                                 //----------------------------------------------------------
                                 setnull(ldt_data_temp)                        
                                 setnull(ll_id_tipo_pag_scad)
                                 setnull(ls_tipo_pag)
                                 setnull(ls_esito_prec)

							 //-------------------------------------------------------------------
							 // [GIO] 30/10/12 Se ho insoluti storici i successivi pagamenti vanno 
							 //                                                                                          fatti con data registrazione.Questo risolve il problema
							  //                                                                                          dei pagamenti parziali su insoluti senza riemissione
							 //-------------------------------------------------------------------
							 ll_conta_insoluti             = 0
							 
							 select count(*)
							  into :ll_conta_insoluti
							  from dba.mov_scadenza
							  where id_scadenza = :ll_id_scadenza and
									 esito_pagamento = 'I'
							  using sqlca;
												  
							 if isnull(ll_conta_insoluti) then ll_conta_insoluti = 0
                                                                                                                                                                            
                                 select id_tipo_pagamento,data_scadenza,esito_pagamento
                                    into :ll_id_tipo_pag_scad,:ldt_data_temp,:ls_esito_prec
                                    from dba.scadenza
                                    where id_scadenza = :ll_id_scadenza
                                    using sqlca;
                                    
                                 select tipo
                                    into :ls_tipo_pag
                                    from dba.tipo_pagamento
                                    where id_tipo_pagamento = :ll_id_tipo_pag_scad
                                    using sqlca;
                                 
                                 if isnull(ls_tipo_pag) then ls_tipo_pag = ""
                                 if isnull(ls_esito_prec) then ls_esito_prec = ""
                                 if ( ls_tipo_pag = "RIBA" or ls_tipo_pag = "RID" ) and ls_esito_prec <> "I" and ll_conta_insoluti = 0 then
                                    ldt_data_mov_Scadenza   = ldt_data_temp
                                 else
                                    ldt_data_mov_Scadenza   = ldt_data_registrazione
                                 end if
                                 
                              end if

                              
                           case else
                              ldt_data_mov_Scadenza   = ldt_data_registrazione
                              
                        end choose
                        
                        //-----------------------------------------------------------
                        // Per sicurezza
                        //-----------------------------------------------------------
                        if isnull(ldt_data_mov_Scadenza) or ldt_data_mov_Scadenza = Datetime(date(1900,01,01)) then
                           ldt_data_mov_Scadenza   = ldt_data_registrazione
                        end if
                                             
								
								INSERT INTO dba.mov_scadenza
									(id_mov_contabile,
									id_scadenza,
									data,
									dare,
									avere,
									dare_valuta,
									avere_valuta,
									id_azienda,
									note,
									esito_pagamento)
								VALUES
									(:ll_id_mov_contabile,
									:ll_id_scadenza,
									:ldt_data_mov_scadenza,
									:ldc_importo_dare,
									:ldc_importo_avere,
									:ldc_importo_dare_valuta,
									:ldc_importo_avere_valuta,
									:il_id_azienda,
									:ls_note_mov_scadenza,
									:ls_esito_mov_scadenza)
								USING it_tran;
								
							 	ll_sql_mov_scadenza=it_tran.sqlcode
                        if ll_sql_mov_scadenza <> 0 then
                           if ib_display_error = true then
                              ls_msg[1]   = it_tran.sqlerrtext
                              //n_err_mgr.nf_accoda( 55586,ls_msg )
                           else
                              uf_set_info(-100,it_tran.sqlerrtext)
                           end if
                        end if

							NEXT
						END IF
						
                  /* Creazione dei movimenti di centri di costo. */
                  IF ib_crea_cc_mov_contabile = TRUE AND &
                     (ll_i <= UpperBound(is_reg_pd_cc_mov_contabile)) AND &
                     (UpperBound(is_reg_pd_cc_mov_contabile) > 0) THEN
                     
                     FOR ll_j = 1 TO UpperBound(is_reg_pd_cc_mov_contabile[ll_i].l_id_cc_conto)
                        
                        ls_bloccato   = is_reg_pd_cc_mov_contabile[ll_i].s_bloccato[ll_j]
                        
                        if isnull(ls_bloccato) or ls_bloccato = "" then
                           ls_bloccato = "N"
                        end if
                        
                        // Se non hi passato i valori di risorsa,cantiere ubicazione e materiale
                        // e li ho su mov contabile prendo quelli
                        if isnull(is_reg_pd_cc_mov_contabile[ll_i].l_id_cantiere[ll_j]) then
                           if not isnull(il_id_cantiere_mc[ll_i]) then
                              is_reg_pd_cc_mov_contabile[ll_i].l_id_cantiere[ll_j] = il_id_cantiere_mc[ll_i]
                           end if
                        end if

                        if isnull(is_reg_pd_cc_mov_contabile[ll_i].l_id_ubicazione[ll_j]) then
                           if not isnull(il_id_ubicazione_mc[ll_i]) then
                              is_reg_pd_cc_mov_contabile[ll_i].l_id_ubicazione[ll_j] = il_id_ubicazione_mc[ll_i]
                           end if
                        end if
                        
                        if isnull(is_reg_pd_cc_mov_contabile[ll_i].l_id_materiale[ll_j]) then
                           if not isnull(il_id_materiale_mc[ll_i]) then
                              is_reg_pd_cc_mov_contabile[ll_i].l_id_materiale[ll_j] = il_id_materiale_mc[ll_i]
                           end if
                        end if
                           
                        if isnull(is_reg_pd_cc_mov_contabile[ll_i].l_id_risorsa[ll_j]) then
                           if not isnull(il_id_risorsa_mc[ll_i]) then
                              is_reg_pd_cc_mov_contabile[ll_i].l_id_risorsa[ll_j] = il_id_risorsa_mc[ll_i]
                           end if
                        end if
                           
                        
                        INSERT INTO dba.cc_mov_contabile
                           (id_cc_conto,
                           id_mov_contabile,
                           num_riga,
                           dare,
                           avere,
                           note,
                           id_materiale,
                           id_ubicazione,
                           id_risorsa,
                           id_cantiere,
                           percentuale,
                           bloccato)
                        VALUES
                           (:is_reg_pd_cc_mov_contabile[ll_i].l_id_cc_conto[ll_j],
                           :ll_id_mov_contabile,
                           :is_reg_pd_cc_mov_contabile[ll_i].l_num_riga[ll_j],
                           :is_reg_pd_cc_mov_contabile[ll_i].dc_dare[ll_j],
                           :is_reg_pd_cc_mov_contabile[ll_i].dc_avere[ll_j],
                           :is_reg_pd_cc_mov_contabile[ll_i].s_note[ll_j],
                           :is_reg_pd_cc_mov_contabile[ll_i].l_id_materiale[ll_j],
                           :is_reg_pd_cc_mov_contabile[ll_i].l_id_ubicazione[ll_j],
                           :is_reg_pd_cc_mov_contabile[ll_i].l_id_risorsa[ll_j],
                           :is_reg_pd_cc_mov_contabile[ll_i].l_id_cantiere[ll_j],
                           :is_reg_pd_cc_mov_contabile[ll_i].dc_percentuale[ll_j],
                           :ls_bloccato
                           )
                        USING
                           it_tran;
                           
                        ll_SQL_cc_mov_contabile=it_tran.sqlcode
                        if ll_SQL_cc_mov_contabile <> 0 then
                           if ib_display_error = true then
                              ls_msg[1]   = it_tran.sqlerrtext
                              //n_err_mgr.nf_accoda( 55586,ls_msg )
                           else
                              uf_set_info(-100,it_tran.sqlerrtext)
                           end if
                        end if
                     NEXT 
                  END IF
   
						
	
					END IF
				NEXT
			END IF
         //----------------------------------------------------------------------------------------
         // [GIU] 14/02/2007 se il documento gestisce intra devo inserire anche questi dati
         //----------------------------------------------------------------------------------------
         if is_doc_intra = 'S' then 
            //uf_update_intra()
         end if
        
         /* Creazione dei record in wiz_reg_pd per l'esecuzione dei wizard. */
         ls_style = "style(type=grid)"
         lds_wiz_source = Create datastore
         //   19/01/2010 [Piero] Inserito filtro sul flag dba.wiz_profilo.utilizzabile = 'S' per operare solo sui wizard utilizzabili.
         ls_sql_wiz_source = "SELECT " + String(il_id_reg_pd) + " AS id_reg_pd, dba.wizard.id_wizard AS id_wizard, dba.prof_registrazione.id_prof_registrazione AS id_prof_registrazione, dba.wiz_profilo.num_ordine AS num_ordine, dba.wizard.nome AS nome FROM dba.wizard LEFT OUTER JOIN dba.par_wizard ON dba.wizard.id_wizard = dba.par_wizard.id_wizard LEFT OUTER JOIN dba.prof_registrazione ON dba.par_wizard.valore = dba.prof_registrazione.codice, dba.wiz_profilo WHERE ( dba.wiz_profilo.utilizzabile = 'S') and ( dba.wizard.id_wizard = dba.wiz_profilo.id_wizard ) and ( dba.wiz_profilo.id_prof_registrazione = " + String(il_id_prof_registrazione) + " )"
         ls_dwsyntax_wiz_source = it_tran.SyntaxFromSQL(ls_sql_wiz_source, ls_style, ls_errors)
         lds_wiz_source.Create(ls_dwsyntax_wiz_source, ls_errors)
         lds_wiz_source.SetTransObject(it_tran)
         
         IF lds_wiz_source.Retrieve() > 0 THEN
            FOR ll_i = 1 TO lds_wiz_source.RowCount()
               
               ll_id_reg_pd              		 = lds_wiz_source.GetItemNumber(ll_i, 1)
               ll_id_wizard               		= lds_wiz_source.GetItemNumber(ll_i, 2)
               ll_id_prof_registrazione   = lds_wiz_source.GetItemNumber(ll_i, 3)
               ll_num_ordine               = lds_wiz_source.GetItemNumber(ll_i, 4)
               ls_descrizione               = lds_wiz_source.GetItemString(ll_i, 5)
               
               INSERT INTO dba.wiz_reg_pd
                  (id_reg_pd,
                  id_wizard,
                  id_prof_registrazione,
                  num_ordine,
                  descrizione)
               VALUES
                  (:ll_id_reg_pd,
                  :ll_id_wizard,
                  :ll_id_prof_registrazione,
                  :ll_num_ordine,
                  :ls_descrizione)
               USING
                  it_tran;
                  
               IF it_tran.SQLCode <> 0 THEN
                  ll_SQL_wiz =it_tran.sqlcode
                  if ib_display_error = true then
                     ls_msg[1]   = it_tran.sqlerrtext
                     //n_err_mgr.nf_accoda( 55587,ls_msg )
                  else
                     uf_set_info(-100,it_tran.sqlerrtext)
                  end if
               END IF
            NEXT
         END IF
         
         IF IsValid(lds_wiz_source) THEN Destroy lds_wiz_source

         /* ----------------------------------------------------------------------------- */
         /* Chiama la funzione per la creazione delle scadenze.                           */
         /* ----------------------------------------------------------------------------- */
         if is_ges_par_aperte_new = "S" then
         //---------------------------------------------------------------------------------
         // [GIU] 19/09/2008
         // Ho aggiunto la condizione sul flag ib_importa_scadenze per indicare se si sta usando 
         // il programma di importazione delle registrazioni ed è necessario creare o importare le scadenze
         // La variabile è passata dall'oggetto u_import_reg_pd
         // E' stato necessario perchè venivano create doppie scadenze, dalla registrazione e dal file
         //--------------------------------------------------------------------------------
            if ib_importa_scadenze = false then 
               ll_Scad_doc   = 0
               
               if not isnull(il_id_documento) and il_id_documento > 0 then
                  select count(*) 
                     into :ll_scad_doc
                     from dba.documento
                     where id_documento = :il_id_documento
                     USING it_tran;
                     
                  if isnull(ll_scad_doc) then ll_scad_doc = 0
               end if            
               
               if ll_scad_doc = 0 then
                  ib_crea_scadenze = true
               else
                  ib_crea_scadenze = false
               end if
            end if
         end if
         
         li_retval_scadenze = uf_crea_scadenze(ll_id_con_pagamento)
         
         uf_aggiorna_scadenza()
			
         /* ----------------------------------------------------------------------------- 
         25/9/2015  Incasso automatico senza Wizard: ricordarsi di disattivare il wizard altrimenti viene doppio.
          ----------------------------------------------------------------------------- */
		uf_crea_incasso_automatico(il_id_reg_pd)
         
         /* ----------------------------------------------------------------------------- */
         /* Chiama la funzione per l'attivazioene dei wizard.                             */
         /* ----------------------------------------------------------------------------- */
         if uf_attiva_wizard(il_id_reg_pd) < 0 then
			uf_set_info(-25)
			return -25
		end if
         /* ----------------------------------------------------------------------------- */
         /* [GIU] 10/01/2007 Aggiornamento delle scadenze                                 */
         /* ----------------------------------------------------------------------------- */
   //      uf_aggiorna_scadenza(ls_tipo_registro )
         /* ----------------------------------------------------------------------------- */
         /* COMMIT e chiusura della transazione.                                          */
         /* ----------------------------------------------------------------------------- */
			IF ib_AutoCommit = TRUE THEN
				IF (ll_SQL_reg_pd = 0) AND (ll_SQL_mov_iva = 0) AND (ll_SQL_mov_contabile = 0) THEN
					IF ib_AutoCommit = TRUE THEN               
						//it_tran.nf_commit()
						COMMIT;
					END IF
	
	            if ib_calcola_cc_mov_contabile = true then
                  uf_calcola_cc_mov_cont(il_id_reg_pd)
               end if
               
               //---------------------------------------
               // Apre la finestra di modifica delle des_contabili
               //---------------------------------------
               if ib_ModifyDesContabile = true then
                  //uf_visualizza_mov_cont(il_id_reg_pd)
               end if

					uf_visualizza_reg_pd(il_id_reg_pd)
				ELSE
					//it_tran.nf_rollback()
					ROLLBACK;
					il_id_reg_pd = 0
				END IF
				
			END IF
		end if	
	else
      //------------------------------------------------------------------------------
      // [GIO] 07/08/02 Se non riesce a creare reg_pd e sono in autocommit devo chiudere la transazione
      //------------------------------------------------------------------------------
      IF ib_AutoCommit = true THEN   
         ROLLBACK;
      END IF      
		
	END IF
ELSE
   /* Non esiste un profilo con l'ID passato. */
	uf_set_info(-5)
   RETURN -5
END IF

uf_set_info(il_id_reg_pd)
RETURN il_id_reg_pd
end function

public function long uf_aggiorna (long al_id_reg_pd);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/06/98  [ANT]    Aggiorna una registrazione contabile esistente.
//
// *****************************************************************************************
/*
long ll_i, ll_SQL_mov_iva = 0, ll_SQL_mov_contabile = 0
integer li_mc_bilanciati

li_mc_bilanciati = uf_mc_bilanciati()

IF li_mc_bilanciati <> 0 THEN
	/* Movimenti contabili non bilanciati o con totali a zero. */
	RETURN li_mc_bilanciati
END IF

UPDATE
   dba.reg_pd
SET
	id_registro               = :il_id_registro,
	num_progressivo           = :il_num_progressivo,
	data_competenza           = :id_data_competenza,
	id_prof_registrazione     = :il_id_prof_registrazione,
	data_registrazione        = :id_data_registrazione,
	id_valuta                 = :il_id_valuta,
	cambio                    = :idc_cambio,
	importo                   = :idc_importo,
	imp_valuta                = :idc_imp_valuta,
	descrizione               = :is_descrizione,
	ges_oper_intracomunitarie = :ic_ges_oper_intracomunitarie,
	num_documento             = :is_num_documento,
	data_documento            = :id_data_documento,
	id_conto                  = :il_id_conto,
	id_reg_pd_padre           = :il_id_reg_pd_padre,
	segno_reg_iva             = :ii_segno_reg_iva,
	tipo_fattura              = :ic_tipo_fattura,
	id_azienda                = :il_id_azienda
WHERE
   dba.reg_pd.id_reg_pd = :al_id_reg_pd
USING
	it_tran;

IF (it_tran.SQLCode = 0) AND (it_tran.SQLNRows = 1) THEN

	IF UpperBound(idc_imponibile) > 0 THEN
		/* Aggiornamento dei movimenti IVA. */
		FOR ll_i = 1 TO UpperBound(idc_imponibile)
			UPDATE
            dba.mov_iva
			SET
				imponibile = :idc_imponibile[ll_i],
				id_tab_iva = :il_id_tab_iva[ll_i],
				imposta    = :idc_imposta[ll_i],
				id_azienda = :il_id_azienda
			WHERE
            dba.mov_iva.id_reg_pd = :al_id_reg_pd
			USING
				it_tran;

			IF it_tran.SQLCode <> 0 THEN	ll_SQL_mov_iva = it_tran.SQLCode
		NEXT
	END IF

	/* Creazione dei movimenti contabili. */
	FOR ll_i = 1 TO UpperBound(il_id_conto_mc)
		CHOOSE CASE ic_segno_mc[ll_i]
			CASE "D"
				ldc_importo_dare = idc_importo_mc[ll_i]
				ldc_importo_avere = ldc_NULL
			CASE "A"
				ldc_importo_dare = ldc_NULL
				ldc_importo_avere =idc_importo_mc[ll_i]
		END CHOOSE

		IF ib_mc_iva[ll_i] = TRUE THEN
			ll_numero = 9999
		ELSE
			ll_numero = ll_i
		END IF

        INSERT INTO dba.mov_contabile
						( id_reg_pd,
						  numero,
						  id_conto,
						  id_valuta,
						  cambio,
						  des_contabile,
						  dare,
						  avere,
						  data_val_banca,
						  data_inizio_competenza,
						  data_fine_competenza,
						  id_azienda )
			  VALUES ( :ll_id_reg_pd,
						  :ll_numero,
						  :il_id_conto_mc[ll_i],
						  :il_id_valuta_mc[ll_i],
						  :idc_cambio_mc[ll_i],
						  :is_des_contabile[ll_i],
						  :ldc_importo_dare,
						  :ldc_importo_avere,
						  :id_data_val_banca[ll_i],
						  :id_data_inizio_competenza[ll_i],
						  :id_data_fine_competenza[ll_i],
                    :il_id_azienda)
              USING it_tran;

ELSE
	ROLLBACK USING it_tran;
END IF
*/

RETURN 0
end function

public subroutine uf_reset ();// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/04/98  [ANT]    Imposta a zero tutte le variabili di istanza. Utile in caso di
//                          chiamate successive di uf_crea().
//   02  02/04/01  [ANT]    Aggiunta la selezione del campo di par_sistema che indica se 
//                          l'esercizio contabile è infra-annuale ("A") o va dall'1/1 al 31/12 ("E").
//
// *****************************************************************************************

long lal_NULL[]

char lac_NULL[]
boolean lab_NULL[]
string las_NULL[]
date lad_NULL[]
decimal ladc_NULL[]
s_reg_pd_scadenza       ls_reg_pd_scadenza[]
s_reg_pd_mov_scadenza ls_reg_pd_mov_scadenza[]

ib_show_sbilanciata           = TRUE
ib_allow_negativi              = FALSE
ib_crea_mov_scadenza         = FALSE
ib_elimina_esistente           = FALSE
ib_mc_iva                    = lab_NULL
ib_mc_iva_alt                = lab_NULL
ib_zero_mov_scadenza         = FALSE
ib_no_mc                       = FALSE

ib_crea_scadenze               = FALSE
ib_notify_existing           = TRUE
ib_display_error              = true
ib_calcola_cc_mov_contabile   = false

//is_doc_intra                 = "N"
ic_esito_pagamento           = 'P'
ic_ges_oper_intracomunitarie = 'N'
ic_segno_mc                  = lac_NULL
ic_tipo_fattura              = 'N'

id_data_fine_competenza      = lad_NULL
id_data_inizio_competenza    = lad_NULL
id_data_val_banca            = lad_NULL

id_data_partita              = lad_NULL

idc_cambio                   = 0
idc_cambio_mc                = ladc_NULL
idc_imponibile               = ladc_NULL
idc_importo                  = 0
idc_importo_mc               = ladc_NULL
idc_imposta                  = ladc_NULL

ii_segno_reg_iva             = 1

il_id_azienda                = 1
il_id_conto_mc               = lal_NULL
il_id_esercizio              = 0
il_id_mov_contabile          = lal_NULL
il_id_mov_iva                = lal_NULL
//il_id_mov_partita              = lal_NULL
il_id_par_aperta             = lal_NULL
il_id_prof_registrazione     = 0
il_id_registro               = 0
il_id_scadenza               = lal_NULL
il_id_tab_iva                = lal_NULL
il_id_valuta                 = 0
il_id_operatore              = 0
il_id_capo_commessa           = 0
il_id_valuta_mc              = lal_NULL
il_num_progressivo           = 0

il_id_cantiere_mc            = lal_NULL
il_id_ubicazione_mc          = lal_NULL
il_id_risorsa_mc             = lal_NULL
il_id_materiale_mc           = lal_NULL
il_id_cc_chiave              = lal_NULL

is_des_contabile             = las_NULL
is_descr_registro            = ""
is_info                      = ""
is_note_mov_scadenza         = ''
is_num_documento             = ""

is_rif_partita                 = las_NULL

SetNull(ic_rettifica)
setnull(id_data_calcolo_scad)
SetNull(id_data_competenza)
SetNull(id_data_documento)
SetNull(id_data_registrazione)
setnull(id_data_dec_pagamento)
SetNull(id_periodo_liquidazione)
SetNull(id_periodo_reg_iva)
SetNull(idc_imp_valuta)
SetNull(il_id_con_pagamento)
SetNull(il_id_conto)
SetNull(il_id_documento)
SetNull(il_id_reg_pd)
SetNull(il_id_reg_pd_padre)
SetNull(is_descrizione)

is_reg_pd_scadenza      = ls_reg_pd_scadenza

is_reg_pd_mov_scadenza  = ls_reg_pd_mov_scadenza

/* ----------------------------------------------------------------------- */
/* Selezione del campo di par_sistema che indica se la numerazione delle   */
/* registrazioni IVA è relativa all'anno solare o all'esercizio contabile. */
/* ----------------------------------------------------------------------- */
SELECT dba.par_sistema.rel_num_prog_iva
   INTO :ic_rel_num_prog_iva
   FROM dba.par_sistema
   WHERE dba.par_sistema.id_azienda = 1
   USING it_tran;
   
CHOOSE CASE it_tran.SQLCode
   CASE 0
   CASE ELSE
      ic_rel_num_prog_iva = 'E'
END CHOOSE


end subroutine

public subroutine uf_id_operatore (long al_id_operatore);/* --------------------------------------------------------------------------------

Tipo          : funzione
Nome          : uf_id_operatore
Autore        : [STF]
Data inizio   : 30-04-2004
Scopo         : imposta l'operatore (il_id_operatore)
Argomenti     : al_id_capo_commessa = ID dell'operatore da impostare
Ritorno       : (none)

Note :

 [nn]  gg-mm-aaaa  [<sigla>]  <descrizione della modifica effettuata, al limite su
                              più righe>
-------------------------------------------------------------------------------- */

il_id_operatore = al_id_operatore

end subroutine

public subroutine uf_imponibile_omaggi (decimal adc_imponibile_omaggi[]);
// -----------------------------------------------------------------------------------------
//   [GIU] 09/06/2005    Assegna gli imponibili omaggi (idc_imponibile_omaggi[]).
//
// -----------------------------------------------------------------------------------------

idc_imponibile_omaggi = adc_imponibile_omaggi

end subroutine

public subroutine uf_importa_scadenze (boolean ab_flag);ib_importa_scadenze	= ab_flag

end subroutine

public function integer uf_materiale_mc (long al_id_materiale_mc[]);il_id_materiale_mc = al_id_materiale_mc
return 0

end function

public subroutine uf_modify_des_contabile (boolean ab_value);ib_modifydescontabile	= ab_value
end subroutine

public subroutine uf_quantita (decimal adc_quantita[]);idc_quantita	= adc_quantita
end subroutine

public function integer uf_reg_pd_scadenza (s_reg_pd_scadenza as_reg_pd_scadenza[]);is_reg_pd_scadenza	= as_reg_pd_scadenza
return 0

end function

public function integer uf_risorsa_mc (long al_id_risorsa_mc[]);il_id_risorsa_mc = al_id_risorsa_mc
return 0

end function

public function integer uf_set_data_calcolo_scad (date ad_data);id_data_calcolo_scad   = ad_data
return 0

end function

public function integer uf_set_display_error (boolean ab_value);ib_display_error   = ab_value
return 0

end function

public subroutine uf_set_info (long al_val);string ls_ErrText, ls_RifDoc

IF IsNull(il_num_progressivo) THEN il_num_progressivo = 0
IF IsNull(id_data_registrazione) THEN id_data_registrazione = Date("01/01/1900")
IF IsNull(il_id_reg_pd) THEN il_id_reg_pd = 0

ls_RifDoc = "Registro: '" + is_descr_registro + "' Progressivo: " + string(il_num_progressivo) + " Data: " + string(id_data_registrazione, "dd/mm/yyyy") + " (" + string(il_id_reg_pd) + ") ==> "

CHOOSE CASE al_val
   CASE IS > 0
      ls_ErrText = "OK"
   CASE -1
      ls_ErrText = "Movimenti contabili non bilanciati o con totali a zero."
   CASE -2
      ls_ErrText = "Il numero degli elementi non è uguale per tutti gli array dei movimenti contabili."
   CASE -3
      ls_ErrText = "L'ID del profilo registrazione non è valido."
   CASE -4
      ls_ErrText = "L'ID dell'esercizio passato è zero o nullo e l'azienda corrente non ha un esercizio impostato."
   CASE -5
      ls_ErrText = "Non esiste un profilo con l'ID passato."
   CASE -6
      ls_ErrText = "Impossibile generare la registrazione: immettere un conto sulla riga."
   CASE -7
      ls_ErrText = "Impostare i segni dei movimenti contabili nel profilo selezionato."
   CASE -8
      ls_ErrText = "Registrazione già esistente."
   CASE -9
      ls_ErrText = "Impossibile generare la registrazione: immettere la valuta nei Parametri di Sistema."
   CASE -10
      ls_ErrText = "Il tipo del registro è di tipo PN o MN ma il totale imposta non è zero."
   CASE -11
      ls_ErrText = "Non ci sono informazioni sufficienti per creare i movimenti contabili."
   CASE -20
      ls_ErrText = "Impossibile sostituire la registrazione perchè quella esistente è stata già stampata."
   CASE -21
      ls_ErrText = "Impossibile sostituire la registrazione: errore in fase di cancellazione della registrazione esistente."
   CASE -22
      ls_ErrText = "Impossibile creare la registrazione: l'esercizio risulta bloccato."
   CASE -23
      ls_ErrText = "Impossibile creare la registrazione: il registro risulta bloccato alla data della registrazione."
   CASE -24
      ls_ErrText = "Impossibile recuperare un esercizio valido per la data registrazione: " + string(id_data_registrazione,'DD/MM/YYYY')
   CASE -100
      ls_ErrText = "Si è verificato un errore SQL."
      
   CASE ELSE
      ls_ErrText = "Errore generico durante la contabilizzazione."
END CHOOSE

IF IsNull(ls_RifDoc) THEN ls_RifDoc = ""
IF IsNUll(ls_ErrText) THEN ls_ErrText = ""

is_info = ls_RifDoc + ls_ErrText

end subroutine

public subroutine uf_set_info (long al_val, string as_msg);string ls_ErrText, ls_RifDoc

IF IsNull(il_num_progressivo) THEN il_num_progressivo = 0
IF IsNull(id_data_registrazione) THEN id_data_registrazione = Date("01/01/1900")
IF IsNull(il_id_reg_pd) THEN il_id_reg_pd = 0
IF IsNull(as_msg) THEN as_msg = ""

ls_RifDoc = "Registro: '" + is_descr_registro + "' Progressivo: " + string(il_num_progressivo) + " Data: " + string(id_data_registrazione, "dd/mm/yyyy") + " (" + string(il_id_reg_pd) + ") ==> "

CHOOSE CASE al_val
   CASE IS > 0
      ls_ErrText = "OK"
   CASE -1
      ls_ErrText = "Movimenti contabili non bilanciati o con totali a zero."
   CASE -2
      ls_ErrText = "Il numero degli elementi non è uguale per tutti gli array dei movimenti contabili."
   CASE -3
      ls_ErrText = "L'ID del profilo registrazione non è valido."
   CASE -4
      ls_ErrText = "L'ID dell'esercizio passato è zero o nullo e l'azienda corrente non ha un esercizio impostato."
   CASE -5
      ls_ErrText = "Non esiste un profilo con l'ID passato."
   CASE -6
      ls_ErrText = "Impossibile generare la registrazione: immettere un conto sulla riga."
   CASE -7
      ls_ErrText = "Impostare i segni dei movimenti contabili nel profilo selezionato."
   CASE -8
      ls_ErrText = "Registrazione già esistente."
   CASE -9
      ls_ErrText = "Impossibile generare la registrazione: immettere la valuta nei Parametri di Sistema."
   CASE -10
      ls_ErrText = "Il tipo del registro è di tipo PN o MN ma il totale imposta non è zero."
   CASE -11
      ls_ErrText = "Non ci sono informazioni sufficienti per creare i movimenti contabili."
   CASE -20
      ls_ErrText = "Impossibile sostituire la registrazione perchè quella esistente è stata già stampata."
   CASE -21
      ls_ErrText = "Impossibile sostituire la registrazione: errore in fase di cancellazione della registrazione esistente."
   CASE -22
      ls_ErrText = "Impossibile creare la registrazione: l'esercizio risulta bloccato."
   CASE -23
      ls_ErrText = "Impossibile creare la registrazione: il registro risulta bloccato alla data della registrazione."
   CASE -24
      ls_ErrText = "Impossibile recuperare un esercizio valido per la data registrazione: " + string(id_data_registrazione,'DD/MM/YYYY')
   CASE -25
      ls_ErrText = "Si è verificato un errore durante l'attivazione del wizard iva uf_attica_ivacee"
   CASE -100
      ls_ErrText = "Si è verificato un errore SQL."
      
   CASE ELSE
      ls_ErrText = "Errore generico durante la contabilizzazione."
END CHOOSE

IF IsNull(ls_RifDoc) THEN ls_RifDoc = ""
IF IsNUll(ls_ErrText) THEN ls_ErrText = ""

if as_msg > "" then
   is_info = ls_RifDoc + ls_ErrText + " Dettaglio: " + as_msg
else
   is_info = ls_RifDoc + ls_ErrText
end if

end subroutine

public subroutine uf_set_notify_existing (boolean ab_notify_existing);ib_notify_existing = ab_notify_existing

end subroutine

public subroutine uf_show_sbilanciata (boolean ab_flag);ib_show_sbilanciata = ab_flag

end subroutine

public function integer uf_sincronizza_banca_azienda (long al_id);/*
long                  ll_row
long                  ll_id_reg_pd
string               ls_integra
n_omst_banca_azienda   lnv_banca_azienda

select integra_banca_azienda
   into :ls_integra
   from dba.azienda
   where id_azienda = 1
   using sqlca;

if isnull(ls_integra) then ls_integra = "N"

if ls_integra = "S" then

   lnv_banca_azienda   = create n_omst_banca_azienda
   
   if not isnull(al_id) and al_id > 0 then
      
      lnv_banca_azienda.nf_add_reg_pd(al_id)
      
   end if
   
   if isvalid(lnv_banca_azienda) then
      destroy lnv_banca_azienda
   end if
   
end if

*/

return 0

end function

public function integer uf_ubicazione_mc (long al_id_ubicazione_mc[]);il_id_ubicazione_mc = al_id_ubicazione_mc
return 0

end function

public function integer uf_aggiorna_scadenza ();long     ll_i, ll_id_conto,ll_count
long     ll_id_mov_contabile
long   ll_numero
string ls_conto_ges_partite
string ls_crea_partite_prof
string ls_crea_partite_mc

//---------------------------------------------------------
// Se non devo creare le scadenze perchè sono state create 
// dal documento, devo fare l'update di queste di reg_pd
//---------------------------------------------------------

if ib_crea_scadenze = false then 
   update dba.scadenza
      set id_reg_pd = :il_id_reg_pd
      where dba.scadenza.id_documento = :il_id_documento
      using it_tran;
      
   select top 1 id_conto
      into :ll_id_conto
      from dba.scadenza
      where id_documento = :il_id_documento
      using it_tran;
      
   select top 1 numero
      into :ll_numero
      from dba.mov_contabile
      where id_reg_pd = :il_id_reg_pd and
            id_conto = :ll_id_conto
      using it_tran;
      
   update dba.scadenza
      set num_mov_contabile = :ll_numero
      where dba.scadenza.id_documento = :il_id_documento
      using it_tran;
   
end if

if is_ges_par_aperte_new = 'S'  then 
   
   select dba.prof_registrazione.crea_partita
      into :ls_crea_partite_prof
      from dba.prof_registrazione
         join dba.reg_pd
            on dba.reg_pd.id_prof_registrazione = dba.prof_registrazione.id_prof_registrazione
      where dba.reg_pd.id_reg_pd = :il_id_reg_pd
      using it_tran;      
   
       
   for ll_i = 1 to UpperBound(il_id_mov_contabile)
      
      ll_id_conto          = il_id_conto_mc[ll_i]
      ll_id_mov_contabile    = il_id_mov_contabile[ll_i]
      
      select dba.conto.ges_par_aperte,
             dba.mov_contabile.crea_partita
         into :ls_conto_ges_partite,
              :ls_crea_partite_mc
         from dba.mov_contabile
                  join dba.conto
                     on dba.conto.id_conto = dba.mov_contabile.id_conto
         where dba.mov_contabile.id_mov_contabile = :ll_id_mov_contabile
         using it_tran;
         
      // Se il conto gestisce partite aperte, cosi come il movimento contabile e il profilo,
      // faccio l'update di mov_contabile su scadenza 
      
      if ls_conto_ges_partite = 'S' and ls_crea_partite_mc = 'S'  and ls_crea_partite_prof='S' then 
         
         ll_count = 0
         
         select count(*)
            into :ll_count
            from dba.scadenza
            where dba.scadenza.id_conto = :ll_id_conto and
                  dba.scadenza.num_mov_contabile = :ll_i and
                  dba.scadenza.id_reg_pd = :il_id_reg_pd
            using it_tran;
         
         if isnull(ll_count) then ll_count = 0
         if ll_count   > 0 then
            update dba.scadenza
               set id_mov_contabile = :ll_id_mov_contabile               
               where dba.scadenza.id_conto = :ll_id_conto and
                     dba.scadenza.num_mov_contabile = :ll_i and
                     dba.scadenza.id_reg_pd = :il_id_reg_pd
               using it_tran;      
         else
            update dba.scadenza
               set id_mov_contabile = :ll_id_mov_contabile,
                   num_mov_contabile = :ll_i
               where dba.scadenza.id_conto = :ll_id_conto and
                     dba.scadenza.id_reg_pd = :il_id_reg_pd
               using it_tran;                  
         end if
         
      end if
   next

end if

return 0


end function

public function integer uf_aggiungi_fornitore_a_costo (long al_id_conto, ref string as_des_contabile, long al_id_conto_testa);string   ls_tipo,ls_rag_soc

select dba.mastro.tipo
   into :ls_tipo
   from dba.conto
         join dba.sottomastro
            on dba.conto.id_sottomastro = dba.sottomastro.id_sottomastro
         join dba.mastro
            on dba.sottomastro.id_mastro = dba.mastro.id_mastro
   where dba.conto.id_conto = :al_id_conto
   using it_tran;

//ROCCO    19/07/05 aggiunto anche il conto economico
if ls_tipo = "C" or ls_tipo = "R" or ls_tipo = "E" then
   
   if not isnull(al_id_conto) and al_id_conto > 0 then
      
      select descrizione
         into :ls_rag_soc
         from dba.conto
         where id_conto = :al_id_conto_testa
         using it_tran;
   
   
      if not isnull(ls_rag_soc) and ls_rag_Soc > "" then
      
         as_des_contabile   = as_des_contabile + " " + ls_rag_soc
      
         if len(as_des_contabile) > 80 then
            as_des_contabile = Left(as_des_contabile, 80)
         end if
      end if      
   end if
end if

return 0

end function

public subroutine uf_allow_negativi (boolean ab_flag);ib_allow_negativi = ab_flag

end subroutine

public subroutine uf_attiva_wizard (boolean ab_attiva_wizard);ib_attiva_wizard = ab_attiva_wizard

end subroutine

public function integer uf_attiva_wizard (long al_id_reg_pd);// --------------------------------------------------------------------------------
// Oggetto           u_reg_pd_base
// Evento/Funzione   uf_attiva_wizard
// Argomenti         value long al_id_reg_pd   
//
// Ritorno           integer
//
// Descrizione       Attiva i wizard associati alla registrazione.
// --------------------------------------------------------------------------------
//
// Revisioni
// 01 04/10/02 [ANT] Prima stesura.
//
// --------------------------------------------------------------------------------
// Copyright © 1996-2002 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------
integer li_retval = 0

uo_wizard luo_wizard 

IF ib_attiva_wizard = TRUE THEN
	
   luo_wizard = Create uo_wizard 
  // luo_wizard.uf_set_quiet_mode(TRUE)
  // luo_wizard.uf_autocommit(FALSE)
   /**********************************************************************************************
   [GIU] 21/04/2009    Nuova gestione ritenute
   **********************************************************************************************/
  	if uf_isivacee() then  
		li_retval = luo_wizard.uf_attiva_ivacee( il_id_wizard_ivacee,al_id_reg_pd)
	end if
  	Destroy luo_wizard 
	  
end if

RETURN li_retval


end function

public function long uf_calc_id_con_pagamento ();// --------------------------------------------------------------------------------
// Oggetto           u_reg_pd_base
// Evento/Funzione   uf_calc_id_con_pagamento
// Argomenti         
//
// Ritorno           long
//
// Descrizione       Calcola la condizione di pagamento.
// --------------------------------------------------------------------------------
//
// Revisioni
// 01 15/11/01 [ANT] Prima stesura.
// 02 06/12/01 [ANT] Aggiunto l'annullamento della condizione di pagamento se il profilo
//                     non ha il flag "Crea Scadenze".
//
// --------------------------------------------------------------------------------
// Copyright © 1996-2001 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

long ll_retval, ll_id_con_pagamento, ll_num_scadenze,ll_id_cpag_profilo
string ls_crea_partita,ls_con_pag_profilo_prevale

SetNull(ll_retval)

IF ib_crea_scadenze = TRUE THEN
   /* ----------------------------------------------------------------------------- */
   /* Viene considerata innanzitutto la condizione impostata con la funzione        */
   /* uf_id_con_pagamento.                                                          */
   /* ----------------------------------------------------------------------------- */
   ll_retval = il_id_con_pagamento
   
   /* ----------------------------------------------------------------------------- */
   /* Se non è stata passata nessuna condizione mediante uf_id_con_pagamento, viene */
   /* considerata quella del soggetto commerciale.                                  */
   /* ----------------------------------------------------------------------------- */
   IF IsNull(ll_retval) THEN
      SELECT dba.sog_commerciale.id_con_pagamento
         INTO :ll_id_con_pagamento
         FROM dba.sog_commerciale
         WHERE dba.sog_commerciale.id_conto = :il_id_conto
         USING it_tran;
         
      IF ll_id_con_pagamento > 0 THEN
         ll_retval = ll_id_con_pagamento
      END IF
   END IF
   
   /* ----------------------------------------------------------------------------- */
   /* Se il soggetto commerciale non ha condizione di pagamento predefinita, viene  */
   /* considerata quella del profilo registrazione.                                 */
   /* ----------------------------------------------------------------------------- */
   //IF IsNull(ll_retval) THEN
      SELECT dba.prof_registrazione.id_con_pagamento,dba.prof_registrazione.con_pag_prevale_soggetto
         INTO :ll_id_cpag_profilo,:ls_con_pag_profilo_prevale
         FROM dba.prof_registrazione
         WHERE dba.prof_registrazione.id_prof_registrazione = :il_id_prof_registrazione
         USING it_tran;
      
      //------------------------------------------------------------------------
      // Imposto la condizione di pagamento del profilo se non esiste quella passata
      // o se nel profilo è scritto che deve prevalere
      //------------------------------------------------------------------------
      IF not isnull(ll_id_cpag_profilo) and ll_id_cpag_profilo > 0 THEN
         if ls_con_pag_profilo_prevale = "S" or IsNull(ll_retval) then
            ll_retval = ll_id_cpag_profilo
         end if
      END IF
   //END IF
   
   /* ----------------------------------------------------------------------------- */
   /* Se il profilo non ha la condizione di pagamento, viene considerata quella     */
   /* predefinita memorizzata nei parametri di sistema.                             */
   /* ----------------------------------------------------------------------------- */
   IF IsNull(ll_retval) THEN
      SELECT dba.par_sistema.id_con_pagamento
         INTO :ll_id_con_pagamento
         FROM dba.par_sistema
         WHERE dba.par_sistema.id_azienda = 1
         USING it_tran;
         
      IF ll_id_con_pagamento > 0 THEN
         ll_retval = ll_id_con_pagamento
      END IF
   END IF
   
   /* ----------------------------------------------------------------------------- */
   /* Se il documento ha già creato scadenze, non vengono ricreate.                 */
   /* ----------------------------------------------------------------------------- */
   IF NOT IsNull(ll_retval) AND NOT IsNull(il_id_documento) THEN
      SELECT Count(*)
         INTO :ll_num_scadenze
         FROM dba.scadenza
         WHERE dba.scadenza.id_documento = :il_id_documento
         USING it_tran;
         
      IF ll_num_scadenze > 0 THEN
         ib_crea_scadenze = FALSE
      END IF
   END IF   

   /* ----------------------------------------------------------------------------- */
   /* In ultimo, viene controllato il flag "Crea Scadenze" del profilo: se questo è */
   /* a N, la condizione viene impostata a NULL in modo da non creare le scadenze.  */
   /* ----------------------------------------------------------------------------- */
   IF NOT IsNull(ll_retval) THEN
      SELECT dba.prof_registrazione.crea_partita
         INTO :ls_crea_partita
         FROM dba.prof_registrazione
         WHERE dba.prof_registrazione.id_prof_registrazione = :il_id_prof_registrazione
         USING it_tran;
         
      IF ls_crea_partita = "N" THEN
         ib_crea_scadenze = FALSE
      END IF
   END IF   
   
   /* ----------------------------------------------------------------------------- */
   /* Se a questo punto la variabile ll_retval è ancora nulla, non sarà possibile   */
   /* creare le scadenze.                                                           */
   /* ----------------------------------------------------------------------------- */
ELSE
   /* ----------------------------------------------------------------------------- */
   /* Se non devono essere create scadenze, imposto comunque ll_retval. Tale valore */
   /* valore verrà assegnato a reg_pd e la condizione di pagamento sarà visibile    */ 
   /* richiamando la registrazione.                                                 */
   /* ----------------------------------------------------------------------------- */
   ll_retval = il_id_con_pagamento
END IF

RETURN ll_retval

end function

public function integer uf_calcola_cc_mov_cont (long al_id_reg_pd);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  06/10/98  [ANT]    Attiva l'oggetto dei centri di costo.
//
// *****************************************************************************************
/*
long ll_i, ll_rows, ll_id_conto, ll_id_mov_contabile,ll_id_cc_chiave
decimal ldc_importo
string   ls_ges_cc,ls_ges_cc_nuova
char lc_segno
u_ds_main   lds_mov_cont
datastore   lds_mov_cont
n_cen_costo ln_cen_costo

lds_mov_cont   = create u_ds_main
lds_mov_cont.dataobject = "d_mov_contabile_gd"
lds_mov_cont.SetTransObject(SQLCA)
lds_mov_cont.Retrieve(al_id_reg_pd)

select ges_cc_nuova
   into :ls_ges_cc_nuova
   from dba.par_sistema
   using sqlca;
   
if isnull(ls_ges_cc_nuova) or ls_ges_cc_nuova = "" then ls_ges_cc_nuova = "N"   

ln_cen_costo = Create n_cen_costo

ll_rows = lds_mov_cont.RowCount()
FOR ll_i = 1 TO ll_rows
   
   ll_id_mov_contabile    = lds_mov_cont.GetItemNumber(ll_i,"id_mov_contabile")
   ll_id_conto            = lds_mov_cont.GetItemNumber(ll_i,"id_conto")
   ll_id_cc_chiave      = lds_mov_cont.GetItemNumber(ll_i,"mov_contabile_id_cc_chiave")
   ls_ges_cc            = "N"
   
   select dba.sottomastro.ges_centri_costo
      into :ls_ges_cc
      from dba.conto
            join dba.sottomastro
               on dba.conto.id_sottomastro = dba.sottomastro.id_sottomastro
      where dba.conto.id_conto = :ll_id_conto
      using sqlca;
      
   if ls_ges_cc = "S" then   
   
      IF IsNull(lds_mov_cont.GetItemDecimal(ll_i,"dare")) THEN
         ldc_importo = lds_mov_cont.GetItemDecimal(ll_i,"avere")
         lc_segno = "A"
      ELSE
         ldc_importo = lds_mov_cont.GetItemDecimal(ll_i,"dare")
         lc_segno = "D"
      END IF
      
      if not isnull(ll_id_cc_chiave) and ll_id_cc_chiave > 0 and ls_ges_cc_nuova = "S" then
         ln_cen_costo.of_start_new(ll_id_mov_contabile, ll_id_conto, ll_id_cc_chiave, ldc_importo, lc_segno, TRUE)
      else 
         ln_cen_costo.of_start(ll_id_mov_contabile, ll_id_conto, ldc_importo, lc_segno, TRUE)
      end if
      
   end if
NEXT

if isvalid(ln_cen_costo) then
   Destroy ln_cen_costo
end if

if isvalid(lds_mov_cont) then
   destroy lds_mov_cont
end if
*/
return 0

end function

public function integer uf_calcola_cc_mov_contabile (boolean ab_value);ib_calcola_cc_mov_contabile   = ab_value
return 0

end function

public function integer uf_cantiere_mc (long al_id_cantiere_mc[]);il_id_cantiere_mc = al_id_cantiere_mc
return 0

end function

public function string uf_controlla_conto (long al_id_reg_pd);long ll_row
long ll_id_conto
long ll_id_sog_commerciale
long ll_num_compensi
long ll_id_wizard
long ll_rows
long ll_num_record
long  ll_size
long  ll_rows_mov, ll_i, ll_j
long  ll_numero
long  ll_id_scadenza
string ls_nat_giuridica
string ls_tipo_registro
string ls_descrizione
string ls_tipo
string ls_tipo_conto
string ls_usa_apertura
decimal ldc_saldo_mov_scad
decimal ldc_avere
boolean lb_cancella = false
boolean lb_inserisci = false
boolean lb_controlla_wiz = false

long ll_size_array, ll_size_mov

ll_size_array = UpperBound(il_id_scadenza)
ll_size_mov   = UpperBound(is_reg_pd_mov_scadenza)

setNull(ls_tipo)

select dba.prof_registrazione.usa_apertura
   into :ls_usa_apertura
   from dba.prof_registrazione
   where dba.prof_registrazione.id_prof_registrazione = :il_id_prof_registrazione
   using it_tran;
   

   SELECT dba.registro.tipo_registro
      INTO :ls_tipo_registro           
      FROM dba.registro
      WHERE dba.registro.id_registro = :il_id_registro
      USING it_tran;
      
if ls_tipo_registro = 'IA' then 

   if not isNull(il_id_conto) and il_id_conto > 0 then 
      
      select dba.sog_commerciale.nat_giuridica,
             dba.sog_commerciale.id_sog_commerciale
         into :ls_nat_giuridica,
              :ll_id_sog_commerciale
         from dba.anagrafica
            join dba.sog_commerciale 
               on dba.sog_commerciale.id_anagrafica = dba.anagrafica.id_anagrafica
         where dba.sog_commerciale.id_conto = :il_id_conto
         using it_tran;
         
      if ls_nat_giuridica = 'F' then 
         
         ls_tipo = 'RICFATP'
         
         select count(dba.wiz_reg_pd.id_wiz_reg_pd) 
            into :ll_num_record
            from dba.wiz_reg_pd
                  join dba.wizard
                     on dba.wizard.id_wizard = dba.wiz_reg_pd.id_wizard
            where (dba.wizard.tipo = 'RICFATP' or dba.wizard.tipo = 'RITACC') and 
                        dba.wiz_reg_pd.id_reg_pd = :al_id_reg_pd
            using it_tran;
            
         if ll_num_record > 0 then 
            lb_cancella = true
            lb_inserisci = true
         else
            lb_inserisci = true
         end if   
         
         if lb_cancella = true then 
            // Sono in una transazione
            //it_tran.nf_begintran()
            
            delete from dba.wiz_reg_pd
               from dba.wiz_reg_pd 
                  join dba.wizard
                     on dba.wizard.id_wizard = dba.wiz_reg_pd.id_wizard
               where (dba.wizard.tipo = 'RICFATP' or dba.wizard.tipo = 'RITACC') and 
                     dba.wiz_reg_pd.id_reg_pd = :al_id_reg_pd
               using it_tran;
               
            if it_tran.sqlcode = 0 then 
               //it_tran.nf_commit()
               lb_cancella = false
            else
               //it_tran.nf_rollback()
            end if
            
         end if         
         
      end if
      
   end if
   
else
   
   if ls_tipo_registro <>'MN'    and ls_tipo_registro <>'PZ' and ls_usa_apertura = 'N' then
      
      ll_size = UpperBound(il_id_conto_mc)
      
      for ll_i = 1 to ll_size
         
         ll_id_conto = il_id_conto_mc[ll_i]
         
         select dba.sottomastro.uso
            into :ls_tipo_conto
            from dba.sottomastro
                  join dba.conto
                     on dba.conto.id_sottomastro = dba.sottomastro.id_sottomastro
            where dba.conto.id_conto = :ll_id_conto         
            using it_tran;
            
         if ls_tipo_conto = 'F' then                         
            
            setnull(ls_nat_giuridica)
            setnull(ll_id_sog_commerciale)
            
            select dba.sog_commerciale.nat_giuridica,
                   dba.sog_commerciale.id_sog_commerciale
               into :ls_nat_giuridica,
                    :ll_id_sog_commerciale
               from dba.anagrafica
                     join dba.sog_commerciale 
                        on dba.sog_commerciale.id_anagrafica = dba.anagrafica.id_anagrafica
               where dba.sog_commerciale.id_conto = :ll_id_conto
               using it_tran;
               
            if ls_nat_giuridica = 'F' then                         
               
               // In casi in cui pago uso o la struttura o l'array di id_scadenza               
               IF   (ib_crea_mov_scadenza = TRUE) AND (ll_i <= UpperBound(is_reg_pd_mov_scadenza)) AND (UpperBound(is_reg_pd_mov_scadenza) > 0) THEN   
                  
                  FOR ll_j = 1 TO UpperBound(is_reg_pd_mov_scadenza[ll_i].l_id_scadenza)                                    
                     
                     ll_id_scadenza = is_reg_pd_mov_scadenza[ll_i].l_id_scadenza[ll_j]
                     
                     if Not isNull(ll_id_scadenza) and ll_id_scadenza >0 then
                        // devo pagare il compenso
                        ls_tipo = 'PAGRIT'
                        lb_controlla_wiz = true
                     else
                        // controllo se l'importo è stato messo in dare o in avere
                        // Nel caso sia in avere si sta creando la parcella di un professionista 
                        // senza partita iva, perciò devo far scattare wizard RITACC
                        if ic_segno_mc[ll_i] = 'A' and idc_importo_mc[ll_i] > 0 then 
                           ls_tipo = 'RICFATP'
                        else
                           ls_tipo = 'PAGFATP'
                        end if
                        
                        lb_controlla_wiz = true         
                     end if
                  next                  
               
               end if
               
               if isNull(ls_tipo) or ls_tipo = '' then 
                  
                  IF   (ib_crea_mov_scadenza = TRUE) AND (ll_i <= UpperBound(il_id_scadenza)) AND (UpperBound(il_id_scadenza) > 0) THEN   
                     
                     FOR ll_j = 1 TO UpperBound(il_id_scadenza)                                    
                        
                        ll_id_scadenza = il_id_scadenza[ll_j]
                        
                        if Not isNull(ll_id_scadenza) and ll_id_scadenza >0 then
                           // devo pagare il compenso
                           ls_tipo = 'PAGRIT'
                           lb_controlla_wiz = true
                        else
                           // controllo se l'importo è stato messo in dare o in avere
                           // Nel caso sia in avere si sta creando la parcella di un professionista 
                           // senza partita iva, perciò devo far scattare wizard RITACC
                           if ic_segno_mc[ll_i] = 'A' and idc_importo_mc[ll_i] > 0 then 
                              ls_tipo = 'RICFATP'
                           else
                              ls_tipo = 'PAGFATP'
                           end if
                           
                           lb_controlla_wiz = true         
                        end if
                     next
                     
                  end if                                                      
                  
               end if
               
            end if
            
         end if

      next
      
      if lb_controlla_wiz = true then                            
         
         select count(dba.wiz_reg_pd.id_wiz_reg_pd) 
            into :ll_num_record
            from dba.wiz_reg_pd
                  join dba.wizard
                     on dba.wizard.id_wizard = dba.wiz_reg_pd.id_wizard
            where (dba.wizard.tipo in ('PAGRIT','PAGFATP','RICFATP', 'RITACC' )) and 
                  dba.wiz_reg_pd.id_reg_pd = :al_id_reg_pd
            using it_tran;         
            
         if ll_num_record > 0 then 
            lb_cancella = true
            lb_inserisci = true
         else
            lb_inserisci = true
         end if               
         
      else
         lb_inserisci = true
      end if
         
      if lb_cancella = true then 
         
         //Sono dentro una transazione
         //it_tran.nf_begintran()   
         
         delete from dba.wiz_reg_pd            
            from dba.wiz_reg_pd
                  join dba.wizard
                     on dba.wizard.id_wizard = dba.wiz_reg_pd.id_wizard
               where (dba.wizard.tipo in ('PAGRIT','PAGFATP','RICFATP', 'RITACC' )) and 
                     dba.wiz_reg_pd.id_reg_pd = :al_id_reg_pd
               using it_tran;
               
         if it_tran.sqlcode = 0 then 
            //it_tran.nf_commit()
            lb_cancella = false
         else
            //it_tran.nf_rollback()
         end if
         
      end if   
      
      lb_controlla_wiz = false
                     
   end if          
      
end if
   
if lb_inserisci = true then 
   
   SELECT top 1 dba.wizard.id_wizard,
         dba.wizard.nome
      into :ll_id_wizard,
           :ls_descrizione
      from dba.wizard 
      where dba.wizard.tipo = :ls_tipo
      using it_tran;
   
   if it_tran.sqlcode = 0 then 
      if not isNull(ll_id_wizard) and ll_id_wizard > 0 then 
         //it_tran.nf_begintran()
         insert into dba.wiz_reg_pd (id_reg_pd, id_wizard, num_ordine, eseguito)                  
            values (:al_id_reg_pd,:ll_id_wizard,2, 'N')
            using it_tran;
         
         if it_tran.sqlcode = 0 then 
            //it_tran.nf_commit()
         else
            //it_tran.nf_rollback()
         end if
      end if 
   end if
end if

return ls_tipo

end function

public subroutine uf_crea_cc_mov_contabile (boolean ab_crea_cc_mov_contabile);// --------------------------------------------------------------------------------
// Oggetto           u_reg_pd_base
// Evento/Funzione   uf_crea_cc_mov_contabile
// Argomenti         
//
// Ritorno           (none)
//
// Descrizione       Imposta la variabile di istanza che indica se creare i movimenti
//                     di centri di costo.
// --------------------------------------------------------------------------------
//
// Revisioni
// 01 19/12/01 [ANT] Prima stesura.
//
// --------------------------------------------------------------------------------
// Copyright © 1996-2001 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

ib_crea_cc_mov_contabile = ab_crea_cc_mov_contabile

end subroutine

public function string uf_crea_partite_da_prof ();string ls_crea_partite_prof

select dba.prof_registrazione.crea_partita
   into :ls_crea_partite_prof
   from dba.prof_registrazione
         where    dba.prof_registrazione.id_prof_registrazione = :il_id_prof_registrazione

   using it_tran;   
   
return ls_crea_partite_prof

end function

public subroutine uf_crea_scadenze (boolean ab_flag);ib_crea_scadenze = ab_flag

end subroutine

public function integer uf_crea_scadenze (long al_id_con_pagamento);// --------------------------------------------------------------------------------
// Oggetto           u_reg_pd_base
// Evento/Funzione   uf_crea_scadenze
// Argomenti         value long al_id_con_pagamento
//
// Ritorno           integer (1 = scadenze create con successo, 0 = scadenze non create, -1 = errore in fase di creazione)
//
// Descrizione       Crea le scadenze in base alla condizione di pagamento passata
//                     nell'argomento al_id_con_pagamento.
// --------------------------------------------------------------------------------
//
// Revisioni
// 01 15/11/01 [ANT] Prima stesura.
// 02 29/01/02 [ANT] Aggiunto il controllo della presenza del wizard IVACEE per generare 
//                     le scadenze al netto di IVA.
//
// --------------------------------------------------------------------------------
// Copyright © 1996-2001 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------
/*
long ll_id_tipo_pagamento[], ll_id_banca_sog_commerciale[], ll_id_banca_azienda[],ll_c
long   ll_id_conto,ll_id_profilo,ll_id_cpag_profilo,ll_id_cpag_par
char lc_tipo_scadenza[], lc_da_raggruppare[]
string   ls_prof_crea_partita,ls_conto_ges_partite
string   ls_rif_partita
date      ld_Data_partita
integer li_retval = 0
boolean   lb_crea
datetime ld_data_scadenza[]
decimal ldc_dare[], ldc_avere[], ldc_dare_valuta[], ldc_avere_valuta[]
char      lc_smuso
n_scadenza ln_scadenza

setnull(ll_id_cpag_profilo)
setnull(ll_id_cpag_par)

ln_scadenza = Create n_scadenza

IF ( ib_crea_scadenze = TRUE AND al_id_con_pagamento > 0 ) or ( is_ges_par_aperte_new = "S" and ib_crea_scadenze = TRUE ) THEN
   
   
   select id_con_pagamento
      into :ll_id_cpag_par
      from dba.par_sistema
      using sqlca;
   
   ll_id_profilo   = il_id_prof_registrazione
   
   select crea_partita,id_con_pagamento
      into :ls_prof_crea_partita,:ll_id_cpag_profilo
      from dba.prof_registrazione
      where id_prof_registrazione = :ll_id_profilo
      using sqlca;
      
   if isnull(ll_id_cpag_profilo) then
      ll_id_cpag_profilo = ll_id_cpag_par
   end if
      
   if isnull(ls_prof_crea_partita) or ls_prof_crea_partita = "" then 
      ls_prof_crea_partita = "N"   
   end if
   
   if ls_prof_crea_partita = "S" then
      
      for ll_c = 1 to UpperBound(il_id_conto_mc)
         
         ls_rif_partita      = is_rif_partita[ll_c]
         ld_data_partita   = id_data_partita[ll_c]
         
         lb_crea   = true
         
         // --------------------------------------------------------------------------------
         // [GIO] Se ci sono dei pagamenti non posso creare anche scadenze, se verranno fuori casi particolari 
         //         li gestiremo.
         // --------------------------------------------------------------------------------
         if UpperBound(is_reg_pd_mov_scadenza ) >= ll_c then
            if isvalid(is_reg_pd_mov_scadenza[ll_c]) then            
               if UpperBound(is_reg_pd_mov_scadenza[ll_c].l_id_scadenza) > 0  and ib_crea_mov_scadenza = true then
                  lb_crea = false
               end if
            end if
         end if
         
         // --------------------------------------------------------------------------------
         // [GIO] 007/08/08 I mov scadenza vengono create con due procedure
         // --------------------------------------------------------------------------------
         if UpperBound(il_id_scadenza) >= ll_c then
            if not isnull(il_id_scadenza[ll_c]) and il_id_scadenza[ll_c] > 0 and ib_crea_mov_scadenza = true then            
               lb_crea = false
            end if            
         end if
         
         
         if upperbound(ib_num_righe_no_Scad) >= ll_c and lb_crea = true then
            lb_crea   = ib_num_righe_no_Scad[ll_c]
         end if
         
         if UpperBound(is_reg_pd_scadenza ) >= ll_c then         
            if isvalid(is_reg_pd_scadenza[ll_c]) then
               if UpperBound(is_reg_pd_scadenza[ll_c].dt_data_scadenza) > 0 then
                  lb_crea = false
               end if
            end if
         end if

            
         if lb_crea = true   then
         
            setnull(ls_conto_ges_partite)
            
            ll_id_conto = il_id_conto_mc[ll_c]
            
            select ges_par_aperte
               into :ls_conto_ges_partite
               from dba.conto
               where id_conto = :ll_id_conto
               using sqlca;
   
            lc_smuso = ''
   
            select   dba.sottomastro.uso
               into   :lc_smuso
               from   dba.sottomastro
                     join dba.conto
                        on dba.sottomastro.id_sottomastro = dba.conto.id_sottomastro
               where   dba.conto.id_conto = :ll_id_conto
               using sqlca;
            
            if isnull(ls_conto_ges_partite) or ls_conto_ges_partite = "" then ls_conto_ges_partite = "N"   
            
            IF ls_prof_crea_partita = "S" and ls_conto_ges_partite = "S" then
               
               if ll_c = 1 and il_id_conto_mc[ll_c] = il_id_conto then
                  
                  ln_scadenza.nf_reset()
                  
                  if not isnull(id_data_calcolo_scad) then
                     ln_scadenza.nf_set_data_calcolo_Scad(id_data_calcolo_scad)
                  else
                     if not isnull(id_data_dec_pagamento) then
                        ln_scadenza.nf_set_data_calcolo_Scad(id_data_dec_pagamento)
                     else
                        IF NOT IsNull(id_data_documento) THEN
                           ln_scadenza.nf_set_data_calcolo_Scad(id_data_documento)
                        else
                           ln_scadenza.nf_set_data_calcolo_Scad(id_data_registrazione)
                        end if
                     end if
                  end if
                  
                  ln_scadenza.nf_set_imponibile(uf_get_tot_imponibile())
                  ln_scadenza.nf_set_imponibile_valuta(uf_get_tot_imponibile_valuta())
                  IF uf_IsIVACEE() = FALSE THEN
                     ln_scadenza.nf_set_imposta(uf_get_tot_imposta())
                     ln_scadenza.nf_set_imposta_valuta(uf_get_tot_imposta_valuta())
                  ELSE
                     ln_scadenza.nf_set_imposta(0)
                     ln_scadenza.nf_set_imposta_valuta(0)
                  END IF

                  if not isnull(ld_data_partita) then
                     ln_scadenza.nf_set_anno_partita(year(ld_data_partita))
                  else
                     IF NOT IsNull(id_data_documento) THEN
                        ln_scadenza.nf_set_anno_partita(year(id_data_documento))
                     else
                        ln_scadenza.nf_set_anno_partita(year(id_data_registrazione))
                     end if
                  end if
                  
                  IF NOT IsNull(id_data_documento) THEN
                     ln_scadenza.nf_set_data_documento(id_data_documento)
                  else
                     ln_scadenza.nf_set_data_documento(id_data_registrazione)
                  end if
                  
                  if not isnull(ls_rif_partita) and ls_rif_partita > "" then
                     ln_scadenza.nf_set_num_partita(ls_rif_partita)
                  else
                     if isnull(is_num_documento) or is_num_documento = "" then                        
                        ln_scadenza.nf_set_num_partita(string(il_num_progressivo))                     
                     else
                        ln_scadenza.nf_set_num_partita(is_num_documento)                                             
                     end if
                  end if
                  ln_scadenza.nf_set_num_documento(is_num_documento)
            //------------------------------------------------------------------------------                  
                  ln_scadenza.nf_set_num_mov_contabile(ll_c)
                  ln_scadenza.nf_set_id_con_pagamento(al_id_con_pagamento)
                  ln_scadenza.nf_set_tipo_fattura(ic_tipo_fattura)
                  ln_scadenza.nf_set_id_valuta(il_id_valuta)
                  ln_scadenza.nf_set_cambio(idc_cambio)
                  ln_scadenza.nf_set_esercizio_id_valuta(il_esercizio_id_valuta)
                  ln_scadenza.nf_set_esercizio_cambio(idc_esercizio_cambio)
                  ln_scadenza.nf_set_id_conto(il_id_conto)
                  ln_scadenza.nf_set_sottomastro_uso(lc_smuso)                  
//                  ln_scadenza.nf_set_sottomastro_uso(ic_sottomastro_uso)
                  ln_scadenza.nf_set_id_reg_pd(il_id_reg_pd)
                  ln_scadenza.nf_set_update(TRUE)
                  li_retval = ln_scadenza.nf_crea()
                  
               else
                  
                  if not isnull(ll_id_cpag_profilo) and ll_id_cpag_profilo > 0 then
                     ln_scadenza.nf_reset()
                  
                     IF ic_segno_mc[ll_c] = "A" THEN
                        ln_scadenza.nf_set_segno_scadenza("A")
                     ELSE
                        ln_scadenza.nf_set_segno_scadenza("D")
                     END IF
                     
                     ln_scadenza.nf_set_imponibile(idc_importo_mc[ll_c])
                     ln_scadenza.nf_set_imposta(0)               
                     ln_scadenza.nf_set_imponibile_valuta(idc_imp_valuta_mc[ll_c])
                     ln_scadenza.nf_set_imposta_valuta(0)
                     
                     if not isnull(id_data_calcolo_scad) then
                        ln_scadenza.nf_set_data_calcolo_Scad(id_data_calcolo_scad)
                     else
                        if not isnull(id_data_dec_pagamento) then
                           ln_scadenza.nf_set_data_calcolo_Scad(id_data_dec_pagamento)
                        else
                           if not isnull(id_data_dec_pagamento) then
                              ln_scadenza.nf_set_data_calcolo_Scad(id_data_dec_pagamento)
                           else
                              IF NOT IsNull(id_data_documento) THEN
                                 ln_scadenza.nf_set_data_calcolo_Scad(id_data_documento)
                              else
                                 ln_scadenza.nf_set_data_calcolo_Scad(id_data_registrazione)
                              end if
                           end if
                        end if
                     end if
                     
                     if not isnull(ld_data_partita) then
                        ln_scadenza.nf_set_anno_partita(year(ld_data_partita))
                     else
                        IF NOT IsNull(id_data_documento) THEN
                           ln_scadenza.nf_set_anno_partita(year(id_data_documento))
                        else
                           ln_scadenza.nf_set_anno_partita(year(id_data_registrazione))
                        end if
                     end if
                     
                     IF NOT IsNull(id_data_documento) THEN
                        ln_scadenza.nf_set_data_documento(id_data_documento)
                     else
                        ln_scadenza.nf_set_data_documento(id_data_registrazione)
                     end if
                     if not isnull(ls_rif_partita) and ls_rif_partita > "" then
                        ln_scadenza.nf_set_num_partita(ls_rif_partita)
                     else
                        if isnull(is_num_documento) or is_num_documento = "" then                        
                           ln_scadenza.nf_set_num_partita(string(il_num_progressivo))                     
                        else
                           ln_scadenza.nf_set_num_partita(is_num_documento)                                             
                        end if
                     end if
                     ln_scadenza.nf_set_num_documento(is_num_documento)
                     
                     ln_scadenza.nf_set_num_mov_contabile(ll_c)
                     
                     ln_scadenza.nf_set_id_con_pagamento(ll_id_cpag_profilo)
                     
                     ln_scadenza.nf_set_id_valuta(il_id_valuta)
                     ln_scadenza.nf_set_cambio(idc_cambio)
                     ln_scadenza.nf_set_esercizio_id_valuta(il_esercizio_id_valuta)
                     ln_scadenza.nf_set_esercizio_cambio(idc_esercizio_cambio)
                     ln_scadenza.nf_set_id_conto(il_id_conto_mc[ll_c])
                     ln_scadenza.nf_set_sottomastro_uso(lc_smuso)
//                     ln_scadenza.nf_set_sottomastro_uso(ic_sottomastro_uso)
                     ln_scadenza.nf_set_id_reg_pd(il_id_reg_pd)
                     ln_scadenza.nf_set_update(TRUE)
                     li_retval = ln_scadenza.nf_crea()               
                  end if
                  
               end if
               
            end if
         end if         
      next
      
   end if
   
END IF

Destroy ln_scadenza

RETURN li_retval

*/
return 0

end function

public function integer uf_data_dec_pagamento (date ad_data);id_data_dec_pagamento = ad_data
return 0

end function

public function integer uf_data_partita_mc (date ad_data_partita[]);id_data_partita   = ad_data_partita
return 0

end function

public subroutine uf_doc_intra (string as_doc_intra);//-----------------------------------------------------------------------------
// [GIU] 14/02/2007 Assegna il valore (S o N) alla variabile di istanza che 
// indica se il documento da contabilizzare gestisce Intra
//---------------------------------------------------------------------------
is_doc_intra = as_doc_intra

end subroutine

public function boolean uf_elab_des_contabile ();boolean lb_retval = TRUE
string  ls_elab_des_contabile

IF il_id_prof_registrazione > 0 THEN
   
   SELECT dba.prof_registrazione.elab_des_contabile
      INTO :ls_elab_des_contabile
      FROM dba.prof_registrazione
           
      WHERE   dba.prof_registrazione.id_prof_registrazione = :il_id_prof_registrazione            
      USING it_tran;
      
      if it_tran.Sqlcode = 0 then 
         if ls_elab_des_contabile = 'N' then 
            lb_retval = false
         else
            lb_retval = true
         end if
      end if
   
END IF

RETURN lb_retval

end function

public subroutine uf_ges_art74 (string as_ges_art74[]);//-----------------------------------------------------------------------------
// [GIU] 28/08/2006 Assegna il valore (S o N) alla variabile di istanza che 
// indica se il tipo iva gestisce articolo74 (rottami ferrosi)
//---------------------------------------------------------------------------

is_ges_art74[] = as_ges_art74[]

end subroutine

public function decimal uf_get_tot_imponibile ();// --------------------------------------------------------------------------------
// Oggetto           u_reg_pd_base
// Evento/Funzione   uf_get_tot_imponibile
// Argomenti         
//
// Ritorno           decimal
//
// Descrizione       Restituisce il totale imponibile sommando i valori dell'array relativo.
// --------------------------------------------------------------------------------
//
// Revisioni
// 01 16/11/01 [ANT] Prima stesura.
// 02 11/12/01 [ANT] Aggiunto controllo sull'esistenza di elementi dell'array idc_imponibile.
//                     Se non ha elementi, allora si tratta di una registrazione di PrimaNota.
//                     In tal caso il totale imponibile è dato dall'importo del primo movimento
//                     contabile e il totale imposta è zero.
//
// --------------------------------------------------------------------------------
// Copyright © 1996-2001 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

decimal ldc_retval = 0
long ll_i

IF UpperBound(idc_imponibile) > 0 THEN
   FOR ll_i = 1 TO UpperBound(idc_imponibile)
      ldc_retval += idc_imponibile[ll_i]
   NEXT
ELSE
   IF UpperBound(idc_importo_mc) >= 1 THEN
      ldc_retval = idc_importo_mc[1]
   END IF
END IF

RETURN ldc_retval

end function

public function decimal uf_get_tot_imponibile_valuta ();// --------------------------------------------------------------------------------
// Oggetto           u_reg_pd_base
// Evento/Funzione   uf_get_tot_imponibile_valuta
// Argomenti         
//
// Ritorno           decimal
//
// Descrizione       
// --------------------------------------------------------------------------------
//
// Revisioni
// 01 16/11/01 [ANT] Prima stesura.
//
// --------------------------------------------------------------------------------
// Copyright © 1996-2001 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

decimal ldc_retval = 0, ldc_tot_imponibile_valuta[], ldc_tot_imponibile

ldc_tot_imponibile = uf_get_tot_imponibile()

in_currency_sync.nf_converti_valore(il_esercizio_id_valuta,    &
                                    idc_esercizio_cambio,      &
                                    ldc_tot_imponibile,        &
                                    il_id_valuta,              &
                                    idc_cambio,                &
                                    ldc_tot_imponibile_valuta)

IF UpperBound(ldc_tot_imponibile_valuta) >= 3 THEN
   ldc_retval = ldc_tot_imponibile_valuta[3]
END IF

RETURN ldc_retval

end function

public function decimal uf_get_tot_imposta ();// --------------------------------------------------------------------------------
// Oggetto           u_reg_pd_base
// Evento/Funzione   uf_get_tot_imposta
// Argomenti         
//
// Ritorno           decimal
//
// Descrizione       Restituisce il totale delle imposte sommando gli elementi dell'array
//                     relativo.
// --------------------------------------------------------------------------------
//
// Revisioni
// 01 16/11/01 [ANT] Prima stesura.
// 02 11/12/01 [ANT] Aggiunto controllo sull'esistenza di elementi dell'array idc_imposta.
//                     Se non ha elementi, allora si tratta di una registrazione di PrimaNota.
//                     In tal caso il totale imponibile è dato dall'importo del primo movimento
//                     contabile e il totale imposta è zero.
//
// --------------------------------------------------------------------------------
// Copyright © 1996-2001 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

decimal ldc_retval = 0
long ll_i

IF UpperBound(idc_imposta) > 0 THEN
   FOR ll_i = 1 TO UpperBound(idc_imposta)
      ldc_retval += idc_imposta[ll_i]
   NEXT
ELSE
   ldc_retval = 0
END IF

RETURN ldc_retval

end function

public function decimal uf_get_tot_imposta_valuta ();// --------------------------------------------------------------------------------
// Oggetto           u_reg_pd_base
// Evento/Funzione   uf_get_tot_imposta_valuta
// Argomenti         
//
// Ritorno           decimal
//
// Descrizione       
// --------------------------------------------------------------------------------
//
// Revisioni
// 01 16/11/01 [ANT] Prima stesura.
//
// --------------------------------------------------------------------------------
// Copyright © 1996-2001 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

decimal ldc_retval = 0, ldc_tot_imposta_valuta[], ldc_tot_imposta

ldc_tot_imposta = uf_get_tot_imposta()

in_currency_sync.nf_converti_valore(il_esercizio_id_valuta,    &
                                    idc_esercizio_cambio,      &
                                    ldc_tot_imposta,        &
                                    il_id_valuta,              &
                                    idc_cambio,                &
                                    ldc_tot_imposta_valuta)

IF UpperBound(ldc_tot_imposta_valuta) >= 3 THEN
   ldc_retval = ldc_tot_imposta_valuta[3]
END IF

RETURN ldc_retval

end function

public subroutine uf_id_capo_commessa (long al_id_capo_commessa);/* --------------------------------------------------------------------------------

Tipo          : funzione
Nome          : uf_id_capo_commessa
Autore        : [STF]
Data inizio   : 30-04-2004
Scopo         : imposta il capo commessa (il_id_capo_commessa)
Argomenti     : al_id_capo_commessa = ID del capo commessa da impostare
Ritorno       : (none)

Note :

 [nn]  gg-mm-aaaa  [<sigla>]  <descrizione della modifica effettuata, al limite su
                              più righe>
-------------------------------------------------------------------------------- */

il_id_capo_commessa = al_id_capo_commessa

end subroutine

public function integer uf_id_cc_chiave_mc (long al_id_cc_chiave[]);il_id_cc_chiave = al_id_cc_chiave
return 0

end function

public subroutine uf_id_cc_mov_contabile (s_reg_pd_cc_mov_contabile as_reg_pd_cc_mov_contabile[]);is_reg_pd_cc_mov_contabile = as_reg_pd_cc_mov_contabile

end subroutine

public function boolean uf_isivacee ();// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  02/10/98  [ANT]    Restituisce TRUE se il profilo ha collegato il Wizard IVACEE.
//     02  19/01/2010 [Piero] Inserito flag dba.wiz_profilo.utilizzabile = 'S' per operare solo sui wizard che sono utilizzabili.
// *****************************************************************************************

boolean lb_retval = FALSE
long ll_id_wizard

IF il_id_prof_registrazione > 0 THEN
   SetNull(ll_id_wizard)
   SELECT dba.wizard.id_wizard
      INTO :ll_id_wizard
      FROM dba.prof_registrazione,
           dba.wiz_profilo,
           dba.wizard
      WHERE dba.prof_registrazione.id_prof_registrazione = dba.wiz_profilo.id_prof_registrazione AND
            dba.wizard.id_wizard = dba.wiz_profilo.id_wizard AND
            dba.prof_registrazione.id_prof_registrazione = :il_id_prof_registrazione AND
            dba.wizard.tipo = 'IVACEE' AND
            dba.wiz_profilo.utilizzabile = 'S'
      USING it_tran;

   IF ll_id_wizard > 0 THEN
      lb_retval = TRUE
   END IF
END IF

return lb_retval

end function

public function integer uf_rif_partita_mc (string as_rif_partita[]);is_rif_partita = as_rif_partita
return 0

end function

public function integer uf_righe_no_crea_scad (boolean al_righe[]);ib_num_righe_no_scad	= al_righe
return 0

end function

public subroutine uf_set_transaction (ref transaction at_tran);string ls_str

ls_str = ""

//it_tran = at_tran

in_currency_sync = create n_currency_sync
in_currency_sync.uf_set_transaction(it_tran)

iu_reg_pd_funct = create u_reg_pd_funct
iu_reg_pd_funct.uf_set_transaction(it_tran)


select dba.par_sistema.ges_par_aperte_new,ges_ritenute_new
   into :is_ges_par_aperte_new, :is_ges_ritenute_new
   from dba.par_sistema
   where dba.par_sistema.id_azienda = 1 
   using it_tran;
   
select ges_cc_nuova
   into :is_ges_cc_nuova
   from dba.par_sistema
   using it_tran;   


end subroutine

public function integer uf_check_rateo_risconto (long al_row, datetime adt_data_inizio, datetime adt_data_fine, long al_id_esercizio);long                      ll_id_conto
string    ls_tipo_mastro,ls_risconto
datetime            ldt_in_es,ldt_fn_es

//------------------------------------------------------------
// Se diverso da N allora qualcuno lo ha imposto dal chiamante
//------------------------------------------------------------
if is_rateo_risconto[al_row] <> "N" then
                return 0
end if

select data_inizio,data_fine
                into :ldt_in_es,:ldt_fn_es
                from dba.esercizio
                where id_esercizio = :al_id_esercizio
                using sqlca;
                
if adt_data_fine > ldt_fn_es then           
                ls_risconto = "S"
else
                if adt_data_inizio < ldt_in_es then
                               ls_risconto = "T"
                end if
end if

if UpperBound(il_id_conto_mc) >= al_row then
                
                ll_id_conto        = il_id_conto_mc[al_row]
                
                if not isnull(ll_id_conto) and ll_id_conto > 0 then
                               
                               select dba.mastro.tipo
                                               into :ls_tipo_mastro
                                               from dba.conto
                                                                              join dba.sottomastro
                                                                                              on dba.conto.id_sottomastro = dba.sottomastro.id_sottomastro
                                                                               join dba.mastro
                                                                                              on dba.sottomastro.id_mastro = dba.mastro.id_mastro
                                               where id_conto = :ll_id_conto
                                               using sqlca;
                                               
                               if ( ls_tipo_mastro = 'C' or ls_tipo_mastro = 'R' ) and ( ls_risconto = 'S' or ls_risconto = 'T' ) then
                                               is_rateo_risconto[al_row] = ls_risconto
                               end if
                               
                end if
                
end if

return 0

end function

public function integer uf_crea_incasso_automatico (long al_id_reg_pd);long                                      ll_id_conto,ll_c
long                                      ll_id_con_pag
long                                      ll_id_doc,ll_id_valuta,ll_null,ll_id_reg_pd 
long                                      ll_id_prog_reg,ll_id_conto_incasso,ll_id_conto_abb_pass,ll_id_conto_abb_att
long                                      ll_id_scadenza,ll_id_tipo_pag_scad,ll_num_reg
long                                      ll_id_conto_mc[],ll_id_scadenza_mc[]
string                    ls_incasso_contestuale,ls_tipo_pagamento,ls_cod_pagamento
string                    ls_riferimenti_mc[],ls_descrizione_mc[],ls_msg[]
string                    ls_num_doc,ls_data_doc,ls_num_reg,ls_rif
char                                      lc_segno_mc[]
datetime                            ldt_data_reg,ldt_data_doc
decimal                               ldc_tot_doc,ldc_tot_inc,ldc_abbuono,ldc_cambio
decimal                               ldc_saldo,ldc_tot_cpt
decimal                               ldc_importo_mc[]
u_reg_pd_base                    lnv_reg_pd
datastore						 lds_ds

if ib_disabilita_incasso_automatico = true then
                return 0
end if

ll_id_reg_pd      = 0

select dba.reg_pd.data_registrazione,
                               dba.reg_pd.id_conto,
                               dba.reg_pd.id_con_pagamento,
                               dba.reg_pd.importo,
                               dba.documento.contante,
                               dba.con_pagamento.incasso_contestuale,
                               dba.reg_pd.id_valuta,
                               dba.reg_pd.cambio,
                               dba.reg_pd.num_documento,
                               dba.reg_pd.data_documento,
                               dba.registro.numero
                into       :ldt_data_reg,
                                               :ll_id_conto,
                                :ll_id_con_pag,
                                               :ldc_tot_doc,
                                               :ldc_tot_inc,
                                               :ls_incasso_contestuale,
                                               :ll_id_valuta,
                                               :ldc_cambio,
                                               :ls_num_doc,
                                               :ldt_data_doc,
                                               :ll_num_reg
                from dba.reg_pd
                                               join dba.registro
                                                               on dba.reg_pd.id_registro = dba.registro.id_registro
                                               left outer join dba.documento
                                                               on dba.reg_pd.id_documento = dba.documento.id_documento
                                               left outer join dba.con_pagamento
                                                               on dba.reg_pd.id_con_pagamento = dba.con_pagamento.id_con_pagamento
                where dba.reg_pd.id_reg_pd = :al_id_reg_pd
                using sqlca;

if isnull(ldc_tot_doc) then ldc_tot_doc = 0
if isnull(ldc_tot_inc) then ldc_tot_inc = 0
if isnull(ls_incasso_contestuale) or ls_incasso_contestuale = "" then ls_incasso_contestuale = "N"

if ls_incasso_contestuale = "S" then
                
                if not isnull(ll_id_con_pag) and ll_id_con_pag > 0 and &
                               not isnull(ll_id_conto) and ll_id_conto > 0 then
                               
                               select id_prof_reg_incasso,id_conto_incasso,id_conto_abbuoni_pass,id_conto_abbuoni_att
                                               into :ll_id_prog_reg,:ll_id_conto_incasso,:ll_id_conto_abb_pass,:ll_id_conto_abb_att
                                               from dba.con_pagamento
                                               where id_con_pagamento = :ll_id_con_pag
                                               using sqlca;
                               
                               if not isnull(ll_id_prog_reg) and ll_id_prog_reg > 0 and &
                                               not isnull(ll_id_conto_incasso) and ll_id_conto_incasso > 0 then
                                               
                                               if ldc_tot_inc > 0 then
                                                               ldc_abbuono     = ldc_tot_doc - ldc_tot_inc
                                               else
                                                               ldc_abbuono     = 0
                                               end if
                                               
                                               lnv_reg_pd        = create u_reg_pd_base
                                               
                                               lds_ds                  = create datastore
                                               lds_ds.dataobject = "d_scadenze_incasso_contestuale_gd"
                                               lds_ds.SetTransObject(sqlca)
                                               lds_ds.Retrieve(al_id_reg_pd)
                                               
                                               //sqlca.nf_begintran()
                                               
                                               SetNull(ll_NULL)

                                               if lds_ds.RowCount() > 0 then

                                                               lnv_reg_pd.uf_reset()
                                                               
                                                               // Testata
                                                              lnv_reg_pd.uf_autocommit(false)
                                                               lnv_reg_pd.uf_id_prof_registrazione(ll_id_prog_reg)
                                                               lnv_reg_pd.uf_data_registrazione(date(ldt_data_reg))
                                                               lnv_reg_pd.uf_id_valuta(ll_id_valuta)
                                                               lnv_reg_pd.uf_cambio(ldc_cambio)
                                                               
                                                               ls_data_doc = string(date(ldt_data_doc),"dd/mm/yyyy")
                                                               ls_num_reg       = string(ll_num_reg)
                                                               
                                                               if not isNull(ls_num_reg) and ls_num_reg > "" then 
                                                                              ls_num_doc += "/" + ls_num_reg
                                                               end if
                                                               
                                                               IF IsNull(ls_num_doc) THEN ls_num_doc = ""
                                                              IF IsNull(ls_data_doc) THEN ls_data_doc = ""
                                                               
                                                               IF ls_num_doc > "" THEN
                                                                              ls_rif = "Rif. Doc.: N. " + ls_num_doc + " del " + ls_data_doc 
                                                               END IF
                
                                                               for ll_c = 1 to lds_ds.RowCount()
                               
                                                                              ll_id_scadenza                                 = lds_ds.GetItemNumber(ll_c,"id_scadenza")
                                                                              ldc_saldo                                                           = lds_ds.GetItemDecimal(ll_c,"saldo")
                                                                              
                                                                              ll_id_conto_mc[UpperBound(ll_id_conto_mc) + 1]  = ll_id_conto
                                                                              ldc_importo_mc[UpperBound(ldc_importo_mc) + 1]    = ldc_saldo
                                                                              lc_segno_mc[UpperBound(lc_segno_mc) + 1]        = "A"
                                                                              
                                                                              ldc_tot_cpt += ldc_saldo
                                                                              
                                                                              ll_id_scadenza_mc[UpperBound(ll_id_scadenza_mc) + 1] = ll_id_scadenza
                                                                              ls_riferimenti_mc[UpperBound(ls_riferimenti_mc) + 1] = ls_rif
                                                                                                              
                                                               next                      

                                                               if ldc_Abbuono > 0 then 
                                                                              ll_id_conto_mc[UpperBound(ll_id_conto_mc) + 1]  = ll_id_conto_abb_pass                                                                                                                                                  
                                                                              ldc_importo_mc[UpperBound(ldc_importo_mc) + 1]    = ldc_Abbuono
                                                                              lc_segno_mc[UpperBound(lc_segno_mc) + 1]        = "D"
                                                                                                              
                                                                              ll_id_scadenza_mc[UpperBound(ll_id_scadenza_mc) + 1] = ll_null
                                                                              ls_riferimenti_mc[UpperBound(ls_riferimenti_mc) + 1] = ls_rif
                                                                              ls_descrizione_mc[UpperBound(ll_id_conto_mc)] = 'Abbuono Passivo' 
                                                                              
                                                               end if
                                                               
                                                               if ldc_Abbuono < 0 then 
                                                                              ll_id_conto_mc[UpperBound(ll_id_conto_mc) + 1]  = ll_id_conto_abb_att                                                                                                                      
                                                                              ldc_importo_mc[UpperBound(ldc_importo_mc) + 1]    = Abs(ldc_Abbuono)
                                                                              lc_segno_mc[UpperBound(lc_segno_mc) + 1]        = "A"
                                                                              
                                                                              ll_id_scadenza_mc[UpperBound(ll_id_scadenza_mc) + 1] = ll_null
                                                                               ls_riferimenti_mc[UpperBound(ls_riferimenti_mc) + 1] = ls_rif
                                                                              ls_descrizione_mc[UpperBound(ll_id_conto_mc)] = 'Abbuono Attivo'
                                                               end if
                                                                              
                                                               // -----------------------------------------------------
                                                               // Contropartita di cassa
                                                               // -----------------------------------------------------
                                                               ll_id_conto_mc[UpperBound(ll_id_conto_mc) + 1]       = ll_id_conto_incasso
                                                               ldc_importo_mc[UpperBound(ldc_importo_mc) + 1]       = ldc_tot_cpt - ldc_Abbuono
                                                               lc_segno_mc[UpperBound(lc_segno_mc) + 1]             = "D"
                                                               ll_id_scadenza_mc[UpperBound(ll_id_scadenza_mc) + 1] = ll_null
                                                               
                                                               // Libro giornale
                                                               
                                                               lnv_reg_pd.uf_id_conto_mc(ll_id_conto_mc)
                                                               lnv_reg_pd.uf_importo_mc(ldc_importo_mc)
                                                               lnv_reg_pd.uf_segno_mc(lc_segno_mc)
                                                               lnv_reg_pd.uf_id_scadenza(ll_id_scadenza_mc)
                                                               lnv_reg_pd.uf_riferimenti_mc(ls_riferimenti_mc)
                                                               lnv_reg_pd.uf_des_contabile(ls_descrizione_mc)
                                                               lnv_reg_pd.uf_crea_mov_scadenza(TRUE)
                                                               lnv_reg_pd.uf_DisplayMode(0)
                                                                              
                                                               ll_id_reg_pd = lnv_reg_pd.uf_crea()
                                                                              
                                                               if ll_id_reg_pd > 0 then
                                                                              //sqlca.nf_commit()                                     
                                                               else
                                                                              ll_id_reg_pd      = -1
                                                                              //sqlca.nf_rollback()
                                                                              //---------------------------------------------------------------------
                                                                              // [GIU] 29/04/2008 Messaggio nel caso in cui non venga creata la registrazione
                                                                              // recupero il motivo per il quale non è stata creata la registrazione
                                                                              //----------------------------------------------------------------------
                                                                              ls_msg[1] = lnv_reg_pd.uf_get_info()
                                                                              //n_err_mgr.nf_accoda(55523, ls_msg)                   
                                                               end if
                                                               
                                               end if
                                               
                                               if isvalid(lnv_reg_pd) then
                                                               destroy lnv_reg_pd
                                               end if
                                               
                                               if isvalid(lds_ds) then
                                                               destroy lds_ds
                                               end if
                                               
                               end if
                                               
                end if
                
end if

return ll_id_reg_pd

end function

on u_reg_pd_base.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_reg_pd_base.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/04/98  [ANT]    Chiama la funzione per l'azzeramento delle variabili di istanza.
//   02  27/01/99  [ANT]    Aggiunta la creazione dell'oggetto per conversioni tra valute.
//
// *****************************************************************************************

//uf_reset()
//in_currency_sync = create n_currency_sync
//in_currency_sync.uf_set_transaction(it_tran)

//iu_reg_pd_funct = create u_reg_pd_funct
//iu_reg_pd_funct.uf_set_transaction(it_tran)

//select dba.par_sistema.ges_par_aperte_new,ges_ritenute_new
//   into :is_ges_par_aperte_new, :is_ges_ritenute_new
//   from dba.par_sistema
//   where dba.par_sistema.id_azienda = 1 
//   using it_tran;
//   
//select ges_cc_nuova
//   into :is_ges_cc_nuova
//   from dba.par_sistema
//   using it_tran;   

end event

event destructor;// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  27/01/99  [ANT]    Distruzione dell'oggetto per conversioni tra valute.
//
// *****************************************************************************************

IF IsValid(in_currency_sync) THEN
	Destroy in_currency_sync
END IF

IF IsValid(iu_reg_pd_funct) THEN
	Destroy iu_reg_pd_funct
END IF
end event

