﻿$PBExportHeader$u_reg_pd_funct.sru
$PBExportComments$Funzioni per i default per la creazione di registrazioni contabili.
forward
global type u_reg_pd_funct from nonvisualobject
end type
end forward

global type u_reg_pd_funct from nonvisualobject
end type
global u_reg_pd_funct u_reg_pd_funct

type variables
n_tran      it_tran
end variables

forward prototypes
public function string uf_proponi_descr_reg_pd (long al_id_prof_registrazione)
public function string uf_proponi_num_documento (long al_id_registro)
public function date uf_monthrelativedate (date ad_date, long al_months_after)
public function datetime uf_get_esercizio_data_fine (long al_id_esercizio)
public function long uf_proponi_valuta (long al_id_conto, long al_id_prof_registrazione)
public function long uf_proponi_registro (long al_id_prof_registrazione, long al_id_esercizio)
public function date uf_proponi_data_reg_pd (long al_id_registro)
public function date uf_proponi_data_documento (long al_id_registro)
public function long uf_proponi_conto (long al_id_prof_registrazione, ref string as_conto_codice, ref string as_conto_descrizione)
public function string uf_proponi_simbolo (long al_id_valuta)
public function long uf_proponi_cifra_arrotondamento (long al_id_valuta)
public function character uf_proponi_tipo_fattura (long al_id_prof_registrazione)
public function long uf_get_mesi_post_liquidazione (long al_id_registro)
public function long uf_get_mesi_post_reg_iva (long al_id_registro)
public function date uf_get_ult_data_stampa_bollato (long al_id_esercizio)
public function date uf_get_ult_data_stampa_liquidazione (long al_id_attivita)
public function boolean uf_is_registro_cee (long al_id_registro)
public function long uf_proponi_num_reg_pd (long al_id_registro)
public subroutine uf_set_transaction (transaction af_tran)
public function long uf_esiste_reg_pd (long al_id_registro, long al_num_progressivo, date ad_data_registrazione)
public function decimal uf_proponi_cambio (long al_id_valuta)
end prototypes

public function string uf_proponi_descr_reg_pd (long al_id_prof_registrazione);// ***********************************************************************************
//
//			Autore: [ANT]
//			  Data: 28/09/97
//			 Scopo: Accetta un ID profilo registrazione e restituisce la sua descrizione
//					  contabile
//  Chiamata Da: dw_reg_pd_pn_ff di w_reg_pd_cx
//					  dw_pag_riscossioni_ff di w_pag_riscossioni_wz
//    Argomenti: long al_id_prof_registrazione
//       RetVal: string
//
// ***********************************************************************************

string ls_Descrizione_Profilo

IF NOT (IsNull(al_id_prof_registrazione) OR al_id_prof_registrazione = 0) THEN
   SELECT dba.prof_registrazione.des_contabile
		INTO :ls_Descrizione_Profilo
      FROM dba.prof_registrazione
      WHERE dba.prof_registrazione.id_prof_registrazione = :al_id_prof_registrazione
      USING it_tran ;

	RETURN ls_Descrizione_Profilo
END IF

RETURN ""
end function

public function string uf_proponi_num_documento (long al_id_registro);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  24/11/97  [ANT]    Restituisce il numero documento dell'ultima registrazione
//                          (incrementato di uno), del registro passato come argomento.
//   02  02/12/97  [ANT]    Modificata la funzione in modo da restituire il massimo numero
//                          documento incrementato di uno, scelto tra il massimo numero
//                          documento delle registrazioni e dei documenti con il registro
//                          passato.
//
// *****************************************************************************************

long ll_Ultimo_Num_Doc, ll_RetVal
decimal ldc_RetVal, ldc_Max_Reg_PD, ldc_Max_Documento
string ls_Max_Reg_PD, ls_Max_Documento, ls_abs_max

SELECT MAX(CONVERT(Numeric, dba.reg_pd.doc_globale))
	INTO :ldc_Max_Reg_PD
	FROM dba.reg_pd
	WHERE dba.reg_pd.id_registro = :al_id_registro AND
         ( dba.reg_pd.id_reg_pd_padre IS NULL )
   USING it_tran;

SELECT MAX(CONVERT(Numeric, dba.documento.doc_globale))
	INTO :ldc_Max_Documento
	FROM dba.documento
   WHERE dba.documento.id_registro = :al_id_registro
   USING it_tran;

ls_Max_Reg_PD = String(ldc_Max_Reg_PD )
IF IsNull(ls_Max_Reg_PD) THEN ls_Max_Reg_PD = ""

ls_Max_Documento = string(ldc_Max_Documento)
IF IsNull(ls_Max_Documento) THEN ls_Max_Documento = ""

IF ls_Max_Reg_PD >= ls_Max_Documento THEN
	ls_abs_max = ls_Max_Reg_PD
ELSEIF ls_Max_Documento > ls_Max_Reg_PD THEN
	ls_abs_max = ls_Max_Documento
END IF

ldc_RetVal = dec(right(ls_abs_max, 12))
IF IsNull(ldc_RetVal) THEN ldc_RetVal = 0
ll_RetVal = long(ldc_RetVal) + 1

RETURN String(ll_RetVal)
end function

public function date uf_monthrelativedate (date ad_date, long al_months_after);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  05/08/99  [ANT]    Accetta una data e restituisce la stessa aumentata di al_months_after
//                          mesi.
//
// *****************************************************************************************

integer li_day, li_month, li_year, li_new_day,  li_new_month, li_new_year
date ld_retval

IF (al_months_after > 0) AND (al_months_after < 13) AND NOT IsNull(ad_date) THEN
	li_day   = Day(ad_date)
	li_month = Month(ad_date)
	li_year  = Year(ad_date)

	/* Calcolo del nuovo mese. */
	li_new_day   = li_day
	li_new_month = li_month + al_months_after
	li_new_year  = li_year
	IF li_new_month > 12 THEN
		li_new_month = li_new_month - 12
		li_new_year = li_year + 1
	END IF

	/* Calcolo e controllo della validità della nuova data. */
//	IF NOT IsDate(String(li_new_year) + "/" + String(li_new_month) + "/" + String(li_new_day)) THEN
//		li_new_day --
		DO WHILE NOT IsDate(String(li_new_year) + "/" + String(li_new_month) + "/" + String(li_new_day))
			li_new_day --
		LOOP
		ld_retval = Date(String(li_new_year) + "/" + String(li_new_month) + "/" + String(li_new_day))
//	ELSE
//		ld_retval = Date(String(li_new_year) + "/" + String(li_new_month) + "/" + String(li_new_day))
//	END IF
ELSE
	ld_retval = ad_date
END IF

RETURN ld_retval
end function

public function datetime uf_get_esercizio_data_fine (long al_id_esercizio);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  22/05/00  [ANT]    Accetta un id_esercizio e restituisce la sua data fine.
//
// *****************************************************************************************

datetime ldt_retval

IF al_id_esercizio > 0 THEN
	SELECT
		dba.esercizio.data_fine
	INTO
		:ldt_retval
	FROM
		dba.esercizio
	WHERE
		dba.esercizio.id_esercizio = :al_id_esercizio
	USING
		it_tran;
		
	CHOOSE CASE it_tran.SQLCode
		CASE 0
		CASE ELSE
			SetNull(ldt_retval)
	END CHOOSE

END IF

RETURN ldt_retval
end function

public function long uf_proponi_valuta (long al_id_conto, long al_id_prof_registrazione);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  24/11/97  [ANT]    Restituisce l'ID della valuta del soggetto commerciale. Se
//                          questo non ha valuta, viene proposta quella del profilo. Sel il
//                          profilo Registrazione non ha valuta, restituisce quella
//                          memorizzata nei parametri di sistema.
//   02  27/11/00  [ANT]    Sostituita la valuta dei parametri di sistema con la valuta dell'
//                          esercizio proposto dell'azienda.
//
// *****************************************************************************************

long ll_id_valuta
Integer LI_AZIENDA_FITTIZIA
LI_AZIENDA_FITTIZIA = 1

SELECT dba.anagrafica.id_valuta
	INTO :ll_id_valuta
   FROM dba.sog_commerciale,
        dba.anagrafica,
        dba.conto
   WHERE ( dba.anagrafica.id_anagrafica = dba.sog_commerciale.id_anagrafica ) and
         ( dba.conto.id_conto = dba.sog_commerciale.id_conto ) and
         ( ( dba.conto.id_conto = :al_id_conto ) )
   USING it_tran ;

IF IsNull(ll_id_valuta) OR ll_id_valuta = 0 THEN
   SELECT dba.prof_registrazione.id_valuta
		INTO :ll_id_valuta
      FROM dba.prof_registrazione
      WHERE ( dba.prof_registrazione.id_prof_registrazione = :al_id_prof_registrazione )
      USING it_tran;
END IF

IF IsNull(ll_id_valuta) OR ll_id_valuta = 0 THEN
   SELECT 
		dba.valuta.id_valuta
	INTO 
		:ll_id_valuta
	FROM 
		dba.azienda
		JOIN dba.esercizio
			ON dba.azienda.id_esercizio_proposto = dba.esercizio.id_esercizio,
		dba.valuta
	WHERE
		dba.valuta.liraeuro = dba.esercizio.valuta_base
   USING 
		it_tran;
END IF

RETURN ll_id_valuta
end function

public function long uf_proponi_registro (long al_id_prof_registrazione, long al_id_esercizio);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  26/11/97  [ANT]    Seconda versione della funzione f_proponi_registro: il registro
//                          viene proposto in base al Mod_Registro e all'Esercizio.
//
// *****************************************************************************************

long ll_id_registro, ll_id_esercizio, LL_ID_AZIENDA
LL_ID_AZIENDA = 1
ll_id_esercizio = al_id_esercizio

IF NOT (IsNull(al_id_prof_registrazione) OR (al_id_prof_registrazione = 0)) THEN

	/* Se l'esercizio passato è zero o nullo, considera quello predefinito dell'azienda. */
	IF IsNull(al_id_esercizio) OR (al_id_esercizio = 0) THEN
		  SELECT dba.azienda.id_esercizio_proposto
			 INTO :ll_id_esercizio
			 FROM dba.azienda
         WHERE dba.azienda.id_azienda = :LL_ID_AZIENDA
         USING it_tran ;

		CHOOSE CASE it_tran.SQLCode
			CASE -1
            g_mb.messagebox("Errore sul DataBase", "u_reg_pd_funct / uf_proponi_registro / 2")
            //n_err_mgr.nf_mess_immediato(55547)
		END CHOOSE

	END IF

	  SELECT dba.registro.id_registro
		 INTO :ll_id_registro
		 FROM dba.esercizio,
				dba.mod_registro,
				dba.prof_registrazione,
				dba.registro
		WHERE ( dba.prof_registrazione.id_mod_registro       = dba.mod_registro.id_mod_registro ) and
				( dba.registro.id_mod_registro                 = dba.mod_registro.id_mod_registro ) and
				( dba.registro.id_esercizio                    = dba.esercizio.id_esercizio ) and
				( dba.prof_registrazione.id_prof_registrazione = :al_id_prof_registrazione ) AND
            ( dba.esercizio.id_esercizio                   = :ll_id_esercizio )
      USING it_tran;

	CHOOSE CASE it_tran.SQLCode
		CASE -1
         g_mb.messagebox("Errore sul DataBase", "u_reg_pd_funct / uf_proponi_registro / 1")
         //n_err_mgr.nf_mess_immediato(55548)
	END CHOOSE

END IF

RETURN ll_id_registro
end function

public function date uf_proponi_data_reg_pd (long al_id_registro);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  12/11/97  [ANT]    Accetta l'ID di un registro e restituisce la data della
//                          registrazione più recente.
//   02  03/12/97  [ANT]    Se la data non cade nell'esercizio corrente, propone il primo
//                          giorno dell'esercizio corrente.
//   03  29/09/99  [ANT]    Aggiunto il controllo sulle date dell'ultima data stampa bollato
//                          e ultima data stampa liquidazione.
//
// *****************************************************************************************

long LL_ID_AZIENDA, ll_id_esercizio, ll_NULL
datetime ld_Ultima_Data_Reg, ld_data_inizio, ld_data_fine, ld_Ultima_Data_Reg_doc
string ls_tipo_registro
date ld_oggi, ld_ult_data_stampa_bollato, ld_ult_data_stampa_liquidazione

LL_ID_AZIENDA = 1
ld_oggi = Today()
SetNull(ll_NULL)

IF NOT (IsNull(al_id_registro) OR al_id_registro = 0) THEN
	SELECT MAX(dba.reg_pd.data_registrazione)
		INTO :ld_Ultima_Data_Reg
		FROM dba.reg_pd
      WHERE dba.reg_pd.id_registro = :al_id_registro
      USING it_tran;

	CHOOSE CASE it_tran.SQLCode
		CASE -1
         g_mb.messagebox("Errore sul DataBase", "u_reg_pd_funct / uf_proponi_data_reg_pd / 1")
         //n_err_mgr.nf_mess_immediato(55536)
	END CHOOSE

  SELECT dba.esercizio.id_esercizio,
  			dba.esercizio.data_inizio,
         dba.esercizio.data_fine,
			dba.registro.tipo_registro
    INTO :ll_id_esercizio,
	 		:ld_data_inizio,
         :ld_data_fine,
			:ls_tipo_registro
    FROM dba.esercizio,
         dba.registro
   WHERE ( dba.registro.id_esercizio = dba.esercizio.id_esercizio ) and
         ( ( dba.registro.id_registro = :al_id_registro ) )
   USING it_tran ;


	CHOOSE CASE it_tran.SQLCode
		CASE 0
			/* Se la data calcolata non cade nell'esercizio corrente, ritorna la data di inizio
			di quest'ultimo. */
			//(NOT ((ld_oggi >= Date(ld_data_inizio)) AND (ld_oggi <= Date(ld_data_fine)))) OR &
         IF (NOT ((Date(ld_Ultima_Data_Reg) >= Date(ld_data_inizio)) AND (Date(ld_Ultima_Data_Reg) <= Date(ld_data_fine)))) OR &
				IsNull(ld_Ultima_Data_Reg) THEN
				ld_Ultima_Data_Reg = ld_data_inizio
			END IF
		CASE -1
         g_mb.messagebox("Errore sul DataBase", "u_reg_pd_funct / uf_proponi_data_reg_pd / 2")
         //n_err_mgr.nf_mess_immediato(55537)
	END CHOOSE
	
   /* Controllo se la data proposta è maggiore o uguale rispetto all'ultima data stampa
   bollato. */
   ld_ult_data_stampa_bollato = uf_get_ult_data_stampa_bollato(ll_id_esercizio)
   IF Date(ld_Ultima_Data_Reg) < ld_ult_data_stampa_bollato THEN
      ld_Ultima_Data_Reg = DateTime(ld_ult_data_stampa_bollato)
   END IF

   IF (ls_tipo_registro <> "PN") AND (ls_tipo_registro <> "MN") THEN
		/* In caso di registrazione IVA, considero anche l'ultima data documento. */
		SELECT MAX(dba.documento.data_registrazione)
			INTO :ld_Ultima_Data_Reg_doc
			FROM dba.documento
			WHERE dba.documento.id_registro = :al_id_registro
			USING it_tran;
	
		CHOOSE CASE it_tran.SQLCode
			CASE -1
				g_mb.messagebox("Errore sul DataBase", "u_reg_pd_funct / uf_proponi_data_reg_pd / 1")
				//n_err_mgr.nf_mess_immediato(55536)
		END CHOOSE
		
		IF ld_Ultima_Data_Reg_doc > ld_Ultima_Data_Reg THEN
			ld_Ultima_Data_Reg = ld_Ultima_Data_Reg_doc
		END IF
		
		/* In caso di registrazione IVA, controllo se la data proposta è maggiore rispetto
		all'ultima data stampa liquidazione. */
      ld_ult_data_stampa_liquidazione = uf_get_ult_data_stampa_liquidazione(ll_NULL)
      IF NOT IsNull(ld_ult_data_stampa_liquidazione) THEN
         IF Date(ld_Ultima_Data_Reg) <= ld_ult_data_stampa_liquidazione THEN
            ld_Ultima_Data_Reg = DateTime(RelativeDate(ld_ult_data_stampa_liquidazione, 1))
         END IF
      END IF
   END IF

END IF

IF IsNull(ld_Ultima_Data_Reg) THEN ld_Ultima_Data_Reg = DateTime(ld_oggi)
RETURN Date(ld_Ultima_Data_Reg)
end function

public function date uf_proponi_data_documento (long al_id_registro);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  24/11/97  [ANT]    Restituisce la data documento più recente tra le registrazioni
//                          con il registro passato.
//
// *****************************************************************************************

datetime ld_Ultima_Data_Doc

SELECT MAX(dba.reg_pd.data_documento)
	INTO :ld_Ultima_Data_Doc
	FROM dba.reg_pd
	WHERE dba.reg_pd.id_registro = :al_id_registro;

CHOOSE CASE it_tran.SQLCode
	CASE -1
      g_mb.messagebox("Errore sul DataBase", "u_reg_pd_funct / uf_proponi_data_documento / 1")
      //n_err_mgr.nf_mess_immediato(55539)
		RETURN Date("")
END CHOOSE

IF IsNull(ld_Ultima_Data_Doc) THEN ld_Ultima_Data_Doc = DateTime(Today())
RETURN Date(ld_Ultima_Data_Doc)
end function

public function long uf_proponi_conto (long al_id_prof_registrazione, ref string as_conto_codice, ref string as_conto_descrizione);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  24/11/97  [ANT]    Restituisce l'ID del conto della prima riga dei prof_movimento
//                          del profilo passato in al_id_prof_registrazione. Inoltre, nei
//                          due argomenti passati by reference as_conto_codice e as_conto_descrizione,
//                          memorizza il codice e la descrizione del conto.
//
// *****************************************************************************************

long ll_IDConto_Prof_Mov_Contab, ll_RetVal
string ls_filtro_conto

SELECT dba.prof_mov_contabile.id_conto,
       dba.prof_mov_contabile.filtro_conto
   INTO :ll_IDConto_Prof_Mov_Contab,
        :ls_filtro_conto
	FROM dba.prof_mov_contabile
	WHERE dba.prof_mov_contabile.id_prof_registrazione = :al_id_prof_registrazione AND
         dba.prof_mov_contabile.num_riga = 1
   USING it_tran;

CHOOSE CASE it_tran.SQLCode
	CASE 0
		ll_RetVal = ll_IDConto_Prof_Mov_Contab
//		l_status = GetItemStatus(ll_CurrRow, 0, Primary!)
//		IF l_status = New! OR l_status = NewModified! THEN
//			// ... propone il conto della prima riga dei profili movimenti del profilo
//			// selezionato ...
//			THIS.SetItem(ll_CurrRow, id_conto, ll_IDConto_Prof_Mov_Contab)

			SELECT dba.conto.codice, dba.conto.descrizione
				INTO :as_conto_codice, :as_conto_descrizione
				FROM dba.conto
            WHERE dba.conto.id_conto = :ll_IDConto_Prof_Mov_Contab
            USING it_tran;

			CHOOSE CASE it_tran.SQLCode
				CASE -1
               g_mb.messagebox("Errore sul Database", "u_reg_pd_funct / uf_proponi_conto / 2")
               //n_err_mgr.nf_mess_immediato(55540)
					ll_RetVal = 0
			END CHOOSE

//		END IF
	CASE -1
      g_mb.messagebox("Errore sul Database", "u_reg_pd_funct / uf_proponi_conto / 1")
      //n_err_mgr.nf_mess_immediato(55541)
		ll_RetVal = 0
END CHOOSE

RETURN ll_RetVal
end function

public function string uf_proponi_simbolo (long al_id_valuta);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  24/11/97  [ANT]    Restituisce il simbolo della valuta il cui ID è in al_id_valuta.
//
// *****************************************************************************************

string ls_simbolo = ""

SELECT dba.valuta.simbolo
	INTO :ls_simbolo
	FROM dba.valuta
   WHERE (dba.valuta.id_valuta = :al_id_valuta)
   USING it_tran;

CHOOSE CASE it_tran.SQLCode
	CASE -1
      g_mb.messagebox("Errore Sul DataBase", "u_reg_pd_funct / uf_proponi_simbolo / 1")
      //n_err_mgr.nf_mess_immediato(55542)
		ls_simbolo = ""
END CHOOSE

RETURN ls_simbolo

end function

public function long uf_proponi_cifra_arrotondamento (long al_id_valuta);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  15/12/98  [ANT]    Restituisce il campo cifra_arrotondamento della valuta il cui
//                          ID è in al_id_valuta.
//
// *****************************************************************************************

long ll_cifra_arrotondamento

SELECT dba.valuta.cifra_arrotondamento
	INTO :ll_cifra_arrotondamento
	FROM dba.valuta
   WHERE (dba.valuta.id_valuta = :al_id_valuta)
   USING it_tran;

CHOOSE CASE it_tran.SQLCode
	CASE 100
		ll_cifra_arrotondamento = 0
	CASE -1
      g_mb.messagebox("Errore Sul DataBase", "u_reg_pd_funct / uf_proponi_cifra_arrotondamento / 1")
      //n_err_mgr.nf_mess_immediato(55543)
		ll_cifra_arrotondamento = 0
END CHOOSE

RETURN ll_cifra_arrotondamento

end function

public function character uf_proponi_tipo_fattura (long al_id_prof_registrazione);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  01/07/99  [ANT]    Restituisce il tipo_fattura del profilo passato come argomento.
//
// *****************************************************************************************

string ls_args[]
char lc_retval = ""

IF NOT IsNull(al_id_prof_registrazione) AND (al_id_prof_registrazione > 0) THEN
   SELECT dba.prof_registrazione.tipo_fattura
		INTO :lc_retval
		FROM dba.prof_registrazione
		WHERE dba.prof_registrazione.id_prof_registrazione = :al_id_prof_registrazione
		USING it_tran;

	CHOOSE CASE it_tran.SQLCode
		CASE -1
         g_mb.messagebox("Errore sul DataBase", it_tran.SQLErrText)
         ls_args[1] = it_tran.SQLErrText
         //n_err_mgr.nf_mess_immediato(55544, ls_args)
	END CHOOSE

END IF

RETURN lc_retval
end function

public function long uf_get_mesi_post_liquidazione (long al_id_registro);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  05/08/99  [ANT]    Accetta un id_registro e restituisce il campo mesi_post_liquidazione.
//
// *****************************************************************************************

long ll_retval = 0

IF NOT IsNull(al_id_registro) THEN
   SELECT dba.registro.mesi_post_liquidazione
		INTO :ll_retval
		FROM dba.registro
		WHERE id_registro = :al_id_registro
		USING it_tran;

	CHOOSE CASE it_tran.SQLCode
		CASE -1
         g_mb.messagebox("Errore sul DataBase", "u_reg_pd_funct / uf_get_mesi_post_liquidazione / 1")
         //n_err_mgr.nf_mess_immediato(55545)
	END CHOOSE

	IF IsNull(ll_retval) THEN ll_retval = 0
END IF

RETURN ll_retval
end function

public function long uf_get_mesi_post_reg_iva (long al_id_registro);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  05/08/99  [ANT]    Accetta un id_registro e restituisce il campo mesi_post_reg_iva.
//
// *****************************************************************************************

long ll_retval = 0

IF NOT IsNull(al_id_registro) THEN
   SELECT dba.registro.mesi_post_reg_iva
		INTO :ll_retval
		FROM dba.registro
		WHERE id_registro = :al_id_registro
		USING it_tran;

	CHOOSE CASE it_tran.SQLCode
		CASE -1
         g_mb.messagebox("Errore sul DataBase", "u_reg_pd_funct / uf_get_mesi_post_reg_iva / 1")
         //n_err_mgr.nf_mess_immediato(55546)
	END CHOOSE

	IF IsNull(ll_retval) THEN ll_retval = 0
END IF

RETURN ll_retval
end function

public function date uf_get_ult_data_stampa_bollato (long al_id_esercizio);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  29/09/99  [ANT]    Restituisce il valore del campo esercizio.ult_data_stampa_bollato.
//
// *****************************************************************************************

datetime ld_retval

SELECT dba.esercizio.ult_data_stampa_bollato
   INTO :ld_retval
   FROM dba.esercizio
   WHERE dba.esercizio.id_esercizio = :al_id_esercizio
   USING it_tran;

CHOOSE CASE it_tran.SQLCode
   CASE 0
   CASE -1
      g_mb.messagebox("Registrazioni Contabili", "u_reg_pd_bob / nf_get_ult_data_stampa_bollato / Errore 55551.")
		//n_err_mgr.nf_mess_immediato(55551)
      SetNull(ld_retval)
END CHOOSE

RETURN Date(ld_retval)
end function

public function date uf_get_ult_data_stampa_liquidazione (long al_id_attivita);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  29/09/99  [ANT]    Restituisce l'ultima data stampa liquidazione.
//
// *****************************************************************************************

datetime ld_retval

IF IsNull(al_id_attivita) THEN
	SELECT dba.azienda.id_attivita
		INTO :al_id_attivita
		FROM dba.azienda
		WHERE dba.azienda.id_azienda = 1
		USING it_tran;
		
	CHOOSE CASE it_tran.SQLCode
		CASE 0
			IF IsNull(al_id_attivita) THEN
				g_mb.messagebox("Registrazioni Contabili", "u_reg_pd_funct / uf_get_ult_data_stampa_liquidazione:~rAttenzione: non è stata impostata l'attività predefinita~rper l'avienda corrente.")
				//n_err_mgr.nf_mess_immediato(55552)
			END IF
		CASE -1
			g_mb.messagebox("Registrazioni Contabili", "u_reg_pd_funct / uf_get_ult_data_stampa_liquidazione / Errore 55553.")
			//n_err_mgr.nf_mess_immediato(55553)
	END CHOOSE

END IF

IF NOT IsNull(al_id_attivita) THEN
	SELECT max(dba.riep_iva.ult_data_bollato)
		INTO :ld_retval
		FROM dba.riep_iva
		WHERE dba.riep_iva.id_attivita = :al_id_attivita
		USING it_tran;

END IF

RETURN Date(ld_retval)
end function

public function boolean uf_is_registro_cee (long al_id_registro);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  10/04/00  [ANT]    Accetta un id_registro e restituisce TRUE se questo ha il campo
//                          usa_ivacee = "S", altrimenti FALSE.
//
// *****************************************************************************************

boolean lb_retval = FALSE
char lc_usa_ivacee
string ls_args[]

SELECT dba.registro.usa_ivacee
	INTO :lc_usa_ivacee
	FROM dba.registro
	WHERE dba.registro.id_registro = :al_id_registro
	USING it_tran;
	
CHOOSE CASE it_tran.SQLCode
	CASE 0
		CHOOSE CASE lc_usa_ivacee
			CASE "S"
				lb_retval = TRUE
			CASE ELSE
				lb_retval = FALSE
		END CHOOSE
	CASE -1
		ls_args[1] = "u_reg_pd_funct / uf_is_registro_cee.~r~n" + it_tran.SQLErrText
		//n_err_mgr.nf_mess_immediato(55504, ls_args)
		g_mb.messagebox("u_regpd_funct", ls_args[1])
END CHOOSE

RETURN lb_retval 
end function

public function long uf_proponi_num_reg_pd (long al_id_registro);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  28/09/97  [ANT]    Accetta l'ID di un registro e restituisce l'ultimo numero
//                          registrazione + 1 con il registro passato.
//   02  01/12/97  [ANT]    - In caso di registro prima nota o manageriale, propone l'ultimo
//                            numero registrazione prima nota o manageriale incrementato di uno.
//                          - In caso di registro IVA propone l'ultimo numero registrazione
//                            (incrementato di uno) tra registrazioni contabili IVA e Documento.
//                          - In caso di registro di tipo DDT Acquisti o DDT Vendite propone
//                            l'ultimo progressivo incrementato di uno.
//
// *****************************************************************************************

long ll_Ultimo_Num_Reg, ll_RetVal
decimal ldc_Max_Reg_PD, ldc_Max_Documento, ldc_abs_max, ldc_RetVal
string ls_Max_Reg_PD, ls_Max_Documento, ls_abs_max
string ls_tipo_registro
SetNull(ll_RetVal)

IF NOT (IsNull(al_id_registro) OR al_id_registro = 0) THEN
	SELECT dba.registro.tipo_registro
		INTO :ls_tipo_registro
		FROM dba.registro
      WHERE dba.registro.id_registro = :al_id_registro
      USING it_tran ;

	CHOOSE CASE it_tran.SQLCode
		CASE 0

			CHOOSE CASE ls_tipo_registro
				CASE "PN", "MN", "PE", "PZ"
					SELECT MAX(dba.reg_pd.num_progressivo)
						INTO :ldc_Max_Reg_PD
						FROM dba.reg_pd
                  WHERE dba.reg_pd.id_registro = :al_id_registro
                  USING it_tran ;

					IF IsNull(ldc_Max_Reg_PD) THEN ldc_Max_Reg_PD = 0
					ll_RetVal = (ldc_Max_Reg_PD + 1)
				CASE "IV", "IA", "IC"
					SELECT MAX(dba.reg_pd.prog_globale)
						INTO :ldc_Max_Reg_PD
						FROM dba.reg_pd
                  WHERE dba.reg_pd.id_registro = :al_id_registro
                  USING it_tran;

					SELECT MAX(dba.documento.prog_globale)
						INTO :ldc_Max_Documento
						FROM dba.documento
                  WHERE dba.documento.id_registro = :al_id_registro
                  USING it_tran;

					ls_Max_Reg_PD = String(ldc_Max_Reg_PD )
					IF IsNull(ls_Max_Reg_PD) THEN ls_Max_Reg_PD = ""

					ls_Max_Documento = string(ldc_Max_Documento)
					IF IsNull(ls_Max_Documento) THEN ls_Max_Documento = ""

					IF ls_Max_Reg_PD >= ls_Max_Documento THEN
						ls_abs_max = ls_Max_Reg_PD
					ELSEIF ls_Max_Documento > ls_Max_Reg_PD THEN
						ls_abs_max = ls_Max_Documento
					END IF

					ldc_RetVal = dec(right(ls_abs_max, 12))
					IF IsNull(ldc_RetVal) THEN ldc_RetVal = 0
					ll_RetVal = long(ldc_RetVal) + 1
				CASE "DV"
					SELECT MAX(dba.documento.prog_globale)
						INTO :ldc_Max_Documento
						FROM dba.documento
                  WHERE dba.documento.id_registro = :al_id_registro
                  USING it_tran ;
					IF IsNull(ldc_Max_Documento) THEN ldc_Max_Documento = 0
					ll_RetVal = (ldc_Max_Documento + 1)
			END CHOOSE

		CASE -1
         g_mb.messagebox("Errore sul DataBase", "u_reg_pd_funct / uf_proponi_num_reg_pd / 1")
         //n_err_mgr.nf_mess_immediato(55535)
	END CHOOSE

END IF

RETURN ll_RetVal
end function

public subroutine uf_set_transaction (transaction af_tran);it_tran = af_tran
end subroutine

public function long uf_esiste_reg_pd (long al_id_registro, long al_num_progressivo, date ad_data_registrazione);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  13/05/99  [ANT]    Accetta un id_registro, un numero progressivo e una data registrazione
//                          e verifica se esiste una registrazione con tali valori. Se esiste
//                          restituisce il suo id, altrimenti 0.
//
// *****************************************************************************************

long ll_retval = 0
datetime ldt_data_registrazione

ldt_data_registrazione = DateTime(ad_data_registrazione)

SELECT dba.reg_pd.id_reg_pd
	INTO :ll_retval
	FROM dba.reg_pd
	WHERE (dba.reg_pd.id_registro        = :al_id_registro)        AND
			(dba.reg_pd.num_progressivo    = :al_num_progressivo)    AND
         (dba.reg_pd.data_registrazione = :ldt_data_registrazione)
	USING it_tran;

CHOOSE CASE it_tran.SQLCode
	CASE 0
	CASE ELSE
		ll_retval = 0
END CHOOSE

RETURN ll_retval
end function

public function decimal uf_proponi_cambio (long al_id_valuta);// *****************************************************************************************
//
//   N.  Data      Autore   Modifica
// -----------------------------------------------------------------------------------------
//   01  24/11/97  [ANT]    Restituisce il cambio della valuta il cui ID è in al_id_valuta.
//
// *****************************************************************************************

decimal ldc_retval, ldc_cambio, ldc_parita_euro, ldc_cambio_euro
string ls_liraeuro

SELECT dba.valuta.liraeuro,
		 dba.valuta.cambio,
		 dba.valuta.parita_euro,
		 dba.valuta.cambio_euro
	INTO :ls_liraeuro,
		  :ldc_cambio,
		  :ldc_parita_euro,
		  :ldc_cambio_euro
	FROM dba.valuta
	WHERE (dba.valuta.id_valuta = :al_id_valuta)
	USING it_tran;

CHOOSE CASE it_tran.SQLCode
	CASE 0
		CHOOSE CASE ls_liraeuro
			CASE "I"
				ldc_retval = ldc_cambio
			CASE "E"
				ldc_retval = ldc_cambio
			CASE "C"
				ldc_retval = ldc_parita_euro
			CASE "X"
				ldc_retval = ldc_cambio_euro
		END CHOOSE

	CASE 100
		ldc_retval = 1
	CASE -1
      g_mb.messagebox("Errore Sul DataBase", "u_reg_pd_funct / uf_proponi_cambio / 1")
      //n_err_mgr.nf_mess_immediato(55538)
		ldc_retval = 1
END CHOOSE

RETURN ldc_retval
end function

on u_reg_pd_funct.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_reg_pd_funct.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

