﻿$PBExportHeader$uo_wizard_base.sru
$PBExportComments$Oggetto per l'attivazione dei wizard della prima nota.
forward
global type uo_wizard_base from nonvisualobject
end type
end forward

global type uo_wizard_base from nonvisualobject
end type
global uo_wizard_base uo_wizard_base

type variables
datastore	ids_wiz_profilo
datastore	ids_wiz_profilo_mov_iva
datastore	ids_wiz_profilo_mov_contabile
datastore	ids_wizard_pagrit_ds

datastore	ids_reg_pd_ff
datastore	ids_mov_iva_gd
datastore	ids_mov_contabile_gd

string		is_stor_rit_previdenziale
string      is_ges_ritenute_new

boolean		ib_quiet_mode = FALSE // Indica se le generazioni automatiche devono essere fatte senza interazione con l'utente.
boolean		ib_autocommit = TRUE

u_reg_pd_base		iu_reg_pd
string	is_riferimenti = "Rif. Doc.:"
string is_ges_partite_new
end variables

forward prototypes
public function integer uf_attiva_ivacee (integer al_id_wizard, long al_id_reg_pd_padre)
end prototypes

public function integer uf_attiva_ivacee (integer al_id_wizard, long al_id_reg_pd_padre);// --------------------------------------------------------------------------------
// Oggetto           uo_wizard_base
// Evento/Funzione   uf_attiva_ivacee
// Argomenti         value integer al_id_wizard	 
// 						value long al_id_reg_pd_padre	 
//
// Ritorno           (none)
//
// Descrizione       Genera una registrazione IVA vendite (per storno IVA) partendo
//							da una registrazione IVA acquisti CEE.
// --------------------------------------------------------------------------------
//
// Revisioni
// 01 17/10/97 [ANT] Prima stesura.
//	02	20/11/97	[ANT]	Aggiunta MessageBox di notifica con gli estremi della registrazione
//							appena creata.
//	03	14/03/98	[ANT]	Trasferito e adattato l'evento da w_reg_pd_cx.
//	04	18/12/98	[ANT]	Sostituita la ricerca del registro da utilizzare per la registrazione
//							generata: viene calcolato in base al mod_registro del profilo
//							immesso come primo parametro del wizard IVACEE.
//	05	19/04/99	[ANT]	Commentata l'assegnazione del numero progressivo della registrazione
//							generata. In questo modo il progressivo verrà ricalcolato in base al
//							registro.
//	06	13/05/99	[ANT]	Ripristinata l'assegnazione del numero progressivo della registrazione
//							generata. Il numero viene riassegnato solo se esiste già una
//							registrazione con quel numero. Per verificare se la registrazione
//							esiste già viene utilizzata la funzione uf_esiste_reg_pd di u_reg_pd_funct.
//	07	03/01/00	[ANT]	Modificata l'impostazione dell'esercizio: viene considerato quello
//							della registrazione che ha attivto il wizard.
//	08	10/04/00	[ANT]	Modificato il controllo per l'assegnazione del numero progressivo
//							della registrazione generata: viene assegnato lo stesso numero della
//							registrazione di partenza solo se il registro ha il campo "usa_ivacee"
//							uguale a "S".
//	09	19/09/00	[ANT]	Modificata l'attribuzione della descrizione contabile della registrazione
//							generata: viene presa prima quella del profilo, e se questa è nulla
//							viene presa quella della registrazione di partenza.
//
// --------------------------------------------------------------------------------
// Copyright © 1996-2001 Office Software Solutions. Tutti i diritti riservati
// --------------------------------------------------------------------------------

// Variabili per i campi di Reg_PD
long ll_CurrRow_Reg_PD, ll_IDReg_PD, ll_IDProf_Registrazione, ll_id_registro, ll_num_progressivo
long ll_IDFornitore, ll_IDValuta, ll_IDAzienda, ll_id_esercizio, ll_id_mod_registro, ll_id_reg_pd
long ll_num_progressivo_esistente
string ls_descrizione, ls_descr_registro, ls_num_documento, ls_prof_registrazione_codice
string ls_tipo_fattura, ls_prof_ges_art74
date ld_data_registrazione, ld_data_competenza, ld_data_documento
decimal ldc_cambio, ldc_importo, ldc_importo_val
char lc_ges_oper_intracomunitarie
boolean	lb_tran
string ls_msg[]
// Variabili per i campi di Mov_IVA
decimal ldc_imponibile[], ldc_imposta[]
long ll_id_tab_iva[], ll_i, ll_rows, ll_a
string ls_des_contabile, ls_sql, ls_dw_syntax, ls_retval
String ls_ges_art74[]
datastore lds_mov_iva

// Altre variabili
long ll_IDReg_PD_generata_esistente, ll_retval
string ls_cod_profilo, ls_segno_1, ls_segno_9999, ls_args[]
decimal ldc_NULL, ldc_temp //, ldc_dare, ldc_avere
integer li_RetVal

// Variabili temporanee per cursore su mov_iva
decimal ldc_imponibile_temp, ldc_imposta_temp
long ll_id_tab_iva_temp


iu_reg_pd = CREATE u_reg_pd_base
iu_reg_pd.it_tran = sqlci
iu_reg_pd.uf_set_transaction(sqlci)

iu_reg_pd.uf_reset()

ls_msg[1] = ''
u_reg_pd_funct lu_reg_pd_funct
lu_reg_pd_funct = CREATE u_reg_pd_funct
lu_reg_pd_funct.uf_set_transaction( sqlci )

SetNull(ldc_NULL)

ids_wiz_profilo = create datastore
ids_wiz_profilo.dataobject = 'd_wiz_profilo_ds'
ids_wiz_profilo.settransobject(sqlci)

ll_rows = ids_wiz_profilo.retrieve(al_id_reg_pd_padre)

// se manca il wizard è un errore dato che è previsto il giroconto iva cee
if ll_rows <= 0 then return -1

ll_CurrRow_Reg_PD = ids_wiz_profilo.GetRow()
ll_IDReg_PD = ids_wiz_profilo.GetItemNumber(ll_CurrRow_Reg_PD, "reg_pd_id_reg_pd")

// Se esiste già una registrazione di giroconto IVA CEE, la cancello. Una tale registrazione
// esiste già quando sono in modifica di una registrazione per ricevuta fattura di acquisto
// CEE.
SELECT dba.reg_pd.id_reg_pd
	INTO :ll_IDReg_PD_generata_esistente
   FROM dba.reg_pd
   WHERE dba.reg_pd.id_reg_pd_padre = :ll_IDReg_PD
   USING sqlci;

CHOOSE CASE sqlci.SQLCode
	CASE 0
		SELECT dba.reg_pd.num_progressivo
			INTO :ll_num_progressivo_esistente
			FROM dba.reg_pd
			WHERE dba.reg_pd.id_reg_pd = :ll_IDReg_PD_generata_esistente
			USING sqlci;
			
		//sqlci.nf_BeginTran()
			sqlci.nf_elimina_reg_pd(ll_IDReg_PD_generata_esistente, ll_retval)		
		IF ll_retval = 0 THEN
			//sqlci.nf_Commit()
		ELSE
			//sqlci.nf_Rollback()
		END IF
	CASE -1
      //MessageBox("Errore sul DataBase", "uo_wizard_base / uf_attiva_ivacee / 5")
     // n_err_mgr.nf_mess_immediato(55915)
END CHOOSE

ll_IDReg_PD                  = ids_wiz_profilo.GetItemNumber(ll_CurrRow_Reg_PD, "reg_pd_id_reg_pd")
ll_id_registro               = ids_wiz_profilo.GetItemNumber(ll_CurrRow_Reg_PD, "reg_pd_id_registro")
ll_num_progressivo           = ids_wiz_profilo.GetItemNumber(ll_CurrRow_Reg_PD, "reg_pd_num_progressivo")
ld_data_competenza           = Date(ids_wiz_profilo.GetItemDateTime(ll_CurrRow_Reg_PD, "reg_pd_data_competenza"))
ll_IDProf_Registrazione      = ids_wiz_profilo.GetItemNumber(ll_CurrRow_Reg_PD, "reg_pd_id_prof_registrazione")
ld_data_registrazione        = Date(ids_wiz_profilo.GetItemDateTime(ll_CurrRow_Reg_PD, "reg_pd_data_registrazione"))
ll_IDValuta                  = ids_wiz_profilo.GetItemNumber(ll_CurrRow_Reg_PD, "reg_pd_id_valuta")
ldc_cambio                   = ids_wiz_profilo.GetItemDecimal(ll_CurrRow_Reg_PD, "reg_pd_cambio")
ldc_importo                  = ids_wiz_profilo.GetItemDecimal(ll_CurrRow_Reg_PD, "reg_pd_importo")
ldc_importo_val              = ids_wiz_profilo.GetItemDecimal(ll_CurrRow_Reg_PD, "reg_pd_imp_valuta")
ls_descrizione               = ids_wiz_profilo.GetItemString(ll_CurrRow_Reg_PD, "reg_pd_descrizione")
lc_ges_oper_intracomunitarie = ids_wiz_profilo.GetItemString(ll_CurrRow_Reg_PD, "reg_pd_ges_oper_intracomunitarie")
ls_num_documento             = ids_wiz_profilo.GetItemString(ll_CurrRow_Reg_PD, "reg_pd_num_documento")
ld_data_documento            = Date(ids_wiz_profilo.GetItemDateTime(ll_CurrRow_Reg_PD, "reg_pd_data_documento"))
ll_IDFornitore               = ids_wiz_profilo.GetItemNumber(ll_CurrRow_Reg_PD, "reg_pd_id_conto")
ll_IDAzienda                 = ids_wiz_profilo.GetItemNumber(ll_CurrRow_Reg_PD, "reg_pd_id_azienda")
ls_tipo_fattura              = ids_wiz_profilo.GetItemString(ll_CurrRow_Reg_PD, "reg_pd_tipo_fattura")
ll_id_esercizio 				  = ids_wiz_profilo.GetItemNumber(ll_CurrRow_Reg_PD, "registro_id_esercizio")

//------------------------------------------------------------------------------------------------------
// [GIU] 30/08/2006
// Verifico se il profilo di registrazione della registrazione ha il flag per la gestione di rottami ferrosi (articolo74)
//------------------------------------------------------------------------------------------------------
select dba.prof_registrazione.ges_articolo_74
	into :ls_prof_ges_art74 
	FROM dba.prof_registrazione
	where dba.prof_registrazione.id_prof_registrazione = :ll_idprof_registrazione
	using sqlci;


// Seleziono l'ID del profilo il cui codice è memorizzato nella prima riga dei par_wizard
// del wizard IVACEE.

SELECT dba.prof_registrazione.id_prof_registrazione,
		 dba.prof_registrazione.codice,
       dba.prof_registrazione.des_contabile,
		 dba.prof_registrazione.id_mod_registro
		 
	INTO :ll_IDProf_Registrazione,
		  :ls_prof_registrazione_codice,
		  :ls_des_contabile,
		  :ll_id_mod_registro
		 
	FROM dba.par_wizard,
		  dba.prof_registrazione
	WHERE (dba.par_wizard.valore = dba.prof_registrazione.codice) and
         (dba.par_wizard.id_wizard = :al_id_wizard)  and
         (dba.par_wizard.num_ordine = 1)
	USING sqlci;

IF IsNull(ls_des_contabile) OR (ls_des_contabile = "") THEN
	ls_des_contabile = ls_descrizione
END IF

/* Selezione del registro. */
SetNull(ll_id_registro)
SELECT dba.registro.id_registro,
		 dba.registro.descrizione
	INTO :ll_id_registro,
		  :ls_descr_registro
	FROM dba.registro
	WHERE (dba.registro.id_mod_registro = :ll_id_mod_registro) AND
			(dba.registro.id_esercizio = :ll_id_esercizio)
	USING sqlci;

IF IsNull(ll_id_registro) THEN
   //MessageBox("Generazioni Automatiche", "Attenzione: non esiste un registro valido per il modello registro impostato~rnel profilo " + ls_prof_registrazione_codice + ". Impossibile generare la registrazione.", StopSign!)
   ls_args[1] = ls_prof_registrazione_codice
   //n_err_mgr.nf_mess_immediato(55917, ls_args)
	RETURN -1
END IF

// ******************************************************************************
//
//  Copia dei movimenti IVA
//
// ******************************************************************************
//ls_sql = "SELECT dba.mov_iva.imponibile, " + &
// 			"		 dba.mov_iva.id_tab_iva, " + &
// 			"		 dba.mov_iva.imposta, " + &
// 			"		 dba.mov_iva.id_azienda " + &
//    		"FROM dba.mov_iva " + &
//   		"WHERE dba.mov_iva.id_reg_pd = " + String(ll_IDReg_PD)

//lds_mov_iva = Create datastore
//ls_dw_syntax = sqlci.SyntaxFromSQL(ls_sql, "style(type=grid)", ls_retval)
//lds_mov_iva.Create(ls_dw_syntax)
//lds_mov_iva.SetTransObject(sqlci)

//-----------------------------------------------------
// [GIU] 28/08/2006
// Creato un ds con un dataobject fisico  anzichè in sintassi
//-----------------------------------------------------

lds_mov_iva = create datastore
lds_mov_iva.DataObject   = "d_wizard_mov_iva_ds"
lds_mov_iva.SetTransObject(sqlci)
ll_rows = lds_mov_iva.Retrieve(ll_idreg_pd)


//IF lds_mov_iva.retrieve() > 0 THEN
//	FOR ll_i = 1 TO lds_mov_iva.RowCount()
//		ldc_imponibile[ll_i] = lds_mov_iva.GetItemDecimal(ll_i, 1)
//		ll_id_tab_iva[ll_i]  = lds_mov_iva.GetItemNumber(ll_i, 2)
//		ldc_imposta[ll_i]    = lds_mov_iva.GetItemDecimal(ll_i, 3)
//	
//	NEXT
//END IF

IF ll_rows > 0 THEN
	ll_a = 0
	FOR ll_i = 1 TO ll_rows
		ls_ges_art74[ll_i] 	= lds_mov_iva.GetItemString( ll_i, "ges_articolo_74" )
		if ls_prof_ges_art74 = 'S' then
			if ls_ges_art74[ll_i] = 'S' then
				ll_a ++
				ldc_imponibile[ll_a] = lds_mov_iva.GetItemDecimal( ll_i, "mov_iva_imponibile" )
				ll_id_tab_iva[ll_a]	= lds_mov_iva.GetItemNumber( ll_i, "mov_iva_id_tab_iva" )
				ldc_imposta[ll_a] 	= lds_mov_iva.GetItemDecimal( ll_i, "mov_iva_imposta" )
			end if
		else			
			ldc_imponibile[ll_i] = lds_mov_iva.GetItemDecimal( ll_i, "mov_iva_imponibile" )
			ll_id_tab_iva[ll_i]	= lds_mov_iva.GetItemNumber( ll_i, "mov_iva_id_tab_iva" )
			ldc_imposta[ll_i] 	= lds_mov_iva.GetItemDecimal( ll_i, "mov_iva_imposta" )
		end if
	NEXT
END IF

IF IsValid(lds_mov_iva) THEN Destroy lds_mov_iva

/* Testata. */
if ib_quiet_mode = true then
	iu_reg_pd.uf_DisplayMode(0)
else
	iu_reg_pd.uf_DisplayMode(2)
end if
iu_reg_pd.uf_autocommit(ib_autocommit)

iu_reg_pd.uf_Reset()
iu_reg_pd.uf_id_registro(ll_id_registro)

IF lu_reg_pd_funct.uf_is_registro_cee(ll_id_registro) OR ll_IDReg_PD_generata_esistente > 0 THEN
	iu_reg_pd.uf_num_progressivo(ll_num_progressivo)
END IF
IF ll_num_progressivo_esistente > 0 THEN
	iu_reg_pd.uf_num_progressivo(ll_num_progressivo_esistente)
END IF

iu_reg_pd.uf_data_competenza(ld_data_competenza)
iu_reg_pd.uf_id_prof_registrazione(ll_IDProf_Registrazione)
iu_reg_pd.uf_data_registrazione(ld_data_registrazione)
iu_reg_pd.uf_id_valuta(ll_IDValuta)
iu_reg_pd.uf_cambio(ldc_cambio)
iu_reg_pd.uf_importo(ldc_importo)
iu_reg_pd.uf_descrizione(ls_des_contabile)
iu_reg_pd.uf_ges_oper_intracomunitarie(lc_ges_oper_intracomunitarie)
iu_reg_pd.uf_tipo_fattura(ls_tipo_fattura)
iu_reg_pd.uf_num_documento(ls_num_documento)
iu_reg_pd.uf_data_documento(ld_data_documento)
iu_reg_pd.uf_id_conto(ll_IDFornitore)
iu_reg_pd.uf_id_azienda(ll_IDAzienda)
iu_reg_pd.uf_id_reg_pd_padre(ll_IDReg_PD)
iu_reg_pd.uf_imp_valuta(ldc_importo_val)
iu_reg_pd.uf_id_reg_pd_padre(al_id_reg_pd_padre)

/* Movimenti IVA */
iu_reg_pd.uf_imponibile(ldc_imponibile)
iu_reg_pd.uf_id_tab_iva(ll_id_tab_iva)
iu_reg_pd.uf_imposta(ldc_imposta)
iu_reg_pd.uf_ges_art74(ls_ges_art74)

/* Movimenti Contabili */
iu_reg_pd.uf_no_mc(TRUE)

ll_id_reg_pd = iu_reg_pd.uf_crea()

IF IsValid(lu_reg_pd_funct) THEN
	Destroy lu_reg_pd_funct
END IF

//-----------------------------------------------------------------------------
// [GIO] 11/10/04 Se il wizard viene eseguito marca il record di wiz_reg_pd
//-----------------------------------------------------------------------------
if ll_id_reg_pd > 0 then
	
	//lb_tran	= sqlci.nf_during_tran()
	//if lb_tran = false then
	//	sqlci.nf_begintran()
	//end if
	
	update dba.wiz_reg_pd
		set eseguito = 'S'
		where id_reg_pd = :al_id_reg_pd_padre and
				id_wizard = :al_id_wizard
		using sqlci;
		
	if lb_tran = false then	
		if sqlci.sqlcode = 0 then
			//sqlci.nf_commit()
		else
			//sqlci.nf_rollback()
		end if
	end if
	
	//iu_reg_pd.uf_sincronizza_banca_azienda(ll_id_reg_pd)
else

	//---------------------------------------------------------------------
	// [GIU] 29/04/2008 Messaggio nel caso in cui non venga creata la registrazione
	// recupero il motivo per il quale non è stata creata la registrazione
	//----------------------------------------------------------------------
	ls_msg[1] = iu_reg_pd.uf_get_info()
	//n_err_mgr.nf_mess_immediato(55523, ls_msg)
end if

return 0


end function

on uo_wizard_base.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_wizard_base.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

