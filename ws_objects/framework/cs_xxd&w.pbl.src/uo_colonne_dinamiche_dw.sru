﻿$PBExportHeader$uo_colonne_dinamiche_dw.sru
forward
global type uo_colonne_dinamiche_dw from uo_std_dw
end type
end forward

global type uo_colonne_dinamiche_dw from uo_std_dw
integer width = 1079
integer height = 784
string dataobject = "d_colonne_dinamiche_dw"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
event uoe_set_dddw ( )
end type
global uo_colonne_dinamiche_dw uo_colonne_dinamiche_dw

type variables
// Modalità di utilizzo
CONSTANT string FULL_EDIT_MODE = "F"
CONSTANT string ONLY_VIEW_MODE = "V"

private:
	string is_window_name
	string is_pks[]
	string is_update_table
	datawindow idw_parent
	
	string is_error
	string is_mode
end variables

forward prototypes
public function boolean uof_set_window_name (string as_window_name)
public function boolean uof_set_dw_parent (ref datawindow adw_parent)
public function boolean uof_set_dw_primary_keys (string as_pks[])
public function string uof_get_error ()
public function boolean uof_set_dw (ref datawindow adw_parent, string as_primary_keys[])
public function long retrieve ()
public function boolean uof_set_update_table (string as_update_table)
public function integer update ()
public function integer deleterow (long r)
private function any uof_get_value (integer ai_row)
private function boolean uof_is_valid_pks ()
private function any uof_get_parent_value (long al_row, string as_column)
private function string uof_build_insert_pk_values ()
public function boolean uof_verify_changes ()
public function boolean uof_delete ()
private function any uof_get_parent_value (long al_row, string as_column, ref string as_column_type, dwbuffer adb_buffer)
public subroutine uof_set_dw_mode (string as_mode)
private function boolean uof_has_col_din (string as_cod_col_din)
private function boolean uof_build_where (dwbuffer adb_buffer, ref string as_where)
end prototypes

event uoe_set_dddw();f_PO_LoadDDDW_DW(this, "cod_colonna_dinamica", sqlca, &
                 "tab_colonne_dinamiche","cod_colonna_dinamica","des_colonna_dinamica",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
		
// inietto una riga default per aggiungere le nuove colonne
//if is_mode = FULL_EDIT_MODE then
//	
//	long ll_row
//	DataWindowChild ldw_child
//	
//	getchild("cod_colonna_dinamica", ref ldw_child)
//	
//	ll_row = ldw_child.insertrow(1)
//	ldw_child.setitem(ll_row, "data_column", IS_NEW_COL_COD)
//	ldw_child.setitem(ll_row, "display_column", "Aggiungi colonna dinamica")
//end if
end event

public function boolean uof_set_window_name (string as_window_name);/**
 * stefanop
 * 22/11/2011
 *
 * Imposta il nome della finestra per recuperare le colonne dinamiche.
 *
 * La funzione viene chiamata in automatico tramite il costruttore oppure è possibile forzare
 * il caricamento delle colonne su un'altra finestra.
 **/
 
is_window_name = as_window_name
return true
end function

public function boolean uof_set_dw_parent (ref datawindow adw_parent);/**
 * stefanop
 * 22/11/2011
 *
 * Imposto la datawindow padre da dove verranno recuperati i valori per le chiavi primarie
 **/
 
if isvalid(adw_parent) and not isnull(adw_parent) then
	idw_parent = adw_parent
	setnull(is_error)
	
	// recupero il nome del nome tabella
	uof_set_update_table(idw_parent.Object.DataWindow.Table.updatetable + "_col_din")
	return true
else
	setnull(idw_parent)
	is_error = "Impostare una datawindow padre per recuperare i valori necessari"
	return false
end if


end function

public function boolean uof_set_dw_primary_keys (string as_pks[]);/**
 * stefanop
 * 22/11/2011
 *
 * Imposto i nomi delle colonne che sono chavi primarie per poter recuperare i valori
 * per eseguire l'SQL
 **/
 
if isnull(as_pks) or upperbound(as_pks) < 1 then
	is_error = "Impostare le chiavi primarie in maniera corretta"
	return false
else
	is_pks = as_pks
	setnull(is_error)
	return true
end if


end function

public function string uof_get_error ();/**
 * stefanop
 * 22/11/2011
 *
 * Ritorna la stringa dell'ultimo errore avvenuto.
 * Se nulla è andato tutto a buon fine
 **/
 
 
return is_error
end function

public function boolean uof_set_dw (ref datawindow adw_parent, string as_primary_keys[]);/**
 * stefanop
 * 22/11/2011
 *
 * Funzione per inizializzare i campi base per il funzionamento
 **/
 
 
if not uof_set_dw_parent(adw_parent) then return false

if not uof_set_dw_primary_keys(as_primary_keys) then return false
end function

public function long retrieve ();string ls_sql, ls_errore, ls_cod_col_din, ls_flag_visualizza_subito, ls_flag_obbligatorio, ls_where
long ll_rowcount, ll_i, ll_row, ll_test
datastore lds_store

setredraw(false)
reset()
resetupdate()

// se non ho le chiavi primarie impostate nel padre non posso fare nessun query
if not uof_is_valid_pks() then
	setredraw(true)
	return 0
end if

if is_mode = ONLY_VIEW_MODE then
	// carico le colonne dinamiche impostate dalla gestione multi-lingua
	ls_sql = "SELECT tab_colonne_dinamiche_window.cod_colonna_dinamica, flag_visualizza_subito, flag_obbligatorio, " + &
				"tab_colonne_dinamiche.flag_tipo_colonna, tab_colonne_dinamiche.formato FROM tab_colonne_dinamiche_window " + &
				"LEFT JOIN tab_colonne_dinamiche ON tab_colonne_dinamiche.cod_colonna_dinamica = tab_colonne_dinamiche_window.cod_colonna_dinamica " + &
				"WHERE tab_colonne_dinamiche_window.cod_azienda='" + s_cs_xx.cod_azienda + "' AND nome_window='" + is_window_name + "' "
	
	ll_rowcount = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_errore)
	
	// errore
	if ll_rowcount < 0 then
		is_error = ls_errore
		return -1
	end if
	
	for ll_i = 1 to ll_rowcount
		
		ls_cod_col_din = lds_store.getitemstring(ll_i, 1)
		ls_flag_visualizza_subito = lds_store.getitemstring(ll_i, 2)
		ls_flag_obbligatorio = lds_store.getitemstring(ll_i, 3)
		
		if ls_flag_visualizza_subito = "S" then
			
			ll_row = insertrow(0)
			setitem(ll_row, "cod_colonna_dinamica", ls_cod_col_din)
			setitem(ll_row, "flag_tipo_colonna", lds_store.getitemstring(ll_i, 4))
			setitem(ll_row, "formato", lds_store.getitemstring(ll_i, 5))
			setitem(ll_row, "flag_modalita", is_mode)
			setitemstatus(ll_row, 0, Primary!, NotModified!)
			
		end if
		
	next
end if

if not uof_build_where(Primary!, ls_where) then
	is_error = "Errore durante la lettura delle chiavi delle colonne dinamiche!"
	return -1
end if

// carico le colonne dinamiche impostate dall'utente ed i relativi valori
ls_sql = "SELECT tab_colonne_dinamiche.cod_colonna_dinamica, id_valore_lista, valore_stringa, valore_numero, valore_data, tab_colonne_dinamiche.flag_tipo_colonna FROM " + is_update_table + &
			" LEFT JOIN tab_colonne_dinamiche ON tab_colonne_dinamiche.cod_colonna_dinamica =" + is_update_table+ ".cod_colonna_dinamica " + ls_where
			
ll_rowcount = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_errore)

// errore
if ll_rowcount < 0 then
	is_error = ls_errore
	return -1
end if

for ll_i = 1 to ll_rowcount
	
	ls_cod_col_din = lds_store.getitemstring(ll_i, 1)
	
	ll_row = find("cod_colonna_dinamica='" + ls_cod_col_din + "'", 0, rowcount())
	
	if ll_row < 1 then 
		ll_row = insertrow(0)
	end if
	
	setitem(ll_row, "cod_colonna_dinamica",  ls_cod_col_din)
	setitem(ll_row, "flag_tipo_colonna", lds_store.getitemstring(ll_i, 6))
	setitem(ll_row, "id_valore_lista", lds_store.getitemnumber(ll_i, 2))
	setitem(ll_row, "valore_stringa", lds_store.getitemstring(ll_i, 3))
	setitem(ll_row, "valore_numero", lds_store.getitemnumber(ll_i, 4))
	setitem(ll_row, "valore_data", lds_store.getitemdatetime(ll_i, 5))
	setitem(ll_row, "flag_modalita", is_mode)
	setitemstatus(ll_row, 0, Primary!, DataModified!)
	setitemstatus(ll_row, 0, Primary!, NotModified!)
next

setredraw(true)
return  0
end function

public function boolean uof_set_update_table (string as_update_table);/**
 * stefanop
 * 22/11/2011
 *
 * Impostare il nome della tabella dove verranno salvati i nomi delle colonen dinamiche.
 * Il sistema carica in automatico il nome prelevando il nome tabella dalla datawindow parent
 * e aggiungendo la string "_col_din".
 * Esempio: anag_prodotti -> anag_prodotti_col_din
 **/
 
 
is_update_table = as_update_table
return true
end function

public function integer update ();string ls_sql, ls_where, ls_value, ls_column_type, ls_where_delete_buff
int li_i, li_count
any la_value
DWItemStatus ldw_item_status

accepttext()
if not uof_build_where(Primary!, ls_where) then
	return 1
end if


// Elimino le colonne nel buffer di cancellazione
for li_i = 1 to DeletedCount()
	
	ls_sql = "DELETE FROM " + is_update_table + ls_where + " AND cod_colonna_dinamica='" + getitemstring(li_i, "cod_colonna_dinamica", Delete!, true)  + "' "
	
	execute immediate :ls_sql;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la cancellazione delle colonne dinamiche.", sqlca)
		rollback;
		return -1
	end if
	
next

for li_i = 1 to rowcount()
	
	ldw_item_status = getitemstatus(li_i, 0, Primary!)
	la_value = uof_get_value(li_i)
	
	// controllo lo stato della riga per decidere cosa fare
//	if ldw_item_status = NotModified! or ldw_item_status = New! or isnull(la_value) then
	if ldw_item_status = NotModified! or ldw_item_status = New! then
		
		// NON DEVO SEMPRE CANCELLARE, SOLO SE HO FATTO UNA MODIFICA
//		if isnull(la_value) then
//			
//			ls_sql = "DELETE FROM " + is_update_table + ls_where + " AND cod_colonna_dinamica='" + getitemstring(li_i, "cod_colonna_dinamica")  + "' "
//	
//			execute immediate :ls_sql;
//			
//			if sqlca.sqlcode < 0 then
//				g_mb.error("Errore durante la cancellazione delle colonne dinamiche.", sqlca)
//				rollback;
//				return -1
//			end if
//			
//		end if
		
		// non è stato toccato nulla, passo alla prossima riga
		// la riga è nuova, ma non modificata, quindi non occorre salvarla
		continue
			
	elseif ldw_item_status = DataModified! then
		// la colonna esiste nel DB, devo solo aggiornare i valori
		
		if isnull(la_value) then
			ls_sql = "DELETE FROM " + is_update_table + ls_where + " AND cod_colonna_dinamica='" + getitemstring(li_i, "cod_colonna_dinamica")  + "' "
			f_log_sistema(ls_sql, "COLDIN")
			
		else
			ls_sql = "UPDATE " + is_update_table + " SET "
			
			choose case getitemstring(li_i, "flag_tipo_colonna")
				case "L"
					ls_sql += " id_valore_lista=" + string(la_value)
					
				case "C"
					ls_sql += " valore_stringa='" + string(la_value) + "' "
					
				case "D"
					ls_sql += " valore_data='" + string(la_value, s_cs_xx.db_funzioni.formato_data) + "' "
					
				case "N"
					ls_sql += " valore_numero=" + string(la_value)
					
			end choose
			
			ls_sql += ls_where + " AND cod_colonna_dinamica='" + getitemstring(li_i, "cod_colonna_dinamica")  + "' "
			
		end if
		
	elseif ldw_item_status = NewModified!  then
		// la colonna è nuova e modificata, la devo inserire
		ls_sql = "INSERT INTO " + is_update_table + " (cod_azienda, cod_colonna_dinamica, " + guo_functions.uof_implode(is_pks, ", ")
		ls_value = ""
		
		choose case getitemstring(li_i, "flag_tipo_colonna")
			case "L"
				if not isnull(la_value) then
					ls_sql += ",id_valore_lista"
					ls_value = string(la_value)
				end if
				
			case "C"
				if not isnull(la_value) then
					ls_sql += ",valore_stringa"
					ls_value = " '" + string(la_value) + "' "
				end if
				
			case "D"
				if not isnull(la_value) then
					ls_sql += ",valore_data"
					ls_value = " '" + string(la_value, s_cs_xx.db_funzioni.formato_data) + "' "
				end if
				
			case "N"
				if not isnull(la_value) then
					ls_sql += ",valore_numero"
					ls_value = string(la_value)
				end if
				
		end choose
		
		ls_sql += ") VALUES ('" + s_cs_xx.cod_azienda + "', '" + getitemstring(li_i, "cod_colonna_dinamica") + "', " + uof_build_insert_pk_values() + "," + ls_value + ")"
		
	end if
	
	execute immediate :ls_sql;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il salvataggio delle colonne dinamiche.", sqlca)
		return -1
	end if
next

commit;


// TODO: Verificare in caso di errore se va bene questo reset
resetupdate()

return 1
end function

public function integer deleterow (long r);/**
 * stefanop
 * 24/11/2011
 *
 * Cancello la riga, ma dato che la DW non ha le proprietà di update
 * devo spostare la riga nel buffer di delete a mano
 **/
 
// copio la riga del buffer di delete
rowscopy(r, r, Primary!, this, 9999, Delete!)

// cancello la riga dalla DW
super::deleterow(r)

return 1
end function

private function any uof_get_value (integer ai_row);/**
 * stefanop
 * 24/11/2011
 *
 * Centralizzo il recupero del valore all'interno della DW in base al tipo colonna dimanica
 **/
 
any la_value
 
choose case getitemstring(ai_row, "flag_tipo_colonna")
		
	case "L"
		la_value = getitemnumber(ai_row, "id_valore_lista")
		
	case "C"
		la_value = getitemstring(ai_row, "valore_stringa")
		
	case "D"
		la_value = getitemdatetime(ai_row, "valore_data")
		
	case "N"
		la_value = getitemnumber(ai_row, "valore_numero")
		
end choose

return la_value
end function

private function boolean uof_is_valid_pks ();/**
 * stefanop
 * 24/11/2011
 *
 * Verifico che le chiavi primarie del padre siano correttamente inzializzate,
 * altrimenti la retrieve o altri controllo non andrebbero a buon fine.
 **/
 
int li_i
long ll_parent_row
any la_value

// controllo dw
if not isvalid(idw_parent) or isnull(idw_parent) then return false

// controllo riga
ll_parent_row = idw_parent.getrow()
if ll_parent_row < 1 then return false
 
for li_i = 1 to upperbound(is_pks)
	
	// recupero valore della riga, colonna
	la_value = uof_get_parent_value(ll_parent_row, is_pks[li_i])
		
	if isnull(la_value) then return false
	
next

 
return true
end function

private function any uof_get_parent_value (long al_row, string as_column);/**
 * stefanop
 * 24/11/2011
 *
 * Centralizzo la gestione di recupero valore dalla datawindo padre in base alla tipologia di 
 * colonna.
 **/
 
string ls_column_type
return uof_get_parent_value(al_row, as_column, ls_column_type, Primary!)
end function

private function string uof_build_insert_pk_values ();/**
 * stefanop
 * 22/11/2011
 *
 * Recupero i valori delle chiavi primarie e li preparo per essere passati alla insert.
 **/
 
string ls_where[], ls_column_type
int li_i
long ll_parent_row
any la_value

if not isvalid(idw_parent) or isnull(idw_parent) then return ""

ll_parent_row = idw_parent.getrow()

for li_i = 1 to upperbound(is_pks)
	
	la_value = uof_get_parent_value(ll_parent_row, is_pks[li_i], ls_column_type, Primary!)
	
	// recupero la tipologia della colonna parent per creare un maniera corretta la where
	choose case ls_column_type
		case "char("
			if not isnull(la_value) then ls_where[li_i] = "'" + string(la_value) + "' "
			
		case "date"
			if not isnull(la_value) then ls_where[li_i] = "'" + string(la_value, s_cs_xx.db_funzioni.formato_data) + "' "
			
		case "datet"
			if not isnull(la_value) then ls_where[li_i] = "" + string(la_value, s_cs_xx.db_funzioni.formato_data) + "' "
			
		case "int", "long", "numbe", "real"
			if not isnull(la_value) then ls_where[li_i] = string(la_value)
			
	end choose
	
next

return guo_functions.uof_implode(ls_where, ", ")
end function

public function boolean uof_verify_changes ();/**
 * stefanop
 * 29/11/2011
 *
 * Controllo se ci sono modifiche all'itnerno della DW.
 * L'evento va codificato all'interno dell'evento rowfocuschanging della DW padre
 **/
 

int li_i
boolean lb_changes
dwitemstatus ldw_status

lb_changes = false

// controllo modifiche al buffer primario
for li_i = 1 to rowcount()
	
	ldw_status = getitemstatus(li_i, 0, Primary!)

	if ldw_status = NotModified! or ldw_status = New! then continue

	lb_changes = true
	exit	
next

// controllo modifiche al buffer di cancellazione
if not lb_changes then
	lb_changes = (deletedcount() > 0)
end if

if lb_changes then
	if g_mb.confirm("Ci sono delle modifiche alle colonne dinamiche, Salvo?") then
		update()
	end if
end if

return true
end function

public function boolean uof_delete ();/**
 * stefanop
 * 29/11/2011
 *
 * Cancello le righe che sono nel buffer Delete! del padre.
 * Da chiamare all'evento updatestart
 **/
 
int li_i, li_count
string ls_sql, ls_where

if idw_parent.DeletedCount() < 1 then return true

if not uof_build_where(Delete!, ls_where) then
	return true
end if

for li_i = 1 to  idw_parent.DeletedCount()
	
	ls_sql = "DELETE FROM " + is_update_table + ls_where
	
	f_log_sistema(ls_sql, "COLDIN")
	
	execute immediate :ls_sql;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la cancellazione delle colonne dinamiche.", sqlca)
		return false
	end if
	
next
 
return true
end function

private function any uof_get_parent_value (long al_row, string as_column, ref string as_column_type, dwbuffer adb_buffer);/**
 * stefanop
 * 24/11/2011
 *
 * Centralizzo la gestione di recupero valore dalla datawindo padre in base alla tipologia di 
 * colonna.
 **/
  
try
	as_column_type = left(lower(idw_parent.Describe(as_column + ".ColType")), 5)
	
	choose case as_column_type
		case "char("
			return idw_parent.getitemstring(al_row, as_column, adb_buffer, false)
			
		case "date"
			return idw_parent.getitemdate(al_row, as_column, adb_buffer, false)
			
		case "datet"
			return idw_parent.getitemdatetime(al_row, as_column, adb_buffer, false)
			
		case "int", "long", "numbe", "real"
			return idw_parent.getitemnumber(al_row, as_column, adb_buffer, false)
			
	end choose
	
catch (Runtimeerror ex)
	g_mb.error("Errore durante il recupero del valore per la colonna " + as_column + ".~r~nProbabilmente non esiste!")
	is_error = ex.getmessage()
end try
end function

public subroutine uof_set_dw_mode (string as_mode);/**
 * stefanop
 * 03/12/2011
 *
 * Imposto la modalità di funzionamento della datawindow
 *
 **/
 
 string ls_error 
 
 if as_mode <> FULL_EDIT_MODE and as_mode <> ONLY_VIEW_MODE then
	as_mode = ONLY_VIEW_MODE
end if


is_mode = as_mode

if is_mode = ONLY_VIEW_MODE then
	
	modify("cod_colonna_dinamica.TabSequence=0")
	modify("DataWindow.Summary.Height=0")
	
else
	
	modify("cod_colonna_dinamica.TabSequence=1")
	modify("DataWindow.Summary.Height=24")
	
end if


end subroutine

private function boolean uof_has_col_din (string as_cod_col_din);/**
 * stefanop
 * 05/12/2011
 *
 * Controllo se la colonna dinamica è già stata inserita all'interno della DW
 **/
 
 
long ll_find

ll_find = find("cod_colonna_dinamica='" + as_cod_col_din + "'", 1, rowcount())

return (ll_find > 0)
end function

private function boolean uof_build_where (dwbuffer adb_buffer, ref string as_where);/**
 * stefanop
 * 22/11/2011
 *
 * Creo la where per eseguire la query in base alle PK passate durante la creazione.
 **/
 
string ls_where[], ls_column_type
int li_i
long ll_parent_row
any la_value

if not isvalid(idw_parent) or isnull(idw_parent) then return false

ll_parent_row = idw_parent.getrow()

for li_i = 1 to upperbound(is_pks)
	
	la_value = uof_get_parent_value(ll_parent_row, is_pks[li_i], ls_column_type, adb_buffer)
	
	// recupero la tipologia della colonna parent per creare un maniera corretta la where
	choose case ls_column_type
		case "char("
			if not isnull(la_value) then ls_where[li_i] = is_pks[li_i] + "='" + string(la_value) + "' "
			
		case "date"
			if not isnull(la_value) then ls_where[li_i] = is_pks[li_i] + "='" + string(la_value, s_cs_xx.db_funzioni.formato_data) + "' "
			
		case "datet"
			if not isnull(la_value) then ls_where[li_i] = is_pks[li_i] + "='" + string(la_value, s_cs_xx.db_funzioni.formato_data) + "' "
			
		case "int", "long", "numbe", "real"
			if not isnull(la_value) then ls_where[li_i] = is_pks[li_i] + "=" + string(la_value)
			
	end choose
	
next

// L'array della where deve corrispondere con quello della PK
if upperbound(is_pks) <> upperbound(ls_where) then return false

ls_where[upperbound(ls_where) + 1] = is_update_table + ".cod_azienda='" + s_cs_xx.cod_azienda + "' "

as_where = " WHERE " + guo_functions.uof_implode(ls_where, " AND ")
return true
end function

on uo_colonne_dinamiche_dw.create
call super::create
end on

on uo_colonne_dinamiche_dw.destroy
call super::destroy
end on

event constructor;call super::constructor;
// recupero il nome della finestra
if getparent().typeOf() = UserObject! then
	if getparent().getparent().TypeOf() = Tab! then
		uof_set_window_name(getparent().getparent().getparent().classname())
	else	
		uof_set_window_name(getparent().getparent().classname())
	end if
else
	uof_set_window_name(getparent().classname())
end if

// controllo parametro aziendale COL per verificare
string ls_mode

ls_mode = ONLY_VIEW_MODE

select stringa
into :ls_mode
from parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	flag_parametro = 'S' and
	cod_parametro = 'MCD';
	
uof_set_dw_mode(ls_mode)


trigger event uoe_set_dddw()


object.b_cancella.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\delete.png"
object.p_add_icon.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\add.png"

end event

event rowfocuschanged;call super::rowfocuschanged;//if currentrow > 0 then
//	selectrow(0, false)
//	selectrow(currentrow, true)
//end if

if getitemstring(currentrow, "flag_tipo_colonna") = "L" then
	
	setcolumn("id_valore_lista")
	f_po_loaddddw_dw(this, &
                 "id_valore_lista", &
                 sqlca, &
                 "tab_colonne_dinamiche_valori", &
                 "progressivo", &
                 "des_valore", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' AND cod_colonna_dinamica='" + getitemstring(currentrow, "cod_colonna_dinamica") + "'")

end if
end event

event clicked;call super::clicked;string ls_cod_colonna_dinamica, ls_des_colonna_dinamica
long ll_row

setrow(row)

choose case dwo.name
		
	// stefanop 16/11/2011: se clicco sulla computeted seleziono la tendina!
	case "cf_valore_lista"
		setcolumn('id_valore_lista')
		
	case "b_cancella"
		if row < 1 then return 0
		ls_cod_colonna_dinamica = getitemstring(row, "cod_colonna_dinamica")
		ls_des_colonna_dinamica = f_des_tabella("tab_colonne_dinamiche", "cod_colonna_dinamica='" + ls_cod_colonna_dinamica +"'", "des_colonna_dinamica")
		if g_mb.confirm("Cancello la colonna dinamica '" + ls_des_colonna_dinamica + "'?") then deleterow(row)
		
	case "new_col_din_t", "p_add_icon"
		ll_row = insertrow(0)
		setrow(ll_row)
		
end choose
end event

event losefocus;call super::losefocus;accepttext()
end event

event itemchanged;call super::itemchanged;string ls_flag_tipo_colonna

choose case dwo.name
		
	case "cod_colonna_dinamica"
		// non posso cambiare la colonna dinamica se è già impostato il tipo
		//if len(getitemstring(row, "flag_tipo_colonna")) > 0 then return 2
		
		if uof_has_col_din(data) then
			g_mb.show("Colonna dinamica già presente!")
			return 0
		end if
		
		if len(data) < 1 then return 0
		
		select flag_tipo_colonna
		into :ls_flag_tipo_colonna
		from tab_colonne_dinamiche
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_colonna_dinamica = :data;
			
		setitem(row, "flag_tipo_colonna", ls_flag_tipo_colonna)
		
		if ls_flag_tipo_colonna = "L" then
			setcolumn("id_valore_lista")
		elseif ls_flag_tipo_colonna = "D" then
			setcolumn("valore_data")
		elseif ls_flag_tipo_colonna = "N" then
			setcolumn("valore_numero")
		else
			setcolumn("valore_stringa")
		end if
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_salva"
		if update() = 1 then
			
			g_mb.success("Salvataggio colonne dinamiche avvenuto con successo!")
			
		end if
		
end choose
end event

