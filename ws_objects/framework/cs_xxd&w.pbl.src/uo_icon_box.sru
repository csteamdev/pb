﻿$PBExportHeader$uo_icon_box.sru
forward
global type uo_icon_box from userobject
end type
type st_title from statictext within uo_icon_box
end type
type p_icon from picture within uo_icon_box
end type
end forward

global type uo_icon_box from userobject
integer width = 521
integer height = 324
long backcolor = 67108864
string text = "none"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
event resize ( )
event clicked pbm_lbuttonclk
st_title st_title
p_icon p_icon
end type
global uo_icon_box uo_icon_box

type variables
public:
	long il_draging_color = 12632256
	string is_title = "Titolo"
	long il_background_color=12632256
end variables

forward prototypes
public subroutine uof_set_backcolor (long il_backcolor)
public subroutine uof_set_image (string as_path)
public subroutine uof_set_title (string as_title)
end prototypes

event resize();int li_height

li_height = p_icon.height + st_title.height + 10
li_height = int((height - li_height) / 2)

p_icon.x = int((width / 2) - (p_icon.width / 2))
p_icon.y = li_height //int((height / 2) - (p_icon.height / 2) - 40) 

st_title.move(20, p_icon.y +  p_icon.height+ 10)
st_title.width = width - 40
st_title.backcolor = backcolor
end event

public subroutine uof_set_backcolor (long il_backcolor);backcolor = il_backcolor
st_title.backcolor = il_backcolor
end subroutine

public subroutine uof_set_image (string as_path);p_icon.picturename = as_path
end subroutine

public subroutine uof_set_title (string as_title);is_title = as_title
st_title.text = as_title
end subroutine

on uo_icon_box.create
this.st_title=create st_title
this.p_icon=create p_icon
this.Control[]={this.st_title,&
this.p_icon}
end on

on uo_icon_box.destroy
destroy(this.st_title)
destroy(this.p_icon)
end on

event constructor;st_title.text = is_title

uof_set_backcolor(il_background_color)
event resize()
end event

event rbuttondown;parent.postevent("rbuttondown")
end event

type st_title from statictext within uo_icon_box
integer x = 14
integer y = 148
integer width = 320
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Titolo"
alignment alignment = center!
boolean focusrectangle = false
end type

event clicked;parent.triggerevent("clicked")
end event

event dragleave;parent.event dragleave(source)
end event

event dragenter;parent.event dragenter(source)
end event

event dragdrop;parent.event dragdrop(source)
end event

event rbuttondown;parent.postevent("rbuttondown")
end event

type p_icon from picture within uo_icon_box
integer x = 18
integer y = 16
integer width = 146
integer height = 128
boolean originalsize = true
boolean focusrectangle = false
end type

event clicked;parent.triggerevent("clicked")
end event

event dragdrop;parent.event dragdrop(source)
end event

event dragenter;parent.event dragenter(source)
end event

event dragleave;parent.event dragleave(source)
end event

event rbuttondown;parent.postevent("rbuttondown")
end event

