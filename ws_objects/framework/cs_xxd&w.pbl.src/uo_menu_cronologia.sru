﻿$PBExportHeader$uo_menu_cronologia.sru
forward
global type uo_menu_cronologia from userobject
end type
type st_avvisi from statictext within uo_menu_cronologia
end type
type dw_menu from datawindow within uo_menu_cronologia
end type
end forward

global type uo_menu_cronologia from userobject
integer width = 1353
integer height = 1864
long backcolor = 67108864
string text = "none"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
st_avvisi st_avvisi
dw_menu dw_menu
end type
global uo_menu_cronologia uo_menu_cronologia

type variables
transaction it_sqlcb

public:
	long il_background_color=12632256
end variables

forward prototypes
public subroutine wf_set_background_color (long al_background_color)
public subroutine wf_chiudi ()
public subroutine wf_ridimensiona ()
public subroutine wf_modifica_cronologia (integer ai_cod_profilo, integer ai_progressivo_menu, string as_nome, string as_collegamento, character ac_nuovo, character ac_modifica, character ac_cancella)
end prototypes

public subroutine wf_set_background_color (long al_background_color);il_background_color=al_background_color
backColor=il_background_color
dw_menu.Modify("background.color="+String(il_background_color))
end subroutine

public subroutine wf_chiudi ();String ls_error
transaction lt_sqlcb

//Inizializzo la nuova transazione
if(Not (guo_functions.uof_create_transaction_from(sqlca, lt_sqlcb, ls_error))) Then
	g_mb.error(ls_error)
	return
end if
dw_menu.setTransObject(lt_sqlcb);

dw_menu.Update()
COMMIT using lt_sqlcb;

Disconnect using lt_sqlcb;
Destroy lt_sqlcb;
end subroutine

public subroutine wf_ridimensiona ();st_avvisi.width = width
st_avvisi.y = (height/2)

dw_menu.width = width
dw_menu.height = height

dw_menu.Modify("nome.width="+String(width - 68))
// -83 perchè ci sono 73 di immagine e 10 arbitrari tra bordo (o non è conteggiato nella larghezza della colonna?) e un po' di margine...
dw_menu.Modify("p_iconafinestra.x="+String(Long(dw_menu.Describe("nome.width")) - 83))
end subroutine

public subroutine wf_modifica_cronologia (integer ai_cod_profilo, integer ai_progressivo_menu, string as_nome, string as_collegamento, character ac_nuovo, character ac_modifica, character ac_cancella);integer li_riga

dw_menu.setRedraw(false)

li_riga=dw_menu.Find("(menu_cronologia_cod_profilo="+String(ai_cod_profilo)+") AND (menu_cronologia_progressivo_menu="+String(ai_progressivo_menu)+")", 0, dw_menu.rowCount())
//In questo caso ho trovato un risultato
if(li_riga>0) Then
	dw_menu.SetItem(li_riga, "ultima_apertura", DateTime(Today(), Now()))
	dw_menu.SetItem(li_riga, "conteggio", (dw_menu.getItemNumber(li_riga, "conteggio") + 1))
else
	li_riga=dw_menu.InsertRow(0)
	dw_menu.SetItem(li_riga, "cod_azienda", s_cs_xx.cod_azienda)
	dw_menu.SetItem(li_riga, "cod_utente", s_cs_xx.cod_utente)
	dw_menu.SetItem(li_riga, "menu_cronologia_cod_profilo", ai_cod_profilo)
	dw_menu.SetItem(li_riga, "menu_cronologia_progressivo_menu", ai_progressivo_menu)
	dw_menu.SetItem(li_riga, "nome", as_nome)
	dw_menu.SetItem(li_riga, "menu_collegamento", as_collegamento)
	dw_menu.SetItem(li_riga, "ultima_apertura", DateTime(Today(), Now()))
	dw_menu.SetItem(li_riga, "conteggio", 1)
	dw_menu.SetItem(li_riga, "menu_flag_nuovo", ac_nuovo)
	dw_menu.SetItem(li_riga, "menu_flag_modifica", ac_modifica)
	dw_menu.SetItem(li_riga, "menu_flag_cancella", ac_cancella)
end if

dw_menu.Sort()

dw_menu.setRedraw(true)
dw_menu.SelectRow(0, false)

//Limito a 20 il numero di elementi della cronologia
if(dw_menu.RowCount()>20) Then
	dw_menu.DeleteRow(21)
end if

//Non ci sono dati
if(dw_menu.RowCount()=0) Then
	st_avvisi.Text="Nessun elemento presente nella cronologia.~nGli elementi si aggiungeranno automaticamente durante l'uso del programma."
	dw_menu.Visible=false
	st_avvisi.Visible=true
	
//Ci sono dei dati
else
	st_avvisi.Visible=false
	dw_menu.Visible=true
end if
end subroutine

on uo_menu_cronologia.create
this.st_avvisi=create st_avvisi
this.dw_menu=create dw_menu
this.Control[]={this.st_avvisi,&
this.dw_menu}
end on

on uo_menu_cronologia.destroy
destroy(this.st_avvisi)
destroy(this.dw_menu)
end on

event constructor;long ll_retrieve
String ls_error

backColor=il_background_color
st_avvisi.backColor=il_background_color

//Pulsante della DataWindow
dw_menu.object.p_iconafinestra.Filename  = guo_functions.uof_risorse("menu/window.png")

dw_menu.setTransObject(SQLCA);
ll_retrieve=dw_menu.Retrieve(s_cs_xx.cod_azienda, s_cs_xx.cod_utente)

//Errore nella lettura dei dati
if(ll_retrieve<0) Then
	g_mb.messagebox("Menu Cronologia","Errore nella lettura dei dati:~nImpossibile caricare la cronologia.",stopsign!)
	st_avvisi.Text="Errore nella lettura dei dati:~nImpossibile caricare la cronologia."
	dw_menu.Visible=false
	st_avvisi.Visible=true
	return
else
	//Non ci sono dati
	if(ll_retrieve=0) Then
		st_avvisi.Text="Nessun elemento presente nella cronologia.~nGli elementi si aggiungeranno automaticamente durante l'uso del programma."
		dw_menu.Visible=false
		st_avvisi.Visible=true
		
	//Ci sono dei dati
	else
		st_avvisi.Visible=false
		dw_menu.Visible=true
	end if
end if

dw_menu.setSort("ultima_apertura DESC")
end event

type st_avvisi from statictext within uo_menu_cronologia
integer y = 804
integer width = 1353
integer height = 256
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_menu from datawindow within uo_menu_cronologia
boolean visible = false
integer width = 1353
integer height = 1860
integer taborder = 10
string title = "none"
string dataobject = "d_menu_cronologia"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;long ll_elemento

//Evito errori in caso di doppio click con griglia vuota...
if(row<1) Then
	return
end if
		
if dw_menu.getitemstring(row,"menu_flag_nuovo") = "S" then
	s_cs_xx.flag_nuovo = true
else
	s_cs_xx.flag_nuovo = false
end if

if dw_menu.getitemstring(row,"menu_flag_modifica") = "S" then
	s_cs_xx.flag_modifica = true
else
	s_cs_xx.flag_modifica = false
end if

if dw_menu.getitemstring(row,"menu_flag_cancella") = "S" then
	s_cs_xx.flag_cancella = true
else
	s_cs_xx.flag_cancella = false
end if

window lw_window

window_type_open(lw_window,dw_menu.getitemstring(row,"menu_collegamento"), 6)
if isvalid(w_extra_windows) then
	w_extra_windows.add_window(dw_menu.getitemstring(row,"menu_collegamento"), dw_menu.getitemstring(row,"nome"))
end if

w_menu_80.uo_cronologia.wf_modifica_cronologia(dw_menu.getitemNumber(row, "menu_cronologia_cod_profilo"), dw_menu.getitemNumber(row, "menu_cronologia_progressivo_menu"), dw_menu.getitemstring(row,"nome"), dw_menu.getitemstring(row,"menu_collegamento"), dw_menu.getitemString(row, "menu_flag_nuovo"), dw_menu.getitemString(row, "menu_flag_modifica"), dw_menu.getitemString(row, "menu_flag_cancella"))

dw_menu.SelectRow(1, true)
TIMER(3)
end event

event clicked;dw_menu.SelectRow(0, false)
if(row>0) Then
	dw_menu.SelectRow(row, true)
end if
end event

