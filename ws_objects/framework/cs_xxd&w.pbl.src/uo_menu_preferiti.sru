﻿$PBExportHeader$uo_menu_preferiti.sru
forward
global type uo_menu_preferiti from userobject
end type
type st_avvisi from statictext within uo_menu_preferiti
end type
type dw_menu from datawindow within uo_menu_preferiti
end type
end forward

global type uo_menu_preferiti from userobject
integer width = 1353
integer height = 1864
long backcolor = 67108864
string text = "none"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
st_avvisi st_avvisi
dw_menu dw_menu
end type
global uo_menu_preferiti uo_menu_preferiti

type variables
boolean ib_clicked=false;
m_preferiti im_menu_destro;

public:
	long il_background_color=12632256
end variables

forward prototypes
public subroutine wf_set_background_color (long al_background_color)
public subroutine wf_aggiungi_preferito (integer ai_cod_profilo, integer ai_progressivo_menu, string as_nome, string as_collegamento, character ac_nuovo, character ac_modifica, character ac_cancella)
public subroutine wf_elimina_preferito ()
public subroutine wf_ridimensiona ()
public subroutine wf_riga_editabile ()
public subroutine wf_errore (string as_text)
end prototypes

public subroutine wf_set_background_color (long al_background_color);il_background_color=al_background_color
backColor=il_background_color
dw_menu.Modify("background.color="+String(il_background_color))
end subroutine

public subroutine wf_aggiungi_preferito (integer ai_cod_profilo, integer ai_progressivo_menu, string as_nome, string as_collegamento, character ac_nuovo, character ac_modifica, character ac_cancella);integer li_riga, li_rowcount
String ls_error
transaction lt_sqlcb

li_rowcount =  dw_menu.RowCount()

// controllo che l'elemento esista
if not (dw_menu.Find("(menu_preferiti_cod_profilo="+String(ai_cod_profilo)+") AND (menu_preferiti_progressivo_menu="+String(ai_progressivo_menu)+")", 0, li_rowcount)=0) then
	return
end if

//Inizializzo la nuova transazione
if(Not (guo_functions.uof_create_transaction_from(sqlca, lt_sqlcb, ls_error))) Then
	g_mb.error("Errore durante la creazione della nuova transazione", lt_sqlcb)
	return
end if

INSERT INTO menu_preferiti (cod_azienda, cod_utente, cod_profilo, progressivo_menu, nome, ordinamento)
	VALUES (:s_cs_xx.cod_azienda, :s_cs_xx.cod_utente, :ai_cod_profilo, :ai_progressivo_menu, :as_nome, :li_rowcount + 1)
	USING lt_sqlcb;

if(lt_sqlcb.SQLCode<0) Then
	g_mb.error("Errore durante l'inserimento del nuovo preferito", lt_sqlcb)
	wf_errore("Errore nella scrittura dei dati:~nImpossibile aggiungere l'elemento ai preferiti.")
	rollback using lt_sqlcb;
else 
	commit using lt_sqlcb;
	st_avvisi.Visible=false
	dw_menu.Visible=true
end if

disconnect using lt_sqlcb;
destroy lt_sqlcb;

// Se c'è stato un errore la DW è nascosta
if dw_menu.Visible=false then return

dw_menu.setRedraw(false)

//In questo caso ho trovato un risultato
li_riga=dw_menu.InsertRow(0)

dw_menu.SetItem(li_riga, "cod_azienda", s_cs_xx.cod_azienda)
dw_menu.SetItem(li_riga, "cod_utente", s_cs_xx.cod_utente)
dw_menu.SetItem(li_riga, "menu_preferiti_cod_profilo", ai_cod_profilo)
dw_menu.SetItem(li_riga, "menu_preferiti_progressivo_menu", ai_progressivo_menu)
dw_menu.SetItem(li_riga, "nome", as_nome)
dw_menu.SetItem(li_riga, "menu_collegamento", as_collegamento)
dw_menu.SetItem(li_riga, "ordinamento", li_rowcount)
dw_menu.SetItem(li_riga, "menu_descrizione_voce", as_nome)
dw_menu.SetItem(li_riga, "menu_flag_nuovo", ac_nuovo)
dw_menu.SetItem(li_riga, "menu_flag_modifica", ac_modifica)
dw_menu.SetItem(li_riga, "menu_flag_cancella", ac_cancella)

dw_menu.Sort()

dw_menu.setRedraw(true)


end subroutine

public subroutine wf_elimina_preferito ();Integer li_cod_profilo, li_progressivo_menu
string ls_error
long ll_i, ll_riga, ll_ordinamento
transaction lt_sqlcb

if not g_mb.confirm("Eliminare l'elemento dai preferiti?", 2) Then
	return
end if

ll_riga = dw_menu.getRow( )
li_cod_profilo = dw_menu.getitemNumber(ll_riga, "menu_preferiti_cod_profilo")
li_progressivo_menu = dw_menu.getitemNumber(ll_riga, "menu_preferiti_progressivo_menu")
ll_ordinamento =  dw_menu.getitemnumber(ll_riga, "ordinamento")
 
if(Not (guo_functions.uof_create_transaction_from(sqlca, lt_sqlcb, ls_error))) Then
	wf_errore("Errore nella scrittura dei dati:~nImpossibile eliminare l'elemento dai preferiti.")
	g_mb.error(ls_error)
	return
end if

delete from menu_preferiti
where ((cod_azienda = :s_cs_xx.cod_azienda) and
		 (cod_utente = :s_cs_xx.cod_utente) and
		 (cod_profilo = :li_cod_profilo) and
		 (progressivo_menu = :li_progressivo_menu))
using lt_sqlcb;
		 
if  lt_sqlcb.sqlcode < 0 then
	wf_errore("Errore nella scrittura dei dati:~nImpossibile eliminare l'elemento dai preferiti.")
	g_mb.error("Errore durante la cancellazione del preferito.", lt_sqlcb)
	rollback using lt_sqlcb;
	disconnect using lt_sqlcb;
	return
else
	commit using lt_sqlcb;
end if


dw_menu.setRedraw(false)

dw_menu.deleteRow(ll_riga)

//For ll_i=ll_riga To dw_menu.rowCount()
//	dw_menu.SetItem(ll_i, "ordinamento", (dw_menu.getItemNumber(ll_i, "ordinamento") - 1))
//Next
For ll_i=1 To dw_menu.rowCount()
	dw_menu.SetItem(ll_i, "ordinamento", ll_i)
Next

dw_menu.Sort()

dw_menu.setRedraw(true)

if(dw_menu.RowCount()=0) Then
	st_avvisi.Text="Nessun elemento impostato come preferito.~nPer aggiungere un elemento, trascinarlo dal menù principale al pulsante ~"Preferiti~" in basso."
	dw_menu.Visible=false
	st_avvisi.Visible=true
	disconnect using lt_sqlcb;
	destroy lt_sqlcb;
	return

//Ci sono dei dati
else
	st_avvisi.Visible=false
	dw_menu.Visible=true
end if

// aggiorno indici in tabella
update menu_preferiti
set ordinamento = ordinamento - 1
where ((cod_azienda = :s_cs_xx.cod_azienda) and
		 (cod_utente = :s_cs_xx.cod_utente) and
		 (ordinamento >= :ll_ordinamento))
using lt_sqlcb;

disconnect using lt_sqlcb;
destroy lt_sqlcb;

//if(dw_menu.Update()<1) Then
//	g_mb.messagebox("Menu Preferiti","Errore nella scrittura dei dati:~nImpossibile aggiungere l'elemento ai preferiti.",stopsign!)
//	st_avvisi.Text="Errore nella scrittura dei dati:~nImpossibile aggiungere l'elemento ai preferiti."
//	dw_menu.Visible=false
//	st_avvisi.Visible=true
//else
//	COMMIT using lt_sqlcb;
//	//Non ci sono dati
//	if(dw_menu.RowCount()=0) Then
//		st_avvisi.Text="Nessun elemento impostato come preferito.~nPer aggiungere un elemento, trascinarlo dal menù principale al pulsante ~"Preferiti~" in basso."
//		dw_menu.Visible=false
//		st_avvisi.Visible=true
//		
//	//Ci sono dei dati
//	else
//		st_avvisi.Visible=false
//		dw_menu.Visible=true
//	end if
////end if
end subroutine

public subroutine wf_ridimensiona ();st_avvisi.width = width
st_avvisi.y = (height/2)

dw_menu.width = width
dw_menu.height = height

dw_menu.Modify("nome.width="+String(width - 68))
// -83 perchè ci sono 73 di immagine e 10 arbitrari tra bordo (o non è conteggiato nella larghezza della colonna?) e un po' di margine...
dw_menu.Modify("p_iconafinestra.x="+String(Long(dw_menu.Describe("nome.width")) - 83))
end subroutine

public subroutine wf_riga_editabile ();dw_menu.Object.nome.protect='0';
dw_menu.setRow(w_menu_80.uo_preferiti.dw_menu.getSelectedRow(0))
end subroutine

public subroutine wf_errore (string as_text);/**
 * fabiob
 * 11/01/2012
 *
 * Imposto il messaggio di errore e nascondo le DW
 **/
 
//g_mb.error("Errore nella scrittura dei dati:~nImpossibile aggiungere l'elemento ai preferiti.")
st_avvisi.Text = as_text
dw_menu.Visible = false
st_avvisi.Visible = true
end subroutine

on uo_menu_preferiti.create
this.st_avvisi=create st_avvisi
this.dw_menu=create dw_menu
this.Control[]={this.st_avvisi,&
this.dw_menu}
end on

on uo_menu_preferiti.destroy
destroy(this.st_avvisi)
destroy(this.dw_menu)
end on

event constructor;long ll_retrieve
String ls_error

backColor=il_background_color
st_avvisi.backColor=il_background_color

//Pulsante della DataWindow
dw_menu.object.p_iconafinestra.Filename  = guo_functions.uof_risorse("menu/window.png")

dw_menu.setTransObject(SQLCA);
ll_retrieve=dw_menu.Retrieve(s_cs_xx.cod_azienda, s_cs_xx.cod_utente)

//Errore nella lettura dei dati
if(ll_retrieve<0) Then
	g_mb.messagebox("Menu Preferiti","Errore nella lettura dei dati:~nImpossibile caricare i preferiti.",stopsign!)
	st_avvisi.Text="Errore nella lettura dei dati:~nImpossibile caricare i preferiti."
	dw_menu.Visible=false
	st_avvisi.Visible=true
	return
else
	//Non ci sono dati
	if(ll_retrieve=0) Then
		st_avvisi.Text="Nessun elemento impostato come preferito.~nPer aggiungere un elemento, trascinarlo dal menu principale al pulsante ~"Preferiti~" in basso."
		dw_menu.Visible=false
		st_avvisi.Visible=true
		
	//Ci sono dei dati
	else
		st_avvisi.Visible=false
		dw_menu.Visible=true
	end if
end if

dw_menu.setSort("ordinamento ASC")
dw_menu.Object.nome.protect='1';
im_menu_destro=create m_preferiti;
end event

type st_avvisi from statictext within uo_menu_preferiti
integer y = 804
integer width = 1353
integer height = 256
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_menu from datawindow within uo_menu_preferiti
event ue_keydown pbm_dwnkey
event ue_mousemove pbm_dwnmousemove
event ue_leftbuttonup pbm_dwnlbuttonup
event ue_blocca_nome ( )
boolean visible = false
integer width = 1353
integer height = 1860
integer taborder = 10
string title = "none"
string dataobject = "d_menu_preferiti"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_keydown;if((key=keyDelete!) And (dw_menu.GetRow()>0)) Then
	wf_elimina_preferito()
end if
if((key=keyEnter!) And (dw_menu.GetRow()>0)) Then
	//dw_menu.postevent("itemchanged");
	dw_menu.setcolumn("nome")
	yield();
end if
end event

event ue_mousemove;if(ib_clicked) Then
	if((dw_menu.GetRow())<>row) Then
		drag(Begin!)
	end if
end if
end event

event ue_leftbuttonup;ib_clicked=false
drag(End!)
end event

event ue_blocca_nome();integer li_riga, li_cod_profilo, li_progressivo_menu
String ls_error, ls_nome
Transaction lt_sqlcb;

dw_menu.Object.nome.protect='1';

//Inizializzo la nuova transazione
if(Not (guo_functions.uof_create_transaction_from(sqlca, lt_sqlcb, ls_error))) Then
	g_mb.error("Errore durante la creazione della nuova transazione", lt_sqlcb)
	return
end if

li_riga=dw_menu.getRow()
ls_nome=dw_menu.getItemString(li_riga, "nome")
li_cod_profilo=dw_menu.getItemNumber(li_riga, "menu_preferiti_cod_profilo")
li_progressivo_menu=dw_menu.getItemNumber(li_riga, "menu_preferiti_progressivo_menu")

UPDATE menu_preferiti
	SET nome=:ls_nome
	WHERE ((cod_azienda = :s_cs_xx.cod_azienda) and
			 (cod_utente = :s_cs_xx.cod_utente) and
			 (cod_profilo = :li_cod_profilo) AND
			 (progressivo_menu=:li_progressivo_menu))
	USING lt_sqlcb;

if(lt_sqlcb.SQLCode<0) Then
	g_mb.error("Errore durante la rinominazione del nuovo preferito", lt_sqlcb)
	wf_errore("Errore nella scrittura dei dati:~nImpossibile aggiungere l'elemento ai preferiti.")
	rollback using lt_sqlcb;
else 
	commit using lt_sqlcb;
	st_avvisi.Visible=false
	dw_menu.Visible=true
end if

disconnect using lt_sqlcb;
destroy lt_sqlcb;
end event

event doubleclicked;long ll_elemento

//Evito errori in caso di doppio click con griglia vuota...
if(row<1) Then
	return
end if

if dw_menu.getitemstring(row,"menu_flag_nuovo") = "S" then
	s_cs_xx.flag_nuovo = true
else
	s_cs_xx.flag_nuovo = false
end if

if dw_menu.getitemstring(row,"menu_flag_modifica") = "S" then
	s_cs_xx.flag_modifica = true
else
	s_cs_xx.flag_modifica = false
end if

if dw_menu.getitemstring(row,"menu_flag_cancella") = "S" then
	s_cs_xx.flag_cancella = true
else
	s_cs_xx.flag_cancella = false
end if

window lw_window

window_type_open(lw_window,dw_menu.getitemstring(row,"menu_collegamento"), 6)
if isvalid(w_extra_windows) then
	w_extra_windows.add_window(dw_menu.getitemstring(row,"menu_collegamento"), dw_menu.getitemstring(row,"nome"))
end if

w_menu_80.uo_cronologia.wf_modifica_cronologia(dw_menu.getitemNumber(row, "menu_preferiti_cod_profilo"), dw_menu.getitemNumber(row, "menu_preferiti_progressivo_menu"), dw_menu.getitemstring(row,"menu_descrizione_voce"), dw_menu.getitemstring(row,"menu_collegamento"), dw_menu.getitemString(row, "menu_flag_nuovo"), dw_menu.getitemString(row, "menu_flag_modifica"), dw_menu.getitemString(row, "menu_flag_cancella"))

TIMER(3)
end event

event dragwithin;source.dragicon =guo_functions.uof_risorse("menu\riordina_preferito.ico")
if(row>0) Then
	dw_menu.selectRow(0, false)
	dw_menu.selectRow(row, true)
end if
end event

event clicked;if(dw_menu.Object.nome.protect='0') Then
	if(row<>dw_menu.getSelectedRow(0)) Then
		dw_menu.postevent("itemchanged");
		yield();
	end if
end if

ib_clicked=true

dw_menu.SelectRow(0, false)

if(row>0) Then
	dw_menu.SelectRow(row, true)
	dw_menu.SetRow(row)
end if
end event

event dragdrop;Integer li_cod_profilo, li_progressivo_menu
string ls_error
transaction lt_sqlcb

if(row=0) Then
	return
end if

if(source=this) Then
	integer li_riga, li_ordinamento
	DataWindow dw_provvisoria
	
	dw_provvisoria=dw_menu
	
	if(dw_provvisoria.getRow()=row) Then
		return
	end if
	
	dw_menu.setRedraw(false)
	
	li_ordinamento=dw_menu.getItemNumber(row, "ordinamento")
	
	if(dw_provvisoria.getRow()<row) Then
		For li_riga=(dw_provvisoria.getRow()+1) To row
			dw_menu.SetItem(li_riga, "ordinamento", (dw_menu.getItemNumber(li_riga, "ordinamento") - 1))
		Next
	else
		For li_riga=row To dw_provvisoria.getRow()
			dw_menu.SetItem(li_riga, "ordinamento", (dw_menu.getItemNumber(li_riga, "ordinamento") + 1))
		Next
	end if
	
	dw_menu.SetItem(dw_provvisoria.getRow(), "ordinamento", li_ordinamento)
	
	dw_menu.Sort()
	
	dw_menu.SelectRow(0, false)
	dw_menu.SetRow(row)
	dw_menu.SelectRow(row, true)
	dw_menu.setRedraw(true)
	
	// Creo nuova transazione
	if(Not (guo_functions.uof_create_transaction_from(sqlca, lt_sqlcb, ls_error))) Then
		wf_errore("Errore nella scrittura dei dati:~nImpossibile riordinare l'elemento dei preferiti.")
		g_mb.error(ls_error)
		return
	end if
	
	// aggiorno indici in tabella
	For li_riga=1  To dw_menu.rowcount()
		
		li_cod_profilo = dw_menu.getitemNumber(li_riga, "menu_preferiti_cod_profilo")
		li_progressivo_menu = dw_menu.getitemNumber(li_riga, "menu_preferiti_progressivo_menu")
		
		update menu_preferiti
		set ordinamento = :li_riga
		where ((cod_azienda = :s_cs_xx.cod_azienda) and
				 (cod_utente = :s_cs_xx.cod_utente) and
				 (cod_profilo = :li_cod_profilo) AND
			 	 (progressivo_menu=:li_progressivo_menu))
		using lt_sqlcb;
		
	next
	
	disconnect using lt_sqlcb;
	destroy lt_sqlcb;

	
//	if(dw_menu.Update()<1) Then
//		g_mb.messagebox("Menu Preferiti","Errore nella scrittura dei dati:~nImpossibile riordinare gli elementi.",stopsign!)
//		st_avvisi.Text="Errore nella scrittura dei dati:~nImpossibile aggiungere l'elemento ai preferiti."
//		dw_menu.Visible=false
//		st_avvisi.Visible=true
//	else
//		COMMIT using lt_sqlcb;
//		//Non ci sono dati
//		if(dw_menu.RowCount()=0) Then
//			st_avvisi.Text="Nessun elemento impostato come preferito.~nPer aggiungere un elemento, trascinarlo dal menù principale al pulsante ~"Preferiti~" in basso."
//			dw_menu.Visible=false
//			st_avvisi.Visible=true
//			
//		//Ci sono dei dati
//		else
//			st_avvisi.Visible=false
//			dw_menu.Visible=true
//		end if
//	end if
	
end if
end event

event dragleave;source.dragicon =guo_functions.uof_risorse("10.5\MENU_dragstop.ico")
end event

event rbuttondown;if(row>0) Then
	dw_menu.SelectRow(0, false)
	dw_menu.SelectRow(row, true)
	dw_menu.SetRow(row)
	w_menu_80.ib_blocca_autohide=true
	im_menu_destro.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	w_menu_80.ib_blocca_autohide=false
	TIMER(3)
end if
end event

event itemchanged;postevent("ue_blocca_nome");
return 0
end event

