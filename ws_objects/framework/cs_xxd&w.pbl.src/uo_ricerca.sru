﻿$PBExportHeader$uo_ricerca.sru
forward
global type uo_ricerca from nonvisualobject
end type
end forward

global type uo_ricerca from nonvisualobject
event ue_leggi_finestra_ricerca ( )
end type
global uo_ricerca uo_ricerca

type variables
private:
	// indica se la finestra di ricerca è modale 
	boolean ib_response = false
	
	// Indica se la ricerca è una ricerca di tipo select DISTINCT
	boolean ib_distinct = false
	
	// array dei valori di ritorno nel caso la finestra sia response e non sia stata
	// passata nessuna datawindow
	any ia_last_return_values[]
	
	string is_last_table_query = ""
	
	window iw_parent
	
	string is_forced_where
	
	string is_nome_finestra_ricerca = "w_ricerca"
end variables

forward prototypes
public function boolean uof_ricerca_cliente (ref datawindow adw_datawindow)
public function boolean uof_ricerca_cliente (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_fornitore (ref datawindow adw_datawindow, string as_column_name)
private subroutine uof_ricerca_template (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_attrezzatura (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_contatto (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_banca_clifor (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_aspetto_beni (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_prodotto (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_documenti (ref datawindow adw_datawindow, string as_column_name)
public function boolean uof_set_response ()
private subroutine wf_open_search (str_ricerca astr_data)
public subroutine uof_ricerca_fornitore_potenziale (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_deposito (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_banca (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_abicab (ref datawindow adw_datawindow, string as_column_name_abi, string as_column_name_cab)
public subroutine uof_ricerca_agente (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_commessa (ref datawindow adw_datawindow, string as_column_name_anno_commessa, string as_column_name_num_commessa)
public subroutine uof_ricerca_divisione (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_formule_calcolo (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_mansionario (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_area_aziendale (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_reparto (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_operaio (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_set_where (string as_where)
public subroutine uof_set_distinct (boolean ab_distinct)
private subroutine uof_clear_var ()
public subroutine uof_ricerca_utente (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_operatore (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_azienda (ref datawindow adw_datawindow, string as_column_name)
public function string uof_get_last_table_name ()
public subroutine uof_get_last_results (ref any aa_results[])
public function boolean uof_get_results (ref any aa_results[])
public subroutine uof_ricerca_colore_tessuto (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_set_parent (window aw_parent_window)
public subroutine uof_get_parent (ref window aw_parent_window)
public function boolean uof_set_response (window aw_window_parent)
public subroutine uof_ricerca_formule_db (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_richiedente (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_variabili_formule (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_nomenclature (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_tipo_manutenzione (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_cat_attrezzature (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_giro_consegna (ref datawindow adw_datawindow, string as_column_name)
public subroutine uof_ricerca_aspetto_beni (ref datawindow adw_datawindow, string as_column_name, boolean ab_show_descrpition)
end prototypes

event ue_leggi_finestra_ricerca();
string		ls_NFR

select stringa
into :ls_NFR
from parametri
where cod_parametro='NFR' and flag_parametro='S';

if ls_NFR<>"" and not isnull(ls_NFR) then
	is_nome_finestra_ricerca = ls_NFR
end if
end event

public function boolean uof_ricerca_cliente (ref datawindow adw_datawindow);return uof_ricerca_cliente(adw_datawindow, "cod_cliente")
end function

public function boolean uof_ricerca_cliente (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_cliente"

lstr_ricerca.table_name = "anag_clienti"
lstr_ricerca.window_title = "Ricerca Clienti"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_cliente"}

lstr_ricerca.column_default = {"cod_cliente", "rag_soc_1"}
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)

return true
end function

public subroutine uof_ricerca_fornitore (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_fornitore"

lstr_ricerca.table_name = "anag_fornitori"
lstr_ricerca.window_title = "Ricerca Fornitori"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_fornitore"}

lstr_ricerca.column_default = {"cod_fornitore", "rag_soc_1", "rag_soc_2" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

private subroutine uof_ricerca_template (ref datawindow adw_datawindow, string as_column_name);/**
 * stefanop
 * 20/09/2011
 *
 * Funzione di tempate per usare a pieno il processo di ricerca
 **/

str_ricerca lstr_ricerca

// FILTRI
// array delle colonne che sono obbligatorie, cioè che l'utente non può deselezionare nella
// personalizzazione della datawindow
lstr_ricerca.column_fixed = { "cod_cliente" , "rag_soc_1"} 

// array delle colonne di default da usare quando un utente apre la finestra e non ha ancora personalizzato la vista
lstr_ricerca.column_default = {"cod_cliente", "rag_soc_1"}

// tabella principale dove filtrare i dati
lstr_ricerca.table_name = "anag_clienti"

// titolo della finestra della ricerca
lstr_ricerca.window_title = "Ricerca Clienti" 

// datawindow dove andranno impostati i valori di ritorno, DEVE essere passata by ref alla funzione
lstr_ricerca.dw = adw_datawindow

// [OPZIONALE] Indica se la query di selezione è DISTINCT o meno
ib_distinct = true

// array con il nome delle colonne della datawindow di ritorno dove saranno impostati i valori selezionati.
// I valori selezionati vengono presi dalle colonne dw della finestra di ricerca devono essere codificate nel secondo array.
// I due array sono stati fatti perchè può capitare che i due campi non combaciano; ad esempio il "rs_cod_cliente" della dw di ricerca 
// corrisponde alla colonna cod_cliente del DB.
lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_cliente"}

// -- OPZIONALE -------------------------------------------
// Inizializzazione della struttura JOIN
str_ricerca_join lstr_join

// nome della tabella in join
lstr_join.table = "anag_prodotti"

// array delle colonne da visualizzare presenti nella tabella di join
lstr_join.columns = {"des_prodotto"}

// where di join che verrà utilizzata per la join. La join sarà sempre LEFT OUTER JOIN
lstr_join.where = "anag_prodotti.cod_prodotto = anag_commesse.cod_prodotto AND anag_prodotti.cod_azienda = anag_commesse.cod_azienda" 

// array delle tabelle di join, in questo modo posso avere più join nella stessa finestra
lstr_ricerca.joins = {lstr_join}
// -- FINE OPZIONALE  ------------------------------------

// apre la finestra di ricerca e passa la struttura per inizializzarla correttamente 
wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_attrezzatura (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_fixed = {"cod_attrezzatura"}
lstr_ricerca.column_read = {"cod_attrezzatura"}
lstr_ricerca.column_default = {"cod_attrezzatura", "descrizione" }

lstr_ricerca.table_name = "anag_attrezzature"
lstr_ricerca.window_title = "Ricerca Attrezzatura"
lstr_ricerca.dw = adw_datawindow
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_contatto (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_contatto"

lstr_ricerca.table_name = "anag_contatti"
lstr_ricerca.window_title = "Ricerca Contatti"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_contatto"}

lstr_ricerca.column_default = {"cod_contatto", "rag_soc_1", "rag_soc_2" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_banca_clifor (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_banca_clien_for"

lstr_ricerca.table_name = "anag_banche_clien_for"
lstr_ricerca.window_title = "Ricerca Banche Clienti Fornitori"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_banca_clien_for"}

lstr_ricerca.column_default = {"cod_banca_clien_for", "des_banca" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_aspetto_beni (ref datawindow adw_datawindow, string as_column_name);uof_ricerca_aspetto_beni(adw_datawindow, as_column_name, false)
end subroutine

public subroutine uof_ricerca_prodotto (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_prodotto"

lstr_ricerca.table_name = "anag_prodotti"
lstr_ricerca.window_title = "Ricerca Prodotti"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_prodotto"}

lstr_ricerca.column_default = {"cod_prodotto", "des_prodotto" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_documenti (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed = {"anno_registrazione", "num_registrazione", "des_elemento"}

lstr_ricerca.table_name = "tes_documenti"
lstr_ricerca.window_title = "Ricerca Documenti ISO 9000"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {"anno_registrazione", "num_registrazione", "des_elemento"}
lstr_ricerca.column_read = {"anno_registrazione", "num_registrazione", "des_elemento"}

lstr_ricerca.column_default = {"anno_registrazione", "num_registrazione", "des_elemento"}
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public function boolean uof_set_response ();/**
 * stefanop
 * 10/10/2011
 *
 * Imposta la modalità di apertura della finestra di ricerca in response.
 * Alla chiusura della finestra la variabile torna al valore di default (false)
 **/
 
this.ib_response = true
return this.ib_response
end function

private subroutine wf_open_search (str_ricerca astr_data);/**
 * stefanop
 * 11/10/2011
 *
 * Centralizzo l'apertura delle finestre di ricerca
 **/

window lw_parent, lw_search_window
string ls_response_search_window

astr_data.distinct = ib_distinct
astr_data.forced_where = is_forced_where

uof_get_parent(lw_parent)

//esempio di gestione personalizzata per cliente della finestra di ricerca
//  inserire un parametro multiaziendale NFR che identifica il nome della finestra di ricerca
//  se tale parametro non esiste allora prende il,valore standard di default:   is_nome_finestra_ricerca = "w_ricerca"
//  per aprire quella response verrà accodato al nome la stringa "_response"

if not ib_response then
	//openwithparm(w_ricerca, astr_data, lw_parent)
	openwithparm(lw_search_window, astr_data, is_nome_finestra_ricerca,  lw_parent)
else
	//openwithparm(w_ricerca_response, astr_data, lw_parent)
	
	ls_response_search_window = is_nome_finestra_ricerca + "_response"
	openwithparm(lw_search_window, astr_data, ls_response_search_window,  lw_parent)
	
	// se non ho passato la dw allora mi aspetto dei valori di ritorno all'intreno della struttura
	if isnull(astr_data.dw) then
		str_ricerca lstr_data
		lstr_data = Message.powerobjectparm
		
		setnull(Message.powerobjectparm)
		
		try
			if not isnull(lstr_data) then
				if not isnull(lstr_data.return_values) then
					ia_last_return_values = lstr_data.return_values
				end if
			end if
		catch(RuntimeError ex)
			// ho chiuso la finestra tramite la X.
			// non posso fare ne isNUll e ne isValid perchè sulle strutture non vanno
			// GRAZIE PB
		end try
		// ---
		
		// è possibile recuperare i valori di ritorno con la funzione
		// uof_get_results(any_array[])
	end if
end if

// pulisco le variabili di istanza
uof_clear_var()
end subroutine

public subroutine uof_ricerca_fornitore_potenziale (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_for_pot"

lstr_ricerca.table_name = "anag_for_pot"
lstr_ricerca.window_title = "Ricerca Fornitori Potenziali"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_for_pot"}

lstr_ricerca.column_default = {"cod_for_pot", "rag_soc_1", "rag_soc_2" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_deposito (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_deposito"

lstr_ricerca.table_name = "anag_depositi"
lstr_ricerca.window_title = "Ricerca Depositi"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_deposito"}

lstr_ricerca.column_default = {"cod_deposito", "des_deposito" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_banca (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_banca"

lstr_ricerca.table_name = "anag_banche"
lstr_ricerca.window_title = "Ricerca Banche"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_banca"}

lstr_ricerca.column_default = {"cod_banca", "des_banca" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_abicab (ref datawindow adw_datawindow, string as_column_name_abi, string as_column_name_cab);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed = {"cod_abi", "cod_cab"}

lstr_ricerca.table_name = "tab_abicab"
lstr_ricerca.window_title = "Ricerca Abi Cab"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name_abi, as_column_name_cab}
lstr_ricerca.column_read = {"cod_abi", "cod_cab"}

lstr_ricerca.column_default = {"cod_abi", "cod_cab", "indirizzo", "localita" }

lstr_ricerca.evidenzia_bloccati=false

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_agente (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_agente"

lstr_ricerca.table_name = "anag_agenti"
lstr_ricerca.window_title = "Ricerca Agenti"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_agente"}

lstr_ricerca.column_default = {"cod_agente", "rag_soc_1" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_commessa (ref datawindow adw_datawindow, string as_column_name_anno_commessa, string as_column_name_num_commessa);str_ricerca lstr_ricerca
str_ricerca_join lstr_join

lstr_ricerca.column_fixed = {"anno_commessa", "num_commessa"}

lstr_ricerca.table_name = "anag_commesse"
lstr_ricerca.window_title = "Ricerca Commesse"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name_anno_commessa, as_column_name_num_commessa}
lstr_ricerca.column_read = {"anno_commessa", "num_commessa"}

lstr_ricerca.column_default = {"anno_commessa", "num_commessa", "cod_prodotto"}

// join
lstr_join.table = "anag_prodotti"
lstr_join.columns = {"des_prodotto"}
lstr_join.where = "anag_prodotti.cod_prodotto = anag_commesse.cod_prodotto AND anag_prodotti.cod_azienda = anag_commesse.cod_azienda"
lstr_ricerca.joins = {lstr_join}
lstr_ricerca.evidenzia_bloccati=true


wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_divisione (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_divisione"

lstr_ricerca.table_name = "anag_divisioni"
lstr_ricerca.window_title = "Ricerca Divisioni"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_divisione"}

lstr_ricerca.column_default = {"cod_divisione", "des_divisione" }
lstr_ricerca.evidenzia_bloccati=false

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_formule_calcolo (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed = {"cod_formula"}

lstr_ricerca.table_name = "tab_formule_calcolo"
lstr_ricerca.window_title = "Ricerca Formule Calcolo"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_formula"}

lstr_ricerca.column_default = {"cod_formula", "des_formula" }
lstr_ricerca.evidenzia_bloccati=false

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_mansionario (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed = {"cod_resp_divisione"}

lstr_ricerca.table_name = "mansionari"
lstr_ricerca.window_title = "Ricerca Mansionari"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_resp_divisione"}

lstr_ricerca.column_default = {"cod_resp_divisione", "nome", "cognome" }
lstr_ricerca.evidenzia_bloccati=false

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_area_aziendale (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca
str_ricerca_join lstr_join

lstr_ricerca.column_fixed = {"cod_area_aziendale"}

lstr_ricerca.table_name = "tab_aree_aziendali"
lstr_ricerca.window_title = "Ricerca Aree Aziendali"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_area_aziendale"}

lstr_ricerca.column_default = {"cod_area_aziendale", "des_area" }

lstr_join.table = "anag_divisioni"
lstr_join.columns = {"des_divisione"}
lstr_join.where = "anag_divisioni.cod_azienda=tab_aree_aziendali.cod_azienda AND anag_divisioni.cod_divisione=tab_aree_aziendali.cod_divisione"

lstr_ricerca.joins = {lstr_join}
lstr_ricerca.evidenzia_bloccati = false


wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_reparto (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed = {"cod_reparto"}

lstr_ricerca.table_name = "anag_reparti"
lstr_ricerca.window_title = "Ricerca Reparti"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_reparto"}

lstr_ricerca.column_default = {"cod_reparto", "des_reparto" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_operaio (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_operaio"

lstr_ricerca.table_name = "anag_operai"
lstr_ricerca.window_title = "Ricerca Operai"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_operaio"}

lstr_ricerca.column_default = {"cod_operaio", "nome", "cognome" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_set_where (string as_where);/**
 * stefanop
 * 27/10/11
 *
 * Passo una query da aggiungere alla finestra di ricerca
 **/
 
is_forced_where = as_where
end subroutine

public subroutine uof_set_distinct (boolean ab_distinct);this.ib_distinct = ab_distinct
end subroutine

private subroutine uof_clear_var ();/**
 * stefanop
 * 27/10/2011
 *
 * Funzione che si occupa di pulire tutte le variabili di istanza
 **/
 
ib_distinct = false
ib_response = false

setnull(iw_parent)
setnull(is_forced_where)
end subroutine

public subroutine uof_ricerca_utente (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed = {"cod_utente"}

lstr_ricerca.table_name = "utenti"
lstr_ricerca.window_title = "Ricerca Utenti"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_utente"}

lstr_ricerca.column_default = {"cod_utente", "username", "nome_cognome" }
lstr_ricerca.column_hidden = {"password"}
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_operatore (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed = {"cod_operatore"}

lstr_ricerca.table_name = "tab_operatori"
lstr_ricerca.window_title = "Ricerca Operatori"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_operatore"}

lstr_ricerca.column_default = {"cod_operatore", "des_operatore" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_azienda (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca
str_ricerca_join lstr_join

lstr_ricerca.column_fixed = {"cod_azienda"}

lstr_ricerca.table_name = "aziende"
lstr_ricerca.window_title = "Ricerca Azienda"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_azienda"}

lstr_ricerca.column_default = {"cod_azienda", "rag_soc_1" }
lstr_ricerca.evidenzia_bloccati=true


wf_open_search(lstr_ricerca)
end subroutine

public function string uof_get_last_table_name ();/**
 * stefanop
 * 19/01/2012
 *
 * Ritorno il nome dell'ultima tabella aperta nella ricerca
 **/
 
return is_last_table_query
end function

public subroutine uof_get_last_results (ref any aa_results[]);/**
 * stefanop
 * 12/10/2011
 **/
 
aa_results = ia_last_return_values


end subroutine

public function boolean uof_get_results (ref any aa_results[]);/**
 * stefanop
 * 12/10/2011
 *
 * Nel caso abbia aperto la ricerca in modalità response e non abbia definito la datawindow
 * il programma mi ritorna i valori selezionati in un array di any.
 * La funzione ritorna all'utente i valori per poterli elaborare e li cancella dalla cache.
 **/
 
any la_empty[]

aa_results = ia_last_return_values

// cancello cache
ia_last_return_values = la_empty

return upperbound(aa_results) > 0


end function

public subroutine uof_ricerca_colore_tessuto (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_colore_tessuto"

lstr_ricerca.table_name = "tab_colori_tessuti"
lstr_ricerca.window_title = "Ricerca Colori"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_colore_tessuto"}

lstr_ricerca.column_default = {"cod_colore_tessuto", "des_colore_tessuto" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_set_parent (window aw_parent_window);this.iw_parent = aw_parent_window
end subroutine

public subroutine uof_get_parent (ref window aw_parent_window);
if isvalid(iw_parent) then
	if not isnull(iw_parent) then
		aw_parent_window = this.iw_parent
		return
	end if
end if

aw_parent_window = w_cs_xx_mdi
end subroutine

public function boolean uof_set_response (window aw_window_parent);/**
 * stefanop
 * 10/10/2011
 *
 * Imposta la modalità di apertura della finestra di ricerca in response.
 * Alla chiusura della finestra la variabile torna al valore di default (false)
 **/
 
uof_set_parent(aw_window_parent)


return uof_set_response()
end function

public subroutine uof_ricerca_formule_db (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_formula"

lstr_ricerca.table_name = "tab_formule_db"
lstr_ricerca.window_title = "Ricerca formule"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_formula"}

lstr_ricerca.column_default = {"cod_formula", "des_formula" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_richiedente (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_fixed = {"cod_richiedente"}
lstr_ricerca.column_read = {"cod_richiedente"}
lstr_ricerca.column_default = {"cod_richiedente", "nominativo" }

lstr_ricerca.table_name = "tab_richiedenti"
lstr_ricerca.window_title = "Ricerca Richiedente"
lstr_ricerca.dw = adw_datawindow
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_variabili_formule (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_variabile"

lstr_ricerca.table_name = "tab_variabili_formule"
lstr_ricerca.window_title = "Ricerca Variabili"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_variabile"}

lstr_ricerca.column_default = {"cod_variabile", "nome_campo_database" }
lstr_ricerca.evidenzia_bloccati=false

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_nomenclature (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_fixed = {"cod_nomenclatura"}
lstr_ricerca.column_read = {"cod_nomenclatura"}
lstr_ricerca.column_default = {"cod_nomenclatura", "des_nomenclatura" }

lstr_ricerca.table_name = "tab_nomenclature"
lstr_ricerca.window_title = "Ricerca Nomenclature"
lstr_ricerca.dw = adw_datawindow
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_tipo_manutenzione (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_fixed = {"cod_tipo_manutenzione"}
lstr_ricerca.column_read = {"cod_tipo_manutenzione"}
lstr_ricerca.column_default = {"cod_tipo_manutenzione", "cod_attrezzatura", "des_tipo_manutenzione" }

lstr_ricerca.table_name = "tab_tipi_manutenzione"
lstr_ricerca.window_title = "Ricerca Tipo Manutenzione"
lstr_ricerca.dw = adw_datawindow
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_cat_attrezzature (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_fixed = {"cod_cat_attrezzature"}
lstr_ricerca.column_read = {"cod_cat_attrezzature"}
lstr_ricerca.column_default = {"cod_cat_attrezzature", "des_cat_attrezzature" }

lstr_ricerca.table_name = "tab_cat_attrezzature"
lstr_ricerca.window_title = "Ricerca Categoria Attrezzatura"
lstr_ricerca.dw = adw_datawindow
lstr_ricerca.evidenzia_bloccati=false

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_giro_consegna (ref datawindow adw_datawindow, string as_column_name);str_ricerca lstr_ricerca

lstr_ricerca.column_fixed[1] = "cod_giro_consegna"

lstr_ricerca.table_name = "tes_giri_consegne"
lstr_ricerca.window_title = "Ricerca Giri Consegna"
lstr_ricerca.dw = adw_datawindow

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_read = {"cod_giro_consegna"}

lstr_ricerca.column_default = {"cod_giro_consegna", "des_giro_consegna", "cod_deposito" }
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

public subroutine uof_ricerca_aspetto_beni (ref datawindow adw_datawindow, string as_column_name, boolean ab_show_descrpition);str_ricerca lstr_ricerca

lstr_ricerca.dw_search_columns = {as_column_name}
lstr_ricerca.column_fixed = {"cod_imballo"}

if ab_show_descrpition then
	lstr_ricerca.column_read = {"des_imballo"}
else
	lstr_ricerca.column_read = {"cod_imballo"}
end if

lstr_ricerca.column_default = {"cod_imballo", "des_imballo" }

lstr_ricerca.table_name = "tab_imballi"
lstr_ricerca.window_title = "Ricerca Aspetto Beni"
lstr_ricerca.dw = adw_datawindow
lstr_ricerca.evidenzia_bloccati=true

wf_open_search(lstr_ricerca)
end subroutine

on uo_ricerca.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ricerca.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;postevent("ue_leggi_finestra_ricerca")
end event

