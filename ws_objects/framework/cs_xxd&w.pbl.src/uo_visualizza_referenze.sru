﻿$PBExportHeader$uo_visualizza_referenze.sru
forward
global type uo_visualizza_referenze from nonvisualobject
end type
end forward

global type uo_visualizza_referenze from nonvisualobject
end type
global uo_visualizza_referenze uo_visualizza_referenze

type variables

end variables

forward prototypes
public subroutine uof_referenze_prodotto (string as_cod_prodotto)
end prototypes

public subroutine uof_referenze_prodotto (string as_cod_prodotto);str_referenze lstr_data
str_referenza_item lstr_i
int li_i

lstr_data.windowName = "Referenze del prodotto " + as_cod_prodotto
li_i = 1

lstr_i.table = "anag_prodotti"
lstr_i.where = "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + as_cod_prodotto + "' and cod_prodotto <> '" + as_cod_prodotto + "' "
lstr_i.colonne = {"cod_prodotto", "des_prodotto"}
lstr_i.groupName = "Prodotti"
lstr_i.itemName = "Prodotto %s %s"
lstr_i.windowname = "w_prodotti_tv"

lstr_data.tabelle[li_i] = lstr_i
li_i++

lstr_i.table = "stock"
lstr_i.where = "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + as_cod_prodotto + "'"
lstr_i.colonne = {"cod_prodotto", "cod_deposito", "cod_ubicazione", "cod_lotto"}
lstr_i.itemName = "Stock: %s - Dep: %s - Ubi: %s - Lotto: %s"
lstr_i.groupName = "Stock"
lstr_i.windowname = "w_stock"
lstr_data.tabelle[li_i] = lstr_i
li_i++

// Distinta
lstr_i.table = "distinta_padri"
lstr_i.where = "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + as_cod_prodotto + "'"
lstr_i.colonne = {"cod_prodotto", "cod_versione"}
lstr_i.itemName = "Distinta: %s - Versione: %s"
lstr_i.groupname = "Distinta Padri"
lstr_i.windowname = "w_distinta_padri"
lstr_data.tabelle[li_i] = lstr_i
li_i++

lstr_i.table = "distinta"
lstr_i.where = "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto_padre='" + as_cod_prodotto + "' OR cod_prodotto_figlio='" + as_cod_prodotto +"' "
lstr_i.colonne = {"cod_prodotto_padre"}
lstr_i.itemName = "Ramo Prodotto Padre: %s"
lstr_i.groupname = "Distinta"
lstr_i.windowname = "w_distinta_padri"
lstr_data.tabelle[li_i] = lstr_i
li_i++

// Ordini
lstr_i.table = "det_ord_ven"
lstr_i.where = "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + as_cod_prodotto + "'"
lstr_i.colonne = {"anno_registrazione", "num_registrazione", "prog_riga_ord_ven"}
lstr_i.itemName = "Ordine: %d/%d/%d"
lstr_i.groupName = "Ordini di vendita"
lstr_i.windowname = "w_tes_ord_ven"
lstr_data.tabelle[li_i] = lstr_i
li_i++

lstr_i.table = "det_ord_acq"
lstr_i.where = "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + as_cod_prodotto + "'"
lstr_i.colonne = {"anno_registrazione", "num_registrazione", "prog_riga_ord_acq"}
lstr_i.itemName = "Ordine: %d/%d/%d"
lstr_i.groupname = "Ordini di Acquisto"
lstr_i.windowname = "w_tes_ord_acq"
lstr_data.tabelle[li_i] = lstr_i
li_i++

// Bolle
lstr_i.table = "det_bol_ven"
lstr_i.where = "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + as_cod_prodotto + "'"
lstr_i.colonne = {"anno_registrazione", "num_registrazione", "prog_riga_bol_ven"}
lstr_i.itemName = "Bolla: %d/%d/%d"
lstr_i.groupName = "Bolla di Vendita"
lstr_data.tabelle[li_i] = lstr_i
li_i++

lstr_i.table = "det_bol_acq"
lstr_i.where = "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + as_cod_prodotto + "'"
lstr_i.colonne = {"anno_registrazione", "num_registrazione", "prog_riga_bolla_acq"}
lstr_i.itemName = "Bolla: %d/%d/%d"
lstr_i.groupName = "Bolla di Acquisto"
lstr_data.tabelle[li_i] = lstr_i
li_i++

// Fatture
lstr_i.table = "det_fat_ven"
lstr_i.where = "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + as_cod_prodotto + "'"
lstr_i.colonne = {"anno_registrazione", "num_registrazione", "prog_riga_fat_ven"}
lstr_i.itemName = "Fattura: %d/%d/%d"
lstr_i.groupName = "Fatture di Vendita"
lstr_data.tabelle[li_i] = lstr_i
li_i++

lstr_i.table = "det_fat_acq"
lstr_i.where = "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + as_cod_prodotto + "'"
lstr_i.colonne = {"anno_registrazione", "num_registrazione", "prog_riga_fat_acq"}
lstr_i.itemName = "Fattura: %d/%d/%d"
lstr_i.groupName = "Fatture di Acquisto"
lstr_data.tabelle[li_i] = lstr_i
li_i++

openWithParm(w_visualizza_referenze, lstr_data, w_cs_xx_mdi)
end subroutine

on uo_visualizza_referenze.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_visualizza_referenze.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

