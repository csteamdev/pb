﻿$PBExportHeader$w_about.srw
$PBExportComments$Finestra Informazioni su
forward
global type w_about from window
end type
type lb_files from listbox within w_about
end type
type st_applicazione_versione from statictext within w_about
end type
type shl_1 from statichyperlink within w_about
end type
type st_patch from statictext within w_about
end type
type st_release from statictext within w_about
end type
type st_path_l from statictext within w_about
end type
type st_release_l from statictext within w_about
end type
type st_num_serie from statictext within w_about
end type
type st_num_serie_l from statictext within w_about
end type
type st_2 from statictext within w_about
end type
type st_copyright from statictext within w_about
end type
type st_applicazione_nome from statictext within w_about
end type
type cb_ok from commandbutton within w_about
end type
type p_applicazione_bmp from picture within w_about
end type
type r_1 from rectangle within w_about
end type
type ln_1 from line within w_about
end type
end forward

global type w_about from window
integer x = 667
integer y = 520
integer width = 1989
integer height = 1068
boolean titlebar = true
string title = "Informazioni su"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 67108864
boolean center = true
event po_position pbm_custom75
event ue_get_last_modified ( )
lb_files lb_files
st_applicazione_versione st_applicazione_versione
shl_1 shl_1
st_patch st_patch
st_release st_release
st_path_l st_path_l
st_release_l st_release_l
st_num_serie st_num_serie
st_num_serie_l st_num_serie_l
st_2 st_2
st_copyright st_copyright
st_applicazione_nome st_applicazione_nome
cb_ok cb_ok
p_applicazione_bmp p_applicazione_bmp
r_1 r_1
ln_1 ln_1
end type
global w_about w_about

type variables

end variables

forward prototypes
public subroutine wf_get_last_modified ()
end prototypes

event ue_get_last_modified();wf_get_last_modified()
end event

public subroutine wf_get_last_modified ();/**
 * stefanop
 * 02/07/2012
 *
 * Recupero la data di ultima modifica dei file 
 **/
 
string ls_file_date, ls_file, ls_cd
long li_count, ll_i
datetime ldt_date, ldt_file_date, ldt_csteam_date

ls_cd = GetCurrentDirectory() + "\"
ldt_date = datetime("01-01-1900 00:00:00")
ldt_csteam_date = ldt_date

lb_files.dirlist(ls_cd, 0)
li_count = lb_files.TotalItems()

for ll_i = 1 to li_count
	
	lb_files.SelectItem(ll_i)
	ls_file = lb_files.SelectedItem()
	
	ls_file_date = guo_functions.uof_get_file_attribute(ls_cd + ls_file, 3)
	
	// non ho la data a disposizione, salto
	if isnull(ls_file_date) or ls_file_date = "" then continue
	
	ldt_file_date = datetime(ls_file_date)
	
	if ldt_date < ldt_file_date then
		ldt_date = ldt_file_date
	end if
	
	if ls_file = "cs_team.exe" then
		ldt_csteam_date = ldt_file_date
	end if
	
next


st_release.text = string(ldt_csteam_date, "dd/mm/yyyy")
st_patch.text = string(ldt_date, "dd/mm/yyyy")
 
end subroutine

event open;string ls_num_serie

lb_files.visible = false

p_applicazione_bmp.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\logo_cs_128_about.jpg"
st_copyright.text  = "Copyright © 1995-" + string(today(), "yyyy") + " Consulting&&Software"

title = w_pomanager_std.parm.application_name
st_applicazione_nome.text = title
st_applicazione_versione.text = guo_application.uof_versione() //w_pomanager_std.parm.application_rev

// stefanop 16/07/2008
select num_serie,
into :ls_num_serie
from registrazione;

st_num_serie.text = ls_num_serie

if right(ls_num_serie, 3) <> "AAA" then
	//st_3.text = "Questo prodotto è una DEMO concessa in licenza a:"
end if

// stefanop 13/09/2010
st_release.text = "calcolo in corso..."
st_patch.text = "calcolo in corso..."

event post ue_get_last_modified()
end event

on w_about.create
this.lb_files=create lb_files
this.st_applicazione_versione=create st_applicazione_versione
this.shl_1=create shl_1
this.st_patch=create st_patch
this.st_release=create st_release
this.st_path_l=create st_path_l
this.st_release_l=create st_release_l
this.st_num_serie=create st_num_serie
this.st_num_serie_l=create st_num_serie_l
this.st_2=create st_2
this.st_copyright=create st_copyright
this.st_applicazione_nome=create st_applicazione_nome
this.cb_ok=create cb_ok
this.p_applicazione_bmp=create p_applicazione_bmp
this.r_1=create r_1
this.ln_1=create ln_1
this.Control[]={this.lb_files,&
this.st_applicazione_versione,&
this.shl_1,&
this.st_patch,&
this.st_release,&
this.st_path_l,&
this.st_release_l,&
this.st_num_serie,&
this.st_num_serie_l,&
this.st_2,&
this.st_copyright,&
this.st_applicazione_nome,&
this.cb_ok,&
this.p_applicazione_bmp,&
this.r_1,&
this.ln_1}
end on

on w_about.destroy
destroy(this.lb_files)
destroy(this.st_applicazione_versione)
destroy(this.shl_1)
destroy(this.st_patch)
destroy(this.st_release)
destroy(this.st_path_l)
destroy(this.st_release_l)
destroy(this.st_num_serie)
destroy(this.st_num_serie_l)
destroy(this.st_2)
destroy(this.st_copyright)
destroy(this.st_applicazione_nome)
destroy(this.cb_ok)
destroy(this.p_applicazione_bmp)
destroy(this.r_1)
destroy(this.ln_1)
end on

type lb_files from listbox within w_about
boolean visible = false
integer x = 937
integer y = 760
integer width = 480
integer height = 400
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_applicazione_versione from statictext within w_about
integer x = 663
integer y = 120
integer width = 1257
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean enabled = false
string text = "Versione"
boolean focusrectangle = false
end type

type shl_1 from statichyperlink within w_about
integer x = 46
integer y = 880
integer width = 434
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 134217734
long backcolor = 67108864
string text = "www.csteam.com"
boolean focusrectangle = false
string url = "http://www.csteam.com"
end type

type st_patch from statictext within w_about
integer x = 663
integer y = 740
integer width = 297
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "n.d."
boolean focusrectangle = false
end type

type st_release from statictext within w_about
integer x = 663
integer y = 680
integer width = 297
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "n.d."
boolean focusrectangle = false
end type

type st_path_l from statictext within w_about
integer x = 229
integer y = 740
integer width = 398
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Data ultima patch:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_release_l from statictext within w_about
integer x = 229
integer y = 680
integer width = 398
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Data rilascio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_num_serie from statictext within w_about
integer x = 663
integer y = 620
integer width = 800
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
boolean focusrectangle = false
end type

type st_num_serie_l from statictext within w_about
integer x = 229
integer y = 620
integer width = 398
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Numero di Serie:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_about
integer x = 663
integer y = 280
integer width = 1257
integer height = 240
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean enabled = false
string text = "Il presente programma è tutelato dalle leggi sul copyright © e dai trattati internazionali. La riproduzione o la distribuzione non autorizzata di questo programma, o di parte di esso, sarà perseguibile civilmente e penalmente."
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_copyright from statictext within w_about
integer x = 663
integer y = 220
integer width = 1257
integer height = 80
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean enabled = false
string text = "Copyright © 1995-2009 Consulting&Software"
boolean focusrectangle = false
end type

type st_applicazione_nome from statictext within w_about
integer x = 663
integer y = 40
integer width = 1257
integer height = 80
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean enabled = false
string text = "Applicazione Nome"
boolean focusrectangle = false
end type

type cb_ok from commandbutton within w_about
integer x = 1486
integer y = 840
integer width = 434
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
boolean default = true
end type

on clicked;close(parent)
end on

type p_applicazione_bmp from picture within w_about
integer x = 23
integer y = 20
integer width = 585
integer height = 512
boolean enabled = false
boolean originalsize = true
string picturename = "C:\cs_115\framework\risorse\11.5\logo_cs_128_about.jpg"
boolean focusrectangle = false
end type

type r_1 from rectangle within w_about
long linecolor = 16777215
integer linethickness = 4
long fillcolor = 1073741824
integer width = 2331
integer height = 580
end type

type ln_1 from line within w_about
long linecolor = 268435456
integer linethickness = 4
integer beginy = 580
integer endx = 1989
integer endy = 580
end type

