﻿$PBExportHeader$w_anteprima.srw
$PBExportComments$Finestra Anteprima di Stampa
forward
global type w_anteprima from w_cs_xx_principale
end type
type pb_print_current from picturebutton within w_anteprima
end type
type r_1 from rectangle within w_anteprima
end type
type st_1 from statictext within w_anteprima
end type
type ddlb_zoom from dropdownlistbox within w_anteprima
end type
type pb_vertical from picturebutton within w_anteprima
end type
type pb_horizontal from picturebutton within w_anteprima
end type
type st_pages from statictext within w_anteprima
end type
type em_current_page from editmask within w_anteprima
end type
type pb_imp_pagina from picturebutton within w_anteprima
end type
type pb_pag_suc from picturebutton within w_anteprima
end type
type pb_pag_prec from picturebutton within w_anteprima
end type
type pb_printsetup from picturebutton within w_anteprima
end type
type pb_stampa from picturebutton within w_anteprima
end type
type dw_anteprima from datawindow within w_anteprima
end type
type s_parms from structure within w_anteprima
end type
end forward

type s_parms from structure
    window mdi_frame
    integer update_seconds
    boolean show_clock
    boolean show_resources
end type

global type w_anteprima from w_cs_xx_principale
integer width = 3758
integer height = 2324
string title = ""
windowtype windowtype = popup!
windowstate windowstate = maximized!
boolean clientedge = true
boolean center = true
pb_print_current pb_print_current
r_1 r_1
st_1 st_1
ddlb_zoom ddlb_zoom
pb_vertical pb_vertical
pb_horizontal pb_horizontal
st_pages st_pages
em_current_page em_current_page
pb_imp_pagina pb_imp_pagina
pb_pag_suc pb_pag_suc
pb_pag_prec pb_pag_prec
pb_printsetup pb_printsetup
pb_stampa pb_stampa
dw_anteprima dw_anteprima
end type
global w_anteprima w_anteprima

type variables
private:
	boolean ib_zoom
	string is_attributi[]
	datawindow idw_datawindow
	
	int ii_current_page = 1
	int ii_page_count = -1
	
	constant int LANDSCAPE = 1
	constant int PORTRAIT = 2

	string is_w_title
end variables

forward prototypes
public subroutine imp_attributi ()
public subroutine wf_zoom (integer ai_zoom_level)
public subroutine wf_toggle_rules ()
private subroutine wf_set_page (integer ai_current_row)
public subroutine wf_scroll_to_page (integer ai_scroll_page)
public function integer wf_page_count ()
public subroutine wf_set_orientation (integer ai_orientation)
public function string wf_printername ()
public function boolean wf_print (integer ai_page, boolean ab_canceldialog, boolean ab_showprintdialog)
public subroutine wf_set_title ()
public subroutine wf_print_shortcut (keycode key, unsignedlong keyflags)
end prototypes

public subroutine imp_attributi ();long ll_i, ll_num_attributi
string ls_modify, ls_valore


ll_num_attributi = upperbound(is_attributi)
for ll_i = 1 to ll_num_attributi
	ls_valore = idw_datawindow.describe(is_attributi[ll_i])
	if ls_valore <> "" then
		ls_modify = ls_modify + is_attributi[ll_i] + "=" + ls_valore + " "
	end if
next

dw_anteprima.modify(ls_modify)

end subroutine

public subroutine wf_zoom (integer ai_zoom_level);long ll_i, ll_zoom, ll_zoom_preview
//dw_anteprima.setfocus()

//ll_zoom_preview = long(dw_anteprima.describe("datawindow.print.preview.zoom"))

if ai_zoom_level > 20 and ai_zoom_level < 300 then
	//ll_zoom <> ll_zoom_preview then
	dw_anteprima.modify("datawindow.print.preview.zoom=" + string(ai_zoom_level))
end if
end subroutine

public subroutine wf_toggle_rules ();string ls_righello, ls_bmp


//ls_righello = dw_anteprima.describe("datawindow.print.preview.rulers")
//if ls_righello = "yes" then
//	ls_righello = "no"
//	ls_bmp = "\cs_xx\risorse\rig.bmp"
//else
//	ls_righello = "yes"
//	ls_bmp = "\cs_xx\risorse\rigdis.bmp"
//END IF
//
//picturename = ls_bmp
//
//dw_anteprima.setfocus()
//
//dw_anteprima.modify("datawindow.print.preview.rulers=" + ls_righello)

end subroutine

private subroutine wf_set_page (integer ai_current_row);/**
 * stefaop
 * 21/09/2010
 *
 * Calcola la pagina corrente in base alla numero di riga
 **/
 
if ai_current_row > 0 then
	em_current_page.text  = dw_anteprima.describe("Evaluate('Page()'," + string(ai_current_row) +")")
	ii_current_page = long(em_current_page.text)
	
	dw_anteprima.setfocus()
end if
end subroutine

public subroutine wf_scroll_to_page (integer ai_scroll_page);/**
 * stefanop
 * 21/09/2010
 *
 * Consente di saltare ad una determinata pagina
 **/
 
int li_offset, li_i

if ai_scroll_page < 1 then
	ai_scroll_page = 1
elseif ai_scroll_page > ii_page_count then
	ai_scroll_page = ii_page_count
elseif ai_scroll_page = ii_current_page then
	return
end if

// se offset + allora vado in giù, se offset - allora vado in su
li_offset = ai_scroll_page - ii_current_page

dw_anteprima.setredraw(false)
this.title = string(li_offset)

if li_offset > 0 then
	
	do while ii_current_page < ai_scroll_page
		dw_anteprima.scrollnextpage()
		ii_current_page = integer(dw_anteprima.describe("evaluate('page()'," + string(dw_anteprima.Object.Datawindow.FirstRowOnPage) + ")"))
	loop
	
else
	
	do while ii_current_page > ai_scroll_page
		dw_anteprima.scrollpriorpage()
		ii_current_page = integer(dw_anteprima.describe("evaluate('page()'," + string(dw_anteprima.Object.Datawindow.FirstRowOnPage) + ")"))
	loop
end if

em_current_page.text = string(ai_scroll_page)
dw_anteprima.setredraw(true)
end subroutine

public function integer wf_page_count ();/**
 * stefanop
 * 21/09/2010
 *
 * La funzione restituisce il numero di pagine da stampare, inoltre imposta correttamente la label nella finestra
 **/
 
ii_page_count = integer(dw_anteprima.describe("evaluate('pagecount()'," + string (dw_anteprima.rowcount() ) + ")"))
st_pages.text = "\ " + string(ii_page_count)

dw_anteprima.setfocus()
return ii_page_count
end function

public subroutine wf_set_orientation (integer ai_orientation);/**
 * stefanop
 * 21/09/2010
 *
 * Imposta la visualizzazione della datawindow
 **/

dw_anteprima.setredraw(false)
if dw_anteprima.modify("DataWindow.Print.Orientation='" + string(ai_orientation) +"'") = "" then
	wf_page_count()
end if
dw_anteprima.setredraw(true)
end subroutine

public function string wf_printername ();/**
 * Stefanop
 * 27/07/2011
 *
 * Recuper il nome della stampante corrente
 **/
 
string ls_curent_printer

// Nome della stampante corrente
ls_curent_printer = PrintGetPrinter()
return left(ls_curent_printer, pos(ls_curent_printer, "~t") - 1)
end function

public function boolean wf_print (integer ai_page, boolean ab_canceldialog, boolean ab_showprintdialog);/**
 * stefanop
 * 28/07/2011
 *
 * Unisco in un''unica funzione la stampa
 * ai_page = indica il numero di pagina da stampare, ad esempio su 10 foglio potrei stampare solo il 5
 **/
 
string ls_error

dw_anteprima.setfocus()

// imposto se stampare una pagina solo o tutte
if not isnull(ai_page) and ai_page > 0 then
	ls_error = dw_anteprima.modify("DataWindow.Print.Page.Range = '" + string(ai_page) + "'")
else
	ls_error = dw_anteprima.modify("DataWindow.Print.Page.Range = ''")
end if

if ls_error = "" then
	dw_anteprima.print(ab_canceldialog, ab_showprintdialog)
else
	g_mb.error("", ls_error)
	return false
end if

return true
end function

public subroutine wf_set_title ();/**
 * Imposta il nome della finestra
 **/
 
 title =  is_w_title + " su " + wf_printername()
end subroutine

public subroutine wf_print_shortcut (keycode key, unsignedlong keyflags);/**
 * stefanop
 * 28/07/2011
 *
 * se premo CTRL + INVIO = stampa immediata
 * se premo SHIFT + INVIO = finestra della stampante
 **/
 
// controll
if keyflags = 2 and key = KeyEnter!then
	wf_print(-1, false, false)
	
// shift
elseif keyflags = 1 and key = KeyEnter! then
	wf_print(-1, true, true)

end if
end subroutine

on w_anteprima.create
int iCurrent
call super::create
this.pb_print_current=create pb_print_current
this.r_1=create r_1
this.st_1=create st_1
this.ddlb_zoom=create ddlb_zoom
this.pb_vertical=create pb_vertical
this.pb_horizontal=create pb_horizontal
this.st_pages=create st_pages
this.em_current_page=create em_current_page
this.pb_imp_pagina=create pb_imp_pagina
this.pb_pag_suc=create pb_pag_suc
this.pb_pag_prec=create pb_pag_prec
this.pb_printsetup=create pb_printsetup
this.pb_stampa=create pb_stampa
this.dw_anteprima=create dw_anteprima
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.pb_print_current
this.Control[iCurrent+2]=this.r_1
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.ddlb_zoom
this.Control[iCurrent+5]=this.pb_vertical
this.Control[iCurrent+6]=this.pb_horizontal
this.Control[iCurrent+7]=this.st_pages
this.Control[iCurrent+8]=this.em_current_page
this.Control[iCurrent+9]=this.pb_imp_pagina
this.Control[iCurrent+10]=this.pb_pag_suc
this.Control[iCurrent+11]=this.pb_pag_prec
this.Control[iCurrent+12]=this.pb_printsetup
this.Control[iCurrent+13]=this.pb_stampa
this.Control[iCurrent+14]=this.dw_anteprima
end on

on w_anteprima.destroy
call super::destroy
destroy(this.pb_print_current)
destroy(this.r_1)
destroy(this.st_1)
destroy(this.ddlb_zoom)
destroy(this.pb_vertical)
destroy(this.pb_horizontal)
destroy(this.st_pages)
destroy(this.em_current_page)
destroy(this.pb_imp_pagina)
destroy(this.pb_pag_suc)
destroy(this.pb_pag_prec)
destroy(this.pb_printsetup)
destroy(this.pb_stampa)
destroy(this.dw_anteprima)
end on

event pc_setwindow;call super::pc_setwindow;long ll_i, ll_num_colonne,ll_x,ll_y
string ls_syntax, ls_indice, ls_nome_colonna
datawindowchild ldwc_datawindow, ldwc_anteprima


idw_datawindow = s_cs_xx.parametri.parametro_dw_1

is_attributi[] = {"DataWindow.Print.Color", &
						"DataWindow.Print.Copies", &
						"DataWindow.Print.DocumentName", &
						"DataWindow.Print.Duplex", &
						"DataWindow.Print.Margin.Bottom", &
						"DataWindow.Print.Margin.Left", &
						"DataWindow.Print.Margin.Right", &
						"DataWindow.Print.Margin.Top", &
						"DataWindow.Print.Orientation", &
						"DataWindow.Print.Paper.Size", &
						"DataWindow.Print.Paper.Source", &
						"DataWindow.Print.Quality", &
						"DataWindow.Zoom"}


if isvalid(idw_datawindow) then
	if idw_datawindow.dataobject = "" or &
		idw_datawindow.describe("datawindow.syntax.modified") = "yes" then
		ls_syntax = idw_datawindow.describe("datawindow.syntax")
		dw_anteprima.create(ls_syntax)
	else
		dw_anteprima.dataobject = idw_datawindow.dataobject
	end if

	idw_datawindow.sharedata(dw_anteprima)

	ll_num_colonne = long(idw_datawindow.describe("datawindow.column.count"))
	for ll_i = 1 to ll_num_colonne
		ls_indice = "#" + string(ll_i)
		if idw_datawindow.describe(ls_indice + ".visible") = "1" and &
			idw_datawindow.describe(ls_indice + ".edit.style") = "dddw" then
			ls_nome_colonna = idw_datawindow.describe(ls_indice + ".name")
			idw_datawindow.getchild(ls_nome_colonna, ldwc_datawindow)
			dw_anteprima.getchild(ls_nome_colonna, ldwc_anteprima)
			ldwc_datawindow.sharedata(ldwc_anteprima)
		end if
	next

	imp_attributi()
	
	for ll_i = 1 to ll_num_colonne
		ls_indice = "#" + string(ll_i)
		if idw_datawindow.describe(ls_indice + ".visible") = "1" then
			ls_nome_colonna = idw_datawindow.describe(ls_indice + ".name")
			dw_anteprima.modify(ls_nome_colonna + ".background.color='16777215'")
		end if
	next

	dw_anteprima.modify("datawindow.print.preview=yes")
	
	// stefanop 27/07/2011: imposto il valore di zoom della dw
	//ddlb_zoom.text = idw_datawindow.describe("DataWindow.Zoom") + "%"

end if

is_w_title = pcca.application_name + " - " + s_cs_xx.parametri.parametro_s_1
wf_set_title()

pb_stampa.PictureName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\preview_printer.png"
pb_print_current.PictureName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\preview_print_current.png"
pb_imp_pagina.PictureName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\preview_margin.png"
pb_printsetup.PictureName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\preview_edit.png"
pb_pag_prec.PictureName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\page_up.png"
pb_pag_suc.PictureName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\page_down.png"
pb_horizontal.PictureName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\page_horizontal.png"
pb_vertical.PictureName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\page_vertical.png"

wf_page_count()

end event

event resize;r_1.move(-10, -10)
r_1.width = newwidth + 20

dw_anteprima.move(0, r_1.height - 13)
dw_anteprima.resize(newwidth, newheight - r_1.height)



end event

event closequery;call super::closequery;long ll_i, ll_num_colonne
string ls_indice, ls_nome_colonna
datawindowchild ldwc_anteprima

if isvalid(idw_datawindow) then
	ll_num_colonne = long(idw_datawindow.describe("datawindow.column.count"))
	for ll_i = 1 to ll_num_colonne
		ls_indice = "#" + string(ll_i)
		if idw_datawindow.describe(ls_indice + ".visible") = "1" and &
			idw_datawindow.describe(ls_indice + ".edit.style") = "dddw" then
			ls_nome_colonna = idw_datawindow.describe(ls_indice + ".name")
			dw_anteprima.getchild(ls_nome_colonna, ldwc_anteprima)
			ldwc_anteprima.sharedataoff()
		end if
	next
end if

dw_anteprima.sharedataoff()
end event

event key;call super::key;wf_print_shortcut(key, keyflags)
end event

type pb_print_current from picturebutton within w_anteprima
event mousemove pbm_mousemove
integer x = 137
integer y = 24
integer width = 110
integer height = 96
integer taborder = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
vtextalign vtextalign = vcenter!
string powertiptext = "Stampa pagina corrente"
long backcolor = 67108864
end type

event clicked;wf_print(ii_current_page, false, false)

end event

type r_1 from rectangle within w_anteprima
long linecolor = 8421504
integer linethickness = 4
long fillcolor = 12632256
integer x = -23
integer y = -20
integer width = 3749
integer height = 180
end type

type st_1 from statictext within w_anteprima
integer x = 1691
integer y = 32
integer width = 229
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Zoom"
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_zoom from dropdownlistbox within w_anteprima
integer x = 1943
integer y = 20
integer width = 320
integer height = 540
integer taborder = 100
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "100%"
boolean allowedit = true
boolean sorted = false
string item[] = {"250%","200%","150%","100%","75%","50%","25%"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;if index < 1 then return

string ls_zoom

ls_zoom = item[index]

if pos(ls_zoom, "%") > 0 then
	ls_zoom = left(ls_zoom, len(ls_zoom) - 1)
end if

wf_zoom(integer(ls_zoom))
end event

event modified;int li_zoom

text = trim(text)
if isnull(text) or text = "" then
	li_zoom = 100
	text = "100%"
else
	if pos(text, "%") > 0 then
		li_zoom = integer(left(text, len(text) - 1))
	else
		li_zoom = integer(text)
		text += "%"
	end if
end if

// controllo minimi e massimi
if li_zoom < 25 then
	li_zoom = 25
	text = "25%"
elseif li_zoom > 275 then
	li_zoom = 275
	text = "275%"
end if
	

wf_zoom(li_zoom)
end event

type pb_vertical from picturebutton within w_anteprima
event mousemove pbm_mousemove
integer x = 1463
integer y = 20
integer width = 110
integer height = 96
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\page_vertical.png"
vtextalign vtextalign = vcenter!
string powertiptext = "Orientamento Verticale"
long backcolor = 67108864
end type

event clicked;wf_set_orientation(PORTRAIT)
end event

type pb_horizontal from picturebutton within w_anteprima
event mousemove pbm_mousemove
integer x = 1349
integer y = 20
integer width = 110
integer height = 96
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\page_horizontal.png"
vtextalign vtextalign = vcenter!
string powertiptext = "Orientamento orizzontale"
long backcolor = 67108864
end type

event clicked;wf_set_orientation(LANDSCAPE)
end event

type st_pages from statictext within w_anteprima
integer x = 1120
integer y = 36
integer width = 183
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "/ 99"
boolean focusrectangle = false
end type

type em_current_page from editmask within w_anteprima
integer x = 960
integer y = 32
integer width = 137
integer height = 76
integer taborder = 90
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###"
string minmax = "1~~"
end type

event modified;wf_scroll_to_page(integer(text))
end event

type pb_imp_pagina from picturebutton within w_anteprima
event mousemove pbm_mousemove
integer x = 366
integer y = 24
integer width = 110
integer height = 96
integer taborder = 90
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\preview_margin.png"
vtextalign vtextalign = vcenter!
string powertiptext = "Impostazioni"
long backcolor = 67108864
end type

event clicked;dw_anteprima.setfocus()

setpointer(hourglass!)
s_cs_xx.parametri.parametro_dw_1 = idw_datawindow
open(w_imp_pagina)

imp_attributi()
end event

type pb_pag_suc from picturebutton within w_anteprima
event mousemove pbm_mousemove
event ue_scroll pbm_vscroll
integer x = 823
integer y = 24
integer width = 110
integer height = 96
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\page_down.png"
vtextalign vtextalign = vcenter!
string powertiptext = "Pagina successiva"
long backcolor = 67108864
end type

event clicked;wf_set_page(dw_anteprima.scrollnextpage())
end event

type pb_pag_prec from picturebutton within w_anteprima
event mousemove pbm_mousemove
integer x = 709
integer y = 24
integer width = 110
integer height = 96
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\page_up.png"
vtextalign vtextalign = vcenter!
string powertiptext = "Pagina precedente"
long backcolor = 67108864
end type

event clicked;wf_set_page(dw_anteprima.scrollpriorpage())
end event

type pb_printsetup from picturebutton within w_anteprima
event mousemove pbm_mousemove
integer x = 480
integer y = 24
integer width = 110
integer height = 96
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\preview_edit.png"
vtextalign vtextalign = vcenter!
string powertiptext = "Seleziona stampante"
long backcolor = 67108864
end type

event clicked;w_cs_xx_mdi.im_menu_cs.m_file.m_impostastampante.event clicked()
wf_set_title()
end event

type pb_stampa from picturebutton within w_anteprima
event mousemove pbm_mousemove
integer x = 23
integer y = 24
integer width = 110
integer height = 96
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\cs_115\framework\risorse\11.5\preview_printer.png"
vtextalign vtextalign = vcenter!
string powertiptext = "Stampa documento"
long backcolor = 67108864
end type

event clicked;wf_print(-1, false, false)
end event

type dw_anteprima from datawindow within w_anteprima
event mousemove pbm_mousemove
event ue_press_key pbm_dwnkey
integer x = 23
integer y = 260
integer width = 3657
integer height = 1920
integer taborder = 70
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event ue_press_key;wf_print_shortcut(key, keyflags)
end event

event scrollvertical;wf_set_page(long(Object.Datawindow.FirstRowOnPage))
end event

