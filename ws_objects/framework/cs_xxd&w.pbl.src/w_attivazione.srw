﻿$PBExportHeader$w_attivazione.srw
forward
global type w_attivazione from w_cs_xx_principale
end type
type shl_1 from statichyperlink within w_attivazione
end type
type cb_copy from commandbutton within w_attivazione
end type
type sle_societa from singlelineedit within w_attivazione
end type
type sle_nome from singlelineedit within w_attivazione
end type
type st_6 from statictext within w_attivazione
end type
type st_5 from statictext within w_attivazione
end type
type st_4 from statictext within w_attivazione
end type
type st_3 from statictext within w_attivazione
end type
type st_2 from statictext within w_attivazione
end type
type cb_1 from commandbutton within w_attivazione
end type
type mle_seriale from multilineedit within w_attivazione
end type
type sle_adapter from singlelineedit within w_attivazione
end type
type st_1 from statictext within w_attivazione
end type
type r_1 from rectangle within w_attivazione
end type
end forward

global type w_attivazione from w_cs_xx_principale
integer width = 1952
integer height = 1408
string title = ""
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
string icon = "UserObject5!"
boolean clientedge = true
boolean center = true
event ue_custom_backcolor ( )
shl_1 shl_1
cb_copy cb_copy
sle_societa sle_societa
sle_nome sle_nome
st_6 st_6
st_5 st_5
st_4 st_4
st_3 st_3
st_2 st_2
cb_1 cb_1
mle_seriale mle_seriale
sle_adapter sle_adapter
st_1 st_1
r_1 r_1
end type
global w_attivazione w_attivazione

type variables
private:
	long il_color_normal = rgb(255,255,255)
	long il_color_required = rgb(255,255,0)
end variables

event ue_custom_backcolor();st_1.backcolor = rgb(255,255,255)
st_2.backcolor = rgb(255,255,255)
end event

on w_attivazione.create
int iCurrent
call super::create
this.shl_1=create shl_1
this.cb_copy=create cb_copy
this.sle_societa=create sle_societa
this.sle_nome=create sle_nome
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.cb_1=create cb_1
this.mle_seriale=create mle_seriale
this.sle_adapter=create sle_adapter
this.st_1=create st_1
this.r_1=create r_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.shl_1
this.Control[iCurrent+2]=this.cb_copy
this.Control[iCurrent+3]=this.sle_societa
this.Control[iCurrent+4]=this.sle_nome
this.Control[iCurrent+5]=this.st_6
this.Control[iCurrent+6]=this.st_5
this.Control[iCurrent+7]=this.st_4
this.Control[iCurrent+8]=this.st_3
this.Control[iCurrent+9]=this.st_2
this.Control[iCurrent+10]=this.cb_1
this.Control[iCurrent+11]=this.mle_seriale
this.Control[iCurrent+12]=this.sle_adapter
this.Control[iCurrent+13]=this.st_1
this.Control[iCurrent+14]=this.r_1
end on

on w_attivazione.destroy
call super::destroy
destroy(this.shl_1)
destroy(this.cb_copy)
destroy(this.sle_societa)
destroy(this.sle_nome)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.cb_1)
destroy(this.mle_seriale)
destroy(this.sle_adapter)
destroy(this.st_1)
destroy(this.r_1)
end on

event closequery;s_cs_xx.parametri.parametro_s_1 = trim(sle_nome.text)
s_cs_xx.parametri.parametro_s_2 = trim(sle_societa.text)
s_cs_xx.parametri.parametro_bl_1 = blob(mle_seriale.text, EncodingAnsi!)
end event

event pc_setwindow;call super::pc_setwindow;string ls_adapter

// piccolo trick, elimino le { } dalla stringa
ls_adapter = mid(s_cs_xx.parametri.parametro_s_1, 2, len(s_cs_xx.parametri.parametro_s_1) - 2)
sle_adapter.text =ls_adapter

event post ue_custom_backcolor()
end event

event key;call super::key;// *** stefanop 04/10/2010: Per saltare la validazione premere CTRL+SHIFT+P e nel seriale attivazione deve essere scritto CS
string ls_error, ls_key
if key = KeyP! and keyflags = 3 and left(mle_seriale.text, 2) = "CS" then	
	
	uo_licenze luo_license
	luo_license = create uo_licenze
	
	ls_key = luo_license.uof_gen_seriale(sle_adapter.text, date("2020-01-01"), 1, 1, ref ls_error)
	if ls_error <> "" then
		g_mb.error("", ls_error)
		return
	end if
	mle_seriale.text = ls_key
	
end if
end event

type shl_1 from statichyperlink within w_attivazione
integer x = 91
integer y = 1200
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 12632256
string text = "www.csteam.com"
boolean focusrectangle = false
string url = "http://www.csteam.com/"
end type

type cb_copy from commandbutton within w_attivazione
integer x = 1646
integer y = 700
integer width = 178
integer height = 80
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "copy"
end type

event clicked;Clipboard(sle_adapter.text)
end event

type sle_societa from singlelineedit within w_attivazione
integer x = 594
integer y = 600
integer width = 1234
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_nome from singlelineedit within w_attivazione
integer x = 594
integer y = 500
integer width = 1234
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_6 from statictext within w_attivazione
integer x = 91
integer y = 800
integer width = 475
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seriale:"
boolean focusrectangle = false
end type

type st_5 from statictext within w_attivazione
integer x = 91
integer y = 700
integer width = 475
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Codice attivazione:"
boolean focusrectangle = false
end type

type st_4 from statictext within w_attivazione
integer x = 91
integer y = 600
integer width = 475
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Società:"
boolean focusrectangle = false
end type

type st_3 from statictext within w_attivazione
integer x = 91
integer y = 500
integer width = 475
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nome:"
boolean focusrectangle = false
end type

type st_2 from statictext within w_attivazione
integer x = 91
integer y = 140
integer width = 1737
integer height = 220
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 8421504
long backcolor = 16777215
string text = "Attiva il prodotto inviando una mail con il codice di attivazione o contattando la Consulting&&Software al numero 0429 1695111 o al 0429 1891508 o con una mail ad assistenza@csteam.com"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_attivazione
integer x = 1211
integer y = 1180
integer width = 617
integer height = 100
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Attiva"
end type

event clicked;if len(trim(sle_nome.text)) < 1 then
	sle_nome.backcolor = il_color_required
	sle_nome.setfocus()
	return
else
	sle_nome.backcolor = il_color_normal
end if

// società
if len(trim(sle_societa.text)) < 1 then
	sle_societa.backcolor = il_color_required
	sle_societa.setfocus()
	return
else
	sle_societa.backcolor = il_color_normal
end if

// seriale
if len(trim(mle_seriale.text)) < 1 then
	mle_seriale.backcolor = il_color_required
	mle_seriale.setfocus()
	return
else
	mle_seriale.backcolor = il_color_normal
end if

close(parent)
end event

type mle_seriale from multilineedit within w_attivazione
integer x = 594
integer y = 800
integer width = 1234
integer height = 320
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_adapter from singlelineedit within w_attivazione
integer x = 594
integer y = 700
integer width = 1029
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_attivazione
integer x = 91
integer y = 60
integer width = 914
integer height = 80
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "ATTIVAZIONE PRODOTTO"
boolean focusrectangle = false
end type

type r_1 from rectangle within w_attivazione
long linecolor = 16777215
integer linethickness = 4
long fillcolor = 1073741824
integer width = 1943
integer height = 420
end type

