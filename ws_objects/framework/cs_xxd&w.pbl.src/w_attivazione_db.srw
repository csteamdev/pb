﻿$PBExportHeader$w_attivazione_db.srw
forward
global type w_attivazione_db from window
end type
type cb_1 from commandbutton within w_attivazione_db
end type
type sle_2 from singlelineedit within w_attivazione_db
end type
type st_2 from statictext within w_attivazione_db
end type
type sle_1 from singlelineedit within w_attivazione_db
end type
type st_1 from statictext within w_attivazione_db
end type
end forward

global type w_attivazione_db from window
integer width = 1609
integer height = 544
boolean titlebar = true
string title = "Attivazione Database"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "Database!"
boolean center = true
cb_1 cb_1
sle_2 sle_2
st_2 st_2
sle_1 sle_1
st_1 st_1
end type
global w_attivazione_db w_attivazione_db

on w_attivazione_db.create
this.cb_1=create cb_1
this.sle_2=create sle_2
this.st_2=create st_2
this.sle_1=create sle_1
this.st_1=create st_1
this.Control[]={this.cb_1,&
this.sle_2,&
this.st_2,&
this.sle_1,&
this.st_1}
end on

on w_attivazione_db.destroy
destroy(this.cb_1)
destroy(this.sle_2)
destroy(this.st_2)
destroy(this.sle_1)
destroy(this.st_1)
end on

type cb_1 from commandbutton within w_attivazione_db
integer x = 1166
integer y = 300
integer width = 366
integer height = 92
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Genera Codice"
end type

event clicked;string ls_seriale, ls_codice
n_cst_crypto luo_crypt

luo_crypt = create n_cst_crypto

ls_seriale = sle_1.text
ls_codice = luo_crypt.encryptserial(ls_seriale)

sle_2.text = ls_codice

destroy luo_crypt
end event

type sle_2 from singlelineedit within w_attivazione_db
integer x = 571
integer y = 180
integer width = 960
integer height = 80
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_attivazione_db
integer x = 46
integer y = 180
integer width = 526
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Codice Attivazione:"
boolean focusrectangle = false
end type

type sle_1 from singlelineedit within w_attivazione_db
integer x = 571
integer y = 80
integer width = 503
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_attivazione_db
integer x = 46
integer y = 80
integer width = 274
integer height = 80
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seriale:"
boolean focusrectangle = false
end type

