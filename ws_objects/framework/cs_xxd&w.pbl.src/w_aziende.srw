﻿$PBExportHeader$w_aziende.srw
$PBExportComments$Finestra Gestione Aziende
forward
global type w_aziende from w_cs_xx_principale
end type
type dw_aziende_det_2 from uo_cs_xx_dw within w_aziende
end type
type dw_aziende_det_1 from uo_cs_xx_dw within w_aziende
end type
type dw_aziende_lista from uo_cs_xx_dw within w_aziende
end type
type dw_folder from u_folder within w_aziende
end type
end forward

global type w_aziende from w_cs_xx_principale
integer width = 3447
integer height = 2116
string title = "Amministrazione Anagrafica Aziende"
event ue_test_mail ( )
dw_aziende_det_2 dw_aziende_det_2
dw_aziende_det_1 dw_aziende_det_1
dw_aziende_lista dw_aziende_lista
dw_folder dw_folder
end type
global w_aziende w_aziende

event ue_test_mail();// invio mail di test dell'utente

boolean 		lb_result
string			ls_destinatari[], ls_error,ls_from,ls_sender_name,ls_smtp_server,ls_smtp_usd,ls_smtp_pwd, ls_cod_azienda
integer		li_ret

ls_cod_azienda = dw_aziende_lista.getitemstring(dw_aziende_lista.getrow(),"cod_azienda")


//recupero info principali per inviare email (sender, smtp, user_smtp, pwd_smtp)
SELECT	email_mittente,
			sender_name,
			smtp_server,
			smtp_user,
			smtp_password
into		:ls_from,
			:ls_sender_name,
			:ls_smtp_server,
			:ls_smtp_usd,
			:ls_smtp_pwd
from aziende
where cod_azienda=:ls_cod_azienda;

if sqlca.sqlcode < 0 then
	//errore
	g_mb.error("Errore in lettura valori SMTP in tabella utenti: "+sqlca.sqlerrtext + " PROCESSO TERMINATO")
	return
	
elseif sqlca.sqlcode = 100 then
	//utente non trovato in tabella
	g_mb.error("Utente con codice '"+s_cs_xx.cod_utente+"' non trovato in tabella utenti!  PROCESSO TERMINATO")
	return
end if

if isnull(ls_from) or ls_from="" then
	g_mb.error("E-mail non specificata per l'utente con codice '"+s_cs_xx.cod_utente+"!  PROCESSO TERMINATO")
	return
end if

if isnull(ls_smtp_server) or ls_smtp_server="" then
	g_mb.error("Indirizzo server SMTP non specificato per l'utente con codice '"+s_cs_xx.cod_utente+"!  PROCESSO TERMINATO")
	return
end if

if isnull(ls_sender_name) then ls_sender_name = "<Sender>"
if isnull(ls_smtp_usd) then ls_smtp_usd = ""
if isnull(ls_smtp_pwd) then ls_smtp_pwd = ""


uo_sendmail luo_sendmail
luo_sendmail = create uo_sendmail

ls_destinatari[1]=ls_from

luo_sendmail.uof_set_from(ls_from)
luo_sendmail.uof_set_to(ls_destinatari[])
luo_sendmail.uof_set_subject("Mail Test CSTEAM")
luo_sendmail.uof_set_message("Questa è una mail di test generata dalla gestione utenti di Apice/Omnia (Csteam srl)")
luo_sendmail.uof_set_smtp(ls_smtp_server, ls_smtp_usd, ls_smtp_pwd)
luo_sendmail.uof_set_html( true )

lb_result = luo_sendmail.uof_send()
ls_error = luo_sendmail.uof_get_error()

if lb_result then
	g_mb.show(g_str.format("Mail inviata con successo all'indirizzo $1.",ls_from))
else
	g_mb.Error("Errore invio mail~r~n"+ls_error)
end if
return


end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


lw_oggetti[1] = dw_aziende_lista
dw_folder.fu_assigntab(1, "Lista", lw_oggetti[])
lw_oggetti[1] = dw_aziende_det_1
dw_folder.fu_assigntab(2, "Anagrafica", lw_oggetti[])
lw_oggetti[1] = dw_aziende_det_2
dw_folder.fu_assigntab(3, "Fattura Elettronica", lw_oggetti[])
dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)


dw_aziende_lista.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_default, &
                                c_default)

dw_aziende_det_1.set_dw_options(sqlca, &
                              dw_aziende_lista, &
                              c_sharedata + c_scrollparent, &
                              c_default)
dw_aziende_det_2.set_dw_options(sqlca, &
                              dw_aziende_lista, &
                              c_sharedata + c_scrollparent, &
                              c_default)
iuo_dw_main = dw_aziende_lista
end event

on close;call w_cs_xx_principale::close;s_cs_xx.menu = 1
end on

on w_aziende.create
int iCurrent
call super::create
this.dw_aziende_det_2=create dw_aziende_det_2
this.dw_aziende_det_1=create dw_aziende_det_1
this.dw_aziende_lista=create dw_aziende_lista
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_aziende_det_2
this.Control[iCurrent+2]=this.dw_aziende_det_1
this.Control[iCurrent+3]=this.dw_aziende_lista
this.Control[iCurrent+4]=this.dw_folder
end on

on w_aziende.destroy
call super::destroy
destroy(this.dw_aziende_det_2)
destroy(this.dw_aziende_det_1)
destroy(this.dw_aziende_lista)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_aziende_det_2, &
                 "fatel_idpaese_trasmittente", &
                 sqlca, &
                 "tab_nazioni_iso", &
                 "cod_nazione_iso", &
                 "des_nazione_iso", &
                 "")

f_po_loaddddw_dw(dw_aziende_det_2, &
                 "fatel_cod_nazione", &
                 sqlca, &
                 "tab_nazioni_iso", &
                 "cod_nazione_iso", &
                 "des_nazione_iso", &
                 "")

f_po_loaddddw_dw(dw_aziende_det_2, &
                 "fatel_sede_cod_nazione", &
                 sqlca, &
                 "tab_nazioni_iso", &
                 "cod_nazione_iso", &
                 "des_nazione_iso", &
                 "")

end event

type dw_aziende_det_2 from uo_cs_xx_dw within w_aziende
integer x = 46
integer y = 120
integer width = 3269
integer height = 1860
integer taborder = 20
string dataobject = "d_aziende_det_2"
boolean border = false
end type

on pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;dw_aziende_lista.triggerevent("pcd_validaterow")
end on

type dw_aziende_det_1 from uo_cs_xx_dw within w_aziende
integer x = 46
integer y = 120
integer width = 3291
integer height = 1660
integer taborder = 30
string dataobject = "d_aziende_det_1"
boolean border = false
end type

on pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;dw_aziende_lista.triggerevent("pcd_validaterow")
end on

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_test_mail"
		parent.event ue_test_mail()
end choose
end event

type dw_aziende_lista from uo_cs_xx_dw within w_aziende
integer x = 69
integer y = 120
integer width = 3269
integer height = 1820
integer taborder = 20
string dataobject = "d_aziende_lista"
boolean vscrollbar = true
boolean border = false
end type

on pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;string ls_codice, ls_tabella, ls_descrizione


if i_rownbr > 0 then
   ls_tabella = "aziende"
   ls_codice = "cod_azienda"
   ls_descrizione = "rag_soc_1"
   f_upd_partita_iva(ls_tabella, ls_codice, ls_descrizione)
end if

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore

ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_folder from u_folder within w_aziende
integer width = 3383
integer height = 2000
integer taborder = 10
end type

