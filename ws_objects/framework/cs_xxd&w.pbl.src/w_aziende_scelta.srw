﻿$PBExportHeader$w_aziende_scelta.srw
$PBExportComments$Finestra Scelta Aziende
forward
global type w_aziende_scelta from w_cs_xx_risposta
end type
type cb_annulla from uo_cb_close within w_aziende_scelta
end type
type dw_aziende_scelta_2 from uo_cs_xx_dw within w_aziende_scelta
end type
type cb_ok from uo_cb_ok within w_aziende_scelta
end type
type dw_aziende_scelta_1 from uo_cs_xx_dw within w_aziende_scelta
end type
end forward

global type w_aziende_scelta from w_cs_xx_risposta
integer width = 1545
integer height = 856
string title = "Scelta Azienda"
cb_annulla cb_annulla
dw_aziende_scelta_2 dw_aziende_scelta_2
cb_ok cb_ok
dw_aziende_scelta_1 dw_aziende_scelta_1
end type
global w_aziende_scelta w_aziende_scelta

type variables
string is_cod_azienda
end variables

event pc_accept;call super::pc_accept;if isnull(is_cod_azienda) or is_cod_azienda = "" then
  	g_mb.messagebox("Attenzione", "Selezionare un codice azienda valido.", exclamation!, ok!)
	return
end if

s_cs_xx.cod_azienda = is_cod_azienda
f_azienda_scelta()

/*s_cs_xx.menu = 1

// ------------------------------------- creazione della transazione SQLCI verso la contabilità di impresa -------------------------

string ls_stringa, ls_impresa
integer li_risposta

ls_impresa = "FALSE"
li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "imp", ls_impresa)

ls_impresa = upper(ls_impresa)

select stringa
into   :ls_stringa
from   parametri
where  cod_parametro = 'OIM';
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice - Impresa","Connessione verso la contabilità non effettuata a causa di un errore. L'applicazione verrà terminata. Dettaglio " + sqlca.sqlerrtext)
	halt close
end if

// ENRICO 28/02/2001 AGGIUNTO PER GESTIONE DI PIU' AZIENDE -------------------------------------

if sqlca.sqlcode = 100 then
	select stringa
	into   :ls_stringa
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'OIM';
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice - Impresa","Connessione verso la contabilità non effettuata a causa di un errore. L'applicazione verrà terminata. Dettaglio " + sqlca.sqlerrtext)
		halt close
	end if
end if

// ---------------------------------------------------------------------------------------------

if sqlca.sqlcode = 0 and ls_impresa = "TRUE" then		// esiste il parametro e allora mi connetto alla contabilità IMPRESA.
	destroy sqlci			// distruggo transazione precedente; non si sa mai !!!
	sqlci = CREATE n_tran
	sqlci.servername = "CS_DB"
	sqlci.logid = ""
	sqlci.logpass = ""
	sqlci.dbms = "Odbc"
	sqlci.database = ""
	sqlci.userid = "DBA"
	sqlci.dbpass = "nonlaso"
	sqlci.dbparm = "Connectstring='DSN=" + ls_stringa + "'"
	if f_po_connect(sqlci, TRUE) <> 0 then
		g_mb.messagebox("Apice - Impresa","Errore durante il collegamento alla fonte dati della contabilità Impresa")
		halt close
	end if
	s_cs_xx.parametri.impresa = TRUE
else
	s_cs_xx.parametri.impresa = FALSE
end if */

/*
29/11/2012 - F. Baschirotto
Tolta apertura automatica del menù in quanto incompatibile con il nuovo menù a 3 schede
e con la nuova selezione delle aziende dal menù a tendina della finestra MDI.

// --------------------------------  OPEN DEL MENU GENERALE ----------------------------------------

open(w_menu_80,w_cs_xx_mdi)

close(this)*/
end event

event pc_setwindow;call super::pc_setwindow;if s_cs_xx.cod_utente = "CS_SYSTEM" then
   dw_aziende_scelta_2.visible = false
   dw_aziende_scelta_1.set_dw_options(sqlca, pcca.null_object, c_default, c_default)
	dw_aziende_scelta_1.setfocus()
	dw_aziende_scelta_1.change_dw_current()
else
   dw_aziende_scelta_1.visible = false
   dw_aziende_scelta_2.set_dw_options(sqlca, pcca.null_object, c_default, c_default)
	dw_aziende_scelta_2.setfocus()
	dw_aziende_scelta_2.change_dw_current()
end if

if isvalid(w_menu_80) then
	w_menu_80.triggerevent("ue_close")
end if
end event

event pc_close;call super::pc_close;pcca.pcmgr.i_exitwindow = pcca.mdi_frame
postevent(pcca.pcmgr, "pc_exitapp")
end event

on w_aziende_scelta.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.dw_aziende_scelta_2=create dw_aziende_scelta_2
this.cb_ok=create cb_ok
this.dw_aziende_scelta_1=create dw_aziende_scelta_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.dw_aziende_scelta_2
this.Control[iCurrent+3]=this.cb_ok
this.Control[iCurrent+4]=this.dw_aziende_scelta_1
end on

on w_aziende_scelta.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.dw_aziende_scelta_2)
destroy(this.cb_ok)
destroy(this.dw_aziende_scelta_1)
end on

type cb_annulla from uo_cb_close within w_aziende_scelta
integer x = 411
integer y = 660
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
boolean cancel = true
end type

type dw_aziende_scelta_2 from uo_cs_xx_dw within w_aziende_scelta
integer x = 23
integer y = 20
integer width = 1463
integer height = 620
integer taborder = 30
string dataobject = "d_aziende_scelta_2"
boolean vscrollbar = true
boolean border = false
end type

event doubleclicked;call super::doubleclicked;if getrow() > 0 then
	cb_ok.postevent("clicked")
end if
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_utente)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

if rowcount() > 0 then
	setrow(1)
   is_cod_azienda = getitemstring(1,1)
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if currentrow > 0 then
   is_cod_azienda = getitemstring(currentrow,"cod_azienda")
end if
end event

type cb_ok from uo_cb_ok within w_aziende_scelta
integer x = 800
integer y = 660
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
boolean default = true
end type

type dw_aziende_scelta_1 from uo_cs_xx_dw within w_aziende_scelta
integer x = 23
integer y = 20
integer width = 1463
integer height = 620
integer taborder = 20
string dataobject = "d_aziende_scelta_1"
boolean vscrollbar = true
boolean border = false
end type

event doubleclicked;call super::doubleclicked;if getrow() > 0 then
	cb_ok.postevent("clicked")
end if
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if

if rowcount() > 0 then
	setrow(1)
   is_cod_azienda = getitemstring(1,1)
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if currentrow > 0 then
   is_cod_azienda = getitemstring(currentrow,"cod_azienda")
end if
end event

