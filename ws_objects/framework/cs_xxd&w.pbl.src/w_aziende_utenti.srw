﻿$PBExportHeader$w_aziende_utenti.srw
$PBExportComments$Finestra Gestione Utenti Aziende
forward
global type w_aziende_utenti from w_cs_xx_principale
end type
type dw_aziende_utenti from uo_cs_xx_dw within w_aziende_utenti
end type
type cb_carica_aziende from uo_cb_ok within w_aziende_utenti
end type
end forward

global type w_aziende_utenti from w_cs_xx_principale
integer width = 3314
integer height = 1760
string title = "Gestione Aziende per Utente"
dw_aziende_utenti dw_aziende_utenti
cb_carica_aziende cb_carica_aziende
end type
global w_aziende_utenti w_aziende_utenti

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_aziende_utenti, &
                 "cod_utente", &
                 sqlca, &
                 "utenti", &
                 "cod_utente", &
                 "nome_cognome", &
                 "(flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")")

f_po_loaddddw_dw(dw_aziende_utenti, &
                 "cod_azienda", &
                 sqlca, &
                 "aziende", &
                 "cod_azienda", &
                 "rag_soc_1", &
                 "(flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")")
					  
f_po_loaddddw_dw(dw_aziende_utenti, &
                 "cod_profilo", &
                 sqlca, &
                 "profili", &
                 "cod_profilo", &
                 "des_profilo", &
                 "")
					  
f_po_loaddddw_dw(dw_aziende_utenti, &
                 "cod_lingua_applicazione", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on pc_accept;call w_cs_xx_principale::pc_accept;window_open(w_carica_aziende, 0)


triggerevent("pc_retrieve")
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_aziende_utenti.ib_proteggi_chiavi = false

dw_aziende_utenti.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_default, &
                                 c_default)
end on

on w_aziende_utenti.create
int iCurrent
call super::create
this.dw_aziende_utenti=create dw_aziende_utenti
this.cb_carica_aziende=create cb_carica_aziende
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_aziende_utenti
this.Control[iCurrent+2]=this.cb_carica_aziende
end on

on w_aziende_utenti.destroy
call super::destroy
destroy(this.dw_aziende_utenti)
destroy(this.cb_carica_aziende)
end on

type dw_aziende_utenti from uo_cs_xx_dw within w_aziende_utenti
integer x = 23
integer y = 20
integer width = 3223
integer height = 1500
integer taborder = 20
string dataobject = "d_aziende_utenti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore

ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type cb_carica_aziende from uo_cb_ok within w_aziende_utenti
integer x = 2880
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Car. Aziende"
end type

