﻿$PBExportHeader$w_cambia_dw.srw
$PBExportComments$Finestra Tools cambio sintassi sql nella datawindow
forward
global type w_cambia_dw from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_cambia_dw
end type
type mle_1 from multilineedit within w_cambia_dw
end type
type st_1 from statictext within w_cambia_dw
end type
type st_2 from statictext within w_cambia_dw
end type
type st_3 from statictext within w_cambia_dw
end type
type cb_2 from commandbutton within w_cambia_dw
end type
type dw_lista_sostituzioni from uo_cs_xx_dw within w_cambia_dw
end type
type st_4 from statictext within w_cambia_dw
end type
type st_5 from statictext within w_cambia_dw
end type
type cb_3 from commandbutton within w_cambia_dw
end type
type gb_2 from groupbox within w_cambia_dw
end type
type gb_1 from groupbox within w_cambia_dw
end type
type st_10 from statictext within w_cambia_dw
end type
type cb_4 from commandbutton within w_cambia_dw
end type
type sle_library from singlelineedit within w_cambia_dw
end type
type sle_library_dest from singlelineedit within w_cambia_dw
end type
type sle_oggetto_iniziale from singlelineedit within w_cambia_dw
end type
type sle_oggetto_finale from singlelineedit within w_cambia_dw
end type
type sle_cerca from singlelineedit within w_cambia_dw
end type
type sle_sostitusci from singlelineedit within w_cambia_dw
end type
type st_11 from statictext within w_cambia_dw
end type
type ddlb_tipo_oggetto from dropdownlistbox within w_cambia_dw
end type
type st_6 from statictext within w_cambia_dw
end type
end forward

global type w_cambia_dw from w_cs_xx_principale
int Width=2167
int Height=1985
boolean TitleBar=true
string Title="Sostituzioni Sisntassi DW"
cb_1 cb_1
mle_1 mle_1
st_1 st_1
st_2 st_2
st_3 st_3
cb_2 cb_2
dw_lista_sostituzioni dw_lista_sostituzioni
st_4 st_4
st_5 st_5
cb_3 cb_3
gb_2 gb_2
gb_1 gb_1
st_10 st_10
cb_4 cb_4
sle_library sle_library
sle_library_dest sle_library_dest
sle_oggetto_iniziale sle_oggetto_iniziale
sle_oggetto_finale sle_oggetto_finale
sle_cerca sle_cerca
sle_sostitusci sle_sostitusci
st_11 st_11
ddlb_tipo_oggetto ddlb_tipo_oggetto
st_6 st_6
end type
global w_cambia_dw w_cambia_dw

on w_cambia_dw.create
int iCurrent
call w_cs_xx_principale::create
this.cb_1=create cb_1
this.mle_1=create mle_1
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.cb_2=create cb_2
this.dw_lista_sostituzioni=create dw_lista_sostituzioni
this.st_4=create st_4
this.st_5=create st_5
this.cb_3=create cb_3
this.gb_2=create gb_2
this.gb_1=create gb_1
this.st_10=create st_10
this.cb_4=create cb_4
this.sle_library=create sle_library
this.sle_library_dest=create sle_library_dest
this.sle_oggetto_iniziale=create sle_oggetto_iniziale
this.sle_oggetto_finale=create sle_oggetto_finale
this.sle_cerca=create sle_cerca
this.sle_sostitusci=create sle_sostitusci
this.st_11=create st_11
this.ddlb_tipo_oggetto=create ddlb_tipo_oggetto
this.st_6=create st_6
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_1
this.Control[iCurrent+2]=mle_1
this.Control[iCurrent+3]=st_1
this.Control[iCurrent+4]=st_2
this.Control[iCurrent+5]=st_3
this.Control[iCurrent+6]=cb_2
this.Control[iCurrent+7]=dw_lista_sostituzioni
this.Control[iCurrent+8]=st_4
this.Control[iCurrent+9]=st_5
this.Control[iCurrent+10]=cb_3
this.Control[iCurrent+11]=gb_2
this.Control[iCurrent+12]=gb_1
this.Control[iCurrent+13]=st_10
this.Control[iCurrent+14]=cb_4
this.Control[iCurrent+15]=sle_library
this.Control[iCurrent+16]=sle_library_dest
this.Control[iCurrent+17]=sle_oggetto_iniziale
this.Control[iCurrent+18]=sle_oggetto_finale
this.Control[iCurrent+19]=sle_cerca
this.Control[iCurrent+20]=sle_sostitusci
this.Control[iCurrent+21]=st_11
this.Control[iCurrent+22]=ddlb_tipo_oggetto
this.Control[iCurrent+23]=st_6
end on

on w_cambia_dw.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_1)
destroy(this.mle_1)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.cb_2)
destroy(this.dw_lista_sostituzioni)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.cb_3)
destroy(this.gb_2)
destroy(this.gb_1)
destroy(this.st_10)
destroy(this.cb_4)
destroy(this.sle_library)
destroy(this.sle_library_dest)
destroy(this.sle_oggetto_iniziale)
destroy(this.sle_oggetto_finale)
destroy(this.sle_cerca)
destroy(this.sle_sostitusci)
destroy(this.st_11)
destroy(this.ddlb_tipo_oggetto)
destroy(this.st_6)
end on

event pc_setwindow;call super::pc_setwindow;
dw_lista_sostituzioni.set_dw_options(sqlca, &
                                     pcca.null_object, &
												 c_nonew + &
												 c_nodelete + &
												 c_nomodify + &
												 c_disablecc+ &
												 c_disableccinsert, &
												 c_default)
save_on_close(c_socnosave)

iuo_dw_main = dw_lista_sostituzioni
end event

type cb_1 from commandbutton within w_cambia_dw
int X=1738
int Y=1261
int Width=366
int Height=81
int TabOrder=140
boolean BringToTop=true
string Text="&Elabora"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_entries, ls_str, ls_dw[], ls_sql, ls_new_str[], ls_old_str[], ls_comment, &
       ls_comm[], ls_riga, ls_cerca, ls_fine_riga
long ll_pos, ll_pos1, ll_i, ll_y, ll_z, ll_lunghezza, ll_start_pos, ll_end_pos, &
     ll_pos2, ll_pos3, ll_file, ll_filenum, ll_errore1, ll_errore2



ls_fine_riga = char(13) + char(10)
if isnull(ddlb_tipo_oggetto.text) or len(ddlb_tipo_oggetto.text) < 1 then
	return
end if
if isnull(sle_library.text) or len(sle_library.text) < 1 then
	return
end if
if isnull(sle_library_dest.text) or len(sle_library_dest.text) < 1 then
	return
end if


ll_y = dw_lista_sostituzioni.rowcount()
if ll_y < 1 then
	g_mb.messagebox("APICE","Manca indicazione stringhe ricerca e sostituzione")
	return
end if
for ll_i = 1 to ll_y
	ls_old_str[ll_i] = dw_lista_sostituzioni.getitemstring(ll_i,"stringa_cerca")
	ls_new_str[ll_i] = dw_lista_sostituzioni.getitemstring(ll_i,"stringa_sostituita")
	if isnull(ls_new_str[ll_i]) then ls_new_str[ll_i] = ""
next

ll_i = 1
choose case ddlb_tipo_oggetto.text
	case "DataWindow"
		ls_entries = librarydirectory(sle_library.text, dirdatawindow!)
	case "Window"
		ls_entries = librarydirectory(sle_library.text, dirwindow!)
end choose
do 
	choose case ddlb_tipo_oggetto.text
		case "DataWindow"
			ll_pos = pos(ls_entries,"d_")
		case "Window"
			ll_pos = pos(ls_entries,"w_")
	end choose
	if ll_pos = 0 or isnull(ll_pos) then exit
	if ll_pos > 1 then
		ls_entries = mid(ls_entries, ll_pos)
		ll_pos = 1
	end if
	if ll_pos = 0 or isnull(ll_pos) then exit
	ll_pos1 = pos(ls_entries,"~t")
	ls_str = mid(ls_entries, ll_pos, ll_pos1 - ll_pos)
	ls_comment = mid(ls_entries, ll_pos1 + 1)
	ll_pos2 = pos(ls_comment,"~t") + 1
	ll_pos3 = pos(ls_comment,"~n") - 2
	if ll_pos2 <= ll_pos3 then
		ls_comment = mid(ls_comment, ll_pos2, ll_pos3 - ll_pos2 + 1)
		ls_comment = trim(ls_comment)
	else
		ls_comment = ""
	end if
	if not isnull(sle_oggetto_iniziale.text) and len(sle_oggetto_iniziale.text) > 0 then
		if upper(sle_oggetto_iniziale.text) = upper(ls_str) then
			ls_dw[ll_i] = ls_str
			ls_comm[ll_i] = ls_comment
			ll_i ++
		end if
	else
		ls_dw[ll_i] = ls_str
		ls_comm[ll_i] = ls_comment
		ll_i ++
	end if	
	ls_entries = mid(ls_entries, ll_pos1)
loop until 1 = 2

//  ------------ Ciclo Operativo

ll_y = upperbound(ls_dw)
for ll_i = 1 to ll_y
	mle_1.text = libraryexport(sle_library.text, &
	                     ls_dw[ll_i], &
					         exportdatawindow!)
	
		st_6.text = ls_dw[ll_i] + " - " + ls_comm[ll_i]
		
		for ll_z = 1 to upperbound(ls_old_str)
			ll_lunghezza = len(ls_old_str[ll_z])
			ll_start_pos = pos(mle_1.text, ls_old_str[ll_z], 1)
			do while ll_start_pos > 0
				st_6.text = st_6.text + "    ->" + string(ll_start_pos)
				mle_1.text = replace(mle_1.text, ll_start_pos, ll_lunghezza, ls_new_str[ll_z])
				ll_start_pos = pos(mle_1.text, ls_old_str[ll_z],  ll_start_pos + len(ls_new_str[ll_z]))
			loop
		next

//		ls_cerca = char(126) + char(34)
//		if ddlb_tipo_oggetto.text = "DataWindow" then
//			ll_start_pos = pos(mle_1.text, "retrieve=", 1)
//			ll_end_pos   = pos(mle_1.text, ls_fine_riga, ll_start_pos)
//			do
//				ll_pos = pos(mle_1.text, ls_cerca, ll_start_pos)
//				if (ll_pos > 0) and (ll_pos >= ll_start_pos) and (ll_pos <= ll_end_pos) then
//					mle_1.text = replace(mle_1.text, ll_pos, 2, "")
//				else
//					exit
//				end if
//			loop until 1 = 2
//		end if
	
//		ll_pos2 = 1
//		ls_cerca = char(126) + char(126) + char(126) + char(34)
//		ls_fine_riga = " )"
//		if ddlb_tipo_oggetto.text = "DataWindow" then
//		do
//			ll_start_pos = pos(mle_1.text, "WHERE(   ", ll_pos2)
//			if ll_start_pos < 1 then exit
//			ll_end_pos   = pos(mle_1.text, ls_fine_riga, ll_start_pos)
//			do
//				ll_pos = pos(mle_1.text, ls_cerca, ll_start_pos)
//				if (ll_pos > 0) and (ll_pos >= ll_start_pos) and (ll_pos <= ll_end_pos) then
//					mle_1.text = replace(mle_1.text, ll_pos, len(ls_cerca), "")
//					ll_end_pos   = pos(mle_1.text, ls_fine_riga, ll_start_pos)
//				else
//					exit
//				end if
//			loop until 1 = 2
//			ll_pos2 = pos(mle_1.text, ls_fine_riga, ll_start_pos)			
//		loop until 1= 2
//		end if


	if libraryimport(sle_library_dest.text, &
						  ls_dw[ll_i] + sle_oggetto_finale.text, &
						  importdatawindow!, &
						  mle_1.text, &
						  ls_str, &
						  ls_comm[ll_i]) = -1 then
		g_mb.messagebox("APICE","Errore durante importazione oggetto: " + ls_str)
	end if
next

end event

type mle_1 from multilineedit within w_cambia_dw
int X=23
int Y=1261
int Width=1692
int Height=501
int TabOrder=150
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
boolean AutoHScroll=true
boolean AutoVScroll=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_cambia_dw
int X=151
int Y=201
int Width=641
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Libreria Origine:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_cambia_dw
int X=92
int Y=441
int Width=700
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Estensione Oggetto Finale:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_cambia_dw
int X=92
int Y=361
int Width=700
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Oggetto da Convertire:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_2 from commandbutton within w_cambia_dw
int X=1966
int Y=181
int Width=69
int Height=81
int TabOrder=90
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long   ll_return
string ls_path, ls_nome

sle_library.text = "aaaaaaa"
ll_return = getfileopenname("Selezionare Libreria", ls_path, ls_nome, "PBL", "Pbl Files (*.PBL),*.PBL")
if ll_return >= 0 then sle_library.text = ls_path



end event

type dw_lista_sostituzioni from uo_cs_xx_dw within w_cambia_dw
int X=69
int Y=801
int Width=1806
int Height=417
int TabOrder=40
string DataObject="d_lista_sostituzioni"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

type st_4 from statictext within w_cambia_dw
int X=275
int Y=621
int Width=206
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Cerca:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_cambia_dw
int X=69
int Y=701
int Width=412
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Sostituisci con:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_3 from commandbutton within w_cambia_dw
int X=1166
int Y=701
int Width=366
int Height=81
int TabOrder=50
boolean BringToTop=true
string Text="&Aggiungi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_stringa_cercata, ls_stringa_sostituzione
long ll_riga

ls_stringa_cercata = sle_cerca.text
ls_stringa_sostituzione = sle_sostitusci.text

if isnull(ls_stringa_cercata) or len(ls_stringa_cercata) < 1 then
	g_mb.messagebox("APICE","Inserire la stringa cercata")
	return
end if

ll_riga = dw_lista_sostituzioni.insertrow(0)
dw_lista_sostituzioni.setitem(ll_riga,"stringa_cerca", ls_stringa_cercata)
dw_lista_sostituzioni.setitem(ll_riga,"stringa_sostituita", ls_stringa_sostituzione)
dw_lista_sostituzioni.setitemstatus(ll_riga, 0, primary!, notmodified!)
end event

type gb_2 from groupbox within w_cambia_dw
int X=23
int Y=541
int Width=2081
int Height=701
int TabOrder=70
string Text="Sostituzioni"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within w_cambia_dw
int X=23
int Y=21
int Width=2081
int Height=521
int TabOrder=10
string Text="Oggetti"
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_10 from statictext within w_cambia_dw
int X=151
int Y=281
int Width=641
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Libreria Destinazione:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_4 from commandbutton within w_cambia_dw
event clicked pbm_bnclicked
int X=1966
int Y=261
int Width=69
int Height=81
int TabOrder=100
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long   ll_return
string ls_path, ls_nome

ll_return = getfileopenname("Selezionare Libreria", ls_path, ls_nome, "PBL", "Pbl Files (*.PBL),*.PBL")
if ll_return >= 0 then sle_library_dest.text = ls_path


end event

type sle_library from singlelineedit within w_cambia_dw
int X=823
int Y=181
int Width=1143
int Height=81
int TabOrder=30
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_library_dest from singlelineedit within w_cambia_dw
int X=823
int Y=261
int Width=1143
int Height=81
int TabOrder=130
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_oggetto_iniziale from singlelineedit within w_cambia_dw
int X=823
int Y=341
int Width=1143
int Height=81
int TabOrder=110
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_oggetto_finale from singlelineedit within w_cambia_dw
int X=823
int Y=421
int Width=1143
int Height=81
int TabOrder=120
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_cerca from singlelineedit within w_cambia_dw
int X=526
int Y=621
int Width=618
int Height=81
int TabOrder=80
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_sostitusci from singlelineedit within w_cambia_dw
int X=526
int Y=701
int Width=618
int Height=81
int TabOrder=60
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_11 from statictext within w_cambia_dw
int X=92
int Y=101
int Width=700
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Tipo Oggetto da Elaborare:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ddlb_tipo_oggetto from dropdownlistbox within w_cambia_dw
int X=823
int Y=81
int Width=1143
int Height=261
int TabOrder=20
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
string Item[]={"DataWindow",&
"Window"}
end type

type st_6 from statictext within w_cambia_dw
int X=23
int Y=1781
int Width=2081
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

