﻿$PBExportHeader$w_carica_aziende.srw
$PBExportComments$Finestra Caricamento Aziende
forward
global type w_carica_aziende from w_cs_xx_risposta
end type
type lb_cod_utente from listbox within w_carica_aziende
end type
type st_1 from statictext within w_carica_aziende
end type
type cb_annulla from uo_cb_close within w_carica_aziende
end type
type cb_ok from uo_cb_ok within w_carica_aziende
end type
end forward

global type w_carica_aziende from w_cs_xx_risposta
int Width=1162
int Height=633
boolean TitleBar=true
string Title="Caricamento Aziende"
lb_cod_utente lb_cod_utente
st_1 st_1
cb_annulla cb_annulla
cb_ok cb_ok
end type
global w_carica_aziende w_carica_aziende

event pc_accept;call super::pc_accept;long ll_i, ll_num_utenti
string ls_cod_utente[]


ll_num_utenti = f_po_selectlb(lb_cod_utente, ls_cod_utente[])

if ll_num_utenti <= 0 then
   g_mb.messagebox("Attenzione", "Selezionare almeno un codice utente.", &
              exclamation!, ok!)
   return
end if


for ll_i = 1 to ll_num_utenti
   delete
   from   aziende_utenti
   where  aziende_utenti.cod_utente = :ls_cod_utente[ll_i];
   if sqlca.sqlcode = 0 then
      insert into aziende_utenti  
             (cod_utente,   
              cod_azienda)  
      select :ls_cod_utente[ll_i], aziende.cod_azienda
      from   aziende;
   end if
next


close(this)

end event

on w_carica_aziende.create
int iCurrent
call w_cs_xx_risposta::create
this.lb_cod_utente=create lb_cod_utente
this.st_1=create st_1
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=lb_cod_utente
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=cb_annulla
this.Control[iCurrent+4]=cb_ok
end on

on w_carica_aziende.destroy
call w_cs_xx_risposta::destroy
destroy(this.lb_cod_utente)
destroy(this.st_1)
destroy(this.cb_annulla)
destroy(this.cb_ok)
end on

on pc_setddlb;call w_cs_xx_risposta::pc_setddlb;f_po_loadlb(lb_cod_utente, &
            sqlca, &
            "utenti", &
            "cod_utente", &
            "cod_utente", &
            "", &
            "")
end on

type lb_cod_utente from listbox within w_carica_aziende
int X=23
int Y=21
int Width=595
int Height=361
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean MultiSelect=true
boolean ExtendedSelect=true
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_carica_aziende
int X=641
int Y=21
int Width=458
int Height=361
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
string Text="Selezionare gli utenti a cui verranno caricate in automatico tutte le aziende"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_annulla from uo_cb_close within w_carica_aziende
int X=343
int Y=401
int Width=366
int Height=101
int TabOrder=30
string Text="&Annulla"
boolean Cancel=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_ok from uo_cb_ok within w_carica_aziende
int X=732
int Y=401
int Width=366
int Height=101
int TabOrder=20
string Text="&Ok"
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

