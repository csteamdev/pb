﻿$PBExportHeader$w_carica_voci.srw
$PBExportComments$Finestra Caricamento Voci
forward
global type w_carica_voci from w_cs_xx_risposta
end type
type lb_cod_utente from listbox within w_carica_voci
end type
type st_1 from statictext within w_carica_voci
end type
type cb_annulla from uo_cb_close within w_carica_voci
end type
type cb_ok from uo_cb_ok within w_carica_voci
end type
type cbx_flag_abilitato from checkbox within w_carica_voci
end type
type st_2 from statictext within w_carica_voci
end type
type cbx_flag_nuovo from checkbox within w_carica_voci
end type
type st_3 from statictext within w_carica_voci
end type
type cbx_flag_modifica from checkbox within w_carica_voci
end type
type st_4 from statictext within w_carica_voci
end type
type st_5 from statictext within w_carica_voci
end type
type cbx_flag_cancella from checkbox within w_carica_voci
end type
type st_6 from statictext within w_carica_voci
end type
type cbx_flag_visualizza from checkbox within w_carica_voci
end type
end forward

global type w_carica_voci from w_cs_xx_risposta
int Width=1281
int Height=877
boolean TitleBar=true
string Title="Caricamento Voci"
lb_cod_utente lb_cod_utente
st_1 st_1
cb_annulla cb_annulla
cb_ok cb_ok
cbx_flag_abilitato cbx_flag_abilitato
st_2 st_2
cbx_flag_nuovo cbx_flag_nuovo
st_3 st_3
cbx_flag_modifica cbx_flag_modifica
st_4 st_4
st_5 st_5
cbx_flag_cancella cbx_flag_cancella
st_6 st_6
cbx_flag_visualizza cbx_flag_visualizza
end type
global w_carica_voci w_carica_voci

event pc_accept;call super::pc_accept;long ll_i, ll_num_utenti
string ls_cod_utente[], ls_flag_abilitato, ls_flag_nuovo, ls_flag_modifica, &
       ls_flag_cancella, ls_flag_visualizza


ll_num_utenti = f_po_selectlb(lb_cod_utente, ls_cod_utente[])

if ll_num_utenti <= 0 then
   g_mb.messagebox("Attenzione", "Selezionare almeno un codice utente.", &
              exclamation!, ok!)
   return
end if


if cbx_flag_abilitato.checked then
   ls_flag_abilitato = "S"
else
   ls_flag_abilitato = "N"
end if

if cbx_flag_nuovo.checked then
   ls_flag_nuovo = "S"
else
   ls_flag_nuovo = "N"
end if

if cbx_flag_modifica.checked then
   ls_flag_modifica = "S"
else
   ls_flag_modifica = "N"
end if

if cbx_flag_cancella.checked then
   ls_flag_cancella = "S"
else
   ls_flag_cancella = "N"
end if

if cbx_flag_visualizza.checked then
   ls_flag_visualizza = "S"
else
   ls_flag_visualizza = "N"
end if


for ll_i = 1 to ll_num_utenti
   delete
   from   utenti_voci
   where  utenti_voci.cod_utente = :ls_cod_utente[ll_i];
   if sqlca.sqlcode = 0 then
      insert into utenti_voci  
             (cod_utente,   
              cod_voce,
              flag_abilitato,
              flag_nuovo,
              flag_modifica,
              flag_cancella,
              flag_visualizza)  
      select :ls_cod_utente[ll_i], voci.cod_voce, :ls_flag_abilitato, :ls_flag_nuovo,
             :ls_flag_modifica, :ls_flag_cancella, :ls_flag_visualizza
      from   voci;
   end if
next


close(this)

end event

on w_carica_voci.create
int iCurrent
call w_cs_xx_risposta::create
this.lb_cod_utente=create lb_cod_utente
this.st_1=create st_1
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.cbx_flag_abilitato=create cbx_flag_abilitato
this.st_2=create st_2
this.cbx_flag_nuovo=create cbx_flag_nuovo
this.st_3=create st_3
this.cbx_flag_modifica=create cbx_flag_modifica
this.st_4=create st_4
this.st_5=create st_5
this.cbx_flag_cancella=create cbx_flag_cancella
this.st_6=create st_6
this.cbx_flag_visualizza=create cbx_flag_visualizza
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=lb_cod_utente
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=cb_annulla
this.Control[iCurrent+4]=cb_ok
this.Control[iCurrent+5]=cbx_flag_abilitato
this.Control[iCurrent+6]=st_2
this.Control[iCurrent+7]=cbx_flag_nuovo
this.Control[iCurrent+8]=st_3
this.Control[iCurrent+9]=cbx_flag_modifica
this.Control[iCurrent+10]=st_4
this.Control[iCurrent+11]=st_5
this.Control[iCurrent+12]=cbx_flag_cancella
this.Control[iCurrent+13]=st_6
this.Control[iCurrent+14]=cbx_flag_visualizza
end on

on w_carica_voci.destroy
call w_cs_xx_risposta::destroy
destroy(this.lb_cod_utente)
destroy(this.st_1)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.cbx_flag_abilitato)
destroy(this.st_2)
destroy(this.cbx_flag_nuovo)
destroy(this.st_3)
destroy(this.cbx_flag_modifica)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.cbx_flag_cancella)
destroy(this.st_6)
destroy(this.cbx_flag_visualizza)
end on

on pc_setddlb;call w_cs_xx_risposta::pc_setddlb;f_po_loadlb(lb_cod_utente, &
            sqlca, &
            "utenti", &
            "cod_utente", &
            "cod_utente", &
            "", &
            "")
end on

type lb_cod_utente from listbox within w_carica_voci
int X=23
int Y=21
int Width=595
int Height=361
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean MultiSelect=true
boolean ExtendedSelect=true
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_carica_voci
int X=23
int Y=421
int Width=1189
int Height=201
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
string Text="Selezionare gli utenti a cui verranno caricate in automatico tutte le voci con l'abilitazione indicata"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_annulla from uo_cb_close within w_carica_voci
int X=458
int Y=641
int Width=366
int Height=101
int TabOrder=40
string Text="&Annulla"
boolean Cancel=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_ok from uo_cb_ok within w_carica_voci
int X=846
int Y=641
int Width=366
int Height=101
int TabOrder=30
string Text="&Ok"
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_flag_abilitato from checkbox within w_carica_voci
int X=983
int Y=21
int Width=229
int Height=61
int TabOrder=20
string Text="Sì"
BorderStyle BorderStyle=StyleLowered!
boolean Checked=true
boolean LeftText=true
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_carica_voci
int X=663
int Y=21
int Width=298
int Height=61
boolean Enabled=false
string Text="Abilitato:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_flag_nuovo from checkbox within w_carica_voci
int X=983
int Y=101
int Width=229
int Height=61
string Text="Sì"
BorderStyle BorderStyle=StyleLowered!
boolean Checked=true
boolean LeftText=true
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_carica_voci
int X=663
int Y=101
int Width=298
int Height=61
boolean Enabled=false
string Text="Nuovo:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_flag_modifica from checkbox within w_carica_voci
int X=983
int Y=181
int Width=229
int Height=61
string Text="Sì"
BorderStyle BorderStyle=StyleLowered!
boolean Checked=true
boolean LeftText=true
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_carica_voci
int X=663
int Y=181
int Width=298
int Height=61
boolean Enabled=false
string Text="Modifica:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_carica_voci
int X=663
int Y=261
int Width=298
int Height=61
boolean Enabled=false
string Text="Cancella:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_flag_cancella from checkbox within w_carica_voci
int X=983
int Y=261
int Width=229
int Height=61
string Text="Sì"
BorderStyle BorderStyle=StyleLowered!
boolean Checked=true
boolean LeftText=true
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_6 from statictext within w_carica_voci
int X=663
int Y=341
int Width=298
int Height=61
boolean Enabled=false
string Text="Visualizza:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_flag_visualizza from checkbox within w_carica_voci
int X=983
int Y=341
int Width=229
int Height=61
string Text="Sì"
BorderStyle BorderStyle=StyleLowered!
boolean Checked=true
boolean LeftText=true
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

