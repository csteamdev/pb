﻿$PBExportHeader$w_collegamenti.srw
$PBExportComments$Window collegamenti
forward
global type w_collegamenti from w_cs_xx_risposta
end type
type cb_mostra_segnalibro from commandbutton within w_collegamenti
end type
type cb_1 from cb_web within w_collegamenti
end type
type cb_chiudi from commandbutton within w_collegamenti
end type
type dw_det_documenti_lista_collegamenti from uo_cs_xx_dw within w_collegamenti
end type
type ole_1 from olecontrol within w_collegamenti
end type
type dw_collegamenti_det from uo_cs_xx_dw within w_collegamenti
end type
type dw_collegamenti_lista from uo_cs_xx_dw within w_collegamenti
end type
end forward

global type w_collegamenti from w_cs_xx_risposta
integer width = 2642
integer height = 2052
string title = "Collegamento documenti e URL"
cb_mostra_segnalibro cb_mostra_segnalibro
cb_1 cb_1
cb_chiudi cb_chiudi
dw_det_documenti_lista_collegamenti dw_det_documenti_lista_collegamenti
ole_1 ole_1
dw_collegamenti_det dw_collegamenti_det
dw_collegamenti_lista dw_collegamenti_lista
end type
global w_collegamenti w_collegamenti

forward prototypes
public function integer wf_dragdrop ()
public function integer wf_refresh_ole ()
end prototypes

public function integer wf_dragdrop ();long ll_progressivo,ll_num_registrazione
integer li_anno_registrazione
string ls_nome_documento

dw_det_documenti_lista_collegamenti.Drag(end!)
ll_num_registrazione = dw_det_documenti_lista_collegamenti.getitemnumber(dw_det_documenti_lista_collegamenti.getrow(),"num_registrazione")
li_anno_registrazione = dw_det_documenti_lista_collegamenti.getitemnumber(dw_det_documenti_lista_collegamenti.getrow(),"anno_registrazione")
ls_nome_documento = dw_det_documenti_lista_collegamenti.getitemstring(dw_det_documenti_lista_collegamenti.getrow(),"nome_documento")

select max(progressivo)
into   :ll_progressivo
from   collegamenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_window=:s_cs_xx.parametri.parametro_s_1;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Framework","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 0
end if

if isnull(ll_progressivo) or ll_progressivo = 0 then
	ll_progressivo = 1
else
	ll_progressivo++
end if

insert into collegamenti
(cod_azienda,
 nome_window,
 progressivo,
 anno_registrazione,
 num_registrazione,
 nome_documento)
values
(:s_cs_xx.cod_azienda,
 :s_cs_xx.parametri.parametro_s_1,
 :ll_progressivo,
 :li_anno_registrazione,
 :ll_num_registrazione,
 :ls_nome_documento);
 

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia","Errore in fase associazione del documento alla lista dei collegamenti: probabilmente esiste già! (dettaglio errore:" + sqlca.sqlerrtext + ")",stopsign!)	
	return 0
end if


dw_collegamenti_lista.change_dw_current()
w_collegamenti.triggerevent("pc_retrieve")

return 0
end function

public function integer wf_refresh_ole ();long    ll_num_registrazione,ll_progressivo,ll_righe
integer li_risposta,li_anno_registrazione, li_ris
string ls_db

transaction sqlcb
blob    lbb_blob_documento

ll_righe=dw_collegamenti_lista.rowcount()

if ll_righe=0 then return 0

li_anno_registrazione=dw_collegamenti_lista.getitemnumber(dw_collegamenti_lista.getrow(),"anno_registrazione")
ll_num_registrazione=dw_collegamenti_lista.getitemnumber(dw_collegamenti_lista.getrow(),"num_registrazione")


if isnull(li_anno_registrazione ) or li_anno_registrazione = 0 then 
	setnull(lbb_blob_documento)
	ole_1.objectdata = lbb_blob_documento
	return 0
end if

select progressivo
into   :ll_progressivo
from   det_documenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione
and    flag_storico='N'
and    approvato_da is not null
and    autorizzato_da is not null;

if sqlca.sqlcode <0 then
	g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
	return 0
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_ris = f_crea_sqlcb(sqlcb)

	selectblob blob_documento
	into   :lbb_blob_documento
	from   det_documenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    progressivo = :ll_progressivo
	using  sqlcb;

	if sqlcb.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		destroy sqlcb;
		return 0
	end if
	
	destroy sqlcb;
	
else

	selectblob blob_documento
	into   :lbb_blob_documento
	from   det_documenti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    progressivo = :ll_progressivo;

	if sqlca.sqlcode <0 then
		g_mb.messagebox("Omnia","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
		return 0
	end if

end if


ole_1.objectdata = lbb_blob_documento

commit;

return 0
end function

on w_collegamenti.create
int iCurrent
call super::create
this.cb_mostra_segnalibro=create cb_mostra_segnalibro
this.cb_1=create cb_1
this.cb_chiudi=create cb_chiudi
this.dw_det_documenti_lista_collegamenti=create dw_det_documenti_lista_collegamenti
this.ole_1=create ole_1
this.dw_collegamenti_det=create dw_collegamenti_det
this.dw_collegamenti_lista=create dw_collegamenti_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_mostra_segnalibro
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_chiudi
this.Control[iCurrent+4]=this.dw_det_documenti_lista_collegamenti
this.Control[iCurrent+5]=this.ole_1
this.Control[iCurrent+6]=this.dw_collegamenti_det
this.Control[iCurrent+7]=this.dw_collegamenti_lista
end on

on w_collegamenti.destroy
call super::destroy
destroy(this.cb_mostra_segnalibro)
destroy(this.cb_1)
destroy(this.cb_chiudi)
destroy(this.dw_det_documenti_lista_collegamenti)
destroy(this.ole_1)
destroy(this.dw_collegamenti_det)
destroy(this.dw_collegamenti_lista)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_enablepopup)


dw_collegamenti_lista.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_default, &
                       c_default)
dw_collegamenti_det.set_dw_options(sqlca, &
                             dw_collegamenti_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)


dw_det_documenti_lista_collegamenti.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_nonew + c_nomodify + c_nodelete, &
                       c_default)
							  


end event

type cb_mostra_segnalibro from commandbutton within w_collegamenti
integer x = 960
integer y = 592
integer width = 73
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;string ls_segnalibro


ls_segnalibro = dw_collegamenti_lista.getitemstring(dw_collegamenti_lista.getrow(),"segnalibro")

if isnull(ls_segnalibro) or ls_segnalibro = "" then
	ole_1.activate(offsite!)
else
	ole_1.activate(offsite!)
	ole_1.object.application.ActiveDocument.Bookmarks.item(ls_segnalibro).Select
end if

end event

type cb_1 from cb_web within w_collegamenti
integer x = 2469
integer y = 820
integer width = 73
integer height = 80
integer taborder = 40
end type

event clicked;call super::clicked;string ls_url

ls_url = dw_collegamenti_lista.getitemstring(dw_collegamenti_lista.getrow(),"url")
//uo_web.executewebpage(ls_url )
end event

type cb_chiudi from commandbutton within w_collegamenti
integer x = 2217
integer y = 1860
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

type dw_det_documenti_lista_collegamenti from uo_cs_xx_dw within w_collegamenti
integer x = 823
integer y = 940
integer width = 1760
integer height = 900
integer taborder = 10
string dragicon = "DosEdit5!"
string dataobject = "d_det_documenti_lista_collegamenti"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event clicked;call super::clicked;if this.getrow() > 0 then Drag(Begin!)
end event

type ole_1 from olecontrol within w_collegamenti
integer x = 23
integer y = 940
integer width = 777
integer height = 900
integer taborder = 40
long backcolor = 16777215
boolean focusrectangle = false
string binarykey = "w_collegamenti.win"
omactivation activation = activateondoubleclick!
omdisplaytype displaytype = displayascontent!
omcontentsallowed contentsallowed = containsany!
end type

type dw_collegamenti_det from uo_cs_xx_dw within w_collegamenti
integer x = 23
integer y = 480
integer width = 2560
integer height = 440
integer taborder = 20
string dataobject = "d_collegamenti_det"
borderstyle borderstyle = styleraised!
end type

event dragdrop;call super::dragdrop;wf_dragdrop()
end event

type dw_collegamenti_lista from uo_cs_xx_dw within w_collegamenti
integer x = 23
integer y = 20
integer width = 2560
integer height = 440
integer taborder = 10
string dataobject = "d_collegamenti_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_s_1)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i,ll_progressivo

select max(progressivo)
into   :ll_progressivo
from   collegamenti
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_window=:s_cs_xx.parametri.parametro_s_1;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Framework","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return 
end if

if isnull(ll_progressivo) or ll_progressivo = 0 then
	ll_progressivo =1 
else
	ll_progressivo ++
end if
for ll_i = 1 to rowcount()
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
		setitem(ll_i, "nome_window", s_cs_xx.parametri.parametro_s_1)
		setitem(ll_i, "progressivo", ll_progressivo)
		
		ll_progressivo++
   end if
next
end event

event dragdrop;call super::dragdrop;wf_dragdrop()
end event

event rowfocuschanged;call super::rowfocuschanged;wf_refresh_ole()
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
01w_collegamenti.bin 
2B00000600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe00000006000000000000000000000001000000010000000000001000fffffffe00000000fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
11w_collegamenti.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
