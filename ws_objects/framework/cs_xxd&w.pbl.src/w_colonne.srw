﻿$PBExportHeader$w_colonne.srw
$PBExportComments$Finestra Colonne
forward
global type w_colonne from w_cs_xx_principale
end type
type dw_colonne_lista from uo_cs_xx_dw within w_colonne
end type
type dw_colonne_det from uo_cs_xx_dw within w_colonne
end type
end forward

global type w_colonne from w_cs_xx_principale
integer width = 2258
integer height = 1372
string title = "Gestione Colonne"
dw_colonne_lista dw_colonne_lista
dw_colonne_det dw_colonne_det
end type
global w_colonne w_colonne

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_colonne_lista.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_default, &
                                c_default)

dw_colonne_det.set_dw_options(sqlca, &
                              dw_colonne_lista, &
                              c_sharedata + c_scrollparent, &
                              c_default)
iuo_dw_main = dw_colonne_lista
end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loaddddw_dw(dw_colonne_det, &
                 "cod_voce", &
                 sqlca, &
                 "voci", &
                 "cod_voce", &
                 "des_voce", &
                 "")
end on

on w_colonne.create
int iCurrent
call super::create
this.dw_colonne_lista=create dw_colonne_lista
this.dw_colonne_det=create dw_colonne_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_colonne_lista
this.Control[iCurrent+2]=this.dw_colonne_det
end on

on w_colonne.destroy
call super::destroy
destroy(this.dw_colonne_lista)
destroy(this.dw_colonne_det)
end on

type dw_colonne_lista from uo_cs_xx_dw within w_colonne
integer x = 23
integer y = 20
integer width = 2171
integer height = 500
integer taborder = 10
string dataobject = "d_colonne_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore

ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "nome_datawindow")) or len(trim(this.getitemstring(ll_i, "nome_datawindow"))) < 1 then
      this.setitem(ll_i, "nome_datawindow", "TUTTI")
   end if
next
end event

type dw_colonne_det from uo_cs_xx_dw within w_colonne
integer x = 23
integer y = 540
integer width = 2171
integer height = 700
integer taborder = 20
string dataobject = "d_colonne_det"
borderstyle borderstyle = styleraised!
end type

