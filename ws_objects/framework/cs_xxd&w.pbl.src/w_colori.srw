﻿$PBExportHeader$w_colori.srw
forward
global type w_colori from w_cs_xx_principale
end type
type st_4 from statictext within w_colori
end type
type st_3 from statictext within w_colori
end type
type st_2 from statictext within w_colori
end type
type st_anteprima from statictext within w_colori
end type
type st_b from statictext within w_colori
end type
type st_g from statictext within w_colori
end type
type st_r from statictext within w_colori
end type
type htb_r from htrackbar within w_colori
end type
type htb_g from htrackbar within w_colori
end type
type htb_b from htrackbar within w_colori
end type
type sle_codice from singlelineedit within w_colori
end type
type st_1 from statictext within w_colori
end type
end forward

global type w_colori from w_cs_xx_principale
integer x = 673
integer y = 265
integer height = 592
string title = "Info Colori"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
st_4 st_4
st_3 st_3
st_2 st_2
st_anteprima st_anteprima
st_b st_b
st_g st_g
st_r st_r
htb_r htb_r
htb_g htb_g
htb_b htb_b
sle_codice sle_codice
st_1 st_1
end type
global w_colori w_colori

forward prototypes
public subroutine wf_color ()
public subroutine wf_reverse ()
end prototypes

public subroutine wf_color ();long ll_color


ll_color = htb_r.position + (htb_g.position * 256) + (htb_b.position * 65536)

sle_codice.text = string(ll_color)

st_anteprima.backcolor = ll_color
end subroutine

public subroutine wf_reverse ();long ll_color


ll_color = st_anteprima.backcolor

sle_codice.text = string(ll_color)

htb_b.position = truncate((ll_color / 65536),0)

ll_color -= (htb_b.position * 65536)

htb_g.position = truncate((ll_color / 256),0)

ll_color -= (htb_g.position * 256)

htb_r.position = ll_color
end subroutine

on w_colori.create
int iCurrent
call super::create
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_anteprima=create st_anteprima
this.st_b=create st_b
this.st_g=create st_g
this.st_r=create st_r
this.htb_r=create htb_r
this.htb_g=create htb_g
this.htb_b=create htb_b
this.sle_codice=create sle_codice
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_4
this.Control[iCurrent+2]=this.st_3
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.st_anteprima
this.Control[iCurrent+5]=this.st_b
this.Control[iCurrent+6]=this.st_g
this.Control[iCurrent+7]=this.st_r
this.Control[iCurrent+8]=this.htb_r
this.Control[iCurrent+9]=this.htb_g
this.Control[iCurrent+10]=this.htb_b
this.Control[iCurrent+11]=this.sle_codice
this.Control[iCurrent+12]=this.st_1
end on

on w_colori.destroy
call super::destroy
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_anteprima)
destroy(this.st_b)
destroy(this.st_g)
destroy(this.st_r)
destroy(this.htb_r)
destroy(this.htb_g)
destroy(this.htb_b)
destroy(this.sle_codice)
destroy(this.st_1)
end on

event open;call super::open;wf_reverse()
end event

type st_4 from statictext within w_colori
integer x = 1189
integer y = 360
integer width = 320
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 79741120
alignment alignment = center!
boolean focusrectangle = false
end type

type st_3 from statictext within w_colori
integer x = 1189
integer y = 200
integer width = 320
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 79741120
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_colori
integer x = 1189
integer y = 40
integer width = 320
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 79741120
alignment alignment = center!
boolean focusrectangle = false
end type

type st_anteprima from statictext within w_colori
integer x = 1600
integer y = 20
integer width = 411
integer height = 280
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean border = true
boolean focusrectangle = false
end type

event clicked;backcolor = f_scegli_colore()

wf_reverse()
end event

type st_b from statictext within w_colori
integer x = 46
integer y = 320
integer width = 82
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 79741120
string text = "B"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_g from statictext within w_colori
integer x = 46
integer y = 200
integer width = 82
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 79741120
string text = "G"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_r from statictext within w_colori
integer x = 46
integer y = 80
integer width = 82
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 79741120
string text = "R"
alignment alignment = center!
boolean focusrectangle = false
end type

type htb_r from htrackbar within w_colori
integer x = 91
integer y = 40
integer width = 1088
integer height = 108
integer maxposition = 255
integer position = 255
integer tickfrequency = 64
htickmarks tickmarks = hticksonneither!
end type

event moved;st_2.text = string(scrollpos)
wf_color()
end event

type htb_g from htrackbar within w_colori
integer x = 91
integer y = 200
integer width = 1088
integer height = 108
integer maxposition = 255
integer position = 255
integer tickfrequency = 64
htickmarks tickmarks = hticksonneither!
end type

event moved;st_3.text = string(scrollpos)
wf_color()
end event

type htb_b from htrackbar within w_colori
integer x = 91
integer y = 360
integer width = 1088
integer height = 108
integer maxposition = 255
integer position = 255
integer tickfrequency = 64
htickmarks tickmarks = hticksonneither!
end type

event moved;st_4.text = string(scrollpos)
wf_color()
end event

type sle_codice from singlelineedit within w_colori
integer x = 1737
integer y = 340
integer width = 274
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 79741120
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_colori
integer x = 1600
integer y = 360
integer width = 137
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 79741120
string text = "Cod."
alignment alignment = center!
boolean focusrectangle = false
end type

