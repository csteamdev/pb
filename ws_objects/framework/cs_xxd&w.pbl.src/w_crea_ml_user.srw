﻿$PBExportHeader$w_crea_ml_user.srw
forward
global type w_crea_ml_user from window
end type
type cb_1 from commandbutton within w_crea_ml_user
end type
type st_1 from statictext within w_crea_ml_user
end type
type gb_1 from groupbox within w_crea_ml_user
end type
end forward

global type w_crea_ml_user from window
integer width = 1275
integer height = 604
boolean titlebar = true
string title = "Creazione utente remoto"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_1 cb_1
st_1 st_1
gb_1 gb_1
end type
global w_crea_ml_user w_crea_ml_user

on w_crea_ml_user.create
this.cb_1=create cb_1
this.st_1=create st_1
this.gb_1=create gb_1
this.Control[]={this.cb_1,&
this.st_1,&
this.gb_1}
end on

on w_crea_ml_user.destroy
destroy(this.cb_1)
destroy(this.st_1)
destroy(this.gb_1)
end on

type cb_1 from commandbutton within w_crea_ml_user
integer x = 23
integer y = 20
integer width = 1211
integer height = 100
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "CREA NUOVO UTENTE REMOTO"
end type

event clicked;long 		ll_userid, ll_try, ll_test

string	ls_username, ls_command

boolean	lb_trovato


select
	max(user_id)
into
	:ll_userid
from
	ml_user;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("UTENTI REMOTI","Errore in verifica massimo ID utenti remoti",stopsign!,ok!,true,sqlca,true,this,false,false)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_userid) then
	ll_userid = 0
end if

ll_userid++

ls_username = "remote_user_" + string(ll_userid,"############")

ls_command = "dbmluser -c ~"dsn=dbmluser~" -u " + ls_username + " -p " + ls_username

run(ls_command,minimized!)

setpointer(hourglass!)

lb_trovato = false

do
	
	sleep(1)
	
	ll_try++
	
	select
		count(*)
	into
		:ll_test
	from
		ml_user
	where
		name = :ls_username;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("UTENTI REMOTI","Errore in verifica creazione nuovo utente",stopsign!,ok!,true,sqlca,true,this,false,false)
		exit
	end if
	
	if ll_test > 0 then
		lb_trovato = true
		exit
	end if
	
loop while ll_try < 10

setpointer(arrow!)

if lb_trovato then
	st_1.backcolor = rgb(0,255,0)
	st_1.text = string(ll_userid)
else
	st_1.backcolor = rgb(255,0,0)
	st_1.text = "errore"
end if
end event

type st_1 from statictext within w_crea_ml_user
integer x = 46
integer y = 220
integer width = 1166
integer height = 260
boolean bringtotop = true
integer textsize = -36
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 67108864
string text = "???"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_crea_ml_user
integer x = 23
integer y = 140
integer width = 1211
integer height = 360
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 67108864
string text = "Codice generato"
end type

