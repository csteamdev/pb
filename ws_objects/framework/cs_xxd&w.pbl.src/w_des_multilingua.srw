﻿$PBExportHeader$w_des_multilingua.srw
forward
global type w_des_multilingua from window
end type
type dw_des_multilingue_lista from datawindow within w_des_multilingua
end type
type cb_add from commandbutton within w_des_multilingua
end type
type dw_des_multilingua from datawindow within w_des_multilingua
end type
end forward

global type w_des_multilingua from window
integer width = 2089
integer height = 1276
boolean titlebar = true
string title = "Input Descrizione in Lingua"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
dw_des_multilingue_lista dw_des_multilingue_lista
cb_add cb_add
dw_des_multilingua dw_des_multilingua
end type
global w_des_multilingua w_des_multilingua

type variables
str_des_multilingua istr_multilingua
end variables

event open;string ls_descrizione_lingua
long ll_row

istr_multilingua = message.powerobjectparm

if isnull(istr_multilingua.nome_tabella) or istr_multilingua.nome_tabella = "" then
	g_mb.messagebox("FRAMEWORK", "E' stato passato un nome tabella non valido")
	close(this)
end if

dw_des_multilingua.reset()
ll_row = dw_des_multilingua.insertrow(0)

dw_des_multilingua.setitem(ll_row, "descrizione_origine", istr_multilingua.descrizione_origine)



f_po_loaddddw_dw(dw_des_multilingua, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


dw_des_multilingue_lista.settransobject(sqlca)

dw_des_multilingue_lista.postevent("ue_retrieve")

dw_des_multilingua.setfocus()
end event

on w_des_multilingua.create
this.dw_des_multilingue_lista=create dw_des_multilingue_lista
this.cb_add=create cb_add
this.dw_des_multilingua=create dw_des_multilingua
this.Control[]={this.dw_des_multilingue_lista,&
this.cb_add,&
this.dw_des_multilingua}
end on

on w_des_multilingua.destroy
destroy(this.dw_des_multilingue_lista)
destroy(this.cb_add)
destroy(this.dw_des_multilingua)
end on

type dw_des_multilingue_lista from datawindow within w_des_multilingua
event ue_retrieve ( )
integer x = 23
integer y = 440
integer width = 2034
integer height = 740
integer taborder = 30
string title = "none"
string dataobject = "d_des_multilingue_lista"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_retrieve();long ll_rows
string ls_chiave_str_1, ls_chiave_str_2, ls_filter

//if isnull(istr_multilingua.chiave_str_1) then
//	ls_chiave_str_1 = "%"
//else
//	ls_chiave_str_1 = istr_multilingua.chiave_str_1
//end if
//
//if isnull(istr_multilingua.chiave_str_2) then
//	ls_chiave_str_2 = "%"
//else
//	ls_chiave_str_2 = istr_multilingua.chiave_str_2
//end if

setredraw(false)

ll_rows = retrieve(istr_multilingua.nome_tabella)

if ll_rows < 0 then
	g_mb.messagebox("FRAMEWORK","Errore in ricerca dati da tabella multilingue")
	rollback;
	return
end if

ls_filter =""
if not isnull(istr_multilingua.chiave_str_1) then
	ls_filter = " chiave_str_1 = '" + istr_multilingua.chiave_str_1 + "' "
end if

if not isnull(istr_multilingua.chiave_str_2) then
	if len(ls_filter) > 0 then ls_filter += " and " 
	ls_filter = " chiave_str_2 = '" + istr_multilingua.chiave_str_2 + "' "
end if

setfilter(ls_filter)
filter()

setredraw(true)

end event

event clicked;if isvalid(dwo) then
	choose case dwo.name 
		case "b_delete"
			if messagebox("FRAMEWORK","Elimino la descrizione?",Question!,YesNo!,2) = 1 then
				long ll_progressivo
				string ls_descrizione_lingua, ls_cod_lingua, ls_str, ls_chiave_str_1, ls_chiave_str_2
				
				ls_chiave_str_1 = getitemstring(row,"chiave_str_1")
				ls_chiave_str_2 = getitemstring(row,"chiave_str_2")
				ls_cod_lingua   = getitemstring(row,"cod_lingua")
				if not isnull(ls_chiave_str_1) and isnull(ls_chiave_str_2) then
					delete tab_tabelle_lingue_des
					where cod_azienda = :s_cs_xx.cod_azienda and
							nome_tabella = :istr_multilingua.nome_tabella and
							cod_lingua = :ls_cod_lingua and
							chiave_str_1 = :ls_chiave_str_1 and
							chiave_str_2 is null;
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("FRAMEWORK","Errore in cancellazione descrizione in lingua~r~n"+sqlca.sqlerrtext )
						rollback;
					else
						commit;
					end if
				elseif not isnull(ls_chiave_str_1) and not isnull(ls_chiave_str_2) then
					delete tab_tabelle_lingue_des
					where cod_azienda = :s_cs_xx.cod_azienda and
							nome_tabella = :istr_multilingua.nome_tabella and
							cod_lingua   = :ls_cod_lingua and
							chiave_str_1 = :ls_chiave_str_1 and
							chiave_str_2 = :ls_chiave_str_2;
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("FRAMEWORK","Errore in cancellazione descrizione in lingua~r~n"+sqlca.sqlerrtext )
						rollback;
					else
						commit;
					end if
					
					postevent("ue_retrieve")
					
				end if
				
			end if
	end choose
end if
end event

type cb_add from commandbutton within w_des_multilingua
integer x = 1669
integer y = 340
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi"
end type

event clicked;long ll_progressivo
string ls_descrizione_lingua, ls_cod_lingua, ls_str

dw_des_multilingua.accepttext()

ls_descrizione_lingua = dw_des_multilingua.getitemstring(dw_des_multilingua.getrow(),"descrizione_destinazione")
ls_cod_lingua = dw_des_multilingua.getitemstring(dw_des_multilingua.getrow(),"cod_lingua")
if isnull(ls_descrizione_lingua) then 
	g_mb.messagebox("FRAMEWORK","Manca la descrizione in lingua")
	return
end if

if isnull(ls_cod_lingua) then 
	g_mb.messagebox("FRAMEWORK","Indicare un codice lingua")
	return
end if

if isnull(istr_multilingua.chiave_str_1) then
	select progressivo
	into :ll_progressivo
	from tab_tabelle_lingue_des
	where cod_azienda = :s_cs_xx.cod_azienda and
			nome_tabella = :istr_multilingua.nome_tabella and
			cod_lingua = :ls_cod_lingua and
			chiave_str_1 is null and
			chiave_str_2 is null;

elseif not isnull(istr_multilingua.chiave_str_1) and isnull(istr_multilingua.chiave_str_2) then
	select progressivo
	into :ll_progressivo
	from tab_tabelle_lingue_des
	where cod_azienda = :s_cs_xx.cod_azienda and
			nome_tabella = :istr_multilingua.nome_tabella and
			cod_lingua = :ls_cod_lingua and
			chiave_str_1 = :istr_multilingua.chiave_str_1 and
			chiave_str_2 is null;
	

elseif not isnull(istr_multilingua.chiave_str_1) and not isnull(istr_multilingua.chiave_str_2) then
	select progressivo
	into :ll_progressivo
	from tab_tabelle_lingue_des
	where cod_azienda = :s_cs_xx.cod_azienda and
			nome_tabella = :istr_multilingua.nome_tabella and
			cod_lingua   = :ls_cod_lingua and
			chiave_str_1 = :istr_multilingua.chiave_str_1 and
			chiave_str_2 = :istr_multilingua.chiave_str_2;
	
end if

if sqlca.sqlcode = 100 then
	
	select max(progressivo)
	into :ll_progressivo
	from tab_tabelle_lingue_des;
	
	if isnull(ll_progressivo) then
		ll_progressivo = 1
	else
		ll_progressivo ++
	end if
	
	insert into tab_tabelle_lingue_des
	(progressivo,
	 cod_azienda,
	 cod_lingua,
	 nome_tabella,
	 chiave_str_1,
	 chiave_str_2,
	 descrizione_lingua)
	values
	(:ll_progressivo,
	 :s_cs_xx.cod_azienda,
	 :ls_cod_lingua,
	 :istr_multilingua.nome_tabella,
	 :istr_multilingua.chiave_str_1,
	 :istr_multilingua.chiave_str_2,
	 :ls_descrizione_lingua);
	 
elseif sqlca.sqlcode = 0 then
	
	update tab_tabelle_lingue_des
	set    descrizione_lingua = :ls_descrizione_lingua
	where  progressivo = :ll_progressivo;
end if	
		  
if sqlca.sqlcode < 0 then
	g_mb.messagebox("framework","Errore in memorizzazione descrizione in lingua~r~n"+sqlca.sqlerrtext)
	rollback;
else
	commit;
end if

dw_des_multilingue_lista.postevent("ue_retrieve")

end event

type dw_des_multilingua from datawindow within w_des_multilingua
integer y = 20
integer width = 2080
integer height = 320
integer taborder = 10
string title = "none"
string dataobject = "d_des_multilingua"
boolean border = false
boolean livescroll = true
end type

event itemchanged;if isvalid(dwo) then
	choose case dwo.name
		case "cod_lingua"
			
			string ls_descrizione_lingua, ls_cod_lingua
			long ll_row
			
			setitem(row, "descrizione_destinazione", "")
			
			ls_cod_lingua = data
			
			if isnull(ls_cod_lingua) or len(ls_cod_lingua) < 1 then
				return -1
			end if

			
			if isnull(istr_multilingua.chiave_str_1) then
				select descrizione_lingua
				into :ls_descrizione_lingua
				from tab_tabelle_lingue_des
				where cod_azienda = :s_cs_xx.cod_azienda and
						nome_tabella = :istr_multilingua.nome_tabella and
						cod_lingua = :ls_cod_lingua and
						chiave_str_1 is null and
						chiave_str_2 is null;
			
			elseif not isnull(istr_multilingua.chiave_str_1) and isnull(istr_multilingua.chiave_str_2) then
				select descrizione_lingua
				into :ls_descrizione_lingua
				from tab_tabelle_lingue_des
				where cod_azienda = :s_cs_xx.cod_azienda and
						nome_tabella = :istr_multilingua.nome_tabella and
						cod_lingua = :ls_cod_lingua and
						chiave_str_1 = :istr_multilingua.chiave_str_1 and
						chiave_str_2 is null;
				
			
			elseif not isnull(istr_multilingua.chiave_str_1) and not isnull(istr_multilingua.chiave_str_2) then
				select descrizione_lingua
				into :ls_descrizione_lingua
				from tab_tabelle_lingue_des
				where cod_azienda = :s_cs_xx.cod_azienda and
						nome_tabella = :istr_multilingua.nome_tabella and
						cod_lingua = :ls_cod_lingua and
						chiave_str_1 = :istr_multilingua.chiave_str_1 and
						chiave_str_2 = :istr_multilingua.chiave_str_2;
				
			else 
				g_mb.messagebox("FRAMEWORK", "Errore generale nella impostazione dei valori chiave_str_x: contattare il servizio di assistenza")
				close(parent)
			end if
				
			setitem(row, "descrizione_destinazione", ls_descrizione_lingua)

			
	end choose
end if
end event

