﻿$PBExportHeader$w_duplica_utenti_voci.srw
$PBExportComments$Finestra Duplicazione Utenti Voci
forward
global type w_duplica_utenti_voci from w_cs_xx_risposta
end type
type lb_cod_utente_1 from listbox within w_duplica_utenti_voci
end type
type st_1 from statictext within w_duplica_utenti_voci
end type
type cb_annulla from uo_cb_close within w_duplica_utenti_voci
end type
type cb_ok from uo_cb_ok within w_duplica_utenti_voci
end type
type lb_cod_utente_2 from listbox within w_duplica_utenti_voci
end type
type st_2 from statictext within w_duplica_utenti_voci
end type
type st_3 from statictext within w_duplica_utenti_voci
end type
end forward

global type w_duplica_utenti_voci from w_cs_xx_risposta
int Width=1317
int Height=933
boolean TitleBar=true
string Title="Duplicazione Utenti Voci"
lb_cod_utente_1 lb_cod_utente_1
st_1 st_1
cb_annulla cb_annulla
cb_ok cb_ok
lb_cod_utente_2 lb_cod_utente_2
st_2 st_2
st_3 st_3
end type
global w_duplica_utenti_voci w_duplica_utenti_voci

event pc_accept;call super::pc_accept;long ll_i, ll_num_utenti
string ls_utente_origine[], ls_utente_dest[], ls_flag_abilitato, ls_flag_nuovo, ls_flag_modifica, &
       ls_flag_cancella, ls_flag_visualizza


ll_num_utenti = f_po_selectlb(lb_cod_utente_1, ls_utente_origine[])

if ll_num_utenti <= 0 then
   g_mb.messagebox("Attenzione", "Selezionare il codice utente da cui copiare le voci.", &
              exclamation!, ok!)
   return
end if
if ll_num_utenti > 1 then
   g_mb.messagebox("Attenzione", "Selezionare un solo codice utente.", &
              exclamation!, ok!)
   return
end if


ll_num_utenti = f_po_selectlb(lb_cod_utente_2, ls_utente_dest[])

if ll_num_utenti <= 0 then
   g_mb.messagebox("Attenzione", "Selezionare il codice utente destinazione.", &
              exclamation!, ok!)
   return
end if
if ll_num_utenti > 1 then
   g_mb.messagebox("Attenzione", "Selezionare un solo codice utente.", &
              exclamation!, ok!)
   return
end if


   delete from   utenti_voci where  utenti_voci.cod_utente = :ls_utente_dest[1];
   if sqlca.sqlcode = 0 then
      insert into utenti_voci  
             (cod_utente,   
              cod_voce,
              flag_abilitato,
              flag_nuovo,
              flag_modifica,
              flag_cancella,
              flag_visualizza)  
      select :ls_utente_dest[1], utenti_voci.cod_voce, utenti_voci.flag_abilitato, utenti_voci.flag_nuovo,
             utenti_voci.flag_modifica, utenti_voci.flag_cancella, utenti_voci.flag_visualizza
      from   utenti_voci
      where  utenti_voci.cod_utente = :ls_utente_origine[1];
   end if


close(this)

end event

on w_duplica_utenti_voci.create
int iCurrent
call w_cs_xx_risposta::create
this.lb_cod_utente_1=create lb_cod_utente_1
this.st_1=create st_1
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.lb_cod_utente_2=create lb_cod_utente_2
this.st_2=create st_2
this.st_3=create st_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=lb_cod_utente_1
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=cb_annulla
this.Control[iCurrent+4]=cb_ok
this.Control[iCurrent+5]=lb_cod_utente_2
this.Control[iCurrent+6]=st_2
this.Control[iCurrent+7]=st_3
end on

on w_duplica_utenti_voci.destroy
call w_cs_xx_risposta::destroy
destroy(this.lb_cod_utente_1)
destroy(this.st_1)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.lb_cod_utente_2)
destroy(this.st_2)
destroy(this.st_3)
end on

on pc_setddlb;call w_cs_xx_risposta::pc_setddlb;f_po_loadlb(lb_cod_utente_1, &
            sqlca, &
            "utenti", &
            "cod_utente", &
            "cod_utente", &
            "", &
            "")

f_po_loadlb(lb_cod_utente_2, &
            sqlca, &
            "utenti", &
            "cod_utente", &
            "cod_utente", &
            "", &
            "")
end on

type lb_cod_utente_1 from listbox within w_duplica_utenti_voci
int X=23
int Y=81
int Width=595
int Height=361
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean MultiSelect=true
boolean ExtendedSelect=true
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_duplica_utenti_voci
int X=23
int Y=481
int Width=1235
int Height=201
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
string Text="Selezionare l'utente di origine  da cui copiare  le abilitazioni sull'utente di destinazione"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_annulla from uo_cb_close within w_duplica_utenti_voci
int X=503
int Y=701
int Width=366
int Height=101
int TabOrder=40
string Text="&Annulla"
boolean Cancel=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_ok from uo_cb_ok within w_duplica_utenti_voci
int X=892
int Y=701
int Width=366
int Height=101
int TabOrder=30
string Text="&Ok"
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type lb_cod_utente_2 from listbox within w_duplica_utenti_voci
int X=641
int Y=81
int Width=595
int Height=361
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean MultiSelect=true
boolean ExtendedSelect=true
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_duplica_utenti_voci
int X=23
int Y=21
int Width=595
int Height=61
boolean Enabled=false
string Text="Origine"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_duplica_utenti_voci
int X=641
int Y=21
int Width=618
int Height=61
boolean Enabled=false
string Text="Destinazione"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

