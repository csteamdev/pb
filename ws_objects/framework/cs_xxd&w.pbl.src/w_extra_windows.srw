﻿$PBExportHeader$w_extra_windows.srw
forward
global type w_extra_windows from window
end type
type plb_1 from picturelistbox within w_extra_windows
end type
end forward

global type w_extra_windows from window
integer width = 1609
integer height = 444
boolean border = false
windowtype windowtype = child!
long backcolor = 268435456
string icon = "AppIcon!"
boolean toolbarvisible = false
integer transparency = 20
event ue_resize ( )
plb_1 plb_1
end type
global w_extra_windows w_extra_windows

type variables
private:
	string is_last_windows[]
	string is_last_windows_label[]
end variables

forward prototypes
public subroutine add_window (string as_window_link, string as_window_name)
end prototypes

event ue_resize();

move(w_cs_xx_mdi.mdi_1.width - width - 55,  30)
plb_1.x = 10
plb_1.y = 10
plb_1.width = width - 20
plb_1.height = height - 20
setPosition(NoTopMost!)
end event

public subroutine add_window (string as_window_link, string as_window_name);int li_count

li_count = upperbound(is_last_windows_label) + 1

is_last_windows_label[li_count] = as_window_name
is_last_windows[li_count] = as_window_link

plb_1.insertitem(as_window_name, 1, 1)
end subroutine

on w_extra_windows.create
this.plb_1=create plb_1
this.Control[]={this.plb_1}
end on

on w_extra_windows.destroy
destroy(this.plb_1)
end on

event open;event ue_resize()

plb_1.deletepictures()
plb_1.addPicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_editor_doc_child.png")
end event

type plb_1 from picturelistbox within w_extra_windows
integer x = 23
integer y = 20
integer width = 1554
integer height = 400
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 268435456
boolean border = false
boolean sorted = false
borderstyle borderstyle = stylelowered!
string picturename[] = {"C:\cs_115\framework\risorse\11.5\tree_editor_doc_child.png"}
long picturemaskcolor = 536870912
end type

event doubleclicked;string ls_current
int li_i, li_j
window lw_window


if index > 0 then

	ls_current = SelectedItem()
	
	for li_i = 1 to upperbound(is_last_windows_label)
		if is_last_windows_label[li_i] = ls_current then
			
			// controllo se è aperta
			for li_j = 1 to PCCA.Num_Windows
				
				if not isnull(pcca.window_list[li_j]) and isvalid(pcca.window_list[li_j]) then
					if lower(is_last_windows[li_i]) = classname(pcca.window_list[li_j]) then
						
						pcca.window_list[li_j].show()
						pcca.window_list[li_j].SetFocus()
						return
						
					end if
				end if
				
			next
			
			window_type_open(lw_window, is_last_windows[li_i],-1)
			
		end if
	next
	
end if
end event

event losefocus;SelectItem(0)
end event

