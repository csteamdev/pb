﻿$PBExportHeader$w_filesaveas.srw
$PBExportComments$Finestra Esporta
forward
global type w_filesaveas from Window
end type
type st_file_tipo from statictext within w_filesaveas
end type
type ddlb_file_tipo from dropdownlistbox within w_filesaveas
end type
type lb_file_lista from listbox within w_filesaveas
end type
type st_unita from statictext within w_filesaveas
end type
type st_directory from statictext within w_filesaveas
end type
type lb_directory from listbox within w_filesaveas
end type
type st_directory_corrente from statictext within w_filesaveas
end type
type cb_annulla from commandbutton within w_filesaveas
end type
type ddlb_unita from dropdownlistbox within w_filesaveas
end type
type cbx_intestazioni from checkbox within w_filesaveas
end type
type st_file_nome from statictext within w_filesaveas
end type
type sle_file_nome from singlelineedit within w_filesaveas
end type
type cb_ok from commandbutton within w_filesaveas
end type
end forward

global type w_filesaveas from Window
int X=270
int Y=285
int Width=2268
int Height=1301
boolean TitleBar=true
string Title="Esporta"
long BackColor=12632256
boolean ControlMenu=true
WindowType WindowType=response!
st_file_tipo st_file_tipo
ddlb_file_tipo ddlb_file_tipo
lb_file_lista lb_file_lista
st_unita st_unita
st_directory st_directory
lb_directory lb_directory
st_directory_corrente st_directory_corrente
cb_annulla cb_annulla
ddlb_unita ddlb_unita
cbx_intestazioni cbx_intestazioni
st_file_nome st_file_nome
sle_file_nome sle_file_nome
cb_ok cb_ok
end type
global w_filesaveas w_filesaveas

type variables
boolean ib_intestazioni
string is_directory_corrente
string is_unita_corrente
string is_file_tipo
datawindow idw_attiva
saveastype ist_file_tipo
end variables

event open;idw_attiva = w_pomanager_std.parm.dw_name

ddlb_unita.dirlist(is_unita_corrente, 16384)
ddlb_unita.dirselect(is_unita_corrente)

is_directory_corrente = is_unita_corrente + "*"
lb_directory.dirlist(is_directory_corrente, 32784, &
                     st_directory_corrente)

ddlb_file_tipo.additem("Testo con valori separati da tab (*.txt)")
ddlb_file_tipo.additem("Testo con valori separati da virgole (*.csv)")
ddlb_file_tipo.additem("Data Interchange Format (*.dif)")
ddlb_file_tipo.additem("Microsoft Excel (*.xls)")
ddlb_file_tipo.additem("Lotus 1-2-3 (*.wk1)")
ddlb_file_tipo.additem("Lotus 1-2-3 (*.wks)")
ddlb_file_tipo.additem("dBASE II (*.dbf)")
ddlb_file_tipo.additem("dBASE III (*.dbf)")
ddlb_file_tipo.additem("Powersoft Report (*.psr)")
ddlb_file_tipo.additem("Windows Metafile (*.wmf)")
ddlb_file_tipo.additem("Text with HTML formatting (*.htm)")

is_unita_corrente = "[-" + mid(st_directory_corrente.text, 1, 1) + "-]"
ddlb_unita.text = is_unita_corrente

ddlb_file_tipo.selectitem(1)
ddlb_file_tipo.triggerevent(selectionchanged!)

f_po_setwindowposition(this)

sle_file_nome.setfocus()

end event

on w_filesaveas.create
this.st_file_tipo=create st_file_tipo
this.ddlb_file_tipo=create ddlb_file_tipo
this.lb_file_lista=create lb_file_lista
this.st_unita=create st_unita
this.st_directory=create st_directory
this.lb_directory=create lb_directory
this.st_directory_corrente=create st_directory_corrente
this.cb_annulla=create cb_annulla
this.ddlb_unita=create ddlb_unita
this.cbx_intestazioni=create cbx_intestazioni
this.st_file_nome=create st_file_nome
this.sle_file_nome=create sle_file_nome
this.cb_ok=create cb_ok
this.Control[]={ this.st_file_tipo,&
this.ddlb_file_tipo,&
this.lb_file_lista,&
this.st_unita,&
this.st_directory,&
this.lb_directory,&
this.st_directory_corrente,&
this.cb_annulla,&
this.ddlb_unita,&
this.cbx_intestazioni,&
this.st_file_nome,&
this.sle_file_nome,&
this.cb_ok}
end on

on w_filesaveas.destroy
destroy(this.st_file_tipo)
destroy(this.ddlb_file_tipo)
destroy(this.lb_file_lista)
destroy(this.st_unita)
destroy(this.st_directory)
destroy(this.lb_directory)
destroy(this.st_directory_corrente)
destroy(this.cb_annulla)
destroy(this.ddlb_unita)
destroy(this.cbx_intestazioni)
destroy(this.st_file_nome)
destroy(this.sle_file_nome)
destroy(this.cb_ok)
end on

type st_file_tipo from statictext within w_filesaveas
int X=23
int Y=1001
int Width=1761
int Height=61
boolean Enabled=false
string Text="Tipo File:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ddlb_file_tipo from dropdownlistbox within w_filesaveas
int X=23
int Y=1081
int Width=1761
int Height=301
int TabOrder=50
BorderStyle BorderStyle=StyleLowered!
boolean Sorted=false
boolean VScrollBar=true
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;is_file_tipo = left(right(this.text, 4), 3)

choose case this.finditem(this.text, 0)
   case 1
		ist_file_tipo = text!
	case 2
		ist_file_tipo = csv!
	case 3
		ist_file_tipo = dif!
	case 4
		ist_file_tipo = excel!
	case 5
		ist_file_tipo = wk1!
	case 6
		ist_file_tipo = wks!
	case 7
		ist_file_tipo = dbase2!
	case 8
		ist_file_tipo = dbase3!
	case 9
		ist_file_tipo = psreport!
	case 10
		ist_file_tipo = wmf!
	case 11
		ist_file_tipo = HTMLTable!

end choose

if sle_file_nome.text = "" then
   sle_file_nome.text = "UNTITLED." + is_file_tipo
else
   sle_file_nome.text = mid(sle_file_nome.text, 1, &
                            pos(sle_file_nome.text, ".")) + &
                        is_file_tipo
end if

lb_file_lista.dirlist("*." + is_file_tipo, 0)

cb_ok.enabled = sle_file_nome.text <> ""

sle_file_nome.setfocus()
end event

type lb_file_lista from listbox within w_filesaveas
int X=23
int Y=181
int Width=801
int Height=781
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on selectionchanged;sle_file_nome.text = this.selecteditem()

cb_ok.enabled = sle_file_nome.text <> ""

sle_file_nome.setfocus()

end on

type st_unita from statictext within w_filesaveas
int X=869
int Y=801
int Width=915
int Height=61
boolean Enabled=false
string Text="Unità:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_directory from statictext within w_filesaveas
int X=869
int Y=21
int Width=915
int Height=61
boolean Enabled=false
string Text="Directory:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type lb_directory from listbox within w_filesaveas
int X=869
int Y=181
int Width=915
int Height=601
int TabOrder=30
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on doubleclicked;lb_directory.dirselect(is_directory_corrente)

lb_directory.dirlist(is_directory_corrente, 32784, &
                     st_directory_corrente)

lb_file_lista.dirlist("*." + is_file_tipo, 0)

sle_file_nome.setfocus()

end on

type st_directory_corrente from statictext within w_filesaveas
int X=869
int Y=101
int Width=915
int Height=61
boolean Enabled=false
string Text="Directory Corrente"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_annulla from commandbutton within w_filesaveas
int X=1829
int Y=141
int Width=366
int Height=101
int TabOrder=80
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;close(parent)
end on

type ddlb_unita from dropdownlistbox within w_filesaveas
int X=869
int Y=881
int Width=915
int Height=301
int TabOrder=40
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on selectionchanged;ddlb_unita.dirselect(is_unita_corrente)

is_directory_corrente = is_unita_corrente + "*"
lb_directory.dirlist(is_directory_corrente, 32784, &
                     st_directory_corrente)

lb_file_lista.dirlist("*." + is_file_tipo, 0)

sle_file_nome.setfocus()
end on

type cbx_intestazioni from checkbox within w_filesaveas
int X=1829
int Y=1101
int Width=380
int Height=61
int TabOrder=60
string Text="Intestazioni"
BorderStyle BorderStyle=StyleLowered!
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;ib_intestazioni = cbx_intestazioni.checked

sle_file_nome.setfocus()

end on

type st_file_nome from statictext within w_filesaveas
int X=23
int Y=21
int Width=801
int Height=61
boolean Enabled=false
string Text="Nome File:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_file_nome from singlelineedit within w_filesaveas
int X=23
int Y=101
int Width=801
int Height=81
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
boolean HideSelection=false
TextCase TextCase=Upper!
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on modified;cb_ok.enabled = this.text <> ""
end on

type cb_ok from commandbutton within w_filesaveas
int X=1829
int Y=21
int Width=366
int Height=101
int TabOrder=70
string Text="&Ok"
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;integer li_ritorno
string ls_file_nome


if sle_file_nome.text = "" then
   g_mb.messagebox("Attenzione", "Inserire un nome file valido.", &
              exclamation!, ok!)
   return
end if

if mid(st_directory_corrente.text, len(st_directory_corrente.text), 1) = &
   "\" then
   ls_file_nome = st_directory_corrente.text + sle_file_nome.text    
else
   ls_file_nome = st_directory_corrente.text + "\" + sle_file_nome.text  
end if  

if fileexists(ls_file_nome) then
   li_ritorno = g_mb.messagebox("Attenzione", "File " + ls_file_nome + &
                                         " già esistente.~nSostituirlo?", &
                exclamation!, yesno!, 2)
   if li_ritorno = 2 then return
end if

setpointer(hourglass!)

idw_attiva.saveas(ls_file_nome, ist_file_tipo, ib_intestazioni)

setpointer(arrow!)

w_pomanager_std.parm.current_directory = st_directory_corrente.text

close(parent)

end on

