﻿$PBExportHeader$w_filtri_automatici.srw
forward
global type w_filtri_automatici from w_cs_xx_risposta
end type
type cb_5 from commandbutton within w_filtri_automatici
end type
type st_1 from statictext within w_filtri_automatici
end type
type cb_4 from uo_cb_save within w_filtri_automatici
end type
type cb_3 from uo_cb_delete within w_filtri_automatici
end type
type cb_2 from uo_cb_modify within w_filtri_automatici
end type
type cb_1 from uo_cb_new within w_filtri_automatici
end type
type dw_1 from datawindow within w_filtri_automatici
end type
type dw_elenco_filtri from uo_cs_xx_dw within w_filtri_automatici
end type
end forward

global type w_filtri_automatici from w_cs_xx_risposta
integer width = 4521
integer height = 1888
string title = "Filtri Automatici"
cb_5 cb_5
st_1 st_1
cb_4 cb_4
cb_3 cb_3
cb_2 cb_2
cb_1 cb_1
dw_1 dw_1
dw_elenco_filtri dw_elenco_filtri
end type
global w_filtri_automatici w_filtri_automatici

type variables
string is_nome_dw
end variables

on w_filtri_automatici.create
int iCurrent
call super::create
this.cb_5=create cb_5
this.st_1=create st_1
this.cb_4=create cb_4
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_1=create dw_1
this.dw_elenco_filtri=create dw_elenco_filtri
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_5
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.cb_4
this.Control[iCurrent+4]=this.cb_3
this.Control[iCurrent+5]=this.cb_2
this.Control[iCurrent+6]=this.cb_1
this.Control[iCurrent+7]=this.dw_1
this.Control[iCurrent+8]=this.dw_elenco_filtri
end on

on w_filtri_automatici.destroy
call super::destroy
destroy(this.cb_5)
destroy(this.st_1)
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_1)
destroy(this.dw_elenco_filtri)
end on

event pc_setwindow;call super::pc_setwindow;str_des_multilingua lstr_des_multilingua

dw_elenco_filtri.set_dw_key("cod_azienda")
dw_elenco_filtri.set_dw_key("nome_dw")
dw_elenco_filtri.set_dw_key("progressivo")

dw_elenco_filtri.set_dw_options(sqlca, &
                          pcca.null_object, &
								  c_default, &
								  c_default + c_NoHighlightSelected)

lstr_des_multilingua = message.powerobjectparm

is_nome_dw = lstr_des_multilingua.dataobject

this.title = "Filtri Automatici per la Finestra " + lstr_des_multilingua.nome_oggetto + " (" + lstr_des_multilingua.title + ") "

st_1.text = is_nome_dw


end event

event pc_setddlb;call super::pc_setddlb;string ls_str, ls_type, ls_oggetto, ls_testo, ls_username, ls_cod_utente
long ll_pos, ll_controls, ll_i
		
dw_1.dataobject = is_nome_dw
		
ls_str = dw_1.Object.DataWindow.Objects
if len(trim(ls_str)) < 1 or isnull(ls_str) then
	g_mb.messagebox("FrameWork", "Nessuna colonna presente nella DW selezionata.")
	return 1
end if
		
ll_i = 0
ls_str += "~t"

do while true
	ll_pos = pos(ls_str, "~t", 1)
	if ll_pos < 1 then exit
	ls_oggetto = left(ls_str, ll_pos - 1)
	
	ls_type = dw_1.Describe(ls_oggetto + ".type")
	if ls_type = "column" then
		ll_i ++
		dw_elenco_filtri.setvalue("colonna_dw", ll_i, ls_oggetto)
		
	end if

	ls_str = mid(ls_str, ll_pos + 1)
loop


ll_i = 0

DECLARE cu_utenti CURSOR FOR  
SELECT username, cod_utente
 FROM utenti;
open cu_utenti;

do while true
	fetch cu_utenti into :ls_username, :ls_cod_utente;
	if sqlca.sqlcode <> 0 then exit
	ll_i ++
	dw_elenco_filtri.setvalue("cod_utente", ll_i, ls_username + "~t" + ls_cod_utente)
loop

close cu_utenti;
end event

type cb_5 from commandbutton within w_filtri_automatici
integer x = 4091
integer y = 20
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica"
boolean flatstyle = true
end type

event clicked;openwithparm(w_filtri_automatici_duplica, is_nome_dw  )
end event

type st_1 from statictext within w_filtri_automatici
integer x = 1577
integer y = 20
integer width = 1531
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_4 from uo_cb_save within w_filtri_automatici
integer x = 1189
integer y = 20
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
boolean flatstyle = true
end type

type cb_3 from uo_cb_delete within w_filtri_automatici
integer x = 800
integer y = 20
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cancella"
boolean flatstyle = true
end type

type cb_2 from uo_cb_modify within w_filtri_automatici
integer x = 411
integer y = 20
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Modifica"
boolean flatstyle = true
end type

type cb_1 from uo_cb_new within w_filtri_automatici
integer x = 23
integer y = 16
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi"
boolean flatstyle = true
end type

type dw_1 from datawindow within w_filtri_automatici
boolean visible = false
integer x = 1691
integer y = 360
integer width = 366
integer height = 156
integer taborder = 20
string title = "none"
boolean livescroll = true
end type

type dw_elenco_filtri from uo_cs_xx_dw within w_filtri_automatici
integer y = 140
integer width = 4457
integer height = 1620
integer taborder = 10
string dataobject = "dw_tab_dw_filtri"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event itemchanged;call super::itemchanged;string ls_colonna

choose case i_colname
		
	case "colonna_dw"
		ls_colonna = dw_1.Describe(data + ".dbName")
		setitem(getrow(),"colonna_db", ls_colonna)
		
end choose

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, is_nome_dw)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_max

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "nome_dw")) then
		setitem(getrow(),"nome_dw", is_nome_dw)
	end if
next

select max(progressivo)
into   :ll_max
from   tab_dw_filtri
where  cod_azienda = :s_cs_xx.cod_azienda;

if isnull(ll_max) then ll_max = 0

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemnumber(ll_i, "progressivo")) or this.getitemnumber(ll_i, "progressivo") < 1 then
		ll_max ++
      this.setitem(ll_i, "progressivo", ll_max)
   end if
next
end event

