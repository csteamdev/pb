﻿$PBExportHeader$w_filtri_automatici_aggiungi.srw
$PBExportComments$Finestra Aggiunta filtri automatici
forward
global type w_filtri_automatici_aggiungi from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_filtri_automatici_aggiungi
end type
type dw_elenco_campi from datawindow within w_filtri_automatici_aggiungi
end type
type cb_tutti from commandbutton within w_filtri_automatici_aggiungi
end type
type cb_azzera from commandbutton within w_filtri_automatici_aggiungi
end type
type cb_ok from commandbutton within w_filtri_automatici_aggiungi
end type
type cb_annulla from uo_cb_close within w_filtri_automatici_aggiungi
end type
type dw_destinatari_lista from datawindow within w_filtri_automatici_aggiungi
end type
end forward

global type w_filtri_automatici_aggiungi from w_cs_xx_principale
integer width = 3721
integer height = 2036
string title = "Aggiungi filtri automatici"
cb_1 cb_1
dw_elenco_campi dw_elenco_campi
cb_tutti cb_tutti
cb_azzera cb_azzera
cb_ok cb_ok
cb_annulla cb_annulla
dw_destinatari_lista dw_destinatari_lista
end type
global w_filtri_automatici_aggiungi w_filtri_automatici_aggiungi

type variables
long   il_row, il_selezione, il_campi

string  is_nome_dw

boolean ib_esterna = false


end variables

forward prototypes
public subroutine wf_pos_ricerca ()
public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca)
public subroutine wf_carica_destinatari ()
end prototypes

public subroutine wf_pos_ricerca ();
end subroutine

public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca);
end subroutine

public subroutine wf_carica_destinatari ();long   ll_riga

string ls_select, ls_cod_utente, ls_username, ls_nome_cognome

ls_select = &
" select cod_utente, " + &
"        username, " + &
"		   nome_cognome " + &
" from	utenti " + &
" order  by nome_cognome "

declare destinatari dynamic cursor for sqlsa;

prepare sqlsa from :ls_select;

open destinatari;

if sqlca.sqlcode <> 0 then
	
	g_mb.messagebox("OMNIA","Errore nella open del cursore destinatari: " + sqlca.sqlerrtext)
	
	return
	
end if

do while true
	
	fetch destinatari
	into  :ls_cod_utente,
	      :ls_username,
			:ls_nome_cognome;
			
	if sqlca.sqlcode < 0 then
		
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore destinatari: " + sqlca.sqlerrtext)
		
		close destinatari;
		
		return
		
	elseif sqlca.sqlcode = 100	then
		
		close destinatari;
		
		exit
		
	end if
	
	ll_riga = dw_destinatari_lista.insertrow(0)
	
	dw_destinatari_lista.setitem( ll_riga, "codice", ls_cod_utente)	
	
	dw_destinatari_lista.setitem( ll_riga, "descrizione", ls_nome_cognome)
	
	dw_destinatari_lista.setitem( ll_riga, "indirizzo", ls_username)
	
	dw_destinatari_lista.setitem( ll_riga, "selezione", "N")
	
loop

return 
end subroutine

event pc_setwindow;call super::pc_setwindow;dw_destinatari_lista.settransobject(sqlca)

dw_destinatari_lista.setrowfocusindicator(hand!)

dw_elenco_campi.postevent("ue_carica")

if isnull(s_cs_xx.parametri.parametro_s_1) then
	ib_esterna = false
else
	ib_esterna = true
end if

is_nome_dw = s_cs_xx.parametri.nome_oggetto

setnull(s_cs_xx.parametri.parametro_s_1)

setnull(s_cs_xx.parametri.nome_oggetto)

end event

on w_filtri_automatici_aggiungi.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_elenco_campi=create dw_elenco_campi
this.cb_tutti=create cb_tutti
this.cb_azzera=create cb_azzera
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.dw_destinatari_lista=create dw_destinatari_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_elenco_campi
this.Control[iCurrent+3]=this.cb_tutti
this.Control[iCurrent+4]=this.cb_azzera
this.Control[iCurrent+5]=this.cb_ok
this.Control[iCurrent+6]=this.cb_annulla
this.Control[iCurrent+7]=this.dw_destinatari_lista
end on

on w_filtri_automatici_aggiungi.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_elenco_campi)
destroy(this.cb_tutti)
destroy(this.cb_azzera)
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.dw_destinatari_lista)
end on

event open;call super::open;wf_carica_destinatari()
end event

type cb_1 from commandbutton within w_filtri_automatici_aggiungi
integer x = 3310
integer y = 884
integer width = 343
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Riga"
end type

event clicked;long ll_riga

if ib_esterna then
	
	ll_riga = dw_elenco_campi.insertrow(0)
	
	dw_elenco_campi.Modify("nome_colonna.TabSequence = '100'")
	
end if
end event

type dw_elenco_campi from datawindow within w_filtri_automatici_aggiungi
event type long ue_carica ( )
integer x = 23
integer y = 20
integer width = 3630
integer height = 840
integer taborder = 80
string dataobject = "d_filtri_automatici_aggiungi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event type long ue_carica();long   	ll_riga, ll_i, ll_frequenza, ll_pos

string 	ls_str, ls_oggetto, ls_type, ls_nome_colonna[], ls_tipo_colonna[], ls_tipo, ls_db_colonna[], ls_db, ls_coltype


datastore lds_datastore

reset()

if is_nome_dw = "" or isnull(is_nome_dw) then return -1

setpointer(hourglass!)

if not ib_esterna then

	lds_datastore = create datastore
	
	lds_datastore.dataobject = is_nome_dw	
			
	ls_str = lds_datastore.Object.DataWindow.Objects
	
	if len(trim(ls_str)) < 1 or isnull(ls_str) then
		
		ls_str = ""
		
	else
	
		ls_str += "~t"
		
	end if
			
	ll_i = 0
	
	do while true
		
		ll_pos = pos(ls_str, "~t", 1)
		
		if ll_pos < 1 then exit
		
		ls_oggetto = left(ls_str, ll_pos - 1)
		
		ls_type = lds_datastore.Describe( ls_oggetto + ".type")
		
		ls_db = lds_datastore.Describe( ls_oggetto + ".dbName")
		
		if ls_type = "column" then
			
			ls_coltype = lds_datastore.Describe( ls_oggetto + ".coltype")
		
			ll_i ++
			
			ls_nome_colonna[ll_i] = ls_oggetto
			
			choose case lower( left( ls_coltype,5 ) )
				case "char(", "char"
					ls_tipo = "S"
				case "date", "datet", "time", "times"
					ls_tipo = "D"
				case "decim", "long", "numbe", "real"
					ls_tipo = "N"
			end choose
			
			ls_tipo_colonna[ll_i] = ls_tipo
			
			ls_db_colonna[ll_i] = ls_db
			
		end if
	
		ls_str = mid(ls_str, ll_pos + 1)
	loop
	
	destroy lds_datastore;

end if

if UpperBound(ls_nome_colonna) > 0 then
	
	Modify("nome_colonna.TabSequence = '0'")
	
	for ll_i = 1 to upperbound(ls_nome_colonna)
	
		ll_riga = insertrow(0)
		
		setitem( ll_riga, "nome_colonna", ls_nome_colonna[ll_i])
		
		setitem( ll_riga, "nome_colonna_db", ls_tipo_colonna[ll_i])
		
		setitem( ll_riga, "tipo", ls_tipo_colonna[ll_i])
		
	next
	
end if

setpointer(arrow!)

sort()

return 0
end event

event itemchanged;if isnull(row) or row < 1 then
	return
end if

if dwo.name = "selezione" then
	if data = "S" then
		il_campi ++
	else
		il_campi --
	end if
end if
end event

type cb_tutti from commandbutton within w_filtri_automatici_aggiungi
integer x = 1806
integer y = 880
integer width = 343
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Tutti"
end type

event clicked;long ll_i


for ll_i = 1 to dw_destinatari_lista.rowcount()	
	dw_destinatari_lista.setitem(ll_i,"selezione","S")	
next

enabled = false

cb_azzera.enabled = true


end event

type cb_azzera from commandbutton within w_filtri_automatici_aggiungi
integer x = 1806
integer y = 980
integer width = 343
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Azzera"
end type

event clicked;long ll_i


for ll_i = 1 to dw_destinatari_lista.rowcount()	
	dw_destinatari_lista.setitem(ll_i,"selezione","N")	
next

enabled = false

cb_tutti.enabled = true


end event

type cb_ok from commandbutton within w_filtri_automatici_aggiungi
integer x = 1806
integer y = 1840
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;long   ll_j, ll_i, ll_progressivo

string ls_nome_colonna, ls_db_colonna, ls_tipo_colonna, ls_insert, ls_select, ls_operatore, ls_cod_utente, &
		 ls_flag_vis_subito, ls_flag_ambiente

ll_j = 0

select max(progressivo) 
into   :ll_progressivo
from   tab_dw_filtri
where  cod_azienda = :s_cs_xx.cod_azienda;

if isnull(ll_progressivo) then ll_progressivo = 0

for ll_i = 1 to dw_elenco_campi.rowcount()
	
	ls_nome_colonna = dw_elenco_campi.getitemstring( ll_i, "nome_colonna")
	
	ls_db_colonna = dw_elenco_campi.getitemstring( ll_i, "nome_colonna_db")
	
	if isnull(ls_db_colonna) or ls_db_colonna = "" then ls_db_colonna = ls_nome_colonna
	
	ls_tipo_colonna = dw_elenco_campi.getitemstring( ll_i, "tipo")
	
	if ls_tipo_colonna = "" or isnull(ls_tipo_colonna) then
		
		continue
		
	end if
	
	ls_flag_vis_subito = dw_elenco_campi.getitemstring( ll_i, "flag_visualizza_subito")	
	
	ls_flag_ambiente = dw_elenco_campi.getitemstring( ll_i, "flag_ambiente")	
	
	ls_insert = dw_elenco_campi.getitemstring( ll_i, "insert")	
	
	ls_select = dw_elenco_campi.getitemstring( ll_i, "select")
	
	if (ls_insert = "" or isnull(ls_insert)) and (ls_select = "" or isnull(ls_select)) then
		
		continue
		
	end if	
	
	ls_operatore = dw_elenco_campi.getitemstring( ll_i, "operatore")
	
	for ll_j = 1 to dw_destinatari_lista.rowcount()
		
		if dw_destinatari_lista.getitemstring( ll_j, "selezione") = "S" then
			
			ls_cod_utente = dw_destinatari_lista.getitemstring( ll_j, "codice")
			
			ll_progressivo = ll_progressivo + 1
			
			insert into tab_dw_filtri ( cod_azienda,
			                            progressivo,
												 nome_dw,
												 colonna_dw,
												 colonna_db,
												 cod_utente,
												 valore_select,
												 valore_insert,
												 flag_tipo_valore,
												 operatore,
												 flag_visualizza_subito,
												 flag_ambiente)
			values                    ( :s_cs_xx.cod_azienda,
			                            :ll_progressivo,
												 :is_nome_dw,
												 :ls_nome_colonna,
												 :ls_db_colonna,
												 :ls_cod_utente,
												 :ls_select,
												 :ls_insert,
												 :ls_tipo_colonna,
												 :ls_operatore,
												 :ls_flag_vis_subito,
												 :ls_flag_ambiente);
												 
			if sqlca.sqlcode < 0 then
				
				g_mb.messagebox( "Framework", "Attenzione: errore durante l'inserimento: " + sqlca.sqlerrtext )
				
				return -1
				
			end if
			
			commit;

		end if
	
	next
	
next

i_openparm.triggerevent("pc_retrieve")

close(parent)
end event

type cb_annulla from uo_cb_close within w_filtri_automatici_aggiungi
integer x = 1806
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type dw_destinatari_lista from datawindow within w_filtri_automatici_aggiungi
event ue_key pbm_dwnkey
integer x = 23
integer y = 880
integer width = 1760
integer height = 1040
integer taborder = 70
string dataobject = "d_invio_messaggi_destinatari"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;long ll_count


ll_count = dw_destinatari_lista.rowcount()

if dwo.name = "selezione" then
	
	choose case data
			
		case "S"
			
			// Nuovo destinatario selezionato
			
			il_selezione ++
			
			cb_azzera.enabled = true
			
			if il_selezione = ll_count then
				
				cb_tutti.enabled = false
				
			end if
			
		case "N"
			
			// Un destinatario è stato deselezionato
			
			il_selezione --
			
			cb_tutti.enabled = true
			
			if il_selezione = 0 then
				
				cb_azzera.enabled = false
							
			end if				
			
	end choose
	
end if
end event

