﻿$PBExportHeader$w_filtri_automatici_duplica.srw
$PBExportComments$Duplicazione Filtri Automatici da utente a utente
forward
global type w_filtri_automatici_duplica from w_cs_xx_risposta
end type
type cbx_duplica_tutto from checkbox within w_filtri_automatici_duplica
end type
type cb_esegui from commandbutton within w_filtri_automatici_duplica
end type
type dw_filtri_automatici_duplica_destinazion from datawindow within w_filtri_automatici_duplica
end type
type st_2 from statictext within w_filtri_automatici_duplica
end type
type st_1 from statictext within w_filtri_automatici_duplica
end type
type dw_filtri_automatici_duplica_origine from datawindow within w_filtri_automatici_duplica
end type
end forward

global type w_filtri_automatici_duplica from w_cs_xx_risposta
integer width = 3227
integer height = 1260
string title = "DUPLICAZIONE FILTRI AUTOMATICI"
cbx_duplica_tutto cbx_duplica_tutto
cb_esegui cb_esegui
dw_filtri_automatici_duplica_destinazion dw_filtri_automatici_duplica_destinazion
st_2 st_2
st_1 st_1
dw_filtri_automatici_duplica_origine dw_filtri_automatici_duplica_origine
end type
global w_filtri_automatici_duplica w_filtri_automatici_duplica

type variables
string is_nome_dw
end variables

on w_filtri_automatici_duplica.create
int iCurrent
call super::create
this.cbx_duplica_tutto=create cbx_duplica_tutto
this.cb_esegui=create cb_esegui
this.dw_filtri_automatici_duplica_destinazion=create dw_filtri_automatici_duplica_destinazion
this.st_2=create st_2
this.st_1=create st_1
this.dw_filtri_automatici_duplica_origine=create dw_filtri_automatici_duplica_origine
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_duplica_tutto
this.Control[iCurrent+2]=this.cb_esegui
this.Control[iCurrent+3]=this.dw_filtri_automatici_duplica_destinazion
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.dw_filtri_automatici_duplica_origine
end on

on w_filtri_automatici_duplica.destroy
call super::destroy
destroy(this.cbx_duplica_tutto)
destroy(this.cb_esegui)
destroy(this.dw_filtri_automatici_duplica_destinazion)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_filtri_automatici_duplica_origine)
end on

event pc_setwindow;call super::pc_setwindow;dw_filtri_automatici_duplica_origine.settransobject(sqlca)
dw_filtri_automatici_duplica_origine.retrieve()

dw_filtri_automatici_duplica_destinazion.settransobject(sqlca)
dw_filtri_automatici_duplica_destinazion.retrieve()

is_nome_dw = message.stringparm
end event

type cbx_duplica_tutto from checkbox within w_filtri_automatici_duplica
integer x = 23
integer y = 1060
integer width = 882
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Duplica i filtri di tutte le finestre"
end type

type cb_esegui from commandbutton within w_filtri_automatici_duplica
integer x = 2789
integer y = 1060
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;string ls_cod_utente_orig, ls_cod_utente_dest[],ls_flag_selezione, ls_nome_dw, ls_colonna_dw, ls_colonna_db, ls_valore_select,   &
 		 ls_valore_insert,   ls_flag_tipo_valore,   ls_operatore,   ls_flag_visualizza_subito ,ls_nome_dw_insert, ls_flag_ambiente
long ll_i, ll_y,ll_progressivo

ls_nome_dw = is_nome_dw
if messagebox("Framework","Attenzione i dati degli utenti di destinazione saranno cancellati!",question!,yesno!,2) = 2 then
	return
end if

if cbx_duplica_tutto.checked = true then
	if messagebox("Framework","Attenzione TUTTI i dati DI TUTTI I FILTRI degli utenti di destinazione saranno cancellati: PROSEGUO ?!",question!,yesno!,2) = 2 then
		return
	end if
	ls_nome_dw = "%"
end if

setnull(ls_cod_utente_orig)

for ll_i = 1 to dw_filtri_automatici_duplica_origine.rowcount()
	
	ls_flag_selezione = dw_filtri_automatici_duplica_origine.getitemstring(ll_i, "flag_selezione")
	
	if ls_flag_selezione = "S" then
		if not isnull(ls_cod_utente_orig) then
			if messagebox("Framework","E' stato selezionato più di 1 utente di origine.",question!,yesno!,2) = 2 then
				return
			end if
		end if
		ls_cod_utente_orig = dw_filtri_automatici_duplica_origine.getitemstring(ll_i, "cod_utente")
		
	end if
	
next


if isnull(ls_cod_utente_orig) then
	messagebox("Framework","Nessun utente di orgine selezionato.",stopsign!)
	return
end if


ll_y = 0

for ll_i = 1 to dw_filtri_automatici_duplica_destinazion.rowcount()
	
	if dw_filtri_automatici_duplica_destinazion.getitemstring(ll_i, "flag_selezione") = "S" then
		
		ll_y ++
		ls_cod_utente_dest[ll_y] = dw_filtri_automatici_duplica_destinazion.getitemstring(ll_i, "cod_utente")
		if ls_cod_utente_dest[ll_y] = ls_cod_utente_orig then
			messagebox("Framework","L'utente di origine non può comparire fra quelli di destiazione.",stopsign!)
			return
		end if
		
	end if
	
next


if ll_y = 0 then
	messagebox("Framework","Nessun utente di destinazione selezionato.",stopsign!)
	return
end if


for ll_i = 1 to upperbound(ls_cod_utente_dest)
	
	delete tab_dw_filtri
	where cod_utente = :ls_cod_utente_dest[ll_i] and
			nome_dw like :ls_nome_dw;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("FRAMEWORK","Errore in cancellazione dati utente~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
next

select max(progressivo)
into   :ll_progressivo
from   tab_dw_filtri
where  cod_azienda = :s_cs_xx.cod_azienda;

if isnull(ll_progressivo) or ll_progressivo = 0 then
	ll_progressivo = 0
end if

declare cu_filtri cursor for
select     nome_dw,   
           colonna_dw,   
           colonna_db,   
           valore_select,   
           valore_insert,   
           flag_tipo_valore,   
           operatore,   
           flag_visualizza_subito,
			  flag_ambiente
      from tab_dw_filtri  
	  where cod_utente = :ls_cod_utente_orig and
			  nome_dw LIKE :ls_nome_dw;



for ll_i = 1 to upperbound(ls_cod_utente_dest)

	open cu_filtri;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("FRAMEWORK","Errore in OPEN cursore~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	do while true
		fetch cu_filtri into :ls_nome_dw_insert, :ls_colonna_dw, :ls_colonna_db, :ls_valore_select, :ls_valore_insert, :ls_flag_tipo_valore, :ls_operatore, :ls_flag_visualizza_subito, :ls_flag_ambiente;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("FRAMEWORK","Errore in FETCH cursore~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
	
		if sqlca.sqlcode = 100 then
			exit
		end if
		
		ll_progressivo ++
	
		insert into tab_dw_filtri  
				( cod_azienda,   
				  progressivo,   
				  nome_dw,   
				  colonna_dw,   
				  colonna_db,   
				  cod_utente,   
				  valore_select,   
				  valore_insert,   
				  flag_tipo_valore,   
				  operatore,   
				  flag_visualizza_subito,
				  flag_ambiente)  
		 values(:s_cs_xx.cod_azienda,   
				  :ll_progressivo,   
				  :ls_nome_dw_insert,   
				  :ls_colonna_dw,   
				  :ls_colonna_db,   
				  :ls_cod_utente_dest[ll_i],   
				  :ls_valore_select,   
				  :ls_valore_insert,   
				  :ls_flag_tipo_valore,   
				  :ls_operatore,   
				  :ls_flag_visualizza_subito,
				  :ls_flag_ambiente);
			  
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("FRAMEWORK","Errore in cancellazione dati utente~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
	loop
	
	close cu_filtri;
	
next

commit;

g_mb.messagebox("FRAMEWORK","DUPLICAZIONE ESEGUITA CON SUCCESSO" + sqlca.sqlerrtext)


end event

type dw_filtri_automatici_duplica_destinazion from datawindow within w_filtri_automatici_duplica
integer x = 1600
integer y = 100
integer width = 1554
integer height = 940
integer taborder = 10
string title = "none"
string dataobject = "d_filtri_automatici_duplica_origine"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_filtri_automatici_duplica
integer x = 1600
integer y = 20
integer width = 1554
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SELEZIONAREGLI UTENTI DI DESTINAZIONE"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_1 from statictext within w_filtri_automatici_duplica
integer x = 23
integer y = 20
integer width = 1554
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SELEZIONARE L~'UTENTE DI ORIGINE"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_filtri_automatici_duplica_origine from datawindow within w_filtri_automatici_duplica
integer x = 23
integer y = 100
integer width = 1554
integer height = 940
integer taborder = 10
string title = "none"
string dataobject = "d_filtri_automatici_duplica_origine"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

