﻿$PBExportHeader$w_filtri_automatici_estesa.srw
forward
global type w_filtri_automatici_estesa from w_cs_xx_risposta
end type
type dw_1 from datawindow within w_filtri_automatici_estesa
end type
type tab_1 from tab within w_filtri_automatici_estesa
end type
type tabpage_1 from userobject within tab_1
end type
type cb_6 from commandbutton within tabpage_1
end type
type em_1 from editmask within tabpage_1
end type
type st_3 from statictext within tabpage_1
end type
type dw_elenco_dw from datawindow within tabpage_1
end type
type tabpage_1 from userobject within tab_1
cb_6 cb_6
em_1 em_1
st_3 st_3
dw_elenco_dw dw_elenco_dw
end type
type tabpage_2 from userobject within tab_1
end type
type cb_duplica from commandbutton within tabpage_2
end type
type cb_7 from commandbutton within tabpage_2
end type
type cb_5 from commandbutton within tabpage_2
end type
type sle_dw from singlelineedit within tabpage_2
end type
type st_1 from statictext within tabpage_2
end type
type tabpage_2 from userobject within tab_1
cb_duplica cb_duplica
cb_7 cb_7
cb_5 cb_5
sle_dw sle_dw
st_1 st_1
end type
type tab_1 from tab within w_filtri_automatici_estesa
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
type cb_1 from commandbutton within w_filtri_automatici_estesa
end type
type cb_4 from uo_cb_save within w_filtri_automatici_estesa
end type
type cb_3 from uo_cb_delete within w_filtri_automatici_estesa
end type
type cb_2 from uo_cb_modify within w_filtri_automatici_estesa
end type
type dw_elenco_filtri from uo_cs_xx_dw within w_filtri_automatici_estesa
end type
end forward

global type w_filtri_automatici_estesa from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 3922
integer height = 2112
string title = "Filtri Automatici"
event reseize pbm_size
dw_1 dw_1
tab_1 tab_1
cb_1 cb_1
cb_4 cb_4
cb_3 cb_3
cb_2 cb_2
dw_elenco_filtri dw_elenco_filtri
end type
global w_filtri_automatici_estesa w_filtri_automatici_estesa

type variables
string is_nome_dw
end variables

forward prototypes
public function integer wf_cerca ()
public function integer wf_cerca_esterna ()
end prototypes

public function integer wf_cerca ();string ls_str, ls_oggetto, ls_type, ls_username, ls_cod_utente

long   ll_i, ll_pos

dw_1.dataobject = is_nome_dw	
		
ls_str = dw_1.Object.DataWindow.Objects
if len(trim(ls_str)) < 1 or isnull(ls_str) then
	g_mb.messagebox("FrameWork", "Nessuna colonna presente nella DW selezionata.")
	return 1
end if
		
ll_i = 0
ls_str += "~t"

do while true
	ll_pos = pos(ls_str, "~t", 1)
	if ll_pos < 1 then exit
	ls_oggetto = left(ls_str, ll_pos - 1)
	
	ls_type = dw_1.Describe(ls_oggetto + ".type")
	if ls_type = "column" then
		ll_i ++
		dw_elenco_filtri.setvalue("colonna_dw", ll_i, ls_oggetto)
		
	end if

	ls_str = mid(ls_str, ll_pos + 1)
loop


ll_i = 0

DECLARE cu_utenti CURSOR FOR  
SELECT  username, 
        cod_utente
FROM    utenti;

open cu_utenti;

do while true
	fetch cu_utenti into :ls_username, 
	                     :ls_cod_utente;
								
	if sqlca.sqlcode <> 0 then exit
	
	ll_i ++
	
	dw_elenco_filtri.setvalue("cod_utente", ll_i, ls_username + "~t" + ls_cod_utente)
	
loop

close cu_utenti;

triggerevent("pc_retrieve")

//dw_elenco_filtri.triggerevent("pcd_view")

return 0
end function

public function integer wf_cerca_esterna ();string ls_str, ls_oggetto, ls_type, ls_username, ls_cod_utente

long   ll_i, ll_pos

DECLARE cu_utenti CURSOR FOR  
SELECT  username, 
        cod_utente
FROM    utenti;

open cu_utenti;

do while true
	fetch cu_utenti into :ls_username, 
	                     :ls_cod_utente;
								
	if sqlca.sqlcode <> 0 then exit
	
	ll_i ++
	
	dw_elenco_filtri.setvalue("cod_utente", ll_i, ls_username + "~t" + ls_cod_utente)
	
loop

close cu_utenti;

triggerevent("pc_retrieve")

return 0
end function

on w_filtri_automatici_estesa.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.tab_1=create tab_1
this.cb_1=create cb_1
this.cb_4=create cb_4
this.cb_3=create cb_3
this.cb_2=create cb_2
this.dw_elenco_filtri=create dw_elenco_filtri
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.tab_1
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.cb_4
this.Control[iCurrent+5]=this.cb_3
this.Control[iCurrent+6]=this.cb_2
this.Control[iCurrent+7]=this.dw_elenco_filtri
end on

on w_filtri_automatici_estesa.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.tab_1)
destroy(this.cb_1)
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.dw_elenco_filtri)
end on

event pc_setwindow;call super::pc_setwindow;dw_elenco_filtri.set_dw_key("cod_azienda")
dw_elenco_filtri.set_dw_key("nome_dw")
dw_elenco_filtri.set_dw_key("progressivo")

dw_elenco_filtri.set_dw_options(sqlca, &
                          pcca.null_object, &
								  c_default, &
								  c_default + c_NoHighlightSelected)
								  

end event

event pc_setddlb;call super::pc_setddlb;string ls_str, ls_type, ls_oggetto, ls_testo, ls_username, ls_cod_utente
long ll_pos, ll_controls, ll_i

if is_nome_dw = "" or isnull(is_nome_dw) then return 0
	
dw_1.dataobject = is_nome_dw	
		
ls_str = dw_1.Object.DataWindow.Objects
if len(trim(ls_str)) < 1 or isnull(ls_str) then
	g_mb.messagebox("FrameWork", "Nessuna colonna presente nella DW selezionata.")
	return 1
end if
		
ll_i = 0
ls_str += "~t"

do while true
	ll_pos = pos(ls_str, "~t", 1)
	if ll_pos < 1 then exit
	ls_oggetto = left(ls_str, ll_pos - 1)
	
	ls_type = dw_1.Describe(ls_oggetto + ".type")
	if ls_type = "column" then
		ll_i ++
		dw_elenco_filtri.setvalue("colonna_dw", ll_i, ls_oggetto)
		
	end if

	ls_str = mid(ls_str, ll_pos + 1)
loop


ll_i = 0

DECLARE cu_utenti CURSOR FOR  
SELECT username, 
       cod_utente
FROM   utenti;

open cu_utenti;

do while true
	fetch cu_utenti into :ls_username, 
	                     :ls_cod_utente;
								
	if sqlca.sqlcode <> 0 then exit
	
	ll_i ++
	
	dw_elenco_filtri.setvalue("cod_utente", ll_i, ls_username + "~t" + ls_cod_utente)
	
loop

close cu_utenti;
end event

event resize;move(10,10)
end event

type dw_1 from datawindow within w_filtri_automatici_estesa
boolean visible = false
integer x = 2743
integer y = 1980
integer width = 686
integer height = 400
integer taborder = 70
string title = "none"
boolean livescroll = true
end type

type tab_1 from tab within w_filtri_automatici_estesa
event create ( )
event destroy ( )
integer x = 23
integer y = 20
integer width = 3840
integer height = 700
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

type tabpage_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3803
integer height = 576
long backcolor = 12632256
string text = "Applicazione Corrente"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_6 cb_6
em_1 em_1
st_3 st_3
dw_elenco_dw dw_elenco_dw
end type

on tabpage_1.create
this.cb_6=create cb_6
this.em_1=create em_1
this.st_3=create st_3
this.dw_elenco_dw=create dw_elenco_dw
this.Control[]={this.cb_6,&
this.em_1,&
this.st_3,&
this.dw_elenco_dw}
end on

on tabpage_1.destroy
destroy(this.cb_6)
destroy(this.em_1)
destroy(this.st_3)
destroy(this.dw_elenco_dw)
end on

type cb_6 from commandbutton within tabpage_1
integer x = 2405
integer y = 32
integer width = 402
integer height = 84
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Estrai"
boolean flatstyle = true
end type

event clicked;long		ll_controls, ll_i, ll_row
string	ls_nome_oggetto
window 	l_window
datawindow l_dw

dw_elenco_dw.reset()

if len(em_1.text) > 0 then 
	ls_nome_oggetto = em_1.text
else
	messagebox("Filtri Automatici", "Indicare un nome valido di oggetto",stopsign!)
	return
end if

l_window = create using ls_nome_oggetto

ll_controls = upperbound(l_window.control)

for ll_i = 1 to ll_controls
	
	if l_window.control[ll_i].typeof() = datawindow! then
		l_dw = l_window.control[ll_i]
		ls_nome_oggetto = l_dw.dataobject
		// non estraggo la DW del folder
		if ls_nome_oggetto = "d_folder_tb" then continue
		ll_row = dw_elenco_dw.insertrow(0)
		dw_elenco_dw.setitem(ll_row, "dw_name", ls_nome_oggetto)
	
	end if
	
next

end event

type em_1 from editmask within tabpage_1
integer x = 736
integer y = 32
integer width = 1646
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean border = false
maskdatatype maskdatatype = stringmask!
end type

type st_3 from statictext within tabpage_1
integer x = 27
integer y = 32
integer width = 699
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nome Finestra Applicativa:"
boolean focusrectangle = false
end type

type dw_elenco_dw from datawindow within tabpage_1
integer x = 5
integer y = 132
integer width = 3771
integer height = 440
integer taborder = 30
string title = "none"
string dataobject = "dw_tab_dw_filtri_elenco_dw"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;string ls_nome_dw


if row > 0 then
	
	ls_nome_dw = getitemstring(row, "dw_name")
	
	if ls_nome_dw <> "" and not isnull(ls_nome_dw) then
	
		is_nome_dw = ls_nome_dw
		dw_1.dataobject = ls_nome_dw
		parent.triggerevent("pc_setddlb")
		wf_cerca()

	end if
	
end if

end event

event clicked;if dwo.name = "b_aggiungi" then
	
	string ls_dw, ls_esterna

	ls_dw = getitemstring(row, "dw_name")

	if ls_dw <> "" and not isnull(ls_dw) then
	
		is_nome_dw = ls_dw
	
		s_cs_xx.parametri.nome_oggetto = ls_dw
	
		setnull(s_cs_xx.parametri.parametro_s_1)
	
		setnull(s_cs_xx.parametri.parametro_s_1)
	
		Window_Open_Parm( w_filtri_automatici_aggiungi, -1, parent)
	
	end if
	
end if

end event

type tabpage_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3803
integer height = 576
long backcolor = 12632256
string text = "OMNIA.net"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_duplica cb_duplica
cb_7 cb_7
cb_5 cb_5
sle_dw sle_dw
st_1 st_1
end type

on tabpage_2.create
this.cb_duplica=create cb_duplica
this.cb_7=create cb_7
this.cb_5=create cb_5
this.sle_dw=create sle_dw
this.st_1=create st_1
this.Control[]={this.cb_duplica,&
this.cb_7,&
this.cb_5,&
this.sle_dw,&
this.st_1}
end on

on tabpage_2.destroy
destroy(this.cb_duplica)
destroy(this.cb_7)
destroy(this.cb_5)
destroy(this.sle_dw)
destroy(this.st_1)
end on

type cb_duplica from commandbutton within tabpage_2
integer x = 3273
integer y = 452
integer width = 402
integer height = 80
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica"
boolean flatstyle = true
end type

event clicked;string ls_dw

ls_dw = sle_dw.text

openwithparm(w_filtri_automatici_duplica, ls_dw)
end event

type cb_7 from commandbutton within tabpage_2
integer x = 2839
integer y = 452
integer width = 402
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Aggiungi Multiplo"
boolean flatstyle = true
end type

event clicked;string ls_dw, ls_esterna

ls_dw = sle_dw.text

if ls_dw <> "" and not isnull(ls_dw) then
	
	is_nome_dw = ls_dw
	
	s_cs_xx.parametri.nome_oggetto = ls_dw
	
	setnull(s_cs_xx.parametri.parametro_s_1)
	
	s_cs_xx.parametri.parametro_s_1 = 'S'
	
	Window_Open_Parm( w_filtri_automatici_aggiungi, -1, parent)
	
end if
end event

type cb_5 from commandbutton within tabpage_2
integer x = 3273
integer y = 12
integer width = 402
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Estrai"
boolean flatstyle = true
end type

event clicked;string ls_dw, ls_flag_esterna

ls_dw = sle_dw.text

if ls_dw <> "" and not isnull(ls_dw) then
	
	is_nome_dw = ls_dw
	
	parent.triggerevent("pc_setddlb")
	
	wf_cerca_esterna()

	
end if
end event

type sle_dw from singlelineedit within tabpage_2
integer x = 1079
integer y = 12
integer width = 2171
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean border = false
end type

type st_1 from statictext within tabpage_2
integer x = 50
integer y = 32
integer width = 1006
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Oggetto chiave di riferimento Intranet:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_filtri_automatici_estesa
integer x = 1074
integer y = 1900
integer width = 311
integer height = 84
integer taborder = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiungi"
boolean flatstyle = true
end type

event clicked;//string ls_dw, ls_esterna
//
//ls_dw = sle_1.text
//
//if ls_dw <> "" and not isnull(ls_dw) then
//	
//	is_nome_dw = ls_dw
//	
//	s_cs_xx.parametri.nome_oggetto = ls_dw
//	
//	setnull(s_cs_xx.parametri.parametro_s_1)
//	
//	if (cbx_1.checked) then	s_cs_xx.parametri.parametro_s_1 = 'S'
//	
//	Window_Open_Parm( w_filtri_automatici_aggiungi, -1, parent)
//	
//end if
end event

type cb_4 from uo_cb_save within w_filtri_automatici_estesa
integer x = 731
integer y = 1900
integer height = 84
integer taborder = 50
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
boolean flatstyle = true
end type

type cb_3 from uo_cb_delete within w_filtri_automatici_estesa
integer x = 366
integer y = 1900
integer height = 84
integer taborder = 40
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cancella"
boolean flatstyle = true
end type

type cb_2 from uo_cb_modify within w_filtri_automatici_estesa
integer x = 23
integer y = 1900
integer height = 84
integer taborder = 40
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Modifica"
boolean flatstyle = true
end type

type dw_elenco_filtri from uo_cs_xx_dw within w_filtri_automatici_estesa
integer x = 23
integer y = 740
integer width = 3840
integer height = 1140
integer taborder = 10
string dataobject = "dw_tab_dw_filtri"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event itemchanged;call super::itemchanged;string ls_colonna

choose case i_colname
		
	case "colonna_dw"
		ls_colonna = dw_1.Describe(data + ".dbName")
		setitem(getrow(),"colonna_db", ls_colonna)
		
end choose

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, is_nome_dw)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_max

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "nome_dw")) then
		setitem(getrow(),"nome_dw", is_nome_dw)
	end if
next

select max(progressivo)
into   :ll_max
from   tab_dw_filtri
where  cod_azienda = :s_cs_xx.cod_azienda;

if isnull(ll_max) then ll_max = 0

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemnumber(ll_i, "progressivo")) or this.getitemnumber(ll_i, "progressivo") < 1 then
		ll_max ++
      this.setitem(ll_i, "progressivo", ll_max)
   end if
next
end event

