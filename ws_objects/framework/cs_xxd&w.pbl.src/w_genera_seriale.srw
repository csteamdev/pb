﻿$PBExportHeader$w_genera_seriale.srw
forward
global type w_genera_seriale from w_cs_xx_principale
end type
type cb_copy from commandbutton within w_genera_seriale
end type
type st_6 from statictext within w_genera_seriale
end type
type mle_seriale from multilineedit within w_genera_seriale
end type
type st_5 from statictext within w_genera_seriale
end type
type em_license from editmask within w_genera_seriale
end type
type ddlb_licenza from dropdownlistbox within w_genera_seriale
end type
type dp_validita from datepicker within w_genera_seriale
end type
type sle_adapter from singlelineedit within w_genera_seriale
end type
type st_4 from statictext within w_genera_seriale
end type
type st_3 from statictext within w_genera_seriale
end type
type st_2 from statictext within w_genera_seriale
end type
type st_1 from statictext within w_genera_seriale
end type
type cb_genera from commandbutton within w_genera_seriale
end type
type r_1 from rectangle within w_genera_seriale
end type
end forward

global type w_genera_seriale from w_cs_xx_principale
integer width = 1376
integer height = 2220
string title = "Genera Seriale"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
string icon = "UserObject5!"
boolean center = true
event ue_custom_color ( )
cb_copy cb_copy
st_6 st_6
mle_seriale mle_seriale
st_5 st_5
em_license em_license
ddlb_licenza ddlb_licenza
dp_validita dp_validita
sle_adapter sle_adapter
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
cb_genera cb_genera
r_1 r_1
end type
global w_genera_seriale w_genera_seriale

type variables
private:
	uo_licenze iuo_license
end variables

event ue_custom_color();st_5.backcolor = rgb(255,255,255)
end event

on w_genera_seriale.create
int iCurrent
call super::create
this.cb_copy=create cb_copy
this.st_6=create st_6
this.mle_seriale=create mle_seriale
this.st_5=create st_5
this.em_license=create em_license
this.ddlb_licenza=create ddlb_licenza
this.dp_validita=create dp_validita
this.sle_adapter=create sle_adapter
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.cb_genera=create cb_genera
this.r_1=create r_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_copy
this.Control[iCurrent+2]=this.st_6
this.Control[iCurrent+3]=this.mle_seriale
this.Control[iCurrent+4]=this.st_5
this.Control[iCurrent+5]=this.em_license
this.Control[iCurrent+6]=this.ddlb_licenza
this.Control[iCurrent+7]=this.dp_validita
this.Control[iCurrent+8]=this.sle_adapter
this.Control[iCurrent+9]=this.st_4
this.Control[iCurrent+10]=this.st_3
this.Control[iCurrent+11]=this.st_2
this.Control[iCurrent+12]=this.st_1
this.Control[iCurrent+13]=this.cb_genera
this.Control[iCurrent+14]=this.r_1
end on

on w_genera_seriale.destroy
call super::destroy
destroy(this.cb_copy)
destroy(this.st_6)
destroy(this.mle_seriale)
destroy(this.st_5)
destroy(this.em_license)
destroy(this.ddlb_licenza)
destroy(this.dp_validita)
destroy(this.sle_adapter)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_genera)
destroy(this.r_1)
end on

event pc_setwindow;call super::pc_setwindow;iuo_license = create uo_licenze

ddlb_licenza.selectitem(1)
event post ue_custom_color()
end event

event close;call super::close;destroy iuo_license
end event

type cb_copy from commandbutton within w_genera_seriale
integer x = 1051
integer y = 1620
integer width = 229
integer height = 80
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "copy"
end type

event clicked;Clipboard(mle_seriale.text)
end event

type st_6 from statictext within w_genera_seriale
integer x = 91
integer y = 1640
integer width = 891
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seriale:"
boolean focusrectangle = false
end type

type mle_seriale from multilineedit within w_genera_seriale
integer x = 91
integer y = 1700
integer width = 1189
integer height = 380
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_5 from statictext within w_genera_seriale
integer x = 91
integer y = 60
integer width = 503
integer height = 80
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "Genera seriale"
boolean focusrectangle = false
end type

type em_license from editmask within w_genera_seriale
integer x = 114
integer y = 1260
integer width = 343
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
borderstyle borderstyle = stylelowered!
string mask = "#####"
end type

type ddlb_licenza from dropdownlistbox within w_genera_seriale
integer x = 91
integer y = 980
integer width = 686
integer height = 360
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string item[] = {"Nessun controllo","Trial","Licenze"}
borderstyle borderstyle = stylelowered!
end type

type dp_validita from datepicker within w_genera_seriale
integer x = 91
integer y = 620
integer width = 686
integer height = 100
integer taborder = 20
boolean border = true
borderstyle borderstyle = stylelowered!
date maxdate = Date("2999-12-31")
date mindate = Date("1800-01-01")
datetime value = DateTime(Date("2010-10-04"), Time("15:26:42.000000"))
integer textsize = -9
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
integer calendarfontweight = 400
boolean todaysection = true
boolean todaycircle = true
end type

type sle_adapter from singlelineedit within w_genera_seriale
integer x = 91
integer y = 360
integer width = 1143
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_4 from statictext within w_genera_seriale
integer x = 114
integer y = 1140
integer width = 1189
integer height = 120
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Se il tipo di licenza è basato sul controllo delle licenze specificare il numero qui"
boolean focusrectangle = false
end type

type st_3 from statictext within w_genera_seriale
integer x = 91
integer y = 780
integer width = 1189
integer height = 200
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Tipo di licenza: 1. Nessun controllo di licenza, 2. Periodo di prova (è sempre valido questo controllo), 3. Su numero di licenze attive"
boolean focusrectangle = false
end type

type st_2 from statictext within w_genera_seriale
integer x = 91
integer y = 480
integer width = 1189
integer height = 140
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Data di validità del prodotto, dopo tale data il software non sarà più utilizzabile"
boolean focusrectangle = false
end type

type st_1 from statictext within w_genera_seriale
integer x = 91
integer y = 300
integer width = 1189
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Codice generato dal client:"
boolean focusrectangle = false
end type

type cb_genera from commandbutton within w_genera_seriale
integer x = 91
integer y = 1420
integer width = 1189
integer height = 160
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Genera seriale"
end type

event clicked;/**
 * stefanop
 * 30/09/2010
 *
 * Genera un seriale di attivazione per il prodotto
 * Stringa che forma il seriale
 * 1. codice adattatore client (racchiuso tra { })
 * 2. data ultimo login (10 caratteri)
 * 3. data di validità software (10 caratteri)
 * 4. tipo licenza (1 carattere)
 * 5. numero licenze (restanti caratteri)
 *
 * Es: {AB3EC8A1-41C3-4311-9E82-0C9F7E20DC91}01/01/190010/10/2010325
 *
 * ATTENZIONE: la secure key NON deve essere cambiata MAI!!!!
 * se si ha necessità di cambiarla occorre controllare anche
 **/
 
string ls_key, ls_error

ls_key = iuo_license.uof_gen_seriale(sle_adapter.text, date(dp_validita.value), integer(ddlb_licenza.text), integer(em_license.text), ref ls_error)
if ls_error <> "" then
	g_mb.error("", ls_error)
	return
end if

mle_seriale.text = ls_key

end event

type r_1 from rectangle within w_genera_seriale
long linecolor = 16777215
integer linethickness = 4
long fillcolor = 1073741824
integer width = 1714
integer height = 200
end type

