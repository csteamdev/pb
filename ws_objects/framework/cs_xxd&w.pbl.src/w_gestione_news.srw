﻿$PBExportHeader$w_gestione_news.srw
forward
global type w_gestione_news from w_cs_xx_principale
end type
type dw_gestione_news_det from uo_cs_xx_dw within w_gestione_news
end type
type dw_gestione_news_lista from uo_cs_xx_dw within w_gestione_news
end type
end forward

global type w_gestione_news from w_cs_xx_principale
integer width = 2761
integer height = 1476
string title = "Gestione News"
boolean maxbox = false
boolean resizable = false
dw_gestione_news_det dw_gestione_news_det
dw_gestione_news_lista dw_gestione_news_lista
end type
global w_gestione_news w_gestione_news

on w_gestione_news.create
int iCurrent
call super::create
this.dw_gestione_news_det=create dw_gestione_news_det
this.dw_gestione_news_lista=create dw_gestione_news_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_gestione_news_det
this.Control[iCurrent+2]=this.dw_gestione_news_lista
end on

on w_gestione_news.destroy
call super::destroy
destroy(this.dw_gestione_news_det)
destroy(this.dw_gestione_news_lista)
end on

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_gestione_news_lista

dw_gestione_news_lista.change_dw_current()

dw_gestione_news_lista.set_dw_options(sqlca, &
										  		  pcca.null_object, &
										  		  c_default, &
										  		  c_default)
													 
dw_gestione_news_det.set_dw_options(sqlca, &
                                		dw_gestione_news_lista, &
                                		c_sharedata + c_scrollparent, &
                                		c_default)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_gestione_news_lista,"cod_area_aziendale",sqlca,"tab_aree_aziendali", &
					  "cod_area_aziendale","des_area","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_gestione_news_lista,"cod_utente",sqlca,"utenti", &
					  "cod_utente","nome_cognome"," ")
end event

type dw_gestione_news_det from uo_cs_xx_dw within w_gestione_news
integer y = 840
integer width = 2743
integer height = 540
integer taborder = 20
string dataobject = "d_gestione_news_det"
boolean border = false
end type

type dw_gestione_news_lista from uo_cs_xx_dw within w_gestione_news
integer x = 23
integer y = 20
integer width = 2697
integer height = 800
integer taborder = 10
string dataobject = "d_gestione_news_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_prog


for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemnumber(ll_i,"prog_news")) then
		
		select max(prog_news)
		into	 :ll_prog
		from	 news
		where	 cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("News","Errore in lettura massimo progressivo: " + sqlca.sqlerrtext,stopsign!)
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ll_prog) then
			ll_prog = 0
		end if
		
		ll_prog ++
		
		setitem(ll_i,"prog_news",ll_prog)
		
	end if
	
	// stefanop 17/06/2010: flag_letto default = N
	if isnull(getitemstring(ll_i, "flag_letto")) then
		setitem(ll_i, "flag_letto", "N")
	end if
	// ----
	
next
end event

