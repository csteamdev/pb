﻿$PBExportHeader$w_help.srw
$PBExportComments$Finestra Gestione Help
forward
global type w_help from w_cs_xx_principale
end type
type dw_help from uo_cs_xx_dw within w_help
end type
end forward

global type w_help from w_cs_xx_principale
int Width=3283
int Height=1149
boolean TitleBar=true
string Title="Gestione Help"
dw_help dw_help
end type
global w_help w_help

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_help.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_default, &
                       c_default)
end on

on w_help.create
int iCurrent
call w_cs_xx_principale::create
this.dw_help=create dw_help
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_help
end on

on w_help.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_help)
end on

type dw_help from uo_cs_xx_dw within w_help
int X=23
int Y=21
int Width=3201
int Height=1001
int TabOrder=20
string DataObject="d_help"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end on

