﻿$PBExportHeader$w_imp_pagina.srw
$PBExportComments$Imposta Pagina
forward
global type w_imp_pagina from window
end type
type tab_1 from tab within w_imp_pagina
end type
type tabpage_m from userobject within tab_1
end type
type st_unita_destro from statictext within tabpage_m
end type
type st_unita_sinistro from statictext within tabpage_m
end type
type st_unita_inferiore from statictext within tabpage_m
end type
type st_unita_superiore from statictext within tabpage_m
end type
type st_destra from statictext within tabpage_m
end type
type st_sinistra from statictext within tabpage_m
end type
type st_inferiore from statictext within tabpage_m
end type
type st_superiore from statictext within tabpage_m
end type
type em_destro from editmask within tabpage_m
end type
type em_sinistro from editmask within tabpage_m
end type
type em_inferiore from editmask within tabpage_m
end type
type em_superiore from editmask within tabpage_m
end type
type tabpage_m from userobject within tab_1
st_unita_destro st_unita_destro
st_unita_sinistro st_unita_sinistro
st_unita_inferiore st_unita_inferiore
st_unita_superiore st_unita_superiore
st_destra st_destra
st_sinistra st_sinistra
st_inferiore st_inferiore
st_superiore st_superiore
em_destro em_destro
em_sinistro em_sinistro
em_inferiore em_inferiore
em_superiore em_superiore
end type
type tabpage_d from userobject within tab_1
end type
type st_alimentazione from statictext within tabpage_d
end type
type ddlb_alimentazioni from dropdownlistbox within tabpage_d
end type
type st_dimensione from statictext within tabpage_d
end type
type ddlb_dimensioni from dropdownlistbox within tabpage_d
end type
type tabpage_d from userobject within tab_1
st_alimentazione st_alimentazione
ddlb_alimentazioni ddlb_alimentazioni
st_dimensione st_dimensione
ddlb_dimensioni ddlb_dimensioni
end type
type tab_1 from tab within w_imp_pagina
tabpage_m tabpage_m
tabpage_d tabpage_d
end type
type cb_ok from commandbutton within w_imp_pagina
end type
type cb_annulla from commandbutton within w_imp_pagina
end type
end forward

global type w_imp_pagina from window
integer x = 334
integer y = 252
integer width = 1550
integer height = 964
boolean titlebar = true
string title = "Imposta Pagina"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
tab_1 tab_1
cb_ok cb_ok
cb_annulla cb_annulla
end type
global w_imp_pagina w_imp_pagina

type variables
long il_preview_w, il_preview_h
string is_attributi[]
datawindow idw_datawindow
end variables

forward prototypes
public subroutine imp_attributi ()
public subroutine imp_anteprima ()
end prototypes

public subroutine imp_attributi ();long ll_i, ll_num_attributi
string ls_modify, ls_valore


ll_num_attributi = upperbound(is_attributi)
for ll_i = 1 to ll_num_attributi
	ls_valore = idw_datawindow.describe(is_attributi[ll_i])
	if ls_valore <> "" then
		ls_modify = ls_modify + is_attributi[ll_i] + "=" + ls_valore + " "
	end if
next

//dw_anteprima.modify(ls_modify)

end subroutine

public subroutine imp_anteprima ();//long ll_i, ll_posizione, ll_fine, ll_x, ll_y
//real lr_w, lr_h, lr_per
//string ls_dimensioni
//
//
//ls_dimensioni = ddlb_dimensioni.text
//ll_posizione = pos(ls_dimensioni, " x ")
//if ll_posizione > 0 then
//	for ll_i = ll_posizione - 1 to 1 step -1
//		if mid(ls_dimensioni, ll_i, 1) = " " then exit
//	next
//	ll_i ++
//	lr_w = double(mid(ls_dimensioni, ll_i, ll_posizione - ll_i))
//	ll_posizione += 3
//	ll_fine = pos(ls_dimensioni, " cm")
//	lr_h = double(mid(ls_dimensioni, ll_posizione, ll_fine - ll_posizione))
//else
//	lr_w = 21
//	lr_h = 29.7
//end if
//if rb_orrizontale.checked then
//	lr_per = lr_w
//	lr_w = lr_h
//	lr_h = lr_per
//end if
//lr_per = (lr_w / lr_h) / (21 / 29.7)
//if lr_per > 1 then
//	ll_x = il_preview_W
//	ll_y = il_preview_H / lr_per
//else
//	ll_x = il_preview_W * lr_per
//	ll_y = il_preview_H
//end if
//
//dw_anteprima.hide()
//dw_anteprima.resize(ll_x, ll_y)
//ll_x = gb_anteprima.x + (gb_anteprima.width - ll_x) / 2
//ll_y = gb_anteprima.y + (gb_anteprima.height - ll_y) * 2 / 3
//dw_anteprima.move(ll_x, ll_y)
//if rb_orrizontale.checked then
//	lr_per = lr_w / lr_h / lr_per
//end if
//lr_per = 14 / lr_per
//dw_anteprima.modify("datawindow.print.preview.zoom=" + string(lr_per))
//dw_anteprima.show()
end subroutine

event open;string ls_spazi, ls_stringa, ls_campo
long ll_i, ll_contatore
windowobject lw_oggetti[]


idw_datawindow = s_cs_xx.parametri.parametro_dw_1

//il_preview_w = dw_anteprima.width
//il_preview_h = dw_anteprima.height

is_attributi[] = {"DataWindow.Print.Color", &
						"DataWindow.Print.Margin.Bottom", &
						"DataWindow.Print.Margin.Left", &
						"DataWindow.Print.Margin.Right", &
						"DataWindow.Print.Margin.Top", &
						"DataWindow.Print.Orientation", &
						"DataWindow.Print.Paper.Size", &
						"DataWindow.Print.Paper.Source"}


//dw_anteprima.dataobject = idw_datawindow.dataobject

//idw_datawindow.sharedata(dw_anteprima)

imp_attributi()

//dw_anteprima.modify("datawindow.print.preview=yes")

//dw_anteprima.border = false


ls_spazi = space(50) + "~t"

tab_1.tabpage_d.ddlb_alimentazioni.setredraw(false)
tab_1.tabpage_d.ddlb_alimentazioni.additem("Predefinita" + ls_spazi + "0")
tab_1.tabpage_d.ddlb_alimentazioni.additem("Cassetto superiore" + ls_spazi + "1")
tab_1.tabpage_d.ddlb_alimentazioni.additem("Cassetto medio" + ls_spazi + "3")
tab_1.tabpage_d.ddlb_alimentazioni.additem("Cassetto inferiore" + ls_spazi + "2")
tab_1.tabpage_d.ddlb_alimentazioni.additem("Manuale" + ls_spazi + "4")
tab_1.tabpage_d.ddlb_alimentazioni.additem("Buste" + ls_spazi + "5")
tab_1.tabpage_d.ddlb_alimentazioni.additem("Buste manuale" + ls_spazi + "6")
tab_1.tabpage_d.ddlb_alimentazioni.additem("Selezione automatica" + ls_spazi + "7")
tab_1.tabpage_d.ddlb_alimentazioni.additem("Trattore" + ls_spazi + "8")
tab_1.tabpage_d.ddlb_alimentazioni.additem("Grande capacità" + ls_spazi + "11")
tab_1.tabpage_d.ddlb_alimentazioni.setredraw(true)

tab_1.tabpage_d.ddlb_dimensioni.setredraw(false)
tab_1.tabpage_d.ddlb_dimensioni.additem("Predefinite" + ls_spazi + "0")
tab_1.tabpage_d.ddlb_dimensioni.additem("Lettera 21,6 x 27,9 cm" + ls_spazi + "1")
tab_1.tabpage_d.ddlb_dimensioni.additem("Lettera ridotta 21,6 x 27,9 cm" + ls_spazi + "2")
tab_1.tabpage_d.ddlb_dimensioni.additem("Tabloid 43,2 x 27,9 cm" + ls_spazi + "3")
tab_1.tabpage_d.ddlb_dimensioni.additem("Ledger 43,2 x 27,9 cm" + ls_spazi + "4")
tab_1.tabpage_d.ddlb_dimensioni.additem("Legale 21,6 x 35,6 cm" + ls_spazi + "5")
tab_1.tabpage_d.ddlb_dimensioni.additem("Statement 14 x 21,7 cm" + ls_spazi + "6")
tab_1.tabpage_d.ddlb_dimensioni.additem("Executive 18,4 x 26,7 cm" + ls_spazi + "7")
tab_1.tabpage_d.ddlb_dimensioni.additem("A3 29,7 x 42 cm" + ls_spazi + "8")
tab_1.tabpage_d.ddlb_dimensioni.additem("A4 21 x 29,7 cm" + ls_spazi + "9")
tab_1.tabpage_d.ddlb_dimensioni.additem("A4 Small 21 x 29,7 cm" + ls_spazi + "10")
tab_1.tabpage_d.ddlb_dimensioni.additem("A5 14,8 x 21 cm" + ls_spazi + "11")
tab_1.tabpage_d.ddlb_dimensioni.additem("B4 25 x 35,4 cm" + ls_spazi + "12")
tab_1.tabpage_d.ddlb_dimensioni.additem("B5 18,2 x 25,7 cm" + ls_spazi + "13")
tab_1.tabpage_d.ddlb_dimensioni.additem("Folio 21,6 x 33 cm" + ls_spazi + "14")
tab_1.tabpage_d.ddlb_dimensioni.additem("Quarto 21,5 x 27,5 cm" + ls_spazi + "15")
tab_1.tabpage_d.ddlb_dimensioni.additem("25,4 x 35,6 cm" + ls_spazi + "16")
tab_1.tabpage_d.ddlb_dimensioni.additem("27,9 x 43,2 cm" + ls_spazi + "17")
tab_1.tabpage_d.ddlb_dimensioni.additem("Nota 21,6 x 27,9 cm" + ls_spazi + "18")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta #9 9,8 x 22,5 cm" + ls_spazi + "19")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta #10 10,4 x 24,1 cm" + ls_spazi + "20")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta #11 11,4 x 26,3 cm" + ls_spazi + "21")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta #12 10,2 x 27,9 cm" + ls_spazi + "22")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta #14 12,7 x 29,2 cm" + ls_spazi + "23")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta DL 11 x 22 cm" + ls_spazi + "27")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta C5 16,2 x 22,9 cm" + ls_spazi + "28")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta C3 32,4 x 45,8 cm" + ls_spazi + "29")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta C4 22,9 x 32,4 cm" + ls_spazi + "30")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta C6 11,4 x 16,2 cm" + ls_spazi + "31")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta C65 11,4 x 22,9 cm" + ls_spazi + "32")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta B4 25 x 35,3 cm" + ls_spazi + "33")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta B5 17,6 x 25 cm" + ls_spazi + "34")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta B6 25 x 12,5 cm" + ls_spazi + "35")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta Monarch 9,8 x 19 cm" + ls_spazi + "37")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta 9,2 x 16,5 cm" + ls_spazi + "38")
tab_1.tabpage_d.ddlb_dimensioni.additem("Busta 11 x 23 cm" + ls_spazi + "36")
tab_1.tabpage_d.ddlb_dimensioni.additem("Foglio C" + ls_spazi + "24")
tab_1.tabpage_d.ddlb_dimensioni.additem("Foglio D" + ls_spazi + "25")
tab_1.tabpage_d.ddlb_dimensioni.additem("Foglio E" + ls_spazi + "26")
tab_1.tabpage_d.ddlb_dimensioni.additem("US Fanfold 37,8 x 27,9 cm" + ls_spazi + "39")
tab_1.tabpage_d.ddlb_dimensioni.additem("German Fanfold 21,6 x 30,4 cm" + ls_spazi + "40")
tab_1.tabpage_d.ddlb_dimensioni.additem("German Legal 21,6 x 33 cm" + ls_spazi + "41")
tab_1.tabpage_d.ddlb_dimensioni.setredraw(true)

tab_1.tabpage_m.em_superiore.text = idw_datawindow.describe("datawindow.print.margin.top")
tab_1.tabpage_m.em_inferiore.text = idw_datawindow.describe("datawindow.print.margin.bottom")
tab_1.tabpage_m.em_sinistro.text = idw_datawindow.describe("datawindow.print.margin.left")
tab_1.tabpage_m.em_destro.text = idw_datawindow.describe("datawindow.print.margin.right")
choose case idw_datawindow.describe("datawindow.units")
	case "0"
		tab_1.tabpage_m.st_unita_superiore.text = "PBU"
	case "1"
		tab_1.tabpage_m.st_unita_superiore.text = "pixel"
	case "2"
		tab_1.tabpage_m.st_unita_superiore.text = "pollici/1000"
	case "3"
		tab_1.tabpage_m.st_unita_superiore.text = "cm/1000"
end choose
tab_1.tabpage_m.st_unita_inferiore.Text = tab_1.tabpage_m.st_unita_superiore.Text
tab_1.tabpage_m.st_unita_sinistro.Text = tab_1.tabpage_m.st_unita_superiore.Text
tab_1.tabpage_m.st_unita_destro.Text = tab_1.tabpage_m.st_unita_superiore.Text

//if idw_datawindow.describe("datawindow.print.orientation") = "1" then
//	rb_orrizontale.checked = true
//	rb_orrizontale.triggerevent(clicked!)
//else
//	rb_verticale.checked = true
//	rb_verticale.triggerevent(clicked!)
//end if

ls_stringa = idw_datawindow.describe("datawindow.print.paper.source")
ll_contatore = tab_1.tabpage_d.ddlb_alimentazioni.totalitems()
for ll_i = ll_contatore to 2 step -1
	ls_campo = tab_1.tabpage_d.ddlb_alimentazioni.text(ll_i)
	if mid(ls_campo, pos(ls_campo, "~t") + 1) = ls_stringa then exit
next
tab_1.tabpage_d.ddlb_alimentazioni.selectitem(ll_i)

ls_stringa = idw_datawindow.describe("datawindow.print.paper.size")
ll_contatore = tab_1.tabpage_d.ddlb_dimensioni.totalitems()
for ll_i = ll_contatore to 2 step -1
	ls_campo = tab_1.tabpage_d.ddlb_dimensioni.text(ll_i)
	if mid(ls_campo, pos(ls_campo, "~t") + 1) = ls_stringa then exit
next
tab_1.tabpage_d.ddlb_dimensioni.selectitem(ll_i)


//lw_oggetti[1] = st_dimensioni
//lw_oggetti[2] = ddlb_dimensioni
//lw_oggetti[3] = st_alimentazioni
//lw_oggetti[4] = ddlb_alimentazioni
//lw_oggetti[5] = gb_orientamento
//lw_oggetti[6] = p_orrizontale
//lw_oggetti[7] = p_verticale
//lw_oggetti[8] = rb_orrizontale
//lw_oggetti[9] = rb_verticale
//dw_folder.fu_assigntab(2, "Foglio", lw_oggetti[])
//lw_oggetti[1]  = st_superiore
//lw_oggetti[2]  = em_superiore
//lw_oggetti[3]  = st_inferiore
//lw_oggetti[4]  = em_inferiore
//lw_oggetti[5]  = st_sinistro
//lw_oggetti[6]  = em_sinistro
//lw_oggetti[7]  = st_destro
//lw_oggetti[8]  = em_destro
//lw_oggetti[9]  = st_unita_superiore
//lw_oggetti[10] = st_unita_inferiore
//lw_oggetti[11] = st_unita_sinistro
//lw_oggetti[12] = st_unita_destro
//dw_folder.fu_assigntab(1, "Margini", lw_oggetti[])
//dw_folder.fu_foldercreate(2, 4)
//dw_folder.fu_selecttab(1)

//dw_anteprima.bringtotop = true
//gb_orientamento.bringtotop = true
//gb_anteprima.bringtotop = true

//imp_anteprima()

//// stefanop 22/09/2010: imposto immagine
//p_verticale.picturename = s_cs_xx.volume + s_cs_xx.risorse + "ver.bmp"
//p_orrizontale.picturename = s_cs_xx.volume + s_cs_xx.risorse + "orr.bmp"
// ----

f_po_setwindowposition(this)

end event

on w_imp_pagina.create
this.tab_1=create tab_1
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.Control[]={this.tab_1,&
this.cb_ok,&
this.cb_annulla}
end on

on w_imp_pagina.destroy
destroy(this.tab_1)
destroy(this.cb_ok)
destroy(this.cb_annulla)
end on

type tab_1 from tab within w_imp_pagina
event create ( )
event destroy ( )
integer x = 23
integer y = 40
integer width = 1486
integer height = 680
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_m tabpage_m
tabpage_d tabpage_d
end type

on tab_1.create
this.tabpage_m=create tabpage_m
this.tabpage_d=create tabpage_d
this.Control[]={this.tabpage_m,&
this.tabpage_d}
end on

on tab_1.destroy
destroy(this.tabpage_m)
destroy(this.tabpage_d)
end on

type tabpage_m from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 1449
integer height = 552
long backcolor = 67108864
string text = "Margini"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_unita_destro st_unita_destro
st_unita_sinistro st_unita_sinistro
st_unita_inferiore st_unita_inferiore
st_unita_superiore st_unita_superiore
st_destra st_destra
st_sinistra st_sinistra
st_inferiore st_inferiore
st_superiore st_superiore
em_destro em_destro
em_sinistro em_sinistro
em_inferiore em_inferiore
em_superiore em_superiore
end type

on tabpage_m.create
this.st_unita_destro=create st_unita_destro
this.st_unita_sinistro=create st_unita_sinistro
this.st_unita_inferiore=create st_unita_inferiore
this.st_unita_superiore=create st_unita_superiore
this.st_destra=create st_destra
this.st_sinistra=create st_sinistra
this.st_inferiore=create st_inferiore
this.st_superiore=create st_superiore
this.em_destro=create em_destro
this.em_sinistro=create em_sinistro
this.em_inferiore=create em_inferiore
this.em_superiore=create em_superiore
this.Control[]={this.st_unita_destro,&
this.st_unita_sinistro,&
this.st_unita_inferiore,&
this.st_unita_superiore,&
this.st_destra,&
this.st_sinistra,&
this.st_inferiore,&
this.st_superiore,&
this.em_destro,&
this.em_sinistro,&
this.em_inferiore,&
this.em_superiore}
end on

on tabpage_m.destroy
destroy(this.st_unita_destro)
destroy(this.st_unita_sinistro)
destroy(this.st_unita_inferiore)
destroy(this.st_unita_superiore)
destroy(this.st_destra)
destroy(this.st_sinistra)
destroy(this.st_inferiore)
destroy(this.st_superiore)
destroy(this.em_destro)
destroy(this.em_sinistro)
destroy(this.em_inferiore)
destroy(this.em_superiore)
end on

type st_unita_destro from statictext within tabpage_m
integer x = 827
integer y = 348
integer width = 594
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "none"
boolean focusrectangle = false
end type

type st_unita_sinistro from statictext within tabpage_m
integer x = 827
integer y = 248
integer width = 594
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "none"
boolean focusrectangle = false
end type

type st_unita_inferiore from statictext within tabpage_m
integer x = 827
integer y = 148
integer width = 594
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "none"
boolean focusrectangle = false
end type

type st_unita_superiore from statictext within tabpage_m
integer x = 827
integer y = 48
integer width = 594
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "none"
boolean focusrectangle = false
end type

type st_destra from statictext within tabpage_m
integer x = 96
integer y = 348
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "Destra"
boolean focusrectangle = false
end type

type st_sinistra from statictext within tabpage_m
integer x = 96
integer y = 248
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "Sinistra"
boolean focusrectangle = false
end type

type st_inferiore from statictext within tabpage_m
integer x = 96
integer y = 148
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "Inferiore"
boolean focusrectangle = false
end type

type st_superiore from statictext within tabpage_m
integer x = 96
integer y = 48
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "Superiore"
boolean focusrectangle = false
end type

type em_destro from editmask within tabpage_m
integer x = 507
integer y = 348
integer width = 297
integer height = 80
integer taborder = 90
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
borderstyle borderstyle = stylelowered!
string mask = "#####0"
end type

type em_sinistro from editmask within tabpage_m
integer x = 507
integer y = 248
integer width = 297
integer height = 80
integer taborder = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
borderstyle borderstyle = stylelowered!
string mask = "#####0"
end type

type em_inferiore from editmask within tabpage_m
integer x = 507
integer y = 148
integer width = 297
integer height = 80
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
borderstyle borderstyle = stylelowered!
string mask = "#####0"
end type

type em_superiore from editmask within tabpage_m
integer x = 507
integer y = 48
integer width = 297
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "none"
borderstyle borderstyle = stylelowered!
string mask = "#####0"
end type

type tabpage_d from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 1449
integer height = 552
long backcolor = 67108864
string text = "Dimensioni"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_alimentazione st_alimentazione
ddlb_alimentazioni ddlb_alimentazioni
st_dimensione st_dimensione
ddlb_dimensioni ddlb_dimensioni
end type

on tabpage_d.create
this.st_alimentazione=create st_alimentazione
this.ddlb_alimentazioni=create ddlb_alimentazioni
this.st_dimensione=create st_dimensione
this.ddlb_dimensioni=create ddlb_dimensioni
this.Control[]={this.st_alimentazione,&
this.ddlb_alimentazioni,&
this.st_dimensione,&
this.ddlb_dimensioni}
end on

on tabpage_d.destroy
destroy(this.st_alimentazione)
destroy(this.ddlb_alimentazioni)
destroy(this.st_dimensione)
destroy(this.ddlb_dimensioni)
end on

type st_alimentazione from statictext within tabpage_d
integer x = 96
integer y = 268
integer width = 434
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "A&limentazione:"
boolean focusrectangle = false
end type

type ddlb_alimentazioni from dropdownlistbox within tabpage_d
integer x = 96
integer y = 328
integer width = 777
integer height = 360
integer taborder = 90
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_dimensione from statictext within tabpage_d
integer x = 96
integer y = 48
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "Dimensioni:"
boolean focusrectangle = false
end type

type ddlb_dimensioni from dropdownlistbox within tabpage_d
integer x = 96
integer y = 108
integer width = 777
integer height = 360
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_ok from commandbutton within w_imp_pagina
integer x = 731
integer y = 760
integer width = 366
integer height = 100
integer taborder = 90
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Ok"
boolean default = true
end type

event clicked;string	ls_stringa, ls_campo


ls_stringa = "datawindow.print.margin.top=" + tab_1.tabpage_m.em_superiore.text
ls_stringa = ls_stringa + " datawindow.print.margin.bottom=" + tab_1.tabpage_m.em_inferiore.text
ls_stringa = ls_stringa + " datawindow.print.margin.left=" + tab_1.tabpage_m.em_sinistro.text
ls_stringa = ls_stringa + " datawindow.print.margin.right=" + tab_1.tabpage_m.em_destro.text

//ls_stringa = ls_stringa + " datawindow.print.orientation="
//if rb_orrizontale.checked then
//	ls_stringa = ls_stringa + "1"
//else
//	ls_stringa = ls_stringa + "2"
//end if

ls_campo = tab_1.tabpage_d.ddlb_alimentazioni.text
ls_stringa = ls_stringa + " datawindow.print.paper.source=" + &
				 mid(ls_campo, pos(ls_campo, "~t") + 1)

ls_campo = tab_1.tabpage_d.ddlb_dimensioni.text
ls_stringa = ls_stringa + " datawindow.print.paper.size=" + &
				 mid(ls_campo, pos(ls_campo, "~t") + 1)

ls_stringa = idw_datawindow.modify(ls_stringa)


close(parent)

end event

type cb_annulla from commandbutton within w_imp_pagina
integer x = 1120
integer y = 760
integer width = 366
integer height = 100
integer taborder = 100
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Annulla"
boolean cancel = true
end type

on clicked;close(parent)

end on

