﻿$PBExportHeader$w_import_grid_form.srw
$PBExportComments$Window Report di tipo Grid e Form
forward
global type w_import_grid_form from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_import_grid_form
end type
type cb_seleziona_file from commandbutton within w_import_grid_form
end type
type cb_importa from commandbutton within w_import_grid_form
end type
type st_log from statictext within w_import_grid_form
end type
type dw_import from uo_cs_xx_dw within w_import_grid_form
end type
end forward

global type w_import_grid_form from w_cs_xx_principale
integer x = 37
integer y = 212
integer width = 4178
integer height = 2008
string title = "Report"
cb_1 cb_1
cb_seleziona_file cb_seleziona_file
cb_importa cb_importa
st_log st_log
dw_import dw_import
end type
global w_import_grid_form w_import_grid_form

type variables
private:
	boolean ib_file_selected = false
end variables

forward prototypes
public subroutine wf_enable_edit ()
end prototypes

public subroutine wf_enable_edit ();string ls_error
int li_i
for li_i = 1 to integer(dw_import.Object.DataWindow.Column.Count)
	ls_error = dw_import.modify("#" + string(li_i) + ".TabSequence=" + string(li_i * 10))
	if ls_error <> "" then
		messagebox("", ls_error)
		return
	end if
next
end subroutine

on w_import_grid_form.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_seleziona_file=create cb_seleziona_file
this.cb_importa=create cb_importa
this.st_log=create st_log
this.dw_import=create dw_import
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_seleziona_file
this.Control[iCurrent+3]=this.cb_importa
this.Control[iCurrent+4]=this.st_log
this.Control[iCurrent+5]=this.dw_import
end on

on w_import_grid_form.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_seleziona_file)
destroy(this.cb_importa)
destroy(this.st_log)
destroy(this.dw_import)
end on

event pc_setwindow;call super::pc_setwindow;string ls_sintassi,ls_errore,ls_indice
long ll_width
integer li_num_colonne,li_i

dw_import.ib_dw_report = true

dw_import.Create(s_cs_xx.parametri.parametro_s_15, ls_errore)
if len(ls_errore) > 0 then
	g_mb.messagebox("Apice", "Errore Durante la Creazione del Report", Exclamation!)
end if

dw_import.set_dw_options(sqlca, c_nulldw, c_NoRetrieveOnOpen, c_default)

iuo_dw_main = dw_import

save_on_close(c_socnosave)
wf_enable_edit()
end event

event resize;/**
 * stefanop
 * 01/01/2010
 *
 * TOLTO ANCESTOR SCRIPT
 **/

cb_importa.move(newwidth - cb_importa.width - 20, 20)
cb_seleziona_file.move(cb_importa.x - cb_seleziona_file.width - 20, 20)
st_log.move(20, 40)

dw_import.move(20, cb_seleziona_file.y + cb_seleziona_file.height + 20)
dw_import.resize(newwidth - 40 , newheight - dw_import.y - 30)

end event

type cb_1 from commandbutton within w_import_grid_form
integer x = 2949
integer y = 20
integer width = 343
integer height = 104
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "none"
end type

event clicked;string ls_error

ls_error = dw_import.modify("Datawindow.Table.UpdateWhere=2")
ls_error = dw_import.modify("Datawindow.Table.updatetable='parametri_azienda'")
//ls_error = dw_import.modify("Datawindow.Table.UpdateWhere=2")
//ls_error = dw_import.modify("Datawindow.Table.UpdateWhere=2")
//if ls_error <> "" then
//	st_log.text = ls_error
//	return -1
//end if
end event

type cb_seleziona_file from commandbutton within w_import_grid_form
integer x = 3291
integer y = 20
integer width = 471
integer height = 100
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Seleziona file"
end type

event clicked;string ls_file_path, ls_file_name, ls_filter_ext, ls_error
int li_import_return

ls_filter_ext = "File ammessi (*.txt; *.csv;*.xml),*.txt;*.csv;*.xml"

if GetFileOpenName("Seleziona file da importare", ls_file_path, ls_file_name, "TXT", ls_filter_ext) = 0 then
	return -1
end if

st_log.text = "Importazione file in corso..."
li_import_return = dw_import.importfile(ls_file_path)
choose case li_import_return
	case -1
		st_log.text = "No rows or startrow value supplied is greater than the number of rows in the file"
	case -2
		st_log.text = "Empty file"
	case -3
		st_log.text = "Invalid argument"
	case -4
		st_log.text = "Invalid input"
	case -5
		st_log.text = "Could not open the file"
	case -6
		st_log.text = "Could not close the file"
	case -7
		st_log.text = "Error reading the text"
	case -8
		st_log.text = "Unsupported file name suffix (must be *.txt, *.csv, *.dbf or *.xml)"
	case -10
		st_log.text = "Unsupported dBase file format (not version 2 or 3)"
	case -11
		st_log.text = "XML Parsing Error; XML parser libraries not found or XML not well formed"
	case -12
		st_log.text = "XML Template does not exist or does not match the DataWindow"
	case -13
		st_log.text = "Unsupported DataWindow style for import"
	case -14
		st_log.text = "Error resolving DataWindow nesting"
	case -15
		st_log.text = "File size exceeds limit"
end choose

if li_import_return < 0 then 
	return -1
end if

ls_error = dw_import.modify("Datawindow.Table.UpdateWhere=2")
if ls_error <> "" then
	st_log.text = ls_error
	return -1
end if

cb_importa.enabled = true
wf_enable_edit()
//parent.triggerevent("pc_modify")

st_log.text = ""
end event

type cb_importa from commandbutton within w_import_grid_form
integer x = 3771
integer y = 20
integer width = 343
integer height = 100
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Importa"
end type

event clicked;if dw_import.rowcount() > 0 then
	
	st_log.text = "Salvataggio nel database..."
	if dw_import.update() = 1 then
		st_log.text = "Salvataggio eseguito con successo"
		dw_import.reset()
		
		ib_file_selected = false
		enabled = false
	else
		st_log.text = "Errore durante il salvataggio nel database"
	end if
else
	st_log.text = "Niente da importare"
end if
end event

type st_log from statictext within w_import_grid_form
integer x = 20
integer y = 40
integer width = 2423
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type dw_import from uo_cs_xx_dw within w_import_grid_form
event ue_leftbottonup pbm_lbuttonup
integer x = 23
integer y = 140
integer width = 4069
integer height = 1220
integer taborder = 30
string dataobject = ""
boolean hscrollbar = true
boolean vscrollbar = true
end type

event ue_leftbottonup;//object_style los_style
//string ls_mod, ls_nome_oggetto, ls_colore_sfondo
//int li_i
//
//// se ho degli oggetti selezionati ma il tasto Cntl non è premuto,
//// allora questa non è una multi-selezione e devo deselezionare tutti
//// gli oggetti precedentemente selezionati resettando i colori di sfondo e
//// testo ai vecchi valori
//
//if KeyDown(keycontrol!) = false and UpperBound(is_nome_oggetti) > 0 then
//	for li_i = 1 to UpperBound(is_nome_oggetti)
//		if is_nome_oggetti[li_i] <> "" then
//			// Se lo sfondo originale è trasparente forzo il "mode"
//			if is_hold_backmode [li_i] = "1" then
//				ls_colore_sfondo = is_nome_oggetti [li_i] + ".background.mode=~"1~"" 
//			else
//				ls_colore_sfondo = is_nome_oggetti [li_i] + ".background.color=~"" + is_hold_backcolor[li_i] + "~""
//			end if
//			ls_mod = is_nome_oggetti[li_i] + ".color=~"" + is_hold_color[li_i] + "~" " + ls_colore_sfondo
//			dw_report.Modify(ls_mod)
//			is_nome_oggetti[li_i] = ""
//		end if
//	next
//end if
//
//ls_nome_oggetto = dw_report.GetObjectAtPointer( )
//if ls_nome_oggetto = "" then Return
//ls_nome_oggetto = f_estrai_token(ls_nome_oggetto, "~t")
//
//if UpperBound(is_nome_oggetti) = 0 then
//	li_i = 1
//else
//	For li_i = 1 to UpperBound(is_nome_oggetti)
//		if is_nome_oggetti [li_i] = ls_nome_oggetto then return
//		if is_nome_oggetti [li_i] = "" then exit
//	next
//end if
//
//is_nome_oggetti[li_i] = ls_nome_oggetto
//
//is_hold_color[li_i] = dw_report.Describe(ls_nome_oggetto + ".color")
//is_hold_backcolor [li_i] = dw_report.Describe(ls_nome_oggetto + ".background.color")
//is_hold_backmode [li_i] = dw_report.Describe(ls_nome_oggetto + ".background.mode")
//
//if is_hold_color[li_i] = "?" then is_hold_color[li_i] = "0"
//
//if is_hold_backmode [li_i] = "1" then
//	is_hold_backcolor [li_i] = dw_report.Describe("datawindow.color")
//end if
//
//los_style.font_face = dw_report.Describe(ls_nome_oggetto + ".font.face")
//los_style.font_weight = dw_report.Describe(ls_nome_oggetto + ".font.weight") 
//los_style.italic = dw_report.Describe(ls_nome_oggetto + ".font.italic")
//los_style.underline = dw_report.Describe(ls_nome_oggetto + ".font.underline")
//los_style.border = dw_report.Describe(ls_nome_oggetto + ".border")
//los_style.alignment = dw_report.Describe(ls_nome_oggetto + ".alignment")
//los_style.font_height = dw_report.Describe(ls_nome_oggetto + ".font.height")
//
//if Integer(los_style.font_height) > 0 Then 
//	los_style.font_height = 	wf_conv_altezza_punti(dw_report,los_style.font_height)
//else
//	los_style.font_height =  String(Abs(Integer(los_style.font_height)))
//end if
//
//if dw_report.Describe(ls_nome_oggetto + ".type") = "text" and li_i = 1 then
//	los_style.text = f_elimina_carattere (dw_report.Describe(ls_nome_oggetto + ".text"), "~"")
//	is_text = true
//else
//	los_style.text = ""
//	is_text = false
//end if
//
//ls_mod = ls_nome_oggetto + ".background.mode=~"0~" " + &
//				 ls_nome_oggetto + ".color=~"" + string(rgb(255,255,255)) + "~" " + &
//				 ls_nome_oggetto + ".background.color=~"0~"" 
//dw_report.Modify(ls_mod)
//
//ib_updating = true
//
//uo_font.uof_set_fontstyle(los_style)
//wf_set_border(los_style)
//
//uo_font_color.uof_set_color(long(dw_report.Describe(ls_nome_oggetto + ".color")))
//uo_background_color.uof_set_color(long(dw_report.Describe(ls_nome_oggetto + ".background.color")))
//
//ib_updating = false
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

