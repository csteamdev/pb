﻿$PBExportHeader$w_importa_voci.srw
forward
global type w_importa_voci from w_cs_xx_principale
end type
type cbx_1 from checkbox within w_importa_voci
end type
type cb_1 from commandbutton within w_importa_voci
end type
end forward

global type w_importa_voci from w_cs_xx_principale
integer width = 1326
integer height = 324
string title = "Importazione Voci nel Repository"
boolean minbox = false
boolean maxbox = false
cbx_1 cbx_1
cb_1 cb_1
end type
global w_importa_voci w_importa_voci

on w_importa_voci.create
int iCurrent
call super::create
this.cbx_1=create cbx_1
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_1
this.Control[iCurrent+2]=this.cb_1
end on

on w_importa_voci.destroy
call super::destroy
destroy(this.cbx_1)
destroy(this.cb_1)
end on

type cbx_1 from checkbox within w_importa_voci
integer x = 23
integer y = 140
integer width = 1234
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Azzera Repository prima dell~'importazione"
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_importa_voci
integer x = 23
integer y = 20
integer width = 1230
integer height = 104
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "IMPORTA REPOSITORY DALLA TABELLA VOCI"
end type

event clicked;long   ll_i

string ls_nome_window, ls_descrizione

datastore lds_voci


if cbx_1.checked then
	
	delete from voci_repository;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("VOCI","Errore in svuotamento repository: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
end if

setpointer(hourglass!)

lds_voci = create datastore

lds_voci.dataobject = "d_importa_voci"

lds_voci.settransobject(sqlca)

lds_voci.retrieve()

for ll_i = 1 to lds_voci.rowcount()
	
	ls_nome_window = lds_voci.getitemstring(ll_i,"nome_window")
	
	ls_descrizione = lds_voci.getitemstring(ll_i,"des_voce")
	
	insert
	into   voci_repository
		  	 (id_voce,
		  	 collegamento,
		  	 descrizione,
		  	 flag_aggiornato)
	values (:ll_i,
			  :ls_nome_window,
			  :ls_descrizione,
			  'N');
			  
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("VOCI","Errore in inserimento voce: " + sqlca.sqlerrtext)
		destroy lds_voci
		rollback;
		setpointer(arrow!)
		return -1
	end if
	
next

destroy lds_voci

setpointer(arrow!)

commit;
end event

