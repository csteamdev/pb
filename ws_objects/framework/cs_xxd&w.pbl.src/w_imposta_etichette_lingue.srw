﻿$PBExportHeader$w_imposta_etichette_lingue.srw
forward
global type w_imposta_etichette_lingue from window
end type
type dw_colonne_dinamiche from datawindow within w_imposta_etichette_lingue
end type
type pb_trova from picturebutton within w_imposta_etichette_lingue
end type
type dw_scelta_lingua from datawindow within w_imposta_etichette_lingue
end type
type tv_label from treeview within w_imposta_etichette_lingue
end type
type cb_3 from commandbutton within w_imposta_etichette_lingue
end type
type cb_2 from commandbutton within w_imposta_etichette_lingue
end type
type dw_2 from datawindow within w_imposta_etichette_lingue
end type
type cb_cancella from commandbutton within w_imposta_etichette_lingue
end type
type dw_1 from datawindow within w_imposta_etichette_lingue
end type
type dw_oggetti_window from datawindow within w_imposta_etichette_lingue
end type
type dw_imposta_colonne_importanti from datawindow within w_imposta_etichette_lingue
end type
type dw_repository_tooltip from datawindow within w_imposta_etichette_lingue
end type
type sle_cerca from singlelineedit within w_imposta_etichette_lingue
end type
type st_1 from statictext within w_imposta_etichette_lingue
end type
end forward

global type w_imposta_etichette_lingue from window
integer width = 4768
integer height = 2752
boolean titlebar = true
string title = "Gestione Sistema Multilingue"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
event ue_carica_tree ( )
event ue_filtra_finestra ( )
dw_colonne_dinamiche dw_colonne_dinamiche
pb_trova pb_trova
dw_scelta_lingua dw_scelta_lingua
tv_label tv_label
cb_3 cb_3
cb_2 cb_2
dw_2 dw_2
cb_cancella cb_cancella
dw_1 dw_1
dw_oggetti_window dw_oggetti_window
dw_imposta_colonne_importanti dw_imposta_colonne_importanti
dw_repository_tooltip dw_repository_tooltip
sle_cerca sle_cerca
st_1 st_1
end type
global w_imposta_etichette_lingue w_imposta_etichette_lingue

type variables
boolean ib_modificato_colonne=false, ib_modificato_oggetti=false, ib_modificato_tooltip=false, ib_modificato_col_din = false, ib_modificato_storico_dw=false

long il_rowstart=1, il_handle=0, il_rootitem=0, il_handle_padre=-1

string is_nome_window
end variables

forward prototypes
public function integer wf_carica_oggetti_window (long al_handle)
public function integer wf_trova (string as_ricerca, long al_handle)
public subroutine wf_carica_label_dw (string fs_nome_datawindow, long fl_tipo_item)
public subroutine wf_elimina ()
public subroutine wf_salva (long al_handle)
public subroutine wf_salva_colonne (long al_handle)
public subroutine wf_salva_oggetti (long al_handle)
public subroutine wf_salva_tooltip (long al_handle)
public function integer wf_tv (string fs_library)
public function integer wf_carica_colonne_dinamiche ()
public subroutine wf_salva_col_din (long al_handle)
public subroutine wf_carica_label_dw_tab (tab at_tab)
public subroutine wf_carica_colonne_storico (string as_nomefinestra, string as_nomidw[])
public subroutine wf_salva_storico_dw (string as_nomefinestra)
public subroutine wf_carica_dw_storico (string as_nomefinestra, ref datawindow dws_appoggio)
end prototypes

event ue_carica_tree();string ls_library

tv_label.setredraw( false )

// sto impostando il filtro per partendo da una finestra padre?
if isvalid(pcca.window_current) and not isnull(pcca.window_current) then
	sle_cerca.text = pcca.window_current.ClassName()
end if
// ----

setnull( ls_library )
wf_tv( ls_library )

tv_label.setredraw( true )

post event ue_filtra_finestra()

end event

event ue_filtra_finestra();/**
 * filtro in base alla finestra aperta
 **/
 
if isvalid(pcca.window_current) and not isnull(pcca.window_current) then
	
	sle_cerca.text = pcca.window_current.ClassName()
	pb_trova.trigger event clicked()
	
end if
end event

public function integer wf_carica_oggetti_window (long al_handle);integer li_contDw
string ls_str, ls_type, ls_oggetto, ls_testo, ls_cod_lingua, ls_nomi[], ls_nome_oggetto, ls_tooltip, ls_nomiDw[]
long   ll_riga, ll_pos, ll_controls, ll_i, ll_pos2, ll_for, ll_tipo_item
window l_window
str_label_window lstr_label_win
treeviewitem ltv_item
commandbutton l_commandbutton
checkbox      l_checkbox
radiobutton   l_radiobutton
statictext    l_statictext
groupbox      l_groupbox
datawindow	  l_datawindow

this.setRedraw(false)
dw_oggetti_window.setredraw(false)
dw_imposta_colonne_importanti.setredraw(false)
dw_repository_tooltip.setredraw(false)

dw_oggetti_window.reset()
dw_imposta_colonne_importanti.reset()
dw_repository_tooltip.reset()

ls_cod_lingua = dw_scelta_lingua.getitemstring(1,"cod_lingua")

if isnull(ls_cod_lingua) then ls_cod_lingua = "GENERALE"

tv_label.GetItem (al_handle, ltv_item )

lstr_label_win = ltv_item.data

ls_nome_oggetto = lstr_label_win.nome
ll_tipo_item	 = lstr_label_win.tipo_item

l_window = create using ls_nome_oggetto

ll_controls = upperbound(l_window.control)


// ----------  INSERISCO MANUALMENTE LA RIGA RELATIVA AL TITOLO DELLA FINESTRA --------------------- //

ll_riga = dw_oggetti_window.insertrow(0)
dw_oggetti_window.setitem(ll_riga, "nome_dw", ls_nome_oggetto)
dw_oggetti_window.setitem(ll_riga, "nome_oggetto", "this.title")
dw_oggetti_window.setitem(ll_riga, "testo", l_window.title)
dw_oggetti_window.setitem(ll_riga, "flag_tipo_oggetto", "W")
ls_oggetto = "this.title"

setnull(ls_testo)
if ls_cod_lingua <> "GENERALE" then
	select testo
	into   :ls_testo
	from   tab_w_text_repository
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_lingua = :ls_cod_lingua and
			 nome_window = :ls_nome_oggetto and
			 nome_oggetto = :ls_oggetto and
			 flag_ambiente = 'C';
else
	select testo
	into   :ls_testo
	from   tab_w_text_repository
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_lingua is null and
			 nome_window = :ls_nome_oggetto and
			 nome_oggetto = :ls_oggetto and
			 flag_ambiente = 'C';
end if

if len(trim(ls_testo)) < 1 then setnull(ls_testo)

dw_oggetti_window.setitem(ll_riga, "testo_modificato", ls_testo )

// ----------  PROCEDO CON IL RESTO DEGLI OGGETTI --------------------- //

if(il_handle_padre<>tv_label.findItem(ParentTreeItem!, al_handle)) Then
	li_contDw=0
	//dw_storico.Reset()
end if

for ll_i = 1 to ll_controls
	
	if l_window.control[ll_i].typeof() = checkbox! or &
		l_window.control[ll_i].typeof() = commandbutton! or &
		l_window.control[ll_i].typeof() = radiobutton!  or &
		l_window.control[ll_i].typeof() = statictext! or &
		l_window.control[ll_i].typeof() = datawindow! or &
		l_window.control[ll_i].typeof() = groupbox! or &
		l_window.control[ll_i].typeof() = tab! then
		
		choose case l_window.control[ll_i].typeof()
				
			case checkbox! 
				l_checkbox = l_window.control[ll_i]
				ls_testo = l_checkbox.text
				
			case commandbutton!
				l_commandbutton = l_window.control[ll_i]
				ls_testo = l_commandbutton.text
				
			case radiobutton!   
				l_radiobutton = l_window.control[ll_i]
				ls_testo = l_radiobutton.text
				
			case statictext! 
				l_statictext = l_window.control[ll_i]
				ls_testo = l_statictext.text
				
			case groupbox!
				l_groupbox = l_window.control[ll_i]
				ls_testo = l_groupbox.text
				
			case datawindow!

				//l_datawindow = Create datawindow
				l_datawindow = l_window.control[ll_i]

				OpenUserObject(l_datawindow,10,10)
				
				if l_datawindow.dataobject <> "d_folder_tb" then
					wf_carica_label_dw(l_datawindow.dataobject, ll_tipo_item)
					if(il_handle_padre<>tv_label.findItem(ParentTreeItem!, al_handle)) Then
						//Se la tabella è aggiornabile...
						if(l_datawindow.Describe("Datawindow.Table.Updatetable")<>"?") Then
							li_contDw++
							//ls_nomiDw[li_contDw]=l_datawindow.dataobject
							ls_nomiDw[li_contDw]=l_datawindow.ClassName()
							wf_carica_dw_storico(ls_nome_oggetto,l_datawindow)
						end if
					end if
				end if
				
				CloseUserObject(l_datawindow)
				
				//destroy l_datawindow
				
				continue
				
			case tab!
				wf_carica_label_dw_tab(l_window.control[ll_i])
				
			case else
				
				continue
					
		end choose
		
		if ll_tipo_item = 3 then
			
			if isnull(ls_cod_lingua) then
				// è tooltip
				ll_riga = dw_repository_tooltip.insertrow(0)
				dw_repository_tooltip.setitem(ll_riga, "nome_dw", ls_nome_oggetto)
				dw_repository_tooltip.setitem(ll_riga, "nome_oggetto", l_window.control[ll_i].classname())
				dw_repository_tooltip.setitem(ll_riga, "flag_tipo_oggetto", "W")
			end if
		else
			// non è tooltip
			ll_riga = dw_oggetti_window.insertrow(0)
			dw_oggetti_window.setitem(ll_riga, "nome_dw", ls_nome_oggetto)
			dw_oggetti_window.setitem(ll_riga, "nome_oggetto", l_window.control[ll_i].classname())
			dw_oggetti_window.setitem(ll_riga, "testo", ls_testo)
			dw_oggetti_window.setitem(ll_riga, "flag_tipo_oggetto", "W")
		end if
		
		ls_oggetto = l_window.control[ll_i].classname()
		setnull(ls_testo)
		if ls_cod_lingua <> "GENERALE" then
			select testo
			into   :ls_testo
			from   tab_w_text_repository
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_lingua = :ls_cod_lingua and
					 nome_window = :ls_nome_oggetto and
					 nome_oggetto = :ls_oggetto and
			 		 flag_ambiente = 'C';
		else
			select testo
			into   :ls_testo
			from   tab_w_text_repository
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_lingua is null and
					 nome_window = :ls_nome_oggetto and
					 nome_oggetto = :ls_oggetto and
			 		 flag_ambiente = 'C';
		end if
		
		if len(trim(ls_testo)) < 1 then setnull(ls_testo)
		
		if ll_tipo_item = 3 then
			if isnull( ls_cod_lingua) then
				dw_repository_tooltip.setitem(ll_riga, "tooltip", ls_testo )
			end if
		else
			dw_oggetti_window.setitem(ll_riga, "testo_modificato", ls_testo )
		end if
		
	end if
	
next

if(il_handle_padre<>tv_label.findItem(ParentTreeItem!, al_handle)) Then
	wf_carica_colonne_storico(l_window.className(), ls_nomiDw)
	il_handle_padre=tv_label.findItem(ParentTreeItem!, al_handle)
end if

destroy l_window

dw_oggetti_window.setsort("flag_tipo_oggetto DESC, nome_dw ASC, nome_oggetto ASC")
dw_oggetti_window.sort()

dw_imposta_colonne_importanti.setsort("flag_tipo_oggetto DESC, nome_dw ASC, nome_oggetto ASC")
dw_imposta_colonne_importanti.sort()

dw_repository_tooltip.setsort("flag_tipo_oggetto DESC, nome_dw ASC, nome_oggetto ASC")
dw_repository_tooltip.sort()

dw_oggetti_window.setredraw(true)
dw_imposta_colonne_importanti.setredraw(true)
dw_repository_tooltip.setredraw(true)
this.setRedraw(true)

return 0

end function

public function integer wf_trova (string as_ricerca, long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item



ll_handle = al_handle

do while ll_handle > 0
	
	tv_label.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(as_ricerca),1) > 0 then
		tv_label.setfocus()
		tv_label.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	end if
	
	ll_handle = tv_label.finditem(nexttreeitem!,ll_handle)
	
	// non ci sono più rami nel tree (fine dei fratelli di stesso livello)
	if ll_handle = 0 then
		return 100
	end if
		
loop

return 100

//ll_handle = tv_label.finditem(childtreeitem!,al_handle)
//
//if ll_handle > 0 then
//	
//	tv_label.getitem(ll_handle,ltv_item)
//	
//	if pos(upper(ltv_item.label),upper(as_ricerca),1) > 0 then
//		tv_label.setfocus()
//		tv_label.selectitem(ll_handle)
//		il_handle = ll_handle
//		return 0
//	else
//		if wf_trova(as_ricerca,ll_handle) = 0 then
//			return 0
//		else
//			return 100
//		end if
//	end if
//	
//end if

//ll_handle = tv_label.finditem(nexttreeitem!,al_handle)

//ll_handle = al_handle
//
//if ll_handle > 0 then
//	
//	tv_label.getitem(ll_handle,ltv_item)
//	
//	if pos(upper(ltv_item.label),upper(as_ricerca),1) > 0 then
//		tv_label.setfocus()
//		tv_label.selectitem(ll_handle)
//		il_handle = ll_handle
//		return 0
//	else
//		
//		ll_handle = tv_label.finditem(nexttreeitem!,al_handle)
//		
//		if wf_trova(as_ricerca,ll_handle) = 0 then
//			return 0
//		else
//			return 100
//		end if
//	end if
//	
//end if
//
//ll_padre = al_handle
//
//do
//	
//	ll_padre = tv_label.finditem(parenttreeitem!,ll_padre)
//
//	if ll_padre > 0 then
//		
//		ll_handle = tv_label.finditem(nexttreeitem!,ll_padre)
//		
//		if ll_handle > 0 then
//		
//			tv_label.getitem(ll_handle,ltv_item)
//			
//			if pos(upper(ltv_item.label),upper(as_ricerca),1) > 0 then
//				tv_label.setfocus()
//				tv_label.selectitem(ll_handle)
//				il_handle = ll_handle
//				return 0
//			else
//				if wf_trova(as_ricerca,ll_handle) = 0 then
//					return 0
//				else
//					return 100
//				end if
//			end if
//			
//		end if
//		
//	end if
//
//loop while ll_padre > 0
//
//return 100
end function

public subroutine wf_carica_label_dw (string fs_nome_datawindow, long fl_tipo_item);string ls_str, ls_type, ls_oggetto, ls_testo, ls_cod_lingua, ls_objects, ls_dataobjects, &
		 ls_flag_importante, ls_tabsequence, ls_flag_obbligatorio, ls_tooltip

long   ll_riga, ll_pos, ll_controls, ll_i, ll_pos2, ll_for

ls_cod_lingua = dw_scelta_lingua.getitemstring(1,"cod_lingua")
if isnull(ls_cod_lingua) then ls_cod_lingua = "GENERALE"

// EnMe 04-04-13 se passo un nome vuoto (caso DW dinamica)
if isnull(fs_nome_datawindow) or len(trim(fs_nome_datawindow)) < 1 then return 

dw_1.dataobject = fs_nome_datawindow

ls_str = dw_1.Object.DataWindow.Objects

// stefanop 27/07/2011: aggiunto campo zoom a mano per ogni DW
ll_riga = dw_oggetti_window.insertrow(0)
ls_oggetto = "Zoom"
dw_oggetti_window.setitem(ll_riga, "nome_dw", fs_nome_datawindow)
dw_oggetti_window.setitem(ll_riga, "nome_oggetto", ls_oggetto)
dw_oggetti_window.setitem(ll_riga, "flag_tipo_oggetto", "D")
dw_oggetti_window.setitem(ll_riga, "testo", "Impostare il valore di zoom in %")
if ls_cod_lingua <> "GENERALE" then
	select testo
	into   :ls_testo
	from   tab_dw_text_repository
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_lingua = :ls_cod_lingua and
			 nome_dw = :fs_nome_datawindow and
			 nome_oggetto = :ls_oggetto and
			 flag_ambiente = 'C';
else
	select testo
	into   :ls_testo
	from   tab_dw_text_repository
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_lingua is null and
			 nome_dw = :fs_nome_datawindow and
			 nome_oggetto = :ls_oggetto and
			 flag_ambiente ='C';
end if
dw_oggetti_window.setitem(ll_riga, "testo_modificato", ls_testo)
// ----

do while true
	ll_pos = pos(ls_str, "~t", 1)
	
//	if ll_pos < 1 then exit
	if ll_pos = 0 then 
		ls_oggetto = ls_str
	else
		ls_oggetto = left(ls_str, ll_pos - 1)
	end if

	ls_type = dw_1.Describe(ls_oggetto + ".type")
	
	choose case ls_type
		case "text"
		
			if fl_tipo_item = 3 then
				// è un tooltip
				ll_riga = dw_repository_tooltip.insertrow(0)
				dw_repository_tooltip.setitem(ll_riga, "nome_dw", fs_nome_datawindow)
				dw_repository_tooltip.setitem(ll_riga, "nome_oggetto", ls_oggetto)
				dw_repository_tooltip.setitem(ll_riga, "flag_tipo_oggetto", "D")
			else
				// è tutt'altro che un tooltip
				ll_riga = dw_oggetti_window.insertrow(0)
				dw_oggetti_window.setitem(ll_riga, "nome_dw", fs_nome_datawindow)
				dw_oggetti_window.setitem(ll_riga, "nome_oggetto", ls_oggetto)
				dw_oggetti_window.setitem(ll_riga, "testo", dw_1.Describe(ls_oggetto + ".text") )
				dw_oggetti_window.setitem(ll_riga, "flag_tipo_oggetto", "D")
			end if
			
			setnull(ls_testo)
			setnull(ls_tooltip)
			
			if ls_cod_lingua <> "GENERALE" then
				select testo, tooltip
				into   :ls_testo, :ls_tooltip
				from   tab_dw_text_repository
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_lingua = :ls_cod_lingua and
						 nome_dw = :fs_nome_datawindow and
						 nome_oggetto = :ls_oggetto and
						 flag_ambiente = 'C';
			else
				select testo, tooltip
				into   :ls_testo, :ls_tooltip
				from   tab_dw_text_repository
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_lingua is null and
						 nome_dw = :fs_nome_datawindow and
						 nome_oggetto = :ls_oggetto and
						 flag_ambiente ='C';
			end if
		
			if fl_tipo_item = 3 then
				dw_repository_tooltip.setitem(ll_riga, "tooltip", ls_tooltip )
			else
				dw_oggetti_window.setitem(ll_riga, "testo_modificato", ls_testo )
			end if
			
		case "report"
		
			ls_dataobjects = dw_1.Describe(ls_oggetto + ".DataObject")
			dw_2.dataobject = ls_dataobjects
			ls_objects = dw_2.Object.DataWindow.Objects
							
			do while true
				
				ll_pos2 = pos(ls_objects, "~t", 1)
				
				if ll_pos2 < 1 then exit
				
				ls_oggetto = left(ls_objects, ll_pos2 - 1)
		
				ls_type = dw_2.Describe(ls_oggetto + ".type")
	
				if ls_type = "text" then

					if fl_tipo_item = 3 then
						ll_riga = dw_repository_tooltip.insertrow(0)
						dw_repository_tooltip.setitem(ll_riga, "nome_dw", ls_dataobjects)
						dw_repository_tooltip.setitem(ll_riga, "nome_oggetto", ls_oggetto)
						dw_repository_tooltip.setitem(ll_riga, "flag_tipo_oggetto", "D")
					else
						ll_riga = dw_oggetti_window.insertrow(0)
						dw_oggetti_window.setitem(ll_riga, "nome_dw", ls_dataobjects)
						dw_oggetti_window.setitem(ll_riga, "nome_oggetto", ls_oggetto)
						dw_oggetti_window.setitem(ll_riga, "testo", dw_2.Describe(ls_oggetto + ".text") )
						dw_oggetti_window.setitem(ll_riga, "flag_tipo_oggetto", "D")
					end if
					
					setnull(ls_testo)
					setnull(ls_tooltip)
					
					if ls_cod_lingua <> "GENERALE" then
						
						select testo, tooltip
						into   :ls_testo, :ls_tooltip
						from   tab_dw_text_repository
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_lingua = :ls_cod_lingua and
								 nome_dw = :ls_dataobjects and
								 nome_oggetto = :ls_oggetto and
								 flag_ambiente = 'C';
					else
						select testo, tooltip
						into   :ls_testo, :ls_tooltip
						from   tab_dw_text_repository
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_lingua is null and
								 nome_dw = :ls_dataobjects and
								 nome_oggetto = :ls_oggetto and
								 flag_ambiente ='C';
					end if
			
					if isnull(ls_testo) then setnull(ls_testo)
				
					if fl_tipo_item = 3 then
						dw_repository_tooltip.setitem(ll_riga, "tooltip", ls_tooltip )
					else
						dw_oggetti_window.setitem(ll_riga, "testo_modificato", ls_testo )
					end if						
				
				end if
			
				ls_objects = mid(ls_objects, ll_pos2 + 1)
			loop	
			
		case "column"

			ls_tabsequence = dw_1.Describe(ls_oggetto + ".TabSequence")
			// faccio vedere solo le colonne visibili e quelle editabili
			if ls_tabsequence <> '?' and ls_tabsequence <> '0' then
				
				if fl_tipo_item=3 then
					// in caso di tooltip
				
					ll_riga = dw_repository_tooltip.insertrow(0)
					dw_repository_tooltip.setitem(ll_riga, "nome_dw", fs_nome_datawindow)
					dw_repository_tooltip.setitem(ll_riga, "nome_oggetto", ls_oggetto)
					dw_repository_tooltip.setitem(ll_riga, "flag_tipo_oggetto", "D")
					
					setnull(ls_tooltip)
				
					select tooltip
					into   :ls_tooltip
					from   tab_dw_text_repository
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_lingua is null and
							 nome_dw = :fs_nome_datawindow and
							 nome_oggetto = :ls_oggetto and
							 flag_ambiente ='C';
				
					dw_repository_tooltip.setitem(ll_riga, "tooltip", ls_tooltip )
					
				else
				
					ll_riga = dw_imposta_colonne_importanti.insertrow(0)
					dw_imposta_colonne_importanti.setitem(ll_riga, "nome_dw", fs_nome_datawindow)
					dw_imposta_colonne_importanti.setitem(ll_riga, "nome_oggetto", ls_oggetto)
					setnull(ls_flag_importante)
					setnull(ls_flag_obbligatorio)
				
				
					select flag_importante,
							 flag_obbligatorio
					into   :ls_flag_importante,
							 :ls_flag_obbligatorio
					from   tab_dw_text_repository
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_lingua is null and
							 nome_dw = :fs_nome_datawindow and
							 nome_oggetto = :ls_oggetto and
							 flag_ambiente ='C';
				
					if ls_flag_importante = "S" and not isnull(ls_flag_importante) then
						dw_imposta_colonne_importanti.setitem(ll_riga, "flag_importante", "S" )
					end if
					if ls_flag_obbligatorio = "S" and not isnull(ls_flag_obbligatorio) then
						dw_imposta_colonne_importanti.setitem(ll_riga, "flag_obbligatorio", "S" )
					end if
					
				end if
				
			end if
			
	end choose

	ls_str = mid(ls_str, ll_pos + 1)
	
	if len(trim(ls_str)) < 1 or ll_pos = 0 then exit
loop

return
end subroutine

public subroutine wf_elimina ();string ls_cod_lingua, ls_testo, ls_nome_oggetto, ls_nome_dw, ls_nome_dw_old, ls_flag_importante, &
		 ls_flag_obbligatorio, ls_titolo_window, ls_nome_window, ls_testo_window, ls_tooltip

long   ll_i, ll_max, ll_j, ll_progressivo


if il_handle < 1 then return

if g_mb.messagebox("APICE","Elimino tutti i dati di testo / colonne obbligatorie / suggerimenti  della lingua selezionata?",Question!, YesNo!, 2) = 1 then return

ls_cod_lingua = dw_scelta_lingua.getitemstring(1,"cod_lingua")

treeviewitem ltv_item

str_label_window lstr_label_win

tv_label.GetItem (il_handle, ltv_item )

lstr_label_win = ltv_item.data

ls_nome_dw_old = ""

for ll_j = 1 to dw_imposta_colonne_importanti.rowcount()

	ls_nome_dw = dw_imposta_colonne_importanti.getitemstring( ll_j, "nome_dw")
	
	if ls_nome_dw <> ls_nome_dw_old then
	
		if isnull( ls_cod_lingua ) then
	
			delete 	tab_dw_text_repository  
			where 	cod_azienda 	= :s_cs_xx.cod_azienda and
						cod_lingua is null and
						nome_dw = :ls_nome_dw and
			 			flag_ambiente = 'C';
			
		else
		
			delete 	tab_dw_text_repository  
			where 	cod_azienda 	= :s_cs_xx.cod_azienda and
						cod_lingua = :ls_cod_lingua and
						nome_dw = :ls_nome_dw and
			 			flag_ambiente = 'C';
					
		end if
		
		
		ls_nome_dw_old = ls_nome_dw
		
	end if
	
next


commit;


return
end subroutine

public subroutine wf_salva (long al_handle);if isnull(al_handle) or al_handle <= 0 then
	return
end if

dw_imposta_colonne_importanti.accepttext()

dw_oggetti_window.accepttext()

dw_repository_tooltip.accepttext()


if ib_modificato_colonne or ib_modificato_oggetti or ib_modificato_tooltip or ib_modificato_col_din Or (ib_modificato_storico_dw) then
	
	if g_mb.messagebox("FRAMEWORK", "Salvo le modifiche?", Question!, YesNo!, 2) = 1 then
		
		if(al_handle>-1) Then
			String ls_nome_oggetto
			TreeViewItem ltv_item
			str_label_window lstr_label_win
			tv_label.GetItem (al_handle, ltv_item )
			lstr_label_win = ltv_item.data
			ls_nome_oggetto = lstr_label_win.nome
			
			if(ib_modificato_storico_dw) Then
				wf_salva_storico_dw(ls_nome_oggetto)
			end if
		end if

		if ib_modificato_oggetti then wf_salva_oggetti(al_handle)
	
		if ib_modificato_colonne then wf_salva_colonne(al_handle)
	
		if ib_modificato_tooltip then wf_salva_tooltip(al_handle)
		
		if ib_modificato_col_din then wf_salva_col_din(al_handle)

	end if

end if

ib_modificato_colonne 	= false
ib_modificato_oggetti 	= false
ib_modificato_tooltip 		= false
ib_modificato_col_din 	= false
ib_modificato_storico_dw=false

end subroutine

public subroutine wf_salva_colonne (long al_handle);string ls_cod_lingua, ls_testo, ls_nome_oggetto, ls_nome_dw, ls_nome_dw_old, ls_flag_importante, &
		 ls_flag_obbligatorio, ls_titolo_window, ls_nome_window, ls_testo_window, ls_tooltip

long   ll_i, ll_max, ll_j, ll_progressivo


if al_handle < 1 then return

dw_imposta_colonne_importanti.accepttext()

ls_cod_lingua = dw_scelta_lingua.getitemstring(1,"cod_lingua")
if isnull(ls_cod_lingua) then ls_cod_lingua = "GENERALE"

// EnMe 18/08/2008
// gestione delle colonne importanti da inserire visualizzandole con un colore diverso.
ls_nome_dw_old = ""
setnull(ls_cod_lingua)

treeviewitem ltv_item

str_label_window lstr_label_win

tv_label.GetItem (al_handle, ltv_item )

lstr_label_win = ltv_item.data

ls_nome_oggetto = lstr_label_win.nome

// faccio un primo passaggio per impostare tutto al valore N
ls_nome_dw_old = ""

for ll_j = 1 to dw_imposta_colonne_importanti.rowcount()

	ls_nome_dw = dw_imposta_colonne_importanti.getitemstring( ll_j, "nome_dw")
	
	if ls_nome_dw <> ls_nome_dw_old then
	
		update 	tab_dw_text_repository  
		set    	flag_importante 	= 'N',
					flag_obbligatorio = 'N'
		where 	cod_azienda 	= :s_cs_xx.cod_azienda and
					nome_dw = :ls_nome_dw;
		
		
		ls_nome_dw_old = ls_nome_dw
		
	end if
	
next



// poi faccio un secondo passaggio per impostare le colonne importanti

for ll_j = 1 to dw_imposta_colonne_importanti.rowcount()

	ls_nome_dw = dw_imposta_colonne_importanti.getitemstring( ll_j, "nome_dw")
	
	ls_nome_oggetto = dw_imposta_colonne_importanti.getitemstring(ll_j, "nome_oggetto")
	
	ls_flag_importante = dw_imposta_colonne_importanti.getitemstring(ll_j, "flag_importante")
	ls_flag_obbligatorio = dw_imposta_colonne_importanti.getitemstring(ll_j, "flag_obbligatorio")
	
	if not isnull(ls_flag_importante) and ls_flag_importante = "S" then
		
		
		// *** nota: in questo caso la lingua è sempre NULL
		
		select progressivo
		into   :ll_progressivo
		from   tab_dw_text_repository
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_lingua is null and
				 nome_dw = :ls_nome_dw and
				 nome_oggetto = :ls_nome_oggetto;
		
		choose case sqlca.sqlcode
			case 0
				// aggiorno flag_importante o flag_obbligatorio sul record trovato
				
				update 	tab_dw_text_repository  
				set    		flag_importante 	 = :ls_flag_importante,
						 	flag_obbligatorio = :ls_flag_obbligatorio
				where  	cod_azienda 	= :s_cs_xx.cod_azienda and
						 	progressivo   = :ll_progressivo;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in aggiornamento dati in reporitory text.~r~n" + sqlca.sqlerrtext)
					rollback;
					return
				end if
				
			case 100
				// il recordo non esiste; non è stata personalizzata la label, è stato indicato il flag_obbligatorio o flag_importante
			
				ls_nome_oggetto = dw_imposta_colonne_importanti.getitemstring(ll_j, "nome_oggetto")
					
				select max(progressivo)
				into   :ll_max
				from   tab_dw_text_repository  
				where  cod_azienda = :s_cs_xx.cod_azienda;
				
				if isnull(ll_max) then ll_max = 0
		
				ll_max ++
				
				INSERT INTO tab_dw_text_repository  
						( progressivo,   
						  cod_azienda,   
						  cod_lingua,   
						  nome_dw,   
						  nome_oggetto,   
						  testo,
						  flag_importante,
						  flag_obbligatorio,
						  flag_ambiente)  
				VALUES ( :ll_max,   
						  :s_cs_xx.cod_azienda,   
						  null,   
						  :ls_nome_dw,   
						  :ls_nome_oggetto,   
						  :ls_testo_window,
						  :ls_flag_importante,
						  :ls_flag_obbligatorio,
						  'C')  ;
								  
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in inserimento dati in reporitory text.~r~n" + sqlca.sqlerrtext)
					rollback;
					return
				end if
				
			case else
				
		end choose
	end if	
next


dw_imposta_colonne_importanti.reset()

commit;
end subroutine

public subroutine wf_salva_oggetti (long al_handle);string ls_cod_lingua, ls_testo, ls_nome_oggetto, ls_nome_dw, ls_titolo_window, ls_nome_window, ls_testo_window, ls_flag_elimina

long   ll_i, ll_max, ll_j, ll_progressivo


if al_handle < 1 then return

dw_oggetti_window.accepttext()

dw_oggetti_window.SetSort("flag_tipo_oggetto DESC, nome_dw ASC, nome_oggetto ASC")

dw_oggetti_window.Sort()

ls_cod_lingua = dw_scelta_lingua.getitemstring(1,"cod_lingua")

if  len(ls_cod_lingua) < 1 then setnull(ls_cod_lingua)

for ll_j = 1 to dw_oggetti_window.rowcount()

	ls_nome_dw = dw_oggetti_window.getitemstring( ll_j, "nome_dw")
	
	ls_testo = dw_oggetti_window.getitemstring(ll_j, "testo_modificato")
	
	ls_flag_elimina = dw_oggetti_window.getitemstring(ll_j, "flag_elimina")

	if ( not isnull(ls_testo) and len(trim(ls_testo)) > 0 ) or ls_flag_elimina = "S" then
		
		ls_nome_oggetto = dw_oggetti_window.getitemstring(ll_j, "nome_oggetto")
		
		choose case dw_oggetti_window.getitemstring(ll_j, "flag_tipo_oggetto")
				
			case "D"
				
				if ls_flag_elimina = "S" then
			
					if isnull(ls_cod_lingua) then
						
						update	tab_dw_text_repository
						set   	testo = null
						where 	cod_azienda = :s_cs_xx.cod_azienda and
									cod_lingua is null and
									nome_dw = :ls_nome_dw and
									nome_oggetto = :ls_nome_oggetto;
									
					else
						
						update	tab_dw_text_repository
						set		testo = null
						where 	cod_azienda = :s_cs_xx.cod_azienda and
									cod_lingua 	= :ls_cod_lingua and
									nome_dw 	= :ls_nome_dw and
									nome_oggetto = :ls_nome_oggetto;
									
					end if		
					
				else 
				
					ll_progressivo = 0
					
					if isnull(ls_cod_lingua) then
						
						// verifico se esiste il record
						select 	progressivo
						into    	:ll_progressivo
						from 		tab_dw_text_repository
						where 	cod_azienda = :s_cs_xx.cod_azienda and
									cod_lingua is null and
									nome_dw = :ls_nome_dw and
									nome_oggetto = :ls_nome_oggetto;
									
					else
						
						// verifico se esiste il record
						select 	progressivo
						into    	:ll_progressivo
						from 		tab_dw_text_repository
						where 	cod_azienda = :s_cs_xx.cod_azienda and
									cod_lingua 	= :ls_cod_lingua and
									nome_dw 	= :ls_nome_dw and
									nome_oggetto = :ls_nome_oggetto;
									
					end if		
					
					
					
					// record esiste; vado in update
					if ll_progressivo > 0 then
						
						update 	tab_dw_text_repository
						set   		testo = :ls_testo
						where 	cod_azienda = :s_cs_xx.cod_azienda and
									progressivo 	= :ll_progressivo;
						
					else
						
						select max(progressivo)
						into   :ll_max
						from   tab_dw_text_repository  
						where  cod_azienda = :s_cs_xx.cod_azienda;
						
						if ll_max = 0 or isnull(ll_max) then
							ll_max = 1
						else
							ll_max ++
						end if
			
						INSERT INTO tab_dw_text_repository  
								( progressivo,   
								  cod_azienda,   
								  cod_lingua,   
								  nome_dw,   
								  nome_oggetto,   
								  testo,
								  flag_importante,
								  flag_obbligatorio,
								  flag_ambiente)  
						VALUES ( :ll_max,   
								  :s_cs_xx.cod_azienda,   
								  :ls_cod_lingua,   
								  :ls_nome_dw,   
								  :ls_nome_oggetto,   
								  :ls_testo,
								  'N',
								  'N',
								  'C')  ;
	
					end if	  

				end if
				
			case "W"
				
				if ls_flag_elimina = "S" then

					if isnull(ls_cod_lingua) then
						
						delete	tab_w_text_repository
						where 	cod_azienda = :s_cs_xx.cod_azienda and
									cod_lingua  is null and
									nome_window = :ls_nome_dw and
									nome_oggetto = :ls_nome_oggetto and
									flag_ambiente = 'C';
									
					else
						
						delete	tab_w_text_repository
						where 	cod_azienda = :s_cs_xx.cod_azienda and
									cod_lingua = :ls_cod_lingua and
									nome_window = :ls_nome_dw and
									nome_oggetto = :ls_nome_oggetto and
									flag_ambiente = 'C';
	
					end if
				
				else
					
					ll_progressivo = 0
					
					if isnull(ls_cod_lingua) then
						
						// verifico se esiste il record
						select 	progressivo
						into    	:ll_progressivo
						from 		tab_w_text_repository
						where 	cod_azienda = :s_cs_xx.cod_azienda and
									cod_lingua  is null and
									nome_window = :ls_nome_dw and
									nome_oggetto = :ls_nome_oggetto;
									
					else
						
						// verifico se esiste il record
						select 	progressivo
						into    	:ll_progressivo
						from 		tab_w_text_repository
						where 	cod_azienda = :s_cs_xx.cod_azienda and
									cod_lingua = :ls_cod_lingua and
									nome_window = :ls_nome_dw and
									nome_oggetto = :ls_nome_oggetto;
	
					end if
					
					
					// record esiste; vado in update
					if ll_progressivo > 0 then
						
						update 	tab_w_text_repository
						set   	testo = :ls_testo
						where 	cod_azienda = :s_cs_xx.cod_azienda and
									progressivo 	= :ll_progressivo;
						
					else
						
						select max(progressivo)
						into   :ll_max
						from   tab_w_text_repository  
						where  cod_azienda = :s_cs_xx.cod_azienda;
						
						if ll_max = 0 or isnull(ll_max) then
							ll_max = 1
						else
							ll_max ++
						end if
			
						INSERT INTO tab_w_text_repository  
								( progressivo,   
								  cod_azienda,   
								  cod_lingua,   
								  nome_window,   
								  nome_oggetto,   
								  testo,
								  flag_ambiente)  
						VALUES ( :ll_max,   
								  :s_cs_xx.cod_azienda,   
								  :ls_cod_lingua,   
								  :ls_nome_dw,   
								  :ls_nome_oggetto,   
								  :ls_testo,
								  'C')  ;
								  
					end if
					
				end if
				
				
			case else
				
				g_mb.messagebox("Multilingue","Tipo di oggetto non gestito")
				rollback;
				return 
		end choose
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in inserimento dati in reporitory text.~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
	
	end if	
	
next

commit;

end subroutine

public subroutine wf_salva_tooltip (long al_handle);string ls_nome_oggetto, ls_nome_dw, ls_tooltip, ls_flag_elimina, ls_cod_lingua

long   ll_i, ll_max, ll_j, ll_progressivo


if al_handle < 1 then return

dw_repository_tooltip.accepttext()

// memorizzo il dato del tooltip se è stato scritto
ls_cod_lingua = dw_scelta_lingua.getitemstring(1,"cod_lingua")

if isnull(ls_cod_lingua) then

	for ll_j = 1 to dw_repository_tooltip.rowcount()
	
		ls_nome_dw 		= dw_repository_tooltip.getitemstring( ll_j, "nome_dw")
		ls_nome_oggetto 	= dw_repository_tooltip.getitemstring(ll_j, "nome_oggetto")
		ls_tooltip 			= dw_repository_tooltip.getitemstring(ll_j, "tooltip")
		ls_flag_elimina		= dw_repository_tooltip.getitemstring(ll_j, "flag_elimina")
		
		if ( not isnull(ls_tooltip) and len(ls_tooltip) > 0 ) or ls_flag_elimina ="S" then
			
			if ls_flag_elimina = "S" then
			
				update	tab_dw_text_repository
				set		tooltip = null
				where  	cod_azienda = :s_cs_xx.cod_azienda and
							cod_lingua is null and
							nome_dw = :ls_nome_dw and
							nome_oggetto = :ls_nome_oggetto;
			
			else
				
				select progressivo
				into   :ll_progressivo
				from   tab_dw_text_repository
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_lingua is null and
						 nome_dw = :ls_nome_dw and
						 nome_oggetto = :ls_nome_oggetto;
				
				choose case sqlca.sqlcode
					case 0
						// trovato, quindi eseguo update
						ls_nome_oggetto = dw_repository_tooltip.getitemstring(ll_j, "nome_oggetto")
						
						update 	tab_dw_text_repository  
						set    	tooltip = :ls_tooltip
						where 	cod_azienda 	= :s_cs_xx.cod_azienda and
									progressivo   = :ll_progressivo;
								 
						if sqlca.sqlcode <> 0 then
							g_mb.messagebox("APICE","Errore in aggiornamento dati in reporitory text.~r~n" + sqlca.sqlerrtext)
							rollback;
							return
						end if
						
					case 100
						// il recordo non esiste; non è stata personalizzata la label, è stato indicato il flag_obbligatorio o flag_importante
					
						ls_nome_oggetto = dw_repository_tooltip.getitemstring(ll_j, "nome_oggetto")
							
						select max(progressivo)
						into   :ll_max
						from   tab_dw_text_repository  
						where  cod_azienda = :s_cs_xx.cod_azienda;
						
						if isnull(ll_max) then ll_max = 0
				
						ll_max ++
						
						INSERT INTO tab_dw_text_repository  
								( progressivo,   
								  cod_azienda,   
								  cod_lingua,   
								  nome_dw,   
								  nome_oggetto,   
								  testo,
								  flag_importante,
								  flag_obbligatorio,
								  tooltip,
								  flag_ambiente)  
						VALUES ( :ll_max,   
								  :s_cs_xx.cod_azienda,   
								  null,   
								  :ls_nome_dw,   
								  :ls_nome_oggetto,   
								  null,
								  'N',
								  'N',
								  :ls_tooltip,
								  'C')  ;
										  
						if sqlca.sqlcode <> 0 then
							g_mb.messagebox("APICE","Errore in inserimento dati in reporitory text.~r~n" + sqlca.sqlerrtext)
							rollback;
							return
						end if
						
					case else
						
				end choose
				
			end if
				
		end if	
		
	next

end if

commit;

dw_repository_tooltip.reset()
end subroutine

public function integer wf_tv (string fs_library);string ls_librarylist, ls_library[], ls_objects, ls_windowname,ls_title, ls_filtro
long ll_i, ll_pos, ll_ret, ll_handle, ll_handle_padre
window l_window
datastore lds_window
str_label_window lstr_label_win
treeviewitem ltv_item


// Ripulisco il tree
do
	ll_handle = tv_label.finditem(roottreeitem!,0)
	
	if ll_handle > 0 then
		tv_label.deleteitem(ll_handle)
	end if
	
loop while ll_handle <> -1


// inserisco la radice
ltv_item.label = "Personalizzazione Applicazione"
ltv_item.pictureindex = 1
ltv_item.children = true

ll_handle_padre = tv_label.insertitemlast(0,ltv_item)
il_rootitem = ll_handle_padre

// se imposto un nome di finestra, apro solo quella e non tutto
ls_filtro = sle_cerca.text

// leggo la librarylist e ottengo il nome delle librerie.

if isnull(fs_library) then

	ls_librarylist = getlibrarylist ()
	ll_i = 0
	
	do while true
		if len(trim(ls_librarylist)) < 1 then exit
		
		ll_pos = pos(ls_librarylist, ",")
		
		if ll_pos > 0 then
			ll_i ++
			ls_library[ll_i] = mid(ls_librarylist,1,ll_pos - 1)
			ls_librarylist = mid(ls_librarylist, ll_pos +1)
		else
			ll_i ++
			ls_library[ll_i] =  ls_librarylist
			EXIT
		end if
	loop

else
	ls_library[1] = fs_library
	
end if
	
lds_window = CREATE datastore
lds_window.dataobject = "d_imposta_etichette_lingue"

for ll_i = 1 to upperbound(ls_library)
	
	sle_cerca.hide()
	st_1.text = "ATTENDERE ... caricamemnto oggetti in corso"
	st_1.show()
	
	Yield()
	
//	if pos(lower(ls_library[ll_i]), "framework") > 0 then continue
	
	ls_objects = LibraryDirectoryEx(ls_library[ll_i], dirwindow!)
	ll_ret = lds_window.importstring(ls_objects)
	
	if ll_ret < -1  then
		messagebox("ERRORE", ll_ret)
	end if
next

lds_window.setsort("titolo")
lds_window.sort()

for ll_i = 1 to lds_window.rowcount()
	ls_windowname = lds_window.getitemstring(ll_i, "nome_window")
	if lower(ls_windowname) = "w_gaussiana" then continue
	
	if len(ls_filtro) > 0 and ls_windowname <> ls_filtro then continue
	
	st_1.text = "ATTENDERE ... verifica oggetti in corso"
	Yield()
	
	l_window = create using ls_windowname
	ls_title = l_window.title
	lds_window.setitem(ll_i, "titolo", ls_title)
	
	lstr_label_win.nome = lds_window.getitemstring(ll_i, "nome_window")
	lstr_label_win.titolo = lds_window.getitemstring(ll_i, "titolo")
	lstr_label_win.commento = lds_window.getitemstring(ll_i, "commento")
	lstr_label_win.data_modifica = lds_window.getitemstring(ll_i, "data_modifica")
	lstr_label_win.tipo_item = 0
	
	if isnull(lstr_label_win.titolo) then lstr_label_win.titolo = ""
	if isnull(lstr_label_win.commento) then lstr_label_win.commento = ""
	if isnull(lstr_label_win.data_modifica) then lstr_label_win.data_modifica = ""
	
	ltv_item.data = lstr_label_win
	ltv_item.label = lstr_label_win.titolo + " (" + lstr_label_win.nome + ") " + lstr_label_win.data_modifica + " - " + lstr_label_win.commento
	ltv_item.pictureindex = 2
	ltv_item.SelectedPictureIndex = 2
	ltv_item.children = true
	ll_handle = tv_label.insertitemlast(ll_handle_padre, ltv_item)
	
	/**
	 * 0 = window
	 * 1 = label internazionali
	 * 2 = importanti
	 * 3 = tolltip
	 * 4 = colonne dinamiche
	 * 5 = storico datawindow
	 **/
	
	ltv_item.pictureindex = 3
	ltv_item.SelectedPictureIndex = 3
	ltv_item.label = "Importanti e Obbligatori"
	ltv_item.children = false
	lstr_label_win.tipo_item = 2
	ltv_item.data = lstr_label_win
	tv_label.insertitemlast(ll_handle, ltv_item)
	
	ltv_item.pictureindex = 4
	ltv_item.SelectedPictureIndex = 4
	ltv_item.label = "Internazionale"
	ltv_item.children = false
	lstr_label_win.tipo_item = 1
	ltv_item.data = lstr_label_win
	tv_label.insertitemlast(ll_handle, ltv_item)
	
	ltv_item.pictureindex = 5
	ltv_item.SelectedPictureIndex = 5
	ltv_item.label = "Suggerimenti"
	ltv_item.children = false
	lstr_label_win.tipo_item = 3
	ltv_item.data = lstr_label_win
	tv_label.insertitemlast(ll_handle, ltv_item)
	
	ltv_item.pictureindex = 6
	ltv_item.SelectedPictureIndex = 6
	ltv_item.label = "Colonne Dinamiche"
	ltv_item.children = false
	lstr_label_win.tipo_item = 4
	ltv_item.data = lstr_label_win
	tv_label.insertitemlast(ll_handle, ltv_item)
	
//	ltv_item.pictureindex = 6
//	ltv_item.SelectedPictureIndex = 6
//	ltv_item.label = "Cronologia Variazioni DataWindow"
//	ltv_item.children = false
//	lstr_label_win.tipo_item = 5
//	ltv_item.data = lstr_label_win
//	tv_label.insertitemlast(ll_handle, ltv_item)

next

st_1.text = ""

tv_label.ExpandItem ( ll_handle_padre )

sle_cerca.show()
st_1.text = ""
st_1.hide()


return 0
end function

public function integer wf_carica_colonne_dinamiche ();/**
*/

string ls_sql
long ll_i, ll_find
datastore lds_store

for ll_i = 1 to dw_colonne_dinamiche.rowcount()
	dw_colonne_dinamiche.setitem(ll_i, "flag_visualizza", "N")
	dw_colonne_dinamiche.setitem(ll_i, "flag_visualizza_subito", "N")
	dw_colonne_dinamiche.setitem(ll_i, "flag_obbligatorio", "N")
next

ls_sql = "SELECT cod_colonna_dinamica, flag_visualizza_subito, flag_obbligatorio FROM tab_colonne_dinamiche_window WHERE cod_azienda='"+ s_cs_xx.cod_azienda + "' AND "
ls_sql += "nome_window='" + is_nome_window + "' "

for ll_i = 1 to guo_functions.uof_crea_datastore(lds_store, ls_sql)
	
	ll_find = dw_colonne_dinamiche.find("cod_colonna_dinamica='" + lds_store.getitemstring(ll_i, 1) + "'", 0, dw_colonne_dinamiche.rowcount())
	
	if ll_find > 0 then
	
		dw_colonne_dinamiche.setitem(ll_find, "flag_visualizza", "S")
		dw_colonne_dinamiche.setitem(ll_find, "flag_visualizza_subito", lds_store.getitemstring(ll_i, 2))
		dw_colonne_dinamiche.setitem(ll_find, "flag_obbligatorio", lds_store.getitemstring(ll_i, 3))
	
	end if
	
next

dw_colonne_dinamiche.resetupdate()

return 1
end function

public subroutine wf_salva_col_din (long al_handle);string ls_cod_col_din, ls_flag_visualizza_subito, ls_flag_obbligatorio, ls_flag_visualizza
long ll_i, ll_count

dw_colonne_dinamiche.accepttext()

for ll_i = 1 to dw_colonne_dinamiche.rowcount()
	
	ls_cod_col_din = dw_colonne_dinamiche.getitemstring(ll_i, "cod_colonna_dinamica")
	ls_flag_visualizza = dw_colonne_dinamiche.getitemstring(ll_i, "flag_visualizza")
	ls_flag_visualizza_subito = dw_colonne_dinamiche.getitemstring(ll_i, "flag_visualizza_subito")
	ls_flag_obbligatorio = dw_colonne_dinamiche.getitemstring(ll_i, "flag_obbligatorio")
	//is_nome_window
	
	// controllo se è stata salvata nel DB
	select count(*)
	into :ll_count
	from tab_colonne_dinamiche_window
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		nome_window = :is_nome_window and
		cod_colonna_dinamica = :ls_cod_col_din;
	
	// elimino
	if ls_flag_visualizza = "N" then
		
		if ll_count > 0 then
			// esiste, quindi cancello
			delete from tab_colonne_dinamiche_window
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				nome_window = :is_nome_window and
				cod_colonna_dinamica = :ls_cod_col_din;
				
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore durante la cancellazione della colonna dinamica dalla finestra " + is_nome_window, sqlca)
				rollback;
				return
			end if
				
		end if
	else
	// Inserisco
		if  ll_count < 1 then
			
			insert into tab_colonne_dinamiche_window (
				cod_azienda, nome_window, cod_colonna_dinamica, flag_visualizza_subito, flag_obbligatorio
			) values (
				:s_cs_xx.cod_azienda, :is_nome_window, :ls_cod_col_din, :ls_flag_visualizza_subito, :ls_flag_obbligatorio
			);
			
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore durante l'inserimento della colonna dinamica dalla finestra " + is_nome_window, sqlca)
				rollback;
				return
			end if
			
		else
			
			update tab_colonne_dinamiche_window
			set
				flag_visualizza_subito = :ls_flag_visualizza_subito,
				flag_obbligatorio = :ls_flag_obbligatorio
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				nome_window = :is_nome_window and
				cod_colonna_dinamica = :ls_cod_col_din;
				
				
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore durante l'aggiornamento della colonna dinamica dalla finestra " + is_nome_window, sqlca)
				rollback;
				return
			end if
			
		end if
	end if
next

commit;
end subroutine

public subroutine wf_carica_label_dw_tab (tab at_tab);/**
 * stefanop
 * 28/06/2012
 *
 * Controllo il tab alla ricerca degli oggetti al suo interno
 **/
 
int li_i, li_j, li_tabs, li_controls, li_row
userobject l_tabpage
datawindow l_datawindow

li_tabs =  upperbound(at_tab.control)
for li_i= 1 to li_tabs
	
	l_tabpage = at_tab.control[li_i]
	li_controls = upperbound(l_tabpage.control)
	
	/**
	 * PER IL MOMENTO COMMENTO PERCHé NESSUNO USERA STA GESTIONE E NON 
	 * VOGLIO PERDERE TEMPO
	 **/
	/*
	// # Nome del tab
	li_row = dw_oggetti_window.insertrow(0)
	dw_oggetti_window.setitem(li_row, "nome_dw", at_tab.classname())
	dw_oggetti_window.setitem(li_row, "nome_oggetto", l_tabpage.classname())
	dw_oggetti_window.setitem(li_row, "testo", l_tabpage.text)
	dw_oggetti_window.setitem(li_row, "flag_tipo_oggetto", "T")
	*/
	
	for li_j = 1 to li_controls
		choose case l_tabpage.control[li_j].typeof()
				
			case datawindow!
				l_datawindow = l_tabpage.control[li_j]
				if not isnull(l_datawindow.dataobject) and len( trim(l_datawindow.dataobject) ) > 0 then
					wf_carica_label_dw(l_datawindow.dataobject, 1)
				end if
				
			case else
				
		end choose
	next
next
end subroutine

public subroutine wf_carica_colonne_storico (string as_nomefinestra, string as_nomidw[]);integer li_i, li_j
long ll_count
String ls_elencoNomiDw, ls_provvisorio, ls_query, ls_contenuto, ls_errore
DataStore lds_colonne_storiche

//lds_colonne_storiche=create datastore
//
//////dw_storico.setRedraw(false)
//
////Ordino i nomi delle DW in ordine alfabetico (Bubble Sort)
//li_i=UpperBound(as_nomiDw)
//if(li_i=0) Then
//	destroy lds_colonne_storiche
////	//dw_storico.setRedraw(true)
//	return
//end if
//g_str.bubbleSort(as_nomiDw)
//
////Creo una stringa unica con i nomi delle DW
//for li_i=1 To UpperBound(as_nomiDw) - 1
//	ls_elencoNomiDw=ls_elencoNomiDw+"'"+as_nomiDw[li_i]+"', "
//Next
//ls_elencoNomiDw=ls_elencoNomiDw+"'"+as_nomiDw[UpperBound(as_nomiDw)]+"'"
//
//////dw_storico.setSort("nome_dw asc, nome_colonna asc")
//////dw_storico.sort()
//
//ls_query="SELECT * FROM storico_dw_colonne WHERE ((cod_azienda='"+s_cs_xx.cod_azienda+"') AND (cod_utente='"+s_cs_xx.cod_utente+"') AND (nome_finestra='"+as_nomeFinestra+"') AND (nome_dw IN ("+ls_elencoNomiDw+"))) ORDER BY nome_dw ASC, nome_colonna ASC"
//
//ls_contenuto = SQLCA.syntaxFromSQL(ls_query, "", ls_errore)
//if (ls_errore<>"") Then
//	g_mb.messagebox("Cronologia DataWindow","Si è verificato un errore:~n" + ls_errore,stopsign!)
//	destroy lds_colonne_storiche
//	//dw_storico.setRedraw(true)
//	return
//end if
//
//ll_count=guo_functions.uof_crea_datastore(lds_colonne_storiche, ls_query)
//
//if (ll_count<0) then
//	g_mb.messagebox("Cronologia DataWindow","Errore in lettura database." ,stopsign!)
//	destroy lds_colonne_storiche
//	//dw_storico.setRedraw(true)
//	return
//else
//	if(ll_count=0) Then
//		//dw_storico.setRedraw(true)
//		return
//	end if
//end if

//For li_i=1 To lds_colonne_storiche.rowCount()
//	li_j=//dw_storico.Find("(nome_dw='"+lds_colonne_storiche.getItemString(li_i, "nome_dw")+"') AND (nome_colonna='"+lds_colonne_storiche.getItemString(li_i, "nome_colonna")+"')", li_i, //dw_storico.rowCount())
//	if(li_j>0) Then
//		//dw_storico.setitem(li_j, "flag_storico", "S")
//	end if
//Next

/*
 * Lascio questa parte perché era un capolavoro... Tuttavia, si risolve anche con un "find()"...
*/
//li_i=0
//li_j=0
//
//Do While((li_i<=//dw_storico.rowCount()) And (li_j<=lds_colonne_storiche.rowCount()))
//	
//	//Questo if FA SCHIFO!
//	if((li_i=0) And (li_j=0)) Then
//		li_i=1
//		li_j=1
//	end if
//	
//	Do While(//dw_storico.getItemString(li_i, "nome_dw")<>lds_colonne_storiche.getItemString(li_j, "nome_dw"))
//		Do While(//dw_storico.getItemString(li_i, "nome_dw")<lds_colonne_storiche.getItemString(li_j, "nome_dw"))
//			if(li_i>//dw_storico.rowCount()) Then
//				exit;
//			end if
//			li_i++;
//		Loop
//		
//		Do While(//dw_storico.getItemString(li_i, "nome_dw")>lds_colonne_storiche.getItemString(li_j, "nome_dw"))
//			if(li_j>lds_colonne_storiche.rowCount()) Then
//				exit;
//			end if
//			li_j++;
//		Loop
//		
//		if((li_i>//dw_storico.rowCount()) And (li_j>lds_colonne_storiche.rowCount())) Then
//			exit;
//		end if
//	Loop
//	
//	Do While(//dw_storico.getItemString(li_i, "nome_dw")=lds_colonne_storiche.getItemString(li_j, "nome_dw"))
//		if(//dw_storico.getItemString(li_i, "nome_colonna")=lds_colonne_storiche.getItemString(li_j, "nome_colonna")) Then
//			//dw_storico.setitem(li_i, "flag_storico", "S")
//			li_i++
//			li_j++
//		else
//			if(//dw_storico.getItemString(li_i, "nome_colonna")<lds_colonne_storiche.getItemString(li_j, "nome_colonna")) Then
//				if(li_i>//dw_storico.rowCount()) Then
//					exit;
//				end if
//				li_i++;
//			else
//				if(li_j>lds_colonne_storiche.rowCount()) Then
//					exit;
//				end if
//				li_j++;
//			end if
//		end if
//		if(li_i>//dw_storico.rowCount()) Then
//			exit;
//		end if
//	Loop
//	
//Loop

//dw_storico.setRedraw(true)
end subroutine

public subroutine wf_salva_storico_dw (string as_nomefinestra);integer li_i, li_j, li_k
long ll_count
String ls_contenuto, ls_errore, ls_dwAttuale, ls_codUtente, ls_nomeColonna, ls_error
DataStore lds_datiVecchi
transaction lt_sqlcb

//lds_datiVecchi
//lds_datiVecchi=create datastore
//
////Inizializzo la nuova transazione
//if(Not (guo_functions.uof_create_transaction_from(sqlca, lt_sqlcb, ls_error))) Then
//	g_mb.error(ls_error)
//	return
//end if
//
//ll_count=guo_functions.uof_crea_datastore(lds_datiVecchi, "SELECT * FROM storico_dw_colonne WHERE ((cod_azienda='"+s_cs_xx.cod_azienda+"') AND (cod_utente='"+s_cs_xx.cod_utente+"') AND (nome_finestra='"+as_nomeFinestra+"')) ORDER BY nome_dw ASC, nome_colonna ASC")
//
//if (ll_count<0) then
//	g_mb.error("Errore in lettura database.")
//	destroy lds_datiVecchi
//	Disconnect using lt_sqlcb;
//	return
//end if
//
//dw_storico.sort()
//li_i=1
//li_j=1
//
//Do While(li_i<=dw_storico.rowcount( ))
//	
//	ls_dwAttuale=dw_storico.getItemString(li_i, "nome_dw")
//	Do While(dw_storico.getItemString(li_i, "nome_dw")=ls_dwAttuale)
//		Do While(dw_storico.getItemString(li_i, "nome_colonna")=lds_datiVecchi.getItemString(li_j, "nome_colonna"))
//			if(dw_storico.getItemString(li_i, "flag_storico")="N") Then
//				ls_nomeColonna=dw_storico.getItemString(li_i, "nome_colonna")
//				DELETE FROM storico_dw_colonne
//					WHERE ((cod_azienda=:s_cs_xx.cod_azienda) AND (cod_utente=:s_cs_xx.cod_utente) AND (nome_finestra=:as_nomeFinestra) AND (nome_dw=:ls_dwAttuale) AND (nome_colonna=:ls_nomeColonna))
//					USING lt_sqlcb;
//				if(lt_sqlcb.sqlcode<>0) Then
//					g_mb.error("Errore in inserimento dati in reporitory text.~r~n" + lt_sqlcb.sqlerrtext)
//					rollback using lt_sqlcb;
//					Disconnect using lt_sqlcb;
//					return
//				end if
//			end if
//			li_i++;
//			if((li_j<lds_datiVecchi.rowcount( )) And (lds_datiVecchi.getItemString(li_j+1, "nome_dw")=ls_dwAttuale)) Then
//				li_j++;
//			end if
//			if(li_i=dw_storico.rowcount( )) Then
//				exit;
//			end if
//		Loop
//		
//		Do While(dw_storico.getItemString(li_i, "nome_colonna")<>lds_datiVecchi.getItemString(li_j, "nome_colonna"))
//			
//			if((dw_storico.getItemString(li_i, "nome_colonna")<lds_datiVecchi.getItemString(li_j, "nome_colonna")) Or(li_j>=lds_datiVecchi.rowcount( ))) Then
//				if(dw_storico.getItemString(li_i, "flag_storico")="S") Then
//					ls_nomeColonna=dw_storico.getItemString(li_i, "nome_colonna")
//					INSERT INTO storico_dw_colonne(cod_azienda, cod_utente, nome_finestra, nome_dw, nome_colonna)
//						VALUES(:s_cs_xx.cod_azienda, :s_cs_xx.cod_utente, :as_nomeFinestra, :ls_dwAttuale, :ls_nomeColonna)
//						USING lt_sqlcb;
//						
//					if(lt_sqlcb.sqlcode<>0) Then
//						g_mb.error("Errore in inserimento dati in reporitory text.~r~n" + lt_sqlcb.sqlerrtext)
//						rollback using lt_sqlcb;
//						Disconnect using lt_sqlcb;
//						return
//					end if
//					
//				end if
//				li_i++
//			else
//				if(li_j<lds_datiVecchi.rowcount( )) Then
//					if(lds_datiVecchi.getItemString(li_j+1, "nome_dw")=ls_dwAttuale) Then
//						li_j++
//					else
//						li_i++
//					end if
//				else
//					li_i++
//				end if
//			end if
//			
//			if((li_i>dw_storico.rowcount( )) Or (dw_storico.getItemString(li_i, "nome_dw")<>ls_dwAttuale)) Then
//				Exit;
//			end if
//		Loop
//	Loop
//Loop
//
ib_modificato_storico_dw=false
end subroutine

public subroutine wf_carica_dw_storico (string as_nomefinestra, ref datawindow dws_appoggio);integer li_i
long ll_riga
String ls_descrizioneColonna

//for li_i=1 To Integer(dws_appoggio.Object.DataWindow.Column.Count)
//	ll_riga = dw_storico.insertrow(0)
//	dw_storico.setItem(ll_riga, "cod_azienda", s_cs_xx.cod_azienda)
//	dw_storico.setitem(ll_riga, "cod_utente", s_cs_xx.cod_utente)
//	dw_storico.setitem(ll_riga, "nome_finestra",as_nomeFinestra)
//	dw_storico.setitem(ll_riga, "nome_dw", dws_appoggio.ClassName())
//	dw_storico.setitem(ll_riga, "nome_colonna", dws_appoggio.Describe("#"+String(li_i)+".Name"))
//	ls_descrizioneColonna=dw_1.Describe(dws_appoggio.Describe("#"+String(li_i)+".Name")+"_t.Text")
//	if(ls_descrizioneColonna="!") Then
//		dw_storico.setitem(ll_riga, "descrizione_colonna", "")
//	else
//		dw_storico.setitem(ll_riga, "descrizione_colonna", ls_descrizioneColonna)
//	end if
//	dw_storico.setitem(ll_riga, "flag_storico", "")
//Next
//
//
end subroutine

on w_imposta_etichette_lingue.create
this.dw_colonne_dinamiche=create dw_colonne_dinamiche
this.pb_trova=create pb_trova
this.dw_scelta_lingua=create dw_scelta_lingua
this.tv_label=create tv_label
this.cb_3=create cb_3
this.cb_2=create cb_2
this.dw_2=create dw_2
this.cb_cancella=create cb_cancella
this.dw_1=create dw_1
this.dw_oggetti_window=create dw_oggetti_window
this.dw_imposta_colonne_importanti=create dw_imposta_colonne_importanti
this.dw_repository_tooltip=create dw_repository_tooltip
this.sle_cerca=create sle_cerca
this.st_1=create st_1
this.Control[]={this.dw_colonne_dinamiche,&
this.pb_trova,&
this.dw_scelta_lingua,&
this.tv_label,&
this.cb_3,&
this.cb_2,&
this.dw_2,&
this.cb_cancella,&
this.dw_1,&
this.dw_oggetti_window,&
this.dw_imposta_colonne_importanti,&
this.dw_repository_tooltip,&
this.sle_cerca,&
this.st_1}
end on

on w_imposta_etichette_lingue.destroy
destroy(this.dw_colonne_dinamiche)
destroy(this.pb_trova)
destroy(this.dw_scelta_lingua)
destroy(this.tv_label)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.dw_2)
destroy(this.cb_cancella)
destroy(this.dw_1)
destroy(this.dw_oggetti_window)
destroy(this.dw_imposta_colonne_importanti)
destroy(this.dw_repository_tooltip)
destroy(this.sle_cerca)
destroy(this.st_1)
end on

event open;String ls_error

// OGGETI; posiziono per resize
pb_trova.move(20,20)
sle_cerca.move(pb_trova.x + pb_trova.width + 40, 20)
tv_label.move(20, pb_trova.y + pb_trova.height + 40)
dw_scelta_lingua.move(sle_cerca.x + sle_cerca.width + 40, 20)
cb_cancella.y = 20
dw_oggetti_window.y = tv_label.y
dw_imposta_colonne_importanti.y = tv_label.y
dw_repository_tooltip.y = tv_label.y
dw_colonne_dinamiche.y = tv_label.y
//dw_storico.y = tv_label.y
// ---

dw_scelta_lingua.insertrow(0)

f_po_loaddddw_dw(dw_scelta_lingua, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


pb_trova.picturename = s_cs_xx.volume + s_cs_xx.risorse + "trova.bmp"

tv_label.addpicture( s_cs_xx.volume + s_cs_xx.risorse + "11.5\internaz_inizio.png")
tv_label.addpicture( s_cs_xx.volume + s_cs_xx.risorse + "11.5\internaz_window.png")
tv_label.addpicture( s_cs_xx.volume + s_cs_xx.risorse + "11.5\internaz_importante.png")
tv_label.addpicture( s_cs_xx.volume + s_cs_xx.risorse + "11.5\internaz_Oggetti.png")
tv_label.addpicture( s_cs_xx.volume + s_cs_xx.risorse + "11.5\internaz_tooltip.png")
tv_label.addpicture( s_cs_xx.volume + s_cs_xx.risorse + "11.5\internaz_col_din.png")

dw_colonne_dinamiche.settransobject(sqlca)
dw_colonne_dinamiche.retrieve(s_cs_xx.cod_azienda)

yield()

postevent("ue_carica_tree")
end event

event closequery;wf_salva(il_handle)

end event

event resize;integer li_i
long ll_dw_width

tv_label.height = newheight - tv_label.y - 20

ll_dw_width = newwidth - dw_colonne_dinamiche.x - 20

dw_oggetti_window.resize(ll_dw_width, tv_label.height)
dw_imposta_colonne_importanti.resize(ll_dw_width, tv_label.height)
dw_repository_tooltip.resize(ll_dw_width, tv_label.height)
dw_colonne_dinamiche.resize(ll_dw_width, tv_label.height)
//dw_storico.resize(ll_dw_width, tv_label.height)

cb_cancella.x = newwidth - cb_cancella.width - 20
end event

type dw_colonne_dinamiche from datawindow within w_imposta_etichette_lingue
boolean visible = false
integer x = 1682
integer y = 136
integer width = 3017
integer height = 2220
integer taborder = 110
string title = "none"
string dataobject = "d_imposta_colonne_dinamiche"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;ib_modificato_col_din = true

choose case dwo.name
		
	case "flag_visualizza"
		if data = "N" then
			setitem(row, "flag_visualizza_subito", "N")
			setitem(row, "flag_obbligatorio", "N")
		end if
		
	case "flag_visualizza_subito"
		if data = "S" then setitem(row, "flag_visualizza", "S")
		
end choose
end event

type pb_trova from picturebutton within w_imposta_etichette_lingue
integer x = 14
integer y = 8
integer width = 137
integer height = 120
integer taborder = 110
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
boolean flatstyle = true
vtextalign vtextalign = vcenter!
end type

event clicked;string ls_ricerca


ls_ricerca = sle_cerca.text

if isnull(ls_ricerca) or trim(ls_ricerca) = "" then
	g_mb.messagebox("Personalizzazione Applicazione","Immettere un parametro per la ricerca",exclamation!)
	return -1
end if

if il_handle <= 0 or isnull(il_handle) then
	// trovo la radice
	il_handle = tv_label.finditem(roottreeitem!,0)
	
	// mi posiziono sul primo ramo di livello 1
	il_handle = tv_label.finditem(childtreeitem!,il_handle)
	
else
	
	il_handle = tv_label.finditem(nexttreeitem!,il_handle)
	
end if

//tv_label.setfocus()

tv_label.selectitem(il_handle)

if il_handle > 0 then
	if wf_trova(ls_ricerca,il_handle) = 100 then
		g_mb.messagebox("Menu Principale","Elemento non trovato",information!)
		tv_label.setfocus()
	end if
end if
end event

type dw_scelta_lingua from datawindow within w_imposta_etichette_lingue
integer x = 1682
integer y = 24
integer width = 1915
integer height = 92
integer taborder = 60
string title = "none"
string dataobject = "d_imposta_etichette_lingue_dddw"
boolean border = false
boolean livescroll = true
end type

event itemchanged;tv_label.postevent("ue_carica_oggetti")
end event

type tv_label from treeview within w_imposta_etichette_lingue
event ue_carica_oggetti ( )
integer x = 14
integer y = 136
integer width = 1637
integer height = 2220
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean hasbuttons = false
boolean linesatroot = true
boolean hideselection = false
boolean singleexpand = true
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event ue_carica_oggetti();if il_handle > 0 and not isnull(il_handle) and il_handle <> il_rootitem then
	
	wf_carica_oggetti_window (il_handle )
	
end if
end event

event clicked;//if handle <> il_rootitem then
//	
//	wf_salva( il_handle )
//
//	if isnull(handle) or handle <= 0 then
//		return 0
//	end if
//	
//	
//	str_label_window lstr_label_win
//	
//	treeviewitem ltv_item
//	
//	getitem( handle, ltv_item )
//	
//	try
//		lstr_label_win = ltv_item.data	
//	catch  ( PBXRuntimeError re )
//		return
//	finally
//	
//	end try
//	// 0=window 1=label  2=importanti  3=tolltip
//	
//	choose case lstr_label_win.tipo_item
//		case 1
//			dw_oggetti_window.show()
//			dw_imposta_colonne_importanti.hide()
//			dw_repository_tooltip.hide()
//			
//		case 2
//			dw_oggetti_window.hide()
//			dw_imposta_colonne_importanti.show()
//			dw_repository_tooltip.hide()
//			
//		case 3
//			dw_oggetti_window.hide()
//			dw_imposta_colonne_importanti.hide()
//			dw_repository_tooltip.show()
//			
//	end choose
//	
//	if lstr_label_win.tipo_item > 0 then
//		wf_carica_oggetti_window ( handle )
//	end if
//	
//end if
//
//
end event

event selectionchanged;str_label_window lstr_label_win
treeviewitem ltv_item

if newhandle < 0 or newhandle = il_rootitem then return 0

wf_salva( il_handle )

il_handle = newhandle

getitem(newhandle, ltv_item )

try
	lstr_label_win = ltv_item.data	
catch  ( PBXRuntimeError re )
	return 0
end try

// 0=window 1=label  2=importanti  3=tolltip 4=colonne dinamiche
is_nome_window = lstr_label_win.nome

choose case lstr_label_win.tipo_item
	case 1 // label
		dw_oggetti_window.show()
		dw_imposta_colonne_importanti.hide()
		dw_repository_tooltip.hide()
		dw_colonne_dinamiche.hide()
		//dw_storico.hide()
		
	case 2 // importanti
		dw_oggetti_window.hide()
		dw_imposta_colonne_importanti.show()
		dw_repository_tooltip.hide()
		dw_colonne_dinamiche.hide()
		//dw_storico.hide()
		
	case 3 // tooltip
		dw_oggetti_window.hide()
		dw_imposta_colonne_importanti.hide()
		dw_repository_tooltip.show()
		dw_colonne_dinamiche.hide()
		//dw_storico.hide()
		
	case 4 // colonne dinamiche
		dw_oggetti_window.hide()
		dw_imposta_colonne_importanti.hide()
		dw_repository_tooltip.hide()
		dw_colonne_dinamiche.show()
		//dw_storico.hide()
		wf_carica_colonne_dinamiche()
		return 0
		
	case 5 //Storico datawindow
		dw_oggetti_window.hide()
		dw_imposta_colonne_importanti.hide()
		dw_repository_tooltip.hide()
		dw_colonne_dinamiche.hide()
		//dw_storico.show()
		
end choose

if lstr_label_win.tipo_item > 0 then
	wf_carica_oggetti_window ( newhandle )
end if

//end if


end event

type cb_3 from commandbutton within w_imposta_etichette_lingue
boolean visible = false
integer x = 1806
integer y = 1560
integer width = 402
integer height = 112
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "EXCEL"
end type

event clicked;//tab_1.tab_lista_window.dw_3.saveas("C:\picopallino.xls", excel!,true)
end event

type cb_2 from commandbutton within w_imposta_etichette_lingue
boolean visible = false
integer x = 2491
integer y = 1580
integer width = 389
integer height = 80
integer taborder = 70
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
end type

event clicked;//string ls_classname, ls_ancestorname
//long ll_i
//classdefinition	lcld_class, lcld_ancestor
//window l_window
//
//for ll_i = 1 to tab_1.tab_lista_window.dw_3.rowcount()
//	ls_classname = tab_1.tab_lista_window.dw_3.getitemstring(ll_i,"nome_window")
//	
//	l_window = create using ls_classname
//	lcld_class = l_window.classdefinition
//	
//	lcld_ancestor = lcld_class.ancestor
//	
//	if lower(lcld_ancestor.name) = "window" then
//		tab_1.tab_lista_window.dw_3.setitem(ll_i,"nome_window","___" + ls_classname)
//	end if
//	
//	destroy l_window
//next
end event

type dw_2 from datawindow within w_imposta_etichette_lingue
boolean visible = false
integer x = 1349
integer y = 1660
integer width = 457
integer height = 148
integer taborder = 20
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_cancella from commandbutton within w_imposta_etichette_lingue
integer x = 4320
integer y = 24
integer width = 366
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina"
end type

event clicked;string ls_cod_lingua, ls_testo, ls_nome_oggetto, ls_nome_dw, ls_nome_dw_old, ls_flag_importante, &
		 ls_flag_obbligatorio, ls_titolo_window, ls_nome_window, ls_testo_window, ls_tooltip

long   ll_i, ll_max, ll_j, ll_progressivo


ls_cod_lingua = dw_scelta_lingua.getitemstring(1,"cod_lingua")
if isnull(ls_cod_lingua) then ls_cod_lingua = "GENERALE"

if g_mb.messagebox("FRAMEWORK","Tutti i dati della gestione selezionata (Oggetti, Obbligatori e Suggerimenti) di tutte le lingue saranno CANCELLATI: PROSEGUO?",Question!, YesNo!,2) = 2 then return

ls_nome_dw_old = ""

for ll_j = 1 to dw_oggetti_window.rowcount()
	
	choose case dw_oggetti_window.getitemstring(ll_j, "flag_tipo_oggetto")
			
		case "D"
			ls_nome_dw = dw_oggetti_window.getitemstring( ll_j, "nome_dw")
			
			if ls_nome_dw <> ls_nome_dw_old then
			
				delete 	tab_dw_text_repository  
				where 	cod_azienda 	= :s_cs_xx.cod_azienda and
							nome_dw = :ls_nome_dw;
				
				ls_nome_dw_old = ls_nome_dw
				
			end if
	
		case "W"
			
			ls_nome_dw = dw_oggetti_window.getitemstring( ll_j, "nome_dw")
			
			if ls_nome_dw <> ls_nome_dw_old then
			
				delete 	tab_w_text_repository  
				where 	cod_azienda 	= :s_cs_xx.cod_azienda and
							nome_window = :ls_nome_dw;
				
				ls_nome_dw_old = ls_nome_dw
				
			end if

end choose

next

commit;

tv_label.postevent("ue_carica_oggetti")

end event

type dw_1 from datawindow within w_imposta_etichette_lingue
boolean visible = false
integer x = 2789
integer y = 1640
integer width = 457
integer height = 148
integer taborder = 10
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_oggetti_window from datawindow within w_imposta_etichette_lingue
integer x = 1682
integer y = 136
integer width = 3017
integer height = 2220
integer taborder = 80
string title = "none"
string dataobject = "d_modifica_text_grid"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;if isvalid(dwo) then
	if dwo.name = "testo_modificato" then
		
		if isnull(data) or len(trim(data)) < 1 then
			setitem(row, "flag_elimina", "S")
		else
			setitem(row, "flag_elimina", "N")
		end if
			
	end if
end if

ib_modificato_oggetti = true
end event

event doubleclicked;if dwo.name='testo_modificato' then
	openwithparm(w_imposta_etichette_lingue_text, getitemstring(row,"testo_modificato"))
	if message.stringparm <> "Cancel!" then
		setitem(row, dwo.name, message.stringparm)
	end if
end if
end event

type dw_imposta_colonne_importanti from datawindow within w_imposta_etichette_lingue
integer x = 1682
integer y = 136
integer width = 3017
integer height = 2220
integer taborder = 90
string title = "none"
string dataobject = "d_imposta_colonne_importanti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;if isvalid(dwo) then
	if dwo.name = "flag_obbligatorio" then
			
			setitem(row, "flag_importante", data)
			
	end if
end if

ib_modificato_colonne = true
end event

type dw_repository_tooltip from datawindow within w_imposta_etichette_lingue
integer x = 1682
integer y = 136
integer width = 3017
integer height = 2220
integer taborder = 120
string title = "none"
string dataobject = "d_repository_tooltip"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;if isvalid(dwo) then
	if dwo.name = "tooltip" then
		
		if isnull(data) or len(trim(data)) < 1 then
			setitem(row, "flag_elimina", "S")
		else
			setitem(row, "flag_elimina", "N")
		end if
			
	end if
end if

ib_modificato_tooltip = true
end event

event doubleclicked;if isvalid(dwo) then
	if dwo.name = "tooltip" then
		

		s_cs_xx.parametri.parametro_i_1 = 255
		s_cs_xx.parametri.parametro_s_1 = gettext()
		
		window_open(w_ext_note_mss,0)
		
		
		if not isnull(s_cs_xx.parametri.parametro_s_1) then
			
			settext( s_cs_xx.parametri.parametro_s_1 )
		
		end if
		
		setnull(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_i_1 = 0
			
	end if
end if

end event

type sle_cerca from singlelineedit within w_imposta_etichette_lingue
event ue_seleziona ( )
event ue_key pbm_keydown
integer x = 165
integer y = 28
integer width = 1486
integer height = 88
integer taborder = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

event ue_seleziona();selecttext(1,len(text))
end event

event ue_key;if key = keyenter! then
	pb_trova.postevent("clicked")
end if
end event

event getfocus;postevent("ue_seleziona")
end event

type st_1 from statictext within w_imposta_etichette_lingue
integer x = 160
integer y = 28
integer width = 1371
integer height = 88
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

