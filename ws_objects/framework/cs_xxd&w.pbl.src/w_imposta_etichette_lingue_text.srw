﻿$PBExportHeader$w_imposta_etichette_lingue_text.srw
forward
global type w_imposta_etichette_lingue_text from window
end type
type cb_cancel from commandbutton within w_imposta_etichette_lingue_text
end type
type cb_ok from commandbutton within w_imposta_etichette_lingue_text
end type
type dw_importa_etichette_lingue_text from datawindow within w_imposta_etichette_lingue_text
end type
end forward

global type w_imposta_etichette_lingue_text from window
integer width = 1947
integer height = 800
boolean titlebar = true
string title = "Testo Personalizzato"
windowtype windowtype = response!
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_cancel cb_cancel
cb_ok cb_ok
dw_importa_etichette_lingue_text dw_importa_etichette_lingue_text
end type
global w_imposta_etichette_lingue_text w_imposta_etichette_lingue_text

event open;dw_importa_etichette_lingue_text.insertrow(0)
dw_importa_etichette_lingue_text.setitem(1, "testo", message.StringParm)


end event

on w_imposta_etichette_lingue_text.create
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
this.dw_importa_etichette_lingue_text=create dw_importa_etichette_lingue_text
this.Control[]={this.cb_cancel,&
this.cb_ok,&
this.dw_importa_etichette_lingue_text}
end on

on w_imposta_etichette_lingue_text.destroy
destroy(this.cb_cancel)
destroy(this.cb_ok)
destroy(this.dw_importa_etichette_lingue_text)
end on

type cb_cancel from commandbutton within w_imposta_etichette_lingue_text
integer x = 1097
integer y = 600
integer width = 389
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Annulla"
end type

event clicked;CloseWithReturn ( parent, "Cancel!" )
end event

type cb_ok from commandbutton within w_imposta_etichette_lingue_text
integer x = 1509
integer y = 600
integer width = 389
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "OK"
end type

event clicked;dw_importa_etichette_lingue_text.accepttext()

CloseWithReturn ( parent, dw_importa_etichette_lingue_text.getitemstring(1,"testo") )
end event

type dw_importa_etichette_lingue_text from datawindow within w_imposta_etichette_lingue_text
integer width = 1920
integer height = 600
integer taborder = 10
string title = "none"
string dataobject = "d_importa_etichette_lingue_text"
boolean border = false
boolean livescroll = true
end type

