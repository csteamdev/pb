﻿$PBExportHeader$w_imposta_pwd_db.srw
$PBExportComments$Window impostazione password
forward
global type w_imposta_pwd_db from window
end type
type st_6 from statictext within w_imposta_pwd_db
end type
type cb_2 from commandbutton within w_imposta_pwd_db
end type
type cb_1 from commandbutton within w_imposta_pwd_db
end type
type sle_seriale_attivazione from singlelineedit within w_imposta_pwd_db
end type
type sle_seriale from singlelineedit within w_imposta_pwd_db
end type
type st_5 from statictext within w_imposta_pwd_db
end type
type st_4 from statictext within w_imposta_pwd_db
end type
type st_3 from statictext within w_imposta_pwd_db
end type
type sle_conferma_pwd from singlelineedit within w_imposta_pwd_db
end type
type sle_pwd from singlelineedit within w_imposta_pwd_db
end type
type cb_annulla from commandbutton within w_imposta_pwd_db
end type
type cb_ok from commandbutton within w_imposta_pwd_db
end type
type st_2 from statictext within w_imposta_pwd_db
end type
type st_1 from statictext within w_imposta_pwd_db
end type
end forward

global type w_imposta_pwd_db from window
integer width = 1765
integer height = 732
boolean titlebar = true
string title = "Impostazione Password"
windowtype windowtype = response!
long backcolor = 12632256
boolean center = true
event ue_keydown pbm_keydown
st_6 st_6
cb_2 cb_2
cb_1 cb_1
sle_seriale_attivazione sle_seriale_attivazione
sle_seriale sle_seriale
st_5 st_5
st_4 st_4
st_3 st_3
sle_conferma_pwd sle_conferma_pwd
sle_pwd sle_pwd
cb_annulla cb_annulla
cb_ok cb_ok
st_2 st_2
st_1 st_1
end type
global w_imposta_pwd_db w_imposta_pwd_db

type variables
n_cst_crypto iuo_crypto
boolean ib_trial = false
end variables

forward prototypes
public subroutine wf_change_state ()
end prototypes

event ue_keydown;// *** stefanop 14/07/2008: Per saltare la validazione premere CTRL+SHIFT+P e nel seriale attivazione deve essere scritto CS
if key = KeyP! and keyflags = 3 and sle_seriale_attivazione.text = "CS" then	
	
	wf_change_state()
	
end if
end event

public subroutine wf_change_state ();title = "Impostazione Password"

// nascondo i controlli di attivazione
st_3.visible = false
st_4.visible = false
st_5.visible = false
st_6.visible = false
sle_seriale.visible = false
sle_seriale_attivazione.visible = false
cb_1.visible = false
cb_2.visible = false

// mostro i controlli per la nuova password
st_1.visible = true
st_2.visible = true
sle_pwd.visible = true
sle_conferma_pwd.visible = true
cb_ok.visible = true
cb_annulla.visible = true
end subroutine

on w_imposta_pwd_db.create
this.st_6=create st_6
this.cb_2=create cb_2
this.cb_1=create cb_1
this.sle_seriale_attivazione=create sle_seriale_attivazione
this.sle_seriale=create sle_seriale
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.sle_conferma_pwd=create sle_conferma_pwd
this.sle_pwd=create sle_pwd
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.st_2=create st_2
this.st_1=create st_1
this.Control[]={this.st_6,&
this.cb_2,&
this.cb_1,&
this.sle_seriale_attivazione,&
this.sle_seriale,&
this.st_5,&
this.st_4,&
this.st_3,&
this.sle_conferma_pwd,&
this.sle_pwd,&
this.cb_annulla,&
this.cb_ok,&
this.st_2,&
this.st_1}
end on

on w_imposta_pwd_db.destroy
destroy(this.st_6)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.sle_seriale_attivazione)
destroy(this.sle_seriale)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.sle_conferma_pwd)
destroy(this.sle_pwd)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.st_2)
destroy(this.st_1)
end on

event open;iuo_crypto = create n_cst_crypto

title = "Attivazione"

sle_seriale.text = string(rand(9)) + string(rand(9)) + string(rand(9)) + string(rand(9)) + string(rand(9)) + string(rand(9))

// Solo per test!!!!!
//sle_seriale_attivazione.text = iuo_crypto.encryptserial(sle_seriale.text)
end event

type st_6 from statictext within w_imposta_pwd_db
integer x = 46
integer y = 80
integer width = 1669
integer height = 140
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Attenzione: per poter attivare il prodotto è necessario inserire un seriale valido e disporre dei diritti di Amministratore."
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_imposta_pwd_db
integer x = 46
integer y = 500
integer width = 229
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;closewithReturn(parent, -1)
end event

type cb_1 from commandbutton within w_imposta_pwd_db
integer x = 1303
integer y = 500
integer width = 402
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Attivazione"
boolean default = true
end type

event clicked;

if iuo_crypto.decryptserial(sle_seriale.text, sle_seriale_attivazione.text) then
	
	wf_change_state()
	
else 
	
	messagebox('Attivazione', 'ATTENZIONE: seriale di attivazione errato!', StopSign!)
	
end if
end event

type sle_seriale_attivazione from singlelineedit within w_imposta_pwd_db
integer x = 549
integer y = 380
integer width = 1166
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
end type

type sle_seriale from singlelineedit within w_imposta_pwd_db
integer x = 549
integer y = 280
integer width = 457
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
boolean displayonly = true
end type

type st_5 from statictext within w_imposta_pwd_db
integer x = 46
integer y = 380
integer width = 475
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seriale Attivazione:"
boolean focusrectangle = false
end type

type st_4 from statictext within w_imposta_pwd_db
integer x = 46
integer y = 300
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seriale:"
boolean focusrectangle = false
end type

type st_3 from statictext within w_imposta_pwd_db
integer x = 46
integer y = 20
integer width = 800
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "ATTIVAZIONE DEL PRODOTTO"
boolean focusrectangle = false
end type

type sle_conferma_pwd from singlelineedit within w_imposta_pwd_db
boolean visible = false
integer x = 640
integer y = 240
integer width = 1074
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
boolean password = true
integer limit = 15
end type

type sle_pwd from singlelineedit within w_imposta_pwd_db
boolean visible = false
integer x = 640
integer y = 140
integer width = 1074
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
boolean password = true
integer limit = 15
end type

type cb_annulla from commandbutton within w_imposta_pwd_db
boolean visible = false
integer x = 1349
integer y = 460
integer width = 366
integer height = 100
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;closeWithReturn(parent, -1)
end event

type cb_ok from commandbutton within w_imposta_pwd_db
boolean visible = false
integer x = 937
integer y = 460
integer width = 366
integer height = 100
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;string ls_computer_name
n_cst_crypto lnv_crypt

//*** stefanop 20/06/08 - Aggiunto controllo che le password devono essere di almeno 1 carattere
if len(trim(sle_pwd.text))<1 and len(trim(sle_conferma_pwd.text))<1 then
	g_mb.messagebox("APICE","Le password non possono essere vuote.",stopsign!)
	return
end if

if sle_pwd.text <> sle_conferma_pwd.text then
	g_mb.messagebox("APICE","La nuova password non è stata confermata correttamente. Assicurarsi di confermare esattamente la nuova password.",stopsign!)
	return
end if

lnv_crypt = CREATE n_cst_crypto
ls_computer_name = guo_functions.uof_get_computer_name( )
if lnv_crypt.EncryptData(	 ls_computer_name + sle_conferma_pwd.text,s_cs_xx.parametri.parametro_s_1) < 0 then
	g_mb.messagebox("APICE","La nuova password contiene caratteri non consentiti.~nI caratteri consentiti comprendono:~n~n" + &
					"- tutte le cifre numeriche 0,1,2,...,9~n- tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z~n" + &
					"- alcuni simboli tra cui ! # $ % & ( ) * + , - . / : ; < > = ? @ [ ] \ _ | { }~n~nModificare la password e riprovare",stopsign!)
	return
end if

DESTROY lnv_crypt

s_cs_xx.parametri.parametro_b_1 = true

Close(parent)
end event

type st_2 from statictext within w_imposta_pwd_db
boolean visible = false
integer x = 46
integer y = 240
integer width = 571
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Conferma password:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_imposta_pwd_db
boolean visible = false
integer x = 46
integer y = 140
integer width = 571
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Password DB:"
boolean focusrectangle = false
end type

