﻿$PBExportHeader$w_imposta_report.srw
$PBExportComments$Window imposta report
forward
global type w_imposta_report from w_cs_xx_principale
end type
type dw_appoggio from datawindow within w_imposta_report
end type
type ddlb_tipo from dropdownlistbox within w_imposta_report
end type
type tab_1 from tab within w_imposta_report
end type
type tp_1 from userobject within tab_1
end type
type lb_elenco_selezionato from picturelistbox within tp_1
end type
type lb_elenco_totale from picturelistbox within tp_1
end type
type st_2 from statictext within tp_1
end type
type st_1 from statictext within tp_1
end type
type cb_togli_tutti from commandbutton within tp_1
end type
type cb_togli_uno from commandbutton within tp_1
end type
type cb_aggiungi_tutti from commandbutton within tp_1
end type
type cb_aggiungi_uno from commandbutton within tp_1
end type
type lb_1 from listbox within tp_1
end type
type tp_1 from userobject within tab_1
lb_elenco_selezionato lb_elenco_selezionato
lb_elenco_totale lb_elenco_totale
st_2 st_2
st_1 st_1
cb_togli_tutti cb_togli_tutti
cb_togli_uno cb_togli_uno
cb_aggiungi_tutti cb_aggiungi_tutti
cb_aggiungi_uno cb_aggiungi_uno
lb_1 lb_1
end type
type tp_2 from userobject within tab_1
end type
type dw_imposta_report from datawindow within tp_2
end type
type tp_2 from userobject within tab_1
dw_imposta_report dw_imposta_report
end type
type tab_1 from tab within w_imposta_report
tp_1 tp_1
tp_2 tp_2
end type
type cb_report from commandbutton within w_imposta_report
end type
end forward

global type w_imposta_report from w_cs_xx_principale
integer width = 3259
integer height = 1396
string title = "Imposta Selezione Report"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
boolean center = true
dw_appoggio dw_appoggio
ddlb_tipo ddlb_tipo
tab_1 tab_1
cb_report cb_report
end type
global w_imposta_report w_imposta_report

type variables
string is_tabella
boolean ib_cod_azienda
string is_col_type[]

private:
	string is_mode
	string is_select_columns
	string is_pks[]
end variables

forward prototypes
public function string wf_repl_underscore_with_space (string fs_str_orig)
public function string wf_repl_space_with_underscore (string fs_str_orig)
public function string wf_formatta (string fs_campo, string fs_confronto)
protected function integer wf_imposta_dw (string fs_nome_tabella, ref string fs_errore)
private function integer wf_imposta_pk (string as_nome_tabella, ref string as_errore)
public function boolean wf_is_primary_key (string as_column)
public subroutine wf_move_right (boolean ab_single_element)
public subroutine wf_move_left (boolean ab_single_element)
end prototypes

public function string wf_repl_underscore_with_space (string fs_str_orig);// string Function wf_replace_underscores_with_spaces (string fs_str_orig)

// Returns value of fs_str_orig with each underscore replaced by a space character

int		p
string	s_in, s_out

s_in = fs_str_orig
p = Pos ( s_in, '_' )

do while p > 0
	s_out =	s_out + Left ( s_in, p -1 ) + ' '
	s_in   = Mid ( s_in, p +1)	
	p = Pos (s_in, '_' )
loop

s_out = s_out + s_in

return  s_out
end function

public function string wf_repl_space_with_underscore (string fs_str_orig);// string Function wf_replace_underscores_with_spaces (string fs_str_orig)

// Returns value of fs_str_orig with each underscore replaced by a space character

int		p
string	s_in, s_out

s_in = fs_str_orig
p = Pos ( s_in, ' ' )

do while p > 0
	s_out =	s_out + Left ( s_in, p -1 ) + '_'
	s_in   = Mid ( s_in, p +1)	
	p = Pos (s_in, ' ' )
loop

s_out = s_out + s_in

return  s_out
end function

public function string wf_formatta (string fs_campo, string fs_confronto);// fs_campo     : colonna da cui estrarre il tipo col
// fs_confronto : stringa da formattare
string ls_col_type, ls_ret

ls_col_type = Upper(left(dw_appoggio.describe(fs_campo + ".ColType"), 4))

if ls_col_type = "CHAR" or ls_col_type = "DATE" or ls_col_type = "TIME" then 
	ls_ret = "'" + fs_confronto + "'"
else
	ls_ret = fs_confronto
end if

return ls_ret
end function

protected function integer wf_imposta_dw (string fs_nome_tabella, ref string fs_errore);string ls_nome_colonna,  ls_db, ls_nome, ls_sql, ls_logid, ls_owner, ls_errore
integer li_i, li_count
long ll_id
datastore lds_store

ls_db = f_db()
choose case ls_db
	case "SYBASE_ASA"
						
		select table_id
		into   :ll_id
		from   systable
		where  table_name = :fs_nome_tabella;

		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore nel caricamento id tabella (" + string(sqlca.sqlcode) + ")(" + fs_nome_tabella + "). Dettaglio: " + sqlca.sqlerrtext
			return -1
		end if
			
		ls_sql = " SELECT column_name FROM syscolumn WHERE table_id = " + string(ll_id)
		ls_sql += " AND domain_id <> 12 "	
		ls_sql += " ORDER BY column_id, column_name "		
		
		
	case "SYBASE_ASE"
				
		select id
		into   :ll_id
		from   sysobjects
		where  name = :fs_nome_tabella;
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore nel caricamento id tabella (" + string(sqlca.sqlcode) + ")(" + fs_nome_tabella + "). Dettaglio: " + sqlca.sqlerrtext
			return -1
		end if

		ls_sql = " select name from syscolumns WHERE id = " + string(ll_id) 
		ls_sql = ls_sql + " AND type <> 34 "
		ls_sql = ls_sql + " ORDER BY name "		

	case "MSSQL"
		
		select id
		into   :ll_id
		from   sysobjects
		where  name = :fs_nome_tabella;
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore nel caricamento id tabella (" + string(sqlca.sqlcode) + ")(" + fs_nome_tabella + "). Dettaglio: " + sqlca.sqlerrtext
			return -1
		end if		

		ls_sql = " select name from syscolumns WHERE id = " + string(ll_id) 
		ls_sql += " AND xtype <> 34 "
		ls_sql += " ORDER BY colid, name "	
		
		
	case "ORACLE"
		
		fs_nome_tabella = Upper(fs_nome_tabella)
		
		ls_logid = sqlca.logid
		
		SELECT sys_context('USERENV', 'CURRENT_SCHEMA') into   :ls_owner FROM dual;
		select OBJECT_ID into :ll_id from all_objects where OWNER=:ls_owner and OBJECT_NAME=:fs_nome_tabella ;
		
/*		
		select User#
		into   :ls_owner
		from   sys.user$
		where  name = :ls_logid;
		
		
		select obj#
		into   :ll_id
		from   sys.obj$
		where  name = :fs_nome_tabella
		and    owner# = :ls_owner;
*/
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore nel caricamento id tabella (" + string(sqlca.sqlcode) + ")(" + fs_nome_tabella + "). Dettaglio: " + sqlca.sqlerrtext
			return -1
		end if
		
		ls_sql = " SELECT name FROM sys.col$ WHERE obj# = " + string(ll_id)
		ls_sql += " AND type# <> 24 "
		ls_sql += " AND col# > 0 "		//27/01/2012: problema su anag_attrezzature
		ls_sql += " order by name"
		
		//ls_sql = ls_sql + " AND type# <> 24 order by name"
		
end choose

if not f_crea_datastore(ref lds_store, ls_sql) then
	fs_errore = "Errore durante la creazione del datastore per la tabella " + fs_nome_tabella
	return -1
end if

li_count = lds_store.retrieve()
if li_count < 0 then
	fs_errore = "Errore durante il controllo delle righe presenti nella tabella " + fs_nome_tabella
	return -1
elseif li_count = 0 then
	fs_errore = "Nessuna colonna disponibile per la tabella " + fs_nome_tabella
	return -1
end if

is_select_columns = ""
for li_i = 1 to li_count
	// creo select
	is_select_columns +=  lds_store.getitemstring(li_i, 1)
	if li_i < li_count then  is_select_columns += ", "
next

return wf_imposta_pk(fs_nome_tabella, fs_errore)
end function

private function integer wf_imposta_pk (string as_nome_tabella, ref string as_errore);/**
 * stefanop
 * 07/10/2010
 *
 * recupero le chiavi primarie.
 * Creo la dw e ciclo le colonne per vedere quali sono contrassegnare come PK.
 * Non lo posso fare prima perchè i BD usano una loro logica nel sapere quali sono i campi PK, in questa maniera eseguo una modifica verticale
 * in quanto è la DW che si arrangia a prelevare il valore corretto.
 **/
 
string ls_sql, ls_errore, ls_column
int li_count, li_i, li_icon
 
ls_sql = "select " + is_select_columns + " from " + as_nome_tabella
ls_sql = SQLCA.SyntaxFromSQL(ls_sql ,"style(type=grid)" , ls_errore)

if ls_errore <> ""  then
	as_errore = "Errore durante la creazione del datastore per la tabella " + as_nome_tabella
	return -1
end if

dw_appoggio.create(ls_sql, ls_errore)
if ls_errore <> ""  then
	as_errore = "Errore durante la creazione del datastore per la tabella " + as_nome_tabella
	return -1
end if

li_count = integer(dw_appoggio.Object.DataWindow.Column.Count)
if li_count = 0 then
	as_errore = "La tabella non ha colonne selezionabil."
	return -1
end if

for li_i = 1 to li_count
	ls_column = dw_appoggio.describe("#" + string(li_i) + ".name")
	
	if ls_column = "cod_azienda" then ib_cod_azienda = true
	if is_mode = "REPORT" and ls_column = "note_esterne" then continue
	
	if dw_appoggio.describe("#" + string(li_i) + ".key") = "yes" then
		is_pks[upperbound(is_pks) + 1] = ls_column
		tab_1.tp_1.lb_elenco_selezionato.additem(ls_column, 1)
	else
		tab_1.tp_1.lb_elenco_totale.additem(ls_column, 2)
	end if
	
	//ls_nome_colonna = wf_repl_underscore_with_space(ls_nome)
	tab_1.tp_2.dw_imposta_report.SetValue("campo", li_i, ls_column)
next

return 0
end function

public function boolean wf_is_primary_key (string as_column);/**
 * stefanop
 * 07/10/2010
 *
 * Controllo se la colonna è presente come chiave primaria
 **/
 
int li_i
 
for li_i = 1 to upperbound(is_pks)
	if as_column = is_pks[li_i] then return true
next

return false
end function

public subroutine wf_move_right (boolean ab_single_element);/**
 * stefanoèp
 * 07/10/2010
 *
 * Sposta gli oggetti a destra
 **/
 
int li_i
boolean ib_pk

for li_i = 1 to tab_1.tp_1.lb_elenco_totale.totalitems()
	ib_pk = wf_is_primary_key(tab_1.tp_1.lb_elenco_totale.text(li_i))
	
	if ab_single_element and  tab_1.tp_1.lb_elenco_totale.state(li_i) = 1 then	
		if ib_pk then
			tab_1.tp_1.lb_elenco_selezionato.additem(tab_1.tp_1.lb_elenco_totale.text(li_i), 1)
		else
			tab_1.tp_1.lb_elenco_selezionato.additem(tab_1.tp_1.lb_elenco_totale.text(li_i), 2)
		end if
		
		tab_1.tp_1.lb_elenco_totale.deleteitem(li_i)
		li_i --
	elseif not ab_single_element then
		if ib_pk then
			tab_1.tp_1.lb_elenco_selezionato.additem(tab_1.tp_1.lb_elenco_totale.text(li_i), 1)
		else
			tab_1.tp_1.lb_elenco_selezionato.additem(tab_1.tp_1.lb_elenco_totale.text(li_i), 2)
		end if
	end if
next

if not ab_single_element then
	tab_1.tp_1.lb_elenco_totale.reset()
end if

end subroutine

public subroutine wf_move_left (boolean ab_single_element);/**
 * stefanop
 * 07/10/2010
 *
 * Sposta gli oggetti a sinistra
 **/

int li_i 
boolean ib_pk

for li_i = 1 to tab_1.tp_1.lb_elenco_selezionato.totalitems()
	ib_pk = false
	
	if ab_single_element and tab_1.tp_1.lb_elenco_selezionato.State(li_i) = 1 then
		ib_pk = wf_is_primary_key(tab_1.tp_1.lb_elenco_selezionato.text(li_i))
		if is_mode = "IMPORT" and  ib_pk then
			// le chiavi primarie devono rimanere
			continue
		elseif is_mode = "REPORT" and ib_pk then
			tab_1.tp_1.lb_elenco_totale.additem( tab_1.tp_1.lb_elenco_selezionato.text(li_i), 1)
		else
			tab_1.tp_1.lb_elenco_totale.additem( tab_1.tp_1.lb_elenco_selezionato.text(li_i), 2)
		end if
		
		tab_1.tp_1.lb_elenco_selezionato.deleteitem(li_i)
		li_i --
	elseif not ab_single_element then
		ib_pk = wf_is_primary_key(tab_1.tp_1.lb_elenco_selezionato.text(li_i))
		
		if is_mode = "IMPORT" and ib_pk then
			// le chiavi primarie devono rimanere
			continue
		elseif is_mode = "REPORT" and ib_pk then
			 tab_1.tp_1.lb_elenco_totale.additem(tab_1.tp_1.lb_elenco_selezionato.text(li_i), 1)
		else
			 tab_1.tp_1.lb_elenco_totale.additem(tab_1.tp_1.lb_elenco_selezionato.text(li_i), 2)
		end if
		
		 tab_1.tp_1.lb_elenco_selezionato.deleteitem(li_i)
		 li_i --
	end if
next
end subroutine

on w_imposta_report.create
int iCurrent
call super::create
this.dw_appoggio=create dw_appoggio
this.ddlb_tipo=create ddlb_tipo
this.tab_1=create tab_1
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_appoggio
this.Control[iCurrent+2]=this.ddlb_tipo
this.Control[iCurrent+3]=this.tab_1
this.Control[iCurrent+4]=this.cb_report
end on

on w_imposta_report.destroy
call super::destroy
destroy(this.dw_appoggio)
destroy(this.ddlb_tipo)
destroy(this.tab_1)
destroy(this.cb_report)
end on

event pc_setwindow;call super::pc_setwindow;string ls_db,ls_nome_colonna, ls_sintassi, ls_errore
integer li_i

is_mode = s_cs_xx.parametri.parametro_s_14
is_tabella = s_cs_xx.parametri.parametro_s_15

// Icone
tab_1.tp_1.lb_elenco_totale.deletepictures()
tab_1.tp_1.lb_elenco_selezionato.deletepictures()
tab_1.tp_1.lb_elenco_totale.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\key.png")
tab_1.tp_1.lb_elenco_selezionato.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\key.png")
// ----

if is_tabella <> "?" and is_tabella <> "" and not(isnull(is_tabella)) then
	li_i = wf_imposta_dw(is_tabella,ls_errore)
	if li_i = -1 then
		g_mb.messagebox("Apice", "Sorgente Dati non Valida! Dettaglio: " + ls_errore, Exclamation!)
	elseif isnull(li_i) then
		g_mb.messagebox("Apice", "Sorgente Dati non Valida (risultato nullo)!Tabella:" + is_tabella + ". Dettaglio: " + ls_errore, Exclamation!)
	end if
else
	g_mb.messagebox("Apice", "Sorgente Dati non Valida", Exclamation!)
end if

ddlb_tipo.selectitem(3)
if is_mode = "IMPORT" then 
	ddlb_tipo.visible = false
	cb_report.text = "Importa"
	tab_1.tp_2.visible = false
	
	title = "Imposta Importazione su: " + is_tabella
else
	title = "Imposta Selezione Report su: " + is_tabella
end if

tab_1.tp_2.dw_imposta_report.insertrow(0)
tab_1.tp_2.dw_imposta_report.object.p_elimina.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\delete.png"


end event

type dw_appoggio from datawindow within w_imposta_report
boolean visible = false
integer x = 983
integer y = 1180
integer width = 594
integer height = 100
integer taborder = 150
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type ddlb_tipo from dropdownlistbox within w_imposta_report
integer x = 23
integer y = 1180
integer width = 663
integer height = 360
integer taborder = 140
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean sorted = false
string item[] = {"Tabulare","Dettaglio","Elenco"}
borderstyle borderstyle = stylelowered!
end type

type tab_1 from tab within w_imposta_report
event create ( )
event destroy ( )
integer x = 23
integer y = 20
integer width = 3200
integer height = 1120
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tp_1 tp_1
tp_2 tp_2
end type

on tab_1.create
this.tp_1=create tp_1
this.tp_2=create tp_2
this.Control[]={this.tp_1,&
this.tp_2}
end on

on tab_1.destroy
destroy(this.tp_1)
destroy(this.tp_2)
end on

type tp_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 116
integer width = 3163
integer height = 988
long backcolor = 12632256
string text = "Colonne interessate"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
lb_elenco_selezionato lb_elenco_selezionato
lb_elenco_totale lb_elenco_totale
st_2 st_2
st_1 st_1
cb_togli_tutti cb_togli_tutti
cb_togli_uno cb_togli_uno
cb_aggiungi_tutti cb_aggiungi_tutti
cb_aggiungi_uno cb_aggiungi_uno
lb_1 lb_1
end type

on tp_1.create
this.lb_elenco_selezionato=create lb_elenco_selezionato
this.lb_elenco_totale=create lb_elenco_totale
this.st_2=create st_2
this.st_1=create st_1
this.cb_togli_tutti=create cb_togli_tutti
this.cb_togli_uno=create cb_togli_uno
this.cb_aggiungi_tutti=create cb_aggiungi_tutti
this.cb_aggiungi_uno=create cb_aggiungi_uno
this.lb_1=create lb_1
this.Control[]={this.lb_elenco_selezionato,&
this.lb_elenco_totale,&
this.st_2,&
this.st_1,&
this.cb_togli_tutti,&
this.cb_togli_uno,&
this.cb_aggiungi_tutti,&
this.cb_aggiungi_uno,&
this.lb_1}
end on

on tp_1.destroy
destroy(this.lb_elenco_selezionato)
destroy(this.lb_elenco_totale)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_togli_tutti)
destroy(this.cb_togli_uno)
destroy(this.cb_aggiungi_tutti)
destroy(this.cb_aggiungi_uno)
destroy(this.lb_1)
end on

type lb_elenco_selezionato from picturelistbox within tp_1
integer x = 1719
integer y = 112
integer width = 1417
integer height = 860
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean hscrollbar = true
boolean vscrollbar = true
boolean sorted = false
borderstyle borderstyle = stylelowered!
long picturemaskcolor = 536870912
end type

event doubleclicked;if index < 1 then return

wf_move_left(true)
end event

type lb_elenco_totale from picturelistbox within tp_1
integer x = 27
integer y = 112
integer width = 1417
integer height = 860
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean hscrollbar = true
boolean vscrollbar = true
boolean sorted = false
borderstyle borderstyle = stylelowered!
long picturemaskcolor = 536870912
end type

event doubleclicked;if index < 1 then return

wf_move_right(true)
end event

type st_2 from statictext within tp_1
integer x = 1719
integer y = 32
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Colonne da visualizzare"
boolean focusrectangle = false
end type

type st_1 from statictext within tp_1
integer x = 27
integer y = 32
integer width = 498
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Colonne disponibili"
boolean focusrectangle = false
end type

type cb_togli_tutti from commandbutton within tp_1
integer x = 1513
integer y = 832
integer width = 160
integer height = 100
integer taborder = 40
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<<"
end type

event clicked;//integer li_i 
//
//for li_i = 1 to lb_elenco_selezionato.totalitems()
//	lb_elenco_totale.additem(lb_elenco_selezionato.text(li_i))
//next
//
//lb_elenco_selezionato.reset()
//
//lb_elenco_totale.selectitem(1)

wf_move_left(false)
end event

type cb_togli_uno from commandbutton within tp_1
integer x = 1513
integer y = 712
integer width = 160
integer height = 100
integer taborder = 60
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<"
end type

event clicked;//integer li_i 
//
//for li_i = 1 to lb_elenco_selezionato.totalitems()
//	if lb_elenco_selezionato.State ( li_i ) = 1 and not wf_is_primary_key(lb_elenco_selezionato.SelectedItem()) then
//		lb_elenco_totale.additem(lb_elenco_selezionato.SelectedItem ( ))
//		lb_elenco_selezionato.deleteitem(lb_elenco_selezionato.SelectedIndex ( ))
//		li_i --
//	end if
//next
//
//lb_elenco_selezionato.selectitem(1)
//lb_elenco_totale.selectitem(1)
//
//lb_elenco_selezionato.selectitem(1)
//lb_elenco_totale.selectitem(1)

wf_move_left(true)
end event

type cb_aggiungi_tutti from commandbutton within tp_1
integer x = 1490
integer y = 232
integer width = 160
integer height = 100
integer taborder = 30
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">>"
end type

event clicked;//integer li_i 
//
//for li_i = 1 to lb_elenco_totale.totalitems()
//	lb_elenco_selezionato.additem(lb_elenco_totale.text(li_i))
//next
//
//lb_elenco_totale.reset()
//
//lb_elenco_selezionato.selectitem(1)
//
//

wf_move_right(false)
end event

type cb_aggiungi_uno from commandbutton within tp_1
integer x = 1490
integer y = 112
integer width = 160
integer height = 100
integer taborder = 30
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">"
end type

event clicked;//integer li_i 
//
//for li_i = 1 to lb_elenco_totale.totalitems()
//	if lb_elenco_totale.State ( li_i ) = 1 then
//		lb_elenco_selezionato.additem(lb_elenco_totale.SelectedItem ( ))
//		lb_elenco_totale.deleteitem(lb_elenco_totale.SelectedIndex ( ))
//		li_i --
//	end if
//next
//
//lb_elenco_selezionato.selectitem(1)
//lb_elenco_totale.selectitem(1)

wf_move_right(true)
end event

type lb_1 from listbox within tp_1
integer x = 1394
integer y = 1340
integer width = 1417
integer height = 920
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
boolean extendedselect = true
end type

type tp_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 116
integer width = 3163
integer height = 988
long backcolor = 12632256
string text = "Query di selezione"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_imposta_report dw_imposta_report
end type

on tp_2.create
this.dw_imposta_report=create dw_imposta_report
this.Control[]={this.dw_imposta_report}
end on

on tp_2.destroy
destroy(this.dw_imposta_report)
end on

type dw_imposta_report from datawindow within tp_2
integer x = 27
integer y = 12
integer width = 3109
integer height = 940
integer taborder = 30
string dataobject = "d_imposta_report"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;choose case dwo.name
	
	case "unione"
		if rowcount() = row then insertrow(0)
end choose
end event

event clicked;if row < 1 then return

choose case dwo.name
	case "p_elimina"
		deleterow(row)	
		
		if rowcount() = 0 then
			insertrow(0)
		end if
end choose
end event

type cb_report from commandbutton within w_imposta_report
integer x = 2560
integer y = 1180
integer width = 663
integer height = 100
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_sql, ls_errore, ls_style, ls_report_type, ls_campo, ls_confronto, ls_operatore, ls_apri, ls_chiudi
integer li_i, li_tot_items
long ll_pos

if tab_1.tp_1.lb_elenco_selezionato.totalitems() = 0 then
	g_mb.show("", "Selezionare le colonne da visualizzare prima di procedere")
	return
end if

li_tot_items = tab_1.tp_1.lb_elenco_selezionato.totalitems()
ls_sql = "select "

for li_i = 1 to li_tot_items
	ls_sql = ls_sql + wf_repl_space_with_underscore(tab_1.tp_1.lb_elenco_selezionato.text(li_i))
	if li_i = li_tot_items then
		ls_sql = ls_sql + " "
	else
		ls_sql = ls_sql + ","
	end if
next

ls_sql = ls_sql + " from " + is_tabella
if is_mode = "REPORT" then 
	if ib_cod_azienda then ls_sql = ls_sql + " where cod_azienda='" + s_cs_xx.cod_azienda + "' "
	
	tab_1.tp_2.dw_imposta_report.accepttext()
	if not isnull(tab_1.tp_2.dw_imposta_report.getitemstring(1,"campo")) then
		if ib_cod_azienda then 
			ls_sql = ls_sql + " and "
		else
			ls_sql = ls_sql + " where "
		end if
	
		for li_i = 1 to tab_1.tp_2.dw_imposta_report.rowcount()
			ls_apri = tab_1.tp_2.dw_imposta_report.getitemstring(li_i,"apri_par")
			ls_campo = tab_1.tp_2.dw_imposta_report.getitemstring(li_i,"campo")
			ls_confronto = tab_1.tp_2.dw_imposta_report.getitemstring(li_i,"confronto")
			ls_operatore = tab_1.tp_2.dw_imposta_report.getitemstring(li_i,"operatore")
			ls_chiudi = tab_1.tp_2.dw_imposta_report.getitemstring(li_i,"chiudi_par")
			if li_i = tab_1.tp_2.dw_imposta_report.rowcount() then
				
				if not isnull(ls_apri) then
					ls_sql = ls_sql + "( "
				end if
				
				if isnull(ls_campo) then 
					g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
					return
				else
					ls_sql = ls_sql + wf_repl_space_with_underscore(ls_campo) + " "
				end if
	
				if isnull(ls_operatore) then 
					g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
					return
				else
	// *******    modifiche Michela 19/11/2002: aggiunto operatori like, not like, is null, is not null
					choose case ls_operatore
						case "=="                          //like
							
							ls_sql = ls_sql + " LIKE " 
							if isnull(ls_confronto) then 
								g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
								return
							else
								ls_sql = ls_sql + wf_formatta(wf_repl_space_with_underscore(ls_campo),ls_confronto) + " "
							end if
							
						case "!="
							
							ls_sql = ls_sql + " NOT LIKE "
							if isnull(ls_confronto) then 
								g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
								return
							else
								ls_sql = ls_sql + wf_formatta(wf_repl_space_with_underscore(ls_campo),ls_confronto) + " "
							end if
							
						case "nl"
							ls_sql = ls_sql + " IS NULL "
						case "nn"
							ls_sql = ls_sql + " IS NOT NULL "
						case else
							
							ls_sql = ls_sql + " " + ls_operatore + " "
							if isnull(ls_confronto) then 
								g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
								return
							else
								ls_sql = ls_sql + wf_formatta(wf_repl_space_with_underscore(ls_campo),ls_confronto) + " "
							end if						
							
					end choose
				end if
	
	
				
				if not isnull(ls_chiudi) then
					ls_sql = ls_sql + ") "
				end if
				
			else
				
				if not isnull(ls_apri) then
					ls_sql = ls_sql + "( "
				end if
				
				if isnull(ls_campo) then 
					g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
					return
				else
					ls_sql = ls_sql + wf_repl_space_with_underscore(ls_campo) + " "
				end if
	
				if isnull(ls_operatore) then 
					g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
					return
				else				
					choose case ls_operatore
						case "=="                          //like
							
							ls_sql = ls_sql + " LIKE " 
							if isnull(ls_confronto) then 
								g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
								return
							else
								ls_sql = ls_sql + wf_formatta(wf_repl_space_with_underscore(ls_campo),ls_confronto) + " "
							end if
							
						case "!="
							
							ls_sql = ls_sql + " NOT LIKE "
							if isnull(ls_confronto) then 
								g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
								return
							else
								ls_sql = ls_sql + wf_formatta(wf_repl_space_with_underscore(ls_campo),ls_confronto) + " "
							end if
							
						case "nl"
							ls_sql = ls_sql + " IS NULL "
						case "nn"
							ls_sql = ls_sql + " IS NOT NULL "
						case else
							
							ls_sql = ls_sql + " " + ls_operatore + " "
							if isnull(ls_confronto) then 
								g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)				
								return
							else
								ls_sql = ls_sql + wf_formatta(wf_repl_space_with_underscore(ls_campo),ls_confronto) + " "
							end if						
							
					end choose
				end if
	
				
				if not isnull(ls_chiudi) then
					ls_sql = ls_sql + ") "
				end if
			
				if isnull(tab_1.tp_2.dw_imposta_report.getitemstring(li_i,"unione") + " ") then 
					g_mb.messagebox("Apice","Errore di impostazione selezione: manca un parametro nella maschera di selezione",stopsign!)
					return
				else
					ls_sql = ls_sql + tab_1.tp_2.dw_imposta_report.getitemstring(li_i,"unione") + " "
				end if			
	
			end if
		next
	end if
	
elseif is_mode = "IMPORT" then 
//	if ib_cod_azienda then
//		ls_sql = ls_sql + " where cod_azienda='cod_che_non_esite' "
//	end if
end if

choose case ddlb_tipo.selectitem(ddlb_tipo.text, 1)
	case 1
		ls_report_type = "tabular"
		
	case 2
		ls_report_type = "form"
		
	case 3
		ls_report_type = "grid"
		
end choose

//ls_style = 'style(type=' + ls_report_type + ')' + &
//			'Text(background.mode=0 background.color=1073741824 color=0 ' +&
//			'font.face = "MS Sans Serif"  font.height = -10  font.weight = 700 font.family = 2' + &
//					'font.pitch=2  border = 0 ) ' + &
//			'Column(background.mode=0 background.color=1073741824 color=0 ' +&
//			'font.face = "MS Sans Serif"  font.height = -8  font.weight = 400 font.family = 2' + &
//					'font.pitch = 2 border = 2 ) ' 

ls_style = "style(type=" + ls_report_type + ")"


s_cs_xx.parametri.parametro_s_15 = SQLCA.SyntaxFromSQL(ls_sql ,ls_style , ls_errore)
s_cs_xx.parametri.parametro_s_14 = is_tabella

if Len(ls_errore) > 0 THEN
	g_mb.messagebox("Apice","Errore durante la creazione del Report",stopsign!)
else
	if is_mode = "REPORT" then
		window_open(w_report_grid_form,-1)
	elseif is_mode = "IMPORT" then
		window_open(w_import_grid_form,-1)
	end if
end if
end event

