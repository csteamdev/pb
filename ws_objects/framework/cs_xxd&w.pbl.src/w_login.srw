﻿$PBExportHeader$w_login.srw
$PBExportComments$Finestra Login
forward
global type w_login from w_cs_xx_risposta
end type
type shl_2 from statichyperlink within w_login
end type
type shl_1 from statichyperlink within w_login
end type
type st_msg_password from statictext within w_login
end type
type pb_1 from picturebutton within w_login
end type
type dw_login from datawindow within w_login
end type
type p_1 from picture within w_login
end type
type cb_ok from uo_cb_ok within w_login
end type
end forward

global type w_login from w_cs_xx_risposta
integer width = 1605
integer height = 1412
string title = "Login"
integer animationtime = 500
event ue_post_redraw ( )
shl_2 shl_2
shl_1 shl_1
st_msg_password st_msg_password
pb_1 pb_1
dw_login dw_login
p_1 p_1
cb_ok cb_ok
end type
global w_login w_login

type variables
boolean ib_open_menu=false

private:
	boolean ib_halt_application = true
end variables

forward prototypes
public function integer wf_modifica_password (string fs_username, string fs_password)
public subroutine wf_cambia_password ()
public function long wf_log_gdpr (string as_errore)
end prototypes

event ue_post_redraw();/**
 * stefanop
 * 12/10/2011
 *
 * Forzo il redraw dell'intero MDI e della finestra corrente
 **/
 
setredraw(true)
yield()
w_cs_xx_mdi.setredraw(true)
yield()
end event

public function integer wf_modifica_password (string fs_username, string fs_password);string			ls_username, ls_cod_utente, ls_password_1, ls_flag_collegato, ls_flag_blocco, ls_cod_azienda
datetime		ldt_data_blocco, ldt_oggi
n_cst_crypto lnv_crypt

ldt_oggi = datetime( date( today()), 00:00:00)

// *** decrypto la vecchia password per confrontarla con quella che inserirà

lnv_crypt = CREATE n_cst_crypto

lnv_crypt.decryptdata( fs_password, fs_password)

DESTROY lnv_crypt

s_cs_xx.parametri.parametro_s_2 = fs_password

s_cs_xx.parametri.parametro_b_1 = false

window_open(w_modifica_pwd,0)

if s_cs_xx.parametri.parametro_b_1 = true then
	
	update utenti
	set		password = :s_cs_xx.parametri.parametro_s_1,
				data_modifica_pwd = :ldt_oggi
	where	username = :fs_username;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Attenzione","Errore durante l'aggiornamento della password sul db: " + sqlca.sqlerrtext, stopsign!)
		rollback;
		return -1
	else
		commit;
		g_mb.messagebox( "APICE", "Modifica password avvenuta con successo!", Information!)
		return 0
	end if
	
else

	return -1

end if
				

end function

public subroutine wf_cambia_password ();string			ls_username, ls_cod_utente, ls_password_1, ls_flag_collegato, ls_flag_blocco, ls_cod_azienda
datetime		ldt_data_blocco, ldt_oggi
n_cst_crypto lnv_crypt

ldt_oggi = datetime( date( today()), 00:00:00)

ls_username = dw_login.getitemstring(1, "username")

if isnull(ls_username) or ls_username = "" then
	st_msg_password.text = "Inserire un utente valido."
	dw_login.setfocus()
	dw_login.setcolumn( "username" )
	return
end if

if ls_username <> "CS_SYSTEM" then
	
	select	cod_utente,
				password,
				flag_collegato,
				flag_blocco,
				data_blocco,
				cod_azienda
	into		:ls_cod_utente,
				:ls_password_1,
				:ls_flag_collegato,
				:ls_flag_blocco,
				:ldt_data_blocco,
				:ls_cod_azienda
	from		utenti
	where	username = :ls_username;
	
	if sqlca.sqlcode < 0 then
	  	g_mb.messagebox("Attenzione","Errore in controllo utente: " + sqlca.sqlerrtext, stopsign!, ok!)
		dw_login.setfocus()
		dw_login.setcolumn( "username" )
		return
	elseif sqlca.sqlcode = 100 then
		st_msg_password.text = "Utente non presente in tabella"
		dw_login.setfocus()
		dw_login.setcolumn( "username" )
		return
	end if		
	
	// *** decrypto la vecchia password per confrontarla con quella che inserirà
	
	lnv_crypt = CREATE n_cst_crypto

	lnv_crypt.decryptdata( ls_password_1, ls_password_1)
	
	DESTROY lnv_crypt
	
	s_cs_xx.parametri.parametro_s_2 = ls_password_1

	s_cs_xx.parametri.parametro_b_1 = false

	window_open(w_modifica_pwd,0)

	if s_cs_xx.parametri.parametro_b_1 = true then
		
		update utenti
		set
			password = :s_cs_xx.parametri.parametro_s_1,
			data_modifica_pwd = :ldt_oggi
		where	
			username = :ls_username;
		
		if sqlca.sqlcode < 0 then
		  	g_mb.error("Errore durante l'aggiornamento della password sul db: " + sqlca.sqlerrtext)
			rollback;
			return
		else
			commit;
			g_mb.success( "Modifica password avvenuta con successo!")
			return
		end if
	else	
			g_mb.warning( "Cambio password annullato dall'utente!")
	end if
	
end if
			

end subroutine

public function long wf_log_gdpr (string as_errore);// Funzione per adeguamento a GDPR 25 maggio 2018 //
string			ls_sql, ls_messaggio
long 			ll_ret, ll_i, ll_cont
datetime 	ldt_date_start
date			ldd_date
datastore	lds_data
uo_log_sistema luo_log_sistema

// per prima cosa verifico quante password errate sono state digitate di continuo nelle ultime 24 ore prima di trovare un LOGIN - SUCCESS
ll_cont = 0
ldd_date = relativedate(today(), -1)
ldt_date_start = datetime(ldd_date, now())

ls_sql = "select messaggio from log_sistema where flag_tipo_log='LOGIN_WIN' and data_registrazione >= '" + string(ldt_date_start, s_cs_xx.db_funzioni.formato_data) + "' order by id_log_sistema desc"
ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql, as_errore)

if ll_ret < 0 then return -1
if ll_ret > 0 then
	for ll_i = 1 to ll_ret
		ls_messaggio = lds_data.getitemstring(ll_i,1)
		if left(ls_messaggio, 15) = "LOGIN - SUCCESS" then
			exit
		else
			ll_cont ++
		end if
	next
end if

// se il numero di password errate è MINORE di 5 faccio una semplice registrazione nel log con errore LOGIN - FAILURE
luo_log_sistema = create uo_log_sistema
if ll_cont < 5 then
	luo_log_sistema.uof_write_log_sistema( "LOGIN_WIN", g_str.format("LOGIN - FAILURE: Accesso Utente=$1, ComputerName=$2, IP Address=$3",s_cs_xx.cod_utente, guo_functions.uof_get_computer_name( ),guo_functions.uof_get_ip()) )
else
	// se il numero di password errate è MAGGIORE O UGUALE di 5 faccio una registrazione nel log con errore LOGIN - HACK
	luo_log_sistema.uof_write_log_sistema( "LOGIN_WIN", g_str.format("LOGIN - HACK - POSSIBILE TENTATIVO DI ACCESSO FRAUDOLENTO: Accesso Utente=$1, ComputerName=$2, IP Address=$3",s_cs_xx.cod_utente, guo_functions.uof_get_computer_name( ),guo_functions.uof_get_ip()) )
end if	

destroy luo_log_sistema
commit;

return 0
end function

event pc_accept;call super::pc_accept;integer li_num_utenti,li_risposta
datetime ldt_data_blocco, ldt_data_modifica_pwd, ldt_data_scadenza, ldt_data_avvertimento, ldt_oggi
string  ls_username, ls_password, ls_password_1, ls_flag_supervisore, ls_flag_collegato, ls_flag_blocco, ls_cod_azienda, ls_cod_individuale, ls_cod_utente,ls_cod_lingua_applicazione, ls_errore
long		ll_giorni_scadenza, ll_giorni_avvertimento, ll_ret
boolean	lb_avverti_pwd, lb_modifica_pwd
n_cst_crypto lnv_crypt
uo_log_sistema luo_log_sistema

ldt_oggi = datetime( date(today()), 00:00:00)
dw_login.accepttext()

ls_username = dw_login.getitemstring(1, "username")
ls_password = dw_login.getitemstring(1, "password")

if isnull(ls_password) then ls_password = ""

if isnull(ls_username) or ls_username = "" then
	st_msg_password.text=  "Inserire un utente valido."
	st_msg_password.visible = true
   	dw_login.setfocus()
	dw_login.setcolumn( "username" )
	return
end if

if ls_username = "CS_SYSTEM" then
	
	s_cs_xx.admin = true
	s_cs_xx.cod_utente = "CS_SYSTEM"
	
else
	
	select	cod_utente,
				password,
				flag_collegato,
				flag_blocco,
				data_blocco,
				cod_azienda,
				data_modifica_pwd,
				flag_supervisore
	into		:ls_cod_utente,
				:ls_password_1,
				:ls_flag_collegato,
				:ls_flag_blocco,
				:ldt_data_blocco,
				:ls_cod_azienda,
				:ldt_data_modifica_pwd,
				:ls_flag_supervisore
	from		utenti
	where	username = :ls_username;
	
	if sqlca.sqlcode < 0 then
		st_msg_password.text=  "Errore in controllo utente."
		st_msg_password.visible = true
	  	g_mb.messagebox("Attenzione","Errore in controllo utente: " + sqlca.sqlerrtext, stopsign!, ok!)
		dw_login.setfocus()
		dw_login.setcolumn( "username" )
		return
	elseif sqlca.sqlcode = 100 then
		st_msg_password.text=  "Utente inesistente nel sistema."
		st_msg_password.visible = true
		dw_login.setfocus()
		dw_login.setcolumn( "username" )
		return
	end if
	
	if ls_flag_collegato = "S" then
		s_cs_xx.admin = true
	else
		s_cs_xx.admin = false
	end if
	
	s_cs_xx.cod_utente = ls_cod_utente
	
	s_cs_xx.supervisore=(ls_flag_supervisore = "S")

	
	lnv_crypt = CREATE n_cst_crypto

	if lnv_crypt.EncryptData(ls_password,ls_password) < 0 then
		g_mb.messagebox("APICE","La nuova password contiene caratteri non consentiti.~nI caratteri consentiti comprendono:~n~n" + &
					"- tutte le cifre numeriche 0,1,2,...,9~n- tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z~n" + &
					"- alcuni simboli tra cui ! # $ % & ( ) * + , - . / : ; < > = ? @ [ ] \ _ | { }~n~nModificare la password e riprovare",stopsign!)
		return
	end if

	DESTROY lnv_crypt
	
end if

if (isnull(ls_password_1) or ls_password_1 = "") AND ls_username <> "CS_SYSTEM" then
	g_mb.messagebox("Attenzione", "L'utente non ha alcuna password: è necessario impostarla altrimenti non è possibile accedere al sistema.", Stopsign!)
				  				  
	s_cs_xx.parametri.parametro_b_1 = false

	window_open(w_imposta_pwd,0)

	if s_cs_xx.parametri.parametro_b_1 = true then
		
		update	utenti
		set			password = :s_cs_xx.parametri.parametro_s_1,
					data_modifica_pwd = :ldt_oggi
		where	username = :ls_username;
		
		if sqlca.sqlcode < 0 then
		  	g_mb.messagebox("Attenzione","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
			return
		end if
		
		ldt_data_modifica_pwd = ldt_oggi
		
	else
	
		return
	
	end if
	
end if

if (ls_username = "CS_SYSTEM" and ls_password <> guo_application.cs_pwd)  or (ls_username <> "CS_SYSTEM" and ls_password <> ls_password_1) then
		if wf_log_gdpr( ls_errore ) < 0 then
			st_msg_password.text=  "Errore nel controllo Password."
		else
			st_msg_password.text=  "Password non coerente."
		end if
		st_msg_password.visible = true
		dw_login.setfocus()
		dw_login.setcolumn( "username" )
		return
end if

if ls_username <> "CS_SYSTEM" and ls_flag_blocco = "S" and today() >= date(ldt_data_blocco) then
	if wf_log_gdpr( ls_errore ) < 0 then
		st_msg_password.text=  "Errore nel controllo Password."
	else
		st_msg_password.text=  "Password non coerente."
	end if
	st_msg_password.text=  "Utente bloccato dal " + string(date(ldt_data_blocco), "dd/mm/yyyy") + "."
	st_msg_password.visible = true
	dw_login.setfocus()
	dw_login.setcolumn( "username" )
	return
end if

// *** Michela 28/11/2007 spec. Modifiche_12_2007 csteam: se la password esiste controllo la scadenza prima di farlo continuare

if ls_username <> "CS_SYSTEM" then

	select	numero
	into		:ll_giorni_scadenza
	from		parametri
	where	cod_parametro = 'GSP' and
				flag_parametro = 'N';
				
	if sqlca.sqlcode <> 0 or isnull(ll_giorni_scadenza) then ll_giorni_scadenza = 0 
	
	select	numero
	into		:ll_giorni_avvertimento
	from		parametri
	where	cod_parametro = 'GSA' and
				flag_parametro = 'N';
				
	if sqlca.sqlcode <> 0 or isnull(ll_giorni_avvertimento) then ll_giorni_avvertimento = 0 	
	
	lb_avverti_pwd = false
	lb_modifica_pwd = false
	
	if ll_giorni_scadenza > 0 then	
		ldt_data_scadenza = datetime( relativedate( date(ldt_data_modifica_pwd), ll_giorni_scadenza), 00:00:00)
		if ll_giorni_avvertimento > 0 then	
			ldt_data_avvertimento = datetime( relativedate( date(ldt_data_scadenza), ll_giorni_avvertimento * -1), 00:00:00)
			if ldt_data_avvertimento <= ldt_oggi then
				lb_avverti_pwd = true
			end if
		end if	
		if ldt_data_scadenza <= ldt_oggi or isnull(ldt_data_scadenza) then
			lb_modifica_pwd = true
		end if
	end if
	
	if lb_modifica_pwd then
		g_mb.messagebox( "APICE", "Attenzione: la password risulta scaduta. Si procederà ora con l'impostazione della nuova password!", Information!)
		ll_ret = wf_modifica_password( ls_username, ls_password_1)
		if ll_ret < 0 then
			return 
		end if
	elseif lb_avverti_pwd then
		ll_ret = g_mb.messagebox( "APICE", "Attenzione: la password sta per scadere. Procedere ora con l'impostazione della nuova password?", Exclamation!, YesNo!,2)
		if ll_ret = 1 then
			ll_ret = wf_modifica_password( ls_username, ls_password_1)
			if ll_ret < 0 then
				return
			end if
		end if
	end if
	
end if

// 
			
if ls_username = "CS_SYSTEM" then
													
	li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "azi", s_cs_xx.cod_azienda)
	setnull(s_cs_xx.cod_lingua_applicazione)
	
else
   s_cs_xx.cod_azienda = ls_cod_azienda
end if


if ls_username <> "CS_SYSTEM" then
	if dw_login.getitemstring(1, "remember") = "N" then
		// cancella username e password in LocalUser	
		li_risposta = Registryset(s_cs_xx.chiave_root_user + "applicazione_" +  s_cs_xx.profilocorrente, "cdt", "")
		li_risposta = Registryset(s_cs_xx.chiave_root_user + "applicazione_" +  s_cs_xx.profilocorrente, "pwd", "")
		// cancella username e password in LocalMachine
		li_risposta = Registryset(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "cdt", "")	
		li_risposta = Registryset(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "pwd", "")
	else
		li_risposta = Registryset(s_cs_xx.chiave_root_user + "applicazione_" +  s_cs_xx.profilocorrente, "cdt", ls_username)
		li_risposta = Registryset(s_cs_xx.chiave_root_user + "applicazione_" +  s_cs_xx.profilocorrente, "pwd", ls_password)
	end if
end if

if isnull(s_cs_xx.cod_azienda) or s_cs_xx.cod_azienda = "" then
   window_open(w_aziende_scelta, 0)
else
	ib_open_menu = true
end if

commit;

//F. Baschirotto, 30/11/2012: genero il menù delle aziende, sul menù dell'MDI, per l'utente corrente.
w_cs_xx_mdi.im_menu_cs.mf_carica_aziende()
//F. Baschirotto, 03/12/2012: genero il menù dei profili, sul menù dell'MDI, per l'utente corrente.
w_cs_xx_mdi.im_menu_cs.mf_carica_profili()

// stefnaop 24/12/2010: forzo il refresh della finestra MDI
w_cs_xx_mdi.setredraw(true)
w_cs_xx_mdi.setfocus()
yield()

// se è andato tutto bene allora registra l'accesso dell'utente
luo_log_sistema = create uo_log_sistema
luo_log_sistema.uof_write_log_sistema( "LOGIN_WIN", g_str.format("LOGIN - SUCCESS: Accesso Utente=$1, ComputerName=$2, IP Address=$3",s_cs_xx.cod_utente, guo_functions.uof_get_computer_name( ),guo_functions.uof_get_ip()) )
destroy luo_log_sistema

COMMIT;

ib_halt_application = false
close(this)
end event

on pc_close;call w_cs_xx_risposta::pc_close;pcca.pcmgr.i_exitwindow = pcca.mdi_frame
postevent(pcca.pcmgr, "pc_exitapp")


end on

on w_login.create
int iCurrent
call super::create
this.shl_2=create shl_2
this.shl_1=create shl_1
this.st_msg_password=create st_msg_password
this.pb_1=create pb_1
this.dw_login=create dw_login
this.p_1=create p_1
this.cb_ok=create cb_ok
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.shl_2
this.Control[iCurrent+2]=this.shl_1
this.Control[iCurrent+3]=this.st_msg_password
this.Control[iCurrent+4]=this.pb_1
this.Control[iCurrent+5]=this.dw_login
this.Control[iCurrent+6]=this.p_1
this.Control[iCurrent+7]=this.cb_ok
end on

on w_login.destroy
call super::destroy
destroy(this.shl_2)
destroy(this.shl_1)
destroy(this.st_msg_password)
destroy(this.pb_1)
destroy(this.dw_login)
destroy(this.p_1)
destroy(this.cb_ok)
end on

event pc_setwindow;call super::pc_setwindow;string ls_profilo_corrente,ls_cod_utente,ls_pwd,ls_test
integer li_risposta

n_cst_crypto lnv_crypt

this.BringToTop = TRUE

// stefanop 08/10/2010: imposto colore DW
ls_test = dw_login.modify("Datawindow.Color=" + string(s_themes.theme[s_themes.current_theme].dw_backcolor))
if ls_test <> "" then
	g_mb.error("", ls_test)
end if
// ---

pb_1.picturename = s_cs_xx.volume + s_cs_xx.risorse + "10.5\Login_chage_pwd4.png"

// EnMe 22/9/2008
// prova a vedere se c'è il codice utente in local user questo è utile soprattutto per i terminal server
li_risposta = Registryget(s_cs_xx.chiave_root_user + "applicazione_" +  s_cs_xx.profilocorrente, "cdt", ls_cod_utente)

if li_risposta = -1 then
	li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "cdt", ls_cod_utente)
	li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "pwd", ls_pwd)
else
	li_risposta = Registryget(s_cs_xx.chiave_root_user + "applicazione_" +  s_cs_xx.profilocorrente, "pwd", ls_pwd)
end if

dw_login.insertrow(0)
dw_login.setitem(1, "username", ls_cod_utente )

// stefanop 24/08/2011: imposto autofocus sulla password nel caso il cod_utente sia valido
if not isnull(ls_cod_utente) and ls_cod_utente <> "" then
	dw_login.setcolumn("password")
end if

if ls_pwd <> "" and not isnull(ls_pwd) then
	lnv_crypt = CREATE n_cst_crypto
	
	if lnv_crypt.decryptData(ls_pwd,ls_pwd) < 0 then
		g_mb.messagebox("APICE","Errore in decrypt password: codice non riconosciuto",stopsign!)
		ls_pwd = ""
	end if
				  
	//sle_password.text = ls_pwd
	dw_login.setitem(1, "password", ls_pwd )

	DESTROY lnv_crypt
end if

if isvalid(w_menu_80) then
	w_menu_80.triggerevent("ue_close")
end if

postevent("ue_post_redraw")
end event

event close;call super::close;if ib_open_menu then
	open(w_menu_button,w_cs_xx_mdi)
	//openSheet(w_extra_windows,w_cs_xx_mdi, 0, Original!)
	
end if

// killo l'applicazione
if ib_halt_application then triggerevent("pc_close")
end event

event open;call super::open;setredraw(false)
end event

event resize;// NESSUN RESIZE DELLA FINESTRA.
// LASCIARE COSI COME E'
end event

type shl_2 from statichyperlink within w_login
integer x = 91
integer y = 1180
integer width = 471
integer height = 56
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 10789024
long backcolor = 12632256
string text = "www.csteam.com"
boolean focusrectangle = false
string url = "http://www.csteam.com"
end type

type shl_1 from statichyperlink within w_login
integer x = 91
integer y = 1000
integer width = 489
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 8421504
long backcolor = 12632256
string text = "Cambia Password"
boolean focusrectangle = false
end type

event clicked;wf_cambia_password()
end event

type st_msg_password from statictext within w_login
boolean visible = false
integer x = 91
integer y = 700
integer width = 1394
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean italic = true
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type pb_1 from picturebutton within w_login
integer x = 480
integer y = 1380
integer width = 585
integer height = 168
integer taborder = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean originalsize = true
string picturename = "C:\cs_115\framework\RISORSE\10.5\Login_chage_pwd4.png"
alignment htextalign = right!
end type

event clicked;string			ls_username, ls_cod_utente, ls_password_1, ls_flag_collegato, ls_flag_blocco, ls_cod_azienda
datetime		ldt_data_blocco, ldt_oggi
n_cst_crypto lnv_crypt

ldt_oggi = datetime( date( today()), 00:00:00)

//ls_username = trim(sle_cod_utente.text)
ls_username = dw_login.getitemstring(1, "username")

if isnull(ls_username) or ls_username = "" then
	st_msg_password.text = "Inserire un utente valido."
	dw_login.setfocus()
	dw_login.setcolumn( "username" )
	return
end if

if ls_username <> "CS_SYSTEM" then
	
	select	cod_utente,
				password,
				flag_collegato,
				flag_blocco,
				data_blocco,
				cod_azienda
	into		:ls_cod_utente,
				:ls_password_1,
				:ls_flag_collegato,
				:ls_flag_blocco,
				:ldt_data_blocco,
				:ls_cod_azienda
	from		utenti
	where	username = :ls_username;
	
	if sqlca.sqlcode < 0 then
	  	g_mb.messagebox("Attenzione","Errore in controllo utente: " + sqlca.sqlerrtext, stopsign!, ok!)
		dw_login.setfocus()
		dw_login.setcolumn( "username" )
		return
	elseif sqlca.sqlcode = 100 then
		st_msg_password.text = "Utente non presente in tabella"
		dw_login.setfocus()
		dw_login.setcolumn( "username" )
		return
	end if		
	
	// *** decrypto la vecchia password per confrontarla con quella che inserirà
	
	lnv_crypt = CREATE n_cst_crypto

	lnv_crypt.decryptdata( ls_password_1, ls_password_1)
	
	DESTROY lnv_crypt
	
	s_cs_xx.parametri.parametro_s_2 = ls_password_1

	s_cs_xx.parametri.parametro_b_1 = false

	window_open(w_modifica_pwd,0)

	if s_cs_xx.parametri.parametro_b_1 = true then
		
		update utenti
		set		password = :s_cs_xx.parametri.parametro_s_1,
					data_modifica_pwd = :ldt_oggi
		where	username = :ls_username;
		
		if sqlca.sqlcode < 0 then
		  	g_mb.messagebox("Attenzione","Errore durante l'aggiornamento della password sul db: " + sqlca.sqlerrtext, stopsign!)
			rollback;
			return
		else
			commit;
			g_mb.messagebox( "APICE", "Modifica password avvenuta con successo!", Information!)
			return
		end if
		
	else
	
		return
	
	end if
	
end if
			

end event

type dw_login from datawindow within w_login
event ue_key pbm_dwnkey
integer x = 69
integer y = 760
integer width = 1440
integer height = 360
integer taborder = 10
string title = "none"
string dataobject = "d_login_utente"
boolean border = false
boolean livescroll = true
end type

event ue_key;integer li_risposta
string ls_profilo_corrente

if keyflags = 2 THEN
	IF key = KeyInsert! THEN		
		dw_login.setitem(1, "username", "CS_SYSTEM" )
		dw_login.setitem(1, "password", "" )
		dw_login.setitem(1, "remember", "N" )
		dw_login.setcolumn("password")
	END IF
end if

end event

type p_1 from picture within w_login
integer width = 1586
integer height = 688
boolean originalsize = true
string picturename = "C:\cs_115\framework\RISORSE\logo-login.jpg"
boolean focusrectangle = false
end type

event constructor;//picturename=s_cs_xx.volume + s_cs_xx.risorse + "logo_piccolo.jpg"
picturename=s_cs_xx.volume + s_cs_xx.risorse + "logo-login.jpg"
end event

type cb_ok from uo_cb_ok within w_login
integer x = 914
integer y = 1160
integer width = 594
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Log In"
boolean default = true
end type

