﻿$PBExportHeader$w_login_esterni.srw
$PBExportComments$Finestra Gestione Parametri Azienda
forward
global type w_login_esterni from w_cs_xx_principale
end type
type dw_det from uo_cs_xx_dw within w_login_esterni
end type
type dw_lista from uo_cs_xx_dw within w_login_esterni
end type
end forward

global type w_login_esterni from w_cs_xx_principale
integer width = 3319
integer height = 1752
string title = "Login Esterni"
dw_det dw_det
dw_lista dw_lista
end type
global w_login_esterni w_login_esterni

on w_login_esterni.create
int iCurrent
call super::create
this.dw_det=create dw_det
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det
this.Control[iCurrent+2]=this.dw_lista
end on

on w_login_esterni.destroy
call super::destroy
destroy(this.dw_det)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;string ls_modify

dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_options(sqlca, pcca.null_object, c_default, c_default)

dw_det.set_dw_options(sqlca, dw_lista, c_sharedata + c_scrollparent, c_default)
end event

type dw_det from uo_cs_xx_dw within w_login_esterni
integer x = 846
integer y = 20
integer width = 2377
integer height = 940
integer taborder = 10
string dataobject = "d_login_esterni_dettaglio"
boolean border = false
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_det)
		
	case "b_ricerca_contatto"
		guo_ricerca.uof_ricerca_contatto(dw_det, "cod_contatto")
		
end choose
end event

type dw_lista from uo_cs_xx_dw within w_login_esterni
integer x = 23
integer y = 20
integer width = 800
integer height = 1600
integer taborder = 10
string dataobject = "d_login_esterni_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

