﻿$PBExportHeader$w_mail.srw
$PBExportComments$Finestra Mail
forward
global type w_mail from w_cs_xx_principale
end type
type mle_nota from multilineedit within w_mail
end type
type sle_oggetto from singlelineedit within w_mail
end type
type st_oggetto from statictext within w_mail
end type
type st_nota from statictext within w_mail
end type
type st_allegati from statictext within w_mail
end type
type cb_invia from commandbutton within w_mail
end type
type cbx_lettura from checkbox within w_mail
end type
type cb_browse from commandbutton within w_mail
end type
type mle_allegati from multilineedit within w_mail
end type
type cbx_non_letti from checkbox within w_mail
end type
type dw_folder from u_folder within w_mail
end type
type dw_mail from datawindow within w_mail
end type
type cb_leggi from commandbutton within w_mail
end type
type st_destinatari from statictext within w_mail
end type
type sle_destinatari from singlelineedit within w_mail
end type
type cb_1 from commandbutton within w_mail
end type
end forward

global type w_mail from w_cs_xx_principale
integer width = 1797
integer height = 1568
string title = "Mail"
event ue_open pbm_custom04
mle_nota mle_nota
sle_oggetto sle_oggetto
st_oggetto st_oggetto
st_nota st_nota
st_allegati st_allegati
cb_invia cb_invia
cbx_lettura cbx_lettura
cb_browse cb_browse
mle_allegati mle_allegati
cbx_non_letti cbx_non_letti
dw_folder dw_folder
dw_mail dw_mail
cb_leggi cb_leggi
st_destinatari st_destinatari
sle_destinatari sle_destinatari
cb_1 cb_1
end type
global w_mail w_mail

event ue_open;call w_cs_xx_principale::ue_open;if s_cs_xx.num_livello_mail = 0 then
   g_mb.messagebox("Attenzione", "Gestione mail non abilitata.", exclamation!)
   close(this)
end if
end event

on open;call w_cs_xx_principale::open;windowobject lw_oggetti[]


lw_oggetti[1] = cbx_non_letti
lw_oggetti[2] = cb_leggi
lw_oggetti[3] = dw_mail
dw_folder.fu_assigntab(2, "Leggi", lw_oggetti[])
lw_oggetti[1] = st_destinatari
lw_oggetti[2] = sle_destinatari
lw_oggetti[3] = st_oggetto
lw_oggetti[4] = sle_oggetto
lw_oggetti[5] = st_nota
lw_oggetti[6] = mle_nota
lw_oggetti[7] = st_allegati
lw_oggetti[8] = mle_allegati
lw_oggetti[9] = cb_browse
lw_oggetti[10] = cbx_lettura
lw_oggetti[11] = cb_invia
dw_folder.fu_assigntab(1, "Invia", lw_oggetti[])
dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

postevent("ue_open")
end on

on w_mail.create
int iCurrent
call super::create
this.mle_nota=create mle_nota
this.sle_oggetto=create sle_oggetto
this.st_oggetto=create st_oggetto
this.st_nota=create st_nota
this.st_allegati=create st_allegati
this.cb_invia=create cb_invia
this.cbx_lettura=create cbx_lettura
this.cb_browse=create cb_browse
this.mle_allegati=create mle_allegati
this.cbx_non_letti=create cbx_non_letti
this.dw_folder=create dw_folder
this.dw_mail=create dw_mail
this.cb_leggi=create cb_leggi
this.st_destinatari=create st_destinatari
this.sle_destinatari=create sle_destinatari
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.mle_nota
this.Control[iCurrent+2]=this.sle_oggetto
this.Control[iCurrent+3]=this.st_oggetto
this.Control[iCurrent+4]=this.st_nota
this.Control[iCurrent+5]=this.st_allegati
this.Control[iCurrent+6]=this.cb_invia
this.Control[iCurrent+7]=this.cbx_lettura
this.Control[iCurrent+8]=this.cb_browse
this.Control[iCurrent+9]=this.mle_allegati
this.Control[iCurrent+10]=this.cbx_non_letti
this.Control[iCurrent+11]=this.dw_folder
this.Control[iCurrent+12]=this.dw_mail
this.Control[iCurrent+13]=this.cb_leggi
this.Control[iCurrent+14]=this.st_destinatari
this.Control[iCurrent+15]=this.sle_destinatari
this.Control[iCurrent+16]=this.cb_1
end on

on w_mail.destroy
call super::destroy
destroy(this.mle_nota)
destroy(this.sle_oggetto)
destroy(this.st_oggetto)
destroy(this.st_nota)
destroy(this.st_allegati)
destroy(this.cb_invia)
destroy(this.cbx_lettura)
destroy(this.cb_browse)
destroy(this.mle_allegati)
destroy(this.cbx_non_letti)
destroy(this.dw_folder)
destroy(this.dw_mail)
destroy(this.cb_leggi)
destroy(this.st_destinatari)
destroy(this.sle_destinatari)
destroy(this.cb_1)
end on

type mle_nota from multilineedit within w_mail
integer x = 46
integer y = 420
integer width = 1669
integer height = 500
integer taborder = 40
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type sle_oggetto from singlelineedit within w_mail
integer x = 411
integer y = 220
integer width = 1303
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_oggetto from statictext within w_mail
integer x = 46
integer y = 220
integer width = 343
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Oggetto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_nota from statictext within w_mail
integer x = 46
integer y = 340
integer width = 160
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Nota:"
boolean focusrectangle = false
end type

type st_allegati from statictext within w_mail
integer x = 46
integer y = 960
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Allegati:"
boolean focusrectangle = false
end type

type cb_invia from commandbutton within w_mail
integer x = 1349
integer y = 1340
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Invia"
end type

event clicked;boolean lb_lettura
integer li_contatore, li_inizio, li_posizione
string ls_destinatari[], ls_allegati[], ls_messaggio

uo_outlook luo_outlook


li_contatore = 1
li_inizio = 1
do while true
   li_posizione = pos(sle_destinatari.text, ";", li_inizio)
   if li_posizione > 0 then
      ls_destinatari[li_contatore] = mid(sle_destinatari.text, li_inizio, li_posizione - li_inizio)
      li_contatore ++
      li_inizio = li_posizione + 1
   else
      ls_destinatari[li_contatore] = mid(sle_destinatari.text, li_inizio)
      exit
   end if
loop

li_contatore = 1
li_inizio = 1
do while true
   li_posizione = pos(mle_allegati.text, ";", li_inizio)
   if li_posizione > 0 then
      ls_allegati[li_contatore] = mid(mle_allegati.text, li_inizio, li_posizione - li_inizio)
      li_contatore ++
      li_inizio = li_posizione + 1
   else
      ls_allegati[li_contatore] = mid(mle_allegati.text, li_inizio)
      exit
   end if
loop

if cbx_lettura.checked then
   lb_lettura = true
else
   lb_lettura = false
end if

luo_outlook = create uo_outlook

if luo_outlook.uof_outlook(0,"A",sle_oggetto.text,mle_nota.text,ls_destinatari,ls_allegati,false,ls_messaggio) <> 0 then
	g_mb.messagebox("MAIL",ls_messaggio)
end if

destroy luo_outlook


//s_cs_xx.mail.uf_invia(ls_destinatari[], sle_oggetto.text, mle_nota.text, ls_allegati[], lb_lettura)
end event

type cbx_lettura from checkbox within w_mail
integer x = 46
integer y = 1320
integer width = 663
integer height = 60
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Avviso di Lettura"
boolean lefttext = true
end type

type cb_browse from commandbutton within w_mail
integer x = 274
integer y = 960
integer width = 91
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;string ls_path, ls_file


getfileopenname("Mail - Allegati", ls_path, ls_file, "*.*", "Tutti i files (*.*),*.*")

if not isnull(ls_path) and ls_path <> "" then
   if len(trim(mle_allegati.text)) > 0 then
      mle_allegati.text = mle_allegati.text + ";" + ls_path
   else
      mle_allegati.text = ls_path
   end if
end if
end on

type mle_allegati from multilineedit within w_mail
integer x = 46
integer y = 1040
integer width = 1669
integer height = 240
integer taborder = 60
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cbx_non_letti from checkbox within w_mail
integer x = 46
integer y = 120
integer width = 663
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Non Letti"
boolean checked = true
boolean lefttext = true
end type

type dw_folder from u_folder within w_mail
integer width = 1760
integer height = 1460
integer taborder = 10
end type

type dw_mail from datawindow within w_mail
integer x = 46
integer y = 260
integer width = 1669
integer height = 1160
integer taborder = 50
string dataobject = "d_mail"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_leggi from commandbutton within w_mail
integer x = 1349
integer y = 120
integer width = 366
integer height = 100
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Leggi"
end type

event clicked;//boolean lb_non_letti
//integer li_i
//long ll_num_riga
//
//
//if cbx_non_letti.checked then
//   lb_non_letti = true
//else
//   lb_non_letti = false
//end if
//
//
//s_cs_xx.mail.uf_leggi(lb_non_letti)
//
//
//for li_i = 1 to s_cs_xx.s_mail.num_messaggi
//   if not isnull(s_cs_xx.s_mail.id_messaggio[li_i]) and trim(s_cs_xx.s_mail.id_messaggio[li_i]) <> "" then
//      ll_num_riga = dw_mail.insertrow(dw_mail.rowcount())
////      dw_mail.setitem(ll_num_riga, "id_messaggio", s_cs_xx.s_mail.id_messaggio[li_i])
////      dw_mail.setitem(ll_num_riga, "destinatari", s_cs_xx.s_mail.destinatari[li_i])
//      dw_mail.setitem(ll_num_riga, "oggetto", s_cs_xx.s_mail.oggetto[li_i])
//      dw_mail.setitem(ll_num_riga, "data", s_cs_xx.s_mail.data[li_i])
////      dw_mail.setitem(ll_num_riga, "nota", s_cs_xx.s_mail.nota[li_i])
////      dw_mail.setitem(ll_num_riga, "allegati", s_cs_xx.s_mail.allegati[li_i])
//   end if
//next
end event

type st_destinatari from statictext within w_mail
integer x = 46
integer y = 120
integer width = 343
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Destinatari:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_destinatari from singlelineedit within w_mail
integer x = 411
integer y = 120
integer width = 1303
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_mail
integer x = 960
integer y = 1340
integer width = 366
integer height = 80
integer taborder = 81
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Indirizzi"
end type

event clicked;
//s_cs_xx.mail.uf_elenco_indirizzi()
end event

