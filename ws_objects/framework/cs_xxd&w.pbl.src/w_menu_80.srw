﻿$PBExportHeader$w_menu_80.srw
forward
global type w_menu_80 from window
end type
type p_iconatitolo from picture within w_menu_80
end type
type st_caricamento from statictext within w_menu_80
end type
type uo_cronologia from uo_menu_cronologia within w_menu_80
end type
type p_bloccamenu from picture within w_menu_80
end type
type uo_preferiti from uo_menu_preferiti within w_menu_80
end type
type st_titolo from statictext within w_menu_80
end type
type uo_principale from uo_menu_principale within w_menu_80
end type
type st_resize from statictext within w_menu_80
end type
type str_voce_menu from structure within w_menu_80
end type
end forward

type str_voce_menu from structure
	long		cod_profilo
	long		progressivo
	long		padre
	long		ordinamento
	string		tipo
	string		collegamento
	string		nuovo
	string		modifica
	string		cancella
end type

global type w_menu_80 from window
integer width = 1399
integer height = 2040
boolean border = false
windowtype windowtype = child!
long backcolor = 12632256
integer animationtime = 500
event ue_close ( )
event ue_imposta_controlli ( )
event ue_show_hide ( )
event ue_imposta_window ( )
p_iconatitolo p_iconatitolo
st_caricamento st_caricamento
uo_cronologia uo_cronologia
p_bloccamenu p_bloccamenu
uo_preferiti uo_preferiti
st_titolo st_titolo
uo_principale uo_principale
st_resize st_resize
end type
global w_menu_80 w_menu_80

type variables
string  is_immagini, is_menu_corrente

long 	  il_handle = 0, il_dragx = 0, il_dragy = 0, il_currwidth = 0, il_currheight = 0, il_handle_cutpaste=0

boolean ib_carica_tutto = false, ib_chiudi = false, ib_intranet = false, ib_drag = false, ib_visible = true, ib_autohide = true, ib_cutpaste=false

public:
	long il_background_color_precedente=15132390
	boolean ib_blocca_autohide=false
end variables

forward prototypes
public subroutine wf_apri_chiudi_menu (string as_menu)
public function integer wf_get_width ()
public function integer wf_set_width ()
public subroutine wf_cambia_sfondo (long al_background_color)
public function integer wf_traduci ()
end prototypes

event ue_close();ib_chiudi = true

close(this)
end event

event ue_imposta_controlli();if(ib_visible) Then
	st_resize.x = width - st_resize.width
	st_resize.height=height
	
	st_caricamento.width = width - st_caricamento.x - st_resize.width
	st_caricamento.y = (height/2)
	
	p_bloccaMenu.x = width - p_bloccaMenu.width - st_resize.width
	
	st_titolo.Width = p_bloccaMenu.x - st_titolo.x
	
	uo_principale.width = width - uo_principale.x - st_resize.width
	uo_principale.height=height - uo_principale.y - st_resize.width
	
	uo_preferiti.width = width - uo_preferiti.x - st_resize.width
	uo_preferiti.height=height - uo_preferiti.y - st_resize.width
	
	uo_cronologia.width = width - uo_cronologia.x - st_resize.width
	uo_cronologia.height=height - uo_cronologia.y - st_resize.width
end if
	
end event

event ue_show_hide();if ib_visible then
	ib_visible = false
	hide()
	timer(0)
else
	ib_visible = true
	show()
	timer(1)
	setfocus()
end if

end event

event ue_imposta_window();long ll_x, ll_y, ll_width, ll_height, ll_old_width

ll_x = w_menu_button.x
ll_y = w_cs_xx_mdi.mdi_1.y+w_menu_button.x + 5

move(ll_x,ll_y)

wf_get_width()

ll_width = il_currwidth
ll_old_width = width

ll_height=w_menu_button.y - w_menu_80.y - w_menu_button.x
resize(ll_width, ll_height)
yield()

event  ue_imposta_controlli()
uo_principale.event ue_imposta_controlli()
yield()
uo_preferiti.wf_ridimensiona()
uo_cronologia.wf_ridimensiona()
end event

public subroutine wf_apri_chiudi_menu (string as_menu);/*
* Visualizza o nasconde la finestra con gli oggetti corretti.
* Se nascosto, in base al tipo di menù definisce background color, titolo, e oggetto da caricare.
*/

if ((ib_visible) And (is_menu_corrente=as_menu)) then
	ib_visible = false
	hide()
	timer(0)
else
	uo_principale.Visible=false
	uo_preferiti.Visible=false
	uo_cronologia.Visible=false
	
	choose case as_menu
			
		case "principale"
			backColor=15132390
			st_resize.backColor=15132390
			p_iconaTitolo.pictureName=guo_functions.uof_risorse("menu/icona_utente.png")
			st_Titolo.Text=Upper(uo_principale.is_utente)
			st_Titolo.backColor=15132390
			st_Titolo.pointer="HyperLink!"
			uo_principale.Visible=true
			uo_principale.sle_cerca.setFocus()
			
		case "preferiti"
			backColor=16379084
			p_iconaTitolo.pictureName=guo_functions.uof_risorse("menu/icona_preferiti.png")
			st_resize.backColor=16379084
			st_Titolo.Text="PREFERITI"
			st_Titolo.backColor=16379084
			st_Titolo.pointer=""
			uo_preferiti.Visible=true
			uo_preferiti.dw_menu.setFocus()
			
		case "cronologia"
			backColor=16766899
			p_iconaTitolo.pictureName=guo_functions.uof_risorse("menu/icona_cronologia.png")
			st_resize.backColor=16766899
			st_Titolo.Text="CRONOLOGIA"
			st_Titolo.backColor=16766899
			st_Titolo.pointer=""
			uo_cronologia.Visible=true
			uo_cronologia.dw_menu.setFocus()
			
	end choose
	
	p_bloccaMenu.Visible=false
	p_bloccaMenu.Visible=true
	
	is_menu_corrente=as_menu
	ib_visible = true
	postevent("resize")
	show()
	timer(1)
	setfocus()
	
end if
end subroutine

public function integer wf_get_width ();string ls_width


if registryget(s_cs_xx.chiave_root_user + "applicazione_" +  s_cs_xx.profilocorrente,"RLM",ls_width) = -1 then
	il_currwidth = 1350
end if

il_currwidth = dec(ls_width)

if isnull(il_currwidth) or il_currwidth = 0 then
	il_currwidth = 1350
end if

return 0
end function

public function integer wf_set_width ();if registryset(s_cs_xx.chiave_root_user + "applicazione_" +  s_cs_xx.profilocorrente,"RLM", regstring!, string(width)) = -1 then
	return -1
end if

return 0
end function

public subroutine wf_cambia_sfondo (long al_background_color);this.backcolor=al_background_color
uo_principale.wf_set_background_color(al_background_color)
st_titolo.backcolor=al_background_color
st_resize.backColor=al_background_color
end subroutine

public function integer wf_traduci ();/**
 * stefanop
 * 04/06/2015
 *
 * Traduzione voce di menu
 **/
 
return uo_principale.wf_traduci()
end function

on w_menu_80.create
this.p_iconatitolo=create p_iconatitolo
this.st_caricamento=create st_caricamento
this.uo_cronologia=create uo_cronologia
this.p_bloccamenu=create p_bloccamenu
this.uo_preferiti=create uo_preferiti
this.st_titolo=create st_titolo
this.uo_principale=create uo_principale
this.st_resize=create st_resize
this.Control[]={this.p_iconatitolo,&
this.st_caricamento,&
this.uo_cronologia,&
this.p_bloccamenu,&
this.uo_preferiti,&
this.st_titolo,&
this.uo_principale,&
this.st_resize}
end on

on w_menu_80.destroy
destroy(this.p_iconatitolo)
destroy(this.st_caricamento)
destroy(this.uo_cronologia)
destroy(this.p_bloccamenu)
destroy(this.uo_preferiti)
destroy(this.st_titolo)
destroy(this.uo_principale)
destroy(this.st_resize)
end on

event open;String ls_menuWindow, ls_menuButton, ls_error
Transaction lt_sqlcb;

il_currheight = height

postevent("ue_imposta_window")
yield()

w_menu_80.show()

uo_principale.wf_iniziale()

yield()

st_caricamento.Visible=false
p_bloccamenu.Visible=true
p_iconaTitolo.Visible=true
st_titolo.Visible=true

//Inizializzo la nuova transazione
if(Not (guo_functions.uof_create_transaction_from(sqlca, lt_sqlcb, ls_error))) Then
	g_mb.error(ls_error)
	return
end if

SELECT flag
INTO :ls_menuButton
FROM parametri_azienda_utente
WHERE ((cod_azienda=:s_cs_xx.cod_azienda) AND (cod_utente=:s_cs_xx.cod_utente) AND (cod_parametro='MNB'))
USING lt_sqlcb;

if(ls_menuButton<>"") Then
	if (ls_menuButton="S") Then
		w_menu_button.wf_set_autohide(true)
	else
		w_menu_button.wf_set_autohide(false)
	end if
else
	INSERT INTO parametri_azienda_utente(cod_azienda, cod_utente, cod_parametro, flag)
		VALUES (:s_cs_xx.cod_azienda, :s_cs_xx.cod_utente, 'MNB', 'S')
		USING lt_sqlcb;
	
	COMMIT USING lt_sqlcb;
	
	if (lt_sqlcb.sqlcode <> 0) Then
		g_mb.messagebox("Menù Principale","Errore in inserimento dati in reporitory text.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	w_menu_button.wf_set_autohide(true)
end if

SELECT flag
INTO :ls_menuWindow
FROM parametri_azienda_utente
WHERE ((cod_azienda=:s_cs_xx.cod_azienda) AND (cod_utente=:s_cs_xx.cod_utente) AND (cod_parametro='MNW'))
USING lt_sqlcb;

if(ls_menuWindow<>"") Then
	if (ls_menuWindow="S") Then
		ib_autohide=false
		p_bloccamenu.postevent("clicked")
	else
		ib_autohide=true
		p_bloccamenu.postevent("clicked")
	end if
else
	INSERT INTO parametri_azienda_utente(cod_azienda, cod_utente, cod_parametro, flag)
		VALUES (:s_cs_xx.cod_azienda, :s_cs_xx.cod_utente, 'MNW', 'S')
		USING lt_sqlcb;
	
	COMMIT USING lt_sqlcb;
	
	if (lt_sqlcb.sqlcode <> 0) Then
		g_mb.messagebox("Menù Principale","Errore in inserimento dati in reporitory text.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	ib_autohide=false
	p_bloccamenu.postevent("clicked")
end if

Disconnect using lt_sqlcb;

ib_visible = true
TIMER(3)
end event

event closequery;String ls_menuWindow, ls_menuButton, ls_error;
Transaction lt_sqlcb;

//Inizializzo la nuova transazione
if(Not (guo_functions.uof_create_transaction_from(sqlca, lt_sqlcb, ls_error))) Then
	g_mb.error(ls_error)
	return
end if

if(ib_autohide) Then
	ls_menuWindow="S"
else
	ls_menuWindow="N"
end if

if(w_menu_button.wf_getautohideindipendente()) Then
	ls_menuButton="S"
else
	ls_menuButton="N"
end if

UPDATE parametri_azienda_utente
	SET flag=:ls_menuWindow
	WHERE ((cod_azienda=:s_cs_xx.cod_azienda) AND (cod_utente=:s_cs_xx.cod_utente) AND (cod_parametro='MNW'))
	USING lt_sqlcb;
	
UPDATE parametri_azienda_utente
	SET flag=:ls_menuButton
	WHERE ((cod_azienda=:s_cs_xx.cod_azienda) AND (cod_utente=:s_cs_xx.cod_utente) AND (cod_parametro='MNB'))
	USING lt_sqlcb;

COMMIT USING lt_sqlcb;

Disconnect using lt_sqlcb;

if (lt_sqlcb.sqlcode <> 0) Then
	g_mb.messagebox("Menù Principale","Errore in inserimento dati in reporitory text.~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

//Riporta sul DB la cronologia solo in fase di chiusura...
uo_cronologia.wf_chiudi()

if not(ib_chiudi) then
	return 1
end if
end event

event timer;//st_1.text=string(now())

if ib_autohide and ib_visible and (Not IsValid(w_voci_repository)) And (Not ib_blocca_autohide) Then
	
	long ll_x, ll_y
	
	ll_x = w_cs_xx_mdi.pointerx()
	
	ll_y = w_cs_xx_mdi.pointery()
	
//	st_2.text=">>>" + string(now())
	
	//if ll_x < x or ll_x > (x + width) or ll_y < y or ll_y > (y + height) then
	//Così non considera il "buco" tra la finestra del menù (w_menu_80) e i pulsanti (anch'essi dentro una finestra, w_menu_button)
	if ((ll_x < x) Or (ll_x > (x + width)) Or (ll_y < y) Or (ll_y > (w_menu_button.y + w_menu_button.height))) Then
		postevent("ue_show_hide")
		w_menu_button.wf_apri_chiudi()
	end if
	
end if
end event

event mousemove;TIMER(1)
end event

event show;ib_visible = TRUE
end event

event hide;ib_visible = FALSE
end event

event resize;if not ib_drag then
	
	this.setRedraw(false)
	
	event post ue_imposta_controlli()
	yield()
	uo_principale.wf_ridimensiona()
	uo_preferiti.wf_ridimensiona()
	uo_cronologia.wf_ridimensiona()
	
	w_menu_button.wf_ridimensiona()
	
	if ib_visible then
		il_currwidth = width
		il_currheight = height
	end if
	
	this.setRedraw(true)
	
end if
end event

type p_iconatitolo from picture within w_menu_80
boolean visible = false
integer x = 32
integer y = 48
integer width = 73
integer height = 64
boolean originalsize = true
boolean focusrectangle = false
end type

type st_caricamento from statictext within w_menu_80
integer x = 23
integer y = 980
integer width = 1353
integer height = 84
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Caricamento..."
alignment alignment = center!
boolean focusrectangle = false
end type

type uo_cronologia from uo_menu_cronologia within w_menu_80
boolean visible = false
integer x = 23
integer y = 156
integer height = 1860
integer taborder = 20
long il_background_color = 16766899
end type

on uo_cronologia.destroy
call uo_menu_cronologia::destroy
end on

type p_bloccamenu from picture within w_menu_80
boolean visible = false
integer x = 1266
integer y = 24
integer width = 110
integer height = 96
boolean focusrectangle = false
boolean map3dcolors = true
end type

event clicked;ib_autohide=Not ib_autohide

if (ib_autohide) Then
	p_bloccaMenu.picturename=guo_functions.uof_risorse("menu/menu_sbloccato.png")
	p_bloccaMenu.PowerTipText="Blocca menù"
	w_menu_button.wf_autohide_dipendente(true)
else
	p_bloccaMenu.picturename=guo_functions.uof_risorse("menu/menu_bloccato.png")
	p_bloccaMenu.PowerTipText="Sblocca menù"
	w_menu_button.wf_autohide_dipendente(false)
end if

yield()
end event

type uo_preferiti from uo_menu_preferiti within w_menu_80
boolean visible = false
integer x = 23
integer y = 156
integer height = 1860
integer taborder = 30
long il_background_color = 15853773
end type

on uo_preferiti.destroy
call uo_menu_preferiti::destroy
end on

type st_titolo from statictext within w_menu_80
boolean visible = false
integer x = 137
integer y = 48
integer width = 1211
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Titolo"
boolean focusrectangle = false
end type

event clicked;if(is_menu_corrente="principale") Then
	w_cs_xx_mdi.im_menu_cs.m_db.m_cambia_utente.postevent("clicked")
end if
end event

type uo_principale from uo_menu_principale within w_menu_80
boolean visible = false
integer x = 23
integer y = 156
integer height = 1860
integer taborder = 30
long il_background_color = 15132390
end type

on uo_principale.destroy
call uo_menu_principale::destroy
end on

type st_resize from statictext within w_menu_80
event ue_lbuttondown pbm_lbuttondown
event ue_mousemove pbm_mousemove
event ue_lbuttonup pbm_lbuttonup
integer x = 1376
integer width = 23
integer height = 2040
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string pointer = "SizeWE!"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

event ue_lbuttondown;ib_drag = true

il_dragx = xpos
il_dragy = ypos
end event

event ue_mousemove;if ib_drag then
	
	long ll_newwidth
	
	ll_newwidth = parent.width + (xpos - il_dragx)
	
	if ll_newwidth >= 350  and ll_newwidth <= (w_cs_xx_mdi.width - 10) then
		parent.width = ll_newwidth
	end if
	
	il_dragx = xpos
	
else
	
	parent.event mousemove(flags, xpos, ypos)
	
end if
end event

event ue_lbuttonup;ib_drag = false

il_dragx = 0
il_dragy = 0

wf_set_width()
parent.postevent("resize")
end event

