﻿$PBExportHeader$w_menu_80_menu_principale.srw
forward
global type w_menu_80_menu_principale from window
end type
type pb_bloccamenu from picturebutton within w_menu_80_menu_principale
end type
type dw_ricerca from datawindow within w_menu_80_menu_principale
end type
type sle_cerca from singlelineedit within w_menu_80_menu_principale
end type
type sle_azienda from singlelineedit within w_menu_80_menu_principale
end type
type sle_utente from singlelineedit within w_menu_80_menu_principale
end type
type pb_aziende from picturebutton within w_menu_80_menu_principale
end type
type pb_utenti from picturebutton within w_menu_80_menu_principale
end type
type pb_trova from picturebutton within w_menu_80_menu_principale
end type
type gb_azienda from groupbox within w_menu_80_menu_principale
end type
type gb_utente from groupbox within w_menu_80_menu_principale
end type
type st_resize from statictext within w_menu_80_menu_principale
end type
type tv_menu from treeview within w_menu_80_menu_principale
end type
type str_voce_menu from structure within w_menu_80_menu_principale
end type
end forward

type str_voce_menu from structure
	long		cod_profilo
	long		progressivo
	long		padre
	long		ordinamento
	string		tipo
	string		collegamento
	string		nuovo
	string		modifica
	string		cancella
end type

global type w_menu_80_menu_principale from window
integer width = 1358
integer height = 2112
windowtype windowtype = child!
long backcolor = 12632256
integer animationtime = 500
event ue_imposta_window ( )
event ue_close ( )
event ue_news ( )
event ue_set_mdi_workspace ( boolean ab_menu )
event ue_imposta_controlli ( long al_diffx,  long al_diffy )
event ue_show_hide ( )
event ue_ridimensiona_contenuto_dw ( )
pb_bloccamenu pb_bloccamenu
dw_ricerca dw_ricerca
sle_cerca sle_cerca
sle_azienda sle_azienda
sle_utente sle_utente
pb_aziende pb_aziende
pb_utenti pb_utenti
pb_trova pb_trova
gb_azienda gb_azienda
gb_utente gb_utente
st_resize st_resize
tv_menu tv_menu
end type
global w_menu_80_menu_principale w_menu_80_menu_principale

type variables
string  is_immagini

long 	  il_handle = 0, il_dragx = 0, il_dragy = 0, il_currwidth = 0, il_currheight = 0, il_handle_cutpaste=0, il_cod_profilo[], il_CodProfiloRicerca[],  il_CodMenu[]

boolean ib_carica_tutto = false, ib_chiudi = false, ib_intranet = false, ib_drag = false, ib_visible = true, ib_autohide = true, ib_cutpaste=false

//F.Baschirotto 25/10/2012
//Per la ricerca
//ii_CodMenu: codice dell'elemento all'interno della DW che contiene i dati del menu.
//I due contatori servono da indici. Sono istanze perchè la funzione che genera il menu
//può essere utilizzata come ricorsiva e quindi succederebbe un casino...
integer ii_LivelloMenu[], ii_contCodMenu=1, ii_LivelloAttuale=0;
end variables

forward prototypes
public function integer wf_rinomina ()
public function integer wf_trova (string as_ricerca, long al_handle)
public function integer wf_set_width ()
public function integer wf_modifica_link ()
public function integer wf_flag_cancella (boolean ab_checked)
public function integer wf_flag_modifica (boolean ab_checked)
public function integer wf_flag_nuovo (boolean ab_checked)
public function integer wf_news ()
public function integer wf_cancella_menu (long al_cod_profilo, long al_prog_padre)
public function integer wf_duplica_profilo ()
public function integer wf_nuova_pagina ()
public function integer wf_nuovo_link ()
public function integer wf_nuova_window (ref datawindow adw_datawindow, long al_handle)
public function integer wf_nuovo_profilo ()
public function integer wf_inizio (boolean ab_ricorsivo)
public function integer wf_nuova_cartella ()
public function integer wf_elimina ()
public function integer wf_drag_drop (ref treeview atv_tree, long al_drop, long al_drag, boolean ab_elimina)
public function integer wf_apri_window ()
public function integer wf_trasferisci_ramo (treeview atv_tree, treeviewitem atv_drag, treeviewitem atv_drop, long al_posizione, boolean ab_elimina)
public function integer wf_get_width ()
public function integer wf_taglia_profilo ()
public function integer wf_copia_profilo ()
public function integer wf_incolla_profilo ()
private function integer wf_incolla_profilo (long al_drop, long al_drag)
public subroutine wf_carica_elemento (ref datastore ads_elementi, ref long al_cod_profilo, long al_padre, long al_prog_padre, ref long al_count_elementi)
public function integer wf_carica_menu (long al_cod_profilo, long al_prog_padre, long al_handle_padre)
public subroutine wf_collapseall (long ll_elemento)
public function long wf_seleziona_elemento (integer ai_cod_menu, long al_cod_profilo)
end prototypes

event ue_imposta_window();long ll_x, ll_y, ll_width, ll_height, ll_old_width

ll_x = 0
ll_y = w_cs_xx_mdi.mdi_1.y + 5

move(ll_x,ll_y)

ll_width = il_currwidth
ll_old_width = width

ll_height = w_cs_xx_mdi.mdi_1.height - 5
resize(ll_width, ll_height)

event  ue_imposta_controlli(ll_width - ll_old_width,0)
postevent("ue_ridimensiona_contenuto_dw")


end event

event ue_close();ib_chiudi = true

close(this)
end event

event ue_news();wf_news()
end event

event ue_set_mdi_workspace(boolean ab_menu);// resize del workspace MDI

long ll_menu_width


if ab_menu then
	ll_menu_width = this.width
else
	ll_menu_width = 0
end if

//w_cs_xx_mdi.mdi_1.x = ll_menu_width
	
//w_cs_xx_mdi.mdi_1.width = w_cs_xx_mdi.width - ll_menu_width
end event

event ue_imposta_controlli(long al_diffx, long al_diffy);if ib_visible then

	tv_menu.width += al_diffx
	tv_menu.height += al_diffy
	
	gb_azienda.width += al_diffx
	gb_azienda.y += al_diffy
	
	gb_utente.width += al_diffx
	gb_utente.y += al_diffy
	
	sle_azienda.width += al_diffx
	sle_azienda.y += al_diffy
	
	sle_utente.width += al_diffx
	sle_utente.y += al_diffy
	
	sle_cerca.width += al_diffx
	sle_cerca.y += al_diffy
	
	st_resize.x += al_diffx
	
	st_resize.height += al_diffy
	
	dw_ricerca.width += al_diffx
	dw_ricerca.height += al_diffy
	
	pb_bloccaMenu.y += al_diffy
	pb_bloccaMenu.x += al_diffx
	
	pb_aziende.y += al_diffy
	
	pb_trova.y += al_diffy
	
	pb_utenti.y += al_diffy
	
end if
end event

event ue_show_hide();if ib_visible then
	ib_visible = false
	hide()
	timer(0)
else
	ib_visible = true
	show()
	timer(1)
	setfocus()
	tv_menu.setfocus()
end if

end event

event ue_ridimensiona_contenuto_dw();integer li_larghezza

// -190 perchè c'è la colonna con l'immagine, il Bordo e la Vertical Scroll Bar
li_larghezza=dw_ricerca.width - dw_ricerca.x - st_resize.Width - 190
if(li_larghezza > 500) Then
	dw_ricerca.Modify("risultati.Width="+String(li_larghezza))
	dw_ricerca.Modify("p_pulsanteTornaAlMenu.x="+String(Integer(dw_ricerca.Describe("PulsanteAlbero.x"))+4))
	dw_ricerca.Modify("p_pulsanteAlberoFinestra.x="+String(Integer(dw_ricerca.Describe("PulsanteAlbero.x"))+4))
	dw_ricerca.Modify("p_pulsanteAlberoNodo.x="+String(Integer(dw_ricerca.Describe("PulsanteAlbero.x"))+4))
end if
end event

public function integer wf_rinomina ();tv_menu.editlabels = true

tv_menu.editlabel(il_handle)

return 0
end function

public function integer wf_trova (string as_ricerca, long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tv_menu.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_menu.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(as_ricerca),1) > 0 then
		tv_menu.setfocus()
		tv_menu.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(as_ricerca,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_handle = tv_menu.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tv_menu.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(as_ricerca),1) > 0 then
		tv_menu.setfocus()
		tv_menu.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(as_ricerca,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_padre = al_handle

do
	
	ll_padre = tv_menu.finditem(parenttreeitem!,ll_padre)

	if ll_padre > 0 then
		
		ll_handle = tv_menu.finditem(nexttreeitem!,ll_padre)
		
		if ll_handle > 0 then
		
			tv_menu.getitem(ll_handle,ltv_item)
			
			if pos(upper(ltv_item.label),upper(as_ricerca),1) > 0 then
				tv_menu.setfocus()
				tv_menu.selectitem(ll_handle)
				il_handle = ll_handle
				return 0
			else
				if wf_trova(as_ricerca,ll_handle) = 0 then
					return 0
				else
					return 100
				end if
			end if
			
		end if
		
	end if

loop while ll_padre > 0

return 100
end function

public function integer wf_set_width ();if registryset(s_cs_xx.chiave_root_user + "applicazione_" +  s_cs_xx.profilocorrente,"RLM", regstring!, string(width)) = -1 then
	return -1
end if

return 0
end function

public function integer wf_modifica_link ();treeviewitem ltv_item

str_voce_menu lstr_voce


tv_menu.getitem(il_handle,ltv_item)

lstr_voce = ltv_item.data

s_cs_xx.parametri.parametro_s_1 = lstr_voce.collegamento

window_open(w_nuovo_url,0)

if s_cs_xx.parametri.parametro_d_1 = -1 then
	setnull(s_cs_xx.parametri.parametro_s_1)
	return -1
end if

if s_cs_xx.parametri.parametro_s_1 <> lstr_voce.collegamento then

	lstr_voce.collegamento = s_cs_xx.parametri.parametro_s_1
	
	setnull(s_cs_xx.parametri.parametro_s_1)
	
	update menu
	set	 collegamento = :lstr_voce.collegamento
	where  cod_profilo = :lstr_voce.cod_profilo and
			 progressivo_menu = :lstr_voce.progressivo;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Menu Principale","Errore in aggiornamento link: " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return -1
	end if
	
	commit;
	
	ltv_item.data = lstr_voce
	
	tv_menu.setitem(il_handle,ltv_item)

end if

return 0
end function

public function integer wf_flag_cancella (boolean ab_checked);string ls_flag

treeviewitem ltv_item

str_voce_menu lstr_voce


tv_menu.getitem(il_handle,ltv_item)

lstr_voce = ltv_item.data

if ab_checked then
	ls_flag = "N"
else
	ls_flag = "S"
end if

update menu
set    flag_cancella = :ls_flag
where  cod_profilo = :lstr_voce.cod_profilo and
		 progressivo_menu = :lstr_voce.progressivo;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in modifica flag nuovo: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

lstr_voce.cancella = ls_flag

ltv_item.data = lstr_voce

if tv_menu.setitem(il_handle,ltv_item) = -1 then
	g_mb.messagebox("Menu Principale","Errore in aggiornamento albero menu",stopsign!)
	rollback;
	return -1
end if

commit;

return 0
end function

public function integer wf_flag_modifica (boolean ab_checked);string ls_flag

treeviewitem ltv_item

str_voce_menu lstr_voce


tv_menu.getitem(il_handle,ltv_item)

lstr_voce = ltv_item.data

if ab_checked then
	ls_flag = "N"
else
	ls_flag = "S"
end if

update menu
set    flag_modifica = :ls_flag
where  cod_profilo = :lstr_voce.cod_profilo and
		 progressivo_menu = :lstr_voce.progressivo;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in modifica flag nuovo: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

lstr_voce.modifica = ls_flag

ltv_item.data = lstr_voce

if tv_menu.setitem(il_handle,ltv_item) = -1 then
	g_mb.messagebox("Menu Principale","Errore in aggiornamento albero menu",stopsign!)
	rollback;
	return -1
end if

commit;

return 0
end function

public function integer wf_flag_nuovo (boolean ab_checked);string ls_flag

treeviewitem ltv_item

str_voce_menu lstr_voce


tv_menu.getitem(il_handle,ltv_item)

lstr_voce = ltv_item.data

if ab_checked then
	ls_flag = "N"
else
	ls_flag = "S"
end if

update menu
set    flag_nuovo = :ls_flag
where  cod_profilo = :lstr_voce.cod_profilo and
		 progressivo_menu = :lstr_voce.progressivo;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in modifica flag nuovo: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

lstr_voce.nuovo = ls_flag

ltv_item.data = lstr_voce

if tv_menu.setitem(il_handle,ltv_item) = -1 then
	g_mb.messagebox("Menu Principale","Errore in aggiornamento albero menu",stopsign!)
	rollback;
	return -1
end if

commit;

return 0
end function

public function integer wf_news ();string ls_news


if registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente,"NWS",ls_news) = -1 then
	ls_news = "N"
	return -1
end if

if upper(ls_news) = "S" then
	window_open(w_news,-1)
end if

return 0
end function

public function integer wf_cancella_menu (long al_cod_profilo, long al_prog_padre);long   ll_prog_voce, ll_i

datastore lds_menu


lds_menu = create datastore

lds_menu.dataobject = "ds_menu"

if lds_menu.settransobject(sqlca) < 0 then
	g_mb.messagebox("Menu Principale","Errore in impostazione transazione:~nProfilo " + string(al_cod_profilo) + &
				  "~nPadre " + string(al_prog_padre),stopsign!)
	destroy lds_menu
	return -1
end if

if lds_menu.retrieve({al_cod_profilo},al_prog_padre,"XXX") < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura voci:~nProfilo " + string(al_cod_profilo) + &
				  "~nPadre " + string(al_prog_padre),stopsign!)
	destroy lds_menu
	return -1
end if

for ll_i = 1 to lds_menu.rowcount()
	
	ll_prog_voce = lds_menu.getitemnumber(ll_i,"progressivo_menu")
	
	if wf_cancella_menu(al_cod_profilo,ll_prog_voce) = -1 then
		destroy lds_menu
		return -1
	end if
	
	delete
	from   menu
	where  cod_profilo = :al_cod_profilo and
			 progressivo_menu = :ll_prog_voce;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Menu Principale","Errore in cancellazione voce " + string(ll_prog_voce) + &
					  " del profilo " + string(al_cod_profilo) + ": " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
next

destroy lds_menu

return 0
end function

public function integer wf_duplica_profilo ();if g_mb.messagebox("Menu Principale","Duplicare il profilo selezionato?",question!,yesno!,2) = 2 then
	return 0
end if

setpointer(hourglass!)

long   ll_cod_profilo, ll_handle, ll_prog_menu, ll_prog_padre, ll_ordinamento

string ls_descrizione, ls_tipo_voce, ls_des_voce, ls_collegamento, ls_nuovo, ls_modifica, ls_cancella

treeviewitem ltv_item

str_voce_menu lstr_voce

tv_menu.getitem(il_handle,ltv_item)

lstr_voce = ltv_item.data

ls_descrizione = "Copia di " + ltv_item.label

select max(cod_profilo)
into   :ll_cod_profilo
from   profili;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura massimo progressivo profilo: " + sqlca.sqlerrtext, stopsign!)
	setpointer(arrow!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_cod_profilo) then
	ll_cod_profilo = 0
end if

ll_cod_profilo ++

insert
into   profili
		 (cod_profilo,
		 des_profilo)
values (:ll_cod_profilo,
		 :ls_descrizione);
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in duplicazione profilo: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	setpointer(arrow!)
	return -1
end if

declare voci cursor for
select  progressivo_menu,
		  progressivo_padre,
		  ordinamento,
		  flag_tipo_voce,
		  descrizione_voce,
		  collegamento,
		  flag_nuovo,
		  flag_modifica,
		  flag_cancella
from    menu
where   cod_profilo = :lstr_voce.cod_profilo
order by progressivo_menu;

open voci;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore nella open del cursore voci: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	setpointer(arrow!)
	return -1
end if

do while true
	
	fetch voci
	into  :ll_prog_menu,
			:ll_prog_padre,
			:ll_ordinamento,
			:ls_tipo_voce,
			:ls_des_voce,
			:ls_collegamento,
			:ls_nuovo,
			:ls_modifica,
			:ls_cancella;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Menu Principale","Errore nella fetch del cursore voci: " + sqlca.sqlerrtext,stopsign!)
		close voci;
		rollback;		
		setpointer(arrow!)		
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	insert
	into   menu
			 (cod_profilo,
			 progressivo_menu,
			 progressivo_padre,
		  	 ordinamento,
		  	 flag_tipo_voce,
		  	 descrizione_voce,
		  	 collegamento,
		  	 flag_nuovo,
		  	 flag_modifica,
		  	 flag_cancella)
	values (:ll_cod_profilo,
			 :ll_prog_menu,
			 :ll_prog_padre,
			 :ll_ordinamento,
			 :ls_tipo_voce,
			 :ls_des_voce,
			 :ls_collegamento,
			 :ls_nuovo,
			 :ls_modifica,
			 :ls_cancella);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Menu Principale","Errore in duplicazione voce profilo: " + sqlca.sqlerrtext,stopsign!)
		close voci;
		rollback;
		setpointer(arrow!)
		return -1
	end if
	
loop

close voci;

commit;

setpointer(arrow!)

lstr_voce.cod_profilo = ll_cod_profilo
	
lstr_voce.progressivo = 0

lstr_voce.padre = 0

lstr_voce.ordinamento = 0
	
lstr_voce.tipo = "M"

ltv_item.label = ls_descrizione

ltv_item.bold = true

lstr_voce.collegamento = ""

lstr_voce.nuovo = "N"

lstr_voce.modifica = "N"

lstr_voce.cancella = "N"

ltv_item.expanded = true

ltv_item.selected = false

ltv_item.children = false

ltv_item.data = lstr_voce

ltv_item.pictureindex = 6

ltv_item.selectedpictureindex = 6

ll_handle = tv_menu.insertitemlast(0,ltv_item)

wf_carica_menu(ll_cod_profilo,lstr_voce.progressivo,ll_handle)

tv_menu.selectitem(ll_handle)

il_handle = ll_handle

return 0
end function

public function integer wf_nuova_pagina ();string ls_url

long 	 ll_progressivo, ll_handle, ll_ordinamento

treeviewitem ltv_padre, ltv_item

str_voce_menu lstr_padre, lstr_voce


s_cs_xx.parametri.parametro_s_1 = ""

window_open(w_nuovo_url,0)

if s_cs_xx.parametri.parametro_d_1 = -1 then
	setnull(s_cs_xx.parametri.parametro_s_1)
	return -1
end if

ls_url = s_cs_xx.parametri.parametro_s_1

setnull(s_cs_xx.parametri.parametro_s_1)

tv_menu.getitem(il_handle,ltv_padre)

lstr_padre = ltv_padre.data

select max(progressivo_menu)
into   :ll_progressivo
from   menu
where  cod_profilo = :lstr_padre.cod_profilo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura massimo progressivo voce del profilo " + &
				  string(lstr_padre.cod_profilo) + ": " + sqlca.sqlerrtext, stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) then
	ll_progressivo = 0
end if

ll_progressivo ++

select max(ordinamento)
into   :ll_ordinamento
from   menu
where  cod_profilo = :lstr_padre.cod_profilo and
		 progressivo_padre = :lstr_padre.progressivo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura massimo ordinamento voce del profilo " + &
				  string(lstr_padre.cod_profilo) + " ramo " + string(lstr_padre.progressivo) + &
				  ": " + sqlca.sqlerrtext, stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_ordinamento) then
	ll_ordinamento = 0
end if

ll_ordinamento ++

insert
into   menu
		 (cod_profilo,
		 progressivo_menu,
		 progressivo_padre,
		 ordinamento,
		 flag_tipo_voce,
		 descrizione_voce,
		 collegamento,
		 flag_nuovo,
		 flag_modifica,
		 flag_cancella)
values (:lstr_padre.cod_profilo,
		 :ll_progressivo,
		 :lstr_padre.progressivo,
		 :ll_ordinamento,
		 'P',
		 :ls_url,
		 :ls_url,
		 'N',
		 'N',
		 'N');
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in inserimento nuovo link: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

commit;

lstr_voce.cod_profilo = lstr_padre.cod_profilo
	
lstr_voce.progressivo = ll_progressivo

lstr_voce.padre = lstr_padre.progressivo

lstr_voce.ordinamento = ll_ordinamento
	
lstr_voce.tipo = "P"

ltv_item.label = ls_url

ltv_item.bold = false

lstr_voce.collegamento = ls_url

lstr_voce.nuovo = "N"

lstr_voce.modifica = "N"

lstr_voce.cancella = "N"

ltv_item.expanded = false

ltv_item.selected = false

ltv_item.children = false

ltv_item.data = lstr_voce

ltv_item.pictureindex = 7

ltv_item.selectedpictureindex = 7

ll_handle = tv_menu.insertitemlast(il_handle,ltv_item)

tv_menu.selectitem(ll_handle)

il_handle = ll_handle

tv_menu.editlabels = true

tv_menu.editlabel(ll_handle)

return 0
end function

public function integer wf_nuovo_link ();string ls_url

long 	 ll_progressivo, ll_handle, ll_ordinamento

treeviewitem ltv_padre, ltv_item

str_voce_menu lstr_padre, lstr_voce


s_cs_xx.parametri.parametro_s_1 = ""

window_open(w_nuovo_url,0)

if s_cs_xx.parametri.parametro_d_1 = -1 then
	setnull(s_cs_xx.parametri.parametro_s_1)
	return -1
end if

ls_url = s_cs_xx.parametri.parametro_s_1

setnull(s_cs_xx.parametri.parametro_s_1)

tv_menu.getitem(il_handle,ltv_padre)

lstr_padre = ltv_padre.data

select max(progressivo_menu)
into   :ll_progressivo
from   menu
where  cod_profilo = :lstr_padre.cod_profilo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura massimo progressivo voce del profilo " + &
				  string(lstr_padre.cod_profilo) + ": " + sqlca.sqlerrtext, stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) then
	ll_progressivo = 0
end if

ll_progressivo ++

select max(ordinamento)
into   :ll_ordinamento
from   menu
where  cod_profilo = :lstr_padre.cod_profilo and
		 progressivo_padre = :lstr_padre.progressivo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura massimo ordinamento voce del profilo " + &
				  string(lstr_padre.cod_profilo) + " ramo " + string(lstr_padre.progressivo) + &
				  ": " + sqlca.sqlerrtext, stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_ordinamento) then
	ll_ordinamento = 0
end if

ll_ordinamento ++

insert
into   menu
		 (cod_profilo,
		 progressivo_menu,
		 progressivo_padre,
		 ordinamento,
		 flag_tipo_voce,
		 descrizione_voce,
		 collegamento,
		 flag_nuovo,
		 flag_modifica,
		 flag_cancella)
values (:lstr_padre.cod_profilo,
		 :ll_progressivo,
		 :lstr_padre.progressivo,
		 :ll_ordinamento,
		 'C',
		 :ls_url,
		 :ls_url,
		 'N',
		 'N',
		 'N');
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in inserimento nuovo link: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

commit;

lstr_voce.cod_profilo = lstr_padre.cod_profilo
	
lstr_voce.progressivo = ll_progressivo

lstr_voce.padre = lstr_padre.progressivo

lstr_voce.ordinamento = ll_ordinamento
	
lstr_voce.tipo = "C"

ltv_item.label = ls_url

ltv_item.bold = false

lstr_voce.collegamento = ls_url

lstr_voce.nuovo = "N"

lstr_voce.modifica = "N"

lstr_voce.cancella = "N"

ltv_item.expanded = false

ltv_item.selected = false

ltv_item.children = false

ltv_item.data = lstr_voce

ltv_item.pictureindex = 4

ltv_item.selectedpictureindex = 4

ll_handle = tv_menu.insertitemlast(il_handle,ltv_item)

tv_menu.selectitem(ll_handle)

il_handle = ll_handle

tv_menu.editlabels = true

tv_menu.editlabel(ll_handle)

return 0
end function

public function integer wf_nuova_window (ref datawindow adw_datawindow, long al_handle);treeviewitem ltv_padre

str_voce_menu lstr_padre


tv_menu.selectitem(al_handle)

il_handle = al_handle

tv_menu.getitem(al_handle,ltv_padre)

lstr_padre = ltv_padre.data

if lstr_padre.tipo <> "M" then
	return -1
end if

long   ll_progressivo, ll_handle, ll_ordinamento

string ls_collegamento, ls_descrizione

treeviewitem ltv_item

str_voce_menu lstr_voce
	
ls_collegamento = adw_datawindow.getitemstring(adw_datawindow.getrow(),"collegamento")

ls_descrizione = adw_datawindow.getitemstring(adw_datawindow.getrow(),"descrizione")

select max(progressivo_menu)
into   :ll_progressivo
from   menu
where  cod_profilo = :lstr_padre.cod_profilo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura massimo progressivo voce del profilo " + &
				  string(lstr_padre.cod_profilo) + ": " + sqlca.sqlerrtext, stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) then
	ll_progressivo = 0
end if

ll_progressivo ++

select max(ordinamento)
into   :ll_ordinamento
from   menu
where  cod_profilo = :lstr_padre.cod_profilo and
		 progressivo_padre = :lstr_padre.progressivo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura massimo ordinamento voce del profilo " + &
				  string(lstr_padre.cod_profilo) + " ramo " + string(lstr_padre.progressivo) + &
				  ": " + sqlca.sqlerrtext, stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_ordinamento) then
	ll_ordinamento = 0
end if

ll_ordinamento ++

insert
into   menu
		 (cod_profilo,
		 progressivo_menu,
		 progressivo_padre,
		 ordinamento,
		 flag_tipo_voce,
		 descrizione_voce,
		 collegamento,
		 flag_nuovo,
		 flag_modifica,
		 flag_cancella)
values (:lstr_padre.cod_profilo,
		 :ll_progressivo,
		 :lstr_padre.progressivo,
		 :ll_ordinamento,
		 'W',
		 :ls_descrizione,
		 :ls_collegamento,
		 'S',
		 'S',
		 'S');
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in inserimento nuova window: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

commit;

lstr_voce.cod_profilo = lstr_padre.cod_profilo
	
lstr_voce.progressivo = ll_progressivo

lstr_voce.padre = lstr_padre.progressivo

lstr_voce.ordinamento = ll_ordinamento
	
lstr_voce.tipo = "W"

ltv_item.label = ls_descrizione

ltv_item.bold = false

lstr_voce.collegamento = ls_collegamento

lstr_voce.nuovo = "S"

lstr_voce.modifica = "S"

lstr_voce.cancella = "S"

ltv_item.expanded = false

ltv_item.selected = false

ltv_item.children = false

ltv_item.data = lstr_voce

ltv_item.pictureindex = 3

ltv_item.selectedpictureindex = 3

ll_handle = tv_menu.insertitemlast(il_handle,ltv_item)

tv_menu.selectitem(ll_handle)

il_handle = ll_handle

tv_menu.editlabels = true

tv_menu.editlabel(ll_handle)

return 0
end function

public function integer wf_nuovo_profilo ();long ll_handle, ll_cod_profilo

treeviewitem ltv_item

str_voce_menu lstr_voce


select max(cod_profilo)
into   :ll_cod_profilo
from   profili;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura massimo progressivo profilo: " + sqlca.sqlerrtext, stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_cod_profilo) then
	ll_cod_profilo = 0
end if

ll_cod_profilo ++

insert
into   profili
		 (cod_profilo,
		 des_profilo)
values (:ll_cod_profilo,
		 'Nuovo Profilo');
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in inserimento nuovo profilo: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

commit;

lstr_voce.cod_profilo = ll_cod_profilo
	
lstr_voce.progressivo = 0

lstr_voce.padre = 0

lstr_voce.ordinamento = 0
	
lstr_voce.tipo = "M"

ltv_item.label = "Nuovo Profilo"

ltv_item.bold = true

lstr_voce.collegamento = ""

lstr_voce.nuovo = "N"

lstr_voce.modifica = "N"

lstr_voce.cancella = "N"

ltv_item.expanded = false

ltv_item.selected = false

ltv_item.children = false

ltv_item.data = lstr_voce

ltv_item.pictureindex = 6

ltv_item.selectedpictureindex = 6

ll_handle = tv_menu.insertitemlast(0,ltv_item)

tv_menu.selectitem(ll_handle)

il_handle = ll_handle

tv_menu.editlabels = true

tv_menu.editlabel(ll_handle)

return 0
end function

public function integer wf_inizio (boolean ab_ricorsivo);long   ll_handle, ll_cod_profilo, ll_i, ll_pos

string ls_admin, ls_sql, ls_nome_cognome, ls_username, ls_des_profilo, ls_des_azienda

treeviewitem ltv_item

str_voce_menu lstr_voce

dw_ricerca.Visible=false

if s_cs_xx.cod_utente = "CS_SYSTEM" then
	
	sle_utente.text = s_cs_xx.cod_utente
	
else
	
	select nome_cognome,
			 username	
	into   :ls_nome_cognome,
			 :ls_username
	from   utenti
	where  cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Menu Principale","Errore in lettura nome e cognome utente: " + sqlca.sqlerrtext,stopsign!)
		sle_utente.text = ""
	end if
	
	if not isnull(ls_nome_cognome) and trim(ls_nome_cognome) <> "" then
		sle_utente.text = ls_nome_cognome
	else
		sle_utente.text = ls_username
	end if
	
end if

select rag_soc_1
into   :ls_des_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;
	
if sqlca.sqlcode <> 0 or isnull(ls_des_azienda) then
	g_mb.messagebox("Menu Principale","Errore in lettura ragione sociale azienda: " + sqlca.sqlerrtext,stopsign!)
	sle_azienda.text = ""
else
	
	sle_azienda.text = ls_des_azienda
	
	ll_pos = pos(w_cs_xx_mdi.title," - Azienda: ",1)
	
	if ll_pos > 0 then
		w_cs_xx_mdi.title = left(w_cs_xx_mdi.title,ll_pos - 1) + " - Azienda: " + ls_des_azienda
	else
		w_cs_xx_mdi.title += " - Azienda: " + ls_des_azienda
	end if
	
end if

if s_cs_xx.admin then
	
	tv_menu.dragauto = true
	
	ls_sql = "select cod_profilo " + &
				"from   profili " + &
				"order by cod_profilo "

else
	
	tv_menu.dragauto = false
	
	ls_sql = "select cod_profilo " + &
				"from   aziende_utenti " + &
				"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
				"		  cod_utente = '" + s_cs_xx.cod_utente + "'"
	
end if

setpointer(hourglass!)

do
	
	ll_handle = tv_menu.finditem(roottreeitem!,0)
	
	if ll_handle > 0 then
		tv_menu.deleteitem(ll_handle)
	end if
	
loop while ll_handle <> -1

declare profili dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic profili;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura profili~nErrore nella open del cursore profili: " + &
				  sqlca.sqlerrtext,stopsign!)
	setpointer(arrow!)
	return -1
end if

ll_i=1

do while true

	fetch profili
	into  :ll_cod_profilo;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Menu Principale","Errore in lettura profili~nErrore nella fetch del cursore profili: " + &
					  sqlca.sqlerrtext,stopsign!)
		close profili;
		setpointer(arrow!)
		return -1
	elseif sqlca.sqlcode = 100 then
		close profili;
		exit
	end if
	
	il_cod_profilo[ll_i]=ll_cod_profilo
	ll_i++
	
	if isnull(ll_cod_profilo) or ll_cod_profilo = 0 then
		continue
	end if
	
	select des_profilo
	into   :ls_des_profilo
	from   profili
	where  cod_profilo = :ll_cod_profilo;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Menu Principale","Errore in lettura descrizione profilo: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	ltv_item.expanded = true
	
	ltv_item.selected = false

	ltv_item.children = false
	
	ltv_item.label = ls_des_profilo
	
	ltv_item.bold = true
	
	ltv_item.pictureindex = 6
	
	ltv_item.selectedpictureindex = 6
	
	lstr_voce.cod_profilo = ll_cod_profilo
	
	lstr_voce.progressivo = 0
	
	lstr_voce.padre = 0
	
	lstr_voce.ordinamento = 0
	
	lstr_voce.tipo = "M"
	
	lstr_voce.collegamento = ""
	
	lstr_voce.nuovo = "N"
	
	lstr_voce.modifica = "N"
	
	lstr_voce.cancella = "N"
	
	ltv_item.data = lstr_voce
	
	ll_handle = tv_menu.insertitemlast(0,ltv_item)
	
	if wf_carica_menu(ll_cod_profilo,0,ll_handle) = -1 then
		g_mb.messagebox("Menu Principale","Errore in caricamento menu",stopsign!)
		close profili;
		setpointer(arrow!)
		return -1
	end if
	
loop

setpointer(arrow!)

return 0
end function

public function integer wf_nuova_cartella ();long ll_progressivo, ll_handle, ll_ordinamento

treeviewitem ltv_padre, ltv_item

str_voce_menu lstr_padre, lstr_voce

tv_menu.getitem(il_handle,ltv_padre)


lstr_padre = ltv_padre.data

select max(progressivo_menu)
into   :ll_progressivo
from   menu
where  cod_profilo = :lstr_padre.cod_profilo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura massimo progressivo voce del profilo " + &
				  string(lstr_padre.cod_profilo) + ": " + sqlca.sqlerrtext, stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) then
	ll_progressivo = 0
end if

ll_progressivo ++

select max(ordinamento)
into   :ll_ordinamento
from   menu
where  cod_profilo = :lstr_padre.cod_profilo and
		 progressivo_padre = :lstr_padre.progressivo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura massimo ordinamento voce del profilo " + &
				  string(lstr_padre.cod_profilo) + " ramo " + string(lstr_padre.progressivo) + &
				  ": " + sqlca.sqlerrtext, stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_ordinamento) then
	ll_ordinamento = 0
end if

ll_ordinamento ++

insert
into   menu
		 (cod_profilo,
		 progressivo_menu,
		 progressivo_padre,
		 ordinamento,
		 flag_tipo_voce,
		 descrizione_voce,
		 collegamento,
		 flag_nuovo,
		 flag_modifica,
		 flag_cancella)
values (:lstr_padre.cod_profilo,
		 :ll_progressivo,
		 :lstr_padre.progressivo,
		 :ll_ordinamento,
		 'M',
		 'Nuova Cartella',
		 '',
		 'N',
		 'N',
		 'N');
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in inserimento nuova cartella: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

commit;

lstr_voce.cod_profilo = lstr_padre.cod_profilo
	
lstr_voce.progressivo = ll_progressivo

lstr_voce.padre = lstr_padre.progressivo

lstr_voce.ordinamento = ll_ordinamento
	
lstr_voce.tipo = "M"

ltv_item.label = "Nuova Cartella"

ltv_item.bold = false

lstr_voce.collegamento = ""

lstr_voce.nuovo = "N"

lstr_voce.modifica = "N"

lstr_voce.cancella = "N"

ltv_item.expanded = false

ltv_item.selected = false

ltv_item.children = true

ltv_item.data = lstr_voce

ltv_item.pictureindex = 1

ltv_item.selectedpictureindex = 1

ll_handle = tv_menu.insertitemlast(il_handle,ltv_item)

tv_menu.selectitem(ll_handle)

il_handle = ll_handle

tv_menu.editlabels = true

tv_menu.editlabel(ll_handle)

return 0
end function

public function integer wf_elimina ();if g_mb.messagebox("Menu Principale","Eliminare l'elemento selezionato e tutti i relativi " + &
				  "sottoelementi?",question!,yesno!,2) = 2 then
	return 0
end if

setpointer(hourglass!)

long ll_padre

treeviewitem ltv_item, ltv_padre

str_voce_menu lstr_voce

tv_menu.getitem(il_handle,ltv_item)

lstr_voce = ltv_item.data

if lstr_voce.progressivo = 0 then
	
	long   ll_count = 0
	
	string ls_utente, ls_utenti = ""
	
	declare utenti cursor for
	select username	
	from 	 aziende_utenti,
			 utenti
	where  utenti.cod_utente = aziende_utenti.cod_utente and
			 cod_profilo = :lstr_voce.cod_profilo;
	
	open utenti;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Menu Principale","Errore in controllo utilizzo profilo " + string(lstr_voce.cod_profilo) + &
					  "~nErrore nella open del cursore utenti: " + sqlca.sqlerrtext,stopsign!)
		setpointer(arrow!)
		return -1
	end if
	
	do while true
	
		fetch utenti
		into  :ls_utente;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Menu Principale","Errore in controllo utilizzo profilo " + string(lstr_voce.cod_profilo) + &
						  "~nErrore nella fetch del cursore utenti: " + sqlca.sqlerrtext,stopsign!)
			setpointer(arrow!)
			close utenti;
			return -1
		elseif sqlca.sqlcode = 100 then
			close utenti;
			exit
		end if
		
		ll_count ++
		
		ls_utenti = ls_utenti + " - " + ls_utente + "~n"
		
	loop
	
	if not isnull(ll_count) and ll_count > 0 then
		
		if ll_count = 1 then
			ls_utenti = string(ll_count) + " utente:~n" + ls_utenti
		else
			ls_utenti = string(ll_count) + " utenti:~n" + ls_utenti
		end if
		
		if not(ltv_item.children) then
			g_mb.messagebox("Menu Principale","Questo profilo è attualmente collegato a " + &
						  ls_utenti + "Il profilo non può essere eliminato.",exclamation!)
			setpointer(arrow!)
			return 0
		end if
		
		if g_mb.messagebox("Menu Principale","Questo profilo è attualmente collegato a " + &
						  ls_utenti + "Il profilo non può essere eliminato.~nContinuare eliminando solo le " + &
						  "relative voci?",question!,yesno!,2) = 2 then
			setpointer(arrow!)
			return 0
		else
			
			delete
			from  menu
			where cod_profilo = :lstr_voce.cod_profilo;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Menu Principale","Errore in eliminazione voci profilo " + string(lstr_voce.cod_profilo) + &
							  ": " + sqlca.sqlerrtext,stopsign!)
				rollback;
				setpointer(arrow!)
				return -1
			end if
			
			long ll_handle
			
			do
				
				ll_handle = tv_menu.finditem(childtreeitem!,il_handle)
				
				if ll_handle > 0 then
					tv_menu.deleteitem(ll_handle)
				end if
				
			loop while ll_handle <> -1
			
		end if
	
	else

		delete
		from  menu
		where cod_profilo = :lstr_voce.cod_profilo;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Menu Principale","Errore in eliminazione voci profilo " + string(lstr_voce.cod_profilo) + &
						  ": " + sqlca.sqlerrtext,stopsign!)
			rollback;
			setpointer(arrow!)
			return -1
		end if
		
		delete
		from  profili
		where cod_profilo = :lstr_voce.cod_profilo;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Menu Principale","Errore in eliminazione profilo " + string(lstr_voce.cod_profilo) + &
						  ": " + sqlca.sqlerrtext,stopsign!)
			rollback;
			setpointer(arrow!)
			return -1
		end if
		
		commit;

		tv_menu.deleteitem(il_handle)
		
	end if
	
else
	
	if wf_cancella_menu(lstr_voce.cod_profilo,lstr_voce.progressivo) = -1 then
		rollback;
		setpointer(arrow!)
		return -1
	end if
	
	delete
	from  menu
	where cod_profilo = :lstr_voce.cod_profilo and
			progressivo_menu = :lstr_voce.progressivo;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Menu Principale","Errore in eliminazione voce " + string(lstr_voce.progressivo) + &
					  " profilo " + string(lstr_voce.cod_profilo) + ": " + sqlca.sqlerrtext,stopsign!)
		rollback;
		setpointer(arrow!)
		return -1
	end if
	
	commit;

	ll_padre = tv_menu.finditem(parenttreeitem!,il_handle)
	
	tv_menu.deleteitem(il_handle)
	
	if ll_padre > 0 then
		
		tv_menu.getitem(ll_padre,ltv_padre)
	
		if not(ltv_padre.children) and ltv_padre.level > 1 then
			ltv_padre.pictureindex = 1
			ltv_padre.selectedpictureindex = 1
			tv_menu.setitem(ll_padre,ltv_padre)
		end if
		
	end if
	
end if

setpointer(arrow!)

return 0
end function

public function integer wf_drag_drop (ref treeview atv_tree, long al_drop, long al_drag, boolean ab_elimina);long    ll_padre, ll_delete, ll_ordine

boolean lb_cartella

treeviewitem ltv_drag, ltv_drop, ltv_padre

str_voce_menu lstr_drag, lstr_drop


if al_drag < 1 then
	return -1
end if

atv_tree.getitem(al_drag,ltv_drag)

lstr_drag = ltv_drag.data

atv_tree.getitem(al_drop,ltv_drop)

lstr_drop = ltv_drop.data

ll_padre = al_drop

do
	
	ll_padre = atv_tree.finditem(parenttreeitem!,ll_padre)
	
	if ll_padre > 0 and ll_padre = al_drag then
		g_mb.messagebox("Menu Principale","Impossibile spostare una cartella al proprio interno!",exclamation!)
		return -1
	end if
	
loop while ll_padre > 0

if lstr_drop.tipo = "M" then
	if lstr_drop.progressivo > 0 then
		choose case g_mb.messagebox("Menu Principale","L'elemento di destinazione è una cartella.~n" + &
						"Spostare la voce all'interno della cartella?",question!,yesnocancel!,3)
			case 1
				lb_cartella = true
			case 2
				lb_cartella = false
			case 3
				return -1
		end choose
	else
		lb_cartella = true
	end if
else
	if al_drop = al_drag or al_drop = atv_tree.finditem(nexttreeitem!,al_drag) then
		return -1
	end if
	choose case g_mb.messagebox("Menu Principale","Spostare l'elemento selezionato in questa posizione?",question!,okcancel!,2)
		case 1
			lb_cartella = false
		case 2
			return -1
	end choose
end if

if	lb_cartella = false then
	
	ll_ordine = lstr_drop.ordinamento
	
	al_drop = atv_tree.finditem(parenttreeitem!,al_drop)
	
	if al_drop < 1 then
		g_mb.messagebox("Menu Principale","Errore in lettura padre",stopsign!)
		return -1
	end if
	
	atv_tree.getitem(al_drop,ltv_drop)
	
	lstr_drop = ltv_drop.data
	
else
	
	ll_ordine = 0
	
end if

if wf_trasferisci_ramo(atv_tree,ltv_drag,ltv_drop,ll_ordine,ab_elimina) <> 0 then
	return -1
end if

ib_carica_tutto = false

if ab_elimina then
	
	ll_padre = atv_tree.finditem(parenttreeitem!,al_drag)
	
	atv_tree.deleteitem(al_drag)
	
	if ll_padre > 0 then
		
		atv_tree.getitem(ll_padre,ltv_padre)
	
		if not(ltv_padre.children) and ltv_padre.level > 1 then
			ltv_padre.pictureindex = 1
			ltv_padre.selectedpictureindex = 1
			atv_tree.setitem(ll_padre,ltv_padre)
		end if
		
	end if
	
end if

do
	
	ll_delete = atv_tree.finditem(childtreeitem!,al_drop)
	
	if ll_delete > 0 then
		atv_tree.deleteitem(ll_delete)
	end if
	
loop while ll_delete <> -1

if wf_carica_menu(lstr_drop.cod_profilo,lstr_drop.progressivo,al_drop) = -1 then
	g_mb.messagebox("Menu Principale","Errore in caricamento menu profilo " + string(lstr_drop.cod_profilo) + " ramo " + string(lstr_drop.progressivo),stopsign!)
	return -1
end if

atv_tree.expanditem(al_drop)

return 0
end function

public function integer wf_apri_window ();s_cs_xx.parametri.parametro_s_1 = ""

window_open(w_nuovo_url,0)

if s_cs_xx.parametri.parametro_d_1 = -1 then
	setnull(s_cs_xx.parametri.parametro_s_1)
	return -1
end if

window lw_window
		
window_type_open(lw_window,s_cs_xx.parametri.parametro_s_1,-1)

setnull(s_cs_xx.parametri.parametro_s_1)

return 0
end function

public function integer wf_trasferisci_ramo (treeview atv_tree, treeviewitem atv_drag, treeviewitem atv_drop, long al_posizione, boolean ab_elimina);long ll_child, ll_count, ll_i

treeviewitem  ltv_new, ltv_child

str_voce_menu lstr_drag, lstr_drop, lstr_new, lstr_child

datastore lds_menu


lstr_drag = atv_drag.data

lstr_drop = atv_drop.data

lstr_new.cod_profilo = lstr_drop.cod_profilo

select
	max(progressivo_menu)
into
	:lstr_new.progressivo
from
	menu
where
	cod_profilo = :lstr_new.cod_profilo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura massimo progressivo, profilo " + string(lstr_new.cod_profilo) + &
				  ": " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or isnull(lstr_new.progressivo) then
	lstr_new.progressivo = 0
end if

lstr_new.progressivo ++

lstr_new.padre = lstr_drop.progressivo

if al_posizione = -1 then
	
	lstr_new.ordinamento = lstr_drag.ordinamento
	
elseif al_posizione = 0 then
	
	select
		max(ordinamento)
	into
		:lstr_new.ordinamento
	from
		menu
	where
		cod_profilo = :lstr_new.cod_profilo and
		progressivo_padre = :lstr_new.padre;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Menu Principale","Errore in lettura massimo ordinamento, profilo " + string(lstr_new.cod_profilo) + &
					  " padre " + string(lstr_new.padre) + ": " + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 or isnull(lstr_new.ordinamento) then
		lstr_new.ordinamento = 0
	end if
	
	lstr_new.ordinamento ++
	
else
	
	lstr_new.ordinamento = al_posizione
	
end if

lstr_new.tipo = lstr_drag.tipo

lstr_new.collegamento = lstr_drag.collegamento

lstr_new.nuovo = lstr_drag.nuovo

lstr_new.modifica = lstr_drag.modifica

lstr_new.cancella = lstr_drag.cancella

ltv_new.label = atv_drag.label

ltv_new.bold = atv_drag.bold

ltv_new.expanded = atv_drag.expanded

ltv_new.selected = atv_drag.selected

ltv_new.children = atv_drag.children

ltv_new.pictureindex = atv_drag.pictureindex

ltv_new.selectedpictureindex = atv_drag.selectedpictureindex

ltv_new.data = lstr_new

update
	menu
set
	ordinamento = ordinamento + 1
where
	cod_profilo = :lstr_new.cod_profilo and
	progressivo_padre = :lstr_new.padre and
	ordinamento >= :lstr_new.ordinamento;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in aggiornamento ordinamento, profilo " + string(lstr_new.cod_profilo) + &
				  " padre " + string(lstr_new.padre) + ": " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

insert into
	menu
	(cod_profilo,
	progressivo_menu,
	progressivo_padre,
	ordinamento,
	flag_tipo_voce,
	descrizione_voce,
	collegamento,
	flag_nuovo,
	flag_modifica,
	flag_cancella)
values
	(:lstr_new.cod_profilo,
	:lstr_new.progressivo,
	:lstr_new.padre,
	:lstr_new.ordinamento,
	:lstr_new.tipo,
	:ltv_new.label,
	:lstr_new.collegamento,
	:lstr_new.nuovo,
	:lstr_new.modifica,
	:lstr_new.cancella);

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in inserimento voce, profilo " + string(lstr_new.cod_profilo) + &
				  " padre " + string(lstr_new.padre) + ": " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

lds_menu = create datastore

lds_menu.dataobject = "ds_menu"

if lds_menu.settransobject(sqlca) < 0 then
	g_mb.messagebox("Menu Principale","Errore in impostazione transazione datastore",stopsign!)
	destroy lds_menu
	return -1
end if

if lds_menu.retrieve({lstr_drag.cod_profilo},lstr_drag.progressivo,"XXX") < 0 then
	g_mb.messagebox("Menu Principale","Errore in lettura voci:~nProfilo " + string(lstr_drag.cod_profilo) + &
				  "~nPadre " + string(lstr_drag.progressivo),stopsign!)
	destroy lds_menu
	return -1
end if

ll_count = lds_menu.rowcount()

for ll_i = 1 to ll_count
	
	lstr_child.cod_profilo = lds_menu.getitemnumber(ll_i,"cod_profilo")
	
	lstr_child.progressivo = lds_menu.getitemnumber(ll_i,"progressivo_menu")
	
	lstr_child.padre = lds_menu.getitemnumber(ll_i,"progressivo_padre")
	
	lstr_child.ordinamento = lds_menu.getitemnumber(ll_i,"ordinamento")
	
	lstr_child.tipo = lds_menu.getitemstring(ll_i,"flag_tipo_voce")
	
	ltv_child.label = lds_menu.getitemstring(ll_i,"descrizione_voce")
	
	ltv_child.bold = false
	
	lstr_child.collegamento = lds_menu.getitemstring(ll_i,"collegamento")
	
	lstr_child.nuovo = lds_menu.getitemstring(ll_i,"flag_nuovo")
	
	lstr_child.modifica = lds_menu.getitemstring(ll_i,"flag_modifica")
	
	lstr_child.cancella = lds_menu.getitemstring(ll_i,"flag_cancella")
	
	ltv_child.data = lstr_child
	
	ltv_child.expanded = false
	
	ltv_child.selected = false
	
	choose case lstr_child.tipo
		case "M"
			ltv_child.pictureindex = 1
			ltv_child.selectedpictureindex = 1
			ltv_child.children = true
		case "W"
			ltv_child.pictureindex = 3
			ltv_child.selectedpictureindex = 3
			ltv_child.children = false
		case "C"
			ltv_child.pictureindex = 4
			ltv_child.selectedpictureindex = 4
			ltv_child.children = false
		case "E"
			ltv_child.pictureindex = 5
			ltv_child.selectedpictureindex = 5
			ltv_child.children = false
		case "P"
			ltv_child.pictureindex = 7
			ltv_child.selectedpictureindex = 7
			ltv_child.children = false
	end choose
	
	if wf_trasferisci_ramo(atv_tree,ltv_child,ltv_new,-1,ab_elimina) <> 0 then
		rollback;
		destroy lds_menu
		return -1
	end if
	
next

destroy lds_menu

if ab_elimina then
	
	delete from
		menu
	where
		cod_profilo = :lstr_drag.cod_profilo and
		progressivo_menu = :lstr_drag.progressivo;
		
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Menu Principale","Errore in cancellazione voce, profilo " + string(lstr_drag.cod_profilo) + &
					  " progressivo " + string(lstr_drag.progressivo) + ": " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return -1
	end if
	
end if

commit;

return 0
end function

public function integer wf_get_width ();string ls_width


if registryget(s_cs_xx.chiave_root_user + "applicazione_" +  s_cs_xx.profilocorrente,"RLM",ls_width) = -1 then
	il_currwidth = 1350
end if

il_currwidth = dec(ls_width)

if isnull(il_currwidth) or il_currwidth = 0 then
	il_currwidth = 1350
end if

return 0
end function

public function integer wf_taglia_profilo ();il_handle_cutpaste= tv_menu.FindItem (currenttreeitem!,0 )
ib_cutpaste=true
return 0
end function

public function integer wf_copia_profilo ();il_handle_cutpaste= tv_menu.FindItem (currenttreeitem!,0 )
return 0
end function

public function integer wf_incolla_profilo ();long ll_handle_finale
ll_handle_finale=tv_menu.FindItem(currenttreeitem!,0)

if wf_incolla_profilo(ll_handle_finale ,il_handle_cutpaste) <= 0 then
	setnull(il_handle_cutpaste)
	setnull(ib_cutpaste)
end if

return 0
end function

private function integer wf_incolla_profilo (long al_drop, long al_drag);string ls_message
long    ll_padre, ll_delete, ll_ordine
boolean lb_cartella
treeviewitem ltv_drag, ltv_drop, ltv_padre
str_voce_menu lstr_drag, lstr_drop

if al_drag < 1 and al_drop< 1 then
	return -1
end if

tv_menu.getitem(al_drag,ltv_drag)
lstr_drag = ltv_drag.data

tv_menu.getitem(al_drop,ltv_drop)
lstr_drop = ltv_drop.data

ll_padre = al_drop

if ib_cutpaste then
	ls_message="Vuoi tagliare " + ltv_drag.label + " in " + ltv_drop.label + " ? "
else 
	ls_message="Vuoi copiare " + ltv_drag.label + " in " + ltv_drop.label + " ? "
end if

if not g_mb.confirm(ls_message) then
	return 1
end if

// controllo se copio un profilo all'interno di una cartella o un profilo all'interno di un altro profilo.
if(lstr_drag.tipo = 'M'  and lstr_drop.tipo = 'M') then
	if tv_menu.finditem(parenttreeitem! ,al_drag)<1 then
		messagebox("Errore","Non puoi incollare!")
		return -1
		
	//controllo il padre
	elseif tv_menu.finditem(parenttreeitem! ,al_drop)<1 then
		messagebox("Errore","Non puoi incollare!")
		return -1
	end if
	
//	se copio un menu o cartella e cerco di incollarlo all'interno di una finestra da errore.
elseif (lstr_drag.tipo='M' and lstr_drop.tipo='W') then 
	messagebox("Errore","Non puoi incollare!")
	return -1
end if


ll_ordine=0

if wf_trasferisci_ramo(tv_menu,ltv_drag,ltv_drop,ll_ordine,ib_cutpaste) <> 0 then
	return -1
end if

ib_carica_tutto = false

// verifico se la variabile booleana è a true o false, nel primo caso elimino il dato, nel secondo no, a seconda se mi trovo nel caso "taglia" o "copia"
if ib_cutpaste then
	
	ll_padre = tv_menu.finditem(parenttreeitem!,al_drag)
	
	tv_menu.deleteitem(al_drag)
	
	if ll_padre > 0 then
		
		tv_menu.getitem(ll_padre,ltv_padre)
	
		if not(ltv_padre.children) and ltv_padre.level > 1 then
			ltv_padre.pictureindex = 1
			ltv_padre.selectedpictureindex = 1
			tv_menu.setitem(ll_padre,ltv_padre)
		end if
		
	end if
	
end if

do
	
	ll_delete = tv_menu.finditem(childtreeitem!,al_drop)
	
	if ll_delete > 0 then
		tv_menu.deleteitem(ll_delete)
	end if
	
loop while ll_delete <> -1

if wf_carica_menu(lstr_drop.cod_profilo,lstr_drop.progressivo,al_drop) = -1 then
	g_mb.messagebox("Menu Principale","Errore in caricamento menu profilo " + string(lstr_drop.cod_profilo) + " ramo " + string(lstr_drop.progressivo),stopsign!)
	return -1
end if

tv_menu.expanditem(al_drop)

return 0
end function

public subroutine wf_carica_elemento (ref datastore ads_elementi, ref long al_cod_profilo, long al_padre, long al_prog_padre, ref long al_count_elementi);long ll_j, ll_prog_voce, ll_padre
boolean lb_trovato
treeviewitem ltv_item
str_voce_menu  lstr_voce
lb_trovato=false
ll_j=1
ii_LivelloAttuale++

Do while((Not lb_trovato) And (ll_j<=al_count_elementi))
	
	if(al_prog_padre=ads_elementi.getitemnumber(ll_j,"progressivo_padre")) Then
		lstr_voce.cod_profilo = al_cod_profilo
		ll_prog_voce = ads_elementi.getitemnumber(ll_j,"progressivo_menu")
		lstr_voce.progressivo = ll_prog_voce
		lstr_voce.padre = ads_elementi.getitemnumber(ll_j,"progressivo_padre")
		lstr_voce.ordinamento = ads_elementi.getitemnumber(ll_j,"ordinamento")
		lstr_voce.tipo = ads_elementi.getitemstring(ll_j,"flag_tipo_voce")
		ltv_item.label = ads_elementi.getitemstring(ll_j,"descrizione_voce")
		ltv_item.bold = false
		lstr_voce.collegamento = ads_elementi.getitemstring(ll_j,"collegamento")
		lstr_voce.nuovo = ads_elementi.getitemstring(ll_j,"flag_nuovo")
		lstr_voce.modifica = ads_elementi.getitemstring(ll_j,"flag_modifica")
		lstr_voce.cancella = ads_elementi.getitemstring(ll_j,"flag_cancella")
		ltv_item.data = lstr_voce
		ltv_item.expanded = false
		ltv_item.selected = false
		
		choose case lstr_voce.tipo
			case "M"
				ltv_item.pictureindex = 1
				ltv_item.selectedpictureindex = 1
				ltv_item.children = true
			case "W"
				ltv_item.pictureindex = 3
				ltv_item.selectedpictureindex = 3
				ltv_item.children = false
			case "C"
				ltv_item.pictureindex = 4
				ltv_item.selectedpictureindex = 4
				ltv_item.children = false
			case "E"
				ltv_item.pictureindex = 5
				ltv_item.selectedpictureindex = 5
				ltv_item.children = false
			case "P"
				ltv_item.pictureindex = 7
				ltv_item.selectedpictureindex = 7
				ltv_item.children = false
		end choose
		
		ll_padre=tv_menu.insertitemlast(al_padre,ltv_item)
		
		//Salvo i cod. menu su un vettore (per la ricerca)
		il_CodProfiloRicerca[ii_contCodMenu]=al_cod_profilo
		il_CodMenu[ii_contCodMenu]=Long(ll_prog_voce);
		ii_LivelloMenu[ii_contCodMenu]=ii_LivelloAttuale
		ii_contCodMenu++;
		
		if (ltv_item.children) Then
			wf_carica_elemento (ads_elementi, al_cod_profilo, ll_padre, ll_prog_voce, al_count_elementi)
		end if
		ll_padre=al_padre
		
	else
		if(al_prog_padre<ads_elementi.getitemnumber(ll_j,"progressivo_padre")) Then
			lb_trovato=true
		end if
	end if
	
	ll_j++;
	
Loop

ii_LivelloAttuale=ii_LivelloAttuale - 1
end subroutine

public function integer wf_carica_menu (long al_cod_profilo, long al_prog_padre, long al_handle_padre);string ls_escludi_voce
long   ll_prog_voce, ll_i, ll_count_elementi, ll_elemento
datastore lds_elementi
TreeViewItem ltv_item

if s_cs_xx.admin and ib_intranet then
	ls_escludi_voce = "XXX"
else
	ls_escludi_voce = "P"
end if

//Dati degli elementi
lds_elementi = create datastore
lds_elementi.dataobject = "ds_menu"

if lds_elementi.settransobject(sqlca) < 0 then
	g_mb.messagebox("Menu Principale","Errore in impostazione transazione:~nProfilo " + string(al_cod_profilo) + &
				  "~nPadre " + string(al_prog_padre),stopsign!)
	destroy lds_elementi
	return -1
end if

if (lds_elementi.retrieve({al_cod_profilo}, ls_escludi_voce, "%")<0) then
	g_mb.messagebox("Menu Principale","Errore in lettura voci:~nProfilo " + string(al_cod_profilo) + &
				  "~nPadre " + string(al_prog_padre),stopsign!)
	destroy lds_elementi
	return -1
end if

ll_count_elementi = lds_elementi.rowcount()

wf_carica_elemento(lds_elementi, al_cod_profilo, al_handle_padre, al_prog_padre, ll_count_elementi)
ii_contCodMenu=ii_contCodMenu - 1

destroy lds_elementi

return ll_count_elementi
end function

public subroutine wf_collapseall (long ll_elemento);long ll_elementoProvvisorio;

ll_elementoProvvisorio=tv_menu.FindItem(ChildTreeItem!, ll_elemento)

if(ll_elementoProvvisorio<>-1) Then
	wf_collapseAll(ll_elementoProvvisorio);
	tv_menu.collapseItem(ll_elemento);
end if

ll_elementoProvvisorio=tv_menu.FindItem(NextTreeItem!, ll_elemento)
if(ll_elementoProvvisorio<>-1) Then
	wf_collapseAll(ll_elementoProvvisorio);
end if
end subroutine

public function long wf_seleziona_elemento (integer ai_cod_menu, long al_cod_profilo);/*
* Seleziona l'elemento all'interno dell'albero.
* Ritorna un long che rappresenta l'handle dell'alemento all'interno dell'albero.
* Ritorna -1 in caso di errore.
*/

integer li_i, li_j, li_posizioneElemento, li_livelloRaggiunto, li_IDNodi[];
long ll_elemento;
TreeViewItem ltvi_elemento;
str_voce_menu  lstr_voce

ll_elemento=tv_menu.FindItem(RootTreeItem!, 0)
ll_elemento=tv_menu.FindItem(ChildTreeItem!, ll_elemento)

Do while(ll_elemento<>-1)
	wf_collapseAll(ll_elemento)
	ll_elemento=tv_menu.FindItem(NextTreeItem!, ll_elemento)
Loop

//Determino la posizione dell'elemento cercato
for li_i=1 To ii_contCodMenu
	if((il_CodMenu[li_i]=ai_Cod_Menu) And (il_CodProfiloRicerca[li_i]=al_cod_profilo)) Then
		li_posizioneElemento=li_i;
		Exit
	end if
Next

//Se li_posizioneElemento=0 non c'è l'elemento...
if (li_posizioneElemento=0) Then
	g_mb.messagebox("Menu Principale","Voce cancellata dal repository.~nContattare l'amministratore",exclamation!)
	sle_cerca.Text=""
	return -1
end if

li_livelloRaggiunto=ii_LivelloMenu[li_posizioneElemento];
li_j=1;

//Determino i nodi da espandere...
Do While(li_i>0)
	if(ii_LivelloMenu[li_i]<li_livelloRaggiunto) Then
		li_IDNodi[li_j]=il_CodMenu[li_i];
		li_j++;
		li_livelloRaggiunto=ii_LivelloMenu[li_i];
		if(li_livelloRaggiunto=1) Then
			li_i=0;
		end if
	end if
	li_i=li_i - 1;
Loop

//Espando i nodi e seleziono l'elemento...

//Prendo l'elemento "root"...
ll_elemento=tv_menu.FindItem(RootTreeItem!, 0)

li_j=li_j - 1;

//...Ciclo su tutti gli elementi
Do While(ll_elemento<>-1)
	tv_menu.GetItem(ll_elemento, ltvi_elemento);
	lstr_voce=ltvi_elemento.Data
	if(lstr_voce.cod_profilo=al_cod_profilo) Then
		//Se trovo uno dei nodi da espandere...
		if(li_j>0) Then
			if (Integer(lstr_voce.progressivo)=li_IDNodi[li_j]) Then
				tv_menu.expanditem(ll_elemento);
				li_j=li_j - 1;
			end if
		end if
		//Se trovo l'elemento con lo stesso nome, lo seleziono ed interrompo il ciclo
		if(Integer(lstr_voce.progressivo)=ai_Cod_Menu) Then
			tv_menu.SetFocus()
			tv_menu.SelectItem(ll_elemento);
			return ll_elemento;
		end if
	end if
	
	ll_elemento=tv_menu.FindItem(NextVisibleTreeItem!, ll_elemento)
Loop

ll_elemento=-1
end function

on w_menu_80_menu_principale.create
this.pb_bloccamenu=create pb_bloccamenu
this.dw_ricerca=create dw_ricerca
this.sle_cerca=create sle_cerca
this.sle_azienda=create sle_azienda
this.sle_utente=create sle_utente
this.pb_aziende=create pb_aziende
this.pb_utenti=create pb_utenti
this.pb_trova=create pb_trova
this.gb_azienda=create gb_azienda
this.gb_utente=create gb_utente
this.st_resize=create st_resize
this.tv_menu=create tv_menu
this.Control[]={this.pb_bloccamenu,&
this.dw_ricerca,&
this.sle_cerca,&
this.sle_azienda,&
this.sle_utente,&
this.pb_aziende,&
this.pb_utenti,&
this.pb_trova,&
this.gb_azienda,&
this.gb_utente,&
this.st_resize,&
this.tv_menu}
end on

on w_menu_80_menu_principale.destroy
destroy(this.pb_bloccamenu)
destroy(this.dw_ricerca)
destroy(this.sle_cerca)
destroy(this.sle_azienda)
destroy(this.sle_utente)
destroy(this.pb_aziende)
destroy(this.pb_utenti)
destroy(this.pb_trova)
destroy(this.gb_azienda)
destroy(this.gb_utente)
destroy(this.st_resize)
destroy(this.tv_menu)
end on

event open;backcolor = s_themes.theme[s_themes.current_theme].w_backcolor

gb_azienda.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor
gb_utente.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor
sle_azienda.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor
sle_utente.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor
sle_cerca.backcolor = s_themes.theme[s_themes.current_theme].dw_column_backcolor
st_resize.backcolor = s_themes.theme[s_themes.current_theme].w_backcolor

//Pulsanti della DataWindow di ricerca
dw_ricerca.object.p_pulsanteAlberoFinestra.Filename  = guo_functions.uof_risorse("menu/window.png")
dw_ricerca.object.p_pulsanteAlberoNodo.Filename  = guo_functions.uof_risorse("menu/folder_open.png")
dw_ricerca.object.p_pulsanteTornaAlMenu.Filename  = guo_functions.uof_risorse("treeview/rotate.png")

il_currheight = height

ib_visible = true
ib_autohide = true

wf_get_width()

triggerevent("ue_imposta_window")

setredraw(false)

title = "Menu Principale"

is_immagini = s_cs_xx.volume + s_cs_xx.risorse

//stefanop 10/09/2010
//fabiob 15/11/2012
pb_bloccaMenu.picturename=guo_functions.uof_risorse("menu/unlock_stroke.bmp")
pb_aziende.picturename = is_immagini + "aziende.bmp"
pb_utenti.picturename = is_immagini + "utenti.bmp"
pb_trova.picturename = is_immagini + "trova.bmp"

//pb_aziende.picturename = is_immagini + "menu\azienda.png"
//pb_utenti.picturename = is_immagini + "menu\users.png"
//pb_trova.picturename = is_immagini + "menu\search.png"

/*
stefanop 10/09/2010
//gestione risoluzione colore schermo
environment env
getenvironment(env)

if env.numberofcolors < 65000 then
	
	tv_menu.addpicture(is_immagini + "cartella_chiudi.bmp")
	tv_menu.addpicture(is_immagini + "cartella_apri.bmp")
	tv_menu.addpicture(is_immagini + "window.bmp")
	tv_menu.addpicture(is_immagini + "link.bmp")
	tv_menu.addpicture(is_immagini + "eliminata.bmp")
	tv_menu.addpicture(is_immagini + "menu.bmp")
	tv_menu.addpicture(is_immagini + "intranet.bmp")
	
else
	
	tv_menu.addpicture(is_immagini + "10.5\MENU_cartella_chiudi.ico")
	tv_menu.addpicture(is_immagini + "10.5\MENU_cartella_apri.ico")
	tv_menu.addpicture(is_immagini + "10.5\MENU_window.ico")
	tv_menu.addpicture(is_immagini + "10.5\MENU_link.ico")
	tv_menu.addpicture(is_immagini + "10.5\MENU_eliminata.ico")
	tv_menu.addpicture(is_immagini + "10.5\MENU_profilo.ico")
	tv_menu.addpicture(is_immagini + "10.5\MENU_intranet.ico")
	
end if
*/
tv_menu.addpicture(is_immagini + "menu\folder_close.png")
tv_menu.addpicture(is_immagini + "menu\folder_open.png")
tv_menu.addpicture(is_immagini + "menu\window.png")
tv_menu.addpicture(is_immagini + "menu\link.png")
tv_menu.addpicture(is_immagini + "menu\folder_delete.png")
tv_menu.addpicture(is_immagini + "menu\user.png")
tv_menu.addpicture(is_immagini + "menu\intranet.png")
	

ib_autohide = TRUE

//event trigger ue_autohide(ib_autohide,false)

m_cs_xx lm_cs_xx

lm_cs_xx = menuid

if not isvalid(lm_cs_xx) then lm_cs_xx = pcca.mdi_frame.menuid

lm_cs_xx.mf_imposta_menu_menu()

string ls_flag

select flag
into	 :ls_flag
from   parametri
where  flag_parametro = 'F' and
		 cod_parametro = 'VAI';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Menu Principale","Errore in controllo gestione Intranet: " + sqlca.sqlerrtext,stopsign!)
	ib_intranet = false
elseif sqlca.sqlcode = 100 or isnull(ls_flag) then
	ib_intranet = false
elseif sqlca.sqlcode = 0 then
	if ls_flag = "S" then
		ib_intranet = true
	else
		ib_intranet = false
	end if
end if

wf_inizio(false)

setredraw(true)

postevent("ue_news")

dw_ricerca.setTransObject(SQLCA);




end event

event closequery;if not(ib_chiudi) then
	return 1
end if
end event

event resize;if not ib_drag then
	
	event post ue_imposta_controlli(width - il_currwidth,height - il_currheight)
	postevent("ue_ridimensiona_contenuto_dw")
	
	if ib_visible then
		
		il_currwidth = width
		
		il_currheight = height
	
	end if
	
	event post ue_set_mdi_workspace(true)
	
end if
end event

event close;event trigger ue_set_mdi_workspace(false)
end event

event timer;//st_1.text=string(now())

if ib_autohide and ib_visible and not isvalid(w_voci_repository) then
	
	long ll_x, ll_y
	
	ll_x = w_cs_xx_mdi.pointerx()
	
	ll_y = w_cs_xx_mdi.pointery()
	
//	st_2.text=">>>" + string(now())
	
	if ll_x < x or ll_x > (x + width) or ll_y < y or ll_y > (y + height) then
		postevent("ue_show_hide")
	end if
	
end if

Yield()
end event

event mousemove;TIMER(1)
end event

event show;ib_visible = TRUE
end event

event hide;ib_visible = FALSE
end event

type pb_bloccamenu from picturebutton within w_menu_80_menu_principale
integer x = 1175
integer y = 1972
integer width = 137
integer height = 120
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
boolean flatstyle = true
vtextalign vtextalign = vcenter!
string powertiptext = "Blocca men"
end type

event clicked;ib_autohide=Not ib_autohide

if (ib_autohide) Then
	pb_bloccaMenu.picturename=guo_functions.uof_risorse("menu/unlock_stroke.bmp")
	pb_bloccaMenu.PowerTipText="Blocca men"
else
	pb_bloccaMenu.picturename=guo_functions.uof_risorse("menu/unlock_fill.bmp")
	pb_bloccaMenu.PowerTipText="Sblocca men"
end if
end event

type dw_ricerca from datawindow within w_menu_80_menu_principale
integer x = 9
integer y = 12
integer width = 1303
integer height = 1696
integer taborder = 30
string title = "Ricerca"
string dataobject = "ds_menu"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;/*wf_seleziona_elemento(dw_ricerca.getitemnumber(row,"progressivo_menu"))
dw_ricerca.Visible=false
tv_menu.Visible=true*/
long ll_elemento;

if(String(dwo.Name)="pulsantealbero") Then
	ll_elemento=wf_seleziona_elemento(dw_ricerca.getitemnumber(row,"progressivo_menu"), dw_ricerca.getitemnumber(row,"cod_profilo"))
	dw_ricerca.Visible=false
	if(dw_ricerca.getitemstring(row,"flag_tipo_voce")="M") Then
		tv_menu.expandItem(ll_elemento)
	end if
	tv_menu.Visible=true
	return 0
end if

//E' il tv_menu.doubleclicked(handle) riadattato...

choose case dw_ricerca.getitemstring(row,"flag_tipo_voce")
	
	//In questo caso è un nodo, quindi nascondo la DataWindow e mostro l'albero, con l'elemento selezionato...
	case "M"
		ll_elemento=wf_seleziona_elemento(dw_ricerca.getitemnumber(row,"progressivo_menu"), dw_ricerca.getitemnumber(row,"cod_profilo"))
		dw_ricerca.Visible=false
		tv_menu.expandItem(ll_elemento)
		tv_menu.Visible=true
		
	case "W"
		
		if dw_ricerca.getitemstring(row,"flag_nuovo") = "S" then
			s_cs_xx.flag_nuovo = true
		else
			s_cs_xx.flag_nuovo = false
		end if
		
		if dw_ricerca.getitemstring(row,"flag_modifica") = "S" then
			s_cs_xx.flag_modifica = true
		else
			s_cs_xx.flag_modifica = false
		end if
		
		if dw_ricerca.getitemstring(row,"flag_cancella") = "S" then
			s_cs_xx.flag_cancella = true
		else
			s_cs_xx.flag_cancella = false
		end if
		
		window lw_window
		
		window_type_open(lw_window,dw_ricerca.getitemstring(row,"collegamento"),-1)
		if isvalid(w_extra_windows) then
			w_extra_windows.add_window(dw_ricerca.getitemstring(row,"collegamento"), dw_ricerca.getitemstring(row,"descrizione_voce"))
		end if
		
	case "C"
		
		inet l_inet
		
		l_inet = create inet
		
		l_inet.hyperlinktourl(dw_ricerca.getitemstring(row,"collegamento"))
		
		destroy l_inet
		
	case "E"
		
		g_mb.messagebox("Menu Principale","Voce cancellata dal repository.~nContattare l'amministratore",exclamation!)
		
		return -1
				
end choose 
end event

event clicked;if(String(dwo.Name)="p_pulsantetornaalmenu") Then
	dw_ricerca.Visible=false
	tv_menu.Visible=true
	sle_cerca.Text=""
	return 0
end if
end event

type sle_cerca from singlelineedit within w_menu_80_menu_principale
event ue_seleziona ( )
event ue_key pbm_keydown
integer x = 169
integer y = 1984
integer width = 983
integer height = 100
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

event ue_seleziona();selecttext(1,len(text))
end event

event ue_key;if key = keyenter! then
	pb_trova.postevent("clicked")
end if
end event

event getfocus;postevent("ue_seleziona")
end event

event modified;pb_trova.postevent("clicked")
end event

type sle_azienda from singlelineedit within w_menu_80_menu_principale
integer x = 192
integer y = 1764
integer width = 1097
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = false
boolean displayonly = true
end type

type sle_utente from singlelineedit within w_menu_80_menu_principale
integer x = 192
integer y = 1884
integer width = 1097
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = false
boolean displayonly = true
end type

type pb_aziende from picturebutton within w_menu_80_menu_principale
integer x = 9
integer y = 1732
integer width = 137
integer height = 120
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
boolean flatstyle = true
vtextalign vtextalign = vcenter!
end type

event clicked;if g_mb.messagebox("Menu Principale","Si desidera cambiare azienda?",question!,yesno!,2) = 2 then
	return -1
end if

if pcca.mdi_frame_valid then
   triggerevent(pcca.mdi_frame, "pc_closeall")
end if

window_open(w_aziende_scelta,0)
end event

type pb_utenti from picturebutton within w_menu_80_menu_principale
integer x = 9
integer y = 1852
integer width = 137
integer height = 120
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
boolean flatstyle = true
vtextalign vtextalign = vcenter!
end type

event clicked;if g_mb.messagebox("Menu Principale","Si desidera cambiare utente?",question!,yesno!,2) = 2 then
	return -1
end if

if pcca.mdi_frame_valid then
   triggerevent(pcca.mdi_frame, "pc_closeall")
end if

window_open(w_login,0)
end event

type pb_trova from picturebutton within w_menu_80_menu_principale
integer x = 9
integer y = 1972
integer width = 137
integer height = 120
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
boolean flatstyle = true
vtextalign vtextalign = vcenter!
end type

event clicked;string ls_ricerca, ls_escludi_voce;

ls_ricerca = sle_cerca.text

if isnull(ls_ricerca) or trim(ls_ricerca) = "" then
	// stefanop 10/09/2010: eliminato sto messaggio che rompe solo le .....
	//g_mb.messagebox("Menu Principale","Immettere un parametro per la ricerca",exclamation!)
	dw_ricerca.Visible=false
	tv_menu.Visible=true
	return -1
end if

if s_cs_xx.admin and ib_intranet then
	ls_escludi_voce = "XXX"
else
	ls_escludi_voce = "P"
end if

dw_ricerca.Object.p_pulsanteAlberoFinestra.Tooltip.Enabled
if(UpperBound(il_cod_profilo)>1) Then
	dw_ricerca.Object.risultati.Expression="descrizione_voce+' - '+ profili_des_profilo"
	dw_ricerca.Object.risultati.Tooltip.Enabled=true
else
	dw_ricerca.Object.risultati.Expression="descrizione_voce"
	dw_ricerca.Object.risultati.Tooltip.Enabled=false
end if

dw_ricerca.Retrieve(il_cod_profilo[], ls_escludi_voce, UPPER("%"+ls_ricerca+"%"))

tv_menu.Visible=false
dw_ricerca.Visible=true

/*string ls_ricerca


ls_ricerca = sle_cerca.text

if isnull(ls_ricerca) or trim(ls_ricerca) = "" then
	// stefanop 10/09/2010: eliminato sto messaggio che rompe solo le .....
	//g_mb.messagebox("Menu Principale","Immettere un parametro per la ricerca",exclamation!)
	return -1
end if

if not(ib_carica_tutto) then
	tv_menu.setredraw(false)
	wf_inizio(true)
	tv_menu.setredraw(true)
	ib_carica_tutto = true
end if

if il_handle <= 0 or isnull(il_handle) then
	il_handle = tv_menu.finditem(roottreeitem!,0)
end if

tv_menu.setfocus()

tv_menu.selectitem(il_handle)

if il_handle > 0 then
	if wf_trova(ls_ricerca,il_handle) = 100 then
		g_mb.messagebox("Menu Principale","Elemento non trovato",information!)
		tv_menu.setfocus()
	end if
end if*/
end event

type gb_azienda from groupbox within w_menu_80_menu_principale
integer x = 169
integer y = 1712
integer width = 1143
integer height = 132
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
end type

type gb_utente from groupbox within w_menu_80_menu_principale
integer x = 169
integer y = 1832
integer width = 1143
integer height = 132
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
end type

type st_resize from statictext within w_menu_80_menu_principale
event ue_lbuttondown pbm_lbuttondown
event ue_lbuttonup pbm_lbuttonup
event ue_mousemove pbm_mousemove
integer x = 1326
integer width = 23
integer height = 2108
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string pointer = "SizeWE!"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

event ue_lbuttondown;ib_drag = true

il_dragx = xpos
il_dragy = ypos
end event

event ue_lbuttonup;ib_drag = false

il_dragx = 0
il_dragy = 0

wf_set_width()

parent.postevent("resize")
end event

event ue_mousemove;if ib_drag then
	
	long ll_newwidth
	
	ll_newwidth = parent.width + (xpos - il_dragx)
	
	if ll_newwidth >= 350  and ll_newwidth <= (w_cs_xx_mdi.width - 10) then
		parent.width = ll_newwidth
	end if
	
	il_dragx = xpos
	
end if
end event

type tv_menu from treeview within w_menu_80_menu_principale
event ue_mousemove pbm_mousemove
integer x = 9
integer y = 12
integer width = 1303
integer height = 1696
integer taborder = 20
string dragicon = "WinLogo!"
boolean dragauto = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
boolean hasbuttons = false
boolean disabledragdrop = false
boolean hideselection = false
integer picturewidth = 16
integer pictureheight = 16
long picturemaskcolor = 536870912
integer statepicturewidth = 16
integer statepictureheight = 16
long statepicturemaskcolor = 536870912
end type

event ue_mousemove;parent.triggerevent("mousemove")
end event

event doubleclicked;//In caso di modifiche, attenzione che il dw_ricerca.doubleclicked(xpos, ypos, row, dwo) funziona uguale. Quindi...
//...C'è da modificare anche quello!

treeviewitem ltv_item

str_voce_menu lstr_voce


if isnull(handle) or handle <= 0 then
	return 0
end if

getitem(handle,ltv_item)

lstr_voce = ltv_item.data

choose case lstr_voce.tipo
		
	case "W"
		
		if lstr_voce.nuovo = "S" then
			s_cs_xx.flag_nuovo = true
		else
			s_cs_xx.flag_nuovo = false
		end if
		
		if lstr_voce.modifica = "S" then
			s_cs_xx.flag_modifica = true
		else
			s_cs_xx.flag_modifica = false
		end if
		
		if lstr_voce.cancella = "S" then
			s_cs_xx.flag_cancella = true
		else
			s_cs_xx.flag_cancella = false
		end if
		
		window lw_window
		
		window_type_open(lw_window,lstr_voce.collegamento,-1)
		if isvalid(w_extra_windows) then
			w_extra_windows.add_window(lstr_voce.collegamento, ltv_item.Label)
		end if
		
	case "C"
		
		inet l_inet
		
		l_inet = create inet
		
		l_inet.hyperlinktourl(lstr_voce.collegamento)
		
		destroy l_inet
		
	case "E"
		
		g_mb.messagebox("Menu Principale","Voce cancellata dal repository.~nContattare l'amministratore",exclamation!)
		
		return -1
				
end choose 

return 0
end event

event dragdrop;long ll_drop, ll_drag


setdrophighlight(0)

if handle = 0 or isnull(handle) then
	return -1
end if

ll_drop = handle

ll_drag = tv_menu.finditem(currenttreeitem!,0)

tv_menu.selectitem(handle)

il_handle = handle

if typeof(source) = datawindow! and source.classname() = "dw_voce_selezionata" then
	datawindow ldw_datawindow
	ldw_datawindow = source
	wf_nuova_window(ldw_datawindow,handle)
elseif typeof(source) = treeview! and source.classname() = "tv_menu" then
	m_drag_drop menu
	menu = create m_drag_drop
	m_drag_drop.itv_tree = source
	m_drag_drop.il_drop = ll_drop
	m_drag_drop.il_drag = ll_drag
	m_drag_drop.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
else
	return -1
end if
end event

event dragwithin;if handle = 0 or isnull(handle) then
	setdrophighlight(0)
	dragicon = is_immagini + "10.5\MENU_dragstop.ico"
	return -1
end if

if typeof(source) = datawindow! and source.classname() = "dw_voce_selezionata" then
	
	treeviewitem ltv_item

	str_voce_menu lstr_voce
	
	getitem(handle,ltv_item)
	
	lstr_voce = ltv_item.data
	
	if lstr_voce.tipo = "M" then
		setdrophighlight(handle)
		dragicon = is_immagini + "10.5\MENU_dragdrop.ico"
	else
		setdrophighlight(0)
		dragicon = is_immagini + "10.5\MENU_dragstop.ico"
	end if
	
elseif typeof(source) = treeview! and source.classname() = "tv_menu" then
	setdrophighlight(handle)
	dragicon = is_immagini + "10.5\MENU_dragdrop.ico"
else
	setdrophighlight(0)
	dragicon = is_immagini + "10.5\MENU_dragstop.ico"
end if



end event

event endlabeledit;treeviewitem ltv_item

str_voce_menu lstr_voce


getitem(handle,ltv_item)

if newtext = "" then
	g_mb.messagebox("Menu Principale","Immettere una descrizione!",exclamation!)
	editlabel(handle)
	return 1
end if

if isnull(newtext) then
	if ltv_item.label = "" or isnull(ltv_item.label) then
		g_mb.messagebox("Menu Principale","Immettere una descrizione!",exclamation!)
		editlabel(handle)
	end if
	return 1
end if

lstr_voce = ltv_item.data

if lstr_voce.progressivo = 0 then

	update profili
	set    des_profilo = :newtext
	where  cod_profilo = :lstr_voce.cod_profilo;
			 
else
	
	update menu
	set    descrizione_voce = :newtext
	where  cod_profilo = :lstr_voce.cod_profilo and
			 progressivo_menu = :lstr_voce.progressivo;
			 
end if
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Menu Principale","Errore in salvataggio descrizione: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	editlabels = false
	return 1
end if

commit;

editlabels = false

return 0
end event

event itemcollapsing;treeviewitem ltv_item

str_voce_menu lstr_voce


getitem(handle,ltv_item)

lstr_voce = ltv_item.data

if lstr_voce.tipo = "M" and lstr_voce.progressivo <> 0 then
	ltv_item.pictureindex = 1
	ltv_item.selectedpictureindex = 1
else
	return 1
end if

setitem(handle,ltv_item)

return 0
end event

event itemexpanding;treeviewitem ltv_item

str_voce_menu lstr_voce


getitem(handle,ltv_item)

lstr_voce = ltv_item.data

if lstr_voce.tipo = "M" and lstr_voce.progressivo <> 0 then
	ltv_item.pictureindex = 2
	ltv_item.selectedpictureindex = 2
else
	return 1
end if

setitem(handle,ltv_item)

return 0

/*long ll_figlio

treeviewitem ltv_item

str_voce_menu lstr_voce


getitem(handle,ltv_item)

lstr_voce = ltv_item.data

if not(ib_carica_tutto) then

	do
		
		ll_figlio = tv_menu.finditem(childtreeitem!,handle)
		
		if ll_figlio > 0 then
			tv_menu.deleteitem(ll_figlio)
		end if
		
	loop while ll_figlio > 0
	
	if wf_carica_menu(lstr_voce.cod_profilo,lstr_voce.progressivo,handle) > 0 then
		if lstr_voce.tipo = "M" and lstr_voce.progressivo <> 0 then
			ltv_item.pictureindex = 2
			ltv_item.selectedpictureindex = 2
		end if
	end if
	
else
	
	if lstr_voce.tipo = "M" and lstr_voce.progressivo <> 0 then
		ltv_item.pictureindex = 2
		ltv_item.selectedpictureindex = 2
	end if
	
end if

setitem(handle,ltv_item)

return 0*/
end event

event rightclicked;selectitem(handle)

il_handle = handle

if s_cs_xx.admin then
	
	m_menu menu
	
	menu = create m_menu
	
	if not isnull(il_handle) and il_handle > 0 then
		
		treeviewitem ltv_item, ltvi_start
		str_voce_menu lstr_voce, lstr_start
		
		getitem(il_handle,ltv_item)
		lstr_voce = ltv_item.data
		
		if (lstr_voce.tipo = 'M') then
			// controllo se è un profilo
			m_menu.m_copia.enabled = (tv_menu.finditem(parenttreeitem! ,il_handle) > 0)
			m_menu.m_taglia.enabled = m_menu.m_copia.enabled
		else
			m_menu.m_copia.enabled = true
			m_menu.m_taglia.enabled = true
		end if
		
		m_menu.m_incolla.enabled = false
		
		if (il_handle_cutpaste > 0) then
			m_menu.m_incolla.enabled = true
			
			getitem(il_handle_cutpaste, ltvi_start)
			lstr_start = ltvi_start.data
			
			// controllo se copio un profilo all'interno di una cartella o un profilo all'interno di un altro profilo.
			if(lstr_start.tipo = 'M'  and lstr_voce.tipo = 'M') then
				if tv_menu.finditem(parenttreeitem! ,il_handle_cutpaste)<1 then
					m_menu.m_incolla.enabled = false
					
				//controllo il padre
				elseif tv_menu.finditem(parenttreeitem! ,il_handle)<1 then
					m_menu.m_incolla.enabled = false
				end if
				
			//	se copio un menu o cartella e cerco di incollarlo all'interno di una finestra da errore.
			elseif (lstr_start.tipo='M' and lstr_voce.tipo='W') then 
				m_menu.m_incolla.enabled = false
			elseif (lstr_start.tipo='W' and lstr_voce.tipo='W') then 
				m_menu.m_incolla.enabled = false
			end if
		end if
		
		
		choose case lstr_voce.tipo
				
			case "M"
				
				m_menu.m_profili.visible = true
				m_menu.m_separatore.visible = true
				m_menu.m_utenti.visible = true
				m_menu.m_aziende.visible = true
				m_menu.m_aziende_utenti.visible = true
				m_menu.m_separatore_1.visible = true
				m_menu.m_window.visible = true
				m_menu.m_separatore_2.visible = true
				m_menu.m_cartella.visible = true
				m_menu.m_link.visible = true
				
				if ib_intranet then
					m_menu.m_intranet.visible = true
				else
					m_menu.m_intranet.visible = false
				end if
				
				m_menu.m_elimina.visible = true
				m_menu.m_rinomina.visible = true
				
				if lstr_voce.progressivo = 0 then
					m_menu.m_duplica.visible = true
				else
					m_menu.m_duplica.visible = false
				end if
				
				m_menu.m_separatore_3.visible = false
				m_menu.m_flag_nuovo.visible = false
				m_menu.m_flag_modifica.visible = false
				m_menu.m_flag_cancella.visible = false
				m_menu.m_flag_nuovo.checked = false
				m_menu.m_flag_modifica.checked = false
				m_menu.m_flag_cancella.checked = false
				m_menu.m_url.visible = false
				
			case "W"
				
				m_menu.m_profili.visible = true
				m_menu.m_separatore.visible = true
				m_menu.m_utenti.visible = true
				m_menu.m_aziende.visible = true
				m_menu.m_aziende_utenti.visible = true
				m_menu.m_separatore_1.visible = true
				m_menu.m_window.visible = true
				m_menu.m_separatore_2.visible = true
				m_menu.m_cartella.visible = false
				m_menu.m_link.visible = false
				m_menu.m_intranet.visible = false
				m_menu.m_elimina.visible = true
				m_menu.m_rinomina.visible = true
				m_menu.m_duplica.visible = false
				m_menu.m_separatore_3.visible = true
				m_menu.m_flag_nuovo.visible = true
				m_menu.m_flag_modifica.visible = true
				m_menu.m_flag_cancella.visible = true
				m_menu.m_url.visible = false
				
				if lstr_voce.nuovo = "S" then
					m_menu.m_flag_nuovo.checked = true
				else
					m_menu.m_flag_nuovo.checked = false
				end if
				
				if lstr_voce.modifica = "S" then
					m_menu.m_flag_modifica.checked = true
				else
					m_menu.m_flag_modifica.checked = false
				end if
				
				if lstr_voce.cancella = "S" then
					m_menu.m_flag_cancella.checked = true
				else
					m_menu.m_flag_cancella.checked = false
				end if
				
			case "C"
				
				m_menu.m_profili.visible = true
				m_menu.m_separatore.visible = true
				m_menu.m_utenti.visible = true
				m_menu.m_aziende.visible = true
				m_menu.m_aziende_utenti.visible = true
				m_menu.m_separatore_1.visible = true
				m_menu.m_window.visible = true
				m_menu.m_separatore_2.visible = true
				m_menu.m_cartella.visible = false
				m_menu.m_link.visible = false
				m_menu.m_intranet.visible = false
				m_menu.m_elimina.visible = true
				m_menu.m_rinomina.visible = true
				m_menu.m_duplica.visible = false
				m_menu.m_separatore_3.visible = true
				m_menu.m_flag_nuovo.visible = false
				m_menu.m_flag_modifica.visible = false
				m_menu.m_flag_cancella.visible = false
				m_menu.m_flag_nuovo.checked = false
				m_menu.m_flag_modifica.checked = false
				m_menu.m_flag_cancella.checked = false
				m_menu.m_url.visible = true
				
			case "E"
				
				m_menu.m_profili.visible = true
				m_menu.m_separatore.visible = true
				m_menu.m_utenti.visible = true
				m_menu.m_aziende.visible = true
				m_menu.m_aziende_utenti.visible = true
				m_menu.m_separatore_1.visible = true
				m_menu.m_window.visible = true
				m_menu.m_separatore_2.visible = true
				m_menu.m_cartella.visible = false
				m_menu.m_link.visible = false
				m_menu.m_intranet.visible = false
				m_menu.m_elimina.visible = true
				m_menu.m_rinomina.visible = true
				m_menu.m_duplica.visible = false
				m_menu.m_separatore_3.visible = false
				m_menu.m_flag_nuovo.visible = false
				m_menu.m_flag_modifica.visible = false
				m_menu.m_flag_cancella.visible = false
				m_menu.m_flag_nuovo.checked = false
				m_menu.m_flag_modifica.checked = false
				m_menu.m_flag_cancella.checked = false
				m_menu.m_url.visible = false
				
			case "P"
				
				m_menu.m_profili.visible = true
				m_menu.m_separatore.visible = true
				m_menu.m_utenti.visible = true
				m_menu.m_aziende.visible = true
				m_menu.m_aziende_utenti.visible = true
				m_menu.m_separatore_1.visible = true
				m_menu.m_window.visible = true
				m_menu.m_separatore_2.visible = true
				m_menu.m_cartella.visible = false
				m_menu.m_link.visible = false
				m_menu.m_intranet.visible = false
				m_menu.m_elimina.visible = true
				m_menu.m_rinomina.visible = true
				m_menu.m_duplica.visible = false
				m_menu.m_separatore_3.visible = true
				m_menu.m_flag_nuovo.visible = false
				m_menu.m_flag_modifica.visible = false
				m_menu.m_flag_cancella.visible = false
				m_menu.m_flag_nuovo.checked = false
				m_menu.m_flag_modifica.checked = false
				m_menu.m_flag_cancella.checked = false
				m_menu.m_url.visible = true
				
		end choose
		
	else
		
		m_menu.m_profili.visible = true
		m_menu.m_separatore.visible = true
		m_menu.m_utenti.visible = true
		m_menu.m_aziende.visible = true
		m_menu.m_aziende_utenti.visible = true
		m_menu.m_separatore_1.visible = true
		m_menu.m_window.visible = true
		m_menu.m_separatore_2.visible = false
		m_menu.m_cartella.visible = false
		m_menu.m_link.visible = false
		m_menu.m_intranet.visible = false
		m_menu.m_elimina.visible = false
		m_menu.m_rinomina.visible = false
		m_menu.m_duplica.visible = false
		m_menu.m_separatore_3.visible = false
		m_menu.m_flag_nuovo.visible = false
		m_menu.m_flag_modifica.visible = false
		m_menu.m_flag_cancella.visible = false
		m_menu.m_flag_nuovo.checked = false
		m_menu.m_flag_modifica.checked = false
		m_menu.m_flag_cancella.checked = false
		m_menu.m_url.visible = false
		
	end if
	
	if s_cs_xx.cod_utente = "CS_SYSTEM" then
		m_menu.m_separatore_4.visible = true
		m_menu.m_apri_window.visible = true
	else
		m_menu.m_separatore_4.visible = false
		m_menu.m_apri_window.visible = false
	end if
	
	m_menu.m_separatore_0.visible = m_menu.m_cartella.visible 
	m_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
end if
end event

event selectionchanged;if newhandle > 0 then
	il_handle = newhandle
end if
end event

event dragenter;parent.show()

parent.setfocus()

setfocus()
end event

