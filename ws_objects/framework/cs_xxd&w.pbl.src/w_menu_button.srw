﻿$PBExportHeader$w_menu_button.srw
forward
global type w_menu_button from window
end type
type uo_cronologia from uo_icon_box within w_menu_button
end type
type uo_preferiti from uo_icon_box within w_menu_button
end type
type uo_menu from uo_icon_box within w_menu_button
end type
end forward

global type w_menu_button from window
integer width = 1157
integer height = 256
boolean border = false
windowtype windowtype = child!
long backcolor = 268435456
string icon = "AppIcon!"
boolean toolbarvisible = false
windowanimationstyle openanimation = bottomslide!
integer animationtime = 500
event ue_resize ( )
uo_cronologia uo_cronologia
uo_preferiti uo_preferiti
uo_menu uo_menu
end type
global w_menu_button w_menu_button

type variables
//Larghezza minima determinata su una buona resa del menù
long il_larghezza_minima=1156, il_larghezza_preChiusura, il_larghezza_chiusura=176;
boolean ib_blocca_autohide=false, ib_autohide_dipendente=true;
m_blocca_sblocca_menu_button im_menu_destro;

public:
	boolean ib_autohide=false, ib_show=true;
end variables

forward prototypes
public subroutine wf_ridimensiona ()
public subroutine wf_apri_chiudi ()
public subroutine wf_autohide_dipendente (boolean ab_autohide)
public function boolean wf_switch_autohide ()
public subroutine wf_set_autohide (boolean as_autohide)
public function boolean wf_getautohideindipendente ()
public subroutine wf_rilascia_autohide ()
end prototypes

event ue_resize();/**
 * Stefanop
 * 10/09/2010
 *
 * Spostamento del pulsante in base alla dimensione del padre MDI
 **/
 
move(30,  w_cs_xx_mdi.mdi_1.height - this.height + 100 - 20)
end event

public subroutine wf_ridimensiona ();String ls_width

if(Not ib_show) Then
	uo_menu.width=width
	uo_menu.postevent("resize")
	return
end if

if(IsValid(w_menu_80)) Then
	width=w_menu_80.width
else
	if(registryget(s_cs_xx.chiave_root_user + "applicazione_" +  s_cs_xx.profilocorrente,"RLM",ls_width) <> -1) Then
		width=Long(ls_width)
	end if
end if

if(width<il_larghezza_minima) Then
	width= il_larghezza_minima
end if

uo_menu.width=(width/100)*30
uo_menu.postevent("resize")

uo_preferiti.x=uo_menu.width+(width/100)*5
uo_preferiti.width=(width/100)*30
uo_preferiti.postevent("resize")

uo_cronologia.x=uo_preferiti.x+uo_preferiti.width+(width/100)*5
uo_cronologia.width=(width/100)*30
uo_cronologia.postevent("resize")
end subroutine

public subroutine wf_apri_chiudi ();if((ib_autohide) And (Not ib_blocca_autohide)) Then
	if(ib_show) Then
		//Nascondi
		ib_show=false
		il_larghezza_preChiusura=width
		width=il_larghezza_chiusura
	else
		//Mostra
		ib_show=true
		width=il_larghezza_preChiusura
	end if
	wf_ridimensiona()
end if
end subroutine

public subroutine wf_autohide_dipendente (boolean ab_autohide);if(Not ab_autohide) Then
	ib_autohide_dipendente=ib_autohide
	ib_autohide=false
	im_menu_destro.wf_set_etichetta("Menu bloccato")
	im_menu_destro.m_blocca.Enabled=false
else
	ib_autohide=ib_autohide_dipendente
	im_menu_destro.m_blocca.Enabled=true
	if(ib_autohide) Then
		im_menu_destro.wf_set_etichetta("Blocca")
	else
		im_menu_destro.wf_set_etichetta("Sblocca")
	end if
end if
end subroutine

public function boolean wf_switch_autohide ();ib_autohide=Not ib_autohide
ib_autohide_dipendente=ib_autohide

return ib_autohide
end function

public subroutine wf_set_autohide (boolean as_autohide);ib_autohide=as_autohide
ib_autohide_dipendente=ib_autohide

if(ib_autohide) Then
	im_menu_destro.wf_set_etichetta("Blocca")
else
	im_menu_destro.wf_set_etichetta("Sblocca")
end if
end subroutine

public function boolean wf_getautohideindipendente ();return ib_autohide_dipendente;
end function

public subroutine wf_rilascia_autohide ();ib_blocca_autohide=false
yield()
TIMER(2)
yield()
end subroutine

on w_menu_button.create
this.uo_cronologia=create uo_cronologia
this.uo_preferiti=create uo_preferiti
this.uo_menu=create uo_menu
this.Control[]={this.uo_cronologia,&
this.uo_preferiti,&
this.uo_menu}
end on

on w_menu_button.destroy
destroy(this.uo_cronologia)
destroy(this.uo_preferiti)
destroy(this.uo_menu)
end on

event open;long ll_x, ll_y

im_menu_destro = create m_blocca_sblocca_menu_button

uo_menu.uof_Set_Image(s_cs_xx.volume+s_cs_xx.risorse+"menu\home.png")
uo_preferiti.uof_Set_Image(s_cs_xx.volume+s_cs_xx.risorse+"menu\star.png")
uo_cronologia.uof_Set_Image(s_cs_xx.volume+s_cs_xx.risorse+"menu\clock.png")

wf_ridimensiona()
yield()

event ue_resize()
end event

event dragenter;source.dragicon =guo_functions.uof_risorse("10.5\MENU_dragstop.ico")

ib_blocca_autohide=true
yield()
end event

event timer;if(IsValid(w_menu_80))Then
	if ib_autohide and ib_show And (Not w_menu_80.ib_visible) then
		
		long ll_x, ll_y
		
		ll_x = w_cs_xx_mdi.pointerx()
		
		ll_y = w_cs_xx_mdi.pointery()
		
		if ll_x < x or ll_x > (x + width) or ll_y < y or ll_y > (y + height) then
			wf_apri_chiudi()
		end if
		
	end if
end if

Yield()
end event

event mousemove;if(ib_show) Then
	TIMER(1)
end if
end event

event dragdrop;wf_rilascia_autohide()
end event

event rbuttondown;if(IsValid(w_menu_80)) Then
	ib_blocca_autohide=true
	im_menu_destro.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	wf_rilascia_autohide()
end if
end event

event dragleave;wf_rilascia_autohide()
end event

type uo_cronologia from uo_icon_box within w_menu_button
integer x = 805
integer width = 352
integer height = 256
integer taborder = 10
string dragicon = "Information!"
string is_title = "Cronologia"
long il_background_color = 12615680
end type

on uo_cronologia.destroy
call uo_icon_box::destroy
end on

event clicked;call super::clicked;if(Not IsValid(w_menu_80)) Then
	open(w_menu_80)
end if
w_menu_80.wf_apri_chiudi_menu("cronologia")
end event

event dragenter;call super::dragenter;parent.event dragenter(source)
end event

event dragdrop;call super::dragdrop;parent.event dragdrop(source)
end event

event dragleave;call super::dragleave;parent.event dragleave(source)
end event

type uo_preferiti from uo_icon_box within w_menu_button
integer x = 402
integer width = 352
integer height = 256
integer taborder = 10
string is_title = "Preferiti"
long il_background_color = 16763742
end type

on uo_preferiti.destroy
call uo_icon_box::destroy
end on

event clicked;call super::clicked;if(Not IsValid(w_menu_80)) Then
	open(w_menu_80)
end if

w_menu_80.wf_apri_chiudi_menu("preferiti")
end event

event dragenter;call super::dragenter;if(((source.TypeOf()=TreeView!) And (source.className()="tv_menu")) Or ((source.TypeOf()=DataWindow!) And (source.className()="dw_ricerca"))) Then
	String ls_tipo_voce
	
	if(source.TypeOf()=TreeView!) Then
		long ll_handle
		TreeView ltv_menu
		TreeViewItem ltvi_elemento
		str_voce_menu  lstr_voce
		
		ltv_menu=source
		ll_handle=ltv_menu.finditem(currentTreeItem!,0)
		ltv_menu.getItem(ll_handle, ltvi_elemento)
		lstr_voce=ltvi_elemento.data
		ls_tipo_voce=lstr_voce.tipo
		
	else
		DataWindow dw_provvisoria
		dw_provvisoria=source
		
		ls_tipo_voce=dw_provvisoria.getitemstring(dw_provvisoria.getRow(),"flag_tipo_voce")
		
	end if
	
	if(ls_tipo_voce="W") Then
		source.dragicon =guo_functions.uof_risorse("menu\aggiungi_preferito.ico")
		w_menu_80.il_background_color_precedente=w_menu_80.backColor
		w_menu_80.wf_cambia_sfondo(w_menu_80.uo_preferiti.il_background_color)
	end if
	
else
	source.dragicon =guo_functions.uof_risorse("10.5\MENU_dragstop.ico")

end if
ib_blocca_autohide=true
end event

event dragdrop;call super::dragdrop;if(((source.TypeOf()=TreeView!) And (source.className()="tv_menu")) Or ((source.TypeOf()=DataWindow!) And (source.className()="dw_ricerca"))) Then
	Integer li_cod_profilo, li_progressivo_menu
	String ls_nome, ls_collegamento, ls_nuovo, ls_modifica, ls_cancella, ls_tipo_voce
	
	if(source.TypeOf()=TreeView!) Then
		long ll_handle
		TreeView ltv_menu
		TreeViewItem ltvi_elemento
		str_voce_menu  lstr_voce
		
		ltv_menu=source
		ll_handle=ltv_menu.finditem(currentTreeItem!,0)
		ltv_menu.getItem(ll_handle, ltvi_elemento)
		lstr_voce=ltvi_elemento.data
		
		li_cod_profilo=lstr_voce.cod_profilo
		li_progressivo_menu=lstr_voce.progressivo
		ls_nome=ltvi_elemento.label
		ls_collegamento=lstr_voce.collegamento
		ls_nuovo=lstr_voce.nuovo
		ls_modifica=lstr_voce.modifica
		ls_cancella=lstr_voce.cancella
		ls_tipo_voce=lstr_voce.tipo
		
	else
		DataWindow dw_provvisoria
		dw_provvisoria=source
		
		li_cod_profilo=dw_provvisoria.getitemNumber(dw_provvisoria.getRow(),"cod_profilo")
		li_progressivo_menu=dw_provvisoria.getitemNumber(dw_provvisoria.getRow(),"progressivo_menu")
		ls_nome=dw_provvisoria.getitemstring(dw_provvisoria.getRow(),"descrizione_voce")
		ls_collegamento=dw_provvisoria.getitemstring(dw_provvisoria.getRow(),"collegamento")
		ls_nuovo=dw_provvisoria.getitemstring(dw_provvisoria.getRow(),"flag_nuovo")
		ls_modifica=dw_provvisoria.getitemstring(dw_provvisoria.getRow(),"flag_modifica")
		ls_cancella=dw_provvisoria.getitemstring(dw_provvisoria.getRow(),"flag_cancella")
		ls_tipo_voce=dw_provvisoria.getitemstring(dw_provvisoria.getRow(),"flag_tipo_voce")
		
	end if
	
	if(ls_tipo_voce="W") Then
		w_menu_80.wf_cambia_sfondo(w_menu_80.il_background_color_precedente)
		w_menu_80.uo_preferiti.wf_aggiungi_preferito(li_cod_profilo, li_progressivo_menu, ls_nome, ls_collegamento, ls_nuovo, ls_modifica, ls_cancella)
	end if
	
end if
parent.event dragdrop(source)
end event

event dragleave;call super::dragleave;if(((source.TypeOf()=TreeView!) And (source.className()="tv_menu")) Or ((source.TypeOf()=DataWindow!) And (source.className()="dw_ricerca"))) Then
	w_menu_80.wf_cambia_sfondo(w_menu_80.il_background_color_precedente)
end if
end event

type uo_menu from uo_icon_box within w_menu_button
event ue_mousemove pbm_mousemove
integer width = 352
integer height = 256
integer taborder = 10
long backcolor = 33554431
string is_title = "Menù"
end type

event ue_mousemove;if(Not ib_show) Then
	wf_apri_chiudi()
end if
end event

on uo_menu.destroy
call uo_icon_box::destroy
end on

event clicked;call super::clicked;if(Not IsValid(w_menu_80)) Then
	open(w_menu_80)
end if

w_menu_80.wf_apri_chiudi_menu("principale")
if(Not ib_show) Then
	wf_apri_chiudi()
end if
end event

event dragenter;call super::dragenter;parent.event dragenter(source)
end event

event dragdrop;call super::dragdrop;parent.event dragdrop(source)
end event

event dragleave;call super::dragleave;parent.event dragleave(source)
end event

event constructor;call super::constructor;uo_menu.uof_set_title("Menù")
end event

