﻿$PBExportHeader$w_menu_button_old.srw
forward
global type w_menu_button_old from window
end type
type pb_1 from picturebutton within w_menu_button_old
end type
end forward

global type w_menu_button_old from window
integer width = 242
integer height = 216
boolean border = false
windowtype windowtype = child!
long backcolor = 268435456
string icon = "AppIcon!"
boolean toolbarvisible = false
windowanimationstyle openanimation = bottomslide!
integer animationtime = 500
event ue_resize ( )
pb_1 pb_1
end type
global w_menu_button_old w_menu_button_old

event ue_resize();/**
 * Stefanop
 * 10/09/2010
 *
 * Spostamento del pulsante in base alla dimensione del padre MDI
 **/
 
move(30,  w_cs_xx_mdi.mdi_1.height - this.height + 100 - 20)
end event

on w_menu_button_old.create
this.pb_1=create pb_1
this.Control[]={this.pb_1}
end on

on w_menu_button_old.destroy
destroy(this.pb_1)
end on

event open;long ll_x, ll_y

// stefanop 10/09/2010: cambiato percorso
//pb_1.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\start.png"
//pb_1.disabledname = s_cs_xx.volume + s_cs_xx.risorse + "11.5\start.png"

pb_1.picturename = s_cs_xx.volume + s_cs_xx.risorse + "menu\start.png"
pb_1.disabledname = s_cs_xx.volume + s_cs_xx.risorse + "menu\start.png"

backcolor = s_themes.theme[s_themes.current_theme].mdi_backcolor

/** 
stefanop 10/09/2010
ll_x = 0
ll_y = w_cs_xx_mdi.mdi_1.height - this.height + 100 - 5
move(ll_x,ll_y)
**/

event ue_resize()

end event

type pb_1 from picturebutton within w_menu_button_old
integer width = 238
integer height = 212
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean originalsize = true
string powertiptext = "Menù Utente."
end type

event clicked;if isvalid(w_menu_80) then
	
	if w_menu_80.visible then
		w_menu_80.hide()
	else
		w_menu_80.show()
	end if
	
else
	
	open(w_menu_80, w_cs_xx_mdi)
	
end if
end event

