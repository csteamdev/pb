﻿$PBExportHeader$w_menu_lingue.srw
$PBExportComments$Finestra Gestione Prodotti Lingue
forward
global type w_menu_lingue from w_cs_xx_principale
end type
type dw_prodotti_lingue from uo_cs_xx_dw within w_menu_lingue
end type
end forward

global type w_menu_lingue from w_cs_xx_principale
integer width = 2857
integer height = 1148
string title = "Voci Repository Lingue"
dw_prodotti_lingue dw_prodotti_lingue
end type
global w_menu_lingue w_menu_lingue

type variables
private:
	long il_cod_profilo, il_progressivo_menu
	string is_descrizione_voce
end variables

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_prodotti_lingue, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;s_cs_xx_parametri lstr_data

lstr_data = message.powerobjectparm
il_cod_profilo = lstr_data.parametro_ul_1
il_progressivo_menu = lstr_data.parametro_ul_2


dw_prodotti_lingue.set_dw_key("cod_profilo")
dw_prodotti_lingue.set_dw_key("progressivo_menu")
dw_prodotti_lingue.set_dw_options(sqlca,  pcca.null_object, c_scrollparent, c_default)

title = "Voci Repository Lingue - " + lstr_data.parametro_s_1
end event

on w_menu_lingue.create
int iCurrent
call super::create
this.dw_prodotti_lingue=create dw_prodotti_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prodotti_lingue
end on

on w_menu_lingue.destroy
call super::destroy
destroy(this.dw_prodotti_lingue)
end on

type dw_prodotti_lingue from uo_cs_xx_dw within w_menu_lingue
integer x = 23
integer y = 20
integer width = 2766
integer height = 1000
string dataobject = "d_menu_lingue"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

//parent.title = " Voci Repository Lingue - " + ls_descrizione

ll_errore = retrieve( il_cod_profilo, il_progressivo_menu)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
	
	if isnull(this.getitemnumber(ll_i, "cod_profilo")) then
		this.setitem(ll_i, "cod_profilo", il_cod_profilo)
	end if
	
	
	if isnull(this.getitemnumber(ll_i, "progressivo_menu")) then
		this.setitem(ll_i, "progressivo_menu", il_progressivo_menu)
	end if

next
end event

