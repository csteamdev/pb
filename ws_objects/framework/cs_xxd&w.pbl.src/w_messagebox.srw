﻿$PBExportHeader$w_messagebox.srw
forward
global type w_messagebox from window
end type
type pb_detail from picturebutton within w_messagebox
end type
type pb_ok from picturebutton within w_messagebox
end type
type pb_no from picturebutton within w_messagebox
end type
type pb_cancel from picturebutton within w_messagebox
end type
type mle_messagebox from multilineedit within w_messagebox
end type
type pb_mail from picturebutton within w_messagebox
end type
type dw_messagebox from datawindow within w_messagebox
end type
type p_logo from picture within w_messagebox
end type
type ln_1 from line within w_messagebox
end type
end forward

global type w_messagebox from window
integer width = 2258
integer height = 892
boolean titlebar = true
windowtype windowtype = response!
long backcolor = 16777215
string icon = "AppIcon!"
boolean center = true
event type integer ue_mail ( )
pb_detail pb_detail
pb_ok pb_ok
pb_no pb_no
pb_cancel pb_cancel
mle_messagebox mle_messagebox
pb_mail pb_mail
dw_messagebox dw_messagebox
p_logo p_logo
ln_1 ln_1
end type
global w_messagebox w_messagebox

type variables
str_messagebox istr_messagebox

long il_return_ok, il_return_no, il_return_cancel

boolean ib_mail_completed = false, ib_mail_configured = false
end variables

forward prototypes
public function integer wf_fix_newline (ref string as_string)
end prototypes

event type integer ue_mail();//invio mail SMTP a alias assistenza C&S (da parametri multiaziendali)

int		li_rc

long	ll_i

string	ls_destinatari, ls_sender_email, ls_sender_name, ls_server, ls_usr, ls_pwd, ls_oggetto, ls_testo, ls_attachment

n_cpp_smtp l_smtp


select
	stringa
into
	:ls_server
from
	parametri
where
	cod_parametro = 'MSS';

if sqlca.sqlcode < 0 then
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_server) then
	//ls_server = ""
	return -100
end if

select
	stringa
into
	:ls_usr
from
	parametri
where
	cod_parametro = 'MSU';

if sqlca.sqlcode < 0 then
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_usr) then
	//ls_usr = ""
	return -100
end if

select
	stringa
into
	:ls_pwd
from
	parametri
where
	cod_parametro = 'MSP';

if sqlca.sqlcode < 0 then
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_pwd) then
	//ls_pwd = ""
	return -100
end if

select
	stringa
into
	:ls_destinatari
from
	parametri
where
	cod_parametro = 'MSR';

if sqlca.sqlcode < 0 then
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_destinatari) then
	//ls_destinatari = ""
	return -100
end if

ib_mail_configured = true

ls_sender_email = "autoreport@PB-App"

ls_sender_name = f_des_tabella_senza_azienda("aziende","cod_azienda = '" + s_cs_xx.cod_azienda + "'","rag_soc_1")

ls_sender_email = '"' + ls_sender_name + '" <' + ls_sender_email + '>'

ls_oggetto = title

ls_testo = mle_messagebox.text

//------ salva DW in PDF e allega al messaggio ------
	if dw_messagebox.rowcount() > 0 then
		
		ContextKeyword lcxk_base 
		string ls_Path 
		string ls_values[] 
		
		GetContextService("Keyword", lcxk_base)
		lcxk_base.GetContextKeywords("Temp", ls_values)
		ls_Path = ls_values[1]
		
		ls_attachment = ls_path + "\PB_autoreport.pdf"
		
		string ls_color
		
		ls_color = dw_messagebox.object.datawindow.color
		
		dw_messagebox.setredraw(false)
		
		dw_messagebox.object.datawindow.color = rgb(255,255,255)
		
		dw_messagebox.saveas(ls_attachment,PDF!,true)
		
		dw_messagebox.object.datawindow.color = ls_color
		
		dw_messagebox.setredraw(true)
		
	else
		
		ls_attachment = ""
		
	end if
//---------------------------------------------------------

l_smtp = CREATE n_cpp_smtp

TRY
	
	l_smtp.SetRecipientEmail( ls_destinatari)
	
	l_smtp.SetSenderEmail( ls_sender_email )
		
	l_smtp.SetSMTPServer( ls_server )
	
	l_smtp.setusernamepassword( ls_usr,ls_pwd)
	
	l_smtp.SetSubject( ls_oggetto )
	
	l_smtp.SetMessage( ls_testo )
	
	l_smtp.Setattachment( ls_attachment )
	
	l_smtp.seterrormessageson()
	
	li_rc = l_smtp.Send ( )

CATCH ( PBXRuntimeError re )
	
	messagebox("Invio mail","Errore invio smtp mail (PBXRuntimeError): " + re.getMessage(),stopsign!)
	
	destroy l_smtp
	
	return -1	
	
CATCH ( NullObjectError noe )
	
	MessageBox("Invio mail","Errore invio smtp mail (NullObject): " + noe.getMessage(),stopsign!)
	
	destroy l_smtp
	
	return -1
	
CATCH ( Throwable oe )
	
	MessageBox("Invio mail","Errore invio smtp mail (OtherException): " + oe.getMessage(),stopsign!)
	
	destroy l_smtp
	
	return -1
	
END TRY

iF li_rc <> 1 THEN
	
	messagebox("Invio mail","Errore invio smtp mail: " + String ( li_rc ),stopsign!)
	
	destroy l_smtp
	
	return -1
	
END IF

destroy l_smtp

ib_mail_completed = true

return 0
end event

public function integer wf_fix_newline (ref string as_string);long ll_pos


ll_pos = 1

do
	
	ll_pos = pos(as_string,"~n~r",ll_pos)
	
	if ll_pos > 0 then
		as_string = replace(as_string,ll_pos,2,"~r~n")
		ll_pos += 2
	end if
	
loop while ll_pos > 0


ll_pos = 1

do
	
	ll_pos = pos(as_string,"~n",ll_pos)
	
	if ll_pos > 0 then
		if mid(as_string,ll_pos - 1,1) <> "~r" then
			as_string = replace(as_string,ll_pos,1,"~r~n")
			ll_pos += 2
		else
			ll_pos++
		end if
	end if
	
loop while ll_pos > 0


ll_pos = 1

do
	
	ll_pos = pos(as_string,"~r",ll_pos)
	
	if ll_pos > 0 then
		if mid(as_string,ll_pos + 1,1) <> "~n" then
			as_string = replace(as_string,ll_pos,1,"~r~n")
			ll_pos += 2
		else
			ll_pos++
		end if
	end if
	
loop while ll_pos > 0


return 0
end function

event closequery;message.powerobjectparm = istr_messagebox
end event

event open;istr_messagebox = message.powerobjectparm

setnull(message.powerobjectparm)

title = istr_messagebox.title

wf_fix_newline(istr_messagebox.text)

mle_messagebox.text = istr_messagebox.text

/*
choose case istr_messagebox.icon
	case information!
		p_logo.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_icon_info_11.png"
	case stopsign!
		p_logo.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_icon_error_11.png"
	case exclamation!
		p_logo.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_icon_warning_11.png"
	case question!
		p_logo.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_icon_question_11.png"
	case else
		p_logo.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_icon_info_11.png"
end choose
*/ 

pb_ok.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_button_ok_11.png"
pb_no.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_button_no_11.png"
pb_cancel.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_button_cancel_11.png"
pb_detail.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_view_detail.png"

pb_ok.disabledname = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_button_ok_disabled_11.png"
pb_no.disabledname = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_button_no_disabled_11.png"
pb_cancel.disabledname = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_button_cancel_disabled_11.png"


choose case istr_messagebox.buttons
	case ok!
		pb_ok.text = "OK"
		pb_ok.enabled = true
		pb_ok.default = true
		pb_ok.cancel = false
		il_return_ok = 1
		pb_no.text = ""
		pb_no.enabled = false
		pb_no.default = false
		pb_no.cancel = false
		setnull(il_return_no)
		pb_cancel.text = ""
		pb_cancel.enabled = false
		pb_cancel.default = false
		pb_cancel.cancel = false
		setnull(il_return_cancel)
		// --------------------
		pb_cancel.visible = false
		pb_no.visible = false
	case okcancel!
		pb_ok.text = "OK"
		pb_ok.enabled = true
		pb_ok.default = true
		pb_ok.cancel = false
		il_return_ok = 1
		pb_no.text = ""
		pb_no.enabled = false
		pb_no.default = false
		pb_no.cancel = false
		setnull(il_return_no)
		pb_cancel.text = "ANNULLA"
		pb_cancel.enabled = true
		pb_cancel.default = false
		pb_cancel.cancel = true
		il_return_cancel = 2
		// --------------------
		pb_no.visible = false
	case yesno!
		pb_ok.text = "SI"
		pb_ok.enabled = true
		pb_ok.default = true
		pb_ok.cancel = false
		il_return_ok = 1
		pb_no.text = "NO"
		pb_no.enabled = true
		pb_no.default = false
		pb_no.cancel = true
		il_return_no = 2
		pb_cancel.text = ""
		pb_cancel.enabled = false
		pb_cancel.default = false
		pb_cancel.cancel = false
		setnull(il_return_cancel)
		// --------------------
		pb_cancel.visible = false
	case yesnocancel!
		pb_ok.text = "SI"
		pb_ok.enabled = true
		pb_ok.default = true
		pb_ok.cancel = false
		il_return_ok = 1
		pb_no.text = "NO"
		pb_no.enabled = true
		pb_no.default = false
		pb_no.cancel = false
		il_return_no = 2
		pb_cancel.text = "ANNULLA"
		pb_cancel.enabled = true
		pb_cancel.default = false
		pb_cancel.cancel = true
		il_return_cancel = 3
	case else
		pb_ok.text = "OK"
		pb_ok.enabled = true
		pb_ok.default = true
		pb_ok.cancel = false
		il_return_ok = 1
		pb_no.text = ""
		pb_no.enabled = false
		pb_no.default = false
		pb_no.cancel = false
		setnull(il_return_no)
		pb_cancel.text = ""
		pb_cancel.enabled = false
		pb_cancel.default = false
		pb_cancel.cancel = false
		setnull(il_return_cancel)
		// --------------------
		pb_cancel.visible = false
		pb_no.visible = false
end choose

dw_messagebox.InsertRow(1)

if istr_messagebox.tran_info then
	
	dw_messagebox.SetItem(1, "tran_dbms",istr_messagebox.tran_object.dbms)
	dw_messagebox.SetItem(1, "tran_servername",istr_messagebox.tran_object.servername)
	dw_messagebox.SetItem(1, "tran_database",istr_messagebox.tran_object.database)
	dw_messagebox.SetItem(1, "tran_dbparm",istr_messagebox.tran_object.dbparm)
	dw_messagebox.SetItem(1, "tran_sqlcode",istr_messagebox.tran_object.sqlcode)
	dw_messagebox.SetItem(1, "tran_sqldbcode",istr_messagebox.tran_object.sqldbcode)
	dw_messagebox.SetItem(1, "tran_sqlnrows",istr_messagebox.tran_object.sqlnrows)
	dw_messagebox.SetItem(1, "tran_sqlerrtext",istr_messagebox.tran_object.sqlerrtext)
	
end if
	
if istr_messagebox.pb_object_info then
	
	string ls_libraryname, ls_classname
	
	ClassDefinition lcd_class_def
	
	lcd_class_def = istr_messagebox.pb_object.classdefinition
	
	ls_libraryname = lcd_class_def.libraryname
	
	ls_classname = lcd_class_def.name
	
	if not isnull(ls_classname) then
		
		long	ll_pos
	
		ll_pos = 1
		
		do
			
			ll_pos = pos(ls_classname,"`",ll_pos)
			
			if ll_pos > 0 then
				ls_classname = replace(ls_classname,ll_pos,1,".")
				ll_pos++
			end if
			
		loop while ll_pos > 0
		
	end if
	
	dw_messagebox.SetItem(1, "pbobject_libraryname",ls_libraryname)
	dw_messagebox.SetItem(1, "pbobject_classname",ls_classname)
	
end if

if istr_messagebox.systemerror_info then
	
	dw_messagebox.SetItem(1, "systemerror_object_name", Error.WindowMenu)
	dw_messagebox.SetItem(1, "systemerror_control_name", Error.Object)
	dw_messagebox.SetItem(1, "systemerror_event_name", Error.ObjectEvent)
	dw_messagebox.SetItem(1, "systemerror_line_number", Error.Line)
	dw_messagebox.SetItem(1, "systemerror_error_number", Error.Number)
	dw_messagebox.SetItem(1, "systemerror_error_message", Error.Text)
	
end if

pb_mail.picturename =  s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_button_mail_11.png"
pb_mail.disabledname =  s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_button_mail_11.png"

if istr_messagebox.automail then
	pb_mail.postevent("clicked")
end if

//gestione risoluzione colore schermo
	environment env
	getenvironment(env)
	
	if env.numberofcolors < 65000 then
		pb_ok.picturename = ""
		pb_ok.disabledname = ""
		pb_no.picturename = ""
		pb_no.disabledname = ""
		pb_cancel.picturename = ""
		pb_cancel.disabledname = ""
		pb_mail.picturename =  ""
		pb_mail.disabledname =  ""
	end if


end event

on w_messagebox.create
this.pb_detail=create pb_detail
this.pb_ok=create pb_ok
this.pb_no=create pb_no
this.pb_cancel=create pb_cancel
this.mle_messagebox=create mle_messagebox
this.pb_mail=create pb_mail
this.dw_messagebox=create dw_messagebox
this.p_logo=create p_logo
this.ln_1=create ln_1
this.Control[]={this.pb_detail,&
this.pb_ok,&
this.pb_no,&
this.pb_cancel,&
this.mle_messagebox,&
this.pb_mail,&
this.dw_messagebox,&
this.p_logo,&
this.ln_1}
end on

on w_messagebox.destroy
destroy(this.pb_detail)
destroy(this.pb_ok)
destroy(this.pb_no)
destroy(this.pb_cancel)
destroy(this.mle_messagebox)
destroy(this.pb_mail)
destroy(this.dw_messagebox)
destroy(this.p_logo)
destroy(this.ln_1)
end on

type pb_detail from picturebutton within w_messagebox
integer x = 18
integer y = 640
integer width = 146
integer height = 128
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Verdana"
boolean flatstyle = true
boolean originalsize = true
string picturename = "C:\cs_115\framework\RISORSE\11.5\MB_view_detail.png"
end type

event clicked;if parent.height > 1500 then
	this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_view_detail.png"
	parent.height = 812 + 128
else
	this.picturename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_hide_detail.png"
	parent.height = 1912 + 128
end if	
end event

type pb_ok from picturebutton within w_messagebox
integer x = 1733
integer y = 640
integer width = 475
integer height = 128
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "OK"
boolean flatstyle = true
boolean originalsize = true
string picturename = "C:\cs_115\framework\RISORSE\11.5\MB_button_ok_11.png"
alignment htextalign = right!
end type

event clicked;istr_messagebox.return_value = il_return_ok

close(parent)
end event

type pb_no from picturebutton within w_messagebox
integer x = 1225
integer y = 640
integer width = 475
integer height = 128
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "NO"
boolean flatstyle = true
boolean originalsize = true
string picturename = "C:\cs_115\framework\RISORSE\11.5\MB_button_no_11.png"
alignment htextalign = right!
end type

event clicked;istr_messagebox.return_value = il_return_no

close(parent)
end event

type pb_cancel from picturebutton within w_messagebox
integer x = 722
integer y = 640
integer width = 475
integer height = 128
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "ANNULLA"
boolean flatstyle = true
boolean originalsize = true
string picturename = "C:\cs_115\framework\RISORSE\11.5\MB_button_cancel_11.png"
alignment htextalign = right!
end type

event clicked;istr_messagebox.return_value = il_return_cancel

close(parent)
end event

type mle_messagebox from multilineedit within w_messagebox
integer x = 224
integer y = 8
integer width = 1984
integer height = 612
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean hscrollbar = true
boolean vscrollbar = true
alignment alignment = center!
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type pb_mail from picturebutton within w_messagebox
integer x = 219
integer y = 640
integer width = 475
integer height = 128
integer taborder = 50
boolean bringtotop = true
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Verdana"
string text = "MAIL"
boolean flatstyle = true
string picturename = "C:\cs_115\framework\RISORSE\11.5\MB_button_mail_11.png"
alignment htextalign = right!
end type

event clicked;ib_mail_completed = false

parent.event trigger ue_mail()

enabled = false

if not ib_mail_completed then
	disabledname =  s_cs_xx.volume + s_cs_xx.risorse + "11.5\MB_button_mail_disabled_11.png"
	if istr_messagebox.automail then
		if ib_mail_configured then
			mle_messagebox.text = "Si è verificato un errore interno del sistema.~r~n~r~n" + &
											"Non è stato possibile inviare automaticamente una segnalazione al servizio di assistenza.~r~n~r~n" + &
											"E' necessario contattare il fornitore del sistema."
		else
			mle_messagebox.text = "Si è verificato un errore interno del sistema.~r~n~r~n" + &
											"Il sistema non è configurato per l'invio automatico di segnalazioni al servizio di assistenza.~r~n~r~n" + &
											"E' necessario contattare il fornitore del sistema."
		end if
	end if
else
	if istr_messagebox.automail then
		mle_messagebox.text = "Si è verificato un errore interno del sistema.~r~n~r~n" + &
										"E' stata inviata una segnalazione automatica al servizio di assistenza, è ora possibile continuare a lavorare.~r~n~r~n" + &
										"Se il problema persiste contattare il fornitore del sistema."
	end if
end if

end event

type dw_messagebox from datawindow within w_messagebox
integer x = 23
integer y = 824
integer width = 2185
integer height = 1040
integer taborder = 20
string dataobject = "d_messagebox"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event rowfocuschanging;if currentrow = 1 and newrow <> 1 then
	return 1
end if
end event

type p_logo from picture within w_messagebox
integer width = 219
integer height = 192
boolean originalsize = true
string picturename = "C:\cs_115\framework\RISORSE\11.5\MB_icon_error_11.png"
boolean focusrectangle = false
end type

type ln_1 from line within w_messagebox
long linecolor = 33554432
integer linethickness = 6
integer beginx = 23
integer beginy = 796
integer endx = 2185
integer endy = 796
end type

