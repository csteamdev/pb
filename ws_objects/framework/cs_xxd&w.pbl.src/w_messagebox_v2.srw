﻿$PBExportHeader$w_messagebox_v2.srw
forward
global type w_messagebox_v2 from window
end type
type dw_detail from datawindow within w_messagebox_v2
end type
type cb_cancel from commandbutton within w_messagebox_v2
end type
type cb_no from commandbutton within w_messagebox_v2
end type
type p_icon from picture within w_messagebox_v2
end type
type pb_copy from picturebutton within w_messagebox_v2
end type
type pb_email from picturebutton within w_messagebox_v2
end type
type pb_info from picturebutton within w_messagebox_v2
end type
type mle_message from multilineedit within w_messagebox_v2
end type
type cb_ok from commandbutton within w_messagebox_v2
end type
type r_white from rectangle within w_messagebox_v2
end type
type ln_border from line within w_messagebox_v2
end type
end forward

global type w_messagebox_v2 from window
integer width = 1943
integer height = 492
boolean titlebar = true
windowtype windowtype = response!
long backcolor = 12632256
boolean center = true
event we_resize ( )
dw_detail dw_detail
cb_cancel cb_cancel
cb_no cb_no
p_icon p_icon
pb_copy pb_copy
pb_email pb_email
pb_info pb_info
mle_message mle_message
cb_ok cb_ok
r_white r_white
ln_border ln_border
end type
global w_messagebox_v2 w_messagebox_v2

type variables
constant string NEW_LINE = "~r~n"

private:
	string is_error_log
	string is_base_path
	int ii_approssimative_rows = 1
	long il_return_ok, il_return_no, il_return_cancel, il_original_height
	str_messagebox istr_messagebox
	boolean ib_show_icon = true
	boolean ib_email_configured = true
	boolean ib_email_send = false
	
	string is_file_temp[]
	int ii_row_height = 73
end variables

forward prototypes
private subroutine wf_get_pbobject_info (ref string as_libraryname, ref string as_classname)
private subroutine wf_set_error_details ()
private subroutine wf_wordwrap ()
private function string wf_get_error_message ()
public function boolean wf_get_smtp_info (ref string as_smtp_server, ref string as_smtp_usd, ref string as_smtp_pwd, ref string as_smtp_email, ref string as_smtp_from)
public function boolean wf_save_datawindow (ref string as_file_path)
private function boolean wf_send_mail ()
public function boolean wf_save_pdf ()
public subroutine wf_enable_button (boolean ab_enabled)
end prototypes

event we_resize();long ll_mle_max_height = 1000

setredraw(false)

// resize mle
if ib_show_icon then
	p_icon.move(50,20)
	if ii_approssimative_rows > 1 then
		mle_message.move(300,50)
	else
		mle_message.move(300,80)
	end if
	mle_message.width = this.width - 130 - p_icon.x - p_icon.width
else
	mle_message.move(50,50)
	mle_message.width = this.width - 130
end if

//mle_message.height = mle_message.linecount() * 80 // line * altezza della riga
mle_message.height = ii_approssimative_rows * ii_row_height
//if mle_message.height > ll_mle_max_height then
//	mle_message.height = ll_mle_max_height
//	mle_message.hscrollbar = true
//	mle_message.vscrollbar = true
//end if
setredraw(true)
setredraw(false)

r_white.move(0,0)
r_white.resize(width, mle_message.height + 140)
ln_border.move(0, r_white.height)
ln_border.endx = r_white.width
ln_border.endy = r_white.height

this.height = r_white.height + 300

cb_cancel.move(r_white.width - cb_cancel.width - 40, r_white.height + 40)
pb_info.y = cb_cancel.y 
pb_email.y = cb_cancel.y 
pb_copy.y = cb_cancel.y

choose case istr_messagebox.buttons
	case ok!
		cb_ok.move(cb_cancel.x, cb_cancel.y)
		
	case okcancel!
		cb_ok.move(cb_cancel.x - cb_ok.width - 20, cb_cancel.y)

	case yesno!
		cb_no.move(cb_cancel.x, cb_cancel.y)
		cb_ok.move(cb_no.x - cb_ok.width - 20, cb_no.y)

	case yesnocancel!
		cb_no.move(cb_cancel.x - cb_no.width - 20, cb_cancel.y)
		cb_ok.move(cb_no.x - cb_ok.width - 20, cb_no.y)
	
end choose

il_original_height = this.height
setredraw(true)
end event

private subroutine wf_get_pbobject_info (ref string as_libraryname, ref string as_classname);long	ll_pos
ClassDefinition lcd_class_def

lcd_class_def = istr_messagebox.pb_object.classdefinition
as_libraryname = lcd_class_def.libraryname
as_classname = lcd_class_def.name

if not isnull(as_classname) then	
	ll_pos = 1
	
	do
		ll_pos = pos(as_classname,"`",ll_pos)
		
		if ll_pos > 0 then
			as_classname = replace(as_classname,ll_pos,1,".")
			ll_pos++
		end if
		
	loop while ll_pos > 0

end if
end subroutine

private subroutine wf_set_error_details ();string ls_classname, ls_libraryname

if not istr_messagebox.tran_info and not istr_messagebox.pb_object_info and not istr_messagebox.systemerror_info then
	pb_info.visible = false
	pb_email.visible = false
	pb_copy.visible = false
	return
else
	dw_detail.InsertRow(1)
end if

if istr_messagebox.tran_info then
	
	dw_detail.SetItem(1, "tran_dbms",istr_messagebox.tran_object.dbms)
	dw_detail.SetItem(1, "tran_servername",istr_messagebox.tran_object.servername)
	dw_detail.SetItem(1, "tran_database",istr_messagebox.tran_object.database)
	dw_detail.SetItem(1, "tran_dbparm",istr_messagebox.tran_object.dbparm)
	dw_detail.SetItem(1, "tran_sqlcode",istr_messagebox.tran_object.sqlcode)
	dw_detail.SetItem(1, "tran_sqldbcode",istr_messagebox.tran_object.sqldbcode)
	dw_detail.SetItem(1, "tran_sqlnrows",istr_messagebox.tran_object.sqlnrows)
	dw_detail.SetItem(1, "tran_sqlerrtext",istr_messagebox.tran_object.sqlerrtext)
	
end if

if istr_messagebox.pb_object_info then
	wf_get_pbobject_info(ref ls_libraryname, ref ls_classname)
	dw_detail.SetItem(1, "pbobject_libraryname",ls_libraryname)
	dw_detail.SetItem(1, "pbobject_classname",ls_classname)
	
end if

if istr_messagebox.systemerror_info then
	
	dw_detail.SetItem(1, "systemerror_object_name", Error.WindowMenu)
	dw_detail.SetItem(1, "systemerror_control_name", Error.Object)
	dw_detail.SetItem(1, "systemerror_event_name", Error.ObjectEvent)
	dw_detail.SetItem(1, "systemerror_line_number", Error.Line)
	dw_detail.SetItem(1, "systemerror_error_number", Error.Number)
	dw_detail.SetItem(1, "systemerror_error_message", Error.Text)
	
end if
end subroutine

private subroutine wf_wordwrap ();/**
 * Stefanop
 * 06/09/2010
 *
 * Consente di calcolare quanti caratteri possono essere visualizzati in una riga all'interno
 * del messaggio e spezza la riga in modo da mandarla a capo.
 * PB Fango è l'unico a non avere sta opzione di default
 **/

string ls_message
int li_max_word = 65, li_approssimative_rows
long ll_pos, ll_pos_start, ll_length

if ib_show_icon then li_max_word = 55
//if mle_message.text = "" or len(mle_message.text) <= li_max_word then return
if mle_message.text = "" then return

// calcolo il numero di riga
li_approssimative_rows = 0
ll_pos_start = 1

ll_pos = pos(mle_message.text, NEW_LINE)
if ll_pos > 0 then
		
	do while ll_pos > 0		
		// recupero la linea
		ll_length = ll_pos - ll_pos_start
		
		if ll_length > li_max_word then
			li_approssimative_rows += int(ll_length / li_max_word) + 1
		else
			li_approssimative_rows += 1
		end if

		ll_pos_start = ll_pos + 1
		ll_pos = pos(mle_message.text, NEW_LINE, ll_pos_start)
	loop
	
	// controllo eventuale stringa rimanente
	ll_length = len(mle_message.text) - ll_pos_start
	if ll_length > li_max_word then
		li_approssimative_rows += int(ll_length / li_max_word) + 1
	else
		li_approssimative_rows += 1
	end if
	
else
	li_approssimative_rows += int(len(mle_message.text) / li_max_word) + 1
end if

ii_approssimative_rows = li_approssimative_rows
end subroutine

private function string wf_get_error_message ();/**
 * Stefanop
 * 06/09/2010
 *
 * Crea il messaggio di errore da inviare per email ed inserire nella tabella log_sistema
 **/
 
return "Text: " + Error.text + "~r~nObject: " +  Error.object + "~r~nScript: " + Error.ObjectEvent + "(" + string(Error.line) + ")"
end function

public function boolean wf_get_smtp_info (ref string as_smtp_server, ref string as_smtp_usd, ref string as_smtp_pwd, ref string as_smtp_email, ref string as_smtp_from);/**
 * Stefanop
 * 06/09/2010
 *
 * Recupera le credenziali per l'invio email tramite SMTP
 **/
 
// SMTP SERVER
select stringa
into :as_smtp_server
from parametri
where
	cod_parametro = 'MSS';

if sqlca.sqlcode <> 0 or isnull(as_smtp_server) or as_smtp_server = "" then
	
//Giulio: 17/11/2011
//	messagebox("", "Attenzione: configurare il parametro MSS indicando l'indirizzo del server SMTP")
	return false
end if

// SMTP USERNAME
select stringa
into :as_smtp_usd
from parametri
where
	cod_parametro = 'MSU';

if sqlca.sqlcode < 0 then
	return false
end if

// SMTP PASSWORD
select	stringa
into :as_smtp_pwd
from parametri
where
	cod_parametro = 'MSP';

if sqlca.sqlcode < 0  then
	return false
end if

// SMTP DESTINATARIO
select stringa
into :as_smtp_email
from parametri
where
	cod_parametro = 'MSR';

if sqlca.sqlcode <> 0 or isnull(as_smtp_email) or as_smtp_email = "" then
	
//Giulio: 17/11/2011
//	messagebox("", "Attenzione: configurare il parametro MSR indicando l'indirizzo del destinatario")
	return false
end if

// SMTP INVIO
select e_mail
into :as_smtp_from
from utenti
where
	cod_utente = :s_cs_xx.cod_utente;
	
if sqlca.sqlcode <> 0 or isnull(as_smtp_from) or as_smtp_from = "" then
	//Giulio: 17/11/2011
//	messagebox("","Attenzione: l'utente non ha una mail valida per poter inviare la segnalazione")
// ------ Fine modifica Giulio
	return false
end if
	

return true
end function

public function boolean wf_save_datawindow (ref string as_file_path);string ls_path
datetime ldt_today

ls_path = f_env_var("APPDATA") + "\csteam\"
if not directoryexists(ls_path) then createdirectory(ls_path)

ldt_today = datetime(today(), now())
ls_path += string(ldt_today, "ddmmyyhhmmss") + ".pdf"

if dw_detail.saveas(ls_path, pdf!, true, EncodingUTF8!) < 1 then
	setnull(as_file_path)
	return false
else
	is_file_temp[upperbound(is_file_temp) + 1] = ls_path
	as_file_path = ls_path
	return true
end if
end function

private function boolean wf_send_mail ();string ls_pathsendmail, ls_logfile, ls_smtp_server, ls_smtp_usd, ls_smtp_pwd, ls_smtp_email, &
		ls_smtp_from, ls_azienda, ls_oggetto, ls_msg, ls_atachment

if not ib_email_configured then return false

if not wf_get_smtp_info(ref ls_smtp_server, ref ls_smtp_usd, ref ls_smtp_pwd, ref ls_smtp_email, ref ls_smtp_from) then
	ib_email_configured = false
	
	// chiedo di salvare il pdf
	return wf_save_pdf()
end if

ls_azienda = f_des_tabella_senza_azienda("aziende","cod_azienda = '" + s_cs_xx.cod_azienda + "'","rag_soc_1")
ls_oggetto = ls_azienda + " - segnalazione automatica errore"
ls_msg = wf_get_error_message()


ls_pathsendmail = s_cs_xx.volume + s_cs_xx.risorse+"sendmail\sendemail.exe"
ls_logfile = s_cs_xx.volume + s_cs_xx.risorse+"sendmail\log\mail_" + string(today(), "dd_mm_yyyy") + ".log"
ls_pathsendmail += " -f " + ls_smtp_from
ls_pathsendmail += " -t " + ls_smtp_email
ls_pathsendmail += ' -u "' + ls_oggetto + '"'
ls_pathsendmail += ' -m "' + ls_msg + '"'
ls_pathsendmail += " -q "
ls_pathsendmail += " -s " + ls_smtp_server

if ls_smtp_usd<>"" and not isnull(ls_smtp_usd) then
            ls_pathsendmail += " -xu " + ls_smtp_usd
            ls_pathsendmail += " -xp " + ls_smtp_pwd
end if

ls_pathsendmail += " -l " + ls_logfile
//ls_pathsendmail += " -o message-content-type=html"
//ls_pathsendmail += " -o message-file="+ls_msg_standard

if dw_detail.rowcount() > 0 then
	if wf_save_datawindow(ref ls_atachment) then
		ls_pathsendmail += ' -a "' + ls_atachment + '"'
	end if
end if

try
	run(ls_pathsendmail, Minimized!)
	return true
catch (RuntimeError e)
	return false
end try
end function

public function boolean wf_save_pdf ();/**
 * Stefanop
 * 13/09/2010
 *
 * chiede all'utente dove salvare la DW in formato PDF
 **/
 
string ls_path, ls_file_name
int li_ret

pb_email.picturename = is_base_path + "pdf.png"

li_ret = GetFileSaveName("Seleziona percorso", ls_path, ls_file_name, "pdf", "PDF Document (*.pdf),*.pdf")
if li_ret < 1 then return false

if mid(ls_path, len(ls_path) - 3) <> "pdf" then ls_path += ".pdf"

if dw_detail.saveas(ls_path, pdf!, true, EncodingUTF8!) < 1 then
	g_mb.error("Errore durante il salvataggio del PDF")
	return false
end if

return true
end function

public subroutine wf_enable_button (boolean ab_enabled);cb_ok.enabled = ab_enabled
cb_no.enabled = ab_enabled
cb_cancel.enabled = ab_enabled
end subroutine

on w_messagebox_v2.create
this.dw_detail=create dw_detail
this.cb_cancel=create cb_cancel
this.cb_no=create cb_no
this.p_icon=create p_icon
this.pb_copy=create pb_copy
this.pb_email=create pb_email
this.pb_info=create pb_info
this.mle_message=create mle_message
this.cb_ok=create cb_ok
this.r_white=create r_white
this.ln_border=create ln_border
this.Control[]={this.dw_detail,&
this.cb_cancel,&
this.cb_no,&
this.p_icon,&
this.pb_copy,&
this.pb_email,&
this.pb_info,&
this.mle_message,&
this.cb_ok,&
this.r_white,&
this.ln_border}
end on

on w_messagebox_v2.destroy
destroy(this.dw_detail)
destroy(this.cb_cancel)
destroy(this.cb_no)
destroy(this.p_icon)
destroy(this.pb_copy)
destroy(this.pb_email)
destroy(this.pb_info)
destroy(this.mle_message)
destroy(this.cb_ok)
destroy(this.r_white)
destroy(this.ln_border)
end on

event open;boolean ib_show_extra_buttons = false

istr_messagebox = message.powerobjectparm
setnull(message.powerobjectparm)

is_base_path = s_cs_xx.volume + s_cs_xx.risorse + "messagebox\"
title = istr_messagebox.title
mle_message.text = istr_messagebox.text
wf_wordwrap()

choose case istr_messagebox.icon
	case 1
		p_icon.picturename = is_base_path + "info-icon.png"
		
	case 2
		p_icon.picturename = is_base_path + "cancel-icon.png"
		ib_show_extra_buttons = true
		
	case 3, 6
		p_icon.picturename = is_base_path + "alert-icon.png"
		ib_show_extra_buttons = true
		
	case 4
		p_icon.picturename = is_base_path + "faq-icon.png"
		
	case 5
		p_icon.picturename = is_base_path + "success-icon.png"
		
	case else
		ib_show_icon = false
end choose

if not ib_show_icon then p_icon.visible = false

setnull(il_return_ok)
setnull(il_return_no)
setnull(il_return_cancel)

pb_email.visible = false
pb_info.visible = false
pb_copy.visible = false
dw_detail.visible = false

pb_info.picturename = is_base_path + "information.png"
pb_email.picturename = is_base_path + "email_edit.png"
pb_email.disabledname = is_base_path + "email_edit_disabled.png"
pb_copy.picturename = is_base_path + "paste_plain.png"

// pulsanti
choose case istr_messagebox.buttons
	case ok!
		cb_ok.text = "OK"
		cb_no.visible = false
		cb_cancel.visible = false
		il_return_ok = 1
		cb_ok.default = true

	case okcancel!
		cb_ok.text = "OK"
		cb_cancel.text = "Annulla"
		cb_no.visible = false
		il_return_ok = 1
		il_return_cancel = 2
		
		choose case istr_messagebox.default_button
			case 1
				cb_ok.default = true
				
			case 2
				cb_cancel.default = true
		end choose

	case yesno!
		cb_ok.text = "Si"
		cb_no.text = "No"
		cb_cancel.visible = false
		il_return_ok = 1
		il_return_no = 2
		
		choose case istr_messagebox.default_button
			case 1
				cb_ok.default = true
				
			case 2
				cb_no.default = true
				
		end choose

	case yesnocancel!
		cb_ok.text = "Si"		
		cb_no.text = "No"
		cb_cancel.text = "Annulla"
		il_return_ok = 1
		il_return_no = 2
		il_return_cancel = 3
		
		choose case istr_messagebox.default_button
			case 1
				cb_ok.default = true
				
			case 2
				cb_no.default = true
				
			case 3
				cb_cancel.default = true
		end choose
		
	case else
		cb_ok.text = "OK"
		cb_no.visible = false
		cb_cancel.visible = false
		il_return_ok = 1
		cb_ok.default = true
end choose

if ib_show_extra_buttons or istr_messagebox.tran_info or istr_messagebox.systemerror_info then
	pb_email.visible = true
	pb_info.visible = true
	pb_copy.visible = true
	wf_set_error_details()
end if

if s_cs_xx.cod_utente = "CS_SYSTEM" then
	ib_email_configured = false
elseif istr_messagebox.automail then
	pb_email.postevent("clicked")
end if

if cb_ok.default then
	cb_ok.setfocus()
elseif cb_no.default then
	cb_no.setfocus()
else
	cb_cancel.setfocus()
end if

event we_resize()
end event

event closequery;int li_file

if istr_messagebox.buttons <> ok! then
	// non è possibile chiudere la mascherea tramite la X perchè non darebbe nessun risultato
	// coerente. (return 0)
	if isnull(istr_messagebox.return_value) or istr_messagebox.return_value = 0 then
		return 1
	end if
else
	istr_messagebox.return_value = il_return_ok
end if

// elimino eventuali file temporanei
if upperbound(is_file_temp) > 0 then
	for li_file = 1 to upperbound(is_file_temp)
		filedelete(is_file_temp[li_file])
	next
end if
// ----

message.powerobjectparm = istr_messagebox
end event

type dw_detail from datawindow within w_messagebox_v2
integer x = 18
integer y = 420
integer width = 1879
integer height = 1100
integer taborder = 40
string title = "none"
string dataobject = "d_messagebox"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_cancel from commandbutton within w_messagebox_v2
integer x = 1486
integer y = 260
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Annulla"
end type

event clicked;istr_messagebox.return_value = il_return_cancel

close(parent)
end event

type cb_no from commandbutton within w_messagebox_v2
integer x = 1029
integer y = 260
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "No"
end type

event clicked;istr_messagebox.return_value = il_return_no

close(parent)
end event

type p_icon from picture within w_messagebox_v2
integer x = 46
integer y = 20
integer width = 219
integer height = 192
boolean originalsize = true
string picturename = "C:\cs_115\framework\risorse\messagebox\cancel-icon.png"
boolean focusrectangle = false
boolean map3dcolors = true
end type

type pb_copy from picturebutton within w_messagebox_v2
integer x = 297
integer y = 260
integer width = 110
integer height = 96
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = " "
string picturename = "C:\cs_115\framework\risorse\messagebox\paste_plain.png"
alignment htextalign = left!
string powertiptext = "Copia negli appunti"
end type

event clicked;mle_message.SelectText(0, len(mle_message.text))
mle_message.copy()
mle_message.SelectText(0,0)
end event

type pb_email from picturebutton within w_messagebox_v2
integer x = 183
integer y = 260
integer width = 110
integer height = 96
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string picturename = "C:\cs_115\framework\risorse\messagebox\email_edit.png"
string disabledname = "C:\cs_115\framework\risorse\messagebox\email_edit.png"
alignment htextalign = left!
string powertiptext = "Invia mail di avviso"
end type

event clicked;if ib_email_configured then
	// se mail configurata allora controllo invio	
	if ib_email_send then return

	wf_enable_button(false)
	// se non ancora inviata invio
	if wf_send_mail() then
		picturename = is_base_path + "email_send.png"
		powertiptext = "Email di segnalazione inviata correttamente"
		ib_email_send = true
	end if
	
	wf_enable_button(true)
else
	// altrimenti chiedo dove salvare il PDF
	wf_save_pdf()
	
end if
end event

type pb_info from picturebutton within w_messagebox_v2
integer x = 69
integer y = 260
integer width = 110
integer height = 96
integer taborder = 70
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string picturename = "C:\cs_115\framework\risorse\messagebox\information.png"
alignment htextalign = left!
end type

event clicked;if parent.height > il_original_height then
	// CHIUDO
	parent.height = il_original_height
	dw_detail.visible = false
else
	// APRO
	parent.height += 1100
	dw_detail.move(20, cb_ok.y + cb_ok.height + 40)
	dw_detail.resize(parent.width - 70, parent.height - this.y - 300)
	dw_detail.visible = true
end if
end event

type mle_message from multilineedit within w_messagebox_v2
integer x = 302
integer y = 80
integer width = 1531
integer height = 76
integer taborder = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "MessageBox"
boolean border = false
boolean displayonly = true
borderstyle borderstyle = stylelowered!
boolean hideselection = false
end type

type cb_ok from commandbutton within w_messagebox_v2
integer x = 594
integer y = 260
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Si"
end type

event clicked;istr_messagebox.return_value = il_return_ok

close(parent)
end event

type r_white from rectangle within w_messagebox_v2
long linecolor = 33554431
integer linethickness = 4
long fillcolor = 1073741824
integer width = 1934
integer height = 220
end type

type ln_border from line within w_messagebox_v2
long linecolor = 10789024
integer linethickness = 4
integer beginx = 343
integer beginy = 240
integer endx = 663
integer endy = 240
end type

